![输入图片说明](https://images.legendshop.cn/open/BBC-SHOP.png "BBC.png")
一个基于spring boot、spring oauth2.0、redis的轻量级、前后端分离、防范xss攻击、拥有分布式锁，为生产环境多实例完全准备，数据库为b2b2c设计，拥有完整功能的开源商城


## 前言

`Legendshop`神奇商城项目是一个完整的电商平台， 包括商家端，用户端，管理员端等， 致力于为中小企业打造一个完整、易于维护的开源的电商系统，采用spring boot等流行技术实现。
后台管理系统包含商品管理、订单管理、运费模板、规格管理、会员管理、运营管理、内容管理、统计报表、权限管理、设置等模块所有电商需要的模块，采用docker-compose方式可以实现一键部署。

## 文档

参考文档
gitee：https://gitee.com/legendshop/legendshop-docs


## 授权

除了开源版本，我们商业版有SAAS版，B2C和B2B2C商城，多端呈现：小程序 + PC + H5 + APP，更多详情请查看官网

Legendshop官网 https://www.legendshop.cn

Legendshop 使用 AGPLv3 开源，请遵守 AGPLv3 的相关条款，或者联系作者获取商业授权(https://www.legendshop.cn)

## 项目链接

java后台：https://gitee.com/legendshop/legendshop5.5.git

vue用户前端：https://gitee.com/legendshop/uniapp-mall.git

新零售商城系统SAAS小程序：https://gitee.com/legendshop/wxmp-mall.git

新零售商城系统SAAS小程序：https://gitee.com/legendshop/wx-mall.git

## 演示地址

小程序演示

![小程序](https://images.legendshop.cn/open/micro-program.jpg)

H5用户端：https://mdiamonds.legendshop.cn/
![H5用户端](https://images.legendshop.cn/open/user-h5.png) 

用户PC端：http://diamonds.legendshop.cn/

![用户PC端](https://images.legendshop.cn/open/pc-front.png)


管理后台：http://diamonds.admin.legendshop.cn/

![管理后台](https://images.legendshop.cn/open/pc-admin.png)

## 技术选型

| 技术                   | 版本   | 说明                                    |
| ---------------------- | ------ | --------------------------------------- |
| Spring Boot            | 2.0.9  | MVC核心框架                             |
| Spring Security oauth2 | 2.1.6  | 认证和授权框架                          |
| JdbcTemplate/JdbcPlus   | 3.5.9  | ORM框架                                 |
| Swagger-UI             | 2.9.2  | 文档生产工具                            |
| Hibernator-Validator   | 6.0.16 | 验证框架                                |
| redisson               | 3.9.1 | 对redis进行封装、集成分布式锁等         |
| druid                 | 1.1.9  | 数据库连接池                            |
| lombok                 | 1.18.8 | 简化对象封装工具                        |
| hutool                 | 4.5.0  | 更适合国人的java工具集                  |
| swagger-bootstrap      | 1.9.3  | 基于swagger，更便于国人使用的swagger ui |



## 部署教程

ps: 如果你不清楚如何启动我们的商城，请仔细阅wiki当中的文档


https:https://gitee.com/legendshop/legendshop-docs



- Legendshop官方技术QQ 1群：96642931
  
- Legendshop官方技术QQ 2群：190339088


## 更多信息请查看官网 <https://www.legendshop.cn>
