package com.legendshop.app;

import com.legendshop.config.PropertiesUtil;
import com.legendshop.framework.event.EventHome;
import com.legendshop.framework.event.SysInitEvent;
import com.legendshop.framework.handler.impl.ContextServiceLocator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;

@EnableCaching
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class,
        RedisAutoConfiguration.class, AopAutoConfiguration.class,
        AopAutoConfiguration.class}, scanBasePackages = {"com.legendshop"})
@ImportResource({"classpath:spring/applicationContext.xml"})
public class LegendShopAppApplication {
    /**
     * 日志.
     */
    private final static Logger log = LoggerFactory.getLogger(LegendShopAppApplication.class);

    /**
     * 主方法
     */
    public static void main(String[] args) {
        SpringApplication.run(LegendShopAppApplication.class, args);
		ApplicationContext ctx = ContextServiceLocator.getApplicationContext();
		System.out.println("LegendShopApplication 启动成功！");
		PropertiesUtil propertiesUtil = (PropertiesUtil)ctx.getBean("propertiesUtil");
		// 4. 发送系统启动事件
		EventHome.publishEvent(new SysInitEvent(propertiesUtil.getPcDomainName()));
        // 将hook线程添加到运行时环境中去
        Runtime.getRuntime().addShutdownHook(new CleanWorkThread());
    }

    /**
     * hook线程,关闭时调用
     */
    static class CleanWorkThread extends Thread {
        @Override
        public void run() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            log.info(" LegendShop Frontend System shutdowned.");
        }
    }

}
