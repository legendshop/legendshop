/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;

import springfox.documentation.annotations.ApiIgnore;

/**
 * 错误页面
 */
@ApiIgnore
@Controller
@RequestMapping("/error")
public class AppErrorController extends BaseController {

    @GetMapping("/400")
    public String error400(HttpServletRequest request, HttpServletResponse response) {
        return "/common/400";
    }

    @GetMapping("/404")
    public String error404(HttpServletRequest request, HttpServletResponse response) {
        return "/common/404";
    }

    @GetMapping("/405")
    public String error405(HttpServletRequest request, HttpServletResponse response) {
        return "/common/405";
    }

    @GetMapping("/500")
    public String error500(HttpServletRequest request, HttpServletResponse response) {
        return "/common/500";
    }

}
