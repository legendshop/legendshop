/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.filter;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.constant.AppTokenTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.app.service.AppTokenService;
import com.legendshop.app.util.AppTokenUtil;
import com.legendshop.model.dto.app.AppTokenDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.UserTokenContext;
import com.legendshop.model.entity.AppToken;
import com.legendshop.util.AppUtils;

/**
 * 过滤器.
 */

@Order(-999)
@Component
@WebFilter(urlPatterns = "/*",filterName = "legendAppFilter")
public class LegendAppFilter implements Filter{

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(LegendAppFilter.class);

	@Autowired
	private AppTokenService appTokenService;
	
	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest)req;
		HttpServletResponse response = (HttpServletResponse)resp;
		try {
			String requestURI = request.getRequestURI();
			String userId = request.getHeader("userId");
			String accessToken = request.getHeader("accessToken");
			String securiyCode = request.getHeader("securiyCode");
			String verId = request.getHeader("verId");
			String sign = request.getHeader("sign");
			String platform = request.getHeader("platform");
			
			if(AppUtils.isBlank(verId)){
				verId = "1.0";
			}

			log.info("<!-- Access User App URL {} With Parameters: userId={}, accessToken={}, securiyCode={} ", requestURI, userId, accessToken, securiyCode);

			boolean needAuth = requestURI.indexOf("/p/")  != -1; // 是否需要验证token
			// 验证token
			AppToken appToken = appTokenService.getAppToken(userId,platform,AppTokenTypeEnum.USER.value());

			// 校验参数签名
			ResultDto<Object> result = AppTokenUtil.checkAppToken(appToken, userId, verId, sign, accessToken, securiyCode);
			if(result.getStatus() == 1) {//成功并放行
				AppTokenDto token = new AppTokenDto(appToken.getId(), appToken.getUserId(), appToken.getUserName(), appToken.getAccessToken(), verId, appToken.getOpenId(), appToken.getPlatform());
				UserTokenContext.setToken(token);
			}else {
				if(needAuth) {//需要校验,返回错误页面
					String data = JSONObject.toJSONString(result);
					PrintWriter writer = response.getWriter();
					writer.write(data);
					response.setStatus(200);
					return;
				}
			}
			
			chain.doFilter(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			String data = "{\"msg\":\"System Exception！\" ,\"result\":null,\"status\":\"-999\",\"verId\":\"1.0\"}";
			PrintWriter writer = response.getWriter();
			writer.write(data);
			response.setStatus(200);
		} finally {
			UserTokenContext.clean();
		}

	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}


}
