<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${not empty result.adverts}">
   <div class="floor01 clear">
      <c:forEach items="${result.adverts}" var="advert">
         <c:choose>
			             <c:when test="${not empty advert.actionParam && not empty advert.actionParam.searchParam && fn:length(advert.actionParam.searchParam)>0 }">
			                   <c:choose>
			                      <c:when test="${not empty advert.actionParam.searchParam['brands']}">
			                         <a href="${contextPath}/m_search/list?prop=brandId:${advert.actionParam.searchParam['brands']['brandId']}"><img src="${photopath}${advert.img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty advert.actionParam.searchParam['cates']}">
			                         <a href="${contextPath}/m_search/list?categoryId=${advert.actionParam.searchParam['cates']['catId']}"><img src="${photopath}${advert.img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty advert.actionParam.searchParam['themes']}">
			                         <a href="${contextPath}/theme/themeDetail/${advert.actionParam.searchParam['themes']['themeId']}"><img src="${photopath}${advert.img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty advert.actionParam.searchParam['goods']}">
			                         <a href="${contextPath}/views/${advert.actionParam.searchParam['goods']['goodsInfoId']}"><img src="${photopath}${advert.img}" alt=""></a>
			                      </c:when>
			            </c:choose> 
			       </c:when>
			       <c:otherwise>
			                  <a href="javascript:void(0);"><img src="${photopath}${advert.img}" alt=""></a>
			     </c:otherwise>
		</c:choose>
       </c:forEach>
   	</div>
 </c:if>