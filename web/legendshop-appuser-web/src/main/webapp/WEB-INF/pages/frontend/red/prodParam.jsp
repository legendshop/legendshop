<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,maximum-scale=1.3,user-scalable=yes">
<title>商品详情</title>
<body>
    <div class="desc-area" style="padding: 0px 10px 0 10px;">
       <c:choose>
		<c:when test="${not empty prodParams}">
			<c:forEach items="${prodParams}" var="paramGroup">
				<c:forEach items="${paramGroup.params}" var="p">
					<p style="MARGIN: 13.45pt 0cm; BACKGROUND: white">
						<span style="font-family:Tahoma;color:#404040;FONT-SIZE: 10.5pt; mso-bidi-font-family: Tahoma; mso-ascii-font-family: Tahoma; mso-hansi-font-family: Tahoma">
							<span style="font-family:宋体;">${p.paramName}：</span>
						</span>
						<span lang="EN-US" style="font-family:'Tahoma','sans-serif';color:#404040;FONT-SIZE: 10.5pt">${p.paramValueName}</span>
					</p>
				</c:forEach>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<p style="MARGIN: 13.45pt 0cm; BACKGROUND: white">此商品没有参数</p>
		</c:otherwise>
	  </c:choose>
    </div>
</body>
</html>

