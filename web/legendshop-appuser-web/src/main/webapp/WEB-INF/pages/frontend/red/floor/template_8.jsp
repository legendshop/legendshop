<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<style type="text/css"> 
.floor08 img{
	width:100%;
}
</style>
<c:if test="${not empty floor.content}">
	<div class="floor floor08" style="padding:0;">
		${floor.content}
	</div>
</c:if>
