<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp' %>

<div class="search" style="display: none;">
         <div class="search_bg" style="background-color: rgb(255, 76, 100); opacity: 1;"></div>
         <form action="${contextPath}/m_search/list" method="get" id="searchProductForm2">
         <input  name="curPageNO" type="hidden"  value="1">
		        <div class="search_box">
		            <div class="s_left">
		                <a href="javascript:;" class="backto_page">×</a>
		            </div>
		            <div class="search_form">
		                <i class="ion-ios-search-strong"></i>
		                <input id="searchInput" name="keyword" type="text" placeholder="搜索商品">
		            </div>
		            <div class="s_right">
		                <a href="javascript:" id="search_btn" class="search_btn">搜索</a>
		            </div>
		        </div>
         </form>
         <div class="search_page">
            <div class="search_history">
                <h4>历史搜索</h4>
                <ul id="historySearch"><li><a onclick="subForm(this)" attr-value="你好" href="javascript:;">你好</a></li></ul>
                <div class="clear_history">
                    <button class="btn btn-full-grey" onclick="clearCookie('searchTitle')">清空搜索历史记录</button>
                </div>
            </div>
        </div>
    </div>

	
<script type="text/javascript">
  $(function(){
        getList();

        $('#searchInput').on("keydown", function(event) {
            var key = event.which;
            if (key == 13) {
                $("#search_btn").click();
            }
        });
        
        $("#search_btn").click(function(){
        	var keyword = $("#searchInput").val();
        	if(keyword.trim()==''||keyword==''){
        		alert("请输入搜索关键字");
        		return;
        	}
            checkCookie($.trim($("#searchProductForm2 input[name='keyword']").val()));
            $("#searchProductForm2").submit();
        })

        $(window).scroll(function(){
            if($(this).scrollTop()<200){
                $('.header-box').css('background',"rgba(0,0,0,0)");
            }
            else{
                $('.header-box').css('background',"rgba(0,0,0,0.3)");
            }
        });
        
        $("#searchProductForm1 input[name='keyword']").focus(function(){
             $(".header-box").hide();
             $(".search").show();
             $(".search").addClass('complete_search');
	         $(".search_bg").css('backgroundColor','#dfdfdf');
	         $(".search_page").height($(window).height()-46);
	         $('.backto_page').click(function(){
	            $("#searchProductForm1 input[name='keyword']").blur();
	            $(".search").removeClass('complete_search');
	            $(".header-box").show();
                $(".search").hide();
	            $("#searchProductForm1 input[name='keyword']").val($.trim($("#searchProductForm2 input[name='keyword']").val()));
	        });
        });
        
   });
   
   function subForm(obj){
        $("#searchInput").val($(obj).attr("attr-value"));
        $("#searchProductForm").submit();
    }
    
     //获取cookie
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
        }
        return "";
    }
    
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }
	
	 //清除cookie
    function clearCookie(name) {
        setCookie(name, "", -1);
        getList();
    }
    
     function getList(){
        var title=getCookie("searchTitle");
        var list=spiltCookie(title);
        var str="";
        var count=0;
        for(var i =0;i<list.length;i++){
            if(count <9&&list[i]!=""){
                count++;
                str+='<li><a onclick="subForm(this)" attr-value="'+decodeURIComponent(list[i])+'" href="javascript:;">'+decodeURIComponent(list[i])+'</a></li>';
            }
        }
        $("#historySearch").html(str);
    }
    
    
    function checkCookie(obj) {
        var title=getCookie("searchTitle");
        var list=spiltCookie(title);
        var str="";
        var count=0;
        for(var i =0;i<list.length;i++){
            if(count<8){
                str+=list[i]+'==='
                if(list[i]==encodeURI(obj)) {
                    obj="";
                }
                count++;
            }

        }
        str=encodeURI(obj)+"==="+str;
        setCookie("searchTitle",str,1);
    }
    
    function spiltCookie(dValue){
        var strs= new Array(); //定义一数组
        strs=dValue.split("===");
        var list= new Array(); //定义一数组
        for(var i =strs.length-1;i>=0;i--){
            if(strs[i]!=""){
                list.push(strs[i]);
            }
        }
        return strs;
    }
	
</script>
</body>
</html>

