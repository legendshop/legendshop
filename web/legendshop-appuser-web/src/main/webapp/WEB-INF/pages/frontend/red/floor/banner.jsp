<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:if test="${not empty result.sliders}">
		    <div class="banner" id="Slide_Banner">
		     <div class="bd">
			     <ul class="img clear">
			      <c:forEach items="${result.sliders}" var="slider">
			         <li>
			           <c:choose>
			             <c:when test="${not empty slider.actionParam && not empty slider.actionParam.searchParam && fn:length(slider.actionParam.searchParam)>0 }">
			                   <c:choose>
			                      <c:when test="${not empty slider.actionParam.searchParam['brands']}">
			                         <a href="${contextPath}/m_search/list?prop=brandId:${slider.actionParam.searchParam['brands']['brandId']}"><img src="${photopath}${slider.img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty slider.actionParam.searchParam['cates']}">
			                         <a href="${contextPath}/m_search/list?categoryId=${slider.actionParam.searchParam['cates']['catId']}"><img src="${photopath}${slider.img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty slider.actionParam.searchParam['themes']}">
			                         <a href="${contextPath}/theme/themeDetail/${advert.actionParam.searchParam['themes']['themeId']}"><img src="${photopath}${slider.img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty slider.actionParam.searchParam['goods']}">
			                         <a href="${contextPath}/views/${slider.actionParam.searchParam['goods']['goodsInfoId']}"><img src="${photopath}${slider.img}" alt=""></a>
			                      </c:when>
			                  </c:choose> 
			            </c:when>
			            <c:otherwise>
			                  <a href="#"><img src="${photopath}${slider.img}" alt=""></a>
			            </c:otherwise>
			           </c:choose>
			          </li>
			        </c:forEach>
			     </ul>
			 </div>
			 <div class="hd">
			    <ul class="slide clear">
			    </ul>
			 </div>
		  </div>
		</c:if>