<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<c:if test="${not empty floor.adverts}">
	<div class="floor floor08">
		<%-- <p class="inder-tit">${floor.text}</p>--%>
		<%-- <div class="index-tit clear">
			 <span class="left">${floor.text}</span>
			<span class="right">查看更多</span>
		</div> --%>
		<%-- <div class="up-con under-line clear">
				  <c:choose>
			             <c:when test="${not empty floor.adverts[0].actionParam && not empty floor.adverts[0].actionParam.searchParam && fn:length(floor.adverts[0].actionParam.searchParam)>0 }">
			                   <c:choose>
			                      <c:when test="${not empty floor.adverts[0].actionParam.searchParam['brands']}">
			                         <a href="${contextPath}/m_search/list?prop=brandId:${floor.adverts[0].actionParam.searchParam['brands']['brandId']}"><img src="${photopath}${floor.adverts[0].img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty floor.adverts[0].actionParam.searchParam['cates']}">
			                         <a href="${contextPath}/m_search/list?categoryId=${floor.adverts[0].actionParam.searchParam['cates']['catId']}" ><img src="${photopath}${floor.adverts[0].img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty floor.adverts[0].actionParam.searchParam['goods']}">
			                         <a href="${contextPath}/views/${floor.adverts[0].actionParam.searchParam['goods']['goodsInfoId']}"  ><img src="${photopath}${floor.adverts[0].img}" alt=""  id="image1"></a>
			                      </c:when>
			            </c:choose> 
			       </c:when>
			       <c:otherwise>
			         <a href="javascript:void(0);" ><img src="${photopath}${floor.adverts[0].img}" alt=""></a>
			       </c:otherwise>
		          </c:choose>
		          
		           <c:choose>
			             <c:when test="${not empty floor.adverts[1].actionParam && not empty floor.adverts[1].actionParam.searchParam && fn:length(floor.adverts[1].actionParam.searchParam)>0 }">
			                   <c:choose>
			                      <c:when test="${not empty floor.adverts[1].actionParam.searchParam['brands']}">
			                         <a href="${contextPath}/m_search/list?prop=brandId:${floor.adverts[1].actionParam.searchParam['brands']['brandId']}"><img src="${photopath}${floor.adverts[1].img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty floor.adverts[1].actionParam.searchParam['cates']}">
			                         <a href="${contextPath}/m_search/list?categoryId=${floor.adverts[1].actionParam.searchParam['cates']['catId']}" ><img src="${photopath}${floor.adverts[1].img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty floor.adverts[1].actionParam.searchParam['goods']}">
			                         <a href="${contextPath}/views/${floor.adverts[1].actionParam.searchParam['goods']['goodsInfoId']}"  ><img src="${photopath}${floor.adverts[1].img}" alt=""  id="image1"></a>
			                      </c:when>
			                 </c:choose> 
			             </c:when>
			          <c:otherwise>
			              <a href="javascript:void(0);" ><img src="${photopath}${floor.adverts[1].img}" alt=""></a>
			           </c:otherwise>
		          </c:choose>
		          
		           <c:choose>
			             <c:when test="${not empty floor.adverts[2].actionParam && not empty floor.adverts[2].actionParam.searchParam && fn:length(floor.adverts[2].actionParam.searchParam)>0 }">
			                   <c:choose>
			                      <c:when test="${not empty floor.adverts[2].actionParam.searchParam['brands']}">
			                         <a href="${contextPath}/m_search/list?prop=brandId:${floor.adverts[2].actionParam.searchParam['brands']['brandId']}"><img src="${photopath}${floor.adverts[2].img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty floor.adverts[2].actionParam.searchParam['cates']}">
			                         <a href="${contextPath}/m_search/list?categoryId=${floor.adverts[2].actionParam.searchParam['cates']['catId']}" ><img src="${photopath}${floor.adverts[2].img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty floor.adverts[2].actionParam.searchParam['goods']}">
			                          <a href="${contextPath}/views/${advert[2].actionParam.searchParam['goods']['goodsInfoId']}">
															<span class="pro-img"><img src="${photopath}${advert[2].img}" alt=""></span>
															<span class="pro-name">${advert[2].text}</span>
															<span class="pro-price">¥${advert[2].price}</span>
													  </a>
			                      </c:when>
			            </c:choose> 
			       </c:when>
			       <c:otherwise>
			         <a href="javascript:void(0);" ><img src="${photopath}${floor.adverts[2].img}" alt=""></a>
			       </c:otherwise>
		          </c:choose>
		        </div> --%>

		<div class="down-con7 clear"  id="picScroll" style="overflow: hidden;position: relative;border-bottom:none;">
			<div class="hd" style="border-bottom:none;background:white;">
						<span class="next" style="display:none;"></span>
						<ul style="display:none;"></ul>
						<span class="prev" style="display:none;"></span>
						<span style="padding: 0; margin: 0; list-style: none; text-decoration: none; font-family: 微软雅黑; color: #333; box-sizing: border-box;">${floor.text}<span class="right" style="float:right;"><a href="${floor.lookMore}" style="color: #999;float:right;">查看更多</a></span> </span>
			</div>
			<div class="bd">
				<c:set var="length" value="${(fn:length(floor.adverts))}"></c:set>
				<c:set var="modCount" value="${(fn:length(floor.adverts) mod 3)}"></c:set>
				<c:set var="number" value="10"></c:set>
				<c:choose>
					<c:when test="${modCount != 0}">
						<fmt:formatNumber var="number" type="number"  value="${((length-modCount)/3)+1}"/>
					</c:when>
					<c:otherwise>
						<fmt:formatNumber var="number" type="number"  value="${(length-modCount)/3}"/>
					</c:otherwise>
				</c:choose>
				<ul style=" width:100%;  float:left; padding-top:10px; ">
				<c:forEach begin="1" end="${number}" varStatus="index">
					 
					 	<c:forEach items="${floor.adverts}" var="advert" varStatus="advertIndex" begin="${((index.index)*3)-3}" end="${(index.index)*3-1}">
					 		<c:choose>
						 		<c:when test="${not empty advert.actionParam.searchParam['goods']}">
										<li class="prod-li">
											<a href="${contextPath}/views/${advert.actionParam.searchParam['goods']['goodsInfoId']}">
												<span class="pro-img">
													<img id="proportion-img" src="${photopath}${advert.img}" alt="" height="120px;">
												</span>
												<%--<span class="pro-name">${advert.text}</span>--%>
												<p style="height: 30px; overflow: hidden;">${advert.text}</p>
												<%-- <span style="color: red;">¥${advert.price}</span> --%>
												
											</a>
											<p style="height: 30px;border-top: 1px solid #eee; margin-top: 5px; padding-top: 5px; color: #fd4062; font-size: 14px;">¥${advert.price}</p>
										</li>
								</c:when>
								<c:otherwise>
									<li style="width:30%; float:left; font-size:14px; text-align:center;margin: 8px 13px 0 0;">
										<a href="javascript:void(0);">
											<span class="pro-img">
												<img src="${photopath}${advert.img}" alt=""  height="120px;">
											</span>
											<span class="pro-name">${advert.text}</span>
											<%-- <span style="color: red;">¥${advert.price}</span> --%>
											<p style="border-top: 1px solid #eee; margin-top: 10px; padding-top: 10px; color: #fd4062; font-size: 14px;">¥${advert.price}</p>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
					 	</c:forEach>
					 
				</c:forEach>
			</ul>
					<%-- <c:forEach items="${floor.adverts}" var="advert" varStatus="advertIndex">
						<c:choose>
							<c:when test="${advertIndex.index ==0 || (advertIndex.index+1)%3==0}>
								 <ul style=" width:100%;  float:left; padding-top:10px; ">
							</c:when>
							
						
						</c:choose>
					
							<c:if test="${advertIndex.index ==0 || (advertIndex.index+1)%3==1}">
						     <ul style=" width:100%;  float:left; padding-top:10px; ">
						     </c:if>
							<c:choose>
								<c:when test="${not empty advert.actionParam && not empty advert.actionParam.searchParam && fn:length(advert.actionParam.searchParam)>0 }">
									<c:choose>
										<c:when test="${not empty advert.actionParam.searchParam['goods']}">
											<li style="width:33%; float:left; font-size:14px; text-align:center;  ">
												<a href="${contextPath}/views/${advert.actionParam.searchParam['goods']['goodsInfoId']}">
													<span class="pro-img">
														<img id="proportion-img" src="${photopath}${advert.img}" alt="">
													</span>
													<span class="pro-name">${advert.text}</span>
													<span class="pro-price">¥${advert.price}</span>
												</a>
											</li>
										</c:when>
									</c:choose>
								</c:when>
								<c:otherwise>
									<li style="width:33%; float:left; font-size:14px; text-align:center;  ">
										<a href="javascript:void(0);">
											<span class="pro-img">
												<img src="${photopath}${advert.img}" alt="">
											</span>
											<span class="pro-name">${advert.text}</span>
											<span class="pro-price">¥${advert.price}</span>
										</a>
									</li>
								</c:otherwise>
							</c:choose>
						<c:if test="${advertIndex.index ==0}">
							</ul>
						</c:if>
					</c:forEach> --%>
			</div>
		</div>
	</div>
</c:if>
<script charset="utf-8" src="${contextPath }/resources/common/js/TouchSlide.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		//修改图片高度随宽度改变
	/* 	$("#proportion-img").css("height", $("#proportion-img").css("width"));
		window.onresize = function() {
			$("#proportion-img").css("height", $("#proportion-img").css("width"));
		} */
		 
		TouchSlide({
			slideCell:"#picScroll",
			titCell:".hd ul", //开启自动分页 autoPage:true ，此时设置 titCell 为导航元素包裹层
			autoPage:true, //自动分页
			pnLoop:"false", // 前后按钮不循环
		});
	});
</script>