<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
  request.setAttribute("contextPath", request.getContextPath());
%>
<html lang="zh-cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>商城首页</title>
	<link rel="stylesheet" href="${contextPath }/resources/templets/red/css/appdecorate/common.css" />
	<link rel="stylesheet" href="${contextPath }/resources/templets/red/css/appdecorate/index.css" />

	<script charset="utf-8" src="${contextPath }/resources/common/js/jquery.min.js"></script>
	<script charset="utf-8" src="${contextPath }/resources/common/js/TouchSlide.js"></script>
</head>
<body>
   
    <!--数据 -->
   <div class="index w768">
		<!-- banner -->
	     <%@include file='floor/banner.jsp'%>
		<!-- /banner -->
		
		<!-- 快捷入口(头条菜单) -->
		<div class="index-nav clear">
			<c:forEach items="${headMenuList}" var="headmenu">
		    	<c:choose>
		    		<c:when test="${headmenu.url eq '#' }">
			        	<a href="javascript:void(0);"><img src="${photopath}${headmenu.pic}" alt=""><h4>${headmenu.name}</h4></a>
		    		</c:when>
			    	<c:otherwise>
		        		<a href="${mobileContextPath }${headmenu.url}"><img src="${photopath}${headmenu.pic}" alt=""><h4>${headmenu.name}</h4></a>
			    	</c:otherwise>
				</c:choose>
	        </c:forEach>
		</div>
      <!-- 快捷入口(头条菜单) -->
      
      
       <!-- 一级广告栏 -->
        <%@include file='floor/adverts.jsp'%>
       <!-- 一级广告栏 -->
		
		<!-- 楼层 -->
		 <c:if test="${not empty result.floors}">
		     <c:forEach  items="${result.floors}" var="floor"  varStatus="floorIndex">
			   <c:choose>
			      <c:when test="${floor.template eq 1}">
			           <%@include file='floor/template_1.jsp'%>
			      </c:when>
			      <c:when test="${floor.template eq 2}">
			           <%@include file='floor/template_2.jsp'%>
			      </c:when>
			      <c:when test="${floor.template eq 3}">
			           <%@include file='floor/template_3.jsp'%>
			      </c:when>
			      <c:when test="${floor.template eq 4}">
			           <%@include file='floor/template_4.jsp'%>
			      </c:when>
			       <c:when test="${floor.template eq 5}">
			           <%@include file='floor/template_5.jsp'%>
			      </c:when>
			       <c:when test="${floor.template eq 6}">
			           <%@include file='floor/template_6.jsp'%>
			      </c:when>
				   <c:when test="${floor.template eq 7}">
					   <%@include file='floor/template_7.jsp' %>
				   </c:when>
				   <c:when test="${floor.template eq 8}">
					   <%@include file='floor/template_8.jsp' %>
				   </c:when>
			   </c:choose>
		    </c:forEach>
		 </c:if>
	</div>

<script type="text/javascript">
 $(document).ready(function(){
    TouchSlide({
		slideCell:"#Slide_Banner",
		titCell:".hd ul", //开启自动分页 autoPage:true ，此时设置 titCell 为导航元素包裹层
		mainCell:".bd ul", 
		effect:"left",
		autoPlay:true,//自动播放
		autoPage:true, //自动分页
		switchLoad:"_src", //切换加载，真实图片路径为"_src"
		interTime:5000
	});
 	 resizeFloorImg();
 	 setInterval("resizeFloorImg()",500);
 	 
  });
	
   function resizeFloorImg(){
   
       	var box = $(".floor01").get();
        for(var i=0;i<box.length;i++){
		    var _box = $(box[i]).find("a img:first");
		    var height=$(_box).height(); 
			var width=$(_box).width(); 
		    $(box[i]).find("img:gt(0)").height(height);
			$(box[i]).find("img:gt(0)").width(width);
			if(height>0){
				$(_box).height(height);
			}
		}
    
	    var box = $(".floor05 .up-con").get();
		for(var i=0;i<box.length;i++){
		    var _box = $(box[i]).find(".left img");
		    var height=$(_box).height();
		    if(height>0){
		        $(box[i]).find("img:gt(0)").height((height-6)/2);
			} 
		}
		
		/* 楼层2 */
		var box = $(".floor06 .con").get();
		for(var i=0;i<box.length;i++){
		    var _box = $(box[i]).find(".left img");
		    if(height>0){
		        $(box[i]).find("img:gt(0)").height((height-6)/2);
			} 
		}
		
		var floor06_2 = $(".floor06 .con").find("img").eq(1);
		$(".floor06 .con").find("img:gt(1)").width(($(floor06_2).width()-6)/2);
		/* 楼层2 */
		
		
	}
	
</script>
</body>
</html>

