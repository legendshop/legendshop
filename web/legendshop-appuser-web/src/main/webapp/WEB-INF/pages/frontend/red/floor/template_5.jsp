<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<c:if test="${not empty floor.adverts}">
   <div class="floor floor03">
			   <%-- <p class="inder-tit">${floor.text}</p>--%>
			<div class="index-tit clear">
				<span class="left">${floor.text}</span>
				<span class="right" style="float:right;"><a href="${floor.lookMore}" style="color: #999;float:right;">查看更多</a></span>
			</div>
			   <div class="up-con under-line clear">
				  <c:choose>
			             <c:when test="${not empty floor.adverts[0].actionParam && not empty floor.adverts[0].actionParam.searchParam && fn:length(floor.adverts[0].actionParam.searchParam)>0 }">
			                   <c:choose>
			                      <c:when test="${not empty floor.adverts[0].actionParam.searchParam['brands']}">
			                         <a href="${contextPath}/m_search/list?prop=brandId:${floor.adverts[0].actionParam.searchParam['brands']['brandId']}"><img src="${photopath}${floor.adverts[0].img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty floor.adverts[0].actionParam.searchParam['cates']}">
			                         <a href="${contextPath}/m_search/list?categoryId=${floor.adverts[0].actionParam.searchParam['cates']['catId']}" ><img src="${photopath}${floor.adverts[0].img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty floor.adverts[0].actionParam.searchParam['goods']}">
			                         <a href="${contextPath}/views/${floor.adverts[0].actionParam.searchParam['goods']['goodsInfoId']}"  ><img src="${photopath}${floor.adverts[0].img}" alt=""  id="image1"></a>
			                      </c:when>
			                      <c:when test="${not empty floor.adverts[0].actionParam.searchParam['url']}">
			                         <a href="${floor.adverts[0].actionParam.searchParam['url']['url']}" class="left" ><img src="${photopath}${floor.adverts[0].img}" alt="" ></a>
			                      </c:when>
			            </c:choose> 
			       </c:when>
			       <c:otherwise>
			         <a href="javascript:void(0);" ><img src="${photopath}${floor.adverts[0].img}" alt=""></a>
			       </c:otherwise>
		          </c:choose>
		          
		           <c:choose>
			             <c:when test="${not empty floor.adverts[1].actionParam && not empty floor.adverts[1].actionParam.searchParam && fn:length(floor.adverts[1].actionParam.searchParam)>0 }">
			                   <c:choose>
			                      <c:when test="${not empty floor.adverts[1].actionParam.searchParam['brands']}">
			                         <a href="${contextPath}/m_search/list?prop=brandId:${floor.adverts[1].actionParam.searchParam['brands']['brandId']}"><img src="${photopath}${floor.adverts[1].img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty floor.adverts[1].actionParam.searchParam['cates']}">
			                         <a href="${contextPath}/m_search/list?categoryId=${floor.adverts[1].actionParam.searchParam['cates']['catId']}" ><img src="${photopath}${floor.adverts[1].img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty floor.adverts[1].actionParam.searchParam['goods']}">
			                         <a href="${contextPath}/views/${floor.adverts[1].actionParam.searchParam['goods']['goodsInfoId']}"  ><img src="${photopath}${floor.adverts[1].img}" alt=""  id="image1"></a>
			                      </c:when>
			                      <c:when test="${not empty floor.adverts[1].actionParam.searchParam['url']}">
			                         <a href="${floor.adverts[1].actionParam.searchParam['url']['url']}" ><img src="${photopath}${floor.adverts[1].img}" alt="" ></a>
			                      </c:when>
			                 </c:choose> 
			             </c:when>
			          <c:otherwise>
			              <a href="javascript:void(0);" ><img src="${photopath}${floor.adverts[1].img}" alt=""></a>
			           </c:otherwise>
		          </c:choose>
		          
		           <c:choose>
			             <c:when test="${not empty floor.adverts[2].actionParam && not empty floor.adverts[2].actionParam.searchParam && fn:length(floor.adverts[2].actionParam.searchParam)>0 }">
			                   <c:choose>
			                      <c:when test="${not empty floor.adverts[2].actionParam.searchParam['brands']}">
			                         <a href="${contextPath}/m_search/list?prop=brandId:${floor.adverts[2].actionParam.searchParam['brands']['brandId']}"><img src="${photopath}${floor.adverts[2].img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty floor.adverts[2].actionParam.searchParam['cates']}">
			                         <a href="${contextPath}/m_search/list?categoryId=${floor.adverts[2].actionParam.searchParam['cates']['catId']}" ><img src="${photopath}${floor.adverts[2].img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty floor.adverts[2].actionParam.searchParam['goods']}">
			                         <a href="${contextPath}/views/${floor.adverts[2].actionParam.searchParam['goods']['goodsInfoId']}"  ><img src="${photopath}${floor.adverts[2].img}" alt=""  id="image1"></a>
			                      </c:when>
			                       <c:when test="${not empty floor.adverts[2].actionParam.searchParam['url']}">
			                         <a href="${floor.adverts[2].actionParam.searchParam['url']['url']}" ><img src="${photopath}${floor.adverts[2].img}" alt=""></a>
			                      </c:when>
			            </c:choose> 
			       </c:when>
			       <c:otherwise>
			         <a href="javascript:void(0);" ><img src="${photopath}${floor.adverts[2].img}" alt=""></a>
			       </c:otherwise>
		          </c:choose>
		        </div>
		        
		        <c:if test="${fn:length(floor.adverts)>3}">  
		           <div class="down-con clear">
			           <c:forEach  items="${floor.adverts}" var="advert"  varStatus="advertIndex">
				           <c:if test="${advertIndex.index gt 2 }">  
				                  <c:choose>
							             <c:when test="${not empty advert.actionParam && not empty advert.actionParam.searchParam && fn:length(advert.actionParam.searchParam)>0 }">
							                   <c:choose>
							                      <c:when test="${not empty advert.actionParam.searchParam['brands']}">
							                             <a href="${contextPath}/m_search/list?prop=brandId:${advert.actionParam.searchParam['brands']['brandId']}">
															<span class="pro-img"><img src="${photopath}${advert.img}" alt=""></span>
															<span class="pro-name">${advert.text}</span>
															<span class="pro-price">¥${advert.price}</span>
														  </a>
							                      </c:when>
							                      <c:when test="${not empty advert.actionParam.searchParam['cates']}">
							                         <a href="${contextPath}/m_search/list?categoryId=${advert.actionParam.searchParam['cates']['catId']}">
															<span class="pro-img"><img src="${photopath}${advert.img}" alt=""></span>
															<span class="pro-name">${advert.text}</span>
															<span class="pro-price">¥${advert.price}</span>
													  </a>
							                      </c:when>
							                      <c:when test="${not empty advert.actionParam.searchParam['goods']}">
							                          <a href="${contextPath}/views/${advert.actionParam.searchParam['goods']['goodsInfoId']}">
															<span class="pro-img"><img src="${photopath}${advert.img}" alt=""></span>
															<span class="pro-name">${advert.text}</span>
															<span class="pro-price">¥${advert.price}</span>
													  </a>
							                      </c:when>
							            </c:choose> 
							       </c:when>
							       <c:otherwise>
							             <a href="javascript:void(0);">
											<span class="pro-img"><img src="${photopath}${advert.img}" alt=""></span>
											<span class="pro-name">${advert.text}</span>
											<span class="pro-price">¥${advert.price}</span>
										  </a>
							       </c:otherwise>
						          </c:choose>
				           </c:if>
			           </c:forEach>
		           </div>
		       </c:if>
   </div>
</c:if>
<c:if test="${not empty floor.banners}">
  <div class="floorslide" >
      <div class="down-con" id="Slide_Floor_${floorIndex.index}">
          <div class="bd">
              <ul class="img clear">
                 <c:forEach items="${floor.banners}" var="floor_banner">
                      <li>
			           <c:choose>
			             <c:when test="${not empty floor_banner.actionParam && not empty floor_banner.actionParam.searchParam && fn:length(floor_banner.actionParam.searchParam)>0 }">
			                   <c:choose>
			                      <c:when test="${not empty floor_banner.actionParam.searchParam['brands']}">
			                         <a href="${contextPath}/m_search/list?prop=brandId:${floor_banner.actionParam.searchParam['brands']['brandId']}"><img src="${photopath}${floor_banner.img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty floor_banner.actionParam.searchParam['cates']}">
			                         <a href="${contextPath}/m_search/list?categoryId=${floor_banner.actionParam.searchParam['cates']['catId']}"><img src="${photopath}${floor_banner.img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty floor_banner.actionParam.searchParam['goods']}">
			                         <a href="${contextPath}/views/${floor_banner.actionParam.searchParam['goods']['goodsInfoId']}"><img src="${photopath}${floor_banner.img}" alt=""></a>
			                      </c:when>
			                      <c:when test="${not empty floor_banner.actionParam.searchParam['url']}">
			                         <a href="${floor_banner.actionParam.searchParam['url']['url']}"><img src="${photopath}${floor_banner.img}" alt=""></a>
			                      </c:when>
			                  </c:choose> 
			            </c:when>
			            <c:otherwise>
			                  <a href="#"><img src="${photopath}${floor_banner.img}" alt=""></a>
			            </c:otherwise>
			           </c:choose>
			          </li>
                 </c:forEach>
              </ul>
           </div>
           <div class="hd">
			    <ul class="slide clear">
			    </ul>
		   </div>
     </div>
  </div> 
</c:if>
<script type="text/javascript">
   $(document).ready(function(){
     TouchSlide({
		slideCell:"#Slide_Floor_${floorIndex.index}",
		titCell:".hd ul", //开启自动分页 autoPage:true ，此时设置 titCell 为导航元素包裹层
		mainCell:".bd ul", 
		effect:"left",
		autoPlay:true,//自动播放
		autoPage:true, //自动分页
		switchLoad:"_src", //切换加载，真实图片路径为"_src"
		interTime:4000
	 });
   });
</script>