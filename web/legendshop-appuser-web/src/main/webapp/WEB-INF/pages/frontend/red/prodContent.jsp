<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,maximum-scale=1.3,user-scalable=yes">
<title>商品详情</title>
<c:if test="${not empty  screenWidth}">
	<style type="text/css">
	 	img{max-width:${screenWidth}px !important;}
	</style>
</c:if>
<body>
	<div style="text-align: center;word-break:break-all;">${requestScope.content}</div>
</body>
</html>