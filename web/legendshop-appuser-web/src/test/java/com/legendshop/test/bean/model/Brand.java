
package com.legendshop.test.bean.model;


import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;


/**
 * 产品品牌.
 */
@Entity
@Table(name = "ls_brand")
public class Brand implements GenericEntity<Long> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3941969699979401870L;

	/** The brand id. */
	private Long brandId;

	/** The brand name. */
	private String brandName;
	
	/** The status. */
	protected Integer status;

	/** The seq. */
	private Integer seq;

	/** The user id. */
	private String userId;

	/** The user name. */
	private String userName;

	/** The brand pic. */
	private String brandPic;
	
	/** The brand Authorize. */
	private String brandAuthorize;

	/** The memo. */
	private String memo;
	
	/** The sort id. */
	private Long sortId;

	private Integer commend;
	
	private Date applyTime;
	
	private ShopBrandDto shopBrandDto;
	
	/** 简要描述 **/
	private String brief;
	
	/** 详细内容 （故事）**/
	private String content;
	
	/** 品牌 大图 **/
	private String bigImage;
	//审核时间
	private Date checkTime;
	//审核用户名
	private String checkUserName;
	//审核意见
	private String checkOpinion;
	
	/** 商品数量 **/
	private Integer prodCounts;
	
	/** The file. */
	private MultipartFile file;
	
	/** The file2. */
	private MultipartFile bigImagefile;
	
	
	/** The brandAuthorizefile. */
	private MultipartFile brandAuthorizefile;
	/**
	 * Gets the file.
	 * 
	 * @return the file
	 */
	@Transient
	public MultipartFile getFile() {
		return file;
	}

	/**
	 * Sets the file.
	 * 
	 * @param file
	 *            the new file
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	
	/**
	 * Instantiates a new brand.
	 */
	public Brand() {
	}
	
	/**
	 * Instantiates a new brand.
	 *
	 * @param brandId the brand id
	 * @param brandName the brand name
	 */
	public Brand(Long brandId, String brandName) {
		this.brandId = brandId;
		this.brandName = brandName;
	}

	/**
	 * Gets the brand id.
	 * 
	 * @return the brand id
	 */
	@Id
	@Column(name = "brand_id")
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "generator")
	@TableGenerator(name = "generator", pkColumnValue = "BRAND_SEQ")
	public Long getBrandId() {
		return brandId;
	}

	/**
	 * Sets the brand id.
	 * 
	 * @param brandId
	 *            the new brand id
	 */
	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	/**
	 * Gets the brand name.
	 * 
	 * @return the brand name
	 */
	@Column(name = "brand_name")
	public String getBrandName() {
		return brandName;
	}

	/**
	 * Sets the brand name.
	 * 
	 * @param brandName
	 *            the new brand name
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	/**
	 * Gets the user id.
	 * 
	 * @return the user id
	 */
	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user id.
	 * 
	 * @param userId
	 *            the new user id
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * Gets the user name.
	 * 
	 * @return the user name
	 */
	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 * 
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the memo.
	 * 
	 * @return the memo
	 */
	@Column(name = "memo")
	public String getMemo() {
		return memo;
	}

	/**
	 * Sets the memo.
	 * 
	 * @param memo
	 *            the new memo
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}

	/**
	 * Gets the brand pic.
	 * 
	 * @return the brand pic
	 */
	@Column(name = "brand_pic")
	public String getBrandPic() {
		return brandPic;
	}

	/**
	 * Sets the brand pic.
	 * 
	 * @param brandPic
	 *            the new brand pic
	 */
	public void setBrandPic(String brandPic) {
		this.brandPic = brandPic;
	}
	
	
	@Column(name = "brand_authorize")
	public String getBrandAuthorize() {
		return brandAuthorize;
	}

	public void setBrandAuthorize(String brandAuthorize) {
		this.brandAuthorize = brandAuthorize;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.model.entity.BaseEntity#getId()
	 */
	@Transient
	public Long getId() {
		return brandId;
	}
	
	public void setId(Long id) {
		this.brandId = id;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the seq.
	 *
	 * @return the seq
	 */
	@Column(name = "seq")
	public Integer getSeq() {
		return seq;
	}

	/**
	 * Sets the seq.
	 *
	 * @param seq the new seq
	 */
	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	/**
	 * Gets the sort id.
	 *
	 * @return the sortId
	 */
	@Transient
	public Long getSortId() {
		return sortId;
	}

	/**
	 * Sets the sort id.
	 *
	 * @param sortId the sortId to set
	 */
	public void setSortId(Long sortId) {
		this.sortId = sortId;
	}

	@Column(name = "is_commend")
	public Integer getCommend() {
		return commend;
	}

	public void setCommend(Integer commend) {
		this.commend = commend;
	}
	
	
	@Column(name = "applyTime")
	public Date getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(Date applyTime) {
		this.applyTime = applyTime;
	}


	@Column(name = "brief")
	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	@Column(name = "content")
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column(name = "big_image")
	public String getBigImage() {
		return bigImage;
	}

	public void setBigImage(String bigImage) {
		this.bigImage = bigImage;
	}
	
	@Column(name = "checkTime")
	public Date getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(Date checkTime) {
		this.checkTime = checkTime;
	}
	@Column(name = "checkUserName")
	public String getCheckUserName() {
		return checkUserName;
	}

	public void setCheckUserName(String checkUserName) {
		this.checkUserName = checkUserName;
	}
	@Column(name = "checkOpinion")
	public String getCheckOpinion() {
		return checkOpinion;
	}

	public void setCheckOpinion(String checkOpinion) {
		this.checkOpinion = checkOpinion;
	}

	@Transient
	public Integer getProdCounts() {
		return prodCounts;
	}

	public void setProdCounts(Integer prodCounts) {
		this.prodCounts = prodCounts;
	}

	@Transient
	public MultipartFile getBigImagefile() {
		return bigImagefile;
	}

	public void setBigImagefile(MultipartFile bigImagefile) {
		this.bigImagefile = bigImagefile;
	}

	@Transient
	public MultipartFile getBrandAuthorizefile() {
		return brandAuthorizefile;
	}

	public void setBrandAuthorizefile(MultipartFile brandAuthorizefile) {
		this.brandAuthorizefile = brandAuthorizefile;
	}

	public ShopBrandDto getShopBrandDto() {
		return shopBrandDto;
	}

	public void setShopBrandDto(ShopBrandDto shopBrandDto) {
		this.shopBrandDto = shopBrandDto;
	}
	
}