
package com.legendshop.test.bean.model;

import java.io.Serializable;


/**
 * 产品品牌.
 */
public class BrandDto  implements Serializable {

	private static final long serialVersionUID = 7269408531800290979L;

	/** The brand id. */
	private Long brandId;

	/** The brand name. */
	private String brandName;
	
	private ShopBrandDto shopBrandDto;

	public Long getBrandId() {
		return brandId;
	}

	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public ShopBrandDto getShopBrandDto() {
		return shopBrandDto;
	}

	public void setShopBrandDto(ShopBrandDto shopBrandDto) {
		this.shopBrandDto = shopBrandDto;
	}
	
	

}
