package com.legendshop.test.bean;

import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.legendshop.dao.Utils;
import com.legendshop.test.bean.model.Brand;
import com.legendshop.test.bean.model.BrandDto;
import com.legendshop.test.bean.model.ShopBrandDto;

public class CopyBeanUtil {
	
    /** The Constant SUPPORTED_SQL_OBJECTS. */
    static final Set<Class<?>> SUPPORTED_SQL_OBJECTS = new HashSet<Class<?>>();

    static {
        Class<?>[] classes = {
                boolean.class, Boolean.class,
                short.class, Short.class,
                int.class, Integer.class,
                long.class, Long.class,
                float.class, Float.class,
                double.class, Double.class,
                String.class,
                Date.class,
                Timestamp.class,
                List.class,
                Set.class,
                Map.class,
                ArrayList.class,
                HashMap.class,
                HashSet.class,
                TreeSet.class,
                TreeMap.class,
                MultipartFile.class,
                CommonsMultipartFile.class
        };
        SUPPORTED_SQL_OBJECTS.addAll(Arrays.asList(classes));
    }
    

	public void printCode(Class<?> source, Class<?> target){

		String sourceName = source.getSimpleName();
		String targetName = target.getSimpleName();
		String sourceName1 = toLowCase(sourceName);
		String targetName1 = toLowCase(targetName);
		
		StringBuffer sb = new StringBuffer();
		sb.append("public ").append(targetName).append(" convert").append(targetName).append("(").append(sourceName).append(" ")
		.append(sourceName1).append("){").append("\n");
		printMethod(source, target, sb, 1);
		appendTab(sb,1);
		sb.append("return ").append(targetName1).append(";\n");
		sb.append("}");
		System.out.println(sb);
		
	}
	
	private void printMethod(Class<?> source, Class<?> target, StringBuffer sb, int tabNum){
		Map<String, Method> sourceGetters = Utils.findPublicGetters(source);
		Map<String, Method> targetSetters = Utils.findPublicSetters(target);
		
		String sourceName = source.getSimpleName();
		String targetName = target.getSimpleName();
		String sourceName1 = toLowCase(sourceName);
		String targetName1 = toLowCase(targetName);
		
		appendTab(sb, tabNum);
		sb.append(targetName).append(" ").append(targetName1).append(" = ").append("new ").append(targetName).append("();\n");
		for (String setter : targetSetters.keySet()) {
			Method targetSetter = targetSetters.get(setter);
			Method sourceGetter = sourceGetters.get(setter);
			if(sourceGetter != null && targetSetter != null){
				if(isBaseType(sourceGetter.getReturnType())){
						printMethod(targetName1, sourceName1, targetSetter, sourceGetter, sb, tabNum);
					}else{
						appendTab(sb, tabNum);
						sb.append("//子类调用，请检查是否正确\n");
						printMethod(targetName1, sourceName1, targetSetter, sourceGetter, sb, tabNum);
						//printMethod(sourceGetter.getReturnType(), sourceGetter.getReturnType(),sb, tabNum++);//递归调用子类
						}
		}

		}
	}
	
	private void printMethod(String targetName1, String sourceName1, Method targetSetter, Method sourceGetter, StringBuffer sb, int tabNum){
		appendTab(sb,tabNum);
		sb.append(targetName1).append(".").append(targetSetter.getName()).append("(").append(sourceName1).append(".").append(sourceGetter.getName()).append("());\n");
	
	}
	
	public BrandDto convertBean(Brand brand){
		BrandDto brandDto = new BrandDto();
		brandDto.setBrandName(brand.getBrandName());
		brandDto.setBrandId(brand.getBrandId());
		return brandDto;
	}
	
	public BrandDto convertBrandDto(Brand brand){
		BrandDto brandDto = new BrandDto();
		ShopBrandDto shopBrandDto = new ShopBrandDto();
		shopBrandDto.setBrandId(shopBrandDto.getBrandId());
		shopBrandDto.setBrandName(shopBrandDto.getBrandName());
		
			brandDto.setBrandId(brand.getBrandId());
			brandDto.setBrandName(brand.getBrandName());
			brandDto.setShopBrandDto(brand.getShopBrandDto());
		return brandDto;
	}

	
	private void appendTab(StringBuffer sb, int tabNum){
		for (int i = 0; i < tabNum; i++) {
			sb.append("	");
		}
	}
	
	private String toUpperCase(String sosurce){
		char[] cbuf = sosurce.toCharArray();
		cbuf[0] = Character.toUpperCase(cbuf[0]);
		return new String(cbuf);
	}
	
	private String toLowCase(String sosurce){
		char[] cbuf = sosurce.toCharArray();
		cbuf[0] = Character.toLowerCase(cbuf[0]);
		return new String(cbuf);
	}
	
    /**
     * Checks if is supported sql object.
     *
     * @param clazz the clazz
     * @return true, if is supported sql object
     */
    public  static boolean isBaseType(Class<?> clazz) {
        return clazz.isEnum() || SUPPORTED_SQL_OBJECTS.contains(clazz);
    }

}
