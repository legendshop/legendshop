/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.activity.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.legendshop.model.dto.SkuSearchDto;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.Sku;
import com.legendshop.spi.service.*;
import com.legendshop.util.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;

import com.legendshop.activity.page.ActivityFrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.MarketingTypeEnum;
import com.legendshop.model.dto.marketing.MarketingProdDto;
import com.legendshop.model.entity.Marketing;
import com.legendshop.model.entity.MarketingProds;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;

/**
 * 促销活动控制器
 * 
 */
@Controller
public class ShopMjMarketingController extends BaseController {

	@Autowired
	private MarketingService marketingService;

	@Autowired
	private MarketingProdsService marketingProdsService;

	@Autowired
	private ProductService productService;

	@Autowired
	private MarketingRuleService marketingRuleService;

	@Autowired
	private SkuService skuService;

	/**
	 * 格式化时间
	 * 
	 */
	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		CustomDateEditor editor = new CustomDateEditor(df, false);
		binder.registerCustomEditor(Date.class, editor);
	}

	/**
	 * 单品满折促销
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param marketing
	 * @return
	 */
	@RequestMapping("/s/shopSingleMarketing")
	@Authorize(authorityTypes = AuthorityType.SINGLE_MARKETING)
	public String shopSingleMarketing(HttpServletRequest request, HttpServletResponse response, String curPageNO, Marketing marketing) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		marketing.setIsAllProds(0);
		marketing.setShopId(shopId);
		marketing.setType(0);
		PageSupport<Marketing> pageSupport = assembleMarketTing(curPageNO, marketing);
		PageSupportHelper.savePage(request, pageSupport);
		request.setAttribute("marketing", marketing);
		return PathResolver.getPath(ActivityFrontPage.ACTIVITY_SINGLE_MARKETING);
	}

	/**
	 * 包邮促销
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param marketing
	 * @return
	 */
	@RequestMapping("/s/fullmail")
	@Authorize(authorityTypes = AuthorityType.FULL_MAIL)
	public String fullmail(HttpServletRequest request, HttpServletResponse response, String curPageNO, Marketing marketing) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		marketing.setShopId(shopId);
		marketing.setType(MarketingTypeEnum.FULL_NUM_MAIL.value());
		PageSupport<Marketing> pageSupport = assemblefullmail(curPageNO, marketing);
		PageSupportHelper.savePage(request, pageSupport);
		request.setAttribute("marketing", marketing);
		return PathResolver.getPath(ActivityFrontPage.ACTIVITY_SINGLE_MARKETING);
	}

	/**
	 * 满折促销
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param marketing
	 * @return
	 */
	@RequestMapping("/s/shopFullCourtMarketing")
	@Authorize(authorityTypes = AuthorityType.FULL_COURT_MARKETING)
	public String shopFullCourtMarketing(HttpServletRequest request, HttpServletResponse response, String curPageNO, Marketing marketing) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		marketing.setIsAllProds(1);
		marketing.setShopId(shopId);
		PageSupport<Marketing> pageSupport = assembleMarketTing(curPageNO, marketing);
		PageSupportHelper.savePage(request, pageSupport);
		request.setAttribute("marketing", marketing);
		return PathResolver.getPath(ActivityFrontPage.ACTIVITY_FULLCOURT_MARKETING);
	}

	private PageSupport<Marketing> assembleMarketTing(String curPageNO, Marketing marketing) {
		PageSupport<Marketing> pageSupport = marketingService.getMarketingPage(curPageNO, marketing);
		return pageSupport;
	}

	private PageSupport<Marketing> assemblefullmail(String curPageNO, Marketing marketing) {
		PageSupport<Marketing> pageSupport = marketingService.getAssemblefullmail(curPageNO, marketing);
		return pageSupport;
	}

	/**
	 * 促销活动管理
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/s/shopMarketingManage/{id}")
	@Authorize(authorityTypes = {AuthorityType.SINGLE_MARKETING,AuthorityType.FULL_COURT_MARKETING})
	public String shopMarketingManage(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Marketing marketing = marketingService.getMarketingDetail(shopId, id);
		if (AppUtils.isBlank(marketing)) {
			request.setAttribute("messgae", "找不到该活动！");
			return PathResolver.getPath(ActivityFrontPage.FAIL);
		}
		request.setAttribute("marketing", marketing);
		return PathResolver.getPath(ActivityFrontPage.ADD_SHOPMARKETING_MANSONG);
	}

	/**
	 * 参与营销活动的商品
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/s/marketingRuleProds/{id}")
	@Authorize(authorityTypes = {AuthorityType.SINGLE_MARKETING,AuthorityType.FULL_COURT_MARKETING})
	public String marketingRuleProds(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, String curPage,Integer state) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<MarketingProdDto> pageSupport = marketingProdsService.findMarketingRuleProds(curPage, shopId, id);
		PageSupportHelper.savePage(request, pageSupport);
		request.setAttribute("state",state);
		return PathResolver.getPath("/marketingRuleProds", ActivityFrontPage.VARIABLE);
	}

	/**
	 * 参与营销活动的商品
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/s/marketingProds/{id}")
	@Authorize(authorityTypes = {AuthorityType.SINGLE_MARKETING,AuthorityType.FULL_COURT_MARKETING})
	public String marketingProds(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, String name, String curPage, String type) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<MarketingProdDto> pageSupport = marketingProdsService.findMarketingProds(curPage, shopId, name);
		PageSupportHelper.savePage(request, pageSupport);
		request.setAttribute("type", type);
		request.setAttribute("marketId", id);
		return PathResolver.getPath("/goodsSearchResult", ActivityFrontPage.VARIABLE);
	}

	/**
	 * 添加促销活动的商品
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param curPage
	 * @return
	 */
	@RequestMapping(value = "/s/addMarketingRuleProds/{id}", method = RequestMethod.POST)
	@Authorize(authorityTypes = {AuthorityType.SINGLE_MARKETING,AuthorityType.FULL_COURT_MARKETING})
	public @ResponseBody String addMarketingRuleProds(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, Long prodId, Long skuId,
			Double cash) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Marketing marketing = marketingService.getMarketing(shopId, id);
		if (AppUtils.isBlank(marketing)) {
			return "找不到该活动！";
		} /*
			 * else if(marketing.getState().intValue()==1){ return
			 * "该活动还在上线，请下线再管理！"; }
			 */
		if (new Date().after(marketing.getEndTime())) { // 在 当前时间之前
			return "该活动已过期！";
		}
		
		//不给重复添加同一商品
		List<MarketingProds> marketingProds = marketingProdsService.getMarketingProdsByMarketId(id);
		for (MarketingProds marketingProd : marketingProds) {
			if(marketingProd.getSkuId().equals(skuId)){
				return "该活动已存在此商品，请勿重复添加！";
			}
		}
		
		//判断商品sku是否在参加其他活动 TODO
		String data =  marketingProdsService.iSHaveMarketing(prodId,skuId);
		if (!data.equals(Constants.SUCCESS)) {
			return data;
		}
		
		String result = marketingProdsService.addMarketingRuleProds(id, marketing.getType(), shopId, prodId, skuId, cash);
		if (Constants.SUCCESS.equals(result) && marketing.getState().intValue() == 1) {
			marketingProdsService.clearProdMarketCache(shopId, prodId);
		}
		return result;
	}

	/**
	 * 删除促销活动的商品
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param marketingId
	 * @return
	 */
	@RequestMapping("/s/deleteMarketingRuleProds/{marketingId}/{id}")
	@Authorize(authorityTypes = {AuthorityType.SINGLE_MARKETING,AuthorityType.FULL_COURT_MARKETING})
	public @ResponseBody String deleteMarketingRuleProds(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id,
			@PathVariable Long marketingId) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		MarketingProds prods = marketingProdsService.getMarketingProds(id);
		if (prods == null) {
			return "找不到该信息";
		} else if (!prods.getShopId().equals(shopId)) {
			return "该信息没有权限删除！";
		}
		marketingProdsService.deleteMarketingProds(marketingId, shopId,prods);
		/*Long count = marketingService.isExistProds(marketingId, shopId); // 是否添加了商品信息
		if (count == 0) { // 该活动移除所有的商品信息后,会自动下线
			Marketing marketing = marketingService.getMarketing(marketingId);
			if (marketing.getState().intValue() == 1) {
				marketing.setState(0);
				marketingService.updateMarketing(marketing);
			}
		}*/
		marketingProdsService.clearProdMarketCache(shopId, prods.getProdId());
		return Constants.SUCCESS;
	}

	/**
	 * 删除促销活动
	 * 
	 * @param request
	 * @param response
	 * @param type
	 * @param id
	 * @return
	 */
	@RequestMapping("/s/deleteShopMarketing/{type}/{id}")
	@Authorize(authorityTypes = {AuthorityType.SINGLE_MARKETING,AuthorityType.FULL_COURT_MARKETING,AuthorityType.ZK_MARKETING})
	public String deleteShopMarketing(HttpServletRequest request, HttpServletResponse response, @PathVariable int type, @PathVariable Long id) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Marketing marketing = marketingService.getMarketing(shopId, id);
		if (AppUtils.isBlank(marketing)) {
			request.setAttribute("messgae", "找不到该活动！");
			return PathResolver.getPath(ActivityFrontPage.FAIL);
		}
		Date now = new Date();
		if (marketing.getState().intValue() == 1 && marketing.getEndTime().getTime() >= now.getTime()) {
			request.setAttribute("messgae", "该活动在进行,不能删除");
			return PathResolver.getPath(ActivityFrontPage.FAIL);
		}
		marketingService.deleteMarketing(marketing);

		if (marketing.getIsAllProds().intValue() == 1) { // 针对于全场商品
			marketingProdsService.clearGlobalMarketCache(shopId);
		} else {
			marketingProdsService.clearProdMarketCache();
		}
		if (type == 1 || type == 0) {
			return PathResolver.getRedirectPath("/s/shopSingleMarketing");
		} else if (type == 2) {
			return PathResolver.getRedirectPath("/s/shopZkMarketing");
		} else if (type == MarketingTypeEnum.FULL_NUM_MAIL.value() || type == MarketingTypeEnum.FULL_AMOUNT_MAIL.value()) {
			return PathResolver.getRedirectPath("/s/fullmail");
		}
		return null;
	}

	/**
	 * 下线满送促销活动
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/s/offlineShopMarketing/{type}/{id}")
	@Authorize(authorityTypes = {AuthorityType.SINGLE_MARKETING,AuthorityType.FULL_COURT_MARKETING,AuthorityType.ZK_MARKETING})
	public String offlineShopMarketing(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable int type) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Marketing marketing = marketingService.getMarketing(shopId, id);
		if (AppUtils.isBlank(marketing)) {
			request.setAttribute("messgae", "找不到该活动！");
			return PathResolver.getPath(ActivityFrontPage.FAIL);
		}
		if (marketing.getState().intValue() == 1) {
			marketing.setState(3);
			marketingService.updateMarketing(marketing);
		}

		if (marketing.getIsAllProds().intValue() == 1) { // 针对于全场商品
			marketingProdsService.clearGlobalMarketCache(shopId);
		} else {
			marketingProdsService.clearProdMarketCache();
		}
		if (type == 0 || type == 1) {
			return PathResolver.getRedirectPath("/s/shopSingleMarketing");
		} else if (type == 2) {
			return PathResolver.getRedirectPath("/s/shopZkMarketing");
		} else if (type == MarketingTypeEnum.FULL_NUM_MAIL.value() || type == MarketingTypeEnum.FULL_AMOUNT_MAIL.value()) {
			return PathResolver.getRedirectPath("/s/fullmail");
		}
		return null;
	}

	/**
	 * 添加促销活动
	 * 
	 * @param request
	 * @param response
	 * @param isAllProds
	 * @return
	 */
	@RequestMapping(value = "/s/addShopMarketing/{isAllProds}", method = RequestMethod.GET)
	@Authorize(authorityTypes = {AuthorityType.SINGLE_MARKETING,AuthorityType.FULL_COURT_MARKETING})
	public String addShopMarketing(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer isAllProds) {
		if (isAllProds != 0 && isAllProds != 1) {
			request.setAttribute("messgae", "没有该活动类型！");
			return PathResolver.getPath(ActivityFrontPage.FAIL);
		}
		request.setAttribute("isAllProd", isAllProds);
		return PathResolver.getPath(ActivityFrontPage.ADD_SHOPMARKETING_MANSONG);
	}

	/**
	 * 加载添加促销商品列表
	 * @param request
	 * @param response
	 * @param activeId
	 * @param curPageNO
	 * @param prodName
	 * @return
	 */
	@RequestMapping(value = "/s/addShopMarketingProds", method = RequestMethod.GET)
	public String addShopMarketingProds(HttpServletRequest request, HttpServletResponse response,Long activeId,String curPageNO,String prodName) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		//查看商品
		PageSupport<Product> ps = productService.getProdductByShopId(curPageNO, prodName,shopId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prodName", prodName);
		return PathResolver.getPath(ActivityFrontPage.SHOP_ADDMANSONG_PROD);
	}

	/**
	 *根据ProdId加载Sku集合
	 * @param request
	 * @param prodId 商品id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/s/loadSkuList")
	public Map<String,List<Sku>> loadSkuList(Long prodId) {
		//查看商品
		Map<String,List<Sku>> map = new HashMap<>();
		List<Sku> sku = skuService.getSkuByProd(prodId);
		if (AppUtils.isNotBlank(sku)) {
			// 移除下架SKU
			sku.removeIf(s -> s.getStatus() == 0);
		}
		map.put("skuList", sku);
		return map;
	}

	/**
	 * 查询sku集合
	 * @param skus
	 * @return
	 */
	@RequestMapping(value = "/s/searchSku")
	public String searchSku(HttpServletRequest request,String skuJson) {
		Map<String, JSONArray> skuMap = JSONUtil.getMap(skuJson);
		List<Sku> skuList = new ArrayList<>();
		for (Map.Entry<String, JSONArray> sku :skuMap.entrySet()) {
			List<Long> skuIds = new ArrayList<>();
			JSONArray jsonSkuIds = sku.getValue();
			Iterator<Object> iterator = jsonSkuIds.iterator();
			while (iterator.hasNext()){
				Long skuId = Long.parseLong(String.valueOf(iterator.next()));
				skuIds.add(skuId);
			}
			List<Sku> skus = skuService.getSku(skuIds.toArray(new Long[skuIds.size()]));
			skuList.addAll(skus);
		}
		//查看商品
		request.setAttribute("skuList", skuList);
		return PathResolver.getPath(ActivityFrontPage.SHOP_SHOW_MANSONG_SKULIST);
	}

	/**
	 * 添加促销活动
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/s/shopMarketing/{marketingId}", method = RequestMethod.GET)
	public String showShopMarketing(HttpServletRequest request, HttpServletResponse response, @PathVariable Long marketingId) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Marketing marketing = marketingService.getMarketingDetail(shopId, marketingId);
		request.setAttribute("marketing", marketing);
		return PathResolver.getPath(ActivityFrontPage.SHOP_SHOW_PROMOTION);
	}
	/**
	 * 添加包邮活动
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/s/addShopfullmail/add", method = RequestMethod.GET)
	@Authorize(authorityTypes = AuthorityType.FULL_MAIL)
	public String addShopFullmail(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(ActivityFrontPage.ADD_SHOPMARKETING_FULLMAIL);
	}

	/**
	 * 上线满送促销活动
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/s/onlineShopMarketing/{type}/{id}")
	@Authorize(authorityTypes = {AuthorityType.SINGLE_MARKETING,AuthorityType.FULL_COURT_MARKETING,AuthorityType.ZK_MARKETING})
	public String onlineShopMarketing(HttpServletRequest request, HttpServletResponse response, @PathVariable int type, @PathVariable Long id) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Marketing marketing = marketingService.getMarketing(shopId, id);
		if (AppUtils.isBlank(marketing)) {
			request.setAttribute("messgae", "找不到该活动！");
			return PathResolver.getPath(ActivityFrontPage.FAIL);
		}
		if (new Date().after(marketing.getEndTime())) { // 在 当前时间之前
			request.setAttribute("messgae", "该活动已过期！");
			return PathResolver.getPath(ActivityFrontPage.FAIL);
		}
		//同一时间段只能发布一个全场满折活动
		if(marketing.getIsAllProds()==1){
			int count = marketingService.getAllProdsMarketing(shopId, marketing.getType(), marketing.getStartTime(), marketing.getEndTime());
			if(count != 0){
				request.setAttribute("messgae", "已有其他全场促销活动上线，请先下线其他全场促销活动！");
				return PathResolver.getPath(ActivityFrontPage.FAIL);
			}
		}


		if (marketing.getIsAllProds().intValue() == 0) { // 部分商品
			Long count = marketingService.isExistProds(marketing.getId(), marketing.getShopId()); // 是否添加了商品信息
			if (count == 0) {
				request.setAttribute("messgae", "请设置活动商品！");
				return PathResolver.getPath(ActivityFrontPage.FAIL);
			}
		}
		if (marketing.getState().intValue() == 0 || marketing.getState().intValue() == 2) {
			marketing.setState(1);
			marketingService.updateMarketing(marketing);
		}

		if (marketing.getIsAllProds().intValue() == 1) { // 针对于全场商品
			marketingProdsService.clearGlobalMarketCache(shopId);
		} else {
			marketingProdsService.clearProdMarketCache();
		}

		if (type == 1 || type == 0) {
			return PathResolver.getRedirectPath("/s/shopSingleMarketing");
		} else if (type == 2) {
			return PathResolver.getRedirectPath("/s/shopZkMarketing");
		} else if (type == MarketingTypeEnum.FULL_NUM_MAIL.value() || type == MarketingTypeEnum.FULL_AMOUNT_MAIL.value()) {
			return PathResolver.getRedirectPath("/s/fullmail");
		}
		return null;
	}

	/**
	 * 添加促销活
	 * 
	 * @param request
	 * @param response
	 * @param marketing
	 * @return
	 */
	@RequestMapping(value = "/s/addShopMarketing", method = RequestMethod.POST)
	@Authorize(authorityTypes = {AuthorityType.SINGLE_MARKETING,AuthorityType.FULL_COURT_MARKETING})
	public @ResponseBody Map<String, Object> addShopMarketing(HttpServletRequest request, HttpServletResponse response, Marketing marketing) {
		Map<String, Object> map = new HashMap<>();
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		String userId = user.getUserId();
		boolean config = validateMarketing(marketing);
		if (!config) {
			map.put("status", Constants.FAIL);
			map.put("msg", "填写的数据格式有问题, 请检查");
			return map;
		}
		if(marketing.getIsAllProds().equals(1)){
			List<Marketing> oldMarketings = marketingService.findOngoingMarketing(shopId, marketing.getIsAllProds(), marketing.getStartTime(), marketing.getEndTime());
			if(AppUtils.isNotBlank(oldMarketings)){
				map.put("status", Constants.FAIL);
				map.put("msg", "该时间段内已存在促销活动,请重新选择时间");
				return map;
			}
		}
		if (marketing.getId()!=null){
			Marketing marke = marketingService.getMarketing(marketing.getId());
			marke.setMarketName(marketing.getMarketName());
			marke.setStartTime(marketing.getStartTime());
			marke.setEndTime(marketing.getEndTime());
			marke.setIsAllProds(marketing.getIsAllProds());
			marke.setType(marketing.getType());
			marke.setRemark(marketing.getRemark());
			marketingService.updateMarketing(marke);
			if (AppUtils.isNotBlank(marketing.getManjian_rule())||AppUtils.isNotBlank(marketing.getManze_rule())){
				marketing.setShopId(shopId);
				marketing.setUserId(userId);
				marketingRuleService.deleteMarketingRuleByMarketing(marketing.getId());
				marketingService.saveMarketingRule(marketing);
			}
			marketingProdsService.deleteByMarketId(marketing.getId());
			map.put("msg", "活动修改成功");
		}else {
		marketing.setShopId(shopId);
		marketing.setUserId(userId);
		String result = marketingService.saveMarketing(marketing);
		map.put("msg", "活动添加成功");
		}
		if (AppUtils.isNotBlank(marketing.getSkuIds())){
			for (Long skuId : marketing.getSkuIds()) {
				Sku sku = skuService.getSku(skuId);
				marketingProdsService.addMarketingRuleProds(marketing.getId(),marketing.getType(),shopId,sku.getProdId(),sku.getSkuId(),sku.getPrice());
			}
		}
		map.put("status", Constants.SUCCESS);
		map.put("activId", marketing.getId());
		return map;
	}

	/**
	 * 验证促销活动
	 * 
	 * @param marketing
	 * @return
	 */
	private boolean validateMarketing(Marketing marketing) {
		if (AppUtils.isBlank(marketing.getMarketName())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getStartTime())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getEndTime())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getIsAllProds())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getType())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getManjian_rule()) && AppUtils.isBlank(marketing.getManze_rule())) {
			return false;
		}
		return true;
	}

	/**
	 * 验证包邮促销
	 * 
	 * @param marketing
	 * @return
	 */
	private boolean validateMarketingFull(Marketing marketing) {
		if (AppUtils.isBlank(marketing.getMarketName())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getStartTime())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getEndTime())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getIsAllProds())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getType())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getManjian_fullmail_rule()) && AppUtils.isBlank(marketing.getManyuan_fullmail_rule())) {
			return false;
		}
		return true;
	}

	/**
	 * 保存包邮促销
	 * 
	 * @param request
	 * @param response
	 * @param marketing
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/s/saveFullMail")
	@Authorize(authorityTypes = AuthorityType.FULL_MAIL)
	public String saveFullMail(HttpServletRequest request, HttpServletResponse response, Marketing marketing) {
		if (MarketingTypeEnum.FULL_NUM_MAIL.value().equals(marketing.getType())) {
			if (AppUtils.isBlank(marketing.getManjian_fullmail_rule())) {
				return "请添加满件包邮规则";
			}
		} else if (MarketingTypeEnum.FULL_AMOUNT_MAIL.value().equals(marketing.getType())) {
			if (AppUtils.isBlank(marketing.getManyuan_fullmail_rule())) {
				return "请添加满元包邮规则";
			}
		}
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		String userId = user.getUserId();
		
		boolean config = validateMarketingFull(marketing);
		if (!config) {
			return "填写的数据格式有问题！";
		}
		marketing.setShopId(shopId);
		marketing.setUserId(userId);
		String result = marketingService.saveMarketingByFullmail(marketing);
		return result;
	}


	/**
	 * 暂停满送促销活动
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/s/suspendShopMarketing/{type}/{id}")
	@Authorize(authorityTypes = {AuthorityType.SINGLE_MARKETING,AuthorityType.FULL_COURT_MARKETING,AuthorityType.ZK_MARKETING})
	public String suspend(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id,@PathVariable Long type) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		Marketing marketing = marketingService.getMarketing(shopId, id);
		if (AppUtils.isBlank(marketing)) {
			request.setAttribute("messgae", "找不到该活动！");
			return PathResolver.getPath(ActivityFrontPage.FAIL);
		}
		if (marketing.getState().intValue() == 1) {
			marketing.setState(2);
			marketingService.updateMarketing(marketing);
		}
		if (marketing.getIsAllProds().intValue() == 1) { // 针对于全场商品
			marketingProdsService.clearGlobalMarketCache(shopId);
		} else {
			marketingProdsService.clearProdMarketCache();
		}


		if (type==2){
			return PathResolver.getRedirectPath("/s/shopZkMarketing");
		}
		return PathResolver.getRedirectPath("/s/shopSingleMarketing");
	}
}
