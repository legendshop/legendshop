/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.activity.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.Sku;
import com.legendshop.spi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.activity.page.ActivityFrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.MarketingTypeEnum;
import com.legendshop.model.dto.marketing.MarketingProdDto;
import com.legendshop.model.entity.Marketing;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;

/**
 * 折扣促销
 * 
 */
@Controller
public class ShopZkMarketingController extends BaseController {

	@Autowired
	private MarketingService marketingService;

	@Autowired
	private ProductService productService;

	@Autowired
	private MarketingProdsService marketingProdsService;

	@Autowired
	private SkuService skuService;

	@Autowired
	private MarketingRuleService marketingRuleService;

	/**
	 * 格式化时间
	 * 
	 */
	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		CustomDateEditor editor = new CustomDateEditor(df, false);
		binder.registerCustomEditor(Date.class, editor);
	}

	/**
	 * 折扣促销
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param marketing
	 * @return
	 */
	@RequestMapping("/s/shopZkMarketing")
	@Authorize(authorityTypes = AuthorityType.ZK_MARKETING)
	public String shopZkMarketing(HttpServletRequest request, HttpServletResponse response, String curPageNO, Marketing marketing) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<Marketing> ps = marketingService.getMarketingPage(curPageNO, shopId, marketing);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("marketing", marketing);
		return PathResolver.getPath(ActivityFrontPage.ACTIVE_PROMOTION_MANGZHE);
	}

	/**
	 * 添加折扣活动
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/s/addShopZkMarketing", method = RequestMethod.GET)
	public String addShopMarketing(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(ActivityFrontPage.ADD_SHOPMARKETING_MANGZHE);
	}

	/**
	 * 添加促销活动
	 * 
	 * @param request
	 * @param response
	 * @param marketing
	 * @return
	 */
	@RequestMapping(value = "/s/addShopZkMarketing", method = RequestMethod.POST)
	public @ResponseBody String addShopZkMarketing(HttpServletRequest request, HttpServletResponse response, Marketing marketing) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		String userId = user.getUserId();
		String rs="";
		boolean config = validateMarketing(marketing);
		if (!config) {
			return "填写的数据格式有问题！";
		}
		if(marketing.getIsAllProds().equals(1)){
			List<Marketing> oldMarketings = marketingService.findOngoingMarketing(shopId, marketing.getIsAllProds(), marketing.getStartTime(), marketing.getEndTime());
			if(AppUtils.isNotBlank(oldMarketings)){
				return "已有全场促销活动在运行中，请下线后再重新创建！";
			}
		}
		if (AppUtils.isNotBlank(marketing.getId())){
			Marketing marke = marketingService.getMarketing(marketing.getId());
			marke.setMarketName(marketing.getMarketName());
			marke.setStartTime(marketing.getStartTime());
			marke.setEndTime(marketing.getEndTime());
			marke.setIsAllProds(marketing.getIsAllProds());
			marke.setRemark(marketing.getRemark());
			marke.setDiscount(marketing.getDiscount());
			marke.setType(MarketingTypeEnum.XIANSHI_ZE.value());
			marketingService.updateMarketing(marke);
			if (AppUtils.isNotBlank(marketing.getDiscount())){
				marketing.setShopId(shopId);
				marketing.setUserId(userId);
				marketing.setType(MarketingTypeEnum.XIANSHI_ZE.value());
				marketingRuleService.deleteMarketingRuleByMarketing(marketing.getId());
				marketingService.saveMarketingRule(marketing);
			}
			marketingProdsService.deleteByMarketId(marketing.getId());
			rs="修改成功";
		}else {
			marketing.setShopId(shopId);
			marketing.setUserId(userId);
			marketing.setType(MarketingTypeEnum.XIANSHI_ZE.value());
			marketingService.saveMarketing(marketing);
			rs="添加成功";
		}

		if (AppUtils.isNotBlank(marketing.getSkuIds())){
			for (Long skuId : marketing.getSkuIds()) {
				Sku sku = skuService.getSku(skuId);
				marketingProdsService.addMarketingRuleProds(marketing.getId(),2,shopId,sku.getProdId(),sku.getSkuId(),sku.getPrice());
			}
		}
		marketingService.cleanMarketCache();
		return rs;
	}

	/**
	 * 折扣活动管理
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/s/shopZkMarketingManage/{id}")
	public String shopMarketingManage(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Marketing marketing = marketingService.getMarketingDetail(shopId, id);
		if (AppUtils.isBlank(marketing)) {
			request.setAttribute("messgae", "找不到该活动！");
			return PathResolver.getPath(ActivityFrontPage.FAIL);
		} else if (!MarketingTypeEnum.XIANSHI_ZE.value().equals(marketing.getType())) {
			request.setAttribute("messgae", "该活动不是限时折扣活动！");
			return PathResolver.getPath(ActivityFrontPage.FAIL);
		}
		request.setAttribute("marketing", marketing);
		return PathResolver.getPath(ActivityFrontPage.ADD_SHOPMARKETING_MANGZHE);
	}
	
	/**
	 * 查看折扣活动（全场折扣）
	 */
	@RequestMapping("/s/shopZkMarketingDetail/{id}")
	public String shopZkMarketingDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Marketing marketing = marketingService.getMarketingDetail(shopId, id);
		if (AppUtils.isBlank(marketing)) {
			request.setAttribute("messgae", "找不到该活动！");
			return PathResolver.getPath(ActivityFrontPage.FAIL);
		} else if (!MarketingTypeEnum.XIANSHI_ZE.value().equals(marketing.getType())) {
			request.setAttribute("messgae", "该活动不是限时折扣活动！");
			return PathResolver.getPath(ActivityFrontPage.FAIL);
		}
		request.setAttribute("marketing", marketing);
		return PathResolver.getPath(ActivityFrontPage.ACTIVE_ZK_DETAIL);
	}
	
	/**
	 * 参与营销活动的商品
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/s/marketingZkRuleProds/{id}")
	public String marketingRuleProds(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, String curPage,Integer state) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<MarketingProdDto> pageSupport = marketingProdsService.findMarketingRuleProds(curPage, shopId, id);
		PageSupportHelper.savePage(request, pageSupport);
		request.setAttribute("state", state);
		return PathResolver.getPath("/marketingZkRuleProds", ActivityFrontPage.VARIABLE);
	}

	/**
	 * 添加限时折扣促销活动的商品
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param curPage
	 * @return
	 */
	@RequestMapping("/s/addZkMarketingRuleProds/{id}")
	public @ResponseBody String addZkMarketingRuleProds(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, Long prodId,
			Long skuId, Double cash, Double discountPrice) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Marketing marketing = marketingService.getMarketing(shopId, id);
		if (AppUtils.isBlank(marketing)) {
			return "找不到该活动！";
		} else if (marketing.getState().intValue() == 1) {
			return "该活动还在上线，请下线再管理！";
		}
		if (new Date().after(marketing.getEndTime())) { // 在 当前时间之前
			return "该活动已过期";
		}
		
		//判断商品sku是否在参加其他活动 TODO
		String data =  marketingProdsService.iSHaveMarketing(prodId,skuId);
		if (!data.equals(Constants.SUCCESS)) {
			return data;
		}
		
		String result = marketingProdsService.addZkMarketingRuleProds(id, shopId, prodId, skuId, cash, discountPrice);
		if (Constants.SUCCESS.equals(result)) {
			if (marketing.getIsAllProds().intValue() == 1) { // 针对于全场商品
				marketingProdsService.clearGlobalMarketCache(shopId);
			} else {
				marketingProdsService.clearProdMarketCache(shopId, prodId);
			}
		}
		return result;
	}

	/**
	 * 验证活动
	 * 
	 * @param marketing
	 * @return
	 */
	private boolean validateMarketing(Marketing marketing) {
		if (AppUtils.isBlank(marketing.getMarketName())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getStartTime())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getEndTime())) {
			return false;
		}
		if (AppUtils.isBlank(marketing.getIsAllProds())) {
			return false;
		}
		if (marketing.getIsAllProds().intValue() == 1) {
			if (AppUtils.isBlank(marketing.getDiscount())) {
				return false;
			} else if (marketing.getDiscount() < 1 || marketing.getDiscount() > 9.9) {
				return false;
			}
		}
		return true;
	}
	
}
