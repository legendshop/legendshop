/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.activity.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 活动的前端页面定义.
 */
public enum ActivityFrontPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 单品满折促销 */
	ACTIVITY_SINGLE_MARKETING("/shopSingleDiscountList"),

	/** 全场商品折扣活动 */
	ACTIVITY_FULLCOURT_MARKETING("/shopFullDiscountList"),

	/**错误页面 */
	FAIL("/activityFail"),

	/** 添加满送活动 */
	ADD_SHOPMARKETING_MANSONG("/shopAddMansong"),

	SHOP_SHOW_MANSONG("/shopShowMansong"),

	SHOP_SHOW_PROMOTION("/shopShowPromotion"),

	SHOP_SHOW_MANSONG_SKULIST("/shopMansongSkuList"),

	/** 添加包邮活动 */
	ADD_SHOPMARKETING_FULLMAIL("/shopAddFullMail"),

	/** 促销活动管理 */
	ACTIVE_PROMOTION_MANAGE("/shopMarketingManage"),

	/** 限时折扣活动列表 */
	ACTIVE_PROMOTION_MANGZHE("/shopZkMarketingList"),

	/** 添加折扣活动 */
	ADD_SHOPMARKETING_MANGZHE("/shopAddZkMarketing"),

	/** 限时折扣活动管理 */
	ACTIVE_PROMOTION_ZK("/shopZkMarketingManage"),

//	/***查看显示折扣(全场)**/
//	ACTIVE_ZK_DETAIL("/shopZKMarketingDetail"),

	/***查看显示折扣(全场)**/
	ACTIVE_ZK_DETAIL("/shopShowZkMarketing"),

	/**加载促销活动商品列表**/
	SHOP_ADDMANSONG_PROD("/shopAddMansongProd"),

	/** 操作错误 */
	OPERATION_ERROR("/operationError"),

	/**包邮活动**/
	SHIPPING_ACTIVE_LIST("/shipping/shippingActiveList"),

	/**添加店铺包邮活动*/
	ADD_SHOPSHIPPING_PAGE("/shipping/addShopShipping"),

	/** 添加商品包邮 */
	ADD_PROD_SHIPPING("/shipping/addProdShipping"),

	FULL_PROD("/shipping/addFullProd"),

	FULL_EDIT_PROD_DOWN("/shipping/editProdFull"),

	FULL_EDIT_PROD("/shipping/editFullProd");


	/** The value. */
	private final String value;

	private ActivityFrontPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath("activity", path);
	}

	public String getNativeValue() {
		return value;
	}

}
