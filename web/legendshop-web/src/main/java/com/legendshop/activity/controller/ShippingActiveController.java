/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.activity.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.activity.page.ActivityFrontPage;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ShippingTypeEnum;
import com.legendshop.model.entity.ShippingActive;
import com.legendshop.model.entity.ShippingProd;
import com.legendshop.model.prod.ActiveDto;
import com.legendshop.model.prod.EditActiveNameDto;
import com.legendshop.model.prod.FullActiveProdDto;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.ShippingActiveService;
import com.legendshop.spi.service.ShippingProdService;
import com.legendshop.util.AppUtils;

/**
 * 包邮活动控制器
 */
@Controller
@RequestMapping("/s/shippingActive")
public class ShippingActiveController {

	@Autowired
	private ShippingActiveService shippingActiveService;

	@Autowired
	private ShippingProdService shippingProdService;

	@Autowired
	private ProductService productService;

	/**
	 * 包邮活动列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/query")
	@Authorize(authorityTypes = AuthorityType.SHIPPING_MANGE)
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, ShippingActive shippingActive) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<ShippingActive> ps = shippingActiveService.queryShippingActiveList(curPageNO, shopId, shippingActive);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("name", shippingActive.getName());
		request.setAttribute("date", new Date());
		return PathResolver.getPath(ActivityFrontPage.SHIPPING_ACTIVE_LIST);
	}


	/*打开添加店铺包邮页面*/
	@RequestMapping(value = "/addShopShipping/{activeId}")
	@Authorize(authorityTypes = AuthorityType.SHIPPING_MANGE)
	public String addShopShipping(HttpServletRequest request, HttpServletResponse response, @PathVariable Long activeId) {
		ShippingActive shippingActive = shippingActiveService.getShippingActive(activeId);
		request.setAttribute("shippingActive", shippingActive);
		return PathResolver.getPath(ActivityFrontPage.ADD_SHOPSHIPPING_PAGE);
	}

	/*保存店铺包邮*/
	@RequestMapping(value = "/saveShopShipping", method = RequestMethod.POST)
	@ResponseBody
	@Authorize(authorityTypes = AuthorityType.SHIPPING_MANGE)
	public Map<String, Object> saveShopShipping(HttpServletRequest request, HttpServletResponse response, ShippingActive shippingActive) {
		Map<String, Object> map = new HashMap<>();
		
		if(AppUtils.isBlank(shippingActive)) {
			map.put("status", Constants.FAIL);
			map.put("msg", "保存失败，包邮活动为空");
			return map;
		}

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		if(AppUtils.isNotBlank(shippingActive.getId())) { //编辑
			ShippingActive oldShippingActive = shippingActiveService.getShippingActiveById(shippingActive.getId(), shopId);
			oldShippingActive.setName(shippingActive.getName());
			shippingActiveService.updateShippingActive(oldShippingActive);
		}else {//新增

			Date now = new Date();
			if(shippingActive.getEndDate().getTime() <= now.getTime()){
				map.put("status", Constants.FAIL);
				map.put("msg", "活动结束时间不能小于当前时间");
				return map;
			}

			shippingActive.setShopId(shopId);
			shippingActive.setCreateDate(new Date());
			shippingActive.setStatus(true);
			shippingActive.setFullType(ShippingTypeEnum.SHOP.value());
			shippingActive.setSource("PC");
			shippingActiveService.saveShippingActive(shippingActive);
		}
		map.put("status", Constants.SUCCESS);
		map.put("msg", "保存成功");
		return map;
	}

	/*打开添加商品包邮页面*/
	@RequestMapping(value = "/addProdShipping")
	@Authorize(authorityTypes = AuthorityType.SHIPPING_MANGE)
	public String addProdFull(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(ActivityFrontPage.ADD_PROD_SHIPPING);
	}

	/*商品满减送页面的商品*/
	@RequestMapping(value = "/fullProd")
	@Authorize(authorityTypes = AuthorityType.SHIPPING_MANGE)
	public String fullProd(HttpServletRequest request, HttpServletResponse response,Long activeId,String curPageNo,String prodName) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		//商品满减送活动查看商品
		PageSupport<FullActiveProdDto> ps = productService.getFullActiveProdDto(shopId,curPageNo,prodName);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prodName", prodName);
		return PathResolver.getPath(ActivityFrontPage.FULL_PROD);
	}


	@RequestMapping(value = "/saveProdShopPing",method=RequestMethod.POST)
	@ResponseBody
	@Authorize(authorityTypes = AuthorityType.SHIPPING_MANGE)
	public String saveProdShopPing(HttpServletRequest request, HttpServletResponse response,@RequestBody ActiveDto activeDto) {
		boolean checkFormat = getCheckFormat(activeDto);

		if(!checkFormat){
			return "参数错误，请刷新重试";
		}

		if(AppUtils.isBlank(activeDto.getProdIds()) || activeDto.getProdIds().length==0){
			return "活动商品不能为空";
		}

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		ShippingActive active = new ShippingActive();
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		Date startDate = null;
		Date endDate = null;
		try {
			startDate = dateFormat.parse(activeDto.getStartDate());
			endDate = dateFormat.parse(activeDto.getEndDate());
		} catch (ParseException e) {
			e.printStackTrace();
			return "日期选择错误";
		}

		Date now = new Date();
		if(endDate.getTime()<=now.getTime()){
			return "活动结束时间不能小于当前时间";
		}

		active.setShopId(shopId);
		active.setName(activeDto.getActiveName());
		active.setStartDate(startDate);
		active.setEndDate(endDate);
		active.setStatus(true);
		active.setFullType(ShippingTypeEnum.PROD.value());
		active.setFullValue(activeDto.getFullValue());
		active.setReduceType(activeDto.getReduceType());
		active.setDescription(activeDto.getDescription());
		active.setCreateDate(now);
		active.setSource("PC");
		shippingActiveService.saveActiveAndProd(active,activeDto.getProdIds());
		return Constants.SUCCESS;
	}


	private boolean getCheckFormat(ActiveDto activeDto) {
		if(AppUtils.isBlank(activeDto)){
			return false;
		}
		if(AppUtils.isBlank(activeDto.getActiveName()) || AppUtils.isBlank(activeDto.getStartDate()) || AppUtils.isBlank(activeDto.getEndDate())){
			return false;
		}
		return true;
	}

	//商品满减送编辑
	@RequestMapping(value = "/editProdShipping/{id}")
	@Authorize(authorityTypes = AuthorityType.SHIPPING_MANGE)
	public String editProdFull(HttpServletRequest request, HttpServletResponse response,@PathVariable Long id) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		ShippingActive active = shippingActiveService.getShippingActive(id);
		if(AppUtils.isBlank(active) || !shopId.equals(active.getShopId())){
			request.setAttribute(Constants.MESSAGE, "对不起,该活动不存在，请返回重试");
			return PathResolver.getPath(ActivityFrontPage.OPERATION_ERROR);
		}

		//已选择的商品
		List<FullActiveProdDto> activeProdList = shippingProdService.getActiveProdByActiveId(id,shopId);

		request.setAttribute("active", active);
		request.setAttribute("activeProdList", activeProdList);
		request.setAttribute("isProd", activeProdList.size());
		request.setAttribute("now",new Date());

		return PathResolver.getPath(ActivityFrontPage.FULL_EDIT_PROD_DOWN);
	}


	/*取消参加的测试方法*/
	@RequestMapping(value = "/cancelProd")
	@Authorize(authorityTypes = AuthorityType.SHIPPING_MANGE)
	public String cancelProd(HttpServletRequest request, HttpServletResponse response,Long activeId,Long prodId){
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		ShippingActive active = shippingActiveService.getShippingActive(activeId);
		List<ShippingProd> shippingProdList = shippingProdService.getProdByActiveId(active.getId(), shopId);
		if(shippingProdList != null && shippingProdList.size() > 0){
			for(ShippingProd shippingProd : shippingProdList){
				if(shippingProd.getProdId().equals(prodId)){
					shippingProdService.deleteShippingProd(shippingProd);
				}
			}
		}

		return "redirect:/s/shippingActive/editProdShipping/"+activeId;
	}


	/*商品满减送页面的商品*/
	@RequestMapping(value = "/editfullProd/{id}")
	@Authorize(authorityTypes = AuthorityType.SHIPPING_MANGE)
	public String editfullProd(HttpServletRequest request, HttpServletResponse response,Long activeId,String curPageNo,String prodName,@PathVariable Long id,String check) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		if(AppUtils.isNotBlank(check) && check.equals("check") ){
			request.setAttribute("check", check);
		}

		//已选择的商品
		List<ShippingProd> list = shippingProdService.getProdByActiveId(id,shopId);

		//商品满减送活动查看商品
		PageSupport<FullActiveProdDto> ps = productService.getFullActiveProdDto(shopId,curPageNo,prodName);
		List<FullActiveProdDto> temp = ps.getResultList();

		for(FullActiveProdDto fullActiveProdDto:temp){
			for(ShippingProd activeProd:list){
				if(fullActiveProdDto.getProdId().equals(activeProd.getProdId())){
					fullActiveProdDto.setHook(1);//已选择
				}
			}
		}
		ps.setResultList(temp);
		PageSupportHelper.savePage(request, ps);
        ShippingActive active = shippingActiveService.getShippingActive(id);
		request.setAttribute("prodName", prodName);
		request.setAttribute("editActiveId", id);
		request.setAttribute("now",new Date());
		request.setAttribute("startTime",active.getStartDate());

		request.setAttribute("isProd", list.size());

		return PathResolver.getPath(ActivityFrontPage.FULL_EDIT_PROD);
	}


	/*编辑提交*/
	@RequestMapping(value = "/editCommitProdFull/{id}",method=RequestMethod.POST)
	@ResponseBody
	@Authorize(authorityTypes = AuthorityType.SHIPPING_MANGE)
	public String editCommitProdFull(HttpServletRequest request, HttpServletResponse response,@PathVariable Long id,@RequestBody EditActiveNameDto editActiveNameDto) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		ShippingActive active = shippingActiveService.getShippingActive(id);
		if(AppUtils.isBlank(active) || !shopId.equals(active.getShopId())){
			request.setAttribute(Constants.MESSAGE, "对不起,该活动不存在，请返回重试");
			return PathResolver.getPath(ActivityFrontPage.OPERATION_ERROR);
		}

		if(AppUtils.isBlank(active)){
			return "操作失败，该活动不存在或已被删除";
		}

		if(AppUtils.isBlank(editActiveNameDto.getActiveName())){
			return "活动名称不能为空";
		}

		if(editActiveNameDto.getActiveName().length()<2 || editActiveNameDto.getActiveName().length()>20){
			return "活动名称限制2-20个字";
		}
		active.setName(editActiveNameDto.getActiveName());
		shippingActiveService.updateShippingActiveByProd(active,editActiveNameDto.getProdIds());

		return Constants.SUCCESS;
	}


	/*撤销包邮活动*/
	@RequestMapping(value = "/cancelShipping/{activeId}", method = RequestMethod.POST)
	@ResponseBody
	@Authorize(authorityTypes = AuthorityType.SHIPPING_MANGE)
	public Map<String, Object> cancelShipping(HttpServletRequest request, HttpServletResponse response, @PathVariable Long activeId) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Map<String, Object> map = new HashMap<>();
		ShippingActive shippingActive = shippingActiveService.getShippingActiveById(activeId, shopId);
		if(AppUtils.isBlank(shippingActive)) {
			map.put("status", Constants.FAIL);
			map.put("msg", "撤销失败，包邮活动为空");
			return map;
		}
		shippingActive.setStatus(false);
		shippingActiveService.updateShippingActive(shippingActive);
		map.put("status", Constants.SUCCESS);
		map.put("msg", "保存成功");
		return map;
	}

	/*删除包邮活动*/
	@RequestMapping(value = "/delShipping/{activeId}", method = RequestMethod.POST)
	@ResponseBody
	@Authorize(authorityTypes = AuthorityType.SHIPPING_MANGE)
	public Map<String, Object> delShipping(HttpServletRequest request, HttpServletResponse response, @PathVariable Long activeId) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Map<String, Object> map = new HashMap<>();
		ShippingActive shippingActive = shippingActiveService.getShippingActiveById(activeId, shopId);
		if(AppUtils.isBlank(shippingActive)) {
			map.put("status", Constants.FAIL);
			map.put("msg", "撤销失败，包邮活动为空");
			return map;
		}
		shippingActiveService.deleteShippingActive(shippingActive);
		map.put("status", Constants.SUCCESS);
		map.put("msg", "删除成功");
		return map;
	}

}
