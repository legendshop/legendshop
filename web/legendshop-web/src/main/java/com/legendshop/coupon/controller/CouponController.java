/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.coupon.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.coupon.page.CouponFrontPage;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.CouponGetTypeEnum;
import com.legendshop.model.constant.CouponProviderEnum;
import com.legendshop.model.constant.CouponTypeEnum;
import com.legendshop.model.constant.SendCouponTypeEnum;
import com.legendshop.model.constant.UserCouponEnum;
import com.legendshop.model.dto.buy.PersonalCouponDto;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.CouponProdService;
import com.legendshop.spi.service.CouponService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.UserCouponService;
import com.legendshop.util.AppUtils;
import com.legendshop.web.util.ValidateCodeUtil;

/**
 * 我的礼券控制器.
 */
@Controller
public class CouponController extends BaseController {
	
	private final Logger logger = LoggerFactory.getLogger(CouponController.class);

	@Autowired
	private UserCouponService userCouponService;

	@Autowired
	private CouponService couponService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private CouponProdService couponProdService;
	
	@Autowired
	private ProductService productService;

	/**
	 * 领券中心.
	 *
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/couponCenter")
	public String integralCenter(HttpServletRequest request, HttpServletResponse response) {
		Date nowDate = new Date();
		// 优惠券
		List<Coupon> couponList = this.couponService.getCouponByProvider(CouponGetTypeEnum.FREE.value(), CouponProviderEnum.SHOP.value(), nowDate);
		// 红包
		List<Coupon> redpackList = this.couponService.getCouponByProvider(CouponGetTypeEnum.FREE.value(), CouponProviderEnum.PLATFORM.value(), nowDate);
		request.setAttribute("couponList", couponList);
		request.setAttribute("redpackList", redpackList);
		return PathResolver.getPath(CouponFrontPage.COUPON_CENTER);
	}

	/**
	 * 领券中心 ：更多 红包或者优惠券.
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param type
	 * @return the string
	 */
	@RequestMapping(value = "/couponCenter/couponMore/{type}")
	public String integralCenterList(HttpServletRequest request, HttpServletResponse response, String curPageNO, @PathVariable String type) {
		PageSupport<Coupon> ps = this.couponService.getCouponsPage(curPageNO, type);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("type", type);
		return PathResolver.getPath(CouponFrontPage.COUPON_MORE);
	}

	/**
	 * 全部优惠券 包括可使用 已使用 已过期.
	 *
	 * @param useStatus
	 *            使用状态：1 未使用 2 已使用 3过期
	 * @param couPro
	 *            礼券提供方，区分优惠券(shop)和红包(platform)
	 * @param curPageNO
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/p/coupon")
	public String queryCoupon(Integer useStatus, String couPro, String curPageNO, HttpServletRequest request, HttpServletResponse response) {
		this.logger.debug("<----------start--------CouponController#queryCoupon------------");
		this.logger.debug("params: useStatus = {} , couPro = {} , curPageNO = {}", useStatus, couPro, curPageNO);
		int sizePage = 10;
		if (AppUtils.isBlank(curPageNO)) {
			curPageNO = "1";
		}
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		PageSupport<PersonalCouponDto> ps = couponService.queryPersonalCouponDto(userId, sizePage, curPageNO, useStatus, couPro);
		PageSupportHelper.savePage(request, ps);
		// 返回选中的使用状态
		request.setAttribute("useStatus", useStatus);
		request.setAttribute("couPro", couPro);
		if ("shop".equals(couPro)) {
			request.setAttribute("couponName", "优惠券");
		} else {
			request.setAttribute("couponName", "红包");
		}
		this.logger.debug("-----------end--------CouponController#queryCoupon----------->");
		String path = PathResolver.getPath(CouponFrontPage.COUPON_INDEX_PAGE);
		return path;
	}

	/**
	 * 进入领取礼券页面.
	 *
	 * @param couPro
	 * @param request
	 * @param response
	 * @return the coupon
	 */
	@RequestMapping(value = "/p/getCoupon")
	public String getCoupon(String couPro, HttpServletRequest request, HttpServletResponse response) {
		if ("shop".equals(couPro)) {
			request.setAttribute("couponName", "优惠券");
		} else {
			request.setAttribute("couponName", "红包");
		}
		request.setAttribute("couPro", couPro);
		String path = PathResolver.getPath(CouponFrontPage.GET_COUPON_PAGE);
		return path;
	}

	/**
	 * 激活优惠券.
	 *
	 * @param couPro
	 * @param verifyCode
	 *            验证码
	 * @param couponPwd
	 *            卡密
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/p/activateCoupon", method = RequestMethod.POST)
	@ResponseBody
	public String activateCoupon(String couPro, String verifyCode, String couponPwd, HttpServletRequest request, HttpServletResponse response) {
		this.logger.debug("<----------start--------CouponController#activateCoupon------------");
		this.logger.debug("params: couPro = {} , verifyCode = {} , couponPwd = {}", couPro, verifyCode, couponPwd);
		// 验证码验证
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		
		if (AppUtils.isBlank(verifyCode) || !ValidateCodeUtil.validateCode(verifyCode, request.getSession())) {
			request.setAttribute("messgae", "请输入正确的图形验证码");
			this.logger.debug("----------end--------CouponController#activateCoupon------------>");
			return Constants.ERROR_CODE;
		}
		// 判断激活码是否为空
		if (AppUtils.isBlank(couponPwd)) {
			request.setAttribute("message", "请输入激活码...");
			this.logger.debug("----------end--------CouponController#activateCoupon------------>");
			return Constants.ACTIVATE_CODE_ERROR;
		} else {
			// 输入激活码，查出coupon，判断是否存在和正确
			UserCoupon userCoupon = userCouponService.getCouponByCouponPwd(couponPwd);
			if (userCoupon == null) {
				request.setAttribute("message", "请输入正确激活码...");
				this.logger.debug("----------end--------CouponController#activateCoupon------------>");
				return Constants.ACTIVATE_CODE_ERROR;
			} else if (userCoupon.getUserId() == null) {
				long couponId = userCoupon.getCouponId();
				Coupon coupon = couponService.getCoupon(couponId);
				// 判断输入的礼券激活码是否是对应的礼券，比如红包激活码不能激活优惠券激活码
				if (!(couPro.equals(coupon.getCouponProvider()))) {
					this.logger.debug("----------end--------CouponController#activateCoupon------------>");
					return Constants.ACTIVATE_CODE_ERROR;
				}
				// 判断礼券是否已经下线
				int status = coupon.getStatus();
				if (status == 0) {
					request.setAttribute("message", "礼券已失效...");
					this.logger.debug("----------end--------CouponController#activateCoupon------------>");
					return "offline";
				}
				// 查询用户已经领取了几张优惠券
				int number = userCouponService.getUserCouponCount(userId, couponId);
				// 判断是否已经达到领取上限
				long limit = coupon.getGetLimit();
				if (number >= limit) {
					request.setAttribute("message", "领取优惠券已达到上限...");
					this.logger.debug("----------end--------CouponController#activateCoupon------------>");
					return Constants.LIMIT;
				} else {
					// 判断时候已经超过活动结束时间
					Date endDate = coupon.getEndDate();
					Date nowDate = new Date();
					if (nowDate.getTime() > endDate.getTime()) {
						request.setAttribute("message", "活动已经结束...");
						this.logger.debug("----------end--------CouponController#activateCoupon------------>");
						return Constants.ACTIVITY_END;
					} else {
						// 初始化优惠券数量
						long initNumber = coupon.getCouponNumber();
						// 已绑定优惠券数量
						long bindNumber = coupon.getBindCouponNumber();
						if (bindNumber >= initNumber) {
							request.setAttribute("message", "优惠券已领完...");
							this.logger.debug("----------end--------CouponController#activateCoupon------------>");
							return "nothing";
						} else {
							// 可以正常激活
							couponService.activateCoupon(userId, userName, coupon, userCoupon);
							this.logger.debug("----------end--------CouponController#activateCoupon------------>");
							return Constants.SUCCESS;
						}
					}
				}
			} else {
				this.logger.debug("----------end--------CouponController#activateCoupon------------>");
				// 已有人领取
				return "owner";
			}
		}
	}

	/**
	 * 优惠码 兑换 优惠券.
	 *
	 * @param request
	 * @param response
	 * @param couponSn
	 *            优惠码
	 * @param curPageNO
	 * @return the string
	 */
	@RequestMapping(value = "/p/checkConpus")
	public String checkConpus(HttpServletRequest request, HttpServletResponse response, String couponSn, String curPageNO) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		if (couponSn == null || "".equals(couponSn)) {
			this.logger.info("couponSn is null");
			request.setAttribute("message", "请输入优惠码。");
		}
		UserCoupon userCoupon = userCouponService.getCouponByCouponSn(couponSn);
		Date date = new Date();
		if (userCoupon == null) {
			this.logger.info("userCoupons is null");
			request.setAttribute("message", "对不起，这张优惠券不能兑换。");
		} else {
			if (userCoupon.getUseStatus() == UserCouponEnum.USED.value()) {
				this.logger.info("this userCoupons has been used");
				request.setAttribute("message", "对不起，这张优惠券不能兑换。");
			} else {
				if (userCoupon.getUserName() == null) {
					if (userCoupon.getCouponId() == null) {
						this.logger.info("this userCoupons has not  CpnsId");
						request.setAttribute("message", "对不起，这张优惠券不能兑换。");
					} else {
						Coupon c = couponService.getCoupon(userCoupon.getCouponId());
						if (c == null || c.getSendType() == null || c.getSendType() != SendCouponTypeEnum.OFFLINE.value()
								|| c.getStatus() == Constants.OFFLINE.intValue()) {
							request.setAttribute("message", "对不起，这张优惠券不能兑换。");
						} else if (date.getTime() > c.getEndDate().getTime()) {
							request.setAttribute("message", "对不起，这张优惠券已经过期。");
						} else {
							// 正常绑定 优惠券到用户
							com.legendshop.security.model.SecurityUserDetail userDetail = UserManager.getUser(request);
							userCouponService.updateUsercouponProperty(userCoupon, userDetail.getUserId(), userDetail.getUsername(), c);
							request.setAttribute("message", "恭喜您，绑定成功。。");
						}
					}
				} else {
					this.logger.info("this userCoupons has been bind");
					request.setAttribute("message", "对不起，这张优惠券已经被绑定。");
				}
			}
		}

		PageSupport<Coupon> ps = couponService.queryCouponPage(curPageNO, userName);

		request.setAttribute("useStatus", UserCouponEnum.UNUSED.value());
		request.setAttribute("coupons", ps.getResultList());
		PageSupportHelper.savePage(request, ps);
		String path = PathResolver.getPath(CouponFrontPage.COUPON_INDEX_PAGE);
		return path;
	}

	/**
	 * 礼券领取
	 *
	 * @param request
	 * @param response
	 * @param couponId
	 * @return the string
	 */
	@RequestMapping(value = "/p/coupon/apply/{couponId}")
	public String applyCoupon(HttpServletRequest request, HttpServletResponse response, @PathVariable Long couponId) {
		if (couponId != null) {
			Coupon coupon = couponService.getCoupon(couponId);
			if (coupon != null) {
				if (AppUtils.isNotBlank(coupon.getShopId())) {
					ShopDetail shopDetail = shopDetailService.getShopDetailById(coupon.getShopId());
					coupon.setShopName(shopDetail.getSiteName());
				}
				request.setAttribute("coupon", coupon);
			}
		}
		return PathResolver.getPath(CouponFrontPage.APPLY_COUPON);
	}

	/**
	 * 申请优惠卷.
	 *
	 * @param request
	 * @param response
	 * @param couponId
	 * @return the map< string, string>
	 */
	@RequestMapping(value = "/obtainCoupon/{couponId}")
	@ResponseBody
	public Map<String, String> obtainCoupon(HttpServletRequest request, HttpServletResponse response, @PathVariable Long couponId) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		Map<String, String> resultMap = new HashMap<String, String>();
		resultMap.put("code", "fail");
		if (AppUtils.isBlank(userName)) {
			resultMap.put("msg", "请先登录");
			return resultMap;
		} else {
			Date date = new Date();
			Coupon coupon = couponService.getCoupon(couponId);
			if (coupon == null || coupon.getStatus() == Constants.OFFLINE.intValue()) {
				resultMap.put("msg", "当前券已失效，快寻找新目标吧~");
				return resultMap;
			} else if (date.getTime() > coupon.getEndDate().getTime()) {
				resultMap.put("msg", "当前券已失效，快寻找新目标吧~");
				return resultMap;
			} else {

				if (coupon.getGetLimit() != null) {
					List<UserCoupon> userCouponList = userCouponService.queryUserCouponList(userName, couponId);
					if (userCouponList != null && userCouponList.size() >= coupon.getGetLimit()) {
						resultMap.put("msg", "领取失败，您该张券的领取量已超限额~");
						return resultMap;
					}
					if (coupon.getFrequency() != null && AppUtils.isNotBlank(userCouponList)) {
						int sec = (int) (coupon.getFrequency() * 60 * 60);
						UserCoupon userCoupon = userCouponList.get(0);
						Calendar cal = Calendar.getInstance();
						cal.add(Calendar.SECOND, -sec);
						if (userCoupon.getGetTime().after(cal.getTime())) {
							resultMap.put("msg", "领取太频繁，当前券需隔" + coupon.getFrequency() + "小时领取一次");
							return resultMap;
						}
					}
				}

				List<UserCoupon> unuseList = userCouponService.getUnUseUserCouponsByCoupon(couponId);
				if (AppUtils.isBlank(unuseList)) {
					resultMap.put("msg", "当前券已被领完，快寻找新目标吧~");
					return resultMap;
				} else {
					// 正常绑定 优惠券到用户
					userCouponService.updateUsercouponProperty(unuseList.get(0),  userId, userName, coupon);
					resultMap.put("code", "ok");
					resultMap.put("msg", "恭喜您领取成功");
					return resultMap;
				}
			}
		}
	}

	/**
	 * 免费优惠券，积分查询.
	 *
	 * @param request
	 * @param response
	 * @param couponId
	 * @return the string
	 */
	@RequestMapping(value = "/p/coupon/query")
	public String query(HttpServletRequest request, HttpServletResponse response, Long couponId) {
		if (AppUtils.isNotBlank(couponId)) {
			Coupon couPon = this.couponService.getCoupon(couponId);
			request.setAttribute("couPon", couPon);
		}
		return "跳到优惠券详情页";
	}

	/**
	 * 领取优惠卷.
	 *
	 * @param request
	 * @param response
	 * @param couponId
	 * @return the string
	 */
	@RequestMapping(value = "/p/coupon/receive/{couponId}")
	@ResponseBody
	public String receive(HttpServletRequest request, HttpServletResponse response, @PathVariable Long couponId) {
		SecurityUserDetail user = UserManager.getUser(request);
		String result = userCouponService.couponReceive(couponId, user.getUsername(),user.getUserId());
		return result;
	}
	
	
	/**
	 * 从优惠券到店铺商品页面
	 */
	@RequestMapping(value = "/p/coupon/couponProds/{couponId}")
	public String couponProds(HttpServletRequest request, HttpServletResponse response, @PathVariable Long couponId) {
		Coupon coupon = couponService.getCoupon(couponId);
		if(AppUtils.isBlank(coupon)) {
			request.setAttribute("message", "优惠券已不存在");
			return PathResolver.getPath(CouponFrontPage.OPERATION_ERROR);
		}
		request.setAttribute("coupon", coupon);
		request.setAttribute("curPageNO", 1);
		return PathResolver.getPath(CouponFrontPage.COUPON_PROD);
	}
		
	
	/**
	 * 从优惠券到店铺商品页面
	 */
	@RequestMapping(value = "/p/coupon/couponProdList")
	public String couponProdList(HttpServletRequest request, HttpServletResponse response, 
			Coupon coupon, String curPageNO, String orders) {
		PageSupport<Product> ps = null;
		if(CouponProviderEnum.SHOP.value().equals(coupon.getCouponProvider())) {//店铺优惠券
			if (CouponTypeEnum.PRODUCT.value().equals(coupon.getCouponType())) {//商品卷
				//查找优惠券的相关商品Id
				List<Long> prodIds = couponProdService.getCouponProdIdByCouponId(coupon.getCouponId());
				if(AppUtils.isBlank(prodIds)) {
					return "";
				}
				ps = productService.getCouponProds(null, prodIds, curPageNO, orders, null);
			}else if(CouponTypeEnum.COMMON.value().equals(coupon.getCouponType())){
				ps = productService.getCouponProds(coupon.getShopId(), null, curPageNO, orders, null);
			}
		}
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("coupon", coupon);
		request.setAttribute("orders", orders);
		return PathResolver.getPath(CouponFrontPage.COUPON_PROD_LIST);
	}
	
	
	

}