/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.coupon.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSON;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.coupon.page.CouponFrontPage;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.coupon.UserCouponListDto;
import com.legendshop.processor.coupon.CounponsUtils;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.SubService;
import com.legendshop.spi.service.TransportManagerService;
import com.legendshop.util.AppUtils;

/**
 * 订单优惠券控制器.
 */
@Controller
public class CouponOrderController extends BaseController {

	private final Logger logger = LoggerFactory.getLogger(CouponOrderController.class);

	/** 下单 Counpons 工具类 */
	@Autowired
	private CounponsUtils counponsUtil;

	@Autowired
	private SubService subService;
     
	@Autowired
	private TransportManagerService transportManagerService;

	/**
	 * 用户订单页面 查询 可用和不可用的 所有优惠券.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param userCouponIdsStr
	 *            the user coupon ids str
	 * @return the string
	 */
	@RequestMapping(value = "/p/coupon/orderCouponsPage")
	public String orderCouponsPage(HttpServletRequest request, HttpServletResponse response, String userCouponIdsStr,
			String cartTypeEnum, @RequestParam("token") Integer token) {

		if (!cartTypeEnum.equals(CartTypeEnum.NORMAL.toCode())) {
			return PathResolver.getPath(CouponFrontPage.ORDER_COUPONS_PAGE);
		}

		// 获取登录用户的userId
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		// get cache 从上个页面获取进入缓存的订单
		UserShopCartList userShopCartList = null;

		if (AppUtils.isBlank(cartTypeEnum) || cartTypeEnum.equals(CartTypeEnum.NORMAL.toCode())) {
			userShopCartList = subService.findUserShopCartList(CartTypeEnum.NORMAL.toCode(), userId);
		} else if (cartTypeEnum.equals(CartTypeEnum.AUCTIONS.toCode())) {
			userShopCartList = subService.findUserShopCartList(CartTypeEnum.AUCTIONS.toCode(), userId);
		}

		Integer sessionToken = (Integer) request.getSession().getAttribute(Constants.TOKEN); // 取session中保存的token
		if (AppUtils.isBlank(userShopCartList) || !token.equals(sessionToken)) {
			// throw new CacheException("缓存数据不正确！");
			return PathResolver.getPath(CouponFrontPage.ORDER_COUPONS_PAGE);
		}

		// 分割 已选中的 用户优惠券ID
		List<Long> userCouponIds = new ArrayList<Long>();
		if (AppUtils.isNotBlank(userCouponIdsStr)) {
			String[] userCouponIdStrs = userCouponIdsStr.split(",");
			for (int i = 0; i < userCouponIdStrs.length; i++) {
				try {
					Long userCouponId = Long.valueOf(userCouponIdStrs[i]);
					userCouponIds.add(userCouponId);
				} catch (NumberFormatException e) {
					logger.error("userCouponId:{} NumberFormatException", userCouponIdStrs[i]);
				}
			}
		}
		// 将所有购物车项的优惠券金额重置为0
		for (ShopCarts shopCarts : userShopCartList.getShopCarts()) {
			for (ShopCartItem shopCartItem : shopCarts.getCartItems()) {
				shopCartItem.setCouponOffPrice(0.0);
				shopCartItem.setRedpackOffPrice(0.0);
			}
		}
		// 分析处理 红包和优惠券
		UserCouponListDto couponListDto = counponsUtil.analysisCoupons(userId, userShopCartList, userCouponIds);
		request.setAttribute("usableUserCouponList", couponListDto.getUsableUserCouponList());
		request.setAttribute("usableCouponList", JSON.toJSONString(couponListDto.getUsableUserCouponList()));
		request.setAttribute("disableUserCouponList", couponListDto.getDisableUserCouponList());
		userShopCartList.setUserCouponIds(userCouponIds);

		transportManagerService.clacCartDeleivery(userShopCartList, true);
		request.setAttribute("freightAmount", userShopCartList.getOrderCurrentFreightAmount());
		request.getSession().setAttribute("userShopCartList", userShopCartList);
		subService.putUserShopCartList(CartTypeEnum.NORMAL.toCode(), userId, userShopCartList);
		return PathResolver.getPath(CouponFrontPage.ORDER_COUPONS_PAGE);
	}
	
	
	
	/**
	 * 用户订单页面 查询 可用和不可用的 所有优惠券.
	 */
	@RequestMapping(value = "/p/coupon/orderFreightPage")
	public String orderFreightPage(HttpServletRequest request, HttpServletResponse response) {
		UserShopCartList userShopCartList = (UserShopCartList) request.getSession().getAttribute("userShopCartList");
		request.setAttribute("userShopCartList", userShopCartList);
		return PathResolver.getPath(CouponFrontPage.ORDER_FREIGHT_PAGE);
	}

}