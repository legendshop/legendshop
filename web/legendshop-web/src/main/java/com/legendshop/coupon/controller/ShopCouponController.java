/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.coupon.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.coupon.page.CouponFrontPage;
import com.legendshop.coupon.page.RedirectPage;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.CouponProviderEnum;
import com.legendshop.model.constant.SendCouponTypeEnum;
import com.legendshop.model.dto.Select2Dto;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.CouponProd;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.CouponProdService;
import com.legendshop.spi.service.CouponService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.UserCouponService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;

/**
 * 店铺优惠卷控制器
 *
 */
@Controller
@RequestMapping("/s/coupon")
public class ShopCouponController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ShopCouponController.class);
	@Autowired
	private CouponService couponService;

	@Autowired
	private UserCouponService userCouponService;

	@Autowired
	private ProductService productService;

	@Autowired
	private CouponProdService couponProdService;

	@Autowired
	private UserDetailService userDetailService;
	
	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		CustomDateEditor editor = new CustomDateEditor(df, false);
		binder.registerCustomEditor(Date.class, editor);
	}

	/**
	 * 进入查看优惠券详情页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/watchCoupon/{id}")
	public String watchRedPacket(String curPageNO, HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Coupon coupon = couponService.getCoupon(id);

		// 查看优惠券下的所有详细优惠券
		if (AppUtils.isBlank(curPageNO) || "0".equals(curPageNO)) {
			curPageNO = "1";
		}
		PageSupport<UserCoupon> ps = userCouponService.getUserCouponListPage(curPageNO, id);
		PageSupportHelper.savePage(request, ps);

		// 如果是商品券,取出该商品券中所有的商品名称
		if (coupon.getCouponType().equals("product")) {
			List<CouponProd> couponProdList = couponProdService.getCouponProdByCouponId(id);
			List<Product> productList = new ArrayList<Product>();
			for (int i = 0; i < couponProdList.size(); i++) {
				CouponProd couponProd = couponProdList.get(i);
				Product product = productService.getProductById(couponProd.getProdId());
				productList.add(product);
			}
			request.setAttribute("productList", productList);
		}
		request.setAttribute("coupon", coupon);
		request.setAttribute("curPageNO", curPageNO);
		return PathResolver.getPath(CouponFrontPage.COUPONS_EDIT_PAGE);
	}

	/**
	 * 添加优惠券
	 */
	@RequestMapping(value = "/saveCoupon")
	public String save(HttpServletRequest request, HttpServletResponse response, Coupon coupon, String prodidList) {
		SecurityUserDetail user = UserManager.getUser(request);
		coupon.setShopId(user.getShopId());
		coupon.setCouponProvider(CouponProviderEnum.SHOP.value());
		couponService.saveByCouponAndShopId(coupon, user.getUsername(),user.getUserId(), user.getShopId(), prodidList);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.SHOP_COUPONS_LIST_QUERY);
	}

	/**
	 * 挑选商品弹框
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param product
	 * @return
	 */
	@RequestMapping("/loadSelectProd")
	public String loadSelectProd(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<Product> ps = productService.getloadSelectProd(shopId, curPageNO, product);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prod", product);
		String result = PathResolver.getPath(CouponFrontPage.LOAD_SELECT_PROD);
		return result;
	}

	/**
	 * 商家优惠券管理列表
	 */
	@RequestMapping("/query")
	@Authorize(authorityTypes = AuthorityType.COUPON_MANAGE)
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Coupon coupon) {

		StringBuffer sql = new StringBuffer();
		StringBuffer sqlCount = new StringBuffer();
		List<Object> objects = new ArrayList<Object>();
		sql.append("SELECT co.*,sd.site_name AS shopName FROM ls_coupon AS co,ls_shop_detail AS sd  WHERE co.shop_id=sd.shop_id AND co.shop_del=0 AND co.coupon_provider=? AND sd.shop_id=? ");
		sqlCount.append("SELECT COUNT(*) FROM ls_coupon AS co,ls_shop_detail AS sd WHERE co.shop_id=sd.shop_id AND co.shop_del=0 AND co.coupon_provider=?  AND sd.shop_id=?");
		objects.add(CouponProviderEnum.SHOP.value());
		// objects.add(CouponTypeEnum.CATEGORY.value());

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		objects.add(shopId);

		Date date = new Date();
		if (AppUtils.isNotBlank(coupon.getStatus())) {
			if (coupon.getStatus() == -1) {
				sql.append(" AND end_Date < ?");
				sqlCount.append(" AND end_Date < ?");
				objects.add(date);
			} else if (coupon.getStatus() == 1) {
				sql.append(" AND end_Date > ?");
				sqlCount.append(" AND end_Date > ?");
				objects.add(date);
				sql.append(" AND co.status=?");
				sqlCount.append(" AND co.status=?");
				objects.add(coupon.getStatus());
			} else {
				sql.append(" AND co.status=?");
				sqlCount.append(" AND co.status=?");
				objects.add(coupon.getStatus());
				sql.append(" AND end_Date > ?");
				sqlCount.append(" AND end_Date > ?");
				objects.add(date);
			}
		}
		if (AppUtils.isNotBlank(coupon.getCouponName())) {
			sql.append(" AND coupon_name like ?");
			sqlCount.append(" AND coupon_name like ?");
			objects.add("%" + coupon.getCouponName() + "%");
		}
		if (AppUtils.isNotBlank(coupon.getGetType())) {
			sql.append(" AND get_Type=?");
			sqlCount.append(" AND get_Type=?");
			objects.add(coupon.getGetType());
		}
		sql.append(" order by co.create_date desc");
		PageSupport<Coupon> ps = couponService.queryCoupon(sql, curPageNO, sqlCount, objects);
		PageSupportHelper.savePage(request, ps);
		//获取vue端的域名
		String vueDomainName = propertiesUtil.getVueDomainName();
		request.setAttribute("vueDomainName", vueDomainName);
		request.setAttribute("coupon", coupon);
		return PathResolver.getPath(CouponFrontPage.COUPONS_LIST);
	}

	/**
	 * 查看礼券使用情况
	 * @param request
	 * @param response
	 * @param couponId
	 * @param curPageNO
	 * @param userName
	 * @return
	 */
	@RequestMapping("/usecoupon_view/{couponId}")
	public String usecoupon_view(HttpServletRequest request, HttpServletResponse response, @PathVariable Long couponId, String curPageNO, String userName) {

		if (AppUtils.isBlank(couponId)) {
			return "";
		}
		Coupon coupon = couponService.getCoupon(couponId);
		if (coupon == null) {
			return "";
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		if (!shopId.equals(coupon.getShopId())) {
			return "";
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder sqlCount = new StringBuilder();

		List<Object> objects = new ArrayList<Object>();

		sql.append(
				" SELECT DISTINCT  uc.user_name as userName,uc.coupon_sn as couponSn,uc.get_time as getTime ,uc.get_sources as getSources,uc.use_time as useTime ,uc.order_number as orderNumber  ,uc.order_price as orderPrice,uc.use_status as useStatus,uc.user_id as userId  ");
		sql.append(" FROM ls_user_coupon AS uc  ");
		sql.append(" WHERE uc.coupon_id= ?  ");

		sqlCount.append(" SELECT DISTINCT COUNT(*)  FROM ls_user_coupon  AS uc ");
		sqlCount.append(" WHERE uc.coupon_id= ? ");

		objects.add(couponId);

		if (AppUtils.isNotBlank(userName)) {
			sql.append(" and  uc.user_name like ?  ");
			sqlCount.append(" and  uc.user_name like ?  ");
			objects.add(userName + "%");
		}

		String sqlString = sortSQL(sql);
		PageSupport<UserCoupon> ps = couponService.queryUserCoupon(sqlString, sqlCount, objects, curPageNO);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("couponId", couponId);
		request.setAttribute("userName", userName);
		return PathResolver.getPath(CouponFrontPage.USE_CPNS_VIEW);

	}

	/**
	 * 生成排序SQL
	 */
	private String sortSQL(StringBuilder sql) {
		sql.append(" order by uc.get_time desc,use_time desc");
		return sql.toString();
	}

	/**
	 * 去掉指定字符串的开头和结尾的指定字符
	 *
	 * @param stream
	 *            要处理的字符串
	 * @param trimstr
	 *            要去掉的字符串
	 * @return 处理后的字符串
	 */

	public static String sideTrim(String stream, String trimstr) {
		// null或者空字符串的时候不处理
		if (stream == null || stream.length() == 0 || trimstr == null || trimstr.length() == 0) {
			return stream;
		}
		stream = stream.trim();
		// 结束位置
		int epos = 0;

		// 正规表达式
		String regpattern = "[" + trimstr + "]*+";
		Pattern pattern = Pattern.compile(regpattern, Pattern.CASE_INSENSITIVE);

		// 去掉结尾的指定字符
		StringBuffer buffer = new StringBuffer(stream).reverse();
		Matcher matcher = pattern.matcher(buffer);
		if (matcher.lookingAt()) {
			epos = matcher.end();
			stream = new StringBuffer(buffer.substring(epos)).reverse().toString();
		}

		// 返回处理后的字符串
		return stream;
	}

	/**
	 * 转换时间
	 * @param start
	 * @param end
	 * @return
	 */
	private String transferLongToDate(Date start, Date end) {
		long between = (end.getTime() - start.getTime()) / 1000;// 除以1000是为了转换成秒
		long day = between / (24 * 3600);
		long hour = between % (24 * 3600) / 3600;
		long minute = between % 3600 / 60;
		String str = null;
		if (day == 0 && hour == 0) {
			str = minute + "分";
		} else {
			str = day + "天" + hour + "时";
		}
		return str;
	}

	/**
	 * 获取简单的商家信息数据
	 */
	@RequestMapping(value = "/getSelect2User")
	public @ResponseBody Object getSelect2User(HttpServletRequest request, HttpServletResponse response, String q, Integer currPage, Integer pageSize) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		List<Select2Dto> list = userDetailService.getSelect2User(q, currPage, pageSize, userId);
		int total = userDetailService.getSelect2UserTotal(q, userId);
		JSONObject result = new JSONObject();
		result.put("results", list);
		result.put("total", total);
		return result;
	}

	/**
	 * 保存优惠券
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, Coupon coupon) {
		SecurityUserDetail user = UserManager.getUser(request);
		coupon.setShopId(user.getShopId());
		coupon.setCouponProvider(CouponProviderEnum.SHOP.value());
		couponService.saveByCouponAndShopId(coupon, user.getUsername(), user.getUserId(), user.getShopId());

		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(RedirectPage.SHOP_COUPONS_LIST_QUERY);
	}

	/**
	 * 删除
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}")
	public @ResponseBody String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		String result = couponService.deleteCoupon(id, shopId,"shop");
		return result;
	}

	/**
	 * 查看优惠卷详情
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Coupon coupon = couponService.getCoupon(id);
		request.setAttribute("coupon", coupon);
		return PathResolver.getPath(CouponFrontPage.COUPONS_EDIT_PAGE);
	}

	/**
	 * 加载优惠卷编辑页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(CouponFrontPage.COUPONS_EDIT_PAGE);
	}

	/**
	 * 更新状态
	 * @param request
	 * @param response
	 * @param id
	 * @param status
	 * @return
	 */
	@RequestMapping(value = "/updatestatus/{id}/{status}")
	public @ResponseBody Integer updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Integer status) {
		if (AppUtils.isBlank(id)) {
			return -1;
		}
		Coupon coupon = couponService.getCoupon(id);
		if (coupon == null) {
			return -1;
		}

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		if (!shopId.equals(coupon.getShopId())) {
			return -1;
		}
		coupon.setStatus(status);
		couponService.updateCoupon(coupon);
		return coupon.getStatus();
	}

	/**
	 * 发放礼券
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "issueCoupon/{id}")
	public String issueCoupon(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		request.setAttribute("couponId", id);
		return PathResolver.getPath(CouponFrontPage.ISSUE_COUPON);
	}

	/**
	 * 线下礼券发放
	 * @param request
	 * @param response
	 * @param couponId
	 * @param count
	 * @return
	 */
	@RequestMapping(value = "/sendForOffLine/{couponId}", method = RequestMethod.POST)
	public @ResponseBody String sendForOffLine(HttpServletRequest request, HttpServletResponse response, @PathVariable Long couponId, Integer count) {
		boolean success = true;

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		String msg = "";
		if (couponId == null && success) {
			msg = "couponId不能为空";
			success = false;
		} else if (AppUtils.isBlank(count) || count <= 0 && success) {
			msg = "生成数要大于0";
			success = false;
		}
		Coupon coupon = couponService.getCoupon(couponId);
		if (coupon == null && success) {
			msg = "找不到该礼券";
			success = false;
		} else if (!shopId.equals(coupon.getShopId())) {
			msg = "该礼券不属于本商城!";
			success = false;
		} else if (!coupon.getSendType().equals(SendCouponTypeEnum.OFFLINE.value()) && success) { // 不是该类型
			msg = "该礼券类型不正确!";
			success = false;
		} else if (coupon.getStatus() == Constants.OFFLINE.intValue() && success) {
			msg = "该礼券已冻结!";
			success = false;
		}
		long ctime = new Date().getTime();
		long end_date = coupon.getEndDate().getTime();
		if (ctime > end_date && success) {
			msg = "该礼券已过期!";
			success = false;
		} else if (coupon.getCouponNumber() == 0 && success) {
			msg = "该礼券初始化数量为0 !";
			success = false;
		} else if (coupon.getCouponNumber() < count && success) {
			msg = "发放数量不能大于初始化数量 !";
			success = false;
		}

		if (success) {
			int init = 100;// 每隔100条循环一次

			int runnum = count % init == 0 ? (count / init) : (count / init) + 1;

			int cycelTotal = count;
			LOGGER.info("线下礼券生成-循环保存的次数：" + runnum);
			String prefix = coupon.getCouponNumPrefix();
			long cnps_number = 0;
			for (int r = 0; r < runnum; r++) {
				if (cycelTotal < init) { // 9
					init = cycelTotal;
				}
				List<UserCoupon> userCoupon = new ArrayList<UserCoupon>();
				for (int j = 0; j < init; j++) { // 每批多少个
					UserCoupon cou = new UserCoupon();
					cou.setCouponId(couponId);
					cou.setCouponName(coupon.getCouponName());
					cou.setUseStatus(1);
					// 生产劵号
					cou.setCouponSn(CommonServiceUtil.getRandomCouponNumberGenerator());
					// cou.setGetTime(new Date());
					cou.setGetSources("线下发放");
					userCoupon.add(cou);
					cnps_number++;
				}
				if (AppUtils.isNotBlank(userCoupon)) {
					boolean config = userCouponService.saveOffUserCoupon(userCoupon, userCoupon.size(), couponId);
					userCoupon = null;
				}
				cycelTotal -= init;
			}
			LOGGER.info("线下礼券生成count：" + cnps_number);
			success = true;
		}

		String res = "{\"success\":" + success + ",\"msg\":\"" + msg + "\"}";
		return res;
	}

	/**
	 * 导出礼券为Excel
	 * @param request
	 * @param response
	 * @param couponId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/exportExcel/{couponId}", method = RequestMethod.POST)
	public @ResponseBody String exportPordExcel(HttpServletRequest request, HttpServletResponse response, @PathVariable Long couponId) throws Exception {
		boolean success = true;
		String msg = "";

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		if (AppUtils.isBlank(couponId) && success) {
			msg = "couponId不能为空";
			success = false;
		}
		Coupon coupon = couponService.getCoupon(couponId);
		if (coupon == null && success) {
			msg = "找不到该礼券";
			success = false;
		} else if (!shopId.equals(coupon.getShopId())) {
			msg = "该礼券不属于本商城!";
			success = false;
		} else if (!coupon.getSendType().equals(SendCouponTypeEnum.OFFLINE.value()) && success) { // 不是该类型
			msg = "该礼券类型不正确!";
			success = false;
		} else if (coupon.getStatus() == Constants.OFFLINE.intValue() && success) {
			msg = "该礼券已冻结!";
			success = false;
		}
		long ctime = new Date().getTime();
		long end_date = coupon.getEndDate().getTime();
		if (ctime > end_date && success) {
			msg = "该礼券已过期!";
			success = false;
		}
		// 查询要导出的数据
		List<UserCoupon> userCoupon = userCouponService.getOffExportExcelCoupon(couponId);
		if (AppUtils.isBlank(userCoupon) && success) {
			msg = "没有相关的礼券信息,请重新生成发放!";
			success = false;
		}
		if (success) {
			String[] headers = { "礼券ID", "礼券名称", "礼券序号" };
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmm");
			String fileName = "offline_coupon_" + dateFormat.format(new Date()) + ".xlsx";
			String filePath = systemParameterUtil.getDownloadFilePath();
			OutputStream out = new FileOutputStream(getExcelFile(filePath, fileName));

			boolean config = userCouponService.exportExcel(couponId + "线下发放礼券表", headers, userCoupon, out, "yyyy-MM-dd");
			if (config) {
				msg = fileName;
				success = true;
			} else {
				msg = "下载失败,请联系管理员";
				success = false;
			}
		}
		String res = "{\"success\":" + success + ",\"msg\":\"" + msg + "\"}";
		return res;
	}

	/**
	 * 创建文件
	 * @param filePath
	 * @param fileName
	 * @return
	 */
	private File getExcelFile(String filePath, String fileName) {
		File file = new File(filePath);
		if (!file.exists() && !file.isDirectory()) {
			file.mkdirs();
		}
		File excelFile = new File(filePath, fileName);
		return excelFile;
	}
}
