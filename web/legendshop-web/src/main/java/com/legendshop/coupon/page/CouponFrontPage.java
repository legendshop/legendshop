/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.coupon.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 店铺前台页面定义.
 */
public enum CouponFrontPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),
	
	/**  礼券管理  */
	COUPONS_LIST("/shop/couponList"), 
	
	/** 挑选商品弹框 */
	LOAD_SELECT_PROD("/shop/loadSelectProd"),
	
	/** 查看代金券 */
	COUPONS_EDIT_PAGE("/shop/coupon"), 
	
	/** 赠送礼券 */
	USERCOUPONS_LIST_PAGE("/shop/userCouponList"), 
	
	/** 礼券使用情况 */
	USE_CPNS_VIEW("/shop/useCouponView"),
	
	/** 发放礼券 */
	ISSUE_COUPON("/shop/issueCoupon"),
	
	/** 个人中心--我的优惠券 */
	COUPON_INDEX_PAGE("/couponIndex"),
	
	/** 激活优惠券 */
	GET_COUPON_PAGE("/getCoupon"),
	
	/** 礼券领取 */
	APPLY_COUPON("/applyCoupon"),
	
	/** 订单优惠券列表*/
	ORDER_COUPONS_PAGE("/orderCouponsPage"),
	
	/** 订单运费列表*/
	ORDER_FREIGHT_PAGE("/orderFreightPage"),
	
	/** 领券中心*/
	COUPON_CENTER("/couponCenter"), 
	
	/** 领券中心:更多*/
	COUPON_MORE("/couponMore"),
	
	/** 操作错误页面 */
	OPERATION_ERROR("/operationError"),
	
	/**优惠券商品**/
	COUPON_PROD("/couponProd"),
	
	/**优惠券商品列表**/
	COUPON_PROD_LIST("/couponProdList");
	
	/** The value. */
	private final String value;

	private CouponFrontPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath("coupon", path);
	}

	public String getNativeValue() {
		return value;
	}

}
