package com.legendshop.consult.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 咨询收费页面.
 */
public enum ConsultPage implements PageDefinition {


	/**咨询收费列表**/
	CONSULT_PAGE_LIST("/consultList");

	/** The value. */
	private final String value;

	private ConsultPage(String value) {
		this.value = value;
	}

	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath("consult", path);
	}

	public String getNativeValue() {
		return value;
	}

}
