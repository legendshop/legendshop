package com.legendshop.consult.controller;

import com.legendshop.consult.page.ConsultPage;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ConsultController {


	@RequestMapping("/consultList")
	public String consultList(HttpServletRequest request){
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isNotBlank(user)){
			request.setAttribute("loginUserName", user.getUsername());
		}
		return  PathResolver.getPath(ConsultPage.CONSULT_PAGE_LIST);
	}


}
