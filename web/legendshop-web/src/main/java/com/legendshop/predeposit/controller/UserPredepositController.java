/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.predeposit.controller;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSMSEvent;
import com.legendshop.base.event.ShortMessageInfo;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.controller.UserCenterPersonInfoController;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.MailCategoryEnum;
import com.legendshop.model.constant.SMSTypeEnum;
import com.legendshop.model.dto.UserPredepositDto;
import com.legendshop.model.entity.*;
import com.legendshop.model.vo.CoinVo;
import com.legendshop.predeposit.page.PredepositFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.manager.MailManager;
import com.legendshop.spi.service.*;
import com.legendshop.util.AppUtils;
import com.legendshop.util.RandomStringUtils;
import com.legendshop.web.helper.IPHelper;
import com.legendshop.web.util.ValidateCodeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * 用户预存款控制器
 *
 */
@Controller
@RequestMapping("/p/predeposit")
public class UserPredepositController extends BaseController {

	/**
	 * The log.
	 */
	private final Logger logger = LoggerFactory.getLogger(UserPredepositController.class);

	@Autowired
	private UserCenterPredepositService userCenterPredepositService;

	@Autowired
	private PayTypeService payTypeService;

	@Autowired
	private PdRechargeService pdRechargeService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private MailManager mailManager;

	@Autowired
	private MailEntityService mailEntityService;

	@Autowired
	private SMSLogService smsLogService;

	@Autowired
	private PreDepositService preDepositService;

	@Autowired
	private PdWithdrawCashService pdWithdrawCashService;

	@Autowired
	private PdCashLogService pdCashLogService;

	@Autowired
	private SystemParameterService systemParameterService;

	/**
	* 短信发送者
	*/
	@Resource(name = "sendSMSProcessor")
    private EventProcessor<ShortMessageInfo> sendSMSProcessor;

	/**
	 * 账户余额及充值记录信息
	 */
	@RequestMapping("/account_balance")
	public String accountBalance(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		UserDetail userDetail = userDetailService.getUserDetailByIdNoCache(userId);
		UserPredepositDto predeposit = new UserPredepositDto();
		if (AppUtils.isNotBlank(userDetail)) {
			predeposit.setFreezePredeposit(userDetail.getFreezePredeposit());
			predeposit.setAvailablePredeposit(userDetail.getAvailablePredeposit());
		}
		PageSupport<PdCashLog> pdCashLogs = pdCashLogService.getPdCashLogPage(userId, curPageNO);
		PageSupportHelper.savePage(request, pdCashLogs);
		predeposit.setPdCashLogs(pdCashLogs);

		request.setAttribute("predeposit", predeposit);
		return PathResolver.getPath(PredepositFrontPage.ACCOUNT_BALANCE);
	}

	/**
	 * 金币余额及充值记录信息
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param pdCash
	 * @return
	 */
	@RequestMapping("/coin")
	public String coin(HttpServletRequest request, HttpServletResponse response, String curPageNO) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		CoinVo coinVo = userCenterPredepositService.findUserCoin(userId, curPageNO);
		request.setAttribute("coinVo", coinVo);
		return PathResolver.getPath(PredepositFrontPage.COIN);
	}

	/**
	 * 充值明细记录
	 *
	 */
	@RequestMapping("/recharge_detail")
	public String rechargeDetail(HttpServletRequest request, HttpServletResponse response, String curPageNO, String pdrSn) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		UserDetail userDetail = userDetailService.getUserDetailByIdNoCache(userId);
		UserPredepositDto predeposit = new UserPredepositDto();
		if (AppUtils.isNotBlank(userDetail)) {
			predeposit.setFreezePredeposit(userDetail.getFreezePredeposit());
			predeposit.setAvailablePredeposit(userDetail.getAvailablePredeposit());
		}
		PageSupport<PdRecharge> pageSupport = pdRechargeService.getPdRechargePage(userId, null, pdrSn, curPageNO, 10);
		PageSupportHelper.savePage(request, pageSupport);
		predeposit.setRechargeDetails(pageSupport);

		request.setAttribute("predeposit", predeposit);
		request.setAttribute("curPageNO", curPageNO);
		request.setAttribute("pdrSn", pdrSn);
		return PathResolver.getPath(PredepositFrontPage.RECHARGE_DETAILS);
	}

	/**
	 * 余额提现列表
	 *
	 */
	@RequestMapping("/balance_withdrawal")
	public String balance_withdrawal(HttpServletRequest request, HttpServletResponse response, String curPageNO, String pdcSn, Integer status) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		UserDetail userDetail = userDetailService.getUserDetailByIdNoCache(userId);
		UserPredepositDto predeposit = new UserPredepositDto();
		if (AppUtils.isNotBlank(userDetail)) {
			predeposit.setFreezePredeposit(userDetail.getFreezePredeposit());
			predeposit.setAvailablePredeposit(userDetail.getAvailablePredeposit());
		}
		PageSupport<PdWithdrawCash> pageSupport = pdWithdrawCashService.getFindBalanceWithdrawal(curPageNO, pdcSn, status, userId);
		PageSupportHelper.savePage(request, pageSupport);
		predeposit.setPdCashs(pageSupport);

		request.setAttribute("predeposit", predeposit);
		request.setAttribute("curPageNO", curPageNO);
		request.setAttribute("pdcSn", pdcSn);
		request.setAttribute("status", status);
		return PathResolver.getPath(PredepositFrontPage.BALANCE_WITHDRAWAL);
	}

	/**
	 * 用户冻结余额记录信息
	 *
	 * @param userId
	 * @param curPageNO
	 * @param pdrSn
	 * @return
	 */
	@RequestMapping("/balance_hoding")
	public String balance_hoding(HttpServletRequest request, HttpServletResponse response, String curPageNO, String sn, String type) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		UserPredepositDto predeposit = new UserPredepositDto();
		PageSupport<PdCashHolding> ps = userCenterPredepositService.getBalanceHoding(userId, curPageNO, sn, type);
		PageSupportHelper.savePage(request, ps);
		predeposit.setCashHodings(ps);

		request.setAttribute("predeposit", predeposit);
		request.setAttribute("curPageNO", curPageNO);
		request.setAttribute("sn", sn);
		request.setAttribute("type", type);
		return PathResolver.getPath(PredepositFrontPage.BALANCE_HODING);
	}

	/**
	 * 商城充值单
	 *
	 * @param request
	 * @param response
	 * @param pdrSn
	 * @return
	 */
	@RequestMapping("/deleteRechargeDetail/{pdrSn}")
	public @ResponseBody String deleteRechargeDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable String pdrSn) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		String success = userCenterPredepositService.deleteRechargeDetail(userId, pdrSn);
		return success;
	}

	/**
	 * 充值金额
	 *
	 * @param request
	 * @param response
	 * @param pdrSn
	 * @return
	 */
	@RequestMapping("/recharge_add")
	public String rechargeAdd(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(PredepositFrontPage.RECHARGE_ADD);
	}


	/**
	 * 校验数据
	 */
	@RequestMapping(value = "/validateData", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> validateData(HttpServletRequest request, HttpServletResponse response, Double pdrAmount, String captcha) {
		Map<String, Object> map = new HashMap<>();

		if (AppUtils.isBlank(pdrAmount) || pdrAmount <= 0) {
			map.put("status", Constants.FAIL);
			map.put("msg", "请输入充值金额");
			return map;
		}
		if (AppUtils.isBlank(captcha) || !ValidateCodeUtil.validateCode(captcha, request.getSession())) {
			map.put("status", Constants.FAIL);
			map.put("msg", "验证码不正确");
			return map;
		}
		map.put("status", Constants.SUCCESS);
		map.put("msg", "合法");
		return map;
	}

	/**
	 * 去充值
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/recharge_pay", method = RequestMethod.POST)
	public String rechargeForm(HttpServletRequest request, HttpServletResponse response, Double pdrAmount, String captcha) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();

		PdRecharge pdRecharge = userCenterPredepositService.rechargePay(userId, userName, pdrAmount);
		if (AppUtils.isBlank(pdRecharge)) {
			request.setAttribute("messgae", "创建充值流水失败，请联系管理员");
			return PathResolver.getPath(PredepositFrontPage.RECHARGE_FAIL);
		}
		request.setAttribute("pdRecharge", pdRecharge);
		return PathResolver.getRedirectPath("/p/predeposit/recharge_pay/" + pdRecharge.getSn());
	}

	/**
	 * 去充值
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/recharge_pay/{paySn}", method = RequestMethod.GET)
	public String rechargeForm(HttpServletRequest request, HttpServletResponse response, @PathVariable String paySn) {
		if (AppUtils.isBlank(paySn)) {
			return PathResolver.getRedirectPath("/p/predeposit/recharge_detail");
		}
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		PdRecharge pdRecharge = pdRechargeService.findRechargePay(userId, paySn, 0);
		if (AppUtils.isBlank(pdRecharge)) {
			return PathResolver.getRedirectPath("/p/predeposit/recharge_detail");
		}
		request.setAttribute("pdRecharge", pdRecharge);
		// 获取 支付方式
		Map<String, Integer> payTypesMap = payTypeService.getPayTypeMap();
		request.setAttribute("payTypesMap", payTypesMap);
		return PathResolver.getPath(PredepositFrontPage.RECHARGE_PAY);
	}

	/**
	 * 申请提现页面
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/predepositApply", method = RequestMethod.GET)
	public String rechargeForm(HttpServletRequest request, HttpServletResponse response) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		UserDetail userDetail = userDetailService.getUserDetailByIdNoCache(userId);
		if (AppUtils.isBlank(userDetail)) {
			request.setAttribute("messgae", "找不到该用户信息");
			return PathResolver.getPath(PredepositFrontPage.RECHARGE_FAIL);
		} else if (userDetail.getAvailablePredeposit() <= 0) {
			request.setAttribute("messgae", "亲,您的账户余额为0,不支持提现");
			return PathResolver.getPath(PredepositFrontPage.RECHARGE_FAIL);
		}

		UserSecurity userSecurity = userDetailService.getUserSecurity(userDetail.getUserName());
		SystemParameter systemParameter = systemParameterService.getSystemParameter("WITHDRAWAL_AMOUNT");
		Integer minAmount=Integer.parseInt(systemParameter.getValue());
		String phone = null;
		String email = null;
		if (userSecurity.getPhoneVerifn().intValue() == 1 || userSecurity.getMailVerifn().intValue() == 1) {
			if (userSecurity.getPhoneVerifn().intValue() == 1) {
				phone = mobileSubstitution(userDetail.getUserMobile());
			}
			if (userSecurity.getMailVerifn().intValue() == 1) {
				email = mailSubstitution(userDetail.getUserMail());
			}
		}
		request.setAttribute("minAmount", minAmount);
		request.setAttribute("userPhone", phone);
		request.setAttribute("userEmail", email);
		request.setAttribute("availablePredeposit", userDetail.getAvailablePredeposit());
		return PathResolver.getPath(PredepositFrontPage.PREDEPOSIT_APPLY);
	}

	/**
	 * 提现身份认证
	 *
	 * @param request
	 * @param response
	 * @param withdrawCash
	 * @return
	 */
	@RequestMapping(value = "/autoType/{autoType}", method = RequestMethod.GET)
	public @ResponseBody String autoType(HttpServletRequest request, HttpServletResponse response, @PathVariable String autoType, String codeimage) {
		// 检查图片验证码是否正确
		if (!ValidateCodeUtil.validateCodeAndKeep(codeimage, request.getSession())) {
			return Constants.ERROR_CODE;
		}

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();

		UserDetail usDetailer = userDetailService.getUserDetailById(userId);
		if ("email".equals(autoType)) {
			UserSecurity security = userDetailService.getUserSecurity(userName);
			if (security.getMailVerifn().intValue() == 0) {
				return "该邮箱没有通过身份绑定";
			}
			MailEntity mailEntity = mailEntityService.getMailEntity(usDetailer.getUserMail(), MailCategoryEnum.TIX);
			if (AppUtils.isNotBlank(mailEntity)) {
				// 检查是否超时
				Date recDate = mailEntity.getRecTime();
				// 控制3分钟内不能重复发送
				if (new Date().getTime() < (recDate.getTime() + 60000 * 3)) { // 没有超时
																				// 60000毫秒
																				// ==>
																				// 3分钟
					return "请不要频繁获取安全认证码,请稍候重试";
				}
			}
			// 获得验证码
			String code = RandomStringUtils.randomNumeric(3, 6);
			// 发送邮件
			mailManager.withdrawApply(usDetailer, usDetailer.getUserMail(), code);
			return Constants.SUCCESS;
		} else if ("phone".equals(autoType)) {
			UserSecurity security = userDetailService.getUserSecurity(userName);
			if (security.getPhoneVerifn().intValue() == 0) {
				return "该手机没有通过身份绑定";
			}
			SMSLog smsLog = smsLogService.getValidSMSCode(usDetailer.getUserMobile(), SMSTypeEnum.VAL);
			if (AppUtils.isNotBlank(smsLog)) {
				// 检查是否超时
				Date recDate = smsLog.getRecDate();
				// 控制3分钟内不能重复发送
				if (new Date().getTime() < (recDate.getTime() + 60000 * 3)) { // 没有超时
																				// 60000毫秒
																				// ==>
																				// 3分钟
					return "请不要频繁获取安全认证码,请稍候重试";
				}
			}
			// 获得验证码
			String code = RandomStringUtils.randomNumeric(3, 6);
			// 发送短信
			String ip = "";
			try {
				ip = IPHelper.getIpAddr(request);
			} catch (Exception e) {
				e.printStackTrace();
			}
			logger.info("UserPredepositController autoType 发送短信验证码 mobile：{}，ip：{}", usDetailer.getUserMobile(), ip);
			sendSMSProcessor.process(new SendSMSEvent(usDetailer.getUserMobile(), code, userName,SMSTypeEnum.VAL,"/autoType/{autoType}",ip,new Date()).getSource());
			return Constants.SUCCESS;
		}
		return Constants.FAIL;
	}

	/**
	 * 提交申请页面
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/balanceWithdrawals", method = RequestMethod.POST)
	public @ResponseBody String balanceWithdrawals(HttpServletRequest request, HttpServletResponse response, PdWithdrawCash withdrawCash) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();

		String result = isValidate(withdrawCash, request);
		if (!Constants.SUCCESS.equals(result)) {
			return result;
		}
		// 校验是否身份认证
		UserDetail userDetail = userDetailService.getUserDetailById(userId);
		if ("email".equals(withdrawCash.getAuthType())) {
			MailEntity mailEntity = mailEntityService.getMailEntity(userDetail.getUserMail(), MailCategoryEnum.TIX);
			if (AppUtils.isBlank(mailEntity)) {
				return "请获取邮件的身份认证码！";
			}
			// 检查是否超时
			Date recDate = mailEntity.getRecTime();
			if (new Date().getTime() > (recDate.getTime() + 60000 * 10)) { // 超时
																			// //
																			// 60000毫秒
																			// //
																			// ==>
																			// //
																			// 10分钟
				return "您的验证码已经过期,请重新获取验证码信息";
			}
			if (!withdrawCash.getAuthCode().equals(mailEntity.getMailCode())) {
				return "请输入正确的邮件验证码!";
			} else {
				// 验证成功，清除邮件验证码
				mailEntityService.clearMailCode(mailEntity.getId());
			}
		} else if ("phone".equals(withdrawCash.getAuthType())) {
			SMSLog smsLog = smsLogService.getValidSMSCode(userDetail.getUserMobile(), SMSTypeEnum.VAL);
			if (AppUtils.isBlank(smsLog)) {
				return "请获取手机的身份认证码！";
			}
			if (!withdrawCash.getAuthCode().equals(smsLog.getMobileCode())) {
				return "请输入正确的手机验证码!";
			}
			// 检查短信验证码是否正确
			if (!smsLogService.verifyCodeAndClear(userDetail.getUserMobile(), withdrawCash.getAuthCode(), SMSTypeEnum.VAL)) {
				return "您的验证码已经过期,请重新获取验证码信息!";
			}
		} else {
			return "参数有误！";
		}
		// 检查图片验证码是否正确
		if (!ValidateCodeUtil.validateCode(withdrawCash.getCaptcha(), request.getSession())) {
			return "CAPTCHA_FAIL";
		}
		String pdcSn = CommonServiceUtil.getRandomSn();
		PdWithdrawCash orgin = new PdWithdrawCash();
		orgin.setAddTime(new Date());
		orgin.setPdcSn(pdcSn);
		orgin.setUserId(userId);
		orgin.setUserName(userName);
		orgin.setAmount(withdrawCash.getAmount());
		orgin.setBankName(withdrawCash.getBankName());
		orgin.setBankNo(withdrawCash.getBankNo());
		orgin.setBankUser(withdrawCash.getBankUser());
		orgin.setPaymentState(0);
		orgin.setUserNote(withdrawCash.getUserNote());
		result = preDepositService.withdrawalApply(orgin);
		withdrawCash = null;
		// 提现申请操作
		return result;
	}

	/**
	 * 取消提现申请
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/withdrawalApplyCancel/{id}", method = RequestMethod.POST)
	public @ResponseBody String withdrawalApplyCancel(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		PdWithdrawCash pdWithdrawCash = pdWithdrawCashService.getPdWithdrawCash(id, userId);
		if (AppUtils.isBlank(pdWithdrawCash)) {
			return "没有找到该申请单";
		}
		if (pdWithdrawCash.getPaymentState().intValue() == 0) {
			return preDepositService.withdrawalApplyCancel(pdWithdrawCash.getPdcSn(), pdWithdrawCash.getAmount(), userId);
		}
		return "该申请单正在处理中,请稍候重试";
	}

	/**
	 * 提现申请详情
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/withdrawalDetail/{id}", method = RequestMethod.GET)
	public String withdrawalDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		PdWithdrawCash pdWithdrawCash = pdWithdrawCashService.getPdWithdrawCash(id, userId);
		if (AppUtils.isBlank(pdWithdrawCash)) {
			request.setAttribute("messgae", "没有找到该申请单");
			return PathResolver.getPath(PredepositFrontPage.RECHARGE_FAIL);
		}
		request.setAttribute("withdrawCash", pdWithdrawCash);
		return PathResolver.getPath(PredepositFrontPage.WITHDRAWAL_DETAIL);
	}

	/**
	 * 充值详情
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/rechargeDetail/{id}", method = RequestMethod.GET)
	public String rechargeDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		PdRecharge pdRecharge = pdRechargeService.getPdRecharge(id, userId);
		if (AppUtils.isBlank(pdRecharge)) {
			request.setAttribute("messgae", "没有找到该充值单");
			return PathResolver.getPath(PredepositFrontPage.RECHARGE_FAIL);
		}
		request.setAttribute("recharge", pdRecharge);
		return PathResolver.getPath(PredepositFrontPage.RECHARGE_DETAIL);
	}

	private String isValidate(PdWithdrawCash withdrawCash, HttpServletRequest request) {
		if (AppUtils.isBlank(withdrawCash.getBankName())) {
			return "请选择提现方式";
		}
		if (AppUtils.isBlank(withdrawCash.getBankNo())) {
			return "请输入提款帐号";
		}
		if (AppUtils.isBlank(withdrawCash.getBankUser())) {
			return "请填写您的真实姓名";
		}
		if (AppUtils.isBlank(withdrawCash.getAmount()) || withdrawCash.getAmount() <= 0) {
			return "输入正确提现金额格式,提现金额为大于或者等于0.01的数字";
		}
		if (AppUtils.isNotBlank(withdrawCash.getAmount())) {
			Pattern moneyPattern = Pattern.compile("^\\d+(\\.\\d{1,2})?$");
			if (!moneyPattern.matcher(withdrawCash.getAmount().toString()).matches()) {
				return "输入正确提现金额格式,提现金额为大于或者等于0.01的数字";
			}
		}
		if (AppUtils.isBlank(withdrawCash.getAuthType())) {
			return "请选择认证方式";
		}
		if (AppUtils.isBlank(withdrawCash.getAuthCode())) {
			return "请输入身份认证码";
		}
		if (AppUtils.isBlank(withdrawCash.getCaptcha())) {
			return "请输入图形验证码";
		}
		return Constants.SUCCESS;
	}

	/**
	 * 页面加密手机号码，保留前3个和后3个
	 *
	 * @param from
	 * @param to
	 * @param number
	 * @return
	 */
	private String mobileSubstitution(String mobile) {
		if (mobile == null) {
			return null;
		}
		if (mobile.length() < 11) {
			return mobile;
		}
		return mobile.replaceAll(mobile.substring(3, mobile.length() - 3), "******");
	}

	private String mailSubstitution(String mail) {
		if (AppUtils.isBlank(mail)) {
			return null;
		}

		int pos = mail.indexOf("@");
		String preFix = mail.substring(0, pos);
		String postFix = mail.substring(pos, mail.length());
		int mailPrefixLength = preFix.length();
		StringBuilder sb = new StringBuilder();
		if (mailPrefixLength < 3) {
			for (int i = 0; i < mailPrefixLength; i++) {
				sb.append("*");
			}
		} else {
			sb.append(preFix.subSequence(0, 1));
			for (int i = 1; i < mailPrefixLength - 1; i++) {
				sb.append("*");
			}
			sb.append(preFix.subSequence(mailPrefixLength - 1, mailPrefixLength));
		}
		return sb.toString() + postFix;
	}

}
