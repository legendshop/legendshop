/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.predeposit.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.predeposit.page.PredepositFrontPage;

/**
 * 会员中心交易控制器
 */
@Controller
public class UserCenterDepositController extends BaseController {

	private final Logger logger = LoggerFactory.getLogger(UserCenterDepositController.class);

	/**
	 *  预存款
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/p/adddeposit")
	public String adddeposit(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(PredepositFrontPage.ADD_DEPOSIT);
	}

	/**
	 *  我的预存款
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/p/mydeposit")
	public String mydeposit(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(PredepositFrontPage.MY_DEPOSIT);
	}

}
