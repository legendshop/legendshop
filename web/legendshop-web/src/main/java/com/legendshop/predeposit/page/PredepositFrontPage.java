/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.predeposit.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 预存款前台页面定义
 * 
 */
public enum PredepositFrontPage implements PageDefinition {
	
	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	ADD_DEPOSIT("/adddeposit"),
	
	MY_DEPOSIT("/mydeposit"), 
	
    /** 我的金币 */
    COIN("/coin"),
	
	/** 预存款账户余额 */
	ACCOUNT_BALANCE("/userPredeposit"),
	
	/** 预存款充值明细 */
	RECHARGE_DETAILS("/userRechargeDetail"),
	
	/** 预存款余额提现 */
	BALANCE_WITHDRAWAL("/balanceWithdrawals"),
	
	/** 预存款充值 */
	RECHARGE_ADD("/rechargeAdd"),
	
	/** 预存款支付提交 */
	RECHARGE_PAY("/rechargePay"),
	
	/** 错误页面 */
	RECHARGE_FAIL("/rechargeFail"),
	
	/** 预存款申请提现 */
	PREDEPOSIT_APPLY("/balanceWithdrawalsApply"), 
	
	/** 提现明细 */
	WITHDRAWAL_DETAIL("/withdrawalsDetail"),
	
	/** 充值详情 */
	RECHARGE_DETAIL("/rechargeDetail"),
	
	/** 冻结中的余额信息 */
	BALANCE_HODING("/userProcessingPredeposit"), 

	;

	

	/** The value. */
	private final String value;

	/**
	 * The Constructor.
	 *
	 * @param value the value
	 */
	private PredepositFrontPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath("predeposit", path);
	}

	/* (non-Javadoc)
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

	
	

}
