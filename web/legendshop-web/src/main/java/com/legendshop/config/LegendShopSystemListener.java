/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.config;

import com.legendshop.framework.event.EventHome;
import com.legendshop.framework.event.SysInitEvent;
import com.legendshop.model.constant.Constants;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SystemUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * 系统初始化,代替InitSysListener
 *
 */
@WebListener
public class LegendShopSystemListener implements ServletContextListener{
	
	/** 日志. */
	private final static Logger log = LoggerFactory.getLogger(LegendShopSystemListener.class);
	
	@Autowired
	private PropertiesUtil propertiesUtil;
	
	@Autowired
	private ApplicationContext applicationContext; // Spring应用上下文环境


	private void initSystem(ServletContextEvent event, PropertiesUtil propertiesUtil) {

		// 1. 初始化系统真实路径
		String realPath = event.getServletContext().getRealPath("/");
		if (realPath != null && !realPath.endsWith("/") && !realPath.endsWith("\\")) {
			realPath = realPath + "/";
		}
		SystemUtil.setSystemRealPath(realPath);

		ServletContext servletContext = event.getServletContext();
		// 相对路径
		String contextPath = servletContext.getContextPath();
		SystemUtil.setContextPath(contextPath);

		// 域名
		servletContext.setAttribute(Constants.PC_DOMAIN_NAME, propertiesUtil.getPcDomainName());

		log.info("*********  LegendShop System Initialized PC domain name is " + propertiesUtil.getPcDomainName());

	}

	/**
	 * 打印spring context
	 * 
	 * @param ctx
	 */
	private void printBeanFactory(ApplicationContext ctx) {
		if (log.isWarnEnabled()) {
			int i = 0;
			String[] beans = ctx.getBeanDefinitionNames();
			StringBuffer sb = new StringBuffer("系统配置的Spring Bean [ \n");
			if (AppUtils.isNotBlank(beans)) {
				for (String bean : beans) {
					sb.append(++i).append(" ").append(bean).append("\n");
				}
				sb.append(" ]");
				log.warn(sb.toString());
			}
		}

	}


	@Override
	public void contextInitialized(ServletContextEvent event) {
		System.out.println("######$###  Initializing LegendShop System now #############");
		log.info("********* Initializing LegendShop System now *************");

		//PropertiesUtil propertiesUtil = PropertiesUtilManager.getPropertiesUtil();
		// 1. 初始化系统
		initSystem(event, propertiesUtil);

		// 4. 发送系统启动事件
//		EventHome.publishEvent(new SysInitEvent(propertiesUtil.getPcDomainName()));

		// 5. 设置系统的SQL调试模式
//		ConfigCode sQlCode = ConfigCode.getInstance("classpath*:DAL.cfg.xml");
//		boolean debugMode = propertiesUtil.isSQLInDebugMode();
//		log.debug("System in DEBUG MODE is {}", debugMode);
//		sQlCode.setDebug(debugMode);

		printBeanFactory(applicationContext);
		log.info("*********  LegendShop System Initialized successful **********");
		
	}


	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		log.info("LegendShop Frontend System is going to shutdown");
		
	}

}
