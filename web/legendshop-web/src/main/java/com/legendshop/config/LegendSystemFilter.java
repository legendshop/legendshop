/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.config;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.legendshop.redis.UserOnlineStatusHelper;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Component;

import com.legendshop.core.handler.LegendHandler;
import com.legendshop.core.handler.impl.NormalLegendHandler;
import org.springframework.util.Assert;

/**
 * 过滤器,代替LegendFilter,必须要优先执行,所以Order要设置小一点
 *
 */
@Order(-9999)
@Component
@WebFilter(urlPatterns = "/*",filterName = "legendSystemFilter")
@Slf4j
public class LegendSystemFilter implements Filter{

	private LegendHandler legendHandler = new NormalLegendHandler();

	@Resource(name = "securityContextLogoutHandler")
	private SecurityContextLogoutHandler securityContextLogoutHandler;

	/**
	 * 登录的页面
	 */
	private String LOGIN_PAGE = "/login";

	private String LOTION_ACTION = "/j_spring_security_check";

	@Autowired
	private UserOnlineStatusHelper userOnlineStatusHelper;

	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse resp = (HttpServletResponse)response;
		String uri = req.getRequestURI();

		//不包括有后缀的URL,只是支持Controller的动作（不带后缀）
		boolean isSupportUrl = supportURL(uri);

		//检查用户是否已经登录，如果登录过则会将用户信息放于Redis，目前只需要对需要认证的URL进行过滤
		if(isSupportUrl && uri.startsWith("/s/") || uri.startsWith("/p/") || uri.startsWith("/m")){
			if(!uri.endsWith(LOTION_ACTION)){//登录的动作不拦截
				SecurityUserDetail userDetail = UserManager.getUser(req);
				if(userDetail != null){
					boolean online = userOnlineStatusHelper.isUserOnline(userDetail.getLoginUserType(), userDetail.getUserId());
					if(!online){
						//系统登出
						logout(req,resp);
						resp.sendRedirect(LOGIN_PAGE);
						return;
					}
				}
			}

		}

		legendHandler.doHandle(isSupportUrl, req, resp, chain);
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 不包括有后缀的URL,只是支持Controller的动作（不带后缀）
	 * 
	 * @param url
	 * @return
	 */
	private boolean supportURL(String url) {
		if (url == null) {
			return false;
		}
		return url.indexOf(".") < 0;
	}

	/**
	 *  登出动作
	 */
	private void logout(HttpServletRequest request, HttpServletResponse response) {
		Assert.notNull(request, "HttpServletRequest required");

//		//在redis做session无法清除登录状态，因此需要手动去除session里的内容
		request.getSession().removeAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY);

	}

}
