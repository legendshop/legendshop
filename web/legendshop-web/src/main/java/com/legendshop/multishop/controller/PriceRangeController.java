/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.PriceRangeDto;
import com.legendshop.model.entity.PriceRange;
import com.legendshop.multishop.page.ReportFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.PriceRangeService;
import com.legendshop.util.AppUtils;

/**
 * 价格区间控制器
 *
 */
@Controller
@RequestMapping("/s/priceRange")
public class PriceRangeController extends BaseController {

	@Autowired
	private PriceRangeService priceRangeService;

	/**
	 * 查询价格区间
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param curPageNO
	 *            the cur page no
	 * @param type
	 *            the type
	 * @return the string
	 */
	@RequestMapping(value = "/query/{type}")
	@Authorize(authorityTypes = AuthorityType.PRICE_RANGE)
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, @PathVariable Integer type) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		PageSupport<PriceRange> ps = priceRangeService.getPriceRange(curPageNO, shopId, type);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("type", type);
		return PathResolver.getPath(ReportFrontPage.PRICE_RANGE);
	}

	/**
	 * 保存价格区间
	 * 
	 * @param request
	 * @param response
	 * @param priceRangeDto
	 * @param type
	 * @return
	 */
	@RequestMapping(value = "/save")
	@Authorize(authorityTypes = AuthorityType.PRICE_RANGE)
	@ResponseBody
	public String save(HttpServletRequest request, HttpServletResponse response, PriceRangeDto priceRangeDto, Integer type) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		if (AppUtils.isNotBlank(priceRangeDto) && AppUtils.isNotBlank(priceRangeDto.getPriceRangeList())) {
			priceRangeService.savePriceRangeList(priceRangeDto.getPriceRangeList(), shopId);
		}else{
			return "参数错误,请刷新重试";
		}
		return Constants.SUCCESS;
	}

	/**
	 * 删除价格区间
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete")
	@Authorize(authorityTypes = AuthorityType.PRICE_RANGE)
	public @ResponseBody String delete(HttpServletRequest request, HttpServletResponse response, Long id) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		if (AppUtils.isNotBlank(id)) {
			PriceRange priceRange = priceRangeService.getPriceRange(id);
			if (AppUtils.isNotBlank(priceRange) && shopId.equals(priceRange.getShopId())) {
				priceRangeService.deletePriceRange(priceRange);
				return Constants.SUCCESS;
			} else {
				return Constants.FAIL;
			}
		} else {
			return Constants.FAIL;
		}
	}
}
