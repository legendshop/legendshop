/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 装修重定向页面定义
 */
public enum DecorateRedirectPage implements PageDefinition {
	
	/** 导航列表. */
	SHOP_NAV_LIST("/s/shopNav/list"),
	
	/** 轮播图列表. */
	SHOP_BANNER_LIST("/s/shopBanner/list"),
	
	;

	/** The value. */
	private final String value;

	/**
	 * 构造器.
	 *
	 * @param value the value
	 * @param template the template
	 */
	private DecorateRedirectPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/* (non-Javadoc)
	 * @see com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.String, com.legendshop.core.constant.PageDefinition)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("redirect:", path);
	}

	/**
	 * 实例化一个新的tiles页面
	 * 
	 * @param value
	 */
	private DecorateRedirectPage(String value) {
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}
	
}
