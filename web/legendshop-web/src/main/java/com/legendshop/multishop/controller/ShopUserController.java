/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.controller.HomePageController;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.ShopRole;
import com.legendshop.model.entity.ShopUser;
import com.legendshop.multishop.page.ShopFrontPage;
import com.legendshop.multishop.page.ShopRedirectPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ShopRoleService;
import com.legendshop.spi.service.ShopUserService;
import com.legendshop.util.AppUtils;

/**
 * 卖家中心 员工管理
 *
 */
@Controller
@RequestMapping("/s/shopUsers")
public class ShopUserController extends BaseController {
	
	private final Logger logger = LoggerFactory.getLogger(ShopUserController.class);

	@Autowired
	private ShopRoleService shopRoleService;

	@Autowired
	private ShopUserService shopUserService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	/**
	 * 进入员工管理页面
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/query")
	@Authorize(authorityTypes = AuthorityType.SHOP_USERS)
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		PageSupport<ShopUser> ps = shopUserService.queryShopUser(curPageNO, shopId);
		PageSupportHelper.savePage(request, ps);

		return PathResolver.getPath(ShopFrontPage.SHOPUSER_LIST);
	}

	/**
	 * 加载员工管理编辑页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/load")
	@Authorize(authorityTypes = AuthorityType.SHOP_USERS)
	public String load(HttpServletRequest request, HttpServletResponse response) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		List<ShopRole> shopRoles = shopRoleService.getShopRolesByShopId(shopId);
		request.setAttribute("shopRoles", shopRoles);
		return PathResolver.getPath(ShopFrontPage.SHOPUSER);
	}

	/**
	 * 查看员工管理详情
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param uType    修改类型(基本信息或密码)
	 * @return
	 */
	@RequestMapping("/load/{id}/{uType}")
	@Authorize(authorityTypes = AuthorityType.SHOP_USERS)
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Integer uType) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		ShopUser shopUser = shopUserService.getShopUser(id);
		if (shopUser == null || !shopId.equals(shopUser.getShopId())) {
			throw new BusinessException("shop user:" + id + " is not ower to shop " + shopId);
		}
		request.setAttribute("shopUser", shopUser);
		List<ShopRole> shopRoles = shopRoleService.getShopRolesByShopId(shopId);
		request.setAttribute("shopRoles", shopRoles);
		request.setAttribute("uType", uType);
		return PathResolver.getPath(ShopFrontPage.SHOPUSER);
	}

	/**
	 * 修改密码页面
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/loadPwd/{id}")
	@Authorize(authorityTypes = AuthorityType.SHOP_USERS)
	public String loadPwd(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		ShopUser shopUser = shopUserService.getShopUser(id);
		if (shopUser == null || !shopId.equals(shopUser.getShopId())) {
			throw new BusinessException("shop user:" + id + " is not ower to shop " + shopId);
		}
		request.setAttribute("shopUser", shopUser);
		return PathResolver.getPath(ShopFrontPage.UPDATEPWD);
	}

	@RequestMapping("/updatePwd")
	@Authorize(authorityTypes = AuthorityType.SHOP_USERS)
	public String updatePwd(HttpServletRequest request, HttpServletResponse response, ShopUser shopUser) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		ShopUser orinShopUser = shopUserService.getShopUser(shopUser.getId());
		if (orinShopUser == null || !shopId.equals(orinShopUser.getShopId())) {
			throw new BusinessException("shop user:" + shopUser.getId() + " is not ower to shop " + shopId);
		}

		orinShopUser.setPassword(passwordEncoder.encode(shopUser.getPassword()));

		orinShopUser.setModifyDate(new Date());
		shopUserService.updateShopUser(orinShopUser);

		return PathResolver.getPath(ShopRedirectPage.SHOPUSER_LIST);
	}

	/**
	 * 停止使用
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/stopUse/{id}")
	@ResponseBody
	@Authorize(authorityTypes = AuthorityType.SHOP_USERS)
	public String stopUse(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		ShopUser shopUser = shopUserService.getShopUser(id);
		if (shopUser == null || !shopId.equals(shopUser.getShopId())) {
			return Constants.FAIL;
		}
		shopUser.setEnabled(false);
		shopUser.setModifyDate(new Date());
		shopUserService.updateShopUser(shopUser);
		return Constants.SUCCESS;
	}

	/**
	 * 启用员工
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/beUse/{id}")
	@ResponseBody
	@Authorize(authorityTypes = AuthorityType.SHOP_USERS)
	public String beUse(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		ShopUser shopUser = shopUserService.getShopUser(id);
		if (shopUser == null || !shopId.equals(shopUser.getShopId())) {
			return Constants.FAIL;
		}
		shopUser.setEnabled(true);
		shopUser.setModifyDate(new Date());
		shopUserService.updateShopUser(shopUser);
		return Constants.SUCCESS;
	}

	/**
	 * 保存
	 * 
	 * @param request
	 * @param response
	 * @param shopUser
	 * @return
	 */
	@RequestMapping("/save")
	@Authorize(authorityTypes = AuthorityType.SHOP_USERS)
	public String save(HttpServletRequest request, HttpServletResponse response, ShopUser shopUser) {
		if (AppUtils.isBlank(shopUser.getName())) {
			throw new BusinessException("save shop user error, name can not be empty");
		}


		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		if (shopUser.getId() != null) {
			ShopUser orinShopUser = shopUserService.getShopUser(shopUser.getId());
			if (orinShopUser == null || !shopId.equals(orinShopUser.getShopId())) {
				throw new BusinessException("shop user:" + shopUser.getId() + " is not ower to shop " + shopId);
			}
			orinShopUser.setName(shopUser.getName());
			if (AppUtils.isNotBlank(shopUser.getPassword())) {
				orinShopUser.setPassword(passwordEncoder.encode(shopUser.getPassword()));
			}
			orinShopUser.setRealName(shopUser.getRealName());
			orinShopUser.setShopRoleId(shopUser.getShopRoleId());
			orinShopUser.setModifyDate(new Date());
			shopUserService.updateShopUser(orinShopUser);
		} else {
			shopUser.setPassword(passwordEncoder.encode(shopUser.getPassword()));
			shopUser.setShopId(shopId);
			shopUser.setEnabled(true);
			shopUser.setRecDate(new Date());
			shopUser.setModifyDate(new Date());
			shopUserService.saveShopUser(shopUser);
		}
		return PathResolver.getPath(ShopRedirectPage.SHOPUSER_LIST);
	}

	/**
	 * 检查是否重名
	 * 
	 * @param request
	 * @param response
	 * @param name
	 * @return
	 */
	@RequestMapping("/isNameExist")
	@ResponseBody
	public Boolean isNameExist(HttpServletRequest request, HttpServletResponse response, String name) {
		if (AppUtils.isNotBlank(name)) {
			SecurityUserDetail user = UserManager.getUser(request);
			ShopUser shopUser = shopUserService.getShopUserByNameAndShopId(name, user.getShopId());
			if (shopUser == null) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 删除
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/del/{id}")
	@ResponseBody
	@Authorize(authorityTypes = AuthorityType.SHOP_USERS)
	public String del(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		ShopUser shopUser = shopUserService.getShopUser(id);
		if (shopUser == null || !shopId.equals(shopUser.getShopId())) {
			logger.error("No right to access shopUser by shopId {}", shopId);
			return Constants.FAIL;
		}
		shopUserService.deleteShopUser(shopUser);
		return Constants.SUCCESS;
	}

}
