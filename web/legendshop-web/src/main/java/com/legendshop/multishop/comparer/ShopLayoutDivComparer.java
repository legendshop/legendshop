/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.comparer;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.dto.shopDecotate.ShopLayoutDivDto;
import com.legendshop.model.entity.shopDecotate.ShopLayoutDiv;
import com.legendshop.util.AppUtils;

/**
 * 自定义店铺布局比较器.
 *
 */
public class ShopLayoutDivComparer implements DataComparer<ShopLayoutDivDto, ShopLayoutDiv> {

	/**
	 * 是否需要更新
	 * @see
	 * java.lang.Object, java.lang.Object)
	 */
	@Override
	public boolean needUpdate(ShopLayoutDivDto dto, ShopLayoutDiv dbObj, Object obj) {
		int num = 0;
		if (isChage(dto.getLayoutModuleType(), dbObj.getLayoutModuleType())) {
			dbObj.setLayoutModuleType(dto.getLayoutModuleType());
			num++;
		}
		if (isChage(dto.getLayoutContent(), dbObj.getLayoutContent())) {
			dbObj.setLayoutContent(dto.getLayoutContent());
			num++;
		}
		if (num > 0) {
			return true;
		}

		return false;
	}

	/**
	 * 是否有改变 *.
	 *
	 * @param a
	 * @param b
	 * @return true, if checks if is chage
	 */
	private boolean isChage(Object a, Object b) {
		if (AppUtils.isNotBlank(a) && AppUtils.isNotBlank(b)) {
			if (!a.equals(b)) {
				return true;
			} else {
				return false;
			}
		} else if (AppUtils.isBlank(a) && AppUtils.isBlank(b)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 是个否已存在
	 * @see com.legendshop.base.compare.DataComparer#isExist(java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public boolean isExist(ShopLayoutDivDto dto, ShopLayoutDiv dbObj) {

		if (dto.getLayoutDivId() == null && dbObj.getLayoutDivId() == null) {
			return true;
		}
		if (dto.getLayoutDivId() == null || dbObj.getLayoutDivId() == null) {
			return false;
		}
		return dto.getLayoutDivId().equals(dbObj.getLayoutDivId());
	}

	/**
	 * 复制配置
	 * com.legendshop.core.compare.DataComparer#copyProperties(java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public ShopLayoutDiv copyProperties(ShopLayoutDivDto dtoj, Object obj) {
		ShopLayoutDiv shopLayoutDiv = new ShopLayoutDiv();
		shopLayoutDiv.setLayoutDiv(dtoj.getLayoutDiv());
		shopLayoutDiv.setLayoutDivId(dtoj.getLayoutDivId());
		shopLayoutDiv.setLayoutId(dtoj.getLayoutId());
		shopLayoutDiv.setLayoutType(dtoj.getLayoutType());
		shopLayoutDiv.setShopDecotateId(dtoj.getShopDecotateId());
		shopLayoutDiv.setShopId(dtoj.getShopId());
		return shopLayoutDiv;
	}

}
