/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.base.BaseController;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.ProductConsultDto;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ProductConsultService;

/**
 * 客户服务控制器.
 */
@Controller
public class CustomerController extends BaseController {

	@Autowired
	private ProductConsultService productConsultService;

	/**
	 * 商家删除回复.
	 *
	 * @param request
	 * @param response
	 * @param consId
	 * @return the string
	 */
	@RequestMapping(value = "/s/deleteReply/{consId}")
	public @ResponseBody String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long consId) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		int result = productConsultService.deleteProductConsult(userId, consId);
		if (result == 0) {
			return Constants.FAIL;
		} else {
			return Constants.SUCCESS;
		}
	}


	/**
	 * 商品咨询回复.
	 *
	 * @param request
	 * @param response
	 * @param consult
	 * @return the string
	 */
	@RequestMapping(value = "/s/consultSave")
	public @ResponseBody String consultSave(HttpServletRequest request, HttpServletResponse response, ProductConsultDto consult) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		int result = productConsultService.updateConsult(userId, consult);
		if (result == 0) {
			return Constants.FAIL;
		} else {
			return Constants.SUCCESS;
		}
	}

}
