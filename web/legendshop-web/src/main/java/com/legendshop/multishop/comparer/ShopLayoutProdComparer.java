/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.comparer;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.dto.shopDecotate.ShopLayoutProdDto;
import com.legendshop.model.entity.shopDecotate.ShopLayoutProd;
import com.legendshop.util.AppUtils;

/**
 * 店铺商品比较器
 *
 */
public class ShopLayoutProdComparer implements DataComparer<ShopLayoutProdDto, ShopLayoutProd> {

	/**
	 * 是否需要更新
	 */
	@Override
	public boolean needUpdate(ShopLayoutProdDto dto, ShopLayoutProd dbObj, Object obj) {
		if (isChage(dto.getSeq(), dbObj.getSeq())) {
			dbObj.setSeq(dto.getSeq());
			return true;
		}
		return false;
	}

	/** 是否有改变 **/
	private boolean isChage(Object a, Object b) {
		if (AppUtils.isNotBlank(a) && AppUtils.isNotBlank(b)) {
			if (!a.equals(b)) {
				return true;
			} else {
				return false;
			}
		} else if (AppUtils.isBlank(a) && AppUtils.isBlank(b)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 是否已存在
	 */
	@Override
	public boolean isExist(ShopLayoutProdDto dto, ShopLayoutProd dbObj) {
		if (AppUtils.isBlank(dto.getLayoutProdId())) {
			return false;
		}
		if (dto.getLayoutProdId().equals(dbObj.getId())) {
			return true;
		}
		return false;
	}

	/**
	 * 复制配置
	 */
	@Override
	public ShopLayoutProd copyProperties(ShopLayoutProdDto dtoj, Object obj) {
		ShopLayoutProd layoutProd = new ShopLayoutProd();
		layoutProd.setId(dtoj.getLayoutProdId());
		layoutProd.setLayoutId(dtoj.getLayoutId());
		layoutProd.setShopId(dtoj.getShopId());
		layoutProd.setShopDecotateId(dtoj.getShopDecotateId());
		layoutProd.setSeq(dtoj.getSeq() == null ? 0 : dtoj.getSeq());
		layoutProd.setProdId(dtoj.getProdId());
		return layoutProd;
	}

}
