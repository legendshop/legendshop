/*
 * LegendShop 多用户商城系统
 *  版权所有,并保留所有权利。
 */
package com.legendshop.multishop.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.legendshop.base.util.PageUtils;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.appdecorate.DecorateProdDto;
import com.legendshop.model.dto.appdecorate.DecorateShopCategoryDto;
import com.legendshop.model.entity.Attachment;
import com.legendshop.model.entity.AttachmentTree;
import com.legendshop.model.entity.Product;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.AttachmentTreeService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.ShopCategoryService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.uploader.service.AttachmentService;
import com.legendshop.util.AppUtils;

/**
 * 商家-移动店铺装修服务中心
 */
@RestController
@RequestMapping("/s/decorate/action")
public class AppShopIndexDecorateCenterController{
	
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(AppShopIndexDecorateCenterController.class);

	@Autowired
	private ProductService productService;
	
	@Autowired
	private ShopCategoryService shopCategoryService;
	
	@Autowired
	private AttachmentTreeService attachmentTreeService;
	
	@Autowired
	private AttachmentService attachmentService;
	
	@Autowired
	private AttachmentManager attachmentManager;
	
	
	/**
	 * 挑选商品弹层
	 */
	@PostMapping("/prodList")
	public ResultDto<PageSupport<DecorateProdDto>> prodList(HttpServletRequest request,String curPageNO,String prodName) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<Product> ps = productService.getProductList(prodName, curPageNO, shopId);
		
		PageSupport<DecorateProdDto> convertPageSupport = PageUtils.convert(ps, new PageUtils.ResultListConvertor<Product, DecorateProdDto>() {

			@Override
			public List<DecorateProdDto> convert(List<Product> form) {

				List<DecorateProdDto> toList = new ArrayList<DecorateProdDto>();
				for (Product product : form) {
					
					DecorateProdDto decorateProdDto = convertToDecorateProdDto(product);
					toList.add(decorateProdDto);
				}
				return toList;
			}
		});
		return ResultDtoManager.success(convertPageSupport);
	}	
	
	
	/**
	 * 挑选店铺分类弹层
	 */
	@PostMapping("/category")
	public ResultDto<List<DecorateShopCategoryDto>> category(HttpServletRequest request,String curPageNO,String prodName) {
		
		try {
			
			SecurityUserDetail user = UserManager.getUser(request);
			Long shopId = user.getShopId();
			
			List<DecorateShopCategoryDto> shopCategoryList = shopCategoryService.getShopCategoryDtoList(shopId);
			
			//组装树结构
			List<DecorateShopCategoryDto> tree = getTree(0L,shopCategoryList);
	    	
			return ResultDtoManager.success(tree);
			
		} catch (Exception e) {
			log.error("获取分类数据异常",e);
		}
		return ResultDtoManager.fail();
	}	
	
	/**
	 * 上传商品图片文件
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@PostMapping(value = "/uploadPicture")
	public ResultDto<Object> uploadPicture(MultipartHttpServletRequest request, @RequestParam("files[]") MultipartFile[] files) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isNotBlank(files)) {
			try {
				for (MultipartFile file : files) {
					if (file.getSize() > 0) {
						String pic = attachmentManager.upload(file);
						attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), pic, file, AttachmentTypeEnum.ADVERTISEMENT);
					}
				}
				return ResultDtoManager.success();
			} catch (Exception e) {
				ResultDtoManager.fail();
			}
		} else {
			return ResultDtoManager.fail("请上传图片");
		}
		return ResultDtoManager.fail();
	}

	
	
	
	
	/**
	 * 获取图片空间目录
	 */
	@PostMapping("/imagesTree")
	public ResultDto<List<AttachmentTree>> imagesTree(HttpServletRequest request) {
		
		try {
			SecurityUserDetail user = UserManager.getUser(request);
			if(AppUtils.isBlank(user)){
				return null;
			}
			
			//组装树结构 
			List<AttachmentTree> attmntTrees = attachmentTreeService.getAttachmentTreeDtoByUserName(user.getUsername());
			
			if (AppUtils.isBlank(attmntTrees)) {
				
				AttachmentTree attachmentTree = new AttachmentTree();
				attachmentTree.setId(0l);
				attachmentTree.setName("根目录");
				attachmentTree.setParentId(0l);
				attachmentTree.setUserName(user.getUsername());
				attachmentTree.setIsParent(true);
				attachmentTree.setOpen(true);
				attmntTrees.add(attachmentTree);
			}
			
			return ResultDtoManager.success(attmntTrees);
		} catch (Exception e) {
			log.error("获取图片空间目录异常",e);
		}
		return ResultDtoManager.fail();
	}	
	

	/**
	 * 图片控件 获取目录下的图片
	 * @param treeId 树id
	 * @param curPageNO 当前页
	 * @param orderBy  排序
	 * @param searchName  搜索名称
	 */
	@PostMapping("/images")
	public ResultDto<PageSupport<Attachment>> images(HttpServletRequest request,Long treeId, String curPageNO,String orderBy,String searchName) {
		
		try {
			
			SecurityUserDetail user = UserManager.getUser(request);
			if(AppUtils.isBlank(user)){
				log.warn("visit img getPropChildImg, userId is null and return");
				return ResultDtoManager.fail();
			}
			Long shopId = user.getShopId();

			PageSupport<Attachment> ps = attachmentService.getPropChildImg(treeId, curPageNO, orderBy, searchName,shopId);
			
			return ResultDtoManager.success(ps);
		} catch (Exception e) {
			log.error("获取图片数据异常",e);
		}
		return ResultDtoManager.fail();
	}
	
	
	//递归遍历树
	private List<DecorateShopCategoryDto> getTree(Long pid ,List<DecorateShopCategoryDto> list){
		
		List<DecorateShopCategoryDto> tree = new ArrayList<DecorateShopCategoryDto>();
		
		for (DecorateShopCategoryDto shopCategory : list) {
			
			Long parentId = shopCategory.getParentId();
			if (shopCategory.getParentId() == null) {
				parentId = 0L;
			}
			if (pid.equals(parentId)) {
				shopCategory.setChildrenList(getTree(shopCategory.getId(), list));
                tree.add(shopCategory);
            }
		}
		return tree;
	}
	
	
	
	
	/**
	 * 转换弹层商品
	 * @param product
	 */
	private DecorateProdDto convertToDecorateProdDto(Product product){
		  DecorateProdDto decorateProdDto = new DecorateProdDto();
		  
		  decorateProdDto.setProdId(product.getProdId());
		  decorateProdDto.setSpuName(product.getName());
		  decorateProdDto.setPrice(product.getCash());
		  decorateProdDto.setPic(product.getPic());
		  return decorateProdDto;
	}
	
}
