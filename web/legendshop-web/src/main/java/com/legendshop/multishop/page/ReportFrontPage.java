/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 报表前台页面定义
 */
public enum ReportFrontPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),
	
	/** 价格区间列表 */
	PRICE_RANGE("/priceRangeList"),
	
	/** 店铺概况 */
	REPORT_SALES("/sales"),
	
	/** 热卖商品 */
	REPORT_PROD_ANALYSIS("/prodAnalysis"),
	
	/** 运营报告 */
	REPORT_SHOP_OPERATING("/operatingReport"),
	
	/** 商品分析 */
	REPORT_PROD_SALESTREND("/prodSalesTrend"),
	
	/** 流量分析 */
	REPORT_TRAFFIC_STATISTICS("/trafficStatistics"),
	
	/** 购买分析 */
	REPORT_PURCHASE_ANALYSIS("/purchaseAnalysis"),
	
	/** 购买分析 */
	REPORT_PURCHASE_TIME("/purchaseTime"),
	
	/** 区域分析 运营报告 */
	REPORT_REGIONAL_DISTRIBUTION("/regionalDistribution"),
	;

	/** The value. */
	private final String value;

	private ReportFrontPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath("report", path);
	}

	public String getNativeValue() {
		return value;
	}

}
