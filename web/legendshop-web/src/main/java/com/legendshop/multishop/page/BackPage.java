/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 后台页面定义.
 */
public enum BackPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 错误页面. */
	BACK_ERROR_PAGE(ERROR_PAGE_PATH),

	/** 商品类型 */
	CATEGORY_PROD_TYPE_PAGE("/category/prodType"),

	/** 商品价值 */
	PROD_PROP_VALUE("/productProperty/prodPropValue"),

	/** 产品品牌 */
	BRAND_PAGE("/brand/brandPage")

	;

	/** The value. */
	private final String value;

	/**
	 * 实例化一个新的后台页面
	 *
	 * @param value
	 */
	private BackPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculateBackendPath(path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
