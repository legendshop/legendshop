/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.multishop.controller;

import com.legendshop.base.util.XssFilterUtil;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.page.CommonFrontPage;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.RefundReturnStatusEnum;
import com.legendshop.model.constant.RefundReturnTypeEnum;
import com.legendshop.model.constant.SubHistoryEnum;
import com.legendshop.model.dto.AuditItemReturnDTO;
import com.legendshop.model.dto.order.RefundSearchParamDto;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubHistory;
import com.legendshop.model.entity.SubItem;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.multishop.page.ShopFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.*;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;
import com.legendshop.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 退款退货控制器.
 */
@Controller
public class ShopRefundReturnController {

	@Autowired
	private StockService stockService;

	@Autowired
	private SubItemService subItemService;

	@Autowired
	private SubHistoryService subHistoryService;

	@Autowired
	private SubRefundReturnService subRefundReturnService;

	@Autowired
	SubService subService;

	@Autowired
	private ShopDetailService shopDetailService;

	/**
	 * 跳转到退款,退货列表页面.
	 *
	 * @param request   the request
	 * @param response  the response
	 * @param applyType the apply type
	 * @return the string
	 */
	@RequestMapping("/s/refundReturnList/{applyType}")
	@Authorize(authorityTypes = {AuthorityType.ORDER_RETURN})
	public String refundReturnList(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer applyType) {
		request.setAttribute("applyType", applyType);
		return PathResolver.getPath(ShopFrontPage.REFUND_REFUND_LIST);
	}

	/**
	 * 获取退款列表内容.
	 *
	 * @param request   the request
	 * @param response  the response
	 * @param curPageNO the cur page no
	 * @param paramData the param data
	 * @return the string
	 */
	@RequestMapping("/s/refundList")
	@Authorize(authorityTypes = {AuthorityType.ORDER_RETURN})
	public String refundList(HttpServletRequest request, HttpServletResponse response, String curPageNO, RefundSearchParamDto paramData) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		PageSupport<SubRefundReturn> ps = subRefundReturnService.getSubRefundReturnPage(curPageNO, paramData, shopId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("paramData", paramData);
		return PathResolver.getPath(ShopFrontPage.REFUND_LIST);
	}

	/**
	 * 获取退货列表内容.
	 *
	 * @param request   the request
	 * @param response  the response
	 * @param curPageNO the cur page no
	 * @param paramData the param data
	 * @return the string
	 */
	@RequestMapping("/s/returnList")
	@Authorize(authorityTypes = {AuthorityType.ORDER_RETURN})
	public String returnList(HttpServletRequest request, HttpServletResponse response, String curPageNO, RefundSearchParamDto paramData) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		PageSupport<SubRefundReturn> ps = subRefundReturnService.getSubReturnListPage(curPageNO, paramData, shopId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("paramData", paramData);
		return PathResolver.getPath(ShopFrontPage.RETURN_LIST);
	}

	/**
	 * 设置查询退款,退货列表的条件.
	 *
	 * @param cq        CriteriaQuery对象
	 * @param shopId    the shop id
	 * @param applyType 申请类型: 退款,退货
	 * @param paramData 查询参数
	 */
	private void setSearchCondition(CriteriaQuery cq, Long shopId, Integer applyType, RefundSearchParamDto paramData) {
		cq.eq("shopId", shopId);
		cq.eq("applyType", applyType);
		if (AppUtils.isNotBlank(paramData.getSellerState())) {
			cq.eq("sellerState", paramData.getSellerState());
		}
		if (AppUtils.isNotBlank(paramData.getStartDate())) {
			cq.ge("applyTime", paramData.getStartDate());
		}
		if (AppUtils.isNotBlank(paramData.getEndDate())) {
			cq.le("applyTime", paramData.getEndDate());
		}
		if (AppUtils.isNotBlank(paramData.getNumber())) {
			if (RefundSearchParamDto.ORDER_NO.equals(paramData.getNumType())) {// 订单编号
				cq.like("subNumber", paramData.getNumber());
			} else if (RefundSearchParamDto.REFUND_NO.equals(paramData.getNumType())) {// 退款编号
				cq.like("refundSn", paramData.getNumber());
			}
		}
	}

	/**
	 * 查看退款详情.
	 *
	 * @param request  the request
	 * @param response the response
	 * @param refundId the refund id
	 * @return the string
	 */
	@RequestMapping("/s/refundDetail/{refundId}")
	@Authorize(authorityTypes = {AuthorityType.ORDER_RETURN})
	public String refundDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable Long refundId) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
		if (AppUtils.isBlank(subRefundReturn)) {
			request.setAttribute("message", "对不起,您要查看的退款记录不存在货已被删除!");
			return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
		}
		//七天的时间
		int sTime = 3600 * 24 * 7 * 1000;
		//剩余时间 = 七天时间-（当前时间-申请时间）；
		Long endTime = sTime - (new Date().getTime() - subRefundReturn.getApplyTime().getTime());

		if (!shopId.equals(subRefundReturn.getShopId())) {
			request.setAttribute("message", "对不起,您没有权限!");
			return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
		}

		if (RefundReturnStatusEnum.APPLY_FINISH.value().equals(Integer.parseInt(Long.toString(subRefundReturn.getApplyState())))) {
			endTime = 0L;
		}
		Sub sub = subService.getSubById(subRefundReturn.getSubId());
		request.setAttribute("subRefundReturn", subRefundReturn);
		request.setAttribute("endTime", endTime);
		request.setAttribute("returnFreightAmount", 0);
		StringBuffer sb = new StringBuffer();
		sb.append("定");
		sb.append(subRefundReturn.getDepositRefund());
		sb.append(" + 尾");
		sb.append(subRefundReturn.getRefundAmount());
		if (subRefundReturn.getRefundAmount() !=null && subRefundReturn.getRefundAmount().compareTo(BigDecimal.ZERO) == 1){//预售商品，定金尾款情况
			sb.append("  (运费" + sub.getFreightAmount() +")");
			request.setAttribute("returnFreightAmount", sub.getFreightAmount());
		}
		request.setAttribute("info", sb.toString());
		return PathResolver.getPath(ShopFrontPage.REFUND_DETAIL);
	}

	/**
	 * 查看退货详情.
	 *
	 * @param request  the request
	 * @param response the response
	 * @param refundId 退款,退货ID
	 * @return the string
	 */
	@RequestMapping("/s/returnDetail/{refundId}")
	@Authorize(authorityTypes = {AuthorityType.ORDER_RETURN})
	public String returnDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable Long refundId) {
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
		if (AppUtils.isBlank(subRefundReturn)) {
			request.setAttribute("message", "对不起,您要查看的退款记录不存在货已被删除!");
			return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
		}

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		if (!shopId.equals(subRefundReturn.getShopId())) {
			request.setAttribute("message", "对不起,您没有权限!");
			return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
		}

		// 待审核时获取默认退货地址
		if (subRefundReturn.getSellerState() == 1) {
			ShopDetail detail = shopDetailService.getShopDetailById(shopId);
			subRefundReturn.setReturnShopAddr(detail.getReturnShopAddr());
			subRefundReturn.setReturnProvinceId(detail.getReturnProvinceId());
			subRefundReturn.setReturnCityId(detail.getReturnCityId());
			subRefundReturn.setReturnAreaId(detail.getReturnAreaId());
			subRefundReturn.setReturnContact(detail.getContactName());
			subRefundReturn.setReturnPhone(detail.getContactMobile());
		}

		request.setAttribute("subRefundReturn", subRefundReturn);

		return PathResolver.getPath(ShopFrontPage.RETURN_DETAIL);
	}

	/**
	 * 审核用户申请订单退款.
	 *
	 * @param request       the request
	 * @param response      the response
	 * @param refundId      退款,退货ID
	 * @param isAgree       the is agree
	 * @param sellerMessage the seller message
	 * @return the string
	 */
	@RequestMapping("/s/auditRefund/{refundId}")
	@ResponseBody
	public String auditRefund(HttpServletRequest request, HttpServletResponse response, @PathVariable Long refundId, Boolean isAgree, String sellerMessage) {
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
		if (AppUtils.isBlank(subRefundReturn)) {
			return "对不起,您要操作的记录不存在!";
		}

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		if (!shopId.equals(subRefundReturn.getShopId())
				|| RefundReturnTypeEnum.REFUND.value() != subRefundReturn.getApplyType()) {
			return "非法操作!";
		}
		saveOrderHistoryMoney(subRefundReturn, isAgree);
		subRefundReturnService.shopAuditRefund(subRefundReturn, isAgree, sellerMessage, true);
		return Constants.SUCCESS;
	}


	/**
	 * 审核用户申请订单项退货.
	 *
	 * @param request       the request
	 * @param response      the response
	 * @param refundId      退款,退货ID
	 * @param auditItemReturnDTO       退款退货DTO
	 * @return the string
	 */
	@RequestMapping("/s/auditItemReturn/{refundId}")
	@ResponseBody
	public String auditItemReturn(HttpServletRequest request, HttpServletResponse response, @PathVariable Long refundId, AuditItemReturnDTO auditItemReturnDTO) {
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
		if (AppUtils.isBlank(subRefundReturn)) {
			return "对不起,您要操作的记录不存在!";
		}

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		if (!shopId.equals(subRefundReturn.getShopId())
				|| RefundReturnTypeEnum.REFUND_RETURN.value() != subRefundReturn.getApplyType()) {
			return "非法操作!";
		}
		Sub sub = new Sub();
		sub.setSubId(subRefundReturn.getSubId());
		saveOrderHistory(sub, auditItemReturnDTO.getIsAgree());

		// 保存退货地址信息
		subRefundReturn.setReturnDetailAddress(XssFilterUtil.cleanXSS(auditItemReturnDTO.getReturnDetailAddress()));
		subRefundReturn.setReturnContact(XssFilterUtil.cleanXSS(auditItemReturnDTO.getReturnContact()));
		subRefundReturn.setReturnPhone(XssFilterUtil.cleanXSS(auditItemReturnDTO.getReturnPhone()));

		subRefundReturnService.shopAuditReturn(subRefundReturn, auditItemReturnDTO.getIsAgree(), auditItemReturnDTO.getIsJettison(), auditItemReturnDTO.getSellerMessage(), true);
		return Constants.SUCCESS;
	}

	/**
	 * show收货窗口.
	 *
	 * @param request  the request
	 * @param response the response
	 * @param refundId 退款,退货ID
	 * @return the string
	 */
	@RequestMapping("/s/showReceive/{refundId}")
	public String showReceiveWin(HttpServletRequest request, HttpServletResponse response, @PathVariable Long refundId) {
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
		if (AppUtils.isBlank(subRefundReturn)) {
			request.setAttribute("message", "对不起,您要操作的退款记录不存在货已被删除!");
			return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
		}

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		if (!shopId.equals(subRefundReturn.getShopId())) {
			request.setAttribute("message", "对不起,您没有权限!");
			return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
		}
		request.setAttribute("refund", subRefundReturn);
		return PathResolver.getPath(ShopFrontPage.RECEIVE_WIN);
	}

	/**
	 * 处理收货.
	 *
	 * @param request        the request
	 * @param response       the response
	 * @param refundId       退款,退货ID
	 * @param goodsState     the goods state
	 * @param receiveMessage the receive message
	 * @return the string
	 */
	@RequestMapping("/s/receiveGoods/{refundId}")
	@ResponseBody
	public String receiveGoods(HttpServletRequest request, HttpServletResponse response, @PathVariable Long refundId, Integer goodsState,
							   String receiveMessage) {
		if (!RefundReturnStatusEnum.LOGISTICS_UNRECEIVED.value().equals(goodsState) && !RefundReturnStatusEnum.LOGISTICS_RECEIVED.value().equals(goodsState)) {
			return "非法操作!";
		}

		SecurityUserDetail user = UserManager.getUser(request);
		if (null == user) {
			return "非法操作!";
		}
		Long shopId = user.getShopId();


		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);

		if (AppUtils.isBlank(subRefundReturn)) {
			return "对不起,您要操作的记录不存在!";
		}
		if (!shopId.equals(subRefundReturn.getShopId())) {
			return "非法操作!";
		}
		if (subRefundReturn.getGoodsState() == goodsState) {
			return "非法操作!";
		}
		if (RefundReturnStatusEnum.LOGISTICS_UNRECEIVED.value().equals(goodsState)) {
			if (!DateUtils.getDay(subRefundReturn.getShipTime(), 5).before(new Date())) {
				return "非法操作!";
			}
		}
		subRefundReturn.setReceiveMessage(receiveMessage);
		try{
			subRefundReturnService.receiveGoods(goodsState,subRefundReturn);
		}catch (Exception e){
			e.printStackTrace();
			return Constants.FAIL;
		}

		return Constants.SUCCESS;
	}

	/**
	 * 商家处理退款订单日志
	 *
	 * @param sub
	 */
	private void saveOrderHistoryMoney(SubRefundReturn sub, Boolean rs) {
		SubHistory subHistory = new SubHistory();
		Date date = new Date();
		String time = subHistory.DateToString(date);
		subHistory.setRecDate(date);
		subHistory.setSubId(sub.getSubId());
		subHistory.setStatus(SubHistoryEnum.AGREED_RETURNMONEY.value());
		StringBuilder sb = new StringBuilder();
		sb.append("[退货/退款]商家").append("于").append(time).append("商家" + (rs == true ? "同意" : "不同意") + "退款 ");
		subHistory.setUserName("商家");
		subHistory.setReason(sb.toString());
		subHistoryService.saveSubHistory(subHistory);
	}


	/**
	 * 商家同意退货退款订单日志
	 *
	 * @param sub
	 */
	private void saveOrderHistory(Sub sub, Boolean rs) {
		SubHistory subHistory = new SubHistory();
		Date date = new Date();
		String time = subHistory.DateToString(date);
		subHistory.setRecDate(date);
		subHistory.setSubId(sub.getSubId());
		subHistory.setStatus(SubHistoryEnum.AGREED_RETURNGOOD.value());
		StringBuilder sb = new StringBuilder();
		sb.append("[退货/退款]商家").append("于").append(time).append("商家").append(rs ? "同意" : "不同意").append("退货退款");
		subHistory.setUserName("商家");
		subHistory.setReason(sb.toString());
		subHistoryService.saveSubHistory(subHistory);
	}

}
