/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ShopAppDecorateStatusEnum;
import com.legendshop.model.entity.ShopAppDecorate;
import com.legendshop.multishop.page.ShopFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ShopAppDecorateService;
import com.legendshop.util.AppUtils;

/**
 * 卖家中心 移动端店铺首页装修
 *
 */
@Controller
@RequestMapping("/s/appShopIndexDecorate")
public class AppShopIndexDecorateController extends BaseController {
	
	@Autowired
	private ShopAppDecorateService shopAppDecorateService;

	
	
	
	/**
	 * 装修页面
	 */
	@RequestMapping("/decorate")
	public String decorate(HttpServletRequest request, HttpServletResponse response) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		ShopAppDecorate shopAppDecorate = shopAppDecorateService.getShopAppDecorateByShopId(shopId);

		if (AppUtils.isBlank(shopAppDecorate)) {
			shopAppDecorate = new ShopAppDecorate();
			shopAppDecorate.setData("{\"backgroundType\":\"\",\"background\":\"\",\"components\":[]}");
		}
		
		request.setAttribute("shopAppDecorate", shopAppDecorate);
		return PathResolver.getPath(ShopFrontPage.APP_SHOP_INDEX_DECORATE);
	}
	
	
	
	/**
	 * 保存装修
	 * @param decotateData 装修数据
	 */
	@RequestMapping(value="/save",method=RequestMethod.POST)
	@ResponseBody
	public String save(HttpServletRequest request, HttpServletResponse response,String decotateData){
		
		if(AppUtils.isBlank(decotateData)){
			return "请先装修任意楼层";
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		ShopAppDecorate shopAppDecorate = shopAppDecorateService.getShopAppDecorateByShopId(shopId);
		
		if(AppUtils.isNotBlank(shopAppDecorate)){ // 编辑
			shopAppDecorate.setData(decotateData);
			shopAppDecorate.setRecDate(new Date());
			
			if (shopAppDecorate.getStatus() != ShopAppDecorateStatusEnum.PLATFORM_OFF_LINE.value() ) {//如果是平台下线，保存不修改状态
				shopAppDecorate.setStatus(ShopAppDecorateStatusEnum.MODIFIED.value());
			}
			shopAppDecorateService.updateShopAppDecorate(shopAppDecorate);
		}else{ // 新建
			shopAppDecorate = new ShopAppDecorate();
			shopAppDecorate.setShopId(shopId);
			shopAppDecorate.setStatus(ShopAppDecorateStatusEnum.OFF_LINE.value());
			shopAppDecorate.setData(decotateData);
			shopAppDecorate.setRecDate(new Date());
			shopAppDecorateService.saveShopAppDecorate(shopAppDecorate);
		}
		return Constants.SUCCESS;
	}
	
	/**
	 * 发布页面装修
	 * @param category 页面类型
	
	 */
	@RequestMapping(value="/release",method=RequestMethod.POST)
	@ResponseBody
	public String release(HttpServletRequest request, HttpServletResponse response,String decotateData){
		
		if(AppUtils.isBlank(decotateData)){
			return "请先装修任意楼层";
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		ShopAppDecorate shopAppDecorate = shopAppDecorateService.getShopAppDecorateByShopId(shopId);
		
		if(AppUtils.isNotBlank(shopAppDecorate)){ // 编辑
			shopAppDecorate.setData(decotateData);
			shopAppDecorate.setReleaseData(decotateData);// 更新已发布装修数据
			shopAppDecorate.setRecDate(new Date());
			shopAppDecorate.setStatus(ShopAppDecorateStatusEnum.ON_LINE.value());
			shopAppDecorateService.updateShopAppDecorate(shopAppDecorate);
		}else{ // 新建
			shopAppDecorate = new ShopAppDecorate();
			
			shopAppDecorate.setShopId(shopId);
			shopAppDecorate.setStatus(ShopAppDecorateStatusEnum.ON_LINE.value());
			shopAppDecorate.setData(decotateData);
			shopAppDecorate.setReleaseData(decotateData);//发布装修数据
			shopAppDecorate.setRecDate(new Date());
			shopAppDecorateService.saveShopAppDecorate(shopAppDecorate);
		
		}
		return Constants.SUCCESS;
	}
	




}
