/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ShopPermFunctionEnum;
import com.legendshop.model.entity.ShopRole;
import com.legendshop.model.entity.ShopUser;
import com.legendshop.model.security.ShopMenu;
import com.legendshop.model.security.ShopMenuGroup;
import com.legendshop.multishop.page.ShopFrontPage;
import com.legendshop.multishop.page.ShopRedirectPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ShopPermService;
import com.legendshop.spi.service.ShopRoleService;
import com.legendshop.spi.service.ShopUserService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.StringUtil;

/**
 * 卖家中心 职位管理
 *
 */
@Controller
@RequestMapping("/s/shopJobs")
public class ShopJobsController extends BaseController {

	@Autowired
	private ShopPermService shopPermService;

	@Autowired
	private ShopRoleService shopRoleService;

	@Autowired
	private ShopUserService shopUserService;

	@Resource
	private List<ShopMenuGroup> shopMenuGroups;

	/**
	 * 进入职位管理页面
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/query")
	@Authorize(authorityTypes = AuthorityType.SHOP_JOBS)
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<ShopRole> ps = shopRoleService.getShopRolePage(curPageNO, shopId);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(ShopFrontPage.SHOPJOBS_LIST);
	}

	/**
	 * 加载设置职位权限页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/load")
	@Authorize(authorityTypes = AuthorityType.SHOP_JOBS)
	public String load(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("shopMenuGroups", shopMenuGroups);
		return PathResolver.getPath(ShopFrontPage.SHOPJOB);
	}

	/**
	 * 查看职位权限详情
	 * @param request
	 * @param response
	 * @param shopRoleId
	 * @return
	 */
	@RequestMapping("/load/{shopRoleId}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long shopRoleId) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		ShopRole shopRole = shopRoleService.getShopRole(shopRoleId);

		if (shopRole == null) {
			throw new BusinessException("shopRole:" + shopRoleId + " is not exist");
		}

		if (!shopId.equals(shopRole.getShopId())) {
			throw new BusinessException("shopRole:" + shopRoleId + " is not ower to shop " + shopId);
		}

		List<String> shopPermList = shopPermService.getShopPermsByRoleId(shopRoleId);

		List<ShopMenuGroup> menuGroups = new ArrayList<ShopMenuGroup>();

		for (ShopMenuGroup group : shopMenuGroups) {
			ShopMenuGroup newGroup = new ShopMenuGroup();
			newGroup.setName(group.getName());
			newGroup.setLabel(group.getLabel());
			List<ShopMenu> shopMenuList = new ArrayList<ShopMenu>();
			for (ShopMenu menu : group.getShopMenuList()) {
				ShopMenu newMenu = new ShopMenu();
				newMenu.setName(menu.getName());
				newMenu.setLabel(menu.getLabel());
				newMenu.setUrlList(menu.getUrlList());
				newMenu.setEditorUrlList(menu.getEditorUrlList());
				if (shopPermList.contains(menu.getLabel())) {
					newMenu.setSelected(true);
					newGroup.setSelected(true);
					
					String function = shopPermService.getFunction(shopRoleId,menu.getLabel());
					if (AppUtils.isNotBlank(function)) {
						if (function.contains(ShopPermFunctionEnum.CHECK_FUNC.value())) {
							newMenu.setCheckFunc(true);
						}
						if (function.contains(ShopPermFunctionEnum.EDITOR_FUNC.value())) {
							newMenu.setEditorFunc(true);
						}
					}
				}
				shopMenuList.add(newMenu);
			}
			newGroup.setShopMenuList(shopMenuList);
			menuGroups.add(newGroup);
		}

		request.setAttribute("shopMenuGroups", menuGroups);
		request.setAttribute("shopRole", shopRole);
		return PathResolver.getPath(ShopFrontPage.SHOPJOB);
	}

	/**
	 * 保存设置职位权限
	 * @param request
	 * @param response
	 * @param shopRole
	 * @return
	 */
	@RequestMapping("/save")
	@Authorize(authorityTypes = AuthorityType.SHOP_JOBS)
	public String save(HttpServletRequest request, HttpServletResponse response, ShopRole shopRole) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		String name = shopRole.getName();
		if (AppUtils.isBlank(name)) {
			throw new BusinessException("name can not be empty");
		}
		shopRoleService.saveJobRole(shopRole,shopMenuGroups,shopId);
		return PathResolver.getPath(ShopRedirectPage.SHOPJOBS_LIST);
	}

	/**
	 * 判断是否重名
	 * @param request
	 * @param response
	 * @param name
	 * @return
	 */
	@RequestMapping("/isNameExist")
	@ResponseBody
	public Boolean isNameExist(HttpServletRequest request, HttpServletResponse response, String name) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		ShopRole shopRole = shopRoleService.getShopRole(shopId, name);
		if (shopRole != null) {
			return true;
		}
		return false;
	}

	/**
	 * 删除
	 * @param request
	 * @param response
	 * @param shopRoleId
	 * @return
	 */
	@RequestMapping("/del/{shopRoleId}")
	@ResponseBody
	@Authorize(authorityTypes = AuthorityType.SHOP_JOBS)
	public String del(HttpServletRequest request, HttpServletResponse response, @PathVariable Long shopRoleId) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		ShopRole shopRole = shopRoleService.getShopRole(shopRoleId);
		if (shopRole == null || !shopId.equals(shopRole.getShopId())) {
			return Constants.FAIL;
		}
		List<ShopUser> shopUsers = shopUserService.getShopUseRoleId(shopRoleId);
		if (AppUtils.isNotBlank(shopUsers)) {
			return "hasShopUsers";
		}
		shopRoleService.deleteShopRole(shopRole);
		
		return Constants.SUCCESS;
	}

}
