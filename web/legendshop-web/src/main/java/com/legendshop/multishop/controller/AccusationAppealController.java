/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.AccusationAppeal;
import com.legendshop.spi.service.AccusationAppealService;

/**
 * 商家上诉控制器
 *
 */
@Controller
@RequestMapping("/s/accusationAppeal")
public class AccusationAppealController extends BaseController{
	
	@Autowired
	private AccusationAppealService accusationAppealService;

	/**
	 * 查询商家上诉列表
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, AccusationAppeal accusationAppeal) {
		PageSupport<AccusationAppeal> ps = accusationAppealService.getAccusationAppealPage(curPageNO);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("accusationAppeal", accusationAppeal);

		return "/accusationAppeal/accusationAppealList";
	}

	/**
	 * 保存商家上诉
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, AccusationAppeal accusationAppeal) {
		accusationAppealService.saveAccusationAppeal(accusationAppeal);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return "forward:/s/accusationAppeal/query.htm";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// FowardPage.ACCUSATIONAPPEAL_LIST_QUERY);
	}

	/**
	 * 删除商家上诉
	 */
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		AccusationAppeal accusationAppeal = accusationAppealService.getAccusationAppeal(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// accusationAppeal.getUserName());
		// if(result!=null){
		// return result;
		// }
		accusationAppealService.deleteAccusationAppeal(accusationAppeal);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return "forward:/s/accusationAppeal/query.htm";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// FowardPage.ACCUSATIONAPPEAL_LIST_QUERY);
	}

	/**
	 * 获取商家上诉详情
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		AccusationAppeal accusationAppeal = accusationAppealService.getAccusationAppeal(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// accusationAppeal.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("accusationAppeal", accusationAppeal);
		return "/accusationAppeal/accusationAppeal";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.ACCUSATIONAPPEAL_EDIT_PAGE);
	}

	/**
	 * 加载商家上诉编辑页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return "/accusationAppeal/accusationAppeal";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.ACCUSATIONAPPEAL_EDIT_PAGE);
	}
	
	/**
	 * 更新商家上诉
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		AccusationAppeal accusationAppeal = accusationAppealService.getAccusationAppeal(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// accusationAppeal.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("accusationAppeal", accusationAppeal);
		return "forward:/s/accusationAppeal/query.htm";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.ACCUSATIONAPPEAL_EDIT_PAGE);
	}

}
