/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 店铺重定向页面定义
 */
public enum ShopRedirectPage implements PageDefinition {
	
	/** 登录 */
	LOGIN("/login"),
	
	/** 商家的产品类目 */
	SHOPCAT_QUERY("/s/shopCategory"),
	
	/** 支付类型 */
	PAYTYPE_QUERY("/s/payType"),
	
	/** 标签管理 */
	TAG_MANAGE("/s/tagManage"),
	
	/** 运费模板 */
	TRANSPORT("/s/transport"),
	
	/** 三级商品类目 */
	SUBSHOPCAT_QUERY("/s/subShopCat/query"),
		
	/** 二级商品类目 */
	NEXTSHOPCAT_QUERY("/s/nextShopCat/query"),
	
	/** 导航设置 */
	NAVIGATION_SETTING("/s/navigationSetting"),
	
	/** 品牌申请 */
	APPLY_BRAND("/s/applyBrand"),
	
	/**  */
	PRODUCT_VIEWS_QUERY("/views"),
	
	/** 配送方式 */
	DELIVERYTYPE_LIST_QUERY("/s/deliveryType"),
	
	/** 打印模板 */
	PRINTTMPL_LIST_QUERY("/s/printTmpl/query"),
	
	/** 卖家中心首页 */
	SELLER_HOME("/sellerHome"),
	
	/** 销售中的商品 */
	SELLING_PROD_QUERY("/s/sellingProd"),
	
	/** 订单列表 */
	ORDERSMANAGE_QUERY("/s/ordersManage"),
	
	/** 开店页面 */
	SHOP_AGREEMENT("/p/shopAgreement"), 
	
	/** 职位管理 */
	SHOPJOBS_LIST("/s/shopJobs/query"),
	
	/** 员工管理 */
	SHOPUSER_LIST("/s/shopUsers/query")

	;

	/** The value. */
	private final String value;

	private ShopRedirectPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("redirect:", path);
	}

	/**
	 * Instantiates a new tiles page.
	 * 
	 * @param value
	 *            the value
	 */
	private ShopRedirectPage(String value) {
		this.value = value;
	}

	public String getNativeValue() {
		return value;
	}

}
