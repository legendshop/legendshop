/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONArray;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.core.utils.ExportExcelUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.*;
import com.legendshop.model.dto.order.ProdExcelDTO;
import com.legendshop.model.dto.order.TenderProdExcelDTO;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.Sku;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.SkuService;
import com.legendshop.util.EnvironmentConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.base.helper.FileProcessor;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.POIReadeExcelUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProductImport;
import com.legendshop.multishop.page.ShopFrontPage;
import com.legendshop.multishop.util.CSVReaderUtil;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ProductImportService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 商品导入动作.
 */
@Controller
public class ProdImportController extends BaseController {

	private final Logger log = LoggerFactory.getLogger(ProdImportController.class);

	@Autowired
	private ProductImportService productImportService;

    @Autowired
    private ProductService productService;

    @Autowired
    private SkuService skuService;


    /**
	 * 商品导入列表.
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param productImport
	 * @return the string
	 */
	@RequestMapping(value = "/s/prod/importlist")
	@Authorize(authorityTypes = AuthorityType.PRODUCT_IMPORT)
	public String joinDistProd(HttpServletRequest request, HttpServletResponse response, String curPageNO, ProductImport productImport) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<ProductImport> ps = productImportService.getProductImportPage(curPageNO, productImport, shopId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prodName", productImport.getProdName());
		return PathResolver.getPath(ShopFrontPage.PROD_IMPORT);
	}

	/**
	 * 下载Excel模版.
	 *
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/s/prod/import/downloadtemp")
	@ResponseBody
	public String save(HttpServletRequest request, HttpServletResponse response) {
		String path = FileProcessor.getExcelByName("templet.xls");
		String fileName = "templet.xls";
		request.getServletContext().getResourceAsStream("");
		OutputStream outputStream = null;
		try {
			response.setContentType("plan/text");
			response.setHeader("Location", fileName);
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
			outputStream = response.getOutputStream();
			InputStream inputStream = new FileInputStream(path);
			byte[] buffer = new byte[1024];
			int i = -1;
			while ((i = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, i);
			}
			outputStream.flush();

		} catch (FileNotFoundException e1) {
			System.out.println("没有找到您要的文件");
		} catch (Exception e) {
			System.out.println("下载错误");
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return "";

	}

	/**
	 * 删除已经上传的商品.
	 *
	 * @param request
	 * @param response
	 * @param jsonID
	 * @return the string
	 */
	@RequestMapping(value = "/s/prod/import/delete", method = RequestMethod.POST)
	@ResponseBody
	public String deleteImport(HttpServletRequest request, HttpServletResponse response, String jsonID) {
		List<Long> idList = JSONUtil.getArray(jsonID, Long.class);
		if (AppUtils.isNotBlank(idList)) {
			productImportService.deleteProductImport(idList);
			return "success";
		}
		return "没有要删除的数据";
	}

	/**
	 * 导入LegendShop格式的商品.
	 *
	 * @param request
	 * @param response
	 * @param csvFile
	 * @return the string
	 */
	@RequestMapping(value = "/s/prod/importprod")
	@ResponseBody
	public String importprod(HttpServletRequest request, HttpServletResponse response, MultipartFile csvFile) {
		try {
			POIReadeExcelUtil readeExcelUtil = new POIReadeExcelUtil();
			ProductImportResult result = readeExcelUtil.readExcels(csvFile.getInputStream());
			
			SecurityUserDetail user = UserManager.getUser(request);
			Long shopId = user.getShopId();
			
			if (AppUtils.isNotBlank(result.getMessage())) {
				log.warn("商城Id {} 导入失败, 结果为 {}", shopId, result.getMessage());
				return "对不起,导入失败!失败原因: " + result.getMessage();
			}

			List<ProductImport> importList = result.getResult();
			for (ProductImport productImport : importList) {// 设置默认值
				productImport.setShopId(shopId);
				Date date = new Date();
				productImport.setRecDate(date);
				productImport.setModifyDate(date);
				productImport.setStatus(0);
			}
			if (AppUtils.isNotBlank(importList)) {
				productImportService.saveProductImport(importList);
			}
		} catch (Exception e) {
			log.error("", e);
		}
		return "OK";
	}

	/**
	 * Split product import.
	 *
	 * @param importList
	 * @param start
	 * @param end
	 * @return the list< product import>
	 */
	private List<ProductImport> splitProductImport(List<ProductImport> importList, int start, int end) {
		List<ProductImport> result = new ArrayList<ProductImport>();
		int index = start;
		while (index <= end) {
			result.add(importList.get(index++));
		}
		return result;
	}

	/**
	 * 导入淘宝格式的商品.
	 *
	 * @param request
	 * @param response
	 * @param csvFile
	 * @return the string
	 */
	@RequestMapping(value = "/s/prod/importtaobao", method = RequestMethod.POST)
	@ResponseBody
	public String importTaobao(HttpServletRequest request, HttpServletResponse response, MultipartFile csvFile) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		List<ProductImport> importList = null;
		try {
			importList = CSVReaderUtil.readCSVToList(csvFile.getInputStream(), ProductImport.class);
			for (ProductImport productImport : importList) {// 设置默认值
				productImport.setShopId(shopId);
				Date date = new Date();
				productImport.setRecDate(date);
				productImport.setModifyDate(date);
				productImport.setStatus(0);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (AppUtils.isNotBlank(importList)) {
			productImportService.saveProductImport(importList);
		}
		return "OK";
	}

	/**
	 * 加载导入excel文件页面.
	 *
	 * @param request
	 * @param response
	 * @param data
	 * @return the string
	 */
	@RequestMapping(value = "/s/prod/load")
	public String load(HttpServletRequest request, HttpServletResponse response, String data) {
		request.setAttribute("data", "1");
		return PathResolver.getPath(ShopFrontPage.P_IMPORT_UPLOAD);
	}

	/**
	 * 加载导入cvs文件页面.
	 *
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/s/prod/load1")
	public String load1(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("data", "2");
		return PathResolver.getPath(ShopFrontPage.P_IMPORT_UPLOAD);
	}

    /**
     * 导出类目模板
     * @param request
     * @param response
     * @param type (0:平台类目;1:店铺类目)
     * @return
     */
    @RequestMapping(value = "/s/cateGoryExecl/exportCateGory/{type}")
    @ResponseBody
    public String exportCateGory(HttpServletRequest request, HttpServletResponse response,@PathVariable Long type) {
        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();
        if(shopId == null){
            request.setAttribute("message", "登录异常,请刷新后重试");
            return PathResolver.getPath(ShopFrontPage.OPERATION_ERROR);
        }
        String[] headers = {"类目ID","类目名称"};
        List<CateGoryExportDTO> cateGoryExportDTOList = null;
        String title = "平台类目";
        if(type.equals(0L)){
            response.setHeader("Content-Disposition", "attachment;filename=platformCategoryExcel.xls");
            //查询平台类目数据
            cateGoryExportDTOList = productImportService.getCateGoryExportDTOList();
        } else if (type.equals(1L)) {
            title = "店铺类目";
            response.setHeader("Content-Disposition", "attachment;filename=shopCategoryExcel.xls");
            //查询店铺类目数据
            cateGoryExportDTOList = productImportService.getShopCateGoryExportDTOList(shopId);
        }

        ExportExcelUtil<CateGoryExportDTO> ex = new ExportExcelUtil<CateGoryExportDTO>();
        try {
            ex.exportExcelUtil(title,headers,cateGoryExportDTOList,response.getOutputStream(),null,null);
        } catch (IOException e) {
            log.error("导出类目模板失败",e);
            request.setAttribute("message", "导出失败,请联系管理员");
            return PathResolver.getPath(ShopFrontPage.OPERATION_ERROR);
        }
        return Constants.SUCCESS;
    }

    /**
     * 导出本地商品模板(用于本地商品导入)
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/s/prodExecl/exportLocalProduct")
    @ResponseBody
    public void  exportLocalProduct(HttpServletRequest request, HttpServletResponse response) {
        try {
            String fileName = "product.xls";

            //设置要下载的文件的名称
            response.setHeader("Content-disposition", "attachment;fileName=" + fileName);
            response.setContentType("application/vnd.ms-excel;charset=UTF-8");

			InputStream input = FileProcessor.getInputStreamByName(fileName);

            OutputStream out = response.getOutputStream();
            byte[] b = new byte[2048];
            int len;
            while ((len = input.read(b)) != -1) {
                out.write(b, 0, len);
            }
            input.close();
            log.info("模板下载完成");
            System.out.println("模板下载完成");
        }catch (Exception ex){
            log.info("模板下载失败"+ex);
        }
    }
    /**
     * excel批量导入商品(维护中标商品信息)
     * @param request
     * @param response
     * @param uploadFile
     * @return -1:导入失败 0:部分导入成功 1:导入成功
     */
    @RequestMapping(value = "/s/product/importTenderProdByExcel")
    @ResponseBody
    public Map<String,Object> importTenderProdByExcel(HttpServletRequest request, HttpServletResponse response, MultipartFile uploadFile) {
        Map<String,Object> resultMap = new HashMap<>();
        SecurityUserDetail userDetail = UserManager.getUser(request);
        if(userDetail == null){
            resultMap.put("status","-1");
            resultMap.put("msg","登录异常,请稍后重试");
            return resultMap;
        }
        if(uploadFile == null){
            resultMap.put("status","-1");
            resultMap.put("msg","文件为空,请选择上传文件");
            return resultMap;
        }
        String userId = userDetail.getUserId();
        String userName = userDetail.getUsername();
        Long shopId = userDetail.getShopId();

        List<TenderProdExcelDTO> tenderProdExcelDTOList = null;
        //导入失败的list
        List<TenderProdExcelDTO> totalErrorList = new ArrayList<TenderProdExcelDTO>();
        //总数量
        Integer totalCount = 0;
        //导入失败数量
        Integer errorCount = 0;
        try{
            POIReadeExcelUtil readeExcelUtil = new POIReadeExcelUtil();
            //数据内容
            Map<Integer, String> map = readeExcelUtil.readExcelContent(uploadFile.getInputStream());
            tenderProdExcelDTOList = readeExcelUtil.readTenderProductExcels(map);
            if(AppUtils.isBlank(tenderProdExcelDTOList) || tenderProdExcelDTOList.size() ==0){
                resultMap.put("status","-1");
                resultMap.put("msg","文件为空,请选择上传文件");
                return resultMap;
            }
            totalCount = tenderProdExcelDTOList.size();
            //基础校验->返回错误的数据
            List<TenderProdExcelDTO> errorList1 = validTenderProdExcelDTOList(tenderProdExcelDTOList);
            totalErrorList.addAll(errorList1);
            //保存商品数据,返回错误数据
                List<TenderProdExcelDTO> errorList2 = productImportService.saveImportTenderProduct(userId,userName,shopId,tenderProdExcelDTOList);
            totalErrorList.addAll(errorList2);
        }catch (Exception e){
            e.printStackTrace();
            resultMap.put("status","-1");
            resultMap.put("msg","商品导入失败,请联系管理员");
            return resultMap;
        }
        if(totalErrorList.size()!=0){
            resultMap.put("status","0");
            resultMap.put("msg","部分商品导入成功");
            resultMap.put("succCount",totalCount-totalErrorList.size());
            resultMap.put("errorCount",totalErrorList.size());
            resultMap.put("errorList",totalErrorList);
            return resultMap;
        }
        resultMap.put("status","1");
        resultMap.put("succCount",totalCount);
        resultMap.put("errorCount",0);
        resultMap.put("msg","商品导入成功");
        for (TenderProdExcelDTO tenderProdExcel:tenderProdExcelDTOList){
            List<String> list = new ArrayList<>();
            list.add(tenderProdExcel.getSpuId());
        }

        return resultMap;
    }
    /** 中标商品过滤基本错误数据 */
    private List<TenderProdExcelDTO> validTenderProdExcelDTOList(List<TenderProdExcelDTO> list) {
        List<TenderProdExcelDTO> errorList = new ArrayList<TenderProdExcelDTO>();
        for (int i = 0; i < list.size() ; i++) {
            TenderProdExcelDTO tenderProdExcelDTO = list.get(i);
            //1.spuid 和 skuid 都没有的数据
            if(AppUtils.isBlank(tenderProdExcelDTO.getSpuId()) && AppUtils.isBlank(tenderProdExcelDTO.getSkuId())){
                errorList.add(tenderProdExcelDTO);
                list.remove(i);
            }
            //2.spuid 和skuid 都有的数据
            if(AppUtils.isNotBlank(tenderProdExcelDTO.getSpuId()) && AppUtils.isNotBlank(tenderProdExcelDTO.getSkuId())){
                errorList.add(tenderProdExcelDTO);
                list.remove(i);
            }
            //3.错误的spuid或者不存在的spuid
            if(AppUtils.isNotBlank(tenderProdExcelDTO.getSpuId())){
                String spuIdString = tenderProdExcelDTO.getSpuId().trim();
                Long spuId = null;
                try {
                    spuId = Long.parseLong(spuIdString);
                } catch (NumberFormatException e) {
                    errorList.add(tenderProdExcelDTO);
                    list.remove(i);
                }
                if(spuId != null){
                    //查询数据库是否存在这个spu
                    Product product = productService.getProductById(spuId);
                    if(product == null){
                        errorList.add(tenderProdExcelDTO);
                        list.remove(i);
                    }
                }
            }
            //4.错误的skuid或者不存在的skuid
            if(AppUtils.isNotBlank(tenderProdExcelDTO.getSkuId())){
                String skuIdString = tenderProdExcelDTO.getSkuId();
                Long skuId = null;
                try {
                    skuId = Long.parseLong(skuIdString);
                } catch (NumberFormatException e) {
                    errorList.add(tenderProdExcelDTO);
                    list.remove(i);
                }
                if(skuId != null){
                    //查询数据库是否存在这个sku
                    Sku sku = skuService.getSku(skuId);
                    if(sku == null){
                        errorList.add(tenderProdExcelDTO);
                        list.remove(i);
                    }
                }
            }
        }
        return errorList;
    }
    /**
     * excel批量导入商品方法(普通商品)
     * @param request
     * @param response
     * @param uploadFile
     * @return
     */
    @RequestMapping(value = "/s/product/importProdByExcel")
    public @ResponseBody Map<String,Object> importProdByExcel(HttpServletRequest request, HttpServletResponse response,MultipartFile uploadFile) {
        Map<String,Object> resultMap = new HashMap<>();
        SecurityUserDetail userDetail = UserManager.getUser(request);
        if(userDetail == null){
            resultMap.put("status","-1");
            resultMap.put("msg","登录异常,请稍后重试");
            return resultMap;
        }
        if(uploadFile == null){
            resultMap.put("status","-1");
            resultMap.put("msg","文件为空,请选择上传文件");
            return resultMap;
        }
        String userId = userDetail.getUserId();
        String userName = userDetail.getUsername();
        Long shopId = userDetail.getShopId();
        List<ProdExcelDTO> prodExcelDTOlist = null;
        //导入失败的list
       List<ProdExportDTO> totalErrorList = new ArrayList<ProdExportDTO>();
        //导入失败的索引list
        Set<Integer> errorIndexList = new HashSet<Integer>();
        //商品总条数
        Integer totalCount = 0;
        try {
            POIReadeExcelUtil readeExcelUtil = new POIReadeExcelUtil();
            //数据内容
            Map<Integer, String> map = readeExcelUtil.readExcelContent(uploadFile.getInputStream());
            if(map == null){
                resultMap.put("status","-1");
                resultMap.put("msg","文件为空,请选择上传文件");
                return resultMap;
            }
            prodExcelDTOlist = readeExcelUtil.readBasicProductExcels(map);
            if(AppUtils.isBlank(prodExcelDTOlist) || prodExcelDTOlist.size() == 0){
                resultMap.put("status","-1");
                resultMap.put("msg","文件为空,请选择上传文件");
                return resultMap;
            }
            //把sku关联到spu上
            List<ProdExportDTO> prodExportDTOList = copyExcelProdField(prodExcelDTOlist);
            //获取数据的总条数
            for (ProdExportDTO prodExportDTO :prodExportDTOList) {
                totalCount ++ ;
                if(prodExportDTO.getSkuList() != null){
                    totalCount += prodExportDTO.getSkuList().size();
                }
            }
            //保存商品数据,返回错误数据的索引
            //错误的数据
            for (int i = 0; i < prodExportDTOList.size() ; i++) {
                try {
                    ProdExportDTO prodExportDTO = prodExportDTOList.get(i);
                    productImportService.saveImportProduct(userId, userName, shopId, prodExportDTO);
                } catch (NumberFormatException e) {
                    errorIndexList.add(i);
                    log.warn(e.getMessage());
                } catch (BusinessException e) {
                    errorIndexList.add(i);
                    log.warn(e.getMessage());
                }
            }
            for (int i = 0; i < prodExportDTOList.size() ; i++) {
                if(errorIndexList.contains(i)){
                    totalErrorList.add(prodExportDTOList.get(i));
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
            resultMap.put("status","-1");
            resultMap.put("msg","商品导入失败,请联系管理员");
            return resultMap;
        }
        if(totalErrorList.size()!=0){
            //导入失败数量
            Integer errorCount = 0;
            for (int i = 0; i < totalErrorList.size(); i++) {
                errorCount ++;
                if(totalErrorList.get(i).getSkuList()!=null){
                    errorCount += totalErrorList.get(i).getSkuList().size();
                }
            }
            resultMap.put("status","0");
            resultMap.put("msg","部分商品导入成功");
            resultMap.put("succCount",totalCount-errorCount);
            resultMap.put("errorCount",errorCount);
            resultMap.put("errorList",totalErrorList);
            return resultMap;
        }
        resultMap.put("status","1");
        resultMap.put("succCount",totalCount);
        resultMap.put("errorCount",0);
        resultMap.put("msg","商品导入成功");
        return resultMap;
    }

	/**
	 * 下载导入失败的普通商品
	 *
	 * @param request  the request
	 * @param response the response
	 * @return String
	 */
	@RequestMapping(value = "/s/prodExecl/downLoadImportErrorProdList", method = RequestMethod.POST)
	public String downLoadImportErrorProdList(HttpServletRequest request, HttpServletResponse response, String listJson) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		if (shopId == null) {
			request.setAttribute("message", "登录异常,请稍后重试");
			return PathResolver.getPath(ShopFrontPage.OPERATION_ERROR);
		}
		String[] noteArr = getProdImportExcelTips();
		String[] headers = {"*商品名称", "*平台分类", "店铺分类", "参数项1", "参数值1", "参数项2", "参数值2", "参数项3", "参数值3", "参数项4", "参数值4"
				, "*售价（元）", "详细介绍（电脑版）", "*副标题"
				, "规格项1", "规格值1", "规格项2", "规格值2", "规格项3", "规格值3", "规格项4", "规格值4"
				, "*销售价格", "*可售库存", "物流重量（单位：kg）", "商家编码", "条形码", "*是否上架", "备注"};
		//转化json数据
		JSONArray jsonArr = cn.hutool.json.JSONUtil.parseArray(listJson);
		List<ProdExportDTO> list = jsonArr.toList(ProdExportDTO.class);
		if (list == null || list.size() == 0) {
			request.setAttribute("message", "数据为空,导出失败");
			return PathResolver.getPath(ShopFrontPage.OPERATION_ERROR);
		}
		List<ProdExcelDTO> excelList = copyProdExcelDTO(list);

		ExportExcelUtil<ProdExcelDTO> ex = new ExportExcelUtil<ProdExcelDTO>();
		try {
			response.setHeader("Content-Disposition", "attachment;filename=ImportFailprodExcel.xls");
			ex.exportExcelUtil("导入失败的商品数据", headers, excelList, response.getOutputStream(), null, noteArr);
		} catch (IOException e) {
			log.error("导出商品失败", e.getMessage());
			request.setAttribute("message", "导出失败,请联系管理员");
			return PathResolver.getPath(ShopFrontPage.OPERATION_ERROR);
		}
		return Constants.SUCCESS;
	}

	private String[] getProdImportExcelTips() {
		return new String[]{"温馨提示:"
				, "1、建议商品条数<=500,文件大小<=10M,数据量过多将可能影响导入时的性能;"
				, "2、带\"*\"为必填项,红色字段不允许修改;"
				, "3、相同spu只读取第一条spu,修改spu信息只需修改第一条spu信息即可;"
				, "4、付款方式:1代表货到付款,2代表款到发货,3代表预付款(禁止修改为预付款方式);"
				, "5、预付款商品的单品价格不能低于100元;"
				, "6、参与了团购/拼团的商品修改信息无效;"
				, "7、商品图片链接支持填写多条,之间用\";\"分隔,最多填写6条,超过6条只读取前6条;"
				, "8、单品是否上架:Y代表上架,N代表下架;"
				, "9、单品库存必须大于0且不大于10000000;"
				, "10、平台分类请参考平台分类文件,填写相应的类目ID;"
				, "11、店铺分类请参考店铺分类文件,填写相应的店铺分类,如果没有对应的店铺分类,则会创建相对应的店铺分类;"
				, "12、系统会按照填写的规格生成未补全的单品,默认下架状态;"
				, "13、不支持导入无规格商品;"
				, "14、删除excel单元行请使用正确的方式清除干净,否则将影响导入后的提示信息;"
				, "15、填写的数据过长/过大可能造成系统错误,请严格按照要求填写;"};
	}

	/**
	 * ProdExportDTO -> ProdExportDTO
	 * @param list
	 * @return
	 */
	private List<ProdExcelDTO> copyProdExcelDTO(List<ProdExportDTO> list) {
		List<ProdExcelDTO> prodExcelDTOList = new ArrayList<>();
		for (ProdExportDTO prodExportDTO : list) {
			List<SkuExportDTO> skuList = prodExportDTO.getSkuList();
			for (int i = 0; i < skuList.size(); i++) {
				ProdExcelDTO prodExcelDTO = new ProdExcelDTO();
				SkuExportDTO skuExportDTO = skuList.get(i);
				if(i == 0){
					BeanUtil.copyProperties(prodExportDTO,prodExcelDTO);
				}
				prodExcelDTO.setSkuName(skuExportDTO.getSkuName());
				prodExcelDTO.setFirstProperties(skuExportDTO.getFirstProperties());
				prodExcelDTO.setFirstPropertiesValue(skuExportDTO.getFirstPropertiesValue());
				prodExcelDTO.setSecondProperties(skuExportDTO.getSecondProperties());
				prodExcelDTO.setSecondPropertiesValue(skuExportDTO.getSecondPropertiesValue());
				prodExcelDTO.setThirdProperties(skuExportDTO.getThirdProperties());
				prodExcelDTO.setThirdPropertiesValue(skuExportDTO.getThirdPropertiesValue());
				prodExcelDTO.setFourthProperties(skuExportDTO.getFourthProperties());
				prodExcelDTO.setFourthPropertiesValue(skuExportDTO.getFourthPropertiesValue());
				prodExcelDTO.setSkuPrice(skuExportDTO.getSkuPrice());
				prodExcelDTO.setStocks(skuExportDTO.getStocks());
				/*prodExcelDTO.setWeight(skuExportDTO.getWeight());*/
				prodExcelDTO.setPartyCode(skuExportDTO.getPartyCode());
				prodExcelDTO.setModelId(skuExportDTO.getModelId());
				prodExcelDTO.setStatus(skuExportDTO.getStatus());
				prodExcelDTO.setRemark(skuExportDTO.getRemark());
				prodExcelDTOList.add(prodExcelDTO);
			}
		}
		return prodExcelDTOList;
	}




	/** 封装数据(普通商品) */
    private List<ProdExportDTO> copyExcelProdField(List<ProdExcelDTO> list) {
        List<ProdExportDTO> prodExportList = new ArrayList<ProdExportDTO>();
        for (int i = 0; i < list.size() ; i++) {
            ProdExcelDTO prodExcelDTO =list.get(i);
            if(AppUtils.isNotBlank(prodExcelDTO.getProdName())){
                ProdExportDTO prodExportDTO = new ProdExportDTO();
                prodExportDTO.setShopCategroy(prodExcelDTO.getShopCategroy());
                prodExportDTO.setFirstParameterValue(prodExcelDTO.getFirstParameterValue());
                prodExportDTO.setThirdParameter(prodExcelDTO.getThirdParameter());
                prodExportDTO.setContent(prodExcelDTO.getContent());
//                prodExportDTO.setPicture(prodExcelDTO.getPicture());
                prodExportDTO.setCategroyId(prodExcelDTO.getCategroyId());
                prodExportDTO.setSecondParameterValue(prodExcelDTO.getSecondParameterValue());
                prodExportDTO.setFourthParameter(prodExcelDTO.getFourthParameter());
               /* prodExportDTO.setPayType(prodExcelDTO.getPayType());*/
                prodExportDTO.setThirdParameterValue(prodExcelDTO.getThirdParameterValue());
                prodExportDTO.setSpuPrice(prodExcelDTO.getSpuPrice());
                prodExportDTO.setFirstParameter(prodExcelDTO.getFirstParameter());
                prodExportDTO.setFourthParameterValue(prodExcelDTO.getFourthParameterValue());
                prodExportDTO.setProdName(prodExcelDTO.getProdName());
                prodExportDTO.setSecondParameter(prodExcelDTO.getSecondParameter());
                prodExportList.add(prodExportDTO);
            }
            if(AppUtils.isNotBlank(prodExcelDTO.getSkuName())){
                SkuExportDTO skuExportDTO = new SkuExportDTO();
                skuExportDTO.setSkuName(prodExcelDTO.getSkuName());
                skuExportDTO.setFirstProperties(prodExcelDTO.getFirstProperties());
                skuExportDTO.setFirstPropertiesValue(prodExcelDTO.getFirstPropertiesValue());
                skuExportDTO.setSecondProperties(prodExcelDTO.getSecondProperties());
                skuExportDTO.setSecondPropertiesValue(prodExcelDTO.getSecondPropertiesValue());
                skuExportDTO.setThirdProperties(prodExcelDTO.getThirdProperties());
                skuExportDTO.setThirdPropertiesValue(prodExcelDTO.getThirdPropertiesValue());
                skuExportDTO.setFourthProperties(prodExcelDTO.getFourthProperties());
                skuExportDTO.setFourthPropertiesValue(prodExcelDTO.getFourthPropertiesValue());
                skuExportDTO.setSkuPrice(prodExcelDTO.getSkuPrice());
                skuExportDTO.setStocks(prodExcelDTO.getStocks());
//                skuExportDTO.setWeight(prodExcelDTO.getWeight());
                skuExportDTO.setPartyCode(prodExcelDTO.getPartyCode());
                skuExportDTO.setModelId(prodExcelDTO.getModelId());
                skuExportDTO.setStatus(prodExcelDTO.getStatus());
                skuExportDTO.setRemark(prodExcelDTO.getRemark());
                setSkuExportDTOList(prodExportList,skuExportDTO,i);
            }
        }
        return prodExportList;
    }
    /** 设置setSkuExportDTOList */
    private void setSkuExportDTOList(List<ProdExportDTO> prodExportList, SkuExportDTO skuExportDTO, int i) {
        for (int j = i; j > 0; j--) {
            if(prodExportList.size()<j){
                continue;
            }
            ProdExportDTO ProdExportDTO = prodExportList.get(j-1);
            if(AppUtils.isNotBlank(ProdExportDTO.getProdName())){
                ProdExportDTO.getSkuList().add(skuExportDTO);
                break;
            }

        }
    }
}
