/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ShopLayoutDivTypeEnum;
import com.legendshop.model.constant.ShopLayoutModuleTypeEnum;
import com.legendshop.model.constant.ShopLayoutTypeEnum;
import com.legendshop.model.dto.shopDecotate.ShopDecotateDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutCateogryDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutDivDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutHotDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutHotProdDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutModuleDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutProdDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutShopInfoDto;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.shopDecotate.ShopDecotate;
import com.legendshop.model.entity.shopDecotate.ShopLayoutBanner;
import com.legendshop.model.entity.shopDecotate.ShopLayoutParam;
import com.legendshop.model.entity.shopDecotate.ShopNav;
import com.legendshop.multishop.page.DecorateFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.ShopDecotateCacheService;
import com.legendshop.spi.service.ShopDecotateService;
import com.legendshop.spi.service.ShopLayoutBannerService;
import com.legendshop.spi.service.ShopLayoutService;
import com.legendshop.spi.service.ShopNavService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 店铺装修
 *
 */
@Controller
@RequestMapping("/s/shopDecotate")
public class ShopDecotateController extends BaseController {

	private final static Logger log = LoggerFactory.getLogger(ShopDecotateController.class);

	@Autowired
	private ShopDecotateService shopDecotateService;
	
	@Autowired
	private ShopDecotateCacheService shopDecotateCacheService;

	@Autowired
	private ShopLayoutService shopLayoutService;

	@Autowired
	private ShopNavService shopNavService;

	@Autowired
	private ShopLayoutBannerService shopLayoutBannerService;

	@Autowired
	private ProductService productService;

	/**
	 * 商家进入装修
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/decorate")
	@Authorize(authorityTypes = AuthorityType.SHOP_DECORATE)
	public String query(HttpServletRequest request, HttpServletResponse response) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
	    log.info("[decorate] querying shopId = {}", shopId);
		ShopDecotate decotate = shopDecotateService.getShopDecotateByShopId(shopId);
		if (AppUtils.isBlank(decotate)) {
			decotate = new ShopDecotate();
			decotate.setShopId(shopId);
			decotate.setIsbanner(0);
			decotate.setIsInfo(0);
			decotate.setIsNav(0);
			decotate.setIsSlide(0);
			decotate.setCreateDate(new Date());
			Long id = shopDecotateService.saveShopDecotate(decotate);
			decotate.setId(id);
		}
		final ShopDecotateDto decotateDto = shopDecotateService.findShopDecotateDto(decotate);
		request.setAttribute("shopDecotate", decotateDto);
		return PathResolver.getPath(DecorateFrontPage.DECORATE);
	}

	/**
	 * 装修预览
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/decorate/preview")
	@Authorize(authorityTypes = AuthorityType.SHOP_DECORATE)
	public String decoratePreview(HttpServletRequest request, HttpServletResponse response) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
        log.info("[decorate] Preview querying shopId = {}", shopId);
		ShopDecotateDto decotateDto = null;
		try {
			decotateDto = shopDecotateService.findShopDecotateCache(shopId);
		} catch (Exception e) {
			return PathResolver.getRedirectPath("/s/shopDecotate/decorate");
		}
		if (decotateDto == null) {
			throw new BusinessException("[decorate] decotateDto  is null ");
		}
		request.setAttribute("shopDecotate", decotateDto);
		request.setAttribute("preview", true);
		String path = PathResolver.getPath(DecorateFrontPage.DECORATE_PREVIEW);
		return path;
	}

	/**
	 * 数据保存退出
	 * 
	 * @param request
	 * @param response
	 * @param type
	 * @param status
	 * @return
	 */
	@RequestMapping(value = "/decorateSaveQuite", method = RequestMethod.POST)
	@ResponseBody
	@Authorize(authorityTypes = AuthorityType.SHOP_DECORATE)
	public String decorateSaveQuite(HttpServletRequest request, HttpServletResponse response) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
    	log.info("[decorate] decorateSaveQuite shopId = {}", shopId);
    	ShopDecotateDto decotateDto = null;
    	try {
    		decotateDto = shopDecotateService.findShopDecotateCache(shopId);
		} catch (Exception e) {
			log.error("findShopDecotateCache shopId = {} failed", shopId);
			return Constants.FAIL;
		}
    	
    	if(decotateDto==null){
    		log.error("[decorate] findShopDecotateCache decotateDto is null, shopId = {}", shopId);
    		return Constants.FAIL;
    	}

		decotateDto = shopDecotateService.decorateSaveQuite(decotateDto);
		shopDecotateCacheService.cachePutShopDecotate(decotateDto);
		return Constants.SUCCESS;
	}

	/**
	 * 撤销动作
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/revocation", method = RequestMethod.POST)
	@ResponseBody
	@Authorize(authorityTypes = AuthorityType.SHOP_DECORATE)
	public String revocation(HttpServletRequest request, HttpServletResponse response) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
        log.info("[decorate] revocation shopId = {}", shopId);
		if (shopId == null) {
            log.error("shopId is null");
			return Constants.FAIL;
		}
		shopDecotateCacheService.cachePutShopDecotate(shopId);
		return Constants.SUCCESS;
	}

	/**
	 * 设置基础装修模块
	 * @param request
	 * @param response
	 * @param type
	 * @param status
	 * @return
	 */
	@RequestMapping(value = "/set_decorate_base_module", method = RequestMethod.POST)
	@ResponseBody
	@Authorize(authorityTypes = AuthorityType.SHOP_DECORATE)
	public String set_decorate_base_module(HttpServletRequest request, HttpServletResponse response, String type, Integer status) {
		if (AppUtils.isBlank(type) || AppUtils.isBlank(status)) {
            log.error("[decorate] set_decorate_base_module, but type is null or status is null");
			return null;
		}
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
        log.info("[decorate] set_decorate_base_module shopId = {}", shopId);
		ShopDecotateDto decotateDto = shopDecotateService.findShopDecotateCache(shopId);
		if (decotateDto == null) {
			return null;
		}
		boolean ismove = false;
		if ("banner".equals(type)) {
			decotateDto.setIsBanner(status);
			ismove = true;
		} else if ("info".equals(type)) {
			decotateDto.setIsInfo(status);
			ismove = true;
		} else if ("nav".equals(type)) {
			decotateDto.setIsNav(status);
			ismove = true;
		} else if ("slide".equals(type)) {
			decotateDto.setIsSlide(status);
			ismove = true;
		}
		if (ismove) {
			shopDecotateCacheService.cachePutShopDecotate(decotateDto);
		}
		return null;
	}

	/**
	 * 设置装修背景
	 * 
	 * @param request
	 * @param response
	 * @param method
	 * @param bgImgId
	 * @param bg_color
	 * @param repeat
	 * @return
	 */
	@RequestMapping(value = "/background_set", method = RequestMethod.POST)
	@ResponseBody
	@Authorize(authorityTypes = AuthorityType.SHOP_DECORATE)
	public String background_set(HttpServletRequest request, HttpServletResponse response, String method, String bgImgId, String bg_color, String repeat) {
		if (AppUtils.isBlank(method)) {
            log.error("[decorate] background_set method is null");
			return Constants.FAIL;
		}
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
        log.info("[decorate] background_set shopId = {}", shopId);
		ShopDecotateDto decotateDto = shopDecotateService.findShopDecotateCache(shopId);
		if (decotateDto == null) {
            log.error("[decorate] background_set decotateDto is null");
			return null;
		}
		boolean ismove = false;
		if ("save".equals(method)) {
			decotateDto.setBgImgId(bgImgId);
			decotateDto.setBgColor(bg_color);
			decotateDto.setBgStyle(repeat);
			ismove = true;
		} else if ("cancle".equals(method)) {
			decotateDto.setBgImgId(null);
			decotateDto.setBgColor(null);
			decotateDto.setBgStyle(null);
			ismove = true;
		}
		if (ismove) {
			shopDecotateCacheService.cachePutShopDecotate(decotateDto);
		}
		return null;
	}

	/** 附件上传文件 */
	@Autowired
	private AttachmentManager attachmentManager;

	/**
	 * 背景图片
	 * 
	 * @param request
	 * @param response
	 * @param file
	 * @return
	 */
	@RequestMapping(value = "/decorate_background_upload")
	@Authorize(authorityTypes = AuthorityType.SHOP_DECORATE)
	public @ResponseBody String backgroundUpload(MultipartHttpServletRequest request, HttpServletResponse response, MultipartFile bg_img) {
		if (AppUtils.isNotBlank(bg_img)) {
            log.info("[decorate] backgroundUpload");
			if (bg_img.getSize() > 0) {
				SecurityUserDetail user = UserManager.getUser(request);
				Long shopId = user.getShopId();
				
				ShopDecotateDto decotateDto = shopDecotateService.findShopDecotateCache(shopId);
				if (decotateDto == null) {
					return Constants.FAIL;
				}
				String pic = attachmentManager.upload(bg_img);
				return pic;
			} else {
				return Constants.FAIL;
			}
		} else {
			return Constants.FAIL;
		}
	}

	/**
	 * 添加布局
	 * 
	 * @param request
	 * @param response
	 * @param layout
	 * @return
	 */
	@RequestMapping(value = "/shopDecLoyout", method = RequestMethod.POST)
	@Authorize(authorityTypes = AuthorityType.SHOP_DECORATE)
	public String addShopDecLoyout(HttpServletRequest request, HttpServletResponse response, String layout) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		ShopDecotateDto decotateDto = shopDecotateService.findShopDecotateCache(shopId);
        log.info("[decorate] shopDecLoyout shopId = {}", shopId);
		if (decotateDto == null) {
            log.error("[decorate] shopDecLoyout shopId is null");
			request.setAttribute("msg", "ERROR");
			return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_ERROR);
		}

		if (shopLayoutService.getLayoutCount(shopId, decotateDto.getDecId()) >= 20) {
            log.info("[decorate] shopDecLoyout OVER_LIMIT, shopId = {}", shopId);
			request.setAttribute("msg", "OVER_LIMIT");
			return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_ERROR);
		}

		ShopLayoutDto shopLayouts = shopLayoutService.saveShopLayout(layout, shopId, decotateDto.getDecId());
		if (ShopLayoutTypeEnum.LAYOUT_0.value().equals(layout)) {//通栏布局上
			List<ShopLayoutDto> topShopLayouts = decotateDto.getTopShopLayouts();
			topShopLayouts.add(shopLayouts);
			decotateDto.setTopShopLayouts(topShopLayouts);
		} else if (ShopLayoutTypeEnum.LAYOUT_1.value().equals(layout)) {//通栏布局下
			List<ShopLayoutDto> bottomShopLayouts = decotateDto.getBottomShopLayouts();
			bottomShopLayouts.add(shopLayouts);
			decotateDto.setBottomShopLayouts(bottomShopLayouts);
		} else {
			//layout3和4   1:2布局、2:1布局
			List<ShopLayoutDto> mainShopLayouts = decotateDto.getMainShopLayouts();
			mainShopLayouts.add(shopLayouts);
			decotateDto.setMainShopLayouts(mainShopLayouts);
		}
		shopDecotateCacheService.cachePutShopDecotate(decotateDto);
		request.setAttribute("shopLayout", shopLayouts);
		return PathResolver.getPath(DecorateFrontPage.DECORATE_LAYOUT);
	}

	/**
	 * 删除布局模块
	 * 
	 * @param request
	 * @param response
	 * @param layoutParam
	 * @return
	 */
	@RequestMapping(value = "/deleLayout", method = RequestMethod.POST)
	@Authorize(authorityTypes = AuthorityType.SHOP_DECORATE)
	public @ResponseBody String deleLayout(HttpServletRequest request, HttpServletResponse response, ShopLayoutParam layoutParam) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
        log.info("[decorate] deleLayout, shopId = {}", shopId);
		if (shopId == null || layoutParam.getLayoutId() == null || layoutParam.getLayout() == null) {
			return Constants.FAIL;
		}
		ShopDecotateDto decotateDto = shopDecotateService.findShopDecotateCache(shopId);
		if (decotateDto == null) {
            log.info("[decorate] deleLayout, decotateDto is null");
			return Constants.FAIL;
		}
		ShopDecotateDto shopDecotateDto = shopLayoutService.deleLayout(decotateDto, layoutParam);
		shopDecotateCacheService.cachePutShopDecotate(shopDecotateDto);
		request.setAttribute("layout", layoutParam.getLayout());
		return Constants.SUCCESS;
	}

	/**
	 * 加载模块编辑
	 * 
	 * @param request
	 * @param response
	 * @param layoutParam
	 * @return
	 */
	@RequestMapping(value = "/decorateModule")
	@Authorize(authorityTypes = AuthorityType.SHOP_DECORATE)
	public String decorateModule(HttpServletRequest request, HttpServletResponse response, ShopLayoutParam layoutParam) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
        log.info("[decorate] decorateModule, shopId = {}", shopId);
		request.setAttribute("layoutParam", layoutParam);
		return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE);
	}

	/**
	 * 装修模块设置
	 * @param request
	 * @param response
	 * @param layoutParam
	 * @return
	 */
	@RequestMapping(value = "/decorateModuleSet")
	@Authorize(authorityTypes = AuthorityType.SHOP_DECORATE)
	public String decorateModuleSet(HttpServletRequest request, HttpServletResponse response, ShopLayoutParam layoutParam) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
        log.info("[decorate] decorateModuleSet, shopId = {}", shopId);
		if (shopId == null || layoutParam.getLayoutModuleType() == null) {
			return Constants.FAIL;
		}
		ShopDecotateDto decotateDto = shopDecotateService.findShopDecotateCache(shopId);
		if (decotateDto == null) {
			return Constants.FAIL;
		}
		layoutParam.setShopId(shopId);
		layoutParam.setDecId(decotateDto.getDecId());
		List<ShopLayoutDto> shopLayoutDtos = decotateDto.getMainShopLayouts();
		ShopLayoutDto layoutDto = existLayout(layoutParam.getLayoutId(), shopLayoutDtos);
		if (layoutDto == null) {
			log.warn("[decorate] error  layoutDto is null ");
			return Constants.FAIL;
		}
		request.setAttribute("title", layoutDto.getTitle());
		request.setAttribute("fontImg", layoutDto.getFontImg());
		request.setAttribute("fontColor", layoutDto.getFontColor());
		request.setAttribute("backColor", layoutDto.getBackColor());
		request.setAttribute("layoutParam", layoutParam);
		return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_SET);
	}

	/**
	 * 模块编辑及保存
	 * 
	 * @param request
	 * @param response
	 * @param layoutParam
	 * @return
	 */
	@RequestMapping(value = "/decorateModuleSave")
	@Authorize(authorityTypes = AuthorityType.SHOP_DECORATE)
	public String decorateModuleSave(HttpServletRequest request, HttpServletResponse response, ShopLayoutParam layoutParam) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		String type = layoutParam.getLayoutModuleType();
        log.info("[decorate] decorateModuleSave, shopId = {}, layoutParam = {}", shopId, layoutParam);
		if (shopId == null || type == null) {
			return Constants.FAIL;
		}
		String path = null;
		ShopDecotateDto decotateDto = shopDecotateService.findShopDecotateCache(shopId);
		if (decotateDto == null) {
			return Constants.FAIL;
		}
		layoutParam.setShopId(shopId);
		layoutParam.setDecId(decotateDto.getDecId());
		String layout = layoutParam.getLayout();

		List<ShopLayoutDto> shopLayoutDtos = null;
		if (ShopLayoutTypeEnum.LAYOUT_0.value().equals(layout)) {
			shopLayoutDtos = decotateDto.getTopShopLayouts();
		} else if (ShopLayoutTypeEnum.LAYOUT_1.value().equals(layout)) {
			shopLayoutDtos = decotateDto.getBottomShopLayouts();
		} else {
			shopLayoutDtos = decotateDto.getMainShopLayouts();
		}
		ShopLayoutDto layoutDto = existLayout(layoutParam.getLayoutId(), shopLayoutDtos);
		if (layoutDto == null) {
			log.warn(" error  layoutDto is null ");
			return Constants.FAIL;
		}
		ShopLayoutModuleDto moduleDto = layoutDto.getShopLayoutModule();
		if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_NAV.value().equals(type)) {
			List<ShopNav> shopNavs = null;
			if (!type.equals(layoutDto.getLayoutModuleType())) {
				layoutDto = shopDecotateService.decorateModuleShopNavs(layoutParam, layoutDto);
			}
			shopNavs = moduleDto.getShopNavs();
			layoutDto.setLayoutModuleType(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_NAV.value());
			request.setAttribute("shopNavs", shopNavs);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_NAV);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_BANNER.value().equals(type)) {
			List<ShopLayoutBanner> layoutBanners = moduleDto.getShopLayoutBanners();
			if (AppUtils.isBlank(layoutBanners)) {
				layoutBanners = shopLayoutBannerService.getShopLayoutBanner(decotateDto.getDecId(), layoutParam.getLayoutId());
				moduleDto.setShopLayoutBanners(null);
				moduleDto.setShopLayoutBanners(layoutBanners);
			}
			request.setAttribute("layoutBanners", layoutBanners);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_BANNER_LAYOUT);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CATEGORY.value().equals(type)) {
			List<ShopLayoutCateogryDto> cateogryDtos = moduleDto.getCateogryDtos();
			if (!type.equals(layoutDto.getLayoutModuleType())) {
				cateogryDtos = shopDecotateService.findShopCateogryDtos(layoutParam, layoutDto);
			}
			layoutDto.setLayoutModuleType(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CATEGORY.value());
			request.setAttribute("cateogryDtos", cateogryDtos);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_CATEGORY);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_HOT_PROD.value().equals(type)) {
			List<ShopLayoutHotProdDto> hotProds = moduleDto.getHotProds();
			if (!type.equals(layoutDto.getLayoutModuleType())) {
				hotProds = shopDecotateService.findShopHotProds(layoutParam, layoutDto);
			}
			layoutDto.setLayoutModuleType(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_HOT_PROD.value());
			request.setAttribute("hotProds", hotProds);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_HOT_PROD);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_SHOPINFO.value().equals(type)) {
			ShopLayoutShopInfoDto shopInfo = moduleDto.getShopInfo();
			if (!type.equals(layoutDto.getLayoutModuleType())) {
				shopInfo = shopDecotateService.findShopInfo(layoutParam, layoutDto);
			}
			layoutDto.setLayoutModuleType(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_SHOPINFO.value());
			request.setAttribute("shopInfo", shopInfo);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_SHOPINFO);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CUSTOM_PROD.value().equals(type)) {
			List<ShopLayoutProdDto> prodDtos = moduleDto.getLayoutProdDtos();
			if (AppUtils.isBlank(prodDtos)) {
				prodDtos = shopDecotateService.findShopLayoutProdDtos(layoutParam, layoutDto);
			}
			layoutDto.setLayoutModuleType(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CUSTOM_PROD.value());
			request.setAttribute("prods", prodDtos);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_PROD_LAYOUT);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CUSTOM_DESC.value().equals(type)) {
			String content = null;
			if (AppUtils.isNotBlank(layoutParam.getDivId())) {
				ShopLayoutDivDto divDto = existDivLayout(layoutParam.getDivId(), layoutDto.getShopLayoutDivDtos());
				if (divDto != null) {
					content = divDto.getLayoutContent();
				}
			} else {
				content = moduleDto.getLayoutContent();
			}
			request.setAttribute("layoutContent", content);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_CUST_CONTENT_LAYOUT);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_HOT.value().equals(type)) {
			ShopLayoutHotDto shopLayoutHotDto = moduleDto.getShopLayoutHotDto();
			if (AppUtils.isBlank(shopLayoutHotDto)) {
				shopLayoutHotDto = shopDecotateService.findShopLayoutHotDto(layoutParam, layoutDto);
			}
			request.setAttribute("hots", shopLayoutHotDto);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_HOT_LAYOUT);
		}

		shopDecotateCacheService.cachePutShopDecotate(decotateDto);
		request.setAttribute("layoutParam", layoutParam);
		return path;
	}

	/**
	 * loding layout module 页面初始化加载模块布局页面
	 * 
	 * @param request
	 * @param response
	 * @param layoutParam
	 * @return
	 */
	@RequestMapping(value = "/lodingLayouModule")
	public String lodingLayouModule(HttpServletRequest request, HttpServletResponse response, ShopLayoutParam layoutParam) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		String type = layoutParam.getLayoutModuleType();
        log.info("[decorate] lodingLayouModule, shopId = {}, layoutParam = {}", shopId, layoutParam);
		if (shopId == null || type == null) {
			return "";
		}
		ShopDecotateDto decotateDto = shopDecotateService.findShopDecotateCache(shopId);
		if (decotateDto == null) {
			return "";
		}
		layoutParam.setShopId(shopId);
		layoutParam.setDecId(decotateDto.getDecId());
		Long layoutId = layoutParam.getLayoutId();
		String layout = layoutParam.getLayout();
		List<ShopLayoutDto> shopLayoutDtos = null;
		if (ShopLayoutTypeEnum.LAYOUT_0.value().equals(layout)) {
			shopLayoutDtos = decotateDto.getTopShopLayouts();
		} else if (ShopLayoutTypeEnum.LAYOUT_1.value().equals(layout)) {
			shopLayoutDtos = decotateDto.getBottomShopLayouts();
		} else {
			shopLayoutDtos = decotateDto.getMainShopLayouts();
		}
		ShopLayoutDto layoutDto = existLayout(layoutId, shopLayoutDtos);
		if (layoutDto == null) {
			log.warn(" error  layoutDto is null ");
			return Constants.FAIL;
		}
		ShopLayoutModuleDto moduleDto = layoutDto.getShopLayoutModule();
		String path = null;
		if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_NAV.value().equals(type)) {
			List<ShopNav> shopNavs = moduleDto.getShopNavs();
			if (AppUtils.isBlank(shopNavs)) { // if cache not exist
				shopNavs = shopNavService.getLayoutNav(layoutParam.getShopId());
				moduleDto.setShopNavs(null);
				moduleDto.setShopNavs(shopNavs);
			}
			request.setAttribute("shopNavs", shopNavs);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_NAV);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_BANNER.value().equals(type)) {
			List<ShopLayoutBanner> layoutBanners = moduleDto.getShopLayoutBanners();
			if (AppUtils.isBlank(layoutBanners)) {
				layoutBanners = shopLayoutBannerService.getShopLayoutBanner(decotateDto.getDecId(), layoutId);
				moduleDto.setShopLayoutBanners(null);
				moduleDto.setShopLayoutBanners(layoutBanners);
			}
			request.setAttribute("shopLayoutBanners", layoutBanners);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_BANNER);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CATEGORY.value().equals(type)
				&& ShopLayoutDivTypeEnum.DIV_1.value().equals(layoutParam.getLayoutDiv())) {
			List<ShopLayoutCateogryDto> cateogryDtos = moduleDto.getCateogryDtos();
			if (AppUtils.isBlank(cateogryDtos)) {
				cateogryDtos = shopDecotateService.findShopCateogryDtos(layoutParam, layoutDto);
			}
			request.setAttribute("cateogryDtos", cateogryDtos);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_CATEGORY);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_HOT_PROD.value().equals(type)
				&& ShopLayoutDivTypeEnum.DIV_1.value().equals(layoutParam.getLayoutDiv())) {
			List<ShopLayoutHotProdDto> hotProdDtos = moduleDto.getHotProds();
			if (AppUtils.isBlank(hotProdDtos)) {
				hotProdDtos = shopDecotateService.findShopHotProds(layoutParam, layoutDto);
			}

			request.setAttribute("hotProds", hotProdDtos);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_HOT_PROD);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_SHOPINFO.value().equals(type)
				&& ShopLayoutDivTypeEnum.DIV_1.value().equals(layoutParam.getLayoutDiv())) {
			ShopLayoutShopInfoDto shopInfo = moduleDto.getShopInfo();
			if (AppUtils.isBlank(shopInfo)) {
				shopInfo = shopDecotateService.findShopInfo(layoutParam, layoutDto);
			}
			request.setAttribute("shopInfo", shopInfo);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_SHOPINFO);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CUSTOM_PROD.value().equals(type)) {
			List<ShopLayoutProdDto> prodDtos = moduleDto.getLayoutProdDtos();
			if (AppUtils.isBlank(prodDtos)) {
				prodDtos = shopDecotateService.findShopLayoutProdDtos(layoutParam, layoutDto);
			}

			request.setAttribute("title", layoutDto.getTitle());
			request.setAttribute("fontImg", layoutDto.getFontImg());
			request.setAttribute("fontColor", layoutDto.getFontColor());
			request.setAttribute("backColor", layoutDto.getBackColor());
			request.setAttribute("prods", prodDtos);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_PROD);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CUSTOM_DESC.value().equals(type)) {
			String content = null;
			if (AppUtils.isNotBlank(layoutParam.getDivId())) {
				ShopLayoutDivDto divDto = existDivLayout(layoutParam.getDivId(), layoutDto.getShopLayoutDivDtos());
				if (divDto != null) {
					content = divDto.getLayoutContent();
				}
			} else {
				content = moduleDto.getLayoutContent();
			}
			request.setAttribute("layoutContent", content);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_CUST_CONTENT);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_HOT.value().equals(type)) {
			ShopLayoutHotDto shopLayoutHotDto = moduleDto.getShopLayoutHotDto();
			if (AppUtils.isBlank(shopLayoutHotDto)) {
				shopLayoutHotDto = shopDecotateService.findShopLayoutHotDto(layoutParam, layoutDto);
			}
			request.setAttribute("hots", shopLayoutHotDto);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_HOT_LOAD);
		}
		shopDecotateCacheService.cachePutShopDecotate(decotateDto);
		request.setAttribute("layoutParam", layoutParam);
		return path;
	}

	/**
	 * 加载商品楼层
	 * @param request
	 * @param response
	 * @param name
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping(value = "/productLoad")
	public String productFloorLoad(HttpServletRequest request, HttpServletResponse response, String name, String curPageNO) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<Product> ps = productService.getProductFloorLoad(curPageNO, name, shopId);
		List<Product> prodList = (List<Product>) ps.getResultList();
		String newName = null;
		for (Product prod : prodList) {
			newName = prod.getName().replaceAll("'", " ");// 去掉双引号和单引号
			newName = newName.replace("\"", " ");
			prod.setName(newName);
		}
		ps.setResultList(prodList);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("name", name);
		request.setAttribute("curPageNO", curPageNO);
		return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_HOT_PROD_LOAD);
	}

	/**
	 * 是否包含该模块
	 * 
	 * @param layoutId
	 * @param shopLayoutDtos
	 * @return
	 */
	private ShopLayoutDto existLayout(Long layoutId, List<ShopLayoutDto> shopLayoutDtos) {
		if (AppUtils.isBlank(shopLayoutDtos)) {
			return null;
		}
		for (ShopLayoutDto shopLayoutDto : shopLayoutDtos) {
			if (layoutId.equals(shopLayoutDto.getLayoutId())) {
				return shopLayoutDto;
			}
		}
		return null;
	}

	/**
	 * 是否包含该模块
	 * 
	 * @param layoutId
	 * @param shopLayoutDtos
	 * @return
	 */
	private ShopLayoutDivDto existDivLayout(Long divId, List<ShopLayoutDivDto> layoutDivDtos) {
		if (AppUtils.isBlank(layoutDivDtos)) {
			return null;
		}
		for (ShopLayoutDivDto divDto : layoutDivDtos) {
			if (divId.equals(divDto.getLayoutDivId())) {
				return divDto;
			}
		}
		return null;
	}

}
