/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.exception;

/**
 * Cvs映射策略异常
 */
public class CvsMappingStrategyException extends RuntimeException {

	private static final long serialVersionUID = -5798235546770643504L;

	public CvsMappingStrategyException() {
		// TODO Auto-generated constructor stub
	}

	public CvsMappingStrategyException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CvsMappingStrategyException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public CvsMappingStrategyException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CvsMappingStrategyException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
