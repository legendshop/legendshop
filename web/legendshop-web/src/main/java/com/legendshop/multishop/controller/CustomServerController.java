/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.multishop.controller;

import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.entity.im.CustomServer;
import com.legendshop.multishop.page.ShopFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.websocket.service.CustomServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * The Class CustomServerController 控制器
 */
@Controller
@RequestMapping("/s/customServer")
public class CustomServerController {

	@Autowired
	private CustomServerService customServerService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@RequestMapping(method = RequestMethod.GET)
	public String index(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(ShopFrontPage.CUSTOM_SERVER);
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public String list(HttpServletRequest request, HttpServletResponse response, String name, String account, String curPageNO) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		PageSupport<CustomServer> ps = customServerService.search(shopId, name, account, curPageNO);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(ShopFrontPage.CUSTOM_SERVERLIST);
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String add(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(ShopFrontPage.CUSTOM_SERVER_ADD);
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		CustomServer customServer = customServerService.getCustomServer(shopId, id);

		if (customServer == null || !customServer.getShopId().equals(shopId)) {
			return "redirect:/s/customServer/list";
		}
		request.setAttribute("item", customServer);
		return PathResolver.getPath(ShopFrontPage.CUSTOM_SERVER_UPDATE);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public String save(HttpServletRequest request, CustomServer customServer) {
		SecurityUserDetail user = UserManager.getUser(request);
		CustomServer old = customServerService.getCustomServer(user.getShopId(), customServer.getId());
		if (customServer.getId() != null && !old.getShopId().equals(user.getShopId())) {
			return "fail";
		}

		CustomServer exists = customServerService.getCustomServer(user.getShopId(), customServer.getAccount());
		if (exists != null && (customServer.getId() == null || !customServer.getId().equals(exists.getId()))) {
			return "账号已存在";
		}

		if (old != null) {
			customServer.setAccount(old.getAccount());
		}

		customServer.setShopId(user.getShopId());
		customServer.setCreateTime(new Date());

		if (customServer.getPassword() != null && !"".equals(customServer.getPassword())) {
			customServer.setPassword(passwordEncoder.encode(customServer.getPassword()));
		} else {
			customServer.setPassword(old.getPassword());
		}

		customServerService.saveCustomServer(customServer);
		return "ok";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto delete(HttpServletRequest request, Long id) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		CustomServer customServer = customServerService.getCustomServer(shopId, id);

		ResultDto result = new ResultDto();
		result.setStatus(-1);
		if (customServer != null && !customServer.getShopId().equals(shopId)) {
			result.setMsg("删除失败");
			return result;
		}
		// TODO 不允许删除最后一个客服
		Integer count = customServerService.getCountByShopId(shopId);
		if (count <= 1) {
			result.setMsg("不允许删除最后一个客服");
			return result;
		}
		customServerService.deleteCustomServer(customServer);
		result.setStatus(1);
		return result;
	}

	/**
	 * 更改密码页面
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updatePassword/{id}", method = RequestMethod.GET)
	public String updatePassword(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

		request.setAttribute("customServerId", id);
		return PathResolver.getPath(ShopFrontPage.CUSTOM_SERVER_UPDATE_PSW);
	}

	@RequestMapping(value = "/updatePsw", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto updatePsw(HttpServletRequest request, CustomServer customServer) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		CustomServer originCustomServer = customServerService.getCustomServer(shopId, customServer.getId());

//		CustomServer originCustomServer = customServerService.getCustomServer(customServer.getId());

//		if (!passwordEncoder.matches(customServer.getPassword(), originCustomServer.getPassword())) {
//			return "您输入的旧密码不正确，请重新输入！";
//		}
		originCustomServer.setPassword(passwordEncoder.encode(customServer.getNewPassword()));
		customServerService.updateCustomServer(originCustomServer);
		ResultDto result = new ResultDto();
		result.setStatus(1);
		return result;
	}

}
