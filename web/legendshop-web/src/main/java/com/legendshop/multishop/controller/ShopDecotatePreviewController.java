/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.entity.DvyTypeCommStat;
import com.legendshop.model.entity.ProductCommentStat;
import com.legendshop.model.entity.ShopCommStat;
import com.legendshop.spi.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ShopLayoutDivTypeEnum;
import com.legendshop.model.constant.ShopLayoutModuleTypeEnum;
import com.legendshop.model.constant.ShopLayoutTypeEnum;
import com.legendshop.model.dto.shopDecotate.ShopDecotateDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutCateogryDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutDivDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutHotDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutHotProdDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutModuleDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutProdDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutShopInfoDto;
import com.legendshop.model.dto.shopDecotate.StoreListDto;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.shopDecotate.ShopLayoutBanner;
import com.legendshop.model.entity.shopDecotate.ShopLayoutParam;
import com.legendshop.model.entity.shopDecotate.ShopNav;
import com.legendshop.multishop.page.DecorateFrontPage;
import com.legendshop.util.AppUtils;

import static com.legendshop.multishop.page.DecorateFrontPage.DECORATE_PREVIEW;

/**
 * 店铺主页.
 *
 */
@Controller
public class ShopDecotatePreviewController extends BaseController {

	private final static Logger log = LoggerFactory.getLogger(ShopDecotatePreviewController.class);

	@Autowired
	private ShopDecotateService shopDecotateService;
	
	@Autowired
	private ShopDecotateCacheService shopDecotateCacheService;

	@Autowired
	private ShopNavService shopNavService;

	@Autowired
	private ShopLayoutBannerService shopLayoutBannerService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private ShopLayoutService shopLayoutService;

	@Autowired
	private ProductCommentStatService productCommentStatService;

	@Autowired
	private ShopCommStatService shopCommStatService;

	@Autowired
	private DvyTypeCommStatService dvyTypeCommStatService;

	/**
	 * 进入店铺首页.
	 *
	 * @param request the request
	 * @param response the response
	 * @param shopId the shop id
	 * @return the string
	 */
	@RequestMapping(value = "/shop/index/{shopId}")
	public String query(HttpServletRequest request, HttpServletResponse response, @PathVariable Long shopId) {
		/*
		 * 获取有没有装修过
		 */
		Float shopscore=0f;
		Float prodscore=0f;
		Float logisticsScore=0f;
		DecimalFormat decimalFormat = new DecimalFormat("0.0");
		decimalFormat.setRoundingMode(RoundingMode.FLOOR);

		//计算该店铺所有已评分商品的平均分
		ProductCommentStat prodCommentStat = productCommentStatService.getProductCommentStatByShopId(shopId);
		if(prodCommentStat!=null&&prodCommentStat.getScore()!=null&&prodCommentStat.getComments()!=null){
			prodscore=(float)prodCommentStat.getScore()/prodCommentStat.getComments();
		}

		//计算该店铺所有已评分店铺的平均分
		ShopCommStat shopCommentStat = shopCommStatService.getShopCommStatByShopId(shopId);
		if(shopCommentStat!=null&&shopCommentStat.getScore()!=null&&shopCommentStat.getCount()!=null){
			shopscore=(float)shopCommentStat.getScore()/shopCommentStat.getCount();
		}
		//计算该店铺所有已评分物流的平均分
		DvyTypeCommStat dvyTypeCommStat = dvyTypeCommStatService.getDvyTypeCommStatByShopId(shopId);
		if(dvyTypeCommStat!=null&&dvyTypeCommStat.getScore()!=null&&dvyTypeCommStat.getCount()!=null){
			logisticsScore=(float)dvyTypeCommStat.getScore()/dvyTypeCommStat.getCount();
		}
		Float sum=0f;
		if (shopscore!=0&&prodscore!=0&&logisticsScore!=0) {
			sum=(float)Math.floor((shopscore+prodscore+logisticsScore)/3);
		}else if(shopscore!=0&&logisticsScore!=0){
			sum=(float)Math.floor((shopscore+logisticsScore)/2);
		}
		request.setAttribute("sum",decimalFormat.format(sum));

		Long count = shopLayoutService.getLayoutCountOnline(shopId, null);
		if (count == 0) {
			StoreListDto storeDto = shopDecotateService.findStoreListDto(shopId);
			request.setAttribute("shopDecotate", storeDto);
			String path = PathResolver.getPath(DecorateFrontPage.STORE_LIST);
			return path;
		}
		final ShopDecotateDto decotateDto = shopDecotateService.findOnlineShopDecotateCache(shopId);
		if (decotateDto == null) {
			StoreListDto storeDto = shopDecotateService.findStoreListDto(shopId);
			request.setAttribute("shopDecotate", storeDto);
			String path = PathResolver.getPath(DecorateFrontPage.STORE_LIST);
			return path;
		}
		request.setAttribute("shopDecotate", decotateDto);
		request.setAttribute("preview", false);
		request.setAttribute("shopId", decotateDto.getShopId());
		String path = PathResolver.getPath(DECORATE_PREVIEW);
		return path;
	}

	/**
	 * 店铺装修主页.
	 *
	 * @param request the request
	 * @param response the response
	 * @param shopId the shop id
	 * @param keyword the keyword
	 * @return the string
	 */
	@RequestMapping(value = "/store/list/{shopId}")
	public String storeList(HttpServletRequest request, HttpServletResponse response, @PathVariable Long shopId, String keyword) {
		ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
		if (shopDetail == null || shopDetail.getStatus() == 0) {
			throw new BusinessException("shop is not find by shopId " + shopId);
		}
		StoreListDto storeDto = shopDecotateService.findStoreListDto(shopId);
		request.setAttribute("shopDecotate", storeDto);
		request.setAttribute("fireCat", request.getParameter("fireCat"));
		request.setAttribute("secondCat", request.getParameter("secondCat"));
		request.setAttribute("thirdCat", request.getParameter("thirdCat"));
		request.setAttribute("keyword", keyword);
		request.setAttribute("shopId", shopId);
		String path = PathResolver.getPath(DecorateFrontPage.STORE_LIST);
		return path;
	}

	/**
	 * 加载布局模块
	 *
	 * @param request the request
	 * @param response the response
	 * @param shopId the shop id
	 * @param layoutParam the layout param
	 * @return the string
	 */
	@RequestMapping(value = "/loading/module/{shopId}")
	public String lodingLayouModule(HttpServletRequest request, HttpServletResponse response, @PathVariable Long shopId, ShopLayoutParam layoutParam) {
		String type = layoutParam.getLayoutModuleType();
		String msg = Constants.FAIL;
		if (shopId == null || type == null) {
			request.setAttribute("msg", msg);
			return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_ERROR);
		}
		ShopDecotateDto decotateDto = shopDecotateService.findOnlineShopDecotateCache(shopId);
		if (decotateDto == null) {
			request.setAttribute("msg", msg);
			return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_ERROR);
		}
		layoutParam.setShopId(shopId);
		layoutParam.setDecId(decotateDto.getDecId());
		Long layoutId = layoutParam.getLayoutId();
		String layout = layoutParam.getLayout();
		List<ShopLayoutDto> shopLayoutDtos = null;
		if (ShopLayoutTypeEnum.LAYOUT_0.value().equals(layout)) {
			shopLayoutDtos = decotateDto.getTopShopLayouts();
		} else if (ShopLayoutTypeEnum.LAYOUT_1.value().equals(layout)) {
			shopLayoutDtos = decotateDto.getBottomShopLayouts();
		} else {
			shopLayoutDtos = decotateDto.getMainShopLayouts();
		}
		ShopLayoutDto layoutDto = existLayout(layoutId, shopLayoutDtos);
		if (layoutDto == null) {
			log.warn(" error  layoutDto is null ");
			request.setAttribute("msg", msg);
			return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_ERROR);
		}
		ShopLayoutModuleDto moduleDto = layoutDto.getShopLayoutModule();
		String path = null;
		if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_NAV.value().equals(type)) {
			List<ShopNav> shopNavs = moduleDto.getShopNavs();
			if (AppUtils.isBlank(shopNavs)) { // if cache not exist
				shopNavs = shopNavService.getLayoutNav(layoutParam.getShopId());
				moduleDto.setShopNavs(null);
				moduleDto.setShopNavs(shopNavs);
			}
			request.setAttribute("shopNavs", shopNavs);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_NAV);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_BANNER.value().equals(type)) {
			List<ShopLayoutBanner> layoutBanners = moduleDto.getShopLayoutBanners();
			if (AppUtils.isBlank(layoutBanners)) {
				layoutBanners = shopLayoutBannerService.getShopLayoutBanner(decotateDto.getDecId(), layoutId);
				moduleDto.setShopLayoutBanners(null);
				moduleDto.setShopLayoutBanners(layoutBanners);
			}
			request.setAttribute("shopLayoutBanners", layoutBanners);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_BANNER);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CATEGORY.value().equals(type)
				&& ShopLayoutDivTypeEnum.DIV_1.value().equals(layoutParam.getLayoutDiv())) {
			List<ShopLayoutCateogryDto> cateogryDtos = moduleDto.getCateogryDtos();
			if (AppUtils.isBlank(cateogryDtos)) {
				cateogryDtos = shopDecotateService.findShopCateogryDtos(layoutParam, layoutDto);
			}
			request.setAttribute("cateogryDtos", cateogryDtos);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_CATEGORY);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_HOT_PROD.value().equals(type)
				&& ShopLayoutDivTypeEnum.DIV_1.value().equals(layoutParam.getLayoutDiv())) {
			List<ShopLayoutHotProdDto> hotProdDtos = moduleDto.getHotProds();
			if (AppUtils.isBlank(hotProdDtos)) {
				hotProdDtos = shopDecotateService.findShopHotProds(layoutParam, layoutDto);
			}
			request.setAttribute("hotProds", hotProdDtos);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_HOT_PROD);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_SHOPINFO.value().equals(type)
				&& ShopLayoutDivTypeEnum.DIV_1.value().equals(layoutParam.getLayoutDiv())) {
			ShopLayoutShopInfoDto shopInfo = moduleDto.getShopInfo();
			if (AppUtils.isBlank(shopInfo)) {
				shopInfo = shopDecotateService.findShopInfo(layoutParam, layoutDto);
			}
			request.setAttribute("shopInfo", shopInfo);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_SHOPINFO);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CUSTOM_PROD.value().equals(type)) {
			List<ShopLayoutProdDto> prodDtos = moduleDto.getLayoutProdDtos();
			if (AppUtils.isBlank(prodDtos)) {
				prodDtos = shopDecotateService.findShopLayoutProdDtos(layoutParam, layoutDto);
			}
			request.setAttribute("title", layoutDto.getTitle());
			request.setAttribute("fontImg", layoutDto.getFontImg());
			request.setAttribute("fontColor", layoutDto.getFontColor());
			request.setAttribute("backColor", layoutDto.getBackColor());
			request.setAttribute("prods", prodDtos);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_PROD);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CUSTOM_DESC.value().equals(type)) {
			String content = null;
			if (AppUtils.isNotBlank(layoutParam.getDivId())) {
				ShopLayoutDivDto divDto = existDivLayout(layoutParam.getDivId(), layoutDto.getShopLayoutDivDtos());
				if (divDto != null) {
					content = divDto.getLayoutContent();
				}
			} else {
				content = moduleDto.getLayoutContent();
			}
			request.setAttribute("layoutContent", content);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_CUST_CONTENT);
		} else if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_HOT.value().equals(type)) {
			ShopLayoutHotDto shopLayoutHotDto = moduleDto.getShopLayoutHotDto();
			if (AppUtils.isBlank(shopLayoutHotDto)) {
				shopLayoutHotDto = shopDecotateService.findShopLayoutHotDto(layoutParam, layoutDto);
			}
			request.setAttribute("hots", shopLayoutHotDto);
			path = PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_HOT_LOAD);
		}
		shopDecotateCacheService.cachePutOnlineShopDecotate(decotateDto);
		request.setAttribute("layoutParam", layoutParam);
		return path;
	}

	/**
	 * 是否包含该模块.
	 *
	 * @param layoutId the layout id
	 * @param shopLayoutDtos the shop layout dtos
	 * @return the shop layout dto
	 */
	private ShopLayoutDto existLayout(Long layoutId, List<ShopLayoutDto> shopLayoutDtos) {
		if (AppUtils.isBlank(shopLayoutDtos)) {
			return null;
		}
		for (ShopLayoutDto shopLayoutDto : shopLayoutDtos) {
			if (layoutId.equals(shopLayoutDto.getLayoutId())) {
				return shopLayoutDto;
			}
		}
		return null;
	}

	/**
	 * 是否包含该模块.
	 *
	 * @param divId the div id
	 * @param layoutDivDtos the layout div dtos
	 * @return the shop layout div dto
	 */
	private ShopLayoutDivDto existDivLayout(Long divId, List<ShopLayoutDivDto> layoutDivDtos) {
		if (AppUtils.isBlank(layoutDivDtos)) {
			return null;
		}
		for (ShopLayoutDivDto divDto : layoutDivDtos) {
			if (divId.equals(divDto.getLayoutDivId())) {
				return divDto;
			}
		}
		return null;
	}
}
