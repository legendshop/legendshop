/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.shopDecotate.ShopBanner;
import com.legendshop.multishop.page.DecorateFrontPage;
import com.legendshop.multishop.page.DecorateRedirectPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ShopBannerService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 店铺轮播图
 *
 */
@Controller
@RequestMapping("/s/shopBanner")
public class ShopBannerController extends BaseController {
    
	@Autowired
    private ShopBannerService shopBannerService;
    
    @Autowired
    private AttachmentManager attachmentManager;

    /**
     * 查询商家店铺轮播图片列表
     * @param request
     * @param response
     * @param curPageNO
     * @param shopBanner
     * @return
     */
    @RequestMapping("/list")
    @Authorize(authorityTypes = AuthorityType.SHOP_BANNER)
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, ShopBanner shopBanner) {
    	
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
        PageSupport<ShopBanner> ps = shopBannerService.getShopBanner(curPageNO,shopId);
        PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(DecorateFrontPage.SHOP_BANNE_LIST);
    }

    /**
     * 保存商家店铺轮播图片
     * @param request
     * @param response
     * @param shopBanner
     * @return
     */
    @RequestMapping(value = "/save")
    @Authorize(authorityTypes = AuthorityType.SHOP_BANNER)
    public String save(HttpServletRequest request, HttpServletResponse response, ShopBanner shopBanner) {
    	
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
        shopBanner.setShopId(shopId);
        String brandPic = null; //
    	if (shopBanner.getFile() != null && shopBanner.getFile().getSize() > 0) {
			brandPic = attachmentManager.upload(shopBanner.getFile());
    	}
        if (!AppUtils.isBlank(shopBanner.getId())) {
        	ShopBanner orginBanner=shopBannerService.getShopBanner(shopBanner.getId());
        	if (shopBanner.getFile() != null && shopBanner.getFile().getSize() > 0) {
        		if(AppUtils.isNotBlank(brandPic)){
        			if(AppUtils.isNotBlank(orginBanner.getImageFile())){
        				//删除
        				attachmentManager.delAttachmentByFilePath(orginBanner.getImageFile());
        			}
        			orginBanner.setImageFile(brandPic);
        		}
    		}
        	orginBanner.setSeq(shopBanner.getSeq());
			orginBanner.setUrl(shopBanner.getUrl());
			shopBannerService.updateShopBanner(orginBanner);
        }else{
        	shopBanner.setImageFile(brandPic);
        	shopBannerService.saveShopBanner(shopBanner);
        }
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
        return PathResolver.getPath(DecorateRedirectPage.SHOP_BANNER_LIST);
    }

    /**
     * 删除商家店铺轮播图片
     * @param request
     * @param response
     * @param id
     * @return
     */
    @RequestMapping(value = "/delete/{id}")
    @Authorize(authorityTypes = AuthorityType.SHOP_BANNER)
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        ShopBanner shopBanner= shopBannerService.getShopBanner(id);
        shopBannerService.deleteShopBanner(shopBanner);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
        return PathResolver.getPath(DecorateRedirectPage.SHOP_BANNER_LIST);
        
    }

    /**
     * 查看商家店铺轮播图片详情
     * @param request
     * @param response
     * @param id
     * @return
     */
    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        ShopBanner shopBanner = shopBannerService.getShopBanner(id);
        request.setAttribute("bean", shopBanner);
        return PathResolver.getPath(DecorateFrontPage.SHOP_BANNER_ADD);
        
    }
    
    /**
     * 加载商家店铺轮播图片编辑页面
     * @param request
     * @param response
     * @return
     */
	@RequestMapping(value = "/load")
	@Authorize(authorityTypes = AuthorityType.SHOP_BANNER)
	public String load(HttpServletRequest request, HttpServletResponse response) {
		 return PathResolver.getPath(DecorateFrontPage.SHOP_BANNER_ADD);
	}

}
