/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.multishop.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ShopLayoutModuleTypeEnum;
import com.legendshop.model.constant.ShopLayoutTypeEnum;
import com.legendshop.model.dto.shopDecotate.HotDataDto;
import com.legendshop.model.dto.shopDecotate.ShopDecotateDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutDivDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutHotDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutModuleDto;
import com.legendshop.model.dto.shopDecotate.ShopLayoutProdDto;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.shopDecotate.ShopLayoutBanner;
import com.legendshop.model.entity.shopDecotate.ShopLayoutBannerParam;
import com.legendshop.model.entity.shopDecotate.ShopLayoutContentParam;
import com.legendshop.model.entity.shopDecotate.ShopLayoutHotParam;
import com.legendshop.model.entity.shopDecotate.ShopLayoutModuleSetParam;
import com.legendshop.model.entity.shopDecotate.ShopLayoutProdParam;
import com.legendshop.multishop.page.DecorateFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ShopDecotateCacheService;
import com.legendshop.spi.service.ShopDecotateService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 店铺装修
 */
@Controller
@RequestMapping("/s/shopDecotate/layout")
public class ShopDecotateLayoutController extends BaseController {

    @Autowired
    private ShopDecotateService shopDecotateService;

    @Autowired
    private ShopDecotateCacheService shopDecotateCacheService;

    @Autowired
    private AttachmentManager attachmentManager;

    /**
     * 模块Banner保存
     *
     * @param request
     * @param response
     * @param layoutParam
     * @return
     */
    @RequestMapping(value = "/bannerSave")
    public String decorateModuleSave(HttpServletRequest request, HttpServletResponse response, ShopLayoutBannerParam layoutParam) {
        String msg = Constants.FAIL;
        if (AppUtils.isBlank(layoutParam.getImg_files())) {
            request.setAttribute("msg", msg);
            return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_ERROR);
        }
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
        ShopDecotateDto decotateDto = shopDecotateService.findShopDecotateCache(shopId);
        if (decotateDto == null) {
            request.setAttribute("msg", msg);
            return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_ERROR);
        }
        layoutParam.setShopId(shopId);
        layoutParam.setDecId(decotateDto.getDecId());
        List<ShopLayoutDto> shopLayoutDtos = null;
        if (ShopLayoutTypeEnum.LAYOUT_0.value().equals(layoutParam.getLayout())) {
            shopLayoutDtos = decotateDto.getTopShopLayouts();
        } else if (ShopLayoutTypeEnum.LAYOUT_1.value().equals(layoutParam.getLayout())) {
            shopLayoutDtos = decotateDto.getBottomShopLayouts();
        } else {
            shopLayoutDtos = decotateDto.getMainShopLayouts();
        }

        ShopLayoutDto layoutDto = existLayout(layoutParam.getLayoutId(), shopLayoutDtos);
        if (AppUtils.isBlank(layoutDto)) {
            request.setAttribute("msg", msg);
            return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_ERROR);
        }
        ShopLayoutModuleDto layoutModuleDto = layoutDto.getShopLayoutModule();
        if (AppUtils.isBlank(layoutModuleDto)) {
            request.setAttribute("msg", msg);
            return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_ERROR);
        }
        if (ShopLayoutModuleTypeEnum.LOYOUT_MODULE_BANNER.value().equals(layoutParam.getLayoutModuleType())) {
            layoutDto.setLayoutModuleType(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_BANNER.value());
            List<ShopLayoutBanner> shopLayoutBanners = new ArrayList<ShopLayoutBanner>();
            String[] fils = layoutParam.getImg_files();
            for (int i = 0; i < fils.length; i++) {
                ShopLayoutBanner banner = new ShopLayoutBanner();
                banner.setImageFile(fils[i]);
                banner.setUrl(layoutParam.getUrls()[i]);
                banner.setShopDecotateId(decotateDto.getDecId());
                banner.setSeq(i);
                banner.setShopId(shopId);
                banner.setLayoutId(layoutParam.getLayoutId());
                shopLayoutBanners.add(banner);
            }
            layoutModuleDto.setShopLayoutBanners(null);
            layoutModuleDto.setShopLayoutBanners(shopLayoutBanners);
            request.setAttribute("shopLayoutBanners", shopLayoutBanners);
        }
        if (ShopLayoutTypeEnum.LAYOUT_3.value().equals(layoutParam.getLayout()) || ShopLayoutTypeEnum.LAYOUT_4.value().equals(layoutParam.getLayout())) {
            List<ShopLayoutDivDto> divDtos = layoutDto.getShopLayoutDivDtos();
            if (AppUtils.isNotBlank(divDtos)) {
                for (ShopLayoutDivDto shopLayoutDivDto : divDtos) {
                    if (shopLayoutDivDto.getLayoutDivId().equals(layoutParam.getDivId())) {
                        shopLayoutDivDto.setLayoutModuleType(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_BANNER.value());
                    }
                }
            }
        }
        shopDecotateCacheService.cachePutShopDecotate(decotateDto);
        request.setAttribute("layoutParam", layoutParam);
        request.setAttribute("isadmin", "1");
        return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_BANNER);
    }

    /**
     * 加载滑动布局
     *
     * @param request
     * @param response
     * @param img_file_1
     * @param img_file_2
     * @param img_file_3
     * @param img_file_4
     * @param img_file_5
     * @return
     */
    @RequestMapping(value = "/layout_slide_upload")
    public @ResponseBody
    String layout_slide_upload(MultipartHttpServletRequest request, HttpServletResponse response, MultipartFile img_file_1,
                               MultipartFile img_file_2, MultipartFile img_file_3, MultipartFile img_file_4, MultipartFile img_file_5) {
        if (AppUtils.isNotBlank(img_file_1) || AppUtils.isNotBlank(img_file_2) || AppUtils.isNotBlank(img_file_3) || AppUtils.isNotBlank(img_file_4)
                || AppUtils.isNotBlank(img_file_5)) {
            MultipartFile file = null;
            if (AppUtils.isNotBlank(img_file_1)) {
                file = img_file_1;
            } else if (AppUtils.isNotBlank(img_file_2)) {
                file = img_file_2;
            } else if (AppUtils.isNotBlank(img_file_3)) {
                file = img_file_3;
            } else if (AppUtils.isNotBlank(img_file_4)) {
                file = img_file_4;
            } else if (AppUtils.isNotBlank(img_file_5)) {
                file = img_file_5;
            }
            if (file.getSize() > 0) {
                String pic = attachmentManager.upload(file);
                return pic;
            } else {
                return Constants.FAIL;
            }
        } else {
            return Constants.FAIL;
        }
    }

    /**
     * 是否包含该模块
     *
     * @param layoutId
     * @param shopLayoutDtos
     * @return
     */
    private ShopLayoutDto existLayout(Long layoutId, List<ShopLayoutDto> shopLayoutDtos) {
        for (ShopLayoutDto shopLayoutDto : shopLayoutDtos) {
            if (layoutId.equals(shopLayoutDto.getLayoutId())) {
                return shopLayoutDto;
            }
        }
        return null;
    }

    /**
     * 保存自定义商品
     *
     * @param request
     * @param response
     * @param layoutProdParam
     * @return
     */
    @RequestMapping(value = "/saveLayoutProduct", method = RequestMethod.POST)
    public String saveLayoutProduct(HttpServletRequest request, HttpServletResponse response, ShopLayoutProdParam layoutProdParam) {
        String msg = Constants.FAIL;
        if (AppUtils.isBlank(layoutProdParam.getProducts())) {
            request.setAttribute("msg", msg);
            return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_ERROR);
        }
        List<ShopLayoutProdDto> productList = JSONUtil.getArray(layoutProdParam.getProducts(), ShopLayoutProdDto.class);
        if (AppUtils.isBlank(productList)) {
            request.setAttribute("msg", msg);
            return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_ERROR);
        }
        if (ShopLayoutTypeEnum.LAYOUT_0.value().equals(layoutProdParam.getLayout())
                || ShopLayoutTypeEnum.LAYOUT_1.value().equals(layoutProdParam.getLayout())) {
            request.setAttribute("msg", "LAYOUT_ERROR");
            return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_ERROR);
        }

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
        ShopDecotateDto decotateDto = shopDecotateService.findShopDecotateCache(shopId);
        if (decotateDto == null) {
            request.setAttribute("msg", msg);
            return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_ERROR);
        }

        layoutProdParam.setShopId(shopId);
        layoutProdParam.setDecId(decotateDto.getDecId());
        List<ShopLayoutDto> shopLayoutDtos = decotateDto.getMainShopLayouts();
        ;
        ShopLayoutDto layoutDto = existLayout(layoutProdParam.getLayoutId(), shopLayoutDtos);
        if (AppUtils.isBlank(layoutDto)) {
            request.setAttribute("msg", msg);
            return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_ERROR);
        }
        ShopLayoutModuleDto layoutModuleDto = layoutDto.getShopLayoutModule();
        if (AppUtils.isBlank(layoutModuleDto)) {
            request.setAttribute("msg", msg);
            return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_ERROR);
        }
        if (ShopLayoutTypeEnum.LAYOUT_3.value().equals(layoutProdParam.getLayout())
                || ShopLayoutTypeEnum.LAYOUT_4.value().equals(layoutProdParam.getLayout())) {
            List<ShopLayoutDivDto> divDtos = layoutDto.getShopLayoutDivDtos();
            if (AppUtils.isNotBlank(divDtos)) {
                for (ShopLayoutDivDto shopLayoutDivDto : divDtos) {
                    if (shopLayoutDivDto.getLayoutDivId().equals(layoutProdParam.getDivId())) {
                        shopLayoutDivDto.setLayoutModuleType(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CUSTOM_PROD.value());
                    }
                }
            }
        }
        layoutDto.setLayoutModuleType(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CUSTOM_PROD.value());
        layoutModuleDto.setLayoutProdDtos(productList);
        shopDecotateCacheService.cachePutShopDecotate(decotateDto);
        request.setAttribute("layoutParam", layoutProdParam);
        request.setAttribute("prods", productList);
        return PathResolver.getPath(DecorateFrontPage.DECORATE_MODULE_PROD);
    }

    /**
     * 保存自定义商品
     *
     * @param request
     * @param response
     * @param layoutProdParam
     * @return
     */
    @RequestMapping(value = "/saveLayoutContent", method = RequestMethod.POST)
    public @ResponseBody
    String saveLayoutContent(HttpServletRequest request, HttpServletResponse response, ShopLayoutContentParam contentParam) {
        if (AppUtils.isBlank(contentParam.getLayoutContent())) {
            return Constants.FAIL;
        }

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
        
        ShopDecotateDto decotateDto = shopDecotateService.findShopDecotateCache(shopId);
        if (decotateDto == null) {
            return Constants.FAIL;
        }
        contentParam.setShopId(shopId);
        contentParam.setDecId(decotateDto.getDecId());
        List<ShopLayoutDto> shopLayoutDtos = null;
        if (ShopLayoutTypeEnum.LAYOUT_0.value().equals(contentParam.getLayout())) {
            shopLayoutDtos = decotateDto.getTopShopLayouts();
        } else if (ShopLayoutTypeEnum.LAYOUT_1.value().equals(contentParam.getLayout())) {
            shopLayoutDtos = decotateDto.getBottomShopLayouts();
        } else {
            shopLayoutDtos = decotateDto.getMainShopLayouts();
        }
        ShopLayoutDto layoutDto = existLayout(contentParam.getLayoutId(), shopLayoutDtos);
        if (AppUtils.isBlank(layoutDto)) {
            return Constants.FAIL;
        }
        ShopLayoutModuleDto layoutModuleDto = layoutDto.getShopLayoutModule();
        if (AppUtils.isBlank(layoutModuleDto)) {
            return Constants.FAIL;
        }
        if (ShopLayoutTypeEnum.LAYOUT_3.value().equals(contentParam.getLayout()) || ShopLayoutTypeEnum.LAYOUT_4.value().equals(contentParam.getLayout())) {
            ShopLayoutDivDto divDto = existDivLayout(contentParam.getDivId(), layoutDto.getShopLayoutDivDtos());
            if (AppUtils.isNotBlank(divDto)) {
                divDto.setLayoutModuleType(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CUSTOM_DESC.value());
                divDto.setLayoutContent(null);
                divDto.setLayoutContent(contentParam.getLayoutContent());
            }
            layoutModuleDto.setLayoutContent(null);
            layoutDto.setLayoutModuleType(null);
            layoutDto.setShopLayoutModule(null);
        } else {
            layoutDto.setLayoutModuleType(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_CUSTOM_DESC.value());
            layoutModuleDto.setLayoutContent(null);
            layoutModuleDto.setLayoutContent(contentParam.getLayoutContent());
        }
        layoutDto.setShopLayoutModule(layoutModuleDto);
        shopDecotateCacheService.cachePutShopDecotate(decotateDto);
        return Constants.SUCCESS;
    }

    /**
     * 模块设置
     *
     * @param request
     * @param response
     * @param contentParam
     * @return
     */
    @RequestMapping(value = "/decorateModuleSetSave", method = RequestMethod.POST)
    public @ResponseBody
    String decorateModuleSetSave(HttpServletRequest request, HttpServletResponse response, ShopLayoutModuleSetParam param) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
        ShopDecotateDto decotateDto = shopDecotateService.findShopDecotateCache(shopId);
        if (decotateDto == null) {
            return Constants.FAIL;
        }
        param.setShopId(shopId);
        param.setDecId(decotateDto.getDecId());
        List<ShopLayoutDto> shopLayoutDtos = decotateDto.getMainShopLayouts();
        ShopLayoutDto layoutDto = existLayout(param.getLayoutId(), shopLayoutDtos);
        if (AppUtils.isBlank(layoutDto)) {
            return Constants.FAIL;
        }
        if (AppUtils.isNotBlank(param.getFile()) && param.getFile().getSize() > 0) {
            if (AppUtils.isNotBlank(layoutDto.getFontImg())) {
                attachmentManager.deleteTruely(layoutDto.getFontImg());
            }
            String pic = attachmentManager.upload(param.getFile());
            layoutDto.setFontImg(pic);
        }
        layoutDto.setBackColor(param.getBackColor());
        layoutDto.setFontColor(param.getFontColor());
        layoutDto.setTitle(param.getTitle());
        shopDecotateCacheService.cachePutShopDecotate(decotateDto);
        return Constants.SUCCESS;
    }

    /**
     * 是否包含该模块
     *
     * @param layoutId
     * @param shopLayoutDtos
     * @return
     */
    private ShopLayoutDivDto existDivLayout(Long divId, List<ShopLayoutDivDto> layoutDivDtos) {
        if (AppUtils.isBlank(layoutDivDtos)) {
            return null;
        }
        for (ShopLayoutDivDto divDto : layoutDivDtos) {
            if (divId.equals(divDto.getLayoutDivId())) {
                return divDto;
            }
        }
        return null;
    }

    /**
     * 加载热点背景
     *
     * @param request
     * @param response
     * @param bg_img
     * @return
     */
    @RequestMapping(value = "/decorate_hot_upload")
    public @ResponseBody
    Map<String, String> backgroundHotUpload(MultipartHttpServletRequest request, HttpServletResponse response, MultipartFile bg_img) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("status", Constants.FAIL);
        if (AppUtils.isNotBlank(bg_img)) {
            if (bg_img.getSize() > 0) {
        		SecurityUserDetail user = UserManager.getUser(request);
        		Long shopId = user.getShopId();
        		
                ShopDecotateDto decotateDto = shopDecotateService.findShopDecotateCache(shopId);
                if (decotateDto == null) {
                    map.put("status", Constants.FAIL);
                } else {
                    BufferedImage bufferedImg;
                    int imgWidth = 300;
                    int imgHeight = 300;
                    try {
                        bufferedImg = ImageIO.read(bg_img.getInputStream());
                        imgWidth = bufferedImg.getWidth();
                        imgHeight = bufferedImg.getHeight();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                    }
                    String pic = attachmentManager.upload(bg_img);
                    bufferedImg = null;
                    map.put("status", Constants.SUCCESS);
                    map.put("pic", pic);
                    map.put("width", String.valueOf(imgWidth));
                    map.put("height", String.valueOf(imgHeight));
                }
            }
        }
        return map;
    }

    /**
     * 保存热点模块
     *
     * @param request
     * @param response
     * @param layoutProdParam
     * @return
     */
    @RequestMapping(value = "/saveLayoutHot", method = RequestMethod.POST)
    public @ResponseBody
    String saveLayoutHot(HttpServletRequest request, HttpServletResponse response, ShopLayoutHotParam layoutHotParam) {
        if (AppUtils.isBlank(layoutHotParam.getCoorDatas()) || AppUtils.isBlank(layoutHotParam.getImageFile())) {
            return Constants.FAIL;
        }
        List<HotDataDto> dataDtos = JSONUtil.getArray(layoutHotParam.getCoorDatas(), HotDataDto.class);
        if (AppUtils.isBlank(dataDtos)) {
            return Constants.FAIL;
        }
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
        ShopDecotateDto decotateDto = shopDecotateService.findShopDecotateCache(shopId);
        if (decotateDto == null) {
            return Constants.FAIL;
        }
        layoutHotParam.setShopId(shopId);
        layoutHotParam.setDecId(decotateDto.getDecId());
        List<ShopLayoutDto> shopLayoutDtos = null;
        if (ShopLayoutTypeEnum.LAYOUT_0.value().equals(layoutHotParam.getLayout())) {
            shopLayoutDtos = decotateDto.getTopShopLayouts();
        } else if (ShopLayoutTypeEnum.LAYOUT_1.value().equals(layoutHotParam.getLayout())) {
            shopLayoutDtos = decotateDto.getBottomShopLayouts();
        } else {
            shopLayoutDtos = decotateDto.getMainShopLayouts();
        }
        ShopLayoutDto layoutDto = existLayout(layoutHotParam.getLayoutId(), shopLayoutDtos);
        if (AppUtils.isBlank(layoutDto)) {
            return Constants.FAIL;
        }
        ShopLayoutModuleDto layoutModuleDto = layoutDto.getShopLayoutModule();
        if (AppUtils.isBlank(layoutModuleDto)) {
            return Constants.FAIL;
        }
        if (ShopLayoutTypeEnum.LAYOUT_3.value().equals(layoutHotParam.getLayout()) || ShopLayoutTypeEnum.LAYOUT_4.value().equals(layoutHotParam.getLayout())) {
            List<ShopLayoutDivDto> divDtos = layoutDto.getShopLayoutDivDtos();
            if (AppUtils.isNotBlank(divDtos)) {
                for (ShopLayoutDivDto shopLayoutDivDto : divDtos) {
                    if (shopLayoutDivDto.getLayoutDivId().equals(layoutHotParam.getDivId())) {
                        shopLayoutDivDto.setLayoutModuleType(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_HOT.value());
                    }
                }
            }
            layoutDto.setLayoutModuleType(null);
        } else {
            layoutDto.setLayoutModuleType(ShopLayoutModuleTypeEnum.LOYOUT_MODULE_HOT.value());
        }

        ShopLayoutHotDto hotDto = layoutModuleDto.getShopLayoutHotDto();
        if (hotDto == null) {
            hotDto = new ShopLayoutHotDto();
        }
        hotDto.setHotId(hotDto.getHotId());
        hotDto.setImageFile(layoutHotParam.getImageFile());
        hotDto.setHotDatas(layoutHotParam.getCoorDatas());
        hotDto.setLayoutDivId(layoutHotParam.getDivId());
        hotDto.setLayoutId(layoutHotParam.getLayoutId());
        hotDto.setDataDtos(dataDtos);
        layoutModuleDto.setShopLayoutHotDto(null);
        layoutModuleDto.setShopLayoutHotDto(hotDto);
        layoutDto.setShopLayoutModule(null);
        layoutDto.setShopLayoutModule(layoutModuleDto);
        shopDecotateCacheService.cachePutShopDecotate(decotateDto);

        return Constants.SUCCESS;
    }

    /**
     * 选择店铺优惠卷
     *
     * @param request
     * @param response
     * @param layoutProdParam
     * @return
     */
    @RequestMapping(value = "/selectShopCoupon", method = RequestMethod.GET)
    public String selectShopCoupon(HttpServletRequest request, HttpServletResponse response) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
        List<Coupon> coupons = shopDecotateService.getCouponByShopId(shopId);
        request.setAttribute("coupons", coupons);
        request.setAttribute("shopId", shopId);
        return PathResolver.getPath(DecorateFrontPage.SELECT_SHOP_COUPON);
    }

}
