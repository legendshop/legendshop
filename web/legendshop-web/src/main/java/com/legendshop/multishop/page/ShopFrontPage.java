/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 店铺前台页面定义
 */
public enum ShopFrontPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 商品搜索结果 */
	LOAD_PROD_LIST_BY_TAG("/loadProdListByTag"),

	/** 商品搜索结果 */
	LOAD_PROD_LIST_FOR_TAG("/prodListForTag"),

	/** 标签管理  绑定管理 */
	BIND_PROD_MANAGE("/bindProdManage"),

	/** 标签管理 */
	TAG_MANAGE("/tagManage"),

	/** 标签编辑 */
	EDIT_TAG("/editTag"),

	/** 商品发布 */
	PROD_PUBLISH_PAGE("/publish"),

	/** 商品发布 */
	PROD_PUBLISH2_PAGE("/publish2"),

	/** 加载分类 */
	LOAD_CATEGORY_PAGE("/loadCategory"),

	/** 品牌发布 */
	PROD_PUBLISH4_PAGE("/publish4"),

	/** 评论商品列表 */
	COMMENT_PROD_LIST("/comments/commentProdList"),

	/** 评论列表 */
	COMMENT_LIST("/comments/commentList"),

	/** 评论内容看ie表 */
	COMMENT_LIST_CONTETN("/comments/commentListContent"),

	/** 支付编辑  支付方式 */
	PAY_TYPE("/payType"),

	/** 支付配置 */
	PAY_CONFIGURE("/payConfigure"),

	/** 运费模板列表 */
	TRANSPORT_LIST("/transportList"),

	/** 运费模板 */
	TRANSPORT("/transport"),

	/** 运费模板 */
	TRANSPORT_OVERLAY("/transportOverlay"),

	/** 举报商品 */
	REPORT_FORBIT_OVERLAY("/reportForbitOverlay"),

	/** 销售属性模板管理 */
	ATTRIBUTE_OVERLAY("/attributeOverlay"),

	/** 保存属性模板 */
	SAVE_ATTRIBUTE("/saveAttribute"),

	/** 参数模板管理 */
	USERPARAM_OVERLAY("/userParamOverlay"),

	/** 保存自定义参数模板 */
	SAVE_USERPARAM("/saveUserParam"),

	/** 售后弹框页面 */
	AFTERSALE_OVERLAY("/afterSaleOverlay"),

	/** 店铺设置 */
	SHOP_SETTING("/shopSetting"),

	/** 添加发货地址 */
	LOAD_DELIVER_ADDRESS_OVERLAY("/deliverAddressOverlay"),

	/** 加载公共分类 */
	LOAD_CATEGORY_COMM_PAGE("/loadCategoryComm"),

	/** 商品列表页面 */
	LOAD_PROD_LIST_PAGE("/loadProdListPage"),

	/** 购物清单打印 */
	PRINT_ORDER_PAGE("/printOrder"),

	/** 打印配送单 */

	PRINT_DELIVERY("/printDelivery"),
	/** 配送方式 */
	DELIVERYTYPE_LIST("/deliveryType"),

	/**设置运单模版  */
	PRINTTMPL_DESIGN("/printtmplDesign"),

	/** 添加发货物流信息 */
	DELIVER_GOODS("/deliverGoods"),

	/** 修改物流信息 */
	CHANGE_DELIVER_NUM("/changeDeliverNum"),

	/** 搜索类目 */
	SEARCH_CATEGORY_PAGE("/searchCategory"),

	/** 调整订单金额 */
	CHANGE_ORDERFEE("/changeOrderFee"),

	/** 调整预售订单费用窗口 */
	CHANGE_PRE_ORDERFEE("/presell/changePreOrderFee"),

	/** 创建职位 */
	SHOPJOB("/shopJob"),

	/** 员工管理 */
	SHOPUSER("/shopUser"),
	
	/** 批量修改商品类目 */
	BATCHEDIT("/batchedit"),

	/** 批量修改商品库存 */
	STOCKSBATCHEDIT("/stocksBatchEdit"),
	
	UPDATEPWD("/updatePwd"),

	/** 预分销商品 */
	LOAD_WANT_DIST_PROD("/loadWantDistProd"),

	/** 淘宝商品发布 */
	TAOBAO_PROD_IMPORT("/taobaoProdImport"),

	/** 卖家中心 */
	SELLER_HOME("/sellerHome"),

	/** 商品编辑 */
	PROD_PUBLISH_PROD_PAGE("/publishProd"),

	/** 出售中的商品 */
	SELLING_PROD("/sellingProd"),

	/** 仓库中的商品 */
	PROD_INSTOREHOUSE("/prodInStoreHouse"),

	/** 违规的商品 */
	VIOLATION_PROD("/violationProd"),

	/** 商品类目 */
	SHOP_CAT("/shopCat"),

	/** 类目配置 */
	SHOP_CAT_CONFIG("/shopCatConfig"),

	/** 二级商品类目 */
	NEXT_SHOPCAT("/nextShopCat"),

	/** 三级商品类目 */
	SUB_SHOPCAT("/subCat"),

	/** 二级类目配置 */
	NEXT_SHOPCAT_CONFIG("/nextCatConfig"),

	/** 三级类目配置 */
	SUB_SHOPCAT_CONFIG("/subCatConfig"),

	/** 品牌列表 */
	APPLY_BRAND("/applyBrand"),

	/** 品牌申请 */
	APPLYBRAND_CONFIGURE("/applyBrandConfigure"),

	/** 被举报禁售商品 */
	REPORT_FORBIT("/reportForbit"),

	/** 导入淘宝商品 */
	IMPORT_TAOBAO("/importTaobao"),

	/** 二级域名配置 */
	DOMAIN_SETTING("/domainSetting"),

	/**  */
	SUB_ACCOUNT("/subAccount"),

	/** 广告编辑 */
	ADVERTISEMENT("/advertisement"),

	/**  */
	PHOTO("/photo"),

	/** 已卖出的商品 */
	ORDERS_MANAGE("/ordersManage"),

	/** 已卖出的预售商品 */
	PRESELL_ORDERS_MANAGE("/presell/presellOrdersManage"),

	/** 预售订单详情 */
	PRELLORDERDETAIL("/presell/presellOrderDetail"),

	/** 订单详情 */
	ORDERDETAIL("/orderDetail"),

	/** 发货地址管理 */
	DELIVER_ADDRESS("/deliverGoodsAddress"),

	/** 商品咨询 */
	PRODCONSULT("/prodcons"),

	/** 配送类型配置 */
	DELIVERYTYPE_EDIT_PAGE("/deliveryTypeConfigure"),

	/** 打印模板 */
	PRINTTMPL_LIST_PAGE("/printmpl"),

	/** 打印模板编辑 */
	PRINTTMPL_EDIT_PAGE("/printmplEdit"),

	/** 添加打印模板 */
	PRINTTMPL_ADD_PAGE("/printmplAdd"),

	/** 物流详情 */
	ORDEREXPRESS("/orderExpress"),

	/** 待审核的商品 */
	AUDITING_PROD("/auditingProd"),

	/** 审核失败的商品 */
	AUDIT_FAIL_PROD("/auditFailProd"),

	/** 商品回收站 */
	PROD_IN_DUSTBIN("/prodInDustbin"),

	/** 产品品牌 */
	BRAND_PAGE("/brandPage"),

	/** 商家结算 */
	SHOPORDERBILL_LIST("/shopOrderBillList"),

	/** 商家结算详情 */
	SHOPORDERBILL_DETAIL("/shopOrderBillDetail"),

	/** 职位管理 */
	SHOPJOBS_LIST("/shopJobsList"),

	/** 员工管理 */
	SHOPUSER_LIST("/shopUserList"),

	/** 分销卖出的商品 */
	SHOPDISTORDER("/shopDistOrder"),

	/** 分销的商品 */
	JOIN_DIST_PROD("/joinDistProd"),

	/** 商品导入 */
	PROD_IMPORT("/prodImport"),

	/** 导入文件 */
	P_IMPORT_UPLOAD("/uploadImport"),

	/** 商品告警 */
	STOCK_WARNING("/stockWarning"),

	/** 库存编辑 */
	STOCK_EDIT_PAGE("/stockEdit"),

	/** 到货通知 */
	PROD_ARRIVAL("/prodArrival"),

	/** 查看到货通知用户页面 */
	SELECT_ARRIVAL("/selectArrival"),

	/** 商家首页 */
	STORE_LIST("/storeDefaultIndex"),

	/** 商品列表排序 */
	STORE_LIST_PRODS("/storeListProds"),

	/** 退款详情页面 **/
	SHOP_APPLY_ORDER_REFUND("/refund/shopApplyOrderRefund"),
	
	REFUND_DETAIL("/refund/shopRefundDetail"),

	/** 退货详情页 */
	RETURN_DETAIL("/refund/shopReturnDetail"),

	/** 退款退货列表页 */
	REFUND_REFUND_LIST("/refund/shopRefundReturnList"),

	/** 退款列表 */
	REFUND_LIST("/refund/shopRefundList"),

	/** 退货列表 */
	RETURN_LIST("/refund/shopReturnList"),

	/** 收货窗口 */
	RECEIVE_WIN("/refund/receiveWin"),

	/** 包邮设置 */
	MAILINGFEE_SETTING("/mailingFeeSetting"),
	
	LOAD_SKU_LIST_PAGE("/stockLog"),
	
	INVOICE_MANAGER("/invoiceManager"),
	
	LOAD_STOCK_LOG_PAGE("/loadStockLogPage"),
	
	/**商家分类**/
	LOADSHOPALLCATEGORY("/loadShopAllCategory"),
	
//客服
	CUSTOM_SERVER_SETTING("/customServer/setting"),

	CUSTOM_SERVER_CONVERSATION("/customServer/conversation"),

	CUSTOM_SERVER_LOGIN("/customServer/login"),

	CUSTOM_SERVER_UPDATE("/customServer/updateCustomServer"),

	CUSTOM_SERVER_ADD("/customServer/addCustomServer"),
	
	CUSTOM_SERVER_UPDATE_PSW("/customServer/updateCustomServerPsw"),

	CUSTOM_SERVERLIST("/customServer/customServerList"),

	CONVERSATION_ORDER("/customServer/conversationOrder"),

	CONVERSATION_PROD("/customServer/conversationProd"),

	CONVERSATION_PROD_LIST("/customServer/conversationProdList"),

	CUSTOMER_CHAT_RECORD("/customServer/customerHistory"),

	CUSTOMER_CHAT_RECORD_LIST("/customServer/customerHistoryList"),

	/**
	 * 客服人员
	 */
	CUSTOM_SERVER("/customServer/customServer"),

	/**
	 * 客服设置
	 */
	SHOP_CUSTOM_SERVICE_SET("/shopCustomServiceSet/shopCustomServiceSet"),


	EXCEL_iMPORT_ORDER("/excelImportOrder"),
	
	APP_SHOP_INDEX_DECORATE("/appShopIndexDecorate/shopIndexDecoration"),
	
	/**
	 * 操作错误
	 */
	OPERATION_ERROR("/operationError"), 
	;

	/** The value. */
	private final String value;

	private ShopFrontPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath("multishop", path);
	}

	public String getNativeValue() {
		return value;
	}

}
