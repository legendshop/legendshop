/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.multishop.controller;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.base.util.ExpressMD5;
import com.legendshop.base.util.PhotoPathDto;
import com.legendshop.base.util.PhotoPathResolver;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.DeliveryPrintDataUtils;
import com.legendshop.core.utils.ExportExcelUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.constant.*;
import com.legendshop.model.dto.ExpressDeliverDto;
import com.legendshop.model.dto.OrderExprotDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.order.*;
import com.legendshop.model.entity.*;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.model.entity.presell.PresellSubEntity;
import com.legendshop.model.entity.store.Store;
import com.legendshop.model.entity.store.StoreOrder;
import com.legendshop.multishop.page.BackPage;
import com.legendshop.multishop.page.ShopFrontPage;
import com.legendshop.multishop.page.ShopRedirectPage;
import com.legendshop.processor.helper.DelayCancelHelper;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.*;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;
import com.legendshop.util.HttpUtil;
import com.legendshop.util.JSONUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 商家订单控制器.
 */
@Controller
public class ShopOrderController extends BaseController {


	private final static Logger log = LoggerFactory.getLogger(ShopOrderController.class);

	@Autowired
	private SubService subService;

	@Autowired
	private DeliverGoodsAddrService deliverGoodsAddrService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private PrintTmplService printTmplService;

	@Autowired
	private DeliveryTypeService deliveryTypeService;

	@Autowired
	private KuaiDiService kuaiDiService;

	@Autowired
	private DelayCancelHelper delayCancelHelper;

	@Autowired
	private DeliveryCorpService deliveryCorpService;

	@Autowired
	private SystemParameterService systemParameterService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private SubRefundReturnService subRefundReturnService;

	@Autowired
	private SystemConfigService systemConfigService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private AttachmentManager attachmentManager;

	@Autowired
	private SubHistoryService subHistoryService;

	@Autowired
	private StoreOrderService storeOrderService;

	@Autowired
	private StoreService storeService;

	/**
	 * 发送站内信
	 */
	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;

	/**
	 * 查看.
	 *
	 * @param request
	 * @param response
	 * @param paramDto
	 * @return the string
	 */
	@RequestMapping(value = "/s/ordersManage")
	@Authorize(authorityTypes = AuthorityType.ORDERS_MANAGE)
	public String ordersManage(HttpServletRequest request, HttpServletResponse response, OrderSearchParamDto paramDto) {

		SecurityUserDetail user = UserManager.getUser(request);

		if (!UserManager.isShopUser(user)) {// 还没有开店
			return PathResolver.getPath(ShopRedirectPage.SHOP_AGREEMENT);
		}

		int pageSize = 10;// 默认一页显示条数
		paramDto.setPageSize(pageSize);
		paramDto.setShopId(user.getShopId());

		paramDto.setIncludeDeleted(true);
		PageSupport<MySubDto> ps = orderService.getShopOrderDtos(paramDto);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("paramDto", paramDto);
		//获取订单自动取消的时间
		int cancelMins = subService.getOrderCancelExpireDate();
		request.setAttribute("cancelMins", cancelMins);
		return PathResolver.getPath(ShopFrontPage.ORDERS_MANAGE);
	}

	/**
	 * 物流单号批量导入 弹窗
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/s/excelImportOrder")
	public String excelImportProd(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(ShopFrontPage.EXCEL_iMPORT_ORDER);
	}


	/**
	 * 查询预售订单列表.
	 *
	 * @param request
	 * @param response
	 * @param paramDto
	 * @return the string
	 */
	@RequestMapping(value = "/s/presellOrdersManage")
	@Authorize(authorityTypes = AuthorityType.PRESELL_ORDERS_MANAGE)
	public String presellOrdersManage(HttpServletRequest request, HttpServletResponse response, OrderSearchParamDto paramDto) {

		SecurityUserDetail user = UserManager.getUser(request);
		if (!UserManager.isShopUser(user)) {// 还没有开店
			return PathResolver.getPath(ShopRedirectPage.SHOP_AGREEMENT);
		}

		if (AppUtils.isBlank(paramDto.getPageSize())) {// 默认每页显示10条
			paramDto.setPageSize(10);
		}
		paramDto.setShopId(user.getShopId());

		paramDto.setIncludeDeleted(true);// 查询包含删除的
		PageSupport<PresellSubDto> ps = orderService.getPresellOrderList(paramDto);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("paramDto", paramDto);
		return PathResolver.getPath(ShopFrontPage.PRESELL_ORDERS_MANAGE);
	}

	/**
	 * 查看订单信息.
	 *
	 * @param request
	 * @param response
	 * @param subNumber
	 * @return the string
	 */
	@RequestMapping("/s/orderDetail/{subNumber}")
	@Authorize(authorityTypes = AuthorityType.ORDERS_MANAGE)
	public String orderDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumber) {
		MySubDto myorder = subService.findOrderDetail(subNumber);

		//如果是门店订单，获取门店相关信息以及提货码
		StoreOrder storeOrder = storeOrderService.getDeliveryOrderBySubNumber(myorder.getSubNum());
		if (AppUtils.isNotBlank(storeOrder)) {
			Store store = storeService.getStore(storeOrder.getStoreId());
			request.setAttribute("store", store);
		}
		request.setAttribute("order", myorder);
		request.setAttribute("storeOrder", storeOrder);
		String result = PathResolver.getPath(ShopFrontPage.ORDERDETAIL);
		return result;
	}

	/**
	 * 查看預售訂單详情.
	 *
	 * @param request
	 * @param response
	 * @param subNumber
	 * @return the string
	 */
	@RequestMapping("/s/presellOrderDetail/{subNumber}")
	public String presellOrderDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumber) {
		PresellSubDto presellOrder = subService.findPresellOrderDetail(subNumber);
		request.setAttribute("order", presellOrder);
		String result = PathResolver.getPath(ShopFrontPage.PRELLORDERDETAIL);
		return result;
	}

	/**
	 * 商家前往退款申请页面
	 *
	 * @param orderId 订单ID
	 * @param userId  订单所属用户ID
	 * @return
	 */
	@RequestMapping("/s/shopRefund/apply/{userId}/{orderId}")
	@Authorize(authorityTypes = AuthorityType.ORDERS_MANAGE)
	public String shopApplyRefund(HttpServletRequest request, HttpServletResponse response, @PathVariable String userId, @PathVariable Long orderId) {
		ApplyRefundReturnDto refund = subRefundReturnService.getApplyRefundReturnDto(orderId, RefundReturnTypeEnum.OP_ORDER, userId);
		Sub sub = subService.getSubById(orderId);
		if (AppUtils.isBlank(refund)) {
			request.setAttribute("message", "该订单为空");
			return PathResolver.getPath(ShopFrontPage.OPERATION_ERROR);
		}
		//检查订单是否允许退款操作
		if (!isAllowRefund(refund, RefundSouceEnum.SHOP.value())) {//不允许
			request.setAttribute("message", "该订单不允许退款操作");
			return PathResolver.getPath(ShopFrontPage.OPERATION_ERROR);
		}

		if (Double.doubleToRawLongBits(refund.getSubMoney()) == Double.doubleToLongBits(0.00)) {
			request.setAttribute("message", "该订单金额为0，不支持退款");
			return PathResolver.getPath(ShopFrontPage.OPERATION_ERROR);
		}

		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		request.setAttribute("systemConfig", systemConfig);
		request.setAttribute("refund", refund);
		request.setAttribute("userId", userId);
		request.setAttribute("freightAmount", sub.getFreightAmount());
		return PathResolver.getPath(ShopFrontPage.SHOP_APPLY_ORDER_REFUND);
	}

	/**
	 * 保存商家主动退还订金申请
	 *
	 * @param form   订金退款表单参数
	 * @param userId 订金退款的用户ID
	 * @return
	 */
	@RequestMapping("/s/shopRefund/save")
	public String saveShopRefund(HttpServletRequest request, HttpServletResponse response,
								 @Validated ApplyRefundForm form, String userId, BindingResult error) {

		if (error.hasErrors()) {
			request.setAttribute("message", "对不起,您提交的数据有误!");
			return PathResolver.getPath(ShopFrontPage.OPERATION_ERROR);
		}

		ApplyRefundReturnDto refund = subRefundReturnService.getApplyRefundReturnDto(form.getOrderId(), RefundReturnTypeEnum.OP_ORDER, userId);

		if (AppUtils.isBlank(refund)) {
			request.setAttribute("message", "对不起,您操作的订单不存在或已被删除!");
			return PathResolver.getPath(ShopFrontPage.OPERATION_ERROR);
		}

		//检查订单是否允许退款
		if (!isAllowRefund(refund, RefundSouceEnum.SHOP.value())) {
			request.setAttribute("message", "对不起,您操作的订单不允许退款!");
			return PathResolver.getPath(ShopFrontPage.OPERATION_ERROR);
		}

		//获取退款用户信息
		UserDetail userDetail = userDetailService.getUserDetailByIdNoCache(userId);

		if (AppUtils.isBlank(userDetail)) {
			request.setAttribute("message", "对不起,你操作的订单所属用户不存在");
			return PathResolver.getPath(ShopFrontPage.OPERATION_ERROR);
		}

		// 计算定金跟尾款
		if (refund.getSubType().equals(CartTypeEnum.PRESELL.toCode()) && refund.getPayPctType() == 1) {
			double percent = refund.getDepositRefundAmount() / refund.getProdCash();
			Double prodCash = refund.getProdCash();
			double preVal = prodCash * percent;
			double finalPayVal = refund.getRefundAmount() - preVal;
			refund.setDepositRefundAmount(preVal);
			refund.setFinalRefundAmount(finalPayVal);
		}
		SubRefundReturn subRefundReturn = new SubRefundReturn();

		subRefundReturn.setSubId(form.getOrderId());
		subRefundReturn.setSubNumber(refund.getSubNumber());
		subRefundReturn.setSubMoney(BigDecimal.valueOf(refund.getSubMoney()));//设置订单总金额
		subRefundReturn.setIsHandleSuccess(0);

		subRefundReturn.setRefundAmount(BigDecimal.valueOf(refund.getFinalRefundAmount()));
		subRefundReturn.setFlowTradeNo(refund.getFinalFlowTradeNo());
		subRefundReturn.setSubSettlementSn(refund.getFinalSettlementSn());
		subRefundReturn.setOrderDatetime(refund.getFinalOrderDateTime());
		subRefundReturn.setPayTypeId(refund.getFinalPayTypeId());
		subRefundReturn.setPayTypeName(refund.getPayTypeName());

		subRefundReturn.setShopId(refund.getShopId());
		subRefundReturn.setShopName(refund.getShopName());
		subRefundReturn.setRefundSn(CommonServiceUtil.getRandomSn(new Date(), "yyyyMMdd"));
		subRefundReturn.setUserId(userId);
		subRefundReturn.setUserName(userDetail.getUserName());
		subRefundReturn.setSellerState(RefundReturnStatusEnum.SELLER_AGREE.value());
		subRefundReturn.setApplyState(RefundReturnStatusEnum.APPLY_WAIT_ADMIN.value());
		subRefundReturn.setApplyTime(new Date());
		subRefundReturn.setBuyerMessage(form.getBuyerMessage());
		subRefundReturn.setReasonInfo("商家主动发起退款");
		subRefundReturn.setSellerTime(new Date());
		subRefundReturn.setSellerMessage("预售订单，商家主动发起退款。原因：" + form.getReasonInfo());

		//保存退款及退货凭证图片
		if (form.getPhotoFile1().getSize() > 0 && form.getPhotoFile1().getSize() <= Constants._5M) {
			String picUrl = attachmentManager.upload(form.getPhotoFile1());
			subRefundReturn.setPhotoFile1(picUrl);
		}
		if (form.getPhotoFile2() != null && form.getPhotoFile2().getSize() > 0 && form.getPhotoFile2().getSize() <= Constants._5M) {
			String picUrl = attachmentManager.upload(form.getPhotoFile2());
			subRefundReturn.setPhotoFile2(picUrl);
		}
		if (form.getPhotoFile3() != null && form.getPhotoFile3().getSize() > 0 && form.getPhotoFile3().getSize() <= Constants._5M) {
			String picUrl = attachmentManager.upload(form.getPhotoFile3());
			subRefundReturn.setPhotoFile3(picUrl);
		}

		//订单全部退款,这些设置为0
		subRefundReturn.setSubItemId(0L);
		subRefundReturn.setProductImage(refund.getProdPic());
		subRefundReturn.setProductId(0L);
		subRefundReturn.setSkuId(0L);
		subRefundReturn.setProductName("预售订单退款");
		subRefundReturn.setApplyType(RefundReturnTypeEnum.REFUND.value());
		subRefundReturn.setRefundAmount(BigDecimal.valueOf(form.getRefundAmount()));//尾款金额

		if (SubTypeEnum.PRESELL.value().equals(refund.getSubType())) {//预售订单
			subRefundReturn.setIsPresell(true);
			if (refund.getPayPctType() == 1) {
				//subRefundReturn.setRefundAmount(BigDecimal.valueOf(refund.getFinalRefundAmount()));
				subRefundReturn.setPayTypeId(refund.getPayTypeId());
				subRefundReturn.setFlowTradeNo(refund.getFlowTradeNo());
				subRefundReturn.setSubSettlementSn(refund.getSubSettlementSn());
				subRefundReturn.setOrderDatetime(refund.getOrderDateTime());
			}
		} else {
			subRefundReturn.setIsPresell(false);
		}

		subRefundReturn.setIsRefundDeposit(form.getIsRefundDeposit());

		// 如果未支付尾款，则是退定金
		if(refund.getIsPayFinal() != 1){
			subRefundReturn.setIsRefundDeposit(true);
		}else {

			// 现有逻辑是 尾款 == RefundAmount  定金是额外字段 DepositRefund
			subRefundReturn.setIsRefundDeposit(form.getIsRefundDeposit());
			subRefundReturn.setRefundAmount(BigDecimal.valueOf(refund.getFinalRefundAmount()));
		}

		subRefundReturn.setIsDepositHandleSucces(RefundReturnStatusEnum.DEPOSIT_REFUND_PROCESSING.value());

		subRefundReturn.setDepositRefund(BigDecimal.valueOf(refund.getDepositRefundAmount()));
		subRefundReturn.setRefundSouce(RefundSouceEnum.SHOP.value());

		subRefundReturn.setDepositFlowTradeNo(refund.getFlowTradeNo());
		subRefundReturn.setDepositSubSettlementSn(refund.getSubSettlementSn());
		subRefundReturn.setDepositOrderDatetime(refund.getOrderDateTime());
		subRefundReturn.setDepositPayTypeId(refund.getPayTypeId());
		subRefundReturn.setDepositPayTypeName(refund.getPayTypeName());

		subRefundReturnService.saveApplyOrderRefund(subRefundReturn);
		//商家发起退款，发送给用户的站内信通知
		sendSiteMessageProcessor.process(new SendSiteMsgEvent("系统", userDetail.getUserName(), "退款通知", "亲，您的订单[" + subRefundReturn.getSubNumber() + "],商家发起订金退款").getSource());
		return PathResolver.getPath(ShopRedirectPage.ORDERSMANAGE_QUERY);
	}

	/**
	 * 检查订单是否允许退订金
	 *
	 * @param refund
	 * @param source 申请来源
	 * @return
	 */
	private boolean isAllowRefund(ApplyRefundReturnDto refund, Integer source) {

		//用户不能申请 退订金
		if (!(source == RefundSouceEnum.SHOP.value() || source == RefundSouceEnum.PLATFORM.value())) {
			return false;
		}
		//不是预售订单，没有退订金的说话
		if (!refund.getSubType().equals(SubTypeEnum.PRESELL.value())) {
			return false;
		}
		//已申请退订金，或已经退订金
		if (refund.getRefundStatus() != RefundReturnStatusEnum.ORDER_NO_REFUND.value()) {
			return false;
		}
		//如果是货到付款，不允许退款
		if (PayMannerEnum.DELIVERY_ON_PAY.value().equals(refund.getPayManner())) {
			return false;
		}
		//预售订金支付，订单状态还是未支付状态
		if (!(OrderStatusEnum.UNPAY.value().equals(refund.getOrderStatus()) || OrderStatusEnum.PADYED.value().equals(refund.getOrderStatus()))) {
			return false;
		}
		//定不是已支付状态，尾款不是未支付状态
		if ((refund.getIsPayDeposit() != 1 && refund.getIsPayFinal() != 0) || (refund.getIsPayDeposit() != 1 && refund.getIsPayFinal() != 1)) {
			return false;
		}
		return true;
	}


	/**
	 * 查看订单历史记录.
	 *
	 * @param request
	 * @param response
	 * @param subId
	 * @return the string
	 */
	@RequestMapping(value = "/s/findSubHisInfo", method = RequestMethod.POST)
	public @ResponseBody
	String findSubHisInfo(HttpServletRequest request, HttpServletResponse response, Long subId) {
		SecurityUserDetail user = UserManager.getUser(request);

		if (subId == null) {
			log.warn("subId is null");
			return Constants.FAIL;
		}

		Sub sub = subService.getSubByShopId(subId, user.getShopId());//检查权限
		if (sub == null) {
			log.error("No right to access the order by subId {}, shopId {}", subId, user.getShopId());
			return "";
		}

		return subService.findSubhisInfo(subId);
	}

	/**
	 * 获取物流详情.
	 */
	@RequestMapping(value = "/s/order/getExpress", method = RequestMethod.POST)
	public @ResponseBody
	String getExpress(HttpServletRequest request, HttpServletResponse response, String dvyFlowId, Long dvyId, String reciverMobile) {

		if (AppUtils.isBlank(dvyId) || AppUtils.isBlank(dvyFlowId) || AppUtils.isBlank(reciverMobile)) {
			log.warn("Parameter missing");
			return Constants.FAIL;
		}
		DeliveryCorp deliveryCorp = deliveryCorpService.getDeliveryCorpByDvyId(dvyId);
		if (AppUtils.isNotBlank(deliveryCorp)) {

			// 这处判断会导致查看物流信息失败，因为物流方式是后台统一配的，没有shopId
    		/*SecurityUserDetail user = UserManager.getUser(request);
    		Long shopId = user.getShopId();
    		if(!shopId.equals(deliveryCorp.getShopId())) {
    			log.error("No right to access the object by shopId {}, userId {}", shopId, user.getUserId());
    			return Constants.FAIL;
    		}*/

			SystemParameter systemParameter = systemParameterService.getSystemParameter("EXPRESS_DELIVER");

			//查询到没配置密钥则 是免费的
			if (AppUtils.isBlank(systemParameter) || AppUtils.isBlank(systemParameter.getValue())) {

				String url = deliveryCorp.getQueryUrl();
				if (AppUtils.isNotBlank(url)) {
					url = url.replaceAll("\\{dvyFlowId\\}", dvyFlowId);
				}
				KuaiDiLogs logs = kuaiDiService.query(url);
				if (AppUtils.isNotBlank(logs) && AppUtils.isNotBlank(logs.getEntries()) && logs.getEntries().size() > 0) {
					return JSONUtil.getJson(logs.getEntries());
				}

			} else {  //快递100

				if (AppUtils.isBlank(deliveryCorp.getCompanyCode())) {
					return Constants.FAIL;
				}

				ExpressDeliverDto express = JSONUtil.getObject(systemParameter.getValue(), ExpressDeliverDto.class);

				//组装参数密钥
				String param = "{\"com\":\"" + deliveryCorp.getCompanyCode() + "\",\"num\":\"" + dvyFlowId + "\",\"mobiletelephone\":\"" + reciverMobile + "\"}";
				String sign = ExpressMD5.encode(param + express.getKey() + express.getCustomer());
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("param", param);
				params.put("sign", sign);
				params.put("customer", express.getCustomer());
				//请求第三方接口
				String text = HttpUtil.httpPost(ExpressMD5.API_EXPRESS_URL, params);
				if (AppUtils.isBlank(text)) {
					return Constants.FAIL;
				}

				JSONObject jsonObject = JSONObject.parseObject(text);
				String result = jsonObject.getString("status");
				if (AppUtils.isBlank(result) || !"200".equals(result)) {
					log.warn("Express inquiry failed message = {},returnCode = {}", jsonObject.getString("message"), jsonObject.getString("returnCode"));
					return Constants.FAIL;
				}
				return jsonObject.getString("data");
			}
		}
		return Constants.FAIL;
	}

	/**
	 * 发货地址管理.
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return the string
	 */
	@RequestMapping(value = "/s/deliverGoodsAddress")
	@Authorize(authorityTypes = AuthorityType.DELIVER_ADDRESS)
	public String deliverGoodsAddress(HttpServletRequest request, HttpServletResponse response, String curPageNO) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		PageSupport<DeliverGoodsAddr> ps = deliverGoodsAddrService.getDeliverGoodsAddrPage(curPageNO, shopId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("total", ps.getTotal());
		return PathResolver.getPath(ShopFrontPage.DELIVER_ADDRESS);
	}

	/**
	 * 加载地址弹框,如果为0则是新增.
	 *
	 * @param request
	 * @param response
	 * @param addrId
	 * @return the string
	 */
	@RequestMapping("/s/defaultDeliverAddress/{addrId}")
	@Authorize(authorityTypes = AuthorityType.DELIVER_ADDRESS)
	public @ResponseBody
	String defaultDeliverAddress(HttpServletRequest request, HttpServletResponse response, @PathVariable Long addrId) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		DeliverGoodsAddr userArrress = deliverGoodsAddrService.getDeliverGoodsAddr(addrId);
		if (userArrress != null && shopId.equals(userArrress.getShopId())) {
			deliverGoodsAddrService.updateDefaultDeliverAddress(addrId, shopId);
			return Constants.SUCCESS;
		} else {
			return Constants.FAIL;
		}
	}

	/**
	 * 加载地址弹框,如果为0则是新增.
	 *
	 * @param request
	 * @param response
	 * @param addrId
	 * @return the string
	 */
	@RequestMapping("/s/loadDeliverGoodsAddress/{addrId}")
	@Authorize(authorityTypes = AuthorityType.DELIVER_ADDRESS)
	public String loadDeliverGoodsAddress(HttpServletRequest request, HttpServletResponse response, @PathVariable Long addrId) {
		if (addrId != 0) {
			DeliverGoodsAddr userArrress = deliverGoodsAddrService.getDeliverGoodsAddr(addrId);
			request.setAttribute("userArrress", userArrress);
		}
		return PathResolver.getPath(ShopFrontPage.LOAD_DELIVER_ADDRESS_OVERLAY);
	}

	/**
	 * 取消订单.
	 *
	 * @param request
	 * @param response
	 * @param subNumber
	 * @return the string
	 */
	@RequestMapping(value = "/s/order/cancleOrder")
	@ResponseBody
	public String cancleOrder(HttpServletRequest request, HttpServletResponse response, String subNumber) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		Long shopId = user.getShopId();
		try {
			if (AppUtils.isNotBlank(subNumber)) {
				Sub sub = subService.getSubBySubNumberByShopId(subNumber, shopId);
				if (AppUtils.isNotBlank(sub) && OrderStatusEnum.UNPAY.value().equals(sub.getStatus())) {
					StringBuilder sb = new StringBuilder();
					String time = DateUtils.format(new Date(), DateUtils.PATTERN_CLASSICAL);
					sb.append("商家").append(userName).append("于").append(time).append("取消订单 ");
					Date date = new Date();
					sub.setUpdateDate(date);
					sub.setStatus(OrderStatusEnum.CLOSE.value());
					sub.setCancelReason(sb.toString());
					boolean result = subService.cancleOrder(sub);
					if (result) {
						//写入订单历史
						SubHistory subHistory = new SubHistory();
						subHistory.setRecDate(date);
						subHistory.setSubId(sub.getSubId());
						subHistory.setStatus(SubHistoryEnum.ORDER_CANCEL.value());

						subHistory.setUserName(userName);
						subHistory.setReason(sb.toString());

						subHistoryService.saveSubHistory(subHistory);

						orderService.cleanOrderCache(sub.getUserId(), sub.getShopId());

						// remove delay queue ordre
						delayCancelHelper.remove(sub.getSubNumber());
						return Constants.SUCCESS;
					}
				} else {
					return Constants.FAIL;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Constants.FAIL;
	}

	/**
	 * 保存地址.
	 *
	 * @param request
	 * @param response
	 * @param userAddress
	 * @return the string
	 */
	@RequestMapping(value = "/s/saveDeliverAddress", method = RequestMethod.POST)
	@Authorize(authorityTypes = AuthorityType.DELIVER_ADDRESS)
	public @ResponseBody
	String saveDeliverAddress(HttpServletRequest request, HttpServletResponse response, DeliverGoodsAddr userAddress) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		if (userAddress.getAddrId() == null) { // for save
			if (checkMaxNumber(shopId)) {
				return Constants.MAX_NUM;
			}
			userAddress.setShopId(shopId);
			userAddress.setCreateTime(new Date());
			userAddress.setDetault(0);
		}
		deliverGoodsAddrService.saveDeliverGoodsAddr(userAddress, shopId);
		return Constants.SUCCESS;
	}

	/**
	 * 检查最大数量.
	 *
	 * @param shopId
	 * @return true, if check max number
	 */
	private boolean checkMaxNumber(Long shopId) {
		Long MaxNumber = deliverGoodsAddrService.getMaxNumber(shopId);
		if (MaxNumber >= 5) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 删除地址.
	 *
	 * @param request
	 * @param response
	 * @param addrId
	 * @return the string
	 */
	@RequestMapping(value = "/s/deliverGoodsAddr/{addrId}")
	@Authorize(authorityTypes = AuthorityType.DELIVER_ADDRESS)
	public String deliverGoodsAddr(HttpServletRequest request, HttpServletResponse response, @PathVariable Long addrId) {
		DeliverGoodsAddr userAddress = deliverGoodsAddrService.getDeliverGoodsAddr(addrId);
		if (userAddress != null) {

			SecurityUserDetail user = UserManager.getUser(request);
			Long shopId = user.getShopId();

			if (shopId.equals(userAddress.getShopId())) {
				deliverGoodsAddrService.deleteDeliverGoodsAddr(userAddress);
			}
		}
		return PathResolver.getPath(ShopFrontPage.DELIVER_ADDRESS);
	}

	/**
	 * 打印订单.
	 *
	 * @param request
	 * @param response
	 * @param subNumber
	 * @return the string
	 */
	@RequestMapping("/s/orderPrint/{subNumber}")
	@Authorize(authorityTypes = AuthorityType.ORDERS_MANAGE)
	public String orderPrint(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumber) {
		SecurityUserDetail user = UserManager.getUser(request);
		MySubDto myorder = subService.findOrderDetail(subNumber);
		if (myorder != null && myorder.getShopId().equals(user.getShopId())) {//自己商城的订单才可以看
			request.setAttribute("order", myorder);
		}

		return PathResolver.getPath(ShopFrontPage.PRINT_ORDER_PAGE);
	}

	/**
	 * 打印发货单.
	 *
	 * @param request
	 * @param response
	 * @param orderId
	 * @param delvId
	 * @return the string
	 * @throws Exception
	 */
	@RequestMapping(value = "/s/printDeliveryDocument/{orderId}/{delvId}")
	@Authorize(authorityTypes = AuthorityType.ORDERS_MANAGE)
	public String printDeliveryDocument(HttpServletRequest request, HttpServletResponse response, @PathVariable Long orderId, @PathVariable Long delvId)
			throws Exception {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		Long shopId = user.getShopId();

		PrintTmpl printTmpl = printTmplService.getPrintTmplByDelvId(delvId);
		if (AppUtils.isBlank(printTmpl)) {
			log.warn("打印失败,没有找到你运单模版信息 shopId = {},  userId = {}", shopId, userId);
			request.setAttribute("mess", "打印失败,没有找到你运单模版信息");
			return PathResolver.getPath(BackPage.BACK_ERROR_PAGE);
		}

		if (!shopId.equals(printTmpl.getShopId())) {//检查权限
			log.warn("打印失败,加载数据异常 shopId = {},  userId = {}", shopId, userId);
			request.setAttribute("mess", "打印失败,加载数据异常");
			return PathResolver.getPath(BackPage.BACK_ERROR_PAGE);
		}

		if (AppUtils.isBlank(printTmpl.getBgimage())) {
			log.warn("打印失败,打印失败,请设置打印背景图 shopId = {},  userId = {}", shopId, userId);
			request.setAttribute("mess", "打印失败,,请设置打印背景图");
			return PathResolver.getPath(BackPage.BACK_ERROR_PAGE);
		}

		if (AppUtils.isBlank(printTmpl.getPrtTmplData())) {
			log.warn("打印失败,请设置打印背景的参数数据 shopId = {},  userId = {}", shopId, userId);
			request.setAttribute("mess", "打印失败,请设置打印背景的参数数据！");
			return PathResolver.getPath(BackPage.BACK_ERROR_PAGE);
		}

		DeliveryPrintDataUtils printData = new DeliveryPrintDataUtils(printTmpl.getPrtTmplHeight(), printTmpl.getPrtTmplWidth(), printTmpl.getPrtTmplData());

		PhotoPathDto result = PhotoPathResolver.getInstance().calculateFilePath(printTmpl.getBgimage());
		printData.setTitle(printTmpl.getPrtTmplTitle());
		printData.setIsSystem(printTmpl.getIsSystem());
		printData.setSubId(orderId);
		printData.setShopId(shopId);
		printData.setIsSystem(printTmpl.getIsSystem());
		/*if (printTmpl.getIsSystem().intValue() == 0) {
			printData.setBackgroung(PhotoPathResolver.getInstance().calculateFilePath(printTmpl.getBgimage()).getFilePath());
		}*/
		printData.setBackgroung(result.getFilePath());

		request.setAttribute("printData", printData);
		return PathResolver.getPath(ShopFrontPage.PRINT_DELIVERY);
	}

	/**
	 * 确认发货.
	 *
	 * @param request
	 * @param response
	 * @param subNumber
	 * @return the string
	 */
	@RequestMapping("/s/deliverGoods/{subNumber}")
	public String deliverGoods(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumber) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		Sub sub = subService.getSubBySubNumber(subNumber);
		if (AppUtils.isBlank(sub)) {
			new BusinessException("sub is Null");
		}
		boolean isCod = sub.isCod();// 是否货到付款订单
		// 已付款 or 已发货 or 是到付订单
		if (OrderStatusEnum.PADYED.value().equals(sub.getStatus()) || OrderStatusEnum.CONSIGNMENT.value().equals(sub.getStatus()) || isCod) {
			// 查询配送方式,进行页面显示
			List<DeliveryType> deliveryTypes = deliveryTypeService.getDeliveryTypeByShopId(shopId);
			request.setAttribute("sub", sub);
			request.setAttribute("deliveryTypes", deliveryTypes);
			return PathResolver.getPath(ShopFrontPage.DELIVER_GOODS);
		}
		return "";
	}

	/**
	 * 修改物流单号.
	 *
	 * @param request
	 * @param response
	 * @param subNumber
	 * @return the string
	 */
	@RequestMapping("/s/changeDeliverNum/{subNumber}")
	public String changeDeliverNum(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumber) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		Sub sub = subService.getSubBySubNumber(subNumber);
		if (AppUtils.isBlank(sub)) {
			return "";
		}

		if (!sub.getShopId().equals(shopId)) {
			log.error("No right to access sub by subNumber {}, shopId {}", subNumber, shopId);
			return "";
		}

		if (OrderStatusEnum.PADYED.value().equals(sub.getStatus()) || OrderStatusEnum.CONSIGNMENT.value().equals(sub.getStatus()) || sub.isCod()) {
			List<DeliveryType> deliveryTypes = deliveryTypeService.getDeliveryTypeByShopId(shopId);
			request.setAttribute("sub", sub);
			request.setAttribute("deliveryTypes", deliveryTypes);
			return PathResolver.getPath(ShopFrontPage.CHANGE_DELIVER_NUM);
		}
		return "";
	}

	/**
	 * 弹出调整订单费用窗口页面.
	 *
	 * @param request
	 * @param response
	 * @param subNumber
	 * @return the string
	 */
	@RequestMapping("/s/changeOrderFee/{subNumber}")
	public String changeOrderFee(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumber) {

		SecurityUserDetail user = UserManager.getUser(request);

		MySubDto subDto = subService.getMySubDtoBySubNumber(subNumber);
		if (subDto != null && !subDto.getShopId().equals(user.getShopId())) {
			throw new BusinessException("It can not load order does not own to shopId " + user.getShopId() + " by subNumber  " + subNumber);
		}

		request.setAttribute("subDto", subDto);
		return PathResolver.getPath(ShopFrontPage.CHANGE_ORDERFEE);
	}

	/**
	 * 弹出调整预售订单费用窗口页面.
	 *
	 * @param request
	 * @param response
	 * @param subNumber
	 * @return the string
	 */
	@RequestMapping("/s/changePreOrderFee/{subNumber}")
	public String changePreOrderFee(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumber) {

		SecurityUserDetail user = UserManager.getUser(request);

		PresellSubEntity presellSub = subService.getPreSubBySubNoAndShopId(subNumber, user.getShopId());
		if (AppUtils.isBlank(presellSub)) {
			throw new BusinessException("presellSub is Null");
		}

		request.setAttribute("sub", presellSub);
		return PathResolver.getPath(ShopFrontPage.CHANGE_PRE_ORDERFEE);
	}

	/**
	 * 调整订单费用.
	 *
	 * @param request
	 * @param response
	 * @param subNumber
	 * @param freight
	 * @param orderAmount
	 * @return the string
	 */
	@RequestMapping(value = "/s/changeOrderFee", method = RequestMethod.POST)
	public @ResponseBody
	String changeOrderFee(HttpServletRequest request, HttpServletResponse response, String subNumber, Double freight, Double orderAmount) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();

		if (userName == null || subNumber == null || AppUtils.isBlank(orderAmount) || orderAmount == 0) {
			log.warn("changeOrderFee failed for parameter missing");
			return Constants.FAIL;
		}
		if (orderAmount == null || orderAmount < 0 || orderAmount > 9999999999999D) {
			log.warn("changeOrderFee failed for orderAmount error");
			return Constants.FAIL;
		}
		if (AppUtils.isBlank(freight)) {
			freight = 0.00;
		}
		boolean result = false;
		try {
			Sub subBySubNumber = subService.getSubBySubNumber(subNumber);

			//检查订单归属
			if (subBySubNumber == null) {
				log.warn("changeOrderFee failed for  sub is null");
				return Constants.FAIL;
			}

			if (!subBySubNumber.getShopId().equals(user.getShopId())) {
				log.warn("changeOrderFee failed for  sub is null");
				return Constants.FAIL;
			}

			result = subService.updateSubPrice(subNumber, freight, orderAmount, userName);
			if (!subBySubNumber.getTotal().equals(orderAmount) || !subBySubNumber.getFreightAmount().equals(freight)) {
				// 发送站内信 通知
				sendSiteMessageProcessor.process(new SendSiteMsgEvent(subBySubNumber.getUserName(), "价格变更通知", "亲，您的待支付订单[" + subNumber + "]价格已变更").getSource());
			}
		} catch (Exception e) {
			throw new BusinessException("update price failed");
		}
		if (result) {
			return Constants.SUCCESS;
		} else {
			return Constants.FAIL;
		}
	}

	/**
	 * 调整预售订单费用.
	 *
	 * @param request
	 * @param response
	 * @param presellSub
	 * @return the string
	 */
	@RequestMapping(value = "/s/changePreOrderFee", method = RequestMethod.POST)
	public @ResponseBody
	String changePreOrderFee(HttpServletRequest request, HttpServletResponse response, PresellSubEntity presellSub) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();

		PresellSubEntity oldPresellsub = subService.getPreSubBySubNo(presellSub.getSubNumber());
		if (AppUtils.isBlank(presellSub.getSubNumber()) || AppUtils.isBlank(presellSub.getPreDepositPrice()) || AppUtils.isBlank(presellSub.getFinalPrice())) {
			log.warn("Missing parameter in changePreOrderFee");
			return Constants.FAIL;
		}
		if (presellSub.getPreDepositPrice().doubleValue() <= 0 || presellSub.getPreDepositPrice().doubleValue() > 9999999999999D) {
			log.warn("PreDepositPrice = {} parameter error ", presellSub.getPreDepositPrice());
			return Constants.FAIL;
		}
		if (oldPresellsub.getPayPctType().equals(1) && (presellSub.getFinalPrice().doubleValue() <= 0 || presellSub.getFinalPrice().doubleValue() > 9999999999999D)) {
			log.warn("FinalPrice = {} parameter error ", presellSub.getFinalPrice());
			return Constants.FAIL;
		}
		return subService.updateSubPrice(presellSub, userName) ? Constants.SUCCESS : Constants.FAIL;
	}

	/**
	 * 改变物流信息.
	 *
	 * @param request
	 * @param response
	 * @param subNumber
	 * @param deliv
	 * @param dvyFlowId
	 * @param info
	 * @return the string
	 */
	@RequestMapping(value = "/s/order/changeDeliverGoods")
	public @ResponseBody
	String changeDeliverGoods(HttpServletRequest request, HttpServletResponse response, String subNumber, Long deliv, String dvyFlowId, String info) {
		if (subNumber == null || deliv == null || dvyFlowId == null) {
			log.warn("Missing parameter in changeDeliverGoods");
			return Constants.FAIL;
		}

		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();

		boolean result = false;
		try {
			result = subService.changeDeliverGoods(subNumber, deliv, dvyFlowId, info, userName);
		} catch (Exception e) {
			throw new BusinessException("changeDeliverGoods failed");
		}
		if (result) {
			return Constants.SUCCESS;
		} else {
			return Constants.FAIL;
		}
	}

	/**
	 * 发货，填入物流单号.
	 *
	 * @param request
	 * @param response
	 * @param subNumber
	 * @param deliv
	 * @param dvyFlowId
	 * @param info
	 * @return the string
	 */
	@RequestMapping(value = "/s/order/fahuo", method = RequestMethod.POST)
	public @ResponseBody
	String fahuo(HttpServletRequest request, HttpServletResponse response, String subNumber, Long deliv, String dvyFlowId, String info) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		if (!UserManager.isShopUser(user) || subNumber == null || deliv == null || dvyFlowId == null) {
			log.warn("发货参数不足 shopId = {}, subNumber = {} ", shopId, subNumber);
			return Constants.FAIL;
		}
		String result;
		try {
			// 发货
			result = subService.fahuo(subNumber, deliv, dvyFlowId, info, shopId);
			log.info("发货结果  ： " + result);
		} catch (Exception e) {
			log.warn("发货失败 shopId = {}, subNumber = {} ", shopId, subNumber);
			throw new BusinessException("changeDeliverGoods failed");
		}
		return result;
	}

	/**
	 * 修改物流单号.
	 *
	 * @param request
	 * @param response
	 * @param subNumber
	 * @param deliv
	 * @param dvyFlowId
	 * @param info
	 * @return the string
	 */
	@RequestMapping(value = "/s/order/changeDeliverNum", method = RequestMethod.POST)
	public @ResponseBody
	String changeDeliverNum(HttpServletRequest request, HttpServletResponse response, String subNumber, Long deliv, String dvyFlowId, String info) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (!UserManager.isShopUser(user) || subNumber == null || deliv == null || dvyFlowId == null) {
			return Constants.FAIL;
		}
		String result;
		try {
			result = subService.changeDeliverNum(subNumber, deliv, dvyFlowId, info, user.getShopId());
			log.info("changeDeliverNum result  ： " + result);
		} catch (Exception e) {
			throw new BusinessException("changeDeliverGoods failed");
		}
		return result;
	}

	/**
	 * 订单物流详情.
	 *
	 * @param request
	 * @param response
	 * @param subNumber
	 * @return the string
	 */
	@RequestMapping("/s/orderExpress/{subNumber}")
	@Authorize(authorityTypes = AuthorityType.ORDERS_MANAGE)
	public String orderExpress(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumber) {

		SecurityUserDetail user = UserManager.getUser(request);

		MySubDto myorder = subService.findOrderDetail(subNumber, user.getShopId());
		request.setAttribute("order", myorder);
		String result = PathResolver.getPath(ShopFrontPage.ORDEREXPRESS);
		return result;
	}

	@RequestMapping("/s/invoiceManager")
	@Authorize(authorityTypes = AuthorityType.INVOICDE_MGT)
	public String invoiceManager(HttpServletRequest request, HttpServletResponse response, String curPageNO, InvoiceSubDto invoiceSubDto) {
		SecurityUserDetail user = UserManager.getUser(request);

		if (AppUtils.isBlank(curPageNO)) {
			curPageNO = "1";
		}

		PageSupport<InvoiceSubDto> ps = subService.getInvoiceOrder(curPageNO, user.getShopId(), invoiceSubDto);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("invoiceSubDto", invoiceSubDto);
		return PathResolver.getPath(ShopFrontPage.INVOICE_MANAGER);
	}


	@RequestMapping(value = "/s/addInvoice", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto addInvoice(HttpServletRequest request, HttpServletResponse response, String subNumber) {
		SecurityUserDetail user = UserManager.getUser(request);
		Sub sub = subService.getSubBySubNumber(subNumber);
		if (sub != null && sub.getShopId().equals(user.getShopId())) {
			sub.setHasInvoice(1);
			subService.updateSub(sub);
			return ResultDtoManager.success();
		}

		return ResultDtoManager.fail();
	}

	/**
	 * 导出
	 *
	 * @param request
	 * @param response
	 * @param
	 * @return
	 * @throws ParseException
	 * @throws IOException
	 */
	@RequestMapping(value = "/s/orderMange/export")
	@ResponseBody
	public String export(HttpServletRequest request, HttpServletResponse response, OrderSearchParamDto orderSearchParamDto) throws ParseException, IOException {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		orderSearchParamDto.setShopId(shopId);
		List<OrderExprotDto> dataset = orderService.exportOrder(orderSearchParamDto);
		ExportExcelUtil<OrderExprotDto> ex = new ExportExcelUtil<OrderExprotDto>();

		String[] headers = {"订单编号", "订单类型", "订单状态", "支付状态", "退款状态", "订单总价", "运费", "优惠券金额", "红包优惠", "配送方式", "支付类型名称(非预售)", "买家名称", "收货人", "手机号码", "买家地址", "买家留言", "商品名称", "商品数量", "商品属性",
				"下单时间", "店铺名称", "商品价格", "商品条形码", "商家编码", "退款状态2", "支付方式", "付款方式", "订金支付流水号", "订金金额", "订金支付类型名称(预售)", "尾款支付流水号", "尾款金额", "尾款支付类型名称(预售)", "尾款支付状态"};
		response.setHeader("Content-Disposition", "attachment;filename=orderList.xls");
		ex.exportSubExcelUtil(headers, dataset, response.getOutputStream());
		return "";
	}

	/**
	 * 商家添加备注
	 */
	@RequestMapping(value = "/s/orderMange/remarkShopOrder")
	@ResponseBody
	public Map<String, Object> remarkShopOrder(HttpServletRequest request, HttpServletResponse response, String subNum, String shopRemark) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		Map<String, Object> result = new HashMap<>();
		Sub sub = subService.getSubBySubNumber(subNum);

		if (AppUtils.isBlank(sub)) {
			result.put("status", Constants.FAIL);
			result.put("msg", "订单已不存在，请刷新页面或联系管理员");
			return result;
		}

		if (!shopId.equals(sub.getShopId())) {
			result.put("status", Constants.FAIL);
			result.put("msg", "备注失败，不是当前商家的订单");
			return result;
		}

		sub.setShopRemark(shopRemark);
		sub.setShopRemarkDate(new Date());
		sub.setIsShopRemarked(true);
		subService.updateSub(sub);

		result.put("status", Constants.SUCCESS);
		result.put("msg", "备注成功");
		return result;
	}

	/**
	 * 是否设置商家地址
	 */
	@RequestMapping(value = "/s/hasShopAddress")
	@ResponseBody
	public Map<String, Object> hasShopAddress(HttpServletRequest request, HttpServletResponse response) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		Map<String, Object> result = new HashMap<>();

		ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
		if (AppUtils.isBlank(shopDetail)) {
			result.put("status", Constants.FAIL);
			result.put("msg", "商家不存在");
			return result;
		}
		if (AppUtils.isBlank(shopDetail.getProvinceid()) || AppUtils.isBlank(shopDetail.getCityid())
				|| AppUtils.isBlank(shopDetail.getAreaid())) {

			result.put("status", Constants.FAIL);
			result.put("msg", "请先设置商家地址");
			return result;
		}
		result.put("status", Constants.SUCCESS);
		result.put("msg", "已设置");
		return result;
	}

}
