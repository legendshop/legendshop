/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ProdCommStatusEnum;
import com.legendshop.model.dto.ProductCommentDto;
import com.legendshop.model.dto.ProductCommentsQueryParams;
import com.legendshop.model.dto.ShopCommProdDto;
import com.legendshop.model.entity.ProdAddComm;
import com.legendshop.model.entity.ProductComment;
import com.legendshop.model.entity.ProductCommentCategory;
import com.legendshop.multishop.page.ShopFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ProdAddCommService;
import com.legendshop.spi.service.ProductCommentService;
import com.legendshop.util.SafeHtml;

/**
 * 评论控制器.
 */
@Controller
public class TradingController extends BaseController {

	private final Logger LOGGER = LoggerFactory.getLogger(TradingController.class);

	@Autowired
	private ProductCommentService productCommentService;

	@Autowired
	private ProdAddCommService prodAddCommService;

	/**
	 * 评价管理.
	 * 
	 * @param curPageNO
	 * @return the string
	 */
	@RequestMapping(value = "/s/comments/commentProdList")
	@Authorize(authorityTypes = AuthorityType.PROD_COMMENT)
	public String prodComment(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<ShopCommProdDto> ps = productCommentService.queryProdComment(curPageNO, shopId);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(ShopFrontPage.COMMENT_PROD_LIST);
	}

	/**
	 * 获取商品的所有评论
	 * 
	 * @param curPageNO
	 * @return the string
	 */
	@RequestMapping(value = "/s/comments/commentList")
	public String getCommentsList(HttpServletRequest request, HttpServletResponse response, ProductCommentsQueryParams params) {
		// 计算当前商品的评论情况
		ProductCommentCategory pcc = productCommentService.initProductCommentCategory(params.getProdId());
		request.setAttribute("prodCommCategory", pcc);

		getProdCommentList(request, response, params);

		return PathResolver.getPath(ShopFrontPage.COMMENT_LIST);
	}

	/**
	 * 异步加载商品的所有评论
	 * 
	 * @param curPageNO
	 * @return the string
	 */
	@RequestMapping(value = "/s/comments/commentListContent")
	public String loadCommentsListContent(HttpServletRequest request, HttpServletResponse response, ProductCommentsQueryParams params) {

		getProdCommentList(request, response, params);

		return PathResolver.getPath(ShopFrontPage.COMMENT_LIST_CONTETN);
	}

	private void getProdCommentList(HttpServletRequest request, HttpServletResponse response, ProductCommentsQueryParams params) {

		PageSupport<ProductCommentDto> ps = productCommentService.queryProductCommentDto(params);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("params", params);
	}

	/**
	 * 商家回复初次评论
	 * 
	 * @param curPageNO
	 * @param payType
	 * @return the string
	 */
	@RequestMapping(value = "/s/comments/replyComment", method = RequestMethod.POST)
	@ResponseBody
	public String shopReplyComment(HttpServletRequest request, HttpServletResponse response, @RequestParam Long prodComentId,
			@RequestParam String replyContent) {

		try {
			SecurityUserDetail user = UserManager.getUser(request);
			String userName = user.getUsername();

			ProductComment productComment = productCommentService.getProductCommentById(prodComentId);
			if (null == productComment) {
				return "对不起, 您要回复的评论不存在!";
			}

			if (!productComment.getOwnerName().equals(userName)) {
				return "对不起, 您无权回复此评论!";
			}

			if (!ProdCommStatusEnum.SUCCESS.value().equals(productComment.getStatus())) {
				return "对不起, 该评论暂不可以回复!";
			}

			if (productComment.getIsReply()) {
				return "对不起, 您已经回复过该评论了!";
			}

			SafeHtml safeHtml = new SafeHtml();
			String safeReplyContent = safeHtml.makeSafe(replyContent.trim());

			productComment.setIsReply(true);
			productComment.setShopReplyContent(safeReplyContent);
			productComment.setShopReplyTime(new Date());

			productCommentService.update(productComment);

			return Constants.SUCCESS;
		} catch (Exception e) {
			LOGGER.error("商家回复评论异常!", e);
			return Constants.FAIL;
		}
	}

	/**
	 * 商家回复追加评论
	 * 
	 * @return the string
	 */
	@RequestMapping(value = "/s/comments/replyAddComment", method = RequestMethod.POST)
	@ResponseBody
	public String replyAddComment(HttpServletRequest request, HttpServletResponse response, @RequestParam Long prodComentId,
			@RequestParam String replyContent) {

		try {
			SecurityUserDetail user = UserManager.getUser(request);
			String userName = user.getUsername();

			ProductComment productComment = productCommentService.getProductCommentById(prodComentId);

			if (null == productComment) {
				return "对不起, 您要回复的评论不存在!";
			}

			if (!productComment.getOwnerName().equals(userName)) {
				return "对不起, 您无权回复此评论!";
			}

			if (!ProdCommStatusEnum.SUCCESS.value().equals(productComment.getStatus()) || !productComment.getIsAddComm()) {
				return "对不起, 该评论暂不可以回复!";
			}

			ProdAddComm prodAddComm = prodAddCommService.getProdAddCommByCommId(prodComentId);

			if (!ProdCommStatusEnum.SUCCESS.value().equals(prodAddComm.getStatus())) {
				return "对不起, 该评论暂不可以回复!";
			}

			if (prodAddComm.getIsReply()) {
				return "对不起, 该评论你已经回复过了!";
			}

			prodAddComm.setShopReplyContent(replyContent);
			prodAddComm.setShopReplyTime(new Date());
			prodAddComm.setIsReply(true);

			prodAddCommService.updateProdAddComm(prodAddComm);

			return Constants.SUCCESS;
		} catch (Exception e) {
			LOGGER.error("商家回复追加评论异常", e);
			return Constants.FAIL;
		}
	}

	

}
