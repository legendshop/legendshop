/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.DeliveryType;
import com.legendshop.multishop.page.ShopFrontPage;
import com.legendshop.multishop.page.ShopRedirectPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.DeliveryTypeService;
import com.legendshop.util.AppUtils;

/**
 * 配送方式控制器
 *
 */
@Controller
public class ShopDeliveryTypeController {

	@Autowired
	private DeliveryTypeService deliveryTypeService;

	/**
	 * 查询配送方式
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/s/deliveryType")
	@Authorize(authorityTypes = AuthorityType.DELIVERY_TYPE)
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		SecurityUserDetail user = UserManager.getUser(request);
		if(AppUtils.isBlank(curPageNO)){
			curPageNO = "1";
		}
		PageSupport<DeliveryType> ps = deliveryTypeService.getDeliveryTypeCq(user.getShopId(), curPageNO);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(ShopFrontPage.DELIVERYTYPE_LIST);
	}

	/**
	 * 查看配送方式详情
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/s/deliveryType/load")
	@Authorize(authorityTypes = AuthorityType.DELIVERY_TYPE)
	public String load(HttpServletRequest request, HttpServletResponse response) {
		SecurityUserDetail user = UserManager.getUser(request);
		request.setAttribute("shopId", user.getShopId());
		return PathResolver.getPath(ShopFrontPage.DELIVERYTYPE_EDIT_PAGE);
	}
	
	/**
	 * 加载配送方式编辑页面
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/s/deliveryType/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		DeliveryType deliveryType = deliveryTypeService.getDeliveryType(id);
		request.setAttribute("deliveryType", deliveryType);
		request.setAttribute("shopId", user.getShopId());
		return PathResolver.getPath(ShopFrontPage.DELIVERYTYPE_EDIT_PAGE);
	}

	/**
	 * 保存配送方式
	 * @param request
	 * @param response
	 * @param deliveryType
	 * @return
	 */
	@RequestMapping(value = "/s/deliveryType/save")
	@Authorize(authorityTypes = AuthorityType.DELIVERY_TYPE)
	public String save(HttpServletRequest request, HttpServletResponse response, DeliveryType deliveryType) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		DeliveryType dt = null;
		if (AppUtils.isNotBlank(deliveryType.getDvyTypeId())) {
			dt = deliveryTypeService.getDeliveryType(deliveryType.getDvyTypeId());
			dt.setName(deliveryType.getName());
			dt.setDvyId(deliveryType.getDvyId());
			dt.setNotes(deliveryType.getNotes());
//			dt.setPrinttempId(deliveryType.getPrinttempId());
		} else {
//			long number = deliveryTypeService.checkMaxNumber(user.getShopId());
//			if (number >= 10) {
//				throw new BusinessException("最多可以添加10条配送方式");
//			}
			dt = deliveryType;
			dt.setCreateTime(new Date());
			dt.setIsSystem(0);
			dt.setShopId(user.getShopId());
		}
		dt.setPrinttempId(-1L);
		dt.setUserId(user.getUserId());
		dt.setUserName(user.getUsername());
		dt.setModifyTime(new Date());
		deliveryTypeService.saveDeliveryType(dt);

		return PathResolver.getPath(ShopRedirectPage.DELIVERYTYPE_LIST_QUERY);
	}
	
	/**
	 * 校验配送方式数量
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/s/deliveryType/checkNumber")
	@ResponseBody
	private String checkNumber(HttpServletRequest request, HttpServletResponse response){
		SecurityUserDetail user = UserManager.getUser(request);
		
		long number = deliveryTypeService.checkMaxNumber(user.getShopId());
		if (number >= 10) {
			return Constants.FAIL;
		}
		return Constants.SUCCESS;
	}

	/**
	 * 删除配送方式
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/s/deliveryType/delete/{id}")
	@Authorize(authorityTypes = AuthorityType.DELIVERY_TYPE)
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		DeliveryType deliveryType = deliveryTypeService.getDeliveryType(id);
		deliveryTypeService.deleteDeliveryType(deliveryType);
		return PathResolver.getPath(ShopRedirectPage.DELIVERYTYPE_LIST_QUERY);
	}

}
