/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.multishop.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.util.XssFilterUtil;
import com.legendshop.util.Arith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.exception.NotFoundException;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.KeyValueListEntity;
import com.legendshop.model.ResultCustomPropertyDto;
import com.legendshop.model.SendArrivalInform;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.BrandStatusEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ImageTypeEnum;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.constant.SeckillStatusEnum;
import com.legendshop.model.constant.SupportDistEnum;
import com.legendshop.model.dto.BrandDto;
import com.legendshop.model.dto.CategorySearchDto;
import com.legendshop.model.dto.CategoryTree;
import com.legendshop.model.dto.ProductDto;
import com.legendshop.model.dto.ProductIdDto;
import com.legendshop.model.dto.ProductPropertyDto;
import com.legendshop.model.dto.ProductPropertyValueDto;
import com.legendshop.model.dto.PublishProductDto;
import com.legendshop.model.dto.ShopCategoryDto;
import com.legendshop.model.dto.TreeNode;
import com.legendshop.model.entity.AfterSale;
import com.legendshop.model.entity.Brand;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.CategoryComm;
import com.legendshop.model.entity.ProdType;
import com.legendshop.model.entity.ProdUserAttribute;
import com.legendshop.model.entity.ProdUserParam;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductConsult;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.ProductImport;
import com.legendshop.model.entity.ProductPropertyValue;
import com.legendshop.model.entity.SeckillActivity;
import com.legendshop.model.entity.ShopCategory;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.Sku;
import com.legendshop.model.entity.StockLog;
import com.legendshop.model.entity.Transport;
import com.legendshop.multishop.page.BackPage;
import com.legendshop.multishop.page.ShopFrontPage;
import com.legendshop.multishop.page.ShopRedirectPage;
import com.legendshop.processor.SendArrivalInformProcessor;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.AfterSaleService;
import com.legendshop.spi.service.BrandService;
import com.legendshop.spi.service.CategoryCommService;
import com.legendshop.spi.service.CategoryManagerService;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.spi.service.ProdTypeService;
import com.legendshop.spi.service.ProductConsultService;
import com.legendshop.spi.service.ProductImportService;
import com.legendshop.spi.service.ProductPropertyService;
import com.legendshop.spi.service.ProductPropertyValueService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.SeckillActivityService;
import com.legendshop.spi.service.ShopCategoryService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.SkuService;
import com.legendshop.spi.service.StockLogService;
import com.legendshop.spi.service.TransportService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.SafeHtml;

/**
 * 产品管理控制器.
 */
@SuppressWarnings("all")
@Controller
public class ProdManageController extends BaseController {

    private final static Logger log = LoggerFactory.getLogger(ProdManageController.class);

    @Autowired
    private BrandService brandService;

    @Autowired
    private ProductService productService;

    @Autowired
    private SkuService skuService;

    @Autowired
    private StockLogService stockLogService;

    @Autowired
    private ProductPropertyService productPropertyService;

    @Autowired
    private TransportService transportService;

    @Autowired
    private AfterSaleService afterSaleService;

    @Autowired
    private ProdTypeService prodTypeService;

    @Autowired
    private ProductPropertyValueService productPropertyValueService;

    @Autowired
    private AttachmentManager attachmentManager;

    @Autowired
    private ShopCategoryService shopCategoryService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryManagerService categoryManagerService;

    @Autowired
    private CategoryCommService categoryCommService;

    @Autowired
    private ProductConsultService productConsultService;

    @Autowired
    private ProductImportService productImportService;

    @Autowired
    private ShopDetailService shopDetailService;

    @Resource(name = "productIndexDelProcessor")
    private EventProcessor<ProductIdDto> productIndexDelProcessor;

    @Resource(name = "productIndexSaveProcessor")
    private EventProcessor<ProductIdDto> productIndexSaveProcessor;

    @Resource(name = "productIndexUpdateProcessor")
    private EventProcessor<ProductIdDto> productIndexUpdateProcessor;

    @Autowired
    private SendArrivalInformProcessor sendArrivalInformProcessor;

    @Autowired
    private SeckillActivityService seckillActivityService;

    /**
     * 发布产品.
     *
     * @param request
     * @param response
     * @param impId
     * @return the string
     */
    @RequestMapping(value = "/s/publish")
    @Authorize(authorityTypes = AuthorityType.PUBLISH_PROD)
    public String publish(HttpServletRequest request, HttpServletResponse response, Long impId) {
        Long topLevel = 0L;
        List<Category> categoryList = categoryService.findCategory(topLevel);
        request.setAttribute("categoryList", categoryList);

        // 找出常用分类
        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        List<CategoryComm> categoryCommList = categoryCommService.getCategoryCommListByShopId(shopId);
        for (CategoryComm categoryComm : categoryCommList) {
            List<TreeNode> treeNodes = categoryManagerService.getTreeNodeNavigation(categoryComm.getCategoryId(), ",");
            categoryComm.setTreeNodes(treeNodes);
        }
        request.setAttribute("categoryCommList", categoryCommList);

        if (AppUtils.isNotBlank(categoryList)) {
            request.setAttribute("level", categoryList.get(0).getGrade());
        }

        request.setAttribute("impId", impId);
        return PathResolver.getPath(ShopFrontPage.PROD_PUBLISH_PAGE);
    }

    /**
     * 发布产品.
     *
     * @param request
     * @param response
     * @param prodId
     * @param categoryId
     * @return the string
     */
    @RequestMapping(value = "/s/publish/{prodId}")
    public String publish(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId, Long categoryId) {
        Long topLevel = 0L;
        List<Category> categoryList = categoryService.findCategory(topLevel);
        request.setAttribute("categoryList", categoryList);
        if (AppUtils.isNotBlank(prodId) && !prodId.equals(0L)) {
            request.setAttribute("prodId", prodId);
        }

        // 找出常用分类
        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        List<CategoryComm> categoryCommList = categoryCommService.getCategoryCommListByShopId(shopId);
        for (CategoryComm categoryComm : categoryCommList) {
            List<TreeNode> treeNodes = categoryManagerService.getTreeNodeNavigation(categoryComm.getCategoryId(), ",");
            categoryComm.setTreeNodes(treeNodes);
        }
        request.setAttribute("categoryCommList", categoryCommList);

        if (AppUtils.isNotBlank(categoryList)) {
            request.setAttribute("level", categoryList.get(0).getGrade());
        }
        if (AppUtils.isNotBlank(categoryId)) {
            String treeNodeStr = categoryManagerService.getNodeObjectIdPath(0L, categoryId, ",");
            request.setAttribute("treeNodeStr", treeNodeStr);
        }
        return PathResolver.getPath(ShopFrontPage.PROD_PUBLISH_PAGE);
    }

    /**
     * 到选择分类页面.
     *
     * @param request
     * @param response
     * @param prodId
     * @param categoryId
     * @return the string
     */
    @RequestMapping(value = "/s/publishSimilar/{prodId}")
    public String publishSimilar(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId, Long categoryId) {
        request.setAttribute("isSimilar", 1);// 发布相似商品 才设改值
        return this.publish(request, response, prodId, categoryId);
    }

    /**
     * 选择类似商品导入 弹框页.
     *
     * @param request
     * @param response
     * @param curPageNO
     * @param product
     * @return the string
     */
    @RequestMapping(value = "/s/loadProdListPage")
    @Authorize(authorityTypes = AuthorityType.PUBLISH_PROD)
    public String loadProdListPage(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        PageSupport<Product> ps = productService.getloadProdListPageByshopId(curPageNO, shopId, product);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("prod", product);
        return PathResolver.getPath(ShopFrontPage.LOAD_PROD_LIST_PAGE);
    }

    /**
     * 选择要加入分销的商品.
     *
     * @param request
     * @param response
     * @param curPageNO
     * @param product
     * @return the string
     */
    @RequestMapping(value = "/s/loadWantDistProd")
    public String loadWantDistProd(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        PageSupport<Product> ps = productService.getloadWantDistProd(curPageNO, shopId, product);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("prod", product);
        return PathResolver.getPath(ShopFrontPage.LOAD_WANT_DIST_PROD);
    }

    /**
     * 添加分销商品.
     *
     * @param request
     * @param response
     * @param prodId
     * @param distCommisRate
     * @return the string
     */
    @RequestMapping(value = "/s/addDistProd")
    @ResponseBody
    public String addDistProd(HttpServletRequest request, HttpServletResponse response, Long prodId, Double distCommisRate) {
        if (AppUtils.isNotBlank(prodId)) {

            SecurityUserDetail user = UserManager.getUser(request);
            Long shopId = user.getShopId();

            Product product = productService.getProductById(prodId);
            if (shopId.equals(product.getShopId())) {
                if (distCommisRate > 0 && distCommisRate < 1) {
                    product.setSupportDist(SupportDistEnum.SUPPORT.value());
                    product.setDistCommisRate(distCommisRate);
                    productService.updateProdDistCommis(product);
                    productIndexUpdateProcessor.process(new ProductIdDto(product.getProdId()));
                    return Constants.SUCCESS;
                }
            }
        }
        return Constants.FAIL;
    }

    /**
     * 商品退出分销.
     *
     * @param request
     * @param response
     * @param prodId
     * @return the string
     */
    @RequestMapping(value = "/s/quitDist")
    @ResponseBody
    public String quitDist(HttpServletRequest request, HttpServletResponse response, Long prodId) {
        if (AppUtils.isNotBlank(prodId)) {
            SecurityUserDetail user = UserManager.getUser(request);
            Long shopId = user.getShopId();
            Product product = productService.getProductById(prodId);
            if (product != null && shopId.equals(product.getShopId())) {
                product.setSupportDist(SupportDistEnum.NOT_SUPPORT.value());
                product.setDistCommisRate(0d);
                productService.updateProdDistCommis(product);
                productIndexUpdateProcessor.process(new ProductIdDto(product.getProdId()));
                return Constants.SUCCESS;
            }
        }
        return Constants.FAIL;
    }

    /**
     * 查找下一级商品分类.
     *
     * @param request
     * @param response
     * @param categoryId
     * @return the string
     */
    @RequestMapping(value = "/s/publish2/{categoryId}")
    public String nextCategory(HttpServletRequest request, HttpServletResponse response, @PathVariable Long categoryId) {

        List<Category> categoryList = categoryService.findCategory(categoryId);
        request.setAttribute("categoryList", categoryList);

        if (AppUtils.isNotBlank(categoryList)) {
            request.setAttribute("level", categoryList.get(0).getGrade());
        }
        return PathResolver.getPath(ShopFrontPage.PROD_PUBLISH2_PAGE);
    }

    /**
     * 保存常用分类.
     *
     * @param request
     * @param response
     * @param categoryId
     * @return the string
     */
    @RequestMapping(value = "/s/saveCategoryComm/{categoryId}")
    @ResponseBody
    public String saveCategoryComm(HttpServletRequest request, HttpServletResponse response, @PathVariable Long categoryId) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        CategoryComm categoryComm = categoryCommService.getCategoryComm(categoryId, shopId);
        if (AppUtils.isBlank(categoryComm)) {
            categoryComm = new CategoryComm();
            categoryComm.setCategoryId(categoryId);
            categoryComm.setShopId(shopId);
            categoryComm.setRecDate(new Date());
            categoryCommService.saveCategoryComm(categoryComm);
            return Constants.SUCCESS;
        } else {
            return Constants.EXIST;
        }
    }

    /**
     * 删除常用分类.
     *
     * @param request
     * @param response
     * @param id
     * @return the string
     */
    @RequestMapping(value = "/s/delCategoryComm/{id}")
    @ResponseBody
    public String delCategoryComm(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        categoryCommService.delCategoryComm(id, shopId);
        return Constants.SUCCESS;
    }

    /**
     * 加载常用分类.
     *
     * @param request
     * @param response
     * @return the string
     */
    @RequestMapping(value = "/s/loadCategoryComm")
    public String loadCategoryComm(HttpServletRequest request, HttpServletResponse response) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        List<CategoryComm> categoryCommList = categoryCommService.getCategoryCommListByShopId(shopId);
        for (CategoryComm categoryComm : categoryCommList) {
            List<TreeNode> treeNodes = categoryManagerService.getTreeNodeNavigation(categoryComm.getCategoryId(), ",");
            categoryComm.setTreeNodes(treeNodes);
        }
        request.setAttribute("categoryCommList", categoryCommList);
        return PathResolver.getPath(ShopFrontPage.LOAD_CATEGORY_COMM_PAGE);
    }

    /**
     * 发布商品搜索分类.
     *
     * @param request
     * @param response
     * @param categoryName
     * @return the string
     */
    @RequestMapping(value = "/s/searchCategory")
    public String searchCategory(HttpServletRequest request, HttpServletResponse response, String categoryName) {
        List<TreeNode> treeNodeList = new ArrayList<TreeNode>();
        Map<Long, TreeNode> treeNodeMap = new LinkedHashMap<Long, TreeNode>();
        List<TreeNode> leafTreeNodeList = new ArrayList<TreeNode>();
        List<CategorySearchDto> catDtoList = new ArrayList<CategorySearchDto>();
        // 找出所有 含有搜索关键字 的分类
        List<Category> catList = categoryService.getCategoryListByName(categoryName);
        if (AppUtils.isNotBlank(catList)) {
            // 找出所有的晚辈集合
            for (Category category : catList) {

                //修复无法搜索类目的问题
                /*TreeNode treeNode = categoryManagerService.getTreeNodeById(category.getId());*/
                TreeNode treeNode = categoryManagerService.getAllTreeNodeById(category.getId());
                if (AppUtils.isNotBlank(treeNode)) {
                    treeNodeMap.put(treeNode.getSelfId(), treeNode);
                    List<TreeNode> juniors = treeNode.getJuniors();
                    if (AppUtils.isNotBlank(juniors)) {
                        for (TreeNode juniorTreeNode : juniors) {
                            treeNodeMap.put(juniorTreeNode.getSelfId(), juniorTreeNode);
                        }
                    }
                }
            }
            treeNodeList.addAll(treeNodeMap.values());
            // 找出所有的叶子节点集合
            for (TreeNode treeNode : treeNodeList) {
                if (AppUtils.isBlank(treeNode.getChildList())) {
                    leafTreeNodeList.add(treeNode);
                }
            }
            // 组装 面包屑导航名称
            for (TreeNode treeNode : leafTreeNodeList) {
                CategorySearchDto categorySearchDto = new CategorySearchDto();
                categorySearchDto.setCategoryId(treeNode.getSelfId());
                List<TreeNode> parentTreeNodes = categoryManagerService.getTreeNodeNavigation(treeNode.getSelfId(), ",");
                String navigationName = "";
                for (int i = 0; i < parentTreeNodes.size(); i++) {
                    TreeNode naviTreeNode = parentTreeNodes.get(i);
                    if (i > 0) {
                        navigationName += ">>";
                    }
                    navigationName += naviTreeNode.getNodeName();
                }
                categorySearchDto.setNavigationName(navigationName);
                catDtoList.add(categorySearchDto);
            }
        }
        request.setAttribute("catDtoList", catDtoList);
        return PathResolver.getPath(ShopFrontPage.SEARCH_CATEGORY_PAGE);
    }

    /**
     * 根据名称和类型找到对应的商品分类 发布产品是选择分类.
     *
     * @param request
     * @param response
     * @param cateName
     * @param level
     * @param parentId
     * @return the string
     */
    @RequestMapping(value = "/s/loadCategory")
    public String loadCategory(HttpServletRequest request, HttpServletResponse response, String cateName, Integer level, Long parentId) {
        if (AppUtils.isBlank(parentId)) {
            parentId = 0L;
        }
        List<Category> cateList = categoryService.findCategory(cateName, level, parentId);
        request.setAttribute("cateList", cateList);
        return PathResolver.getPath(ShopFrontPage.LOAD_CATEGORY_PAGE);
    }

    /**
     * 产品发布页面.
     *
     * @param request
     * @param response
     * @param categoryId
     * @param impId
     * @return the string
     */
    @RequestMapping(value = "/s/publishProd")
    public String publishProd(HttpServletRequest request, HttpServletResponse response, Long categoryId, Long impId) {

        PublishProductDto publishProductDto = productPropertyService.queryProductProperty(categoryId);
        ProductDto productDto = new ProductDto();
        if(AppUtils.isNotBlank(publishProductDto.isAttrEditable())){
            productDto.setAttrEditable(publishProductDto.isAttrEditable()==1?true:false);
        }
        productDto.setProdPropDtoList(publishProductDto.getSpecDtoList());

        // 商品导入进来 赋值
        if (impId != null) {
            ProductImport productImport = productImportService.getProductImport(impId);
            if (productImport != null) {
                productDto.setName(productImport.getProdName());
                productDto.setCash(productImport.getCash());
                productDto.setPrice(productImport.getPrice());
                productDto.setBrief(productImport.getBrief());
                productDto.setKeyWord(productImport.getKeyWord());
                productDto.setMetaTitle(productImport.getMetaTitle());
                productDto.setMetaDesc(productImport.getMetaDesc());
                productDto.setModelId(productImport.getModelId());
                productDto.setPartyCode(productImport.getPartyCode());
                if (productImport.getStocksArm() != null) {
                    productDto.setStocksArm(productImport.getStocksArm().intValue());
                }
                request.setAttribute("impId", impId);
            }
        }

        request.setAttribute("categoryId", categoryId);
        request.setAttribute("productDto", productDto);
        request.setAttribute("publishProductDto", publishProductDto);

        // 属性的大小
        int specSize = publishProductDto.getSpecDtoList() == null ? 0 : publishProductDto.getSpecDtoList().size();
        request.setAttribute("specSize", specSize);

        // 加载运费模板选项
        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
        request.setAttribute("shopDetail", shopDetail);
        List<Transport> transportList = transportService.getTransports(shopId);
        request.setAttribute("transportList", transportList);

        // 查询该分类
        if (AppUtils.isNotBlank(categoryId)) {
            List<TreeNode> treeNodes = categoryManagerService.getTreeNodeNavigation(categoryId, ",");
            request.setAttribute("treeNodes", treeNodes);
        }

        return PathResolver.getPath(ShopFrontPage.PROD_PUBLISH_PROD_PAGE);
    }

    /**
     * 产品发布页面更新页面.
     *
     * @param request
     * @param response
     * @param prodId     商品Id
     * @param categoryId 分类Id
     * @param isSimilar  是否类似商品导入
     * @return 页面
     */
    @RequestMapping(value = "/s/updateProd/{prodId}")
    @Authorize(authorityTypes = AuthorityType.SELLING_PROD)
    public String updateProd(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId, Long categoryId, String isSimilar) {
        // 商品
        Product product = productService.getProductById(prodId);
        if (product == null) {
            throw new BusinessException("product is not null");
        }
        // 当前用户的shopId
        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);

        if (!shopId.equals(product.getShopId())) {
            throw new BusinessException("product is not ower to shop " + shopId);
        }

        Long publishCategoryId = categoryId;
        if (AppUtils.isBlank(publishCategoryId)) {
            publishCategoryId = product.getCategoryId();
        }

        boolean isSimilarImport = AppUtils.isNotBlank(isSimilar);// 如果非空则是类似商品导入

        //1. 获取商品分类下的标准信息
        PublishProductDto publishProductDto = productPropertyService.queryProductProperty(publishCategoryId);

        //2. 计算该商品的属性
        ProductDto productDto = productService.getProductDto(publishProductDto, product, prodId, isSimilarImport);

        if (AppUtils.isNotBlank(categoryId)) {// 更新商品和分类
            productDto.setSkuDtoList(null);
            productDto.setPropNameList(null);
            // 清除选中的属性值
            List<ProductPropertyDto> ppDtoList = productDto.getProdPropDtoList();
            if (AppUtils.isNotBlank(ppDtoList)) {
                for (ProductPropertyDto productPropertyDto : ppDtoList) {
                    List<ProductPropertyValueDto> ppvList = productPropertyDto.getProductPropertyValueList();
                    if (AppUtils.isNotBlank(ppvList)) {
                        for (ProductPropertyValueDto productPropertyValueDto : ppvList) {
                            productPropertyValueDto.setIsSelected(false);
                        }
                    }
                }
            }
            productDto.setPropImageDtoList(null);
            productDto.setParameter(null);
            productDto.setCategoryId(categoryId);
        }
        List<TreeNode> treeNodes = categoryManagerService.getTreeNodeNavigation(publishCategoryId, ",");

        request.setAttribute("treeNodes", treeNodes);
        request.setAttribute("productDto", productDto);
        request.setAttribute("categoryId", publishCategoryId);
        request.setAttribute("publishProductDto", publishProductDto);
        request.setAttribute("shopDetail", shopDetail);

        //添加编辑标识
        request.setAttribute("edit", "1");

        // 加载运费模板选项
        List<Transport> transportList = transportService.getTransports(shopId);
        request.setAttribute("transportList", transportList);

        return PathResolver.getPath(ShopFrontPage.PROD_PUBLISH_PROD_PAGE);
    }

    /**
     * 发布相似商品.
     *
     * @param request
     * @param response
     * @param prodId
     * @param categoryId
     * @return the string
     */
    @RequestMapping(value = "/s/similarProd/{prodId}")
    public String similarProd(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId, Long categoryId) {
        request.setAttribute("isSimilar", 1);// 发布相似商品才设该值，如果是相似商品则需要解决用户的自定义属性的问题， Remark： Newway
		request.setAttribute("rsImage", 1);
		return this.updateProd(request, response, prodId, categoryId, "1");
    }

    /**
     * 查询动态属性弹框.
     *
     * @param request
     * @param response
     * @param curPageNO
     * @param prodUserAttribute
     * @return the string
     */
    @RequestMapping("/s/attributeOverlay")
    public String prodSpecOverlay(HttpServletRequest request, HttpServletResponse response, String curPageNO, ProdUserAttribute prodUserAttribute) {
        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();
        PageSupport<ProdUserAttribute> ps = productService.getProdUserAttributeByPage(curPageNO, shopId);
        PageSupportHelper.savePage(request, ps);
        return PathResolver.getPath(ShopFrontPage.ATTRIBUTE_OVERLAY);
    }

    /**
     * 删除用户自定义属性.
     *
     * @param request
     * @param response
     * @param id
     * @return the string
     */
    @RequestMapping("/s/UserAttribute/delete")
    public @ResponseBody
    String deleteProdspec(HttpServletRequest request, HttpServletResponse response, Long id) {
        SecurityUserDetail user = UserManager.getUser(request);
        productService.deleteProdUserAttribute(id, user.getShopId());
        return Constants.SUCCESS;
    }

    /**
     * 使用此动态属性.
     *
     * @param request
     * @param response
     * @param curPageNO
     * @param id
     * @return the string
     */
    @RequestMapping("/s/useAttribute/{id}")
    public @ResponseBody
    String useProdspec(HttpServletRequest request, HttpServletResponse response, String curPageNO, @PathVariable Long id) {
        ProdUserAttribute prodUserAttr = productService.getProdUserAttribute(id);
        return prodUserAttr.getContent();
    }

    /**
     * 去往保存属性弹框的div.
     *
     * @param request
     * @param response
     * @return the string
     */
    @RequestMapping("/s/saveAttribute")
    public String saveProdspec(HttpServletRequest request, HttpServletResponse response) {
        return PathResolver.getPath(ShopFrontPage.SAVE_ATTRIBUTE);
    }

    /**
     * 保存用户自定义属性模板.
     *
     * @param request
     * @param response
     * @param content
     * @param name
     * @return the string
     */
    @RequestMapping("/s/UserAttribute/save")
    public @ResponseBody
    String saveSpec(HttpServletRequest request, HttpServletResponse response, String content, String name) {
        if (AppUtils.isBlank(name)) {
            log.warn("Name can not empty when save userParameter!");
            return Constants.FAIL;
        }

        List<KeyValueListEntity> contentList = JSONUtil.getArray(content, KeyValueListEntity.class);
        if (AppUtils.isBlank(contentList)) {
            log.warn("Content can not empty when save userParameter!");
            return Constants.FAIL;
        }

        boolean haveData = false;
        for (KeyValueListEntity entity : contentList) {
            if (AppUtils.isNotBlank(entity.getKey()) || AppUtils.isNotBlank(entity.getValue())) {
                haveData = true;
                break;
            }
        }

        if (!haveData) {
            log.warn("Content format incorrect when save userParameter!");
            return Constants.FAIL;
        }

        SecurityUserDetail userDetail = UserManager.getUser(request);
        ProdUserAttribute prodUserAttribute = new ProdUserAttribute();
        prodUserAttribute.setContent(content);
        prodUserAttribute.setName(name);
        prodUserAttribute.setUserId(userDetail.getUserId());
        prodUserAttribute.setShopId(userDetail.getShopUser().getShopId());
        prodUserAttribute.setRecDate(new Date());
        productService.saveProdUserAttribute(prodUserAttribute);
        return Constants.SUCCESS;
    }

    /**
     * 去往选择用户参数弹框.
     *
     * @param request
     * @param response
     * @param curPageNO
     * @param prodUserParam
     * @return the string
     */
    @RequestMapping("/s/userParamOverlay")
    public String userParamOverlay(HttpServletRequest request, HttpServletResponse response, String curPageNO, ProdUserParam prodUserParam) {
        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();
        PageSupport<ProdUserParam> ps = productService.getUserParamOverlay(curPageNO, shopId);
        PageSupportHelper.savePage(request, ps);
        return PathResolver.getPath(ShopFrontPage.USERPARAM_OVERLAY);
    }

    /**
     * 删除用户自定义参数.
     *
     * @param request
     * @param response
     * @param id
     * @return the string
     */
    @RequestMapping("/s/userParam/delete")
    public @ResponseBody
    String deleteParam(HttpServletRequest request, HttpServletResponse response, Long id) {
        SecurityUserDetail user = UserManager.getUser(request);
        productService.deleteProdUserParam(id, user.getShopId());
        return Constants.SUCCESS;
    }

    /**
     * 去往保存用户自定义参数的div.
     *
     * @param request
     * @param response
     * @return the string
     */
    @RequestMapping("/s/saveUserParam")
    public String saveUserParam(HttpServletRequest request, HttpServletResponse response) {
        return PathResolver.getPath(ShopFrontPage.SAVE_USERPARAM);
    }

    /**
     * 保存用户自定义参数.
     *
     * @param request
     * @param response
     * @param content
     * @param name
     * @return the string
     */
    @RequestMapping("/s/userParam/save")
    public @ResponseBody
    String userParamSave(HttpServletRequest request, HttpServletResponse response, String content, String name) {
        if (AppUtils.isBlank(name)) {
            log.warn("Name can not empty when save userParameter!");
            return Constants.FAIL;
        }

        List<KeyValueEntity> contentList = JSONUtil.getArray(content, KeyValueEntity.class);
        if (AppUtils.isBlank(contentList)) {
            log.warn("Content can not empty when save userParameter!");
            return Constants.FAIL;
        }

        boolean haveData = false;
        for (KeyValueEntity entity : contentList) {
            if (AppUtils.isNotBlank(entity.getKey()) || AppUtils.isNotBlank(entity.getValue())) {
                haveData = true;
                break;
            }
        }

        if (!haveData) {
            log.warn("Content format incorrect when save userParameter!");
            return Constants.FAIL;
        }

        SecurityUserDetail user = UserManager.getUser(request);
        String userId = user.getUserId();
        Long shopId = user.getShopId();

        ProdUserParam prodUserParam = new ProdUserParam();
        prodUserParam.setContent(content);
        prodUserParam.setName(name);
        prodUserParam.setRecDate(new Date());
        prodUserParam.setUserId(userId);
        prodUserParam.setShopId(shopId);
        productService.saveProdUserParam(prodUserParam);
        return Constants.SUCCESS;
    }

    /**
     * Use param.
     *
     * @param request
     * @param response
     * @param curPageNO
     * @param id
     * @return the string
     */
    @RequestMapping("/s/useParam/{id}")
    public @ResponseBody
    String useParam(HttpServletRequest request, HttpServletResponse response, String curPageNO, @PathVariable Long id) {
        ProdUserParam prodUserParam = productService.getProdUserParam(id);
        return prodUserParam.getContent();
    }

    /**
     * 去往售后弹框页面.
     *
     * @param request   the request
     * @param response  the response
     * @param curPageNO the cur page no
     * @return the string
     */
    @RequestMapping("/s/afterSaleOverlay")
    public String afterSaleOverlay(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
        SecurityUserDetail user = UserManager.getUser(request);
        String userId = user.getUserId();
        String userName = user.getUsername();
        PageSupport<AfterSale> ps = afterSaleService.getAfterSalePage(curPageNO, userName, userId);
        PageSupportHelper.savePage(request, ps);
        return PathResolver.getPath(ShopFrontPage.AFTERSALE_OVERLAY);
    }

    /**
     * 保存或更新售后说明.
     *
     * @param request
     * @param response
     * @param id
     * @param title
     * @param content
     * @return the long
     */
    @RequestMapping(value = "/s/saveAfterSale")
    public @ResponseBody
    Long saveAfterSale(HttpServletRequest request, HttpServletResponse response, Long id, String title, String content) {
        SecurityUserDetail user = UserManager.getUser(request);
        String userId = user.getUserId();
        String userName = user.getUsername();
        Long shopId = user.getShopId();
        AfterSale afterSale = new AfterSale();
        afterSale.setId(id);
        afterSale.setRecDate(new Date());
        afterSale.setTitle(title);
        afterSale.setContent(content);
        afterSale.setShopId(shopId);
        afterSale.setUserId(userId);
        afterSale.setUserName(userName);
        Long afterSaleId = afterSaleService.saveAfterSale(afterSale);
        return afterSaleId;
    }

    /**
     * 删除售后说明.
     *
     * @param request
     * @param response
     * @param id
     * @return the string
     */
    @RequestMapping(value = "/s/deleteAfterSale")
    public @ResponseBody
    String deleteAfterSale(HttpServletRequest request, HttpServletResponse response, Long id) {
        if (AppUtils.isBlank(id)) {
            return Constants.FAIL;
        } else {
            afterSaleService.deleteByAfterSaleId(id);
            return Constants.SUCCESS;
        }
    }

    /**
     * 上传商品图片文件.
     *
     * @param request
     * @param response
     * @param files
     * @return the string
     */
    @RequestMapping(value = "/s/prodpicture/save")
    public @ResponseBody
    String savePhoto(MultipartHttpServletRequest request, HttpServletResponse response, @RequestParam MultipartFile[] files) {
        SecurityUserDetail user = UserManager.getUser(request);
        String picStrs = "";
        if (AppUtils.isNotBlank(files)) {
            try {
                for (MultipartFile file : files) {
                    System.out.println(file.getSize());
                    if (file.getSize() > 0) {
                        String pic = attachmentManager.upload(file);
                        attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), pic, file, AttachmentTypeEnum.PRODUCT);
                        if (AppUtils.isNotBlank(picStrs)) {
                            picStrs += ",";
                        }
                        picStrs += pic;
                    }
                }
            } catch (Exception e) {
                return Constants.FAIL;
            }
        }
        return picStrs;
    }


    /**
     * 上传商品视频.
     *
     * @param request
     * @param response
     * @param files
     * @return the string
     */
    @RequestMapping(value = "/s/prodpicture/saveVedio")
    public @ResponseBody
    String savePhotoVedio(MultipartHttpServletRequest request, HttpServletResponse response, @RequestParam MultipartFile[] files) {
        SecurityUserDetail user = UserManager.getUser(request);
        String picStrs = "";

        if (AppUtils.isNotBlank(files)) {
            try {
                for (MultipartFile file : files) {
                    if (file.getSize() >= 31457280) {
                        throw new Exception("视频大于30mb");
                    }
                    if (file.getSize() > 0) {
                        String pic = attachmentManager.upload(file);
                        attachmentManager.saveAttachment(user.getUsername(), user.getUserId(), user.getShopId(), pic, file, AttachmentTypeEnum.PRODUCT, ImageTypeEnum.VIDEO);
                        if (AppUtils.isNotBlank(picStrs)) {
                            picStrs += ",";
                        }
                        picStrs += pic;
                    }
                }
            } catch (Exception e) {
                return Constants.FAIL;
            }
        }
        return picStrs;
    }


    /**
     * Save color image.
     *
     * @param request
     * @param response
     * @param valueImageFiles
     * @return the string
     */
    @RequestMapping(value = "/s/propImage/save")
    public @ResponseBody
    String saveColorImage(MultipartHttpServletRequest request, HttpServletResponse response,
                          @RequestParam MultipartFile[] valueImageFiles) {
        SecurityUserDetail user = UserManager.getUser(request);
        String picStrs = "";
        if (AppUtils.isNotBlank(valueImageFiles)) {
            try {
                for (MultipartFile file : valueImageFiles) {
                    String pic = attachmentManager.upload(file);
                    attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), pic, file, AttachmentTypeEnum.PRODPROPVAL);
                    if (AppUtils.isNotBlank(picStrs)) {
                        picStrs += ",";
                    }
                    picStrs += pic;
                }
            } catch (Exception e) {
                return Constants.FAIL;
            }
        }
        return picStrs;
    }

    /**
     * 保存商品 1、检查用户是否登录 2、检查商品是否有图片 3、对商品sku的保存.
     *
     * @param request
     * @param response
     * @param product  商品
     * @param impId    导入的商品Id
     * @return 返回页面
     */
    @RequestMapping(value = "/s/saveProd")
    public String saveProd(HttpServletRequest request, HttpServletResponse response, Product product, Long impId) {

        // 检查用户是否登录
        SecurityUserDetail userDetail = UserManager.getUser(request);
        String userId = userDetail.getUserId();
        String userName = userDetail.getUsername();
        Long shopId = userDetail.getShopUser().getShopId();
        String shopName = userDetail.getShopUser().getShopName();

		if(AppUtils.isBlank(product.getName()) || AppUtils.isBlank(product.getCash())){
			request.setAttribute(Constants.MESSAGE, "商品信息错误！请检查商品名称或价格，填写正确的发布信息！");
			return  PathResolver.getPath(ShopFrontPage.OPERATION_ERROR);
		}

        //过滤html代码
        product.setName(XssFilterUtil.cleanXSS(product.getName()));

        boolean hasProdId = AppUtils.isNotBlank(product.getProdId());//由于调dao的save方法会set进去prodId。所以提前标志

        // 发布商品
        Long prodId = productService.releaseOfProduct(shopId, userId, userName, product);
        //到货通知需要重新设计

        //发送到货通知消息
        List<Sku> list = product.getUpdateSkuList();
        if (list != null) {
            for (Sku sku : list) {
                long skuId = sku.getSkuId();
                long stocks = sku.getStocks();
                sendArrivalInformProcessor.process(new SendArrivalInform(skuId, stocks, shopName));
            }
        }

        // 如果是商品导入进来，发布保存完，清除导入表的记录
        if (impId != null) {
            productImportService.deleteProductImportById(impId);
        }

        if (hasProdId) {
            //如果商品ID不为空，更新商品索引
			productIndexUpdateProcessor.process(new ProductIdDto(product.getProdId()));
        } else {
            product.setId(prodId);
            //创建商品后 ，索引数据
            productIndexSaveProcessor.process(new ProductIdDto(product.getProdId()));
        }

        return PathResolver.getPath(ShopRedirectPage.PRODUCT_VIEWS_QUERY) + "/" + prodId;
    }

    /**
     * 出售中的商品.
     *
     * @param request
     * @param response
     * @param curPageNO
     * @param product
     * @return the string
     */
    @RequestMapping(value = "/s/sellingProd")
    @Authorize(authorityTypes = AuthorityType.SELLING_PROD)
    public String sellingProd(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {

        SecurityUserDetail user = UserManager.getUser(request);

        if (!UserManager.isShopUser(user)) {// 还没有开店
            return PathResolver.getPath(ShopRedirectPage.SHOP_AGREEMENT);
        }

        PageSupport<Product> ps = productService.getSellingProd(curPageNO, user.getShopId(), product);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("prod", product);
        return PathResolver.getPath(ShopFrontPage.SELLING_PROD);
    }

    /**
     * 查找商品所有的SKU
     *
     * @param request
     * @param response
     * @param prodId
     * @param name
     * @param curPageNO
     * @return
     */
    @RequestMapping(value = "/s/loadSkuListPage")
    public String loadSkuListPage(HttpServletRequest request, HttpServletResponse response, @RequestParam Long prodId, String name, String curPageNO) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        ProductDetail detail = productService.getProdDetail(prodId);
        if (!shopId.equals(detail.getShopId())) {
            log.error("shopId={},prodShopId={}", shopId, detail.getShopId());
            throw new BusinessException("shopId!=prodShopId");
        }
        PageSupport<Sku> ps = skuService.loadSkuListPage(curPageNO, prodId, name);
        PageSupportHelper.savePage(request, ps, "Sku");
        request.setAttribute("name", name);
        request.setAttribute("prodId", prodId);
        request.setAttribute("prodName", detail.getName());
        return PathResolver.getPath(ShopFrontPage.LOAD_SKU_LIST_PAGE);
    }

    @RequestMapping(value = "/s/loadStockLog")
    public String loadStockLog(HttpServletRequest request, HttpServletResponse response, @RequestParam Long prodId, @RequestParam Long skuId, String curPageNO) {
        PageSupport<StockLog> ps = stockLogService.loadStockLog(prodId, skuId, curPageNO);
        PageSupportHelper.savePage(request, ps, "StockLog");
        request.setAttribute("skuId", skuId);
        return PathResolver.getPath(ShopFrontPage.LOAD_STOCK_LOG_PAGE);
    }

    /**
     * 仓库中的商品.
     *
     * @param request
     * @param response
     * @param curPageNO
     * @param product
     * @return the string
     */
    @RequestMapping(value = "/s/prodInStoreHouse")
    @Authorize(authorityTypes = AuthorityType.INSTORE_PROD)
    public String prodInStoreHouse(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        PageSupport<Product> ps = productService.getProdInStoreHouse(curPageNO, shopId, product);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("prod", product);
        return PathResolver.getPath(ShopFrontPage.PROD_INSTOREHOUSE);
    }

    /**
     * 违规下线商品.
     *
     * @param request
     * @param response
     * @param curPageNO
     * @param product
     * @return the string
     */
    @RequestMapping(value = "/s/violationProd")
    @Authorize(authorityTypes = AuthorityType.INSTORE_PROD)
    public String violationProd(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        PageSupport<Product> ps = productService.getViolationProd(curPageNO, shopId, product);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("prod", product);
        return PathResolver.getPath(ShopFrontPage.VIOLATION_PROD);
    }

    /**
     * 等待审核的商品.
     *
     * @param request
     * @param response
     * @param curPageNO
     * @param product
     * @return the string
     */
    @RequestMapping(value = "/s/auditingProd")
    @Authorize(authorityTypes = AuthorityType.INSTORE_PROD)
    public String auditingProd(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        PageSupport<Product> ps = productService.getAuditingProd(curPageNO, shopId, product);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("prod", product);
        return PathResolver.getPath(ShopFrontPage.AUDITING_PROD);
    }

    /**
     * 审核失败的商品.
     *
     * @param request
     * @param response
     * @param curPageNO
     * @param product
     * @return the string
     */
    @RequestMapping(value = "/s/auditFailProd")
    @Authorize(authorityTypes = AuthorityType.INSTORE_PROD)
    public String auditFailProd(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        PageSupport<Product> ps = productService.getAuditFailProd(curPageNO, shopId, product);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("prod", product);
        return PathResolver.getPath(ShopFrontPage.AUDIT_FAIL_PROD);
    }

    /**
     * 回收站的商品.
     *
     * @param request
     * @param response
     * @param curPageNO
     * @param product
     * @return the string
     */
    @RequestMapping(value = "/s/prodInDustbin")
    @Authorize(authorityTypes = AuthorityType.INDUSBIN_PROD)
    public String prodInDustbin(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        PageSupport<Product> ps = productService.getProdInDustbin(curPageNO, shopId, product);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("prod", product);
        return PathResolver.getPath(ShopFrontPage.PROD_IN_DUSTBIN);
    }

    /**
     * 更改商品状态.
     *
     * @param request   the request
     * @param response  the response
     * @param productId the product id
     * @param status    the status
     * @return the integer
     */
    @RequestMapping(value = "/s/updatestatus/{productId}/{status}", method = RequestMethod.GET)
    @Authorize(authorityTypes = AuthorityType.SELLING_PROD)
    public @ResponseBody
    Integer updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long productId,
                         @PathVariable Integer status) {
        Product product = productService.getProductById(productId);

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        // 判断商品是否属于该用户所在的商城
        if (product == null || !product.getShopId().equals(shopId)) {
            return -1;
        }

        // 商品在 审核中、审核失败、违规下架， 用户都不能直接改状态
        if (ProductStatusEnum.PROD_AUDIT.value().equals(product.getStatus())) {
            return -1;
        } else if (ProductStatusEnum.PROD_AUDIT_FAIL.value().equals(product.getStatus())) {
            return -1;
        } else if (ProductStatusEnum.PROD_ILLEGAL_OFF.value().equals(product.getStatus())) {
            return -1;
        }

        if (!status.equals(product.getStatus())) {
            productService.updateStatus(status, product.getProdId(), null);
            if (status == 0) {
                productIndexDelProcessor.process(new ProductIdDto(product.getProdId()));
            } else {
                productIndexUpdateProcessor.process(new ProductIdDto(product.getProdId()));
            }
        }
        return status;
    }

    /**
     * 删除商品.
     *
     * @param request
     * @param response
     * @param prodId
     * @return the string
     */
    @RequestMapping(value = "/s/delete/{prodId}")
    @Authorize(authorityTypes = AuthorityType.INDUSBIN_PROD)
    public @ResponseBody
    String productDelete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        String result = productService.delete(shopId, prodId, false);
        if (Constants.SUCCESS.equals(result)) {
            // 删除商品后 ，删除索引数据
            productIndexDelProcessor.process(new ProductIdDto(prodId));
        }
        return result;
    }

    /**
     * 将商品放入回收站.
     *
     * @param request
     * @param response
     * @param prodId
     * @return the string
     */
    @RequestMapping(value = "/s/product/toDustbin/{prodId}")
    @Authorize(authorityTypes = AuthorityType.SELLING_PROD)
    public @ResponseBody
    String toDustbin(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {
        if (AppUtils.isNotBlank(prodId)) {
            Product product = productService.getProductById(prodId);

            SecurityUserDetail user = UserManager.getUser(request);
            Long shopId = user.getShopId();

            if (product.getShopId().equals(shopId)) {
                productService.updateStatus(Constants.STOPUSE, prodId, null);
                return Constants.SUCCESS;
            }
        }
        return Constants.FAIL;
    }

    /**
     * 将回收站商品 还原.
     *
     * @param request
     * @param response
     * @param prodId
     * @return the string
     */
    @RequestMapping(value = "/s/product/restore/{prodId}")
    @Authorize(authorityTypes = AuthorityType.INDUSBIN_PROD)
    public @ResponseBody
    String restore(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {
        if (AppUtils.isNotBlank(prodId)) {
            Product product = productService.getProductById(prodId);

            SecurityUserDetail user = UserManager.getUser(request);
            Long shopId = user.getShopId();

            if (product != null && product.getShopId().equals(shopId)) {
                if (product.getPreStatus() != null) {
                    productService.updateStatus(product.getPreStatus(), prodId, null);
                } else {
                    productService.updateStatus(ProductStatusEnum.PROD_OFFLINE.value(), prodId, null);
                }
                return Constants.SUCCESS;
            }
        }
        return Constants.FAIL;
    }


    /**
     * 商家永久删除商品
     *
     * @param request
     * @param response
     * @param prodId
     * @return the string
     */
    @RequestMapping(value = "/s/product/deleteDustbin/{prodId}")
    @Authorize(authorityTypes = AuthorityType.INDUSBIN_PROD)
    public @ResponseBody
    String deleteDustbin(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {
        if (AppUtils.isNotBlank(prodId)) {
            Product product = productService.getProductById(prodId);

            SecurityUserDetail user = UserManager.getUser(request);
            Long shopId = user.getShopId();

            if (product != null && product.getShopId().equals(shopId)) {
                productService.updateStatus(ProductStatusEnum.PROD_SHOP_DELETE.value(), prodId, null);
                return Constants.SUCCESS;
            }
        }
        return Constants.FAIL;
    }

    /**
     * 释放锁定秒杀商品状态.
     *
     * @param request
     * @param response
     * @param prodId
     * @return the string
     */
    @RequestMapping(value = "/s/product/releaseSeckillStatus/{prodId}")
    public @ResponseBody
    String releaseSeckillStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        Product product = productService.getProductById(prodId);
        if (product == null) {
            return "未找到该商品！";
        }
        if (!product.getShopId().equals(shopId)) {
            return "没有权限操作！";
        }

        // 释放秒杀活动
        SeckillActivity seckillActivity = seckillActivityService.getSeckillActivityByShopIdAndActiveId(shopId, product.getAcitveId());
        if (null != seckillActivity && !SeckillStatusEnum.TRANSORDER.value().equals(seckillActivity.getStatus())
                && !SeckillStatusEnum.PAYMENT_FINISH.value().equals(seckillActivity.getStatus())
                && new Date().getTime() >= seckillActivity.getEndTime().getTime()) {

            productService.releaseSeckill(seckillActivity);
        }
        return Constants.SUCCESS;
    }

    @RequestMapping(value = "/s/product/isProdChange")
    public @ResponseBody
    String isProdChange(HttpServletRequest request, HttpServletResponse response, Long prodId) {

        return skuService.isProdChange(prodId);
    }


    /**
     * 一级类目列表.
     *
     * @param request      the request
     * @param response     the response
     * @param curPageNO    the cur page no
     * @param shopCategory the shop category
     * @return the string
     */
    @RequestMapping(value = "/s/shopCategory")
    @Authorize(authorityTypes = AuthorityType.SHOP_CATEGORY)
    public String shopCategory(HttpServletRequest request, HttpServletResponse response, String curPageNO, ShopCategory shopCategory) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        PageSupport<ShopCategory> ps = shopCategoryService.queryShopCategory(curPageNO, shopCategory, shopId);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("shopCate", shopCategory);
        return PathResolver.getPath(ShopFrontPage.SHOP_CAT);
    }

    /**
     * 加载 一级类目.
     *
     * @param request
     * @param response
     * @param shopCategoryDto
     * @return the string
     */
    @RequestMapping(value = "/s/shopCatConfig")
    @Authorize(authorityTypes = AuthorityType.SHOP_CATEGORY)
    public String loadShopCatById(HttpServletRequest request, HttpServletResponse response, ShopCategoryDto shopCategoryDto) {
        ShopCategory shopCategory = shopCategoryService.getShopCategory(shopCategoryDto.getShopCatId());
        request.setAttribute("shopCat", shopCategory);
        return PathResolver.getPath(ShopFrontPage.SHOP_CAT_CONFIG);
    }

    /**
     * 删除 商品类目.
     *
     * @param request
     * @param response
     * @param shopCategoryDto
     * @return the string
     */
    @RequestMapping(value = "/s/deleteShopCat")
    @Authorize(authorityTypes = AuthorityType.SHOP_CATEGORY)
    public @ResponseBody
    String deleteShopCat(HttpServletRequest request, HttpServletResponse response, ShopCategoryDto shopCategoryDto) {

        ShopCategory shopCat = shopCategoryService.getShopCategory(shopCategoryDto.getShopCatId());

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        if (AppUtils.isBlank(shopId) || !shopId.equals(shopCat.getShopId())) {
            return Constants.USER_STATUS_DISABLE;
        }

        boolean flag = productService.checkShopCategoryUse(shopCategoryDto.getShopCatId());
        if (flag) {
            return Constants.LIMIT;
        }


        List<ShopCategory> nextCatList = shopCategoryService.queryByParentId(shopCategoryDto.getShopCatId());
        if (AppUtils.isNotBlank(nextCatList)) {
            return Constants.FAIL;
        } else {
            shopCategoryService.deleteShopCategory(shopCategoryDto.getShopCatId());
            return Constants.SUCCESS;
        }
    }

    /**
     * 保存 商品类目.
     *
     * @param request
     * @param response
     * @param shopCat
     * @return the string
     */
    @RequestMapping("/s/saveShopCate")
    @Authorize(authorityTypes = AuthorityType.SHOP_CATEGORY)
    public String saveShopCate(HttpServletRequest request, HttpServletResponse response, ShopCategory shopCat) {
        SecurityUserDetail user = UserManager.getUser(request);
        shopCategoryService.saveShopCategory(shopCat, user.getUsername(), user.getUserId(), user.getShopId());
        return PathResolver.getPath(ShopRedirectPage.SHOPCAT_QUERY);
    }

    /**
     * 修改商家类目状态.
     *
     * @param request
     * @param response
     * @param shopCatId
     * @param status
     * @return the integer
     */
    @RequestMapping(value = "/s/updateShopCatStatus/{shopCatId}/{status}", method = RequestMethod.GET)
    @Authorize(authorityTypes = AuthorityType.SHOP_CATEGORY)
    public @ResponseBody
    Integer updateShopCatStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long shopCatId,
                                @PathVariable Integer status) {
        ShopCategory shopCat = shopCategoryService.getShopCategory(shopCatId);

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        if (AppUtils.isBlank(shopId) || AppUtils.isBlank(shopCat)) {
            return -1;
        }

        if (status.equals(0)) { // 下线操作
            Long count = shopCategoryService.getNextCategoryCount(shopCatId);
            if (count > 0) {
                return -2;
            }
        } else { // 上线操作
            // 如果不是顶级分类
            if (shopCat.getParentId() != null) {
                ShopCategory ParentshopCat = shopCategoryService.getShopCategory(shopCat.getParentId());
                if ((ParentshopCat.getStatus()).equals(0)) { // 且父分类的状态为下线状态
                    return -3;
                }
            }
        }

        if (!status.equals(shopCat.getStatus())) {
            if (Constants.ONLINE.equals(status) || Constants.OFFLINE.equals(status)) {
                shopCat.setStatus(status);
                shopCategoryService.updateShopCategory(shopCat);
            }
        }
        return shopCat.getStatus();
    }

    /**
     * 二级类目 查询.
     *
     * @param request
     * @param response
     * @param curPageNO
     * @param shopCatDto
     * @return the string
     */
    @RequestMapping("/s/nextShopCat/query")
    @Authorize(authorityTypes = AuthorityType.SHOP_CATEGORY)
    public String nextShopCat(HttpServletRequest request, HttpServletResponse response, String curPageNO, ShopCategoryDto shopCatDto) {
        if (AppUtils.isBlank(shopCatDto.getShopCatId())) {
            throw new BusinessException("shop Category id can not be empty");
        }

        PageSupport<ShopCategory> ps = shopCategoryService.getNextShopCat(curPageNO, shopCatDto);

        PageSupportHelper.savePage(request, ps);
        request.setAttribute("shopCat", shopCatDto);

        return PathResolver.getPath(ShopFrontPage.NEXT_SHOPCAT);
    }

    /**
     * 前往 二级商品类目 有 nextCatId.
     *
     * @param request
     * @param response
     * @param shopCategoryDto
     * @return the string
     */
    @RequestMapping(value = "/s/nextCat/load")
    public String nextCatConfig(HttpServletRequest request, HttpServletResponse response, ShopCategoryDto shopCategoryDto) {
        ShopCategory shopcat = shopCategoryService.getShopCategory(shopCategoryDto.getNextCatId());
        request.setAttribute("nextShopCat", shopcat);
        request.setAttribute("shopCatId", shopCategoryDto.getShopCatId());
        return PathResolver.getPath(ShopFrontPage.NEXT_SHOPCAT_CONFIG);
    }

    /**
     * 保存 二级 商品类目.
     *
     * @param request
     * @param response
     * @param shopCat
     * @return the string
     */
    @RequestMapping(value = "/s/nextCat/save")
    public String saveNextCat(HttpServletRequest request, HttpServletResponse response, ShopCategory shopCat) {
        SecurityUserDetail user = UserManager.getUser(request);

        shopCategoryService.saveShopCategory(shopCat, user.getUsername(), user.getUserId(), user.getShopId());
        saveMessage(request, ResourceBundleHelper.getSucessfulString());
        return PathResolver.getPath(ShopRedirectPage.NEXTSHOPCAT_QUERY) + "?shopCatId=" + shopCat.getParentId();
    }

    /**
     * 新模板 保存 二级 商品类目.
     *
     * @param request
     * @param response
     * @param shopCat
     * @return the string
     */
    @RequestMapping(value = "/s/nextCat/newSave")
    public String newSave(HttpServletRequest request, HttpServletResponse response, ShopCategory shopCat) {
        SecurityUserDetail user = UserManager.getUser(request);
        shopCategoryService.saveShopCategory(shopCat, user.getUsername(), user.getUserId(), user.getShopId());
        saveMessage(request, ResourceBundleHelper.getSucessfulString());
        return PathResolver.getPath(ShopRedirectPage.SHOPCAT_QUERY);
    }

    /**
     * 删除 二级 商品类目.
     *
     * @param request
     * @param response
     * @param shopCategoryDto
     * @return the string
     */
    @RequestMapping(value = "/s/nextCat/delete")
    @ResponseBody
    public String deleteNextCat(HttpServletRequest request, HttpServletResponse response, ShopCategoryDto shopCategoryDto) {
        if (AppUtils.isBlank(shopCategoryDto.getNextCatId())) {
            throw new BusinessException("shop Category id can not be empty");
        }

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        if (AppUtils.isBlank(shopId)) {
            return Constants.USER_STATUS_DISABLE;
        }

        boolean flag = productService.checkShopNextCategoryUse(shopCategoryDto.getNextCatId());
        if (flag) {
            return Constants.LIMIT;
        }

        shopCategoryService.deleteNextCategory(shopCategoryDto.getNextCatId());
        return Constants.SUCCESS;
    }

    /**
     * 新模板 删除 二级 商品类目.
     *
     * @param request
     * @param response
     * @param shopCategoryDto
     * @return the string
     */
    @RequestMapping(value = "/s/newNextCat/delete")
    public String newDeleteNextCat(HttpServletRequest request, HttpServletResponse response, ShopCategoryDto shopCategoryDto) {
        if (AppUtils.isBlank(shopCategoryDto.getShopCatId()) && AppUtils.isBlank(shopCategoryDto.getNextCatId())) {
            throw new BusinessException("shop Category id can not be empty");
        }

        shopCategoryService.deleteNextCategory(shopCategoryDto.getNextCatId());
        return PathResolver.getPath(ShopRedirectPage.SHOPCAT_QUERY);
    }

    /**
     * 三级类目 查询.
     *
     * @param request
     * @param response
     * @param curPageNO
     * @param shopCatDto
     * @return the string
     */
    @RequestMapping("/s/subShopCat/query")
    public String subShopCat(HttpServletRequest request, HttpServletResponse response, String curPageNO, ShopCategoryDto shopCatDto) {
        if (AppUtils.isBlank(shopCatDto.getNextCatId())) {
            throw new BusinessException("shop Category id can not be empty");
        }

        PageSupport<ShopCategory> ps = shopCategoryService.getSubShopCat(curPageNO, shopCatDto);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("shopCat", shopCatDto);

        return PathResolver.getPath(ShopFrontPage.SUB_SHOPCAT);
    }

    /**
     * 前往 三级分类 编辑界面.
     *
     * @param request         the request
     * @param response        the response
     * @param shopCategoryDto the shop category dto
     * @return the string
     */
    @RequestMapping(value = "/s/subShopCat/load")
    public String updateSubShopCat(HttpServletRequest request, HttpServletResponse response, ShopCategoryDto shopCategoryDto) {
        ShopCategory subCat = shopCategoryService.getShopCategory(shopCategoryDto.getSubCatId());
        request.setAttribute("subShopCat", subCat);
        request.setAttribute("nextCatId", shopCategoryDto.getNextCatId());
        request.setAttribute("shopCatId", shopCategoryDto.getShopCatId());
        return PathResolver.getPath(ShopFrontPage.SUB_SHOPCAT_CONFIG);
    }

    /**
     * 保存 三级商品类目.
     *
     * @param request  the request
     * @param response the response
     * @param subCat   the sub cat
     * @return the string
     */
    @RequestMapping(value = "/s/subCat/save")
    public String saveSubCat(HttpServletRequest request, HttpServletResponse response, ShopCategory subCat) {
        SecurityUserDetail user = UserManager.getUser(request);

        shopCategoryService.saveShopCategory(subCat, user.getUsername(), user.getUserId(), user.getShopId());
        ShopCategory shopCat = shopCategoryService.getShopCategory(subCat.getParentId());
        saveMessage(request, ResourceBundleHelper.getSucessfulString());
        return PathResolver.getPath(ShopRedirectPage.SUBSHOPCAT_QUERY) + "?nextCatId=" + subCat.getParentId() + "&shopCatId="
                + shopCat.getParentId();
    }

    /**
     * 新模板 保存 三级商品类目.
     *
     * @param request  the request
     * @param response the response
     * @param subCat   the sub cat
     * @return the string
     */
    @RequestMapping(value = "/s/subCat/newSave")
    public String newSaveSubCat(HttpServletRequest request, HttpServletResponse response, ShopCategory subCat) {
        SecurityUserDetail user = UserManager.getUser(request);
        shopCategoryService.saveShopCategory(subCat, user.getUsername(), user.getUserId(), user.getShopId());
        shopCategoryService.getShopCategory(subCat.getParentId());
        saveMessage(request, ResourceBundleHelper.getSucessfulString());
        return PathResolver.getPath(ShopRedirectPage.SHOPCAT_QUERY);
    }

    /**
     * 删除 三级 商品类目.
     *
     * @param request         the request
     * @param response        the response
     * @param shopCategoryDto the shop category dto
     * @return the string
     */
    @RequestMapping(value = "/s/subCat/delete")
    @ResponseBody
    public String deleteSubCat(HttpServletRequest request, HttpServletResponse response, ShopCategoryDto shopCategoryDto) {
        if (AppUtils.isBlank(shopCategoryDto.getSubCatId())) {
            throw new BusinessException("shop Category id can not be empty");
        }
        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        if (AppUtils.isBlank(shopId)) {
            return Constants.USER_STATUS_DISABLE;
        }

        boolean flag = productService.checkShopSubCategoryUse(shopCategoryDto.getSubCatId());
        if (flag) {
            return Constants.FAIL;
        }

        shopCategoryService.deleteNextCategory(shopCategoryDto.getSubCatId());
        return Constants.SUCCESS;
    }

    /**
     * 新模板 删除 三级 商品类目.
     *
     * @param request         the request
     * @param response        the response
     * @param shopCategoryDto the shop category dto
     * @return the string
     */
    @RequestMapping(value = "/s/newSubCat/delete")
    public String newDeleteSubCat(HttpServletRequest request, HttpServletResponse response, ShopCategoryDto shopCategoryDto) {
        if (AppUtils.isBlank(shopCategoryDto.getShopCatId()) && AppUtils.isBlank(shopCategoryDto.getSubCatId())) {
            throw new BusinessException("shop Category id can not be empty");
        }
        shopCategoryService.deleteNextCategory(shopCategoryDto.getSubCatId());
        return PathResolver.getPath(ShopRedirectPage.SHOPCAT_QUERY);
    }

    /**
     * 类型 下拉框 查询.
     *
     * @param request  the request
     * @param response the response
     * @param typeName the type name
     * @return the prod type
     */
    @RequestMapping(value = "/s/sort/getProdType")
    public String getProdType(HttpServletRequest request, HttpServletResponse response, String typeName) {
        PageSupport<ProdType> ps = prodTypeService.getProdTypeByName(typeName);
        PageSupportHelper.savePage(request, ps);
        return PathResolver.getPath(BackPage.CATEGORY_PROD_TYPE_PAGE);
    }

    /**
     * 参数属性 下拉框 查询.
     *
     * @param request
     * @param response
     * @param valueName
     * @param propId
     * @return the prop name
     */
    @RequestMapping(value = "/s/ProductPropertyValue/getPropValueName")
    public String getPropName(HttpServletRequest request, HttpServletResponse response, String valueName, Long propId) {
        PageSupport<ProductPropertyValue> ps = productPropertyValueService.getProductPropertyValuePage(valueName, propId);
        PageSupportHelper.savePage(request, ps);
        return PathResolver.getPath(BackPage.PROD_PROP_VALUE);
    }

    /**
     * 品牌 下拉框 查询.
     *
     * @param request
     * @param response
     * @param brandName
     * @return the prop name
     */
    @RequestMapping(value = "/s/brand/getBrandName")
    public String LikePropName(HttpServletRequest request, HttpServletResponse response, String brandName, Long categoryId) {
		/*String userName = UserManager.getUserName(request.getSession());
		PageSupport<Brand> ps = brandService.getDataByBrandName(brandName, userName);
		PageSupportHelper.savePage(request, ps);*/

        List<BrandDto> list = brandService.likeBrandName(brandName, categoryId);
        request.setAttribute("bList", list);
        return PathResolver.getPath(ShopFrontPage.BRAND_PAGE);
    }

    /**
     * 品牌申请.
     *
     * @param request   the request
     * @param response  the response
     * @param curPageNO the cur page no
     * @return the string
     */
    @RequestMapping(value = "/s/applyBrand")
    @Authorize(authorityTypes = AuthorityType.APPLY_BRAND)
    public String applyBrand(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
        SecurityUserDetail user = UserManager.getUser(request);
        String userId = user.getUserId();
        PageSupport<Brand> ps = brandService.getDataByPageByUserId(curPageNO, userId);
        PageSupportHelper.savePage(request, ps);
        return PathResolver.getPath(ShopFrontPage.APPLY_BRAND);
    }

    /**
     * Apply brand delete.
     *
     * @param request
     * @param response
     * @param id
     * @return the string
     */
    @RequestMapping(value = "/s/applyBrand/delete/{id}")
    public @ResponseBody
    String applyBrandDelete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
        Brand brand = brandService.getBrand(id);

        if (brandService.hasChildProduct(brand.getBrandId())) {
            return "商品品牌下有产品, 该品牌不能删除！";
        }
        brandService.delete(id);
        return Constants.SUCCESS;
    }

    /**
     * Delete multiple.
     *
     * @param request
     * @param response
     * @param ids
     * @return the string
     */
    @RequestMapping(value = "/s/applyBrand/deleteMultiple")
    public @ResponseBody
    String deleteMultiple(HttpServletRequest request, HttpServletResponse response, String ids) {
        List<Long> idList = JSONUtil.getArray(ids, Long.class);
        if (AppUtils.isNotBlank(idList)) {
            for (Long id : idList) {
                brandService.getBrand(id);

                brandService.delete(id);
            }
        }
        return Constants.SUCCESS;
    }

    /**
     * Apply brandupdate.
     *
     * @param request
     * @param response
     * @param id
     * @return the string
     */
    @RequestMapping(value = "/s/applyBrand/update/{id}")
    @Authorize(authorityTypes = AuthorityType.APPLY_BRAND)
    public String applyBrandupdate(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
        Brand brand = brandService.getBrand(id);

        request.setAttribute("bean", brand);
        return PathResolver.getPath(ShopFrontPage.APPLYBRAND_CONFIGURE);
    }

    /**
     * Apply brand load.
     *
     * @param request
     * @param response
     * @return the string
     */
    @RequestMapping(value = "/s/applyBrand/load")
    @Authorize(authorityTypes = AuthorityType.APPLY_BRAND)
    public String applyBrandLoad(HttpServletRequest request, HttpServletResponse response) {
        return PathResolver.getPath(ShopFrontPage.APPLYBRAND_CONFIGURE);
    }

    /**
     * 申请品牌.
     *
     * @param request
     * @param response
     * @param brand
     * @return the string
     */
    @RequestMapping(value = "/s/applyBrand/save", method = RequestMethod.POST)
    @Authorize(authorityTypes = AuthorityType.APPLY_BRAND)
    public String applyBrandSave(HttpServletRequest request, HttpServletResponse response, Brand brand) {
        Brand originbrand = null;
        String brandPic = null;
        String brandPicMobile = null;
        String brandAuthorize = null;
        String brandBigImg = null;
        SecurityUserDetail user = UserManager.getUser(request);
        // 更新品牌
        if ((brand != null) && (brand.getBrandId() != null)) { // update
            originbrand = brandService.getBrand(brand.getBrandId());
            if (originbrand == null) {
                throw new NotFoundException("Origin Brand is empty");
            }
            originbrand.setUserId(user.getUserId());
            originbrand.setUserName(user.getUsername());
            originbrand.setStatus(BrandStatusEnum.BRAND_AUDIT.value());
            originbrand.setMemo(brand.getMemo());
            originbrand.setCommend(brand.getCommend());
            originbrand.setBrandName(brand.getBrandName());
            originbrand.setApplyTime(brand.getApplyTime());
            originbrand.setShopId(brand.getShopId());
            originbrand.setSeq(brand.getSeq());
            if (brand.getFile() != null && brand.getFile().getSize() > 0) {
                brandPic = attachmentManager.upload(brand.getFile());
                // 保存附件表
                attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), brandPic, brand.getFile(), AttachmentTypeEnum.BRAND);
                originbrand.setBrandPic(brandPic);
            }
            if (brand.getMobileLogoFile() != null && brand.getMobileLogoFile().getSize() > 0) {
				brandPicMobile = attachmentManager.upload(brand.getMobileLogoFile());
                // 保存附件表
                attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), brandPicMobile, brand.getMobileLogoFile(), AttachmentTypeEnum.BRAND);
                originbrand.setBrandPicMobile(brandPicMobile);
            }
            if (brand.getBigImagefile() != null && brand.getBigImagefile().getSize() > 0) {
                brandBigImg = attachmentManager.upload(brand.getBigImagefile());
                // 保存附件表
                attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), brandBigImg, brand.getBigImagefile(), AttachmentTypeEnum.BRAND);
                originbrand.setBigImage(brandBigImg);
            }
            if (brand.getBrandAuthorizefile() != null && brand.getBrandAuthorizefile().getSize() > 0) {
                brandAuthorize = attachmentManager.upload(brand.getBrandAuthorizefile());
                // 保存附件表
                attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), brandAuthorize, brand.getBrandAuthorizefile(), AttachmentTypeEnum.BRAND);
                originbrand.setBrandAuthorize(brandAuthorize);
            }
            brandService.update(originbrand);

        } else {
            if (brand.getFile() != null && brand.getFile().getSize() > 0) {
                brandPic = attachmentManager.upload(brand.getFile());
                brand.setBrandPic(brandPic);
            }
            if (brand.getMobileLogoFile() != null && brand.getMobileLogoFile().getSize() > 0) {
				brandPicMobile = attachmentManager.upload(brand.getMobileLogoFile());
                brand.setBrandPicMobile(brandPicMobile);
            }
            if (brand.getBrandAuthorizefile() != null && brand.getBrandAuthorizefile().getSize() > 0) {
                brandAuthorize = attachmentManager.upload(brand.getBrandAuthorizefile());
                brand.setBrandAuthorize(brandAuthorize);
            }
            if (brand.getBigImagefile() != null && brand.getBigImagefile().getSize() > 0) {
                brandBigImg = attachmentManager.upload(brand.getBigImagefile());
                brand.setBigImage(brandBigImg);
            }
            brand.setUserId(user.getUserId());
            brand.setUserName(user.getUsername());
            brand.setStatus(BrandStatusEnum.BRAND_AUDIT.value());
            brand.setShopId(user.getShopId());
            brand.setApplyTime(new Date());
            brand.setCommend(0);
            brandService.save(brand);
            if (AppUtils.isNotBlank(brandPic)) {
                // 保存附件表
                attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), brandPic, brand.getFile(), AttachmentTypeEnum.BRAND);
            }
            if (AppUtils.isNotBlank(brandPicMobile)) {
                // 保存附件表
                attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), brandPicMobile, brand.getMobileLogoFile(), AttachmentTypeEnum.BRAND);
            }

            if (AppUtils.isNotBlank(brandAuthorize)) {
                // 保存附件表
                attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), brandAuthorize, brand.getBrandAuthorizefile(), AttachmentTypeEnum.BRAND);
            }
            if (AppUtils.isNotBlank(brandBigImg)) {
                // 保存附件表
                attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), brandBigImg, brand.getBigImagefile(), AttachmentTypeEnum.BRAND);
            }
        }
        String result = PathResolver.getPath(ShopRedirectPage.APPLY_BRAND);
        return result;
    }

    /**
     * Update brand status.
     *
     * @param request
     * @param response
     * @param brandId
     * @param status
     * @return the string
     */
    @RequestMapping(value = "/s/brand/updateBrandStatus", method = RequestMethod.POST)
    public @ResponseBody
    String updateBrandStatus(HttpServletRequest request, HttpServletResponse response, Long brandId, Integer status) {
        if (brandId == null || status == null) {
            return Constants.FAIL;
        }
        Brand brand = brandService.getBrand(brandId);
        if (brand == null) {
            return "该品牌不存在";
        }

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        if (!shopId.equals(brand.getShopId())) {
            return "没有操作权限";
        }
        brand.setStatus(status);
        brandService.update(brand);
        return Constants.SUCCESS;
    }

    /**
     * 根据品牌名检查品牌是否已存在
     *
     * @param brandName
     * @return
     */
    @RequestMapping(value = "/s/brand/checkBrandByName")
    public @ResponseBody
    boolean checkBrandByName(HttpServletRequest request, HttpServletResponse response, String brandName, Long brandId) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        if (AppUtils.isNotBlank(brandName)) {
            return brandService.checkBrandByNameByShopId(brandName.trim(), brandId, shopId);
        }
        return false;
    }

    /**
     * 被举报禁售.
     *
     * @param request
     * @param response
     * @param curPageNO
     * @param product
     * @return the string
     */
    @RequestMapping(value = "/admin/reportForbit")
    public String reportForbit(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {

        SecurityUserDetail user = UserManager.getUser(request);
        String userName = user.getUsername();

        PageSupport<Product> ps = productService.getReportForbit(curPageNO, userName, product);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("productName", product.getName());
        return PathResolver.getPath(ShopFrontPage.REPORT_FORBIT);
    }

    /**
     * 应用客户自定义属性.生成Id
     * 将原来的属性定义加入新的定义并返回
     *
     * @param request
     * @param response
     * @param originUserProperties 原来的属性
     * @param customAttribute      客户新加的自定义属性
     * @return the string
     */
    @RequestMapping(value = "/s/applyProperty")
    public @ResponseBody
    String applyProperty(HttpServletRequest request, HttpServletResponse response, String originUserProperties, String customAttribute) {

        SecurityUserDetail user = UserManager.getUser(request);

        List<ResultCustomPropertyDto> resultCustomPropertyDtoList = productPropertyService.applyProperty(originUserProperties, user.getUsername(), customAttribute);
        // 返回结果
        return JSONUtil.getJson(resultCustomPropertyDtoList);
    }

    /**
     * 异步加载 运费模板选项.
     *
     * @param request
     * @param response
     * @return the string
     */
    @RequestMapping(value = "/s/transport/refresh")
    public @ResponseBody
    String refreshTransport(HttpServletRequest request, HttpServletResponse response) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        List<Transport> transportList = transportService.getTransports(shopId);
        List<KeyValueEntity> kvList = new ArrayList<KeyValueEntity>();
        for (Transport transport : transportList) {
            KeyValueEntity kv = new KeyValueEntity();
            kv.setKey(String.valueOf(transport.getId()));
            kv.setValue(transport.getTransName());
            kvList.add(kv);
        }
        return JSONUtil.getJson(kvList);
    }

    /**
     * 商品咨询.
     *
     * @param request
     * @param response
     * @param curPageNO
     * @param productConsult
     * @return the string
     */
    @RequestMapping("/s/prodcons")
    @Authorize(authorityTypes = AuthorityType.PROD_CONS)
    public String prodcons(HttpServletRequest request, HttpServletResponse response, String curPageNO, ProductConsult productConsult) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        PageSupport<ProductConsult> ps = productConsultService.queryProdcons(curPageNO, shopId, productConsult);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("productConsult", productConsult);
        return PathResolver.getPath(ShopFrontPage.PRODCONSULT);
    }

    /**
     * 商品咨询回复.
     *
     * @param request
     * @param response
     * @param consId
     * @return the string
     */
    @RequestMapping(value = "/s/prodcons/load/{consId}", method = RequestMethod.GET)
    public @ResponseBody
    String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long consId) {
        ProductConsult productConsult = productConsultService.getProductConsult(consId);
        if (productConsult == null) {
            throw new NotFoundException("productConsult not found with Id " + consId);
        }
        Product product = productService.getProductById(productConsult.getProdId());
        productConsult.setProdName(product.getName());
        return JSONUtil.getJson(productConsult);
    }

    /**
     * 商品咨询回复保存.
     *
     * @param request
     * @param response
     * @param consId
     * @param answer
     * @return the string
     */
    @RequestMapping(value = "/s/prodcons/update", method = RequestMethod.POST)
    @ResponseBody
    @Authorize(authorityTypes = AuthorityType.PROD_CONS)
    public String update(HttpServletRequest request, HttpServletResponse response, Long consId, String answer) {

        SecurityUserDetail user = UserManager.getUser(request);
        String userName = user.getUsername();

        ProductConsult productConsult = new ProductConsult();
        productConsult.setConsId(consId);
        productConsult.setAnswer(answer);
        productConsultService.updateProductConsult(userName, productConsult);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
        return Constants.SUCCESS;
    }

    /**
     * 参加分销的商品.
     *
     * @param request
     * @param response
     * @param curPageNO
     * @param product
     * @return the string
     */
    @RequestMapping(value = "/s/joinDistProd")
    @Authorize(authorityTypes = AuthorityType.JOIN_DIST_PROD)
    public String joinDistProd(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        PageSupport<Product> ps = productService.getJoinDistProd(curPageNO, shopId, product);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("prod", product);
        return PathResolver.getPath(ShopFrontPage.JOIN_DIST_PROD);
    }


    /**
     * 加载商品类目页面
     *
     * @param request
     * @param response
     * @return the string
     */
    @RequestMapping(value = "/s/loadShopCategory")
    public String loadShopCategory(HttpServletRequest request, HttpServletResponse response) {

        SecurityUserDetail user = UserManager.getUser(request);
        Long shopId = user.getShopId();

        List<ShopCategory> catList = shopCategoryService.getShopCategoryList(shopId);
        List<CategoryTree> catTrees = new ArrayList<CategoryTree>();
        for (ShopCategory cat : catList) {
            CategoryTree tree = new CategoryTree(cat.getId(), cat.getParentId(), cat.getName());
            catTrees.add(tree);
        }
        request.setAttribute("catTrees", JSONUtil.getJson(catTrees));
        return PathResolver.getPath(ShopFrontPage.LOADSHOPALLCATEGORY);
    }

    /**
     * 加载批量修改页面
     *
     * @param request
     * @param response
     * @param selAry   选中的商品id集合
     * @return
     */
    @RequestMapping("/s/batchEdit")
    public String toBatchEdit(HttpServletRequest request, HttpServletResponse response, @RequestParam String selAry) {
        request.setAttribute("prodIds", selAry);
        String result = PathResolver.getPath(ShopFrontPage.BATCHEDIT);
        return result;
    }


    /**
     * 批量修改商品类目
     *
     * @param prodIds         商品id集合
     * @param shopFirstCatId  一级id
     * @param shopSecondCatId 二级 id
     * @param shopThirdCatId  三级id
     * @return
     */
    @ResponseBody
    @RequestMapping("/s/batchEditUpdate")
    public String batchEditUpdate(HttpServletRequest request, HttpServletResponse response, @RequestParam() String prodIds
            , @RequestParam(value = "shopFirstCatId") Long shopFirstCatId, @RequestParam(value = "shopSecondCatId") Long shopSecondCatId, @RequestParam(value = "shopThirdCatId") Long shopThirdCatId) {
        try {
            String[] pids = prodIds.split(",");
            for (String psid : pids) {
                Product product = productService.getProductById(Long.parseLong(psid));
                if (shopFirstCatId != -1) {
                    product.setShopFirstCatId(shopFirstCatId);
                }
                if (shopSecondCatId != -1) {
                    product.setShopSecondCatId(shopSecondCatId);
                }
                if (shopThirdCatId != -1) {
                    product.setShopThirdCatId(shopThirdCatId);
                }
                productService.updateProd(product);
            }
        } catch (Exception e) {
            return "false";
        }
        return "true";
    }

    /**
     * 加载批量修改库存页面
     *
     * @param selAry 选中的商品id集合
     * @return
     */
    @RequestMapping("/s/toBatchEditStocks")
    public String toBatchEditStocks(HttpServletRequest request, HttpServletResponse response, @RequestParam String selAry) {
        request.setAttribute("skuIds", selAry);
        String result = PathResolver.getPath(ShopFrontPage.STOCKSBATCHEDIT);
        return result;
    }

	/**
	 * 批量修改库存
	 *
	 * @param skuIds 商品skuId集合
	 * @param stock  库存
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/s/batchEditUpdateStocks")
	public String batchEditUpdateStocks(HttpServletRequest request, HttpServletResponse response, @RequestParam() String skuIds, Long stock, Long actualStock) {
		try {
			// 检查用户是否登录
			SecurityUserDetail userDetail = UserManager.getUser(request);
			String shopName = userDetail.getShopUser().getShopName();
			Long prodId = 0L;
			Long beforeStock = 0L;
			Long beforeActualStock = 0L;
			String[] sids = skuIds.split(",");
			for (String sid : sids) {
				Sku originSku = skuService.getSku(Long.parseLong(sid));
				prodId = originSku.getProdId();
				if (!stock.equals(originSku.getStocks())) {
					beforeStock = originSku.getStocks();
					originSku.setStocks(stock);
					originSku.setBeforeStock(originSku.getStocks());
					originSku.setBeforeActualStock(originSku.getActualStocks());
				}
				if (!actualStock.equals(originSku.getActualStocks())) {
					if (originSku.getActualStocks() == null) {
						originSku.setActualStocks(0L);
					}
					beforeActualStock = originSku.getActualStocks();
					originSku.setActualStocks(actualStock);
				}
				skuService.updateSku(originSku);
				//更新库存记录
				StockLog stockLog = new StockLog();
				stockLog.setProdId(originSku.getProdId());
				stockLog.setSkuId(originSku.getSkuId());
				stockLog.setName(originSku.getName());
				if (beforeStock!=0){
					stockLog.setBeforeStock(beforeStock);
				}else{
					stockLog.setBeforeStock(originSku.getStocks());
				}
				if (beforeActualStock != 0) {
					stockLog.setBeforeActualStock(beforeActualStock);
				} else {
					stockLog.setBeforeActualStock(originSku.getActualStocks());
				}
				stockLog.setAfterStock(stock);
				stockLog.setAfterActualStock(actualStock);
				stockLog.setUpdateTime(new Date());
				stockLog.setUpdateRemark("修改库存'"+originSku.getName()+"'，商品销售库存为:'" + stock + "'，实际库存为:'" + actualStock + "'");
				stockLogService.saveStockLog(stockLog);
				// 到货通知事件
				sendArrivalInformProcessor.process(new SendArrivalInform(Long.parseLong(sid),stock,shopName));
			}
			updateProdStock(prodId);
		} catch (Exception e) {
			return "false";
		}
		return "true";
	}

	/**
	 * 修改商品库存
	 *
	 * @param skuId 商品skuId
	 * @param stock 库存
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/s/editUpdateStocks")
	public String editUpdateStocks(HttpServletRequest request, HttpServletResponse response, Long skuId, Long stock, Long actualStocks) {


		// 检查用户是否登录
		SecurityUserDetail userDetail = UserManager.getUser(request);
		String shopName = userDetail.getShopUser().getShopName();
		Long beforeStock = 0L;
		Long beforeActualStocks = 0L;
		Sku originSku = skuService.getSku(skuId);
		int count = 0;
		if (!stock.equals(originSku.getStocks())) {
			beforeStock = originSku.getStocks();
			originSku.setBeforeStock(beforeStock);
			originSku.setStocks(stock);
			count++;
		}

		if (!actualStocks.equals(originSku.getActualStocks())) {
			if (originSku.getActualStocks() == null) {
				originSku.setActualStocks(0L);
			}
			beforeActualStocks = originSku.getActualStocks();
			originSku.setBeforeActualStock(beforeActualStocks);
			originSku.setActualStocks(actualStocks);
			count++;
		}

		if (count > 0) {
			skuService.updateSku(originSku);
		}
		//更新库存记录
		StockLog stockLog = new StockLog();
		stockLog.setProdId(originSku.getProdId());
		stockLog.setSkuId(originSku.getSkuId());
		stockLog.setName(originSku.getName());
		if (beforeStock!=0){
			stockLog.setBeforeStock(beforeStock);
		}else{
			stockLog.setBeforeStock(originSku.getStocks());
		}

		if (beforeActualStocks != 0) {
			stockLog.setBeforeActualStock(beforeActualStocks);
		} else {
			stockLog.setBeforeActualStock(originSku.getActualStocks());
		}
		stockLog.setAfterStock(stock);
		stockLog.setAfterActualStock(actualStocks);
		stockLog.setUpdateTime(new Date());
		stockLog.setUpdateRemark("修改库存'"+originSku.getName()+"'，商品销售库存为:'"+ stock + "'，实际库存为:'" + actualStocks + "'");
		stockLogService.saveStockLog(stockLog);

		// 到货通知事件
		sendArrivalInformProcessor.process(new SendArrivalInform(skuId,stock,shopName));
		updateProdStock(originSku.getProdId());
		return actualStocks + "";
	}

	private void updateProdStock(Long prodId){
		Product product = productService.getProductById(prodId);
		if (AppUtils.isNotBlank(product)){
			Long stocks = 0L;
			Long actualStocks = 0L;
			List<Sku> skuList = skuService.getSkuByProd(product.getProdId());
			if (AppUtils.isNotBlank(skuList)){
				for (Sku sku : skuList) {
					stocks+=sku.getStocks();
					actualStocks+=sku.getActualStocks();
				}
			}
			product.setStocks(Integer.parseInt(stocks+""));
			product.setActualStocks(Integer.parseInt(actualStocks+""));
			productService.updateProd(product);
		}

	}



	@ResponseBody
	@RequestMapping(value = "/s/expireShopCategory")
	@Authorize(authorityTypes = AuthorityType.SHOP_CATEGORY)
	public Boolean  expireShopCategory(HttpServletRequest request, HttpServletResponse response, String curPageNO, ShopCategory shopCategory) {
		try {
			SecurityUserDetail user = UserManager.getUser(request);
			Long shopId = user.getShopId();
			if (AppUtils.isNotBlank(shopId)){
				shopCategoryService.expireShopCategory(shopId);
				shopCategoryService.expireShopCategoryIndex(shopId);
			}
		}catch (Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
