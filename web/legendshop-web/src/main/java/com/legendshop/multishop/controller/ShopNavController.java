/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.shopDecotate.ShopNav;
import com.legendshop.multishop.page.DecorateFrontPage;
import com.legendshop.multishop.page.DecorateRedirectPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ShopNavService;

/**
 * 商家导航控制器
 *
 */
@Controller
@RequestMapping("/s/shopNav")
public class ShopNavController extends BaseController {

	@Autowired
	private ShopNavService shopNavService;

	/**
	 * 获取商家导航列表
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/list")
	@Authorize(authorityTypes = AuthorityType.SHOP_NAV)
	public String list(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<ShopNav> ps = shopNavService.getShopNavPage(curPageNO, shopId);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(DecorateFrontPage.SHOP_NAV_LIST);
	}

	/**
	 * 保存商家导航
	 * @param request
	 * @param response
	 * @param shopNav
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@Authorize(authorityTypes = AuthorityType.SHOP_NAV)
	public String save(HttpServletRequest request, HttpServletResponse response, ShopNav shopNav) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		shopNav.setShopId(shopId);
		shopNavService.saveShopNav(shopNav);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));

		return PathResolver.getPath(DecorateRedirectPage.SHOP_NAV_LIST);
	}

	/**
	 * 删除商家导航
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}")
	@Authorize(authorityTypes = AuthorityType.SHOP_NAV)
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ShopNav shopNav = shopNavService.getShopNav(id);
		shopNavService.deleteShopNav(shopNav);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return PathResolver.getPath(DecorateRedirectPage.SHOP_NAV_LIST);
	}

	/**
	 * 查看商家导航详情
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ShopNav shopNav = shopNavService.getShopNav(id);
		request.setAttribute("bean", shopNav);
		return PathResolver.getPath(DecorateFrontPage.SHOP_NAV_ADD);
	}

	/**
	 * 加载商家导航编辑页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/load")
	@Authorize(authorityTypes = AuthorityType.SHOP_NAV)
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(DecorateFrontPage.SHOP_NAV_ADD);
	}

}
