/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.comparer;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.entity.shopDecotate.ShopLayoutBanner;

/**
 * 店铺品牌布局比较器
 *
 */
public class ShopLayoutBannerComparer implements DataComparer<ShopLayoutBanner, ShopLayoutBanner> {

	/**
	 * 是否需要更新
	 * com.legendshop.core.compare.DataComparer#needUpdate(java.lang.Object,
	 * java.lang.Object, java.lang.Object)
	 */
	@Override
	public boolean needUpdate(ShopLayoutBanner dto, ShopLayoutBanner dbObj, Object obj) {
		return false;
	}

	/**
	 * 是否已存在
	 * 
	 * @see com.legendshop.base.compare.DataComparer#isExist(java.lang.Object,
	 *      java.lang.Object)
	 */
	@Override
	public boolean isExist(ShopLayoutBanner dto, ShopLayoutBanner dbObj) {
		if (dto.getImageFile().equals(dbObj.getImageFile())) {
			return true;
		}
		return false;
	}

	/**
	 * 复制配置
	 * com.legendshop.core.compare.DataComparer#copyProperties(java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public ShopLayoutBanner copyProperties(ShopLayoutBanner dtoj, Object obj) {
		return dtoj;
	}

}
