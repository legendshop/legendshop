/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.multishop.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.page.CommonRedirectPage;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.framework.handler.PluginRepository;
import com.legendshop.framework.plugins.PluginManager;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.multishop.page.ShopFrontPage;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.ShopDetailService;

/**
 * 店铺主页控制器.
 * 
 */
@Controller
public class ShopIndexController extends BaseController {

	private final Logger log = LoggerFactory.getLogger(ShopIndexController.class);	
	
	@Autowired
	private ShopDetailService shopDetailService;
	
	private PluginManager pluginManager;

	@Autowired
	private ProductService productService;
	
	/**
	 * 店铺主页入口方法
	 * @param request
	 * @param response
	 * @param shopId
	 * @return
	 */
	@RequestMapping(value = "/store/{shopId}")
    public String query(HttpServletRequest request, HttpServletResponse response,@PathVariable Long shopId) {
		ShopDetail shopDetail=shopDetailService.getShopDetailById(shopId);
		if(shopDetail==null || shopDetail.getStatus()==0){
			String path = PathResolver.getPath(CommonRedirectPage.HOME_QUERY);
    		return path;
		}
		if(pluginManager==null){
			pluginManager=PluginRepository.getInstance();
		}
		return  PathResolver.getForwardPath("/shop/index/"+shopId);
    }
	
	/**
	 * 店铺商品列表
	 * @param request
	 * @param response
	 * @param shopId
	 * @param curPageNO
	 * @param fireCat
	 * @param secondCat
	 * @param thirdCat
	 * @param keyword
	 * @param order
	 * @return
	 */
	@RequestMapping(value = "/store/prodcts/{shopId}",method = RequestMethod.POST)
	 public String storeProdcts(HttpServletRequest request, HttpServletResponse response, @PathVariable Long shopId,String curPageNO,
			Long fireCat,Long secondCat,Long thirdCat,String keyword,String order) {
			PageSupport<Product> pageSupport=productService.getStoreProdcts(shopId,curPageNO,fireCat,secondCat,thirdCat,keyword,order);
			
			request.setAttribute("fireCat",  fireCat);
			request.setAttribute("secondCat",  secondCat);
			request.setAttribute("thirdCat",  thirdCat);
			request.setAttribute("order", order);
			PageSupportHelper.savePage(request, pageSupport);
	    	String path = PathResolver.getPath(ShopFrontPage.STORE_LIST_PRODS);
	    	return path;
	}
	 
	
	
	
}
