/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.comparer;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.constant.ShopLayoutTypeEnum;
import com.legendshop.model.dto.shopDecotate.ShopLayoutDto;
import com.legendshop.model.entity.shopDecotate.ShopLayout;
import com.legendshop.util.AppUtils;

/**
 * 店铺布局比较器
 *
 */
public class ShopLayoutComparer implements DataComparer<ShopLayoutDto, ShopLayout> {

	/**
	 * 是否需要更新
	 * 
	 * @see com.legendshop.base.compare.DataComparer#needUpdate(java.lang.Object,
	 *      java.lang.Object, java.lang.Object)
	 */
	@Override
	public boolean needUpdate(ShopLayoutDto dto, ShopLayout dbObj, Object obj) {
		dbObj.setLayoutContent(dto.getShopLayoutModule().getLayoutContent());
		dbObj.setDisable(1);
		int num = 0;
		if (ShopLayoutTypeEnum.LAYOUT_2.value().equals(dto.getLayoutType())) {
			if (isChage(dto.getTitle(), dbObj.getTitle())) {
				dbObj.setTitle(dto.getTitle());
				num++;
			}
			if (isChage(dto.getBackColor(), dbObj.getBackColor())) {
				dbObj.setBackColor(dto.getBackColor());
				num++;
			}
			if (isChage(dto.getFontColor(), dbObj.getFontColor())) {
				dbObj.setFontColor(dto.getFontColor());
				num++;
			}
			if (isChage(dto.getFontImg(), dbObj.getFontImg())) {
				dbObj.setFontImg(dto.getFontImg());
				num++;
			}
		}
		/*
		 * 如果是layout3 or layout4 则不需要更新layout 只是更新layout div
		 */
		if (ShopLayoutTypeEnum.LAYOUT_3.value().equals(dto.getLayoutType()) || ShopLayoutTypeEnum.LAYOUT_4.value().equals(dto.getLayoutType())) {
			return true;
		}
		if (isChage(dto.getLayoutModuleType(), dbObj.getLayoutModuleType())) {
			dbObj.setLayoutModuleType(dto.getLayoutModuleType());
			num++;
		}
		if (num > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 是否有改变 *.
	 *
	 * @param a
	 *            the a
	 * @param b
	 *            the b
	 * @return true, if checks if is chage
	 */
	private boolean isChage(Object a, Object b) {
		if (AppUtils.isNotBlank(a) && AppUtils.isNotBlank(b)) {
			if (!a.equals(b)) {
				return true;
			} else {
				return false;
			}
		} else if (AppUtils.isBlank(a) && AppUtils.isBlank(b)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 是否已存在
	 * @see com.legendshop.base.compare.DataComparer#isExist(java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public boolean isExist(ShopLayoutDto dto, ShopLayout dbObj) {
		if (dto.getLayoutId().equals(dbObj.getLayoutId())) {
			return true;
		}
		return false;
	}

	/**
	 * 复制配置
	 * 
	 * @see com.legendshop.base.compare.DataComparer#copyProperties(java.lang.Object,
	 *      java.lang.Object)
	 */
	@Override
	public ShopLayout copyProperties(ShopLayoutDto dtoj, Object obj) {

		ShopLayout shopLayout = new ShopLayout();
		shopLayout.setDisable(1);
		shopLayout.setLayoutId(dtoj.getLayoutId());
		shopLayout.setShopId(dtoj.getShopId());
		shopLayout.setShopDecotateId(dtoj.getShopDecotateId());
		shopLayout.setShopId(dtoj.getShopId());
		shopLayout.setLayoutModuleType(dtoj.getLayoutModuleType());
		shopLayout.setLayoutType(dtoj.getLayoutType());
		shopLayout.setLayoutContent(dtoj.getShopLayoutModule().getLayoutContent());
		shopLayout.setSeq(dtoj.getSeq());

		return shopLayout;
	}

}
