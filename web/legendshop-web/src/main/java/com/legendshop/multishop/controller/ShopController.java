/*
 *
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 *
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.multishop.controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.legendshop.base.util.XssFilterUtil;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.page.CommonFrontPage;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.multishop.page.ShopFrontPage;
import com.legendshop.multishop.page.ShopRedirectPage;
import com.legendshop.processor.ShopInfoCacheUpdateProcessor;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SafeHtml;
import org.springframework.web.multipart.MultipartFile;

/**
 * 卖家中心控制器
 */
@Controller
@RequestMapping("/s")
public class ShopController extends BaseController {

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private AccusationService accusationService;

	@Autowired
	private TransportService transportService;

	@Autowired
	private LocationService locationService;

	@Autowired
	private AttachmentManager attachmentManager;

	@Autowired
	private SubItemService subItemService;

	@Autowired
	private ProdTagService prodTagService;

	@Autowired
	private ProdTagRelService prodTagRelService;

	@Autowired
	private ShopInfoCacheUpdateProcessor shopInfoCacheUpdateProcessor;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Autowired
	private ShopCompanyDetailService shopCompanyDetailService;

	/**
	 * 在ProdTagRel添加商品数据
	 *
	 * @param request
	 * @param response
	 * @param prodIdList
	 * @param tagId
	 * @return
	 */
	@RequestMapping("/addProdToTag/{prodIdList}/{tagId}")
	@ResponseBody
	public String addProdToTag(HttpServletRequest request, HttpServletResponse response,
							   @PathVariable String prodIdList, @PathVariable Long tagId) {
		//拆分成商品id	
		if (AppUtils.isNotBlank(prodIdList)) {
			String[] strArray = null;
			ProdTagRel prodTagRel = null;
			strArray = prodIdList.split(",");
			for (String str : strArray) {
				prodTagRel = new ProdTagRel();
				prodTagRel.setProdId(Long.parseLong(str));
				prodTagRel.setTagId(tagId);
				prodTagRelService.saveProdTagRel(prodTagRel);
			}
			return Constants.SUCCESS;
		}
		return Constants.FAIL;
	}

	/**
	 * 挑选商品弹框
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param product
	 * @return
	 */
	@RequestMapping("/loadProdListByTag/{tagId}")
	public String loadProdListByTag(@PathVariable Integer tagId, HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("tagId", tagId);
		return PathResolver.getPath(ShopFrontPage.LOAD_PROD_LIST_BY_TAG);
	}

	/**
	 * 挑选商品弹框 商品列表
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param product
	 * @return
	 */
	@RequestMapping("/loadProdListForTag")
	public String loadProdListForTag(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		PageSupport<ProdTagRel> ps = prodTagRelService.queryProdTagRel(curPageNO, product, shopId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prod", product);
		return PathResolver.getPath(ShopFrontPage.LOAD_PROD_LIST_FOR_TAG);
	}

	/**
	 * 删除商品标签绑定
	 *
	 * @param request
	 * @param response
	 * @param entity
	 * @return
	 */
	@RequestMapping("/deleteProdTagRel/{prodId}")
	@ResponseBody
	public String deleteProdTagRel(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {
		if (AppUtils.isNotBlank(prodId) && prodTagRelService.deleteProdTagRelByProdId(prodId)) {
			return Constants.SUCCESS;
		}
		return Constants.FAIL;
	}

	/**
	 * 进入绑定商品列表
	 *
	 * @param prodName
	 * @param request
	 * @param response
	 * @param id
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/bindProdManage/{id}")
	public String BindProdManage(String prodName, HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, String curPageNO) {
		ProdTag prodTag = prodTagService.getById(id);
		PageSupport<ProdTagRel> ps = prodTagRelService.queryBindProdManage(curPageNO, id, prodName);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prodTag", prodTag);
		request.setAttribute("prodName", prodName);
		return PathResolver.getPath(ShopFrontPage.BIND_PROD_MANAGE);
	}

	/**
	 * 改变标签的状态
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @param toStatus
	 * @return
	 */
	@RequestMapping("/updateTagStatus/{id}/{toStatus}")
	@ResponseBody
	public String updateTagStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Long toStatus) {
		ProdTag prodTag = prodTagService.getById(id);
		prodTag.setStatus(toStatus);
		if (prodTagService.updateByProdTag(prodTag)) {
			return Constants.SUCCESS;
		}
		return Constants.FAIL;
	}

	/**
	 * 删除标签
	 *
	 * @param request
	 * @param response
	 * @param entity
	 * @return
	 */
	@RequestMapping("/deleteProdTag/{id}")
	@ResponseBody
	public String deleteProdTag(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		if (AppUtils.isNotBlank(id) && prodTagService.deleteProdTagById(id)) {
			//同时要删除商品标签关系记录
			prodTagRelService.deleteProdTagRelByTagId(id);
			return Constants.SUCCESS;
		}
		return Constants.FAIL;
	}

	/**
	 * 保存标签
	 *
	 * @param request
	 * @param response
	 * @param entity
	 * @return
	 */
	@RequestMapping("/saveProdTag")
	public String saveShopCate(HttpServletRequest request, HttpServletResponse response, ProdTag prodTag) {
		SecurityUserDetail user = UserManager.getUser(request);
		prodTagService.saveProdTag(prodTag, user.getUsername(), user.getUserId(), user.getShopId());
		return PathResolver.getPath(ShopRedirectPage.TAG_MANAGE);
	}

	/**
	 * 进入标签编辑页面
	 *
	 * @param request
	 * @param response
	 * @param prodTag
	 * @return
	 */
	@RequestMapping("/updateProdTagUI/{id}")
	public String updateProdTagUI(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		ProdTag prodTag = prodTagService.getById(id);
		request.setAttribute("prodTag", prodTag);
		return PathResolver.getPath(ShopFrontPage.EDIT_TAG);
	}

	/**
	 * 进入添加标签页面
	 *
	 * @param request
	 * @param response
	 * @param prodTag
	 * @return
	 */
	@RequestMapping("/saveProdTagUI")
	public String saveProdTagUI(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(ShopFrontPage.EDIT_TAG);
	}

	/**
	 * 进入标签管理
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param prodTag
	 * @return
	 */
	@RequestMapping(value = "/tagManage")
	@Authorize(authorityTypes = AuthorityType.TAG_MANAGE)
	public String tagManage(HttpServletRequest request, HttpServletResponse response, String curPageNO, ProdTag prodTag) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		PageSupport<ProdTag> ps = prodTagService.getProdTagManage(curPageNO, prodTag, shopId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prodTag", prodTag);
		return PathResolver.getPath(ShopFrontPage.TAG_MANAGE);
	}

	/**
	 * 店铺设置
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/shopSetting")
	@Authorize(authorityTypes = AuthorityType.SHOP_SETTING)
	public String shopSetting(HttpServletRequest request, HttpServletResponse response) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		ShopDetail shopDetail = shopDetailService.getShopDetailByIdNoCache(shopId);
		ShopCompanyDetail shopCompanyDetail = shopCompanyDetailService.getShopCompanyDetailByShopId(shopId);
		if (shopCompanyDetail != null) {
			shopDetail.setCompanyName(shopCompanyDetail.getCompanyName());
		}
		request.setAttribute("shopSetting", shopDetail);
		return PathResolver.getPath(ShopFrontPage.SHOP_SETTING);
	}

	/***
	 * 店铺设置 更新
	 * @param request
	 * @param response
	 * @param shopDetail
	 * @return
	 */
	@RequestMapping(value = "/shopSetting/update")
	@Authorize(authorityTypes = AuthorityType.SHOP_SETTING)
	public @ResponseBody
	String updateShopSetting(HttpServletRequest request, HttpServletResponse response, ShopDetail shopDetail) {
		SecurityUserDetail user = UserManager.getUser(request);
		shopDetail.setUserName(user.getUsername());
		Long shopId = user.getShopId();

		if (!shopId.equals(shopDetail.getShopId())) {
			return "没有操作权限";
		}
		if (AppUtils.isBlank(shopDetail.getSiteName())) {
			return "请输入店铺名称";
		}
		if (AppUtils.isBlank(shopDetail.getContactName())) {
			return "请输入联系人姓名";
		}
		if (AppUtils.isBlank(shopDetail.getContactMobile())) {
			return "请输入联系人手机号码";
		}
		ShopDetail origin = shopDetailService.getShopDetailByIdNoCache(shopId);
		if (AppUtils.isBlank(origin)) {
			return "找不到该店铺信息";
		}
		origin.setSiteName(XssFilterUtil.cleanXSS(shopDetail.getSiteName().trim()));
		origin.setContactName(XssFilterUtil.cleanXSS(shopDetail.getContactName().trim()));
		origin.setContactMobile(shopDetail.getContactMobile().trim());
		origin.setContactTel(shopDetail.getContactTel());
		origin.setContactMail(shopDetail.getContactMail());
		origin.setContactQQ(XssFilterUtil.cleanXSS(shopDetail.getContactQQ()));
		origin.setShopAddr(XssFilterUtil.cleanXSS(shopDetail.getShopAddr()));
		origin.setBankCard(XssFilterUtil.cleanXSS(shopDetail.getBankCard()));
		origin.setPayee(shopDetail.getPayee());
		origin.setCode(shopDetail.getCode());
		origin.setPostAddr(shopDetail.getPostAddr());
		origin.setRecipient(shopDetail.getRecipient());
		origin.setProvinceid(shopDetail.getProvinceid());
		origin.setCityid(shopDetail.getCityid());
		origin.setAreaid(shopDetail.getAreaid());
		origin.setBriefDesc(XssFilterUtil.cleanXSS(shopDetail.getBriefDesc()));
		origin.setDetailDesc(XssFilterUtil.cleanXSS(shopDetail.getDetailDesc()));
		origin.setSwitchInvoice(shopDetail.getSwitchInvoice());

		origin.setReturnShopAddr(XssFilterUtil.cleanXSS(shopDetail.getReturnShopAddr()));
		origin.setReturnProvinceId(shopDetail.getReturnProvinceId());
		origin.setReturnCityId(shopDetail.getReturnCityId());
		origin.setReturnAreaId(shopDetail.getReturnAreaId());

		//营业执照信息
		origin.setBusinessHours(shopDetail.getBusinessHours());
//		origin.setEnterpriseName(shopDetail.getCompanyName());
		origin.setRegisterNumber(shopDetail.getRegisterNumber());
		origin.setRepresentative(shopDetail.getRepresentative());
		origin.setBusinessAddress(shopDetail.getBusinessAddress());
		origin.setRegisterAmount(shopDetail.getRegisterAmount());
		origin.setAvailableTime(shopDetail.getAvailableTime());
		origin.setBusinessRange(shopDetail.getBusinessRange());
		origin.setBusinessImage(shopDetail.getBusinessImage());

		if (shopDetail.getFile() != null && shopDetail.getFile().getSize() > 0) {
			String picUrl = attachmentManager.upload(shopDetail.getFile());
			//保存附件表
			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), picUrl, shopDetail.getFile(), AttachmentTypeEnum.SHOP);
			//删除原图片
			if (AppUtils.isNotBlank(origin.getShopPic())) {
				attachmentManager.delAttachmentByFilePath(origin.getShopPic());
				attachmentManager.deleteTruely(origin.getShopPic());
			}
			origin.setShopPic(picUrl);
		}

		if (shopDetail.getFile2() != null && shopDetail.getFile2().getSize() > 0) {
			String picUrl = attachmentManager.upload(shopDetail.getFile2());
			//保存附件表
			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), picUrl, shopDetail.getFile2(), AttachmentTypeEnum.SHOP);
			//删除原图片
			if (AppUtils.isNotBlank(origin.getShopPic2())) {
				attachmentManager.delAttachmentByFilePath(origin.getShopPic2());
				attachmentManager.deleteTruely(origin.getShopPic2());
			}
			origin.setShopPic2(picUrl);
		}


		if (shopDetail.getBannerfile() != null && shopDetail.getBannerfile().getSize() > 0) {
			String bannerPic = attachmentManager.upload(shopDetail.getBannerfile());
			//保存附件表
			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), bannerPic, shopDetail.getBannerfile(), AttachmentTypeEnum.SHOP);
			//删除原图片
			if (AppUtils.isNotBlank(origin.getBannerPic())) {
				attachmentManager.delAttachmentByFilePath(origin.getBannerPic());
				attachmentManager.deleteTruely(origin.getBannerPic());
			}
			origin.setBannerPic(bannerPic);
		}
		origin.setModifyDate(new Date());
		shopDetailService.update(origin);
		origin.setProvince(shopDetail.getProvince());
		origin.setCity(shopDetail.getCity());
		origin.setArea(shopDetail.getArea());

		//更新商家装修的Cache
		shopInfoCacheUpdateProcessor.process(origin);
		return Constants.SUCCESS;
	}

	@RequestMapping("/uploadImg")
	@ResponseBody
	public void uploadImg(@RequestParam MultipartFile file, ModelMap model, HttpServletRequest request, HttpServletResponse response) {
		SecurityUserDetail user = UserManager.getUser(request);
		System.out.println(user);
		Long shopId = user.getShopId();
		System.out.println(shopId);
		ShopDetail origin = shopDetailService.getShopDetailByIdNoCache(shopId);
		try {
			if (file != null && file.getSize() > 0) {
				String businessImage = attachmentManager.upload(file);
				//保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), businessImage, file, AttachmentTypeEnum.SHOP);
				//删除原图片
				if (AppUtils.isNotBlank(origin.getBusinessImage())) {
					attachmentManager.delAttachmentByFilePath(origin.getBusinessImage());
					attachmentManager.deleteTruely(origin.getBusinessImage());
				}
				origin.setBusinessImage(businessImage);
				model.put("url", businessImage);
				model.put("code", 200);
				model.put("msg", "success");
			} else {
				model.put("msg", "error");
				model.put("code", 0);
			}
		} catch (Exception e) {
			model.put("msg", "error");
			model.put("code", 0);
			e.printStackTrace();
		} finally {
			String jsonResult = JSON.toJSONString(model);
			response.setContentType("text/html;charset=UTF-8");
			renderData(response, jsonResult);
		}
	}

	/**
	 * 通过PrintWriter将响应数据写入response，ajax可以接受到这个数据
	 *
	 * @param response
	 * @param data
	 */
	private void renderData(HttpServletResponse response, String data) {
		PrintWriter printWriter = null;
		try {
			printWriter = response.getWriter();
			printWriter.print(data);
		} catch (IOException ex) {
		} finally {
			if (null != printWriter) {
				printWriter.flush();
				printWriter.close();
			}
		}
	}


	/**
	 * 包邮设置
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/mailingFee/load")
	@Authorize(authorityTypes = AuthorityType.MAILINGFEE_SETTING)
	public String mailingFee(HttpServletRequest request, HttpServletResponse response) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
		request.setAttribute("shopDetail", shopDetail);
		return PathResolver.getPath(ShopFrontPage.MAILINGFEE_SETTING);
	}

	/**
	 * 保存包邮设置
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/mailingFee/save")
	@ResponseBody
	public String saveMailingFee(HttpServletRequest request, HttpServletResponse response, ShopDetail shopDetail) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		ShopDetail origin = shopDetailService.getShopDetailById(shopId);
		if (AppUtils.isBlank(origin)) {
			return "找不到该店铺信息";
		}

		origin.setMailfeeSts(shopDetail.getMailfeeSts());
		origin.setMailfeeType(shopDetail.getMailfeeType());
		origin.setMailfeeCon(shopDetail.getMailfeeCon());

		shopDetailService.update(origin);
		return Constants.SUCCESS;
	}


	/**
	 * 子账号管理
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/subAccount")
	public String subAccount(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(ShopFrontPage.SUB_ACCOUNT);
	}


	/**
	 * 更新主题
	 *
	 * @param request
	 * @param response
	 * @param themeName
	 * @param themeUrl
	 * @return
	 */
	@RequestMapping(value = "/saveTheme")
	public String saveTheme(HttpServletRequest request, HttpServletResponse response, String themeName, String themeUrl) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
		if (!shopDetail.getFrontEndTemplet().equals(themeName)) {
			shopDetail.setFrontEndTemplet(themeName);
			shopDetail.setTheme(themeUrl);
			shopDetailService.update(shopDetail);
		}
		return Constants.SUCCESS;
	}


	/**
	 * 被举报禁售
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/reportForbit")
	@Authorize(authorityTypes = AuthorityType.REPORT_FORBIT)
	public String reportForbit(HttpServletRequest request, HttpServletResponse response, String curPageNO) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		PageSupport<Accusation> ps = accusationService.queryReportForbit(curPageNO, shopId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("reportForbitTotal", ps.getTotal());
		return PathResolver.getPath(ShopFrontPage.REPORT_FORBIT);
	}

	@RequestMapping(value = "/reportForbit/loadOverlay/{id}")
	@Authorize(authorityTypes = AuthorityType.REPORT_FORBIT)
	public String OverlayLoad(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PageSupport<Accusation> ps = accusationService.queryOverlayLoad(id);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(ShopFrontPage.REPORT_FORBIT_OVERLAY);
	}

	/**
	 * 运费模板
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/transport")
	@Authorize(authorityTypes = AuthorityType.TRANSPORT_TEMP)
	public String freightChargesTemp(HttpServletRequest request, HttpServletResponse response, String curPageNO, String transName) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		PageSupport<Transport> ps = transportService.getFreightChargesTemp(curPageNO, shopId, transName);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("transName", transName);
		return PathResolver.getPath(ShopFrontPage.TRANSPORT_LIST);
	}

	@RequestMapping(value = "/transport/load")
	@Authorize(authorityTypes = AuthorityType.TRANSPORT_TEMP)
	public String transportLoad(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(ShopFrontPage.TRANSPORT);
	}

	@RequestMapping(value = "/transport/save")
	@Authorize(authorityTypes = AuthorityType.TRANSPORT_TEMP)
	public String transportSave(HttpServletRequest request, HttpServletResponse response, Transport transport) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		transport.setShopId(shopId);
		transport.setRecDate(new Date());
		transport.setStatus(Constants.ONLINE);
		//transport.setRegionalSales(transport.isRegionalSales());
		transportService.saveTransport(transport);
		return PathResolver.getPath(ShopRedirectPage.TRANSPORT);
	}

	@RequestMapping(value = "/transport/checkName")
	@ResponseBody()
	public boolean checkName(HttpServletRequest request, HttpServletResponse response, String name) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		boolean rs = transportService.checkByName(name, shopId) > 0 ? true : false;
		return rs;
	}


	@RequestMapping(value = "/transport/load/{id}")
	public String transportLoad(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		Transport transport = transportService.getDetailedTransport(shopId, id);
		request.setAttribute("transport", transport);
		return PathResolver.getPath(ShopFrontPage.TRANSPORT);
	}

	@RequestMapping(value = "/transport/delete/{id}")
	@ResponseBody
	@Authorize(authorityTypes = AuthorityType.TRANSPORT_TEMP)
	public String deleteTransport(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		if (shopId != null) {
			//根据id 查找是否有被 商品引用
			boolean isApp = transportService.isAppTransport(id);
			if (isApp) {
				return "运费模板已被商品引用，请先删除商品的引用。";
			} else {
				transportService.deleteTransport(id, shopId);
				return Constants.SUCCESS;
			}
		} else {
			return "删除失败！";
		}
	}

	@RequestMapping(value = "/transport/loadOverlay")
	@Authorize(authorityTypes = AuthorityType.TRANSPORT_TEMP)
	public String loadOverlay(HttpServletRequest request, HttpServletResponse response, String transCityType, Integer transIndex) {
		List<Region> regionList = locationService.getRegionList();
		request.setAttribute("regionList", regionList);
		request.setAttribute("transCityType", transCityType);
		request.setAttribute("transIndex", transIndex);
		return PathResolver.getPath(ShopFrontPage.TRANSPORT_OVERLAY);
	}

	/**
	 * 分销卖出的商品
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param status
	 * @param subItemNum
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@RequestMapping("/shopDistOrder")
	@Authorize(authorityTypes = AuthorityType.ORDER_DIST)
	public String shopDistOrder(HttpServletRequest request, HttpServletResponse response,
								String curPageNO, Integer status, String distUserName, String subItemNum, String startDate, String endDate) {

		SecurityUserDetail user = UserManager.getUser(request);

		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(CommonFrontPage.LOGIN_HINT);
		}

		Long shopId = user.getShopId();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-d");
		Date fromDate = null;
		Date toDate = null;
		try {
			if (AppUtils.isNotBlank(startDate)) {
				fromDate = dateFormat.parse(startDate);
			}
			if (AppUtils.isNotBlank(endDate)) {
				toDate = dateFormat.parse(endDate);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		PageSupport<SubItem> ps = subItemService.querySubItems(curPageNO, shopId, distUserName, status, subItemNum, fromDate, toDate);
		PageSupportHelper.savePage(request, ps);

		int distDay = systemParameterUtil.getDistributionSettlementDay();
		request.setAttribute("distDay", distDay);
		request.setAttribute("distUserName", distUserName);
		request.setAttribute("startDate", fromDate);
		request.setAttribute("endDate", toDate);
		request.setAttribute("curPageNO", curPageNO);
		request.setAttribute("status", status);
		request.setAttribute("subItemNum", subItemNum);
		String result = PathResolver.getPath(ShopFrontPage.SHOPDISTORDER);
		return result;
	}


}
