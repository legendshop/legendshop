/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.entity.Area;
import com.legendshop.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.OrderPayStatusEnum;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.dto.ProdReportDto;
import com.legendshop.model.dto.ReportDto;
import com.legendshop.model.dto.SalesRankDto;
import com.legendshop.model.dto.order.MySubDto;
import com.legendshop.model.dto.order.OrderSearchParamDto;
import com.legendshop.model.entity.City;
import com.legendshop.model.entity.PriceRange;
import com.legendshop.model.entity.Province;
import com.legendshop.model.report.ReportRequest;
import com.legendshop.multishop.page.ReportFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.LocationService;
import com.legendshop.spi.service.OrderService;
import com.legendshop.spi.service.PriceRangeService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.ReportService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;
import com.legendshop.util.JSONUtil;

/**
 * 店铺概况控制器
 *
 */
@Controller
@RequestMapping("/s/shopReport")
public class ShopReportController extends BaseController {

	@Autowired
	private ReportService reportService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private ProductService productService;

	@Autowired
	private LocationService locationService;

	@Autowired
	private PriceRangeService priceRangeService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * 进入店铺概况页面
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/sales")
	@Authorize(authorityTypes = AuthorityType.SHOP_SALES)
	public String sales(HttpServletRequest request, HttpServletResponse response, String curPageNO) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		Date endDate = new Date();
		String dashboardDate = systemParameterUtil.getDashboardDate();// 倒数30天
		Date startDate = DateUtils.getDay(endDate, Integer.parseInt(dashboardDate));
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		// 查询 最近30内下的订单
		OrderSearchParamDto paramDto = new OrderSearchParamDto();
		if (AppUtils.isNotBlank(curPageNO)) {
			paramDto.setCurPageNO(curPageNO);
		}
		int pageSize = 5;// 默认一页显示条数
		paramDto.setPageSize(pageSize);
		paramDto.setShopId(shopId);

		// 添加 查询 时间 条件
		paramDto.setStartDate(startDate);
		paramDto.setEndDate(endDate);

		// 添加 查询 订单状态
		// paramDto.setStatus(OrderStatusEnum.SUCCESS.value());
		paramDto.setIsPayed(OrderPayStatusEnum.PADYED.value());

		// 不去 加载 物流
		paramDto.setLoadDelivery(false);
		PageSupport<MySubDto> ps = orderService.getShopOrderDtos(paramDto);
		PageSupportHelper.savePage(request, ps);

		List<ReportDto> reportDtoList = reportService.queryShopSales(shopId);

		List<Integer> subCounts = new ArrayList<Integer>();
		List<Double> subCash = new ArrayList<Double>();

		Calendar calendar = Calendar.getInstance();
		boolean flag;
		String formatDate = "";
		while (startDate.compareTo(endDate) <= 0) {
			flag = true;
			calendar.setTime(startDate);
			formatDate = dateFormat.format(startDate);
			for (ReportDto reportDto : reportDtoList) {
				if (reportDto.getRecDate().equals(formatDate)) {
					subCounts.add(reportDto.getNumbers());
					subCash.add(reportDto.getTotalCash());

					flag = false;
					break;
				}
			}

			if (flag) {
				subCounts.add(0);
				subCash.add(0d);
			}

			calendar.add(Calendar.DATE, 1);
			startDate = calendar.getTime();
		}

		String subCountsJson = JSONUtil.getJson(subCounts);
		request.setAttribute("subCountsJson", subCountsJson);

		String subCashJson = JSONUtil.getJson(subCash);
		request.setAttribute("subCashJson", subCashJson);

		// 获取 X轴 时间
		List<String> xAxisData = reportService.getXAxis();
		String xAxis = JSONUtil.getJson(xAxisData);
		request.setAttribute("xAxis", xAxis);

		return PathResolver.getPath(ReportFrontPage.REPORT_SALES);
	}

	
	/**
	 * 热卖商品
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/prodAnalysis")
	@Authorize(authorityTypes = AuthorityType.PROD_SALESTREND)
	public String prodAnalysis(HttpServletRequest request, HttpServletResponse response, String curPageNO) {

		List<SalesRankDto> resultList = queryProd(request, response, curPageNO, null);

		List<Integer> prodCounts = new ArrayList<Integer>();
		List<Double> prodCash = new ArrayList<Double>();

		for (SalesRankDto salesRankDto : resultList) {
			prodCounts.add(AppUtils.isBlank(salesRankDto.getBuys()) ? 0 : salesRankDto.getBuys());// 销售数量
			prodCash.add(AppUtils.isBlank(salesRankDto.getTotalCash()) ? 0d : salesRankDto.getTotalCash());// 销售金额
		}
		request.setAttribute("resultSize", resultList.size());
		request.setAttribute("prodCountsJson", JSONUtil.getJson(prodCounts));
		request.setAttribute("prodCashJson", JSONUtil.getJson(prodCash));
		return PathResolver.getPath(ReportFrontPage.REPORT_PROD_ANALYSIS);
	}

	/**
	 * 商品详情 (商品 销售 走势)
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/prodSalesTrend")
	@Authorize(authorityTypes = AuthorityType.PROD_SALESTREND)
	public String prodSalesTrend(HttpServletRequest request, HttpServletResponse response, String curPageNO, String prodName) {
		queryProd(request, response, curPageNO, prodName);
		request.setAttribute("prodName", prodName);
		return PathResolver.getPath(ReportFrontPage.REPORT_PROD_SALESTREND);
	}

	/** 获取 商品 **/
	private List<SalesRankDto> queryProd(HttpServletRequest request, HttpServletResponse response, String curPageNO, String prodName) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Date endDate = new Date();
		String dashboardDate = systemParameterUtil.getDashboardDate();
		Date startDate = DateUtils.getDay(endDate, Integer.parseInt(dashboardDate));
		PageSupport<SalesRankDto> ps = productService.getProductList(curPageNO, shopId, prodName, startDate, endDate);
		PageSupportHelper.savePage(request, ps);
		return ps.getResultList();
	}

	/**
	 * 查看 商品 最近30天的 销售走势
	 * 
	 * @param request
	 * @param response
	 * @param prodId
	 * @return
	 */
	@RequestMapping("/viewTrend")
	public @ResponseBody Map<String, String> viewTrend(HttpServletRequest request, HttpServletResponse response, Long prodId) {
		List<Integer> prodCounts = new ArrayList<Integer>();
		List<Double> prodCash = new ArrayList<Double>();
		List<String> xAxisData = new ArrayList<String>();

		Date now = new Date();
		String dashboardDate = systemParameterUtil.getDashboardDate();
		Date startDate = DateUtils.getDay(now, Integer.parseInt(dashboardDate));
		Date endDate = DateUtils.getDay(now, 0);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		List<ProdReportDto> result = reportService.getProdTrend(shopId, prodId, startDate, endDate);

		Calendar calendar = Calendar.getInstance();
		String dateFormat = "";
		boolean flag;
		while (startDate.compareTo(endDate) <= 0) {
			xAxisData.add(sdf.format(startDate));

			flag = true;
			calendar.setTime(startDate);
			dateFormat = sdf.format(startDate);
			for (ProdReportDto prodReportDto : result) {
				if (prodReportDto.getSubDate().equals(dateFormat)) {
					prodCounts.add(AppUtils.isBlank(prodReportDto.getBuys()) ? 0 : prodReportDto.getBuys());
					prodCash.add(AppUtils.isBlank(prodReportDto.getTotalCash()) ? 0 : prodReportDto.getTotalCash());
					flag = false;
					break;
				}
			}

			if (flag) {
				prodCounts.add(0);
				prodCash.add(0d);
			}

			calendar.add(Calendar.DATE, 1);
			startDate = calendar.getTime();
		}

		Map<String, String> resultMap = new HashMap<String, String>();
		resultMap.put("prodCountsJson", JSONUtil.getJson(prodCounts));
		resultMap.put("prodCashJson", JSONUtil.getJson(prodCash));
		resultMap.put("xAxisJson", JSONUtil.getJson(xAxisData));

		return resultMap;
	}

	/**
	 * 运营报表 from release version TODO 需要检查
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/operatingReport")
	@Authorize(authorityTypes = AuthorityType.OPERATING_REPORT)
	public String OperatingReport(HttpServletRequest request, HttpServletResponse response, String curPageNO, ReportRequest reportRequest) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		reportRequest.setShopId(shopId);

		if (AppUtils.isNotBlank(reportRequest.getSelectedDate()) || AppUtils.isNotBlank(reportRequest.getSelectedYear())) {
			List<ReportDto> result = reportService.getOperatingReport(reportRequest);
			String reportJson = JSONUtil.getJson(result);
			request.setAttribute("reportJson", reportJson);
		}

		// 添加 查询 时间 条件
		OrderSearchParamDto paramDto = new OrderSearchParamDto();
		int pageSize = 5;// 默认一页显示条数
		if (AppUtils.isNotBlank(curPageNO)) {
			paramDto.setCurPageNO(curPageNO);
		}

		paramDto.setPageSize(pageSize);
		paramDto.setShopId(shopId);
		if (AppUtils.isNotBlank(reportRequest.getStatus())) {
			paramDto.setStatus(reportRequest.getStatus());
		} else {
			paramDto.setStatus(OrderStatusEnum.UNPAY.value());
		}

		Date startDate = null, endDate = null;
		if(AppUtils.isBlank(reportRequest.getQueryTerms())){
			reportRequest.setQueryTerms(Constants.CALCULATION_DAYS);
			reportRequest.setSelectedDate(new Date());
		}
		if (Constants.CALCULATION_DAYS.equals(reportRequest.getQueryTerms()) && AppUtils.isNotBlank(reportRequest.getSelectedDate())
				&& AppUtils.isNotBlank(reportRequest.getSelectedDate())) {
			startDate = reportRequest.getSelectedDate();
			
			// 少于下一天  TODO 加一天会导致运营报告按当天统计，会出现两天的数据,更改成拿当天的最后一刻
			/*endDate = DateUtil.getDay(reportRequest.getSelectedDate(), 1);*/
			endDate = DateUtils.getEndMonment(reportRequest.getSelectedDate());
		}

		if (Constants.CALCULATION_MONTH.equals(reportRequest.getQueryTerms())) {
			startDate = DateUtil.getDate(String.valueOf(reportRequest.getSelectedYear()), String.valueOf(reportRequest.getSelectedMonth()), "01");
			endDate = DateUtil.getMonth(startDate, 1);
		}

		if (Constants.CALCULATION_WEEK.equals(reportRequest.getQueryTerms()) && AppUtils.isNotBlank(reportRequest.getSelectedWeek())) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-d");
			String[] week = reportRequest.getSelectedWeek().split("~");

			try {
				startDate = dateFormat.parse(week[0]);
				endDate = dateFormat.parse(week[1]);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			endDate = DateUtils.getDay(endDate, 1); // next day
		}

		if (AppUtils.isNotBlank(startDate)) {
			paramDto.setStartDate(startDate);
		}

		if (AppUtils.isNotBlank(endDate)) {
			paramDto.setEndDate(endDate);
		}

		// 不去 加载 物流
		paramDto.setLoadDelivery(false);
		PageSupport<MySubDto> ps = orderService.getShopOrderDtos(paramDto);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("reportRequest", reportRequest);
		return PathResolver.getPath(ReportFrontPage.REPORT_SHOP_OPERATING);
	}

	/**
	 * 流量统计
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/trafficStatistics")
	@Authorize(authorityTypes = AuthorityType.TRAFFIC_STATISTICS)
	public String trafficStatistics(HttpServletRequest request, HttpServletResponse response, ReportRequest reportRequest) {

		if (AppUtils.isNotBlank(reportRequest.getSelectedDate()) || AppUtils.isNotBlank(reportRequest.getSelectedYear())) {
			SecurityUserDetail user = UserManager.getUser(request);
			reportRequest.setShopId(user.getShopId());
			List<ReportDto> result = reportService.getTrafficStatistics(reportRequest);
			String reportJson = JSONUtil.getJson(result);
			request.setAttribute("reportJson", reportJson);
		}

		request.setAttribute("reportRequest", reportRequest);
		return PathResolver.getPath(ReportFrontPage.REPORT_TRAFFIC_STATISTICS);
	}

	/**
	 * 订单价格销量
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/purchaseAnalysis")
	@Authorize(authorityTypes = AuthorityType.PURCHASE_ANALYSIS)
	public String purchaseAnalysis(HttpServletRequest request, HttpServletResponse response, ReportRequest reportRequest) {

		if (AppUtils.isNotBlank(reportRequest.getSelectedDate()) || AppUtils.isNotBlank(reportRequest.getSelectedYear())) {
			
			SecurityUserDetail user = UserManager.getUser(request);
			Long shopId = user.getShopId();
			
			List<PriceRange> priceRangeList = priceRangeService.queryPriceRange(shopId, 1);// 1是订单价格区间

			if (AppUtils.isNotBlank(priceRangeList)) {
				reportRequest.setShopId(shopId);
				List<ReportDto> result = reportService.getPurchaseAnalysis(priceRangeList, reportRequest);
				String reportJson = JSONUtil.getJson(result);
				request.setAttribute("reportJson", reportJson);

				List<Double> xAxisData = new ArrayList<Double>();
				for (PriceRange priceRange : priceRangeList) {
					xAxisData.add(priceRange.getEndPrice());
				}
				String xAxis = JSONUtil.getJson(xAxisData);
				request.setAttribute("xAxis", xAxis);
			}
		}

		request.setAttribute("reportRequest", reportRequest);
		return PathResolver.getPath(ReportFrontPage.REPORT_PURCHASE_ANALYSIS);
	}

	/**
	 * 购买 时段
	 * 
	 * @param request
	 * @param response
	 * @param reportRequest
	 * @return
	 */
	@RequestMapping("/purchaseTime")
	@Authorize(authorityTypes = AuthorityType.PURCHASE_ANALYSIS)
	public String purchaseTime(HttpServletRequest request, HttpServletResponse response, ReportRequest reportRequest) {

		if (AppUtils.isNotBlank(reportRequest.getSelectedDate()) || AppUtils.isNotBlank(reportRequest.getSelectedYear())) {
			
			SecurityUserDetail user = UserManager.getUser(request);
			Long shopId = user.getShopId();
			
			reportRequest.setShopId(shopId);
			List<ReportDto> result = reportService.getPurchaseTime(reportRequest);
			String reportJson = JSONUtil.getJson(result);
			request.setAttribute("reportJson", reportJson);
		}
		request.setAttribute("reportRequest", reportRequest);
		return PathResolver.getPath(ReportFrontPage.REPORT_PURCHASE_TIME);
	}

	/**
	 * 区域分布
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/regionalDistribution")
	@Authorize(authorityTypes = AuthorityType.OPERATING_REPORT)
	public String regionalDistribution(HttpServletRequest request, HttpServletResponse response, ReportRequest reportRequest) {

		if (AppUtils.isNotBlank(reportRequest.getSelectedDate()) || AppUtils.isNotBlank(reportRequest.getSelectedYear())) {

			SecurityUserDetail user = UserManager.getUser(request);
			Long shopId = user.getShopId();
			
			reportRequest.setShopId(shopId);
			List<Province> provinceList = locationService.getAllProvince();
			List<City> cityList = locationService.getAllCity();
			List<City> areaList = new ArrayList<>();
			for (City city : cityList) {
				if (city.getProvinceid() == 1 || city.getProvinceid() == 2 || city.getProvinceid() == 9) {
					List<Area> area = locationService.getArea(city.getId());
					for(Area a : area) {
						City c = new City();
						c.setId(cityList.size() + 1);
						c.setCityid(a.getAreaid());
						c.setCity(a.getArea());
						c.setProvinceid(a.getCityid());
						areaList.add(c);
					}
				}
			}
			cityList.addAll(areaList);


			//获取每个城市订单的数量
			List<KeyValueEntity> cityOrderQuantity = reportService.getCityOrderQuantity(reportRequest);
			List<KeyValueEntity> cityOrderQuantitySpecial = reportService.getCityOrderQuantitySpecial(reportRequest);

			for (City city : cityList) {
				boolean flag = true;

				for (KeyValueEntity kv : cityOrderQuantity) {
					if (city.getCity().equals(kv.getKey())) {
						city.setSubNumber(kv.getValue());
						flag = false;
						break;
					}
				}
				if (flag) {
					city.setSubNumber("0");
				}

				for (KeyValueEntity kv : cityOrderQuantitySpecial) {
					if (city.getCity().equals(kv.getKey())) {
						city.setSubNumber(kv.getValue());
						flag = false;
						break;
					}
				}
				if (flag) {
					city.setSubNumber("0");
				}
			}
			request.setAttribute("provinceList", JSONUtil.getJson(provinceList));
			request.setAttribute("cityList", JSONUtil.getJson(cityList));
		}
		request.setAttribute("reportRequest", reportRequest);
		String path = PathResolver.getPath(ReportFrontPage.REPORT_REGIONAL_DISTRIBUTION);
		return path;
	}

	/**
	 * 选项，获取周期
	 * 
	 * @param request
	 * @param response
	 * @param year
	 * @param month
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/getWeek")
	public @ResponseBody String getWeek(HttpServletRequest request, HttpServletResponse response, int year, int month) throws Exception {
		return reportService.getWeek(year, month);
	}

	/** 计算 所给 月份 的最后一天 **/
	private static int getLastDay(Integer year, Integer month) {
		if (AppUtils.isNotBlank(year) && AppUtils.isNotBlank(month)) {

			SimpleDateFormat format = new SimpleDateFormat("dd");
			Calendar calendar = Calendar.getInstance();
			calendar.set(year.intValue(), month.intValue() - 1, 1);
			calendar.roll(Calendar.DATE, -1);
			String dateTime = format.format(calendar.getTime());

			return Integer.parseInt(dateTime);
		}
		return 0;
	}
}
