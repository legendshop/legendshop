/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;

import com.legendshop.multishop.exception.CvsMappingStrategyException;
import com.opencsv.CSVReader;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
import com.opencsv.bean.MappingStrategy;

/**
 * @Description
 * @author 关开发
 */
public class CSVReaderUtil {

	/** 默认字符编码 */
	public static final Charset CHARSET_DEFAULT = Charset.forName("UTF-16");
	
	/** 默认分隔符 */
	public static final char SEPARATOR_DEFAULT = '\t';
	
	/** 默认内容界定符 */
	public static final char QUOTECHAR_DEFAULT = '\"';
	
	/** 默认开始行 */
	public static final int START_LINE_DEFAULT = 3;

	public static <T> List<T> readCSVToList(InputStream csvFile, Class<T> beanType) {

		return readCSVToList(csvFile, CHARSET_DEFAULT, SEPARATOR_DEFAULT, QUOTECHAR_DEFAULT, START_LINE_DEFAULT, new ColumnPositionMappingStrategy<T>(),
				beanType);
	}

	public static <T> List<T> readCSVToList(InputStream csvFile, Charset charset, Class<T> beanType) {

		return readCSVToList(csvFile, charset, SEPARATOR_DEFAULT, QUOTECHAR_DEFAULT, START_LINE_DEFAULT, new ColumnPositionMappingStrategy<T>(), beanType);
	}

	public static <T> List<T> readCSVToList(InputStream csvFile, Charset charset, char separator, Class<T> beanType) {

		return readCSVToList(csvFile, charset, separator, QUOTECHAR_DEFAULT, START_LINE_DEFAULT, new ColumnPositionMappingStrategy<T>(), beanType);
	}

	public static <T> List<T> readCSVToList(InputStream csvFile, Charset charset, char separator, char quotechar, Class<T> beanType) {

		return readCSVToList(csvFile, charset, separator, quotechar, START_LINE_DEFAULT, new ColumnPositionMappingStrategy<T>(), beanType);
	}

	public static <T> List<T> readCSVToList(InputStream csvFile, Charset charset, char separator, char quotechar, int startLine, Class<T> beanType) {

		return readCSVToList(csvFile, charset, separator, quotechar, startLine, new ColumnPositionMappingStrategy<T>(), beanType);
	}

	public static <T> List<T> readCSVToList(InputStream csvFile, Charset charset, char separator, char quotechar, int startLine,
			MappingStrategy<T> mappingStrategy, Class<T> beanType) {

		InputStreamReader fileReader = null;
		CSVReader csvReader = null;
		List<T> list = null;
		try {
			fileReader = new InputStreamReader(csvFile, charset);
			csvReader = new CSVReader(fileReader, separator, quotechar, startLine);

			CsvToBean<T> csv = new CsvToBean<T>();

			if (mappingStrategy instanceof ColumnPositionMappingStrategy) {

				ColumnPositionMappingStrategy<T> mapper = (ColumnPositionMappingStrategy<T>) mappingStrategy;
				mapper.setType(beanType);
				list = csv.parse(mapper, csvReader);

			} else if (mappingStrategy instanceof HeaderColumnNameTranslateMappingStrategy) {

				HeaderColumnNameTranslateMappingStrategy<T> mapper = (HeaderColumnNameTranslateMappingStrategy<T>) mappingStrategy;
				mapper.setType(beanType);
				list = csv.parse(mapper, csvReader);

			} else if (mappingStrategy instanceof HeaderColumnNameMappingStrategy) {

				HeaderColumnNameMappingStrategy<T> mapper = (HeaderColumnNameMappingStrategy<T>) mappingStrategy;
				mapper.setType(beanType);
				list = csv.parse(mapper, csvReader);

			} else {
				throw new CvsMappingStrategyException("cvs映射器异常!");
			}
		} finally {
			if (null != csvReader) {
				try {
					csvReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return list;
	}
}
