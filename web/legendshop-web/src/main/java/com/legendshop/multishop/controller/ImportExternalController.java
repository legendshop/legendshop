/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.multishop.page.ShopFrontPage;

/**
 * 外部导入控制器
 *
 */
@Controller
public class ImportExternalController extends BaseController {

	/**
	 * 淘宝导入.
	 *
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping(value = "/s/importTaobao")
	public String reportForbit(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(ShopFrontPage.IMPORT_TAOBAO);
	}
}
