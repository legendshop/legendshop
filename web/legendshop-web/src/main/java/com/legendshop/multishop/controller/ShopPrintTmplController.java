/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.legendshop.base.exception.NotFoundException;
import com.legendshop.base.util.PhotoPathResolver;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.PrintTmpl;
import com.legendshop.multishop.page.BackPage;
import com.legendshop.multishop.page.ShopFrontPage;
import com.legendshop.multishop.page.ShopRedirectPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.PrintTmplService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

/**
 * 打印模板控制器
 * 
 */
@Controller
@RequestMapping("/s/printTmpl")
public class ShopPrintTmplController {
	
	private final Logger logger = LoggerFactory.getLogger(ShopPrintTmplController.class);

	@Autowired
	private PrintTmplService printTmplService;

	@Autowired
	private AttachmentManager attachmentManager;

	/**
	 * 进入打印模板页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/query")
	@Authorize(authorityTypes = AuthorityType.PRINT_TMPL)
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		if(AppUtils.isBlank(curPageNO)){
			curPageNO = "1";
		}
		PageSupport<PrintTmpl> ps = printTmplService.getPrintTmplPageByShopId(shopId, curPageNO);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(ShopFrontPage.PRINTTMPL_LIST_PAGE);
	}

	/**
	 * 保存打印模板
	 * 
	 * @param request
	 * @param response
	 * @param printTmpl
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@Authorize(authorityTypes = AuthorityType.PRINT_TMPL)
	public String save(HttpServletRequest request, HttpServletResponse response, PrintTmpl printTmpl) {
		SecurityUserDetail user = UserManager.getUser(request);
		String pic = null; 
		PrintTmpl origin = null;
		if (AppUtils.isNotBlank(printTmpl.getPrtTmplId())) { // update
			origin = printTmplService.getPrintTmpl(printTmpl.getPrtTmplId());
			if (origin == null) {
				throw new NotFoundException("Origin Brand is empty");
			}
			origin.setPrtTmplTitle(printTmpl.getPrtTmplTitle());
			origin.setPrtDisabled(printTmpl.getPrtDisabled());
			origin.setPrtTmplHeight(printTmpl.getPrtTmplHeight());
			origin.setPrtTmplWidth(printTmpl.getPrtTmplWidth());

			if (printTmpl.getBgimageFile() != null && printTmpl.getBgimageFile().getSize() > 0) {
				pic = attachmentManager.upload(printTmpl.getBgimageFile());
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), pic, printTmpl.getBgimageFile(), AttachmentTypeEnum.OTHER);
				// 删除附件记录
				attachmentManager.delAttachmentByFilePath(origin.getBgimage());
				// 删除图片文件
				attachmentManager.deleteTruely(origin.getBgimage());

				origin.setBgimage(pic);
			}
			printTmplService.updatePrintTmpl(origin);
		} else {
			if (printTmpl.getBgimageFile() != null && printTmpl.getBgimageFile().getSize() > 0) {
				pic = attachmentManager.upload(printTmpl.getBgimageFile());
				// 保存附件表
				attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), pic, printTmpl.getBgimageFile(), AttachmentTypeEnum.OTHER);
			}
			printTmpl.setShopId(user.getShopId());
			printTmpl.setIsSystem(0);
			printTmpl.setBgimage(pic);
			printTmplService.savePrintTmpl(printTmpl);
		}
		return PathResolver.getPath(ShopRedirectPage.PRINTTMPL_LIST_QUERY);
	}

	/**
	 * 保存模板设计
	 * @param request
	 * @param response
	 * @param printTmpl
	 * @return
	 */
	@RequestMapping(value = "/savePrintDesign")
	@Authorize(authorityTypes = AuthorityType.PRINT_TMPL)
	public String savePrintDesign(HttpServletRequest request, HttpServletResponse response, PrintTmpl printTmpl) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(printTmpl.getPrtTmplId())) {
			return "";
		}
		
		PrintTmpl origin = printTmplService.getPrintTmpl(printTmpl.getPrtTmplId());
		
		if (origin == null) {
			logger.error("Can not load origin PrintTmpl by {} ", printTmpl.getPrtTmplId());
			return "";
		}
		
		if(!origin.getShopId().equals(user.getShopId())) {
			logger.error("No right to access  {} PrintTmpl by shopId {} ", printTmpl.getPrtTmplId(), user.getShopId());
			return "";
		}

		origin.setPrtTmplTitle(printTmpl.getPrtTmplTitle());
		origin.setPrtDisabled(printTmpl.getPrtDisabled());
		origin.setPrtTmplHeight(printTmpl.getPrtTmplHeight());
		origin.setPrtTmplWidth(printTmpl.getPrtTmplWidth());
		origin.setPrtTmplData(printTmpl.getPrtTmplData());
		
		printTmplService.updatePrintTmpl(origin,printTmpl);
		
		return PathResolver.getPath(ShopRedirectPage.PRINTTMPL_LIST_QUERY);
	}

	/**
	 * 删除打印模板
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}")
	@Authorize(authorityTypes = AuthorityType.PRINT_TMPL)
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PrintTmpl printTmpl = printTmplService.getPrintTmpl(id);
		printTmplService.deletePrintTmpl(printTmpl);
		return PathResolver.getPath(ShopRedirectPage.PRINTTMPL_LIST_QUERY);
	}

	/**
	 * 查看打印模板详情
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	@Authorize(authorityTypes = AuthorityType.PRINT_TMPL)
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PrintTmpl printTmpl = printTmplService.getPrintTmpl(id);
		request.setAttribute("printTmpl", printTmpl);
		return PathResolver.getPath(ShopFrontPage.PRINTTMPL_EDIT_PAGE);
	}

	
	/**
	 * 设置运单模版
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/loadDesign/{id}")
	public String loadDesign(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PrintTmpl printTmpl = printTmplService.getPrintTmpl(id);
		if (printTmpl == null) {
			return PathResolver.getPath(BackPage.BACK_ERROR_PAGE);
		}
		if (printTmpl.getIsSystem().intValue() == 0) {
			printTmpl.setBgimage(PhotoPathResolver.getInstance().calculateFilePath(printTmpl.getBgimage()).getFilePath());
		}
		request.setAttribute("printTmpl", printTmpl);
		return PathResolver.getPath(ShopFrontPage.PRINTTMPL_DESIGN);
	}

	/**
	 * 下载文件
	 * @param request
	 * @param response
	 * @param imagesfile
	 * @return
	 */
	@RequestMapping(value = "/uploadFile")
	public @ResponseBody String uploadFile(MultipartHttpServletRequest request, HttpServletResponse response, MultipartFile imagesfile) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return Constants.FAIL;
		}
		if (imagesfile != null && imagesfile.getSize() > 0) {
			String pic = attachmentManager.upload(imagesfile);
			// 保存附件表
			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), pic, imagesfile, AttachmentTypeEnum.OTHER);
			return pic;
		} else {
			return Constants.FAIL;
		}
	}

	/**
	 * 加载设置运单模版编辑页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(ShopFrontPage.PRINTTMPL_ADD_PAGE);
	}

}
