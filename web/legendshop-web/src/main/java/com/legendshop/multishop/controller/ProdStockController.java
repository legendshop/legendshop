/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.UpdSkuStockDto;
import com.legendshop.model.dto.stock.ProdArrivalInformDto;
import com.legendshop.model.dto.stock.StockArmDto;
import com.legendshop.multishop.page.ShopFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ProductArrivalInformService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.StockService;

/**
 * 商品库存管理.
 */
@Controller
public class ProdStockController extends BaseController{
	
	@Autowired
	private StockService stockService;
	
	@Autowired
	private ProductArrivalInformService arrivalInformService;
	
	@Autowired
	private ProductService productService;
		
	/**
	 * 商品库存告警.
	 *
	 * @param curPageNO the cur page no
	 * @param productName the product name
	 * @param request the request
	 * @param response the response
	 * @return the string
	 */
	@RequestMapping(value = "/s/stock/warning")
	@Authorize(authorityTypes = AuthorityType.STOCK_WARNING)
	public String stockwarning(String curPageNO,String productName,HttpServletRequest request, HttpServletResponse response) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<StockArmDto> ps = stockService.getStockwarning(shopId,curPageNO,productName);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("searchProName", productName);
		String path = PathResolver.getPath(ShopFrontPage.STOCK_WARNING);
		return path;
	}
	
	/**
	 * 库存编辑页面.
	 *
	 * @param skuId the sku id
	 * @param request the request
	 * @param response the response
	 * @return the string
	 */
	@RequestMapping(value = "/s/stock/edit")
	public String edit(Long skuId,HttpServletRequest request, HttpServletResponse response){
		List<UpdSkuStockDto> lists = stockService.getStockBySkuId(skuId);
		request.setAttribute("list", lists);
		request.setAttribute("skuId", skuId);
		String path = PathResolver.getPath(ShopFrontPage.STOCK_EDIT_PAGE);
		return path;
	}
	
	/**
	 * 库存更新.
	 *
	 * @param jsondata the jsondata
	 * @param skuId the sku id
	 * @param request the request
	 * @param response the response
	 * @return the string
	 */
	@RequestMapping(value="/s/stock/update",method=RequestMethod.POST)
	@ResponseBody
	public String updateStock(HttpServletRequest request, HttpServletResponse response,@RequestParam(required=true) Long skuId,
			@RequestParam(required=true)  Long stocks, @RequestParam(required=true) Long prodId){

		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		
		String result = productService.updateStock(userName,skuId,stocks,prodId);
		return result;
		
	}
	
	
	/**
	 * 查看到货通知的商品.
	 *
	 * @param productName the product name
	 * @param request the request
	 * @param response the response
	 * @param curPageNO the cur page no
	 * @return the string
	 */
	@RequestMapping(value = "/s/prod/arrival")
	@Authorize(authorityTypes = AuthorityType.PRODUCT_ARRIVAL)
	public String prodArrival(String productName,HttpServletRequest request, HttpServletResponse response,String curPageNO) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<ProdArrivalInformDto> ps = arrivalInformService.getProdArrival(shopId,productName,curPageNO);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("searchProName", productName);
		String path = PathResolver.getPath(ShopFrontPage.PROD_ARRIVAL);
		return path;
	}
	
	/**
	 * 进入查看到货通知弹窗，查看对应商品的设置到货通知的用户.
	 *
	 * @param skuId the sku id
	 * @param curPageNO the cur page no
	 * @param request the request
	 * @param response the response
	 * @return the string
	 */
	@RequestMapping(value = "/s/prod/selectArrival")
	public String selectArrival(String skuId,String curPageNO,HttpServletRequest request, HttpServletResponse response){

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<ProdArrivalInformDto> ps = arrivalInformService.getSelectArrival(curPageNO,skuId,shopId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("skuId", skuId);
		String path = PathResolver.getPath(ShopFrontPage.SELECT_ARRIVAL);
		return path;
	}
}


