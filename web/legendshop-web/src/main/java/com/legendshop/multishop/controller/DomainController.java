/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.multishop.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.SecDomainNameSetting;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.multishop.page.ShopFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.spi.service.DomainService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.util.AppUtils;

/**
 * 域名设置Controller
 * 
 */
@Controller
@RequestMapping("/s")
public class DomainController extends BaseController {

	@Autowired
	private DomainService domainService;

	@Autowired
	private ConstTableService constTableService;

	@Autowired
	private ShopDetailService shopDetailService;

	/**
	 * 二级域名设置
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/domainSetting")
	public String domainSetting(HttpServletRequest request, HttpServletResponse response) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		ShopDetail shopDetail = shopDetailService.getShopDetailByUserId(userId);
		request.setAttribute("shopDetail", shopDetail);

		ConstTable constTable = constTableService.getConstTablesBykey("SHOP_SEC_DOMAIN_NAME", "SHOP_SEC_DOMAIN_NAME");

		SecDomainNameSetting secDomainNameSetting = JSONObject.parseObject(constTable.getValue(), SecDomainNameSetting.class);

		request.setAttribute("secDomainNameSetting", secDomainNameSetting);
		request.setAttribute("isAllowModify", secDomainNameSetting.isAllowModify(shopDetail.getSecDomainRegDate()));

		return PathResolver.getPath(ShopFrontPage.DOMAIN_SETTING);
	}

	/**
	 * 查询该二级域名是否已经绑定
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/domain/query/{secDomainName}")
	public @ResponseBody boolean queryDomainName(HttpServletRequest request, HttpServletResponse response, @PathVariable String secDomainName) {
		return domainService.queryDomainNameBinded(secDomainName);
	}

	/**
	 * 绑定二级域名
	 * 
	 * @param request
	 * @param response
	 * @return 结果大于0则表示更新成功，否则更新失败
	 */
	@RequestMapping(value = "/domain/update/{secDomainName}")
	public @ResponseBody String updateDomainName(HttpServletRequest request, HttpServletResponse response, @PathVariable String secDomainName) {

		if (AppUtils.isBlank(secDomainName)) {
			return "对不起,不能注册空域名!";
		}

		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		ShopDetail shopDetail = shopDetailService.getShopDetailByUserId(userId);

		SecDomainNameSetting secDomainNameSetting = this.getSecDomainNameSetting();

		if (AppUtils.isNotBlank(shopDetail.getSecDomainName())) {
			if (!secDomainNameSetting.isAllowModify(shopDetail.getSecDomainRegDate())) {
				return "对不起,您距离重新注册的时间未到!";
			}
		}

		if (!secDomainNameSetting.getIsEnable()) {
			return "对不起,系统没有开启二级域名!";
		}

		if (secDomainNameSetting.isRetain(secDomainName)) {
			return "对不起,该域名是系统保留域名,不能使用!";
		}

		String sizeRange = secDomainNameSetting.getSizeRange();
		String[] sizeRanges = sizeRange.split("-");
		int length = secDomainName.length();
		int min = Integer.parseInt(sizeRanges[0]);
		int max = Integer.parseInt(sizeRanges[1]);

		if (length < min && length > max) {
			return "对不起,域名长度必须要在3-12个字符之间!";
		}

		if (domainService.queryDomainNameBinded(secDomainName)) {
			return "对不起,该域名已被注册!";
		}

		int result = domainService.updateDomainName(userId, secDomainName.toLowerCase(), new Date());
		if (result <= 0) {
			return "对不起,注册失败,请联系管理员!";
		}

		return "恭喜您,注册成功!";
	}

	/**
	 * 获取二级域名全局配置实体
	 * 
	 * @return
	 */
	public SecDomainNameSetting getSecDomainNameSetting() {
		ConstTable constTable = constTableService.getConstTablesBykey("SHOP_SEC_DOMAIN_NAME", "SHOP_SEC_DOMAIN_NAME");

		SecDomainNameSetting secDomainNameSetting = null;

		if (AppUtils.isNotBlank(constTable)) {
			// 获取二级域名全局配置
			secDomainNameSetting = JSONObject.parseObject(constTable.getValue(), SecDomainNameSetting.class);
		}

		return secDomainNameSetting;
	}
}
