/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.excel.EasyExcel;
import com.legendshop.model.constant.ShopOrderBillTypeEnum;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.*;
import com.legendshop.util.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ShopOrderBillStatusEnum;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.multishop.page.ShopFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * 卖家中心 商家结算
 *
 */
@Controller
@RequestMapping("/s/shopOrderBill")
public class ShopOrderBillController extends BaseController {

	private final Logger log = LoggerFactory.getLogger(ShopOrderBillController.class);

	@Autowired
	private ShopOrderBillService shopOrderBillService;

	@Autowired
	private SubService subService;

	@Autowired
	private SubRefundReturnService subRefundReturnService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Autowired
	private SubItemService subItemService;

	@Autowired
	private PresellSubService presellSubService;

	@Autowired
	private AuctionDepositRecService auctionDepositRecService;

	/**
	 * 进入商家结算页面
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param shopOrderBill
	 * @return
	 */
	@RequestMapping("/query")
	@Authorize(authorityTypes = AuthorityType.SHOP_ORDER_BILL)
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, ShopOrderBill shopOrderBill) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<ShopOrderBill> ps = shopOrderBillService.getShopOrderBillPage(shopId, curPageNO, shopOrderBill);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("shopOrderBill", shopOrderBill);

		int shopBillDay = systemParameterUtil.getShopBillDay();
		request.setAttribute("shopBillDay", shopBillDay);
		return PathResolver.getPath(ShopFrontPage.SHOPORDERBILL_LIST);
	}

	/**
	 * 查看商家结算详情
	 * @param id 结算ID
	 * @param subNumber 订单号
	 * @param curPageNO 当前页码
	 * @param type tab类型
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	@Authorize(authorityTypes = AuthorityType.SHOP_ORDER_BILL)
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, String subNumber, String curPageNO, Long type) {

		// 获取结算单详情
		ShopOrderBill shopOrderBill = shopOrderBillService.getShopOrderBill(id);

		// 获取当前登录用户
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		if (shopOrderBill != null && shopOrderBill.getShopId().equals(shopId)) {

			// 结算订单列表
			if (type == null || ShopOrderBillTypeEnum.ORDER.value().equals(type)) {
				PageSupport<Sub> ps = subService.getSubListPage(curPageNO, shopId, subNumber, shopOrderBill);
				PageSupportHelper.savePage(request, ps);

			// 结算退款单列表
			} else if(ShopOrderBillTypeEnum.RETURN_ORDER.value().equals(type)) {
				PageSupport<SubRefundReturn> ps = subRefundReturnService.getSubRefundReturnPage(curPageNO, shopId, subNumber, shopOrderBill);
				PageSupportHelper.savePage(request, ps);

			// 结算分销订单列表
			}else if(ShopOrderBillTypeEnum.DIST_ORDER.value().equals(type)) {

				PageSupport<SubItem> ps = subItemService.queryDistSubItemList(curPageNO, shopId, subNumber, shopOrderBill);
				PageSupportHelper.savePage(request, ps);

			// 结算预售订单列表
			}else if(ShopOrderBillTypeEnum.PRESELL_ORDER.value().equals(type)){

				PageSupport<PresellSub> ps = presellSubService.queryPresellSubList(curPageNO,shopId,subNumber,shopOrderBill);
				PageSupportHelper.savePage(request, ps);

			// 结算拍卖保证金记录
			}else if (ShopOrderBillTypeEnum.AUCTION_ORDER.value().equals(type)){

				PageSupport<AuctionDepositRec> ps = auctionDepositRecService.queryAuctionDepositRecList(curPageNO,shopId,shopOrderBill);
				PageSupportHelper.savePage(request, ps);
			}
			request.setAttribute("shopOrderBill", shopOrderBill);
			request.setAttribute("subNumber", subNumber);
			request.setAttribute("type", type);
			return PathResolver.getPath(ShopFrontPage.SHOPORDERBILL_DETAIL);
		} else {
			log.error("can not load shopOrderBill with id:{},shopId:{}", id, shopId);
			return null;
		}

	}

	/**
	 * 商家确认
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/confirm/{id}")
	@Authorize(authorityTypes = AuthorityType.SHOP_ORDER_BILL)
	public @ResponseBody String confirm(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		ShopOrderBill shopOrderBill = shopOrderBillService.getShopOrderBill(id);
		if (shopOrderBill != null && shopOrderBill.getShopId().equals(shopId)) {
			shopOrderBill.setStatus(ShopOrderBillStatusEnum.SHOP_CONFIRM.value());
			shopOrderBillService.updateShopOrderBill(shopOrderBill);
			return Constants.SUCCESS;
		}
		return Constants.FAIL;
	}



	@RequestMapping(value = "/outputOrder/{id}")
	public  void outputOrder(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, String subNumber, String curPageNO, Long type)throws IOException{
		response.setContentType("application/vnd.ms-excel");
		response.setCharacterEncoding("utf-8");
		String fileName = null;

		// 获取结算单详情
		ShopOrderBill shopOrderBill = shopOrderBillService.getShopOrderBill(id);

		// 获取当前登录用户
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		if (shopOrderBill != null && shopOrderBill.getShopId().equals(shopId)) {
			// 结算订单列表
			if (type == null || ShopOrderBillTypeEnum.ORDER.value().equals(type)) {
				PageSupport<Sub> ps = subService.getSubListPage(curPageNO, shopId, subNumber, shopOrderBill);
				List<Sub> resultList = new ArrayList<>();
				if (AppUtils.isNotBlank(ps)){
					resultList = ps.getResultList();
				}
				fileName = URLEncoder.encode("订单列表", "UTF-8");

				response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");

				EasyExcel.write(response.getOutputStream(), Sub.class).sheet("模板").doWrite(resultList);
				// 结算退款单列表
			} else if (ShopOrderBillTypeEnum.RETURN_ORDER.value().equals(type)) {
				PageSupport<SubRefundReturn> ps = subRefundReturnService.getSubRefundReturnPage(curPageNO, shopId, subNumber, shopOrderBill);
				List<SubRefundReturn> resultList = new ArrayList<>();
				if (AppUtils.isNotBlank(ps)){
					resultList = ps.getResultList();
				}
				fileName = URLEncoder.encode("退款订单列表", "UTF-8");

				response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");

				EasyExcel.write(response.getOutputStream(), SubRefundReturn.class).sheet("模板").doWrite(resultList);
			}
		}
	}

}
