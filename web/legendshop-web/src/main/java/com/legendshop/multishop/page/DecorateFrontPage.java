/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 店铺前台页面定义.
 */
public enum DecorateFrontPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 店铺装修-商家后台 */
	DECORATE("/decorate"),

	/** 装修布局 */
	DECORATE_LAYOUT("/decorateLayout"),

	/** 装修模块 */
	DECORATE_MODULE("/decorateModule"),

	/** 装修模块设置 */
	DECORATE_MODULE_SET("/decorateModuleSet"),

	/**  导航模块 */
	DECORATE_MODULE_NAV("/decorateModuleNav"), 

	/** 幻灯片模块添加页面 */
	DECORATE_MODULE_BANNER_LAYOUT("/decorateModuleBannerLayout"), 

	/** banner模块 */
	DECORATE_MODULE_BANNER("/decorateModuleBanner"), 

	/** 分类模块 */
	DECORATE_MODULE_CATEGORY("/decorateModuleCategory"), 

	/** 热销模块 */
	DECORATE_MODULE_HOT_PROD("/decorateModuleHotProd"), 

	/** 热销模块 */
	DECORATE_MODULE_SHOPINFO("/decorateModuleShopInfo"), 

	/** 初始化加载自定义商品模块 */
	DECORATE_MODULE_PROD("/decorateModuleProd"), 

	/** 自定义商品模块 */
	DECORATE_MODULE_PROD_LAYOUT("/decorateModuleProdLayout"), 

	/** 加载筛选的商品信息 */
	DECORATE_MODULE_HOT_PROD_LOAD("/decorateModuleProdLoad"), 

	/** 内容自定义 */
	DECORATE_MODULE_CUST_CONTENT_LAYOUT("/decorateModuleContentLayout"), 

	/** 加载自定义信息 */
	DECORATE_MODULE_CUST_CONTENT("/decorateModuleContent"), 

	/** 热点装修模块布局 */
	DECORATE_MODULE_HOT_LAYOUT("/decorateModuleHotLayout"),

	/** 热点装修模块加载 */
	DECORATE_MODULE_HOT_LOAD("/decorateModuleHotLoad"),

	/** 装修模块错误 */
	DECORATE_MODULE_ERROR("/decorateModuleError"),

	/** 商家导航列表 */
	SHOP_NAV_LIST("/shopNav"),

	/** 添加商家导航 */
	SHOP_NAV_ADD("/shopNavConfigure"),

	/** 商家店铺轮播图片列表 */
	SHOP_BANNE_LIST("/shopBanner"),

	/** 增加店铺轮播图片 */
	SHOP_BANNER_ADD("/shopBannerConfigure"),

	/** 商家首页 */
	DECORATE_PREVIEW("/decoratePreview"),

	/** 店铺装修主页 */
	STORE_LIST("/storeIndex"),

	/** 选择店铺优惠卷 */
	SELECT_SHOP_COUPON("/selectShopCoupon"),

	;

	/** The value. */
	private final String value;

	private DecorateFrontPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath("decorate", path);
	}

	public String getNativeValue() {
		return value;
	}

}
