/*
 *
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 *
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.multishop.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.LoginUserTypeEnum;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.dto.SalesRankDto;
import com.legendshop.model.entity.LoginHistory;
import com.legendshop.model.entity.Pub;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.ShopOrderBill;
import com.legendshop.model.entity.SystemConfig;
import com.legendshop.multishop.page.ShopFrontPage;
import com.legendshop.multishop.page.ShopRedirectPage;
import com.legendshop.multishop.page.TempletGoatFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.AccusationService;
import com.legendshop.spi.service.LoginHistoryService;
import com.legendshop.spi.service.ProductConsultService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.PubService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.ShopOrderBillService;
import com.legendshop.spi.service.SubService;
import com.legendshop.spi.service.SystemConfigService;
import com.legendshop.util.AppUtils;

/**
 * 卖家中心控制器.
 *
 */
@Controller
public class ShopSellerHomeController extends BaseController {

	private final Logger log = LoggerFactory.getLogger(ShopSellerHomeController.class);

	@Autowired
	private AccusationService accusationService;

	@Autowired
	private ProductService productService;

	@Autowired
	private SubService subService;

	@Autowired
	private ProductConsultService productConsultService;

	@Autowired
	private PubService pubService;

	@Autowired
	private LoginHistoryService loginHistoryService;

	@Autowired
	private SystemConfigService systemConfigService;

	@Autowired
	private ShopOrderBillService shopOrderBillService;

	@Autowired
	private ShopDetailService shopDetailService;

	/**
	 * 卖家中心首页
	 *
	 * @param request
	 * @param response
	 * @param uc
	 * @return
	 */
	@RequestMapping("/sellerHome")
	public String seller(HttpServletRequest request, HttpServletResponse response, String uc) {
		log.debug("Visit sellerHome");
		
		SecurityUserDetail user = UserManager.getUser(request);
		if(null == user){
			return PathResolver.getPath(ShopRedirectPage.LOGIN);
		}
		
		Long shopId = user.getShopId();
		
		if(null == shopId){
			return PathResolver.getPath(ShopRedirectPage.SHOP_AGREEMENT);
		}
		
		if (!UserManager.isShopUser(user)) {//没有商家权限时
			return PathResolver.getPath(ShopRedirectPage.SHOP_AGREEMENT);
		}
		
		//判断该用户是否已开店
		
		ShopDetail shop = null;
		
		//登录账号类型
		String loginUserType = user.getLoginUserType();
		if(LoginUserTypeEnum.EMPLOYEE.value().equals(loginUserType)){//如果是员工
			shop = shopDetailService.getShopDetailById(user.getShopId());
		}else{
			shop = shopDetailService.getShopDetailByUserId(user.getUserId());
		}
		
		if(AppUtils.isBlank(shop) || !ShopStatusEnum.NORMAL.value().equals(shop.getStatus())){ //店铺处于非上线状态时
			return PathResolver.getPath(TempletGoatFrontPage.NON_EXIT_SHOP);
		}
		
		request.setAttribute("REQ_SHOPDETAIL_VIEW", shop);//访问当前的商城
		
		// 咨询个数
		int consultCounts = productConsultService.getConsultCounts(shopId);
		request.setAttribute("consultCounts", consultCounts);

		// 投诉、举报个数
		int accusationCounts = accusationService.getAccusationCounts(shopId);
		request.setAttribute("accusationCounts", accusationCounts);

		// 卖家商品状态数量 提示
		List<KeyValueEntity> prodCounts = productService.getProdCounts(shopId);
		if (AppUtils.isNotBlank(prodCounts)) {
			for (KeyValueEntity keyValueEntity : prodCounts) {
				ProductStatusEnum[] prodStatusArray = ProductStatusEnum.values();
				for (int i = 0; i < prodStatusArray.length; i++) {
					if (prodStatusArray[i].value().equals(Integer.parseInt(keyValueEntity.getKey()))) {
						request.setAttribute(prodStatusArray[i].name(), keyValueEntity.getValue());
					}
				}

			}
		}

		// 交易提示 (最近一个月)
		List<KeyValueEntity> subCounts = subService.getSubCounts(shopId);
		if (AppUtils.isNotBlank(subCounts)) {
			// 最近销售的数量 = (已经付款,但卖家没有发货) status = 2. + (发货，导致实际库存减少) = 3 +
			// (交易成功，购买数增加1) status = 4;
			int tradingInOrderNum = 0;

			for (KeyValueEntity keyValueEntity : subCounts) {
				OrderStatusEnum[] orderStatsArray = OrderStatusEnum.values();

				if (AppUtils.isNotBlank(keyValueEntity.getKey()) && AppUtils.isNotBlank(keyValueEntity.getValue())) {
					Integer key = Integer.parseInt(keyValueEntity.getKey());
					Integer value = Integer.parseInt(keyValueEntity.getValue());

					for (int i = 0; i < orderStatsArray.length; i++) {
						if (orderStatsArray[i].value().equals(key)) {
							request.setAttribute(orderStatsArray[i].name(), value);
						}
					}

					if (OrderStatusEnum.PADYED.value().equals(key) || OrderStatusEnum.CONSIGNMENT.value().equals(key)
							|| OrderStatusEnum.SUCCESS.value().equals(key)) {
						tradingInOrderNum += value;
					}
				}
			}

			request.setAttribute("tradingInOrderNum", tradingInOrderNum);
		}

		// 销售排行
		List<SalesRankDto> salesRankList = productService.getSalesRank(shopId);
		request.setAttribute("salesRankList", salesRankList);

		// 最新公告
		List<Pub> pubList = pubService.queryShopPub();
		request.setAttribute("pubList", pubList);

		// 获取上次登录时间
		LoginHistory loginhist = loginHistoryService.getLastLoginTime(user.getUsername());
		request.setAttribute("loginhist", loginhist);

		// 获取平台QQ号码
		SystemConfig config = systemConfigService.getSystemConfig();
		String qqNumber = config.getQqNumber();
		List<String> qqNumberList = new ArrayList<>();
		if (qqNumber != null && !qqNumber.equals("") && qqNumber.contains(",")) {
			qqNumberList = Arrays.asList(qqNumber.split("[,]"));
		} else if (qqNumber != null && !qqNumber.equals("") && !qqNumber.contains(",")) {
			qqNumberList.add(qqNumber);
		}
		request.setAttribute("qqNumberList", qqNumberList);

		// 平台电话
		request.setAttribute("telephone", config.getAreaCode() +"-"+ config.getTelephone());

		// TODO 统计 退款退货申请数量
		// subRefundReturnService.getLastMonthRefundCountByShopId(shopId);

		// 最近一次商家结算
		ShopOrderBill shopOrderBill = shopOrderBillService.getLastShopOrderBillByShopId(shopId);
		request.setAttribute("shopOrderBill", shopOrderBill);

		request.setAttribute("loginUserType",loginUserType);
		return PathResolver.getPath(ShopFrontPage.SELLER_HOME);
	}


	@RequestMapping("/shopStatusAbnormal")
	public String shopStatusAbnormal(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(TempletGoatFrontPage.NON_EXIT_SHOP);
	}

}
