/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.multishop.page.ShopFrontPage;

/**
 * 店铺其余控制器
 *
 */
@Controller
public class ShopOthersController extends BaseController {

	/**
	 * 广告管理
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/s/advertisement")
	public String advertisement(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(ShopFrontPage.ADVERTISEMENT);
	}

	/**
	 * 图片管理
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/s/photo")
	public String photo(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(ShopFrontPage.PHOTO);
	}
}
