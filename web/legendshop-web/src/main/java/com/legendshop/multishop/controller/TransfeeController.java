/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Transfee;
import com.legendshop.spi.service.TransfeeService;

/**
 * 运输费用控制器
 *
 */
@Controller
@RequestMapping("/s/Transfee")
public class TransfeeController extends BaseController {

	@Autowired
	private TransfeeService transfeeService;

	/**
	 * 获取运输费用列表
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param transfee
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Transfee transfee) {
		PageSupport<Transfee> ps = transfeeService.getTransfeePage(curPageNO, transfee);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("Transfee", transfee);

		return "/Transfee/TransfeeList";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.Transfee_LIST_PAGE);
	}

	/**
	 * 保存运输费用
	 * @param request
	 * @param response
	 * @param transfee
	 * @return
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, Transfee transfee) {
		transfeeService.saveTransfee(transfee);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return "forward:/s/Transfee/query.htm";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// FowardPage.Transfee_LIST_QUERY);
	}

	/**
	 * 删除
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Transfee transfee = transfeeService.getTransfee(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// Transfee.getUserName());
		// if(result!=null){
		// return result;
		// }
		transfeeService.deleteTransfee(transfee);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return "forward:/s/Transfee/query.htm";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// FowardPage.Transfee_LIST_QUERY);
	}

	/**
	 * 查看运输费用详情
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Transfee transfee = transfeeService.getTransfee(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// Transfee.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("transfee", transfee);
		return "/Transfee/Transfee";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.Transfee_EDIT_PAGE);
	}

	/**
	 * 加载运输费用编辑页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return "/Transfee/Transfee";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.Transfee_EDIT_PAGE);
	}

	/**
	 * 更新
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Transfee transfee = transfeeService.getTransfee(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// Transfee.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("Transfee", transfee);
		return "forward:/s/Transfee/query.htm";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.Transfee_EDIT_PAGE);
	}
}
