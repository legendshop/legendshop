/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.multishop.controller;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.dto.TaoBaoProductImportDto;
import com.legendshop.model.dto.TreeNode;
import com.legendshop.model.vo.TaobaoProdImportVo;
import com.legendshop.multishop.page.ShopFrontPage;
import com.legendshop.multishop.util.CSVReaderUtil;
import com.legendshop.spi.service.CategoryManagerService;
import com.legendshop.util.AppUtils;

/**
 * 淘宝商品导入
 */
@Controller
public class TaobaoProdImportController extends BaseController {

	@Autowired
	private CategoryManagerService categoryManagerService;

	/**
	 * 跳转到批量导入淘宝商品界面
	 * 
	 * @param request
	 * @param response
	 * @param categoryId
	 * @return
	 */
	@RequestMapping(value = "/s/taobaoImport/page")
	public String taobaoImportPage(HttpServletRequest request, HttpServletResponse response, Long categoryId) {

		String formId = UUID.randomUUID().toString();
		request.getSession().setAttribute("formId", formId);
		request.setAttribute("categoryId", categoryId);

		// 查询该分类
		if (AppUtils.isNotBlank(categoryId)) {
			List<TreeNode> treeNodes = categoryManagerService.getTreeNodeNavigation(categoryId, ",");
			request.setAttribute("treeNodes", treeNodes);
		}

		return PathResolver.getPath(ShopFrontPage.TAOBAO_PROD_IMPORT);
	}

	/**
	 * 批量导入淘宝商品
	 * 
	 * @param request
	 * @param response
	 * @param categoryId
	 * @return
	 */
	@RequestMapping(value = "/s/taobaoImport/import")
	@ResponseBody
	public String taobaoImport(HttpServletRequest request, HttpServletResponse response, TaobaoProdImportVo taobaoProdImportVo) {
		// String formId = taobaoProdImportVo.getFormId();
		Long categoryId = taobaoProdImportVo.getCategoryId();
		MultipartFile csvFile = taobaoProdImportVo.getCsvFile();
		List<TaoBaoProductImportDto> list = null;
		try {
			list = CSVReaderUtil.readCSVToList(csvFile.getInputStream(), TaoBaoProductImportDto.class);
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (TaoBaoProductImportDto data : list) {
			System.out.println(data);
		}

		return "OK";
	}
}
