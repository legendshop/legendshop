/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.store.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.page.CommonFrontPage;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.store.Store;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.StoreService;
import com.legendshop.store.frontendPage.StoreFrontPage;
import com.legendshop.store.frontendPage.StoreRedirectPage;
import com.legendshop.util.AppUtils;

/**
 * 门店系统.
 *
 */
@Controller
@RequestMapping("/s/store")
public class ShopStoreController {
	
	@Autowired
	private StoreService storeService;
	
    @Autowired
    private PasswordEncoder passwordEncoder;
	 
 	/**
 	 * 查询门店列表
 	 *
 	 * @param request the request
 	 * @param response the response
 	 * @param curPageNO the cur page no
 	 * @return the string
 	 */
    @RequestMapping("/query")
    @Authorize(authorityTypes = AuthorityType.STORE_MANAGE)
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
    	
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<Store> ps =storeService.getStore(curPageNO,shopId);
		PageSupportHelper.savePage(request,ps);
		String result=PathResolver.getPath(StoreFrontPage.SHOP_STORE_LIST); 
    	return result;
    }
    
    /**
     * 保存门店
     *
     * @param request the request
     * @param response the response
     * @param store the store
     * @return the string
     */
    @RequestMapping(value = "/save")
    @Authorize(authorityTypes = AuthorityType.STORE_MANAGE)
    public String save(HttpServletRequest request, HttpServletResponse response, Store store) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
    	if(AppUtils.isNotBlank(store.getId())){
    		Store orgin=storeService.getStore(store.getId());
    		if(orgin==null || !shopId.equals(orgin.getShopId())){
            	request.setAttribute("message", "没有操作权限");
            	String result=PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
            	return result; 
            }
    		orgin.setUserName(store.getUserName());
    		orgin.setName(store.getName());
    		orgin.setContactMobile(store.getContactMobile());
    		orgin.setProvinceid(store.getProvinceid());
    		orgin.setCityid(store.getCityid());
    		orgin.setAreaid(store.getAreaid());
    		orgin.setAddress(store.getAddress());
    		orgin.setProvince(store.getProvince());
    		orgin.setCity(store.getCity());
    		orgin.setArea(store.getArea());
    		orgin.setShopAddr(store.getShopAddr());
    		orgin.setLng(store.getLng());
    		orgin.setLat(store.getLat());
    		orgin.setAddress(store.getAddress());
    		orgin.setTransitRoute(store.getTransitRoute());
    		orgin.setBusinessHours(store.getBusinessHours());
    		storeService.updateStore(orgin);
    	}else{
    		store.setShopId(shopId);
        	store.setAddtime(new Date());
        	store.setIsDisable(true);
        	store.setPasswd(passwordEncoder.encode(store.getPasswd()));
            storeService.saveStore(store);
    	}
        return PathResolver.getPath(StoreRedirectPage.STORE_QUERY);
    }
    
    /**
     * 检查门店名字
     *
     * @param request the request
     * @param response the response
     * @param userName the user name
     * @param no_id the no_id
     * @return true, if check store name
     */
    @RequestMapping(value = "/checkStoreName",method=RequestMethod.POST)
    public @ResponseBody boolean checkStoreName(HttpServletRequest request, HttpServletResponse response, String userName,Long no_id) {
    	if(AppUtils.isNotBlank(no_id)){
    		Store store=storeService.getStore(no_id);
    		if(store!=null){
    			if(store.getUserName().equals(userName)){
    				return true;
    			}
    		}
    	}
        return storeService.checkStoreName(userName);
    }
    
    /**
     * 删除门店
     *
     * @param request the request
     * @param response the response
     * @param id the id
     * @return the string
     */
    @RequestMapping(value = "/delete/{id}")
    @Authorize(authorityTypes = AuthorityType.STORE_MANAGE)
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
    	//主要权限删除

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
    	
        Store store=storeService.getStore(id);
		if(store==null || !shopId.equals(store.getShopId()) ){
        	request.setAttribute("message", "没有操作权限");
        	String result=PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
        	return result; 
        }
		storeService.deleteStore(store);
        return PathResolver.getPath(StoreRedirectPage.STORE_QUERY);
    }
    
    
    /**
     * 门店下线
     *
     * @param request the request
     * @param response the response
     * @param id the id
     * @return the string
     */
    @RequestMapping(value = "/offlineStore/{id}")
    public String offlineStore(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
    	 Store store=storeService.getStore(id);
		if(store==null || !shopId.equals(store.getShopId()) ){
        	request.setAttribute("message", "没有操作权限");
        	String result=PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
        	return result; 
        }
		store.setIsDisable(false);
		storeService.updateStore(store);
        return PathResolver.getPath(StoreRedirectPage.STORE_QUERY);
    }
    
    
    
    /**
     * 门店上线
     *
     * @param request the request
     * @param response the response
     * @param id the id
     * @return the string
     */
    @RequestMapping(value = "/onlineStore/{id}")
    public String onlineStore(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
    	
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
        Store store=storeService.getStore(id);
		if(store==null || !shopId.equals(store.getShopId()) ){
        	request.setAttribute("message", "没有操作权限");
        	String result=PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
        	return result; 
        }
		store.setIsDisable(true);
		storeService.updateStore(store);
        return PathResolver.getPath(StoreRedirectPage.STORE_QUERY);
    }
    
    

    /**
     * 查看门店详情
     *
     * @param request the request
     * @param response the response
     * @param id the id
     * @return the string
     */
    @RequestMapping(value = "/load/{id}")
    @Authorize(authorityTypes = AuthorityType.STORE_MANAGE)
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
    	
    	Store store=storeService.getStore(id);
		if(store==null || !shopId.equals(store.getShopId()) ){
        	request.setAttribute("message", "没有操作权限");
        	String result=PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
        	return result; 
        }
        request.setAttribute("store", store);
        String result=PathResolver.getPath(StoreFrontPage.SHOP_EDIT_STORE);
    	return result; 
    }
    
    /**
     * 加载门店编辑页面.
     *
     * @param request the request
     * @param response the response
     * @return the string
     */
    @RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
    	String result=PathResolver.getPath(StoreFrontPage.SHOP_STORE);
     	return result; 
	}
    
    /**
     * 更新密码
     *
     * @param request the request
     * @param response the response
     * @param id the id
     * @return the string
     */
    @RequestMapping(value = "/updatePasswd/{id}")
   	public String updatePasswd(HttpServletRequest request, HttpServletResponse response, @PathVariable
   		    Long id) {
       	request.setAttribute("storeId", id);
    	String result=PathResolver.getPath(StoreFrontPage.SHOP_STORE_UPDATE_PASSWD);
        return result; 
   	}
    
    /**
     * 改变门店密码
     *
     * @param request the request
     * @param response the response
     * @param id the id
     * @param password the password
     * @return the string
     */
    @RequestMapping(value = "/changeStorePwd/{id}")
   	public  @ResponseBody String  changeStorePwd(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id,
   		    @RequestParam String password) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
    	
    	Store store=storeService.getStore(id);
		if(store==null || !shopId.equals(store.getShopId())){
        	return "没有操作权限";
        }
		store.setPasswd(passwordEncoder.encode(password));
		storeService.updateStore(store);
    	return "OK";
   	}
    

}
