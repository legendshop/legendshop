/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.store.exception;

/**
 * 保存门店商品异常
 * 
 * @Description
 */
public class SaveStoreProdException extends RuntimeException {

	private static final long serialVersionUID = -4777693823142363110L;

	public SaveStoreProdException() {
		// TODO Auto-generated constructor stub
	}

	/** 保存门店商品异常 */
	public SaveStoreProdException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	/** 保存门店商品异常 */
	public SaveStoreProdException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/** 保存门店商品异常 */
	public SaveStoreProdException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/** 保存门店商品异常 */
	public SaveStoreProdException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
