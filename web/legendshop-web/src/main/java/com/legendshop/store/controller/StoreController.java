/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.store.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.dto.store.CheckChainStock;
import com.legendshop.model.dto.store.StoreCitys;
import com.legendshop.spi.service.StoreProdService;
import com.legendshop.store.frontendPage.StoreFrontPage;
import com.legendshop.util.AppUtils;

/**
 * 门店控制器
 */
@Controller
public class StoreController {

	@Autowired
	private StoreProdService storeProdService;

	/**
	 * 是否支持门店，看门店是否有商品.
	 *
	 * @param request
	 * @param response
	 * @param poductId
	 * @param skuId
	 * @return true, if enable store
	 */
	@RequestMapping(value = "/enableStore", method = RequestMethod.GET)
	@ResponseBody
	public boolean enableStore(HttpServletRequest request, HttpServletResponse response, Long poductId, Long skuId) {
		if (skuId == null) {
			skuId = 0l;
		}
		long number = storeProdService.getStoreProdCount(poductId, skuId);
		return number > 0;
	}

	/**
	 * 门店所在城市
	 *
	 * @param request
	 * @param response
	 * @param poductId
	 * @param province
	 * @param city
	 * @param areaid
	 * @return 城市里的门店
	 */
	@RequestMapping(value = "/storeProductCitys", method = RequestMethod.GET)
	@ResponseBody
	public List<StoreCitys> storeProductCitys(HttpServletRequest request, HttpServletResponse response, @RequestParam Long poductId, String province,
			String city, String area) {
		List<StoreCitys> storeCitys = storeProdService.findStoreCitys(poductId, province, city, area);
		return storeCitys;
	}

	/**
	 * 加载按商品查找城市门店页面.
	 *
	 * @param request
	 * @param response
	 * @param poductId
	 * @return the string
	 * @deprecated 用ajax call实现
	 */
	@RequestMapping(value = "/storeProductCitysPage/{poductId}", method = RequestMethod.GET)
	public String storeProductCitysPage(HttpServletRequest request, HttpServletResponse response, @PathVariable Long poductId) {
		request.setAttribute("currProdId", poductId);
		String result = PathResolver.getPath(StoreFrontPage.STORE_PRODUCT_CITYS_PAGE);
		return result;
	}

	/**
	 * 查找商城在城市里的门店.
	 *
	 * @param request
	 * @param response
	 * @param shopId
	 * @param province
	 * @param city
	 * @param areaid
	 * @return the list< store citys>
	 */
	@RequestMapping(value = "/storeShopCitys", method = RequestMethod.POST)
	@ResponseBody
	public List<StoreCitys> storeShopCitys(HttpServletRequest request, HttpServletResponse response, @RequestParam Long shopId, @RequestParam String province,
			@RequestParam String city, @RequestParam String area) {
		List<StoreCitys> storeCitys = storeProdService.findStoreShopCitys(shopId, province, city, area);
		return storeCitys;
	}

	/**
	 * 检查是否连锁
	 *
	 * @param request
	 * @param response
	 * @param chainId
	 * @param product
	 * @return the check chain stock
	 */
	@RequestMapping(value = "/checkChainStock", method = RequestMethod.POST)
	@ResponseBody
	public CheckChainStock checkChainStock(HttpServletRequest request, HttpServletResponse response, @RequestParam Long chainId,
			@RequestParam("product[]") List<String> product) {
		CheckChainStock stock = new CheckChainStock();
		if (AppUtils.isBlank(product)) {
			stock.setState("error");
			return stock;
		}
		List<String> products = new ArrayList<String>();
		for (String string : product) {
			String[] array = string.split(":");
			Long prodId = Long.valueOf(array[0]);
			Long skuId = Long.valueOf(array[1]);
			Integer count = Integer.valueOf(array[2]);
			Integer stocks = storeProdService.getStocks(chainId, prodId, skuId);
			if (stocks == null || stocks - count < 0) {
				String value = prodId + "_" + skuId;
				products.add(value);
			}
		}
		stock.setState("success");
		stock.setProduct(products);
		return stock;
	}

}
