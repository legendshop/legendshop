/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.store.exception;

/**
 * 没有发现门店商品异常
 * @Description 
 * @author 关开发
 */
public class StorePordNotFindException extends RuntimeException {

	private static final long serialVersionUID = -968836909413308086L;

	public StorePordNotFindException() {
		// TODO Auto-generated constructor stub
	}

	/** 没有发现门店商品异常 */
	public StorePordNotFindException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/** 没有发现门店商品异常 */
	public StorePordNotFindException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/** 没有发现门店商品异常 */
	public StorePordNotFindException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/** 没有发现门店商品异常 */
	public StorePordNotFindException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
