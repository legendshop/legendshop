/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.store.controller;

import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.StoreOrderEnum;
import com.legendshop.model.constant.SubHistoryEnum;
import com.legendshop.model.dto.StoreOrderSerachDto;
import com.legendshop.model.dto.StoreProductDto;
import com.legendshop.model.dto.StoreSkuDto;
import com.legendshop.model.dto.order.StoreOrderDto;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.Sku;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubHistory;
import com.legendshop.model.entity.store.Store;
import com.legendshop.model.entity.store.StoreOrder;
import com.legendshop.model.entity.store.StoreProd;
import com.legendshop.model.entity.store.StoreSku;
import com.legendshop.processor.helper.DelayCancelHelper;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.OrderService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.SkuService;
import com.legendshop.spi.service.StoreOrderService;
import com.legendshop.spi.service.StoreProdService;
import com.legendshop.spi.service.StoreService;
import com.legendshop.spi.service.StoreSkuService;
import com.legendshop.spi.service.SubHistoryService;
import com.legendshop.spi.service.SubService;
import com.legendshop.store.frontendPage.StoreFrontPage;
import com.legendshop.store.frontendPage.StoreRedirectPage;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 门店管理控制器.
 *
 */
@Controller
@RequestMapping("/m/store")
public class ShopStoreHomeController {

	private final Logger log = LoggerFactory.getLogger(ShopStoreHomeController.class);

	@Autowired
	private StoreService storeService;

	@Autowired
	private ProductService productService;

	@Autowired
	private SkuService skuService;

	@Autowired
	private StoreProdService storeProdService;

	@Autowired
	private StoreSkuService storeSkuService;

	@Autowired
	private StoreOrderService storeOrderService;

	@Autowired
	private SubService subService;

	@Autowired
	private DelayCancelHelper delayCancelHelper;

	@Autowired
	private OrderService orderService;
	
	@Autowired
	private SubHistoryService subHistoryService;

	/**
	 * 进入门店首页
	 *
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping("/home")
	public String home(HttpServletRequest request, HttpServletResponse response) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String storeId = user.getUserId();
		Long shopId = user.getShopId();

		Store store = storeService.getStore(Long.valueOf(storeId), shopId);

		request.getSession().setAttribute("store", store);
		String result = PathResolver.getPath(StoreRedirectPage.STORE_ORDER_QUERY);
		return result;
	}

	/**
	 * 查看门店订单.
	 *
	 * @param request
	 * @param response
	 * @param paramdto
	 * @return the string
	 */
	@RequestMapping("/order")
	public String order(HttpServletRequest request, HttpServletResponse response, StoreOrderSerachDto paramdto) {

		SecurityUserDetail user = UserManager.getUser(request);
		String storeId = user.getUserId();
		Long shopId = user.getShopId();
		
		PageSupport<StoreOrderDto> ps = storeOrderService.getDeliveryOrderDtos(shopId, Long.valueOf(storeId), paramdto);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("paramdto", paramdto);
		String result = PathResolver.getPath(StoreFrontPage.SHOP_STORE_HOME);
		return result;
	}

	/**
	 * 付款并自提.
	 *
	 * @param request
	 * @param response
	 * @param subNumer
	 * @param actualTotal
	 * @return the string
	 */
	@RequestMapping("/shopStoreTakenLayout")
	public String shopStoreTakenLayout(HttpServletRequest request, HttpServletResponse response, @RequestParam String subNumer, @RequestParam String actualTotal) {
		StoreOrder storeOrder = storeOrderService.getDeliveryOrderBySubNumber(subNumer);
		request.setAttribute("storeOrder", storeOrder);
		request.setAttribute("actualTotal", actualTotal);
		String result = PathResolver.getPath(StoreFrontPage.SHOP_STORE_TAKEN_LAYOUT);
		return result;
	}

	/**
	 * 取消门店订单.
	 *
	 * @param request
	 * @param response
	 * @param subNumer
	 * @return the string
	 */
	@RequestMapping(value = "/shopStoreCancelOrder/{subNumer}", method = RequestMethod.POST)
	@ResponseBody
	public String shopStoreCancelOrder(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumer) {
		Sub sub = subService.getSubBySubNumber(subNumer);
		if (AppUtils.isBlank(sub) || !OrderStatusEnum.UNPAY.value().equals(sub.getStatus())) {
			return Constants.FAIL;
		}
		Date date = new Date();
		sub.setUpdateDate(date);
		sub.setStatus(OrderStatusEnum.CLOSE.value());
		boolean result = subService.cancleOrder(sub);
		if (result) {
			SubHistory subHistory = new SubHistory();
			String time = subHistory.DateToString(date);
			subHistory.setRecDate(date);
			subHistory.setSubId(sub.getSubId());
			subHistory.setStatus(SubHistoryEnum.ORDER_CANCEL.value());
			StringBuilder sb = new StringBuilder();
			sb.append(sub.getUserName()).append("于").append(time).append("取消订单 ");
			subHistory.setUserName(sub.getUserName());
			subHistory.setReason(sb.toString());
			subHistoryService.saveSubHistory(subHistory);
			orderService.cleanOrderCache(sub.getUserId(), sub.getShopId());

			// remove delay queue ordre
			delayCancelHelper.remove(sub.getSubNumber());

			return Constants.SUCCESS;
		}
		return Constants.FAIL;
	}

	/**
	 * 验证码核对.
	 *
	 * @param request
	 * @param response
	 * @param pickup_code
	 * @param subNumber
	 * @return the string
	 */
	@RequestMapping(value = "/validateCode", method = RequestMethod.POST)
	public @ResponseBody String validateCode(HttpServletRequest request, HttpServletResponse response, @RequestParam String pickup_code,
			@RequestParam String subNumber) {
		StoreOrder storeOrder = storeOrderService.getDeliveryOrderBySubNumber(subNumber);
		if (AppUtils.isBlank(storeOrder) || storeOrder.getDlyoSatus() == StoreOrderEnum.DELIVERY.value()) {
			return "订单异常";
		}
		if (!storeOrder.getDlyoPickupCode().equals(pickup_code)) {
			return "提货码错误";
		}
		int result = storeOrderService.updateDeliveryOrderStatus(subNumber, StoreOrderEnum.DELIVERY.value());
		if (result <= 0) {
			return "提货失败";
		}
		return Constants.SUCCESS;
	}

	/**
	 * 获取门店商品
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param poductId
	 * @param productname
	 * @return the string
	 */
	@RequestMapping("/products")
	public String products(HttpServletRequest request, HttpServletResponse response, String curPageNO, Long poductId, String productname) {
		String result = PathResolver.getPath(StoreFrontPage.STORE_HOME_PRODS);
		return result;
	}

	/**
	 * 查看门店商品详情
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param poductId
	 * @param productname
	 * @return the string
	 */
	@RequestMapping("/products/content")
	public String productsContent(HttpServletRequest request, HttpServletResponse response, String curPageNO, Long poductId, String productname) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String storeId = user.getUserId();
		Long shopId = user.getShopId();
		
		int pageSize = 10;
		PageSupport<StoreProductDto> ps = storeService.getStore(curPageNO, pageSize, storeId, shopId, poductId, productname);
		PageSupportHelper.savePage(request, ps);
		String result = PathResolver.getPath(StoreFrontPage.STORE_HOME_PRODS_CONTENT); // simple
		return result;
	}

	/**
	 * 加载商品列表
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param product
	 * @return the string
	 */
	@RequestMapping("/storeProdLayout")
	public String storeProdLayout(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		int pageSize = 9;
		PageSupport<Product> ps = productService.getProductLists(curPageNO, pageSize, shopId, product);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prod", product);
		String result = PathResolver.getPath(StoreFrontPage.STORE_PROD_LAYOUT);
		return result;
	}

	/**
	 * 加载商品的sku列表
	 *
	 * @param request
	 * @param response
	 * @param prodId
	 * @param curPageNO
	 * @return the string
	 */
	@RequestMapping("/storeProdSku/{prodId}")
	public String storeProdSku(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId, String curPageNO) {
		int status = 1;
		PageSupport<Sku> ps = skuService.getSku(curPageNO, prodId, status);
		PageSupportHelper.savePage(request, ps);
		List<Sku> skuList =  ps.getResultList();

		if (AppUtils.isNotBlank(skuList)) {// 如果有SKU
			// 对sku列表进行过滤,过滤掉已在ls_store_sku表存在的sku
			SecurityUserDetail user = UserManager.getUser(request);
			Long storeId = Long.valueOf(user.getUserId());
			
			List<StoreSku> storeSkus = storeSkuService.getStoreSku(storeId, prodId);

			if (AppUtils.isNotBlank(storeSkus)) {
				request.setAttribute("storeProdId", storeSkus.get(0).getStoreProdId());
			}

			for (StoreSku storeSku : storeSkus) {
				Long storeSkuId = storeSku.getSkuId();
				ListIterator<Sku> iterator = skuList.listIterator();
				while (iterator.hasNext()) {
					Sku sku = iterator.next();
					if (storeSkuId.equals(sku.getSkuId())) {
						iterator.remove();
						break;
					}
				}
			}

			request.setAttribute("skuList", skuList);
		} else {// 如果该商品没有sku
			throw new BusinessException("no sku found");
		}
		String result = PathResolver.getPath(StoreFrontPage.STORE_PROD_SKU_LIST);
		return result;
	}

	/**
	 * 门店添加商品和单品.
	 *
	 * @param request
	 * @param response
	 * @param storeProd
	 * @return the string
	 */
	@RequestMapping(value = "/addProd", method = RequestMethod.POST)
	@ResponseBody
	public String addProd(HttpServletRequest request, HttpServletResponse response, @RequestParam String storeProd) {
		StoreProductDto prod = JSONUtil.getObject(storeProd, StoreProductDto.class);
		if (prod == null) {
			return "参数有误!";
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long storeId = Long.valueOf(user.getUserId());
		Long shopId = user.getShopId();

		// 判断商品是否已存在
		if (isExistStoreProd(storeId, prod)) {
			return "该商品已存在!";
		}
		prod.setShopId(shopId);
		prod.setStoreId(storeId);
		String result = storeProdService.addProd(prod);
		return result;
	}

	/**
	 * 设置库存
	 *
	 * @param request
	 * @param response
	 * @param storeProd
	 * @return the string
	 */
	@RequestMapping(value = "/setStock", method = RequestMethod.POST)
	@ResponseBody
	public String setStock(HttpServletRequest request, HttpServletResponse response, @RequestParam String storeProd) {
		StoreProductDto prod = JSONUtil.getObject(storeProd, StoreProductDto.class);
		if (prod == null) {
			return "参数有误";
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long storeId = Long.valueOf(user.getUserId());
		Long shopId = user.getShopId();
		
		prod.setShopId(shopId);
		prod.setStoreId(storeId);
		String result = storeProdService.setStock(prod);
		return result;
	}

	/**
	 * 删除商品
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@RequestMapping("/deleteProd/{id}")
	@ResponseBody
	public String deleteProd(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		try {
			StoreProd storeProd = storeProdService.getStoreProd(id);
			if (AppUtils.isBlank(storeProd)) {
				return "对不起,该商品不存在或已被删除!";
			}

			return storeProdService.deleteProd(storeProd);
		} catch (Exception e) {
			log.error("未知错误", e);
			return Constants.FAIL;
		}
	}

	/**
	 * 删除商品sku
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@RequestMapping("/deleteSku/{id}")
	@ResponseBody
	public String deleteSku(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		try {
			StoreSku storeSku = storeSkuService.getStoreSku(id);
			if (AppUtils.isBlank(storeSku)) {
				return "该SKU不存在或已被删除!";
			}
			return storeProdService.deleteSku(storeSku);
		} catch (Exception e) {
			log.error("未知错误", e);
			return Constants.FAIL;
		}
	}

	/**
	 * 弹出设置库存窗口
	 *
	 * @param request
	 * @param response
	 * @param prodId
	 * @return the string
	 */
	@RequestMapping("/loadStoreProd/{prodId}")
	public String loadStoreProd(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long storeId = Long.valueOf(user.getUserId());

		StoreProductDto storeProductDto = null;
		if (AppUtils.isNotBlank(prodId) && AppUtils.isNotBlank(storeId)) {
			storeProductDto = storeProdService.getStoreProductDto(storeId, prodId);
			if (AppUtils.isBlank(storeProductDto)){
				throw new RuntimeException("该门店商品已被修改或删除，请删除后重新添加！！！");
			}
		}

		request.setAttribute("storeProductDto", storeProductDto);

		String result = PathResolver.getPath(StoreFrontPage.STORE_SETTING_STOCKS);
		return result;
	}

	/**
	 * 判断商品是否已存在.
	 *
	 * @param storeId
	 * @param prod
	 * @return true: 代表存在商品，不可以继续增加
	 */
	private boolean isExistStoreProd(Long storeId, StoreProductDto prod) {
		// StoreProd isExistStoreProd = storeProdService.getStoreProd(storeId,
		// prodId);
		//
		// if(AppUtils.isBlank(isExistStoreProd)){
		// return false;
		// }
		//
		// List<Sku> skus = skuService.getSkuByProd(prodId);
		// if(AppUtils.isNotBlank(skus)){
		// return false;
		// }
		boolean result = false;
		List<StoreSku> storeSkuList = storeSkuService.getStoreSku(storeId, prod.getProductId());
		if (storeSkuList == null) {
			return false;
		}
		List<StoreSkuDto> storeSkuDtoList = prod.getStoreSkuDto(); // 页面传过来的SKU

		for (StoreSkuDto storeSkuDto : storeSkuDtoList) {
			for (StoreSku storeSku : storeSkuList) {
				if (storeSku.getSkuId().equals(storeSkuDto.getSkuId())) {
					result = true;// 发现已经存在的sku
					break;
				}
			}
		}

		return result;
	}
}
