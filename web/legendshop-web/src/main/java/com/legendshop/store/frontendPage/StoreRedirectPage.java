/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.store.frontendPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * The Enum RedirectPage.
 */
public enum StoreRedirectPage implements PageDefinition {
	
	/** The VARIABLE. 可变路径 */
	VARIABLE(""),
	
	/** 查询门店 */
	STORE_QUERY("/s/store/query"),
	
	/** 门店订单 */
	STORE_ORDER_QUERY("/m/store/order"),
	
	;
	
	/** The value. */
	private final String value;

	private StoreRedirectPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("redirect:", path);
	}

	/**
	 * Instantiates a new tiles page.
	 * 
	 * @param value
	 *            the value
	 */
	private StoreRedirectPage(String value) {
		this.value = value;
	}

	public String getNativeValue() {
		return value;
	}

}
