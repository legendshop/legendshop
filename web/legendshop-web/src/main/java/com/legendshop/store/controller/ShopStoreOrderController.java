/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.store.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.entity.UserAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.page.RedirectPage;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.page.CommonFrontPage;
import com.legendshop.core.page.CommonRedirectPage;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.PayMannerEnum;
import com.legendshop.model.constant.SubmitOrderStatusEnum;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.StoreCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.order.AddOrderMessage;
import com.legendshop.model.dto.order.StoreOrderDetailsParamsDto;
import com.legendshop.model.dto.store.StoreOrderAddParamDto;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.store.Store;
import com.legendshop.processor.ProdSnapshotProcessor;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.OrderCartResolverManager;
import com.legendshop.spi.service.OrderUtil;
import com.legendshop.spi.service.StoreProdService;
import com.legendshop.spi.service.StoreService;
import com.legendshop.spi.service.SubService;
import com.legendshop.store.frontendPage.StoreFrontPage;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import com.legendshop.util.JSONUtil;

/**
 * 门店订单处理
 *
 */
@Controller
@RequestMapping("/p/store/order")
public class ShopStoreOrderController {

	private final Logger log = LoggerFactory.getLogger(ShopStoreOrderController.class);

	// 订单处理类
	@Autowired
	private OrderCartResolverManager orderCartResolverManager;

	@Autowired
	private SubService subService;

	// 订单常用处理工具类
	@Autowired
	private OrderUtil orderUtil;

	@Autowired
	private StoreProdService storeProdService;
	
	/** 订单快照 **/
	@Autowired
	private ProdSnapshotProcessor prodSnapshotProcessor;
	
	@Autowired
	private StoreService storeService;

	/**
	 * 去往订单详情页面 ----下单.
	 *
	 * @param request
	 * @param response
	 * @return the string
	 */
	@RequestMapping("/storeOrderDetails")
	public String storeOrderDetails(HttpServletRequest request, HttpServletResponse response,StoreOrderDetailsParamsDto reqParams) {

		com.legendshop.security.model.SecurityUserDetail userDetail = UserManager.getUser(request);

		Boolean isBuyNow = reqParams.getBuyNow();
		if (AppUtils.isBlank(reqParams.getIds()) && (AppUtils.isBlank(isBuyNow) || !isBuyNow)) {
			return PathResolver.getPath(RedirectPage.MYSHOPCART);
		}
		
		/* 根据类型查询订单列表 */
		Map<String, Object> params = new HashMap<String, Object>();
		if (isBuyNow) {
			JSONObject jsonObject = orderUtil.storeBuyNow(isBuyNow, reqParams.getProdId(), reqParams.getSkuId(), reqParams.getCount(),reqParams.getStoreId());
			if (!jsonObject.getBooleanValue("result")) {
				String msg = jsonObject.getString("msg");
				request.setAttribute("message",msg);
				return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
			}
			ShopCartItem shopCartItem = jsonObject.getObject("data", ShopCartItem.class);
			params.put("buyNow", true);
			params.put("shopCartItem", shopCartItem);
		}
		
		params.put("userId", userDetail.getUserId());
		params.put("userName", userDetail.getUsername());
		params.put("shopId", reqParams.getShopId());
		params.put("ids", reqParams.getIds());
		UserShopCartList shopCartList = orderCartResolverManager.queryOrders(CartTypeEnum.SHOP_STORE, params);
		if (AppUtils.isBlank(shopCartList)) {
			request.setAttribute("数据已经发送改变,请重新刷新后操作!", shopCartList);
			return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
		}
		
		//处理购物车，按门店分组
		handlerUserShopCartList(shopCartList);
		
		String items = JSONUtil.getJson(reqParams.getIds());
		request.setAttribute("shopCartItems", items.substring(1, items.length() - 1));
		
		request.setAttribute("shopId", reqParams.getShopId());
		request.setAttribute("userShopCartList", shopCartList);
		

		// 设置订单提交令牌,防止重复提交
		Integer token = CommonServiceUtil.generateRandom();
		request.getSession().setAttribute(Constants.TOKEN, token);
		subService.putUserShopCartList(CartTypeEnum.SHOP_STORE.toCode(), userDetail.getUserId(), shopCartList);
		return PathResolver.getPath(StoreFrontPage.ORDERDETAILS);
	}

	/**
	 * 处理购物车，按门店分组 TODO
	 * @param shopCartList
	 */
	private void handlerUserShopCartList(UserShopCartList shopCartList) {

		ShopCarts shopCarts  = shopCartList.getShopCarts().get(0);
		List<ShopCartItem> shopCartItem = shopCarts.getCartItems();

		// 获取用户默认地址
		UserAddress userAddress = shopCartList.getUserAddress();
		
		if(AppUtils.isNotBlank(shopCartItem)){

			Set<Long> storeCartsSet = new HashSet<Long>();//购物车的storeId集合
			int size = shopCartItem.size();
			for (int i = 0; i < size; i++) {
				storeCartsSet.add(shopCartItem.get(i).getStoreId());
			}
			List <StoreCarts> storeCartList = new ArrayList<StoreCarts>();//按storeId来分
			
			for (Long storeId : storeCartsSet) {//按门店来分单
				
				//查询该门店的信息
				Store store = storeService.getStore(storeId);
				if(store == null){
					continue;
				}
				
				StoreCarts storeCart = new StoreCarts();
				
				// 组装门店信息
				storeCart.setStoreStatus(store.getIsDisable());
				storeCart.setStoreId(storeId);
				storeCart.setStoreName(store.getName());
				storeCart.setStoreAddr(store.getShopAddr());

				// 提货人信息取默认收货地址的信息
				if(AppUtils.isNotBlank(userAddress)){

					storeCart.setBuyerName(userAddress.getReceiver());
					storeCart.setTelphone(userAddress.getMobile());
				}

				List<ShopCartItem> cartItems = new ArrayList<ShopCartItem>();
				
				for (ShopCartItem cartItem : shopCartItem) {
					
					if (cartItem.getStoreId().equals(storeId)) {
						
						cartItems.add(cartItem);
					}
				}
				storeCart.setCartItems(cartItems);
				storeCartList.add(storeCart);
			}
			shopCarts.setStoreCarts(storeCartList);
		}
	}

	/**
	 * 门店提交订单.
	 *
	 */
	@RequestMapping(value = "/storeSubmitOrder", method = RequestMethod.POST)
	public @ResponseBody AddOrderMessage storeSubmitOrder(HttpServletRequest request, HttpServletResponse response, 
			StoreOrderAddParamDto param) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		
		AddOrderMessage message = new AddOrderMessage();
		message.setResult(false);

		// 判断是否重复提交
		Integer sessionToken = (Integer) request.getSession().getAttribute(Constants.TOKEN); // 取session中保存的token
		Integer userToken = Integer.parseInt(param.getToken()); // 取用户提交过来的token进行对比
		if (!userToken.equals(sessionToken)) {
			message.setCode(SubmitOrderStatusEnum.INVALID_TOKEN.value());
			return message;
		}

		// get cache
		UserShopCartList userShopCartList = subService.findUserShopCartList(CartTypeEnum.SHOP_STORE.toCode(), userId);
		if (AppUtils.isBlank(userShopCartList)) {
			message.setCode(SubmitOrderStatusEnum.NOT_PRODUCTS.value());
			return message;
		}
		
		

		// 检查金额： 如果促销导致订单金额为负数
		if (userShopCartList.getOrderActualTotal() <= 0) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("订单金额为负数,下单失败!");
			return message;
		}

		ShopCarts shopCarts = userShopCartList.getShopCarts().get(0);
		
		// 检查库存 暂时不在提交订单流程中检查库存 TODO
		/*String result = verifySafetyStock(shopCarts, chainId);
		if (!Constants.SUCCESS.equals(result)) {
			message.setMessage(result);
			return message;
		}*/
		
		// 处理门店提货人名称
		orderUtil.handlerStoreBuyerName(param.getBuyerName(),userShopCartList);
		
		// 处理门店提货人号码
		orderUtil.handlerStoreTelPhone(param.getTelPhone(),userShopCartList);
		
		//处理门店买家留言
		orderUtil.handlerStoreRemarkText(param.getRemarkText(), userShopCartList);

		// 验证成功，清除令牌
		request.getSession().removeAttribute(Constants.TOKEN);
		userShopCartList.setUserId(userId);
		userShopCartList.setUserName(userName);
		userShopCartList.setInvoiceId(param.getInvoiceId());
		userShopCartList.setPayManner(param.getPayManner());

		/**
		 * 处理订单
		 */
		List<String> subIds = null;
		try {
			subIds = orderCartResolverManager.addOrder(CartTypeEnum.SHOP_STORE, userShopCartList);
		} catch (BusinessException e) {
			log.error(e.getMessage());
			if (e instanceof BusinessException) {
				message.setCode(SubmitOrderStatusEnum.ERR.value());
				message.setMessage(e.getMessage());
				return message;
			}
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("下单失败,请联系商城管理员或商城客服");
			return message;
		}
		if (AppUtils.isBlank(subIds)) {
			message.setMessage("下单失败,请联系商城管理员或商城客服");
			return message;
		} else {

			// offer 延时取消队列
			// EventHome.publishEvent(new DelayCancelOfferEvent(subIds));

			// 发送网站快照备份
			prodSnapshotProcessor.process(subIds);
		}
		subService.evictUserShopCartCache(CartTypeEnum.SHOP_STORE.toCode(), userId);
		subService.evictUserShopCartCache(CartTypeEnum.NORMAL.toCode(), userId);

		PayMannerEnum mannerEnum = PayMannerEnum.fromCode(userShopCartList.getPayManner());
		// 标记回收
		userShopCartList = null;
		StringBuffer url = new StringBuffer();
		switch (mannerEnum) {
		case SHOP_STORE_PAY:
			url.append("/p/store/order/storePaySuccess?");
			url.append("subNums=");
			break;
		default:
			url.append("/p/orderSuccess?");
			url.append("subNums=");
			break;
		}
		for (int i = 0; i < subIds.size(); i++) {
			url.append(subIds.get(i)).append(",");
		}
		String path = url.substring(0, url.length() - 1);
		message.setResult(true);
		message.setUrl(path);
		return message;
	}

	/**
	 * 检测下单的清单有商品库存不足的清单.
	 *
	 * @param carts
	 * @param storeId
	 * @return the string
	 */
	private String verifySafetyStock(ShopCarts carts, Long storeId) {
		String result = Constants.SUCCESS;
		List<ShopCartItem> list = carts.getCartItems();
		int count = list.size();
		for (int i = 0; i < count; i++) {
			ShopCartItem basket = list.get(i);
			if (basket != null) {
				Integer stocks = storeProdService.getStocks(storeId, basket.getProdId(), basket.getSkuId());
				// 产品库存
				if (stocks == null) {
					stocks = 0;
				}
				if (stocks - basket.getBasketCount() < 0) {
					result = "";
					result = basket.getProdName() + " 库存不足，只剩 " + stocks + " 件,请重新选择商品件数";
					break;
				}
			}
		}
		return result;
	}

	/**
	 * 门店支付成功
	 * @param request
	 * @param response
	 * @param subNums
	 * @return the string
	 */
	@RequestMapping("/storePaySuccess")
	public String storePaySuccess(HttpServletRequest request, HttpServletResponse response, String subNums) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		if (AppUtils.isBlank(userId)) {
			return PathResolver.getPath(CommonRedirectPage.LOGIN);
		}
		
		if (AppUtils.isBlank(subNums)) {
			return PathResolver.getPath(CommonRedirectPage.MYORDER);
		}
		
		String[] orders = subNums.split(",");
		if (AppUtils.isBlank(orders)) {
			return PathResolver.getPath(CommonRedirectPage.MYORDER);
		}
		
		List<Sub> subs = subService.getSubBySubNums(orders, userId, OrderStatusEnum.UNPAY.value());
		if (AppUtils.isBlank(subs)) {
			return PathResolver.getPath(CommonRedirectPage.MYORDER);
		}

		String subIds = "";
		Double totalAmount = 0.0;
		String subject = "";

		List<Sub> orderList = new ArrayList<Sub>();
		for (Sub sub : subs) {
			orderList.add(sub);
			totalAmount = Arith.add(totalAmount, sub.getActualTotal());
			subIds += sub.getId() + ",";
			subject += subject + sub.getProdName() + ",";
		}
		subIds = subIds.substring(0, subIds.length() - 1);

		request.setAttribute("subject", subject);
		request.setAttribute("subNums", subIds);
		request.setAttribute("orderList", orderList);
		request.setAttribute("totalAmount", totalAmount);
		return PathResolver.getPath(StoreFrontPage.ORDERSUCCESS);
	}

}
