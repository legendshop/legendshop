/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.store.frontendPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 门店前端页面定义
 */
public enum StoreFrontPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 门店管理首页 */
	SHOP_STORE_HOME("/shopStoreHome"),

	/** 门店列表 */
	SHOP_STORE_LIST("/shopStoreList"),

	/** 添加门店 */
	SHOP_STORE("/shopStore"),

	/** 更改密码 */
	SHOP_STORE_UPDATE_PASSWD("/changePasswsd"),

	/** 门店编辑 */
	SHOP_EDIT_STORE("/shopEditStore"),

	/** 门店商品 */
	STORE_HOME_PRODS("/storeHomeProds"),

	/** 门店商品内容 */
	STORE_HOME_PRODS_CONTENT("/storeHomeProdsContent"),

	/** 门店订单信息 */
	ORDERDETAILS("/storeOrderDetails"),

	/** 门店商品样式 */
	STORE_PROD_LAYOUT("/storeProdLayout"),

	/** 提货验证 */
	SHOP_STORE_TAKEN_LAYOUT("/shopStoreTakenLayout"),

	/** 门店商品Sku设置 */
	STORE_PROD_SKU_LIST("/storeProdSku"),

	/** 门店库存设置 */
	STORE_SETTING_STOCKS("/storeSettingStocks"),

	/** 门店订单支付成功 */
	ORDERSUCCESS("/storePaySuccess"),

	/** 按商品找城市的门店 */
	STORE_PRODUCT_CITYS_PAGE("/storeProductCitysPage");

	/** The value. */
	private final String value;

	private StoreFrontPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath("store", path);
	}

	public String getNativeValue() {
		return value;
	}

}
