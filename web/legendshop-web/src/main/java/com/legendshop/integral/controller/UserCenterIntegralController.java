/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.integral.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.util.DateUtils;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.integral.page.IntegralFrontPage;
import com.legendshop.model.dto.integral.IntegralOrderDto;
import com.legendshop.model.dto.integral.IntegralRuleDto;
import com.legendshop.model.dto.order.OrderSearchParamDto;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.integral.IntegraLog;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.spi.service.IntegraLogService;
import com.legendshop.spi.service.IntegralOrderService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 用户中心积分控制器.
 */
@Controller
@RequestMapping("/p")
public class UserCenterIntegralController extends BaseController {

	@Autowired
	private IntegraLogService integraLogService;

	@Autowired
	private IntegralOrderService integralOrderService;

	@Autowired
	private ConstTableService constTableService;

	@Autowired
	private UserDetailService userDetailService;

	/**
	 * 我的积分明细
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param logDesc
	 * @param logType
	 * @return
	 */
	@RequestMapping("/integral")
	public String integral(HttpServletRequest request, HttpServletResponse response, String curPageNO, String logDesc, Long logType,Date startDate,Date endDate) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		// 获取该用户的积分值
		Integer score = userDetailService.getScore(userId);
		request.setAttribute("score", score);
		if (AppUtils.isNotBlank(endDate)) {
			endDate = DateUtils.getEndMonment(endDate);
		}	
		PageSupport<IntegraLog> ps = integraLogService.getIntegraLog(userId, curPageNO, logDesc, logType,startDate,endDate);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("curPageNO", curPageNO);
		request.setAttribute("logDesc", logDesc);
		request.setAttribute("logType", logType);
		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);

		// 获得规则
		ConstTable constTable = constTableService.getConstTablesBykey("INTEGRAL_RELU_SETTING", "INTEGRAL_RELU_SETTING");
		IntegralRuleDto releSetting = null;
		if (AppUtils.isNotBlank(constTable)) {
			String setting = constTable.getValue();
			releSetting = JSONUtil.getObject(setting, IntegralRuleDto.class);
		}
		request.setAttribute("releSetting", releSetting);
		return PathResolver.getPath(IntegralFrontPage.MYINTEGRAL);
	}

	/**
	 *  兑换记录
	 * @param request
	 * @param response
	 * @param paramDto
	 * @return
	 */
	@RequestMapping("/integral/list")
	public String list(HttpServletRequest request, HttpServletResponse response, OrderSearchParamDto paramDto) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		paramDto.setPageSize(5);
		paramDto.setUserId(userId);
		if (AppUtils.isNotBlank(paramDto.getEndDate())) {
			paramDto.setEndDate(DateUtils.getEndMonment(paramDto.getEndDate()));// 结束日期要延迟一天
		}

		PageSupport<IntegralOrderDto> pageSupport = integralOrderService.getIntegralOrderDtos(paramDto);
		PageSupportHelper.savePage(request, pageSupport);
		request.setAttribute("paramDto", paramDto);
		request.setAttribute("status", paramDto.getStatus());
		return PathResolver.getPath(IntegralFrontPage.INTEGRAL_ORDER_LIST_PAGE);
	}

	/**
	 * 用户积分订单详情
	 * @param request
	 * @param response
	 * @param orderSn
	 * @return
	 */
	@RequestMapping("/integral/orderDetail/{orderSn}")
	public String orderDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable String orderSn) {
		IntegralOrderDto order = integralOrderService.findIntegralOrderDetail(orderSn);
		request.setAttribute("order", order);
		return PathResolver.getPath(IntegralFrontPage.INTEGRAL_ORDER_DETAIL_PAGE);
	}

	/**
	 * 确认收货
	 * @param request
	 * @param response
	 * @param sn
	 * @return
	 */
	@RequestMapping(value = "/integralOrderReceive/{sn}", method = RequestMethod.POST)
	@ResponseBody
	public String integralOrderReceive(HttpServletRequest request, HttpServletResponse response, @PathVariable String sn) {
		String result = integralOrderService.shouhuo(sn);
		return result;
	}

	/**
	 * 更新订单状态
	 * 
	 * @param subId
	 * @param status
	 * @param userName
	 * @return
	 */
	@RequestMapping(value = "/integral/orderCancel/{sn}", method = RequestMethod.POST)
	public @ResponseBody String orderCancel(HttpServletRequest request, HttpServletResponse response, @PathVariable String sn) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String result = integralOrderService.orderCancel(sn, null, userId, "用户取消订单,该订单流水号是:" + sn);
		return result;
	}

	/**
	 * 删除订单
	 * 
	 * @param request
	 * @param response
	 * @param sn
	 * @return
	 */
	@RequestMapping(value = "/integral/orderDel/{sn}", method = RequestMethod.POST)
	public @ResponseBody String orderDel(HttpServletRequest request, HttpServletResponse response, @PathVariable String sn) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		return integralOrderService.orderDel(sn, userId);
	}

}
