/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.integral.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 用户中心前台页面.
 */
public enum IntegralFrontPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 分类筛选页面 */
	SORT_LIST("/integralCategorys"),

	/** 积分商品管理 */
	PRODUCT_LIST("/integralProdList"),

	/** 积分竞兑排行榜 */
	EXCHANGE_LIST("/exchangeList"),

	/** 浏览历史 */
	EXCHANGE_VIEW_LIST("/exchangeViewList"),

	/** 兑换积分订单 */
	INTEGRAL_BUY("/integralBuy"),

	/** 积分商城列表 */
	INTEGRA_PROD_LIST("/integralList"),

	/** 积分商品详情 */
	INTEGRA_PROD_DETAIL("/integralProdDetail"),

	/** 我的积分 */
	MYINTEGRAL("/userIntegral"),

	/** 我的积分列表 */
	INTEGRAL_ORDER_LIST_PAGE("/userIntegralList"),

	/** 积分订单信息 */
	INTEGRAL_ORDER_DETAIL_PAGE("/userIntegralOrderDetail"),


	/** 积分中心 */
	INTEGRAL_CENTER("/integralCenter"), 
	
	/** 积分中心 更多页面 */
	INTEGRAL_COUPONMORE("/integraCouponLis");

	/** The value. */
	private final String value;

	private IntegralFrontPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath("integral", path);
	}

	public String getNativeValue() {
		return value;
	}

}
