/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.integral.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.util.XssFilterUtil;
import com.legendshop.util.SafeHtml;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.page.CommonFrontPage;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.integral.page.IntegralFrontPage;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.CouponGetTypeEnum;
import com.legendshop.model.constant.CouponProviderEnum;
import com.legendshop.model.dto.integral.IntegralProdSeachParam;
import com.legendshop.model.dto.integral.IntegralRuleDto;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.integral.IntegralOrderItem;
import com.legendshop.model.entity.integral.IntegralProd;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.spi.service.IntegralOrderItemService;
import com.legendshop.spi.service.IntegralOrderService;
import com.legendshop.spi.service.IntegralProdService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 积分商品控制器
 *
 */
@Controller
@RequestMapping("/integral")
public class IntegralProdController extends BaseController {

	@Autowired
	private IntegralProdService integralProdService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private IntegralOrderService integralOrderService;

	@Autowired
	private ConstTableService constTableService;
	
	@Autowired
	private IntegralOrderItemService integralOrderItemService;

	/**
	 * 积分列表
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param param
	 * @return
	 */
	@RequestMapping("/list")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, IntegralProdSeachParam param) {
		Category firstCid = null;
		Category twoCid = null;
		Category thirdCid = null;
		if (!AppUtils.isBlank(param.getFirstCid())) {
			firstCid = integralProdService.findCategory(param.getFirstCid());
		}
		if (!AppUtils.isBlank(param.getTwoCid())) {
			twoCid = integralProdService.findCategory(param.getTwoCid());
		}
		if (!AppUtils.isBlank(param.getThirdCid())) {
			thirdCid = integralProdService.findCategory(param.getThirdCid());
		}
		PageSupport<IntegralProd> ps = integralProdService.getIntegralProd(curPageNO, param.getFirstCid(), param.getTwoCid(), param.getThirdCid());
		List<Category> categories = integralProdService.findIntegralCategory();
		request.setAttribute("categorys", categories);
		request.setAttribute("param", param);
		request.setAttribute("firstCategory", firstCid);
		request.setAttribute("twoCategory", twoCid);
		request.setAttribute("thirdCategory", thirdCid);
		PageSupportHelper.savePage(request, ps);
		String path = PathResolver.getPath(IntegralFrontPage.INTEGRA_PROD_LIST);
		return path;
	}

	/**
	 * 积分列表 for ajax
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param param
	 * @return
	 */
	@RequestMapping("/prodList")
	public String prodList(HttpServletRequest request, HttpServletResponse response, String curPageNO, IntegralProdSeachParam param) {

		//防止做注入
		String integralKeyWord = XssFilterUtil.cleanXSS(param.getIntegralKeyWord());
		String orders = XssFilterUtil.cleanXSS(param.getOrders());

		PageSupport<IntegralProd> ps = integralProdService.getIntegralProd(curPageNO, param.getFirstCid(), param.getTwoCid(), param.getThirdCid(),
				param.getStartIntegral(), param.getEndIntegral(), integralKeyWord, orders);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("param", param);
		String path = PathResolver.getPath(IntegralFrontPage.PRODUCT_LIST);
		return path;
	}

	/**
	 * 左边积分分类栏目
	 * 
	 * @param request
	 * @param response
	 * @param categoryId
	 * @return
	 */
	@RequestMapping("/sortlist")
	public String sortList(HttpServletRequest request, HttpServletResponse response, Long categoryId) {
		return PathResolver.getPath(IntegralFrontPage.SORT_LIST);
	}

	/**
	 * 积分兑换排行榜
	 * 
	 * @param request
	 * @param response
	 * @param categoryId
	 * @return
	 */
	@RequestMapping("/exchangeList")
	public String exchangeList(HttpServletRequest request, HttpServletResponse response, Long firstCid, Long twoCid, Long thirdCid) {
		List<IntegralProd> exchangeList = integralProdService.findExchangeList(firstCid, twoCid, thirdCid);
		request.setAttribute("exchangeList", exchangeList);
		return PathResolver.getPath(IntegralFrontPage.EXCHANGE_LIST);
	}

	/**
	 * 商品详情积分兑换排行榜
	 * 
	 * @param request
	 * @param response
	 * @param categoryId
	 * @return
	 */
	@RequestMapping("/exchangeViewList")
	public String exchangeViewList(HttpServletRequest request, HttpServletResponse response, Long firstCid, Long twoCid, Long thirdCid) {
		List<IntegralProd> exchangeList = integralProdService.findExchangeList(firstCid, twoCid, thirdCid);
		request.setAttribute("exchangeList", exchangeList);
		return PathResolver.getPath(IntegralFrontPage.EXCHANGE_VIEW_LIST);
	}

	/**
	 * 积分兑换排行榜
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/view/{id}")
	public String view(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String loginUserName = null;
		if (AppUtils.isNotBlank(user)){
			loginUserName = user.getUsername();
		}

		IntegralProd prod = integralProdService.findIntegralProd(id);
		if (prod == null) {
			return PathResolver.getPath(CommonFrontPage.PROD_NON_EXISTENT);
		}
		request.setAttribute("integralProd", prod);
		request.setAttribute("loginUserName", loginUserName);
		return PathResolver.getPath(IntegralFrontPage.INTEGRA_PROD_DETAIL);
	}

	/**
	 * 积分中心
	 */
	@RequestMapping(value = "/integralCenter")
	public String integralCenter(HttpServletRequest request, HttpServletResponse response) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		if (user != null) {
			UserDetail	userDetail = this.userDetailService.getUserDetailById(user.getUserId());
			request.setAttribute("users", userDetail);
		}
		Date nowDate = new Date();
		// 代金券
		List<Coupon> lists = this.integralProdService.getCouponByProvider(CouponGetTypeEnum.POINTS.value(), CouponProviderEnum.SHOP.value(), nowDate);
		// 红包
		List<Coupon> listp = this.integralProdService.getCouponByProvider(CouponGetTypeEnum.POINTS.value(), CouponProviderEnum.PLATFORM.value(), nowDate);
		// 热门礼品
		List<IntegralProd> integralProds = integralOrderService.getHotIntegralProds();
		request.setAttribute("listp", listp);
		request.setAttribute("lists", lists);
		request.setAttribute("integralProds", integralProds);
		// 获得规则
		ConstTable constTable = constTableService.getConstTablesBykey("INTEGRAL_RELU_SETTING", "INTEGRAL_RELU_SETTING");
		IntegralRuleDto releSetting = null;
		if (AppUtils.isNotBlank(constTable)) {
			String setting = constTable.getValue();
			releSetting = JSONUtil.getObject(setting, IntegralRuleDto.class);
		}
		request.setAttribute("releSetting", releSetting);
		return PathResolver.getPath(IntegralFrontPage.INTEGRAL_CENTER);
	}

	/**
	 * 红包，代金券 更多
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param type
	 * @return
	 */
	@RequestMapping(value = "/integralCenter/list/{type}")
	public String integralCenterList(HttpServletRequest request, HttpServletResponse response, String curPageNO, @PathVariable String type) {
		PageSupport<Coupon> ps = this.integralOrderService.getCouponListPage(curPageNO, type);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("type", type);
		return PathResolver.getPath(IntegralFrontPage.INTEGRAL_COUPONMORE);
	}
	
	/**
	 * 判断用户购买的积分商品是否已达上限
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/checkIntegralItemCount")
	@ResponseBody
	public String checkIntegralItemCount(HttpServletRequest request, HttpServletResponse response, Long prodId){
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		List<IntegralOrderItem> list = integralOrderItemService.queryByUserIdAndProdId(userId, prodId);
		if(AppUtils.isNotBlank(list)){
			return Integer.toString(list.size());
		}
		return Constants.SUCCESS;
	}

}
