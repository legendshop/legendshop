/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.integral.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.integral.page.IntegralFrontPage;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.UserSecurity;
import com.legendshop.model.entity.integral.IntegralProd;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.IntegralOrderService;
import com.legendshop.spi.service.IntegralProdService;
import com.legendshop.spi.service.UserAddressService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 积分订单兑换.
 *
 */
@Controller
public class IntegralOrderController extends BaseController {

	@Autowired
	private IntegralProdService integralProdService;

	@Autowired
	private IntegralOrderService integralOrderService;

	@Autowired
	private UserAddressService userAddressService;

	@Autowired
	private UserDetailService userDetailService;
	
    @Autowired
    private PasswordEncoder passwordEncoder;

	/**
	 * 积分订单兑换.
	 *
	 * @param request
	 * @param response
	 * @param prodId
	 * @param count
	 * @return the map< string, string>
	 */
	@RequestMapping(value = "/integral/cart/addBuy/{prodId}", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, String> addBuyIntegral(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId, Integer count) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String loginUserName = user.getUsername();
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("result", "false");
		if (AppUtils.isBlank(prodId) || AppUtils.isBlank(count)) {
			map.put("message", "违法参数,请重试");
			return map;
		}
		IntegralProd integralProd = integralProdService.getIntegralProd(prodId);
		return validateIntegral(request, loginUserName, userId, integralProd, count);
	}

	/**
	 * 验证积分.
	 *
	 * @param request
	 * @param loginUserName
	 * @param userId
	 * @param integralProd
	 * @param count
	 * @return the map< string, string>
	 */
	private Map<String, String> validateIntegral(HttpServletRequest request, String loginUserName, String userId, IntegralProd integralProd, Integer count) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("result", "false");

		SecurityUserDetail user = UserManager.getUser(request);
		
		if (!UserManager.isUserLogined(user)) {
			map.put("code", "LOGIN");
			return map;
		}
		if (AppUtils.isBlank(integralProd) || Constants.OFFLINE.equals(integralProd.getStatus())) {
			map.put("message", "商品已下架或者不存在");
			return map;
		}
		if (Constants.ONLINE.equals(integralProd.getIsLimit())) { // 如果启用购买限制
			if (count > integralProd.getLimitNum()) {
				map.put("message", "超过购买限制！");
				return map;
			}
			Long userByCount = integralOrderService.getUserBuyShopCount(userId, integralProd.getId());
			if (AppUtils.isNotBlank(userByCount) && userByCount >= integralProd.getLimitNum() || userByCount + count > integralProd.getLimitNum()) {
				map.put("message", "您已购买过" + userByCount + "件此商品，超过购买限制");
				return map;
			}
		}
		if (count > integralProd.getProdStock()) {
			map.put("message", "商品库存不足,请重新选择数量！");
			return map;
		}
		Double score = Arith.mul(integralProd.getExchangeIntegral(), count);
		UserDetail userDetail = userDetailService.getUserDetailById(userId);
		if (AppUtils.isBlank(userDetail.getScore()) || userDetail.getScore() <= 0 || userDetail.getScore() < score) {
			map.put("message", "用户积分不足,您的积分余额为:" + (userDetail.getScore() == null ? 0 : userDetail.getScore()));
			return map;
		}
		// 查看用户是否添加过用户下单地址
		Long number = userAddressService.getMaxNumber(loginUserName);
		if (number == 0) {
			map.put("message", "亲,请设置订单收货地址");
			map.put("code", "ORDER_ADDRESS");
			return map;
		}
		map.put("result", "true");
		return map;
	}

	/**
	 * 积分订单兑换.
	 *
	 * @param request
	 * @param response
	 * @param prodId
	 * @param count
	 * @param score
	 * @return the string
	 */
	@RequestMapping(value = "/p/integral/cart/{prodId}")
	public String integralCart(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId, Integer count, Integer score) {
		// 设置订单提交令牌,防止重复提交
		if (AppUtils.isNotBlank(count) && AppUtils.isNotBlank(score)) {
			
			SecurityUserDetail user = UserManager.getUser(request);
			String userName = user.getUsername();
			
			List<UserAddress> userAddress = userAddressService.getUserAddress(userName);
			Integer token = CommonServiceUtil.generateRandom();
			request.getSession().setAttribute(Constants.INTEGRAL_TOKEN, token);
			request.setAttribute("prodId", prodId);
			request.setAttribute("count", count);
			Double totalScore = Arith.mul(score, count);
			request.setAttribute("totalScore", totalScore);
			request.setAttribute("userAddress", userAddress);
			return PathResolver.getPath(IntegralFrontPage.INTEGRAL_BUY);
		}
		return PathResolver.getPath("", IntegralFrontPage.VARIABLE);
	}

	/**
	 * 积分购买商品.
	 *
	 * @param request
	 * @param response
	 * @param prodId
	 * @param addressId
	 * @param count
	 * @param token
	 * @return the map< string, string>
	 */
	@RequestMapping("/p/integral/cart/submit/{prodId}")
	@ResponseBody
	public Map<String, String> shopBuyIntegral(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId, Long addressId,
			Integer count, Integer token, String password) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String loginUserName = user.getUsername();
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("result", "false");
		if (AppUtils.isBlank(prodId) || AppUtils.isBlank(count) || AppUtils.isBlank(token) || AppUtils.isBlank(password)) {
			map.put("message", "违法参数,请重试");
			return map;
		}
		// 判断是否重复提交
		Integer sessionToken = (Integer) request.getSession().getAttribute(Constants.INTEGRAL_TOKEN); // 取session中保存的token
		if (!token.equals(sessionToken)) {
			map.put("message", "请勿重复提交订单");
			return map;
		}
		// 判断支付密码是否正确
		UserSecurity security = userDetailService.getUserSecurity(loginUserName);
		if (security.getPaypassVerifn().intValue() == 0) { //是否通过支付验证
            map.put("message", "请开启安全支付密码");
            return map;
        }
		UserDetail userDetail = userDetailService.getUserDetailByIdNoCache(userId);
		if (!passwordEncoder.matches(password, userDetail.getPayPassword())) {
            map.put("message", "支付密码错误请重新输入！");
            return map;
        }
		//积分商品兑换
		IntegralProd integralProd = integralProdService.getIntegralProd(prodId);
		map = validateIntegral(request, loginUserName, userId, integralProd, count);
		if ("false".equals(map.get("result"))) {
			return map;
		}
		UserAddress userAddress = userAddressService.getUserAddress(addressId);
		if (AppUtils.isBlank(userAddress) || !userAddress.getUserId().equals(userId)) {
			map.put("message", "没有找到该用户的收货信息");
			return map;
		}
		request.getSession().removeAttribute(Constants.INTEGRAL_TOKEN);
		try {
			String result = integralOrderService.shopBuyIntegral(userId, userAddress, integralProd, count, "");
			if (!Constants.SUCCESS.equals(result)) {
				map.put("result", "false");
				map.put("message", result);
				return map;
			}
		} catch (Exception e) {
			map.put("result", "false");
			if (e instanceof BusinessException) {
				map.put("message", e.getMessage());
				return map;
			} else {
				e.printStackTrace();
				map.put("message", "系统错误,请稍候重试");
			}
			return map;
		}

		map.put("result", "true");
		return map;
	}

}
