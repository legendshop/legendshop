/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.group.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 团购前进页面定义.
 */
public enum GroupFowardPage implements PageDefinition {

	/** 团购商品列表 */
	PROD_LIST_QUERY("/admin/group/product/query"),

	/** The gsort list query. */
	GSORT_LIST_QUERY("/admin/gsort/query"), ;

	/** The value. */
	private final String value;

	/**
	 * 实例化一个新的团购前台页面
	 * 
	 * @param value
	 * @param template
	 */
	private GroupFowardPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */

	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */

	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("forward:", path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */

	public String getNativeValue() {
		return value;
	}

}
