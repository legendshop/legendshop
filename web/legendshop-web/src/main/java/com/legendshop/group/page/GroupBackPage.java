/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.group.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 后台页面定义.
 */
public enum GroupBackPage implements PageDefinition {

	/** 团购商品展示 */	
	GROUP_PROD_LAYOUT("/shop/groupProdLayout"),
	
	/** 团购商品分类页面 */
	PRODLIST_LOADCATEGORY_PAGE("/shop/loadAllCategory"),
	
	/** The VARIABLE. 可变路径 */
	VARIABLE("");
	
	/** The value. */
	private final String value;

	/**
	 * 实例化一个新的团购后台页面
	 *
	 * @param value the value
	 * @param template the template
	 */
	private GroupBackPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */

	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */

	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath("group", path);
	}

	/* (non-Javadoc)
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
