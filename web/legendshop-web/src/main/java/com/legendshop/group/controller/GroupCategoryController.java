/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.group.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.group.page.GroupBackPage;
import com.legendshop.model.dto.CategoryTree;
import com.legendshop.model.entity.Category;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.util.JSONUtil;

/**
 * 团购分类类目.
 *
 */
@Controller
@RequestMapping("/s/group")
public class GroupCategoryController extends BaseController {

	@Autowired
	private CategoryService categoryService;

	
	/**
	 * 加载团购分类类目页面.
	 *
	 * @param request
	 * @param response
	 * @param type
	 * @return the string
	 */
	@RequestMapping(value = "/loadAllCategory/{type}")
	public String loadCategory(HttpServletRequest request, HttpServletResponse response, @PathVariable String type) {
		List<Category> catList = categoryService.getCategoryListByStatus(type);
		List<CategoryTree> catTrees = new ArrayList<CategoryTree>();
		for (Category cat : catList) {
			CategoryTree tree = new CategoryTree(cat.getId(), cat.getParentId(), cat.getName());
			catTrees.add(tree);
		}
		request.setAttribute("catTrees", JSONUtil.getJson(catTrees));
		return PathResolver.getPath(GroupBackPage.PRODLIST_LOADCATEGORY_PAGE);
	}
}