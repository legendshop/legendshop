/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.group.controller;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.legendshop.model.dto.GroupSelProdDto;
import com.legendshop.model.dto.MergeGroupSelProdDto;
import com.legendshop.spi.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.group.page.GroupBackPage;
import com.legendshop.group.page.GroupFrontPage;
import com.legendshop.group.page.GroupRedirectPage;
import com.legendshop.mergeGroup.frontendPage.MergeGroupFrontPage;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.GroupStatusEnum;
import com.legendshop.model.constant.SkuActiveTypeEnum;
import com.legendshop.model.dto.MergeGroupOperationDto;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.entity.Group;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.Sku;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SystemParameter;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;

/**
 * 团购控制器.
 */
@Controller
@RequestMapping("/s/group")
public class GroupController extends BaseController {
	
	private final Logger log = LoggerFactory.getLogger(GroupController.class);

	@Autowired
	private GroupService groupService;

	@Autowired
	private ProductService productService;

	@Autowired
	private ShopDetailService shopDetailService;
	
	@Autowired
	private SystemParameterService systemParameterService;
	
	@Autowired
	private SkuService skuService;
	
	@Autowired
	private SubService subService;

	@Autowired
	private GroupSkuService groupSkuService;
	
	/**
	 * 团购列表页面
	 */
	@RequestMapping("/query")
	@Authorize(authorityTypes = AuthorityType.GROUP_MANAGE)
	public String query(HttpServletRequest request, HttpServletResponse response, Group group, String curPageNO) {
		
 		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		group.setShopId(shopId);
		
		PageSupport<Group> ps = groupService.getGroupPage(curPageNO, group);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("group", group);
		return PathResolver.getPath(GroupFrontPage.GROUP_LIST_PAGE);
	}

	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		String result=PathResolver.getPath(GroupFrontPage.GROUP_PAGE);
		return result;
	}

	/**
	 * 挑选商品弹层
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param product
	 * @return
	 */
	@RequestMapping("/groupProdLayout")
	@Authorize(authorityTypes = AuthorityType.GROUP_MANAGE)
	public String groupProdLayout(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<Product> ps = productService.groupProdLayout(curPageNO, shopId, product);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prod", product);
		String result = PathResolver.getPath(GroupBackPage.GROUP_PROD_LAYOUT);
		return result;
	}

	/**
	 * 拿到prodId对应的商品
	 * 
	 * @param request
	 * @param response
	 * @param prodId
	 * @return
	 */
	@RequestMapping("/groupProd/{prodId}")
	public String groupProd(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {
		List<Sku> skuList = skuService.getSkuByProd(prodId);
		Iterator<Sku> iterator = skuList.iterator();
		while (iterator.hasNext()){
			Sku sku = iterator.next();
			if (sku.getStocks() == 0){
				iterator.remove();
			}
		}
		request.setAttribute("skuList",skuList);
		String result = PathResolver.getPath(GroupFrontPage.GROUP_PROD);
		return result;
	}

	/**
	 * 保存团购活动
	 * @param request
	 * @param response
	 * @param group
	 * @return
	 */
	@RequestMapping("/save")
	@Authorize(authorityTypes = AuthorityType.GROUP_MANAGE)
	public String save(HttpServletRequest request, HttpServletResponse response, Group group) {
		HttpSession session = request.getSession();
		SecurityUserDetail user = UserManager.getUser(request);
		SystemParameter systemParameter=systemParameterService.getSystemParameter("GROUP_ACTIVITY_AUDIT");
		Long groupStatus;
		if(AppUtils.isNotBlank(systemParameter)&&systemParameter.getValue().equals("true")){//确定团购活动是否需要审核
			groupStatus=GroupStatusEnum.VALIDATING.value();
		}else{
			groupStatus=GroupStatusEnum.ONLINE.value();
		}
		if (AppUtils.isBlank(group.getId())) {
			String shopName = shopDetailService.getShopName(user.getShopId());
			if (AppUtils.isNotBlank(user.getShopId()) && AppUtils.isNotBlank(shopName)) {
				boolean result = groupService.saveGroupProd(group, user.getShopId(), user.getUsername(), user.getUserId(), shopName, groupStatus);
				if (!result) {
					throw new NullPointerException("save fail");
				}
			}
		} else {
			// update
			Group oldGroup = null;
			if (AppUtils.isNotBlank(group.getId())) {
				oldGroup = groupService.getGroup(group.getId());
			}
			boolean result = groupService.updateGroup(group, oldGroup, user.getShopId(), user.getUsername(),user.getUserId(), groupStatus);
			if (!result) {
				throw new NullPointerException("update fail");
			}
		}
		return PathResolver.getPath(GroupRedirectPage.GROUP_QUERY);
	}

	/**
	 * 终止活动
	 */
	@RequestMapping(value = "/updateStatus/{id}", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
	@Authorize(authorityTypes = AuthorityType.GROUP_MANAGE)
	public @ResponseBody String updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Group groupPo = groupService.getGroup(shopId, id);
		if (AppUtils.isBlank(groupPo)) {
			return "该活动不存在或已被删除！";
		}
		if (groupPo.getStatus() == 1) {
			String result = groupService.offlineGroup(groupPo);
			return result;
		}
		return "数据有误,请刷新后重试！";
	}

	/**
	 * 删除团购活动
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	@ResponseBody
	@Authorize(authorityTypes = AuthorityType.GROUP_MANAGE)
	public String deleteGroup(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Group group = groupService.getGroup(shopId, id);
		if (AppUtils.isBlank(group)) {
			return "该活动不存在或已被删除！";
		}
		Date nowDate = new Date();
		if (group.getStartTime().before(nowDate) && group.getEndTime().after(nowDate) && group.getStatus()==1) {
			return "此活动还在活动进行中,不能此操作";
		} else {
			//删除活动商品
			groupSkuService.deleteGroupSkuByGroupId(group.getId());
			groupService.deleteGroup(group);
			return Constants.SUCCESS;
		}
	}
	
	/**
	 * 更新团购活动删除状态（逻辑删除 ）
	 */
	@RequestMapping("/updateDeleteStatus/{id}")
	@ResponseBody
	@Authorize(authorityTypes = AuthorityType.GROUP_MANAGE)
	public String updateDeleteStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		
		Group group = groupService.getGroup(id);
		
		if (AppUtils.isBlank(group)) {
			return "该活动不存在或已被删除";
		}
			
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Integer deleteStatus = 1;
		//删除活动商品
		groupSkuService.deleteGroupSkuByGroupId(group.getId());
		groupService.updateDeleteStatus(shopId,group,deleteStatus);
		return Constants.SUCCESS;
		
	}
	
	/**
	 * 活动运营列表
	 */
	@RequestMapping("/operation/{groupId}")
	public String queryOperationList(HttpServletRequest request, HttpServletResponse response, String curPageNO, @PathVariable Long groupId) {
		Integer pageSize = 10;

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		// 获取运营结果统计数据
		OperateStatisticsDTO operateStatistics = groupService.getOperateStatistics(groupId);
		
		PageSupport<Sub> ps = subService.getGroupOperationList(curPageNO, pageSize, groupId, shopId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("date", new Date());
		request.setAttribute("groupId", groupId);
		request.setAttribute("operateStatistics",operateStatistics);
		return PathResolver.getPath(GroupFrontPage.GROUP_OPERATION);
	}
	
	

	/**
	 * @Description: 编辑
	 * @date 2016-5-9
	 */
	@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	@Authorize(authorityTypes = AuthorityType.GROUP_MANAGE)
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Group group = groupService.getGroup(id);
		if (AppUtils.isBlank(group)) {
			throw new NullPointerException("seckillActivity is null");
		}
		List<GroupSelProdDto> prodLists = productService.queryGroupProductByGroupId(group.getId());
		request.setAttribute("prodLists", prodLists);
		request.setAttribute("group", group);
		return PathResolver.getPath(GroupFrontPage.GROUP_PAGE);
	}

	/**
	 * 查看活动详情
	 */
	@RequestMapping(value = "/groupView/{id}", method = RequestMethod.GET)
	public String groupView(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Group group = groupService.getGroup(shopId, id);
		if (AppUtils.isBlank(group)) {
			throw new BusinessException("group is null");
		}
		request.setAttribute("group", group);

		List<GroupSelProdDto> prodLists = productService.queryGroupProductByGroupId(group.getId());
		request.setAttribute("prodLists", prodLists);
		return PathResolver.getPath(GroupFrontPage.GROUP_VIEW);
	}
	
	/**
	 * 上传图片大小限制
	 * @param request
	 * @param response
	 * @param groupPicFile
	 * @return
	 */
	@RequestMapping(value = "/checkSize", method = RequestMethod.POST)
	@ResponseBody
	public Map checkSize(HttpServletRequest request, HttpServletResponse response, long fileSize ) {
		
		//获取数据库中限制文件上传的值
		SystemParameter systemParameter=systemParameterService.getSystemParameter("MAX_FILE_SIZE");
		String value1 = systemParameter.getValue();
		
		Map<String, String>  map = new HashMap<String, String>();
		//获取前台页面上传图片的大小
		long size = fileSize/1024;
		
		if(size > Long.valueOf(value1)){
			map.put("msg", "ok");
			map.put("valueq", value1);
			
		}else{
			map.put("msg", "fail");
		}
	return map;
	}
	
	/**
	 * 判断当前商品是否已参与其他营销活动
	 * @param request
	 * @param response
	 * @param id
	 * @return 返回'false' 表示该商品没有参加营销活动 
	 */
	@RequestMapping(value = "/isAttendActivity/{prodId}", method = RequestMethod.GET)
	@ResponseBody
	public String isAttendActivity(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {
		//团购是spu级别的，所以只要商品有一个sku参加了其他活动，都不能参加团购活动
		List<Sku> skus = skuService.getSkuListByProdId(prodId);
		for (Sku sku : skus) {
			if(!sku.getSkuType().equals(SkuActiveTypeEnum.PRODUCT.value())) {
				return SkuActiveTypeEnum.getSkuTypeMes(sku.getSkuType());
			}
		}
		return "false";
	}
}
