/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.group.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.spi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.exception.NotStocksException;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.page.CommonFrontPage;
import com.legendshop.group.page.GroupFrontPage;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.GroupStatusEnum;
import com.legendshop.model.constant.SubmitOrderStatusEnum;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.group.GroupExposerDto;
import com.legendshop.model.dto.order.AddOrderMessage;
import com.legendshop.model.entity.Group;
import com.legendshop.model.entity.Invoice;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.processor.DelayCancelOfferProcessor;
import com.legendshop.processor.ProdSnapshotProcessor;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;
import com.legendshop.util.MD5Util;

/**
 * 团购入口.
 */
@Controller
public class GroupOrderController {

	// 订单常用处理工具类
	/** 订单常用处理工具类 */
	@Autowired
	private OrderUtil orderUtil;

	@Autowired
	private SubService subService;

	@Autowired
	private GroupService groupService;

	@Autowired
	private StockService stockService;

	@Autowired
	private ProductService productService;

	@Autowired
	private InvoiceService invoiceService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private UserAddressService userAddressService;
	
	@Autowired
	private ShopDetailService shopDetailService;

	/** 订单处理器封装器 */
	@Autowired
	private OrderCartResolverManager orderCartResolverManager;
	
	/** 订单快照 **/
	@Autowired
	private ProdSnapshotProcessor prodSnapshotProcessor;
	
	/** 订单延迟取消队列 **/
	@Autowired
	private DelayCancelOfferProcessor delayCancelOfferProcessor;

	/**
	 * 团购下单.
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @param productId
	 * @param skuId
	 * @param number
	 * @return the group exposer dto
	 */
	@RequestMapping(value = "/p/checkGroup/{id}/{productId}/{skuId}", method = RequestMethod.POST)
	@ResponseBody
	public GroupExposerDto checkGroup(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Long productId,
			@PathVariable Long skuId, @RequestParam Integer number) {
		// 获取登录用户的userId
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		GroupExposerDto exposerDto = new GroupExposerDto();
		Group group = groupService.getGroup(id);
		Product item = this.productService.getProductById(productId);
		if (userId.equals(item.getUserId())) {
			exposerDto.setExpose(false);
			exposerDto.setMsg("自己的团购，不能参团");
			return exposerDto;
		}
		if (group == null) {
			exposerDto.setExpose(false);
			exposerDto.setMsg("找不到团购信息");
			return exposerDto;
		}
		if (GroupStatusEnum.ONLINE.value().intValue() != group.getStatus()) {
			exposerDto.setExpose(false);
			exposerDto.setMsg("团购未上线");
			return exposerDto;
		}
		Date startTime = group.getStartTime();
		Date endTime = group.getEndTime();
		Date now = new Date();
		if (now.getTime() >= endTime.getTime()) { // 活动已经过期
			exposerDto.setExpose(false);
			exposerDto.setMsg("团购活动已经过期");
			return exposerDto;
		} else if (now.getTime() < startTime.getTime()) { // 距离活动开始时间
			exposerDto.setExpose(false);
			exposerDto.setMsg("团购活动未开始");
			return exposerDto;
		}
		/*
		 * 1.付款减库存 简单的检查商品库存够不够
		 */
		Integer stocks = stockService.getStocksByMode(productId, skuId);
		if (stocks - number <= 0) {
			exposerDto.setExpose(false);
			exposerDto.setMsg("团购商品库存不足,请选择其他团购商品");
			return exposerDto;
		}
		if (group.getBuyQuantity() != null && group.getBuyQuantity() > 0) {
			if (number > group.getBuyQuantity()) {
				exposerDto.setExpose(false);
				exposerDto.setMsg("超出活动限购数");
				return exposerDto;
			}
			/**
			 * 查找用户曾经下过的团购单，检查这个用户是否超出限购数量
			 */
			Integer count = subService.findUserOrderGroup(userId, productId, skuId, id);
			if ((count + number) > group.getBuyQuantity()) {
				exposerDto.setExpose(false);
				exposerDto.setMsg("超出活动限购数");
				return exposerDto;
			}
		}
		StringBuffer buffer = new StringBuffer().append(id).append("_").append(productId).append("_").append(skuId);
		String _md5 = MD5Util.toMD5(buffer.toString() + Constants._MD5);
		exposerDto.setToken(_md5);
		exposerDto.setExpose(true);
		exposerDto.setMsg("检测通过");
		exposerDto.setId(id);
		request.getSession().setAttribute("GROUP_TOKEN", _md5);
		request.getSession().setAttribute("GROUP_NUMBER", number);
		return exposerDto;
	}

	/**
	 * PC端团购下单.
	 *
	 * @param request
	 * @param response
	 * @param groupId
	 * @param productId
	 * @param skuId
	 * @return the string
	 */
	@RequestMapping(value = "/p/group/orderDetails/{groupId}/{productId}/{skuId}", method = RequestMethod.POST)
	public String orderDetails(HttpServletRequest request, HttpServletResponse response, @PathVariable Long groupId, @PathVariable Long productId,
			@PathVariable Long skuId) {
		// 获取登录用户的userId
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		StringBuffer buffer = new StringBuffer().append(groupId).append("_").append(productId).append("_").append(skuId);
		String _md5 = MD5Util.toMD5(buffer.toString() + Constants._MD5);
		String GROP_TOKEN = (String) request.getSession().getAttribute("GROUP_TOKEN");
		if (!_md5.equals(GROP_TOKEN)) {
			request.setAttribute("message", "非法访问");
			return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
		}
		Integer number = (Integer) request.getSession().getAttribute("GROUP_NUMBER");
		if (number == null || number <= 0) {
			return PathResolver.getRedirectPath("/group/view/" + groupId);
		}

		/* 根据类型查询订单列表 */
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("groupId", groupId);
		params.put("productId", productId);
		params.put("skuId", skuId);
		params.put("number", number);
		params.put("userId", userId);
		params.put("userName", userName);

		UserShopCartList shopCartList = orderCartResolverManager.queryOrders(CartTypeEnum.GROUP, params);
		if (AppUtils.isBlank(shopCartList)) {
			return PathResolver.getRedirectPath("/group/view/" + groupId);
		}
		if (shopCartList == null || AppUtils.isBlank(shopCartList.getShopCarts())) {
			return PathResolver.getRedirectPath("/group/view/" + groupId);
		}

		shopCartList.setUserId(userId);
		shopCartList.setUserName(userName);

		// 查出买家的默认发票内容
		Invoice userdefaultInvoice = invoiceService.getDefaultInvoice(userId);
		request.setAttribute("userdefaultInvoice", userdefaultInvoice);
		request.setAttribute("userShopCartList", shopCartList);
		request.setAttribute("groupId", groupId);
		
		////获取卖家是否开启发票功能
		Long shopId = shopCartList.getShopCarts().get(0).getShopId();
	    ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
	    
	    if (AppUtils.isBlank(shopDetail.getSwitchInvoice())) {
	    	Integer switchInvoice = 0;
	    	request.setAttribute("switchInvoice",switchInvoice);
		}
	    request.setAttribute("switchInvoice", shopDetail.getSwitchInvoice());
		
		// 设置订单提交令牌,防止重复提交
		Integer token = CommonServiceUtil.generateRandom();
		request.getSession().setAttribute(Constants.TOKEN, token);
		subService.putUserShopCartList(CartTypeEnum.GROUP.toCode(), userId, shopCartList);
		return PathResolver.getPath(GroupFrontPage.ORDERDETAILS);

	}

	/**
	 * PC端提交订单.
	 *
	 * @param request
	 * @param response
	 * @param groupId
	 * @param productId
	 * @param skuId
	 * @param remarkText
	 * @param invoiceId
	 * @param delivery
	 * @param token
	 * @return the adds the order message
	 */
	@RequestMapping(value = "/p/group/submitOrder/{groupId}/{productId}/{skuId}", method = RequestMethod.POST)
	public @ResponseBody AddOrderMessage submitOrder(HttpServletRequest request, HttpServletResponse response, @PathVariable Long groupId,
			@PathVariable Long productId, @PathVariable Long skuId, @RequestParam String remarkText, @RequestParam Long invoiceId,
			@RequestParam String delivery, @RequestParam String token,@RequestParam Integer switchInvoice) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		
		StringBuffer buffer = new StringBuffer().append(groupId).append("_").append(productId).append("_").append(skuId);
		String _md5 = MD5Util.toMD5(buffer.toString() + Constants._MD5);
		String PRESELL_TOKEN = (String) request.getSession().getAttribute("GROUP_TOKEN");
		AddOrderMessage message = new AddOrderMessage();
		message.setResult(false);
		if (!_md5.equals(PRESELL_TOKEN)) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("非法访问");
			return message;
		}

		// TODO 校验用户参数，SpringSession后台过期机制有问题
		if (!userDetailService.isUserExist(userName)) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("用户不存在！");
			return message;
		}

		Integer number = (Integer) request.getSession().getAttribute("GROUP_NUMBER");
		if (number == null || number <= 0) {
			message.setCode(SubmitOrderStatusEnum.NOT_PRODUCTS.value());
			return message;
		}
		Integer sessionToken = (Integer) request.getSession().getAttribute(Constants.TOKEN); // 取session中保存的token
		Integer userToken = Integer.parseInt(token); // 取用户提交过来的token进行对比
		if(AppUtils.isBlank(userToken)){
			message.setCode(SubmitOrderStatusEnum.NULL_TOKEN.value());
			return message;
		}
		if (!userToken.equals(sessionToken)) {
			message.setCode(SubmitOrderStatusEnum.INVALID_TOKEN.value());
			return message;
		}
		// get cache
		UserShopCartList userShopCartList = subService.findUserShopCartList(CartTypeEnum.GROUP.toCode(), userId);
		if (AppUtils.isBlank(userShopCartList)) {
			message.setCode(SubmitOrderStatusEnum.NOT_PRODUCTS.value());
			return message;
		}

		// 检查金额： 如果促销导致订单金额为负数
		if (userShopCartList.getOrderActualTotal() < 0) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("订单金额为负数,下单失败!");
			return message;
		}

		ShopCarts shopCarts = userShopCartList.getShopCarts().get(0);
		shopCarts.setRemark(remarkText);
		/** 根据id获取收货地址信息 **/
		UserAddress userAddress = userAddressService.getDefaultAddress(userId);
		if (AppUtils.isBlank(userAddress)) {
			message.setCode(SubmitOrderStatusEnum.NO_ADDRESS.value());
			return message;
		}
		userShopCartList.setUserAddress(userAddress);

		// 处理运费模板
		String result = orderUtil.handlerDelivery(delivery, userShopCartList);
		if (!Constants.SUCCESS.equals(result)) {
			message.setCode(result);
			return message;
		}

		// 验证成功，清除令牌
		request.getSession().removeAttribute(Constants.TOKEN);
		userShopCartList.setUserId(userId);
		userShopCartList.setUserName(userName);
		userShopCartList.setInvoiceId(invoiceId);
		userShopCartList.setUserAddress(userAddress);
		userShopCartList.setPayManner(2);
		userShopCartList.setType(CartTypeEnum.GROUP.toCode());
		userShopCartList.setActiveId(groupId);
	
		userShopCartList.setSwitchInvoice(switchInvoice);
		/**
		 * 处理订单
		 */
		List<String> subIds = null;
		try {
			subIds = orderCartResolverManager.addOrder(CartTypeEnum.GROUP, userShopCartList);
		} catch (NotStocksException e) {
			message.setCode(SubmitOrderStatusEnum.UNDERSTOCK.value());
			message.setMessage(e.getMessage());
			return message;
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof BusinessException) {
				message.setCode(SubmitOrderStatusEnum.ERR.value());
				message.setMessage(e.getMessage());
				return message;
			}
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("下单失败,请联系商城管理员或商城客服");
			return message;
		}
		if (AppUtils.isBlank(subIds)) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("下单失败,请联系商城管理员或商城客服");
			return message;
		} else {

			// offer 延时取消队列
			delayCancelOfferProcessor.process(subIds);

			// 发送网站快照备份
			prodSnapshotProcessor.process(subIds);
		}

		subService.evictUserShopCartCache(CartTypeEnum.GROUP.toCode(), userId);
		request.getSession().removeAttribute("GROUP_NUMBER");
		request.getSession().removeAttribute("GROUP_TOKEN");
		StringBuffer url = new StringBuffer();
		url.append("/p/orderSuccess?subNums=");
		for (int i = 0; i < subIds.size(); i++) {
			url.append(subIds.get(i)).append(",");
		}
		String path = url.substring(0, url.length() - 1);
		message.setResult(true);
		message.setUrl(path);
		return message;
	}

}
