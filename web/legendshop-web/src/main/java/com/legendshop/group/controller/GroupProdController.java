/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.group.controller;

import com.legendshop.base.xssfilter.XssDtoFilter;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.page.CommonFrontPage;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.group.page.GroupFrontPage;
import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.dto.group.GroupDto;
import com.legendshop.model.dto.group.GroupProdSeachParam;
import com.legendshop.model.dto.group.JsonDto;
import com.legendshop.model.entity.*;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.*;
import com.legendshop.util.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

/**
 * 团购控制器.
 */
@Controller
@RequestMapping("/group")
public class GroupProdController extends BaseController {

	private final Logger log = LoggerFactory.getLogger(GroupProdController.class);

	@Autowired
	private GroupService groupService;

	@Autowired
	private ShopDetailService shopDetailService;
	
	@Autowired
	private ProductCommentStatService productCommentStatService;

	@Autowired
	private ShopCommStatService shopCommStatService;

	@Autowired
	private DvyTypeCommStatService dvyTypeCommStatService;

	@Autowired
	private UserDetailService userDetailService;
	/**
	 * 团购产品查询页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/list")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Integer firstCid, Integer twoCid, Integer thirdCid) {
		PageSupport<Group> ps = groupService.queryGroupPage(curPageNO, firstCid, twoCid, thirdCid);
		PageSupportHelper.savePage(request, ps);
		List<Category> categorys = groupService.findgroupCategory();
		request.setAttribute("categorys", categorys);
		return PathResolver.getPath(GroupFrontPage.GROUP_PROD_LIST);
	}
	
	/**
	 * 团购列表 for ajax
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param searchParams
	 * @return
	 */
	@RequestMapping("/groupList")
	public String groupList(HttpServletRequest request, HttpServletResponse response, String curPageNO, GroupProdSeachParam searchParams) {

		searchParams = XssDtoFilter.safeProductSearchParms(searchParams);

		PageSupport<Group> ps = groupService.queryGroupList(curPageNO, searchParams);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("searchParams", searchParams);
		request.setAttribute("_page", curPageNO);
		String path = PathResolver.getPath(GroupFrontPage.GROUP_LIST);
		return path;
	}

	/**
	 * 团购详情
	 * 
	 * @param request
	 * @param response
	 * @param categoryId
	 * @return
	 */
	@RequestMapping("/view/{id}")
	public String view(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		GroupDto groupdto = groupService.getGroupDto(id);
		
		//团购活动审核中(-1)、下线(0)时
		if (AppUtils.isBlank(groupdto)) {
			return PathResolver.getPath(CommonFrontPage.PROD_NON_EXISTENT);
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		//获取商家
		Long shopId = groupdto.getShopId();
		ShopDetail shopDetail = shopDetailService.getShopDetailByIdNoCache(shopId);
		if(AppUtils.isBlank(shopDetail) || shopDetail.getStatus() != ShopStatusEnum.NORMAL.value()){
			return PathResolver.getPath(CommonFrontPage.PROD_NON_EXISTENT);
		}

		//计算该店铺所有已评分商品的平均分
		Float shopscore=0f;
		Float prodscore=0f;
		Float logisticsScore=0f;
		DecimalFormat decimalFormat = new DecimalFormat("0.0");
		decimalFormat.setRoundingMode(RoundingMode.FLOOR);

		//计算该店铺所有已评分商品的平均分
		ProductCommentStat prodCommentStat = productCommentStatService.getProductCommentStatByShopId(shopId);
		if(prodCommentStat!=null&&prodCommentStat.getScore()!=null&&prodCommentStat.getComments()!=null){
			prodscore=(float)prodCommentStat.getScore()/prodCommentStat.getComments();
		}
		request.setAttribute("prodAvg",decimalFormat.format(prodscore));

		//计算该店铺所有已评分店铺的平均分
		ShopCommStat shopCommentStat = shopCommStatService.getShopCommStatByShopId(shopId);
		if(shopCommentStat!=null&&shopCommentStat.getScore()!=null&&shopCommentStat.getCount()!=null){
			shopscore=(float)shopCommentStat.getScore()/shopCommentStat.getCount();
		}
		request.setAttribute("shopAvg",decimalFormat.format(shopscore));
		//计算该店铺所有已评分物流的平均分
		DvyTypeCommStat  dvyTypeCommStat = dvyTypeCommStatService.getDvyTypeCommStatByShopId(shopId);
		if(dvyTypeCommStat!=null&&dvyTypeCommStat.getScore()!=null&&dvyTypeCommStat.getCount()!=null){
			logisticsScore=(float)dvyTypeCommStat.getScore()/dvyTypeCommStat.getCount();
		}
		request.setAttribute("dvyAvg",decimalFormat.format(logisticsScore));
		Float sum=0f;
		if (shopscore!=0&&prodscore!=0&&logisticsScore!=0) {
			sum=(float)Math.floor((shopscore+prodscore+logisticsScore)/3);
		}else if(shopscore!=0&&logisticsScore!=0){
			sum=(float)Math.floor((shopscore+logisticsScore)/2);
		}
		request.setAttribute("sum",decimalFormat.format(sum));
		request.setAttribute("shopDetail", shopDetail);
		request.setAttribute("groupdto", groupdto);
		if (UserManager.isUserLogined(user)) {
			request.setAttribute("loginUser", user.getUsername());
		}

		if(AppUtils.isNotBlank(user)){
			UserDetail userInfo = userDetailService.getUserDetailById(user.getUserId());
			if(AppUtils.isNotBlank(userInfo)){
				String password = userInfo.getUserName()+"123456";
				request.setAttribute("user_name",userInfo.getUserName());
				request.setAttribute("user_pass", password);
			}
		}

		return PathResolver.getPath(GroupFrontPage.GROUP_DETAIL);
	}

	/**
	 * 获取当前时间
	 * 
	 * @return
	 */
	@RequestMapping(value = "/time/now", method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public JsonDto<Date> getTime() {
		return new JsonDto<Date>(true, new Date());
	}
}
