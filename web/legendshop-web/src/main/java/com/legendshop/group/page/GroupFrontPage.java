/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.group.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 前台页面定义.
 */
public enum GroupFrontPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 团购列表页面 */
	GROUP_LIST_PAGE("/shop/groupList"),

	/** 添加团购活动 */
	GROUP_PAGE("/shop/group"),

	/** 商品信息 */
	GROUP_PROD("/shop/groupProd"),

	/** 团购详情 */
	GROUP_VIEW("/shop/groupView"),

	/** 团购列表 */
	GROUP_PROD_LIST("/groupList"),

	/** 团购商品列表 */
	GROUP_LIST("/groupProdList"),

	/** 团购详情 */
	GROUP_DETAIL("/groupDetail"),

	/** 团购订单详情 */
	ORDERDETAILS("/groupOrderDetail"),
	
	/** 团购运营数据 */
	GROUP_OPERATION("/groupOperation"),

	;

	/** The value. */
	private final String value;

	/**
	 * 实例化一个新的团购前台页面
	 * 
	 * @param value
	 * @param value
	 */
	private GroupFrontPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */

	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String, java.util.List)
	 */

	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath("group", path);
	}

	public String getNativeValue() {
		return value;
	}

}
