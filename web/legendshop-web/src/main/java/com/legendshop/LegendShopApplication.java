/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop;

import com.legendshop.config.PropertiesUtil;
import com.legendshop.framework.event.EventHome;
import com.legendshop.framework.event.SysInitEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.util.AppUtils;

/**
 * 系统启动类.
 */
@EnableCaching
@EnableWebSecurity
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class,
		RedisAutoConfiguration.class, AopAutoConfiguration.class,
		AopAutoConfiguration.class}, scanBasePackages = {"com.legendshop"})
@ImportResource({"classpath:spring/applicationContext.xml"})
public class LegendShopApplication {

	/**
	 * 日志.
	 */
	private final static Logger log = LoggerFactory.getLogger(LegendShopApplication.class);

	/**
	 * 主方法
	 */
	public static void main(String[] args) {
		SpringApplication.run(LegendShopApplication.class, args);
		ApplicationContext ctx = ContextServiceLocator.getApplicationContext();
		printBeanFactory(ctx);
		System.out.println("LegendShopApplication 启动成功！");
		PropertiesUtil propertiesUtil = (PropertiesUtil)ctx.getBean("propertiesUtil");
		// 4. 发送系统启动事件
		EventHome.publishEvent(new SysInitEvent(propertiesUtil.getPcDomainName()));
		// 将hook线程添加到运行时环境中去
		Runtime.getRuntime().addShutdownHook(new CleanWorkThread());
	}


	/**
	 * 打印spring context
	 *
	 * @param ctx
	 */
	private static void printBeanFactory(ApplicationContext ctx) {
		if (log.isWarnEnabled()) {
			int i = 0;
			String[] beans = ctx.getBeanDefinitionNames();
			StringBuffer sb = new StringBuffer("系统配置的Spring Bean [ \n");
			if (AppUtils.isNotBlank(beans)) {
				for (String bean : beans) {
					sb.append(++i).append(" ").append(bean).append("\n");
				}
				sb.append(" ]");
				log.warn(sb.toString());
			}
		}

	}

	/**
	 * hook线程,关闭时调用
	 */
	static class CleanWorkThread extends Thread {
		@Override
		public void run() {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			log.info(" LegendShop Frontend System shutdowned.");
		}
	}


}
