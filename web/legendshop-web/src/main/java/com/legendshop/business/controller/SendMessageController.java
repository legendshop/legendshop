/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.model.entity.User;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.manager.MailManager;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;

/**
 * 短信发送Controller
 * 
 */
@Controller
@RequestMapping("/send")
public class SendMessageController extends BaseController {
	
	@Autowired
	private MailManager mailManager;

	@Autowired
	private UserDetailService userDetailService;
	
	/**
	 * 发送短信
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/editsms")
	public String editsms(HttpServletRequest request, HttpServletResponse response) {
		return "/pages/common/smssender";
	}
	
	/**
	 * 发送短信
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/editmail")
	public String editmail(HttpServletRequest request, HttpServletResponse response) {
		return "/pages/common/mailsender";
	}
	
	@RequestMapping(value = "/mail", method = RequestMethod.POST)
	public @ResponseBody String sendSMS(HttpServletRequest request, HttpServletResponse response,String mail) {
		//获得随机码
		String validateCode = CommonServiceUtil.getRandomString(50);
		
		SecurityUserDetail securityUserDetail = UserManager.getUser(request);
		if(AppUtils.isNotBlank(securityUserDetail)){
			User user = new User();
			user.setName(securityUserDetail.getUsername());
			String nickName = UserManager.getNickName(securityUserDetail);
			
			user.setNickName(nickName);
			//发送邮件到邮箱去验证
			mailManager.updateMail(user,mail,validateCode);
			
			//将邮箱及验证码 更新到 ls_usr_security
			userDetailService.updateSecurityMail(securityUserDetail.getUsername(),validateCode,mail);
			
			return "异步发送邮件成功";
		}else{
			return "异步发送邮件失败";
		}
	}
	
}
