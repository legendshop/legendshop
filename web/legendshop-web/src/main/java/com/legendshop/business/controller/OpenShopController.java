/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.page.FrontPage;
import com.legendshop.business.page.RedirectPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.FunctionEnum;
import com.legendshop.model.constant.ShopOPstatusEnum;
import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.constant.ShopTypeEnum;
import com.legendshop.model.entity.ShopAudit;
import com.legendshop.model.entity.ShopCompanyDetail;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.SystemConfig;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ShopCompanyDetailService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.SystemConfigService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
/**
 * 
 * 开店
 *
 */
@Controller
@RequestMapping("/p")
public class OpenShopController extends BaseController {
	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private ShopCompanyDetailService shopCompanyDetailService;

	@Autowired
	private SystemConfigService systemConfigService;
	
	@Autowired
	private AttachmentManager attachmentManager;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;


	/**
	 * 协议页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/shopAgreement")
	public String shopAgreement(HttpServletRequest request, HttpServletResponse response) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		// 用户必须登录，不再用spring security来限制，因为买家和商家一起访问该页面
		if (!UserManager.hasFunction(user, FunctionEnum.FUNCTION_USER.value())) {
			return PathResolver.getPath(FrontPage.LOGIN);
		}
		
		//判断是否支持开店，不支持开店，给出友好提示
		Boolean openShop = systemParameterUtil.isSupportOpenShop();
		
		if(!openShop){
			request.setAttribute(Constants.MESSAGE, "暂未开放开店功能！详情请联系后台客服或管理员！");
			return  PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}
		
		// 判断用户是否已开店
		String userId = user.getUserId();
		
		ShopDetail detail = shopDetailService.getShopDetailByUserId(userId);
		
		if (AppUtils.isBlank(detail)) {// 未开店
			return PathResolver.getPath(FrontPage.SHOP_AGREEMENT);// 调到确认协议页面
		} else {
			if (ShopStatusEnum.NORMAL.value().equals(detail.getStatus())) { // 审核成功后提醒
				if (UserManager.isShopUser(user)) {
					return PathResolver.getPath(RedirectPage.SHOP_SETTING);
				}
			}
		}
		request.setAttribute("shopDetailInfo", detail);
		request.setAttribute("shopType", detail.getType());
		request.setAttribute("shopId", detail.getShopId());
		if (detail.getType().equals(ShopTypeEnum.BUSINESS.value())) {
			ShopCompanyDetail companyDetail = shopCompanyDetailService.getShopCompanyDetailByShopId(detail.getShopId());
			request.setAttribute("shopCompanyDetail", companyDetail);
		}
		List<ShopAudit> shopAuditList = shopDetailService.getShopAuditInfo(detail.getShopId());
		request.setAttribute("shopAuditList", shopAuditList);

		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		request.setAttribute("systemConfig", systemConfig);
		
		// 页面状态不正常 (根据店铺状态或者开店流程状态跳转到对应的页面)
		return jumpPage(request, response, detail.getStatus(), detail.getOpStatus(), detail.getType());
	}

	/**
	 * 选择开店类型页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/selectShopType")
	public String selectShopType(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(FrontPage.SELECTSHOPTYPE);
	}

	/**
	 * 跳转到入驻须知节点页面
	 * 
	 * @param request
	 * @param response
	 * @param shopType
	 * @return
	 */
	@RequestMapping("/settledAgreement")
	public String settledAgreement(HttpServletRequest request, HttpServletResponse response, Integer shopType) {
		if (AppUtils.isBlank(shopType)) {
			throw new BusinessException("shopType is null");
		}
		request.setAttribute("shopType", shopType);
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		request.setAttribute("systemConfig", systemConfig);
		return PathResolver.getPath(FrontPage.SETTLEDAGREEMENT);
	}

	/**
	 * 跳转联系人页面
	 * 
	 * @param request
	 * @param response
	 * @param shopType
	 * @return
	 */
	@RequestMapping("/contactInfo")
	public String contactInfo(HttpServletRequest request, HttpServletResponse response, Integer shopType) {
		if (AppUtils.isBlank(shopType)) {
			throw new BusinessException("type is null");
		}
		if (!ShopTypeEnum.BUSINESS.value().equals(shopType) && !ShopTypeEnum.PERSONAL.value().equals(shopType)) {
			throw new BusinessException("shoptype error");
		}
		request.setAttribute("shopType", shopType);
		return PathResolver.getPath(FrontPage.CONTACTINFO);
	}

	/**
	 * 保存联系人信息
	 * 
	 * @param request
	 * @param response
	 * @param shopDetail
	 * @return
	 */
	@RequestMapping(value = "/saveContactInfo", method = RequestMethod.POST)
	public String saveContactInfo(HttpServletRequest request, HttpServletResponse response, ShopDetail shopDetail) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(FrontPage.LOGIN);
		}
		if (AppUtils.isBlank(shopDetail.getType()) || AppUtils.isBlank(shopDetail.getContactName()) || AppUtils.isBlank(shopDetail.getContactMobile())
				|| AppUtils.isBlank(shopDetail.getContactMail())) {
			return PathResolver.getPath(FrontPage.ERROR);
		}
		ShopDetail oldShopDetail = shopDetailService.getShopDetailByUserId(user.getUserId());
		Long shopId = null;
		if (AppUtils.isBlank(oldShopDetail)) {
			shopId = shopDetailService.saveContactInfo(shopDetail, user.getUserId(), user.getUsername());
			if (AppUtils.isNotBlank(shopId)) {
				request.setAttribute("shopId", shopId);
				request.setAttribute("shopType", shopDetail.getType());
			} else {
				throw new BusinessException("shopid is null");
			}
		} else {
			if (ShopStatusEnum.OFFLINE.value().equals(shopDetail.getStatus())) {
				return PathResolver.getPath(FrontPage.ERROR);
			}
			shopId = oldShopDetail.getShopId();
			shopDetailService.updateContactInfo(oldShopDetail, shopDetail);
		}
		return PathResolver.getRedirectPath("/p/to_shopdetail/" + shopDetail.getType());
	}

	
	/**
	 * 选择开店类型
	 * @param request
	 * @param response
	 * @param type
	 * @return
	 */
	@RequestMapping(value = "/to_shopdetail/{type}", method = RequestMethod.GET)
	public String to_shopdetail(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer type) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(FrontPage.LOGIN);
		}

		ShopDetail detail = shopDetailService.getTypeShopDetailByUserId(user.getUserId());
		request.setAttribute("shopDetailInfo", detail);

		if (!ShopTypeEnum.PERSONAL.value().equals(type) && !ShopTypeEnum.BUSINESS.value().equals(type)) {
			return PathResolver.getPath(FrontPage.ERROR);
		}

		if (ShopTypeEnum.PERSONAL.value().equals(type)) {
			request.setAttribute("shopType", type);
			return PathResolver.getPath(FrontPage.SHOPDETAIL);
		} else if (ShopTypeEnum.BUSINESS.value().equals(type)) {
			request.setAttribute("shopType", type);
			return PathResolver.getPath(FrontPage.COMPANYSHOPDETAIL);
		}
		return null;
	}

	/**
	 * 上一步操作
	 * 
	 * @param request
	 * @param response
	 * @param op
	 * @param step
	 * @return
	 */
	@RequestMapping(value = "/shopPrevious")
	public String shopPrevious(HttpServletRequest request, HttpServletResponse response, String op, Long step) {

		if (!op.equals("back") || AppUtils.isBlank(step)) {
			throw new BusinessException("option error");
		}
		SecurityUserDetail user = UserManager.getUser(request);
		
		ShopDetail shopDetail = shopDetailService.getShopDetailByUserId(user.getUserId());
		if (AppUtils.isBlank(shopDetail) || ShopStatusEnum.OFFLINE.value().equals(shopDetail.getStatus())) {
			return PathResolver.getPath(FrontPage.ERROR);
		}
		/*if (step != 2) {
			request.setAttribute("shopDetailInfo", shopDetail);
			request.setAttribute("shopType", shopDetail.getType());
		}*/

		if (step == 1l) {
			return PathResolver.getPath(FrontPage.CONTACTINFO);
		} else if (step == 2l) {
			ShopCompanyDetail shopCompanyDetail = shopCompanyDetailService.getShopCompanyDetailByShopId(shopDetail.getShopId());
			request.setAttribute("shopCompanyDetail", shopCompanyDetail);
			return PathResolver.getPath(FrontPage.COMPANYINFO);
		} else if (step == 3l) {
			request.setAttribute("shopDetailInfo", shopDetail);
			request.setAttribute("shopType", shopDetail.getType());
			return PathResolver.getPath(FrontPage.SHOPDETAIL);
		} else if(step == 4l){
			ShopCompanyDetail shopCompanyDetail = shopCompanyDetailService.getShopCompanyDetailByShopId(shopDetail.getShopId());
			if(AppUtils.isNotBlank(shopCompanyDetail)){
				shopDetail.setCompanyName(shopCompanyDetail.getCompanyName());
				shopDetail.setLicenseNumber(shopCompanyDetail.getLicenseNumber());
				shopDetail.setLicensePicFilePath(shopCompanyDetail.getLicensePic());
				request.setAttribute("shopDetailInfo", shopDetail);
				request.setAttribute("shopType", shopDetail.getType());
				return PathResolver.getPath(FrontPage.COMPANYSHOPDETAIL);
			}else{
				request.setAttribute("shopDetailInfo", shopDetail);
				request.setAttribute("shopType", shopDetail.getType());
				return PathResolver.getPath(FrontPage.SHOPDETAIL);
			}
		}
		return null;
	}

	/**
	 * 保存公司信息
	 * 
	 * @param request
	 * @param response
	 * @param shopCompanyDetail
	 * @return
	 */
	@RequestMapping(value = "/saveCompanyInfo", method = RequestMethod.POST)
	public String saveCompanyInfo(HttpServletRequest request, HttpServletResponse response, ShopCompanyDetail shopCompanyDetail) {
		SecurityUserDetail user = UserManager.getUser(request);
		ShopDetail oldShopdetail = shopDetailService.getShopDetailByUserId(user.getUserId());
		if (AppUtils.isBlank(oldShopdetail) || ShopStatusEnum.OFFLINE.value().equals(oldShopdetail.getStatus())) {
			return PathResolver.getPath(FrontPage.ERROR);
		}
		ShopCompanyDetail companyDetail = shopCompanyDetailService.getShopCompanyDetailByShopId(oldShopdetail.getShopId());
		boolean result;
		String legalIdCard = null;
		String licensePics = null;
		
		//上传文件要放在数据库操作之前
		if (shopCompanyDetail.getLegalIdCardPic().getSize() > 0) {
			legalIdCard = attachmentManager.upload(shopCompanyDetail.getLegalIdCardPic());
		}
		
		if (shopCompanyDetail.getLicensePics().getSize() > 0) {
			licensePics = attachmentManager.upload(shopCompanyDetail.getLicensePics());
		}
		
		if (AppUtils.isBlank(companyDetail)) {
			result = shopDetailService.saveCompanyInfo(oldShopdetail, shopCompanyDetail,legalIdCard, licensePics);
		} else {
			result = shopDetailService.updateCompanyInfo(oldShopdetail.getUserName(),oldShopdetail.getUserId(),oldShopdetail.getShopId(), companyDetail, shopCompanyDetail,legalIdCard, licensePics);
		}
		if (!result)
			throw new BusinessException("save || update CompanyInfo fail");
		return PathResolver.getRedirectPath("/p/to_certificateInfo");
	}

	/**
	 * 跳转至公司银行及税务提交
	 * 
	 * @param request
	 * @param response
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/to_certificateInfo", method = RequestMethod.GET)
	public String to_certificateInfo(HttpServletRequest request, HttpServletResponse response) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		ShopDetail oldShopdetail = shopDetailService.getShopDetailByUserId(userId);
		if (AppUtils.isBlank(oldShopdetail) || ShopStatusEnum.OFFLINE.value().equals(oldShopdetail.getStatus())
				|| !ShopTypeEnum.BUSINESS.value().equals(oldShopdetail.getType())) {
			return PathResolver.getPath(FrontPage.ERROR);
		}
		ShopCompanyDetail companyDetail = shopCompanyDetailService.getShopCompanyDetailByShopId(oldShopdetail.getShopId());
		request.setAttribute("shopCompanyDetail", companyDetail);
		return PathResolver.getPath(FrontPage.CERTIFICATEINFO);
	}

	/**
	 * 保存公司银行及税务
	 * 
	 * @param request
	 * @param response
	 * @param shopCompanyDetail
	 * @return
	 */
	@RequestMapping(value = "/saveCertificateInfo", method = RequestMethod.POST)
	public String saveCertificateInfo(HttpServletRequest request, HttpServletResponse response, ShopCompanyDetail shopCompanyDetail) {
		SecurityUserDetail user = UserManager.getUser(request);
		String taxRegistrationNumE = null;
		String bankPermitPic = null;
		ShopDetail shopdetail = shopDetailService.getShopDetailByUserId(user.getUserId());
		if (AppUtils.isBlank(shopdetail) || ShopStatusEnum.OFFLINE.value().equals(shopdetail.getStatus())
				|| !ShopTypeEnum.BUSINESS.value().equals(shopdetail.getType())) {
			return PathResolver.getPath(FrontPage.ERROR);
		}
		if (shopCompanyDetail.getCodeType().equals(1l)) {
			if (shopCompanyDetail.getTaxRegistrationNumEPic().getSize() > 0) {
				taxRegistrationNumE = attachmentManager.upload(shopCompanyDetail.getTaxRegistrationNumEPic());
			}
		}
		if (shopCompanyDetail.getBankPermitPic().getSize() > 0) {
			bankPermitPic = attachmentManager.upload(shopCompanyDetail.getBankPermitPic());
		}
		boolean result = shopDetailService.saveCertificateInfo(shopdetail.getUserName(),shopdetail.getUserId(), shopdetail.getShopId(), shopCompanyDetail,taxRegistrationNumE,bankPermitPic);
		if (!result)
			throw new BusinessException("save CompanyInfo fail");
		shopDetailService.changeOptionStatus(user.getUserId(), shopdetail.getShopId(), ShopOPstatusEnum.COMPANY_INFO_OK.value());

		return PathResolver.getRedirectPath("/p/to_cmp_shopdetail");
	}

	/**
	 * 保存公司银行及税务后跳转至店铺信息
	 * 
	 * @param request
	 * @param response
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/to_cmp_shopdetail", method = RequestMethod.GET)
	public String to_cmp_shopdetail(HttpServletRequest request, HttpServletResponse response) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(FrontPage.LOGIN);
		}
		ShopDetail shopDetail = shopDetailService.getShopDetailByUserId(user.getUserId());
		if (AppUtils.isBlank(shopDetail) || ShopStatusEnum.OFFLINE.value().equals(shopDetail.getStatus())
				|| !ShopTypeEnum.BUSINESS.value().equals(shopDetail.getType())) {
			return PathResolver.getPath(FrontPage.ERROR);
		}
		ShopCompanyDetail shopCompanyDetail = shopCompanyDetailService.getShopCompanyDetailByShopId(shopDetail.getShopId());
		if (shopCompanyDetail == null) {
			return PathResolver.getPath(FrontPage.ERROR);
		}
		if (AppUtils.isBlank(shopDetail.getContactName()) || AppUtils.isBlank(shopCompanyDetail.getCompanyName())
				|| AppUtils.isBlank(shopCompanyDetail.getBankAccountName())) {
			return PathResolver.getRedirectPath("/p/selectShopType");
		}
		shopCompanyDetail.setShopId(shopDetail.getShopId());
		request.setAttribute("shopId", shopDetail.getShopId());
		request.setAttribute("shopDetailInfo", shopDetail);
		request.setAttribute("shopType", shopDetail.getType());
		request.setAttribute("shopCompanyDetail", shopCompanyDetail);
		return PathResolver.getPath(FrontPage.SHOPDETAIL);
	}


	/** 
	 * 优化流程，个人店铺信息保存
	 * @param request
	 * @param response
	 * @param shopDetail
	 * @return
	 */
	@RequestMapping(value = "/saveShopInfo", method = RequestMethod.POST)
	public String saveShopInfo(HttpServletRequest request, HttpServletResponse response, ShopDetail shopDetail) {
		SecurityUserDetail user = UserManager.getUser(request);
		ShopDetail oldShopDetail = shopDetailService.getShopDetailByUserId(user.getUserId());
		MultipartFile idCardPicFile = shopDetail.getIdCardPicFile();
		MultipartFile idCardBackPicFile = shopDetail.getIdCardBackPicFile();
		String idCardPic = null;
		String idCardBackPic = null;
		//判断是保存还是更新
		if(AppUtils.isNotBlank(oldShopDetail)){


			// 因updateShopInfo方法用户跟后台公用，以flag区分逻辑
			String flag = "user";

			//更新个人店铺信息
			int i = shopDetailService.updateShopInfo(oldShopDetail, shopDetail,flag);
			if(i<1) {
				throw new BusinessException("save shopDetai fail");
			}
		}else {
			if ((idCardPicFile != null) && (idCardPicFile.getSize() > 0)) {
				idCardPic = attachmentManager.upload(idCardPicFile);
			}
			if ((idCardBackPicFile != null) && (idCardBackPicFile.getSize() > 0)) {
				idCardBackPic = attachmentManager.upload(idCardBackPicFile);
			}
			//保存个人店铺信息
			Long shopId = shopDetailService.saveShopDetailInfo(shopDetail, user.getUsername(), user.getUserId(), user.getShopId(),idCardPic,idCardBackPic);
			if (AppUtils.isBlank(shopId)) {
				throw new BusinessException("save shopDetai fail");
			}
			shopDetailService.changeOptionStatus(user.getUserId(), shopDetail.getShopId(), ShopOPstatusEnum.SHOP_INFO_OK.value());
		}
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		request.setAttribute("systemConfig", systemConfig);
		return PathResolver.getRedirectPath("/p/checkSpeed");
	}

	/**
	 * 优化流程，企业和企业店铺信息保存
	 * @param request
	 * @param response
	 * @param shopDetail
	 * @return
	 */
	@RequestMapping(value = "/saveCompanyShopInfo", method = RequestMethod.POST)
	@Transactional
	public String saveCompanyShopInfo(HttpServletRequest request, HttpServletResponse response, ShopDetail shopDetail) {
		SecurityUserDetail user = UserManager.getUser(request);
		ShopDetail oldShopDetail = shopDetailService.getShopDetailByUserId(user.getUserId());
		MultipartFile idCardPicFile = shopDetail.getIdCardPicFile();
		MultipartFile idCardBackPicFile = shopDetail.getIdCardBackPicFile();
		String idCardPic = null;
		String idCardBackPic = null;
		String licensePics = null;
		if (shopDetail.getLicensePicFile().getSize() > 0) {
			licensePics = attachmentManager.upload(shopDetail.getLicensePicFile());
		}
		//判断是保存还是更新
		if(AppUtils.isNotBlank(oldShopDetail)){

			// 因updateShopInfo方法用户跟后台公用，以flag区分逻辑
			String flag = "user";

			//更新个人店铺和企业
			shopDetailService.updateShopInfo(oldShopDetail, shopDetail,flag);
			ShopCompanyDetail oldShopCompanyDetail = shopCompanyDetailService.getShopCompanyDetailByShopId(oldShopDetail.getShopId());
			oldShopCompanyDetail.setCompanyName(shopDetail.getCompanyName());
			oldShopCompanyDetail.setLicenseNumber(shopDetail.getLicenseNumber());
			oldShopCompanyDetail.setLicensePics(shopDetail.getLicensePicFile());
			shopCompanyDetailService.updateShopCompanyDetail(oldShopCompanyDetail);
		}else{
			if ((idCardPicFile != null) && (idCardPicFile.getSize() > 0)) {
				idCardPic = attachmentManager.upload(idCardPicFile);
			}
			if ((idCardBackPicFile != null) && (idCardBackPicFile.getSize() > 0)) {
				idCardBackPic = attachmentManager.upload(idCardPicFile);
			}
			// 先保存shopDetail获取shopId
			Long shopId = shopDetailService.saveShopDetailInfo(shopDetail,user.getUsername(), user.getUserId(), user.getShopId(),idCardPic,idCardBackPic);
			if (AppUtils.isBlank(shopId)) {
				throw new BusinessException("save shopDetai fail");
			}
			shopDetailService.changeOptionStatus(user.getUserId(), shopDetail.getShopId(), ShopOPstatusEnum.SHOP_INFO_OK.value());
			// 保存企业信息
			boolean companyResult = shopDetailService.saveCompanyInfo(shopDetail, shopId, user.getUsername(),licensePics);
			if (!companyResult) {
				throw new BusinessException("save companyShopDetai fail");
			}
		}
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		request.setAttribute("systemConfig", systemConfig);
		return PathResolver.getRedirectPath("/p/checkSpeed");
	}
	

	/**
	 * 跳转在线协议
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/to_onlineAgreement", method = RequestMethod.GET)
	public String to_onlineAgreement(HttpServletRequest request, HttpServletResponse response) {
		SecurityUserDetail user = UserManager.getUser(request);
		ShopDetail shopDetail = shopDetailService.getShopDetailByUserId(user.getUserId());
		if (AppUtils.isBlank(shopDetail) || ShopStatusEnum.OFFLINE.value().equals(shopDetail.getStatus())) {
			return PathResolver.getPath(FrontPage.ERROR);
		}
		return PathResolver.getPath(FrontPage.ONLINEAGREEMENT);
	}

	/**
	 * 店铺审核进度
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/checkSpeed")
	public String checkSpeed(HttpServletRequest request, HttpServletResponse response) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		ShopDetail shopDetail = shopDetailService.getShopDetailByUserId(userId);
		if (AppUtils.isBlank(shopDetail)) {
			return PathResolver.getPath(FrontPage.SHOP_AGREEMENT);
		}
		if (shopDetail.getStatus() == 1) { // 审核通过
			request.setAttribute("shopDetailInfo", shopDetailService.getShopDetailByUserId(userId));
			request.setAttribute("shopType", shopDetail.getType());
			return PathResolver.getPath(FrontPage.CHECKSPEED);
		}
		shopDetailService.changeOptionStatus(userId, shopDetail.getShopId(), ShopOPstatusEnum.AUDITING.value());
		shopDetailService.changeStatus(userId, shopDetail.getShopId(), ShopStatusEnum.AUDITING.value());
		request.setAttribute("shopDetailInfo", shopDetailService.getShopDetailByUserId(userId));
		request.setAttribute("shopType", shopDetail.getType());
		return PathResolver.getRedirectPath("/p/toCheckSpeed");
	}

	/**
	 * 去往店铺审核进度页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toCheckSpeed")
	public String toCheckSpeed(HttpServletRequest request, HttpServletResponse response) {
		SecurityUserDetail user = UserManager.getUser(request);
		ShopDetail shopDetail = shopDetailService.getShopDetailByUserId(user.getUserId());

		if (AppUtils.isBlank(shopDetail)) {
			return PathResolver.getPath(FrontPage.SHOP_AGREEMENT);
		}

		if(shopDetail != null) {
			List<ShopAudit> shopAuditList = shopDetailService.getShopAuditInfo(shopDetail.getShopId());
			request.setAttribute("shopAuditList", shopAuditList);
		}

		request.setAttribute("shopDetailInfo", shopDetail);
		request.setAttribute("shopType", shopDetail.getType());
		return PathResolver.getPath(FrontPage.CHECKSPEED);
	}

	/**
	 * 页面状态不正常时跳转
	 * 
	 * @param request
	 * @param response
	 * @param status
	 * @param opStatus
	 * @param type
	 * @return 跳转的URL
	 */
	private String jumpPage(HttpServletRequest request, HttpServletResponse response, Integer status, Integer opStatus, Integer type) {
		String result = null;
		// 店铺状态为下线状态跳转到店铺状态页面
		if (ShopStatusEnum.OFFLINE.value().equals(status)) {
			result = PathResolver.getPath(FrontPage.CHECKSPEED);
		}

		// 店铺关闭跳转到店铺状态页面
		if (ShopStatusEnum.CLOSED.value().equals(status)) {
			result = PathResolver.getPath(FrontPage.CHECKSPEED);
		}
		if (ShopStatusEnum.NORMAL.value().equals(status)) {
			result = PathResolver.getPath(FrontPage.CHECKSPEED);
		}
		// 联系人信息失败跳到联系人信息页面重新填写
		if (ShopStatusEnum.CONTACT_INFO_FAIL.value().equals(status)) {
			result = PathResolver.getPath(FrontPage.CHECKSPEED);

		// 公司信息失败跳到公司信息页面重新填写
		} else if (ShopStatusEnum.COMPANY_INFO_FAIL.value().equals(status)) {
			result = PathResolver.getPath(FrontPage.CHECKSPEED);

		// 店铺信息失败跳到店铺信息重新填写
		} else if (ShopStatusEnum.SHOP_INFO_FAIL.value().equals(status)) {
			result = PathResolver.getPath(FrontPage.CHECKSPEED);

		// 如果为审核中进行下面的判断
		} else if (ShopStatusEnum.AUDITING.value().equals(status)) {

			// 操作状态为联系人信息已完善则进行下面判断
			if (ShopOPstatusEnum.CONTACT_INFO_OK.value().equals(opStatus)) {

				// 店铺类型为个人就跳转到下一步——店铺信息
				if (type.equals(ShopTypeEnum.PERSONAL.value())) {
					result = PathResolver.getPath(FrontPage.SHOPDETAIL);

				// 店铺类型为商家就跳转到下一步——公司信息
				} else if (type.equals(ShopTypeEnum.BUSINESS.value())) {
					result = PathResolver.getPath(FrontPage.COMPANYINFO);
				}
			// 操作状态为公司信息已完善就则跳转到店铺信息
			} else if (ShopOPstatusEnum.COMPANY_INFO_OK.value().equals(opStatus)) {
				result = PathResolver.getPath(FrontPage.SHOPDETAIL);

			// 操作状态为公司信息已完善就则跳转到店铺信息
			} else if (ShopOPstatusEnum.SHOP_INFO_OK.value().equals(opStatus)) {
				result = PathResolver.getPath(FrontPage.ONLINEAGREEMENT);

			// 完成状态再审核
			} else if (ShopOPstatusEnum.AUDITED.value().equals(opStatus)) {
				result = PathResolver.getPath(FrontPage.CHECKSPEED);

			// 成功提交提交入驻申请
			} else if (ShopOPstatusEnum.AUDITING.value().equals(opStatus)) {
				result = PathResolver.getPath(FrontPage.CHECKSPEED);
			}
		// 如果是拒绝.则提示跳到消息页面提示用户
		} else if (ShopStatusEnum.DENY.value().equals(status)) {
			request.setAttribute(Constants.MESSAGE, "对不起,您的开店申请被管理员拒绝,请联系后台客服或管理员!");
			result = PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}
		return result;
	}

	@RequestMapping(value = "/reWrite")
	public String ReWrite(HttpServletRequest request, HttpServletResponse response, Integer op) {
		
		if (AppUtils.isBlank(op)) {
			return PathResolver.getPath(FrontPage.ERROR);
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		if (!UserManager.hasFunction(user, FunctionEnum.FUNCTION_USER.value())) {
			return PathResolver.getPath(FrontPage.LOGIN);
		}
		
		ShopDetail detail = shopDetailService.getShopDetailByUserId(userId);
		if (AppUtils.isBlank(detail)) {
			return PathResolver.getPath(FrontPage.SHOP_AGREEMENT);
		}
		request.setAttribute("shopDetailInfo", detail);
		request.setAttribute("shopType", detail.getType());
		
		return PathResolver.getRedirectPath("/p/to_shopdetail/" + detail.getType());

	}
}
