/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.business.page.FrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;

/**
 * 错误页面
 */
@Controller
public class ErrorPageController extends BaseController {

	/**
	 * 权限不足.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the string
	 */
	@RequestMapping("/insufficientAuthority")
	public String insufficientAuthority(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(FrontPage.INSUFFICIENT_AUTHORITY);
	}

}
