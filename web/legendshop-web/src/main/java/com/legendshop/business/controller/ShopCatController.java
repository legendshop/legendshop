/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.base.BaseController;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ShopCategoryService;
import com.legendshop.util.AppUtils;


/**
 * 商家分类
 *
 */
@Controller
public class ShopCatController extends BaseController{

	@Autowired
	private ShopCategoryService shopCategoryService;
	
	/**
	 * 用于填充一级商品分类的select options
	 * @param request
	 * @param response
	 * @param sortType
	 * @return
	 */
	@RequestMapping("/shopCat/loadCat")
	public @ResponseBody
	List<KeyValueEntity> loadCat(HttpServletRequest request, HttpServletResponse response) {
		SecurityUserDetail user = UserManager.getUser(request);
		if(AppUtils.isNotBlank(user) && AppUtils.isNotBlank(user.getShopId())) {
			List<KeyValueEntity> result = shopCategoryService.loadCat(user.getShopId());
			return result;
		}
		return null;
	}
	
	/**
	 * 用于填充二级商品分类的select options
	 * @param request
	 * @param response
	 * @param sortId
	 * @return
	 */
	@RequestMapping("/shopCat/loadNextCat/{shopCatId}")
	public @ResponseBody
	List<KeyValueEntity> loadNextCat(HttpServletRequest request, HttpServletResponse response, @PathVariable Long shopCatId) {
		SecurityUserDetail user = UserManager.getUser(request);
		if(AppUtils.isNotBlank(user) && AppUtils.isNotBlank(user.getShopId())) {
			return shopCategoryService.loadNextCat(shopCatId, user.getShopId());
		}
		return null;
	}
	
	/**
	 * 用于填充三级商品分类的select options
	 * @param request
	 * @param response
	 * @param nsortId
	 * @return
	 */
	@RequestMapping("/shopCat/loadSubCat/{nextCatId}")
	public @ResponseBody
	List<KeyValueEntity> loadSubCat(HttpServletRequest request, HttpServletResponse response, @PathVariable Long nextCatId) {
		SecurityUserDetail user = UserManager.getUser(request);
		if(AppUtils.isNotBlank(user) && AppUtils.isNotBlank(user.getShopId())) {
			return shopCategoryService.loadNextCat(nextCatId, user.getShopId());
		}
		return null;
	}
}
