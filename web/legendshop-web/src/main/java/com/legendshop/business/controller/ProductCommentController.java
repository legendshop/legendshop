/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.util.XssFilterUtil;
import com.legendshop.model.dto.*;
import com.legendshop.spi.service.*;
import com.legendshop.util.SafeHtml;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.legendshop.business.page.FrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductComment;
import com.legendshop.model.entity.ProductCommentCategory;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.ProductReply;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;
import com.legendshop.web.helper.IPHelper;

/**
 * 商品评论控制器.
 */
@Controller
@RequestMapping("/productcomment")
public class ProductCommentController extends BaseController {

	/**
	 * The log.
	 */
	private final Logger log = LoggerFactory.getLogger(ProductCommentController.class);

	/**
	 * The product service.
	 */
	@Autowired
	private ProductService productService;

	/**
	 * The product comment service.
	 */
	@Autowired
	private ProductCommentService productCommentService;

	@Autowired
	private ProductReplyService productReplyService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private CategoryManagerService categoryManagerService;

	@Autowired
	private ShopDetailService shopDetailService;

	/**
	 * Update.
	 *
	 * @param prodId the prod id
	 * @return the string
	 * @throws Exception the exception
	 */
	@RequestMapping("/update/{prodId}")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) throws Exception {
		ProductDetail prod = productService.getProdDetail(prodId);
		request.setAttribute("prod", prod);
		return PathResolver.getPath(FrontPage.PRODUCT_COMMENT);
	}

	/**
	 * 点赞
	 *
	 * @param prodComId
	 * @return
	 */
	@RequestMapping(value = "/updateUsefulCounts", method = RequestMethod.POST)
	public @ResponseBody
	String updateUsefulCounts(HttpServletRequest request, HttpServletResponse response, Long prodComId) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		if (AppUtils.isBlank(userId)) {
			return "LOGIN";
		}
		try {
			ProductComment productComment = productCommentService.getProductCommentById(prodComId);

			if (null == productComment) {
				return "亲, 您要操作的评论不存在!";
			}

			if (productCommentService.isAlreadyUseful(prodComId, userId)) {
				return "亲, 您已经点赞过了!";
			}

			int result = productCommentService.updateUsefulCounts(prodComId, userId);
			if (result == 0) {
				return Constants.FAIL;
			}
			return Constants.SUCCESS;
		} catch (Exception e) {
			log.error("updateUsefulCounts", e);
			return Constants.FAIL;
		}
	}

	/**
	 * 查询评论的回复列表
	 *
	 * @param prodComId
	 * @return
	 */
	@RequestMapping(value = "/productCommentDetail", method = RequestMethod.GET)
	public String queryProductCommentsDetail(HttpServletRequest request, HttpServletResponse response,
											 @RequestParam Long prodComId, String curPageNO) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = "";
		if (AppUtils.isNotBlank(user)) {
			userId = user.getUserId();
			request.setAttribute("loginUserName", user.getUsername());
		}


		//查询评价信息
		ProductCommentDto productComment = productCommentService.getProductCommentDetail(prodComId, userId);

		if (null == productComment) {
			request.setAttribute(Constants.MESSAGE, "对不起, 您要查看的评论不存在!");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}

		//查询商品信息
		Product product = productService.getProductById(productComment.getProdId());
		if (null == product) {
			request.setAttribute(Constants.MESSAGE, "对不起, 您要查看的评论对应的商品不存在!");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}

		//查询该分类
		if (AppUtils.isNotBlank(product.getCategoryId())) {
			List<TreeNode> treeNodes = categoryManagerService.getTreeNodeNavigation(product.getCategoryId(), ",");
			request.setAttribute("treeNodes", treeNodes);
		}

		//查询当前评价回复列表
		PageSupport<ProductCommentReplyDto> ps = productReplyService.queryProductReplyList(prodComId, curPageNO);

		PageSupportHelper.savePage(request, ps);
		request.setAttribute("product", product);
		request.setAttribute("productComment", productComment);

		return PathResolver.getPath(FrontPage.PRODUCT_COMMENTS_DETAIL);
	}

	/**
	 * 回复评价
	 *
	 * @param prodComId
	 * @param replyText
	 * @return
	 */
	@RequestMapping(value = "/replyComment", method = RequestMethod.POST)
	@ResponseBody
	public Object reply(HttpServletRequest request, HttpServletResponse response,
						@RequestParam Long prodComId, @RequestParam String replyContent) {

		Map<String, Object> result = new HashMap<String, Object>();
		try {

			SecurityUserDetail user = UserManager.getUser(request);
			String userId = user.getUserId();
			if (AppUtils.isBlank(userId)) {
				result.put("status", 0);
				return result;
			}

			ProductComment productComment = productCommentService.getProductCommentById(prodComId);

			if (null == productComment) {
				result.put("status", -1);
				result.put("msg", "亲, 您要回复评论不存在!");
				return result;
			}

			if (productComment.getUserId().equals(userId)) {
				result.put("status", -1);
				result.put("msg", "亲, 您不能自己评价自己!");
				return result;
			}

			String userName = userDetailService.getNickNameByUserId(userId);
			if (AppUtils.isBlank(userName)) {
				userName = user.getUsername();
			}
			ProductReply productReply = new ProductReply();
			productReply.setProdcommId(prodComId);
			productReply.setReplyContent(XssFilterUtil.cleanXSS(replyContent));
			productReply.setReplyUserId(userId);
			productReply.setReplyUserName(userName);
			productReply.setStatus(1);
			productReply.setPostip(IPHelper.getIpAddr(request));
			productReply.setReplyTime(new Date());
			productReplyService.saveProductReply(productReply);

			result.put("status", 1);
			result.put("data", productReply);
			return result;
		} catch (Exception e) {
			log.error("用户回复评价异常!", e);
			result.put("status", -1);
			result.put("msg", "亲, 服务异常, 请稍候重试!");
			return result;
		}

	}

	/**
	 * 回复其他回复人
	 *
	 * @param prodComId
	 * @param replyText
	 * @param parentReplyId
	 * @param parentId
	 * @return
	 */
	@RequestMapping(value = "/replyParent", method = RequestMethod.POST)
	@ResponseBody
	public Object replyParent(HttpServletRequest request, HttpServletResponse response,
							  @RequestParam Long parentReplyId, @RequestParam String replyContent, @RequestParam Long prodId) {

		Map<String, Object> result = new HashMap<String, Object>();
		try {

			SecurityUserDetail user = UserManager.getUser(request);

			if (AppUtils.isBlank(user)) {
				result.put("status", 0);
				return result;
			}

			String userId = user.getUserId();

			ProductReply parentReply = productReplyService.getProductReply(parentReplyId);

			if (null == parentReply) {
				result.put("status", -1);
				result.put("msg", "亲, 您要回复评论不存在!");
				return result;
			}

			if (parentReply.getReplyUserId().equals(userId)) {
				result.put("status", -1);
				result.put("msg", "亲, 自己不能回复自己哦!");
				return result;
			}

			String userName = userDetailService.getNickNameByUserId(userId);
			if (AppUtils.isBlank(userName)) {
				userName = user.getUsername();
			}
			//判断是否掌柜回复
			Long userShopId = userDetailService.getShopIdByUserId(userId);
			if (AppUtils.isNotBlank(userShopId)) {
				Long shopId = productService.getProdDetail(prodId).getShopId();
				if (shopId.equals(userShopId)) {
					userName = userName + "（掌柜）";
				}
			}
			ProductReply productReply = new ProductReply();
			productReply.setParentUserId(parentReply.getReplyUserId());
			productReply.setParentUserName(parentReply.getReplyUserName());
			productReply.setProdcommId(parentReply.getProdcommId());
			productReply.setReplyContent(XssFilterUtil.cleanXSS(replyContent));
			productReply.setParentReplyId(parentReplyId);
			productReply.setReplyUserId(userId);
			productReply.setReplyUserName(userName);
			productReply.setPostip(IPHelper.getIpAddr(request));
			productReply.setStatus(1);
			productReply.setReplyTime(new Date());
			productReplyService.saveProductReply(productReply);

			result.put("status", 1);
			result.put("data", productReply);

			return result;
		} catch (Exception e) {
			log.error("回复其他人回复异常!", e);
			result.put("status", -1);
			result.put("msg", "亲, 服务异常, 请稍候重试!");
			return result;
		}
	}

	/**
	 * 商品评论列表
	 *
	 * @param request
	 * @param response
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list(HttpServletRequest request, HttpServletResponse response, ProductCommentsQueryParams params) throws Exception {

		parseCommentList(request, response, params);

		return PathResolver.getPath(FrontPage.PRODUCT_COMMENTS);
	}

	@RequestMapping(value = "/listcontent", method = RequestMethod.GET)
	public String listcontent(HttpServletRequest request, HttpServletResponse response,
							  ProductCommentsQueryParams params) throws Exception {

		parseCommentList(request, response, params);

		return PathResolver.getPath(FrontPage.PRODUCT_COMMENT_LIST);
	}

	/**
	 * 商品评论列表内容
	 *
	 * @param request
	 * @param response
	 * @param params
	 */
	private void parseCommentList(HttpServletRequest request, HttpServletResponse response,
								  ProductCommentsQueryParams params) {

		PageSupport<ProductCommentDto> ps = productCommentService.queryProductComment(params);
		PageSupportHelper.savePage(request, ps);

		//计算当前商品的评论情况
		ProductCommentCategory pcc = productCommentService.initProductCommentCategory(params.getProdId());

		request.setAttribute("prodCommCategory", pcc);
	}

	/**
	 * 获取用户服务说明
	 */
	@ApiOperation(value = "根据商品id获取服务说明", produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/serviceShow")
	@ApiImplicitParam(name = "prodId", paramType = "query", value = "商品id", required = true, dataType = "Long")
	public String getServerShow(HttpServletRequest request, Long prodId) {

		String result = productService.getServerShowById(prodId);
		request.setAttribute("show", result);
//		return result;
		return PathResolver.getPath(FrontPage.PROD_SERVICE_SHOW);
	}

	/**
	 * 获取营业执照信息
	 */
	@ApiOperation(value = "根据店铺id获取营业执照信息", produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping(value = "/business/message")
	@ApiImplicitParam(name = "shopId", paramType = "query", value = "店铺id", required = true, dataType = "Long")
	public String getBusinessMessage(HttpServletRequest request, Long shopId) {
		BusinessMessageDTO businessMessage = shopDetailService.getBusinessMessage(shopId);
		if (businessMessage != null && businessMessage.getType() != 0) {
			request.setAttribute("message", businessMessage);
			return PathResolver.getPath(FrontPage.SHOP_BUSINESS_DETAIL);
		}
		return null;
	}

	/**
	 * 判断个人商家店铺
	 */
	@ApiOperation(value = "判断个人商家店铺", produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping(value = "/business/flag")
	@ApiImplicitParam(name = "shopId", paramType = "query", value = "店铺id", required = true, dataType = "Long")
	@ResponseBody
	public Integer getBusinessflag(Long shopId) {
		BusinessMessageDTO businessMessage = shopDetailService.getBusinessMessage(shopId);
		return businessMessage.getType();
	}

}
