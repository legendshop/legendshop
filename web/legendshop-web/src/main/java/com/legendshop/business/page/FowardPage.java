/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * The Enum FowardPage.
 */
public enum FowardPage implements PageDefinition {
	/** The VARIABLE. 可变路径 */
	VARIABLE(""),
	
	/** forward to index page**/
	INDEX_QUERY("/index"),
	
	/** forward to home page**/
	HOME_QUERY("/home"),

	/** The Product View. */
	VIEWS("/views"),
	
   /** 商城配置*/
	SHOP_SETTING("/s/shopSetting");

	/** The value. */
	private final String value;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.String, com.legendshop.core.constant.PageDefinition)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("forward:", path);
	}

	/**
	 * Instantiates a new tiles page.
	 * 
	 * @param value
	 *            the value
	 */
	private FowardPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
