/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;



import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.legendshop.business.page.FrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Theme;
import com.legendshop.model.entity.ThemeModule;
import com.legendshop.model.entity.ThemeModuleProd;
import com.legendshop.model.entity.ThemeRelated;
import com.legendshop.spi.service.ThemeModuleService;
import com.legendshop.spi.service.ThemeRelatedService;
import com.legendshop.spi.service.ThemeService;
/**
 * 活动主题
 *
 */
@Controller
@RequestMapping("/theme")
public class ThemeController extends BaseController {
	
	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(IndexController.class);

	@Autowired
	private ThemeService themeService;
	@Autowired
	private ThemeRelatedService themeRelatedService;
	@Autowired
	private ThemeModuleService themeModuleService;
	
	/**
	 * 专题活动列表
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping(value="/allThemes",method=RequestMethod.GET)
	public String allThemes(HttpServletRequest request, HttpServletResponse response,String curPageNO) {

		PageSupport<Theme> ps=themeService.allThemes(curPageNO);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(FrontPage.THEME_LIST);
	}
	
	
	@RequestMapping(value="/themeDetail/{themeId}",method=RequestMethod.GET)
	public String themeDetail(HttpServletRequest request, HttpServletResponse response,@PathVariable Long themeId) {
		Theme theme = themeService.getTheme(themeId);
		if(theme == null){
			log.warn("Can not found theme by id {}", themeId);
			return null;
		}
		Map<ThemeModule, List<ThemeModuleProd>>  themeModuleMap = themeModuleService.getThemeModuleAndProdByThemeId(themeId);
		List<ThemeRelated> themeRelated= themeRelatedService.getThemeRelatedByTheme(themeId);
		request.setAttribute("theme", theme);
		request.setAttribute("themeModuleMap", themeModuleMap);
		request.setAttribute("themeRelated", themeRelated);
		request.setAttribute("disTime", theme.getEndTime().getTime()-new Date().getTime());
		return PathResolver.getPath(FrontPage.THEME);
	}

}
