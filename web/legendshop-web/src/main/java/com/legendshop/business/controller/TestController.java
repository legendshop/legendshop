/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.config.PropertiesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.business.page.FrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.entity.Pub;
import org.springframework.web.bind.annotation.RestController;


/**
 * 投诉
 */
@RestController
public class TestController extends BaseController {
	 

	@Autowired
	private PropertiesUtil propertiesUtil;
	

	 /**
	  * 投诉管理
	  * @return
	  */
	 @RequestMapping("/test")
	public String accusation() {

	 	return propertiesUtil.getPcDomainName();
	}
	  
	  
}
