/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.business.page.FrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.CommonPage;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.dto.compare.ProductCompareDto;
import com.legendshop.spi.service.ProductCompareService;
import com.legendshop.util.AppUtils;

@Controller
public class ProductCompareController extends BaseController {

	@Autowired
	private  ProductCompareService productCompareService;
	
	
	@RequestMapping("/compare")
	public String compare(HttpServletRequest request, HttpServletResponse response, String gids) {
		if(AppUtils.isBlank(gids)){
			return PathResolver.getPath(FrontPage.COMPARE);
		}
		String[] it=gids.split(",");
		if(AppUtils.isBlank(it)){
			return PathResolver.getPath(FrontPage.COMPARE);
		}
		List<Long> items=new ArrayList<Long>();
		for (int i = 0; i < it.length; i++) {
			try {
		       String str=it[i];
		       if(AppUtils.isBlank(str)){
		    	  continue;
		       }
		       Long  p=Long.valueOf(str);
			   items.add(p);
			} catch (NumberFormatException e) {
				String path =  PathResolver.getPath(CommonPage.ERROR_FRAME_PAGE);
				return path;
			}
		}
		Long []arrays=changeArray(items);
		ProductCompareDto compare=productCompareService.getProductCompareDto(arrays);
		request.setAttribute("compare", compare);
		return PathResolver.getPath(FrontPage.COMPARE);
	}
	
	private Long[] changeArray(List<Long> items){
		if(AppUtils.isBlank(items)){
			return null;
		}
		Long[] array=new Long[items.size()];
		for (int i = 0; i < items.size(); i++) {
			array[i]=items.get(i);
		}
		items=null;
		return array;
	}
	
	
	
	
}
