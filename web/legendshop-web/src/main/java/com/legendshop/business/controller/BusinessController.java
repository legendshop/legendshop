/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.business.page.FrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.util.AppUtils;

/**
 * 前台主要功能.
 */
@Controller
public class BusinessController extends BaseController {
	
	
	/**
	 * After operation.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the string
	 */
	@RequestMapping("/afteroperation")
	public String afterOperation(HttpServletRequest request, HttpServletResponse response,String userName) {
		if(AppUtils.isNotBlank(userName)){
			request.setAttribute("userName", userName);
		}
		return PathResolver.getPath(FrontPage.AFTER_OPERATION);
	}

	
	@RequestMapping("/regbindEdit")
	public String regbindEdit(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(FrontPage.REGBIND_EDIT);
	}

}
