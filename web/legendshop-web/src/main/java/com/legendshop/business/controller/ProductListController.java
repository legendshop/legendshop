/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import org.springframework.stereotype.Controller;

import com.legendshop.core.base.BaseController;


/**
 * 前台主要功能.
 */
@Controller
public class ProductListController extends BaseController {

}
