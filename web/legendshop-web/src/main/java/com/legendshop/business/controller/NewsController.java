/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.business.page.FrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.entity.News;
import com.legendshop.model.entity.NewsCategory;
import com.legendshop.spi.service.NewsCategoryService;
import com.legendshop.spi.service.NewsService;
import com.legendshop.util.AppUtils;

/**
 * 产品分类控制器.
 */
@Controller
public class NewsController extends BaseController {

	@Autowired
	private NewsService newsService;

	@Autowired
	private NewsCategoryService newsCategoryService;


	/**
	 * News.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param newsId
	 *            the news id
	 * @return the string
	 */
	@RequestMapping("/news/{newsId}")
	public String news(HttpServletRequest request, HttpServletResponse response, @PathVariable Long newsId) {
		if (newsId != null) {
			News news = newsService.getNewsById(newsId);
			if (news != null) {
				// 上一条 下一条
				List<News> newsList = newsService.getNearNews(newsId);
				request.setAttribute("newsList", newsList);
				request.setAttribute("news", news);
			}
			// load news by category 文章分类
			Map<KeyValueEntity, List<News>> newsCatList = newsService.getNewsByCategory();
			request.setAttribute("newsCatList", newsCatList);
		}
		return PathResolver.getPath(FrontPage.NEWS);
	}

	/**
	 * 文章中心.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param curPageNO
	 *            the cur page no
	 * @param newsCategory
	 *            the news category
	 * @return the string
	 */
	@RequestMapping("/allnews")
	public String allNews(HttpServletRequest request, HttpServletResponse response, String curPageNO, Long newsCategoryId) {
		if (AppUtils.isBlank(curPageNO)) {
			curPageNO = "1";
		}
		PageSupport<News> ps = newsService.getNews(curPageNO, newsCategoryId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("newsCategoryId", newsCategoryId);
		if (newsCategoryId != null) {
			NewsCategory newsCategory = newsCategoryService.getNewsCategoryById(newsCategoryId);
			if (newsCategory != null) {
				request.setAttribute("newsCategoryName", newsCategory.getNewsCategoryName());
			}
		}
		// load news by category
		Map<KeyValueEntity, List<News>> newsCatList = newsService.getNewsByCategory();
		request.setAttribute("newsCatList", newsCatList);
		return PathResolver.getPath(FrontPage.ALL_NEWS);
	}

}
