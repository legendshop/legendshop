package com.legendshop.business.controller;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.advanced.search.model.FacetProxy;
import com.legendshop.advanced.search.service.SearchService;
import com.legendshop.base.util.XssFilterUtil;
import com.legendshop.base.xssfilter.XssDtoFilter;
import com.legendshop.business.page.FrontPage;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.DefaultPagerProvider;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.spi.service.SensitiveWordService;
import com.legendshop.util.AppUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
  *   商品搜索
 *
 */
@Controller
public class AdvancedSearchController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(AdvancedSearchController.class);
	
	@Autowired
	private SearchService searchService;

	@Autowired
	private SensitiveWordService sensitiveWordService;
	
	
	/**
	 * 1. 搜索页入口
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param parms
	 * @return
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list(HttpServletRequest request, HttpServletResponse response,Integer curPageNO,ProductSearchParms parms) {

		//防止做注入
		parms = XssDtoFilter.safeProductSearchParms(parms);

		// 敏感字过滤
		Set<String> findwords = sensitiveWordService.checkSensitiveWords(parms.getKeyword());
		if (AppUtils.isNotBlank(findwords)) {
			request.setAttribute("message", "您查询的内容含有敏感词 " + findwords + ",根据本地法律无法显示，请更正再重试");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}

		request.setAttribute("searchParams", parms);
		request.setAttribute("curPageNO", curPageNO);
		return PathResolver.getPath(FrontPage.LIST);
	}
	
	/**
	 * 2. 商品搜索页找到商品的参数
	 * @param request
	 * @param response
	 * @param parms
	 * @return
	 */
	@RequestMapping(value="/prodParamList",method = RequestMethod.POST)
	public String prodParamList(HttpServletRequest request, HttpServletResponse response,ProductSearchParms parms) {

		Map<String, String[]> requestParams = request.getParameterMap();
		parms.setRequestParams(requestParams);

		parms = XssDtoFilter.safeProductSearchParms(parms);

		FacetProxy facetProxy=searchService.prodParamList(parms);
		if(facetProxy==null){
			return PathResolver.getPath(FrontPage.PRODUCT_PARAM_LIST);
		}
		request.setAttribute("searchParams", parms);
		request.setAttribute("facetProxy", facetProxy);
		return PathResolver.getPath(FrontPage.PRODUCT_PARAM_LIST);
	}
	
	/**
	 * 3. 商品搜索页详情
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param parms
	 * @return
	 */
	@RequestMapping(value="/prodlist",method = RequestMethod.POST)
	public String prodlist(HttpServletRequest request, HttpServletResponse response,Integer curPageNO, ProductSearchParms parms) {
		curPageNO = AppUtils.isNotBlank(curPageNO) ?curPageNO : 1;
		Integer pageSize=20;

		//防止做注入
		parms = XssDtoFilter.safeProductSearchParms(parms);

		JSONObject json=searchService.prodlist(curPageNO,pageSize,parms);
		request.setAttribute("searchParams", parms);
		request.setAttribute("keyword", parms.getKeyword());
		if(json==null){
			return PathResolver.getPath(FrontPage.PRODUCT_SORT_LIST);
		}
		@SuppressWarnings("unchecked")
		List<ProductDetail> details=(List<ProductDetail>) json.get("result");
		Long  numFound=json.getLong("numFound");
		initPageProvider(request,numFound,curPageNO,pageSize,details);
		return PathResolver.getPath(FrontPage.PRODUCT_SORT_LIST);
	}
	
	private void initPageProvider(HttpServletRequest request, long total, Integer curPageNO, Integer pageSize, List<ProductDetail> resultList) {
		DefaultPagerProvider pageProvider = new DefaultPagerProvider(total, curPageNO, pageSize);
		int toatalPage = pageProvider.getPageCount();
		PageSupport<ProductDetail> ps = new PageSupport<ProductDetail>(resultList, pageProvider);
		request.setAttribute("toatalPage", toatalPage);
		PageSupportHelper.savePage(request, ps);
	}
	
	
    /**
     * 	
     * @param request
     * @param response
     * @param keyword
     * @return
     */
	@RequestMapping(value = "/seach/suggest", method = RequestMethod.GET)
	@ResponseBody
	public Object[] suggest(HttpServletRequest request, HttpServletResponse response, String keyword) {
	     if(AppUtils.isNotBlank(keyword)){
	    	    String value=keyword.trim();
	    	    if(AppUtils.isBlank(value)){
	    	    	return  new Object[] {};
	    	    }
	    	    else if (keyword.length()>100) {
	    	    	return  new Object[] {};
	    		}
	    	    if(AppUtils.isBlank(keyword)){
	    	    	return  new Object[] {};
	    	    }
	    	    List<String> str=null;
				try {
					str=searchService.suggest(keyword);
				} catch (Exception e) {
					log.debug("recordSearchHistory error:" + e.getMessage());
					return  new Object[] {};
				}
				if(AppUtils.isNotBlank(str)){
					return str.toArray();
				}
				return  new Object[] {};
			}
		return  new Object[] {};
	}


	
}
