/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.controller;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.constants.SysParameterEnum;
import com.legendshop.base.dto.DefaultAddressDTO;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.*;
import org.apache.commons.math3.analysis.function.Logistic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.Mergeable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.exception.ErrorCodes;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.base.model.UserMessages;
import com.legendshop.business.page.FrontPage;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.CommonPage;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.helper.VisitHistoryHelper;
import com.legendshop.core.page.CommonFowardPage;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.constant.ProductTypeEnum;
import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.constant.SkuActiveTypeEnum;
import com.legendshop.model.constant.VisitSourceEnum;
import com.legendshop.model.constant.VisitTypeEnum;
import com.legendshop.model.dto.ProductDto;
import com.legendshop.model.dto.SkuDto;
import com.legendshop.model.dto.TransfeeDto;
import com.legendshop.model.dto.TreeNode;
import com.legendshop.model.dto.buy.CartMarketRules;
import com.legendshop.model.dto.marketing.RuleResolver;
import com.legendshop.model.dto.seckill.Seckill;
import com.legendshop.processor.VisitLogProcessor;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.security.service.LoginService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.ip.IPSeeker;
import com.legendshop.web.helper.IPHelper;

/**
 * 产品分类控制器。
 */
@Controller
public class ProductController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(ProductController.class);

	/** The product service. */
	@Autowired
	private ProductService productService;

	@Autowired
	private SkuService skuService;

	@Autowired
	private LocationService locationService;

	@Autowired
	private SystemParameterService systemParameterService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private ProductCommentStatService productCommentStatService;

	@Autowired
	private ShopCommStatService shopCommStatService;

	@Autowired
	private  DvyTypeCommStatService dvyTypeCommStatService ;

	@Autowired
	private CategoryManagerService categoryManagerService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private ActiveRuleResolverManager activeRuleResolverManager;

	@Autowired
	private StockService stockService;

	@Autowired
	private ProductArrivalInformService productArrivalInformService;

	@Autowired
	private ProdTagRelService prodTagRelService;

	@Autowired
	private ProdTagService prodTagService;

	@Autowired
	private ProdPropImageService prodPropImageService;

	@Autowired
	private ImgFileService imgFileService;

	@Autowired
	private CouponService couponService;

	@Autowired
	private ShippingActiveService shippingActiveService;

	/**
	 * 日志查看
	 */
	@Autowired
	private VisitLogProcessor visitLogProcessor;

	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;

	@Autowired
	private SystemParameterUtil systemParameterUtil;


	@RequestMapping(value="/views/isAlreadySave",method=RequestMethod.POST)
	@ResponseBody
	public String isAlreadySave(Long skuId,HttpServletRequest request, HttpServletResponse response){
		//判断当前用户是否已经设置该商品为到货通知
		SecurityUserDetail securityUserDetail = UserManager.getUser(request);
		if(securityUserDetail == null){
			return "login";
		}
		int status = 0;
		ProdArrivalInform user = productArrivalInformService.getAlreadySaveUser(securityUserDetail.getUserId(), skuId, status);
		if(user == null){
			return "notExist";
		}else{
			return "exist";
		}
	}

	/**
	 * 到货通知弹出窗
	 */
	@RequestMapping(value="/views/prodArrInfoPage",method=RequestMethod.GET)
	public String prodArrInfoPage(Long skuId,Long prodId,Long shopId,HttpServletRequest request, HttpServletResponse response){
		request.setAttribute("shopId", shopId);
		request.setAttribute("prodId", prodId);
		request.setAttribute("skuId", skuId);
		String path = PathResolver.getPath(FrontPage.PRODUCT_ARRIVAL_INFORM_PAGE);
		return path;
	}

	/**
	 * 添加商品到货通知信息
	 */
	@RequestMapping(value="/views/prodArrInform")
	@ResponseBody
	public String prodArrInform(String jsondata,HttpServletRequest request, HttpServletResponse response){
		SecurityUserDetail user = UserManager.getUser(request);
		if(AppUtils.isBlank(user)){
			return Constants.USER_LOGIN_REQUIRED;
		}

		String userId = user.getUserId();
		String userName = user.getUsername();

		ProdArrivalInform prodArrInform = JSONUtil.getObject(jsondata, ProdArrivalInform.class);

		//添加到货通知
		prodArrInform.setUserId(userId);
		prodArrInform.setUserName(userName);
		productArrivalInformService.saveProdArriInfo(prodArrInform);
		return Constants.SUCCESS;
	}

	/**
	 * 查看商品详情.
	 *
	 */
	@RequestMapping("/views/{prodId}")
	public String views(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {
		Float shopscore=0f;
		Float prodscore=0f;
		Float logisticsScore=0f;
		DecimalFormat decimalFormat = new DecimalFormat("0.0");
		decimalFormat.setRoundingMode(RoundingMode.FLOOR);
		if (prodId == null) {
			return PathResolver.getPath(CommonFowardPage.INDEX_QUERY);
		}
		ProductDto prod = productService.getProductDto(prodId);

		if (prod != null && !ProductStatusEnum.PROD_DELETE.value().equals(prod.getStatus())) {

			//计算 营销活动
			calculateMarketing(prod);

			//不能设置空,否则在spring boot环境下js报错
			if(AppUtils.isNotBlank(prod.getSkuDtoList())) {
				String skuDtoList = JSON.toJSONString(prod.getSkuDtoList(),SerializerFeature.DisableCircularReferenceDetect);
				prod.setSkuDtoListJson(skuDtoList);
			}

			//判断 生效时间 
			if(ProductStatusEnum.PROD_ONLINE.value().equals(prod.getStatus())){
				Date startDate = prod.getStartDate();
				Date now = new Date();
				//还没生效
				if(startDate!=null && startDate.after(now)){
					request.setAttribute("invalid", true);
				}
			}

			SecurityUserDetail securityUserDetail = UserManager.getUser(request);

			//评论数 和 平均评分
			prod.setCommCount(0);
			prod.setProdAvrScore(0);
			ProductCommentStat productCommentStat = productCommentStatService.getProductCommentStatByProdId(prodId);
			if(AppUtils.isNotBlank(productCommentStat)){
				if(productCommentStat.getScore()!=null && productCommentStat.getComments()!=null && productCommentStat.getComments()!=0){
					prod.setCommCount(productCommentStat.getComments());
					prod.setProdAvrScore((int)Math.round(productCommentStat.getScore().doubleValue()/productCommentStat.getComments()));
				}
			}
			request.setAttribute("prod", prod);

			//分销的用户名称
			String uid = request.getParameter("uid");
			if(AppUtils.isNotBlank(uid)){
				request.setAttribute("uid", uid);
			}

			//查询店铺的详情信息
			Long shopId=prod.getShopId();
			ShopDetail shopDetail= shopDetailService.getShopDetailByIdNoCache(shopId);

			if(AppUtils.isBlank(shopDetail) || shopDetail.getStatus() != ShopStatusEnum.NORMAL.value()){
				return PathResolver.getPath(FrontPage.PROD_NON_EXISTENT);
			}else {
				request.setAttribute("shopDetail", shopDetail);

				String mailfeeStr = shippingActiveService.queryShopMailfeeStr(shopId,prodId);
				request.setAttribute("mailfeeStr", mailfeeStr);
			}

			//计算该店铺所有已评分商品的平均分
			ProductCommentStat prodCommentStat = productCommentStatService.getProductCommentStatByShopId(shopId);
			if(prodCommentStat!=null&&prodCommentStat.getScore()!=null&&prodCommentStat.getComments()!=null){
				prodscore=(float)prodCommentStat.getScore()/prodCommentStat.getComments();
			}
			request.setAttribute("prodAvg",decimalFormat.format(prodscore));

			//计算该店铺所有已评分店铺的平均分
			ShopCommStat shopCommentStat = shopCommStatService.getShopCommStatByShopId(shopId);
			if(shopCommentStat!=null&&shopCommentStat.getScore()!=null&&shopCommentStat.getCount()!=null){
				shopscore=(float)shopCommentStat.getScore()/shopCommentStat.getCount();
			}
			request.setAttribute("shopAvg",decimalFormat.format(shopscore));
			//计算该店铺所有已评分物流的平均分
			DvyTypeCommStat  dvyTypeCommStat = dvyTypeCommStatService.getDvyTypeCommStatByShopId(shopId);
			if(dvyTypeCommStat!=null&&dvyTypeCommStat.getScore()!=null&&dvyTypeCommStat.getCount()!=null){
			    logisticsScore=(float)dvyTypeCommStat.getScore()/dvyTypeCommStat.getCount();
			}
			request.setAttribute("dvyAvg",decimalFormat.format(logisticsScore));
			Float sum=0f;
			if (shopscore!=0&&prodscore!=0&&logisticsScore!=0) {
				sum=(float)Math.floor((shopscore+prodscore+logisticsScore)/3);
			}else if(shopscore!=0&&logisticsScore!=0){
				sum=(float)Math.floor((shopscore+logisticsScore)/2);
			}
			request.setAttribute("sum",decimalFormat.format(sum));


			//查询该分类
			if(AppUtils.isNotBlank(prod.getCategoryId())){
				List<TreeNode> treeNodes=categoryManagerService.getTreeNodeNavigation(prod.getCategoryId(), ",");
				request.setAttribute("treeNodes", treeNodes);
			}
			//查询团购
			if(prod.getIsGroup()==1){
				Group group=productService.getGroupPrice(prodId);
				if(AppUtils.isNotBlank(group)){
					Date nowDate = new Date();
					if(nowDate.getTime()>group.getEndTime().getTime()){//如果当前时间已经大于活动的结束时间就把商品的状态回显
						productService.updataIsGroup(prodId,0);	//设置不是团购商品
						prod.setIsGroup(0);
					}
				}
				request.setAttribute("group", group);
			}
			//查询秒杀
			if(prod.getActiveId()!=null){
				Seckill seckill=productService.getSeckillPrice(prod.getActiveId(), prod.getProdId(), prod.getSkuId());
				if (seckill!=null) {
					Date nowDate = new Date();
					if(nowDate.getTime()<seckill.getEndTime().getTime()){//如果当前时间已经大于活动的结束时间就把商品的状态回显
						request.setAttribute("seckill", seckill);
					}
				}
			}
			//获取商品拥有者信息 

//			UserDetail userDetail = userDetailService.getUserDetail(prod.getUserName());
//			request.setAttribute("userDetail", userDetail);

			// 在cookies中记录商品访问历史
			VisitHistoryHelper.visit(prodId, request, response);


			List<Province> provinces = locationService.getAllProvince();
			request.setAttribute("provinces", provinces);

			//根据IP获得用户当前位置
			String ip = IPHelper.getIpAddr(request);
			if(checkIP(ip)){ //合法的Ip
				IPSeeker seeker = 	IPSeeker.getInstance();
				String  country = seeker.getCountry(ip);
				if(country != null && country.length() > 2){
					String 	provinceName = country.substring(0, 3);
					String  cityName = country.substring(3);
					//配送至地址 默认值
					List<Province> province = locationService.getProvinceByName(provinceName);
					List<City> city =  locationService.getCityByName(cityName);
					if(AppUtils.isNotBlank(province)&&AppUtils.isNotBlank(city)){
						Province p1 = province.get(0);
						City c1 = city.get(0);
						setDefaultAddress(request, p1.getId(),c1.getId(), country);//由客户所在的IP地址决定的
					}else{
						setDefaultAddress(request, 2,2, country);//天津市滨海新区
					}
				}else{
					setDefaultAddress(request, 2,2, "天津市滨海新区");
				}
			}else{
				setDefaultAddress(request, 2,2, "天津市滨海新区");
			}

			//获取手机端的域名
			String vueDomainName = propertiesUtil.getVueDomainName();
			request.setAttribute("vueDomainName", vueDomainName);

			//获取商品相关优惠券
			List<Coupon> couponList = couponService.getProdsCoupons(shopId, prodId);
			request.setAttribute("couponList", couponList);

			// 多线程记录访问历史
			if (systemParameterUtil.isVisitLogEnable()) {
				String userName = "";
				if(securityUserDetail != null) {
					 userName = securityUserDetail.getUserId();
				}

				VisitLog visitLog = new VisitLog(IPHelper.getIpAddr(request), userName, shopDetail.getShopId(), shopDetail.getSiteName(),
						prodId, prod.getName(), prod.getPic(), prod.getCash(), VisitTypeEnum.PROD.value(), VisitSourceEnum.PC.value(), new Date());
				visitLogProcessor.process(visitLog);
			}

			if(AppUtils.isNotBlank(securityUserDetail)){
				UserDetail user = userDetailService.getUserDetailById(securityUserDetail.getUserId());
				if(AppUtils.isNotBlank(user)){
					String password = user.getUserName()+"123456";
					request.setAttribute("user_name",user.getUserName());
					request.setAttribute("user_pass", password);
				}
			}
			return PathResolver.getPath(FrontPage.VIEWS);
		} else {
			return PathResolver.getPath(FrontPage.PROD_NON_EXISTENT);
		}
	}

	/**
	 * 设置默认的地址
	 * @param request
	 * @param country
	 */
	private void setDefaultAddress(HttpServletRequest request, Integer provinceid, Integer cityid, String country){
		SystemParameter systemParameter = systemParameterService.getSystemParameter(SysParameterEnum.DEFAULT_PRODUCT_ADDRESS.name());
		String value = systemParameter.getValue();
		DefaultAddressDTO defaultAddressDTO = cn.hutool.json.JSONUtil.toBean(value, DefaultAddressDTO.class);
		List<City> citys = locationService.getCity(defaultAddressDTO.getIpProvinceId()); //默认是天津市
		List<Area> areas = locationService.getArea(defaultAddressDTO.getIpCityId());//默认是滨海新区
		request.setAttribute("citys", citys);
		if(AppUtils.isNotBlank(citys)){
			request.setAttribute("cityId", citys.get(0).getId());
		}
		request.setAttribute("areas", areas);
		request.setAttribute("provinceId", defaultAddressDTO.getIpProvinceId());
		request.setAttribute("ipCountry", country);
		request.setAttribute("ipProvince", defaultAddressDTO.getIpProvince());
		request.setAttribute("ipCity", defaultAddressDTO.getIpCity());
		request.setAttribute("ipArea", defaultAddressDTO.getIpArea());
	}

	/**
	 * 检查是否为合法的Ip
	 * @param str
	 * @return
	 */
	  private boolean checkIP(String str) {
	        Pattern pattern = Pattern.compile("^((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5]|[*])\\.){3}(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5]|[*])$");
	        boolean result =  pattern.matcher(str).matches();
	        if(!result){
	        	log.warn("Invalid IP {} ", str );
	        }
	        return result;
	    }

	  public static void main(String[] args) {
		  ProductController t = new ProductController();
		System.out.println(t.checkIP("192.168.0.12"));
	}

	/**
	 * 打开环信的页面
	 */
	@RequestMapping(value="/openHuanXin")
	public String openHuanXin(HttpServletRequest request, HttpServletResponse response,Long shopId){
		ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
		if(AppUtils.isNotBlank(shopDetail)){//获取访问商家基本信息
			request.setAttribute("shop_name", shopDetail.getUserName());
			request.setAttribute("site_name", shopDetail.getSiteName());

			SecurityUserDetail securityUserDetail = UserManager.getUser(request);
			String userId = "";
			if(securityUserDetail != null) {
				userId = securityUserDetail.getUserId();
			}

			if(AppUtils.isNotBlank(userId)){//获取自己登录账户的基本信息
				//如果登录的是商家，显示的应该是商家昵称
				ShopDetail personalShopDetail = shopDetailService.getShopDetailByUserId(userId);
				if(AppUtils.isNotBlank(personalShopDetail)){
					UserDetail user = userDetailService.getUserDetailById(userId);
					if(AppUtils.isNotBlank(user)){
						String password = user.getUserName()+"123456";
						request.setAttribute("user_name", user.getUserName());
						request.setAttribute("user_pass", password);
						request.setAttribute("user_nick", personalShopDetail.getSiteName());
					}
				}else{
					UserDetail user = userDetailService.getUserDetailById(userId);
					if(AppUtils.isNotBlank(user)){
						String password = user.getUserName()+"123456";
						request.setAttribute("user_name", user.getUserName());
						request.setAttribute("user_pass", password);
						request.setAttribute("user_nick", user.getNickName());
					}
				}

			}
			return PathResolver.getPath(FrontPage.PROD_HUANXIN);
		}else{
			//跳到操作失败页面
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}

	}


	/**
	 * 将用户昵称显示出来
	 */
	@RequestMapping(value="/getNickName",produces="application/json;charset=UTF-8")
	@ResponseBody
	public String getNickName(String userName){
		UserDetail userDetail = userDetailService.getUserDetail(userName);
		if(AppUtils.isNotBlank(userDetail)){
			ShopDetail shopDetail = shopDetailService.getShopDetailByUserId(userDetail.getUserId());
			if(AppUtils.isNotBlank(shopDetail)){
				return shopDetail.getSiteName();
			}
			return userDetail.getNickName();
		}else{
			return Constants.FAIL;
		}
	}


	/**
	 * 取出标签
	 * @param prodId
	 * @return
	 */
	@RequestMapping(value="/ajaxTag/{prodId}")
	@ResponseBody
	public String ajaxTag(@PathVariable Long prodId){
		ProdTagRel prodTagRel = prodTagRelService.getProdTagRelByProdId(prodId);
		String arr;
		if(AppUtils.isNotBlank(prodTagRel)){
			//取出有效的标签
			ProdTag prodTag = prodTagService.getByIdAndStatus(prodTagRel.getTagId());
			if(AppUtils.isNotBlank(prodTag)){
				arr = prodTag.getRightTag()+","+prodTag.getBottomTag();
			}else{
				return Constants.FAIL;
			}
		}else{
			return Constants.FAIL;
		}
		return arr;
	}

	private void calculateMarketing(ProductDto prod){
		List<SkuDto> list=prod.getSkuDtoList();
		if(AppUtils.isBlank(list)){
			return ;
		}
		if(!ProductTypeEnum.PRODUCT.value().equals(prod.getProdType())){//普通商品才计算营销
			return ;
		}
		/*if( prod.getIsGroup()!=null && prod.getIsGroup().intValue()==1){ //如果是团购,不参与计算营销
			return ;
		}*/
		for(SkuDto skuDto:list){
			RuleResolver resolver=new RuleResolver();
			resolver.setPrice(skuDto.getPrice());
			resolver.setPromotionPrice(skuDto.getPrice());
			resolver.setBasketCount(1);
			resolver.setShopId(prod.getShopId());
			resolver.setProdId(prod.getProdId());
			resolver.setSkuId(skuDto.getSkuId());
			/**
			 * 查询该商品的参与的营销规则-->计算价格
			 */
			activeRuleResolverManager.executorMarketing(resolver);

			List<CartMarketRules> marketRules=resolver.getCartMarketRules();
			skuDto.setPromotionPrice(resolver.getPromotionPrice()); //设置计算营销后的价格
			skuDto.setRuleList(marketRules);
			resolver=null;
		}
	}

	/**
	 * 计算运费
	 * @param shopId
	 * @param transportId
	 * @param cityId
	 * @param totalCount
	 * @param totalWeight
	 * @param totalvolume
	 * @return
	 */
	@RequestMapping(value="/getProdFreight",method=RequestMethod.POST)
	@ResponseBody
	public String  getProdFreight(Long shopId,Long transportId, Integer cityId,double totalCount,
			 double totalWeight ,
			 double totalvolume) {
		String result="";
		if(AppUtils.isBlank(transportId) || AppUtils.isBlank(shopId)){
			return result;
		}
		List<TransfeeDto> transfeeDtos=productService.getProdFreight(shopId,transportId,cityId,totalCount,totalWeight,totalvolume);
		if(AppUtils.isBlank(transfeeDtos)){
			return result;
		}else{
			result=JSONUtil.getJson(transfeeDtos);
		}
		return result;
	}

	/**
	 * 根据城市计算库存
	 * @param prodId 商品Id
	 * @param skuId skuId
	 * @param cityId 城市Id
	 * @param transportId 运费模板Id
 	 * @return
	 */
	@RequestMapping(value="/calSkuStocksByProv",method=RequestMethod.POST)
	@ResponseBody
	public Map<String,String> calSkuStocksByProv(Long prodId,Long skuId,Integer cityId,Long transportId,Long shopId,
												 @RequestParam(value = "storeId", required = false) Long storeId){
		// 如果有门店Id，计算门店库存
		if (AppUtils.isNotBlank(storeId) && -1 != storeId) {
			return stockService.calculateSkuStocksByCity(prodId, skuId,cityId,transportId,shopId, storeId);
		}
		Map<String,String> map = stockService.calculateSkuStocksByCity(prodId, skuId,cityId,transportId,shopId);
		return map;
	}

	/**
	 * 检验商品库存
	 * @param request
	 * @param response
	 * @param skuId
	 * @param productId
	 * @param buyNum
	 * @return
	 */
	@RequestMapping(value="/veryBuyNum/{productId}/{skuId}",method=RequestMethod.GET)
	@ResponseBody
	public String veryBuyNum(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Long productId,@PathVariable Long skuId,
			@RequestParam Integer buyNum
			) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user == null ? null: user.getUserId();
		if(buyNum<=0){
			log.warn("请输入正确的购买数量 userId = {}", userId);
			return "请输入正确的购买数量";
		}
		/*
		 * TODO  没有检查商品的仓库库存
		 */
		Product item=productService.getProductById(productId);
		if(item==null || !ProductStatusEnum.PROD_ONLINE.value().equals(item.getStatus())){
			log.warn("数据已发生改变，请刷新后再操作 userId = {}", userId);
			return "数据已发生改变，请刷新后再操作!";
		}


        if(user != null && item.getShopId().equals(user.getShopId())){
        	log.warn("自己的商品,不能购买 userId = {}", userId);
        	return "自己的商品,不能购买!";
        }

        Sku sku=skuService.getSku(skuId);
        if(sku==null || sku.getStatus().intValue()!=1){
        	log.warn("数据已发生改变，请刷新后再操作 userId = {}", userId);
        	return "数据已发生改变，请刷新后再操作!";
        }
        if(sku.getStocks()-buyNum>=0){
        	return Constants.SUCCESS;
        }
        String result = "当前库存数据不足，您当前最多能购买数量为 "+sku.getStocks()+"！";
        log.warn(result);
        return result;
	}

	/**
	 * 根据选择的省份，load出城市
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/load/cityList/{id}")
	public String loadCitys(HttpServletRequest request, HttpServletResponse response,@PathVariable Integer id) {
		List<City> cityList = locationService.getCity(id);
		request.setAttribute("cityList", cityList);
		return PathResolver.getPath(FrontPage.CITIES_PAGE);
	}

	/**
	 * 根据选择的城市， load出地区
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/load/areaList/{id}")
	public String loadAreas(HttpServletRequest request, HttpServletResponse response,@PathVariable Integer id) {
		List<Area> areaList = locationService.getArea(id);
		request.setAttribute("areaList", areaList);
		return PathResolver.getPath(FrontPage.AREA_PAGE);
	}

	/**
	 * 商品图片库
	 *
	 */
	@RequestMapping("/productGallery/{prodId}")
	public String productGallery(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId)
			throws Exception {
		if (AppUtils.isBlank(prodId)) {
			return PathResolver.getPath(CommonFowardPage.INDEX_QUERY);
		}

		ProductDetail prod = productService.getProdDetail(prodId);
		if (prod != null) {
			request.setAttribute("prod", prod);
			List<String> prodPics = new ArrayList<String>();
			List<ProdPropImage>  prodPropImages = prodPropImageService.getProdPropImageByProdId(prodId);
			if(AppUtils.isNotBlank(prodPropImages)){
				for (ProdPropImage prodPropImage : prodPropImages) {
					prodPics.add(prodPropImage.getUrl());
				}
			}else{
				// 查看商品的说明图片
				List<ImgFile> imgFiles = imgFileService.getAllProductPics(prodId);
				for (ImgFile imgFile : imgFiles) {
					prodPics.add(imgFile.getFilePath());
				}
			}
			if (AppUtils.isNotBlank(prodPics)) {
				request.setAttribute("prodPics", prodPics);
			}
			return PathResolver.getPath(FrontPage.PROD_PIC_GALLERY);
		} else {
			UserMessages uem = new UserMessages();
			uem.setTitle(ResourceBundleHelper.getString("product.not.found"));
			uem.setDesc(ResourceBundleHelper.getString("product.status.check"));
			uem.setCode(ErrorCodes.ENTITY_NO_FOUND);
			request.setAttribute(UserMessages.MESSAGE_KEY, uem);
			return PathResolver.getPath(CommonPage.ERROR_FRAME_PAGE);
		}
	}

	/**
	 *  商品查看历史
	 *
	 */
	@RequestMapping("/visitedprod")
	public String visitedProd(HttpServletRequest request, HttpServletResponse response) {
		List<Object> prodIds = VisitHistoryHelper.getVisitedProd(request);
		List<ProductDetail> products = new ArrayList<ProductDetail>();
		for (Object prodId : prodIds) {
			ProductDetail productDetail = productService.getProdDetail(Long.parseLong(prodId.toString()));
			if(productDetail!=null && ProductStatusEnum.PROD_ONLINE.value().equals(productDetail.getStatus())){
				products.add(productDetail);
			}
		}
		request.setAttribute("visitedProd", products);
		return PathResolver.getPath(FrontPage.VISITED_PROD);
	}

	/**
	 * 旧模版的浏览器模版
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/visitedprod_old")
	public String visitedprodForView(HttpServletRequest request, HttpServletResponse response) {
		List<Object> prodIds = VisitHistoryHelper.getVisitedProd(request);
		List<ProductDetail> products = new ArrayList<ProductDetail>();
		for (Object prodId : prodIds) {
			ProductDetail productDetail = productService.getProdDetail(Long.parseLong(prodId.toString()));
			if(productDetail!=null && ProductStatusEnum.PROD_ONLINE.value().equals(productDetail.getStatus())){
				products.add(productDetail);
			}
		}
		request.setAttribute("visitedProd", products);
		return PathResolver.getPath(FrontPage.VISITED_PROD_OLD);
	}

	/**
	 * 猜你喜欢
	 */
	@RequestMapping("/like")
	public String like(HttpServletRequest request, HttpServletResponse response, Integer count) {
		if (count == null || count <= 0) {
			return PathResolver.getPath(FrontPage.LIKE_PROD);
		}

		int num = count;

		List<ProductDetail> guesLikeProdList = new ArrayList<>();

		List<Object> prodIds = VisitHistoryHelper.getVisitedProd(request);
		List<ProductDetail> products = new ArrayList<>();
		for (Object prodId : prodIds) {
			ProductDetail productDetail = productService.getProdDetail(Long.parseLong(prodId.toString()));
			if (productDetail != null && ProductStatusEnum.PROD_ONLINE.value().equals(productDetail.getStatus())) {
				products.add(productDetail);
			}
		}

		//循环 浏览历史中 同分类下 的销量最高的几条商品
		if (AppUtils.isNotBlank(products)) {
			Map<Long, Long> catMap = new HashMap<>();
			for (ProductDetail productDetail : products) {
				if (productDetail.getCategoryId() != null) {
					catMap.put(productDetail.getCategoryId(), productDetail.getCategoryId());
				}
			}
			for (Long aLong : catMap.keySet()) {
				List<ProductDetail> likes = this.productService.getLikeProd(aLong);
				if (AppUtils.isNotBlank(likes)) {
					for (ProductDetail like : likes) {
						if (like != null && ProductStatusEnum.PROD_ONLINE.value().equals(like.getStatus())) {
							if (guesLikeProdList.size() >= num) {
								request.setAttribute("likes", guesLikeProdList);
								return PathResolver.getPath(FrontPage.LIKE_PROD);
							}
							guesLikeProdList.add(like);
							guesLikeProdList = guesLikeProdList.stream().distinct().collect(Collectors.toList());
						}
					}
				}
			}
		}
		//如果数量不够，从最新上架的商品中获取
		if (guesLikeProdList.size() < num) {
			List<Product> newProds = productService.getNewProds(num - guesLikeProdList.size());
			if (AppUtils.isNotBlank(newProds)) {
				for (Product product : newProds) {
					if(product != null && ProductStatusEnum.PROD_ONLINE.value().equals(product.getStatus())){
						ProductDetail productDetail = new ProductDetail();
						productDetail.setId(product.getId());
						productDetail.setCash(product.getCash());
						productDetail.setName(product.getName());
						productDetail.setPic(product.getPic());
						guesLikeProdList.add(productDetail);
					}
				}
			}
		}
		guesLikeProdList = guesLikeProdList.stream().distinct().collect(Collectors.toList());
		request.setAttribute("likes", guesLikeProdList);
		return PathResolver.getPath(FrontPage.LIKE_PROD);
	}

	/**
	 * 根据sku返回参与了哪些活动
	 * @param prodId 商品Id
	 * @param skuId skuId
 	 * @return
	 */

	@SuppressWarnings("all")
	@RequestMapping(value="/bySkuActivity",method=RequestMethod.POST)
	@ResponseBody
	public Map<String,String> bySkuActivity(Long prodId,Long skuId){
		Map resultMap=new HashMap<>();
		ProductDto prod = productService.getProductDto(prodId);
		Sku sku=skuService.getSku(skuId);
		prod.setSkuId(skuId);
		prod.setSkuType(sku.getSkuType());
		//查询团购
		if(prod.getIsGroup()==1){
			Group group=productService.getGroupPrice(prodId);
			if(AppUtils.isNotBlank(group)){
				Date nowDate = new Date();
				if(nowDate.getTime()>group.getEndTime().getTime()){//如果当前时间已经大于活动的结束时间就把商品的状态回显
					productService.updataIsGroup(prodId,0);	//设置不是团购商品
					prod.setIsGroup(0);
				}
			}
			resultMap.put("group", group);
		}

		//查询秒杀
		if("SECKILL".equals(prod.getSkuType())){
			Seckill seckill=productService.getSeckillPrice(prod.getActiveId(),prod.getProdId(),skuId);
			if (seckill!=null) {
				Date nowDate = new Date();
				if(nowDate.getTime()<seckill.getEndTime().getTime()){//如果当前时间已经大于活动的结束时间就把商品的状态回显
					resultMap.put("seckill",seckill);
				}
			}
		}

		//查询拍卖
		if("AUCTION".equals(prod.getSkuType())){
			Auctions auctions=productService.getAuctionsPrice(prod.getProdId(),skuId);
			if (auctions!=null) {
				Date nowDate = new Date();
				if(nowDate.getTime()<auctions.getEndTime().getTime()){//如果当前时间已经大于活动的结束时间就把商品的状态回显
					resultMap.put("auctions",auctions);
				}
			}
		}

		//查询预售
		if("PRESELL".equals(prod.getSkuType())){
			PresellProd presell=productService.getPresellProdPrice(prod.getProdId(),skuId);
			if (presell!=null) {
				Date nowDate = new Date();
				if(nowDate.getTime()<presell.getPreSaleEnd().getTime()){//如果当前时间已经大于活动的结束时间就把商品的状态回显
					resultMap.put("presell",presell);
				}
			}
		}
		resultMap.put("prod", prod);
		return resultMap;
	}

}
