/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.business.page.UserCenterFrontPage;
import com.legendshop.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.dao.SubDao;
import com.legendshop.business.page.FrontPage;
import com.legendshop.business.page.RedirectPage;
import com.legendshop.business.service.impl.PaymentUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.SubSettlementTypeEnum;
import com.legendshop.model.constant.SubTypeEnum;
import com.legendshop.model.constant.SubmitOrderStatusEnum;
import com.legendshop.model.dto.OrderAddParamDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.order.AddOrderMessage;
import com.legendshop.model.dto.order.OrderDetailsParamsDto;
import com.legendshop.model.entity.Invoice;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubItem;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.processor.DelayCancelOfferProcessor;
import com.legendshop.processor.ProdSnapshotProcessor;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.BasketService;
import com.legendshop.spi.service.InvoiceService;
import com.legendshop.spi.service.OrderCartResolverManager;
import com.legendshop.spi.service.OrderUtil;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.SubItemService;
import com.legendshop.spi.service.SubService;
import com.legendshop.spi.service.UserAddressService;
import com.legendshop.spi.service.UserDetailService;

/**
 * 
 * 订单控制器
 *
 * @author legendshop
 */
@Controller
@RequestMapping("/p")
public class OrderController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(OrderController.class);

	@Autowired
	private SubService subService;

	@Autowired
	private PaymentUtil paymentUtil;

	/** The basket service. */
	@Autowired
	private BasketService basketService;

	/** The user detail service. */
	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private PayTypeService payTypeService;

	@Autowired
	private UserAddressService userAddressService;

	@Autowired
	private InvoiceService invoiceService;

	@Autowired
	private SubDao subDao;

	/**
	 * 订单处理类
	 */
	@Autowired
	private OrderCartResolverManager orderCartResolverManager;

	@Autowired
	private ShopDetailService shopDetailService;

	// 订单常用处理工具类
	@Autowired
	private OrderUtil orderUtil;

	@Autowired
	private SubItemService subItemService;

	/** 订单快照 **/
	@Autowired
	private ProdSnapshotProcessor prodSnapshotProcessor;

	/** 订单延迟取消队列 **/
	@Autowired
	private DelayCancelOfferProcessor delayCancelOfferProcessor;

	/**
	 * 根据 basketId 删除 数据
	 * 
	 * @param request
	 * @param response
	 * @param basketId
	 * @return
	 */
	@RequestMapping("/deleteBasket")
	public @ResponseBody String deleteBasket(HttpServletRequest request, HttpServletResponse response, Long basketId) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(FrontPage.LOGIN_HINT);
		}
		if (basketId == null) {
			basketService.deleteBasketByUserId(user.getUserId());
		} else {
			basketService.deleteBasketById(basketId);
		}
		return Constants.SUCCESS;
	}

	/**
	 * 用户地址翻页
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/userAddressPage")
	public String UserAddressPage(HttpServletRequest request, HttpServletResponse response, String curPageNO) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		PageSupport<UserAddress> ps = userAddressService.UserAddressPage(curPageNO, userId);
		PageSupportHelper.savePage(request, ps);
		Long total = ps.getTotal();
		boolean flag = total.doubleValue() / ps.getPageSize() > Long.valueOf(curPageNO);
		request.setAttribute("flag", flag);
		return PathResolver.getPath(FrontPage.USERADDRESS_PAGE);
	}



	/**
	 * （新增）
	 * 发票选择列表弹窗
	 */
	@RequestMapping(value = "/invoiceListPop", method = RequestMethod.GET)
	public String invoiceListPop(HttpServletRequest request, HttpServletResponse response,String curPageNO,Integer invoiceType) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();

		if (AppUtils.isBlank(invoiceType)){

			//默认查询普通发票
			invoiceType = 1;
		}

		List<Invoice> invoiceList =  invoiceService.getInvoiceByType(userName,invoiceType);

		request.setAttribute("invoiceList",invoiceList);
		request.setAttribute("invoiceType",invoiceType);
		return PathResolver.getPath(FrontPage.INVOICE_LIST_POP);
	}

	/**
	 * （旧）
	 * 发票信息翻页
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/invoicePage")
	public String invoicePage(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		
		PageSupport<Invoice> ps = invoiceService.getInvoicePage(userName, curPageNO, 10);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(FrontPage.INVOICE_PAGE);
	}

	/**
	 * 删除发票信息
	 * 
	 * @param request
	 * @param response
	 * @param invoiceId
	 * @return
	 */
	@RequestMapping("/delInvoice/{invoiceId}")
	public @ResponseBody String delInvoice(HttpServletRequest request, HttpServletResponse response, @PathVariable Long invoiceId) {
		invoiceService.delById(invoiceId);
		return Constants.SUCCESS;
	}

	/**
	 * 保存发票信息
	 * 
	 * @param request
	 * @param response
	 * @param invoice
	 * @return
	 */
	@RequestMapping(value = "/saveInvoice", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> saveInvoice(HttpServletRequest request, HttpServletResponse response, Invoice invoice) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		Map<String, Object> result = new HashMap<String, Object>();
		invoice.setCreateTime(new Date());
		invoice.setUserId(userId);
		invoice.setUserName(userName);
		// 保存新的发票信息
		Long invoiceId = invoiceService.saveInvoice(invoice);
		if (invoiceId > 0) {
			// 将其变为默认发票信息
			invoiceService.updateDefaultInvoice(invoiceId, userName);
			result.put("success", Constants.SUCCESS);
			result.put("invoiceId", invoiceId);
		} else {
			result.put("success", Constants.FAIL);
			result.put("invoiceId", invoiceId);
		}
		return result;
	}

	/**
	 * 保存发票信息
	 * 
	 * @param request
	 * @param response
	 * @param invoiceId
	 * @return
	 */
	@RequestMapping(value = "/updateDefaultInvoice/{invoiceId}", method = RequestMethod.POST)
	public @ResponseBody String updateDefaultInvoice(HttpServletRequest request, HttpServletResponse response, @PathVariable Long invoiceId) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		
		// 将其变为默认发票信息
		invoiceService.updateDefaultInvoice(invoiceId, userName);
		return Constants.SUCCESS;
	}

	/**
	 * 新增地址
	 * 
	 * @param request
	 * @param response
	 * @param addrId
	 * @return
	 */
	@RequestMapping(value = "/addNewAddress", method = RequestMethod.GET)
	public String addNewAddress(HttpServletRequest request, HttpServletResponse response, Long addrId) {
		if (addrId != null) {
			UserAddress userAddress = userAddressService.getUserAddress(addrId);
			request.setAttribute("userAddress", userAddress);
		}
		return PathResolver.getPath(FrontPage.ADD_ADDRESS_PAGE);
	}

	/**
	 * 保存新增地址，并将其变为默认地址
	 * 
	 * @param request
	 * @param response
	 * @param userAddress
	 * @return
	 */
	@RequestMapping(value = "/saveUserAddress", method = RequestMethod.POST)
	@ResponseBody
	public String saveUserAddress(HttpServletRequest request, HttpServletResponse response, UserAddress userAddress) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		if (checkMaxNumber(userName) && AppUtils.isBlank(userAddress.getAddrId())) {
			return Constants.MAX_NUM;
		}
		SafeHtml safeHtml = new SafeHtml();
		userAddress.setReceiver(safeHtml.makeSafe(userAddress.getReceiver()));
		userAddress.setSubAdds(safeHtml.makeSafe(userAddress.getSubAdds()));
		userAddress.setMobile(safeHtml.makeSafe(userAddress.getMobile()));
		userAddress.setSubPost(safeHtml.makeSafe(userAddress.getSubPost()));
		userAddress.setAliasAddr(safeHtml.makeSafe(userAddress.getAliasAddr()));
		userAddress.setModifyTime(new Date());
		if (userAddress.getAddrId() == null) { // for save
			userAddress.setUserName(userName);
			userAddress.setUserId(userId);
			userAddress.setCreateTime(new Date());
			userAddress.setCommonAddr("0");
			userAddress.setVersion(1L);
		}

		userAddress.setStatus(Constants.ONLINE);

		// 保存新增地址,将其变为默认地址
		Long addrId = userAddressService.saveUserAddressAndDefault(userAddress, userId);
		return Constants.SUCCESS;
	}

	/**
	 * 检查地址是否超过20个
	 * 
	 * @param userName
	 * @return
	 */
	private boolean checkMaxNumber(String userName) {
		Long MaxNumber = userAddressService.getMaxNumber(userName);
		if (MaxNumber >= 20) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 删除常用地址信息
	 * 
	 * @param request
	 * @param response
	 * @param addrId
	 * @return
	 */
	@RequestMapping(value = "/delUserAddress", method = RequestMethod.POST)
	public @ResponseBody String delUserAddress(HttpServletRequest request, HttpServletResponse response, Long addrId) {
		UserAddress userAddress = userAddressService.getUserAddress(addrId);
		userAddressService.deleteUserAddress(userAddress);
		return Constants.SUCCESS;
	}

	/**
	 * 编辑常用地址信息
	 * 
	 * @param request
	 * @param response
	 * @param addrId
	 * @return
	 */
	@RequestMapping(value = "/editUserAddress", method = RequestMethod.POST)
	public String editUserAddress(HttpServletRequest request, HttpServletResponse response, Long addrId) {
		if (addrId != null) {
			UserAddress userAddress = userAddressService.getUserAddress(addrId);
			request.setAttribute("userAddress", userAddress);
		}
		return PathResolver.getPath(FrontPage.EDITADDRESS_PAGE);
	}

	/**
	 * 根据addrId 查出收货地址并将其设为默认
	 * 
	 * @param request
	 * @param response
	 * @param addrId
	 * @return
	 */
	@RequestMapping(value = "/consigneeAddress", method = RequestMethod.POST)
	public String consigneeAddress(HttpServletRequest request, HttpServletResponse response, Long addrId) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		UserAddress userAddress = userAddressService.getUserAddress(addrId);

		if (AppUtils.isBlank(userAddress) || !userAddress.getUserId().equals(userId)) {
			return "";
		}

		// 将其变为默认地址
		userAddressService.updateDefaultUserAddress(addrId, userId);

		request.setAttribute("defaultAddress", userAddress);
		return PathResolver.getPath(FrontPage.CONSIGNEE_ADDRESS);
	}

	/**
	 * 根据addrId 将其设为默认
	 * 
	 * @param request
	 * @param response
	 * @param addrId
	 * @return
	 */
	@RequestMapping(value = "/consigneeAddressAjdx", method = RequestMethod.POST)
	public @ResponseBody String consigneeAddressAjdx(HttpServletRequest request, HttpServletResponse response, Long addrId) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		// 将其变为默认地址
		userAddressService.updateDefaultUserAddress(addrId, userId);
		return Constants.SUCCESS;
	}

	/**
	 * 去往订单详情页面 ----下单
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/orderDetails")
	public String orderDetails(HttpServletRequest request, HttpServletResponse response, OrderDetailsParamsDto reqParams) {
		com.legendshop.security.model.SecurityUserDetail userDetail = UserManager.getUser(request);
		List<Long> shopCartItems = reqParams.getShopCartItems();
		Boolean isBuyNow = reqParams.getBuyNow();
		if (AppUtils.isBlank(shopCartItems) && (AppUtils.isBlank(isBuyNow) || !isBuyNow)) {
			return PathResolver.getPath(RedirectPage.MYSHOPCART);
		}

		/* 根据类型查询订单列表 */
		Map<String, Object> params = new HashMap<String, Object>();
		if (isBuyNow) {
			JSONObject jsonObject = orderUtil.buyNow(isBuyNow, reqParams.getProdId(), reqParams.getSkuId(), reqParams.getCount());
			if (!jsonObject.getBooleanValue("result")) {
				return jsonObject.getString("data");
			}
			ShopCartItem shopCartItem = jsonObject.getObject("data", ShopCartItem.class);
			params.put("buyNow", true);
			params.put("shopCartItem", shopCartItem);
		}
		params.put("userId", userDetail.getUserId());
		params.put("userName", userDetail.getUsername());
		params.put("ids", shopCartItems);

		// 根据您的下单, 查询用户的订单
		String type = reqParams.getType();
		type = (type == null ? "NORMAL" : type);
		CartTypeEnum typeEnum = CartTypeEnum.fromCode(type);
		UserShopCartList shopCartList = orderCartResolverManager.queryOrders(typeEnum, params);
		if (AppUtils.isBlank(shopCartList)) {
			request.setAttribute("message", "数据已经发送改变,请重新刷新后操作!");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}

		// 检查金额： 如果促销导致订单金额为负数
		if (shopCartList.getOrderActualTotal() < 0) {
			request.setAttribute("message", "订单金额有误,下单失败");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}

		// 获取卖家是否开启发票功能
		Long shopId = shopCartList.getShopCarts().get(0).getShopId();
		ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);

		if (AppUtils.isBlank(shopDetail.getSwitchInvoice())) {
			Integer switchInvoice = 0;
			request.setAttribute("switchInvoice", switchInvoice);
		}
		request.setAttribute("switchInvoice", shopDetail.getSwitchInvoice());
		Invoice userInvoice = null;
		if (AppUtils.isNotBlank(shopDetail.getSwitchInvoice()) && shopDetail.getSwitchInvoice().equals(1)) {

			if (AppUtils.isBlank(reqParams.getInvoiceId())){
				// 查出买家的默认发票内容
				userInvoice = invoiceService.getDefaultInvoice(userDetail.getUserId());
			}else{
				// 查出传递过来的发票
				userInvoice = invoiceService.getInvoice(reqParams.getInvoiceId(),userDetail.getUserId());
			}
		}
		request.setAttribute("userdefaultInvoice", userInvoice);

		String items = JSONUtil.getJson(shopCartItems); // 用于刷新页面使用 TODO sessionStorge 改造
		if (AppUtils.isNotBlank(shopCartItems)) {
			request.setAttribute("shopCartItems", items.substring(1, items.length() - 1));
		}
		// 设置订单提交令牌,防止重复提交
		Integer token = CommonServiceUtil.generateRandom();
		request.getSession().setAttribute(Constants.TOKEN, token);
		subService.putUserShopCartList(type, userDetail.getUserId(), shopCartList);
		request.setAttribute("userShopCartList", shopCartList);

		request.setAttribute("isBuyNow", isBuyNow);
		request.setAttribute("reqParams", reqParams);
		request.setAttribute("type", type);
		// 清理未付款订单缓存
		subDao.cleanUnpayOrderCount(userDetail.getUserId());
		String path = PathResolver.getPath(FrontPage.ORDERDETAILS);

		return path;
	}

	/**
	 * 提交订单
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/submitOrder", method = RequestMethod.POST)
	public @ResponseBody AddOrderMessage submitOrder(HttpServletRequest request, HttpServletResponse response, OrderAddParamDto paramDto) {
		// 参数检查
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();


		// 取session中保存的token
		Integer sessionToken = (Integer) request.getSession().getAttribute(Constants.TOKEN);
		// 订单类型
		String type = (paramDto.getType() == null ? "NORMAL" : paramDto.getType());
		// 判断是否重复提交
		CartTypeEnum typeEnum = CartTypeEnum.fromCode(type);

		// 检查输入参数
		AddOrderMessage message = orderUtil.checkSubmitParam(userId, sessionToken, typeEnum, paramDto);

		if (message.hasError()) {// 有错误就返回
			return message;
		}

		// TODO 校验用户参数，SpringSession后台过期机制有问题
		if (!userDetailService.isUserExist(userName)) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("用户不存在！");
			return message;
		}

		// get cache 从上个页面获取设置进入缓存的订单
		UserShopCartList userShopCartList = subService.findUserShopCartList(type, userId);
		if (AppUtils.isBlank(userShopCartList)) {
			message.setCode(SubmitOrderStatusEnum.NOT_PRODUCTS.value());
			return message;
		}

		// 检查金额： 如果促销导致订单金额为负数
		if (userShopCartList.getOrderActualTotal() < 0) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("订单金额为负数,下单失败!");
			return message;
		}

		// 处理订单的卖家留言备注，多个商家拼接在一起
		try {
			orderUtil.handlerRemarkText(paramDto.getRemarkText(), userShopCartList);
		} catch (Exception e) {
			message.setCode(SubmitOrderStatusEnum.PARAM_ERR.value());
			message.setMessage("下单失败，请联系商城管理员或商城客服");
			return message;
		}

		/** 根据id获取收货地址信息 **/
		UserAddress userAddress = userShopCartList.getUserAddress();
		if (AppUtils.isBlank(userAddress)) {
			userAddress = userAddressService.getDefaultAddress(userId);
			if (AppUtils.isBlank(userAddress)) {
				message.setCode(SubmitOrderStatusEnum.NO_ADDRESS.value());
				return message;
			}
			userShopCartList.setUserAddress(userAddress);
		}

		// 处理运费模板
		String result = orderUtil.handlerDelivery(paramDto.getDelivery(), userShopCartList);
		if (!Constants.SUCCESS.equals(result)) {
			message.setCode(result);
			return message;
		}

		// 处理优惠券
		orderUtil.handleCouponStr(userShopCartList);

		// 验证成功，清除令牌
		request.getSession().removeAttribute(Constants.TOKEN);
		userShopCartList.setUserId(userId);
		userShopCartList.setUserName(userName);
		userShopCartList.setInvoiceId(paramDto.getInvoiceId());
		userShopCartList.setUserAddress(userAddress);
		userShopCartList.setPayManner(paramDto.getPayManner());
		userShopCartList.setType(typeEnum.toCode());

		// 商家是否开启发票
		userShopCartList.setSwitchInvoice(paramDto.getSwitchInvoice());

		/**
		 * 处理订单
		 */
		// 订单号集合
		List<String> subIds = null;
		try {
			subIds = orderCartResolverManager.addOrder(typeEnum, userShopCartList);
		} catch (Exception e) {
			log.error(e.getMessage());
			if (e instanceof BusinessException) {
				message.setCode(SubmitOrderStatusEnum.ERR.value());
				message.setMessage(e.getMessage());
				return message;
			}
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("下单失败,请联系商城管理员或商城客服");
			return message;
		}
		if (AppUtils.isBlank(subIds)) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("下单失败,请联系商城管理员或商城客服");
			return message;
		// 下单成功
		} else {

			// offer 延时取消队列
			delayCancelOfferProcessor.process(subIds);
			// 发送网站快照备份
			prodSnapshotProcessor.process(subIds);

		}
		// 标记回收
		userShopCartList = null;
		// 更新session购物车数量
		subService.evictUserShopCartCache(type, userId);

		StringBuffer url = new StringBuffer();
		url.append("/p/orderSuccess?subNums=");
		for (int i = 0; i < subIds.size(); i++) {
			url.append(subIds.get(i)).append(",");
		}
		String path = url.substring(0, url.length() - 1);
		message.setResult(true);
		message.setUrl(path);
		return message;
	}

	/**
	 * 订单提交成功返回
	 * 
	 * @param request
	 * @param response
	 * @param subNums
	 * @return
	 */
	@RequestMapping("/orderSuccess")
	public String orderSuccess(HttpServletRequest request, HttpServletResponse response, String[] subNums) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		if (AppUtils.isBlank(userId)) {
			return PathResolver.getPath(FrontPage.LOGIN);
		} else if (AppUtils.isBlank(subNums)) {
			return PathResolver.getPath(RedirectPage.MYORDER);
		}
		List<Sub> subs = subService.getSubBySubNums(subNums, userId, OrderStatusEnum.UNPAY.value());
		if (AppUtils.isBlank(subs)) {
			return PathResolver.getPath(RedirectPage.MYORDER);
		}

		// 获取订单自动取消的时间
		int cancelMins = subService.getOrderCancelExpireDate();

		String subIds = "";
		List<Sub> orderList = new ArrayList<Sub>();
		/** 订单支付金额 */
		Double totalAmount = 0.0;

		for (Sub sub : subs) {

			Date subDate = sub.getSubDate();
			Date rollMinute = DateUtils.rollMinute(subDate, 10);

			if(new Date().compareTo(rollMinute) == 1 && !SubTypeEnum.PRESELL.value().equals(sub.getSubType())){
				request.setAttribute("message", "订单操作失败,已超过有效支付时间!");
				return PathResolver.getPath(FrontPage.OPERATION_ERROR);
			}

			if (SubTypeEnum.PRESELL.value().equals(sub.getSubType())) {
				request.setAttribute("message", "订单操作失败,预售订单不能合并支付!");
				return PathResolver.getPath(FrontPage.OPERATION_ERROR);
			}
			orderList.add(sub);
			totalAmount = Arith.add(totalAmount, sub.getActualTotal());
			subIds += sub.getSubNumber() + ",";
		}
		subIds = subIds.substring(0, subIds.length() - 1);

		/*
		 * 首先先查询这个支付单据有没有已经支付成功的单据信息
		 */
		if (paymentUtil.checkRepeatPay(subNums, userId, SubSettlementTypeEnum.USER_ORDER_PAY.value())) {
			PaymentLog.error("===>请勿重复支付");
			request.setAttribute("message", "请勿重复支付");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}

		Double availablePredeposit = userDetailService.getAvailablePredeposit(userId);
		Double coin = userDetailService.getUserCoin(userId); // 用户的金币

		// 获取 支付方式
		Map<String, Integer> payTypesMap = payTypeService.getPayTypeMap();

		request.setAttribute("payTypesMap", payTypesMap);
		request.setAttribute("cancelMins", cancelMins);
		request.setAttribute("subSettlementType", SubSettlementTypeEnum.USER_ORDER_PAY.value());
		request.setAttribute("subNums", subIds);
		request.setAttribute("orderList", orderList);
		request.setAttribute("totalAmount", totalAmount);
		request.setAttribute("availablePredeposit", availablePredeposit == null ? 0 : availablePredeposit);
		request.setAttribute("coin", coin == null ? 0 : coin);
		request.getSession().removeAttribute(Constants.PASSWORD_CALLBACK);
		return PathResolver.getPath(FrontPage.ORDERSUCCESS);
	}

	/**
	 * 再来一单，先进入购物车
	 * 
	 * @param request
	 * @param response
	 * @param subNumber
	 * @return
	 */
	@RequestMapping(value = "/addAnotherSub", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto addAnotherSub(HttpServletRequest request, HttpServletResponse response, String subNumber) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		if (AppUtils.isNotBlank(userName)) {
			Sub sub = subService.getSubBySubNumber(subNumber);
			List<SubItem> subItemList = subItemService.getSubItem(subNumber);
			for (SubItem subItem : subItemList) {
				basketService.saveToCart(userId, subItem.getProdId(), (int) subItem.getBasketCount(), subItem.getSkuId(), sub.getShopId(), subItem.getDistUserName(), null);
			}
			return ResultDtoManager.success();
		}
		return ResultDtoManager.fail(0, "创建失败");
	}

	/**
	 * 再来一单，直接进入下单界面
	 * 
	 * @param request
	 * @param response
	 * @param subNumber
	 * @return
	 */
	@RequestMapping(value = "/addAnotherSub2", method = RequestMethod.POST)
	@ResponseBody
	public List<Long> addAnotherSub2(HttpServletRequest request, HttpServletResponse response, String subNumber) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		List<Long> cartList = new ArrayList<Long>();
		if (AppUtils.isNotBlank(userName)) {
			Sub sub = subService.getSubBySubNumber(subNumber);
			List<SubItem> subItemList = subItemService.getSubItem(subNumber);
			for (SubItem subItem : subItemList) {
				Long basketId = basketService.saveToCart(userId, subItem.getProdId(), (int) subItem.getBasketCount(), subItem.getSkuId(), sub.getShopId(), subItem.getDistUserName(), null);
				cartList.add(basketId);
			}
		}
		return cartList;
	}
}
