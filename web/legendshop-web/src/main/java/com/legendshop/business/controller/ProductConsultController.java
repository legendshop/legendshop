/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.util.XssFilterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.business.page.FrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProductConsult;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ProductConsultService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SafeHtml;
import com.legendshop.web.helper.IPHelper;

/**
 * 商品咨询控制器
 */
@Controller
@RequestMapping("/productConsult")
public class ProductConsultController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(ProductConsultController.class);

	/** The product consult service. */
	@Autowired
	private ProductConsultService productConsultService;

	/**
	 * 到prodconsults页面， 包括prodconsultlist页面
	 */
	@RequestMapping("/list/{prodId}")
	public String list(HttpServletRequest request, HttpServletResponse response, String curPageNO, Integer pointType,
			@PathVariable Long prodId) {
		parseConsult(request, response, curPageNO, pointType, prodId);
		return PathResolver.getPath(FrontPage.PRODUCT_CONSULTS);
	}

	/**
	 * 到 prodconsultlist 页面
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param pointType
	 * @param prodId
	 * @return
	 */
	@RequestMapping("/listcontent/{prodId}")
	public String listcontent(HttpServletRequest request, HttpServletResponse response, String curPageNO, Integer pointType,
			@PathVariable Long prodId) {
		parseConsult(request, response, curPageNO, pointType, prodId);
		return PathResolver.getPath(FrontPage.PRODUCT_CONSULTS_LIST);
	}

	private void parseConsult(HttpServletRequest request, HttpServletResponse response, String curPageNO, Integer pointType,
			Long prodId) {
		if(AppUtils.isBlank(curPageNO)){
			curPageNO = "1"; //默认第一页开始， 缓存不能传递空值
		}
		
		if(AppUtils.isBlank(pointType)){
			pointType = 0; //方便更新缓存
		}
		try{
			Integer.parseInt(curPageNO);
		}catch(NumberFormatException e){
			curPageNO = "1";
		}
		PageSupport<ProductConsult> ps = productConsultService.getProductConsultForList(curPageNO, pointType, prodId);

		// 重新命名，防止跟同一个页面的其他列表冲突
		request.setAttribute("prodCousultList", ps.getResultList());
		request.setAttribute("prodCousultCurPageNO", curPageNO);
		request.setAttribute("prodId", prodId);
		request.setAttribute("prodCousultTotal", ps.getTotal());
		request.setAttribute("prodCousultPageSize", ps.getPageSize());
	}


	/**
	 * Save.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param productConsult
	 *            the product consult
	 * @return the string
	 */
	@RequestMapping(value = "/save")
	public @ResponseBody
	Integer save(HttpServletRequest request, HttpServletResponse response, ProductConsult consult) {
		// only for update
		SecurityUserDetail user = UserManager.getUser(request);
		if (user == null) {
			log.debug("save product consult required user login before");
			return -1;
		}
		String content = consult.getContent();
		if (AppUtils.isBlank(content) || content.length() < 5 || content.length() > 20000) {
			// 检查长度
			return -2;
		}
		
		// 频率检查
		long frequency = productConsultService.checkFrequency(consult);
		if (frequency > 0) {
			return -3;
		}

		consult.setContent(XssFilterUtil.cleanXSS(content));
		consult.setRecDate(new Date());
		consult.setPostip(IPHelper.getIpAddr(request));
		consult.setUserId(user.getUserId());
		consult.setAskUserName(user.getUsername());
		consult.setReplySts(0);
		consult.setDelSts(0);
		productConsultService.saveProductConsult(consult);
		return 0; // OK
	}

}
