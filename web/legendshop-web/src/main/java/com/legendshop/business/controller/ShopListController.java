/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.xssfilter.XssDtoFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.legendshop.business.page.FrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.dto.ShopSearchParamDto;
import com.legendshop.model.dto.ShopSeekDto;
import com.legendshop.spi.service.ShopDetailService;

/**
 * 商家分类
 * 
 * @author Tony
 * 
 */
@Controller
public class ShopListController extends BaseController {

	@Autowired
	private ShopDetailService shopDetailService;
	/**
	 * 用于填充三级商品分类的select options
	 *
	 * @return
	 */
	@RequestMapping(value = "/shop/seek", method = RequestMethod.GET)
	public String loadShopList(HttpServletRequest request, HttpServletResponse response) {
		ShopSearchParamDto paramDto = new ShopSearchParamDto();
		paramDto.setPageSize(5);
		paramDto.setCurPageNO(1);
		paramDto.setStatus(ShopStatusEnum.NORMAL.value());
		paramDto.setProdCount(5);
		PageSupport<ShopSeekDto> ps = shopDetailService.queryShopSeekList(paramDto);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(FrontPage.SHOP_SEEK);
	}


}
