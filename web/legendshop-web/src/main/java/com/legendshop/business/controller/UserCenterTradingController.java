/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import com.legendshop.core.base.BaseController;

/**
 * The Class UserCenterTradingController.
 */
@Controller
public class UserCenterTradingController extends BaseController {

	/** The log. */
	private final Logger logger = LoggerFactory.getLogger(UserCenterTradingController.class);

}
