/*
 * 
* LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.business.page.UserCenterFrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.ExpensesRecord;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ExpensesRecordService;
import com.legendshop.util.AppUtils;

/**
 * 消费记录控制器
 *
 */
@Controller
@RequestMapping("/p")
public class ExpensesRecordController extends BaseController {
	@Autowired
	private ExpensesRecordService expensesRecordService;

	@RequestMapping("/expensesRecord")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		int pageSize = 10;
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		PageSupport<ExpensesRecord> ps = expensesRecordService.getExpensesRecord(curPageNO, userId, pageSize);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(UserCenterFrontPage.EXPENSES_RECORD_LIST); 
	}

	/**
	 * 删除消费记录
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/expensesRecord/delete/{id}", method = RequestMethod.POST)
	public @ResponseBody String expensesRecordDel(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		boolean result = expensesRecordService.deleteExpensesRecord(userId, id);
		if (result) {
			return Constants.SUCCESS;
		} else {
			return Constants.FAIL;
		}
	}

	/**
	 * 批量删除消费记录
	 * 
	 * @param request
	 * @param response
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "/deleteExpensesRecord")
	public @ResponseBody String deleteExpensesRecord(HttpServletRequest request, HttpServletResponse response, String ids) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (user == null) {
			return Constants.FAIL;
		}
		if (AppUtils.isNotBlank(ids)) {
			expensesRecordService.deleteExpensesRecord(user.getUserId(), ids);
		}
		return Constants.SUCCESS;
	}
}
