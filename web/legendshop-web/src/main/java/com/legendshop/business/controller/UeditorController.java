///*
// *
// * LegendShop 多用户商城系统
// *
// *  版权所有,并保留所有权利。
// *
// */
//package com.legendshop.business.controller;
//
//import java.io.IOException;
//import java.net.URL;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.commons.fileupload.FileUploadException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//import com.baidu.ueditor.ActionEnter;
//import com.baidu.ueditor.define.ActionMap;
//import com.baidu.ueditor.define.AppInfo;
//import com.baidu.ueditor.define.BaseState;
//import com.legendshop.core.base.BaseController;
//import com.legendshop.security.UserManager;
//import com.legendshop.security.model.SecurityUserDetail;
//import com.legendshop.spi.manager.AttachmentManager;
//
///**
// * 百度Ueditor富文本编辑器配置
// */
//@Controller
//@RequestMapping("/s/ueditor")
//public class UeditorController extends BaseController {
//
//    @Autowired
//    private AttachmentManager attachmentManager;
//
//    private String configFilePath; //配置文件的地址
//
//    /**
//     * Ueditor上传文件组件
//     *
//     * @param request
//     * @param response
//     * @return
//     */
//    @RequestMapping("/config")
//    public void query(HttpServletRequest request, HttpServletResponse response) throws FileUploadException, IOException {
//        response.setContentType("application/json; charset=utf-8");//返回JSON
//        String actionType = request.getParameter("action");
//        if (actionType == null || !ActionMap.mapping.containsKey(actionType)) {
//            response.getWriter().print(new BaseState(false, AppInfo.INVALID_ACTION).toJSONString());
//            return;
//        }
//
//        SecurityUserDetail userDetail = UserManager.getUser(request);
//        if (userDetail == null) {
//            response.getWriter().print(getError("您尚未登录"));
//            return;
//        }
//        String rootPath = request.getServletContext().getRealPath("/");
//        String result = new ActionEnter(attachmentManager, userDetail, request, rootPath, getPath(request)).exec();
//        response.getWriter().print(result);
//        return;
//    }
//
//    /**
//     * 计算配置文件的路径
//     * @param request
//     * @return
//     */
//    private String getPath(HttpServletRequest request) {
//        if (configFilePath != null) {
//            return configFilePath;
//        } else {
//            String rootPath = request.getServletContext().getRealPath("/");
//            URL url = UeditorController.class.getClassLoader().getResource("/config.json");
//            configFilePath = url.getPath();
//            return url.getPath();
//        }
//    }
//
//    /**
//     * 返回错误信息
//     *
//     * @param message
//     * @return
//     */
//    private String getError(String message) {
//        return new BaseState(false, message).toJSONString();
//    }
//
//}
