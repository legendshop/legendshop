/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.business.page.FrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.BrandStatusEnum;
import com.legendshop.model.entity.Brand;
import com.legendshop.spi.service.BrandService;

/**
 * 品牌控制器
 * 
 * @author George
 * 
 */

@Controller
public class BrandController extends BaseController {
	
	@Autowired
	private BrandService brandService;
	
	/** The log. */
	private final Logger logger = LoggerFactory.getLogger(IndexController.class);

	@RequestMapping("/allbrands")
	public String allBrands(HttpServletRequest request, HttpServletResponse response) {
		List<Brand> list=brandService.getAllCommentBrand();
		request.setAttribute("allbrands",list);

		return PathResolver.getPath(FrontPage.ALL_BRANDS);
	}
	
	/**
	 * 用于填充三级商品下的品牌分类的select options
	 * @param request
	 * @param response
	 * @param sortId
	 * @return
	 */
	@RequestMapping("/queryBrands")
	public String queryBrands(HttpServletRequest request,HttpServletResponse response, String curPageNO){
		PageSupport<Brand> ps =  brandService.queryBrands(curPageNO, BrandStatusEnum.BRAND_ONLINE.value());
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(FrontPage.BRAND_LOAD_PAGE);
	}

}
