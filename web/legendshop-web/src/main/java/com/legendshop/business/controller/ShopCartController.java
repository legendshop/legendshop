/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.entity.store.Store;
import com.legendshop.spi.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.business.page.FrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.handler.ShopCartHandler;
import com.legendshop.framework.handler.PluginRepository;
import com.legendshop.framework.plugins.PluginManager;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.LoginUserTypeEnum;
import com.legendshop.model.constant.PluginIdEnum;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.constant.SaveToCartStatusEnum;
import com.legendshop.model.dto.BasketCookieDto;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.marketing.MarketingDto;
import com.legendshop.model.entity.Basket;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.Sku;
import com.legendshop.model.entity.store.StoreSku;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;

/**
 * 购物车处理中心
 * @author tony
 */
@Controller
@RequestMapping("/shopCart")
public class ShopCartController extends BaseController{

	private final Logger log = LoggerFactory.getLogger(ShopCartController.class);
	
	@Autowired
	private SkuService skuService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ShopCartHandler shopCartHandler;
	
	@Autowired
	private BasketService basketService;
	
	@Autowired
	private SubService subService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private OrderUtil orderUtil;
	
	@Autowired
	private StoreSkuService storeSkuService;

	@Autowired
	private StoreService storeService;
	
	private volatile static PluginManager pluginManager;
	
	static{
		pluginManager=PluginRepository.getInstance();
	}
	
	
	/**
	 * 购物车商品数
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value ="/count" , method = RequestMethod.GET)
	public @ResponseBody int count(HttpServletRequest request, HttpServletResponse response) {
		int allCount = 0;
		SecurityUserDetail user = UserManager.getUser(request);
		//用户是否登录
		boolean isUserLogined = false;
		if(user != null) {
			 isUserLogined = LoginUserTypeEnum.USER.value().equals(user.getLoginUserType());
		}
		
		if(!isUserLogined){
			allCount = shopCartHandler.getShopCartAllCount(request);
		}else{
			allCount = basketService.getBasketCountByUserId(user.getUserId());
		}
		return allCount;
	}
	
	
	/**
	 * 立即购买,也会加入到购物车,只不过直接重定向到订单详情页
	 * @param request
	 * @param response
	 * @param prodId  商品ID
	 * @param skuId skuID
	 * @param count 商品数量
	 * @param distUserName 分销用户
	 * @return
	 */
	@RequestMapping(value ="/buyNow" , method = RequestMethod.GET)
	@ResponseBody
	public JSONObject buyNow(HttpServletRequest request, HttpServletResponse response,
		@RequestParam Long prodId,
		@RequestParam Long skuId,
		@RequestParam int count,String distUserName) {
		JSONObject result = new JSONObject();

		SecurityUserDetail user = UserManager.getUser(request);
		
		//用户是否登录
		boolean isUserLogined = false;
		if(user != null) {
			 isUserLogined = LoginUserTypeEnum.USER.value().equals(user.getLoginUserType());
		}
		
		if (!isUserLogined) {//用户还没有登录
			result.put("code", -1);
			result.put("result", SaveToCartStatusEnum.NO_LOGIN.toString());
			return result;
		}
		Product product = productService.getProductById(prodId);
		String status = orderUtil.checkCartParams(product,skuId,count,user.getUserId());
		
		//如果商品不可以购买
		if(!Constants.SUCCESS.equals(status)){
			result.put("code", -1);
			result.put("result",status);
			return result;
		}
		
		//用户已经登录
		Long basketId = basketService.saveToCart(user.getUserId(), prodId, count, skuId,product.getShopId(),distUserName,null);
		result.put("code", 1);
		result.put("result",basketId);
		return result;
	}
	
	/**
	 * 获取购物车信息
	 */
	@RequestMapping(value ="/shopBuy" , method = RequestMethod.GET)
	public String shopBuy(HttpServletRequest request, HttpServletResponse response) {
		//构建购物车信息
		buildBuyTable(request);
		
		return PathResolver.getPath(FrontPage.BUY); 
	}
	
	/**
	 * 异步获取 购物车的 信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value ="/buyTable" , method = RequestMethod.GET)
	public String buyTable(HttpServletRequest request, HttpServletResponse response) {
		//构建购物车信息
		buildBuyTable(request);
		
		return PathResolver.getPath(FrontPage.BUY_TABLE); 
	}
	
	/**
	 * 构建购物车信息
	 * @param request
	 */
	private void buildBuyTable(HttpServletRequest request){
		
		SecurityUserDetail user = UserManager.getUser(request);
		List<Basket> baskets = null;
		List<ShopCartItem> cartItems=null;
		
		if(!UserManager.isUserLogined(user)){
			//没有登录，从cookie获取购物车信息
			baskets = shopCartHandler.getShopCart(request);
			cartItems = basketService.getShopCartItems(baskets);
			
			//处理购物车项添加加入购物车时选中的门店ID
			disposeStore(baskets,cartItems);
			
		}else{
			// 已经登录，从数据库获取购物车
			cartItems = basketService.getShopCartByUserId(user.getUserId());
			request.setAttribute("userName", user.getUsername());
		}
		
		if(AppUtils.isNotBlank(cartItems)){
			// 购物车根据来源分组,按照商城来分组
			UserShopCartList userShopCartList= basketService.getShoppingCarts(cartItems);
			
			if(userShopCartList!=null && AppUtils.isNotBlank(userShopCartList.getShopCarts())){
				
				// 判断这个购物车的商品是否有支持门店的商品
				boolean existStorePlugin=pluginManager.isPluginRunning(PluginIdEnum.STORE.value());
				// 是否支持门店插件
				if(existStorePlugin){
					List<ShopCarts> carts=userShopCartList.getShopCarts();
					for (int i = 0; i < carts.size(); i++) {
						ShopCarts shopCarts=carts.get(i);
						List<MarketingDto> marketingDtos = shopCarts.getMarketingDtoList();
						int num=0;
						for (MarketingDto marketingDto : marketingDtos) {
							List<ShopCartItem> shopCartItems = marketingDto.getHitCartItems();
							// 防止为空
							if (shopCartItems != null) {
								for (int j = 0; j < shopCartItems.size(); j++) {
									ShopCartItem cartItem=shopCartItems.get(j);
									// 判断是否支付门店
									boolean supportStore = orderService.getStoreProdCount(cartItem.getProdId(), cartItem.getSkuId());
									if(supportStore) {
										num++;
									}
									cartItem.setSupportStore(supportStore);

									Store store = storeService.getStore(cartItem.getStoreId());
									// 不支持门店或者门店不存在
									if (AppUtils.isBlank(store) && AppUtils.isNotBlank(cartItem.getStoreId()) && cartItem.getStoreId()!=0){

										cartItem.setSupportStore(false);
										num--;
									// 	支持门店，但门店未上线
									}else if(AppUtils.isNotBlank(store) && !store.getIsDisable()){
										cartItem.setSupportStore(false);
										num--;
									}
								}
							}
						}
						shopCarts.setSupportStore(num>0);
					}
				}
				
				request.setAttribute("userShopCartList",userShopCartList);
			}
		}
	}
	
	
	/**
	 * 处理购物车项添加加入购物车时选中的门店ID
	 * @param baskets
	 * @param cartItems
	 */
	private void disposeStore(List<Basket> baskets, List<ShopCartItem> cartItems) {
		
		if (AppUtils.isNotBlank(baskets) && AppUtils.isNotBlank(cartItems)) {
			
			for (ShopCartItem shopCartItem : cartItems) {
				
				for (Basket basket : baskets) {
					
					if (basket.getProdId().equals(shopCartItem.getProdId()) && basket.getSkuId().equals(shopCartItem.getSkuId())) {
						
						shopCartItem.setStoreId(basket.getStoreId());
					}
				}
			}
		}
	}


	/**
	 * 改变  购物车项 的选中状态
	 */
	@RequestMapping(value ="/changeBasketSts" , method = RequestMethod.POST)
	public @ResponseBody String changeBasketSts(HttpServletRequest request, HttpServletResponse response,Long basketId, Long prodId, Long skuId, int checkSts) {
		SecurityUserDetail user = UserManager.getUser(request);
		if(user == null){
			shopCartHandler.updateBasketChkSts(request, response, prodId, skuId,checkSts);
		}else {
			basketService.updateBasketChkSts(basketId, user.getUserId(), checkSts);
			//清除购物车缓存
			subService.evictUserShopCartCache(CartTypeEnum.NORMAL.toCode(), user.getUserId());
		}
		return Constants.SUCCESS;
	}
	
	/**
	 * 批量改变  购物车项 的选中状态
	 * @param request
	 * @param response
	 * @param changeStr 拼接字符串  basketId1:prodId1:skuId1:checkSts1;basketId2:prodId2:skuId2:checkSts2
	 * @return
	 */
	@RequestMapping(value ="/batchChgBasketSts" , method = RequestMethod.POST)
	public @ResponseBody String batchChgBasketSts(HttpServletRequest request, HttpServletResponse response,String changeStr) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		//用户是否登录
		boolean isUserLogined = false;
		if(user != null) {
			 isUserLogined = LoginUserTypeEnum.USER.value().equals(user.getLoginUserType());
		}
		
		List<Basket> basketList = new ArrayList<Basket>();
		List<BasketCookieDto> dto=new ArrayList<BasketCookieDto>();
		//拆分 拼接的字符串
		if(AppUtils.isNotBlank(changeStr)){
			String[] itemStrs = changeStr.split(";");
			for (String itemStr : itemStrs) {
				if(AppUtils.isNotBlank(itemStr)){
					String[] attrStrs = itemStr.split(":");
					if(attrStrs.length==4){
						String basketIdStr = attrStrs[0];
						String prodIdStr = attrStrs[1];
						String skuIdStr = attrStrs[2];
						String checkStsStr = attrStrs[3];
						BasketCookieDto basketCook=new BasketCookieDto();
						basketCook.setBasketId(Long.valueOf(basketIdStr));
						basketCook.setProdId(Long.valueOf(prodIdStr));
						basketCook.setSkuId(Long.valueOf(skuIdStr));
						basketCook.setCheckSts(Integer.valueOf(checkStsStr));
						dto.add(basketCook);
					}
				}
			}
			if(isUserLogined){
				for (BasketCookieDto basketCookieDto : dto) {
					if(AppUtils.isNotBlank(basketCookieDto.getBasketId()) && AppUtils.isNotBlank(basketCookieDto.getCheckSts())){
						try {
							Basket basket = new Basket();
							basket.setBasketId(basketCookieDto.getBasketId());
							basket.setCheckSts(basketCookieDto.getCheckSts());
							basketList.add(basket);
						} catch (Exception e) {
							log.error("batchChgBasketSts() number format exception basketIdStr="+basketCookieDto.getBasketId());
						}
					}
				}
			}else{
				shopCartHandler.updateBasketChkSts(request, response,dto);

			}
			
			
			
			//设置list待定
		}
		
		if(isUserLogined){
			//清除购物车缓存
			basketService.batchUpdateBasketChkSts(user.getUserId(), basketList);
			subService.evictUserShopCartCache(CartTypeEnum.NORMAL.toCode(),user.getUserId());
		}
		return Constants.SUCCESS;
	}
	
	
	/**
	 * 加入购物车
	 * @param prodId 商品ID
	 * @param skuId  单品ID
	 * @param count  数量
	 * @param shopId 店铺ID
	 * @param distUserName 分销用户 ，暂时没有用，有待去掉
	 * @param storeId 门店ID （普通加入购物车为null）
	 * @return
	 */
	@RequestMapping("/addShopBuy")
	public @ResponseBody String addShopBuy(HttpServletRequest request, 
			HttpServletResponse response,
			@RequestParam Long prodId,
			@RequestParam Long skuId,
			@RequestParam int count,
			@RequestParam Long shopId,
			@RequestParam String distUserName,
			@RequestParam Long storeId) {
		SecurityUserDetail user = UserManager.getUser(request);
		
		String userId = null;//checkCartParams要用userId字段,所以要先定义, 用户可能尚未登录
		if(user != null) {
			userId = user.getUserId();
		}
		
		Product product = productService.getProductById(prodId);
		String result = null;
		if (AppUtils.isBlank(storeId)) {
			
			result = orderUtil.checkCartParams(product,skuId,count,userId);
		}else{
			
			result = orderUtil.checkStoreCartParams(product, skuId, count, userId, storeId);
		}
		
		if(!Constants.SUCCESS.equals(result)){
			return result;
		}
		if (user == null) {
			//用户还没有登录， 放到COOKIE中
			Sku sku = skuService.getSku(skuId);
			shopCartHandler.addShopCart(request, response, prodId, skuId, count,product.getShopId(), distUserName,sku.getStocks(),storeId);
		}else{
			//加入购物车，如果购物车已有该商品则增加数量
			Long number = basketService.saveToCart(user.getUserId(), prodId, count, skuId,product.getShopId(),distUserName,storeId);
			if(number<0){
				return "购物车商品过多，请先处理！";
			}
		}
		return Constants.SUCCESS;
	}
	
	

	/**
	 * 删除单个商品
	 * @param request
	 * @param response
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	@RequestMapping(value ="/deletShopCart/{prodId}/{skuId}" , method = RequestMethod.POST)
	public @ResponseBody String deletShopCart(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Long prodId,
			@PathVariable Long skuId) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		//用户是否登录
		boolean isUserLogined = false;
		if(user != null) {
			 isUserLogined = LoginUserTypeEnum.USER.value().equals(user.getLoginUserType());
		}
		
		if(!isUserLogined){
			shopCartHandler.deleteShopCartByProdId(request, response, prodId, skuId);
		}else {
			basketService.deleteBasketById(prodId,skuId, user.getUserId());
			//清除购物车缓存
			subService.evictUserShopCartCache(CartTypeEnum.NORMAL.toCode(), user.getUserId());
		}
		return Constants.SUCCESS;
	}
	
	/**
	 * 清空购物车
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value ="/clearShopCart" , method = RequestMethod.POST)
	public @ResponseBody String clearShopCart(HttpServletRequest request, HttpServletResponse response) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		if(AppUtils.isBlank(user)){
			shopCartHandler.clearShopCarts(request, response);
		}else {
			try {
				basketService.deleteBasketByUserId(user.getUserId());
				//清除购物车缓存
				subService.evictUserShopCartCache(CartTypeEnum.NORMAL.toCode(),user.getUserId());
			} catch (Exception e) {
				log.error("清空购物车数据"+e);
				return Constants.FAIL;
			}
		}
		return Constants.SUCCESS;
	}
	
	
	/**
	 * 更改购物车商品数量
	 * @param request
	 * @param response
	 * @param productId
	 * @param skuId
	 * @param basketCount
	 * @return
	 */
	@RequestMapping(value ="/updateBasketCount" , method = RequestMethod.POST)
	public @ResponseBody Map<String,String> updateBasketCount(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Long productId, 
			@RequestParam Long skuId, 
			@RequestParam Integer basketCount
			) {
		Map<String,String> buyResult=new HashMap<String,String>();
		buyResult.put("buySts", Constants.SUCCESS);
		buyResult.put("buyMsg", null);
		
		if(basketCount<=0){
			buyResult.put("buySts", Constants.FAIL);
			buyResult.put("buyMsg", "请输入正确的购买数量");
			return buyResult;
		}
		
		Product item=productService.getProductById(productId);
		if(item==null || !ProductStatusEnum.PROD_ONLINE.value().equals(item.getStatus())){
			buyResult.put("buySts", Constants.FAIL);
			buyResult.put("buyMsg", "数据已发生改变，请刷新后再操作!");
			return buyResult;
		}
		
		Sku sku=skuService.getSku(skuId);
		if(sku==null || sku.getStatus().intValue()!=1){
			buyResult.put("buySts", Constants.FAIL);
			buyResult.put("buyMsg", "数据已发生改变，请刷新后再操作!");
			return buyResult;
		}
		
		if(sku.getStocks()<basketCount){
			buyResult.put("buySts", Constants.FAIL);
			buyResult.put("buyMsg", "库存不足!");
			return buyResult;
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		if (user == null) {//用户没有登录
			shopCartHandler.changeShopCartNum(request, response, productId,skuId, basketCount);
		}else{
			basketService.updateBasket(productId,skuId,basketCount, user.getUserId());
		}
		
		return buyResult;
	}
	
	
	/**
	 * 切换门店页面
	 * @return
	 */
	@RequestMapping(value ="/shopStoreServer" , method = RequestMethod.GET)
	public String shopStoreServer(HttpServletRequest request, HttpServletResponse response,Long prodId,Long skuId,Integer basketCount) {
		
		request.setAttribute("prodId", prodId);
		request.setAttribute("skuId", skuId);
		request.setAttribute("basketCount", basketCount);
		return PathResolver.getPath(FrontPage.SHOP_STORE_SELECT); 
	}
	
	
	/**
	 * 更改门店

	 * @param productId
	 * @param skuId
	 * @param storeId
	 * @return
	 */
	@RequestMapping(value ="/updateShopStore" , method = RequestMethod.POST)
	public @ResponseBody Map<String,String> updateShopStore(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Long productId, 
			@RequestParam Long skuId, 
			@RequestParam Long storeId,
			@RequestParam Integer basketCount
			) {
		Map<String,String> buyResult=new HashMap<String,String>();
		buyResult.put("buySts", Constants.SUCCESS);
		buyResult.put("buyMsg", null);
		
		
		
		Product item=productService.getProductById(productId);
		if(item==null || !ProductStatusEnum.PROD_ONLINE.value().equals(item.getStatus())){
			buyResult.put("buySts", Constants.FAIL);
			buyResult.put("buyMsg", "数据已发生改变，请刷新后再操作!");
			return buyResult;
		}
		
		Sku sku=skuService.getSku(skuId);
		if(sku==null || sku.getStatus().intValue()!=1){
			buyResult.put("buySts", Constants.FAIL);
			buyResult.put("buyMsg", "数据已发生改变，请刷新后再操作!");
			return buyResult;
		}
		
		StoreSku storeSku = storeSkuService.getStoreSkuBySkuId(storeId, skuId);
		if (null == storeSku) {
			buyResult.put("buySts", Constants.FAIL);
			buyResult.put("buyMsg", "数据已发生改变，请刷新后再操作!");
			return buyResult;
		}
		
		if(storeSku.getStock()<basketCount){
			buyResult.put("buySts", Constants.FAIL);
			buyResult.put("buyMsg", "该门店库存不足，请重新选择!");
			return buyResult;
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		if (user == null) {//用户没有登录
			shopCartHandler.changeShopStore(request, response, productId,skuId,storeId);
		}else{
			basketService.updateShopStore(productId,skuId,storeId, user.getUserId());
		}
		
		return buyResult;
	}
	

}
