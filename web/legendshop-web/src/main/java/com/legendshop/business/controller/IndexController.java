/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import com.legendshop.base.util.RedisImCacheUtil;
import com.legendshop.business.page.FrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.HistoryMessageTypeEnum;
import com.legendshop.model.dto.im.ChatBody;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.MessageService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.websocket.service.DialogueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Set;

/**
 * 首页Controller
 */
@Controller
public class IndexController extends BaseController {
	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(IndexController.class);

	@Autowired
	private MessageService messageService;
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private RedisImCacheUtil imCacheUtil;

	@Autowired
	private DialogueService dialogueService;
	
	/**
	 * 前台首页.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the string
	 */
	@RequestMapping(value = "/index", method =RequestMethod.GET)
	public String index(HttpServletRequest request, HttpServletResponse response) {
		log.debug("visit index page");
		return PathResolver.getPath(FrontPage.INDEX);
	}
	 
	
	/**
	 * 计算用户的未读消息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value ="/calUnreadMsgCount" , method = RequestMethod.GET)
	public @ResponseBody int calUnreadMsgCount(HttpServletRequest request, HttpServletResponse response) {
		SecurityUserDetail user = UserManager.getUser(request);
		if(user == null){//用户尚未登录
			return 0;
		}else{
			String userId = user.getUserId();
			UserDetail userDetail=userDetailService.getUserDetailById(userId);
			if(userDetail==null){
				return 0;
			}
			/**可读信息数**/
			Integer messageCount = messageService.calUnreadMsgCount(userDetail.getUserName(), userDetail.getGradeId(), userDetail.getUserRegtime()); 
			
			//获取客服未读消息数
			int unReadCustomerMessage = 0;

			//排除时间msgType=7 的统计
			Map<String, Object> messagesCount = dialogueService.countCustomOfflineMessage(userId);
			for (Object count:messagesCount.values()){
				unReadCustomerMessage += (Long)count;
			}
			return messageCount+unReadCustomerMessage;
		}
	}
}
