/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.business.page.FrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.TagTypeEnum;
import com.legendshop.model.dto.NewsDto;
import com.legendshop.model.dto.NewsTagDto;
import com.legendshop.model.entity.News;
import com.legendshop.spi.service.NewsService;
import com.legendshop.spi.service.TagService;
import com.legendshop.util.AppUtils;

/**
 * 文章中心的控制器.
 */
@Controller
public class HelpCenterController extends BaseController {
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(HelpCenterController.class);
	
	/** The tag service. */
	@Autowired
	private TagService tagService;

	/** The news service. */
	@Autowired
	private NewsService newsService;
	
	/**
	 * Help center.
	 *
	 * @param request the request
	 * @param response the response
	 * @return the string
	 */
	@RequestMapping("/helpcenter")
	public String helpCenter(HttpServletRequest request, HttpServletResponse response) {
		//获得所有的tag标签的所关联的文章
		List<NewsTagDto> list= tagService.getNewsTagDto(TagTypeEnum.HELP_CENTER.value());
		Map<String, List<NewsDto>> map=new LinkedHashMap<String, List<NewsDto>>();
		for(int i=0;i<list.size();i++){
			String catName=list.get(i).getItemName();
			if(!map.containsKey(catName)){ 
				List<NewsDto> dtos=new ArrayList<NewsDto>();
				dtos.add(list.get(i).getNewsDto());
				map.put(catName, dtos);
			}else{
				List<NewsDto> dto= map.get(catName);
				dto.add(list.get(i).getNewsDto());
			}
		}
		request.setAttribute("tagMap", map);
		//结束
		
		//查询所有分类文章列表
		Map<KeyValueEntity, List<News>> newsCatList = newsService.getNewsByCategory();
		request.setAttribute("newsCatList", newsCatList);
		return PathResolver.getPath(FrontPage.HELP_CENTER);
	}

	@GetMapping("/paidInformation")
	public String paidInformation(HttpServletRequest request) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (null != user) {
			request.setAttribute("userName", user.getUsername());
		} else {
			request.setAttribute("userName", "");
		}
		Map<KeyValueEntity, List<News>> newsCatList = newsService.findByCategoryId();
		request.setAttribute("newsCatList", newsCatList);
		return PathResolver.getPath(FrontPage.PAID_INFORMATION);
	}
	
	/**
	 * Help centersearch.
	 *
	 * @param request the request
	 * @param response the response
	 * @param keyword the keyword
	 * @param curPageNO the cur page no
	 * @return the string
	 */
	@RequestMapping(value="/helpCenter/search") 
	public  String helpCentersearch(HttpServletRequest request, HttpServletResponse response, String word,String curPageNO) {
		if(AppUtils.isBlank(curPageNO)){
			curPageNO="1";
		}
		PageSupport<News> ps =newsService.getSerachNews(word,curPageNO);
		List<News> newList = (List<News>) ps.getResultList();
		//去除 文章内容中的html标签
		for (News news : newList) {
			String content = news.getNewsContent();
			content = content.replaceAll("\\<.*?>", ""); 
			news.setNewsContent(content);
		}
		PageSupportHelper.savePage(request, ps);
		Map<KeyValueEntity, List<News>> newsCatList = newsService.getNewsByCategory();
		request.setAttribute("newsCatList", newsCatList);
		return PathResolver.getPath(FrontPage.HELP_CENTER_NEWS);
	}
}
