/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.util.RedisImCacheUtil;
import com.legendshop.business.page.UserCenterFrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.DialogueSourceEnum;
import com.legendshop.model.constant.MsgStatusEnum;
import com.legendshop.model.dto.DialogueDto;
import com.legendshop.model.dto.im.ImSkuDetailDto;
import com.legendshop.model.dto.im.ImSubRefundReturn;
import com.legendshop.model.entity.MsgText;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.im.ChatRecord;
import com.legendshop.model.entity.im.Dialogue;
import com.legendshop.model.siteInformation.SiteInformation;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.MessageService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.SafeHtml;
import com.legendshop.websocket.service.CommonServiceUtil;
import com.legendshop.websocket.service.DialogueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 消息控制器
 * 
 */
@Controller
public class SiteMessageController extends BaseController {
	/** The logger. */
	private final Logger logger = LoggerFactory.getLogger(SiteMessageController.class);
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private ShopDetailService shopDetailService;
	
	@Autowired
	private  DialogueService dialogueService;

	@Autowired
	private RedisImCacheUtil imCacheUtil;

	
	/**
	 * 去往收件箱
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/p/inbox")
	public String toInbox(HttpServletRequest request, HttpServletResponse response,String curPageNO) {
		try {
			SecurityUserDetail user = UserManager.getUser(request);
			String userName = user.getUsername();
			inboxSiteInformation(userName,curPageNO, request);
			
			Integer gradeId = messageService.getGradeId(userName);
			getUnReadMsgNum(userName, gradeId, request);
			} catch (Exception e) {
				logger.error("invoking toUserCenterPage", e);
			}
		return PathResolver.getPath(UserCenterFrontPage.SITE_MESSAGE_INBOX);
	}
	
	/**
	 * 去往发件箱
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/p/outbox")
	public String toOutbox(HttpServletRequest request, HttpServletResponse response,String curPageNO) {
		try {
			SecurityUserDetail user = UserManager.getUser(request);
			String userName = user.getUsername();
			outboxSiteInformation(userName,curPageNO,request);
		} catch (Exception e) {
			logger.error("invoking toUserCenterPage", e);
		}
		return PathResolver.getPath(UserCenterFrontPage.SITE_MESSAGE_OUTBOX);
	}
	
	/**
	 * 去往系统邮件箱
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/p/systemMessages")
	public String systemMessages(HttpServletRequest request, HttpServletResponse response,String curPageNO) {
		try {
			SecurityUserDetail user = UserManager.getUser(request);
			String userName = user.getUsername();
			Integer gradeId = messageService.getGradeId(userName);
			systemInformation(userName,gradeId,curPageNO,request);
			
			getUnReadMsgNum(userName, gradeId, request);
		} catch (Exception e) {
			logger.error("invoking toUserCenterPage", e);
		}
		return PathResolver.getPath(UserCenterFrontPage.SYSTEM_MESSAGES);
	}
	
	/**
	 * 发站内信
	 *
	 */
	@RequestMapping("/p/sendInformation")
	public @ResponseBody String sendMessage(HttpServletRequest request, HttpServletResponse response,String receiveNames,String title,String text) {
		try {
			SecurityUserDetail user = UserManager.getUser(request);
			String userName = user.getUsername();
			SafeHtml safeHtml = new SafeHtml();
			MsgText msgText = new MsgText();
			msgText.setText(safeHtml.makeSafe(text));
			msgText.setTitle(safeHtml.makeSafe(title));
			msgText.setRecDate(new Date());
			
			//把昵称 转换成 userName
			String receiveUserNames = null;
			String[] nameList = receiveNames.split(",");
			if (nameList.length == 1) {
				receiveUserNames = userDetailService.getUserNameByNickName(receiveNames);
			}else{
				for (int i = 0; i < nameList.length; i++) {
					String recUserName = userDetailService.getUserNameByNickName(nameList[i]);
					if(i>0){
						receiveUserNames+=",";
					}
					receiveUserNames+=recUserName;
				}
			}
			
			messageService.saveSiteInformation(msgText,userName,receiveUserNames);
			
		} catch (Exception e){
			logger.error("Exception", e);
			return "消息发送失败";
		}
		return "消息发送成功";
	}
	
	
	
	/**
	 * 去往发消息页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/p/sendMessage")
	public String sendMessage(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(UserCenterFrontPage.SEND_MESSAGE);
	}
	
	
	/**
	 * 收件箱，通过msgId查找出邮件的内容
	 * @param request
	 * @param response
	 * @param msgId
	 * @return
	 */
	@RequestMapping("/p/loadinbox/{msgId}")
	public String loadInBox(HttpServletRequest request, HttpServletResponse response,@PathVariable Long msgId) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		String nickName = UserManager.getNickName(user);
		SiteInformation siteInformation = messageService.getMsgText(userName, msgId, true);
		siteInformation.setMsgId(msgId);
		if(AppUtils.isNotBlank(nickName)){
			siteInformation.setReceiveName(nickName);
		}
		String sendNickName = userDetailService.getNickNameByUserName(siteInformation.getSendName());
		if(AppUtils.isNotBlank(sendNickName)){
			siteInformation.setSendName(sendNickName);
		}
		request.setAttribute("siteInformation",siteInformation);
		return PathResolver.getPath(UserCenterFrontPage.INBOX_MAIL_INFO);
	}
	
	/**
	 * 发件箱，通过msgId查找出邮件的内容
	 * @param request
	 * @param response
	 * @param msgId
	 * @return
	 */
	@RequestMapping("/p/loadoutbox/{msgId}")
	public String loadOutBox(HttpServletRequest request, HttpServletResponse response,@PathVariable Long msgId) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		String nickName = UserManager.getNickName(user);
		SiteInformation siteInformation = messageService.getMsgText(userName, msgId,false);
		siteInformation.setMsgId(msgId);
		if(AppUtils.isNotBlank(nickName)){
			siteInformation.setSendName(nickName);
		}
		String recvNickName = userDetailService.getNickNameByUserName(siteInformation.getReceiveName());
		if(AppUtils.isNotBlank(recvNickName)){
			siteInformation.setReceiveName(recvNickName);
		}
		request.setAttribute("siteInformation",siteInformation);
		return PathResolver.getPath(UserCenterFrontPage.INBOX_MAIL_INFO);
	}
	
	/**
	 * 系统通知，通过msgId查找出邮件的内容
	 * @param request
	 * @param response
	 * @param msgId
	 * @return
	 */
	@RequestMapping("/p/loadSystemMail/{msgId}")
	public String loadSystemMailInfo(HttpServletRequest request, HttpServletResponse response,@PathVariable Long msgId) {
		//将ls_msg_status中的状态更新为1
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		String nickName = UserManager.getNickName(user);
		messageService.updateSystemMsgStatus(userName,msgId,MsgStatusEnum.READED.value());

		SiteInformation siteInformation = messageService.getMsgText(userName,msgId, false);
		if(AppUtils.isNotBlank(nickName)){
			siteInformation.setReceiveName(nickName);
		}else{
			siteInformation.setReceiveName(userName);
		}
		String sendNickName = userDetailService.getNickNameByUserName(siteInformation.getSendName());
		if(AppUtils.isNotBlank(sendNickName)){
			siteInformation.setSendName(sendNickName);
		}
		siteInformation.setMsgId(msgId);
		request.setAttribute("siteInformation",siteInformation);
		return PathResolver.getPath(UserCenterFrontPage.INBOX_MAIL_INFO);
	}
	
	/**
	 * 检查昵称是否为空
	 */
	@RequestMapping("/p/isReceiverExist")
	public @ResponseBody boolean isReceiverExist(HttpServletRequest request, HttpServletResponse response,String receiveName) {
		return messageService.isReceiverExist(receiveName);
	}
	
	
	/** 
	 * 删除 发件箱的站内信，将删除状态  +1 ，可删除多个
	 * @param request
	 * @param response
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "/p/deleteOutboxMessage")
    public @ResponseBody String  deleteOutboxMessage(HttpServletRequest request, HttpServletResponse response, String ids){
		List<Long> idList = JSONUtil.getArray(ids, Long.class);
		if(AppUtils.isNotBlank(idList)){
    		for (Long msgId : idList) {
    			messageService.deleteOutbox(msgId);
			}
    	}
	return Constants.SUCCESS;
    }
	
	/**
	 * 删除 收件箱的站内信，将删除状态2 +1，可删除多个
	 * @param request
	 * @param response
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "/p/deleteInboxMessage")
    public @ResponseBody String  deleteInboxMessage(HttpServletRequest request, HttpServletResponse response, String ids){
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		List<Long> idList = JSONUtil.getArray(ids, Long.class);
		if(AppUtils.isNotBlank(idList)){
    		for (Long msgId : idList) {
    			messageService.deleteInbox(msgId,userName);
			}
    	}
	return Constants.SUCCESS;
    }
	
	/**
	 * 清空 收件箱的站内信，将删除状态2 +1，清空所有
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/p/clearInboxMessage")
    public @ResponseBody String  clearInboxMessage(HttpServletRequest request, HttpServletResponse respons){
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		this.messageService.clearInboxMessage(userName);
		return Constants.SUCCESS;
    }
	
	/**
	 * 删除 系统通知 ，将ls_msg_status中的状态更新为-1,可删除多个
	 * @param request
	 * @param response
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "/p/deleteMessages")
    public @ResponseBody String  deleteMessages(HttpServletRequest request, HttpServletResponse response, String ids){
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		List<Long> idList = JSONUtil.getArray(ids, Long.class);
		if(AppUtils.isNotBlank(idList)){
    		for (Long msgId : idList) {
    			messageService.updateSystemMsgStatus(userName, msgId, MsgStatusEnum.DELETED.value());
			}
    	}
	return Constants.SUCCESS;
    }
	
	/**
	 * 信息内容 inBoxMailInfo中，删除系统通知
	 * @param request
	 * @param response
	 * @param msgId
	 * @return
	 */
	@RequestMapping(value = "/p/deleteSystemMsg")
    public @ResponseBody String  deleteSystemMsg(HttpServletRequest request, HttpServletResponse response, Long msgId){
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		messageService.updateSystemMsgStatus(userName, msgId, MsgStatusEnum.DELETED.value());
	return Constants.SUCCESS;
    }
	
	/** 
	 * 信息内容 inBoxMailInfo中，发件人的删除
	 */
	@RequestMapping(value = "/p/deleteOutBox")
    public @ResponseBody String  deleteOutBox(HttpServletRequest request, HttpServletResponse response, Long msgId){
		messageService.deleteOutbox(msgId);
	return Constants.SUCCESS;
    }
	
	/** 
	 * 信息内容 inBoxMailInfo中，收件人的删除
	 */
	@RequestMapping(value = "/p/deleteInBox")
    public @ResponseBody String  deleteInBox(HttpServletRequest request, HttpServletResponse response, Long msgId){
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		messageService.deleteInbox(msgId,userName);
	return Constants.SUCCESS;
    }
	

	private Long findById(Map<String, Object> messages, Long id) {
		Object tmp = messages.get(id.toString());
		if (tmp == null) {
			return 0L;
		}
		return (Long) tmp;
	}

	private ChatRecord findRecord(List<ChatRecord> lastMessage, Long shopId) {
		for (ChatRecord record : lastMessage) {
			if (record.getShopId().equals(shopId)) {
				return record;
			}
		}
		return null;
	}
	
	/**
	 * 查找未读消息数
	 * @param userName
	 * @param gradeId
	 * @param request
	 */
	private void getUnReadMsgNum(String userName,Integer gradeId, HttpServletRequest request){
		//获取收件箱，未读消息数
		Integer unReadInboxMsgNum = messageService.getUnReadMsgNum(userName);
		if(AppUtils.isBlank(unReadInboxMsgNum)) {
			unReadInboxMsgNum = 0;
		}
		request.setAttribute("unReadInboxMsgNum", unReadInboxMsgNum);
		
		//获取系统未读消息数
		UserDetail userDetail = userDetailService.getUserDetail(userName);
		Integer unReadMsgNum = messageService.getCalUnreadSystemMessagesCount(userName, gradeId, userDetail.getUserRegtime());
		request.setAttribute("unReadSystemMsgNum", unReadMsgNum);
		
		//获取客服未读消息数
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		long unReadCustomerMessage = 0;
		/**
		 * 离线的消息数量
		 */
		//排除时间msgType=7 的统计
		Map<String, Object> messagesCount = dialogueService.countCustomOfflineMessage(userId);
		for (Object count:messagesCount.values()){
			unReadCustomerMessage += (Long)count;
		}
		request.setAttribute("unReadCustomerMessage", unReadCustomerMessage);
	}	

	
	
	private void inboxSiteInformation(String userName, String curPageNO, HttpServletRequest request){
		 PageSupport<SiteInformation> ps = messageService.query(curPageNO,userName);
		 PageSupportHelper.savePage(request, ps);
	}
	
	private void outboxSiteInformation(String userName, String curPageNO, HttpServletRequest request){
		 PageSupport<SiteInformation> ps = messageService.queryMessage(curPageNO,userName);
		 PageSupportHelper.savePage(request,ps);
	}
	
	private void systemInformation(String userName,Integer gradeId,String curPageNO,HttpServletRequest request){
		 UserDetail userDetail = userDetailService.getUserDetail(userName);
		 PageSupport<SiteInformation> ps = messageService.query(curPageNO,gradeId,userName,userDetail);
		 PageSupportHelper.savePage(request,ps);
	}
}
