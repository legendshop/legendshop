/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.util.Date;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.core.base.BaseController;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.UserFeelbackStatusEnum;
import com.legendshop.model.entity.UserFeedBack;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.UserFeedBackService;
import com.legendshop.util.AppUtils;
import com.legendshop.web.helper.IPHelper;

/**
 * 用户反馈控制器
 *
 */
@Controller
@RequestMapping("/userFeedBack")
public class UserFeedBackController extends BaseController {
    @Autowired
    private UserFeedBackService userFeedBackService;

    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, UserFeedBack userFeedBack) {
        return null;
    }

    @RequestMapping(value = "/save",method=RequestMethod.GET)
    @ResponseBody
    public  String save(HttpServletRequest request, HttpServletResponse response, String content,String mobile) {
    	UserFeedBack userFeedBack=new UserFeedBack();
    	if(content.isEmpty()){
    		throw new BusinessException("content is null");
    	}
		SecurityUserDetail user = UserManager.getUser(request);
    	if(AppUtils.isNotBlank(user)){
    		userFeedBack.setUserId(user.getUserId());
    		userFeedBack.setName(user.getUsername());
    	}

    	userFeedBack.setRecDate(new Date());
    	userFeedBack.setContent(content);
    	userFeedBack.setStatus(UserFeelbackStatusEnum.UNREAD.value());
    	userFeedBack.setIp(IPHelper.getIpAddr(request));
    	userFeedBack.setFeedBackSource(UserFeelbackStatusEnum.PC.value());//add 反馈来源 2016.07.11
    	userFeedBack.setContactInformation(mobile);
        userFeedBackService.saveUserFeedBack(userFeedBack);
		return Constants.SUCCESS;
    }

    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        UserFeedBack userFeedBack = userFeedBackService.getUserFeedBack(id);
		userFeedBackService.deleteUserFeedBack(userFeedBack);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
        return "forward:/admin/userFeedBack/query.htm";
    }
    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        UserFeedBack userFeedBack = userFeedBackService.getUserFeedBack(id);
        request.setAttribute("userFeedBack", userFeedBack);
        return "/userFeedBack/userFeedBack";
    }
    
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return "/userFeedBack/userFeedBack";
	}

    @RequestMapping(value = "/update")
    public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {        
        UserFeedBack userFeedBack = userFeedBackService.getUserFeedBack(id);
		request.setAttribute("userFeedBack", userFeedBack);
		return "forward:/admin/userFeedBack/query.htm";
    }

}
