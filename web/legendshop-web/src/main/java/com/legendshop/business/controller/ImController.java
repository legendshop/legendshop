/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import cn.hutool.core.util.StrUtil;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.util.RedisImCacheUtil;
import com.legendshop.business.page.FrontPage;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.helper.VisitHistoryHelper;
import com.legendshop.model.constant.*;
import com.legendshop.model.dto.im.*;
import com.legendshop.model.entity.*;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import com.legendshop.util.MD5Util;
import com.legendshop.websocket.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * 客服的入口.
 */
@Controller
@RequestMapping("/p/im")
public class ImController extends BaseController{
	
	@Autowired
	private ImProductService productService;
	
	@Autowired
	private ImSkuService skuService;
	
	@Autowired
	private ImOrderService orderService;
	
	@Autowired
	private ImShopDetailService shopDetailService;
	
	@Autowired
	private ImProductCommentStatService productCommentStatService;
	
	@Autowired
	private ImUserDetailService userDetailService;
		
	@Autowired
	private AttachmentManager attachmentManager;
	
	@Autowired
	private DialogueService dialogueService;
	
	@Autowired
	private ImSubRefundReturnService subRefundReturnService;
	
	@Autowired
	private ImUserGradeService userGradeService;
	
	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;
	
	@Autowired
	private RedisImCacheUtil redisImCacheUtil;

	@Autowired
	private CustomServerService customServerService;

	@Autowired
	private HttpServletRequest request;

	/**
	 * IM主页.
	 *
	 * @param skuId the sku id
	 * @return the string
	 */
	@RequestMapping("/index/{skuId}")
	public String im(@PathVariable Long skuId) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		ImUserDetail userDetail = userDetailService.getUserDetailById(userId);
		
		UserGrade userGrade = userGradeService.getUserGrade(userDetail.getGradeId());
		
		if (AppUtils.isNotBlank(userGrade)) {
			request.setAttribute("userGrade", userGrade);
		}
		
		//sku详情,用于Im系统查找信息
		ImSkuDetailDto imSkuDetailDto = skuService.getSkuDetailDtoBySkuId(skuId);
		if(AppUtils.isBlank(imSkuDetailDto)){
			return PathResolver.getPath(FrontPage.PROD_NON_EXISTENT);
		}
		
		//密钥
		TreeMap<String, String> params=new TreeMap<String,String>();
		params.put("userId", userId);
		params.put("userName",userDetail.getUserName());
		params.put("shopId", String.valueOf(imSkuDetailDto.getShopId()));
		params.put("consultType",ImConsultTypeEnum.PROD.value());
		params.put("consultId",String.valueOf(imSkuDetailDto.getSkuId()));
		params.put("consultSource", DialogueSourceEnum.PC.value());
		String sign=MD5Util.createSign(params,Constants._MD5);

		//该商家的客服数量
		Integer cusCount=customServerService.getCountByShopId(imSkuDetailDto.getShopId());
		request.setAttribute("consultCount", cusCount);
		request.setAttribute("consultId", imSkuDetailDto.getSkuId());
		request.setAttribute("consultType",ImConsultTypeEnum.PROD.value());
		request.setAttribute("shopId",imSkuDetailDto.getShopId());
		request.setAttribute("userId",userId);
		request.setAttribute("userName",userDetail.getUserName());
		request.setAttribute("sign", sign);
		request.setAttribute("consultSource", DialogueSourceEnum.PC.value());
		
		request.setAttribute("userDetail", userDetail);
		request.setAttribute("skuId", skuId);
		request.setAttribute("IM_BIND_ADDRESS", propertiesUtil.getImBindAddress());
		return PathResolver.getPath(FrontPage.IM);
	}
	
	/**
	 * 联系卖家.
	 *
	 */
	@RequestMapping("/customerShop")
	public String contactShop(Long shopId,String subNum) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		ImUserDetail userDetail = userDetailService.getUserDetailById(userId);
		
		UserGrade userGrade = userGradeService.getUserGrade(userDetail.getGradeId());
		
		if (AppUtils.isNotBlank(userGrade)) {
			request.setAttribute("userGrade", userGrade);
		}

		if(subNum != null){
			request.setAttribute("subNum", subNum);
		}

		if(shopId != null){
			request.setAttribute("shopId", shopId);
		}


		//密钥
		TreeMap<String, String> params=new TreeMap<>();
		params.put("userId", userId);
		params.put("userName",userDetail.getUserName());
		params.put("shopId", String.valueOf(shopId));
		params.put("consultType",ImConsultTypeEnum.ORDER.value());
		
		//保证签名统一，subNum为空就转成空字符串
		if (AppUtils.isBlank(subNum)) {
			subNum = "";
		}
		params.put("consultId",subNum);
		params.put("consultSource", DialogueSourceEnum.PC.value());
		String sign=MD5Util.createSign(params,Constants._MD5);
		
		request.setAttribute("consultId", subNum);  //订单号
		request.setAttribute("consultType",ImConsultTypeEnum.ORDER.value());
		request.setAttribute("shopId",String.valueOf(shopId));
		request.setAttribute("userId",userId);
		request.setAttribute("userName",userDetail.getUserName());
		request.setAttribute("sign", sign);
		request.setAttribute("consultSource", DialogueSourceEnum.PC.value());
		
		request.setAttribute("userDetail", userDetail);
		request.setAttribute("skuId", 0);
		request.setAttribute("IM_BIND_ADDRESS", propertiesUtil.getImBindAddress());
		return PathResolver.getPath(FrontPage.IM);
	}
	
	/**
	 * 联系买家.
	 *
	 * @return the string
	 */
	@RequestMapping("/customer/{userId}")
	public String contactUser(@PathVariable String userId) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		ImUserDetail userDetail = userDetailService.getUserDetailById(userId);
		
		if(userDetail == null){
			throw new BusinessException("Can not find user by: " + userId);
		}
		
		UserGrade userGrade = userGradeService.getUserGrade(userDetail.getGradeId());
		
		if (AppUtils.isNotBlank(userGrade)) {
			request.setAttribute("userGrade", userGrade);
		}
		
		//密钥
		TreeMap<String, String> params=new TreeMap<String,String>();
		params.put("userId", userId);
		params.put("userName",userDetail.getUserName());
		params.put("shopId", String.valueOf(shopId));
		params.put("consultType",ImConsultTypeEnum.ORDER.value());
		params.put("consultId","0");
		params.put("consultSource", DialogueSourceEnum.PC.value());
		String sign=MD5Util.createSign(params,Constants._MD5);
		
		request.setAttribute("consultId", "0");
		request.setAttribute("consultType",ImConsultTypeEnum.ORDER.value());
		request.setAttribute("shopId",String.valueOf(shopId));
		request.setAttribute("userId",userId);
		request.setAttribute("userName",userDetail.getUserName());
		request.setAttribute("sign", sign);
		request.setAttribute("consultSource", DialogueSourceEnum.PC.value());
		
		request.setAttribute("userDetail", userDetail);
		request.setAttribute("skuId", 0);
		request.setAttribute("IM_BIND_ADDRESS", propertiesUtil.getImBindAddress());
		return PathResolver.getPath(FrontPage.IM);
	}
	
	/**
	 * Customer shop.退货退款联系商家
	 *
	 */
	@RequestMapping("/customerShop/{refundSn}/{shopId}")
	public String customerShop(@PathVariable String refundSn,@PathVariable Long shopId) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		ImUserDetail userDetail = userDetailService.getUserDetailById(userId);
		//查看退款退货详情
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturnByRefundSn(refundSn);

		if(subRefundReturn != null && subRefundReturn.getUserId().equals(userId)){//必须是自己的退款单
			if( subRefundReturn.getSkuId() == 0){
				List<ImOrderDto> imOrderList = orderService.getImSubDto(subRefundReturn.getSubNumber());
				request.setAttribute("imOrderList", imOrderList);
				request.setAttribute("subNum", subRefundReturn.getSubNumber());
			}else{
				List<ImOrderDto> imOrderList = orderService.getImSubItemDto(subRefundReturn.getSubItemId());
				request.setAttribute("imOrderList", imOrderList);
				request.setAttribute("subNum", subRefundReturn.getSubItemId());
			}
		}

		UserGrade userGrade = userGradeService.getUserGrade(userDetail.getGradeId());
		if (AppUtils.isNotBlank(userGrade)) {
			request.setAttribute("userGrade", userGrade);
		}
		
		//密钥
		TreeMap<String, String> params=new TreeMap<String,String>();
		params.put("userId", userId);
		params.put("userName",userDetail.getUserName());
		params.put("shopId", String.valueOf(shopId));
		params.put("consultType",ImConsultTypeEnum.REFUND.value());
		params.put("consultId",String.valueOf(refundSn));
		params.put("consultSource", DialogueSourceEnum.PC.value());
		String sign=MD5Util.createSign(params,Constants._MD5);
		
		request.setAttribute("consultId", refundSn);
		request.setAttribute("consultType",ImConsultTypeEnum.REFUND.value());
		request.setAttribute("shopId",String.valueOf(shopId));
		request.setAttribute("userId",userId);
		request.setAttribute("userName",userDetail.getUserName());
		request.setAttribute("sign", sign);
		request.setAttribute("consultSource", DialogueSourceEnum.PC.value());
		
		request.setAttribute("userDetail", userDetail);
		if(AppUtils.isNotBlank(subRefundReturn)){
			request.setAttribute("skuId", subRefundReturn.getSkuId());
		}else{
			request.setAttribute("skuId", 0); //默认为0
		}
		request.setAttribute("IM_BIND_ADDRESS", propertiesUtil.getImBindAddress());
		return PathResolver.getPath(FrontPage.IM);
	}
	
	/**
	 * Customer admin.
	 *
	 * @param refundSn the refund sn 退款单号
	 * @return the string
	 */
	@RequestMapping("/customerAdmin/{refundSn}")
	public String customerAdmin(@PathVariable String refundSn) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		ImUserDetail userDetail = userDetailService.getUserDetailById(userId);
		UserGrade userGrade = userGradeService.getUserGrade(userDetail.getGradeId());
		if(StrUtil.equals("0",refundSn)){
			refundSn = ""; //表示为不查退单
		}

		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturnByRefundSn(refundSn);


		if (AppUtils.isNotBlank(userGrade)) {
			request.setAttribute("userGrade", userGrade);
		}
		
		//密钥
		TreeMap<String, String> params=new TreeMap<String,String>();
		params.put("userId", userId);
		params.put("userName",userDetail.getUserName());
		params.put("shopId", "0");
		params.put("consultType",ImConsultTypeEnum.ORDER.value());
		params.put("consultId",String.valueOf(refundSn));
		params.put("consultSource", DialogueSourceEnum.PC.value());
		String sign=MD5Util.createSign(params,Constants._MD5);
		
		request.setAttribute("consultId", refundSn);
		request.setAttribute("consultType",ImConsultTypeEnum.ORDER.value());
		request.setAttribute("shopId","0");
		request.setAttribute("userId",userId);
		request.setAttribute("userName",userDetail.getUserName());
		request.setAttribute("sign", sign);
		request.setAttribute("consultSource", DialogueSourceEnum.PC.value());
		request.setAttribute("consultCount", 1);
		
		request.setAttribute("userDetail", userDetail);
		if(AppUtils.isNotBlank(subRefundReturn)){
			request.setAttribute("skuId", subRefundReturn.getSkuId());
		}else{
			request.setAttribute("skuId", 0); //默认为0
		}

		request.setAttribute("refundSn_On", refundSn);
		request.setAttribute("IM_BIND_ADDRESS", propertiesUtil.getImBindAddress());
		return PathResolver.getPath(FrontPage.IM);
	}
	
	/**
	 * 获取IM的咨询内容
	 *
	 * @param skuId the sku id
	 * @return the im consult
	 */
	@RequestMapping(value="/getImConsult", method = RequestMethod.POST)
	public String getImConsult(Long skuId,String consultId,String subNum,String consultType) {
		//sku详情,用于Im系统查找信息
		if(consultType.equals(ImConsultTypeEnum.PROD.value())){
			skuId = Long.valueOf(consultId);
			consultId = "0";
		}
		ImSkuDetailDto skuDetailDto = new ImSkuDetailDto();
		if(skuId!=null&& skuId!=0){//咨询商家
			skuDetailDto = skuService.getSkuDetailDtoBySkuId(skuId);
		}
		List<ImOrderDto> imOrderList = null;


		if(consultId != null && !consultId.equals("0") && !consultId.equals("0,") && !consultId.equals(subNum)){
			SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturnByRefundSn(consultId);
			if(subRefundReturn != null && subRefundReturn.getSubItemId() == 0){
				subNum = subRefundReturn.getSubNumber();
				imOrderList = orderService.getImSubDto(subNum);
			}else if(subRefundReturn != null) {
				imOrderList = orderService.getImSubItemDto(subRefundReturn.getSubItemId());
			}
			request.setAttribute("imOrderList", imOrderList);

			if(skuDetailDto.getShopId() == null){
				//设置当前的商城Id
				if(subRefundReturn != null){
					skuDetailDto.setShopId(subRefundReturn.getShopId());
				}
			}
		}else if(subNum != null && !subNum.equals("")){

			imOrderList = orderService.getImSubDto(subNum);
			request.setAttribute("imOrderList", imOrderList);

			if(AppUtils.isNotBlank(imOrderList)){
				if(skuDetailDto.getShopId() == null){
					//设置当前的商城Id
					skuDetailDto.setShopId(imOrderList.get(0).getShopId());
				}
			}

		}

		
		if(skuId==null && AppUtils.isNotBlank(consultId)){//咨询平台
			SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturnByRefundSn(consultId);
			if(AppUtils.isNotBlank(subRefundReturn) && AppUtils.isNotBlank(skuDetailDto)) {
				skuDetailDto.setProdName(subRefundReturn.getProductName());
				skuDetailDto.setProdPic(subRefundReturn.getProductImage());
			}
			request.setAttribute("subRefundReturn", subRefundReturn);

			if(skuDetailDto.getShopId() == null){
				//设置当前的商城Id
				if(subRefundReturn != null){
					skuDetailDto.setShopId(subRefundReturn.getShopId());
				}
			}
		}
		
		//商品浏览历史
		List<Object> prodIds = VisitHistoryHelper.getVisitedProd(request);
		List<ImProduct> products = new ArrayList<ImProduct>();
		for (Object prodId : prodIds) {
			ImProduct productDetail = productService.getProdDetail(Long.parseLong(prodId.toString()));
			if(productDetail!=null && ProductStatusEnum.PROD_ONLINE.value().equals(productDetail.getStatus())){
				if(AppUtils.isNotBlank(skuDetailDto) && productDetail.getShopId().equals(skuDetailDto.getShopId())){//必须要是本商城下的
					products.add(productDetail);
				}
			}
		}
		if(AppUtils.isNotBlank(skuDetailDto)){
			skuDetailDto.setProducts(products);
		}
		request.setAttribute("skuDetailDto", skuDetailDto);
		return PathResolver.getPath(FrontPage.IM_CONSULT);
	}

	/**
	 * 根据shopId获取IM的咨询内容,主要是获取访问历史
	 *
	 */
	@RequestMapping(value="/getImConsultByShopId", method = RequestMethod.POST)
	public String getImConsultByShopId(HttpServletRequest request, Long shopId) {

		ImSkuDetailDto skuDetailDto = new ImSkuDetailDto();

		//商品浏览历史
		List<Object> prodIds = VisitHistoryHelper.getVisitedProd(request);
		List<ImProduct> products = new ArrayList<ImProduct>();
		for (Object prodId : prodIds) {
			ImProduct productDetail = productService.getProdDetail(Long.parseLong(prodId.toString()));
			if(productDetail!=null && ProductStatusEnum.PROD_ONLINE.value().equals(productDetail.getStatus())){
				if(productDetail.getShopId().equals(shopId)){//必须要是本商城下的
					products.add(productDetail);
				}
			}
		}
		if(AppUtils.isNotBlank(skuDetailDto)){
			skuDetailDto.setProducts(products);
		}
		request.setAttribute("skuDetailDto", skuDetailDto);
		return PathResolver.getPath(FrontPage.IM_CONSULT);
	}

	/**
	 * 我的订单.
	 *
	 */
	@RequestMapping(value="/order", method = RequestMethod.POST)
	public String order(Long shopId) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		List<ImOrderDto> imOrderList = orderService.getImOrderDto(userId,shopId);
		request.setAttribute("imOrderList", imOrderList);
		return PathResolver.getPath(FrontPage.IM_ORDER);
	}
	
	
	
	/**
	 * 店铺信息.
	 *
	 */
	@RequestMapping(value="/shopDetai", method = RequestMethod.POST)
	public String shopDetai(Long shopId) {
		ImShopDetail shopDetail = shopDetailService.getShopDetailByShopId(shopId);
		ProductCommentStat commentStat = productCommentStatService.getProductCommentStatByShopId(shopId);
		if(AppUtils.isNotBlank(commentStat) && commentStat.getScore()!=null && commentStat.getComments()!=null){
			Double credit = Arith.div(Double.valueOf(commentStat.getScore()), Double.valueOf(commentStat.getComments()), 2);
			shopDetail.setCredit(credit.intValue());
		}
		request.setAttribute("shopDetail", shopDetail);
		return PathResolver.getPath(FrontPage.IM_SHOP_DETAIL);
	}
	
	
	/**
	 * 常见问题.
	 *
	 */
	@RequestMapping(value="/problem", method = RequestMethod.POST)
	public String problem(Long shopId) {
		return PathResolver.getPath(FrontPage.IM_PROBLEM);
	}
	
	
	/**
	 * 获取指定用户的聊天记录. 改为数据库的方式
	 * @deprecated
	 * @param fromUserId  // 发送用户id;
	 * @param userId // 接收用户id;
	 * @param beginTime // 消息开始时间;
	 * @param endTime // 消息结束时间
	 * @param offset // 分页偏移量
	 * @param count // 数量
	 * @return the set< object>
	 *
	 */
	@RequestMapping(value = "/user/message_old", method = RequestMethod.POST)
	public @ResponseBody Set<Object> message_old(
			String fromUserId,
			String userId, // 接收用户id;
			Double beginTime, //消息的开始时间
			Double endTime, 
		    Integer offset, Integer count) {
		
		//转化客服的离线消息
		redisImCacheUtil.converOfflineMessage(fromUserId, userId, HistoryMessageTypeEnum.CUSTOMER.value());
		
		//获取在线消息
		Set<Object> messages= redisImCacheUtil.getOnlineMessage(fromUserId, userId, beginTime, endTime, offset, count);
		
		//读取自己发的离线消息
		if(offset == 0) {
			Set<Object> off_messages = redisImCacheUtil.getOfflineMessageForSelf(fromUserId, userId, HistoryMessageTypeEnum.USER.value());
			off_messages.addAll(messages);
			return off_messages;
		}else {
			return messages;
		}				
	}

	/**
	 * 获取指定用户的聊天记录. 改为数据库的方式  data: {"fromUserId":userId,"userId":customId,"offset":offset,"count":count},
	 * @param fromUserId  // 发送用户id;
	 * @param userId // 接收用户id; customId
	 * @param beginTime // 消息开始时间;
	 * @param endTime // 消息结束时间
	 * @param offset // 分页偏移量
	 * @param count // 数量
	 * @return the set< object>
	 *
	 */
	@RequestMapping(value = "/user/message", method = RequestMethod.POST)
	public @ResponseBody List<ChatBody> message(
			String fromUserId,
			String userId, // 接收用户id;
			Long shopId,
			Double beginTime, //消息的开始时间
			Double endTime,
			Integer offset, Integer count) {

		//用户获取客服的消息
		return dialogueService.getImMessage( shopId, fromUserId, userId,offset, HistoryMessageTypeEnum.CUSTOMER.value());

	}

	/**
	 * 上传聊天图片.
	 *
	 * @param sendImgFile the send img file
	 * @return the map< string, string>
	 * @throws IOException the IO exception
	 */
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody Map<String,String> upload(MultipartFile sendImgFile) throws IOException {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		
		HashMap<String, String> map = new HashMap<String, String>();
		if(AppUtils.isBlank(sendImgFile)){
			map.put("status", Constants.FAIL);
			map.put("message", "请选择上传图片");
			return map;
		}
		// 处理图片上传
		UploadResultDto uploadResultDto= attachmentManager.uploadImage(ImagePurposeEnum.USER_FILE,userId,userName,0l,sendImgFile);
		if (AppUtils.isBlank(uploadResultDto)) {
			map.put("status", Constants.FAIL);
			map.put("message", "上传失败，请稍后重试");
			return map;
		}
		if (!uploadResultDto.isSuccess()){
			map.put("status", Constants.FAIL);
			map.put("message", uploadResultDto.getMessage());
			return map;
		}
		map.put("status", Constants.SUCCESS);
		map.put("path", uploadResultDto.getFirstPath());
		return map;
	}
	
	
	/**
	 * Sets the consult text empty.
	 *
	 * @param shopId the shop id
	 * @param customId the custom id
	 */
	@ResponseBody
	@RequestMapping(value = "/setConsultTextEmpty", method = RequestMethod.POST)
	public void setConsultTextEmpty(Long shopId,Long customId) {
		SecurityUserDetail user = UserManager.getUser(request);
		dialogueService.setDialogueContextEmpty(user.getUserId(),customId,shopId);
	}
	
	
	/**
	 * 客服消息列表过来的
	 */
	@RequestMapping("/message/index/{shopId}")
	public String mesIm(@PathVariable Long shopId,
			Long skuId, String refundSn, String consultType, String customId) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		ImUserDetail userDetail = userDetailService.getUserDetailById(userId);
		if(AppUtils.isBlank(userDetail)){
			return PathResolver.getPath(FrontPage.LOGIN);
		}		
		//密钥
		TreeMap<String, String> params=new TreeMap<String,String>();
		params.put("userId", userId);
		params.put("userName",userDetail.getUserName());
		params.put("shopId", String.valueOf(shopId));		
		if(AppUtils.isNotBlank(consultType) && consultType.equals(ImConsultTypeEnum.ORDER.value())) {//商品咨询
			params.put("consultType",ImConsultTypeEnum.ORDER.value());
			params.put("consultId",refundSn);
		}else {
			params.put("consultType",ImConsultTypeEnum.PROD.value());
			if(AppUtils.isBlank(skuId)) {
				params.put("consultId","0");//默认为空,不能为0
			}else {
				params.put("consultId",String.valueOf(skuId));
			}
		}		
		params.put("consultSource", DialogueSourceEnum.H5.value());
		String sign=MD5Util.createSign(params,Constants._MD5);
		request.setAttribute("shopId", shopId);
		request.setAttribute("userId",userId);
		request.setAttribute("userName",userDetail.getUserName());
		request.setAttribute("sign", sign);
		request.setAttribute("consultCount", 1);
		request.setAttribute("consultSource", DialogueSourceEnum.H5.value());
		request.setAttribute("userDetail", userDetail);
		if (AppUtils.isNotBlank(consultType) && consultType.equals(ImConsultTypeEnum.ORDER.value())) {
			request.setAttribute("consultType", ImConsultTypeEnum.ORDER.value());
			request.setAttribute("refundSn_On", refundSn);
			request.setAttribute("consultId", refundSn);
			request.setAttribute("skuId", 0);
		}else {
			request.setAttribute("consultType", ImConsultTypeEnum.PROD.value());
			if(AppUtils.isBlank(skuId)) {
				request.setAttribute("skuId", 0);
				request.setAttribute("consultId", 0);
			}else {
				request.setAttribute("skuId", skuId);
				request.setAttribute("consultId", skuId);
			}
		}
		request.setAttribute("customId", customId);
		request.setAttribute("IM_BIND_ADDRESS", propertiesUtil.getImBindAddress());
		return PathResolver.getPath(FrontPage.IM);
	}
	
	
}
