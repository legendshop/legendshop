/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.entity.*;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.business.page.FrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.page.CommonFowardPage;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.dto.KeyValueDto;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 商品快照控制器
 */
@Controller
public class ProductSnapshotController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(ProductSnapshotController.class);
	
	@Autowired
	private ProductSnapshotService productSnapshotService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ShopCommStatService shopCommStatService;
	
	@Autowired
	private DvyTypeCommStatService dvyTypeCommStatService;
	
	@Autowired
	private ShopDetailService shopDetailService;
	
	@Autowired
	private ProductCommentStatService productCommentStatService;

	@Autowired
	private UserDetailService userDetailService;
	
	/**
	 * 商品快照
	 * @param request
	 * @param response
	 * @param snapshotId
	 * @return
	 */
	@RequestMapping("/snapshot/{snapshotId}")
	public String snapshot(HttpServletRequest request, HttpServletResponse response, @PathVariable Long snapshotId) {
		Float shopscore=0f;
		Float prodscore=0f;
		Float logisticsScore=0f;
		DecimalFormat decimalFormat = new DecimalFormat("0.0");
		decimalFormat.setRoundingMode(RoundingMode.FLOOR);
		if (snapshotId == null) {
			return PathResolver.getPath(CommonFowardPage.INDEX_QUERY);
		}
		ProductSnapshot productSnapshot = productSnapshotService.getProductSnapshot(snapshotId);

		SecurityUserDetail securityUserDetail = UserManager.getUser(request);

		if(AppUtils.isNotBlank(securityUserDetail)){
			UserDetail user = userDetailService.getUserDetailById(securityUserDetail.getUserId());
			if(AppUtils.isNotBlank(user)){
				String password = user.getUserName()+"123456";
				request.setAttribute("user_name",user.getUserName());
				request.setAttribute("user_pass", password);
			}
		}

		if(productSnapshot!=null){
			Long prodId = productSnapshot.getProdId();
			Long shopId = productSnapshot.getShopId();
			Product prod = productService.getProductById(prodId);
			//解析商品参数  [{key:value},{key:value}]
			if(AppUtils.isNotBlank(productSnapshot.getParameter())){
				List<KeyValueDto> keyValList = JSONUtil.getArray(productSnapshot.getParameter(), KeyValueDto.class);
				request.setAttribute("paramList", keyValList);
			}
			//解析商品属性   key:val;key:val;
			if(AppUtils.isNotBlank(productSnapshot.getAttribute())){
				List<KeyValueDto> attrList = new ArrayList<KeyValueDto>();
				String[] attrs = productSnapshot.getAttribute().split(";");
				for (String attr : attrs) {
					if(AppUtils.isNotBlank(attr)){
						String[] kv = attr.split(":");
						KeyValueDto keyValueDto = new KeyValueDto();
						keyValueDto.setKey(kv[0]);
						keyValueDto.setValue(kv[1]);
						attrList.add(keyValueDto);
					}
				}
				request.setAttribute("attrList", attrList);
			}
			if(AppUtils.isNotBlank(shopId)){
				//查询店铺的详情信息
				ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
				if(shopDetail!=null){
					request.setAttribute("shopDetail", shopDetail);
				}
				
				//计算该店铺所有已评分商品的平均分
				ProductCommentStat prodCommentStat = productCommentStatService.getProductCommentStatByShopId(shopId);
				if(prodCommentStat!=null&&prodCommentStat.getScore()!=null&&prodCommentStat.getComments()!=null){
					prodscore=(float)prodCommentStat.getScore()/prodCommentStat.getComments();
				}
				request.setAttribute("prodAvg",decimalFormat.format(prodscore));

				//计算该店铺所有已评分店铺的平均分
				ShopCommStat shopCommentStat = shopCommStatService.getShopCommStatByShopId(shopId);
				if(shopCommentStat!=null&&shopCommentStat.getScore()!=null&&shopCommentStat.getCount()!=null){
					shopscore=(float)shopCommentStat.getScore()/shopCommentStat.getCount();
				}
				request.setAttribute("shopAvg",decimalFormat.format(shopscore));
				//计算该店铺所有已评分物流的平均分
				DvyTypeCommStat  dvyTypeCommStat = dvyTypeCommStatService.getDvyTypeCommStatByShopId(shopId);
				if(dvyTypeCommStat!=null&&dvyTypeCommStat.getScore()!=null&&dvyTypeCommStat.getCount()!=null){
				    logisticsScore=(float)dvyTypeCommStat.getScore()/dvyTypeCommStat.getCount();
				}
				request.setAttribute("dvyAvg",decimalFormat.format(logisticsScore));
				Float sum=0f;
				if (shopscore!=0&&prodscore!=0&&logisticsScore!=0) {
					sum=(float)Math.floor((shopscore+prodscore+logisticsScore)/3);
				}else if(shopscore!=0&&logisticsScore!=0){
					sum=(float)Math.floor((shopscore+logisticsScore)/2);
				}
				request.setAttribute("sum",decimalFormat.format(sum));
			}
			request.setAttribute("productSnapshot", productSnapshot);
			//用于title显示
			request.setAttribute("prod", prod);
			if(prod!=null && ProductStatusEnum.PROD_ONLINE.value().equals(prod.getStatus())){
				request.setAttribute("isProdOnline", true);
			}
			return PathResolver.getPath(FrontPage.SNAPSHOT);
		}else{
			return PathResolver.getPath(FrontPage.PROD_NON_EXISTENT);
		}
	}
}
