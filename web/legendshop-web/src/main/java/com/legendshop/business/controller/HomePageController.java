/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.controller;


import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.dto.app.ResultDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.business.page.FrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.VisitSourceEnum;
import com.legendshop.model.constant.VisitTypeEnum;
import com.legendshop.model.entity.VisitLog;
import com.legendshop.processor.VisitLogProcessor;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.web.helper.IPHelper;

/**
 * 首页.
 *
 */
@Controller
public class HomePageController extends BaseController {

	private final Logger logger = LoggerFactory.getLogger(HomePageController.class);

	/**
	 * 日志查看
	 */
	@Autowired
	private VisitLogProcessor visitLogProcessor;


	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * 前台首页.
	 *
	 */
	@RequestMapping(value ="/", method = RequestMethod.GET)
	public String index(HttpServletRequest request, HttpServletResponse response) {
		return doAccessHomePage(request, response);
	}


	/**
	 * 前台首页home.
	 *
	 */
	@RequestMapping(value ="/home", method =RequestMethod.GET)
	public String home(HttpServletRequest request, HttpServletResponse response) {
		return doAccessHomePage(request, response);
	}


	/**
	 * 首页楼层
	 */
	@RequestMapping(value="/loadFloor", method = RequestMethod.GET)
	public String loadFloor(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(FrontPage.FLOOR);
	}

	/**
	 * 记录用户的访问记录
	 */
	private void logUserAccess(HttpServletRequest request) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user == null ? null : user.getUsername();
		// 多线程记录访问历史
		if (systemParameterUtil.isVisitLogEnable()) {
			VisitLog visitLog = new VisitLog(IPHelper.getIpAddr(request), userName, systemParameterUtil.getDefaultShopId(), systemParameterUtil.getDefaultShopName(),
					VisitTypeEnum.INDEX.value(), VisitSourceEnum.PC.value(), new Date());
			visitLogProcessor.process(visitLog);
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("[{}],{} visit index {}", IPHelper.getIpAddr(request), userName, systemParameterUtil.getDefaultShopName());
			}
		}
	}

	/**
	 * 访问首页
	 */
	private String doAccessHomePage(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) {
		try {
			logUserAccess(request);
			com.legendshop.security.model.SecurityUserDetail su = com.legendshop.security.UserManager.getUser(request);
			javax.servlet.http.HttpSession session = request.getSession();
			if (com.legendshop.util.AppUtils.isNotBlank(su)) {
				session.setAttribute("userType", su.getLoginUserType());
			}
			//默认店铺进入平台首页
			return PathResolver.getPath(FrontPage.HOME);
		} catch (Exception e) {
			logger.error("invoking index", e);
			// redirect to the install page
			throw new com.legendshop.base.exception.BusinessException(e, "Visit home error", com.legendshop.base.exception.ErrorCodes.BUSINESS_ERROR);
		}
	}


	/**
	 * 获取推荐浏览器页面
	 */
	@GetMapping(value="/browser")
	public String browser() {
		return PathResolver.getPath(FrontPage.BROWSER);
	}

	/**
	 * 返回时间差
	 */
	@ResponseBody
	@GetMapping(value = "/timeDifference")
	public Long timeDifference(@RequestParam("time") Long time) {
		try {
			Date nowDate = new Date();
			return nowDate.getTime() - time;
		} catch (Exception e) {
			return 0L;
		}
	}
}
