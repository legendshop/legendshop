/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.page.FrontPage;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.Constants;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SystemUtil;

/**
 * 购物车控制器。.
 */
@Controller
@RequestMapping("/basket")
public class BasketController extends BaseController {

	/** The log. */
	protected final Logger log = LoggerFactory.getLogger(BasketController.class);

	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;

	/**
	 * Query.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the string
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response) {
		String prodId = request.getParameter("prodId");
		if (AppUtils.isNotBlank(prodId)) {
			return getBasket(request, response, Long.parseLong(prodId));
		} else {
			throw new BusinessException("product id can not be null, prodId =  " + prodId);
		}

	}

	/**
	 * Gets the basket.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param prodId
	 *            the prod id
	 * @return the basket
	 */
	private String getBasket(HttpServletRequest request, HttpServletResponse response, Long prodId) {

		SecurityUserDetail user = UserManager.getUser(request);
		
		if (user == null) {
			String destView = "/basket/load/";
			request.setAttribute(Constants.RETURN_URL, propertiesUtil.getPcDomainName() + SystemUtil.getContextPath() + destView + prodId);
			return PathResolver.getPath(FrontPage.LOGIN_HINT);
		}
		if (prodId == null) {
			log.error("Missing prodId in getBasket");
			return PathResolver.getPath(FrontPage.ERROR);
		}
		String count = request.getParameter("count");
		if (count == null) {
			count = "1";
		}
		request.getSession().setAttribute(Constants.BASKET_HW_COUNT, count);
		String prodattr = request.getParameter("prodattr");
		request.getSession().setAttribute(Constants.BASKET_HW_ATTR, prodattr);

		return PathResolver.getPath(FrontPage.BUY);
	}

	/**
	 * Load.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param prodId
	 *            the prod id
	 * @return the string
	 */
	@RequestMapping("/load/{prodId}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {
		return getBasket(request, response, prodId);
	}

}
