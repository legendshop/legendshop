/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;
import com.legendshop.core.helper.ThreadLocalContext;

/**
 * The Enum FrontPage.
 */
public enum FrontPage implements PageDefinition {
	/** 可变路径 */
	VARIABLE(""),
	
	/** 错误页面. */
	ERROR_PAGE(ERROR_PAGE_PATH),
	
	//登录的用户session的情况
	LOGIN_USER("/login_user"),
	
	/** 错误页面. */
	FAIL_PAGE("/failPage"), 

	/** The INSTALL. */
	INSTALL("/install/index"),

	/** The AL l_ page. */
	ALL_PAGE("/all"),

	/** The TOPALL. */
	TOPALL("/topAll"),

	/** The TOP. */
	TOP("/top"),
	
	/** The header. */
	HEADER("/header"),

	/** The PRO d_ pi c_ gallery. */
	PROD_PIC_GALLERY("/gallery"),

	/** the top page. */
	HOME_TOP("/home/top"),

	/** the common bottom page. */
	BOTTOM("/bottom"),
	
	CART_TOP("/cartTop"),
	
	SIMPLE_TOP("/simpleTop"),
	
	//帮助中心
	HELP_CENTER("/helpCenter"),

	// 收费咨询
	PAID_INFORMATION("/paidInformation"),
	
	//帮助中心文章
	HELP_CENTER_NEWS("/helpCenterNews"),
	
	SIMPLE_BOTTOM("/simpleBottom"),

	// 产品查看历史
	/** The visited prod. */
	VISITED_PROD("/visitedprod"),
	
	VISITED_PROD_OLD("/visitedprod_old"),
	
	LIKE_PROD("/guessLike"),

	/** The product comments. */
	PRODUCT_COMMENTS("/prodcomments"),

	/** The product comment list. */
	PRODUCT_COMMENT_LIST("/prodcommentlist"),

	/** The product consults. */
	PRODUCT_CONSULTS("/prodconsults"),

	/** The product consults list. */
	PRODUCT_CONSULTS_LIST("/prodconsultlist"), 
	
	BRAND_LOAD_PAGE("/brandload"), 
	
	/** 楼层 **/
	FLOOR("/floor"),
	
	/** 协议弹框 **/
	AGREEMENT_OVERLAY("/agreeMentOverlay"),
	
	/** 验证身份 **/
	PROVE_STATUS("/proveStatus"),
	
	/** 拦截页面**/
	INTERCEPT("/intercept"),
	
	/** 更改密码**/
	CHANGE_PWD ("/changePwd"),
	
	/** 修改密码成功**/
	PWD_RESET_SUCCESS("/pwdResetSuccess"),
	
	/** 修改密码失败**/
	PWD_RESET_FAIL("/pwdResetFail"),
	
	/** 验证邮箱**/
	SEND_EMAIL_PAGE("/sendEmailPage"),
	
	/** 注册页面 **/
	REG("/regs"),
	
	/** 注册绑定 **/
	REG_BIND("/regbind"),
	
	/** 绑定登陆界面 **/
	REG_BIND_EDIT("/regbindEdit"),
	
	/** 评论详情页面 **/
	PRODUCT_COMMENTS_DETAIL("/productCommentsDetail"),
	
	/**城市选项页面 **/
	CITIES_PAGE("/citiesPage"),
	
	/**地区选项页面**/
	AREA_PAGE("/areasPage"),
	
	/**用户地址分页页面**/
	USERADDRESS_PAGE("/userAddressPage"),
	
	/**选定的收货地址**/
	CONSIGNEE_ADDRESS("/consigneeAddress"),
	
	/**编辑用户收货地址页面**/
	EDITADDRESS_PAGE("/editAddressPage"),
	
	/**添加用户收货地址页面**/
	ADD_ADDRESS_PAGE("/addAddressPage"),
	
	/**发票信息分页**/
	INVOICE_PAGE("/invoicePage"),
	
	/**商品参数页面**/
	PROD_PARAM_PAGE("/prodParam"),

	/**商品服务说明**/
	PROD_SERVICE_SHOW("/prodService"),

	/**店铺营业执照信息**/
	SHOP_BUSINESS_DETAIL("/businessMessage"),

	/**合并--------*/
	
	/** 购物车 */
	BUY("/buy"),
	
	/** 所有品牌 **/
	ALL_BRANDS("/allbrands"),
	
	/** 操作成功 **/
	AFTER_OPERATION("/afteroperation"),

	/** 注册绑定. */
	REGBIND_EDIT("/regbindEdit"),
	
	/** 所有分类 **/
	ALL_CATEGORYS("/allCategorys"),
	
	/** 错误页面 **/	
	ERROR("/error"),
	
	/** 权限不足页面 **/	
	INSUFFICIENT_AUTHORITY("/error401"), 
	
	/** 主页 */
	HOME("/home"),
	
	/** 主页 */
	INDEX("/index"),
	
	/** 文章. */
	NEWS("/news"),
	
	/** 文章列表. */
	ALL_NEWS("/allnews"),
	
	SELECTSHOPTYPE("/openShop/selectShopType"),
	
	SETTLEDAGREEMENT("/openShop/settledAgreement"),
	
	CONTACTINFO("/openShop/contactInfo"),
	
	/** 登录 **/
	LOGIN("/login"),
	
	/** 登录 **/
	EMPLOYEE_LOGIN("/employeeLogin"),
	
	/** 登录 **/
	SHOPLOGIN("/shopLogin"),
	
	/** 个人店铺详细信息 */
	SHOPDETAIL("/openShop/shopdetailPage"),
	
	/** 企业店铺详细信息 */
	COMPANYSHOPDETAIL("/openShop/shopCompanyDetail"),
	
	COMPANYINFO("/openShop/companyInfo"),
	
	CERTIFICATEINFO("/openShop/certificateInfo"),
	
	ONLINEAGREEMENT("/openShop/onlineAgreement"),
	
	/*协议*/
	SHOP_AGREEMENT("/openShop/agreement"),
	
	CHECKSPEED("/openShop/checkSpeed"),
	
	/** 登录提示 **/
	LOGIN_HINT("/login"),
	
	/** 订单详情 */
	ORDERDETAILS("/orderDetails"),
	
	/** 订单成功 */
	ORDERSUCCESS("/orderSuccess"),
	
	RETURNPAY("/returnPay"),
	
	/** 支付结果消息 */
	PAY_MESSAGE("/payMessage"),
	
	/** The pay page. */
	PAY_PAGE("/payment/payto"),
	
	/** The pay page. */
	WEIXIN_SACNCODE("/payment/WeiXinSacnCode"),
	
	PRODUCT_COMMENT("/productcomment"),
	
	/** 商品比较页面. */
	COMPARE("/compare"),
	
	/** 到货通知弹出窗 */
	PRODUCT_ARRIVAL_INFORM_PAGE("/prodArrInfoPage"),
	
	/** 商品详情 **/
	VIEWS("/views"),
	
	/** 商品不存在的提示 **/	
	PROD_NON_EXISTENT("/nonExitProd"),
	
	/**打开环信*/
	PROD_HUANXIN("/huanXin"),

	/** 商品快照 **/	
	SNAPSHOT("/snapshot"), 
	
	/** 公告 **/
	PUB("/pub"),
	
	/** 购物车 商品table */
	BUY_TABLE("/buyTable"),
	
	/** 商家列表*/
	SHOP_SEEK("/shopSeek"),

	/** 专题列表 **/
	THEME_LIST("/themeList"),
	
	/** 专题. */
	THEME("/theme"),
	
	/** 开店 */
	OPEN_SHOP("/openshop"),
	
	/**开店成功*/
	OPEN_SUCCESS("/openSuccess"),
	
	/** 开店成功. */
	AFTER_OPENSHOP("/afteropenshop"),
	
	//操作错误
	OPERATION_ERROR("/operationError"),
	
	/**更改密码 **/
	SETNEWPWD("/setNewPwd"),
	
	/**找回密码  */
	RETRIEVE_PD("/retrievePassword"),
	
	
	/**测试页面**/
	TEST_PAGE("/testPage"),
	
	/** PC IM聊天页面 */
	IM("/im/im"), 
	
	/** 未知什么页面 */
	IM_CONSULT("/im/imConsult"),

	/** 未知什么页面 */
	IM_ORDER("/im/imOrder"),
	
	/** 未知什么页面 */
	IM_SHOP_DETAIL("/im/imShopDetail"),
	
	/** 未知什么页面 */
	IM_PROBLEM("/im/imProblem"),

	/** 搜索 */
	PRODUCT_SORT_LIST("/prodsortlist"),
	
	/** 搜索商品参数 */
	PRODUCT_PARAM_LIST("/prodParamList"),
	
	/** 商品列表 **/
	LIST("/list"),
	
	/** 绑定账号页面 */
	BIND_ACCOUNT("/bindAccount"), 
	
	/** 推荐浏览器页面  */
	BROWSER("/browser"),
	
	/** 门店选择页面  */
	SHOP_STORE_SELECT("/shopStoreSelect"),

	/** 发票列表弹窗页面 */
	INVOICE_LIST_POP("/invoiceListPop"),
	
	;

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new front page.
	 * 
	 * @param value
	 *            the value
	 * @param template
	 *            the template
	 */
	private FrontPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.String, java.util.List)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath(ThreadLocalContext.getFrontType(), path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
