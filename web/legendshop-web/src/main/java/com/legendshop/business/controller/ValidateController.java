/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.business.page.FrontPage;
import com.legendshop.business.page.UserCenterFrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.SendIntegralRuleEnum;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.UserSecurity;
import com.legendshop.spi.service.IntegralService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;

/**
 * 校验控制器
 */
@Controller
public class ValidateController extends BaseController {
		
	@Autowired
	private UserDetailService userDetailService;
	
	/**
	 * 积分服务
	 */
	@Autowired
	private IntegralService integralService;
	
	
	/**
	 * 从第三方邮件验证邮箱是否有效
	 * @param request
	 * @param response
	 * @param userName
	 * @param code
	 * @return
	 */
	@RequestMapping("/validate/mail")
	public String validateMail(HttpServletRequest request, HttpServletResponse response, String userName, String code) {
		//第三方邮箱验证逻辑1，用户请求回来通过用户名与CODE查询数据不为空且发送时间与当前时间间隔不小于24小时则把邮箱箱更新到userDetail中，否则提示链接过期，成功后把当前数据的CODE删除
		try {
			if(AppUtils.isNotBlank(userName)&&AppUtils.isNotBlank(code)){
				UserSecurity userSecurity =userDetailService.getUserSecurity(userName);
				if(AppUtils.isNotBlank(userSecurity) && code.equals(userSecurity.getEmailCode())){
					//判断发送加上24小时大于当前时间
					if(DateUtil.add(userSecurity.getSendMailTime(), Calendar.HOUR, 24).after(new Date())){
						Integer emailVerifn = userDetailService.getMailVerifn(userName);
						UserDetail userDetail =userDetailService.getUserDetail(userName);
						if(emailVerifn == 0){
							//该用户安全等级+1,邮箱验证变为1 且验证次数变为1
							userSecurity.setSecLevel(userSecurity.getSecLevel()+1);
							userSecurity.setTimes(1);
							userSecurity.setMailVerifn(1);
							//First validate email send events
							integralService.addScore(userDetail.getUserId(),SendIntegralRuleEnum.VAL_EMAIL);
						}
						
						userDetail.setUserMail(userSecurity.getUpdateMail());
						userSecurity.setEmailCode("");
						userDetailService.updateUserInfo(userSecurity,userDetail);
						request.setAttribute("secLevel", userSecurity.getSecLevel());
						return PathResolver.getPath(UserCenterFrontPage.MAIL_FINISH);
					}
				}
			}
		} catch (Exception e) {
			return PathResolver.getPath(UserCenterFrontPage.MAIL_FAIL);
		}
		
		return PathResolver.getPath(UserCenterFrontPage.MAIL_FAIL);
		
	}

	/**
	 * 跳到邮箱编辑界面
	 * @param request
	 * @param response
	 * @param userName
	 * @param code
	 * @return 
	 */
	@RequestMapping("/preUpdateMail/{userName}/{code}")
	public String preUpdateMail(HttpServletRequest request, HttpServletResponse response,@PathVariable String userName,@PathVariable String code) {
		if(AppUtils.isNotBlank(userName)&& AppUtils.isNotBlank(code)){
			UserSecurity Security = userDetailService.getUserSecurity(userName);
			Date end = new Date();
			long day = (end.getTime()-Security.getSendMailTime().getTime())/(24*60*60*1000);
			//判断是否为24小时内以及是第一次点击
			if(day ==1 || Security.getTimes() ==1){
				return PathResolver.getPath(UserCenterFrontPage.MAIL_FAIL);
			}
			
			if(code.equals(Security.getEmailCode())){
				//验证次数变为1
				userDetailService.updatetimes(1,userName);
				request.setAttribute("code", code);
				request.setAttribute("userName", userName);
				return PathResolver.getPath(UserCenterFrontPage.TO_UPDATE_MAIL);
			}else{
				return PathResolver.getPath(UserCenterFrontPage.MAIL_FAIL);
			}
		}else{
			return PathResolver.getPath(UserCenterFrontPage.MAIL_FAIL);
		}
		
	}
	
	/**
	 * 跳到密码编辑界面
	 * @param request
	 * @param response
	 * @param userName
	 * @param code
	 * @return
	 */
	@RequestMapping("/preUpdatePassWord/{userName}/{code}")
	public String preUpdatePassWord(HttpServletRequest request, HttpServletResponse response,@PathVariable String userName,@PathVariable String code) {
			
		if(AppUtils.isNotBlank(userName)&& AppUtils.isNotBlank(code)){
			UserSecurity Security = userDetailService.getUserSecurity(userName);
			Date end = new Date();
			long day = (end.getTime()-Security.getSendMailTime().getTime())/(24*60*60*1000);
			//判断是否为24小时内以及是第一次点击
			if(day ==1 || Security.getTimes() ==1){
				return PathResolver.getPath(UserCenterFrontPage.MAIL_FAIL);
			}
			
			if(code.equals(Security.getEmailCode())){
				//验证次数变为1
				userDetailService.updatetimes(1,userName);
				request.setAttribute("code", code);
				request.setAttribute("userName", userName);
				return PathResolver.getPath(UserCenterFrontPage.UPDATE_PASSWORD);
			}else{
				return PathResolver.getPath(UserCenterFrontPage.MAIL_FAIL);
			}
		}else{
			return PathResolver.getPath(UserCenterFrontPage.MAIL_FAIL);
		}
		
	}
	
	/**
	 * 跳到支付密码编辑界面
	 * @param request
	 * @param response
	 * @param userName
	 * @param code
	 * @return
	 */
	@RequestMapping("/preUpdatePaymentpassword/{userName}/{code}")
	public String preUpdatePaymentpassword(HttpServletRequest request, HttpServletResponse response,@PathVariable String userName,@PathVariable String code) {
			
		if(AppUtils.isNotBlank(userName)&& AppUtils.isNotBlank(code)){
			UserSecurity Security = userDetailService.getUserSecurity(userName);
			Date end = new Date();
			long day = (end.getTime()-Security.getSendMailTime().getTime())/(24*60*60*1000);
			//判断是否为24小时内以及是第一次点击
			if(day ==1 || Security.getTimes() ==1){
				return PathResolver.getPath(UserCenterFrontPage.MAIL_FAIL);
			}
			
			if(code.equals(Security.getEmailCode())){
				//验证次数变为1
				userDetailService.updatetimes(1,userName);
				request.setAttribute("code", code);
				request.setAttribute("userName", userName);
				return PathResolver.getPath(UserCenterFrontPage.UPDATE_PAYMENT_PASSWORD);
			}else{
				return PathResolver.getPath(UserCenterFrontPage.MAIL_FAIL);
			}
		}else{
			return PathResolver.getPath(UserCenterFrontPage.MAIL_FAIL);
		}
		
	}
	
	
	/**
	 * 跳转到完成页面
	 * @param request
	 * @param response
	 * @param userName
	 * @param code
	 * @return
	 */
	@RequestMapping("/preFinish/{userName}/{code}")
	public String preFinish(HttpServletRequest request, HttpServletResponse response,@PathVariable String userName,@PathVariable String code) {

		if(AppUtils.isNotBlank(userName)&& AppUtils.isNotBlank(code)){
			
			UserSecurity Security = userDetailService.getUserSecurity(userName);
			Date end = new Date();
			long day = (end.getTime()-Security.getSendMailTime().getTime())/(24*60*60*1000);
			//判断是否为24小时内以及是第一次点击
			if(day ==1 || Security.getTimes() ==1){
				return PathResolver.getPath(FrontPage.FAIL_PAGE);
			}
			
			
			if(code.equals(Security.getEmailCode())){
				//在数据库保存邮箱
				userDetailService.updateUserEmail(Security.getUpdateMail(),userName);
				
				Integer secLevel = userDetailService.getSecLevel(userName);
				request.setAttribute("secLevel", secLevel);
				return PathResolver.getPath(UserCenterFrontPage.FINISH);
			}else{
				return PathResolver.getPath(FrontPage.FAIL_PAGE);
			}
		}else{
			return PathResolver.getPath(FrontPage.FAIL_PAGE);
		}
		
	}

}
