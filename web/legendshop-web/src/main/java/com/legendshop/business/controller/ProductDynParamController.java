/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.business.page.FrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;

/**
 * 商品规格 商品动态属性，动态参数控制器.
 */
@Controller
@RequestMapping(value = "/dynamic")
public class ProductDynParamController extends BaseController {
	
	/**
	 * 查询商品动态参数
	 * @param prodId
	 * @return
	 */
	@RequestMapping(value = "/queryDynamicParameter")
	public String queryDynamicParameter(HttpServletRequest request, HttpServletResponse response,Long prodId) {
		request.setAttribute("prodId", prodId);
		return PathResolver.getPath(FrontPage.PROD_PARAM_PAGE);
	}

}
