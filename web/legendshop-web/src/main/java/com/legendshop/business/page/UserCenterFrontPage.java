/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * The Enum UserCenterFrontPage.
 */
public enum UserCenterFrontPage implements PageDefinition {

	
	/** The VARIABLE. 可变路径 */
	VARIABLE(""),
	
	PHOTO_OVERLAY("/photoOverlay"),
	
 	REGISTER_OVERLAY("/registerOverlay"),
 	
	LOGIN_OVERLAY_CONTENT("/loginOverlayContent"), 
	
	LOAD_ADDRESS_OVERLAY("/addressOverlay"),
	
	INTEREST_OVERLAY("/interestOverlay"),
	
	ISEXISTS_INTEREST("/isExistsInterest"),
	
	LOGIN_OVERLAY("/loginOverlay"),
	
	INBOX_MAIL_INFO("/inBoxMailInfo"),
	
	FAIL("/fail"),
	
	UPDATE_MOBILE("/updateMobile"),
	
	TO_UPDATE_MAIL("/toUpdateMail"),
	
	UPDATE_PASSWORD("/updatePassword"),
	
	UPDATE_PAYMENT_PASSWORD("/updatePaymentpassword"),
	
	SEND_MAIL_SUCCESS("/sendMailSuccess"),
	
	LOAD_APPLY_PAYINFO("/loadApplyPayInfo"),
	
	APPLY_PAYINFO("/applyPayInfo"), 
	
	ACCUSATION_OVERLAY("/accusationOverlay"),
	
	
	/////////////////////////////////////////////////////////
	

	@Deprecated
	PREUPDATE_MAIL("/preUpdateMail"),
	
	USERCENTER_HOME("/usercenterHome"), 
	
	PROD_COMMENT("/prodcomment"),
	
	FAVORITE_SHOP("/favoriteShop"),
	
	MY_FAVOURITE("/myfavorite"),
	
	PROD_CONSULT("/prodcons"), 
	
	ACCUSATION_LIST("/accusationList"),
	
	SITE_MESSAGE_INBOX("/inbox"), 
	
	SITE_MESSAGE_OUTBOX("/outbox"),
	
	SYSTEM_MESSAGES("/systemMessages"),
	
	SEND_MESSAGE("/sendMessage"),
	
	ACCUSATION("/accusation"),
	
	/** 显示用户个人资料 */
	USER_INFO("/userinfo"),
	
	/** 编辑用户个人资料 */
	USER_EDIT("/userInfoEdit"),
	
	USER_GRADE("/userGrade"),
	
	USER_SHARE("/share"),
	
	ACCOUNT_SECURITY("/security"),
	
	RECEIVE_ADDRESS("/receiveaddress"),
	
	
	TO_UPDATE_MOBILE("/updateMobile"),
	
	FINISH("/finish"),
	
	ESTABLISH_IDENTITY("/establishIdentity"),
	
	PASSWORD_VERIFICATION("/passwordVerification"),
	
	EMAIL_VERIFICATION("/emailVerification"),
	
	ISEXISTS_FAVORITE_SHOP("/isExistsfavoriteShop"),
	
	/** 账户绑定 **/
	ACCOUNT_BINDING("/accountBinding"),
	
	/** 我的订单 */
	MYORDER("/myorder"),
	
	/** 我的发票 */
	MYINVOICE("/myinvoice"),
	
	/** 预售订单 */
	PRESELLORDER("/presellOrder"),
	
	/** 预售订单回收站 */
	PRESELLORDERDELS("/presellOrderDels"),
	
	/** 查看预售订单详情 */
	PRESELLORDERDETAIL("/presellOrderDetail"),
	
	MYORDERDEL("/orderDels"),
	
	MYDISTORDER("/myDistOrder"),
	
	ORDERDETAIL("/orderDetail"),
	
	ORDEREXPRESS("/orderExpress"),
	
	APPLYORDERRETURNLIST("/applyOrderReturnList"),
	
	CHOOSE_LOGISTICS("/chooseLogistics"),
	
	MYORDERRETURNTIMEOUT("/myorderreturntimeout"),
	
	MYORDERRETURN("/myReturnOrder"),
	
	MYORDERRETURNDETAIL("/myReturnOrderDetail"),
	
	MYMONEYRETURN("/myReturnMoney"),
	
	APPLYORDERRETURN("/applyOrderReturn"),
	
	EXPENSES_RECORD_LIST("/expensesRecordList"), 
	
	MAIL_FINISH("/mailFinish"), 
	
	MAIL_FAIL("/mailFail"),
	
	/** 用户发票管理列表 */
	INVOICE_LIST("/invoiceList"),

	/** 新增发票页面 */
	ADD_INVOICE("/addInvoice"),

	CANCLEORDERSHOE("/cancleOrderShow"),
	
/** ------------------- 退款/退货 ----------------- */
	
	/** 申请 "订单退款" 页面 **/
	APPLY_ORDER_REFUND("/refund/applyOrderRefund"),
	
	/** 申请 "订单项退款,退货" 页面 **/
	APPLY_ITEM_RETURN("/refund/applyItemReturn"),
	
	/** 退款详情页面 **/
	REFUND_DETAIL("/refund/refundDetail"),
	
	REFUND_DETAIL_NEW("/refund/refundDetailNew"),
	
	/** 退货详情页 */
	RETURN_DETAIL("/refund/returnDetail"), 
	
	/** 退货详情页 */
	RETURN_DETAIL_NEW("/refund/returnDetailNew"), 
	
	/** 退款退货列表页 */
	REFUND_REFUND_LIST("/refund/refundReturnList"),
	
	/** 退款列表 */
	REFUND_LIST("/refund/refundList"),
	
	/** 退货列表 */
	RETURN_LIST("/refund/returnList"),
	
	/** 客服页面  */
	IM_MESSAGES("/im/imMessages"),
	
	;

	/** The value. */
	private final String value;

	private UserCenterFrontPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath("usercenter", path);
	}

	public String getNativeValue() {
		return value;
	}

}
