/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.business.page.FrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.ProductTypeEnum;
import com.legendshop.model.dto.CategoryDto;
import com.legendshop.spi.service.CategoryManagerService;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.util.AppUtils;

/**
 * 商品分类控制器
 * @author bevan
 *
 */
@Controller
public class CategoryManageController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(CategoryManageController.class);
	
	@Autowired(required= false)
	private CategoryManagerService categoryManagerService;
	
	@Autowired(required= false)
	private CategoryService categoryService;


	@RequestMapping("/allCategorys")
	public String allCategorys(HttpServletRequest request, HttpServletResponse response) {
		final Set<CategoryDto> categoryList=categoryService.getAllCategoryByType(ProductTypeEnum.PRODUCT.value());
		if(AppUtils.isNotBlank(categoryList)){
			int size = categoryList.size() / 2;
			if(size < 1){
				size = 1;
			}
		   request.setAttribute("halfSize", size);
		}
		request.setAttribute("sortList", categoryList);
		return PathResolver.getPath(FrontPage.ALL_CATEGORYS);
	}	
}
