/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.business.page.FrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.page.CommonRedirectPage;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.entity.News;
import com.legendshop.model.entity.Pub;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.NewsService;
import com.legendshop.spi.service.PubService;
import com.legendshop.util.AppUtils;

/**
 * 公告控制器.
 */
@Controller
public class PubController extends BaseController {

	@Autowired
	private NewsService newsService;

	/** The pub service. */
	@Autowired
	private PubService pubService;

	/**
	 *  公告
	 *  
	 */
	@RequestMapping("/pub/{id}")
	public String pub(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		if (AppUtils.isNotBlank(id)) {
			Pub pub = pubService.getPubById(id);
			if (AppUtils.isBlank(pub)) {
				return PathResolver.getPath(CommonRedirectPage.HOME_QUERY);
			}
			
			request.setAttribute("pub", pub);
			
			SecurityUserDetail user = UserManager.getUser(request);
			
			boolean isUser =  UserManager.getShopUser(user) == null;
			
			// 上一条 下一条
			List<Pub> pubList = pubService.getPub(pub.getUserName(), id, isUser);
			request.setAttribute("pubList", pubList);
		

			// load news by category
			Map<KeyValueEntity, List<News>> newsCatList = newsService.getNewsByCategory();
			request.setAttribute("newsCatList", newsCatList);

		}
		return PathResolver.getPath(FrontPage.PUB);
	}

}
