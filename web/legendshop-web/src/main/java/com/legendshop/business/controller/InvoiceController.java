/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.business.dao.InvoiceDao;
import com.legendshop.business.dao.InvoiceSubDao;
import com.legendshop.business.page.FrontPage;
import com.legendshop.business.page.UserCenterFrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.OrderDeleteStatusEnum;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.order.MySubDto;
import com.legendshop.model.dto.order.OrderSearchParamDto;
import com.legendshop.model.entity.Invoice;
import com.legendshop.model.entity.InvoiceSub;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.Sub;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.InvoiceService;
import com.legendshop.spi.service.InvoiceSubService;
import com.legendshop.spi.service.OrderService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.SubService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 发票管理
 * */
@Controller
@RequestMapping("/p/invoice")
public class InvoiceController extends BaseController {

	@Autowired
	private InvoiceService invoiceService;
	
	@Autowired
	private SubService subService;
	
	@Autowired
	private OrderService orderService;
	
	protected InvoiceDao invoiceDao;
	
	protected InvoiceSubDao invoiceSubDao;
	
	@Autowired
	private ShopDetailService shopDetailService;
	
	@Autowired
	private InvoiceSubService InvoiceSubService;

	/**
	 * 发票信息管理列表
	 */
	@RequestMapping(value = "/invoiceList", method = RequestMethod.GET)
	public String invoiceList(HttpServletRequest request, HttpServletResponse response,String curPageNO) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();

		PageSupport<Invoice> ps = invoiceService.getInvoicePage(userName, curPageNO, 10);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(UserCenterFrontPage.INVOICE_LIST);
	}

	/**
	 * 添加发票信息弹框页面
	 */
	@RequestMapping(value = "/createPage", method = RequestMethod.GET)
	public String createPage(HttpServletRequest request, HttpServletResponse reponse, Invoice invoice) {

		request.setAttribute("invoice",invoice);
		return PathResolver.getPath(UserCenterFrontPage.ADD_INVOICE);
	}



	/**
	 * 修改发票信息
	 */
	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public String update(HttpServletRequest request, HttpServletResponse reponse, Long invoiceId) {

		Invoice invoice = invoiceService.getInvoice(invoiceId);
		if(null == invoice) {
			return "对不起，您操作的数据有误";
		}

		request.setAttribute("invoice",invoice);
		return PathResolver.getPath(UserCenterFrontPage.ADD_INVOICE);
	}




	/**
	 * 保存发票
	 * @param invoice 发票信息
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public String createAndSave(HttpServletRequest request, HttpServletResponse reponse, Invoice invoice) {
		SecurityUserDetail user = UserManager.getUser(request);

		if (AppUtils.isBlank(user)){

			return "登录失效，请刷新后重新保存";
		}

		invoice.setUserId(user.getUserId());
		invoice.setUserName(user.getUsername());
		invoice.setCreateTime(new Date());

		// 保存或更新发票
		Long invoiceId = invoiceService.saveInvoice(invoice);

		//检查当前保存的发票是否选择设置为默认
		if(invoice.getCommonInvoice().equals(1)){

			invoiceService.updateDefaultInvoice(invoiceId,user.getUsername());
		}

		return Constants.SUCCESS;
	}


    /**
     * 设置发票为默认
     */
	@RequestMapping(value = "/changeInvoiceCommon", method = RequestMethod.POST)
	@ResponseBody
	public String changeInvoiceCommon(HttpServletRequest request, HttpServletResponse reponse, Long id){

        Invoice invoice = invoiceService.getInvoice(id);
        if(null == invoice) {
            return "对不起，您操作的数据有误";
        }

		SecurityUserDetail userDetail = UserManager.getUser(request);
		invoiceService.updateDefaultInvoice(id, userDetail.getUsername());
		return Constants.SUCCESS;
	}

	
	/**
	 * 删除发票
	 */
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	@ResponseBody
	public String delete(HttpServletRequest request, HttpServletResponse reponse, @PathVariable Long id) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		Invoice invoice = invoiceService.getInvoice(id);
		if(null == invoice || !invoice.getUserId().equals(userId)) {
			return "对不起，您操作的数据有误";
		}
		invoiceService.delById(id);
		return Constants.SUCCESS;
	}

	/**
	 * 补开发票
	 * @param subNumber 订单号
	 * @return
	 */
	@RequestMapping(value = "/getInvoice", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getInvoice(HttpServletRequest request, HttpServletResponse response, String subNumber) {

		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail(-1, "登录失效，请刷新后重试");
		}

		String userId = user.getUserId();
		Sub sub = subService.getSubBySubNumber(subNumber);

		Invoice defaultInvoice = invoiceService.getDefaultInvoice(userId);

		if(AppUtils.isBlank(defaultInvoice)){
			return ResultDtoManager.fail(0, "请先设置默认发票");
		}

		InvoiceSub invoiceSub = new InvoiceSub();
		invoiceSub.setUserId(defaultInvoice.getUserId());
		invoiceSub.setUserName(defaultInvoice.getUserName());
		invoiceSub.setType(defaultInvoice.getType());
		invoiceSub.setTitle(defaultInvoice.getTitle());
		invoiceSub.setCompany(defaultInvoice.getCompany());
		invoiceSub.setContent(defaultInvoice.getContent());
		invoiceSub.setInvoiceHumNumber(defaultInvoice.getInvoiceHumNumber());
		invoiceSub.setRegisterAddr(defaultInvoice.getRegisterAddr());
		invoiceSub.setRegisterPhone(defaultInvoice.getRegisterPhone());
		invoiceSub.setDepositBank(defaultInvoice.getDepositBank());
		invoiceSub.setBankAccountNum(defaultInvoice.getBankAccountNum());
		invoiceSub.setCreateTime(new Date());

		Long invoiceSubId =  InvoiceSubService.saveInvoiceSub(invoiceSub);
		sub.setNeedInvoice(true);
		sub.setInvoiceSubId(invoiceSubId);
		subService.updateSub(sub);
		return ResultDtoManager.success();
	}
	
	/**
	 * 加载我的发票列表
	 */
	@RequestMapping(value = "/myInvoice", method = RequestMethod.GET)
	public String myInvoice(HttpServletRequest request, HttpServletResponse response, String curPageNO, Integer state_type, String order_sn, Date startDate,
			Date endDate, String sub_type) {
		SecurityUserDetail user = UserManager.getUser(request);
		
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(FrontPage.LOGIN_HINT);
		}
		
		String userId = user.getUserId();
		Long shopId = user.getShopId();

		// 默认一页显示条数
		int pageSize = 10;
		
		OrderSearchParamDto paramDto = new OrderSearchParamDto();
		Date fromDate = startDate;
		Date toDate = endDate;
		if (AppUtils.isNotBlank(endDate)) {
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(endDate);
			calendar.add(calendar.DATE, 1);
			toDate = calendar.getTime();
		}
		paramDto.setPageSize(pageSize);
		paramDto.setCurPageNO(curPageNO);
		paramDto.setUserId(userId);
		paramDto.setStatus(state_type);
		paramDto.setSubNumber(order_sn);
		paramDto.setStartDate(startDate);
		paramDto.setEndDate(toDate);
		paramDto.setSubType(sub_type);
		paramDto.setDeleteStatus(OrderDeleteStatusEnum.NORMAL.value());
		paramDto.setNeedInvoice(0);
//		只获取下单时候，商家是开启发票功能的订单
		paramDto.setSwitchInvoice(1);

		PageSupport<MySubDto> ps = orderService.getMyorderDtos(paramDto);
		PageSupportHelper.savePage(request, ps);

		request.setAttribute("startDate", fromDate);
		request.setAttribute("endDate", endDate);
		request.setAttribute("paramDto", paramDto);
		request.setAttribute("curPageNO", curPageNO);
		request.setAttribute("state_type", state_type);
		request.setAttribute("order_sn", order_sn);
		request.setAttribute("sub_type", sub_type);
		//获取默认的发票
		if(AppUtils.isNotBlank(shopId)){
			ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
			request.setAttribute("switchInvoice", shopDetail.getSwitchInvoice());
			
			if(AppUtils.isNotBlank(shopDetail.getSwitchInvoice()) && shopDetail.getSwitchInvoice().equals(1)) {
		    	//查出买家的默认发票内容
		    	Invoice userdefaultInvoice = invoiceService.getDefaultInvoice(userId);
		    	request.setAttribute("userdefaultInvoice", userdefaultInvoice);
		    }
		}
		String result = PathResolver.getPath(UserCenterFrontPage.MYINVOICE);
		return result;
	}

}
