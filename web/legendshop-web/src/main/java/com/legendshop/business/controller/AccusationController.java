/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.model.entity.*;
import com.legendshop.sms.event.processor.SendSiteMessageProcessor;
import com.legendshop.spi.service.ShopDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.legendshop.base.exception.NotFoundException;
import com.legendshop.business.page.UserCenterFrontPage;
import com.legendshop.business.page.UserCenterRedirectPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.CommonPage;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.ImageTypeEnum;
import com.legendshop.model.dto.AccusationDto;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.AccusationService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 投诉
 */
@Controller
public class AccusationController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(AccusationController.class);

	@Autowired
	private AccusationService accusationService;

	@Autowired
	private ProductService productService;

    @Autowired
    private ShopDetailService shopDetailService;

	@Autowired
    private SendSiteMessageProcessor sendSiteMessageProcessor;

	@Autowired
	private AttachmentManager attachmentManager;

	/**
	 * 投诉管理
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/p/accusation")
	public String accusation(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		PageSupport<AccusationDto> ps = accusationService.query(curPageNO, userId);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(UserCenterFrontPage.ACCUSATION_LIST); // simple
	}

	@RequestMapping(value = "/p/accusation/loadOverlay/{id}")
	public String OverlayLoad(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		PageSupport<Accusation> ps = accusationService.query(id, userId);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(UserCenterFrontPage.ACCUSATION_OVERLAY);
	}

	/**
	 * 商品页面发起投诉商品， 在usercenter框架中触发accusation动作
	 * 
	 * @param request
	 * @param response
	 * @param prodId
	 * @return
	 */
	@RequestMapping(value = "/p/accusation/toAccusation/{prodId}")
	public String toAccusation(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {
		return PathResolver.getPath(UserCenterRedirectPage.USER_CENTER_QUERY) + "?tab=toAccusation&code=" + prodId;
	}

	/**
	 * 保存投诉 TODO to be deleted?
	 * 
	 * @param request
	 * @param response
	 * @param prodId
	 * @return
	 */
	@RequestMapping(value = "/p/accusation/toSaveAccusation/{prodId}")
	public String toSaveAccusation(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {
		Product product = productService.getProductById(prodId);
		List<AccusationSubject> titleList = accusationService.queryAccusationSubject();
		List<AccusationType> typeList = accusationService.queryAccusationType();
		request.setAttribute("titleList", titleList);
		request.setAttribute("typeList", typeList);
		request.setAttribute("product", product);
		return PathResolver.getPath(UserCenterFrontPage.ACCUSATION);
	}

	/**
	 * 查看投诉内容
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/p/accusation/load/{id}")
	public String accusationLoad(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Accusation accusation = accusationService.getAccusation(id);
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		if (accusation == null || !userId.equals(accusation.getUserId())) {// 只能查看自己的投诉内容
			throw new NotFoundException("Not accusation id " + id);
		}

		request.setAttribute("accusation", accusation);
		return PathResolver.getPath(UserCenterFrontPage.ACCUSATION);
	}

	/**
	 * 删除投诉内容
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/p/accusation/del/{id}")
	public String accusationDel(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Accusation accusation = accusationService.getAccusation(id);
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		if (accusation == null || !userId.equals(accusation.getUserId())) {// 只能删除自己的投诉内容
			throw new NotFoundException("Not accusation id " + id);
		}
		accusation.setUserDelStatus(1);
		accusationService.updateAccusation(accusation);
		return PathResolver.getPath(UserCenterRedirectPage.ACCUSATION);
	}

	/**
	 * 批量删除投诉内容
	 * 
	 * @param request
	 * @param response
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "/p/accusation/delList/{ids}")
	public String accusationDelList(HttpServletRequest request, HttpServletResponse response, @PathVariable String ids) {
		List<Long> idList = JSONUtil.getArray(ids, Long.class);
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		for (Long id : idList) {
			Accusation accusation = accusationService.getAccusation(id);
			if (accusation == null || !userId.equals(accusation.getUserId())) {// 只能删除自己的投诉内容
				throw new NotFoundException("Not accusation id " + id);
			}
			accusationService.deleteAccusation(accusation);
		}
		return PathResolver.getPath(UserCenterRedirectPage.ACCUSATION);
	}

	/**
	 * 清空投诉内容
	 * 
	 * @param request
	 * @param response
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "/p/accusation/delAll")
	public String accusationDelAll(HttpServletRequest request, HttpServletResponse response) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		List<Accusation> accs = accusationService.getAccusation(userName);
		accusationService.deleteAccusation(accs);
		return PathResolver.getPath(UserCenterRedirectPage.ACCUSATION);
	}

	/**
	 * 到举报编辑页面
	 * 
	 * @param request
	 * @param response
	 * @param prodId
	 * @return
	 */
	@RequestMapping(value = "/p/accusation/{prodId}")
	public String accusation(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {
		// Load product by id
		ProductDetail product = productService.getProdDetail(prodId);
		if (product == null) { // go to error page
			SecurityUserDetail user = UserManager.getUser(request);
			String userName = user.getUsername();
			log.warn("userName '{}' try to  find a not exist product by ID {}", userName, prodId);
			String path = PathResolver.getPath(CommonPage.ERROR_PAGE);
			return path;
		} else {
			request.setAttribute("product", product);
			return PathResolver.getPath(UserCenterFrontPage.ACCUSATION);
		}
	}

	// 查找出所有举报主题
	@RequestMapping("/p/AccusationType")
	public @ResponseBody List<KeyValueEntity> AccusationTypeList(HttpServletRequest request, HttpServletResponse response) {
		return accusationService.AccusationTypeList();
	}

	// 查找出所有举报主题
	@RequestMapping("/p/AccusationType1")
	public @ResponseBody String AccusationTypeList1(HttpServletRequest request, HttpServletResponse response) {
		return "Success中文";
	}

	// 根据subjectId 查找类型
	@RequestMapping("/p/AccusationSubject/{typeId}")
	public @ResponseBody List<KeyValueEntity> AccusationSubject(HttpServletRequest request, HttpServletResponse response, @PathVariable Long typeId) {
		return accusationService.getAccusationSubject(typeId);
	}

	@RequestMapping(value = "/p/accusation/save")
	public String accusationSave(MultipartHttpServletRequest request, HttpServletResponse response, Accusation accusation) {
		String accusationPic1 = null, accusationPic2 = null, accusationPic3 = null;
		SecurityUserDetail user = UserManager.getUser(request);


		if (accusation.getPic1File().getSize() > 0) {
			accusationPic1 = attachmentManager.upload(accusation.getPic1File());
			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), accusationPic1, accusation.getPic1File(), AttachmentTypeEnum.USER);
			accusation.setPic1(accusationPic1);
		}
		if (accusation.getPic2File().getSize() > 0) {
			accusationPic2 = attachmentManager.upload(accusation.getPic2File());
			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), accusationPic2, accusation.getPic2File(), AttachmentTypeEnum.USER);
			accusation.setPic2(accusationPic2);
		}
		if (accusation.getPic3File().getSize() > 0) {
			accusationPic3 = attachmentManager.upload(accusation.getPic3File());
			attachmentManager.saveImageAttachment(user.getUsername(), user.getUserId(), user.getShopId(), accusationPic3, accusation.getPic3File(), AttachmentTypeEnum.USER);
			accusation.setPic3(accusationPic3);
		}
		accusation.setUserId(user.getUserId());
		accusation.setUserName(user.getUsername());
		accusation.setStatus((long) 0);
		accusation.setRecDate(new Date());
		accusation.setUserDelStatus(0);
		Accusation acc = accusationService.getAccusationByUserAndProd(user.getUserId(), accusation.getProdId());
		if (AppUtils.isNotBlank(acc)) {
			// 重复举报，跳转到 举报列表页，并携带 参数 err=reAcc
			return PathResolver.getPath(UserCenterRedirectPage.ACCUSATION) + "?err=reAcc";
		} else {
            accusationService.saveAccusation(accusation);
			// 保存成功，跳转到 举报列表页
            Product product = productService.getProductById(accusation.getProdId());
            //举报成功之后发送消息给商户 需要商户id
            ShopDetail shopDetail = shopDetailService.getShopDetailById(accusation.getShopId());
            if(AppUtils.isNotBlank(product)&&AppUtils.isNotBlank(product.getShopId())&&AppUtils.isNotBlank(product.getUserName())){
                sendSiteMessageProcessor.process(new SendSiteMsgEvent(product.getUserName(),"举报提醒","您的商品名称为【"+product.getName()+"】被举报了,请注意！").getSource());
            }
 			return PathResolver.getPath(UserCenterRedirectPage.ACCUSATION);
		}
	}

}
