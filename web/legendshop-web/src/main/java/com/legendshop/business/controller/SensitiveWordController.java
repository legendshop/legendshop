/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.base.BaseController;
import com.legendshop.spi.service.SensitiveWordService;

/**
 * 敏感字控制器
 *
 */
@Controller
public class SensitiveWordController extends BaseController {
	
	@Autowired
	private SensitiveWordService sensitiveWordService;

	/**
	 * 用来进行过滤的控制器
	 * @param request
	 * @param response
	 * @param src
	 * @return
	 */
	@RequestMapping(value = "/sensitiveWord/filter")
	public @ResponseBody Set<String> filter(HttpServletRequest request, HttpServletResponse response, String src) {
		Set<String> findwords = sensitiveWordService.checkSensitiveWords(src);
		return findwords;
	}

}
