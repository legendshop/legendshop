/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.util.ValidationUtil;
import com.legendshop.business.manager.impl.ThirdUserAuthorizeManagerFactory;
import com.legendshop.business.page.FrontPage;
import com.legendshop.business.page.RedirectPage;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PassportSourceEnum;
import com.legendshop.model.dto.PassportFull;
import com.legendshop.model.dto.ThirdUserAuthorizeResult;
import com.legendshop.model.entity.User;
import com.legendshop.model.form.UserMobileForm;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.security.service.LoginService;
import com.legendshop.spi.manager.ThirdUserAuthorizeManager;
import com.legendshop.spi.service.ThirdLoginService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.web.helper.IPHelper;

/**
 * 第三方网站应用oauth2认证, 
 */
@Controller
@RequestMapping("/thirdlogin")
public class WebOauthController {
	
    @Autowired
    private LoginService loginService;
    
    @Autowired
    private ThirdLoginService thirdLoginService;
    
    @Autowired
    private UserDetailService userDetailService;

	/**
	 * 前往认证授权
	 * @param request
	 * @param response
	 * @param type 类型
	 * @param source 来源
	 * @return
	 */
	@GetMapping("/{type}/{source}/authorize")
	public String authorize(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable String type, @PathVariable String source) {
		
		String authorizeURL = "";
		try {
			ThirdUserAuthorizeManager thirdUserAuthorizeManager = ThirdUserAuthorizeManagerFactory.getIntance(type);
			authorizeURL = thirdUserAuthorizeManager.constructAuthorizeUrl(source);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return PathResolver.getRedirectPath(authorizeURL);
	}
	
	/**
	 * PC网页授权成功回调
	 * @param request
	 * @param response
	 * @param type 类型
	 * @param source 来源
	 * @return
	 */
	@GetMapping("/{type}/pc/callback")
	public String pcAuthorizeCallback(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable String type, String code, String state) {

		try {
			ThirdUserAuthorizeManager thirdUserAuthorizeManager = ThirdUserAuthorizeManagerFactory.getIntance(type);
			
			ThirdUserAuthorizeResult result = thirdUserAuthorizeManager.authorizeCallback(code, state, PassportSourceEnum.PC.value());
			if(result.isSuccess()){// 认证通过, 拿到用户
				
				PassportFull passport = result.getPassport();

				SecurityUserDetail userDetail = UserManager.getUser(request);
	            if (userDetail != null && result.getPassport().getOpenId().equals(userDetail.getOpenId())) {
	            	return  PathResolver.getPath(RedirectPage.HOME_QUERY);
	            }
				
				loginService.onAuthentication(request, response, result.getUser().getName(), passport.getOpenId(), passport.getAccessToken(), passport.getType(), PassportSourceEnum.PC.value());
                
                return  PathResolver.getPath(RedirectPage.HOME_QUERY);
			}
			
			if(result.isNeedBindAccount()){// 认证通过, 需要绑定账号
				return  PathResolver.getRedirectPath("/thirdlogin/bind/" + result.getPassportIdKey());
			}
			
			// 其他错误
			request.setAttribute(Constants.MESSAGE,"登录失败, 未知错误!");
			return  PathResolver.getPath(FrontPage.OPERATION_ERROR);
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("message","登录失败, 未知错误!");
			return  PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}
	}
	
	/**
	 * 跳转绑定账号页面
	 * @param type 类型
	 * @return
	 */
	@GetMapping("/bind/{passportIdKey}")
	public String toBindAccount(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable String passportIdKey) {
		
		// 如果用户有传 passportKey
		PassportFull passport = thirdLoginService.checkPassportIdKey(passportIdKey);
		if(null == passport){
			request.setAttribute(Constants.MESSAGE, "对不起, 您的授权凭证无效, 请重新授权!");
			return  PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}
		
		// 说明通行证已经绑定用户了
		if(AppUtils.isNotBlank(passport.getUserId())){
			request.setAttribute(Constants.MESSAGE, "对不起, 这个微信已被绑定了!");
			return  PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}
		
		request.setAttribute("passportIdKey", passportIdKey);
		request.setAttribute("passport", passport);
		
		return  PathResolver.getPath(FrontPage.BIND_ACCOUNT);
	}
	
	/**
	 * 绑定账号
	 * @param passportIdKey 
	 * @param phone
	 * @param code
	 * @return
	 */
	@PostMapping("/bind")
	@ResponseBody
	public String bindAccount(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam(required = true) String passportIdKey, 
			@RequestParam(required = true) String phone, 
			@RequestParam(required = true) String code) {
		
		// 如果用户有传 passportKey
		PassportFull passport = thirdLoginService.checkPassportIdKey(passportIdKey);
		if(null == passport){
			return "对不起, 您的授权凭证无效, 请重新授权!";
		}
		
		// 说明通行证已经绑定用户了
		if(AppUtils.isNotBlank(passport.getUserId())){
			return "对不起, 这个微信已被绑定了!";
		}
		
		// 检查手机号码的格式
		boolean isMobile=ValidationUtil.checkPhone(phone);
		if(!isMobile){
			return "请输入正确的手机号码";
		}

		// 检查验证码是否正确
		boolean mobileResult = userDetailService.verifyMobileCode(phone, code);
		if(!mobileResult){
			return "验证码不正确";
		}
		
		User user =userDetailService.getUserByMobile(phone);
		if(null == user){// 短信注册,如果用户不存在则直接新增一个用户
			UserMobileForm userForm = new UserMobileForm();
			userForm.setMobile(phone);
			String nickName = "手机尾号" + phone.substring(7) + "用户";
			userForm.setNickName(nickName);
			userForm.setActivated(true);
			user = userDetailService.saveUserReg(IPHelper.getIpAddr(request), userForm, "");
			
			if(null == user){
				return "绑定失败, 请联系平台客服!";
			}
			userDetailService.updatePaypassVerifn(user.getUserName());
		}
		
		if("0".equals(user.getEnabled())){
			return "用户已被冻结,请联系平台客服!";
		}

		String bindResult  = thirdLoginService.bindAccount(user, passport);
		if(!Constants.SUCCESS.equals(bindResult)){
			return "绑定失败, 请联系平台客服!";
		}
		
		// 登录
		loginService.onAuthentication(request, response, user.getName(), passport.getOpenId(), passport.getAccessToken(), passport.getType(), PassportSourceEnum.PC.value());
		
		// 清除缓存中的PassportID
		if(AppUtils.isNotBlank(passportIdKey)){
			thirdLoginService.removePassportIDFromCache(passportIdKey);
		}
		
		return Constants.SUCCESS;
	}
}
