/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.controller;

import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.constant.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSMSEvent;
import com.legendshop.base.event.ShortMessageInfo;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.base.model.UserMessages;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.base.util.ValidationUtil;
import com.legendshop.business.page.FrontPage;
import com.legendshop.business.page.RedirectPage;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.ErrorForm;
import com.legendshop.model.ValidationMessage;
import com.legendshop.model.dto.UserResultDto;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.SystemConfig;
import com.legendshop.model.entity.User;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.UserSecurity;
import com.legendshop.model.form.UserForm;
import com.legendshop.model.form.UserMobileForm;
import com.legendshop.model.securityCenter.UserInformation;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.security.service.LoginService;
import com.legendshop.spi.manager.MailManager;
import com.legendshop.spi.service.ErrorHandleSerivce;
import com.legendshop.spi.service.SMSLogService;
import com.legendshop.spi.service.SensitiveWordService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.SystemConfigService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.RandomStringUtils;
import com.legendshop.web.helper.IPHelper;
import com.legendshop.web.util.ValidateCodeUtil;

/**
 * 用户控制器。
 */
@Controller
public class UserController extends BaseController {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private MailManager mailManager;

	@Autowired
	private LoginService loginService;

	@Autowired
	private ErrorHandleSerivce errorHandleSerivce;

	@Autowired
	private SMSLogService smsLogService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private SystemConfigService systemConfigService;

	@Autowired
	private SensitiveWordService sensitiveWordService;

	/**
	 * 短信发送者
	 */
	@Resource(name = "sendSMSProcessor")
	private EventProcessor<ShortMessageInfo> sendSMSProcessor;

	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;

    @Autowired
    private PasswordEncoder passwordEncoder;


	/**
	 * 弹出层用户登录
	 */
	@RequestMapping("/userlogin")
	public @ResponseBody String userLogin(HttpServletRequest request, HttpServletResponse response, String userName, String pwd, String randNum) {

		// 检查图片验证码是否正确
		if (!ValidateCodeUtil.validateCode(randNum, request.getSession())) {
			return Constants.FAIL;
		}

		// 根据userName 查找相应密码
		Authentication authentication = null;
		try {
			authentication = loginService.onAuthentication(request, response, userName, pwd);
			if (AppUtils.isBlank(authentication)) {
				return Constants.FAIL;
			} else {
				return Constants.SUCCESS;
			}
		} catch (Exception e) {
			return Constants.FAIL;
		}
	}

	/**
	 * 弹出层快速注册
	 */
	@RequestMapping("/quickregister")
	public @ResponseBody String quickregister(HttpServletRequest request, HttpServletResponse response, UserMobileForm userMobileForm) {
//		// 检查短信验证码是否正确
//		if (!smsLogService.verifyCodeAndClear(userMobileForm.getMobile(), userMobileForm.getMobileCode())) {
//			log.error("Incorrect mobile code");
//			return Constants.FAIL;
//		}
//
//		// 检查图片验证码是否正确
//		if (!ValidateCodeUtil.validateCode(userMobileForm.getRandNum(), request.getSession())) {
//			log.error("Incorrect validate code");
//			return Constants.FAIL;
//		}

		boolean existed = userDetailService.isPhoneExist(userMobileForm.getMobile());
		if (existed) {
			// 检查是否重名
			UserMessages messages = new UserMessages();
			messages.addCallBackList(ResourceBundleHelper.getString("errors.phone"));
			request.setAttribute(UserMessages.MESSAGE_KEY, messages);
			request.setAttribute("userForm", userMobileForm);
			return Constants.FAIL;
		}

		User user = userDetailService.saveUserReg(IPHelper.getIpAddr(request), userMobileForm,passwordEncoder.encode(userMobileForm.getPassword()));

		request.setAttribute("userName", userMobileForm.getNickName());

		if (user == null) {
			throw new BusinessException("register user error");
		}

		// 手机注册的用户，用户安全等级+1,手机验证变为1
		userDetailService.updatePhoneVerifn(user.getName());

		if (Constants.USER_STATUS_ENABLE.equals(user.getEnabled())) {
			// 用户注册即登录
			loginService.onAuthentication(request, response, userMobileForm.getMobile(), userMobileForm.getPassword());
		}

		return Constants.SUCCESS;
	}

	/**
	 * Login.
	 *
	 * @param request  the request
	 * @param response the response
	 * @return the string
	 */
	@RequestMapping("/login")
	public String login(HttpServletRequest request, HttpServletResponse response) {
		// 关开发添加,用于动态显示登录方式
		/*
		 * System.out.println(PropertiesUtil.getObject(SysParameterEnum.QQ_LOGIN_ENABLE,
		 * Boolean.class)); request.setAttribute("qqLoginEnable",
		 * PropertiesUtil.getObject(SysParameterEnum.QQ_LOGIN_ENABLE, Boolean.class));
		 * request.setAttribute("weiboLoginEnable",
		 * PropertiesUtil.getObject(SysParameterEnum.WEIBO_LOGIN_ENABLE,
		 * Boolean.class)); request.setAttribute("weixinLoginEnable",
		 * PropertiesUtil.getObject(SysParameterEnum.WEIXIN_LOGIN_ENABLE,
		 * Boolean.class));
		 */
		return PathResolver.getPath(FrontPage.LOGIN);
	}

	/**
	 * Login.
	 *
	 * @param request  the request
	 * @param response the response
	 * @return the string
	 */
	@RequestMapping("/storeLogin")
	public String storeLogin(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(FrontPage.SHOPLOGIN);
	}

    /**
     * 手机登录获取验证码
     *
     * @param request
     * @param response
     * @param tel
     * @time 2019-01-5
     */
   /* @RequestMapping(value = "/sendSMSCodeByLogin", method = RequestMethod.POST)
    public @ResponseBody
    String sendSMSCodeByLogin(HttpServletRequest request, HttpServletResponse response, String tel, String randNum2) {
        String mobileCode = CommonServiceUtil.getRandomSMSCode();
        String userName = UserManager.getUserName(request);
        //先查询数据库是否存在用户，直接发送登陆验证短信
        boolean flag = userDetailService.isPhoneExist(tel)
                || ValidateCodeUtil.validateCodeAndKeep(randNum2, request.getSession());
        if (flag) {
            SysMsgTemplate sysMsgTemplate = sysMsgTemplateService.getSysMsgTemplate(SmsTemplateTypeEnum.VAL.value(), MsgTemplateTypeEnum.SMS.value());
            sendSMSProcessor.sendValSMS(tel, mobileCode, userName, sysMsgTemplate.getContent(), sysMsgTemplate.getTitle(), sysMsgTemplate.getSmsTemplateCode());
            return Constants.SUCCESS;
        }
        return Constants.FAIL;
    }*/


    /**
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/employeeLogin")
	public String employeeLogin(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(FrontPage.EMPLOYEE_LOGIN);
	}

	/**
	 * 找回密码
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/retrievepassword")
	public String retrievepassword(HttpServletRequest request, HttpServletResponse response) {
		String result = PathResolver.getPath(FrontPage.RETRIEVE_PD);
		return result;
	}

	/**
	 * 去往错误界面
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/failPage/{errorCode}")
	public String failPageWithErrorCoe(HttpServletRequest request, HttpServletResponse response, @PathVariable String errorCode) {
		ErrorForm errorForm = errorHandleSerivce.getErrorForm(errorCode);
		request.setAttribute("errorForm", errorForm);
		return PathResolver.getPath(FrontPage.FAIL_PAGE);
	}

	@RequestMapping("/failPage")
	public String failPage(HttpServletRequest request, HttpServletResponse response) {
		ErrorForm errorForm = errorHandleSerivce.getErrorForm("defalut");
		request.setAttribute("errorForm", errorForm);
		return PathResolver.getPath(FrontPage.FAIL_PAGE);
	}

	/**
	 * 去往验证身份页面
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/proveStatus")
	public String proveStatus(HttpServletRequest request, HttpServletResponse response, String name) {
		UserInformation userInfo = null;
		if (checkPhone(name)) {
			userInfo = userDetailService.getUserInfoByPhone(name);
		} else if (checkEmail(name)) {
			userInfo = userDetailService.getUserInfoByMail(name);
		} else {
			userInfo = userDetailService.getUserInfo(name); // user name
		}

		if (AppUtils.isNotBlank(userInfo)) {
			// 处理手机号码
			if (AppUtils.isNotBlank(userInfo.getUserMobile())) {
				String mobile = mobileSubstitution(userInfo.getUserMobile());
				request.setAttribute("mobileScreen", mobile);
			}

			// 处理邮箱号码
			if (AppUtils.isNotBlank(userInfo.getUserMail())) {
				String mail = mailSubstitution(userInfo.getUserMail());
				userInfo.setUserMail(mail);
			}

			if (userInfo.getPhoneVerifn() == 0 && userInfo.getMailVerifn() == 0) {
				return PathResolver.getPath(FrontPage.INTERCEPT);
			}

			request.setAttribute("userInfo", userInfo);
			return PathResolver.getPath(FrontPage.PROVE_STATUS);

		} else {
			return PathResolver.getPath(FrontPage.INTERCEPT);
		}
	}

	@RequestMapping(value = "/sendSMSCode", method = RequestMethod.POST)
	public @ResponseBody
	String sendSMSCode(HttpServletRequest request, HttpServletResponse response, String userName, String mobile, String randNum) {

		// 检查图片验证码是否正确
		if (!ValidateCodeUtil.validateCodeAndKeep(randNum, request.getSession())) {
			log.error("Incorrect validate code");
			return Constants.FAIL;
		}
		String ip = "";
		String requestUrl = "";
		try {
			requestUrl = request.getRequestURL().toString();
			ip = IPHelper.getIpAddr(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("UserController sendSMSCode 发送短信验证码 mobile：{}，ip：{}", mobile, ip);
		String mobileCode = CommonServiceUtil.getRandomSMSCode();
		sendSMSProcessor.process(new SendSMSEvent(mobile, mobileCode, userName, StringUtils.isNotBlank(requestUrl) ? requestUrl : "/sendSMSCode", ip, new Date()).getSource());
		// 将验证码传入db
		userDetailService.updateUserSecurity(userName, mobileCode);

		return Constants.SUCCESS;
	}

	/**
	 * 手机注册获取验证码
	 *
	 */
	@RequestMapping(value = "/sendSMSCodeByReg", method = RequestMethod.POST)
	public @ResponseBody String sendSMSCodeByReg(HttpServletRequest request, HttpServletResponse response, String tel, String randNum) {
		// 检查图片验证码是否正确
		if (!ValidateCodeUtil.validateCodeAndKeep(randNum, request.getSession())) {
			log.error("Incorrect validate code");
			return Constants.FAIL;
		}

		String mobileCode = CommonServiceUtil.getRandomSMSCode();

		SecurityUserDetail user = UserManager.getUser(request);

		String userName = "";
		if(user != null) {
			userName = user.getUsername();
		}
		String ip = "";
		String requestUrl = "";
		try {
			requestUrl = request.getRequestURL().toString();
			ip = IPHelper.getIpAddr(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("UserController sendSMSCodeByReg 发送短信验证码 mobile：{}，ip：{}", tel, ip);
		sendSMSProcessor.process(new SendSMSEvent(tel, mobileCode, userName,StringUtils.isNotBlank(requestUrl)?requestUrl:"/sendSMSCodeByReg",ip,new Date()).getSource());
		return Constants.SUCCESS;
	}

	/**
	 * 第三方手机注册获取验证码
	 */
	@RequestMapping(value = "/sendSMSCodeByThirdReg", method = RequestMethod.POST)
	public @ResponseBody
	String sendSMSCodeByThirdReg(HttpServletRequest request, HttpServletResponse response, String tel, String randNum) {
		// 检查图片验证码是否正确
		//if (!ValidateCodeUtil.validateCodeAndKeep(randNum, request.getSession())) {
		//	log.error("Incorrect validate code");
		//	return Constants.FAIL;
		//}
		String mobileCode = CommonServiceUtil.getRandomSMSCode();
		SecurityUserDetail user = UserManager.getUser(request);

		String userName = "";
		if (user != null) {
			userName = user.getUsername();
		}
		String ip = "";
		String requestUrl = "";
		try {
			requestUrl = request.getRequestURL().toString();
			ip = IPHelper.getIpAddr(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("UserController sendSMSCodeByReg 发送短信验证码 mobile：{}，ip：{}", tel, ip);
		sendSMSProcessor.process(new SendSMSEvent(tel, mobileCode, userName, StringUtils.isNotBlank(requestUrl) ? requestUrl : "/sendSMSCodeByThirdReg", ip, new Date()).getSource());
		return Constants.SUCCESS;
	}

	/**
	 * 发送邮件
	 *
	 * @param request
	 * @param response
	 * @param userName
	 * @return
	 */
	@RequestMapping(value = "/confirmationMail", method = RequestMethod.POST)
	public String confirmationMail(HttpServletRequest request, HttpServletResponse response, String userName) {
		User user = userDetailService.getUserByName(userName);
		String userEmail = userDetailService.getUserEmail(user.getId());
		String code = getRandomString(50);
		mailManager.verifyEmail(user, userEmail, code);

		// 将邮箱及验证码 更新到 ls_usr_security
		userDetailService.updateSecurityMail(userName, code, userEmail);
		return PathResolver.getPath(FrontPage.SEND_EMAIL_PAGE);
	}

	/**
	 * 找回密码 对比验证码是否一致
	 *
	 * @param request
	 * @param response
	 * @param code
	 * @return
	 */
	@RequestMapping(value = "/verifySMSCode", method = RequestMethod.POST)
	public @ResponseBody Boolean verifySMSCode(HttpServletRequest request, HttpServletResponse response, String userName, String code) {

		Boolean test = userDetailService.verifySMSCode(userName, code);
		if (test) {
			UserDetail userDetail = userDetailService.getUserDetail(userName);
			// 判断用户和手机是否为空
			if (AppUtils.isNotBlank(userDetail) && AppUtils.isNotBlank(userDetail.getUserMobile())) {
				// 详细校验安全码
				test = smsLogService.verifyMobileCode(code, userDetail.getUserMobile());
			} else
				test = false;

		}
		return test;
	}

	/**
	 * 去往修改密码页面
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/changePwd", method = RequestMethod.POST)
	public String changePwd(HttpServletRequest request, HttpServletResponse response, String userName, String code) {
		request.setAttribute("userName", userName);
		request.setAttribute("code", code);
		return PathResolver.getPath(FrontPage.CHANGE_PWD);
	}

	/**
	 * 邮件链接回来后，直接去密码修改页面
	 *
	 * @param request
	 * @param response
	 * @param userName
	 * @param code
	 * @return
	 */
	@RequestMapping(value = "/setNewPwd/{userName}/{code}")
	public String setNewPwd(HttpServletRequest request, HttpServletResponse response, @PathVariable String userName, @PathVariable String code) {
		// 验证链接的是否为24小时内，次数为0
		UserSecurity security = userDetailService.getUserSecurity(userName);

		if (AppUtils.isNotBlank(security)) {
			Date end = new Date();
			long day = (end.getTime() - security.getSendMailTime().getTime()) / (24 * 60 * 60 * 1000);
			// 判断是否为24小时内以及是第一次点击
			if (day >= 1 || security.getTimes() >= 1) {
				return PathResolver.getPath(FrontPage.FAIL_PAGE);
			}

			request.setAttribute("userName", userName);
			request.setAttribute("code", code);
			return PathResolver.getPath(FrontPage.SETNEWPWD);
		} else {
			return PathResolver.getPath(FrontPage.FAIL_PAGE);
		}
	}

	/**
	 * 成功页面
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/pwdResetSuccess", method = RequestMethod.POST)
	public String success(HttpServletRequest request, HttpServletResponse response, String newPassword, String userName, String code) {
		if (AppUtils.isBlank(code) || AppUtils.isBlank(userName)) {
			return PathResolver.getPath(FrontPage.PWD_RESET_FAIL);
		}
		// 验证邮箱验证码
		boolean isMailCodeExists = userDetailService.isMailCodeExist(userName, code);
		if (isMailCodeExists) {
			Boolean result = userDetailService.updateNewPassword(userName, passwordEncoder.encode(newPassword));
			if (!result) {
				return PathResolver.getPath(FrontPage.PWD_RESET_FAIL);
			} else {
				// 清除邮箱验证码
				userDetailService.clearMailCode(userName);
				return PathResolver.getPath(FrontPage.PWD_RESET_SUCCESS);
			}
		} else if (checkSecurityCode(userName, code)) {// 验证短信验证码
			Boolean result = userDetailService.updateNewPassword(userName, passwordEncoder.encode(newPassword));
			if (!result) {
				return PathResolver.getPath(FrontPage.PWD_RESET_FAIL);
			} else {
				// 清除短信验证码
				userDetailService.clearValidateCode(userName);
				return PathResolver.getPath(FrontPage.PWD_RESET_SUCCESS);
			}
		} else {
			return PathResolver.getPath(FrontPage.PWD_RESET_FAIL);
		}
	}
    /**
     * @param request
     * @param response
     * @param j_username_mobile
     * @param j_password_mobile
     * @return
     * @Description 验证短信验证码
     */
    @RequestMapping(value = "/checkSMSCode")
    @ResponseBody
    public String checkSMSCode(HttpServletRequest request, HttpServletResponse response, String j_username_mobile, String j_password_mobile) {
        // 判断验证码是否正确
        String mobile = j_username_mobile;

        if (smsLogService.verifyCodeAndClear(mobile, j_password_mobile, SmsTemplateTypeEnum.VAL)) {
            //放到session中用来给后面
            // Attribute("j_password_mobile", j_password_mobile);
            return Constants.SUCCESS;
        }

        return "短信验证码错误";
    }

	/**
	 * 检查手机验证码和数据库中的是否一致
	 *
	 * @param userName
	 * @param securityCode
	 * @return
	 */
	private boolean checkSecurityCode(String userName, String securityCode) {
		if (AppUtils.isBlank(securityCode)) {
			return false;
		}
		UserSecurity userSecurity = userDetailService.getUserSecurity(userName);
		if (userSecurity == null || userSecurity.getValidateCode() == null) {
			return false;
		}
		return userSecurity.getValidateCode().equals(securityCode);
	}

	/**
	 * 用户注册动作 在旧模版中使用
	 *
	 * @param request  the request
	 * @param response the response
	 * @param userForm the user form
	 * @return the string
	 */
	@RequestMapping("/userReg")
	public String doUserReg(HttpServletRequest request, HttpServletResponse response, UserForm userForm) {
		// 检查图片验证码是否正确
		if (!ValidateCodeUtil.validateCode(userForm.getRandNum(), request.getSession())) {
			log.error("Incorrect validate code");
			return PathResolver.getPath(RedirectPage.REG);
		}

		ValidationMessage message = userForm.validate();
		if (message.isFailed()) {
			log.error("register failed: " + message);
			return PathResolver.getPath(RedirectPage.REG);
		}

		if (AppUtils.isBlank(userForm.getUserName()) || !ValidationUtil.checkUserName(userForm.getUserName())) {
			log.error("register failed: 用户名不能为空或非法");
			return PathResolver.getPath(RedirectPage.REG);
		}

		String ip = IPHelper.getIpAddr(request);
		UserResultDto userResultDto = userDetailService.saveUserReg(ip, userForm, passwordEncoder.encode(userForm.getPassword()));
		if (!userResultDto.success()) {
			throw new BusinessException("register user error");
		}
		if (Constants.USER_STATUS_ENABLE.equals(userResultDto.getUser().getEnabled())) {
			// 用户注册即登录
			loginService.onAuthentication(request, response, userForm.getNickName(), userForm.getPassword());
		}

		return PathResolver.getPath(RedirectPage.AFTER_OPERATION);
	}

	/**
	 * 加载开店页面,该动作需要用户登录
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */

	@RequestMapping("/openShop")
	public String openShop(HttpServletRequest request, HttpServletResponse response) throws Exception {

		SecurityUserDetail user = UserManager.getUser(request);
		if(user == null) {
			return PathResolver.getPath(FrontPage.LOGIN);
		}

		// 用户必须登录，不再用spring security来限制，因为买家和商家一起访问该页面
		if (!UserManager.hasFunction(user, FunctionEnum.FUNCTION_USER.value())) {
			return PathResolver.getPath(FrontPage.LOGIN);
		}

		// 商家的所有者
		String shopUserId = UserManager.getShopUserId(user);
		ShopDetail shopDetail = null;
		if (shopUserId != null) {
			shopDetail = shopDetailService.getShopDetailByUserId(shopUserId);
		}
		if (shopDetail == null) {// 开店页面
			return PathResolver.getPath(FrontPage.OPEN_SHOP);
		} else {
			if (ShopStatusEnum.NORMAL.value().equals(shopDetail.getStatus())) { // 审核成功后提醒
				if (UserManager.isShopUser(user)) {
					return PathResolver.getPath(RedirectPage.SHOP_SETTING);
				} else {
					return PathResolver.getPath(FrontPage.AFTER_OPENSHOP);
				}

			} else {// 审核中
				request.setAttribute("shopDetail1", shopDetail);
				return PathResolver.getPath(FrontPage.OPEN_SHOP);
			}
		}

	}

	/**
	 * 用户在登录后的开店流程
	 */
	@RequestMapping("/p/doOpenShop")
	public String doOpenShop(HttpServletRequest request, HttpServletResponse response, ShopDetail shopDetail) {
		// 检查图片验证码是否正确
		if (!ValidateCodeUtil.validateCode(shopDetail.getRandNum(), request.getSession())) {
			log.error("Incorrect validate code");
			return PathResolver.getPath(RedirectPage.OPEN_SHOP);
		}
		if (AppUtils.isBlank(shopDetail.getContactName())) {
			log.error("ContactName is null");
			return PathResolver.getPath(RedirectPage.OPEN_SHOP);
		}
		if (AppUtils.isBlank(shopDetail.getContactMobile())) {
			log.error("ContactMobile is null");
			return PathResolver.getPath(RedirectPage.OPEN_SHOP);
		}

		if (AppUtils.isBlank(shopDetail.getSiteName())) {
			log.error("SiteName is null");
			return PathResolver.getPath(RedirectPage.OPEN_SHOP);
		}
		if (AppUtils.isBlank(shopDetail.getIdCardNum())) {
			log.error("IdCardNum is null");
			return PathResolver.getPath(RedirectPage.OPEN_SHOP);
		}

		shopDetail.setIp(IPHelper.getIpAddr(request));

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();

		boolean result = false;
		if (AppUtils.isNotBlank(shopDetail.getShopId())) {
			result = shopDetailService.reApplyOpenShop(shopDetail, userId, userName);
		} else {
			result = userDetailService.saveShopReg(userName, shopDetail);
		}
		if (!result) {
			throw new BusinessException("register shop error");
		}
		// 返回申请开店成功页面
		return PathResolver.getPath(RedirectPage.OPENSHOP_SUCCES);
	}

	@RequestMapping("/p/doOpenShopSuc")
	public String doOpenShopSuc(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(FrontPage.OPEN_SUCCESS);
	}

	/**
	 * 用户通过手机注册
	 *
	 * @param request
	 * @param response
	 * @param userMobileForm
	 * @author liyuan
	 * @time 2015-11-11
	 */
	@RequestMapping("/userRegByPhone")
	public String userRegByPhone(HttpServletRequest request, HttpServletResponse response, UserMobileForm userMobileForm) {
		/*
		 * 表单提交前已经做过验证，验证码被清除，再次验证会不成功 //检查短信验证码是否正确
		 * if(!smsLogService.verifyCodeAndClear(userMobileForm.getMobile(),
		 * userMobileForm.getMobileCode())){ log.error("Incorrect mobile code"); return
		 * PathResolver.getPath(request, response, RedirectPage.REG); }
		 */
		// 检查图片验证码是否正确
		if (!ValidateCodeUtil.validateCode(userMobileForm.getRandNum(), request.getSession())) {
			log.error("Incorrect validate code");
			return PathResolver.getPath(RedirectPage.REG);
		}

		boolean existed = userDetailService.isPhoneExist(userMobileForm.getMobile());
		if (existed) {
			// 检查是否重名
			UserMessages messages = new UserMessages();
			messages.addCallBackList(ResourceBundleHelper.getString("errors.phone"));
			request.setAttribute(UserMessages.MESSAGE_KEY, messages);
			request.setAttribute("userForm", userMobileForm);
			return PathResolver.getPath(RedirectPage.REG);
		}

		if (AppUtils.isBlank(userMobileForm.getUserName()) || !ValidationUtil.checkUserName(userMobileForm.getUserName())) {
			log.error("register failed: 用户名不能为空或非法");
			return PathResolver.getPath(RedirectPage.REG);
		}

		User user = userDetailService.saveUserReg(IPHelper.getIpAddr(request), userMobileForm,passwordEncoder.encode(userMobileForm.getPassword()));

		request.setAttribute("userName", userMobileForm.getNickName());
		if (user == null) {
			throw new BusinessException("register user error");
		}

		// 手机注册的用户，用户安全等级+1,手机验证变为1
		userDetailService.updatePhoneVerifn(user.getName());
		if (Constants.USER_STATUS_ENABLE.equals(user.getEnabled())) {
			// 用户注册即登录
			loginService.onAuthentication(request, response, userMobileForm.getMobile(), userMobileForm.getPassword());

			// 注册环信，账号为手机号
			return PathResolver.getPath(RedirectPage.AFTER_OPERATION) + "?userName=" + user.getUserName();
		}
		return null;
	}

	/**
	 * 加载用户注册页面.
	 *
	 * @param request  the request
	 * @param response the response
	 * @return the string
	 */
	@RequestMapping("/reg")
	public String reg(HttpServletRequest request, HttpServletResponse response) {
		// 分销的用户名称
		String uid = request.getParameter("uid");
		if (AppUtils.isNotBlank(uid)) {
			request.setAttribute("uid", uid);
		}

		// 判断访问设备：PC端或者移动端
		String userAgent = request.getHeader("user-agent");
		if (userAgent.toLowerCase().contains("Mobile".toLowerCase())) {
			String url = null;
			String vueDomainName = propertiesUtil.getVueDomainName();
			if (AppUtils.isNotBlank(uid)) {
				url = vueDomainName + "/accountModules/register/register?parentUserName=" + uid;
			} else {
				url = vueDomainName + "/accountModules/register/register";
			}
			return "redirect:" + url;
		}

		return PathResolver.getPath(FrontPage.REG);
	}

	/**
	 * 检测是否是移动设备访问
	 */
	private boolean checkMobileAgent(String userAgent) {
		if (AppUtils.isBlank(userAgent)) {
			return false;
		}
		String phoneReg = "\\b(ip(hone|od)|android|opera m(ob|in)i" + "|windows (phone|ce)|blackberry" + "|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp"
				+ "|laystation portable)|nokia|fennec|htc[-_]" + "|mobile|up.browser)\\b";
		String tableReg = "\\b(ipad|tablet|(Nexus 7)|up.browser" + ")\\b";

		// 移动设备正则匹配：手机端、平板
		Pattern phonePat = Pattern.compile(phoneReg, Pattern.CASE_INSENSITIVE);
		Pattern tablePat = Pattern.compile(tableReg, Pattern.CASE_INSENSITIVE);

		// 匹配
		Matcher matcherPhone = phonePat.matcher(userAgent);
		Matcher matcherTable = tablePat.matcher(userAgent);
		if (matcherPhone.find() || matcherTable.find()) {
			return true;
		} else {
			return false;
		}
	}

	// 协议弹框
	@RequestMapping("/agreementoverlay")
	public String agreeMentOverlay(HttpServletRequest request, HttpServletResponse response) {
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		String content = systemConfig.getRegProtocolTemplate();
		request.setAttribute("regItem", content);
		return PathResolver.getPath(FrontPage.AGREEMENT_OVERLAY);
	}

	/**
	 * Adds the shop.
	 *
	 * @return the string
	 */
	/*
	 * @RequestMapping("/addShop") public String addShop(HttpServletRequest request,
	 * HttpServletResponse response, ShopDetail shopDetail) { // 用户需要登录 String
	 * userName = UserManager.getUserName(request); if (AppUtils.isBlank(userName))
	 * { return PathResolver.getPath(request, response, FrontPage.LOGIN_HINT); }
	 * return userDetailService.saveShop(request, response, shopDetail); }
	 */

	@RequestMapping("/isUserExist")
	public @ResponseBody Boolean isUserExist(String userName) {
		if (AppUtils.isBlank(userName)) {
			return true;// 用户名不允许有空值
		}
		return userDetailService.isUserExist(userName.trim());
	}

	/**
	 * 用户是否已经登录 True: 已经登录 False: 没有登录
	 *
	 * @param request
	 * @return
	 */
	@RequestMapping("/isUserLogin")
	public @ResponseBody Boolean isUserLogin(HttpServletRequest request) {
		SecurityUserDetail user = UserManager.getUser(request);
		return UserManager.isUserLogined(user);
	}

	@RequestMapping("/isEmailExist")
	public @ResponseBody Boolean isEmailExist(String email) {
		return userDetailService.isEmailExist(email);
	}

	@RequestMapping("/isPhoneExist")
	public @ResponseBody Boolean isPhoneExist(String Phone) {
		return userDetailService.isPhoneExist(Phone);
	}

	@RequestMapping("/isUserNameExist")
	public @ResponseBody Boolean isUserNameExist(String userName) {
		return userDetailService.isUserExist(userName);
	}

	/**
	 * 检查验证码是否有效
	 *
	 * @param mobile
	 * @param verifyCode
	 * @return
	 */
	@RequestMapping("/verifyCode")
	public @ResponseBody boolean verifyCode(String mobile, String verifyCode) {
		return smsLogService.verifyCodeAndClear(mobile, verifyCode, SMSTypeEnum.VAL);
	}

	/**
	 * 提交表单前检查验证码是否有效
	 *
	 * @param mobile
	 * @param verifyCode
	 * @return
	 */
	@RequestMapping("/verifyCodeBeforeSubmit")
	public @ResponseBody boolean verifyCodeBeforeSubmit(String mobile, String verifyCode) {
		return smsLogService.verifyCodeBeforeSubmit(mobile, verifyCode, SMSTypeEnum.VAL);
	}

	/**
	 * 判断昵称是否存在
	 *
	 * @param nickName
	 * @return
	 */
	@RequestMapping(value = "/isNickNameExist", produces = "application/json;charset=UTF-8")
	public @ResponseBody String isNickNameExist(String nickName) {
		if (AppUtils.isBlank(nickName)) {
			return "true";
		}
		if (sensitiveWordService.checkSensitiveWordsNameExist(nickName)) {
			return "sensitivity";
		}
		if (userDetailService.isNickNameExist(nickName)) {
			return "true";
		} else {
			return "false";
		}
	}

	/**
	 * 判断店铺名称是否存在
	 *
	 * @param siteName
	 * @return
	 */
	@RequestMapping("/isSiteNameExist")
	public @ResponseBody Boolean isSiteNameExist(String siteName) {
		if (AppUtils.isBlank(siteName)) {
			return true;
		}
		boolean result = userDetailService.isSiteNameExist(siteName.trim());
		return result;
	}

	/**
	 * 验证用户名
	 *
	 * @param userName
	 * @return
	 */
	@RequestMapping("/verifyName")
	public @ResponseBody Boolean verifyName(String userName) {
		if (ValidationUtil.checkEmail(userName)) {
			return userDetailService.isEmailExist(userName);
		} else if (ValidationUtil.checkPhone(userName)) {
			return userDetailService.isPhoneExist(userName);
		} else if (ValidationUtil.checkAlpha(userName)) {
			// return userDetailService.isUserExist(userName);
			return userDetailService.isNickNameExist(userName);
		}
		return true;
	}

	/**
	 * 重置用户名密码
	 *
	 * @param userName
	 * @param userMail
	 * @return
	 */
	@RequestMapping("/resetPassword")
	public @ResponseBody String resetPassword(String userName, String userMail) {
		if (AppUtils.isBlank(userName) || AppUtils.isBlank(userMail)) {
			return "fail";
		}
		try {
			String newPassword = RandomStringUtils.randomNumeric(10, 6);
			if (userDetailService.updatePassword(userName, userMail,passwordEncoder.encode(newPassword))) {
				return null;
			} else {
				return "fail";
			}
		} catch (Exception e) {
			log.error("", e);
			return "fail";
		}

	}

	/**
	 * 图形验证码 前端ajax验证 验证码 此时不清除session中的验证码
	 *
	 * @param randNum
	 * @return
	 */
	@RequestMapping("/validateRandImg")
	public @ResponseBody boolean validateRandImg(HttpServletRequest request, HttpServletResponse response, String randNum) {
		return ValidateCodeUtil.validateCodeAndKeep(randNum, request.getSession());
	}

	/**
	 * 设置生日日期
	 *
	 * @param birthDate the birth date
	 * @param request   the request
	 */
	private void setBirthDate(String birthDate, HttpServletRequest request) {
		try {
			String year = birthDate.substring(0, 4);
			String month = birthDate.substring(4, 6);
			String day = birthDate.substring(6, 8);
			request.setAttribute("userBirthYear", year);
			request.setAttribute("userBirthMonth", month);
			request.setAttribute("userBirthDay", day);
		} catch (Exception e) {

		}

	}

	/**
	 * 页面加密手机号码，保留前3个和后3个
	 *
	 * @param mobile
	 * @return
	 */
	private String mobileSubstitution(String mobile) {
		if (mobile == null) {
			return null;
		}
		if (mobile.length() < 11) {
			return mobile;
		}
		return mobile.replaceAll(mobile.substring(3, mobile.length() - 3), "******");
	}

	private String mailSubstitution(String mail) {
		int pos = mail.indexOf("@");
		if (pos != 0) {
			String preFix = mail.substring(0, pos);
			String postFix = mail.substring(pos, mail.length());
			int mailPrefixLength = preFix.length();
			StringBuilder sb = new StringBuilder();
			if (mailPrefixLength < 3) {
				for (int i = 0; i < mailPrefixLength; i++) {
					sb.append("*");
				}
			} else {
				sb.append(preFix.subSequence(0, 1));
				for (int i = 1; i < mailPrefixLength - 1; i++) {
					sb.append("*");
				}
				sb.append(preFix.subSequence(mailPrefixLength - 1, mailPrefixLength));
			}
			return sb.toString() + postFix;
		}
		return "";
	}

	/**
	 * Check email.
	 *
	 * @param email the email
	 * @return true, if successful
	 */
	private boolean checkEmail(String email) {
		if (AppUtils.isBlank(email)) {
			return false;
		}
		Pattern pattern = Pattern.compile("^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\\.[a-zA-Z0-9_-]{2,3}){1,2})$");
		Matcher matcher = pattern.matcher(email);
		if (matcher.matches()) {
			return true;
		}
		return false;
	}

	/**
	 * Check phone.
	 *
	 * @param phone the phone
	 * @return true, if successful
	 */
	private boolean checkPhone(String phone) {
		if (AppUtils.isBlank(phone)) {
			return false;
		}
		Pattern pattern = Pattern.compile("^(1)\\d{10}$"); // 以1开始的11位数字
		Matcher matcher = pattern.matcher(phone);
		if (matcher.matches()) {
			return true;
		}
		return false;
	}

	// 随机产生的邮箱验证码
	private String getRandomString(int length) {
		String base = "abcdefghijklmnopqrstuvwxyz0123456789";
		Random random = new Random();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++) {
			int number = random.nextInt(base.length());
			sb.append(base.charAt(number));
		}
		return sb.toString();
	}

	/**
	 * 判断用户是否登录
	 */
	@RequestMapping(value = "/loginFlag", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public String loginFlag(HttpServletRequest request, HttpServletResponse response) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return Constants.FAIL;
		}
		return Constants.SUCCESS;
	}

	/**
	 * 环信登录
	 */
	@RequestMapping("/loginHuanXin")
	public String loginHuanXin(HttpServletRequest request, HttpServletResponse response) {
		SecurityUserDetail securityUserDetail = UserManager.getUser(request);
		if (AppUtils.isNotBlank(securityUserDetail)) {
			ShopDetail personalShopDetail = shopDetailService.getShopDetailByUserId(securityUserDetail.getUserId());
			if (AppUtils.isNotBlank(personalShopDetail)) {// 1. 商家登录
				UserDetail user = userDetailService.getUserDetailById(securityUserDetail.getUserId());

				if (AppUtils.isNotBlank(user)) {
					if (!Integer.valueOf(1).equals(user.getIsRegIM())) {
						request.setAttribute("hasReg", 0);
						user.setIsRegIM(1);
						userDetailService.updateUserDetail(user);
					}
					String password = user.getUserName() + "123456";
					request.setAttribute("user_name", user.getUserName());// 用户名称
					request.setAttribute("user_pass", password);// 用户密码
					request.setAttribute("user_nick", personalShopDetail.getSiteName());
					request.setAttribute("pic", personalShopDetail.getShopPic());
					return PathResolver.getPath(FrontPage.PROD_HUANXIN);
				} else {
					return PathResolver.getPath(FrontPage.OPERATION_ERROR);
				}
			} else {// 2. 用户登录
				UserDetail user = userDetailService.getUserDetailById(securityUserDetail.getUserId());

				if (AppUtils.isNotBlank(user)) {
					if (!Integer.valueOf(1).equals(user.getIsRegIM())) {
						request.setAttribute("hasReg", 0);
						user.setIsRegIM(1);
						userDetailService.updateUserDetail(user);
					}
					String password = user.getUserName() + "123456";
					request.setAttribute("user_name", user.getUserName());
					request.setAttribute("user_pass", password);
					request.setAttribute("user_nick", user.getNickName());
					request.setAttribute("pic", user.getPortraitPic());
					return PathResolver.getPath(FrontPage.PROD_HUANXIN);
				} else {
					return PathResolver.getPath(FrontPage.OPERATION_ERROR);
				}
			}
		} else {
			return PathResolver.getPath(FrontPage.LOGIN);
		}
	}
}
