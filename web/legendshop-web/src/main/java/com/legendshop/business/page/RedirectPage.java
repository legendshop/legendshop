/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * The Enum RedirectPage.
 */
public enum RedirectPage implements PageDefinition {
	
	HOME_QUERY("/home"),

	REG("/reg"),
	
	/** 二级分类列表页面 **/
	NSORT_LIST_QUERY("/admin/nsort/query"),
	
	
	VIEWS("/views"), 
	
	/** 购物车  **/
	MYSHOPCART("/shopCart/shopBuy"), 
	
	/** 提交订单成功页面 **/
	ORDERSUCCESS("/p/orderSuccess"), 
	
	
	MYORDER("/p/myorder"),
	
	RETURNPAY("/p/returnPay"), 
	
    OPEN_SHOP("/openShop"),
    
    OPENSHOP_SUCCES("/p/doOpenShopSuc"),
	
	/** 商城配置*/
	SHOP_SETTING("/s/shopSetting"),
	
		/** 操作成功. */
	AFTER_OPERATION("/afteroperation"),
	
	SHOP_COUPONS_LIST_QUERY("/s/coupon/query"),
	
	ORDER_DETAIL("/p/orderDetails"),
	
	MAILINGFEE_SETTING("/s/mailingFee/load"),
	
	AUCTION_DEPOSIT("/auction/views"), 
	
	/** 绑定手机页面 */
	BIND_ACCOUNT("/thirdlogin/bind"),
	
	;
	

	/** The value. */
	private final String value;

	private RedirectPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("redirect:", path);
	}

	/**
	 * Instantiates a new tiles page.
	 * 
	 * @param value
	 *            the value
	 */
	private RedirectPage(String value) {
		this.value = value;
	}

	public String getNativeValue() {
		return value;
	}

}
