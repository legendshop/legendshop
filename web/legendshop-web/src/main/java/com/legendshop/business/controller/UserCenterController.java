/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.constant.*;
import com.legendshop.model.dto.order.*;
import com.legendshop.spi.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.util.DateUtils;
import com.legendshop.base.util.ExpressMD5;
import com.legendshop.business.page.FrontPage;
import com.legendshop.business.page.UserCenterFrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.dto.ExpressDeliverDto;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.DeliveryCorp;
import com.legendshop.model.entity.LoginHistory;
import com.legendshop.model.entity.Pub;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubHistory;
import com.legendshop.model.entity.SubItem;
import com.legendshop.model.entity.SystemParameter;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.UserSecurity;
import com.legendshop.model.entity.store.Store;
import com.legendshop.model.entity.store.StoreOrder;
import com.legendshop.processor.helper.DelayCancelHelper;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;
import com.legendshop.util.HttpUtil;
import com.legendshop.util.JSONUtil;

/**
 * 用户中心controller.
 * 
 * 
 */

@Controller
public class UserCenterController extends BaseController {

	/** The log. */
	private final Logger logger = LoggerFactory.getLogger(UserCenterController.class);

	private String DEAULT_CURRENT_PAGE_NO = "1";

	/** The order service. */
	@Autowired
	private SubService subService;

	@Autowired
	private SubItemService subItemService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private KuaiDiService kuaiDiService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private LoginHistoryService loginHistoryService;

	@Autowired
	private DelayCancelHelper delayCancelHelper;

	@Autowired
	private StoreOrderService storeOrderService;
	
	@Autowired
	private PubService pubService;
	
	@Autowired
	private ShopDetailService shopDetailService;
	
	@Autowired
	private ConstTableService constTableService;
	
	@Autowired
	private DeliveryCorpService deliveryCorpService;
	
	@Autowired
	private SystemParameterService systemParameterService;
	
	@Autowired
	private SubHistoryService subHistoryService;
	
	@Autowired
	private StoreService storeService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Autowired
	private MergeGroupAddService mergeGroupAddService;

	@Autowired
	private SubRefundReturnService subRefundReturnService;
	
	/**
	 * 发送站内信
	 */
	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;
	
	/**
	 * @Deprecated
	 * @param request
	 * @param response
	 * @param
	 * @return @RequestMapping("/p/usercenter") public String
	 *         usercenter(HttpServletRequest request, HttpServletResponse
	 *         response, String uc) { return PathResolver.getPath(request,
	 *         response, UserCenterFrontPage.USER_CENTER_MAIN); }
	 **/

	@RequestMapping("/p/uchome")
	public String home(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		try {
			SecurityUserDetail user = UserManager.getUser(request);
			String userName = user.getUsername();
			queryOrdersLatestMonth(userName, DEAULT_CURRENT_PAGE_NO, request);

			// String portraitPic = userDetailService.getPortraitPic(userName);
			UserDetail userDetail = userDetailService.getUserDetail(userName);
			UserSecurity userSecurity = userDetailService.getUserSecurity(userName);
			LoginHistory loginHistory = loginHistoryService.getLastLoginTime(userName);
			if (AppUtils.isNotBlank(loginHistory)) {
				request.setAttribute("lastLoginTime", loginHistory.getTime());
			}
			//最新公告
			List<Pub> pubList = pubService.queryUserPub();
			request.setAttribute("pubList", pubList);
			request.setAttribute("portraitPic", userDetail.getPortraitPic());
			request.setAttribute("userDetail", userDetail);
			request.setAttribute("userSecurity", userSecurity);
		} catch (Exception e) {
			logger.error("invoking toUserCenterPage", e);
		}
		return PathResolver.getPath(UserCenterFrontPage.USERCENTER_HOME);
	}

	/**
	 * 去往注册弹框
	 */
	@RequestMapping("/loadRegisterOverlay")
	public String loadRegisterOverlay(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(UserCenterFrontPage.REGISTER_OVERLAY);
	}

	/**
	 * 去往注册弹框
	 */
	@RequestMapping("/loadLoginOverlayContent")
	public String loadRegisterOverlayContent(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(UserCenterFrontPage.LOGIN_OVERLAY_CONTENT);
	}

	/**
	 * 去往登录弹框
	 */
	@RequestMapping("/loadLoginOverlay")
	public String loadLoginOverlay(HttpServletRequest request, HttpServletResponse response) {
		response.setHeader("X-Frame-Options", "SAMEORIGIN");
		return PathResolver.getPath(UserCenterFrontPage.LOGIN_OVERLAY);
	}

	private void queryOrdersLatestMonth(String userName, String curPageNO, HttpServletRequest request) {
		PageSupport<Sub> ps = subService.getOrderList(curPageNO, userName);
		PageSupportHelper.savePage(request, ps);
	}

	/**
	 * 我的订单
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/p/myorder")
	public String myorder(HttpServletRequest request, HttpServletResponse response, String curPageNO, Integer state_type, String order_sn, Date startDate, Date endDate, String sub_type) {
		int pageSize = 10;// 默认一页显示条数
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		if (AppUtils.isBlank(userId)) {
			return PathResolver.getPath(FrontPage.LOGIN_HINT);
		}
		OrderSearchParamDto paramDto = new OrderSearchParamDto();
		Date fromDate = startDate;
		Date toDate = endDate;
		if (AppUtils.isNotBlank(endDate)) {
			//获取结束时间的最后一刻
			toDate = DateUtils.getEndMonment(endDate);
		}
		paramDto.setPageSize(pageSize);
		paramDto.setCurPageNO(curPageNO);
		paramDto.setUserId(userId);
		paramDto.setStatus(state_type);
		paramDto.setSubNumber(order_sn);
		paramDto.setStartDate(startDate);
		paramDto.setEndDate(toDate);
		paramDto.setSubType(sub_type);
		paramDto.setDeleteStatus(OrderDeleteStatusEnum.NORMAL.value());

		PageSupport<MySubDto> ps = orderService.getMyorderDtos(paramDto);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("startDate", fromDate);
		request.setAttribute("endDate", endDate);
		request.setAttribute("paramDto", paramDto);
		request.setAttribute("curPageNO", curPageNO);
		request.setAttribute("state_type", state_type);
		request.setAttribute("order_sn", order_sn);
		request.setAttribute("sub_type", sub_type);
		ConstTable constTable =constTableService.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
		ConstTable auctionConstTable =constTableService.getConstTablesBykey("AUCTION_ORDER_SETTING", "AUCTION_ORDER_SETTING");
		OrderSetMgDto orderSetting=null;
		OrderSetMgDto auctionOrderSetting=null;
		if(AppUtils.isNotBlank(constTable)){
			String setting=constTable.getValue();
		   orderSetting=JSONUtil.getObject(setting, OrderSetMgDto.class);
		}
		if(AppUtils.isNotBlank(auctionConstTable)){
			String auctionSetting=auctionConstTable.getValue();
			auctionOrderSetting=JSONUtil.getObject(auctionSetting, OrderSetMgDto.class);
		}
		Integer order_cancel=Integer.parseInt(orderSetting.getAuto_order_cancel());
		Integer auction_order_cancel=Integer.parseInt(auctionOrderSetting.getAuto_order_cancel());
		request.setAttribute("order_cancel", order_cancel);
		request.setAttribute("auction_order_cancel", auction_order_cancel);
		String result = PathResolver.getPath(UserCenterFrontPage.MYORDER);
		return result;
	}

	/**
	 * 我的订单-回收站
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/p/myorderdels")
	public String myorderDel(HttpServletRequest request, HttpServletResponse response, String curPageNO, Integer state_type, String order_sn, Date startDate,
			Date endDate) {
		int pageSize = 10;// 默认一页显示条数
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(FrontPage.LOGIN_HINT);
		}
		String userId = user.getUserId();
		OrderSearchParamDto paramDto = new OrderSearchParamDto();
		Date fromDate = startDate;
		Date toDate = endDate;
		paramDto.setPageSize(pageSize);
		paramDto.setCurPageNO(curPageNO);
		paramDto.setUserId(userId);
		paramDto.setDeleteStatus(OrderDeleteStatusEnum.DELETED.value());
		paramDto.setSubNumber(order_sn);
		paramDto.setStartDate(startDate);
		paramDto.setEndDate(endDate);
		paramDto.setStatus(state_type);
		PageSupport<MySubDto> ps = orderService.getMyorderDtos(paramDto);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("startDate", fromDate);
		request.setAttribute("endDate", toDate);
		request.setAttribute("paramDto", paramDto);
		request.setAttribute("curPageNO", curPageNO);
		request.setAttribute("state_type", state_type);
		request.setAttribute("order_sn", order_sn);
		String result = PathResolver.getPath(UserCenterFrontPage.MYORDERDEL);
		return result;
	}

	/**
	 * 查询预售订单
	 * 
	 * @param request
	 * @param response
	 * @param
	 * @return
	 */
	@RequestMapping("/p/presellOrder")
	public String presellOrder(HttpServletRequest request, HttpServletResponse response, OrderSearchParamDto paramDto) {
		// 检查用户是否登陆
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(FrontPage.LOGIN_HINT);
		}
		
		if (AppUtils.isBlank(paramDto.getPageSize())) {
			paramDto.setPageSize(10);// 默认的每页显示10条记录
		}
		paramDto.setUserId(user.getUserId());
		paramDto.setDeleteStatus(OrderDeleteStatusEnum.NORMAL.value());// 查询正常订单

		PageSupport<PresellSubDto> ps = orderService.getPresellOrderList(paramDto);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("paramDto", paramDto);
		return PathResolver.getPath(UserCenterFrontPage.PRESELLORDER);
	}

	/**
	 * 查询预售订单 - 回收站
	 * 
	 * @param request
	 * @param response
	 * @param
	 * @return
	 */
	@RequestMapping("/p/presellOrderDels")
	public String presellOrderDels(HttpServletRequest request, HttpServletResponse response, OrderSearchParamDto paramDto) {
		// 检查用户是否登陆
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(FrontPage.LOGIN_HINT);
		}

		if (AppUtils.isBlank(paramDto.getPageSize())) {
			paramDto.setPageSize(10);// 默认的每页显示10条记录
		}
		paramDto.setUserId(user.getUserId());
		paramDto.setDeleteStatus(OrderDeleteStatusEnum.DELETED.value());// 查询正常订单

		PageSupport<PresellSubDto> ps = orderService.getPresellOrderList(paramDto);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("paramDto", paramDto);

		return PathResolver.getPath(UserCenterFrontPage.PRESELLORDERDELS);
	}

	/**
	 * 取消订单
	 * @param request
	 * @param response
	 * @param subNumber
	 * @param cancelReason
	 * @return
	 */
	@RequestMapping(value = "/p/order/cancleOrder")
	@ResponseBody
	public String cancleOrder(HttpServletRequest request, HttpServletResponse response, String subNumber, String cancelReason) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		try {
			if (AppUtils.isNotBlank(subNumber)) {
				Sub sub = subService.getSubBySubNumberByUserId(subNumber, userId);
				if (AppUtils.isNotBlank(sub) && OrderStatusEnum.UNPAY.value().equals(sub.getStatus())) {
					Date date = new Date();
					sub.setUpdateDate(date);
					sub.setStatus(OrderStatusEnum.CLOSE.value());
					sub.setCancelReason(cancelReason);
					boolean result = subService.cancleOrder(sub);
					if (result) {
						SubHistory subHistory = new SubHistory();
						String time = DateUtils.format(date, DateUtils.PATTERN_CLASSICAL);
						subHistory.setRecDate(date);
						subHistory.setSubId(sub.getSubId());
						subHistory.setStatus(SubHistoryEnum.ORDER_CANCEL.value());
						StringBuilder sb = new StringBuilder();
						sb.append(sub.getUserName()).append("于").append(time).append("因为：("+cancelReason+")取消订单 .");
						subHistory.setUserName(sub.getUserName());
						subHistory.setReason(sb.toString());
						subHistoryService.saveSubHistory(subHistory);
						orderService.cleanOrderCache(sub.getUserId(), sub.getShopId());
						
						// remove delay queue ordre
						delayCancelHelper.remove(sub.getSubNumber());

						return Constants.SUCCESS;
					}
				} else {
					return Constants.FAIL;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return Constants.FAIL;
	}
	
	
	@RequestMapping("/p/order/cancleOrderShow")
	public String cancleOrderShow(HttpServletRequest request, HttpServletResponse response, String subNumber){
		request.setAttribute("subNumber", subNumber);
		return PathResolver.getPath(UserCenterFrontPage.CANCLEORDERSHOE);
	}

	/**
	 * 删除订单
	 * 
	 * @param request
	 * @param response
	 * @param subNumber
	 * @return
	 */
	@RequestMapping(value = "/p/dropOrder/{subNumber}")
	@ResponseBody
	public String dropOrder(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumber) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (user == null || subNumber == null) {
			return Constants.FAIL;
		}
		Sub sub = subService.getSubBySubNumberByUserId(subNumber, user.getUserId());

		if (AppUtils.isNotBlank(sub) && OrderDeleteStatusEnum.DELETED.value().equals(sub.getDeleteStatus())) {
			if (OrderStatusEnum.SUCCESS.value().equals(sub.getStatus())) {// 已经支付，
																			// 逻辑删除
				updateOrderDeleteStatus(sub, OrderDeleteStatusEnum.PERMANENT_DELETED.value());
				return Constants.SUCCESS;
			} else if (OrderStatusEnum.CLOSE.value().equals(sub.getStatus())) {// 用户还没有支付，永久真实删除
				boolean result = subService.dropOrder(sub);
				if (result) {
					orderService.cleanOrderCache(sub.getUserId(), sub.getShopId());
					return Constants.SUCCESS;
				}
			} else {
				return Constants.FAIL;
			}
		}

		return Constants.FAIL;
	}

	/**
	 * 用户将订单放入回收站
	 * 
	 * @param request
	 * @param response
	 * @param subNumber
	 * @return
	 */
	@RequestMapping(value = "/p/order/removeOrder")
	@ResponseBody
	public String removeOrder(HttpServletRequest request, HttpServletResponse response, String subNumber) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		if (AppUtils.isNotBlank(subNumber)) {
			Sub sub = subService.getSubBySubNumberByUserId(subNumber, userId);
			if (AppUtils.isNotBlank(sub) && // 只有成功或关闭的订单才可以删除，并且没有退单的情况下
					(OrderStatusEnum.CLOSE.value().equals(sub.getStatus()) || OrderStatusEnum.SUCCESS.value().equals(sub.getStatus()) &&
							sub.getRefundState() != 1		)) {
				updateOrderDeleteStatus(sub, OrderDeleteStatusEnum.DELETED.value());
				return Constants.SUCCESS;
			} else {
				return Constants.FAIL;
			}
		}
		return Constants.FAIL;
	}

	/**
	 * 用户将回收站的订单还原
	 * 
	 * @param request
	 * @param response
	 * @param subNumber
	 * @return
	 */
	@RequestMapping(value = "/p/order/restoreOrder")
	@ResponseBody
	public String restoreOrder(HttpServletRequest request, HttpServletResponse response, String subNumber) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		if (AppUtils.isNotBlank(subNumber)) {
			Sub sub = subService.getSubBySubNumberByUserId(subNumber, userId);
			if (AppUtils.isNotBlank(sub) && // 只有删除状态的订单可以还原
					OrderDeleteStatusEnum.DELETED.value().equals(sub.getDeleteStatus())) {
				updateOrderDeleteStatus(sub, OrderDeleteStatusEnum.NORMAL.value());
				return Constants.SUCCESS;
			} else {
				return Constants.FAIL;
			}
		}
		return Constants.FAIL;
	}

	/**
	 * 更改 订单 删除状态
	 * 
	 * @param sub
	 * @param deleteStatus
	 */
	private void updateOrderDeleteStatus(Sub sub, Integer deleteStatus) {
		Date date = new Date();
		sub.setUpdateDate(date);
		sub.setDeleteStatus(deleteStatus);
		subService.updateSub(sub);
		orderService.cleanOrderCache(sub.getUserId(), sub.getShopId());
	}

	/**
	 * 查看订单详情
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param subNumber
	 *            the sub number
	 * @return the string
	 */
	@RequestMapping("/p/orderDetail/{subNumber}")
	public String orderDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumber) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(FrontPage.LOGIN);
		}
		MySubDto myorder = subService.findOrderDetail(subNumber, user.getUserId());
		
		//如果是门店订单，获取门店相关信息以及提货码
		StoreOrder storeOrder = storeOrderService.getDeliveryOrderBySubNumber(myorder.getSubNum());
		if (AppUtils.isNotBlank(storeOrder)) {
			Store store = storeService.getStore(storeOrder.getStoreId());
			request.setAttribute("store", store);
		}
		
		request.setAttribute("order", myorder);
		request.setAttribute("storeOrder", storeOrder);
		String result = PathResolver.getPath(UserCenterFrontPage.ORDERDETAIL);
		return result;
	}
	

	/**
	 * 查看预售订单详情
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param subNumber
	 *            the sub number
	 * @return the string
	 */
	@RequestMapping("/p/presellOrderDetail/{subNumber}")
	public String presellOrderDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumber) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(FrontPage.LOGIN);
		}
		PresellSubDto preSellOrder = subService.findPresellOrderDetail(subNumber, user.getUserId());
		request.setAttribute("order", preSellOrder);
		return PathResolver.getPath(UserCenterFrontPage.PRESELLORDERDETAIL);
	}

	@RequestMapping("/p/orderExpress/{subNumber}")
	public String orderExpress(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumber) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(FrontPage.LOGIN);
		}
		MySubDto myOrder = subService.findOrderDetail(subNumber, user.getUserId());
		request.setAttribute("order", myOrder);
		return PathResolver.getPath(UserCenterFrontPage.ORDEREXPRESS);
	}

	/**
	 * 确认收货
	 */
	@RequestMapping("/p/orderReceive/{subNumber}")
	@ResponseBody
	public String orderReceive(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumber) {
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user) || AppUtils.isBlank(subNumber)) {
			return Constants.FAIL;
		}
		boolean result = subService.orderReceive(subNumber, user.getUserId());
		if (result) {
			return Constants.SUCCESS;
		} else {
			return Constants.FAIL;
		}
	}

	@RequestMapping(value = "/p/order/getExpress", method = RequestMethod.POST)
	public @ResponseBody String getExpress(HttpServletRequest request, HttpServletResponse response, String dvyFlowId, Long dvyTypeId,String reciverMobile) {
		
		if (AppUtils.isBlank(dvyTypeId) || AppUtils.isBlank(dvyFlowId) || AppUtils.isBlank(reciverMobile)) {
			return Constants.FAIL;
		}
		DeliveryCorp deliveryCorp = deliveryCorpService.getDeliveryCorpByDvyId(dvyTypeId);
		if (AppUtils.isNotBlank(deliveryCorp)) {
			
			SystemParameter systemParameter = systemParameterService.getSystemParameter("EXPRESS_DELIVER");
			if (AppUtils.isBlank(systemParameter) || AppUtils.isBlank(systemParameter.getValue())) { //查询到没配置密钥则 是免费的  
				
				String url = deliveryCorp.getQueryUrl();
				if (AppUtils.isNotBlank(url)) {
					url = url.replaceAll("\\{dvyFlowId\\}", dvyFlowId);
				}
				KuaiDiLogs logs = kuaiDiService.query(url);
				if (AppUtils.isNotBlank(logs) && AppUtils.isNotBlank(logs.getEntries()) && logs.getEntries().size()>0) {
					return JSONUtil.getJson(logs.getEntries());
				}
				
			}else{  //快递100
				
				if(AppUtils.isBlank(deliveryCorp.getCompanyCode())){
					return Constants.FAIL;
				}
				
				ExpressDeliverDto express = JSONUtil.getObject(systemParameter.getValue(), ExpressDeliverDto.class);
				
				//组装参数密钥
				String param ="{\"com\":\""+deliveryCorp.getCompanyCode()+"\",\"num\":\""+dvyFlowId+"\",\"mobiletelephone\":\""+reciverMobile+"\"}";
				String sign = ExpressMD5.encode(param+express.getKey()+express.getCustomer());
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("param",param);
				params.put("sign",sign);
				params.put("customer",express.getCustomer());
				//请求第三方接口
				String text = HttpUtil.httpPost(ExpressMD5.API_EXPRESS_URL, params);
				if (AppUtils.isBlank(text)) {
					return Constants.FAIL;
				}
				
				JSONObject jsonObject = JSONObject.parseObject(text);
				String result = jsonObject.getString("status");
				if(AppUtils.isBlank(result) || !"200".equals(result)){ 
					logger.warn("Express inquiry failed message = {},returnCode = {}", jsonObject.getString("message"),jsonObject.getString("returnCode"));
					return Constants.FAIL;
				}
				return jsonObject.getString("data");
			}
		}
		return Constants.FAIL;
	}

	/**
	 * 我的分销单
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param status
	 * @param subItemNum
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	@RequestMapping("/p/myDistOrder")
	public String myDistOrder(HttpServletRequest request, HttpServletResponse response, String curPageNO, Integer status, String subItemNum, String startDate,
			String endDate) {

		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(FrontPage.LOGIN_HINT);
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-d");
		Date fromDate = null;
		Date toDate = null;
		try {
			if (AppUtils.isNotBlank(startDate)) {
				fromDate = dateFormat.parse(startDate);
			}
			if (AppUtils.isNotBlank(endDate)) {
				toDate = dateFormat.parse(endDate);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		PageSupport<SubItem> ps = subItemService.querySubItems(curPageNO, user.getUsername(), status, subItemNum, fromDate, toDate);
		PageSupportHelper.savePage(request, ps);

		int distDay = systemParameterUtil.getDistributionSettlementDay();
		request.setAttribute("distDay", distDay);
		request.setAttribute("startDate", fromDate);
		request.setAttribute("endDate", toDate);
		request.setAttribute("curPageNO", curPageNO);
		request.setAttribute("status", status);
		request.setAttribute("subItemNum", subItemNum);
		String result = PathResolver.getPath(UserCenterFrontPage.MYDISTORDER); // simple
		return result;
	}
	
	/**
	 * @Description: 提醒发货
	 * @date 2016-8-1 
	 */
	@ResponseBody
	@RequestMapping(value="/p/sendShipMsg")
	public String sendSiteMsg(HttpServletRequest request, HttpServletResponse response, String subNumber,Long shopId){
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		try {
			ShopDetail shopDetail=shopDetailService.getShopDetailById(shopId);
			if(AppUtils.isBlank(shopDetail)){
				return "该店铺信息不存在";
			}
			Sub sub = subService.getSubBySubNumber(subNumber);
			if(AppUtils.isNotBlank(sub)){
				if(AppUtils.isNotBlank(sub.getRemindDelivery())){
					if(sub.getRemindDelivery()==1){
						return "您已经提醒过商家了哦, 请耐心等候吧";
					}
				}else if(!(0==sub.getRefundState())){
					return "正在申请退款中,请耐心等候";
				}
				//判断下单时间是否超过一天
				/*if((DateUtil.getOffsetHours(sub.getSubDate(), new Date()))<=24){
					return "未到发货时间，请耐心等待";
				}*/
			}
			subService.remindDeliveryBySN(subNumber,userId);
			//发送站内信 通知
			sendSiteMessageProcessor.process(new SendSiteMsgEvent(shopDetail.getUserName(),"发货提醒","亲，订单【"+subNumber+"】该发货啦").getSource());
			return "成功提醒商家发货，请耐心等候吧";
		} catch (Exception e) {
			logger.error("提醒发货出错---------------->"+e.getMessage());
			return "对不起, 提醒发货失败, 请联系客服!";
		}
	}

	/**
	 * 订单退款前 判断该订单是否是拼团的团长免单订单
	 */
	@ResponseBody
	@RequestMapping(value="/p/isMergeGroupHeadFreeSub")
	public String isMergeGroupHeadFreeSub(HttpServletRequest request,Long subId){
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		Sub sub = subService.getSubById(subId);
		if (SubTypeEnum.MERGE_GROUP.value().equals(sub.getSubType())){

			Boolean result = mergeGroupAddService.isMergeGroupHeadFreeSub(subId);
			if (result){
				List<SubItem> list = subItemService.getSubItem(sub.getSubNumber());
				ApplyRefundReturnDto refund = subRefundReturnService.getApplyRefundReturnDto(list.get(0).getId(),RefundReturnTypeEnum.OP_ORDER_ITEM, userId);
				if (null == refund){
					return Constants.FAIL;
				}
				if (refund.getRefundAmount() == 0){
					return Constants.FAIL;
				}
			}
		}
		return Constants.SUCCESS;
	}
}
