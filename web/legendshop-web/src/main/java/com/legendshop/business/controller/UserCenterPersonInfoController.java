/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSMSEvent;
import com.legendshop.base.event.ShortMessageInfo;
import com.legendshop.base.exception.PermissionException;
import com.legendshop.base.util.*;
import com.legendshop.business.page.UserCenterFrontPage;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.framework.commond.ErrorCode;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.SendIntegralRuleEnum;
import com.legendshop.model.entity.*;
import com.legendshop.model.securityCenter.UserInformation;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.manager.MailManager;
import com.legendshop.spi.service.*;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.uploader.model.FeignMultipartFile;
import com.legendshop.uploader.service.AttachmentService;
import com.legendshop.uploader.util.ValidateFileProcessor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SystemUtil;
import com.legendshop.web.helper.IPHelper;
import com.legendshop.web.util.ValidateCodeUtil;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * 个人中心控制器.
 */
@Controller
public class UserCenterPersonInfoController extends BaseController {

    /**
     * The log.
     */
    private final Logger logger = LoggerFactory.getLogger(UserCenterPersonInfoController.class);

    /**
     * The user center person info service.
     */
    @Autowired
    private UserCenterPersonInfoService userCenterPersonInfoService;

    @Autowired
    private UserAddressService userAddressService;

    @Autowired
    private UserDetailService userDetailService;

    @Autowired
    private MailManager mailManager;

    @Autowired
    private MailHelper mailHelper;

    @Autowired
    private PassportService passportService;

    @Autowired
    private AttachmentManager attachmentManager;

    @Autowired
    private LocationService locationService;
    
    @Autowired
    private SMSLogService smsLogService;
    
    @Autowired
    private AttachmentService attachmentService;
    
	/**
	* 积分服务
	*/
	@Autowired
	private IntegralService integralService;
	
	/**
	* 短信发送者
	*/
	@Resource(name = "sendSMSProcessor")
    private EventProcessor<ShortMessageInfo> sendSMSProcessor;	
	
	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;
	
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private HttpServletRequest request;


    /**
     * 查看个人信息
     */
    @RequestMapping(value = "/p/user")
    public String user() {
    	
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
        MyPersonInfo myPersonInfo = userCenterPersonInfoService.getUserCenterPersonInfo(userId);
        //处理手机号码
        if (AppUtils.isNotBlank(myPersonInfo.getUserMobile())) {
            String mobile = mobileSubstitution(myPersonInfo.getUserMobile());
            myPersonInfo.setUserMobile(mobile);
        }
        //处理邮箱号码
        if (AppUtils.isNotBlank(myPersonInfo.getUserMail())) {
            String mail = mailSubstitution(myPersonInfo.getUserMail());
            myPersonInfo.setUserMail(mail);
        }
        //获取地址
        if (AppUtils.isNotBlank(myPersonInfo.getProvinceid())) {
            String province = locationService.getProvinceById(Integer.parseInt(myPersonInfo.getProvinceid())).getProvince();
            myPersonInfo.setProvince(province);
        }
        if (AppUtils.isNotBlank(myPersonInfo.getCityid())) {
            String city = locationService.getCityById(Integer.parseInt(myPersonInfo.getCityid())).getCity();
            myPersonInfo.setCity(city);
        }
        if (AppUtils.isNotBlank(myPersonInfo.getAreaid())) {
            String area = locationService.getAreaById(Integer.parseInt(myPersonInfo.getAreaid())).getArea();
            myPersonInfo.setArea(area);
        }
        request.setAttribute("MyPersonInfo", myPersonInfo);
        return PathResolver.getPath(UserCenterFrontPage.USER_INFO);
    }

    /**
     * 修改个人信息
     *
     */
    @RequestMapping(value = "/p/user/edit")
    public String editUserInfo() {
		SecurityUserDetail user = UserManager.getUser(request);
        MyPersonInfo myPersonInfo = userCenterPersonInfoService.getUserCenterPersonInfo(user.getUserId());

        //处理手机号码
        if (AppUtils.isNotBlank(myPersonInfo.getUserMobile())) {
            String mobile = mobileSubstitution(myPersonInfo.getUserMobile());
            myPersonInfo.setUserMobile(mobile);
        }

        //处理邮箱号码
        if (AppUtils.isNotBlank(myPersonInfo.getUserMail())) {
            String mail = mailSubstitution(myPersonInfo.getUserMail());
            myPersonInfo.setUserMail(mail);
        }
        request.setAttribute("MyPersonInfo", myPersonInfo);
        return PathResolver.getPath(UserCenterFrontPage.USER_EDIT);
    }

    @RequestMapping(value = "/p/loadphotoverlay")
    public String loadPhotoOverlay(HttpServletRequest request, HttpServletResponse response) {
    	SecurityUserDetail user = UserManager.getUser(request);
    	String userName = user.getUsername();
        //获取用户头像路径
        UserDetail userDetail = userDetailService.getUserDetail(userName);
        if(AppUtils.isBlank(userDetail)){
        	return PathResolver.getPath(UserCenterFrontPage.FAIL); 
        }
        String portraitPic = userDetailService.getPortraitPic(userName);
        //清除上次的未保存头像路径
        if(AppUtils.isNotBlank(userDetail.getUpdatePic())){
        	userDetail.setUpdatePic(null);
        	userDetailService.updateUserDetail(userDetail);
        }
        request.setAttribute("portraitPic", portraitPic);
        return PathResolver.getPath(UserCenterFrontPage.PHOTO_OVERLAY);
    }

    /**
     * 保存头像文件
     *
     */
    @RequestMapping(value = "/p/savePhoto")
    public
    @ResponseBody
    String savePhoto(MultipartHttpServletRequest request,  MultipartFile file) {
    	SecurityUserDetail user = UserManager.getUser(request);
        UserDetail userDetail = userDetailService.getUserDetail(user.getUsername());
        if(AppUtils.isBlank(userDetail)){
       	 return Constants.FAIL;
       }
        if (file.getSize() > 0 && ValidateFileProcessor.validateFile(file.getOriginalFilename(), file.getSize())) {
            String originlogoPic = userDetail.getPortraitPic();
            String pic = attachmentManager.upload(file);
            //保存附件表
            attachmentManager.saveImageAttachment(user.getUsername(),user.getUserId(),user.getShopId(), pic, file, AttachmentTypeEnum.USER);
            //此时只是预览，不能保存 
            if(AppUtils.isNotBlank(userDetail.getUpdatePic())){
            	attachmentManager.delAttachmentByFilePath(userDetail.getUpdatePic());
                attachmentManager.deleteTruely(userDetail.getUpdatePic());
            }
            userDetail.setUpdatePic(pic);
            userDetailService.updateUserDetail(userDetail);
            return pic;
        } else {
            return Constants.FAIL;
        }

    }

    /**
     * Out logo.
     *
     * @param file    the file
     * @param url     the url
     * @param formatedName     the formatedName
     * @param dwidth  the dwidth
     * @param dheight the dheight
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void outLogo(InputStream file, String url, String formatedName, int dwidth, int dheight) throws IOException {
        BufferedInputStream stream = new BufferedInputStream(file, 4096);
        BufferedImage src = ImageIO.read(stream);
        int width = src.getWidth(null);
        int height = src.getHeight(null);
        int towidth, toheight;
        if (width > dwidth || height > dheight) {
            if (((float) width / dwidth) >= ((float) height / dheight)) {
                towidth = dwidth;
                //toheight = (height * dwidth) / width;
                toheight = dheight;
            } else {
                toheight = dheight;
                //towidth = (width * dheight) / height;
                towidth = dwidth;
            }
        } else {
            towidth = width;
            toheight = height;
        }

        if (url != null) {
            logger.debug("generate small image url  {}", url);
            File parentPath = new File(url.substring(0, url.lastIndexOf("/")));
            if (!parentPath.exists()) {
                parentPath.mkdirs();
            }
        }

        BufferedImage zoomImage = zoom(src, towidth, toheight);
        ImageIO.write(zoomImage, formatedName, new File(url));


        src = null;
    }


    /**
     * 压缩图片
     *
     * @param sourceImage 待压缩图片
     * @param width       压缩图片高度
     * @param height       压缩图片宽度
     */
    private static BufferedImage zoom(BufferedImage sourceImage, int width, int height) {
        BufferedImage zoomImage = new BufferedImage(width, height, sourceImage.getType());
        Image image = sourceImage.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        Graphics gc = zoomImage.getGraphics();
        gc.setColor(Color.WHITE);
        gc.drawImage(image, 0, 0, null);
        return zoomImage;
    }

    /**
     * 对图片裁剪，并把裁剪完的新图片保存
     *
     */
    @RequestMapping(value = "/p/cropPhoto")
    public
    @ResponseBody
    String cropPhoto(Double width, Double height, Double marginLeft, Double marginTop) {
    	if (width == null || height == null || marginLeft == null || marginTop == null) {
            return Constants.FAIL;
        }
        String fileName = "";
        String srcpath = "";
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
        // 图片路径名称如: 判断是否上传了
        UserDetail userDetail = userDetailService.getUserDetail(userName);
        if(AppUtils.isBlank(userDetail.getUpdatePic())){
        	srcpath = userDetail.getPortraitPic();
        }else{
        	srcpath = userDetail.getUpdatePic();
        	//不管截图成不成，先保存，避免坏图
        	userDetail.setPortraitPic(srcpath);
        	userDetailService.updateUserDetail(userDetail);
        }
        try {
            fileName = cutImageWithScale(srcpath, marginLeft, marginTop, width, height);
            if (AppUtils.isNotBlank(fileName)) {
            	 userDetail.setPortraitPic(fileName);
                 userDetail.setUpdatePic(null);//保存完，清除修改头像的路径
                 userDetailService.updateUserDetail(userDetail);
            }
        } catch (Exception e) {
            logger.error("cropPhoto error", e);
            return Constants.FAIL;
        }
        
        if(AppUtils.isBlank(fileName)){
        	return srcpath;
        }else
        	return fileName;
    }

    /**
     * 前往我的等级页面
     *
     */
    @RequestMapping(value = "/p/usergrade")
    public String usergrade() {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
        UserGrade userGrade = userCenterPersonInfoService.getUserGrade(userId);
        String nextGradeName = userCenterPersonInfoService.getNextGradeName(userGrade.getGradeId());
        request.setAttribute("userGrade", userGrade);
        request.setAttribute("nextGradeName", nextGradeName);
        return PathResolver.getPath(UserCenterFrontPage.USER_GRADE);
    }

    /**
     * 前往分享设置页面
     *
     */
    @RequestMapping(value = "/p/share")
    public String share() {
        return PathResolver.getPath(UserCenterFrontPage.USER_SHARE);
    }

    /**
     * 前往账户安全
     *
     */
    @RequestMapping("/p/security")
    public String security() {
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
        UserInformation userInfo = userDetailService.getUserInfo(userName);
        if (userInfo == null) {
            userDetailService.saveDefaultUserSecurity(userName);
            userInfo = userDetailService.getUserInfo(userName);
        }
        //处理手机号码
        if (AppUtils.isNotBlank(userInfo.getUserMobile())) {
            String mobile = mobileSubstitution(userInfo.getUserMobile());
            userInfo.setUserMobile(mobile);
        }

        //处理邮箱号码
        if (AppUtils.isNotBlank(userInfo.getUserMail())) {
            String mail = mailSubstitution(userInfo.getUserMail());
            userInfo.setUserMail(mail);
        }

        request.setAttribute("userInfo", userInfo);
        return PathResolver.getPath(UserCenterFrontPage.ACCOUNT_SECURITY);
    }

    /**
     * 收货地址
     *
     */
    @RequestMapping(value = "/p/receiveaddress")
    public String receiveaddress(String curPageNO) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
        PageSupport<UserAddress> ps = userAddressService.getUserAddress(curPageNO, userName);
        List<UserAddress> resultList = (List<UserAddress>) ps.getResultList();
        if (AppUtils.isNotBlank(resultList)) {
            List<UserAddress> list = new ArrayList<UserAddress>(resultList.size());
            for (UserAddress userAddress : resultList) {
                if (AppUtils.isBlank(userAddress.getAliasAddr())) {
                    Area area = userAddressService.getAreaById(userAddress.getAreaId());
                    if (area != null) {
                        userAddress.setAliasAddr(userAddress.getReceiver() + " - " + area.getArea());
                    }
                }
                list.add(userAddress);
            }
            ps.setResultList(list);
        }
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("total", ps.getTotal());
        return PathResolver.getPath(UserCenterFrontPage.RECEIVE_ADDRESS);  //simple	
    }

    /**
     * 查询用户绑定的第三方账号, TODO 此方法要改造
     */
    @RequestMapping(value = "/p/accountBinding")
    public String accountBinding(String curPageNO) {
    	SecurityUserDetail user = UserManager.getUser(request);
    	//查询一个用户已绑定的通行证
        List<Passport> passportList = passportService.getPassport(user.getUserId());

        request.setAttribute("passportDtoList", passportList);
        return PathResolver.getPath(UserCenterFrontPage.ACCOUNT_BINDING);
    }


    @RequestMapping(value = "/p/unbinding")
    public
    @ResponseBody
    String unbinding(String type) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
        if ("weixin".equals(type)) {
            passportService.unbindWeixinAccount(userId);
        } else {
            passportService.deletePassport(userId, type);
        }
        return Constants.SUCCESS;
    }

    /**
     * 加载地址弹框,如果为0则是新增
     *
     */
    @RequestMapping("/p/loadaddressOverlay/{addrId}")
    public String loadaddressOverlay(@PathVariable Long addrId) {
        if (addrId != 0) {
            UserAddress userArrress = userAddressService.getUserAddress(addrId);
            request.setAttribute("userArrress", userArrress);
        }
        return PathResolver.getPath(UserCenterFrontPage.LOAD_ADDRESS_OVERLAY);
    }

    /**
     * 保存地址
     *
     */
    @RequestMapping(value = "/p/saveaddress", method = RequestMethod.POST)
    public
    @ResponseBody
    String saveAddress(UserAddress userAddress) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
        if (checkUserAddress(userAddress)) {//检查非空字段
            return Constants.FAIL;
        } else if (AppUtils.isBlank(userAddress.getAddrId()) && checkMaxNumber(userName)) {
            return Constants.MAX_NUM;
        }
        if (userAddress.getAddrId() == null) { //for save
            userAddress.setUserName(userName);
            userAddress.setUserId(userId);
            userAddress.setCreateTime(new Date());
            userAddress.setCommonAddr("0");
            userAddress.setVersion(1l);
            userAddress.setModifyTime(new Date());
            userAddress.setSubPost(userAddress.getSubPost());
        }
        userAddress.setStatus(1);
        userAddressService.saveUserAddress(userAddress, userId);
        return Constants.SUCCESS;
    }


    /**
     * 删除地址
     *
     * @param addrId
     * @return
     */
    @RequestMapping(value = "/p/deladdress/{addrId}")
    public String deladdress(@PathVariable Long addrId) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
        UserAddress userAddress = userAddressService.getUserAddress(addrId);
        if (userAddress != null) {
            if (userName.equals(userAddress.getUserName())) {
                userAddressService.deleteUserAddress(userAddress);
            }
        }

        return PathResolver.getPath(UserCenterFrontPage.RECEIVE_ADDRESS);
    }

    /**
     * 默认地址
     *
     * @param addrId
     * @return
     */
    @RequestMapping(value = "/p/defaultaddress/{addrId}")
    public
    @ResponseBody
    String defaultaddress(@PathVariable Long addrId) {
    	SecurityUserDetail user = UserManager.getUser(request);
        //将此id的地址设为默认
        UserAddress userAddress = userAddressService.getUserAddress(addrId);
        if (userAddress != null && user.getUserId().equals(userAddress.getUserId())) {
            userAddressService.updateDefaultUserAddress(addrId, user.getUserId());
            return Constants.SUCCESS;
        } else {
            return Constants.FAIL;
        }
    }

    /**
     * 身份验证
     *
     * @return
     */
    @RequestMapping(value = "/p/establishIdentity/{toPage}/{secLevel}")
    public String establishIdentity(@PathVariable String toPage, @PathVariable Integer secLevel) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
        UserSecurity userSecurity = userDetailService.getUserSecurity(userName);
        request.setAttribute("toPage", toPage);
        request.setAttribute("userSecurity", userSecurity);
        if (secLevel == 1) {
            //安全等级为1，先去往密码验证页面,根据userId查出用户password
            return PathResolver.getPath(UserCenterFrontPage.PASSWORD_VERIFICATION);
        } else if (secLevel == 2) {
            //安全等级为2，按顺序应去往邮箱验证页面 

            //如果手机已验证好，去手机的验证页面
            if (userSecurity.getPhoneVerifn() == 1) {
                String userMobile = userDetailService.getUserMobile(userId);
                request.setAttribute("userMobile", userMobile);
                request.setAttribute("userSecMobile", mobileSubstitution(userMobile));
                return PathResolver.getPath(UserCenterFrontPage.ESTABLISH_IDENTITY);
            }
            //如果点击的是开通手机，是去到验证密码而不是验证邮箱
            if (toPage.equals("toUpdateMobile")) {
                return PathResolver.getPath(UserCenterFrontPage.PASSWORD_VERIFICATION);
            }

            //根据userId查出用户邮箱
            String usermail = mailSubstitution(userDetailService.getUserEmail(userId));
            request.setAttribute("userMail", usermail);
            return PathResolver.getPath(UserCenterFrontPage.EMAIL_VERIFICATION);
        } else {//手机验证
            String userMobile = userDetailService.getUserMobile(userId);
            request.setAttribute("userSecMobile", mobileSubstitution(userMobile));
            request.setAttribute("userMobile", userMobile);
            return PathResolver.getPath(UserCenterFrontPage.ESTABLISH_IDENTITY);
        }
    }

    /**
     * 身份验证
     *
     * @return
     */
    @RequestMapping(value = "/p/mailIdentity/{toPage}")
    public String mailIdentity(@PathVariable String toPage) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
        UserSecurity userSecurity = userDetailService.getUserSecurity(userName);
        request.setAttribute("toPage", toPage);
        request.setAttribute("userSecurity", userSecurity);
        //根据userId查出用户邮箱
        String usermail = mailSubstitution(userDetailService.getUserEmail(userId));
        request.setAttribute("userMail", usermail);
        return PathResolver.getPath(UserCenterFrontPage.EMAIL_VERIFICATION);
    }

    /**
     * 比较密码
     *
     */
    @RequestMapping(value = "/p/passwordComparison", method = RequestMethod.POST)
    public
    @ResponseBody
    boolean passwordComparison(String password) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
        String userpd = userDetailService.getUserPassword(userId);
        if (!passwordEncoder.matches(password, userpd)) {
            return false;
        }
        return true;
    }

    /**
     * 发送邮件
     *
     */
    @RequestMapping(value = "/p/confirmationMail", method = RequestMethod.POST)
    public String confirmationMail(String toPage) {
		SecurityUserDetail securityUserDetail = UserManager.getUser(request);
		String userId = securityUserDetail.getUserId();
		String userName = securityUserDetail.getUsername();

        String userEmail = userDetailService.getUserEmail(userId);

        User user = userDetailService.getUser(userId);
        String nickName = UserManager.getNickName(securityUserDetail);
        user.setNickName(nickName);
        //获得验证码
        String code = getRandomString(50);


        //发邮件
        if ("toUpdatePassword".equals(toPage)) {
            mailManager.updatePassword(user, userEmail, code);
        } else if ("toUpdatePaymentpassword".equals(toPage)) {
            mailManager.updatePaymentpassword(user, userEmail, code);
        } else {
            mailManager.confirmationMail(user, userEmail, code);
        }

        //将邮箱及验证码 更新到 ls_usr_security
        userDetailService.updateSecurityMail(userName, code, userEmail);

        String updateEmail = mailSubstitution(userDetailService.getSecurityEmail(userName));
        request.setAttribute("updateEmail", updateEmail);
        return PathResolver.getPath(UserCenterFrontPage.SEND_MAIL_SUCCESS);
    }

    /**
     * 发送短信
     *
     */
    @RequestMapping(value = "/p/verifyMessage", method = RequestMethod.POST)
    public
    @ResponseBody
    String verifyMessage(String mobile, String securityCode, String randNum) {

        //检查图片验证码是否正确
        if (!ValidateCodeUtil.validateCodeAndKeep(randNum, request.getSession())) {
            return Constants.ERROR_CODE;
        }

        //检查发送频率
        //将验证码存入数据库
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
        UserSecurity userSecurity = userDetailService.getUserSecurity(userName);
        Date nows = new Date();
        Date lastSendTime = userSecurity.getSendSmsTime();
        if (lastSendTime != null && nows.getTime() - lastSendTime.getTime() < 1000*60) { //1分钟内
            return Constants.LIMIT;
        }

        String mobileCode = CommonServiceUtil.getRandomSMSCode();

		String ip = "";
		try {
			ip = IPHelper.getIpAddr(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("UserCenterPersonInfoController verifyMessage 发送短信验证码 mobile：{}，ip：{}", mobile, ip);
		//验证码正确，则更新验证码
		if (AppUtils.isNotBlank(securityCode)) {
			if (userSecurity.getValidateCode().equals(securityCode) && lastSendTime != null && nows.getTime() - lastSendTime.getTime() < 1000 * 300) { //验证码正确并且在5分钟内（即300秒）
				sendSMSProcessor.process(new SendSMSEvent(mobile, mobileCode, userName, "/p/verifyMessage", ip, new Date()).getSource());
				//开始发送短信
				userDetailService.updateUserSecurity(userName, mobileCode);
				//将验证码传到页面
				return Constants.SUCCESS;
			} else {
				return Constants.ERROR_SECURITY_CODE;
			}
		} else {
			sendSMSProcessor.process(new SendSMSEvent(mobile, mobileCode, userName, "/p/verifyMessage", ip, new Date()).getSource());
			//开始发送短信
			userDetailService.updateUserSecurity(userName, mobileCode);
			return Constants.SUCCESS;
		}
	}

    /**
     * 验证验证码是否正确
     *
     */
    @RequestMapping(value = "/p/verifySMSCode", method = RequestMethod.POST)
    @ResponseBody
    public Boolean verifySMSCode(String validateCode) {
        
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
        
        //校验短信验证码
        boolean test =  userCenterPersonInfoService.verifySMSCode(userName, validateCode);
        if(test){
			UserDetail userDetail = userDetailService.getUserDetail(userName);
			//判断用户和手机是否为空
			if(AppUtils.isNotBlank(userDetail) && AppUtils.isNotBlank(userDetail.getUserMobile())){
				//详细校验安全码
				test = smsLogService.verifyMobileCode(validateCode,userDetail.getUserMobile());
			}else
				test = false;
			
		}
		
		return test;
    }
    
    /**
     * 更新手机验证验证码和手机号码是否正确
     *
     */
    @RequestMapping(value = "/p/verifyMobileCode", method = RequestMethod.POST)
    public
    @ResponseBody
    Boolean verifyMobileCode(String validateCode,String mobile) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
        boolean test =  userCenterPersonInfoService.verifySMSCode(userName, validateCode);
        if(test){
        	//判断手机号码和验证码是否匹配
        	test = smsLogService.verifyMobileCode(validateCode,mobile);
        }
        return test;
    }

    /**
     * 检查邮箱是否存在,除了自己以外
     *
     */
    @RequestMapping(value = "/p/isEmailOnly", method = RequestMethod.POST)
    public
    @ResponseBody
    Boolean isEmailOnly(String email) {
		SecurityUserDetail user = UserManager.getUser(request);
        return userDetailService.isEmailOnly(email, user.getUsername());
    }

    /**
     * 检查手机号码是否存在,除了自己以外。也就是手机是否唯一
     *
     */
    @RequestMapping(value = "/isPhoneOnly", method = RequestMethod.POST)
    public
    @ResponseBody
    Boolean isPhoneOnly(String phone) {
        SecurityUserDetail user = UserManager.getUser(request);
        return userDetailService.isPhoneOnly(phone, user.getUsername());
    }

    /**
     * 更新密码
     *
     */
    @RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
    public String updatePassword(String oldPassword, String newPassword, String securityCode, String emailCode, String userName) {
		SecurityUserDetail securityUserDetail = UserManager.getUser(request);
		String userId = securityUserDetail.getUserId();
        if (AppUtils.isBlank(userName)) {
            userName = securityUserDetail.getUsername();
        }
        User user = userDetailService.getUser(userId);
        if (checkPassword(user.getPassword(), oldPassword) || checkEmailCode(userName, emailCode) || checkSecurityCode(userName, securityCode)) {
            user.setPassword(passwordEncoder.encode(newPassword));
            userDetailService.updateUser(user);

            Integer secLevel = userDetailService.getSecLevel(userName);
            request.setAttribute("secLevel", secLevel);
            return PathResolver.getPath(UserCenterFrontPage.FINISH);
        } else {
            return PathResolver.getPath(UserCenterFrontPage.FAIL);
        }

    }


    /**
     * 验证邮箱、更新邮箱
     *
     */
    @RequestMapping(value = "/updateEmail", method = RequestMethod.POST)
    public String updateEmail(String oldPassword, String userEmail, String emailCode, String userName, String securityCode) {
		SecurityUserDetail securityUserDetail = UserManager.getUser(request);
		String userId = securityUserDetail.getUserId();
        if (AppUtils.isBlank(userName)) {
            userName = securityUserDetail.getUsername();
        }
        User user = userDetailService.getUser(userId);
        String nickName = UserManager.getNickName(securityUserDetail);
        user.setNickName(nickName);
        //检查是否有同名的邮箱
        if (userDetailService.isEmailOnly(userEmail, userName)) {
            return Constants.MAIL_EXISTS;
        }

        if (checkPassword(user.getPassword(), oldPassword) || checkEmailCode(userName, emailCode) || checkSecurityCode(userName, securityCode)) {
            //获得随机码
            String validateCode = getRandomString(50);

            //发送邮件到邮箱去验证
            mailManager.updateMail(user, userEmail, validateCode);

            //将邮箱及邮箱验证码 更新到 ls_usr_security
            userDetailService.updateSecurityMail(userName, validateCode, userEmail);


            //获取邮箱的类型
            String officialEmail = mailHelper.getMailAddress(userEmail.substring(userEmail.indexOf("@") + 1));
            //如果 officialEmail 为空，则前台不显示跳转邮箱按钮
            request.setAttribute("officialEmail", officialEmail);

            UserSecurity userSecurity = userDetailService.getUserSecurity(userName);
            request.setAttribute("userEmail", mailSubstitution(userSecurity.getUpdateMail()));
            return PathResolver.getPath(UserCenterFrontPage.SEND_MAIL_SUCCESS);
        } else {
            return PathResolver.getPath(UserCenterFrontPage.FAIL);
        }
    }

    /**
     * 验证,更新手机
     *
     */
    @RequestMapping(value = "/p/updateMobile", method = RequestMethod.POST)
    public String updateMobile(String userMobile, String oldPassword, String securityCode) {
		SecurityUserDetail securityUserDetail = UserManager.getUser(request);
		String userId = securityUserDetail.getUserId();
		String userName = securityUserDetail.getUsername();
        User user = userDetailService.getUser(userId);
        Integer PhoneVerifn = userDetailService.getPhoneVerifn(userName);
        if (checkPassword(user.getPassword(), oldPassword) || checkSecurityCode(userName, securityCode)) {
            userDetailService.updateUserMobile(userMobile, userName);

            if (PhoneVerifn == 0) {
                //该用户安全等级+1,手机验证变为1
                userDetailService.updatePhoneVerifn(userName);
    			//验证手机送积分
    			integralService.addScore(userId,SendIntegralRuleEnum.VAL_PHONE);
            }

            Integer secLevel = userDetailService.getSecLevel(userName);
            request.setAttribute("secLevel", secLevel);
            return PathResolver.getPath(UserCenterFrontPage.FINISH);
        } else {
            return PathResolver.getPath(UserCenterFrontPage.FAIL);
        }
    }

    /**
     * 验证,更新支付密码
     */
    @RequestMapping(value = "/p/updatePaymentpassword", method = RequestMethod.POST)
    public String updatePaymentPassword(String paymentPassword, String securityCode, Integer payStrength, String emailCode) {
    	SecurityUserDetail userDetail = UserManager.getUser(request);
    	String userName = userDetail.getUsername();
        String userId = userDetail.getUserId();
        User user = userDetailService.getUser(userId);
        if (checkSecurityCode(userName, securityCode) || checkEmailCode(userName, emailCode)) {
            userCenterPersonInfoService.updatePaymentPassword(user.getName(), passwordEncoder.encode(paymentPassword), payStrength);

            Integer paypassVerifn = userDetailService.getPaypassVerifn(userName);
            if (paypassVerifn == 0) {
                //该用户安全等级+1,支付密码验证变为1
                userDetailService.updatePaypassVerifn(userName);
            }

            Integer secLevel = userDetailService.getSecLevel(userName);
            request.setAttribute("secLevel", secLevel);
            return PathResolver.getPath(UserCenterFrontPage.FINISH);
        } else {
            return PathResolver.getPath(UserCenterFrontPage.FAIL);
        }
    }

    /**
     * 去往更新密码页面
     *
     */
    @RequestMapping(value = "/p/toUpdatePassword")
    public String toUpdatePassword(String oldPassword, String securityCode) {
        request.setAttribute("oldPassword", oldPassword);
        request.setAttribute("securityCode", securityCode);
        return PathResolver.getPath(UserCenterFrontPage.UPDATE_PASSWORD);
    }

    /**
     * 去往验证邮箱页面
     *
     */
    @RequestMapping(value = "/p/toUpdateMail")
    public String toUpdateMail(String oldPassword, String securityCode) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
        Integer mailVerifn = userDetailService.getMailVerifn(userName);
        request.setAttribute("mailVerifn", mailVerifn);
        request.setAttribute("oldPassword", oldPassword);
        request.setAttribute("securityCode", securityCode);
        return PathResolver.getPath(UserCenterFrontPage.TO_UPDATE_MAIL);
    }

    /**
     * 通过邮箱中邮件的连接进入修改邮箱页面
     *
     */
    @RequestMapping(value = "/toUpdateMail")
    public String toUpdateFromMail(String oldPassword, String securityCode) {
        request.setAttribute("oldPassword", oldPassword);
        request.setAttribute("securityCode", securityCode);
        return PathResolver.getPath(UserCenterFrontPage.TO_UPDATE_MAIL);
    }

    /**
     * 去往更新手机页面
     *
     */
    @RequestMapping(value = "/p/toUpdateMobile")
    public String toUpdateMobile(String oldPassword, String securityCode) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
        Integer phoneVerifn = userDetailService.getPhoneVerifn(userName);
        request.setAttribute("phoneVerifn", phoneVerifn);

        request.setAttribute("oldPassword", oldPassword);
        request.setAttribute("securityCode", securityCode);
        String userMobile = userDetailService.getUserMobile(userId);
        request.setAttribute("userMobile", userMobile);
        return PathResolver.getPath(UserCenterFrontPage.TO_UPDATE_MOBILE);
    }

    /**
     * 去往支付密码管理页面
	 *
     */
    @RequestMapping(value = "/p/toUpdatePaymentpassword")
    public String toUpdatePaymentpassword(String securityCode) {
    	
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();

        if (AppUtils.isNotBlank(securityCode)) {
            boolean result = userCenterPersonInfoService.verifySMSCode(userName, securityCode);
            if (!result) {
                throw new PermissionException("securityCode error", ErrorCode.PARAMETER_ERROR);
            }
            request.setAttribute("securityCode", securityCode);
        }

        Integer paypassVerifn = userDetailService.getPaypassVerifn(userName);
        request.setAttribute("paypassVerifn", paypassVerifn);
        return PathResolver.getPath(UserCenterFrontPage.UPDATE_PAYMENT_PASSWORD);
    }


    public void setMailManager(MailManager mailManager) {
        this.mailManager = mailManager;
    }

    /**
	    * 修改密码前检查用户名密码跟原来的密码是否一致
	    * 包括用户名密码或者是验证码的检查
     *
     * newPassword 
     */
    private boolean checkPassword(String encodedPassword, String rawPassword) {
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }

    /**
     * 检查手机验证码和数据库中的是否一致
     *
     * @param userName
     * @param securityCode
     * @return
     */
    private boolean checkSecurityCode(String userName, String securityCode) {
    	//当关闭验证码时，所有的验证码默认有效。正式环境不能开启。
		if (systemParameterUtil.getCloseSmsValidateCode()) {
			return true;
		}
        if (AppUtils.isBlank(securityCode)) {
            return false;
        }
        UserSecurity userSecurity = userDetailService.getUserSecurity(userName);
        if (userSecurity == null || userSecurity.getValidateCode() == null) {
            return false;
        }
        return userSecurity.getValidateCode().equals(securityCode);
    }

    /**
     * 检查邮件验证码和数据库中的是否一致
     *
     * @param userName
     * @param emailCode
     * @return
     */
    private boolean checkEmailCode(String userName, String emailCode) {
        if (AppUtils.isBlank(emailCode)) {
            return false;
        }
        UserSecurity userSecurity = userDetailService.getUserSecurity(userName);
        if (userSecurity == null || userSecurity.getEmailCode() == null) {
            return false;
        }
        return userSecurity.getEmailCode().equals(emailCode);
    }

    /**
     * 页面加密手机号码，保留前3个和后3个
     *
     */
    private String mobileSubstitution(String mobile) {
        if (mobile == null) {
            return null;
        }
        if (mobile.length() < 11) {
            return mobile;
        }
        return mobile.replaceAll(mobile.substring(3, mobile.length() - 3), "******");
    }


    private String mailSubstitution(String mail) {

    	if(mail == null){
			logger.info("mail is null");
    		return null;
		}

        int pos = mail.indexOf("@");
        String preFix = mail.substring(0, pos);
        String postFix = mail.substring(pos, mail.length());
        int mailPrefixLength = preFix.length();
        StringBuilder sb = new StringBuilder();
        if (mailPrefixLength < 3) {
            for (int i = 0; i < mailPrefixLength; i++) {
                sb.append("*");
            }
        } else {
            sb.append(preFix.subSequence(0, 1));
            for (int i = 1; i < mailPrefixLength - 1; i++) {
                sb.append("*");
            }
            sb.append(preFix.subSequence(mailPrefixLength - 1, mailPrefixLength));
        }
        return sb.toString() + postFix;
    }

    private boolean checkMaxNumber(String userName) {
        Long MaxNumber = userAddressService.getMaxNumber(userName);
        if (MaxNumber >= 20) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkUserAddress(UserAddress userAddress) {
        if (AppUtils.isNotBlank(userAddress.getEmail())) {
            if (!ValidationUtil.checkEmail(userAddress.getEmail())) {
                return true;
            }
        }

        if (AppUtils.isBlank(userAddress.getReceiver()) || AppUtils.isBlank(userAddress.getSubAdds()) ||
                (!ValidationUtil.checkPhone(userAddress.getMobile()) && AppUtils.isBlank(userAddress.getTelphone()))
                ) {
            return true;
        }

        return false;
    }

    //随机产生的邮箱验证码
    private String getRandomString(int length) {
        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

    /**
     * 图片切割
     *
     * @param imagePath 原图地址
     * @param x         目标切片坐标 X轴起点
     * @param y         目标切片坐标 Y轴起点
     * @param w         目标切片 宽度
     * @param h         目标切片 高度
     */
    @Deprecated
    private void cutImage(String imagePath, int x, int y, int w, int h) {
        try {
            Image img;
            ImageFilter cropFilter;
            // 读取源图像  
            BufferedImage bi = ImageIO.read(new File(imagePath));
            int srcWidth = bi.getWidth();      // 源图宽度  
            int srcHeight = bi.getHeight();    // 源图高度  

            //若原图大小大于切片大小，则进行切割  
            if (srcWidth >= w && srcHeight >= h) {
                Image image = bi.getScaledInstance(srcWidth, srcHeight, Image.SCALE_DEFAULT);

                int x1 = x * srcWidth / 300;
                int y1 = y * srcHeight / 300;
                int w1 = w * srcWidth / 300;
                int h1 = h * srcHeight / 300;

                cropFilter = new CropImageFilter(x1, y1, w1, h1);
                img = Toolkit.getDefaultToolkit().createImage(new FilteredImageSource(image.getSource(), cropFilter));
                BufferedImage tag = new BufferedImage(w1, h1, BufferedImage.TYPE_INT_RGB);
                Graphics g = tag.getGraphics();
                g.drawImage(img, 0, 0, null); // 绘制缩小后的图  
                g.dispose();
                // 输出为文件  
                ImageIO.write(tag, "JPEG", new File(imagePath));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 图片切割
     * @update  2018.7.11 增加oss裁剪
     * @param imagePath 原图地址
     * @param xScale    目标切片坐标 X轴起点 比例
     * @param yScale    目标切片坐标 Y轴起点 比例
     * @param wScale    目标切片 宽度 比例
     * @param hScale    目标切片 高度 比例
     * @return
     */
    private String cutImageWithScale(String imagePath, double xScale, double yScale, double wScale, double hScale) {
    	
    	String fileName = null;
    	String attachmentType = propertiesUtil.getAttachmentType();
		// 是否使用阿里云的oss
		if (PhotoPathEnum.OSS.name().equalsIgnoreCase(attachmentType)) {
			//判断是否重新上传的图
			int indexOf = imagePath.indexOf("?");
			if(indexOf != -1){
				imagePath = imagePath.substring(0, indexOf);
			}
			//获取图片实际宽高
			Attachment attachment = attachmentService.getAttachmentByFilePath(imagePath);
			if(AppUtils.isNotBlank(attachment)){
				int w1 = (int) (attachment.getWidth() * wScale);//起点宽度
	            int h1 = (int) (attachment.getHeight() * hScale);//起点高度
	            int x1 = (int) (attachment.getWidth() * xScale);//裁剪宽度
                int y1 = (int) (attachment.getHeight() * yScale);//裁剪高度
	          //拼接oss的裁剪
				fileName = String.format("%s?x-oss-process=image/crop,x_%s,y_%s,w_%s,h_%s", imagePath,x1,y1,w1,h1) ; 
			}	
			return fileName;
		}
		//photoServer裁剪
        try {
            Image img;
            ImageFilter cropFilter;
            // 读取源图像  
            BufferedImage bi = null;

            String contextPahtString = SystemUtil.getContextPath();
            String photoServer = propertiesUtil.getInnerPhotoServer();
            if (!photoServer.contains(contextPahtString) || !photoServer.equals(contextPahtString)) {
                String urlString = photoServer + ResourcePathUtil.getPhotoPathPrefix() + imagePath;
                URL url = new URL(urlString);
                bi = ImageIO.read(url);
            } else {
            	
                bi = ImageIO.read(new File(propertiesUtil.getBigPicPath() + "/" + imagePath));
            }

            int srcWidth = bi.getWidth();      // 源图宽度  
            int srcHeight = bi.getHeight();    // 源图高度  

            int newWidth = (int) (srcWidth * wScale);
            int newHeight = (int) (srcHeight * hScale);

            //若原图大小大于切片大小，则进行切割  
            if (srcWidth >= newWidth && srcHeight >= newHeight) {
                Image image = bi.getScaledInstance(srcWidth, srcHeight, Image.SCALE_DEFAULT);

                int w1 = newWidth;
                int h1 = newHeight;
                int x1 = (int) (srcWidth * xScale);
                int y1 = (int) (srcHeight * yScale);

                cropFilter = new CropImageFilter(x1, y1, w1, h1);
                img = Toolkit.getDefaultToolkit().createImage(new FilteredImageSource(image.getSource(), cropFilter));
                BufferedImage tag = new BufferedImage(w1, h1, BufferedImage.TYPE_INT_RGB);
                Graphics g = tag.getGraphics();
                g.drawImage(img, 0, 0, null); // 绘制缩小后的图  
                g.dispose();
                // 输出为文件  
                ByteArrayOutputStream oStream = new ByteArrayOutputStream();
                ImageIO.write(tag, "JPEG", oStream);
                
                FeignMultipartFile file = new FeignMultipartFile("user.jpg", "user.jpg", oStream.toByteArray());
                fileName = attachmentManager.upload(file);
                attachmentManager.deleteTruely(imagePath);
                attachmentManager.delAttachmentByFilePath(imagePath);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileName;
    }
    
    /**
	 * Update user info.
	 *
	 * @param request      the request
	 * @param response     the response
	 * @param myPersonInfo the my person info
	 * @return the int
	 */
	@RequestMapping(value = "/p/user/updateUserInfo", method = RequestMethod.POST)
	public
	@ResponseBody
	int updateUserInfo(HttpServletRequest request, HttpServletResponse response, MyPersonInfo myPersonInfo) {
		myPersonInfo.setIdCard(XssFilterUtil.cleanXSS(myPersonInfo.getIdCard()));
		myPersonInfo.setUserAdds(XssFilterUtil.cleanXSS(myPersonInfo.getUserAdds()));
		myPersonInfo.setQq(XssFilterUtil.cleanXSS(myPersonInfo.getQq()));
		myPersonInfo.setInterest(XssFilterUtil.cleanXSS(myPersonInfo.getInterest()));

		userCenterPersonInfoService.updateUserInfo(myPersonInfo);
		if (AppUtils.isNotBlank(myPersonInfo.getNickName())) {
			SecurityUserDetail user = UserManager.getUser(request);
			user.setNickName(myPersonInfo.getNickName());
			request.getSession().setAttribute(Constants.USER_NAME, myPersonInfo.getNickName());
		}
		return 0;
	}
}
