/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.business.page.UserCenterFrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ProdCommStatusEnum;
import com.legendshop.model.constant.SendIntegralRuleEnum;
import com.legendshop.model.dto.UserAddCommParamsDto;
import com.legendshop.model.dto.UserCommParamsDto;
import com.legendshop.model.entity.FavoriteShop;
import com.legendshop.model.entity.Myfavorite;
import com.legendshop.model.entity.ProdAddComm;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductComment;
import com.legendshop.model.entity.ProductConsult;
import com.legendshop.model.form.ProductForm;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.FavoriteShopService;
import com.legendshop.spi.service.IntegralService;
import com.legendshop.spi.service.ProductCommentService;
import com.legendshop.spi.service.ProductConsultService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.SensitiveWordService;
import com.legendshop.spi.service.UserCenterService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.SafeHtml;
import com.legendshop.web.helper.IPHelper;

/**
 * 店铺收藏和商品收藏
 */
@Controller
public class MyFavouriteController extends BaseController {
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(MyFavouriteController.class);

	/** The user center service. */
	@Autowired
	private UserCenterService userCenterService;

	@Autowired
	private ProductConsultService productConsultService;

	@Autowired
	private ProductCommentService productCommentService;

	@Autowired
	private FavoriteShopService favoriteShopService;

	@Autowired
	private AttachmentManager attachmentManager;

	@Autowired
	private ProductService productService;

	@Autowired
	private SensitiveWordService sensitiveWordService; // 敏感字过滤
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * 积分服务
	 */
	@Autowired
	private IntegralService integralService;

	/**
	 * 商品收藏
	 *
	 * @param request   the request
	 * @param response  the response
	 * @param curPageNO the cur page no
	 * @return the string
	 */
	@RequestMapping("/p/favourite")
	public String myFavourite(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		if (log.isDebugEnabled()) {
			log.debug("To my favourite page, curPageNO:{}", curPageNO);
		}
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		PageSupport<Myfavorite> ps = userCenterService.getFavoriteList(curPageNO, systemParameterUtil.getPageSize(), userId);
		PageSupportHelper.savePage(request, ps);

		if (log.isDebugEnabled() && ps != null) {
			log.debug("Found total:{}, data list:{}", ps.getTotal(), ps.getResultList());
		}

		return PathResolver.getPath(UserCenterFrontPage.MY_FAVOURITE); // simple
	}

	/**
	 * 关注店铺弹框
	 * 
	 * @param request
	 * @param response
	 * @param shopName
	 * @return
	 */
	@RequestMapping("/p/favoriteShopOverlay")
	public @ResponseBody String addFavoriteShop(HttpServletRequest request, HttpServletResponse response, Long shopId) {
		SecurityUserDetail userDetail = UserManager.getUser(request);
		if (favoriteShopService.isExistsFavoriteShop(userDetail.getUserId(), shopId)) {
			return "isexist";
		}
		FavoriteShop favoriteShop = new FavoriteShop();
		favoriteShop.setShopId(shopId);
		favoriteShop.setUserId(userDetail.getUserId());
		favoriteShop.setUserName(userDetail.getUsername());
		favoriteShop.setRecDate(new Date());
		favoriteShopService.savefavoriteShop(favoriteShop);
		return Constants.SUCCESS;
	}

	/**
	 * 加关注， 去往弹框页面
	 * 
	 * @param request
	 * @param response
	 * @param prodId
	 * @return
	 */
	@RequestMapping("/loadInterestOverlay/{prodId}")
	public String addInterest(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		if (AppUtils.isBlank(user)) {
			return PathResolver.getPath(UserCenterFrontPage.LOGIN_OVERLAY);
		}
		
		String userId = user.getUserId();
		String userName = user.getUsername();

		// 收藏此商品的用户，还收藏其他什么商品
		List<ProductForm> products = productCommentService.getProductList(userId, prodId);
		if (AppUtils.isNotBlank(products)) {
			request.setAttribute("products", products);
		}

		// 该用户是否已关注该商品
		if (userCenterService.isExistsFavorite(prodId, userName)) {
			// 该用户究竟关注过多少商品
			Long favoriteLength = userCenterService.getfavoriteLength(userId);
			request.setAttribute("favoriteLength", favoriteLength);
			return PathResolver.getPath(UserCenterFrontPage.ISEXISTS_INTEREST);
		} else {
			// 让该用户将该商品设为关注
			Myfavorite myfavorite = new Myfavorite();
			myfavorite.setAddtime(new Date());
			myfavorite.setUserId(userId);
			myfavorite.setUserName(userName);
			myfavorite.setProdId(prodId);
			userCenterService.saveFavorite(myfavorite);

			// 该用户究竟关注过多少商品
			Long favoriteLength = userCenterService.getfavoriteLength(userId);
			request.setAttribute("favoriteLength", favoriteLength);

			return PathResolver.getPath(UserCenterFrontPage.INTEREST_OVERLAY);
		}
	}

	/**
	 * 对弹框中别人关注过的产品，加关注
	 * 
	 * @param request
	 * @param response
	 * @param prodId
	 * @return
	 */
	@RequestMapping(value = "/p/addMarks", method = RequestMethod.POST)
	public @ResponseBody String addMarks(HttpServletRequest request, HttpServletResponse response, Long prodId) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		// 该用户是否已关注该商品
		if (userCenterService.isExistsFavorite(prodId, userName)) {
			return Constants.FAIL;
		} else {
			// 让该用户将该商品设为关注
			Myfavorite myfavorite = new Myfavorite();
			myfavorite.setAddtime(new Date());
			myfavorite.setUserId(userId);
			myfavorite.setUserName(userName);
			myfavorite.setProdId(prodId);
			userCenterService.saveFavorite(myfavorite);
			return Constants.SUCCESS;
		}

	}

	/**
	 * 删除关注.
	 *
	 * @param request      the request
	 * @param response     the response
	 * @param selectedFavs the selected favs
	 * @return the string
	 * @throws Exception the exception
	 */
	@RequestMapping("/p/delfavourite")
	public @ResponseBody String deleteFavourite(HttpServletRequest request, HttpServletResponse response, String selectedFavs) throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("To delete selectedFavs:{}", selectedFavs);
		}
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		if (selectedFavs != null) {
			this.userCenterService.deleteFavs(userId, selectedFavs);
			return Constants.SUCCESS;
		} else {
			return Constants.FAIL;
		}

	}

	/**
	 * 清除关注.
	 *
	 * @param request  the request
	 * @param response the response
	 * @return the string
	 * @throws Exception the exception
	 */
	@RequestMapping("/p/clrfavourite")
	public @ResponseBody String clearFavourite(HttpServletRequest request, HttpServletResponse response) throws Exception {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		if (log.isDebugEnabled()) {
			log.debug("To delete all Favs for user with id:{}", userId);
		}
		try {
			this.userCenterService.deleteAllFavs(userId);
			return Constants.SUCCESS;
		} catch (Exception e) {
			log.error("Failed to clear my favourite", e);
			return Constants.FAIL;
		}
	}

	/**
	 * 产品评论
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/p/prodcomment/{subId}")
	public String prodcomment(HttpServletRequest request, HttpServletResponse response, String curPageNO, @PathVariable Long subId) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		PageSupport<ProductComment> ps = productCommentService.query(curPageNO, userName, subId);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(UserCenterFrontPage.PROD_COMMENT); // simple
	}

	/**
	 * 获取评论的商品
	 * 
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/p/prodcomment")
	public String prodcomment(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		PageSupport<ProductComment> ps = productCommentService.query(curPageNO, userName);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(UserCenterFrontPage.PROD_COMMENT); // simple
	}

	/**
	 * 对商品进行评论
	 * 
	 * @param prodId
	 * @param score
	 * @param content
	 * @return
	 */
	@RequestMapping(value = "/p/comments/save", method = RequestMethod.POST)
	@ResponseBody
	public String saveComments(HttpServletRequest request, HttpServletResponse response, UserCommParamsDto params) {

		try {
			SecurityUserDetail user = UserManager.getUser(request);
			String userId = user.getUserId();
			String userName = user.getUsername();

			// 检查评分是否合法
			Integer score = params.getProdScore();
			Integer shopScore = params.getShopScore();
			Integer logisticsScore = params.getLogisticsScore();
			if (AppUtils.isBlank(score) || score <= 0 || score > 5) {
				return "对不起, 您的宝贝评分不合法, 请重新选择!";
			}
			if (AppUtils.isBlank(shopScore) || shopScore <= 0 || shopScore > 5) {
				return "对不起, 您的卖家评分不合法, 请重新选择!";
			}
			if (AppUtils.isBlank(logisticsScore) || logisticsScore <= 0 || logisticsScore > 5) {
				return "对不起, 您的物流评分不合法, 请重新选择!";
			}

			Long prodId = params.getProdId();

			Product prod = productService.getProductById(prodId);

			if (null == prod) {
				return "亲, 您评论的商品不存在哦!";
			}

			// 查询用户是否 可以评论此商品
			boolean canComm = productCommentService.canCommentThisProd(params.getProdId(), params.getSubItemId(), userId);
			if (!canComm) {
				return "亲, 您不能评价该商品哦!";
			}

			// 敏感字过滤
			Set<String> findwords = sensitiveWordService.checkSensitiveWords(params.getContent());
			if (AppUtils.isNotBlank(findwords)) {
				return "亲, 您提交的内容含有敏感词 " + findwords + ", 请更正再提交！";
			}

			Long subItemId = params.getSubItemId();

			ProductComment productComment = new ProductComment();

			// 处理匿名评论
			Map<String, Object> handleResult = productCommentService.handleAnonymousComments(userName, params.getIsAnonymous());
			productComment.setIsAnonymous((Integer) handleResult.get("isAnonymous"));
			productComment.setUserName((String) handleResult.get("userName"));

			// 处理评论内容
			SafeHtml safeHtml = new SafeHtml();
			String html = safeHtml.makeSafe(params.getContent().trim());
			StringBuilder nicksb = new StringBuilder();
			int l = html.length();
			for (int i = 0; i < l; i++) {
				char _s = html.charAt(i);
				if (isEmojiCharacter(_s)) {
					nicksb.append(_s);
				}
			}
			productComment.setContent(nicksb.toString()); // 评论内容

			List<MultipartFile> photos = params.getPhotos();
			if (AppUtils.isNotBlank(photos)) {
				if (photos.size() > 9) {
					return "对不起, 评论图片不能超过9张!";
				}
				StringBuilder photosStr = new StringBuilder();
				for (int i = 0, len = photos.size(); i < len; i++) {

					MultipartFile file = photos.get(i);

					// 评论图片1
					if (null != file && file.getSize() > 0) {
						String photoPath = attachmentManager.upload(file);
						if (i == (len - 1)) {
							photosStr.append(photoPath);
						} else {
							photosStr.append(photoPath).append(",");
						}
					}
				}
				productComment.setPhotos(photosStr.toString());
			}

			productComment.setUserId(userId);
			productComment.setSubItemId(subItemId);
			productComment.setProdId(prodId);
			productComment.setOwnerName(prod.getUserName());

			/** 是否需要审核 */
			String result = "";
			Boolean needAudit = systemParameterUtil.isCommentNeedReview();
			if (needAudit) {
				productComment.setStatus(ProdCommStatusEnum.WAIT_AUDIT.value());
				result = "SUCCESS";
			} else {
				productComment.setStatus(ProdCommStatusEnum.SUCCESS.value());
				result = Constants.SUCCESS;
			}
			productComment.setScore(score);// 评论得分
			productComment.setShopScore(shopScore);// 卖家评分
			productComment.setLogisticsScore(logisticsScore);// 物流评分
			productComment.setPostip(IPHelper.getIpAddr(request));
			productComment.setUsefulCounts(0);// 有用的计数
			productComment.setReplayCounts(0);// 回复的次数
			productComment.setIsReply(false);
			productComment.setAddtime(new Date());

			productCommentService.saveProductComment(productComment);

			// 评论商品送积分
			integralService.addScore(userId, SendIntegralRuleEnum.ORDER_COMMENT);

			return result;
		} catch (Exception e) {
			log.error("用户发表评论异常!", e);
			return Constants.FAIL;
		}
	}

	/**
	 * 追加评论
	 * 
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/p/comments/addComm", method = RequestMethod.POST)
	@ResponseBody
	public String saveAddComm(HttpServletRequest request, UserAddCommParamsDto params) {
		try {

			SecurityUserDetail user = UserManager.getUser(request);
			String userId = user.getUserId();

			if (AppUtils.isBlank(params.getProdCommId())) {
				return "对不起, 您提交的参数不合法!";
			}

			if (AppUtils.isBlank(params.getContent())) {
				return "亲, 评论内容不能为空哦!";
			}

			// 敏感字过滤
			Set<String> findwords = sensitiveWordService.checkSensitiveWords(params.getContent());
			if (AppUtils.isNotBlank(findwords)) {
				return "亲, 您提交的内容含有敏感词 " + findwords + ", 请更正再提交！";
			}

			// 查询用户是否可以追评
			String canAdd = productCommentService.isCanAddComment(params.getProdCommId(), userId);
			if (!Constants.SUCCESS.equals(canAdd)) {
				return canAdd;
			}

			ProdAddComm prodAddComm = new ProdAddComm();

			// 处理评论内容
			SafeHtml safeHtml = new SafeHtml();
			String html = safeHtml.makeSafe(params.getContent().trim());
			StringBuilder nicksb = new StringBuilder();
			int l = html.length();
			for (int i = 0; i < l; i++) {
				char _s = html.charAt(i);
				if (isEmojiCharacter(_s)) {
					nicksb.append(_s);
				}
			}
			prodAddComm.setContent(nicksb.toString()); // 评论内容

			List<MultipartFile> photos = params.getPhotos();
			if (AppUtils.isNotBlank(photos)) {
				if (photos.size() > 9) {
					return "对不起, 评论图片不能超过9张!";
				}
				StringBuilder photosStr = new StringBuilder();
				for (int i = 0, len = photos.size(); i < len; i++) {

					MultipartFile file = photos.get(i);

					// 评论图片1
					if (null != file && file.getSize() > 0) {
						String photoPath = attachmentManager.upload(file);
						if (i == (len - 1)) {
							photosStr.append(photoPath);
						} else {
							photosStr.append(photoPath).append(",");
						}
					}
				}
				prodAddComm.setPhotos(photosStr.toString());
			}

			prodAddComm.setProdCommId(params.getProdCommId());

			/** 评论是否需要审核 */
			Boolean needAudit = systemParameterUtil.isCommentNeedReview();

			String result = "";
			if (needAudit) {
				prodAddComm.setStatus(ProdCommStatusEnum.WAIT_AUDIT.value());
				result = "SUCCESS";
			} else {
				prodAddComm.setStatus(ProdCommStatusEnum.SUCCESS.value());
				result = Constants.SUCCESS;
			}
			prodAddComm.setPostip(IPHelper.getIpAddr(request));
			prodAddComm.setIsReply(false);
			prodAddComm.setCreateTime(new Date());

			productCommentService.addProdComm(prodAddComm);

			return result;
		} catch (Exception e) {
			log.error("追加评论异常!", e);
			return Constants.FAIL;
		}

	}

	/**
	 * 是否是表情符号
	 * 
	 * @param codePoint
	 * @return
	 */
	private boolean isEmojiCharacter(char codePoint) {
		return (codePoint == 0x0) || (codePoint == 0x9) || (codePoint == 0xA) || (codePoint == 0xD) || ((codePoint >= 0x20) && (codePoint <= 0xD7FF))
				|| ((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) || ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF));
	}

	/**
	 * 用户查看商品回复
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/p/prodcons")
	public String prodcons(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userName = user.getUsername();
		
		PageSupport<ProductConsult> ps = productConsultService.query(curPageNO, userName);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(UserCenterFrontPage.PROD_CONSULT); // simple
	}

	@RequestMapping(value = "/p/deletemulticonsult")
	public @ResponseBody String deletemulticonsult(HttpServletRequest request, HttpServletResponse response, String ids) {
		List<Long> idList = JSONUtil.getArray(ids, Long.class);
		if (AppUtils.isNotBlank(idList)) {
			for (Long consId : idList) {
				productConsultService.deleteProductConsultByShopUser(consId);
			}
		}
		return Constants.SUCCESS;
	}

	/**
	 * 删除多个评论
	 * 
	 * @param request
	 * @param response
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "/p/deletemulticoment")
	public @ResponseBody String deletemulticoment(HttpServletRequest request, HttpServletResponse response, String ids) {
		List<Long> idList = JSONUtil.getArray(ids, Long.class);
		productCommentService.delete(idList);
		return Constants.SUCCESS;
	}

	/**
	 * 商城收藏
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param favoriteShop
	 * @return
	 */
	@RequestMapping("/p/favoriteShop")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, FavoriteShop favoriteShop) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		PageSupport<FavoriteShop> ps = favoriteShopService.query(curPageNO, userId);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(UserCenterFrontPage.FAVORITE_SHOP); // simple
	}

	/**
	 * 删除店铺商城收藏
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/p/favoriteShop/delete/{id}")
	public @ResponseBody String favoriteShopDel(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		boolean result = favoriteShopService.deletefavoriteShop(id, userId);
		if (result) {
			return Constants.SUCCESS;
		} else {
			return Constants.FAIL;
		}
	}

	/**
	 * 批量删除店铺商城收藏
	 * 
	 * @param request
	 * @param response
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "/p/deletefavoriteShop")
	public @ResponseBody String deletefavoriteShop(HttpServletRequest request, HttpServletResponse response, String ids) {
		
		if (AppUtils.isBlank(ids)) {
			return Constants.FAIL;
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		if (user == null) {
			return Constants.FAIL;
		}
		
		favoriteShopService.deletefavoriteShop(user.getUserId(), ids);
		return Constants.SUCCESS;
	}

	/**
	 * 清空店铺商城收藏
	 */
	@RequestMapping(value = "/p/deleteAllfavoriteShop", produces = "application/json;charset=UTF-8")
	public @ResponseBody String deleteAllfavoriteShop(HttpServletRequest request, HttpServletResponse response) {
		SecurityUserDetail user = UserManager.getUser(request);
		
		if (user == null) {
			return Constants.FAIL;
		}
		
		favoriteShopService.deleteAllfavoriteShop(user.getUserId());
		return Constants.SUCCESS;
	}

}
