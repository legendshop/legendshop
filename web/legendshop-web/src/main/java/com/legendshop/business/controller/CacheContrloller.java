/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.legendshop.model.entity.Brand;
import com.legendshop.spi.service.BrandService;
import com.legendshop.util.JSONUtil;

@Controller
@RequestMapping("test")
public class CacheContrloller {
	
	/** The brand service. */
	@Autowired
	private BrandService brandService;
	
    @RequestMapping(value = "/jgroup",method = RequestMethod.GET)
    public void test(HttpServletRequest request,HttpServletResponse response) throws InterruptedException{
    	int i=0;
    	while(true){
        	Brand brand = brandService.getBrand(316l);
        	System.out.println("--------------PC select --------------"+JSONUtil.getJson(brand));
        	Thread.sleep(20000);
        	brand.setBrandName("丹希路111"+(i++));
        	brandService.update(brand);
        	System.out.println("---------------PC update -------------"+JSONUtil.getJson(brand));
        	Thread.sleep(20000);
    	}
    }

}
