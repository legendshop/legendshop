/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.legendshop.core.base.BaseController;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.AdvAnalysis;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.AdvAnalysisService;
import com.legendshop.util.AppUtils;
import com.legendshop.web.helper.IPHelper;


/**
 * 广告分析类控制器
 */
@Controller
public class AdvAnalysisController extends BaseController{
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(AdvAnalysisController.class);
	
	/** 广告控制器服务类 */
	@Autowired
	private AdvAnalysisService advAnalysisService;
	

	/**
	 * 添加广告统计数
	 * 
	 * TODO 需要改成同一个IP 或者同一个用户，一段时间内点击多次，只记录一次，（防止攻击）.
	 *
	 * @param request the request
	 * @param response the response
	 * @return the string
	 */
	@RequestMapping(value="/advAnalysis", method = RequestMethod.GET)
	public void advAnalysis(HttpServletRequest request, HttpServletResponse response){
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		String url = request.getParameter("urlLink");
		String sourcePage = request.getHeader("Referer");
		
		AdvAnalysis  advAnalysis = new AdvAnalysis();
		advAnalysis.setIp(IPHelper.getIpAddr(request));
		advAnalysis.setUrl(url);
		advAnalysis.setSourcePage(sourcePage);
		advAnalysis.setCreateTime(new Date());
		if(AppUtils.isNotBlank(user)){
			advAnalysis.setUserId(user.getUserId());
		}
		advAnalysisService.save(advAnalysis);
		try {
			//为解决java.lang.IllegalStateException: Cannot forward after response has been committed问题,取消fastjson的@ResponseBody模式
			response.getWriter().print(Constants.SUCCESS);
		} catch (IOException e) {
			log.warn("save advAnalysis error", e.getLocalizedMessage());
		}
	}
}
