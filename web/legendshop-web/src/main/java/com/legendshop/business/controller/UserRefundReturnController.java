/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.business.controller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.log.RefundLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.page.FrontPage;
import com.legendshop.business.page.UserCenterFrontPage;
import com.legendshop.business.page.UserCenterRedirectPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.PayMannerEnum;
import com.legendshop.model.constant.PayPctTypeEnum;
import com.legendshop.model.constant.RefundReturnStatusEnum;
import com.legendshop.model.constant.RefundReturnTypeEnum;
import com.legendshop.model.constant.RefundSouceEnum;
import com.legendshop.model.constant.SubHistoryEnum;
import com.legendshop.model.constant.SubTypeEnum;
import com.legendshop.model.dto.order.ApplyRefundForm;
import com.legendshop.model.dto.order.ApplyRefundReturnBase;
import com.legendshop.model.dto.order.ApplyRefundReturnDto;
import com.legendshop.model.dto.order.ApplyReturnForm;
import com.legendshop.model.dto.order.MySubDto;
import com.legendshop.model.dto.order.OrderSetMgDto;
import com.legendshop.model.dto.order.RefundSearchParamDto;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubHistory;
import com.legendshop.model.entity.SystemConfig;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.spi.service.OrderService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.SubHistoryService;
import com.legendshop.spi.service.SubRefundReturnService;
import com.legendshop.spi.service.SubService;
import com.legendshop.spi.service.SystemConfigService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import com.legendshop.util.DateUtils;
import com.legendshop.util.JSONUtil;

/**
 * @Description 退款及退货的Controller
 * @author 关开发
 */
@Controller
public class UserRefundReturnController extends BaseController{

	@Autowired
	private OrderService orderService;

	@Autowired
	private SubHistoryService subHistoryService;


	@Autowired
	private SubRefundReturnService subRefundReturnService;

	@Autowired
    private AttachmentManager attachmentManager;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ProductService productService;

	@Autowired
	private SubService subService;

	@Autowired
	ConstTableService constTableService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
    private SystemConfigService systemConfigService;

	/**
	 * 发送站内信
	 */
	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;

	/**
	 * 跳转到退款,退货列表页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/p/refundReturnList/{applyType}")
	public String refundReturnList(HttpServletRequest request,HttpServletResponse response,@PathVariable Integer applyType) {
		if(AppUtils.isBlank(applyType)){//默认获取退款列表
			applyType = RefundReturnTypeEnum.REFUND.value();
		}
		request.setAttribute("applyType", applyType);
		return PathResolver.getPath(UserCenterFrontPage.REFUND_REFUND_LIST);
	}

	/**
	 * 获取退款列表内容
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/p/refundList")
	public String refundList(HttpServletRequest request,HttpServletResponse response,String curPageNO,RefundSearchParamDto paramData) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		PageSupport<SubRefundReturn> ps = subRefundReturnService.getSubRefundReturnByCenter(curPageNO,userId,paramData);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("paramData",paramData);
		request.setAttribute("curPageNO",curPageNO);
		return PathResolver.getPath(UserCenterFrontPage.REFUND_LIST);
	}

	/**
	 * 获取退货列表内容
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/p/returnList")
	public String returnList(HttpServletRequest request,HttpServletResponse response,String curPageNO,RefundSearchParamDto paramData) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		PageSupport<SubRefundReturn> ps = subRefundReturnService.getReturnByCenter(curPageNO,userId,paramData);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("paramData",paramData);
		request.setAttribute("curPageNO",curPageNO);
		return PathResolver.getPath(UserCenterFrontPage.RETURN_LIST);
	}

	/**
	 * 查看退款详情
	 * @param request
	 * @param response
	 * @param refundId
	 * @return
	 */
	@RequestMapping("/p/refundDetail/{refundId}")
	public String refundDetail(HttpServletRequest request,HttpServletResponse response, @PathVariable Long refundId) {
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
		if(AppUtils.isBlank(subRefundReturn)){
			request.setAttribute("message", "找不到申请单据号");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		if(!userId.equals(subRefundReturn.getUserId())){
			request.setAttribute("message", "没有操作权限");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}
		if (RefundReturnTypeEnum.REFUND.value()!=subRefundReturn.getApplyType()) {// 不允许
			request.setAttribute("message", "不是退款类型的退款单据");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}

		// 返回商家待审核的倒计时
		Integer number = findAudoReview(0);

		if (RefundReturnStatusEnum.APPLY_WAIT_SELLER.value() == subRefundReturn.getApplyState()) { // 发起审核
			Date applyTime = DateUtils.getDay(subRefundReturn.getApplyTime(), number);
			request.setAttribute("applyTime", applyTime);
		}
		Sub sub = subService.getSubById(subRefundReturn.getSubId());
		request.setAttribute("returnFreightAmount", 0);
		StringBuffer sb = new StringBuffer();
		sb.append("定");
		sb.append(subRefundReturn.getDepositRefund());
		sb.append(" + 尾");
		sb.append(subRefundReturn.getRefundAmount());
		if (subRefundReturn.getRefundAmount() !=null && subRefundReturn.getRefundAmount().compareTo(BigDecimal.ZERO) == 1){//预售商品，定金尾款情况
			sb.append("  (运费" + sub.getFreightAmount() +")");
			request.setAttribute("returnFreightAmount", sub.getFreightAmount());
		}
		request.setAttribute("info", sb.toString());
		request.setAttribute("subRefundReturn", subRefundReturn);

		return PathResolver.getPath(UserCenterFrontPage.REFUND_DETAIL_NEW);
	}

	/**
	 * 查看退货详情
	 * @param request
	 * @param responsereceiveGoods
	 * @param refundId 退款,退货ID
	 * @return
	 */
	@RequestMapping("/p/returnDetail/{refundId}")
	public String returnDetail(HttpServletRequest request,HttpServletResponse response, @PathVariable Long refundId) {
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
		if(AppUtils.isBlank(subRefundReturn)){
			request.setAttribute("message", "找不到申请单据号");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		if(!userId.equals(subRefundReturn.getUserId())){
			request.setAttribute("message", "没有操作权限");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}
		if (RefundReturnTypeEnum.REFUND_RETURN.value()!=subRefundReturn.getApplyType()) {// 不允许
			request.setAttribute("message", "不是退货类型的退款单据");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}

		// 返回商家待审核的倒计时
		Integer number = findAudoReview(0);

		if (RefundReturnStatusEnum.APPLY_WAIT_SELLER.value() == subRefundReturn.getApplyState()) { // 发起审核
			Date applyTime = DateUtils.getDay(subRefundReturn.getApplyTime(), number);
			request.setAttribute("applyTime", applyTime);
		}

		// 商家同意退货 返回用户发货的倒计时
		if (RefundReturnStatusEnum.SELLER_AGREE.value() == subRefundReturn.getSellerState() && RefundReturnStatusEnum.LOGISTICS_WAIT_DELIVER.value() == subRefundReturn.getGoodsState()) {
			Date waitDelive = DateUtils.getDay(subRefundReturn.getSellerTime(), number);
			request.setAttribute("waitDelive", waitDelive);
			// 返回商家默认收货地址
			/*
			 * ShopAddress returnAddr =
			 * shopAddressService.getShopReturnAddr(shopId,1);
			 * request.setAttribute("returnAddr", returnAddr);
			 */
		}
		// 用户已发货 商家未确认
		if (RefundReturnStatusEnum.SELLER_AGREE.value() == subRefundReturn.getSellerState() && RefundReturnStatusEnum.LOGISTICS_WAIT_RECEIVE.value() == subRefundReturn.getGoodsState()) {
			Date waitReceive = DateUtils.getDay(subRefundReturn.getShipTime(), number);
			request.setAttribute("waitReceive", waitReceive);
		}

		request.setAttribute("subRefundReturn", subRefundReturn);

		return PathResolver.getPath(UserCenterFrontPage.RETURN_DETAIL_NEW);
	}

	/**
	 * 跳转到 "申请订单退款" 页面
	 * @param request
	 * @param response
	 * @param orderId 订单ID
	 * @return
	 */
	@RequestMapping("/p/refund/apply/{orderId}")
	public String applyRefund(HttpServletRequest request,HttpServletResponse response,
			@PathVariable Long orderId) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		ApplyRefundReturnDto refund = subRefundReturnService.getApplyRefundReturnDto(orderId,RefundReturnTypeEnum.OP_ORDER,userId);
		if(null == refund){
			return PathResolver.getPath(FrontPage.ERROR);
		}
		//检查订单是否允许退款操作
		if(!isAllowRefund(refund)){//不允许
			request.setAttribute("message", "该订单不允许退款操作");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}

		if(Double.doubleToRawLongBits(refund.getSubMoney())==Double.doubleToLongBits(0.00)){
			request.setAttribute("message", "该订单金额为0，不支持退款");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}

		Sub order = subService.getSubById(orderId);
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		request.setAttribute("systemConfig", systemConfig);
		request.setAttribute("refund", refund);
		request.setAttribute("order", order);
		return PathResolver.getPath(UserCenterFrontPage.APPLY_ORDER_REFUND);
	}

	/**
	 * 跳转到 申请订单项 "退款且退货" 页面
	 * @param request
	 * @param response
	 * @param orderId 订单ID
	 * @param orderItemId 订单项ID
	 * @return
	 */
	@RequestMapping("/p/return/apply/{orderId}/{orderItemId}")
	public String applyReturn(HttpServletRequest request,HttpServletResponse response,
			@PathVariable Long orderId,@PathVariable Long orderItemId) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		//查询子订单
		ApplyRefundReturnDto refund = subRefundReturnService.getApplyRefundReturnDto(orderItemId,RefundReturnTypeEnum.OP_ORDER_ITEM, userId);
		if(null == refund){
			return PathResolver.getPath(FrontPage.ERROR);
		}
		String subNumber = request.getParameter("subNum");
		MySubDto order = subService.findOrderDetail(subNumber);
		if(order == null){
			return PathResolver.getPath(FrontPage.ERROR);
		}
		//检查订单是否允许退款退货操作
		if(!isAllowItemReturn(refund)){//不允许
			request.setAttribute("message", "该订单不允许退款操作");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}
		if(OrderStatusEnum.SUCCESS.value().equals(order.getStatus()) && !isInReturnValidPeriod(refund,order)){
			request.setAttribute("message", "该订单退换货时间已到期");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}

		if(SubTypeEnum.PRESELL.value().equals(refund.getSubType()) && refund.getPayPctType() == PayPctTypeEnum.FRONT_MONEY.value()) {//预售订金支付
			Double shouldRefund = Arith.sub(refund.getRefundAmount(), refund.getDepositRefundAmount());
			refund.setRefundAmount(shouldRefund);
		}else {
			//已退金额
			Double subRefundAmount = calculateAlreadyRefundAmount(order.getSubSettlement(),order.getSubId());

			Double shouldRefund = Arith.sub(refund.getSubMoney(), subRefundAmount);
			//检查可以退款的金额,可退金额不能超过应退金额
			if(shouldRefund < refund.getRefundAmount()){
				refund.setRefundAmount(shouldRefund);
			}
		}
		request.setAttribute("order", order);
		request.setAttribute("refund", refund);
		return PathResolver.getPath(UserCenterFrontPage.APPLY_ITEM_RETURN);
	}

	/**
	 * 计算已经退款的金额
	 * @return
	 */
	private Double calculateAlreadyRefundAmount(String subSettlementSn,Long subId){
		//根据settlementSn获得所有已经退款成功的订单项
		List<SubRefundReturn> sunRefundlist = subRefundReturnService.getSubRefundsSuccessBySettleSn(subSettlementSn);
		Double alreadyAmount = 0d;  //已经成功退款的总金额
		for(SubRefundReturn refund:sunRefundlist.stream().filter(u-> u.getSubId().equals(subId)).collect(Collectors.toList())){
			Double successRefund = refund.getRefundAmount().doubleValue();  //已经成功退款的金额
			alreadyAmount = Arith.add(alreadyAmount,successRefund);  //累加订单中已经成功退款的总金额
		}

		return alreadyAmount;
	}

	/**
	 * 保存订单退款申请 --- 当订单处于待发货状态 只能整个订单退款处理
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/p/refund/save")
	public String saveOrderRefund(HttpServletRequest request,HttpServletResponse response,
			@Validated ApplyRefundForm form, BindingResult error) {
		if(error.hasErrors()){
			request.setAttribute("message", "对不起,您提交的数据有误!");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		ApplyRefundReturnDto refund = subRefundReturnService.getApplyRefundReturnDto(form.getOrderId(),RefundReturnTypeEnum.OP_ORDER, userId);
		if(AppUtils.isBlank(refund)){
			request.setAttribute("message", "对不起,您操作的订单不存在或已被删除!");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}
		//检查订单是否允许退款
		if(!isAllowRefund(refund)){
			request.setAttribute("message", "对不起,您操作的订单不允许退款!");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}

		RefundLog.info(" ########### 获取已设值的SubRefundReturn对象  ################## ");
		//给SubRefundReturn属性设值,获取已设值的SubRefundReturn对象
		SubRefundReturn subRefundReturn = setCommonProperty(form,refund,request);

		RefundLog.info(" ########### 获取已设值的SubRefundReturn对象成功  ################## ");
		//订单全部退款,这些设置为0
		subRefundReturn.setSubItemId(0L);
		subRefundReturn.setProductImage(refund.getProdPic());
		subRefundReturn.setProductId(0L);
		subRefundReturn.setSkuId(0L);
		subRefundReturn.setProductName("订单商品全部退款");
		subRefundReturn.setApplyType(RefundReturnTypeEnum.REFUND.value());
		subRefundReturn.setRefundAmount(new BigDecimal(refund.getRefundAmount()));//退款金额等于订单金额
		request.setAttribute("message","ok");
		//改造预售退款增加  2019-1-18
		if(SubTypeEnum.PRESELL.value().equals(refund.getSubType())) {//预售订单
			subRefundReturn.setIsPresell(true);
		}else {
			subRefundReturn.setIsPresell(false);
		}
		subRefundReturn.setIsRefundDeposit(false);
		subRefundReturn.setIsDepositHandleSucces(RefundReturnStatusEnum.DEPOSIT_NO_REFUND.value());
		subRefundReturn.setDepositRefund(new BigDecimal(0));
		subRefundReturn.setRefundSouce(RefundSouceEnum.USER.value());

		RefundLog.info(" ########### 开始撤销历史未完成退款申请  ################## ");
		subRefundReturnService.returnProcess(subRefundReturn.getSubId(), subRefundReturn.getSubItemId(), userId);
		RefundLog.info(" ########### 撤销历史未完成退款申请成功  ################## ");

		RefundLog.info(" ########### 开始保存订单历史  ################## ");
		saveOrderHistory(subRefundReturn);
		RefundLog.info(" ########### 保存订单历史成功  ################## ");

		RefundLog.info(" ########### 开始保存退款记录  ################## ");
		subRefundReturnService.saveApplyOrderRefund(subRefundReturn);
		RefundLog.info(" ########### 保存退款记录成功  ################## ");

		RefundLog.info(" ########### 开始发送站内信  ################## ");
		//发送站内信 通知
		UserDetail shopUser = userDetailService.getUserDetailByShopId(refund.getShopId());
		sendSiteMessageProcessor.process(new SendSiteMsgEvent("系统", shopUser.getUserName(), "退款通知" , "亲，您的订单["+subRefundReturn.getSubNumber()+"],买家申请退款！").getSource());
		RefundLog.info(" ########### 发送站内信成功  ################## ");
		return PathResolver.getPath(UserCenterRedirectPage.REFUND_RETURN_LIST) + "/" + RefundReturnTypeEnum.REFUND.value();
	}

	/**
	 * 保存订单项 "仅退款" 申请
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/p/itemRefund/save")
	@ResponseBody
	public String saveItemRefund(HttpServletRequest request,HttpServletResponse response,
			@Validated ApplyRefundForm form, BindingResult error) {
		if(error.hasErrors()){
			return "对不起,您提交的数据有误!";
		}

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		ApplyRefundReturnDto refund = subRefundReturnService.getApplyRefundReturnDto(form.getOrderItemId(),RefundReturnTypeEnum.OP_ORDER_ITEM,userId);
		if(AppUtils.isBlank(refund)){
			return "对不起,您操作的订单不存在或已被删除!";
		}

		//已退金额
	//	Double subRefundAmount = calculateAlreadyRefundAmount(refund.getSubSettlementSn());

		//检查可以退款的金额,可退金额不能超过应退金额
		//Double shouldRefund = Arith.sub(refund.getSubMoney(), subRefundAmount);
		Double shouldRefund =refund.getRefundAmount();

		if(SubTypeEnum.PRESELL.value().equals(refund.getSubType()) && refund.getPayPctType() == PayPctTypeEnum.FRONT_MONEY.value()) {//预售订金支付
			shouldRefund = Arith.sub(refund.getRefundAmount(), refund.getDepositRefundAmount());
		}

		//校验退款金额
		if(form.getRefundAmount().doubleValue() <= 0L
				|| form.getRefundAmount().doubleValue() > shouldRefund){
			return "对不起,您提交的数据有误!";
		}

		//检查订单项是否允许退款
		if(!isAllowItemReturn(refund)){
			return "对不起,您操作的订单不允许退款!";
		}

		//给SubRefundReturn属性设值
		SubRefundReturn subRefundReturn = new SubRefundReturn();
		if(SubTypeEnum.PRESELL.value().equals(refund.getSubType())) { //预售订单
			//给SubRefundReturn属性设值
			 subRefundReturn = setPresellCommonProperty(form,refund,request);//设置各种申请的公共属性
		} else {//其他订单
			//给SubRefundReturn属性设值
			 subRefundReturn = setCommonProperty(form,refund,request);//设置各种申请的公共属性
		}

		subRefundReturn.setSubItemId(form.getOrderItemId());
		subRefundReturn.setProductId(refund.getProdId());
		subRefundReturn.setSkuId(refund.getSkuId());
		subRefundReturn.setProductName(refund.getProdName());
		subRefundReturn.setProductImage(refund.getProdPic());
		subRefundReturn.setProductNum(refund.getBasketCount());
		subRefundReturn.setProductAttribute(refund.getAttribute());
		subRefundReturn.setRefundAmount(new BigDecimal(form.getRefundAmount()));
		subRefundReturn.setIsRefundDeposit(form.getIsRefundDeposit());
		subRefundReturn.setDepositRefund(new BigDecimal(form.getDepositRefundAmount()));

		subRefundReturn.setGoodsNum(Long.valueOf(refund.getGoodsCount()));
		subRefundReturn.setApplyType(RefundReturnTypeEnum.REFUND.value());

		subRefundReturnService.returnProcess(subRefundReturn.getSubId(), subRefundReturn.getSubItemId(), userId);

		subRefundReturnService.saveApplyItemRefund(subRefundReturn);
		//发送站内信 通知
		UserDetail shopUser = userDetailService.getUserDetailByShopId(refund.getShopId());
		sendSiteMessageProcessor.process(new SendSiteMsgEvent("系统", shopUser.getUserName(), "退款通知" , "亲，您的订单["+subRefundReturn.getSubNumber()+"],买家申请退款！").getSource());
		return Constants.SUCCESS;
	}

	/**
	 * 保存订单项 "退款且退货" 申请
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/p/itemReturn/save")
	@ResponseBody
	public String saveItemReturn(HttpServletRequest request,HttpServletResponse response,
			@Validated ApplyReturnForm form, BindingResult error) {
		if(error.hasErrors()){
			return "对不起,您提交的数据有误!";
		}

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		ApplyRefundReturnDto refund = subRefundReturnService.getApplyRefundReturnDto(form.getOrderItemId(),RefundReturnTypeEnum.OP_ORDER_ITEM,userId);


		if(AppUtils.isBlank(refund)){
			return "对不起,您操作的订单不存在或已被删除!";
		}

		Double shouldRefund = 0d;
		if(SubTypeEnum.PRESELL.value().equals(refund.getSubType())
				&& refund.getPayPctType() == PayPctTypeEnum.FRONT_MONEY.value()) { //预售订金支付
			shouldRefund = Arith.sub(refund.getRefundAmount(), refund.getDepositRefundAmount());
		}else {
			//已退金额
			Sub sub = subService.getSubBySubNumber(refund.getSubNumber());
			Double subRefundAmount = calculateAlreadyRefundAmount(refund.getSubSettlementSn(),sub.getSubId());
			//检查可以退款的金额,可退金额不能超过应退金额
			shouldRefund = Arith.sub(refund.getSubMoney(), subRefundAmount);
		}

		//校验退款金额
		if(form.getRefundAmount().doubleValue() <= 0L
				|| form.getRefundAmount().doubleValue() > shouldRefund){
			return "对不起,您提交的数据有误，请求退款金额不能大于可退金额!";
		}
		//校验退货数量
		if(form.getGoodsNum().intValue() <=0 || form.getGoodsNum() >refund.getGoodsCount()){
			return "对不起,您提交的数据有误!";
		}
		//检查订单是否允许退款退货
		if(!isAllowItemReturn(refund)){
			return "对不起,您操作的订单不允许退款!";
		}
		SubRefundReturn subRefundReturn = new SubRefundReturn();

		if(SubTypeEnum.PRESELL.value().equals(refund.getSubType())) { //预售订单
			//给SubRefundReturn属性设值
			 subRefundReturn = setPresellCommonProperty(form,refund,request);//设置各种申请的公共属性
		} else {//其他订单
			//给SubRefundReturn属性设值
			 subRefundReturn = setCommonProperty(form,refund,request);//设置各种申请的公共属性
		}

		subRefundReturn.setSubItemId(form.getOrderItemId());
		subRefundReturn.setProductId(refund.getProdId());
		subRefundReturn.setSkuId(refund.getSkuId());
		subRefundReturn.setProductName(refund.getProdName());
		subRefundReturn.setProductImage(refund.getProdPic());
		subRefundReturn.setProductNum(refund.getBasketCount());
		subRefundReturn.setProductAttribute(refund.getAttribute());
		subRefundReturn.setRefundAmount(new BigDecimal(form.getRefundAmount()));
		subRefundReturn.setIsRefundDeposit(form.getIsRefundDeposit());
		subRefundReturn.setDepositRefund(new BigDecimal(form.getDepositRefundAmount()));

		subRefundReturn.setGoodsNum(form.getGoodsNum());
		subRefundReturn.setApplyType(RefundReturnTypeEnum.REFUND_RETURN.value());

		subRefundReturnService.returnProcess(subRefundReturn.getSubId(), subRefundReturn.getSubItemId(), userId);

		subRefundReturnService.saveApplyItemReturn(subRefundReturn);
		Sub sub=new Sub();
		sub.setSubId(subRefundReturn.getSubId());
		sub.setUserName(subRefundReturn.getUserName());
		saveOrderHistoryByProduct(sub);
		//发送站内信 通知
		UserDetail shopUser = userDetailService.getUserDetailByShopId(refund.getShopId());
		sendSiteMessageProcessor.process(new SendSiteMsgEvent("系统", shopUser.getUserName(), "退款通知" , "亲，您的订单["+subRefundReturn.getSubNumber()+"],买家申请退款且退货！").getSource());

		return Constants.SUCCESS;
	}

	/**
	 * 用户退货给商家action
	 * @param refundId 退货记录ID
	 * @param expressName 物流公司
	 * @param expressNo 物流单号
	 * @return
	 */
	@RequestMapping("/p/returnGoods")
	@ResponseBody
	public String returnGoods(HttpServletRequest request,HttpServletResponse response,
			Long refundId, String expressName, String expressNo) {
		if(null == refundId || AppUtils.isBlank(expressName) || AppUtils.isBlank(expressNo)){
			return "对不起,请输入物流信息再提交!";
		}
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
		if(null == subRefundReturn){
			return "对不起,您操作的退货记录不存在!";
		}
		subRefundReturn.setExpressName(expressName);
		subRefundReturn.setExpressNo(expressNo);
		subRefundReturn.setShipTime(new Date());
		subRefundReturn.setGoodsState(RefundReturnStatusEnum.LOGISTICS_WAIT_RECEIVE.value());//物流状态,待收货
		subRefundReturnService.shipSubRefundReturn(subRefundReturn);
		Sub sub = new Sub();
		sub.setSubId(subRefundReturn.getSubId());
		sub.setUserName(subRefundReturn.getUserName());
		saveOrderHistoryUserProduct(sub);
		return Constants.SUCCESS;
	}

	/**
	 * 用户退货给商家action
	 * @param refundId 退货记录ID
	 * @return
	 */
	@RequestMapping("/p/refund/delete/{refundId}")
	@ResponseBody
	public String deleteRefundReturn(HttpServletRequest request,HttpServletResponse response,
			@PathVariable Long refundId) {
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
		if(null == subRefundReturn){
			return "对不起,您操作的记录不存在或已被删除!";
		}
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		if(!userId.equals(subRefundReturn.getUserId())){
			return "请不要非法操作!";
		}
		if(RefundReturnStatusEnum.SELLER_DISAGREE.value() != subRefundReturn.getSellerState()){
			return "请不要非法操作";
		}
		subRefundReturnService.deleteRefundReturn(subRefundReturn);
		return Constants.SUCCESS;
	}



	/**
	 * 设置预售订单仅退款和退款及退货申请的公共属性
	 * @return 返回设置完成的 SubRefundReturn 对象
	 */
	private SubRefundReturn setPresellCommonProperty(ApplyRefundReturnBase form, ApplyRefundReturnDto refund, HttpServletRequest request){

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();

		SubRefundReturn subRefundReturn = new SubRefundReturn();
		subRefundReturn.setSubId(form.getOrderId());
		subRefundReturn.setSubNumber(refund.getSubNumber());
		subRefundReturn.setSubMoney(new BigDecimal(refund.getSubMoney()));//设置订单总金额
		subRefundReturn.setSubItemMoney(refund.getProdCash());//订单项的金额
		subRefundReturn.setShopId(refund.getShopId());
		subRefundReturn.setShopName(refund.getShopName());
		subRefundReturn.setRefundSn(CommonServiceUtil.getRandomSn(new Date(),"yyyyMMdd"));
		subRefundReturn.setUserId(userId);
		subRefundReturn.setUserName(userName);

		subRefundReturn.setSellerState(RefundReturnStatusEnum.SELLER_WAIT_AUDIT.value());
		subRefundReturn.setApplyState(RefundReturnStatusEnum.APPLY_WAIT_SELLER.value());
		subRefundReturn.setApplyTime(new Date());
		subRefundReturn.setBuyerMessage(form.getBuyerMessage());
		subRefundReturn.setReasonInfo(form.getReasonInfo());

		subRefundReturn.setRefundSouce(RefundSouceEnum.USER.value());
		subRefundReturn.setIsPresell(true);


		if(PayPctTypeEnum.FULL_PAY.value() == refund.getPayPctType()) {//全额支付，不需要处理订金
			subRefundReturn.setFlowTradeNo(refund.getFlowTradeNo());
			subRefundReturn.setSubSettlementSn(refund.getSubSettlementSn());
			subRefundReturn.setOrderDatetime(refund.getOrderDateTime());
			subRefundReturn.setPayTypeId(refund.getPayTypeId());
			subRefundReturn.setPayTypeName(refund.getPayTypeName());
			subRefundReturn.setIsDepositHandleSucces(RefundReturnStatusEnum.DEPOSIT_NO_REFUND.value());

		}else {
			subRefundReturn.setFlowTradeNo(refund.getFinalFlowTradeNo());
			subRefundReturn.setSubSettlementSn(refund.getFinalSettlementSn());
			subRefundReturn.setOrderDatetime(refund.getFinalOrderDateTime());
			subRefundReturn.setPayTypeId(refund.getFinalPayTypeId());
			subRefundReturn.setPayTypeName(refund.getFinalPayTypeName());

			subRefundReturn.setDepositFlowTradeNo(refund.getFlowTradeNo());
			subRefundReturn.setDepositSubSettlementSn(refund.getSubSettlementSn());
			subRefundReturn.setDepositOrderDatetime(refund.getOrderDateTime());
			subRefundReturn.setDepositPayTypeId(refund.getPayTypeId());
			subRefundReturn.setDepositPayTypeName(refund.getPayTypeName());

			if(form.getIsRefundDeposit()!=null && form.getIsRefundDeposit()) { //用户选择退订金
				subRefundReturn.setIsDepositHandleSucces(RefundReturnStatusEnum.DEPOSIT_REFUND_PROCESSING.value());
			}else {
				form.setIsRefundDeposit(false);
				subRefundReturn.setIsDepositHandleSucces(RefundReturnStatusEnum.DEPOSIT_NO_REFUND.value());
			}
		}

		//保存退款及退货凭证图片
		if (form.getPhotoFile1().getSize() > 0 && form.getPhotoFile1().getSize() <= Constants._5M) {
			String picUrl = attachmentManager.upload(form.getPhotoFile1());
			subRefundReturn.setPhotoFile1(picUrl);
		}
		if (form.getPhotoFile2() != null && form.getPhotoFile2().getSize() > 0 && form.getPhotoFile2().getSize() <= Constants._5M) {
			String picUrl = attachmentManager.upload(form.getPhotoFile2());
			subRefundReturn.setPhotoFile2(picUrl);
		}
		if (form.getPhotoFile3() != null && form.getPhotoFile3().getSize() > 0 && form.getPhotoFile3().getSize() <= Constants._5M) {
			String picUrl = attachmentManager.upload(form.getPhotoFile3());
			subRefundReturn.setPhotoFile3(picUrl);
		}
		return subRefundReturn;
	}


	/**
	 * 设置仅退款和退款及退货申请的公共属性
	 * @return 返回设置完成的 SubRefundReturn 对象
	 */
	private SubRefundReturn setCommonProperty(ApplyRefundReturnBase form, ApplyRefundReturnDto refund, HttpServletRequest request){
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();

		SubRefundReturn subRefundReturn = new SubRefundReturn();
		subRefundReturn.setSubId(form.getOrderId());
		subRefundReturn.setSubNumber(refund.getSubNumber());
		subRefundReturn.setSubMoney(new BigDecimal(refund.getSubMoney()));//设置订单总金额
		subRefundReturn.setSubItemMoney(refund.getProdCash());//订单项的金额
		subRefundReturn.setFlowTradeNo(refund.getFlowTradeNo());
		subRefundReturn.setSubSettlementSn(refund.getSubSettlementSn());
		subRefundReturn.setOrderDatetime(refund.getOrderDateTime());
		subRefundReturn.setPayTypeId(refund.getPayTypeId());
		subRefundReturn.setPayTypeName(refund.getPayTypeName());
		subRefundReturn.setShopId(refund.getShopId());
		subRefundReturn.setShopName(refund.getShopName());
		subRefundReturn.setRefundSn(CommonServiceUtil.getRandomSn(new Date(),"yyyyMMdd"));
		subRefundReturn.setUserId(userId);
		subRefundReturn.setUserName(userName);
		subRefundReturn.setSellerState(RefundReturnStatusEnum.SELLER_WAIT_AUDIT.value());
		subRefundReturn.setApplyState(RefundReturnStatusEnum.APPLY_WAIT_SELLER.value());
		subRefundReturn.setApplyTime(new Date());
		subRefundReturn.setBuyerMessage(form.getBuyerMessage());
		subRefundReturn.setReasonInfo(form.getReasonInfo());
		subRefundReturn.setRefundSouce(RefundSouceEnum.USER.value());
		subRefundReturn.setIsPresell(false);
		subRefundReturn.setIsDepositHandleSucces(RefundReturnStatusEnum.DEPOSIT_NO_REFUND.value());

		//保存退款及退货凭证图片
		if (form.getPhotoFile1() != null && form.getPhotoFile1().getSize() > 0 && form.getPhotoFile1().getSize() <= Constants._5M) {
			String picUrl = attachmentManager.upload(form.getPhotoFile1());
			subRefundReturn.setPhotoFile1(picUrl);
		}
		if (form.getPhotoFile2() != null && form.getPhotoFile2().getSize() > 0 && form.getPhotoFile2().getSize() <= Constants._5M) {
			String picUrl = attachmentManager.upload(form.getPhotoFile2());
			subRefundReturn.setPhotoFile2(picUrl);
		}
		if (form.getPhotoFile3() != null && form.getPhotoFile3().getSize() > 0 && form.getPhotoFile3().getSize() <= Constants._5M) {
			String picUrl = attachmentManager.upload(form.getPhotoFile3());
			subRefundReturn.setPhotoFile3(picUrl);
		}

		return subRefundReturn;
	}

	/**
	 * 检查订单是否允许退款操作
	 *
	 * @param refund
	 * @return
	 */
	private boolean isAllowRefund(ApplyRefundReturnDto refund) {
		//如果是货到付款 并且订单已经完结
		if(PayMannerEnum.DELIVERY_ON_PAY.value().equals(refund.getPayManner()) && OrderStatusEnum.SUCCESS.value().equals(refund.getOrderStatus())){
			return true;
		}
		if(!PayMannerEnum.DELIVERY_ON_PAY.value().equals(refund.getPayManner())  &&
				OrderStatusEnum.PADYED.value().equals(refund.getOrderStatus()) &&
				RefundReturnStatusEnum.ORDER_NO_REFUND.value().equals(refund.getRefundStatus())){
			//订单处于待发货 并且没有处理过退款单
			return true;
		}
		return false;
	}


	/**
	 * 检查订单项是否允许退款,退货 操作
	 * @param refund
	 * @return
	 */
	private boolean isAllowItemReturn(ApplyRefundReturnDto refund) {
		//如果这个订单项处于退款操作中或已经完成 则不允许
		if(RefundReturnStatusEnum.ITEM_REFUND_PROCESSING.value().equals(refund.getItemRefundStatus())
				|| RefundReturnStatusEnum.ITEM_REFUND_FINISH.value().equals(refund.getItemRefundStatus())){
			return false;
		}

		//如果不是交易成功
		if(!OrderStatusEnum.CONSIGNMENT.value().equals(refund.getOrderStatus()) && !OrderStatusEnum.SUCCESS.value().equals(refund.getOrderStatus())){
			return false;
		}
		//如果是货到付款 并且订单已经完结
		if (PayMannerEnum.DELIVERY_ON_PAY.value().equals(refund.getPayManner()) && ! OrderStatusEnum.SUCCESS.value().equals(refund.getOrderStatus())) {
			return false;
		}
		return true;
	}

	/**
	 * 检查订单项是否在退换货有效时间内
	 * @return
	 */
	private boolean isInReturnValidPeriod(ApplyRefundReturnDto refund,MySubDto order){
		Long prodId = refund.getProdId();
		ProductDetail prod = productService.getProdDetail(prodId);
		Integer days = 0;
		if (AppUtils.isNotBlank(prod)){
			Long categoryId = prod.getCategoryId();
			Category category = categoryService.getCategory(categoryId);
			days = category.getReturnValidPeriod();
		}
		//当分类退换货时间设置为空，获取系统默认设置
		if(days == null || days == 0){
			ConstTable constTable =constTableService.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
			OrderSetMgDto orderSetting=null;
			if(AppUtils.isNotBlank(constTable)){
				String setting=constTable.getValue();
			   orderSetting=JSONUtil.getObject(setting, OrderSetMgDto.class);
			}
			days = Integer.parseInt(orderSetting.getAuto_product_return());
		}
		Date finallyDate = order.getFinallyDate();
		if(null == finallyDate){//说明订单还未完成，默认有效
			return true;
		}
		return isInDates(finallyDate, days);
	}

	/**
	 * 代替 com.legendshop.util.DateUtils#isInDates(java.util.Date, int) 的方法，判断当前时间是否在范围之内
	 * @param date		时间
	 * @param days		偏移天数
	 * @return
	 */
	private static boolean isInDates(Date date, long days) {
		long ms = days * 24 * 3600 * 1000;
		long times = date.getTime() + ms;
		long now = (new Date()).getTime();
		return times > now;
	}

	/**
	 * 用户撤销退货action
	 *
	 * @param refundSn
	 *            物流单号
	 * @return
	 */
	@RequestMapping("/p/refund/candelAction/{refundSn}")
	@ResponseBody
	public String candelAction(HttpServletRequest request, HttpServletResponse response, @PathVariable String refundSn) {

		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturnByRefundSn(refundSn);

		if (null == subRefundReturn) {
			return "对不起,您操作的记录不存在或已被删除!";
		}
		SecurityUserDetail user = UserManager.getUser(request);
		if (!user.getUserId().equals(subRefundReturn.getUserId())) {
			return "请不要非法操作!";
		}
		// 仅在卖家未处理该申请的时候才允许撤销操作
		if (RefundReturnStatusEnum.SELLER_WAIT_AUDIT.value() != subRefundReturn.getSellerState()) {
			return "该订单已在审核流程，无法撤销，请联系商家";
		}
		Sub sub = new Sub();
		sub.setSubId(subRefundReturn.getSubId());
		sub.setUserName(subRefundReturn.getUserName());
		saveOrderHistoryCancel(sub);
		subRefundReturnService.undoApplySubRefundReturn(subRefundReturn);
		return Constants.SUCCESS;
	}

	/**
	 * 获取退换货自动审核时间或者退换货有效时间
	 *
	 * @param type
	 *            0 获取退换货自动审核时间 1 获取退换货有效时间
	 * @return
	 */
	private Integer findAudoReview(Integer type) {
		Integer number = 7;
		ConstTable constTable = constTableService.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
		OrderSetMgDto orderSetting = null;
		if (AppUtils.isNotBlank(constTable)) {
			String setting = constTable.getValue();
			orderSetting = JSONUtil.getObject(setting, OrderSetMgDto.class);
			if (1 == type && AppUtils.isNotBlank(orderSetting.getAuto_product_return())) {
				number = Integer.parseInt(orderSetting.getAuto_product_return());
			}
			if (0 == type && AppUtils.isNotBlank(orderSetting.getAuto_return_review())) {
				number = Integer.parseInt(orderSetting.getAuto_return_review());
			}
		}
		return number;
	}


	/**
	 * 用户退款订单历史
	 * @param sub
	 */
	private void saveOrderHistory(SubRefundReturn sub) {
		SubHistory subHistory = new SubHistory();
		Date date = new Date();
		String time = subHistory.DateToString(date);
		subHistory.setRecDate(date);
		subHistory.setSubId(sub.getSubId());
		subHistory.setStatus(SubHistoryEnum.ORDER_RETURNMONEY.value());
		StringBuilder sb = new StringBuilder();
		sb.append("[退货/退款]"+sub.getUserName()).append("于").append(time).append("发起订单退款 ");
		subHistory.setUserName(sub.getUserName());
		subHistory.setReason(sb.toString());
		subHistoryService.saveSubHistory(subHistory);
	}

	/**
	 * 用户撤销退款订单历史
	 * @param sub
	 */
	private void saveOrderHistoryCancel(Sub sub) {
		SubHistory subHistory = new SubHistory();
		Date date = new Date();
		String time = subHistory.DateToString(date);
		subHistory.setRecDate(date);
		subHistory.setSubId(sub.getSubId());
		subHistory.setStatus(SubHistoryEnum.ORDER_RETURNMONEY.value());
		StringBuilder sb = new StringBuilder();
		sb.append("[退货/退款]"+sub.getUserName()).append("于").append(time).append("撤销退[退货/退款]");
		subHistory.setUserName(sub.getUserName());
		subHistory.setReason(sb.toString());
		subHistoryService.saveSubHistory(subHistory);
	}


	/**
	 * 用户退货退款订单历史
	 * @param sub
	 */
	private void saveOrderHistoryUserProduct(Sub sub) {
		SubHistory subHistory = new SubHistory();
		Date date = new Date();
		String time = subHistory.DateToString(date);
		subHistory.setRecDate(date);
		subHistory.setSubId(sub.getSubId());
		subHistory.setStatus(SubHistoryEnum.BUYERS_RETURNGOOD.value());
		StringBuilder sb = new StringBuilder();
		sb.append("[退货/退款]"+sub.getUserName()).append("于").append(time).append("用户进行退货");
		subHistory.setUserName(sub.getUserName());
		subHistory.setReason(sb.toString());
		subHistoryService.saveSubHistory(subHistory);
	}

	/**
	 * 用户退货及退款订单历史
	 * @param sub
	 */
	private void saveOrderHistoryByProduct(Sub sub) {
		SubHistory subHistory = new SubHistory();
		Date date = new Date();
		String time = subHistory.DateToString(date);
		subHistory.setRecDate(date);
		subHistory.setSubId(sub.getSubId());
		subHistory.setStatus(SubHistoryEnum.ORDER_RETURNGOOD.value());
		StringBuilder sb = new StringBuilder();
		sb.append("[退货/退款]"+sub.getUserName()).append("于").append(time).append("发起商品的退货及退款 ");
		subHistory.setUserName(sub.getUserName());
		subHistory.setReason(sb.toString());
		subHistoryService.saveSubHistory(subHistory);
	}
}
