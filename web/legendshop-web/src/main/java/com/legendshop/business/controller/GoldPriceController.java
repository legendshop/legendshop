/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.base.BaseController;
import com.legendshop.model.constant.Constants;

/**
 * 
 * 广告控制器
 *
 */
@Controller
public class GoldPriceController extends BaseController{
	
	/**
	 * 添加广告统计数
	 * 
	 * TODO 需要改成同一个IP 或者同一个用户，一段时间内点击多次，只记录一次，（防止攻击）
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/gold/updateprice", method = RequestMethod.POST)
	public @ResponseBody String advAnalysis(HttpServletRequest request, HttpServletResponse response){
		//1. 检查token
		//2. 更新价格表，并把之前的记录插入历史表，如果价格没有更改则无需插入历史表
		return Constants.SUCCESS;
	}
}
