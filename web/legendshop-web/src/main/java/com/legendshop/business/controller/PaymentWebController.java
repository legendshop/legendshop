/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.log.PaymentLog;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.dao.SubDao;
import com.legendshop.business.page.FrontPage;
import com.legendshop.business.service.impl.PaymentUtil;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PaySourceEnum;
import com.legendshop.model.constant.PaymentProcessorEnum;
import com.legendshop.model.constant.SubSettlementTypeEnum;
import com.legendshop.model.dto.PaytoParamsDto;
import com.legendshop.model.dto.payment.InitPayFromDto;
import com.legendshop.model.entity.AuctionDepositRec;
import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.entity.SubSettlementItem;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.UserSecurity;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.AuctionDepositRecService;
import com.legendshop.spi.service.PaymentService;
import com.legendshop.spi.service.SubSettlementItemService;
import com.legendshop.spi.service.SubSettlementService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.web.helper.IPHelper;

/**
 * 支付中心
 *
 */
@Controller
@RequestMapping(value = "/p/payment")
public class PaymentWebController {

    @Autowired
    private PaymentUtil paymentUtil;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private SubSettlementService subSettlementService;

    @Autowired
    private UserDetailService userDetailService;

    @Autowired
    private SubSettlementItemService subSettlementItemService;

    @Autowired
    private AuctionDepositRecService auctionDepositRecService;
    
    @Autowired
	private SubDao subDao;
    
	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;
	
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * 使用预付款或金币
     *
     * @param request
     * @param password
     * @param payType  1：预付款  2：金币
     * @return
     */
    @RequestMapping(value = "/userPredeposit/{payType}", method = RequestMethod.POST)
    public
    @ResponseBody
    Map<String, Object> userPredeposit(HttpServletRequest request, @PathVariable Integer payType, @RequestParam String password) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("status", false);
        if (AppUtils.isBlank(password)) {
            map.put("msg", "请输入支付密码");
            return map;
        }

        SecurityUserDetail user = UserManager.getUser(request);
        
        UserSecurity security = userDetailService.getUserSecurity(user.getUsername());
        if (security.getPaypassVerifn().intValue() == 0) { //是否通过支付验证
            map.put("msg", "请开启安全支付密码");
            return map;
        }
        UserDetail detail = userDetailService.getUserDetailByIdNoCache(user.getUserId());
        if (detail.getAvailablePredeposit() <= 0 && payType == 1) {
            map.put("msg", "账户余额不足,请充值");
            return map;
        } else if (detail.getUserCoin() <= 0 && payType == 2) {
            map.put("msg", "金币不足,请充值");
            return map;
        }
        
        if (!passwordEncoder.matches(password, detail.getPayPassword())) {
            map.put("msg", "支付密码错误请重新输入！");
            return map;
        }
        map.put("status", true);
        request.getSession().setAttribute(Constants.PASSWORD_CALLBACK, "true");
        return map;
    }


    /**
     * 支付
     * @param request
     * @param response
     * @param subSettlementType
     * @param paytoParam
     * @param submitPwd
     * @return
     */
    @RequestMapping(value = "/payto/{subSettlementType}", method = RequestMethod.POST)
    public String payment(HttpServletRequest request, HttpServletResponse response, @PathVariable String subSettlementType, PaytoParamsDto paytoParam,String submitPwd) {
        String[] subNumbers = paytoParam.getSubNumbers();
        Integer prePayTypeVal = paytoParam.getPrePayTypeVal(); //余额支付方式(1: 预付款  2：金币)
        String password_callback = paytoParam.getPasswordCallback();
        String payTypeId = paytoParam.getPayTypeId();
        SecurityUserDetail securityDetail = UserManager.getUser(request);
        if (AppUtils.isBlank(subNumbers)) {
            request.setAttribute("message", "参数有误,订单流水号不符合规则！");
            return PathResolver.getPath(FrontPage.OPERATION_ERROR);
        }
        
        //判断是否启用全额预付款支付
        boolean walletPay = false;  //钱包支付
        String isCallback = (String) request.getSession().getAttribute(Constants.PASSWORD_CALLBACK);
        if(("1".equals(password_callback) && "true".equals(isCallback)) || PaymentProcessorEnum.FULL_PAY.value().equals(payTypeId) || PaymentProcessorEnum.WX_BLEND_PAY.value().equals(payTypeId)){
            if (prePayTypeVal != 1 && prePayTypeVal != 2) { //余额支付方式(1: 预付款  2：金币)
                request.setAttribute("message", "参数有误,余额支付方式不符合规则！");
                return PathResolver.getPath(FrontPage.OPERATION_ERROR);
            }
            	if (AppUtils.isBlank(submitPwd)) {
                    PaymentLog.error("请输入支付密码");
                    request.setAttribute("message", "请输入支付密码");
                    return PathResolver.getPath(FrontPage.OPERATION_ERROR);
                }
                
                UserSecurity security = userDetailService.getUserSecurity(securityDetail.getUsername());
                if (security.getPaypassVerifn().intValue() == 0) { //是否通过支付验证
                    PaymentLog.error("请开启安全支付密码");
                    request.setAttribute("message", "请开启安全支付密码");
                    return PathResolver.getPath(FrontPage.OPERATION_ERROR);
                }
                
                com.legendshop.model.entity.UserDetail userDetail = userDetailService.getUserDetailByIdNoCache(securityDetail.getUserId());
                if (!passwordEncoder.matches(submitPwd, userDetail.getPayPassword())) {
                    PaymentLog.error("支付密码错误请重新输入");
                    request.setAttribute("message", "支付密码错误请重新输入");
                    return PathResolver.getPath(FrontPage.OPERATION_ERROR);
                }
                if (userDetail.getAvailablePredeposit() <= 0 && prePayTypeVal == 1) {
                    PaymentLog.error("预付款不足");
                    request.setAttribute("message", "预付款不足");
                    return PathResolver.getPath(FrontPage.OPERATION_ERROR);
                } else if (userDetail.getUserCoin() <= 0 && prePayTypeVal == 2) {
                    PaymentLog.error("金币不足");
                    request.setAttribute("message", "金币不足");
                    return PathResolver.getPath(FrontPage.OPERATION_ERROR);
                }
                
            walletPay = true;
        }


        PaymentLog.info(" ################## 用户端[legendshop]支付  ################### ");

        PaymentLog.info("===>payto params subNumbers={}, userId={},prePayTypeVal={},password_callback={},payTypeId={} "
                , subNumbers, securityDetail.getUserId(), prePayTypeVal, password_callback, payTypeId);

        String domainURL = propertiesUtil.getPcDomainName();
        String ip = IPHelper.getIpAddr(request);
        PaymentLog.info("payto ip={},domainURL={}", ip, domainURL);

        String subSettlementSn = CommonServiceUtil.getRandomSn();

        InitPayFromDto initPayFromDto = new InitPayFromDto(subSettlementSn, subNumbers, walletPay, prePayTypeVal, payTypeId, securityDetail.getUserId(),
        		securityDetail.getOpenId(), domainURL, null, subSettlementType, PaySourceEnum.PC.getDesc(), ip, paytoParam.getPayPctType(), paytoParam.isPresell());

        PaymentLog.log("===>initPayFromDto={}", JSONUtil.getJson(initPayFromDto));
        
		/*
		 * 首先先查询这个支付单据有没有已经支付成功的单据信息
		 */
        if (paymentUtil.checkRepeatPay(subNumbers, securityDetail.getUserId(), subSettlementType)) {
            PaymentLog.error("===>请勿重复支付");
            request.setAttribute("message", "请勿重复支付");
            return PathResolver.getPath(FrontPage.OPERATION_ERROR);
        }

        Map<String, Object> payment_result = paymentUtil.initPay(initPayFromDto);
        if (!Boolean.parseBoolean(payment_result.get("result").toString())) {
            request.setAttribute("message", payment_result.get("message"));
            PaymentLog.info(" 支付请求参数:{}", payment_result.get("message"));
            return PathResolver.getPath(FrontPage.OPERATION_ERROR);
        }


        SysPaymentForm paymentForm = (SysPaymentForm) payment_result.get("message");

        PaymentLog.info(" paymentForm {} ##########", JSONUtil.getJson(paymentForm));

        /**
         * 获取支付数据信息
         */
        payment_result = paymentService.payto(paymentForm.getProcessorEnum(), paymentForm);
        if (!Boolean.parseBoolean(payment_result.get("result").toString())) {
            PaymentLog.error(payment_result.get("message").toString());
            request.setAttribute("message", payment_result.get("message").toString());
            return PathResolver.getPath(FrontPage.OPERATION_ERROR);
        }

        /**
         * 初始化单据信息
         */
        paymentUtil.saveSubSettlement(paymentForm);
		
		/*if (PaymentProcessorEnum.WX_SACN_PAY.name().equals(payTypeId)) { // 如果是微信扫码支付
			request.setAttribute("subSettlementSn", subSettlementSn);
			request.setAttribute("code_url", payment_result.get("code_url"));
			request.setAttribute("totalFee", paymentForm.getTotalAmount());
			request.setAttribute("show_url", paymentForm.getShowUrl());
			PaymentLog.info("===> go WeiXinSacnCode");
			return PathResolver.getPath(request, response, FrontPage.WEIXIN_SACNCODE);
		}*/
        PaymentLog.log(" buildRequest={} ", payment_result.get("message"));
        request.setAttribute("payment_result", payment_result.get("message"));
        //清理未付款订单缓存
        subDao.cleanUnpayOrderCount(securityDetail.getUserId());
        return PathResolver.getPath(FrontPage.PAY_PAGE);
    }


    /**
     * 支付成功提示页面
     *
     * @param request
     * @param response
     * @param subSettlementSn
     * @param subSettlementType
     * @return
     */
    @RequestMapping(value = "/returnPay", method = RequestMethod.GET)
    public String payResult(HttpServletRequest request, HttpServletResponse response, @RequestParam String subSettlementSn, @RequestParam String subSettlementType) {
        PaymentLog.log("#################支付成功回调   ######################");
        PaymentLog.log("################# 商户系统内部的订单号={},单据类型={}  ######################", subSettlementSn, subSettlementType);
        if (subSettlementType.equals(SubSettlementTypeEnum.AUCTION_DEPOSIT.value())) {
            List<SubSettlementItem> subSettlementList = subSettlementItemService.getSettlementItem(subSettlementSn);
            if (AppUtils.isNotBlank(subSettlementList)) {
                AuctionDepositRec auctionDepositRec = auctionDepositRecService.getAuctionDepositRec(subSettlementList.get(0).getUserId(), subSettlementList.get(0).getSubNumber());
                return PathResolver.getRedirectPath("/auction/views/" + auctionDepositRec.getAId());
            }
        }
//        else if (subSettlementType.equals(SubSettlementTypeEnum.USER_PREPAID_RECHARGE.value())) {
//            return PathResolver.getPath(request, response, "/p/predeposit/account_balance", RedirectPage.VARIABLE);
//        }
        request.setAttribute("subSettlementType",subSettlementType);
        return PathResolver.getPath("/returnPay", FrontPage.VARIABLE);
    }

    @RequestMapping(value = "/payError", method = RequestMethod.GET)
    public String payError(HttpServletRequest request, HttpServletResponse response,
                           String message
    ) {
        PaymentLog.log("#################支付错误回调 {} ######################", message);
        if (AppUtils.isBlank(message)) {
            request.setAttribute("message", "提示信息： 交易错误,请记录您的支付订单号联系商家确认订单状态");
        } else {
            request.setAttribute("message", message);
        }
        return PathResolver.getPath(FrontPage.OPERATION_ERROR);
    }

    @RequestMapping(value = "/orderQuery/{subSettlementSn}", method = RequestMethod.POST)
    public
    @ResponseBody
    String orderQuery(HttpServletRequest request, HttpServletResponse response, @PathVariable String subSettlementSn) {
    	SecurityUserDetail user = UserManager.getUser(request);
        SubSettlement subSettlement = subSettlementService.getSubSettlement(subSettlementSn, user.getUserId());
        if (subSettlement == null) {
            return "未找到记录";
        }
        if (subSettlement.getIsClearing()) {
            return Constants.SUCCESS;
        }
        return "未支付成功";
    }
}
