UE.registerUI('dialog',function(editor,uiName){

    //创建dialog
    var dialog = new UE.ui.Dialog({
        //指定弹出层中页面的路径，这里只能支持页面,因为跟addCustomizeDialog.js相同目录，所以无需加路径
        id: "imgDialog",
        iframeUrl:contextPath + '/s/system/openImageSite/many',
        //需要指定当前的编辑器实例
        editor:editor,
        //指定dialog的名字
        name: uiName,
        //dialog的标题
        title: "图片上传",

        //指定dialog的外围样式
        cssRules:"width:960px;height:540px;",

        //如果给出了buttons就代表dialog有确定和取消
        buttons:[
            {
                className:'edui-okbutton',
                label:'确定',
                onclick:function () {
                    var _iframe = document.getElementById(dialog.id+'_iframe').contentWindow;
                    var    selectedImages = $(_iframe.document).find(".pic .check");
                    var srcList = new Array();
                    selectedImages.each( function( index, val ) {//找到选中的记录
                        srcList.push({"src":photoPrefix + $(this).attr("filePath")});
                    });

                    if(srcList.length ==0){
                        toastrWaring("请先选择图片");
                        return;
                    }
                    //console.log("srcList" + JSON.stringify(srcList));

                    editor.execCommand( 'insertimage', srcList );
                    dialog.close(true);
                }
            },
            {
                className:'edui-cancelbutton',
                label:'取消',
                onclick:function () {
                    dialog.close(false);
                }
            }
        ]});

    //参考addCustomizeButton.js
    var btn = new UE.ui.Button({
        name:"图片上传",
        title:"图片上传",
        //需要添加的额外样式，指定icon图标，这里默认使用一个重复的icon
        cssRules :'background-position: -726px -77px',
        onclick:function () {
            //渲染dialog
            dialog.render();
            dialog.open();
        }
    });

    return btn;
}/*index 指定添加到工具栏上的那个位置，默认时追加到最后,editorId 指定这个UI是那个编辑器实例上的，默认是页面上所有的编辑器都会添加这个按钮*/);