<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <%@ include file='/WEB-INF/pages/common/taglib.jsp' %>
    <title>完整demo </title>
    <script type="text/javascript" src="${contextPath}/resources/common/js/jquery-1.9.1.js"></script>
    <link href="${contextPath}/resources/templets/saas/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="${contextPath}/resources/templets/saas/css/bootstrap-theme.min.css">

    <c:set var="photoPrefix" value="${shopApi:getPhotoPrefix()}"></c:set>
    <c:set var="imagesSuffix" value="${shopApi:getImagesSuffix(1)}"></c:set>

    <script type = "text/javascript" >
        var contextPath = '${contextPath}';
        var photoPrefix = '${photoPrefix}';
    </script>
    <script src="${contextPath}/resources/templets/saas/js/common/common_alert.js"></script>
    <script type="text/javascript" charset="utf-8" src="${contextPath}/resources/plugins/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="${contextPath}/resources/plugins/ueditor/ueditor.all.js"></script>
    <script type="text/javascript" charset="utf-8" src="${contextPath}/resources/plugins/ueditor/customizeMultiUploadDialog.js"></script>
    <script type="text/javascript" charset="utf-8" src="${contextPath}/resources/plugins/ueditor/lang/zh-cn/zh-cn.js"></script>

    <script type = "text/javascript" >

        //实例化编辑器
        //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
        var ue = UE.getEditor('editor',{
            enterTag : 'br'
        });
    </script>
    <%--<style type="text/css">--%>
        <%--div {--%>
            <%--width: 100%;--%>
        <%--}--%>
    <%--</style>--%>
</head>
<body>
    <div>
       <script id="editor" type="text/plain" style="width:1000px;height:500px;"></script>
    </div>
</body>
</html>
