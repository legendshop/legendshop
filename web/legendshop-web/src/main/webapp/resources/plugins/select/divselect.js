(function($){
	$.extend($.fn, {
	divselect: function(){
		$(document).click(function(event){
			 //console.debug(event.target.className);
			   if(event.target.className!="kui-icon-dropdown" && event.target.className!="kui-dropdown-search"  && event.target.className!="kui-dropdown-search valid" ){
				   $(".kui-list").hide();
		       }
		});
		
		
		return this.each(function(i){
			var _this = $(this);
			  
			  $(".kui-icon-dropdown",_this).click(function(){
				  $(".kui-list").hide();
					var kuiList = $(".kui-list",_this);
					var showed = kuiList.css("display")=="none";
					if(showed){
						kuiList.show();
					}else{
						kuiList.slideUp("fast");
					}
				});
			  
				$(_this).delegate(".kui-list .kui-option","click",function(){
					var $this = $(this);
					var txt = $this.text();
					var caption = $(".kui-combobox-caption",_this);
					caption.val(txt);
					var value = $this.attr("data-value");
					if(value == -1){
						//把readonly去掉
						caption.removeAttr("readonly");
						caption.val("");
						caption.focus();
					}else{
						//把readonly加上
						caption.attr("readonly","readonly");
					}
					var selectElement = $(".keyPropClass option",_this);
					selectElement.val(value);
					selectElement.text(txt);
					$(".kui-list",_this).hide();
					
				});
		});
		}

	});
})(jQuery);