jQuery(document).ready(function(){
	$("#js_imglist .frm_checkbox_label input[type=checkbox]").change(function(){
		checkboxChange();
	});

	$("#js_all").change(function(){
		if($("#js_all").prop("checked")){
			$(".frm_checkbox_label input[type=checkbox]").prop("checked",true);
		}else{
			$(".frm_checkbox_label input[type=checkbox]").prop("checked",false);
		}
		checkboxChange();
	});

	$("#js_batchmove").click(function(){
		if($(this).hasClass("btn_disabled")){
			return;
		}else{
			var imgList = $("#js_imglist .frm_checkbox_label input[type=checkbox]:checked").get();
			var idStr="";
			for(var i=0;i<imgList.length;i++){
				if(i!=0){idStr+=",";}
				idStr+=$(imgList[i]).data("id");
			}

		        layer.open({
				  title :"所选图片移动到分组",
				  type: 1,
				  content:  $(".popover").html(), //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
				  area: ['300px', '180px'],
				  btn: ['确定','关闭'], //按钮
				  yes:function(index, layero){
					  var groupId = $("input[name='changeGroup']:checked").data("id");
						if(groupId==undefined){
							return false;
						}else{
							$.ajax({
								url : contextPath + "/admin/weixinNewstemplate/changeImgGroup",
								data : {
									"groupId" : groupId,"idStr":idStr
								},
								type : 'post',
								async : true, // 默认为true 异步
								dataType : 'json',
								success : function(data) {
									if (data == "OK") {
										 window.location.reload();
									}
									return true;
								}
							});
						}
				  }
				});
		}
	});

	$("#js_batchdel").click(function(){
		if($(this).hasClass("btn_disabled")){
			return;
		}else{
			layer.confirm("确定删除所选图片？", {
				 icon: 3
			     ,btn: ['确定','关闭'] //按钮
			   }, function(){
				   var imgList = $("#js_imglist .frm_checkbox_label input[type=checkbox]:checked").get();
					var idStr="";
					for(var i=0;i<imgList.length;i++){
						if(i!=0){idStr+=",";}
						idStr+=$(imgList[i]).data("id");
					}

					$.ajax({
						url : contextPath + "/admin/weixinNewstemplate/delImgs",
						data : {
							"idStr":idStr
						},
						type : 'post',
						async : true, // 默认为true 异步
						dataType : 'json',
						success : function(data) {
							window.location.reload();
						}
					});

			   });
		}
	});
});

function checkboxChange(){
	var len = $("#js_imglist .frm_checkbox_label input[type=checkbox]:checked").get().length;
	if(len>0){
		$("#js_batchmove").removeClass("btn_disabled");
		$("#js_batchdel").removeClass("btn_disabled");
	}else{
		$("#js_batchmove").addClass("btn_disabled");
		$("#js_batchdel").addClass("btn_disabled");
	}
}
function createGroup(){
	  layer.open({
		  title :"新增分组",
		  type: 1,
		  content:  '<div style="padding:10px;">分组名称：<input type="text" id="newGroupName"/></div><br />',
		  area: ['300px', '180px'],
		  btn: ['确定','关闭'], //按钮
		  yes:function(index, layero){
			  var groupName = $.trim($("#newGroupName").val());
				if(groupName.length>6||groupName==0){
					$("#newGroupName").focus();
					layer.msg("分组名字为1-6个字符",{icon:0});
					return false;
				}else	if (groupName != null && groupName != '') {
					$.ajax({
						url : contextPath + "/admin/weixinNewstemplate/saveImgGroup",
						data : {
							"groupName" : groupName
						},
						type : 'post',
						async : true, // 默认为true 异步
						dataType : 'json',
						success : function(data) {
							if (data == "OK") {
								 window.location.reload();
							} else {
								layer.msg("该分组已经存在",{icon:0});
							}
							return true;
						}
					});
				}
		  }
	  });
}

function renameGroup(groupId,groupName){
	 layer.open({
		  title :"编辑分组名称",
		  type: 1,
		  content:  '<div style="padding:10px;"><input type="text" id="newGroupName" value="'+groupName+'"/></div><br />',
		  area: ['300px', '180px'],
		  btn: ['确定','关闭'], //按钮
		  yes:function(index, layero){
			  var newGroupName = $.trim($("#newGroupName").val());
				if(groupName==newGroupName){
					return true;
				}
				if (newGroupName != null && newGroupName != '') {
					$.ajax({
						url : contextPath + "/admin/weixinNewstemplate/renameImgGroup",
						data : {
							"groupName" : newGroupName,"groupId":groupId
						},
						type : 'post',
						async : true, // 默认为true 异步
						dataType : 'json',
						success : function(data) {
							if (data == "OK") {
								 window.location.reload();
							} else {
								layer.msg("该分组已经存在",{icon:0});
								$("#newGroupName").focus();
							}
						}
					});
					return false;
				} else {
					layer.msg("分组名不能为空",{icon:0});
					$("#newGroupName").focus();
					return false;
				}
		  }
	 });
}

function delGroup(groupId,groupName){
	layer.confirm("仅删除分组，不删除图片，组内图片将自动归入未分组", {
		 icon: 3
	     ,btn: ['确定','关闭'] //按钮
	   }, function(){
		   $.ajax({
				url : contextPath + "/admin/weixinNewstemplate/delImgGroup",
				data : {
					"groupId":groupId
				},
				type : 'post',
				async : true, // 默认为true 异步
				dataType : 'json',
				success : function(data) {
					window.location=contextPath+"/admin/weixinNewstemplate/filePage"
				}
			});
	   });
}

function renameImg(imgId,imgName){
	 layer.open({
		  title :"编辑图片名称",
		  type: 1,
		  content:  '<div style="padding:10px;"><input type="text" id="newImgName" value="'+imgName+'"/></div><br />',
		  area: ['300px', '180px'],
		  btn: ['确定','关闭'], //按钮
		  yes:function(index, layero){
			  var newImgName = $.trim($("#newImgName").val());
				if(imgName==newImgName){
					return true;
				}
				if (newImgName != null && newImgName != '') {
					$.ajax({
						url : contextPath + "/admin/weixinNewstemplate/renameImg",
						data : {"imgName" : newImgName,"imgId":imgId},
						type : 'post',
						async : true, // 默认为true 异步
						dataType : 'json',
						success : function(data) {
							if (data == "OK") {
								 window.location.reload();
							}
						}
					});
					return false;
				} else {
					layer.msg("图片名不能为空",{icon:0);
					$("#newImgName").focus();
					return false;
				}
		  }
	 });
}

function moveImg(imgId,imgName){
	  layer.open({
		  title :"将该图片移动到分组",
		  type: 1,
		  content:  $(".popover").html(),
		  area: ['300px', '180px'],
		  btn: ['确定','关闭'], //按钮
		  yes:function(index, layero){
			  $.ajax({
					url : contextPath + "/admin/weixinNewstemplate/changeImgGroup",
					data : {
						"groupId" : groupId,"idStr":imgId
					},
					type : 'post',
					async : true, // 默认为true 异步
					dataType : 'json',
					success : function(data) {
						if (data == "OK") {
							 window.location.reload();
						}
						return true;
					}
				});
			}
		  }
	  });
}

function delImg(imgId,imgName){
	layer.confirm("确定删除图片 [ "+imgName+" ] ?", {
		 icon: 3
	     ,btn: ['确定','关闭'] //按钮
	   }, function(){
		   $.ajax({
				url : contextPath + "/admin/weixinNewstemplate/delImgs",
				data : {
					"idStr":imgId
				},
				type : 'post',
				async : true, // 默认为true 异步
				dataType : 'json',
				success : function(data) {
					window.location.reload();
				}
			});
	   });
}

function uploadFile(){
	$("#js_upload_btn").click();
	$('#js_upload_btn').change(function() {
		$('#js_upload').html('上传中...');
        $('#formFile').submit();
	});
}

function uploadSuccess() {
	//window.location.reload();
	window.location.href = contextPath +  "/admin/weixinNewstemplate/filePage?type="+type;
}

function pager(curPageNO){
    window.location=contextPath+"/admin/weixinNewstemplate/filePage?type="+type+"&curPageNO="+curPageNO;
}
