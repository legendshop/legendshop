//添加微信分组
function addGroup(){
	layer.open({
		  title :"新增分组",
		  type: 1,
		  content: '分组名称：<input type="text" id="newGroupName" maxlength="19"/><br />',
		  btn: ['确定','关闭'],
		  yes:function(index, layero){
			  var groupName=$("#newGroupName").val();
		    	if(groupName!=null && groupName!=''){
		    		var config=false;
			       	$.ajax({
						url: contextPath+"/admin/weixinGroup/save",
						data: {"newGroupName":groupName},
						type:'post',
						async : true, //默认为true 异步
						dataType:'json',
						success:function(data){
							if(data=="OK"){
								layer.msg("添加成功!",{icon:1},function(){
									window.location = contextPath+"/admin/weixinUser/query";
									config= true;
								});

							}else {
								layer.msg(data,{icon:2});
							}
							config= false;
						}
					});
			       	if(!config)
			       		return false;
		    	}else{
		    		layer.msg("分组名不能为空",{icon:0});
		    		return false;
		    	}
		  }
		});
   };

//变更修改分组
function changeUserGroup(userId,groupId,groupName){
	layer.open({
		  title :"变更分组",
		  type: 2,
		  content: contextPath+"/admin/weixinGroup/query/"+groupId,
		  btn: ['确定','关闭'],
		  yes:function(index, layero){
			  var groupid=$("#group option:selected").attr("value");
	    		var groupname=$("#group option:selected").text();
	    		if(groupName!=$.trim(groupname)){
		    		window.location=contextPath+"/admin/weixinUser/update/"+userId+"/"+groupid;
		    		layer.msg("分组修改成功",{icon:1});
		    		return true;
	    		}
	    		layer.msg("分组未做更改",{icon:0});
	    		return  false;

		  });
		});
}


//修改备注
function changeBzname(userid,bzname){
	var content='备注名称：<input type="text" id="name" value="'+bzname+'" maxlength="19"/><br />'
	layer.open({
		  title :"修改备注",
		  type: 1,
		  content: content,
		  btn: ['确定','关闭'],
		  yes:function(index, layero){
			  var name=$("#name").val();
		    	if(bzname!=name){
			    	if(name!=null && name!=''){
				       	$.ajax({
							url: contextPath+"/admin/weixinUser/update",
							data: {"id":userid,"bzname":name},
							type:'post',
							async : true, //默认为true 异步
							dataType:'json',
							success:function(data){
								if(data=="OK"){
									layer.msg("修改成功!",{icon:1});
									window.location = contextPath+"/admin/weixinUser/query";
								}else{
									layer.msg("修改失败!",{icon:2});
								}
							}
						});
			    	}else{
			    		layer.msg("备注名不能为空",{icon:0});
			    		return false;
			    	}
		    	}else{
		    		layer.msg("备注未做更改",{icon:0});
		    		return true;
		    	}
		  });
		  }
};
