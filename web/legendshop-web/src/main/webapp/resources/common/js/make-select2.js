	function makeSelect2(url,element,text,label,value){
			$("#"+element).select2({
		        placeholder: "选择"+text,
		        allowClear: true,
		        ajax: {  
		    	  url : url,
		    	  data: function (term,page) {
		                return {
		                	 q: term
		                };
		            },
	    			type : "GET",
	    			dataType : "JSON",
	    			delay: 500,
	                results: function (data, page, query) {
	                    return {
	                    	results:data
	                    };
	                },
	                cache: true
		      }, 
		      	initSelection: function (element, callback) {
		            callback({text: element.val()});//这里初始化data
		        },
		        formatInputTooShort: "请输入店铺名",
		        formatNoMatches: "没有匹配的店铺",
		        formatSearching: "查询中...",
		        formatResult: function(row) {//选中后select2显示的 内容
		            return row.text;
		        },formatSelection: function(row) { //选择的时候，需要保存选中的id
		            return row.text;//选择时需要显示的列表内容
		        }, 
		    });
	}
