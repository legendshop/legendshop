 //调用选色板
 function initColorEdit(selector){
	 $(selector).spectrum({
    showInput: true,
    className: "full-spectrum",
    showInitial: true,
    showPalette: true,
    showSelectionPalette: true,
    maxPaletteSize: 10,
    preferredFormat: "hex",
    localStorageKey: "spectrum.demo",
    allowEmpty: true,
    move: function (color) {

    },
    show: function () {

    },
    beforeShow: function () {

    },
    hide: function () {

    },
    change: function() {

    },
    palette: [
        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
        "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
    ]
});
 }
$(function(){
    $('.subject_admin_tag li').click(function(){
        $(this).addClass('am-active').siblings().removeClass('am-active');
        var _index=$(this).index();
        $('.subject_admin .subject_admin_con').eq(_index).addClass('subject_admin_check').siblings().removeClass('subject_admin_check');
    });
    $('.subAdmin_popup_tag a').click(function(){
        $(this).addClass('am-active').siblings().removeClass('am-active');
        var _index=$(this).index();
        $('.subject_admin_popup .subject_admin_com').eq(_index).addClass('subject_admin_check').siblings().removeClass('subject_admin_check');
    });
    $('.subject_admin_popup .cancel').click(function(){
        $('.popup_shade,.subject_admin_popup').hide();
    });
//    $('.add_commodity,.oper .edit').click(function(){
//    	$('.popup_shade,.subject_select_popup').show();
//    });

//    $(".subject_select_popup .save").click(function(){
//    	$('.subject_select_popup').hide();
//        $('.popup_shade,.subject_admin_popup').show();
//    });
    $('.subject_select_popup .cancel').click(function(){
        $('.popup_shade,.subject_select_popup').hide();
    });
});

$(document).ready(function() {
	initColorEdit(".color_edit");
	if(isTitleShow==1){
		$("#isTitleShow").attr("checked","checked");
		$("#isTitleShow").parent().parent().addClass("checkbox-wrapper-checked");
	}
	if(isIntroShow==1){
		$("#isIntroShow").attr("checked","checked");
		$("#isIntroShow").parent().parent().addClass("checkbox-wrapper-checked");
	}
	if(isIntroMShow==1){
		$("#isIntroMShow").attr("checked","checked");
		$("#isIntroMShow").parent().parent().addClass("checkbox-wrapper-checked");
	}

	//循环设置商品板块 的 横幅区显示的checkbox
	 var index = 1;
	while($("#moduleTitleShow"+index).val()!=undefined){
		if($("#moduleTitleShow"+index).attr("isShow")==1){
			$("#moduleTitleShow"+index).attr("checked","checked");
		}
		index++;
		moduleCount++;
	}

	KindEditor.options.filterMode=false;
	 KindEditor.create('textarea[name="customContent"]', {
			cssPath : path+'/resources/plugins/kindeditor/plugins/code/prettify.css',
			uploadJson : path+'/editor/uploadJson/upload;jsessionid='+cookieValue,
			fileManagerJson : path+'/editor/uploadJson/fileManager',
			allowFileManager : true,
			afterBlur:function(){this.sync();},
			width : '700px',
			height:'450px',
			afterCreate : function() {
				var self = this;
				KindEditor.ctrl(document, 13, function() {
					self.sync();
					document.forms['example'].submit();
				});
				KindEditor.ctrl(self.edit.doc, 13, function() {
					self.sync();
					document.forms['example'].submit();
				});
			}
	});

});

//自定义弹框
function alertMessage(mess){

		layer.confirm(mess,{
	  		 icon: 3
	  	     ,btn: ['确定','取消'] //按钮
	  	   }, function () {
	  		 return true;
		});
}

function searchThemeProd(_curPageNO){
	var _prodName = $(".themProdName").val();
	var categoryId = $("#prodCatId").val();
    var url = contextPath + "/admin/theme/productThemeLoad";
    $.ajax({
        url: url,
        type:'post',
		async : false, //默认为true 异步
        data: {
        	"name": _prodName,
        	"curPageNO":_curPageNO,
        	"categoryId" : categoryId
        },
        dataType: "html",
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
           $("#theme_goods_list").empty().append(result);
        }
    });

}
function pager(_curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	searchThemeProd(_curPageNO);
}

function goods_list_set(img, productId, name) {
	var ceaseVar = true;
    $(".floor_box_pls .floor_goods_info img").each(function () {
        if (jQuery(this).attr("id") == productId) {
            art.dialog.alert("已经存在该商品");
            ceaseVar = false;
        }
    });
    if (ceaseVar == false) {
        return;
    }
    var s = "<span class='floor_goods_info' ondblclick='dbDeleteThemeProd(this)'>"
		+ "<span class='floor_pro'>"
		+"<img id='"+ productId +"' src='"+photoPrex+img+"' />"
		+"</span>"
		+"<span class='floor_pro_name'>"+name+"</span>"
		+"</span>";

    $("#selProdId").val(productId);
    $(".floor_box_pls").html(s);
}

function dbDeleteThemeProd(_this) {
	$(_this).remove();
	$("#selProdId").val("");
	var str = '<span style="display: block;width: 100%;text-align: center;color:  #FFC7AC;line-height: 139px;"> 选择预览区</span>';
	$(".floor_box_pls").html(str);
}

//保存专题
function saveTheme() {
	var title = $.trim($("#title").val());
	var titleColor = $("#titleColor").val();
	var startTime = $("#startTime").val();
	var endTime = $("#endTime").val();
	var intro=$.trim($("#intro").val());
	var introColor = $("#introColor").val();
	var introMobile=$.trim($("#introMobile").val());
	var introMColor = $("#introMColor").val();
	if (title == null || title == ""
			|| title == undefined) {
		layer.alert('请输入标题！', {icon: 0});
		return;
	}
//	if ($.trim($("#themeDesc").val())=="") {
//		alertMessage("请输入描述!");
//		return;
//	}
//	if(titleColor==""){
//		alertMessage("请选择标题颜色");
//		return;
//	}
	if(intro=="" || introMobile==""){
		layer.alert('请输入简介！', {icon: 0});
		return;
	}
//	if(introColor=="" || introMColor==""){
//		alertMessage("请选择简介颜色");
//		return;
//	}
	if(startTime==""){
		$("#startTime").attr("name","");
	}else{
		$("#startTime").attr("name","startTime");
	}
	if(endTime==""){
		$("#endTime").attr("name","");
	}else{
		$("#endTime").attr("name","endTime");
	}

	if(startTime=="" || endTime==""){
		layer.alert('请输入开始日期和截止日期', {icon: 0});
		return;
	}

	if(startTime>=endTime){
		layer.alert('截止日期必须大于开始日期', {icon: 0});
		return;
	}

	if($("input[name='themePcImg']").val()==""){
		if($("#themePcImgFile").val()==""){
			layer.alert('请上传专题活动图(网页版)!', {icon: 0});
			return;
		}
	}

	if($("input[name='themeMobileImg']").val()==""){
		if($("#themeMobileImgFile").val()==""){
			layer.alert('请上传专题活动图(手机版)!', {icon: 0});
			return;
		}
	}

	if($("input[name='bannerPcImg']").val()==""){
		if($("#bannerPcImgFile").val()=="" && $("#bannerImgColor").val()==""){
			layer.alert('请上传专题横幅图(网页版)或者选择背景色!', {icon: 0});
			return;
		}
	}
	if($("input[name='bannerMobileImg']").val()==""){
		if($("#bannerMobileImgFile").val()==""  && $("#bannerImgColor").val()==""){
			layer.alert('请上传专题横幅图(手机版)或者选择背景色！', {icon: 0});
			return;
		}
	}
	if($("input[name='backgroundPcImg']").val()==""){
		if($("#backgroundPcImgFile").val()==""  && $("#backgroundColor").val()==""){
			layer.alert('请上传背景图(网页版)或者选择背景色!', {icon: 0});
			return;
		}
	}
	if($("input[name='backgroundMobileImg']").val()==""){
		if($("#backgroundMobileImgFile").val()==""  && $("#backgroundColor").val()==""){
			layer.alert('请上传背景图(手机版)或者选择背景色!', {icon: 0});
			return;
		}
	}
	if($("#themePcImgFile").val()==""){
		$("#themePcImgFile").attr("name","");
	}else{
		$("#themePcImgFile").attr("name","themePcImgFile");
	}
	if($("#themeMobileImgFile").val()==""){
		$("#themeMobileImgFile").attr("name","");
	}else{
		$("#themeMobileImgFile").attr("name","themeMobileImgFile");
	}
	if($("#bannerPcImgFile").val()==""){
		$("#bannerPcImgFile").attr("name","");
	}else{
		$("#bannerPcImgFile").attr("name","bannerPcImgFile");
	}
	if($("#bannerMobileImgFile").val()==""){
		$("#bannerMobileImgFile").attr("name","");
	}else{
		$("#bannerMobileImgFile").attr("name","bannerMobileImgFile");
	}
	if($("#backgroundPcImgFile").val()==""){
		$("#backgroundPcImgFile").attr("name","");
	}else{
		$("#backgroundPcImgFile").attr("name","backgroundPcImgFile");
	}
	if($("#backgroundMobileImgFile").val()==""){
		$("#backgroundMobileImgFile").attr("name","");
	}else{
		$("#backgroundMobileImgFile").attr("name","backgroundMobileImgFile");
	}
	if(	$("#attrSubmit").attr("buttonStatus")=="0"){
		$("#attrSubmit").attr("buttonStatus","1");
		//ajax提交专题form
		$("#form1").ajaxForm().ajaxSubmit(function(data){
			$("#attrSubmit").attr("buttonStatus","0");
			var result=$.parseJSON(data);
			if(result.retCode=="OK"){
				//图片回显
				if(result.themePcImg!=null){
					$("#themePcImg").attr("src",$("#imagesScale2").val().replace('PIC_DEFAL', result.themePcImg));
					$("input[name='themePcImg']").val(result.themePcImg);
				}
				if(result.themeMobileImg!=null){
					$("#themeMobileImg").attr("src",$("#imagesScale2").val().replace('PIC_DEFAL',result.themeMobileImg));
					$("input[name='themeMobileImg']").val(result.themeMobileImg);
				}
				if(result.bannerPcImg!=null){
					$("#bannerPcImg").attr("src",$("#imagesScale2").val().replace('PIC_DEFAL', result.bannerPcImg));
					$("input[name='bannerPcImg']").val(result.bannerPcImg);
				}
				if(result.bannerMobileImg!=null){
					$("#bannerMobileImg").attr("src",$("#imagesScale2").val().replace('PIC_DEFAL', result.bannerMobileImg));
					$("input[name='bannerMobileImg']").val(result.bannerMobileImg);
				}
				if(result.backgroundPcImg!=null){
					$("#backgroundPcImg").attr("src",$("#imagesScale2").val().replace('PIC_DEFAL', result.backgroundPcImg));
					$("input[name='backgroundPcImg']").val(result.backgroundPcImg);
				}
				if(result.backgroundMobileImg!=null){
					$("#backgroundMobileImg").attr("src",$("#imagesScale2").val().replace('PIC_DEFAL', result.backgroundMobileImg));
					$("input[name='backgroundMobileImg']").val(result.backgroundMobileImg);
				}
				//去除已保存文件
				$("#themePcImgFile").val("");
				$("#themeMobileImgFile").val("");
				$("#bannerPcImgFile").val("");
				$("#bannerMobileImgFile").val("");
				$("#backgroundPcImgFile").val("");
				$("#backgroundMobileImgFile").val("");

				$("#themeId").val(result.themeId);
				$("input[name='themeId']").val(result.themeId);
				//alertMessage("保存成功!");
				layer.alert("保存成功",{icon:1});
					$('#plate_editor').click();

			}else{
				layer.alert('保存失败!', {icon: 2});
			}
		});
	}else{
		layer.alert('正在保存，请稍后!', {icon: 0});
	}
}

function preview(){
	var themeId = $("#themeId").val();
	if(themeId==""){
		layer.alert('请先保存专题属性编辑!', {icon: 0});
		return;
	}
	var url= path+"/admin/theme/preview/"+themeId;
	window.open(url,'_blank');
}

function onLine()
{
	var themeId = $("#themeId").val();
	if(themeId==""){
		layer.alert('请先保存专题属性编辑!', {icon: 0});
		return;
	}

	var desc="确定要发布吗?";
	layer.confirm(desc,{
 		 icon: 3
 	     ,btn: ['确定','取消'] //按钮
 	   }, function () {
 		  jQuery.ajax({
				url:path+"/admin/theme/updataStatus/"+ themeId +"/1",
				type:'get',
				async : false, //默认为true 异步
				dataType : 'json',
				success:function(result){
					if('OK' == result){
						layer.alert('发布成功!', {icon: 1},function(){
							var url= path+"/admin/theme/query";
							window.location.href=url;
						});

			   		}else{
			   			layer.alert(result, {icon: 0});
			   			return;
			   		}
				}
		    });
	});
}


//保存专题版块
function saveThemeModule(index){
	var themeId = $("#themeId").val();
	var moduleMoreUrl1 = $("#moduleMoreUrl1").val();
	if(themeId==""){
		layer.alert('请先保存专题属性编辑!', {icon: 0});
		return;
	}
//	if(moduleMoreUrl1==""){
//		alertMessage("更多跳转不能为空!");
//		return;
//	}
	var moduleTitle = $("#moduleTitle"+index).val();
	var moduleTitleColor = $("#moduleTitleColor"+index).val();
	var moduleTitleBgColor = $("#moduleTitleBgColor"+index).val();
	var moduleMoreUrl = $("#moduleMoreUrl"+index).val();
	var moduleSeq = $.trim($("#moduleSeq"+index).val());
	$("#moduleSeq"+index).val(moduleSeq);
	if(moduleTitle==""){
		layer.alert('请输入标题!', {icon: 0});
		return;
	}
	var reg = /^[1-9]\d*$/;
	if(!reg.test(moduleSeq)){
		layer.alert('顺序请输入正整数!', {icon: 0});
		return;
	}
//	if(moduleTitleColor==""){
//		alertMessage("请选择字体颜色!");
//		return;
//	}
//	if(moduleMoreUrl==""){
//		alertMessage("请输入'更多'跳转链接");
//		return;
//	}
//	if(moduleSeq==""){
//		alertMessage("请输入顺序");
//		return;
//	}
//	var reg =  /^\d+$/;
//	if(!reg.test(moduleSeq)){
//		alertMessage("顺序 必须是数字!");
//		return;
//	}
//	if(moduleTitleBgColor==""){
//		if($("#titleBgPcimg"+index).val()=="" && $("#titleBgPcimgFile"+index).val()==""){
//			alertMessage("请上传版头背景图(网页版) 或者选择背景色!");
//			return;
//		}
//		if($("#titleBgMobileimg"+index).val()=="" && $("#titleBgMobileimgFile"+index).val()==""){
//			alertMessage("请上传版头背景图(手机版) 或者选择背景色!");
//			return;
//		}
//	}
	if($("#titleBgPcimgFile"+index).val()==""){
		$("#titleBgPcimgFile"+index).attr("name","");
	}else{
		$("#titleBgPcimgFile"+index).attr("name","titleBgPcimgFile");
	}
	if($("#titleBgMobileimgFile"+index).val()==""){
		$("#titleBgMobileimgFile"+index).attr("name","");
	}else{
		$("#titleBgMobileimgFile"+index).attr("name","titleBgMobileimgFile");
	}
	//ajax提交相应版块form
	$("#moduleForm"+index).ajaxForm().ajaxSubmit(function (data){
		var result=$.parseJSON(data);
		if(result.retCode=="OK"){
			$("#themeModuleId"+index).val(result.themeModuleId);
			if(result.titleBgPcimg!=null){
				$("#modulePcImg"+index).attr("src",$("#imagesScale2").val().replace('PIC_DEFAL', result.titleBgPcimg));
				$("#titleBgPcimg"+index).val(result.titleBgPcimg);
			}
			if(result.titleBgMobileimg!=null){
				$("#moduleMobileImg"+index).attr("src",$("#imagesScale2").val().replace('PIC_DEFAL', result.titleBgMobileimg));
				$("#titleBgMobileimg"+index).val(result.titleBgMobileimg);
			}
			$("#titleBgPcimgFile"+index).val("");
			$("#titleBgMobileimgFile"+index).val("");
			layer.alert("保存成功",{icon:1});
				$('#correlation_subject').click();

		}else{
			 layer.alert('保存失败，输入长度超出限制!', {icon: 2});
		}
	});

}

//新增版块
function addModule(){
	var themeId = $("#themeId").val();
	var str = "";
	str += "<form action=\""+path+"/admin/theme/saveThemeModule\" method=\"post\" id=\"moduleForm"+moduleCount+"\" enctype=\"multipart/form-data\">";
   	str += "<input type=\"hidden\" name=\"themeId\" value=\""+themeId+"\">";
    str += "<input type=\"hidden\" id=\"themeModuleId"+moduleCount+"\" name=\"themeModuleId\">";
    str += "<input type=\"hidden\" id=\"titleBgPcimg"+moduleCount+"\" name=\"titleBgPcimg\">";
    str += "<input type=\"hidden\" id=\"titleBgMobileimg"+moduleCount+"\" name=\"titleBgMobileimg\" >";
    str += "<h2>商品版块"+moduleCount+"<a href=\"javascript:delModule("+moduleCount+");\" class=\"fr\">删除</a></h2>";
    str += "<div class=\"subject_commodity_con\">";
	str += "<ul>";
	str += "<li><label><em style='color: #e5004f; margin-right: 5px;'>*</em>版块名称：</label><input id=\"moduleTitle"+moduleCount+"\"  name=\"title\" type='text' class=\"subject_attr_input\" />";
	str += "<div class=\"subject_attr_div\">字体颜色<input id=\"moduleTitleColor"+moduleCount+"\"  name=\"titleColor\" type='text' class=\"color_edit\" />";
	str += "         <input type=\"checkbox\" class=\"subject_attr_checkbox\"  id=\"moduleTitleShow"+moduleCount+"\"  value=\"1\"  name=\"isTitleShow\" />横幅区显示</div></li>";
	str += "         <li><div class='bg_box'><label>版头背景：</label><span>背景色&nbsp;&nbsp;</span><input  id=\"moduleTitleBgColor"+moduleCount+"\"  type='text' class=\"color_edit\"   name=\"titleBgColor\"/>";
	str += "         <br><label></label> <a href=\"javascript:void(0);\" class=\"subject_attr_file file-btn \" style='height: 26px;' onclick=\"uploadtitleBgPcimgFile("+moduleCount+")\">PC端<input type='file' name=\"titleBgPcimgFile\" id=\"titleBgPcimgFile"+moduleCount+"\" /></a>";
	str += "<span style=\"color:gray;\">(双击图片可删除图片)，建议尺寸：1190*120</span></div>";
	str += "<label></label> <img ondblclick=\"delModuleImg2(this);\" Width=\"800\" Height=\"80\" id=\"modulePcImg"+moduleCount+"\" alt=\"\" title=\"网页版版头背景图\" src=\"\">";
	str += "         <div class='bg_box'><label></label> <a href=\"javascript:void(0);\" class=\"subject_attr_file  file-btn\" style='height: 26px;' onclick=\"uploadtitleBgMobileimgFile("+moduleCount+")\" >手机端<input type='file' name=\"titleBgMobileimgFile\" id=\"titleBgMobileimgFile"+moduleCount+"\"/></a>";
	str += "<span style=\"color:gray;\">(双击图片可删除图片)，建议尺寸：750*120</span></div>";
	str += "<label></label> <img ondblclick=\"delModuleImg3(this);\" Width=\"350\" Height=\"120\" id=\"moduleMobileImg"+moduleCount+"\" alt=\"\" title=\"手机版版头背景图\" src=\"\"></li>";
	str += "         <li><label>“更多”跳转：</label><input id=\"moduleMoreUrl"+moduleCount+"\" name=\"moreUrl\" type='text' class=\"subject_attr_input\" /></li>";
	str += "         <li><label><em style='color: #e5004f; margin-right: 5px;'>*</em>顺序：</label><input id=\"moduleSeq"+moduleCount+"\"  name=\"seq\" type='text' class=\"subject_attr_input\" /></li>";
	str += "      </ul>";
    str += "</div>";
    str += "  <div class=\"subject_admin_but\"><a href=\"javascript:void(0);\"  class='criteria-btn' onclick=\"javascript:saveThemeModule('"+moduleCount+"');\">保存</a></div>";
    str += "<span style='padding-left:68px;float:left;'>添加商品：&nbsp;&nbsp;&nbsp;</span>"
    str += "<div class=\"subject_commodity_list\">";
    str += "  <ul class=\"clearfix\">";
    str += "    <li class=\"add_commodity\"><a href=\"javascript:popupSelect('"+moduleCount+"');\">+</a></li>";
    str += "  </ul>";
   	str += " </div>";
	str += "</form>";
	$("#allModuleDiv").append(str);
	initColorEdit("#moduleTitleColor"+moduleCount);
	initColorEdit("#moduleTitleBgColor"+moduleCount);
	moduleCount++;
}

//删除版块
function delModule(index){

	var layerIndex=layer.confirm('确定删除该版块？',{
 		 icon: 3
 	     ,btn: ['确定','取消'] //按钮
 	   }, function () {
 		  var themeModuleId = $("#themeModuleId"+index).val();
	 		if(themeModuleId!=""){
	 			$.ajax({
	 				url: path+"/admin/theme/delThemeModule",
	 				data: {"themeModuleId":themeModuleId},
	 				type:'post',
	 				async : true, //默认为true 异步
	 				dataType : 'json',
	 				success:function(data){
	 					$("#moduleForm"+index).remove();
	 				}
	    		 });
	 		}else{
	 			$("#moduleForm"+index).remove();
	 		}
	 		layer.close(layerIndex);
	        return true;
	});

}

//保存版块商品
function saveModuleProd(){
	if($.trim($("#prodName").val())==""){
		 layer.alert('商品名称不能为空!', {icon: 0});
		 return;
	}
	if($("#imgFile").val()==""){
		$("#imgFile").attr("name","");
	}else{
		$("#imgFile").attr("name","imgFile");
	}
	if($("#angleIconFile").val()==""){
		$("#angleIconFile").attr("name","");
	}else{
		$("#angleIconFile").attr("name","angleIconFile");
	}
	$("#modulePrdForm").ajaxForm().ajaxSubmit(function (data){
		var result=$.parseJSON(data);
		if(result.retCode=="OK"){
			if($("#prd"+result.themeModuleProd.modulePrdId).length>0){
				$("#prd"+result.themeModuleProd.modulePrdId+" .prd_img").attr("src",$("#imagesScale8").val().replace('PIC_DEFAL', result.themeModuleProd.img));
				$("#prd"+result.themeModuleProd.modulePrdId+" .prd_name").html(result.themeModuleProd.prodName);
				$("#prd"+result.themeModuleProd.modulePrdId+" .tag").css("background-image","url("+$("#imagesScale3").val().replace('PIC_DEFAL', result.themeModuleProd.angleIcon)+")");
			}else{
				var str = "";
				str += "<li id=\"prd"+result.themeModuleProd.modulePrdId+"\">";
				str += "  <div class=\"subject_list_img\">";
				str += "	<a target=\"_blank\" href=\""+path+"/views/"+result.themeModuleProd.prodId+"\"><img class=\"prd_img\" src=\""+$("#imagesScale8").val().replace('PIC_DEFAL', result.themeModuleProd.img)+"\"></a>";
				str += "	<div class=\"tag\" style=\"background-image:url("+$("#imagesScale3").val().replace('PIC_DEFAL', result.themeModuleProd.angleIcon)+");background-repeat: no-repeat;\"></div>";
				str += "  </div>";
				str += "  <div class=\"subject_list_details\">";
				str += "	<h3 class=\"prd_name\">"+result.themeModuleProd.prodName+"</h3>";
				str += "	<div class=\"subject_list_price\"><span>￥"+result.cash+"</span></div>";
				str += "  </div>";
				str += "  <div class=\"oper\"><a href=\"javascript:delModuleProd('"+result.themeModuleProd.modulePrdId+"');\" class=\"delete\">删除</a><a href=\"javascript:editModuleEdit('"+result.themeModuleProd.modulePrdId+"','"+result.cash+"');\" class=\"edit\">编辑</a></div>";
				str += "</li>";
				$("#moduleForm"+currIndex+" .subject_commodity_list .add_commodity").before(str);
			}
			$('.popup_shade,.subject_admin_popup').hide();
		}else{
			layer.msg('保存失败!', {icon: 2});
		}
	});
}

//查询商品信息
function getProdInfo(){
	var prodId = $.trim($("#selProdId").val());
	if(prodId==""){
		return;
	}
	var reg1 =  /^\d+$/;
	if(!reg1.test(prodId)){
		layer.msg('请输入正整数!', {icon: 0});
		return;
	}
	$("#imgFile").val("");
	$("#angleIconFile").val("");
	$.ajax({
			url: path+"/admin/theme/getProdInfo",
			data: {"prodId":prodId},
			type:'post',
			async : true, //默认为true 异步
			dataType : 'json',
			success:function(result){
				if(result.retCode=="OK"){
					$("#prodId").val(result.prodInfo.prodId);
					$("#prodName").val(result.prodInfo.name);
					$("#prodCash").val(result.prodInfo.cash+"元");
					$("#modulePrdImg").val(result.prodInfo.pic);
					$("#modulePrdAngleIcon").val("");
					$("#prodImg").attr("src",$("#imagesScale8").val().replace('PIC_DEFAL', result.prodInfo.pic));
					$("#isSoldOut").attr("checked",false);
					$("#angleIconImg").attr("src","");
					$("#delIconBtn").hide();
				  	$('.subject_select_popup').hide();
					$('.popup_shade,.subject_admin_popup').show();
				}else if(result.retCode=="NONE"){
					 layer.msg('商品不存在，请输入正确的商品编号!', {icon: 2});
				}else if(result.retCode=="OFF"){
					 layer.msg('该商品不是上架状态!', {icon: 2});
				}
			}
	 });
}

//点+号，弹框
function popupSelect(index){
	currIndex = index;
	var moduleId = $("#themeModuleId"+index).val();
	if(moduleId==""){
		layer.msg('请先保存板块!', {icon: 0});
	}else{
		$("#moduleId").val(moduleId);
		$("#modulePrdId").val("");
		$('.popup_shade,.subject_select_popup').show();
	}
}

//加载分类用于选择
function loadCategoryDialog() {
    layer.open({
		title :"选择父类",
		id:"prodCat",
		type: 2,
		content: contextPath+"/admin/product/loadAllCategory/P",
		area: ['280px', '380px']
	});
}


function clearCategoryCallBack() {
    $("#prodCatId").val("");
    $("#prodCatName").val("");
}

/**选择分类*/
function loadCategoryCallBack(id, catName) {
    $("#prodCatId").val(id);
    $("#prodCatName").val(catName);
}


//编辑商品
function editModuleEdit(modulePrdId,cashStr){
	$("#imgFile").val("");
	$("#angleIconFile").val("");
	$.ajax({
			url: path+"/admin/theme/getModuleProdInfo",
			data: {"modulePrdId":modulePrdId},
			type:'post',
			async : true, //默认为true 异步
			dataType : 'json',
			success:function(result){
			if(result.retCode=="OK"){
				$("#modulePrdId").val(modulePrdId);
				$("#moduleId").val(result.themeModuleProd.themeModuleId);
				$("#prodId").val(result.themeModuleProd.prodId);
				$("#prodName").val(result.themeModuleProd.prodName);
				$("#prodCash").val(cashStr);
				$("#modulePrdImg").val(result.themeModuleProd.img);
				$("#modulePrdAngleIcon").val(result.themeModuleProd.angleIcon);
				$("#prodImg").attr("src",$("#imagesScale4").val().replace('PIC_DEFAL', result.themeModuleProd.img));
				if(result.themeModuleProd.angleIcon!=""){
					$("#delIconBtn").show();
				}else{
					$("#delIconBtn").hide();
				}
				$("#angleIconImg").attr("src",$("#imagesScale3").val().replace('PIC_DEFAL', result.themeModuleProd.angleIcon));
				if(result.themeModuleProd.isSoldOut==1){
					$("#isSoldOut").attr("checked","checked");
				}else{
					$("#isSoldOut").attr("checked",false);
				}
				$('.popup_shade,.subject_admin_popup').show();
			}else{
				layer.alert('获取商品信息失败!', {icon: 2});
			}
			}
	 });
}

//删除商品
function delModuleProd(modulePrdId){

	 var index= layer.confirm('确定删除该版块商品？',{
  		 icon: 3
  	     ,btn: ['确定','取消'] //按钮
  	   }, function () {
  		 $.ajax({
				url: path+"/admin/theme/delModuleProd",
				data: {"modulePrdId":modulePrdId},
				type:'post',
				async : true, //默认为true 异步
				dataType : 'json',
				success:function(result){
					if(result.retCode=="OK"){
						$("#prd"+modulePrdId).remove();
						layer.close(index);
					}else{
						 layer.alert('删除板块商品失败!', {icon: 2});
					}
				}
 		 });
	});
}

//保存相关专题
function saveThemeRelated(){
	var themeId = $("#themeId").val();
	if(themeId==""){
		layer.alert('请先保存专题属性编辑!', {icon: 0});
		return;
	}
	if($("#img1").val()=="" && $("#relatedImgFile1").val()==""){
		layer.alert('请上传相关专题1的图片!', {icon: 0});
		return;
	}
	// if($.trim($("#relatedLink1").val())=="" || $.trim($("#relatedMLink1").val())==""){
	// 	layer.alert('请填写专题1的链接!', {icon: 0});
	// 	return;
	// }
	if($("#img2").val()=="" && $("#relatedImgFile2").val()==""){
		layer.alert('请上传相关专题2的图片!', {icon: 0});
		return;
	}
	// if($.trim($("#relatedLink2").val())=="" || $.trim($("#relatedMLink2").val())==""){
	// 	layer.alert('请填写专题2的链接!', {icon: 0});
	// 	return;
	// }
	if($("#img3").val()=="" && $("#relatedImgFile3").val()==""){
		layer.alert('请上传相关专题3的图片!', {icon: 0});
		return;
	}
	// if($.trim($("#relatedLink3").val())=="" || $.trim($("#relatedMLink3").val())==""){
	// 	layer.alert('请填写专题3的链接!', {icon: 0});
	// 	return;
	// }
	if($("#relatedImgFile1").val()==""){
		$("#relatedImgFile1").attr("name","");
	}else{
		$("#relatedImgFile1").attr("name","imgFile1");
	}
	if($("#relatedImgFile2").val()==""){
		$("#relatedImgFile2").attr("name","");
	}else{
		$("#relatedImgFile2").attr("name","imgFile2");
	}
	if($("#relatedImgFile3").val()==""){
		$("#relatedImgFile3").attr("name","");
	}else{
		$("#relatedImgFile3").attr("name","imgFile3");
	}
	$("#relatedForm").ajaxForm().ajaxSubmit(function (data){
		var result=$.parseJSON(data);
		if(result.retCode=="OK"){
			$("#relatedImgFile1").val("");
			$("#relatedImgFile2").val("");
			$("#relatedImgFile3").val("");
			if(result.img1!=null){
				$("#img1").val(result.img1);
				$("#relatedImg1").attr("src",$("#imagesScale2").val()+result.img1);
			}
			if(result.img2!=null){
				$("#img2").val(result.img2);
				$("#relatedImg2").attr("src",$("#imagesScale2").val()+result.img2);
			}
			if(result.img3!=null){
				$("#img3").val(result.img3);
				$("#relatedImg3").attr("src",$("#imagesScale2").val()+result.img3);
			}
			if(result.relatedId1!=null){
				$("#relatedId1").val(result.relatedId1);
			}
			if(result.relatedId2!=null){
				$("#relatedId2").val(result.relatedId2);
			}
			if(result.relatedId3!=null){
				$("#relatedId3").val(result.relatedId3);
			}
			layer.alert("保存成功",{icon:1});
				$('#preview_release').click();

		}else{
			 layer.alert('保存失败!', {icon: 2});
		}
	});
}

//删除专题相关图片
function delThemeImg(type,fileInput){
	if(type=="themePcImg"){
		if($("input[name='themePcImg']").val()==""){
			return;
		}
	}else if(type=="themeMobileImg"){
		if($("input[name='themeMobileImg']").val()==""){
			return;
		}
	}
	else if(type=="bannerPcImg"){
		if($("input[name='bannerPcImg']").val()==""){
			return;
		}
	}
	else if(type=="bannerMobileImg"){
		if($("input[name='bannerMobileImg']").val()==""){
			return;
		}
	}
	else if(type=="backgroundPcImg"){
		if($("input[name='backgroundPcImg']").val()==""){
			return;
		}
	}
	else if(type=="backgroundMobileImg"){
		if($("input[name='backgroundMobileImg']").val()==""){
			return;
		}
	}


	layer.confirm('确定删除该图片？',{
 		 icon: 3
 	     ,btn: ['确定','取消'] //按钮
 	   }, function () {
 		  $.ajax({
				url: path+"/admin/theme/delThemeImg",
				data: {"themeId":$("#themeId").val(),"type":type},
				type:'post',
				async : true, //默认为true 异步
				dataType : 'json',
				success:function(result){
					if(result.retCode=="OK"){
						if(type=="themePcImg"){
							$("#themePcImg").attr("src","");
							$("input[name='themePcImg']").val("");
						}else if(type=="themeMobileImg"){
							$("#themeMobileImg").attr("src","");
							$("input[name='themeMobileImg']").val("");
						}
						else if(type=="bannerPcImg"){
							$("#bannerPcImg").attr("src","");
							$("input[name='bannerPcImg']").val("");
						}
						else if(type=="bannerMobileImg"){
							$("#bannerMobileImg").attr("src","");
							$("input[name='bannerMobileImg']").val("");
						}
						else if(type=="backgroundPcImg"){
							$("#backgroundPcImg").attr("src","");
							$("input[name='backgroundPcImg']").val("");
						}
						else if(type=="backgroundMobileImg"){
							$("#backgroundMobileImg").attr("src","");
							$("input[name='backgroundMobileImg']").val("");
						}
						layer.alert('删除图片成功!', {icon: 1},function(){
							window.location.reload(true);
						});

					}else{
						layer.alert('删除图片失败!', {icon: 2});
					}
				}
  		 });
	});
}


//删除图片
function delCleanImg(obj){
	obj.src	="";
	$("#bannerPcImgFile").val("");
}

function delCleanImg2(obj){
	obj.src	="";
	$("#bannerMobileImgFile").val("");
}

function delCleanImg3(obj){
	obj.src	="";
	$("#backgroundPcImgFile").val("");
}

function delCleanImg4(obj){
	obj.src	="";
	$("#backgroundMobileImgFile").val("");
}
function delModuleImg2(obj){
	obj.src	="";
	$("#titleBgPcimgFile1").val("");
}
function delModuleImg3(obj){
	obj.src	="";
	$("#titleBgMobileimgFile1").val("");
}

function delModuleImg4(type,index){
	if(type=="titleBgPcimg"){
		$("#modulePcImg"+index).attr("src","");
		$("#titleBgPcimgFile1"+index).val("");
	}else if(type=="titleBgMobileimg"){
		$("#moduleMobileImg"+index).attr("src","");
		$("#titleBgMobileimgFile1"+index).val("");
	}
}

//删除板块相关图片
function delModuleImg(type,index){
	if(type=="titleBgPcimg"){
		if($("#titleBgPcimg"+index).val()==""){
			return;
		}
	}else if(type=="titleBgMobileimg"){
		if($("#titleBgMobileimg"+index).val()==""){
			return;
		}
	}
	var themeModuleId = $("#themeModuleId"+index).val();

	layer.confirm('确定删除该图片？',{
 		 icon: 3
 	     ,btn: ['确定','取消'] //按钮
 	   }, function () {
 		  $.ajax({
				url: path+"/admin/theme/delModuleImg",
				data: {"themeModuleId":themeModuleId,"type":type},
				type:'post',
				async : true, //默认为true 异步
				dataType : 'json',
				success:function(result){
					if(result.retCode=="OK"){
						if(type=="titleBgPcimg"){
							$("#titleBgPcimg"+index).val("");
							$("#modulePcImg"+index).attr("src","");
						}else if(type=="titleBgMobileimg"){
							$("#titleBgMobileimg"+index).val("");
							$("#moduleMobileImg"+index).attr("src","");
						}
						layer.alert('删除图片成功!', {icon: 1});
					}else{
						layer.alert('删除图片失败!', {icon: 2});
					}
				}
  		 });
	});
}

//删除角标图片
function delAngleIcon(){
	var modulePrdId = $("#modulePrdId").val();

	layer.confirm('确定删除角标图片？',{
 		 icon: 3
 	     ,btn: ['确定','取消'] //按钮
 	   }, function () {
 		  $.ajax({
				url: path+"/admin/theme/delAngleIcon",
				data: {"modulePrdId":modulePrdId},
				type:'post',
				async : true, //默认为true 异步
				dataType : 'json',
				success:function(result){
					if(result.retCode=="OK"){
						$("#prd"+modulePrdId+" .prd_icon").attr("src","");
						$("#modulePrdAngleIcon").val("");
						$("#angleIconImg").attr("src","");
						$("#delIconBtn").hide();
						layer.alert('删除图片成功！', {icon: 1});
					}else{
						 layer.alert('删除图片失败！', {icon: 2});
					}
				}
  		 });
	});
}

$(function() {
	// 为 专题/精选 单选按钮绑定点击事件
	$('input[name="templateType"]').each(function() {
		$(this).on("click", function() {
			var select_value = $(this).val();
			if (select_value == 2) {
				$("#page_title").html("精选编辑页面");
				$("#plate_editor").css("display", "none");
			} else {
				$("#page_title").html("专题编辑页面");
				$("#plate_editor").css("display", "block");
			}
		});
	});
	// 初始
	if($('input[name="templateType"]:checked').val() == 2){
		$("#page_title").html("精选编辑页面");
		$("#plate_editor").css("display", "none");
	}

	$("#themePcImgFile").uploadPreview({ Img: "themePcImg", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
	$("#themeMobileImgFile").uploadPreview({ Img: "themeMobileImg", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
	$("#bannerPcImgFile").uploadPreview({ Img: "bannerPcImg", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
	$("#bannerMobileImgFile").uploadPreview({ Img: "bannerMobileImg", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
	$("#backgroundPcImgFile").uploadPreview({ Img: "backgroundPcImg", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
	$("#backgroundMobileImgFile").uploadPreview({ Img: "backgroundMobileImg", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
	$("#relatedImgFile1").uploadPreview({ Img: "relatedImg1", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
	$("#relatedImgFile2").uploadPreview({ Img: "relatedImg2", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
	$("#relatedImgFile3").uploadPreview({ Img: "relatedImg3", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
});
function uploadtitleBgPcimgFile(index){
	$("#titleBgPcimgFile"+index).uploadPreview({ Img: "modulePcImg"+index, Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
}
function uploadtitleBgMobileimgFile(index){
	$("#titleBgMobileimgFile"+index).uploadPreview({ Img: "moduleMobileImg"+index, Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
}


/**
 * *介绍:基于JQUERY扩展,图片上传预览插件 目前兼容浏览器(IE 谷歌 火狐) 不支持safari
 *插件网站:http://keleyi.com/keleyi/phtml/image/16.htm
 *参数说明: Img:图片ID;Width:预览宽度;Height:预览高度;ImgType:支持文件类型;Callback:选择文件显示图片后回调方法;
 *使用方法:
 <div>
 <img id="themePcImg" width="120" height="120" /></div>
 <input type="file" id="themePcImgFile" />
 把需要进行预览的IMG标签外 套一个DIV 然后给上传控件ID给予uploadPreview事件
 $("#themePcImgFile").uploadPreview({ Img: "themePcImg", Width: 120, Height: 120, ImgType: ["gif", "jpeg", "jpg", "bmp", "png"], Callback: function () { }});
*/

jQuery.fn.extend({
    uploadPreview: function (opts) {
        var _self = this,
            _this = $(this);
        opts = jQuery.extend({
            Img: "ImgPr",
            Width: 100,
            Height: 100,
            ImgType: ["gif", "jpeg", "jpg", "bmp", "png"],
            Callback: function () {}
        }, opts || {});
        _self.getObjectURL = function (file) {
            var url = null;
            if (window.createObjectURL != undefined) {
                url = window.createObjectURL(file)
            } else if (window.URL != undefined) {
                url = window.URL.createObjectURL(file)
            } else if (window.webkitURL != undefined) {
                url = window.webkitURL.createObjectURL(file)
            }
            return url
        };
        _this.change(function () {
            if (this.value) {
                if (!RegExp("\.(" + opts.ImgType.join("|") + ")$", "i").test(this.value.toLowerCase())) {
                    layer.alert("选择文件错误,图片类型必须是" + opts.ImgType.join("，") + "中的一种",{icon:0});
                    this.value = "";
                    return false
                }else if(!checkImgSize(this,5*1024)){
					this.value = "";
					$("#" + opts.Img).attr('src', "");
					return false;
                }
                if ($.browser.msie) {
                    try {
                        $("#" + opts.Img).attr('src', _self.getObjectURL(this.files[0]))
                    } catch (e) {
                        var src = "";
                        var obj = $("#" + opts.Img);
                        var div = obj.parent("div")[0];
                        _self.select();
                        if (top != self) {
                            window.parent.document.body.focus()
                        } else {
                            _self.blur()
                        }
                        src = document.selection.createRange().text;
                        document.selection.empty();
                        obj.hide();
                        obj.parent("div").css({
                            'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale)',
                            'width': opts.Width + 'px',
                            'height': opts.Height + 'px'
                        });
                        div.filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = src
                    }
                } else {
                    $("#" + opts.Img).attr('src', _self.getObjectURL(this.files[0]))
                }
                opts.Callback()
            }
        })
    }
});

