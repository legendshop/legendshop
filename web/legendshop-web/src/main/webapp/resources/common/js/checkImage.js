/**
 * 检查文件类型
 * @param ths file对象
 * @return 是否符合规格
 */
function checkImgType(ths) {
	try {
		var obj_file = $(ths).get(0).files;
	    for(var i=0;i<obj_file.length;i++){
	    	if (!/\.(JPEG|BMP|GIF|JPG|PNG)$/.test(obj_file[i].name.toUpperCase())) {
				layer.msg("仅支持JPG、GIF、PNG、JPEG、BMP格式",{icon:0});
				$(ths).val("");
				return false;
			}
		}
	} catch (e) {
	}
	
	return true;
}

/**
 * 检查文件大小
 * @param ths file对象
 * @param limitSize 限制大小(k)
 * @return 是否符合规格 
 */
function checkImgSize(ths,limitSize){
	try {
		var maxsize = limitSize*1024;
		 var msgSize = limitSize+"K";
		 if(limitSize>=1024){
			 msgSize = limitSize/1024+"M";
		 }
		 var errMsg = "上传的图片不能超过"+msgSize;
	     var obj_file = $(ths).get(0).files;
	     
	     for(var i=0;i<obj_file.length;i++){
			if(obj_file[i].size>maxsize){
				layer.msg(errMsg,{icon:0});
				$(ths).val("");
				return false;
			}
		}
	} catch (e) {
		
	}
     return true;
}


