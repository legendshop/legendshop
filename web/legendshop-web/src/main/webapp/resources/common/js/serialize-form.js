/** 
author : 关开发
将表单序列化为对象,用于ajax发送表单数据
**/
$.fn.extend({
	serializeObject : function(){
		var $form = $(this);
		var o = {};
	    $.each($form.serializeArray(), function (index) {
	    	if (this['value'] && this['value'].length > 0){
		        if (o[this['name']]) {
		            o[this['name']] = o[this['name']] + "," + this['value'];
		        } else {
		            o[this['name']] = this['value'];
		        }
	    	}
	    });
	    return o;
	}
});