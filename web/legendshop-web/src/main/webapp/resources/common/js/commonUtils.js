/**
 * 描述: 一个基于jQuery的工具类,集齐了常用的一些功能.
 * 特别是简化了ajax请求,表单的异步提交,列表异步加载,消息提示,页面跳转等功能
 * 版本 1.0
 * 作者: 开发很忙
 * 最后更新: 2017-02-16 9:18
 */
;(function($){

	//全局属性 TODO 此处属性应该要暴露他的访问方法
	var config = {
		contextPath: contextPath || "/",
		body: $("body"),
		loadingImg: $(".loading-img"),
		loadingBg: $(".loading-bg"),
		loginPage: contextPath + "/login",
		indexPage: contextPath
	}

	//一些常量
	var constant = {
		success: "OK",
		fail: "fail",
		error: "error",
		notFound: "Not Found",
		noLogin: "noLogin",
		illegal: "illegal",
		other: "other"
	}

	//全局默认选项参数
	var defaults = {
		//ajax默认参数
		ajaxOptions: {
			type: "GET",
			data: {},
			dataType: "JSON",
			async: true,
			beforeSend: function(xhr){
				$.loading.showLoading();
			},
			complete: function(xhr,status){
				$.loading.hideLoading();
			},
			success: function(result, status, xhr){
				if(result === constant.success){
					$.remindUtils.showAlert("恭喜您, 操作成功!", function(){
						window.location.reload(true);
					});
				}else if(result === constant.fail){
					if(typeof art !== "undefined"){
						layer.msg("对不起,系统出现异常,请稍后重试!",{icon: 2});
					}else{
						layer.alert("对不起,系统出现异常,请稍后重试!",{icon: 2});
					}
				}else{
					layer.alert(result);
				}
			}
		},
	};

	//继承jquery全局方法
	$.extend($, {
		ajaxUtils:{
			//发送POST请求
			sendPost: function(url, data, sucessCallback, dataType, options){
				var defaultOP = {
					url: url,
					type: "POST",
					data: data || {},
					dataType: dataType,
					beforeSend:function(){
						loading= layer.msg('请稍候……', { icon: 16, shade: 0.01,shadeClose:false,time:60000 });
					},
					complete:function(){
						layer.close(loading);
					},
					success: sucessCallback,

				};
				var settings = $.extend({}, defaults.ajaxOptions, defaultOP, options);
				$.ajax(settings);
			},

			//发送GET请求
			sendGet: function(url, data, sucessCallback, dataType, options){
				var defaultOP = {
					url: url,
					data: data || {},
					dataType: dataType,
					success: sucessCallback
				};
				var settings = $.extend({}, defaults.ajaxOptions, defaultOP, options);
				$.ajax(settings);
			},

			//异步加载列表
			loadList: function(url, params, selector){
				var defaultOP = {
					url: url,
					data: params || {},
					dataType: "HTML",
					error: function(xhr, status, error){
						//TODO 此处代码待补全
						if(error === constant.noLogin){

						}else if(error === constant.illegal){

						}else if(error === constant.other){

						}else if(error === constant.notFound){
							layer.msg("对不起,找不到页面!",{icon: 2});
						}else if(error === constant.other){

						}else{
							layer.msg("status: " + status + ", error: " + error,{icon: 2});
						}
					},
					success: function(result, status, xhr){
						if(!selector){
							selector = $("#listBox");
						}
						$("#" + selector).html(result);
					}
				};
				var settings = $.extend({}, defaults.ajaxOptions, defaultOP);
				$.ajax(settings);
			}
		},

		//页面跳转方法
		pageJumps: {
			//返回上一级,TODO 目前返回上一级的策略是:如果当前页面没有上一级,则关闭当前页面
			back: function(){
				var backURL = document.referrer;
				if(!backURL){
					if(window.confirm("当前页面没有上一级,点击返回就是关闭当前页面,您确定要关闭?")){
						window.close();
					}
					return;
				}
				window.location.href = backURL;
			},
			//跳转到指定URL
			go: function(url){
				window.location.href = url;
			}
		},
		//表单工具
		formUtils: {
			//将表单序列化为一个JSON对象
			serializeObject : function(form){
				var o = {};
				$.each(form.serializeArray(), function (index) {
					if (this['value'] && this['value'].length > 0){
						if (o[this['name']]) {
							o[this['name']] = o[this['name']] + "," + this['value'];
						} else {
							o[this['name']] = this['value'];
						}
					}
				});
				return o;
			},

			//异步提交普通表单
		    ajaxSubmit: function(form, successCallback, options){
				var url = form.action;
				var params = $(form).serialize();

				$.ajaxUtils.sendPost(url, params, successCallback, "JSON", options);
			},

			//异步提交使用FormData构造的表单数据,目的为了实现异步提交包含二进制数据的表单
			ajaxSubmitFormData: function(form, successCallback, options){
				var url = form.action;

				var formData = new FormData(form);

				$.ajaxUtils.sendPost(url, formData, successCallback, "JSON", $.extend({processData: false, contentType: false}, options));
			}
		},
		//消息提示工具
		remindUtils: {
			showAlert: function(msg, callBack){
				if(typeof art !== "undefined"){
					layer.alert(msg, callBack || function(){});
				}else{
					layer.alert(msg);
					callBack && callBack();
				}
			},
			showTips: function(msg, callBack, time){
				layer.msg(msg);
				if(callBack){
					if(!time){
						time = 1000;
					}
					setTimeout(callBack, time);
				}
			}
		},
		//异步加载loading效果
		loading: {
			showLoading: function(){
				var body = config.body;
				var loadingImg = config.loadingImg;
				var loadingBg = config.loadingBg;

				if(!loadingImg.length){
					loadingImg = $("<img />").attr("src", config.contextPath + "/resources/common/images/indicator.gif").addClass("loading-img");
					body.prepend(loadingImg);
					config.loadingImg = loadingImg;
				}
				if(!loadingBg.length){
					loadingBg = $("<div></div>").addClass("loading-bg");
					body.prepend(loadingBg);
					config.loadingBg = loadingBg;
				}

				loadingImg.show();
	    	    loadingBg.show();
			},
			hideLoading: function(){
				var loadingImg = config.loadingImg;
				var loadingBg = config.loadingBg;
				loadingImg.length && loadingImg.hide();
	    	    loadingBg.length && loadingBg.hide();
			}
		}
	});

})(jQuery);
