/**
 * 开发很忙 借鉴https://www.cnblogs.com/moqiutao/p/6279905.html
 */
;(function(win){
	
	//用于压缩图片的canvas
	var canvas = win.document.createElement("canvas");
	var ctx = canvas.getContext('2d');
	
	//瓦片canvas
	var tCanvas = win.document.createElement("canvas");
	var tctx = tCanvas.getContext("2d");
	var maxsize = 500 * 1024;
	
	// 将方法通过 imgCompress 暴露出去
	var imgCompress = win.imgCompress = imgCompress || {};
	
	/**
	 * 压缩图片入口
	 */
	win.imgCompress.compress = function(file, callBack){
		if (!file)
			return;
		
		if (!/\/(?:jpeg|png|gif)/i.test(file.type))
			return;

		var reader = new FileReader();

		reader.onload = function() {
			var result = this.result;
			var img = new Image();
			img.src = result;

			// 如果图片大小小于500kb，则直接上传
			if (result.length <= maxsize) {
				
				callBack(file);
				
				img = null;
				return;
			}

			// 图片加载完毕之后进行压缩，然后上传
			if (img.complete) {
				defaultCallback();
			} else {
				img.onload = defaultCallback;
			}

			function defaultCallback() {
				var base64Str = _method.compressImg(img);
				compressImg = _method.base64ImgToFile(base64Str, file.type, file.name);
				
				callBack(compressImg);
				
				img = null;
			}

		};

		reader.readAsDataURL(file);
	}
	
	/**
	 * 批量压缩图片入口
	 */
	win.imgCompress.batchCompress = function(files, callBack){
		if (!files && files.length)
			return;
		
		var compressImgs = [];
		
		(function iterator(i){
		    if(i == files.length){
		    	callBack(compressImgs);
		        return;
		    }
		    
		    var file = files[i];
		    
		    //异步压缩图片
		    win.imgCompress.compress(file, function(compressImg){
		    	compressImgs.push(compressImg);
				iterator(i+1);
			});
		})(0);
	}
		
	// ---------------- 以下是私有方法, 内部调用 -------------
	
	var _method = {
			
		// 压缩图片
		compressImg: function(img){
			var initSize = img.src.length;
			var width = img.width;
			var height = img.height;

			// 如果图片大于四百万像素，计算压缩比并将大小压至400万以下
			var ratio;
			if ((ratio = width * height / 4000000) > 1) {
				ratio = Math.sqrt(ratio);
				width /= ratio;
				height /= ratio;
			} else {
				ratio = 1;
			}

			canvas.width = width;
			canvas.height = height;

			// 铺底色
			ctx.fillStyle = "#fff";
			ctx.fillRect(0, 0, canvas.width, canvas.height);

			// 如果图片像素大于100万则使用瓦片绘制
			var count;
			if ((count = width * height / 1000000) > 1) {
				count = ~~(Math.sqrt(count) + 1); // 计算要分成多少块瓦片

				// 计算每块瓦片的宽和高
				var nw = ~~(width / count);
				var nh = ~~(height / count);

				tCanvas.width = nw;
				tCanvas.height = nh;

				for (var i = 0; i < count; i++) {
					for (var j = 0; j < count; j++) {
						tctx.drawImage(img, i * nw * ratio, j * nh * ratio, nw * ratio,
								nh * ratio, 0, 0, nw, nh);

						ctx.drawImage(tCanvas, i * nw, j * nh, nw, nh);
					}
				}
			} else {
				ctx.drawImage(img, 0, 0, width, height);
			}

			// 进行最小压缩
			var ndata = canvas.toDataURL('image/jpeg', 0.5);

/*			console.log('压缩前：' + initSize);
			console.log('压缩后：' + ndata.length);
			console.log('压缩率：' + ~~(100 * (initSize - ndata.length) / initSize) + "%");*/

			tCanvas.width = tCanvas.height = canvas.width = canvas.height = 0;

			return ndata;
		},
		
		// 将base64的图片转成二进制对象
		base64ImgToFile: function(basestr, type, fileName){
			var text = win.atob(basestr.split(",")[1]);
			var buffer = new ArrayBuffer(text.length);
			var ubuffer = new Uint8Array(buffer);
			var pecent = 0, loop = null;

			for (var i = 0; i < text.length; i++) {
				ubuffer[i] = text.charCodeAt(i);
			}

			var Builder = win.WebKitBlobBuilder || win.MozBlobBuilder;
			var blob;

			if (Builder) {
				var builder = new Builder();
				builder.append(buffer);
				blob = builder.getBlob(type);
			} else {
				blob = new win.Blob([buffer], {
					type : type
				});
			}

			return new File([blob], fileName);
		}
	}
	
})(window);
