;(function($){
	$.validator.setDefaults({});
	//输入值不能小于或等于param
	$.validator.addMethod("myMin", function(value, element, param) {
		var min = parseFloat(param);
		value = parseFloat(value);
		return this.optional(element) || value > min;   
	}, $.validator.format("请输入大于 {0} 的数字!"));
	
	//输入值不能小于或大于param
	$.validator.addMethod("myMax", function(value, element, param) {
		var max = parseFloat(param);
		value = parseFloat(value);
		return this.optional(element) || value < max;   
	}, $.validator.format("请输入小于 {0} 的数字!"));
	
	//输入值不能小于或大于param
	$.validator.addMethod("checkMoney", function(value, element) {
		var isMoney=/^(([1-9][0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/;
		value = parseFloat(value);
		return this.optional(element) || isMoney.test(value);   
	}, $.validator.format("请输入正确的金额!"));
	
	
	//输入值必须要在param[0],param[1]之间,param[2]指定匹配模式
	$.validator.addMethod("myRange", function(value, element, param) {
		if(this.optional(element)){
			return true;
		}
		var val = parseFloat(value);
		var min = param[0];
		var max = param[1];
		var model = param[2] || "[]";  
		var flag = false;
		
		if(model === "()"){
			flag = (val > min && val < max);
		}else if(model === "(]"){
			flag = (val > min && val <= max);
		}else if(model === "[)"){
			flag = (val >= min && val < max);
		}else if(model === "[]"){
			flag = (val >= min && val <= max);
		}
		return flag;   
	}, $.validator.format("请输入大于 {0} 的数字!"));
	
	//校验手机号码
	$.validator.addMethod("phone", function(value, element, param) {
	  return this.optional(element) || (/^[1][0-9]{10}$/ .test(value));
	}, $.validator.format("请输入正确的手机号码!"));
	  
   //校验固定电话
   $.validator.addMethod("tel", function(value, element, param) {
	  return this.optional(element) || (/\d{3}-\d{8}|\d{4}-\d{7}/.test(value));   
   }, $.validator.format("请输入正确的固定电话!"));
   
   //校验汉字
   $.validator.addMethod("chinese", function(value, element, param) {
	   return this.optional(element) || (/[\u4E00-\u9FA5\uF900-\uFA2D]/.test(value));   
   }, $.validator.format("请输入正确汉字!"));
   
   //校验字符串只能由汉字,字母,数字,下划线组成
   $.validator.addMethod("checkName",function(value,element){
		var regex = new RegExp("^([a-zA-Z0-9_]|[\\u4E00-\\u9FFF])+$", "g");
		return this.optional(element) || regex.test(value);
	},'名字只能由汉字、数字、字母、下划线组成!');
   
   //校验字符串只能由字母,数字,下划线组成, 与上面的区别就是不包含汉字
   $.validator.addMethod("accont",function(value,element){
	   var regex = new RegExp("^([a-zA-Z0-9_])+$", "g");
	   return this.optional(element) || regex.test(value);
   },'名字只能由数字、字母、下划线组成!');
   
   //校验密码
   $.validator.addMethod("passwd", function(value, element) {
	   return this.optional(element) || /^[a-zA-Z]\w{5,17}$/.test(value);   
   }, $.validator.format("密码必须以字母开头，长度在6-18之间，只能包含字符、数字和下划线!"));
   
   //校验特殊字符
   $.validator.addMethod("special", function(value, element) {
	   return this.optional(element) || !(/[~#^$@%&!*]/gi.test(value));   
   }, $.validator.format("对不起,不能包含特殊字符!"));

   //简单校验身份证号码
	$.validator.addMethod("idcard", function(value, element) {
		var reg = /(^\d{15}$)|(^\d{17}([0-9]|X)$)/;
		return this.optional(element) || reg.test(value);
	}, '请输入正确的身份证号码!'); 

   //校验文件必填
   $.validator.addMethod("requiredFile", function(value, element) {
	   var oldFile = $(element).attr("oldFile").trim();
	   return value || oldFile;
   }, "请选择要上传的文件!");
   
   //校验文件必填
   $.validator.addMethod("numberGt", function(value, element, param) {
	   var compareElement = $(param);
	   if(compareElement.length){
		   var compareElementVal = parseFloat(compareElement.val());
		   value = parseFloat(value);
		   
		   return this.optional(element) || value > compareElementVal;
	   }
   }, "不符合条件!");
   
   //校验文件必填
   $.validator.addMethod("numberLt", function(value, element, param) {
	   var compareElement = $(param);
	   if(compareElement.length){
		   var compareElementVal = parseFloat(compareElement.val());
		   value = parseFloat(value);
		   
		   return this.optional(element) || value < compareElementVal;
	   }
   }, "不符合条件!");
   
   //校验文件必填
   $.validator.addMethod("numberGe", function(value, element, param) {
	   var compareElement = $(param);
	   if(compareElement.length){
		   var compareElementVal = parseFloat(compareElement.val());
		   value = parseFloat(value);
		   
		   return this.optional(element) || value >= compareElementVal;
	   }
   }, "不符合条件!");
   
   //校验文件必填
   $.validator.addMethod("numberLe", function(value, element, param) {
	   var compareElement = $(param);
	   if(compareElement.length){
		   var compareElementVal = parseFloat(compareElement.val());
		   value = parseFloat(value);
		   
		   return this.optional(element) || value <= compareElementVal;
	   }
   }, "不符合条件!");
   
   $.validator.addMethod("isEMail", function(value, element) {
	    var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/;
	    return reg.test(value);}, '请输入正确的邮箱格式'); 
   
})(jQuery);
