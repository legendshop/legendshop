/**
* 描述: 一款图片本地预览插件;
* 目前支持IE9+, 以及支持H5的现代FireFox,Chrome的浏览器,safari暂未测试
* 作者: 开发很忙
* 日期: 2017-02-07
* 
*/
;(function($){
	$.fn.extend({
		imgPreview: function(options){

			//默认参数
			var defaults = {
				img: "img-preview", //图片显示的img标签的class
				defaultImg: "",//默认显示图片
				width: 150,//预览图片的宽度
				height: 150,//预览图片的高度
				minSize: 1024,//限制最小文件
				maxSize: 1024*1024,//限制最大文件
				imgTypes: ["jpg", "png", "gif", "bpm"],//限制类型
				error: function(errorType, result){
					if(errorType === "size"){
						
						if (options.minSize >=1024 || options.maxSize >= 1024 ) {
							var msgMinSize = options.minSize/1024+"M";
							var msgMaxSize = options.maxSize/1024/1024+"M";
						}
						/*alert("文件大小要在" + msgMinSize + " - " + msgMaxSize + "之间!");*/
						alert("图片大小不能超过"+msgMaxSize);
						return;
					}

					if(errorType === "type"){
						alert("请选择" + options.imgTypes.join(",") + "类型的图片!");
						return;
					}
				}
			};

			//初始化设置参数
			options = $.extend(defaults, options || {});
			
			//初始化图片预览区域
			var imgs = $("." + options.img);
			$.each(imgs, function(index, item){
				item.width = options.width;
				item.height = options.height;
				if(!item.src && options.defaultImg){
					item.src = options.defaultImg;
				}
			});

			return $.each(this, function(index, item){
				var $item = $(item);
				var img = $(imgs[index]);

				//设置文件选择过滤 TODO accept可能会造成性能问题
				$item.attr("accept", "." + options.imgTypes.join(",."));
				
				//当文件输入框的值被改变时
				$item.on("change", function(event){
					var fileInput = this;

					//支持H5的浏览器,如fireFox,chrome
					if(fileInput.files){
						var file = fileInput.files[0];

						//如果没有文件
						if(!file){
							img.attr("src", options.defaultImg);
							return false;
						}

						//校验文件大小
						if(file.size <= options.minSize || file.size >= options.maxSize){
							fileInput.value = "";
							img.attr("src", options.defaultImg);
							options.error("size",{
								"fileSize": file.size,
								"minSize": options.minSize,
								"maxSize": options.maxSize
							});
							return; 
						}

						//校验文件类型
						var suffix = file.name.substring(file.name.lastIndexOf(".") + 1);
						if($.inArray(suffix.toLowerCase(), options.imgTypes) === -1){
							fileInput.value = "";
							img.attr("src", options.defaultImg);
							options.error && options.error("type",{
								"imgType": suffix,
								"imgTypes": options.imgTypes
							});
							return;
						}
						
						window.URL = window.URL || window.webkitURL;
						var imgURL = window.URL.createObjectURL(file);
						
						//如果支持对象URL方式
						if(img.src = imgURL){ 
							//显示用户选择的图片
							img.attr("src", imgURL);
							img.on("load", function(){
								//成功回调
								options.success && options.success(item, file, img);

								//释放图片引用
								window.URL.revokeObjectURL(imgURL);
							});
							
						}else{//否则使用DataURL方式
							
							//FileReader,将读取图片以base64的编码数据  
							var reader = new FileReader();  
							//DATA URL 协议读取base64编码数据    
							reader.readAsDataURL(file);  
							
							reader.onload = function(){  
                                // 将读取的base64数据赋予img的src
                                img.attr("src", this.result);  
								
								//成功回调
								options.success && options.success(item, file, img);

								img.on("load", function(){
									reader = null;
								});
							}  
						}

						file = null;
						
					}else{//IE9浏览器
						
						var filePath = fileInput.value;

						//判断是否有文件
						if(!filePath){
							img.attr("src", options.defaultImg);
							return;
						}

						//获取文件类型
						var fileType = filePath.substring(filePath.lastIndexOf(".") + 1);

						//检验文件类型
						if($.inArray(fileType.toLowerCase(), options.imgTypes) === -1){
							//重置文件输入框
							fileInput.outerHTML += "";
							img.attr("src", options.defaultImg);

							options.error && options.error("type",{
								"imgType": fileType,
								"imgTypes": options.imgTypes
							});
							return;
						}

						//获取文件大小
						var fileObject = new ActiveXObject("Scripting.FileSystemObject");
						var file = fileObject.GetFile(fileInput.value);
						var fileSize = file.size;

						//校验文件大小
						if(fileSize <= options.minSize || fileSize >= options.maxSize){
							//重置文件输入框
							fileInput.outerHTML += "";
							img.attr("src", options.defaultImg);

							options.error && options.error("size",{
								"fileSize": fileSize,
								"minSize": options.minSize,
								"maxSize": options.maxSize
							});
						}else{
							img.attr("src", filePath);

							//成功回调
							options.success && options.success(item, file, img);
						}

						fileObject = null;
						file = null;
					}

					return false;
				});

				//监听图片双击事件
				img.on("dblclick", function(event){
					$item.val("");
					this.src = "";

					return false;
				});
				
			});
		}
	});
	
})(jQuery);
