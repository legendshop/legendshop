/**
 * each是一个集合迭代函数
 */
Array.prototype.each = function(fn) {
	fn = fn || Function.K;
	var a = [];
	var args = Array.prototype.slice.call(arguments, 1);
	for ( var i = 0; i < this.length; i++) {
		var res = fn.apply(this, [ this[i], i ].concat(args));
		if (res != null)
			a.push(res);
	}
	return a;
};

/**
 * 得到一个数组不重复的元素集合<br/> 唯一化一个数组
 * 
 * @returns {Array} 由不重复元素构成的数组
 */
Array.prototype.uniquelize = function() {
	/*
	var ra = new Array();
	for ( var i = 0; i < this.length; i++) {
		if (!ra.contains(this[i])) {
			ra.push(this[i]);
		}
	}
	return ra;
	*/
	// 遍历arr，把元素分别放入tmp数组(不存在才放)
   /* var tmp = new Array();
    for ( var i = 0; i < this.length; i++) {
    	  //该元素在tmp内部不存在才允许追加
		if (tmp.indexOf(this[i])==-1) {
    	//if(($.inArray(this[i],tmp))==-1){
			tmp.push(this[i]);
		}
	}
    return tmp;
    */
	this.sort();
	var re=[this[0]];
	for(var i = 1; i < this.length; i++)
	{
		if( this[i] !== re[re.length-1])
		{
			re.push(this[i]);
		}
	}
	return re;
  
	 /*
    var res = [];
    var json = {};
    for(var i = 0; i < this.length; i++){
        if(!json[this[i]]){
            res.push(this[i]);
            json[this[i]] = 1;
        }
    }
    return res;
    */
};

Array.intersect2 = function(a,b) {
	var arr=a.concat(b);
    var res = [];
    var json = {};
    var res2=[];
    for(var i = 0; i < arr.length; i++){
        if(!json[arr[i]]){
            res.push(arr[i]);
            json[arr[i]] = 1;
        }else{
        	res2.push(arr[i]);
        }
    }
    return res2;
};

/**
 * @param {Array}
 *            a 集合A
 * @param {Array}
 *            b 集合B
 * @returns {Array} 两个集合的补集
 */
Array.complement = function(a, b) {
	return Array.minus(Array.union(a, b), Array.intersect(a, b));
};

/**
 * @param {Array}
 *            a 集合A
 * @param {Array}
 *            b 集合B
 * @returns {Array} 两个集合的交集
 */
Array.intersect = function(a, b) {
	return a.uniquelize().each(function(o) {
		return b.contains(o) ? o : null
	});
};

/**
 * @param {Array}
 *            a 集合A
 * @param {Array}
 *            b 集合B
 * @returns {Array} 两个集合的差集
 */
Array.minus = function(a, b) {
	return a.uniquelize().each(function(o) {
		return b.contains(o) ? null : o
	});
};

/**
 * @param {Array}
 *            a 集合A
 * @param {Array}
 *            b 集合B
 * @returns {Array} 两个集合的并集
 */
Array.union = function(a, b) {
	return a.concat(b).uniquelize();
};

/**
 * 判断String字符是否相等
 * @param item
 * @returns
 */
Array.prototype.contains = function(item) {
	return RegExp(item).test(this);
};


