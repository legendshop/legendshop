;(function (win) {
    var defaultConfig = {
        center: new AMap.LngLat(113.280637, 23.125178),//默认中心点,广州
        citycode: "020",//默认城市编码,广州
    };

    win.onload = function () {

        //初始化地图对象，加载地图显示
        var map = new AMap.Map("mapBox", {
            resizeEnable: true
        });

        //初始化标记对象
        var marker = new AMap.Marker({
            map: map,
            position: defaultConfig.center,
        });

        var geolocation = null;//定位对象
        var placeSearch = null;//地图搜索服务对象
        var geocoder = null;//地理编码对象

        //为地图添加初始化各种插件
        AMap.plugin(['AMap.ToolBar', 'AMap.Scale', 'AMap.Geolocation',
                'AMap.OverView', 'AMap.Geocoder', 'AMap.PlaceSearch', 'AMap.Autocomplete'],
            function () {
                map.addControl(new AMap.ToolBar());
                map.addControl(new AMap.Scale());
                map.addControl(new AMap.OverView());

                //初始化定位对象
                geolocation = new AMap.Geolocation({
                    timeout: 10000,
                    maximumAge: 30 * 60 * 1000,
                    //GeoLocationFirst: true,
                    buttonOffset: new AMap.Pixel(20, 50),
                    zoomToAccuracy: true,
                });
                map.addControl(geolocation);

                if (!lng || !lat) {
                    geolocation.getCurrentPosition(locationHandler);
                }

                //给定位注册事件
                AMap.event.addListener(geolocation, 'complete', function (result) {//定位完成
                    locationSuccess(result.formattedAddress, result.addressComponent, result.position);
                });
                AMap.event.addListener(geolocation, 'error', function (error) {//定位错误
                    locationFail();
                });

                //初始化地图关键字搜索服务插件
                placeSearch = new AMap.PlaceSearch({
                    map: map,
                    city: defaultConfig.citycode,
                    type: "汽车服务|汽车销售|汽车维修|摩托车服务|餐饮服务|购物服务|生活服务|体育休闲服务|医疗保健服务" +
                        "|住宿服务|风景名胜|商务住宅|政府机构及社会团体|科教文化服务|交通设施服务|金融保险服务|公司企业|道路附属设施|地名地址信息|公共设施",
                    pageSize: 5,
                    pageIndex: 1,
                    panel: "searchPanel",
                    renderStyle: "newpc"
                });

                //为关键字搜索服务注册事件
                AMap.event.addListener(placeSearch, 'complete', function (result) {
                    var poiList = result.poiList.pois;
                    if (poiList && poiList.length > 0) {
                        lnglatHandler(poiList[0].location);
                    }
                });
                AMap.event.addListener(placeSearch, 'error', function (status) {

                });
                AMap.event.addListener(placeSearch, 'listElementClick', function (e) {//结果面板中POI对应的列表项被点击时触发
                    lnglatHandler(e.data.location);
                });
                AMap.event.addListener(placeSearch, 'markerClick', function (e) {//POI在地图中对应的Marker被点击时触发
                    lnglatHandler(e.data.location);
                });

                //初始化搜索提示插件
                var autocomplete = new AMap.Autocomplete({
                    city: defaultConfig.citycode,
                    input: "searchInput",
                });

                //给搜索提示插件注册事件
                AMap.event.addListener(autocomplete, "choose", function (e) {
                    placeSearch.search(e.poi.name);
                    lnglatHandler(e.poi.location);
                });
                AMap.event.addListener(autocomplete, "select", function (e) {
                    placeSearch.search(e.poi.name);
                    lnglatHandler(e.poi.location);
                });

                //初始化地理编码插件: 坐标 -> 地址
                geocoder = new AMap.Geocoder();

            });

        //为地图注册click事件获取鼠标点击出的经纬度坐标,从而做出相应的处理
        map.on('click', function (e) {
            lnglatHandler(e.lnglat);
        });

        //监听搜索按钮单击事件
        document.getElementById("searchBtn").addEventListener("click", function (event) {
            var searchContent = document.getElementById("searchInput").value;
            if (searchContent) {
                placeSearch.search(searchContent);
            }
        });

        //监听重新获取位置按钮
        document.getElementById("relocationBtn").addEventListener("click", function (event) {
            geolocation.getCurrentPosition(locationHandler);
        });

        //对获取到的经纬度进行处理
        function lnglatHandler(lnglat) {
            updateMap(lnglat);

            //根据获取到的经纬度坐标,转换为地址信息
            geocoder.getAddress(lnglat, function (status, result) {
                geocodeHandler(lnglat, status, result)
            });
        }

        //地理编码处理
        function geocodeHandler(lnglat, status, result) {
            if (status === 'complete' && result.info === 'OK') {//成功
                locationSuccess(result.regeocode.formattedAddress, result.regeocode.addressComponent, lnglat);
            } else {//失败
                locationFail();
            }
        }

        //定位处理
        function locationHandler(status, result) {
            if (status === 'complete' && result.info === 'SUCCESS' && result.position !== undefined && result.formattedAddress === undefined) {

                lnglatHandler(result.position);
            } else if (status === 'complete' && result.info === 'SUCCESS') {//成功
                updateMap(result.position);
                locationSuccess(result.formattedAddress, result.addressComponent, result.position);
            } else {//失败
                locationFail();
            }
        }

        //定位成功处理
        function locationSuccess(formattedAddress, addressComponent, lnglat) {

            initShowLocation();

            //对直辖市的特殊处理
            var city = addressComponent.city;
            var province = addressComponent.province;
            var area = addressComponent.district;
            if (!city) {
                city = addressComponent.province;
            }

            //截取formattedAddress（详细地址（包括省市区）），只取区后面的详细地址
            var index = formattedAddress.indexOf("区");
            var address = formattedAddress.substring(index + 1, formattedAddress.length);

            var location = {
                lng: lnglat.lng,
                lat: lnglat.lat,
                province: province,
                city: city,
                area: area,
                address: address,
                shopAddr: formattedAddress,
            };


            defaultConfig.locationSpan.innerHTML = location.shopAddr;
            marker.setTitle(location.shopAddr);

            //设置到表单域
            setLocationToForm(location);
        }

        //定位失败处理
        function locationFail() {
            updateMap(defaultConfig.center);

            initShowLocation();

            defaultConfig.locationSpan.innerHTML = "对不起,位置获取失败,请重试!";
        }

        //更新地图
        function updateMap(lnglat) {
            map.setCenter(lnglat);
            marker.setPosition(lnglat);
            map.setFitView();
        }

        //初始化显示定位信息的DOM对象
        function initShowLocation() {
            if (!defaultConfig.locationSpan) {
                defaultConfig.locationSpan = document.getElementById("shoplocation");
            }
        }

        //设置定位到表单
        function setLocationToForm(location) {
            if (!defaultConfig.lngInput) {
                defaultConfig.lngInput = document.getElementById("lng");
            }
            if (!defaultConfig.latInput) {
                defaultConfig.latInput = document.getElementById("lat");
            }
            if (!defaultConfig.provinceInput) {
                defaultConfig.provinceInput = document.getElementById("province");
            }
            if (!defaultConfig.cityInput) {
                defaultConfig.cityInput = document.getElementById("city");
            }
            if (!defaultConfig.areaInput) {
                defaultConfig.areaInput = document.getElementById("area");
            }
            if (!defaultConfig.addressInput) {
                defaultConfig.addressInput = document.getElementById("address");
            }
            if (!defaultConfig.shopAddrInput) {
                defaultConfig.shopAddrInput = document.getElementById("shopAddr");
            }

            defaultConfig.lngInput.value = location.lng;
            defaultConfig.latInput.value = location.lat;
            defaultConfig.provinceInput.value = location.province;
            defaultConfig.cityInput.value = location.city;
            defaultConfig.areaInput.value = location.area;
            defaultConfig.addressInput.value = location.address;
            defaultConfig.shopAddrInput.value = location.shopAddr;
        }
    }
})(window);	
	