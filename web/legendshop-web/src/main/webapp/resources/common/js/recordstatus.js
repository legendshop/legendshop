function initStatus(itemId, itemName, status, url, target,path){
  	var desc;
  	var toStatus;
  	   if(status == 1){
  	   		toStatus  = 0;
  	   		desc = itemName +' 下线?';
  	   }else{
  	       toStatus = 1;
  	       desc = itemName + ' 上线?';
  	   }

  	   layer.confirm(desc, {icon:3, title:'提示'}, function(index){
  		   jQuery.ajax({
				url: url + itemId + "/" + toStatus,
				type:'get',
				async : false, //默认为true 异步
				dataType : 'json',
				success:function(data){
					$target = $(target);
					if(data == 1){
						//$target.attr("src",path + "/resources/common/images/blue_down.png");
						$target.html("下线");
					}else if(data == 0){
						//$target.attr("src",path + "/resources/common/images/yellow_up.png");
						$target.html("上线");
					}
					$target.attr("status",data);
					window.location.reload();
				}
		 });
  		 return true;
  		 layer.close(index);
  	   });

}
