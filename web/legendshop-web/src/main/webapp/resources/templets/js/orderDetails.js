$(document).ready(function() {
     $("#time_opened").delegate("dl","click",function(){
		  $("#time_opened dl").removeClass("on");
		  $(this).addClass("on");
    });

     $(".delivery").change(function(){
    	   var delivery=$(this).find("option:selected").val();
    	   if(isEmpty(delivery)){
    		  	layer.msg("请选择配送方式",{icon: 0});
	            return;
    	   }

    	   calculateTotal();
	});

   //初始化优惠券
     getCouponsPage("");
     //默认选择金额最大的优惠券
     selectBestCoupon();

     //优惠券的点击事件
     $("#activeCouList .c-detail").live("click",function (){
    	 $(this).toggleClass("on");
    	 var userCouponIdsStr = "";
    	 var selCouponList = $("#activeCouList .c-detail.on").get();
    	 for(var i=0;i<selCouponList.length;i++){
    		 var ucid = $(selCouponList[i]).attr("ucid");
    		 if(i>0){
    			 userCouponIdsStr = userCouponIdsStr + ",";
    		 }
    		 userCouponIdsStr = userCouponIdsStr + ucid;
    	 }
    	 getCouponsPage(userCouponIdsStr);
     });

     $(".choose-coupon-tit i").live("click",function(){
    	 $(this).toggleClass("down");
    	 if($(this).hasClass("down")){
    		 $("#sel-cou-div").hide();
    	 }else{
    		 $("#sel-cou-div").show();
    	 }
     });

});

//根据选中的 优惠券 计算，并返回优惠券列表
function getCouponsPage(userCouponIdsStr){
	$.ajax({
		url : contextPath + "/p/coupon/orderCouponsPage",
		type:'post',
		async : false,
		data : {"userCouponIdsStr":userCouponIdsStr,"cartTypeEnum":type,'token':token},
		dataType : "html",
		success : function(result){
			$("#orderCouponList").html(result);
			getFreightPage(userCouponIdsStr)
		}
	});
	calculateTotal();
}

//根据选中的 优惠券 计算，返回运费模版
function getFreightPage(userCouponIdsStr){
	$.ajax({
		url : contextPath + "/p/coupon/orderFreightPage",
		type:'post',
		async : false,
		data : {"userCouponIdsStr":userCouponIdsStr,"cartTypeEnum":type,'token':token},
		dataType : "html",
		error: function(jqXHR, textStatus, errorThrown) {
			layer.msg("网络异常,请稍后重试！",{icon:2});
		},
		success : function(result){
			$("#shopping").html(result);
		}
	});
}


function calculateTotal(){

	   //获取实际商品价格
	 var allActualCash=$.trim($("#allActualCash").html()).substring(1);
	   if(allActualCash <=0 || isEmpty(allActualCash)){ //实际价格不能为 0
		   layer.msg("亲爱的用户,您的价格有误,请重新下单！", {icon: 0});
        return;
	 }

	   //获取总的优惠券
    var allOffPrice = 0;
    var coupons =  $("#activeCouList .c-detail.on").get();
    for(var i=0;i<coupons.length;i++){
    	var price = $(coupons[i]).attr("price");
    	allOffPrice+=Number(price);
    }
    $("#couponOffPrice").html(allOffPrice);
    $("#couponOffPrice2").html(allOffPrice);
    $("#couponOffPrice3").html(allOffPrice);
    $("#couponCount").html(coupons.length);

    //获取总的促销优惠
    var allDiscount=$.trim($("#allDiscount").html()).substring(2);
    if(isEmpty(allDiscount)){
 	   allDiscount=0;
    }else if(allDiscount<0){
    	 allDiscount=0.00;
    }


    //  //获取所有运费
    // var allfreight = 0; //所有运费价格
    // //获取所有运费
    // var posts = $(".delivery").get();
    // for(var i=0;i<posts.length;i++){
    // 	// var f=$(posts[i]).find("option:selected").val();
    //     var f= freightAmount;
    // 	if(isEmpty(f)){
    // 		f=0;
    // 	}
    // 	allfreight+=Number(f);
    // }

	//TODO 确认修改后的运费是否正常
    //获取所有运费
    var allfreight = 0; //所有运费价格
    var posts = $(".delivery").get(0);

    //var f=$(posts).find("option:selected").val();
    var f= freightAmount;
    //当选择的运费模板为空时或默认请选择时
    if(isEmpty(f) || f == -1){
        f=0;
    }
    allfreight+=Number(f);

   /* if(Number(allfreight) != 0){
		$("#deliveryMode").html("配送方式：快递["+allfreight+"元]");
	}else {
		$("#deliveryMode").html("配送方式：商家承担运费");
	}*/

     var orderAllCash=(Number(allActualCash)+Number(allfreight));
     orderAllCash=orderAllCash-Number(allDiscount)-Number(allOffPrice);

     if(orderAllCash<0){
    	 orderAllCash=0.00;
     }
	  $("#allActualCash").html("￥"+  parseFloat(allActualCash).toFixed(2));
	  $("#allFreightAmount").html("￥"+  parseFloat(allfreight).toFixed(2));
      $("#allCash").html("￥"+  parseFloat(orderAllCash).toFixed(2));
      $("#allCashFormat").html(parseFloat(orderAllCash).toFixed(2));
}

/**
 * 提交订单
 */
function submitOrder(){
	var receiver= $("#deliveryAddress ul dl").length;
	if(receiver==0 || isEmpty(receiver)){
		layer.msg("请填写收货地址", {icon: 0});
		$("html,body").animate({scrollTop: $("#path_list").offset().top-50}, 500);
		return;
	}

	var addrId=$("#deliveryAddress ul ").children(".on").attr("addrid");
	if(isEmpty(addrId)){
		layer.msg("请选择收货地址",{icon: 0});
		$("html,body").animate({scrollTop: $("#path_list").offset().top-50}, 500);
		return;
	}

	var pay_type =$("input:radio[name='pay_opened_pm_radio']:checked").val();
	if(isEmpty(pay_type)){
		layer.msg("请选择支付方式",{icon: 0});
		$("html,body").animate({scrollTop: $("#pay").offset().top-50}, 500);
		return;
	}

	//检查是否无货
	if($(".stocksErr").length>0){
		layer.msg("部分商品缺货或地区限售", {icon: 0});
		return;
	}

	var delivery="";
    var config=true;
    $(".delivery").each(function(){
    	  var deliv=$(this).find("option:selected").val();
          if(Number(deliv) < 0 || isEmpty(deliv)){
        	    layer.msg("请选择您的配送方式", {icon: 0});
            	$("html,body").animate({scrollTop: $(this).parent().parent().parent().parent().offset().top-50}, 300);
            	config=false;
            	return false;//实现break功能
          }else{
        	  var shopId=Number($(this).attr("shopid"));
        	  var transtype=$(this).find("option:selected").attr("transtype");
        	  delivery+=shopId+":"+transtype+";";
          }
     });
     if(!config){
    	return;
     }
     if(!isEmpty(delivery)){
 	 	delivery=delivery.substring(0, delivery.length-1);
 	 }
	  // 获取订单备注
	 var remark =  $("input[name=remark]").get();
	 config=true;
	 var remarkText="";
	 for(var j in remark){
		 var obj=remark[j];
		 var val=$.trim($(obj).val());
		 if (!isEmpty(val)) {
			 if(!is_forbid(val)){
				 layer.msg('订单备注中含有非法字符', {icon: 0});
				 config=false;
			     return ;
			 }
			 var shopId=Number($(obj).attr("shopId"));
			 remarkText+=shopId+":"+val+";";
		 }
	 }
	 if(!config){
	   return;
	 }
	 if(!isEmpty(remarkText)){
		 remarkText=remarkText.substring(0, remarkText.length-1);
	 }
	 /* 旧的发票逻辑 */
	/* var invoiceId = null;
	 if(!isEmpty($(".invoice .invoice-no-spr").children("p").attr("invoice"))){
		 invoiceId=$(".invoice .invoice-no-spr").children("p").attr("invoice");
	 }*/
	 var invoiceId = $("#invoiceIdStr").val();
	 if(isEmpty(invoiceId)){
		 invoiceId=0;
	 }
	 if(data.allCount<1){
		layer.msg("没有要结算的购物清单,请购物", {icon: 0});
	    return ;
	 }

	$("#remarkText").val(remarkText);
	$("#delivery").val(delivery);
	//$("#couponStr").val(couponStr);
	$("#invoiceIdStr").val(invoiceId);
	$("#payManner").val(pay_type);
	$("#submitOrder").css('background-color','#e4e4e4');
	$("#submitOrder").attr("disabled","disabled");
	$("body").showLoading();

	//调用方法 如
	$("#orderForm").ajaxForm().ajaxSubmit({
		 success:function(data) {
			 	if(!data){
			 		layer.msg("订单超时，请重新下单！", {icon: 0,time:1000},function(){
			 			window.location.href=contextPath+"/shopCart/shopBuy";
			 		});
	           	    return false;
			 	}
			 	var result = null;
			 	try{
			 		result = $.parseJSON(data); //在springboot环境下会出错, 在5.5版本又可以了
			 		//result = data;
			 	}catch(e){
			 		layer.msg("订单超时，请重新下单！", {icon: 0},function(){
			 			window.location.href=contextPath+"/shopCart/shopBuy";
			 		});
	           	    return false;
			 	}
			 	if(result.result == true){
			 		var url = result.url;
			    /*var result=$.parseJSON(data);
				if(result.result==true){
					 var url=result.url;*/
					 window.location.href=contextPath+url;
					 return;
				}else if(result.result==false){
					$("body").hideLoading();
					$("#submitOrder").css('background-color',"#e5004f");
					$("#submitOrder").removeAttr("disabled","disabled");
					var errorCode=result.code;
					if(errorCode=="NOT_LOGIN"){
						layer.msg("请登录您的帐号！", {icon: 0},function(){
							window.location.href=contextPath+"/login";
						});
		                return false;
		            }else if(errorCode=="NOT_PRODUCTS"){
		            	 setTimeout(function(){
		                      window.location.href=contextPath+"/shopCart/shopBuy";
		                 },1);
		            	 return false;
		            }else if(errorCode=="NO_ADDRESS"){
		            	layer.msg("请填写您的收货人信息 ！", {icon: 0});
		            	$("html,body").animate({scrollTop: $("#user_addr").offset().top-50}, 500);
		        		return false;
		            }else if(errorCode=="NULL_TOKEN"){
		            	layer.msg("购物信息已失效", {icon: 0},function(){
		            		window.location.href=contextPath+"/p/myorder";
		            	});
		           	    return false;
		            }else if(errorCode=="INVALID_TOKEN"){
		            	layer.msg("请勿重复提交订单！", {icon: 0});
		           	    return false;
		            }
		            else if(errorCode=="PARAM_ERR"){
		            	layer.msg("提交订单参数有误！", {icon: 0},function(){
		            		window.location.href=contextPath+"/shopCart/shopBuy";
		            	});
		           	    return false;
		            }else{
		            	layer.msg(result.message, {icon: 2});
		            	return false;
		            }
				}
		 },
		 error:function(XMLHttpRequest, textStatus,errorThrown) {
			 $("body").hideLoading();
			 layer.msg("订单出现异常", {icon: 2});
			 return false;
		 }

	});

}
/**
 * 刷新订单
 */
function reloadOrder(){
	$("#buyNow").val(isBuyNow);
	$("#prodId").val(prodId);
	$("#skuId").val(skuId);
	$("#count").val(count);

	$("#orderFormReload").submit();
}

/**
 * 判断是否是空
 * @param value
 */
function isEmpty(value){
	if(value == null || value == "" || value == "undefined" || value == undefined || value == "null"){
		return true;
	}
	else{
		value = (value+"").replace(/\s/g,'');
		if(value == ""){
			return true;
		}
		return false;
	}
}
/**
 * 检查是否含有非法字符
 * @param temp_str
 * @returns {Boolean}
 */
function is_forbid(temp_str){
    temp_str = temp_str.replace(/(^\s*)|(\s*$)/g, "");
	temp_str = temp_str.replace('--',"@");
	temp_str = temp_str.replace('/',"@");
	temp_str = temp_str.replace('+',"@");
	temp_str = temp_str.replace('\'',"@");
	temp_str = temp_str.replace('\\',"@");
	temp_str = temp_str.replace('$',"@");
	temp_str = temp_str.replace('^',"@");
	temp_str = temp_str.replace('.',"@");
	temp_str = temp_str.replace(';',"@");
	temp_str = temp_str.replace('<',"@");
	temp_str = temp_str.replace('>',"@");
	temp_str = temp_str.replace('"',"@");
	temp_str = temp_str.replace('=',"@");
	temp_str = temp_str.replace('{',"@");
	temp_str = temp_str.replace('}',"@");
	var forbid_str = new String('@,%,~,&');
	var forbid_array = new Array();
	forbid_array = forbid_str.split(',');
	for(i=0;i<forbid_array.length;i++){
		if(temp_str.search(new RegExp(forbid_array[i])) != -1)
		return false;
	}
	return true;
}

//默认选中金额最大的优惠券
function selectBestCoupon() {
	//拿到所有可用的卷（红包、优惠券）
	if(isEmpty(usableUserCoupons)) {
		return;
	}
	var usableCoupons = JSON.parse(usableUserCoupons);
	var platformRedId = 0;
	var shopCouponId = 0;
	var platformRedCash = 0;
	var shopCouponCash = 0;
	var platformRedFull = 0;
	var shopCouponFull = 0;
	var prodCouponId = 0; //商品券id
	var prodCouponCash = 0;//商品券金额
	var prodCouponFull = 0;//满减金额
	for(var i = 0; i < usableCoupons.length; i++ ) {
		userCoupons = usableCoupons[i];
		if(userCoupons.couponProvider == "platform") {//红包
			if(userCoupons.offPrice > platformRedCash) {
				platformRedId = userCoupons.userCouponId;
				platformRedCash = userCoupons.offPrice;//红包金额
				platformRedFull = userCoupons.fullPrice;//满减金额
			}
		}else if(userCoupons.couponProvider == "shop" && userCoupons.couponType == "common") {//店铺优惠券
			if(userCoupons.offPrice > shopCouponCash) {
				shopCouponId = userCoupons.userCouponId;
				shopCouponCash = userCoupons.offPrice;
				shopCouponFull = userCoupons.fullPrice;
			}
		}else{//商品券
			if(userCoupons.offPrice > prodCouponCash) {
				prodCouponId = userCoupons.userCouponId;
				prodCouponCash = userCoupons.offPrice;
				prodCouponFull = userCoupons.fullPrice;
			}
		}
	}

	var cash=Number($.trim($("#allActualCash").html()).substring(1)) - Number($.trim($("#allDiscount").html()).substring(2));//促销价=商品总价-促销优惠

	var userCouponIdsStr = null

	//1.如果只有一个红包
	if(platformRedId != 0 && shopCouponId == 0  && prodCouponId == 0 ){
		userCouponIdsStr = platformRedId;
	}
	//2.如果只有一个店铺券
	if(shopCouponId != 0 && platformRedId == 0 && prodCouponId == 0 ){
		userCouponIdsStr = shopCouponId;
	}
	//3.如果只有一个商品券
	if(prodCouponId != 0 && platformRedId == 0 && shopCouponId == 0 ){
		userCouponIdsStr = prodCouponId;
	}
	//4.如果有一个红包和一个店铺券
	if(platformRedId != 0 && shopCouponId != 0 && prodCouponId == 0 ){
		var firstOff = 0;//使用一个优惠券/红包之后的价格
		if(Number(platformRedCash)-Number(shopCouponCash)>=0){ //红包大于或等于店铺券
			firstOff = cash-Number(platformRedCash);//优惠价=促销价-红包金额
			//如果店铺券满减金额大于firstOff 不满足店铺券的使用
			if(shopCouponFull>firstOff){
				userCouponIdsStr = platformRedId;
			}else{
				userCouponIdsStr = platformRedId + "," + shopCouponId;
			}
		}else{//店铺券大于等于红包
			firstOff = cash-Number(shopCouponCash);//优惠价=促销价-店铺券
			//如果红包券满减金额大于firstOff 不满足红包的使用
			if(platformRedFull>firstOff){
				userCouponIdsStr = shopCouponId;
			}else{
				userCouponIdsStr = shopCouponId + "," + platformRedId;
			}
		}
	}
	//5.如果有一个红包和一个商品券
	if(platformRedId != 0 && prodCouponId != 0 && shopCouponId == 0 ){
		var firstOff = 0;//使用一个优惠券/红包之后的价格
		if(Number(platformRedCash)-Number(prodCouponCash)>=0){ //红包大于或等于商品券
			firstOff = cash-Number(platformRedCash);//优惠价=促销价-红包金额
			//如果商品券满减金额大于firstOff 不满足商品券的使用
			if(prodCouponFull>firstOff){
				userCouponIdsStr = platformRedId;
			}else{
				userCouponIdsStr = platformRedId + "," + prodCouponId;
			}
		}else{//商品券大于等于红包
			firstOff = cash-Number(prodCouponCash);//优惠价=促销价-商品券
			//如果红包券满减金额大于firstOff 不满足红包的使用
			if(platformRedFull>firstOff){
				userCouponIdsStr = prodCouponId;
			}else{
				userCouponIdsStr = prodCouponId + "," + platformRedId;
			}
		}
	}
	//6.如果有一个店铺券和一个商品券
	if(platformRedId == 0 && shopCouponId != 0 && prodCouponId != 0){
		var firstOff = 0;//使用一个优惠券之后的价格
		if(Number(shopCouponCash)-Number(prodCouponCash)>=0){//店铺券金额大于商品券金额
			firstOff = cash-Number(shopCouponCash);//优惠价=促销价-优惠券
			if(prodCouponFull>firstOff){
				userCouponIdsStr = shopCouponId;
			}else{
				userCouponIdsStr = shopCouponId + "," + prodCouponId;
			}
		}else{
			firstOff = cash-Number(prodCouponCash);//优惠价=促销价-优惠券
			if(shopCouponFull>firstOff){
				userCouponIdsStr = prodCouponId;
			}else{
				userCouponIdsStr = prodCouponId + "," + shopCouponId;
			}
		}
	}
	//7.如果有一个红包和一个店铺券和一个商品券
	if(platformRedId != 0 && shopCouponId != 0 && prodCouponId != 0){
		var firstOff = 0;//使用一个优惠之后的价格
		var secondOff = 0 ;//使用两个优惠之后的价格
		if(Number(platformRedCash)-Number(shopCouponCash)>=0 && Number(platformRedCash)-Number(prodCouponCash)>=0){//红包金额最大
			firstOff = cash - Number(platformRedCash);
			if((Number(shopCouponCash)-Number(prodCouponCash))>=0){//店铺券大于商品券
				if(shopCouponFull<=firstOff){//店铺券满足使用金额
					secondOff = firstOff - shopCouponCash;
					if(prodCouponFull<=secondOff){//商品券满足使用条件
						userCouponIdsStr = platformRedId + "," + shopCouponId + "," + prodCouponId;
					}else{
						userCouponIdsStr = platformRedId + "," + shopCouponId;
					}
				}else if(shopCouponFull<=firstOff){//商品券满足使用金额
					userCouponIdsStr = platformRedId + "," + prodCouponId;
				}else{//店铺券和商品都不满足使用金额
					userCouponIdsStr = platformRedId;
				}
			}else{//商品券大于店铺券
				if(prodCouponFull<=firstOff){//商品券满足使用金额
					secondOff = firstOff - prodCouponCash;
					if(shopCouponFull<=secondOff){//店铺券满足使用条件
						userCouponIdsStr = platformRedId + "," + prodCouponId + "," + shopCouponId;
					}else{
						userCouponIdsStr = platformRedId + "," + prodCouponId;
					}
				}else if(shopCouponFull<=firstOff){//店铺券满足使用金额
					userCouponIdsStr = platformRedId + "," + shopCouponId
				}else{//店铺券和商品都不满足使用金额
					userCouponIdsStr = platformRedId;
				}
			}
		}
		if(Number(shopCouponCash)-Number(platformRedCash)>=0 && Number(shopCouponCash)-Number(prodCouponCash)>=0){//店铺券金额最大
			firstOff = cash - Number(shopCouponCash);
			if((Number(platformRedCash)-Number(prodCouponCash))>=0){//红包大于商品券
				if(platformRedFull<=firstOff){//红包满足使用金额
					secondOff = firstOff - platformRedCash;
					if(prodCouponFull<=secondOff){//商品券满足使用条件
						userCouponIdsStr = shopCouponId + "," + platformRedId + "," + prodCouponId;
					}else{
						userCouponIdsStr = shopCouponId + "," + platformRedId;
					}
				}else if(prodCouponFull<=firstOff){//商品券满足使用金额
					userCouponIdsStr = platformRedId + "," + prodCouponId
				}else{
					userCouponIdsStr = shopCouponId;
				}
			}else{//商品券大于红包
				if(prodCouponFull<=firstOff){//商品券满足使用金额
					secondOff = firstOff - prodCouponFull;
					if(platformRedFull<=secondOff){//红包满足使用条件
						userCouponIdsStr = shopCouponId + "," + prodCouponId + "," + platformRedId;
					}else{
						userCouponIdsStr = shopCouponId + "," + prodCouponId;
					}
				}else if(platformRedFull<=firstOff){//红包满足使用金额
					userCouponIdsStr = shopCouponId  + "," + platformRedId;
				}else{
					userCouponIdsStr = shopCouponId;
				}
			}
		}
		if(Number(prodCouponCash)-Number(platformRedCash)>=0 && Number(prodCouponCash)-Number(shopCouponCash)>=0){//商品券金额最大
			firstOff = cash - Number(prodCouponCash);
			if((Number(platformRedCash)-Number(shopCouponCash))>=0){//红包大于店铺券
				if(platformRedFull<=firstOff){//红包满足使用条件
					secondOff = firstOff - platformRedCash;
					if(shopCouponFull<=secondOff){//店铺券满足使用条件
						userCouponIdsStr = prodCouponId + "," + platformRedId + "," + shopCouponId;
					}else{
						userCouponIdsStr = prodCouponId + "," + platformRedId;
					}
				}else if(shopCouponFull<=firstOff){//店铺券满足使用金额
					userCouponIdsStr = prodCouponId  + "," + shopCouponId;
				}else{
					userCouponIdsStr = prodCouponId;
				}
			}else{//店铺券大于红包
				if(shopCouponFull<=firstOff){//店铺券满足使用条件
					secondOff = firstOff - shopCouponCash;
					if(platformRedFull<=secondOff){//红包满足使用条件
						userCouponIdsStr = prodCouponId + "," + shopCouponId + "," + platformRedId;
					}else{
						userCouponIdsStr = prodCouponId + "," + shopCouponId;
					}
				}else if(platformRedFull<=firstOff){//红包满足使用金额
					userCouponIdsStr = prodCouponId  + "," + platformRedId;
				}else{
					userCouponIdsStr = prodCouponId;
				}
			}
		}
	}

	if(platformRedId != 0) {
		var ths = $("body div[ucid="+ platformRedId +"]")
		$(ths).addClass("on");
	}

	if(shopCouponId != 0) {
		var temp = $("body div[ucid="+ shopCouponId +"]")
		$(ths).addClass("on")
	}

	if(prodCouponId != 0) {
		var temp = $("body div[ucid="+ prodCouponId +"]")
		$(ths).addClass("on")
	}

	if(!isEmpty(userCouponIdsStr)) {
		getCouponsPage(userCouponIdsStr);
	}
}
