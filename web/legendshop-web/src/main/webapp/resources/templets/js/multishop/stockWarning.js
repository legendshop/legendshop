jQuery(document).ready(function(){
  	  userCenter.changeSubTab("stockWarning"); 
  	  
 });

function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("warningFrom").submit();
}

//库存编辑页面
function addStock(stocks, skuId, prodId){
	layer.prompt({
		  formType: 0,
		  value: stocks,
		  title: '更新库存',
		  area: ['800px', '350px'] //自定义文本域宽高
		}, function(value, index, elem){
			var patrn = /^[0-9]*[0-9][0-9]*$/;  //验证是否为数字
			if(!patrn.test(value)){
				layer.alert("输入库存只能为正整数",{icon: 0});
				return;
			}
			//调整库存
			$.ajax({
				type: "post",
				data:{"skuId":skuId,"stocks":value,"prodId":prodId},
             	url: contextPath+"/s/stock/update",
             	dataType: "json",
             	async : true, //默认为true 异步   
             	success: function(msg){
             		layer.msg('更新成功！',{icon:1,time:700},function(){
             			parent.location.reload();
    					layer.close(index);
             		});
             		
                }
			});	
			layer.close(index);
		});
}
	
//全选
function selAll(){
	var allChoose = document.getElementById("checkbox");
	var arrChoose = document.getElementsByClassName("offline");
	var i;
		
	if(allChoose.checked == true){
		for(i=0;i<arrChoose.length;i++){
			arrChoose[i].checked = true;
		}
	}else{
		for(i=0;i<arrChoose.length;i++){
			arrChoose[i].checked = false;
		}
	}
}

function search(){
	$("#warningFrom #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	$("#warningFrom")[0].submit();
}
 
/*function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("from1").submit();
}
	*/