function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("form1").submit();
}
function chooseProd(prodId,prodName,prodPic){
	$("#prodId").val(prodId);
	$("#distCommisContent span").html(prodName);
	$("#distCommisContent img").attr("src",prodPic);
	$("#distContent").hide();
	$("#distCommisContent").show();
}


function addDistProd(){
	var prodId = $("#prodId").val();
	var regex = /^\d+$/;
	var commisRate = $("#commisRate").val().trim();
	if(commisRate==""){
		alert("请输入1-99之间的整数");
	}else if(!regex.test(commisRate)){
		alert("请输入1-99之间的整数");
	}else if(commisRate<1 || commisRate>99){
		alert("请输入1-99之间的整数");
	}else{
		$.ajax({
			url: contextPath+"/s/addDistProd",
			data: {"prodId":prodId,"distCommisRate":commisRate/100},
			type:'post',
			async : true, //默认为true 异步
			dataType : 'json',
			success:function(data){
				if(data=="OK"){
					parent.window.location ="${contextPath}/s/joinDistProd";
				}else{
					alert("提交失败");
				}
			}
		});
	}
}
