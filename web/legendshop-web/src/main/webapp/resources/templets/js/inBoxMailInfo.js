$(document).ready(function() {
	var pageValue =$(".o-mt h2").html(); 
	if(pageValue=="发件箱" || pageValue=="系统邮件"){
		$(".wH").attr("style","display:none");
	}
});

function dvReplyInput(){
	$("#dvReplyBts").attr("style","");
	$("#mailInput").attr("style","height:70px");
	$("#mailInput textarea").attr("style","width:880px;height:68px");
	$("#mailInput label").html("");
	$("#replyContent").val("");
}
$("#cancelButton span").bind('click', function () {
	$("#dvReplyBts").attr("style","display:none");
	$("#mailInput").removeAttr("style");
	$("#mailInput textarea").removeAttr("style");
	$("#mailInput textarea").val("");
	$("#mailInput label").html("快捷回复");
});

$("#mailButton span").bind('click', function () {
	var mailValue = $("textarea").val();
	
	if(title.length < 46){
		title = "回复: "+title;
	}
	if(mailValue.length==0){
		layer.alert("内容不能为空！",{icon:0});
	}else{
		var data = {
				"receiveNames": receName,
				"title": title,
				"text": mailValue
		};
		$.ajax({
			url:contextPath + "/p/sendInformation" , 
			data: data,
			type:'post', 
			dataType : 'json', 
			async : false, //默认为true 异步   
			success:function(result){
				layer.msg(result,{icon: 1,time: 800},function(){
					$("#outbox").click();
				});	
			}
		});
	}
});

/**根据msgId,删除邮件**/
function deleteMsg(msgId){

	layer.confirm("请确定删除该条信息？", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		var pageValue =$(".o-mt h2").html(); 
		var url;
		if(pageValue=="发件箱"){
			url = contextPath + "/p/deleteOutBox";
		}else if(pageValue=="收件箱"){
			url = contextPath + "/p/deleteInBox";
		}else{
			url = contextPath + "/p/deleteSystemMsg";
		}
		$.ajax({
			"url":url , 
			data: {"msgId":msgId},
			type:'post', 
			dataType : 'json', 
			async : false, //默认为true 异步   
			success:function(result){
				if('OK' == result){
					layer.msg('删除邮件成功',{icon: 1,time: 800},function(){
						goback();
					});
				}else{
					layer.msg('删除邮件失败',{icon: 2});
				}
			}
		});
	});
}