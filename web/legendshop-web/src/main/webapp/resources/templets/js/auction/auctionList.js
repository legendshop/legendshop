var set;
clearInterval(set);
set = setInterval(auctionProdToEnd, 1000);
set = setInterval(setRtime, 10);
//更改剩余时间
function setRtime() {
	var auctionProds = $('div[id^="auctionProd_"]');
	for ( var i = 0; i < auctionProds.length; i++) {
		var rTime = $(auctionProds[i]).attr("r") - 10;
		if (rTime > 0) {
			$(auctionProds[i]).attr("r", rTime);
		}
	}
}

//时间格式 一位数时补零
function rightZeroStr(v) {
	if (v < 10) {
		return "0" + v;
	}
	return v + "";
}

//计算时间差
function timeBetweenText(rTime, showType) {
	var timeOffset = rTime;//rTime=endTime-nowTime  
	var dayOfMil = (24 * 60 * 60 * 1000);
	var hourOfMil = 60 * 60 * 1000;
	var minOfMil = 60 * 1000;
	var secOfMil = 1000;
	var hourOffset = timeOffset % dayOfMil;//时偏离的毫秒数
	var minuteOffset = hourOffset % hourOfMil;//分钟偏离的毫秒数
	var seccondOffset = minuteOffset % minOfMil;//秒偏离的毫秒数
	var days = Math.floor(timeOffset /dayOfMil);//日
	var hours = Math.floor(timeOffset / hourOfMil)%24;//时
	var minutes = Math.floor(minuteOffset / minOfMil);//分
	var secconds = Math.floor(seccondOffset / secOfMil);//秒
	/*var mmi = parseInt((timeOffset%1000)/10);*/
	//显示形式
	if(typeof(showType) == "undefined" || showType == 0){
		if(days > 0){
			return "<b>"+rightZeroStr(days)+"</b><span>日</span><b>"+rightZeroStr(hours)+"</b><span>时</span><b>"+rightZeroStr(minutes) +"</b><span>分</span><b>"+rightZeroStr(secconds) + "</b><span>秒</span>";
		}
		else if (hours > 0) {
			return "<b>"+rightZeroStr(hours)+"</b><span>时</span><b>"+rightZeroStr(minutes) +"</b><span>分</span><b>"+rightZeroStr(secconds) + "</b><span>秒</span>";
		} else if (minutes > 0) {
			return "<b>"+rightZeroStr(minutes)+"</b><span>分</span><b>"+rightZeroStr(secconds)+"</b><span>秒</span>";
		} else if (secconds > 0) {
			return "<b>"+rightZeroStr(secconds)+"</b><span>秒</span>";
		} else{
			return "<b>0.00</b><span>秒</span>";
		}
		/*else if (mmi > 0) {
        	return "0."+mmi+"秒";
        } else{
        	return "0.00秒";
        }*/

	}else if(showType == 1){
		//显示格式 00:00:00:00
		return rightZeroStr(days)+":"+rightZeroStr(hours)+":"+rightZeroStr(minutes) +":"+rightZeroStr(secconds);
	}else if(showType == 2){
		if (hours > 0) {
			return formatTime(hours)+"<em>:</em>"+formatTime(minutes) +"<em>:</em>"+formatTime(secconds);
		} else if (minutes > 0) {
			return "<span>0</span><span>0</span><em>:</em>"+formatTime(minutes)+"<em>:</em>"+formatTime(secconds);
		} else if (secconds > 0) {
			return "<span>0</span><span>0</span<em>:</em><span>0</span><span>0</span<em>:</em>"+formatTime(secconds);
		} else{
			return "<span>0</span><span>0</span><em>:</em><span>0</span><span>0</span><em>:</em><span>0</span><span>0</span>";
		}

	}else{
		return "";
	}
}

function formatTime(time){
	time = time+"";
	var text = "";
	for(i=0;i<time.length;i++){
		var t = "<span>"+time.charAt(i)+"</span>";
		text+=t;
	}
	return text;
}

function auctionProdToEnd(){
	generalAuctionProdToEnd();

}

//更新倒计时及样式
/*function generalAuctionProdToEnd(){
	var auctionDivs = $('li[id^="id_"]');//整个li列表
	var auctionProds = $('div[id^="auctionProd_"]');//仅包含倒计时模块
	for(var i=0;i<auctionProds.length;i++){
		var auctionLi=$(auctionDivs[i]);//整个li
		var li = $(auctionProds[i]);//仅包含倒计时模块
		var endTime = $(auctionProds[i]).attr("endTime");
		var startTime = $(auctionProds[i]).attr("startTime");
		var rTime = $(auctionProds[i]).attr("r");//时间差
		var astatus = $(auctionProds[i]).attr("a");
		var nowTime = new Date().getTime();
		if(astatus==3){
		}else if(startTime>nowTime){
			$(auctionProds[i]).attr("a",0);
			astatus=0;
		}else if(endTime<nowTime){
			$(auctionProds[i]).attr("a",2);
			astatus=2;
		}else{
			$(auctionProds[i]).attr("a",1);
			astatus=1;
		}
		var running = $(auctionLi).find('.p-price').html();
		var text;
		var progress = 0;
		if(astatus=="0"){
			//等待拍卖
			text = timeBetweenText(rTime, 0);
			var texts = [];
			texts = text.split(":");
			var str = '<div class=\"p-time\">距离开拍:'
				+ '<b>'+texts[0]+'</b><span>日</span><b>'+texts[1]+'</b><span>时</span><b>'+texts[2]+'</b><span>分<span><b>'+texts[3]+'</b><span>秒</span></div><div class=\"p-state\" ><span  style=\"width:0%;\"></span></div>';
			str = '<div class=\"p-time\">距离开拍：<div>'+texts[0]+'</div></div><div class=\"p-state\" ><span  style=\"width:0%;\"></span></div>';
			$(li).html(str);
			$(auctionLi).find(".p-price a em").text("等待开拍");
			$(auctionLi).find(".p-price a em").css("margin-left","90px");
		}else if(astatus=="2"){
			//拍卖结束			
			$(li).html('<div class=\"p-time\">竞拍已结束</div><div class=\"p-state\" ><span  style=\"width:0%;\"></span></div>');
			$(auctionLi).find('.p-price').html("<a href='javascript:void(0)' style='background-color:#ccc;padding-top:10px'><center>竞拍结束</center></a>");
		}else if(astatus=="3"){
			//拍卖已完成			
			$(li).html('<div class=\"p-time\">竞拍已完成</div><div class=\"p-state\" ><span  style=\"width:0%;\"></span></div>');
			$(auctionLi).find('.p-price').html("<a href='javascript:void(0)' style='background-color:#F0A605;padding-top:10px'><center>竞拍完成</center></a>");
		}else{
			//正在拍卖
			text = timeBetweenText(rTime, 0);
			if(rTime<=100){
				if(endTime-nowTime<=100){
					return;
				}else{
					$(auctionProds[i]).attr("r",endTime-nowTime);
					 rTime=$(auctionLi).attr("r");
				}

			}
			var _1=parseInt(rTime)*100;
			var _2=(parseInt(endTime)-parseInt(startTime));
			progress = 100-(_1/_2).toFixed(2);
			var texts = [];
			try{
				texts = text.split(":");
			}catch(e){texts="nodata";}
			var str = "";
			if(texts!="nodata"){
				showType=0
					str = '<div class=\"p-time\" style=\"overflow:hidden\">剩余：<div>'+texts[0]+'</div></div></div>';
				showType=1
				str = '<div class=\"p-time\">剩余时间：<b>'+texts[0]+'</b><span>日</span><b>'+texts[1]+'</b><span>时</span><b>'+texts[2]+'</b><span>分</span><b>'+texts[3]+'</b><span>秒<span>'+'</div>';
			}
			str+=" <div class='p-state' ><span  style='width:"+progress+"%;'></span></div>";
			$(li).html(str);
			$(li).find(".p-time *").css("color","#E5004F");
			var curPrice = $(auctionLi).find(".p-curPrice").attr("price");
			$(auctionLi).find(".p-price a em").html("当前竞拍价：¥"+curPrice);
			$(auctionLi).find(".p-price a").css("background-color","#E5004F");
			$(auctionLi).find(".p-price a em").css("margin-left","40px");

			var biddingNumber = $(auctionLi).find(".p-times").attr("biddingNumber");
			if(biddingNumber==undefined||biddingNumber==''){
				biddingNumber="暂无出价";
			}
			$(auctionLi).find(".p-times span").html(biddingNumber);
		}
	}
}*/
//计算时间差
function timeBetweenText(rTime, showType) {
	var timeOffset = rTime;//rTime=endTime-nowTime  
	var dayOfMil = (24 * 60 * 60 * 1000);
	var hourOfMil = 60 * 60 * 1000;
	var minOfMil = 60 * 1000;
	var secOfMil = 1000;
	var hourOffset = timeOffset % dayOfMil;//时偏离的毫秒数
	var minuteOffset = hourOffset % hourOfMil;//分钟偏离的毫秒数
	var seccondOffset = minuteOffset % minOfMil;//秒偏离的毫秒数
	var days = Math.floor(timeOffset /dayOfMil);//日
	var hours = Math.floor(timeOffset / hourOfMil)%24;//时
	var minutes = Math.floor(minuteOffset / minOfMil);//分
	var secconds = Math.floor(seccondOffset / secOfMil);//秒
	/*var mmi = parseInt((timeOffset%1000)/10);*/
	//显示形式
	if(typeof(showType) == "undefined" || showType == 0){
		if(days > 0){
			return "<b>"+rightZeroStr(days)+"</b><span>日</span><b>"+rightZeroStr(hours)+"</b><span>时</span><b>"+rightZeroStr(minutes) +"</b><span>分</span><b>"+rightZeroStr(secconds) + "</b><span>秒</span>";
		}
		else if (hours > 0) {
			return "<b>"+rightZeroStr(hours)+"</b><span>时</span><b>"+rightZeroStr(minutes) +"</b><span>分</span><b>"+rightZeroStr(secconds) + "</b><span>秒</span>";
		} else if (minutes > 0) {
			return "<b>"+rightZeroStr(minutes)+"</b><span>分</span><b>"+rightZeroStr(secconds)+"</b><span>秒</span>";
		} else if (secconds > 0) {
			return "<b>"+rightZeroStr(secconds)+"</b><span>秒</span>";
		} else{
			return "<b>0.00</b><span>秒</span>";
		}
		/*else if (mmi > 0) {
        	return "0."+mmi+"秒";
        } else{
        	return "0.00秒";
        }*/

	}else if(showType == 1){
		//显示格式 00:00:00:00
		return rightZeroStr(days)+":"+rightZeroStr(hours)+":"+rightZeroStr(minutes) +":"+rightZeroStr(secconds);
	}else if(showType == 2){
		if (hours > 0) {
			return formatTime(hours)+"<em>:</em>"+formatTime(minutes) +"<em>:</em>"+formatTime(secconds);
		} else if (minutes > 0) {
			return "<span>0</span><span>0</span><em>:</em>"+formatTime(minutes)+"<em>:</em>"+formatTime(secconds);
		} else if (secconds > 0) {
			return "<span>0</span><span>0</span<em>:</em><span>0</span><span>0</span<em>:</em>"+formatTime(secconds);
		} else{
			return "<span>0</span><span>0</span><em>:</em><span>0</span><span>0</span><em>:</em><span>0</span><span>0</span>";
		}

	}else{
		return "";
	}
}

function formatTime(time){
	time = time+"";
	var text = "";
	for(i=0;i<time.length;i++){
		var t = "<span>"+time.charAt(i)+"</span>";
		text+=t;
	}
	return text;
}

function auctionProdToEnd(){
	generalAuctionProdToEnd();

}

//更新倒计时及样式
function generalAuctionProdToEnd(){
	var auctionDivs = $('li[id^="id_"]');//整个li列表
	var auctionProds = $('div[id^="auctionProd_"]');//仅包含倒计时模块
	for(var i=0;i<auctionProds.length;i++){
		var auctionLi=$(auctionDivs[i]);//整个li
		var li = $(auctionProds[i]);//仅包含倒计时模块
		var endTime = $(auctionProds[i]).attr("endTime");
		var startTime = $(auctionProds[i]).attr("startTime");
		var rTime = $(auctionProds[i]).attr("r");//时间差
		var astatus = $(auctionProds[i]).attr("a");
		var nowTime = new Date().getTime();
		if(astatus==3){
		}else if(startTime>nowTime){
			$(auctionProds[i]).attr("a",0);
			astatus=0;
		}else if(endTime<nowTime){
			$(auctionProds[i]).attr("a",2);
			astatus=2;
		}else{
			$(auctionProds[i]).attr("a",1);
			astatus=1;
		}
		var running = $(auctionLi).find('.p-price').html();
		var text;
		var progress = 0;
		if(astatus=="0"){
			//等待拍卖
			text = timeBetweenText(rTime, 0);
			var texts = [];
			texts = text.split(":");
			str = '<div class=\"pro-time\">距离开拍：'+texts[0]+'</div><div class=\"p-state\" ><span  style=\"width:0%;\"></span></div>';
			$(li).html(str);
		}else if(astatus=="1"){
			//正在拍卖
			text = timeBetweenText(rTime, 0);
			if(rTime<=100){
				if(endTime-nowTime<=100){
					return;
				}else{
					$(auctionProds[i]).attr("r",endTime-nowTime);
					rTime=$(auctionLi).attr("r");
				}
			}
			var texts = [];
			try{
				texts = text.split(":");
			}catch(e){texts="nodata";}

			var str = "";
			if(texts!="nodata"){
				str = '<div class=\"pro-time\" style=\"overflow:hidden\">剩余时间：'+texts[0]+'</div>';
			}
			str+=" <div class='p-state' ><span style=\"-left:20px\"></span></div>";
			$(li).html(str);
		}
	}
}
$(document).ready(function(){
	var auctionProds = $('div[id^="auctionProd_"]');//仅包含倒计时模块
	for(var i=0;i<auctionProds.length;i++){
		var li = $(auctionProds[i]);//仅包含倒计时模块
		var astatus = $(auctionProds[i]).attr("a");
		if(astatus=="2"){
			//拍卖结束
			$(li).html('<div class=\"pro-time\" style=\"font-size: medium;\">竞拍已结束</div><div class=\"p-state\" ><span  style=\"width:0%;\"></span></div>');
			$(li).parents(".falg").find(".pro-img").append('<em>已结束</em>');
		}else if(astatus=="3"){
			//拍卖已完成
			$(li).html('<div class=\"pro-time\" style=\"font-size: medium;\">竞拍已结束</div><div class=\"p-state\" ><span  style=\"width:0%;\"></span></div>');
			$(li).parents(".falg").find(".pro-img").append('<em>已结束</em>');
		}
	}
});


function isnull(v){
	return v==null||typeof(v) == "undefined";
}

function pager(curPageNO) {
	document.getElementById("curPageNO").value = curPageNO;
	document.getElementById("form1").submit();
}
$(document).ready(function() {
	$(".auc-load-list em").mouseover(function() {
		$("#loadTypeSelect").show();
	});
	$("#loadTypeSelect").mouseover(function() {
		$("#loadTypeSelect").show();
	});
	$("#loadTypeSelect li").mouseout(function() {
		$("#loadTypeSelect").hide();
	});
	$(".nch-sortbar-array ul li").bind("click",
			function() {
		var id = $(this).attr("id");
		var orderDir = "";
		$(".nch-sortbar-array ul li").each(
				function(i) {
					if (id != $(this).attr("id")) {
						$(this).removeClass("selected");
					}
				});
		$(this).addClass("selected");
		var _a = $(this).find("a");
		if (id == 'startTime') {
			if ($(_a).hasClass("desc")) {
				orderDir = "a.start_time,asc";
			} else if ($(_a).hasClass("asc")) {
				orderDir = "a.start_time,desc";
			} else {
				$(_a).removeClass("asc");
				orderDir = "a.start_time,desc";
			}
		} else if (id == 'floorPrice') {
			if ($(_a).hasClass("desc")) {
				orderDir = "a.floor_price,asc";
			} else if ($(_a).hasClass("asc")) {
				orderDir = "a.floor_price,desc";
			} else {
				orderDir = "a.floor_price,desc";
			}
		} else if (id == 'default') {
			orderDir = "a.status,asc";
		}
		//var loadType = "${param.loadType}";
		if (loadType == undefined
				|| loadType == "") {
			loadType = 1;
		}
		window.location = contextPath+"/auction/autionList?curPageNO=" + _page + "&loadType="+ loadType + "&orders=" + orderDir;
	});

});

//var loadType;
function loadTypeClick(e) {
	loadType = $(e).attr("value");
	window.location = contextPath+"/auction/autionList?curPageNO=" + _page + "&loadType="+ loadType;
};
/*function getData(orderDir, type) {
	var data = {
			curPageNO : _page
			orders : orderDir,
			loadType : type
	}
	return data;
}*/


