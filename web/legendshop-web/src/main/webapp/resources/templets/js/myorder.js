$(document).ready(function(){

  // $(".presellType").each(function () {
  //   //初始化定金金额
  //   var $this=$(this)
  //   var sub=$this.parents(".pay");
  //   var presellPrice =sub.find(".actualTotal").text(); // 预售价
  //   var preDepositPrice = sub.find(".preDepositPrice");// 定金价
  //   var finalPayment =sub.find(".finalPrice"); // 尾款
  //   var percentNum = parseFloat(preDepositPrice.text()/100);//定金百分比
  //   var preVal = parseFloat(presellPrice*percentNum)
  //   var finalPayVal = parseFloat(presellPrice-preVal);
  //   preDepositPrice.text(numberConvert(preVal));
  //   finalPayment.text(numberConvert(finalPayVal));
  // })


	userCenter.changeSubTab("myorder");
	 $(".selectAllNotPayOrder").click(function(){

		if($(this).attr("checked")=="checked"){
			$(this).parent().parent().addClass("checkbox-wrapper-checked");
	         $(".selector").each(function(){
	        	 var disabled =$(this).attr("disabled");
	        	 if(disabled==null || disabled=="" || disabled==undefined){
	        		 $(this).attr("checked",true);
	        		 $(this).parent().parent().addClass("checkbox-wrapper-checked");
	        	 }
	         });
	     }else{
	         $(".selector").each(function(){
	             $(this).attr("checked",false);
	             $(this).parent().parent().removeClass("checkbox-wrapper-checked");
	         });
	         $(this).parent().parent().removeClass("checkbox-wrapper-checked");
	     }
	 });
	 initTime();
});

function selector(obj){
	var flag = true;
	if(!obj.checked){
		$(".selectAllNotPayOrder").prop("checked",false);
		flag = false;
	}
	 $(".selector").each(function(){
		 if(this.checked){
    		 $(this).prop("checked",true);
    		 $(this).parent().parent().addClass("checkbox-wrapper-checked");
        	 flag = true;
		 }else{
			 $(this).prop("checked",false);
             $(this).parent().parent().removeClass("checkbox-wrapper-checked");
             flag = false;
		 }
	 });
	 if(flag){
		 $(".selectAllNotPayOrder").prop("checked",true);
		 $(".selectAllNotPayOrder").parent().parent().addClass("checkbox-wrapper-checked");
	 }else{
		 $(".selectAllNotPayOrder").prop("checked",false);
		 $(".selectAllNotPayOrder").parent().parent().removeClass("checkbox-wrapper-checked");
	 }
}

function mergerOrder(){
    var items = new Array();
    var i=0;
    $(".selector").each(function(){
        if($(this).attr("checked")=="checked"){
            items[i]=$(this).val();
            i++;
        }
    });
    if(items.length==0){
        layer.msg("请选择宝贝付款,可以选择多个!",{icon: 0});
        return;
    }
    var orderIds = items.join(",");
    abstractForm(path+"/p/orderSuccess",orderIds);
}

function abstractForm(URL, subNums){
	   var temp = document.createElement("form");
	   temp.action = URL;
	   temp.method = "post";
	   temp.style.display = "none";
	   var opt = document.createElement("textarea");
	   opt.name = 'subNums';
	   opt.value = subNums;
	   temp.appendChild(opt);
	   temp.appendChild(appendCsrfInput());
	   document.body.appendChild(temp);
	   temp.submit();
	   return temp;
}


function pager(curPageNO){
	$("input[name=curPageNO]").val(curPageNO);
	$("#order_search").submit();
}

var cancleOrderFlag=0;
//取消订单弹窗
var cancleOrder=function(id){
	layer.open({
		title : '取消订单',
		id : 'cancleOrder',
		type : 2,
		content : [contextPath + "/p/order/cancleOrderShow?subNumber="+id, 'no'],
		area: ['450px', '300px'],
	});
}

/**
 * 确认收货
 * @param _number
 */
function orderReceive(_number){
	layer.open({
		  type: 1
		  ,anim: 5
		  ,shadeClose: false
		  ,area: ['420px', '240px'] //宽高
		  ,content: "<div class='eject_con'><dl><dt>订单编号：</dt><dd>"+_number+" <p class='hint'>请注意： 如果你尚未收到货品请不要点击“确认”。大部分被骗案件都是由于提前确认付款被骗的，请谨慎操作！ </p></dd></dl></div>"
		  ,btn: ['确定','关闭'] //按钮
		  ,yes: function(index, layero){
			  $.ajax({
					url:path+"/p/orderReceive/"+_number,
					type:'POST',
					async : false, //默认为true 异步
					dataType:'text',
					success:function(result){
						console.log("result = " + result);
						if(result=="OK"){
							var msg = "确认收货成功";
							layer.msg(msg,{icon:1, time:1000},function(){
								parent.location.reload();
							});
						}else if(result=="fail"){
							var msg = "确认收货失败";
							layer.alert(msg,{icon: 2});
						}
					}
				});
			  }
		})
	}

	//删除订单
	var removeOrder=function(id){
		layer.confirm('您确定要删除吗?删除后该订单可以在回收站找回，或彻底删除？', {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   $.ajax({
					url:path+'/p/order/removeOrder',
					data:{"subNumber":id},
					type:'POST',
					async : false, //默认为true 异步
					dataType:'text',
					success:function(result){
						if(result=="OK"){
							var msg = "删除订单成功";
							layer.msg(msg, {icon: 1,time :2000},function(){
								window.location.reload();
							})
						}else if(result=="fail"){
							var msg = "删除订单失败";
							layer.msg(msg, {icon: 2,time :2000},function(){
								window.location.reload();
							})
						}
					}
				});
		   });
	}


	//永久删除订单
	var dropOrder=function(id){
		layer.confirm('您确定要永久删除吗?永久删除后您将无法再查看该订单，也无法进行投诉维权，请谨慎操作！', {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   $.ajax({
			    	 url:path+'/p/dropOrder/'+id,
						type:'POST',
						async : false, //默认为true 异步
						dataType:'text',
						success:function(result){
							if(result=="OK"){
								var msg = "删除订单成功";
								layer.msg(msg, {icon: 1,time :2000},function(){
									window.location.reload();
								})
							}else if(result=="fail"){
								var msg = "删除订单失败";
								layer.msg(msg, {icon: 2,time :2000},function(){
									window.location.reload();
								})
							}
						}
					});
		   })
	}

	//订单还原
	function restoreOrder(id) {
		layer.confirm('您确定要还原该订单吗?', {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   $.ajax({
					url:path+'/p/order/restoreOrder',
					data:{"subNumber":id},
					type:'POST',
					async : false, //默认为true 异步
					dataType:'text',
					success:function(result){
						if(result=="OK"){
							var msg = "还原订单成功";
							layer.msg(msg, {icon: 1,time :2000},function(){
								window.location.reload();
							})
						}else if(result=="fail"){
							var msg = "还原订单失败";
							layer.msg(msg, {icon: 2,time :2000},function(){
								window.location.reload();
							})
						}
					}
				});
		   });
}


	function initTime(){
		countdown();
		auctionCountdown();
	}
	//倒计时结束订单
	function countdown(){
		$('.pay-td .countdown').each( function(index,element) {
			var date=$(element).attr("datetime");
              var startTime=new Date(date);
              startTime.setMinutes(startTime.getMinutes()+Number(order_cancel));
        	  $(element).countdown(startTime,function(event) {
        		  $(this).html(event.strftime('<i class="icon-countdown"></i>剩余<lable data-cd-type="minutes">%M</lable>分<lable  data-cd-type="second">%S</lable>秒'));
        	   }).on('finish.countdown', function(event) {
        		   window.location.reload();
        	   });
		});
	}
	function auctionCountdown(){
		$('.pay-td .auctionCountdown').each( function(index,element) {
			var date=$(element).attr("datetime");
              var startTime=new Date(date);
              startTime.setMinutes(startTime.getMinutes()+Number(auction_order_cancel));
        	  $(element).countdown(startTime,function(event) {
        		  $(this).html(event.strftime('<i class="icon-countdown"></i>剩余<lable data-cd-type="day">%D</lable>天<lable data-cd-type="hour">%H</lable>时<lable data-cd-type="minutes">%M</lable>分<lable  data-cd-type="second">%S</lable>秒'));
        	   }).on('finish.countdown', function(event) {
        		   window.location.reload();
        	   });
		});
	}

function numberConvert(num) {

  var numConvert = Math.round(num * 1000) / 1000;
  var number = numConvert.toString();

  var index =  number.indexOf('.');
  var price = index != -1?number.substring(0,index+3):number;
  return price;
}
