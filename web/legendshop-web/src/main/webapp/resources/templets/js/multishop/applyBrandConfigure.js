var isSubmitted = false; 
function loadbrandList(){
   		  sendData(contextPath+"/s/applyBrand");
}
 
     	$(document).ready(function(e) {
				userCenter.changeSubTab("applyBrand"); //高亮菜单
				
				//校验是否是修改,如果是修改则忽略掉校验图片
				if(!isBlank(brandPic)){
					$("#file").addClass("ignore");
				}
				if(!isBlank(brandPicMobile)){
					$("#mobileLogoFile").addClass("ignore");
				}
				if(!isBlank(bigImage)){
					$("#bigImagefile").addClass("ignore");
				}
				if(!isBlank(brandAuthorize) ){
					$("#brandAuthorizefile").addClass("ignore");
				}
				
				jQuery.validator.addMethod("checkBrandName", function(value,element){
		    		var returnMsg=true;
		    		jQuery.ajax({
		    			url: contextPath+"/s/brand/checkBrandByName",
		    			data: {"brandName" : $("#brandName").val(),"brandId" : $("#brandId").val()},
		    			type: "post",
		    			async: false,
		    			success: function(retData){
		    				if(retData=='true'){
		    					returnMsg=false;
		        			}
		    			}
		    		});
		    		return returnMsg;
		    	}, "此品牌已被创建!"); 
				
				jQuery("#form1").validate({
					ignore: ".ignore",
					rules: {
						brandName:{
							"required":true,
							checkBrandName: true,
							maxlength:20//期望的是true,如果为false则展示提示信息
						},
						 file:{
							"required":true,
						},
            mobileLogoFile:{
							"required":true,
						},

						brandAuthorizefile:{
							"required":true,
						},
						brief:{
							"required":true,
							maxlength: 100,
						}
					},
					messages: {
						brandName:{
							required:"请填写品牌名称",
							maxlength:"品牌名称过长"
						},
						file:{
							required:"请上传PC端品牌LOGO",
						},
            mobileLogoFile:{
							required:"请上传手机端品牌LOGO",
						},
						brandAuthorizefile:{
							required:"请上传品牌产品授权书原件扫描件",
						}, 
						brief:{
							required:"请填写品牌介绍",
							maxlength: "品牌介绍不能超过100个字符！"
						}
					},
					submitHandler : function(form){
						//防止表单重复提交
				    	if(!isSubmitted){
				    		form.submit();
				    		isSubmitted = true;
				    	}else{
				    		layer.alert('请不要重复提交表单!', {icon: 0});
				    	}
					}
     			});
					/*$("input[name=brandName]").change(function(){
						checkBrandName();
					});*/
					$("input[name=file]").change(function(){
 						checkImgType(this);
						checkImgSize(this,5120);
 					});
					$("input[name=mobileLogoFile]").change(function(){
 						checkImgType(this);
						checkImgSize(this,5120);
 					});
 					$("input[name=brandAuthorizefile]").change(function(){
 						checkImgType(this);
						checkImgSize(this,5120);
 					});
 					$("input[name=bigImagefile]").change(function(){
 						checkImgType(this);
						checkImgSize(this,5120);
 					});
		});
     	
     	function isBlank(value){
     		return value == undefined || value == null || $.trim(value) == "";
     	}
