$(document).ready(function() {
	
	$(".menu").mouseover(function(){
		var _this = $(this);
		 _this.addClass("on");
	});
	$(".menu").mouseout(function(){
		var _this = $(this);
		 _this.removeClass("on");
	});
	
    //首页判断
    if($("#isIndex").val()) {
        //判断是否显示分类导航
        $(".showlist").show();
    }else{
    	$(".showlist").hide();
    	
    	 $(".menu-cate").bind("mouseover",function(){
    		 $(this).find(".showlist").show();
         });
         $(".menu-cate").bind("mouseout",function(){
        	 $(this).find(".showlist").hide();
         });
    	   
    };
    
    var t = "";
    $(".showlist dl ").mouseover(function(){
    	var cur = $(this);
        var index = $(this).index();
        $(cur).children("dd").show();
        $(cur).siblings().find("dd").hide();
    }).mouseout(function(){
    	$(this).find("dd").hide();
    });
    
    $(".menu-drop").mouseover(function(){
    	$(this).addClass("z-menu-drop-open")
    }).mouseout(function(){
    	$(this).removeClass("z-menu-drop-open")
    });
    
    calUnreadMsgCount();
});

function calUnreadMsgCount() {
    //刷新top bar 区域
    $.ajax({
        type : "GET",
        url :"/calUnreadMsgCount",
        data : null,
        success : function(response) {
            $("#msgCount").html(response);
            $(".msgCount").html(response);
        }
    });
};

//首页展开隐藏
function proSort(){
    var st = 0;
    $(".showlist").bind("mouseenter",function(){
        st = setTimeout(function(){
        },200);
    });
    $(".showlist").bind("mouseleave",function(){
        clearTimeout(st);
    });
};

function hideSort(){
    $(".showlist dl dd").hide();
};

setTimeout(function(){
	/**
	 * 检测浏览器版本 ,如果过低, 则给出相关的提示
	 */
	!function() {
	    var a, c, h;
	    if (c = window.navigator.userAgent, h = /;\s*MSIE (\d+).*?;/.exec(c), h && +h[1] < 9) {
	        if (a = document.cookie.match(/(?:^|;)\s*ic=(\d)/), a && a[1])
	            return;
	        
	        $("body").prepend(["<div id='js-compatible' class='compatible-contianer'>", "<p class='cpt-ct'><i></i>您的浏览器版本过低。为保证最佳的购物体验，<a href='/browser' target='_blank'>请更新浏览器版本或下载其他浏览器</a></p>", "<div class='cpt-handle'><a href='javascript:;' class='cpt-agin'>以后再说</a><a href='javascript:;' class='cpt-close'><i></i></a>", "</div>"].join("")),
	       
	        $("#js-compatible .cpt-agin").click(function() {
	            var d = new Date;
	            d.setTime(d.getTime() + 2592e6),
	            document.cookie = "ic=1; expires=" + d.toGMTString() + "; path=/",
	            $("#js-compatible").remove()
	        }),
	        
	        $("#js-compatible .cpt-close").click(function() {
	            $("#js-compatible").remove()
	        })
	    }
	}();
}, 0);

		