document.documentElement.style.overflow='hidden';

loadProdList();

$(document).delegate("#submit","click",function(){
	loadProdList();
});

function loadProdList(){
	$.ajax({
		url:contextPath+'/s/loadProdListForTag',
		type:"POST",		       		     
		data:$('#form1').serialize(),
		success: function(data) {
			$("#prodListTable").html(data);	           
		}
	});
}

function submitProd(){
	$(this).attr('disabled', 'disabled');
	var tableLength =  $("table[class='prodList'] tr").length;
	if(tableLength<=1){
		alert("请添加商品数据!");
		return;
	}

	var prodIdList="";
	$("table[class='prodList'] tr").each(function (index,domEle){				 		
		var prod_id=$(this).find("#prodId").val();

		if(!(prod_id==null||prod_id==""||prod_id==undefined)){
			prodIdList += prod_id+",";				
		}
	});

	var tagId = $("#tagId").val();			
	$.ajax({
		url:contextPath+"/s/addProdToTag/"+prodIdList+"/"+tagId,
		type:'get',		       		     
		data:'',
		dataType:'json',
		success: function(data) {
			if(data == "OK"){
				alert("添加成功！");		        		 
				parent.window.location.reload(); 
			}else{
				$(this).removeAttr('disabled');
				alert("添加失败！");
			}      
		},
		error: function(data){
			$(this).removeAttr('disabled');
			alert("添加失败！");
		}
	});					

}

function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	$("#submit").click();
}

function deleteProd(obj){
	$(obj).parent().parent().remove();
}