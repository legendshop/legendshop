$(document).ready(function() {
	userCenter.changeSubTab("FULL_COURT_MARKETING"); //高亮菜单
});

function onlineShopMarketing(_id){
	layer.confirm('确定要发布该活动么？',{
		icon: 3
		,btn: ['确定','取消'] //按钮
	}, function () {
		window.location.href=contextPath+"/s/onlineShopMarketing/1/"+_id;
	});
}

function offlineShopMarketing(_id){
	layer.confirm('确定要下线该活动么？',{
		icon: 3
		,btn: ['确定','取消'] //按钮
	}, function () {
		window.location.href=contextPath+"/s/offlineShopMarketing/1/"+_id;
	});
}

function deleteShopMarketingMansong(_id){
	layer.confirm('确定要删除活动么？',{
		icon: 3
		,btn: ['确定','取消'] //按钮
	}, function () {
		window.location.href=contextPath+"/s/deleteShopMarketing/1/"+_id;
	});

}