$(function() {
  //初始化定金金额
  // var presellPrice =$("#actualTotal").text(); // 预售价
  // var preDepositPrice = $("#preDepositPrice");// 定金价
  // var finalPayment =$("#finalPrice"); // 尾款
  // var percentNum = parseFloat(preDepositPrice.text()/100);//定金百分比
  // var preVal = parseFloat(presellPrice*percentNum)
  // var finalPayVal = parseFloat(presellPrice-preVal);
  // $(".preDepositPrice").text(numberConvert(preVal));
  // $(".finalPrice").text(numberConvert(finalPayVal));


	userCenter.changeSubTab("presellOrdersManage");

	// 打印发货单
	$(document).on("click", ".printInvoice", function() {
		var _subId = $(this).attr("data-subId");
		var _dvyTypeId = $(this).attr("data-dvyTypeId");
		$.ajax({
			url : contextPath + '/s/hasShopAddress',
			type : 'POST',
			dataType : 'json',
			success : function(result) {
				if (result.status == "OK") {
					var url = contextPath + "/s/printDeliveryDocument/" + _subId + "/" + _dvyTypeId;
					window.open(url);
				} else {
					layer.confirm('请先去设置商家地址', {
						btn : [ '确定', '取消' ]
					}, function(index) {
						layer.close(index);
						var url = contextPath + "/s/shopSetting";
						window.open(url);
					});
				}
			}
		});
	});

});

function pager(curPageNO){
   document.getElementById("curPageNO").value=curPageNO;
   document.getElementById("ListForm").submit();
}

//确认发货
function deliverGoods(_orderId){

	layer.open({
		  title :"确认发货",
		  id: "deliverGoods",
		  type: 2,
		  content: [contextPath+'/s/deliverGoods/'+_orderId,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['430px', '250px']
		});
 }

//修改物流单号
 function changeDeliverNum(_orderId){

    	layer.open({
  		  title :"修改物流单号",
  		  id: "changeDeliverNum",
  		  type: 2,
  		  resize: false,
  		  content: [contextPath+'/s/changeDeliverNum/'+_orderId,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
  		  area: ['430px', '250px']
  		});
 }

 //调整定金金额
 function changeOrderFee(_orderId){
	 layer.open({
		  title :"调整订单金额",
		  id: "changeOrderFee",
		  type: 2,
		  resize: false,
		  content: [contextPath+'/s/changeOrderFee/'+_orderId,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['430px', '380px']
		});
 }

//取消订单
function cancleOrder(id){
		layer.confirm('你确定取消这个订单？',{
	  		 icon: 3
	  	     ,btn: ['确定','取消'] //按钮
	  	   }, function () {
		     $.ajax({
					url:contextPath+'/s/order/cancleOrder',
					data:{"subNumber":id},
					type:'POST',
					async : false, //默认为true 异步
					dataType:'text',
					success:function(result){
						if(result=="OK"){
							var msgs = "取消订单成功";
							layer.msg(msgs, {icon: 1,time:700},function(){
								window.location.reload();
							});
						}else if(result=="fail"){
							var msgs = "取消订单失败";
							layer.msg(msgs, {icon: 2});
						}
					}
				});
		});
  }

function numberConvert(num) {

  var numConvert = Math.round(num * 1000) / 1000;
  var number = numConvert.toString();

  var index =  number.indexOf('.');
  var price = index != -1?number.substring(0,index+3):number;
  return price;
}

//商家退定金
function shopRefundDeposit(orderId, userId) {
  location.href = contextPath + "/s/shopRefund/apply/" + userId + "/" + orderId;
}
