$(function(){
    //改变加号鼠标指针
    $(".chooseSku").mouseover (function () {
      $(this).css("cursor","pointer");
    })
    //prod全选
    $(".selectAll").click(function(){
        $(".check-arr").hide();
        //如果被选中
        if($(this).attr("checked")){
          //添加class
            $(this).parent().parent().addClass("checkbox-wrapper-checked");
            $(".selectOne").each(function(){
                $(this).attr("checked",true);
                $(this).parent().parent().addClass("checkbox-wrapper-checked");
            });
        }else{
            $(".selectOne").each(function(){
                $(this).attr("checked",false);
                $(this).parent().parent().removeClass("checkbox-wrapper-checked");
            });
            $(this).parent().parent().removeClass("checkbox-wrapper-checked");
        }
        checkAddSku(true);
        $(".prodNum").text(parent.prods.length);
    });

    //sku全选
    $(".skuAll").click(function(){
        if($(this).attr("checked")){
            $(this).parent().parent().addClass("checkbox-wrapper-checked");
            $(".skuOne").each(function(){
                $(this).attr("checked",true);
                $(this).parent().parent().addClass("checkbox-wrapper-checked");
            });
        }else{
            $(".skuOne").each(function(){
                $(this).attr("checked",false);
                $(this).parent().parent().removeClass("checkbox-wrapper-checked");
            });
            $(this).parent().parent().removeClass("checkbox-wrapper-checked");
        }
    });

    loadProd();
    //商品选择提交事件
    $("#submit").click(function(){
        $("#form1").submit();
        saveProd();
    })
    //商品选择关闭按钮
    $(".close").on("click",function(){
        $(".check-arr").hide();
    })
    //sku选择提交事件
    $(".skuSubmit").on("click",function(){
        var prodid = $("#refProd").val();
        saveSku(prodid);
        if ($(".skuOne:checked").size()>0){
            $(":checkbox:checked[value='"+prodid+"']").parents(".first").find(".skuNum").text("已选规格("+$(".skuOne:checked").size()+")");
        }else {
            $(":checkbox[value='"+prodid+"']").parents(".first").find(".skuNum").text("选择规格");
        }
        $(".check-arr").hide();

    })
    //点击选择规格限时sku列表
  $(".first").on("click",".chooseSkuList",function(){
        var prodid = $(this).siblings(".prodCheck").find("input:checkbox").val();
        $.post("/s/loadSkuList",{prodId:prodid},function (data) {
            if (data!=null){
              $(".skuTr").remove();
                $(data.skuList).each(function(index,dom){
                  var cnProperties = dom.cnProperties;
                  if (dom.cnProperties==""||dom.cnProperties==null){
                    cnProperties="暂无";
                  }
                    var tr = "<tr class='skuTr'>\n" +
                        "\t\t\t\t\t\t\t\t<td class='skuCheck' width='35px'>\n" +
                        "\t\t\t\t\t\t\t\t\t<label class='checkbox-wrapper'>\n" +
                        "\t\t\t\t\t\t\t\t\t\t<span class='checkbox-item'>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t<input type='checkbox' id='checkbox' class='checkbox-input skuOne'  onclick='skuOne(this);' value="+dom.skuId+">\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t<input type='hidden' id='refProd'  value="+prodid+">\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t<span class='checkbox-inner'></span>\n" +
                        "\t\t\t\t\t\t\t\t\t\t</span>\n" +
                        "\t\t\t\t\t\t\t\t\t</label>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='70px'>"+cnProperties+"</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='70px'>"+dom.price+"</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='70px' class='skuStocks'>"+dom.stocks+"</td>\n" +
                        "</tr>"
                    $(".skuList").append(tr);
                    var array = new Array();
                    if (parent.skus.has(prodid)){
                        array = parent.skus.get(prodid)
                    }
                    if (array.indexOf($(tr).find(".skuOne").val())!=-1){
                        $(".skuList").find(".skuOne:eq("+index+")").attr("checked","checked");
                        $(".skuList").find(".skuOne:eq("+index+")").parent().parent().addClass("checkbox-wrapper-checked");
                    }
                    $(".skuAll").prop("checked",$(".skuOne:not(:checked)").size()>0?false:true);
                    if($(".skuOne:not(:checked)").size()<=0){
                        $(".skuAll").prop("checked",true);
                        $(".skuAll").parent().parent().addClass("checkbox-wrapper-checked");
                    }else{
                        $(".skuAll").prop("checked",false);
                        $(".skuAll").parent().parent().removeClass("checkbox-wrapper-checked");
                    }
                })
                $(".check-arr").css("z-index","2");
                $(".check-arr").show();
            }
        },"json")
    })

});

// sku单选
function skuOne(obj){
    // if ($(obj).parents(".skuCheck").siblings(".skuStocks").text()<=0){
    //     layer.msg("该规格库存不足",{icon:0})
    //     obj.checked = false;
    //     return;
    // }
    if(!obj.checked){
        $(".skuAll").checked = obj.checked;
        $(obj).prop("checked",false);
        $(obj).parent().parent().removeClass("checkbox-wrapper-checked");
    }else{

        $(obj).prop("checked",true);
        $(obj).parent().parent().addClass("checkbox-wrapper-checked");
    }
    var flag = true;
    var arr = $(".skuOne");
    for(var i=0;i<arr.length;i++){
        if(!arr[i].checked){
            flag=false;
            break;
        }
    }
    if(flag){
        $(".skuAll").prop("checked",true);
        $(".skuAll").parent().parent().addClass("checkbox-wrapper-checked");
    }else{
        $(".skuAll").prop("checked",false);
        $(".skuAll").parent().parent().removeClass("checkbox-wrapper-checked");
    }
}

// prod单选
function selectOne(obj){
    // if ($(obj).parents(".prodCheck").siblings(".stocks").text()<=0){
    //     layer.msg("该商品库存不足",{icon:0})
    //     obj.checked = false;
    //     return;
    // }
    $(".check-arr").hide();
    if(!obj.checked){
        $(".selectAll").checked = obj.checked;
        $(obj).prop("checked",false);
        $(obj).parent().parent().removeClass("checkbox-wrapper-checked");
    }else{
        $(obj).prop("checked",true);
        $(obj).parent().parent().addClass("checkbox-wrapper-checked");
    }
    var flag = true;
    var arr = $(".selectOne");
    for(var i=0;i<arr.length;i++){
        if(!arr[i].checked){
            flag=false;
            break;
        }
    }
    if(flag){
        $(".selectAll").prop("checked",true);
        $(".selectAll").parent().parent().addClass("checkbox-wrapper-checked");
    }else{
        $(".selectAll").prop("checked",false);
        $(".selectAll").parent().parent().removeClass("checkbox-wrapper-checked");
    }
    if (!$(obj).prop("checked")){
        parent.prods.splice(parent.prods.indexOf($(obj).val()),1);
        $(obj).parents(".prodCheck").siblings(".chooseSku").find(".addSku").hide();
        $(obj).parents(".prodCheck").siblings(".chooseSku").removeClass("chooseSkuList");
        parent.skus.delete($(obj).val());
        $(obj).parents(".prodCheck").siblings(".chooseSku").find(".skuNum").text("选择规格");
    }else{
        parent.prods.push($(obj).val());
        $(obj).parents(".prodCheck").siblings(".chooseSku").find(".addSku").show();
        $(obj).parents(".prodCheck").siblings(".chooseSku").addClass("chooseSkuList");
    }
    $(".prodNum").text(parent.prods.length);
}

// 取消
function cancel(){
    var index = parent.layer.getFrameIndex('addProd'); //先得到当前iframe层的索引
    parent.layer.close(index);
}

//提交
function submit(){
    var rs = true;
    $(".prodId:checked").each(function () {
        if (!parent.skus.has(this.value) || parent.skus.get(this.value)==null || parent.skus.get(this.value)==""){
            layer.msg("您选择的部分商品没有选择规格，请选择规格后再提交！",{icon:0})
            rs = false;
            return;
        }
    })

    if ($(".prodId:checked").size()<=0){
        layer.msg("请至少选择一个商品！",{icon:0})
        rs = false;
    }
    if (!rs){
        return;
    }
    parent.searchSku();
    layer.closeAll();
}
//分页事件
function pager(curPageNO){
    document.getElementById("curPageNO").value=curPageNO;
    document.getElementById("form1").submit();
    saveProd();
}

function saveProd() {
    $(".prodId:checked").each(function () {
        if (parent.prods.indexOf(this.value) == -1) {
            parent.prods.push(this.value)
        }
    })
}
//保存sku
function saveSku(prodid) {
    var array = new Array();
    if (parent.skus.has(prodid)){
        array = parent.skus.get(prodid);
    }
    $(".skuOne").each(function () {
        if (this.checked){
            if (array.indexOf(this.value) == -1) {
                array.push(this.value);
            }
        }else {
            if (array.indexOf(this.value)!=-1){
                array.splice(array.indexOf(this.value), 1);
            }
        }
    })
    if (!parent.skus.has(prodid)){
        parent.skus.set(prodid,array);
    }
}
//回显商品选择
function loadProd() {
    $(".prodId").each(function () {
        if (parent.prods.indexOf(this.value)!=-1){
            $(this).prop("checked",true);
            $(this).parent().parent().addClass("checkbox-wrapper-checked");
        }
    })
    $(".prodNum").text(parent.prods.length)
    $(".selectAll").prop("checked",$(".prodId:not(:checked)").size()>0?false:true);
    if($(".prodId:not(:checked)").size()<=0){
        $(".selectAll").prop("checked",true);
        $(".selectAll").parent().parent().addClass("checkbox-wrapper-checked");
    }else{
        $(".selectAll").prop("checked",false);
        $(".selectAll").parent().parent().removeClass("checkbox-wrapper-checked");
    }
    checkAddSku(false);
}
//检查是否选择了商品
function  checkAddSku(rs){
    $(".prodId").each(function (index,dom) {
        if (!$(dom).prop("checked")){
            if (rs){
                parent.prods.splice(parent.prods.indexOf($(dom).val()),1);
            }
            $(dom).parents(".prodCheck").siblings(".chooseSku").removeClass("chooseSkuList").find(".addSku").hide();
        }else{
            if (rs){
                if (parent.prods.indexOf(dom.value) == -1) {
                    parent.prods.push(dom.value)
                }
            }
            $(dom).parents(".prodCheck").siblings(".chooseSku").addClass("chooseSkuList").find(".addSku").show();
        }
    })
}

// function checkSkuStocks(){
//     var rs = true;
//     $(".prodId").each(function (index,dom) {
//         if ($(dom).parents(".skuCheck").siblings(".skuStocks").text()<=0){
//             layer.msg("您选择了库存不足的商品请重新选择!",{icon:0})
//             dom.checked = false;
//             rs = false;
//             return;
//         }
//     })
// 	return rs;
// }
//
// function checkProdStocks(){
//     var rs = true;
//     $(".skuId:checked").each(function (index,dom) {
//         if ($(dom).parents(".skuCheck").siblings(".skuStocks").text()<=0){
//             layer.msg("您选择了库存不足的商品请重新选择!",{icon:0})
//             dom.checked = false;
//             rs = false;
//             return;
//         }
//     })
//     return rs;
// }



