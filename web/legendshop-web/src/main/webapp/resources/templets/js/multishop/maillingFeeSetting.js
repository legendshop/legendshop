$(document).ready(function() {
	userCenter.changeSubTab("MailingFeeSetting"); //高亮菜单
	judgeIsMailfee();
});

function saveMailingFee(){
	var mailfeeSts = $("input[name='mailfeeSts']:checked").val();
	if(1==mailfeeSts){
		var mailfeeType = $("input[name='mailfeeType']:checked").val();
		var mailfeeCon = 0;
		if(mailfeeType==1){
			mailfeeCon = $.trim($("#mailfeeCon_1").val());
		}else{
			mailfeeCon = $.trim($("#mailfeeCon_2").val());
		}

		var reg = /(^[1-9](\d+)?(\.\d{1,2})?$)|(^0$)|(^\d\.\d{1,2}$)/; //正数金额，两位小数
		var patrn = /^([+]?)(\d+)$/; //是否为正整数
		if(mailfeeType==1 && !reg.test(mailfeeCon)){
			layer.msg('请输正数字或两位小数的整数', {icon: 0});
			return false;
		}else if(mailfeeType==2 && !patrn.test(Number(mailfeeCon))){
			layer.msg('请输入正整数', {icon: 0});
			return false;
		}
	}

	jQuery.ajax({
		url : contextPath + "/s/mailingFee/save",
		type : 'post',
		async : true, // 默认为true 异步
		dataType:"json",
		data:{"mailfeeSts":mailfeeSts,"mailfeeType":mailfeeType,"mailfeeCon":mailfeeCon},
		success : function(result) {
			if(result=="OK"){
				layer.msg("保存成功",{icon: 1,time:1000},function(){
					window.location.reload();
				});
			}else{
				layer.alert(result, {icon: 0});
			}
		}
	});

}
function judgeIsMailfee(){//判断初始化时，是否开启包邮
	var isChecked=$("#mailfeeSts_0").prop("checked");
	if(isChecked){
		$("#mailfeeType_all").hide();
		$("#mailfeeCon_rule_all").hide();
	}
}

$("#mailfeeSts_1").click(function(){
	$("#mailfeeType_all").show();
	$("#mailfeeCon_rule_all").show();
});
$("#mailfeeSts_0").click(function(){
	$("#mailfeeType_all").hide();
	$("#mailfeeCon_rule_all").hide();
});

$("#mailfeeType_1").click(function(){
	$("#mailfeeCon_rule_1").show();
	$("#mailfeeCon_rule_2").hide();
});

$("#mailfeeType_2").click(function(){
	$("#mailfeeCon_rule_2").show();
	$("#mailfeeCon_rule_1").hide();
});

//是否为正整数
function isPositiveInteger(s){
	var patrn = /^([+]?)(\d+)$/;
	return patrn.test(s)
}
