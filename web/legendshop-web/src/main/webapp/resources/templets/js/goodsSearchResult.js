function prodPager(_curPage){
	var url =contextPath +"/s/marketingProds/"+marketId+"?curPage="+_curPage+"&type="+type;
	url += '&' + $.param({name: $('#search_goods_name').val()});
	$('#div_goods_search_result').load(url);
}

function selectProd(_prodId,_skuId,_cash){
	
	//选择商品之前先判断该商品是否正在参加其他促销活动
	
	
	if(type=='2'){
		discountPrice(_prodId,_skuId,_cash);
	}else{

		$.ajax({
			//提交数据的类型 POST GET
			type:"POST",
			//提交的网址
			url:contextPath+"/s/addMarketingRuleProds/"+marketId,
			data:{"prodId":_prodId,"skuId":_skuId,"cash":_cash},
			//提交的数据
			async : false,  
			//返回数据的格式
			datatype: "json",
			//成功返回之后调用的函数            
			success:function(data){
				var result=eval(data);
				if(result=="OK"){
					layer.msg('添加成功', {icon: 1},function(index){
						$("#prodContent").load(contextPath+"/s/marketingRuleProds/"+marketId+"?curPage=1");
						return ;
					});
				}else{
					layer.alert(result, {icon: 0});
					return false;
				}
			}       
		});
	}
}

function discountPrice(_prodId,_skuId,_cash){
	$.ajax({
		//提交数据的类型 POST GET
		type:"POST",
		//提交的网址
		url:contextPath+"/s/addZkMarketingRuleProds/"+marketId,
		data:{"prodId":_prodId,"skuId":_skuId,"cash":_cash,"discountPrice":0},
		//提交的数据
		async : false,  
		//返回数据的格式
		datatype: "json",
		//成功返回之后调用的函数            
		success:function(data){
			var result=eval(data);
			if(result=="OK"){
				layer.msg('添加成功', {icon: 1},function(){
					$("#prodContent").load(contextPath+"/s/marketingZkRuleProds/"+marketId+"?curPage=1");
					return ;
				});

			}else{
				layer.msg(result, {icon: 0});
				return false;
			}
		}       
	});
};

//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined || _value.length==0){
		return true;
	}
	return false;
}