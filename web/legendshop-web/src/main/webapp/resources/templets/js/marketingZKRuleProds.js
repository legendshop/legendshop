function ruleProdPager(_curPage){
	$("#prodContent").load(contextPath+"/s/marketingRuleProds/"+marketId+"?curPage="+_curPage);
}
function deletemark(_this){
	var id=$(_this).attr("id");
	var desc="确定要移除该商品麽？";
	if(state=="1"){
		desc="该活动是上线状态,是否移除该商品！";
	}
	layer.confirm(desc, {
		icon: 3
		,btn: ['确定','取消'] //按钮
	},function () {
		$.ajax({
			//提交数据的类型 POST GET
			type:"POST",
			//提交的网址
			url:contextPath+"/s/deleteMarketingRuleProds/"+marketId+"/"+id,
			//提交的数据
			async : false,  
			//返回数据的格式
			datatype: "json",
			//成功返回之后调用的函数            
			success:function(data){
				var result=eval(data);
				if(result=="OK"){
					layer.msg('移除成功');
					$(_this).parent().parent().parent().remove();
					return ;
				}else{
					layer.msg(result);
					return false;
				}
			}       
		});
	});
}