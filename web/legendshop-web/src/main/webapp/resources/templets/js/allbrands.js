$(document).ready(function() {
	$(".l1:eq(0)").addClass("curr");
	var url = contextPath+"/queryBrands";
	sendData(url);
	
	$(".l1").bind("click", function() {
  		$(".l1").removeClass("curr");
  		var _this = $(this);
 		 _this.addClass("curr");
			var url = contextPath+"/queryBrands";
			sendData(url);
	});
});

function sendData(url){
    			$.ajax({
				"url":url, 
				type:'post', 
				async : true, //默认为true 异步   
				success:function(result){
				   $("#BrandList").html(result);
				}
				});
	}
	
function pager(curPageNO){
	var url = contextPath+"/queryBrands?curPageNO="+curPageNO;
	sendData(url);
}

function changePager(curPageNO){
	var url = contextPath+"/queryBrands?curPageNO="+curPageNO;
	sendData(url);
}