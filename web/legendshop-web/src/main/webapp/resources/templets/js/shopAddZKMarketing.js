$(document).ready(function() {
	userCenter.changeSubTab("mz_Marketing"); //高亮菜单
	//全选商品
    prodChange();
    $("input:radio[name='isAllProds']").change(function () {
        prodChange();
    })
	//移除元素
    $(".tab-shop").on("click",".removeSku",function() {
        $(this).parents("tr").empty().remove();
        if ($("#skuList").find(".sku").size() == 0) {
            $(".noSku").show();
        }
        return false;
    })

    $("#addProds").click(function () {
        layer.open({
            title : "添加商品",
            type : 2,
            id : "addProd",
            content : ['/s/addShopMarketingProds'],
            area : ['850px', '660px']
        });
    })

	laydate.render({
		elem: '#startTime',
		calendar: true,
		theme: 'grid',
		type:'datetime',
		min:'-1',
		trigger: 'click'
	});

	laydate.render({
		elem: '#endTime',
		calendar: true,
		theme: 'grid',
		type:'datetime',
		min:'-1',
		trigger: 'click'
	});
});

jQuery.validator.methods.greaterThanStartDate = function(value, element) {
	var start_date = $("#startTime").val();
	var date1 = new Date(Date.parse(start_date.replace(/-/g, "/")));
	var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
	return date1 < date2;
};


jQuery.validator.methods.isAllProds = function(value, element) {
	//var type= $("input[name='isAllProds']:checked").val();
	//if(type==1){
	if(isNaN(value)) {
		return false;
	}
	if(value < 1 || value >=10){
		return false;
	}

	var str = value.split(".");
	if(str.length==2){
		if(str[1].length>1){
			return false;
		}
	}
	//}
return true;
};


//页面输入内容验证
$("#add_form").validate({
	errorPlacement: function(error, element){
		var error_td = element.nextAll('span.error-message');
        error_td.append(error);
	},
	ignore: "",
	onfocusout: false,
	rules : {
		marketName : {
			required : true
		},
		startTime : {
			required : true,
		},
		endTime : {
			required : true,
			greaterThanStartDate : true
		},
		type:{
			required : true
		},
		discount:{
			isAllProds : true
		}
	},
	messages : {
		marketName : {
			required : '<i class="icon-exclamation-sign"></i>活动名称不能为空'
		},
		startTime : {
			required : '<i class="icon-exclamation-sign"></i>开始时间不能为空'
		},
		endTime : {
			required : '<i class="icon-exclamation-sign"></i>结束时间不能为空',
			greaterThanStartDate : '<i class="icon-exclamation-sign"></i>结束时间必须大于开始时间'
		},
		type:{
			required : '<i class="icon-exclamation-sign"></i>请选择促销类型',
		},
		discount: {
			isAllProds: '<i class="icon-exclamation-sign"></i>请输入正确的折扣格式,请输入1-9.9之间的小数,小数点后只保留一位'
		}
	},
	submitHandler:function(form){
        if($("#goods_status_0:checked").size()>0){
            if ($(".sku").size()==0){
                layer.msg("您暂未选择商品，请重新选择商品",{icon:0});
                return;
            }
        }

        var time = new Date($("#startTime").val().replace(/-/g,"/"));

        //开始时间不能小于等于当前时间
        // if(time.getTime() <= new Date().getTime()){
        //     layer.alert("活动开始时间不能小于当前时间!", {icon:0});
        //     return false;
        // }

        $("input[class='submit']").attr("disabled","disabled");//再改成disabled
        $("#add_form").ajaxForm().ajaxSubmit({
			success:function(data) {
				var result=eval("("+data+")");
				if(result.indexOf("成功") > -1 ){
					layer.alert(result, {icon: 1},function(){
						window.location.href=contextPath+"/s/shopZkMarketing";
					});

					return;
				}else{
					$("input[class='submit']").removeAttr("disabled");

					layer.alert(result, {icon: 2});
					return ;
				}
			},
			error:function(XMLHttpRequest, textStatus,errorThrown) {
				$("input[class='submit']").removeAttr("disabled");

				layer.alert('失败！请重试！', {icon: 2});
				return false;
			}
		});
	}

});

function  prodChange(){
    //初始化商品选择方式
    $("input:radio[name='isAllProds']").each(function(){
        if(this.checked){
            $(this).parent().parent().addClass("radio-wrapper-checked");
            if(this.value==1){
                $("#prods").hide();
            }else if (this.value==0) {
                $("#prods").show();
            }
        }else{
            $(this).parent().parent().removeClass("radio-wrapper-checked");
        }

    });
}

function searchSku(){
    var skuJson = mapToJson(skus);
    $.post("/s/searchSku",{"skuJson":skuJson},function (data) {
        skus.clear();
        prods.splice(0,prods.length);
        $(".tab-shop").html(data);
        layer.closeAll();
    })
}

function strMapToObj(strMap){
    let obj= Object.create(null);
    for (let[k,v] of strMap) {
        obj[k] = v;
    }
    return obj;
}
/**
 *map转换为json
 */
function mapToJson(map) {
    return JSON.stringify(this.strMapToObj(map));
}
