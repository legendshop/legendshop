function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("from1").submit();
}

$(document).ready(function(){
	$("#shopCatConfig").click(function(){//前往 一级类目配置
		window.location.href=contextPath+"/s/shopCatConfig";
	});
	
	$("#shopCatList").click(function(){//前往 一级类目列表
		window.location.href=contextPath+"/s/shopCategory";
	});	
	
	
	//状态变更
    $("a[name='statusImg']").click(function(event){
		$this = $(this);
		initStatus($this.attr("itemId"), $this.attr("itemName"),  $this.attr("status"), contextPath+"/s/updateShopCatStatus/", event.target,contextPath);
		 
    }
	);
});

//删除二级分类
function deleteCategory(nextCatId,nextCatName,shopCatId) {
	
	layer.confirm("确定要删除类目["+nextCatName+"]?",{
		 icon: 3
	     ,btn: ['确定','取消'] //按钮
	   }, function () { 
		  $.ajax({
				url:contextPath+"/s/nextCat/delete?nextCatId="+nextCatId,
				type:'post', 
				dataType : 'json', 
				async : true, //默认为true 异步   
				success:function(result){
					if(result=="0"){
						layer.alert('用户无效状态，不可进行删除操作 !', {icon: 2});
					} else if(result=="fail"){
						layer.alert('请先删除该类目下的三级商品类目!', {icon: 0});
					}else if(result=="limit"){
						layer.alert('该分类关联商品，无法删除!', {icon: 0});
					}else if(result=="OK"){
						window.location.reload();
					}else{
						layer.alert('删除类目失败', {icon: 0});
					}
				}
			}); 
	  });
}


