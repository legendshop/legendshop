$(function () {
  //初始化对象
  // window.oldSocket = null;
  var chat = {};
  var customId;//当前会话的客服ID
  var MsgType = {TEXT: 0, IMAGE: 1, PROD: 5, ORDER: 6, TIME: 7, READ: 8};//消息类型;(如：0:text、1:image、2:voice、3:vedio、4:music、5:prod 6:order 7:时间消息)
  var ChatResp = {SUCCESS: 10000, OFFLINE: 10001, CHANGE_CUSTOMER_SERVICE: 10003};//发送响应状态,SUCCESS成功，OFFLINE离线
  var EMOJI_MAP = {
    "U1F401": '<img key="' + 'U1F401" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_1.png">',
    "U1F402": '<img key="' + 'U1F402" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_2.png">',
    "U1F403": '<img key="' + 'U1F403" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_3.png">',
    "U1F404": '<img key="' + 'U1F404" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_4.png">',
    "U1F405": '<img key="' + 'U1F405" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_5.png">',
    "U1F406": '<img key="' + 'U1F406" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_6.png">',
    "U1F407": '<img key="' + 'U1F407" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_7.png">',
    "U1F408": '<img key="' + 'U1F408" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_8.png">',
    "U1F409": '<img key="' + 'U1F409" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_9.png">',
    "U1F410": '<img key="' + 'U1F410" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_10.png">',
    "U1F411": '<img key="' + 'U1F411" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_11.png">',
    "U1F412": '<img key="' + 'U1F412" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_12.png">',
    "U1F413": '<img key="' + 'U1F413" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_13.png">',
    "U1F414": '<img key="' + 'U1F414" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_14.png">',
    "U1F415": '<img key="' + 'U1F415" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_15.png">',
    "U1F416": '<img key="' + 'U1F416" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_16.png">',
    "U1F417": '<img key="' + 'U1F417" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_17.png">',
    "U1F418": '<img key="' + 'U1F418" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_18.png">',
    "U1F419": '<img key="' + 'U1F419" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_19.png">',
    "U1F420": '<img key="' + 'U1F420" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_20.png">',
    "U1F421": '<img key="' + 'U1F421" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_21.png">',
    "U1F422": '<img key="' + 'U1F422" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_22.png">',
    "U1F423": '<img key="' + 'U1F423" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_23.png">',
    "U1F424": '<img key="' + 'U1F424" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_24.png">',
    "U1F425": '<img key="' + 'U1F425" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_25.png">',
    "U1F426": '<img key="' + 'U1F426" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_26.png">',
    "U1F427": '<img key="' + 'U1F427" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_27.png">',
    "U1F428": '<img key="' + 'U1F428" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_28.png">',
    "U1F429": '<img key="' + 'U1F429" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_29.png">',
    "U1F430": '<img key="' + 'U1F430" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_30.png">',
    "U1F431": '<img key="' + 'U1F431" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_31.png">',
    "U1F432": '<img key="' + 'U1F432" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_32.png">',
    "U1F433": '<img key="' + 'U1F433" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_33.png">',
    "U1F434": '<img key="' + 'U1F434" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_34.png">',
    "U1F435": '<img key="' + 'U1F435" ' + 'src="' + contextPath + '/resources/templets/images/faces/ee_35.png">'
  };

  //处理事件函数
  chat.toggle = function (ele, cleName, fn) {
    $(document).on(ele, cleName, function (e) {
      fn(e)
    })
  };

  //初始化webSocket
  var initWebSocket = function () {
    //判断当前浏览器是否支持WebSocket
    if ('WebSocket' in window) {
      websocket = new WebSocket(IM_BIND_ADDRESS);
    } else {
      layerMsg("当前浏览器 Not support websocket");
    }

    /*
     * websocket.readyState
   * readyState表示连接有四种状态：
   * CONNECTING (0)：表示还没建立连接
   * OPEN (1)： 已经建立连接，可以进行通讯
   * CLOSING (2)：通过关闭握手，正在关闭连接
   * CLOSED (3)：连接已经关闭或无法打开
   */

    //连接成功建立的回调方法
    websocket.onopen = function (event) {
      heartCheck.reset().start();//心跳检测重置
      console.log("WebSocket连接成功,状态：" + websocket.readyState + "  成功时间：" + new Date());
      console.log("consultId: " + consultId + "  consultType : " + consultType + "  date : " + new Date() + "  shopId : " + shopId + "  sign : " + sign + "  userId : " + userId + "  userName : " + userName);
      debugger;
      chat.login(5, consultId, consultType, new Date(), shopId, sign, userId, userName);//5代表login，一旦链接马上进行登录操作
    };

    //连接发生错误的回调方法
    websocket.onerror = function (event) {
      console.log("WebSocket连接发生错误,状态：" + websocket.readyState);
      reconnect();
      layerMsg("WebSocket连接发生错误");
    };

    //接收到消息的回调方法
    websocket.onmessage = function (event) {
      heartCheck.reset().start(); //拿到任何消息都说明当前连接是正常的
      var _object = $.parseJSON(event.data);

      // 处理消息的回调及其显示

      //登录失败响应
      if (_object.command == "COMMAND_UNKNOW") {
        layerMsg(_object.data);//无法登录,密钥信息有误 只会显示出问题, 用处不大, 先去掉 Newway
        setTimeout(function () {
          window.history.go(-1);
        }, 1000);
        return;
      }

      //登录成功响应
      if (_object.command == "COMMAND_LOGIN_RESP") {
        // console.log("COMMAND_LOGIN_RESP==>登录成功");
        // console.log(event.data);
        // // debugger;
        debugger;
        if (window.oldSocket != null) {
          return;
        }
        var _groups = _object.data.user.imCustoms;//联系人
        //console.log(_groups);
        var _shopList = "";
        for (var i = 0, len = _groups.length; i < len; i++) {
          // // debugger;
          var shop_name = _groups[i].name;//店铺名字_客服名字 +"_"+_groups[i].name
          var shop_flag = '<em class="shop-txt">商家</em>';
          if (_groups[i].shopId == 0) {//平台客服
            shop_name = "平台客服";
            _groups[i].name = shop_name;//店铺名字_客服名字 +"_"+_groups[i].name
            shop_flag = '<em class="shop-txt" style="background: #ff9b00;">平台</em>';
          }
          _groups[i].shopName = shop_name;
          debugger
          var isCurrent = '<a href="javascript:void(0)" class="list-item" data-customId="' + _groups[i].groupId + '" data-shopId="' + _groups[i].shopId + '">';
          if (i == 0) {
            if (_groups[i].shopId == 0) {
              $(".shop-sign").text("平台");
            }
            var currentShopName = $(".currentShopName");
            currentShopName.empty();
            currentShopName.html(shop_name);

            isCurrent = '<a href="javascript:void(0)" class="list-item current" data-customId="' + _groups[i].groupId + '" data-shopId="' + _groups[i].shopId + '">';
            debugger;
            customId = _object.data.user.customId; //当前会话的客服ID
            // shopId = _object.data.user.shopId;
            if (_object.data.user.imCustoms[0]) {//该商家有客服
              onlineStatus = _object.data.user.imCustoms[0].online; //当前客服的状态
              if (onlineStatus == 0) { //当前客服状态为下线
                $(".shop-on").removeClass("on");
              }
            }

            chat.getChatRecord();//当前客服==》获取聊天记录
          }
          //默认头像，须和前台保持一致
          var _pic = '<img src="' + contextPath + '/resources/templets/images/im/head-default.jpg" class="left-img">';
          if (!isBlank(_groups[i].shopPic)) {
            _pic = '<img src="' + imagesPrefix + _groups[i].shopPic + '">';
          }

          var _num = "";
          if (i != 0 && _groups[i].unReadMsg > 0) {//初始化未读消息数，第一条不显示
            _num = '<i class="num">' + _groups[i].unReadMsg + '</i>';
          }

          // debugger;
          _shopList = _shopList + isCurrent +
            '<span class="s-img">' + _pic + shop_flag +
            '</span>' +
            '<span class="s-info">' +
            '<span class="s-name">' + shop_name + '</span>' +
            '<span class="s-tip clear">' +
            '<span class="tip-txt"></span>' +
            '<span class="tip-time"></span>' +
            '</span>' +
            '</span>' + _num +
            '</a>';
        }
        $(".shop-list").append(_shopList);
        chat.init();//初始化==》正在咨询
      }

      //接收到文本信息
      if (_object.command == "COMMAND_CHAT_REQ") {
        //console.log(_object);
        var _received = _object.data;
        if (isBlank(_received) || _received.length == 0) {
          return;
        }
        if (isBlank(_received.content)) {
          return;
        }
        debugger;
        $(".shop-list a.list-item[data-shopId='" + _received.shopId + "']").attr("data-customId", _received.from);//当前会话的客服ID
        if (shopId == _received.shopId) {//当前聊天客服ID==发送信息的客服ID
          // 更新当前的customer, 当收到消息的时候, form是客服ID
          // $(".shop-list a.list-item[data-shopId='" + _received.shopId + "']").attr("data-customId", _received.from);//当前会话的客服ID
          customId = _received.from;
          chat.receiveText(_received.msgType, _received.content);
        } else {//未读消息
          var num = $(".shop-list a.list-item[data-shopId='" + _received.shopId + "']").find("i.num");
          if (num.length == 0) {
            $(".shop-list a.list-item[data-shopId='" + _received.shopId + "']").append('<i class="num">1</i>');
          } else {
            var number = parseInt(num.text()) + 1;
            num.text(number);
          }
          if (_received.msgType == 0) {//文本消息才给出提示
            var sendTime = fmtDate(_received.createTime);
            $(".shop-list a.list-item[data-shopId='" + _received.shopId + "']").find(".s-tip .tip-txt").html(parseEmoji(_received.content, EMOJI_MAP));
            $(".shop-list a.list-item[data-shopId='" + _received.shopId + "']").find(".s-tip .tip-time").text(sendTime);

            //放到最前面
            var $clonedCopy = $(".shop-list a.list-item[data-shopId='" + _received.shopId + "']").clone();
            $(".shop-list a.list-item[data-shopId='" + _received.shopId + "']").remove();
            $(".shop-list").find("a.list-item:eq(1)").before($clonedCopy);
          }
        }

      }

      //发送信息反馈，用户不在线
      if (_object.command == "COMMAND_CHAT_RESP") {

        if (_object.code == ChatResp.OFFLINE) {
          //layerMsg("客服已经下线,请稍候再发");
          //window.location.reload();//重新画页面
          //layerMsg("发送失败,"+_object.msg);
          debugger;
          $(".shop-on").removeClass("on");
          var tmp = $(".chat-list").find("div.chat-txt:last").find("i.dia-arrow");
          tmp.remove("span");
          tmp.after('<span class="un-read">未读</span>');
          return;
        } else if (_object.code == ChatResp.CHANGE_CUSTOMER_SERVICE) {

          debugger;
          $(".shop-list a.list-item[data-shopId='" + shopId + "']").attr("data-customId", _object.data);//当前会话的客服ID
          // $(".current").click();
          // layerMsg("客服" + customId + "为您服务!");
          // var tmp = $(".chat-list").find("div.chat-txt:last").find("i.dia-arrow")
          //
          // //.after('<span class="un-read">未读</span>');
          //
          // tmp.remove("span");
          // tmp.after('<span class="un-read">未读</span>');
          //
          // setTimeout(function () {
          //   window.location.reload();
          // }, 3000);
          // return;
        }

        $(".shop-on").addClass("on");
      }

    }

    //连接关闭的回调方法
    websocket.onclose = function () {
      console.log("WebSocket连接关闭,状态：" + websocket.readyState);
      console.log("连接关闭时间：" + new Date());
      //initWebSocket();
      //window.location.reload(true);
      reconnect();
    }


    //监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
    window.onbeforeunload = function () {
      console.log("WebSocket连接关闭,状态：" + websocket.readyState);
      websocket.close();
      //连接关闭置空会话咨询内容JSON==>consult_content
      chat.setConsultTextEmpty();
    }
  }

  //初始化webSocket
  initWebSocket();

  //init==>登录成功,初始化咨询数据并设进缓存
  chat.init = function () {
    var url = contextPath + "/p/im/getImConsult?subNum=" + subNum + "&consultId=" + consultId + "&skuId=" + skuId + "&consultType=" + consultType;
    $.ajax({
      url: url,
      data: {"refundSn": refundSn},
      type: "post",
      dataType: "html",
      success: function (result) {
        $(".rightContext").html(result);
        //页面进来的时候应当存好data,正在咨询放在缓存中data
        if (isBlank($(".right-sidebar").data("imConsult"))) {
          $(".right-sidebar").data("imConsult", $(".rightContext").html());
          var e = $(".rightContext").find(".oth-list");
          $(e).find("div.list-box").css("height", "360px");
          $(".right-sidebar").data("imConsult_othList", '<div class="my-asking">' + $(e)[0].outerHTML + '</div>');
        }

        /**
         * 切换成浏览商家的浏览历史
         */
        if (shopId == 0) {
          $(".browse-history").show();
        } else {
          $(".browse-history").hide();
          $(".browse-history-" + shopId).show();
        }

      }
    });
  }


  //init==>登录成功,初始化咨询数据并设进缓存
  chat.initImConsult = function (shopId) {
    var url = contextPath + "/p/im/getImConsultByShopId?shopId=" + shopId;
    $.ajax({
      url: url,
      type: "post",
      dataType: "html",
      success: function (result) {
        $(".rightContext").html(result);
        //页面进来的时候应当存好data,正在咨询放在缓存中data
        if (isBlank($(".right-sidebar").data("imConsult"))) {
          $(".right-sidebar").data("imConsult", $(".rightContext").html());
          var e = $(".rightContext").find(".oth-list");
          $(e).find("div.list-box").css("height", "360px");
          $(".right-sidebar").data("imConsult_othList", '<div class="my-asking">' + $(e)[0].outerHTML + '</div>');
        }

        /**
         * 切换成浏览商家的浏览历史
         */
        if (shopId == 0) {
          $(".browse-history").show();
        } else {
          $(".browse-history").hide();
          $(".browse-history-" + shopId).show();
        }

      }
    });
  }


  // if(consultId == '0' ){
  // 	consultId = subNum;
  // }
  //
  //登录
  chat.login = function (cmd, consultId, consultType, createTime, shopId, sign, userId, userName) {
    debugger;
    var options = {
      cmd: cmd,
      consultId: consultId,
      consultType: consultType,
      createTime: createTime,
      shopId: shopId,
      sign: sign,
      userId: userId,
      userName: userName,
      consultSource: consultSource,
      consultContentId: skuId,
      customId: dialogueCustomId
    };
    websocket.send(JSON.stringify(options));
  }

  //滑动==》将信息滑动到底部
  chat.scrollTop = function () {
    var scrollHeight = $('.chat-list').prop("scrollHeight");
    $('.chat-list').animate({scrollTop: scrollHeight}, 600);
  }

  //发送消息
  chat.send = function (context, msgType) {
    var createTime = new Date().getTime();
    // debugger;
    var msg = {
      cmd: 7,
      chatType: 2,// 聊天类型;(如1公聊、2私聊)
      msgType: msgType,// 消息类型;(如：0:text、1:image、2:voice、3:vedio、4:music、5:prod 6:order)
      content: context,// 消息内容;
      createTime: createTime,
      from: userId,// 来自channel id;
      to: customId, // 目标channel id;
      customChat: false, //是否客服发送
      shopId: shopId,
      customId: customId
    };

    // 已读未读
    if (msgType == MsgType.READ && websocket.readyState == WebSocket.OPEN) { //每次发送前应判断连接状态==》OPEN (1)： 已经建立连接，可以进行通讯
      websocket.send(JSON.stringify(msg));
      return;
    }

    var lastTime = $('.chat-list').find(".chat-txt").last().attr("date");
    if (createTime - lastTime >= 1 * 60 * 1000) { //300000时间戳,以五分钟一个跨度显示时间
      console.log("超过五分钟......");
      msg.msgType = MsgType.TIME;
      msg.content = createTime;
      if (websocket.readyState == WebSocket.OPEN) { //每次发送前应判断连接状态==》OPEN (1)： 已经建立连接，可以进行通讯
        websocket.send(JSON.stringify(msg));
      }
      $(".chat-list").append('<div class="chat-time" data="' + createTime + '"><p class="time">' + new Date(createTime).toLocaleString() + '</p></div>');
      msg.msgType = msgType;
      msg.content = context;
    }

    console.log("发送消息--------- msgType = " + msgType);
    console.log(JSON.stringify(msg));
    if (websocket.readyState == WebSocket.OPEN) { //每次发送前应判断连接状态==》OPEN (1)： 已经建立连接，可以进行通讯
      websocket.send(JSON.stringify(msg));
      return;
    }
    debugger;
    console.log("连接中断...暂不予处理...");
    if (msgType != MsgType.READ) {
      // $(".chat-list").children("div:last-child").find(".dia").append("<span class=\"dia-err\"></span>");
      // debugger;
      // $(".chat-list").children("div:last-child").find(".dia-arrow").after('<span class="dia-err"></span>')
      // layerMsg("连接中断,请求稍后重试!")

      setTimeout(function () {
        // $(".chat-list").children("div:last-child").find(".dia-arrow").after('<span class="dia-err"></span>')
        layerMsg("连接中断,请刷新后重试!")
      }, 500);


    }


  }

  //发送消息==》回显页面信息
  chat.setMessageInnerHTML = function (context, rs) {
    var _name = $("span.u-name").text();//昵称
    $("#context").text("");//清空输入框信息
    debugger
    var _sendText = getSendText(_name, context, rs ? MsgStatus.SUCCESS : MsgStatus.FAIL, new Date().getTime(), EMOJI_MAP);
    $(".chat-list").append(_sendText);
    chat.scrollTop();
  }

  //接收消息==》回显文本信息
  chat.receiveText = function (msgType, valuestr) {
    var _currentShopName = $(".currentShopName").text();
    var _receiveText = "";
    var createTime = new Date().getTime();
    switch (msgType) {
      case MsgType.TEXT: //文本消息
        _receiveText = getReceiveText(_currentShopName, valuestr, createTime, EMOJI_MAP);
        break;
      case MsgType.IMAGE: //图片消息
        _receiveText = getReceivePicText(_currentShopName, valuestr, createTime);
        break;
      case MsgType.PROD://商品消息
        var _prodObj = $.parseJSON(valuestr);
        _receiveText = getReceiveProdText(_currentShopName, _prodObj, createTime);
        break;
      case MsgType.ORDER://订单消息
        var _orderObj = $.parseJSON(valuestr);
        _receiveText = getReceiveOrderText(_currentShopName, _orderObj, createTime);
        break;
      case MsgType.TIME://时间消息
        _receiveText = getSendTimeText(valuestr);
        break;
      case MsgType.READ:// 对方已读
        // 更新已读消息 但是不显示
        debugger;
        // var arr = JSON.parse(valuestr);
        // $.each(arr, function (e, i, a) {
        //   debugger;
        //   $("#_chat_id_" + i).remove();
        // });
        // $(".dia-err").remove();
        // if (shopId == )
        $(".un-read").text("已读")
        // $(".un-read").remove();
        break;
    }
    $(".chat-list").append(_receiveText);
    chat.scrollTop();
  }

  //获取聊天记录==》参数==》fromUserId:发送用户id;userId:接收用户id;
  chat.getChatRecord = function () {
    offset = 0; //初始化分页偏移量
    count = 10; //初始化聊天记录展示记录条数
    $.ajax({
      url: contextPath + "/p/im/user/message",
      data: {"fromUserId": userId, "shopId": shopId, "offset": offset, "count": count},
      type: "post",
      dataType: "json",
      success: function (result) {
        // console.log("getChatRecord success, customId = " + customId);
        var _html = chat.getChatRecordHtml(result);
        debugger;
        $(".chat-list").append(_html);
        if ($(".chat-list").prop("scrollHeight") > $(".chat-list").prop("clientHeight")) {  //垂直滚动条
          $(".chat-list").find("div:eq(0)").before('<div class="chat-more"><a href="javascript:void(0)" class="more">点击加载更多</a></div>');
        }
        $('.chat-list').scrollTop($('.chat-list').prop("scrollHeight"), 200);//必须在判断滚动条存在之后
      }
    });
  }

  //获取聊天记录==》查询更多聊天信息
  chat.toggle("click", ".more", function (e) {// 如：0-5,6-11,12-17
    var temp = count - offset;
    offset = count;
    count = count + temp;
    // alert(count);
    debugger;
    $.ajax({
      url: contextPath + "/p/im/user/message",
      data: {"fromUserId": userId, "userId": customId, "offset": offset, "count": count, "shopId":shopId},
      type: "post",
      dataType: "json",
      success: function (result) {
        if (isBlank(result)) {
          $('.chat-list').find(".chat-more").html('<span class="txt">没有更多了</span>');
          return;
        }
        debugger;
        var _html = chat.getChatRecordHtml(result);
        $(".chat-list").find("div.chat-txt:eq(0)").before(_html);

        if (result.length < (temp)) {//返回条数不足
          $('.chat-list').find(".chat-more").html('<span class="txt">没有更多了</span>');
        }

      }
    });
  })

  //获取聊天记录HTML
  chat.getChatRecordHtml = function (result) {
    // debugger;
    console.log(result);
    var _html = "";
    var _name = $("span.u-name").text();//昵称
    var _currentShopName = $(".currentShopName").text();//客服名称
    var unReadIds = [];
    for (var i = 0, len = result.length; i < len; i++) {
      var _content = result[len - i - 1].content;//消息内容
      var _msgType = result[len - i - 1].msgType;//消息类型
      var status = result[len - i - 1].status;//消息状态
      var createTime = result[len - i - 1].createTime;
      var id = result[len - i - 1].id;
      if (result[len - i - 1].chatSource == "0") {//发送人,右侧
        switch (_msgType) {
          case MsgType.TEXT: //文本消息
            _html = _html + getSendText(_name, _content, status, createTime, EMOJI_MAP, id);
            break;
          case MsgType.IMAGE: //图片消息
            _html = _html + getSendPicText(_name, _content, status, createTime, id);
            break;
          case MsgType.PROD://商品消息
            var _prodObj = $.parseJSON(_content);
            _html = _html + getSendProdText(_name, _prodObj, status, createTime, id);
            break;
          case MsgType.ORDER://订单消息
            var _orderObj = $.parseJSON(_content);
            _html = _html + getSendOrderText(_name, _orderObj, status, createTime, id);
            break;
          case MsgType.TIME://时间消息
            _html = _html + getSendTimeText(_content);
            break;
        }
      } else {//接收人,左侧

        if (status == MsgStatus.FAIL) {
          // 发送已读消息
          // 收集起来 然后批量发送
          unReadIds.push(result[len - i - 1].id);
        }

        switch (_msgType) {
          case MsgType.TEXT: //文本消息
            _html = _html + getReceiveText(_currentShopName, _content, createTime, EMOJI_MAP);
            break;
          case MsgType.IMAGE: //图片消息
            _html = _html + getReceivePicText(_currentShopName, _content, createTime);
            break;
          case MsgType.PROD://商品消息
            var _prodObj = $.parseJSON(_content);
            _html = _html + getReceiveProdText(_currentShopName, _prodObj, createTime);
            break;
          case MsgType.ORDER://订单消息
            var _orderObj = $.parseJSON(_content);
            _html = _html + getReceiveOrderText(_currentShopName, _orderObj, createTime);
            break;
          case MsgType.TIME://时间消息
            _html = _html + getSendTimeText(_content);
            break;
        }
      }
    }
    if (unReadIds.length > 0) {
      chat.send(unReadIds, MsgType.READ);
    }
    return _html;
  }

  //连接关闭置空会话咨询的JSON
  chat.setConsultTextEmpty = function () {
    var firstCustomId = $(".shop-list a.list-item:eq(0)").attr("data-customId");
    $.ajax({
      url: contextPath + "/p/im/setConsultTextEmpty",
      data: {"shopId": initShopId, "customId": firstCustomId},
      type: "post",
      dataType: "json",
      success: function (result) {
      }
    });
  }

  //发送==》发送文本信息
  chat.toggle("click", "#send", function (e) {
    debugger;
    var rs = true;
    //不再检查客服是否在线, 发送过去chatReqHandler系统会自动判断
    // if($("#consultCount").val()==null||$("#consultCount").val()<=0||$("#consultCount").val()==""){
    // 	layerMsg("该商家暂无客服");
    // 	rs=false;
    // }
    var context = $.trim($("#context").html());
    if (isBlank(context)) {
      $("#context").text("");
      return;
    }

    var flag = true;//是否要发送图片
    var str = $("#context").html();
    $("#context").find("img").each(function (index, element) {
      var src = $(element)[0].src;
      var key = $(element).attr("key"); //表情图片会有key

      if (isBlank(key)) {
        var path = src.substring(imagesPrefix.length); //去掉前缀
        var _nickName = $("span.u-name").text();//昵称
        var _text = getSendPicText(_nickName, path, MsgStatus.SUCCESS, new Date().getTime());
        $(".chat-list").append(_text);
        chat.scrollTop();
        chat.send(path, MsgType.IMAGE);//消息类型;(如：0:text、1:image、2:voice、3:vedio、4:music、5:prod 6:order)
        $("#context").text("");//清空输入框信息
        flag = false;
      } else {//将html替换表情图片
        str = str.replace($(element)[0].outerHTML, key);
      }
    });

    if (!flag) {
      return false;
    }
    if (rs) {
      chat.send(str, MsgType.TEXT);
    }
    chat.setMessageInnerHTML(str, rs);//将消息显示在网页上
  })

  //选择图片==>回显至聊天记录
  chat.toggle("change", "#sendImgFile", function (e) {
    var file = e.target.files[0];
    if (isBlank(sendImgFile)) {
      return;
    }
    var formData = new FormData();
    formData.append('sendImgFile', file);
    $.ajax({
      url: contextPath + "/p/im/upload",
      data: formData,
      type: "post",
      dataType: "json",
      contentType: false,
      processData: false,
      mimeType: "multipart/form-data",
      success: function (result) {
        console.log("result => ", result)
        if (result.status == "OK") {
          var _nickName = $("span.u-name").text();//昵称
          var _text = getSendPicText(_nickName, result.path, MsgStatus.SUCCESS, new Date().getTime());
          $(".chat-list").append(_text);
          chat.scrollTop();
          chat.send(result.path, MsgType.IMAGE);//消息类型;(如：0:text、1:image、2:voice、3:vedio、4:music、5:prod 6:order)
        } else {
          layer.msg(result.message, {icon: 2});
        }
      }
    });
  })

  /*//隐藏==》聊完记得给客服MM一个评价哟
  setTimeout(function(){
    $("#to_comment_tip").fadeOut("slow");
  },2000);

  //关闭==》聊完记得给客服MM一个评价哟
  chat.toggle("click","#to_comment_tip .close",function(e){
    $("#to_comment_tip").fadeOut("slow");
  })

  //右上叉==》关闭评价客服
  chat.toggle("click","#to_comment .cloose",function(e){
    $("#to_comment").fadeOut("slow");
  })

  //切换/关闭==》评价客服
  chat.toggle("click",".service-box",function(e){
     var _this = e.srcElement || e.target;
     var _openComment = $(_this).parents("#degreeButton");//打开服务评价
     var _toComment = $(_this).parents("#to_comment");//服务评价

     if(_openComment.length>0 || _this.id=="degreeButton"){//点击打开服务
       $("#to_comment").fadeToggle("slow");
       return false;
     }
     if(_toComment.length <=0){//点击服务评价内内容
       if(_this.id=="to_comment"){
         return false;
       }
       $("#to_comment").fadeOut("slow");
       return false;
     }
  })

  //五星客服评价==》选择星星
  chat.toggle("click",".sta-inf01,.sta-inf02,.sta-inf03,.sta-inf04,.sta-inf05",function(e){
    var _this = e.srcElement || e.target;
    $(".inf span").removeClass("add_inf_span_red");
    $(_this).addClass("add_inf_span_red");
    $("#to_comment .sub-btn").css("background","#E66464");
  })

  //评价==》提交
  chat.toggle("click",".submitCom",function(e){
    var _currentShopName = $(".currentShopName").text();
    var _text = '<div class="chat-tip clear">'+
            '<div class="chat-mes notice">'+
              '<i class="tip-i"></i>'+
              '<p class="tip-con">您对<strong>'+_currentShopName+'</strong>评价成功！</p>'+
            '</div>'+
          '</div>';
    $("#to_comment").fadeOut("slow");
    $(".chat-list").append(_text);
    chat.scrollTop();
  })*/

  //hover==》？怎么发截图
  /*chat.toggle("mouseover",".rep-rule",function(e){
    $(".pop-rule").removeClass("hide");
  })
  chat.toggle("mouseout",".rep-rule",function(e){
    $(".pop-rule").addClass("hide");
  })*/

  //表情==》切换
  chat.toggle("click", ".rep-face .too-i,.edit-box", function (e) {
    $(".emoji").toggleClass("hide");
  })

  //表情==》点击文本框隐藏
  chat.toggle("click", ".edit-box", function (e) {
    $(".emoji").addClass("hide");
  })

  //表情==》单击选择表情
  chat.toggle("click", ".emoji li", function (e) {
    var _this = e.srcElement || e.target;
    var _html = $(_this).prop('outerHTML');
    $("#context").append(_html);
    $(".emoji").addClass("hide");
  })

  //切换==》正在咨询/我的订单/店铺信息/常见问题
  chat.toggle("click", ".sid-tab .tab-item", function (e) {
    var _this = e.srcElement || e.target;
    var _type = $(_this).attr("data-name");
    var e = $(".rightContext");
    $(".sid-tab .tab-item").removeClass("current");
    $(_this).parents(".tab-item").addClass("current");

    switch (_type) {
      case "imConsult": //正在咨询
        var _data = "";
        if (initShopId == shopId) {
          if (!isBlank($(".right-sidebar").data("imConsult"))) {
            _data = $(".right-sidebar").data("imConsult");
          }
        } else {
          if (!isBlank($(".right-sidebar").data("imConsult_othList"))) {
            _data = $(".right-sidebar").data("imConsult_othList");
          }
        }
        e.html(_data);
        break;
      case "imOrder": //我的订单
        var url = contextPath + "/p/im/order";
        asyncload(url, shopId, e);
        break;
      case "imShopDetail": //店铺信息
        var url = contextPath + "/p/im/shopDetai";
        asyncload(url, shopId, e);
        break;
      case "imProblem": //常见问题
        var url = contextPath + "/p/im/problem";
        asyncload(url, shopId, e);
        break;
    }
  })

  //发送商品==>追加内容到聊天记录
  chat.toggle("click", "#sendProduct", function (e) {
    var _this = e.srcElement || e.target;
    var _img = $(_this).parents(".now-list").find("img").attr("src");
    var _name = $(_this).parents(".now-list").find(".name").text();
    var _price = $(_this).parents(".now-list").find(".price").attr("data");
    var _prodId = $(_this).attr("data-prodId");
    var _nickName = $("span.u-name").text();//昵称

    var _prodObj = {prodId: _prodId, prodName: _name, pic: _img, price: _price};//传递的内容对象
    if (isBlank(_prodId)) {//证明咨询的退款订单，咨询平台
      _prodObj.refundSn = $(_this).attr("data-refundSn");
      _prodObj.refundId = $(_this).attr("data-refundId");
      _prodObj.applyType = $(_this).attr("data-applyType");
    }
    chat.send(JSON.stringify(_prodObj), MsgType.PROD);//消息类型;(如：0:text、1:image、2:voice、3:vedio、4:music、5:prod 6:order)

    var _prodText = getSendProdText(_nickName, _prodObj, MsgStatus.SUCCESS, new Date().getTime());
    $(".chat-list").append(_prodText);
    chat.scrollTop();
  })

  //发送浏览商品==>追加内容到聊天记录
  chat.toggle("click", ".sendVisitedProd", function (e) {
    var _this = e.srcElement || e.target;
    var _img = $(_this).parents(".list-item").find("img").attr("src");
    var _name = $(_this).parents(".list-item").find(".name").text();
    var _price = $(_this).parents(".list-item").find(".price").attr("data");
    var _prodId = $(_this).attr("data-prodId");
    var _nickName = $("span.u-name").text();//昵称

    var _prodObj = {prodId: _prodId, prodName: _name, pic: _img, price: _price};//传递的内容对象
    chat.send(JSON.stringify(_prodObj), MsgType.PROD);//消息类型;(如：0:text、1:image、2:voice、3:vedio、4:music、5:prod 6:order)

    var _prodText = getSendProdText(_nickName, _prodObj, MsgStatus.SUCCESS, new Date().getTime());
    $(".chat-list").append(_prodText);
    chat.scrollTop();
  })

  //发送订单==>追加内容到聊天记录
  chat.toggle("click", ".sendOrder", function (e) {
    var _this = e.srcElement || e.target;
    var _subNumber = $(_this).attr("data-number");
    var _price = $(_this).attr("data-price");
    var _date = $(_this).attr("data-date");
    var _img = $(_this).parents(".ord-item").find(".ord-info img:eq(0)").attr("src");
    var _prodId = $(_this).parents(".ord-item").find(".ord-info a:eq(0)").attr("data-prodId");
    var _nickName = $("span.u-name").text();//昵称
    var _name = $(_this).attr("data-name");
    var _orderObj = {
      prodId: _prodId,
      pic: _img,
      subNumber: _subNumber,
      price: _price,
      orderDate: _date,
      prodName: _name
    };//传递的内容对象
    chat.send(JSON.stringify(_orderObj), MsgType.ORDER);//消息类型;(如：0:text、1:image、2:voice、3:vedio、4:music、5:prod 6:order)

    var _orderText = getSendOrderText(_nickName, _orderObj, MsgStatus.SUCCESS, new Date().getTime());
    $(".chat-list").append(_orderText);
    chat.scrollTop();
  })

  //切换聊天对话，回显聊天记录
  chat.toggle("click", ".shop-list a.list-item", function (e) {
    var _this = e.srcElement || e.target;
    var _item = $(_this).parents("a.list-item");
    if (_item.length > 0) {
      _this = _item;
    }
    // if ($(_this).hasClass("current")) {
    //   return false;
    // }

    $(".shop-list a.list-item").removeClass("current");

    $(_this).addClass("current");
    debugger;
    customId = $(_this).attr("data-customId");//当前会话的客服ID
    shopId = $(_this).attr("data-shopId");//当前会话商家ID
    $(".currentShopName").html($(_this).find(".s-info .s-name").text());
    if (shopId == 0) {
      $(".shop-sign").text("平台");
    } else {
      $(".shop-sign").text("商家");
    }

    debugger;
    $(".shop-list a.list-item[data-customId='" + customId + "']").find(".s-tip .tip-txt").text("");
    $(".shop-list a.list-item[data-customId='" + customId + "']").find(".s-tip .tip-time").text("");
    $(".shop-list a.list-item[data-shopId='" + shopId + "']").find(".s-tip .tip-txt").text("");
    $(".shop-list a.list-item[data-shopId='" + shopId + "']").find(".s-tip .tip-time").text("");
    var num = $(".shop-list a.list-item[data-shopId='" + shopId + "']").find("i.num");
    if (num.length > 0) {
      num.remove();
    }

    //处理右侧
    $(".sid-tab .tab-item").removeClass("current");
    $(".sid-tab .tab-item:eq(0)").addClass("current");
    if (initShopId == shopId) {
      if (!isBlank($(".right-sidebar").data("imConsult"))) {
        $(".rightContext").html($(".right-sidebar").data("imConsult"));
      }


    } else {
      if (!isBlank($(".right-sidebar").data("imConsult_othList"))) {
        $(".rightContext").html($(".right-sidebar").data("imConsult_othList"));
      }
    }
    /**
     * 切换成浏览商家的浏览历史
     */
    if (shopId == 0) {
      $(".browse-history").show();
    } else {
      $(".browse-history").hide();
      $(".browse-history-" + shopId).show();
    }


    $(".chat-list").html("");
    //回显与当前客服的聊天记录
    chat.getChatRecord();
  })

  //处理键盘回车事件
  document.onkeydown = function (event) {
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if (e && e.keyCode == 13) {
      $("#send").click();
    }
  };

  //查询客服
  chat.toggle("keyup", "#seachShopName", function (e) {
    // debugger;
    //第一次存缓存
    if (isBlank($(".left-sidebar").data("shopList"))) {
      $(".left-sidebar").data("shopList", $(".shop-list").prop("outerHTML"));
    }
    var _html = $(".left-sidebar").data("shopList");
    var val = $("#seachShopName").val();
    var len = $(_html).find("a.list-item").length;
    $(".shop-list").html($(_html).html());
    for (var i = 0; i < len; i++) {
      var customId = $($(_html).find("a.list-item")[i]).attr("data-customId");
      var text = $($(_html).find("a.list-item")[i]).find(".s-name").text();
      if (text.indexOf(val) == -1) {
        $(".shop-list").find("a.list-item[data-customId='" + customId + "']").remove();
      }
    }
  })

  //心跳检测
  var heartCheck = {
    timeout: 30000,        //30秒发一次心跳
    timeoutObj: null,
    serverTimeoutObj: null,
    reset: function () {
      clearTimeout(this.timeoutObj);
      clearTimeout(this.serverTimeoutObj);
      return this;
    },
    start: function () {
      var self = this;
      this.timeoutObj = setTimeout(function () {
        //这里发送一个心跳，后端收到后，返回一个心跳消息，
        //onmessage拿到返回的心跳就说明连接正常
        var options = {
          type: 'pc-user',
          cmd: 9,
          userId: userId
        };
        try {
          websocket.send(JSON.stringify(options));
        } catch (e) {
          debugger;
        }
        console.log("心跳包 " + JSON.stringify(options) + "  " + new Date());
        self.serverTimeoutObj = setTimeout(function () {
          //如果超过一定时间还没重置，说明后端主动断开了
          //如果onclose会执行reconnect，我们执行ws.close()就行了.如果直接执行reconnect 会触发onclose导致重连两次
          websocket.close();
        }, self.timeout)
      }, this.timeout)
    }
  }

  //强制关闭浏览器  调用websocket.close（）,进行正常关闭
  window.onunload = function () {
    websocket.close();
  }

})

var MsgStatus = {SUCCESS: "success", FAIL: "fail"};//发送消息状态

//发送文本消息、聊天记录文本==》[右侧]==>new Date(createTime).toLocaleString()
function getSendText(_name, context, status, createTime, _emojiArrObj, id) {

  var offline = "";
  // 用socket判断是发送失败还是成功
  if (websocket.readyState == WebSocket.OPEN) {
    offline = '<span class="un-read" id="_chat_id_"' + id + '>未读</span>';//红色感叹号
    if (status == MsgStatus.SUCCESS) {
      offline = "";
    }
  } else {
    offline = '<span class="dia-err"></span>';//红色感叹号
  }

  context = parseEmoji(context, _emojiArrObj);
  var _text = '<div class="chat-txt clear" date="' + createTime + '">' +
    '<div class="chat-area cus">' +
    '<p class="name">' + _name + '</p>' +
    '<div class="dialog">' +
    '<i class="dia-arrow"></i>' + offline +
    '<div class="dia-con"><p class="dia-con-p">' + context + '</p></div>' +
    '</div>' +
    '</div>' +
    '</div>';
  return _text;
}

//发送咨询商品/浏览商品、聊天记录商品解析
function getSendProdText(_name, _prodObj, status, createTime, id) {
  // var offline = '<span class="un-read"></span>';//红色感叹号
  var offline = "";
  // '<span class="un-read" id="_chat_id_"' + id + '>未读</span>';//红色感叹号

  if (websocket.readyState == WebSocket.OPEN) {
    offline = '<span class="un-read" id="_chat_id_"' + id + '>未读</span>';//红色感叹号
    if (status == MsgStatus.SUCCESS) {
      offline = "";
    }
  } else {
    offline = '<span class="dia-err"></span>';//红色感叹号
  }

  if (_prodObj.pic.indexOf("logo") != -1) {//设置默认图片
    _prodObj.pic = contextPath + "/resources/templets/images/im/head-default.jpg";
  }
  var href = contextPath + '/views/' + _prodObj.prodId;
  var refundSn_Html = "";
  if (!isBlank(_prodObj.refundSn)) {
    refundSn_Html = '<span class="g-num">退款编号：' + _prodObj.refundSn + '</span>';
    href = contextPath + '/p/refundDetail/' + _prodObj.refundId;
    if (_prodObj.applyType == 2) {
      href = contextPath + '/p/returnDetail/' + _prodObj.refundId;
    }
  }
  var _text = '<div class="chat-txt clear" date="' + createTime + '">' +
    '<div class="chat-area cus">' +
    '<p class="name">' + _name + '</p>' +
    '<div class="dialog">' +
    '<i class="dia-arrow"></i>' + offline +
    '<div class="dia-con">' +
    '<a href="' + href + '" class="dia-goods clear" target="_blank">' +
    '<span class="g-img"><img src="' + _prodObj.pic + '"></span>' +
    '<span class="g-info">' +
    '<span class="g-nam">商品名称：' + _prodObj.prodName + '</span>' +
    refundSn_Html +
    '<span class="g-num">商品价格：' + _prodObj.price + '</span>' +
    '</span>' +
    '</a>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';
  return _text;
}

//发送订单、聊天记录订单解析
function getSendOrderText(_name, _orderObj, status, createTime, id) {
  // var offline = '<span class="un-read"></span>';//红色感叹号
  var offline = "";
  //= '<span class="un-read" id="_chat_id_"' + id + '>未读</span>';//红色感叹号

  if (websocket.readyState == WebSocket.OPEN) {
    offline = '<span class="un-read" id="_chat_id_"' + id + '>未读</span>';//红色感叹号
    if (status == MsgStatus.SUCCESS) {
      offline = "";
    }
  } else {
    offline = '<span class="dia-err"></span>';//红色感叹号
  }

  var _text = '<div class="chat-txt clear" date="' + createTime + '">' +
    '<div class="chat-area cus">' +
    '<p class="name">' + _name + '</p>' +
    '<div class="dialog">' +
    '<i class="dia-arrow"></i>' + offline +
    '<div class="dia-con">' +
    '<a href="' + contextPath + '/p/orderDetail/' + _orderObj.subNumber + '" class="dia-goods clear" target="_blank">' +
    '<span class="g-img"><img src="' + _orderObj.pic + '" ></span>' +
    '<span class="g-info">' +
    '<span class="g-num">订单编号：' + _orderObj.subNumber + '</span>' +
    '<span class="g-nam">订单金额：' + _orderObj.price + '</span>' +
    '<span class="g-nam">下单时间：' + _orderObj.orderDate + '</span>' +
    '</span>' +
    '</a>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';
  return _text;
}

//发送图片、聊天记录图片解析
function getSendPicText(_nickName, path, status, createTime, id) {
  // var offline = '<span class="un-read"></span>';//红色感叹号
  var offline = ""; // '<span class="un-read" id="_chat_id_"' + id + '>未读</span>';//红色感叹号

  if (websocket.readyState == WebSocket.OPEN) {
    offline = '<span class="un-read" id="_chat_id_"' + id + '>未读</span>';//红色感叹号
    if (status == MsgStatus.SUCCESS) {
      offline = "";
    }
  } else {
    offline = '<span class="dia-err"></span>';//红色感叹号
  }

  var _text = '<div class="chat-txt clear" date="' + createTime + '">' +
    '<div class="chat-area cus">' +
    '<p class="name">' + _nickName + '</p>' +
    '<div class="dialog">' +
    '<i class="dia-arrow"></i>' + offline +
    '<div class="dia-con">' +
    '<a href="javascript:void(0)" class="dia-goods clear">' +
    '<span class="g-img" style="width:150px;margin-right:0px;height: inherit;"><img src="' + imagesPrefix + path + '"></span>' +
    '<span class="g-info" style="padding-left:0px"></span>' +
    '</a>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';
  return _text;
}

//发送时间消息==》[左侧]
function getSendTimeText(createTime) {
  var _text = '<div class="chat-time" date="' + createTime + '"><p class="time">' + new Date(+createTime).toLocaleString() + '</p></div>';
  return _text;
}

//接收文本消息、聊天记录文本==》[左侧]
function getReceiveText(_currentShopName, valuestr, createTime, _emojiArrObj) {
  valuestr = parseEmoji(valuestr, _emojiArrObj);
  var _text = '<div class="chat-txt clear" date="' + createTime + '">' +
    '<div class="chat-area sho">' +
    '<p class="name">' + _currentShopName + '</p>' +
    '<div class="dialog">' +
    '<i class="dia-arrow"></i>' +
    '<div class="dia-con">' +
    '<p class="dia-con-p">' + valuestr + '</p>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';
  return _text;
}

//接收图片、聊天记录图片==》[左侧]
function getReceivePicText(_currentShopName, path, createTime) {
  var _text = '<div class="chat-txt clear" date="' + createTime + '">' +
    '<div class="chat-area sho">' +
    '<p class="name">' + _currentShopName + '</p>' +
    '<div class="dialog">' +
    '<i class="dia-arrow"></i>' +
    '<div class="dia-con">' +
    '<a href="javascript:void(0)" class="dia-goods clear">' +
    '<span class="g-img" style="width:150px;margin-right:0px;height: inherit;">' +
    '<img src="' + imagesPrefix + path + '">' +
    '</span>' +
    '<span class="g-info" style="padding-left:0px"></span>' +
    '</a>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';
  return _text;
}

//接收商品、聊天记录商品==》[左侧]
function getReceiveProdText(_currentShopName, _prodObj, createTime) {
  var _text = '<div class="chat-txt clear" date="' + createTime + '">' +
    '<div class="chat-area sho">' +
    '<p class="name">' + _currentShopName + '</p>' +
    '<div class="dialog">' +
    '<i class="dia-arrow"></i>' +
    '<div class="dia-con">' +
    '<a href="' + contextPath + '/views/' + _prodObj.prodId + '" class="dia-goods clear" target="_blank">' +
    '<span class="g-img"><img src="' + _prodObj.pic + '"></span>' +
    '<span class="g-info">' +
    '<span class="g-nam">商品名称：' + _prodObj.prodName + '</span>' +
    '<span class="g-num">商品价格：' + _prodObj.price + '</span>' +
    '</span>' +
    '</a>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';
  return _text;
}

//接收订单、聊天记录订单==》[左侧]
function getReceiveOrderText(_currentShopName, _orderObj, createTime) {
  var _text = '<div class="chat-txt clear" date="' + createTime + '">' +
    '<div class="chat-area sho">' +
    '<p class="name">' + _currentShopName + '</p>' +
    '<div class="dialog">' +
    '<i class="dia-arrow"></i>' +
    '<div class="dia-con">' +
    '<a href="' + contextPath + '/views/' + _orderObj.prodId + '" class="dia-goods clear" target="_blank">' +
    '<span class="g-img"><img src="' + _orderObj.pic + '"></span>' +
    '<span class="g-info">' +
    '<span class="g-num">订单编号：' + _orderObj.subNumber + '</span>' +
    '<span class="g-nam">订单金额：' + _orderObj.price + '</span>' +
    '<span class="g-nam">下单时间：' + _orderObj.orderDate + '</span>' +
    '</span>' +
    '</a>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';
  return _text;
}

//右侧栏==》异步替换数据
function asyncload(url, shopId, classObject) {
  $.ajax({
    url: url,
    data: {"shopId": shopId},
    type: "post",
    dataType: "html",
    success: function (result) {
      classObject.html(result);
    }
  });
}

//方法，判断是否为空
function isBlank(_value) {
  return _value == null || _value == "" || _value == undefined;
}

//layer提示
function layerMsg(context) {
  layer.msg(context, {
    time: 2000
  });
}

//格式化金额
function formatMoney(value) {
  if (isBlank(value)) {
    return "0.00";
  }
  var value = Math.round(parseFloat(value) * 100) / 100;
  var xsd = value.toString().split(".");
  if (xsd.length == 1) {
    value = value.toString() + ".00";
    return value;
  }
  if (xsd.length > 1) {
    if (xsd[1].length < 2) {
      value = value.toString() + "0";
    }
    return value;
  }
}

//时间戳==》格式化
function fmtDate(time) {
  var date = new Date(time);
  var HH = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
  var ss = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
  var mm = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
  return HH + ss + mm;
}

var web_socket_count = 0;

function reconnect() {
  setTimeout(function () {     //没连接上会一直重连，设置延迟避免请求过多
    // debugger;
    try {
      if (websocket.readyState == WebSocket.OPEN) {
        return;
      }
      // if (web_socket_count > 10) {
      //   return;
      // }
      web_socket_count++;
      var _message = websocket.onmessage;
      // websocket = new WebSocket(IM_BIND_ADDRESS);
      window.oldSocket = websocket;
      websocket = new WebSocket(IM_BIND_ADDRESS);
      websocket.onmessage = _message;
      websocket.onclose = reconnect;
      websocket.onerror = reconnect;
    } catch (e) {
      websocket.onclose = reconnect;
      websocket.onerror = reconnect;
      websocket.onmessage = _message;
    }
  }, 2000);
}


//解析表情
function parseEmoji(valuestr, _emojiArrObj) {
  for (var emoji in _emojiArrObj) {
    valuestr = valuestr.replaceAll(emoji, _emojiArrObj[emoji]);
  }
  return valuestr;
}

//replaceAll 全局替换
String.prototype.replaceAll = function (FindText, RepText) {
  var regExp = new RegExp(FindText, "g");
  return this.replace(regExp, RepText);
}
