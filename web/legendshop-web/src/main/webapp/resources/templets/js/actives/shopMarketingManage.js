jQuery(document).ready(function() {
	$("#prodContent").load(contextPath + "/s/marketingRuleProds/" + marketId + "?curPage=1&&state="+state);

			userCenter.changeSubTab("zj_Marketing"); // 高亮菜单

			// 现实商品搜索
			$('#btn_show_goods_select').on('click', function() {
				$('#div_goods_select').show();
			});

			// 隐藏商品搜索
			$('#btn_hide_goods_select').on('click', function() {
				$('#div_goods_select').hide();
			});

			$('#btn_search_goods').on('click', function() {
						var url = contextPath + "/s/marketingProds/" + marketId + "?curPage=1";
						url += '&' + $.param({
							name : $('#search_goods_name').val()
						});
						$('#div_goods_search_result').load(url);
			});

			$('#div_goods_search_result').on('click', 'a.demo', function() {
				$('#div_goods_search_result').load($(this).attr('href'));
				return false;
			});
});

function ruleProdPager(_curPage){
    $("#prodContent").load(contextPath+"/s/marketingRuleProds/"+marketId+"?curPage="+_curPage);
}

function deletemark(_this){
   var id=$(_this).attr("id");
   var desc="确定要移除该商品麽？";
   var prodNum=$("#ruleProds>tr").length;
   if(state=="1"&&prodNum<=1){
     desc="该活动是上线状态,移除该商品后,该活动会自动下线！";
   }
   
   layer.confirm(desc,{
		 icon: 3
  	     ,btn: ['确定','取消'] //按钮
  	   }, function () {
		$.ajax({
      //提交数据的类型 POST GET
      type:"POST",
      //提交的网址
      url:contextPath+"/s/deleteMarketingRuleProds/"+marketId+"/"+id,
      //提交的数据
      async : false,  
      //返回数据的格式
      datatype: "json",
      //成功返回之后调用的函数            
      success:function(data){
      	 var result=eval(data);
      	 if(result=="OK"){
      		 layer.msg('移除成功！', {icon: 1});
      	     $(_this).parent().parent().parent().remove();
      		 return ;
      	 }else{
      		 layer.msg(result, {icon: 0});
      		 return false;
      	 }
      }       
  });
	});
   
    
}

