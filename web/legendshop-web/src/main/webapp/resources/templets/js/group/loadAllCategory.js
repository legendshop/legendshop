function selectCategory(){
	var zTree = $.fn.zTree.getZTreeObj("catTrees");
	var chkNodes = zTree.getCheckedNodes(true);
	if(chkNodes.length<1) {
		layer.msg("请选择类目",{icon:0});
		return;
	}
	var catId = chkNodes[0].id;
	var catName = chkNodes[0].name;

	var level= chkNodes[0].level;
	var first_cid=catId;
	var two_cid;
	var third_cid;
	if(level==1){  //2级
		var parent = chkNodes[0].getParentNode();
		first_cid=parent.id;
		two_cid=catId;
	}else if(level==2){ //3级
		var parent = chkNodes[0].getParentNode();
		var firstParent = parent.getParentNode();
		first_cid=firstParent.id;
		two_cid=parent.id;
		third_cid=catId;
	}
	if(type=="I" || type=="G"){
		window.parent.loadCategoryCallBack(first_cid,two_cid,third_cid,catName);
	}
	else{
		window.parent.loadCategoryCallBack(catId,catName);
	}
	var index = window.parent.layer.getFrameIndex('prodCat'); //先得到当前iframe层的索引
	window.parent.layer.close(index); //再执行关闭 
}

function clearCategory(){
	window.parent.document.getElementById('categoryName').value='';
	var index = parent.layer.getFrameIndex('prodCat'); //先得到当前iframe层的索引
	parent.layer.close(index); //再执行关闭 
}


$(document).ready(function(){
	//zTree设置
	var catTrees=JSON.parse(_catTrees);
	var setting = {
			check:{
				enable: true,
				chkboxType: {'Y':'ps','N':'ps'},
				chkStyle: 'radio',
				radioType: 'all'
			}, 
			data:{
				simpleData:{ enable: true }
			}, callback:{
				beforeCheck: beforeCheck,
				onCheck: onCheck
			} };
	$.fn.zTree.init($("#catTrees"), setting,catTrees);
});

function beforeCheck(treeId, treeNode, clickFlag) {
	if(all!=undefined && all!=null && all!=""){
		return true;
	}
	if(treeNode.isParent){
		layer.alert("请选择最底层分类",{icon:0});
	}
	return !treeNode.isParent;//当是父节点 返回false 不让选取
}

function onCheck(){
}