var reg = /^[a-zA-Z0-9\s]*$/;

function deliverGoods(){
	var deliv=$("#ecc_id").find("option:selected").val();
	if(deliv==undefined || deliv==null || deliv==""){
		layer.msg('请去设置配送方式', {icon: 0});
		return ;
	}

	var dvyFlowId=$.trim($("#dvyFlowId").val());
	if(dvyFlowId==undefined || dvyFlowId==null || dvyFlowId==""){
		layer.msg('请输入物流单号', {icon: 0});
		return ;
	}
	if(!reg.test(dvyFlowId)){
		layer.msg('物流单号格式有误', {icon: 0});
		return ;
	}
	var state_info="";
	var subNumber=$.trim($("#subNumber").val());
	if(subNumber==undefined || subNumber==null || subNumber==""){
		return;
	}

	$.post(contextPath+"/s/order/changeDeliverGoods" , {"subNumber": subNumber, "deliv": deliv,"dvyFlowId":dvyFlowId,"info":state_info},
			function(retData) {
		if(retData == 'OK' ){
			window.parent.location.reload();
			layer.closeAll('iframe');
		}else{
			layer.msg('保存发货信息失败，订单号 ' + subNumber, {icon: 2});
		}
	},'json');

}

function fahuo(){
	var deliv=$("#ecc_id").find("option:selected").val();

	if(deliv==undefined || deliv==null || deliv==""){
		layer.msg('请去设置配送方式', {icon: 0});
		return ;
	}

	var dvyFlowId=$.trim($("#dvyFlowId").val());
	if(dvyFlowId==undefined || dvyFlowId==null || dvyFlowId==""){
		layer.msg('请输入物流单号', {icon: 0});
		return ;
	}
	if(!reg.test(dvyFlowId)){
		layer.msg('物流单号格式有误', {icon: 0});
		return ;
	}
	var state_info="";
	var subNumber=$.trim($("#subNumber").val());
	if(subNumber==undefined || subNumber==null || subNumber==""){
		return;
	}
	layer.confirm("请确认发货单是否正确？",{
		icon: 3
		,btn: ['确定','取消'] //按钮
	}, function () {
		$.post(contextPath+"/s/order/fahuo" , {"subNumber": subNumber, "deliv": deliv,"dvyFlowId":dvyFlowId,"info":state_info},
				function(retData) {
			if(retData == 'OK' ){
				layer.msg("发货成功！",{icon:1,time:500},function(){
					layer.closeAll('iframe');
					window.parent.location.reload();
				});

			}else if(retData == 'fail'){
				layer.msg('发货失败，订单号 ' + subNumber, {icon: 2});
			}else{
				layer.msg(retData,{icon: 2}) ;
			}
		},'json');
	});
}