$(function(){
	userCenter.changeSubTab("adddeposit");
	
	$('#recharge_form').validate({
        errorPlacement: function(error, element){
            var error_td = element.parent('dd').children('span');
            error_td.append(error);
        },
        rules : {
        	pdrAmount      : {
	        	required  : true,
	            number    : true,
	            max		  : 99999,
	            min       : 1,
	            digits    : true
            }
            ,
            captcha : {
                required : true,
                minlength: 4
            } 
        },
        messages : {
        	pdrAmount		: {
            	required  :'请添加充值金额',
            	number    :'输入正确格式,充值金额输入值必须是不小于1且不大于99999的正整数',
            	max		  :'充值金额输入值必须是不小于1且不大于99999的正整数',
                min    	  :'充值金额输入值必须是不小于1且不大于99999的正整数',
                digits 	  :'不能为小数'
            }
            ,
            captcha : {
                required : '请正确输入图形验证码',
                minlength: '请正确输入图形验证码',
            } 
        }
    });
	
	//提交
	$(".submit").on("click", function(){
		
		if(!$("#recharge_form").valid()) {
			return;
		}
		var _pdrAmount = $("#pdrAmount").val();
		var _captcha = $("#captcha").val();
		$.ajax({
	         url: contextPath + "/p/predeposit/validateData",
	         type: "POST",
	         data: {
	        	 "pdrAmount": _pdrAmount,
	        	 "captcha" : _captcha
	         },
	         dataType:"json",
	         success: function (data) {
	             if (data.status =="OK") { //订单状态为1表示支付成功
	            	 $('#recharge_form').submit();
	             }else{
	            	 layer.msg(data.msg,{icon:0,time:1500});
	            	 setTimeout(function(){
	            		 changecodeimage();
	            	 },1500);
	             }
	         },
	         error: function () {
	         }
		});
	});
})

 function changecodeimage(){
    var obj = document.getElementById("codeimage") ;
    obj.src = contextPath + "/validCoderRandom?d=" + (new Date()).valueOf();
}