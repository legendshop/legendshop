function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("order_search").submit();
}

function orderCancel(sn){
	layer.confirm("是否将订单：'"+sn+"'取消?", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		$.ajax({
			url: path + "/p/integral/orderCancel/" + sn,
			type: "post",
			async : false, //默认为true 异步
			dataType : "json",
			success: function(data){
				if(data=="OK"){
					location.reload(true);
				}else{
					layer.alert("对不起，您操作的数据有误",{icon:2});
				}
			}
		});
	});
}


function orderDel(sn){
	layer.confirm("是否将订单：'"+ sn +"删除,请慎重考虑,删除后不能恢复该订单'?", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		jQuery.ajax({
			url:path+"/p/integral/orderDel/"+ sn,
			type: "post",
			async : false, //默认为true 异步
			dataType : "json",
			success:function(data){
				if(data=="OK"){
					window.location.reload(true);
				}else{
					layer.msg("网络延迟，请稍后再试",{icon:2});
				}
			}
		});

	});
}
/** 确认收货 **/
function confirmReceipt(sn){
	layer.confirm('如果你尚未收到货品，请不要“确认收货”。', {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		jQuery.ajax({
			url:path+"/p/integralOrderReceive/"+sn,
			type:'POST',
			async : false, //默认为true 异步
			dataType:'json',
			success:function(result){
				if(result=="OK"){
				    window.location.reload(true);
				}else if(result=="fail"){
					layer.msg("网络延迟，请稍后再试",{icon:2});
				}
		   }
		});

	});
}
