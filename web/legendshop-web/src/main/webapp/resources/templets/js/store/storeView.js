jQuery(document).ready(function($) {
	// call ajax
	$.get(contextPath + "/enableStore", {"poductId":currProdId,"skuId":currProdSku},function(retData) {
		if(retData){
			var html="<dt>门店服务：</dt><dd><i class='icon-chain'></i><a href='javascript:void(0)'>门店自提</a>· 选择有现货的门店下单，可立即提货</dd>";
			$("dl[class='ncs-chain']").html(html);
            $("#storeInfo").show();//显示门店信息
            $("#deliveryType").show();//显示配送方式
		}else{

		}
	}, 'json');

	 var province = "";
	 var city = "";
	 var area = "";

   // 获取该商品所在的全部门店
   getStore(currProdId,province,city,area);

   $("select.combox").initSelect();

   //切换省份
   $("#provinceid").change(function(){

	   province = $("#provinceid").children('option:selected').html();
	   //搜索门店
	   getStore(currProdId,province,city,area);

   });

   //切换城市
   $("#cityid").change(function(){

	  province = $("#provinceid").children('option:selected').html();
	  city = $("#cityid").children('option:selected').html();

	  //搜索门店
	  getStore(currProdId,province,city,area);

   });

   //切换地区
   $("#areaid").change(function(){
	   var area = $(this).children('option:selected').html();
	   if(area != '' && area != null && area != undefined){
		   province = $("#provinceid").children('option:selected').html();
		   city = $("#cityid").children('option:selected').html();
		   area = $("#areaid").children('option:selected').html();

		   //搜索门店
		   getStore(currProdId,province,city,area);
	   }
   });

});

function skuEnableStore(){
	currProdSku = $("#currSkuId").val();
	$.get(contextPath + "/enableStore", {"poductId":currProdId,"skuId":currProdSku},function(retData) {
		if(retData){
			var html="<dt>门店服务：</dt><dd><i class='icon-chain'></i><a href='javascript:void(0)'>门店自提</a>· 选择有现货的门店下单，可立即提货</dd>";
			$("dl[class='ncs-chain']").html(html);
			//$("#storeInfo").show();//显示门店信息
			//$("#deliveryType").show();//显示配送方式
		}else{
			$("dl[class='ncs-chain']").html("");
			//$("#storeInfo").hide();
			//$("#deliveryType").hide();//显示配送方式

			//默认选择快递
			// var deliveryType = $('input[name="deliveryType"]:checked').val();
			// if (deliveryType == 'PICKUP') {// 门店自提 value="EXPRESS"
      //
			// 	$('input[value="PICKUP"]').removeAttr("checked");
      //   $('input[value="PICKUP"]').parent().parent().remove("radio-wrapper-checked");
			// 	$('input[value="EXPRESS"]').attr("checked","checked");
      //   $('input[value="EXPRESS"]').parent().parent().addClass("radio-wrapper-checked");
      //
			// 	$("#J_RSPostageCont").show();
			// 	$("#selectShopStroe").hide();
			// }


		}
	}, 'json');
}

//弹出选择层
function shopStoreServer(){
	layer.open({
		title :"门店选择",
		id: "shopStoreServer",
		type: 1,
		content: $("#boxy"),//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		area: ['660px', '560px'],
    end: function () {
      try{
        var fn=eval(boxyCallback);
        if (typeof(fn)==='function'){
          fn();
        }else{
          console.log(boxyCallback+"函数不存在！");
        }
      }catch(e){

      }
    }
	});
}

//得判断是否为数字的同时，也判断其是不是正数
function isLogin() {
	if(loginUserName=="" || loginUserName==undefined || loginUserName==null){
		return true;
	}
	return false;
}

// 获取商品所在门店列表
function getStore(currProdId,province,city,area){

   var data={"poductId":currProdId,"province":province,"city":city,"area":area}
   $.ajax({
        //提交数据的类型 POST GET
        type:"GET",
        //提交的网址
        url:contextPath+"/storeProductCitys",
        //提交的数据
        async : true,
        //返回数据的格式
        datatype: "json",
        data:data,
        //成功返回之后调用的函数
        success:function(data){
        	if(data != null && data != '' && data != undefined && data != '[]'){
				var result=eval(data);
				var html="";
				$.each(result, function(i,val){
					html+=" <li><div class='handle'><a href='javascript:void(0);'  onclick='selectStore("+val.storeId+",\""+val.storeName+"\",\""+val.address+"\");' >选择&gt;</a></div>";
					html+=" <h5><i></i><a target='_blank' href='javascript:void(0);'>"+val.storeName+"</a></h5>";
					html+="  <p>"+val.address+"</p></li>";
				});
				$("ul[nctype='chain_see']").html(html);
			}else{
				$("ul[nctype='chain_see']").html("<p>所选地区没有该商户的门店，请选择其他地区或在线购买！</p>");
			}
        }
    });
}

//选择门店并回显
function selectStore(storeId,storeName,address){

	$("#selectStore").html(address+" "+storeName);
	$("input[name=selectedStoreId]").val(storeId);

	layer.closeAll();
}


