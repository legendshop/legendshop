/**
 *  Tony 
 */
jQuery(document).ready(function(){
	
});


function validateFile() {
	var _this=jQuery("input[name='bg_img']");
	var filepath = jQuery("input[name='bg_img']").val();
	var extStart = filepath.lastIndexOf(".");
	var ext = filepath.substring(extStart, filepath.length).toUpperCase();
	if (ext != ".BMP" && ext != ".PNG" && ext != ".GIF" && ext != ".JPG" && ext != ".JPEG") {
		layer.alert('图片限于bmp,png,gif,jpeg,jpg格式', {icon: 0});
		return false;
	}
	return true;
}


//设置模块
function module_set(obj) {
	var layoutId = jQuery(obj).attr("mark");
	var layoutDiv = jQuery(obj).attr("div");
	var divId = jQuery(obj).attr("divId");
	var layout=jQuery(obj).attr("layout");
	var seq=jQuery("#layoutSeq").val();
	var layoutModuleType=jQuery(obj).attr("type");
	jQuery.post(contextPath+"/s/shopDecotate/decorateModuleSave",
					{
						"layoutId" : layoutId,
						"layoutDiv" : layoutDiv,
						"divId" : divId,
						"layout":layout,
						"seq":seq,
						"layoutModuleType":layoutModuleType
					},
					function(data) {
						//jQuery("#module_edit").remove();
						if (!isBlank(layoutDiv)) {
							var html = '<a class="f_set" href="javascript:void(0);" '
									+ layoutId
									+ '&div='
									+ layoutDiv
									+ '" dialog_top="200" dialog_title="模块设置" dialog_width="450" dialog_height="100" dialog_id="module_edit"><i></i>设置</a>';
							
							//parent.$("div[option=" + layoutId + "][div=" + layoutDiv + "]").find("a:first").before(html);
							parent.$("div[mark=" + layoutId + "][div=" + layoutDiv+ "][id='content']").html("");
							parent.$("div[mark=" + layoutId + "][div=" + layoutDiv+ "][id='content']").html(data);
						
							var index = parent.layer.getFrameIndex('decorateModule'); //先得到当前iframe层的索引
							parent.layer.close(index); //再执行关闭   

						} else {
							var html = '<a class="f_set" href="javascript:void(0);" '
									+ layoutId
									+ '"><i></i>设置</a>';
							//parent.$("div[option=" + layoutId + "]").find("a:first").before(html);
							/*//var obj= jQuery("div[mark=" + layoutId + "][id='content']");
							var obj= parent.$("div[mark=" + layoutId + "][id='content']");
							alert(obj.html());*/
							 parent.$("div[mark=" + layoutId + "][id='content']").html("");
							 parent.$("div[mark=" + layoutId + "][id='content']").html(data);
							
							 var index = parent.layer.getFrameIndex('decorateModule'); //先得到当前iframe层的索引
							 parent.layer.close(index); //再执行关闭  
						}
	}, "html");
	
}


//设置模块
function banner_module_set(obj) {
	var layoutId = jQuery(obj).attr("mark");
	var layoutDiv = jQuery(obj).attr("div");
	var divId = jQuery(obj).attr("divId");
	var layout=jQuery(obj).attr("layout");
	var seq=jQuery("#layoutSeq").val();
	var layoutModuleType="loyout_module_banner";
	
	var urlPath=contextPath+"/s/shopDecotate/decorateModuleSave?layoutId="+layoutId+"&layout=" +
		""+layout+"&layoutModuleType="+layoutModuleType+"&layoutDiv="+layoutDiv+"&divId="+divId+"&seq"+seq;
	
	 parent.layer.open({
		  title :"幻灯片模块",
		  id: "decorateModuleBanner",
		  type: 2, 
		  resize: true,
		  content: [urlPath,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['700px', '380px']
	 }); 
	 var index = parent.layer.getFrameIndex('decorateModule'); //先得到当前iframe层的索引
	 parent.layer.close(index); //再执行关闭  
}

//设置模块
function prod_module_set(obj) {
	var layoutId = jQuery(obj).attr("mark");
	var layoutDiv = jQuery(obj).attr("div");
	var divId = jQuery(obj).attr("divId");
	var layout=jQuery(obj).attr("layout");
	var seq=jQuery("#layoutSeq").val();
	var layoutModuleType="loyout_module_custom_prod";
	
	var urlPath=contextPath+"/s/shopDecotate/decorateModuleSave?layoutId="+layoutId+"&layout=" +
		""+layout+"&layoutModuleType="+layoutModuleType+"&layoutDiv="+layoutDiv+"&divId="+divId+"&seq"+seq;
	
	 
	 
	 parent.layer.open({
		  title :"商品自定义模块",
		  id: "decorateModuleProd",
		  type: 2, 
		  resize: true,
		  content: [urlPath,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['700px', '700px']
		}); 
	 
	 var index = parent.layer.getFrameIndex('decorateModule'); //先得到当前iframe层的索引
	 parent.layer.close(index); //再执行关闭  
}

//设置模块
function content_module_set(obj) {
	var layoutId = jQuery(obj).attr("mark");
	var layoutDiv = jQuery(obj).attr("div");
	var divId = jQuery(obj).attr("divId");
	var layout=jQuery(obj).attr("layout");
	var seq=jQuery("#layoutSeq").val();
	var layoutModuleType="loyout_module_custom_desc";
	
	var urlPath=contextPath+"/s/shopDecotate/decorateModuleSave?layoutId="+layoutId+"&layout=" +
		""+layout+"&layoutModuleType="+layoutModuleType+"&layoutDiv="+layoutDiv+"&divId="+divId+"&seq"+seq;
	
	 
	 
	 parent.layer.open({
		  title :"自定义模块",
		  id: "decorateModuleContent",
		  type: 2, 
		  resize: true,
		  content: [urlPath,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['800px', '550px']
		}); 
	 
	 var index = parent.layer.getFrameIndex('decorateModule'); //先得到当前iframe层的索引
	 parent.layer.close(index); //再执行关闭  
 }

function hot_module_set(obj) {
	var layoutId = jQuery(obj).attr("mark");
	var layoutDiv = jQuery(obj).attr("div");
	var divId = jQuery(obj).attr("divId");
	var layout=jQuery(obj).attr("layout");
	var seq=jQuery("#layoutSeq").val();
	var layoutModuleType="loyout_module_hot";
	
	var urlPath=contextPath+"/s/shopDecotate/decorateModuleSave?layoutId="+layoutId+"&layout=" +
		""+layout+"&layoutModuleType="+layoutModuleType+"&layoutDiv="+layoutDiv+"&divId="+divId+"&seq"+seq;
	 parent.layer.open({
		  title :"热点模块",
		  id: "decorateModuleHot",
		  type: 2, 
		  resize: false,
		  content: [urlPath,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['100%', '100%']
		}); 
	 
	 var index = parent.layer.getFrameIndex('decorateModule'); //先得到当前iframe层的索引
	 parent.layer.close(index); //再执行关闭  
 }




function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}


