var originalBirthdayDay;

var petNameFlag,cidFlag,relName=0;
$(document).ready(function(){ 
	//检查身份证
		$("#id_card").val($.trim($("#id_card").val()));
		if($("#id_card").val().length != 0 && !IdCardValidate($("#id_card").val())){
			$("#ucid_msg").parent().addClass("prompt-error");
			$("#ucid_msg").parent().removeClass("prompt-06");
			$("#ucid_msg").html("<span style='color:red;'>请输入正确的身份证号码</span>");
			cidFlag=1;
		}else{
			cidFlag=0;
			$("#ucid_msg").html("");
		}
		
		userCenter.changeSubTab("personinfo");
	}); 
/**
 * 初始化用户信息
 */
function initUserInfo(){
	  laydate.render({
		   elem: '#birthDate',
		   calendar: true,
		   theme: 'grid',
		   max: '1',
		   trigger: 'click'
	  });
	
	$("select.combox").initSelect();
	
	//头像上传
	$("#uploadthickbox").bind("click",function(){
		layer.open({
			title :"头像编辑",
			id:"touxiang",
			  type: 2, 
			  content: contextPath+"/p/loadphotoverlay", //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
			  area: ['540px', '500px']
			}); 
	});
	
	//昵称 获取焦点事件
	$("#petName").focus(function() {
		$("#petName_msg").parent().removeClass("prompt-error");
		$("#petName_msg").parent().addClass("prompt-06");
		$("#petName_msg").html("2-20个字符，可由中英文、数字及符号组成");
	});

	$("#id_card").blur(function() {
		var id_card=$.trim($("#id_card").val());
		if(id_card.length!=0){
			if(!IdCardValidate($("#id_card").val())){
				$("#ucid_msg").parent().addClass("prompt-error");
				$("#ucid_msg").parent().removeClass("prompt-06");
				$("#ucid_msg").html("<span style='color:red;'>请输入正确的身份证号码</span>");
				cidFlag=1;
				return;
			}
		}
		cidFlag=0;
		$("#ucid_msg").html("");
	});

	// 校验 昵称是否正确
	$("#petName").blur(function() {
		var petName = $("#petName").val();
		if (petName == null || petName == "") {
			petNameFlag = 1;
			$("#petName_msg").html("请输入2个字符以上的昵称");
			$("#petName_msg").parent().addClass("prompt-error");
			$("#petName_msg").parent().removeClass("prompt-06");
			$("#petName_orderly").removeClass("icon-orderly");
			return;
		}
		if (originalPetName == petName) {
			petNameFlag = 0;
			$("#petName_msg").html("");
			return;
		}
		if (petName.length < 2 || petName.length > 20) {
			petNameFlag = 1;
			$("#petName_msg").parent().addClass("prompt-error");
			$("#petName_msg").parent().removeClass("prompt-06");
			$("#petName_msg").html("请输入2-20个字符，可由中英文、数字及符号组成");
			$("#petName_orderly").removeClass("icon-orderly");
		}else {
			checkPetName();
		}
	});
}

//去掉空格 并用去掉空格后的字符串替代显示
function delspace(name){
	var inputValue=$("#"+name).val();
	while(inputValue.indexOf(" ")!=-1){
		inputValue=inputValue.replace(" ","");
	}
	$("#"+name).val(inputValue);
}

//去掉左右尖括号 并用去掉空格后的字符串替代显示
function replaceBrackets(name){
	var inputValue=$(name).val();
	while(inputValue.indexOf("<")!=-1){
		inputValue=inputValue.replace("<","[");
	}
	while(inputValue.indexOf(">")!=-1){
		inputValue=inputValue.replace(">","]");
	}
	while(inputValue.indexOf("&")!=-1){
		inputValue=inputValue.replace("&"," ");
	}
	$(name).val(inputValue);
}

//去掉某个字符  （消除对后面验证正则表达式的判定影响）
function replaceChar(name,char){
	var inputValue=$("#"+name).val();
	while(inputValue.indexOf(char)!=-1){ //去掉-影响
		inputValue=inputValue.replace(char,"");
	}
	return inputValue;
}

//提交 更新用户信息
function updateUserInfo(){
	validPetName();
	if (petNameFlag != 0) {
    	scroller("petName", 500);
    	return;
	}
	if (cidFlag != 0) {
    	scroller("id_card", 500);
    	return;
	}
    var data = {
    		"id": $("#id").val(),
        	"realName": $("#real_name").val(),
        	"nickName": $("#petName").val(),
        	"qq": $("#user_qq").val(),
        	"birthDate": $("#birthDate").val(),
        	"userAdds": $("#user_adds").val(),
        	"idCard": $("#id_card").val(),
        	"provinceid": $("#provinceid").val(),
        	"cityid": $("#cityid").val(),
        	"areaid": $("#areaid").val(),
        	"sex": $('input[name="sex"]:checked').val(),
        	"marryStatus": $('input[name="marryStatus"]:checked').val(),
        	"incomeLevel": $("#incomeLevel").val(),
        	"interest": $("#interest").val()
        };
		$.ajax({
			url: contextPath + "/p/user/updateUserInfo" , 
			data: data,
			type:'post', 
			dataType : 'json', 
			async : true, //默认为true 异步   
			success:function(result){
			   if(result == -1){
				   layer.alert('生日日期有误', {icon: 0});
			   }else if(result == 0){
				   //$("#userNickName").html(data.nickName);
				   layer.msg("更新个人资料成功",{icon: 1,time:1000},function(){
					   window.location.href = contextPath + "/p/user";
				   });
			   }else{
				   layer.alert('更新个人信息失败', {icon: 2});
			   }
			}
		});
}

//去往账户信息
function toUserInfo(){
	var url = contextPath + "/p/user";
	sendData(url);
}

function sendData(url){
	window.location.href = url;
}

//刷新页面
function refreshUserinfo(){
  	$("#personinfo").click();
} 

function checkPetName() {
	var _petName = $("#petName").val();
	if(isBlank(trim(_petName))) {
		$("#petName_msg").parent().addClass("prompt-error");
		$("#petName_msg").parent().removeClass("prompt-06");
		$("#petName_msg").html("昵称不能为空");
		$("#petName").removeAttr("disabled");
		petNameFlag = 1;
		$("#petName_orderly").removeClass("icon-orderly");
		return;
	}
	
	petNameFlag = 0;
	
	/*$("#petName").attr({
		disabled : "disabled"
	});
	$("#petName_msg").html("昵称唯一性验证中，请稍等...");
	
	$.ajax({
			url: contextPath+"/isNickNameExist", 
			data: {"nickName": _petName},
			type:'post', 
			async : false, //默认为true 异步   
			error: function(jqXHR, textStatus, errorThrown) {
				$("#petName_msg").parent().addClass("prompt-error");
				$("#petName_msg").parent().removeClass("prompt-06");
				$("#petName_msg").html("网络传输异常，请稍后再试...");
				$("#petName").removeAttr("disabled");
				petNameFlag = 1;
				$("#petName_orderly").removeClass("icon-orderly");
			},
			success:function(retData){
		 		if('true' == retData){
		 			$("#petName_msg").parent().addClass("prompt-error");
					$("#petName_msg").parent().removeClass("prompt-06");
					$("#petName_msg").html("<span style='color:red;'>此昵称已被其他用户抢注，请修改</span>");
					$("#petName").removeAttr("disabled");
					petNameFlag = 1;
					$("#petName_orderly").removeClass("icon-orderly");
				  }else if('sensitivity'==retData){
					  $("#petName_msg").parent().addClass("prompt-error");
						$("#petName_msg").parent().removeClass("prompt-06");
						$("#petName_msg").html("<span style='color:red;'>此昵称含有敏感字信息，请修改</span>");
						$("#petName").removeAttr("disabled");
						petNameFlag = 1;
						$("#petName_orderly").removeClass("icon-orderly");
				  }else{
					  $("#petName_msg").parent().removeClass("prompt-error");
						$("#petName_msg").parent().addClass("prompt-06");
						$("#petName_msg").html("");
						$("#petName_orderly").addClass("icon-orderly");
						$("#petName").removeAttr("disabled");
						petNameFlag = 0;
				  }
			}
	});*/
	
}

// 判断输入框内是否为邮箱格式 name为输入框ID msgName为提示信息的ID
function checkEmailFont(name, msgName) {
	var email = $("#" + name).val();

	if (email.replace(/[^\x00-\xff]/g, "**").length <= 4
			|| email.replace(/[^\x00-\xff]/g, "**").length >= 50) {

		$("#" + msgName).parent().addClass("prompt-error");
		$("#" + msgName).parent().removeClass("prompt-06");
		$("#" + msgName).html("邮箱长度不正确");
		$("#email_orderly").removeClass("icon-orderly");
		return 1;
	}

	var reg = /^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/;
	if (reg.test(email)) {
		$("#" + msgName).html("");
		return 0;
	} else {
		$("#" + msgName).parent().addClass("prompt-error");
		$("#" + msgName).parent().removeClass("prompt-06");
		$("#" + msgName).html("邮箱格式不正确");
		$("#email_orderly").removeClass("icon-orderly");
		return 1;
	}
}

function newtype() {
    jQuery.jdThickBox({
        type: "text", 
        title: "提示",
        width: 120,
        height:70,
        source: "<div class=\"m warn\"><div class=\"mc\"><s><\/s>资料保存成功<div class=\"clr\"><\/div><\/div><div class=\"btns\"><a href=\"javascript:void(0)\" class=\"btn btn-12 ftx-03\" onclick=\"jdThickBoxclose()\"><s><\/s>关闭<\/a><\/div><\/div>",
        _autoReposi: true
        });
} 

function validPetName(){
	var petName = $("#petName").val();
	if (petName == null || petName == "") {
		petNameFlag = 1;
		$("#petName_msg").html("请输入2个字符以上的昵称");
		$("#petName_msg").parent().addClass("prompt-error");
		$("#petName_msg").parent().removeClass("prompt-06");
		$("#petName_orderly").removeClass("icon-orderly");
		return;
	}
	if (originalPetName == petName) {
		petNameFlag = 0;
		$("#petName_msg").html("");
		return;
	}
}

//方法，判断是否为空
function isBlank(_value){
	return _value==null || _value == "" || _value == undefined;
}


function scroller(id,second){
	$("html,body").animate({scrollTop:$("#"+id).offset().top},second);
}