$(function(){

    // 标识不可选的商品
    $("tr.first").each(function(){
        var prodId = $(this).attr("prodId");
        var flag = isAttendActivity(prodId);

        if( flag != "false" ){

            $(this).find("div").css("display","block");
            $(this).find("div").children().html("已参加<br />"+flag);

            $(this).find("input").addClass("unChecked");
            $(this).find("input").attr("disabled","disabled");
            $(this).find("input").next().css("background","#eee");
            $(this).find("label").removeClass("radio-wrapper");
        }
    });

    //切换选择商品
    $("input:radio[name='prodId']").change(function(){

        var prodId = $(this).val();

        //判断商品互斥（是否已参与其他活动）
        var flag = isAttendActivity(prodId);
        if( flag != "false" ){
            layer.msg("该商品已参加"+flag+"活动",{icon:0});

            this.checked = false;
            return;
        }

        $("input:radio[name='prodId']").each(function(){

            if(this.checked){
                $(this).parent().parent().addClass("radio-wrapper-checked");
            }else{
                $(this).parent().parent().removeClass("radio-wrapper-checked");
            }
        });
    });

});

// 分页搜索
function pager(curPageNO){
    document.getElementById("curPageNO").value=curPageNO;
    document.getElementById("form1").submit();
}

// 确认选择商品
function confirm(){

    var prodId = $("input:radio[name='prodId']:checked").val();

    if(isBlank(prodId)){
        layer.msg("请先选择参与活动的商品",{icon:0});
        return;
    }

    // 确认后逻辑 TODO
    $.ajax({
        url : contextPath + "/s/mergeGroupActivity/selProd",
        data:{prodId:prodId},
        async : false,
        success : function(result){
            parent.$(".check-bto").css("display","inline-block");
            parent.$(".tab-shop").html(result);
        }
    });

    var index = parent.layer.getFrameIndex('showProdlist'); //先得到当前iframe层的索引
    parent.layer.close(index);
}

// 取消
function cancel(){

    var index = parent.layer.getFrameIndex('showProdlist'); //先得到当前iframe层的索引
    parent.layer.close(index);
}

// 是否参加其他活动
function isAttendActivity(prodId){
    var flag = "false";
    $.ajax({
        url : contextPath + "/s/mergeGroupActivity/isAttendActivity/" + prodId ,
        async : false,
        dataType : "JSON",
        success : function(result){
            flag = result;
        }
    });
    return flag;
}
function isBlank(value){
    return value == undefined ||  value == null || value == "";
}