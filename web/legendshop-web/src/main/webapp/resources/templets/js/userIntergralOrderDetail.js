$(function(){
	$('#show_shipping').on('hover',function(){
		var html="";
		if(dvyFlowId=="" || dvyTypeId==""){
			html="没有物流信息";
		}
		else{
			if(!loaddely){
				$.ajax({
					url:contextPath+"/p/order/getExpress", 
					data:{"dvyFlowId":dvyFlowId,"dvyId":queryUrl},
					type:'POST', 
					async : false, //默认为true 异步   
					dataType:'json',
					success:function(result){
						if(result=="fail" || result=="" ){
							html="没有物流信息";
							loaddely=true;
						}else{
							var list = eval(result);
							for(var obj in list){ //第二层循环取list中的对象 
								html+="<li>"+list[obj].context+"<li>";
							} 
							loaddely=true;
						}
					}
				});
			}
		}

		$('#shipping_ul').html(html);
		$('#show_shipping').unbind('hover'); 
	});


});


function orderReceive(_number){
	layer.open({
		type: 1, 
		content: "<div class='eject_con'><dl><dt>订单编号：</dt><dd>"+_number+" <p class='hint'>请注意： 如果你尚未收到货品请不要点击“确认”。大部分被骗案件都是由于提前确认付款被骗的，请谨慎操作！ </p>"
		+" </dd></dl></div>", 
		area: ['430px', '290px'],
		btn: ['确定','关闭'],
		yes:function(){
			$.ajax({
				url:contextPath+"/p/integralOrderReceive/"+_number,
				type:'POST', 
				async : false, //默认为true 异步   
				dataType:'json',
				success:function(result){
					if(result=="OK"){
						var msg = "确认收货成功";
						layer.msg(msg,{icon:1,time:500},function(){
							window.location.reload();
						});
					}else if(result=="fail"){
						var msg = "确认收货失败";
						layer.msg(msg,{icon:2});
					}
				}
			});
		}
	}); 
}