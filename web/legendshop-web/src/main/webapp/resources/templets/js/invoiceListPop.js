/* 设为默认发票 */
function setDefault(invoiceId){

    layer.confirm("确定设置该发票为默认？", {
        icon: 3
        ,btn: ['确定','关闭'] //按钮
    }, function(index){
        $.ajax({
            url:contextPath+"/p/invoice/changeInvoiceCommon?id="+invoiceId,
            type:'post',
            async : true,
            dataType : 'json',
            success:function(result){

                if (result == 'OK'){
                    //刷新发票列表
                    window.location.reload();
                }else {
                    layer.msg(result,{icon:0});
                }
            }
        });
        layer.close(index); //再执行关闭
    });
}

/*发票信息 删除*/
function deleteById(invoiceId){
    layer.confirm("确定删除该发票信息？", {
        icon: 3
        ,btn: ['确定','关闭'] //按钮
    }, function(index){
        $.ajax({
            url:contextPath+"/p/invoice/delete/"+invoiceId,
            type:'post',
            async : true,
            dataType : 'json',
            success:function(result){

                if (result == 'OK'){
                    //刷新发票列表
                    window.location.reload();
                }else {
                    layer.msg("删除失败，请稍后重试",{icon:0});
                }
            }
        });
        layer.close(index); //再执行关闭
    });
}

/* 取消选择发票 */
function cancel(){
    parent.layer.closeAll();
}

/* 选择发票  curInvoice:当前选择的发票 */
var beforeInvoice = null;
function checkedInvoice(invoiceId,currInvoice){
    $(beforeInvoice).removeClass("checked");
    $(currInvoice).addClass("checked");
    beforeInvoice = currInvoice;

    $("#checkedInvoice").val(invoiceId);
}

/* 提交选择的发票 重新提交表单 */
function choiceInvoice() {

    if (invoiceType == 3) { // 选择不开发票
        var invoiceId = 0;
    }else{
        var invoiceId = $("#checkedInvoice").val();

        if (isBlank(invoiceId)){
            layer.msg("请选择发票",{icon: 0});
            return;
        }
    }
    $("#invoiceId", window.parent.document).val(invoiceId);
    layer.closeAll();

    // 重新请求核对订单页面
    parent.reloadOrder();

}



/* 空判断 */
function isBlank(_value){
    if(_value==null || _value=="" || _value==undefined){
        return true;
    }
    return false;
}