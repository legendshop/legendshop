jQuery(document).ready(function () {
    $("#userName").focus(function () {
        $("#userName").css('color', 'black');
    }).blur(function () {
        $("#userName").css('color', '#ccc');
    });

    $("#telephone").focus(function () {
        $("#telephone").css('color', 'black');
    }).blur(function () {
        $("#telephone").css('color', '#ccc');
    });


    $("#randNum").focus(function () {
        $("#randNum").css('color', 'black !important');
    }).blur(function () {
        $("#randNum").css('color', 'ccc');
    });


    $("#erifyCode").focus(function () {
        $("#erifyCode").css('color', 'black');
    }).blur(function () {
        $("#erifyCode").css('color', '#ccc');
    });

    $("#password").focus(function () {
        $("#password").css('color', 'black');
    }).blur(function () {
        $("#password").css('color', '#ccc');
    });

    $("#password2").focus(function () {
        $("#password2").css('color', 'black');
    }).blur(function () {
        $("#password2").css('color', '#ccc');
    });


    $("#telephone").bind('blur', function () {
        checkTel();
    });

    $("#userName").bind('blur', function () {
        checkUserName();
    });

    $("#password").bind('blur', function () {
        checkPwd();
    });

    $("#password2").bind('blur', function () {
        checkPwd2();
    });

    $("#erifyCode").bind('blur', function () {
    	checkErifyCode2();
    });

    $("#randNum").bind('blur', function () {
    	checkedRandNum2();
    });

    $("#protocol").bind("click", function () {
    	layer.open({
    		title :"用户注册协议",
		    type: 2,
		    id :"agreement",
		    content: ['agreementoverlay','no'],
		    area: ['950px', '480px']
    		});
    });

    $("#readme").change(function () {
        if ($(this).is(':checked')) {
            $("#regBtn").attr("disabled", false);
            $("#regBtn").val("立即注册");
            $("#regBtn").css("background-color", "#e5004f");
            $("#regBtn").attr("onclick", "javascript:submitTable();");
        } else {
            $("#regBtn").attr("disabled", true);
            $("#regBtn").css("background-color", "#d1d1d1");
            $("#regBtn").val("请阅读并同意网络服务和使用协议");
        }
    });
});


//检查电话号码
function checkTel() {
    var isPhone = /^[1][0-9]{10}$/;
    var telValue = $("#telephone").val();
    var telInput = $("#telephone");
    var tipText = telInput.next();
    if (telValue == null || telValue == '' || telValue.length <= 0 || telValue == '请输入手机号码') {
        tipText.attr("class", "con_error");
        tipText.html("请输入手机号码");
        telInput.css('color', '#ccc');
        telInput.attr("class", "text highlight2");
        return false;
    }
    if (telValue.length != 11) {
        tipText.attr("class", "con_error");
        tipText.html("手机号码必须为11位");
        telInput.css('color', '#000');
        telInput.attr("class", "text highlight2");
        return false;
    }
    if (!isPhone.test(telValue)) {
        tipText.attr("class", "con_error");
        tipText.html("手机号码无效或存在特殊字符");
        telInput.css('color', '#000');
        telInput.attr("class", "text highlight2");
        return false;
    }
    var config = true;
    $.ajax({
        url: contextPath + "/isPhoneExist",
        data: {"Phone": telValue},
        type: 'post',
        async: false, //默认为true 异步
        success: function (retData) {
            if ('true' == retData) {
                tipText.attr("class", "con_error");
                tipText.html("该手机号码已注册, 请前往登录！");
                telInput.css('color', '#000');
                telInput.attr("class", "text highlight2");
                config = false;
                return false;
            }
        }
    });
    if (!config) {
        return false;
    }
    tipText.attr("class", "con_succeed");
    tipText.html("");
    telInput.css('color', '#000');
    telInput.attr("class", "text");
    return true;
}

function checkUserName() {
	/* 更换用户名匹配正则，之前这个不能以下划线结尾 */
    /*var isUserName = /^(?![_0-9])(?!.*?_$)[a-zA-Z0-9]{4,16}$/;*/
    var isUserName = /^[a-z][\da-z_]{3,16}$/i;
    var userNameValue = $("#userName").val();
    var userNameInput = $("#userName");
    var tipText = userNameInput.next();
    if (userNameValue == null || userNameValue == '' || userNameValue.length <= 0) {
        tipText.attr("class", "con_error");
        tipText.html("请输入用户名");
        userNameInput.css('color', '#ccc');
        userNameInput.attr("class", "text highlight2");
        return false;
    }
    if (userNameValue.length < 4 || userNameValue.length > 16) {
        tipText.attr("class", "con_error");
        tipText.html("用户名长度不正确，应在4-16个字符之间");
        userNameInput.css('color', '#000');
        userNameInput.attr("class", "text highlight2");
        return false;
    }
    if (!isUserName.test(userNameValue)) {
        tipText.attr("class", "con_error");
        tipText.html("用户名需要为字母、数字和_，不能以数字和_开头");
        userNameInput.css('color', '#000');
        userNameInput.attr("class", "text highlight2");
        return false;
    }
    var config = true;
    $.ajax({
        url: contextPath + "/isUserNameExist",
        data: {"userName": userNameValue},
        type: 'post',
        async: false, //默认为true 异步
        error: function (jqXHR, textStatus, errorThrown) {
        	layer.alert("网络异常,请稍后重试！",{icon:2});
        },
        success: function (retData) {
            if ('true' == retData) {
                tipText.attr("class", "con_error");
                tipText.html("此用户名已存在，请重新输入！");
                userNameInput.css('color', '#000');
                userNameInput.attr("class", "text highlight2");
                config = false;
                return false;
            }
        }
    });
    if (!config) {
        return false;
    }
    tipText.attr("class", "con_succeed");
    tipText.html("");
    userNameInput.css('color', '#000');
    userNameInput.attr("class", "text");
    return true;
}

//检查密码
function checkPwd() {
    var pwdValue = $("#password").val();
    var pwdInput = $("#password");
    var tipText = pwdInput.next();
    console.log(pwdValue);
    //var regx = /(?!.*[\u4E00-\u9FA5\s])(?!^[a-zA-Z]+$)(?!^[\d]+$)(?!^[^a-zA-Z\d]+$)^.{6,20}$/ ;
    var regx1= /^[0-9A-z`~!@#$%^&*()_\-+=<>?:"{}|,.\/;'\\[\]·~！@#￥%……&*（）——\-+={}|《》？：“”【】、；‘’，。、]+$/g;
    //var regx= /(?!^[0-9]+$)(?!^[A-z]+$)(?!^[~\\`!@#$%\\^&*\\(\\)-_+={}|\\[\\];':\\\",\\.\\\\\/\\?]+$)(?!^[^A-z0-9]+$)^.{6,20}$/;
    var regx= /^(?![0-9]+$)(?![a-z]+$)(?![A-Z]+$)(?!([^(0-9a-zA-Z)]|[\(\)])+$)([^(0-9a-zA-Z)]|[\(\)]|[a-z]|[A-Z]|[0-9]){6,20}$/;
    if(pwdValue.indexOf(" ") > -1){
    	tipText.attr("class", "con_error");
        tipText.html("登录密码不能包含空格");
        pwdInput.attr("class", "text highlight2");
        return false;
    }
    if (pwdValue == null || pwdValue == '' || pwdValue.length <= 0 || pwdValue == '请输入6-20位密码') {
        tipText.attr("class", "con_error");
        tipText.html("登录密码不能为空");
        pwdInput.attr("class", "text highlight2");
        return false;
    }
    if (pwdValue.length < 6 || pwdValue.length > 20) {
        tipText.attr("class", "con_error");
        tipText.html("为了您的账号安全，密码长度在 6-20 个字符之间");
        pwdInput.attr("class", "text highlight2");
        return false;
    }
	if (!regx.test(pwdValue) || !regx1.test(pwdValue)) {
	    tipText.attr("class", "con_error");
	    tipText.html("密码为6-20位字母、数字和标点符号的组合");
	    pwdInput.attr("class", "text highlight2");
	    return false;
	}
    /*if (regx.test(pwdValue) || regx1.test(pwdValue)) {
        if(!regx2.test(pwdValue)){
			tipText.attr("class", "con_error");
			tipText.html("密码为6-20位字母、数字和标点符号的组合");
			pwdInput.attr("class", "text highlight2");
			return false;
		}
    }*/
    tipText.attr("class", "con_succeed");
    tipText.html("");
    pwdInput.css('color', '#000');
    pwdInput.attr("class", "text");
    return true;
}
//检查二次输入的密码
function checkPwd2() {
    var pwd2Value = jQuery("#password2").val();
    var pwdValue = jQuery("#password").val();
    var pwd2Input = jQuery("#password2");
    var tipText = pwd2Input.next();
    if (pwd2Value == null || pwd2Value == '' || pwd2Value.length <= 0 || pwd2Value == '请再次输入6-20位密码') {
        tipText.attr("class", "con_error");
        tipText.html("确认密码不能为空");
        pwd2Input.attr("class", "text highlight2");
        return false;
    }
    if (pwd2Value.length < 6) {
        tipText.attr("class", "con_error");
        tipText.html("用户密码不能少于6个字符");
        pwd2Input.attr("class", "text highlight2");
        return false;
    }
    if (pwdValue.toString() != pwd2Value.toString()) {
        tipText.attr("class", "con_error");
        tipText.html("两次输入的密码要一致");
        pwd2Input.attr("class", "text highlight2");
        return false;
    }
    tipText.attr("class", "con_succeed");
    tipText.html("");
    pwd2Input.css('color', '#000');
    pwd2Input.attr("class", "text");
    return true;
}

//检查短信验证码
function checkErifyCode() {
    var erifyValue = jQuery("#erifyCode").val();
    var erifyInput = $("#erifyCode");
    var tipText = erifyInput.parent().find("label");
    if (erifyValue == null || erifyValue == '' || erifyValue.length <= 0 || erifyValue == '请输入短信校验码') {
        tipText.attr("class", "con_error");
        tipText.html("短信校验码不能为空");
        erifyInput.attr("class", "text-1 text highlight2");
        return false;
    }
    if (erifyValue.length < 6) {
        tipText.attr("class", "con_error");
        tipText.html("短信验证码必须是6个字符");
        erifyInput.attr("class", "text-1 text highlight2");
        return false;
    }
    tipText.attr("class", "con_succeed");
    tipText.html("");
    erifyInput.css('color', '#000');
    erifyInput.attr("class", "text-1 text");
    return true;
}

//检查短信验证码
function checkErifyCode2() {
    var erifyValue = jQuery("#erifyCode").val();
    var erifyInput = $("#erifyCode");
    var tipText = erifyInput.parent().find("label");
    if (erifyValue == null || erifyValue == '' || erifyValue.length <= 0 || erifyValue == '请输入短信校验码') {
        tipText.attr("class", "con_error");
        tipText.html("短信校验码不能为空");
        erifyInput.attr("class", "text-1 text highlight2");
        return false;
    }
    if (erifyValue.length < 6) {
        tipText.attr("class", "con_error");
        tipText.html("短信验证码必须是6个字符");
        erifyInput.attr("class", "text-1 text highlight2");
        return false;
    }
    //校验短信验证码是否正确
    var telValue = $("#telephone").val();
    $.ajax({
        url: contextPath + "/verifyCodeBeforeSubmit",
        data: {"mobile": telValue, "verifyCode": erifyValue},
        type: 'post',
        async: false, //默认为true 异步
        error: function (jqXHR, textStatus, errorThrown) {
        	layer.alert("网络异常,请稍后重试！",{icon:2});
        },
        success: function (retData) {
            if ('true' == retData) {
            	tipText.attr("class", "con_succeed");
            	tipText.html("");
                erifyInput.css('color', '#000');
                erifyInput.attr("class", "text-1 text");
                return true;
            } else {
                tipText.attr("class", "con_error");
                tipText.html("短信验证码不正确！");
                jQuery("#erifyCode").attr("class", "text-1 text highlight2");
            }
        }
    });
}

function checkedRandNum() {
    var randNumValue = jQuery("#randNum").val();
    var randNumInput = $("#randNum");
    var tipText = randNumInput.parent().find("label");
    if (randNumValue == null || randNumValue == '' || randNumValue.length <= 0 || randNumValue == '请输入图片中的文字') {
        tipText.attr("class", "con_error");
        tipText.html("验证码不能为空");
        randNumInput.attr("class", "text-1 text highlight2");
        return false;
    }
    if (randNumValue.length < 4) {
        tipText.attr("class", "con_error");
        tipText.html("验证码必须是4个字符");
        randNumInput.attr("class", "text-1 text highlight2");
        return false;
    }
    tipText.attr("class", "con_succeed");
    tipText.html("");
    randNumInput.css('color', '#000');
    randNumInput.attr("class", "text-1 text");
    return true;
}

function checkedRandNum2() {
    var randNumValue = jQuery("#randNum").val();
    var randNumInput = $("#randNum");
    var tipText = randNumInput.parent().find("label");
    if (randNumValue == null || randNumValue == '' || randNumValue.length <= 0 || randNumValue == '请输入图片中的文字') {
        tipText.attr("class", "con_error");
        tipText.html("验证码不能为空");
        randNumInput.attr("class", "text-1 text highlight2");
        return false;
    }
    if (randNumValue.length < 4) {
        tipText.attr("class", "con_error");
        tipText.html("验证码必须是4个字符");
        randNumInput.attr("class", "text-1 text highlight2");
        return false;
    }
    if(!checkRandImage()){ //校验验证码是否正确
        return false;
    }
    tipText.attr("class", "con_succeed");
    tipText.html("");
    randNumInput.css('color', '#000');
    randNumInput.attr("class", "text-1 text");
    return true;
}

//校验 验证码（提交表单前）
function checkRandImageBeforeSubmit() {
    var randImage = $('randImage');
    if (!validateRandNum(contextPath)) {
        inputError(randImage);
        randImage.focus();
        return false;
    } else {
        return true;
    }
    ;
}

//校验 验证码
function checkRandImage() {
    var randImage = $('randImage');
    if (!validateRandNum(contextPath)) {
        inputError(randImage);
        randImage.focus();
        changeRandImg(contextPath);
        return false;
    } else {
        return true;
    }
    ;
}

// 校验 验证码（ 输入错误提示）
function inputError(input) {
    clearTimeout(inputError.timer);
    var num = 0;
    var fn = function () {
        inputError.timer = setTimeout(function () {
            input.className = input.className === '' ? 'login-form-error' : '';
            if (num === 5) {
                input.className === '';
            } else {
                fn(num++);
            }
            ;
        }, 150);
    };
    fn();
}

function validateRandNum(contextPath) {
    var checkResult = checkRandNum();
    if (!checkResult) {
        //验证失败，返回
        return checkResult;
    }
    var randNum = $("#randNum").val();
    if (randNum != null || randNum == '' || randNum) {
        var result = true;
        var data = {"randNum": randNum};
        jQuery.ajax({
            url: contextPath + "/validateRandImg",
            type: 'post',
            data: data,
            async: false, //默认为true 异步
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
            	layer.alert("网络异常,请稍后重试！",{icon:2});
            },
            success: function (retData) {
                result = retData;
                var tipText = $("#randNum").parent().find("label");
                if (!retData) {
                    changeRandImg(contextPath);
                    $("#randNum").attr("class", "text text-1 highlight2");
                    tipText.html("请输入正确验证码！");
                    tipText.attr("class", "con_error");
                    result = false;
                } else {
                    tipText.attr("class", "con_succeed");
                    tipText.html("");
                    $("#randNum").attr("class", "text text-1");
                    $("#randNum").css('color', '#000');
                    result = true;
                }
            }
        });
        return result;
    }
}

function checkRandNum() {
    var error;
    var inputVal = document.getElementById('randNum');
    var tipText = jQuery("#randNum").parent().find("label");

    //如果找不到对象则表示不用验证
    if (inputVal == null) {
        return true;
    }

    if (inputVal.value == null || inputVal.value == '') {
        jQuery("#randNum").attr("class", "text text-1 highlight2");
        tipText.html("请输入正确验证码！");
        tipText.attr("class", "con_error");
        return false; //验证失败
    }
    if (inputVal.value.length != 4) {
        jQuery("#randNum").attr("class", "text text-1 highlight2");
        tipText.html("请输入正确验证码！");
        tipText.attr("class", "con_error");
        return false; //验证失败
    }

    return true;
}

function changeRandImg() {
    var obj = document.getElementById("randImage");
    obj.src = contextPath + "/validCoderRandom\?d=" + new Date().getTime();
}
var wait = 60;
function time(btn) {
    $btn = $(btn);
    if (wait == 0) {
        $btn.removeAttr("disabled");
        $btn.val("获取短信校验码");
        wait = 60;
    } else {
        $btn.attr("disabled", true);
        $btn.val(wait + "秒后重新获取");
        wait--;
        setTimeout(function () {
                time($btn);
            },
            1000)
    }
}

function sendMobileCode() {
    if (checkTel() && checkRandImage()) {
        var tel = $("#telephone").val();
        var randNum = $("#randNum").val();
        var data = {"tel": tel, "randNum": randNum};
        jQuery.ajax({
            url: contextPath + "/sendSMSCodeByReg",
            type: 'post',
            data: data,
            async: false, //默认为true 异步
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
            	layer.alert("网络异常,请稍后重试！",{icon:2});
            },
            success: function (retData) {
                if (!retData == "OK") {
                   layer.msg('短信发送失败', {icon: 2});
                    $("#erifyCodeBtn").val("重新发送");
                } else {
                    layer.msg('短信发送成功',{icon: 1});
                    time($("#erifyCodeBtn"));
                }
            }
        });
    }
}
function submitTable() {
    var errorCounts = true;
    var i = 0;
    if (!checkTel()) {
        //console.debug(i++);
        errorCounts = false;
    }

    if (!checkUserName()) {
        //console.debug(i++);
        errorCounts = false;
    }

    if (!checkPwd()) {
        //console.debug(i++);
        errorCounts = false;
    }

    if (!checkPwd2()) {
        //console.debug(i++);
        errorCounts = false;
    }
    if (!checkedRandNum()) {
        //console.debug(i++);
        errorCounts = false;
    } else if (!checkRandImage()) {
        //console.debug(i++);
        errorCounts = false;
    }
    if (!checkErifyCode()) {
        //console.debug(i++);
        errorCounts = false;
    }

    if (errorCounts) {
        //console.debug(i++);

        var tipText = jQuery("#erifyCode").parent().find("label");
        var telValue = $("#telephone").val();
        var pageErifyCode = $("#erifyCode").val();
        $.ajax({
            url: contextPath + "/verifyCode",
            data: {"mobile": telValue, "verifyCode": pageErifyCode},
            type: 'post',
            async: false, //默认为true 异步
            error: function (jqXHR, textStatus, errorThrown) {
            	layer.alert("网络异常,请稍后重试！",{icon:2});
            },
            success: function (retData) {
                if ('true' == retData) {
                	//防止多次点击
                	$("#regBtn").attr("disabled",true);
                    userRegForm.submit();
                } else {
                    jQuery("#erifyCode").attr("class", "text text-1 highlight2");
                    tipText.html("短信验证码不正确！");
                    tipText.attr("class", "con_error");
                    changeRandImg();
                    jQuery("#randNum").val("");
                    jQuery("#erifyCode").val("");
                    var tiplabel = jQuery("#randNum").parent().find("label");
                    tiplabel.removeClass("con_succeed");
                }
            }
        });
    }


}
