var url = contextPath + "/p/refundList";
$(function(){
	if(applyType == 2){
		url = contextPath + "/p/returnList";
		$("#listType li[value='1']").removeClass("on");
		$("#listType li[value='2']").addClass("on");
		$("#refundOption").hide();
		$("#refundOption").attr("name", "");
		$("#returnLOption").show();
		$("#returnLOption").attr("name", "customStatus");
	}
	loadList(url,null);

	$("#listType li").click(function(){
		var value = $(this).val();
		if(value == applyType){
			return;
		}
		applyType = value;
		$("#listType li").removeClass("on");
		$(this).addClass("on");
		if(value == 1){
			$("#refundOption").show();
			$("#refundOption").attr("name", "customStatus");
			$("#returnLOption").hide();
			$("#returnLOption").attr("name", "");
			url = contextPath + "/p/refundList";
			loadList(url, null);
		}else{
			$("#refundOption").hide();
			$("#refundOption").attr("name", "");
			$("#returnLOption").show();
			$("#returnLOption").attr("name", "customStatus");
			url = contextPath + "/p/returnList";
			loadList(url, null);
		}
	});
});

//分页
function pager(curPageNO){
    $("#curPageNO").val(curPageNO);
	var params = $("#searchForm").serialize();
	if(applyType == 2){
		url = contextPath + "/p/returnList";
	}
	loadList(url, params);
}

//搜索退款/退货记录
function search(){
	if(applyType == 2){
		url = contextPath + "/p/returnList";
	}
	$("#searchForm #curPageNO").val("1");
	var param = $("#searchForm").serializeObject();
	loadList(url,param);
}

//加载退款,退货列表
function loadList(url,param){
	$.ajax({
		url : url,
		type : "GET",
		data : param,
		dataType:"html",
		success : function(result,status,xhr){
			$("#listBox").html(result);
		}
	});
	//实现无刷新改变浏览器地址框
	if ('pushState' in window.history){//检查兼容
		window.history.pushState(null,null,contextPath + "/p/refundReturnList/"+applyType);
	}
}

//删除退款/退货记录
function deleteAction(id){
	layer.confirm("您确定要删除该退款记录吗?一旦删除将不可恢复!", {
		 icon: 3
	     ,btn: ['确定','关闭'] //按钮
	   }, function(){
		   $.ajax({
				url : contextPath + "/p/refund/delete/" + id,
				type : "GET",
				dataType : "JSON",
				ansyc : true,
				success : function(result,status,xhr){
					if(result == "OK"){
						layer.alert("恭喜您,删除成功!",{icon: 1},function(index){
							search();
							layer.close(index);
						});
					}else{
						layer.alert(result,{icon: 2});
					}
				}
			});
	   });
}

//撤销申请
function candelAction(refundSn){
	 //询问框
	layer.confirm("您确定要撤销该次退款申请吗?",{icon: 3,btn: ['确定','关闭']},function(){
		$.ajax({
			url : contextPath + "/p/refund/candelAction/" + refundSn,
			type : "GET",
			dataType : "JSON",
			ansyc : true,
			success : function(result,status,xhr){
				if(result == "OK"){
					layer.alert("恭喜您,撤销成功!", {icon: 1}, function(index){
						search();
						layer.close(index);
					});
				}else{
					layer.alert(result,{icon: 2});
				}
			}
		});
	});
}



