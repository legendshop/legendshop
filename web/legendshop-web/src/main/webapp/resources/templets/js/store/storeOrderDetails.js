$(document).ready(function() {


});
/**
 * 计算价格
 */
function calculateTotal() {
    //获取实际商品价格
    var allActualCash = $.trim($("#allActualCash").html()).substring(1);
    if (allActualCash <= 0 || isEmpty(allActualCash)) { //实际价格不能为 0
        layer.alert("亲爱的用户,您的价格有误,请重新下单！", { icon: 0 });
        return;
    }

    //获取总的店铺优惠券
    var allOffPrice = 0;
    var shopCoupons = $(".shop_coupon").get();
    for (var i = 0; i < shopCoupons.length; i++) {
        var c = $(shopCoupons[i]).find("option:selected").val();
        if (isEmpty(c)) {
            c = 0;
        }
        allOffPrice += Number(c);
    }

    //获取总的促销优惠
    var allDiscount = $.trim($("#allDiscount").html()).substring(2);
    if (isEmpty(allDiscount)) {
        allDiscount = 0;
    }

    var orderAllCash = Number(allActualCash);
    orderAllCash = orderAllCash - Number(allDiscount) - Number(allOffPrice);

    $("#allActualCash").html("￥" + parseFloat(allActualCash).toFixed(2));
    $("#allCash").html("￥" + parseFloat(orderAllCash).toFixed(2));
    $("#allCashFormat").html(parseFloat(orderAllCash).toFixed(2));

}


/**
 * 提交门店订单
 */
function submitOrder() {

    // 判断订单金额
    var allCash = parseFloat($("#allCashFormat").html());
    if (allCash <= 0) {
        layer.alert('订单金额有误请重新下单', { icon: 0 });
        return;
    }
    if (allCount < 1) {
        layer.alert("没有要结算的购物清单,请重新选择", { icon: 0 });
        return;
    }

    //支付方式
    var pay_type = $("input:radio[name='pay_opened_pm_radio']:checked").val();

    if (isEmpty(pay_type)) {
        layer.alert("请选择支付方式", { icon: 0 });
        $("html,body").animate({ scrollTop: $("#pay").offset().top - 50 }, 500);
        return;
    }

    // 处理提货人名称
    var buyerName = $("input[name='buyerName']").get();

    if (isEmpty(buyerName)) {
        layer.alert("请填写提货人信息", { icon: 0 });
        return;
    }

    config = true;
    var buyerNameText = "";
    for (var j in buyerName) {
        var obj = buyerName[j];
        var val = $.trim($(obj).val());
        if (!isEmpty(val)) {
            if (!is_forbid(val)) {
                layer.msg('提货人中含有非法字符', { icon: 0 });
                config = false;
                return;
            }
            var storeId = Number($(obj).attr("storeId"));
            buyerNameText += storeId + ":" + val + ";";
        }else{
        	 layer.msg('请完整填写提货人名称', { icon: 0});
        	 config = false;
             return;
        }
    }

    if (!config) {
        return;
    }
    if (!isEmpty(buyerNameText)) {
        buyerNameText = buyerNameText.substring(0, buyerNameText.length - 1);
    }


    // 处理提货人手机号码
    var telPhone = $("input[name='telPhone']").get();

    if (isEmpty(telPhone)) {

        layer.alert("请填写提货人手机号码", { icon: 0 });
        return;
    }

    config = true;
    var telPhoneText = "";
    for (var j in telPhone) {
        var obj = telPhone[j];
        var val = $.trim($(obj).val());
        
        if (!isEmpty(val)) {

            var storeId = Number($(obj).attr("storeId"));
            telPhoneText += storeId + ":" + val + ";";
        }
        //检查手机号码
        if (!checkMobileAndPhone(val)) {
        	config = false;
            return;
		}
        
    }

    if (!config) {
        return;
    }
    if (!isEmpty(telPhoneText)) {
        telPhoneText = telPhoneText.substring(0, telPhoneText.length - 1);
    }

    // 获取买家留言
    
    var remark = $("input[name='remark']").get();
    config = true;
    var remarkText = "";
    for (var j in remark) {
        var obj = remark[j];
        var val = $.trim($(obj).val());
        if (!isEmpty(val)) {
            if (!is_forbid(val)) {
                layer.msg('买家留言中含有非法字符', { icon: 0 });
                config = false;
                return;
            }
            var storeId = Number($(obj).attr("storeId"));
            remarkText += storeId + ":" + val + ";";
        }
    }

    if (!config) {
        return;
    }
    if (!isEmpty(remarkText)) {
    	remarkText = remarkText.substring(0, remarkText.length - 1);
    }

    // 组装提交参数
    var data = {

        "payManner": pay_type,
        "buyerName": buyerNameText,
        "telPhone": telPhoneText,
        "remarkText": remarkText,
        "token": token
    };

    // loading加载效果 
    $("body").showLoading();

    $.ajax({
        url: contextPath + "/p/store/order/storeSubmitOrder",
        data: data,
        type: 'post',
        async: false, //默认为true 异步   
        dataType: 'json',
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            $("body").hideLoading();
            layer.alert("订单出现异常", { icon: 2 });
            return false;
        },
        success: function(result) {

            if (result.result == true) {
                var url = result.url;
                window.location.href = contextPath + url;
                return;
            } else if (result.result == false) {
                $("body").hideLoading();
                $("#submitOrder").css('background-color', "#e5004f");
                $("#submitOrder").removeAttr("disabled", "disabled");
                var errorCode = result.code;
                if (errorCode == "NOT_LOGIN") {
                    layer.alert("请登录您的帐号！", { icon: 0 });
                    setTimeout(function() {
                        window.location.href = contextPath + "/login";
                    }, 1);
                    return false;
                } else if (errorCode == "NOT_PRODUCTS") {
                    setTimeout(function() {
                        window.location.href = contextPath + "/shopCart/shopBuy";
                    }, 1);
                    return false;
                } else if (errorCode == "NO_ADDRESS") {
                    layer.alert("请填写您的收货人信息 ！", { icon: 0 });
                    $("html,body").animate({ scrollTop: $("#user_addr").offset().top - 50 }, 500);
                    return false;
                } else if (errorCode == "INVALID_TOKEN") {
                    layer.alert("请勿重复提交订单！", { icon: 0 });
                    return false;
                } else if (errorCode == "PARAM_ERR") {
                    layer.alert("提交订单参数有误！", { icon: 0 }, function() {
                        window.location.href = contextPath + "/shopCart/shopBuy";
                    });
                    return false;
                } else {
                    layer.alert(result.message, { icon: 2 });
                    return false;
                }
            }
        }
    });

}

function checkMobileAndPhone(value) {
    var errorFlag = false;
   
    if (isEmpty(value)) {
        layer.alert("请您填写收货人手机号码", { icon: 0 });
        return false;
    } else {
        var regu = /^\d{11}$/; //!(/(^(1)\d{10}$)/.test(mobile.val()))
        var re = new RegExp(regu);
        if (!re.test(value)) {
            layer.alert("手机号码格式不正确", { icon: 0 });
            return false;
        }
    }
    return true;
}


/**
 * 判断是否是空
 * @param value
 */
function isEmpty(value) {
    if (value == null || value == "" || value == "undefined" || value == undefined || value == "null") {
        return true;
    } else {
        value = (value + "").replace(/\s/g, '');
        if (value == "") {
            return true;
        }
        return false;
    }
}


/**
 * 检查是否含有非法字符
 * @param temp_str
 * @returns {Boolean}
 */
function is_forbid(temp_str) {
    temp_str = temp_str.replace(/(^\s*)|(\s*$)/g, "");
    temp_str = temp_str.replace('--', "@");
    temp_str = temp_str.replace('/', "@");
    temp_str = temp_str.replace('+', "@");
    temp_str = temp_str.replace('\'', "@");
    temp_str = temp_str.replace('\\', "@");
    temp_str = temp_str.replace('$', "@");
    temp_str = temp_str.replace('^', "@");
    temp_str = temp_str.replace('.', "@");
    temp_str = temp_str.replace(';', "@");
    temp_str = temp_str.replace('<', "@");
    temp_str = temp_str.replace('>', "@");
    temp_str = temp_str.replace('"', "@");
    temp_str = temp_str.replace('=', "@");
    temp_str = temp_str.replace('{', "@");
    temp_str = temp_str.replace('}', "@");
    var forbid_str = new String('@,%,~,&');
    var forbid_array = new Array();
    forbid_array = forbid_str.split(',');
    for (i = 0; i < forbid_array.length; i++) {
        if (temp_str.search(new RegExp(forbid_array[i])) != -1)
            return false;
    }
    return true;
}