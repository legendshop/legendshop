var EMOJI_MAP={
		    "U1F401":'<img src="' + contextPath + '/resources/templets/images/faces/ee_1.png">',
		    "U1F402":'<img src="' + contextPath + '/resources/templets/images/faces/ee_2.png">',
		    "U1F403":'<img src="' + contextPath + '/resources/templets/images/faces/ee_3.png">',
		    "U1F404":'<img src="' + contextPath + '/resources/templets/images/faces/ee_4.png">',
		    "U1F405":'<img src="' + contextPath + '/resources/templets/images/faces/ee_5.png">',
		    "U1F406":'<img src="' + contextPath + '/resources/templets/images/faces/ee_6.png">',
		    "U1F407":'<img src="' + contextPath + '/resources/templets/images/faces/ee_7.png">',
		    "U1F408":'<img src="' + contextPath + '/resources/templets/images/faces/ee_8.png">',
		    "U1F409":'<img src="' + contextPath + '/resources/templets/images/faces/ee_9.png">',
		    "U1F410":'<img src="' + contextPath + '/resources/templets/images/faces/ee_10.png">',
		    "U1F411":'<img src="' + contextPath + '/resources/templets/images/faces/ee_11.png">',
		    "U1F412":'<img src="' + contextPath + '/resources/templets/images/faces/ee_12.png">',
		    "U1F413":'<img src="' + contextPath + '/resources/templets/images/faces/ee_13.png">',
		    "U1F414":'<img src="' + contextPath + '/resources/templets/images/faces/ee_14.png">',
		    "U1F415":'<img src="' + contextPath + '/resources/templets/images/faces/ee_15.png">',
		    "U1F416":'<img src="' + contextPath + '/resources/templets/images/faces/ee_16.png">',
		    "U1F417":'<img src="' + contextPath + '/resources/templets/images/faces/ee_17.png">',
		    "U1F418":'<img src="' + contextPath + '/resources/templets/images/faces/ee_18.png">',
		    "U1F419":'<img src="' + contextPath + '/resources/templets/images/faces/ee_19.png">',
		    "U1F420":'<img src="' + contextPath + '/resources/templets/images/faces/ee_20.png">',
		    "U1F421":'<img src="' + contextPath + '/resources/templets/images/faces/ee_21.png">',
		    "U1F422":'<img src="' + contextPath + '/resources/templets/images/faces/ee_22.png">',
		    "U1F423":'<img src="' + contextPath + '/resources/templets/images/faces/ee_23.png">',
		    "U1F424":'<img src="' + contextPath + '/resources/templets/images/faces/ee_24.png">',
		    "U1F425":'<img src="' + contextPath + '/resources/templets/images/faces/ee_25.png">',
		    "U1F426":'<img src="' + contextPath + '/resources/templets/images/faces/ee_26.png">',
		    "U1F427":'<img src="' + contextPath + '/resources/templets/images/faces/ee_27.png">',
		    "U1F428":'<img src="' + contextPath + '/resources/templets/images/faces/ee_28.png">',
		    "U1F429":'<img src="' + contextPath + '/resources/templets/images/faces/ee_29.png">',
		    "U1F430":'<img src="' + contextPath + '/resources/templets/images/faces/ee_30.png">',
		    "U1F431":'<img src="' + contextPath + '/resources/templets/images/faces/ee_31.png">',
		    "U1F432":'<img src="' + contextPath + '/resources/templets/images/faces/ee_32.png">',
		    "U1F433":'<img src="' + contextPath + '/resources/templets/images/faces/ee_33.png">',
		    "U1F434":'<img src="' + contextPath + '/resources/templets/images/faces/ee_34.png">',
		    "U1F435":'<img src="' + contextPath + '/resources/templets/images/faces/ee_35.png">'
		};
$(function(){
	initList(EMOJI_MAP);
	$(".offline-message").on("click", ".mes-item", function(){
		 var _consultType = $(this).attr("consultType");
		 var _skuId = $(this).attr("skuId");
		 var _refundSn = $(this).attr("refundSn");
		 var _shopId = $(this).attr("shopId");
		 var _customId = $(this).attr("customId");
		 var url = contextPath + "/p/im/message/index/" + _shopId + "/?consultType=" +  _consultType + "&skuId=" + _skuId + "&refundSn=" + _refundSn + "&customId=" + _customId;
		 location.href = url;
	});
});

function systemMessages(){
	var url = contextPath + "/p/systemMessages";
	location.href = url;
}

function inbox(){
	var url= contextPath + "/p/inbox";
	location.href = url;
}

//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}

//针对客服用的
function initList(_emojiArrObj) {
	$(".item-dl .txt").each(function(){
		var text = $(this).text();
		var str = parseEmoji(text, _emojiArrObj);
		$(this).html(str);
	  });
}

//解析表情
function parseEmoji(valuestr,_emojiArrObj){
	for(var emoji in _emojiArrObj){  
		valuestr = valuestr.replaceAll(emoji,_emojiArrObj[emoji]);
	}
	return valuestr;
}
//replaceAll 全局替换
String.prototype.replaceAll = function (FindText, RepText) {
    var regExp = new RegExp(FindText,"g");
    return this.replace(regExp, RepText);
}
