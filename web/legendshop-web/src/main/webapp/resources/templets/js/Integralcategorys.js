$(document).ready(function(e) {

	if(isBlank(firstCid) && isBlank(twoCid) && isBlank(thirdCid)){
		$('#files').tree({
			expanded : 'li:lt(2)'
		});
	}else{
		$('#files').tree({
		});
	}

	$("ul li[role=treeitem] a").each(function(){
		if($(this).hasClass("selected")){
			$(this).parents("li[role=treeitem]").find("i.tree-parent").trigger('expand');
			$(this).parents("li[role=treeitem]").siblings().find("i.tree-parent").trigger('collapse');
		}
	});

	$.post(contextPath+"/integral/exchangeList", {"firstCid":firstCid,"twoCid":twoCid,"thirdCid":thirdCid},
			function(retData) {
		$("#exchangeList").html(retData);
	},'html'); 
});