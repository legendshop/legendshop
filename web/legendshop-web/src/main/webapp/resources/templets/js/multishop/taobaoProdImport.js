$(function(){
	//初始化日期下拉框
	dateOption(16);

	$.validator.setDefaults({});
	$("#importForm").validate({
		rules: {
			csvFile: {required: true}
		},
		messages: {
			csvFile: "您还未选择要导入的csv文件!"
		}
	});

	//当file输入框改变时,对文件进行校验
	$("#importForm :file").change(function(){
		var file = this.files[0];

		if(file.size == 0){
			alert("您选择的文件大小为0,请选择有效的文件!");
			$(this).val("");
			return false;
		}	    
		if(!file.name.match(/.*\.(csv|xlsx|xls)$/)){
			alert("请选择文件扩展名为csv、xls、xlsx的文件!");
			$(this).val("");
			return false;
		}
		return true;
	});

	$("#importForm :radio[name='publishStatus']").change(function(){
		/* alert(""); */
	});

	//监听表单提交事件
	$("#importForm").submit(function(){
		//提交前对表单进行校验
		if(!$("#importForm").validate().form()){
			return false;
		}

		//获取设定时间的值
		var startDate = $("#year").val()+" "+$("#hours").val()+":"+$("#minute").val()+":00";//格式：2015-7-30 9:30:00
		$("#startDate").val(startDate);


		var formData= new FormData($("#importForm")[0]);

		var xhr = getXhr();

		xhr.open("POST", $('#importForm').attr('action'), true);
		xhr.onload = function(oEvent) {
			if (xhr.status == 200) {
				var result = eval("(" + xhr.responseText + ")");
				if(result == "OK"){
					alert("恭喜你,导入成功!");
				}
				if(result == "FAIL"){
					alert("恭喜你,导入成功!");
				}
			} else {
				alert("导入失败,请求出错!");
			}
		}

		xhr.send(formData);
		return false;
	});
});

function importProd(){
	$("#importForm").submit();
}
