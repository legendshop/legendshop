function deleteAction(type){
	//获取选择的记录集合
	selAry = $(".selectOne");
	if(!checkSelect(selAry)){
		layer.msg('删除时至少选中一条记录！',{icon:0});
		return false;
	}

	layer.confirm("删除后不可恢复, 确定要删除吗？", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		var ids = []; 
		for(i=0;i<selAry.length;i++){
			if(selAry[i].checked){
				ids.push(selAry[i].value);
			}
		}
		var result = deleteComment(ids,type);
		if('OK' == result){
			layer.msg('删除回复成功',{icon:1,time:500},function(){
				$("#prodcons").click();
			});
		}else{
			layer.msg('删除回复失败',{icon:2});
		}

	})
	return true;
}

function deleteComment(ids,type) {
	var result;
	var idsJson = JSON.stringify(ids);
	var ids = {"ids": idsJson};
	var url = contextPath+"/p/" + type;
	jQuery.ajax({
		url:url , 
		data: ids,
		type:'post', 
		async : false, //默认为true 异步   
		dataType : 'json', 
		success:function(retData){
			result = retData;
		}
	});
	return result;
}


function pager(curPageNO){
	userCenter.loadPageByAjax("/p/prodcons?curPageNO=" + curPageNO);
}

$(document).ready(function() {
	userCenter.changeSubTab("prodcons");
});

//全选
$(".selectAll").click(function(){
    
	if($(this).attr("checked")=="checked"){
		$(this).parent().parent().addClass("checkbox-wrapper-checked");
         $(".selectOne").each(function(){
        		 $(this).attr("checked",true);
        		 $(this).parent().parent().addClass("checkbox-wrapper-checked");
         });
     }else{
         $(".selectOne").each(function(){
             $(this).attr("checked",false);
             $(this).parent().parent().removeClass("checkbox-wrapper-checked");
         });
         $(this).parent().parent().removeClass("checkbox-wrapper-checked");
     }
 });

function selectOne(obj){
	if(!obj.checked){
		$(".selectAll").checked = obj.checked;
		$(obj).prop("checked",false);
		$(obj).parent().parent().removeClass("checkbox-wrapper-checked");
	}else{
		$(obj).prop("checked",true);
		$(obj).parent().parent().addClass("checkbox-wrapper-checked");
	}
	 var flag = true;
	  var arr = $(".selectOne");
	  for(var i=0;i<arr.length;i++){
	     if(!arr[i].checked){
	    	 flag=false; 
	    	 break;
	    	 }
	  }
	 if(flag){
		 $(".selectAll").prop("checked",true);
		 $(".selectAll").parent().parent().addClass("checkbox-wrapper-checked");
	 }else{
		 $(".selectAll").prop("checked",false);
		 $(".selectAll").parent().parent().removeClass("checkbox-wrapper-checked");
	 }
}