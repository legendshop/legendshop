$(document).ready(function() {
	userCenter.changeSubTab("myCommis");
	$("select.combox").initSelect();

	laydate.render({
		elem: '#startTime',
		calendar: true,
		theme: 'grid',
		trigger: 'click'
	});

	laydate.render({
		elem: '#endTime',
		calendar: true,
		theme: 'grid',
		trigger: 'click'
	});

});

function searchAward(curPageNO){
	if(curPageNO==""){
		curPageNO = 1;
	}
	$("#curPageNO").val(curPageNO);
	var url = contextPath + "/p/promoter/searchAward";
	var param = $("#searchForm").serialize();
	$.ajax({
		url : url,
		type :  "GET",
		data : param,
		dataType : "HTML",
		beforeSend : function(){

		},
		complate : function(xhr,status){

		},
		success : function(result,status,xhr){
			$("#awardList").empty();
			$("#awardList").html(result);
		}
	});
}

function pager(curPageNO){
	searchAward(curPageNO);
}
