/**
 *  Tony
 */
jQuery(document).ready(function(){

	//滚动条滚动事件
	jQuery(window).scroll(function(){
	var top = jQuery(document).scrollTop();
	if(top==0){
		  jQuery("#back_box").hide();
		  jQuery(".back_box_x").hide();
		}else{
		  jQuery("#back_box").show();
	      jQuery(".back_box_x").show();
		}
	});
	//
	jQuery("#toTop").click(function(){
       jQuery('body,html').animate({scrollTop:0},1000);
       return false;
    });



		// .slide({ titCell:"#hd_${layoutParam.layoutId} li", mainCell:"#bd_${layoutParam.layoutId} ul", effect:"fade",autoPlay:true, delayTime:500 });

	     //jQuery("#fullSlide_${layoutParam.layoutId}").slide({ titCell:"#hd_${layoutParam.layoutId} li", mainCell:"#bd_${layoutParam.layoutId} ul", effect:"fade",autoPlay:true, delayTime:500 });


	//页面加载模块
	$("div[id=content]").each(function (){
		var _this=jQuery(this);
		var layoutId = jQuery(this).attr("mark");
		var layoutDiv = jQuery(this).attr("div");
		var layout=jQuery(this).attr("layout");
		var layoutModuleType=jQuery(this).attr("type");
		var layoutDivId=jQuery(this).attr("layoutDivId");
		if(isBlank(layoutModuleType)){
			layoutModuleType="";
		}
		if(isBlank(layoutDiv)){
			layoutDiv="";
		}
		if(isBlank(layoutDivId)){
			layoutDivId="";
		}
		if(!isBlank(layoutModuleType)){
			  var data={"layoutId" : layoutId, "layoutDiv" : layoutDiv,"layout":layout,"layoutModuleType":layoutModuleType,"divId":layoutDivId};
			  jQuery.ajax({
		           //提交数据的类型 POST GET
		           type:"POST",
		           //提交的网址
		           url:loadModuleUrl,
		           //提交的数据
		           data:data,
		           async : false,
		           cache: true,
		           //返回数据的格式
		           datatype: "html",
		           //成功返回之后调用的函数
		           success:function(html){
			        	  if(html==null || html.indexOf("fail")!=-1){
			        		  jQuery(_this).parent.remove();
			        	  }else{
			        		  jQuery(_this).html("");
			        		  jQuery(_this).html(html);
			        	  }
		           } ,
		           //调用执行后调用的函数
		           complete: function(XMLHttpRequest, textStatus){
		           }
		     });

	    }
    });

	jQuery("#store_head_box").css("cursor","peointer").mouseover(function(){
			jQuery("#store_level").show();
		})

	jQuery("#store_head_box").mouseout(function(){
			jQuery("#store_level").hide();
		});

	jQuery(".store_nav li").each(function(){
		   var nav_id = jQuery(this).attr("id");
		   if (nav_id == "current") {
			jQuery(this).addClass("this");
		   } else
		  jQuery(this).removeClass("this");
	 });


	//默认幻灯
	jQuery("#default_fullSlide").slide({
		titCell : "#default_hd li",
		mainCell : "#default_bd ul",
		effect : "fold",
		autoPlay : true,
		delayTime : 700
	});
	//商品分类点击事件
	jQuery("i[id^='i_']").click(function(){
		  var str = jQuery(this).attr("id").substring(2,5);
		  var child_show=jQuery(this).attr("child_show");
		  if(str == "one") {//点击的是一级
			  firstCatOnOff(this, child_show);
		  }else {//点击的是二级
			  secondCatOnOff(this, child_show);
		  }
	 });

	 //初始收起分类
	 initCat();

	/*//轮播幻灯片
	$("div[id^='fullSlide_']").each(function(i){
		var layoutId=$(this).attr("layoutId");
		$(this).slide({ titCell:"#hd_"+layoutId+" li", mainCell:"#bd_"+layoutId+" ul", effect:"fade",autoPlay:true, delayTime:500 });
	});*/
});

function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}

//初始收起分类
function initCat(){
	//全部关闭
	$(".level_second").hide();
	$(".level_third").hide();
	$("i[id^='i_']").attr("child_show","false");
	$("i[id^='i_']").removeClass("i_cut");
	$("i[id^='i_']").addClass("i_add");

	//第一个打开
	var firstLevel = $(".level_li").first();
	$(firstLevel).find(".level_one").find("i").attr("child_show","true");
	$(firstLevel).find(".level_one").find("i").removeClass("i_add");
	$(firstLevel).find(".level_one").find("i").addClass("i_cut");
	$(firstLevel).find(".level_second").show();

	var secondLevel = $(firstLevel).find(".level_second").find("ul");
	$(secondLevel).find("li").first().find("i").attr("child_show","true");
	$(secondLevel).find("li").first().find("i").removeClass("i_add");
	$(secondLevel).find("li").first().find("i").addClass("i_cut");
	$(secondLevel).find(".level_third").first().show();

}

//一级菜单开关
function firstCatOnOff(obj, _on_of) {
	if(_on_of == "true"){
	    $(obj).removeClass("i_cut");
		$(obj).addClass("i_add");
		$(obj).parent().next(".level_second").slideUp('normal');
	    $(obj).attr("child_show","false");
	 }else{
	    $(obj).removeClass("i_add");
		$(obj).addClass("i_cut");
	    $(obj).parent().next(".level_second").slideDown('normal');
	    $(obj).attr("child_show","true");
	 }
}

//二级菜单开关
function secondCatOnOff(obj, _on_of) {
	if(_on_of == "true"){
	    $(obj).removeClass("i_cut");
		$(obj).addClass("i_add");
		$(obj).parent().next(".level_third").slideUp('normal');
	    $(obj).attr("child_show","false");
	 }else{
	    $(obj).removeClass("i_add");
		$(obj).addClass("i_cut");
		$(obj).parent().next(".level_third").slideDown('normal');
	    $(obj).attr("child_show","true");
	 }
}
$(function () {//获取表单并跳转

  var data = {
    "shopId": shopId
  };
  jQuery.ajax({
    url: contextPath + "/productcomment/business/flag",
    data: data,
    type: "GET",
    async: true, //默认为true 异步
    success: function (result) {
      shopType = result;
      if (result == 1){
        $("#clickMessage").html("注：点击可查看商家证照信息");
      }
    }
  });

  $(".store_name").click(function () {

      if (shopType == 1) {
        var url= contextPath +"/productcomment/business/message?shopId="+shopId;
        location.href=url;
      }
  });
  // $(".store_name").click(function () {
  //   var url= contextPath +"/productcomment/business/message?shopId="+shopId;
  //   location.href=url;
  // });
});
