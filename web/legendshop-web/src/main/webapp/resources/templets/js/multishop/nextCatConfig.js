function submitTable(){
	$("#form1").submit();
}

$(document).ready(function() {
	userCenter.changeSubTab("shopCategory");
	$("#shopCatConfig").click(function(){//前往 一级类目配置
		window.location.href=contextPath+"/s/shopCatConfig";
	});

	$("#shopCatList").click(function(){//前往 一级类目列表
		window.location.href=contextPath+"/s/shopCategory";
	});	

	$("#nextCatList").click(function(){//前往二级类目列表
		window.location.href=contextPath+"/s/nextShopCat/query?shopCatId="+shopCatId;
	});

	$(".J_spu-property").divselect();
	jQuery("#form1").validate({
		rules: {
			name: "required",
			seq: {
				number:true,
				max:99999
			}
		},
		messages: {
			name: "请输入类目名称",
			seq: {
				number: "请输入数字",
				max:"次序不超过99999"
			}
		}
	});

	$("input[name=file]").change(function(){
		checkImgType(this);
		checkImgSize(this,512);
	});
});