var url = contextPath + "/s/refundList";
$(function(){
	if(applyType == 2){
		url = contextPath + "/s/returnList";
		$("#listType li[value='1']").removeClass("on");
		$("#listType li[value='2']").addClass("on");
		$("#refundOption").hide();
		$("#refundOption").attr("name", "");
		$("#returnLOption").show();
		$("#returnLOption").attr("name", "customStatus");
	}
	loadList(url, null);

	$("#listType li").click(function(){
		var value = $(this).val();
		if(value == applyType){
			return;
		}

		$("#listType li").removeClass("on");
		$(this).addClass("on");

		if(value == 1){
			$("#refundOption").show();
			$("#refundOption").attr("name", "customStatus");
			$("#returnLOption").hide();
			$("#returnLOption").attr("name", "");
			url = contextPath + "/s/refundList";
			loadList(url, null);
		}else{
			$("#refundOption").hide();
			$("#refundOption").attr("name", "");
			$("#returnLOption").show();
			$("#returnLOption").attr("name", "customStatus");
			url = contextPath + "/s/returnList";
			loadList(url, null);
		}
		applyType = value;
	});
});

function search(){
	$("#searchForm #curPageNO").val("1");
	var params = $("#searchForm").serialize();
	if(applyType == 2){
		url = contextPath + "/s/returnList";
	}
	loadList(url,params);
}

function loadList(url,param){
	$.ajax({
		url : url,
		type : "GET",
		data : param,
		dataType: "html",
		success : function(result,status,xhr){
			$("#listBox").html(result);
		}
	});
}



function pager(curPageNO){
    $("#curPageNO").val(curPageNO);
	var params = $("#searchForm").serialize();
	if(applyType == 2){
		url = contextPath + "/s/returnList";
	}
	loadList(url, params);
}

//显示收货窗口
function showReceiveWin(refundId){
	layer.open({
		title :"确认收货",
		  type: 2,
		  content: contextPath + "/s/showReceive/" + refundId, //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['500px', '380px']
	});
}
