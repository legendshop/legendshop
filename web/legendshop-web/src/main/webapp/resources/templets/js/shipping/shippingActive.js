$(function(){
	userCenter.changeSubTab("shippingMange"); //高亮菜单
	//搜索活动
	$("#searchBtn").on("click", function(){
		$("#myForm").submit();
	});

	//添加店铺包邮
	$("#add-shop").on("click", function(){
		location.href = contextPath + "/s/shippingActive/addShopShipping/0";
	});

	//添加商品包邮
	$("#add-prod").on("click", function(){
		location.href = contextPath + "/s/shippingActive/addProdShipping";
	});

	//编辑店铺店铺包邮
	$("#shipContent").on("click", ".edit", function(){
		var id = $(this).attr("data-id");
		var fullType = $(this).attr("data-fullType");
		if(0==fullType){
			location.href = contextPath + "/s/shippingActive/addShopShipping/" + id;
		}else{
			location.href = contextPath + "/s/shippingActive/editProdShipping/" + id;
		}
	});

	//撤销包邮活动
	$("#shipContent").on("click", ".cancel", function(){
		var id = $(this).attr("data-id");
		var url = contextPath + "/s/shippingActive/cancelShipping/" + id;
		layer.open({
			  btn: ['确定','取消'],
			  skin: ['user-btn-class'],
			  content: "确定撤销包邮活动吗",
			  yes: function(index, layero){
				  $.ajax({
						type : "POST",
						dataType : "json",
						url : url,
						success : function(result) {
							if (result.status == "OK") {
								layer.msg('撤销成功');
								setTimeout(function() {
									location.reload();
								}, 1000)
							} else {
								layer.msg(result.msg);
							}
						},
					});
			  }
			});

	});

	//删除包邮活动
	$("#shipContent").on("click", ".del", function(){
		var id = $(this).attr("data-id");
		var url = contextPath + "/s/shippingActive/delShipping/" + id;
		layer.open({
			  btn: ['确定','取消'],
			  skin: ['user-btn-class'],
			  content: "确定删除包邮活动吗",
			  yes: function(index, layero){
				  $.ajax({
						type : "POST",
						dataType : "json",
						url : url,
						success : function(result) {
							if (result.status == "OK") {
								layer.msg('删除成功');
								setTimeout(function() {
									location.reload();
								}, 1000)
							} else {
								layer.msg(result.msg);
							}
						},
					});
			  }
			});
	});
});

function pager(curPageNO) {
	$("#curPageNO").val(curPageNO);
	$("#myForm").submit();
}
