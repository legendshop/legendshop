$(function(){
	prodAnalysis.doExecute();
	userCenter.changeSubTab("prodAnalysis");
});


//路径配置
require.config({
	paths: {
        echarts: contextPath+'/resources/plugins/ECharts/dist'
    }
});

//使用
require(
		[ 
	        'echarts', 
	        'echarts/chart/bar',
	        'echarts/chart/line'
	    ],
	    function (ec) {
			// 基于准备好的dom，初始化echarts图表
	        var myChart = ec.init(document.getElementById('main')); 
			
	        option = {
	        	    tooltip : {
	        	        trigger: 'axis'
	        	    },
	        	    toolbox: {
	        	        show : true,
	        	        feature : {
	        	            mark : {show: true},
	        	            dataView : {show: true, readOnly: false},
	        	            magicType: {show: true, type: ['line', 'bar']},
	        	            restore : {show: true},
	        	            saveAsImage : {show: true}
	        	        }
	        	    },
	        	    calculable : true,
	        	    legend: {
	        	        data:['商品销量','销售金额']
	        	    },
	        	    xAxis : [
	        	        {
	        	            type : 'category',
	        	            data :getXAxisData()
	        	        }
	        	    ],
	        	    yAxis : [
	        	        {
	        	            type : 'value',
	        	            name : '商品销量'	  
	        	        },
	        	        {
	        	            type : 'value',
	        	            name : '销售金额'
	        	        }
	        	    ],
	        	    series : [
	        	        {
	        	            name:'商品销量',
	        	            type:'bar',
	        	            itemStyle:{
	                              normal:{color:'#2ec7c9'}
	                         },
	        	            data :getBarData()
	        	        },
	        	        {
	        	            name:'销售金额',
	        	            type:'line',
	        	            yAxisIndex: 1,
	        	            itemStyle:{
	                              normal:{color:'#b6a2de'}
	                        },
	        	            data :getLineData()
	        	        }
	        	    ]
	        	};
	        	                    
	        
	     	// 为echarts对象加载数据 
	        myChart.setOption(option); 
		}
);

function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("from1").submit();
}

//获取 x轴 数据
function getXAxisData(){
	var list = [];
	if(!isBlank($("#resultSize").val())){
		var resultSize = Number($("#resultSize").val());
		for(var x=1;x<=resultSize;x++){
			list.push(x);
		}
	}
    return list;
}

//获取 商品销量 数据
function getBarData(){
	var list = [];
	if(!isBlank($("#prodCountsJson").val())){
		var prodCountsData = jQuery.parseJSON($("#prodCountsJson").val());
	    for(var x=0;x<prodCountsData.length;x++){
	    	list.push(prodCountsData[x]);
	    }
	}
    return list;
}

//获取 销售金额 数据
function getLineData(){
	var list = [];
	if(!isBlank($("#prodCashJson").val())){
		var prodCashData = jQuery.parseJSON($("#prodCashJson").val());
	    for(var x=0;x<prodCashData.length;x++){
	    	list.push(prodCashData[x]);
	    }
	}
    return list;
}

/**方法，判断是否为空 **/
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}