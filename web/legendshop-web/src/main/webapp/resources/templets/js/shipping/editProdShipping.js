$(function() {
	var isClick=0;//是否可以追加

	  //提交数据
	  $("#commit").click(function(){
		  var activeId = $("#activeId").val();
		  var activeName = $("#name").val();
		  var flag = 1;
		  if(isBlank(activeName) || parseInt(activeName.length)<2 || parseInt(activeName.length)>20){
			  $("#name").css("border","1px solid #f47162");
			  $("div[name='namePrompt']").css("display","block");
			  flag = 0;
		  }

		  var isProd = $("#isProd").val();
		  if(isProd==0){
			  art.dialog.tips("请选择活动商品");
			  return;
		  }

		  if(flag==1){
			  var url = contextPath+"/s/shippingActive/editCommitProdFull/"+activeId;
			  var editActiveNameDto = {};
			  editActiveNameDto.activeName = activeName;
			  editActiveNameDto.prodIds = joinProdIdList;
			  $.ajax({
					url: url,
					data: JSON.stringify(editActiveNameDto),
					type: "post",
					async : false, //默认为true 异步
					dataType : 'json',
				    contentType: "application/json",
					success : function(result) {
						if (result == "OK") {
							layer.alert('添加成功', {icon: 1},function(){
						            window.location.href=contextPath+"/s/shippingActive/query";
					    	});
						} else {
							layer.alert('添加失败，请重试！', {icon: 2});
						}
					},
				});
		  }else{
			  return;
		  }
	  });


	  /**商品 tab 切换*/
	  $(".ant-tabs-tab").click(function(){
	  	var type=$(this).attr("data-type");
	  	$(".ant-tabs-tab").removeClass("ant-tabs-tab-active");
	  	$(this).addClass("ant-tabs-tab-active");

	  	if ("select"==type) {
	  		$('.ant-tabs-content-animated').css("margin-left","0%");
	  		$('.ant-tabs-ink-bar-animated').css("transform","translate3d(0px, 0px, 0px)");
	  	} else {
	  		$('.ant-tabs-content-animated').css("margin-left","-100%");
	  		$('.ant-tabs-ink-bar-animated').css("transform","translate3d(120px, 0px, 0px)");
	  	}
	  });


	//点击商品头部全选
	$(".all").on("click",".ant-table-thead .ant-checkbox",function(){
		var optional = $(this).attr("optional");
		if(optional=="true"){//可以勾
			if($(this).hasClass("ant-checkbox-checked")){//再点击取消勾选
				  $(this).removeClass("ant-checkbox-checked");
				  $(this).removeClass("ant-checkbox-checked-1");

				  $(this).parents(".prodTable").find(".ant-table-tbody .ant-checkbox[optional='true']").removeClass("ant-checkbox-checked");
				  $(this).parents(".prodTable").find(".ant-table-tbody .ant-checkbox[optional='true']").removeClass("ant-checkbox-checked-1");

				  $(this).parents(".prodTable").find(".ant-table-tbody .ant-checkbox[optional='true']").each(function(index,element){
					  var prodId = $(element).attr("data-id");
					  var index = prodIdList.indexOf(prodId);
					  if(index>-1){//证明数组里面有，干掉
						  prodIdList.splice(index, 1);
					  }
				  });
			  }else{//勾上
				  $(this).addClass("ant-checkbox-checked");
				  $(this).addClass("ant-checkbox-checked-1");

				  $(this).parents(".prodTable").find(".ant-table-tbody .ant-checkbox[optional='true']").addClass("ant-checkbox-checked");
				  $(this).parents(".prodTable").find(".ant-table-tbody .ant-checkbox[optional='true']").addClass("ant-checkbox-checked-1");

				  $(this).parents(".prodTable").find(".ant-table-tbody .ant-checkbox[optional='true']").each(function(index,element){
					  var prodId = $(element).attr("data-id");
					  var index = prodIdList.indexOf(prodId);
					  if(index==-1){//证明数组里面没有，要push进去
						  prodIdList.push(prodId);
					  }
				  });
			  }

		}
		return false;
	})

	//点击商品单个选择
	$(".all").on("click",".ant-table-tbody .ant-checkbox",function(){
		 var optional = $(this).attr("optional");
		 var prodId = $(this).attr("data-id");//商品ID
		 if(optional=="true"){//可以勾
			 if($(this).hasClass("ant-checkbox-checked")){//取消勾
				 $(this).removeClass("ant-checkbox-checked");
				  $(this).removeClass("ant-checkbox-checked-1");

				  //将总选择勾掉
				  $(this).parents(".prodTable").find(".ant-table-thead .ant-checkbox").removeClass("ant-checkbox-checked");
				  $(this).parents(".prodTable").find(".ant-table-thead .ant-checkbox").removeClass("ant-checkbox-checked-1");

				  //从数组干掉勾掉的数据
				  var index = prodIdList.indexOf(prodId);
				  if(index > -1) {
					  prodIdList.splice(index, 1);
				  }
			  }else{//勾上
				  $(this).addClass("ant-checkbox-checked");
				  $(this).addClass("ant-checkbox-checked-1");

				  var index = prodIdList.indexOf(prodId);
				  if(index == -1) {//不存在
					prodIdList.push(prodId);
				  }
			  }
		 }
		 return false;
	})


	//点击左侧商品的单个参加
	$(".all").on("click",".ant-table-tbody button[name='join']",function(){
		$("#isProd").val(parseInt($("#isProd").val())+1);
		var prodId = $(this).attr("data-id");
		if($(".all").data(prodId)==null){
			$(".all").data(prodId,$(this).parents(".caozuo").html());
		}

		//获取文本
		var text = $(this).parents(".ant-table-row").html();
		var _dom = $(this).parents(".ant-table-row");

		//左侧将前面所有复选框取消勾选
		$(this).parents(".prodTable").find(".ant-checkbox").removeClass("ant-checkbox-checked");
		$(this).parents(".prodTable").find(".ant-checkbox").removeClass("ant-checkbox-checked-1");
		//清空批量操作的数组
		prodIdList.splice(0,prodIdList.length);

		//为这个参加的商品移除optional属性，以便我再次勾全选时勾不到这个商品
		$(this).parents(".ant-table-row").find(".ant-checkbox").removeAttr("optional");

		$(this).parents(".caozuo").html("<span>已选择<a class='pushl' data-id='"+prodId+"'>取消参加</a></span>");

		//加进join数组
		var index = joinProdIdList.indexOf(prodId);
		if(index == -1) {//不存在
			joinProdIdList.push(prodId);
		}
		//往右侧列表追加数据
		$(".selected").append("<tr data-id='"+prodId+"'>"+text+"</tr>");//不在改变文本之后在append，是因为改变之后就选不到单前的this，button已被移除
		$(".selected").find("tr[data-id='"+prodId+"']").find(".caozuo").html("<span>已选择<a class='pushl' data-id='"+prodId+"'>取消参加</a></span>");
		$(".selected").find("tr[data-id='"+prodId+"']").find("td:first").text("");

		//将右侧列表样式移除
		$(".selected").find(".ant-checkbox").removeClass("ant-checkbox-checked");
		$(".selected").find(".ant-checkbox").removeClass("ant-checkbox-checked-1");

		//将左侧自己的复选框不可编辑
		_dom.find(".ant-checkbox-input").attr("disabled","disabled");

	})

   //点击左侧商品单个取消参加
	$(".all").on("click",".ant-table-tbody .pushl",function(){
		$("#isProd").val(parseInt($("#isProd").val())-1);
		//为这个参加的商品增加optional属性，以便我再次勾全选时勾到这个商品
		$(this).parents(".ant-table-row").find(".ant-checkbox").attr("optional","true");
		$(this).parents(".ant-table-row").find(".ant-checkbox-input").removeAttr("disabled");

		var prodId = $(this).attr("data-id");
		$(this).parents(".caozuo").html($(".all").data(prodId));

		//从数组干掉勾掉的数据
		var index = joinProdIdList.indexOf(prodId);
		if(index > -1) {
			joinProdIdList.splice(index, 1);
		}
		//移除追加的右侧列表
		$(".selected").find("tr[data-id='"+prodId+"']").remove();
	});

	//点击左侧商品批量参加
	$(".all").on("click",".batchJoin",function(){

		if(isBlank(prodIdList)){
			art.dialog.tips("请勾选参加活动的商品");
			return;
		}

		//左侧将前面所以复选框取消勾选
		$(this).parents(".prodTable").find(".ant-checkbox").removeClass("ant-checkbox-checked");
		$(this).parents(".prodTable").find(".ant-checkbox").removeClass("ant-checkbox-checked-1");

		for(var i = 0;i < prodIdList.length;i++) {
			$("#isProd").val(parseInt($("#isProd").val())+1);
			var prodId = prodIdList[i];
			var _dom = $(".prodTable").find("tr[data-id='"+prodId+"']");//初始的tr行this

			if(_dom.html()==undefined || _dom.html()==null){
				_dom = $(".all").data("context").find("tr[data-id='"+prodId+"']");
			}

			//将文本存缓存,存完缓存在修改，方便回复的时候修改
			if($(".all").data(prodId)==null){
				$(".all").data(prodId,_dom.find(".caozuo").html());
			}

			_dom.find(".caozuo").html("<span>已选择<a class='pushl' data-id='"+prodId+"'>取消参加</a></span>");

			//加进join数组
			var index = joinProdIdList.indexOf(prodId);
			if(index == -1) {//不存在
				joinProdIdList.push(prodId);
			}

			//往右侧列表追加数据
			var text = _dom.html();
			$(".selected").append("<tr data-id='"+prodId+"'>"+text+"</tr>");
			$(".selected").find("tr[data-id='"+prodId+"']").find("td:first").text("");

			//为这个参加的商品移除optional属性，以便我再次勾全选时勾不到这个商品
			_dom.find(".ant-checkbox").removeAttr("optional");

			//将左侧自己的复选框不可编辑
			_dom.find(".ant-checkbox-input").attr("disabled","disabled");
		}

		//清空批量操作的数组
		prodIdList.splice(0,prodIdList.length);

		//左侧将所有复选框取消勾选
		$(".prodTable").find(".ant-checkbox").removeClass("ant-checkbox-checked");
		$(".prodTable").find(".ant-checkbox").removeClass("ant-checkbox-checked-1");

		//将右侧列表样式移除
		$(".selected").find(".ant-checkbox").removeClass("ant-checkbox-checked");
		$(".selected").find(".ant-checkbox").removeClass("ant-checkbox-checked-1");

	});


	 //为追加按钮绑定了移入事件
	$(".all").on("mouseover",".allSel",function(){
		var flag =0;//不予许
		$(".prodTable").find(".ant-table-tbody .ant-table-row").each(function(index,element){
			if($(element).find(".ant-checkbox").attr("optional")=="true"){
				flag =1;
				return false;
			}
		})
		if(flag==0){
			$(this).find(".ant-checkbox-input").attr("disabled","disabled");
			$(this).find(".ant-checkbox").removeAttr("optional");
		}else{
			$(this).find(".ant-checkbox-input").removeAttr("disabled");
			$(this).find(".ant-checkbox").attr("optional","true");
		}
	})


	//右侧取消参加
	$(".selected").on("click",".pushl",function(){
		var prodId = $(this).attr("data-id");

		//为这个参加的商品增加optional属性，以便我再次勾全选时勾到这个商品
		$(".prodTable").find("tr[data-id='"+prodId+"']").find(".ant-checkbox").attr("optional","true");
		$(".prodTable").find("tr[data-id='"+prodId+"']").find(".ant-checkbox-input").removeAttr("disabled");

		$(".prodTable").find("tr[data-id='"+prodId+"']").find(".caozuo").html($(".all").data(prodId));

		//从数组干掉勾掉的数据
		var index = joinProdIdList.indexOf(prodId);
		if(index > -1) {//存在
			joinProdIdList.splice(index, 1);
		}
		//移除追加的右侧列表
		$(".selected").find("tr[data-id='"+prodId+"']").remove();

	})


})

/**翻页*/
function pager(curPageNO){

	//存对象到缓存
	var falg = 0;
	$(".prodTable").find(".ant-table-row").each(function(index,element){
		if($(element).find(".ant-checkbox").hasClass("ant-checkbox-checked")){
			falg=1;
		}
	})
	if(falg==1){
		if($(".all").data("context")==null){
			$(".all").data("context",$(".prodTable"));
		}else{
			var context = $(".all").data("context");
			var h = 0;
			$(".prodTable").find(".ant-table-row").each(function(index,element){//本页的tr
				if($(element).find(".ant-checkbox").hasClass("ant-checkbox-checked")){//证明需要添加的当前页勾选
					var prodId = $(element).attr("data-id");
					var re = 1;
					context.find(".ant-table-tbody .ant-table-row").each(function(index,ele){
						var pid = $(ele).attr("data-id");
						if(prodId==pid){//证明需要添加的缓存里面有了
							re = 0;
						}
					});
					if(re==1){
						context.find(".ant-table-tbody").append("<tr class='ant-table-row ant-table-row-level-0' data-id='"+prodId+"'>"+$(element).html()+"</tr>");
						h=1;
					}
				}
			})
			if(h==1){
				$(".all").data("context",context);
			}
		}
	}

	asyncload(curPageNO);
}

function asyncload(curPageNO) {
	var editActiveId = $("#editActiveId").val();
	var url = contextPath + "/s/shippingActive/editfullProd/"+editActiveId;
	var prodName = $("#prodName").val();
	var check = $("#isCheck").val();
	$.ajax({
		url : url,
		data : {"curPageNo":curPageNO,"prodName":prodName,"check":check},
		type : "get",
		dataType : "html",
		success : function(result) {
			$(".all-prod").html(result);
		}
	});
}

//名称失去焦点
$("#name").blur(function(){
	var activeName = $("#name").val();
	 if(isBlank(activeName) || parseInt(activeName.length)<2 || parseInt(activeName.length)>25){
		  $("#name").css("border","1px solid #f47162");
		  $("div[name='namePrompt']").css("display","block");
	  }else{
		  $("#name").css("border","1px solid #d9d9d9");
		  $("div[name='namePrompt']").css("display","none");
	  }
});


//判断是否为空
function isBlank(value){
	return value == undefined ||  value == null || value == "";
}

//格式化小数
 function toDecimal(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return;
    }
    f = Math.round(x*100)/100;
    return f;
  }

//保留多少为小数
 function fomatFloat(src,pos){
	 return Math.round(src*Math.pow(10, pos))/Math.pow(10, pos);
 }

//获取索引
Array.prototype.indexOf = function(val) {
	for (var i = 0; i < this.length; i++) {
	if (this[i] == val) return i;
	}
	return -1;
};

function isContains(str, substr) {//true代表包含
	 return str.indexOf(substr) >= 0;
}


function updateOne(url,id){
	 location.href = url;
}
