$(document).ready(function() {
	userCenter.changeSubTab("presellManage"); //高亮菜单

	//初始化预售活动列表
	search();

	$("#statusTab>span").click(function(){
		$("#statusTab>span").removeClass("on");
		$(this).addClass("on");
		var searchType = $(this).attr("value");
		$("#searchType").val(searchType);
		search();
	});
});

function pager(curPageNO){
    document.getElementById("curPageNO").value=curPageNO;
    search();
}

Date.prototype.format = function(fmt) {
    var o = {
       "M+" : this.getMonth()+1,                 //月份
       "d+" : this.getDate(),                    //日
       "h+" : this.getHours(),                   //小时
       "m+" : this.getMinutes(),                 //分
       "s+" : this.getSeconds(),                 //秒
       "q+" : Math.floor((this.getMonth()+3)/3), //季度
       "S"  : this.getMilliseconds()             //毫秒
   };
   if(/(y+)/.test(fmt)) {
           fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
   }
    for(var k in o) {
       if(new RegExp("("+ k +")").test(fmt)){
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
        }
    }
   return fmt;
}

$("#searchPresellProd").submit(function () {
  search();
  return false;
})

function search(){
	var param = $("#searchPresellProd").serializeObject();
	if(!isEmpty(param.preSaleStart)){
		param.preSaleStart=new Date(param.preSaleStart).format("yyyy-MM-dd hh:mm");
	}
	if(!isEmpty(param.preSaleEnd)){
		param.preSaleEnd=new Date(param.preSaleEnd).format("yyyy-MM-dd hh:mm");
	}
	$.ajax({
		url : contextPath + "/s/presellProd/listContent",
		type : "GET",
		data : param,
		cache : false,
		async : true,
		beforeSend : function(xhr){

		},
		complete : function(xhr,status){

		},
		success : function(result,status,xhr){
			$("#presellList").html(result);
		}
	});

	//实现无刷新改变浏览器地址框
	if ('pushState' in window.history){//检查兼容
		window.history.pushState(null,null,contextPath + "/s/presellProd/query/"+param.searchType);
	}
}
function isEmpty(value) {
	if (value == null || value == "" || value == "undefined"
			|| value == undefined || value == "null") {
		return true;
	} else {
		value = (value + "").replace(/\s/g, '');
		if (value == "") {
			return true;
		}
		return false;
	}
}
//删除预售活动
function deletePresellProd(id){

	layer.confirm('您确定要删除该预售活动?',{
 		 icon: 3
 	     ,btn: ['确定','取消'] //按钮
 	   }, function(index) {
 		  $.ajax({
 	    		url : contextPath + "/s/presellProd/delete/"+id,
 	    		type : "GET",
 	    		dataType : "JSON",
 	    		success : function(result){
 	    			if(result == "OK"){
 	    				layer.msg("删除成功!", {icon:1, time:1000}, function(){
 	    					search();
 	    				});
 	    			}else{
 	    				layer.alert(result, {icon: 0});
 	    			}
 	    		}
 	    	});
 		  layer.close(index);
	})
}

//预售活动提审
function submitPresellProd(id){
    layer.confirm('您确定要提审该活动吗?',{
		 icon: 3
	     ,btn: ['确定','取消'] //按钮
	   }, function () {
		   $.ajax({
	    		url : contextPath + "/s/presellProd/submit/"+id,
	    		type : "GET",
	    		dataType : "JSON",
	    		success : function(result){
	    			if(result == "OK"){
	    				layer.msg("恭喜你 ,提审成功!", {icon:1, time:700}, function(){
	    					search();
	    				});
	    			}else{
	    				layer.alert(result, {icon: 0});
	    			}
	    		}
	    	});
	})
}

