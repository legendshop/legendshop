function onlineShopMarketing(_id,text){
	layer.confirm("确定要"+text+"该活动么？", {
		icon: 3
		,btn: ['确定','取消'] //按钮
	},function () {
		if(chang>=4){
			window.location.href=contextPath+"/s/onlineShopMarketing/4/"+_id;
		}else{
			window.location.href=contextPath+"/s/onlineShopMarketing/1/"+_id;
		}
	});
}

function offlineShopMarketing(_id){
	layer.confirm("确定要终止该活动么？",{
		icon: 3
		,btn: ['确定','取消'] //按钮
	}, function () {
		if(chang>=4){
			window.location.href=contextPath+"/s/offlineShopMarketing/4/"+_id;
		}else{
			window.location.href=contextPath+"/s/offlineShopMarketing/1/"+_id;
		}
	});
}

function suspendShopMarketing(_id){
    layer.confirm("确定要暂停该活动么？",{
        icon: 3
        ,btn: ['确定','取消'] //按钮
    }, function () {
            window.location.href=contextPath+"/s/suspendShopMarketing/1/"+_id;
    });
}

function deleteShopMarketingMansong(_id){
	layer.confirm("确定要删除活动么？", {
		icon: 3
		,btn: ['确定','取消'] //按钮
	},function () {
		if(chang>=4){
			window.location.href=contextPath+"/s/deleteShopMarketing/4/"+_id;
		}else{
			window.location.href=contextPath+"/s/deleteShopMarketing/1/"+_id;
		}

	});

}
function pager(curPageNO){
	$("#curPageNO").val(curPageNO);
	$("#form2").submit();
}