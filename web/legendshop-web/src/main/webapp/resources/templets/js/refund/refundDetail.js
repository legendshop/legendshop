//打开申请维权页面
function openApplyInterven(refundSn){
    	art.dialog.open(contextPath+'/p/apply/intervene/'+refundSn,
    		{
    		  id:'refundSn',
    		  title: '申请客服维权',
    		  lock:true,
    		  resize: false,
    		  width:'470px',
              height:'280px'
    		}
    );
 }

//查看维权信息
function viewInterven(refundSn){
	art.dialog.open(contextPath+'/p/intervene/query/'+refundSn,
			{
		id:'refundSn',
		title: '查看客服维权',
		lock:true,
		resize: false,
		width:'470px',
		height:'420px'
			}
	);
}

//确认退货
function confirmReturn() {
	var refundId = $("#refundId").val();
	var expressName = $("#expressName").val();
	var expressNo = $("#expressNo").val();

	if (isBlank(refundId) || isBlank(expressName) || isBlank(expressNo)) {
		art.dialog.tips("对不起,请您完善好物流信息后再确认!");
		return;
	}
	var rule= /^[A-Za-z0-9]+$/;
    if(!rule.test(expressNo)){
   	 art.dialog.tips("请输入正确的物流单号");
   	 return ;
    }

	var param = {
		"refundId" : refundId,
		"expressName" : expressName,
		"expressNo" : expressNo
	};
	$.ajax({
		url : contextPath+"/p/returnGoods",
		type : "POST",
		data : param,
		dataType : "JSON",
		async : true,
		beforeSend : function(xhr) {
			$("#submitBtn").attr("disabled", true);
		},
		complete : function(xhr, status) {
			$("#submitBtn").attr("disabled", false);
		},
		success : function(result, status, xhr) {
			if (result == "OK") {
				art.dialog.alert("恭喜你,操作成功!",function(){
					window.location.href = contextPath + "/p/refundReturnList/"+type;
				});
			} else {
				art.dialog.tips(result);
			}
		}
	});
}

function isBlank(value) {
	return value == undefined || value == null || $.trim(value) === "";
}
