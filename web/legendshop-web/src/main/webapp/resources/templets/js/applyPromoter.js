jQuery(document).ready(function() {
	$("#realName").bind('blur', function () {
		checkRealName();
	});
	
	$("#mobile").bind('blur', function () {
		checkTel();
	});
	
	$("#email").bind('blur', function () {
		checkEmail();
	});
	
	$("#randNum").bind('blur', function () {
		checkedRandNum();
	});
	
	$("#erifyCode").bind('blur', function () {
		checkErifyCode();
	});
	
	$("#userAdds").bind('blur', function () {
		checkUserAdds();
	});
	
});

//检查真实姓名
function checkRealName(){
	var realName=$("#realName").val();
	var realNameInput = $("#realName");
	var tipText = realNameInput.next();
	if(isBlank(realName)){
		tipText.attr("class","item-con_error");
		tipText.html("请输入真实姓名");
		return false;
	}
	if(realName.indexOf(" ")>-1){
		tipText.attr("class","item-con_error");
		tipText.html("真实姓名不能包含空格");
		return false;
	}
	tipText.attr("class","con_succeed");
	tipText.html("");
	return true;
}

//检查邮箱
function checkEmail(){
	var mail=$("#email").val();
	var mailInput=$("#email");
	var tipText = mailInput.next();
	if(isBlank(mail)){
		tipText.attr("class","item-con_error");
		tipText.html("邮箱不能为空");
		return false;
	}
	var isEmail=/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
	if(!isEmail.test(mail)){
		tipText.attr("class","item-con_error");
		tipText.html("邮箱格式不正确");
		return false;
	}
	
	var config=true;
	$.ajax({
		url: contextPath + "/p/isEmailOnly", 
		data: {"email":mail},
		type:'post', 
		async : false, //默认为true 异步   
		success:function(retData){
		 	if('true' == retData){
		 		tipText.attr("class","item-con_error");
				tipText.html("此邮箱已存在!");
				telInput.css('color','#000');
				telInput.attr("class","text highlight2");
				config=false;
				return false;
			}
		}
	});
	if(!config){
		return false;
	}
	
	
	tipText.attr("class","con_succeed");
	tipText.html("");
	return true;
}

//检查电话号码
function checkTel(){
	var isPhone=/^1\d{10}$/;
	var telValue = $("#mobile").val();
	var telInput = $("#mobile");
	var tipText = telInput.next();
	if(isBlank(telValue)){
		tipText.attr("class","item-con_error");
		tipText.html("请输入电话号码");
		return false;
	}
	if(telValue.length!=11){
		tipText.attr("class","item-con_error");
		tipText.html("电话号码必须为11位");
		return false;
	}
	if(!isPhone.test(telValue)){
		tipText.attr("class","item-con_error");
		tipText.html("电话号码无效或存在特殊字符");
		return false;
	}
	var config=true;
	$.ajax({
		url: contextPath + "/p/promoter/isPhoneExist", 
		data: {"Phone":telValue},
		type:'post', 
		async : false, //默认为true 异步   
		success:function(retData){
		 	if('true' == retData){
		 		tipText.attr("class","item-con_error");
				tipText.html("此号码已存在!");
				telInput.css('color','#000');
				telInput.attr("class","text highlight2");
				config=false;
				return false;
			}
		}
	});
	if(!config){
		return false;
	}
	tipText.attr("class","con_succeed");
	tipText.html("");
	return true;
}


//检查短信验证码非空
function checkErifyCode(){
	var erifyValue = jQuery("#erifyCode").val();
	var erifyInput = $("#erifyCode");
	var tipText = erifyInput.parent().find("label");
	if(isBlank(erifyValue)){
		tipText.attr("class","item-con_error");
		tipText.html("短信校验码不能为空");
		erifyInput.attr("class","text-1 text highlight2");
		return false;
	}
	tipText.attr("class","con_succeed");
	tipText.html("");
	return true;
}

/**图形验证码非空**/
function checkedRandNum(){
	var randNumValue = $("#randNum").val();
	var randNumInput = $("#randNum");
	var tipText = randNumInput.parent().find("label");
	if(isBlank(randNumValue)){
		tipText.attr("class","item-con_error");
		tipText.html("验证码不能为空");
		return false;
	}
	tipText.attr("class","con_succeed");
	tipText.html("");
	return true;
}

//异步判断图形验证码是否正确
function validateRandNum(contextPath){
	var checkResult = checkedRandNum();
	if(!checkResult){
		 //验证失败，返回
		return checkResult;
	}
	var randNum = $("#randNum").val();
	if(randNum!=null||randNum==''||randNum){
		var result = true;
		var data = {"randNum": randNum};
		 jQuery.ajax({
				url: contextPath + "/validateRandImg", 
				type:'post', 
				data:data,
				async : false, //默认为true 异步   
			    dataType : 'json', 
				success:function(retData){
					result = retData;
					var tipText = $("#randNum").parent().find("label");
					 if(!retData){
					 	changeRandImg(contextPath);
					 	tipText.html("请输入正确验证码！");
					 	tipText.attr("class","item-con_error");
					 	result=false;
					 }else{
						 tipText.attr("class","con_succeed");
						 tipText.html("");
						 result=true;
					 }
				}
			});	 
		 
		 return result;
	}
}



//校验 验证码
function checkRandImage(){
	   var randImage = $('#randImage');
		if (!validateRandNum(contextPath)) {
			inputError(randImage);
		    randImage.focus();
			changeRandImg(contextPath);
			return false;
		} else {
			return true;
		};
}
	
// 校验 验证码（ 输入错误提示）
function inputError(input){
	clearTimeout(inputError.timer);
	var num = 0;
	var fn = function () {
		inputError.timer = setTimeout(function () {
			input.className = input.className === '' ? 'login-form-error' : '';
			if (num === 5) {
				input.className === '';
			} else {
				fn(num ++);
			};
		}, 150);
	};
	fn();
}

	
function changeRandImg(){
     var obj = document.getElementById("randImage") ;
     obj.src = contextPath + "/validCoderRandom\?d=" + new Date().getTime();
}
var wait=60;
function time(btn){
	 $btn = $(btn);
	 if (wait == 0) {
		 $btn.removeAttr("disabled");
		 $btn.val("获取短信校验码");
         wait = 60;
     } else {
    	 $btn.attr("disabled", true);
    	 $btn.val(wait+"秒后重新获取");
         wait--;
         setTimeout(function () {
             time($btn);
         },
         1000)
     }
}

/**发送短信**/
function sendMobileCode() {
	if(checkTel() && checkRandImage()){
		var tel=$("#mobile").val();
		var randNum = $("#randNum").val();
		var data = {"tel": tel,"randNum":randNum};
		jQuery.ajax({
				url: contextPath + "/sendSMSCodeByReg", 
				type:'post', 
				data:data,
				async : false, //默认为true 异步   
			    dataType : 'json', 
				success:function(retData){
					 if(!retData=="OK"){
						 layer.msg('短信发送失败',{icon: 2});
						 $("#codeBtn").val("重新发送");
					 }else{
						 layer.msg('短信发送成功',{icon: 1});
						 	time($("#codeBtn"));
						 }
				}
			});	
	}
}

//检查所在地区
function checkAddress(){
	var provinceid=$("#provinceid").find("option:selected").val();
	var cityid=$("#areaid").find("option:selected").val();
	var areaid=$("#areaid").find("option:selected").val();
	var tipText = $("#provinceid").parent().find("label");
	if(isBlank(provinceid)||isBlank(cityid)||isBlank(areaid)){
		    tipText.html("请选择完整的地区！");
		    tipText.attr("class","item-con_error");
		    return false;
	}
	tipText.attr("class","con_succeed");
	tipText.html("");
	return true;
}

//检查详细地址
function checkUserAdds(){
	var userAdds=$("#userAdds").val();
	var tipText = $("#userAdds").parent().find("label");
	if(isBlank(userAdds)){
		    tipText.html("请填写详细地址！");
		    tipText.attr("class","item-con_error");
		    return false;
	}
	tipText.attr("class","con_succeed");
	tipText.html("");
	return true;
}


/**验证短信验证码并提交表单**/
function submitTable(){
	var errorCounts = true;
	var chkRealName=$("#chkRealName").val();//是否检查真实姓名
	var chkMail=$("#chkMail").val();
	var chkMobile=$("#chkMobile").val();
	var chkAddr=$("#chkAddr").val();
	var realName="";
	var email="";
	var mobile="";
	var verifyCode=""; //短信验证码
	var provinceid="";
	var cityid="";
	var areaid="";
	var userAdds="";
	
	if(chkRealName==1){ //真实姓名
		realName=$("#realName").val();
		if(!checkRealName()){
			errorCounts = false;
		}
	}
	
	if(chkMail==1){ //邮件
		email=$("#email").val();
		if(!checkEmail()){
			errorCounts = false;
		}
	}
	
	if(chkMobile==1){ //手机号码跟验证码
		mobile=$("#mobile").val();
		verifyCode=$("#erifyCode").val();
		if(!checkTel()||!checkErifyCode()){
			errorCounts = false;
		}
	}
	
	if(chkAddr==1){ //地址
		 provinceid=$("#provinceid").find("option:selected").val();
		 cityid=$("#cityid").find("option:selected").val();
		 areaid=$("#areaid").find("option:selected").val();
		 userAdds=$("#userAdds").val();
		 if(!checkAddress()||!checkUserAdds()){
			 errorCounts = false;
		 }
	}
	
	if(!checkedRandNum()||!checkRandImage()){ //图形验证码
		errorCounts = false;
	}
	
	var data={"realName":realName,
			  "userMail":email,
			  "userMobile":mobile,
			  "verifyCode":verifyCode,
			  "provinceid":provinceid,
			  "cityid":cityid,
			  "areaid":areaid,
			  "userAdds":userAdds,
			  "userId":$("#userId").val()
	}
	if(errorCounts){
		$.ajax({
			url: contextPath + "/p/promoter/submitPromoter", 
			data: data,
			type:'post', 
			async : false, //默认为true 异步   
			success:function(retData){
				var data=eval('('+retData+')');
			 	if('errorCode' == data){
			 		   layer.msg('短信验证码不正确！',{icon: 2});
			 		var tipText = $("#erifyCode").parent().find("label");
			 		    tipText.html("短信验证码不正确！");
					    tipText.attr("class","item-con_error");
					    changeRandImg();
					    jQuery("#randNum").val("");
						jQuery("#erifyCode").val("");
						 var tiplabel = jQuery("#randNum").parent().find("label");
						 tiplabel.removeClass("con_succeed");
				}else if('OK' == data){
					window.location.href=contextPath+"/p/promoter/applyPromoter";
				}else{
					layer.msg('申请失败！',{icon: 2});
				}
			}
		});
	}
}	


//方法，判断是否为空
function isBlank(_value){ 
	if(_value==null || _value=="" || _value==undefined || _value=="null"){
		return true;
	}
	return false;
}