var _prodIds = new Array; //用于再次点击选择商品，回显选择的商品
jQuery.validator.addMethod("compareDate", function (value, element, param) {
    var startDate = jQuery(param).val();
    startDate= startDate.replace(/-/g, '/');
    value = $(element).val().replace(/-/g, '/');

    if (startDate == null || startDate == '' || value == null || value == '') {//只要有一个为空就不比较
        return true;
    }
    var date1 = new Date(startDate);
    var date2 = new Date(value);
    return date1.getTime() < date2.getTime();
}, '开始时间不能大于结束时间');

$(function(){
    //限制拼团价格
    $(".tab-shop").on("blur",".mergePrice",function () {
        if (this.value==null || this.value=="" || this.value<=0 || isNaN(this.value)){
            layer.alert("拼团价格不能为空并且打大于0",{icon:2})
            $(this).css("background-color","pink");
            $(this).val("");
            return;
        }
        $(this).css("background-color","white");
    })

	laydate.render({
		   elem: '#startTime',
		   calendar: true,
		   theme: 'grid',
		   type:'datetime',
		   min:'-1',
		   trigger: 'click'
	  });

		laydate.render({
		   elem: '#endTime',
		   calendar: true,
		   theme: 'grid',
		   type:'datetime',
		   min:'-1',
		   trigger: 'click'
	  });

	// 选择商品
	$("#selProd").click(function(){
		 /*art.dialog.open(contextPath+'/s/mergeGroupActivity/selProd?_prodIds='+_prodIds,
		     		{
		     		  id:'selProd',
		     		  title: '选择商品',
		     		  lock:true,
		     		  resize: false,
		     		  width:'850px',
		              height:'470px',
		              close:function(){//关闭按钮
		            	 var prodIds = artDialog.data("prodIds");//数组，alert出来是字符串格式[1,2,3]
		            	 if(isBlank(prodIds)){
		            		 return;
		            	 }
		            	 if(isBlank(_prodIds)){//第一次点击
		            		 _prodIds = prodIds;//赋值，用于回显选择商品
			            	 var url= contextPath+'/s/mergeGroupActivity/getProdToSku';
			            	 $.ajax({
									url: url,
									type: "post",
									data: JSON.stringify(prodIds),
									contentType:"application/json",
									dataType: "html",
									success: function(result) {
										$(".goods-table").html(result);
									}
			            	 });
		            	 }else{//再次修改
		            		 var tempIds = [].concat(prodIds);//复制数组，临时保存商品Ids

		            		 for(var i=0;i<_prodIds.length;i++){//循环父窗口IDs
			            		 var index = prodIds.indexOf(_prodIds[i]);
			            		 if(index>=0){//父窗口有，子窗口也有，移除子窗口需要添加的该ID
			            			 prodIds.splice(index, 1);
			            		 }else{//父窗口有，子窗口没有，删除父窗口对应的商品ID
			            			 $(".goods-table").find(".first-leve[data-id='"+_prodIds[i]+"']").nextUntil(".first-leve").remove();
			            			 $(".goods-table").find(".first-leve[data-id='"+_prodIds[i]+"']").remove();
			            		 }
			            	 }

		            		 _prodIds = tempIds;//先判断后【用于数据保存html】，在重新赋值，用于回显选择商品

		            		 if(isBlank(prodIds)){
			            		 return;
			            	 }
			            	 var url= contextPath+'/s/mergeGroupActivity/getProdToSkuList';
			            	 $.ajax({
									url: url,
									type: "post",
									data: JSON.stringify(prodIds),
									contentType:"application/json",
									dataType: "json",
									success: function(result) {
										var _str="";
										for(var i=0;i<result.length;i++){	//循环商品
											_str = _str+'<tr class="first-leve" data-id="'+result[i].prodId+'">'+
															'<td width="40"><div class="goods-checkbox"><input type="checkbox" class="checkedProd"></div></td>'+
												            '<td width="110">'+
												                '<div class="goods-img">'+
												               ' <span class="switch open">-</span>'+
												                '<img src="'+photoPath+result[i].prodPic+'" alt="">'+
												                '</div>'+
												            '</td>'+
												            '<td width="200"><div class="goods-name">'+result[i].prodName+'</div></td>'+
												            '<td width="110">'+result[i].cash+'</td>'+
												            '<td width="110">'+result[i].stocks+'</td>'+
												            '<td width="100"></td>'+
												           '<td width="60"><a href="javascript:void(0)" class="del" data-id="'+result[i].prodId+'">删除</a></td>'+
												        '</tr>';
											for(var j=0;j<result[i].dtoList.length;j++){	//循环sku
												if(isBlank(result[i].dtoList[j].cnProperties)){
													result[i].dtoList[j].cnProperties="默认规格"
												}
												_str = _str+ '<tr class="second-leve" data-id="'+result[i].prodId+'">'+
													            '<td width="40"><div class="goods-checkbox"></div></td>'+
													           '<td width="110">'+
													                '<div class="goods-img">'+
													            		'<input type="checkbox" class="second-check checkedSku" data-prodId="'+result[i].prodId+'">'+
													            			'<img src="'+photoPath+result[i].dtoList[j].skuPic+'" alt="">'+
													                '</div>'+
													            '</td>'+
													            '<td width="200"><div class="goods-name">'+result[i].dtoList[j].cnProperties+'</div></td>'+
													            '<td width="110">'+result[i].dtoList[j].skuPrice+'</td>'+
													            '<td width="110">'+result[i].dtoList[j].skuStocks+'</td>'+
													            '<td width="100">'+
													            	'<input type="text" class="set-input mergePrice" value="'+result[i].dtoList[j].skuPrice+'">'+
													            	'<input type="hidden" class="mergeProdId" value="'+result[i].prodId+'">'+
													            	'<input type="hidden" class="mergeSkuId" value="'+result[i].dtoList[j].skuId+'">'+
													            	'<input type="hidden" class="mergeSkuPrice" value="'+result[i].dtoList[j].skuPrice+'">'+
													            '</td>'+
													            '<td width="60"></td>'+
													        '</tr>';
											}
										}
										$(".prodTable").append(_str);
									}
			            	 });

		            	 }
		              }
		     		}
		     );*/

		layer.open({
			title :"选择商品",
			id: "showProdlist",
			type: 2,
			resize: false,
			content: [contextPath+"/s/mergeGroupActivity/mergeGroupProdLayout",'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
			area: ['850px', '700px']
		});

	});

	userCenter.changeSubTab("mergeGroupManage"); //高亮菜单
	$("input[type='file']").imgPreview({
		maxSize:2*1024*1024

	});
	$("input[name='file']").change(function(){
		$(".addphoto").remove();
		$(".img-preview").css("opacity",1);

		$(".fileTip").css("display","none");
	});




	//批量修改价格
	$("#confim").click(function(){
		var _val = $("#batchPrice").val();

        if (isNaN(_val)||_val<=0) {
            layer.alert("拼团价必须大于0",{icon:0})
            $("#batchPrice").val("");
            return;
        }

        if($(".sku").size()<=0){
            layer.alert("请选择商品",{icon:0});
            return;
        }
		if(isBlank(_val)){
            layer.alert("请选择输入拼团价",{icon:0});
			return;
		}

        if($(".selectOne:checked").length<=0){
            layer.alert("请勾选商品",{icon:0});
            return;
        }

        $(".selectOne:checked").each(function(index,element){
			if($(element).is(':checked')){//选中，改变价格
				$(element).parents(".sku").find(".mergePrice").val(_val);
			}
		})
		$("#batchPrice").val("");
	});


	//限购
	$(".limit").click(function(){
		$(this).addClass("on");
		$(".nolimit").removeClass("on");
		$("#isLimit").val("true");
		var str='<span class="act-des">每人最多购买</span>'+
					'<input type="text" name="limitNumber" id="limitNumber" class="limit-input">'+
				'<span class="act-des">件</span>';
		$(".number").append(str);
		$("input[name='limitNumber']").rules("add",{required:true,messages: {required:"必填"} });
		$("input[name='limitNumber']").rules("add",{maxlength:2,messages: {maxlength:"最多输入两位数"} });
		$("input[name='limitNumber']").rules("add",{digits:true,messages: {digits:"请输入整数"} });
		$("input[name='limitNumber']").rules("add",{range:[1,99999],messages: {range:"输入值在2-99"} });
	})

	//不限购
	$(".nolimit").click(function(){
		$(this).addClass("on");
		$(".limit").removeClass("on");
		$("#isLimit").val("false");
		$(".number").html("");
	})

	//团长不免单
	$(".noFree").click(function(){
		$(this).addClass("on");
		$(".free").removeClass("on");
		$("#isHeadFree").val("false");
	})

	$(".free").click(function(){
		$(this).addClass("on");
		$(".noFree").removeClass("on");
		$("#isHeadFree").val("true");
	})


	$("#mergeForm").validate({
		rules: {
			mergeName: {
				required: true,
				minlength:2,
				maxlength: 25
			},
			startTime:{
				required: true
			},
			endTime:{
				required: true,
                compareDate: "#startTime"
			},
			peopleNumber:{
				required: true,
				digits:true,
				range:[2,99]
			}
		},
		messages: {
			mergeName: {
				required: "请填写活动名称",
				minlength:"不能小于2个字符",
				maxlength: "不能超过25个字符"
			},
			startTime:{
				required: "请选择活动时间"
			},
			endTime:{
				required: "请选择活动时间",
                compareDate: "结束日期必须大于开始日期!"

			},
			peopleNumber:{
				required: "请填写成团人数",
				digits:"请输入整数",
				range:"输入值在2-99"
			}
		},
		submitHandler: function(){

		}
	});

})



function saveMergeGroup(){

	$(".tab-shop").find(".mergePrice").each(function(index,element){
		$(element).attr("name","mergeProductList["+index+"].mergePrice");
		$(element).next("input[class='mergeProdId']").attr("name","mergeProductList["+index+"].prodId");
		$(element).next().next("input[class='mergeSkuId']").attr("name","mergeProductList["+index+"].skuId");
		$(element).next().next().next("input[class='mergeSkuPrice']").attr("name","mergeProductList["+index+"].skuPrice");
		$(element).rules("add",{required:true,messages: {required:"必填"} });
		$(element).rules("add",{number:true,messages: {number:"请输入数字"} });
		$(element).rules("add",{maxlength:8,messages: {maxlength:"最多输入八位数"} });
		$(element).rules("add",{range:[1,99999],messages: {range:"输入值在1-99999"} });
	});

	var validate=$("#mergeForm").valid();
	if(validate){
		var file = $("#file").val();
        $("#file").attr("name","file");
        if (photo!=null && photo!="" && $(".add-img").attr("src")!="" && $("#file").attr("src")==""){
            $("#file").attr("name","");
        }
		if (isBlank(file)&&photo==""||photo!="" && $(".add-img").attr("src")=="") {
			$(".fileTip").css("display", "block");
			return;
		}
		if($(".sku").size()<=0){
			// art.dialog.tips("请选择商品");
			layer.alert("请选择商品",{icon:0});
			return;
		}

        var time = new Date($("#startTime").val().replace(/-/g,"/"));

        //开始时间不能小于等于当前时间
        if(time.getTime() <= new Date().getTime()){
            layer.alert("活动开始时间不能小于当前时间!", {icon:0});
            return false;
        }


        if(!checkImgType($("#file"))){
			return false;
		}
		if(!checkImgSize($("#file"), 2048)){
			return false;
		}
		var formData = new FormData($("#mergeForm")[0]);
		var url = contextPath + "/s/mergeGroupActivity/saveCommit";
		$.ajax({
			url: url,
			data: formData,
			type: "POST",
			dataType: "json",
			processData: false,
		    contentType: false,
			success: function(result) {
				if(result.indexOf("成功")!=-1) {
                    layer.alert(result,{icon:1},function () {
                        location.href=contextPath + "/s/mergeGroupActivity/query";
                    })
                }else{
					art.dialog.tips(result);
				}
			}
			,
			beforeSend : function(xhr) {
			   layer.load(2, { shade: [0.01, '#000'] });
			},
			complete : function(xhr, status) {
			   layer.closeAll('loading');
			}
		});
	}

}

function checkPrice(e) {
	var str = $(e).val();

	 if(isBlank(str)){
		return false;
	}
	if(isNaN(str)){ //判断是否是数字
		$(e).val('');
		$(e).focus();
		return;
	}

	if(str<=0){ //判断数字是否是负数
		str = 0.01;
	}
	if(String(str).indexOf(".")>-1){//是小数
		str = toDecimal(str);
	}
	var a=/^(([1-9][0-9]*)|(([0]\.\d{1,2}|[1-9][0-9]*\.\d{1,2})))$/;//校检金额
	if(!a.test(str)){
		str = 0.01;
	}
	if(parseInt(str)>100000){
		str=99999;
	}
	$(e).val(str);
}

//格式化小数
function toDecimal(x) {
   var f = parseFloat(x);
   if (isNaN(f)) {
       return;
   }
   f = Math.round(x*100)/100;
   return f;
 }


//方法，判断是否为空
function isBlank(_value){
	return _value==null || _value=="" || _value==undefined;
}
//全选
$(".tab-shop").on("click",".selectAll",function(){
    if($(this).attr("checked")=="checked"){
        $(this).parent().parent().addClass("checkbox-wrapper-checked");
        $(".selectOne").each(function(){
            $(this).attr("checked",true);
            $(this).parent().parent().addClass("checkbox-wrapper-checked");
        });
    }else{
        $(".selectOne").each(function(){
            $(this).attr("checked",false);
            $(this).parent().parent().removeClass("checkbox-wrapper-checked");
        });
        $(this).parent().parent().removeClass("checkbox-wrapper-checked");
    }
});

// 单选
function selectOne(obj){
    if(!obj.checked){
        $(".selectAll").checked = obj.checked;
        $(obj).prop("checked",false);
        $(obj).parent().parent().removeClass("checkbox-wrapper-checked");
    }else{
        $(obj).prop("checked",true);
        $(obj).parent().parent().addClass("checkbox-wrapper-checked");
    }
    var flag = true;
    var arr = $(".selectOne");
    for(var i=0;i<arr.length;i++){
        if(!arr[i].checked){
            flag=false;
            break;
        }
    }
    if(flag){
        $(".selectAll").prop("checked",true);
        $(".selectAll").parent().parent().addClass("checkbox-wrapper-checked");
    }else{
        $(".selectAll").prop("checked",false);
        $(".selectAll").parent().parent().removeClass("checkbox-wrapper-checked");
    }
}

