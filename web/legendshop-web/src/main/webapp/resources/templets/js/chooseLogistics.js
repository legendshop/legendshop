function fahuo(){
	var returnId=$("#returnId").val();
	if(isBlank(returnId)){
		layer.msg("请刷新重新选择",{icon:0});
		return ;
	}

	var dvyFlowId=$.trim($("#courierNumber").val());
	if(dvyFlowId==undefined || dvyFlowId==null || dvyFlowId==""){
		layer.msg("请输入物流单号",{icon:0});
		return ;
	}

	var courierId=$("#courierId").find("option:selected").val();
	var courierCompany=$("#courierId").find("option:selected").attr("company");
	if(courierId==undefined || courierId==null || courierId==""){
		layer.msg("请选择物流公司",{icon:0});
		return ;
	}

	$.ajax({
		url: contextPath+"/p/saveLogistics" , 
		data: {"id":returnId,"logisticsOrderCode":dvyFlowId,"logisticsCompany":courierCompany},
		type:'post', 
		dataType : 'json', 
		async : true, //默认为true 异步   
		success:function(result){
			if(result == 'OK'){
				parent.location.reload();
			}else{
				layer.alert('保存失败,请稍后重试！');
			}
		}
	});
}

function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}