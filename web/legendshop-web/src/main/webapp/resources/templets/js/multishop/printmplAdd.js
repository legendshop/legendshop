$("input[id='bgimageFile']").change(function(){
	checkImgType(this);
	checkImgSize(this,1024);
});

$(document).ready(function() {
	userCenter.changeSubTab("transportPrintTmpl");
	jQuery("#form1").validate({
		errorPlacement: function(error, element) {
			element.parent().find("em").html("");
			error.appendTo(element.parent().find("em"));
		},
		rules: {
			prtTmplTitle: {
				required : true,
				maxlength : 10
			},
			prtTmplWidth: {
				required : true,
				digits: true 
			},
			prtTmplHeight: {
				required : true,
				digits: true 
			},
			bgimageFile: {
				required : true,
				/*  isImg:true */
			}
		},
		messages: {
			prtTmplTitle: {
				required : "模板名称不能为空",
				maxlength : "模板名称最多10个字" 
			},
			prtTmplWidth: {
				required : "宽度不能为空",
				digits: "宽度必须为数字"
			},
			prtTmplHeight: {
				required : "高度不能为空",
				digits: "高度必须为数字"
			},
			bgimageFile: {
				required : "图片不能为空",
				/* isImg:"仅支持JPG、GIF、PNG、JPEG、BMP格式，请重新选择！" */
			}
		}
	});
});

function sub(form){  
	if(jQuery(form).valid()){  
		jQuery("#submit").attr("disabled", "disabled");  
	}  
	return true;  
}  


