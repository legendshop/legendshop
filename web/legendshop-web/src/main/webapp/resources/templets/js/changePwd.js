jQuery(document).ready(function() {
		$("#password").bind('keyup onfocus onblur', function () {
			  $("#pwdstrength").show();
			  var index = checkStrong(this.value);
			  $("#pwdstrength").removeClass("strengthA");
			  $("#pwdstrength").removeClass("strengthB");
			  $("#pwdstrength").removeClass("strengthC");
			  var  letter = ( index == 0? 0:index == 1? 'A':index == 2? 'B':'C') ;
			  if(letter==0){
				  $("#pwdstrength").hide();
			  }
			$("#pwdstrength").attr("class","strength" + letter );
		});
		
		//密码检测密码强度
		function checkStrong(sValue) {
		    var modes = 0;
		    //正则表达式验证符合要求的
		    if (sValue.length < 1) return modes;
		    if (/\d/.test(sValue)) modes++; //数字
		    if (/[a-z]/.test(sValue)) modes++; //小写
		    if (/[A-Z]/.test(sValue)) modes++; //大写  
		    if (/\W/.test(sValue)) modes++; //特殊字符
		   
		   //逻辑处理
		    switch (modes) {
		        case 1:
		            return 1;
		            break;
		        case 2:
		            return 2;
		        case 3:
		        case 4:
		            return sValue.length < 12 ? 3 : 4;
		            break;
		    }
		}

	});
	
	$("#password").bind('focus', function () {
		//var pwError = $("#password_error");
		 $("#password").attr("class","text highlight1");
		 $("#password_error").hide();
		 //pwError.attr("class","msg-text");
		 //pwError.text("由字母加数字或符号至少两种以上字符组成的6-20位半角字符，区分大小写。");
	});
	
	$("#password").bind('blur', function () {
		var pwValue = $("#password").val();
		var error = $("#password_error");
		 if(pwValue.length>0 && pwValue.length<6){
			 $("#password").attr("class","text highlight2");
			 error.show();
			 error.attr("class","msg-error");
			 error.text("密码长度不正确，请重新设置");
		 }else{
			 $("#password").attr("class","text");
			 error.hide();
		 }
	});
	
	$("#repassword").bind('focus',function(){
		$("#repassword").attr("class","text highlight1");
	    $("#repassword_error").hide();
	});
	
	$("#repassword").bind('blur', function () {
		var rpwValue = $("#repassword").val();
		var pwValue = $("#password").val();
		var error = $("#repassword_error");
		if(rpwValue.length != 0 && rpwValue != pwValue){
				$("#repassword").attr("class","text highlight2");
				error.show();
				error.attr("class","msg-error");
				error.text("两次输入的密码不一致，请重新输入");
		}else{
			$("#repassword").attr("class","text");
			error.hide();
		}
	});
