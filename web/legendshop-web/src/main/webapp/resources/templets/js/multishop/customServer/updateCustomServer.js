$(document).ready(function () {
	userCenter.changeSubTab("CustomServer"); //高亮菜单
});

$.validator.addMethod("password", function(value) {
	var regx = /(?!.*[\u4E00-\u9FA5\s])(?!^[a-zA-Z]+$)(?!^[\d]+$)(?!^[^a-zA-Z\d]+$)^.{6,15}$/;
	if (value === "") {
		return true;
	}
	if (!regx.test(value)) {
		return false;
	}
	return true;
});

$("#form").validate({
	rules : {
		name : {
			required : true
		},
		account : {
			required : true
		},
		password : {
			password : 0
		},
		frozen : {
			required : true
		}
	},
	messages : {
		name : {
			required : "客服名称不能为空"
		},
		account : {
			required : "客服账号不能为空"
		},
		password : {
			password : "密码由6-15位字母、数字或符号的两种及以上组成！"
		},
		frozen : {
			required : "属性不能为空"
		}
	},
	submitHandler : function() {
		$.ajax({
			url : "../save",
			type : "POST",
			dataType : "json",
			data : $("#form").serialize(),
			success : function(result) {
				if (result === "ok") {
					art.dialog.tips("修改成功");
					setTimeout(function() {
						window.location.href = contextPath + "/s/customServer";
					}, 1000)
				} else {
					art.dialog.tips(result);
				}
			}
		});
	}
});
