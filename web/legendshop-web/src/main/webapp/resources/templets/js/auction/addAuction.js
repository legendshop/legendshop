var skuId=-1;
function showProdlist(){
	layer.open({
  		  title :"选择商品",
  		  id: "actionShowProdlist",
  		  type: 2,
  		  resize: false,
  		  content: [contextPath+"/s/auction/AuctionProdLayout",'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
  		  area: ['850px', '660px']
  		});

}

jQuery.validator.addMethod("isNumber", function(value, element) {
         return this.optional(element) || /^[-\+]?\d+$/.test(value) || /^[-\+]?\d+(\.\d+)?$/.test(value);
    }, "必须整数或小数");

jQuery.validator.addMethod("isMoney", function(value, element) {
    return this.optional(element) || /^\d+(\.\d{1,2})?$/.test(value);
}, "请输入两位小数以内的价格");

jQuery.validator.addMethod("isFloatGtZero", function(value, element) {
         value=parseFloat(value);
         return this.optional(element) || value>=1;
    }, "必须大于等于1元");

//jQuery.validator.addMethod("checkMinVal", function(value, element) {
//	var floor =parseFloat($("#floorPrice").val());
//	var fixed =parseFloat($("#fixedPrice").val());
//	var newValue=parseFloat(value);
//  return this.optional(element) || floor+newValue<fixed;  
// }, "起拍价与最低加价之和不可超过一口价");
//
//jQuery.validator.addMethod("checkFixVal", function(value, element) {
//	var floor =parseFloat($("#floorPrice").val());
//	var newValue=parseFloat(value);
//  return this.optional(element) || floor<newValue;  
// }, "必须大于起拍价");

jQuery.validator.addMethod("compareDate", function (value, element, param) {
    var startDate = jQuery(param).val();
    startDate= startDate.replace(/-/g, '/');
    value = $(element).val().replace(/-/g, '/');

    if (startDate == null || startDate == '' || value == null || value == '') {//只要有一个为空就不比较
        return true;
    }
    var date1 = new Date(startDate);
    var date2 = new Date(value);
    return date1.getTime() < date2.getTime();

}, '结束时间必须大于开始时间');

jQuery.validator.addMethod("checkMaxVal01", function(value, element) {
		var min =parseFloat($("#minMarkupRange").val());
		var newValue=parseFloat(value);
    	var fixedPrice=parseFloat($("#fixedPrice").val());
    	return this.optional(element) || newValue>min;
     }, "必须大于最小金额");

jQuery.validator.addMethod("checkMaxVal02", function(value, element) {

    // 是否选择封顶
    var isCeilingSelect = $("select[name='isCeilingSelect'] option:selected").val();

    if(0 == isCeilingSelect){//不封顶
      return true;
    }

    var newValue=parseFloat(value);
    var fixedPrice=parseFloat($("#fixedPrice").val());
    var rs=true;
    if (!isNaN(fixedPrice)) {
        rs = newValue<fixedPrice?true:false;
    }
    return this.optional(element) || rs;
}, "必须小于一口价");

jQuery.validator.addMethod("checkMinVal", function(value, element) {

    // 是否选择封顶
    var isCeilingSelect = $("select[name='isCeilingSelect'] option:selected").val();

    if(0 == isCeilingSelect){//不封顶
      return true;
    }

    var fixedPrice=parseFloat($("#fixedPrice").val());
    var newValue=parseFloat(value);
    var rs=true;
    if (!isNaN(fixedPrice)) {
		rs = newValue<fixedPrice?true:false;
	}
    return this.optional(element) || rs;
}, "必须小于一口价");

jQuery.validator.addMethod("checkMoney", function(value, element) {
	var min =parseFloat($("#floorPrice").val());
	var newValue=parseFloat(value);
  return this.optional(element) || newValue>min;
 }, "一口价必须大于起拍价");

    $(document).ready(function() {
    	if (auctions==null || auctions==""){
            $("#isCeiling").val(0);
            $(".fixedPrice").hide(300);
            $("#fixedPrice").addClass("ignore");

            $("#isSecurity").val(1);
            $(".securityPrice").show(300);
            $(".delayTime").show(300);
            $("#securityPrice").removeClass("ignore");
            $("#delayTime").removeClass("ignore");

            $("#hideFloorPrice").val(0);
		}


	    jQuery("#form1").validate({
	    ignore: ".ignore",
	    errorPlacement: function(error, element) {
	    	element.parent().find("em").html("");
			error.appendTo(element.parent().find("em"));
		},
	            rules: {
	            auctionsTitle: {
	                required: true,
	                maxlength:30
	            }, startTime: {
	                required: true
	            },endTime: {
	                required: true,
	                compareDate: "#startTime"
	            },floorPrice: {
	                required: true,
	                isNumber:true,
	                isMoney:true,
	                isFloatGtZero:true
	            },fixedPrice: {
	                required: true,
	                isNumber:true,
	                isMoney:true,
	                isFloatGtZero:true,
	                checkMoney:true
	            },minMarkupRange: {
	                required: true,
	                isNumber:true,
	                isMoney:true,
	                isFloatGtZero:true,
					checkMinVal:true
	            },maxMarkupRange: {
	                required: true,
	                isNumber:true,
	                isMoney:true,
	                isFloatGtZero:true,
	                checkMaxVal01:true,
					checkMaxVal02:true
	            },securityPrice:{
	                required: true,
	                isNumber:true,
	                isMoney:true,
	                isFloatGtZero:true
	            },delayTime:{
	                required: true,
	                digits:true
	            },prodId:{
	            	required: true
	            },isCeilingSelect:{
	            	required: true,min:0
	            },isSecuritySelect:{
	            	required: true,min:0
	            },hideFloorPriceSelect:{
	            	required: true,min:0
	            }
	        },
	        messages: {
	            auctionsTitle: {
	                required: "请输入活动名称",
	                maxlength:"活动名称最多30个字符"
	            },startTime: {
	                required: "请输入开始时间"
	            },endTime: {
	                required: "请输入结束时间"
	            },floorPrice: {
	                required: "请输入起拍价",
	            },fixedPrice: {
	                required: "请输入一口价"
	            },minMarkupRange: {
	                required: "请输入最小加价幅度"
	            },maxMarkupRange: {
	                required: "请输入最大加价幅度"
	            },securityPrice:{
	                required: "请输入保证金"
	            },delayTime:{
	                required: "请输入延长时间(分钟)",
	                digits:"必须为整数"
	            },prodId:{
	            	required: "商品不能为空"
	            },isCeilingSelect:{
	            	required:"请选择",
	            	min:"请选择"
	            },isSecuritySelect:{
	            	required:"请选择",
	            	min:"请选择"
	            },hideFloorPriceSelect:{
	            	required:"请选择",
	            	min:"请选择"
	            }
	        },

	        submitHandler: function (form) {
	    		if ($(".first").size()<=0){
	    			layer.alert("请先选择商品",{icon:0})
					return;
				}
                var time = new Date($("#startTime").val().replace(/-/g,"/"));

                //开始时间不能小于等于当前时间
                if(time.getTime() <= new Date().getTime()){
                    layer.alert("活动开始时间不能小于当前时间!", {icon:0});
                    return false;
                }

				var isCeiling= $('.isCeiling option:selected') .val();
				var isSecurity= $('.isSecurity option:selected') .val();
				var hideFloorPrice = $('.hideFloorPrice option:selected').val();

	        	if(!isBlank(isCeiling)&&!isBlank(isSecurity)&&!isBlank(ishideFloorPrice)){
	        		$("#isCeiling").val(isCeiling);
	        		$("#isSecurity").val(isSecurity);
	        		$('#hideFloorPrice').val(hideFloorPrice);
	        		form.submit();
	        	}
	        },

	    });


});
    $(".isCeiling").change(function(){
    	var s=$(this).val();
    	if(s==1){
    		$("#isCeiling").val(s);
    		$(".fixedPrice").show(300);
    		$("#fixedPrice").removeClass("ignore");
    		$("#fixedPrice").val("");
    	}else{
    		$("#isCeiling").val(s);
    		$(".fixedPrice").hide(300);
    		$("#fixedPrice").addClass("ignore");
    	}
    });

    $(".isSecurity").change(function(){
    	var s=$(this).val();
    	if(s==0){
    		$("#isSecurity").val(s);
    		$(".securityPrice").hide(300);
    		$("#securityPrice").addClass("ignore");
    		$("#delayTime").addClass("ignore");
    		$("#securityPrice").val("");
    		$("#delayTime").val("");

    	}else{
    		$("#isSecurity").val(s);
    		$(".securityPrice").show(300);
    		$("#securityPrice").removeClass("ignore");
    		$("#delayTime").removeClass("ignore");
    	}
    });

    $(".hideFloorPrice").change(function(){
    	var s=$(this).val();
    	$("#hideFloorPrice").val(s);
    });
    KindEditor.options.filterMode=false;
    var editor =KindEditor.create('textarea[name="auctionsDesc"]', {
		cssPath : paramData.contextPath + '/resources/plugins/kindeditor/plugins/code/prettify.css',
		uploadJson : paramData.contextPath + '/editor/uploadJson/upload;jsessionid=' + paramData.cookieValue,
		fileManagerJson : paramData.contextPath + '/editor/uploadJson/fileManager',
		allowFileManager : true,
		afterBlur:function(){this.sync();},
		width : '680px',
		height:'350px',
		afterCreate : function() {
			var self = this;
			KindEditor.ctrl(document, 13, function() {
				self.sync();
				document.forms['example'].submit();
			});
			KindEditor.ctrl(self.edit.doc, 13, function() {
				self.sync();
				document.forms['example'].submit();
			});
		}
	});



    function addProdTo(prodId,skuId,skuName,skuPrice,cnProperties){
    	$(".prodInfo").css("display","");
    	if(name.length>50){
    		name=name.substring(0,50)+'…';
    	}
    	$("#prodId").val(prodId);
    	$("#skuId").val(skuId);
    	$("#prodId").next().css("display","none");

    	$("#skuName").text(skuName);
    	$("#skuPrice").text(skuPrice);
    	if(isBlank(cnProperties)){
    		$("#cnProperties").text("无");
    	}else{
    		$("#cnProperties").text(cnProperties);
    	}

    }

	function removeProd(){
		$("#prodId").val("");
		$("#skuId").val("");
		$(".prodInfo").css("display","none");
	}


	function isBlank(_value){
		if(_value==null || _value=="" || _value==undefined){
			return true;
		}
		return false;
	}
/*
	function show(obj){
		var val= $(obj).val();
		if(val==1){
			$("#securityPrice").removeClass("ignore");
			$("#delayTime").removeClass("ignore");
			$(".securityPrice").css("display","");
	    	$(".delayTime").css("display","");
		}else{
			$("#securityPrice").addClass("ignore");
			$("#delayTime").addClass("ignore");
			$("#securityPrice").val("");
			$("#delayTime").val("");
			$(".securityPrice").css("display","none");
	    	$(".delayTime").css("display","none");
		}
	}
	
	$(".isCeiling").click(function(){
		  if($(this).is(':checked')){
		  	$(".onePrice").css("display","none");
		  	$("#fixedPrice").val("");
		  	$("#fixedPrice").addClass("ignore");
		  	$("#isCeiling").val(1);
		  }else{
		  	$(".onePrice").css("display","");
		  	$("#fixedPrice").removeClass("ignore");
		  	$("#isCeiling").val(0);
		  }
	});
	
	function addProdTo(skuid,name,prodid){
			$(".prodInfo").css("display","");
			$("#prodName").text(name);
			$("#prodName").attr("href","${pageContext.request.contextPath}/views/"+prodid);
			$("#prodId").val(prodid);
			$("#skuId").val(skuid);
			$("#prodId").next().css("display","none");
			$("#remove").show();
			
	}
	
	function removeProd(){
		$("#prodId").val("");
		$("#skuId").val("");
		$(".prodInfo").css("display","none");
	}*/

function addProdTo(skuId){
    var url=contextPath+"/s/auction/querySkuInfo?skuId="+skuId;
    // 加载商品列表回显到页面
    $(".prodList").load(url, function(){
    	layer.closeAll();
    });
}
