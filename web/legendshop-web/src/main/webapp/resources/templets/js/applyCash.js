var isSubmitted = false;

$(document).ready(function() {
  userCenter.changeSubTab("myCommis");
  $("select.combox").initSelect();

   //表单校验
   $("#form1").validate({
		ignore: ".ignore",
        rules: {
            money: {
                required : true,
                isNumber : true,
                isValidMoney : true,
                isValidMoney2 : true
            },
            passwd : {
            	required : true,
            }
    },
    messages: {
            money: {
                required : true,
                isNumber : "请输入正确的金额!",
                isValidMoney : "金额必须大于0!"
            },
            passwd : {
            	required : "请输入您的密码!",
	        }
    },
    submitHandler : function(form){
     	 $.ajax({
       		url : contextPath + "/p/promoter/applyCash",
       		type : "POST",
       		data : $(form).serialize(),
 			dataType : "JSON",
 			beforeSend : function(xhr){
 				$("#withdrawBtn").attr("disabled",true);
 				$("#withdrawBtn").css({"background":"#aaa","color":"#fff"});
 			},
 			complete : function(xhr,status){
 				$("#withdrawBtn").attr("disabled",false);
 				$("#withdrawBtn").removeAttr("style");
 			},
 			success : function(result,status,xhr){
 				if(result.status == "OK"){
 					layer.alert(result.msg,{icon:1},function(){
 						window.location.href = contextPath + "/p/promoter/withdrawList";
 					});
 				}else{
 					layer.msg(result.msg,{icon:0});
 					$("#passwd").val("");
 				}
 			}

       	 });
    }

  });
 });

jQuery.validator.addMethod("isNumber", function(value, element) {
    return this.optional(element) || /^[-\+]?\d+$/.test(value) || /^[-\+]?\d+(\.\d+)?$/.test(value);
}, "必须整数或小数");

jQuery.validator.addMethod("isValidMoney", function(value, element) {
    value = parseFloat(value);
    return this.optional(element) || value> 0;
}, "提现金额必须要大于0元");

jQuery.validator.addMethod("isValidMoney2", function(value, element) {
    value = parseFloat(value);
    return this.optional(element) || value <= settledDistCommis;
}, "提现金额不能超过: " + settledDistCommis + "元");
