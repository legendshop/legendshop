Date.prototype.format = function(format) {
    var o = {
        "MM+" : this.getMonth() + 1, // month
        "dd+" : this.getDate(), // day
        "hh+" : this.getHours(), // hour
        "mm+" : this.getMinutes(), // minute
        "ss+" : this.getSeconds(), // second
        "q+" : Math.floor((this.getMonth() + 3) / 3), // quarter
        "S" : this.getMilliseconds()
    // millisecond
    };
    if (/(y+)/.test(format))
        format = format.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
    for ( var k in o)
        if (new RegExp("(" + k + ")").test(format))
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
                    : ("00" + o[k]).substr(("" + o[k]).length));
    return format;
}
var validCoder=false;


jQuery(document).ready(function($) {



	$("#randImage").on('click', function () {
		changeRandImg();
	});
});


function exposeUrl(){
     return contextPath+"/seckill/"+seckillId+"/exposeUrl";
}

function  executeUrl(md5){
    return contextPath+"/seckill/"+seckillId+"/"+md5+"/execute";
}

function nowTime() {

  return contextPath + "/seckill/time/now";
}



//初始化检查
function initCheck(){
	 $.get(nowTime(),{},function(result){  //获取系统的时间
	   console.log(result);
	   console.log(endTime);
		 if(result.isSuccess){
			 var nowTime = result["data"];
			 if(nowTime>=endTime){  //秒杀结束
				 $("#skuStock_div").hide();
				 endSeck();
			 }else if(nowTime<startTime){  //距离活动开始时间
				 showSeckill();
                 //倒计时处理
                 countdown(seckillId,startTime);
             }
			 else{ //活动进行中
				 if(allSelected){
					 startSeck();
	            	 showSeckill();
				 }
				 finishCountdown();
            	 _switch=1;
             }
		 }
	 });
}

function countdown (seckillId,startTime){
    var date=new Date(startTime);
    $('#seckill_ready .pm-status-time').countdown(date,function(event) {
        $(this).html(event.strftime('<span><em>%D</em>天<em>%H</em>时<em> %M</em>分<em> %S</em>秒</span>'));
    }).on('finish.countdown',function(){
    	 _switch=1;
    	 if(allSelected){
        	 showSeckill();
		 }
    	 startSeck();
    	 finishCountdown();
    });
}

function finishCountdown(){
	var date=new Date(endTime);
	$('#seckill_starting .pm-status-time').countdown(date).on('update.countdown', function(event) {
	     $(this).html(event.strftime('<span><em>%D</em>天<em>%H</em>时<em> %M</em>分<em> %S</em>秒</span>'));
    }).on('finish.countdown', function(event) {
      	//活动时间到
    	_switch=0;
    	endSeck();
    });
}


function endSeck(){
	  var str=new Date(endTime).format('<span><em>MM</em>月<em><em>dd</em>日<em>hh</em>时<em>mm</em>分<em>ss</em>秒</span>');
	  $("#seckill_end .pm-status-time span").html(str);
	  $("#but_seck_end").show();
	  $("#seckill_end").show();
	  $("#seckill_starting").hide();
	  $("#seckill_start").hide();
	  $('#seckill-btn').show();
	  $('#seckill-btn').attr("value","已结束").css({backgroundColor:"#999"}).attr("disabled","disabled");
}

function readyStartSeck(){
	  $("#but_seck_ready").show();
	  $("#seckill_ready").show();
	  $("#seckill_starting").hide();
	  $("#seckill_end").hide();
	  $('#seckill-btn').show();
	  $('#seckill-btn').attr("value","准备开始").css({backgroundColor:"#999"}).attr("disabled","disabled");
}

function startSeck(){
	  $("#but_seck").show();
	  $("#seckill_starting").show();
	  $("#seckill_end").hide();
	  $("#seckill_ready").hide();
	  $('#seckill-btn').show();
	  $('#seckill-btn').attr("value","立即抢购").css({backgroundColor:"#c6171f"}).removeAttr("disabled","disabled");
}


function sellOut(){
	$('#seckill-btn').attr("value","已售罄").css({backgroundColor:"#999"}).attr("disabled","disabled");
}

function showSeckill(){
	if(!allSelected){
		return;
	}
	var data={"prodId":prodId,"skuId":currSkuId};
	$.ajax({
        //提交数据的类型 POST GET
        type:"POST",
        //提交的网址
        url:exposeUrl(),
        data:data,
        //提交的数据
        async : false,
        //返回数据的格式
        datatype: "json",
        //成功返回之后调用的函数
		success : function(data) {
			var result=jQuery.parseJSON(data);
        	var isSuccess=result.isSuccess;
        	var stock=result.stock;
        	if(isSuccess){
        		 var data=result["data"];
                 var isExpose=data["isExpose"];
                 if(isExpose){
                      executeSeckill(data["md5"]);
                 }
                 $("#skuStock").html(stock);
        	}
        	else{
        		var status=result.status;
        		if(status==-2){
        			layer.alert("系统繁忙,请稍候！", {icon: 0});
        			$('#seckill-btn').css({backgroundColor:"#999"}).attr("disabled","disabled");
        		}else if(status==-3){
        			sellOut();
        		}else if(status==-4){
        			$('#seckill-btn').attr("value","活动已下线").css({backgroundColor:"#999"}).attr("disabled","disabled");
        		}
        		else if(status==-5){
        			endSeck();
        		}
        		else if(status==-6){
        			readyStartSeck();
        			$('#seckill-btn').css({backgroundColor:"#999"}).attr("disabled","disabled");
        		}else if(status==-7){
        			$('#seckill-btn').css({backgroundColor:"#999"}).attr("disabled","disabled");
        		}
        		else{
        			var error=result.error;
        			alert(error);
        			$('#seckill-btn').css({backgroundColor:"#999"}).attr("disabled","disabled");
        		}
        		$("#skuStock").html(stock);
        		return;
        	}
		},
        //调用执行后调用的函数
        complete: function(XMLHttpRequest, textStatus){
        }
    });
}
var _md5;
function seckill(md5){
	//判断是否登录
	 if(isLogin()){
		login();
		return;
	 }

	 //是否校验验证码;
	 if(!validCoder){
		 $(".sec-val").show();
		 $(".black_overlay").show();
		 _md5=md5;
		 return;
	 }
	 $("#paidui").show();
	 $(".black_overlay").show();
	 $('#seckill-btn').css({backgroundColor:"#999"}).attr("disabled","disabled");
	 var _prodId=prodId;
	 var _currSkuId=currSkuId;
	 var _seckillId=seckillId;
	 var data={"prodId":_prodId,"skuId":_currSkuId};
	 $.ajax({
        //提交数据的类型 POST GET
        type:"POST",
        //提交的网址
        url:executeUrl(md5),
        timeout : 10000, //超时时间设置，单位10秒
        data:data,
        //提交的数据
        async : true,
        //返回数据的格式
        datatype: "json",
        //成功返回之后调用的函数
		 success : function(data) {
			var result=jQuery.parseJSON(data);
          	var isSuccess=result.isSuccess;
          if(isSuccess){
        	  executeSuccess(_seckillId,md5,_prodId,_currSkuId);
        	  $('#seckill-btn').css({backgroundColor:"#999"}).attr("disabled","disabled");
        	  return;
           }else{
           	 var status=result.status;
       		if(status==-2){
       			layer.alert("系统繁忙,请稍候！", {icon: 0});
       		}else if(status==-3){
       			hidePaidui();
       			sellOut();
       	        layer.alert("该商品已售罄！", {icon: 0});
       		}else if(status==-4){
       			hidePaidui();
       		}
       		else if(status==-5){
       			hidePaidui();
       			endSeck();
       			layer.alert("活动已经结束", {icon: 0});
       		}
       		else if(status==-6){
       			hidePaidui();
       			readyStartSeck();
       			layer.alert("活动未开始", {icon: 0});
       		}else if(status==4){
       			hidePaidui();
       			login();
       			return;
       		}else if(status==2){
       			hidePaidui();
       			layer.msg("您已经参与了秒杀，快点击确定去付款吧！", {icon: 1,time: 1000},function(){
       				window.location.href=contextPath+"/p/seckill/record"
       			});
       			return;
       		}else{
       			hidePaidui();
       			var error=result.error;
       			layer.alert(error, {icon: 2});
       		}
       		return;
           }

		 },
        //调用执行后调用的函数
        complete: function(XMLHttpRequest, textStatus){
       	  if(textStatus=='timeout'){ //超时,status还有success,error等值的情况
       	  }
        },
        //调用出错执行的函数
        error: function(XMLHttpRequest, textStatus){
       	   if(textStatus=='timeout'){ // 超时,status还有success,error等值的情况
        	 }else{
       		 layer.alert("系统繁忙,请稍候！", {icon: 0});
       	    }
        }
    });
}


function executeSeckill(md5){ //执行秒杀
	startSeck();
	$("#seckill-btn").removeAttr("onclick");
	$('#seckill-btn').attr('onclick',"seckill('"+md5+"');");
}


function  executeSuccess(_seckillId,md5,_prodId,_skuId){
	window.location.href=contextPath+"/p/seckill/order/"+_seckillId+"/"+md5+"/"+_prodId+"/"+_skuId+"/success";
}

function login(){
	layer.open({
		title :"登录",
		  type: 2,
		  content: contextPath + '/loadLoginOverlay', //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['440px', '530px']
		});
}


//得判断是否为数字的同时，也判断其是不是正数
function isLogin() {
	if(isEmpty(loginUserName)){
		return true;
	}
	return false;
}

function changeRandImg(){
    var obj = document.getElementById("randImage") ;
    obj.src = contextPath + "/validCoderRandom\?d=" + new Date().getTime();
}



function hideNum(){
	 $("#randNum").val();
	 $(".sec-val").hide();
	 $(".black_overlay").hide();
}

function hidePaidui(){
	$("#paidui").hide();
	$(".black_overlay").hide();
}

function hideSaleOut(){
	$("#sale-out").hide();
	$(".black_overlay").hide();
}

function validateRandNum(){
	 var checkResult = checkedRandNum();
	 if(!checkResult){
		 validCoder=false;
		 //验证失败，返回
		return;
	 }
	 var randNum = $("#randNum").val();
	 var result = true;
	 jQuery.ajax({
			url: contextPath + "/validateRandImg",
			type:'post',
			data:{"randNum":randNum},
			async : false, //默认为true 异步
		    dataType : 'json',
			success:function(retData){
				result = retData;
				 if(!retData){
				 	changeRandImg();
				 	layer.alert("请输入正确验证码！", {icon: 0});
				 	result=false;
				 	validCoder=false;
				 }else{
					 $(".sec-val").hide();
					 $(".black_overlay").hide();
					 result=true;
					 validCoder=true;
					 seckill(_md5);
				 }
			}
	   });
	return result;
}

function checkedRandNum(){
	var randNumValue = jQuery("#randNum").val();
	var randNumInput = $("#randNum");
	var tipText = randNumInput.parent().find("label");
	if(randNumValue ==null || randNumValue =='' || randNumValue.length<=0 ||randNumValue=='请输入图片中的文字'){
		$("#randNum").attr("style","border:1px solid red;");
		layer.alert("验证码不能为空", {icon: 0});
		return false;
	}
	if(randNumValue.length < 4){
		$("#randNum").attr("style","border:1px solid red;");
		layer.alert("请输入正确的验证码", {icon: 0});
		return false;
	}
	$("#randNum").attr("style","");
	return true;
}
/**
 * 判断是否是空
 * @param value
 */
function isEmpty(value){
	if(value == null || value == "" || value == "undefined" || value == undefined || value == "null"){
		return true;
	}
	else{
		value = (value+"").replace(/\s/g,'');
		if(value == ""){
			return true;
		}
		return false;
	}
}
