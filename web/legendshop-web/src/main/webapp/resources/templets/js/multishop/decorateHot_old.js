/**
 *  Tony 
 */
$(document).ready(function(){
	$(".fiit_hot_tab li").click(function(){
		var tar =$(this).attr("tar");
		$("div[id^=tar_]").hide();
		$("#tar_"+tar).show();
		$(".fiit_hot_tab li").removeClass("this");
		$(this).addClass("this");
		if(tar=="img"){
			 $(".balck_bg").show()
			}else{
			 $(".balck_bg").hide()	
	     }
		var apply_mark =$("#apply_mark").val();		
		cancelSelection(apply_mark);		
	});
	
	//背景设置
	$("#bg_img").live("change",function(){	
		 if(!validateFile()){
			 return false;
		 }
		    var val =$(this).val();
		    var img_id =$(".fiit_hot_img img").attr("id");		
			$("#bg_img_show").val($(this).val());
			$.ajaxFileUpload({
			      url: contextPath + '/s/shopDecotate/layout/decorate_hot_upload?img_id='+img_id,             
			      secureuri:false,
			      fileElementId:'bg_img',                         
			      dataType: 'json',
			      error: function (data, status, e){
			    	  parent.art.dialog.alert(data);
			       },
			       success: function (data, status){  
			     	  if(data.status =="fail"){
			     		 parent.art.dialog.alert("商城失败");
			     	 }else{
						 $("#img_show").val(data.pic);
						// $(".fiit_bg_prew img").attr("src",photoPath+data.pic);
						 $(".fiit_hot_img").show().find("img").attr("src",photoPath+data.pic).attr("id",data.pic);
						// $(".fiit_hot_img span").css("height",data.height).css("width",data.width);
						// $(".balck_bg").show().css("height",data.height);
						 $("#hotspots").html("");
						 $(".fiit_hot_img .choose").each(function(){
							 $(this).remove();										   
						 });
			     	 }
			     }
		  });
	 });
	
	$("#hotspots li").live("click",function(){
			$(this).parent().find("li").removeClass("this");
			var mark =$(this).addClass("this").attr("mark");
			var url = $(this).data("bind-url");
			$("#hotspot_url").val(url);
			$(".fiit_hot_img").find(".choose").removeClass("this");
			$(".choose[mark="+mark+"]").addClass("this");
			$("#url_count").html(mark);
	 });
	
	
	$('.fiit_hot_img img').imgAreaSelect({
		handles:true,									  
		onSelectEnd: function (img, selection) {
			var spot=jQuery("#hotspots li").length;
			var x1 =selection.x1;
			var y1 =selection.y1;
			var x2 =selection.x2;
			var y2 =selection.y2;
			
			console.log("----------------"+x1 + "," + y1 + "," + x2 + "," + y2);
			
			if(x1!=x2 && y1!=y2){//非点击
				var apply_mark =jQuery("#apply_mark").val();
				if(apply_mark==""){//非移动
					add_hotspot(x1,y1,x2,y2);
					}else{
						var data="";
						data=x1+","+y1+","+x2+","+y2;
						$("#apply_mark").data("bind-coor",data);
						setSelection(x1,y1,x2,y2);
					}
				}else{
				     cancelSelection(spot);	
				}
			}
      });
	
	//新增热点区域
	$("#add_hotspot").click(function(){							  
		addHotspot("","","","");
	});
	
	//初始化数据
	var count=array.length;
	if(count>0){
		var _count=0;
		for (i in array)
		{
			_count++;
			var x1 =array[i].x1;
			var y1 =array[i].y1;
			var x2 = array[i].x2;
			var y2 = array[i].y2;
			var url= array[i].hotspotUrl;
			var coor = x1 + "," + y1 + "," + x2 + "," + y2;
			console.log("coor--"+coor);
			var width = x2 - x1;
			var height = y2 - y1;
			var top = y1;
			var left = x1;
			
			
			var jsonObj=new Object();
			jsonObj.x1 = x1;
			jsonObj.y1 = y1;
		    jsonObj.x2 = x2;
		    jsonObj.y2 = y2;
			
			var text='<li mark='+_count+' class="this"><a href="javascript:void(0);">热点'+_count+'</a><i onclick="cancleHotspot('+_count+');"></i></li>';		
			$("#hotspots").append(text);
			$("#hotspots li[mark=" + _count + "]").data("bind-data", url + "==" + JSON.stringify(jsonObj)).data("bind-url", url);
			
			$(".fiit_hot_img").find(".choose").removeClass("this");
			var dataHtml="<div style='position:absolute;top:"+top+"px; left:"+left+"px; width:"+width+"px; height:"+height+"px;' mark="+_count+" class='choose'></div>";
			$(".fiit_hot_img").find("span").append(dataHtml);
		}
	}
	
});

function validateFile() {
	var _this=$("input[name='bg_img']");
	var filepath = $("input[name='bg_img']").val();
	var extStart = filepath.lastIndexOf(".");
	var ext = filepath.substring(extStart, filepath.length).toUpperCase();
	if (ext != ".BMP" && ext != ".PNG" && ext != ".GIF" && ext != ".JPG"
			&& ext != ".JPEG") {
		parent.art.dialog.alert("图片限于bmp,png,gif,jpeg,jpg格式");
		return false;
	}
	return true;
}



//取消当前选择区域	
function cancelSelection(spot){
		var ias = $('.fiit_hot_img img').imgAreaSelect({instance: true });	
		ias.cancelSelection();
		if(spot!="" && spot!=undefined){//将热点块去掉
			var apply_mark =$("#apply_mark").val();
			$("#hotspots").find("li[mark="+apply_mark+"]").remove();
			$("#apply_mark").val("");
		}
		$("#apply_mark").val("");
		$("#hotspot_url").val("");	
};	

function setSelection(x1,y1,x2,y2){
	
	var img = $('.fiit_hot_img img').imgAreaSelect({instance: true });	
	img.setOptions({ show: true });
	var jsonObj=new Object();
		if (x1 != "" && y1 != "" && x2 != "" && y2 != "") {
			jsonObj.x1 = x1;
			jsonObj.y1 = y1;
		    jsonObj.x2 = x2;
		    jsonObj.y2 = y2;
			//data = x1 + "," + y1 + "," + x2 + "," + y2;
			img.setSelection(x1, y1, x2, y2, true);
			var width = x2 - x1;
			var height = y2 - y1;
			var top = y1;
			var left = x1;
		} else {
			jsonObj.x1 = x1;
			jsonObj.y1 = y1;
		    jsonObj.x2 = x2;
		    jsonObj.y2 = y2;
			//data = 0 + "," + 0 + "," + 174 + "," + 110;
			img.setSelection(0, 0, 174, 110, true);
			var width = 174;
			var height = 110;
			var top = 0;
			var left = 0;
		}
		console.log("bind-coor="+JSON.stringify(jsonObj));
		console.log("width="+width);
		console.log("height="+height);
		console.log("top="+top);
		console.log("left="+left);
		$("#apply_mark").data("bind-coor",JSON.stringify(jsonObj));
		$("#apply_mark").data("width",width);
		$("#apply_mark").data("height",height);
		$("#apply_mark").data("top",top);
		$("#apply_mark").data("left",left);
		img.update(); 
};

//删除热点模块
function cancleHotspot(count) {
	$(".choose[mark=" + count + "]").remove();
	$("#hotspots li[mark=" + count + "]").remove();
	cancelSelection(count);
}

function applyHotspot(){
	var img_id =$(".fiit_hot_img img").attr("id");
	if(img_id!=""){
		var url =$("#hotspot_url").val();
		if(url!=""){
			$("#noticed").hide();	
			var apply_mark = $("#apply_mark").val();
			if (apply_mark != "") {
				var coor = $("#apply_mark").data("bind-coor");
				var width = $("#apply_mark").data("width");
				var height = $("#apply_mark").data("height");
				var top = $("#apply_mark").data("top");
				var left = $("#apply_mark").data("left");
				$("#hotspots li[mark=" + apply_mark + "]").data("bind-data", url + "==" + coor).data("bind-url", url);
				$(".fiit_hot_img").find(".choose").removeClass("this");
				var text = '<div class="choose this" mark="' + apply_mark+ '" style="position:absolute; top:' + top+ 'px; left:' + left + 'px; width:' + width+ 'px; height:' + height + 'px;"></div>';
				$(".fiit_hot_img span").append(text);
				cancelSelection();
			} else {
				parent.art.dialog.alert("请划分热点区域！");
			}
		}else {
			    parent.art.dialog.alert("请填写热点链接！");
		    }
    }else{
		   parent.art.dialog.alert("请先设置背景图片！");
	 }
}

//添加新热点区域
function addHotspot(x1,y1,x2,y2){
	   var img_id =$(".fiit_hot_img img").attr("id");
	   var spot_count=$("#hotspots li:last-child").attr("mark");
	   if(spot_count==null || spot_count==""|| spot_count==undefined){
		   spot_count=0;
	   }
	   var apply_mark =$("#apply_mark").val();
	   spot_count++;
		if(img_id!=""){
			if(spot_count==1){
			  $("#hotspots").html("");
			}
			setSelection(x1,y1,x2,y2);
			if(apply_mark==""){
				$("#hotspots li").removeClass("this");
				var text='<li mark='+spot_count+' class="this"><a href="javascript:void(0);">热点'+spot_count+'</a><i onclick="cancleHotspot('+spot_count+');"></i></li>';		
				$("#hotspots").append(text);
				$("#apply_mark").val(spot_count);
				$("#url_count").html(spot_count);
				}
		}else{
			 parent.art.dialog.alert("请先设置背景图片！");
		}
}

function save_form() {

	var coorImg = $(".fiit_hot_img img").attr("id");
	if(coorImg=="" || coorImg==undefined || coorImg==null){
		parent.art.dialog.alert("请先设置背景图片！");
		return false;
	}
	var length=$("#hotspots li").length;
	if(length==0){
		 parent.art.dialog.alert("请先设置热点！");
		 return false;
	}
	var coorDatas=new Array();
	$("#hotspots li").each(function() {
		var sentence = $(this).data("bind-data");
		if(sentence!="" && sentence!=undefined){
			var jsonObj=new Object();
			var words = sentence.split('==');
			var url=words[0];
			var _coorObj= new Obj;
			jsonObj.x1 = _coorObj.x1;
			jsonObj.y1 = _coorObj.y1;
		    jsonObj.x2 = _coorObj.x2;
		    jsonObj.y2 = _coorObj.y2;
		    jsonObj.hotspotUrl=url;
		    coorDatas.push(jsonObj);
		}
	});
	var coorImg = $(".fiit_hot_img img").attr("id");
	var coor_datas=JSON.stringify(coorDatas);
	console.log("coor_datas="+coor_datas);
	console.log("coor_img="+coorImg);
	 var layoutDiv=$("#layoutDiv").val();
	 var layoutId=$("#layoutId").val();
	 var data={
	    "coorDatas":coor_datas,
	    "imageFile":coorImg,
	    "layoutModuleType":$("#layoutModuleType").val(),
	    "layoutId":layoutId,
	    "layout":$("#layout").val(),
	    "divId":$("#divId").val(),
	    "layoutDiv":layoutDiv
	 }
	var imageHtml="<img border='0' src='"+photoPath+coorImg+"'>";
	$("#save").attr("disabled", "disabled");
	$.ajax({
        //提交数据的类型 POST GET
        type:"POST",
        //提交的网址
        url:contextPath+"/s/shopDecotate/layout/saveLayoutHot",
        //提交的数据
        data: data,
        async : true,  
        //返回数据的格式
        datatype: "json",
        //成功返回之后调用的函数            
		success : function(html) {
			if(html=="fail"){
				 parent.art.dialog.alert("保存失败！");
			}
			else{
				if (!isBlank(layoutDiv)) {
					parent.$("div[mark=" + layoutId + "][div=" + layoutDiv+ "][id='content']").html("");
					parent.$("div[mark=" + layoutId + "][div=" + layoutDiv+ "][id='content']").html(imageHtml);
					parent.art.dialog.list['decorateModuleHot'].close();
				} else {
					 parent.$("div[mark=" + layoutId + "][id='content']").html("");
					 parent.$("div[mark=" + layoutId + "][id='content']").html(imageHtml);
					 parent.art.dialog.list['decorateModuleHot'].close();
				}
				
			}
		}   ,
        //调用执行后调用的函数
        complete: function(XMLHttpRequest, textStatus){
        }     
    });
	

}

function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}



	
	
	