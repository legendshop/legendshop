$(document).ready(function() {
		userCenter.changeSubTab("mj_Marketing"); //高亮菜单
        typeChange();
        prodChange();
        $("input:radio[name='isAllProds']").change(function () {
            prodChange();
        })
        $("input:radio[name='type']").change(function () {
            typeChange();
        })

        $("#addProds").click(function () {
            layer.open({
                title : "添加商品",
                type : 2,
                id : "addProd",
                content : ['/s/addShopMarketingProds'],
                area : ['850px', '660px']
            });
        })
		laydate.render({
			   elem: '#startTime',
			   calendar: true,
			   theme: 'grid',
			   type:'datetime',
			   min:'-1',
			   trigger: 'click'
		  });

  		laydate.render({
			   elem: '#endTime',
			   calendar: true,
			   theme: 'grid',
			   type:'datetime',
			   min:'-1',
			   trigger: 'click'
		  });
    $(".tab-shop").on("click",".removeSku",function() {
        $(this).parents("tr").empty().remove();
        if ($("#skuList").find(".sku").size() == 0) {
            $(".noSku").show();
        }
        return false;
    })
	});

    jQuery.validator.methods.greaterThanStartDate = function(value, element) {
        var start_date = $("#startTime").val();
        var date1 = new Date(Date.parse(start_date.replace(/-/g, "/")));
        var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
        return date1 < date2;
    };
    jQuery.validator.methods.notNowDate = function(value, element) {
    	var now = new Date();
    	var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
    	return now < date2;
    };


    //页面输入内容验证
    $("#add_form").validate({
        errorPlacement: function(error, element){
            var error_td = element.next("span");
            error_td.append(error);
        },
        ignore: "",
        onfocusout: false,
        rules : {
            marketName : {
                required : true
            },
            startTime : {
                required : true,
            },
            endTime : {
                required : true,
                greaterThanStartDate : true,
                notNowDate : true
            },
            status:{
               required : true
            },
            type:{
               required : true
            },
            rule_count: {
                required: true,
                min: 1
            }
        },
        messages : {
            marketName : {
                required : '<i class="icon-exclamation-sign"></i>活动名称不能为空'
            },
            startTime : {
                required : '<i class="icon-exclamation-sign"></i>开始时间不能为空'
            },
            endTime : {
                required : '<i class="icon-exclamation-sign"></i>结束时间不能为空',
                greaterThanStartDate : '<i class="icon-exclamation-sign"></i>结束时间必须大于开始时间',
                notNowDate : '<i class="icon-exclamation-sign"></i>结束时间必须大于当前时间'
            },
            status:{
               required : '<i class="icon-exclamation-sign"></i>请选择商品参与方式',
            },
            type:{
               required : '<i class="icon-exclamation-sign"></i>请选择促销类型',
            },
            rule_count: {
                required: '<i class="icon-exclamation-sign"></i>请至少添加一条规则并确定',
                min: '<i class="icon-exclamation-sign"></i>请至少添加一条规则并确定'
            }
        },
        submitHandler:function(form){
            if($("#goods_status_0:checked").size()>0){
                if ($(".sku").size()==0){
                    layer.msg("您暂未选择商品，请重新选择商品",{icon:0});
                    return;
                }
            }
            var time = new Date($("#startTime").val().replace(/-/g,"/"));

            // //开始时间不能小于等于当前时间
            // if(time.getTime() <= new Date().getTime()){
            //     layer.alert("活动开始时间不能小于当前时间!", {icon:0});
            //     return false;
            // }

            $("input[type='submit']").attr("disabled","disabled");
            $("#add_form").ajaxForm().ajaxSubmit({
			   success:function(data) {
				  var result = JSON.parse(data);
			      if(result.status == 'OK'){
			    	  layer.msg(result.msg, {icon:1});
				      window.setTimeout("window.location.href=contextPath+'/s/shopSingleMarketing'",2000);
			      }else{
			    	  console.log(result.msg)
			    	 layer.alert(result.msg, {icon: 2});
			         $("input[type='submit']").removeAttr("disabled","disabled");
			         return ;
			      }
			   },
			   error:function(XMLHttpRequest, textStatus,errorThrown) {
				   layer.alert('您没有权限操作！', {icon: 2});
			     $("input[type='submit']").removeAttr("disabled","disabled");
				 return false;
			  }
	        });
        }

    });

     // 限时添加规则窗口
	    $('#btn_add_rule').on('click', function() {
	        var type= $("input[name='type']:checked").val();
	        if(type=='0'){  //满减促销
	              $('#add_manjian_rule').show();
	              $('#add_manze_rule').hide();
	              $("#manze_rule_list").hide();
	              $("#manjian_rule_list").show();
	               $('#manjian_price').val('');
	              $('#manjian_discount').val('');
	         }else if(type=='1'){ //满折促销
	              $('#add_manze_rule').show();
	              $('#add_manjian_rule').hide();
	              $("#manjian_rule_list").hide();
	              $("#manze_rule_list").show();
	              $('#manze_price').val('');
	              $('#manze_discount').val('');
	         }
	        $('#btn_add_rule').hide();
	    });

      $('#btn_add_rule').hide();
      $('#add_manjian_rule').show();
      $('#add_manze_rule').hide();
      // $("#goods_type_0").attr("checked","checked");


     $("input[name='type']").change(function(){
         var type=$(this).val();
         var rule_count ;
         if(type=='0'){  //满减促销
               rule_count = $('#manjian_rule_list').find('[nctype="mansong_rule_item"]').length;
              if( rule_count >= 3) {
                 $('#add_manjian_rule').hide();
              }else{
                 $('#add_manjian_rule').show();
              }
              $('#add_manze_rule').hide();
              $('#manjian_price').val('');
              $('#manjian_discount').val('');
               $("#manze_rule_list").hide();
              $("#manjian_rule_list").show();
         }else if(type=='1'){ //满折促销
             rule_count = $('#manze_rule_list').find('[nctype="mansong_rule_item"]').length;
             if(rule_count >= 3) {
                  $('#add_manze_rule').hide();
              }else{
                 $('#add_manze_rule').show();
              }
              $('#add_manjian_rule').hide();
              $('#manze_price').val('');
              $('#manze_discount').val('');
              $("#manjian_rule_list").hide();
              $("#manze_rule_list").show();
         }
         $('#btn_add_rule').hide();
         $('#mansong_rule_count').val(rule_count);
     });


     // 规则保存
    $('#save_mj_rule').on('click', function() {

    	var calTypeVal = $("#manjian_calType").val();
        var calType = "元";
        if(calTypeVal==1){
        	calType = "件";
        }

        var mansong = {};
        mansong.price = Number($('#manjian_price').val());
        mansong.discount = Number($('#manjian_discount').val());
        if(calTypeVal==0){
	        if(isNaN(mansong.price) || mansong.price <= 0) {
	            layer.alert('规则金额不能为空且必须为金额', {icon: 0});
	            return false;
	        }
	        if(!isMoney(mansong.price) || !isMoney(mansong.discount)){
	        	layer.alert('请输入正确的规则金额，金额最多只能有两位小数!', {icon: 0});
	            return false;
	        }
	        if(mansong.discount >= mansong.price){
	           layer.alert('满减金额必须小于规则金额', {icon: 0});
	           return false;
	        }
        }else{
        	if(isNaN(mansong.price) || !isPositiveNum(mansong.price)) {
	            layer.alert('规则件数不能为空且必须为正整数', {icon: 0});
	            return false;
	        }
        }

        if(isNaN(mansong.discount) || mansong.discount <= 0) {
            layer.alert('规则金额不能为空且必须为数字', {icon: 0});
            return false;
        }

        //遍历校验是否存在同样金额规则
        var judge = false;
		$("#manjian_rule_list li").each(function() {
			var i = $(this).find("input").val().split(",")[0];
			if(i == mansong.price) {
				layer.alert('不能创建相同满额规则', {icon: 0});
				judge = true;
			}
		});
		if(judge) { return false; }


        var mansong_rule_item=ruleHtml(mansong.price,mansong.discount,calType,calTypeVal);

        $('#manjian_rule_list').append(mansong_rule_item);

        close_mj_rule();
    });


     // 规则保存
    $('#save_mz_rule').on('click', function() {
    	var calTypeVal = $("#manze_calType").val();
        var calType = "元";
        if(calTypeVal==1){
        	calType = "件";
        }

        var mansong = {};
        mansong.price = Number($('#manze_price').val());
        if(calTypeVal==0){
	        if(isNaN(mansong.price) || mansong.price <= 0) {
	            layer.alert('规则金额不能为空且必须为金额', {icon: 0});
	            return false;
	        }
	        if(!isMoney(mansong.price)){
	        	layer.alert('请输入正确的规则金额，金额最多只能有两位小数!', {icon: 0});
	            return false;
	        }
        }else{
        	if(isNaN(mansong.price) || !isPositiveNum(mansong.price)) {
	            layer.alert('规则件数不能为空且必须为正整数', {icon: 0});
	            return false;
	        }
        }
        var discount=$.trim($('#manze_discount').val());
        if(discount=="" || discount==null || discount==undefined){
            layer.alert('规则金额不能为空且必须为数字', {icon: 0});
            return false;
        }
        var re =/^[1-9]([.]{1}[1-9])?$/;
		if(!re.test(discount)) {
		 	layer.alert('规则金额只能是0-10之间的1位整数或小数1-9.9之间', {icon: 0});
            return false;
		}

        var mansong_rule_item=ruleHtml2(mansong.price,discount,calType,calTypeVal);
        $('#manze_rule_list').append(mansong_rule_item);
        close_mz_rule();
    });



     // 关闭规则添加窗口
    function close_mz_rule() {
        var rule_count = $('#manze_rule_list').find('[nctype="mansong_rule_item"]').length;
        if( rule_count >= 3) {
            $('#btn_add_rule').hide();
        } else {
            $('#btn_add_rule').show();
        }
        $('#add_manze_rule').hide();
        $('#mansong_rule_count').val(rule_count);
    }

     // 关闭规则添加窗口
    function close_mj_rule() {
        var rule_count = $('#manjian_rule_list').find('[nctype="mansong_rule_item"]').length;
        if( rule_count >= 3) {
            $('#btn_add_rule').hide();
        } else {
            $('#btn_add_rule').show();
        }
        $('#add_manjian_rule').hide();
        $('#mansong_rule_count').val(rule_count);
    }

       // 删除已添加的规则
    $('#manjian_rule_list').on('click', '[nctype="btn_del_mansong_rule"]', function() {
        $(this).parents('[nctype="mansong_rule_item"]').remove();
        close_mj_rule();
    });

          // 删除已添加的规则
    $('#manze_rule_list').on('click', '[nctype="btn_del_mansong_rule"]', function() {
        $(this).parents('[nctype="mansong_rule_item"]').remove();
        close_mz_rule();
    });

    // 取消添加规则
    $('#btn_cancel_mj_rule').on('click', function() {
        close_mj_rule();
    });

    // 取消添加规则
    $('#btn_cancel_mz_rule').on('click', function() {
        close_mz_rule();
    });

    function ruleHtml(price,discount,calType,calTypeVal){
       var rule_count = $('#manjian_rule_list').find('[nctype="mansong_rule_item"]').length;
       var html="<li nctype='mansong_rule_item'><span>满<strong>"+price+"</strong>"+calType+"， </span><span>立减现金<strong>"+discount+"</strong>元， </span>";
       html+="<input type='hidden' value='"+price+","+discount+","+calTypeVal+"' name='manjian_rule["+rule_count+"]'>";
       html+="<a class='btn-r' style='margin-top: 3px;line-height: 23px;height: 23px;' href='javascript:void(0);' nctype='btn_del_mansong_rule'>删除</a></li>";
       return html;
    }

     function ruleHtml2(price,discount,calType,calTypeVal){
       var rule_count = $('#manze_rule_list').find('[nctype="mansong_rule_item"]').length;
       var html="<li nctype='mansong_rule_item'><span>满<strong>"+price+"</strong>"+calType+"， </span><span>打<strong>"+discount+"</strong>折， </span>";
       html+="<input type='hidden' value='"+price+","+discount+","+calTypeVal+"' name='manze_rule["+rule_count+"]'>";
       html+="<a class='btn-r' style='margin-top: 3px;line-height: 23px;height: 23px;' href='javascript:void(0);' nctype='btn_del_mansong_rule'>删除</a></li>";
       return html;
    }

     function isMoney(obj) {
 		if (!obj)
 			return false;
 		return (/^\d+(\.\d{1,2})?$/).test(obj);
 	 }

    function isPositiveNum(s){//是否为正整数
	    var re = /^[0-9]*[1-9][0-9]*$/ ;
	    return re.test(s)
	}

function  prodChange(){
    //初始化商品选择方式
    $("input:radio[name='isAllProds']").each(function(){
        if(this.checked){
            $(this).parent().parent().addClass("radio-wrapper-checked");
            if(this.value==1){
                $("#prods").hide();
            }else if (this.value==0) {
                $("#prods").show();
            }
        }else{
            $(this).parent().parent().removeClass("radio-wrapper-checked");
        }

    });
}

function  typeChange(){
    //初始活动类型
    $("input:radio[name='type']").each(function(){
        if(this.checked){
            $(this).parent().parent().addClass("radio-wrapper-checked");
        }else{
            $(this).parent().parent().removeClass("radio-wrapper-checked");
        }

    });
}

function searchSku(){
    var skuJson = mapToJson(skus);
    $.post("/s/searchSku",{"skuJson":skuJson},function (data) {
        skus.clear();
        prods.splice(0,prods.length);
        $(".tab-shop").html(data);
        layer.closeAll();
    })
}

function strMapToObj(strMap){
    let obj= Object.create(null);
    for (let[k,v] of strMap) {
        obj[k] = v;
    }
    return obj;
}
/**
 *map转换为json
 */
function mapToJson(map) {
    return JSON.stringify(this.strMapToObj(map));
}

function loadType(fullAmount,offAmount,calType){
    //初始化促销条件
    var unitName;
    if (calType == 0){
        unitName="元";
    }else if (calType == 1) {
        unitName="件";
    }
    if (marketingMjRules != null && marketingMjRules != ""){
        var mansong_rule_item=ruleHtml(fullAmount,offAmount,unitName,calType);
        $('#manjian_rule_list').append(mansong_rule_item);
        close_mj_rule();
    }else if (marketingMzRules != null && marketingMzRules !="") {
        var mansong_rule_item=ruleHtml2(fullAmount,offAmount,unitName,calType);
        $('#manze_rule_list').append(mansong_rule_item);
        close_mz_rule();
        $('#add_manjian_rule').hide()
    }
}


