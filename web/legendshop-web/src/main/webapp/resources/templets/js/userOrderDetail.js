var cancleOrder=function(id){
	layer.open({
		title : '取消订单',
		id : 'cancleOrder',
		type : 2,
		content : [contextPath + "/p/order/cancleOrderShow?subNumber="+id, 'no'],
		area: ['430px', '290px'],
	});
}

function orderReceive(_number){
	layer.open({
		title : '确认收货',
		id : 'cancleOrder',
		type : 1,
		content: "<div class='eject_con'><dl><dt>订单编号：</dt><dd>"+_number+" <p class='hint'>请注意： 如果你尚未收到货品请不要点击“确认”。大部分被骗案件都是由于提前确认付款被骗的，请谨慎操作！ </p>"
		+" </dd></dl></div>",
		area: ['430px', '290px'],
		btn: ['确定','关闭'],
		yes: function(){
			$.ajax({
				url:contextPath + "/p/orderReceive/"+_number,
				type:'POST',
				async : false, //默认为true 异步
				dataType:'text',
				success:function(result){
					if(result=="OK"){
						var msg = "确认收货成功";
						layer.msg(msg,{icon: 1,time:1000},function(){
							window.location.reload();
						});
					}else if(result=="fail"){
						var msg = "确认收货失败";
						layer.alert(msg,{icon:2});
					}
				}
			});
		}
	});
}

// 跳转订单售后选择页面
function returnApply(subId,subItemId,subNum) {

  //location.href = contextPath+"/p/return/apply/"+subId+"/"+subItemId+"?subNum="+subNum;
  // 判断订单是否为团长免单订单
  $.ajax({
    url : contextPath+"/p/isMergeGroupHeadFreeSub",
    type : 'post',
    data : {"subId":subId},
    dataType:'json',
    success : function(result){

      if(result == 'fail'){

        layer.msg("该拼团订单为团长免单订单哦,不能发起退货退款申请，您支付的款项拼团成功过后会自动退还",{icon: 0});
        return;
      }else {
        location.href = contextPath+"/p/return/apply/"+subId+"/"+subItemId+"?subNum="+subNum;
      }

    }
  });

}

var loaddely=false;
$(function(){
	$('#show_shipping').on('hover',function(){
		var html="";
		if(dvyFlowId=="" || dvyTypeId==""){
			html="没有物流信息";
		}else{
			if(!loaddely){
				$.ajax({
					url:contextPath + "/p/order/getExpress",
					data:{"dvyFlowId":dvyFlowId,"dvyTypeId":dvyTypeId,"reciverMobile":reciverMobile},
					type:'POST',
					async : false, //默认为true 异步
					dataType:'json',
					success:function(result){
						if(result=="fail" || result=="" ){
							html="没有物流信息";
							loaddely=true;
						}else{
							var list = eval(result);
							for(var obj in list){ //第二层循环取list中的对象
								html+="<li>"+list[obj].context+"<li>";
							}
							loaddely=true;
						}
					}
				});
			}
		}

		$('#shipping_ul').html(html);
		$('#show_shipping').unbind('hover');
	});
});
