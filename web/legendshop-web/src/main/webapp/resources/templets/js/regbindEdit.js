
	
/**
 * 发送短信验证码
 */
function sendMobileCode() {
	if(checkTel()){
		var tel=$("#phone").val();
		var randNum = $("#randNum").val();
		var data = {"tel": tel,"randNum":randNum};
		jQuery.ajax({
				url: contextPath + "/sendSMSCodeByThirdReg", 
				type: 'POST', 
				data: data,
				async : true, //默认为true 异步   
			    dataType : 'JSON', 
				success:function(retData){
					 if(!retData=="OK"){
						layer.msg('短信发送失败',{icon:2});
						   $("#erifyCodeBtn").val("重新发送");
					 }else{
						layer.msg('短信发送成功',{icon:1},function(){
							time($("#erifyCodeBtn"));
						});
						 }
				}
			});	
	}
}

// 验证码发送后的倒计时
var wait=60;
function time(btn){
	 $btn = $(btn);
	 if (wait == 0) {
		 $btn.removeAttr("disabled");
		 $btn.val("获取短信校验码");
         wait = 60;
     } else {
    	 $btn.attr("disabled", true);
    	 $btn.val(wait+"秒后重新获取");
         wait--;
         setTimeout(function () {
             time($btn);
         },
         1000)
     }
}

// 用户注册协议
$("#protocol").bind("click",function(){
	layer.open({
		title :"用户注册协议",
	    type: 2, 
	    id :"agreement",
	    content: ['agreementoverlay','no'],
	    area: ['950px', '480px']
		}); 
});
	