$(function(){
	purchaseAnalysis.doExecute();
	userCenter.changeSubTab("purchaseAnalysis");
	
	var queryTerms = $("#queryTerms").val();
	change(queryTerms);
});

//路径配置
require.config({
	paths: {
        echarts: contextPath+'/resources/plugins/ECharts/dist'
    }
});

// 使用
require(
    [ 
        'echarts', 
        'echarts/chart/bar',
        'echarts/chart/line'
    ],
    function (ec) {
    	
        // 基于准备好的dom，初始化echarts图表
        var myChart = ec.init(document.getElementById('main')); 
        
        var option = {
            tooltip: {
            	trigger: 'axis'
            },
            legend: {
            	data:['订单数量']
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataView : {readOnly:true},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 0,
                end : 100
            },
            xAxis : [
                {
                	type : 'category',
                    boundaryGap : true,
                    data : getXAxisData(),
                    axisLabel : {
                        formatter: '{value} 元'
                    }
                }
            ],
            yAxis : [
                {
                    type : 'value',
                    axisLabel : {
                        formatter: '{value} 单'
                    }
                }
            ],
            series : [
                      {
                          name:'订单数量',
                          type:'line',
                          itemStyle:{
                              normal:{color:'#87cefa'}
                          },
                          data:getReportData()
                      },
                      
                ]
        };

        // 为echarts对象加载数据 
        myChart.setOption(option); 
        
    }
);

//获取 x轴 数据
function getXAxisData(){
	 var list = [];
     var str = "";
     
    var xAxisJson = $("#xAxisJson").val();
    if(!isBlank(xAxisJson)){
    	var xAxisData = jQuery.parseJSON(xAxisJson);
     	for(var x = 0;x < xAxisData.length;x++){
     		list.push(xAxisData[x]);
     	}
    }
     return list;
}

//获取 报表 数据
function getReportData(){
	var list = [];
    if(!isBlank($("#reportJson").val())){
  	  var numberList = jQuery.parseJSON($("#reportJson").val());
  	  for(var q = 0;q < numberList.length;q++){
      	  list.push(numberList[q].numbers);
        }
    }
    return list;
}