jQuery(document).ready(function() {		
	
 	function isBlank(value){
 		return value == undefined ||  value == null || value == "";
 	}
	
	$("#main-nav-holder").on("click", ".fSort", function() {
			var id = $(this).attr("id");
			var orderDir = "";
			$(".fSort").removeClass("fSort-cur");			
			$(this).addClass("fSort-cur");
			var _a=$(this).find("i");
			if (id == 'cash') {
				if ($(_a).hasClass("f-ico-arrow-d")) {
					orderDir = "cash,asc";
				} else if($(_a).hasClass("f-ico-arrow-u")){
					orderDir = "cash,desc";
				}else{
					orderDir = "cash,desc";
				}
			} else if (id == 'buys') {
				if ($(_a).hasClass("f-ico-arrow-d")) {
					orderDir = "buys,asc";
				} else if($(_a).hasClass("f-ico-arrow-u")){
					orderDir = "buys,desc";
				}else{
					orderDir = "buys,desc";
				}
			} else if (id == 'comments') {
				if ($(_a).hasClass("f-ico-arrow-d")) {
					orderDir = "comments,asc";
				} else if($(_a).hasClass("f-ico-arrow-u")){
					orderDir = "comments,desc";
				}else{
					orderDir = "comments,desc";
				}
			} else if (id == 'default') {
				if ($(_a).hasClass("f-ico-arrow-d")) {
					orderDir = "recDate,asc";
				} else if($(_a).hasClass("f-ico-arrow-u")){
					orderDir = "recDate,desc";
				}else{
					orderDir = "recDate,desc";
				}
			}
			$("#orders").val(orderDir);
			var no_results=$.trim($("#no_results").html());
			if(!isBlank(no_results)){
				return false;
			}
			sendData();
		});
	
});
		

function pager(curPageNO){
	$("#curPageNO").val(curPageNO);
	sendData();
}	

function sendData(){
	$("#divPageLoading").show();
	$("#list_form").ajaxForm().ajaxSubmit({
		  success:function(result) {
			 $("#main-nav-holder").removeAttr("style");
			 $("#divPageLoading").hide();
			 $("#main-nav-holder").html(result); 
		   },
		   error:function(XMLHttpRequest, textStatus,errorThrown) {
			 $("#main-nav-holder").removeAttr("style");
			 $("#main-nav-holder").html("<div class='w'><div id='no_results' style='padding:200px 0;text-align:center;'><i></i>没有找到符合条件的商品</div></div>"); 
			 $("#divPageLoading").hide();
			 return false;
		  }	
	});
}

String.prototype.endWith = function(s) {
	if (s == null || s == "" || this.length == 0 || s.length > this.length)
		return false;
	if (this.substring(this.length - s.length) == s)
		return true;
	else
		return false;
	return true;
}

String.prototype.startWith = function(s) {
	if (s == null || s == "" || this.length == 0 || s.length > this.length)
		return false;
	if (this.substr(0, s.length) == s)
		return true;
	else
		return false;
	return true;
}
