$(function() {
	userCenter.changeSubTab("mergeGroupManage"); // 高亮菜单
	// 添加拼团活动
	$(".add-btn").click(function() {
		location.href = contextPath + "/s/mergeGroupActivity/save";
	});

	// 终止活动
	$(".downline").click(function() {
		var itemId = $(this).attr("itemId");
		var status = $(this).attr("status");
		var toStatus;
		if (status == 1) {
			toStatus = -1;
		}
		layer.confirm('是否确定进行终止操作 ? 如果终止活动，未成团的订单货款将全部原路返回！',{
				icon : 3,
				btn : [ '确定', '取消' ]
			// 按钮
			},
			function(index) {
				$.ajax({
					url : contextPath+ "/s/mergeGroupActivity/updateStatus/"+ itemId + "/"+ toStatus,
					type : 'post',
					dataType : 'json',
					async : true, // 默认为true// 异步
					success : function(result) {

						if (result == "OK") {
							window.location.reload();
						} else {
							layer.alert(result,{icon : 0});
						}
					}
				});
				layer.close(index);
			});
		});
});


// 物理删除
function confirmDelete(id) {
	layer.confirm('您确定要删除该活动?', {
		icon : 3,
		btn : [ '确定', '取消' ]
	// 按钮
	}, function(index) {
		jQuery.ajax({
			url : contextPath + "/s/mergeGroupActivity/delete/" + id,
			type : 'get',
			async : false, // 默认为true 异步
			dataType : 'json',
			success : function(data) {
				if (data == "OK") {
					window.location.reload();
				}else {
					layer.alert(data, {icon : 0});
				}
			}
		});
		layer.close(index);
	});
}

// 逻辑删除
function updateDeleteStatus(id) {
	layer.confirm('您确定要删除该活动?', {
		icon : 3,
		btn : [ '确定', '取消' ]
	// 按钮
	}, function(index) {
		jQuery.ajax({
			url : contextPath + "/s/mergeGroupActivity/updateDeleteStatus/" + id,
			type : 'get',
			async : false, // 默认为true 异步
			dataType : 'json',
			success : function(data) {
				if (data == "OK") {
					window.location.reload();
				}else {
					layer.alert(data, {icon : 0});
				}
			}
		});
		layer.close(index);
	});
}


// 分页条搜索
function pager(curPageNO){
    document.getElementById("curPageNO").value=curPageNO;
    document.getElementById("searchMergeGroup").submit();
}

//方法，判断是否为空
function isBlank(_value) {
	return _value == null || _value == "" || _value == undefined;
}
