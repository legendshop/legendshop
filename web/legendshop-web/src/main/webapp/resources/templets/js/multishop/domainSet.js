jQuery(document).ready(function(){
		 //三级联动
  	  $("select.combox").initSelect();
  	userCenter.changeSubTab("shopDomainSet");
 });
function search(){
	var secDomainName = $("#secDomainName").val();

	if(secDomainName == null || $.trim(secDomainName) == ""){
		layer.tips("输入框不能为空!",{icon:0});
		return;
	}

	if(secDomainName.length< min || secDomainName.length >max){
		layer.msg("二级域名长度必须要在"+sizeRange+"之间!",{icon:0});
		return;
	}

	$.ajax({
		 type: "GET",
		 url: contextPath+"/s/domain/query/"+secDomainName,
		 dataType: "JSON",
		 async : true,
		 success:function(result){
		 	if(result){
		 		$("#checkResult").attr("class","sec-no");
		 		$("#checkResult").html("对不起，这个域名已经被注册，请您搜索其他域名");
		 	}else{
		 		$("#checkResult").attr("class","sec-yes");
		 		$("#checkResult").html("恭喜您,&quot;" + secDomainName +"&quot;这个二级域名可以使用,<a href='javascript:regist(&apos;"+secDomainName+"&apos;)' style='color:green;'>我要注册!</a>");
		 	}
		}
	});
}

function regist(secDomainName){
	if(secDomainName == null || $.trim(secDomainName) == ""){
		layer.tips("输入框不能为空!",{icon:0});
		return;
	}
	if(secDomainName.length< min || secDomainName.length >max){
		layer.tips("二级域名长度必须要在"+sizeRange+"之间!",{icon:0});
		return;
	}

	$.ajax({
		 type: "GET",
		 url: contextPath+"/s/domain/update/"+secDomainName,
		 dataType: "JSON",
		 async : true,
		 success:function(result){
		 	layer.tips(result,{icon:0,time:700},function(){
		 		window.location.reload(true);
		 	});

		}
	});
}

