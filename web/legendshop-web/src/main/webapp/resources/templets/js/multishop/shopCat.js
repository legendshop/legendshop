$(document).ready(function() {
	$("#shopCatConfig").click(function(){//前往 一级类目配置
		window.location.href=contextPath+"/s/shopCatConfig";
	});

  $("#expireShopCategory").click(function(){//清除类目缓存
      $.get(contextPath+"/s/expireShopCategory",function (result) {
          if (result){
              layer.alert("清除成功！！！",{icon:1});
          }else {
            layer.alert("清除失败 请稍后重试！！！",{icon:0});
          }
      },"json")
  });

});

function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("from1").submit();
}

function updateCat(categoryId){
	window.location.href=contextPath+"/s/shopCatConfig?shopCatId="+categoryId;
}

function deleteCat(categoryId,categoryName){

	layer.confirm("确定要删除类目["+categoryName+"]?",{
 		 icon: 3
 	     ,btn: ['确定','取消'] //按钮
 	   }, function () {
 		  $.ajax({
				url:contextPath+"/s/deleteShopCat?shopCatId="+categoryId,
				type:'post',
				dataType : 'json',
				async : true, //默认为true 异步
				success:function(result){
					if(result=="0"){
						layer.alert('用户无效状态，不可进行删除操作 !', {icon: 2});
					} else if(result=="fail"){
						layer.alert('请先删除该类目下的二级商品类目!', {icon: 0});
					} else if(result=="limit"){
						layer.alert('该分类关联商品，无法删除!', {icon: 0});
					}else{
						window.location.reload();
					}
				}
			});
	  });
}

/** 更改商品类目状态**/
	jQuery("a[name='statusImg']").click(function(event){
  	var catId = $(this).attr("catId");
  	var status = $(this).attr("status");
  	var desc;
  	var toStatus;
  	   if(status == 1){
  	   		toStatus  = 0;
  	   	 desc = $(this).attr("catName")+' 下线?';
  	   }else{
  	       toStatus = 1;
  	       desc = $(this).attr("catName")+' 上线?';
  	   }

   	layer.confirm(desc,{
		 icon: 3
	     ,btn: ['确定','取消'] //按钮
	   }, function () {
		   jQuery.ajax({
				url:contextPath+"/s/updateShopCatStatus/" + catId + "/" + toStatus,
				type:'get',
				async : false, //默认为true 异步
				dataType : 'json',
				success:function(data){
					if(data==-1){
						layer.alert('用户无效状态，不可进行改操作 !', {icon: 2});
						return;
					}else if(data==-2){
						layer.alert('请先下线该类目下的子商品类目 !', {icon: 0});
						return;
					}else if(data==-3){
						layer.alert('请先上线该类目的上级商品类目 !', {icon: 0});
						return;
					}else{
					window.location.href=contextPath+"/s/shopCategory";
					}
				}
				});
	  });
 });

/** 下一级类目查询  **/
function nextCatQuery(shopCatId){
	window.location.href=contextPath+"/s/nextShopCat/query?shopCatId="+shopCatId;
}

 $(document).ready(function() {
			userCenter.changeSubTab("shopCategory");
	});
