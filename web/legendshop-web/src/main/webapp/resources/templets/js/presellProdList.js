function pager(curPageNO) {
	document.getElementById("curPageNO").value = curPageNO;
	document.getElementById("form1").submit();
}
$(document).ready(function() {
	$(".auc-load-list em").mouseover(function() {
		$("#loadTypeSelect").show();
	});
	$("#loadTypeSelect").mouseover(function() {
		$("#loadTypeSelect").show();
	});
	$("#loadTypeSelect li").mouseout(function() {
		$("#loadTypeSelect").hide();
	});
	$(".nch-sortbar-array ul li").bind("click",function() {
		var id = $(this).attr("id");
		var orderDir = "";
		$(".nch-sortbar-array ul li").each(function(i) {
			if (id != $(this).attr("id")) {
				$(this).removeClass("selected");
			}
		});
		$(this).addClass("selected");
		var _a = $(this).find("a");
		if (id == 'startTime') {
			if ($(_a).hasClass("desc")) {
				orderDir = "pre_sale_start,asc";
			} else if ($(_a).hasClass(
			"asc")) {
				orderDir = "pre_sale_start,desc";
			} else {
				$(_a).removeClass("asc");
				orderDir = "pre_sale_start,desc";
			}
		} else if (id == 'floorPrice') {
			if ($(_a).hasClass("desc")) {
				orderDir = "pre_price,asc";
			} else if ($(_a).hasClass(
			"asc")) {
				orderDir = "pre_price,desc";
			} else {
				orderDir = "pre_price,desc";
			}
		} else if (id == 'default') {
			orderDir = "";
		}
		if (loadType == undefined
				|| loadType == "") {
			loadType = 1;
		}
		window.location = contextPath+"/presell/presellProd/list?curPageNO="+_page+"&loadType="+ loadType+ "&orders="+ orderDir;
	});
});

var loadType;
function loadTypeClick(e) {
	loadType = $(e).attr("value");
	window.location = contextPath+"/presell/presellProd/list?curPageNO="+_page+"&loadType="+ loadType;
};

function getData(orderDir, type) {
	var data = {
			curPageNO : _page,
			orders : orderDir,
			loadType : type
	}
	return data;
}

