$(document).ready(function() {
	  $(".delivery").change(function(){
    	   var delivery=$(this).find("option:selected").val();
    	   if(isEmpty(delivery)){
    		  	layer.msg("请选择配送方式", {icon: 0});
	            return;
    	   }
    	   calculateTotal();
	  });
});

function calculateTotal(){
	
	   //获取实际商品价格
	 var allActualCash=$.trim($("#allActualCash").html()).substring(1); 
	   if(allActualCash <=0 || isEmpty(allActualCash)){ //实际价格不能为 0
		layer.alert("亲爱的用户,您的价格有误,请重新下单！", {icon: 0});
        return; 
	 }
	   
	   //获取总的店铺优惠券
    var allOffPrice = 0;
    var shopCoupons = $(".shop_coupon").get();
    for(var i=0;i<shopCoupons.length;i++){
 	   var c=$(shopCoupons[i]).find("option:selected").val();
 	   if(isEmpty(c)){
     		c=0;
     	}
    	    allOffPrice+=Number(c);
    }
   
    //获取所有运费
     var allfreight = 0; //所有运费价格
     var posts = $(".delivery").get();
     for(var i=0;i<posts.length;i++){
     	var f=$(posts[i]).find("option:selected").val();
     	if(isEmpty(f)){
     		f=0;
     	}
     	allfreight+=Number(f);
     }
     
     var orderAllCash=(Number(allActualCash)+Number(allfreight));
     orderAllCash=orderAllCash-Number(allOffPrice);
     
	  $("#allActualCash").html("￥"+  parseFloat(allActualCash).toFixed(2));
	  $("#allFreightAmount").html("￥"+  parseFloat(allfreight).toFixed(2));
     $("#allCash").html("￥"+  parseFloat(orderAllCash).toFixed(2));
     $("#allCashFormat").html(parseFloat(orderAllCash).toFixed(2));
}
/**
 * 提交订单
 */
function submitOrder(){
	
	var receiver= $("#deliveryAddress ul dl").length;
	if(receiver==0 || isEmpty(receiver)){
		layer.msg("请填写收货地址", {icon: 0});
		$("html,body").animate({scrollTop: $("#path_list").offset().top-50}, 500);
		return;
	}
	
	var addrId=$("#deliveryAddress ul ").children(".on").attr("addrid");
	if(isEmpty(addrId)){
		layer.msg("请选择收货地址", {icon: 0});
		$("html,body").animate({scrollTop: $("#path_list").offset().top-50}, 500);
		return;
	}
	
	//检查是否无货
	if($(".stocksErr").length>0){
		layer.msg("部分商品缺货或区域限售",{icon: 0});
		return;
	}
	
	var delivery="";
    var config=true;
    $(".delivery").each(function(){
    	  var deliv=$(this).find("option:selected").val();
          if(Number(deliv) < 0 || isEmpty(deliv)){
        	    layer.alert("请选择您的配送方式",{icon: 0});
            	$("html,body").animate({scrollTop: $(this).parent().parent().parent().parent().offset().top-50}, 300);
            	config=false;
            	return false;//实现break功能
          }else{
        	  var shopId=Number($(this).attr("shopid"));
        	  var transtype=$(this).find("option:selected").attr("transtype");
        	  delivery+=shopId+":"+transtype+";";
          }
     });
    
     if(!config){
    	return;
     }
     if(!isEmpty(delivery)){
 	 	delivery=delivery.substring(0, delivery.length-1);
 	 }
 	
	 // 获取订单备注
	 var remark =  $("input[name=remark]").val();
	 
	 var invoiceId=$(".invoice .invoice-no-spr").children("p").attr("invoice");
	 if(isEmpty(invoiceId)){
		 invoiceId=0;
	 }
	 
	 var allCash=parseFloat($("#allCashFormat").html());
	 if(allCash<=0){
		 layer.alert('订单金额有误请重新下单',{icon: 0});
		 return;
	 }
	 
	$("#remarkText").val(remark);
	$("#delivery").val(delivery);
	$("#invoiceIdStr").val(invoiceId);
	$("#submitOrder").css('background-color','#e4e4e4');
	$("#submitOrder").attr("disabled","disabled");
	
	
	$("body").showLoading();
    
	//调用方法 如       
	$("#orderForm").ajaxForm().ajaxSubmit({
		 success:function(data) {
			    var result=$.parseJSON(data);
				if(result.result==true){
					 var url=result.url;
					 window.location.href=contextPath+url;
					 return;
				}else if(result.result==false){
					$("body").hideLoading();
					$("#submitOrder").css('background-color',"#c6171f");
					$("#submitOrder").removeAttr("disabled","disabled");
					var errorCode=result.code;
					if(errorCode=="NOT_LOGIN"){
						layer.alert("请登录您的帐号！",{icon: 0},function(){
							window.location.href=contextPath+"/login";
						});
		                return false;
		            }else if(errorCode=="NOT_PRODUCTS"){
		            	 setTimeout(function(){
		                      window.location.href=contextPath+"/shopCart/shopBuy";
		                 },1);
		            	 return false;
		            }else if(errorCode=="NO_ADDRESS"){
		            	layer.alert("请填写您的收货人信息 ！",{icon: 0});
		            	$("html,body").animate({scrollTop: $("#user_addr").offset().top-50}, 500);
		        		return false;
		            }else if(errorCode=="NULL_TOKEN"){
		            	layer.alert("请重新登录后再下单", {icon: 0});
		           	    return false;
		            }else if(errorCode=="INVALID_TOKEN"){
		            	layer.alert("请重新登录后再下单", {icon: 0},function(){
		            		window.location.href=contextPath+"/login";
		            	});
		            }
		            else if(errorCode=="PARAM_ERR"){
		            	layer.alert("提交订单参数有误！",{icon: 0},function(){
		            		window.location.href=contextPath+"/shopCart/shopBuy";
		            	});
		           	    return false;
		            }else{
		            	layer.alert(result.message,{icon: 2});
		            	 return false;
		            }
				}
		 },
		 error:function(XMLHttpRequest, textStatus,errorThrown) {
			 $("body").hideLoading();
			 layer.alert("订单出现异常",{icon: 2});
			 return false;
		 }
		
	});
	
}

/**
 * 判断是否是空
 * @param value
 */
function isEmpty(value){
	if(value == null || value == "" || value == "undefined" || value == undefined || value == "null"){
		return true;
	}
	else{
		value = (value+"").replace(/\s/g,'');
		if(value == ""){
			return true;
		}
		return false;
	}
}


/**
 * 检查是否含有非法字符
 * @param temp_str
 * @returns {Boolean}
 */
function is_forbid(temp_str){
    temp_str = temp_str.replace(/(^\s*)|(\s*$)/g, "");
	temp_str = temp_str.replace('--',"@");
	temp_str = temp_str.replace('/',"@");
	temp_str = temp_str.replace('+',"@");
	temp_str = temp_str.replace('\'',"@");
	temp_str = temp_str.replace('\\',"@");
	temp_str = temp_str.replace('$',"@");
	temp_str = temp_str.replace('^',"@");
	temp_str = temp_str.replace('.',"@");
	temp_str = temp_str.replace(';',"@");
	temp_str = temp_str.replace('<',"@");
	temp_str = temp_str.replace('>',"@");
	temp_str = temp_str.replace('"',"@");
	temp_str = temp_str.replace('=',"@");
	temp_str = temp_str.replace('{',"@");
	temp_str = temp_str.replace('}',"@");
	var forbid_str = new String('@,%,~,&');
	var forbid_array = new Array();
	forbid_array = forbid_str.split(',');
	for(i=0;i<forbid_array.length;i++){
		if(temp_str.search(new RegExp(forbid_array[i])) != -1)
		return false;
	}
	return true;
}
