$(document).ready(function () {
	userCenter.changeSubTab("CustomServer"); //高亮菜单
});

var pager = function(pageNO) {
	$.ajax({
		url : "customServer/list",
		type : "POST",
		dataType : "html",
		data : {
			curPageNO : pageNO,
			name : $("#name").val(),
			account : $("#account").val()
		},
		success : function(result) {
			$("#list").html(result);
		},
	});
}

pager("1");


$("#search").click(function() {
	pager("1");
});

