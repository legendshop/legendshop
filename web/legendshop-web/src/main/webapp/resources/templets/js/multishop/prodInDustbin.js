jQuery(document).ready(function(){
		//三级联动
	  	$("select.combox").initSelect();
	  	userCenter.changeSubTab("prodInDustbin");
	});

	function pager(curPageNO){
		document.getElementById("curPageNO").value=curPageNO;
		document.getElementById("from1").submit();
	}

	function restore(prodId,name){
		layer.confirm("是否将商品：'"+name+"' 还原?",{
      		 icon: 3
      	     ,btn: ['确定','关闭'] //按钮
      	   },function () {
			jQuery.ajax({
				url:contextPath+"/s/product/restore/"+prodId,
				type:'get',
				async : false, //默认为true 异步
				dataType : 'json',
				success:function(data){
					if(data=="OK"){
						window.location.reload(true);
					}else{
						layer.msg('操作失败！', {icon: 2});
					}
				}
				});
		});
	}

	function realDel(prodId,name){
		layer.confirm("删除后不可恢复, 确定要删除'"+name+"' 吗?",{
      		 icon: 3
      	     ,btn: ['确定','关闭'] //按钮
      	   }, function () {
			jQuery.ajax({
				url:contextPath+"/s/product/deleteDustbin/"+prodId,
				type:'get',
				async : false, //默认为true 异步
				dataType : 'json',
				success:function(data){
					if(data=="OK"){
						layer.msg("删除成功！", {icon: 1,time:700},function(){
							window.location.reload(true);
						});
					}else{
						layer.msg(data, {icon: 2});
					}
				}
			});
		});
	}
