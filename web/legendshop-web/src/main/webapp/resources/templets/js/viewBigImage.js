function zoom_image(obj) {
    if (obj.hasClass('photoBox')) {
        var load = obj.find('.loadingBox');
        load.show();
        var img = obj.next().find('img');
        if (img.attr('src') == 'about:blank') {
            img.attr('src', obj.find('img').attr('src').replace('m.', 'l.'));
            img.load(function() {
                obj.hide();
                obj.next().show();
            });
        } else {
            obj.hide();
            obj.next().show();
        }
    } else {
        obj.hide();
        obj.prev().show();
        obj.prev().find('.loadingBox').hide();
    }
}