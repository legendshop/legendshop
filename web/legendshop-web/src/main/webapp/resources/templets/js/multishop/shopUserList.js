jQuery(document).ready(function(){
  	userCenter.changeSubTab("shopUsers");
 });

function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("from1").submit();
}

function createShopUser(){
	layer.open({
		  title :"新增员工",
		  id: "shopUser",
		  type: 2,
		  resize: false,
		  content: [contextPath+"/s/shopUsers/load",'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['430px', '400px']
		});
}

function updateShopUser(id,uType){

	layer.open({
		  title :"修改员工信息",
		  id: "shopUser",
		  type: 2,
		  resize: false,
		  content: [contextPath+"/s/shopUsers/load/"+id+"/"+uType,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['430px', '400px']
		});
}

function updatePwd(id){

	layer.open({
		  title :"修改员工密码",
		  id: "updatePwd",
		  type: 2,
		  resize: false,
		  content: [contextPath+"/s/shopUsers/loadPwd/"+id,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['430px', '250px']
		});
}


function stopUse(id){
	layer.confirm("确定要停用该账号吗？",{
 		 icon: 3
  	     ,btn: ['确定','取消'] //按钮
  	   },function(){
		jQuery.ajax({
			url:contextPath+"/s/shopUsers/stopUse/"+id,
			type:'get',
			async : false, //默认为true 异步
			dataType : 'json',
			success:function(result){
				layer.msg('停用成功', {icon: 1,time:700},function(){
            		window.location.reload();
            	});
			}
	 });
	});
}

function beUse(id){
	layer.confirm("确定要启用该账号吗？",{
 		 icon: 3
  	     ,btn: ['确定','取消'] //按钮
  	   },function(){
		jQuery.ajax({
			url:contextPath+"/s/shopUsers/beUse/"+id,
			type:'get',
			async : false, //默认为true 异步
			dataType : 'json',
			success:function(result){
				layer.msg('启用成功', {icon: 1,time:700},function(){
            		window.location.reload();
            	});
			}
	 });
	});
}

function delUser(id){
	layer.confirm("确定要删除该账号吗？",{
 		 icon: 3
  	     ,btn: ['确定','取消'] //按钮
  	   },function(){
		jQuery.ajax({
			url:contextPath+"/s/shopUsers/del/"+id,
			type:'get',
			async : false, //默认为true 异步
			dataType : 'json',
			success:function(result){
				layer.msg('删除成功', {icon: 1,time:700},function(){
            		window.location.reload();
            	});
			}
	 });
	});
}
