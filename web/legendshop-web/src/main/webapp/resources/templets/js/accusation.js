$(document).ready(function() {
	userCenter.changeSubTab("accusation");

	//三级联动
	$("select.combox").initSelect();
	KindEditor.options.filterMode=false;
	var editor = KindEditor.create('textarea[name="content"]', {
		uploadJson : contextPath + '/editor/uploadJson/upload;jsessionid='+UUID,
		fileManagerJson : contextPath + '/editor/uploadJson/fileManager',
		resizeType : 1,
		allowPreviewEmoticons : false,
		allowImageUpload : true,
		allowFileManager:true, //允许对上传图片进行管理
		afterBlur:function(){this.sync();},
		afterFocus : function(){
			$("#content").parent().parent().removeClass("err_bg");
		},
		width : '700px',
		height:'300px',
		afterCreate : function() {
			var self = this;
			KindEditor.ctrl(document, 13, function() {
				self.sync();
				document.forms['example'].submit();
			});
			KindEditor.ctrl(self.edit.doc, 13, function() {
				self.sync();
				document.forms['example'].submit();
			});
		},
		items : [
		         'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
		         'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
		         'insertunorderedlist', '|', 'emoticons', 'link']
	});

	$("#subjectId").change(function(){
		$("#subjectId").parent().parent().removeClass("err_bg");
	});

	$("#typeId").change(function(){
		$("#typeId").parent().parent().removeClass("err_bg");
	});

});

function loadAccusationList(){
	window.location.href = contextPath + "/p/accusation";
}

$("input[type=file]").change(function(){
	checkImgType(this);
	checkImgSize(this,1024);
});

function submitTable(){
	var flag = 0;
	for(i=1;i<4;i++){
		var f=document.getElementById("pic"+i+"File").value;
		if(isBlank(f)){
			flag++;
			continue;
		}
	}
	if(flag==3){ //全部为空
		layer.alert("请至少上传一张图片凭证！", {icon:0});
		return false;
	}
	$("#form1").submit();
}

//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}

$(document).ready(function(e) {
	jQuery("#form1").validate({
		errorPlacement: function(error, element) {
			error.appendTo(element.parent().parent().find("span"));
		},	
		rules: {
			typeId:{
				required:true
			},
			subjectId:{
				required:true
			},
			content:{
				required:true,
				maxlength:1000
			}
		},
		messages: {
			typeId:{
				required:"请选择举报类型"
			},
			subjectId:{
				required:"请选择举报主题"
			},
			content:{
				required:"请输入举报内容",
				maxlength:"举报内容字数最多1000字" 
			}
		}
	});
});