function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("form1").submit();
}

var beforeProd = null;
function loadProdSku(prodId,currProd){
	$(beforeProd).removeClass("prod-box-on");
	$(currProd).addClass("prod-box-on");
	beforeProd = currProd;
	var url = contextPath + "/s/auction/AuctionProdSku/"+prodId;
	document.getElementById("auctionProdSkuListFrame").src = url;
}

function addProd(prodId,skuId,skuName,skuPrice,cnProperties){
	//判断当前选择的sku是否已参加其他活动
	if(isAttendActivity(prodId,skuId)){
		layer.alert("对不起,您选择的SKU已参加其他活动!",{icon:0});
		return false;
	}
	parent.addProdTo(prodId,skuId,skuName,skuPrice,cnProperties);
	var index = parent.layer.getFrameIndex('actionShowProdlist'); //先得到当前iframe层的索引
	parent.layer.close(index); //再执行关闭 
}

//是否已参加其他营销活动
function isAttendActivity(prodId,skuId){
	var b = false;
	$.ajax({
		url : contextPath + "/s/presellProd/isAttendActivity/" + prodId + "/"+ skuId,
		async : false, 
		dataType : "JSON",
		error : function(){
			b = false;
		},
		success : function(result){
			b = result;
		}
	});
	return b;
}
