/**
 *  Tony
 */
jQuery(document).ready(function(){

	userCenter.changeSubTab("couponManage");

	/*jQuery包装集调用方式,调用tips弹层*/
	$('.share').pt({
	    position: 'l', // 默认属性值
	    align: 'c',	   // 默认属性值
	    width:280,
	    leaveClose:true, // 打开失去焦点猴关闭
	    autoClose:false, //取消自动关闭
		//去除移动端推广<a href="javascript:void(0);" type="link">优惠券链接</a><div class="sha-cou-con" style="display: none;">'+
		// 	    '<dl><dt>移动端</dt><dd><input type="text" placeholder="" id="oneMobile" disabled="true"><a class="copybtn" id="copyMobile" data-clipboard-text="" href="javascript:void(0);">'+
		// 	    '复制</a></dd></dl></div>
	    content: '<div class="share-coupon" id="oneUrl" data-id=""><div class="sha-cou-nav">'+
	    '<a href="javascript:void(0);"  style="padding-left: 70px" class="active" type="code">优惠券二维码</a></div>'+
	    '<div class="sha-cou-con" style="padding: 10px 80px;"><div class="cou-code" id="oneUrlCode"></div><div class="code-download">'+
	    '<a href="javascript:void(0);" onclick="uploadImg()">下载二维码</a></div></div></div>'
	});


	$('.share').mouseenter(function(){
		var id = $(this).attr("data-id");

		$("#oneUrl").attr("data-id",id);

		var vueUrl = vueDomainName + "/walletModules/myCoupon/couponDetail?couponId=" + id;
		//渲染二维码
		var qrcode = new QRCode(document.getElementById("oneUrlCode"), {
			  text: vueUrl,
			  width: 120,
			  height: 120,
			  colorDark : "#000000",
			  colorLight : "#ffffff",
			  correctLevel : QRCode.CorrectLevel.H
		});

		$("#oneMobile").val(vueUrl);
		$("#copyMobile").attr("data-clipboard-text",vueUrl);

	});

	//tab切换
	$("body").on("click",".sha-cou-nav a", function() {
		var type = $(this).attr("type");

		if ("code" == type) {
			$("#oneUrl").children("div").eq(1).show();
			$("#oneUrl").children("div").eq(2).hide();
		}else if("link" == type){
			$("#oneUrl").children("div").eq(1).hide();
			$("#oneUrl").children("div").eq(2).show();
		}

		$(this).parent().find("a").removeClass("active");
		$(this).addClass("active");

	});


	 var clipboard = new Clipboard('.copybtn');
	    clipboard.on('success', function(e) {
	    	layer.msg('复制成功');
	 });



});

function uploadImg(){
	var imgPathURL=$("#oneUrlCode img").attr("src");
    if(imgPathURL){
        oDownLoad(imgPathURL);
    }else{
        toastrWaring("二维码图片为空");
    }
}

//判断浏览器类型
function myBrowser(){
    var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
    var isOpera = userAgent.indexOf("Opera") > -1;
    if (isOpera) {
        return "Opera"
    }; //判断是否Opera浏览器
    if (userAgent.indexOf("Firefox") > -1) {
        return "FF";
    } //判断是否Firefox浏览器
    if (userAgent.indexOf("Chrome") > -1){
        return "Chrome";
    }
    if (userAgent.indexOf("Safari") > -1) {
        return "Safari";
    } //判断是否Safari浏览器
    if (userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera) {
        return "IE";
    }; //判断是否IE浏览器
    if (userAgent.indexOf("Trident") > -1) {
        return "Edge";
    } //判断是否Edge浏览器
}

//IE浏览器图片保存本地
function SaveAs5(imgURL)
{
    var oPop = window.open(imgURL,"","width=1, height=1, top=5000, left=5000");
    for(; oPop.document.readyState != "complete"; )
    {
        if (oPop.document.readyState == "complete")break;
    }
    oPop.document.execCommand("SaveAs");
    oPop.close();
}

function oDownLoad(url) {
    if (myBrowser()==="IE"||myBrowser()==="Edge"){
        SaveAs5(url);
    }else{
        download(url);
    }
}

//谷歌，360极速等浏览器下载
function download(src) {
    var $a = document.createElement('a');
    $a.setAttribute("href", src);
    $a.setAttribute("download", "");

    var evObj = document.createEvent('MouseEvents');
    evObj.initEvent( 'click', true, true, window, 0, 0, 0, 0, 0, false, false, true, false, 0, null);
    $a.dispatchEvent(evObj);
}


function seacher(){
   $(".editAdd_load").show();
   $("#form1").submit();
}

function pager(curPageNO) {
	document.getElementById("curPageNO").value = curPageNO;
	document.getElementById("form1").submit();
}

function offLine(id){
	jQuery.ajax({
		url : contextPath+"/s/coupon/updatestatus/" + id+"/"+0,
		type : 'post',
		async : false, //默认为true 异步
		dataType : 'json',
		error : function(data) {

			layer.alert('下线失败', {icon: 2});
			return;
		},
		success : function(data) {
			layer.msg('下线成功！', {icon: 1,time:700},function(){
        		window.location.reload();
        	});
		}
	});
}

function onLine(id){
	jQuery.ajax({
		url : contextPath+"/s/coupon/updatestatus/" + id+"/"+1,
		type : 'post',
		async : false, //默认为true 异步
		dataType : 'json',
		error : function(data) {
			layer.alert('上线失败', {icon: 2});
			return;
		},
		success : function(data) {
			layer.msg('上线成功！', {icon: 1,time:700},function(){
        		window.location.reload();
        	});
		}
	});
}

function highlightTableRows(tableId) {
	var previousClass = null;
	var table = document.getElementById(tableId);
	if (table == null)
		return;
	var tbody;
	if (table.getElementsByTagName("tbody") != null) {
		tbody = table.getElementsByTagName("tbody")[0];
	}
	if (tbody == null) {
		var rows = table.getElementsByTagName("tr");
	} else {
		var rows = tbody.getElementsByTagName("tr");
	}
	for (i = 0; i < rows.length; i++) {
		rows[i].onmouseover = function() {
			previousClass = this.className;
			this.className = 'pointercolor'
		};
		rows[i].onmouseout = function() {
			this.className = previousClass
		};
	}
}

function issueCoupon(id) {

	layer.open({
		  title :"提示",
		  id: "Retested",
		  type: 2,
		  resize: false,
		  content: [contextPath+"/s/coupon/issueCoupon/" + id,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['430px', '250px']
		});
}

function downloadExcel(id) {
	$(".editAdd_load").show();
	jQuery.ajax({
		url : contextPath+"/s/coupon/exportExcel/" + id,
		type : 'post',
		async : false, //默认为true 异步
		dataType : 'json',
		error : function(data) {
			$(".editAdd_load").hide();

			layer.alert('礼券下载失败', {icon: 2});
			return;
		},
		success : function(data) {
			var cToObj = JSON.parse(data);
			if (!cToObj.success) {
				alert(cToObj.msg);
				$(".editAdd_load").hide();
				window.location.href = contextPath+"/s/coupon/query";
				return;
			} else {
				$(".editAdd_load").hide();
				var url = contextPath+"/servlet/downloadfileservlet/" + cToObj.msg;
				window.open(url);
				window.location.reload(true);
				return;
			}
		}
	});
}

function confirmDelete(prodId, name) {
	layer.confirm("确定要删除优惠券'" + name + "'吗？", {
 		 icon: 3
  	     ,btn: ['确定','取消'] //按钮
  	   },function() {
		var result;
		jQuery.ajax({
			url : contextPath+"/s/coupon/delete/" + prodId,
			type : 'post',
			async : false, //默认为true 异步
			dataType : 'json',
      error : function(){
        layer.msg("您没有权限操作！！！", {icon: 2});
        result="false";
      },
			success : function(retData) {
				result = retData;
			}
		});

		if ('OK' == result) {
			layer.msg('删除成功！', {icon: 1,time:700},function(){
        		window.location.reload();
        	});
		} else {
		  if (result!="false"){
        layer.msg(result, {icon: 2});
      }
		}
	}, function() {
		//cancel
	});

}
