$(document).ready(function () {
	userCenter.changeSubTab("seckillManage"); //高亮菜单

  $("#search").click(function () {
    document.getElementById("curPageNO").value = 1;
  })
});


function pager(curPageNO) {
	document.getElementById("curPageNO").value = curPageNO;
	document.getElementById("searchseckillActivity").submit();
}

function onlineShopseckillActivity(_id) {
	layer.confirm('确定要发布该活动么？',{
		icon: 3
		,btn: ['确定','取消'] //按钮
	}, function () {
		window.location.href = contextPath + "/s/seckillActivity/updateStatus/" + _id + "/1";
	})
}

function offlineShopseckillActivity(_id) {
	layer.confirm('确定要终止该活动么？',{
		icon: 3
		,btn: ['确定','取消'] //按钮
	}, function () {
		window.location.href = contextPath + "/s/seckillActivity/updateStatus/" + _id + "/0";
	})


}

function deleteShopseckillActivity(_id) {
	layer.confirm('确定要删除活动么？',{
		icon: 3
		,btn: ['确定','取消'] //按钮
	}, function () {
		window.location.href = contextPath + "/s/seckillActivity/delete/" + _id;
	})
}

function updateShopseckillActivity(_id) {
	window.location.href = contextPath + "/s/seckillActivity/update/" + _id;
}

$(".question-mark").mouseover(function () {
  var auditOpinion = $("#auditOpinion").val();
  var html = "<ul><li>" + auditOpinion + "</li></ul>";
  $("#history").html(html);
  $("#opinion").css("display", "block")
});

$(".question-mark").mouseout(function () {
  $("#history").html("");
  $("#opinion").css("display", "none")
});
