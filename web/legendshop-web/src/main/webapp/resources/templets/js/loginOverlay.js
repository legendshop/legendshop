	function register(){
		$("#login").removeClass("curr");
		$("#register").addClass("curr");
		url = contextPath + "/loadRegisterOverlay";
		sendData(url);
	}

	function login(){
		$("#register").removeClass("curr");
		$("#login").addClass("curr");
		url = contextPath +  "/loadLoginOverlayContent";
		sendData(url);
	}

	function sendData(url){
		$.ajax({
		"url":url,
		type:'post',
		async : true, //默认为true 异步
		success:function(result){
		  $("#loginOverlayDiv").html(result);
		}
		});
	}

	//当焦点落入时
	function nameJudge(){
		$("#name").attr("class", "text highlight1");
		$("#regName_error").attr("class", "focus");
		$("#regName_error").html("请输入手机号码");
		$("#regName_succeed").attr("class", "registBrank");
		$("#morePinDiv").hide();
	}

	//当焦点离开时
	function nameLeave(){
		var name = $("#name").val();
		//去除绿边框
		$("#name").attr("class","text");
		$("#regName_error").attr("class","hide");

		checkTel();
	}

	function passwordJudge(){
		$("#password").attr("class", "text highlight1");
		$("#pwd_error").attr("class","hide");
		$("#pwd_succeed").attr("class","registBrank");
	}

	function passwordLeave(){
		var pdValue = $("#password").val();
		$("#password").attr("class","text");
		var regx1= /^[0-9A-z`~!@#$%^&*()_\-+=<>?:"{}|,.\/;'\\[\]·~！@#￥%……&*（）——\-+={}|《》？：“”【】、；‘’，。、]+$/g;
		var regx= /^(?![0-9]+$)(?![a-z]+$)(?![A-Z]+$)(?!([^(0-9a-zA-Z)]|[\(\)])+$)([^(0-9a-zA-Z)]|[\(\)]|[a-z]|[A-Z]|[0-9]){6,20}$/;
		/*if(pdValue.length!=0){
			if(pdValue.length>=6 && pdValue.length<=20){
				$("#pwd_succeed").attr("class","registBrank succeed");
			}else{
				$("#password").addClass("highlight2");
				$("#pwd_error").attr("class", "error");
				$("#pwd_error").html("密码长度只能在6-20位字符之间");
			}
		}*/
		if(pdValue.indexOf(" ") > -1){
			$("#password").addClass("highlight2");
			$("#pwd_error").attr("class", "error");
			$("#pwd_error").html("密码不能带有空格");
		}else if (pdValue == null || pdValue == '' || pdValue.length <= 0){
			$("#password").addClass("highlight2");
			$("#pwd_error").attr("class", "error");
			$("#pwd_error").html("密码不能为空");
		}else if (pdValue.length < 6 || pdValue.length > 20){
			$("#password").addClass("highlight2");
			$("#pwd_error").attr("class", "error");
			$("#pwd_error").html("密码长度在 6-20 个字符之间");
		}else if (!regx.test(pdValue) || !regx1.test(pdValue)){
			$("#password").addClass("highlight2");
			$("#pwd_error").attr("class", "error");
			$("#pwd_error").html("密码为6-20位字母、数字和标点符号的组合");
		}else{
			$("#pwd_succeed").attr("class","registBrank succeed");
		}
	}

	function password2Judge(){
		$("#password2").attr("class", "text highlight1");
		$("#pwdRepeat_error").attr("class","hide");
		$("#pwdRepeat_succeed").attr("class","registBrank");
	}

	function password2Leave(){
		var pd = $("#password").val();
		var pd2 = $("#password2").val();
		$("#password2").attr("class","text");
		if(pd2.length!=0){
			if(pd2.length>=6 && pd2.length<=20){
				if(pd==pd2 ){
					$("#pwdRepeat_succeed").attr("class","registBrank succeed");
				}else{
					$("#password2").addClass("highlight2");
					$("#pwdRepeat_error").attr("class", "error");
					$("#pwdRepeat_error").html("两次输入密码不一致");
				}
			}else{
				$("#password2").addClass("highlight2");
				$("#pwdRepeat_error").attr("class", "error");
				$("#pwdRepeat_error").html("密码长度只能在6-20位字符之间");
			}
	    }
	}

	function randnumJudge(){
		$("#authcode_error").attr("class", "hide");
		$("#randNum").css("color","black");
		$("#randNum").css("border","1px solid #E1E1E1");
	}

	function erifyCodeJudge(){
		$("#erifyCode_error").attr("class", "hide");
	}

	function mailJudge(){
		$("#mail").attr("class", "text highlight1");
		$("#mail_error").attr("class","hide");
	}

	function mailLeave(){
		//验证邮箱
		var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
		var mailValue = $("#mail").val();
		$("#mail").attr("class","text");
		if(mailValue.length!=0){
			if(reg.test(mailValue)){
				if(!checkEmail()){
					$("#mail_succeed").attr("class","registBrank succeed");
				}else{
					$("#mail").addClass("highlight2");
					$("#mail_error").attr("class", "error");
					$("#mail_error").html("该邮箱已被使用，请更换其他邮箱");
				}
			}else{
				$("#mail").addClass("highlight2");
				$("#mail_error").attr("class", "error");
				$("#mail_error").html("请输入正确 的邮箱");
			}
		}
	}

	function quickRegister(){
			var pd = $("#password").val();
			var regx1= /^[0-9A-z`~!@#$%^&*()_\-+=<>?:"{}|,.\/;'\\[\]·~！@#￥%……&*（）——\-+={}|《》？：“”【】、；‘’，。、]+$/g;
			var regx= /^(?![0-9]+$)(?![a-z]+$)(?![A-Z]+$)(?!([^(0-9a-zA-Z)]|[\(\)])+$)([^(0-9a-zA-Z)]|[\(\)]|[a-z]|[A-Z]|[0-9]){6,20}$/;
			var password2 = $("#password2").val();
			var name = $("#name").val();

      var readme = $("#readme").prop('checked');

      if(!readme){
        layer.msg('请先勾选注册协议！',{icon:0});
        return;
      }
			if(!checkTel()){
				return;
			}else{
				if(pd.length==0){
					$("#password").addClass("highlight2");
					$("#pwd_error").attr("class", "error");
					$("#pwd_error").html("请输入密码");
					return;
				}else if(pd.indexOf(" ") > -1){
					$("#password").addClass("highlight2");
					$("#pwd_error").attr("class", "error");
					$("#pwd_error").html("密码不能带有空格");
					return;
				}else if(pd.length<6  && pd.length>20){
					$("#password").addClass("highlight2");
					$("#pwd_error").attr("class", "error");
					$("#pwd_error").html("密码长度只能在6-20位字符之间");
					return;
				}else if (!regx.test(pd) || !regx1.test(pd)){
					$("#password").addClass("highlight2");
					$("#pwd_error").attr("class", "error");
					$("#pwd_error").html("密码为6-20位字母、数字和标点符号的组合");
				}
				if(password2.length==0){
					$("#password2").addClass("highlight2");
					$("#pwdRepeat_error").attr("class", "error");
					$("#pwdRepeat_error").html("请输入密码");
					return;
				}else if(password2.length<6  && password2.length>20){
					$("#password2").addClass("highlight2");
					$("#pwdRepeat_error").attr("class", "error");
					$("#pwdRepeat_error").html("密码长度只能在6-20位字符之间");
					return;
				}else if(pd!=password2){
					$("#password2").addClass("highlight2");
					$("#pwdRepeat_error").attr("class", "error");
					$("#pwdRepeat_error").html("两次输入密码不一致");
					return;
				}
				if(pd.length!=0 && password2.length!=0){
					if(validateRandNum(contextPath)){
						if(checkErifyCode()){
							var telValue = $("#name").val();
							var pageErifyCode =$("#erifyCode").val();
							$.ajax({
								url: contextPath + "/verifyCode",
								data: {"mobile":telValue,"verifyCode":pageErifyCode},
								type:'post',
								async : false, //默认为true 异步
								success:function(retData){
								 	if('true' == retData){
								 		var url = contextPath + "/quickregister";
										var data = {"password":pd,"mobile":telValue,"randNum":$("#randNum").val(),"mobileCode":pageErifyCode};
										submitRegister(url,data);
									}else{
											$("#erifyCode_succeed").attr("class", "blank");
											$("#erifyCode_error").attr("class", "error");
											$("#erifyCode_error").html("短信验证码不正确！");
									}
								}
							});
						}else{

						}
					}else{
						$("#authcode_error").html("验证码错误");
						$("#authcode_error").attr("class", "error");
					}
				}
			}
	}


	//提交注册
	function submitRegister(url,data){
		$.ajax({
			"url":url,
			"data":data,
			type:'post',
			dataType : 'json',
			async : false, //默认为true 异步
			success:function(result){
				if(result == 'OK'){
					layer.msg('注册成功',{icon:1,time:500},function(){
						parent.location.reload();
					});
				}else{
					layer.msg('注册失败',{icon:2});
				}
			}
			});
	}

	//用户协议弹窗
	function showProtocol(){
		parent.layer.open({
    		title :"用户注册协议",
		    type: 2,
		    id :"agreement",
		    content: ['/agreementoverlay','no'],
		    area: ['950px', '480px']
    		});
	}

	//当焦点落在用户名时
	function userNameJudge(){
		$("#username").addClass("highlight1");
		$("#loginname_error").attr("class","hide");
	}
	//当焦点落在密码时
	function pwdJudge(){
		$("#pwd").addClass("highlight1");
		$("#loginpwd_error").attr("class","hide");
	}

	//当焦点离开时
	function userNameLeave(){
		$("#username").attr("class","text");
	}

	//当焦点离开时
	function pwdLeave(){
		$("#pwd").attr("class","text");
	}

	function submitLoginOverlay(){
		var userName = 	$("#username").val();
		var password = 	$("#pwd").val();

		if(userName.length == 0){
			$("#loginname_error").attr("class", "null");
			$("#loginname_error").html("请输入邮箱/用户名/已验证手机");
		}
		if(password.length == 0){
			$("#loginpwd_error").attr("class", "null");
			$("#loginpwd_error").html("请输入密码");
		}
		if(password.length != 0&&userName.length != 0){
			if(validateRandNum(contextPath)){
				$.ajax({
					url: contextPath + '/userlogin',
					data:{"userName":userName,"pwd":password,"randNum":$("#randNum").val()},
					type:'post',
					dataType : 'json',
					async : false, //默认为true 异步
					success:function(result){
						if(result == 'OK'){
							layer.msg('登录成功',{icon:1,time:700},function(){
								parent.location.reload();
							});
						}else{
							layer.msg('密码错误',{icon:2,});
						}
					}
				});
			}else{
				$("#authcode_error").html("验证码错误");
				$("#authcode_error").attr("class", "error");
			}
		}
	}

	function sendMobileCode() {
		if(checkTel()){
			var tel=$("#name").val();
			var randNum = $("#randNum").val();
			var data = {"tel": tel,"randNum":randNum};
			jQuery.ajax({
					url: contextPath + "/sendSMSCodeByReg",
					type:'post',
					data:data,
					async : false, //默认为true 异步
				    dataType : 'json',
					success:function(retData){

						 if(!retData=="OK"){
							layer.msg('短信发送失败',{icon:2},function(){
								$("#erifyCodeBtn").val("重新发送");
							});
						 }else{
							layer.msg('短信发送成功',{icon:1},function(){
								time($("#erifyCodeBtn"));
							});
						 }
					}
				});
		}
	}

	//检查电话号码
	function checkTel(){
		var isPhone=/^1\d{10}$/;
		var telValue = $("#name").val();
		$("#name").attr("class","text");
		$("#regName_error").attr("class","hide");
		if(telValue ==null || telValue =='' || telValue.length<=0 ||telValue=='请输入手机号码'){
			$("#name").attr("class", "text highlight2");
			$("#regName_error").attr("class", "error");
			$("#regName_error").html("<div class='error-ifo'>请输入手机号码</div>");
			return false;
		}
		if(telValue.length!=11){
			$("#name").attr("class", "text highlight2");
			$("#regName_error").attr("class", "error");
			$("#regName_error").html("<div class='error-ifo'>手机号码必须为11位</div>");
			return false;
		}
		if(!isPhone.test(telValue)){
			$("#name").attr("class", "text highlight2");
			$("#regName_error").attr("class", "error");
			$("#regName_error").html("<div class='error-ifo'>手机号码无效或存在特殊字符</div>");
			return false;
		}
		var config=true;
		$.ajax({
			url: contextPath + "/isPhoneExist",
			data: {"Phone":telValue},
			type:'post',
			async : false, //默认为true 异步
			success:function(retData){
			 	if('true' == retData){
					$("#name").attr("class", "text highlight2");
					$("#regName_error").attr("class", "error");
					$("#regName_error").html("<div class='error-ifo'>此号码已存在!</div>");
					config=false;
					return false;
				}
			}
		});
		if(!config){
			return false;
		}

		return true;
	}

	var wait=60;
	function time(btn){
		 $btn = $(btn);
		 if (wait == 0) {
			 $btn.removeAttr("disabled");
			 $btn.val("获取短信校验码");
	         wait = 60;
	     } else {
	    	 $btn.attr("disabled", true);
	    	 $btn.val(wait+"秒后重新获取校验码");
	         wait--;
	         setTimeout(function () {
	             time($btn);
	         },
	         1000)
	     }
	}

	//检查短信验证码
	function checkErifyCode(){
		var erifyValue = jQuery("#erifyCode").val();
		var erifyInput = $("#erifyCode");
		$("#erifyCode_error").attr("class","hide");
		if(erifyValue ==null || erifyValue =='' || erifyValue.length<=0){
			$("#erifyCode_succeed").attr("class", "blank");
			$("#erifyCode_error").attr("class", "error");
			$("#erifyCode_error").html("<div class='error-ifo'>短信校验码不能为空</div>");
			return false;
		}
		if(erifyValue.length < 6){
			$("#erifyCode_succeed").attr("class", "blank");
			$("#erifyCode_error").html("<div class='error-ifo'>短信验证码必须是6个字符</div>");
			$("#erifyCode_error").attr("class","error");
			return false;
		}
		$("#erifyCode_error").html("");
		$("#erifyCode_error").removeClass("error");
		return true;
	}
