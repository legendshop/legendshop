$(function () {
    //改变加号鼠标指针
    $(".chooseSku").mouseover (function () {
      $(this).css("cursor","pointer");
    })
    // 标识不可选的商品
    $("tr.first").each(function(){
      var prodId = $(this).attr("prodId");
      var flag = isDisable(prodId);

      if(!flag){

        $(this).find("div").css("display","block");
        $(this).find("div").children().html("无可用sku");

        $(this).find("input").addClass("unChecked");
        $(this).find("input").attr("disabled","disabled");
        $(this).find("input").next().css("background","#eee");
        $(this).find("label").removeClass("radio-wrapper");
      }
    });

    //切换选择商品
    $("input:radio[name='prodId']").change(function () {
        $(".check-arr").hide();
        var prodId = $(this).val();

        $("input:radio[name='prodId']").each(function () {

            if (this.checked) {
                $(this).parent().parent().addClass("radio-wrapper-checked");
            } else {
                $(this).parent().parent().removeClass("radio-wrapper-checked");
                parent.skuId=-1;
            }
        });
        if ($(this).prop("checked")){
            $(".addSku").hide();
            $(".chooseSku").removeClass("chooseSkuList")
            $(".skuNum").text("选择规格");
            $(this).parents(".prodCheck").siblings(".chooseSku").find(".addSku").show();
           $(this).parents(".prodCheck").siblings(".chooseSku").addClass("chooseSkuList");
        }
    });

    //切换选择商品的规格
    $(window).on("click",".skuOne",function () {
        var skuType = $(this).next(".skuType").val();

        //判断商品互斥（是否已参与其他活动）
        var flag = isAttendActivity(skuType);
        if (flag != "false") {
            layer.msg("该商品已参加" + flag + "活动", {icon: 0});
            this.checked = false;
            return;
        }

        $(".skuOne").each(function () {

            if (this.checked) {
                $(this).parent().parent().addClass("radio-wrapper-checked");
            } else {
                $(this).parent().parent().removeClass("radio-wrapper-checked");
            }
        });
    });

});

// 分页搜索
function pager(curPageNO) {
    document.getElementById("curPageNO").value = curPageNO;
    document.getElementById("form1").submit();
}

// 确认选择商品
function confirm() {

    var prodId = $("input:radio[name='prodId']:checked").val();

    if (isBlank(prodId)) {
        layer.msg("请先选择参与活动的商品", {icon: 0});
        return;
    }
    var skuId =parent.skuId;

    if (isBlank(skuId)||skuId==-1) {
        layer.msg("请活动的商品的规格", {icon: 0});
        return;
    }

    parent.addProdTo(skuId);

    parent.skuId=-1;
    parent.layer.closeAll();
}

// 取消
function cancel() {
    parent.layer.closeAll();
}

// 是否参加其他活动
function isAttendActivity(skuType) {
    var flag = "false";
    if (skuType=="GROUP"){
        flag = "团购"
    }else if (skuType=="MERGE"){
        flag = "拼团";
    }else if (skuType=="SECKILL"){
        flag = "秒杀";
    }else if (skuType=="AUCTION"){
        flag = "拍卖";
    }else if (skuType=="PRESELL"){
        flag = "预售";
    }
    return flag;
}

// 是否参加其他活动
function isDisable(prodId){
  var flag = true;
  $.ajax({
    url : contextPath + "/s/seckillActivity/isAttendActivity/" + prodId ,
    async : false,
    dataType : "JSON",
    success : function(result){
      flag = result;
    }
  });
  return flag;
}


function isBlank(value) {
    return value == undefined || value == null || value == "";
}

//点击选择规格限时sku列表
$(".first").on("click",".chooseSkuList",function(){
    var prodid = $(this).siblings(".prodCheck").find("input:radio").val();
    $.post("/s/loadSkuList",{prodId:prodid},function (data) {
        if (data!=null){
            $(".skuTr").remove();
            $(data.skuList).each(function(index,dom){
                var cnProperties = dom.cnProperties;
                if (dom.cnProperties==""||dom.cnProperties==null){
                    cnProperties="暂无";
                }
                var tr = "<tr class='skuTr'>\n" +
                    "\t\t\t\t\t\t\t\t<td class='skuCheck' width='35px'>\n" +
                    "\t\t\t\t\t\t\t\t\t<label class='radio-wrapper'>\n" +
                    "\t\t\t\t\t\t\t\t\t\t<span class='radio-item'>\n" +
                    "\t\t\t\t\t\t\t\t\t\t\t<input type='radio' id='radio' name='skuOne' class='radio-input skuOne'  value="+dom.skuId+">\n" +
                    "\t\t\t\t\t\t\t\t\t\t\t<input type='hidden' class='skuType'  value="+dom.skuType+">\n" +
                    "\t\t\t\t\t\t\t\t\t\t\t<input type='hidden' id='refProd'  value="+prodid+">\n" +
                    "\t\t\t\t\t\t\t\t\t\t\t<span class='radio-inner'></span>\n" +
                    "\t\t\t\t\t\t\t\t\t\t</span>\n" +
                    "\t\t\t\t\t\t\t\t\t</label>\n" +
                    "\t\t\t\t\t\t\t\t</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='300px' style='text-align: center'>"+cnProperties+"</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='70px'>"+dom.price+"</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='70px' class='skuStocks'>"+dom.stocks+"</td>\n" +
                    "</tr>"
                $(".skuList").append(tr);
            })
            $(".check-arr").css("z-index","2");
            $(".check-arr").show();
        }
    },"json")
})
//商品选择关闭按钮
$(".close").on("click",function(){
    $(".check-arr").hide();
})

//sku选择提交事件
$(".skuSubmit").on("click",function(){
    var prodid = $("#refProd").val();
    parent.skuId = $(".skuOne:checked").val();
    if ($(".skuOne:checked").size()>0){
        $(":radio:checked[value='"+prodid+"']").parents(".first").find(".skuNum").text("已选规格("+$(".skuOne:checked").size()+")");
    }else {
        $(":radio[value='"+prodid+"']").parents(".first").find(".skuNum").text("选择规格");
    }
    $(".check-arr").hide();
})
