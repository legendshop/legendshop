/**
 *  Tony
 */
jQuery(document).ready(function(){

	//店铺分类
	/*jQuery("i[id^='i_']").click(function(){
		  var str = jQuery(this).attr("id").substring(2);
		  var child_show=jQuery(this).attr("child_show");
		  if(child_show=="true"){
		    jQuery(this).removeClass("i_cut");
			jQuery(this).addClass("i_add");
			jQuery(this).parent().next(".level_second").slideUp('normal');
		    jQuery(this).attr("child_show","false");
		  }else{
		    jQuery(this).removeClass("i_add");
			jQuery(this).addClass("i_cut");
		   jQuery(this).parent().next(".level_second").slideDown('normal');
		    jQuery(this).attr("child_show","true");
		  }
	 });*/

	initCat();

	//商品分类点击事件
	jQuery("i[id^='i_']").click(function(){
		  var str = jQuery(this).attr("id").substring(2,5);
		  var child_show=jQuery(this).attr("child_show");
		  if(str == "one") {//点击的是一级
			  firstCatOnOff(this, child_show);
		  }else {//点击的是二级
			  secondCatOnOff(this, child_show);
		  }
	 });

	//滚动条滚动事件
	jQuery(window).scroll(function(){
	var top = jQuery(document).scrollTop();
	if(top==0){
		  jQuery("#back_box").hide();
		  jQuery(".back_box_x").hide();
		}else{
		  jQuery("#back_box").show();
	      jQuery(".back_box_x").show();
		}
	});

	//商品排序
	$(".nch-sortbar-array ul li").live( 'click',function(){
		var id = $(this).attr("id");
		var cata1=$("#catagoryType1").val();
		var cata2=$("#catagoryType2").val();
		var cata3=$("#catagoryType3").val();
		if(isBlank(cata1)&&isBlank(cata2)&&isBlank(cata3)){
			cata1=fireCat;
			cata2=fireCat;
			cata3=fireCat;
		}
		var orderDir = "";
		$(".nch-sortbar-array ul li").each(function(i) {
			if (id != $(this).attr("id")) {
				$(this).removeClass("selected");
			}
		});
		$(this).addClass("selected");
		var _a=$(this).find("a");
		if (id == 'cash') {
			if ($(_a).hasClass("desc")) {
				orderDir = "cash,asc";
			} else if($(_a).hasClass("asc")){
				orderDir = "cash,desc";
			}else{
				$(_a).removeClass("asc");
				orderDir = "cash,desc";
			}
		} else if (id == 'buys') {
			if ($(_a).hasClass("desc")) {
				orderDir = "buys,asc";
			} else if($(_a).hasClass("asc")){
				orderDir = "buys,desc";
			}else{
				orderDir = "buys,desc";
			}
		} else if (id == 'comments') {
			if ($(_a).hasClass("desc")) {
				orderDir = "comments,asc";
			} else if($(_a).hasClass("asc")){
				orderDir = "comments,desc";
			}else{
				orderDir = "comments,desc";
			}
		} else if (id == 'default') {
			orderDir = "recDate,desc";
		}
		order=orderDir;
		loadStoreProd(1,cata1,cata2,cata3,keyword,orderDir);
		return false;
	});



	//滚动条滚动事件
	jQuery(window).scroll(function(){
	   var top = jQuery(document).scrollTop();
	   if(top==0){
		  jQuery("#back_box").hide();
		  jQuery(".back_box_x").hide();
		}else{
		  jQuery("#back_box").show();
	      jQuery(".back_box_x").show();
		}
	});

	//
	jQuery("#toTop").click(function(){
       jQuery('body,html').animate({scrollTop:0},1000);
       return false;
    });

	jQuery("#store_head_box").css("cursor","peointer").mouseover(function(){
		jQuery("#store_level").show();
	})

	jQuery("#store_head_box").mouseout(function(){
			jQuery("#store_level").hide();
	});

     loadStoreProd(1,fireCat,secondCat,thirdCat,keyword,order);
	 var element=$(".scroll_red");
     progress(soc,element);

});


function pager(_currentPage){
   loadStoreProd(_currentPage,fireCat,secondCat,thirdCat,keyword,order);
}


function fireCat1(fireCat){
	$("#catagoryType1").val(fireCat);
	$("#catagoryType2").val("");
	$("#catagoryType3").val("");
    loadStoreProd(1,fireCat,"","","","");
 }

 function secondCat1(secondCat){
	$("#catagoryType1").val("");
	$("#catagoryType2").val(secondCat);
	$("#catagoryType3").val("");
    loadStoreProd(1,"",secondCat,"","","");
 }

 function thirdCat1(thirdCat){
	$("#catagoryType1").val("");
	$("#catagoryType2").val("");
	$("#catagoryType3").val(thirdCat);
    loadStoreProd(1,"","",thirdCat,"","");
 }


var seq=null;
function loadStoreProd(_curPageNO,_fireCat,_secondCat,_thirdCat,_keyword,_orderDir){
	if(_fireCat==undefined){
		_fireCat="";
     }
	if(_secondCat==undefined){
		_secondCat="";
     }
	if(_thirdCat==undefined){
		_thirdCat="";
     }
	if(_orderDir==undefined){
		_orderDir="";
     }
	jQuery.ajax({
        //提交数据的类型 POST GET
        type:"POST",
        //提交的网址
        url:contextPath+"/store/prodcts/"+_shopId,
        //提交的数据
        data:{"curPageNO":_curPageNO,"fireCat":_fireCat,"secondCat":_secondCat,"thirdCat":_thirdCat,"keyword":_keyword,"order":_orderDir},
        async : true,
        cache: true,
        //返回数据的格式
        datatype: "html",
        //成功返回之后调用的函数
        success:function(html){
	        jQuery("#store_prod_list").html("");
	        jQuery("#store_prod_list").html(html);
	        if (_orderDir!=null && _orderDir!=""){
            var checkedStr = _orderDir.toString().split(",");
            var checked = "#"+checkedStr[0];
            $(".selected").removeClass("selected");
            $(""+checked+"").addClass("selected");
            var _a= $(""+checked+"").find("a");
            seq=checkedStr;
            if (checkedStr[1]=="asc"){
              $(_a).addClass("asc")
              $(_a).removeClass("desc")
            }else if( checkedStr[1]=="desc"){
              $(_a).addClass("desc")
              $(_a).removeClass("asc")
            }
          }

        } ,
        //调用执行后调用的函数
        complete: function(XMLHttpRequest, textStatus){
        }
  });

}

function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}

//初始收起分类
function initCat(){
	//全部关闭
	$(".level_second").hide();
	$(".level_third").hide();
	$("i[id^='i_']").attr("child_show","false");
	$("i[id^='i_']").removeClass("i_cut");
	$("i[id^='i_']").addClass("i_add");

	var _curCat = null;
	if(!isBlank(fireCat)) {
		_curCat = fireCat;
	}else if (!isBlank(secondCat)) {
		_curCat = secondCat;
	}else if (!isBlank(thirdCat)) {
		_curCat = thirdCat;
	}
	//获取当前 _curCat dom节点
	var _curDom = $(".cat_" + _curCat);
	console.log(_curDom);

	$(_curDom).css("color", "#ff3300");
	//当前处于第几级分类
	var _catLevel = $(_curDom).attr("cat-level");

	switch (_catLevel) {
		case "one": //一级
			break;
		case "two": //打开二级类目
			var firstLevel = $(_curDom).parents(".level_li");
			$(firstLevel).find(".level_one").find("i").attr("child_show","true");
			$(firstLevel).find(".level_one").find("i").removeClass("i_add");
			$(firstLevel).find(".level_one").find("i").addClass("i_cut");
			$(firstLevel).find(".level_second").show();
			break;
		case "third": //打开三级类目
			var firstLevel = $(_curDom).parents(".level_li");
			$(firstLevel).find(".level_one").find("i").attr("child_show","true");
			$(firstLevel).find(".level_one").find("i").removeClass("i_add");
			$(firstLevel).find(".level_one").find("i").addClass("i_cut");

			$(firstLevel).find(".level_second").show();
			var secondLevel = $(firstLevel).find(".level_second").find("ul");
			$(secondLevel).find("li").first().find("i").attr("child_show","true");
			$(secondLevel).find("li").first().find("i").removeClass("i_add");
			$(secondLevel).find("li").first().find("i").addClass("i_cut");
			$(secondLevel).find(".level_third").first().show();
			break;
	}
}

//一级菜单开关
function firstCatOnOff(obj, _on_of) {
	if(_on_of == "true"){
	    $(obj).removeClass("i_cut");
		$(obj).addClass("i_add");
		$(obj).parent().next(".level_second").slideUp('normal');
	    $(obj).attr("child_show","false");
	 }else{
	    $(obj).removeClass("i_add");
		$(obj).addClass("i_cut");
	    $(obj).parent().next(".level_second").slideDown('normal');
	    $(obj).attr("child_show","true");
	 }
}

//二级菜单开关
function secondCatOnOff(obj, _on_of) {
	if(_on_of == "true"){
	    $(obj).removeClass("i_cut");
		$(obj).addClass("i_add");
		$(obj).parent().next(".level_third").slideUp('normal');
	    $(obj).attr("child_show","false");
	 }else{
	    $(obj).removeClass("i_add");
		$(obj).addClass("i_cut");
		$(obj).parent().next(".level_third").slideDown('normal');
	    $(obj).attr("child_show","true");
	 }
}
