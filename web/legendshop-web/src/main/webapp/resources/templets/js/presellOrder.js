$(document).ready(function(){
	userCenter.changeSubTab("presellOrder");
	 $(".selectAllNotPayOrder").click(function(){
	     
		if($(this).attr("checked")=="checked"){
	         $(".selector").each(function(){
	        	 var disabled =$(this).attr("disabled");
	        	 if(disabled==null || disabled=="" || disabled==undefined){
	        		 $(this).attr("checked",true);
	        	 }
	         });
	     }else{
	         $(".selector").each(function(){
	             $(this).attr("checked",false);
	         });
	     }
	 });
});


function abstractForm(URL, subNums){
	   var temp = document.createElement("form");        
	   temp.action = URL;        
	   temp.method = "post";        
	   temp.style.display = "none";        
	   var opt = document.createElement("textarea");        
	   opt.name = 'subNums';        
	   opt.value = subNums;        
	   temp.appendChild(opt);        
	   document.body.appendChild(temp);        
	   temp.submit();        
	   return temp;  
}


function pager(curPageNO){
	$("input[name=curPageNO]").val(curPageNO);
	$("#order_search").submit();
}

var cancleOrderFlag=0;
var cancleOrder = function(id){
	layer.open({
		title : '取消订单',
		id : 'cancleOrder',
		type : 2,
		content : [contextPath + "/p/order/cancleOrderShow?subNumber="+id, 'no'],
		area: ['430px', '290px'],
	});
}


function orderReceive(_number) {
	// 页面层
	layer.open({
				type : 1,
				anim : 6,
				area : [ '420px', '240px' ], // 宽高
				content : "<div class='eject_con'><dl><dt>订单编号：</dt><dd>"
						+ _number
						+ " <p class='hint'>请注意： 如果你尚未收到货品请不要点击“确认”。大部分被骗案件都是由于提前确认付款被骗的，请谨慎操作！ </p>"
						+ " </dd></dl></div>",
				btn : [ '确定', '关闭' ], //按钮
				yes: function(index, layero){
					$.ajax({
						url:path+"/p/orderReceive/"+_number, 
						type:'POST', 
						async : false, //默认为true 异步   
						dataType:'text',
						success:function(result){
							if(result=="OK"){
								layer.msg("确认收货成功!", {icon:1, time:1000}, function(){
									window.location.reload();
								});
							}else if(result=="fail"){
								layer.msg("取消订单失败", {icon:2, time:1000}, function(){
									window.location.reload();
								});
							}
						}
					});
			    }
	});
}
	
	
   //删除订单
	var removeOrder=function(id){
		layer.confirm('您确定要删除吗?删除后该订单可以在回收站找回，或彻底删除？', {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   $.ajax({
					url:path+'/p/order/removeOrder', 
					data:{"subNumber":id},
					type:'POST', 
					async : false, //默认为true 异步   
					dataType:'text',
					success:function(result){
						if(result=="OK"){
							var msg = "删除订单成功";
							layer.msg(msg, {icon: 1,time:1000},function(){
								window.location.reload();
							});
						}else if(result=="fail"){
							var msg = "删除订单失败";
							layer.alert(msg, {icon: 2},function(){
								window.location.reload();
							});
						}
					}
				});
		   });
	}
	
	
	//永久删除订单
	var dropOrder=function(id){
		layer.confirm('您确定要永久删除吗?永久删除后您将无法再查看该订单，也无法进行投诉维权，请谨慎操作！', {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   $.ajax({
			    	 url:path+'/p/dropOrder/'+id,
						type:'POST', 
						async : false, //默认为true 异步   
						dataType:'text',
						success:function(result){
							if(result=="OK"){
								var msg = "删除订单成功";
								layer.msg(msg, {icon: 1,time:1000},function(){
									window.location.reload();
								});
							}else if(result=="fail"){
								var msg = "删除订单失败";
								layer.alert(msg, {icon: 2},function(){
									window.location.reload();
								});
							}
						}
					});
			   
		   });
	}
	
	//订单还原
	function restoreOrder(id) {
		layer.confirm('您确定要还原该订单吗?', {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   $.ajax({
					url:path+'/p/order/restoreOrder', 
					data:{"subNumber":id},
					type:'POST', 
					async : false, //默认为true 异步   
					dataType:'text',
					success:function(result){
						if(result=="OK"){
							var msg = "还原订单成功";
							layer.msg(msg, {icon: 1,time:1000},function(){
								window.location.reload();
							});
						}else if(result=="fail"){
							var msg = "还原订单失败";
							layer.alert(msg, {icon: 2},function(){
								window.location.reload();
							});
						}
					}
				});
			   
		   });
}