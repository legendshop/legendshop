jQuery(document).ready(function(){
  // $("#from2").hide();

	//三级联动
	$("select.combox").initSelect();
	userCenter.changeSubTab("orderBill");
	userCenter.changeSubTab("orderBill");

	//订单金额tip
  $('#orderAmount').hover(function(){

    var that = this;
    //tips
    tips = layer.tips("<span style='color:#666;'>结算周期内产生确认收货，订单状态为已完成的订单，包含运费</span>",that, {
      tips: [1, '#fef2f6'],
      time: 10000
    });
  },function(){
    layer.close(tips);
  });
	//退单金额tip
  $('#refundAmount').hover(function(){

    var that = this;
    //tips
    tips = layer.tips("<span style='color:#666;'>结算周期内平台确认退款，退款单状态为已完成的退款金额</span>",that, {
      tips: [1, '#fef2f6'],
      time: 10000
    });
  },function(){
    layer.close(tips);
  });
	//平台佣金tip
  $('#commisTotals').hover(function(){

    var that = this;
    //tips
    tips = layer.tips("<span style='color:#666;'>平台佣金 = (订单金额 - 退单金额) * 平台结算佣金比例% </span>",that, {
      tips: [1, '#fef2f6'],
      time: 10000
    });
  },function(){
    layer.close(tips);
  });
	//分销佣金tip
  $('#distCommisTotals').hover(function(){

    var that = this;
    //tips
    tips = layer.tips("<span style='color:#666;'>周期内，已完成且没有退货或者退货不成功的分销订单</span>",that, {
      tips: [1, '#fef2f6'],
      time: 10000
    });
  },function(){
    layer.close(tips);
  });
	//红包金额tip
  $('#redpackTotals').hover(function(){

    var that = this;
    //tips
    tips = layer.tips("<span style='color:#666;'>订单使用的红包总额</span>",that, {
      tips: [1, '#fef2f6'],
      time: 10000
    });
  },function(){
    layer.close(tips);
  });
	//退款红包金额tip
  $('#returnRedpackTotals').hover(function(){

    var that = this;
    //tips
    tips = layer.tips("<span style='color:#666;'>退款订单退的红包金额，订单产生全退款就不退还（部分商品的退货按比例补回）退款红包金额 = 订单项使用的红包金额 * 原价 / 订单总金额</span>",that, {
      tips: [1, '#fef2f6'],
      time: 10000
    });
  },function(){
    layer.close(tips);
  });
	//预售定金tip
  $('#preDepositPriceTotal').hover(function(){

    var that = this;
    //tips
    tips = layer.tips("<span style='color:#666;'>平台收取的预售定金（已支付定金未支付尾款且尾款支付时间已结束的预售订单）</span>",that, {
      tips: [1, '#fef2f6'],
      time: 10000
    });
  },function(){
    layer.close(tips);
  });
	//拍卖保证金tip
  $('#auctionDepositTotal').hover(function(){

    var that = this;
    //tips
    tips = layer.tips("<span style='color:#666;'>拍卖活动结束后，在用户中标并且24小时内未转订单或中标转成订单后但是72小时内未支付的情况下，按规则扣除保证金不予退还，平台按周期与商家结算收取的拍卖保证金</span>",that, {
      tips: [1, '#fef2f6'],
      time: 10000
    });
  },function(){
    layer.close(tips);
  });


});

function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("from1").submit();
}

function  formsubmit() {
  $("#formsubmit").click();
  return false;
}

function confirmShopBill(id){
	layer.confirm("一旦确认将无法恢复，系统将自动进入结算环节,确认系统出账单计算无误吗?",{
		icon: 3
		,btn: ['确定','取消'] //按钮
	},function(){
		jQuery.ajax({
			url:contextPath+"/s/shopOrderBill/confirm/"+id,
			type:'get',
			async : false, //默认为true 异步
			dataType : 'json',
			success:function(data){
				if(data=="OK"){
					layer.msg('确认成功！', {icon: 1,time:1000},function(){
						window.location = contextPath+"/s/shopOrderBill/load/"+id;
					});

				}else{
					layer.msg('确认失败！', {icon: 2});
				}
			}
		});
	});

}

