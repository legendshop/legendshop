var isSubmitted = false;//表单是否已提交

//页面加载的时候
$(function(){
	//切换申请类型
	var oldAapplyType = 2;
	$("#applyTypeSelect li").click(function(){
		var value = Number($(this).val());//获取用户选择的操作类型
		//如果选择的类型没有变
		if(oldAapplyType === value){
			return;
		}
		
		//样式切换
		$("#applyTypeSelect li").removeClass("on");
		$(this).addClass("on");
		
		var fileLi1 = $("#refundForm").find("#fileLi1");
		if(value === 1){//仅退款
			$("#refundReturnBox").hide();
			$("#refundBox").show();
		}else{//退款及退货
			$("#refundBox").hide();
			$("#refundReturnBox").show();
			fileLi1 = $("#refundReturnForm").find("#fileLi1");
		}

		if(fileLi1.find("#addBtn").length <= 0){
			var addBtn = $("<input id='addBtn' type='button' value='添加' onclick='addFile(this)'/>");
			fileLi1.append(addBtn);
		}
		fileLi1.find("#photoFile1").val("");
		fileLi1.next().remove();
		
		oldAapplyType = value;
		fileCount = 1;
	});
	
	//仅退款表单数据校验
	$("#refundForm").validate({
	 		ignore: ".ignore",
/*		    errorPlacement: function(error, element) {
		    	element.parent().find("em").html("");
				error.appendTo(element.parent().find("em"));
			},*/
	        rules: {
	        	orderId: {
		            required: true
		        },
		        orderItemId: {
		        	required: true
		        },
		        buyerMessage: {
		            required: true
		        },
		        refundAmount: {
		            required: true,
		            isNumber: true,
		            isValidMoney: true,
		            checkRefundMoney : true,
		            minNumber: true
		        },
		        reasonInfo: {
		            required: true,
		            minlength: 5,
		            maxlength: 300
		        },
		        /*photoFile1: {
		            required: true
		        }*/
	     }, errorPlacement: function(error, element){
				if(element.is(refundAmount) || element.is(photoFile1)){
					error.insertAfter(element.next().next());
				}else{
					error.insertAfter(element);
				}
		},
	     messages: {
	        	orderId: {
		            required: "对不起,没有订单Id!"
		        },
		        orderItemId: {
		        	required: "对不起,没有订单项ID!"
		        },
		        buyerMessage: {
		            required: "请选择退款原因!"
		        },
		        refundAmount: {
		            required: "请输入退款金额!",
		            isNumber: "请输入正确的金额!",
		            isValidMoney: "对不起,金额必须大于0!",
		            checkRefundMoney : "对不起,退款金额不能大于￥" + maxRefundMoney + " !"
		        },
		        reasonInfo: {
		            required: "请输入退款说明!",
		            minlength: "退款说明不能少于5个字符",
		            maxlength: "退款说明不能超过300个字符!"
		        },
		        /*photoFile1: {
		            required: "至少添加一个退款凭证图片!"
		        }*/
	     },
	     
	     submitHandler : function(form){
	     	if(!isSubmitted){
	     		
	     		layer.msg('请稍候……', { icon: 16, shade: 0.01,shadeClose:false,time:60000 });
	     		//执行图片压缩代码后，提交表单
				var photoFile;
				var formData = new FormData(form);
				var arr = [];
				
				form = $(form);
				var j = form.find(".photoFile").size();
				console.log("jjj:"+j);
				//删除原有文件
				console.log("content:"+formData.get("content"));
				for (let i = 0; i < j ; i++) {
					photoFile = form.find(".photoFile")[i].files[0];
					if (photoFile!=""&&photoFile!=undefined&&photoFile!=null){
						arr.push(photoFile);
						console.log("photoFile:" + photoFile+"size:"+photoFile.size)
					}
				}
				console.log("arrFile:"+arr);
				if(arr && arr.length){
					imgCompress.batchCompress(arr, function(compressImg){
						console.log("compressImg:"+compressImg)
						//将图片加入formData
						for (let i = 0; i < arr.length ; i++) {
							if (compressImg[i]!=""&&compressImg[i]!=undefined&&compressImg[i]!=null){
								console.log("压缩后图片:"+compressImg[i]+":"+compressImg[i].size);
								formData.set("photoFile"+(i+1), compressImg[i]);
							}
						}
						console.log("提交文件"+formData.getAll("photos"))
						submitRefundForm(formData,form);
					});
				}else {
					submitRefundForm(formData,form);
				}
	     		isSubmitted = true;
	     	}else{
	     		layer.alert("对不起,请不要重复提交表单!",{icon: 0});
	     	}
	     } 
	 });
	
	//退款且退货表单数据校验
	$("#refundReturnForm").validate({
		ignore: ".ignore",
		rules: {
			orderId : {
				required: true
			},
			orderItemId: {
				required: true
			},
			buyerMessage: {
				required: true
			},
			refundAmount: {
				required: true,
				isNumber: true,
				isValidMoney: true,
				checkRefundMoney2 : true,
				minNumber: true
			},
			goodsNum: {
				required: true,
				isIntegral: true,
				min: 1,
				checkGoodsCount: true
			},
			reasonInfo: {
				required: true,
				minlength: 5,
				maxlength: 300
			},
			photoFile1: {
				required: true
			}
		}, errorPlacement: function(error, element){
			if(element.is(goodsNum) || element.is(refundAmount) || element.is(photoFile1)){
				error.insertAfter(element.next().next());
			}else{
				error.insertAfter(element);
			}
		},
		messages: {
			orderId: {
				required: "对不起,没有订单Id!"
			},
			orderItemId: {
				required: true
			},
			buyerMessage: {
				required: "请选择退货原因!"
			},
			refundAmount: {
				required: "请输入退款金额!",
				isNumber: "请输入正确的金额!",
				isValidMoney: "对不起,金额必须大于0!",
				checkRefundMoney2 : "对不起,退款金额超出最大值 !"
			},
			goodsNum: {
				required: "请输入退货数量!",
				min: "对不起,退货数量必须1件或一件以上!",
				checkGoodsCount: "对不起,退货数量不能大于 " + maxGoodsCount + " !"
			},
			reasonInfo: {
				required: "请输入退款退货说明!",
				minlength: "退款退货说明不能少于5个字符",
				maxlength: "退款退货说明不能超过300个字符!"
			},
			photoFile1: {
				required: "至少添加一个退款凭证图片!"
			}
		},
		submitHandler : function(form){
			if(!isSubmitted){
				
				layer.msg('请稍候……', { icon: 16, shade: 0.01,shadeClose:false,time:60000 });
				//执行图片压缩代码后，提交表单
				var photoFile;
				var formData = new FormData(form);
				var arr = [];
				
				form = $(form);
				var j = form.find(".photoFile").size();
				console.log("jjj:"+j);
				//删除原有文件
				console.log("content:"+formData.get("content"));
				for (let i = 0; i < j ; i++) {
					photoFile = form.find(".photoFile")[i].files[0];
					if (photoFile!=""&&photoFile!=undefined&&photoFile!=null){
						arr.push(photoFile);
						console.log("photoFile:" + photoFile+"size:"+photoFile.size)
					}
				}
				console.log("arrFile:"+arr);
				if(arr && arr.length){
					imgCompress.batchCompress(arr, function(compressImg){
						console.log("compressImg:"+compressImg)
						//将图片加入formData
						for (let i = 0; i < arr.length ; i++) {
							if (compressImg[i]!=""&&compressImg[i]!=undefined&&compressImg[i]!=null){
								console.log("压缩后图片:"+compressImg[i]+":"+compressImg[i].size);
								formData.set("photoFile"+(i+1), compressImg[i]);
							}
						}
						console.log("提交文件"+formData.getAll("photos"))
						submitRefundReturnForm(formData,form);
					});
				}else {
					submitRefundReturnForm(formData,form);
				}
				isSubmitted = true;
			}else{
				layer.alert("对不起,请不要重复提交表单!");
			}
		} 
	});
	
	
	//选择是否退定金
	$(document).on("click", "#isRefundDeposit", function(){
 		if($(this).attr("checked")=="checked"){
 			$(this).parent().parent().addClass("checkbox-wrapper-checked");
 			 $(this).attr("checked",true);
 	     }else{
 	    	 $(this).attr("checked",false);
 	         $(this).parent().parent().removeClass("checkbox-wrapper-checked");
 	     }
 	 });
	
	
});

jQuery.validator.addMethod("checkRefundMoney", function(value, element) {       
	return Number(value) <= maxRefundMoney;
}); 

jQuery.validator.addMethod("checkRefundMoney2", function(value, element) {       
	return Number(value) <= finalRefundMoney;
});

jQuery.validator.addMethod("checkGoodsCount", function(value, element) {       
	return Number(value) <= maxGoodsCount;
});  

jQuery.validator.addMethod("isNumber", function(value, element) {       
    return this.optional(element) || /^[-\+]?\d+$/.test(value) || /^[-\+]?\d+(\.\d+)?$/.test(value);       
}, "必须整数或小数");  

//自定义validate验证输入的数字小数点位数不能大于两位
jQuery.validator.addMethod("minNumber",function(value, element){
    var returnVal = true;
    inputZ=value;
    var ArrMen= inputZ.split(".");    //截取字符串
    if(ArrMen.length==2){
        if(ArrMen[1].length>2){    //判断小数点后面的字符串长度
            returnVal = false;
            return false;
        }
    }
    return returnVal;
},"小数点后最多为两位");

jQuery.validator.addMethod("isValidMoney", function(value, element) { 
    value = parseFloat(value);      
    return this.optional(element) || value> 0;       
}, "金额必须要大于0元"); 

jQuery.validator.addMethod("isIntegral", function(value, element){
	return this.optional(element) || /^([+]?)(\d+)$/.test(value);
}, "退货数量必须是正整数");


//提交退货且退款表单
function submitRefundReturnForm(formData,form){
	$.ajax({
		url: form.attr("action"),
		type: "POST",
		data: formData,
		dataType: "JSON",
		timeout: 10000, //十秒超时
		async: true,
		//要想用jquery的ajax来提交FormData数据,
		// 则必须要把这两项设为false
		processData: false,
		contentType: false,
		success: function(result){
			
			if (result == 'OK') {
				layer.msg("申请成功",{icon:1,time:30000});
				window.location.href = contextPath+"/p/refundReturnList/2";
			}else{
				
				layer.msg(result,{icon:0});
			}
		}
	});
}
//提交退货退款选择仅退款表单
function submitRefundForm(formData,form){
	$.ajax({
		url: form.attr("action"),
		type: "POST",
		data: formData,
		dataType: "JSON",
		timeout: 10000, //十秒超时
		async: true,
		//要想用jquery的ajax来提交FormData数据,
		// 则必须要把这两项设为false
		processData: false,
		contentType: false,
		success: function(result){
			
			if (result == 'OK') {
				layer.msg("申请成功",{icon:1,time:30000});
				window.location.href = contextPath+"/p/refundReturnList/1";
			}else{
				
				layer.msg(result,{icon:0});
			}
		}
	});
}
