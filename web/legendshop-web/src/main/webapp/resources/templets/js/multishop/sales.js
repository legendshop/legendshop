$(function(){
	sales.doExecute();
	userCenter.changeSubTab("sales");
});

function pager(curPageNO){
    document.getElementById("curPageNO").value=curPageNO;
    document.getElementById("ListForm").submit();
}

//路径配置
require.config({
	paths: {
        echarts: contextPath+'/resources/plugins/ECharts/dist'
    }
});

//使用
require(
		[ 
	        'echarts', 
	        'echarts/chart/bar',
	        'echarts/chart/line'
	    ],
	    function (ec) {
			// 基于准备好的dom，初始化echarts图表
	        var myChart = ec.init(document.getElementById('main')); 
			
	        option = {
	        	    tooltip : {
	        	        trigger: 'axis'
	        	    },
	        	    toolbox: {
	        	        show : true,
	        	        feature : {
	        	            mark : {show: true},
	        	            dataView : {show: true, readOnly: false},
	        	            magicType: {show: true, type: ['line', 'bar']},
	        	            restore : {show: true},
	        	            saveAsImage : {show: true}
	        	        }
	        	    },
	        	    calculable : true,
	        	    legend: {
	        	        data:['成交数量','成交金额']
	        	    },
	        	    xAxis : [
	        	        {
	        	            type : 'category',
	        	            data : getXAxisData()
	        	        }
	        	    ],
	        	    yAxis : [
	        	        {
	        	            type : 'value',
	        	            name : '成交数量'	  
	        	        },
	        	        {
	        	            type : 'value',
	        	            name : '成交金额'
	        	        }
	        	    ],
	        	    series : [

	        	        {
	        	            name:'成交数量',
	        	            type:'bar',
	        	            itemStyle:{
	                              normal:{color:'#2ec7c9'}
	                         },
	        	            data : getBarData()
	        	        },
	        	        {
	        	            name:'成交金额',
	        	            type:'line',
	        	            yAxisIndex: 1,
	        	            itemStyle:{
	                              normal:{color:'#b6a2de'}
	                        },
	        	            data : getLineData()
	        	        }
	        	    ]
	        	};
	        
	     	// 为echarts对象加载数据 
	        myChart.setOption(option); 
		}
);

/**方法，判断是否为空 **/
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}

//获取 x轴 数据
function getXAxisData(){
	var list = [];
	var xAxisJson = $("#xAxisJson").val(); 
	if(!isBlank(xAxisJson)){
	var xAxisData = jQuery.parseJSON(xAxisJson);
    	for(var y=0;y<xAxisData.length;y++){
        	list.push(xAxisData[y]);
        }
	}
    return list;
}

//获取 销量 数据
function getBarData(){
	var list = [];
	var subCountsJson =$("#subCountsJson").val();
	if(!isBlank(subCountsJson)){
		var subCounts = jQuery.parseJSON(subCountsJson);
        for(var x=0;x<subCounts.length;x++){
        	list.push(subCounts[x]);
        }
	}
    return list;
}

//获取 销售金额 数据
function getLineData(){
	var list = [];
	var subCashJson =$("#subCashJson").val();
	if(!isBlank(subCashJson)){
		var subCash = jQuery.parseJSON(subCashJson);
        for(var x=0;x<subCash.length;x++){
        	list.push(subCash[x]);
        }
	}
    return list;
}