//店铺分数
$(function(){
  $.each($(".scroll_red"),function(){
    var width=parseInt($(this).parents("li").find("font").text())*2+"0%";
    $(this).css("width",width);
  })
})
//收藏店铺
function collectShop(shopId) {
  jQuery.ajax({
    url : contextPath + "/isUserLogin",
    type : 'get',
    async : false, // 默认为true 异步
    success : function(result) {
      if(result=="true"){
        jQuery.ajax({
          url : contextPath + "/p/favoriteShopOverlay?shopId="+shopId,
          async : false, // 默认为true 异步
          dataType : 'json',
          success : function(result) {
            if(result=="fail"){
              login();
            }else if(result=="OK"){
              layer.alert("收藏店铺成功",{icon: 1});
            }else if(result="isexist"){
              layer.msg("您已收藏该店铺，无须重复收藏",{icon: 0});
            }
          }
        });
      }else{
        login();
      }
    }
  });

}
function loginIm() {
  var user_name = $("#user_name").val();
  var user_pass = $("#user_pass").val();
  var skuId = $("#skuId").val();
  if (user_name == '' || user_pass == '') {
    login();
  } else {
    window.open(contextPath + "/p/im/index/" + skuId, '_blank');
  }
}
