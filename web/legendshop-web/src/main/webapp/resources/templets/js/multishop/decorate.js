/**
 *  Tony 
 */
jQuery(document).ready(function(){
	//切换菜单
	jQuery(".f_top_nav li").click(function(){
       jQuery("div[div_tab='decorate']").hide();
	   var tab_target=jQuery(this).attr("tab_target");
	   jQuery("div[id="+tab_target+"]").show();
	   jQuery(".f_top_nav li").removeClass("this");
	   jQuery(this).addClass("this");
	   jQuery.cookie("tab_target",tab_target);
   });
	//通过cookie记录当前装修菜单
	var tab_target = jQuery.cookie("tab_target");
	if(tab_target!=null){
		jQuery("div[div_tab='decorate']").hide();
		   jQuery("div[id="+tab_target+"]").show();
		   jQuery(".f_top_nav li").removeClass("this");
		   jQuery("li[tab_target='"+tab_target+"']").addClass("this");
	 }
	
	//背景设置
	jQuery("#bg_img").live("change",function(){	
		 if(!validateFile()){
			 return false;
		 }
        var bg_img_id =jQuery("#bgImgId").val();
		jQuery("#bg_img_show").val(jQuery(this).val());
		jQuery.ajaxFileUpload({
		      url: contextPath + '/s/shopDecotate/decorate_background_upload',             
		      secureuri:false,
		      fileElementId:'bg_img',                         
		      dataType: 'json',
		      error: function (data, status, e){
		    	  layer.alert(data, {icon: 2});
		       },
		       success: function (data, status){  
		     	  if(data =="fail"){
		     		 layer.alert(data, {icon: 2});
		     	 }else{
					 jQuery("#bgImgId").val(data);
					 jQuery(".fiit_bg_prew img").attr("src",photoPath+data);
		     	 }
		       }
		    });
	 });
	
	//背景颜色							
	$("#bgColor").bigColorpicker(function(el,color){
	    jQuery(el).val(color);
	    jQuery(el).parent().find(".color_block").css("background-color",color);
	});
	
	if(color_block!=""){
		$("#bgColor").parent().find(".color_block").css("background-color",color_block);
	}
	
	
	//页面加载模块
	$("div[id=content]").each(function (){
		var layoutId = jQuery(this).attr("mark");
		var layoutDiv = jQuery(this).attr("div");
		var layout=jQuery(this).attr("layout");
		var layoutModuleType=jQuery(this).attr("type");
		var layoutDivId=jQuery(this).attr("layoutDivId");
		if(isBlank(layoutModuleType)){
			layoutModuleType="";
		}
		if(isBlank(layoutDiv)){
			layoutDiv="";
		}
		if(isBlank(layoutDivId)){
			layoutDivId="";
		}
		if(layoutModuleType!=""){
			  $(this).load(contextPath+"/s/shopDecotate/lodingLayouModule", {"layoutId" : layoutId, "layoutDiv" : layoutDiv,"layout":layout,"layoutModuleType":layoutModuleType,"divId":layoutDivId}, function(){
			  });
	    }
    });

	
});

function changeColor(_this){
	var color=$(_this).val();
	if(color==null || color=="" || color==undefined){
		return ;
	}
	$("#bgColor").parent().find(".color_block").css("background-color",color);
}


/**
 * 撤销
 */
function revocation(){
	layer.confirm('当前装修信息数据将会丢失，是否继续?', {icon:3},function () {
		 $.ajax({
	        //提交数据的类型 POST GET
	        type:"POST",
	        //提交的网址
	        url:contextPath+"/s/shopDecotate/revocation",
	        //提交的数据
	        async : false,  
	        //返回数据的格式
	        datatype: "json",
	        //成功返回之后调用的函数            
	        success:function(data){
	        	 var result=eval(data);
	        	 if(result=="OK"){
	        
	        		 layer.alert('撤销成功！', {icon: 1},function(){
	                		window.location.reload();
	                	});
	       
	        	 }else{
	        		 layer.alert('撤销失败', {icon: 2});
	        	 }
	        },
	        //调用执行后调用的函数
	        complete: function(XMLHttpRequest, textStatus){
	        }    
	    });
	 }, function () {
     });
}

/**
 * 退出
 */
function closewin(){
	layer.confirm('确定要退出吗？', {
 		 icon: 3
  	     ,btn: ['确定','取消'] //按钮
  	   },function () {
		window.open("about:blank","_self").close();
	});
}



function subquite(){
	layer.confirm('确认保存现在的店铺装修数据?',{icon:3}, function () {
		 $.ajax({
	        //提交数据的类型 POST GET
	        type:"POST",
	        //提交的网址
	        url:contextPath+"/s/shopDecotate/decorateSaveQuite",
	        //提交的数据
	        async : false,  
	        //返回数据的格式
	        datatype: "json",
	        //成功返回之后调用的函数            
	        success:function(data){
	        	 var result=eval(data);
	        	 if(result=="OK"){
	        		 layer.alert('数据保存成功！', {icon: 1},function(){
	        			 window.open("about:blank","_self").close();
	                	});
	        		 
	        	 }else{
	        		 layer.alert('保存退出失败', {icon: 2});
	        	 }
	        },
	        //调用执行后调用的函数
	        complete: function(XMLHttpRequest, textStatus){
	        }    
	    });
	 }, function () {
     });
}



//设置基础模块	
function set_decorate_base_module(obj){
	var mark =jQuery(obj).attr("mark");
	var status =jQuery(obj).attr("class");
	
	if("banner"==mark){
		if(exist_bannerPic=="true"){
			 layer.alert('亲,您还没有设置商铺默认Banner,请前往店铺进行设置！', {icon: 0});
			 window.open(contextPath+'/s/shopSetting','_blank');
			 return false;
		}
	}else if("nav"==mark){
		if(exist_shopNavs=="true"){
			 layer.alert('亲,您还没有设置默认商家导航,请前往导航管理设置！', {icon: 0});
			 window.open(contextPath+'/s/shopNav/load','_blank');
			 return false;
		}
	}
	else if("slide"==mark){
		if(exist_shopBanners=="true"){
			 layer.alert('亲,您还没有设置默认轮播图片,请前往轮播图管理设置！', {icon: 0});
			 window.open(contextPath+'/s/shopBanner/list','_blank');
			 return false;
		}
	}
	
	if(status=="on"){
		jQuery(obj).attr("class","off");
		var status="0";
		jQuery("#store_"+mark).hide();
		 
		jQuery.ajax({
	         //提交数据的类型 POST GET
	         type:"POST",
	         //提交的网址
	         url:contextPath+"/s/shopDecotate/set_decorate_base_module",
	         //提交的数据
	         data:{"type":mark,"status":status},
	         async : false,  
	         //返回数据的格式
	         datatype: "text",
	         //成功返回之后调用的函数            
	         success:function(html){
	         },
	         //调用执行后调用的函数
	         complete: function(XMLHttpRequest, textStatus){
	         }       
	   });
		 
		 
  }else{
		jQuery(obj).attr("class","on");	
		var status="1";
		jQuery("#store_"+mark).show();	
		 jQuery.ajax({
	         //提交数据的类型 POST GET
	         type:"POST",
	         //提交的网址
	         url:contextPath+"/s/shopDecotate/set_decorate_base_module",
	         //提交的数据
	         data:{"type":mark,"status":status},
	         async : false,  
	         //返回数据的格式
	         datatype: "text",
	         //成功返回之后调用的函数            
	         success:function(html){
	         },
	         //调用执行后调用的函数
	         complete: function(XMLHttpRequest, textStatus){
	         }      
	   }); 
		 
   }
}

function bg_set(method){
	if(method=="cancle"){
		jQuery("#bgImgId").val("");
		jQuery("#bgColor").val("");
		jQuery("#bg_img_show").val("");
	}	
	 
	 var bgImgId=$("input[name='bgImgId']").val();
	 var method=method;
	 var bg_color=$("input[name='bg_color']").val();
	 var repeat= $(':radio[name="repeat"]:checked').val(); 
	 
	 var data={"bgImgId":bgImgId,"method":method,"bg_color":bg_color,"repeat":repeat}
	 
	 jQuery.ajax({
         //提交数据的类型 POST GET
         type:"POST",
         //提交的网址
         url:contextPath+"/s/shopDecotate/background_set",
         //提交的数据
         data:data,
         async : false,  
         //返回数据的格式
         datatype: "html",
         //成功返回之后调用的函数            
         success:function(html){
        	 location.reload();
         },
         //调用执行后调用的函数
         complete: function(XMLHttpRequest, textStatus){
         }    
   });
}

function validateFile() {
	var _this=jQuery("input[name='bg_img']");
	var filepath = jQuery("input[name='bg_img']").val();
	var extStart = filepath.lastIndexOf(".");
	var ext = filepath.substring(extStart, filepath.length).toUpperCase();
	if (ext != ".BMP" && ext != ".PNG" && ext != ".GIF" && ext != ".JPG"
			&& ext != ".JPEG") {
		layer.alert('图片限于bmp,png,gif,jpeg,jpg格式!', {icon: 0});
		return false;
	}
	return true;
}


//添加新布局模块
function addShopDecLoyout(obj){
	   jQuery(".no_decorate").remove(); //去掉没有装修的说明
	   var layout =jQuery(obj).attr("layout");
	   jQuery.ajax({
           //提交数据的类型 POST GET
           type:"POST",
           //提交的网址
           url:contextPath+"/s/shopDecotate/shopDecLoyout",
           //提交的数据
           data:{"layout":layout},
           async : false,  
           //返回数据的格式
           datatype: "html",
           //成功返回之后调用的函数            
           success:function(html){
        	   if(html.indexOf('ERROR')!=-1){
        		   layer.alert('添加失败', {icon: 2});
        		   return false;
        	   }
        	   else if(html.indexOf('OVER_LIMIT')!=-1){
        		   layer.alert('添加布局失败,超过限制,只能20个', {icon: 2});
        		   return false;
        	   }
        	   if (layout == "layout0") {
       			  jQuery("#main_layout").before(html);
	       		}
	       		if (layout == "layout1") {
	       			jQuery("#main_layout").after(html);
	       		}
	       		if (layout == "layout2" || layout == "layout3" || layout == "layout4") {
	       			jQuery("#main_layout").append(html);
	       		}
           }   ,
           //调用执行后调用的函数
           complete: function(XMLHttpRequest, textStatus){
           }       
     });
	   
}


//删除布局模块
function dele_layout(obj){
	 var layoutDiv = jQuery(obj).attr("div");
	 var layoutDivId= jQuery(obj).attr("layoutDivId");
	 var layout = jQuery(obj).attr("layout");
	 var layoutId = jQuery(obj).attr("mark");
	 var layoutModuleType=jQuery(obj).attr("moduleType");
	 if(isBlank(layoutId) || isBlank(layout)){
		 layer.alert('删除数据有问题', {icon: 2});
		 return false;
	 }
	 var data={"layout":layout,"layoutId":layoutId,"layoutDiv":layoutDiv,"divId":layoutDivId,"layoutModuleType":layoutModuleType};
	 layer.confirm('确定要删除数据吗?',{
  		 icon: 3
  	     ,btn: ['确定','取消'] //按钮
  	   }, function () {
		 jQuery.ajax({
	         //提交数据的类型 POST GET
	         type:"POST",
	         //提交的网址
	         url:contextPath+"/s/shopDecotate/deleLayout",
	         //提交的数据
	         data:data,
	         async : false,  
	         //返回数据的格式
	         datatype: "json",
	         //成功返回之后调用的函数            
	         success:function(data){
	        	 var result=eval(data);
	        	 if(result=="OK"){
						location.reload();
				 }else{
					 layer.alert('删除失败', {icon: 2});
				 }
	         }   ,
	         //调用执行后调用的函数
	         complete: function(XMLHttpRequest, textStatus){
	         }     
	    });
	 });
	  
}

function decorate_module(obj){
	
	var layoutId = jQuery(obj).attr("mark");
	var layout = jQuery(obj).attr("layout");
	var moduleType=jQuery(obj).attr("moduleType");
	var div=jQuery(obj).attr("div"); //div1 or div2 
	var layoutDivId=jQuery(obj).attr("layoutDivId"); //div1 id or div2 id
	var seq=jQuery(obj).attr("seq");
	if(isBlank(layoutId) || isBlank(layout)){
		 layer.alert('数据有问题', {icon: 2});
		 return false;
	}
	if(isBlank(div)){
		div="";
	}
	if(isBlank(div)){
		layoutDivId="";
	}
	
	
	var urlPath=contextPath+"/s/shopDecotate/decorateModule?layoutId="+layoutId+"&layout=" +
			""+layout+"&layoutModuleType="+moduleType+"&layoutDiv="+div+"&divId="+layoutDivId+"&seq"+seq;
	
	layer.open({
		  title :"模块编辑",
		  id: "decorateModule",
		  type: 2, 
		  resize: true,
		  content: [urlPath,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['480px', '450px']
		}); 
}


function decorate_module_set(obj){
	
	var layoutId = jQuery(obj).attr("mark");
	var layout = jQuery(obj).attr("layout");
	var moduleType="loyout_module_custom_prod";
	
	
	var urlPath=contextPath+"/s/shopDecotate/decorateModuleSet?layoutId="+layoutId+"&layout=" +
			""+layout+"&layoutModuleType="+moduleType;
	
	layer.open({
		  title :"模块设置",
		  id: "decorateModuleSet",
		  type: 2, 
		  resize: true,
		  content: [urlPath,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['480px', '300px']
		}); 
}






function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}

function show(){
	$("#module_edit").show();
}
function hide(){
	$("#module_edit").hide();
}




