$(document).ready(function() {
	getList();
	try {
		 $("#_keyword").autocomplete(contextPath+'/seach/suggest',{
				     max: 10,    //列表里的条目数
				     minChars:1,    //自动完成激活之前填入的最小字符
				     multiple: false,
				     matchCase:true,
				     matchContains: true,//是否全匹配, 如数据中没有此数据,将无法输入
				     autoFill: false,    //自动填充
				     dataType: "json",
				     extraParams:{keyword:function(){
				    	    var search=stripscript($.trim($('#_keyword').val()));
				    	    return search;
				        }
				     },
			         parse: function(data) {
		              return $.map(data, function(row) {
		                  return {
		                      data: row,
		                      value: row,
		                      result: row
		                  }
		              });
		           },
		             formatItem: function(row, i, max) {
							return  "<span class='ac_name'>"+row + "</span>";
						},
						formatMatch: function(row, i, max) {
							return row + " ";
						},
						formatResult: function(row) {
							return row;
						}
			    }).result(function(e, item) {
		      });
			} catch (e) {
				console.log(e);
		}
}); 

function searchproduct(){
	 var keywork=$("#_keyword").val();
	 checkCookie($.trim(keywork));
	 document.getElementById("searchform").submit();	
}

$('#_keyword').on("keydown", function(event) {
    var key = event.which;
   // console.log(key)
    if (key == 13) {
    	searchproduct();
    }
});

$("#_keyword").on('click',function(){
    $('#search-tip').show();
});

$("#search-his-del").on('click',function(){
	clearCookie('searchTitle');
});

$("#_keyword").on('blur',function(){
    $('#search-tip').hide();
});

$('#search-tip').hover(function(){
    $("#_keyword").off('blur');
},function(){
    $("#_keyword").on('blur',function(){
        $('#search-tip').hide();
    });
});

function checkCookie(obj) {
    var title=getCookie("searchTitle");
    var list=spiltCookie(title);
    var str="";
    var count=0;
    for(var i =0;i<list.length;i++){
        if(count<8){
            str+=list[i]+'==='
            if(list[i]==encodeURI(obj)) {
                obj="";
            }
            count++;
        }

    }
    str=encodeURI(obj)+"==="+str;
    setCookie("searchTitle",str,1);
}

function spiltCookie(dValue){
    var strs= new Array(); //定义一数组
    strs=dValue.split("===");
    var list= new Array(); //定义一数组
    for(var i =strs.length-1;i>=0;i--){
        if(strs[i]!=""){
            list.push(strs[i]);
        }
    }
    return strs;
}

//获取cookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

 //清除cookie
function clearCookie(name) {
    setCookie(name, "", -1);
    getList();
}

function getList(){
    var title=getCookie("searchTitle");
    if(title){
        var str="";
        var count=0;
        
        var list=spiltCookie(title);
        if(list && list.length){
            for(var i =0;i<list.length;i++){
                if(count <9&&list[i]!=""){
                    count++;
                    str+='<li><a data-hot-value="'+decodeURIComponent(list[i])+'"  href="'+contextPath+'/list?keyword='+decodeURIComponent(list[i])+'">'+decodeURIComponent(list[i])+'</a></li>';
                }
            }
            $("#search-his-list").html(str);
        }
    }else{
    	$("#search-his-list").html("");
    }
    
}

function stripscript(s){   
    var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
    var rs = "";   
    for (var i = 0; i < s.length; i++) {   
         rs = rs+s.substr(i, 1).replace(pattern, '');   
    }   
    rs = rs.replace(/<(script)[\S\s]*?\1>|<\/?(a|img)[^>]*>/gi, "");
    return rs;   
}  
//function isIE(){
window.onload=function (){
	$(function(){
	       var UA = navigator.userAgent,
	          isIE = UA.indexOf('MSIE') > -1,
	          v = isIE ? /\d+/.exec(UA.split(';')[1]) : 'no ie';  
	     if(v<=8)  //ie8及以下版本
	      { 
	    	 //获取热搜链接
	    	 var a_ie = $("a[name='a_ie']");
	    	 for(var i=0;i<a_ie.length;i++){
	    		 var a_ie_href = a_ie[i].href;
	    		 //热搜链接中文进行转码
		    	 var a_ie_href_encode = encodeURI(a_ie_href);
		    	 a_ie[i].href = a_ie_href_encode; 
	    	 }
	      } 
	     });
	 }
//}

		