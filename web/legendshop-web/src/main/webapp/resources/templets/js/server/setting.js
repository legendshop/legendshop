$(function(){
	  //登录
    chat.login = function (cmd,customId,name,shopId,sign) {
        var options = {
        	cmd : cmd,
        	customId: customId,
        	name: name,
        	shopId: shopId,
        	sign: sign,
        };
        websocket.send(JSON.stringify(options));
    }
    
    //心跳检测
    var heartCheck = {
        timeout: 30000,        //30秒发一次心跳
        timeoutObj: null,
        serverTimeoutObj: null,
        reset: function () {
            clearTimeout(this.timeoutObj);
            clearTimeout(this.serverTimeoutObj);
            return this;
        },
        start: function () {
            var self = this;
            this.timeoutObj = setTimeout(function () {
                //这里发送一个心跳，后端收到后，返回一个心跳消息，
                //onmessage拿到返回的心跳就说明连接正常
                websocket.send("{cmd : 9}");
                console.log("心跳------"+new Date());
                self.serverTimeoutObj = setTimeout(function () {
	                 //如果超过一定时间还没重置，说明后端主动断开了
	                 //如果onclose会执行reconnect，我们执行ws.close()就行了.如果直接执行reconnect 会触发onclose导致重连两次
	                 websocket.close();     
	             },self.timeout)
            },this.timeout)
        }
    }
    
});



    

$("#nav").children().click(function() {
	var $con = $("#nav").siblings().children().eq($(this).index());
	$con.show();
	$con.siblings().hide();
	$(this).addClass("cur");
	$(this).siblings().removeClass("cur");
});

$("#edit").click(function() {
	$("#personal").find("input").removeAttr("readonly");
	$("#personal").find("textarea").removeAttr("readonly");
	$(this).hide();
	$(this).siblings().show();
});

$("#personal").validate({
	rules : {
		name : {
			required : true
		}
	},
	messages : {
		name : {
			required : "名称不能为空"
		}
	},
	submitHandler : function() {
		$.ajax({
			url : "update",
			type : "POST",
			dataType : "JSON",
			data : $("#personal").serialize(),
			success : function(result) {
				if (result === "ok") {
					art.dialog.tips("修改成功");
					setTimeout(function() {
						location.reload();
					}, 1000);
				} else if (result === "login") {
					location.href = "login";
				} else {
					art.dialog.tips(result);
				}
			}
		});
	}
});

$.validator.addMethod("password", function(value) {
	var regx = /(?!.*[\u4E00-\u9FA5\s])(?!^[a-zA-Z]+$)(?!^[\d]+$)(?!^[^a-zA-Z\d]+$)^.{6,15}$/;
	if (!regx.test(value)) {
		return false;
	}
	return true;
},"密码由6-15位字母、数字或符号的两种及以上组成！");

$("#password").validate({
	rules : {
		old : {
			required : true
		},
		password : {
			required : true,
			password : 0
		},
		confirm : {
			required : true,
			equalTo : "#new"
		}
	},
	messages : {
		old : {
			required : "密码不能为空"
		},
		password : {
			required : "密码不能为空",
			password : "密码由6-15位字母、数字或符号的两种及以上组成！"
		},
		confirm : {
			required : "密码不能为空",
			equalTo : "密码必须一致"
		}
	},
	submitHandler : function() {
		$(".item-btn").attr("disabled",true);
		$.ajax({
			url : contextPath+"/customerService/updatePassword",
			type : "POST",
			dataType : "JSON",
			data : $("#password").serialize(),
			success : function(result) {
				$(".item-btn").attr("disabled",false);
				if (result === "ok") {
					art.dialog.tips("修改成功");
					setTimeout(function() {
						location.reload();
					}, 1000);
				} else if (result === "login") {
					location.href = "login";
				} else {
					art.dialog.tips(result);
				}
			}
		});
	}
});
