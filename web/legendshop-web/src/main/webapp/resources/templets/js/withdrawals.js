var ALLOW_SEND = true;
    $(document).ready(function() {
		userCenter.changeSubTab("mydeposit");
		changeRandImg(contextPath);
        $('.submit').on('click',function(){
		    if (!$('#auth_form').valid()){
			     //changeRandImg('${pageContext.request.contextPath}');
		    } else {
			    $('#auth_form').submit();
		    }
	    });
	    
	    $('#send_auth_code').on('click',function(){
			if (!ALLOW_SEND) return;
			var auth_type=$.trim($("#authType").val());
			var codeimage = $("#captcha").val();
			if(auth_type=="" || auth_type==null ){  
			    layer.alert("请选择一种身份认证方式",{icon: 0});
			    return false;
			}else if(codeimage==""){
				layer.alert("请输入正确的图形验证码",{icon: 0});
			    return false;
			}
			ALLOW_SEND = !ALLOW_SEND;
			$('#sending').show();
			$.ajax( {
					 url: contextPath + "/p/predeposit/autoType/"+auth_type+"?codeimage="+codeimage, 
					 type:'GET', 
					 async : false, //默认为true 异步   
					 dataType:"json",
					success:function(result){
						if('OK' == result){
						   	$('#sending').hide();
							$('#show_times').html(180);
						    $('.send_success_tips').show();
						    setTimeout(StepTimes,1000);
						}else if("errorCode"==result){
							$('#sending').hide();
							layer.alert("请输入正确的图形验证码",{icon: 0});
							 ALLOW_SEND = !ALLOW_SEND;
						}else if('fail'==result){
						    ALLOW_SEND = !ALLOW_SEND;
				            $('#sending').hide();
				            layer.alert("发送失败",{icon: 2});
						}else{
						    ALLOW_SEND = !ALLOW_SEND;
				            $('#sending').hide();
				            layer.alert(result,{icon: 2});
						}
					}
			});
		
		});
		
	   function StepTimes() {
			$num = parseInt($('#show_times').html());
			$num = $num - 1;
			$('#show_times').html($num);
			if ($num <= 0) {
				ALLOW_SEND = !ALLOW_SEND;
				$('.send_success_tips').hide();
			} else {
				setTimeout(StepTimes,1000);
			}
		 }
		 
		 
  	});
  	
  	jQuery.validator.addMethod("eqCash", function (value, element) {  
  	        var available_cash=$.trim($("#available_cash").html());
  	        var cash=parseFloat(available_cash);
  	        var _value=parseFloat(value);
  	        if(_value>cash){
  	           return false;
  	        }
            return true;
        }, "不能超出账户余额");  
  	
  	$('#auth_form').validate({
		        rules : {
			         bankName:{
			           required : true
			        },
			        bankNo:{
			           required : true
			        },
			        bankUser:{
			           required : true,
			           rangelength: [2, 20]
			        /* 少数名族名字长*/
			           
			        },
		           amount:{
			        	required  : true,
			            number    : true,
			            min       :minAmount,
			            eqCash:true
		            },
		            userNote:{
		                maxlength: 110
		            },
		            authtType:{
		              required : true
		            },
		        	authCode : {
		                required : true,
		                maxlength : 6,
		                minlength : 6,
		                digits : true
		            },
		            captcha : {
		                required : true,
		                minlength: 4
		            } 
		        },
		        messages : {
		             bankName:{
			           required : "请选择提现方式"
			        },
			        bankNo:{
			          required : "请填写您的帐号信息"
			        },
			        bankUser:{
			           required : "请填写您的真实姓名",
			           rangelength: "长度在2位至20位"
			        },
		            amount: {
			            	required  :'请输入提现金额',
			            	number    :"输入正确格式,提现金额为大于或者等于"+minAmount+"的数字",
			                min    	  :"提现金额为大于或者等于"+minAmount+"的数字",
			                eqCash:"不能超出账户余额"
			        },
			        userNote:{
		                maxlength: "超出输入限制,字数不能超出110个字符"
		            },
		            authType:{
		              required : '请选择认证方式'
		            },
		        	authCode : {
		                required : '请正确输入验证码',
		                maxlength : '请正确输入验证码',
						minlength : '请正确输入验证码',
						digits : '请正确输入验证码'
		            },
		            captcha : {
		                required : '请正确输入图形验证码',
		                minlength: '请正确输入图形验证码',
		            } 
		        },
		        submitHandler: function(validator, form, submitButton) {
		                $('.submit').attr("disabled","disabled");
					    var data = $('#auth_form').serialize();
				        $.ajax({
								url:contextPath+"/p/predeposit/balanceWithdrawals", 
								data:data,
								type:'POST', 
								async : false, //默认为true 异步   
								dataType:'json',
								error: function(jqXHR, textStatus, errorThrown) {
								    $('.submit').removeAttr("disabled");
								      changeRandImg(contextPath);
								      layer.alert("申请提现失败",{icon: 2});
								      return false;
								},
								success:function(result){
									   if("CAPTCHA_FAIL"==result){
									        changeRandImg(contextPath);
									        layer.alert("请输入正确的验证码",{icon: 0});
									   }else if("NOT_USER"==result){
									        changeRandImg(contextPath);
									        layer.alert("申请提现失败,没有找到该帐号",{icon: 2});
									   }else if("INSUFFICIENT_AMOUNT"==result){
									        changeRandImg(contextPath);
									        layer.alert("申请提现失败,账户余额不足",{icon: 2});
									   }else if("OK"==result){
										     layer.alert("申请提现成功",{icon: 1});
									         window.location.href=contextPath+"/p/predeposit/balance_withdrawal";
									   }else{
										   layer.alert(result,{icon: 2});
									    }
								       $('.submit').removeAttr("disabled");
								    return ;
								}
						  });
			    }
		  });
	
		      
    function changeRandImg(contextPath){
        var obj = document.getElementById("codeimage") ;
        $(obj).attr("src",contextPath + "/validCoderRandom\?d=" + new Date().getTime())
        //obj.src = contextPath + "/validCoderRandom\?d=" + new Date().getTime();
     }
    
    
    /**
     * 查询提现申请记录
     */
    function searcBalanceWithdrawal(){
        var curPageNO=$("#curPageNO").val();
        var pdcSn=$.trim($("#pdcSn").val());
        var status=$("#paystate_search").find("option:selected").val();
         window.location.href=contextPath+"/p/predeposit/balance_withdrawal?curPageNO="+curPageNO+"&pdcSn="+pdcSn+"&status="+status;
      }
      
    
    

    /**
     * 删除该提现申请
     * @param _id
     */
    function cancelWithdrawals(_id){
    	
    	layer.confirm("是否确认删除该提现申请?", {
   		 icon: 3
   	     ,btn: ['确定','关闭'] //按钮
   	   }, function(){
   		 $.ajax({
		        type:"POST",
		        //提交的网址
		        url:contextPath+"/p/predeposit/withdrawalApplyCancel/"+_id,
		        //提交的数据
		        async : false,  
		        //返回数据的格式
		        datatype: "json",
		        //成功返回之后调用的函数            
		        success:function(date){
		                        var result=eval(date);
				                if ("NOT_USER" == result) {
									layer.alert("取消提现申请失败,没有找到该帐号",{icon: 2});
								} else if ("NON_COMPLIANCE" == result) {
									layer.alert("取消提现申请失败,该申请正在操作过程中,请稍候重试！",{icon: 2});
								} else if ("OK" == result) {
									layer.alert("取消申请提现成功",{icon: 1});
									window.location.reload(true);
								} else {
									layer.alert(result,{icon: 2});
								}
				},
				//调用出错执行的函数
				error : function() {
					layer.alert("取消失败",{icon: 2});
				}
			});
   	   });
	}
