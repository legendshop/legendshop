var xAxisData = "";
var prodCountsData = "";
var prodCashData = "";

$(function(){
	prodAnalysis.doExecute();
	userCenter.changeSubTab("prodAnalysis");
	
	//路径配置
	require.config({
		paths: {
	        echarts: contextPath+'/resources/plugins/ECharts/dist'
	    }
	});
});

function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("from1").submit();
}

//查看商品走势图
function viewTrend(prodId,obj){
	var name=$(obj).attr("data");
	
	$("#main").show();
	$.ajax({
		url: contextPath + "/s/shopReport/viewTrend", 
		data: {"prodId":prodId},
		type:'post', 
		async : false, //默认为true 异步   
		success:function(retData){
		 	var obj = jQuery.parseJSON(retData); 
		 	xAxisData = obj.xAxisJson;
		 	prodCountsData = obj.prodCountsJson;
		 	prodCashData = obj.prodCashJson;
		}
		});
	
	//使用
	require(
			[ 
		        'echarts', 
		        'echarts/chart/bar',
		        'echarts/chart/line'
		    ],
		    function (ec) {
				// 基于准备好的dom，初始化echarts图表
		        var myChart = ec.init(document.getElementById('main')); 
				
		        option = {
		        	    tooltip : {
		        	        trigger: 'axis'
		        	    },
		        	    toolbox: {
		        	        show : true,
		        	        feature : {
		        	            mark : {show: true},
		        	            dataView : {show: true, readOnly: false},
		        	            magicType: {show: true, type: ['line', 'bar']},
		        	            restore : {show: true},
		        	            saveAsImage : {show: true}
		        	        }
		        	    },
		        	    calculable : true,
		        	    legend: {
		        	        data:['商品销量','销售金额']
		        	    },
		        	    xAxis : [
		        	        {
		        	            type : 'category',
		        	            data : getXAxisData()
		        	        }
		        	    ],
		        	    yAxis : [
		        	        {
		        	            type : 'value',
		        	            name : '商品销量'	  
		        	        },
		        	        {
		        	            type : 'value',
		        	            name : '销售金额'
		        	        }
		        	    ],
		        	    series : [
		        	        {
		        	            name:'商品销量',
		        	            type:'bar',
		        	            itemStyle:{
		                              normal:{color:'#2ec7c9'}
		                         },
		        	            data : getBarData()
		        	        },
		        	        {
		        	            name:'销售金额',
		        	            type:'line',
		        	            yAxisIndex: 1,
		        	            itemStyle:{
		                              normal:{color:'#b6a2de'}
		                        },
		        	            data : getLineData()
		        	        }
		        	    ]
		        	};
		        	                    
		        
		     	// 为echarts对象加载数据 
		        myChart.setOption(option); 
		        
		        $("#main").hide();
		        layer.open({
					  title :name,
					  type: 1, 
					  content: $('#main'),
					  area: ['967px', '470px']
					}); 
			}
	);
	
	//$('html,body').animate({scrollTop:$('#main').offset().top}, 1000);
	
}

//获取 x轴 数据
function getXAxisData(){
	var list = [];
	xAxisData = jQuery.parseJSON(xAxisData);
    for(var y=0;y<xAxisData.length;y++){
    	list.push(xAxisData[y]);
    }
    return list;
}

//获取 商品销量 数据
function getBarData(){
	prodCountsData = jQuery.parseJSON(prodCountsData);
	var list = [];
    for(var x=0;x<prodCountsData.length;x++){
    	list.push(prodCountsData[x]);
    }
    return list;
}

//获取 销售金额 数据
function getLineData(){
	prodCashData = jQuery.parseJSON(prodCashData);
	var list = [];
    for(var x=0;x<prodCashData.length;x++){
    	list.push(prodCashData[x]);
    }
    return list;
}