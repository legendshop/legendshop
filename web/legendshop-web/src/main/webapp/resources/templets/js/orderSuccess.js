$(document).ready(function() {
		$li.mouseover(function() {
						var $this = $(this);
						var $t = $this.index();
						$li.removeClass('on');
						$this.addClass('on');
						$ul.css('display', 'none');
						$ul.eq($t).css('display', 'block');
		});
	   
		$('.onl-pay-con div').bind('click', function () {
		    var payment_code=$(this).attr("payment_code");
		    $("#payTypeId").val(payment_code);
		    var bank_code=$(this).attr("bank-code");
		    if(isEmpty(bank_code)){
		       bank_code="";
		    }
		    $("#bankCode").val(bank_code);
		    $('.onl-pay').find("div").removeClass('pay-selected');
	        $(this).addClass('pay-selected');
	    });
	       
		//去付款
	    $('#next_button').on('click',function() {
	         var payment_code=$('#payTypeId').val();
	     	 var payTypeVal=$('input[name="pd_pay"]:checked').val();  //预付款或金币
	     	 
        	if($("#pre-deposit").length>0){
        		if (!isEmpty(payTypeVal) && $('#password_callback').val() != '1') {
					layer.msg('使用余额支付，需输入支付密码并确认  ',{icon:0});
					return;
				}
        		if(checkNeedPayTypeId()){
        			//var apiPayAmount = Number($("#api_pay_amount").html());
    				if(!isEmpty(payTypeVal)){
    					if (isEmpty(payment_code)) {
    						layer.msg('请选择一种在线支付方式',{icon:0});
    						return;
    					}
    				}
        		}else{
        			payment_code = "FULL_PAY";
        			$('#payTypeId').val("FULL_PAY");
        		}
        	}else{
        		if (isEmpty(payment_code)) {
					layer.msg('请选择一种在线支付方式',{icon:0});
					return;
				}
        	}
        	
        	if(isEmpty(payment_code)&&isEmpty(payTypeVal)){
        		if (isEmpty(payment_code)) {
					layer.msg('请选择一种在线支付方式',{icon:0});
					return;
				}
        	}
        	
        	//设置余额支付方式(1:预付款  2：金币)
        	$("#prePayTypeVal").val(payTypeVal);
        	//支付密码
        	$("#submitPwd").val($("#pay-password").val());
			$("#buy_form").submit();
			layer.open({
				  title :"支付提醒",
				  type: 1, 
				  content: $("#pay_overlay").html(),
				  area: ['400px', '220px']
				  ,btn: ['我知道了'],
				  yes:function(index, layero){
					  layer.close(index); 
				  }
				}); 
	     });
	             
	             $('#payTypeId').val('');
	             $('#password_callback').val('');
                 $('input[name="pd_pay"]').attr('checked',false);
	            
                 
	            $('input[name="pd_pay"]').on('change', function() {
						showPaySubmit(this);
				});
				
				//余额确认支付
				$('#pd_pay_submit').on('click', function() {
						if (isEmpty($.trim($('#pay-password').val()))) {
							layer.msg("请输入支付密码",{icon:0});
							return false;
						}
						
					   var payTypeVal=$('input[name="pd_pay"]:checked').val();
					   console.log(payTypeVal);
						
						$('#password_callback').val('');
						$.ajax({
					          url:contextPath+"/p/payment/userPredeposit/"+payTypeVal,
					          data:({password:$('#pay-password').val()}),
					          type: "POST",
					          dataType:"json",
					          cache:false,
					          success:function(data){
					        	  var result=eval(data);
					        	  if(result.status){
					        		 $('#password_callback').val('1');
			                	     $('#pd_password').hide();
					        	  }else{
					        	      $('#pay-password').val('');
					        		 layer.alert(result.msg,{icon:2});
					        	  }
					          },
					          error:function(XMLHttpRequest, textStatus) {
					              $('#pay-password').val('');
					              if (XMLHttpRequest.status == 500) {
					                  var result = eval("(" + XMLHttpRequest.responseText + ")");
					                 layer.alert("付页面停留太久，请放回订单详情重新支付！",{icon:2});
					              }
					          }
					      });
				 });
	             
	 });

	function checkNeedPayTypeId(){
		var totalP = Number( $("#totalPrice").html());
		if(totalP==0){
			return false;
		}else{
			return true;
		}
	}
	 
	 function showPaySubmit(obj) {
		
		 // alert($(obj).val());
			if ($(obj).attr('checked')) {
				$('input[name="pd_pay"]').attr('checked',false); //两个都清空
				$(obj).attr('checked',true); //当前选中
				$('#pay-password').val('');
				$('#password_callback').val('');
				$('#pd_password').show();
			} else {
			    $('#password_callback').val('');
				$('#pd_password').hide();
			}
			
			var checkValue=$(obj).val();
			var _diff_amount=pay_diff_amount;
			var _prede_price;

			
			//$('input[name="pd_pay"]'
			if ($(obj).attr('checked')) {
				if(checkValue==1){  //预付款
					_diff_amount -= member_pd;
				}else if(checkValue==2){ //金币
					_diff_amount -= coin;
				}
			}
			
			
			if (_diff_amount < 0) { 
				_diff_amount = 0;
				_prede_price = pay_diff_amount;
			}else if($(obj).attr('checked') && _diff_amount>=0){
				_prede_price = member_pd;
			}else if(!$(obj).attr('checked') && _diff_amount>=0){
				_prede_price = '0.00'
			}
			
			 var pos_decimal = _diff_amount.toString().indexOf('.');
		     if (pos_decimal < 0)
		     {
		          	_diff_amount += '.00';
		     }
		     
    	     
    	 	if(checkValue==1){  //预付款
    	 		$('#api_pay_amount1').html(Number(_diff_amount).toFixed(2)); 
    	 		$('#api_pay_amount2').html(Number(coin).toFixed(2)); 
			}else if(checkValue==2){ //金币
				$('#api_pay_amount1').html(Number(pay_diff_amount).toFixed(2)); 
    	 		$('#api_pay_amount2').html(Number(_diff_amount).toFixed(2)); 
			}
    	     
			$('#totalPrice').html(Number(_diff_amount).toFixed(2));
			$('#predePrice').html(Number(_prede_price).toFixed(2));
			 
		}
		
/**
 * 判断是否是空
 * @param value
 */
function isEmpty(value){
	if(value == null || value == "" || value == "undefined" || value == undefined || value == "null"){
		return true;
	}
	else{
		value = (value+"").replace(/\s/g,'');
		if(value == ""){
			return true;
		}
		return false;
	}
}
		