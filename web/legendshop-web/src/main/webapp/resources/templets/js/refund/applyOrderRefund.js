var isSubmitted = false;//表单是否已提交

//页面加载的时候
$(function(){
	
	//仅退款表单数据校验
	$("#refundForm").validate({
	 		ignore: ".ignore",
/*		    errorPlacement: function(error, element) {
		    	element.parent().find("em").html("");
				error.appendTo(element.parent().find("em"));
			},*/
	        rules: {
	        	orderId: {
		            required: true
		        },
		        buyerMessage: {
		            required: true
		        },
		        reasonInfo: {
		            required: true,
		            minlength: 5,
		            maxlength: 300
		        },
		        /*photoFile1: {
		            required: true
		        }*/
	     },
	     messages: {
	        	orderId: {
		            required: "对不起,没有订单Id!"
		        },
		        buyerMessage: {
		            required: "请选择退款原因!"
		        },
		        reasonInfo: {
		            required: "请输入退款说明!",
		            minlength: "退款说明不能少于5个字符",
		            maxlength: "退款说明不能超过300个字符!"
		        },
		        /*photoFile1: {
		            required: "至少添加一个退款凭证图片!"
		        }*/
	     },
	     submitHandler : function(form){
	     	if(!isSubmitted){
	     		form.submit();
	     		isSubmitted = true;
	     	}else{
	     		layer.alert("对不起,请不要重复提交表单!",{icon: 0});
	     	}
	     } 
	 });
	
});

//检查退款金额,不能大于订单金额
jQuery.validator.addMethod("checkRefundMoney", function(value, element) {       
	return Number(value) <= maxRefundMoney;
});  

jQuery.validator.addMethod("isNumber", function(value, element) {       
    return this.optional(element) || /^[-\+]?\d+$/.test(value) || /^[-\+]?\d+(\.\d+)?$/.test(value);       
}, "必须整数或小数");  

jQuery.validator.addMethod("isValidMoney", function(value, element) { 
    value = parseFloat(value);      
    return this.optional(element) || value> 0;       
}, "金额必须要大于0元"); 

function qqLink(qqs) {
	if (qqs != undefined && qqs != "") {
    	if (qqs.indexOf(",") > 0) {
        	var qqList = qqs.split(",");
        	window.location = "tencent://message/?uin=" + qqList[0] + "&Site=在线客服&Menu=yes";
   		 } else {
        	window.location = "tencent://message/?uin=" + qqs + "&Site=在线客服&Menu=yes";
   		 }
		 }
}

