$("#personal").click(function(){
	$(this).addClass("selected");
	$("#enterprise").removeClass("selected");
});

$("#enterprise").click(function(){
	$(this).addClass("selected");
	$("#personal").removeClass("selected");
});

//点击"下一步,完善入驻信息"按钮时,判断获取用户选择的入驻类型
$("#settledInfo").click(function(){
	var type= $(".cho-sty").find(".selected").attr("type");
	if(isBlank(type)){
		layer.msg("请选择店铺类型", {icon: 0});
		return false;
	}
	/*window.location=contextPath+"/p/settledAgreement?shopType="+type;*/
	window.location=contextPath+"/p/to_shopdetail/"+ type;
});

/*对联系人信息进行验证*/
function isMobile(str){
	var reg = /^1\d{10}$/;
	return reg.test(str);
	}

function isEMail(str){
	var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/;
	return reg.test(str);
	}

$.validator.addMethod("isMobile", function(value, element) {
    return isMobile(value);}, '请输入正确的手机号码');

$.validator.addMethod("isEMail", function(value, element) {
    return isEMail(value);}, '请输入正确的手机号码');


	/*$(document).ready(function(e) {
	    $("#contactform").validate({
		 ignore: "",
		errorPlacement: function(error, element) {
					element.parent().find("label").html("");
						error.appendTo(element.parent().find("label"));
		},
	     rules: {
	    	 contactName:{required:true},
	    	 contactMobile:{required:true,isMobile:true},
	    	 contactMail:{required:true,isEMail:true}
				},
	     messages: {
	    	 contactName:{required:"请填写联系人姓名"},
	    	 contactMobile:{required:"请填写联系人电话",isMobile:"电话号码格式不正确"},
	    	 contactMail:{required:"请填写联系人邮箱",isEMail:"邮箱格式不正确"}
			  }

	  });
	});*/
	function saveContact(){
		$("#contactform").submit();
	}
/*对联系人信息进行验证完毕*/


	function shopPrevious(step){
		window.location=contextPath+"/p/shopPrevious?op=back&step="+step;
	}

/*对公司信息进行验证*/
	function isTel(str){
		var reg = /^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/;
		return reg.test(str);
		}

	jQuery.validator.addMethod("isTel", function(value, element) {
		if(value!=null&&value!=''){
	     	return isTel(value);
	     	}else{
	     		return true
	     	}
	     }, '固定电话格式为:010-XXXXXXXX');

	$(document).ready(function(e) {
		 window.history.forward(1);
	    $("#companyInfo").validate({
		 ignore: "",
		errorPlacement: function(error, element) {
					element.parent().find("em").html("");
						error.appendTo(element.parent().find("em"));
		},
	     rules: {
	    	 companyName:{required:true},
	    	 licenseNumber:{required:true,number:true},
	    	 legalIdCardPic: {checkFile: true},
	    	 licenseAreaid:{required: true},
	    	 licenseAddr:{required: true},
	    	 licenseEstablishDate:{required: true},
	    	 licenseStartDate:{required: true},
	    	 licenseEndDate:{required: true},
	    	 licenseRegCapital:{required: true,digits:true},
	    	 licenseBusinessScope:{required: true},
	    	 licensePics:{checkFile: true},
	    	 companyAreaid:{required: true},
	    	 companyAddress:{required: true},
	    	 companyTelephone:{required: true,isTel:true},
	    	 companyNum:{required: true},
	    	 companyIndustry:{required: true},
	    	 companyProperty:{required: true}
				},
	     messages: {
	    	 companyName:{required:"公司名不能为空"},
	    	 licenseNumber: {required:"请填写营业执照号码",number:"格式不正确"},
	    	 licenseAreaid:{required:"地区不能为空"},
	    	 licenseAddr:{required:"营业执照详情地址不能为空"},
	    	 licenseEstablishDate:{required:"成立日期不能为空"},
	    	 licenseStartDate:{required:"开始时间不能为空"},
	    	 licenseEndDate:{required:"结束时间不能为空"},
	    	 licenseRegCapital:{required:"注册资本不能为空",digits:"格式不正确"},
	    	 licenseBusinessScope:{required:"经营范围不能为空"},
	    	 companyAreaid:{required:"地区不能为空"},
	    	 companyAddress:{required:"公司地址不能为空"},
	    	 companyTelephone:{required:"公司电话不能为空"},
	    	 companyNum:{required:"公司人数不能为空"},
	    	 companyIndustry:{required:"公司行业不能为空"},
	    	 companyProperty:{required:"公司性质不能为空"}
			  }

	  });
	});
	function saveCompanyInfo(){
		$("#companyInfo").submit();
	}

/*对公司信息进行验证完成*/

/*对证书进行验证*/
	$("input[type=radio]").click(function(){
		var a=$(this).val();
		if(a==2){
			$("#new").show(500);
			$("#old").hide(500);
			$("#taxpayerId").addClass("ignore");
			$("#taxRegistrationNum").addClass("ignore");
			$("#taxRegistrationNumEPic").addClass("ignore");
			$("#socialCreditCode").removeClass("ignore");
			$("#codeType").val(a);
			$("#taxpayerId").val("");
			$("#taxRegistrationNum").val("");
			$("#taxRegistrationNumEPic").attr("oldFile","");

		}else{
			$("#old").show(500);
			$("#new").hide(500);
			$("#socialCreditCode").addClass("ignore");
			$("#taxpayerId").removeClass("ignore");
			$("#taxRegistrationNum").removeClass("ignore");
			$("#taxRegistrationNumEPic").removeClass("ignore");
			$("#codeType").val(a);
			$("#socialCreditCode").val("");
		}
	});

	$(document).ready(function(e) {
		 window.history.forward(1);
		  $("#cretificateInfo").validate({
			  	ignore: ".ignore",
				errorPlacement: function(error, element) {
							element.parent().find("em").html("");
								error.appendTo(element.parent().find("em"));
				},
			     rules: {
			    	 code:{required:true},
			    	 taxpayerId:{required:true},
			    	 taxRegistrationNum:{required:true},
			    	 taxRegistrationNumEPic: {checkFile: true},
			    	 socialCreditCode:{required: true},
			    	 bankAccountName:{required: true},
			    	 bankCard:{required: true},
			    	 bankName:{required: true},
			    	 bankLineNum:{required: true},
			    	 bankAreaid:{required: true},
			    	 bankPermitPic: {checkFile: true}
						},
			     messages: {
			    	 code:{required:"不能为空"},
			    	 taxpayerId:{required:"纳税人识别号不能为空"},
			    	 taxRegistrationNum: {required:"请填写身份证号码"},
			    	 socialCreditCode:{required:"统一社会信用代码号不能为空"},
			    	 bankAccountName:{required: "银行开户名不能为空"},
			    	 bankCard:{required: "公司银行账号不能为空"},
			    	 bankName:{required: "开户银行支行不能为空"},
			    	 bankLineNum:{required: "开户行支行联行号不能为空"},
			    	 bankAreaid:{required: "开户银行支行所在地不能为空"}
					  }

			  });
	});

	function saveCretificateInfo(){
		$("#cretificateInfo").submit();
	}
/*对证书进行验证完成*/

/*对店铺信息进行验证*/
	$.validator.addMethod("checkSiteName",function(value,element){
		var regex = new RegExp("^([a-zA-Z0-9_]|[\\u4E00-\\u9FFF])+$", "g");
		if(regex.test(value)){
			return true;
		}else{
			return false;
		}
	},'店铺名字只能由汉字、数字、字母、下划线组成');
	$.validator.addMethod("checkVal",function(value,element){
		var config=true;
			if(value==oldSiteName){
				return config;
			}else{
				$.ajax({
					type: "POST",
					url: contextPath+"/isSiteNameExist",
					async:false,
					data:{"siteName":value},
					dataType: "json",
					success:function(data){
						if(!data)
							config=false;
					}
				});
				return config;
			}
	},'店铺名已存在');

	jQuery.validator.addMethod("isIdCard", function(value, element) {
	     return isIdCard(value);}, '请输入正确的身份证号码');

	function isIdCard(str){
		return checkIDCard(str);
	}
    jQuery.validator.addMethod("isLicenseNumber", function(value, element) {
        return checkLicense(value);}, '请输入正确的营业执照');

    function isIdCard(str){
        return checkIDCard(str);
    }
//     function checkLicense(code){
//     var tip = "OK";
//     var pass= true;
//
//     if(code.length != 18){
//         tip = "社会信用代码长度错误！";
//         pass = false;
//     }
//     var reg = /^([159Y]{1})([1239]{1})([0-9]{6})([0-9ABCDEFGHJKLMNPQRTUWXY]{9})([0-9ABCDEFGHJKLMNPQRTUWXY]{1})$/;
//     if(!reg.test(code)){
//         tip = "社会信用代码校验错误！";
//         pass = false;
//     }
//     //不用I、O、S、V、Z
//     var str = '0123456789ABCDEFGHJKLMNPQRTUWXY';
//     var ws =[1,3,9,27,19,26,16,17,20,29,25,13,8,24,10,30,28];
//
//     var codes  = new Array();
//     var sum = 0;
//     codes[0] = code.substr(0,code.length-1);
//     codes[1] = code.substr(code.length-1,code.length);
//
//     for(var i=0;i<codes[0].length;i++){
//         var Ancode = codes[0].charAt(i);
//         var Ancodevalue = str.indexOf(Ancode);
//         sum += Ancodevalue * ws[i];
//     }
//     var indexOfc18 = 31 - (sum % 31);
//     var c18 = str.charAt(indexOfc18);
//     if(c18 != codes[1]){
//         tip = "社会信用代码有误！";
//         pass = false;
//     }
//
//     return pass;
// }



function checkLicense(code){
  var tip = "OK";
  var pass= true;

  var patrn = /^[0-9A-Z]+$/;
  //18位校验及大写校验
  if ((code.length != 18) || (patrn.test(code) == false))
  {
    tip = "社会信用代码长度错误！";
        pass = false;
    // alert("不是有效的统一社会信用编码！");
  }
  else
  {
    var Ancode;//统一社会信用代码的每一个值
    var Ancodevalue;//统一社会信用代码每一个值的权重
    var total = 0;
    var weightedfactors = [1, 3, 9, 27, 19, 26, 16, 17, 20, 29, 25, 13, 8, 24, 10, 30, 28];//加权因子
    var str = '0123456789ABCDEFGHJKLMNPQRTUWXY';
    //不用I、O、S、V、Z
    for (var i = 0; i < code.length - 1; i++)
    {
      Ancode = code.substring(i, i + 1);
      Ancodevalue = str.indexOf(Ancode);
      total = total + Ancodevalue * weightedfactors[i];
      //权重与加权因子相乘之和
    }
    // alert(total);
    var logiccheckcode = 31 - total % 31;
    if (logiccheckcode == 31)
    {
      logiccheckcode = 0;
    }
    var Str = "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,J,K,L,M,N,P,Q,R,T,U,W,X,Y";
    var Array_Str = Str.split(',');
    logiccheckcode = Array_Str[logiccheckcode];
    // alert(logiccheckcode);


    var checkcode = code.substring(17, 18);
    if (logiccheckcode != checkcode)
    {
      pass = false;
      tip = "不是有效的统一社会信用编码！";
      // alert("不是有效的统一社会信用编码！");
    }
  }



  return pass;
}


// 函数参数必须是字符串，因为二代身份证号码是十八位，而在javascript中，十八位的数值会超出计算范围，造成不精确的结果，导致最后两位和计算的值不一致，从而该函数出现错误。
// 详情查看javascript的数值范围
    function checkIDCard(idcode){
    // 加权因子
    var weight_factor = [7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2];
    // 校验码
    var check_code = ['1', '0', 'X' , '9', '8', '7', '6', '5', '4', '3', '2'];

    var code = idcode + "";
    var last = idcode[17];//最后一个

    var seventeen = code.substring(0,17);

    // ISO 7064:1983.MOD 11-2
    // 判断最后一位校验码是否正确
    var arr = seventeen.split("");
    var len = arr.length;
    var num = 0;
    for(var i = 0; i < len; i++){
        num = num + arr[i] * weight_factor[i];
    }

    // 获取余数
    var resisue = num%11;
    var last_no = check_code[resisue];

    // 格式的正则
    // 正则思路
    /*
    第一位不可能是0
    第二位到第六位可以是0-9
    第七位到第十位是年份，所以七八位为19或者20
    十一位和十二位是月份，这两位是01-12之间的数值
    十三位和十四位是日期，是从01-31之间的数值
    十五，十六，十七都是数字0-9
    十八位可能是数字0-9，也可能是X
    */
    var idcard_patter = /^[1-9][0-9]{5}([1][9][0-9]{2}|[2][0][0|1][0-9])([0][1-9]|[1][0|1|2])([0][1-9]|[1|2][0-9]|[3][0|1])[0-9]{3}([0-9]|[X])$/;

    // 判断格式是否正确
    var format = idcard_patter.test(idcode);

    // 返回验证结果，校验码和格式同时正确才算是合法的身份证号码
    return last === last_no && format ? true : false;
}
	  function checkFile(val,element){
		   if(val=="" && $(element).attr("oldFile")==""){
			   return false;
		   }
		   return true;
	   }

	   jQuery.validator.addMethod("checkFile", function(value, element) {
			 return checkFile(value,element);}, '文件不能为空');

	$(document).ready(function(e) {
		 window.history.forward(1);
	    $("#shopDetaiInfo").validate({
		 ignore: "",
		errorPlacement: function(error, element) {
					element.parent().find("em").html("");
						error.appendTo(element.parent().find("em"));
		},
	     rules: {
	    	 responsible:{required:true},
	    	 companyName:{required: true,minlength:5},
	    	 licenseNumber:{required:true,isLicenseNumber:true},
	    	 idCardPicFile: {checkFile: true},
	    	 idCardBackPicFile: {checkFile: true},
	    	 idCardNum:{required:true,minlength:15,isIdCard:true},
	    	 licensePicFile: {checkFile: true},
	    	 siteName:{required:true,minlength: 2,checkVal:true,checkSiteName:true},
	    	 shopType:{required: true,min:0},
	    	 contactName:{required:true},
	    	 contactMobile:{required:true,isMobile:true},
	    	 contactMail:{required:true,isEMail:true}
				},
	     messages: {
	    	 responsible:{required:"负责人不能为空"},
	    	 companyName:{required:"公司名不能为空",minlength: "公司名最少5位"},
	    	 licenseNumber: {required:"请填写营业执照号码",isLicenseNumber:"格式不正确"},
	    	 idCardPicFile:{checkFile:"负责人身份证正面不能为空"},
	    	 idCardBackPicFile:{checkFile:"负责人身份证反面不能为空"},
	    	 idCardNum:{required:"请填写身份证号码",minlength: '身份证最少15位',IdCardValidate:'身份证格式错误'},
	    	 licensePicFile:{checkFile:"营业执照副本电子版不能为空"},
	    	 siteName:{required:"店铺名不能为空",minlength:"店铺名最少2位"},
	    	 shopType:{required:"请选择店铺类型",min:"请选择店铺类型"},
	    	 contactName:{required:"请填写联系人姓名"},
	    	 contactMobile:{required:"请填写联系人电话",isMobile:"电话号码格式不正确"},
	    	 contactMail:{required:"请填写联系人邮箱",isEMail:"邮箱格式不正确"}
			  }

	  });
	});
	function saveShopInfo(){
		$("#shopDetaiInfo").submit();
	}


/*对店铺信息进行验证完毕*/

/*同意协议*/
function agreeOnline(){
	var val=$(".agreeOnline").attr("checked");
	   if(val!="checked"){
		   layer.msg("请仔细阅读并同意协议",{icon: 0});
		   return false;
	   }else{
		   layer.msg("恭喜您，提交入驻申请成功！请等待审核",{icon: 1});
		   window.location=contextPath+"/p/checkSpeed";
	   }
}


function rewrite(op){
	window.location=contextPath+"/p/reWrite?op="+op;

}

$("input[type=file]").change(function(){
		checkImgType(this);
		checkImgSize(this,5120);
});

function isBlank(_value){
	   if(_value==null || _value=="" || _value==undefined){
	     return true;
	   }
	   return false;
}
