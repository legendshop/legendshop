 //敏感字检查
 function checkSensitiveData(content){
		var url = contextPath + "/sensitiveWord/filter";
		$.ajax({
		"url":url, 
		data: {"src": content},
		type:'post', 
		dataType : 'json', 
		async : false, //默认为true 异步   
		success:function(retData){
	   		result = retData;
		}
		});
		return result;
	}
//评论回复
 function replay(prodComId){

	if($("#replyItem_"+prodComId).attr("class")=="item-reply reply-lz"){
		$("#replyItem_"+prodComId).attr("class","item-reply reply-lz hide");
	}else{
		if($("#replyItem_"+prodComId).parent().children().length ==1){
			$.ajax({
				url: contextPath + "/productcomment/recoveryData", 
				data:{"prodComId":prodComId},
				type:'post', 
				async : false, //默认为true 异步   
				success:function(retData){
					$("#replyItem_"+prodComId).attr("class","item-reply reply-lz");
					$("#replyItem_"+prodComId).after(retData);
				}
				});
		}else{
			$("#replyItem_"+prodComId).attr("class","item-reply reply-lz");
		}
		
	}
	 
 }
 
 function showReplayBtn(prodId){
	 $("#replayBtn"+prodId).attr("style","");
 }
 
 function hideReplayBtn(prodId){
	 $("#replayBtn"+prodId).attr("style","visibility: hidden;");
 }
 
//判断回复人的有用是否为1
 	function isUsefulExist(prodComId){
 		var result ;
 		$.ajax({
			url:contextPath + "/productcomment/isUsefulExist", 
			data:{"prodComId":prodComId},
			type:'post', 
			async : false, //默认为true 异步   
			dataType : 'json', 
			success:function(retData){
				result = retData;
			}
			});
 		return result;
 	}
 
 function agree(usefulCounts,prodComId){
		 usefulCounts =usefulCounts+1;
			$.ajax({
				url: contextPath + "/productcomment/updateUsefulCounts", 
				data:{"prodComId":prodComId},
				type:'post', 
				async : true, //默认为true 异步   
				dataType : 'json', 
				success:function(result){
					if('OK' == result){
						$("#useful_" +prodComId).html(usefulCounts);
					}else if('UR' == result){
						layer.msg("一个评价只能点一次呦",{icon:0});
					}else if('LR' == result){
						login();
					}else{
						layer.msg("更新数据失败！",{icon:2});
					}
				}
				});
 }
 
 function replyMessage(prodComName,prodComId,obj){
	 var replyText = $(obj).prev().children("input").val();
	 var counts = $("#replyItem_"+prodComId).next().children("strong").html();
	 if(counts !='null' && counts !=null){
		 counts= parseInt(counts)+1;
	 }else{
		 counts = 1;
	 }
	 if(replyText.length ==0){
		 layer.msg("回复的信息不可为空!",{icon:0});
	 }else if(replyText.length >200){
		 layer.msg("回复内容不能超过200个字!",{icon:0});
	 }else{
		 var chkResult = checkSensitiveData(replyText);	 
		   	if(chkResult != null && chkResult != '' ){ 	
			   	 layer.msg("您的输入含有敏感字： '"+chkResult+"' 请更正后再提交",{icon:0});
			   	 return;
		   	 }
		 $.ajax({
				url:contextPath + "/productcomment/replys", 
				data:{"prodComName":prodComName,"prodComId":prodComId,"replyText":replyText},
				type:'post', 
				async : false, //默认为true 异步   
				error: function(jqXHR, textStatus, errorThrown) {
					var errTxt=jqXHR.getResponseHeader("sessionstatus");
					if(errTxt!=null && "nologin"==errTxt){
						login();
					}
				},
				success:function(result){
					$("#replyItem_"+prodComId).after(result);
					$("#replyItem_"+prodComId).next().children("strong").html(counts);
					$(obj).prev().children("input").val("");
					$("#replyItem_"+prodComId).attr("class","item-reply reply-lz hide");
				}
				});
	 }
 }
 
 function replayBtn(obj){
	 var replayform = $(obj).parent().next().toggleClass("hide");
 }
 
 function replyParent(parentReplyName,parentReplyId,parentId,prodComId,obj){
	 var replyText = $(obj).prev().children("input").val();
	 var counts = parseInt($("#replyItem_"+prodComId).next().children("strong").html()) +1;
	 if(replyText.length ==0){
		 layer.msg("回复的信息不可为空!",{icon:0});
	 }else{
		 var chkResult = checkSensitiveData(replyText);	 
		   	if(chkResult != null && chkResult != '' ){
		   	  layer.msg("您的输入含有敏感字： '"+chkResult+"' 请更正后再提交",{icon:0});
		   	  return;
		   	 }
		 $.ajax({
				url: contextPath + "/productcomment/replyParents", 
				data:{"parentReplyName":parentReplyName,"prodComId":prodComId,"replyText":replyText,"parentReplyId":parentReplyId,"parentId":parentId},
				type:'post', 
				async : false, //默认为true 异步   
				error: function(jqXHR, textStatus, errorThrown) {
					var errTxt=jqXHR.getResponseHeader("sessionstatus");
					if(errTxt!=null && "nologin"==errTxt){
						login();
					}
				},
				success:function(result){
					$("#replyItem_"+prodComId).after(result);
					$("#replyItem_"+prodComId).next().children("strong").html(counts);
					$(obj).prev().children("input").val("");
					$(obj).parent().parent().parent().attr("class","replay-form hide");
					$("#replyItem_"+prodComId).attr("class","item-reply reply-lz hide");
				}
				});
	 }
 }
 
 function login(){
	 layer.open({
		title :"登录",
		type: 2,
		id:"login",
		content: contextPath + '/loadLoginOverlay', 
	    area: ['440px', '530px']
	}); 
 }
 