function search() {
    $("#order_search #curPageNO").val("1");//只要是搜索,都是从第一页开始查
    $("#order_search")[0].submit();
}

//发票信息 修改
function showForm_invoice(){
	//发票列表
	invoicePager(1);
	
	$(".invoice .invoice-open").hide();
	$(".invoice .invoice-close").show();
	$(".invoice .invoice-no-spr").hide();
	$(".invoice .invoice-spr").show();
	$(".invoice-spr .invoice-spr-cho").hide();
}

//发票信息 关闭
function close_invoice(){
	$(".invoice .invoice-open").show();
	$(".invoice .invoice-no-spr").show();
	$(".invoice .invoice-close").hide();
	$(".invoice .invoice-spr").hide();
}

function onPttt2(obj){
	if(obj.checked){
		$(obj).parent().parent().addClass("radio-wrapper-checked");
		$("#invoince_pttt1").parent().parent().removeClass("radio-wrapper-checked");
	}else{
		$(obj).parent().parent().removeClass("radio-wrapper-checked");
		$("#invoince_pttt1").parent().parent().addClass("radio-wrapper-checked");
	}
	$("#invoice_humnumber").show();
}

function onPttt1(obj){
	if(obj.checked){
		$(obj).parent().parent().addClass("radio-wrapper-checked");
		$("#invoince_pttt2").parent().parent().removeClass("radio-wrapper-checked");
	}else{
		$(obj).parent().parent().removeClass("radio-wrapper-checked");
		$("#invoince_pttt2").parent().parent().addClass("radio-wrapper-checked");
	}
	$("#invoice_humnumber").hide();
}

//发票信息 列表分页
function invoicePager(curPageNO){
  $.ajax({
		url:contextPath+"/p/invoicePage",
		data:{"curPageNO":curPageNO},
		type:'post', 
		async : true,  
		success:function(result){
			$("#invoiceList").html(result);
		}
	});
}


function pager(curPageNO){
	userCenter.loadPageByAjax( "/p/invoice/myInvoice?curPageNO=" + curPageNO);
}


//发票信息 删除
function DelInvoice(invoiceId){
	layer.confirm("确定删除该发票信息？", {
		 icon: 3
	     ,btn: ['确定','关闭'] //按钮
	   }, function(index){
		   $.ajax({
	 			url:contextPath+"/p/delInvoice/"+invoiceId,
	 			type:'post', 
	 			async : true,
	 			dataType : 'json', 
	 			success:function(result){
	 				//刷新发票列表
	 				invoicePager(1);
	 			}
	 		});
		   layer.close(index); //再执行关闭 
	   });
}

//点击新的地址xinxi
function new_invoince(){
	$(".invoice-spr .invoice-spr-cho").show();
	$(".invoice-spr .invoice-but").show();
}

//发票信息 保存
function savePart_invoice(){
	//判读是否是新增发票
	var is_invoiceId= $("#invoiceList").find('input:radio[name="invoiceId"]:last').attr("checked");
	if(is_invoiceId && !$(".invoice-spr .invoice-spr-cho").hasClass("hide")){ //说明是新增
		var _number=$("#invoiceList ul li").length;
		if(_number==11){
			layer.alert("您最多可以添加10条发票信息，请先删除部分不常用发票后再添加", {icon:2});
			return;
		}
		saveInvoice();
	}else{
	  //更改选择发票信息
		var _invoice=$("#invoiceList").find('input:radio[name="invoiceId"]:checked');
		if(_invoice.length==0){
			layer.msg("请先选择发票");
		}else{
			$.ajax({
				url:contextPath+"/p/invoice/changeInvoiceCommon",
				data: {"id":_invoice.val()},
				type:'post', 
				dataType : 'json', 
				async : false, //默认为true 异步   
				success:function(result){
					if(result == "OK"){
						$(".invoice .invoice-no-spr").children("p").attr("invoice",$(_invoice).val());
						$(".invoice .invoice-no-spr").children("p").html($.trim($(_invoice).next().html()));
						$(".invoice .invoice-open").show();
						$(".invoice .invoice-close").hide();
						$(".invoice .invoice-no-spr").show();
						$(".invoice .invoice-spr").hide();
					}else{
						layer.alert("对不起，您操作的数据有误",{icon: 2});
					}
				}
			});
			
			/*$(".invoice .invoice-no-spr").children("p").attr("invoice",$(_invoice).val());
			$(".invoice .invoice-no-spr").children("p").html($.trim($(_invoice).next().html()));
			$(".invoice .invoice-open").show();
			$(".invoice .invoice-close").hide();
			$(".invoice .invoice-no-spr").show();
			$(".invoice .invoice-spr").hide();*/
		}
	}
	
}

function saveInvoice(){
	$("#titNameNote").hide();
	var invoiceType= $('input:radio[name="invoince_type"]:checked').val();
	var invoinceTitle = $('input:radio[name="invoince_pttt"]:checked').val();
	var invoinceContent = $('input:radio[name="invoince_content"]:checked').val();
	var titName=$.trim($("#invoice_Unit_TitName").val());
	var invoiceHumNumber=$.trim($("#invoice_hum_number").val());
	var invoice_title="个人";
	var company="";
	
	if(isEmpty(titName)){
		 $("#titNameNote").text("请输入发票抬头");
		 $("#titNameNote").show();
	     return ;
	}else if(!is_forbid(titName)){
		 $("#titNameNote").text("发票抬头中含有非法字符");
		 $("#titNameNote").show();
	     return ;
	}
	
	company="("+titName+")";
	
	if(invoinceTitle==2){
		invoice_title="单位";
		if(isEmpty(invoiceHumNumber)){
			 layer.alert("请输入发票纳税人识别号",{icon: 0});
		     return ;
		}
		var Reg = /^[A-Z0-9]+$/;
		if(!Reg.test(invoiceHumNumber) || invoiceHumNumber.length>150){
			layer.alert("请输入正确的纳税人识别号",{icon: 0});
			return;
		}
		
		company="("+titName+","+invoiceHumNumber+")";
	}
	
  var data = {
		"typeId":invoiceType,
		"titleId": invoinceTitle,
		"contentId": invoinceContent,
		"invoiceHumNumber":invoiceHumNumber,
		"company":titName
	 };
	var result=sendData(data);
	if(result!=""){
		$(".invoice .invoice-no-spr").children("p").attr("invoice",result);
		$(".invoice .invoice-no-spr").children("p").html("普通发票&nbsp;&nbsp;"+invoice_title+company+"&nbsp;&nbsp;明细");
		$(".invoice .invoice-open").show();
		$(".invoice .invoice-close").hide();
		$(".invoice .invoice-no-spr").show();
		$(".invoice .invoice-spr").hide();
	}
}


//发票信息 保存
function no_invoice(){
	
	$(".invoice .invoice-no-spr").children("p").attr("invoice","");
	$(".invoice .invoice-no-spr").children("p").html("不需要发票");
	$(".invoice .invoice-open").show();
	$(".invoice .invoice-close").hide();
	$(".invoice .invoice-no-spr").show();
	$(".invoice .invoice-spr").hide();
}

//传送数据
function sendData(data){
	var value="";
	 $.ajax({
			url:contextPath+"/p/invoice/createAndSave",
			data: data,
			type:'post', 
			dataType : 'json', 
			async : false, //默认为true 异步   
			success:function(result){
				var date = JSON.parse(result);
				if(date.result == 'OK'){
					layer.msg('保存成功', {icon:1, time:1000});
					value= date.invoiceId;
				}else {
					layer.alert(date.result, {icon:0});
				}
			}
	  });
	 return value;
}

function isEmpty(value){
	if(value == null || value == "" || value == "undefined" || value == undefined || value == "null"){
		return true;
	}
	else{
		value = (value+"").replace(/\s/g,'');
		if(value == ""){
			return true;
		}
		return false;
	}
}

/**
 * 检查是否含有非法字符
 * @param temp_str
 * @returns {Boolean}
 */
function is_forbid(temp_str){
    temp_str = temp_str.replace(/(^\s*)|(\s*$)/g, "");
	temp_str = temp_str.replace('--',"@");
	temp_str = temp_str.replace('/',"@");
	temp_str = temp_str.replace('+',"@");
	temp_str = temp_str.replace('\'',"@");
	temp_str = temp_str.replace('\\',"@");
	temp_str = temp_str.replace('$',"@");
	temp_str = temp_str.replace('^',"@");
	temp_str = temp_str.replace('.',"@");
	temp_str = temp_str.replace(';',"@");
	temp_str = temp_str.replace('<',"@");
	temp_str = temp_str.replace('>',"@");
	temp_str = temp_str.replace('"',"@");
	temp_str = temp_str.replace('=',"@");
	temp_str = temp_str.replace('{',"@");
	temp_str = temp_str.replace('}',"@");
	var forbid_str = new String('@,%,~,&');
	var forbid_array = new Array();
	forbid_array = forbid_str.split(',');
	for(i=0;i<forbid_array.length;i++){
		if(temp_str.search(new RegExp(forbid_array[i])) != -1)
		return false;
	}
	return true;
}

//补开发票
function getInvoice(subNumber) {
	//$(".invoice .invoice-no-spr").children("p").attr("invoice") == null
    if (isEmpty($(".invoice .invoice-no-spr").children("p").attr("invoice"))) {
    	layer.msg('请在页面顶部先填写发票信息',{icon:7,time:1000}, function(index){
        	showForm_invoice();
        	var speed=10;//滑动的速度
        	$('#body,html').animate({ scrollTop: 0 }, speed);
        });
    	return "";
    }
    data = {"subNumber": subNumber, "invoiceId": $(".invoice .invoice-no-spr").children("p").attr("invoice")};
    $.ajax({
        "url": contextPath + "/p/invoice/getInvoice",
        type: 'post',
        dataType: 'json',
        data: data,
        async: false,
        error: function (jqXHR, textStatus, errorThrown) {
        	layer.alert("系统错误异常！",{icon: 2});
        },
        success: function (result) {
            if (result.status == 1) {
            	layer.msg('发票申请成功', {icon: 1,time: 1000}, function(){
            		window.location.href = path + "/p/invoice/myInvoice";
            	});
            }
            if (result.status == 0) {
            	layer.alert("发票申请失败",{icon: 2})
            }
        }
    });
}
