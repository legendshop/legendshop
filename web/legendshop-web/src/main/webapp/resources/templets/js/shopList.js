$(function(){
	//处理排序下拉列表的隐藏与显示
	$("#order-div .order-item:first").click(function(event){
		if($("#orderList").is(":visible")){//判断元素是否可见
			$("#orderList").hide();
		}else{
			$("#orderList").show();
		}
		return false;//阻止事件冒泡
	});

	//交换本次选择与上次选择的orderType
	$("#orderList .order-item").click(function(event){
		var $this = $(this);
		var $firstItem = $("#order-div .order-item:first");

		var curText = $this.text();
		var curOrderWay = $this.attr("order-way");

		var firText = $firstItem.text();
		var firOrderWay = $firstItem.attr("order-way");

		$this.text(firText);
		$this.attr("order-way",firOrderWay);

		$firstItem.text(curText);
		$firstItem.attr("order-way",curOrderWay);

		$("#searchForm #orderWay").val(curOrderWay);

		//重新加载商品列表
		$("#curPageNO").val("1");
		loadShopList();
	});

	//当鼠标点击页面其他地方时,隐藏排序下拉列表
	$(document).click(function(event){
		$("#orderList").hide();
	});
});

function pager(curPageNO){
	$("#curPageNO").val(curPageNO);
	loadShopList();
}

/**
 * 搜索商品
 */
function searchShop(){
	$("#curPageNO").val("1");
	loadShopList();
}

function loadShopList(){
	var param = $("#searchForm").serialize();
	$.ajax({
	  url: contextPath + "/shop/list",
	  type: "GET",
	  data: param,
	  dataType: "HTML",
	  async: true,
	  cache: true,
	  beforeSend :function(xhr){

	  },
	  complete: function(xhr,status){

	  },
	  error: function(){
		  $("#shopList").empty();
	  },
	  success: function(result,status,xhr){
	    $("#shopList").html(result);
	  }
	});
}

/**
 * 收藏店铺
 */
//收藏店铺
function collectShop(_this, shopId) {
	var url = contextPath + "/p/favoriteShopOverlay?shopId="+shopId;
	$.ajax({
		url : url,
		async : false, // 默认为true 异步
		dataType : "json",
		success : function(result) {
			if(result == "OK"){
				layer.msg("收藏店铺成功",{icon:1},function(){
					var em = $(_this).parents("li").find(".collection");
					var collection = em.html();
					collection = Number(collection) + 1;
					em.html(collection);
				});

			}else {
				layer.msg("您好，您已收藏该店铺",{icon:0});
			}
		}
	});
}

/**
 * 登陆窗口
 */
function login(){
	layer.open({
		title :"登录",
		  type: 2,
		  content: contextPath + '/loadLoginOverlay', //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['440px', '530px']
		});
}
