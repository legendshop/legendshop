$(document).ready(function() {

	// Create variables (in this scope) to hold the API and image size
	var jcrop_api, boundx, boundy,

	// Grab some information about the preview pane
	$preview = $('#preview-pane'), $pcnt = $('#preview-pane .preview-container'), $pimg = $('#preview-pane .preview-container img'),

	xsize = $pcnt.width(), ysize = $pcnt.height();

	$('#target').Jcrop({
		onChange : updatePreview,
		onSelect : updatePreview,
		aspectRatio : xsize / ysize,
		maxSize : [ 300, 300 ]
	}, function() {
		// Use the API to get the real image size
		var bounds = this.getBounds();
		boundx = bounds[0];
		boundy = bounds[1];
		// Store the API in the jcrop_api variable
		jcrop_api = this;

		// Move the preview into the jcrop container for css positioning
		$preview.appendTo(jcrop_api.ui.holder);
		jcrop_api.setSelect([ 0, 0, 300, 300 ]);
	});

	function updatePreview(c) {
		if (parseInt(c.w) > 0) {
			var rx = xsize / c.w;
			var ry = ysize / c.h;

			$pimg.css({
				width : Math.round(rx * boundx) + 'px',
				height : Math.round(ry * boundy) + 'px',
				marginLeft : '-' + Math.round(rx * c.x) + 'px',
				marginTop : '-' + Math.round(ry * c.y) + 'px',
			});

			$("#width").val(c.w);
			$("#height").val(c.h);
			$("#marginLeft").val(c.x);
			$("#marginTop").val(c.y);
		}
	}
	;

	$("#uploadImg").click(function() {
		ajaxFileUpload();
	});
	$("#file").change(function(){
		checkImgType($("#file"));
		checkImgSize($("#file"),512);
	});
});

//上传头像
function ajaxFileUpload() {
	$.ajaxFileUpload({
		url : contextPath+'/p/savePhoto',
		secureuri : false,
		fileElementId : 'file',
		dataType : 'json',
		success : function(data, status) {
			if (data == "fail") {
				layer.msg("仅支持JPG、GIF、PNG、JPEG、BMP格式，小于512K的文件");
			} else {
				$(window.parent.document).find("#infoimg").attr("src",photoPath+ data);
				$('img[name="target"]').attr("src",photoPath+ data);
				$(".jcrop-holder img").attr("src",photoPath+ data);
				$("#smallImage").attr("src", photoPath+ data);
				$("#cutImg").removeAttr("disabled");

			}
		}
	});
}

//裁剪头像
function cropPhoto() {
	var width = $("#width").val(), height = $("#height").val(), marginLeft = $(
			"#marginLeft").val(), marginTop = $("#marginTop").val();

	var widthScale = width / $('#target').width(),
	heightScale = height/ $('#target').height(),
	marginLeftScale = marginLeft/ $('#target').width(),
	marginTopScale = marginTop/ $('#target').height();

	$.ajax({
		url : contextPath+"/p/cropPhoto",
		data : {
			"width" : widthScale,
			"height" : heightScale,
			"marginLeft" : marginLeftScale,
			"marginTop" : marginTopScale
		},
		type : 'post',
		dataType : 'json',
		async : true, //默认为true 异步
		success : function(result) {
			if (result == "fail") {
				layer.alert('图片截取失败', {icon: 0});
			} else {
				var touxiang= parent.$('#infoimg').attr("src",photoPath+result);
				/*$("#infoimg" , parent.document).attr("src",photoPath+result);*/
				//$("#infoimg" , parent.document).attr("src",photoPath+result);
			/*	parent.find("#infoimg").attr("src",photoPath+result);*/
				layer.msg('图片截取成功', {icon: 1,time:700},function(){
					var index = parent.layer.getFrameIndex('touxiang'); //先得到当前iframe层的索引
					parent.layer.close(index); //再执行关闭
            	});

			}
		}
	});
}
