jQuery(document).ready(function(){
		//三级联动
	  	$("select.combox").initSelect();
	  //全选
	  	$(".selectAll").click(function(){

	  		if($(this).attr("checked")=="checked"){
	  			$(this).parent().parent().addClass("checkbox-wrapper-checked");
	  	         $(".selectOne").each(function(){
	  	        		 $(this).attr("checked",true);
	  	        		 $(this).parent().parent().addClass("checkbox-wrapper-checked");
	  	         });
	  	     }else{
	  	         $(".selectOne").each(function(){
	  	             $(this).attr("checked",false);
	  	             $(this).parent().parent().removeClass("checkbox-wrapper-checked");
	  	         });
	  	         $(this).parent().parent().removeClass("checkbox-wrapper-checked");
	  	     }
	  	 });

	  	function selectOne(obj){
	  		if(!obj.checked){
	  			$(".selectAll").checked = obj.checked;
	  			$(obj).prop("checked",false);
	  			$(obj).parent().parent().removeClass("checkbox-wrapper-checked");
	  		}else{
	  			$(obj).prop("checked",true);
	  			$(obj).parent().parent().addClass("checkbox-wrapper-checked");
	  		}
	  		 var flag = true;
	  		  var arr = $(".selectOne");
	  		  for(var i=0;i<arr.length;i++){
	  		     if(!arr[i].checked){
	  		    	 flag=false;
	  		    	 break;
	  		    	 }
	  		  }
	  		 if(flag){
	  			 $(".selectAll").prop("checked",true);
	  			 $(".selectAll").parent().parent().addClass("checkbox-wrapper-checked");
	  		 }else{
	  			 $(".selectAll").prop("checked",false);
	  			 $(".selectAll").parent().parent().removeClass("checkbox-wrapper-checked");
	  		 }
	  	}
	});

	function pager(curPageNO){
		document.getElementById("curPageNO").value=curPageNO;
		document.getElementById("from1").submit();
	}

  	/** 更改商品状态**/
 	function pullOff(ths){
	   	var item_id = $(ths).attr("productId");
	   	var status = $(ths).attr("status");
	   	var desc;
	   	var toStatus;
   	    if(status == 1){
   	   		toStatus  = 0;
   	   		desc ="将'" + $(ths).attr("productName")+"'放入仓库?";
   	    }else{
   	       toStatus = 1;
   	       desc = $(ths).attr("productName")+' 上线?';
   	    }

     	layer.confirm(desc,{
	  		 icon: 3
	  	     ,btn: ['确定','取消'] //按钮
	  	   }, function () {
	  		 jQuery.ajax({
					url : contextPath+"/s/updatestatus/" + item_id + "/" + toStatus,
					type : 'get',
					async : false, //默认为true 异步
					dataType : 'json',
					success : function(data) {
						window.location.reload();
					}
				});
		})
	}

	/**删除多个商品**/
	function confirmDelete(prodId, name) {
		layer.confirm("是否将商品：'"+name+"' 放入商品回收站?",{
      		 icon: 3
      	     ,btn: ['确定','取消'] //按钮
      	   }, function () {
			  jQuery.ajax({
					url:contextPath+"/s/product/toDustbin/"+prodId,
					type:'get',
					async : false, //默认为true 异步
					dataType : 'json',
					success:function(data){
						if(data=="OK"){
							window.location.reload();
						}else{
							layer.msg('操作失败！', {icon: 2});
						}
					}
					});
		  });

	}

	function deleteAction() {
		//获取选择的记录集合
//		selAry = document.getElementsByName("strArray");
		selAry = $(".selectOne");
		if (!checkSelect(selAry)) {
			layer.msg('删除时至少选中一条记录', {icon: 0});
			return false;
		}
		layer.confirm("是否放入回收站？",{
      		 icon: 3
      	     ,btn: ['确定','取消'] //按钮
      	   }, function() {
			var totalToDel = 0;
			var onError = 0;
			for (i = 0; i < selAry.length; i++) {
				if (selAry[i].checked) {
					totalToDel = totalToDel + 1;
					var prodId = selAry[i].value;
					var name = selAry[i].getAttribute("arg");
					var result = deleteProduct(prodId, true);
					if ('OK' != result) {
						onError = onError + 1;
					}
				}
			}
			if (onError == 0) {
				layer.msg('删除商品成功！', {icon: 1,time:700},function(){
					window.location.reload();
				});

			} else if (onError < totalToDel) {
				layer.msg('删除部分商品成功！', {icon: 1,time:700},function(){
					window.location.reload();
				});

			} else if (onError == totalToDel) {
				layer.msg('删除商品失败！', {icon: 2});
			}
		}, function() {
			//cancel
		});

		return true;
	}

	function batchOnline(){
		//获取选择的记录集合
		selAry = $(".selectOne");
		if (!checkSelect(selAry)) {
			layer.msg('上线时至少选中一条记录', {icon: 0});
			return false;
		}
		layer.confirm("确定要把选中的商品上线吗？", {
      		 icon: 3
      	     ,btn: ['确定','取消'] //按钮
      	   },function() {
			var totalToOnline = 0;
			var onError = 0;
			for (i = 0; i < selAry.length; i++) {
				if (selAry[i].checked) {
					totalToOnline = totalToOnline + 1;
					var prodId = selAry[i].value;
					var name = selAry[i].getAttribute("arg");
					var result = onlineProduct(prodId);
					if (1 != result) {
						onError = onError + 1;
					}
				}
			}
			if (onError == 0) {
				layer.msg('商品上线成功！', {icon: 1,time:700},function(){
					window.location.reload();
				});
			} else if (onError < totalToOnline) {
				layer.msg('部分商品上线成功！', {icon: 1,time:700},function(){
					window.location.reload();
				});

			} else if (onError == totalToOnline) {
				layer.msg('商品上线失败！', {icon: 2});
			}
		}, function() {
			//cancel
		});

		return true;
	}

	function onlineProduct(prodId){
		var result = 1;
		jQuery.ajax({
			url : contextPath+"/s/updatestatus/" + prodId + "/1",
			type : 'get',
			async : false, //默认为true 异步
			dataType : 'json',
			success : function(data) {
				result = data;
			}
		});
		return result;
	}

	function deleteProduct(prodId, multiDel) {
		var result;
		  jQuery.ajax({
				url:contextPath+"/s/product/toDustbin/"+prodId,
				type:'get',
				async : false, //默认为true 异步
				dataType : 'json',
				success:function(data){
					result = data;
				}
				});
		  return result;
	}
   		  $(document).ready(function() {
			userCenter.changeSubTab("prodInStoreHouse");
	});
