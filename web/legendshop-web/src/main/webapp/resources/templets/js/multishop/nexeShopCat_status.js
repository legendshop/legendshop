function initStatus(itemId, itemName, status, url, target,path){
  	var desc;
  	var toStatus;
  	   if(status == 1){
  	   		toStatus  = 0;
  	   		desc = itemName +' 下线?';
  	   }else{
  	       toStatus = 1;
  	       desc = itemName + ' 上线?';
  	   }

   	layer.confirm(desc,{
		 icon: 3
	     ,btn: ['确定','取消'] //按钮
	   }, function () {

		   jQuery.ajax({
				url: url + itemId + "/" + toStatus,
				type:'get',
				async : false, //默认为true 异步
				dataType : 'json',
				success:function(data){
					$target = $(target);
					if(data==-1){
						layer.alert('用户无效状态，不可进行改操作 !', {icon: 2});
						return;
					}else if(data==-2){
						layer.alert('请先下线该类目下的子商品类目 !', {icon: 0});
						return;
					}else if(data==-3){
						layer.alert('请先上线该类目的上级商品类目 !', {icon: 0});
						return;
					}else if(data == 1){
						//$target.attr("src",path + "/resources/common/images/blue_down.png");
						$target.html("下线");
					}else if(data == 0){
						//$target.attr("src",path + "/resources/common/images/yellow_up.png");
						$target.html("");
					}
					$target.attr("status",data);
					window.location.reload();
				}
				});
	  });

}
