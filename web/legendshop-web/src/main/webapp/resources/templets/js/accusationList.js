function getById(id) {
	window.location.href = contextPath + "/p/accusation/load/" + id;
}

function alertAccusation(id) {
	layer.open({
		title : "举报信息",
		type : 2,
		content : contextPath + '/p/accusation/loadOverlay/' + id,
		area : [ '800px', '500px' ]
	});
}

function deleteById(id) {
	layer.confirm("确定要删除吗？", {
		icon : 3,
		btn : [ '确定', '关闭' ]
	//按钮
	}, function() {
		window.location.href = contextPath + "/p/accusation/del/" + id;

	});
}

function deleteAction() {
	//获取选择的记录集合
	var selAry = $(".selectOne");
	if (!checkSelect(selAry)) {
		layer.msg('删除时至少选中一条记录！', {icon : 0});
		return false;
	}
	layer.confirm("删除后不可恢复, 确定要删除吗？", {
		icon : 3,
		btn : [ '确定', '关闭' ]
	//按钮
	}, function() {
		var accIdArray = [];
		for (i = 0; i < selAry.length; i++) {
			if (selAry[i].checked) {
				var id = selAry[i].value;
				accIdArray.push(id);
			}
		}
		var accIds = JSON.stringify(accIdArray);
		accIds=escape(accIds);
		window.location.href = contextPath + "/p/accusation/delList/"+ accIds;

	});
}

function pager(curPageNO) {
	userCenter.loadPageByAjax("/p/accusation?curPageNO=" + curPageNO);
}

function clearAction() {
	layer.confirm("清空后不可恢复,确定要清空吗？", {
		icon : 3,
		btn : [ '确定', '关闭' ]
	//按钮
	}, function() {
		window.location.href = contextPath + "/p/accusation/delAll";

	});
}

$(document).ready(function() {
	if ("reAcc" == '${param.err}') {
		layer.msg("您已经举报过该商品", {
			time : 500
		});
	}
	userCenter.changeSubTab("accusation");
});

//全选
$(".selectAll").click(function(){
    
	if($(this).attr("checked")=="checked"){
		$(this).parent().parent().addClass("checkbox-wrapper-checked");
         $(".selectOne").each(function(){
        		 $(this).attr("checked",true);
        		 $(this).parent().parent().addClass("checkbox-wrapper-checked");
         });
     }else{
         $(".selectOne").each(function(){
             $(this).attr("checked",false);
             $(this).parent().parent().removeClass("checkbox-wrapper-checked");
         });
         $(this).parent().parent().removeClass("checkbox-wrapper-checked");
     }
 });

function selectOne(obj){
	if(!obj.checked){
		$(".selectAll").checked = obj.checked;
		$(obj).prop("checked",false);
		$(obj).parent().parent().removeClass("checkbox-wrapper-checked");
	}else{
		$(obj).prop("checked",true);
		$(obj).parent().parent().addClass("checkbox-wrapper-checked");
	}
	 var flag = true;
	  var arr = $(".selectOne");
	  for(var i=0;i<arr.length;i++){
	     if(!arr[i].checked){
	    	 flag=false; 
	    	 break;
	    	 }
	  }
	 if(flag){
		 $(".selectAll").prop("checked",true);
		 $(".selectAll").parent().parent().addClass("checkbox-wrapper-checked");
	 }else{
		 $(".selectAll").prop("checked",false);
		 $(".selectAll").parent().parent().removeClass("checkbox-wrapper-checked");
	 }
}
