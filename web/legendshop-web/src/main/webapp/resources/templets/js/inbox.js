function sendMessage(){
	$(".l1").removeClass("on");
	$("#sendMessage").addClass("on");
	url = contextPath +"/p/sendMessage";
	sendData(url);
}

function outbox(){
	$(".l1").removeClass("on");
	$("#outbox").addClass("on");
	url=contextPath +"/p/outbox";
	sendData(url);
}

function systemMessages(){
	$(".l1").removeClass("on");
	$("#systemMessages").addClass("on");
	url=contextPath +"/p/systemMessages";
	sendData(url);
}

function imMessages(){
	$(".l1").removeClass("on");
	$("#imMessages").addClass("on");
	url=contextPath +"/p/imMessages";
	sendData(url);
}

function pager(curPageNO){
	sendData(contextPath +"/p/inbox?curPageNO=" + curPageNO);	
}

function sendData(url){
	window.location.href = url;
}

//页面的删除
function deleteAction(type){
	//获取选择的记录集合
	selAry = $(".selectOne");
	if(!checkSelect(selAry)){
		layer.alert('删除时至少选中一条记录！',{icon: 0});
		return false;
	}

	layer.confirm("删除后不可恢复, 确定要删除吗？", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		var ids = []; 
		for(i=0;i<selAry.length;i++){
			if(selAry[i].checked){
				ids.push(selAry[i].value);
			}
		}
		var result = deleteInbox(ids,type);
		if('OK' == result){
			layer.msg('删除消息成功',{icon: 1});
			setTimeout("goback();","1000");
		}else{
			layer.msg('删除消息失败',{icon: 2});
		}

	});
	return true;
}

//清空
function clearAction(){
	layer.confirm("清空后不可恢复, 确定要清空吗？", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		$.post(contextPath +"/p/clearInboxMessage",function(result){
			if('OK' == result){
				layer.msg('清空消息成功',{icon: 1});
				setTimeout("goback();","1000");
			}else{
				layer.msg('清空消息失败',{icon: 2});
			}
		},"json");
	});
}
function deleteInbox(ids,type) {
	var result;
	var idsJson = JSON.stringify(ids);
	var ids = {"ids": idsJson};
	var url = contextPath +"/p/" + type;
	jQuery.ajax({
		"url":url , 
		data: ids,
		type:'post', 
		async : false, //默认为true 异步   
		dataType : 'json', 
		success:function(retData){
			result = retData;
		}
	});

	return result;
}

function mailinfo(msgId){
	if(msgId !=" "){
		$.ajax({
			url: contextPath +"/p/loadinbox/"+msgId, 
			type:'post', 
			async : true, //默认为true 异步   
			success:function(result){
				$("#mailInfo").html(result);
			}
		});
	}
}

function goback(){
	$("#inbox").click();
}

$(document).ready(function() {
	userCenter.changeSubTab("inbox");
});
