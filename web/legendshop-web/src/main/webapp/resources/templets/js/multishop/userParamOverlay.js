function pager(curPageNO){
	var thisObj = $("a[class^=J_PickTrigger]");
	selectUserParam(thisObj,curPageNO);
}

//删除自定义属性
function deleteParam(id){
	$.ajax({
		url:contextPath + '/s/userParam/delete',
		data: {"id":id},
		type:'post', 
		async : true, //默认为true 异步   
		success:function(result){
			$("#pup" + id).remove();
		}
	});
}

//使用该模板
function useParam(id){
	$.ajax({
		url:contextPath + '/s/useParam/'+id, 
		type:'post', 
		dataType : 'json',
		async : true, //默认为true 异步   
		error: function(jqXHR, textStatus, errorThrown) {
			// alert(textStatus, errorThrown);
		},
		success:function(result){
			if(result !=null){
				//console.debug("result: "+result);
				var dataObj = eval("("+result+")");

				$.each(dataObj,function(idx,item){     
					//输出
					// console.debug(item.key+"，"+item.value+"，"+idx);
					$("#prop"+idx).val(item.key);
					$("#propValue"+idx).val(item.value);
				})
				$('.area_box').remove();
			}
		}
	});
}