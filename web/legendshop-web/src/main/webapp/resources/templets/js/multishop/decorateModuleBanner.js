/**
 *  Tony 
 */
$(document).ready(function(){
	
	  //切换
	  $(".fiit_side_tab li").live("click",function(){
			var count =$(this).attr("count");
			$(".fiit_side_tab li").removeClass("this");
			$(this).addClass("this");
			$(".fiit_banner dl").hide();
			$(".fiit_banner dl[count="+count+"]").show();
      });
	  
	  
		//ajax上传图片
	  $("input[id^=img_file_]").live("change",function(){
		  var count=$(this).attr("count");	
		  var fileElementId='img_file_'+count;
		 if(!validateFile(fileElementId)){
				 return false;
		 }
		 var name=$(this).attr("name");	
		 var val =$(this).val();
		 $("#"+name+"_show").val(val);							 
		 var img_id =$("#slide_img_"+count+" img").attr("id");	
		 var slide_id =$(".fiit_side_tab li[class='this']").attr("slide_id");
		 jQuery.ajaxFileUpload({
			      url: contextPath + '/s/shopDecotate/layout/layout_slide_upload',             
			      secureuri:false,
			      fileElementId:['img_file_'+count],                         
			      dataType: 'json',
			      error: function (data, status, e){
			    	  parent.layer.alert(data, {icon: 2});
			       },
			       success: function (data, status){  
			     	 if(data =="fail"){
			     		 parent.layer.alert(data, {icon: 2});
			     	 }else{
			     		 var image="<img path="+data+"  src="+photoPath+data+">";
			     		 $("#slide_img_"+count+"").html(image);
			     		 $("#img_files_"+count+"").val(data);
						// $("#slide_img_"+count+" img").removeAttr("style").attr("src",photoPath+data).attr("path",data);
			     	 }
			      }
		  });
 	  });
	
});

//添加幻灯
function addSlide() {
	var count = $(".fiit_side_tab ul li").length;
	count++;
	if (count <= 5) {
		var slide_tab = '<li count="' + count
				+ '"><a href="javascript:void(0);">幻灯' + count + '</a></li>';
		var slide_text = '<dl count="'
				+ count
				+ '"  style="display:none"><dt><span class="fl">幻灯'
				+ count
				+ '：</span><span class="fr"><a href="javascript:void(0);" onclick="removeslide()"><i></i>删除</a></span></dt> <dd><span class="fl">上传图片：</span><span class="fr"><div class="file_box"><input  type="hidden" name="img_files"  id="img_files_'
				+ count
				+ '" /><input class="ip180" type="text"  id="img_file_'
				+ count
				+ '_show" /><input type="button" class="btn" value="浏览..." /><input type="file" class="file" id="img_file_'
				+ count
				+ '" name="img_file_'
				+ count
				+ '" count="'
				+ count
				+ '"/> </div> </span></dd><dd><span class="fl">图片：</span><span class="fr img" id="slide_img_'
				+ count
				+ '"></span></dd> <dd><span class="fl">图片url：</span><span class="fr"><input class="ip300" id="url_'
				+ count + '" name="urls' 
				+ '" type="text" /></span></dd></dl>';
		$(".fiit_side_tab ul").append(slide_tab);
		$(".fiit_banner").append(slide_text);
	} else {
		parent.layer.alert("最多可添加5个幻灯", {icon: 0});
	}
}

function saveSlide() {
	var params = $("#BannerForm").serialize();
	var layoutId = $("#layoutId").val();
	var layoutDiv = $("#layoutDiv").val();
	var layout=$("#layout").val();
	
	var count = $(".fiit_side_tab ul li").length;
	for( var i=1;i<=count;i++){
		var _url=$("#url_"+i);
		var url=$.trim($(_url).val());
		if(isBlank(url)){
			$(".fiit_side_tab li").removeClass("this");
			$(".fiit_side_tab li[count="+i+"] ").addClass("this");
			$(".fiit_banner dl").hide();
			$(".fiit_banner dl[count="+i+"]").show();
			parent.layer.alert("请输入Url地址", {icon: 0});
			return ;
		}
	 }
	 var imageCount=$(".fiit_banner dl img").length;
	 if(imageCount==0){
	 	parent.layer.alert("至少上传一张图片", {icon: 0});
		return ;
	   }
	   $("#save").attr("disabled","disabled");
	   $.ajax({
	         //提交数据的类型 POST GET
	         type:"POST",
	         //提交的网址
	         url:contextPath+"/s/shopDecotate/layout/bannerSave",
	         //提交的数据
	         data: params,
	         async : false,  
	         //返回数据的格式
	         datatype: "text",
	         //成功返回之后调用的函数            
	         success:function(data){
	        	  var result=$.trim(data);
	        	  if(result.indexOf("fail")!=-1){
	        		  parent.layer.alert("保存失败", {icon: 2});
	        		  $("#save").removeAttr("disabled");
	        	  }
	        	  else{
						$('#module_edit').remove();
						if (layoutDiv != "") {
							parent.$("div[mark=" + layoutId + "][div=div2][id='content']").html("");
							parent.$("div[mark=" + layoutId + "][div=div2][id='content']").html(result);
						} else {
							//parent.$("div[option=" + layoutId + "]").find("a:first").before(data);\
							parent.$("div[mark=" + layoutId + "][id='content']").html("");
							parent.$("div[mark=" + layoutId + "][id='content']").html(result);
							
						}
						$("#save").removeAttr("disabled");
					
						var index = parent.layer.getFrameIndex('decorateModuleBanner'); //先得到当前iframe层的索引
						parent.layer.close(index); //再执行关闭   

	        	  }
	         },
	         //调用执行后调用的函数
	         complete: function(XMLHttpRequest, textStatus){
	         }      
	   });
}
		 
    


function removeslide(){
	 jQuery(".fiit_banner dl:last").remove();
	  jQuery(".fiit_side_tab li:last").remove();
	  jQuery(".fiit_side_tab li").removeClass("this");
	  jQuery(".fiit_banner dl").hide();
	  jQuery("dl[count=1]").show();
	  jQuery(".fiit_side_tab li[count=1]").addClass("this").show();
}


function validateFile(_name) {
	var filepath = jQuery("input[name='"+_name+"']").val();
	var extStart = filepath.lastIndexOf(".");
	var ext = filepath.substring(extStart, filepath.length).toUpperCase();
	if (ext != ".BMP" && ext != ".PNG" && ext != ".GIF" && ext != ".JPG"
			&& ext != ".JPEG") {
		parent.layer.alert("图片限于bmp,png,gif,jpeg,jpg格式", {icon: 0});
		return false;
	}
	return true;
}


function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}


