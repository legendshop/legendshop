var period_timeCount = $(".down-time");//本场倒计时
var cur_hour = new Date().getHours();
var date = new Date();
var cur_Tab=$(".now");//当前时段tab
var cur_index = $(".sec-lis-tit ul li").index(cur_Tab);//当前时段在ul中的index
var set;//计时器
clearInterval(set);
set = setInterval("setRemainTime('Running')",1000);
var time_start;//活动时段的开始时间
var time_end;//活动时段的结束时间
$(document).ready(function(){
	getTimeOfSeckill(cur_hour);//取得当前活动的开始和结束时间
	if(cur_hour<9){
		$(".sec-lis-tit ul li").find("em").html("秒杀准备中");
		$(".sec-lis-tit ul li").first().addClass("sec-arrow now");
		$(".sec-lis-tit ul li").first().click();
		// "<div>秒杀活动时间为<b>上午9：00--晚上24：00</b></div>"
		return;
	}else{
		//console.log(cur_index);
		//当前时段之前的所有时段都设为已结束
		cur_Tab.prevAll().find("em").html("已结束");
		//当前时段之后的所有时段都设为未开始
		cur_Tab.nextAll().find("em").html("秒杀准备中");
		//当前时段设为秒杀中
		cur_Tab.find("em").html("秒杀中");
		timeCount("Running");
		cur_Tab.attr("hasclicked","1");
		cur_Tab.click();
	}
	
});

function getTimeOfSeckill(cur_hour){//获得活动所在时间段	
	if(cur_hour<9){
		time_start=0;
		time_end=0;
	}else if(9<=cur_hour&&cur_hour<12){
		time_start = date.setHours(9,0,0);
		time_end = date.setHours(11,59,59);
	
	}else if(12<=cur_hour&&cur_hour<15){
		time_start =date.setHours(12,0,0);
		time_end = date.setHours(14,59,59);
	}else if(15<=cur_hour&&cur_hour<18){
		time_start =date.setHours(15,0,0);
		time_end =date.setHours(17,59,59);
	
	}else if(18<=cur_hour&&cur_hour<21){
		time_start = date.setHours(18,0,0);
		time_end = date.setHours(20,59,59);
	
	}else if(21<=cur_hour&&cur_hour<24){
		time_start = date.setHours(21,0,0);
		time_end = date.setHours(23,59,59);
	}
}
function getTimeOfClick(click_tab){//获得选中时间段
	//console.log(click_tab);
	var text=$(click_tab).find("span").html().split(":");
	time_start=date.setHours(parseInt(text[0]),0,0);
	time_end=date.setHours(parseInt(text[0])+2,59,59);
}


function tabClick(e){
	if($(e).hasClass("sec-arrow")){
		if(!$(e).hasClass("now")){
			return;
		}else{
			if($(e).attr("hasClicked") == "0"){
			 return;
			 }else if($(e).attr("hasClicked") == "1"){
				 $(e).attr("hasClicked","0");
			}
		}
	}
	
	//点击时段tab 添加sec-arrow 其他的移除sec-arrow
	var click_index=$(".sec-lis-tit ul li").index($(e));
	if(click_index!=0){
		$(e).addClass("sec-arrow");
		$(e).prevAll().removeClass("sec-arrow");
		$(e).nextAll().removeClass("sec-arrow");
		//本场倒计时更新
		if(click_index<cur_index){
			//本场已结束
			$(".sec-lis-tim span").hide();
			period_timeCount.find("p").html("本场秒杀已结束");
			period_timeCount.find("ul").hide();
			getTimeOfClick(e);
			timeCount("");
		}else if(click_index==cur_index){
			//本场进行中
			$(".sec-lis-tim span").show();
			period_timeCount.find("p").html("距离秒杀结束还有");
			period_timeCount.find("ul").show();
			period_timeCount.find("ul li").html("00");
			getTimeOfClick(e);
			timeCount("Running");
		}else if(click_index>cur_index){
			//本场未开始
			$(".sec-lis-tim span").hide();
			period_timeCount.find("p").html("距离秒杀开始还有");
			period_timeCount.find("ul").show();
			getTimeOfClick(e);
			timeCount("Ready");
		}
		//console.log(date.getHours());
		var startTime = date.getHours()-2;
		//window.location = contextPath+"/seckillActivity/list?startTime="+startTime;
		var url = contextPath+"/seckillActivity/querySeckillList?startTime="+ startTime+"&&curPageNO="+pageNo;
		$(".seckillProdList").load(url);

	}
}
function timeCount(statu){
	if(statu==""||statu==undefined ||statu==null){
	}else {
		//console.log(statu);
		clearInterval(set);
		if("Running"==statu){			
			set = setInterval("setRemainTime('Running')",10);
		}else if("Ready"==statu){
			set = setInterval("setRemainTime('Ready')",10);
		}
	}
}
function setRemainTime(statu){	
	//取得当前时间
	var nowTime = new Date().getTime(); //设定当前时间
	var time_distance ;// 时间差
	//取得时间差
	if("Running"==statu){
		time_distance = time_end - nowTime; 
	}else if("Ready"==statu){
		time_distance = time_start - nowTime; 
	}
	//减少时间差
	 time_distance=time_distance-10;
	 if(time_distance>0){
	 	//将时间差转换为时分秒形式
		var h_off=time_distance/1000/3600;
		var m_off=time_distance/1000/60;
		var s_off=time_distance/1000;
		var hours =format(Math.floor(h_off%24));
		var mins = format(Math.floor(m_off%60));
		var secs = format(Math.floor(s_off%60));
		//console.log(hours+":"+mins+":"+secs);
		//更新界面时间
		period_timeCount.find("ul").html("<li>"+hours+"</li><li>"+mins+"</li><li>"+secs+"</li>");
	  }
}
function format(time){
	if(time<10){
		return "0"+time;
	}else return time;
}