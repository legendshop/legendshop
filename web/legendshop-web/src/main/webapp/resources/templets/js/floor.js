$(function(){
	//首页Tab标签卡滑门切换
    $(".pro-right-tit > ul > li > h3").bind('mouseover', (function(e) {
    	if (e.target == this) {
    		var _li=$(this).parent();
    		$(_li).addClass("arrow").siblings().removeClass("arrow");
    		var panels =$(_li).parent().parent().parent().find(".pro-right-con");
    		$(panels).css('display','none');
    		var index =  $(_li).index();
    		$(panels).eq(index).css('display','block');
    	}
    	
    }));
});