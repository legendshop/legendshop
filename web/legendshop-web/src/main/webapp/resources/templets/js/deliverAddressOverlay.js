function validateForm(){
     var param = new Object();
     param.errorCount = 0;
     param.focus = false;
		validateNullFiled($('#contactName'),param, 'consigneeNameNote', '请您填写收货人姓名');
		validateNullFiled($('#provinceid'),param,'areaNote','请选择');
		validateNullFiled($('#cityid'),param,'areaNote','请选择');
		validateNullFiled($('#areaid'),param,'areaNote','请选择');
		validateNullFiled($('#subAdds'),param, 'consigneeAddressNote', '请输入详细地址');
		validatePhoneFiled($('#mobile'), param, 'consigneeMobileNote','请输入手机号码');
		return param.errorCount;
}


function validatePhoneFiled(obj, param, name, description){
	if(param.errorCount == 0){
	 	var nameNote = $('#' + name);
		//验证电话号码手机号码，包含至今所有号段   
	 	var reg=/^1\d{10}$/;
		if(!reg.test(obj.val())){
				   param.errorCount =  param.errorCount + 1;
			     nameNote.removeClass("hide");
			     if(!param.focus){
			     	obj.focus();
			     	param.focus = true;
			     }
			     nameNote.html(description);
			     }else{
			     	nameNote.addClass("hide");
			     }
			 }
}

function addDeliverAddress(){
	if(validateForm() == 0){
	       var data = {
	    	"addrId": $("#id").val(),
	    	"contactName": $("#contactName").val(),
         	"provinceId": $("#provinceid").val(),
         	"cityId": $("#cityid").val(),
         	"areaId": $("#areaid").val(),
         	"adds": $("#subAdds").val(),
         	"mobile": $("#mobile").val(),
            };
			$.ajax({
			url: contextPath + "/s/saveDeliverAddress" , 
			data: data,
			type:'post', 
			dataType : 'json', 
			async : true, //默认为true 异步   
			success:function(result){
			    if(result == 'OK'){
			      layer.msg('保存成功！', {icon: 1});
			      window.parent.refreshAddress();
				 layer.closeAll();
			      return;
			    }else if(result == 'MAX'){
			    	 layer.msg('您已经创建了5个收货地址！', {icon: 0});
			    	 return;
			    }else{
			       layer.msg('保存失败！', {icon: 2});
			    }
			  }
			});
	}

}

function validateNullFiled(obj, param, name, description){
	if(param.errorCount == 0){
	 	var nameNote = $('#' + name);
		if(obj.val().length == 0){
				   param.errorCount =  param.errorCount + 1;
			     nameNote.removeClass("hide");
			     if(!param.focus){
			     	obj.focus();
			     	param.focus = true;
			     }
			     nameNote.html(description);
			     }else{
			     	nameNote.addClass("hide");
			     }
	}

}

