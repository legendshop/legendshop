
/* 添加发票 */
function addInvoice(){

    layer.open({
        title :"添加发票信息",
        id: "addInvoice",
        type: 2,
        resize: false,
        content: [contextPath+"/p/invoice/createPage",'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
        area: ['860px', '600px']
    });
}
/* 更新发票 */
function updateById(invoiceId){

    layer.open({
        title :"编辑发票信息",
        id: "updateInvoice",
        type: 2,
        resize: false,
        content: [contextPath+"/p/invoice/update?invoiceId="+invoiceId,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
        area: ['860px', '600px']
    });
}

/* 设为默认发票 */
function setDefault(invoiceId){

    layer.confirm("确定设置该发票为默认？", {
        icon: 3
        ,btn: ['确定','关闭'] //按钮
    }, function(index){
        $.ajax({
            url:contextPath+"/p/invoice/changeInvoiceCommon?id="+invoiceId,
            type:'post',
            async : true,
            dataType : 'json',
            success:function(result){

                if (result == 'OK'){
                    //刷新发票列表
                    window.location.reload();
                }else {
                    layer.msg(result,{icon:0});
                }
            }
        });
        layer.close(index); //再执行关闭
    });
}


/*分页*/
function pager(curPageNO){
    userCenter.loadPageByAjax("/p/invoice/invoiceList?curPageNO=" + curPageNO);
}

/*发票信息 删除*/
function deleteById(invoiceId){
    layer.confirm("确定删除该发票信息？", {
        icon: 3
        ,btn: ['确定','关闭'] //按钮
    }, function(index){
        $.ajax({
            url:contextPath+"/p/invoice/delete/"+invoiceId,
            type:'post',
            async : true,
            dataType : 'json',
            success:function(result){

                if (result == 'OK'){
                    //刷新发票列表
                    window.location.reload();
                }else {
                    layer.msg("删除失败，请稍后重试",{icon:0});
                }
            }
        });
        layer.close(index); //再执行关闭
    });
}

/* 关闭添加发票的弹窗*/
function cancelAddInvoice(){

    layer.closeAll();
}

/* 保存或更新发票信息成功后执行的函数 */
function saveOrUpdateInvoiceSuccess() {

    layer.closeAll();
    location.reload();
}
