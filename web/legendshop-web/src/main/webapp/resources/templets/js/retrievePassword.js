/**检查是否为手机号码形式**/
	function isPhone(str){
		var mobile = /^1\d{10}$/;
		return mobile.test(str);
	}
	
	/**检查手机号码是否存在**/
	function checkPhone() {
		var result = true;
		var userPhoneValue = jQuery("#name").val();
		$.ajax({
				url: contextPath + "/isPhoneExist", 
				data: {"Phone":userPhoneValue},
				type:'post', 
				async : false, //默认为true 异步   
				success:function(retData){
					if('true' == retData){
						result = false;
					}
				}
			});
		return result;
	}
	
	/*** 检查是否由数字字母和下划线组成 ***/
	String.prototype.isAlpha = function() {
		return (this.replace(/\w/g, "").length == 0);
	}
	/**检查用户名是否存在**/
	function checkName() {
		var result = true;
		var nameValue = jQuery("#name").val();
	    $.ajax({
			url: contextPath + "/isNickNameExist", 
			data: {"nickName":nameValue},
			type:'post', 
			async : false, //默认为true 异步   
			success:function(retData){
				 if('true' == retData){
					result = false;
					}
			}
		});
    return result;
	}
	
	/**检查是否为邮箱形式**/
	function isEmail(str){
		var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
		return reg.test(str);
	}
	/**检查邮箱是否存在**/	
	function checkEmail() {
		var result = true;
		var userMailValue = jQuery("#name").val();
		$.ajax({
			url: contextPath + "/isEmailExist", 
			data: {"email":userMailValue},
			type:'post', 
			async : false, //默认为true 异步   
			success:function(retData){
				if('true' == retData){
					result = false;
				}
			}
		});
		return result;
	}

	function setfocus(){
		$("#name").attr("class","text highlight1");
		$("#name_error").show();
		$("#name_error").attr("class","msg-text");
		$("#name_error").html("请输入您的用户名/邮箱/已验证手机");
	}
	
	function setblur(){
		var name = $("#name").val().trim();
		if(name.length!=0){
			if(isEmail(name)){
				if(checkEmail()){
					$("#name").attr("class","text highlight2");
					$("#name_error").show();
					$("#name_error").attr("class","msg-error");
					$("#name_error").html("此邮箱不存在！");
				}else{
					$("#name").attr("class","text");
					$("#name_error").hide();
				}
			}else if(isPhone(name)){
				if(checkPhone()){
					$("#name").attr("class","text highlight2");
					$("#name_error").show();
					$("#name_error").attr("class","msg-error");
					$("#name_error").html("此手机号码不存在！");
				}else{
					$("#name").attr("class","text");
					$("#name_error").hide();
				}
			}else if(checkName()){
				$("#name").attr("class","text highlight2");
				$("#name_error").show();
				$("#name_error").attr("class","msg-error");
				$("#name_error").html("此用户名不存在！");
			}else{
				$("#name").attr("class","text");
				$("#name_error").hide();
			}
		}else{
			$("#name").attr("class","text");
			$("#name_error").hide();
		}
	}
	
	function randNumSetFocus(){
		$("#randNum").attr("class","text highlight1");
	}
	
	function randNumSetBlur(){
		$("#randNum").attr("class","text");
	}
	
	function doIndex(){
		if($("#name").val().length ==0){
			$("#name").attr("class","text highlight2");
			$("#name_error").show();
			$("#name_error").attr("class","msg-error");
			$("#name_error").html("请填写您的用户名/邮箱/已验证手机");
		}else{
		  if(validateRandNum(contextPath)){
			$.ajax({
				url:contextPath+'/proveStatus', 
				data:{"name":$("#name").val()},
				type:'post', 
				async : true, //默认为true 异步   
				success:function(result){
				  $("#entry").html(result);
				  changeRandImg(contextPath);
				}
				});
		   }
	   }
	}
	
	function selectVerifyType(obj){
		if(obj=="email"){
			$("#emailDiv").show();
			$("#mobileDiv").hide();
		}else{
			$("#mobileDiv").show();
			$("#emailDiv").hide();
		}
	}
	
	//验证用户输入的验证码是否正确
	function verifySMSCode(){
		var verify = false;
		var name = $("#userName").val();
		$.ajax({
			url:contextPath+'/verifySMSCode', 
			data:{"userName":name,"code":$("#code").val()},
			type:'post', 
			dataType:"json",
			async : false, //默认为true 异步   
			success:function(result){
				verify = result;
			}
			});
		return verify;
	}
	
	function codeFocus(){
	    $("#code").attr("class","text text-1 highlight1");
	    $("#code_error").hide();
	}
	
	function codeBlur(){
		var codeValue = $("#code").val();
		if(codeValue.length == 0 || codeValue.length < 6){
			$("#code").attr("class","text text-1 highlight2");
			$("#code_error").show();
			$("#code_error").attr("class","msg-error");
			$("#code_error").html("验证码错误");
		}else{
			$("#code").attr("class","text text-1");
			$("#code_error").hide();
		}
	}
	
	//去往修改密码页面
	function validFindPwdCode(){
		if(verifySMSCode()==true){
			$.ajax({
				url:contextPath+'/changePwd', 
				type:'post', 
				data:{"userName":$("#userName").val(),"code":$("#code").val()},
				async : true, //默认为true 异步   
				success:function(result){
				  $("#entry").html(result);
				}
				});
		}else{
			$("#code").attr("class","text text-1 highlight2");
			$("#code_error").show();
			$("#code_error").attr("class","msg-error");
			$("#code_error").html("验证码错误");
		}
	}
	
	function updatePassword(){
		var rpwValue = $("#repassword").val();
		var pwValue = $("#password").val();
		var pwError = $("#password_error");
		var rpwError = $("#repassword_error");
			
		var regx1= /^[0-9A-z`~!@#$%^&*()_\-+=<>?:"{}|,.\/;'\\[\]·~！@#￥%……&*（）——\-+={}|《》？：“”【】、；‘’，。、]+$/g;
		var regx= /(?!^[0-9]+$)(?!^[A-z]+$)(?!^[~\\`!@#$%\\^&*\\(\\)-_+={}|\\[\\];':\\\",\\.\\\\\/\\?]+$)(?!^[^A-z0-9]+$)^.{6,20}$/;

		if(pwValue.indexOf(" ") > -1){
			$("#password").attr("class","text highlight2");
			pwError.show();
			pwError.attr("class","msg-error");
			pwError.text("密码不能包含空格");	
			return;
		}
		
		if (!regx.test(pwValue) || !regx1.test(pwValue)) {
			pwError.show();
			pwError.attr("class","msg-error");
			pwError.text("密码应为6-20位字母、数字和标点符号的组合！");
			return;
		}
		
		if(pwValue.length ==0){
			$("#password").attr("class","text highlight2");
			pwError.show();
			pwError.attr("class","msg-error");
			pwError.text("请设置新密码");
		}else if(pwValue.length < 6 || pwValue.length > 20){
			$("#password").attr("class","text highlight2");
			pwError.show();
			pwError.attr("class","msg-error");
			pwError.text("密码长度不正确，请重新设置");
		}else if(rpwValue.length ==0){
			$("#repassword").attr("class","text highlight2");
			rpwError.show();
			rpwError.attr("class","msg-error");
			rpwError.text("请确认新密码");
		}else if(rpwValue != pwValue){
			$("#repassword").attr("class","text highlight2");
			rpwError.show();
			rpwError.attr("class","msg-error");
			rpwError.text("两次输入的密码不一致，请重新输入");
		}else{
			$.ajax({
				url:contextPath+'/pwdResetSuccess', 
				type:'post', 
				data:{"newPassword":$("#password").val(),"userName":$("#userName").val(),"code":$("#code").val()},
				async : false, //默认为true 异步   
				success:function(result){
				  $("#entry").html(result);
				}
				});
		}
	}
