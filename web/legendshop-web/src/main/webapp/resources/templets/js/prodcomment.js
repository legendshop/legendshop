$(document).ready(function() {
	mayFav.bindPageAction();
	userCenter.changeSubTab("prodcomment");
	//初始化组件
	layui.use(['rate'], function(){
		var rate = layui.rate;
		$("div[id^='prodScore_'").each(function(index, element){
			rate.render({
				elem: element,
				value: 0,
				text: true,
				theme: '#E4393C',
				choose: function(value){
					$(element).next().val(value);
				}
			})
		});

		$("div[id^='shopScore_'").each(function(index, element){
			rate.render({
				elem: element,
				value: 0,
				text: true,
				theme: '#E4393C',
				choose: function(value){
					$(element).next().val(value);
				}
			})
		});

		$("div[id^='logisticsScore_'").each(function(index, element){
			rate.render({
				elem: element,
				value: 0,
				text: true,
				theme: '#E4393C',
				choose: function(value){
					$(element).next().val(value);
				}
			})
		});
	}); 

	//展开或收起发表评论表单
	$(".comments-btn").on("click", function(){
		var $this = $(this);

		var tr = $this.parent().parent().parent().next("tr");

		if(tr.is(":visible")){
			tr.removeClass("show");
			tr.addClass("hide");
		}else{
			tr.removeClass("hide");
			tr.addClass("show");
		}
		return false;
	});

	//发表评价
	$(".commit-comment-btn").on("click", function(){
		var $this = $(this);
		$this.css('pointer-events', 'none');
		var form = $this.parent().parent().parent().parent("form.comments-form");

		var prodScore = Number(form.find("input[name='prodScore']").val());
		var shopScore = Number(form.find("input[name='shopScore']").val());
		
	    var prodId = form.find("input[name='prodId']").val();
	    var subItemId = form.find("input[name='subItemId']").val();
		var logisticsScore = Number(form.find("input[name='logisticsScore']").val());
		if(prodScore < 0 || prodScore > 5){
			$this.css('pointer-events', 'auto');
			layer.msg("对不起, 宝贝评分异常, 请重新选择评分!",{icon: 0});
			return false;
		}
		if(shopScore < 0 || shopScore > 5){
			$this.css('pointer-events', 'auto');
			layer.msg("对不起, 店铺评分异常, 请重新选择评分!",{icon: 0});
			return false;
		}
		if(logisticsScore < 0 || logisticsScore > 5){
			$this.css('pointer-events', 'auto');
			layer.msg("对不起, 物流评分异常, 请重新选择评分!",{icon: 0});
			return false;
		}

		if(prodScore==0){
			$this.css('pointer-events', 'auto');
			layer.msg("对不起, 宝贝评分不能为空,请选择评分!",{icon: 0});
			return false;
		}

		if(shopScore==0){
			$this.css('pointer-events', 'auto');
			layer.msg("对不起, 店铺评分不能为空,请选择评分!",{icon: 0});
			return false;
		}

		if(logisticsScore==0){
			$this.css('pointer-events', 'auto');
			layer.msg("对不起, 物流评分不能为空,请选择评分!",{icon: 0});
			return false;
		}
		var content = form.find("textarea[name='content']").val();
		content = $.trim(content);
		if(!content){
			$this.css('pointer-events', 'auto');
			layer.msg("对不起, 评论内容不能为空!",{icon: 0});
			return false;
		}
		if(content.length < 5){
			$this.css('pointer-events', 'auto');
			layer.msg("对不起, 评论内容不能少于5个字符!",{icon: 0});
			return false;
		}
		if(content.length > 500){
			$this.css('pointer-events', 'auto');
			layer.msg("对不起, 评论内容不能超过500个字符!",{icon: 0});
			return false;
		}

		var checkResult = checkSensitiveData(content);
		if(checkResult && checkResult.length){
			$this.css('pointer-events', 'auto');
			layer.msg("对不起, 您输入的内容包含 [" + checkResult+ "] 等敏感词!",{icon: 0});
			return false;
		}

		if(!checkPhotos(form)){
			return false;
		}
		var addCommForm = $("#faddComm" + "-" + prodId + "-" + subItemId);
		var photoFile;
		var formData = new FormData(addCommForm[0]);
		var arr = [];

		//删除原有文件
		formData.delete("photos")
		//console.log("content:"+formData.get("content"));
		for (let i = 0; i < 5 ; i++) {
            photoFile = addCommForm.find(".photoFile")[i].files[0];

            if (photoFile!=""&&photoFile!=undefined&&photoFile!=null){
                arr.push(photoFile);
                //console.log("photoFile:" + photoFile+"size:"+photoFile.size)
            }
        }
		//console.log("arrFile:"+arr);
        if(arr && arr.length){
            imgCompress.batchCompress(arr, function(compressImg){
                //console.log("compressImg:"+compressImg)
                //将图片加入formData
                for (let i = 0; i < 5 ; i++) {
                    if (compressImg[i]!=""&&compressImg[i]!=undefined&&compressImg[i]!=null){
                       // console.log("压缩后图片:"+compressImg[i]);
                        formData.append("photos", compressImg[i]);
                    }
                }
                //console.log("提交文件"+formData.getAll("photos"))
                submitCommForm(formData,addCommForm);
            });
        }else {
			//console.log("111");
			submitCommForm(formData,addCommForm);
		}
		//console.log("2222");
	});
	function submitCommForm(formData,addCommForm){
		$.ajax({
			url: addCommForm.attr("action"),
			type: "POST",
			data: formData,
			dataType: "JSON",
			timeout: 10000, //十秒超时
			async: true,
			//要想用jquery的ajax来提交FormData数据,
			// 则必须要把这两项设为false
			processData: false,
			contentType: false,
			beforeSend:function(){
				loading= layer.msg('请稍候……', { icon: 16, shade: 0.01,shadeClose:false,time:60000 });
			},
			complete:function(){
					layer.close(loading);
			},
			success: function(result){
				if(result === "OK"){
					$.remindUtils.showTips("恭喜您, 评价成功!", function(){
						window.location.reload(true);
					});
				}else if(result == "SUCCESS"){
					$.remindUtils.showTips("评论提交成功", function(){
						window.location.reload(true);
					});
				}else if(result === "fail"){
					// $this.css('pointer-events', 'auto');
          $(".commit-comment-btn").css('pointer-events', 'auto');
          $(".commit-add-comment-btn").css('pointer-events', 'auto');
					layer.msg("对不起，文件上传有误， 请稍后重试！",{icon: 2});
				}else{
					// $this.css('pointer-events', 'auto');
          $(".commit-comment-btn").css('pointer-events', 'auto');
          $(".commit-add-comment-btn").css('pointer-events', 'auto');
					layer.msg(result,{icon: 2});
				}
			}
		});
	}
	//发表追加评论
	$(".commit-add-comment-btn").on("click", function(){
		var $this = $(this);
		
		$this.css('pointer-events', 'none');
		
		var form = $this.parent().parent().parent().parent("form.add-comments-form");
	    var prodCommId = form.find("input[name='prodCommId']").val();

		var content = form.find("textarea[name='content']").val();

		content = $.trim(content);
		if(!content){
			$this.css('pointer-events', 'auto');
			layer.msg("对不起, 评论内容不能为空!",{icon: 0});
			return false;
		}
		if(content.length > 500){
			$this.css('pointer-events', 'auto');
			layer.msg("对不起, 评论内容不能超过500个字符!",{icon: 0});
			return false;
		}
		if(content.length < 5){
			$this.css('pointer-events', 'auto');
			layer.msg("对不起, 评论内容不能少于5个字符!",{icon: 0});
			return false;
		}

		var checkResult = checkSensitiveData(content);
		if(checkResult && checkResult.length){
			$this.css('pointer-events', 'auto');
			layer.msg("对不起, 您输入的内容包含 [" + checkResult+ "] 等敏感词!",{icon: 0});
			return false;
		}

		if(!checkPhotos(form)){
			return false;
		}
		var addCommForm = $("#addComm" + '-' + prodCommId);
		var photoFile;
		var formData = new FormData(addCommForm[0]);
        var arr = [];
		formData.set("content",content);
		//console.log("content",formData.get("content"))
        //删除原有文件
        formData.delete("photos")
        for (let i = 0; i < 5 ; i++) {
            photoFile = addCommForm.find(".photoFile")[i].files[0];

            if (photoFile!=""&&photoFile!=undefined&&photoFile!=null){
                arr.push(photoFile);
               // console.log("photoFile:" + photoFile+"size:"+photoFile.size)
            }
        }
       // console.log("arrFile:"+arr);
        if(arr && arr.length){
            imgCompress.batchCompress(arr, function(compressImg){
             //   console.log("compressImg:"+compressImg)
                //将图片加入formData
                for (let i = 0; i < 5 ; i++) {
                    if (compressImg[i]!=""&&compressImg[i]!=undefined&&compressImg[i]!=null){
                    //    console.log("压缩后图片:"+compressImg[i]);
                        formData.append("photos", compressImg[i]);
                    }
                }
               // console.log("提交文件"+formData.getAll("photos"))
                submitCommForm(formData,addCommForm);
            });
        }else {
			submitCommForm(formData,addCommForm);
		}

	});
	
});



//敏感字检查
function checkSensitiveData(content){
	var url = contextPath + "/sensitiveWord/filter";
	$.ajax({
		url:url, 
		data: {"src": content},
		type:'post', 
		dataType : 'json', 
		async : false, //默认为true 异步   
		success:function(retData){
			result = retData;
		}
	});
	return result;
}
function pager(curPageNO){
	userCenter.loadPageByAjax("/p/prodcomment?curPageNO=" + curPageNO);
}

function checkPhotos(_form){
	var flag = true;
	var a = _form.find("div input[name=photos]");
	a.each(function(){
		var _this = $(this);
		var pic = _this.val();
		if(!flag) return false;
		if(pic!=""){
			//判断图片类型
			if(!/\.(jpg|jpeg|png|JPG|PNG)$/.test(pic)){
				layer.msg("图片类型必须是jpeg,jpg,png中的一种",{icon: 0});
				flag = false;
        $(".commit-comment-btn").css('pointer-events', 'auto');
        $(".commit-add-comment-btn").css('pointer-events', 'auto');
				return;
			}

			if(this.files && this.files.length){
				var fileData = this.files[0];  
				var max_size = 1024;// 1024k  
				var size = fileData.size;  
				if (size > max_size * 1024 * 5) {  
					layer.msg("图片大小不能超过5M",{icon: 0});  
					flag = false;
          $(".commit-comment-btn").css('pointer-events', 'auto');
          $(".commit-add-comment-btn").css('pointer-events', 'auto');
					return;
				}
			}
		}
	});
	return flag;
}
