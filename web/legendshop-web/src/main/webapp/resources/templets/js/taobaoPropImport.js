	 function IsPinYear(year) {//判断是否闰平年                      
	    return (0 == year % 4 && (year % 100 != 0 || year % 400 == 0));
	  }

	//日期选项 加载
	function dateOption(maxLength){
	    //console.debug("new Date().getFullYear():"+new Date().getFullYear());
	    //console.debug("new Date().getMonth():"+new Date().getMonth() + 1);
	    var commonYear = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];//平年月份的总天数
	    var LeapYear = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];//闰年月份的总天数 
	    var nowYear = new Date().getFullYear();
	    var nowMonth = new Date().getMonth() + 1;
	    var nowDay = new Date().getDate();
	    var nowHours = new Date().getHours();
	    var nowMinutes = new Date().getMinutes();
	    
	    var option="",monthDays,hoursOption="",minOption="";
	    
	    var dateStr1 ="";//格式 yyyy-mm-dd
	    var dateStr2="";//格式 yyyy年mm月dd日
	    var temp1,temp2,temp3,temp4;//作为临时记录
	    
	    var index = 0,x,y,z,h,m;
	    
	    for (x = nowYear; x < nowYear+2; x++) {
	    	dateStr1 = dateStr1 + x + "-";
	    	dateStr2 = dateStr2 + x + "年";
	    	temp1 = dateStr1;
	    	temp2 = dateStr2;
	    	
			if(IsPinYear(x)){ //true : 润年，false :平年
				monthDays = LeapYear;
			}else{
				monthDays = commonYear;
			}
			
			if(x==nowYear){y = nowMonth;}else{y = 1;}
	    	for(y;y<= 12; y++){
	    		dateStr1 = temp1 + y + "-";
	    		dateStr2 = temp2 + y + "月";
	    		temp3 = dateStr1;
	    		temp4 = dateStr2;
	    		
	    		if(y == nowMonth){z = nowDay;}else{z = 1;}
	    		for(z; z<=monthDays[y-1];z++){
	    			index = index + 1;
	    			if(index >= maxLength){break;}
	    			
	    			dateStr1 = temp3 + z;
	    			dateStr2 = temp4 + z + "日";
	    			
	    			option = "<option value='"+dateStr1+"'>"+dateStr2+"</option>";
	    			
	    			$("#year").append(option);
	    			
	    			dateStr1 = "";
	    			dateStr2 = "";
	    		}
	    		
	    		dateStr1 = "";
				dateStr2 = "";
	    	}
	    	
	    }
	    
	    //加载 小时 和 分钟 下啦
	    timeOption($("#year"));
	    
	}
	
	//加载小时 和分钟
	function timeOption(obj){
		var nowYear = new Date().getFullYear();
		var nowMonth = new Date().getMonth() + 1;
		var nowDay = new Date().getDate();
		var nowHours = new Date().getHours();
	    var nowMinutes = new Date().getMinutes();
		var h = 0,m = 0;
		
		var selectDay =$(obj).val();
		//console.debug("selectDay:"+selectDay);
		  
		var nowDay = nowYear + "-" + nowMonth + "-" + nowDay;
		//console.debug("nowDay:"+nowDay);
		
		//判断用户选择的是否是当天
		if(!isBlank(selectDay)){
			if(selectDay == nowDay){
				h = nowHours; m = nowMinutes;
			}
		}
		
		$("#hours").empty();
		for(h;h<=23;h++){
	    	$("#hours").append("<option value='"+h+"'>"+h+"</option>");
	    }
	    
		$("#minute").empty();
	    for(m;m<=59;m++){
	    	$("#minute").append("<option value='"+m+"'>"+m+"</option>");
	    }
	}
	
	//方法，判断是否为空
	function isBlank(_value){
		if(_value==null || _value=="" || _value==undefined){
			return true;
		}
		return false;
	}
	
	function getXhr(){
		var xmlhttp = null;
		if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		}else{// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		return xmlhttp;
	}