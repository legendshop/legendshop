function pager(curPageNO) {
    sendData(contextPath + "/s/applyBrand?curPageNO=" + curPageNO);
}

function brandConfigure(brandId) {
    sendData(contextPath + "/s/applyBrand/update/" + brandId);
}

function deleteById(brandId) {
    layer.confirm("确定删除 ?", {
 		 icon: 3
  	     ,btn: ['确定','取消'] //按钮
  	   },function () {
        jQuery.ajax({
            url: contextPath + "/s/applyBrand/delete/" + brandId,
            type: 'post',
            async: false, //默认为true 异步   
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus, errorThrown);
            },
            success: function (retData) {
                if ('OK' == retData) {
                    layer.msg('删除成功！', {icon: 1,time:700},function(){
                    	window.location.reload();
                    });
                    
                } else {
                	layer.msg('删除失败！', {icon: 2});
                }
            }
        });
    });
}

function brandDown(brandId) {
    layer.confirm("确定下线吗 ?", {
 		 icon: 3
  	     ,btn: ['确定','取消'] //按钮
  	   },function () {
        $.ajax({
            url: contextPath + "/s/brand/updateBrandStatus",
            data: {"brandId": brandId, "status": "0"},
            type: 'post',
            async: false, //默认为true 异步   
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus, errorThrown);
            },
            success: function (data) {
                if (data == "OK") {
                	layer.msg('下线成功！', {icon: 1,time:700},function(){
                		window.location.reload();
                	});
                    
                } else {
                    layer.msg('下线失败！', {icon: 2});
                }
            }
        });
    });
}

function brandUp(brandId) {

    layer.confirm("确定上线吗 ?", {
 		 icon: 3
  	     ,btn: ['确定','取消'] //按钮
  	   },function () {
        $.ajax({
            url: contextPath + "/s/brand/updateBrandStatus",
            data: {"brandId": brandId, "status": "-1"},
            type: 'post',
            async: false, //默认为true 异步   
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
                alert(textStatus, errorThrown);
            },
            success: function (data) {
                if (data == "OK") {
                	layer.msg('上线成功！', {icon: 1,time:700},function(){
                		window.location.reload();
                	});
                    
                } else {
                	layer.msg('上线失败！', {icon: 2});
                }
            }
        });
    });
}


function updateById(brandId, status) {
    $.ajax({
        url: contextPath + "/s/brand/updateBrandStatus",
        data: {"brandId": brandId, "status": status},
        type: 'post',
        async: false, //默认为true 异步   
        dataType: 'json',
        error: function (jqXHR, textStatus, errorThrown) {
            alert(textStatus, errorThrown);
        },
        success: function (data) {
            if (data == "OK") {
            	layer.msg('成功！', {icon: 1,time:700},function(){
            		window.location.reload();
            	});
                
            } else {
            	layer.msg('失败！', {icon: 2});
            }
        }
    });
}

function loadConfigure() {
    sendData(contextPath + "/s/applyBrand/load");
}


$(document).ready(function (e) {
    userCenter.changeSubTab("applyBrand"); //高亮菜单
});
