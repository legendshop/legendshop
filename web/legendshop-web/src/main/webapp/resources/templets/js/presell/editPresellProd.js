var isSubmitted = false;

jQuery.validator.addMethod("isNumber", function(value, element) {
	return this.optional(element) || /^\d+(\.\d{1,2})?$/.test(value);
}, "必须正整数或两位以内的小数");

jQuery.validator.addMethod("isValidMoney", function(value, element) {
	value = parseFloat(value);
	return this.optional(element) || value> 0;
}, "金额必须要大于0元");

jQuery.validator.addMethod("isValidMoney2", function(value, element) {
	value = parseFloat(value);
	var percent=/^(100|[1-9]\d|\d)(.\d{1,2})?$/;
	if(!percent.test(value)||value<=0.00||value>100.00){
		return false;
	}
	return true;
}, "定金百分比必须在0.01%-100%之间");

jQuery.validator.addMethod("compareDate", function (value, element, param) {
	var startDate = jQuery(param).val();
	startDate= startDate.replace(/-/g, '/');
	value = $(element).val().replace(/-/g, '/');

	if (startDate == null || startDate == '' || value == null || value == '') {//只要有一个为空就不比较
		return true;
	}
	var date1 = new Date(startDate);
	var date2 = new Date(value);
	return date1.getTime() < date2.getTime();

}, '开始时间不能大于结束时间');

//校验方案名是否已存在
$.validator.addMethod("checkVal",function(value,element){
	var config = true;
	if(value == schemeName){
		return config;
	}else{
		$.ajax({
			url: contextPath+"/s/presellProd/isSchemeNameExist",
			async:false,
			data:{"schemeName":value},
			dataType: "json",
			success:function(data){
				if(!data)
					config=false;
			}
		});
		return config;
	}
},"该方案名已被使用,请换一个!");


//发货输入框获取焦点时
var preDeliveryTimeStr = document.getElementById("preDeliveryTimeStr");
preDeliveryTimeStr.onfocus = function(event){
	if($("#preSaleStart").val() && $("#preSaleEnd").val()){
		var minDate = minDeliveryDate();//计算出发货时间能选择的最小时间
		//WdatePicker({dateFmt:"yyyy-MM-dd",minDate:minDate});
	}else{
		layer.msg('对不起,请先选择预售时间！', {icon: 0});
	}
}

$(document).ready(function() {
	//限制预售价格
	$(".prodList").on("blur",".prsellPrice",function () {
		if (this.value==null || this.value=="" || this.value<=0 || isNaN(this.value)){
			layer.alert("预售价格不能为空并且打大于0",{icon:2})
			$(this).css("background-color","pink");
			$(this).val("");
			return;
		}
		$(this).css("background-color","white");
	})

	//高亮菜单
	userCenter.changeSubTab("presellManage");
	laydate.render({
		elem: '#preSaleStart',
		calendar: true,
		theme: 'grid',
		trigger: 'click',
		min: '-1',
		type:'datetime',
		done: function(value, date, endDate){
			// end.config.min ={
			// 	year:date.year,
			// 	month:date.month-1,
			// 	date: date.date,
			// 	hours: date.hours,
			// 	minutes: date.minutes,
			// 	seconds : date.seconds+1
			// };
		}
	});

	var end=laydate.render({
		elem: '#preSaleEnd',
		calendar: true,
		trigger: 'click',
		theme: 'grid',
		min: '-1',
		type:'datetime',
		ready: function(date){
			if( isBlank($("#preSaleStart").val())){
				end.hint("请先选择开始时间");
			}
		}
	});
	laydate.render({
		elem: '#finalMStart',
		calendar: true,
		theme: 'grid',
		type:'datetime',
		trigger: 'click'
	});
	laydate.render({
		elem: '#finalMEnd',
		calendar: true,
		theme: 'grid',
		type:'datetime',
		trigger: 'click'
	});
	laydate.render({
		elem: '#preDeliveryTimeStr',
		calendar: true,
		theme: 'grid',
		type:'date',
		min: '-1',
		trigger: 'click'
	});


	//当预售价格或定金金额change时
	$("#prePrice,#preDepositPrice").change(function(){
		calculatePayPct();//计算定金占预售价格百分比
	});

	//当支付设置改变时
	$("#form1 input[name='payPctType']").change(function(){
		if($(this).val() == "1"){
			if($("#preSaleEnd").val()){
				$("#depositPay").show("normal",function(){
					//使与定金支付相关的input启用
					$("#payPct").removeAttr("disabled");
					$("#preDepositPrice").removeAttr("disabled");
					$("#finalMStart").removeAttr("disabled");
					$("#finalMEnd").removeAttr("disabled");
					//生成尾款支付时间
					generateFinalMTime();
				});
			}else{
				document.getElementById("payPctType-1").checked = false;
				document.getElementById("payPctType-0").checked = true;
				layer.msg('对不起,请先填写预售时间！', {icon: 0});
			}
		}else{
			$("#depositPay").hide("normal",function(){
				//使与定金支付相关的input禁用
				$("#payPct").attr("disabled","disabled");
				$("#preDepositPrice").attr("disabled","disabled");
				$("#finalMStart").attr("disabled","disabled");
				$("#finalMEnd").attr("disabled","disabled");
			});
		}

		//重置发货时间
		if($(this).val() == oldPayPacType){
			$("#preDeliveryTimeStr").val(oldDeliveryTime);
		}else{
			$("#preDeliveryTimeStr").val("");
		}
	});

	//预售结束时间获取焦点时
	/*$("#form1 input[name='preSaleEnd']").focus(function(){
		if( isBlank($("#preSaleStart").val())){
			layer.msg('对不起,请先填写预售时间！', {icon: 0});
		}
	});*/

	//预售结束时间更改时
	$("#form1 input[name='preSaleEnd']").change(function(){
		//重置发货时间
		$("#preDeliveryTimeStr").val("");
		//生成尾款时间
		var payPctType = $("input[name='payPctType']:checked").val();
		if(preSaleEnd && payPctType == "1"){
			generateFinalMTime();
		}
	});

	jQuery("#form1").validate({
		ignore: "hidden",
		rules: {
			schemeName: {
				required : true,
				maxlength : 50,
				checkVal : true
			},
			preSaleStart: {
				required: true

			},
			preSaleEnd: {
				required: true,
				compareDate: "#preSaleStart"
			},
			payPctType: {
				required: true
			},
			preDepositPrice: {
				required : true,
				isValidMoney2 : true
			},
			finalMStart:{
				required: true
			},
			finalMEnd:{
				required: true
			},
			preDeliveryTimeStr:{
				required: true
			}
		},
		messages: {
			schemeName: {
				required: "请输入方案名称!",
				maxlength : "方案名长度不能大于50个字符!"
			},
			preSaleStart: {
				required : "请输入预售开始时间!"
			},
			preSaleEnd : {
				required : "请输入预售结束时间!",
				compareDate: "结束日期必须大于开始日期!"
			},
			payPctType: {
				required: "请选择支付类型!"
			},
			preDepositPrice: {
				required: "请输入定金百分比!",
				isValidMoney2: "定金百分比必须在0.01%-100%之间!"
			},
			finalMStart:{
				required: "请输入尾款支付开始时间!"
			},
			finalMEnd:{
				required: "请输入尾款支付结束时间!"
			},
			preDeliveryTimeStr:{
				required: "请输入发货时间!"
			}
		},
		submitHandler : function(form){
			var rs=true;
			$(".prsellPrice").each(function () {
				if (this.value==null || this.value=="" || this.value<=0 || isNaN(this.value)){
					layer.alert("预售价格不能为空并且打大于0",{icon:2})
					rs = false;
					$(this).css("background-color","pink");
					$(this).val("");
				}
			})
			if (!rs){
				return;
			}

			$(".prodList").find(".prsellPrice").each(function(index,element){
				$(element).attr("name","skus["+index+"].prsellPrice");
				$(element).next("input[class='prsellProdId']").attr("name","skus["+index+"].prodId");
				$(element).next().next("input[class='prsellSkuId']").attr("name","skus["+index+"].skuId");
				$(element).rules("add",{required:true,messages: {required:"必填"} });
				$(element).rules("add",{number:true,messages: {number:"请输入数字"} });
				$(element).rules("add",{maxlength:8,messages: {maxlength:"最多输入八位数"} });
				$(element).rules("add",{range:[1,99999],messages: {range:"输入值在1-99999"} });
			});

			if(isBlank($("#prodId").val())){
				layer.alert('请选择参与预售的商品后再提交!', {icon: 0});
				return false;
			}

			var startTime=$("#preSaleStart").val();
			var endTime=$("#preSaleEnd").val();
			if(startTime > endTime){
				layer.msg('活动结束时间不能大于开始时间', {icon: 2});
				return false;
			}

			//校验预售时间与发货时间
			if(!validationPreDate() || !validationDeliveryDate() || !validationFinaPayDate()){
				return false;
			}
			$("#prePrice").val($("#prsellPrice").val());

			//防止表单重复提交
			if(!isSubmitted){
				form.submit();
				isSubmitted = true;
			}else{
				layer.alert('请不要重复提交表单!', {icon: 0});
			}
		}
	});

});

//计算定金占预售价格百分比
function calculatePayPct(){
	var prePrice = parseFloat($("#prePrice").val());//预售价格
	var preDepositPrice = parseFloat($("#preDepositPrice").val());//定金
	if(prePrice && preDepositPrice){
		var payPct = preDepositPrice/prePrice;
		$("#payPctSpan").text((payPct*100).toFixed(2));
	}
}

//生成尾款时间
function generateFinalMTime(){
	var preSaleEnd = $("#preSaleEnd").val();
	if(preSaleEnd){
		$("#finalMStart").val(preSaleEnd);//尾款开始时间等于预售结束时间
		var finalMStart = $("#finalMStart").val();
		var finalMEnd = new Date(finalMStart.replace(/-/g,"/"));
		//尾款支付结束时间等于从尾款支付开始时间的3天后的第一个24点
		finalMEnd.setDate(finalMEnd.getDate() + 4);
		var formatFinalMEnd = finalMEnd.getFullYear() + "-" + (finalMEnd.getMonth()+1) + "-" + finalMEnd.getDate() + " " + "00" + ":" + "00" + ":" + "00";
		$("#finalMEnd").val(formatFinalMEnd);
	}
}

//最小发货日期
function minDeliveryDate(){
	var payPctType = $("input[name='payPctType']:checked").val();
	var minDate = null;
	var date = null;
	if(payPctType == "0"){
		date = new Date($("#preSaleEnd").val().replace(/-/g,"/"));
	}else{
		date = new Date($("#finalMEnd").val().replace(/-/g,"/"));
	}
	date.setDate(date.getDate()+1);
	minDate = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
	return minDate;
}

//校验预售时间
function validationPreDate(){
	var currTime = new Date();
	var startTime = new Date($("#preSaleStart").val().replace(/-/g,"/"));
	var endTime = new Date($("#preSaleEnd").val().replace(/-/g,"/"));

	//开始时间不能小于等于当前时间
	if(($("#id").val()==""||$("#id").val()==null)&&startTime.getTime() <= currTime.getTime()){
		layer.alert('预售的开始时间不能小于当前时间!', {icon: 0});
		return false;
	}

	//开始时间与结束时间必须相隔24小时,24小时*3600秒*1000=24小时的毫秒数
	if((endTime.getTime() - startTime.getTime())<(24*3600*1000)){
		layer.alert('预售的开始时间与结束时间必须相隔24小时(包含)以上!', {icon: 0});
		return false;
	}

	return true;
}


//校验尾款支付时间
function validationFinaPayDate(){
	var activEndTime = new Date($("#preSaleEnd").val().replace(/-/g,"/"));
	var payStartTime = new Date($("#finalMStart").val().replace(/-/g,"/"));
	var payEndTime = new Date($("#finalMEnd").val().replace(/-/g,"/"));
	var payPctType = $("#payPctType-0");
	//支付尾款开始时间不能小于等于当前时间
	if(!payPctType.prop("checked")&&payStartTime.getTime() < activEndTime.getTime()){
		layer.alert('尾款支付时间不能小于活动结束的时间!', {icon: 0});
		return false;
	}

	//结束时间必须大于开始时间
	if(!payPctType.prop("checked")&&payEndTime.getTime() <= payStartTime.getTime()){
		layer.alert('尾款支付结束时间不能小于开始时间!', {icon: 0});
		return false;
	}

	return true;

}

//校验发货时间
function validationDeliveryDate(){
	var payPctType = $("input[name='payPctType']:checked").val();
	var preDeliveryTime = new Date($("#preDeliveryTimeStr").val().replace(/-/g,"/"));
	var preSaleEnd = new Date($("#preSaleEnd").val().replace(/-/g,"/"));
	var finalMEnd = new Date($("#finalMEnd").val().replace(/-/g,"/"));
	var errorMsg = null;
	//如果是全额支付,发货时间要晚于预售结束时间
	if(payPctType == "0"){
		errorMsg = "发货时间要晚于预售结束时间!";
	}else if(payPctType == "1"){//如果是全额支付,发货时间要晚于尾款结束时间
		errorMsg = "发货时间要晚于尾款结束时间!";
	}else{
		layer.alert('对不起,请不要非法操作!', {icon: 2});
		return false;
	}

	if((preDeliveryTime.getTime() <= preSaleEnd.getTime())&&payPctType == "0"){
		layer.alert(errorMsg,{icon: 0});
		return false;
	}
	if((preDeliveryTime.getTime() <= finalMEnd.getTime())&&payPctType == "1"){
		layer.alert(errorMsg,{icon: 0});
		return false;
	}

	var preSaleEndSecond = preSaleEnd.getTime();
	var preDeliverySecond = preDeliveryTime.getTime();
	var days = (preDeliverySecond - preSaleEndSecond)/(24*3600*1000);

	if(days > 10){
		layer.alert("发货时间不能晚于预售结束时间10天以上",{icon:0});
		return false;
	}

	return true;
}

//显示添加商品窗口
function showProdList(){

	layer.open({
		title :"选择商品",
		id: "showProdList",
		type: 2,
		resize: false,
		content: [contextPath+"/s/presellProd/addProdSkuLayout",'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		area: ['1040px', '512px']
	});
}


function addProdTo(prodId){
	if(prodId==null||prodId==""||prodId==undefined){
		layer.alert('请选选择商品', {icon: 2});
	}
	$("#productId").val(prodId);
	var url=contextPath+"/s/presellProd/loadProdSku/"+prodId;

	// 加载商品列表回显到页面
	$(".prodList").load(url, function(){
		layer.closeAll();
	});
}
// function addProdTo(prodId,skuId,skuName,skuPrice,cnProperties){
// 	$(".prod-table").css("display","");
// 	$("#prodId").val(prodId);
// 	$("#skuId").val(skuId);
// 	$("#skuName").text(skuName);
// 	$("#skuName").attr("href",contextPath + "/views/" + prodId);
// 	$("#skuPrice").text(skuPrice);
// 	if(isBlank(cnProperties)){
// 		$("#cnProperties").text("无");
// 	}else{
// 		$("#cnProperties").text(cnProperties);
// 	}
// }

function isBlank(value){
	return value == undefined || value == null || $.trim(value) == "";
}

function showProdlist(){
	layer.open({
		title :"选择商品",
		id: "showProdlist",
		type: 2,
		resize: false,
		content: [contextPath+"/s/group/groupProdLayout",'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		area: ['850px', '660px']
	});
}

//批量修改价格
$("#confim").click(function(){
	var _val = $("#batchPrice").val();

	if (isNaN(_val)||_val<=0) {
		layer.alert("预售价必须大于0",{icon:0})
		$("#batchPrice").val("");
		return;
	}

	if($(".sku").size()<=0){
		layer.alert("请选择商品",{icon:0});
		return;
	}
	if(isBlank(_val)){
		layer.alert("请选择输入预售价",{icon:0});
		return;
	}

	if($(".selectOne:checked").length<=0){
		layer.alert("请勾选商品",{icon:0});
		return;
	}

	$(".selectOne:checked").each(function(index,element){
		if($(element).is(':checked')){//选中，改变价格
			$(element).parents(".sku").find(".prsellPrice").val(_val);
		}
	})
	$("#batchPrice").val("");
});

//全选
$(".prodList").on("click",".selectAll",function(){
	if($(this).attr("checked")=="checked"){
		$(this).parent().parent().addClass("checkbox-wrapper-checked");
		$(".selectOne").each(function(){
			$(this).attr("checked",true);
			$(this).parent().parent().addClass("checkbox-wrapper-checked");
		});
	}else{
		$(".selectOne").each(function(){
			$(this).attr("checked",false);
			$(this).parent().parent().removeClass("checkbox-wrapper-checked");
		});
		$(this).parent().parent().removeClass("checkbox-wrapper-checked");
	}
});

// 单选
function selectOne(obj){
	if(!obj.checked){
		$(".selectAll").checked = obj.checked;
		$(obj).prop("checked",false);
		$(obj).parent().parent().removeClass("checkbox-wrapper-checked");
	}else{
		$(obj).prop("checked",true);
		$(obj).parent().parent().addClass("checkbox-wrapper-checked");
	}
	var flag = true;
	var arr = $(".selectOne");
	for(var i=0;i<arr.length;i++){
		if(!arr[i].checked){
			flag=false;
			break;
		}
	}
	if(flag){
		$(".selectAll").prop("checked",true);
		$(".selectAll").parent().parent().addClass("checkbox-wrapper-checked");
	}else{
		$(".selectAll").prop("checked",false);
		$(".selectAll").parent().parent().removeClass("checkbox-wrapper-checked");
	}
}

