$(document).ready(function() {
	
	userAddressPage(1);
		
	$("#path_list").delegate("#deliveryAddress ul dl","click",function(){
		 var _this=$(this);
		 var config=$(_this).hasClass("on");
		 if(!config){
			 layer.confirm("确认更换地址吗？置换地址过后，需要重新确认订单信息",{
	      		 icon: 3
	      	     ,btn: ['确定','取消'] //按钮
	      	   },function(){
					$("body").showLoading();
					 var addrId=$(_this).attr("addrId");
					 if(addrId==null){
						 $("body").hideLoading();
						 return;
					 }
					 $.ajax({
							url:contextPath+"/p/consigneeAddressAjdx",
							data: {"addrId":addrId},
							type:'post', 
							async : false, //默认为true 异步   
							error: function(jqXHR, textStatus, errorThrown) {
								$("body").hideLoading();
								return;
							},
							success:function(result){
								 $("body").hideLoading();
								 layer.msg("更改成功！",{icon:1,time:500},function(){
									 userAddressPage(1);
									 reloadOrder();
								 });
								 
							}
					});
			 }); 	
		 }
	});
	
/*	jQuery(document).delegate("#deliveryAddress ul dl","mouseover",function(){
		 var _obj=$(this).parent();
		 
	});
	
	jQuery(document).delegate("#deliveryAddress ul dl","mouseout",function(){
	});
	*/
	 
});

function reloadOrder(){
/*	$("#buyNow").val(isBuyNow);
	$("#prodId").val(prodId);
	$("#skuId").val(skuId);
	$("#count").val(count);*/
	$("#orderFormReload").submit();
}



function addNewAddress(){
	var receiver= $("#deliveryAddress ul dl").length;
	if(receiver>=20){ 
		layer.alert("收货地址不能超过20个!",{icon:2});
		return;
	}
	layer.open({
		title :"添加收货信息",
		  type: 2, 
		  content: [contextPath + '/p/addNewAddress','no'],
		  area: ['530px', '550px']
		}); 
}

//编辑常用地址信息
function editAddress(addrId,e){
	//取消冒泡
    e.cancelBubble = true;
    layer.open({
		title :"修改收货信息",
		  type: 2, 
		  content: [contextPath + '/p/addNewAddress?addrId='+addrId,'no'],
		  area: ['530px', '550px']
		}); 
}

//地址分页
function userAddressPage(curPageNO){
    $.ajax({
		url:contextPath+"/p/userAddressPage",
		data:{"curPageNO":curPageNO},
		type:'post', 
		async : false,  
		success:function(result){
			$("#path_list").html(result);
			$("#path_list").initAddrSlide();
		}
	});
}


//删除常用地址信息
function delAddress(addrId,e){
	e.cancelBubble = true;
	layer.confirm("确定删除该常用地址？",{
 		 icon: 3
  	     ,btn: ['确定','取消'] //按钮
  	   },function(index){
		$("body").showLoading();
		$.ajax({
			url:contextPath+"/p/delUserAddress",
			data: {"addrId": addrId},
			type:'post', 
			dataType : 'json', 
			async : false, //默认为true 异步   
			error: function(jqXHR, textStatus, errorThrown) {
				 $("body").hideLoading();
				 return;
			},
			success:function(result){
				//刷新地址列表
				userAddressPage(1);
				$("body").hideLoading();
				layer.close(index);
			   }
			});
 	 }); 	
}

//设置默认用户地址
function setDefaultAddr(addrId){
	layer.confirm("是否将地址设定成默认地址？",{
 		 icon: 3
  	     ,btn: ['确定','取消'] //按钮
  	   },function(){
		   $("body").showLoading();
		   $.ajax({
				url:contextPath+"/p/consigneeAddressAjdx",
				data: {"addrId":addrId},
				type:'post', 
				async : false, //默认为true 异步   
				error: function(jqXHR, textStatus, errorThrown) {
					$("body").hideLoading();
					return;
				},
				success:function(result){
					 reloadOrder();
				}
			});
 	 }); 	
}
