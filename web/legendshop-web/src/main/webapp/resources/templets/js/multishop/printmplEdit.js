var bgimage = '${printTmpl.bgimage}';

jQuery.validator.addMethod("isImg", function(value, element) {
	var fileName = $("#bgimageFile").val();
	var strFileName = fileName.substring(fileName.lastIndexOf("\\")+1); 
	if (!/\.(JPEG|BMP|GIF|JPG|PNG)$/.test(strFileName.toUpperCase())) {
		return false;
	}
	return true;
}, "仅支持JPG、GIF、PNG、JPEG、BMP格式，请重新选择！");

$(document).ready(function() {
	userCenter.changeSubTab("transportPrintTmpl"); //高亮菜单
	//校验是否是修改,如果是修改则忽略掉校验图片
	if(!isBlank(bgimage)){
		$("#bgimageFile").addClass("ignore");
	}
	
	jQuery("#form1").validate({
		ignore: ".ignore",
		rules: {
			prtTmplTitle: {
				required : true,
				maxlength : 10
			},
			prtTmplWidth: {
				required : true,
				digits: true 

			},
			prtTmplHeight: {
				required : true,
				digits: true 
			},
			bgimageFile:{
				required : true,
				isImg:true
			}
		},
		messages: {
			prtTmplTitle: {
				required : "模板名称不能为空",
				maxlength : "模板名称最多10个字" 
			},
			prtTmplWidth: {
				required : "宽度不能为空",
				digits: "宽度必须为数字"
			},
			prtTmplHeight: {
				required : "高度不能为空",
				digits: "高度必须为数字"
			},
			bgimageFile:{
				required : "请上传背景",
				isImg:"仅支持JPG、GIF、PNG、JPEG、BMP格式，请重新选择！"
			}
		}
	});
	

	/**判断是否为空**/
	function isBlank(_value){
		if(_value==null || _value=="" || _value==undefined){
			return true;
		}
		return false;
	}
});