var isSubmitted = false;//表单是否已提交

//页面加载的时候
$(function(){
	//切换申请类型
	var oldAapplyType = 1;
	$("#applyTypeSelect li").click(function(){
		var value = Number($(this).val());//获取用户选择的操作类型
		//如果选择的类型没有变
		if(oldAapplyType === value){
			return;
		}
		
		//样式切换
		$("#applyTypeSelect li").removeClass("on");
		$(this).addClass("on");
		
		var fileLi1 = $("#refundForm").find("#fileLi1");
		if(value === 1){//仅退款
			$("#refundReturnBox").hide();
			$("#refundBox").show();
		}else{//退款及退货
			$("#refundBox").hide();
			$("#refundReturnBox").show();
			fileLi1 = $("#refundReturnForm").find("#fileLi1");
		}

		if(fileLi1.find("#addBtn").length <= 0){
			var addBtn = $("<input id='addBtn' type='button' value='添加' onclick='addFile(this)'/>");
			fileLi1.append(addBtn);
		}
		fileLi1.find("#photoFile1").val("");
		fileLi1.next().remove();
		
		oldAapplyType = value;
		fileCount = 1;
	});
	
	//仅退款表单数据校验
	$("#refundForm").validate({
	 		ignore: ".ignore",
/*		    errorPlacement: function(error, element) {
		    	element.parent().find("em").html("");
				error.appendTo(element.parent().find("em"));
			},*/
	        rules: {
	        	orderId: {
		            required: true
		        },
		        orderItemId: {
		        	required: true
		        },
		        buyerMessage: {
		            required: true
		        },
		        refundAmount: {
		            required: true,
		            isNumber: true,
		            isValidMoney: true,
//		            checkRefundMoney : true
		        },
		        reasonInfo: {
		            required: true,
		            minlength: 5,
		            maxlength: 300
		        },
		        photoFile1: {
		            required: true
		        }
	     },
	     messages: {
	        	orderId: {
		            required: "对不起,没有订单Id!"
		        },
		        orderItemId: {
		        	required: "对不起,没有订单项ID!"
		        },
		        buyerMessage: {
		            required: "请选择退款原因!"
		        },
		        refundAmount: {
		            required: "请输入退款金额!",
		            isNumber: "请输入正确的金额!",
		            isValidMoney: "对不起,金额必须大于0!",
//		            checkRefundMoney : "对不起,退款金额不能大于￥" + maxRefundMoney + " !"
		        },
		        reasonInfo: {
		            required: "请输入退款金额!",
		            minlength: "退款说明不能少于5个字符",
		            maxlength: "退款说明不能超过300个字符!"
		        },
		        photoFile1: {
		            required: "至少添加一个退款凭证图片!"
		        }
	     },
	     submitHandler : function(form){
	     	if(!isSubmitted){
	     		form.submit();
	     		isSubmitted = true;
	     	}else{
	     		layer.alert("对不起,请不要重复提交表单!",{icon: 0});
	     	}
	     } 
	 });
	
	//退款且退货表单数据校验
	$("#refundReturnForm").validate({
		ignore: ".ignore",
		rules: {
			orderId : {
				required: true
			},
			orderItemId: {
				required: true
			},
			buyerMessage: {
				required: true
			},
			refundAmount: {
				required: true,
				isNumber: true,
				isValidMoney: true,
//				checkRefundMoney : true
			},
			goodsNum: {
				required: true,
				isNumber: true,
				min: 1,
//				checkGoodsCount: true
			},
			reasonInfo: {
				required: true,
				minlength: 5,
				maxlength: 300
			},
			photoFile1: {
				required: true
			}
		},
		messages: {
			orderId: {
				required: "对不起,没有订单Id!"
			},
			orderItemId: {
				required: true
			},
			buyerMessage: {
				required: "请选择退款/退货原因!"
			},
			refundAmount: {
				required: "请输入退款金额!",
				isNumber: "请输入正确的金额!",
				isValidMoney: "对不起,金额必须大于0!",
//				checkRefundMoney : "对不起,退款金额不能大于￥" + maxRefundMoney + " !"
			},
			goodsNum: {
				required: "请输入退货数量!",
				isNumber: "请输入正确的数量!",
				min: "对不起,退货数量必须1件或一件以上!",
//				checkGoodsCount: "对不起,退货数量不能大于 " + maxGoodsCount + " !"
			},
			reasonInfo: {
				required: "请输入退款退货说明!",
				minlength: "退款退货说明不能少于5个字符",
				maxlength: "退款退货说明不能超过300个字符!"
			},
			photoFile1: {
				required: "至少添加一个退款凭证图片!"
			}
		},
		submitHandler : function(form){
			if(!isSubmitted){
				form.submit();
				isSubmitted = true;
			}else{
				layer.alert("对不起,请不要重复提交表单!",{icon: 0});
			}
		} 
	});
	
});

jQuery.validator.addMethod("checkRefundMoney", function(value, element) {       
	return Number(value) <= maxRefundMoney;
});  

jQuery.validator.addMethod("checkGoodsCount", function(value, element) {       
	return Number(value) <= maxGoodsCount;
});  

jQuery.validator.addMethod("isNumber", function(value, element) {       
    return this.optional(element) || /^[-\+]?\d+$/.test(value) || /^[-\+]?\d+(\.\d+)?$/.test(value);       
}, "必须整数或小数");  

jQuery.validator.addMethod("isValidMoney", function(value, element) { 
    value = parseFloat(value);      
    return this.optional(element) || value> 0;       
}, "金额必须要大于0元"); 
