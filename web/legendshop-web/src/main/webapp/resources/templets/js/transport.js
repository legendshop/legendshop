var mail_batch=0;
var express_batch=0;
var ems_batch=0;

jQuery(document).ready(function(){


	var mail_city_count= $("#mail_trans_city_info table tbody tr").length;
	var express_city_count=  $("#express_trans_city_info table tbody tr").length;
	var ems_city_count= $("#ems_trans_city_info table tbody tr").length;
	var transType = $("#transType").val();
	tabTransType(transType);



   //userCenter.changeSubTab("transport");
	jQuery.validator.addMethod("checkTrans", function(value, element){
		var length = $("input[trans='trans']:checked").length;
		return length!=0;
	},"运送方式不能为空");

	//自定义validate验证输入的数字小数点位数不能大于两位
    jQuery.validator.addMethod("minNumber",function(value, element){
        var returnVal = true;
        inputZ=value;
        var ArrMen= inputZ.split(".");    //截取字符串
        if(ArrMen.length==2){
            if(ArrMen[1].length>2){    //判断小数点后面的字符串长度
                returnVal = false;
                return false;
            }
        }
        return returnVal;
    },"小数点后最多为两位");

	jQuery("#form1").validate({
		rules: {
    	  transName:{required:true, maxlength:20},
    	  isRegionalSales:{required:true},
    	  transEms:{checkTrans:true},
    	  "mailList[0].transWeight":{required:true,digits:true,min:0,max:999},
    	  "mailList[0].transFee":{required:true,number:true,min:0,max:999,minNumber:true},
    	  "mailList[0].transAddWeight":{required:true,digits:true,min:0,max:999},
    	  "mailList[0].transAddFee":{required:true,number:true,min:0,max:999,minNumber:true},
    	  "expressList[0].transWeight":{required:true,digits:true,min:0,max:999},
    	  "expressList[0].transFee":{required:true,number:true,min:0,max:999,minNumber:true},
    	  "expressList[0].transAddWeight":{required:true,digits:true,min:0,max:999},
    	  "expressList[0].transAddFee":{required:true,number:true,min:0,max:999,minNumber:true},
    	  "emsList[0].transWeight":{required:true,digits:true,min:0,max:999},
    	  "emsList[0].transFee":{required:true,number:true,min:0,max:999,minNumber:true},
    	  "emsList[0].transAddWeight":{required:true,digits:true,min:0,max:999},
    	  "emsList[0].transAddFee":{required:true,number:true,min:0,max:999,minNumber:true},
        transExpress:{required:true}
		},
		errorPlacement: function(error, element){
				if(element.is(":radio")){
					error.appendTo($(".salesEroor"));
				}else if(element.is(":checkbox")){
					error.appendTo($(".transError"));
				}else {
					error.insertAfter(element);
				}
		},
	   messages: {
		   transName:{required:"模板名称不能为空", maxlength:"模板名称长度最大为20"},
		   isRegionalSales:{required:"区域限售不能为空"},
		   "mailList[0].transWeight":{required:"不能为空",digits:"只能为整数",min:"请输入0-999的数字",max:"请输入0-999的数字"},
		   "mailList[0].transFee":{required:"不能为空",number:"请输数字",min:"请输入0-999的数字",max:"请输入0-999的数字"},
		   "mailList[0].transAddWeight":{required:"不能为空",digits:"只能为整数",min:"请输入0-999的数字",max:"请输入0-999的数字"},
		   "mailList[0].transAddFee":{required:"不能为空",number:"请输数字",min:"请输入0-999的数字",max:"请输入0-999的数字"},
		   "expressList[0].transWeight":{required:"不能为空",digits:"只能为整数",min:"请输入0-999的数字",max:"请输入0-999的数字"},
		   "expressList[0].transFee":{required:"不能为空",number:"请输数字",min:"请输入0-999的数字",max:"请输入0-999的数字"},
		   "expressList[0].transAddWeight":{required:"不能为空",digits:"只能为整数",min:"请输入0-999的数字",max:"请输入0-999的数字"},
		   "expressList[0].transAddFee":{required:"不能为空",number:"请输数字",min:"请输入0-999的数字",max:"请输入0-999的数字"},
		   "emsList[0].transWeight":{required:"不能为空",digits:"只能为整数",min:"请输入0-999的数字",max:"请输入0-999的数字"},
		   "emsList[0].transFee":{required:"不能为空",number:"请输数字",min:"请输入0-999的数字",max:"请输入0-999的数字"},
		   "emsList[0].transAddWeight":{required:"不能为空",digits:"只能为整数",min:"请输入0-999的数字",max:"请输入0-999的数字"},
		   "emsList[0].transAddFee":{required:"不能为空",number:"请输数字",min:"请输入0-999的数字",max:"请输入0-999的数字"},
       transExpress:{required:"请勾选运送方式并设置"}
		},
    submitHandler : function(form){
		  return checkCityIds();
    }
 });

	if(mail_city_count !=0){
		//$("#transMail").attr("checked","checked");
		//$("#transMail_info").show();

		for(var i=1;i<=mail_city_count;i++){
			$("#mail_trans_weight"+i).rules("add",{ required:true,digits:true,min:0,max:999, messages: {required:"不能为空",digits:"请输整数",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
			$("#mail_trans_fee"+i).rules("add",{ required:true,number:true,min:0,max:999, messages: {required:"不能为空",number:"请输数字",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
			$("#mail_trans_add_weight"+i).rules("add",{ required:true,digits:true,min:0,max:999, messages: {required:"不能为空",digits:"请输整数",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
			$("#mail_trans_add_fee"+i).rules("add",{ required:true,number:true,min:0,max:999, messages: {required:"不能为空",number:"请输数字",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
		}
	}
	if(express_city_count !=0){
		//$("#transExpress").attr("checked","checked");
		//$("#transExpress_info").show();

		for(var x=1;x<=express_city_count;x++){
			 $("#express_trans_weight"+x).rules("add",{ required:true,digits:true,min:0,max:999, messages: {required:"不能为空",digits:"请输整数",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
			 $("#express_trans_fee"+x).rules("add",{ required:true,number:true,min:0,max:999, messages: {required:"不能为空",number:"请输数字",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
			 $("#express_trans_add_weight"+x).rules("add",{ required:true,digits:true,min:0,max:999, messages: {required:"不能为空",digits:"请输整数",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
			 $("#express_trans_add_fee"+x).rules("add",{ required:true,number:true,min:0,max:999, messages: {required:"不能为空",number:"请输数字",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
		}
	}
	if(ems_city_count !=0){
		//$("#transEms").attr("checked","checked");
		//$("#transEms_info").show();

		for(var y=1;y<=ems_city_count;y++){
			$("#ems_trans_weight"+y).rules("add",{ required:true,digits:true,min:0,max:999, messages: {required:"不能为空",digits:"请输整数",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
			$("#ems_trans_fee"+y).rules("add",{ required:true,number:true,min:0,max:999, messages: {required:"不能为空",number:"请输数字",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
			$("#ems_trans_add_weight"+y).rules("add",{ required:true,digits:true,min:0,max:999, messages: {required:"不能为空",digits:"请输整数",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
			$("#ems_trans_add_fee"+y).rules("add",{ required:true,number:true,min:0,max:999, messages: {required:"不能为空",number:"请输数字",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
		}
	}

	 //切换计价方式
	 jQuery(":radio[name=transType]").click(function(){
		 var transType=jQuery(this).val();
	     layer.confirm('正在切换计价方式，确定继续么？', {
	    	 icon: 3,btn: ['确定','取消'] //按钮
	     }, function(index){
	    	 tabTransType(transType);
	    	 checkType = transType;
	    	 layer.close(index);
	     }, function(index){
	    	 if("1"==checkType){
           eachRadio(0)
	    	 }else if("2"==checkType){
           eachRadio(1)
	    	 }else if("3"==checkType){
           eachRadio(2)
	    	 }else{
           eachRadio(0)
	    		 tabTransType(1);
	    	 }
	   		layer.close(index);
	     });
function eachRadio(num) {
  $("input[name='transType']").each(function (index,doc) {
    var parent = $(doc).parents(".radio-wrapper");
      if (index!=num){
        $(doc).attr("checked",false);
        parent.removeClass("radio-wrapper-checked")
      }else {
        $(doc).attr("checked",true);
        parent.addClass("radio-wrapper-checked");
      }
  })
}

	 });

});

//切换计量方式，所对应的动作。
function tabTransType(transType){
	if(transType=="3"){
		$("#mailTransWeight").next().html("m³内，");
    	$("#mailTransAddWeight").next().html("m³，增加运费");
    	$("#mailTransWeightType").html("首体积(m³)");
    	$("#mailTransAddWeightType").html("续体积(m³)");

    	$("#expressTransWeight").next().html("m³内，");
    	$("#expressTransAddWeight").next().html("m³，增加运费");
    	$("#expressTransWeightType").html("首体积(m³)");
    	$("#expressTransAddWeightType").html("续体积(m³)");

    	$("#emsTransWeight").next().html("m³内，");
    	$("#emsTransAddWeight").next().html("m³，增加运费");
    	$("#emsTransWeightType").html("首体积(m³)");
    	$("#emsTransAddWeightType").html("续体积(m³)");
 }else if(transType=="2"){
	 	$("#mailTransWeight").next().html("kg内，");
    	$("#mailTransAddWeight").next().html("kg，增加运费");
    	$("#mailTransWeightType").html("首重(kg)");
    	$("#mailTransAddWeightType").html("续重(kg)");

    	$("#expressTransWeight").next().html("kg内，");
    	$("#expressTransAddWeight").next().html("kg，增加运费");
    	$("#expressTransWeightType").html("首重(kg)");
    	$("#expressTransAddWeightType").html("续重(kg)");

    	$("#emsTransWeight").next().html("kg内，");
    	$("#emsTransAddWeight").next().html("kg，增加运费");
    	$("#emsTransWeightType").html("首重(kg)");
    	$("#emsTransAddWeightType").html("续重(kg)");
 }else{
		$("#mailTransWeight").next().html("件内，");
	 	$("#mailTransAddWeight").next().html("件，增加运费");
	 	$("#mailTransWeightType").html("首件(件)");
	 	$("#mailTransAddWeightType").html("续件(件)");

	 	$("#expressTransWeight").next().html("件内，");
	 	$("#expressTransAddWeight").next().html("件，增加运费");
	 	$("#expressTransWeightType").html("首件(件)");
	 	$("#expressTransAddWeightType").html("续件(件)");

	 	$("#emsTransWeight").next().html("件内，");
	 	$("#emsTransAddWeight").next().html("件，增加运费");
	 	$("#emsTransWeightType").html("首件(件)");
	 	$("#emsTransAddWeightType").html("续件(件)");
 }
}


function loadTransportList(){
	  sendData(paramData.contextPath+"/s/transport");
}

//运送方式 勾选事件绑定
jQuery("input[type=checkbox].freightMode").live("click",function(){
    var id = $(this).attr("id");
	 if($(this).attr("checked") == "checked"){
	   $("#"+id+"_info").show();
	 }else{
	   $("#"+id+"_info").hide();
	 }
  });

function trans_city(id){
	  var the_id="";
	  var s="";
	  if(id=="express"){
		var express_tr_count =$("#express_trans_city_info table tbody tr").length;
	    express_city_count=express_tr_count+1;
		the_id="express"+express_city_count;
		jQuery("#express_city_count").val(express_city_count);
      if(express_batch==1){
        s='<tr index="'+express_city_count+'"><td><span class="width2"><i><input style="width:30px;"  id="trans_ck_'+express_city_count+'" name="trans_ck_'+express_city_count+'" type="checkbox" value="" /></i><input id="express_city_ids'+express_city_count+'" name="expressList['+express_city_count+'].cityIdList" type="hidden" value="" /><input id="express_city_names'+express_city_count+'" name="express_city_names'+express_city_count+'" type="hidden" value="" /><input id="select_id'+express_city_count+'" value="0" type="hidden" /><a id="edit_trans_city'+express_city_count+'"  href="javascript:void(0);" onclick="edit_trans_city(this);" trans_city_type="'+id+'">编辑</a></span><span class="width1" id="'+the_id+'">   未添加地区</span></td><td><input type="text" value="1" class="in" id="express_trans_weight'+express_city_count+'" name="expressList['+express_city_count+'].transWeight" /></td><td><input type="text" value="1" class="in" id="express_trans_fee'+express_city_count+'" name="expressList['+express_city_count+'].transFee" /></td><td><input type="text" value="1" class="in" id="express_trans_add_weight'+express_city_count+'" name="expressList['+express_city_count+'].transAddWeight" /></td><td><input type="text" value="1" class="in" id="express_trans_add_fee'+express_city_count+'" name="expressList['+express_city_count+'].transAddFee" /></td><td><span class="width3"><a href="javascript:void(0);" onclick="isDelete(this)">删除</a></span></td></tr>';
      }else{
        s='<tr index="'+express_city_count+'"><td><span class="width2"><i><input  id="trans_ck_'+express_city_count+'" name="trans_ck_'+express_city_count+'" type="checkbox" value="" style="width:30px; display:none;" /></i><input id="express_city_ids'+express_city_count+'" name="expressList['+express_city_count+'].cityIdList" type="hidden" value="" /><input id="express_city_names'+express_city_count+'" name="express_city_names'+express_city_count+'" type="hidden" value="" /><input id="select_id'+express_city_count+'" value="0" type="hidden" /><a id="edit_trans_city'+express_city_count+'"  href="javascript:void(0);" onclick="edit_trans_city(this);" trans_city_type="'+id+'">编辑</a></span><span class="width1" id="'+the_id+'">   未添加地区</span></td><td><input type="text" value="1" class="in" id="express_trans_weight'+express_city_count+'" name="expressList['+express_city_count+'].transWeight" /></td><td><input type="text" value="1" class="in" id="express_trans_fee'+express_city_count+'" name="expressList['+express_city_count+'].transFee" /></td><td><input type="text" value="1" class="in" id="express_trans_add_weight'+express_city_count+'" name="expressList['+express_city_count+'].transAddWeight" /></td><td><input type="text" value="1" class="in" id="express_trans_add_fee'+express_city_count+'" name="expressList['+express_city_count+'].transAddFee" /></td><td><span class="width3"><a href="javascript:void(0);" onclick="isDelete(this)">删除</a></span></td></tr>';
      }
	  jQuery("#"+id+"_trans_city_info table tbody").append(s);
	  jQuery("#"+id+"_trans_city_info").show();

	//动态添加验证
	  $("#express_trans_weight"+express_city_count).rules("add",{ required:true,digits:true,min:0,max:999, messages: {required:"不能为空",digits:"请输整数",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
	  $("#express_trans_fee"+express_city_count).rules("add",{ required:true,number:true,min:0,max:999, messages: {required:"不能为空",number:"请输数字",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
	  $("#express_trans_add_weight"+express_city_count).rules("add",{ required:true,digits:true,min:0,max:999, messages: {required:"不能为空",digits:"请输整数",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
	  $("#express_trans_add_fee"+express_city_count).rules("add",{ required:true,number:true,min:0,max:999, messages: {required:"不能为空",number:"请输数字",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
	  }
	  if(id=="ems"){
		var ems_tr_count = $("#ems_trans_city_info table tbody tr").length;
	    ems_city_count=ems_tr_count+1;
		the_id="ems"+ems_city_count;
		jQuery("#ems_city_count").val(ems_city_count);
	    if(ems_batch==1){
	       s='<tr index="'+ems_city_count+'"><td><span class="width2"><i><input  style="width:30px;"  id="trans_ck_'+ems_city_count+'" name="trans_ck_'+ems_city_count+'" type="checkbox" value="" /></i><input id="ems_city_ids'+ems_city_count+'" name="emsList['+ems_city_count+'].cityIdList" type="hidden" value="" /><input id="ems_city_names'+ems_city_count+'" name="ems_city_names'+ems_city_count+'" type="hidden" value="" /><a  href="javascript:void(0);" onclick="edit_trans_city(this);" trans_city_type="'+id+'">编辑</a></span><span class="width1" id="'+the_id+'">   未添加地区</span></td><td><input type="text" value="1" class="in" id="ems_trans_weight'+ems_city_count+'" name="emsList['+ems_city_count+'].transWeight" /></td><td><input type="text" value="1" class="in" id="ems_trans_fee'+ems_city_count+'" name="emsList['+ems_city_count+'].transFee" /></td><td><input type="text" value="1" class="in" id="ems_trans_add_weight'+ems_city_count+'" name="emsList['+ems_city_count+'].transAddWeight" /></td><td><input type="text" value="1" class="in" id="ems_trans_add_fee'+ems_city_count+'" name="emsList['+ems_city_count+'].transAddFee" /></td><td><span class="width3"><a href="javascript:void(0);" onclick="isDelete(this)">删除</a></span></td></tr>';
		}else{
	      s='<tr index="'+ems_city_count+'"><td><span class="width2"><i><input  id="trans_ck_'+ems_city_count+'" name="trans_ck_'+ems_city_count+'" type="checkbox" value="" style="width:30px; display:none;" /></i><input id="ems_city_ids'+ems_city_count+'" name="emsList['+ems_city_count+'].cityIdList" type="hidden" value="" /><input id="ems_city_names'+ems_city_count+'" name="ems_city_names'+ems_city_count+'" type="hidden" value="" /><a  href="javascript:void(0);" onclick="edit_trans_city(this);" trans_city_type="'+id+'">编辑</a></span><span class="width1" id="'+the_id+'">   未添加地区</span></td><td><input type="text" value="1" class="in" id="ems_trans_weight'+ems_city_count+'" name="emsList['+ems_city_count+'].transWeight" /></td><td><input type="text" value="1" class="in" id="ems_trans_fee'+ems_city_count+'" name="emsList['+ems_city_count+'].transFee" /></td><td><input type="text" value="1" class="in" id="ems_trans_add_weight'+ems_city_count+'" name="emsList['+ems_city_count+'].transAddWeight" /></td><td><input type="text" value="1" class="in" id="ems_trans_add_fee'+ems_city_count+'" name="emsList['+ems_city_count+'].transAddFee" /></td><td><span class="width3"><a href="javascript:void(0);" onclick="isDelete(this)">删除</a></span></td></tr>';
	   }
	  jQuery("#"+id+"_trans_city_info table tbody").append(s);
	  jQuery("#"+id+"_trans_city_info").show();

	//动态添加验证
	  $("#ems_trans_weight"+ems_city_count).rules("add",{ required:true,digits:true,min:0,max:999, messages: {required:"不能为空",digits:"请输整数",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
	  $("#ems_trans_fee"+ems_city_count).rules("add",{ required:true,number:true,min:0,max:999, messages: {required:"不能为空",number:"请输数字",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
	  $("#ems_trans_add_weight"+ems_city_count).rules("add",{ required:true,digits:true,min:0,max:999, messages: {required:"不能为空",digits:"请输整数",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
	  $("#ems_trans_add_fee"+ems_city_count).rules("add",{ required:true,number:true,min:0,max:999, messages: {required:"不能为空",number:"请输数字",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
	  }
	  if(id=="mail"){
		var mail_tr_count = $("#mail_trans_city_info table tbody tr").length;
	    mail_city_count= mail_tr_count+1;
		the_id="mail"+mail_city_count;
		jQuery("#mail_city_count").val(mail_city_count);
		if(mail_batch==1){
	       s='<tr index="'+mail_city_count+'"><td><span class="width2"><i><input  style="width:30px;"  id="trans_ck_'+mail_city_count+'" name="trans_ck_'+mail_city_count+'" type="checkbox" value="" /></i><input id="mail_city_ids'+mail_city_count+'" name="mailList['+mail_city_count+'].cityIdList" type="hidden" value="" /><input id="mail_city_names'+mail_city_count+'" name="mail_city_names'+mail_city_count+'" type="hidden" value="" /><a  href="javascript:void(0);" onclick="edit_trans_city(this);" trans_city_type="'+id+'">编辑</a></span><span class="width1" id="'+the_id+'">   未添加地区</span></td><td><input type="text" value="1" class="in" id="mail_trans_weight'+mail_city_count+'" name="mailList['+mail_city_count+'].transWeight" /></td><td><input type="text" value="1" class="in" id="mail_trans_fee'+mail_city_count+'" name="mailList['+mail_city_count+'].transFee" /></td><td><input type="text" value="1" class="in" id="mail_trans_add_weight'+mail_city_count+'" name="mailList['+mail_city_count+'].transAddWeight" /></td><td><input type="text" value="1" class="in" id="mail_trans_add_fee'+mail_city_count+'" name="mailList['+mail_city_count+'].transAddFee" /></td><td><span class="width3"><a href="javascript:void(0);" onclick="isDelete(this)">删除</a></span></td></tr>';
		}else{
	      s='<tr index="'+mail_city_count+'"><td><span class="width2"><i><input  id="trans_ck_'+mail_city_count+'" name="trans_ck_'+mail_city_count+'" type="checkbox" value="" style="width:30px; display:none;" /></i><input id="mail_city_ids'+mail_city_count+'" name="mailList['+mail_city_count+'].cityIdList" type="hidden" value="" /><input id="mail_city_names'+mail_city_count+'" name="mail_city_names'+mail_city_count+'" type="hidden" value="" /><a  href="javascript:void(0);" onclick="edit_trans_city(this);" trans_city_type="'+id+'">编辑</a></span><span class="width1" id="'+the_id+'">   未添加地区</span></td><td><input type="text" value="1" class="in" id="mail_trans_weight'+mail_city_count+'" name="mailList['+mail_city_count+'].transWeight" /></td><td><input type="text" value="1" class="in" id="mail_trans_fee'+mail_city_count+'" name="mailList['+mail_city_count+'].transFee" /></td><td><input type="text" value="1" class="in" id="mail_trans_add_weight'+mail_city_count+'" name="mailList['+mail_city_count+'].transAddWeight" /></td><td><input type="text" value="1" class="in" id="mail_trans_add_fee'+mail_city_count+'" name="mailList['+mail_city_count+'].transAddFee" /></td><td><span class="width3"><a href="javascript:void(0);" onclick="isDelete(this)">删除</a></span></td></tr>';
		}
	  jQuery("#"+id+"_trans_city_info table tbody").append(s);
	  jQuery("#"+id+"_trans_city_info").show();

	  //动态添加验证
	  $("#mail_trans_weight"+mail_city_count).rules("add",{ required:true,digits:true,min:0,max:999, messages: {required:"不能为空",digits:"请输整数",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
	  $("#mail_trans_fee"+mail_city_count).rules("add",{ required:true,number:true,min:0,max:999, messages: {required:"不能为空",number:"请输数字",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
	  $("#mail_trans_add_weight"+mail_city_count).rules("add",{ required:true,digits:true,min:0,max:999, messages: {required:"不能为空",digits:"请输整数",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
	  $("#mail_trans_add_fee"+mail_city_count).rules("add",{ required:true,number:true,min:0,max:999, messages: {required:"不能为空",number:"请输数字",min:"请输入0-999的数字",max:"请输入0-999的数字"} });
	  }
	}

//编辑运送到的地区
function edit_trans_city(obj){
  let id = obj.id
  //edit_trans_city2
  let num = id.substring(15, id.length)
  //$("#pdAmount").val(_prede_price)
  console.log(num)
  $("#select_id" + num).val(1)
	 var transCityType=jQuery(obj).attr("trans_city_type");
	 var transIndex=jQuery(obj).parent().parent().parent().attr("index");
	 var areaBoxTop=$(obj).offset().top-200;
	 jQuery.ajax({
			type:'POST',
			url:paramData.contextPath+'/s/transport/loadOverlay',
			data:{"transCityType":transCityType,"transIndex":transIndex},
			success:function(data){
			             jQuery(".main").append(data);
			             $(".area_box").css("top",areaBoxTop);
					  }
			})
}

//操作：删除
function remove_trans_city(obj){
	  jQuery(obj).parent().parent().parent().remove();
	}

//批量操作
jQuery("a[id^=batch_set_]").live("click",function(){
    jQuery(this).parent().parent().find(":checkbox").show();
	 var type=jQuery(this).attr("trans_type");
	 jQuery("#"+type+"_trans_city_op").show();
	 jQuery(this).hide();
	 jQuery("#batch_cancle_"+type).show();
	 if(type=="mail"){
	   mail_batch=1;
	 }
	 if(type=="express"){
      express_batch=1;
	 }
	 if(type=="ems"){
	    ems_batch=1;
	 }
 });

 //取消批量操作
 jQuery("a[id^=batch_cancle_]").live("click",function(){
    jQuery(this).parent().parent().find(":checkbox").hide();
	 var type=jQuery(this).attr("trans_type");
	 jQuery("#"+type+"_trans_city_op").hide();
	 jQuery(this).hide();
	 jQuery("#batch_set_"+type).show();
	 if(type=="mail"){
	   mail_batch=0;
	 }
	 if(type=="express"){
      express_batch=0;
	 }
	 if(type=="ems"){
	    ems_batch=0;
	 }
 });

 //批量操作中的 批量删除
 jQuery("a[id^=batch_del_]").live("click",function(){
    jQuery(this).parent().parent().find(":checkbox[checked][id^=trans_ck]").each(function(){
	     jQuery(this).parent().parent().parent().parent().remove();
	 });
	 jQuery("#mail_trans_all").attr("checked",false);
	 jQuery("#express_trans_all").attr("checked",false);
	 jQuery("#ems_trans_all").attr("checked",false);
 });

 //批量操作中的 全选 （平邮）
 jQuery(":checkbox[id$=mail_trans_all]").live("click",function(){
	 var ck=jQuery(this).attr("checked");
	 if(ck=="checked"){
       jQuery(this).parent().parent().parent().find(":checkbox").attr("checked",true);
	 }else{
	   jQuery(this).parent().parent().parent().find(":checkbox").attr("checked",false);
	 }
  });
 //批量操作中的 全选 （快递）
 jQuery(":checkbox[id$=express_trans_all]").live("click",function(){
	 var ck=jQuery(this).attr("checked");
	 if(ck=="checked"){
       jQuery(this).parent().parent().parent().find(":checkbox").attr("checked",true);
	 }else{
	   jQuery(this).parent().parent().parent().find(":checkbox").attr("checked",false);
	 }
  });
 //批量操作中的 全选 （EMS）
 jQuery(":checkbox[id$=ems_trans_all]").live("click",function(){
	 var ck=jQuery(this).attr("checked");
	 if(ck=="checked"){
       jQuery(this).parent().parent().parent().find(":checkbox").attr("checked",true);
	 }else{
	   jQuery(this).parent().parent().parent().find(":checkbox").attr("checked",false);
	 }
  });

 function submitTable(){
   $("#form1").submit();
 }

//为指定地区城市设置运费 不可为空
 function checkCityIds(){
	 var rs=true;
	 var mailIds = $("input[id^='mail_city_ids']").get();
	 if(!isBlank(mailIds)){
		 for(var x=0;x<mailIds.length;x++){
			 if(isBlank($(mailIds[x]).val())){layer.alert("平邮处，指定运送到的城市不可为空！", {icon: 0}); rs=false; }
		 }
	 }

	 var expressIds = $("input[id^='express_city_ids']").get();
	 if(!isBlank(expressIds)){
		 for(var y=0;y<expressIds.length;y++){
			 if(isBlank($(expressIds[y]).val())){layer.alert("快递处，指定运送到的城市不可为空！", {icon: 0}); rs=false; }
		 }
	 }

	 var emsIds= $("input[id^='ems_city_ids']").get();
	 if(!isBlank(emsIds)){
		 for(var z=0;z<emsIds.length;z++){
			 if(isBlank($(emsIds[z]).val())){layer.alert("ems处，指定运送到的城市不可为空！", {icon: 0}); rs=false; }
		 }
	 }
	if($("#id").val()==null||$("#id").val()==""){
		 $.ajax({
	          type : "post",
	          url : "/s/transport/checkName",
	          data : {"name":$("#transName").val()},
	          async : false,
	          dataType : "json",
	          success : function(data){
	        	  if(data){
	     			 layer.msg("模板名称已存在",{icon:2});
	     			 rs= false;
	     		 }
	          }
	     });
	}
	 return rs;
 }


//方法，判断是否为空
 function isBlank(_value){
 	if(_value==null || _value=="" || _value==undefined || _value=="null"){
 		return true;
 	}
 	return false;
 }

 //是否删除
 function isDelete(ben) {
   layer.confirm("确认要删除当前地区的设置么？",
     {icon : 3,  btn : [ '确定', '关闭' ]},
     function() {
       remove_trans_city(ben);
       layer.closeAll();
     });
 }
