var isPresell = false;//记住用户当前选中的sku是否是预售商品,默认为否
var isAuction = false; //记住用户当前选中的sku是否是拍卖商品,默认为否
var isSeckill = false; //记住用户当前选中的sku是否是秒杀商品,默认为否
var presellId = null;//预售活动ID
var auctionId = null;//拍卖活动ID
var seckillId = null;//秒杀活动ID
var arrivalInformSkuId;

$(function () {

  //初始化配送方式选择
  $("input:radio[name='deliveryType']").each(function () {
    if (this.checked) {
      $(this).parent().parent().addClass("radio-wrapper-checked");
    } else {
      $(this).parent().parent().removeClass("radio-wrapper-checked");
    }
  });

  var deliveryType = $('input[name="deliveryType"]:checked').val();
  // 根据选择的配送方式 显示不同的配送方式
  if (deliveryType == 'EXPRESS') {// 快递配送
    $(".store-hide").show();
    $("#selectShopStroe").hide();
  }
  if (deliveryType == 'PICKUP') {// 门店自提

    $("#selectShopStroe").show();
    $(".store-hide").hide();
  }

  //切换配送方式
  $("input:radio[name='deliveryType']").change(function () {
    $("input:radio[name='deliveryType']").each(function () {
      if (this.checked) {
        $(this).parent().parent().addClass("radio-wrapper-checked");
      } else {
        $(this).parent().parent().removeClass("radio-wrapper-checked");
      }
    });

    var deliveryType = $('input[name="deliveryType"]:checked').val();
    // 根据选择的配送方式 显示不同的配送方式
    if (deliveryType == 'EXPRESS') {// 快递配送
      $(".store-hide").show();
      $("#selectShopStroe").hide();
    }
    if (deliveryType == 'PICKUP') {// 门店自提

      $("#selectShopStroe").show();
      $(".store-hide").hide();
    }

    //处理sku信息
    handleSkuMessage();
  });

  // 处理默认选中的信息
  var liSelecteds = $(".prop_ul .li-selected").get();
  if (isBlank(liSelecteds) || liSelecteds.length == 0) {
    if (typeof (skuDtoList) == "undefined" || skuDtoList == "null" || skuDtoList == "" || skuDtoList == null) {
      return;
    }
    var promotionPrice = Number(skuDtoList[0].promotionPrice);
    var cash = Number(skuDtoList[0].cash);
    if (promotionPrice <= cash) { //说明有折扣
      var discountPrice = Math.round((cash - promotionPrice) * 100) / 100;
      $("#prodCash").html(promotionPrice.toFixed(2));
      selectSkuCash = promotionPrice.toFixed(2);
      if (discountPrice > 0) {
        var html = " (原价:￥" + cash.toFixed(2) + " 已优惠￥" + discountPrice.toFixed(2) + ")";
        $("#promotionPriceStr").html(html);
      } else {
        $("#promotionPriceStr").html("");
      }
    } else {
      $("#prodCash").html(cash.toFixed(2));
      selectSkuCash = cash.toFixed(2);
    }
    //Sku 促销
    var ruleList = skuDtoList[0].ruleList;
    var html = rulesHtml(ruleList);
    if (isBlank(html)) {
      $("#rules").html("");
    } else {
      $("#rules").html("");
      $("#rules").html(html);
    }
    handleSkuMessage();
  } else {
    for (var i = 0; i < liSelecteds.length; i++) {
      checkFollowingPropVal($(liSelecteds[i]).parent().attr("index"));
      handleSkuMessage();
      getPropValImgs($(liSelecteds[i]).attr("valId"));
    }
  }

  //店铺分数
  var shopScore = $("#shopScoreNum").text();
  if (shopScore == null) {
    shopScore = 0;
  }
  $(".scroll_red").css("width", (shopScore / 5) * 100 + "%");


  $("#recommend ul li").click(function () {
    $("#recommend ul li").each(function (i) {
      $(this).removeClass("on");
    });
    $(this).addClass("on");
  });

  $(".prop_ul .li-show").live("click", function () {
    clickPropVal(this);
  });

  $("#productTab ul li").click(function () {
    $("#productTab ul li").each(function (i) {
      $(this).removeClass("on");
      var id = $(this).attr("id");
      // alert($(id + "Pane"));
      $("#" + id + "Pane").hide();
    });
    $(this).addClass("on");
    var id = $(this).attr("id");
    $("#" + id + "Pane").show();
    changeProductTab(id);
    if (jQuery("#productTab").hasClass("pro_fixed")) {
      document.getElementById("detail").scrollIntoView();
    }
  });

  // 上浮事件
  var pro_tab_top = jQuery("#productTab").offset().top;
  jQuery(window).scroll(function () {
    var top = jQuery("#productTab").offset().top;
    var scrolla = jQuery(window).scrollTop();
    var i = top - scrolla;
    if (i <= 0) {
      jQuery("#productTab").addClass("pro_fixed");
      $(".nav-minicart-inner").show();
    }
    if (scrolla < pro_tab_top) {
      jQuery("#productTab").removeClass("pro_fixed");
      $(".nav-minicart-inner").hide();
    }
  });

  //配送地址 hover
  $("#storeSelector").hover(function () {
    openSelector(this);
  }, function () {
    closeStoreSelector();
  })

  // 配送至 选择框 省份选择
  jQuery("#stock_province_item ul[class=area-list] li").live("click",
    function () {
      var provinceId = $(this).find("a").attr("data-value");
      var province = $(this).find("a").html();
      $("#stockTab li[data-index=0] a em").html(province);
      $("#stockTab li[data-index=0]").attr("provinceid", provinceId);

      jQuery.ajax({
        url: contextPath + "/load/cityList/" + provinceId,
        type: 'post',
        async: true, // 默认为true 异步
        success: function (result) {
          $("#stock_city_item").html(result);

          $("#stockTab li").each(function (i) {
            $(this).removeClass("curr");
            $(this).find("a").removeClass("hover");
          });

          var cityTab = $("#stockTab li[data-index=1]");
          cityTab.addClass("curr");
          cityTab.find("a").addClass("hover");
          cityTab.find("em").html("请选择");
          cityTab.attr("style", "display: block;");

          $("#JD-stock div[class=mc]").each(function () {
            $(this).attr("style", "display: none;");
          });
          $("#stock_city_item").attr("style",
            "display: block;");
        }
      });
    }
  );

  // 配送至 选择框 城市选择
  jQuery("#stock_city_item ul[class=area-list] li").live("click",
    function () {
      var cityId = $(this).find("a").attr("data-value");
      var city = $(this).find("a").html();
      $("#selCityId").val(cityId);
      $("#stockTab li[data-index=1] a em").html(city);

      jQuery.ajax({
        url: contextPath + "/load/areaList/" + cityId,
        type: 'post',
        async: true, // 默认为true 异步
        success: function (result) {
          $("#stock_area_item").html(result);

          $("#stockTab li").each(function (i) {
            $(this).removeClass("curr");
            $(this).find("a").removeClass("hover");
          });

          var areaTab = $("#stockTab li[data-index=2]");
          areaTab.addClass("curr");
          areaTab.find("a").addClass("hover");
          areaTab.find("em").html("请选择");
          areaTab.attr("style", "display: block;");

          $("#JD-stock div[class=mc]").each(function () {
            $(this).attr("style", "display: none;");
          });
          $("#stock_area_item").attr("style",
            "display: block;");
        }
      });
    }
  );

  // 配送至 选择框 地区选择
  $("#stock_area_item").on("click", "ul[class=area-list] li", function () {

      var areaId = $(this).find("a").attr("data-value");

      var area = $(this).find("a").html();
      var city = $("#stockTab li[data-index=1] a em").html();
      var province = $("#stockTab li[data-index=0] a em").html();
      var text = province + " " + city + " " + area;

      $("#stockTab li[data-index=2] a em").html(area);

      $("#storeSelector div[class=text] div").html(text);
      $("#storeSelector div[class=text] div").attr("title",
        text);
      $("#storeSelector").removeClass("hover");

      $("#selProvinceId").val($("#stockTab li[data-index=0]").attr("provinceid"));
      //计算运费
      calProdFreight();

      //计算库存
      calSkuStocks();
    }
  );

  //qq在线客服
  $('#close_im').bind('click', function () {
    $('#main-im').css("height", "0");
    $('#im_main').hide();
    $('#open_im').show();
  });

  $('#open_im').bind('click', function (e) {
    $('#main-im').css("height", "272");
    $('#im_main').show();
    $(this).hide();
  });

  //计算运费
  calProdFreight();

  // 产品评论
  queryProdComment(currProdId, 0);

  // 产品咨询
  queryProdConsult(currProdId);
  // call ajax
  $.post(contextPath + "/visitedprod_old", function (retData) {
    $("#visitedprod").html(retData);
  }, 'html');

});


/**
 * 点击属性值
 */
function clickPropVal(obj) {

  //当属性值之前没有被选中时
  if ($(obj).children("i").length <= 0) {
    //把当前值 改为选中状态
    $(obj).addClass("li-selected").siblings().removeClass("li-selected");
    $(obj).addClass("li-selected").siblings().children("i").remove();
    $(obj).append("<i></i>");
    $("#pamount").val("1");

    //获取当前属性的序号
    var propIndex = $(obj).parent().attr("index");

    //根据SKU判断后面的属性值，不可选的属性值 灰掉
    checkFollowingPropVal(propIndex);

    //处理sku信息
    handleSkuMessage();

    //获取属性值图片
    getPropValImgs($(obj).attr("valId"));
  }
}

//处理商品是否参与活动  理论上 一个sku或商品，只会参加一个营销活动
function handleActivities() {
  if (!existPresellPlugin || !existAuctionPlugin) { //相关营销活动插件下线
    return;
  }
  var _skuType = $("#skuType").val();
  if (_skuType == 'PRESELL') {
    isPresellProd();//当前选中sku是否预售
  } else if (_skuType == 'AUCTION') {
    isAuctionProd();// 当前选中sku是否拍卖
  } else if (_skuType == 'SECKILL') {
    isSeckillProd();// 当前选中sku是否秒杀
  } else {//正常购买
    isNormalBuy();
  }
}

//判断用户当前选中的sku是否是已参与预售
function isPresellProd() {
  if (isBlank(isGroup) || isGroup == 1) {
    return false;
  }
  var skuId = $("#currSkuId").val();
  $.ajax({
    url: contextPath + "/presell/isPresellProd",
    async: true,
    data: {
      "prodId": currProdId,
      "skuId": skuId
    },
    dataType: "JSON",
    success: function (result) {
      if (result) {
        presellId = result.id;
        isPresell = true;
        $("#storeInfo").hide();//隐藏门店信息
        $("#storeInfo").attr("style", "display:none;");
        $("#deliveryType").hide();//隐藏配送方式
        $("#deliveryType").attr("style", "display:none;");
        $("#presell-tips").show();
        $("#presell-tips").find("span").html("正在预售的商品,无法以正常模式下单,请您先预定");
        $("#J_LinkBuy").text("立刻预定");
        $("#J_LinkBuy").attr("href", "javascript:reserveNow()");
        $("#J_LinkBasket").hide();
        $("#phoneBuy").hide();
      } else {
        isPresell = false;
        // $("#storeInfo").show();//显示门店信息
        $("#presell-tips").hide();
        $("#J_LinkBasket").show();
        $("#J_LinkBuy").text("立刻购买");
        $("#J_LinkBuy").removeAttr("href");
        $("#J_LinkBuy").attr("onclick", "javascript:buyNow()");
      }
    }
  });
}

//立即预定
function reserveNow() {
  window.location = contextPath + "/presell/views/" + presellId;
}


//判断用户当前选中的sku是否是已经参与拍卖
function isAuctionProd() {
  if (isBlank(isGroup) || isGroup == 1) {
    return false;
  }
  var skuId = $("#currSkuId").val();
  $.ajax({
    url: contextPath + "/auction/isAuction",
    async: true,
    data: {
      "prodId": currProdId,
      "skuId": skuId
    },
    dataType: "JSON",
    success: function (result) {
      if (result) {
        auctionId = result.id;
        isAuction = true;
        $("#storeInfo").hide();//隐藏门店信息
        $("#storeInfo").attr("style", "display:none;")
        $("#presell-tips").show();
        $("#presell-tips").find("span").html("正在拍卖的商品,无法以正常模式下单,请您先预定");
        $("#J_LinkBuy").text("前往拍卖");
        $("#J_LinkBuy").attr("href", "javascript:toAuction()");
        $("#J_LinkBasket").hide();
        $("#phoneBuy").hide();
      } else {
        isAuction = false;
        $("#presell-tips").hide();
        $("#J_LinkBasket").show();
        $("#J_LinkBuy").text("立刻购买");
        $("#J_LinkBuy").removeAttr("href");
        $("#J_LinkBuy").attr("onclick", "javascript:buyNow()");
      }
    }
  });
}


//判断用户当前选中的sku是否是已经参与秒杀
function isSeckillProd() {
  if (isBlank(isGroup) || isGroup == 1) {
    return false;
  }
  var skuId = $("#currSkuId").val();
  $.ajax({
    url: contextPath + "/seckill/isSeckill",
    async: true,
    data: {
      "prodId": currProdId,
      "skuId": skuId
    },
    dataType: "JSON",
    success: function (result) {
      if (result) {
        seckillId = result.activeId;
        isSeckill = true;
        $("#storeInfo").hide();//隐藏门店信息
        $("#storeInfo").attr("style", "display:none;")
        $("#presell-tips").show();
        $("#presell-tips").find("span").html("正在秒杀的商品,无法以正常模式下单,请您先预定");
        $("#J_LinkBuy").text("前往秒杀");
        $("#J_LinkBuy").attr("href", "javascript:toSeckill()");
        $("#J_LinkBasket").hide();
        $("#phoneBuy").hide();
      } else {
        isSeckill = false;
        $("#presell-tips").hide();
        $("#J_LinkBasket").show();
        $("#J_LinkBuy").text("立刻购买");
        $("#J_LinkBuy").removeAttr("href");
        $("#J_LinkBuy").attr("onclick", "javascript:buyNow()");
      }
    }
  });
}

//立即秒杀预定
function toSeckill() {
  window.location = contextPath + "/seckill/views/" + seckillId;
}

//立即拍卖预定
function toAuction() {
  window.location = contextPath + "/auction/views/" + auctionId;
}

//正常购买，什么营销活动都没有参加
function isNormalBuy() {
  presellId = null;
  auctionId = null;
  isPresell = false;
  isAuction = false;
  isSeckill = false;
  $("#phoneBuy").show();
  $("#presell-tips").hide();
  $("#J_LinkBasket").show();
  $("#J_LinkBuy").text("立刻购买");
  $("#J_LinkBuy").removeAttr("href");
  $("#J_LinkBuy").attr("onclick", "javascript:buyNow()");
}


/**
 * 获取 包含前面已选属性值 的SKU集合
 */
function getSelectedSkus(propIndex) {
  var selectedLiList = [];
  var selIndex = 0;
  for (var h = 0; h <= propIndex; h++) {
    if ($("#prop_" + h + " .li-selected").length != 0) {
      selectedLiList[selIndex] = $("#prop_" + h + " .li-selected").get(0);
      selIndex++;
    }
  }
  //sku的properties是k:v;k:v;k:v的形式
  //循环判断之前选中的属性值 k:v 是否在某个sku里面
  var index = 0;
  var newSkus = [];
  for (var i = 0; i < skuDtoList.length; i++) {
    var has = true;
    for (var j = 0; j < selectedLiList.length; j++) {
      var kv = $(selectedLiList[j]).attr("data-value");
      if (skuDtoList[i].properties.indexOf(kv) < 0) {
        has = false;
        break;
      }
    }
    if (has) {
      newSkus[index] = skuDtoList[i];
      index++;
    }
  }
  return newSkus;
}

/**
 * 处理sku信息
 */
function handleSkuMessage() {
  $(".prop_main dl").removeClass("no-selected");
  //当所有属性 都选中时，组成一个SKU
  if ($(".li-selected").length == $(".prop_ul").length) {
    //计算选中sku的价格和促销信息
    calculateSkuPrice();
    allSelected = true;//全局变量,用于判断是否可以加入购物车
    bySkuActivity();//判断活动
    calSkuStocks();//计算库存
    //判断门店库存，用于是否显示到店自提
    skuEnableStore();
  } else {
    var dlList = $(".prop_main dl").get();
    for (var i = 0; i < dlList.length; i++) {
      if ($(dlList[i]).find(".li-selected").length == 0) {
        /*	$(dlList[i]).addClass("no-selected"); */
      }
    }
    allSelected = false;//全局变量,用于判断是否可以加入购物车
  }
}

/**
 * 根据SKU判断后面的属性值，不可选的属性值 灰掉
 */
function checkFollowingPropVal(propIndex) {
  var id = Number(propIndex);
  while ($("#prop_" + id).length != 0) {
    var aList = $("#prop_" + id + " a").get();
    for (var i = 0; i < aList.length; i++) {
      var kv = $(aList[i]).parent().attr("data-value");
      var has = false;
      var newSkus = getSelectedSkus(id - 1);
      for (var j = 0; j < newSkus.length; j++) {
        if (newSkus[j].properties.indexOf(kv) >= 0 && newSkus[j].status == 1) {
          has = true;
          break;
        }
      }
      if (!has) {
        $(aList[i]).parent().removeClass("li-selected");
        $(aList[i]).parent().children("i").remove();
        $(aList[i]).parent().addClass("li-hidden");
        $(aList[i]).parent().removeClass("li-show");
      } else {
        $(aList[i]).parent().addClass("li-show");
        $(aList[i]).parent().removeClass("li-hidden");
      }
    }
    id++;
  }
}

/**
 * 计算选中sku的价格和促销信息
 */
function calculateSkuPrice() {
  var kvList = [];
  var selectedList = $(".li-selected").get();
  for (var k = 0; k < selectedList.length; k++) {
    kvList[k] = $(selectedList[k]).attr("data-value");
  }
  for (var i = 0; i < skuDtoList.length; i++) {
    var isThisSku = true;
    for (var j = 0; j < kvList.length; j++) {
      if (skuDtoList[i].properties.indexOf(kvList[j]) < 0) {
        isThisSku = false;
        break;
      }
    }

    if (isThisSku) {
      $("#currSkuId").val(skuDtoList[i].skuId);
      $("#skuType").val(skuDtoList[i].skuType);
      if (skuDtoList[i].name != null && skuDtoList[i].name != '') {
        $("#prodName").html(skuDtoList[i].name);
      } else {
        $("#prodName").html(prodName);
      }
      var promotionPrice = Number(skuDtoList[i].promotionPrice);
      var cash = Number(skuDtoList[i].cash);

      if (promotionPrice <= cash) { //说明有折扣
        var discountPrice = Math.round((cash - promotionPrice) * 100) / 100;
        $("#prodCash").html(promotionPrice.toFixed(2));
        selectSkuCash = promotionPrice.toFixed(2);
        if (discountPrice > 0) {
          var html = " (原价:￥" + cash.toFixed(2) + " 已优惠￥" + discountPrice.toFixed(2) + ")";
          $("#promotionPriceStr").html(html);

        } else {
          $("#promotionPriceStr").html("");
        }
      } else {
        $("#prodCash").html(cash.toFixed(2));
        selectSkuCash = cash.toFixed(2);
      }
      //Sku 促销
      var ruleList = skuDtoList[i].ruleList;
      var html = rulesHtml(ruleList);
      if (html == null || html == "" || html == undefined) {
        $("#rules").html("");
      } else {
        $("#rules").html("");
        $("#rules").html(html);
      }

      //设置选中sku的重量、体积 2018-11-28 添加
      if (!isBlank(skuDtoList[i].weight)) {
        weight = skuDtoList[i].weight;
      }
      if (!isBlank(skuDtoList[i].volume)) {
        volume = skuDtoList[i].volume;
      }
      //选择sku 重新计算运费
      calProdFreight();

      //处理用户选择的SKU参与营销活动的情况
      handleActivities();
      break;
    }
  }
}

function rulesHtml(ruleList) {
  if (ruleList == null || ruleList == "" || ruleList == undefined) {
    return;
  } else if (ruleList.length == 0) {
    return;
  }
  var rulesHtml = "<dt>促销信息：</dt><dd>";
  for (var i in ruleList) {
    rulesHtml += "<span>" + ruleList[i].promotionInfo + "</span>";
    rulesHtml += "<br/>";
  }
  rulesHtml += "</dd>";
  return rulesHtml;

}

/**
 * 获取属性值图片
 */
function getPropValImgs(valId) {
  var arr = imgPath3.split("?");
  var prefix = arr[0];
  var suffix = arr[1];
  for (var i = 0; i < propValueImgList.length; i++) {
    if (propValueImgList[i].valueId == valId) {

      var imgList = propValueImgList[i].imgList;
      var str = "";
      for (var j = 0; j < imgList.length; j++) {
        str += "<li onmouseover='viewPic(this);' data-url='" + imgList[j] + "' class='img-default'>";
        str += "<table cellpadding='0' cellspacing='0' border='0'><tr><td height='65' width='65' style='text-align: center;'>";
        str += "<img src='" + prefix + imgList[j] + "?" + suffix + "' style='max-width:65px;max-height:65px;'  />";
        str += "</td></tr></table>";
        str += "</li>";

      }
      $(".preview .simglist").html(str);
      viewPic($(".preview .simglist li").get(0));
      $("#spec-list").infiniteCarousel();
      break;
    }
  }
}

//切换详情介绍的  tab
function changeProductTab(id) {
  if ("prodSpec" == id) {
    queryParameter(currProdId);
  } else if ("prodService" == id) {
    queryProdService(currProdId)
  } else if ("prodComment" == id) {
    queryProdComment(currProdId, 0);
  }
}

//获取参数页面
var paramResult;

function queryParameter(prodId) {
  if (paramResult != undefined) {
    $("#prodSpecPane").html(paramResult);
  } else {
    $.ajax({
      url: contextPath + "/dynamic/queryDynamicParameter",
      data: {"prodId": prodId},
      type: 'post',
      async: true, //默认为true 异步
      success: function (data) {
        paramResult = data;
        $("#prodSpecPane").html(data);
      }
    });
  }
}

//获取评论
function queryProdComment(prodId, condition) {
  var data = {
    "curPageNO": jQuery("#prodCommentCurPageNO").val(),
    "prodId": prodId,
    "condition": condition
  };

  jQuery.ajax({
    url: contextPath + "/productcomment/list",
    data: data,
    type: "GET",
    async: true, //默认为true 异步
    success: function (result) {
      $("#prodCommentPagenationPane").html(result);
    }
  });
}

// 获取服务说明
function queryProdService(prodId) {
  var data = {
    "prodId": prodId
  };
  jQuery.ajax({
    url: contextPath + "/productcomment/serviceShow",
    data: data,
    type: "GET",
    async: true, //默认为true 异步
    success: function (result) {
      $("#prodServicePane").html(result);
    }
  });
}

//获取咨询
function queryProdConsult(prodId) {
  var data = {
    "curPageNO": jQuery("#prodCousultCurPageNO").val()
  };
  jQuery.ajax({
    url: contextPath + "/productConsult/list/" + prodId,
    data: data,
    type: 'post',
    async: true, //默认为true 异步
    success: function (result) {
      jQuery("#prodCousultPagenationPane").html(result);
    }
  });
}

//敏感字检查
function checkSensitiveData(content) {
  var url = contextPath + "/sensitiveWord/filter";
  $.ajax({
    "url": url,
    data: {"src": content},
    type: 'post',
    dataType: 'json',
    async: false, //默认为true 异步
    success: function (retData) {
      result = retData;
    }
  });
  return result;
}

//提交咨询
function saveConsult() {
  var content = jQuery("#consultationContent").val();
  if (content == null || content.length < 5 || content.length > 100) {
    layer.msg("请认真填写，咨询内容必须为5-100个字");
  }

  var chkResult = checkSensitiveData(content);
  if (chkResult != null && chkResult != '') {
    layer.alert("您的输入含有敏感字： '" + chkResult + "' 请更正后再提交", {icon: 2});
    return;
  }

  var data = {
    "content": content,
    prodId: currProdId,
    pointType: jQuery('input[type="radio"][name="pointType"]:checked')
      .val()
  };
  jQuery.ajax({
    url: contextPath + "/productConsult/save",
    data: data,
    type: 'post',
    async: true, // 默认为true 异步
    success: function (result) {
      if (result == 0) {
        jQuery("#consultationContent").val("");
        jQuery("#consult_refer_result").html("提交咨询成功");
        // 刷新数据
        var pointType = jQuery("#pointType").val();
        // curPage = 1返回第一页
        queryProdConsultList(currProdId, pointType, 1);
      } else if (result == -1) {
        jQuery("#consult_refer_result").html("请登录后再提交咨询");
      } else if (result == -2) {
//				jQuery("#consult_refer_result").html("请认真填写，咨询内容必须大于5个字。");
        $("#consult_refer_result").hidden();
      } else if (result == -3) {
        jQuery("#consult_refer_result").html("您刚才已经提交了咨询，请5分钟后再提交咨询");
      } else {
        jQuery("#consult_refer_result").html("提交咨询失败");
      }
      jQuery("#column_refer_result").show();
    }
  });
}

//关注商品
function alertAddInterestDiag(prodId) {
  jQuery.ajax({
    url: contextPath + "/isUserLogin",
    type: 'get',
    async: false, // 默认为true 异步
    success: function (result) {
      if (result == "true") {
        layer.open({
          title: "商品收藏",
          type: 2,
          content: contextPath + '/loadInterestOverlay/' + prodId, //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
          area: ['430px', '560px']
        });
      } else {
        login();
      }
    }
  });
}

function closeSelector(obj) {
  jQuery(obj).removeClass("hover");
}

function onmouseOver() {
  $("#storeSelector").addClass("hover");
}

function onmouseOut() {
  $("#storeSelector").removeClass("hover");
}

function openSelector(obj) {
  var left = jQuery(obj).offset().left - 100;
  var top = jQuery(obj).offset().top + 25;
  var closeLeft = jQuery(obj).offset().left + 312;
  var closeTop = jQuery(obj).offset().top + 22;
  jQuery(obj).find("div[class=content]").attr("style",
    "top:" + top + "px;left:" + left + "px;");
  jQuery(obj).find("div[class=close]").attr("style",
    "top:" + closeTop + "px;left:" + closeLeft + "px;");
  jQuery(obj).addClass("hover");
}

function closeStoreSelector() {
  var obj = $(".addrselector");
  obj.removeClass("hover");
}

//选择省份
function selectProvince(obj) {
  $("#stockTab li").each(function () {
    $(this).removeClass("curr");
    $(this).find("a").removeClass("hover");
    $(this).attr("style", "display:none");
  });

  $(obj).addClass("curr");
  $(obj).find("a").addClass("hover");
  $(obj).find("em").html("请选择");
  $(obj).attr("style", "display:block");

  $("#JD-stock div[class=mc]").each(function () {
    $(this).attr("style", "display: none;");
  });
  $("#stock_province_item").attr("style", "display: block;");

}

//选择城市
function selectCity(obj) {
  $("#stockTab li").each(function (i) {
    $(this).removeClass("curr");
    $(this).find("a").removeClass("hover");
  });
  $("#stockTab li[data-index=2]").attr("style", "display: none;");
  $(obj).addClass("curr");
  $(obj).find("a").addClass("hover");
  $(obj).find("em").html("请选择");

  $("#JD-stock div[class=mc]").each(function () {
    $(this).attr("style", "display: none;");
  });
  $("#stock_city_item").attr("style", "display: block;");
}

function getProdAttr() {
  var prodattr = "";
  var errMsg = "";
  var attrselect = $(".attrselect");
  for (var i = 0; i < attrselect.size(); i++) {
    if (attrselect.get(i).value == '') {
      errMsg = errMsg + " " + attrselect.get(i).getAttribute("dataValue");
    } else {
      prodattr = prodattr + attrselect.get(i).getAttribute("dataValue")
        + ":" + attrselect.get(i).value + ";";
    }

  }
  if (errMsg != "") {
    prodattr = "error" + errMsg;
  }
  return prodattr;
}

var windowHeight = $(window).height();
var windowWidth = $(window).width();

//加入购物车
function addShopCart() {
  if (!allSelected) {
    return false;
  }
  var result = validatebuyNumber();
  var prodId = currProdId;
  var pamount = $("#pamount").val();

  var scrollY = $(document).scrollTop();
  if (result) {

    var deliveryType = $('input[name="deliveryType"]:checked').val();
    var storeId;
    if (deliveryType == 'PICKUP') { //选择门店自提 TODO
      storeId = $("#selectedStoreId").val();
      if (isBlank(storeId)) {
        layer.msg("请选择门店");
        return;
      }
    } else {
      storeId = "";
    }

    var params = {
      "prodId": prodId,
      "skuId": $("#currSkuId").val(),
      "count": pamount,
      "shopId": shopId,
      "distUserName": distUserName,
      "storeId": storeId
    };
    var config = true;
    $.ajax({
      type: "POST",
      url: contextPath + "/shopCart/addShopBuy",
      data: params,
      async: false, //默认为true 异步
      dataType: "json",
      success: function (response) {
        if (response == "OK") {
          var offset = $("#J-MiniCart").offset();
          var _default = $(".simglist").children(".img-default").get(0);
          ;
          var img = $(_default).find("img").attr('src');
          var flyer = $('<img class="u-flyer" src="' + img + '">');
          flyer.fly({
            start: {
              left: windowWidth / 2, //鼠标指针位置相对于窗口客户区域的X坐标
              top: windowHeight / 2 //鼠标指针位置相对于窗口客户区域的Y坐标
            },
            end: {
              left: offset.left + 10,
              top: offset.top + 10,
              width: 0,
              height: 0
            },
            onEnd: function () {
              refreshTopBar();
              this.destory();
            }
          });
        } else {
          layer.alert(response, {icon: 2});
          config = false;
        }
      }
    });
    return config;
  }
  return false;
}

function refreshTopBar() {
  //刷新top bar 区域
  $.ajax({
    type: "GET",
    url: contextPath + "/shopCart/count",
    data: {'timeStamp': new Date().getTime()},
    success: function (response) {
      if (response > 99) {
        $("#shppingItemCt").html("99+");
      } else
        $("#shppingItemCt").html(response);
    }
  });
};


function validatebuyNumber() {
  var pamount = $.trim($("#pamount").val());
  var testNumber = /^[0-9]*[1-9][0-9]*$/;
  if (isBlank(pamount)) {
    $("#pamount").css("border", "1px solid red");
    $("#pamount").attr("title", "不能为空！");
    return false;
  }
  if (isNaN(pamount)) {//如果不是数字就返回
    $("#pamount").css("border", "1px solid red");
    $("#pamount").attr("title", "请输入正确的格式！");
    return false;
  }
  if (!testNumber.test(pamount)) {
    $("#pamount").css("border", "1px solid red");
    $("#pamount").attr("title", "请输入正确的格式！");
    return false;
  }
  if (Number(pamount) <= 0) {//如果不是数字就返回
    $("#pamount").css("border", "1px solid red");
    $("#pamount").attr("title", "购买件数必须大于0！");
    return false;
  }
  var productId = $.trim(currProdId);
  var shopId = $.trim(shopId);
  var skuId = $.trim($("#currSkuId").val());
  var result;
  $.ajax({
    type: "GET",
    url: contextPath + "/veryBuyNum/" + productId + "/" + skuId,
    data: {"buyNum": Number(pamount)},
    dataType: "json",
    async: false,
    success: function (response) {
      if (response == "OK") {
        $("#pamount").removeAttr("style");
        $("#pamount").removeAttr("title");
        result = true;
      } else {
        layer.alert(response, {icon: 2});
        $("#pamount").css("border", "1px solid red");
        $("#pamount").attr("title", response);
        result = false;
      }
    }
  });
  return result;
}

//立即购买
function buyNow() {

  //判断是否可以立即购买
  if (!allSelected) {
    return;
  }

  if (isLogin()) {
    login();
    return;
  }

  //校验购买数量
  var result = validatebuyNumber();
  if (!result) {
    return;
  }

  var prodId = currProdId;//当前SKU
  var skuId = $("#currSkuId").val();//当前商品
  var count = $("#pamount").val();//购买数量
  var buyNow = true;

  // 根据配送方式 立即购买跳转不同的详情页面
  var deliveryType = $('input[name="deliveryType"]:checked').val();
  var storeId;
  if (deliveryType == 'PICKUP') { //选择门店自提 TODO
    storeId = $("#selectedStoreId").val();
    if (isBlank(storeId)) {
      layer.msg("请选择门店");
      return;
    }
    // 组装门店自提立即购买参数
    var params = {
      "prodId": prodId,
      "skuId": skuId,
      "count": count,
      "buyNow": buyNow,
      "storeId": storeId
    }
    abstractStoreBuyNowForm(contextPath + '/p/store/order/storeOrderDetails', params);
  } else {

    // 组装快递配送立即购买参数
    var params = {
      "prodId": prodId,
      "skuId": skuId,
      "count": count,
      "buyNow": buyNow
    }
    abstractBuyNowForm(contextPath + "/p/orderDetails", params);
  }

}

//到货通知弹出窗
function productInformWindow() {
  //检查当前用户是否已经添加了到货通知
  var isExist = isAlreadySave();
  if (isExist == "login") {
    layer.msg("请登录后再添加到货通知!");
    return;
  } else if (isExist == "exist") {
    layer.msg("您已添加过到货通知，不需要重复添加！");
    return;
  }
  var skuId = $("#currSkuId").val();
  var data = "?shopId=" + shopId + "&prodId=" + currProdId + "&skuId=" + skuId;

  layer.open({
    title: "到货通知",
    id: 'prodArrInfoPage',
    type: 2,
    content: contextPath + "/views/prodArrInfoPage" + data, //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
    area: ['400px', '220px']
  });
}

//检查是否已经添加过到货通知
function isAlreadySave() {
  var skuId = $("#currSkuId").val();
  var result;
  jQuery.ajax({
    url: contextPath + "/views/isAlreadySave",
    data: {
      "skuId": skuId
    },
    type: "post",
    async: false, //默认为true,异步
    dataType: "json",
    success: function (msg) {
      result = msg;
    }
  });
  return result;
}

// 普通商品立即购买表单
function abstractBuyNowForm(URL, params) {
  var temp = document.createElement("form");
  temp.action = URL;
  temp.method = "post";
  temp.style.display = "none";

  var opt1 = document.createElement("input");
  opt1.name = "buyNow";
  opt1.value = params.buyNow;
  var opt2 = document.createElement("input");
  opt2.name = "prodId";
  opt2.value = params.prodId;
  var opt3 = document.createElement("input");
  opt3.name = "skuId";
  opt3.value = params.skuId;
  var opt4 = document.createElement("input");
  opt4.name = "count";
  opt4.value = params.count;

  temp.appendChild(opt1);
  temp.appendChild(opt2);
  temp.appendChild(opt3);
  temp.appendChild(opt4);

  temp.appendChild(appendCsrfInput());
  document.body.appendChild(temp);
  temp.submit();
  return temp;
}

// 门店自提立即购买表单
function abstractStoreBuyNowForm(URL, params) {
  var temp = document.createElement("form");
  temp.action = URL;
  temp.method = "post";
  temp.style.display = "none";

  var opt1 = document.createElement("input");
  opt1.name = "buyNow";
  opt1.value = params.buyNow;
  var opt2 = document.createElement("input");
  opt2.name = "prodId";
  opt2.value = params.prodId;
  var opt3 = document.createElement("input");
  opt3.name = "skuId";
  opt3.value = params.skuId;
  var opt4 = document.createElement("input");
  opt4.name = "count";
  opt4.value = params.count;
  var opt5 = document.createElement("input");
  opt5.name = "storeId";
  opt5.value = params.storeId;

  temp.appendChild(opt1);
  temp.appendChild(opt2);
  temp.appendChild(opt3);
  temp.appendChild(opt4);
  temp.appendChild(opt5);

  temp.appendChild(appendCsrfInput());
  document.body.appendChild(temp);
  temp.submit();
  return temp;
}

// 加入购物车表单
function abstractCartItemsForm(URL, chains) {
  var temp = document.createElement("form");
  temp.action = URL;
  temp.method = "post";
  temp.style.display = "none";
  var opt = document.createElement("input");
  opt.name = 'shopCartItems';
  opt.value = chains;
  temp.appendChild(opt);
  temp.appendChild(appendCsrfInput());
  document.body.appendChild(temp);
  temp.submit();
  return temp;
}


function sumitForm(prodId, skuId) {
  var sfForm = document.createElement("form");
  sfForm.method = "post";
  sfForm.action = contextPath + "/buySuccess";

  var tmpInput1 = document.createElement("input");
  tmpInput1.type = "hidden";
  tmpInput1.name = "prodId";
  tmpInput1.value = prodId;
  sfForm.appendChild(tmpInput1);
  if (skuId != "") {
    var tmpInput2 = document.createElement("input");
    tmpInput2.type = "hidden";
    tmpInput2.name = "skuId";
    tmpInput2.value = skuId;
    sfForm.appendChild(tmpInput2);
  }

  document.body.appendChild(sfForm);
  sfForm.submit();
}

//增加购买数量
function addPamount() {
  var pamount = $("#pamount").val();
  $("#pamount").val(pamount * 1 + 1);
}

//减少购买数量
function reducePamount() {
  var pamount = $("#pamount").val();
  if (pamount <= 1) {
    $("#pamount").val(1);
  } else {
    $("#pamount").val(pamount * 1 - 1);
  }
}

//得判断是否为数字的同时，也判断其是不是正数
function validateInput() {
  var pamount = $("#pamount").val();
  var re = /^[1-9]+[0-9]*]*$/;
  if (isNaN(pamount) || !re.test(pamount)) {
    //错误时
    $("#pamount").attr("style", "border: 1px solid #f40000;");
    $("#pamountError").html("请输入正确的数量！");
    $("#pamountError").removeClass("hide");
  } else {
    //正确时
    $("#pamount").attr("style", "");
    $("#pamountError").html("");
    $("#pamountError").attr("class", "classic-error hide");
  }

}

//计算运费
function calProdFreight() {
  if (goods_transfee == '' || goods_transfee == null || goods_transfee == undefined) {
    return;
  }

  if (goods_transfee == 0) { //买家承担运费
    if (transport_type == 0) { //运费模版
      transport();
    } else if (transport_type == 1) { //固定运费
      //var str = "<select class='frei_sel'><option>EMS:"+ems_trans_fee+"</option><option>快递:"+express_trans_fee+"</option><option>平邮:"+mail_trans_fee+"</option></select>";
      var str = "<select class='frei_sel'><option>快递:" + express_trans_fee + "</option></select>";
      $("#freightText").html(str);
    }
  } else if (goods_transfee == 1) { //卖家家承担运费
    $("#freightText").html("商家承担运费");
  }
  //根据省份计算库存
  //calSkuStocks();
}

function transport() {

  if (weight == "") {
    weight = 0;
  }
  if (volume == "") {
    volume = 0;
  }
  jQuery.ajax({
    url: contextPath + "/getProdFreight",
    data: {
      "shopId": shopId,
      "transportId": transportId,
      "cityId": $("#selCityId").val(),
      "totalCount": 1,
      "totalWeight": weight,
      "totalvolume": volume
    },
    type: 'post',
    async: true, //默认为true 异步
    dataType: 'json',
    success: function (retData) {
      if (retData == "") {
        $("#freightText").html("");
      } else {

        var fList = eval(retData);
        var str = "<select class='frei_sel'>";
        for (var i = 0; i < fList.length; i++) {
          str += "<option>";
          str += fList[i].desc;
          str += "</option>";
        }
        str += "</select>";
        $("#freightText").html(str);
        calSkuStocks();
      }
    }
  });
}

//根据城市计算库存
function calSkuStocks() {
  var deliveryType = $('input[name="deliveryType"]:checked').val();
  var storeId = "-1";
  if (deliveryType == 'PICKUP') { //选择门店自提 TODO
    storeId = $("#selectedStoreId").val();
  }

  jQuery.ajax({
    url: contextPath + "/calSkuStocksByProv",
    data: {
      "prodId": currProdId,
      "skuId": $("#currSkuId").val(),
      "cityId": $("#selCityId").val(),
      "transportId": transportId,
      "shopId": shopId,
      "storeId": storeId
    },
    type: 'post',
    async: true, //默认为true 异步
    dataType: 'json',
    success: function (result) {
      var stocksMsg = null;
      if (result == null || retData == "" || result == 0) {  //sku商品不存在库存
        if (!isPresell || !isAuction || !isSeckill) {//非预售商品
          //禁用加入购物车按钮,并变灰
          //$("#J_LinkBasket").attr("href","javascript:void(0);");
          $("#J_LinkBasket").removeAttr("onclick");
          $("#J_LinkBasket").css({"background-color": "#ddd", "border": "1px solid #ddd"});
          $("#J_LinkBuy").css({"background-color": "#ddd", "border": "1px solid #ddd", "color": "#fff"});//隐藏立即购买按钮
          $("#J_LinkBuy").removeAttr("onclick");
        }
        //连库存都没有，不能点击到货通知
        $("#arrivalButton").attr("href", "javascript:void(0);");
        $("#arrivalButton").css({"background-color": "#ddd", "border": "1px solid #ddd", "color": "#fff"});
        stocksMsg = "<span style='color:#e5004f'>无货&nbsp;&nbsp;&nbsp;</span>";
        $("#nowBuy").css({"display": "none"});
        $("#J_LinkBuy").removeAttr("href");
        $("#phoneBuy").css({"display": "none"});
        $("#arrivalInform").css({"display": "none", "class": "tb-btn-buy tb-btn-sku"});
      } else {
        //库存
        var retData = result.stocks;
        if (retData == null || retData == "" || retData == 0) {//无货
          $("#J_LinkBuy").css({"background-color": "#ddd", "border": "1px solid #ddd", "color": "#fff"});//隐藏立即购买按钮
          $("#J_LinkBuy").attr("href", "javascript:void(0);");
          $("#J_LinkBuy").removeAttr("onclick");
          $("#skuStock").html(" ");
          if (!isPresell) {//非预售商品

            $("#nowBuy").css({"display": "none"});
            //禁用加入购物车按钮
            $("#J_LinkBasket").css({"display": "none"});
            // $("#J_LinkBasket").removeAttr("onclick");
            // $("#J_LinkBasket").css({"background-color": "#ddd","border": "1px solid #ddd"});
          }
          stocksMsg = "<span style='color:#e5004f'>无货&nbsp;&nbsp;&nbsp;</span>";
          //没货，立即购买和手机购买隐藏 ，到货通知显示
          $("#nowBuy").css({"display": "none"});
          $("#phoneBuy").css({"display": "none"});
          $("#arrivalInform").css({"display": "block", "class": "tb-btn-buy tb-btn-sku"});
          $("#arrivalInform").attr("onclick", "productInformWindow();");
        } else if (retData == -1) {
          stocksMsg = "<span style='color:#e5004f'>该区域限售&nbsp;&nbsp;&nbsp;</span>";
          //没货，立即购买和手机购买隐藏 ，到货通知显示
          $("#nowBuy").css({"display": "none"});
          $("#phoneBuy").css({"display": "none"});
          $("#arrivalInform").css({"display": "block", "class": "tb-btn-buy tb-btn-sku"});
          $("#arrivalInform").attr("onclick", "productInformWindow();");
          $("#J_LinkBasket").removeAttr("onclick");
          $("#J_LinkBasket").css({"display": "none"});
          $("#J_LinkBasket").css({"background-color": "#ddd", "border": "1px solid #ddd"});
        } else {//有货
          //显示立即购买和手机购买，到货通知隐藏
          $("#nowBuy").css({"display": "block", "class": "tb-btn-buy tb-btn-sku"});
          // $("#phoneBuy").css({"display":"block","class":"tb-btn-phonebuy tb-btn-sku"});
          $("#arrivalInform").css({"display": "none"});
          if (retData < 10) {//库存少于10
            stocksMsg = "有货，仅剩" + retData + "件&nbsp;&nbsp;&nbsp;";
            $("#skuStock").html("剩余" + retData + "件");

          } else {
            stocksMsg = "有货，仅剩" + retData + "件&nbsp;&nbsp;&nbsp;";
            $("#skuStock").html("剩余" + retData + "件");
          }
          $("#J_LinkBuy").removeAttr("style");//隐藏立即购买按钮
          if (!isPresell && !isSeckill && !isAuction) {//非预售商品
            $("#J_LinkBuy").attr("onclick", "javascript:buyNow();");
            //启用加入购物车按钮,并变亮
            $("#J_LinkBasket").attr("onclick", "javascript:addShopCart();");
            $("#J_LinkBasket").removeAttr("style");
          } else {
            $("#J_LinkBuy").attr("onclick", "javascript:reserveNow();");
          }
        }
      }

      $("#skuStock").html(stocksMsg);
    }
  });
}

function distProd() {
  jQuery.ajax({
    url: contextPath + "/isUserLogin",
    type: 'get',
    async: false, // 默认为true 异步
    success: function (result) {
      if (result == "true") {
        $(".dist_action").hide();
        $("#distShareBtn").show();
      } else {
        login();
      }
    }
  });
}


//根据sku返回参与了那些活动
function bySkuActivity() {
  jQuery.ajax({
    url: contextPath + "/bySkuActivity",
    data: {
      "prodId": currProdId,
      "skuId": $("#currSkuId").val(),
    },
    type: 'post',
    async: true, //默认为true 异步
    dataType: 'json',
    success: function (result) {
      var rules = $("#skuActivity");
      if (result.prod.skuType != null && result.prod.skuType == "SECKILL" && result.seckill != null) {
        rules.html("<div class='spe-dic' id='group'>" +
          "<dt>秒杀</dt>" +
          "<dd style='width: 200px;padding-left: 21px;'><font style='font-weight: bold;' color='red'>秒杀价：¥" + result.seckill.seckillPrice + "</font>" +
          "<a style='color: #06c; font-family: SimSun;' href='/seckill/views/" + result.seckill.seckillId + "' target='_blank'>" +
          "&nbsp;&nbsp;&nbsp;立即参加>></a></dd>" +
          "</div>")
      } else if (result.prod.isGroup != null && result.prod.isGroup == "1" && result.group != null && result.group.status == "1") {
        rules.html("<div class='spe-dic' id='group'>" +
          "<dt>团购</dt>" +
          "<dd style='width: 200px;padding-left: 21px;'><font style='font-weight: bold;' color='red'>团购价：¥" + result.group.groupPrice + "</font>" +
          "<a style='color: #06c; font-family: SimSun;' href='/group/view/" + result.group.id + "' target='_blank'>" +
          "&nbsp;&nbsp;&nbsp;立即参加>></a></dd>" +
          "</div>")
      } else if (result.prod.skuType != null && result.prod.skuType == "AUCTION" && result.auctions != null) {
        var auctionsPrice = "当前竞拍价：¥" + result.auctions.curPrice;
        if (result.auctions.hideFloorPrice) {
          auctionsPrice = "拍卖价：¥" + result.auctions.floorPrice;
        }
        rules.html("<div class='spe-dic' id='group'>" +
          "<dt>拍卖</dt>" +
          "<dd style='width: 200px;padding-left: 21px;'><font style='font-weight: bold;' color='red'>" + auctionsPrice + "</font>" +
          "<a style='color: #06c; font-family: SimSun;' href='/auction/views/" + result.auctions.id + "' target='_blank'>" +
          "&nbsp;&nbsp;&nbsp;立即参加>></a></dd>" +
          "</div>")
      } else if (result.prod.skuType != null && result.prod.skuType == "PRESELL" && result.presell != null) {
        rules.html("<div class='spe-dic' id='group'>" +
          "<dt>预售</dt>" +
          "<dd style='width: 200px;padding-left: 21px;'><font style='font-weight: bold;' color='red'>预售价：¥" + result.presell.prePrice + "</font>" +
          "<a style='color: #06c; font-family: SimSun;' href='/presell/views/" + result.presell.id + "' target='_blank'>" +
          "&nbsp;&nbsp;&nbsp;立即参加>></a></dd>" +
          "</div>")
      } else {
        rules.html("");
      }
    }
  });
}


//收藏店铺
function collectShop(shopId) {
  jQuery.ajax({
    url: contextPath + "/isUserLogin",
    type: 'get',
    async: false, // 默认为true 异步
    success: function (result) {
      if (result == "true") {
        jQuery.ajax({
          url: contextPath + "/p/favoriteShopOverlay?shopId=" + shopId,
          async: false, // 默认为true 异步
          dataType: 'json',
          success: function (result) {
            if (result == "fail") {
              login();
            } else if (result == "OK") {
              layer.alert("收藏店铺成功", {icon: 1});
            } else if (result = "isexist") {
              layer.msg("您已收藏该店铺，无须重复收藏");
            }
          }
        });
      } else {
        login();
      }
    }
  });

}

//得判断是否为数字的同时，也判断其是不是正数
function isLogin() {
  if (loginUserName == "" || loginUserName == undefined || loginUserName == null) {
    return true;
  }
  return false;
}

function login() {
  layer.open({
    title: "登录",
    type: 2,
    id: "login",
    content: contextPath + '/loadLoginOverlay', //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
    area: ['440px', '530px']
  });
}

function getCoupon(couponId) {
  jQuery.ajax({
    url: contextPath + "/isUserLogin",
    type: 'get',
    async: false, // 默认为true 异步
    success: function (result) {
      if (result == "true") {
        $.ajax({
          url: contextPath + "/p/coupon/receive/" + couponId,
          type: 'post',
          async: false, //默认为true 异步
          dataType: "json",
          success: function (data) {
            if (data == "OK") {
              layer.alert("恭喜您领取成功", {icon: 1});
              /*setTimeout('skip()',1500);*/
            } else {
              layer.msg(data, {icon: 0});
              /*setTimeout('window.location.reload()',1500);*/
            }
          }
        });
      } else {

        layer.open({
          title: "登录注册",
          id: "login",
          type: 2,
          resize: false,
          content: [contextPath + '/loadLoginOverlay', 'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
          area: ['440px', '530px']
        });
      }
    }
  });
}


function isBlank(value) {
  return value == undefined || value == null || value == "";
}



