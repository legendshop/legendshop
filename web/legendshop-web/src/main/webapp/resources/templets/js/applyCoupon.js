$(".button").bind("click",function(){
	var couponId = $("#couponId").val();
	$.ajax({
		url: contextPath+"/p/coupon/receive/" + couponId,
		type:'post',
		async : false, //默认为true 异步
		dataType:"json",
		success:function(data){
			if(data=="OK"){
				layer.msg("恭喜您领取成功",{icon:1,time:500},function(){
					skip();
				});
			}else{
				layer.msg(data,{icon:2,time:500},function(){
					window.location.reload();
				});
			}
		}
	});

});

function skip(){
	window.location.href=contextPath+"/p/coupon?couPro=shop";
}
