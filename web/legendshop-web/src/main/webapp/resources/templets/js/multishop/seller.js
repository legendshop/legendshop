/** JS for seller center module */
var userCenter = {

	loadPageByAjax : function(url) {
		if(this.isUserLogined()){
			window.location.href = contextPath + url;
		}else{
			this.loadLoginPageByAjax();
		}
	},

	loadLoginPageByAjax : function() {
		window.location.href = contextPath + "/login";
	},

	//登录状态
	isUserLogined : function() {
		if('Y' == this.getCookie("LS")){
			return true;
		}else{
			return false;
		}
	},

	 getCookie  : function(name){
	       var cookieArray=document.cookie.split("; ");
		   for (var i=0;i<cookieArray.length;i++){
		      var arr=cookieArray[i].split("=");
		      if(arr[0]==name){
		    	return unescape(arr[1]);
		      }
		   }
		   return null;
	},

	refreshPageByAjax : function(url, JsonData) {
		if(this.isUserLogined()){
		$.post(contextPath + url, JsonData, function(data) {
			$('#rightContent').html(data);
		});
		}else{
			this.loadLoginPageByAjax();
		}
	},

	changeSubTab: function(name){
  	  	var jqElement = $("#" + name);
		this.changeSubNavClass(jqElement);
		$("#subNavLocation").html(jqElement.html());
	},

	changeSubNavClass : function(jqElement) {
		jqElement.addClass("focus");
	},

	changeNavLocation : function(text) {
		$("#subNavLocation").html(text);
	}
}

var publish = {
		doExecute: function (setting){
			userCenter.changeSubTab("publish"); //高亮菜单
			var obj = this;
			$("li.cc-tree-item").die('click').live('click',function() {
				$this = $(this);
				var sortId = $this.attr("id");
				$("#sortId").val(sortId);
				$("#nsortId").val('');
				$("#subsortId").val('');
				$("#brandId").val('');
				var targetUrl = contextPath + "/s/publish2/" +sortId ;
			 	var childNode = "category2";
			 	obj.retrieveNext(targetUrl, childNode);
				$("li.cc-selected").removeClass("cc-selected");
				$this.addClass("cc-selected");
				$("#category3").empty();
				$("#category4").empty();
				$("#selected-category").empty();
				$("#selected-category").append("<li>" + $this.text() + "<>");
	    });

				 $('.lebelinput').live('focus', function(){
						$(this).prev("label").hide();
					});
					//restoring input value on blrr if the input is left blank
					$('.lebelinput').live('blur', function(){
					   $this = $(this);
						if( $this.val()=='')
						{
							 $this.prev("label").show();
						}
					});

					$('#cc-cbox-filter1').die('keypress').live('keypress', function(e){
					   var valueText = $(this).val();
						 if(e.which == 13 && userCenter.isUserLogined()) {
					        		//call ajax
					               $.post( contextPath + "/admin/sort/loadsort",
								        {
								            "sortName": valueText
								          },
								        function(retData) {
								              	$("#sortList").html(retData);
										},'html');
					    }
					});

              $('#cc-cbox-filter2').die('keypress').live('keypress', function(e){
					   var valueText = $(this).val();
						 if(e.which == 13 && userCenter.isUserLogined()) {
					        		//call ajax
					               $.post(contextPath + "/admin/sort/loadnsort",
								        {
								           "sortId": $("#sortId").val(),
								            "nsortName": valueText
								          },
								        function(retData) {
								              	$("#nsortList").html(retData);
										},'html');
					    }
					});

			 $('#cc-cbox-filter3').die('keypress').live('keypress', function(e){
					   var valueText = $(this).val();
						 if(e.which == 13 && userCenter.isUserLogined()) {
					        		//call ajax
					               $.post(contextPath + "/admin/sort/loadsubsort",
								        {
								           "nsortId": $("#nsortId").val(),
								            "nsortName": valueText
								          },
								        function(retData) {
								              	$("#subSortList").html(retData);
										},'html');
					    }
					});

					$("#catePubBtn").click(function() {
					var sortId = $("#sortId").val();
					var nsortId = $("#nsortId").val();
					var subsortId = $("#subsortId").val();
					var brandId = $("#brandId").val();
						if(sortId == '' || nsortId == '' ||  subsortId == '' ){
							if($("#plsSel")[0] == undefined){
								$("#selected-category").append("<li id='plsSel'><font color='red'>  &nbsp;  -   请选择商品分类</font><>");
							}
						}else{
						var prodId = $("#prodId").val();
						var $mainform = $("#mainform");
						if(prodId != ''){
						   $mainform.attr('action', contextPath + "/admin/product/update/" + prodId);
						}else{
							$mainform.attr('action', contextPath + "/s/publishProd");
						}
						   $mainform.submit();
						}
					});
		},
		//获取下一页
		retrieveNext: function (targetUrl, childNode){
			if(childNode != null && childNode != '' && userCenter.isUserLogined()){
						 jQuery.ajax({
							url: targetUrl,
							type:'post',
							async : false, //默认为true 异步
							dataType : 'html',
							success:function(data){
								   $("#" + childNode).html(data);
							}
							});
				}
			}
}

var publish2 = {
		doExecute: function (setting){
			$("li#category2 li.cc-cbox-item").die('click').live('click',function() {
				$this = $(this);
				var nsortId = $this.attr("sortId");
				$("#nsortId").val(nsortId);
				$("#subsortId").val('');
				$("#brandId").val('');
				var targetUrl = contextPath + "/s/publish3/" +nsortId ;
				var childNode = "category3";
				publish.retrieveNext(targetUrl, childNode);
				var preSelected = $("li#category2 li.cc-selected");
				preSelected.removeClass("cc-selected");
				preSelected.removeAttr("aria-selected");
				$this.addClass("cc-selected");
				$this.attr("aria-selected","true");
				$("#selected-category li:nth-child(4)").remove();
				$("#selected-category li:nth-child(3)").remove();
				$("#selected-category li:nth-child(2)").remove();
				$("#selected-category").append("<li>  > " + $this.text() + "<>");
			});
		}
}

var publish3 = {
		doExecute: function (setting){
			$("li#category3 li.cc-cbox-item").die('click').live('click',function() {
				$this = $(this);
				var subsortId = $this.attr("sortId");
				$("#subsortId").val(subsortId);
				$("#brandId").val('');
				var targetUrl = contextPath + "/s/loadSortBrands/" +subsortId ;
				var childNode = "category4";
				//publish.retrieveNext(targetUrl, childNode);
				var preSelected = $("li#category3 li.cc-selected");
				preSelected.removeClass("cc-selected");
				preSelected.removeAttr("aria-selected");
				$this.addClass("cc-selected");
				$this.attr("aria-selected","true");
				$("#selected-category li:nth-child(4)").remove();
				$("#selected-category li:nth-child(3)").remove();
				$("#selected-category").append("<li>  > " + $this.text() + "<>");
			});
		}
}

var publish4 = {
		doExecute: function (setting){
			$("li#category4 li.cc-cbox-item").die('click').live('click',function() {
				$this = $(this);
				var preSelected = $("li#category4 li.cc-selected");
				preSelected.removeClass("cc-selected");
				preSelected.removeAttr("aria-selected");
				$this.addClass("cc-selected");
				$this.attr("aria-selected","true");
				$("#selected-category li:nth-child(4)").remove();
				$("#brandId").val($this.attr("id"));
				$("#selected-category").append("<li>  > " + $this.text() + "<>");
			});
		}
}

var friendLink = {
		doExecute : function(setting) {
			userCenter.changeSubTab("friendLink");
			this.bindLinkConfigure();
		},
		bindLinkConfigure : function() {
			var jqElement = $("#linkConfigure");
			jqElement.click(function() {
				userCenter.loadPageByAjax("/s/friendLink/load");
				userCenter.changeSubNavClass($("#friendLink"));
				userCenter.changeNavLocation("友情链接");
			});
		}
}

var prodComment = {
		doExecute : function(setting) {
			userCenter.changeSubTab("prodComment");
		}
}

var dashboard = {
		doExecute : function(setting) {
			//斑马条纹
	     	 $("#col1 tr:nth-child(even)").addClass("even");

	  		//斑马条纹
	     	 $("#col2 tr:nth-child(even)").addClass("even");
	     	  highlightTableRows("col1");
	 		  highlightTableRows("col2");
		}

}

var payType = {
		doExecute : function(setting) {
			userCenter.changeSubTab("payType");
		},
		loadpayConfigure: function (){
			sendData(contextPath + "/s/payType/load/");
		},

		payConfigure: function (payId){
			sendData(contextPath + "/s/payType/load/" + payId);
		},

		deleteById: function (obj, payId){
			var $this = this;
			 layer.confirm("确定删除 ?",
					 {
	      		 icon: 3
	      	     ,btn: ['确定','取消'] //按钮
	      	   },function () {
				 $this.deleteData(obj, contextPath + "/s/payType/delete/"+payId);
		     });
		},

		deleteData: function (obj, url){
			$.ajax({
			"url":url,
			type:'post',
			async : true, //默认为true 异步
			success:function(result){
			 window.location.href = contextPath + "/s/payType";
			}
			});
		},

		//页面的删除
		deleteAction: function (){
			var $this = this;
			//获取选择的记录集合
			selAry = document.getElementsByName("strArray");
			if(!checkSelect(selAry)){
			layer.alert('删除时至少选中一条记录!', {icon: 0});
			return false;
			}
			layer.confirm("删除后不可恢复, 确定要删除吗？", {
	      		 icon: 3
	      	     ,btn: ['确定','取消'] //按钮
	      	   },function () {
						var ids = [];
			  			for(i=0;i<selAry.length;i++){
							  if(selAry[i].checked){
							  ids.push(selAry[i].value);
							 }
						}
					var result = $this.deletePayType(ids);
					if('OK' == result){
						layer.msg('删除成功！', {icon: 1});
						sendData(contextPath + "/s/payType");
					}else{
						layer.msg('删除失败！', {icon: 2});
					}
				});

			return true;
		},

		deletePayType:	function (ids) {
			var result;
			var idsJson = JSON.stringify(ids);
			var ids = {"ids": idsJson};
			var url = contextPath + "/s/payType/deleteMultiple";
			jQuery.ajax({
			"url":url ,
			data: ids,
			type:'post',
			async : false, //默认为true 异步
		    dataType : 'json',
			success:function(retData){
				result = retData;
				}
			});

			return result;
		}
}

var payConfigure ={
		doExecute: function (setting){
			userCenter.changeSubTab("payType");
            //三级联动
	           $("select.combox").initSelect();
	          if($("#interfaceType").find("option").length > 1){
						 $("#interfaceType").show();
					 }else{
						$("#interfaceType").hide();
				 }
		},

		loadPayTypeList:function(){
			sendData(contextPath + "/s/payType");
	   }
}

var transportList = {
		doExecute: function (setting){
			userCenter.changeSubTab("transport");
		},

		loadById: function (id){
			sendData( contextPath + "/s/transport/load/"+id);
		},

		loadTransport:	function (){
			sendData(contextPath + "/s/transport/load");
		},

		deleteById:	function (id){
			$.ajax({
				url: contextPath + "/s/transport/delete/"+id,
				type:'post',
				dataType : 'json',
				async : true, //默认为true 异步
				success:function(result){
					if(result=="fail"){
						layer.msg('运费模版使用中， 无法删除！', {icon: 2});
					}else{
						sendData(contextPath + "/s/transport");
					}
				}
				});
		}
}

var shopSetting = {
		doExecute: function (setting){
			userCenter.changeSubTab("shopSetting");
		}
}

var domainSetting ={
		doExecute: function (setting){
			userCenter.changeSubTab("domainSetting");
		}
}

var themeSetting = {
		doExecute: function (setting){
			userCenter.changeSubTab("themeSetting");
		},


		useTheme:  function (themeName,themeUrl){
        	$.ajax({
         		url: contextPath + "/sveTheme",
         		data:{"themeName": themeName,"themeUrl":themeUrl},
         		type:'post',
         		async : true, //默认为true 异步
         		success:function(result){
         			if(result=="OK"){
         				layer.alert('保存成功', {icon: 1});
               		 sendData(contextPath + "/s/themeSetting");
         			}else{
         				layer.alert('保存失败', {icon: 2});
         			}
         		}
         		});
        }

}

var consult = {
		doExecute: function (setting){
			userCenter.changeSubTab("prodConsult");
			//出当前页
			var replyed = setting.replyed;
			$(".pagetab li").removeClass("on");
			if('' == replyed){
			    $("#allConsult").addClass("on");
			}else if('0' == replyed ){
			   $("#unReplyConsult").addClass("on");
			}else if('1' == replyed){
				$("#replyedConsult").addClass("on");
			}

			this.bindingEvent();
		},

		bindingEvent: function(){
			$("#allConsult").click(function() {
				sendData(contextPath + "/s/consult");
			});

			$("#replyedConsult").click(function() {
				sendData(contextPath + "/s/replyedConsult");
			});

			$("#unReplyConsult").click(function() {
				sendData(contextPath + "/s/unReplyConsult");
			});

		}

}

//店铺概况
var sales = {
	doExecute: function (setting){
		userCenter.changeSubTab("sales");
	}
}

//商品分析
var prodAnalysis = {
	doExecute: function (setting){
		userCenter.changeSubTab("prodAnalysis");
	}
}

//运营报告
var operatingReport = {
	doExecute: function (setting){
		userCenter.changeSubTab("operatingReport");
	}
}

//购买分析
var purchaseAnalysis = {
	doExecute: function (setting){
		userCenter.changeSubTab("purchaseAnalysis");
	}
}

//流量统计
var trafficStatistics = {
	doExecute: function (setting){
		userCenter.changeSubTab("trafficStatistics");
	}
}

//价格区间
var priceRange = {
	doExecute: function (setting){
		userCenter.changeSubTab("priceRange");
	}
}

//刷新页面
function refreshProdConsult(consId, answer){
	$("#answer_"+ consId).html(answer);
	$("#answerDiv_"+ consId).show();
}

function bindPage(name,url) {
	var jqElement = $("#"+ name);
	jqElement.unbind("click").click(function() {
		userCenter.loadPageByAjax(url);
	});
}

function sendData(url){
		window.location.href = url;
}

