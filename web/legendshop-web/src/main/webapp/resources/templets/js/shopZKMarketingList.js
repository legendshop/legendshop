$(document).ready(function() {
	userCenter.changeSubTab("zk_Marketing"); //高亮菜单
});

function onlineShopMarketing(_id,text){
	layer.confirm('确定要'+text+'该活动么？',{
		icon: 3
		,btn: ['确定','取消'] //按钮
	}, function () {
		window.location.href=contextPath+"/s/onlineShopMarketing/2/"+_id;
	});
}

function offlineShopMarketing(_id){
	layer.confirm('确定要终止该活动么？',{
		icon: 3
		,btn: ['确定','取消'] //按钮
	}, function () {
		window.location.href=contextPath+"/s/offlineShopMarketing/2/"+_id;
	});
}

function deleteShopMarketing(_id){
	layer.confirm('确定要删除活动么？',{
		icon: 3
		,btn: ['确定','取消'] //按钮
	}, function () {
		window.location.href=contextPath+"/s/deleteShopMarketing/2/"+_id;
	});
}

function suspendShopMarketing(_id){
    layer.confirm("确定要暂停该活动么？",{
        icon: 3
        ,btn: ['确定','取消'] //按钮
    }, function () {
        window.location.href=contextPath+"/s/suspendShopMarketing/2/"+_id;
    });
}