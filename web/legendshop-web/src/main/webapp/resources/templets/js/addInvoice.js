$(function(){

    /* 选择发票类型 */
    $(".invoice-type").click(function(){

        var type = $(this).attr("type");

        /* 点击增值税发票 */
        if (type == 'tax'){

            $(".tax").show();
            $(".common").hide();

            $("[type='tax']").addClass("on");
            $("[type='common']").removeClass("on");


        }
        /* 点击普通发票 */
        if (type == 'common'){

            $(".common").show();
            $(".tax").hide();

            $("[type='common']").addClass("on");
            $("[type='tax']").removeClass("on");
        }

    });

    /* 监听layerui表单 */
    layui.use(['layer', 'jquery', 'form',"layedit"], function () {
        var layer = layui.layer,
            $ = layui.jquery,
            form = layui.form,
            layedit = layui.layedit;

        /* 普票类型监听事件 */
        form.on('select(title)', function(data){

            /*console.log(data.elem); //得到select原始DOM对象
            console.log(); //得到被选中的值
            console.log(data.othis); //得到美化后的DOM对象*/

            var title = data.value;
            /* 个人抬头 */
            if (title == '1'){
                $(".invoice-hum-number").hide();
                $(".invoice-tips").hide();
                $("#title").val(title);

            }else{
                $(".invoice-hum-number").show();
                $(".invoice-tips").show();
                $("#title").val(title);
            }

        });

            //监听普通发票默认开关
            form.on('switch(commonInvoice)', function(data){

            if(this.checked){
                $(this).val("1");
            }else{
                $(this).val("0");
            }
        });
        //监听增值税发票默认开关
        form.on('switch(taxInvoice)', function(data){

            if(this.checked){
                $(this).val("1");
            }else{
                $(this).val("0");
            }
        });

        /* 校验普通发票表单 */
        $("#commonForm").validate({
            errorPlacement: function(error, element) {

                error.appendTo(element.next("label"));
            },
            rules: {
                company : { required: true,minlength: 2,maxlength:50},
                invoiceHumNumber : {required: true,isInvoiceHumNumber:true}
            },
            messages: {
                company: {required: "请输入抬头", minlength: "不能小于2个字符",maxlength: "不能超过50个字符"},
                invoiceHumNumber : {required: "请填写纳税人号"}
            }

        });
        /* 校验增值税发票表单 */
        $("#taxForm").validate({

            errorPlacement: function(error, element) {

                error.appendTo(element.next("label"));
            },
            rules: {
                company : { required: true,minlength: 2,maxlength:50},
                invoiceHumNumber : {required: true,isInvoiceHumNumber:true},
                registerAddr : {required: true,minlength: 2,maxlength:100},
                registerPhone : {required: true,checkMobile:true},
                depositBank : {required: true,minlength: 2,maxlength:100},
                bankAccountNum : {required: true,minlength: 2,maxlength:50}
            },
            messages: {
                company: {required: "请填写公司名称", minlength: "不能小于2个字符",maxlength: "不能超过50个字符"},
                invoiceHumNumber : {required: "请填写纳税人识别码"},
                registerAddr : {required: "请填写公司注册地址", minlength: "不能小于2个字符",maxlength: "不能超过100个字符"},
                registerPhone : {required: "请填写公司注册电话"},
                depositBank : {required: "请填写公司开户银行名称", minlength: "不能小于2个字符",maxlength: "不能超过100个字符"},
                bankAccountNum : {required: "请填写公司开户银行账号", minlength: "不能小于2个字符",maxlength: "不能超过50个字符"}
            }

        });


    });

    /* 编辑js */
    if (!isBlank(invoiceId)){

        if (invoiceType == 1){ /* 普通发票 */

            $(".common").show();
            $(".tax").hide();
            $("[type='common']").addClass("on");
            $("[type='tax']").removeClass("on");
            $("[type='tax']").addClass("notClick");

            if (title == 2){

                $(".invoice-hum-number").show();
                $(".invoice-tips").show();
            }

            $('select').attr('disabled', 'disabled');

        } else{/* 增值税发票 */

            $(".tax").show();
            $(".common").hide();

            $("[type='tax']").addClass("on");
            $("[type='common']").removeClass("on");
            $("[type='common']").addClass("notClick");

        }
    }

    jQuery.validator.addMethod("isInvoiceHumNumber", function(value, element) {

        return this.optional(element) || (checkInvoiceHumNumber(value));

    }, "请填写正确的纳税人识别码");

    jQuery.validator.addMethod("checkMobile", function(value, element) {

        var Reg = /^((0\d{2,3}-\d{7,8})|(1[35874]\d{9}))$/;

        return this.optional(element) || (Reg.test(value));

    }, "请填写正确的注册电话");


});

/* 回显开关状态 */
window.onload = function (){

    if (!isBlank(invoiceId)){

        /* 回显开关状态 */
        if (commonInvoice == 1) {
            var o = $(".layui-form-switch");
            o.prop("class","layui-unselect layui-form-switch layui-form-onswitch");
        }
    }
}


/* 保存或更新 */
function saveInvoice(){

    var invoiceType = $("div[class='invoice-type item on']").attr("type");

    /* 提交普通发票 */
    if (invoiceType == 'common') {

        var flag = $("#commonForm").valid();

        if (!flag){
            return ;
        }

        var title = $("#title").val();
        if (isBlank(title)){
            title = 1;
        }
        var commonInvoice = $("#commonInvoice").val();
        if (isBlank(commonInvoice)) {
            commonInvoice = 0;
        }

        var company = $("#common-company").val();
        var invoiceHumNumber = $("#common-invoiceHumNumber").val();
        var id = $("#id").val();


        var data = {
            "id": id,
            "type" : 1, // 普通发票
            "content" : "2", // 内容默认为明细
            "title": title,
            "commonInvoice" : commonInvoice,
            "company" : company,
            "invoiceHumNumber" : invoiceHumNumber,

        }

        $.ajax({
            url: contextPath + "/p/invoice/save",
            data:data,
            type:'post',
            async : false, //默认为true 异步
            dataType : 'json',
            success:function(result){

                if (result == 'OK') {

                    /* 调用父窗口函数*/
                    parent.saveOrUpdateInvoiceSuccess();
                }else {
                    layer.msg(result,{icon: 0})
                }

            }
        });
    }

    /* 提交增值税发票 */
    if (invoiceType == 'tax') {
        var flag = $("#taxForm").valid();

        if (!flag){
            return ;
        }

        var commonInvoice = $("#taxInvoice").val();
        if (isBlank(commonInvoice)) {
            commonInvoice = 0;
        }

        var company = $("#tax-company").val();
        var invoiceHumNumber = $("#tax-invoiceHumNumber").val();
        var registerAddr = $("#registerAddr").val();
        var registerPhone = $("#registerPhone").val();
        var depositBank = $("#depositBank").val();
        var bankAccountNum = $("#bankAccountNum").val();
        var id = $("#id").val();

        var data = {

            "id" : id,
            "type" : 2, // 增值税发票
            "content" : "2", // 内容默认为明细
            "title": "",
            "commonInvoice" : commonInvoice,
            "company" : company,
            "invoiceHumNumber" : invoiceHumNumber,
            "registerAddr" : registerAddr,
            "registerPhone" : registerPhone,
            "depositBank" : depositBank,
            "bankAccountNum" : bankAccountNum,

        }

        $.ajax({
            url: contextPath + "/p/invoice/save",
            data:data,
            type:'post',
            async : false, //默认为true 异步
            dataType : 'json',
            success:function(result){

                if (result == 'OK') {

                    /* 调用父窗口函数*/
                    parent.saveOrUpdateInvoiceSuccess();
                }else {
                    layer.msg(result,{icon: 0})
                }

            }
        });

    }

}

/* 取消 */
function cancel(){
    parent.cancelAddInvoice();
}

/* 检查纳税人识别码 */
function checkInvoiceHumNumber(invoiceHumNumber) {
    var regArr = [/^[\da-z]{10,15}$/i, /^\d{6}[\da-z]{10,12}$/i, /^[a-z]\d{6}[\da-z]{9,11}$/i, /^[a-z]{2}\d{6}[\da-z]{8,10}$/i, /^\d{14}[\dx][\da-z]{4,5}$/i, /^\d{17}[\dx][\da-z]{1,2}$/i, /^[a-z]\d{14}[\dx][\da-z]{3,4}$/i, /^[a-z]\d{17}[\dx][\da-z]{0,1}$/i, /^[\d]{6}[\da-z]{13,14}$/i],
        i, j = regArr.length;
    for (var i = 0; i < j; i++) {
        if (regArr[i].test(invoiceHumNumber)) {
            return true;
        }
    }
    //纳税人识别号格式错误
    return false;
}


/* 空判断 */
function isBlank(_value){
    if(_value==null || _value=="" || _value==undefined){
        return true;
    }
    return false;
}
