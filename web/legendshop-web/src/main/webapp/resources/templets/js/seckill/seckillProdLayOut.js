$(function(){
    //改变加号鼠标指针
    $(".chooseSku").mouseover (function () {
      $(this).css("cursor","pointer");
    })
    // 标识不可选的商品
    $("tr.first").each(function(){
        var prodId = $(this).attr("prodId");
        var flag = isAttendActivity(prodId);

        if(!flag){

            $(this).find("div").css("display","block");
            $(this).find("div").children().html("无可用sku");

            $(this).find("input").addClass("unChecked");
            $(this).find("input").attr("disabled","disabled");
            $(this).find("input").next().css("background","#eee");
            $(this).find("label").removeClass("radio-wrapper");
        }
    });

    //切换选择商品
    $("input:radio[name='prodId']").change(function(){
        $(".check-arr").hide();
        parent.skuList=new Array();
        var prodId = $(this).val();

        //判断商品互斥（是否已参与其他活动）
        var flag = isAttendActivity(prodId);
        if(!flag){
            layer.msg("该商品全部sku已参加活动",{icon:0});

            this.checked = false;
            return;
        }

        $("input:radio[name='prodId']").each(function(){

            if(this.checked){
                $(this).parent().parent().addClass("radio-wrapper-checked");
                $(".addSku").hide();
                $(".chooseSku").removeClass("chooseSkuList")
                $(".skuNum").text("选择规格");
                $(this).parents(".prodCheck").siblings(".chooseSku").find(".addSku").show();
                $(this).parents(".prodCheck").siblings(".chooseSku").addClass("chooseSkuList");
            }else{
                $(this).parent().parent().removeClass("radio-wrapper-checked");
            }
        });
    });

});

// 分页搜索
function pager(curPageNO){
    document.getElementById("curPageNO").value=curPageNO;
    document.getElementById("form1").submit();
}

// 确认选择商品
function confirm(){

    var prodId = $("input:radio[name='prodId']:checked").val();

    if(isBlank(prodId)){
        layer.msg("请先选择参与活动的商品",{icon:0});
        return;
    }

    if(isBlank(parent.skuList)){
        layer.msg("请先选择参与活动商品的规格",{icon:0});
        return;
    }
    // 确认后逻辑 TODO
    jQuery.ajax({
        url: contextPath + "/s/seckillActivity/checkSeckillProdSku/"+prodId,
        type:'post',
        async : false, //默认为true 异步
        dataType : 'json',
        success:function(result){
            if(result){
                $.get("/s/seckillActivity/seckillProdSku/"+parent.skuList,function(data){
                    parent.skuList = new Array();
                    parent.$(".prodList").html(data);
                    parent.$(".check-bto").show();
                    parent.$(".check-bto").css("display","inline-block");
                    var index = parent.layer.getFrameIndex('showProdlist'); //先得到当前iframe层的索引
                    parent.layer.close(index);
                })
            }else{
                layer.alert('添加失败,请检查该商品的SKU是否全部下线或者全部无库存', {icon: 2});
            }
        }
    });

}

// 取消
function cancel(){

    var index = parent.layer.getFrameIndex('showProdlist'); //先得到当前iframe层的索引
    parent.layer.close(index);
}

// 是否参加其他活动
function isAttendActivity(prodId){
    var flag = true;
    $.ajax({
        url : contextPath + "/s/seckillActivity/isAttendActivity/" + prodId ,
        async : false,
        dataType : "JSON",
        success : function(result){
            flag = result;
        }
    });
    return flag;
}
function isBlank(value){
    return value == undefined ||  value == null || value == "";
}

//点击选择规格限时sku列表
$(".first").on("click",".chooseSkuList",function(){
    $(".skuAll").prop("checked",false);
    $(".skuAll").parent().parent().removeClass("checkbox-wrapper-checked");
    var prodid = $(this).siblings(".prodCheck").find("input:radio").val();
    $.post("/s/loadSkuList",{prodId:prodid},function (data) {
        if (data!=null){
          $(".skuTr").remove();
            $(data.skuList).each(function(index,dom){
                var cnProperties = dom.cnProperties;
                if (dom.cnProperties==""||dom.cnProperties==null){
                    cnProperties="暂无";
                }

                var tr = "<tr class='skuTr'>\n" +
                    "\t\t\t\t\t\t\t\t<td class='skuCheck'>\n" +
                    "\t\t\t\t\t\t\t\t\t<label class='checkbox-wrapper'>\n" +
                    "\t\t\t\t\t\t\t\t\t\t<span class='checkbox-item'>\n" +
                    "\t\t\t\t\t\t\t\t\t\t\t<input type='checkbox' id='checkbox' name='skuOne' class='checkbox-input skuOne'  value="+dom.skuId+">\n" +
                    "\t\t\t\t\t\t\t\t\t\t\t<input type='hidden' class='skuType'  value="+dom.skuType+">\n" +
                    "\t\t\t\t\t\t\t\t\t\t\t<input type='hidden' id='refProd'  value="+prodid+">\n" +
                    "\t\t\t\t\t\t\t\t\t\t\t<span class='checkbox-inner'></span>\n" +
                    "\t\t\t\t\t\t\t\t\t\t</span>\n" +
                    "\t\t\t\t\t\t\t\t\t</label>\n" +
                    "\t\t\t\t\t\t\t\t</td>\n" +
                    "\t\t\t\t\t\t\t\t<td style='text-align: center'>"+cnProperties+"</td>\n" +
                    "\t\t\t\t\t\t\t\t<td >"+dom.price+"</td>\n" +
                    "\t\t\t\t\t\t\t\t<td  class='skuStocks'>"+dom.stocks+"</td>\n" +
                    "</tr>"
                $(".skuList").append(tr);
                if (parent.skuList.indexOf($(tr).find(".skuOne").val())!=-1){
                    $(".skuList").find(".skuOne:eq("+index+")").attr("checked","checked");
                    $(".skuList").find(".skuOne:eq("+index+")").parent().parent().addClass("checkbox-wrapper-checked");
                }
                $(".skuAll").prop("checked",$(".skuOne:not(:checked)").size()>0?false:true);
                if($(".skuOne:not(:checked)").size()<=0){
                    $(".skuAll").prop("checked",true);
                    $(".skuAll").parent().parent().addClass("checkbox-wrapper-checked");
                }else{
                    $(".skuAll").prop("checked",false);
                    $(".skuAll").parent().parent().removeClass("checkbox-wrapper-checked");
                }
            })
            $(".check-arr").css("z-index","2");
            $(".check-arr").show();
        }
    },"json")
})

//切换选择商品的规格
$(window).on("click",".skuOne",function () {
    var skuType = $(this).next(".skuType").val();
    //判断商品互斥（是否已参与其他活动）
    var flag = skuIsAttendActivity(skuType);
    if (flag != "false") {
        layer.msg("该商品已参加" + flag + "活动", {icon: 0});
        this.checked = false;
        return;
    }

    $(".skuOne").each(function () {

        if (this.checked) {
            $(this).parent().parent().addClass("checkbox-wrapper-checked");
        } else {
            $(this).parent().parent().removeClass("checkbox-wrapper-checked");
        }
    });
});

//sku全选
$(".skuAll").click(function(){
    if($(this).attr("checked")){
        $(this).parent().parent().addClass("checkbox-wrapper-checked");
        $(".skuOne").each(function(){
            var skuType = $(this).next(".skuType").val();
            //判断商品互斥（是否已参与其他活动）
            var flag = skuIsAttendActivity(skuType);
            if (flag != "false") {
                layer.msg("该商品部分商品已参加营销活动", {icon: 0});
                this.checked = false;
                return;
            }
            $(this).attr("checked",true);
            $(this).parent().parent().addClass("checkbox-wrapper-checked");
        });
    }else{
        $(".skuOne").each(function(){
            $(this).attr("checked",false);
            $(this).parent().parent().removeClass("checkbox-wrapper-checked");
        });
        $(this).parent().parent().removeClass("checkbox-wrapper-checked");
    }
});

// 是否参加其他活动
function skuIsAttendActivity(skuType) {
    var flag = "false";
    if (skuType=="GROUP"){
        flag = "团购"
    }else if (skuType=="MERGE"){
        flag = "拼团";
    }else if (skuType=="SECKILL"){
        flag = "秒杀";
    }else if (skuType=="AUCTION"){
        flag = "拍卖";
    }else if (skuType=="PRESELL"){
        flag = "预售";
    }
    return flag;
}

//商品选择关闭按钮
$(".close").on("click",function(){
    $(".check-arr").hide();
})

//sku选择提交事件
$(".skuSubmit").on("click",function(){
    parent.skuList=new Array();
    $(".skuOne:checked").each(function(){
        parent.skuList.push(this.value)
    });
    var prodid = $("#refProd").val();
    if ($(".skuOne:checked").size()>0){
        $(":radio:checked[value='"+prodid+"']").parents(".first").find(".skuNum").text("已选规格("+$(".skuOne:checked").size()+")");
    }else {
        $(":radio[value='"+prodid+"']").parents(".first").find(".skuNum").text("选择规格");
    }
    $(".check-arr").hide();
})
