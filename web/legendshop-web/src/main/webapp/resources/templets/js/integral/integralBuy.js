		function buy() {
		  
		  if(isBlank(count) || isBlank(prodId) || isBlank(token) ){
		     layer.alert("请求参数不正确,请重新购买商品",{icon: 0});
		     return false;
		  }
		  
		  var addressId=$("input[name='address']:checked").val();
		  if(isBlank(addressId)){
		     layer.alert("请选择收货人信息",{icon: 0});
		     return false;
		  }
		  
		  var password = $("#pay-password").val();
		  if(isBlank(password)){
			  layer.alert("请先输入支付密码",{icon: 0});
			  return false;
		  }
		  
		  $("#submit").css("background-color","#cccccc");
		  $("#submit").css("disable","true");
		  
		  $.ajax({
	            type:'POST',
	            url:contextPath+'/p/integral/cart/submit/'+prodId,
	            data:{"count":count,"token":token,"addressId":addressId,"password":password},
	            dataType:'json',
	            error : function(XMLHttpRequest, textStatus, errorThrown) {
                    layer.alert("下单出现异常，请稍候重试！",{icon: 2});
                    return false;
                },
	            success:function(retData){
	                if(retData.result=="false"){
							var code=retData.code;
							var message=retData.message;
							if(!isBlank(code)){
								if("ORDER_ADDRESS"==code){
									layer.alert("请设置您的收货地址？",{icon: 0},function(){
										window.location.href = contextPath+"/p/receiveaddress";
									})
								}
							}else{
								layer.alert(message,{icon: 2});
							}
							
						$("#submit").css("background-color","#CD2626");
		                $("#submit").css("disable","false");
		  
							
					}else if(retData.result=="true"){
					    layer.msg("积分订单兑换成功！",{icon: 1},function(){
					    	parent.window.location.href = contextPath+"/p/integral/list";
					    });
						 return;
					}
	            }
           });
		}
		
		
		function isBlank(_value){
			   if(_value==null || _value=="" || _value==undefined){
			     return true;
			   }
			   return false;
		}
