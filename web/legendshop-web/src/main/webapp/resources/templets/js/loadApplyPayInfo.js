function validateForm(){
     var param = new Object();
     param.errorCount = 0;
     param.focus = false;
		validateNullFiled($('#bankCode'),param, 'bankCodeNote', '请选择');
		validateNullFiled($('#provinceid'),param,'areaNote','请选择');
		validateNullFiled($('#cityid'),param,'areaNote','请选择');
		validateNullFiled($('#areaid'),param,'areaNote','请选择');
		validateNullFiled($('#txtbranch'),param, 'txtbranchNote', '请输入分行信息');
		validateNullFiled($('#bankUserName'), param, 'bankUserNameNote','请输入银行用户姓名');
		validateNullFiled($('#bankAccount'),param,'bankAccountNote','请输入用户银行账号');
		validateBankAccountFiled($('#bankAccount'),param,'bankAccountNote','请输入用户银行账号');
		
		validateBankAccountAgain(param);
		return param.errorCount;
}

function validateBankAccountFiled(obj, param, name, description){
	var nameNote = $('#' + name);
	if(param.errorCount == 0){
		if(obj.val().length < 9 || obj.val().length > 25){
			 param.errorCount =  param.errorCount + 1;
			description="请正确输入银行卡卡号";
			 nameNote.removeClass("hide");
		     if(!param.focus){
		     	obj.focus();
		     	param.focus = true;
		     }
		     nameNote.html(description);
		     return ;
		}else{
	     	nameNote.addClass("hide");
	     }
	 }
}
function validateBankAccountAgain(param){
	
	var  bankAccount=$("#bankAccount").val();
	var bankAccountAgain=$("#bankAccountTwo").val();
	if(param.errorCount == 0){
		if(bankAccount!=bankAccountAgain){
			 param.errorCount =  param.errorCount + 1;
			description="两次银行卡账号不一致！";
			$("#bankAccountTwoNote").removeClass("hide");
		     if(!param.focus){
		    	 $("#bankAccountTwo").focus();
		     	param.focus = true;
		     }
		     $("#bankAccountTwoNote").html(description);
		     return ;
		}else{
			 $("#bankAccountTwoNote").addClass("hide");
		}
	}
	
}

function addPayInfo(){
	if(validateForm() == 0){
	  var data = {
	    	"repayUserifoId": $("#id").val(),
	    	"bankCode": $("#bankCode").find("option:selected").text(),
         	"provinceid": $("#provinceid").val(),
         	"cityid": $("#cityid").val(),
         	"areaid": $("#areaid").val(),
         	"txtbranch": $("#txtbranch").val(),
         	"bankUserName": $("#bankUserName").val(),
         	"bankAccount": $("#bankAccount").val()
         };
	  
	 	$.ajax({
			url: contextPath + "/p/saveApplyPayInfo" , 
			data: data,
			type:'post', 
			dataType : 'json', 
			async : true, //默认为true 异步   
			success:function(result){
			    if(result == 'OK'){
			      layer.msg('保存成功',{icon:1,time:800},function(){
			    	  parent.window.loadApyInfo();
			    	  layer.closeAll();
			      });
				
			    }else if(result == 'MAX'){
			    	 layer.msg('您已经创建了3个收货地址',{icon:0});
			    }else{
			     layer.msg('保存失败',{icon:2});
			    }
			}
			});
	}

}

	function delApplyPayInfo(id){
		layer.confirm("确定要删除退款帐号信息吗？", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   $.ajax({
					"url":contextPath + "/p/delApplyPayInfo/"+ id , 
					type:'post', 
					async : true, //默认为true 异步   
					 success:function(result){
						layer.msg('删除收货地址成功',{icon:1,time:500},function(){
							parent.window.loadApyInfo();
						});
					 	}
					});
		   });
	}



function validateNullFiled(obj, param, name, description){
	if(param.errorCount == 0){
	 	var nameNote = $('#' + name);
		if(obj.val().length == 0){
				   param.errorCount =  param.errorCount + 1;
			     nameNote.removeClass("hide");
			     if(!param.focus){
			     	obj.focus();
			     	param.focus = true;
			     }
			     nameNote.html(description);
			     }else{
			     	nameNote.addClass("hide");
			     }
	}

}

