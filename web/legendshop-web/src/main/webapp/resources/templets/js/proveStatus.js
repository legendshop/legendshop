//倒计时, need cookies
function onTimer(remains) {
	var sendMobileCode = $("#sendMobileCode");
	if (remains == 0) {
		$("#sendMobileCodeDiv").show();
		$("#sendMobileCode").unbind("click").bind("click",function(){ sendSMSCode(); });
		$("#sendMobileCode").val("获取短信校验码");
		$("#sendMobileCode").removeAttr("disabled");
		wait = interVal;
		window.clearTimeout(timer);
	} else {
		$("#sendMobileCodeDiv").hide();
		$("#sendMobileCode").val(wait+"秒后重新发送");
		var remains = wait--;
		timer = setTimeout("onTimer(" + remains +")",1000);
	}
}

function calRemainTime(){
  if(wait!= null && wait !=interVal){
	   	var sendMobileCode = $("#sendMobileCode");
		$("#sendMobileCodeDiv").hide();
		$("#code").removeAttr("disabled");
		$("#sendMobileCode").removeAttr("disabled");
		$("#sendMobileCode").unbind("click");
	}else{
		wait = interVal;
	}
}

//发送验证短信
function sendSMSCode(){
	var timer;
	var name = $("#userName").val();
	var randNum = $("#randNum").val();
	if(validateRandNum(contextPath)){
		$.ajax({
			url:contextPath+"/sendSMSCode", 
			data:{"userName":name,"mobile":phone,"randNum":randNum},
			type:'post', 
			dataType : 'json', 
			async : false,   
			success:function(retData){
				var sendMobileCode = $("#sendMobileCode");
				sendMobileCode.unbind("click");
				if(retData == 'OK'){
					layer.msg('短信发送成功',{icon:1},function(){
						$("#code").removeAttr("disabled");
		      			$("#sendMobileCode").attr("disabled",true); 
						timer = setTimeout("onTimer(" + interVal + ")",1000);
					});
	      			
				}else{
					$("#code").attr("disabled",true); 
					layer.msg('验证码不正确',{icon:0});
				}
			}
			});	
	}
	
	return timer;
}

//发送验证邮件
function sendFindPwdEmail(){
	var name = $("#userName").val();
	$.ajax({
		url:contextPath+"/confirmationMail",
		data:{"userName": name},
		type:'post',  
		async : true,   
		success:function(retData){
			 $("#entry").html(retData);
		}
		});	
}
