function isBlank(_value){
   if(_value==null || _value=="" || _value==undefined){
     return true;
   }
   return false;
}
/**
	获取文件名
*/
function getFile(obj,inputName){
	var fileName = $(obj).val();
	var index = fileName.lastIndexOf(".");
	var suffix = fileName.substring(index).toLowerCase();
	if(!isBlank(fileName) && (".xlsx"==suffix||".xls"==suffix)){
		$("input[name='"+inputName+"']").val(fileName);
	}
	else{
		layer.msg("文件格式不对，请选择xls或xlsx格式！",{icon:0});
	}
}
/**
	上传Excel
*/
  function save(){
	var fileName = $(".textbox").val();
	var index = fileName.lastIndexOf(".");
	var suffix = fileName.substring(index).toLowerCase();
	if(isBlank(fileName)){
		layer.msg("文件为空",{icon:0});
		return false;
	}

	if(suffix!=".xlsx" &&  suffix!=".xls"){
		layer.msg("文件格式不对，请选择xls或xlsx格式！",{icon:0});
		return false;
	}
	var formData = new FormData($("#importForm")[0]);

	$.ajax({
			url: contextPath+"/s/importOrderByExcel",
            timeout : 300000,
			type: 'post',
            data:formData,
            processData: false,
   		    contentType: false,
            dataType: 'json',
            beforeSend: function(xhr){
    			$("body").showLoading();
    		},
    		complete: function(xhr,status){
   				$("body").hideLoading();
    		},
            success: function(result){
            	if("OK"==result){
            	     importSuc();
            	}else{
           	    	layer.msg(result,{icon:2});
            	}
            }
           	 });
  }

  /**
   * 导入成功
   */
  function importSuc(){
	  layer.confirm("订单导入成功！", {
			 icon: 1
		     ,btn: ['前往订单列表查看','继续导入'] //按钮
		   }, function(){
			   parent.location.href=contextPath+"/s/ordersManage";
		   });
  }

  /**
   * 关闭layer窗口
   */
  function closeLayer(){
	  var index = parent.layer.getFrameIndex("imporProd"); //先得到当前iframe层的索引
	  parent.layer.close(index); //再执行关闭
  }

  function isEmpty(param) {
	  if(param == null || param == "" || param == undefined) {
		  return true;
	  }
	  return false;
  }
