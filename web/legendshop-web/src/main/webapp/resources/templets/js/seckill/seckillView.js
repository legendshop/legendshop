jQuery(document).ready(function($) {
  //店铺分数
  var shopScore=$("#shopScoreNum").text();
  if(shopScore==null){
    shopScore=0;
  }
  $(".scroll_red").css("width",(shopScore/5)*100+"%");

  var liSelecteds = $(".tb-meta .tb-prop").find(".tb-selected").get();
	 if(isBlank(liSelecteds) || liSelecteds.length == 0){
		 var seckPrice=Number(skuDtoList[0].seckPrice);
		 var cash=Number(skuDtoList[0].price);
		 currSkuId=Number(skuDtoList[0].skuId);
		 $("#seckPrice").html(seckPrice.toFixed(2));
		 $("#prodCash").html(cash.toFixed(2));
	}else{
		for ( var i = 0; i < liSelecteds.length; i++) {
			checkFollowingPropVal($(liSelecteds[i]).parent().attr("index"));
			handleSkuMessage();
			getPropValImgs($(liSelecteds[i]).attr("valId"));
		}
	}

	//点击SKU项
	$("ul[id^='prop_'] li:not(.tb-selected):not(.tb-out-of-stock)").live("click", function() {
		clickPropVal(this);
		if(_switch==1){
			showSeckill(); //
		}
	});


	//点击Tap
	$(".pagetab2 ul li").click(function() {
		$(this).addClass("on").siblings().removeClass("on");
		//$(".right210_m .detail div:gt(0)").hide();
		var id = $(this).attr("id");
		$("#" + id + "Panel").show();

	});

	initCheck();

});

function isBlank(value){
	return value == undefined ||  value == null || value == "";
}

/**
 * 根据SKU判断后面的属性值，不可选的属性值 灰掉
 */
function checkFollowingPropVal(propIndex){
	var id = Number(propIndex);
	  while($("#prop_"+id).length!=0){
		  var aList = $("#prop_"+id+" a").get();
		  for(var i=0;i<aList.length;i++){
			  var kv = $(aList[i]).parent().attr("data-value");
			  var has = false;
			  var newSkus = getSelectedSkus(id-1);
			  for(var j=0;j<newSkus.length;j++){
				  if(newSkus[j].properties.indexOf(kv)>=0 && newSkus[j].status==1){
					  has = true;
					  break;
				  }
			  }
			  if(!has){
				  $(aList[i]).parent().removeClass("li-selected");
				  $(aList[i]).parent().children("i").remove();
				  $(aList[i]).parent().addClass("li-hidden");
				  $(aList[i]).parent().removeClass("li-show");
			  }else{
				  $(aList[i]).parent().addClass("li-show");
				  $(aList[i]).parent().removeClass("li-hidden");
			  }
		  }
		  id++;
	  }
}


/**
 * 计算选中sku的价格和促销信息
 */
function calculateSkuPrice(){
	var kvList = [];
	var selectedList = $(".li-selected").get();
	for(var k=0;k<selectedList.length;k++){
		kvList[k]=$(selectedList[k]).attr("data-value");
	}
	for(var i=0;i<skuDtoList.length;i++){
		var isThisSku = true;
		for(var j=0;j<kvList.length;j++){
			if(skuDtoList[i].properties.indexOf(kvList[j])<0){
				isThisSku = false;
				break;
			}
		}
		if(isThisSku){
			$("#currSkuId").val(skuDtoList[i].skuId);
			if(skuDtoList[i].name!=null && skuDtoList[i].name!=''){
				$("#prodName").html(skuDtoList[i].name);
			}else{
				$("#prodName").html(prodName);
			}
			var seckPrice=Number(skuDtoList[i].seckPrice);
			var prodCash=Number(skuDtoList[i].price);
			$("#seckPrice").html(seckPrice.toFixed(2));
			$("#prodCash").html(seckPrice.toFixed(2));
			break;
		}
	}
}


//点击SKU项
function clickPropVal(obj){
		$(obj).addClass("tb-selected").siblings().removeClass("tb-selected");
	    var propIndex = $(obj).parent().attr("index");
		handleSkuMessage(propIndex);
		getPropValImgs($(obj).attr("valId"));
}

function remainTimeCountDown(){
	if(runTime>0){
		runTime = runTime - 1000;
		var timeText = timeBetweenText();
		$(".pm-status-time").html(timeText);
	}
}


//显示商品详情
function showProdContent(){
	$("#prodContentPanel").show();
	$("#seckillPanel").hide();
}
//显示秒杀规则
function showSeckillContent(){
	$("#seckillPanel").show();
	$("#prodContentPanel").hide();
}
var allSelected = true;

/**
 * 处理sku信息
 */
function handleSkuMessage(propIndex){
	  propIndex = Number(propIndex);
	  var id = Number(propIndex);
	  while($("#prop_"+id).length!=0){
		  var aList = $("#prop_"+id+" li").get();
		  for(var i=0;i<aList.length;i++){
			  var kv = $(aList[i]).attr("data-value"); //data-value="269:831"
			  var has = false;
			  var newSkus = getSelectedSkus(id-1);
			  for(var j=0;j<newSkus.length;j++){
				  if(newSkus[j].properties.indexOf(kv)>=0 && newSkus[j].status==1){
					  if(Number(newSkus[j].seckStock)>0){
						  has = true;
						  break;
					  }
				  }
			  }
			  if(!has){
				  $(aList[i]).removeClass("tb-selected");
				  $(aList[i]).addClass("tb-out-of-stock");
			  }else{
				  $(aList[i]).removeClass("tb-out-of-stock");
			  }
		  }
		  id++;
	  }
	  $(".tb-meta dl").removeClass("no-selected");

	  //判断选择的SKU 是否与预定的SKU项一致
	  if($(".tb-selected").length==$(".tb-metatit").length){
			var kvList = [];
			var selectedList = $(".tb-selected").get();
			for(var k=0;k<selectedList.length;k++){
				kvList[k]=$(selectedList[k]).attr("data-value");
			}
			for(var i=0;i<skuDtoList.length;i++){
				var isT = true;
				for(var j=0;j<kvList.length;j++){
					if(skuDtoList[i].properties.indexOf(kvList[j])<0){
						isT = false;
						break;
					}
				}
				if(isT){
					currSkuId=skuDtoList[i].skuId;
					if(!isEmpty(skuDtoList[i].name)){
						$("#prodName").html(skuDtoList[i].name);
					}else{
						$("#prodName").html(prodName);
					}
					var seckPrice=Number(skuDtoList[i].seckPrice);
					var cash=Number(skuDtoList[i].price);
					$("#seckPrice").html(seckPrice.toFixed(2));
					$("#prodCash").html(cash.toFixed(2));
					break;
				}
			}
			allSelected=true;
		}else{
			var dlList = $(".tb-meta dl").get();
			for(var i=0;i<dlList.length;i++){
				if($(dlList[i]).find(".tb-selected").length==0){
					$(dlList[i]).addClass("no-selected");
				}
			}
			allSelected = false;
	}


}


//获取属性值图片
function getPropValImgs(valId){
	 for(var i=0;i<propValueImgList.length;i++){
		  if(propValueImgList[i].valueId==valId){
			  var imgList = propValueImgList[i].imgList;
			  var str = "";
			  for(var j=0;j<imgList.length;j++){
				  str+= "<li onmouseover='viewPic(this);' data-url='"+imgList[j]+"' class='img-default'>";
				  str+="<table cellpadding='0' cellspacing='0' border='0'><tr><td height='65' width='65' style='text-align: center;'>";
				  str+= "<img src='"+imgPath3.replace('PIC_DEFAL', imgList[j])+"' style='max-width:65px;max-height:65px;'  />";
				  str+="</td></tr></table>";
				  str+= "</li>";

			  }
			  $(".simgbox .simglist").html(str);
			  viewPic($(".simgbox .simglist li").get(0));
			  $("#spec-list").infiniteCarousel();
			  break;
		  }
	  }
}



/**
 * 获取选择的SKU值
 * @param propIndex
 * @returns {Array}
 */
function getSelectedSkus(propIndex){
	var selectedLiList = [];
	  var selIndex = 0;
	  for(var h=0;h<=propIndex;h++){
		  if($("#prop_"+h+" .tb-selected").length!=0){
			  selectedLiList[selIndex]=$("#prop_"+h+" .tb-selected").get(0);
			  selIndex++;
		  }
	  }

	  var index = 0;
	  var newSkus = [];
	  for(var i=0;i<skuDtoList.length;i++){
		  var has = true;
		  for(var j=0;j<selectedLiList.length;j++){
			  var kv = $(selectedLiList[j]).attr("data-value");
			  if(skuDtoList[i].properties.indexOf(kv)<0){
				  has = false;
				  break;
			  }
		  }
		  if(has){
			  newSkus[index]=skuDtoList[i];
			  index++;
		  }
	  }
	  return newSkus;
}
//客服链接
$("#loginIm").click(function () {
  var user_name = $("#user_name").val();
  var user_pass = $("#user_pass").val();
  if (user_name == '' || user_pass == '') {
    login();
  } else {
    window.open(contextPath + "/p/im/index/" + currSkuId, '_blank');
  }
});



