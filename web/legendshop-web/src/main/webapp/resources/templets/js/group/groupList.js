$(document).ready(function () {
	userCenter.changeSubTab("groupManage"); //高亮菜单
});

// 分页搜索
function pager(curPageNO) {
	document.getElementById("curPageNO").value = curPageNO;
	document.getElementById("searchGroup").submit();
}

$(".question-mark").mouseover(function () {
  var auditOpinion = $("#auditOpinion").val();
  var html = "<ul><li>" + auditOpinion + "</li></ul>";
  $("#history").html(html);
  $("#opinion").css("display", "block")
});

$(".question-mark").mouseout(function () {
  $("#history").html("");
  $("#opinion").css("display", "none")
});


// 终止活动
function offlineShopGroup(_id) {
	layer.confirm("确定要终止该活动吗？终止活动后，未成团的订单货款将全部原路返回", {
		icon: 3
		,btn: ['确定','取消'] //按钮
	},function () {
		$.ajax({
			type: "POST",
			//提交的网址
			url: contextPath + "/s/group/updateStatus/" + _id ,
			//提交的数据
			async: false,
			//返回数据的格式
			datatype: "json",
			contentType: "application/json",
			//成功返回之后调用的函数
			success: function (result) {
				if (result == "OK") {
					layer.msg('终止活动成功！', {icon: 1,time:700},function(){
						window.location.reload(true);
					});
				} else {
					layer.alert(result, {icon: 0});
				}
			}
		});
	});
}

// 物理删除活动
function deleteShopGroup(_id) {
	layer.confirm("确定要删除活动么？", {
		icon: 3
		,btn: ['确定','取消'] //按钮
	},function () {
		$.ajax({
			type: "POST",
			//提交的网址
			url: contextPath + "/s/group/delete/" + _id,
			//提交的数据
			async: false,
			//返回数据的格式
			dataType: "json",
			//成功返回之后调用的函数
			success: function (result) {
				if (result == "OK") {
					layer.msg('删除成功！', {icon: 1,time:700},function(){
						window.location.reload(true);
					});
				} else {
					layer.alert(result, {icon: 0});
				}
			}
		});
	});
}
// 逻辑删除活动
function updateDeleteStatus(_id) {
	layer.confirm("确定要删除活动么？", {
		icon: 3
		,btn: ['确定','取消'] //按钮
	},function () {
		$.ajax({
			type: "POST",
			//提交的网址
			url: contextPath + "/s/group/updateDeleteStatus/" + _id,
			//提交的数据
			async: false,
			//返回数据的格式
			dataType: "json",
			//成功返回之后调用的函数
			success: function (result) {
				if (result == "OK") {
					layer.msg('删除成功！', {icon: 1,time:700},function(){
						window.location.reload(true);
					});
				} else {
					layer.alert(result, {icon: 0});
				}
			}
		});
	});
}
function updateShopGroup(_id) {
	window.location.href = contextPath + "/s/group/update/" + _id;
}
