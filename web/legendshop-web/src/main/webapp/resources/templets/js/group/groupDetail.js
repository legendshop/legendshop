jQuery(document).ready(

function($) {
  //店铺分数
  var shopScore=$("#shopScoreNum").text();
  if(shopScore==null){
    shopScore=0;
  }
  $(".scroll_red").css("width",(shopScore/5)*100+"%");

  // 处理默认选中的信息
			var liSelecteds = $(".prop_ul .li-selected").get();
			for ( var i = 0; i < liSelecteds.length; i++) {
				handleSkuMessage($(liSelecteds[i]).parent().attr("index"));
				getPropValImgs($(liSelecteds[i]).attr("valId"));
			}

			$("#recommend ul li").click(function() {
				$("#recommend ul li").each(function(i) {
					$(this).removeClass("on");
				});
				$(this).addClass("on");
			});

			$(".prop_ul .li-show").on("click", function() {
				clickPropVal(this);
			});

			$("#productTab ul li").click(function() {
				$("#productTab ul li").each(function(i) {
					$(this).removeClass("on");
					var id = $(this).attr("id");
					// alert($(id + "Pane"));
					$("#" + id + "Pane").hide();
				});
				$(this).addClass("on");
				var id = $(this).attr("id");
				$("#" + id + "Pane").show();
				changeProductTab(id);
				if (jQuery("#productTab").hasClass("pro_fixed")) {
					document.getElementById("detail").scrollIntoView();
				}
			});
			// call ajax
			$.post(contextPath + "/visitedprod_old", function(retData) {
				$("#visitedprod").html(retData);
			}, 'html');

			// 配送至 选择框 省份选择
			jQuery("#stock_province_item ul[class=area-list] li").bind("click",
					function() {
						var provinceId = $(this).find("a").attr("data-value");
						var province = $(this).find("a").html();

						$("#stockTab li[data-index=0] a em").html(province);

						jQuery.ajax({
							url : contextPath + "/load/cityList/" + provinceId,
							type : 'post',
							async : true, // 默认为true 异步
							success : function(result) {
								$("#stock_city_item").html(result);

								$("#stockTab li").each(function(i) {
									$(this).removeClass("curr");
									$(this).find("a").removeClass("hover");
								});

								var cityTab = $("#stockTab li[data-index=1]");
								cityTab.addClass("curr");
								cityTab.find("a").addClass("hover");
								cityTab.find("em").html("请选择");
								cityTab.attr("style", "display: block;");

								$("#JD-stock div[class=mc]").each(function() {
									$(this).attr("style", "display: none;");
								});
								$("#stock_city_item").attr("style",
										"display: block;");
							}
						});
					}
			);

			// 配送至 选择框 城市选择
			jQuery("#stock_city_item ul[class=area-list] li").bind("click",
					function() {
						var cityId = $(this).find("a").attr("data-value");
						var city = $(this).find("a").html();
						$("#selCityId").val(cityId);

						$("#stockTab li[data-index=1] a em").html(city);

						jQuery.ajax({
							url : contextPath + "/load/areaList/" + cityId,
							type : 'post',
							async : true, // 默认为true 异步
							success : function(result) {
								$("#stock_area_item").html(result);

								$("#stockTab li").each(function(i) {
									$(this).removeClass("curr");
									$(this).find("a").removeClass("hover");
								});

								var areaTab = $("#stockTab li[data-index=2]");
								areaTab.addClass("curr");
								areaTab.find("a").addClass("hover");
								areaTab.find("em").html("请选择");
								areaTab.attr("style", "display: block;");

								$("#JD-stock div[class=mc]").each(function() {
									$(this).attr("style", "display: none;");
								});
								$("#stock_area_item").attr("style",
										"display: block;");
							}
						});
					}
			);

			// 配送至 选择框 地区选择
			jQuery("#stock_area_item ul[class=area-list] li").bind("click",
					function() {
						var areaId = $(this).find("a").attr("data-value");

						var area = $(this).find("a").html();
						var city = $("#stockTab li[data-index=1] a em").html();
						var province = $("#stockTab li[data-index=0] a em")
								.html();
						var text = province + " " + city + " " + area;

						$("#stockTab li[data-index=2] a em").html(area);

						$("#storeSelector div[class=text] div").html(text);
						$("#storeSelector div[class=text] div").attr("title",
								text);
						$("#storeSelector").removeClass("hover");

						//计算运费
						calProdFreight();
					}
			);
			//初始化时间
			if(status == 0){ //下线
				outLine();
			}else if(status == -1){ //审核中
				auditing();
			}else {
				initTimer();
			}
});

function handleSkuMessage(propIndex){
	propIndex = Number(propIndex);
	  var id = Number(propIndex);
	  while($("#prop_"+id).length!=0){
		  var aList = $("#prop_"+id+" a").get();
		  for(var i=0;i<aList.length;i++){
			  var kv = $(aList[i]).parent().attr("data-value");
			  var has = false;
			  var newSkus = getSelectedSkus(id-1);
			  for(var j=0;j<newSkus.length;j++){
				  if(newSkus[j].properties.indexOf(kv)>=0 && newSkus[j].status==1){
					  has = true;
					  break;
				  }
			  }
			  if(!has){
				  $(aList[i]).parent().removeClass("li-selected");
				  $(aList[i]).parent().children("i").remove();
				  $(aList[i]).parent().addClass("li-hidden");
				  $(aList[i]).parent().removeClass("li-show");
			  }else{
				  $(aList[i]).parent().addClass("li-show");
				  $(aList[i]).parent().removeClass("li-hidden");
			  }
		  }
		  id++;
	  }
	  $(".prop_main dl").removeClass("no-selected");
	  if($(".li-selected").length==$(".prop_ul").length){
			var kvList = [];
			var selectedList = $(".li-selected").get();
			for(var k=0;k<selectedList.length;k++){
				kvList[k]=$(selectedList[k]).attr("data-value");
			}
			for(var i=0;i<skuDtoList.length;i++){
				var isT = true;
				for(var j=0;j<kvList.length;j++){
					if(skuDtoList[i].properties.indexOf(kvList[j])<0){
						isT = false;
						break;
					}
				}
				if(isT){
					$("#currSkuId").val(skuDtoList[i].skuId);
					if(skuDtoList[i].name!=null && skuDtoList[i].name!=''){
						$("#prodName").html(skuDtoList[i].name);
					}else{
						$("#prodName").html(prodName);
					}
//团购价格
          var groupPrice=skuDtoList[i].price;
          $("#groupPrice").html(" "+groupPrice+" ");//add 团购价

					//库存
					var stocks=skuDtoList[i].stocks;
					prodStock = stocks;
					$("#stocts").html(" "+skuDtoList[i].stocks+" ");//add 库存
					//库存为0
					if(stocks==0){
						$(".cart_action").hide(); //购买等操作
						$("#stockSpan").html("<span style='color:red;'>对不起,库存不足!</span>");
					}else{
						$(".cart_action").show();
						$("#stockSpan").html("库存<span style='color: #005aa0;' id='stocts'>"+stocks+"</span>件");
					}


					var promotionPrice=Number(skuDtoList[i].promotionPrice);
					var cash=Number(skuDtoList[i].cash);

					if(promotionPrice<=cash){ //说明有折扣
						var discountPrice = Math.round((cash-promotionPrice)*100)/100;
						$("#prodCash").html(promotionPrice.toFixed(2));
						if(discountPrice>0){
							var html=" (原价:￥"+cash.toFixed(2)+" 已优惠￥"+discountPrice.toFixed(2)+")";
							$("#promotionPriceStr").html(html);
						}else{
							$("#promotionPriceStr").html("");
						}
					}else{
						$("#prodCash").html(cash.toFixed(2));
					}
					break;
				}
			}
			allSelected=true;
		}else{
			var dlList = $(".prop_main dl").get();
			for(var i=0;i<dlList.length;i++){
				if($(dlList[i]).find(".li-selected").length==0){
					$(dlList[i]).addClass("no-selected");
				}
			}
			allSelected = false;
		}
}

function getSelectedSkus(propIndex){
	var selectedLiList = [];
	  var selIndex = 0;
	  for(var h=0;h<=propIndex;h++){
		  if($("#prop_"+h+" .li-selected").length!=0){
			  selectedLiList[selIndex]=$("#prop_"+h+" .li-selected").get(0);
			  selIndex++;
		  }
	  }
	  var index = 0;
	  var newSkus = [];
	  for(var i=0;i<skuDtoList.length;i++){
		  var has = true;
		  for(var j=0;j<selectedLiList.length;j++){
			  var kv = $(selectedLiList[j]).attr("data-value");
			  if(skuDtoList[i].properties.indexOf(kv)<0){
				  has = false;
				  break;
			  }
		  }
		  if(has){
			  newSkus[index]=skuDtoList[i];
			  index++;
		  }
	  }
	  return newSkus;
}


//获取属性值图片
function getPropValImgs(valId){
	 for(var i=0;i<propValueImgList.length;i++){
		  if(propValueImgList[i].valueId==valId){
			  var imgList = propValueImgList[i].imgList;
			  var str = "";
			  for(var j=0;j<imgList.length;j++){
				  str+= "<li onmouseover='viewPic(this);' data-url='"+imgList[j]+"' class='img-default'>";
				  str+="<table cellpadding='0' cellspacing='0' border='0'><tr><td height='65' width='65' style='text-align: center;'>";
				  str+= "<img src='"+imgPath3.replace('PIC_DEFAL', imgList[j])+"' style='max-width:65px;max-height:65px;'  />";
				  str+="</td></tr></table>";
				  str+= "</li>";

			  }
			  $(".preview .simglist").html(str);
			  viewPic($(".preview .simglist li").get(0));
			  $("#spec-list").infiniteCarousel();
			  break;
		  }
	  }
}

//点击属性值
function clickPropVal(obj){
	  if($(obj).children("i").length<=0){
		  $(obj).addClass("li-selected").siblings().removeClass("li-selected");
		  $(obj).addClass("li-selected").siblings().children("i").remove();
		  $(obj).append("<i></i>");
		  var propIndex = $(obj).parent().attr("index");
		  handleSkuMessage(propIndex);
		  getPropValImgs($(obj).attr("valId"));
	  }
}


//增加购买数量
function addPamount() {
	var pamount =Number($("#count").val());
	var re = /^[1-9]+[0-9]*]*$/;
	if (isNaN(pamount) || !re.test(pamount)) {
		return ;
	}
    if(limitNum!=0&& pamount>=Number(limitNum)){ //是否超过购买限制
		return;
	}
    else if(pamount>=Number(prodStock)){
    	return;
    }
	$("#count").val(pamount* 1 + 1);
}


//减少购买数量
function reducePamount() {
	var pamount = $("#count").val();
	var re = /^[1-9]+[0-9]*]*$/;
	if (isNaN(pamount) || !re.test(pamount)) {
		return ;
	}
	if (pamount <= 1) {
		$("#count").val(1);
	} else {
		$("#count").val(pamount * 1 - 1);
	}
}

//得判断是否为数字的同时，也判断其是不是正数
function validateInput() {
	var pamount = $("#count").val();
	var re = /^[1-9]+[0-9]*]*$/;
	if (isNaN(pamount) || !re.test(pamount)) {
		//错误时
		$("#count").attr("style", "border: 1px solid #f40000;");
		layer.alert("请输入正确的数量!",{icon: 0});
		return false;
	}
	if(limitNum!=0&& pamount>Number(limitNum)){ //是否超过购买限制
		$("#count").attr("style", "border: 1px solid #f40000;");
		layer.alert("超过购买限制!",{icon: 0});
		return false;
	}
	else if(pamount>Number(prodStock)){
		$("#count").attr("style", "border: 1px solid #f40000;");
		layer.alert("超过库存限制",{icon: 0});
		return false;
	}
	else {
		//正确时
		$("#count").attr("style", "");
		return true;
	}

}

//初始化时间检查
function initTimer(){
	var newTime=contextPath+"/group/time/now"
	 $.get(newTime,{},function(result){  //获取系统的时间
		 if(result.isSuccess){
			 var nowTime = result["data"];
			 if(nowTime>=endTime){  //活动结束
				 end();
			 }else if(nowTime<startTime){  //距离活动开始时间
				 ready();
                 //倒计时处理
                 countdown(startTime);
             }
			 else{ //活动进行中
				 start();
				 finishCountdown();
             }
		 }
	 });
}
//准备开始
function ready(){
	  $("#but_seck_ready").show();
	  $("#group_ready").show();
	  $("#group_starting").hide();
	  $("#group_end").hide();
	  $('#group-btn').show();
	  $('.tb-btn-basket a').text("准备开始").css({backgroundColor:"#999"}).css({borderColor:"#999"}).attr("href","javaScript:void(0)");
}
//开始活动
function start(){
	  $("#but_seck").show();
	  $("#group_starting").show();
	  $("#group_end").hide();
	  $("#group_ready").hide();
	  $('#seckill-btn').show();
	  $('.tb-btn-basket a').text("马上参团").css({backgroundColor:"#c6171f"}).css({borderColor:"#c6171f"}).attr("href","javaScript:joinGroup()");
}

//活动结束
function end(){
	  var str=new Date(endTime).format('活动结束于:<span><em>MM</em>月<em>dd</em>日<em>hh</em>时<em>mm</em>分<em>ss</em>秒</span>');
	  $("#group_end .pm-status-time span").html(str);
	  $("#but_seck_end").show();
	  $("#group_end").show();
	  $("#group_starting").hide();
	  $("#group_start").hide();
	  $('#group-btn').show();
	  $('.tb-btn-basket a').text("已结束").css({backgroundColor:"#999"}).css({borderColor:"#999"}).attr("href","javaScript:void(0)");
}

//活动下线
function outLine(){
	  $("#but_seck_end").show();
	  $("#group_end").show();
	  $("#group_starting").hide();
	  $("#group_start").hide();
	  $('#group-btn').show();
	  $('.tb-btn-basket a').text("已下线").css({backgroundColor:"#999"}).css({borderColor:"#999"}).attr("href","javaScript:void(0)");
}

//活动审核中
function auditing(){
	  $("#but_seck_end").show();
	  $("#group_end").show();
	  $("#group_starting").hide();
	  $("#group_start").hide();
	  $('#group-btn').show();
	  $('.tb-btn-basket a').text("正在审核").css({backgroundColor:"#999"}).css({borderColor:"#999"}).attr("href","javaScript:void(0)");
}

//倒计时
function countdown (startTime){
    var date=new Date(startTime+1000);
    $('#group_ready .pm-status-time').countdown(date,function(event) {
        $(this).html(event.strftime('活动准备开始:<span><em>%D</em>天<em>%H</em>时<em> %M</em>分<em> %S</em>秒</span>'));
    }).on('finish.countdown',function(){
    	 start();
    	 finishCountdown();
    });
}

function finishCountdown(){
	var date=new Date(endTime+1000);
	$('#group_starting .pm-status-time').countdown(date).on('update.countdown', function(event) {
	     $(this).html(event.strftime('活动正在进行:<span><em>%D</em>天<em>%H</em>时<em> %M</em>分<em> %S</em>秒</span>'));
    }).on('finish.countdown', function(event) {
    	end();
    });
}


Date.prototype.format = function(format) {
    var o = {
        "MM+" : this.getMonth() + 1, // month
        "dd+" : this.getDate(), // day
        "hh+" : this.getHours(), // hour
        "mm+" : this.getMinutes(), // minute
        "ss+" : this.getSeconds(), // second
        "q+" : Math.floor((this.getMonth() + 3) / 3), // quarter
        "S" : this.getMilliseconds()
    // millisecond
    };
    if (/(y+)/.test(format))
        format = format.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
    for ( var k in o)
        if (new RegExp("(" + k + ")").test(format))
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
                    : ("00" + o[k]).substr(("" + o[k]).length));
    return format;
}



function joinGroup(){
	//判断是否登录
	 if(isLogin()){
		login();
		return;
	 }
	if (!allSelected) {
			return;
	}
	var prodId = currProdId;
	var pamount = $.trim($("#count").val());
	var skuId=$("#currSkuId").val();
	if(isEmpty(skuId)){
		skuId=0;
	}
	if(isEmpty(pamount)){
		return;
	}
	if(!validateInput()){
		return;
	}
	$("#J_LinkBasket").attr("disabled","disabled");
	$.ajax({
	        //提交数据的类型 POST GET
	        type:"POST",
	        //提交的网址
	        url: contextPath+"/p/checkGroup/"+groupId+"/"+prodId+"/"+skuId,
	        timeout : 10000, //超时时间设置，单位10秒
	        data:{"number":pamount},
	        //提交的数据
	        async : false,
	        //返回数据的格式
	        datatype: "json",
	        //成功返回之后调用的函数
			success : function(data) {
				var result=jQuery.parseJSON(data);
	          	var isExpose=result.expose;
	          	if(isExpose){
	          		submitForm(contextPath+"/p/group/orderDetails/"+groupId+"/"+prodId+"/"+skuId,"{}");
	          	   return;
	          	}else{
	          		 layer.alert(result.msg,{icon: 2});
	          		 return;
	          	}
			},
	        //调用执行后调用的函数
	        complete: function(XMLHttpRequest, textStatus){
	        	$("#J_LinkBasket").removeAttr("disabled");
	        },
	        //调用出错执行的函数
	        error: function(XMLHttpRequest, textStatus){
	       	   if(textStatus=='timeout'){ // 超时,status还有success,error等值的情况
	            }else{
	       		  layer.alert("系统繁忙,请稍候!",{icon: 2});
	           }
	       }
	 });
}


function submitForm(action, params) {
	   var temp = document.createElement("form");
	   temp.action = action;
	   temp.method = "post";
	   temp.target='_self';
	   temp.style.display = "none";
	   for(var i=0 ; i < params.length;i ++){
	        var input1 = document.createElement("input");
	        input1.name = params[i].name;
	        input1.value = params[i].val;
	        temp.appendChild(input1);
	    }
	   temp.appendChild(appendCsrfInput());
	   document.body.appendChild(temp);
	   temp.submit();
	   return temp;
}


//得判断是否为数字的同时，也判断其是不是正数
function isLogin() {
	if(isEmpty(loginUserName)){
		return true;
	}
	return false;
}

function login(){
	layer.open({
		title :"登录",
		  type: 2,
		  content: contextPath + '/loadLoginOverlay', //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['440px', '530px']
		});
}

/**
 * 判断是否是空
 * @param value
 */
function isEmpty(value){
	if(value == null || value == "" || value == "undefined" || value == undefined || value == "null"){
		return true;
	}
	else{
		value = (value+"").replace(/\s/g,'');
		if(value == ""){
			return true;
		}
		return false;
	}
}
//客服链接
$("#loginIm").click(function () {
  var user_name = $("#user_name").val();
  var user_pass = $("#user_pass").val();
  var skuId = $("#currSkuId").val();
  if (user_name == '' || user_pass == '') {
    login();
  } else {
    window.open(contextPath + "/p/im/index/" + skuId, '_blank');
  }
});
