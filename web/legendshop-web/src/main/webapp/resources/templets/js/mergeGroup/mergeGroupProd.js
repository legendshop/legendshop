$(function(){
	$(".sea-btn").click(function(){
		asyncloadCheck();
	});

	//参加
	$(".add-table").on("click",".join-btn",function(){
		if(!isBlank(prodIds) || prodIds.length>0){//只允许添加一个
			art.dialog.tips("已选择活动商品");
			return;
		}
		var prodId = $(this).attr("data-id");

		//检查该商品是否参见过预售、秒杀、团购、拍卖
		$.ajax({
				url : contextPath + "/s/mergeGroupActivity/is/join/active/"+prodId,
				type : 'get',
				async : false, // 默认为true 异步
				dataType : "json",
				success : function(data) {
					if(data=="OK"){
						var index = $.inArray("prodId", prodIds);
						if(index<0){//prodIds中不存在
							prodIds.push(prodId);
							$(this).parents("td").append("<a class='rm-btn' data-id='"+prodId+"' style='color: #3e87cd;cursor: pointer;'>取消参加</a>");
							$(this).remove();
						}
						top.art.dialog({id:"selProd"}).close();
					}else{
						art.dialog.tips("该商品已参加"+data+"，请选择其他商品");
					}
				}
		});

	});

	//取消参加
	$(".add-table").on("click",".rm-btn",function(){
		var prodId = $(this).attr("data-id");
//		var index = prodIds.indexOf(prodId);
		var index = $.inArray(prodId, prodIds);
		if(index>=0){//prodIds中存在
			prodIds.splice(index, 1);
			$(this).parents("td").append("<a class='join-btn' data-id='"+prodId+"' style='cursor: pointer;'>参加</a>");
			$(this).remove();
		}
	});

	//增加进artDialog
	artDialog.data("prodIds",prodIds);
})


/**翻页*/
function pager(curPageNO){
	$("#curPageNO").val(curPageNO);
	asyncload();
}


/**异步获取数据*/
function asyncload() {
	var url = contextPath + "/s/mergeGroupActivity/selProdList";
	var data = $("#myForm").serialize();
	$.ajax({
		url: url,
		data: data,
		type: "get",
		dataType: "html",
		success: function(result) {
			$(".add-table").html(result);
		}
	});
}

/**点击搜索不传递页码*/
function asyncloadCheck() {
	var url = contextPath + "/s/mergeGroupActivity/selProdList";
	var prodName = $("#prodName").val();
	$.ajax({
		url: url,
		data: {"prodName":prodName},
		type: "get",
		dataType: "html",
		success: function(result) {
			$(".add-table").html(result);
		}
	});
}


//方法，判断是否为空
function isBlank(_value){
	return _value==null || _value=="" || _value==undefined;
}
