var loaddely = false;
$(function() {

  // $(".presellType").each(function () {
  //     //初始化定金金额
  //     var $this=$(this)
  //     var sub=$this.parents(".sub");
  //     var presellPrice =sub.find(".actualTotal").text(); // 预售价
  //     var preDepositPrice = sub.find(".preDepositPrice");// 定金价
  //     var finalPayment =sub.find(".finalPrice"); // 尾款
  //     var percentNum = parseFloat(preDepositPrice.text()/100);//定金百分比
  //     var preVal = parseFloat(presellPrice*percentNum)
  //     var finalPayVal = parseFloat(presellPrice-preVal);
  //     preDepositPrice.text(numberConvert(preVal));
  //     finalPayment.text(numberConvert(finalPayVal));
  // })


	userCenter.changeSubTab("ordersManage");
	$('.checkall_s').click(function() {
		var if_check = $(this).attr('checked');
		$('.checkitem').each(function() {
			if (!this.disabled) {
				$(this).attr('checked', if_check);
			}
		});
		$('.checkall_s').attr('checked', if_check);
	});

	$('#skip_off').click(function() {
		url = location.href.replace(/&amp;skip_off=\d*/g, '');
		window.location.href = url + '&amp;skip_off=' + ($('#skip_off').attr('checked') ? '1' : '0');
	});

	$('#show_shipping').on('hover', function() {
		var html = "";
		if (dvyFlowId == "" || dvyTypeId=="") {
			html = "没有物流信息";
		} else {
			if (!loaddely) {
				$.ajax({
					url : contextPath + "/s/order/getExpress",
					data : {
						"dvyFlowId" : dvyFlowId,
						"dvyId" : dvyTypeId
					},
					type : 'POST',
					async : false, // 默认为true 异步
					dataType : 'json',
					success : function(result) {
						if (result == "fail" || result == "") {
							html = "没有物流信息";
							loaddely = true;
						} else {
							var list = eval(result);
							for ( var obj in list) { // 第二层循环取list中的对象
								html += "<li>" + list[obj].content + "<li>";
							}
							loaddely = true;
						}
					}
				});
			}
		}

		$('#shipping_ul').html(html);
		$('#show_shipping').unbind('hover');
	});

	// 打印发货单
	$(document).on("click", ".printInvoice", function() {
		var _subId = $(this).attr("data-subId");
		var _dvyTypeId = $(this).attr("data-dvyTypeId");
		$.ajax({
			url : contextPath + '/s/hasShopAddress',
			type : 'POST',
			dataType : 'json',
			success : function(result) {
				if (result.status == "OK") {
					var url = contextPath + "/s/printDeliveryDocument/" + _subId + "/" + _dvyTypeId;
					window.open(url);
				} else {
					layer.confirm('请先去设置商家地址', {
						btn : [ '确定', '取消' ]
					}, function(index) {
						layer.close(index);
						var url = contextPath + "/s/shopSetting";
						window.open(url);
					});
				}
			}
		});
	});

});

function pager(curPageNO) {
	document.getElementById("curPageNO").value = curPageNO;
	document.getElementById("ListForm").submit();
}

function deliverGoods(_orderId) {
	layer.open({
		title : "确认发货",
		id : "deliverGoods",
		type : 2,
		resize : false,
		content : [ contextPath + '/s/deliverGoods/' + _orderId, 'no' ],// 这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content:
		// ['${contextPath}/s/loadProdListPage',
		// 'no']
		area : [ '450px', '250px' ]
	});
}

function changeDeliverNum(_orderId) {
	layer.open({
		title : "修改物流单号",
		id : "changeDeliverNum",
		type : 2,
		resize : false,
		content : [ contextPath + '/s/changeDeliverNum/' + _orderId, 'no' ],// 这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content:
		area : [ '430px', '250px' ]
	});
}

function changeOrderFee(_orderId) {
	layer.open({
		title : "调整订单金额",
		id : "changeOrderFee",
		type : 2,
		resize : false,
		content : [ contextPath + '/s/changeOrderFee/' + _orderId, 'no' ],// 这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content:
		area : [ '430px', '380px' ]
	});
}

var cancleOrder = function(id) {
	layer.confirm('确认取消该订单？', {
		icon : 3,
		btn : [ '确定', '取消' ]
	// 按钮
	}, function() {
		$.ajax({
			url : contextPath + '/s/order/cancleOrder',
			data : {
				"subNumber" : id
			},
			type : 'POST',
			async : false, // 默认为true 异步
			dataType : 'text',
			success : function(result) {
				if (result == "OK") {
					var msgs = "取消订单成功";
					layer.msg(msgs, {
						icon : 1,
						time : 700
					}, function() {
						window.location.reload();
					});
				} else if (result == "fail") {
					var msgs = "取消订单失败";
					layer.msg(msgs, {
						icon : 2
					});
				}
			}
		});

	})
}

function orderReceive(_number) {
	layer.confirm('你确定要确认收货吗？', {
		icon : 3,
		btn : [ '确定', '取消' ]
	// 按钮
	}, function() {
		$.ajax({
			url : contextPath + "/p/orderReceive/" + _number,
			type : 'POST',
			async : false, // 默认为true 异步
			dataType : 'text',
			success : function(result) {
				if (result == "OK") {
					var msg = "确认收货成功";
					alert(msg);
					layer.alert(msg, {
						icon : 1
					});
					window.location.reload();
				} else if (result == "fail") {
					var msg = "确认收货失败";
					layer.alert(msg, {
						icon : 2
					});
				}
			}
		});
	})
}

//商家退定金
function shopRefundDeposit(orderId, userId) {
	location.href = contextPath + "/s/shopRefund/apply/" + userId + "/" + orderId;
}


function addRemark(subNum, text) {

	var str = '<div style="margin:10px; width:96%;">备注内容<font color="red">(*必填)</font>：<br/><br/>'
			+ '<textarea style="padding:5px;width:95%;" rows="10" cols="50" maxlength="400" id="text"></textarea></div>';

	if (!isBlank(text)) {
		str = '<div style="margin:10px; width:96%;">备注内容<font color="red">(*必填)</font>：<br/><br/>'
				+ '<textarea style="padding:5px;width:95%;" rows="10" cols="50" maxlength="400" id="text">'
				+ text + '</textarea></div>';
	}

	layer.open({
		type : 1,
		title : '添加备注',
		content : str,
		btn : [ '确定', '取消' ],
		skin : [ 'user-btn-class' ],
		shade : 0.5,
		area : [ '370px',"350px" ],
		yes : function(index, layero) {
			var shopRemark = $("#text").val();
			if (shopRemark == null || shopRemark == ''
					|| shopRemark == undefined) {
				layer.msg('备注内容不能为空', {
					time : 1500
				});
				return false;
			}
			$.ajax({
				url : contextPath + "/s/orderMange/remarkShopOrder",
				data : {
					"subNum" : subNum,
					"shopRemark" : shopRemark
				},
				type : 'post',
				async : true, // 默认为true 异步
				dataType : 'json',
				success : function(data) {
					if (data.status == "OK") {
						layer.msg(data.msg, {
							time : 1500
						});
						setTimeout(function() {
							document.getElementById("ListForm").submit();
						}, 1500);
					} else {
						layer.msg(data.msg, {
							time : 1500
						});
						return false;
					}
				}
			});
		},
	});
}

function readRemark(ths) {

	var shopRemark = $(ths).attr("shopRemark");
	var subNum = $(ths).attr("subNumber");
	var shopRemarkDate = $(ths).attr("shopRemarkDate");
	var str = "<div style='padding: 5px 20px 20px;'><table style='width:100%;'>"
			+ "<tr><td style='width: 100px;text-align: center; border-bottom: 1px solid #e4e4e4; padding: 10px 0;'>备注内容：</td>"
			+ "<td style='padding:10px; border-bottom: 1px solid #e4e4e4; word-break: break-all;'>"
			+ shopRemark
			+ "</td></tr>"
			+ "<tr><td style='width:100px;text-align: center;border-bottom: 1px solid #e4e4e4;'>备注时间：</td>"
			+ "<td style='padding:10px; border-bottom: 1px solid #e4e4e4;'>"
			+ shopRemarkDate + "</td></tr></table></div>";

	layer.open({
		type : 1,
		title : '查看备注',
		content : str,
		btn : 0,
		btn : [ '更新备注' ],
		skin : [ 'user-btn-class' ],
		shade : 0.5,
		area : [ '500px' ],
		yes : function(index, layero) {
			addRemark(subNum, shopRemark);
		},
	});
}

// 展示发票信息
function showInvoiceSub() {

  var str = "<div style='padding: 5px 20px 20px;'><table style='width:104%;'>"
    + "<tr><td style='width: 100px;text-align: center; border-bottom: 1px solid #e4e4e4; padding: 5px 0;'>公司名称：</td>"
    + "<td style='padding:10px; border-bottom: 1px solid #e4e4e4; word-break: break-all;'>"
    + company
    + "<tr><td style='width: 100px;text-align: center; border-bottom: 1px solid #e4e4e4; padding: 5px 0;'>纳税人号：</td>"
    + "<td style='padding:10px; border-bottom: 1px solid #e4e4e4; word-break: break-all;'>"
    + invoiceHumNumber
    + "<tr><td style='width: 100px;text-align: center; border-bottom: 1px solid #e4e4e4; padding: 5px 0;'>注册地址：</td>"
    + "<td style='padding:10px; border-bottom: 1px solid #e4e4e4; word-break: break-all;'>"
    + registerAddr
    + "<tr><td style='width: 100px;text-align: center; border-bottom: 1px solid #e4e4e4; padding: 5px 0;'>注册电话：</td>"
    + "<td style='padding:10px; border-bottom: 1px solid #e4e4e4; word-break: break-all;'>"
    + registerPhone
    + "<tr><td style='width: 100px;text-align: center; border-bottom: 1px solid #e4e4e4; padding: 5px 0;'>开户银行：</td>"
    + "<td style='padding:10px; border-bottom: 1px solid #e4e4e4; word-break: break-all;'>"
    + depositBank
    + "</td></tr>"
    + "<tr><td style='width:100px;text-align: center;border-bottom: 1px solid #e4e4e4;'>银行账号：</td>"
    + "<td style='padding:10px; border-bottom: 1px solid #e4e4e4;'>"
    + bankAccountNum + "</td></tr></table></div>";

  layer.open({
    type: 1,
    title : '发票详情',
    area: ['500px', '320px'], //宽高
    content: str
  });
}



function isBlank(value) {
	return value == undefined || value == null || $.trim(value) === "";
}
function numberConvert(num) {
  var numConvert = Math.round(num * 1000) / 1000;
  var number = numConvert.toString();

  var index =  number.indexOf('.');
  var price = index != -1?number.substring(0,index+3):number;
  return price;
}
