  $(function(){
		 //三级联动
  	  $("select.combox").initSelect();

  	  userCenter.changeSubTab("shopSetting");

    //营业执照图片回显
    $("#businessImageFile").on("change", function(e) {
      var file = e.target.files[0];
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function(){
        $("#images5").attr("src", this.result);
        $("#images5").show();
      }
    });

  	  //图片回显1
  	  $("#file").on("change", function(e) {
  		  var file = e.target.files[0];
		  var reader = new FileReader();
		  reader.readAsDataURL(file);
		  reader.onload = function(){
			  $("#images1").attr("src", this.result);
			  $("#images1").show();
		  }
  	  });

  	  //图片回显2
  	  $("#file2").on("change", function(e) {
  		  var file = e.target.files[0];
		  var reader = new FileReader();
		  reader.readAsDataURL(file);
		  reader.onload = function(){
			  $("#images2").attr("src", this.result);
			  $("#images2").show();
		  }
  	  });

  	  //图片回显banner
  	  $("#bannerfile").on("change", function(e) {
  		  var file = e.target.files[0];
		  var reader = new FileReader();
		  reader.readAsDataURL(file);
		  reader.onload = function(){
			  $("#images3").attr("src", this.result);
			  $("#images3").show();
		  }
  	  });

  	jQuery("#form1").validate({
		rules: {
			"siteName": {
				required: true,
				minlength: 2,
				checkVal:true,
				checkSiteName:true
			},
			contactName: {
				required: true
			},
			contactMobile: {
				required: true,
				isMobile:true
			},
			contactTel: {
				isTel:true
			},
			contactMail: {
				email:true
			}
		},
		messages: {
            contactName: {
                required: '请输入联系人姓名'
            },
            contactMobile: {
                required: '请输入手机号',
                	isMobile:"请输入正确的手机号"
            },
            contactMail: {
                email:'请输入正确的Email'
            },
            siteName: {
               required: '请输入店铺名称',
               minlength: '最少2个字符',
            },
            contactTel:{
           		 required: '必填项',
            },

        },
        focusInvalid : false,
        onkeyup : false,
        ignore: "ignore",
        submitHandler : function(form) {
		      var businessImageFile= $("input[name=businessImageFile]").val();
        	var file=$("input[name=file]").val();
        	var file2=$("input[name=file2]").val();
        	var bannerfile=$("input[name=bannerfile]").val();

        	var province=$("select[name=provinceid]").find("option:selected").text();
        	if(!isEmpty(province) && province!="-- 请选择 --"){
        		$("input[name=province]").val(province);
        	}
        	var city=$("select[name=cityid]").find("option:selected").text();
        	if(!isEmpty(city) && city!="-- 请选择 --"){
        		$("input[name=city]").val(city);
        	}
        	var area=$("select[name=areaid]").find("option:selected").text();

        	if(!isEmpty(area) && area!="-- 请选择 --"){
        		$("input[name=area]").val(area);
        	}

          if(isEmpty(businessImageFile)){
            $("input[name=businessImageFile]").attr("disabled","disabled");
          }
        	if(isEmpty(file)){
        		$("input[name=file]").attr("disabled","disabled");
        	}
        	if(isEmpty(file2)){
        		$("input[name=file2]").attr("disabled","disabled");
        	}
        	if(isEmpty(bannerfile)){
        		$("input[name=bannerfile]").attr("disabled","disabled");
        	}
        	$(".set-save a ").html("提交中...");
            $(".set-save a ").attr("onclick","");
            $(form).ajaxForm().ajaxSubmit({
               dataType:"json",
      		   success:function(result) {
      			   if(result="OK"){
      				 layer.alert('保存成功', {icon: 1},function(){
      					window.location.reload(true);
      				 });
      			   }else{
      				 layer.alert(result, {icon: 0});
      			   }
      		   },
      		  //调用执行后调用的函数
   	           complete: function(XMLHttpRequest, textStatus){
                 $("input[name=businessImageFile]").removeAttr("disabled");
   	        	  $("input[name=file]").removeAttr("disabled");
  				  $("input[name=file2]").removeAttr("disabled");
  				  $("input[name=bannerfile]").removeAttr("disabled");
  				  $(".set-save a ").html("保存设置");
  	              $(".set-save a ").attr("onclick","saveShop();");
   	           }
      	    });
        }

	});
 });


   function saveShop(){
     var str = ""
     for (var x=0;x<urls.length;x++)
     {
       if (urls[x] !=null){
         str=(str+urls[x]+",")
       }
     }
     var s = str.substring(0,str.length-1);
     $("#businessImage").attr("value",s);
     $("#form1").submit();
   }



   var oldSiteName = "${shopSetting.siteName}";
  function isMobile(str){
	var reg = /^1\d{10}$/;
	return reg.test(str);
	}
	function isTel(str){
	var reg = /^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/;
	return reg.test(str);
	}

jQuery.validator.addMethod("isMobile", function(value, element) {
     return isMobile(value);}, '请输入正确的手机号码');
jQuery.validator.addMethod("isTel", function(value, element) {
	if(value!=null&&value!=''){
     	return isTel(value);
     	}else{
     		return true
     	}
     }, '请输入正确的固定电话');

$.validator.addMethod("checkVal",function(value,element){
		var config=true;
			if(value==oldSiteName){
				return config;
			}else{
				$.ajax({
					type: "POST",
					url: contextPath+"/isSiteNameExist",
					async:false,
					data:{"siteName":value},
					dataType: "json",
					success:function(data){
						if(!data)
							config=false;
					}
				});
				return config;
			}
	},'店铺名已存在');

$.validator.addMethod("checkSiteName",function(value,element){
	//特殊字符过滤
	//var regex = /[`~{}《》<>\.!@#\$\^*()，。、：；——"‘“\[\]【】（）\+\-%&',:;\/=?\\\|]+/;
	var regex = new RegExp("^([a-zA-Z0-9_]|[\\u4E00-\\u9FFF])+$", "g");
	if(regex.test(value)){
		return true;
	}else{
		return false;
	}
},'只能由汉字、数字、字母、下划线组成');

/**
 * 判断是否是空
 * @param value
 */
function isEmpty(value){
	if(value == null || value == "" || value == "undefined" || value == undefined || value == "null"){
		return true;
	}
	else{
		value = (value+"").replace(/\s/g,'');
		if(value == ""){
			return true;
		}
		return false;
	}
}



 $("input[name=file2]").change(function(){
 						checkImgType(this);
						checkImgSize(this,1024);
 					});


  $("input[name=bannerfile]").change(function(){
 						checkImgType(this);
						checkImgSize(this,1024);
 					});
