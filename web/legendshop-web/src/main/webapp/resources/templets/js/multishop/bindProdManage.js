jQuery(document).ready(function(){
	  //三级联动
  	 // $("select.combox").initSelect();
  	  userCenter.changeSubTab("bindProdManage");

 });

function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("from1").submit();
}

	//加载商品分类用于选择
	function bindProd(tagId){

		layer.open({
  		  title :"选择商品",
  		  id: "bindProd",
  		  type: 2,
  		  resize: false,
  		  content: [contextPath+"/s/loadProdListByTag/"+tagId,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
  		  area: ['700px', '700px']
  		});
	}



    /** 更改商品状态**/
   function pullOff(ths){
    	var id = $(ths).attr("id");
      	var status = $(ths).attr("status");
      	var desc;
      	var toStatus;
      	   if(status == 1){
      	   		toStatus  = 0;
      	   		desc = $(ths).attr("name")+"'下线?";
      	   }else{
      	       toStatus = 1;
      	       desc = $(ths).attr("name")+' 上线?';
      	   }

       	layer.confirm(desc,{
	  		 icon: 3
	  	     ,btn: ['确定','取消'] //按钮
	  	   }, function () {
	  		 jQuery.ajax({
					url:contextPath+"/s/updateTagStatus/" + id + "/" + toStatus,
					type:'get',
					async : false, //默认为true 异步
					dataType : 'json',
					success:function(data){
						window.location.reload();
					}
				});
		})
   	}

		/**删除多个商品**/
 	  function confirmDelete(prodId,name){
 		 layer.confirm("是否将商品：'"+name+"' 放入商品回收站?", {
      		 icon: 3
      	     ,btn: ['确定','取消'] //按钮
      	   },function () {
 			  jQuery.ajax({
 					url:contextPath+"/s/product/toDustbin/"+prodId,
 					type:'get',
 					async : false, //默认为true 异步
 					dataType : 'json',
 					success:function(data){
 						if(data=="OK"){
 							window.location.reload();
 						}else{
 							layer.msg('操作失败！', {icon: 2});
 						}
 					}
 					});
 		  });

  		}

   	function deleteAction(){
   		//获取选择的记录集合
   		selAry = document.getElementsByName("strArray");
   		if(!checkSelect(selAry)){
   		 layer.alert('删除时至少选中一条记录！');
   		 return false;
   		}
   		layer.confirm("删除后不可恢复, 确定要删除吗？",{
      		 icon: 3
      	     ,btn: ['确定','取消'] //按钮
      	   }, function () {
   				var totalToDel = 0;
   				var onError = 0;
				for(i=0;i<selAry.length;i++){
				  if(selAry[i].checked){
					  totalToDel = totalToDel + 1;
					  var prodId = selAry[i].value;
					  var name = selAry[i].getAttribute("arg");
					  var result =	deleteProduct(prodId,true);
					  if('OK' != result){
						onError = onError + 1;
					  }
				  }
   				}
   		   		if(onError == 0){
   		   			window.location.reload();
   		   		}else if(onError < totalToDel ){
   		   			layer.msg('删除部分标签成功！', {icon: 2,time:700},function(){
   		   			window.location.reload();
   		   			});

   		   		}else  if(onError == totalToDel){

   		   			 layer.msg('删除标签失败！', {icon: 2,time:700},function(){
   		   				 window.location.reload();
   		   			});
   		   		}
   			}, function () {
   					//cancel
   			});

   		return true;
   	}

   	function batchOffline(){
		//获取选择的记录集合
		selAry = document.getElementsByName("strArray");
		if (!checkSelect(selAry)) {
			layer.alert('至少选中一条记录！',{icon:0});
			return false;
		}
		layer.confirm("确定要把选中的标签下线吗？",{
      		 icon: 3
      	     ,btn: ['确定','取消'] //按钮
      	   },function() {
			var totalToOffline = 0;
			var onError = 0;
			for (i = 0; i < selAry.length; i++) {
				if (selAry[i].checked) {
					totalToOffline = totalToOffline + 1;
					var prodId = selAry[i].value;
					var name = selAry[i].getAttribute("arg");
					var result = offlineProduct(prodId);
					if ('OK' != result) {
						onError = onError + 1;
					}
				}
			}
			if (onError == 0) {
				layer.msg('标签下线成功！', {icon: 1});
				window.location.reload();
			} else if (onError < totalToOffline) {
				layer.msg('部分标签下线成功！', {icon: 1});
				window.location.reload();
			} else if (onError == totalToOffline) {
				layer.msg('标签下线失败！', {icon: 2});
			}
		}, function() {
			//cancel
		});

		return true;
	}

	function offlineProduct(id){
		var result = 0;
		jQuery.ajax({
			url : contextPath+"/s/updateTagStatus/" + id + "/0",
			type : 'get',
			async : false, //默认为true 异步
			dataType : 'json',
			success : function(data) {
				result = data;
			}
		});
		return result;
	}

   	function deleteProduct(id, multiDel) {
   		var result;
		  jQuery.ajax({
				url:contextPath+"/s/deleteProdTagRel/"+id,
				type:'get',
				async : false, //默认为true 异步
				dataType : 'json',
				success:function(data){
					result = data;
				}
				});
		  return result;
   	}
