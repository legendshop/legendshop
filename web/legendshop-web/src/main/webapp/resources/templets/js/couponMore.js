$(".button").bind("click",function(){
	layer.load(1);
	var couponId = $(this).attr("couponId");
	jQuery.ajax({
		url : contextPath + "/isUserLogin",
		type : 'get',
		async : false, // 默认为true 异步
		success : function(result) {
			if(result=="true"){
				$.ajax({
					url: contextPath+"/p/coupon/receive/" + couponId,
					type:'post',
					async : false, //默认为true 异步
					dataType:"json",
					success:function(data){
						if(data=="OK"){
							layer.msg("恭喜您领取成功",{icon:1,time:1500},function(){
								skip();
							});
						}else{
							layer.msg(data,{icon:0,time:1500},function(){
								window.location.reload()
							});
						}
					}
				});
			}else{
				layer.open({
					title :"登录",
					type: 2,
					content: contextPath + '/loadLoginOverlay', //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
					area: ['440px', '510px']
				});
			}
		}
	});

});

function skip(){
	window.location.reload();
}

function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("form1").submit();
};
