function closeDialog(){
				var index = parent.layer.getFrameIndex('updatePwd'); //先得到当前iframe层的索引
				parent.layer.close(index);
			}

function saveUser(){
	var errCount = 0;
		
		if(!checkPasswd()){
			errCount++;
		}
		if(!checkPasswd2()){
			errCount++;
		}
		if(errCount==0){
			$("#from1").submit();
			layer.msg('添加成功！', {icon: 1},function(){
        		window.location.reload();
        	});
		}
	
}

function checkPasswd(){
	var result = true;
	var password = $("#password").val();
	/*var pwdformat = /(?!.*[\u4E00-\u9FA5\s])(?!^[a-zA-Z]+$)(?!^[\d]+$)(?!^[^a-zA-Z\d]+$)^.{6,20}$/;*/
	var regx1= /^[0-9A-z`~!@#$%^&*()_\-+=<>?:"{}|,.\/;'\\[\]·~！@#￥%……&*（）——\-+={}|《》？：“”【】、；‘’，。、]+$/g;
    var regx= /(?!^[0-9]+$)(?!^[A-z]+$)(?!^[~\\`!@#$%\\^&*\\(\\)-_+={}|\\[\\];':\\\",\\.\\\\\/\\?]+$)(?!^[^A-z0-9]+$)^.{6,20}$/;
    if(password.indexOf(" ") > -1){
    	$("#password").parent().find(".err-msg").html("密码不能包含空格");
		$("#password").parent().find(".err-msg").show();
		result = false;
    }
    if (password.length < 6 || password.length > 20) {
    	$("#password").parent().find(".err-msg").html("为了您的账号安全，密码长度在 6-20 个字符之间");
		$("#password").parent().find(".err-msg").show();
		result = false;
    }
	if(!regx1.test(password) || !regx.test(password)){
		$("#password").parent().find(".err-msg").html("密码由6-20位字母、数字或符号的两种及以上组成");
		$("#password").parent().find(".err-msg").show();
		result = false;
	}
	return result;
}

function checkPasswd2(){
	var result = true;
	var password = $("#password").val();
	var password2 = $("#password2").val();
	if(password!=password2){
		$("#password2").parent().find(".err-msg").html("两次输入的密码不一致");
		$("#password2").parent().find(".err-msg").show();
		result = false;
	}
	return result;
}

function clearErr(ths){
	$(ths).parent().find(".err-msg").hide();
}