jQuery(document).ready(function(){
   jQuery(".area_box_main>ul>li").each(function(){
      var count=jQuery(this).find(".area_level :checkbox[checked]").length;
	  if(count>0){
	    jQuery(this).find("b").html("("+count+")");
	  }
   });

   jQuery(".area_box_main li span").click(function(){
      jQuery(".area_box_main li").removeClass("this");
	  jQuery(".area_level").hide();
	  jQuery(this).parent().addClass("this");
      jQuery(this).parent().find(".area_level").show();
   });

   jQuery(".area_before>:checkbox").click(function(){
	  if(jQuery(this).attr("checked")=="checked"){
        jQuery(this).parents("div[class='area_bg_white']").find(":checkbox[disabled!=true]").attr("checked",true);
		jQuery(this).parents("div[class='area_bg_white']").find(":checkbox[id^=province_]").each(function(){
		   var count=jQuery(this).parent().find(":checkbox[id^=city_][disabled!=true]").length;
		   if(count>0){
		      jQuery(this).parent().find("b").html("("+count+")");
		   }
		})
	  }else{
	    jQuery(this).parents("div[class='area_bg_white']").find(":checkbox[disabled!=true]").attr("checked",false);
		jQuery(this).parents("div[class='area_bg_white']").find(":checkbox[id^=province_]").each(function(){
		   jQuery(this).parent().find("b").html("");
		})
	  }
   });
   jQuery(":checkbox[id^=province_]").click(function(){
      if(jQuery(this).attr("checked")=="checked"){
		  jQuery(this).parent().find(":checkbox[id^=city_][disabled!=true]").attr("checked",true);
	      var count=jQuery(this).parent().find(":checkbox[id^=city_][disabled!=true]").length;
		  if(count>0){
		    jQuery(this).parent().find("b").html("("+count+")");
		  }
	  }else{
		  jQuery(this).parent().find(":checkbox[id^=city_][disabled!=true]").attr("checked",false);
	      jQuery(this).parent().find("b").html("");
	  }
	  var count=jQuery(this).parents("div[class='area_bg_white']").find(":checkbox[id^=province_][checked]").length;
      var total_count=jQuery(this).parents("div[class='area_bg_white']").find(":checkbox[id^=province_]").length;
	  if(count==total_count){
	     jQuery(this).parents("div[class='area_bg_white']").find(":checkbox[id^=group_]").attr("checked",true);
	  }else{
	     jQuery(this).parents("div[class='area_bg_white']").find(":checkbox[id^=group_]").attr("checked",false);
	  }
   });

   jQuery(":checkbox[id^=city_]").click(function(){
      var count=jQuery(this).parents("div[class='area_level']").find(":checkbox[id^=city_][checked]").length;
	  jQuery(this).parents("div[class='area_level']").prev().find("b").html("("+count+")");
	  var total_count=jQuery(this).parents("div[class='area_level']").find(":checkbox[id^=city_]").length;
	  if(count==total_count){
	    jQuery(this).parents("div[class='area_level']").parent().find(":checkbox[id^=province_]").attr("checked",true);
	  }else{
	    jQuery(this).parents("div[class='area_level']").parent().find(":checkbox[id^=province_]").attr("checked",false);
	  }
	  var p_count=jQuery(this).parents("div[class='area_bg_white']").find(":checkbox[id^=province_][checked]").length;
      var p_total_count=jQuery(this).parents("div[class='area_bg_white']").find(":checkbox[id^=province_]").length;
	  if(p_count==p_total_count){
	     jQuery(this).parents("div[class='area_bg_white']").find(":checkbox[id^=group_]").attr("checked",true);
	  }else{
	     jQuery(this).parents("div[class='area_bg_white']").find(":checkbox[id^=group_]").attr("checked",false);
	  }
   });

})
function generic_area(){

  var trans_city_type=jQuery("#trans_city_type").val();
  var trans_index=jQuery("#trans_index").val();
  var citys="";
  var city_ids="";
  jQuery(":checkbox[id^=city_][checked]").each(function(){
     citys=jQuery(this).attr("city_name")+"、"+citys;
	 city_ids=jQuery(this).val()+","+city_ids;
  });

  city_ids = city_ids.substring(0,city_ids.length-1);
  citys = citys.substring(0, citys.length-1);

  var the_id=trans_city_type+trans_index;
  var city_id=trans_city_type+"_city_ids"+trans_index;
  var city_names=trans_city_type+"_city_names"+trans_index;


  var newArry=city_ids.split(",");

  //判断是否存在该城市
  var conf=false;
  $("#"+trans_city_type+"Table tr").each(function(index,element){
	 var aaa= $(this).find("[id^='"+trans_city_type+"_city_ids']").val();
    if(!isBlank(aaa)){
      if (trans_index != index){
        var arry=aaa.split(",");
        for (var i = 0; i < arry.length; i++) {
          var text=arry[i];
          for (var j = 0; j < newArry.length; j++) {
            var newText=newArry[j];
            if(text==newText){
              layer.msg("已存在该城市的模板了",{icon:0});
              conf=true;
            }
            if(conf){
              return false;
            }
          }
          if(conf){
            return false;
          }
        }
      }
    }
  })

  if(!conf){
	  $("#"+the_id).html(citys);
	  $("#"+city_id).val(city_ids);
	  $("#"+city_names).val(citys);
	  $(".area_box").remove();
  }
}

checkedVal()


function checkedVal() {
  let type = 'select_id'
  //if (!cityIds){
  let trans_city_type=jQuery("#trans_city_type").val();
  $("#"+trans_city_type+"Table tr").each(function(index,element){
    let aaa= $(this).find("[id^='"+trans_city_type+"_city_ids']").val();
    let num= $(this).find("[id^='"+type+"']").val();
    if (num == 1){
      if(!isBlank(aaa)){
        let array = aaa.split(",");
        for (let i = 0; i < array.length; i++) {
          $("#city_" + array[i]).attr("checked",true)
        }
      }
      $("#select_id" + index).val(0)
    }
  })
  //}
}

function isBlank(_value){
	   if(_value==null || _value=="" || _value==undefined){
	     return true;
	   }
	   return false;
}
