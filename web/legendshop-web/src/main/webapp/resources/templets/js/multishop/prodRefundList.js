jQuery(document).ready(function(){
		 //三级联动
  	  $("select.combox").initSelect();
  	userCenter.changeSubTab("myreturnmoney"); 
 });


function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("from1").submit();
}
 
function agree(id,state,subNumber){
	layer.confirm("您确定退款吗?", {
		 icon: 3
	     ,btn: ['确定','关闭'] //按钮
	   }, function(){
		   $.ajax({
				url: contextPath+"/s/updateStatus/"+id, 
				data:{"state":state,"subNumber":subNumber},
				type:'post', 
				async : true, //默认为true 异步   
				dataType:"json",
				success:function(retData){
				   if(retData=="OK"){
				     window.location.reload(true);
				     return;
				   }else{
				      layer.alert(retData,{icon:2});
				      return;
				   }
				}
			});
	   });
}

function disAgree(id,state,subNumber){
	layer.confirm("您确定取消退款吗?", {
		 icon: 3
	     ,btn: ['确定','关闭'] //按钮
	   }, function(){
		   $.ajax({
				url: contextPath+"/s/updateStatus/"+id, 
				data:{"state":state,"subNumber":subNumber},
				type:'post', 
				async : true, //默认为true 异步   
				dataType:"json",
				success:function(retData){
				   if(retData=="OK"){
				     window.location.reload(true);
				     return;
				   }else{
				      layer.alert(retData,{icon:2});
				      return;
				   }
				}
			});
	   });
}
