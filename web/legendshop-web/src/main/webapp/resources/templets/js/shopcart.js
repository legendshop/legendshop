Number.prototype.add = function (arg) {
    var m = 0, s1 = this.toString(), s2 = arg.toString(),r1,r2;
    try {
        r1 = s1.split(".")[1].length;
    } catch (e) {
        r1=0;
    };
    try {
        r2= s2.split(".")[1].length;
    } catch (e) {
        r2=0;
    };
    m=Math.pow(10,Math.max(r1,r2)+1);
    return (s1*m + s2*m)/m;
};

Number.prototype.sub = function (arg) {
    var m = 0, s1 = this.toString(), s2 = arg.toString();
    try {
        m += s1.split(".")[1].length;
    } catch (e) {
    }
    ;
    try {
        m += s2.split(".")[1].length;
    } catch (e) {
    }
    ;
    return Number(s1) - Number(s2);
};



Number.prototype.mul = function (arg) {
    var m = 0, s1 = this.toString(), s2 = arg.toString();
    try {
        m += s1.split(".")[1].length;
    } catch (e) {
    }
    ;
    try {
        m += s2.split(".")[1].length;
    } catch (e) {
    }
    ;
    return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m);
};


Number.prototype.div = function (arg) {
    var m1 = 0, m2 = 0, s1 = this.toString(), s2 = arg.toString();
    try {
        m1 += s1.split(".")[1].length;
    } catch (e) {
    }
    ;
    try {
        m2 += s2.split(".")[1].length;
    } catch (e) {
    }
    ;
    return Number(s1.replace(".", "")) / Number(s2.replace(".", "")) * Math.pow(10, m2 - m1);
};


/**
 * 判断是否是空
 * @param value
 */
function isEmpty(value){
	if(value == null || value == "" || value == "undefined" || value == undefined || value == "null"){
		return true;
	}
	else{
		value = (value+"").replace(/\s/g,'');
		if(value == ""){
			return true;
		}
		return false;
	}
}

//重新设置 全选按钮的选中状态
function resetCheckBoxSts(){
	var allCheck = true;
	//重新设置每个店铺的全选按钮
	var shopCheckBoxs = $("input[name='steelShoppingItms']").get();
	for(var i=0;i<shopCheckBoxs.length;i++){
		var _this = shopCheckBoxs[i];
		var index = $(_this).attr("for");
		var shopCheck = true;
		$(".steelShoppingCarIds_"+index).not(":disabled").each(function(){
	        var check = $(this).prop("checked");
	        if(!check){
	        	shopCheck = false;
	        	return false;
	        }
	     });
		if(shopCheck){
			$(_this).prop("checked",true);
		}else{
			allCheck = false;
			$(_this).prop("checked",false);
		}
    var len1 = $(".steelShoppingCarIds_"+index).length;
    var len2 = $("input[class='steelShoppingCarIds_"+index+"']:disabled").length;
		// console.log("steelShoppingCarIds_index_length => ", len1);
		// console.log("steelShoppingCarIds_index_disable_length => ", len2);
		// console.log(len1 === len2);
    if (len1 === len2) {
      $(_this).attr("disabled", "disabled");
      $(_this).prop("checked",false);
    }
  }

	//重新设置总的全选按钮
	if(allCheck){
		$("#checkbox").prop("checked",true);
		$("#checkbox2").prop("checked",true);
	}else{
		$("#checkbox").prop("checked",false);
		$("#checkbox2").prop("checked",false);
	}
}

jQuery(document).ready(function($) {

	resetCheckBoxSts();

    //当整个购物车只有一个或全部锁定（因库存不足或已下架）的产品时，全选框要默认为不选中的。
    if($("input[name=dd]:disabled").length == $("input[name=dd]").length){
    	$("#checkbox2").removeAttr("checked");
    	$("#checkbox").removeAttr("checked");
    	$(".select_shop").removeAttr("checked");
    }

    //全部控制--全选按钮
	$(".selectAll").live("click",function(){
		//  if($(".notChecked").size()>0){
		// 	 $("input[type=checkbox]").prop("checked",false);
		// 	 alert("您选择的商品中包含失效的商品请删除后重新选择！！！");
		// 	 return;
		// }
		$("input[name='steelShoppingItms']").not(":disabled").prop("checked",$(this).prop("checked"));
	    var check = $(this).prop("checked");
	    var goodInput = $("input[name='dd']").not(":disabled");
	    var changeStr = "";
	    goodInput.each(function(){
	    	var checkSts = 0;
	        var basketId = $(this).attr("itemkey");
	        var prodId = $(this).attr("prodId");
	        var skuId = $(this).attr("skuId");

	        if(check){
	        	checkSts = 1;
	        }

	        if(changeStr!=""){
	        	changeStr = changeStr + ";";
	        }
	        changeStr = changeStr + basketId + ":" + prodId+ ":" + skuId+ ":" + checkSts;
	    });


		batchUpdateCheckSts(changeStr);
	});

	// 店铺： items控制
	$("input[name='steelShoppingItms']").live("click",function(){
		//  if($(".notChecked").size()>0){
		// 	$("input[type=checkbox]").prop("checked",false);
		// 	alert("您选择的商品中包含失效的商品请删除后重新选择！！！");
		// 	return;
		// }
	    //往下作用
	    var flag = $(this).prop("checked");
	    var index = $(this).attr("for");

	    var changeStr = "";
	    $(".steelShoppingCarIds_"+index).not(":disabled").each(function(){
	    	var checkSts = 0;
	        var basketId = $(this).attr("itemkey");
	        var prodId = $(this).attr("prodId");
	        var skuId = $(this).attr("skuId");

	        if(flag){
	        	checkSts = 1;
	        }

	        if(changeStr!=""){
	        	changeStr = changeStr + ";";
	        }
	        changeStr = changeStr + basketId + ":" + prodId+ ":" + skuId+ ":" + checkSts;
	     });

		batchUpdateCheckSts(changeStr);
	});



	// 商品 item控制
    $("input[name='dd']").live("click",function(){

      /// sdfsdfsdf
        var checkSts = 0;
        var basketId = $(this).attr("itemkey");
        var prodId = $(this).attr("prodId");
        var skuId = $(this).attr("skuId");

        var check = $(this).prop("checked");
        if(check){
        	checkSts = 1;
        }

        updateCheckSts(basketId,prodId,skuId,checkSts);
    });

    //提交门店自提商品
    $('a[nc_type="chain"]').live('click',function(){
	    if(isLogin()){
	    	login();
	    	return;
	    }
	    var array = $(this).parent().prev().find("input[name='dd']:checked").get();

		if(array.length==0){
			layer.alert("请先选择支持门店自提的商品",{icon: 0});
			return;
		}

		 //设置总合计数量
		var totalNumber = 0;
		  //设置总合计金额
		var totalMoney = 0;

		var arrays=new Array();
		var shopId=$(this).attr("shopId");
		for(var i in array){
			if ($(array[i]).attr("supportStore")=="true" ){
		    	var index  = $(array[i]).attr("itemkey");
		    	var prodId  = $(array[i]).attr("prodId");
			    var skuId = $(array[i]).attr("skuId");
			    var number=$.trim($("#number_"+prodId+"_"+skuId).val());

			    var storeId = $(array[i]).attr("storeId");

			    if (isEmpty(storeId) || storeId == 0 ) {
			    	layer.alert("请先选择门店!",{icon: 0});
					return ;
				}

		        if(isEmpty(number) || isNaN(number) ){
		        	number=0;
		        	layer.alert("请输入正确的格式!",{icon: 2});
		        	return;
		        }
		        var money=$.trim($("#total_money_"+prodId+"_"+skuId).html()).substring(1);
		        number=Number(number);
		        totalMoney = Number(totalMoney).add(money);
		        totalNumber = Number(totalNumber).add(number);
		        arrays.push(index);
		    }
		 }
		 if(Number(totalNumber)<=0 || Number(totalMoney) <= 0){
			 layer.alert("该商品不支持门店自提服务!",{icon: 2});
			 return ;
		 }
		 $(".black_overlay").show();
	     $(".editAdd_load").show();
	     abstractForm(contextPath+'/p/store/order/storeOrderDetails', arrays,shopId);
	});

    // 鼠标悬停门店名称事件
    $('.storeInfo').hover(function(){
		var storeAddr = $(this).attr("storeAddr");
		var that = this;

		if (!isEmpty(storeAddr) ) {
			//tips
			tips = layer.tips("<span style='color:#999999;'>"+storeAddr+"</span>",that, {
		      tips: [1, '#fef2f6'],
		      time: 10000
		    });
		}

	},function(){
		 layer.close(tips);
	});




});

//更新选中状态
function updateCheckSts(basketId,prodId,skuId,checkSts){
	$(".black_overlay").show();
    $(".editAdd_load").show();
	 $.ajax({
	        type : "POST",
	        url : contextPath+"/shopCart/changeBasketSts",
	        data : {"basketId":basketId,"prodId":prodId,"skuId":skuId,"checkSts":checkSts},
	        async:false,
	        cache: false,
	        dataType : "json",
	        success : function(response) {
	        	reloadBuyTable();
	        }
	 });
}

//批量更新选中状态
function batchUpdateCheckSts(changeStr){
	$(".black_overlay").show();
    $(".editAdd_load").show();
	 $.ajax({
	        type : "POST",
	        url : contextPath+"/shopCart/batchChgBasketSts",
	        data : {"changeStr":changeStr},
	        async:false,
	        cache: false,
	        dataType : "json",
	        success : function(response) {
	        	reloadBuyTable();
	        }
	 });
}

//重新加载购物车信息
function reloadBuyTable(){
	$.ajax({
        type : "GET",
        url : contextPath+"/shopCart/buyTable",
        async:true,
        cache:false,
        dataType : "html",
        success : function(result) {
        	 $("#bd").html(result);
        	 $(".black_overlay").hide();
             $(".editAdd_load").hide();
             resetCheckBoxSts();
             // window.location.reload();
        }
	});
}

//商品数量 onblur
function blurCalculate(_this,prodId,skuId,max){
	 $(_this).css({"border":"1px solid #a7a6ac"});
	 var buyNumValue=$.trim($(_this).val());
	 var patrn = /^([+]?)(\d+)$/;
	 if(isEmpty(buyNumValue) || isNaN(buyNumValue)){
		 $(_this).css({"border":"1px solid #cc131d"});
		 return;
	 }else if(patrn.test(buyNumValue) == false){
		 $(_this).css({"border":"1px solid #cc131d"});
		 return;
	 }else if(Number(buyNumValue)>max){
		 buyNumValue=max;
	 } else if(Number(buyNumValue).sub(1)<=0){
		 buyNumValue=1;
	 }
	 updateBasketCount(_this,prodId,skuId,Number(buyNumValue));
}


//点击增加商品数据
function addQuanlity(_this,prodId,skuId,max){
	var number=$(_this).parent().prev();
	$(number).css({"border":"1px solid #a7a6ac"});
	var buyNumValue=$.trim($(number).val());
	if(isEmpty(buyNumValue) || isNaN(buyNumValue)){
		$(number).css({"border":"1px solid #cc131d"});
	    return;
	}else if(Number(buyNumValue).add(1)>max){
		return;
	}
	var count=Number(buyNumValue).add(1);

	updateBasketCount(number,prodId,skuId,count);
}

//点击减少商品数据
function subQuanlity(_this,prodId,skuId){
	var number=$(_this).parent().prev();
	$(number).css({"border":"1px solid #a7a6ac"});
	var buyNumValue=$.trim($(number).val());
	if(isEmpty(buyNumValue) || isNaN(buyNumValue)){
		$(number).css({"border":"1px solid #cc131d"});
	     return;
	}else if(Number(buyNumValue).sub(1)<=0){
		return;
	}
	var count=Number(buyNumValue).sub(1);

	updateBasketCount(number,prodId,skuId,count);
}

//更改购物车商品数量
function updateBasketCount(_number,prodId,skuId,buyNumValue) {
	if (parseInt(buyNumValue) <= 0){
   	   $(_number).css({"border":"1px solid #cc131d"});
	   return;
    }else{
       $(".black_overlay").show();
 	   $(".editAdd_load").show();
	   var params={
				"productId":prodId,
				"skuId":skuId,
				"basketCount":buyNumValue
		};
	    $.ajax({
	        type : "POST",
	        url : contextPath+"/shopCart/updateBasketCount",
	        data : params,
	        async:true,
	        cache: false,
	        dataType : "json",
	        success : function(result) {
	        	 if(result.buySts=="OK"){
	        		 reloadBuyTable();
	        	 }else{
	        		 $(".black_overlay").hide();
		       		 $(".editAdd_load").hide();
	        		 layer.tips(result.buyMsg,{icon: 2});
	        	 }

	        }
	    });
   }
}



//得判断是否为数字的同时，也判断其是不是正数
function validateInput(obj){
	var pamount = $(obj).val();
	 var re = /^[1-9]+[0-9]*]*$/;
	 if( isNaN(pamount) || ! re.test(pamount)) {
		 //错误时
	 	$(obj).attr("style","border: 1px solid #f40000;");
	 	return;
	 }else if(pamount>=9999){
		 $(obj).attr("style","border: 1px solid #f40000;");
		 return;
	 }
	 var basket_id=$(obj).attr("itemkey");
	 var prod_id=$(obj).attr("prodId");
	 var sku_id=$(obj).attr("skuId");
	 var result = changeShopCartNumber(basket_id,pamount,prod_id,sku_id,0);
	 if(!result){
		 $(obj).attr("style","border: 1px solid #f40000;");
		 return;
	 }
	  $(obj).attr("style","");

	  calculateTotal();
}


//单个删除操作
function deleteShopCart(prodId,skuId){
	layer.confirm("删除后不可恢复, 确定要删除吗？", {
		  icon: 3
	     ,btn: ['确定','关闭'] //按钮
	   }, function(index, layero){
		   $(".black_overlay").show();
		   $(".editAdd_load").show();
		 $.ajax({
				url: contextPath+"/shopCart/deletShopCart/"+prodId+"/"+skuId,
				type:'post',
				async : true, //默认为true 异步
				cache: false,
				dataType : 'json',
				success:function(data){
					if(data=="OK"){
						reloadBuyTable();
					}else{
						$(".black_overlay").hide();
			       		 $(".editAdd_load").hide();
						layer.tips("删除失败",{icon: 2});
					}
				}
		});
		  layer.close(index);
	   });
}


/**
 * 清空购物车
 */
function clearShopCart(){

	layer.confirm("确定要清空购物车吗？", {
		 icon: 3
	     ,btn: ['确定','关闭'] //按钮
	   }, function(){
		   $.ajax({
				url: contextPath+"/shopCart/clearShopCart",
				type:'post',
				async : true, //默认为true 异步
				cache: false,
				dataType : 'json',
				success:function(data){
					if(data=="OK"){
						window.location.reload(true);
						return ;
					}else{
						layer.tips("删除失败",{icon: 2});
						return false;
					}
				}
		    });
	   });
}



function summitShopCart(){
	 if(isLogin()){
		 login();
		 return;
	 }
	 var arrays=new Array();
	 //设置总合计数量
	 var totalNumber = 0;
	  //设置总合计金额
	 var totalMoney = 0;

	 var falg = true;
	 $(".select_prod").each(function(){
		    if ($(this).prop("checked") == true || $(this).prop("checked") == "checked"){
		    	var index  = $(this).attr("itemkey");
		    	var prodId  = $(this).attr("prodId");
			    var skuId = $(this).attr("skuId");
			    var patrn = /^([+]?)(\d+)$/;
			    var number=$.trim($("#number_"+prodId+"_"+skuId).val());
		        if(isEmpty(number) || isNaN(number) || patrn.test(number)==false ){
		        	number=0;
		        	layer.alert("请输入正确的格式!",{icon: 2});
		        	falg = false;
		        	return;
		        }
		        var money=$.trim($("#total_money_"+prodId+"_"+skuId).html()).substring(1);
		        number=Number(number);
		        totalMoney = Number(totalMoney).add(money);
		        totalNumber = Number(totalNumber).add(number);
		        arrays.push(index);
		    }
	 });

	 if(!falg){
		 return;
	 }

	 if(arrays.length<=0){
			layer.alert("没有选中商品!",{icon: 0});
			return;
	 }
	 if(Number(totalNumber)<=0 || Number(totalMoney) < 0){
		 layer.alert("数据金额有误!",{icon: 0});
		 return ;
	 }
	 var allProductTotalAmount=$.trim($("#allProductTotalAmount").html()).substring(1);
	 if(Number(allProductTotalAmount) < 0){
		 layer.alert("数据金额有误!",{icon: 0});
		 return ;
	 }
	 $(".black_overlay").show();
     $(".editAdd_load").show();

	 abstractCartItemsForm(contextPath+'/p/orderDetails', arrays);
}

function abstractCartItemsForm(URL, chains){
	   var temp = document.createElement("form");
	   temp.action = URL;
	   temp.method = "post";
	   temp.style.display = "none";
	   var opt = document.createElement("input");
	   opt.name = 'shopCartItems';
	   opt.value = chains;
	   temp.appendChild(opt);
	   temp.appendChild(appendCsrfInput());
	   document.body.appendChild(temp);
	   temp.submit();
	   return temp;
}

function abstractForm(URL, chains,shopId){
	   var temp = document.createElement("form");
	   temp.action = URL;
	   temp.method = "post";
	   temp.style.display = "none";
	   var opt = document.createElement("input");
	   opt.name = 'ids';
	   opt.value = chains;
	   temp.appendChild(opt);
	   var opt = document.createElement("input");
	   opt.name = 'shopId';
	   opt.value = shopId;
	   temp.appendChild(opt);
	   var opt = document.createElement("input");
	   opt.name = 'buyNow';
	   opt.value = false;
	   temp.appendChild(opt);
	   temp.appendChild(appendCsrfInput());
	   document.body.appendChild(temp);
	   temp.submit();
	   return temp;
}

//得判断是否为数字的同时，也判断其是不是正数
function isLogin() {
	if(loginUserName=="" || loginUserName==undefined || loginUserName==null){
		return true;
	}
	return false;
}

function login(){
	layer.open({
		title :"登录",
		  type: 2,
		  content: contextPath + '/loadLoginOverlay', //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['440px', '530px']
		});
}

//门店选择弹层
function shopStoreServer(prodId,skuId,basketCount){
	layer.open({
		title :"门店选择",
		type: 2,
		content: contextPath + 'shopStoreServer?prodId=' +prodId + '&skuId='+skuId+'&basketCount='+basketCount ,//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		area: ['660px', '560px']
    });
}




