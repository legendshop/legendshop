jQuery(document).ready(function(){
	//三级联动
	$("select.combox").initSelect();
	userCenter.changeSubTab("joinDistProd");

	//全选
  	$(".selectAll").click(function(){

  		if($(this).attr("checked")=="checked"){
  			$(this).parent().parent().addClass("checkbox-wrapper-checked");
  	         $(".selectOne").each(function(){
  	        		 $(this).attr("checked",true);
  	        		 $(this).parent().parent().addClass("checkbox-wrapper-checked");
  	         });
  	     }else{
  	         $(".selectOne").each(function(){
  	             $(this).attr("checked",false);
  	             $(this).parent().parent().removeClass("checkbox-wrapper-checked");
  	         });
  	         $(this).parent().parent().removeClass("checkbox-wrapper-checked");
  	     }
  	 });

  	function selectOne(obj){
  		if(!obj.checked){
  			$(".selectAll").checked = obj.checked;
  			$(obj).prop("checked",false);
  			$(obj).parent().parent().removeClass("checkbox-wrapper-checked");
  		}else{
  			$(obj).prop("checked",true);
  			$(obj).parent().parent().addClass("checkbox-wrapper-checked");
  		}
  		 var flag = true;
  		  var arr = $(".selectOne");
  		  for(var i=0;i<arr.length;i++){
  		     if(!arr[i].checked){
  		    	 flag=false;
  		    	 break;
  		    	 }
  		  }
  		 if(flag){
  			 $(".selectAll").prop("checked",true);
  			 $(".selectAll").parent().parent().addClass("checkbox-wrapper-checked");
  		 }else{
  			 $(".selectAll").prop("checked",false);
  			 $(".selectAll").parent().parent().removeClass("checkbox-wrapper-checked");
  		 }
  	}
});

function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("from1").submit();
}

//添加分销商品
function loadWantDistProd(){

	layer.open({
		title :"选择要加入分销的商品",
		id: "loadWantDistProd",
		type: 2,
		resize: false,
		content: [contextPath+"/s/loadWantDistProd",'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		area: ['700px', '520px']
	});
}


function quitDist(prodId){
	layer.confirm("确定将该商品退出分销吗？",{
		icon: 3
		,btn: ['确定','取消'] //按钮
	},
	function(){
		if(prodQuitDist(prodId)){
			window.location =contextPath+"/s/joinDistProd";
		}
	});
}

function prodQuitDist(prodId){
	var result = false;
	$.ajax({
		url: contextPath+"/s/quitDist",
		data: {"prodId":prodId},
		type:'post',
		async : false, //默认为true 异步
		dataType : 'json',
		success:function(data){
			if(data=="OK"){
				result = true;
			}else{
				layer.alert('退出失败', {icon: 2});
			}
		}
	});
	return result;
}



function batchQuitDist(){
	//获取选择的记录集合
	var selAry = $(".selectOne");
	if (!checkSelect(selAry)) {

		layer.msg('至少选中一条记录', {icon: 0});
		return false;
	}
	layer.confirm("确定要把选中的商品退出分销吗？", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	},function() {
		for (i = 0; i < selAry.length; i++) {
			if (selAry[i].checked) {
				var prodId = selAry[i].value;
				prodQuitDist(prodId);
			}
		}
		window.location =contextPath+"/s/joinDistProd";
	}, function() {
		//cancel
	});

	return true;
}
//jsp调用已经注释了
function updateCommisRate(prodId,originCommisRate,prodPic){
	var img = "<ls:images item='"+prodPic+"' scale='3'/>";
	layer.open({
		  title :"修改佣金比例",
		  type: 1,
		  content:"<img style='margin-left: 45px;' src='"+img+"'> <br>设置佣金比例(百分比): <input style='width:30px' id='distCommisRate' value='"+originCommisRate*100+"'>%",
		  btn: ['确定','关闭'],
		  yes:function(){
			  var regex = /^\d+$/;
		      	var commisRate = $("#distCommisRate").val().trim();
		      	if(commisRate==""){
		      		layer.alert('请输入1-99之间的整数', {icon: 0});
		      		return false;
		      	}else if(!regex.test(commisRate)){
		      		layer.alert('请输入1-99之间的整数', {icon: 0});
		      		return false;
		      	}else if(commisRate<1 || commisRate>99){
		      		layer.alert('请输入1-99之间的整数', {icon: 0});
		      		return false;
		      	}else{
		      		$.ajax({
						url: contextPath+"/s/addDistProd",
						data: {"prodId":prodId,"distCommisRate":commisRate/100},
						type:'post',
						async : true, //默认为true 异步
						dataType : 'json',
						success:function(data){
							if(data=="OK"){
								parent.window.location =contextPath+"/s/joinDistProd";
							}else{
								layer.alert('修改失败', {icon: 2});
							}
						}
					});
		      	}
		  }
	});
}
function search(){
  	$("#from1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
  	$("#from1")[0].submit();
}
