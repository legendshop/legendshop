 $(document).ready(function() {
			      loadApyInfo();
		    });
		       
               var applyreturn = function(id,orderNum){
				    var input_checkBoxs = $("input:[name=sub_"+id+"]:checked");
				    if(input_checkBoxs.length <= 0){
				       layer.alert("请勾选订单编号为"+orderNum+"的商品进行退换货",{icon: 0});
				        return false;
				    }else{
				        var $Arr = new Array();
				        input_checkBoxs.each(function(i){
				            $Arr.push(Number($(this).val()));
				        });
				        return;
				    }
				    window.location.href=contextPath+"/p/applyPurchase/"+orderNum+"?prods="+$Arr;
				};
				
				function alertApplyPayAddressDiag(id){
					layer.open({
						title :"添加用户退款信息",
						  type: 2, 
						  content: contextPath +  '/p/loadApplyPayInfo/' + id, //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
						  area: ['700px', '450px']
						}); 
				}
				
				
				function loadApyInfo(){
				   $("#payinfo").load(contextPath + "/p/loadApplyPayInfo");
				}
				
				
				 function delImagesPath(obj){
				    $(obj).parent().remove();
				    $.ajax({
						"url":contextPath + "/p/delReturnOrderFile" , 
						data:{"fileName":$(obj).attr("fileName")},
						type:'post', 
						dataType: 'json',
						async : true, //默认为true 异步   
						 success:function(result){
						 }
				      });
				 }
				 
		         var sys=0;
				 function saveReturnOrder(){
				   if(sys!=0){
				   layer.alert("请勿重复操作！",{icon: 0});
				   return false;
				   }
				   
				   
				   //退款金额
					var orderTotalPrice=$("#orderTotalPrice").val();
					if(isBlank(orderTotalPrice)||Number(orderTotalPrice)==0){
						layer.alert("退款金额不可为空或0！",{icon: 0});
						return false;
					}
					
					var reg=/^[-+]?\d+(\.\d+)?$/;  
					if(!reg.test(orderTotalPrice)){
						layer.alert("请输入正确数值！",{icon: 0});
						return false;
					}
					
					
					//退款上限
					var cash=$("#cash").val();
					if((Number(orderTotalPrice)-Number(cash))>0){
						layer.alert("退款金额不可超过"+cash+"元",{icon: 0});
						return false;
					}
				   
				   var desc=$.trim($("#desc").val());
				   if(isBlank(desc)){
				     layer.alert("请输入问题描述",{icon: 0});
				     return false;
				   }else if(desc.length>512){
				     layer.alert("输入的字数过多",{icon: 0});
				     return false;
				   }
				   
				   var uploadNumber=$(".img-files-img ul li").length;
				   if(uploadNumber==0){
				      layer.alert("请上传图片凭证！",{icon: 0});
					  return false;
				   }
				   if (uploadNumber > 4) {
					  layer.alert("您最多可以上传3张图片",{icon: 0});
					  return false;
					}
						
				   var imagesPaths = new Array();
					$(".img-files-img ul li").each(function() {
						var $this = jQuery(this);
						var path = $this.find("input[name='imgurl']").val();
						if(!isBlank(path)){
							imagesPaths.push(path);
						}
					});
					
				   var payinfo=$("#payinfo .banks").length;
				   if(payinfo==0){
				      layer.alert("请添加退款信息",{icon: 0});
				      return false;
				   }
				   var dialCheckResult=$("input:radio[name='userInfo']:checked").val();
				   if(isBlank(dialCheckResult)){
				     layer.alert("请选择用户退款帐号信息",{icon: 0});
				      return false;
				   }
				   
					if(isBlank(subNum) || isBlank(prods)){
						layer.alert("数据有问题",{icon: 0});
						return false;
					}
					
					var data={
					  "subNum":subNum,
					  "prods":prods,
					  "applyUserInfoId":dialCheckResult,
					  "imagesPaths":JSON.stringify(imagesPaths),
					  "desc":desc,
					  "orderTotalPrice":orderTotalPrice
					};
					sys=1;
				    $.ajax({
						"url":contextPath + "/p/applyReturnOrder" , 
						data:data,
						type:'post', 
						dataType: 'json',
						async : false, //默认为true 异步   
						error: function(jqXHR, textStatus, errorThrown) {
						    sys=0;
						},
						 success:function(result){
						    if(result=="fail"){
						       layer.msg('提交退货申请失败',{icon: 2});
						       	 sys=0;
						       	return false;
						    }else if(result=="OK"){
						      layer.alert("提交退货申请成功,请前往退货记录查看详情",{icon: 1},function(){
						    	  window.location.href=contextPath+"/p/myreturnOrder";
						     		 return true;
						      })
						    }
						 }
				      
				      });
				 };
				 
				 function isBlank(_value){
					if(_value==null || _value=="" || _value==undefined){
						return true;
					}
					return false;
				}
