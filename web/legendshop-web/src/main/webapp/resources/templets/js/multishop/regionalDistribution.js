$(function(){
	operatingReport.doExecute();
	userCenter.changeSubTab("operatingReport");

	var queryTerms = $("#queryTerms").val();
	change(queryTerms);

});

//路径配置
require.config({
	paths: {
        echarts: contextPath+'/resources/plugins/ECharts/dist'
    }
});

//使用
require(
		[
	        'echarts',
	        'echarts/chart/map'
	    ],
	    function (ec) {
			// 基于准备好的dom，初始化echarts图表
	        var myChart = ec.init(document.getElementById('main'));

	        option = {
	        	    tooltip : {
	        	        trigger: 'item'
	        	    },
	        	    toolbox: {
	        	        show : true,
	        	        orient: 'vertical',
	        	        x:'right',
	        	        y:'center',
	        	        feature : {
	        	            mark : {show: true},
	        	            dataView : {show: true, readOnly: false}
	        	        }
	        	    },
	        	    series : [
	        	        {
	        	            tooltip: {
	        	                trigger: 'item',
	        	                formatter: '{b}'
	        	            },
	        	            name: '选择器',
	        	            type: 'map',
	        	            mapType: 'china',
	        	            mapLocation: {
	        	                x: 'left',
	        	                y: 'top',
	        	                width: '30%'
	        	            },
	        	            roam: true,
	        	            selectedMode : 'single',
	        	            itemStyle:{
	        	                //normal:{label:{show:true}},
	        	                emphasis:{label:{show:true},color:'#77DDFF'}
	        	            },
	        	            data:function (){
	        	            	var list = [];
	        	            	var provinceJson = $("#provinceJson").val();
	        	            	if(!isBlank(provinceJson)){
	        	            		var provinceList = jQuery.parseJSON(provinceJson);
		                            for(var z=0;z<provinceList.length;z++){
		                            	var obj = new Object();
		                            	obj.name=provinceList[z].shortName;
		                            	obj.selected=false;
		                            	list.push(obj);
		                            }
	        	            	}
	                            return list;
	        	            }()
	        	        }
	        	    ],
	        	    animation: false
	        	};
	        	var ecConfig = require('echarts/config');
            console.log(ecConfig);
	        	myChart.on(ecConfig.EVENT.MAP_SELECTED, function (param){
	        	    var selected = param.selected;
	        	    var selectedProvince;
	        	    var name;
	        	    for (var i = 0, l = option.series[0].data.length; i < l; i++) {
	        	        name = option.series[0].data[i].name;
	        	        option.series[0].data[i].selected = selected[name];
	        	        if (selected[name]) {
	        	            selectedProvince = name;
	        	        }
	        	    }
	        	    if (typeof selectedProvince == 'undefined') {
	        	        option.series.splice(1);
	        	        option.legend = null;
	        	        option.dataRange = null;
	        	        myChart.setOption(option, true);
	        	        return;
	        	    }
	        	    option.series[1] = {
	        	        name: '订单数量',
	        	        type: 'map',
	        	        mapType: selectedProvince,
	        	        itemStyle:{
	        	            normal:{label:{show:true}},
	        	            emphasis:{label:{show:true},color:'#77DDFF'}
	        	        },
	        	        mapLocation: {
	        	            x: '35%'
	        	        },
	        	        roam: true,
	        	        data:function (){
	        	        	var list = [];
	        	        	var cityJson = $("#cityJson").val();
	        	        	if(!isBlank(cityJson)){
	        	        		var cityList = jQuery.parseJSON(cityJson);
                        for(var x=0;x<cityList.length;x++){
                          var obj = new Object();
                          obj.name=cityList[x].city;
                          obj.value=cityList[x].subNumber;
                          list.push(obj);
                        }
                      }
                      return list;
	        	        }()

	        	    };
	        	    option.legend = {
	        	        x:'right',
	        	        data:['订单数量']
	        	    };
	        	    option.dataRange = {
	        	        orient: 'horizontal',
	        	        x: 'right',
	        	        min: 0,
	        	        max: 100,
	        	        color:['#009FCC','#CCEEFF'],
	        	        text:['高','低'],           // 文本，默认为数值文本
	        	        splitNumber:0
	        	    };
	        	    myChart.setOption(option, true);
	        	})


	     	// 为echarts对象加载数据
	        myChart.setOption(option);
		}
);

/**方法，判断是否为空 **/
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}
