$("#button").click(function(){
	var stocks=$("#stocks").val();
	var prodId=$(".prodId").val();
	if(isBlank(stocks)){
		layer.msg("库存不能为空",{icon:0});
		return;
	}
	var patrn = /^[0-9]*[0-9][0-9]*$/;  //验证是否为数字
	if(!patrn.test(stocks)){
		layer.msg("库存不能为负",{icon:0});
		return;
	}
	$.ajax({
		type: "post",
		data:{"skuId":$("#skuId").val(),"stocks":stocks,"prodId":prodId},
		url: contextPath + "/s/stock/update",
		dataType: "json",
		async : true, //默认为true 异步   
		success: function(msg){
			win.location.reload();
		}
	});	
});

function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}