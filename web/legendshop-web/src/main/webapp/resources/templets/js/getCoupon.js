function isBlank(value){
	return value==null || value.trim()=="";
}

function changecodeimage(){
	var obj = document.getElementById("codeimage");
	obj.src = contextPath + "/validCoderRandom?d=" + (new Date()).valueOf();
}

function activateCoupon(){
	var url = contextPath+"/p/activateCoupon";
	var couponPwd = $("#couponPwd").val();
	var verifyCode = $("#captcha").val();
	var couPro = $("#couPro").val();
	if(isBlank(couponPwd)){
		layer.msg("请输入激活码！",{icon:0});
		return;
	}
	if(isBlank(verifyCode)){
		layer.msg("请输入验证码！",{icon:0});
		return;
	}
	$.ajax({
		url : url,
		type : "POST",
		data : {"couPro":couPro,"couponPwd":couponPwd,"verifyCode":verifyCode},
		async : true,
		dataType:"json",
		success : function(msg){
			if(msg == "OK"){
				layer.msg("领取成功！",{icon:1,time:1000},function(){
					window.location.href=contextPath+"/p/coupon?useStatus=1&couPro=" + couPro;
				});
			}else if(msg == "errorCode"){
				layer.msg("请输入正确验证码！",{icon:0});
			}else if(msg == "actiError"){
				layer.msg("请输入正确激活码！",{icon:0});
			}else if(msg == "limit"){
				layer.msg("您领取已达上限！",{icon:0});
			}else if(msg == "activity_end"){
				layer.msg("活动已结束！",{icon:0});
			}else if(msg == "nothing"){
				layer.msg(couponName+"已领完！",{icon:0});
			}else if(msg == "owner"){
				layer.msg("来晚了，"+couponName+"已被领了！",{icon:0});
			}else if(msg == "offline"){
				layer.msg(couponName+"已失效！",{icon:0});
			}else{
				// layer.msg("未知错误！",{icon:2});
			}
			changecodeimage();
		}
	});
}
