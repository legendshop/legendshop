$(document).ready(function() {
	userCenter.changeSubTab("auctionManage"); //高亮菜单

	$("#statusTab>span").click(function(){
		$("#statusTab>span").removeClass("on");
		$(this).addClass("on");
		var searchType = $(this).attr("value");
		$("#searchType").val(searchType);
		console.log($(this).children("a"));
    $(this).children("a")[0].click();
	});
});

function onlineShopAuction(_id) {
	layer.confirm("确定要发布该活动么？", {
		icon: 3
		,btn: ['确定','取消'] //按钮
	},function() {
		jQuery.ajax({
			url:contextPath+"/s/auction/updatestatus/"+ _id +"/1",
			type:'POST',
			async : false, //默认为true 异步
			dataType : 'json',
			success:function(data){
				if(data=="OK"){
					layer.alert('发布成功', {icon: 1},function(){
						window.location.reload();
					});

				}else{
					layer.alert(data, {icon: 0});
				}
			}
		});

	});
}
function offlineShopAuction(_id) {
	layer.confirm("确定要终止该活动么？", {
		icon: 3
		,btn: ['确定','取消'] //按钮
	},function() {
		jQuery.ajax({
			url:contextPath+"/s/auction/updatestatus/"+ _id +"/0",
			type:'POST',
			async : false, //默认为true 异步
			dataType : 'json',
			success:function(data){
				if(data=="OK"){
					layer.alert('终止成功', {icon: 1},function(){
						window.location.reload();
					});
				}else{
					layer.alert(data, {icon: 0});
				}
			}
		});
	});
}
function deleteShopAuction(_id){
	layer.confirm("确定要删除活动么？", function () {
		jQuery.ajax({
			url:contextPath+"/s/auction/delete/"+ _id,
			type:'POST',
			async : false, //默认为true 异步
			dataType : 'json',
			success:function(data){
				if(data=="OK"){
					layer.alert('删除成功', {icon: 1},function(){
						window.location.reload();
					});
				}else{
					layer.alert(data, {icon: 0});
				}
			}
		});
	});
}

function updateShopAuction(_id){
	window.location.href=contextPath+"/s/auction/updateAuction/"+_id;
}
