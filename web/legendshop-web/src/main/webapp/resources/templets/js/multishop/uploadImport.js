function confirmUpload(){
	var path=$("#file").val();
	var filename=path.split(".")[1];
	var data='${data}';
	if(path.length>0){
		if(data=="1"){
			if(filename=="xls"||filename=="xlsx"){
				var url=contextPath+"/s/prod/importprod";
				ajaxSubmitForm(url);
			}else{

				layer.msg("请选择excel文件",{icon:0});
			}
		}else{
			if(filename=="csv"){
				var url=contextPath + "/s/prod/importtaobao";
				ajaxSubmitForm(url);
			}else{
				layer.msg("请选择CVS文件",{icon:0});
			}
		}
	}else{
		layer.msg("请选择文件",{icon:0});
	}
}

function ajaxSubmitForm(url){
	$("#form1").attr("action",url);
	$("#form1").ajaxForm().ajaxSubmit({
		success:function(result) {
			var result=eval(result);
			if("OK" == result){
				layer.alert("导入成功",{icon:1},function(){
					var index = parent.layer.getFrameIndex('uploadImport'); //先得到当前iframe层的索引
					parent.layer.close(index); //再执行关闭
					window.location.reload();

				});

			}else{
				layer.msg(result,{icon:0});
			}
		}
	});

}
