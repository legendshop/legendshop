function isBlank(_value) {
	if (_value == null || _value == "" || _value == undefined) {
		return true;
	}
	return false;
}
$(document).ready(function () {
	$("#jifenLi").addClass("focus");
	$("#txtStartPonit").val(data.startIntegral);
	$("#txtEndPoint").val(data.endIntegral);
	$("#txtGoodsName").val(data.keyWord);

	$(".nch-sortbar-array ul li").bind("click", function () {
		var id = $(this).attr("id");
		var txtStartPonit = $.trim($("#txtStartPonit").val());
		var txtEndPoint = $.trim($("#txtEndPoint").val());
		var txtGoodsName = $.trim($("#txtGoodsName").val());
		var orderDir = "";
		$(".nch-sortbar-array ul li").each(function (i) {
			if (id != $(this).attr("id")) {
				$(this).removeClass("selected");
			}
		});
		$(this).addClass("selected");
		var _a = $(this).find("a");
		if (id == 'saleNum') {
			if ($(_a).hasClass("desc")) {
				orderDir = "saleNum,asc";
			} else if ($(_a).hasClass("asc")) {
				orderDir = "saleNum,desc";
			} else {
				$(_a).removeClass("asc");
				orderDir = "saleNum,desc";
			}
		} else if (id == 'exchangeIntegral') {
			if ($(_a).hasClass("desc")) {
				orderDir = "exchangeIntegral,asc";
			} else if ($(_a).hasClass("asc")) {
				orderDir = "exchangeIntegral,desc";
			} else {
				orderDir = "exchangeIntegral,desc";
			}
		}
		data.startIntegral = txtStartPonit;
		data.endIntegral = txtEndPoint;
		data.integralKeyWord = txtGoodsName;
		data.orders = orderDir;
		$("#main-nav-holder").load(contextPath+"/integral/prodList", data,
				function () {
		});

	});

});

function pager(curPageNO) {
	data.curPageNO = curPageNO;
	$("#main-nav-holder").load(contextPath+"/integral/prodList", data,
			function () {
	});
}

function searchIntegral() {
	var txtStartPonit = $.trim($("#txtStartPonit").val());
	var txtEndPoint = $.trim($("#txtEndPoint").val());
	var txtGoodsName = $.trim($("#txtGoodsName").val());

	if (!isBlank(txtStartPonit) && isNaN(txtStartPonit)) {
		layer.msg("请输入正确的格式!",{icon:0});
		$("#txtStartPonit").focus();
		return false;
	}

	if (!isBlank(txtEndPoint) && isNaN(txtEndPoint)) {
		layer.msg("请输入正确的格式!",{icon:0});
		$("#txtEndPoint").focus();
		return false;
	}
	data.startIntegral = txtStartPonit;
	data.endIntegral = txtEndPoint;
	data.integralKeyWord = txtGoodsName;
	data.orders = "";
	$("#main-nav-holder").load(contextPath+"/integral/prodList", data,
			function () {
	});
}

//清除搜索条件
function clearCondition() {
	$("#txtStartPonit").val("");
	$("#txtEndPoint").val("");
	$("#txtGoodsName").val("");
}