$(document).ready(function() {
	userCenter.changeSubTab("auctionManage"); //高亮菜单
	$('.bidding-des-con img').attr("style","width: 480px;height: 360px");
});

function bidListPage(curPageNO){
	var url = contextPath+"/s/auction/bidList/" + auctionId+"?curPageNO=" + curPageNO;
	$("#auctionDetail").load(url);
}


function depositRecListPage(curPageNO){
	var url = contextPath+"/s/auction/depositRecList/" + auctionId+"?curPageNO=" + curPageNO;
	$("#depositrecDetail").load(url);
}
$().ready(function() {
	getBidList();
	getDepRec();
});


function getBidList(){
	var url = contextPath+"/s/auction/bidList/" + auctionId
	+"/?curPageNO=1";
	$("#auctionDetail").load(url);
}

function getDepRec(){
	var url = contextPath+"/s/auction/depositRecList/" + auctionId
	+"/?curPageNO=1";
	$("#depositrecDetail").load(url);
}
//退款
function update(id,type){
	layer.open({
		title :"修改信息",
		type: 2,
		content: contextPath+"/s/auction/depositRec?depId="+id, //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		area: ['400px', '300px'],
		btn: ['确定'+type],
		yes: function(index, layero){
			$.ajax({
				url: contextPath+"/s/auction/updateFlagStatus",
				data: {"id":id},
				type:'post',
				async : true, //默认为true 异步
				dataType:'json',
				success:function(data){
					if(data=="OK"){
						layer.msg("修改成功！",{icon:1,time:500},function(){
							window.location.reload(true);
						});
					}
					return true;
				}
			});

		}
	});
}
