$(function(){
	//全选
  	$(".selectAll").click(function(){
  	    
  		if($(this).attr("checked")=="checked"){
  			$(this).parent().parent().addClass("checkbox-wrapper-checked");
  	         $(".selectOne").each(function(){
  	        		 $(this).attr("checked",true);
  	        		 $(this).parent().parent().addClass("checkbox-wrapper-checked");
  	         });
  	     }else{
  	         $(".selectOne").each(function(){
  	             $(this).attr("checked",false);
  	             $(this).parent().parent().removeClass("checkbox-wrapper-checked");
  	         });
  	         $(this).parent().parent().removeClass("checkbox-wrapper-checked");
  	     }
  	 });

  	function selectOne(obj){
  		if(!obj.checked){
  			$(".selectAll").checked = obj.checked;
  			$(obj).prop("checked",false);
  			$(obj).parent().parent().removeClass("checkbox-wrapper-checked");
  		}else{
  			$(obj).prop("checked",true);
  			$(obj).parent().parent().addClass("checkbox-wrapper-checked");
  		}
  		 var flag = true;
  		  var arr = $(".selectOne");
  		  for(var i=0;i<arr.length;i++){
  		     if(!arr[i].checked){
  		    	 flag=false; 
  		    	 break;
  		    	 }
  		  }
  		 if(flag){
  			 $(".selectAll").prop("checked",true);
  			 $(".selectAll").parent().parent().addClass("checkbox-wrapper-checked");
  		 }else{
  			 $(".selectAll").prop("checked",false);
  			 $(".selectAll").parent().parent().removeClass("checkbox-wrapper-checked");
  		 }
  	}
});

if(typeof JSON == 'undefined'){$('head').append($("<script type='text/javascript' src='"+contextPath+"/resources/common/js/json.js'>"));}
	//页面的删除
	function deleteAction(type){
	//获取选择的记录集合
	selAry = $(".selectOne");
	if(!checkSelect(selAry)){
	layer.alert('删除时至少选中一条记录！',{icon: 0});
	return false;
	}
	layer.confirm("删除后不可恢复, 确定要删除吗？", {
 		 icon: 3
  	     ,btn: ['确定','取消'] //按钮
  	   },function () {
				var ids = []; 
	  			for(i=0;i<selAry.length;i++){
					  if(selAry[i].checked){
					  ids.push(selAry[i].value);
					 }
				}
			var result = deleteComment(ids,type);
			if('OK' == result){
				layer.msg('删除成功', {icon: 1,time:700},function(){
					window.location.reload();
				});

			}else{
				layer.msg('删除失败', {icon: 2,time:700},function(){
					window.location.reload();
				});
				
			}
		});
	
	return true;
	}

	function deleteComment(ids,type) {
				var result;
				var idsJson = JSON.stringify(ids);
				var ids = {"ids": idsJson};
				var url = contextPath+"/p/" + type;
				jQuery.ajax({
				url:url , 
				data: ids,
				type:'post', 
				async : false, //默认为true 异步   
			    dataType : 'json', 
				success:function(retData){
					result = retData;
					}
				});
				return result;
	}
	
	function reply(consId){
		if(consId!=null&&consId!=""&&consId!=undefined){
				$.ajax({
					url:contextPath+"/s/prodcons/load/"+consId,
					async : true, //默认为true 异步   
				 	dataType:'json', 
					success:function(data){
						var result=eval("("+data+")");
						if(result!=null){
							var message = "咨询内容：<p style='width:200px;word-wrap: break-word;'>"+result.content+"</p></br></br>回复：<p><textarea rows='5' cols='40' id='answer' name='answer'></textarea></p>";
							layer.alert(message,function(){
								
			    					var ans=$("#answer").val();
			    					if(ans==null||ans==''||ans==undefined){
			    						layer.alert('回复内容为空！', {icon: 0});
			    						return false;
			    					}
			    					if(ans.length>100){
			    						layer.alert('回复字数应少于100！', {icon: 2});
			    						return false;
			    					}
			    					$.ajax({
			    						url:contextPath+"/s/prodcons/update",
			    						data:{"consId":consId,"answer":ans},
			    						type:'post',
										async : true, //默认为true 异步   
				 						dataType:'json', 
			    						success:function(data){
			    							if(data=="OK"){
			    								layer.msg('回复成功！', {icon: 1,time:700},function(){
			    										window.location.reload();}	);
			    								
			    							}
			    						}
			    					});
			    					return true;
							});
						}
					}
				});
		}
	}
	
	
	function pager(curPageNO){
			userCenter.loadPageByAjax("/s/prodcons?curPageNO=" + curPageNO);
        }
        
        $(document).ready(function() {
			userCenter.changeSubTab("prodcons");
	  	});