jQuery(document).ready(function(){
	  //三级联动
  	  $("select.combox").initSelect();
  	  userCenter.changeSubTab("sellingProd");

  	//全选
  	$(".selectAll").click(function(){

  		if($(this).attr("checked")=="checked"){
  			$(this).parent().parent().addClass("checkbox-wrapper-checked");
  	         $(".selectOne").each(function(){
  	        		 $(this).attr("checked",true);
  	        		 $(this).parent().parent().addClass("checkbox-wrapper-checked");
  	         });
  	     }else{
  	         $(".selectOne").each(function(){
  	             $(this).attr("checked",false);
  	             $(this).parent().parent().removeClass("checkbox-wrapper-checked");
  	         });
  	         $(this).parent().parent().removeClass("checkbox-wrapper-checked");
  	     }
  	 });

  	function selectOne(obj){
  		if(!obj.checked){
  			$(".selectAll").checked = obj.checked;
  			$(obj).prop("checked",false);
  			$(obj).parent().parent().removeClass("checkbox-wrapper-checked");
  		}else{
  			$(obj).prop("checked",true);
  			$(obj).parent().parent().addClass("checkbox-wrapper-checked");
  		}
  		 var flag = true;
  		  var arr = $(".selectOne");
  		  for(var i=0;i<arr.length;i++){
  		     if(!arr[i].checked){
  		    	 flag=false;
  		    	 break;
  		    	 }
  		  }
  		 if(flag){
  			 $(".selectAll").prop("checked",true);
  			 $(".selectAll").parent().parent().addClass("checkbox-wrapper-checked");
  		 }else{
  			 $(".selectAll").prop("checked",false);
  			 $(".selectAll").parent().parent().removeClass("checkbox-wrapper-checked");
  		 }
  	}
 });

function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("from1").submit();
}

/** 释放锁定秒杀商品状态 **/
function releaseSeckillStatus(prodId){
	layer.confirm("请确定该商品参与秒杀的活动是已过期完成!", {
		 icon: 3
	     ,btn: ['确定','关闭'] //按钮
	   }, function(){
		   jQuery.ajax({
				url:contextPath+"/s/product/releaseSeckillStatus/"+prodId,
				type:'get',
				async : false, //默认为true 异步
				dataType : 'json',
				success:function(result){
					if(result=="OK"){
						window.location.reload();
					}else{
						layer.msg(result, {icon: 0});
					}
				}
			});

	   });
}

    /** 更改商品状态**/
   function pullOff(ths){
    	var item_id = $(ths).attr("productId");
      	var status = $(ths).attr("status");
      	var desc;
      	var toStatus;
      	   if(status == 1){
      	   		toStatus  = 0;
      	   		desc ="将'" + $(ths).attr("productName")+"'放入仓库?";
      	   }else{
      	       toStatus = 1;
      	       desc = $(ths).attr("productName")+' 上线?';
      	   }

      	 layer.confirm(desc, {
      		 icon: 3
      	     ,btn: ['确定','关闭'] //按钮
      	   }, function(){
      		 jQuery.ajax({
					url:contextPath+"/s/updatestatus/" + item_id + "/" + toStatus,
					type:'get',
					async : false, //默认为true 异步
					dataType : 'json',
					success:function(data){
						window.location.reload();
					}
				});
      	   });

   	}


		/**删除多个商品**/
 	  function confirmDelete(prodId,name){
 		 layer.confirm("是否将商品：'"+name+"' 放入商品回收站?", {
      		 icon: 3
      	     ,btn: ['确定','关闭'] //按钮
      	   }, function(){
      		 jQuery.ajax({
					url:contextPath+"/s/product/toDustbin/"+prodId,
					type:'get',
					async : false, //默认为true 异步
					dataType : 'json',
					success:function(data){
						if(data=="OK"){
							window.location.reload();
						}else{
							layer.msg('操作失败！', {icon: 2});
						}
					}
				});
      	   });
  		}

   	function deleteAction(){
   		//获取选择的记录集合
   		selAry = $(".selectOne");
   		if(!checkSelect(selAry)){
   			layer.msg('删除时至少选中一条记录！',{icon: 2});
   		 return false;
   		}

   	  layer.confirm("删除后不可恢复, 确定要删除吗？", {
   		 icon: 3
   	     ,btn: ['确定','关闭'] //按钮
   	   }, function(){
   		var totalToDel = 0;
			var onError = 0;
	   			for(i=0;i<selAry.length;i++){
					  if(selAry[i].checked){
					  totalToDel = totalToDel + 1;
					  var prodId = selAry[i].value;
					  var name = selAry[i].getAttribute("arg");
						var result =	deleteProduct(prodId,true);
						if('OK' != result){
							onError = onError + 1;
							}
						 }
			}
	   		if(onError == 0){
	   			layer.msg('删除商品成功！', {icon: 1,time:700},function(){
					window.location.reload();
				});
	   		}else if(onError < totalToDel ){
	   			layer.msg('删除部分商品成功！', {icon: 1,time:700},function(){
					window.location.reload();
				});
	   		}else  if(onError == totalToDel){
	   			layer.msg('删除商品失败！', {icon: 2});
	   		}
   	   });
   		return true;
   	}

   	function batchOffline(){
		//获取选择的记录集合
		selAry =$(".selectOne");
		if (!checkSelect(selAry)) {
			layer.msg('至少选中一条记录！',{icon: 2});
			return false;
		}
		 layer.confirm("确定要把选中的商品下线吗？", {
      		 icon: 3
      	     ,btn: ['确定','关闭'] //按钮
      	   }, function(){
   			var totalToOffline = 0;
   			var onError = 0;
   			for (i = 0; i < selAry.length; i++) {
   				if (selAry[i].checked) {
   					totalToOffline = totalToOffline + 1;
   					var prodId = selAry[i].value;
   					var name = selAry[i].getAttribute("arg");
   					var result = offlineProduct(prodId);
   					if (0 != result) {
   						onError = onError + 1;
   					}
   				}
   			}
   			if (onError == 0) {
   				layer.msg('商品下线成功！', {icon: 1,time:700},function(){
					window.location.reload();
				});

   			} else if (onError < totalToOffline) {
   				layer.msg('部分商品下线成功！', {icon: 1,time:700},function(){
					window.location.reload();
				});

   			} else if (onError == totalToOffline) {
   				layer.msg('商品下线失败！', {icon: 2});
   			}else{
   			  return;
        }

      	   });

		return true;
	}


	function batchEdit(){
		//获取选择的记录集合
		selAry =$(".selectOne:checked");
		var array=[];
			selAry.each(function(index,dom){
			array[index]=dom.value;
		})
		if (!checkSelect(selAry)) {
			layer.msg('至少选中一条记录！',{icon: 2});
			return false;
		}
		layer.open({
			  title :"批量修改",
			  id: "batchEdit",
			  type: 2,
			  resize: false,
			  content: [contextPath+"/s/batchEdit?selAry="+array,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
			  area: ['430px', '300px']
			});
		return true;
	}


	function offlineProduct(prodId){
		var result = -2;
		jQuery.ajax({
			url : contextPath+"/s/updatestatus/" + prodId + "/0",
			type : 'get',
			async : false, //默认为true 异步
			dataType : 'json',
			success : function(data) {
				result = data;
			}
		});
		return result;
	}

	/**
	 * 删除商品
	 * @param prodId
	 * @param multiDel
	 * @returns
	 */
   	function deleteProduct(prodId, multiDel) {
   		var result;
			  jQuery.ajax({
					url:contextPath+"/s/product/toDustbin/"+prodId,
					type:'get',
					async : false, //默认为true 异步
					dataType : 'json',
					success:function(data){
						result = data;
					}
					});
			  return result;
   	}


   	/**
   	 * 加载分类用于选择
   	 * @returns
   	 */
    function loadCategoryDialog() {
        var url = contextPath + "/s/loadShopCategory";
        layer.open({
  		  type: 2,
  		  title: "选择分类",
  		  btn: false,
  		  shadeClose: true,
  		  closeBtn:0,
  		  skin: ['user-btn-class'],
  		  shade: 0.8,
  		  area: ['280px', '350px'],
  		  content: url,
  		  success: function(layero, index) {
  			  //页面加载成功，自适应高度
  			  layer.iframeAuto(index);
  		  }
  		});
    }

    function clearCategoryCallBack() {
   	 	$("#shopFirstCatId").val("");
   	 	$("#shopSecondCatId").val("");
        $("#shopThirdCatId").val("");
        $("#prodCatName").val("");
    }

    function loadCategoryCallBack(firstCatId, secondCatId, thirdCatId, catName) {
        $("#shopFirstCatId").val(firstCatId);
        $("#shopSecondCatId").val(secondCatId);
        $("#shopThirdCatId").val(thirdCatId);
        $("#prodCatName").val(catName);
    }
    function clearCategoryCallBack() {
    	$("#shopFirstCatId").val(null);
    	$("#shopSecondCatId").val(null);
    	$("#shopThirdCatId").val(null);
    	$("#prodCatName").val(null);
    }

    function updateProd(prodId){
    	jQuery.ajax({
			url:contextPath+"/s/product/isProdChange",
			type:'post',
			data:{"prodId":prodId},
			async : false, //默认为true 异步
			dataType : 'json',
			success:function(result){
				if(result=="OK"){
					window.open(contextPath+"/s/updateProd/"+prodId);
				}else{
					layer.msg(result, {icon: 0});
				}
			}
		});
    }

function updateStotcks(prodId){
  window.open(contextPath+"/s/loadSkuListPage?prodId="+prodId);
}

