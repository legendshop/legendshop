KindEditor.options.filterMode=false;
var editor =KindEditor.create('textarea[name="seckillDesc"]', {
	cssPath : paramData.contextPath + '/resources/plugins/kindeditor/plugins/code/prettify.css',
	uploadJson : paramData.contextPath + '/editor/uploadJson/upload;jsessionid=' + paramData.cookieValue,
	fileManagerJson : paramData.contextPath + '/editor/uploadJson/fileManager',
	allowFileManager : true,
	afterBlur:function(){this.sync();},
	width : '680px',
	height:'350px',
	afterCreate : function() {
		var self = this;
		KindEditor.ctrl(document, 13, function() {
			self.sync();
			document.forms['example'].submit();
		});
		KindEditor.ctrl(self.edit.doc, 13, function() {
			self.sync();
			document.forms['example'].submit();
		});
	}
});


jQuery.validator.addMethod("isNumber", function(value, element) {
    return this.optional(element) || /^[-\+]?\d+$/.test(value) || /^[-\+]?\d+(\.\d+)?$/.test(value);
}, "必须整数或小数");

jQuery.validator.addMethod("isFloatGtZero", function(value, element) {
    value=parseFloat(value);
    return this.optional(element) || value>=1;
}, "必须大于等于1元");

jQuery.validator.addMethod("startDateValid", function (value, element, param) {
	 var date = new Date($(element).val());
	 if(date.getHours()<=8){
		 return false;
	 }
	 return true;
}, '开始时间不能为9点之前');

jQuery.validator.addMethod("compareDate", function (value, element, param) {
    var startDate = jQuery(param).val();
    startDate= startDate.replace(/-/g, '/');
    value = $(element).val().replace(/-/g, '/');

    if (startDate == null || startDate == '' || value == null || value == '') {//只要有一个为空就不比较
        return true;
    }
    var date1 = new Date(startDate);
    var date2 = new Date(value);
    return date1.getTime() < date2.getTime();

}, '结束时间必须大于开始时间');

$(document).ready(function() {
		if(!isBlank(pic)){
			$("#seckillPicFile").addClass("ignore");
		}

		$(".prodList").on("blur",".seckillStock",function () {
			if (this.value==null || this.value=="" || this.value<=0 || isNaN(this.value)){
				layer.alert("秒杀库存不能为空并且大于0",{icon:2})
				$(this).css("background-color","pink");
                $(this).val("");
                return;
			}
            var stocks = $(this).parent().prev().text();
            if (this.value>parseInt(stocks)){
                layer.alert("秒杀库存不能大于实际库存",{icon:2})
                $(this).css("background-color","pink");
                $(this).val("");
                return;
            }
            $(this).css("background-color","white");
		})

		$(".prodList").on("blur",".seckillPrice",function () {
			if (this.value==null || this.value=="" || this.value<=0 || isNaN(this.value)){
				layer.alert("秒杀价格不能为空并且打大于0",{icon:2})
                $(this).css("background-color","pink");
                $(this).val("");
                return;
			}
            $(this).css("background-color","white");
		})

	    jQuery("#form1").validate({
	    	 ignore: ".ignore",
	    	 errorPlacement: function(error, element) {
	 	    	element.next("em").html("");
	 			error.appendTo(element.next("em"));
	 		},
	 		 rules: {
	             seckillTitle: {
               		 required: true
           		 },startTime: {
               		 required: true,
               		 startDateValid:true
           		 },endTime: {
           			required: true,
           			compareDate: "#startTime"
           		 }, seckillBrief: {
               		 required: true,
               		 maxlength: 100
           		 },seckillPicFile: {
               		 required: true
           		 }
	 		 },
	 		 messages: {
       			 seckillTitle: {
               		 required: "标题不能为空"
           		 },startTime: {
               		 required: "开始时间不能为空"
           		 },endTime: {
           			required: "结束时间不能为空"
           		 },seckillBrief: {
               		 required: "摘要不能为空",
               		maxlength: "摘要长度不能大于100"
           		 },seckillPicFile: {
               		 required: "图片不能为空"
           		 }
	 		 },
	          submitHandler: function (form) {
	        	  		var prodid=$("#prodId").val();
	        	  		if(isBlank(prodid)){
	        	  			layer.msg('商品不能为为空', {icon: 2});
	        	  			return false;
	        	  		}
	        	  		var rs=true;

	        	  		if(status != null && status != 1){

                    $(".seckillPrice").each(function () {
                      if (this.value==null || this.value=="" || this.value<=0 || isNaN(this.value)){
                        layer.alert("秒杀价格不能为空并且打大于0",{icon:2})
                        rs = false;
                        $(this).css("background-color","pink");
                        $(this).val("");
                      }
                    });

                    $(".seckillStock").each(function () {
                      if (this.value==null || this.value=="" || this.value<=0 || isNaN(this.value)){
                        layer.alert("秒杀库存不能为空并且大于0",{icon:2})
                        rs = false;
                        $(this).css("background-color","pink");
                        $(this).val("");
                      }
                      var stocks = $(this).parent().prev().text();
                      if (this.value>parseInt(stocks)){
                        layer.alert("秒杀库存不能大于实际库存",{icon:2})
                        rs = false;
                        $(this).css("background-color","pink");
                        $(this).val("");
                      }
                    });
                  }

	        	  		if (!rs){
                      return;
                  }

	        	  		var startTime=$("#startTime").val();
	        	  		var endTime=$("#endTime").val();
	        	  		if(startTime > endTime){
	        	  			layer.msg('活动开始时间不能大于结束时间', {icon: 2});
	        	  			return false;
	        	  		}

					    var time = new Date($("#startTime").val().replace(/-/g,"/"));

					    //开始时间不能小于等于当前时间
					    if(time.getTime() <= new Date().getTime()){
						   layer.alert("活动开始时间不能小于当前时间!", {icon:0});
						   return false;
					    }
	         			var prodParamArray =getParam();
	         			if(isBlank(prodParamArray)){
	         				return false;
	         			}
						 var prodParameter = JSON.stringify(prodParamArray);
						 if(!isBlank(prodParameter)){
							 $("#skus").val(prodParameter);
							 form.submit();
						 }
				}

	    });
});



function getTimePeriod(hour){
	if(hour>=9 && hour<12){
		return 11;
	}else if(hour>=12 && hour<15){
		return 14;
	}else if(hour>=15 && hour<18){
		return 17;
	}else if(hour>=18 && hour<21){
		return 20;
	}else if(hour>=21 && hour<24){
		return 23;
	}
}

function getTimeStart(hour){
	if(hour>=9 && hour<12){
		return 9;
	}else if(hour>=12 && hour<15){
		return 12;
	}else if(hour>=15 && hour<18){
		return 15;
	}else if(hour>=18 && hour<21){
		return 18;
	}else if(hour>=21 && hour<24){
		return 21;
	}
  return 24;
}

function showProdlist(){
    layer.open({
        title :"选择商品",
        id: "showProdlist",
        type: 2,
        resize: false,
        content: [contextPath+"/s/seckillActivity/seckillProdLayout",'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
        area: ['850px', '660px']
    });

}

function addProdTo(prodId){
	if(prodId==null||prodId==""||prodId==undefined){
	    layer.alert('商品Id为空', {icon: 2});
	 }

    if($()){
        layer.alert('商品Id为空', {icon: 2});
    }
	if(!isAttendActivity(prodId)){
		layer.alert("对不起,您选择的商品已全部参与了活动!", {icon:0});
		return false;
	}
	 $("#prodId").val(prodId);
	//检查该商品的SKU是否全部下线或者无库存
	 jQuery.ajax({
			url: contextPath + "/s/seckillActivity/checkSeckillProdSku/"+prodId,
			type:'post',
			async : false, //默认为true 异步
		    dataType : 'json',
			success:function(result){
				 if(result){
					 var url=contextPath+"/s/seckillActivity/seckillProdSku/"+prodId;
					 $(".prodList").load(url);
					 layer.closeAll('iframe');
				 }else{
					 layer.alert('添加失败,请检查该商品的SKU是否全部下线或者全部无库存', {icon: 2});
				 }
			}
	 });
}

function isAttendActivity(prodId){
    var flag = true;
	$.ajax({
		url : contextPath + "/s/seckillActivity/isAttendActivity/" + prodId ,
		async : false,
		dataType : "JSON",
		success : function(result){
			flag = result;
		}
	});
	return flag;
}

function removeSku(id){
	var length=$(".skulist tr").length;
	if(length<=3){
		layer.alert('最少保留一条数据', {icon: 0});
	}else{
		$(".skulist tr[id=list_"+id+"]").remove();
	}
}

function getParam(){
	var temp=$("#seckillPrice").val();
	var prodParamArray = [];
	var prod,prodid="";skuid="",price="",stock="";
	var id=$("#id").val();
    $(".sku").each(function(){
			prod = new Object();
    	if(isBlank(id)){
			prodid= $(this).find("#prodId").val();
				skuid= $(this).find("#skuId").val();
			var prodprice= $(this).find("#seckillPrice").val();
			var prodstock= $(this).find("#seckillStock").val();
			if(!checkMoney(prodprice)||!checkStock(prodstock)){
				prodParamArray=[];
				return false;
			}
			if(temp>prodprice){
				temp=prodprice
			}
			//skuid为空
			if(!isBlank(skuid)){
					prod.prodid = prodid;
					prod.skuid = skuid;
					prod.price = prodprice;
					prod.stock = prodstock;
					prodParamArray.push(prod);
			}else{
				if(!isBlank(prodid)){
					prod.prodid = prodid;
					prod.skuid=0;
					prod.price = prodprice;
					prod.stock = prodstock;
					prodParamArray.push(prod);
				}
			}
		}else{
			prodid= $("#prodId").val();
			skuid= $(this).find("#skuId").val();
			var prodprice= $(this).find("#seckillPrice").val();
			var prodstock= $(this).find("#seckillStock").val();
			if(!checkMoney(prodprice)||!checkStock(prodstock)){
				prodParamArray=[];
				return false;
			}
			if(!isBlank(skuid)){
					prod.prodid = prodid;
					prod.skuid = skuid;
					prod.price = prodprice;
					prod.stock = prodstock;
					prodParamArray.push(prod);
			}else{
					prod.prodid = prodid;
					prod.skuid=0;
					prod.price = prodprice;
					prod.stock = prodstock;
					prodParamArray.push(prod);
			}
		}
	 });
    $("#seckillLowPrice").val(temp);
	 return prodParamArray;
}
//检查库存
function checkStock(val){
	if(isBlank(val)){
		layer.alert('库存不能为空', {icon: 0});
    	return false;
    }
	var regex=/^[0-9]*[1-9][0-9]*$/;
	if(!regex.test(val)){
    	layer.alert('请输入正确的库存', {icon: 0});
        return false;
    }
	return true;
}



//检查金额
function checkMoney(val){
	if(isBlank(val)){
		layer.alert('价格不能为空', {icon: 0});
    	return false;
    }
	if(parseFloat(val)<=0){
	    	layer.alert('价格不能小于0', {icon: 0});
	    	return false;
	}
    var regex = /^[\d]*(.[\d]{1,2})?$/;
    if(!regex.test(val)){
    	layer.alert('请输入正确的金额格式', {icon: 0});
        return false;
    }
    return true;
}

//检查图片
$("input[id=seckillPicFile]").change(function(){
	checkImgType(this);
	checkImgSize(this,512);
});


//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}

//移除元素
$(".prodList").on("click",".removeSku",function() {
    $(this).parents("tr").empty().remove();
    if ($("#skuList").find(".sku").size() == 0) {
        $(".noSku").show();
    }
    return false;
})

//批量修改价格
$("#confim").click(function(){
  if($(".selectOne:checked").length<=0){
    layer.alert("请勾选商品",{icon:0});
    return;
  }

    var _val = $("#batchPrice").val();

    if (isNaN(_val)||_val<=0) {
		layer.alert("秒杀价必须大于0",{icon:0})
        $("#batchPrice").val("");
		return;
	}

    if($(".sku").size()<=0){
        layer.alert("请选择商品",{icon:0});
        return;
    }
    if(isBlank(_val)){
        layer.alert("请选择输入秒杀价",{icon:0});
        return;
    }

    $(".selectOne:checked").each(function(index,element){
        if($(element).is(':checked')){//选中，改变价格
            $(element).parents(".sku").find(".seckillPrice").val(_val);
        }
    })
    $("#batchPrice").val("");
});

//全选
$(".prodList").on("click",".selectAll",function(){
    if($(this).attr("checked")=="checked"){
        $(this).parent().parent().addClass("checkbox-wrapper-checked");
        $(".selectOne").each(function(){
            $(this).attr("checked",true);
            $(this).parent().parent().addClass("checkbox-wrapper-checked");
        });
    }else{
        $(".selectOne").each(function(){
            $(this).attr("checked",false);
            $(this).parent().parent().removeClass("checkbox-wrapper-checked");
        });
        $(this).parent().parent().removeClass("checkbox-wrapper-checked");
    }
});

// 单选
function selectOne(obj){
    if(!obj.checked){
        $(".selectAll").checked = obj.checked;
        $(obj).prop("checked",false);
        $(obj).parent().parent().removeClass("checkbox-wrapper-checked");
    }else{
        $(obj).prop("checked",true);
        $(obj).parent().parent().addClass("checkbox-wrapper-checked");
    }
    var flag = true;
    var arr = $(".selectOne");
    for(var i=0;i<arr.length;i++){
        if(!arr[i].checked){
            flag=false;
            break;
        }
    }
    if(flag){
        $(".selectAll").prop("checked",true);
        $(".selectAll").parent().parent().addClass("checkbox-wrapper-checked");
    }else{
        $(".selectAll").prop("checked",false);
        $(".selectAll").parent().parent().removeClass("checkbox-wrapper-checked");
    }
}
