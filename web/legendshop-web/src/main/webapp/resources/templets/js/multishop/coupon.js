
/**
 * 初始化select2
 * @param id
 * @param clearFlag
 */
function makeSelect2(url, element, text, label, value) {
    $("#" + element).select2({
        placeholder: "选择" + text,
        allowClear: true,
        ajax: {
            url: url,
            data: function (term, page) {
                return {
                    q: term,
                    pageSize: 10,    //一次性加载的数据条数
                    currPage: page, //要查询的页码
                };
            },
            type: "GET",
            dataType: "JSON",
            results: function (data, page, query) {
                if (page * 10 < data.total) {//判断是否还有数据加载
                    data.more = true;
                }
                return data;
            },
            cache: true,
            quietMillis: 200//延时
        },
        initSelection: function (element, callback) {
            var selectData = [];
            $(element.val().split(",")).each(function () {
                var val = this;
                for (var i = 0; i < tagData.length; i++) {
                    if (tagData[i].id == val) {
                        selectData.push(tagData[i]);
                        continue;
                    }
                }
            });
            callback(selectData);
        },
        multiple: true,
        formatInputTooShort: "请输入" + text,
        formatNoMatches: "没有匹配的" + text,
        formatSearching: "查询中...",
        formatResult: function (row, container) {//选中后select2显示的 内容
            return "<b>" + row.text + "</b>"; //+ "</br>" + row.customText1
        }, formatSelection: function (row) { //选择的时候，需要保存选中的id
            return row.text;//选择时需要显示的列表内容
        },
    });
}

var isAppointUser = false;//体验劵是否对指定用户发放
$("#isAppointUser").on("change", function () {
    var val = $("#isAppointUser select option:selected").val();
    if (val == 1) {
        $("#userIdLists").parent().parent().show();
        $("#grantTotalNumber").hide();
        $("#couponNumber").val();
        $("#couponNumber").parent().parent().hide();
        $("#getLimit").val(1);
        $("#getLimit").parent().parent().hide();
        $(".appoint-user-show").show();
        $("#getType option[value!=free]").hide();
        $("#getType option[value=free]").attr("selected", "true");
        $("#type").empty();
        isAppointUser = true;
    } else {
        $("#userIdLists").parent().parent().hide();
        $("#couponNumber").val("");
        $("#couponNumber").parent().parent().show();
        $("#getLimit").val("");
        $("#getLimit").parent().parent().show();
        $(".appoint-user-show").hide();
        $("#getType option").show();
        isAppointUser = false;
    }
})

function pager(curPageNO) {
    document.getElementById("curPageNO").value = curPageNO;
    document.getElementById("userCouponForm").submit();
}

$(document).ready(function () {
    $("#pro").hide();
    $("#couponType").change(function () {
        $("#pro").hide();
        $("#cate").empty();
        var val = $("#couponType option:selected").val();

        if (val == "product") {
            $("#pro").show();
            $("#cate").append("<span><font color='ff0000'>* </font>选择商品:" +
                "</span><input type='hidden' name='prodidList' id='prodidList'/>" +
                "<a href='javascript:void(0)' onclick='loadSelectProdDialog()'; >" +
                "<font color='#900'>&nbsp;&nbsp;&nbsp;选择</font></a><div class='new-cou-com'><table class='prodList sold-table'>" +
                "<th>商品名称</th><th>商品价格</th><th>操作</th></tr></table></div>");
        }
    });

    //
    //$("#getType").change(function () {
    //    $("#type").empty();
    //    var val = $(this).val();
    //    if (val == "points") {
    //        $("#type").append("<li><div class='new-cou-con'><span><font color='ff0000'>* </font>兑换所需积分:</span>" +
    //            "<input class='${inputclass}' type='text' <c:if test='${ not empty coupon.couponId}'>disabled='disabled'</c:if>" +
    //            " name='needPoints' id='needPoints' value='${coupon.needPoints}' />" +
    //            "</div></li>");
    //    }
    //});

    var messageType = '${coupon.messageType}';
    if (messageType != "" && messageType != undefined && messageType != null) {
        var arr = messageType.split(',');
        if (arr.length > 0) {
            for (var i = 0; i < arr.length; i++) {
                var a = arr[i];
                $("input[name='messageType']").each(function () {
                    var v = $(this).attr("value");
                    if (a == v) {
                        $(this).attr("checked", "checked");
                    }
                });
            }
        }


    }

    jQuery.validator.addMethod("isnumber", function (value, element) {
        return this.optional(element) || /^\d+(\.\d{1,2})?$/.test(value);
    }, "只能包含数字、小数点");

    jQuery.validator.addMethod("isDigits", function (value, element) {
        return this.optional(element) || /^\d+$/.test(value);
    }, "只能输入0-9数字");

    jQuery.validator.addMethod("compareDate", function (value, element, param) {
        var startDate = jQuery(param).val();
        startDate= startDate.replace(/-/g, '/');
        value = $(element).val().replace(/-/g, '/');

        if (startDate == null || startDate == '' || value == null || value == '') {//只要有一个为空就不比较
            return true;
        }
        var date1 = new Date(startDate);
        var date2 = new Date(value);
        return date1.getTime() < date2.getTime();

    }, '结束时间必须大于开始时间');

    jQuery("#form1").validate({
        rules: {
            //couponType : "required",
            couponName: "required",
            fullPrice: {
                required: true,
                isnumber: true
            },
            offPrice: {
                required: true,
                isnumber: true,
                max:99999
            },
            startDate: {
            	required: true
            },
            endDate: {
            	required: true,
            	compareDate: "#startDate"
            },
            getLimit: {
                required: true,
                isDigits: true,
                max:99
            },
            needPoints: {
                required: true,
                digits: true,
            },
            couponNumber: {
                required: true,
                min: 1,
                maxlength: 6,
                isDigits: true
            },
            description: {
            	required: true,
            	maxlength:200
            }

        },
        messages: {
            //couponType : "请选择红包类型",
            couponName: "请输入优惠券名称",
            fullPrice: {
                required: "请输入优惠券值",
                isnumber: "请输入正确格式!"
            },
            offPrice: {
                required: "请输入优惠券值",
                isnumber: "请输入正确格式!",
                max:"优惠劵最大金额为99999"
            },
            startDate: {
            	required: "请选择日期"
            },
            endDate: {
            	required: "请选择日期"
            },
            getLimit: {
                required: "请输入领取上限值!",
                isDigits: "请输入正确格式!",
                max:"每人限领数100以下"
            },
            needPoints: {
                required: "请输入兑换所需积分!",
                digits: "请输入整数数字!"
            },
            couponNumber: {
                required: "请输入可发放总数!",
                min: "请输入最小值",
                maxlength: "最大为6位数字",
                isDigits: "请输入正确格式!"
            },
            description:{
            	required:"请输入描述",
            	maxlength:"超出字数限制！"
            }
        },
        submitHandler: function (form) {
            //把table的prodId循环拼接成字符串

            var couponType = $("#couponType").val();
            if (couponType == "product") {
                var tableLength = $("table[class='prodList sold-table'] tr").length;
                if (tableLength <= 1) {
                    layer.alert("请添加商品数据",{icon:0});
                    return;
                }
            }
            var prodidList = "";
            $("table[class='prodList sold-table'] tr").each(function (index, domEle) {
                var prod_id = $(this).find("#prodId").val();
                if (!isEmpty(prod_id)) {
                    prodidList += prod_id + ",";
                }
            });

            $("#prodidList").val(prodidList);
            //消费金额的计算			 
            if ($("#fullPrice").val() - $("#offPrice").val() <= 0) {
                layer.alert("消费金额应大于面额！",{icon:0});
                return;
            }


            if ($("#getType").val() == "pwd") {
                var couponNum = $("#couponNumber").val();
                if (couponNum < 1 || couponNum > 1000) {
                    layer.alert("如果代金券领取方式为卡密兑换，则发放总数应为1~1000之间的整数！",{icon:0});
                    return;
                }
            }

            layer.alert("确认无误？",{
		  		 icon: 3
		  	     ,btn: ['确定','取消'] //按钮
		  	   },function(){
            	form.submit();
            	layer.load(1);
            });

        }
    });

    //斑马条纹
    $("#col1 tr:nth-child(even)").addClass("even");

    $("#couponType").change(function () {
        var str = $(this).val();
        if (str != 2) {
            $("#full").show();
        } else {
            $("#full").hide();
        }
    });

    var frequency = '${coupon.frequency}';
    if (frequency != undefined && frequency != null && frequency != "") {
        $("#limit").append("<span id='fre'>&nbsp;&nbsp;&nbsp;领取频率:隔&nbsp;<input type='text' style='width:50px;' name='frequency' readonly='readonly' id='frequency' value='" + frequency + "' />&nbsp;小时领取一次</span>");
    }
});


/**
 * 判断是否是空
 * @param value
 */
function isEmpty(value) {
    if (value == null || value == "" || value == "undefined" || value == undefined || value == "null") {
        return true;
    }
    else {
        value = (value + "").replace(/\s/g, '');
        if (value == "") {
            return true;
        }
        return false;
    }
}

function addProdTo(prodId, name, price) {
    //console.log(prodId+name+price)
    if (prodId == null || prodId == "" || prodId == undefined) {
        layer.alert("商品Id为空");
    }

    var config = true;
    $("table[class='prodList sold-table'] tr").each(function (index, domEle) {
        var prod_id = $(this).find("#prodId").val();
        if (prod_id == prodId) {
            layer.alert("该商品已存在");
            config = false;
        }
    });
    if (!config) {
        return;
    }

    var tableLength = $("table[class='prodList sold-table']").length;
    //拼接数据
    var str = '<tr>' +
        '<td><input type="hidden" id="prodId" value="' + prodId + '"/>' + name + '</td>' +
        '<td>' + price + '</td>' +
        '<td><a href="javascript:void(0)" onclick="deleteProd(this)" class="bot-gra">' +
        '删除</a></td>' +
        '</tr>';
    //追加到Table
    $("table[class='prodList sold-table']").append(str);
    return config;
}

function deleteProd(obj) {
    $(obj).parent().parent().remove();
}

//加载商品分类用于选择(优惠券)
function loadSelectProdDialog() {
    
	layer.open({
		title :"选择商品",
		  type: 2, 
		  content: contextPath+"/s/coupon/loadSelectProd", //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['750px', '550px']
		}); 
	
	
    /*$.ajax({
        url: contextPath+"/s/coupon/loadSelectProd",
        type: 'post',
        async: true, //默认为true 异步   
        dataType: 'html',
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            layer.open({
                title: '选择商品',
                area: ['750px', '550px'],
                resize: false,
                type: 2,
                content: result,
                success: function (elem) {
                    layer.close(0); //关闭上一级弹窗
                }
            });

        }
    });*/
}

function change(val) {
    var limit = $.trim($(val).val());
    var re = /^[0-9\s]*$/;
    if (limit == null || limit == undefined || limit == "") {
        return;
    } else if (!re.test(limit)) {
        $("#fre").remove();
        return;
    }
    else if (limit > 1) {
        $("#fre").remove();
        $("#limit").append("<span id='fre'>&nbsp;&nbsp;&nbsp;领取频率:隔&nbsp;<input type='text' style='width:50px;' name='frequency' id='frequency' value='${coupon.frequency}' />&nbsp;小时领取一次</span>");
    } else {
        $("#fre").remove();
    }
}
/*   var frequency="${coupon.frequency}";
 var messageType="${coupon.messageType}";
 userCenter.changeSubTab("couponManage"); */

$("input[type=file]").change(function () {
    checkImgType(this);
    checkImgSize(this, 1024);
});

/**
 * 检查文件类型
 * @param ths file对象
 * @return 是否符合规格
 */
function checkImgType(ths) {
    try {
        var obj_file = $(ths).get(0).files;
        for (var i = 0; i < obj_file.length; i++) {
            if (!/\.(JPEG|BMP|GIF|JPG|PNG)$/.test(obj_file[i].name.toUpperCase())) {
                layer.alert('仅支持JPG、GIF、PNG、JPEG、BMP格式', {icon: 0});
                $(ths).val("");
                return false;
            }
        }
    } catch (e) {
    }
    return true;
}

/**
 * 检查文件大小
 * @param ths file对象
 * @param limitSize 限制大小(k)
 * @return 是否符合规格
 */
function checkImgSize(ths, limitSize) {
    try {
        var maxsize = limitSize * 1024;
        var msgSize = limitSize + "K";
        if (limitSize >= 1024) {
            msgSize = limitSize / 1024 + "M";
        }
        var errMsg = "上传的图片不能超过" + msgSize;
        var obj_file = $(ths).get(0).files;

        for (var i = 0; i < obj_file.length; i++) {
            if (obj_file[i].size > maxsize) {
                layer.alert(errMsg, {icon: 0});
                $(ths).val("");
                return false;
            }
        }
    } catch (e) {
    }
    return true;
}