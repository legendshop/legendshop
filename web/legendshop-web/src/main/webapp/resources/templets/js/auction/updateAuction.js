jQuery.validator.addMethod("isNumber", function(value, element) {       
	return this.optional(element) || /^[-\+]?\d+$/.test(value) || /^[-\+]?\d+(\.\d+)?$/.test(value);       
}, "必须整数或小数");  

jQuery.validator.addMethod("isFloatGtZero", function(value, element) { 
	value=parseFloat(value);      
	return this.optional(element) || value>=1;       
}, "必须大于等于1元"); 

jQuery.validator.addMethod("checkMaxVal", function(value, element) {
	var min =parseFloat($("#minMarkupRange").val());
	var newValue=parseFloat(value);
	return this.optional(element) || newValue>min;  
}, "必须大于最小金额");

$(document).ready(function() {
	jQuery("#form1").validate({
		ignore: ".ignore",
		errorPlacement: function(error, element) {
			element.parent().find("em").html("");
			error.appendTo(element.parent().find("em"));
		},	
		rules: {
			auctionsTitle: {
				required: true
			}, startTime: {
				required: true
			},endTime: {
				required: true
			},floorPrice: {
				required: true,
				isNumber:true,
				isFloatGtZero:true
			},fixedPrice: {
				required: true,
				isNumber:true,
				isFloatGtZero:true
			},minMarkupRange: {
				required: true,
				isNumber:true,
				isFloatGtZero:true
			},maxMarkupRange: {
				required: true,
				isNumber:true,
				isFloatGtZero:true,
				checkMaxVal:true
			},securityPrice:{
				required: true,
				isNumber:true,
				isFloatGtZero:true
			},delayTime:{
				required: true,
				digits:true
			},hideFloorPrice:{
				required: true
			},prodId:{
				required: true
			},isCeilingSelect:{
				required: true,min:0
			},isSecuritySelect:{
				required: true,min:0
			},hideFloorPriceSelect:{
				required:true,
				min:0
			}

		},
		messages: {
			auctionsTitle: {
				required: "请输入活动名称"
			},startTime: {
				required: "请输入开始时间"
			},endTime: {
				required: "请输入结束时间"
			},floorPrice: {
				required: "请输入起拍价",
			},fixedPrice: {
				required: "请输入一口价"
			},minMarkupRange: {
				required: "请输入最小加价幅度"
			},maxMarkupRange: {
				required: "请输入最大加价幅度"
			},securityPrice:{
				required: "请输入保证金"
			},delayTime:{
				required: "请输入延长时间(分钟)",
				digits:"必须为整数"
			},prodId:{
				required: "商品不能为空"
			},isCeilingSelect:{
				required:"请选择",
				min:"请选择"
			},isSecuritySelect:{
				required:"请选择",
				min:"请选择"
			},hideFloorPriceSelect:{
				required:"请选择是否隐起拍价",
				min:"请选择是否隐起拍价"
			}
		},

		submitHandler: function (form) {
			var isCeiling= $('.isCeiling option:selected') .val();
			var isSecurity= $('.isSecurity option:selected') .val();
			var hideFloorPrice= $('.hideFloorPrice option:selected') .val();

			if(!isBlank(isCeiling)&&!isBlank(isSecurity)&&!isBlank(hideFloorPrice)){
				$("#isCeiling").val(isCeiling);
				$("#isSecurity").val(isSecurity);
				$("#hideFloorPrice").val(hideFloorPrice);
				form.submit();
			}
		},

	});


});
$(".isCeiling").change(function(){
	var s=$(this).val();
	if(s==0){
		$("#isCeiling").val(s);//不封顶
		$(".fixedPrice").hide(300);
		$("#fixedPrice").addClass("ignore");
		$("#fixedPrice").val("");
	}else{
		$("#isCeiling").val(s);//封顶
		$(".fixedPrice").show(300);
		$("#fixedPrice").removeClass("ignore");
	}
});

$(".isSecurity").change(function(){
	var s=$(this).val();
	if(s==0){
		$("#isSecurity").val(s);
		$(".securityPrice").hide(300);
		$(".delayTime").hide(300);
		$("#securityPrice").addClass("ignore");
		$("#delayTime").addClass("ignore");
		$("#securityPrice").val("");
		$("#delayTime").val("");

	}else{
		$("#isSecurity").val(s);
		$(".securityPrice").show(300);
		$(".delayTime").show(300);
		$("#securityPrice").removeClass("ignore");
		$("#delayTime").removeClass("ignore");
	}
});

$(".hideFloorPrice").change(function(){
	var s=$(this).val();
	$("#hideFloorPrice").val(s);
});
KindEditor.options.filterMode=false;
var editor =KindEditor.create('textarea[name="auctionsDesc"]', {
	cssPath : paramData.contextPath + '/resources/plugins/kindeditor/plugins/code/prettify.css',
	uploadJson : paramData.contextPath + '/editor/uploadJson/upload;jsessionid=' + paramData.cookieValue,
	fileManagerJson : paramData.contextPath + '/editor/uploadJson/fileManager',
	allowFileManager : true,
	afterBlur:function(){this.sync();},
	width : '680px',
	height:'350px',
	afterCreate : function() {
		var self = this;
		KindEditor.ctrl(document, 13, function() {
			self.sync();
			document.forms['example'].submit();
		});
		KindEditor.ctrl(self.edit.doc, 13, function() {
			self.sync();
			document.forms['example'].submit();
		});
	}
});


/*   
 */    
function addProdTo(prodId,skuId,skuName,skuPrice,cnProperties){
	$(".prodInfo").css("display","");
	if(name.length>50){
		name=name.substring(0,50)+'…';
	}
	$("#prodId").val(prodId);
	$("#skuId").val(skuId);
	$("#prodId").next().css("display","none");

	$("#skuName").text(skuName);
	$("#skuPrice").text(skuPrice);
	if(isBlank(cnProperties)){
		$("#cnProperties").text("无");
	}else{
		$("#cnProperties").text(cnProperties);
	}

}

function removeProd(){
	$("#prodId").val("");
	$("#skuId").val("");
	$("#prodName").text("");
}


function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}

$(document).ready(function() {
	userCenter.changeSubTab("auctionsManage"); //高亮菜单
	//下拉框数据
	$(".isCeiling option[value='${auctions.isCeiling}']").attr("selected","true");
	$(".isSecurity option[value='${auctions.isSecurity}']").attr("selected","true");
	$(".hideFloorPrice option[value='${auctions.hideFloorPrice}']").attr("selected","true");

	if(isCeiling==1){	
		$("#fixedPrice").removeClass("ignore");
	}else{	
		$(".fixedPrice").hide();		
		$("#fixedPrice").addClass("ignore");
	}
	if(isSecurity==1){	
		$("#securityPrice").removeClass("ignore");
		$("#delayTime").removeClass("ignore");

	}else{	
		$(".securityPrice").hide();
		$(".delayTime").hide();
		$("#securityPrice").addClass("ignore");
		$("#delayTime").addClass("ignore");
		
	}
});
function showProdlist(){
	layer.open({
		title :"选择商品",
		id: "showProdlist",
		type: 2, 
		resize: false,
		content: [contextPath+"/s/auction/AuctionProdLayout",'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		area: ['1000px', '520px']
	}); 

}