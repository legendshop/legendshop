$(document).ready(function() {
    $('#pd_pay_submit').on('click', function() {
        if (isEmpty($.trim($('#pay-password').val()))) {
            layer.alert("请输入支付密码",{icon:0});
            return false;
        }
        var api_pay_amount=Number($("#api_pay_amount").html());
        if (Number(member_pd)<= api_pay_amount) {
            layer.alert("余额不足,请充值",{icon:0});
            return false;
        }

        $('#password_callback').val('');
        $.ajax({
            url:"/p/payment/userPredeposit/1",
            data:({password:$('#pay-password').val()}),
            type:"POST",
            dataType:"json",
            success:function(data){
                var result=eval(data);
                if(result.status){
                    $(".black_overlay").show();
                    $("#password_callback").val("1");
                    //支付密码
                    $("#submitPwd").val($("#pay-password").val());
                    $("#buy_form").submit();
                    return;
                }else{
                    $('#pay-password').val('');
                    layer.alert(result.msg,{icon:2});
                }
            },
        });
    });




    $('#next_button').on('click',function() {
        var payment_code=$('#payTypeId').val();
        var payTypeVal=$('input[name="pd_pay"]:checked').val();  //预付款或金币

        if($("#pre-deposit").length>0){
            if (!isEmpty(payTypeVal) && $('#password_callback').val() != '1') {
                layer.msg('使用余额支付，需输入支付密码并确认  ',{icon:0});
                return;
            }
            if(checkNeedPayTypeId()){
                //var apiPayAmount = Number($("#api_pay_amount").html());
                if(!isEmpty(payTypeVal)){
                    if (isEmpty(payment_code)) {
                        layer.msg('请选择一种在线支付方式',{icon:0});
                        return;
                    }
                }
            }else{
                payment_code = "FULL_PAY";
                $('#payTypeId').val("FULL_PAY");
            }
        }else{
            if (isEmpty(payment_code)) {
                layer.msg('请选择一种在线支付方式',{icon:0});
                return;
            }
        }

        if(isEmpty(payment_code)&&isEmpty(payTypeVal)){
            if (isEmpty(payment_code)) {
                layer.msg('请选择一种在线支付方式',{icon:0});
                return;
            }
        }

        //设置余额支付方式(1:预付款  2：金币)
        $("#prePayTypeVal").val(payTypeVal);
        //支付密码
        $("#submitPwd").val($("#pay-password").val());
        $("#buy_form").submit();
        layer.open({
            title :"支付提醒",
            type: 1,
            content: $("#pay_overlay").html(),
            area: ['400px', '220px']
            ,btn: ['我知道了'],
            yes:function(index, layero){
                layer.close(index);
            }
        });
    });

    $('#payTypeId').val('');
    $('#password_callback').val('');
    $('input[name="pd_pay"]').attr('checked',false);


    $('input[name="pd_pay"]').on('change', function() {
        showPaySubmit(this);
    });



    //绑定支付方式
    $('.onl-pay-con div').bind('click', function () {
        priceChange();
        var payment_code=$(this).attr("payment_code");
        $("#payTypeId").val(payment_code);
        var bank_code=$(this).attr("bank-code");
        if(isEmpty(bank_code)){
            bank_code="";
        }
        $("#bankCode").val(bank_code);
        $('.onl-pay').find("div").removeClass('pay-selected');
        $(this).addClass('pay-selected');
    });
    $('input[name="pd_pay"]').on('change', function() {
        showPaySubmit(this);
    });
});

function checkNeedPayTypeId(){
    var totalP = Number( $("#totalPrice").html());
    if(totalP==0){
        return false;
    }else{
        return true;
    }
}
//实现余额支付
function showPaySubmit(obj) {
    priceChange();
    if ($(obj).attr('checked')) {
        $('input[name="pd_pay"]').attr('checked',false); //两个都清空
        $(obj).attr('checked',true); //当前选中
        $('#pay-password').val('');
        $('#password_callback').val('');
        $('#pd_password').show();
    } else {
        $('#password_callback').val('');
        $('#pd_password').hide();
    }
}
/**
 * 判断是否是空
 * @param value
 */
function isEmpty(value) {
    if (value == null || value == "" || value == "undefined"
        || value == undefined || value == "null") {
        return true;
    } else {
        value = (value + "").replace(/\s/g, '');
        if (value == "") {
            return true;
        }
        return false;
    }
}

function priceChange(){
    if ($("#pd_pay_input:checked").size()>0){
        $("#predePrice").text(price);
        $("#totalPrice").text('0.00');
    } else {
        $("#predePrice").text('0.00');
        $("#totalPrice").text(price);
    }
}