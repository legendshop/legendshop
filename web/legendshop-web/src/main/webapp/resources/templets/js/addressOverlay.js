function validateForm(){
    var param = new Object();
    param.errorCount = 0;
    param.focus = false;
	validateNullFiled($('#receiver'),param, 'consigneeNameNote', '请您填写收货人姓名');
	if(!isBlank($("#receiver").val())){
		validateSpace($('#receiver'),param, 'consigneeNameNote', '收货人姓名不能包含空格');
	}
	validateNullFiled($('#provinceid'),param,'areaNote','请选择');
	validateNullFiled($('#cityid'),param,'areaNote','请选择');
	validateNullFiled($('#areaid'),param,'areaNote','请选择');
	validateNullFiled($('#subAdds'),param, 'consigneeAddressNote', '请输入详细地址');
	validateAddress($('#subAdds'),param, 'consigneeAddressNote', '详细地址字数不能大于100');
	if(!isBlank($("#subAdds").val())){
		validateSpace($('#subAdds'),param, 'consigneeAddressNote', '详细地址不能包含空格');
	}
	if(isBlank($("#telphone").val())){
		console.log($("#telphone").val());
		validatePhoneFiled($('#mobile'), param, 'consigneeMobileNote','请输入正确的手机号码');
	}
	valitelePhoneFiled($('#telphone'), param, 'consigneeMobileNote','请输入正确的固定电话号码');
	validateEmailFiled($('#email'),param,'emailNote','请输入邮箱');
	validateSubPostFiled($('#subPost'),param,'subPostNote','邮政编码格式不正确');
	/*validateNullFiled($('#aliasAddr'),param,'consigneeAliasNote','地址别名不能为空');*/
	return param.errorCount;
}

function validateEmailFiled(obj, param, name, description){
	if(param.errorCount == 0 && obj.val().length > 0){
	 	var nameNote = $('#' + name);
		//验证邮箱   
		 var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
		if(!reg.test(obj.val())){
				   param.errorCount =  param.errorCount + 1;
			     nameNote.removeClass("hide");
			     if(!param.focus){
			     	obj.focus();
			     	param.focus = true;
			     }
			     nameNote.html(description);
			     }else{
			     	nameNote.addClass("hide");
			     }
			 }
}

//校验邮政编码
function validateSubPostFiled(obj, param, name,description){
	if(param.errorCount == 0 && obj.val().length > 0){
	 	var nameNote = $('#' + name);
		
	 	var subPost = obj.val()		
	
	 	var myReg=/^[1-9][0-9]{5}$/;
	 	if(!myReg.test(subPost)){
			  param.errorCount =  param.errorCount + 1;
		     nameNote.removeClass("hide");
		     if(!param.focus){
		     	obj.focus();
		     	param.focus = true;
		     }
		     nameNote.html(description);
		     }else{
		     	nameNote.addClass("hide");
		     }		
	}
}


function validatePhoneFiled(obj, param, name, description){
	if(param.errorCount == 0){
	 	var nameNote = $('#' + name);
		//验证电话号码手机号码，包含至今所有号段   
		var reg=/^[1][0-9]{10}$/;
		if(!reg.test(obj.val())){
				   param.errorCount =  param.errorCount + 1;
			     nameNote.removeClass("hide");
			     if(!param.focus){
			     	obj.focus();
			     	param.focus = true;
			     }
			     nameNote.html(description);
			     }else{
			     	nameNote.addClass("hide");
			     }
			 }
}

//验证固定电话号码
function valitelePhoneFiled(obj, param, name, description){
	if(param.errorCount == 0 && obj.val().length > 0){
	 	var nameNote = $('#' + name);
	 	var reg = /^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/;
		if(!reg.test(obj.val())){
				   param.errorCount =  param.errorCount + 1;
			     nameNote.removeClass("hide");
			     if(!param.focus){
			     	obj.focus();
			     	param.focus = true;
			     }
			     nameNote.html(description);
		}else{
			     nameNote.addClass("hide");
		}
	}
}


function addAddress123(){
	if(validateForm() == 0){
	  var data = {
	    	"addrId": $("#id").val(),
	    	"receiver": $("#receiver").val(),
         	"provinceId": $("#provinceid").val(),
         	"cityId": $("#cityid").val(),
         	"areaId": $("#areaid").val(),
         	"subAdds": $("#subAdds").val(),
         	"mobile": $("#mobile").val(),
         	"telphone": $("#telphone").val(),
         	"email": $("#email").val(),
         	"status": $("#status").val(),
         	"aliasAddr":$("#aliasAddr").val(),
         	"subPost":$("#subPost").val(),
         	"commonAddr":$("#commonAddr").val()
         };
			$.ajax({
			url: contextPath + "/p/saveaddress" , 
			data: data,
			type:'post', 
			dataType : 'json', 
			async : true, //默认为true 异步   
			success:function(result){
			    if(result == 'OK'){
			    	layer.msg('保存成功', {icon:1,time:1000});
			    	parent.refreshAddress();
			    	layer.closeAll();
			    }else if(result == 'MAX'){
			    	layer.msg('您已经创建了20个收货地址', {icon:0});
			    }else{
			    	layer.msg('保存失败', {icon:2});
			    }
			}
			});
	}
}

function validateNullFiled(obj, param, name, description){
	if(param.errorCount == 0){
	 	var nameNote = $('#' + name);
		if(obj.val().length == 0){
			 param.errorCount =  param.errorCount + 1;
		     nameNote.removeClass("hide");
		     if(!param.focus){
		     	obj.focus();
		     	param.focus = true;
		     }
		     nameNote.html(description);
	     }else{
	     	nameNote.addClass("hide");
	     }
	}
}

function validateAddress(obj, param, name, description){
	if(param.errorCount == 0 && obj.val().length > 0){
		var nameNote = $('#' + name);
		if(obj.val().length > 100){
			param.errorCount =  param.errorCount + 1;
			nameNote.removeClass("hide");
			if(!param.focus){
				obj.focus();
				param.focus = true;
			}
			nameNote.html(description);
		}else{
			nameNote.addClass("hide");
		}
	}
}

//检验空格
function validateSpace(obj, param, name, description){
	if(param.errorCount == 0 && obj.val().length > 0){
		var nameNote = $('#' + name);
		if(obj.val().indexOf(" ") > -1){
			param.errorCount =  param.errorCount + 1;
			nameNote.removeClass("hide");
			if(!param.focus){
				obj.focus();
				param.focus = true;
			}
			nameNote.html(description);
		}else{
			nameNote.addClass("hide");
		}
	}
}

function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}
