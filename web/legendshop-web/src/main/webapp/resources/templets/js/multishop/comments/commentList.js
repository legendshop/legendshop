$(document).ready(function() {

	//定位当前进入的tab
	var conditionVal = $("#condition").val();
	if (!isBlank(conditionVal)) {
		$("#commentTab ul li").each(function(i) {
			$(this).removeClass("on");
			var condition = $(this).attr("condition");
			if (condition === conditionVal) {
				$(this).addClass("on");
			}
		});
	}

	$("#commentTab ul li").click(function() {
		$("#commentTab ul li").each(function(i) {
			$(this).removeClass("on");
		});
		$(this).addClass("on");
		var condition = $(this).attr("condition");
		queryProdCommentList(prodId, condition, 1);
	});
});

function pager(curPageNO) {
	var score = $("#condition").val();
	queryProdCommentList(prodId, score, curPageNO);
}

function queryProdCommentList(prodId, condition, curPageNO) {

	$("#prodCommentCurPageNO").val(curPageNO);
	$("#condition").val(condition);

	var data = {
		"curPageNO" : curPageNO,
		"prodId": prodId,
		"condition" : condition
	};

	$.ajax({
		url : contextPath + "/s/comments/commentListContent",
		data : data,
		type : "GET",
		async : true, //默认为true 异步
		success : function(result) {
			$("#comments-list").html(result);
		}
	});
}

/**
 * 构建一个回复框
 * @param commentId
 * @param userName
 */
function buildReplyInput(commentId, userName, replyType){
	if(replyInput){
		replyInput.find(".inner .wrap-textarea .reply-input").attr("placeholder", "回复 " + userName + ":").val("");
		replyInput.find(".operate-box .reply-submit").attr("commentId", commentId).attr("replyType", replyType);
	}else{
		var topDiv = $("<div></div>").addClass("reply-textarea").addClass("J-reply-con");

		var towDiv = $("<div></div>").addClass("inner");

		var threeDiv = $("<div></div>").addClass("wrap-textarea");

		var textarea = $("<textarea></textarea>").addClass("reply-input").addClass("f-textarea").attr("placeholder", "回复 " + userName + ":");

		var fourDiv = $("<div></div>").addClass("operate-box");

		var replyBtn = $("<a href='javascript:;' target='_self'></a>").addClass("reply-submit").attr("commentId", commentId).attr("replyType", replyType).text("提交");

		fourDiv.append(replyBtn);
		threeDiv.append(textarea);
		towDiv.append(threeDiv);
		topDiv.append(towDiv);
		topDiv.append(fourDiv);

		replyInput = topDiv;

	}

	return replyInput;
}

/**
 * 敏感字检查
 * @param content
 * @returns
 */
function checkSensitiveData(content){
	var url = contextPath + "/sensitiveWord/filter";
	$.ajax({
		"url":url,
		data: {"src": content},
		type:'GET',
		dataType : 'JSON',
		async : false, //默认为true 异步
		success:function(retData){
	   		result = retData;
		}
	});
	return result;
}

function showLogin(){
	layer.open({
		title :"登录",
		  type: 2,
		  content: contextPath + '/loadLoginOverlay', //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['440px', '530px']
		});
 }
//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}
