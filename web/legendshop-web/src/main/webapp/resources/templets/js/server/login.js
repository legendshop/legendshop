$("#form").validate({
	rules : {
		shopAccount : {
			required : true
		},
		account : {
			required : true
		},
		password : {
			required : true
		},
		vcode : {
			required : true
		}
	},
	messages : {
		shopAccount : {
			required : "请输入商家账号"
		},
		account : {
			required : "请输入客服账号"
		},
		password : {
			required : "请输入密码"
		},
		vcode : {
			required : "请输入验证码"
		}
	},
	errorPlacement : function(error, element) {
		element.parent().append(error);
	},
	errorElement : "div",
	submitHandler : function() {
		$.ajax({
			url : "login",
			type : "POST",
			dataType : "JSON",
			data : $("#form").serialize(),
			success : function(result) {
				if (result === "ok") {
					location.href = contextPath + "/customerService/conversation";
				} else {
					art.dialog.tips(result);
				}
			}
		});
	}
});