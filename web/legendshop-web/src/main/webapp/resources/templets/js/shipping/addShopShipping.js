$(function(){
	userCenter.changeSubTab("shippingMange"); //高亮菜单
	//表单校验
	$("#shopShippingForm").validate({
		rules: {
			name: {
				required: true,
				minlength: 2,
				maxlength: 20,
			},
			startDate:{
				required: true,
			},
			endDate:{
				required: true,
			},
			fullValue:{
				required: true,
			},
		},
		messages: {
			name: {
				required: "必填",
				minlength: "名称长度在 2 ~ 20 字",
				maxlength: "名称长度在 2 ~ 20 字",
			},
			startDate:{
				required: "请选择活动时间",
			},
			endDate:{
				required: "请选择活动时间",
			},
			fullValue:{
				required: "必填",
			},
		}
	});

	//保存活动
	$("#save").on("click", function(){
		//校验
		if(!$("#shopShippingForm").valid()) {
			return;
		}
		var description = "";
		var reduceType = $("#reduceType").val();
		var fullValue = $("#fullValue").val();

		if(1 == reduceType){ //金额
			var temp = /^\d+\.?\d{0,2}$/;
		    if (!temp.test(fullValue)) {
		    	layer.msg("请输入正确金额");
		    	return;
		    }else{
		    	description = "满"+fullValue+"元，包邮";
		    }
		 }else{ //件数
			  var temp = /^[1-9]\d*$/;
			  if (!temp.test(fullValue)) {
				  layer.msg("请输入正整数");
				  return;
			   }else{
				   description = "满"+fullValue+"件，包邮";
			   }
		 }
		$("#description").val(description);
		var formData= new FormData($("#shopShippingForm")[0]);
		$.ajax({
			type : "POST",
			dataType : "json",
			url : contextPath + "/s/shippingActive/saveShopShipping",
			data : formData,
			cache: false,
			contentType:false,
			processData:false,
			success : function(result) {
				if (result.status == "OK") {
					layer.alert(result.msg, {icon: 1},function(){
				            window.location.href=contextPath+"/s/shippingActive/query";
			    	});
				} else {
					layer.alert(result.msg, {icon: 2});
				}
			},
			error : function() {
				layer.alert(result.msg, {icon: 2});
			}
		});
	});

});

function pager(curPageNO) {
	$("#curPageNO").val(curPageNO);
	$("#myForm").submit();
}
