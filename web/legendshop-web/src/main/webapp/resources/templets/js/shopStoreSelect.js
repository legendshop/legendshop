
jQuery(document).ready(function($) {
	
		var province = "";
		var city = "";
		var area = "";
	
	   // 获取该商品所在的全部门店
	   getStore(currProdId,province,city,area);
		
	   $("select.combox").initSelect();
	   
	   //切换省份
	   $("#provinceid").change(function(){
		  
		   province = $("#provinceid").children('option:selected').html(); 
		   //搜索门店
		   getStore(currProdId,province,city,area);
		  
	   });
	   
	   //切换城市
	   $("#cityid").change(function(){
		   
		  province = $("#provinceid").children('option:selected').html(); 
		  city = $("#cityid").children('option:selected').html();
		   
		  //搜索门店
		  getStore(currProdId,province,city,area);
		  
	   });
	   
	   //切换地区
	   $("#areaid").change(function(){
		   var area = $(this).children('option:selected').html();  
		   if(area != '' && area != null && area != undefined){  
			   province = $("#provinceid").children('option:selected').html(); 
			   city = $("#cityid").children('option:selected').html();
			   area = $("#areaid").children('option:selected').html();
			   
			   //搜索门店
			   getStore(currProdId,province,city,area);
		   }  
	   }); 
    
    
});

//获取商品所在门店列表
function getStore(currProdId,province,city,area){
	
   var data={"poductId":currProdId,"province":province,"city":city,"area":area}
   $.ajax({
        //提交数据的类型 POST GET
        type:"GET",
        //提交的网址
        url:contextPath+"/storeProductCitys",
        //提交的数据
        async : true,  
        //返回数据的格式
        datatype: "json",
        data:data,
        //成功返回之后调用的函数            
        success:function(data){
        	if(data != null && data != '' && data != undefined && data != '[]'){
				var result=eval(data);
				var html="";
				$.each(result, function(i,val){
					html+=" <li><div class='handle'><a href='javascript:void(0);'  onclick='updateShopStore("+val.storeId+","+currProdId+","+currSkuId+","+basketCount+");' >选择&gt;</a></div>";
					html+=" <h5><i></i><a target='_blank' href='javascript:void(0);'>"+val.storeName+"</a></h5>";
					html+="  <p>"+val.address+"</p></li>";
				});
				$("ul[nctype='chain_see']").html(html);
			}else{
				$("ul[nctype='chain_see']").html("<p>所选地区没有该商户的门店，请选择其他地区或在线购买！</p>");
			}
        }       
    });
}
//更改门店
function updateShopStore(storeId,prodId,skuId,basketCount) {
	
	var params={
			
		"productId":prodId,
		"skuId":skuId,
		"storeId":storeId,
		"basketCount":basketCount
	};
	
	$.ajax({
        type : "POST",
        url : contextPath+"/shopCart/updateShopStore",
        data : params,
        async:true,
        cache: false,
        dataType : "json",
        success : function(result) {
        	 if(result.buySts=="OK"){
        		 window.parent.reloadBuyTable();
        	 }else{
        		 layer.tips(result.buyMsg,{icon: 2});
        	 }
        	
        }
    });
}



