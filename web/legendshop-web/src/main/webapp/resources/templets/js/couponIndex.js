$(function(){
		curUseStatus = $("#useStatus").val();
		if(curUseStatus == 2){
			$("#listType li").removeClass("on");
			$("#listType li[value='2']").addClass("on");
		}else if(curUseStatus == 3){
			$("#listType li").removeClass("on");
			$("#listType li[value='3']").addClass("on");
		}else if(curUseStatus == 4){
			$("#listType li").removeClass("on");
			$("#listType li[value='4']").addClass("on");
		}else{
			$("#listType li").removeClass("on");
			$("#listType li[value='1']").addClass("on");
		}

		$("#listType li").click(function(){
			var value = $(this).val();
			if(value == curUseStatus){
				return;
			}
			if(value == 4){
				loadList(contextPath + "/p/getCoupon?couPro="+_couPor);
			}else{
				curUseStatus = value;
				$("#listType li").removeClass("on");
				$(this).addClass("on");
				//加载当前使用状态
				changeUseStatus(curUseStatus);
			}

		});
	});

	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}

	//改变使用状态
	function changeUseStatus(usestatus){
		window.location.href=contextPath+"/p/coupon?useStatus="+usestatus+"&couPro="+_couPor;
	}

	function getCoupon(){
		window.location.href=contextPath+"/p/coupon?useStatus="+usestatus+"&couPro="+_couPor;
	}

	function loadList(url){
		$.ajax({
			url : url,
			type : "GET",
			data : "",
			cache : false,
			async : true,
			success : function(result,status,xhr){
				$("#listType li").removeClass("on");
				$("#listType li[value='4']").addClass("on");
				curUseStatus = 4;
				$("#recommend").html(result);
			}
		});
	}
