$(function(){
	/**搜索*/
	$("#seach").click(function(){
		prodIdList.splice(0,prodIdList.length);
		var prodName = $("#prodName").val();
		var editActiveId = $("#editActiveId").val();
		var check = $("#isCheck").val();
		var url = contextPath + "/s/shippingActive/editfullProd/"+editActiveId;
		$.ajax({
			url : url,
			data : {"prodName":prodName,"check":check},
			type : "get",
			dataType : "html",
			success : function(result) {
				$(".all-prod").html(result);
			}
		});
	})


	//用于分页勾选
	$(".ant-table-tbody").find(".ant-table-row").each(function(index,element){
		var prodId = $(element).find(".ant-checkbox").attr("data-id");

		//勾上复选框
		var index = prodIdList.indexOf(prodId);
		if(index > -1) {
			$(element).find(".ant-checkbox").addClass("ant-checkbox-checked");
			$(element).find(".ant-checkbox").addClass("ant-checkbox-checked-1");
		}

		//展示是否已参加
		var joinIndex = joinProdIdList.indexOf(prodId);
		if(joinIndex > -1){
			$(element).find(".caozuo").html("<span>已选择<a class='pushl' data-id='"+prodId+"'>取消参加</a></span>");
		}
	})

	//用于分页展示不可编辑样式
	$(".ant-table-tbody").find(".ant-table-row").each(function(index,element){
		var prodId = $(element).find(".ant-checkbox").attr("data-id");

		//展示是否已参加
		var joinIndex = joinProdIdList.indexOf(prodId);
		if(joinIndex > -1){
			$(element).find(".ant-checkbox-input").attr("disabled","disabled");
			$(element).find(".ant-checkbox").removeAttr("optional");
			$(element).find(".caozuo").html("<span>已选择<a class='pushl' data-id='"+prodId+"'>取消参加</a></span>");
		}
	})




})
