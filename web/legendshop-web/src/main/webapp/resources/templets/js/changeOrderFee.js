$(document).ready(function() {
	
	//监控普通订单调整运费
	$(document).on("change", "#order_fee_amount, #order_fee_ship_price", function(){
		var fee_amount = $("#order_fee_amount").val();
		if(isBlank(fee_amount) || !isMoney(fee_amount) || fee_amount == 0){
			layer.alert('订单金额必须为大于0的数字', {icon: 0});
			return false;
		}
		var _freight=$("#order_fee_ship_price").val();
		if(isBlank(_freight) || !isMoney(_freight)){
			layer.alert('请正确输入配送金额', {icon: 0});
			return false;
		}
		var total_price = (Number(fee_amount)+Number(_freight)).toFixed(2);
		$("#totalPrice").val(total_price);
		$("#totalPriceText").html("￥"+total_price);
	});
	
	//监控预售订单调整运费
	$(document).on("change", "#preDepositPrice, #finalPrice", function(){
		var preDepositPrice = $("#preDepositPrice").val();
		if(isBlank(preDepositPrice) || !isMoney(preDepositPrice) || preDepositPrice == 0){
			layer.alert('请输入正确的定金金额', {icon: 0});
			return false;
		}
		
		var finalPrice = $("#finalPrice").val();
		if(payPctType == 1) { //定金支付
			if(isBlank(finalPrice) || !isMoney(finalPrice) || finalPrice == 0){
				layer.alert('请输入正确的尾款金额', {icon: 0});
				return false;
			}
		}
		
		var totalPrice = 0;
		if(payPctType == 1) {//定金支付
			totalPrice = (Number(preDepositPrice)+Number(finalPrice)).toFixed(2);
		}else {//全额支付
			totalPrice = Number(preDepositPrice).toFixed(2);
		}
		$("#preTotalPrice").val(totalPrice);
		$("#preTotalPriceText").html("￥"+totalPrice);
	});

});


//调整普通订单费用
function changeOrderFee(){
	var _subnumber = $.trim($("#subNumber").val());
	if(isBlank(_subnumber)){
		return;
	}
	var fee_amount = $("#order_fee_amount").val();
	if(isBlank(fee_amount) || !isMoney(fee_amount) || fee_amount==0){
		layer.alert('订单金额必须为大于0的数字', {icon: 0});
		return false;
	}

	var _freight = $("#order_fee_ship_price").val();
	
	if(isBlank(_freight) || !isMoney(_freight) || _freight < 0){
		layer.alert('请正确输入配送金额', {icon: 0});
		return false;
	}
	
	$("#orderFee").attr("disabled","disabled");
	$.ajax({
		url:contextPath+"/s/changeOrderFee", 
		data:{"subNumber":_subnumber,"freight":_freight,"orderAmount":fee_amount},
		type:'POST', 
		async : false, //默认为true 异步   
		dataType:'json',
		error: function(jqXHR, textStatus, errorThrown) {
			$("#orderFee").attr("disabled","");
			return;
		},
		success:function(result){
			if(result=="OK"){
				layer.msg("调整订单费用成功!",{icon:1, time:1000}, function(){
					window.parent.location.reload();
				});
			}else if(result=="fail"){
				$("#orderFee").attr("disabled","");
				layer.alert('调整订单费用失败,金额应该不大于99999999', {icon: 2});
				return;
			}
		}
	});
}

//调整预售订单费用
function changePreOrderFee(){
	
	var subNumber = $.trim($("#subNumber").val())
	var preDepositPrice = $("#preDepositPrice").val();
	var finalPrice  = $("#finalPrice").val();
	
	if(isBlank(subNumber)){
		return;
	}
	
	if(isBlank(preDepositPrice) || !isMoney(preDepositPrice) || preDepositPrice==0){
		layer.alert('请输入正确的定金金额', {icon: 0});
		return false;
	}
	
	if(payPctType == 1) {
		if(isBlank(finalPrice) || !isMoney(finalPrice) || finalPrice < 0){
			layer.alert('请输入正确的尾款金额', {icon: 0});
			return false;
		}
	}else {
		finalPrice = 0;
	}
	
	$("#orderFee").attr("disabled","disabled");
	$.ajax({
		url : contextPath+"/s/changePreOrderFee", 
		data : {"subNumber":subNumber, "preDepositPrice":preDepositPrice, "finalPrice":finalPrice},
		type : "POST", 
		async : false, //默认为true 异步   
		dataType : "JSON",
		error: function(jqXHR, textStatus, errorThrown) {
			$("#orderFee").attr("disabled","");
			return;
		},
		success:function(result){
			if(result=="OK"){
				layer.msg("调整订单费用成功!",{icon:1, time:1000}, function(){
					window.parent.location.reload();
				});
			}else if(result=="fail"){
				layer.alert("调整订单费用失败!", {icon:2});
				$("#orderFee").attr("disabled","");
				return;
			}
		}
	});
}

function isMoney(obj) {
	if (!obj)
		return false;
	return (/^\d+(\.\d{1,2})?$/).test(obj);
}

//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}
