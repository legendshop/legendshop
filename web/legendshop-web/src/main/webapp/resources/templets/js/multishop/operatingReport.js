$(function () { 
	operatingReport.doExecute();
	userCenter.changeSubTab("operatingReport");
	
	var queryTerms = $("#queryTerms").val();
	change(queryTerms);
	
	//路径配置
	require.config({
		paths: {
	        echarts: contextPath+'/resources/plugins/ECharts/dist'
	    }
	});

	//使用
	require(
			[ 
		        'echarts', 
		        'echarts/chart/bar',
		        'echarts/chart/line'
		    ],
		    function (ec) {
				// 基于准备好的dom，初始化echarts图表
		        var myChart = ec.init(document.getElementById('main')); 
				
		        var option = {
		        	    tooltip : {
		        	        trigger: 'axis'
		        	    },
		        	    toolbox: {
		        	        show : true,
		        	        feature : {
		        	            mark : {show: true},
		        	            dataView : {show: true, readOnly: false},
		        	            magicType: {show: true, type: ['line', 'bar']},
		        	            restore : {show: true},
		        	            saveAsImage : {show: true}
		        	        }
		        	    },
		        	    calculable : true,
		        	    legend: {
		        	        data:['订单数量','订单金额']
		        	    },
		        	    xAxis : [
		        	        {
		        	            type : 'category',
		        	            data : getXAxisData()
		        	        }
		        	    ],
		        	    yAxis : [
		        	        {
		        	            type : 'value',
		        	            name : '订单数量'	  
		        	        },
		        	        {
		        	            type : 'value',
		        	            name : '订单金额'
		        	        }
		        	    ],
		        	    series : [

		        	        {
		        	            name:'订单数量',
		        	            type:'bar',
		        	            itemStyle:{
		                              normal:{color:'#2ec7c9'}
		                         },
		        	            data : getBarData()
		        	        },
		        	        {
		        	            name:'订单金额',
		        	            type:'line',
		        	            yAxisIndex: 1,
		        	            itemStyle:{
		                              normal:{color:'#b6a2de'}
		                        },
		        	            data : getLineData()
		        	        }
		        	    ]
		        	};
		        
		     	// 为echarts对象加载数据 
		        myChart.setOption(option); 
			}
	);
}); 

function pager(curPageNO){
    document.getElementById("curPageNO").value=curPageNO;
    document.getElementById("from1").submit();
}

//获取 x轴 数据
function getXAxisData(){
	var list = [];
    var str = "";
	
	var queryTerms = $("#queryTerms").val();
    if(queryTerms == 0){
    	for(var x=0;x<24;x++){
        	str = x+"时";
        	list.push(str);
        }
    }else if(queryTerms == 1){
    	var lastDay = getLastDay(selectedYear,(Number(selectedMonth)-1));
    	for(var y=1;y<=lastDay;y++){
        	str = y+"日";
        	list.push(str);
        }
    }else{
    	for(var z=1;z<=7;z++){
        	str = "星期"+z;
        	list.push(str);
    	}
    }
    
    return list;
}

//获取 商品销量 数据
function getBarData(){
	var list = [];
	var queryTerms = $("#queryTerms").val();
      if(!isBlank($("#reportJson").val())){
    	  var reportList = jQuery.parseJSON($("#reportJson").val());
    	  if(queryTerms == 0){
    		  for(var x=0;x<24;x++){//按天计算
    			  var flag = true;
        		  for(var q = 0;q < reportList.length;q++){
        			  if(x == reportList[q].subDate){
        				  list.push(reportList[q].numbers);
        				  flag = false;
        				  break;
        			  }
                  }
        		  if(flag){
        			  list.push(0);
        		  }
        	  }
    	  }else if(queryTerms == 1){//按月计算
    		  var lastDay = getLastDay(selectedYear,(Number(selectedMonth)-1));
    		  for(var y=1;y<=lastDay;y++){
    			  var flag = true;
    			  for(var q = 0;q < reportList.length;q++){
        			  if(y == reportList[q].subDate){
        				  list.push(reportList[q].numbers);
        				  flag = false;
        				  break;
        			  }
                  }
    			  if(flag){
    				  list.push(0);
    			  }
    		  }
    	  }else{//按周计算
            	for(var z=1;z<=7;z++){
            		var flag = true;
            		for(var q = 0;q < reportList.length;q++){
            			  if(z == reportList[q].subDate){
            				  list.push(reportList[q].numbers);
            				  flag = false;
            				  break;
            			  }
                    }
            		if(flag){
            			list.push(0);
            		}
            	}
          }
    	  
    	  //console.debug("数量list："+list);
      }
      return list;
}

//获取 销售金额 数据
function getLineData(){
	var list = [];
	var queryTerms = $("#queryTerms").val();
    if(!isBlank($("#reportJson").val())){
  	  var reportList = jQuery.parseJSON($("#reportJson").val());		                            	   
  	  if(queryTerms == 0){
  		  for(var x=0;x<24;x++){//按天计算
  			  var flag = true;
      		  for(var q = 0;q < reportList.length;q++){
      			  if(x == reportList[q].subDate){
      				  list.push(reportList[q].totalCash);
      				  flag = false;
      				  break;
      			  }
                }
      		  if(flag){
      			  list.push(0);
      		  }
      	  }
  	  }else if(queryTerms == 1){//按月计算
  		  var lastDay = getLastDay(selectedYear,(Number(selectedMonth)-1));
  		  for(var y=1;y<=lastDay;y++){
  			  var flag = true;
  			  for(var q = 0;q < reportList.length;q++){
      			  if(y == reportList[q].subDate){
      				  list.push(reportList[q].totalCash);
      				  flag = false;
      				  break;
      			  }
                }
  			  if(flag){
  				  list.push(0);
  			  }
  		  }
  	  }else{//按周计算
          	for(var z=1;z<=7;z++){
          		var flag = true;
          		for(var q = 0;q < reportList.length;q++){
          			  if(z == reportList[q].subDate){
          				  list.push(reportList[q].totalCash);
          				  flag = false;
          				  break;
          			  }
                    }
          		if(flag){
          		  list.push(0);
          		}
          	}
        }
  	  //console.debug("金额list："+list);
    }
    return list;
}