userCenter.changeSubTab("sellingProd");
function skuPager(curPageNO){
	var url=contextPath + "/s/loadSkuListPage?prodId="+prodId+"&curPageNO="+curPageNO;
	window.location.href=url;
}

function loadStockLog(prodId,skuId){
	$.ajax({
		type: "POST",
		url: contextPath+"/s/loadStockLog",
		async:true,
		data:{"prodId":prodId,"skuId":skuId},
		dataType: "html",
		beforeSend: function(){
			$("#stoackLogPanl").showLoading();
		},
		complete: function(){
			$("#stoackLogPanl").hideLoading();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			$("#stoackLogPanl").hideLoading();
			layer.alert("您无权限执行此操作!",{icon:2});
		},
		success:function(data){
			$("#stoackLogPanl").empty().append(data)
		}
	});

}

function stockLogPager(curPageNO){
	var skuId=$("#skuId").val();
	var  url=contextPath + "/s/loadStockLog?prodId="+prodId+"&skuId="+skuId+"&curPageNO="+curPageNO;
	$("#stoackLogPanl").load(url);
}
var index = 0;
var orderStocks="";
var orderActualStocks="";
function stockEdit(itself){
    var stocks = $(itself).parent().siblings(".stocks");
    var actualStocks = $(itself).parent().siblings(".actualStocks");
    var skuid = $(itself).parent().siblings(".sid").find("input").val();
    var newStocks=stocks.find("input").val();
    var newActualStocks=actualStocks.find("input").val();
    if (index==0){
        orderStocks=stocks.text();
        orderActualStocks=stocks.text();
        $(itself).text("保存")
        stocks.html("<input style='width:70px'  type='text' maxlength=8 value='"+stocks.text()+"'>");
        actualStocks.html("<input style='width:70px'  type='text' maxlength=8 value='"+actualStocks.text()+"'>");
        index++;
    }else {
        $(itself).text("修改库存");
        if (isNaN(newStocks) || isNaN(newActualStocks)){
            stocks.text(orderStocks);
            actualStocks.text(orderActualStocks);
            layer.alert("库存必须为数字",{icon: 2});
            index=0;
			      return;
        }
        if (!(/(^[1-9]\d*$)/.test(newStocks)) || !(/(^[1-9]\d*$)/.test(newActualStocks))){
          stocks.text(orderStocks);
          actualStocks.text(orderActualStocks);
          layer.alert("库存必须为正整数",{icon: 2});
          index=0;
          return;
        }

        if (newStocks!="" && newStocks!=null) {
            stocks.text(newStocks);
        }else {
            stocks.text(orderStocks);
            actualStocks.text(orderActualStocks);
            index=0;
            return;
		    }

        if (newActualStocks!="" && newActualStocks!=null) {
            actualStocks.text(newActualStocks);
        }else {
            stocks.text(orderStocks);
            actualStocks.text(orderActualStocks);
            index=0;
            return;
		    }
        index=0;
        $.ajax({
            url: '/s/editUpdateStocks',
            data: {
                "skuId": skuid,
                "stock": newStocks,
                "actualStocks": newActualStocks
            },
            type: 'post',
            dataType: 'json',
            async: false, //默认为true 异步
            success: function (result) {
                parent.layer.msg('修改成功', {icon: 1});
                // actualStocks.text(result)
                return;
            }
        });
	}

}
$(function(){
	$("#checkAll").click(function(){
		$(".skuid").prop("checked",this.checked)
    })

    $(".skuid").click(function(){
        $(".skuid").each(function () {
            $("#checkAll").prop("checked",$(".skuid:not(:checked)").size()>0?false:true)
        })
    })
})

function batchEdit(){
    //获取选择的记录集合
    selAry =$(".skuid:checked");
    var array=[];
    selAry.each(function(index,dom){
        array[index]=dom.value;
    })
    if (!checkSelect(selAry)) {
        layer.msg('至少选中一条记录！',{icon: 2});
        return false;
    }
    layer.open({
        title :"批量修改",
        id: "batchEditStock",
        type: 2,
        resize: false,
        content: [contextPath+"/s/toBatchEditStocks?selAry="+array,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
        area: ['400px', '350px']
    });
    return true;
}
