$(document).ready(function() {
	   		 //文本框绑定事件
		    $("#payPwd").bind('keyup onfocus onblur', function () {
			
		    		$("#payPwdMessage").show();
		    		$("#payPwd_error").hide();
			        var index = checkStrong(this.value);
			        $("#strength").removeClass("icon-s-00");
			        $("#strength").removeClass("icon-s-01");
			        $("#strength").removeClass("icon-s-02");
			        $("#strength").removeClass("icon-s-03");
			        if(index == 4){
			        	index = 3;
			        }
					$("#strength").addClass("icon-s-0" + index);
					$("#payStrength").val(index);
					
			    });
	   		 
		    $("#payPwd").bind('blur', function () {
		    	var payPwd = $("#payPwd").val();
		    	$("#payPwdMessage").hide();
		    		if(payPwd.length<6){
		    			$("#payPwd_error").text("支付密码不能少于6位");
		    			$("#payPwd_error").show();
		    		}
		    });
		    
		    $("#rePayPwd").bind('focus', function () {
				$("#rePayPwd_error").hide();
		});
		    
		    $("#rePayPwd").bind('blur',function(){
		    	var rePayPwd = $("#rePayPwd").val();
		    	if(rePayPwd.length<6){
		    		$("#rePayPwd_error").text("密码不能少于6位");
		    		$("#rePayPwd_error").show();
		    	}
		    });
		});
//密码检测密码强度
function checkStrong(sValue) {
    var modes = 0;
    //正则表达式验证符合要求的
    if (sValue.length < 1) return modes;
    if (/\d/.test(sValue)) modes++; //数字
    if (/[a-z]/.test(sValue)) modes++; //小写
    if (/[A-Z]/.test(sValue)) modes++; //大写  
    if (/\W/.test(sValue)) modes++; //特殊字符
   
   //逻辑处理
    switch (modes) {
        case 1:
            return 1;
            break;
        case 2:
            return 2;
        case 3:
        case 4:
            return sValue.length < 12 ? 3 : 4
            break;
    }
}


//更改支付密码成功
function updatePayPwd(){
		var payPwd = $("#payPwd").val();
		
		//var test = /(?!^[a-zA-Z]+$)(?!^[\d]+$)(?!^[^a-zA-Z\d]+$)^.{6,20}$/;
		var test = /^[\x21-\x7E]{6,20}$/;
		
		if(payPwd.indexOf(" ")>-1){
			$("#payPwd_error").text("支付密码由6-20位字母、数字或符号(除空格)组成");
			$("#payPwd_error").show();
			return;
		}
		if(!test.test(payPwd)){
			$("#payPwd_error").text("支付密码由6-20位字母、数字或符号(除空格)组成");
			$("#payPwd_error").show();
			return;
		}
		var rePayPwd  = $("#rePayPwd").val();
					if(payPwd == rePayPwd){
						if(validateRandNum(contextPath)){
						$.ajax({
							url:contextPath + "/p/updatePaymentpassword",
							data:{"paymentPassword":payPwd,"securityCode":$("#securityCode").val(), "payStrength": $("#payStrength").val(),"emailCode":$("#emailCode").val()},
							type:'post', 
							async : true,   
							error: function(jqXHR, textStatus, errorThrown) {
								layer.alert("网络异常，请稍后重试！",{icon:2});
							},
							success:function(retData){
								$("#rightContent").html(retData);
							}
						});	
					}
				}else{
					$("#rePayPwd_error").text("两次输入的密码不匹配!");
	    			$("#rePayPwd_error").show();
				}
}