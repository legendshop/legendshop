$(document).ready(function() {
	userCenter.changeSubTab("favoriteShop");
});
function myFavourite(){
	window.location.href= contextPath + "/p/favourite";
}

function pager(curPageNO){
	userCenter.loadPageByAjax("/p/favoriteShop?curPageNO=" + curPageNO);
}

function deleteById(id){
	layer.confirm("删除后不可恢复, 确定要删除吗？", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		$.ajax({
			url : contextPath + "/p/favoriteShop/delete/"+id,
			type : "POST",
			async : true, //默认为true 异步
			success : function(result){
				layer.msg("删除成功",{icon: 1});
				setTimeout(function(){
					window.location = contextPath + "/p/favoriteShop";
				},800);
			}
		});
	});
}


//页面的删除
function deleteAction(){
	//获取选择的记录集合
	selAry = $(".selectOne");
	if(!checkSelect(selAry)){
		layer.alert("删除时至少选中一条记录！",{icon: 0});
		return false;
	}
	layer.confirm("删除后不可恢复, 确定要删除吗？", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		var ids = [];
		for(i=0;i<selAry.length;i++){
			if(selAry[i].checked){
				ids.push(selAry[i].value);
			}
		}
		var result = deleteFavoriteShop(ids);
		if("OK" == result){
			layer.msg("删除收藏成功",{icon: 1,time: 800},function(){
				$("#favoriteShop").click();
			});
		}else{
			layer.msg("删除收藏失败",{icon: 2});
		}
	});

	return true;
}

//清空
function clearAction(){
	layer.confirm("清空后不可恢复, 确定要清空吗？", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		var url = contextPath + "/p/deleteAllfavoriteShop";
		$.post(url,function(result){
			if("OK" == result){
				layer.msg("清空收藏成功",{icon: 1,time: 800},function(){
					$("#favoriteShop").click();
				});
			}else{
				layer.msg("清空收藏失败",{icon: 2});
			}
		});
	});
}

function deleteFavoriteShop(ids) {
	var result = "FAIL";
	var idsJson = JSON.stringify(ids);
	jQuery.ajax({
		url : contextPath + "/p/deletefavoriteShop",
		data : {"ids" : idsJson},
		type : "POST",
		async : false, //默认为true 异步
		dataType : "JSON",
		success:function(retData){
			result = retData;
		}
	});
	return result;
}

//全选
$(".selectAll").click(function(){

	if($(this).attr("checked")=="checked"){
		$(this).parent().parent().addClass("checkbox-wrapper-checked");
         $(".selectOne").each(function(){
        		 $(this).attr("checked",true);
        		 $(this).parent().parent().addClass("checkbox-wrapper-checked");
         });
     }else{
         $(".selectOne").each(function(){
             $(this).attr("checked",false);
             $(this).parent().parent().removeClass("checkbox-wrapper-checked");
         });
         $(this).parent().parent().removeClass("checkbox-wrapper-checked");
     }
 });

function selectOne(obj){
	if(!obj.checked){
		$(".selectAll").checked = obj.checked;
		$(obj).prop("checked",false);
		$(obj).parent().parent().removeClass("checkbox-wrapper-checked");
	}else{
		$(obj).prop("checked",true);
		$(obj).parent().parent().addClass("checkbox-wrapper-checked");
	}
	 var flag = true;
	  var arr = $(".selectOne");
	  for(var i=0;i<arr.length;i++){
	     if(!arr[i].checked){
	    	 flag=false;
	    	 break;
	    	 }
	  }
	 if(flag){
		 $(".selectAll").prop("checked",true);
		 $(".selectAll").parent().parent().addClass("checkbox-wrapper-checked");
	 }else{
		 $(".selectAll").prop("checked",false);
		 $(".selectAll").parent().parent().removeClass("checkbox-wrapper-checked");
	 }
}
