$(document).ready(function() {
	userCenter.changeSubTab("FULL_MAIL"); //高亮菜单
});

jQuery.validator.methods.greaterThanStartDate = function(value, element) {
	var start_date = $("#startTime").val();
	var date1 = new Date(Date.parse(start_date.replace(/-/g, "/")));
	var date2 = new Date(Date.parse(value.replace(/-/g, "/")));
	return date1 < date2;
};


//页面输入内容验证
$("#add_form").validate({
	errorPlacement: function(error, element){
		var error_td = element.parent('dd').children('span.error-message');
		error_td.append(error);
	},
	ignore: "",
	onfocusout: false,
	rules : {
		marketName : {
			required : true
		},
		startTime : {
			required : true,
		},
		endTime : {
			required : true,
			greaterThanStartDate : true
		},
		status:{
			required : true
		},
		type:{
			required : true
		},
		rule_count: {
			required: true,
			min: 1,
		}
	},
	messages : {
		marketName : {
			required : '<i class="icon-exclamation-sign"></i>活动名称不能为空'
		},
		startTime : {
			required : '<i class="icon-exclamation-sign"></i>开始时间不能为空'
		},
		endTime : {
			required : '<i class="icon-exclamation-sign"></i>结束时间不能为空',
			greaterThanStartDate : '<i class="icon-exclamation-sign"></i>结束时间必须大于开始时间'
		},
		status:{
			required : '<i class="icon-exclamation-sign"></i>请选择商品参与方式',
		},
		type:{
			required : '<i class="icon-exclamation-sign"></i>请选择促销类型',
		},
		rule_count: {
			required: '<i class="icon-exclamation-sign"></i>请至少添加一条规则并确定',
			min: '<i class="icon-exclamation-sign"></i>请至少添加一条规则并确定'
		}
	},
	submitHandler:function(form){
		$("input[type='submit']").attr("disabled","disabled");
		$("#add_form").ajaxForm().ajaxSubmit({
			success:function(data) {
				var result=eval(data);
				if(result=='OK'){
					layer.msg('添加成功',{icon:1,time:500},function(){
						window.location.href=contextPath+"/s/fullmail";

					});
					return;
				}else{
					layer.alert(result,{icon:2});
					$("input[type='submit']").removeAttr("disabled","disabled");
					return ;
				}
			},
			error:function(XMLHttpRequest, textStatus,errorThrown) {
				layer.alert('失败！请重试！',{icon:2});
				$("input[type='submit']").removeAttr("disabled","disabled");
				return false;
			}	
		});
	}

});

// 限时添加规则窗口
$('#btn_add_rule').on('click', function() {
	var type= $("input[name='type']:checked").val();
	if(type=='5'){  //满减促销
		$('#add_manjian_rule').show();
		$('#add_manze_rule').hide();
		$("#manze_rule_list").hide();
		$("#manjian_rule_list").show();
		$('#manjian_price').val('');
		$('#manjian_discount').val('');
	}else if(type=='4'){ //满折促销 
		$('#add_manze_rule').show();
		$('#add_manjian_rule').hide();
		$("#manjian_rule_list").hide();
		$("#manze_rule_list").show();
		$('#manze_price').val('');
		$('#manze_discount').val('');
	}
	$('#btn_add_rule').hide();
});

$('#btn_add_rule').hide(); 
$('#add_manjian_rule').show();
$('#add_manze_rule').hide();
$("#goods_type_0").attr("checked","checked");


$("input[name='type']").change(function(){
	var type=$(this).val();
	var rule_count ;
	if(type=='5'){  //满减促销
		rule_count = $('#manjian_rule_list').find('[nctype="mansong_rule_item"]').length;
		if( rule_count >= 3) {
			$('#add_manjian_rule').hide();
		}else{  
			$('#add_manjian_rule').show();
		}
		$('#add_manze_rule').hide();
		$('#manjian_price').val('');
		$('#manjian_discount').val('');
		$("#manze_rule_list").hide();
		$("#manjian_rule_list").show();
	}else if(type=='4'){ //满折促销 
		rule_count = $('#manze_rule_list').find('[nctype="mansong_rule_item"]').length;
		if(rule_count >= 3) {
			$('#add_manze_rule').hide();
		}else{  
			$('#add_manze_rule').show();
		}
		$('#add_manjian_rule').hide();
		$('#manze_price').val('');
		$('#manze_discount').val('');
		$("#manjian_rule_list").hide();
		$("#manze_rule_list").show();
	}
	$('#btn_add_rule').hide();
	$('#mansong_rule_count').val(rule_count);
});



// 规则保存
$('#save_mj_rule').on('click', function() {
	var mansong = {};
	mansong.price = Number($('#manjian_price').val());
	if(isNaN(mansong.price) || mansong.price < 0) {
		layer.alert("规则金额不能为空且必须为数字",{icon:0});
		return false;
	} 


	var mansong_rule_item=ruleHtml(mansong.price,"包邮");

	$('#manjian_rule_list').append(mansong_rule_item);

	close_mj_rule();
});


// 规则保存
$('#save_mz_rule').on('click', function() {
	var mansong = {};
	var reg=new RegExp("^[0-9]*$");
	mansong.price = Number($('#manze_price').val());
	if(!reg.test(mansong.price) || mansong.price <= 0) {
		layer.alert("规则件数不能为空且必须为整数",{icon:0});
		return false;
	} 

	var mansong_rule_item=ruleHtml2(mansong.price,"包邮");
	$('#manze_rule_list').append(mansong_rule_item);
	close_mz_rule();
});



// 关闭规则添加窗口
function close_mz_rule() {
	var rule_count = $('#manze_rule_list').find('[nctype="mansong_rule_item"]').length;
	if( rule_count >= 3) {
		$('#btn_add_rule').hide();
	} else {
		$('#btn_add_rule').show();
	}
	$('#add_manze_rule').hide();
	$('#mansong_rule_count').val(rule_count);
}

// 关闭规则添加窗口
function close_mj_rule() {
	var rule_count = $('#manjian_rule_list').find('[nctype="mansong_rule_item"]').length;
	if( rule_count >= 3) {
		$('#btn_add_rule').hide();
	} else {
		$('#btn_add_rule').show();
	}
	$('#add_manjian_rule').hide();
	$('#mansong_rule_count').val(rule_count);
}

// 删除已添加的规则
$('#manjian_rule_list').on('click', '[nctype="btn_del_mansong_rule"]', function() {
	$(this).parents('[nctype="mansong_rule_item"]').remove();
	close_mj_rule();
});

// 删除已添加的规则
$('#manze_rule_list').on('click', '[nctype="btn_del_mansong_rule"]', function() {
	$(this).parents('[nctype="mansong_rule_item"]').remove();
	close_mz_rule();
});

// 取消添加规则
$('#btn_cancel_mj_rule').on('click', function() {
	close_mj_rule();
});

// 取消添加规则
$('#btn_cancel_mz_rule').on('click', function() {
	close_mz_rule();
});

function ruleHtml(price,discount){
	var rule_count = $('#manjian_rule_list').find('[nctype="mansong_rule_item"]').length;
	var html="<li nctype='mansong_rule_item'><span>满<strong>"+price+"</strong>元， </span><span><strong>"+discount+"</strong> </span>";
	html+="<input type='hidden' value='"+price+"' name='manyuan_fullmail_rule["+rule_count+"]'>";
	html+="<a class='ncbtn-mini ncbtn-grapefruit' href='javascript:void(0);' nctype='btn_del_mansong_rule'>删除</a></li>";
	return html;
}

function ruleHtml2(price,discount){
	var rule_count = $('#manze_rule_list').find('[nctype="mansong_rule_item"]').length;
	var html="<li nctype='mansong_rule_item'><span>满<strong>"+price+"</strong>件， </span><span><strong>"+discount+"</strong> </span>";
	html+="<input type='hidden' value='"+price+"' name='manjian_fullmail_rule["+rule_count+"]'>";
	html+="<a class='ncbtn-mini ncbtn-grapefruit' href='javascript:void(0);' nctype='btn_del_mansong_rule'>删除</a></li>";
	return html;
}

laydate.render({
	elem: '#startTime',
	calendar: true,
	theme: 'grid',
	type:'datetime',
	min:'-1',
	trigger: 'click'
});

laydate.render({
	elem: '#endTime',
	calendar: true,
	theme: 'grid',
	type:'datetime',
	min:'-1',
	trigger: 'click'
});
