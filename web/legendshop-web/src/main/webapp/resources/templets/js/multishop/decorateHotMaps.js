(function($) {
    jQuery.fn.imageMaps = function(setting) {
        var $container = this;
        if ($container.length == 0) return false;
		$container.each(function(){
			var container = $(this);
			var $images = container.find('img[ref=imageMaps]');
			$images.wrap('<div class="image-maps-conrainer" style="position:relative;"></div>').css('border','1px solid #ccc');
			$images.each(function(){
				var _img_conrainer = $(this).parent();
				_img_conrainer.append('<div class="link-conrainer"></div>').append($.browser.msie ? $('<div class="position-conrainer" style="position:absolute"></div>').css({
					background:'#fff',
					opacity:0
				}) : '<div class="position-conrainer" style="position:absolute;z-index:9999;"></div>');
				var _img_offset = $(this).offset();
				var _img_conrainer_offset = _img_conrainer.offset();
				_img_conrainer.find('.position-conrainer').css({
					top: _img_offset.top - _img_conrainer_offset.top,
					left: _img_offset.left - _img_conrainer_offset.left,
					width:$(this).width(),
					height:$(this).height(),
					border:'1px solid transparent'
				});
				var map_name = $(this).attr('usemap').replace('#','');
				if(map_name !=''){
					var index = 1;
					var _link_conrainer = _img_conrainer.find('.link-conrainer');
					var _position_conrainer = _img_conrainer.find('.position-conrainer');
					var image_param = $(this).attr('name') == '' ? '' : '['+ $(this).attr('name') + ']';
					container.find('map[name='+map_name+']').find('area[shape=rect]').each(function(){
						var coords = $(this).attr('coords').split(",");
						var href = $(this).attr('href');


						var width = coords[2]-coords[0];
						var height = coords[3]-coords[1];
						var top = coords[1];
						var left = coords[0];
						console.log("width："+width);
						console.log("height:"+height);
						console.log("top:"+top);
						console.log("left:"+left);
						console.log("href:"+href);

						//_link_conrainer.append('<p ref="'+index+'" class="map-link"><span class="link-number-text">热点 '+index+'</span>: <input type="text" size="60" name="link'+image_param+'[]" value="'+$(this).attr('href')+'" /><input type="hidden" class="rect-value" name="rect'+image_param+'[]" value="'+coords+'" /></p>');
						//coords = coords.split(',');
						_position_conrainer.append('<div ref="'+index+'" class="map-position" style="left:'+coords[0]+'px;top:'+coords[1]+'px;width:'+(coords[2]-coords[0])+'px;height:'+(coords[3]-coords[1])+'px;"><div class="map-position-bg"></div><span class="link-number-text" style="color:#fbed06;">热点 '+index+'</span><span class="delete">X</span><span class="resize"></span></div>');
						var text='<li mark='+index+' class="this"><a href="javascript:void(0);">热点'+index+'</a></li>';	 //<i onclick="cancleHotspot('+_count+');"></i>
						$("#hotspots").append(text);
						console.log("bind-data:"+$(this).attr('coords'));
						$("#hotspots li[mark=" + index + "]").data("bind-data",$(this).attr('coords')).data("bind-url", href);
						index++;
					});
				}
			});

		});



		$('#add_hotspot').click(function(){
			var img_id =$(".fiit_hot_img img").attr("id");
			 var spot_count=$("#hotspots li:last-child").attr("mark");
			   if(spot_count==null || spot_count==""|| spot_count==undefined){
				   spot_count=0;
			 }
			 spot_count++;

			 if(img_id==null || img_id==undefined || img_id==""){
				 parent.layer.alert("请先设置背景图片！",{icon: 0});
				 return false;
			 }

					if(spot_count==1){
					  $("#hotspots").html("");
					}
					var apply_mark =$("#apply_mark").val();
					if(apply_mark==""){
						$("#hotspots li").removeClass("this");
						var text='<li mark='+spot_count+' class="this"><a href="javascript:void(0);">热点'+spot_count+'</a></li>';
						$("#hotspots").append(text);
						var jsonObj=new Object();
						jsonObj.x1 = 10;
						jsonObj.y1 = 10;
					    jsonObj.x2 = 100;
					    jsonObj.y2 = 40;
					    var coor = 10+","+10 +","+100+","+40
					    $("#apply_mark").data("bind-data",coor);
						$("#apply_mark").val(spot_count);
						$("#url_count").html(spot_count);
						var _position_conrainer = $('.position-conrainer');
						_position_conrainer.append('<div ref="'+spot_count+'" class="map-position" style="left:10px;top:10px;width:90px;height:30px;"><div class="map-position-bg"></div><span class="link-number-text" style="color:#fbed06;">热点 '+spot_count+'</span><span class="delete">X</span><span class="resize"></span></div>');
						bind_map_event();
						define_css();
					}else{
						parent.layer.alert("请先设置热点信息！",{icon: 0});
					}
		});


		//绑定map事件
		function bind_map_event(){
			$('.position-conrainer .map-position .map-position-bg').each(function(){
				var map_position_bg = $(this);
				var conrainer = $(this).parent().parent();
				map_position_bg.unbind('mousedown').mousedown(function(event){
					map_position_bg.data('mousedown', true);
					map_position_bg.data('pageX', event.pageX);
					map_position_bg.data('pageY', event.pageY);
					map_position_bg.css('cursor','move');
					return false;
				}).unbind('mouseup').mouseup(function(event){
					map_position_bg.data('mousedown', false);
					map_position_bg.css('cursor','default');
					return false;
				});
				conrainer.unbind('mousedown').mousemove(function(event){
					if (!map_position_bg.data('mousedown')){
						//console.log(map_position_bg.data('mousedown'));
						return false;
					}
                    var dx = event.pageX - map_position_bg.data('pageX');
                    var dy = event.pageY - map_position_bg.data('pageY');
                    if ((dx == 0) && (dy == 0)){
                        return false;
                    }
					var map_position = map_position_bg.parent();
					var p = map_position.position();
					var left = p.left+dx;
					if(left <0) left = 0;
					var top = p.top+dy;
					if (top < 0) top = 0;
					var bottom = top + map_position.height();
					if(bottom > conrainer.height()){
						top = top-(bottom-conrainer.height());
					}
					var right = left + map_position.width();
					if(right > conrainer.width()){
						left = left-(right-conrainer.width());
					}
					map_position.css({
						left:left,
						top:top
					});
					map_position_bg.data('pageX', event.pageX);
					map_position_bg.data('pageY', event.pageY);

					bottom = top + map_position.height();
					right = left + map_position.width();

					var coor = left+","+top +","+right+","+bottom;

					console.log("bind-data="+coor);
					console.log("width="+map_position.width());
					console.log("height="+map_position.height());
					console.log("top="+top);
					console.log("left="+left);

					$("#apply_mark").data("bind-data",coor);
					$("#apply_mark").data("width",map_position.width());
					$("#apply_mark").data("height=",map_position.height());
					$("#apply_mark").data("top",top);
					$("#apply_mark").data("left",left);

					$("#hotspots li[mark=" + map_position.attr('ref') + "]").data("bind-data",coor);

					return false;
				}).mouseup(function(event){
					map_position_bg.data('mousedown', false);
					map_position_bg.css('cursor','default');
					return false;
				});
			});
			$('.position-conrainer .map-position .resize').each(function(){
				var map_position_resize = $(this);
				var conrainer = $(this).parent().parent();
				map_position_resize.unbind('mousedown').mousedown(function(event){
					map_position_resize.data('mousedown', true);
					map_position_resize.data('pageX', event.pageX);
					map_position_resize.data('pageY', event.pageY);
					return false;
				}).unbind('mouseup').mouseup(function(event){
					map_position_resize.data('mousedown', false);
					return false;
				});
				conrainer.mousemove(function(event){
					if (!map_position_resize.data('mousedown')) return false;
                    var dx = event.pageX - map_position_resize.data('pageX');
                    var dy = event.pageY - map_position_resize.data('pageY');
                    if ((dx == 0) && (dy == 0)){
                        return false;
                    }
					var map_position = map_position_resize.parent();
					var p = map_position.position();
					var left = p.left;
					var top = p.top;
					var height = map_position.height()+dy;
					if((top+height) > conrainer.height()){
						height = height-((top+height)-conrainer.height());
					}
					if (height <20) height = 20;
					var width = map_position.width()+dx;
					if((left+width) > conrainer.width()){
						width = width-((left+width)-conrainer.width());
					}
					if(width <50) width = 50;
					map_position.css({
						width:width,
						height:height
					});
					map_position_resize.data('pageX', event.pageX);
					map_position_resize.data('pageY', event.pageY);

					bottom = top + map_position.height();
					right = left + map_position.width();

				    var coor = left+","+top +","+right+","+bottom;


				    console.log("bind-data="+coor);
					console.log("width="+map_position.width());
					console.log("height="+map_position.height());
					console.log("top="+top);
					console.log("left="+left);

					$("#apply_mark").data("bind-data",coor);
					$("#apply_mark").data("width",map_position.width());
					$("#apply_mark").data("height=",map_position.height());
					$("#apply_mark").data("top",top);
					$("#apply_mark").data("left",left);
     				$("#hotspots li[mark=" + map_position.attr('ref') + "]").data("bind-data",coor);

					return false;
				}).mouseup(function(event){
					map_position_resize.data('mousedown', false);
					return false;
				}).click(function (e) {
          map_position_resize.data('mousedown', false);
          return false;
        });
			});


			$('.position-conrainer .map-position .delete').unbind('click').click(function(){
				var ref = $(this).parent().attr('ref');
				var _link_conrainer = $(this).parent().parent().parent().find('.link-conrainer');
				var _position_conrainer = $(this).parent().parent().parent().find('.position-conrainer');
				_link_conrainer.find('.map-link[ref='+ref+']').remove();
				_position_conrainer.find('.map-position[ref='+ref+']').remove();
				$("#apply_mark").val("");
				$("#hotspot_url").val("");
				$("#hotspots li[mark=" + ref + "]").remove();
			});
		}

		bind_map_event();

		function define_css(){
			//样式定义
			$container.find('.map-position').css({
				position:'absolute',
				border:'1px solid #000',
				'font-weight':'bold'
			});
			$container.find('.map-position .map-position-bg').css({
				position:'absolute',
				background:'#0F0',
				opacity:0.5,
				top:0,
				left:0,
				right:0,
				bottom:0,
				'z-index':10000
			});
			$container.find('.map-position .resize').css({
				display:'block',
				position:'absolute',
				right:0,
				bottom:0,
				width:5,
				height:5,
				cursor:'nw-resize',
				background:'#000',
				'z-index':10001
			});
			$container.find('.map-position .delete').css({
				display:'block',
				position:'absolute',
				right:0,
				top:0,
				width:10,
				height:12,
				'line-height':'11px',
				'font-size':12,
				'font-weight':'bold',
				background:'#000',
				color:'#fff',
				'font-family':'Arial',
				'padding-left':'2px',
				cursor:'pointer',
				opactiey : 1,
				'z-index':10001
			});
			$container.find('.map-position .choose').css({
				background: '#fff',
		        border: '1px solid #23a9db',
		        height: '80px',
		        opacity: 0.4
			});
		}
		define_css();
    };
})(jQuery);
