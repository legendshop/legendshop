$(function(){
	$(".button").bind("click",function(){
		var id=$(this).children("input[type='hidden']").val();
		var need=$(this).siblings("div[class='point']").children("p[class='required']").children("em").text();
		layer.confirm("确定要兑换吗？需要'"+need+"'积分", {
			icon: 3
			,btn: ['确定','关闭']
		}, function(){
			$.ajax({
				url:contextPath+"/p/coupon/receive/" + id,
				type:"post",
				dataType:'json',
				beforeSend: function(){
					layer.load(1);
				},
				async : false, //默认为true 异步   
				success:function(result){
					if(result=="OK"){
						layer.msg("恭喜您领取成功！",{icon: 1,time:1500},function(){
							window.location.reload();
						});
					}else{
						layer.msg(result,{icon: 0,time:1500},function(){
							window.location.reload();
						});
					}

				},
				error:function(e){
					window.location.href=contextPath+"/login";
				}
			});
		})
	});


	$("#coupon").bind("click",function(){
		var type="shop";
		window.location.href=contextPath+"/integral/integralCenter/list/"+type;

	});
	$("#redPack").bind("click",function(){
		var type="platform";
		window.location.href=contextPath+"/integral/integralCenter/list/"+type;
	});
});