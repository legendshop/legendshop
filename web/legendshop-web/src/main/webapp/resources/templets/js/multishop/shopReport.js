/** 加载 月份 选项 **/
function changeMonth(){
	var nowYear = new Date().getFullYear();
	var selectedYear = $("#selectedYear").val();
	var nowMonth = new Date().getMonth() + 1;
	var str ="";
	if( selectedYear != nowYear){
		for(var i=12;i>=1;i--){
			str += "<option value='"+i+"'>"+ i +"月</option>"
		}
	}else{
		for(var n=nowMonth;n>=1;n--){
			str += "<option value='"+n+"'>"+ n +"月</option>"
		}
	}
	
	$("#selectedMonth").html(str);
	getWeek();
}

/** 判断是否闰平年 **/
function IsPinYear(year) {                      
    return (0 == year % 4 && (year % 100 != 0 || year % 400 == 0));
}

/** 获取 当前月的最后一天 **/
function getLastDay(year,month){
	if(IsPinYear(year)){ //true : 润年，false :平年
		return LeapYear[month];
	}else{
	   return commonYear[month];
	}
}

/**方法，判断是否为空 **/
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}

/**获取时间周期**/
function getWeek(){
	var year = $("#selectedYear").val();
	var month = $("#selectedMonth").val();
	$("#selectedWeek").html("");
	jQuery.ajax({
		type:'POST',
		url: contextPath + '/s/shopReport/getWeek',
		data: {"year":year,"month":month},
		dataType : 'json',
		success:function(data){
		           var weekList = jQuery.parseJSON(data);
		         //  console.debug(weekList);
		           var option;
					for ( var i = 0; i < weekList.length; i++) {
						if(!isBlank(selectedWeek) && selectedWeek == weekList[i]){
							option = '<option selected value="'+weekList[i]+'">'+weekList[i]+'</option>';
						}else{
							option = '<option value="'+weekList[i]+'">'+weekList[i]+'</option>';
						}
						
						$("#selectedWeek").append(option);
					}
			}
	});
}

/** 下拉框 选项 加载 **/
function change(valueId){
	if(valueId == 1){//月
		var str = "<select name='selectedYear' id='selectedYear' onchange='changeMonth()' class='criteria-select sele item-sel'>";
		var str1 = "<select name='selectedMonth' id='selectedMonth' class='criteria-select sele item-sel'>";
		
		if(!isBlank(selectedYear)){
			for(var x=nowYear;x>=nowYear-5;x--){
				if(x == selectedYear){
					str+="<option value='"+x+"' selected>"+ x +"年</option>";
				}else{
					str+="<option value='"+x+"'>"+ x +"年</option>";
				}
			}
		}else{
			for(var x=nowYear;x>=nowYear-5;x--){
				str+="<option value='"+x+"'>"+ x +"年</option>";
			}
		}
		str += "</select>";
		$("#range").html(str);
		
		if(!isBlank(selectedMonth)){
			for(var y=12;y>=1;y--){
				if(y==selectedMonth){
					str1+= "<option value='"+y+"' selected>"+ y +"月</option>";
				}else{
					str1+= "<option value='"+y+"'>"+ y +"月</option>";
				}
			}
		}else{
			for(var y=nowMonth;y>=1;y--){
				str1+= "<option value='"+y+"'>"+ y +"月</option>";
			}
		}
		
		str1 += "</select>";
		$("#range").append(str1);
		
	}else if(valueId == 3){//周
		var str = "<select name='selectedYear' id='selectedYear' onchange='changeMonth()' class='criteria-select sele item-sel'>";
		var str1 = "<select name='selectedMonth' id='selectedMonth' onchange='getWeek()' style='margin-left: 5px;' class='criteria-select sele item-sel'>";
		var str2 = "<select name='selectedWeek' id='selectedWeek' class='sele item-sel'><option value='-1'>请选择</option></select>";
		
		if(!isBlank(selectedYear)){
			for(var x=nowYear;x>=nowYear-5;x--){
				if(x == selectedYear){
					str+="<option value='"+x+"' selected>"+ x +"年</option>";
				}else{
					str+="<option value='"+x+"'>"+ x +"年</option>";
				}
			}
		}else{
			for(var x=nowYear;x>=nowYear-5;x--){
				str+="<option value='"+x+"'>"+ x +"年</option>";
			}
		}
		str += "</select>";
		$("#range").html(str);
		
		if(!isBlank(selectedMonth)){
			for(var y=12;y>=1;y--){
				if(y==selectedMonth){
					str1+= "<option value='"+y+"' selected>"+ y +"月</option>";
				}else{
					str1+= "<option value='"+y+"'>"+ y +"月</option>";
				}
			}
		}else{
			for(var y=nowMonth;y>=1;y--){
				str1+= "<option value='"+y+"'>"+ y +"月</option>";
			}
		}
		
		str1 += "</select>";
		$("#range").append(str1);
		
		$("#range").append(str2);	
		
		getWeek();
	}else{//天
		var str = '<input readonly="readonly"  name="selectedDate" id="selectedDate" class="item-inp"  type="text" value="'+selectedDay+'" />';
		$("#range").html(str);
		laydate.render({
			   elem: '#selectedDate',
			   calendar: true,
			   theme: 'grid',
			   trigger: 'click'
		  });
	}
}