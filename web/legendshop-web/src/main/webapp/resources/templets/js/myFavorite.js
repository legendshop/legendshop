$(document).ready(function() {
	mayFav.bindPageAction();
	userCenter.changeSubTab("myfavourite");
});

function pager(curPageNO){
	userCenter.loadPageByAjax("/p/favourite?curPageNO=" + curPageNO);
}

function favoriteShop(){
	window.location.href =contextPath + "/p/favoriteShop";
}

function delFavById(favId,obj){
	layer.confirm("您确定要删除该收藏信息吗？", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(index, layero){
		var jsonData = {
				"selectedFavs" : favId
		};
		var url = contextPath + "/p/delfavourite";
		$.post(url, jsonData, function(data) {
			$(obj).parent().parent().remove(); 
		});
		layer.close(index);
	});
}

//全选
$(".selectAll").click(function(){
    
	if($(this).attr("checked")=="checked"){
		$(this).parent().parent().addClass("checkbox-wrapper-checked");
         $(".selectOne").each(function(){
        		 $(this).attr("checked",true);
        		 $(this).parent().parent().addClass("checkbox-wrapper-checked");
         });
     }else{
         $(".selectOne").each(function(){
             $(this).attr("checked",false);
             $(this).parent().parent().removeClass("checkbox-wrapper-checked");
         });
         $(this).parent().parent().removeClass("checkbox-wrapper-checked");
     }
 });

function selectOne(obj){
	if(!obj.checked){
		$(".selectAll").checked = obj.checked;
		$(obj).prop("checked",false);
		$(obj).parent().parent().removeClass("checkbox-wrapper-checked");
	}else{
		$(obj).prop("checked",true);
		$(obj).parent().parent().addClass("checkbox-wrapper-checked");
	}
	 var flag = true;
	  var arr = $(".selectOne");
	  for(var i=0;i<arr.length;i++){
	     if(!arr[i].checked){
	    	 flag=false; 
	    	 break;
	    	 }
	  }
	 if(flag){
		 $(".selectAll").prop("checked",true);
		 $(".selectAll").parent().parent().addClass("checkbox-wrapper-checked");
	 }else{
		 $(".selectAll").prop("checked",false);
		 $(".selectAll").parent().parent().removeClass("checkbox-wrapper-checked");
	 }
}
