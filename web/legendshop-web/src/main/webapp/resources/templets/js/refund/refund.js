$(function(){
	userCenter.changeSubTab("myreturnorder");//高亮菜单
});

var fileCount = 1;
function addFile(currBtn){
	var $currBtn = $(currBtn);
	var form = $currBtn.parents("form");
	if(fileCount >= 3){
		layer.msg("对不起,最多只允许上传三个凭证!");
		return;
	}
	var file = form.find("#photoFile"+fileCount);
	if(!file.val()){
		layer.msg("对不起,请先选择第"+ fileCount +"个凭证!");
		return;
	}
	
	form.find("#removeBtn").remove();
	$currBtn.remove();
	
	var num = fileCount + 1;
	var li = $("<li></li>");
	var span = $("<span></span>");
	var fileInput = $("<input type='file'/>");
	var addBtn = $("<input id='addBtn' class='btn-g' style='margin-right:5px;' type='button' value='添加' onclick='addFile(this)'/>");
	var rmBtn = $("<input id='removeBtn' class='btn-g' type='button' value='移除' onclick='removeFile(this);'/>");
	span.css("line-height","20px");
	span.text("上传凭证"+ num + "：");
	fileInput.attr("class","photoFile");
	fileInput.attr("id","photoFile" + num);
	fileInput.attr("name","photoFile" + num);
	//fileInput.attr("accept","image/*");
	fileInput.attr("onchange","checkImgFile(this);");
	li.append(span);
	li.append(fileInput);
	li.append(addBtn);
	li.append(rmBtn);
	form.find("#formContent").append(li);
	
	fileCount++;
}

function removeFile(self){
	var _self = $(self);
	var li = _self.parent("li");
	var prevLi = li.prev();
	var addBtn = $("<input id='addBtn' class='btn-g' type='button' value='添加' onclick='addFile(this)'/>");
	if(fileCount > 2){
		_self.one("click",function(){
			removeFile(this);
		});
		prevLi.append(addBtn);
		prevLi.append(_self);
	}else{
		prevLi.append(addBtn);
	}
	li.remove();
	fileCount--;
}

//校验用户选择的图片
function checkImgFile(obj){
	if(!obj.value){
		return;
	}
    var file = obj.files[0];
    if(!file.name.match(/.*\.(jpg|png|bmp|gif|jpeg)/i)){
    	layer.alert("对不起,只支持图片凭证!", {icon:2});
        obj.value = "";
        return false;
    }
    if(file.size <= 0){
    	layer.alert("您选择的文件大小为0,请选择有效的文件!", {icon:2});
        obj.value = "";
        return false;
    }
    if(file.size > (1024*1024*5)){
    	layer.alert("您选择的文件大于5M,请重新选择!", {icon:0});
    	obj.value = "";
    	return false;
    }
    return true;
}

//定义退款,退货表单校验的公共规则
var commonRules = {
		orderId : {
			required: true
		},
		orderItemId: {
			required: true
		},
		buyerMessage: {
			required: true
		},
		refundAmount: {
			required: true,
			isNumber: true,
			isValidMoney: true,
			checkRefundMoney : true
		},
		reasonInfo: {
			required: true,
			maxlength: 300
		},
		photoFile1: {
			required: true
		}
	};