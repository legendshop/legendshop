			function checkName(){
				var regex = /^\w+$/;
				var canUse = false;
				var name = $("#shopUserName").val().trim();
				var srcval = $("#shopUserName").attr("srcval");
				$("#shopUserName").val(name);
				if(name==""){
					$("#shopUserName").parent().find(".err-msg").html("请输入账号");
					$("#shopUserName").parent().find(".err-msg").show();
				}else if(name.length<2){
					$("#shopUserName").parent().find(".err-msg").html("账号不能少于2位");
					$("#shopUserName").parent().find(".err-msg").show();
				}else if(name.length>20){
					$("#shopUserName").parent().find(".err-msg").html("账号不能多于20位");
					$("#shopUserName").parent().find(".err-msg").show();
				}else if(!regex.test(name)){
					$("#shopUserName").parent().find(".err-msg").html("账号只能由数字、字母或者下划线组成");
					$("#shopUserName").parent().find(".err-msg").show();
				}else{
					if(srcval!=name){
					 jQuery.ajax({
							url:contextPath + "/s/shopUsers/isNameExist",
							data:{"name":name},
							type:'get',
							async : false, //默认为true 异步
							dataType : 'json',
							success:function(result){
								if(result){
									$("#shopUserName").parent().find(".err-msg").html("该账号已被使用");
									$("#shopUserName").parent().find(".err-msg").show();
								}else{
									canUse = true;
								}
							}
					 });
					}else{
						canUse = true;
					}
				}
				return canUse;
			}

			function checkPasswd(){
				var password = $("#password").val();
				/*var pwdformat = /(?!.*[\u4E00-\u9FA5\s])(?!^[a-zA-Z]+$)(?!^[\d]+$)(?!^[^a-zA-Z\d]+$)^.{6,20}$/;*/
				var regx1= /^[0-9A-z`~!@#$%^&*()_\-+=<>?:"{}|,.\/;'\\[\]·~！@#￥%……&*（）——\-+={}|《》？：“”【】、；‘’，。、]+$/g;
			    var regx= /(?!^[0-9]+$)(?!^[A-z]+$)(?!^[~\\`!@#$%\\^&*\\(\\)-_+={}|\\[\\];':\\\",\\.\\\\\/\\?]+$)(?!^[^A-z0-9]+$)^.{6,20}$/;
			    if(password==""){
			    	$("#password").parent().find(".err-msg").html("密码不能为空");
					$("#password").parent().find(".err-msg").show();
					return false;
			    }
			    if(password.indexOf(" ") > -1){
			    	$("#password").parent().find(".err-msg").html("密码不能包含空格");
					$("#password").parent().find(".err-msg").show();
					return false;
			    }
			    if (password.length < 6 || password.length > 20) {
			    	$("#password").parent().find(".err-msg").html("为了您的账号安全，密码长度在 6-20 个字符之间");
					$("#password").parent().find(".err-msg").show();
					return false;
			    }
				if(!regx1.test(password) || !regx.test(password)){
					$("#password").parent().find(".err-msg").html("密码由6-20位字母、数字或符号的两种及以上组成");
					$("#password").parent().find(".err-msg").show();
					return false;
				}
				return true;
			}

			function checkPasswd2(){
				var password = $("#password").val();
				var password2 = $("#password2").val();
				if(password2==""){
					$("#password2").parent().find(".err-msg").html("确认密码不能为空");
					$("#password2").parent().find(".err-msg").show();
					return false;
				}
				if(password!=password2){
					$("#password2").parent().find(".err-msg").html("两次输入的密码不一致");
					$("#password2").parent().find(".err-msg").show();
					return false;
				}
				return true;
			}

			function checkRealName(){
				var realName = $("#realName").val().trim();
				$("#realName").val(realName);
				if(realName==""){
					$("#realName").parent().find(".err-msg").html("员工姓名不能为空");
					$("#realName").parent().find(".err-msg").show();
					return false;
				}
				return true;
			}

			function checkShopJob(){
				var shopRoleId = $("#shopRoleId").val();
				if(shopRoleId==null || shopRoleId==""){
					layer.msg("请先添加职位",{icon: 0});
					return false;
				}
				return true;
			}

			function clearErr(ths){
				$(ths).parent().find(".err-msg").hide();
			}

			function saveUser(){
				var errCount = 0;
				var passwd = $("#password").val();
				var passwd2 = $("#password2").val();
				if(uType=="" || uType==null || uType==2){ //新增员工，密码必填
					if(!checkName()){
						errCount++;
					}
					if(!checkPasswd()){
						errCount++;
					}
					if(!checkPasswd2()){
						errCount++;
					}
					if(!checkRealName()){
						errCount++;
					}
					if(!checkShopJob()){
						errCount++;
					}
					if(errCount==0){
						$("#from1").submit();
						layer.msg('添加成功！', {icon: 1},function(){
	                		window.location.reload();
	                	});
					}
				} else {
					if(!checkName()){  //不修改密码
						errCount++;
					}
					if(!checkRealName()){
						errCount++;
					}
					if(!checkShopJob()){
						errCount++;
					}
					if(errCount==0){
						$("#from1").submit();
						layer.msg('添加成功！', {icon: 1},function(){
	                		window.location.reload();
	                	});
					}
				}
			}

			function closeDialog(){
				var index = parent.layer.getFrameIndex('shopUser'); //先得到当前iframe层的索引
				parent.layer.close(index);
			}
