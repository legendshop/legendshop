jQuery(document).ready(function($) {
		$('#aView').jqzoom({
            zoomType: 'standard',
            lens:true,
            preloadImages: false,
            alwaysOn:false,
            zoomWidth:450,
            zoomHeight:450
        });
        $('.zoomPad').css('z-index','auto');

        //call ajax
		$.post(contextPath+"/integral/exchangeViewList", {"firstCid":firstCid,"twoCid":twoCid,"thirdCid":thirdCid},function(retData) {
			$("#exchangeList").html(retData);
		},'html');

    });

//增加购买数量
function addPamount() {
	var pamount =Number($("#pamount").val());
	var re = /^[1-9]+[0-9]*]*$/;
	if (isNaN(pamount) || !re.test(pamount)) {
		return ;
	}
    if(limitNum!=0&& pamount>=Number(limitNum)){ //是否超过购买限制
		return;
	}
    else if(pamount>=Number(prodStock)){
    	return;
    }
	$("#pamount").val(pamount* 1 + 1);
}


//减少购买数量
function reducePamount() {
	var pamount = $("#pamount").val();
	var re = /^[1-9]+[0-9]*]*$/;
	if (isNaN(pamount) || !re.test(pamount)) {
		return ;
	}
	if (pamount <= 1) {
		$("#pamount").val(1);
	} else {
		$("#pamount").val(pamount * 1 - 1);
	}
}

//得判断是否为数字的同时，也判断其是不是正数
function validateInput() {
	var pamount = $("#pamount").val();
	var re = /^[1-9]+[0-9]*]*$/;
	if (isNaN(pamount) || !re.test(pamount)) {
		//错误时
		$("#pamount").attr("style", "border: 1px solid #f40000;");
		layer.msg("请输入正确的数量！",{icon: 0});
		return false;
	}
	if(limitNum!=0&& pamount>Number(limitNum)){ //是否超过购买限制
		$("#pamount").attr("style", "border: 1px solid #f40000;");
		layer.msg("超过购买限制！",{icon: 0});
		return false;
	}
	else if(pamount>Number(prodStock)){
		$("#pamount").attr("style", "border: 1px solid #f40000;");
		layer.msg("超过库存限制！",{icon: 0});
		return false;
	}
	else {
		//正确时
		$("#pamount").attr("style", "");
		return true;
	}

}


//得判断是否为数字的同时，也判断其是不是正数
function isLogin() {
	if(loginUserName=="" || loginUserName==undefined || loginUserName==null){
		return true;
	}
	return false;
}

/**
 * 登录
 */
function login(){
	layer.open({
		title :"登录",
		  type: 2,
		  content: contextPath + '/loadLoginOverlay', //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['440px', '530px']
		});
}



//立即购买
function buyNow() {
	if(!validateInput()){
	   return;
	}
	if(isLogin()){
		login();
		return false;
	}
	//判断是否达到限购数量
	if(limitNum!=0){
		$.ajax({
			url : contextPath + "/integral/checkIntegralItemCount",
			data : {"prodId":prodId},
			type : 'post',
			dataType : 'text',
			async : false, //默认为true 异步
			success : function(retData) {
				if(retData != "OK" && Number(retData) >= Number(limitNum)){
					layer.alert("您已购买过"+retData+"件此商品，超过购买限制",{icon: 0});
					return false;
				}
			}
		});
	}
	var pamount = $("#pamount").val();
	jQuery.ajax({
		url : contextPath + "/integral/cart/addBuy/"+prodId,
		data : {"count" : pamount},
		type : 'post',
		async : false, //默认为true 异步
		dataType : 'json',
		success : function(retData) {
			 if(retData.result=="false"){
				var code=retData.code;
				var message=retData.message;
				if(!isBlank(code)){
					if("LOGIN"==code){
						login();
					}else if("ORDER_ADDRESS"==code){
						layer.alert("请设置您的收货地址？",{icon: 0},function(){
							 // 异步处理前
							 var tab = window.open('about:blank');
							 tab.location = contextPath+"/p/receiveaddress";
						})
					}
				}else{
					layer.alert(message,{icon: 0});
				}
			}else if(retData.result=="true"){
				var score=Number($.trim($("#exchangeIntegral").html()));
				sumitForm(prodId,pamount,score);
			}
		}
	});
}

/**
 * 提交表单
 * @param prodId
 * @param count
 * @param score
 */
function sumitForm(prodId,count,score){
	layer.open({
		title :"积分兑换",
		  type: 2,
		  content: contextPath + '/p/integral/cart/'+prodId+"?count="+count+"&score="+score, //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['650px', '400px']
		});
}


/**
 * 判断是否为空
 * @param _value
 * @returns {Boolean}
 */
function isBlank(_value){
	   if(_value==null || _value=="" || _value==undefined){
	     return true;
	   }
	   return false;
}
