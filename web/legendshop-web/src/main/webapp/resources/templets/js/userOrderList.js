function search(){
	$("#order_search #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	$("#order_search")[0].submit();
}

//提醒发货
function SendSiteMsg(shopId,subNum){
	$.ajax({
		url : contextPath+"/p/sendShipMsg",
		type : 'post',
		data : {"shopId":shopId,"subNumber":subNum},
		success : function(result){
			layer.msg(result,{icon: 1,time:1000},function(){
				location.reload()
			});
		}
	});
}

// 跳转订单退款页面
function refundApply(subId) {
  //location.href = path+"/p/refund/apply/"+subId;
  // 判断订单是否为团长免单订单
  $.ajax({
    url : contextPath+"/p/isMergeGroupHeadFreeSub",
    type : 'post',
    data : {"subId":subId},
    dataType:'json',
    success : function(result){

      if(result == 'fail'){

        layer.msg("该拼团订单为团长免单订单哦,不能发起退款申请，您支付的款项拼团成功过后会自动退还",{icon: 0});
        return;
      }else {
        location.href = path+"/p/refund/apply/"+subId;
      }

    }
  });

}

// 跳转订单售后选择页面
function returnApply(subId,subItemId,subNum) {
  //location.href = contextPath+"/p/return/apply/"+subId+"/"+subItemId+"?subNum="+subNum;
  // 判断订单是否为团长免单订单
  $.ajax({
    url : contextPath+"/p/isMergeGroupHeadFreeSub",
    type : 'post',
    data : {"subId":subId},
    dataType:'json',
    success : function(result){

      if(result == 'fail'){

        layer.msg("该拼团订单为团长免单订单哦,不能发起退款申请，您支付的款项拼团成功过后会自动退还",{icon: 0});
        return;
      }else {
        location.href = contextPath+"/p/return/apply/"+subId+"/"+subItemId+"?subNum="+subNum;
      }

    }
  });

}

//再来一单
function addAnotherSub2(subNumber){
	data = {"subNumber":subNumber};
	$.ajax({
		"url":contextPath+"/p/addAnotherSub2",
		type:'post',
		dataType:'json',
		data:data,
		async : false,
		success:function(result){
			if(result.length>0){
				abstractCartItemsForm(path+'/p/orderDetails', result);
			}
			if(result.status==0){
				layer.alert("下单失败,请稍后重试!", {icon:2});
				return "";
			}
		}
	});
}

//构建form
function abstractCartItemsForm(URL, chains){
	var temp = document.createElement("form");
	temp.action = URL;
	temp.method = "post";
	temp.style.display = "none";
	var opt = document.createElement("input");
	opt.name = 'shopCartItems';
	opt.value = chains;
	temp.appendChild(opt);
	temp.appendChild(appendCsrfInput());
	document.body.appendChild(temp);
	temp.submit();
	return temp;
}
