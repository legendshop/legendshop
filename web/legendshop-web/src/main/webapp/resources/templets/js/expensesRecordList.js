$(document).ready(function() {
	userCenter.changeSubTab("expensesRecord");
});


function pager(curPageNO){
	userCenter.loadPageByAjax("/p/expensesRecord?curPageNO=" + curPageNO);
}

function deleteById(id){
	layer.confirm("确定删除这条记录吗?", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		$.ajax({
			url:contextPath+"/p/expensesRecord/delete/"+id, 
			type:'post', 
			async : true, //默认为true 异步   
			success:function(result){
				layer.msg('删除记录成功', {icon:1,time:800}, function(){
					window.location=contextPath+"/p/expensesRecord";
				});
			}
		}); 
	});
}

//页面的删除
function deleteAction(){
	//获取选择的记录集合
	selAry = $(".selectOne");
	if(!checkSelect(selAry)){
		layer.alert('删除时至少选中一条记录！',{icon: 0});
		return false;
	}
	layer.confirm("删除后不可恢复, 确定要删除吗？", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		var ids = []; 
		for(i=0;i<selAry.length;i++){
			if(selAry[i].checked){
				ids.push(selAry[i].value);
			}
		}
		var result = deleteExpensesRecord(ids);
		if('OK' == result){
			layer.msg('删除记录成功', {icon:1});
			$("#expensesRecord").click();
		}else{
			layer.msg('删除记录失败', {icon:2});
		}

	});
	return true;
}


function deleteExpensesRecord(ids) {
	var result;
	var idsJson = JSON.stringify(ids);
	var ids = {"ids": idsJson};
	var url = contextPath+"/p/deleteExpensesRecord";
	jQuery.ajax({
		"url":url , 
		data: ids,
		type:'post', 
		async : false, //默认为true 异步   
		dataType : 'json', 
		success:function(retData){
			result = retData;
		}
	});
	return result;
}


//清空
function clearAction(){

	layer.confirm("清空后不可恢复, 确定要清空吗？", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		var allSel = $(".selectOne");
		var ids = [];
		for(i=0;i<allSel.length;i++){
			ids.push(allSel[i].value);
		}
		var result = deleteExpensesRecord(ids);
		if('OK' == result){
			layer.msg('清空记录成功',{icon: 1});
			$("#expensesRecord").click();
		}else{
			ayer.msg('清空记录失败',{icon: 2});
		}
	});
}