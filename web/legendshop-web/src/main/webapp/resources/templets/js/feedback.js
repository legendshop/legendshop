$().ready(function(){
		$(".ls-toolbar-tab").hover(function(e){
			$(this).addClass("z-ls-tbar-tab-hover");
		},function(e){
			$(this).removeClass("z-ls-tbar-tab-hover");
			});
		$(window).scroll(function(){
			var top = jQuery(document).scrollTop();
				if(top==0){
					$("#top").hide();
				}else{
					$("#top").show();
				}
				return false;
		});
		});

    //反馈页面
	function feedback(){
		layer.open({
			  type: 1,
			  title: "意见反馈",
			  content: '<div style="padding: 20px 25px;">反馈内容<font color="red">(*必填)</font>：<br/><textarea rows="10" cols="42" placeholder="欢迎提出您在使用过程中遇到的问题或宝贵建议（200字以内），感谢您对的支持。" maxlength="200" id="text"></textarea><br/>'
	    	    	+'联系方式(电话、手机或者邮箱)：<br/><input type="text" id="mobile" maxlength="50" placeholder="请留下您的联系方式（50字内）" size="40"/></div>'
	    	 ,btn: ['提交反馈','关闭'] //按钮
			,yes: function(index, layero){
				var content=$("#text").val();
			 	var mobile=$("#mobile").val();
			 	if(content==null||content==''||content==undefined){
			 		layer.msg("反馈内容不能为空",{ icon: 0});
			 		return false;
			 	}
			 	if(content.length>200){
			 		layer.msg("反馈内容不能多于200",{ icon: 0});
			 		return false;
			 	}
			 	if(mobile==null||mobile==''||mobile==undefined){
			 		layer.msg("联系方式不能为空",{icon: 0});
			 		return false;
			 	}
			 	if(!validContact(mobile)){
			 		layer.msg("请输入正确的联系方式",{icon: 0});
			 		return false;
			 	}
			 	$.ajax({
					url: contextPath+"/userFeedBack/save/",
					data:{"content":content,"mobile":$("#mobile").val()},
					type:'get',
					async : true, //默认为true 异步
					dataType:'json',
					success:function(data){
						if(data=="OK"){
							layer.msg("提交成功",{ icon: 1});
							layer.close(index);
						}else {
							layer.msg(data,{icon: 2});
						}
					}
				});

			  }
		}
		);
    }

	//校验联系方式
	function validContact(_temp) {
		var flag = false;
		//区号+座机号码+分机号码
		var phoneRegexp = /^(?:0[1-9][0-9]{1,2}-)?[2-8][0-9]{6,7}$/;

		//手机号正则
		var mobilePhoneRegexp= /^[1][0-9]{10}$/;

		//邮件正则
		var email = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;

		if( phoneRegexp.test(_temp) || mobilePhoneRegexp.test(_temp) || email.test(_temp)) {
			flag = true;
		}
		return flag;
	}

	function gotop(){
		$('body,html').animate({scrollTop:0},1000);
		$("#top").hide();
	}
