$(document).ready(function() {
	userCenter.changeSubTab("productImport");
	$("#btn_keyword1").unbind("click");
	$("#btn_keyword1").bind("click", function() {
		window.location.href = contextPath+"/s/prod/import/downloadtemp"
	});
	$("#btn_keyword2").unbind("click");
	$("#btn_keyword2").bind("click", function() {
		var page1 = contextPath+"/s/prod/load";
		open(page1);
	});

	$("#btn_keyword3").unbind("click");
	$("#btn_keyword3").bind("click", function() {
		var page1 = contextPath+"/s/prod/load1";
		open(page1);
	});
});

$("#btn_keyword4").unbind("click");
$("#btn_keyword4").bind("click", function() {
	var reg = new RegExp("^[0-9]*$");
	var ids = [];
	var checked = $("input:checked");
	if (checked.length > 0) {

		 layer.confirm("删除后不可恢复, 确定要删除吗？", {
	   		 icon: 3
	   	     ,btn: ['确定','关闭'] //按钮
	   	   }, function(){
	   		for (var i = 0; i < checked.length; i++) {
				var id = $(checked[i]).attr("value");
				if (reg.test(id)) {
					ids.push(id);
				}
			}
			var jsonID = JSON.stringify(ids);
			if (jsonID != null) {
				$.ajax({
					url : contextPath +"/s/prod/import/delete",
					type : "post",
					data : {
						"jsonID" : jsonID
					},
					dataType : 'json',
					async : false, // 默认为true 异步
					success : function(result) {
						if ("success" == result) {
							layer.msg("删除成功",{icon: 1,time:700},function(){
								window.location.reload();
							});

						} else {
							layer.msg(result);
						}
					}
				});
			}
	   	   });
	} else {
		layer.msg("没有要删除的内容",{icon: 0});
	}
});

function open(page1) {
	var page = page1;
	layer.open({
		title :"导入商品",
		id:"uploadImport",
		  type: 2,
		  content: page, //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['500px', '200px']
		});
}

function publishProd(impId) {
	window.location = contextPath + "/s/publish?impId=" + impId;
}
function search() {
	$("#from1 #curPageNO").val("1");// 只要是搜索,都是从第一页开始查
	$("#from1")[0].submit();
}

function pager(curPageNO) {
	document.getElementById("curPageNO").value = curPageNO;
	document.getElementById("from1").submit();
}
