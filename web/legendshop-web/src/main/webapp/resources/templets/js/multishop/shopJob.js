
$("input[type=checkbox]").change(
		function(){
			var chk = $(this).is(':checked');
			var type = $(this).attr("name");
			if(type=="menuGroupLabels"){

				//勾选自己
				$(this).parent().parent().parent().next().find("input").attr("checked",chk);
				if(chk){

					//选中所有下级
					$(this).parent().parent().addClass("checkbox-wrapper-checked");
					$(this).parent().parent().parent().next().find("label").addClass("checkbox-wrapper-checked");
				}else{

					//取消选中所有下级
					$(this).parent().parent().removeClass("checkbox-wrapper-checked");
					$(this).parent().parent().parent().next().find("label").removeClass("checkbox-wrapper-checked");
				}
			}else if(type == "menuLabels"){
				if(chk){

					//选中上一级
					$(this).parent().parent().parent().parent().prev().find("label").addClass("checkbox-wrapper-checked");
					$(this).parent().parent().parent().parent().prev().find("input").attr("checked",true);

					//选中自己
					$(this).parent().parent().addClass("checkbox-wrapper-checked");

					//选中下一级
					$(this).parent().parent().parent().next().find("label").addClass("checkbox-wrapper-checked");
					$(this).parent().parent().parent().next().find("input").attr("checked",true);

				}else{

					//取消选中所有下级
					$(this).parent().parent().parent().next().find("label").removeClass("checkbox-wrapper-checked");
					$(this).parent().parent().parent().next().find("input").attr("checked",false);


					var len = $(this).parent().parent().parent().parent().find("input:checked").get().length;
					$(this).parent().parent().removeClass("checkbox-wrapper-checked");
					if(len==0){
						$(this).parent().parent().parent().parent().prev().find("input").attr("checked",false);
						$(this).parent().parent().parent().parent().prev().find("label").removeClass("checkbox-wrapper-checked");
					}
				}
			}else{

				var funcType = $(this).attr("data-type");
				if (chk) {

					//反选第一级
					$(this).parent().parent().parent().parent().parent().prev().find("label").addClass("checkbox-wrapper-checked");
					$(this).parent().parent().parent().parent().parent().prev().find("input").attr("checked",true);

					//反选第二级
					$(this).parent().parent().parent().parent().prev().find("label").addClass("checkbox-wrapper-checked");
					$(this).parent().parent().parent().parent().prev().find("input").attr("checked",true);

					$(this).parent().parent().addClass("checkbox-wrapper-checked");

					//如果选择是编辑权限，默认勾选查看
					if (funcType == 'editor') {

						$(this).parent().parent().parent().prev().find("label").addClass("checkbox-wrapper-checked");
						$(this).parent().parent().parent().parent().prev().find("input").attr("checked",true);
					}


				}else{

					var len = $(this).parent().parent().parent().parent().find("input:checked").get().length;
					$(this).parent().parent().removeClass("checkbox-wrapper-checked");
					if(len==0){// 当前级如果没有其他勾选，则取消上一级的勾选

						$(this).parent().parent().parent().parent().prev().find("input").attr("checked",false);
						$(this).parent().parent().parent().parent().prev().find("label").removeClass("checkbox-wrapper-checked");
					}

					//再次判断上一级，如果没有其他勾选，则取消顶级勾选
					var len2 = $(this).parent().parent().parent().parent().prev().parent().find("input:checked").get().length;
					if (len2==0) {
						$(this).parent().parent().parent().parent().parent().prev().find("label").removeClass("checkbox-wrapper-checked");
						$(this).parent().parent().parent().parent().parent().prev().find("input").attr("checked",false);
					}

					//如果选择是编辑权限，默认勾选查看
					if (funcType == 'editor') {

						$(this).parent().parent().parent().prev().find("label").removeClass("checkbox-wrapper-checked");
						$(this).parent().parent().parent().parent().prev().find("input").attr("checked",false);
					}
				}
			}
		}
);

function checkName(){
	var canUse = false;
	var name = $("#jobName").val().trim();
	var srcval = $("#jobName").attr("srcval");
	if(name==""){
		$(".err-msg").html("请输入职位名称");
		$(".err-msg").show();
	}else{
		if(srcval!=name){
			jQuery.ajax({
				url:contextPath + "/s/shopJobs/isNameExist",
				data:{"name":name},
				type:'get',
				async : false, //默认为true 异步
				dataType : 'json',
				success:function(result){
					if(result){
						$(".err-msg").html("该职位已存在");
						$(".err-msg").show();
					}else{
						canUse = true;
					}
				}
			});
		}else{
			canUse = true;
		}
	}
	return canUse;
}

function clearErr(){
	$(".err-msg").hide();
}

function saveJob(){
	if(checkName()){
		var len = $("input[name=menuLabels]:checked").get().length;
		if(len==0){
			layer.msg('请设置职位权限！', {icon: 0});
		}else{
			$("#from1").submit();
			layer.msg('编辑成功', {icon: 1,time:700},function(){
				window.location.reload();
			});
		}
	}
}

function closeDialog(){
	var index = parent.layer.getFrameIndex('editJob'); /* 先得到当前iframe层的索引 */
	parent.layer.close(index);
}
