var urls = {
    productCommentDetail: contextPath + "/productcomment/productCommentDetail",
	updateUsefulCounts: contextPath + "/productcomment/updateUsefulCounts",
	replyComment: contextPath + "/productcomment/replyComment",
	replyParent: contextPath + "/productcomment/replyParent",
};

$(function(){

	$("#agree").on("click", function(){

		if(!isLogin()){
			showLogin();
			return false;
		}

		var $this = $(this);
		var usefulCountSpan = $this.find(".num");
		var usefulCount = parseInt(usefulCountSpan.text());

		var prodComId = $this.attr("prodCommId");

		var url = urls.updateUsefulCounts;
		var params = {"prodComId": prodComId};
		$.ajaxUtils.sendPost(url, params, function(result){
			if(result === "OK"){
				usefulCount++;
				usefulCountSpan.text(usefulCount);
				layer.msg("恭喜您, 点赞成功!",{icon: 1});
			}else if(result === "LOGIN"){
				showLogin();
			}else{
				layer.msg(result,{icon: 2});
			}
		});
		return false;
	});

	/** 回复评价 */
	$("#replyCommentBtn").on("click", function(){

		if(!isLogin()){
			showLogin();
			return false;
		}

		var $this = $(this);
		var prodComId = $this.attr("prodComId");
		var replyContent = $this.parent().prev(".inner").find(".reply-input").val();

		if(!$.trim(replyContent)){
			layer.alert("亲, 回复内容不能为空哦!",{icon: 0});
			return false;
		}

		if(replyContent.length > 250){
			layer.alert("亲, 回复内容不能超过250个字符哦!",{icon: 0});
			return false;
		}

		var checkResult = checkSensitiveData(replyContent);
		if(checkResult && checkResult.length){
			layer.alert("亲, 您输入的内容中包含敏感字 " + checkResult + " !",{icon: 0});
			return false;
		}

		var url = urls.replyComment;
		var params = {
			"prodComId": prodComId,
			"replyContent": replyContent,
		};

		$.ajaxUtils.sendPost(url, params, function(result){
			var status = result.status;
			if(status === 1){
				var productReply = result.data;
				$(".reply-items").prepend(buildReplyCommentItem(productReply)[0].outerHTML);
				var numSpan = $("#reply").find(".num");
				var replyCount = parseInt(numSpan.text());
				replyCount++;
				numSpan.text(replyCount);

				$this.parent().prev(".inner").find(".reply-input").val("");
				layer.msg("恭喜您, 回复成功!",{icon: 1});
			}else if(status === 0){
				showLogin();
			}else{
				layer.msg(result.msg,{icon: 2});
			}
		});

		return false;
	});

	/** 隐藏或显示回复输入框 */
	$(".reply-items").on("click", ".a_reply", function(){
		var $this = $(this);
		var replyTextarea = $this.parent().next(".reply-textarea");
		if(replyTextarea.length){//如果有
			if(replyTextarea.is(":visible")){//如果是显示状态
				replyTextarea.hide();
			}else{
				replyTextarea.find(".reply-input").val("");
				replyTextarea.show();
			}
		}else{
			var parentId = $this.attr("parentId");
			var parentUserName = $this.attr("parentUserName");
			replyTextarea = buildReplyInput(parentId, parentUserName);

			$this.parent().parent().append(replyTextarea.show());
		}
		return false;
	});

	/** 回复其他人 */
	$(".reply-items").on("click", ".reply-submit", function(){
		var $this = $(this);
		var parentId = $this.attr("parentId");
		var replyContent = $this.parent().prev(".inner").find(".reply-input").val();

		if(!$.trim(replyContent)){
			layer.alert("亲, 回复内容不能为空哦!",{icon: 0});
			return false;
		}

		if(replyContent.length > 250){
			layer.alert("亲, 回复内容不能超过250个字符哦!",{icon: 0});
			return false;
		}

		var checkResult = checkSensitiveData(replyContent);
		if(checkResult && checkResult.length){
			layer.alert("亲, 您输入的内容中包含敏感字 " + checkResult + " !",{icon: 0});
			return false;
		}

		var url = urls.replyParent;
		var params = {
			"parentReplyId": parentId,
			"replyContent": replyContent,
			"prodId": prodId,
		};

		$.ajaxUtils.sendPost(url, params, function(result){
			var status = result.status;
			if(status === 1){
				var productReply = result.data;
				$(".reply-items").prepend(buildReplyParentItem(productReply)[0].outerHTML);
				var numSpan = $("#reply").find(".num");
				var replyCount = parseInt(numSpan.text());
				replyCount++;
				numSpan.text(replyCount);
				layer.msg("恭喜您, 回复成功!",{icon: 1});
			}else if(status === 0){
				showLogin();
			}else{
				layer.msg(result.msg,{icon: 2});
			}
			$this.parent().parent().hide();
		});

		return false;
	});

});

var replyInput = null;
var replyCommentItem = null;
var replyParentItem = null;

/**
 * 构建一个回复框
 * @param parentId
 * @param parentUserName
 */
function buildReplyInput(parentId, parentUserName){
	if(replyInput){
		replyInput.find(".inner .wrap-textarea .reply-input").attr("placeholder", "回复 " + parentUserName + ":").val("");
		replyInput.find(".operate-box .reply-submit").attr("parentId", parentId).attr("parentUserName", parentUserName);
	}else{
		var topDiv = $("<div></div>").addClass("reply-textarea").addClass("J-reply-con");

		var towDiv = $("<div></div>").addClass("inner");

		var threeDiv = $("<div></div>").addClass("wrap-textarea");

		var textarea = $("<textarea></textarea>").addClass("reply-input").addClass("f-textarea").attr("placeholder", "回复 " + parentUserName + ":");

		var fourDiv = $("<div></div>").addClass("operate-box");

		var replyBtn = $("<a href='javascript:;' target='_self'></a>").addClass("reply-submit").attr("parentId", parentId).attr("parentUserName", parentUserName).text("提交");

		fourDiv.append(replyBtn);
		threeDiv.append(textarea);
		towDiv.append(threeDiv);
		topDiv.append(towDiv);
		topDiv.append(fourDiv);

		replyInput = topDiv;

	}

	return replyInput;
}

/**
 * 构建一个回复评价的回复项
 */
function buildReplyCommentItem(productReply){
	if(replyCommentItem){
		replyCommentItem.find(".tt .name").text(productReply.replyUserName);
		replyCommentItem.find(".tt .con").text(": " + productReply.replyContent);
		replyCommentItem.find(".tc .a_reply").attr("parentId", productReply.id);
		replyCommentItem.find(".tc .reply-time").text(dateFomat(new Date));
	}else{
		var div1 = $("<div></div>").addClass("item");
		var div2 = $("<div></div>").addClass("tt");
		var span1 = $("<span></span>").addClass("name").text(productReply.replyUserName);
		var span2 = $("<span></span>").addClass("con").text(": " + productReply.replyContent);
		var div3 = $("<div></div>").addClass("tc").addClass("clearfix");
		var a1 = $("<a href='#none'>回复</a>").addClass("a_reply").attr("parentId", productReply.id).attr("parentUserName", productReply.parentUserName);
		var span3 = $("<span></span>").addClass("reply-time").text(dateFomat(new Date));

		div3.append(a1);
		div3.append(span3);
		div2.append(span1);
		div2.append(span2);
		div1.append(div2);
		div1.append(div3);

		replyCommentItem = div1;
	}

   return replyCommentItem;
}

/**
 * 构建一个回复其他人的回复项
 */
function buildReplyParentItem(productReply){

	if(replyParentItem){
		replyParentItem.find(".tt .name").text(productReply.replyUserName);
		replyParentItem.find(".tt .con").text(productReply.replyContent);
		replyParentItem.find(".tc .a_reply").attr("parentId", productReply.id);
		replyParentItem.find(".tc .reply-time").text(dateFomat(new Date));
	}else{
		var div1 = $("<div></div>").addClass("item");
		var div2 = $("<div></div>").addClass("tt");
		var span1 = $("<span></span>").addClass("name").text(productReply.replyUserName + ": ").after("回复");
		var span2 = $("<span></span>").addClass("name").text(" " + productReply.parentUserName + " ");
		var span3 = $("<span></span>").addClass("con").text(productReply.replyContent);
		var div3 = $("<div></div>").addClass("tc").addClass("clearfix");
		var a1 = $("<a href='#none'>回复</a>").addClass("a_reply").attr("parentId", productReply.id).attr("parentUserName", productReply.parentUserName);
		var span4 = $("<span></span>").addClass("reply-time").text(dateFomat(new Date));

		div3.append(a1);
		div3.append(span4);
		div2.append(span1);
		div2.append(span2);
		div2.append(span3);
		div1.append(div2);
		div1.append(div3);

		replyParentItem = div1;
	}

   return replyParentItem;
}

/**
 * 敏感字检查
 * @param content
 * @returns
 */
function checkSensitiveData(content){
	var url = contextPath + "/sensitiveWord/filter";
	$.ajax({
		"url":url,
		data: {"src": content},
		type:'GET',
		dataType : 'JSON',
		async : false, //默认为true 异步
		success:function(retData){
	   		result = retData;
		}
	});
	return result;
}


 function isLogin(){
	if(loginUserName){
		return true;
	}
	return false;
 }

 function showLogin(){
	 layer.open({
			title :"登录",
			  type: 2,
			  content: contextPath + '/loadLoginOverlay', //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
			  area: ['440px', '530px']
			});
 }

 function dateFomat(date){
	 return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes();
 }

 function pager(curPageNO){
	 window.location.href = urls.productCommentDetail + "?prodComId=" + prodComId + "&curPageNO=" + curPageNO;
 }
