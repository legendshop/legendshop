var specMap = new Map();//propId:valIdList
var skuMap = new Map();//sku数据
var valueAliasMap = new Map();//用户自定义属性值名
var valueImageMap = new Map();

var number = /^[-\+]?\d+(\.\d+)?$/;
var posInt = /^\d+$/;//非负整数

var skuList = '<div class="sku-group sku-custom">' +
    '<label class="sku-label">' +
    '<div class="J_CustomCaption input-ph-wrap">' +
    '<input type="text" id="paramKey" class="sku-caption text" maxlength="10">：' +
    '</div>' +
    '</label>' +
    '<div class="sku-box">' +
    '<ul class="sku-list">' +
    '<li style="margin-right: 3px;"><input type="text" id="paramValue0" class="text" maxlength="10"></li>' +
    '<li style="margin-right: 3px;"><input type="text" id="paramValue1" class="text" maxlength="10"></li>' +
    '<li style="margin-right: 3px;"><input type="text" id="paramValue2" class="text" maxlength="10"></li>' +
    '<li style="margin-right: 3px;"><input type="text" id="paramValue3" class="text" maxlength="10"></li>' +
    '<li style="margin-right: 3px;"><input type="text" id="paramValue4" class="text" maxlength="10"></li>' +
    '<li style="margin-right: 3px;"><input type="text" id="paramValue5" class="text" maxlength="10"></li>' +
    '<li style="margin-right: 3px;"><input type="text" id="paramValue6" class="text" maxlength="10"></li>' +
    '<li style="margin-right: 3px;"><input type="text" id="paramValue7" class="text" maxlength="10"></li>' +
    '<li style="margin-right: 3px;"><input type="text" id="paramValue8" class="text" maxlength="10"></li>' +
    '<li style="margin-right: 3px;"><input type="text" id="paramValue9" class="text" maxlength="10"></li>' +
    '<li style="margin-right: 3px;"><input type="text" id="paramValue10" class="text" maxlength="10"></li>' +
    '<li style="margin-right: 3px;"><input type="text" id="paramValue11" class="text" maxlength="10"></li>' +
    '</ul>' +
    '<div class="sku-custom-operations"><a title="删除" class="sku-delgroup J_SKUDelGroup" href="javascript:void(0);" onclick="delProperties(this);">X</a></div>' +
    '</div>' +
    '</div>';

Array.prototype.remove = function (el) {
    return this.splice(this.indexOf(el), 1);
}
var properties;
$(document).ready(function () {
    //如果没有属性，隐藏单品信息左侧栏
    $("#video").change(function () {
        if($("#proVideo").val!=""){
            $("#videoMain").css("display","block")
        }
    })
    //属性不能为空
    $(window).on("focus",".labelname input",function(){
        properties=this.value;
    })
    var skuPanelLen = $(".sku-p-l ul li").length;
    if (skuPanelLen == 0) {
        $(".sku-p-l").hide();
    }

    //加载参数选择插件
    $(".J_spu-property").divselect();

    //三级联动
    $("select.combox").initSelect();

    dateOption(16);//日期选项加载

    var publishStatus = $("#publishStatus").val();
    if (isBlank(publishStatus)) {
        $("#now0").attr("checked", true);
    }

    //初始化商品自定义参数
    var initUserParam = $("#userParameter").val();
    if (!isBlank(initUserParam)) {
        $.each(eval("(" + initUserParam + ")"), function (idx, item) {
            $("#prop" + idx).val(item.key);
            $("#propValue" + idx).val(item.value);
        })
    }

    //初始化商品参数
    var initParam = $("#parameter").val();
    if (!isBlank(initParam)) {
        $.each(eval("(" + initParam + ")"), function (idx, item) {
            var paramOption = $("label[paramid=" + item.paramId + "]").next().find("div[class='kui-dropdown-trigger'] input.kui-combobox-caption");
            if (item.paramValueId == "") {
                paramOption.removeAttr("readonly");
                paramOption.attr("paramvalueid", "");
            } else {
                paramOption.attr("paramvalueid", item.paramValueId);
            }
            paramOption.val(item.paramValueName);
        })
    }


    //初始化 specMap
    if (skudata.length > 0) { //2781:12143;2172:9115;
        for (var i = 0; i < skudata.length; i++) {
            var str = skudata[i];
            var props = str.split(";");
            for (var j = 0; j < props.length; j++) {
                var value = props[j];
                var result = value.split(":");
                var p = result[0];
                var v = result[1];

                var valueList = specMap.get(p);
                if (valueList == null) {
                    valueList = new Array();
                }

                //如果valueList里面没有则加入，并将其选中
                if (!valueList.contains(v)) {
                    valueList.push(v);
                    $("#sku_" + v).attr("checked", true);
                    $("#labelInput_" + v).show();
                    $("#labelName_" + v).hide();

                    specMap.put(p, valueList);
                }

            }

        }
    }

    //用户自定义属性值名 读取
    var valueAliasList = jQuery.parseJSON($("#valueAlias").val());
    if (!isBlank(valueAliasList)) {
        for (var q = 0; q < valueAliasList.length; q++) {
            var valueAlia = valueAliasMap.get(valueAliasList[q].valueId);
            if (isBlank(valueAlia)) {
                valueAlia = new Object();
            }
            valueAlia.alias = valueAliasList[q].alias;
            valueAlia.valueId = valueAliasList[q].valueId;
            valueAliasMap.put(valueAliasList[q].valueId, valueAlia);
        }
    }


    //勾选属性后，刷新处理
    var prodId = $("#prodId").val();
    var cookieMap = "";
    if (!isBlank(cookieMap) && isBlank(prodId)) {//第一次发布商品,不是编辑商品
        specMap.elements = jQuery.parseJSON(cookieMap);
        var pvids = specMap.values();
        var pType, pName, pid, vName, vid;
        var trlength = $("#colorTable tbody tr").length;
        for (var n = 0; n < pvids.length; n++) {
            if (pvids[n].length > 1) {
                for (var m = 0; m < pvids[n].length; m++) {
                    pid = $("#sku_" + pvids[n][m]).attr("propid");
                    //pName = $("#skuProp_"+pid).attr("propname");
                    pType = $("#skuProp_" + pid).attr("propType");
                    vName = $("#sku_" + pvids[n][m]).val();
                    vid = $("#sku_" + pvids[n][m]).attr("valueid");
                    //显示 属性值图片列表
                    showValueImgTable(pType, pid, vName, vid, trlength);
                    //选中属性
                    $("#sku_" + pvids[n][m]).attr("checked", true);
                    $("#labelInput_" + pvids[n][m]).show();
                    $("#labelName_" + pvids[n][m]).hide();
                }
            } else {
                pid = $("#sku_" + pvids[n]).attr("propid");
                //pName = $("#skuProp_"+pid).attr("propname");
                pType = $("#skuProp_" + pid).attr("propType");
                vName = $("#sku_" + pvids[n]).val();
                vid = $("#sku_" + pvids[n]).attr("valueid");
                //显示 属性值图片列表
                showValueImgTable(pType, pid, vName, vid, trlength);
                //选中属性
                $("#sku_" + pvids[n]).attr("checked", true);
                $("#labelInput_" + pvids[n]).show();
                $("#labelName_" + pvids[n]).hide();
            }
        }

        //根据选中的属性选项，绘画出相应的sku表
        paintingtSkuTable();
    }


    //绑定商品主图片的 移动、删除事件
    $(".multimage-gallery ul li ").each(function () {
        var $this = jQuery(this);
        if ($this.find("div[class=preview] img").length != 0) {
            $this.mouseover(function () {
                $this.addClass("img-hover");
            });
            $this.mouseout(function () {
                $this.removeClass("img-hover");
            });
        }
    });

    //绑定属性checkbox 的click事件
    jQuery(".sku-wrap input[type=checkbox]").live("click", function () {
        var valueId = jQuery(this).attr("valueId");
        var displayLabel = jQuery(this).attr("value");
        var propId = jQuery(this).attr("propId");
        //var propName = jQuery("#skuProp_"+propId).attr("propname");
        var propType = jQuery("#skuProp_" + propId).attr("propType");
        var trlength = $("#colorTable tbody tr").length;//用来给id ++

        if (jQuery(this).attr("checked") != "checked") {//unchecked
            if($("#isAttrEditable").val()!="false"){
                $("#labelInput_" + valueId).hide();
                $("#labelName_" + valueId).show();
            }
            var valueList = specMap.get(propId);
            console.log(valueList)
            console.log(specMap)
            if (valueList != null) {
                valueList.remove(parseInt(valueId));
                if (valueList.length == 0) {
                    specMap.remove(propId);
                }
            }

            //根据选中的属性选项，绘画出相应的sku表
            paintingtSkuTable();

            //判断 选中的是否为颜色属性，是的话，颜色表减去该颜色
            if (propType == 1) {
                $("#colorId_" + valueId).remove();
            }


        } else {//checked
            if($("#isAttrEditable").val()!="false"){
                $("#labelInput_" + valueId).show();
                $("#labelName_" + valueId).hide();
            }
            var valueList = specMap.get(propId);
            if (valueList == null) {
                valueList = new Array();
            }
            valueList.push(valueId);
            specMap.put(propId, valueList);

            //根据选中的属性选项，绘画出相应的sku表
            paintingtSkuTable();

            //显示 属性值图片列表
            showValueImgTable(propType, propId, displayLabel, valueId, trlength);

            //将specMap 记录到cookie中
            //setCookie("specMap",JSON.stringify(specMap.getElements()));

        }

        //此处用来判断颜色表是否有数据，有则显示，没有则隐藏
        trlength = $("#colorTable tbody tr").length;
        if (trlength > 0) {
            $("#colorTable").show();
        } else {
            $("#colorTable").hide();
        }
    });

    KindEditor.options.filterMode = false;
    var editor = KindEditor.create('textarea[name="content"]', {
        cssPath: paramData.contextPath + '/resources/plugins/kindeditor/plugins/code/prettify.css',
        uploadJson: paramData.contextPath + '/editor/uploadJson/upload;jsessionid=' + paramData.cookieValue,
        fileManagerJson: paramData.contextPath + '/editor/uploadJson/fileManager',
        allowFileManager: true,
        afterBlur: function () {
            this.sync();
        },
        width: '1018px',
        height: '550px',
        imageUploadLimit: 30,
        afterCreate: function () {
            var self = this;
            KindEditor.ctrl(document, 13, function () {
                self.sync();
                document.forms['example'].submit();
            });
            KindEditor.ctrl(self.edit.doc, 13, function () {
                self.sync();
                document.forms['example'].submit();
            });
        },
        items: [
            'source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste',
            'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
            'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
            'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
            'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
            'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image', 'multiimage',
            'media', , 'table', 'hr', 'emoticons', 'baidumap', 'pagebreak',
            'anchor', 'link', 'unlink', '|', 'about'
        ]
    });

    var editor_m = KindEditor.create('textarea[name="contentM"]', {
        cssPath: paramData.contextPath + '/resources/plugins/kindeditor/plugins/code/prettify.css',
        uploadJson: paramData.contextPath + '/editor/uploadJson/upload;jsessionid=' + paramData.cookieValue,
        fileManagerJson: paramData.contextPath + '/editor/uploadJson/fileManager',
        allowFileManager: true,
        afterBlur: function () {
            this.sync();
        },
        width: '670px',
        height: '550px',
        imageUploadLimit: 30,
        afterCreate: function () {
            var self = this;
            KindEditor.ctrl(document, 13, function () {
                self.sync();
                document.forms['example'].submit();
            });
            KindEditor.ctrl(self.edit.doc, 13, function () {
                self.sync();
                document.forms['example'].submit();
            });
        },
        afterChange: function () {
            this.sync();
            $(".pv-device-bd").html($("#contentM").val());
        },
        items: [
            'source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste',
            'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
            'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
            'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
            'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
            'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image', 'multiimage',
            'media', 'table', 'hr', 'emoticons', 'baidumap', 'pagebreak',
            'anchor', 'link', 'unlink', '|', 'about'
        ]
    });

  var editor_p = KindEditor.create('textarea[name="serviceShow"]', {
    cssPath: paramData.contextPath + '/resources/plugins/kindeditor/plugins/code/prettify.css',
    uploadJson: paramData.contextPath + '/editor/uploadJson/upload;jsessionid=' + paramData.cookieValue,
    fileManagerJson: paramData.contextPath + '/editor/uploadJson/fileManager',
    allowFileManager: true,
    afterBlur: function () {
      this.sync();
    },
    width: '670px',
    height: '300px',
    imageUploadLimit: 30,
    afterCreate: function () {
      var self = this;
      KindEditor.ctrl(document, 13, function () {
        self.sync();
        document.forms['example'].submit();
      });
      KindEditor.ctrl(self.edit.doc, 13, function () {
        self.sync();
        document.forms['example'].submit();
      });
    },
    afterChange: function () {
      this.sync();
      $(".pv-device-bd").html($("#serviceShow").val());
    },
    items: [
      'source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste',
      'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
      'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
      'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
      'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
      'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image', 'multiimage',
      'media', 'table', 'hr', 'emoticons', 'baidumap', 'pagebreak',
      'anchor', 'link', 'unlink', '|', 'about'
    ]
  });


//图片空间 中选择 属性值图片的
    $("input[id^='imageSpace_']").live("click", function () {

        var valueImageUl = $(this).parent().parent().find("ul");
        var valueImageLi = valueImageUl.find("li").get();
        var propValueId = $(this).parent().parent().prev().attr("propValueId");
        var propValueName = $(this).parent().parent().prev().attr("propValueName");
        var propId = $(this).parent().parent().prev().attr("propId");

        if (valueImageLi.length >= 6) {
            alertMessage("属性图片最多6张！");
        } else {
            layer.open({
                title: "图片空间",
                type: 2,
                content: paramData.contextPath + '/s/imageAdmin/prop', //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
                area: ['810px', '430px'],
                btn: ['确定', '关闭'],
                yes: function (index, layero) {
                    var url = $("#layui-layer-iframe" + index).contents().find(".image_select").parent().find(".img").attr("photo");
                    var filePath = $("#layui-layer-iframe" + index).contents().find(".image_select").parent().find(".img").attr("filePath");
                    if (url == undefined || url == "") {
                        layer.msg("请最少选择一张图片", {icon: 0})
                        return false;
                    }
                    var img = '<li class="propValueImageLi" style="margin-right: 10px;" >' +
                        '<input propId="' + propId + '" propValueName="' + propValueName + '" propvalueId="' + propValueId + '" value="' + filePath + '" name="propValueImage" type="hidden"/>' +
                        '<img class="propValueImg" src="' + url + '" ondblclick="removeValueImage(this);" /></li>';
                    valueImageUl.append(img);
                    layer.close(index);
                }
            });
        }
    });


    var submitbar_top = $("#panel-base .submit").offset().top;
    jQuery(window).scroll(function () {
        var top = $("#panel-base .submit").offset().top;
        var scrolla = $(window).scrollTop() + $(window).height();
        var i = scrolla - top;
        if (i <= 0) {
            $("#panel-base .submit").addClass("pro_fixed");
        }
        if (scrolla > submitbar_top) {
            $("#panel-base .submit").removeClass("pro_fixed");
        }
    });


    //属性图片拖动排序功能的绑定
    jQuery("ul[id^='valueImageUL_']").sortable();

    //商品图片拖动排序功能的绑定
    jQuery(".multimage-gallery ul").sortable();

    jQuery(":radio[name='supportTransportFree']").click(function () {
        var val = jQuery(this).val();
        if (val == 0) {
            jQuery("#buyer_transport_info").show();
        } else {
            jQuery("#buyer_transport_info").hide();
        }
    });

    jQuery("#buyer_transport_info").find("input[type='text']").live("keyup", function () {
        jQuery(":input[id='mail_trans_fee']").each(function () {
            jQuery(this).val(jQuery(this).val().replace(/[^0-9.]/g, ''));
        });
    });
    jQuery("#buyer_transport_info").find("input[type='text']").live("keyup", function () {
        jQuery(":input[id='express_trans_fee']").each(function () {
            jQuery(this).val(jQuery(this).val().replace(/[^0-9.]/g, ''));
        });
    });
    jQuery("#buyer_transport_info").find("input[type='text']").live("keyup", function () {
        jQuery(":input[id='ems_trans_fee']").each(function () {
            jQuery(this).val(jQuery(this).val().replace(/[^0-9.]/g, ''));
        });
    });

    //运费方式 展示
    var transportTypeValue = jQuery(":radio[name=transportType]:checked").val();
    if (transportTypeValue == 1) {
        jQuery("#transport_template_select").find(".transport-item").hide();
        jQuery("#mail_trans_fee").removeAttr("readonly");
        jQuery("#express_trans_fee").removeAttr("readonly");
        jQuery("#ems_trans_fee").removeAttr("readonly");
    } else {
        jQuery("#transport_template_select").find(".transport-item").show();
        jQuery("#mail_trans_fee").attr("readonly", "readonly");
        jQuery("#express_trans_fee").attr("readonly", "readonly");
        jQuery("#ems_trans_fee").attr("readonly", "readonly");
    }

    jQuery(":radio[name=transportType]").click(function () {
        var val = jQuery(this).val();
        if (val == 0) {
            jQuery("#transport_template_select").find(".transport-item").show();
            jQuery("#mail_trans_fee").attr("readonly", "readonly");
            jQuery("#express_trans_fee").attr("readonly", "readonly");
            jQuery("#ems_trans_fee").attr("readonly", "readonly");
        } else {
            jQuery("#transport_template_select").find(".transport-item").hide();
            jQuery("#mail_trans_fee").removeAttr("readonly");
            jQuery("#express_trans_fee").removeAttr("readonly");
            jQuery("#ems_trans_fee").removeAttr("readonly");
        }
    });


    //图片上传tab
    $(".multimage-wrapper .multimage-tabs .tab").click(function () {
        $(this).addClass("actived").siblings().removeClass("actived");
        if ($(this).attr("type") == "remote-image") {
            //if($(".multimage-panels .remote-image").html().trim()==""){
            $(".multimage-panels .remote-image").html(
                "<iframe style='height:330px;width:810px;border:0px;' src='" + paramData.contextPath + "/s/imageAdmin/remoteImages'></iframe>"
            );
            //}
            $(".multimage-wrapper .multimage-panels .local-panel").hide();
            $(".multimage-wrapper .multimage-panels .remote-image").show();
        } else {
            $(".multimage-wrapper .multimage-panels .local-panel").show();
            $(".multimage-wrapper .multimage-panels .remote-image").hide();
        }
    });

    //详情描述tab
    $(".dc-wapper .dc-tabs .tab").click(function () {
        $(this).addClass("actived").siblings().removeClass("actived");
        if ($(this).attr("type") == "mobile") {
            $(".dc-edit").hide();
            $(".dc-edit-m").show();
        } else {
            $(".dc-edit").show();
            $(".dc-edit-m").hide();
        }
    });

    //回到顶部
    jQuery("#toTop").click(function () {
        jQuery('body,html').animate({scrollTop: 0}, 1000);
        return false;
    });
    //页面导航 点击事件
    jQuery(".seller_right_nav_center a").click(function () {
        var target_id = jQuery(this).attr("target_id");
        if (target_id != "") {
            var top = jQuery("#" + target_id).offset().top - 2;
            jQuery('body,html').animate({scrollTop: top}, 1000);
        }
    });
    //页面导航样式变化
    var list = new Array();
    jQuery(".seller_right_nav_center a").each(function () {
        var target_id = jQuery(this).attr("target_id");
        list.push(target_id);
    });
    jQuery(window).scroll(function () {
        var top = jQuery(document).scrollTop();
        for (var i = 0; i < list.length; i++) {
            if (list[i] != "") {
                var _top1 = jQuery("#" + list[i]).offset().top - top;
                var _top2;
                if (i + 1 == list.length) {
                    _top2 = jQuery("#" + list[i + 1]).offset().top - top;
                } else {
                    _top2 = 1;
                }
                if (_top1 < 100 && _top2 > 0) {
                    jQuery(".seller_right_nav_center a").removeClass("this");
                    jQuery("a[target_id=" + list[i] + "]").addClass("this");
                }
            }
        }
    });

    //输入框 获取焦点后 去除红色外框
    $(document).delegate("input", "focus", function () {
        $(this).removeClass("prod-edit-err");
    });

    $("input[name='supportDist']").live("click", function () {
        var val = $(this).val();
        if (val == 0) {
            $("#level_rate").slideUp();
        } else if (val == 1) {
            $("#level_rate").slideDown();
        }
    });

    $(".sku-p-l ul li").each(function () {
        var row = $(this).attr("propvalids");
        if (!isBlank(row)) {
            recordSkuMap(row);
        }
    });


});


function recordSkuMap(skuId) {

    var sku = skuMap.get(skuId);
    if (isBlank(sku)) {
        var sku = new Object();
    }
    var row = "#row_" + skuId;
    var price = $.trim($(row + "_cash").val());//sku价格
    if (isBlank(price)) {
        price = 0;
    }
    sku.price = price;

    var name = $.trim($(row + "_name").val());//sku名称
    if (isBlank(name)) {
        sku.name = name;
    }

    var code = $.trim($(row + "_code").val());//code名称
    if (isBlank(code)) {
        sku.code = code;
    }

    var status = $('input[name="pro_status_' + skuId + '"]:checked ').val();//状态
    sku.status = status;

    var partyCode = $.trim($(row + "_code").val());
    if (isBlank(partyCode)) {
        sku.partyCode = partyCode;
    }
    var modelId = $.trim($(row + "_model").val());
    if (isBlank(modelId)) {
        sku.modelId = modelId;
    }

    var volume = $.trim($(row + "_volume").val());
    if (isBlank(volume)) {
        sku.volume = volume;
    }

    var weight = $.trim($(row + "_weight").val());
    if (isBlank(weight)) {
        sku.weight = weight;
    }

    skuMap.put(skuId, sku);
}


//递归调用,获得所有的组合
function parseValue(values) {
    var len = values.length;
    if (len >= 2) {
        var len1 = values[0].length;
        var len2 = values[1].length;
        var newlen = len1 * len2;
        var temp = new Array(newlen);
        var index = 0;
        for (var i = 0; i < len1; i++) {
            for (var j = 0; j < len2; j++) {
                temp[index] = values[0][i] + "_" + values[1][j];
                index++;
            }
        }
        var newArray = new Array(len - 1);
        for (var i = 2; i < len; i++) {
            newArray[i - 1] = values[i];
        }
        newArray[0] = temp;
        return parseValue(newArray);
    } else {
        return values[0];
    }
}


// 根据选中的属性选项，绘画出相应的sku表
function paintingtSkuTable() {
    specMap.sort();
    $(".sku-p-l ul").html("");
    $(".sku-p-m").html("");
    $(".sku-default").html("");
    $(".sku-p-l").hide();
    if (specMap.size() > 0) {

        $("#skuTable tbody").remove();
        var result = parseValue(specMap.values());
        var propIds = specMap.keys();

        for (var i = 0; i < result.length; i++) {
            var prodCash, prodName, prodStocks, status = 1, partyCode, modelId, stock, volume, weight;

            var dynLi = "<li propValIds='" + result[i] + "' onclick='changeSkuTab(this);' ";
            if (i == 0) {
                dynLi = dynLi + "class='on'";
            }
            dynLi = dynLi + " ><span>";

            var skuObject = skuMap.get(result[i]);
            if (!isBlank(skuObject)) {
                prodCash = skuObject.price;
                prodName = skuObject.name;
                status = skuObject.status;
                partyCode = skuObject.partyCode;
                modelId = skuObject.modelId;
                volume = skuObject.volume;
                weight = skuObject.weight;
                stock = skuObject.stock;

            }
            if (isBlank(prodName)) {// sku名称
                prodName = $("#prodTitle").val();
            }

            if (isBlank(prodCash)) {// sku价格
                prodCash = $("#cash").val();
                if (isBlank(prodCash)) {
                    prodCash = 0;
                }
            }

            if (isBlank(partyCode)) {
                partyCode = "";
            }

            if (isBlank(modelId)) {
                modelId = "";
            }

            if (isBlank(volume)) {
                volume = "";
            }
            if (isBlank(weight)) {
                weight = "";
            }

            if (isBlank(stock)) {
                stock = "";
            }

            var ids = result[i].toString().split("_");

            for (var j = 0; j < ids.length; j++) {
                var value = $(" #sku_" + ids[j]).val();
                if (!isBlank(value)) {
                    if (j > 0) {
                        dynLi = dynLi + "、";
                    }
                    dynLi = dynLi + "<span class='pv_" + ids[j] + "'>" + value + "</span>";
                }
            }

            dynLi = dynLi + "</span></li>";

            var dynSku = $("#blankSkuDetial").html();
            dynSku = dynSku.replace(new RegExp(/(#propValueIds#)/g), result[i]);
            dynSku = dynSku.replace("#skuPrice#", prodCash);
            dynSku = dynSku.replace("#skuName#", prodName);
            dynSku = dynSku.replace("#partyCode#", partyCode);
            dynSku = dynSku.replace("#modelId#", modelId);
            dynSku = dynSku.replace("#volume#", volume);
            dynSku = dynSku.replace("#weight#", weight);
            dynSku = dynSku.replace("#stock#", stock);

            $(".sku-p-m").append(dynSku);
            $(".sku-p-m").children(":first").show();
            if (isBlank(status) || status == 1) {
                $('input[name="pro_status_' + result[i] + '"].up_yes').click();
            } else {
                $('input[name="pro_status_' + result[i] + '"].up_not').click();
            }
            $(".sku-p-l ul").append(dynLi);
            $(".sku-p-l").show();
        }
    } else {//添加一个默认的单品
        var dynSkuDefault = $("#blankSkuDefault").html();
        $(".sku-default").html(dynSkuDefault);
    }
}

//显示 属性值图片列表
function showValueImgTable(propType, propId, displayLabel, valueId, trlength) {
    //判断 选中的是否为颜色属性，是的话，颜色表增加该颜色
    if (propType == 1) {
        trlength = trlength + 1;
        var tabletd = '<tr id="colorId_' + valueId + '">' +
            '<td propId="' + propId + '" propValueId="' + valueId + '" propValueName="' + displayLabel + '" class="pv_' + valueId + '" >' + displayLabel + '</td>' +
            '<td>' +
            '<a class="propImagebutton" href="javascript:void(0);" style="float:left;margin-right: 5px;">' +
            '<span class="btn-txt">文件上传</span>' +
            '<input type="file" onchange="javascript:uploadPropImage(this);" id="valueImage_' + trlength + '" name="valueImageFiles" class="propImageInput" multiple="multiple">' +
            '</a>' +

            '<a class="propImagebutton" href="javascript:void(0);" style="float:left;margin-right: 10px;">' +
            '<span class="btn-txt">图片空间</span>' +
            '<input type="button" onchange="javascript:void(0);" id="imageSpace_' + trlength + '" class="propImageInput">' +
            '</a>' +

            '<ul style="display: inline;width: 400px;" id="valueImageUL_' + trlength + '"></ul>' +
            '</td>' +
            '</tr>';

        $("#colorTable tbody").append(tabletd);

        $("#colorTable").show();
        //属性图片拖动排序功能的绑定
        jQuery("ul[id^='valueImageUL_']").sortable();
    }
}

//商品属性应用
function applicationProperty() {
    //将要新加的属性propMap

    var propMap = new Map();
    //var valueList = new Array();
    var propertyTextList = $(".sku-customlist .sku-label input").get();
    for (var x = 0; x < propertyTextList.length; x++) {
        var property = $.trim($(propertyTextList[x]).val());
        var valueTextList = $(propertyTextList[x]).parent().parent().parent().find(".sku-box ul li input").get();
        if (!isBlank(property)) {
            var valueList = propMap.get(property);
            if (valueList == null) {
                valueList = new Array();
            }
            for (var y = 0; y < valueTextList.length; y++) {
                if (!isBlank($(valueTextList[y]).val())) {
                    valueList.push($.trim($(valueTextList[y]).val()));
                }
            }
            propMap.put(property, valueList);
        }
    }



    // console.debug("propMap size= " + propMap.size() +",  "  +  JSON.stringify(propMap));

    //ajax传递到后台, 生成Id
    if (propMap.size() > 0) {
      var count = 1;
      propMap.elements.forEach(function ( key,value) {
        count = count * key.value.length;
      });
      console.log(count);
      if(count >= 999){
        layer.msg("过多的规格属性，新建的SKU不易超过999");
        return;
      }

        var originUserProperties = $("#userProperties").val();

        $.ajax({
            url: paramData.contextPath + '/s/applyProperty',
            data: {
                "originUserProperties": originUserProperties,
                "customAttribute": JSON.stringify(propMap.getElements())
            },
            type: 'post',
            dataType: 'json',
            async: true, //默认为true 异步
            success: function (result) {//result为原有的加入新的属性合并之后的结果
                //清除 之前自定义属性的选项
                $("div[type='custom']").remove();
                var skuGroupSize = $("#J_SellProperties .sku-wrap ").children(".sku-group").length - 1;
                if (!isBlank(result)) {
                    var propertyArray = jQuery.parseJSON(result);
                    $("#userProperties").val(result);
                    for (var i = 0; i < propertyArray.length; i++) {
                        var resultProp = propertyArray[i].productProperty;
                        var resultPropValues = propertyArray[i].propertyValueList;

                        if (!isBlank(resultProp) && !isBlank(resultPropValues)) {
                            skuGroupSize = skuGroupSize + 1;
                            // 画自定义的商品属性表格开始 ->
                            var splicingProperties = '<div id="sku-group-' + skuGroupSize + '" class="sku-group" type="custom">' +
                                '<label class="sku-label" id="skuProp_' + resultProp.propId + '" propName=' + resultProp.propName + '>' + resultProp.propName + '：</label>' +
                                '<a href="javascript:void(0);"  style="color:#005ea7;" onclick="removeAttributeOption(this);">删除</a>  <div class="sku-box "><ul class="sku-list">';

                            for (var n = 0; n < resultPropValues.length; n++) {
                                splicingProperties = splicingProperties + '<li class="sku-item">' +
                                    '<input type="checkbox" class="J_Checkbox" value="' + resultPropValues[n].name + '" propid="' + resultProp.propId + '" valueid="' + resultPropValues[n].valueId + '" id="sku_' + resultPropValues[n].valueId + '"> ' +
                                    '<label id="labelInput_' + resultPropValues[n].valueId + '"  class="labelname" title="' + resultPropValues[n].name + '"> ' +
                                    '<input type="text" style="width: 60px; margin-top: 7px;" value="' + resultPropValues[n].name + '" onblur="propValueAlias(this);"> ' +
                                    '</label>' +
                                    '<label id="labelName_' + resultPropValues[n].valueId + '" style="display:none;margin-top: -2px;" class="labelname" title="' + resultPropValues[n].name + '">' + resultPropValues[n].name + '</label>' +
                                    '</li>';
                                var newPropId = resultPropValues[n].propId; //全新的propId
                                var newValueId = resultPropValues[n].valueId;
                                var customValues = specMap.get(newPropId); //检查原来的属性Id是否已经存在
                                if (customValues == null) {//原来没有的
                                    //console.log("resultPropValues[n].propId 找不到 customValues  = " + newPropId);
                                    customValues = new Array();
                                    customValues.push(newValueId);
                                } else {
                                    var notExist = true;
                                    for (var x = 0; x < customValues.length; x++) {
                                        if (customValues[x] == newValueId) {
                                            notExist = false; //原来已经存在
                                        }
                                    }

                                    if (notExist) {
                                        customValues.push(newValueId);
                                    }
                                }
                                specMap.put(newPropId, customValues);
                            }

                            splicingProperties = splicingProperties + '</ul></div></div>';
                            if (i > 0) {
                                $("#user_prop_List").append(splicingProperties);
                            } else {
                                $("#user_prop_List").html(splicingProperties);
                            }
                        }
                    }

                    //清除自定义属性编辑内容
                    $("#J_CustomSKUList div[class='sku-group sku-custom']").remove();

                    //自定义属性全部选中，并重新画sku表格
                    $("#J_SellProperties .sku-wrap div[type='custom'] input[type='checkbox']").attr("checked", true);

                    //根据选中的属性选项，绘画出相应的sku表
                    paintingtSkuTable();
                }

                //判断 自定义的 属性个数是否等于4
                var customPropSize = $("#J_SellProperties .sku-wrap div[type='custom']").length;
                if (customPropSize == 4) {
                    $("#appPropButton").attr("class", "disabled");
                    $("#appPropButton").attr("onclick", "");
                }
            }
        });
    }else {

      layer.msg("请填写完整的属性名称与值", {icon: 0, time: 2000});
    }


}

//删除应用的属性选项(自定义属性选项)
function removeAttributeOption(obj) {

    var parentDiv = $(obj).parent();
    var propidText = $(obj).prev().attr("id");
    var propid = propidText.substring(propidText.indexOf("_") + 1);

    //将选中的值从Map中去除
    if (!isBlank(propid)) {
        specMap.remove(propid);
    } else {
        alertMessage("删除失败!");
    }

    //将原有的数据库中的自定义属性找出来, userProperties 下的该属性去除
    var userProperties = $("#userProperties").val();

    if (!isBlank(userProperties)) {
        var propertyArray = jQuery.parseJSON(userProperties);
        //console.debug("propertyArray： " + JSON.stringify(propertyArray));
        var arrayLength = propertyArray.length;
        for (var i = 0; i < arrayLength; i++) {
            var resultProp = propertyArray[i].productProperty;
            var resultPropValues = propertyArray[i].propertyValueList;
            if (resultProp.propId == propid) {
                propertyArray.remove(propertyArray[i]);
                break;
            }
        }
    }
    $("#userProperties").val(JSON.stringify(propertyArray));
    //console.debug("propertyArray： " + JSON.stringify(propertyArray));

    var deleteUserProperties = $("#deleteUserProperties").val();
    var deletePropArray;
    if (!isBlank(deleteUserProperties)) {
        deletePropArray = jQuery.parseJSON(deleteUserProperties);
        var flag = true;
        for (var x = 0; x < deletePropArray.length; x++) {
            if (deletePropArray[x] == propid) {
                flag = false;
            }
        }

        if (flag) {
            deletePropArray.push(propid);
        }
    } else {
        deletePropArray = new Array();
        deletePropArray.push(propid);
    }

    $("#deleteUserProperties").val(JSON.stringify(deletePropArray));
    //console.debug("deletePropArray： " + JSON.stringify(deletePropArray));

    //自定义属性删除
    parentDiv.remove();

    //重新绘画出相应的sku表
    paintingtSkuTable();

    //判断 自定义的 属性个数是否等于4
    var customPropSize = $("#J_SellProperties .sku-wrap div[type='custom']").length;

    if (customPropSize < 4) {
        $("#appPropButton").attr("class", "");
        $("#appPropButton").attr("onclick", "applicationProperty()");
    } else {
        $("#appPropButton").attr("class", "disabled");
        $("#appPropButton").attr("onclick", "");
    }
}

//选择参数模板
function selectUserParam(obj, curPageNO) {
    jQuery('.area_box').remove();
    jQuery.ajax({
        type: 'POST',
        url: paramData.contextPath + '/s/userParamOverlay',
        data: {"curPageNO": curPageNO},
        success: function (data) {
            jQuery(".box").append(data);
            var left = jQuery(obj).offset().left - 400;
            var top = jQuery(obj).offset().top + 30;
            jQuery(".area_box").css({"top": top + "px", "left": left + "px"}).show();
        }
    })
}

//保存属性模板
function toSaveAttribute(obj) {
    jQuery('.area_box').remove();
    jQuery.ajax({
        type: 'POST',
        url: paramData.contextPath + '/s/saveAttribute',
        success: function (data) {
            jQuery(".box").append(data);
            var left = jQuery(obj).offset().left - 100;
            var top = jQuery(obj).offset().top - 230;
            jQuery(".area_box").css({"top": top + "px", "left": left + "px"}).show();
        }
    });
}

//保存用户自定义属性
function saveUserParam() {
    var name = $("#paramName").val();
    //console.debug("name:"+name);

    var attributeMap = [];

    jQuery(".custom-list li").each(function () {
        var obj = new Object();
        obj.key = jQuery(this).find("input[id^=prop]").val();
        obj.value = jQuery(this).find("input[id^=propValue]").val();
        attributeMap.push(obj);
    });
    var content = JSON.stringify(attributeMap);
    //console.debug("content:"+content);
    $.ajax({
        url: paramData.contextPath + '/s/userParam/save',
        data: {"content": content, "name": name},
        type: 'post',
        dataType: 'json',
        async: true, //默认为true 异步
        success: function (result) {
            if (result == 'OK') {
                layer.msg("保存成功！", {icon: 1, time: 800});
                $('.area_box').remove();
            } else if (result == 'fail') {
                layer.msg("数据不能为空！", {icon: 2, time: 800});
            } else {
                layer.alert('数据错误，保存失败！', {icon: 2})
            }
        }
    });
}

//选择属性模板
function selectAttribute(obj, curPageNO) {
    jQuery('.area_box').remove();
    jQuery.ajax({
        type: 'POST',
        url: paramData.contextPath + '/s/attributeOverlay',
        data: {"curPageNO": curPageNO},
        success: function (data) {
            jQuery(".box").append(data);
            var left = jQuery(obj).offset().left - 300;
            var top = jQuery(obj).offset().top + 30;
            jQuery(".area_box").css({"top": top + "px", "left": left + "px"}).show();
        }
    })
}

//参数处，存为参数模板
function toSaveUserParam(obj) {
    jQuery('.area_box').remove();
    jQuery.ajax({
        type: 'POST',
        url: paramData.contextPath + '/s/saveUserParam',
        success: function (data) {
            jQuery(".box").append(data);
            var left = jQuery(obj).offset().left - 100;
            var top = jQuery(obj).offset().top - 230;
            jQuery(".area_box").css({"top": top + "px", "left": left + "px"}).show();
        }
    })
}

//自定义中的清空
function clearData(obj) {
    $(obj).parent().find("input[type=text]").val("");
}

//保存属性及属性名称
function saveAttribute() {

    var name = $("#attributeName").val();
    //console.debug("name:"+name);
    var paramMap = [];

    $("#J_CustomSKUList").find("div[class^=sku-group]").each(function () {
        var obj = new Object();
        var valueMap = [];
        obj.key = jQuery(this).find("input[id=paramKey]").val();
        //console.debug("key:"+obj.key);
        jQuery(this).find("ul[class=sku-list] li").each(function () {
            //console.debug("Li_value:"+jQuery(this).find("input[id^=paramValue]").val());
            if (jQuery(this).find("input[id^=paramValue]").val() != "") {
                valueMap.push(jQuery(this).find("input[id^=paramValue]").val());
            }
        });
        obj.value = valueMap;
        paramMap.push(obj);
    });

    var content = JSON.stringify(paramMap);

    // console.debug("content:"+content);

    $.ajax({
        url: paramData.contextPath + '/s/UserAttribute/save',
        data: {"content": content, "name": name},
        type: 'post',
        dataType: 'json',
        async: true, //默认为true 异步
        success: function (result) {
            if (result == 'OK') {
                layer.msg("保存成功！", {time: 2500});
                $('.area_box').remove();
            } else if (result == 'fail') {
                layer.msg("数据不能为空！", {time: 1000});
            } else {
                layer.alert('数据错误，保存失败！', {icon: 2})
            }
        }
    });
}

//添加售后说明
function afterSale() {
    layer.open({
        title: "售后说明页面",
        id: "afterSale",
        type: 2,
        content: paramData.contextPath + '/s/afterSaleOverlay', //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
        area: ['850px', '530px']
    });
}


//删除该属性
function delProperties(obj) {
    layer.confirm("您确定要删除该属性？", {icon: 3}, function (index) {
        $(obj).parent().parent().parent().remove();
        $("#newPropertyButton").removeAttr("class");
        $("#newPropertyButton").attr("onclick", "newProperties(this)");
        layer.close(index);
    });
}

//添加商品属性
function newProperties(obj) {
    var skusLength = $("#J_CustomSKUList").find("div[class^=sku-group]").length;
    if (skusLength == 3) {
        $("#J_CustomSKUList").append(skuList);
        $(obj).attr("class", "disabled");
        $(obj).removeAttr("onclick");
    } else {
        $("#J_CustomSKUList").append(skuList);
    }
}


//添加自定义属性
function openProp(obj) {
    $("#addAttrDefined").hide();
    $("#cancelAttrDefined").show();
    $(".custom-list-wrap").attr("style", "");
    $("#customprop").attr("style", "height: 270px;width: 650px;");
}

//取消自定义属性
function closeProp(obj) {
    layer.confirm("您确定要删除该属性？", {
        icon: 3
        , btn: ['确定', '关闭'] //按钮
    }, function () {
        $("#addAttrDefined").show();
        $("#cancelAttrDefined").hide();
        $("#customprop").attr("style", "");
        //清空所自定义参数
        $("input[id^='prop']").val("");
        $("input[id^='propValue']").val("");
        $(".custom-list-wrap").attr("style", "display: none;");
        layer.closeAll();
    });
}


//商品图片处的文件上传，通过Ajax方式上传图片
function uploadFile() {
    var uploadNumber = $(".multimage-gallery ul .haveImage").size();
    if (uploadNumber == 6) {
        layer.alert('您最多可以上传6张图片', {icon: 2});
        return false;
    } else {
        if (!checkImgType($("#file"))) {
            return false;
        } else if (!checkImgSize($("#file"), 500)) {
            return false;
        }
        $.ajaxFileUpload({
            url: paramData.contextPath + '/s/prodpicture/save',
            secureuri: false,
            fileElementId: 'file',
            dataType: 'json',
            success: function (data, status) {
                if (data == "fail") {
                    layer.alert('仅支持JPG、GIF、PNG、JPEG、BMP格式，大小不超过500k', {icon: 2});
                } else {
                    var p = data.split(",");
                    for (var i = 0; i < p.length; i++) {
                        var url = paramData.photoPath + p[i];
                        var img = '<img src="' + url + '"/>';
                        appendImg(img, p[i]);
                        //清除原有数据
                        var file = $("#file");
                        file.val(""); //虽然file的value值不能设为有内容的字符，但是可以设置为空字符
                    }
                }
            }
        });
    }
}

//商品图片处的文件上传，通过Ajax方式上传图片
function uploadVideoFile() {
        var videoVal = $("#video").get(0).files;
        if(videoVal.length>1){
            layer.alert('亲:不支持批量上传哦！请重新选择', {icon: 0});
            //清空文本框
            $("#video").val("");
            return false;
        }
        var file= videoVal[0];
         //如果大于30mb重新选择
            if(videoVal[0].size>1024*1024*30){
                layer.alert('亲:视频过大哦！', {icon: 0});
                //清空文本框
                $("#video").val("");
                return false;
            }
        $.ajaxFileUpload({
            url: paramData.contextPath + '/s/prodpicture/saveVedio',
            secureuri: false,
            fileElementId: 'video',
            dataType: 'json',
            success: function (data, status) {
                if (data == "fail") {
                    layer.alert('上传失败！请检查视频格式和大小或者联系管理员。', {icon: 2});
                } else {
                    layer.alert('上传成功', {icon: 3});
                    var p = data.split(",");
                    var img = "";
                    var videoUrl = "";
                    for (var i = 0; i < p.length; i++) {
                        var url = paramData.photoPath + p[i];
                        videoUrl = p[i];
                    }
                    var videHtml = "<video autoplay='autoplay' style='width: 320px;height: 240px' controls='controls' src = "+url+"></video> <a href='javascript:void(0)'><span onclick='delVideo()'>删除视频</span></a>";
                    $("#myVideo").html(videHtml);
                    $("#proVideo").val(videoUrl);
                    $("#video").val("")

                    //回显视频
                    $("#videoMain").css("display","block")
                }
            }
        });
}

//删除视频
function delVideo() {
    $("#myVideo").html("");
    //
    $("#proVideo").val("")

    //隐藏主图视频
    $("#videoMain").css("display","none")
}

//宝贝图片处 向右移动图片框
function toright(obj) {
    var ThisLi = jQuery(obj).parent().parent();
    var NextLi = jQuery(obj).parent().parent().next();
    if (ThisLi.hasClass("primary")) {
        ThisLi.attr("class", "prodImg has-img");
        NextLi.attr("class", "primary prodImg has-img");
    }
    NextLi.after(ThisLi);
}

//宝贝图片处 向左移动图片框
function toleft(obj) {
    var ThisLi = jQuery(obj).parent().parent();
    var PrevLi = jQuery(obj).parent().parent().prev();
    if (PrevLi.hasClass("primary")) {
        PrevLi.attr("class", "prodImg has-img");
        ThisLi.attr("class", "primary prodImg has-img");
    }
    PrevLi.before(ThisLi);
}

//宝贝图片处 图片框
var prodLi = '<li  class="prodImg">' +
    '<input type="hidden" name="image_pos">' +
    '<div class="preview"></div>' +
    '<div class="operate">' +
    '<i class="toleft" onclick="toleft(this);">左移</i>' +
    '<i class="toright" onclick="toright(this);">右移</i>' +
    '<i class="del" onclick="delImg(this);">删除</i>' +
    '</div>' +
    '</li>';

//宝贝图片处 鼠标移动到图片上之后，点击删除图片 (注：这里只删除图片的URL，并未删除图片文件)
function delImg(obj) {
    var Ul = jQuery(obj).parent().parent().parent();
    var ThisLi = jQuery(obj).parent().parent();
    var NextLi = jQuery(obj).parent().parent().next();
    if (ThisLi.hasClass("primary")) {
        NextLi.attr("class", "primary prodImg has-img");
        ThisLi.remove();
        Ul.append(prodLi);
    } else {
        ThisLi.remove();
        Ul.append(prodLi);
    }
}

//子页面回调的id值设置给input
function setAfterSaleId(id, name) {
    $("#afterSaleId").val(id);
    $("#afterSaleName").html(name);
    $("#afterSaleClear").removeClass("hidden");
    layer.closeAll('iframe');
}

//用于上传图片后，将图片的URL赋值到 商品图片的框中
function appendImg(img, url) {
    if ($(".multimage-gallery ul").find("div[class=preview] input[name=imgurl]").length > 5) {
        layer.msg("最多只能选择6张图片");
        return;
    }
    $(".multimage-gallery ul li ").each(function () {
        var $this = jQuery(this);
        if ($this.find("div[class=preview] input[name=imgurl]").val() == url) {
            layer.msg("同一张图片不能多次添加");
            return false;
        }
        if ($this.find("div[class=preview] img").length == 0) {
            $this.find("div[class=preview]").append(img);
            $this.find("div[class=preview]").append("<input type='hidden' name='imgurl' value='" + url + "'/>");
            $this.addClass("has-img haveImage");

            $this.mouseover(function () {
                $this.addClass("img-hover");
            });
            $this.mouseout(function () {
                $this.removeClass("img-hover");
            });
            return false;
        }
    });
    $("<div[class='multimage-gallery'] ul li.prodImg:first").removeClass("prod-edit-err");
}

//前往 运费模板 编辑界面
function toTransport() {
    window.open(paramData.contextPath + "/s/transport/load");
}

function clearAfterSale() {
    $("#afterSaleId").val("");
    $("#afterSaleClear").addClass("hidden");
}

function searchProperty(obj) {
    var propId = $(obj).attr("propId");
    var value = {"valueName": $(obj).val(), "propId": propId};
    $.ajax({
        url: paramData.contextPath + "/s/ProductPropertyValue/getPropValueName",
        type: 'post',
        data: value,
        async: true, //默认为true 异步
        success: function (result) {
            $(obj).parent().next().html(result);
        }
    });
}

function searchBrand(obj) {
    var categoryId = $("#categoryId").val();
    var value = {"brandName": $(obj).val(), "categoryId": categoryId};
    $.ajax({
        url: paramData.contextPath + "/s/brand/getBrandName",
        type: 'post',
        data: value,
        async: true, //默认为true 异步
        success: function (result) {
            $(obj).parent().next().html(result);
        }
    });
}

//校验商品图片，校验后将图片URL存入imagesPaths
function checkProdImages() {
    var liLength = $("<div[class='multimage-gallery'] ul .has-img").size();
    if (liLength == 0) {
        return false;
    }

    var imagesPaths = new Array();
    $(".multimage-gallery ul .has-img").each(function () {
        var $this = jQuery(this);
        var path = $this.find("div[class=preview] input[name='imgurl']").val();

        if (!isBlank(path)) {
            imagesPaths.push(path);
        }

    });

    //console.debug("imagesPaths:" + JSON.stringify(imagesPaths));
    if (imagesPaths.length != 0) {
        $("#imagesPaths").val(JSON.stringify(imagesPaths));
        return true;
    } else {
        return false
    }

}


//校验分类
function validateCategory() {
    var sortId = $("#globalSort").val();
    if (sortId == null || sortId == undefined || sortId == 0) {
        layer.alert("请选择分类信息", {icon: 0});
        return false;
    }
    var nsortId = $("#globalNsort").val();
    if (nsortId == null || nsortId == undefined || nsortId == 0) {
        layer.alert("请选择分类信息", {icon: 0});
        return false;
    }
    return true;
}

//检查使用有选择商品品牌
function validateProdBrand() {
    var prodBrandId = $("#productBrand").find("div[class='J_spu-property'] ul li select[class='keyPropClass'] option:checked").attr("value");
    var prodBrandName = $("#productBrand").find("div[class='J_spu-property'] ul li select[class='keyPropClass'] option:checked").text();

    if (isBlank(prodBrandId) && isBlank(prodBrandName)) {
        prodBrandId = $("#productBrand").find("div[class='J_spu-property'] ul li input[class='kui-combobox-caption']").attr("brandId");
        prodBrandName = $("#productBrand").find("div[class='J_spu-property'] ul li input[class='kui-combobox-caption']").val();
    }

    $("#prodBrandId").val(prodBrandId);

    //console.debug("prodBrandId:"+prodBrandId);
    if (isBlank(prodBrandId)) {
        return false;
    } else {
        return true;
    }

}

//滑动到 锚点
function animateToId(target_id) {
    var top = jQuery("#" + target_id).offset().top - 2;
    jQuery('body,html').animate({scrollTop: top}, 1000);
}

//检查基本信息
function checkBaseInfo() {
    var errNum = 0;
    var errTargetId = "";
    //用户自定义参数属性
    var userParamArray = [];
    var obj, userParamKey = "", userParamValue = "";
    jQuery(".custom-list li").each(function () {
        obj = new Object();
        userParamKey = jQuery(this).find("input[id^=prop]").val();
        userParamValue = jQuery(this).find("input[id^=propValue]").val();
        if (!isBlank(userParamKey)) {
            obj.key = userParamKey;
            obj.value = userParamValue;
            userParamArray.push(obj);
        }
    });
    var userParameter = JSON.stringify(userParamArray);
    $("#userParameter").val(userParameter);

    //参数属性
    var prodParamArray = [];
    var prodParamList = jQuery(".skin ul li[id^='propertyLi_'] ").get();
    var prodParam, paramId = "", paramName = "", inputParam = "", paramValueId = "", paramValueName = "",
        beRequired = "";
    for (var i = 0; i < prodParamList.length; i++) {
        prodParam = new Object();
        paramId = $(prodParamList[i]).find("label[class='label-title']").attr("paramid"); //参数名id
        paramName = $(prodParamList[i]).find("label[class='label-title']").attr("paramname");//参数名
        inputParam = $(prodParamList[i]).find("label[class='label-title']").attr("inputProp");//是否输入
        beRequired = $(prodParamList[i]).find("label[class='label-title']").attr("beRequired");//是否必填
        if (inputParam == "true") {
            paramValueName = $(prodParamList[i]).find("span input").val();
        } else {
            paramValueId = $(prodParamList[i]).find("div[class='J_spu-property'] ul li select[class='keyPropClass'] option:checked").attr("value");
            paramValueName = $(prodParamList[i]).find("div[class='J_spu-property'] ul li select[class='keyPropClass'] option:checked").text();
            if (isBlank(paramValueId) && isBlank(paramValueName)) {
                paramValueId = $(prodParamList[i]).find("div[class='J_spu-property'] ul li input[class='kui-combobox-caption']").attr("paramValueId")
                paramValueName = $(prodParamList[i]).find("div[class='J_spu-property'] ul li input[class='kui-combobox-caption']").val();
            }
        }
        if (beRequired == "true" && paramValueName == "") {
            $(prodParamList[i]).find("span input").addClass("prod-edit-err");
            errTargetId = "prod-param-tab";
            errNum++;
        }

        prodParam.inputParam = inputParam;
        prodParam.paramId = paramId;
        prodParam.paramName = paramName;
        prodParam.paramValueId = paramValueId;
        prodParam.paramValueName = paramValueName;
        prodParamArray.push(prodParam);
    }

    if (prodParamArray.length != 0 && prodParamArray != "[]") {
        var prodParameter = JSON.stringify(prodParamArray);
        $("#parameter").val(prodParameter);
    }

    //商品标题
    var prodTitle = $.trim($("#prodTitle").val());
    if (isBlank(prodTitle)) {
        $("#prodTitle").addClass("prod-edit-err");
        if (errTargetId == "") {
            errTargetId = "prod-base-tab";
        }
        errNum++;
    }
    if (prodTitle.length > 50) {
        $("#prodTitle").addClass("prod-edit-err");
        if (errTargetId == "") {
            errTargetId = "prod-base-tab";
        }
        errNum++;
    }

    //一口价
    var cash = $.trim($("#cash").val());
    if (isBlank(cash)) {
        $("#cash").addClass("prod-edit-err");
        if (errTargetId == "") {
            errTargetId = "prod-base-tab";
        }
        errNum++;
    } else {
        var numformat = /^\d+(\.\d{1,2})?$/;
        if (!numformat.test(cash)) {
            $("#cash").addClass("prod-edit-err");
            if (errTargetId == "") {
                errTargetId = "prod-base-tab";
                alertMessage("请输入两位小数以内的价格");
            }
            errNum++;
        } else if (cash < 0.01) {
            $("#cash").addClass("prod-edit-err");
            if (errTargetId == "") {
                errTargetId = "prod-base-tab";
                alertMessage("售价不能低于0.01元");
            }
            errNum++;
        }
    }

    //原价
    var price = $.trim($("#price").val());
    if (!isBlank(price)) {
        var numformat = /^\d+(\.\d{1,2})?$/;
        if (!numformat.test(price)) {
            $("#price").addClass("prod-edit-err");
            if (errTargetId == "") {
                errTargetId = "prod-base-tab";
                alertMessage("请输入两位小数以内的价格");
            }
            errNum++;
        } else if (price < 0.01) {
            $("#price").addClass("prod-edit-err");
            if (errTargetId == "") {
                errTargetId = "prod-base-tab";
                alertMessage("原价不能低于0.01元");
            }
            errNum++;
        }
    }

    //库存预警值
    var stocksArm = $.trim($("#stocksArm").val());
    if (!isBlank(stocksArm)) {
        /*var re = /^[1-9]*[0-9][0-9]*$/ ; */
        var re = /^\+?[1-9]\d*$/;
        if (!re.test(stocksArm)) {
            $("#stocksArm").addClass("prod-edit-err");
            if (errTargetId == "") {
                errTargetId = "prod-base-tab";
                alertMessage("请输入大于0的数字正整数");
            }
            errNum++;
        }
    }

    //佣金
    if (pluginId == "promoter") {
        if ($("input[name=supportDist]:checked").val() == 1) {
            var regex = /(?!^0\.0?0$)^[0-9][0-9]?(\.[0-9]{1,2})?$|^100$/;
            var totalRate = 0;
            if (!isBlank($("#firstLevelRate").val())) {
                if (!regex.test($("#firstLevelRate").val())) {
                    alertMessage("设置会员直接上级分佣比例比例格式错误");
                    animateToId("firstLevelRate");
                    return;
                }
                totalRate = totalRate + Number($("#firstLevelRate").val());
            } else {
                alertMessage("分佣比例不能为空");
                animateToId("firstLevelRate");
                return;
            }

            if ($("#secondLevelRate").length > 0) {
                if (!isBlank($("#secondLevelRate").val())) {
                    if (!regex.test($("#secondLevelRate").val())) {
                        alertMessage("设置会员上二级分佣比例比例格式错误");
                        animateToId("secondLevelRate");
                        return;
                    }
                    totalRate = totalRate + Number($("#secondLevelRate").val());
                } else {
                    alertMessage("分佣比例不能为空");
                    animateToId("firstLevelRate");
                    return;
                }
            }

            if ($("#thirdLevelRate").length > 0) {
                if (!isBlank($("#thirdLevelRate").val())) {
                    if (!regex.test($("#thirdLevelRate").val())) {
                        alertMessage("设置会员上三级分佣比例比例格式错误");
                        animateToId("thirdLevelRate");
                        return;
                    }
                    totalRate = totalRate + Number($("#thirdLevelRate").val());
                } else {
                    alertMessage("分佣比例不能为空");
                    animateToId("firstLevelRate");
                    return;
                }
            }

            if (totalRate > 50) {
                alertMessage("分佣比例总和不能超过50%");
                animateToId("firstLevelRate");
                return;
            }
        }
    }

    //校验图片
    var validatePassed = checkProdImages();
    // 获取相应的图片
    if (!validatePassed) {
        $("<div[class='multimage-gallery'] ul li.prodImg:first").addClass("prod-edit-err");
        if (errTargetId == "") {
            errTargetId = "prod-pic-tab";
        }
        errNum++;
    }

    validateProdBrand();

    var volume = $.trim($("#volume").val()); //物流体积(立方米)
    if (!isBlank(volume)) {
        if (!number.test(volume)) {
            $("#volume").addClass("prod-edit-err");
            if (errTargetId == "") {
                errTargetId = "prod-other-tab";
            }
            errNum++;
        }
    }

    var weight = $.trim($("#weight").val()); //物流重量(克)：
    if (!isBlank(weight)) {
        if (!number.test(weight)) {
            $("#weight").addClass("prod-edit-err");
            if (errTargetId == "") {
                errTargetId = "prod-other-tab";
            }
            errNum++;
        }
    }

    if (errNum > 0) {
        animateToId(errTargetId);
        return false;
    }

    var prodContent = $("#content").val();
    if (prodContent.length > 60000) {
        animateToId("prod-detail-tab");
        alertMessage("商品描述不能超过60000个字符");
        return false;
    }

    var contentM = $("#contentM").val();
    if (contentM.length > 60000) {
        animateToId("prod-detail-tab");
        alertMessage("商品描述不能超过60000个字符");
        return false;
    }

    var goods_transfee = $(':radio[name=supportTransportFree]:checked').val();

    var transport_type = $(':radio[name=transportType]:checked').val();

    if (goods_transfee == 0) {
        if (transport_type == 0) {
            var tpId = $("#transportId").val();
            if (tpId == -1) {
                animateToId("prod-other-tab");
                alertMessage("请选择运费模板!");
                return false;
            }
        } else {
            var mail_trans_fee = $("#mail_trans_fee").val();
            var express_trans_fee = $("#express_trans_fee").val();
            var ems_trans_fee = $("#ems_trans_fee").val();
            /*if(mail_trans_fee=="" || express_trans_fee==""||ems_trans_fee==""){*/
            if (express_trans_fee == "") {
                animateToId("prod-other-tab");
                alertMessage("请填写固定运费!");
                return false;
            } else {
                /*if(isNaN(mail_trans_fee)||isNaN(express_trans_fee)||isNaN(ems_trans_fee)){*/
                if (isNaN(express_trans_fee)) {
                    animateToId("prod-other-tab");
                    alertMessage("请输入正确的固定运费!");
                    return false;
                }
                var reg = /^(0(?:[.](?:[1-9]\d?|0[1-9]))|[1-9]\d*(?:[.]\d{1,2}|$))$/;
                /*if(!reg.test(mail_trans_fee))
				{
					  layer.alert('请输入平邮数字格式,只能2位小数', {icon: 0});
					  return false;
			    }*/
                if (!reg.test(express_trans_fee)) {
                    layer.alert('请输入快递数字格式,只能2位小数', {icon: 0});
                    return false;
                }
                /*if(!reg.test(ems_trans_fee))
				{
					  layer.alert('请输入平邮EMS格式,只能2位小数', {icon: 0});
					  return false;
			    }*/
            }
        }
    }
    return true;
}

//发布商品
function submitTable() {

    collectValueImage();//收集商品属性值图片信息

    // 规格属性
    var size = specMap.size();
    var attrSize = $("#J_SellProperties .sku-wrap ").children(".sku-group").length;
    if (size > 0) {
        if (!checkSkuParams()) {
            return false;
        }
    } else {
        if (!checkDefaultSku()) {
            //	layer.alert("您的信息输入有误，库存应为大于0的整数", {icon: 0});
            return false;
        }
    }
    //敏感词验证
    //标题
    var prodTitle = $("#prodTitle").val();
    var checkResult1 = checkSensitiveData(prodTitle);
    if (checkResult1 && checkResult1.length) {
        layer.msg("对不起, 您输入的标题内容包含 [" + checkResult1 + "] 等敏感词!", {icon: 0});
        return false;
    }
    //卖点
    var brief = $("#brief").val();
    var checkResult2 = checkSensitiveData(brief);
    if (checkResult2 && checkResult2.length) {
        layer.msg("对不起, 您输入的卖点内容包含 [" + checkResult2 + "] 等敏感词!", {icon: 0});
        return false;
    }
    //退换货承诺：
    if ($("#rpCheckbox").is(':checked')) {
        $("#rejectPromise").val(1);
    } else {
        $("#rejectPromise").val(0);
    }

    //服务保障：
    if ($("#sgCheckbox").is(':checked')) {
        $("#serviceGuarantee").val(1);
    } else {
        $("#serviceGuarantee").val(0);
    }

    //其他信息：开始时间
    var startDate = $("#year").val() + " " + $("#hours").val() + ":" + $("#minute").val() + ":00";//格式：2015-7-30 9:30:00
    $("#setUpTime").val(startDate);

    delCookie("specMap");//删除cookie

    if (valueAliasMap.size() > 0) {//用户自定义属性值名 赋值
        var valueAliasArray = JSON.stringify(valueAliasMap.values());
        $("#valueAlias").val(valueAliasArray);
    }

    $("#form1").submit();
}

//方法，判断是否为空
function isBlank(_value) {
    if (_value == null || _value == "" || _value == undefined || _value.length == 0) {
        return true;
    }
    return false;
}

function isMoney(obj) {
    if (!obj)
        return false;
    return (/^\d+(\.\d{1,2})?$/).test(obj);
}

//校验默认的SKU
function checkDefaultSku() {
    var errNum = 0;
    var p = {};
    var paramMap = [];
    var allStocks = 0;
    //组装SKU库存
    var partyCode = $(".sku-default .partyCode").val().trim();
    var modelId = $(".sku-default .modelId").val().trim();
    var volume = $.trim($(".sku-default .volume").val());
    var weight = $.trim($(".sku-default .weight").val());
    var stocks = $(".sku-default .stock").val().trim();
    var skuPic = "";
    if (stocks == "" || !isPositiveNum(stocks)) {
        $(".sku-default .stock").addClass("prod-edit-err");
        errNum++;
        stocks = 0;
        layer.alert("您的信息输入有误，库存应为大于0的整数", {icon: 2});
    }
    p.price = $("#cash").val();
    p.name = $("#prodTitle").val();
    p.stocks = stocks;
    p.actualStocks = stocks;
    p.status = 1;
    //p.wareProdList=skuWareArray;
    p.partyCode = partyCode;
    p.modelId = modelId;
    p.volume = volume;
    p.weight = weight;
    if (skuPic == "" || skuPic == null) {
        skuPic = $(".multimage-gallery ul .has-img div[class=preview] input[name='imgurl']").get(0).value;
    }
    p.pic = skuPic;
    paramMap[0] = p;
    var skuList = JSON.stringify(paramMap);
    $("#skuList").val(skuList);
    $("#stocks").val(stocks);//商品的库存等于所有SKU的库存总和
    if (errNum > 0) {
        return false;
    }
    return true;
}

//校验sku参数
function checkSkuParams() {
    var errNum = 0;
    var obj = new Object();
    var parm = {};
    var paramMap = [];
    var resultv = parseValue(specMap.sort().values());
    var allStocks = 0;
    for (var i = 0; i < resultv.length; i++) {
        var ids = resultv[i].toString().split("_");
        var p = {};
        var properties = "";
        var cnProperties = "";
        var skuPic = "";
        for (var j = 0; j < ids.length; j++) {
            var keyvalue = $("#sku_" + ids[j]).attr("propid");
            var skuvalue = $("#sku_" + ids[j]).attr("valueid");
            var pName = $("#sku_" + ids[j]).parents(".sku-group").find(".sku-label").attr("propName");
            var pVal = $("#sku_" + ids[j]).val();

            if (!isBlank(keyvalue) && !isBlank(skuvalue)) {
                properties += keyvalue + ":" + skuvalue + ";"
                cnProperties += pName + ":" + pVal + ";"
            }

            var propValueImgDto = valueImageMap.get(skuvalue);
            if (propValueImgDto != null) {
                skuPic = propValueImgDto.imgList[0];
            }
        }
        //去掉最后的分号
        if (!isBlank(properties)) {
            properties = properties.substring(0, properties.length - 1);
        }
        //去掉最后的分号
        if (!isBlank(cnProperties)) {
            cnProperties = cnProperties.substring(0, cnProperties.length - 1);
        }

        var row = "#row_" + resultv[i];

        var price = $.trim($(row + "_cash").val());//sku价格
        if (isBlank(price)) {
            $(row + "_cash").addClass("prod-edit-err");
            errNum++;
            layer.alert("请输入单品价格", {icon: 2});
            return false;
        }
        if (!isBlank(price)) {
            if (!isMoney($.trim(price)) || price == 0) {
                $(row + "_cash").addClass("prod-edit-err");
                errNum++;
                layer.alert("请输入正确的单品价格", {icon: 2});
                return false;
            }
        }

        var name = $.trim($(row + "_name").val());//sku名称
        if (isBlank(name)) {
            $(row + "_name").addClass("prod-edit-err");
            errNum++;
            layer.alert("请输入单品标题", {icon: 2});
            return false;
        }

        var partyCode = $.trim($(row + "_code").val());
        var modelId = $.trim($(row + "_model").val());
        var volume = $.trim($(row + "_volume").val());
        var weight = $.trim($(row + "_weight").val());

        var stock = $.trim($(row + "_stock").val());//sku库存
        if (isBlank(stock)) {
            $(row + "_stock").addClass("prod-edit-err");
            errNum++;
            layer.alert("请输入单品库存", {icon: 2});
            return false;
        }
        if (!isBlank(stock)) {
            if (!isPositiveNum(stock)) {
                $(row + "_stock").addClass("prod-edit-err");
                errNum++;
                layer.alert("请输入正确的单品库存", {icon: 2});
                return false;
            }
        }
        var status = $('input[name="pro_status_' + resultv[i] + '"]:checked ').val();

        //组装SKU库存
        if (skuPic == "" || skuPic == null) {
            skuPic = $(".multimage-gallery ul .has-img div[class=preview] input[name='imgurl']").get(0).value;
        }
        allStocks = Number(allStocks) + Number(stock);
        p.properties = properties;
        p.cnProperties = cnProperties;
        p.price = price;
        p.name = name;
        p.stocks = stock;
        p.actualStocks = stock;
        p.status = status;
        p.pic = skuPic;
        p.partyCode = partyCode;
        p.modelId = modelId;
        p.volume = volume;
        p.weight = weight;
        paramMap[i] = p;
    }
    $("#stocks").val(allStocks);//商品的库存等于所有SKU的库存总和
    parm.skuList = paramMap;
    var skuList = JSON.stringify(paramMap);
    $("#skuList").val(skuList);
    if (errNum > 0) {
        return false;
    }
    return true;
}

//提示弹框
function alertMessage(mess) {
    layer.alert(mess, {icon: 0});
}

//本地上传商品属性图片
function uploadPropImage(obj) {
    var valueImageUl = $(obj).parent().parent().find("ul");
    var valueImageLi = valueImageUl.find("li").get();
    var valueImageId = $(obj).attr("id");
    var propValueId = $(obj).parent().parent().prev().attr("propValueId");
    var propValueName = $(obj).parent().parent().prev().attr("propValueName");
    var propId = $(obj).parent().parent().prev().attr("propId");
    if (valueImageLi.length >= 6) {
        alertMessage("属性图片最多6张！");
        return;
    }else if(obj.files.length >= 7){
    	alertMessage("属性图片最多6张！");
    	return;
    } else {
        if (!checkImgType($("#" + valueImageId))) {
            return false;
        } else if (!checkImgSize($("#" + valueImageId), 500)) {
            return false;
        }
        $.ajaxFileUpload({
            url: paramData.contextPath + '/s/propImage/save',
            secureuri: false,
            fileElementId: valueImageId,
            dataType: 'json',
            success: function (data, status) {
                if (data == "fail") {
                    layer.alert("仅支持JPG、GIF、PNG、JPEG、BMP格式，大小不超过500k", {icon: 2});

                } else {
                    var p = data.split(",");
                    for (var i = 0; i < p.length; i++) {
                        var url = paramData.photoPath + p[i];
                        var img = '<li class="propValueImageLi" style="margin-right: 10px;">' +
                            '<input propId="' + propId + '" propValueName="' + propValueName + '" propvalueId="' + propValueId + '" value="' + p[i] + '" name="propValueImage" type="hidden"/>' +
                            '<img class="propValueImg" src="' + url + '" ondblclick="removeValueImage(this);"/></li>';
                        valueImageUl.append(img);
                        //清除上传文本框的数据
                        $("#"+valueImageId).val("")
                    }
                }
            }
        });
    }
}

//双击删除属性值图片
function removeValueImage(obj) {
    $(obj).parent().remove();
}

//收集商品属性值图片信息
function collectValueImage() {
    var valueImage = $("input[name='propValueImage']").get();
    valueImageMap = new Map();
    var propvalueId = "", ValueImageUrl = "",
        valueUrlList = null, propValueImgDto = null,
        propValueName = "", propId = "";

    if (valueImage.length > 0) {
        for (var i = 0; i < valueImage.length; i++) {
            propvalueId = $(valueImage[i]).attr("propvalueId");
            ValueImageUrl = $(valueImage[i]).val();
            propValueName = $(valueImage[i]).attr("propValueName");
            propId = $(valueImage[i]).attr("propId");

            propValueImgDto = valueImageMap.get(propvalueId);
            if (propValueImgDto == null) {
                propValueImgDto = new Object();
                valueUrlList = new Array();
                valueUrlList.push(ValueImageUrl);
                propValueImgDto.imgList = valueUrlList;
            } else {
                propValueImgDto.imgList.push(ValueImageUrl);
            }

            propValueImgDto.valueId = propvalueId;
            propValueImgDto.valueName = propValueName;
            propValueImgDto.propId = propId;
            valueImageMap.put(propvalueId, propValueImgDto);
        }

        //console.debug("valueImageMap:"+JSON.stringify(valueImageMap.values()));
        $("#valueImages").val(JSON.stringify(valueImageMap.values()));
    }
}

//设置cookie
function setCookie(cookieName, cookieValue) {
    //var d = new Date();
    //d.setTime(d.getTime() + (1*24*60*60*1000));
    //var expires = "expires="+d.toUTCString();
    //document.cookie = cookieName + "=" + cookieValue + "; " + expires;
    document.cookie = cookieName + "=" + cookieValue;
}

//获取cookie
function getCookie(cookieName) {
    var name = cookieName + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return "";
}

//删除cookies
function delCookie(cookieName) {
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cookieValue = getCookie(cookieName);
    if (cookieValue != null) {
        document.cookie = cookieName + "=" + cookieValue + ";expires=" + exp.toGMTString();
    }
}

//记录sku值
function recordSkuValue(obj) {
    var trId = $(obj).parents(".sku-p-d").attr("id");
    var skuId = trId.substring(4);
    //console.debug("skuId:"+skuId);
    var sku = skuMap.get(skuId);
    if (isBlank(sku)) {
        var sku = new Object();
    }

    var row = "#row_" + skuId;
    var price = $.trim($(row + "_cash").val());//sku价格
    if (isBlank(price)) {
        price = 0;
    }
    sku.price = price;

    var name = $.trim($(row + "_name").val());//sku名称
    if (isBlank(name)) {
        sku.name = name;
    }

    var code = $.trim($(row + "_code").val());//code名称
    if (isBlank(code)) {
        sku.code = code;
    }

    var status = $('input[name="pro_status_' + skuId + '"]:checked ').val();//状态
    sku.status = status;

    var partyCode = $.trim($(row + "_code").val());
    if (isBlank(partyCode)) {
        sku.partyCode = partyCode;
    }
    var modelId = $.trim($(row + "_model").val());
    if (isBlank(modelId)) {
        sku.modelId = modelId;
    }

    var volume = $.trim($(row + "_volume").val());
    if (isBlank(volume)) {
        sku.volume = volume;
    }

    var weight = $.trim($(row + "_weight").val());
    if (isBlank(weight)) {
        sku.weight = weight;
    }

    var stock = $.trim($(row + "_stock").val());
    if (isBlank(stock)) {
        sku.stock = stock;
    }

    var billPrice = $.trim($(row + "_billPrice").val());
    if (isBlank(billPrice)) {
        sku.billPrice = billPrice;
    }

    skuMap.put(skuId, sku);
    //console.log(skuMap);

}

// 异步加载 运费模板选项
function RefreshTp() {
    $.ajax({
        url: paramData.contextPath + "/s/transport/refresh",
        type: 'post',
        async: false, //默认为true 异步
        dataType: 'json',
        success: function (result) {
            if (!isBlank(result)) {
                $("#transportId").html("");
                $("#transportId").append('<option selected="selected" value="-1">请选择运费模板</option>');
                var kvArray = jQuery.parseJSON(result);
                var option;
                for (var i = 0; i < kvArray.length; i++) {
                    option = '<option value="' + kvArray[i].key + '">' + kvArray[i].value + '</option>';
                    $("#transportId").append(option);
                }
            }
        }
    });
}

//属性值 用户自定义名字
function propValueAlias(obj) {
    var alias = $(obj).val();
    if(alias==""){
        layer.alert("属性不能为空",{icon:0});
        $(obj).val(properties);
        return;
    }
    var skuItemLi = $(obj).parent().parent();
    var valueId = skuItemLi.find("input[type='checkbox']").attr("valueid");
    var valueAlia = valueAliasMap.get(valueId);
    if (isBlank(valueAlia)) {
        valueAlia = new Object();
    }
    valueAlia.alias = alias;
    valueAlia.valueId = valueId;

    valueAliasMap.put(valueId, valueAlia);
    // console.debug("valueAliasMap:"+JSON.stringify(valueAliasMap));

    $("#sku_" + valueId).val(alias);
    $("#labelName_" + valueId).html(alias);
    $("#labelInput_" + valueId).find("input").val(alias);

    var pvList = $(".pv_" + valueId).get();
    for (var d = 0; d < pvList.length; d++) {
        $(pvList[d]).html(alias);
    }
}

function IsPinYear(year) {//判断是否闰平年
    return (0 == year % 4 && (year % 100 != 0 || year % 400 == 0));
}

//日期选项 加载
function dateOption(maxLength) {
    //console.debug("new Date().getFullYear():"+new Date().getFullYear());
    //console.debug("new Date().getMonth():"+new Date().getMonth() + 1);
    var commonYear = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];//平年月份的总天数
    var LeapYear = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];//闰年月份的总天数
    var nowYear = new Date().getFullYear();
    var nowMonth = new Date().getMonth() + 1;
    var nowDay = new Date().getDate();
    var nowHours = new Date().getHours();
    var nowMinutes = new Date().getMinutes();

    var selectDays = $("#selectDays").val();
    var selectHours = $("#selectHours").val();
    var selectMinutes = $("#selectMinutes").val();

    var option = "", monthDays, hoursOption = "", minOption = "";

    var dateStr1 = "";//格式 yyyy-mm-dd
    var dateStr2 = "";//格式 yyyy年mm月dd日
    var temp1, temp2, temp3, temp4;//作为临时记录

    var index = 0, x, y, z, h, m;

    for (x = nowYear; x < nowYear + 2; x++) {
        dateStr1 = dateStr1 + x + "-";
        dateStr2 = dateStr2 + x + "年";
        temp1 = dateStr1;
        temp2 = dateStr2;

        if (IsPinYear(x)) { //true : 润年，false :平年
            monthDays = LeapYear;
        } else {
            monthDays = commonYear;
        }

        if (x == nowYear) {
            y = nowMonth;
        } else {
            y = 1;
        }
        for (y; y <= 12; y++) {
            dateStr1 = temp1 + y + "-";
            dateStr2 = temp2 + y + "月";
            temp3 = dateStr1;
            temp4 = dateStr2;

            if (y == nowMonth) {
                z = nowDay;
            } else {
                z = 1;
            }
            for (z; z <= monthDays[y - 1]; z++) {
                index = index + 1;
                if (index >= maxLength) {
                    break;
                }

                dateStr1 = temp3 + z;
                dateStr2 = temp4 + z + "日";

                if (!isBlank(selectDays)) {
                    if (selectDays == dateStr1) {
                        option = "<option value='" + dateStr1 + "' selected>" + dateStr2 + "</option>";
                    } else {
                        option = "<option value='" + dateStr1 + "'>" + dateStr2 + "</option>";
                    }
                } else {
                    option = "<option value='" + dateStr1 + "'>" + dateStr2 + "</option>";
                }

                $("#year").append(option);

                dateStr1 = "";
                dateStr2 = "";
            }

            dateStr1 = "";
            dateStr2 = "";
        }

    }

    for (h = 0; h <= 23; h++) {
        if (!isBlank(selectHours)) {
            if (selectHours == h) {
                hoursOption = "<option value='" + h + "' selected>" + h + "</option>";
            } else {
                hoursOption = "<option value='" + h + "'>" + h + "</option>";
            }
        } else {
            hoursOption = "<option value='" + h + "'>" + h + "</option>";
        }

        $("#hours").append(hoursOption);
    }

    for (m = 0; m <= 59; m++) {
        if (!isBlank(selectMinutes)) {
            if (selectMinutes == m) {
                minOption = "<option value='" + m + "' selected >" + m + "</option>";
            } else {
                minOption = "<option value='" + m + "'>" + m + "</option>";
            }
        } else {
            minOption = "<option value='" + m + "'>" + m + "</option>";
        }

        $("#minute").append(minOption);
    }
}

//加载 小时 和分钟
function timeOption(obj) {
    var nowYear = new Date().getFullYear();
    var nowMonth = new Date().getMonth() + 1;
    var nowDay = new Date().getDate();
    var nowHours = new Date().getHours();
    var nowMinutes = new Date().getMinutes();
    var h, m;

    var selectDay = $(obj).val();
    //console.debug("selectDay:"+selectDay);

    var nowDay = nowYear + "-" + nowMonth + "-" + nowDay;
    //console.debug("nowDay:"+nowDay);

    if (!isBlank(selectDay)) {
        if (selectDay == nowDay) {
            h = nowHours;
            m = nowMinutes;
        } else {
            h = 0;
            m = 0;
        }
    } else {
        //console.debug("selectDay is Blank");
        h = 0;
        m = 0;
    }

    $("#hours").html("");
    for (h; h <= 23; h++) {
        hoursOption = "<option value='" + h + "'>" + h + "</option>";
        $("#hours").append(hoursOption);
    }

    $("#minute").html("");
    for (m; m <= 59; m++) {
        minOption = "<option value='" + m + "'>" + m + "</option>";
        $("#minute").append(minOption);
    }
}

function showTips(ths) {
    $(ths).css("background-color", "green");
    $(ths).next().show();
}

function hideTips(ths) {
    $(ths).css("background-color", "#c2c2c2");
    $(ths).next().hide();
}

function onPasteHandler(ths, len) {
    setTimeout(function () {
        ths.value = ths.value.substring(0, len);
    }, 1);// 1ms should be enough
}

//切换到 单品信息
function panelNext() {
    if (checkBaseInfo()) {
        //重新绘画出相应的sku表
        var isEdit = $("#isEdit").val();
        if (isEdit == undefined || isEdit == null || isEdit == '') {
            paintingtSkuTable();
        }

        $("#panel-b-l").removeClass("active");
        $("#panel-base").hide();
        $("#panel-s-l").addClass("active");
        $("#panel-sku").show();
        $(".seller_right_nav").hide();
    }
}

//切换到 基本信息
function panelPrev() {
    $("#panel-b-l").addClass("active");
    $("#panel-base").show();
    $("#panel-s-l").removeClass("active");
    $("#panel-sku").hide();
    $(".seller_right_nav").show();
}

//切换sku
function changeSkuTab(ths) {
    $(ths).addClass("on");
    //移除同胞元素的class="on"
    $(ths).siblings().removeClass("on");
    var propValIds = $(ths).attr("propValIds");
    $("#row_" + propValIds).show();
    $("#row_" + propValIds).siblings().hide();
}

//显示批量设置库存
function displayBatchSetStock(obj) {
    $(obj).hide();
    $(obj).parent().find(".bat_set_inpt").show();
    $(obj).parent().find(".do_batch_set_stock_ctrl").show();
    $(obj).parent().find(".cancel_batch_set_stock_ctrl").show();
}

//隐藏批量设置库存
function hideBatchSetStock(obj) {
    $(obj).parent().find(".batch_set_stock_ctrl").show();
    $(obj).parent().find(".bat_set_inpt").hide();
    $(obj).parent().find(".do_batch_set_stock_ctrl").hide();
    $(obj).parent().find(".cancel_batch_set_stock_ctrl").hide();
}

//应用批量设置
function doBatchSetStock(obj) {
    var stocks = $(obj).parent().find(".bat_set_inpt").val();
    $(obj).parents(".ware-table").find(".val-i").val(stocks);
    hideBatchSetStock(obj);
}

//将此设置应用到其他单品
function copyWareToOtherSpec(obj) {
    var propvalueid = $(obj).attr("propvalueids");
    var name = $("#row_" + propvalueid).find("#row_" + propvalueid + "_name").val();
    var cash = $("#row_" + propvalueid).find("#row_" + propvalueid + "_cash").val();
//	var billPrice=$("#row_"+propvalueid).find("#row_"+propvalueid+"_billPrice").val();
    var code = $("#row_" + propvalueid).find("#row_" + propvalueid + "_code").val();
    var modelId = $("#row_" + propvalueid).find("#row_" + propvalueid + "_modelId").val();
    var stock = $("#row_" + propvalueid).find("#row_" + propvalueid + "_stock").val();

    //补充两个【物流体积、物流重量】
    var volume = $("#row_" + propvalueid).find("#row_" + propvalueid + "_volume").val();
    var weight = $("#row_" + propvalueid).find("#row_" + propvalueid + "_weight").val();

    var status = $("#row_" + propvalueid).find(":input[name='pro_status_" + propvalueid + "']:checked").val();
    var skuPanelList = $(obj).parents(".sku-p-d").siblings().get();
    for (var j = 0; j < skuPanelList.length; j++) {
        $(skuPanelList[j]).find(".title").val(name);
        $(skuPanelList[j]).find(".price").val(cash);
        //	$(skuPanelList[j]).find(".billPrice").val(code);
        $(skuPanelList[j]).find(".code").val(code);
        $(skuPanelList[j]).find(".modelId").val(modelId);
        $(skuPanelList[j]).find(".stock").val(stock);

        $(skuPanelList[j]).find(".volume").val(volume);
        $(skuPanelList[j]).find(".weight").val(weight);

        $(skuPanelList[j]).find("input[name^='pro_status_'][value='" + status + "']").prop("checked", "checked");
    }
    layer.msg("操作成功，已将此设置应用到其他单品！");
}

//是否为正整数 (包括0)
function isPositiveNum(s) {
    var re = /^[0-9]*[0-9][0-9]*$/;
    return re.test(s)
}


//敏感字检查
function checkSensitiveData(content) {
    var url = contextPath + "/sensitiveWord/filter";
    $.ajax({
        url: url,
        data: {"src": content},
        type: 'post',
        dataType: 'json',
        async: false, //默认为true 异步
        success: function (retData) {
            result = retData;
        }
    });
    return result;
}
