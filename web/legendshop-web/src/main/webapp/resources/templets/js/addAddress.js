/**
 * 判断是否是空
 * @param value
 */
function isEmpty(value){
	if(value == null || value == "" || value == "undefined" || value == undefined || value == "null"){
		return true;
	}
	else{
		value = (value+"").replace(/\s/g,'');
		if(value == ""){
			return true;
		}
		return false;
	}
}

function checkConsigneeName(){
	var errorFlag = false;
	var errorMessage ="";
	var value = $("#consigneeName").val();
	if (isEmpty(value)) {
		errorFlag = true;
		errorMessage = "请您填写收货人姓名";
	}else{
		if (value.length > 25) {
			errorFlag = true;
			errorMessage = "收货人姓名不能大于25位";
		}
		if (!is_forbid(value)) {
			errorFlag = true;
			errorMessage = "收货人姓名中含有非法字符";
		}
	}
	var el = $("#consigneeNameNote");
	if(errorFlag){
		el.text(errorMessage);
		el.show();
	}else{
		el.hide();
	}
	return errorFlag;
}

/**
 * 检查是否含有非法字符
 * @param temp_str
 * @returns {Boolean}
 */
function is_forbid(temp_str){
    temp_str = temp_str.replace(/(^\s*)|(\s*$)/g, "");
	temp_str = temp_str.replace('--',"@");
	temp_str = temp_str.replace('/',"@");
	temp_str = temp_str.replace('+',"@");
	temp_str = temp_str.replace('\'',"@");
	temp_str = temp_str.replace('\\',"@");
	temp_str = temp_str.replace('$',"@");
	temp_str = temp_str.replace('^',"@");
	temp_str = temp_str.replace('.',"@");
	temp_str = temp_str.replace(';',"@");
	temp_str = temp_str.replace('<',"@");
	temp_str = temp_str.replace('>',"@");
	temp_str = temp_str.replace('"',"@");
	temp_str = temp_str.replace('=',"@");
	temp_str = temp_str.replace('{',"@");
	temp_str = temp_str.replace('}',"@");
	var forbid_str = new String('@,%,~,&');
	var forbid_array = new Array();
	forbid_array = forbid_str.split(',');
	for(i=0;i<forbid_array.length;i++){
		if(temp_str.search(new RegExp(forbid_array[i])) != -1)
		return false;
	}
	return true;
}



/**
 * 检查手机号
 * @returns {Boolean}
 */
function checkMobile(){
	var el = $("#consigneeMobileNote");
	var errorFlag = false;
	var errorMessage ="";
	var value = $("#consigneeMobile").val();
	if (isEmpty(value)) {
		errorFlag = true;
		errorMessage = "请您填写收货人手机号码";
	} else {
		var regu = /^\d{11}$/;
		var re = new RegExp(regu);
		if(!(re.test(value) || new RegExp(/^\d{3}\*\*\*\*\d{4}$/).test(value))){
			errorFlag = true;
			errorMessage = "手机号码格式不正确";
		}
	}
	if (!errorFlag) {
		value = $("#consigneePhone").val();
		if($("#consigneeMobile").val() == $("#consigneePhone").val()){
			el.hide();
		    return false;
		 }
		if (!isEmpty(value)) {
			if (!is_forbid(value)) {
				errorFlag = true;
				errorMessage = "固定电话号码中含有非法字符";
			}
			if(value.length > 20){
				errorFlag = true;
				errorMessage = "固定电话号码过长";
			}
			var strlength=value.length;
			var patternStr = "(0123456789-*)";
		    for(var i=0;i<strlength;i++){
		        var tempchar=value.substring(i,i+1);
				if(patternStr.indexOf(tempchar)<0){
					errorFlag = true;
					errorMessage = "固定电话号码格式不正确";
					break;
				}
		    }
			if(strlength >=4 && value.indexOf("*") >-1){
				if(!((new RegExp(/.+\*\*\*\*$/).test(value) && (strlength - value.indexOf("*")) < 5) || (new RegExp(/^\d{11}$/).test(value) || new RegExp(/^\d{3}\*\*\*\*\d{4}$/).test(value)))){
					errorFlag = true;
					errorMessage = "固定电话号码格式不正确";
				}
			}
		}
	}
	
	if(errorFlag){
		 el.text(errorMessage);
		 el.show();
	}else{
		 el.hide();
	}
	return errorFlag;
}

function checkArea(){
	var errorFlag = false;
	var errorMessage="";
	var provinceId = $("#provinceid option:selected").val();
	var cityId = $("#cityid option:selected").val();
	var countyId = $("#areaid option:selected").val();
	// 验证地区是否正确
	if (isEmpty(provinceId) || isEmpty(cityId) || isEmpty(countyId) || provinceId==0 || cityId==0 || countyId==0) {
		errorFlag = true;
		errorMessage = "请您填写完整的地区信息";
	}
	var el = $("#areaNote");
	if(errorFlag){
		 el.text(errorMessage);
		 el.show();
	}else{
		 el.hide();
	}
	return errorFlag;
}


function checkConsigneeAddress(){
	var errorFlag = false;
	var errorMessage = "";
	var value = $("#consigneeAddress").val();
	if (isEmpty(value)) {
		errorFlag = true;
		errorMessage = "请您填写收货人详细地址";
	}
	if (!is_forbid(value)) {
		errorFlag = true;
		errorMessage = "收货人详细地址中含有非法字符";
	}
	if (value.length>100) {
		errorFlag = true;
		errorMessage = "收货人详细地址过长";
	}
	var el = $("#consigneeAddressNote");
	if(errorFlag){
		el.text(errorMessage);
		el.show();
	}else{
		el.hide();
	}
	return errorFlag;
}

/**检查手机号码或固定电话**/
function checkMobileAndPhone(){
	var errorFlag = false;
	var errorMessage ="";
	var mobileValue = $("#consigneeMobile").val(); //手机
	var phoneValue = $("#consigneePhone").val(); //固定电话
	
	if(isEmpty(mobileValue)&&isEmpty(phoneValue)){
		errorFlag = true;
		errorMessage = "请输入手机号码或固定电话";
	}else{
		if(!isEmpty(mobileValue)){
			var regu = /^[1][0-9]{10}$/;
			var re = new RegExp(regu);
			if(!re.test(mobileValue)){
				errorFlag = true;
				errorMessage = "手机号码格式不正确";
			}
		}
	}
	var el = $("#consigneeMobileNote");
	if(errorFlag){
		 el.text(errorMessage);
		 el.show();
	}else{
		 el.hide();
	}
	return errorFlag;
}
/**检查固定电话**/
function checkPhone(){
	var errorFlag = false;
	var errorMessage ="";
	var phoneValue = $("#consigneePhone").val(); //固定电话
	
		if(!isEmpty(phoneValue)){
			if (!is_forbid(phoneValue)) {
				errorFlag = true;
				errorMessage = "固定电话号码中含有非法字符";
			}
			if(phoneValue.length > 20){
				errorFlag = true;
				errorMessage = "固定电话号码过长";
			}
			var reg = /^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/;
			if(!reg.test(phoneValue)){
				errorFlag = true;
				errorMessage = "固定电话号码格式不正确";
			}
		}
		
	var el = $("#consigneePhoneNote");
	if(errorFlag){
		el.text(errorMessage);
		el.show();
	}else{
		el.hide();
	}
	return errorFlag;
}


/**
 * 校验邮箱
 */
function checkEmail(){
	var errorFlag = false;
	var errorMessage ="";
	var value = $("#consigneeEmail").val();
	if(!isEmpty(value)){
		if (value.length > 50) {
			errorFlag = true;
			errorMessage = "邮箱长度不能大于50位";
		}
	   var myReg=/(^\s*)\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*(\s*$)/;
	   var my=/^[0-9a-zA-Z_\-\.]{1,4}@\w+([-.]\w+)*\.\w+([-.]\w+)*(\s*$)/;
	   var reg=/^[0-9a-zA-Z_\-\.]{2}\*+[0-9a-zA-Z_\-\.]{2}@\w+([-.]\w+)*\.\w+([-.]\w+)*(\s*$)/;
	   if(!myReg.test(value) && !reg.test(value) && !my.test(value)){
		   errorFlag=true;
		   errorMessage = "邮箱格式不正确";
	   }
	}
	var el = $("#emailNote");
	if(errorFlag){
		el.text(errorMessage);
		el.show();
	}else{
		 el.hide();
	}
	return errorFlag;
}


/**
 * 邮政编码
 */
function checkPostCode(){
	var errorFlag = false;
	var errorMessage ="";
	var value = $("#consigneePostCode").val();
	if(!isEmpty(value)){
		if (value.length > 6) {
			errorFlag = true;
			errorMessage = "邮政编码长度不能大于6位";
		}
	   var myReg=/^[1-9][0-9]{5}$/;
	   if(!myReg.test(value)){
		   errorFlag=true;
		   errorMessage = "邮政编码格式不正确";
	   }
	}
	var el = $("#postCodeNote");
	if(errorFlag){
		el.text(errorMessage);
		el.show();
	}else{
		 el.hide();
	}
	return errorFlag;
}

/**
 * 保存地址
 */
function addUserAddress(){
    if(checkConsigneeName() || checkArea() || checkConsigneeAddress() || checkMobileAndPhone() || checkPhone() || checkEmail() || checkPostCode() ){
		return;
	}
	var consigneeName = $("#consigneeName").val();
	var consigneeAddress = $("#consigneeAddress").val();
	var mobile = $("#consigneeMobile").val();
	var telphone =  $("#consigneePhone").val();
	
	var provinceId = $("#provinceid option:selected").val();
	var cityId = $("#cityid option:selected").val();
	var areaid = $("#areaid option:selected").val();
	
    var email=$("#consigneeEmail").val();
	var postCode= $("#consigneePostCode").val();
	var aliasAddr=$("#aliasAddr").val();
	var commonAddr = $("#commonAddr").val();

	 var data = {
		"addrId": $("#id").val(),
		"receiver": consigneeName,
	 	"provinceId": provinceId,
	 	"cityId": cityId,
	 	"areaId": areaid,
	 	"subAdds": consigneeAddress,
	 	"mobile": mobile,
	 	"telphone": telphone,
	 	"email": email,
	 	"subPost": postCode,
	 	"aliasAddr":aliasAddr,
	 	"commonAddr":commonAddr
    };
    
    $.ajax({
		url:contextPath+"/p/saveUserAddress",
		data: data,
		type: "post", 
		dataType : "JSON", 
		async : true, //默认为true 异步   
		success:function(result){
			 if(result == 'OK'){
                 parent.window.reloadOrder();
		    }else if(result=='MAX'){
		    	 parent.layer.alert('收货地址不能超过20个!!',{icon:0});
			 }else{
				 parent.layer.alert('操作失败',{icon:2});
			 }
		}
	});
	 
}
  