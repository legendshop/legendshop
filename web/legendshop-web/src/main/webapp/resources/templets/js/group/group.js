$(document).ready(function() {
  //限制拼团价格
  $(".prodList").on("blur",".groupPrice",function () {
    if (this.value==null || this.value=="" || this.value<=0 || isNaN(this.value)){
      layer.alert("团购价格不能为空并且打大于0",{icon:2})
      $(this).css("background-color","pink");
      $(this).val("");
      return;
    }
    $(this).css("background-color","white");
  })

		userCenter.changeSubTab("groupManage");

		laydate.render({
			   elem: '#startTime',
			   calendar: true,
			   theme: 'grid',
			   type:'datetime',
			   min:'-1',
			   trigger: 'click'
		  });

		laydate.render({
			   elem: '#endTime',
			   calendar: true,
			   theme: 'grid',
			   type:'datetime',
			   min:'-1',
			   trigger: 'click'
	  });

		if(!isBlank(groupImage)){
			$("#groupPicFile").addClass("ignore");
		}

		jQuery.validator.addMethod("checkBalance", function(value,element) {
            var balance=0;
            return this.optional(element) || value>balance;
        }, $.validator.format("团购价格必须大于0 "));

		jQuery.validator.addMethod("prodPriceIsGroupPrice", function(value,element) {
			var prodPri = $("#prodCash").val();
			return (value*1.0)<(prodPri*1.0);
		}, $.validator.format("团购价格必须小于商品原价"));

		jQuery.validator.addMethod("stockShortage", function(value,element) {
			var stock =0; //库存量
      $(".prodStocks_p1").each(function () {
        stock+=parseFloat($(this).text());
      })
			var buyQuantity = $("#buyQuantity").val(); //每人限购
			//console.log("库存"+stock+"---成团人数"+value+"*"+value*buyQuantity*1.0);
			return (value*buyQuantity*1.0) <= (stock*1.0);
		}, $.validator.format("未选择商品或者商品库存不满足团购人数"));

		jQuery.validator.addMethod("checkExtName", function(value,element) {
			var fileNane = $("#groupPicFile").val().split('.');
			var extName = fileNane[fileNane.length - 1];
			var type = "jpg,gif,png,jpeg";
			return type.indexOf(extName)>-1?true:false;
        }, $.validator.format("您上传的文件格式有误，请重新上传！ "));

		jQuery.validator.addMethod("compareDate", function (value, element, param) {
		    var startDate = jQuery(param).val();
		    startDate= startDate.replace(/-/g, '/');
		    value = $(element).val().replace(/-/g, '/');

		    if (startDate == null || startDate == '' || value == null || value == '') {//只要有一个为空就不比较
		        return true;
		    }
		    var date1 = new Date(startDate);
		    var date2 = new Date(value);
		    return date1.getTime() < date2.getTime();

		}, '结束时间必须大于开始时间');

	    jQuery("#form1").validate({
	    		ignore: ".ignore",
	    		errorPlacement: function(error, element) {
	    			element.parent().find("em").html("");
	    			error.appendTo(element.parent().find("em"));
	    		},
	            rules: {
		             groupName: { required: true },
		             productId: { required: true },
		             categoryName: { required: true },
	           		 groupPicFile: { required: true, checkExtName: true },
	           		 startTime: { required: true },
	           		 endTime: { required: true, compareDate: "#startTime" },
	           		 buyQuantity: { required: true,digits:true},
	           		 // groupPrice: { required: true, checkBalance:true,prodPriceIsGroupPrice:true },
	           		 peopleCount: { required: true, digits:true,stockShortage:true }
	       		 },
	       		 messages: {
           			 groupName: {
	               		 required: "团购活动名称不能为空"
	           		 },productId: {
	               		 required: "商品不能为空"
	           		 },categoryName: {
	               		 required: "团购分类不能为空"
	           		 },groupPicFile: {
	               		 required: "团购活动图片不能为空"
	           		 },startTime: {
	               		 required: "开始时间不能为空"
	           		 },endTime: {
	               		 required: "结束时间不能为空"
	           		 },buyQuantity: {
	               		 required: "限购数量不能为空",
	               		 digits: "限购数量只能为正整数",
	           		 },peopleCount: {
		           		required: "成团人数不能为空",
		           		digits:"必须输入正整数"
	           		 }
               // ,groupPrice: {
               //   required: "团购价格不能为空"
               // }
	       	  },
	          submitHandler: function (form) {
         			var prodSize=$(".groupProdId").size();
              if(prodSize<0){
                layer.msg('商品不能为为空', {icon: 0});
                return false;
              }

              $(".prodList").find(".groupPrice").each(function(index,element){
                $(element).attr("name","groupProductList["+index+"].groupPrice");
                $(element).next("input[class='groupProdId']").attr("name","groupProductList["+index+"].prodId");
                $(element).next().next("input[class='groupSkuId']").attr("name","groupProductList["+index+"].skuId");
                $(element).next().next().next("input[class='groupSkuPrice']").attr("name","groupProductList["+index+"].skuPrice");
                $(element).rules("add",{required:true,messages: {required:"必填"} });
                $(element).rules("add",{number:true,messages: {number:"请输入数字"} });
                $(element).rules("add",{maxlength:8,messages: {maxlength:"最多输入八位数"} });
                $(element).rules("add",{range:[1,99999],messages: {range:"输入值在1-99999"} });
              });



				   var time = new Date($("#startTime").val().replace(/-/g,"/"));

				   //开始时间不能小于等于当前时间
				   if(time.getTime() <= new Date().getTime()){
				 	  layer.alert("活动开始时间不能小于当前时间!", {icon:0});
					  return false;
				   }
				   $("#submitGroup").attr("disabled", "disabled");
				   form.submit();
				}
	    });
});


function showProdlist(){
	layer.open({
		  title :"选择商品",
		  id: "showProdlist",
		  type: 2,
		  resize: false,
		  content: [contextPath+"/s/group/groupProdLayout",'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['850px', '660px']
		});
}


//加载分类用于选择
function loadCategoryDialog() {
	layer.open({
		  title :"选择分类",
		  id: "prodCat",
		  type: 2,
		  resize: false,
		  content: [contextPath+"/s/group/loadAllCategory/G",'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['280px', '385px']
		});
}

function addProdTo(prodId){
	var rs = isAttendActivity(prodId);
	if(prodId==null||prodId==""||prodId==undefined){
	    layer.alert('请选选择商品', {icon: 2});
	}
	if(rs != "false"){
		layer.alert("对不起,您选择的商品已参加"+rs+"!", {icon:0});
		return false;
	}
	$("#productId").val(prodId);
 	var url=contextPath+"/s/group/groupProd/"+prodId;

 	// 加载商品列表回显到页面
	$(".prodList").load(url, function(){
		layer.closeAll();
	});
  parent.$(".check-bto").show();
  parent.$(".check-bto").css("display","inline-block");
}

function isAttendActivity(prodId){
    var flag = "false";
	$.ajax({
		url : contextPath + "/s/group/isAttendActivity/" + prodId ,
		async : false,
		dataType : "JSON",
		success : function(result){
			flag = result;
		}
	});
	return flag;
}

function loadCategoryCallBack(firstCatId,secondCatId,thirdCatId, catName) {
	if(!isBlank(firstCatId)){
	 $("#firstCatId").val(firstCatId);
	}
	if(!isBlank(secondCatId)){
	 $("#secondCatId").val(secondCatId);
	}
	if(!isBlank(thirdCatId)){
	 $("#thirdCatId").val(thirdCatId);
	}
	$("#categoryName").val(catName);
}

function checkInputNumForNormal(obj, num){
	obj.value = obj.value.replace(/[^\d.]/g, "");// 清除“数字”和“.”以外的字符
	obj.value = obj.value.replace(/^\./g, "");// 验证第一个字符不是.
	obj.value = obj.value.replace(/\.{2,}/g, ".");// 只保留第一个. 清除多余的.
	obj.value = obj.value.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");// 保证.只出现一次，而不能出现两次以上
	if (num == 4){
		obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d\d\d).*$/,'$1$2.$3');// 只能输入两个小数
	}
	else{
		obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');// 只能输入两个小数
	}
	if(obj.value ==  undefined || obj.value==null || obj.value==""){
		return;
	}
	var tempStr = new String(obj.value);
	if(tempStr.indexOf('.') == -1){
	    var valss = parseInt(obj.value);
	    obj.value = Math.round(valss * Math.pow(10, num)) / Math.pow(10, num) ;
	}
}


//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
	return true;
	}
	return false;
}

//批量修改价格
$("#confim").click(function(){
  var _val = $("#batchPrice").val();

  if (isNaN(_val)||_val<=0) {
    layer.alert("团购价必须大于0",{icon:0})
    $("#batchPrice").val("");
    return;
  }

  if($(".sku").size()<=0){
    layer.alert("请选择商品",{icon:0});
    return;
  }
  if(isBlank(_val)){
    layer.alert("请选择输入团购价",{icon:0});
    return;
  }

  if($(".selectOne:checked").length<=0){
    layer.alert("请勾选商品",{icon:0});
    return;
  }

  $(".selectOne:checked").each(function(index,element){
    if($(element).is(':checked')){//选中，改变价格
      $(element).parents(".sku").find(".groupPrice").val(_val);
    }
  })
  $("#batchPrice").val("");
});

//全选
$(".prodList").on("click",".selectAll",function(){
  if($(this).attr("checked")=="checked"){
    $(this).parent().parent().addClass("checkbox-wrapper-checked");
    $(".selectOne").each(function(){
      $(this).attr("checked",true);
      $(this).parent().parent().addClass("checkbox-wrapper-checked");
    });
  }else{
    $(".selectOne").each(function(){
      $(this).attr("checked",false);
      $(this).parent().parent().removeClass("checkbox-wrapper-checked");
    });
    $(this).parent().parent().removeClass("checkbox-wrapper-checked");
  }
});


