function submitForm() {
	$(this).css('pointer-events', 'none');
	var count=$.trim($("#createnum").val());
	if(count==null || count==undefined || count==""){
		$(this).css('pointer-events', 'auto');
		layer.msg("请输入发放数量!",{icon:0});
		return false;
	}else if(count==0){
		$(this).css('pointer-events', 'auto');
		layer.msg("发放数量>0!",{icon:0});
		return false;
	}
	var re = /^[0-9\s]*$/;
	if (!re.test(count)){
		$(this).css('pointer-events', 'auto');
		layer.alert("请输入正确的格式!",{icon:0});
		$("#createnum").focus();
		return false;
	}
	$(".editAdd_load").show();
	jQuery.ajax({
		url:contextPath+"/s/coupon/sendForOffLine/${couponId}", 
		type:'post', 
		data: {"count":count},
		async : false, //默认为true 异步   
		dataType : "json", 
		error: function(jqXHR, textStatus, errorThrown) {
			$(".shadowBoxWhite").hide();
			$(this).css('pointer-events', 'auto');
			layer.alert("礼券发放失败",{icon:2});
			return ;
		},
		success:function(retData){
			var cToObj=JSON.parse(retData); 
			if(!cToObj.success){
				$(this).css('pointer-events', 'auto');
				layer.alert(cToObj.msg,{icon:2});
				$(".editAdd_load").hide();
			}
			else{
				layer.alert("成功发放[" + count + "]张礼券",{icon:1},function(){
					$(".editAdd_load").hide();
					parent.window.location.href=contextPath+"/s/coupon/query";
				});
			}
		}
	});
};