function deliverGoodsAddressPager(curPageNO){
	userCenter.loadPageByAjax( "/s/deliverGoodsAddress?curPageNO=" + curPageNO);
} 
$(document).ready(function() {
	userCenter.changeSubTab("deliveryAddr");
});

function alertDeliverAddAddressDiag(id){

	layer.open({
		title :"添加发货地址",
		id: "alertDeliverAddAddressDiag",
		type: 2, 
		resize: true,
		content: [contextPath +  '/s/loadDeliverGoodsAddress/' + id,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		area: ['650px', '350px']
	}); 
}

function makeDeliverAddressDefault(id){
	layer.confirm("确定要将该地址设置为默认地址？",{
		icon: 3
		,btn: ['确定','取消'] //按钮
	}, function () {
		$.ajax({
			"url":contextPath + "/s/defaultDeliverAddress/"+ id , 
			type:'post', 
			async : true, //默认为true 异步   
			success:function(result){
				layer.msg('设置成功！', {icon: 1,time:700},function(){
					refreshAddress();
				});

			}
		});
	});
}

function delDeliverAddress(id){
	layer.confirm("确定要删除发货地址吗？",{
		icon: 3
		,btn: ['确定','取消'] //按钮
	}, function () {
		$.ajax({
			url:contextPath + "/s/deliverGoodsAddr/"+ id , 
			type:'post', 
			async : true, //默认为true 异步   
			success:function(result){
				layer.msg('删除发货地址成功！', {icon: 1,time:700},function(){
					refreshAddress();
				});


			}
		});
	});
}

//刷新页面
function refreshAddress(){
	window.location.href=contextPath+"/s/deliverGoodsAddress";
}