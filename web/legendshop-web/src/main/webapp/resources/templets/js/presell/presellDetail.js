var presellStatus = 0;//记住预售活动状态,0:未开始,1:正在进行,2:已结束
var isStocks = false;//记住当前选中的sku是否有库存,默认没有库存
if (percent!="1"){
  var percentNum = parseFloat(percent/100);//定金百分比
}
jQuery(document).ready(function($) {

	//点击SKU项
	$("ul[id^='prop_'] li:not(.tb-selected):not(.tb-out-of-stock)").live("click", function() {
		clickPropVal(this);
		if(_switch==1){
			showSeckill(); //
		}
	});
	//初始化预售状态
	initPresellStatus();

	$(".prop_ul .li-show").on("click", function() {
		clickPropVal(this);
	});
	//店铺分数
	var shopScore=$("#shopScoreNum").text();
	if(shopScore==null){
		shopScore=0;
	}
	$(".scroll_red").css("width",(shopScore/5)*100+"%");

	//配送地址 hover
	$("#storeSelector").hover(function(){
		openSelector(this);
	},function(){
		closeStoreSelector();
	})

	// 配送至 选择框 省份选择
	jQuery("#stock_province_item ul[class=area-list] li").live("click",
		function() {
			var provinceId = $(this).find("a").attr("data-value");
			var province = $(this).find("a").html();
			$("#stockTab li[data-index=0] a em").html(province);
			$("#stockTab li[data-index=0]").attr("provinceid",provinceId);

			jQuery.ajax({
				url : contextPath + "/load/cityList/" + provinceId,
				type : 'post',
				async : true, // 默认为true 异步
				success : function(result) {
					$("#stock_city_item").html(result);

					$("#stockTab li").each(function(i) {
						$(this).removeClass("curr");
						$(this).find("a").removeClass("hover");
					});

					var cityTab = $("#stockTab li[data-index=1]");
					cityTab.addClass("curr");
					cityTab.find("a").addClass("hover");
					cityTab.find("em").html("请选择");
					cityTab.attr("style", "display: block;");

					$("#JD-stock div[class=mc]").each(function() {
						$(this).attr("style", "display: none;");
					});
					$("#stock_city_item").attr("style",
						"display: block;");
				}
			});
		}
	);

	// 配送至 选择框 城市选择
	jQuery("#stock_city_item ul[class=area-list] li").live("click",function() {
			var cityId = $(this).find("a").attr("data-value");
			var city = $(this).find("a").html();
			$("#selCityId").val(cityId);
			$("#stockTab li[data-index=1] a em").html(city);

			jQuery.ajax({
				url : contextPath + "/load/areaList/" + cityId,
				type : 'post',
				async : true, // 默认为true 异步
				success : function(result) {
					$("#stock_area_item").html(result);

					$("#stockTab li").each(function(i) {
						$(this).removeClass("curr");
						$(this).find("a").removeClass("hover");
					});

					var areaTab = $("#stockTab li[data-index=2]");
					areaTab.addClass("curr");
					areaTab.find("a").addClass("hover");
					areaTab.find("em").html("请选择");
					areaTab.attr("style", "display: block;");

					$("#JD-stock div[class=mc]").each(function() {
						$(this).attr("style", "display: none;");
					});
					$("#stock_area_item").attr("style",
						"display: block;");
				}
			});
		}
	);

	// 配送至 选择框 地区选择
	jQuery("#stock_area_item ul[class=area-list] li").live("click",function() {
			var areaId = $(this).find("a").attr("data-value");

			var area = $(this).find("a").html();
			var city = $("#stockTab li[data-index=1] a em").html();
			var province = $("#stockTab li[data-index=0] a em").html();
			var text = province + " " + city + " " + area;

			$("#stockTab li[data-index=2] a em").html(area);

			$("#storeSelector div[class=text] div").html(text);
			$("#storeSelector div[class=text] div").attr("title",
				text);
			$("#storeSelector").removeClass("hover");

			$("#selProvinceId").val($("#stockTab li[data-index=0]").attr("provinceid"));
			//计算运费
			calProdFreight();
		}
	);

	//计算运费
	calProdFreight();

	//获取商品浏览记录
	$.post(contextPath + "/visitedprod_old", function(retData) {
		$("#visitedprod").html(retData);
	}, 'html');
});

//获取系统当前时间(毫秒数)
function getCurrTime(){
	return new Date().getTime();
}

//初始化预售活动状态
function initPresellStatus(){

	//初始化价格
	var presellPrice = $("#presellPrice"); // 预售价
	var preDepositPrice = $("#preDepositPrice");// 定金价
	var finalPayment = $("#finalPayment"); // 尾款
	var preVal = parseFloat(presellPrice.text()*percentNum)
	var finalPayVal = parseFloat(presellPrice.text()-preVal);
	preDepositPrice.text(numberConvert(preVal));
	finalPayment.text(numberConvert(finalPayVal));
	//获取系统当前时间(毫秒)
	var currTime = getCurrTime();

	if(currTime>=endTime){//预售活动结束
		endPresell();
	}else if(currTime < startTime){//预售准备开始
		readyPresell();
	}else{//预售正在进行中
		doingPresell();
	}
}

//活动准备开始
function readyPresell(){
	presellStatus = 0;
	//处理距离开始的倒计时
	countdown(startTime,doingPresell);
	$("#activityStatus").show();
	$("#activityStatus .status-label").text("距离活动开始: ");
	$("#J_LinkBasket").text("活动即将开始...");
	$("#J_LinkBasket").attr("class","tb-btn-off");
}

//活动正在进行
function doingPresell(){
	presellStatus = 1;
	//处理距离结束的倒计时
	countdown(endTime,endPresell);

	$("#activityStatus").show();
	$("#activityStatus .status-label").text("距离活动结束: ");
	if(payPctType == 0){//全额支付
		$("#J_LinkBasket").text("立即全额支付");
		$("#J_LinkBasket").attr("onclick","payment()");
	}else if(payPctType == 1){//定金支付
		$("#J_LinkBasket").text("立即支付定金");
		$("#J_LinkBasket").attr("onclick","payment()");
	}
	$("#J_LinkBasket").attr("class","tb-btn-on");
}

//活动已结束
function endPresell(){
	presellStatus = 2;
	$("#activityStatus").hide();
	$("#J_LinkBasket").text("活动已结束");
	$("#J_LinkBasket").attr("class","tb-btn-off");
}

//处理倒计时
function countdown(time,callBack){
	var date = new Date(time);

	$("#activityStatus .status-time").countdown(date,function(event) {
		$(this).html(event.strftime('<span><em>%D</em>天<em>%H</em>时<em> %M</em>分<em> %S</em>秒</span>'));
	}).on('finish.countdown',function(){
		callBack();
	});
}

//支付
function payment(){
	//判断是否登录
	if(isLogin()){
		login();
		return;
	}
	var pamount = $.trim($("#pamount").val());
	if(isEmpty(pamount)){
		return;
	}
	if(!validateInput()){
		return;
	}
	$("#J_LinkBasket").attr("disabled","disabled");
	$.ajax({
		//提交数据的类型 POST GET
		type:"POST",
		//提交的网址
		url: contextPath+"/p/presell/order/checkPresell/"+presellId,
		timeout : 10000, //超时时间设置，单位10秒
		data:{"number":pamount,"skuId":currSkuId},
		//提交的数据
		async : false,
		//返回数据的格式
		datatype: "json",
		//成功返回之后调用的函数
		success : function(data) {
			var result=jQuery.parseJSON(data);
			var isExpose=result.expose;
			if(isExpose){
				submitForm(contextPath+"/p/presell/order/details/"+presellId+"/"+payPctType+"/"+currSkuId);
				return;
			}else{
				if(result.msg=="NOT_STOCK"){
					layer.alert("预售商品库存不足,请选择其他预售商品",{icon: 0});
				}else{
					$("#J_LinkBasket").removeAttr("disabled");
					layer.alert(result.msg,{icon: 2});
				}
				return;
			}
		},
		//调用执行后调用的函数
		complete: function(XMLHttpRequest, textStatus){

		},
		//调用出错执行的函数
		error: function(XMLHttpRequest, textStatus){
			if(textStatus=='timeout'){ // 超时,status还有success,error等值的情况
			}else{
				layer.alert("系统繁忙,请稍候！",{icon: 2});
			}
		}
	});
}


function submitForm(action) {
	var temp = document.createElement("form");
	temp.action = action;
	temp.method = "post";
	temp.target='_self';
	temp.style.display = "none";
	// var input1 = document.createElement("input");
	// input1.name = "skuId";
	// input1.value = params;
	// temp.appendChild(input1);
	temp.appendChild(appendCsrfInput());
	document.body.appendChild(temp);
	temp.submit();
	return temp;
}


function openSelector(obj) {
	var left = jQuery(obj).offset().left - 100;
	var top = jQuery(obj).offset().top + 25;
	var closeLeft = jQuery(obj).offset().left + 312;
	var closeTop = jQuery(obj).offset().top + 22;
	jQuery(obj).find("div[class=content]").attr("style","top:" + top + "px;left:" + left + "px;");
	jQuery(obj).find("div[class=close]").attr("style","top:" + closeTop + "px;left:" + closeLeft + "px;");
	jQuery(obj).addClass("hover");
}

function closeStoreSelector() {
	var obj = $(".addrselector");
	obj.removeClass("hover");
}
//选择省份
function selectProvince(obj) {
	$("#stockTab li").each(function() {
		$(this).removeClass("curr");
		$(this).find("a").removeClass("hover");
		$(this).attr("style", "display:none");
	});

	$(obj).addClass("curr");
	$(obj).find("a").addClass("hover");
	$(obj).find("em").html("请选择");
	$(obj).attr("style", "display:block");

	$("#JD-stock div[class=mc]").each(function() {
		$(this).attr("style", "display: none;");
	});
	$("#stock_province_item").attr("style", "display: block;");

}
//选择城市
function selectCity(obj) {
	$("#stockTab li").each(function(i) {
		$(this).removeClass("curr");
		$(this).find("a").removeClass("hover");
	});
	$("#stockTab li[data-index=2]").attr("style", "display: none;");
	$(obj).addClass("curr");
	$(obj).find("a").addClass("hover");
	$(obj).find("em").html("请选择");

	$("#JD-stock div[class=mc]").each(function() {
		$(this).attr("style", "display: none;");
	});
	$("#stock_city_item").attr("style", "display: block;");
}

//获取商品属性
function getProdAttr() {
	var prodattr = "";
	var errMsg = "";
	var attrselect = $(".attrselect");
	for ( var i = 0; i < attrselect.size(); i++) {
		if (attrselect.get(i).value == '') {
			errMsg = errMsg + " " + attrselect.get(i).getAttribute("dataValue");
		} else {
			prodattr = prodattr + attrselect.get(i).getAttribute("dataValue")
				+ ":" + attrselect.get(i).value + ";";
		}

	}
	if (errMsg != "") {
		prodattr = "error" + errMsg;
	}
	return prodattr;
}

//增加购买数量
function addPamount() {
	var pamount = $("#pamount").val();
	$("#pamount").val(pamount * 1 + 1);
}

//减少购买数量
function reducePamount() {
	var pamount = $("#pamount").val();
	if (pamount <= 1) {
		$("#pamount").val(1);
	} else {
		$("#pamount").val(pamount * 1 - 1);
	}
}

//得判断是否为数字的同时，也判断其是不是正数
function validateInput() {
	var pamount = $("#pamount").val();
	var re = /^[1-9]+[0-9]*]*$/;
	if (isNaN(pamount) || !re.test(pamount)) {
		//错误时
		$("#pamount").attr("style", "border: 1px solid #f40000;");
		$("#pamountError").html("请输入正确的数量！");
		$("#pamountError").removeClass("hide");
		return false;
	} else {
		//正确时
		$("#pamount").attr("style", "");
		$("#pamountError").html("");
		$("#pamountError").attr("class", "classic-error hide");
		return true;
	}

}

//计算运费
function calProdFreight(){
	if(support_transport_tree == '' || support_transport_tree == null || support_transport_tree == undefined){
		return ;
	}
	if(support_transport_tree == 1){
		$("#freightText").html("商家承担运费");
	}else if(support_transport_tree == 0){//买家承担运费

		if(transport_type == 0){//运费模版
			transport();
		}else if(transport_type == 1){//固定运费
			var str = "<select class='frei_sel'><option>EMS:"+ems_trans_fee+"</option><option>快递:"+express_trans_fee+"</option><option>平邮:"+mail_trans_fee+"</option></select>";
			$("#freightText").html(str);
		}

	}
	//根据省份计算库存
	calSkuStocks();
}

//获取运费模板的运费
function transport(){

	if(weight==""){
		weight=0;
	}
	if(volume==""){
		volume=0;
	}
	$.ajax({
		url : contextPath + "/getProdFreight",
		data : {
			"shopId" : shopId,
			"transportId" : transportId,
			"cityId" : $("#selCityId").val(),
			"totalCount" : 1,
			"totalWeight" : weight,
			"totalvolume" : volume
		},
		type : "POST",
		async : true, //默认为true 异步
		dataType : "JSON",
		success : function(retData) {
			if(retData == ""){
				$("#freightText").html("");
			}else{

				var fList = eval(retData);
				var str = "<select class='frei_sel'>";
				for(var i=0;i<fList.length;i++){
					str+="<option>";
					str+=fList[i].desc;
					str+="</option>";
				}
				str += "</select>";
				$("#freightText").html(str);
			}
		}
	});
}

//根据省份计算库存
function calSkuStocks(){
	jQuery.ajax({
		url : contextPath + "/calSkuStocksByProv",
		data : {
			"skuId" : $("#currSkuId").val(),
			"provinceId" : $("#selProvinceId").val(),
			"prodId":currProdId
		},
		type : 'post',
		async : true, //默认为true 异步
		dataType : 'json',
		success : function(result) {
			var stocksMsg = null;
			var retData = result.stocks;
			if(retData==null || retData=="" || retData==0){//无货
				isStocks = false;
				if(presellStatus == 1){//如果预售正在进行
					$("#J_LinkBasket").attr("class","tb-btn-off");
					$("#J_LinkBasket").attr("onclick","");
				}
				//隐藏立即支付按钮
				stocksMsg = "<span style='color:#e5004f'>无货&nbsp;&nbsp;&nbsp;</span>";
			}else{
				isStocks = true;
				if(presellStatus == 1){//如果预售正在进行
					$("#J_LinkBasket").attr("class","tb-btn-on");
					$("#J_LinkBasket").attr("onclick","payment()");
				}
				if(retData<10){
					stocksMsg = "有货，仅剩"+retData+"件&nbsp;&nbsp;&nbsp;";
				}else{
					stocksMsg = "有货&nbsp;&nbsp;&nbsp;";
				}
			}
			$("#stocksText").html(stocksMsg);
		}
	});
}

//收藏店铺
function collectShop(shopId) {
	jQuery.ajax({
		url : contextPath + "/isUserLogin",
		type : 'get',
		async : false, // 默认为true 异步
		success : function(result) {
			if(result=="true"){
				jQuery.ajax({
					url : contextPath + "/p/favoriteShopOverlay?shopId="+shopId,
					async : false, // 默认为true 异步
					dataType : 'json',
					success : function(result) {
						if(result=="fail"){
							login();
						}else if(result=="OK"){
							layer.alert("收藏店铺成功",{icon: 1});
						}else if(result="isexist"){
							layer.msg("您已收藏该店铺，无须重复收藏",{icon: 0});
						}
					}
				});
			}else{
				login();
			}
		}
	});

}

//关注商品
function alertAddInterestDiag(prodId) {
	jQuery.ajax({
		url : contextPath + "/isUserLogin",
		type : 'get',
		async : false, // 默认为true 异步
		success : function(result) {
			if(result=="true"){
				layer.open({
					title :"商品收藏",
					type: 2,
					content: contextPath + '/loadInterestOverlay/' + prodId, //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
					area: ['440px', '510px']
				});
			}else{
				login();
			}
		}
	});
}

//请问是分销吗?
function distProd(){
	jQuery.ajax({
		url : contextPath + "/isUserLogin",
		type : 'get',
		async : false, // 默认为true 异步
		success : function(result) {
			if(result=="true"){
				$(".dist_action").hide();
				$("#distShareBtn").show();
			}else{
				login();
			}
		}
	});
}

//得判断是否已登录
function isLogin() {
	if(isEmpty(loginUserName)){
		return true;
	}
	return false;
}

//弹出登录窗口
function login(){
	layer.open({
		title :"登录",
		type: 2,
		content: contextPath + '/loadLoginOverlay', //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		area: ['440px', '530px']
	});
}

/**
 * 判断是否是空
 * @param value
 */
function isEmpty(value){
	if(value == null || value == "" || value == "undefined" || value == undefined || value == "null"){
		return true;
	}
	else{
		value = (value+"").replace(/\s/g,'');
		if(value == ""){
			return true;
		}
		return false;
	}
}

// /**
//  * 点击属性值
//  */
// function clickPropVal(obj){
//
//     //当属性值之前没有被选中时
//     if($(obj).children("i").length<=0){
//         //把当前值 改为选中状态
//         $(obj).addClass("li-selected").siblings().removeClass("li-selected");
//         $(obj).addClass("li-selected").siblings().children("i").remove();
//         $(obj).append("<i></i>");
//         $("#pamount").val("1");
//
//     }
// }

//点击SKU项
function clickPropVal(obj){
	$(obj).addClass("tb-selected").siblings().removeClass("tb-selected");
	var propIndex = $(obj).parent().attr("index");
	handleSkuMessage(propIndex);
	getPropValImgs($(obj).attr("valId"));
}


/**
 * 处理sku信息
 */
function handleSkuMessage(propIndex){
	propIndex = Number(propIndex);
	var id = Number(propIndex);
	while($("#prop_"+id).length!=0){
		var aList = $("#prop_"+id+" li").get();
		for(var i=0;i<aList.length;i++){
			var kv = $(aList[i]).attr("data-value"); //data-value="269:831"
			var has = false;
			var newSkus = getSelectedSkus(id-1);
			for(var j=0;j<newSkus.length;j++){
				if(newSkus[j].properties.indexOf(kv)>=0 && newSkus[j].status==1){
					if(Number(newSkus[j].skuStock)>0){
						has = true;
						break;
					}
				}
			}
			if(!has){
				$(aList[i]).removeClass("tb-selected");
				$(aList[i]).addClass("tb-out-of-stock");
			}else{
				$(aList[i]).removeClass("tb-out-of-stock");
			}
		}
		id++;
	}
	$(".properties dl").removeClass("no-selected");
	//判断选择的SKU 是否与预定的SKU项一致
	if($(".tb-selected").length==$(".properties .tb-metatit").length){
		var kvList = [];
		var selectedList = $(".tb-selected").get();
		for(var k=0;k<selectedList.length;k++){
			kvList[k]=$(selectedList[k]).attr("data-value");
		}
		for(var i=0;i<skuDtoList.length;i++){
			var isT = true;
			for(var j=0;j<kvList.length;j++){
				if(skuDtoList[i].properties.indexOf(kvList[j])<0){
					isT = false;
					break;
				}
			}
			if(isT){
				currSkuId=skuDtoList[i].skuId;
				if(!isEmpty(skuDtoList[i].name)){
					$("#prodName").html(skuDtoList[i].name);
				}else{
					$("#prodName").html(prodName);
				}
				var prePrice=Number(skuDtoList[i].prePrice); //预售价
				var cash=Number(skuDtoList[i].price); //商品商城价
        $("#presellPrice").html(prePrice.toFixed(2));
        $("#cash").html(cash.toFixed(2));
        if (percent!="1"){
          var preDepositPrice = parseFloat(prePrice*percentNum); //定金
          var finalPayment = parseFloat(prePrice-preDepositPrice);//尾款
          $("#preDepositPrice").text(numberConvert(preDepositPrice));
          $("#finalPayment").text(numberConvert(finalPayment));
        }else{
          $(".onePrice").text(numberConvert(prePrice));
        }

				break;
			}
		}
		allSelected=true;
	}else{
		var dlList = $(".properties dl").get();
		for(var i=0;i<dlList.length;i++){
			if($(dlList[i]).find(".tb-selected").length==0){
				$(dlList[i]).addClass("no-selected");
			}
		}
		allSelected = false;
	}


}

//获取属性值图片
function getPropValImgs(valId){
	for(var i=0;i<propValueImgList.length;i++){
		if(propValueImgList[i].valueId==valId){
			var imgList = propValueImgList[i].imgList;
			var str = "";
			for(var j=0;j<imgList.length;j++){
				str+= "<li onmouseover='viewPic(this);' data-url='"+imgList[j]+"' class='img-default'>";
				str+="<table cellpadding='0' cellspacing='0' border='0'><tr><td height='65' width='65' style='text-align: center;'>";
				str+= "<img src='"+imgPath3.replace('PIC_DEFAL', imgList[j])+"' style='max-width:65px;max-height:65px;'  />";
				str+="</td></tr></table>";
				str+= "</li>";

			}
			$(".simgbox .simglist").html(str);
			viewPic($(".simgbox .simglist li").get(0));
			$("#spec-list").infiniteCarousel();
			break;
		}
	}
}

//显示秒杀规则
function showSeckillContent(){
	$("#seckillPanel").show();
	$("#prodContentPanel").hide();
}


/**
 * 获取选择的SKU值
 * @param propIndex
 * @returns {Array}
 */
function getSelectedSkus(propIndex){
	var selectedLiList = [];
	var selIndex = 0;
	for(var h=0;h<=propIndex;h++){
		if($("#prop_"+h+" .tb-selected").length!=0){
			selectedLiList[selIndex]=$("#prop_"+h+" .tb-selected").get(0);
			selIndex++;
		}
	}

	var index = 0;
	var newSkus = [];
	for(var i=0;i<skuDtoList.length;i++){
		var has = true;
		for(var j=0;j<selectedLiList.length;j++){
			var kv = $(selectedLiList[j]).attr("data-value");
			if(skuDtoList[i].properties.indexOf(kv)<0){
				has = false;
				break;
			}
		}
		if(has){
			newSkus[index]=skuDtoList[i];
			index++;
		}
	}
	return newSkus;
}

function numberConvert(num) {
	var number = num.toString();
	var index =  number.indexOf('.');
	var price = index != -1?number.substring(0,index+3):number;
	return price;
}

//客服链接
$("#loginIm").click(function () {
  var user_name = $("#user_name").val();
  var user_pass = $("#user_pass").val();
  if (user_name == '' || user_pass == '') {
    login();
  } else {
    window.open(contextPath + "/p/im/index/" + currSkuId, '_blank');
  }
});
