function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}

$(".nch-sortbar-array ul li").bind("click",function() {
	var id = $(this).attr("id");
	var orderDir = "";
	$(".nch-sortbar-array ul li").each(function(i) {
		if (id != $(this).attr("id")) {
			$(this).removeClass("selected");
		}
	});
	$(this).addClass("selected");
	var _a=$(this).find("a");
	if (id == 'group_price') {
		if ($(_a).hasClass("desc")) {
			orderDir = "group_price,asc";
		} else if($(_a).hasClass("asc")){
			orderDir = "group_price,desc";
		}else{
			$(_a).removeClass("asc");
			orderDir = "group_price,desc";
		}
	} else if (id == 'start_time') {
		if ($(_a).hasClass("desc")) {
			orderDir = "start_time,asc";
		} else if($(_a).hasClass("asc")){
			orderDir = "start_time,desc";
		}else{
			orderDir = "start_time,desc";
		}
	}
	data.orders=orderDir;
	$("#main-nav-holder").load(contextPath+"/group/groupList", data, 
			function(){
	});

});

function pager(curPageNO){
	data.curPageNO=curPageNO;
	$("#main-nav-holder").load(contextPath+"/group/groupList", data, 
			function(){
	});
}

$(".pagetab2 ul li").click(function(){
	var tab=$(this).attr("tab");
	data.orders="";
	data.timeTab=tab;
	$("#main-nav-holder").load(contextPath+"/group/groupList",data, 
			function(){
	});
});