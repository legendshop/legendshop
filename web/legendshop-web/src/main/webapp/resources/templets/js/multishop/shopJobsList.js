jQuery(document).ready(function(){
  	userCenter.changeSubTab("shopJobs");
 });

function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("from1").submit();
}

function createJob(){
	layer.open({
		  title :"创建职位",
		  id: "editJob",
		  type: 2,
		  resize: false,
		  content: [contextPath+"/s/shopJobs/load",'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['520px', '550px']
		});
}

function updateJob(id){
	layer.open({
		  title :"修改职位",
		  id: "editJob",
		  type: 2,
		  resize: false,
		  content: [contextPath+"/s/shopJobs/load/"+id,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['520px', '550px']
		});
}

function delJob(id){
	layer.confirm("确定要删除该职位吗？",{
 		 icon: 3
  	     ,btn: ['确定','取消'] //按钮
  	   },function(){
		jQuery.ajax({
			url:contextPath+"/s/shopJobs/del/"+id,
			type:'get',
			async : false, //默认为true 异步
			dataType : 'json',
			success:function(result){
				if(result=="hasShopUsers"){
					layer.alert('该职位下有员工，不能删除', {icon: 5});
				}else{
					layer.msg('删除成功', {icon: 1,time:700},function(){
                		window.location.reload();
                	});
				}
			}
	 });
	});
}
