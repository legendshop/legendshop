$(function () {
  //debugger;

  //初始化webSocket
  var initWebSocket = function () {
    //debugger;
    var _message = null;
    if (websocket != null) {
      _message = websocket.onmessage;
      window.oldSocket = websocket;
    }
    //判断当前浏览器是否支持WebSocket
    if ('WebSocket' in window) {
      if (websocket && websocket.readyState == WebSocket.OPEN) {
        return;
      }
      websocket = new WebSocket(IM_BIND_ADDRESS);
      if (_message != null) {
        websocket.onmessage = _message;
      }
    } else {
      layerMsg("当前浏览器 Not support websocket");
    }

    //连接发生错误的回调方法
    websocket.onerror = function (event) {
      console.log("WebSocket连接发生错误,状态：" + websocket.readyState);
      reconnect();
      layerMsg("WebSocket连接发生错误");
    };

    //连接关闭的回调方法
    websocket.onclose = function () {
      console.log("WebSocket连接关闭,状态：" + websocket.readyState);
      console.log("连接关闭时间：" + new Date());
      //window.location.reload(true);
      // debugger;
        // window.location.href = contextPath + "/customerService/login";
      reconnect();
    }

    //监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
    window.onbeforeunload = function () {
      console.log("WebSocket连接关闭,状态：" + websocket.readyState);
     //debugger;
      websocket.close();
    }

    /*
     * websocket.readyState
   * readyState表示连接有四种状态：
   * CONNECTING (0)：表示还没建立连接
   * OPEN (1)： 已经建立连接，可以进行通讯
   * CLOSING (2)：通过关闭握手，正在关闭连接
   * CLOSED (3)：连接已经关闭或无法打开
   */
    //连接成功建立的回调方法
    websocket.onopen = function (event) {
      console.log("WebSocket连接成功,状态：" + websocket.readyState + "  连接成功时间：" + new Date());
     // debugger;
      chat.login(11, customId, name, shopId, sign);
    }


  }

  //初始化webSocket
  initWebSocket();
  window.__init = initWebSocket;

});


function reconnect() {
  setTimeout(function () {     //没连接上会一直重连，设置延迟避免请求过多
    try {
      //debugger;
      if (websocket.readyState == WebSocket.OPEN) {
        return;
      }
      window.__init();
      // websocket = new WebSocket(IM_BIND_ADDRESS);
      // websocket.onclose = reconnect;
      // websocket.onerror = reconnect;
    } catch (e) {
      // websocket.onclose = reconnect;
      // websocket.onerror = reconnect;
    }
  }, 2000);
}
