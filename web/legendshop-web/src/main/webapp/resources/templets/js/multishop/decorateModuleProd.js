//保存
function saveSlide() {
	var params = $("#BannerForm").serialize();
	var layoutId = $("#layoutId").val();
	var layoutDiv = $("#layoutDiv").val();
	var layout=$("#layout").val();
	
	var count = $(".fiit_side_tab ul li").length;
	for( var i=1;i<=count;i++){
		var _url=$("#url_"+i);
		var url=$.trim($(_url).val());
		if(isBlank(url)){
			$(".fiit_side_tab li").removeClass("this");
			$(".fiit_side_tab li[count="+i+"] ").addClass("this");
			$(".fiit_banner dl").hide();
			$(".fiit_banner dl[count="+i+"]").show();
			parent.layer.msg("请输入Url地址",{icon:0});
			return ;
		}
	 }
	 var imageCount=$(".fiit_banner dl img").length;
	 if(imageCount==0){
	 	parent.layer.msg("至少上传一张图片",{icon:0});
		return ;
	   }
	   $("#save").attr("disabled","disabled");
	   $.ajax({
	         //提交数据的类型 POST GET
	         type:"POST",
	         //提交的网址
	         url:contextPath+"/s/shopDecotate/layout/bannerSave",
	         //提交的数据
	         data: params,
	         async : false,  
	         //返回数据的格式
	         datatype: "text",
	         //成功返回之后调用的函数            
	         success:function(data){
	        	  if(data=="fail"){
	        		  parent.layer.msg("保存失败",{icon:2});
	        		  $("#save").removeAttr("disabled");
	        	  }else{
						$('#module_edit').remove();
						if (layoutDiv != "") {
							parent.$("div[mark=" + layoutId + "][div=div2][id='content']").html("");
							parent.$("div[mark=" + layoutId + "][div=div2][id='content']").html(data);
						} else {
							//parent.$("div[option=" + layoutId + "]").find("a:first").before(data);\
							parent.$("div[mark=" + layoutId + "][id='content']").html("");
							parent.$("div[mark=" + layoutId + "][id='content']").html(data);
							
						}
						$("#save").removeAttr("disabled");
						var index = parent.layer.getFrameIndex("decorateModuleBanner"); 
						parent.layer.close(index); 
	        	  }
	         },
	         //调用执行后调用的函数
	         complete: function(XMLHttpRequest, textStatus){
	         }       
	   });
}
		 
    


function removeslide(){
	 jQuery(".fiit_banner dl:last").remove();
	  jQuery(".fiit_side_tab li:last").remove();
	  jQuery(".fiit_side_tab li").removeClass("this");
	  jQuery(".fiit_banner dl").hide();
	  jQuery("dl[count=1]").show();
	  jQuery(".fiit_side_tab li[count=1]").addClass("this").show();
}


function validateFile(_name) {
	var filepath = jQuery("input[name='"+_name+"']").val();
	var extStart = filepath.lastIndexOf(".");
	var ext = filepath.substring(extStart, filepath.length).toUpperCase();
	if (ext != ".BMP" && ext != ".PNG" && ext != ".GIF" && ext != ".JPG"
			&& ext != ".JPEG") {
		parent.layer.msg("图片限于bmp,png,gif,jpeg,jpg格式",{icon:0});
		return false;
	}
	return true;
}


function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}


