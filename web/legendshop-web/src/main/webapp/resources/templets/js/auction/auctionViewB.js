/**
 * 出价
 * */
function bid(){
	var userprice = $("#bidPrice").val();
	var price = Number(jQuery.trim(userprice));
	//判断是否登录
	 if(isLogin()){
		login();
		return;
	 }
	 $('#bidDing').attr('href','javascript:void(0);');//去掉a标签中的href属性 
	if(checkPrice(price)){
	   	 //执行JS 点击验证校验
	   	 var count = parseInt(getCookie('bid_count_'+paimaiId)) + 1;
	   	 if(count>10){  //60S点击次数大于10
	   		layer.alert("您点击的频率过快！",{icon: 0});
	   		 return;
	   	 }
	   	 addCookie("bid_count_"+paimaiId, count, "60"); //10秒
	   	 
	     var data = {"price":price,"prodId":prodId,"skuId":skuId};
	    
		 $.ajax({
		        //提交数据的类型 POST GET
		        type:"POST",
		        data:data,
		        //提交的网址
		        url:contextPath+"/auction/bid/"+paimaiId,
		        //提交的数据
		        async : false,  
		        //返回数据的格式
		        datatype: "json",
		        //成功返回之后调用的函数            
		        success:function(_result){
		        	var r=eval(_result);
		        	result(r);
		        	
		        	if(r=='201'){ //一口价成功
		        		layer.alert('恭喜获拍！请在24小时内转订单，否则将取消获拍资格。', {closeBtn: 0}, function(){
		        			window.location.href = contextPath+"/p/auctionBidRec";
		        		});
		        	}else {
		        		pageCurrentReload(); //出价后页面重新异步加载
		        	}
		        	
		        },
		        //调用执行后调用的函数
		        complete: function(XMLHttpRequest, textStatus){
		        }     
		  });
		 
	}else{
		//验证失败,给一个默认值
		$("#bidPrice").val(Number(Number(currentPrice)+Number(priceLowerOffset)));
	}
	$('#bidDing').attr('href','javascript:bid();');//去掉a标签中的href属性 
}

/**
 * 加价
 */
function incre(){
	var userprice = $("#bidPrice").val();
	var price = Number(jQuery.trim(userprice));
	var limitPrice = !isNaN(maxPrice) && maxPrice >= 1; //判断最高价格是否是数字或者是否大于1
	if(limitPrice){
		if(price+1>maxPrice){
			$("#bidPrice").val(maxPrice);
			return ;
		}
	}
	if(price+1<currentPrice+priceLowerOffset){
		$("#bidPrice").val(currentPrice+priceLowerOffset);
	}
	else if(price+1>=currentPrice+priceLowerOffset && price+1<=currentPrice+priceHigherOffset){
		$("#bidPrice").val(price+1);
	}
	else{
		$("#bidPrice").val(currentPrice+priceLowerOffset);
	}
}


/**
 * 减价
 */
function decre(){
	var userprice = $("#bidPrice").val();
	var price = Number(jQuery.trim(userprice));
	var limitPrice = !isNaN(maxPrice) && maxPrice >= 1;
	if(limitPrice){
		if(price-1>maxPrice){
			$("#bidPrice").val(maxPrice);
			return ;
		}
	}
	if(price-1<currentPrice+priceLowerOffset){
		$("#bidPrice").val(currentPrice+priceLowerOffset);
	}
	else if(price-1>=currentPrice+priceLowerOffset && price-1<=currentPrice+priceHigherOffset){
		$("#bidPrice").val(price-1);
	}
	else{
		$("#bidPrice").val(currentPrice+priceLowerOffset);
	}
}

function result(result){
	if(result=='000' || result=='002' ){
		$("#bidDing").hide();
		$("#bidEnd").show();
		layer.alert("活动已结束！",{icon: 0});
	}
	if(result=='001'){
		$("#bidDing").hide();
		$("#bidStart").hide();
		$("#bidEnd").show();
		layer.alert("活动未开始,请稍候参与出价！",{icon: 0});
	}
	if(result=='200'){
		layer.msg("恭喜您，出价成功!",{icon: 1});
	}else if(result=='login'){
		login();
	}else if(result=='600'){
		layer.alert("出价失败，不能连续出价",{icon: 0});
	}else if(result=='573'){
		layer.alert("啊哦，出价失败-欢迎再次参与~",{icon: 0});
	}else{
		layer.alert(result,{icon: 2});
	}
	
}



/**
 * 出价前价格校验
 * */
function checkPrice(price){
	if(!/^-?\d+$/.test(price)){
		layer.alert("出价必须为正整数",{icon: 0});
        return false;
    }
    if(price==0){
    	layer.alert("出价不能为空",{icon: 0});
    	return false;
	}
    
    if(isCeiling==1 && price==maxPrice){
    	return true;
    }
    
    if(price<startPrice){
    	layer.alert("出价不能低于起拍价(￥"+startPrice+")",{icon: 0});
    	return false;
    }
    
    if(isCeiling==1 && price>maxPrice){
    	layer.alert("出价不能超过本次竞拍封顶价(￥"+maxPrice+")",{icon: 0});
		return false;
    }
	if(price<=currentPrice){
		layer.alert("出价不能低于当前价(￥"+currentPrice+")",{icon: 0});
		return false;
	}
	if(price<currentPrice+priceLowerOffset){
		layer.alert("加价幅度不能低于最低加价幅度(￥"+priceLowerOffset+")",{icon: 0});
		return false;
	}
	if(price>currentPrice+priceHigherOffset){
		layer.alert("加价幅度不能高于最高加价幅度(￥"+priceHigherOffset+")",{icon: 0});
		return false;
	}
	return true;
}


//得判断是否为数字的同时，也判断其是不是正数
function isLogin() {
	if(loginUserName=="" || loginUserName==undefined || loginUserName==null){
		return true;
	}
	return false;
}

function login(){
	layer.open({
		title :"登录",
		  type: 2, 
		  content: contextPath + '/loadLoginOverlay', //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['440px', '530px']
		}); 
}



/**
 * js控制 出价的频率
 */
function addCookie(name, cookievalue, time) {
	if (name != "" && cookievalue != "" && time != "") {
		if (isNaN(time) == false) {
			var expires = new Date();
			expires.setTime(expires.getTime() + time * 1000);
			document.cookie = name + '=' + escape(cookievalue) + ';expires='+ expires.toGMTString();
		}
	}
}

function getCookie(cookieName) {
	var cookieString = document.cookie;
	var start = cookieString.indexOf(cookieName + '=');
	if (start == -1)
		return null;
	start += cookieName.length + 1;
	var end = cookieString.indexOf(';', start);
	if (end == -1)
		return unescape(cookieString.substring(start));
	return unescape(cookieString.substring(start, end));
}


//初始化
(function(){
	initCurrentData();

  //店铺分数
  var shopScore=$("#shopScoreNum").text();
  if(shopScore==null){
    shopScore=0;
  }
  $(".scroll_red").css("width",(shopScore/5)*100+"%");
})();

//查询起始
var queryStart=0;

//查询结束
var queryEnd=20;

function pageCurrentReload(){
	queryStart=0;
	queryEnd=20;
	initCurrentData();
}


//初始化中标记录数据
function initCurrentData(){
	var url =contextPath+ "/auction/current/bidders/"+paimaiId+"?prodId="+prodId+"&skuId="+skuId+"&start="+queryStart+"&end="+queryEnd;
	$.getJSON(url +"&random="+Math.random(),function (response) {
		responseInstall(response);
	});	
	loadAccess();
}

/**
 * 报名人数
 */
function loadAccess(){
    var url= contextPath+"/auction/current/queryAccess/"+paimaiId+"?random="+Math.random();
    jQuery.getJSON(url,function (response) {
         $('#accessSignNum').html(response);
    });
}

var orderStatus=0;
function responseInstall(response){
	auctionStatus=Number(response.auctionStatus);
	remainTime=Number(response.remainTime);
	currentPrice = Number(response.currentPrice);
	qualification= Number(response.qualification);
	orderStatus= Number(response.orderStatus);
	if(currentPrice==0){
		currentPrice=startPrice;
	}
	var crowdWatch= response.crowdWatch;
	$("#accessNum").html(crowdWatch);
	fillPage();
	var bidCount = Number(response.bidCount);
	if(bidCount>0){ //中标总记录
		if(response.bidCount<queryEnd){
			$(".bidding-note .more").hide();
		}
		$("#bidCount").html("出价记录 <span>（共"+response.bidCount+"次出价）</span>");
	}else{
		$(".bidding-note .more").hide();
		$("#bidCount").html("出价记录 <span>（共0次出价）</span>");
	}
	$("#bidCountNumber").html(bidCount);
	var bidInfo = "";


	if($(response.bidList).length <= 0){

	  $(".bidding-step-not").hide();
  }


	$(response.bidList).each(function(key,value){
		var bidTime = value.bidTime;
		var pin = value.userId;
		var price = value.priceStr;
		var status = "无效";
    if(key==0){
      status="领先";
      var bidRecord ="<tr>";
      bidRecord +="<td><p>"+bidTime+"</p></td>";
      bidRecord +="<td><p style='color: #c6171f;'>"+pin+"</p></td>";
      bidRecord +="<td><p style='color: #c6171f;'>"+"¥"+price+"</p></td>";
      bidRecord +="<td><span>"+status+"</span></td>";
      bidRecord +="</tr>";
      bidInfo += bidRecord;
    }else{
      status="出局";
      var bidRecord ="<tr>";
      bidRecord +="<td><p>"+bidTime+"</p></td>";
      bidRecord +="<td><p>"+pin+"</p></td>";
      bidRecord +="<td><p>"+"¥"+price+"</p></td>";
      bidRecord +="<td><span class='out'>"+status+"</span></td>";
      bidRecord +="</tr>";
      bidInfo += bidRecord;
    }

  });
  $(".records tbody").nextAll().remove();
  $(".records tbody").append(bidInfo);

  /* table滚动效果  参数1 tableID,参数2 div高度，参数3 速度，参数4 tbody中tr几条以上滚动 */
  tableScroll("bidding-list",120,30,3);
}

function loadMore(){
	queryEnd=Number(queryEnd)+5;
	if(queryEnd>30){
		queryEnd=30;
	}
	initCurrentData();
}


function fillPage(){
	//设置库存行
	var currentPriceInfo = "当前价：<em style='font-size: 24px; color: #666;'>¥"+currentPrice+"</em>"; //当前价格
	$("#shopPrice").html(currentPriceInfo);
	//拍卖未开始
	if(auctionStatus==1&&remainTime<0){ //距离开拍时间
		//未缴纳保证金
		if(qualification==0){
			$('#pm-operation-ensure-n').show();
			$('#pm-operation-start-n').hide();
			$('#pm-operation-start-y').hide();
			$("#ipu-bid-price").hide();
			$('#paimai_over').hide();
		}
		//已缴纳保证金
		if(qualification==1){
			$('#pm-operation-ensure-n').hide();
			$('#pm-operation-start-n').show();
			$('#pm-operation-start-y').hide();
			$("#ipu-bid-price").hide();
			$('#paimai_over').hide();
		}
	}
	//拍卖结束
	else  if((auctionStatus==2)||(auctionStatus==1 && remainTime>0)){
		$('#pm-operation-ensure-n').hide();
		$('#pm-operation-start-n').hide();
		$('#pm-operation-start-y').hide();
		$("#ipu-bid-price").hide();
		$('#paimai_over').show();
		if(orderStatus==1){  //已经中标了
			$("#pm-operation-over").html("恭喜！您已胜出！");
			$("#pay_overlay").show();
			$(".black_overlay").show();
			Jump(5);
		}
	}else{// 正在进行中
		
		//未缴纳保证金
		if(qualification==0){
			$('#pm-operation-ensure-n').show();
			$('#pm-operation-start-n').hide();
			$('#pm-operation-start-y').hide();
			$("#ipu-bid-price").hide();
			$('#paimai_over').hide();
		}
		
		//已缴纳保证金
		if(qualification==1){
			$('#pm-operation-ensure-n').hide();
			$('#pm-operation-start-n').hide();
			$('#pm-operation-start-y').show();
			$("#ipu-bid-price").show();
			$('#paimai_over').hide();
		}
	}
}



function Jump(Num) {
    Num--;
    document.getElementById("sp_Number").innerHTML = Num;
    if (Num <= 0) {
        window.location.href=contextPath+"/p/auctionBidRec";
    } else {
        timer = setTimeout("Jump(" + Num + ")", 1000);
    }

}

var tab_top = jQuery(".pagetab2").offset().top;
jQuery(window).scroll(function() {
	var top = jQuery(".pagetab2").offset().top;
	var scrolla = jQuery(window).scrollTop();
	var i = top - scrolla;
	if (i <= 0) {
		jQuery(".pagetab2").addClass("fixed");
		jQuery(".pagetab2").css("margin-top","0px");
	}
	if (scrolla < tab_top) {
		jQuery(".pagetab2").removeClass("fixed");
		jQuery(".pagetab2").css("margin-top","30px");
	}
});

function bidLists(){
    var className=$("#bidlists").attr("class");
	if(className=='on'){
		return false;
	}	
	var url = contextPath+"/auction/queryBidList?aId="+paimaiId+"&prodId="+prodId+"&skuId="+skuId;
		$(".tabcon1").hide();
		$(".tabcon").show();
		$(".tabcon").load(url+"&random="+Math.random());
		$(".pagetab2 ul").find("[class='on']").removeClass("on");
		$("#bidlists").addClass("on");
}

function pager(curPageNO){
 	var url = contextPath+"/auction/queryBidList?aId="+paimaiId+"&prodId="+prodId+"&skuId="+skuId+"&curPageNO="+curPageNO;
 	$(".tabcon").load(url+"&random="+Math.random());
}


 function auctionDes(){
		    var className=$("#dec").attr("class");
			if(className=='on'){
				return false;
			}	   
	    	$(".tabcon1").show();
	    	$(".tabcon").hide();
	   	 	$(".pagetab2 ul").find("[class='on']").removeClass("on");
			$("#dec").addClass("on");
}

function giveMoney(){
	if(isLogin()){
		login();
		return;
	}
	if(buyUserId==userId){
		layer.alert("不能拍卖自己的商品！",{icon: 0});
	}else {
		window.location.href=contextPath+"/p/paimai/paySecurity/agreement/"+paimaiId;
	}
}

//时间格式 一位数时补零
function rightZeroStr(v) {
    if (v < 10) {
        return "0" + v;
    }
    return v + "";
}

setInterval(remainTimeCountDown,1000);

function remainTimeCountDown(){
	if(runTime>0){
		runTime = runTime - 1000;
		var timeText = timeBetweenText();
		$(".pm-status-time").html(timeText);
	}
}
//计算时间差
function timeBetweenText() {
	var dayOfMil = (24 * 60 * 60 * 1000);
    var hourOfMil = 60 * 60 * 1000;
    var minOfMil = 60 * 1000;
    var secOfMil = 1000;
    var mmiOfMil=10;
    
    var hourOffset = runTime % dayOfMil;
    var minuteOffset = hourOffset % hourOfMil;
    var seccondOffset = minuteOffset % minOfMil;
    
    var hours = Math.floor(runTime / hourOfMil);
    var minutes = Math.floor(minuteOffset / minOfMil);
    var seconds = Math.floor(seccondOffset / secOfMil);
    
    var text = "";
    if(hours > 24){
    	var day = rightZeroStr(Math.floor(runTime / (hourOfMil * 24)));
    	var _hours = hours - (24 * day);
    	text = "<span><em>"+day+"</em>天<em>"+_hours+"</em>时<em>"+minutes+"</em>分</span>"
    } else{
    	if (hours > 0) {
    		text = "<span><em>"+hours+"</em>时<em>"+minutes+"</em>分<em>"+seconds+"</em>秒</span>"
        } else if (minutes > 0) {
        	text = "<span><em>"+minutes+"</em>分<em>"+seconds+"</em>秒</span>"
        } else if (seconds >= 0) {
        	text = "<span><em>"+seconds+"</em>秒</span>"
        }
    }
    if(runTime<=1000 && timeStatus==0){
    	runTime=time;
    	$("#bidStart").hide();
		$("#bidDing").show();
		$("#bidEnd").hide();
		$(".pm-status-mark").text("正在进行");
		timeStatus=1;
    }else if(runTime<=1000 && timeStatus==1){
    	$("#bidStart").hide();
		$("#bidDing").hide();
		$(".ipu-price").hide();
		$("#bidEnd").show();
		$(".pm-status-mark").text("拍卖结束").css("background","#999");
		$(".pm-status-mark").next().text("结束时间："+endTime);
    }
    return text;
}
//客服链接
$("#loginIm").click(function () {
  var user_name = $("#user_name").val();
  var user_pass = $("#user_pass").val();
  if (user_name == '' || user_pass == '') {
    login();
  } else {
    window.open(contextPath + "/p/im/index/" + skuId, '_blank');
  }
});
