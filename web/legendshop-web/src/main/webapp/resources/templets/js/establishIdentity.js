$(document).ready(function() {
	if(_toPage=="toUpdateMail"){
		if(mailVerifn==0){
			$(".o-mt").html("<h2>验证邮箱</h2>");
			$(".fore2").html("2.验证邮箱<b></b>");
		}else{
			$(".o-mt").html("<h2>修改邮箱</h2>");
			$(".fore2").html("2.修改邮箱<b></b>");
		}
	}else if(_toPage=="toUpdateMobile"){
		$(".mailValA").hide();
		if(phoneVerifn==0){
			$(".o-mt").html("<h2>验证手机</h2>");
			$(".fore2").html("2.验证手机<b></b>");
		}else{
			$(".o-mt").html("<h2>修改手机</h2>");
			$(".fore2").html("2.修改手机<b></b>");
		}
	}else if(_toPage=="toUpdatePaymentpassword"){
		if(paypassVerifn==0){
			$(".o-mt").html("<h2>验证支付密码</h2>");
			$(".fore2").html("2.验证支付密码<b></b>");
		}else{
			$(".o-mt").html("<h2>修改支付密码</h2>");
			$(".fore2").html("2.修改支付密码<b></b>");
		}
	}

	//刷新验证码
	changeRandImg(contextPath);
	$("#sendMobileCode").bind("click",function(){
		timer = sendSMSCode();
	});

	calRemainTime();

});

//验证码校验
$("#randNum").bind("blur",function(){
	validateRandNum(contextPath);
});

//发送验证短信
function sendSMSCode(){
	var timer;
	var randNum = $("#randNum").val();
	if(randNum==null || randNum==undefined || randNum==""){
		layer.msg("请输入图形验证码",{icon: 0});
		return ;
	}
	$.ajax({
		url:contextPath + "/p/verifyMessage", 
		data:{"mobile":userMobile,"securityCode":securityCode,"randNum":randNum},
		type:'post', 
		dataType : 'json', 
		async : false,   
		success:function(retData){
			var sendMobileCode = $("#sendMobileCode");
			sendMobileCode.unbind("click");
			if(retData == 'OK'){
				$("#countDown").show();
				$("#code").removeAttr("disabled");
				$("#code").css("background-color","white");
				//调用倒计时
				timer = setTimeout("onTimer(" + interVal + ")",1000);
			}else if(retData="errorCode"){
				layer.msg("图形验证码错误",{icon: 0});
				changeRandImg(contextPath);
				$("#sendMobileCode").unbind("click").bind("click",function(){ sendSMSCode(); });
			}else{
				sendMobileCode.attr("disabled", true);
				sendMobileCode.attr("class", "disabled-btn");
				var countDown = $("#countDown");
				countDown.html("发送短信频率过高，请稍等再试试");
				countDown.show();
				$("#code").attr("disabled",true); 
			}
		}
	});	
	return timer;
}

//倒计时, need cookies
function onTimer(remains) {
	var sendMobileCode = $("#sendMobileCode");
	if (remains == 0) {
		$("#timer").html(0);
		$("#countDown").hide();
		sendMobileCode.removeAttr("disabled");
		sendMobileCode.attr("class", "btn-q");
		$("#sendMobileCode").unbind("click").bind("click",function(){ sendSMSCode(); });
		//console.debug("stop timer " + timer);
		wait = interVal;
		window.clearTimeout(timer);
	} else {
		sendMobileCode.attr("disabled", true);
		sendMobileCode.attr("class", "disabled-btn");
		$("#timer").html(wait);
		var remains = wait--;
		timer = setTimeout("onTimer(" + remains +")",1000);
	}
}

function calRemainTime(){
	if(wait!= null && wait !=interVal){
		var sendMobileCode = $("#sendMobileCode");
		sendMobileCode.attr("disabled", true);
		sendMobileCode.attr("class", "disabled-btn");
		$("#countDown").show();
		$("#code").removeAttr("disabled");
		$("#code").css("background-color","white");
		$("#sendMobileCode").unbind("click");
		$("#timer").html(wait);
	}else{
		wait = interVal;
	}
}


//前往修改页面
function validCode(){
	if(validateRandNum(contextPath)){

		if(verifySMSCode($("#code").val())){
			$("#securityCode").val($("#code").val());
			var form1 = $("#smsCodeForm1");
			form1.attr("action", contextPath + "/p/"+_toPage); 
			form1.submit();
		}else{
			$("#code_error").html("短信验证码错误 或  验证码过时！");
			$("#code_error").show();
		}
	}else{
		var validateCode=$("#code").val();
		if(validateCode==null || validateCode=="" || validateCode==undefined){
			$("#code_error").html("请输入短信验证码");
			$("#code_error").show();
			return false;
		}else{
			$("#code_error").html("");
			$("#code_error").hide();
		}
	}
}

//验证验证码是否正确
function verifySMSCode(validateCode){
	var result = false;
	$.ajax({
		url: contextPath + "/p/verifySMSCode",
		data: {"validateCode": validateCode},
		type: 'post', 
		dataType : 'json', 
		async : false,   
		success:function(retData){
			result = retData;
			if(!retData){
				changeRandImg(contextPath);
				$("#randNum").val('');
			}
		}
	});	
	return result;
}