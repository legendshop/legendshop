$(".button").bind("click",function(){
	var couponId = $(this).attr("couponId");
	jQuery.ajax({
		url : contextPath + "/isUserLogin",
		type : 'get',
		async : false, // 默认为true 异步
		success : function(result) {
			if(result=="true"){
				layer.load(1);
				$.ajax({
					url: contextPath+"/p/coupon/receive/" + couponId,
					type:'post',
					async : false, //默认为true 异步
					dataType:"json",
					success:function(data){
						if(data=="OK"){
							layer.msg("恭喜您领取成功！", {icon: 1,time:1500},function(){
								skip();
							});
						}else{
							layer.msg(data,{icon:0,time:1500},function(){
								window.location.reload();
							});
						}
					}
				});
			}else{
				layer.open({
					title :"登录注册",
					id: "login",
					type: 2,
					resize: false,
					content: [contextPath + '/loadLoginOverlay','no'],
					area: ['440px', '530px']
				});
			}
		}
	});
});

function skip(){
	window.location.href=contextPath + "/couponCenter";
}

$("#coupon").bind("click",function(){
	var type="shop";
	window.location.href=contextPath + "/couponCenter/couponMore/"+type;

});
$("#redPack").bind("click",function(){
	var type="platform";
	window.location.href=contextPath + "/couponCenter/couponMore/"+type;
});
