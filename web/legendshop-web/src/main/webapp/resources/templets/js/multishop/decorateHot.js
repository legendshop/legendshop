/**
 *  Tony 
 */
$(document).ready(function(){
	
	$(".fiit_hot_tab li").click(function(){
		var tar =$(this).attr("tar");
		$("div[id^=tar_]").hide();
		$("#tar_"+tar).show();
		$(".fiit_hot_tab li").removeClass("this");
		$(this).addClass("this");
		if(tar=="img"){
			 $(".balck_bg").show();
			}else{
			$("#hotspots li").removeClass("this");
			 $(".balck_bg").hide()	
	     }
	});
	
	
	//背景设置
	$("#bg_img").live("change",function(){	
		 if(!validateFile()){
			 return false;
		 }
		    var val =$(this).val();
		    var img_id =$(".fiit_hot_img img").attr("id");		
			$("#bg_img_show").val($(this).val());
			$.ajaxFileUpload({
			      url: contextPath + '/s/shopDecotate/layout/decorate_hot_upload?img_id='+img_id,             
			      secureuri:false,
			      fileElementId:'bg_img',                         
			      dataType: 'json',
			      error: function (data, status, e){
			    	  parent.layer.alert(data,{icon: 2});
			       },
			       success: function (data, status){  
			     	  if(data.status =="fail"){
			     		 parent.layer.msg("状态失败",{icon: 2});
			     	 }else{
						 $("#img_show").val(data.pic);
						 $("#hotspot_url").val();
						 $("#apply_mark").val();
						 $("#apply_mark").attr("value","");
						 $("#hotspots").html("");
						 $(".fiit_hot_img .choose").each(function(){
							 $(this).remove();										   
						 });
						 $(".image-maps-conrainer .position-conrainer .map-position").each(function(){
							 $(this).remove();										   
						 });
						 
						  $(".link-conrainer").remove();
						  $(".position-conrainer").remove();
						
						  var container = $("#imgMap");
						  if((container.find('img[ref=imageMaps]')).size()==0){
							  container.find("span").append("<img  ref='imageMaps' style='border: 1px solid rgb(204, 204, 204);'  usemap='#Map'  >");
						  }
						  $(".fiit_hot_img").show().find("img").attr("src",photoPath+data.pic).attr("id",data.pic);
						  var $images = container.find('img[ref=imageMaps]');
						  $images.each(function(){
								var _img_conrainer = $(this).parent();
								_img_conrainer.append('<div class="link-conrainer"></div>').append($.browser.msie ? $('<div class="position-conrainer" style="position:absolute"></div>').css({
									background:'#fff',
									opacity:0
								}) : '<div class="position-conrainer" style="position:absolute;z-index:9999;"></div>');
								var _img_offset = $(this).offset();
								var _img_conrainer_offset = _img_conrainer.offset();
								_img_conrainer.find('.position-conrainer').css({
									top: _img_offset.top - _img_conrainer_offset.top,
									left: _img_offset.left - _img_conrainer_offset.left,
									width:data.width,
									height:data.height,
									border:'1px solid transparent'
								});
						});
							
							
			     	 }
			     }
		  });
	 });
	
	$("#hotspots li").live("click",function(){
			$(this).parent().find("li").removeClass("this");
			var mark =$(this).addClass("this").attr("mark");
			var url = $(this).data("bind-url");
			$("#hotspot_url").val(url);
			$(".map-position").removeClass("this");
			$(".map-position[ref="+mark+"]").addClass("this");
			$("#url_count").html(mark);
			$("#apply_mark").val(mark);
	 });
	
});



function validateFile() {
	var _this=$("input[name='bg_img']");
	var filepath = $("input[name='bg_img']").val();
	var extStart = filepath.lastIndexOf(".");
	var ext = filepath.substring(extStart, filepath.length).toUpperCase();
	
	//提示上传图片大小
	if (!checkImgSize(_this,5120)) {
		layer.msg("图片不能超过5M",{icon: 0});
		return false;
	}
	
	if (ext != ".BMP" && ext != ".PNG" && ext != ".GIF" && ext != ".JPG"
			&& ext != ".JPEG") {
		layer.msg("图片限于bmp,png,gif,jpeg,jpg格式",{icon: 0});
		return false;
	}
	return true;
}

function selectShopCoupon(){
	var img_id =$(".fiit_hot_img img").attr("id");
	if(!isBlank(img_id)){
		var apply_mark = $("#apply_mark").val();
		if (isBlank(apply_mark)) {
			layer.msg("请划分热点区域！",{icon: 0});
			return;
		}
		var page = contextPath+"/s/shopDecotate/layout/selectShopCoupon";
		layer.open({
			title :"选择商家购物券",
			  type: 2, 
			  content: page, //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
			  area: ['550px', '360px']
			}); 
    }else{
	  layer.msg("请先设置背景图片！",{icon: 0});
	}
}


function couponCallBack(_couponId,_shopId){
	   var path=contextPath+"/p/coupon/apply/"+_couponId;
	   $("#hotspot_url").val(path);
 }



function isBlank(_value){
	if(_value=="" || _value==null || _value==undefined){
		return true;
	}
	return false;
}

function applyHotspot(){
	var img_id =$(".fiit_hot_img img").attr("id");
	if(!isBlank(img_id)){
		var url =$("#hotspot_url").val();
		if(!isBlank(url)){
			var apply_mark = $("#apply_mark").val();
			if (apply_mark != "") {
				console.log("apply_mark-"+apply_mark);
				var coor = $("#apply_mark").data("bind-data");
				var width = $("#apply_mark").data("width");
				var height = $("#apply_mark").data("height");
				var top = $("#apply_mark").data("top");
				var left = $("#apply_mark").data("left");
				$("#hotspots li[mark=" + apply_mark + "]").data("bind-data", coor).data("bind-url", url);
				
				$("#apply_mark").val("");
				$("#hotspot_url").val("");	
				/*var text = '<div class="choose this" mark="' + apply_mark+ '" style="position:absolute; top:' + top+ 'px; left:' + left + 'px; width:' + width+ 'px; height:' + height + 'px;"></div>';
				$(".fiit_hot_img span").append(text);*/
			//	cancelSelection();
			} else {
				layer.msg("请划分热点区域！",{icon: 0});
			}
		}else {
			    layer.msg("请填写热点链接！",{icon: 0});
		    }
    }else{
		  layer.msg("请先设置背景图片！",{icon: 0});
	 }
}

function save_form() {

	var coorImg = $(".fiit_hot_img img").attr("id");
	if(coorImg=="" || coorImg==undefined || coorImg==null){
		parent.layer.msg("请先设置背景图片！",{icon: 0});
		return false;
	}
	var length=$("#hotspots li").length;
	if(length==0){
		 parent.layer.msg("请先设置热点！",{icon: 0});
		 return false;
	}
	var coorDatas=new Array();
	var _index=1;
	var config=true;
	$("#hotspots li").each(function() {
		var sentence = $(this).data("bind-data");
		var url=$(this).data("bind-url");
		if(sentence!="" && sentence!=undefined){
			if(url=="" || url==undefined || url==null){
				parent.layer.msg("请设置热点Url！",{icon: 0});
				config=false;
				return false;
			}
			var words = sentence.split(",");
			var _coorObj= new Object();
			var x1= new Number(words[0]);
			var y1= new Number(words[1]);
			var x2= new Number(words[2]);
			var y2= new Number(words[3]);
			
			_coorObj.x1 = x1.toFixed(0);
			_coorObj.y1 = y1.toFixed(0);
			_coorObj.x2 = x2.toFixed(0);
			_coorObj.y2 = y2.toFixed(0);
			
			_coorObj.hotspotUrl=url;
		    coorDatas.push(_coorObj);
		}
		_index++;
	});
	if(!config){
		return false;
	}
	var coorImg = $(".fiit_hot_img img").attr("id");
	var coor_datas=JSON.stringify(coorDatas);
	console.log("coor_datas="+coor_datas);
	console.log("coor_img="+coorImg);
	 var layoutDiv=$("#layoutDiv").val();
	 var layoutId=$("#layoutId").val();
	 var data={
	    "coorDatas":coor_datas,
	    "imageFile":coorImg,
	    "layoutModuleType":$("#layoutModuleType").val(),
	    "layoutId":layoutId,
	    "layout":$("#layout").val(),
	    "divId":$("#divId").val(),
	    "layoutDiv":layoutDiv
	 }
	var imageHtml="<img border='0' src='"+photoPath+coorImg+"'>";
	$("#save").attr("disabled", "disabled");
	$.ajax({
        //提交数据的类型 POST GET
        type:"POST",
        //提交的网址
        url:contextPath+"/s/shopDecotate/layout/saveLayoutHot",
        //提交的数据
        data: data,
        async : true,  
        //返回数据的格式
        datatype: "json",
        //成功返回之后调用的函数            
		success : function(html) {
			if(html=="fail"){
				 parent.layer.msg("保存失败！",{icon: 0});
				 $("#save").attr("disabled", "true");
			}
			else{
				if (!isBlank(layoutDiv)) {
					parent.$("div[mark=" + layoutId + "][div=" + layoutDiv+ "][id='content']").html("");
					parent.$("div[mark=" + layoutId + "][div=" + layoutDiv+ "][id='content']").html(imageHtml);
					var index=parent.layer.getFrameIndex('decorateModuleHot');
					parent.layer.close(index); 
				} else {
					 parent.$("div[mark=" + layoutId + "][id='content']").html("");
					 parent.$("div[mark=" + layoutId + "][id='content']").html(imageHtml);
					 var index=parent.layer.getFrameIndex('decorateModuleHot');
				     parent.layer.close(index); 
				}
				
			}
		},
        //调用执行后调用的函数
        complete: function(XMLHttpRequest, textStatus){
        }     
    });
	

}

function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}
