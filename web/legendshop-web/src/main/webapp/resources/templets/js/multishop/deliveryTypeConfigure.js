$(document).ready(function() {
	            userCenter.changeSubTab("deliveryType");
			    jQuery("#form1").validate({
			        rules: {
			        	name: {
			                required: true,
			                maxlength:20
			            },
			            dvyId: {
			                required: true
			            }
			            ,
			            printtempId: {
			                required: true
			            },
			            notes:{
			               required: true,
			               maxlength:150
			            }
			        },
			        messages: {
			        	name: {
			                required: '请输入名称',
			                maxlength:"配送名称最大长度为20个字符"
			            },
			            dvyId: {
			                required: '请输入快递公司'
			            }
			            ,
			            // printtempId: {
			            //     required: '请设置物流配送打印模版！'
			            // },
			             notes:{
			               required: "请输入配送描述！",
			               maxlength:"配送描述最大长度为150个字符"
			            }
			        },
			        submitHandler: function(form){
			        	$.ajax({
			        		url: contextPath+'/s/deliveryType/checkNumber',
			        		type:'post',
			        		dataType : 'json',
			        		async : true, //默认为true 异步
			        		success:function(result){
			        			// if(result == 'OK'){
			        				form.submit();
			        			// }
			        			// else{
			        			// 	layer.alert('最多可以添加10条配送方式', {icon:0});
			        			// 	return;
			        			// }
			        		}
			        	});
			        }
	         });
});

function checkNumber(){
	var flag;
	$.ajax({
		url: contextPath+'/s/deliveryType/checkNumber',
		type:'post',
		dataType : 'json',
		async : true, //默认为true 异步
		success:function(result){
			if(result == 'OK'){
				flag = true;
			}else{
				flag = false;
				layer.alert('最多可以添加10条配送方式', {icon:0});
			}
		}
	});
	return flag;
}
