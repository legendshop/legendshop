<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/common.jsp"%>
<html>
<head>
<title>发送短信</title>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script type="text/javascript">
	function send(){
			$.ajax({
			url:"${contextPath}/send/mail",
			data:{"mail": $("#mail").val(), "title": $("#title").val(),  "text": $("#text").val()},
			type:'post', 
			async : true,  
			success:function(result){
				alert(result)	
			}
			});
	}

</script>
</head>
<body>
	<table>
		<tr>
			<td>邮箱</td>
			<td><input  name="mail" id="mail"/></td>
		</tr>
		<tr>
			<td>主题</td>
			<td><input  name="title" id="title"/></td>
		</tr>
		<tr>
			<td>邮件内容</td>
			<td><textarea  name="text" id="text"> </textarea></td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="button"  value="发送" name="发送" onclick="javascript:send()"/>
			</td>
		</tr>
	</table>
</body>
</html>



