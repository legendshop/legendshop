<%@ page language="java" isErrorPage="true" contentType="text/html;charset=UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
<html>
<head>
<title><c:choose>
		<c:when test="${User_Messages.code != null}">${User_Messages.code}</c:when>
		<c:otherwise>500</c:otherwise>
	</c:choose> Error - LegendShop</title>
	<%@ include file="/WEB-INF/pages/common/common.jsp"%>
</head>
<body>
	<center>
		<table cellSpacing="0" cellPadding="0" width="955px" >
			<tbody>
				<tr>
					<td noWrap width="1%">
					<a href="${contextPath}">
					      	<c:choose>
						      	<c:when test="${not empty systemConfig.logo}">
						      		<img src="<ls:photo item="${systemConfig.logo}"/>" width="200px" title="${systemConfig.shopName }" />
						      	</c:when>
						      	<c:otherwise>
						      		<img src="${contextPath}/resources/common/images/legendshop.gif" width="200px" title="LegendShop" />
						      	</c:otherwise>
					      </c:choose>
					</a>
					<td  style="padding-left: 10px;">
							<font face=arial,sans-serif color=red><b>
								<c:choose>
									<c:when test="${User_Messages.code != null}">${User_Messages.code}</c:when>
									<c:otherwise>404</c:otherwise>
								</c:choose> Error
						</b></font>
   				</td>
				</tr>
			</tbody>
		</table>

		<br>
		<table cellSpacing=0 cellPadding=0 width="955px">
			<tr  style="display: none" id="errorMessages" name="errorMessages">
					<td>${ERROR_MESSAGE}</td>
			</tr>
			<tr>
				<td>
					<BLOCKQUOTE>
						<c:choose>
							<c:when test="${User_Messages.title != null}">
								<H2>${User_Messages.title}</H2>
							  			 ${User_Messages.desc} 
							</c:when>
							<c:otherwise>
								<H2>系统错误</H2>
							 			 Internal error found on this server. 
							</c:otherwise>
						</c:choose>
						<ls:i18n key="error.page.message"/>
					</BLOCKQUOTE>
				</td>
			</tr>
		</table>
		<jsp:include page="moreoption.jsp"></jsp:include>
	</center>


	<br>
</body>
</html>
