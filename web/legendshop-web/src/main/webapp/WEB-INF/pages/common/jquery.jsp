<%@ page language="java" pageEncoding="UTF-8"%>
<%
	if (request.getAttribute("JqueryApplied") == null) {
%>
<script src="${contextPath}/resources/common/js/jquery.js"
	type="text/javascript"></script>
<script src="${contextPath}/resources/common/js/jquery-migrate.min.js"
	type="text/javascript"></script>
<script type="text/javascript">
	$().ready(function() {

		$.ajaxSetup({
			error: function(jqXHR, textStatus, errorThrown){
				switch (jqXHR.status){  
				case(500):  
					layer.msg('服务器系统内部错误', {
					  icon: 2,
					  time: 3000 //2秒关闭（如果不配置，默认是3秒）
					}, function(){
						window.location.reload();
					});
					break;  
				case(401):  
					layer.msg('未登录', {
						  icon: 2,
						  time: 3000 //2秒关闭（如果不配置，默认是3秒）
						}, function(){
							window.location.reload();
						});
					break;  
				case(403):  
					layer.msg('您无权限执行此操作', {
						  icon: 2,
						  time: 3000 //2秒关闭（如果不配置，默认是3秒）
						}, function(){
							window.location.reload();
						}); 
					break;  
				case(408):  
					layer.msg('请求超时', {
						  icon: 2,
						  time: 3000 //2秒关闭（如果不配置，默认是3秒）
						}, function(){
							window.location.reload();
						}); 
					break;  
				default:
					layer.msg('未知错误', {
						  icon: 2,
						  time: 3000 //2秒关闭（如果不配置，默认是3秒）
						}, function(){
							window.location.reload();
						});
				}
			},
		});
	});

	$(document).ajaxSend(function(e, xhr, options) {
		var _csrf_header = '${_csrf.headerName}';
		var _csrf = '${_csrf.token}';
		if (_csrf != null && _csrf_header != null) {
			if (xhr.setRequestHeader) {
				xhr.setRequestHeader(_csrf_header, _csrf);
			}
		}
	});
	function appendCsrfInput() {
		var _csrf_header = '${_csrf.parameterName}';
		var _csrf = '${_csrf.token}';
		var hiddenField = document.createElement("input");
		if (_csrf != null && _csrf_header != null) {
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", _csrf_header);
			hiddenField.setAttribute("value", _csrf);
		}
		return hiddenField;
	}
</script>
<%
	request.setAttribute("JqueryApplied", true);
	}
%>