<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/common.jsp"%>
<html>
<head>
<title>发送短信</title>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script type="text/javascript">
	function send(){
			$.ajax({
			url:"${contextPath}/send/sms",
			data:{"mobile": $("#mobile").val(), "text": $("#text").val()},
			type:'post', 
			async : true,  
			success:function(result){
				alert(result);
			}
			});
	}

</script>

</script>
</head>
<body>
	<table>
		<tr>
			<td>手机号码</td>
			<td><input  name="mobile" id="mobile"/></td>
		</tr>
		<tr>
			<td>发送内容（不超过70个字）</td>
			<td><textarea  name="text" id="text"></textarea></td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="button"  value="发送" name="发送" onclick="javascript:send()"/>
			</td>
		</tr>
	</table>
</body>
</html>



