<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<HTML>
<head>
	<TITLE>LegendShop图片空间</TITLE>
	  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
	  <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/ztree/zTreeStyle.css'/>" type="text/css">
	  <link rel="stylesheet" href="<ls:templateResource item='/resources/common/css/imageSpace.css'/>" type="text/css">
	  <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
	  <link rel="stylesheet" href="<ls:templateResource item='/resources/common/css/pager.css'/>" type="text/css">
	  <script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.js'/>"></script>
	<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.exedit-3.5.js'/>"></script>
	<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/ajaxfileupload.js'/>"></script>
	<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>"></script>
	
	<c:set var="photoPrefix" value="${shopApi:getPhotoPrefix()}"></c:set>
	<c:set var="imagesPrefix" value="${shopApi:getImagesPrefix(2)}"></c:set>
	<c:set var="imagesSuffix" value="${shopApi:getImagesSuffix(2)}"></c:set>
	<script>
		var webPath = '${contextPath}';
		var imagesPrefix = '${imagesPrefix}'
		var imagesSuffix = '${imagesSuffix}';
		var photoPath = '${photoPrefix}';
		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting);
			$("#selectAll").bind("click", selectAll);
			ajaxpic(1,0,"","");
			$("#searchBtn").click(function(){
				ajaxpic(1,0,$("#orderBy").val(),$("#searchName").val());
			});
		});
	</script>
	<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/remoteImages.js'/>"></script>
	 <link rel="stylesheet" href="<ls:templateResource item='/resources/common/css/imagesProp.css'/>" type="text/css">
</head>
<body>
<input type="hidden" id="currId" name="treeId" value="0"/>
    <div class="pictures">
    	<div class="pictures_left">
    		<h3>图片目录</h3>
    		<div class="pictures_catalog ztree" id="treeDemo">
    			
    		</div>
    	</div>
    	<div class="pictures_right">
    		<h3><select id="orderBy">
	    		<option value="descDate">按上传时间从晚到早</option>
	    		<option value="ascDate">按上传时间从早到晚</option>
	    		<option value="descSize">按图片从大到小</option>
	    		<option value="ascSize">按图片从小到大</option>
	    		<option value="descName">按图片名降序</option>
	    		<option value="ascName">按图片名升序</option>
    		</select>
    		<input id="searchName" type="text" />
    		<input id="searchBtn" type="button" value="搜索" /></h3>
    		<div class="pictures_con" >
    			<ul id="main-div">
    			</ul>
    			 <input type="hidden" id="curPageNO" name="curPageNO" />
    			 <div class="pictures_page">
    			<div id="toolBar" style="clear:both;text-align: center;padding-top:10px;"></div>
    			</div>
    		</div>
    	</div>
    </div>
</body>
</HTML>