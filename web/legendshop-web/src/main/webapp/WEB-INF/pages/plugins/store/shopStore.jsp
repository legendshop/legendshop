<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/pages/plugins/multishop/back-common.jsp"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
	<title>添加门店-${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/seckill-activity.css'/>" rel="stylesheet">
 	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
 	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/gaode-map.css" />
 	<style type="text/css">
 		.seckill-add ul li input{
 			width:310px;
 		}
 	</style>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="#">首页</a>>
					<a href="#">卖家中心</a>>
					<span class="on"> 门店管理</span>
				</p>
			<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp"%>
			
				<form:form action="${contextPath}/s/store/save" method="post" id="form1" >
					<input type="hidden" id="id" name="id" value="${store.id}"/>
					<input type="hidden" id="province" name="province" value="${store.province}"/>
					<input type="hidden" id="city" name="city" value="${store.city}"/>
					<input type="hidden" id="area" name="area" value="${store.area}"/>
					<input type="hidden" id="address" name="address" value="${store.address}"/>
					<input type="hidden" id="lng" name="lng" value="${store.lng}"/>
					<input type="hidden" id="lat" name="lat" value="${store.lat}"/>
					<div class="seckill-add">
						<ul>
							<li><font><b style="color: red">*</b>门店账号：</font>
							     <input name="userName" id="storeName" type="text"><em></em></li>
							<li>
							     <font><b style="color: red">*</b>登录密码：</font>
							     <input name="passwd" id="passwd" maxlength="20"  type="password"><em></em>
							</li>
							<li>
							     <font><b style="color: red">*</b>确认密码：</font>
							     <input name="repassword" id="repassword" maxlength="20"   type="password"><em></em>
							</li>
							
							<li>
							     <font><b style="color: red">*</b>门店名称：</font>
							     <input name="name" id="name"  maxlength="20"  type="text"><em></em>
							</li>
									
							<li>
							     <font><b style="color: red">*</b>联系手机：</font>
							     <input name="contactMobile" id="contactMobile" maxlength="20"   type="text"><em></em>
							</li>
							<li>
							     <font><b style="color: red">*</b>营业时间：</font>
							     <input name="businessHours" id="businessHours" maxlength="20" placeholder="例如：9:30-23:00"  type="text"><em></em>
							</li>
							
							<li>
							     <%-- <font><b style="color: red">*</b>所在地区：</font>
								 <div class="select" style="width: 450px;display: inline-block;">
									<select class="combox " id="provinceid" name="provinceid"  requiredTitle="true"  childNode="cityid" selectedValue="${store.provinceid}"  retUrl="${contextPath}/common/loadProvinces">
										</select>
										<select class="combox " id="cityid" name="cityid"  requiredTitle="true"  selectedValue="${store.cityid}"   showNone="false"  parentValue="${store.provinceid}" childNode="areaid" retUrl="${contextPath}/common/loadCities/{value}">
										</select>
										<select class="combox "  id="areaid" name="areaid"   requiredTitle="true"  selectedValue="${store.areaid}"    showNone="false"   parentValue="${store.cityid}"  retUrl="${contextPath}/common/loadAreas/{value}">
										</select>
										<em></em>
								 </div> --%>
								 <font><b style="color: red">*</b>所在地区：</font>
								
								 <span id="shoplocation"></span>
								 <a id="relocationBtn" href="javascript:void(0);" class="relocation" style="color: #e5004f;text-decoration: underline;">重新获取位置</a>
								 <input type="hidden" id="shopAddr" name="shopAddr" value="${store.shopAddr}"/> <em></em>
								 
								  <!-- 高德地图 -->
								  <div class="map-wrap">
									<div class="map-search-box">
										<div class="search-input">
											<input type="text" id="searchInput" name="searchContent" placeholder="请输入要搜索的地区" />
											<input id="searchBtn" type="button" value="搜索"/>
										</div>
										<div id="searchPanel"></div>
									</div>
									<div class="map-box" id="mapBox"></div>
								  </div>
								  <!-- / 高德地图-->
								 
							</li>
							<%-- <li><font><b style="color: red">*</b>详细地址：</font>
							    <textarea  name="address" id="address"   maxlength="20">${store.address}</textarea><em></em>
							    </li> --%>
							<%-- <li><font>交通线路：</font>
							    <textarea  name="transitRoute"  maxlength="150">${store.transitRoute}</textarea><em></em>
							</li> --%>
							
						</ul>
						<div class="bid-sub" style="text-align:center;"><input class="btn-r big-btn" style="border: 0;" value="确定" type="submit"/></div>
					</div>
				</form:form>
		</div>
	</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script type="text/javascript" src="https://webapi.amap.com/maps?v=1.3&key=ff4caf1e49503a276af4baab40ed7d61"></script>
<script type="text/javascript" src="https://cache.amap.com/lbs/static/addToolbar.js"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/gaode-map.js'/>"></script>
<script type="text/javascript">

var contextPath = '${contextPath}';

function isBlank(value){
   if(value==null || value=="" || value==undefined){
      return true;
   }
   return false;
}		

//手机号码校验
jQuery.validator.addMethod("isPhone", function(value, element) {
    var length = value.length;
    var mobile = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(17[0-9]{1}))+\d{8})$/;
    return this.optional(element) || (length == 11 && mobile.test(value));
   }, "请填写正确的手机号码");//可以自定义默认提示信息
   
//密码校验
jQuery.validator.addMethod("checkPwd", function(value, element) {
    var length = value.length;
    /* var pwd = /(?!.*[\u4E00-\u9FA5\s])(?!^[a-zA-Z]+$)(?!^[\d]+$)(?!^[^a-zA-Z\d]+$)^.{6,20}$/; */
    var regx1= /^[0-9A-z`~!@#$%^&*()_\-+=<>?:"{}|,.\/;'\\[\]·~！@#￥%……&*（）——\-+={}|《》？：“”【】、；‘’，。、]+$/g;
    var regx= /(?!^[0-9]+$)(?!^[A-z]+$)(?!^[~\\`!@#$%\\^&*\\(\\)-_+={}|\\[\\];':\\\",\\.\\\\\/\\?]+$)(?!^[^A-z0-9]+$)^.{2,20}$/;
    return this.optional(element) || (length >= 2 && length<=20 && regx1.test(value) && regx.test(value) && value.indexOf(" ") < 0);
   }, "密码由2-20位字母、数字或符号(除空格)的两种及以上组成");//可以自定义默认提示信息

$(document).ready(function() {
       /*  //三级联动
  	    $("select.combox").initSelect();
  	    userCenter.changeSubTab("storeManage"); //高亮菜单
  	    
  	    //监听select框的change事件，获取选择的地址名称
	  	$("#provinceid").on("change", function(){
	  		var $this = $(this);
	  		var _val = $this.find("option:selected").html();
	  		$("#province").val(_val);
	  	});
	  	$("#cityid").on("change", function(){
	  		var $this = $(this);
	  		var _val = $this.find("option:selected").html();
	  		$("#city").val(_val);
	  	});
	  	$("#areaid").on("change", function(){
	  		var $this = $(this);
	  		var _val = $this.find("option:selected").html();
	  		$("#area").val(_val);
	  	}); */
  	     
	    jQuery("#form1").validate({
	    	errorPlacement: function(error, element) {
	 	    	element.parent().find("em").html("");
	 			error.appendTo(element.parent().find("em"));
	 		},
	 		ignore: "",
	 		 rules: {
	 		    userName : {
                   required : true,
                   rangelength:[2,20],
                   remote:{                                          //验证用户名是否存在
			　　               type:"POST",
			　　               url:contextPath+"/s/store/checkStoreName",             //servlet
			　　               data:{
			　　                    	userName:function(){return $("#storeName").val();},
			              				no_id:"${store.id}"
			　　                } 
			　　  } 
			　　},
	              passwd: {<c:if test='${empty store.id}'>required:true</c:if>, rangelength :[6,20],checkPwd:true},
           		  repassword: {<c:if test='${empty store.id}'>required:true,</c:if>equalTo:"#passwd"},
           		  name: {required:true},
           		  contactMobile: {required:true,isPhone:true},
           		  businessHours: {required:true},
           		  shopAddr:{required:true}
           		 /*  provinceid:{required:true},
          		  cityid:{required:true},
          		  areaid:{required:true}, */
           		  /* address: {required:true} */
	 		 },
	 		 messages: {
       			 userName:{required:"登录名不能为空！",rangelength:jQuery.format("登录名位数必须在{0}到{1}字符之间！"),remote:jQuery.format("登录名已经被注册")},
       			 passwd: {<c:if test='${empty store.id}'>required:"密码不能为空！",</c:if>rangelength:jQuery.format("密码位数必须大于等于6个字符！"),checkPwd:"密码由6-20位字母、数字或符号(除空格)两种及以上组成"},
                 repassword: {<c:if test='${empty store.id}'>required:"确认密码不能为空！",</c:if>equalTo:"两次密码输入不一致"},
                 name: {required:"请填写门店名称"},
                 contactMobile: {required:"请输入联系人手机",isPhone:"请填写正确的手机号码"},
                 businessHours: {required:"请输入营业时间"},
                 /* provinceid:{required:"请选择省份"},
           		 cityid:{required:"请选择城市"},
           		 areaid:{required:"请选择地区"}, */
           		 shopAddr: {required:"请选择门店所在地区"}
                 /* address: {required:"请填写详细地址"} */
	 		 }	        
	    });
     });
   </script>
</body>
</html>