<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
	<title>
		<c:choose>
			<c:when test="${not empty presellProd.metaTitle}">${presellProd.metaTitle}-${systemConfig.shopName}</c:when>
			<c:otherwise>${presellProd.prodName}-${systemConfig.shopName}</c:otherwise>
		</c:choose> 
	</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/presell/presell-prod-detail.css'/>" rel="stylesheet"/>
	<style type="text/css">
		.shopDetailSpan {
			text-align: center;
			width: 33.3%;
			display: inline-block;
		}

		.shopDetailNum {
			color: #e5004f;
			text-align: center;
			width: 33.3%;
			display: inline-block;
		}
	</style>
</head>
     <%@ include file="/WEB-INF/pages/plugins/main/included/rightSuspensionFrame.jsp" %>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		<input id="currSkuId" value="${presellProd.prodDto.skuId}" type="hidden" />
		<input id="selCityId" value="64" type="hidden" />
		<input id="selProvinceId" value="5" type="hidden" />
		<div id="bd">
			<!--crumb-->
			<div id="J_crumbs" class="crumb yt-wrap">
				<div class="crumbCon">
					<div class="crumbSlide">
						<div class="crumbs-nav-item one-level">
							<a class="crumbs-link" href="<ls:url address="/"/>"> 首页 </a><i class="crumbs-arrow">&gt;</i>
						</div>

						<c:choose>
							<c:when test="${not empty param.keyword}">
								<div class="crumbs-nav-item one-level">
									<a class="crumbs-link" href="javascript:void(0);"><c:out value="${param.keyword}"></c:out>
									</a> <i class="crumbs-arrow">&gt;</i>
								</div>
							</c:when>
							<c:when test="${not empty treeNodes && (fn:length(treeNodes) > 1)}">
								<c:forEach items="${treeNodes}" var="node" varStatus="n">
									<div class="crumbs-nav-item">
										<c:choose>
											<c:when test="${!n.last}">
												<div class="menu-drop">
													<div class="trigger">
														<span class="curr">${node.nodeName}</span><i class="menu-drop-arrow"></i>
													</div>
													<div class="menu-drop-main">
														<c:if test="${not empty node.compatriots}">
															<ul class="menu-drop-list">
																<c:forEach items="${node.compatriots}" var="compatriot" varStatus="c">
																	<li><a title="${compatriot.nodeName}" href="<ls:url address="/list?categoryId=${compatriot.selfId}"/>">${compatriot.nodeName}</a>
																	</li>
																</c:forEach>
															</ul>
														</c:if>
													</div>
												</div>
											</c:when>
											<c:when test="${n.last}">
												<a class="crumbs-link" href="javascript:void(0);">${node.nodeName}</a>
											</c:when>
										</c:choose>
										<c:if test="${!n.last}">
											<i class="crumbs-arrow">&gt;</i>
										</c:if>
									</div>
								</c:forEach>
							</c:when>
							<c:when test="${not empty treeNodes && (fn:length(treeNodes) <= 1)}">
								<c:forEach items="${treeNodes}" var="node" varStatus="n">
									<div class="crumbs-nav-item one-level">
										<a class="crumbs-link" href="javascript:void(0);"><c:out value="${node.nodeName}"></c:out>
										</a>
									</div>
								</c:forEach>
							</c:when>
							<c:otherwise></c:otherwise>
						</c:choose>

						<c:if test="${not empty  facetProxy.selections}">
							<div class="crumbs-nav-item">
								<i class="crumbs-arrow">&gt;</i>
							</div>
							<c:forEach items="${facetProxy.selections}" var="selections">
								<div class="crumbs-nav-item crumbAttr">
									<a href="javascript:void(0);">${fn:substring(selections.title,0,6)}:${selections.name}</a> <a href="javascript:void(0);" onclick="javascript:removeFacetData('${selections.url}');"
										class="crumbDelete">X</a>
								</div>
							</c:forEach>
						</c:if>
					</div>
				</div>
			</div>
			<!--crumb end-->

			<!--主要信息-->
			<div class="infomain yt-wrap">

				<div class="infomain yt-wrap">

					<!-- 店铺信息 -->
					<div class="info_u_r" <c:if test="${empty shopDetail.shopPic}">style="padding: 0px 7px;border:1px solid #eee;width: 190px;height: 249px;box-sizing: border-box;"</c:if> <c:if test="${!empty shopDetail.shopPic}">style="padding: 0px 7px;border:1px solid #eee;width: 190px;height: 323px;box-sizing: border-box;"</c:if>>
						<div class="qjdlogo" style="border-bottom:0px"  <c:if test="${empty shopDetail.shopPic}">style="border-bottom:0px;padding:0px"</c:if>
						<a href="<ls:url address='/store/${shopDetail.shopId}'/>" style="text-align: center;"> <c:if test="${not empty shopDetail.shopPic}">
							<img style="border: 0;vertical-align: middle; width: 180px;max-height: 75px" src="<ls:photo item='${shopDetail.shopPic}'/>">
						</c:if></a>
					</div>
					<div class="shopInfo" style="padding:0px">
						<a class="shopName" href="<ls:url address='/store/${shopDetail.shopId}'/>" target="_blank" title="${shopDetail.siteName}">${shopDetail.siteName}</a><c:choose>
						<c:when test="${shopDetail.shopType == 0}"><strong style="float: right">专营店</strong></c:when>
						<c:when test="${shopDetail.shopType == 1}"><strong style="float: right">旗舰店</strong></c:when>
						<c:when test="${shopDetail.shopType == 2}"><span style="float: right" class="ziying">平台自营</span></c:when>
					</c:choose>
					</div>
					<ul>
						<li><span style="float:left;">综合评分：</span> <span class="scores_scroll"> <span class="scroll_gray"></span> <span class="scroll_red" style="width: 20%"></span> </span><font id="shopScoreNum" class="hide">${sum}</font></li>
						<li style="border-bottom: 0px;height:25px"><span class="shopDetailSpan">描述</span><span class="shopDetailSpan">服务</span><span class="shopDetailSpan">物流</span></li>
						<li><span class="shopDetailNum">${prodAvg==null?'0.0':prodAvg}</span><span class="shopDetailNum">${shopAvg==null?'0.0':shopAvg}</span><span class="shopDetailNum">${dvyAvg==null?'0.0':dvyAvg}</span></li>
						<c:set var="contaceQQ" value="${fn:split(shopDetail.contactQQ, ',')[0]}"></c:set>
						<li>商城客服：<%-- <a href="http://wpa.qq.com/msgrd?v=3&uin=${contaceQQ}&site=qq&menu=yes" target="_blank">点击咨询</a> --%>
							<input type="hidden" id="user_name" value="${user_name}"/>
							<input type="hidden" id="user_pass" value="${user_pass}"/>
							<input type="hidden" id="shopId" value="${shopDetail.shopId}"/>
							<a href="javascript:void(0)" id="loginIm">联系卖家</a>
						</li>
					</ul>
					<div class="col-shop-btns">
						<a href="<ls:url address='/store/${shopDetail.shopId}'/>" target="_blank" style="font-size:12px;color:#ffffff;width:80px;height:30px;background:#333333;border:1px solid #333;">进入店铺</a>
						<a class="col-btn" href="javascript:void(0);" onclick="collectShop('${shopDetail.shopId}')" style="font-size:12px;color:#333333;width:80px;height:30px;background:#f5f5f5;border:1px solid #cccccc;">收藏店铺</a>
					</div>
				</div>


				<div class="clearfix info_u_l">
					<div class="tb-property advance-det">
						<div class="tb-wrap">
                            <div class="tb-detail-hd">      
                                <h1><a target="_blank" href="${contextPath }/views/${presellProd.prodId}"> ${presellProd.prodName}</a></h1>
                                <p class="newp">${presellProd.brief}</p>
                            </div>
                            <div class="shop-price">
								<span>商城价</span><small>￥<em id="cash">${presellProd.prodDto.cash}</em></small>
                            </div>
                            <div class="tm-price">
                              <div class="lef">
								  <span>预定价</span>¥<em id="presellPrice" >${presellProd.prodDto.prePrice}</em>
                                <c:if test="${presellProd.payPctType eq 1}">
									<em class="change">定金<i> ¥<em id="preDepositPrice">${presellProd.preDepositPrice}</em></i> + 尾款<i> ¥<em id="finalPayment">${presellProd.finalPayment}</em></i></em>
                                </c:if>
                                <c:if test="${presellProd.payPctType eq 0}">
									<em class="change">一次性付清<i> ¥<em class="onePrice">${presellProd.prePrice}</em></i></em>
                                </c:if>
                              </div>
                            </div>
                              
                              <!-- 活动状态时间 -->
                              <div id="activityStatus" class="activity-status-wrap" style="display:none;">
	                               <img src="${contextPath }/resources/common/images/time.png" alt=""/>
	                               <span class="status-label"> 结束时间：</span>
	                               <div class="status-time">
	                               		<span><em>0</em>时</span><span><em>0</em>分</span><span><em>0</em>秒</span><span></span>
	                              </div>
                              </div>

							<div class="tb-meta">
								<%-- <dl id="J_RSPostageCont" class="tm-delivery-panel clearfix">
									<dt class="tb-metatit">运费</dt>
									<dd>
										<div class="addrselector" id="storeSelector">
											<div class="text">
												<div title="广东 广州市 天河区">广东 广州市 天河区</div>
												<b></b>
											</div>
											<div class="content">
												<div id="JD-stock" class="m JD-stock" data-widget="tabs">
													<div class="mt">
														<ul class="tab" id="stockTab">
															<li data-index="0" class="" onclick="selectProvince(this);" provinceid="5"><a class="" href="javascript:void(0);"><em>广东</em><i></i> </a></li>
															<li data-index="1" class="" onclick="selectCity(this);"><a class="" href="javascript:void(0);" title="广州市"><em>广州市</em><i></i> </a></li>
															<li data-index="2" class="curr"><a class="hover" href="javascript:void(0);" title="天河区"><em>天河区</em><i></i> </a></li>
														</ul>
														<div class="stock-line"></div>
													</div>
													<div id="stock_province_item" data-widget="tab-content" data-area="0" class="mc" style="display: none;">
														<ul class="area-list">
															<c:forEach items="${provinces}" var="province">
																<li><a data-value="${province.id}" href="#none">${province.province}</a></li>
															</c:forEach>
														</ul>
													</div>
													<div id="stock_city_item" data-widget="tab-content" data-area="1" class="mc" style="display: none;">
														<ul class="area-list">
															<c:forEach items="${citys}" var="city">
																<li><a data-value="${city.id}" href="#none">${city.city}</a></li>
															</c:forEach>
														</ul>
													</div>
													<div id="stock_area_item" data-widget="tab-content" data-area="2" class="mc" style="display: block;">
														<ul class="area-list">
															<c:forEach items="${areas}" var="area">
																<li><a data-value="${area.id}" href="#none">${area.area}</a></li>
															</c:forEach>
														</ul>
													</div>
												</div>
												<span class="clr"></span>
											</div>
											<div onclick="javascript:closeStoreSelector();" class="close"></div>
										</div>
										<div  class="frei_div"><span id="stocksText"></span><span id="freightText"></span></div>
									</dd>
								</dl> --%>

								<%--<c:forEach items="${presellProd.prodPropDtos}" var="prop" >--%>
									<%--<dl class="tb-prop tm-sale-prop clearfix ">--%>
	                                    <%--<dt class="tb-metatit">${prop.memo }</dt>--%>
	                                    <%--<dd>--%>
		                                    <%--<ul class="clearfix J_TSaleProp  ">--%>
		                                    	<%--<c:forEach items="${prop.propValueDtos}" var="propValue">--%>
		                                         	<%--<li class="tb-selected" style="line-height:25px;">--%>
		                                         		<%--<a style="padding: 1px 10px;border: 2px solid #e5004f;">--%>
		                                         			<%--<c:if test="${not empty propValue.pic}">--%>
																<%--<img src="<ls:images item='${propValue.pic }' scale='3'/>" style="max-width: 25px;max-height: 25px;float: left;margin-right:5px;">--%>
															<%--</c:if>--%>
		                                         			<%--<span>${propValue.name }</span>--%>
		                                         		<%--</a>--%>
		                                         	<%--</li>--%>
		                                         <%--</c:forEach>--%>
		                                    <%--</ul>--%>
	                                    <%--</dd>--%>
	                                <%--</dl>--%>
	                             <%--</c:forEach>--%>
                                  <%--<div class="tb-prop tb-clear">--%>
                                    <%--<span class="tb-metatit">服务</span>按时发货 &nbsp;&nbsp; 极速退款 &nbsp;&nbsp;  七天无理由退换--%>
                                 <%--</div>--%>
                                 <!-- 商品属性 -->
									<div class="tb-meta properties" style="margin-top: 30px;color:#888;">
										<c:forEach items="${presellProd.prodDto.prodPropDtoList}" var="prop" varStatus="psts">
											<dl class="tb-prop tm-sale-prop clearfix ">
												<dt class="tb-metatit">${prop.propName}</dt>
												<dd>
													<ul class="clearfix J_TSaleProp" prop="${prop.propName}" id="prop_${psts.index}" index="${psts.index}" >
														<c:forEach items="${prop.productPropertyValueList}" var="propVal">
															<c:choose>
																<c:when test="${propVal.isSelected}">
																	<li  data-value="${prop.propId}:${propVal.valueId}" valId="${propVal.valueId}"   class="tb-selected">
																		<a href="javascript:void(0);" >
																			<c:if test="${not empty propVal.pic}">
																				<img src="<ls:images item='${propVal.pic }' scale='3'/>" style="margin: 1px 3px 1px 1px;display: inline-block;vertical-align: middle;width: 25px;height: 25px;">
																			</c:if>
																			<span>${propVal.name}</span>
																		</a>
																	</li>
																</c:when>
																<c:otherwise>
																	<li  data-value="${prop.propId}:${propVal.valueId}" valId="${propVal.valueId}" >
																		<a href="javascript:void(0);">
																				<%-- <img src="<ls:images item='${propVal.pic}' scale='3'/>"  style="margin: 1px 3px 1px 1px;display: inline-block;vertical-align: middle;width: 25px;height: 25px;"> --%>
																			<span>${propVal.name}</span>
																		</a>
																	</li>
																</c:otherwise>
															</c:choose>
														</c:forEach>
													</ul>
												</dd>
											</dl>
										</c:forEach>
									</div>
									<!-- 商品属性 end-->
	                               <dl class="tb-amount clearfix">
									<dt class="tb-metatit">数量</dt>
									<dd id="J_Amount">
										<span class="tb-amount-widget mui-amount-wrap"> <input type="text" title="请输入购买量" id="pamount" onblur="validateInput();" maxlength="8" value="1" class="tb-text mui-amount-input">
											<span class="mui-amount-btn"> <span class="mui-amount-increase" onclick="addPamount();">∧</span> <span class="mui-amount-decrease" onclick="reducePamount();">∨</span> </span> <span
											class="mui-amount-unit">件</span> </span>
									</dd>
								</dl>
                                 
                              <div class="tb-action clearfix">
                                   <div class="tb-btn-paydeposit"><a class="tb-btn-off"  href="javascript:void(0);"   id="J_LinkBasket">支付定金</a></div>                    
                              </div>
                                
							</div>

						</div>
					</div>

					<!--产品图片-->
					<%@include file="prodpics.jsp"%>
					<!--产品图片end-->

				</div>

			</div>
			
		<!--更多信息-->
		<div class="yt-wrap info_d clearfix">
			<!-- 商品浏览记录 -->
			<div class="left210_m"  id="visitedprod"></div>
			<!-- 商品详情 -->
			<div class="right210_m">
					<div class="detail">
						<div class="pagetab2" id="productTab">
							<ul>
								<li class="on" id="prodContent"><span>商品详情</span></li>
							</ul>
						</div>

						<div style="text-align:center;" class="content" id="prodContentPane">${presellProd.content }</div>
						
					</div>
				</div>
			</div>
		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	
	<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/jquery.countdown.min.js'/>"></script>
	<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/qrcode/jquery.qrcode.min.js'/>"></script>
	<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
	<script type="text/javascript">
			var currProdId = '${presellProd.prodId}';//当前商品ID
			var shopId = '${presellProd.shopId}';//店铺ID
			var presellId = '${presellProd.id}';//活动ID
			
			var startTime = Number('${presellProd.preSaleStart.time}');//预售活动开始时间
			var endTime = Number('${presellProd.preSaleEnd.time}');//预售活动结束时间
			var payPctType = '${presellProd.payPctType}';
			
			//运费相关
			var weight = Number('${presellProd.weight}');
			var volume = Number('${presellProd.volume}');
			var support_transport_tree = '${presellProd.supportTransportFree}';//是否由商家承担运费
			var transport_type = '${presellProd.transportType}';//固定运费或模板运费
			var transportId = Number('${presellProd.transportId}');//运费模板ID
			
			//各种快递的固定运费
			var mail_trans_fee = Number('${presellProd.mailTransFee}');
			var express_trans_fee = Number('${presellProd.expressTransFee}');
			var ems_trans_fee = Number('${presellProd.emsTransFee}');
			var loginUserName = '${loginUser}';

			//sku选择初始化数据
            var prodId =${presellProd.prodDto.prodId};
            var prodName = '${fn:escapeXml(presellProd.prodDto.name)}';
            var skuDtoList = eval('${presellProd.prodDto.skuDtoListJson}');
            var propValueImgList = eval('${presellProd.prodDto.propValueImgListJson}');
            var imgPath3 = "<ls:images item='PIC_DEFAL' scale='3' />";
            var _switch=0;

            var percent = ${empty presellProd.preDepositPrice?'1':presellProd.preDepositPrice};// 定金百分比
			var currSkuId = ${presellProd.prodDto.skuId}

	</script>
	<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/presell/presellDetail.js'/>"></script>
</body>
</html>