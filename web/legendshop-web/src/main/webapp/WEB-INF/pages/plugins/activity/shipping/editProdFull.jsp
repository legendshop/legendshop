<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/dialog.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>商家拼团活动</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/mergeGroup/mergeGroupEdit.css'/>" rel="stylesheet"/>
    <%-- <link type="text/css" href="<ls:templateResource item='/resources/templets/css/shipping/addShipping.css'/>" rel="stylesheet"/> --%>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/shipping/addProdFull.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/shipping/addFull.css'/>" rel="stylesheet"/>
    <script src="<ls:templateResource item='/resources/plugins/My97DatePicker/WdatePicker.js'/>" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/plugins/default/layer.css" />
</head>
<body>
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
	</div>
	<div class="w1190">
		<div class="seller-cru">
			<p>
				您的位置>
				<a href="javascript:void(0)">首页</a>>
				<a href="javascript:void(0)">卖家中心</a>>
				<span class="on">商品包邮</span>
			</p>
		</div>
		<!-- 侧边导航 -->
		<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
		<!-- /侧边导航 -->
		<div class="add-fight">
			<form id="prodShipping">
				<input type="hidden" value="${active.id}" id="activeId" name="activeId">
				<input type="hidden" value="${check}" id="check" name="check">
				<!-- 活动信息 -->
				<div class="add-item">
					<div class="add-tit"><span>活动信息</span></div>
					<div class="activity-mes">
						<span class="act-tit"><em>*</em>活动名称：</span>
						<input type="text" value="${active.name}" name="name" id="name" maxlength="25" class="act-name" placeholder="请填写活动名称" <c:if test="${not empty check && check eq 'check'}">disabled="disabled"</c:if>>
						<span class="act-des">（2-25）字</span>
						<div class="ant-form-explain" name="namePrompt" style="margin-left: 14%;display: none">活动名称限制2-25个字</div>
					</div>
					<div class="activity-mes">
						<span class="act-tit"><em>*</em>活动时间：</span>
						<span class="act-time">
							<lable><em style="color: #e5004f;margin-right: 5px;"></em></lable>
							<input type="text" readonly="readonly" placeholder="开始时间" name="startDate" disabled="disabled" size="21" id="startDate" value='<fmt:formatDate value="${active.startDate}" pattern="yyyy-MM-dd"/>'/><span></span>
							&nbsp;-&nbsp;
							<lable><em style="color: #e5004f;margin-right: 5px;"></em></lable>
							<input readonly="readonly" placeholder="结束时间" name="endDate" disabled="disabled" size="21" id="endDate" value='<fmt:formatDate value="${active.endDate}" pattern="yyyy-MM-dd"/>' type="text" /><span></span>
						</span>
						<span class="act-des">（编辑无法修改时间）</span>
					</div>
				</div>
				<!-- /活动信息 -->
				<!-- 活动规则 -->
				<div class="add-item">
					<div class="add-tit"><span>活动规则</span></div>
					<div class="activity-mes">
						<span class="act-tit"><em>*</em>满：</span>
						<c:if test="${active.reduceType==1 }">
							<input type="text" style="width: 60px;" autocomplete="off" disabled="disabled" value='<fmt:formatNumber value="${active.fullValue }" pattern="0.00"/>'>
						</c:if>
						<c:if test="${active.reduceType==2 }">
							<input type="text" style="width: 60px;" autocomplete="off" disabled="disabled" value='<fmt:formatNumber value="${active.fullValue }" pattern="0"/>'>
						</c:if>
						<c:if test="${empty active.reduceType || active.reduceType==1 }">元</c:if>
						<c:if test="${active.reduceType==2 }">件</c:if>
					</div>
					
					<div class="prodContext">
						<div class="add-tit" style="padding-bottom: 10px;border-bottom: 1px solid #f9f9f9;"><span>选择活动商品</span></div>
						
						<div class="form-box">
							<div class="ant-tabs ant-tabs-top ant-tabs-line">
								<div role="tablist" class="ant-tabs-bar" tabindex="0">
									<div class="ant-tabs-nav-container">
										<div class="ant-tabs-nav-wrap">
											<div class="ant-tabs-nav-scroll">
												<div class="ant-tabs-nav ant-tabs-nav-animated">
													<div class="ant-tabs-ink-bar ant-tabs-ink-bar-animated" style="display: block; transform: translate3d(0px, 0px, 0px); width: 96px;"></div>
													<div role="tab" aria-disabled="false" aria-selected="true" data-type="select" class="ant-tabs-tab-active ant-tabs-tab">选择商品</div>
													<div role="tab" aria-disabled="false" aria-selected="false" data-type="setting" class=" ant-tabs-tab">已选商品</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="ant-tabs-content ant-tabs-content-animated" style="margin-left: 0%;">
									<div role="tabpanel" aria-hidden="true" class="all-prod ant-tabs-tabpane ant-tabs-tabpane-active all">
									</div>
									
									<div role="tabpanel" aria-hidden="false" class="join-prod ant-tabs-tabpane ant-tabs-tabpane-active">
										
										<div class=" clearfix">
											<div class="ant-spin-nested-loading">
												<div class="ant-spin-container">
													<div class="ant-table ant-table-large ant-table-without-column-header ant-table-scroll-position-left">
														<div class="ant-table-content">
															<div class="ant-table-body">
																<table class="">
																	<colgroup>
																		<col>
																		<col style="width: 200px; min-width: 200px;">
																		<col style="width: 200px; min-width: 200px;">
																		<col style="width: 150px; min-width: 150px;">
																		<col style="width: 100px; min-width: 100px;">
																	</colgroup>
																	<thead class="ant-table-thead">
																		<tr>
																			<th class="ant-table-selection-column">
																			</th>
																			<th class=""><span>商品</span></th>
																			<th class=""><span>价格(元)</span></th>
																			<th class=""><span>库存</span></th>
																			<th class=""><span>操作</span></th>
																		</tr>
																	</thead>
																	<tbody class="ant-table-tbody selected">
																	<c:if test="${not empty requestScope.activeProdList}">
																		<c:forEach var="item" items="${requestScope.activeProdList}">
																			<tr class="ant-table-row  ant-table-row-level-0">
																				<td class="ant-table-selection-column">
																				</td>
																				<td class="">
																					<span class="ant-table-row-indent indent-level-0" style="padding-left: 0px;"></span>
																					<span class="good-info">
																						<img width="50" height="50" src="<ls:photo item='${item.pic}'/>" class="ant-table-row-img" alt="${item.prodName }">
																						<span class="name"><span>${item.prodName }</span></span>
																					</span>
																				</td>
																				<td class=""><span class="t-grayer">${item.price }</span></td>
																				<td class=""><span class="t-grayer">${item.actualStocks }</span></td>
																				<td class="">
																					已参加 <c:if test="${active.startDate>now}"><a onclick="cancelProd(${item.prodId})" class='pushl' data-id='${item.prodId}'>取消参加</a></c:if>
																				</td>
																			</tr>
																		</c:forEach>
																	</c:if>
																	</tbody>
																</table>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
	            		</div>
	           	 </div>
					
					<div class="activity-mes" style="margin: 35px 250px;">
						<span class="act-tit"></span>
						<a href="javascript:void(0)" class="lim-btn on" id="commit">确认保存</a>
					</div>
				</div>
				<!-- /活动规则 -->
			</form> 
		</div>
	</div>
	<%--取消参加提交表单--%>
	<form:form id="cancelProdForm" action="/s/shippingActive/cancelProd" method="get">
		<input type="hidden" id="prodId_cancel" name="prodId">
		<input type="hidden" id="activeId_cancel" name="activeId">
	</form:form>
	<!--foot-->
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	<!--/foot-->
</body>

<script src="${contextPath}/resources/common/js/jquery.validate.js"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/layer/layer.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/shipping/editProdShipping.js'/>"></script>
<script type="text/javascript">	
var contextPath = "${contextPath}";
var id = "${active.id}";
$(function() {
	var str = contextPath+"/s/shippingActive/editfullProd/"+id;
	$.ajax({
		url : str,
		type : "get",
		dataType : "html",
		success : function(result) {
			$(".all-prod").html(result);
		}
	});
	
	//定义prodId数组
	prodIdList = new Array;
	//定义参加数组
	joinProdIdList = new Array;
});

function cancelProd(prodId){

	var activeId = '${active.id}';
	$("#prodId_cancel").val(prodId);
	$("#activeId_cancel").val(activeId);
	$("#cancelProdForm").submit();
}
</script>
</html>