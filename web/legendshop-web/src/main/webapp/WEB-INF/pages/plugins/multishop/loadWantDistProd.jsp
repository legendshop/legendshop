<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<html>
<head>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
	<style type="text/css">
		html{font-size:13px;}
		table{    margin: 10px;
    border: 1px solid #ddd;}
		table tbody tr{height:70px;}
		table tbody tr td{border-bottom:1px solid #ddd;text-align: center;}
		table thead tr td{border-bottom:1px solid #ddd;text-align: center;padding:5px;background: #f9f9f9;}
	</style>
</head>
<body>
	<div id="distContent">
	 <form:form  action="${contextPath}/s/loadWantDistProd" id="form1" method="post">
	 	<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
	 	<div style="height:50px;background-color: #f9f9f9;border: 1px solid #ddd;font-size: 14px;margin: 10px;">
	 		<div style="padding-top:10px;padding-left:20px;">
			<input style="width:550px;border:1px solid #ddd;padding: 5px;"  type="text" name="name" id="name" maxlength="20" value="${prod.name}" size="20" placeholder="请输入商品名称123" />
			<input type="submit" value="搜索" style="outline: none;background: #e5004f;color: #fff;padding: 5px 10px;border: 0px;"/>
			</div>
		</div>
	 </form:form>
	<table>
		<thead>
			<tr><td width="150">图片</td><td width="450">名称</td><td width="110">操作</td></tr>
		</thead>
		<tbody>
		<c:if test="${empty requestScope.list}">
			<tr><td colspan="3" style="text-align: center;">没有找到符合条件的商品</td></tr>
		</c:if>
     	<c:forEach items="${requestScope.list}" var="product">
          <tr>
          	<td><img src="<ls:images item='${product.pic}' scale='3' />" ></td>
          	<td>${product.name}</td>
          	<td><button style="border:0px;"><a style="text-decoration: none;color:#333;padding: 5px;background: #999;color: #fff;" target="_blank" href="${contextPath}/views/${product.prodId}">查看</a></button>&nbsp;
          	<button style="padding: 5px;background: #e5004f;color: #fff;border: 0px;cursor: pointer;" onclick="chooseProd('${product.prodId}','${fn:escapeXml(fn:replace(product.name,"'","&acute;"))}','<ls:images item='${product.pic}' scale='1' />')" >选择</button></td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
     <div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
				            		<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
				     			 </div>
		  				</div>
      </div>
      <div id="distCommisContent" style="display:none;text-align: center;padding:60px 0;">
      	<input type="hidden" id="prodId" >
      	<img style="margin-bottom:20px;" src="" ><br>
      	<span ></span><br>
      	设置佣金比例(百分比)：<input style="width: 30px;margin-top:40px;" id="commisRate">%
      	<input onclick="addDistProd();" style="margin-left:5px;padding: 2px 7px;background: #e5004f;border: 0px;color: #fff;cursor: pointer;" type="button" value="保存">
      </div>
      <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
      <script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/loadWantDisProd.js'/>"></script>
		<script type="text/javascript">
			var contextPath="${contextPath}";
		</script>
</body>
</html>