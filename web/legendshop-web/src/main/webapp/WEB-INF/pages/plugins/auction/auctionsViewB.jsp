<!DOCTYPE html>
	<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
	<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
	<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
	<jsp:useBean id="nowDate" class="java.util.Date" /> 
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>保证金拍卖活动-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/auctionView.css'/>" rel="stylesheet"/>
</head>
<%@ include file="/WEB-INF/pages/plugins/main/included/rightSuspensionFrame.jsp" %>
<body class="graybody">
	
	<div id="doc">
	  <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
	</div>
   
   <div id="bd"> 
      <div id="bd">
       <div class="seller-cru" style="width:1190px;margin: 20px auto 10px;">
	        <p>
	          您的位置&gt;
	          <a href="${contextPath}/home">首页</a>&gt;<a href="${contextPath}/auction/autionList">拍卖活动</a>&gt;
	          <span style="color: #e5004f;">${auction.auctionsTitle}</span>
	        </p>
	      </div>
            <div class="crumb yt-wrap" id="J_crumbs"> </div>            
            <!--crumb end-->
            
            <!--主要信息-->
             <div class="infomain yt-wrap" style="border-width: 1px 0px 0px;">
               <%--<div class="bidding-note">
                 <p id="bidCount">出价记录<span>（共0次出价）</span></p>
                 <table cellspacing="0" cellpadding="0" class="records">
                   <tbody>
                   <tr>
                    <td>时间</td>
                    <td>详情</td>
                    <td>状态</td>
                  	</tr>
                 </tbody></table>
                 <div class="more"><a href="javascript:loadMore();">查看更多</a></div>
               </div>--%>
			   <!-- 店铺信息 -->
			   <div class="info_u_r" <c:if test="${empty shopDetail.shopPic}">style="padding: 0px 7px;border:1px solid #eee;width: 190px;height: 249px;box-sizing: border-box;"</c:if> <c:if test="${!empty shopDetail.shopPic}">style="padding: 0px 7px;border:1px solid #eee;width: 190px;height: 323px;box-sizing: border-box;"</c:if>>
				   <div class="qjdlogo" style="border-bottom:0px"  <c:if test="${empty shopDetail.shopPic}">style="border-bottom:0px;padding:0px"</c:if>
				   <a href="<ls:url address='/store/${shopDetail.shopId}'/>" style="text-align: center;"> <c:if test="${not empty shopDetail.shopPic}">
					   <img style="border: 0;vertical-align: middle; width: 180px;max-height: 75px" src="<ls:photo item='${shopDetail.shopPic}'/>">
				   </c:if></a>
			   </div>
			   <div class="shopInfo" style="padding:0px">
				   <a class="shopName" href="<ls:url address='/store/${shopDetail.shopId}'/>" target="_blank" title="${shopDetail.siteName}">${shopDetail.siteName}</a><c:choose>
				   <c:when test="${shopDetail.shopType == 0}"><strong style="float: right">专营店</strong></c:when>
				   <c:when test="${shopDetail.shopType == 1}"><strong style="float: right">旗舰店</strong></c:when>
				   <c:when test="${shopDetail.shopType == 2}"><span style="float: right" class="ziying">平台自营</span></c:when>
			   </c:choose>
			   </div>
			   <ul>
				   <li><span style="float:left;">综合评分：</span> <span class="scores_scroll"> <span class="scroll_gray"></span> <span class="scroll_red" style="width: 20%"></span> </span><font id="shopScoreNum" class="hide">${sum}</font></li>
				   <li style="border-bottom: 0px;height:25px"><span class="shopDetailSpan">描述</span><span class="shopDetailSpan">服务</span><span class="shopDetailSpan">物流</span></li>
				   <li><span class="shopDetailNum">${prodAvg==null?'0.0':prodAvg}</span><span class="shopDetailNum">${shopAvg==null?'0.0':shopAvg}</span><span class="shopDetailNum">${dvyAvg==null?'0.0':dvyAvg}</span></li>
				   <c:set var="contaceQQ" value="${fn:split(shopDetail.contactQQ, ',')[0]}"></c:set>
				   <li>商城客服：<%-- <a href="http://wpa.qq.com/msgrd?v=3&uin=${contaceQQ}&site=qq&menu=yes" target="_blank">点击咨询</a> --%>
					   <input type="hidden" id="user_name" value="${user_name}"/>
					   <input type="hidden" id="user_pass" value="${user_pass}"/>
					   <input type="hidden" id="shopId" value="${shopDetail.shopId}"/>
					   <a href="javascript:void(0)" id="loginIm">联系卖家</a>
				   </li>
			   </ul>
			   <div class="col-shop-btns">
				   <a href="<ls:url address='/store/${shopDetail.shopId}'/>" target="_blank" style="font-size:12px;color:#ffffff;width:80px;height:30px;background:#333333;border:1px solid #333;">进入店铺</a>
				   <a class="col-btn" href="javascript:void(0);" onclick="collectShop('${shopDetail.shopId}')" style="font-size:12px;color:#333333;width:80px;height:30px;background:#f5f5f5;border:1px solid #cccccc;">收藏店铺</a>
			   </div>
			 </div>


               <div class="clearfix info_u_l2" style="overflow: hidden;">
                   <div class="tb-property">
                       <div class="tb-wrap" style="border-left: 0;">
                            <div class="tb-detail-hd">      
                                <h1>${auction.prodName}</h1>
                                
                                <p class="newp">
                                	<c:if test="${not empty auction.cnProperties}">
                                		${auction.cnProperties}
                                	</c:if>
                                </p>
                                <div class="go-shop">
					              <a style="color:#999;margin-left: 0px;" target="_blank" href="${contextPath}/views/${auction.prodId}">去商城查看商品>> </a>
					            </div>
                            </div>
                            
                            	 <c:choose>
                            	 	<c:when test="${nowDate lt auction.startTime}">
                             			<div class="pm-status" id="auctionStatus2">
			                              <div class="pm-status-mark">等待开拍</div>
			                              <span style="margin-left: 13px;">距离开拍仅剩</span><div class="pm-status-time" style="display:inline-block;margin-left:10px;"><span><em>--</em>天<em>--</em>时<em>--</em>分</span></div>
			                              <div class="pm-status-times"><span><em id="accessSignNum">${auction.enrolNumber} </em>人已报名</span><span><em id="accessNum">${auction.crowdWatch} </em>次围观</span></div>
                            			</div>
                            	 	</c:when>
                            	 	<c:when test="${nowDate gt auction.endTime}">
                             			<div id="auctionStatus1">
	                           			 <div class="pm-status" >
				                              <div class="pm-status-mark"  style="background: #999;">拍卖结束</div>
				                              <div class="pm-status-time"><span>结束时间：<fmt:formatDate value='${auction.endTime}' pattern='yyyy-MM-dd HH:mm:ss'></fmt:formatDate></div>
				                              <div class="pm-status-times"><span><em id="accessSignNum">${auction.enrolNumber}</em>人已报名</span><span><em id="accessNum">${auction.crowdWatch}</em>次围观</span></div>
				                            </div>
				                            <c:choose>
												<c:when test="${auction.biddingNumber gt 0}">
													<div id="auctionSuccess" class="tm-price" style="margin: 15px 15px 0px;">
														<span style="margin-right: 35px;color: #888;"><em
															style="font-size: 18px; color: #666;"><img
																src="${contextPath}/resources/templets/images/bidding-g.png"
																alt="">拍卖已成交</em>
														</span>
													</div>
												</c:when>
												<c:otherwise>
													<div id="auctionSuccess" class="tm-price" style="margin: 15px 15px 0px;">
														<span style="margin-right: 35px;color: #888;"><em
															style="font-size: 18px; color: #666;"><img
																src="${contextPath}/resources/templets/images/bidding-g.png"
																alt="">商品已流拍</em>
														</span>
													</div>
												</c:otherwise>
											</c:choose>
                            			</div>
                            	 	</c:when>
                            	 	<c:otherwise>
                            	 		 	<div class="pm-status" id="auctionStatus0">
			                             		 <div class="pm-status-mark" style="padding: 9px 8px 0px 12px;">正在进行</div>
			                              <span style="margin-left: 13px;">距离结束仅剩</span><div class="pm-status-time" style="display:inline-block;margin-left:10px;"><span><em>--</em>天<em>--</em>时<em>--</em>分</span></div>
                            				<div class="pm-status-times"><span><em id="accessSignNum">${auction.enrolNumber} </em>人已报名</span><span><em id="accessNum">${auction.crowdWatch} </em>次围观</span></div>
                            				</div>
                            	 	</c:otherwise>
                           
                            	 </c:choose>
                              
	                            <div class="tm-price" style="margin: 5px 15px 0px;">
	                              <span style="margin-right: 35px;color: #888;" id="shopPrice" >当前价：<em style="font-size: 24px; color: #666;">¥${auction.curPrice }</em></span>
	                              <span style="margin-right: 35px;color: #888;">出价次数：<em style="font-size: 14px; color: #e5004f;" id="bidCountNumber">${auction.biddingNumber }</em>次</span>
	                            </div>
	                            
	                            <div class="tm-price" style="margin: 0px 15px 0px;">
	                              <span style="margin-right: 51px;color: #888;">保证金：<em style="font-size: 24px; color: #e5004f;">¥${auction.securityPrice }</em></span>
	                              <span style="margin-right: 35px;color: #888;">不成拍则结束后退还</span>
	                            </div>
                            
                            <%--
                            <div class="tm-price" style="margin: 15px 15px 0px;display: none;" id="paimai_over">
                              <span style="margin-right: 35px;color: #888;"><img src="<ls:templateResource item='/resources/templets/images/bidding-g.png'/>" alt="" ><em style="font-size: 18px; color: #666;" id="pm-operation-over">拍卖已成交</em></span>
                            </div>
                             --%>
                             <div class="ipu-price" id="ipu-bid-price" style="display: none;margin: 20px 15px 0;">
                               <p><a href="javascript:decre();">－</a><input type="text" id="bidPrice" ><a href="javascript:incre();">＋</a></p>
                               <span>最低加价：<em>¥${auction.minMarkupRange}</em></span><span>最高加价：<em>¥${auction.maxMarkupRange}</em></span>
                            </div>
                            
                             <div class="bid-but" style="margin-top:20px !important;">
	                                 <div id="pm-operation-ensure-n" style="display: none;">
		                                 <a href="javascript:void(0);" onclick="giveMoney()" style="margin-right: 15px;width: 220px;">
		                                   <img src="${contextPath}/resources/templets/images/bidding-s.png" alt="">交保证金报名
		                                 </a> 
	                                 </div> 

                                  <div id="pm-operation-start-n" style="display: none;" >
	                                   <a target="_blank" style="margin-right: 15px;background-color:#888;text-align:center">等待开拍</a>
	                                    </a>
                                  </div>
                                  
                                  <div id="pm-operation-start-y" style="display: none;">
	                                  <a href="javascript:void(0);" onclick="bid();">
                             	       <img src="${contextPath}/resources/templets/images/bidding-s.png" alt="">我要出价
                             	     </a> 
                                  </div>
                                  
                            </div>

                            <div class="pm-att" style="margin-top: 15px;">
                              <ul>
                              <c:if test="${auction.hideFloorPrice==0}">
                                <li class="fore1">
                                  <span>起 拍 价：</span>
                                  <span>¥${auction.floorPrice}</span>
                                </li>
                              </c:if>
                                <%-- <li class="fore2">
                                  <span>加价幅度：</span>
                                  <em>¥${auction.minMarkupRange}至${auction.maxMarkupRange }</em>
                                </li> --%>
                                <li class="fore1">
                                  <span>延时周期：</span>
                                  <span>${auction.delayTime}分钟/次</span>
                                </li>
                                <c:if test="${auction.isCeiling eq 1}">
	                                <li class="fore3">
	                                  <span>一 口 价：</span>
	                                  <span>${auction.fixedPrice}</span>
	                                </li>
                                </c:if>
                              </ul>
                              <%-- <ul>
                                <li class="fore1">
                                  <span>延时周期：</span>
                                  <span>${auction.delayTime}分钟/次</span>
                                </li>
                              </ul>  --%>
                            </div>
						    <div class="pm-att" style="border:none;">
							   <ul>
								   <li class="fore1">
									   <span>01 交保证金 > 02 参与拍卖 > 03 竞拍成功 > 04 支付货款 > 05 交易成功</span>
								   </li>
							   </ul>
							</div>
                            
                       </div>
                   </div>    
	              
	               <%@include file="prodpics.jsp"%>
               </div>
				  <p id="bidCount" style="margin-bottom: 10px;">
					  出价记录<span>（共0次出价）<font color="#e5004f">*</font>当前只展示前20条出价记录</span>
				  </p>
				  <div class="bidding-step-not">

					  <table id="bidding-list" cellspacing="0" cellpadding="0" class="records default-table">
						  <thead>
						  <tr style="background: #fafafa;">
							  <th>时间</th>
							  <th>出价人</th>
							  <th>价格</th>
							  <th>状态</th>
						  </tr>
						  </thead>
						  <tbody class="bidding-list-body"></tbody>
					  </table>
				  </div>
            </div>
             
            <!--主要信息 end-->

            <!--更多信息-->
            <div class="yt-wrap info_d clearfix">
               <div class="right210_m" style="margin-left: 0;">
                    <div class="detail" id="detail">
						<div class="pagetab2" style="width: 1190px;">
						   <ul>
							 <li class="on" id="dec"  onclick="auctionDes()"><span>拍品详情</span></li>
							 <li id="bidlists" onclick="bidLists();"><span>出价记录</span></li>
						   </ul>
						</div>
						<div class="tabcon" style="background:#f6f6f6; zoom:1; text-align: center;">
						  ${auction.auctionsDesc}
						</div>
                     </div>
               </div>
            </div>
     </div>
   </div>
   
        <div class="pop-up" id="pay_overlay" style="display: none;">
		  <div class="pop-warm">拍卖成功</div>
		  <div class="pop-up-dow" >
			  <ul style="text-align: center; margin-top: 10px;">
				  <li>恭喜获拍！</li>
				  <li>请在24小时之内提交订单;若在24小时内未提交订单，系统将自动关闭交易，并扣除您的保证金用于赔付商家</li>
				  <li style="color: #666;"><span id="sp_Number">5</span>秒钟后将自动跳转结算页...</li>
			  </ul>
			 <p ><a  href="${contextPath}/p/auctionBidRec">立即跳转</a></p>
		  </div>
       </div>
      <div class="black_overlay" ></div>  
      
      
   	  <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/jquery.countdown.min.js'/>"></script>
<script type="text/javascript" >

	function yes(){
		window.location.reload();
	}
	var contextPath="${contextPath}";
	 //拍卖状态 0 等待拍卖 1 正在拍卖 2 拍卖结束
	var auctionStatus;

	//是否有缴纳保证金:0.没资格 1.有资格
	var qualification ;

	//拍卖编号
	var paimaiId="${auction.id}";
	//剩余时间
	var remainTime;
	//查询起始
	var queryStart=0;
	//查询结束
	var queryEnd=9;
	//封顶价
	var maxPrice=Number("${auction.fixedPrice}");
	//当前价
	var currentPrice=1;
	//起拍价
	var startPrice=Number("${auction.floorPrice}");
	//最低加价幅度
	var priceLowerOffset=Number("${auction.minMarkupRange}");
	//最高加价幅度
	var priceHigherOffset=Number("${auction.maxMarkupRange}");

	var loginUserName = '${loginUserName}';

	var prodId="${auction.prodId}";

	var skuId="${auction.skuId}";

	var endTime="<fmt:formatDate value='${auction.endTime}' pattern='yyyy-MM-dd HH:mm:ss'></fmt:formatDate>";

	var time="${time}";

	var runTime="${runTime}";

	var timeStatus="${timeStatus}";
	//保证金额度
	var ensurePrice;

	var isCeiling=Number("${auction.isCeiling}");

	//出价人id
	var buyUserId="${buyUserId}";
	//商家id
	var userId="${auction.userId}";

	//是否是延时拍卖
</script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/auction/auctionViewB.js'/>"></script>
<script type="text/javascript"	src="<ls:templateResource item='/resources/common/js/table-autoplay.js'/>"></script>
<script type="text/javascript"	src="<ls:templateResource item='/resources/common/js/jquery.newsTicker.min.js'/>"></script>
</body>
</html>