<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/pages/plugins/multishop/back-common.jsp"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss" var="nowDate" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<title>门店管理-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css"  href="<ls:templateResource item='/resources/templets/css/active/actives${_style_}.css'/>" rel="stylesheet">
<link type="text/css"  href="<ls:templateResource item='/resources/templets/css/base.css' />  rel="stylesheet">
<link type="text/css"  href="<ls:templateResource item='/resources/templets/css/seckill/seckill-activity${_style_}.css'/>" rel="stylesheet">
<style type="text/css">
	.set-up a:hover {
		color: #e5004f;
	}
</style>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include
			file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置> <a href="${contextPath}/home">首页</a>> <a
						href="${contextPath}/sellerHome">卖家中心</a>> <span class="on">门店管理</span>
				</p>
			</div>

			<%@ include
				file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp"%>


			<div id="rightContent" class="right_con">
				<!--page-->

				<!-- 秒杀活动列表 -->
				<div class="seckill-activity" style="margin-top: 21px">
					<div class="pagetab2">
						<ul id="listType">
							<li class="on"><span>门店管理</span></li>
						</ul>
		   			</div>
					<div>
						<div class="sold-ser sold-ser-no-bg"  style="overflow:hidden;text-align:right;">
							<input type="text" onclick="window.location='${contextPath}/s/store/load'" value="添加门店" class="btn-r big-btn" style="cursor: pointer;"/>
			   				<a href="${contextPath}/storeLogin" class="btn-r big-btn" target="_blank" style="margin-left: 10px;line-height:30px;">登录门店</a>
						</div>
					</div>
					<table class="seckill-table sold-table">
						<tr class="seckill-tit">
							<th>门店账号</th>
							<th>门店名称</th>
							<th>门店地址</th>
							<th>状态</th>
							<th>操作</th>
						</tr>
						  <c:choose>
					           <c:when test="${empty list}">
					              <tr>
								 	 <td colspan="20">
								 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
								 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
								 	 </td>
								 </tr>
					           </c:when>
					           <c:otherwise>
					           	    <c:forEach items="${list}" var="store" varStatus="status">
										<tr class="detail">
											<td style="width:141px">${store.userName}</td>
											<td style="width:141px">${store.name}</td>
											<td style="width:215px">${store.province}${store.city}${store.area}${store.address}</td>
											<td style="width:71px">
											  <c:choose>
													<c:when test="${store.isDisable}">正常</c:when>
													<c:otherwise>已冻结</c:otherwise>
												</c:choose>
											</td>
											<td style="width: 250px">
												<ul class="mau" style="width:260px;">
													<li style="margin-left:0px;"><a  href="${contextPath}/s/store/load/${store.id}"
																class="btn-g" style="height:30px;">修改</a></li>
														
														<li style="margin-left:0px;"><a  href="javascript:updatePasswd('${store.id}');"
																class="btn-g" style="height:30px;">修改密码</a></li>
																
														<li style="margin-left:0px;"><a onclick="deleteStore('${store.id}')"
																href="javascript:void(0);" 
																class="btn-g" style="height:30px;">删除</a></li>
														 <c:choose>
													<c:when test="${store.isDisable}">
															                   <li style="margin-left:0px;"><a onclick="offlineStore('${store.id}')"
																href="javascript:void(0);" name="statusImg"
																class="btn-g" style="height:30px;">下线</a></li>
															         </c:when>
													<c:otherwise>
														  <li style="margin-left:0px;"><a onclick="onlineStore('${store.id}')"
																href="javascript:void(0);" name="statusImg"
																class="btn-g" style="height:30px;">上线</a></li>
													 </c:otherwise>
												</c:choose>
														
												</ul>
											</td>
										</tr>
									</c:forEach>
					           </c:otherwise>
				           </c:choose>
					</table>
				</div>
				<!-- 秒杀活动列表 end -->
				<!--page-->
				
				<div class="clear"></div>
				 <div style="margin-top:10px;" class="page clearfix">
     			   <div class="p-wrap">
						<span class="p-num">
							<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
						</span>
				  </div>
		  		</div>
			</div>


		</div>
		<%@ include
			file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<script type="text/javascript">
	
	var contextPath="${contextPath}";
	var host = window.location.host;
	var storeLoginPath=host+contextPath+"/storeLogin"
	$("#storeLoginPath").text(storeLoginPath);
	
	    userCenter.changeSubTab("storeManage"); //高亮菜单
       	function pager(curPageNO){
       	    window.location.href="${contextPath}/s/store/query?curPageNO="+curPageNO;
    	}
    	
	    //上线店铺
    	function onlineStore(_id) {
			layer.confirm("确定要上线该门店吗？",{
	      		 icon: 3
	      	     ,btn: ['确定','取消'] //按钮
	      	   },function() {
				window.location.href = contextPath + "/s/store/onlineStore/"+ _id;
			});
		}
		
    	//下线店铺
		function offlineStore(_id) {
			layer.confirm("确定要下线该门店吗？", {
	      		 icon: 3
	      	     ,btn: ['确定','取消'] //按钮
	      	   },function() {
				window.location.href = contextPath + "/s/store/offlineStore/"+ _id;
			});
		}
		
		//删除店铺
		function deleteStore(_id) {
			layer.confirm("确定要删除该门店吗？(该门店所有的数据将会不可以恢复,请慎重)", {
	      		 icon: 3
	      	     ,btn: ['确定','取消'] //按钮
	      	   },function() {
				window.location.href = contextPath + "/s/store/delete/"+ _id;
			});
		}
		
		//修改密码
		function updatePasswd(id){
			layer.open({
				  title :"修改门店密码",
				  id: "updateStorePwd",
				  type: 2, 
				  resize: false,
				  content: [contextPath+"/s/store/updatePasswd/"+id,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
				  area: ['400px', '250px']
				}); 
		}
	</script>
</body>
</html>
