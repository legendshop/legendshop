<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>库存预警-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-prod${_style_}.css'/>" rel="stylesheet">
    <style type="text/css">
    	.bat-oper-btn2 {border-radius: 2px;text-align: center;background-color: #e5004f;color: #fff !important;padding: 5px 10px;font-size: 12px;border: 0px;cursor: pointer;}
    </style>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">库存预警</span>
				</p>
			</div>
			<%@ include file="included/sellerLeft.jsp" %>
			<div class="seller-selling">
				<div class="pagetab2">
					<ul id="listType">
						<li class="on"><span>库存预警</span></li>
					</ul>
	   			</div>
				<!-- 搜索 -->
				<form:form action="${contextPath}/s/stock/warning" method="get" id="warningFrom">
					<div class="sold-ser sold-ser-no-bg clearfix">
						<div class="fr">
							<div class="item">
								商品名称：
								<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
								<input type="text" class="item-inp" style="width:300px;" name="productName"  id ="name" value="${searchProName }" />
								<input type="button" onclick="search()" class="btn-r" id="btn_keyword" value="搜  索" />	
							</div>
						</div>
					</div>
				</form:form>
		
		<table class="selling-table sold-table">
			<tr class="selling-tit">
				<th>商品</th>
				<th width="25%">商品名称</th>
				<th width="20%">商品属性</th>
				<th>库存</th>
				<th>预警库存</th>
				<th>状态</th>
				<th style="border-right: 1px solid #e4e4e4">操作</th>
			</tr>
			<c:choose>
	           <c:when test="${empty requestScope.list}">
	              <tr>
				 	 <td colspan="20" style="border-right: 1px solid #e4e4e4">
				 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
				 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
				 	 </td>
				 </tr>
	           </c:when>
	           <c:otherwise>
	              <c:forEach items="${requestScope.list}" var="item" varStatus="status">
						<tr class="detail">
							<td class="check">
								<img src="<ls:images scale="3" item='${item.picture}'/>"/>
							</td>
							<td>
								<span>${item.productName}</span>
							</td>
							<td><span>${item.cnProperties}</span></td>
							<td><span class="col-orange">${item.stocks}</span></td>
							<td><span>${item.stocksArm}</span></td>
							<td sts="${item.status}">
								<c:if test="${item.status eq 1}">上线</c:if>
								<c:if test="${item.status eq 0}">下线</c:if>
							</td>
							<td>
							<div class="am-btn-group am-btn-group-xs">
		                   		<button class="updateButton bat-oper-btn2" onclick="addStock(${item.stocks},${item.skuId},${item.prodId })">
		                   		<span>更新库存</span></button>
		              		</div>
							</td>
						</tr>
					</c:forEach>
	           </c:otherwise>
		     </c:choose>
		</table>
		<div class="clear"></div>
		<div style="margin-top:10px;" class="page clearfix">
   			<div class="p-wrap">
           		<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
   			 </div>
		</div>
		</div>
		</div>
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/stockWarning.js'/>"></script>
	<script type="text/javascript">
		var contextPath="${contextPath}";
	</script>
</body>
</html>
