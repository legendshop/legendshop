<!DOCTYPE html>
<html>
<head>
   <%@ page contentType="text/html;charset=UTF-8" language="java"%>
   <%@include file='/WEB-INF/pages/common/taglib.jsp'%>
   <%@ include file="/WEB-INF/pages/common/layer.jsp"%>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
  <c:set var="indexJpgList"  value="${indexApi:findIndexjpg()}"></c:set>
  <title>商品首页-${systemConfig.shopName}-${systemConfig.title}</title>
  <meta name="keywords" content="${systemConfig.keywords}"/>
  <meta name="description" content="${systemConfig.description}" />
  <link type="text/css" href="<ls:templateResource item='/resources/templets/css/index${_style_}.css'/>" rel="stylesheet"/>
</head>
<body  class="graybody">
	<div id="doc">
	    <input id="isIndex" type="hidden" value="1">
	    <%@ include file="home/top.jsp" %>
			<div id="bd">
			       <!--banner-->
			      <div id="tab_nav" style="overflow: hidden;">

			            <div class=" pannels flexslider" style="overflow: visible; position: relative; z-index: 0;">
							<ul class="slides">
							  <c:forEach items="${indexJpgList}" var="banner">
								<li onclick="window.location='${banner.link}'" class="tab_pannel lazy" data-original="<ls:photo item='${banner.img}'/>" style="background: url(${contextPath}/resources/common/images/loading1.gif) no-repeat center top; display: block;">
								</li>
							 </c:forEach>
							</ul>
						</div>
			        </div>
			<div id="floorList">
			</div>
	</div>
		<%@ include file="home/bottom.jsp" %>
	</div>
	<%@ include file="included/rightSuspensionFrame.jsp" %>
	<script type="text/javascript" src="${contextPath}/resources/templets/js/jquery.flexslider-min.js"></script>
	<script type="text/javascript" src="${contextPath}/resources/common/js/jquery.lazyload.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$(".flexslider").flexslider({
			pauseOnAction: false,
		 	animation: "fade",
		 	slideDirection: "horizontal",
		 	animationDuration: 600,
		 	mousewheel: false,
		 	directionNav: true
		});

		//装载楼层
		loadFloor();
		$(".tab_pannel.lazy").lazyload({
		    effect : "fadeIn"
		});

		//广告流量点击 js
		var ua = navigator.userAgent;
	    var clickType = (ua.match(/iPad/i)) ? "touchstart" : "click";
		// $(document).delegate("a",clickType, function(){
		// 	var urlLink = this.href;
		// 	if(urlLink.indexOf("http")>=0){
		// 		recAdv(urlLink);
		// 	}
		// });
	});

	//广告流量点击 js
	function recAdv(urlLink){
		$.ajax({
			url: "${contextPath}/advAnalysis?urlLink="+urlLink,
			type:'get',
			contentType:'json',
			async : true, //默认为true 异步
			cache: false,
			success:function(result){

			}
		});
	}

	function loadFloor(){
			$.ajax({
			url:"${contextPath}/loadFloor",
			type:'get',
			async : true, //默认为true 异步
			success:function(result){
			   $("#floorList").html(result);
			}
			});
	}
</script >
</body>
</html>