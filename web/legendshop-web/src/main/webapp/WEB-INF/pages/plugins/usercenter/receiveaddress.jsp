<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>地址管理 - ${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css"
	href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>"
	rel="stylesheet" />
<link type="text/css"
	href="<ls:templateResource item='/resources/templets/css/accountBinding${_style_}.css'/>"
	rel="stylesheet" />
</head>
<style>
.btn-q{
    color: #333;
    font-weight: 400;
    display: inline-block;
    line-height: 28px;
    margin-left: 5px;
}
</style>
<body class="graybody">
	<div id="bd">
		<%@ include
			file="/WEB-INF/pages/plugins/main/home/top.jsp"%>

		<!----两栏---->
		<div class=" yt-wrap" style="padding-top: 10px;">
			<%@include file='usercenterLeft.jsp'%>

			<!-----------right_con-------------->
			<div class="right_con" id="content">
				<div class="right">
					<div class="o-mt">
						<h2>收货地址</h2>
					</div>
					<div class="pagetab2">
		                <ul>
		                    <li class="on"><span>收货地址</span></li>
		                </ul>
		            </div>
					<div id="addressList" class="o-m">
						<div class="o-mb" style="margin: 10px 0;">

							<h3>
								<input type="hidden" id="curPageNO" name="curPageNO" value="1" />
								<a href="javascript:void(0)" class="btn-r" style="width:100px;margin-left:0;" onclick="alertAddAddressDiag(0)">新增收货地址</a> 
								<span class="ftx-03 btn-q"> 您已创建
									<span class="ftx-04" id="addressNum_top"> ${total} </span>个收货地址，最多可创建<span class="ftx-04">20</span>个
								</span>
							</h3>
						</div>
						<div class="clear"></div>
						<c:forEach items="${requestScope.list}" var="deliveryAddress" varStatus="status">
							<div class="m m4 " id="addresssDiv"
								style="width: 470px; float: left; margin-right: 10px;">
								<div class="mt">
									<h3>
										<span class="ftx-03" style="color: #666;max-width: 290px;text-overflow: -o-ellipsis-lastline;overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 1;-webkit-box-orient: vertical;">
											${deliveryAddress.aliasAddr}
										</span>
									</h3>
									<div class="extra">
										<a href="javascript:void(0);"
											onclick="alertEditAddressDiag(${deliveryAddress.addrId});">修改地址信息</a>
										<a href="javascript:void(0);" class="del"
											onclick="delAddress(${deliveryAddress.addrId});">删除</a>
									</div>
								</div>
								<div class="mc">
									<div class="items new-items">
										<div class="item-lcol"
											style="width: 315px; border-right: 1px dashed #E5E5E5;">
											<div class="item">
												<span class="label">收货人：</span>
												<div class="fl" style="max-width: 230px;text-overflow: -o-ellipsis-lastline;overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 1;-webkit-box-orient: vertical;">
													${deliveryAddress.receiver}</div>
												<div class="clr"></div>
											</div>
											<div class="item">
												<span class="label">地址：</span>
												<div class="fl" style="max-width: 230px;text-overflow: -o-ellipsis-lastline;overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 1;-webkit-box-orient: vertical;">
													${deliveryAddress.subAdds}
												</div>
												<div class="clr"></div>
											</div>
											<div class="item">
												<span class="label">手机：</span>
												<div class="fl" style="max-width: 230px;text-overflow: -o-ellipsis-lastline;overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 1;-webkit-box-orient: vertical;">
													${deliveryAddress.mobile}
												</div>
												<div class="clr"></div>
											</div>
											<div class="item">
												<span class="label">固定电话：</span>
												<div class="fl" style="max-width: 230px;text-overflow: -o-ellipsis-lastline;overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 1;-webkit-box-orient: vertical;">
													${deliveryAddress.telphone}
												</div>
												<div class="clr"></div>
											</div>
											<div class="item">
												<span class="label">电子邮箱：</span>
												<div class="fl" style="max-width: 230px;text-overflow: -o-ellipsis-lastline;overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 1;-webkit-box-orient: vertical;">
													${deliveryAddress.email}
												</div>
												<div class="clr"></div>
											</div>
										</div>

									</div>
									<div class="item-rcol">
										<div class="ac">
											<c:choose>
												<c:when test="${deliveryAddress.commonAddr eq 0}">
													<a class="ncbtn" href="javascript:void(0);" style="padding: 5px 30px;"
														onclick="makeAddressDefault(${deliveryAddress.addrId});">设为默认</a>
												</c:when>
												<c:otherwise>
													<div class="ftx-03">默认地址，默认将商品送到该地址。</div>
												</c:otherwise>
											</c:choose>
										</div>
									</div>
								</div>
							</div>
						</c:forEach>

					</div>
				</div>
			</div>
			<div style="margin-top: 10px;" class="page clearfix">
				<div class="p-wrap">
					<span class="p-num"> 
						<%-- <ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" actionUrl="javascript:receiveAddrPager" type="simple" /> --%>
						<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
					</span>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<!----两栏end---->

		<%@ include
			file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script type="text/javascript">
	 function receiveAddrPager(curPageNO){
		userCenter.loadPageByAjax( "/p/receive address?curPageNO=" + curPageNO);
	} 
	$(document).ready(function() {
			userCenter.changeSubTab("receiveaddress");
	  	});
</script>
</body>
</html>
