<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/common.jsp"%>
<c:forEach items="${requestScope.userShopCartList.shopCarts}" var="carts" varStatus="status">
	<div class="shoppinglist">
		<div class="dis-modes">
			<p>
				店铺：
				<a style="color: #e5004f;" target="_blank" href="${contextPath}/store/${carts.shopId}" title="${carts.shopName}">${carts.shopName}</a>
				<c:if test="${not empty carts.mailfeeStr}">
					<span style="color:#e5004f;">[${carts.mailfeeStr}]</span>
				</c:if>
			</p>

			<div id="deliveryMode" style="margin-top:15px;">
				配送方式：
				<c:choose>
					<c:when test="${carts.regionalSales}">
						<font color="#e5004f;">有地区限售商品存在</font>
					</c:when>
					<c:when test="${carts.freePostage}">
						商家承担运费
					</c:when>
					<c:otherwise>
						<select shopId="${carts.shopId}" class="delivery">
							<c:choose>
								<c:when test="${empty carts.transfeeDtos}">
									<option value="" style="padding: 5px 0px;">请选择配送方式</option>
								</c:when>
								<c:otherwise>
									<c:forEach items="${carts.transfeeDtos}" var="transtype" varStatus="s">
										<option style="padding: 5px 0px;" transtype="${transtype.freightMode}" value="${transtype.deliveryAmount}">${transtype.desc}</option>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</select>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="shop-add">
				<div class="shop-wor">
					<input type="text" placeholder="给卖家留言,限45个字" name="remark" shopId="${carts.shopId}" maxlength="45">
				</div>
			</div>
		</div>
		<table class="cartnew_table" style="width:820px;float:right;margin-top:0px;">
			<thead style="border: 1px solid #e4e4e4;border-bottom: 0;">
			<tr>
				<th align="left" style="width:450px;">店铺宝贝</th>
				<th>单价</th>
				<th>数量</th>
				<th>状态</th>
			</tr>
			</thead>
			<tbody style="border: 1px solid #e4e4e4;border-top: 0;">

				<%-- 循环商家的每个活动 --%>
			<c:forEach items="${carts.marketingDtoList}" var="marketing" varStatus="markSts">
				<c:if test="${not empty marketing.hitCartItems}">
					<c:if test="${not empty marketing.promotionInfo}">
						<tr>
							<td colspan="4">
								<div class="item-header">
									<div class="f-txt">
																	<span class="full-icon">
																		<c:choose>
																			<c:when test="${marketing.type==0}">
																				满减
																			</c:when>
																			<c:when test="${marketing.type==1}">
																				满折
																			</c:when>
																			<c:when test="${marketing.type==2}">
																				限时
																			</c:when>
																		</c:choose>
																	</span>
											${marketing.promotionInfo}
									</div>
									<c:if test="${marketing.totalOffPrice>0}">
										<div class="f-price">
											<strong>¥${marketing.hitProdTotalPrice}</strong>
											<div class="ar">
												<span class="ftx-01">减：¥${marketing.totalOffPrice}</span>
											</div>
										</div>
									</c:if>
								</div></td>
						</tr>
					</c:if>
					<%-- 循环活动下的每个商品 --%>
					<c:forEach items="${marketing.hitCartItems}" var="basket" varStatus="s">
						<tr>
							<td class="textleft">
								<div class="cartnew-img">
									<a class="a_e" target="_blank" href="${contextPath}/views/${basket.prodId}">
										<img title="${basket.prodName}" src="<ls:images item='${basket.pic}' scale='3' />">
									</a>
								</div>
								<ul class="cartnew-ul">
									<li><a href="${contextPath}/views/${basket.prodId}" target="_blank" class="a_e">${basket.prodName}</a>
										<c:if test="${!empty basket.activIntro and basket.transportFree ne 1}"><font color="red" style="margin-left: 6px">* 单品包邮(${basket.activIntro})</font></c:if>
									</li>
									<li><c:if test="${not empty basket.cnProperties}">
										${basket.cnProperties}
									</c:if>
									</li>
									<li style="color: #e5004f;">
										<c:if test="${basket.transportFree eq 1}">
											${basket.shippingStr}
										</c:if>
									</li>
									<c:if test="${not empty basket.cartMarketRules}">
										<c:forEach items="${basket.cartMarketRules}" var="rule">
											<li><span>优惠信息</span><em>${rule.promotionInfo}</em>
											</li>
										</c:forEach>
									</c:if>
								</ul>
							</td>
							<td><c:if test="${basket.price-basket.promotionPrice > 0}">
								<del style="color:#999;">
									￥<fmt:formatNumber pattern="0.00" maxFractionDigits="2" groupingUsed="false" value="${basket.price}" />
								</del>
								<br>
							</c:if> <span style="font-size:14px; color:#e5004f;">
																￥<fmt:formatNumber pattern="0.00" maxFractionDigits="2" groupingUsed="false" value="${basket.promotionPrice}" />
															</span></td>
							<td>x${basket.basketCount}</td>
							<td><c:choose>
								<c:when test="${empty requestScope.userShopCartList.provinceId}">
									<span>有货</span>
								</c:when>
								<c:when test="${empty basket.stocks || basket.stocks==0}">
									<span class="stocksErr">无货</span>
								</c:when>
								<c:when test="${basket.stocks==-1}">
									<span class="stocksErr">该地区限售</span>
								</c:when>
								<c:when test="${basket.stocks<basket.basketCount}">
									<span class="stocksErr">缺货，仅剩${basket.stocks}件</span>
								</c:when>
								<c:otherwise>
									<span>有货</span>
								</c:otherwise>
							</c:choose></td>
						</tr>
					</c:forEach>
				</c:if>
			</c:forEach>
			</tbody>
		</table>
		<div class="clr"></div>
	</div>
</c:forEach>