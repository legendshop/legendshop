<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>促销活动-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/active/actives${_style_}.css'/>" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">促销活动</span>
				</p>
			</div>
			
			<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>

             
			<div id="rightContent" class="right_con">
			<div class="o-mt">
				<h2>促销活动</h2>
			</div>
		
			<div class="ncm-default-form">
				<form:form  action="${contextPath}/s/saveFullMail" id="add_form" method="post">
					<dl>
				      <dt><i class="required">*</i>活动名称：</dt>
				      <dd>
				        <input type="text" class="w400 text" maxlength="25" name="marketName" id="marketName">
				        <span class="error-message"></span>
				        <p class="hint">活动名称最多为25个字符</p>
				      </dd>
				    </dl>
				    
					<dl>
				      <dt><i class="required">*</i>开始时间：</dt>
				      <dd>
				        <input type="text" class="text w130"   name="startTime" id="startTime" readonly="readonly">
				        <span class="error-message"></span>
				      </dd>
				    </dl>
				    
				    <dl>
				      <dt><i class="required">*</i>结束时间：</dt>
				      <dd>
				      <input type="text" class="text w130"  name="endTime" id="endTime" readonly="readonly">
				        <span class="error-message"></span>
				        <p class="hint">
				        </p>
				      </dd>
				    </dl>
				    
				    
					<dl>
						<dt><i class="required">*</i>选择商品：</dt>
						<dd>
					  
							<label for="goods_status_1">
							       <input
										type="radio" checked="checked" id="goods_status_1" value="1"
										name="isAllProds">全部商品</label>
						
						   <label for="goods_status_0"  style="margin-left: 5px;"> 
							         <input type="radio" checked="checked" id="goods_status_0" value="0" name="isAllProds">部分参与商品
							     </label>
						
						</dd>
					</dl>
					
					<dl>
						<dt><i class="required">*</i>促销类型：</dt>
						<dd>
								  <label for="goods_type_0"> 
							        <input type="radio" checked="checked"  id="goods_type_0" value="5" name="type" >满多少元包邮
							       </label>
								    <label for="goods_type_1"  style="margin-left: 10px;"> 
							           <input type="radio" id="goods_type_1" value="4" name="type">满件包邮
							        </label>
							         <span class="error-message"></span>
						</dd>
					</dl>
					
					<dl>
				      <dt><i class="required">*</i>满即包邮规则：</dt>
				      <dd>
				           <input type="hidden" name="rule_count" id="mansong_rule_count">
				           <ul class="ncsc-mansong-rule-list" id="manjian_rule_list">
		                   </ul>
		                   
		                   <ul class="ncsc-mansong-rule-list" id="manze_rule_list">
		                   </ul>
		                   
				           <a class="ncbtn ncbtn-aqua" style="display: none;" id="btn_add_rule" href="javascript:void(0);"><i class="icon-plus-sign"></i>添加规则</a>
				           <div  id="add_manjian_rule">
				            <div class="ncsc-mansong-rule">
				              <span>满&nbsp;<input type="text" class="text w50" id="manjian_price"><em class="add-on">元</em></span>
				              <span>&nbsp;&nbsp;&nbsp;<em class="add-on">包邮</em>，</span>
				                <div class="gift" id="mansong_goods_item"></div>
				                </div>
				                <div style="display:none;" id="mansong_rule_error">请至少选择一种促销方式</div>
				               <div class="mt10">
				                  <a class="ncbtn ncbtn-aqua" style="width: 80px;" id="save_mj_rule" href="javascript:void(0);">确定规则设置</a>
				                  <a class="ncbtn ncbtn-bittersweet" style="width: 80px;" id="btn_cancel_mj_rule" href="javascript:void(0);">取消</a></div>
				             </div>
				            <div style="display:none;" id="add_manze_rule">
				                <div class="ncsc-mansong-rule">
				                  <span>满&nbsp;<input type="text" class="text w50" id="manze_price"><em class="add-on">件</em></span>
				                  <span>&nbsp;&nbsp;<em class="add-on">包邮</em>，</span>
				                  <div class="gift" id="mansong_goods_item"></div>
				                </div>
				                <div style="display:none;" id="mansong_rule_error">请至少选择一种促销方式</div>
				               <div class="mt10">
				                  <a class="ncbtn ncbtn-aqua" style="width: 80px;" id="save_mz_rule" href="javascript:void(0);">确定规则设置
				                  </a>
				                  <a class="ncbtn ncbtn-bittersweet" style="width: 80px;" id="btn_cancel_mz_rule" href="javascript:void(0);">
				                                                 取消</a>
				                  </div>
				             </div>
				            <span class="error-message"></span>
				      </dd>
				    </dl>
				    
				    <dl>
				      <dt>备注：</dt>
				      <dd>
				        <textarea class="textarea w400" maxlength="100" id="remark" rows="3" name="remark"></textarea>
				        <p class="hint">活动备注最多为100个字符</p>
				      </dd>
				    </dl>
									
					<dl class="bottom">
						<dt>&nbsp;</dt>
						<dd>
							<label class="submit-border">
							<input type="submit" value="保存"  class="submit">
							</label>
						</dd>
					</dl>
				</form:form>
			</div>
			
			<div class="m m1" style="margin-top: 100px;">
		   			<div class="mt">
		   				
		   			</div>
					<div class="mc">
						
					</div>
		   		</div>
		   		
		   		
			<div class="clear"></div>
		</div>
				  
		  
		</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
    <script src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>" type="text/javascript"></script>
    <script src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
    <script src="<ls:templateResource item='/resources/templets/js/shopAddFullMail.js'/>"></script>
    <script type="text/javascript">
	    var contextPath = '${contextPath}';
	    var isAllProd="${isAllProd}";
	</script>  
</body>
</html>