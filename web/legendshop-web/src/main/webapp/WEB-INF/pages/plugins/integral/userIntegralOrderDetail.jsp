<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>我的积分 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>
<style>
.hint{
margin-right:30px;
}
</style>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
    <%@include file='/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
   <div class="right_con">

<div class="right-layout">
	<div class="ncm-oredr-show" style="height:1000px;">
		<div class="ncm-order-info">
			<div class="ncm-order-details">
				<div class="title">订单信息</div>
				<div class="content">
					<dl>
						<dt>收货地址：</dt>
						<dd>
							<span>
								<c:if test="${not empty order.usrAddrSubDto}"> 
								   ${order.usrAddrSubDto.subAdds}
								</c:if>
							 </span>
						</dd>
					</dl>
					<dl>
						<dt>订单编号：</dt>
						<dd>
							${order.orderSn}
						</dd>
					</dl>
					<dl>
						<dt>收货人&nbsp;&nbsp;：</dt>
						<dd>
							${order.usrAddrSubDto.receiver}
								  
						</dd>
					</dl>
					<dl>
						<dt>联系电话：</dt>
						<dd>
								   ${order.usrAddrSubDto.mobile}
						</dd>
					</dl>
				</div>
			</div>
			<div class="ncm-order-condition">
				<dl>
					<dt>
						<i class="icon-ok-circle green"></i>订单状态：
					</dt>
					<dd>
					  <c:choose>
							 <c:when test="${order.orderStatus==0 }">
							        已提交
							 </c:when>
							 <c:when test="${order.orderStatus==1 }">
							       已发货
							 </c:when>
							 <c:when test="${order.orderStatus==2 }">
							       已完成
							 </c:when>
							 <c:when test="${order.orderStatus==3 }">
							      已取消
							 </c:when>
					 </c:choose>
					</dd>
				</dl>
				<ul>
					<li>积分商品</li>
				</ul>
			</div>
			<!-- <div class="mall-msg">
				有疑问可咨询<a href="javascript:void(0);"><i class="icon-comments-alt"></i>平台客服</a>
			</div> -->
		</div>
		<div class="ncm-order-step" id="order-step" style="text-align: center;">
			<dl class="step-first current">
				<dt>生成订单</dt>
				<dd class="bg"></dd>
				 <dd title="下单时间" class="date"><fmt:formatDate value="${order.addTime}" type="both" /></dd> 
			</dl>
			<dl class="<c:if test="${not empty order.dvyTime}">current</c:if>">
				<dt>商家发货</dt>
				<dd class="bg"></dd>
				<c:if test="${not empty order.dvyTime}"><dd title="发货时间" class="date"><fmt:formatDate value="${order.dvyTime}" type="both" /></dd></c:if>
			</dl>
			<dl class="<c:if test="${not empty order.finallyTime}">current</c:if>">
				<dt>确认收货</dt>
				<dd class="bg"></dd>
				<c:if test="${not empty order.finallyTime}"><dd title="确认收货时间" class="date"><fmt:formatDate value="${order.finallyTime}" type="both" /></dd></c:if>
			</dl>
		</div>
		<div class="ncm-order-contnet">
			<table class="ncm-default-table order">
				<thead>
					<tr>
						<th width="100">商品图片</th>
					<th width="232">商品名称</th>
					<th width="100">单价</th>
					<th width="100">兑换积分</th>
					<th width="100">数量</th>
					<th width="100">总积分</th>
					<th width="100">交易操作</th>
					</tr>
				</thead>
				<tbody>
				<c:if test="${order.orderStatus==1}">
				    <tr>
						<th style="border-right: none;" colspan="10">
								<div class="order-deliver">
									<span>物流公司： <a href="${order.delivery.delUrl}" target="_blank">${order.delivery.delName}</a></span>
									<span> 物流单号： ${order.delivery.dvyFlowId} </span>
									<span><a id="show_shipping" href="javascript:void(0);">物流跟踪<i class="icon-angle-down"></i>
											<div class="more">
												<span class="arrow"></span>
												<ul id="shipping_ul">
													<li>加载中...</li>
												</ul>
											</div> </a>
									</span>
								</div>
						</th>
					</tr>
					</c:if>
					<!-- S 商品列表 -->
				<c:forEach items="${order.orderItemDtos}" var="orderItem" varStatus="orderItemStatues">
				    <tr class="bd-line">
						<td align="center"><a target="_blank"
							href="<ls:url address='/integral/view/${orderItem.prodId}'/>" >
							<img width="62"
								height="62"
								src="<ls:images item='${orderItem.prodPic}' scale='3'/>">
						</a>
					</td>
						<td align="left"><a target="_blank" class="blue"
						href="<ls:url address='/integral/view/${orderItem.prodId}'/>">${orderItem.prodName}</a>
					</td>
					<td align="center"><b class="red">¥${orderItem.price}</b>
					</td>
					<td align="center">${orderItem.exchangeIntegral}</td>
					<td align="center">${orderItem.basketCount}</td>
					<td align="center">${orderItem.totalIntegral}</td>
					<c:if test="${orderItemStatues.index==0}">  
		                </td>
					
						  <c:choose>
		                		<c:when test="${fn:length(order.orderItemDtos)>0}">
		                			<td rowspan="${fn:length(order.orderItemDtos)}" class="bdl">
		                		</c:when>
		                		<c:otherwise>
		                			<td class="bdl">
		                		</c:otherwise>
		                </c:choose>
							<!-- 取消订单 -->
							<p>
						   <c:choose>
							 <c:when test="${order.orderStatus==3 }">
							    <i
								class="icon-ban-circle"></i> 订单已取消
							 </c:when>
							 <c:when test="${order.orderStatus==1 }">
							    <a class="btn-r" href="javascript:void(0)" onclick="javascript:orderReceive('${order.orderSn}');"><i
								class="icon-ban-circle"></i>确认收货 </a>
							 </c:when>
							 <c:when test="${order.orderStatus==2 }">
							    <i
								class="icon-ban-circle"></i> 已完成
							 </c:when>
						  </c:choose>
						</p>
						</td>
						</c:if>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="clear"></div>
</div>
<!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/userIntergralOrderDetail.js'/>"></script>
<script>
 	var contextPath="${contextPath}";
 	var dvyFlowId="${order.delivery.dvyFlowId}";
	var dvyTypeId="${order.delivery.dvyTypeId}";
	$(document).ready(function() {
		userCenter.changeSubTab("myIntegral");
  	});
</script>
</body>
</html>