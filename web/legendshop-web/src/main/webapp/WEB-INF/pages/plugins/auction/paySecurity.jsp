<!DOCTYPE html>
	<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
	<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
	<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>支付保证金-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
</head>
<%@ include file="/WEB-INF/pages/plugins/main/included/rightSuspensionFrame.jsp" %>
<body class="graybody">
	<div id="doc">
	  <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
	</div>

   <div id="bd">
      <div id="bd" style="height: 300px;">
       <div class="seller-cru" style="width:1190px;margin: 20px auto 10px;">
	        <p>
	          您的位置&gt;
	          <a href="${contextPath}/home">首页</a>&gt;<a href="${contextPath}/auction/autionList">拍卖活动</a>&gt;
	          <span style="color: #e5004f;">支付保证金</span>
	        </p>
	      </div>

            <div style="margin-top:20px;" class="yt-wrap ordermain">
				<%@ include file="paylist.jsp" %>
           <div class="black_overlay" ></div>

     </div>

       <form:form  id="buy_form" method="POST" action="${contextPath}/p/payment/payto/AUCTION_DEPOSIT">
			     <input type="hidden" id="payTypeId" name="payTypeId" value="FULL_PAY" >
			     <input type="hidden" name="subNumbers" value="${subNumber}">
			     <input type="hidden" name="prePayTypeVal" id="prePayTypeVal" value="1">
			     <input type="hidden" value=""  name="passwordCallback" id="password_callback" />
			     <input type="hidden" name="submitPwd" id="submitPwd" value="">
           <!-- 确认订单按钮 -->
           <div class="confirm" id="confirm" style="margin-right: 350px">
               <span id="confirm_totalPrice">余额支付金额：<b>￥<em id="predePrice">0.00</em></b>，在线应付金额：<b>￥<em id="totalPrice">${amount}</em></b></span>
               <button class="on" type="button" id="next_button">去付款</button>
           </div>
        </form:form>


   </div>
       <div style="margin-top: 385px">
           <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
       </div>
   <script type="text/javascript">
       var $li = $('.onl-pay-tit div');
       var $ul = $('.onl-pay .onl-pay-con');
       var contextPath = '${contextPath}';
       var member_pd ="${userDetail.availablePredeposit}";
       var price='${amount}';
   </script>
  <script type="text/javascript" src="${contextPath}/resources/templets/js/paySecurity.js"></script>
</body>
</html>
          
        
          
        