<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
       <!--浏览历史-->
        <div class="nch-module nch-module-style03">
			<h3>浏览历史</h3>

		<c:choose>
			<c:when test="${empty exchangeList }">
				<div class="sidecell" <c:if test="${status.last}">style="border-bottom:0px;"</c:if> >
					<div>暂无浏览历史</div>
				</div>
			</c:when>
			<c:otherwise>
			  <c:forEach items="${exchangeList}" var="prod" varStatus="status">
					<div class="sidecell" <c:if test="${status.last}">style="border-bottom:0px;"</c:if> >
						<div class="limg">
							<a href="${contextPath}/integral/view/${prod.id}" target="_blank">
							<img src="<ls:images item='${prod.prodImage}' scale='2'/>"  alt="${prod.name}"  style="max-height:55px; max-width:55px;" />
							 </a>
						</div>
						<div class="txt">
							<div style="height:35px;overflow: hidden;"><a href="${contextPath}/integral/view/${prod.id}"  target="_blank">${prod.name }</a></div>
							<span class="lprice">兑换积分:${prod.exchangeIntegral}</span>
						</div>
					</div>
			  </c:forEach>
			</c:otherwise>
		</c:choose>
		</div>
