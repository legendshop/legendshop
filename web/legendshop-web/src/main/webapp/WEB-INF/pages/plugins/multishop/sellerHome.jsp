<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>卖家中心-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/sellerHome${_style_}.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller${_style_}.css'/>" rel="stylesheet"/>
</head>

<body  class="graybody">
	<div id="doc">
	    
	     <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
	      
	     
	     <div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置&gt;
					<a href="${contextPath}/home">首页</a>&gt;
					<span class="on">卖家中心</span>
				</p>
			</div>
             <!-- 面包屑 -->
             
             
             <%@ include file="included/sellerLeft.jsp" %>
             
			
                <!-- 侧边导航 -->
			<div class="seller-index">
				<div class="shop-mes">
					<div class="head-scu">
								 <c:choose>
			                 	<c:when test="${not empty REQ_SHOPDETAIL_VIEW.shopPic2}">
			                 		<img src="<ls:photo item='${REQ_SHOPDETAIL_VIEW.shopPic2}'/>" width="100px" height="100px" />
			                 	</c:when>
			                 	<c:otherwise>
			                 		<img src="${contextPath}/resources/templets/images/Default-image.png" width="100px" height="100px">
			                 	</c:otherwise>
			                 </c:choose>
					</div>
					<div class="shop-mes-det">
						<div class="shop-name">
							<b>${REQ_SHOPDETAIL_VIEW.nickName}</b>
							<!-- <span>卖家等级：普通会员</span> -->
						</div>
						<ul>
							<li>店铺名称：<span>${REQ_SHOPDETAIL_VIEW.siteName}</span></li>
							<li>创建时间：<fmt:formatDate value="${REQ_SHOPDETAIL_VIEW.recDate}" pattern="yyyy-MM-dd"/></li>
							<li>上次登录：
								<c:choose>
									<c:when test="${empty loginhist.time}">无</c:when>
									<c:otherwise>
										<fmt:formatDate value="${loginhist.time}" pattern="yyyy-MM-dd HH:mm:ss"/>
									</c:otherwise>
								</c:choose>

							</li>
						</ul>
					</div>
					<div class="shop-mes-log">
						<p class="get-in"><a href="${contextPath}/store/${REQ_SHOPDETAIL_VIEW.shopId}" target="_blank">进入店铺</a></p>
						<p class="quit"><a href="${contextPath}/p/logout">退出登录</a></p>
					</div>
				</div>
<!-- 店铺信息 -->
				<c:if test="${loginUserType != 'E' }"><!-- 子账号不显示店铺统计信息 -->
				<div class="shop-tip">
					<div class="shop-tip-tit">
						<p>店铺提示</p>
					</div>
					<div class="shop-tip-det">
						<ul>
							<li>
								<a href="${contextPath}/s/prodcons">
									<p>咨询提示：</p>新增留言<span>(${consultCounts})</span>
								</a>
							</li>
							<li>
								<a href="${contextPath}/s/reportForbit">
									<p>投诉提示：</p>新增投诉<span>(${accusationCounts})</span>
								</a>
							</li>
							<li>
								<a href="${contextPath}/s/sellingProd">
									出售中的商品：<span>
										<c:choose>
						            		<c:when test="${not empty PROD_ONLINE}">(${PROD_ONLINE})件</c:when>
						            		<c:otherwise>(0)件</c:otherwise>
			            				</c:choose>
									</span>
								</a>
							</li>
							<li>
								<a href="${contextPath}/s/prodInStoreHouse">
									仓库中的商品：<span>
										<c:choose>
		            						<c:when test="${not empty PROD_OFFLINE}">(${PROD_OFFLINE})件</c:when>
		            						<c:otherwise>(0)件</c:otherwise>
		            					</c:choose>
									</span>
								</a>
							</li>
							<li>
								<a href="${contextPath}/s/violationProd">
									违规下架商品：<span>
												<c:choose>
		            								<c:when test="${not empty PROD_ILLEGAL_OFF}">(${PROD_ILLEGAL_OFF})件</c:when>
								            		<c:otherwise>(0)件</c:otherwise>
								            	</c:choose>
										</span>
								</a>
							</li>
							<!-- <li>
								<a href="#">
									审核中的商品：<span>(783)件</span>
								</a>
							</li> -->
						</ul>
					</div>
				</div>
<!-- 店铺提示 -->
				<div class="business-tip">
					<div class="business-tip-tit">
						<p>交易提示(最近一个月)</p>
					</div>
					<div class="business-tip-det">
						<ul>
							<li>
								<a href="${contextPath}/s/ordersManage">
									有效订单：<span>
												<c:choose>
		            								<c:when test="${not empty tradingInOrderNum}">(${tradingInOrderNum})</c:when>
		            							<c:otherwise>(0)</c:otherwise>
		            							</c:choose>
											</span>
								</a>
							</li>
							<li>
								<a href="${contextPath}/s/ordersManage?status=1">
									待付款订单：<span>
												<c:choose>
		            								<c:when test="${not empty UNPAY}">(${UNPAY})</c:when>
		            								<c:otherwise>(0)</c:otherwise>
		            							</c:choose>
											</span>
								</a>
							</li>
							<li>
								<a href="${contextPath}/s/ordersManage?status=4">
									交易成功订单：<span>
												<c:choose>
		            								<c:when test="${not empty SUCCESS}">(${SUCCESS})</c:when>
		            								<c:otherwise>(0)</c:otherwise>
		            							</c:choose>
											</span>
								</a>
							</li>
							<li>
								<a href="${contextPath}/s/ordersManage?status=2">
									已付款待发货：<span>
											<c:choose>
		            							<c:when test="${not empty PADYED}">(${PADYED})</c:when>
		            							<c:otherwise>(0)</c:otherwise>
		            						</c:choose>
										</span>
								</a>
							</li>
							<li>
								<a href="${contextPath}/s/ordersManage?status=3">
									待买家收货：<span>
												<c:choose>
		            								<c:when test="${not empty CONSIGNMENT}">(${CONSIGNMENT})</c:when>
		            							<c:otherwise>(0)</c:otherwise>
		            							</c:choose>
											</span>
								</a>
							</li>
<%-- 							<li>
								<a href="${contextPath}/s/refundReturnList/1">
									退货订单申请：<span>
												<c:choose>
		            								<c:when test="${not empty prodRetrunCounts}">(${prodRetrunCounts})</c:when>
		            								<c:otherwise>(0)</c:otherwise>
		            							</c:choose>
											</span>
								</a>
							</li> --%>
						</ul>
					</div>
				</div>
<!-- 交易提示 --> 
			<c:if test="${not empty salesRankList}">
				<div class="commodity-rank">
					<div class="commodity-rank-tit">
						<p>商品销售排行</p>
					</div>
					<table class="sold-table" style="border:none;" cellpadding="0" cellspacing="0">
						<colgroup>
							<col style="width: 10%; min-width: 10%">
							<col style="width: 30%; min-width: 30%">
							<col style="width: 35%; min-width: 35%">
							<col style="width: 25%; min-width: 25%">
						</colgroup>
						<tbody><tr class="table-tit">
							<td>商品排行</td>
							<td>商品图片</td>
							<td>商品名称</td>
							<td>商品销量</td>
						</tr>
						<c:forEach items="${salesRankList}" var="prod" varStatus="index">
							<tr>
								<td>${index.count}</td>
								<td>
									<a href="${contextPath}/views/${prod.prodId}" target="_blank"><img src="<ls:images item='${prod.prodPic}' scale='3'/>" alt="${prod.prodName}" ></a>
								</td>
								<td style="word-break:break-all;">
									<a href="${contextPath}/views/${prod.prodId}">${prod.prodName}</a>
								</td>
								<td>${prod.buys}</td>
							</tr>
						</c:forEach>
					</tbody></table>
				</div>
			</c:if>
			
			</div>
          <!-- 右侧导航栏 -->
          
            <%@ include file="included/sellerRight.jsp" %>
          </c:if>  
            
		</div>
		
		
		
	    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	
	</div>
</body>
</html>