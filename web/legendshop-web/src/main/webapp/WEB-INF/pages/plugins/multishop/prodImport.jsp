<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>商品导入-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-prod${_style_}.css'/>" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">商品导入</span>
				</p>
			</div>
				<%@ include file="included/sellerLeft.jsp" %>
			<div class="seller-selling">
				<form:form action="${contextPath}/s/prod/importlist" method="get" id="from1">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
						<div class="selling-sea">
							<p>商品名称：</p>
							<input type="text" value="${prodName}" name="prodName"  id ="name">
							<input type="button" onclick="search()" class="serach" id="btn_keyword" value="搜  索" >	
						</div>
						

				</form:form>
				
						<div class="selling-sea" style="margin-top: 5px;padding-left: 10px;">
							<input type="button" class="serach" id="btn_keyword1" value="下载模版" >	
							<input type="button" class="serach" id="btn_keyword2" value="导入商品" >	
							<input type="button" class="serach" id="btn_keyword3" value="导入淘宝商品" >	
							<input type="button" class="serach" id="btn_keyword4" value="删除" >	
						</div>
				
<!-- 搜索 -->
				<table class="selling-table">
					<tr class="selling-tit">
						<td class="check" style="width:50px;"><input type="checkbox" id="checkbox" onclick="selAll();"></td>
						<td width="250px">商品名称</td>
						<td width="200px">卖点</td>
						<td>销售价格</td>
						<td>市场价格</td>
						<td>库存预警</td>
						<td>商家编码</td>
						<td>是否发布</td>
						<td style="width:50px;">操作</td>
					</tr>
					<c:if test="${empty requestScope.list}">
						<tr><td colspan='9' height="60">没有找到符合条件的商品</td></tr>
					</c:if>				
					<c:forEach items="${requestScope.list}" var="prodImp" varStatus="status">
						<tr class="detail">
							<td>
                         				<input name="strArray" type="checkbox" value="${prodImp.id}"  arg="">
							</td>
							<td >
										${prodImp.prodName}
							</td>
							
							<td>${prodImp.brief }</td>
							<td>
								${prodImp.cash }
							</td>
							<td>${prodImp.price}</td>
							<td>${prodImp.stocksArm }</td>
							<td>
								${prodImp.partyCode}
							</td>	
							<td>
							<c:choose>
								<c:when test="${prodImp.status == 0}">
									<span>否</span>
								</c:when>
								<c:otherwise>
									<span>是</span>
								</c:otherwise>
								</c:choose>
							</td>
							<td><ul style="width:46px;"><li><a  onclick="publishProd('${prodImp.id}');" value="发布" class="bot-gra" style="cursor: pointer;">发布</a></li></ul></td>
						</tr>
					</c:forEach>
				</table>
				<div class="clear"></div>
					<div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
				            		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
				     			 </div>
		  				</div>
			</div>
		</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/prodImport.js'/>"></script>
	<script type="text/javascript">
		var contextPath="${contextPath}";
	</script>
</body>
</html>