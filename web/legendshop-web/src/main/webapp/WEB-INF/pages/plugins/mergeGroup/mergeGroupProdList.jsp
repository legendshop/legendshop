<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
<table cellspacing="0" cellpadding="">
	<tr class="tab-tit">
		<th>商品名称</th>
		<th>商品价格</th>
		<th>可销售库存</th>
		<th style="width:83px;">操作</th>
	</tr>
	
	<c:if test="${empty requestScope.list}">
		<tr class="tab-tit"><td colspan='4' height="60" style="text-align: center;color: #999;">没有符合条件的信息</td></tr>
	</c:if>
	
	<c:forEach items="${requestScope.list}" var="item">
		<tr class="sign">
			<td>
				<div class="goods-mes">
					<span class="img"><img src="<ls:photo item='${item.pic}'/>" style="max-width: 50px;max-height: 50px;" alt=""></span>
					<span class="txt">${item.name }</span>
				</div>
			</td>
			<td>¥${item.cash}</td>
			<td>${item.stocks }</td>
			<td class="last"><a class="join-btn" id="${item.prodId}" data-id="${item.prodId }" style="cursor: pointer;">参加</a></td>
		</tr>
	</c:forEach>
</table>

<div class="clear"></div>
<div style="margin-top:10px;" class="page clearfix">
    <div class="p-wrap">
          <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
    </div>
</div>

<script type="text/javascript">
$(function(){
	
	$("tr.sign").each(function(index,element) {
		var prodId = $(element).find("td.last a").attr("data-id");
		//var index = prodIds.indexOf(prodId);
		var index = $.inArray(prodId, prodIds);
		//页面加载判断那些商品正在参加其他活动  参加按钮显示为灰色
		//检查该商品是否正在参与预售、秒杀、团购、拍卖
		$.ajax({
			url : contextPath + "/s/mergeGroupActivity/is/join/active/"+prodId,
			type : 'get',
			async : false, // 默认为true 异步
			dataType : "json",
			success : function(data) {
				if(data != "OK"){
					$("#" + prodId).css("background","#999");
			   }
			}
		});
	}); 
	
})
</script>
