<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ page import="java.util.Date"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>
        <c:if test="${empty coupon.couponId }">添加优惠券</c:if>
        <c:if test="${not empty coupon.couponId }">查看优惠券</c:if>-${systemConfig.shopName}
    </title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/coupons.css'/>" rel="stylesheet">
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/coupon-admin${_style_}.css'/>" rel="stylesheet">
    <link type="text/css" href="<ls:templateResource item='/resources/plugins/select2-3.5.2/tao_select2.css'/>" rel="stylesheet"/>
    <style type="text/css">
        .prodList {
            border: solid #dddddd;
            border-width: 1px 0px 0px 1px;
            width: 100%;
        }

        .prodList tr td {
            text-align: center;
        }
		.sold-table td{
			color:#999 !important;
		}
		
        .error {
            color: #900;
        }
        .w340{
        	width:340px !important;
        }
		.Wdate{
			width:157px !important;
		}        
    </style>
</head>
<!-- 获取当前日期 -->
<c:set var="nowDate">  
    <fmt:formatDate value="<%=new Date()%>" pattern="yyyy-MM-dd HH:mm:ss" type="date"/>  
</c:set> 
<body class="graybody">
<div id="doc">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
    <div class="w1190">
        <div class="seller-cru">
            <p>
                您的位置>
                <a href="${contextPath}/home">首页</a>>
                <a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">
						<c:if test="${empty coupon.couponId }">新增优惠券</c:if>
						<c:if test="${not empty coupon.couponId }">查看优惠券</c:if>
					</span>
            </p>
        </div>
        <%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>


        <div class="seller-coupon-admin">
            <div class="pagetab2">
                <ul>
                    <li><span><a class="but-serch" href="javascript:void(0);" onclick='window.location="<ls:url address='/s/coupon/query'/>"'>优惠券管理</a></span></li>
                    <li class="on">
							<span>
								<c:if test="${empty coupon.couponId }">新增优惠券</c:if>
								<c:if test="${not empty coupon.couponId }">查看优惠券</c:if>
							</span>
                    </li>
                </ul>
            </div>

            <div class="new-coupon">
                <form:form action="${contextPath}/s/coupon/saveCoupon" method="post" id="form1" enctype="multipart/form-data">
                    <input id="couponId" name="couponId" value="${coupon.couponId}" type="hidden">
                    <ul>
                        <li>
                            <div class="new-cou-con"><span><em>*</em>优惠券名称：</span>
                            <input type="text" class="w340" <c:if test="${not empty coupon.couponId}">disabled="disabled"</c:if> name="couponName" id="couponName" value="${coupon.couponName}" maxlength="25"></div>
                        </li>
                        <li>
                            <div class="new-cou-con"><span><em>*</em>领取方式：</span>
                                <select
                                        <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> id="getType" name="getType">
                                    <ls:optionGroup type="select" required="true" cache="true" beanName="GET_TYPE" selectedValue="${coupon.getType}"/>
                                </select>

                            </div>
                            <div class="new-cou-com">
                                <p>“积分兑换”时会员可以在积分中心用积分进行兑换；“卡密兑换”时会员需要在“我的商城——我的优惠券”中输入卡密获得优惠券；</p>
                                <p>“免费领取”时会员可以点击店铺的优惠券推广广告领取优惠券。</p>

                                <c:if test="${coupon.getType eq 'free' && coupon.isDsignatedUser eq 0 }">
                                    <br>
                                    <p><a style="color:#900" href="${contextPath}/p/coupon/apply/${coupon.couponId}" target="_black">点击我，跳转到领取优惠券页面噢！</a></p>
                                </c:if>
                            </div>
                        </li>

                        <div id="type">
                        </div>

                        <c:if test="${not empty coupon.couponId && coupon.getType eq 'points'}">
                            <li>
                                <div class='new-cou-con'><span><font color='ff0000'>* </font>兑换所需积分:</span>
                                    <input class='${inputclass}' type='text' class="w340" 
                                           <c:if test='${ not empty coupon.couponId}'>disabled='disabled'</c:if>
                                           name='needPoints' id='needPoints' value='${coupon.needPoints}' maxlength="8"/>
                                </div>
                            </li>
                        </c:if>

                        <li>
                            <div class="new-cou-con"><span><em>*</em>有效期：</span><input
                                    <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> readonly="readonly" name="startDate" id="startDate" class="Wdate" type="text"
                                    value='<fmt:formatDate value="${coupon.startDate}" pattern="yyyy-MM-dd HH:mm:ss" />'/> —
                                <input
                                        <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> readonly="readonly" name="endDate" id="endDate" class="Wdate" type="text"
                                        value='<fmt:formatDate value="${coupon.endDate}" pattern="yyyy-MM-dd HH:mm:ss" />'/>
                            </div>
                        </li>
                        <li id="isAppointUser">
                            <div class="new-cou-con">
                                <span><font color="ff0000">*</font> 是否对指定用户发放:</span>
                                <select
                                        <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> class="${selectclass}" name="isDsignatedUser">
                                    <option value="0"  <c:if test="${coupon.isDsignatedUser == 0}">selected="selected"</c:if>>否</option>
                                    <option value="1" <c:if test="${coupon.isDsignatedUser == 1}">selected="selected"</c:if>>是</option>
                                </select>
                            </div>
                            <div class="new-cou-com">
                                <p>对指定用户发放优惠劵时,领取方式暂时只支持免费领取</p>
                            </div>
                        </li>
                        <!-- 							 class="exper-show" -->
                        <li class="appoint-user-show" style="display:none;">
                            <div class="new-cou-con"><span><em>*</em>用户名：</span>
                                <input type="text" class="w340" name="userIdLists" id="userIdLists"/>
                            </div>
                            <div class="new-cou-com">
                                <p>对指定用户发放体验劵时,请指定用户</p>
                            </div>
                        </li>
                        <li>
                            <div class="new-cou-con"><span><em>*</em>面额(元)：</span>
                                <input type="text" maxlength="8" placeholder="元" class="w340" 
                                       <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> name="offPrice" id="offPrice" value="<fmt:formatNumber type="number" value="${coupon.offPrice}" pattern="0.00" maxFractionDigits="2"/>"/>
                                <!-- <span style="width:0px;"><i class="yuan">元</i></span> -->
                            </div>
                        </li>

                        <li>
                            <div class="new-cou-con">
                                <span><font color="ff0000">*</font> 优惠券类型:</span>
                                <select
                                        <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> class="${selectclass}" id="couponType" name="couponType">
                                    <option
                                            <c:if test="${coupon.couponType eq 'common'}">selected="selected"</c:if> value="common">通用券
                                    </option>
                                    <option
                                            <c:if test="${coupon.couponType eq 'product'}">selected="selected"</c:if> value="product">商品券
                                    </option>
                                </select>
                            </div>
                        </li>

                        <li id="pro">
                            <div id="cate" class="new-cou-con">
                            </div>
                        </li>

                        <c:if test="${not empty coupon.couponId && not empty productList}">
                            <li>
                                <div class="new-cou-com">
                                    <table class="prodList sold-table">
                                        <tr>
                                            <th>商品图片</th>
                                            <th>商品名称</th>
                                        </tr>
                                        <c:forEach items="${productList }" var="product">
                                            <tr>
                                                <td><img src="<ls:images item='${product.pic}' scale='3' />"></td>
                                                <td>${product.name}</td>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                </div>
                            </li>
                        </c:if>


                        <li>
                            <div class="new-cou-con"><span><em>*</em>可发放总数：</span>
                                <input class="w340" 
                                        <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> type="text" name="couponNumber" id="couponNumber" value="${coupon.couponNumber}" maxlength="8"/>
                            </div>
                            <div class="new-cou-com">
                                <p>如果优惠券领取方式为卡密兑换，则发放总数应为1~1000之间的整数</p>
                            </div>
                        </li>
                        <li>
                            <div class="new-cou-con"><span><em>*</em>每人限领：</span>
                                <input class="w340" 
                                        <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> type="text" name="getLimit" id="getLimit" value="${coupon.getLimit}" maxlength="8"/>
                            </div>
                            <div class="new-cou-com">
                                <p>如果限领为0,则用户领取不了!</p>
                            </div>

                        </li>
                        <li>
                            <div class="new-cou-con"><span><em>*</em>消费金额(元)：</span>
                                <input class="w340" 
                                        <c:if test="${not empty coupon.couponId}">disabled="disabled"</c:if> type="text" name="fullPrice" id="fullPrice" placeholder="元"
                                        value="<fmt:formatNumber type="number" value="${coupon.fullPrice==null?0:coupon.fullPrice}" pattern="0.00" maxFractionDigits="2"/>" maxlength="8"/>
                                <!-- <span style="width:0px;"><i class="yuan">元</i></span> -->
                            </div>
                            <div class="new-cou-com">
                                <p>优惠券消费金额应大于面额</p>
                            </div>
                        </li>

                        <li>
                            <div class="new-cou-con"><span><em>*</em>优惠券描述：</span>
                                <textarea style="width:340px;"
                                        <c:if test="${ not empty coupon.couponId}">disabled="disabled"</c:if> rows="2" cols="5" name="description" id="description" maxlength="250">${coupon.description}</textarea>
                            </div>
                        </li>
                        <li>

                            <div class="new-cou-con">
                                <span style="float: left;">优惠券图片：</span>
                                <c:if test="${not empty coupon.couponId}">
                                    <c:if test="${empty coupon.couponPic}">
                                       	 木有上传优惠券图片噢！
                                    </c:if>
                                    <c:if test="${not empty coupon.couponPic}">
                                        <div class="sub-img">
                                            <i><img src="<ls:photo item='${coupon.couponPic}'/>" height="200" width="200"/></i>
                                        </div>
                                    </c:if>
                                </c:if>
                                <c:if test="${empty coupon.couponId}">
                                <div class="sub-but">
                                    <input type="hidden" class="file" name="couponPic" id="couponPic" size="30" value="${coupon.couponPic}"/>
                                    <input type="file" style="text-indent: 0px;margin-top:5px;" class="file" name="file" id="file" size="30"/>
                                    <!-- <input type="button" value="图片上传"> -->
                                </div>
                                <div class="new-cou-com">
                                    <p>该图片将在领券中心的优惠券模块中显示，建议尺寸为160*160px该图片将在领券中心的优惠券模块中显示，建议尺寸为160*160px</p>
                                </div>
                            </c:if>
                            </div>
                        </li>
                    </ul>


                    <!-- 新增优惠券 -->
                    <!-- 用户优惠券 -->
                    <c:if test="${not empty coupon.couponId}">
                        <table class="ret-tab sold-table">
                            <tbody>
                            <tr class="tit">
                                <th width="100">优惠券名称</th>
                                <th width="150">券号</th>
                                <th width="150">卡密</th>
                                <th width="80">用户名称</th>
                                <th width="130">领取时间</th>
                                <th width="130">使用时间</th>
                                <!-- 						<th width="150">订单编号</th> -->
                                <th width="60">状态</th>
                            </tr>
                            <c:if test="${empty requestScope.list }">
                                <tr class="detail" style="text-align: center;">
                                    <td colspan="9">没有找到符合条件的优惠券</td>
                                </tr>
                            </c:if>
                            <c:forEach items="${requestScope.list}" var="item" varStatus="status">
                                <tr>
                                    <td>${item.couponName}</td>
                                    <td>${item.couponSn}</td>
                                    <td>${item.couponPwd}</td>
                                    <td>${item.userName}</td>
                                    <td><fmt:formatDate value="${item.getTime}" pattern="yyyy-MM-dd HH:mm"/></td>
                                    <td><fmt:formatDate value="${item.useTime}" pattern="yyyy-MM-dd HH:mm"/></td>
                                    <!-- 						    <td>${item.orderNumber}</td>					     -->
                                    <td>
                                        <c:if test="${item.useStatus eq 1}">
                                        <c:choose>
				   							<c:when test="${nowDate<coupon.endDate}">
							   					<c:choose>
							            			<c:when test="${1==coupon.status}">可使用</c:when>
							                		<c:when test="${0==coupon.status}">已失效</c:when>
							            		</c:choose>
				   							</c:when>
			   								<c:otherwise>已过期</c:otherwise>
			   							</c:choose>
                                        </c:if>
                                        <c:if test="${item.useStatus eq 2}">
                                            已使用
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                        <div style="margin-top:10px;" class="page clearfix">
                            <div class="p-wrap">
                                <ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="simple"/>
                            </div>
                        </div>

                    </c:if>
                    <div class="add-cou-sub">
                        <c:if test="${empty coupon.couponId }">
                            <input type="submit" class="btn-r big-btn" value="保存"/>
                        </c:if>
                        <input type="button" class="btn-g big-btn" value="返回" onclick="window.location='<ls:url address="/s/coupon/query"/>'">
                    </div>
                </form:form>
            </div>
            <!-- 用于提交分页 -->
            <c:if test="${not empty coupon.couponId}">
                <form:form action="${contextPath}/s/coupon/watchCoupon/${coupon.couponId}" method="post" id="userCouponForm">
                    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
                </form:form>
            </c:if>
            <div style="height:100px;"></div>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
<img src="<ls:url address='/resources/common/images/indicator.gif'/>" class="editAdd_load"/>
</body>
<script src="${contextPath}/resources/common/js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>" type="text/javascript"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/multishop/coupon.js'/>"></script>


<script type="text/javascript">
    var contextPath = '${pageContext.request.contextPath}';
    makeSelect2(contextPath + "/s/coupon/getSelect2User", "userIdLists", "用户的手机号", "value", "key");

    //EL表达式不能放在JS,只能放在JSP？
    $(document).ready(function () {
    	
    	laydate.render({
			   elem: '#startDate',
			   calendar: true,
			   theme: 'grid',
			   type:'datetime',
			   min:'-1',
			   trigger: 'click'
		  });

		laydate.render({
			   elem: '#endDate',
			   calendar: true,
			   theme: 'grid',
			   type:'datetime',
			   min:'-1',
			   trigger: 'click'
		  });
    	
        $("#getType").change(function () {
            $("#type").empty();
            var val = $(this).val();
            if (val == "points") {
                $("#type").append("<li><div class='new-cou-con'><span><font color='ff0000'>* </font>兑换所需积分:</span>" +
                        "<input class='${inputclass}' maxlength='6' type='text' <c:if test='${ not empty coupon.couponId}'>disabled='disabled'</c:if>" +
                        " name='needPoints' id='needPoints' value='${coupon.needPoints}' />" +
                        "</div></li>");
            }
        });
    });
</script>
</html>

