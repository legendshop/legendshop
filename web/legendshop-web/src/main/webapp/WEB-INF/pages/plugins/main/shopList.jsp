<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<style>
	.pic-button a {
		border: 1px solid #e5004f;
	    background: #e5004f;
	    color: #fff;
	    display: inline-block;
	    padding: 5px 5px;
	    margin-top: 5px;
	    border-radius: 3%;
	}
	.pic-button a:hover {
		border: 1px solid #cc131d;
		background: #cc131d;
		box-shadow: 1px 1px 5px #585858;
	}
	.pic-shopName {
		height: 21px; 
		font-weight: 600;
		overflow: hidden;
	    text-overflow: ellipsis;
	    white-space: nowrap;
	}
	.pic-shopName a {
		color: #4682B4;
	}
	.pic-shopName a:hover {
		color: #4682DF;
	}
	.picLog-div {
		width: 98px;
		height: 98px;
	}
	.picLog-div:hover {
		opacity:0.9;
  		filter:alpha(opacity=90);
  		box-shadow: 1px 1px 5px #585858;
	}
	.picLog-div img {
		width: 100%;
		height: 100%;
	}
	.div-prod dl:hover {
		opacity: 0.9;
  		filter: alpha(opacity = 90);
		box-shadow: 1px 1px 10px #585858;
	}
	.collection {color: #cc131d;}
</style>
<div class="shop-list" id="shopList">
	<div class="shop-search clear">
		<form id="searchForm" method="get">
			<input type="hidden" id="curPageNO" name="curPageNO" value="1" /> <input type="hidden" id="orderWay" name="orderWay" value="time" /> <input type="hidden"
				id="shopType" name="shopType" value="" />
			<div class="key-word">
				<input type="text" id="searchContent" name="searchContent"   value="${searchParams.searchContent}"  placeholder="输入关键字搜索"><input type="button" onclick="searchShop();" />
			</div>
		</form>
	</div>
	<!-- 搜索表单 -->
	<form:form action="${contextPath}/shop/list" method="post" id="searchForm">
		<input type="hidden" id="curPageNO" name="curPageNO" value="${_page}" />
	</form:form>
	<c:choose>
		<c:when test="${not empty requestScope.list}">
			<div class="shop-list-con">
				<ul>
					<c:forEach items="${requestScope.list}" var="shop" varStatus="status">
						<li class="list-item clear">
							<div class="left" style="width: 125px;">
								<div class="pic-shopName">
									店铺：<a title="${shop.shopName}" href="${contextPath}/store/${shop.shopId}">${shop.shopName}</a>
								</div>
								<div class="picLog-div">
									<c:choose>
								   		<c:when test="${not empty shop.shopPic}">
								   			<a href="${contextPath }/store/${shop.shopId}">
								   				<img src="<ls:photo item='${shop.shopPic }'/>" alt="店铺图片" width="100">
								   			</a>
								   		</c:when>
								   		<c:otherwise>
								   			<a href="${contextPath }/store/${shop.shopId}">
								   				<img src="${contextPath }/resources/common/images/default-img.png" alt="店铺图片">
								   			</a>
								   		</c:otherwise>
								   </c:choose>
								</div>
							   <div style="font-weight: 600;">
								   关注：<em class="collection">${shop.collects}</em></span>
							   </div>
							   <div class="pic-button">
									<a href="${contextPath }/store/${shop.shopId}">进入店铺</a>
									<a onclick="collectShop(this, '${shop.shopId}')">加入收藏</a>
								</div>
							</div>
							<div class="right div-prod">
								<c:forEach items="${shop.prodList}" var="prod" varStatus="prodStatus">
									<dl>
										<dt>
											<a href="${contextPath }/views/${prod.prodId }">
												<img src="<ls:photo item='${prod.pic }'/>" alt="商品图片">
											</a>
										</dt>
									</dl>
								</c:forEach>
							</div>
						</li>
					</c:forEach>
				</ul>
			</div>
			<!--分页-->
			<c:if test="${not empty toolBar}">
				<div class="page yt-wrap clearfix" style="margin-top:20px;">
					<div class="p-wrap">
						<ul class="p-num">
							<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
						</ul>
					</div>
			</c:if>
		</c:when>
		<c:otherwise>
			<div class="selection_box search_noresult" style="padding:150px 0;">
				<p class="search_nr_img" align="center">
					<img src="<ls:templateResource item='/resources/templets/images/not_result.png'/>">
				</p>
				<p class="search_nr_desc" align="center">
					没有找到
					<c:if test="${not empty param.keyword}">与“<span>${param.keyword}</span>”</c:if>
					相关的店铺
				</p>
			</div>
		</c:otherwise>
	</c:choose>
	<ls:page pageSize="${pageSize }" total="${total}"
					curPageNO="${curPageNO }" type="default" />
</div>

