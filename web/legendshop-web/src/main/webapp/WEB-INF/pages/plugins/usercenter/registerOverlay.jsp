<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
            <div class="item fore1">
                <span>手机号码</span>

                <div class="item-ifo">
                    <input id="name"  style="color: #999" name="name" class="text" maxlength="20" autocomplete="off" tabindex="1" onblur="if(this.value==''){this.style.color='#999'}; nameLeave();" onfocus="nameJudge();" type="text">
                    <i class="i-name"></i>
                    <ul id="intelligent-regName" class="hide"></ul>
                    <label id="regName_succeed" class="registBrank"></label>
                    <label id="regName_error" class="hide"></label>
                    </div>
                </div>
            </div>
            <div id="o-password">
                <div class="item fore2">
                    <span>请设置密码</span>

                    <div class="item-ifo">
                        <input id="password" name="password" class="text" autocomplete="off" tabindex="2" type="password" onfocus="passwordJudge();" onblur="passwordLeave();" >
                        <i class="i-pass"></i>
                        <label id="pwd_succeed" class="registBrank"></label>

                        <label class="hide" id="pwdstrength"><span class="fl">安全程度：</span><b></b></label>
                        <label id="pwd_error" class="hide"></label>
                    </div>
                </div>
                
                <div class="item fore3">
                    <span>请确认密码</span>

                    <div class="item-ifo">
                        <input id="password2" name="password2" class="text" tabindex="3" autocomplete="off" type="password" onfocus="password2Judge();" onblur="password2Leave();">
                        <i class="i-pass"></i>
                        <label id="pwdRepeat_succeed" class="registBrank"></label>

                        <label id="pwdRepeat_error" class="hide"></label>
                    </div>
                </div>
                
            </div>
                <div class="item fore4" id="authcodeDiv">
                    <span>验证码</span>

                    <div class="item-ifo">
                        <input type="hidden" id="cannonull" name="cannonull" value='<fmt:message key="randomimage.errors.required"/>'/>
						<input type="hidden" id="charactors4" name="charactors4" value='<ls:i18n key="randomimage.charactors.required" length="4"/>'/> 
						<input type="hidden" id="errorImage" name="errorImage" value='<fmt:message key="error.image.validation"/>'/> 
            				
                        <input onfocus="randnumJudge();" id="randNum" class="text text-1" name="randNum" tabindex="6" autocomplete="off" type="text">
            			<span><img id="randImage" name="randImage" src="<ls:templateResource item='/validCoderRandom'/>" style="vertical-align: middle; margin-top: -3px;"/>看不清？
            			<a href="javascript:void(0)" onclick="javascript:changeRandImg('${contextPath}')" style="font-weight: bold;"><fmt:message key="change.random2"/></a>
            			</span>

                        <div class="clr"></div>
                        <label id="authcode_error" class="hide"></label>
                    </div>
                </div>
            
            <div class="item fore5" id="mobileCodeDiv" style="height: 72px;">
                <span>短信验证码</span>

                <div class="item-ifo">

                    <input onfocus="erifyCodeJudge();" maxlength="6" autocomplete="off" tabindex="6" class="text text-1" name="mobileCode" style="ime-mode:disabled" id="erifyCode" type="text">
                    <label class="registBrank invisible"></label>
					<input style="width:150px;height:35px;" class="" type="button" value="获取短信验证码" onclick="sendMobileCode()" id="erifyCodeBtn"/>
                    <div class="clr"></div>
                    <div class="msg-text" id="mobileCodeSucMessage"></div>
                    <label id="erifyCode_error" class="hide"></label>
                    <label id="erifyCode_succeed" class="registBrank invisible hide"></label>


                </div>
            </div>
            <div class="item item-new">
                <div class="item-ifo">
                    <input class="checkbox"  type="checkbox" checked="checked" id="readme" >
                    <label for="readme" class="color666">我已阅读并同意《<a href="#" onclick="showProtocol();">用户注册协议</a>》</label>
                    <label id="protocol_succeed" class="registBrank"></label>

                    <div class="clr"></div>
                    <label id="protocol_error" class="error hide">请接受服务条款</label>
                </div>
            </div>
            <div class="item">
                <input class="btn-img btn-regist" id="popupRegButton" value="立&nbsp;即&nbsp;注&nbsp;册" tabindex="8" type="button" onclick="quickRegister();">
            </div>
  <script type="text/javascript">
	//注册密码文本框绑定事件
	$(document).ready(function() {
	$("#password").bind('keyup onfocus onblur', function () {
			  var index = checkStrong(this.value);
			  $("#pwdstrength").show();
			  $("#pwdstrength").removeClass("strengthA");
			  $("#pwdstrength").removeClass("strengthB");
			  $("#pwdstrength").removeClass("strengthC");
			  var  letter = ( index == 0? 0:index == 1? 'A':index == 2? 'B':'C') ;
			  if(letter==0){
				  $("#pwdstrength").hide();
			  }
			$("#pwdstrength").attr("class","strength" + letter );
		});
	});
		//密码检测密码强度
		function checkStrong(sValue) {
		    var modes = 0;
		    //正则表达式验证符合要求的
		    if (sValue.length < 1) return modes;
		    if (/\d/.test(sValue)) modes++; //数字
		    if (/[a-z]/.test(sValue)) modes++; //小写
		    if (/[A-Z]/.test(sValue)) modes++; //大写  
		    if (/\W/.test(sValue)) modes++; //特殊字符
		   
		   //逻辑处理
		    switch (modes) {
		        case 1:
		            return 1;
		            break;
		        case 2:
		            return 2;
		        case 3:
		        case 4:
		            return sValue.length < 12 ? 3 : 4;
		            break;
		    }
		}
		
  </script>