<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<li class="fieldset"><label class="caption" style="width:100%;text-align:left;margin-left:25px">商品描述：(建议图片宽度最大为930px)</label>
	<div style="width:100px;height:20px;"></div>
	<label class="caption"></label>
	<div class="fields-box">
		<div class="dc-wapper">
			<div class="dc-tabs">
				<div class="tab actived">PC版详情</div>
				<div class="tab" type="mobile">手机版详情</div>
			</div>
			<div class="dc-edit">
				<textarea name="content" id="content" style="width:100%;height:300px;visibility:hidden;">${productDto.content}</textarea>
			</div>
			<div class="dc-edit-m hide">
				<div class="pv-device-bg" id="promInfoView">
					<div class="pv-device-hd">
						<div id="productTitle" class="tit ell">商品详情</div>
					</div>
					<div class="pv-device-bd">
					
					</div>
				</div>
				<div class="pv-cont-p">
					<textarea name="contentM" id="contentM" style="width:100%;height:300px;visibility:hidden;">${productDto.contentM}</textarea>
				</div>
			</div>
		</div>
	</div>
</li>
<script type='text/javascript'>
$(document).ready(function(e) {  
	//非视频，不加载播放器  
	if(document.getElementById('player') != null) {
		jwplayer('player').onReady(function() {});  
		jwplayer('player').onPlay(function() {});
		jwplayer("player").play(); 
	}  
});
</script>