<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/dialog.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>商家拼团活动详情</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/mergeGroup/mergeGroupDetail.css'/>" rel="stylesheet"/>
</head>
<body>
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
	</div>
	<div class="w1190">
		<div class="seller-cru">
			<p>
				您的位置>
				<a href="javascript:void(0)">首页</a>>
				<a href="javascript:void(0)">卖家中心</a>>
				<span class="on">拼团活动详情</span>
			</p>
		</div>
		<!-- 侧边导航 -->
		<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
		<!-- /侧边导航 -->
		<div class="add-fight">
			<form id="mergeForm">
				<!-- 拼团列表 -->
				<div class="add-item">
					<div class="add-tit"><span>选择活动商品</span></div>
						
					<div class="goods-table" style="margin-top: 20px;">
						<%@ include file="mergeGroupDetailSku.jsp" %>
	                </div>
				</div>
				<!-- /拼团列表 -->
				<!-- 活动信息 -->
				<div class="add-item">
					<div class="add-tit"><span>活动信息</span></div>
					<div class="activity-mes">
						<span class="act-tit"><em>*</em>拼团活动名称：</span>
						<input type="text" disabled="disabled" value="${mergeGroup.mergeName }" class="act-name" placeholder="请填写活动名称">
						<span class="act-des">（2-20）字</span>
					</div>
					<div class="activity-mes">
						<span class="act-tit"><em>*</em>活动时间：</span>
						<span class="act-time">
							<input type="text" value='<fmt:formatDate value="${mergeGroup.startTime }" pattern="yyyy-MM-dd HH:mm:ss" type="date"/>' disabled="disabled">
							&nbsp;-&nbsp;
							<input type="text" value='<fmt:formatDate value="${mergeGroup.endTime }" pattern="yyyy-MM-dd HH:mm:ss" type="date"/>' disabled="disabled">
						</span>
					</div>
				</div>
				<!-- /活动信息 -->
				<!-- 活动规则 -->
				<div class="add-item">
					<div class="add-tit"><span>活动规则</span></div>
					<div class="activity-mes">
						<span class="act-tit"><em>*</em>成团人数：</span>
						<input type="text" class="act-name" placeholder="请填写成团人数" value="${mergeGroup.peopleNumber }" disabled="disabled">
						<span class="act-des">（满足设定人数即可成团）</span>
					</div>
					<%-- <div class="activity-mes">
						<span class="act-tit">限购设置：</span>
						<span class="act-limit">
							<a href="javascripe:void(0)" class="lim-btn <c:if test='${!mergeGroup.isLimit }'>on</c:if>">不限</a><a href="javascripe:void(0)" class="lim-btn <c:if test='${mergeGroup.isLimit }'>on</c:if>">限制</a>
						</span>
						<span class="act-limit number" >
						</span>
					</div> --%>
					<div class="activity-mes">
						<span class="act-tit">团长是否免单：</span>
						<span class="act-limit">
							<a href="javascripe:void(0)" onclick="return false" class="lim-btn <c:if test='${!mergeGroup.isHeadFree }'>on</c:if>">否</a><a onclick="return false" href="javascripe:void(0)" class="lim-btn <c:if test='${mergeGroup.isHeadFree }'>on</c:if>">是</a>
						</span>
					</div>
					<div class="activity-mes" style="margin-top: 15px;">
						<span class="act-tit">活动图片：</span>
							<div class="con-input">
								<img class="add-img" src="<ls:images item='${mergeGroup.pic }' scale="3" />">
							</div>
					</div>
					<div class="btn-box">
						<input type="button" value="返回" onclick="window.history.go(-1);" />
					</div>
					<%-- <div class="activity-mes" style="margin-top: 35px;">
						<span class="act-tit"></span>
						<a href="${contextPath }/s/mergeGroupActivity/query" class="lim-btn on" style="background: #fff;color: #666;border: 1px solid #e4e4e4;">返回</a>
					</div> --%>
				</div>
				<!-- /活动规则 -->
			</form> 
		</div>
	</div>
	<!--foot-->
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	<!--/foot-->
</body>

<script src="${contextPath}/resources/common/js/jquery.validate.js"></script>
<script type="text/javascript">	
$(function(){
	userCenter.changeSubTab("mergeGroupManage"); //高亮菜单
})
var contextPath = "${contextPath}";
var photoPath = "<ls:photo item=''/>";
</script>
</html>