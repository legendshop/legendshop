<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
		<title>创建职位-${systemConfig.shopName}</title>
    	<meta name="keywords" content="${systemConfig.keywords}"/>
   	    <meta name="description" content="${systemConfig.description}" />
		<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/shopJob.css'/>" rel="stylesheet"/>
		<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller.css'/>" rel="stylesheet"/>
		<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
	</head>
	<body>
		<form:form  action="${contextPath}/s/shopJobs/save" method="post" id="from1" target="_parent">
			<input type="hidden" name="id" value="${shopRole.id}">
			<!-- 为解决单个输入框 回车即提交的问题 ,加上隐藏的input-->
			<input type="input" style="display:none;">
			<div class="jobname-content">
				<span>职位名称：</span><input id="jobName" maxlength="20" name="name" onblur="checkName();" onfocus="clearErr();" value="${shopRole.name}" srcval="${shopRole.name}" placeholder="请输入职位名称..." />
				<span class="err-msg hide"></span>
			</div>
			<div class="perm-title">
				<span>设置职位权限</span>
			</div>
			<div class="perm-content">
				<c:forEach items="${requestScope.shopMenuGroups}" var="shopMenuGroup" varStatus="status" >
					<h4>
						 <label class="checkbox-wrapper <c:if test="${shopMenuGroup.selected}">checkbox-wrapper-checked</c:if>">
							<span class="checkbox-item">
								<input type="checkbox" class="checkbox-input" name="menuGroupLabels" value="${shopMenuGroup.label}"/>
								<span class="checkbox-inner" style="margin-right: 2px;"></span>
							</span>
							<span>${shopMenuGroup.name}</span>
					   	</label> 
					</h4>
					<dl>
						<c:forEach items="${shopMenuGroup.shopMenuList}" var="shopMenu">
							<dt>
								<label class="checkbox-wrapper <c:if test="${shopMenu.selected}">checkbox-wrapper-checked</c:if>" >
									<span class="checkbox-item">
										<input type="checkbox" <c:if test="${shopMenu.selected}">checked="checked"</c:if> class="checkbox-input" name="menuLabels" value="${shopMenu.label}" />
										<span class="checkbox-inner" style="margin-right: 2px;"></span>
									</span>
									<span>${shopMenu.name}</span>
							   	</label>
							</dt>
							
							<!-- 查看权限 -->
							<dl style="padding: 0;display: -webkit-box;">
							<c:if test="${not empty shopMenu.urlList}">
							<dd>
								<label class="checkbox-wrapper <c:if test="${shopMenu.checkFunc}">checkbox-wrapper-checked</c:if>" >
									<span class="checkbox-item">
										<input type="checkbox" <c:if test="${shopMenu.checkFunc}">checked="checked"</c:if> class="checkbox-input" name="menuFunctions" value="${shopMenu.label}-check" data-type="check" />
										<span class="checkbox-inner" style="margin-right: 2px;"></span>
									</span>
									<span>查看</span>
							   	</label>
							</dd>
							</c:if>
							
							<!-- 编辑权限 -->
							<c:if test="${not empty shopMenu.editorUrlList}">
							<dd style="margin-inline-start:10px;">
								<label class="checkbox-wrapper <c:if test="${shopMenu.editorFunc}">checkbox-wrapper-checked</c:if>" >
									<span class="checkbox-item">
										<input type="checkbox" <c:if test="${shopMenu.editorFunc}">checked="checked"</c:if> class="checkbox-input" name="menuFunctions" value="${shopMenu.label}-editor" data-type="editor"/>
										<span class="checkbox-inner" style="margin-right: 2px;"></span>
									</span>
									<span>编辑</span>
							   	</label>
							</dd>
							</c:if>
							</dl>
						</c:forEach>
					</dl>
				</c:forEach>
			</div>
			<div class="btn-content">
				<input onclick="saveJob();" type="button" value="保存" class="btn-r small-btn">
				<input onclick="closeDialog();" style="margin-left:10px;" type="button" value="取消" class="btn-g small-btn">
			</div>
		</form:form>
		<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/shopJob.js'/>"></script>
		<script type="text/javascript">
			var contextPath = "${contextPath}";
		</script>
	</body>
</html>