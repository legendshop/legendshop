<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://www.legendesign.net/date-util"  prefix="dateUtil" %>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}"  pattern="yyyy-MM-dd HH:mm" var="nowDate"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>订单详情-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/templets/css/multishop/orderdetail.css'/>" />
    	<style>
    	.order tbody tr td{
   		    border-bottom: 1px solid #e7e7e7;
   			vertical-align: top;
    	}
    	.ncm-order-contnet tfoot td dl{
    		margin-top:2px;
    	}
		.ncm-order-contnet tfoot td dl dt{
			width:100px;
			color:#666;
			text-align: right;
			margin-right:20px;
		}
		.ncm-order-contnet tfoot td dl dd{
			min-width:65px;
		}
		.ncm-order-contnet tfoot td .deposit dd{
			
		}
		.ncm-order-contnet tfoot td .final dd{
			
		}
		
		.ncm-order-contnet tfoot td .order-price dt{
			font-size:14px;
			font-weight: bold;
		}
		.ncm-order-contnet tfoot td .order-price dd{
			font-size:14px;
			font-weight: bold;
			color:#e5004f;
		}
	</style>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">订单详情</span>
				</p>
			</div>
				<%@ include file="../included/sellerLeft.jsp" %>
				<div class="right_con">
			<div class="right-layout">
				<div class="ncm-oredr-show">
					<div class="ncm-order-info">
						<div class="ncm-order-details">
							<div class="title">订单信息</div>
							<div class="content">
							<c:if test="${order.subType != 'SHOP_STORE' }">
								<dl>
									<dt>收货地址：</dt>
									<dd>
										<span>
											<c:if test="${not empty order.userAddressSub}"> 
											   ${order.userAddressSub.receiver} ,
											   ${order.userAddressSub.mobile} ,
											   ${order.userAddressSub.detailAddress}
											</c:if>
										 </span>
									</dd>
								</dl>
								</c:if>
								<dl>
									<dt>发&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;票：</dt>
									<dd>	
									  <c:choose>
											<c:when test="${empty order.invoiceSub}"> 
											    不开发票
											</c:when>
											<c:otherwise>
												${order.invoiceSub.type==1?"普通发票":"增值税发票"}&nbsp;
												<c:choose>
													<c:when test="${order.invoiceSub.title==1}">[个人抬头]</c:when>
													<c:otherwise>[公司抬头]</c:otherwise>
												</c:choose>&nbsp;
												${order.invoiceSub.company}&nbsp;
												<c:if test="${order.invoiceSub.title ne 1}">(${order.invoiceSub.invoiceHumNumber})</c:if>
											</c:otherwise>
										</c:choose>
									</dd>
								</dl>
								<dl>
									<dt>买家留言：</dt>
									<dd>
										<c:choose>
											<c:when test="${empty order.orderRemark}">无</c:when>
											<c:otherwise>${order.orderRemark}</c:otherwise>
										</c:choose>
									</dd>
								</dl>
								<dl>
									<dt>订单编号：</dt>
									<dd>
										${order.subNo}
										<a href="javascript:void(0);">更多
							            <div id="history" class="more"></div>
						            </a>
									</dd>
								</dl>
							</div>
						</div>
						<div class="ncm-order-condition">
							<dl>
								<dt>
									<i class="icon-ok-circle green"></i>订单状态：
								</dt>
								<dd>
									<c:choose>
										<c:when test="${order.refundState eq 1}">
											<span class="col-orange">退款退货中</span>
										</c:when>
										<c:otherwise>
											<c:choose>
												 <c:when test="${order.status eq 1 && order.payPctType eq 0}">
												        订单已经提交，等待买家付款
												 </c:when>
												 <c:when test="${order.status eq 1 && order.payPctType eq 1 && order.isPayDeposit eq 1}">
												        订单已支付定金，待付尾款
												 </c:when>
												 <c:when test="${order.status eq 2 && order.isPayDeposit eq 1 && order.isPayFinal eq 1}">
												       <span class="col-orange">待发货</span>
												 </c:when>
												 <c:when test="${order.status==2 && order.payPctType eq 0}">
												 	<span class="col-orange">待发货</span>
												 </c:when>
												 <c:when test="${order.status eq 3 }">
												       待收货
												 </c:when>
												 <c:when test="${order.status eq 4 }">
												     	 已完成
												 </c:when>
												 <c:when test="${order.status eq 5 or order.status eq -1 }">
												     	 交易关闭
												 </c:when>
												 <c:otherwise>
												     	待付尾款
												 </c:otherwise>
											  </c:choose>
										</c:otherwise>
									</c:choose>
								</dd>
							</dl>
							<ul>
							<c:if test="${order.refundState ne 1 }">
							<c:choose>
								 <c:when test="${order.status eq 1 && order.payPctType eq 0}">
									<li>1. 等待卖家购买此订单的商品，请选择<a class="btn-r" href="#order-step">取消订单</a>操作。</li>
								 </c:when>
								 <c:when test="${order.status eq 1 && order.payPctType eq 1 && order.isPayDeposit eq 1}">
									<li>1. 买家已预先支付定金，可选择<a class="btn-r" href="#order-step">退还定金</a>操作。</li>
								 </c:when>
	                             <c:when test="${order.status eq 2}">
	                              	<li>1. 订单已提交商家进行备货发货准备,对订单进行<a href="#order-step" class="btn-r">商品发货</a>操作。</li></li>
							     </c:when>
							     <c:when test="${order.status eq 3}">
									<li>1. 商品已发出；  物流公司：${order.delivery.delName}；  查看 <a class="blue" href="#order-step">“物流跟踪”</a> 情况。</li>
							     </c:when>
							     <c:when test="${order.status eq 5 or order.status eq -1}">
	        								<li>于<fmt:formatDate value="${order.updateDate}" type="both" /> 取消了订单 ( 改买其他商品 )</li>
							     </c:when>
								</c:choose>
								</c:if>
							</ul>
						</div>
					</div>
					<div class="ncm-order-step" id="order-step">
						<dl class="step-first current">
							<dt>生成订单</dt>
							<dd class="bg"></dd>
							 <dd title="下单时间" class="date"><fmt:formatDate value="${order.subCreateTime}" type="both" /></dd> 
						</dl>
<%--						<dl class="<c:if test="${order.isPayDeposit eq 1 && order.isPayFinal eq 1 || (order.payPctType eq 0 && order.status==2)}">current</c:if>">--%>
						<dl class="<c:if test="${order.isPayDeposit eq 1 && order.isPayFinal eq 1 || (order.payPctType eq 0 && order.isPayed==1)}">current</c:if>">
							<dt>完成付款</dt>
							<dd class="bg"></dd>
<%--							<c:if test="${order.isPayDeposit eq 1 && order.isPayFinal eq 1 || (order.payPctType eq 0 && order.status==2)}"><dd title="付款时间" class="date"><fmt:formatDate value="${order.payDate}" type="both" /></dd></c:if>--%>
							<c:if test="${order.isPayDeposit eq 1 && order.isPayFinal eq 1 || (order.payPctType eq 0 && order.isPayed==1)}"><dd title="付款时间" class="date"><fmt:formatDate value="${order.payDate}" type="both" /></dd></c:if>
						</dl>
						<dl class="<c:if test="${not empty order.dvyDate}">current</c:if>">
							<dt>商家发货</dt>
							<dd class="bg"></dd>
							<c:if test="${not empty order.dvyDate}"><dd title="发货时间" class="date"><fmt:formatDate value="${order.dvyDate}" type="both" /></dd></c:if>
						</dl>
						<dl class="<c:if test="${not empty order.finallyDate}">current</c:if>">
							<dt>确认收货</dt>
							<dd class="bg"></dd>
							<c:if test="${not empty order.finallyDate}"><dd title="确认收货时间" class="date"><fmt:formatDate value="${order.finallyDate}" type="both" /></dd></c:if>
						</dl>
						<dl class="<c:if test="${not empty order.finallyDate}">current</c:if>">
							<dt>完成</dt>
							<dd class="bg"></dd>
							<c:if test="${not empty order.finallyDate}"><dd title="完成时间" class="date"><fmt:formatDate value="${order.finallyDate}" type="both" /></dd></c:if>
						</dl>
					</div>
					<div class="ncm-order-contnet">
						<table class="ncm-default-table order sold-table">
							<thead>
								<tr style="border-bottom: 1px solid #ddd;">
									<th colspan="2">商品名称</th>
									<th class="w120">单价（元）</th>
									<th class="w90">数量</th>
									<th class="w100">交易状态</th>
									<th class="w100" style="border-right: 1px solid #e7e7e7;">交易操作</th>
								</tr>
							</thead>
							<tbody>
							
							  <c:if test="${order.status eq 3 or order.status eq 4}">
							    <tr>
									<th colspan="10">
										<div class="order-deliver">
											<span>物流公司： <a href="${order.delivery.delUrl}"
												target="_blank">${order.delivery.delName}</a>
											</span><span> 物流单号： ${order.delivery.dvyFlowId} </span>
											<span><a id="show_shipping" href="<ls:url address='/s/orderExpress/${order.subNo}'/>" target="_blank">物流跟踪<i class="icon-angle-down"></i>
													<div class="more">
														<span class="arrow"></span>
														<ul id="shipping_ul">
															<li>加载中...</li>
														</ul>
													</div> </a>
											</span>
										</div>
									</th>
								</tr>
							  </c:if>
			
								
								<!-- S 商品列表 -->
							<c:forEach items="${order.orderItems}" var="orderItem" varStatus="orderItemStatues">
							    <tr class="bd-line">
									<td width="90">
										<div class="ncm-goods-thumb" style="margin-left: 10px;">
											<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">
												<img onmouseout="toolTip()" onmouseover="toolTip();"src="<ls:images item='${orderItem.prodPic}' scale='3'/>"/>
											</a>
										</div>
									</td>
									<td class="tl">
										<dl class="goods-name">
											<dt>
												<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">${orderItem.prodName}</a>
											</dt>
											<dd>${orderItem.attribute}</dd>
											<!-- 消费者保障服务 -->
									  </dl>
									</td>
									<td class="refund">
										<span id="actualTotal"><fmt:formatNumber value="${orderItem.prodCash}" type="currency" pattern="0.00"></fmt:formatNumber></span>
										<p class="green"></p>
									</td>
									<td>${orderItem.basketCount}</td>
			
									<!-- S 合并TD -->
			                    <c:if test="${orderItemStatues.index==0}">  
				                     <c:choose>
					                		<c:when test="${fn:length(order.orderItems)>0}">
					                			<!-- 未支付 -->
					                			<td rowspan="${fn:length(order.orderItems)}" class="bdl">
					                		</c:when>
					                		<c:otherwise>
					                			<td class="bdl">
					                		</c:otherwise>
					                </c:choose>
					                <c:if test="${order.refundState eq 1}">
					                	<span class="col-orange">退货退款中</span>
					                </c:if>
					                <c:if test="${order.refundState ne 1}">
					                  <c:choose>
										 <c:when test="${order.status eq 1 }">
										        待付款
										 </c:when>
										 <c:when test="${order.status==2 && order.isPayDeposit eq 1 && order.isPayFinal eq 1}">
										       <span class="col-orange">待发货</span>
										 </c:when>
										 <c:when test="${order.status==2 && order.payPctType eq 0}"><span class="col-orange">待发货</span></c:when>
										 <c:when test="${order.status eq 3 }">
										      等待卖家确认收货
										 </c:when>
										 <c:when test="${order.status eq 4 }">
										      已完成
										 </c:when>
										 <c:when test="${order.status eq 5 or order.status eq -1}">
										      交易关闭
										 </c:when>
										 <c:otherwise>
										     待付尾款
										 </c:otherwise>
									  </c:choose>
					                </c:if>
					                </td>
					                
									  <c:choose>
					                		<c:when test="${fn:length(order.orderItems)>0}">
					                			<!-- 未支付 -->
					                			<td rowspan="${fn:length(order.orderItems)}" class="bdl" style="border-right: 1px solid #e7e7e7;">
					                		</c:when>
					                		<c:otherwise>
					                			<td class="bdl" style="border-right: 1px solid #e7e7e7;">
					                		</c:otherwise>
					                </c:choose>
										<!-- 取消订单 -->
										<p>
									   <c:if test="${order.refundState ne 1}">
									   	  <c:choose>
											 <c:when test="${order.status eq 1 && order.payPctType eq 0}">
											    <a class="btn-r" href="javascript:void(0)" onclick="cancleOrder('${order.subNo}');">
											    <i class="icon-ban-circle"></i> 取消订单</a>
											 </c:when>
											 <c:when test="${order.status eq 1 && order.payPctType eq 1 && order.isPayDeposit eq 1}">
											    <a class="btn-r" href="javascript:void(0)" onclick="shopRefundDeposit('${order.id}', '${order.userId}')">
											    <i class="icon-ban-circle"></i> 退还定金</a>
											 </c:when>
											 <c:when test="${order.status eq 2 }">
											     <a class="btn-r" onclick="deliverGoods('${order.subNo}');" >
											     <i class="icon-ban-circle"></i>商品发货</a>
											 </c:when>
											  <c:when test="${order.status eq 3 }">
											     <a class="btn-r" onclick="changeDeliverNum('${order.subNo}');" >
											     <i class="icon-ban-circle"></i>修改物流单号</a>
											 </c:when>
										  </c:choose>
									   </c:if>
									</p>
									</td>
									
									</c:if>
									<!-- E 合并TD -->
								</tr>
							</c:forEach>
							<tr style="color:#999;">
				                <td colspan="2">阶段1：定金</td>
				                <td colspan="1">
									<c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
									¥<span class="preDepositPrice"><fmt:formatNumber value="${order.depositPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
									(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
									</c:if>
									<c:if test="${order.payPctType eq 1}"><!-- 定金支付 -->
									¥<span class="preDepositPrice"><fmt:formatNumber value="${order.depositPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
									</c:if>
				                </td>
				                <%-- <c:if test="${order.status eq 5}">
				                	<td colspan="4"></td>
				                </c:if> --%>
				                <c:if test="${order.status ne 5}">
					                <c:if test="${order.isPayDeposit eq 0 }">
						                <c:if test="${dateUtil:getOffsetMinutes(order.subCreateTime,nowTime) le 60}">
					                		<td colspan="2" align="center">用户可付定金时间剩余<font color="#e5004f">${60 - dateUtil:getOffsetMinutes(order.subCreateTime,nowTime)}</font>分钟</td>
					                		<td align="center" style="border-right: 1px solid #e6e6e6;">买家未付款</td>
					                	</c:if>
					                	<c:if test="${dateUtil:getOffsetMinutes(order.subCreateTime,nowTime) gt 60}">
					                		<td colspan="4" align="center" style="border-right: 1px solid #e6e6e6;">已过期</td>
					                	</c:if>
					                </c:if>
				                </c:if>
				                <c:if test="${order.isPayDeposit eq 1}">
					                	    <td colspan="4" align="center" style="border-right: 1px solid #e6e6e6;">
					                	      		<span class="col-orange">买家已付款</span><br/>
						                	        <span class="col-orange">${order.finalPayName}</span><br/>
						                	        <span class="col-orange">${order.finalTradeNo}</span><br/>
						                 </td>
					             </c:if>
					               
				                
				              </tr>
				              <tr style="color:#999;">
				                <td colspan="2">阶段2：尾款</td>
								  <td colspan="1">¥<span id="finalPrice" class="finalPrice">
									  <c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
											<span class="finalPrice"><fmt:formatNumber value="${order.finalPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
									  </c:if>
									  <c:if test="${order.payPctType eq 1}"><!-- 定金支付 -->
											<span class="finalPrice"><fmt:formatNumber value="${order.finalPrice+order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber></span>
											(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
									  </c:if>
								  </td>
				                <%-- <c:if test="${order.status eq 5}">
				                	<td colspan="4"></td>
				                </c:if> --%>
				                <c:if test="${order.status ne 5}">
				                	<c:if test="${order.isPayFinal eq 0}">
						                <td colspan="2" align="center">
						                	尾款支付时间: </br><fmt:formatDate value="${order.finalMStart }" pattern="yyyy-MM-dd" /> - <fmt:formatDate value="${order.finalMEnd }" pattern="yyyy-MM-dd" />
						                </td>
						                <td colspan="2" align="center" style="border-right: 1px solid #e6e6e6;">
					                	<!-- 未付款尾款 -->
					                		<c:choose>
						                		<c:when test="${order.finalMStart gt nowDate }">未开始</c:when>
						                		<c:when test="${order.finalMStart le nowDate and order.finalMEnd gt nowDate}">买家未付款</c:when>
						                		<c:when test="${order.finalMEnd le nowDate }">已过期</c:when>
						                	</c:choose>
					                    </td>
					                </c:if>
				                </c:if>
				                
				                <!-- 已付尾款 -->
					              <c:if test="${order.isPayFinal eq 1}">
					                  <td colspan="4" align="center" style="border-right: 1px solid #e6e6e6;">
					                	     <span class="col-orange">买家已付款</span><br/>
						                	 <span class="col-orange">${order.finalPayName}</span><br/>
						                	 <span class="col-orange">${order.finalTradeNo}</span><br/>
						             </td>
					              </c:if>
				              </tr>
							</tbody>
							<tfoot>
								<tr>
									<td ></td><td></td><td></td>
									<td colspan="17" style="text-align: right;">
											<c:if test="${not empty order.discountPrice && order.discountPrice!=0}">
												<dl><dd>促销优惠： -￥<fmt:formatNumber value="${order.discountPrice}" type="currency" pattern="0.00"></fmt:formatNumber></dd></dl>
											</c:if>
											<c:if test="${not empty order.couponOffPrice && order.couponOffPrice!=0}">
												<dl><dd>优惠券： -￥<fmt:formatNumber value="${order.couponOffPrice}" type="currency" pattern="0.00"></fmt:formatNumber></dd></dl>
											</c:if>
											<dl class="deposit">
												<dt></dt>
												<dd>商品定金：
													<em>￥<span class="preDepositPrice"><fmt:formatNumber value="${order.depositPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span></em>元
													<c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
										            	(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
									            	</c:if>
												</dd>
											</dl>
											<dl class="final">
												<dt></dt>
												<dd>商品尾款：
													<!-- 全额支付 -->
													<c:if test="${order.payPctType eq 0}">
														<em>￥<span class="finalPrice">0.00
													</c:if>
													<c:if test="${order.payPctType eq 1}">
														<em>￥<span class="finalPrice"><fmt:formatNumber value="${order.finalPrice + order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber></span></em>元
										            	(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
									            	</c:if>
												</dd>
											</dl>
											<dl class="order-price">
												<dt></dt>
												<dd>订单金额：
													<%-- 全额支付 --%>
													<c:if test="${order.payPctType eq 0}">
														<em>￥<fmt:formatNumber value="${order.depositPrice}" type="currency" pattern="0.00"></fmt:formatNumber></em>元
													</c:if>
													<c:if test="${order.payPctType eq 1}">
														<em>￥<fmt:formatNumber value="${order.depositPrice+order.finalPrice+order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber></em>元
													</c:if>
												</dd>
											</dl>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		 </div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/ToolTip.js'/>"></script>
	<script src="<ls:templateResource item='/resources/templets/js/presellOrderManage.js'/>" type="text/javascript"></script>
	<script type="text/javascript">
	 var contextPath ='${contextPath}';
	 var dvyFlowId ='${order.delivery.dvyFlowId}';
	 var queryUrl ='${order.delivery.queryUrl}';
	 var subId =  '${order.subId}';
	 
	  $.ajax({
		url:contextPath+"/s/findSubHisInfo", 
		data:{"subId": subId},
		type:'POST', 
		async : false, //默认为true 异步   
		dataType:'json',
		success:function(result){
		    if(result=="fail" || result=="" ){
			   $("#history").after("");
			}else{
			     var list = eval("("+result+")"); 
			     var html="<ul>";
				 for(var obj in list){ //第二层循环取list中的对象
				    html+="<li>"+list[obj].value+"</li>";
				 } 
				 html+="</ul>";
				 $("#history").html(html);
			}
		}
	 });
 </script>
</body>
</html>