<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/storeProdList.css'/>" rel="stylesheet" />
 <c:if test="${not empty requestScope.list}">
     <nav id="main-nav" class="sort-bar">
		<div class="nch-sortbar-array">
			排序方式：
			<ul>
				<li id="default" class="selected">
				  <a title="默认排序" href="javascript:void(0);">默认</a>
				</li>
				<li id="buys">
				    <a title="点击按销量从高到低排序" href="javascript:void(0);">销量<i></i></a>
			    </li>
				<li id="comments">
				  <a title="点击按评论数从高到低排序" href="javascript:void(0);">评论数<i></i></a>
				 </li>
				<li id="cash">
				    <a title="点击按价格从高到低排序" href="javascript:void(0);">价格<i></i></a>
				</li>
			</ul>
		</div>
	</nav>
 </c:if>
<div class="store_goods_list">
      <c:forEach items="${requestScope.list}" var="product" varStatus="status">
		 <ul class="goods_box">
			<li style="width:200px;height:200px;">
			 <table width="200" height="200" cellspacing="0" cellpadding="0">
						<tr><td valign="middle" style="text-align: center;">
							<a href="<ls:url address="/views/${product.prodId}" />"
								target="_blank"> <img style="max-width:195px;max-height:195px;"
									src="<ls:images item='${product.pic}' scale='0' />">
							</a>
						</td></tr>
					</table>
			</li>
			<li class="goods_name"><a
				href="<ls:url address="/views/${product.prodId}" />" target="_blank">${product.name}</a>
			</li>
			<li class="goods_price"><b>¥${product.cash}</b><span>${product.comments}条评价</span>
			</li>
		</ul>
	</c:forEach>       
   <div style="clear: both;"></div>
   <c:if test="${empty requestScope.list}">
			<p align="center">
				  暂无商品信息
			</p>
	</c:if>
</div>
	<div class="clear"></div>
	<div style="margin-top:10px;" class="page clearfix">
    			 <div class="p-wrap">
           		 <c:if test="${toolBar!=null}">
						<span class="p-num">
							<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
						</span>
					</c:if>
    			 </div>
			</div>
<script type="text/javascript">
  jQuery(document).ready(function(){
     var order="${order}";
    
     $(".nch-sortbar-array ul li").each(function(i) {
		  $(this).removeClass("selected");
	 });
     if(order==""|| order==null || order==undefined){
       $("#default").addClass("selected");
     }
     else{
        var orders=order.split(",");
        $("#"+orders[0]).addClass("selected");
        if (orders[1]=="desc") {
		   $("#"+orders[0]).find("a").attr("class","desc");
		 }else{
			$("#"+orders[0]).find("a").attr("class","asc");
		}
     }
  });
</script>