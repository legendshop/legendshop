<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form"  prefix="form"%> 
<%
  String appContext = request.getContextPath();
  String basePath = request.getScheme()+"://"+request.getServerName()+":"+ request.getServerPort() + appContext;
  request.setAttribute("contextPath", basePath);
  response.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT");   
%>
<!DOCTYPE>
<html>
<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
   <meta name="author" content="qinuoli">
   <title>微信支付</title>
   <meta name="apple-mobile-web-app-capable" content="yes">
   <meta name="apple-mobile-web-app-status-bar-style" content="black">
   <meta name="format-detection" content="telephone=no">
   <link type="text/css" href="${contextPath}/resources/templets/css/pop-pay.css" rel="stylesheet"/>
</head>


<body>
	
	<div class="yt-wrap" style="padding: 0 0 20px;width: 1190px;margin: 0 auto;">
		<div class="c-wechat-payment J_wechat_payment">
			<div class="wechat-payment-header">
                <div class="wechat-payment-title">
                    <div class="u-icon  icon-wechat-payment"></div>
                </div>
			</div>
			<div class="wechat-payment-body">
                <div class="c-wechat-qr  J_wechat_qr is-wechat-qr-data" data-hover="c-wechat-qr-hover" data-touch="z-touch">
                    
                    <div class="wechat-qr-loading">
                        <div class="wechat-qr-inner">
                            <span class="ii-loading-pink-32x32 "></span><span class="wechat-qr-loading-text">生成二维码中...</span>
                        </div>
                    	<div class="wechat-qr-phone-img" id="J_wechat_phone_img"></div>
                    </div>
                    
                    
                </div>

                <div class="wechat-qr-explain">
                    <div class="c-scan-tips J_scan_tips">
                        <div class="scan-tips-body  scan-tips-body-default">
                            <div class="u-icon  icon-qr-scan  scan-tips-icon"></div>
                            <p class="scan-tips-text">请使用微信“扫一扫”<br>扫描二维码支付</p>
                        </div>
                      <!--   <div class="scan-tips-body  scan-tips-body-success">
                            <div class="u-icon  icon-success-large  scan-tips-icon"></div>
                            <p class="scan-tips-text">扫码成功！<br>请在微信上完成支付</p>
                        </div> -->
                    </div>
                </div>

                <div class="wechat-order-info">
                    <span class="wechat-order-total">
                        <span class="m-price  u-highlight">
                            <span class="u-yen">¥</span><span class="u-price">${totalFee}</span>
                        </span>
                    </span>
                </div>

                <div class="wechat-notice">
                    <div class="wechat-notice-text" data-toggle="tooltip">
                        <p class="wechat-tips-text">完成支付没有提示？</p>
                        <div class="c-trigger-tooltips">
                            <span class="if-query"></span>
                            <div class="ui-tooltips  ui-tooltips-top-left-arrow wechat-tips-tooltips">
                                <div class="ui-tooltips-arrow">
                                    <span class="arrow arrow-out">◆</span>
                                    <span class="arrow">◆</span>
                                </div>
                                <div class="ui-tooltips-content">
                                    <p class="ui-tooltips-msg"></p>
                                    <p class="wechat-text-title">温馨提示：</p>
                                                                                                           如您已完成微信支付，但页面无任何提示，请至
                                         <c:choose>
											<c:when test="${fn:startsWith(show_url, 'http://')}">
											   <a href="${show_url}" >订单管理</a>
											</c:when>
											<c:otherwise>
											   <a href="http://${show_url}" >订单管理</a>
											</c:otherwise>
										 </c:choose>                                                                  
                                                                                                           查看支付结果。<p></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
	
    
</body>

<script>
	var contextPath="${contextPath}";
	var url = "${code_url}";
	var show_url="${show_url}";
	var subSettlementSn="${subSettlementSn}";
 </script>
 <script type="text/javascript" language="javascript" src="${contextPath}/resources/common/js/jquery-1.9.1.js"></script> 
 <script type="text/javascript" language="javascript" src="${contextPath}/resources/plugins/qrcode/jquery.qrcode.min.js"></script> 
 <script type="text/javascript" language="javascript" src="${contextPath}/resources/templets/js/weixinscanpay.js"></script>

</html>