<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<html>
<head>
   <%@include file='/WEB-INF/pages/common/taglib.jsp'%>
   <%@ include file="/WEB-INF/pages/common/layer.jsp"%>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>商家列表-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />

    <link rel="stylesheet" href="<ls:templateResource item='/resources/templets/css/base.css'/>" />
    <link rel="stylesheet" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" />
    <link rel="stylesheet" href="<ls:templateResource item='/resources/templets/css/shop-list${_style_}.css'/>" />
</head>
<body  style="background: #ffffff;">
	<div id="doc">
	    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div id="bd">
	     
	            <!--crumb-->
      <div class="seller-cru" style="width:1190px;margin: 20px auto 10px;">
        <p>  您的位置&gt;
          <a href="${contextPath}/home">首页</a>&gt;
          <span style="color: #c6171f;">商家列表</span>
        </p>
      </div>  
			    <!-- 店铺列表 -->
			    <div id="shopList"><%@ include file="shopList.jsp" %></div>
		
    </div>	       
		 <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
   </div>

<script type="text/javascript">
    	function pager(curPageNO){
			document.getElementById("curPageNO").value=curPageNO;
			document.getElementById("form1").submit();
		};
</script>
<script src="<ls:templateResource item='/resources/templets/js/shopList.js'/>"></script>
</body>
</html>