<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<ul class="floor_sear_pro">
<c:forEach items="${requestScope.list}" var="product" varStatus="status">
   <li> 
        <a class="floor_sear_a" goods_id="${product.prodId}" goods_name="${product.name}" cash="${product.cash}" img_uri="${product.pic}" onclick="goods_list_set(this);" href="javascript:void(0);"> <span class="floor_sear_img"> 
         <img width="70" height="70" src="<ls:images item='${product.pic}' scale='2' />"> </span> <span class="floor_sear_name">${product.name}</span> </a> 
  </li>
</c:forEach>

<c:if test="${empty requestScope.list}">
    暂无商品信息
</c:if>
</ul>
<div class="floor_page" align="right">
	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default" actionUrl="javascript:productLoad"/>
</div>
