<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<html>
<head>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>"
          rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/public.css'/>"
          rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/store_black.css'/>"
          rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/decorate.css'/>"
          rel="stylesheet"/>
    <%@ include file="/WEB-INF/pages/common/jquery.jsp" %>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>营业执照信息-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <style type="text/css">
        .guige {
            font-size: 15px;
            width: 25%;
            height: 50px;
        }

        #table {
            border: 1px whitesmoke solid;
            background-color: whitesmoke;

        }

        #businessImage {
            width: 600px;
            height: 300px;
        }
    </style>
</head>


<body class="graybody">
<div id="doc">

    <!--顶部menu-->
    <div id="hd">
        <!--hdtop 开始-->
        <%@ include file="/WEB-INF/pages/plugins/main/home/header.jsp" %>
        <!--hdtop 结束-->

        <!-- hdmain 开始-->
        <%@include file="/WEB-INF/pages/plugins/main/home/hdShopMain.jsp" %>
        <!-- hdmain 结束-->
    </div>


    <div id="bd">

        <div id="tablediv" style="text-align: center; padding-top: 50px">
            <center>
                <div style="font-size: 15px;padding-bottom: 50px">根据国家工商总局《网络交易管理办法》要求对网店营业执照信息公示如下：</div>
                <c:set var="text" value="${message}"></c:set>
                <c:choose>
                    <c:when test="${not empty text}">
                        <table cellpadding="0" cellspacing="0" width="50%" id="table">
                            <tbody>
                            <tr>
                                <th class="guige" colspan="2" style="">企业名称：</th>
                                <td style="text-indent: 10px;">${text.enterpriseName}</td>
                            </tr>
                            <tr>
                                <th class="guige" colspan="2" style=";">营业执照注册号：</th>
                                <td style="text-indent: 10px;">${text.registerNumber}</td>
                            </tr>
                            <tr>
                                <th class="guige" colspan="2" style="">法定代表人姓名：</th>
                                <td style="text-indent: 10px;">${text.representative}</td>
                            </tr>
                            <tr>
                                <th class="guige" colspan="2" style="">营业执照所在地：</th>
                                <td style="text-indent: 10px;">${text.businessAddress}</td>
                            </tr>
                            <tr>
                                <th class="guige" colspan="2" style="">企业注册资金：</th>
                                <td style="text-indent: 10px;">${text.registerAmount}</td>
                            </tr>
                            <tr>
                                <th class="guige" colspan="2" style="">营业执照有效期：</th>
                                <td style="text-indent: 10px;">${text.availableTime}</td>
                            </tr>
                            <tr>
                                <th class="guige" colspan="2" style="">营业执照经营范围：</th>
                                <td style="text-indent: 10px;">${text.businessRange}</td>
                            </tr>
                            <tr>
                                <th class="guige" colspan="2" style="">营业时间：</th>
                                <td style="text-indent: 10px;">${text.businessHours}</td>
                            </tr>
                            <th class="guige" colspan="2" style="">营业执照及其他证照：</th>
                            <c:if test="${not empty text.imageList}">
                                <c:forEach items="${text.imageList}" var="node" varStatus="n">
                                    <tr>
                                        <th class="guige" colspan="2" style=""></th>
                                        <td style="text-indent: 10px;">
                                            <img id="businessImage" style="margin-bottom: 20px;" src="<ls:photo item='${node}'/>">
                                        </td>
                                    </tr>
                                </c:forEach>
                            </c:if>
                                <%--                                <td style="text-indent: 10px;">--%>
                                <%--                                    <img id="businessImage" src="<ls:photo item='${text.businessImage}'/>">--%>
                                <%--                                </td>--%>
                            </tbody>
                        </table>
                    </c:when>
                    <c:otherwise>
                        <div>此店铺没有营业执照信息</div>
                    </c:otherwise>
                </c:choose>
                <div style="font-size: 15px;padding-bottom: 50px;padding-top: 30px">注：以上营业执照信息，根据国家工商总局《网络交易管理办法》要求对入驻商家营业执照信息进行公示，除企业名称通过认证之外，其余信息由卖家自行申报填写。如需进一步核实，请联系当地工商行政管理部门。</div>
            </center>
        </div>


        <!--店铺头部-->


        <!--导航行-->
        <div id="store_nav" style="display: none;" class="store_nav_width">
            <div class="main">
                <div class="store_nav">
                    <ul>
                        <li>
                            <a id="current" href="javascript:void(0);">店铺首页</a></li>
                        <c:forEach items="${shopDecotate.shopDefaultNavs}" var="nav" varStatus="s">
                            <li><a
                                    <c:if test="${nav.winType=='new_win'}">
                                        target="_blank"
                                    </c:if> href="${nav.url}">${nav.title}</a>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
            </div>
        </div>
        <!--导航行-->

        <!--默认幻灯-->
        <div id="store_slide" style="display: none;">
            <div id="default_fullSlide" class="fullSlide">
                <div id="default_bd" class="bd">
                    <ul>
                        <c:forEach items="${shopDecotate.shopDefaultBanners}" var="banner" varStatus="b">
                            <li id="${banner.id}">
                                <a href="${banner.url}" style="text-align:center;">
                                    <img src="<ls:photo item='${banner.imageFile}' />">
                                </a>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
                <div id="default_hd" class="hd">
                    <ul>
                        <c:if test="${fn:length(shopDecotate.shopDefaultBanners)>1}">
                            <c:forEach items="${shopDecotate.shopDefaultBanners}" var="banner" varStatus="b">
                                <li
                                        <c:if test="${b.count==1}">class="on"</c:if> >${b.count}</li>
                            </c:forEach>
                        </c:if>
                    </ul>
                </div>
            </div>
        </div>
        <!--默认幻灯-->

        <!--装修布局内容-->

        <!--布局上-->
        <c:forEach items="${shopDecotate.topShopLayouts}" var="layout_top" varStatus="l">
        <c:if test="${not empty layout_top.layoutModuleType}">
        <div class="layout_one" location_mark="${layout_top.layoutId}" location="true">
            <c:choose>
            <c:when test="${layout_top.layoutModuleType=='loyout_module_hot'}">
            <div layout="layout0" id="content" type="${layout_top.layoutModuleType}" mark="${layout_top.layoutId}"
                 url="" style="text-align: -webkit-center;">
                </c:when>
                <c:otherwise>
                <div layout="layout0" id="content" type="${layout_top.layoutModuleType}" mark="${layout_top.layoutId}"
                     url="" style="text-align: -webkit-center;">
                    </c:otherwise>
                    </c:choose>
                </div>
            </div>
            </c:if>
            </c:forEach>
            <!--布局上-->
            <div style="clear: both;"></div>

            <!--布局中-->
            <div id="main_layout" class="main ui-sortable">
                <%@ include file="/WEB-INF/pages/plugins/decorate/decorateMainLayout.jsp" %>
            </div>
            <!--布局中-->


            <div style="clear: both;"></div>

            <!--布局下-->
            <c:forEach items="${shopDecotate.bottomShopLayouts}" var="layout_bottom" varStatus="l">
                <c:if test="${not empty layout_bottom.layoutModuleType}">
                    <div class="layout_one" location_mark="${layout_bottom.layoutId}" location="true">
                        <div style="text-align:center" layout="layout1" id="content"
                             type="${layout_bottom.layoutModuleType}" mark="${layout_bottom.layoutId}" url="">
                            <h3 class="module_wide">
                            </h3>
                        </div>
                    </div>
                </c:if>
                <div style="clear: both;"></div>
            </c:forEach>
            <!--布局下-->

            <!--装修布局内容-->
        </div>

        <!--返回最顶部-->
        <div style="position: fixed; width: 40px; height: 118px; float: left; right: 20px; bottom: 150px; display: block;z-index:1000;"
             id="back_box">
            <div class="back_top"><a target="_self" title="返回顶部" style="display: block;" bosszone="hometop" href="#"
                                     id="toTop"></a></div>
        </div>


    </div>

    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>

</div>
</body>
<script src="${contextPath}/resources/templets/js/multishop/jquery.SuperSlide.2.1.1.js" type="text/javascript"></script>
<script src="${contextPath}/resources/templets/js/multishop/decoratePreview.js" type="text/javascript"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/score.js'/>"></script>
<script type="text/javascript">
</script>
</html>
