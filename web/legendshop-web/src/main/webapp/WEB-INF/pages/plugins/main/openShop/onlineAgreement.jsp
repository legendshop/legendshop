<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
  <title>在线合同协议-${systemConfig.shopName}</title>
  <meta name="keywords" content="${systemConfig.keywords}"/>
  <meta name="description" content="${systemConfig.description}" />
</head>
<body>
<div id="doc">
   <div id="hd">  
      <%@ include file="../home/top.jsp" %>
   </div>
   <div id="bd">
      
     
       <div class="yt-wrap" style="padding:0px 0 20px;">
            <div class="seller-cru" style="width:1190px;margin: 20px auto 20px;">
              <p>
                您的位置&gt;
                <a href="#">首页</a>&gt;
                <span style="color: #e5004f;">免费开店</span>
              </p>
            </div>
            <div class="main">
              <div class="settled_box">
                <h3><span class="settled_title">在线合同协议确认</span></h3>
                <div class="settled_title_h"><span>《入驻协议》</span></div>
                <div class="settled_inbox">
                  <div class="settled_p">
                  <textarea style="width: 100%;height: 400px;" readonly="readonly">
                  	${systemConfig.openShopProtocolTemplate}
                  	</textarea>
                  </div>
                </div>
                <div class="settled_bottom">
                  <span>
                    <b class="settled_b">
                      <label>
                        <input class="agreeOnline" style="cursor:pointer; vertical-align: middle;" type="checkbox" >
                        <s style="text-decoration:none; cursor:pointer;font-weight: initial;">我已仔细阅读并同意协议</s>
                      </label>
                    </b>
                    <a href="javascript:void(0);" class="up_step_btn" onclick="shopPrevious(4)">上一步</a>
                    <a href="javascript:void(0);" class="settled_btn" onclick="agreeOnline()">
                      <em>提交入驻申请</em>
                    </a>
                  </span>
                </div>
              </div>
            </div>
      </div>
   </div>
		<%@ include file="../home/bottom.jsp" %>
</div>
<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>"  type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/openshop.js'/>"></script>
<script>
$(document).ready(function() {
     window.history.forward(1);
});
 
</script>
</body>
</html>
