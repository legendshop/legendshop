<!DOCTYPE html>
	<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
	<%@ include file='/WEB-INF/pages/frontend/templetGoat/common/taglib.jsp' %>
	<%@ include file='/WEB-INF/pages/frontend/templetGoat/common/common.jsp' %>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<title>我的余额-充值明细</title>
	<link rel="stylesheet" href="<ls:templateResource item='/resources/templets/css/userProdeposit.css'/>">
</head>
<body>

<div class="fanhui_cou">
	<div class="fanhui_1"></div>
	<div class="fanhui_ding">顶部</div>
</div>
	<div class="w768">
		<header>
			<div class="back">
				<a href="${contextPath}/p/userhome">
					  <img src="<ls:templateResource item='/resources/templets/images/back.png'/>" alt=""> 
				</a>
			</div>
			<p>我的余额</p>
		</header>
		
		<div class="collection-nav">
			<ul>
				<li>
					<a href="${contextPath}/p/m/predeposit/account_balance">
						账户记录
					</a>
				</li>
				<li class="collection-nav-on">
					<a href="${contextPath}/p/m/predeposit/predepositIndex">
						充值明细
					</a>
				</li>
			</ul>
		</div>
		
<!-- 头部 -->
		<div class="my-poi">
		 
			<%-- <p>当前可用余额：<span>￥<fmt:formatNumber value="${userDetail.availablePredeposit}" pattern="0.00#"/></span></p> --%>
			<table cellspacing="0" cellpadding="0" id="integralTab">
				<tr>
					<td>充值金额</td>
					<td>支付方式</td>
					<td style="width:40%">创建时间</td>
					<td style="width:18%">操作</td>
				</tr>
			
				<c:forEach items="${requestScope.list}" var="recharge">
					   <tr class="gra">
						    <td><fmt:formatNumber value="${recharge.amount}" pattern="#,#0.00#"/></td>
						    <td>
						                     <c:choose>
												<c:when test="${recharge.paymentState==0}">
												   --------
												</c:when>
												<c:when test="${recharge.paymentState==1}">
												     ${recharge.paymentName}
												</c:when>
											 </c:choose>
						    </td>
						    <td>
						        <fmt:formatDate value="${recharge.addTime}"  type="both"/>
						    </td>
							<td>
								<c:choose>
									<c:when test="${recharge.paymentState==0}">
									   <a href="javascript:void(0);" class="commodity-prize-on" onclick="deleteRechargeDetail('${recharge.sn}');">删除</a>
									</c:when>
									<c:otherwise>
									   <a class="commodity-prize-on" href="<ls:url address='/p/m/predeposit/rechargeDetail/${recharge.id}'/>">查看</a>
									</c:otherwise>
								 </c:choose>
							</td>
					   </tr>
				 </c:forEach>
			
			</table>
			
			<input type="hidden" id="pageCount" name="pageCount"  value="${pageCount}"/>
			<input type="hidden" id="curPageNO" name="curPageNO"  value="${curPageNO}"/>	

	   </div>
 <!-- loading  -->
<section class="loading" id="loading" style="transform-origin: 0px 0px 0px;opacity: 1;transform: scale(1, 1);">
</section>

<!-- error 提示区 -->
<section class="error" style="transform-origin: 0px 0px 0px;opacity: 1;transform: scale(1, 1); display: none;">
    <div>
        <p>
            <span class="text"></span>
        </p>
    </div>
</section>
<%-- <%@ include file='/WEB-INF/pages/frontend/templetGoat/common/footer.jsp' %>
 --%></body>
<script type="text/javascript">
  var pageContext="${contextPath}";
</script>
<script charset="utf-8"src="<ls:templateResource item='/resources/templets/js/prodepost.js'/>"></script>
</html>