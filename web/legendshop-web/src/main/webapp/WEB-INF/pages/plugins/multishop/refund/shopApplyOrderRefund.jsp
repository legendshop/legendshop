<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>

<html>
  <head>
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>申请订单退款-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
	<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
  </head>
<body class="graybody">
	   <div id="bd">
	   		 <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
			 <div class="w1190"> 
       	 		<div class="seller-cru">
					<p>
						您的位置> <a href="${contextPath}/home">首页</a>> 
						<a	href="${contextPath}/sellerHome">卖家中心</a>> 
						<a  href="${contextPath}/s/refundReturnList/1">退款及退货</a>>
						<span class="on">退款详情</span>
					</p>
				</div>
				
				<%@ include file="../included/sellerLeft.jsp" %>
		
			    <!-----------right_con-------------->
			     <div class="right_con">
			     	
			         <div class="o-mt" style="margin-top:20px;"><h2>申请退款</h2></div>
			         <!-- 操作类型选择 -->
			         <div class="pagetab2">
			                 <ul>           
				                   <li class="on"><span>仅退款</span></li>
			                 </ul>
			         </div>
			
					<!-- 操作提示 -->
			         <div class="alert">
			            <h4>操作提示：</h4>
			            <ul>
			              <li>1. 若您未收到货，或已收到货且与商家达成一致不退货仅退款时，请选择<em>“仅退款”</em>选项。</li>
			              <li>2. 若为商品问题，或者不想要了且与商家达成一致退货，请选择<em>“退货退款”</em>选项，退货后请保留物流底单。</li>
			              <li>3. 若提出申请后，商家拒绝退款或退货，可再次提交申请或选择<em><a href="javascript:void(0);" onclick="qqLink('${systemConfig.qqNumber}');" >“商品投诉”</a></em>，请求商城客服人员介入。</li>
						  <li>4. 成功完成退款/退货；经过商城审核后，退款金额原路退回。</li>
			            </ul>
			        </div>
			        
			        <!-- 申请退款块 -->
			        <div id="refundBox" >
				        <!-- 申请进度 -->
				        <div class="ret-step">
				          <ul>
				            <li class="done"><span>用户定金已付款</span></li>
				            <li class="done"><span>商家主动申请退款</span></li>
				            <li><span>平台审核，退款完成</span></li>
				          </ul>
				        </div>
			        
				        <!-- 申请"仅退款"表单 -->
				        <form:form id="refundForm" action="${contextPath}/s/shopRefund/save" method="POST" enctype="multipart/form-data">
				        	<input type="hidden" id="orderId" name="orderId" value="${refund.orderId}"/>
				        	<input type="hidden" id="userId" name="userId" value="${userId}"/>
					        <div class="write-apply">
					          <div class="ste-tit"><b>请填写退款申请</b></div>
					          <div class="ste-con">
					            <ul id="formContent">
					              <li>
					              	<span><em>*</em>退款原因：</span>
					              	<select id="buyerMessage" name="buyerMessage">
					              		<option value="">请选择退款原因</option>
					              		<option value="库存不足">库存不足</option>
					              		<option value="不能按时发货">不能按时发货</option>
					              		<option value="其他">其他</option>
					              	</select>
					              </li>
					              <li>
					              	<input type="hidden" id="depositRefundAmount" name="depositRefundAmount" value="${refund.depositRefundAmount}"/>
					              	<span><em>*</em>定金金额：</span><em style="color: #e5004f;height: 30px;line-height: 30px;">￥<fmt:formatNumber value="${refund.depositRefundAmount}" type="currency" pattern="0.00"></fmt:formatNumber>&nbsp;</em>
					              	<c:choose>
						              	<c:when test="${refund.isPayFinal eq 1}"><!-- 已支付尾款，才能退 -->
						              		<label class="checkbox-wrapper checkbox-wrapper-checked" style="margin-left: 10px; vertical-align: text-bottom;">
												<div class="checkbox-item">
													<input type="checkbox" class="checkbox-input" id="isRefundDeposit" value="true" name="isRefundDeposit" checked="checked">
													<em class="checkbox-inner"></em>
												</div>
												是否退定金
											</label>
						              	</c:when>
						              	<c:otherwise>
						              		<input type="hidden" name="isRefundDeposit" value="false"/>
						              	</c:otherwise>
					               </c:choose>
					              </li>
					              
					              <c:choose>
					              	<c:when test="${refund.isPayFinal eq 1}"><!-- 已支付尾款，才能退 -->
					              		<li>
							              	<input type="hidden" id="refundAmount" name="refundAmount" value="${refund.finalRefundAmount}"/>
											<span><em>*</em>尾款金额：</span><em style="color: #e5004f;height: 30px;line-height: 30px;">￥&nbsp;<fmt:formatNumber value="${refund.finalRefundAmount + freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>（含运费￥${freightAmount}）</em>
										</li>
					              	</c:when>
					              	<c:otherwise>
					              		<input type="hidden" id="refundAmount" name="refundAmount" value="0"/>
					              	</c:otherwise>
					              </c:choose>
					              <li>
					              	<span><em>*</em>退款说明：</span>
					              	<textarea id="reasonInfo" name="reasonInfo" rows="3" cols="80"></textarea>
					              </li>
					              <li>
					              	<span style="line-height: 20px;">上传凭证1：</span>
					              	<input type="file" id="photoFile1" class="photoFile" name="photoFile1" onchange="checkImgFile(this);"/>
					              	<i>（图片不能超过5M）</i> 
					              	<input type="button" class="btn-g" id="addBtn" value="添加" onclick="addFile(this);"/>
					              </li>
					            </ul>
					          </div>
					        </div>
					         <br>
					         <div class="ste-but" style="margin-left:111px;">
					           <input type="submit" value="确认提交" class="btn-r big-btn submit" />&nbsp;
					           <input type="button" value="取消并返回" class="btn-g big-btn" style="padding:0;height:30px;" onclick="window.location.href='${contextPath}/s/ordersManage'"/>
					         </div>
				         </form:form>
				     </div>
				    <!-- 申请退款块 结束 -->
			     </div>
			     <!-----------right_con 结束-------------->
			    <div class="clear"></div>
			 </div>
		 	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
		</div>
	   <script charset="utf-8" src="<ls:templateResource item='/resources/common/js/compress.js'/>"></script>
	   <script type="text/javascript">
			var contextPath = '${contextPath}';

		</script>
		<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
		<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/refund/shopApplyOrderRefund.js'/>"></script>
	</body>
</html>
