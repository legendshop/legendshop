<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<div class="classify">
	<h1 style="background-color:">
		<span style="color:">商品分类</span>
	</h1>
	<div style="background-color:" class="classify_list">
		 <c:forEach items="${cateogryDtos}" var="category" varStatus="s">
			    <div class="level_li">
					<div style="background-color:" class="level_one">
						<i class="i_cut" id="i_one_${category.id}" child_show="true">
						</i>
						<span style="cursor:pointer" <c:if test="${not empty category.childrentDtos}"> child_show="true"</c:if> id="span_${category.id}">
						   <a href="<ls:url address='/store/list/${layoutParam.shopId}?fireCat=${category.id}'/>" >${category.name}</a>
						</span>
					</div>
				   <c:if test="${not empty category.childrentDtos}">
					    <div class="level_second">
						  <ul>
						     <c:forEach items="${category.childrentDtos}" var="chilren" varStatus="c">
							  <li> 
							  	  <i class="i_cut" id="i_two_${chilren.id}" child_show="true"></i>
						          <a href="<ls:url address='/store/list/${layoutParam.shopId}?secondCat=${chilren.id}'/>" >${chilren.name}</a></li>
								   <c:if test="${not empty chilren.childrentDtos}">
								     <div class="level_third">
									      <ul>
										      <c:forEach items="${chilren.childrentDtos}" var="chilren2" varStatus="c2">
										           <li><a href="<ls:url address='/store/list/${layoutParam.shopId}?thirdCat=${chilren2.id}'/>" >${chilren2.name}</a></li>
										      </c:forEach>
									      </ul>
								      </div>
			                       </c:if>					      
							 </c:forEach>
						   </ul>
					    </div>
				   </c:if>
			   </div>
		</c:forEach>
	</div>
</div>


