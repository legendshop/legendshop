 <%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
	<style type="text/css">
		html{font-size:13px;}
		table{    margin: 10px;
    border: 1px solid #ddd;}
		table tbody tr{height:70px;}
		table tbody tr td{border-bottom:1px solid #ddd;text-align: center;}
		table thead tr td{border-bottom:1px solid #ddd;text-align: center;padding:5px;background: #f9f9f9;}
	</style>
</head>
<body>
	 <form:form action="${contextPath}/s/coupon/loadSelectProd" id="form2" method="post">
	 	<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
	 	<div style="height:50px;background-color: #f9f9f9;border: 1px solid #ddd;font-size: 14px;margin: 10px;">
	 		<div style="padding-top:10px;padding-left:20px;">
			<input style="width:550px;border:1px solid #ddd;padding: 5px;"  type="text" name="name" id="name" maxlength="20" value="${prod.name}" size="20" placeholder="请输入商品名称" />
			<input type="submit" value="搜索" style="cursor:pointer; outline: none;background: #e5004f;color: #fff;padding: 5px 10px;border: 0px;"/>
			</div>
		</div>
	 </form:form>
	<table>
		<thead>
			<tr><td width="150">图片</td><td width="450">名称</td><td width="110">操作</td></tr>
		</thead>
		<tbody>
		<c:if test="${empty requestScope.list}">
			<tr><td colspan="3" style="text-align: center;">没有找到符合条件的商品</td></tr>
		</c:if>
     	<c:forEach items="${requestScope.list}" var="product">
          <tr>      
          	<td><img src="<ls:images item='${product.pic}' scale='3' />" ></td>
          	<td>${product.name}</td>
          	<td><button style="border:0px;"><a style="text-decoration: none;color:#333;padding: 5px;background: #999;color: #fff;" target="_blank" href="${contextPath}/views/${product.prodId}">查看</a></button>
          	&nbsp;<button style="padding: 5px;background: #e5004f;color: #fff;border: 0px;cursor: pointer;"  onclick="addProd('${product.prodId}','${product.name}','${product.cash}')">选择</button></td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
      <div align="center">
  		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
      </div>
      <script type="text/javascript">
	      function pager(curPageNO){
	          document.getElementById("curPageNO").value=curPageNO;
	          document.getElementById("form2").submit();
	      }
	      function addProd(prodid,name,price){
	 			var s = parent.addProdTo(prodid,name,price); 
	 			if(s){
	 				layer.alert("添加商品成功",{icon:1});
	 				var index = parent.layer.getFrameIndex(window.name);
	 				parent.layer.close(index);
	 			}
			}
      </script>
</body>
</html>