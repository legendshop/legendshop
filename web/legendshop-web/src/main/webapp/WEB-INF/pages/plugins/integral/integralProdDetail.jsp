<!doctype html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>积分商品详情-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link rel="stylesheet" type="text/css" media='screen' href="<ls:templateResource item='/resources/templets/css/productTab.css'/>" />
    <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/jqzoom/css/jquery.jqzoom.css'/>" type="text/css">
</head>
<body class="graybody">
<div id="doc">
	<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
   <div id="bd">
      <div class="seller-cru" style="width:1190px;margin: 20px auto 10px;">
        <p>
          您的位置&gt;
          <a href="${contextPath}">首页</a>
         <c:if test="${not empty integralProd.categoryName}"></span> &gt; <span><a href="<ls:url address="/integral/list?firstCid=${integralProd.firstCid}"/>">${integralProd.categoryName}</a></c:if>
		<c:if test="${not empty integralProd.twoCategoryName}"></span> &gt; <span><a href="<ls:url address="/integral/list?twoCid=${integralProd.twoCid}"/>">${integralProd.twoCategoryName}</a></c:if>
		<c:if test="${not empty integralProd.thirdCategoryName}"></span> &gt; <span><a href="<ls:url address="/integral/list?firstCid=${integralProd.thirdCid}"/>">${integralProd.thirdCategoryName}</a></c:if>
        </p>
      </div>   
             <div class="infomain yt-wrap">
               <div class="clearfix info_u_l">
                   <div class="tb-property">
                       <div class="tb-wrap">
                            <div class="tb-detail-hd">      
                                <h1><a  href="javascript:viod(0)">${integralProd.name}</a></h1>
                                <p class="newp"></p>
                            </div>
                            <div class="tm-price"><span ><s style="font-size:16px;color:#999;">价格:￥${integralProd.price}</s></span></div>
                            <div class="tm-price">积分:<span id="exchangeIntegral" style="color: #e5004f;font-size: 24px;font-family: Verdana;">${integralProd.exchangeIntegral}</span></div>
                            <div class="tb-meta">
                                 <c:if test="${not empty integralProd.prodNumber}">
                                <dl class="tb-prop tm-sale-prop clearfix ">
                                    <dt class="tb-metatit">商品编码</dt>
	                                    <dd>
		                                    <ul class="clearfix J_TSaleProp  ">
		                                         <li class="tb-selected"><span>${integralProd.prodNumber}</span></li>
		                                    </ul>
	                                    </dd>
                                </dl>
                                </c:if>
                                <c:if test="${not empty integralProd.prodStock && integralProd.prodStock>0}">
	                                <dl class="tb-prop tm-sale-prop clearfix ">
	                                    <dt class="tb-metatit">剩余库存</dt>
		                                    <dd>
			                                    <ul class="clearfix J_TSaleProp  ">
			                                         <li class="tb-selected"><span>${integralProd.prodStock}</span><span id="promotionPriceStr">(已兑换${integralProd.saleNum}件)</span></li>
			                                    </ul>
		                                    </dd>
	                                </dl>
                                </c:if>
                                
                                <c:if test="${not empty integralProd.isLimit && integralProd.isLimit==1}">
                                	<dl class="tb-prop tm-sale-prop clearfix ">
	                                    <dt class="tb-metatit">购买限制</dt>
		                                    <dd>
			                                    <ul class="clearfix J_TSaleProp  ">
			                                         <li class="tb-selected"><span>${integralProd.limitNum}</span></li>
			                                    </ul>
		                                    </dd>
	                                </dl>
                                </c:if>
                                 
                                 <dl class="tb-amount clearfix">
                                    <dt class="tb-metatit">数量</dt>
                                    <dd id="J_Amount">
                                        <span class="tb-amount-widget mui-amount-wrap">
                                            <input class="tb-text mui-amount-input" id="pamount" value="1" onblur="validateInput();" type="text">
                                            <span class="mui-amount-btn">
                                                <span class="mui-amount-increase" onclick="addPamount();">∧</span>
                                                <span class="mui-amount-decrease" onclick="reducePamount();">∨</span>
                                            </span>
                                            <span class="mui-amount-unit">件</span>
                                        </span>
                                        <c:if test="${not empty integralProd.prodStock && integralProd.prodStock>0}">
                                       		 <em id="J_EmStock" class="tb-hidden" style="display: inline;">库存${integralProd.prodStock}件<span id="promotionPriceStr">(已兑换${integralProd.saleNum}件)</span></em>
                                        </c:if>
                                       <c:if test="${not empty integralProd.isLimit && integralProd.isLimit==1}">
                                       	 	<span id="J_StockTips">（每人限购${integralProd.limitNum}件）</span>
                                        </c:if>
                                    </dd>
                                </dl>
                                 
                          <div class="tb-action clearfix">
                           <c:choose>
	                           <c:when test="${1 == integralProd.status && (not empty integralProd.prodStock && integralProd.prodStock>0)}">
                                    <div class="tb-btn-buy tb-btn-sku"><a id="J_LinkBuy" href="javascript:void(0);" onclick="buyNow();">立刻兑换</a></div>
	                           </c:when>
	                           <c:when test="${0== integralProd.status}">
	                           	<h2>此商品已下架</h2>
	                           </c:when>
	                           <c:when test="${ empty integralProd.prodStock || integralProd.prodStock<=0 }">
	                            <h2>已售罄</h2>
	                           </c:when>
                           </c:choose>
                                 </div>
                         
                            </div>
                       </div>
                   </div>   
                   
                   
                   <div class="tb-gallery">
                        <div class="tb-booth">
                            <a href="<ls:photo item='${integralProd.prodImage}'/>" class="jqzoom" rel="gal1" style="outline-style: none; text-decoration: none;">
                                <img id="imgView" src="<ls:photo item='${integralProd.prodImage}'/>" style="max-width: 450px; max-height: 450px; opacity: 1;">
                            </a>
                        </div>
                   </div>
               </div> 
               
            </div>
            <div class="yt-wrap info_d clearfix">
              <div class="left210_m">
                  <div class="sidebox">
                    <!--浏览历史 start-->
						<div id="exchangeList" name="visitedprod">
				
						</div>
                  </div>
               </div>
               <div class="right210_m">
                    <div class="detail" id="pagetab" id="productTab" name="productTab">
                                    <div class="pagetab2">
                                       <ul>           
                                         <li class="on"><span>商品详情</span></li>                  
                                       </ul>
                                    </div>
                            <div class="content" style="text-align:center;padding: 10px;">
                                       		${integralProd.prodDesc} 
                                       </div>   
                     </div>
               </div>
            </div>
   		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
   </div>
</div>
<script src="<ls:templateResource item='/resources/plugins/jqzoom/js/jquery.jqzoom-core.js'/>" type="text/javascript"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/productpics.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/integral/integralprod.js'/>"></script>
<script type="text/javascript">
	var contextPath = '${contextPath}';
	var firstCid="${integralProd.firstCid}";
	var twoCid="${integralProd.twoCid}";
	var thirdCid="${integralProd.thirdCid}";
	var limitNum="${empty integralProd.limitNum?0:integralProd.limitNum}";
	var prodStock="${empty integralProd.prodStock?0:integralProd.prodStock}";
	var loginUserName = '${loginUserName}';
	var prodId="${integralProd.id}";
	var saleNum="${integralProd.saleNum}";
</script>
</body>
</html>