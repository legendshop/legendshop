<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<input type="hidden" data-feed="err_nav_30" id="nav_30">
<div style="display:none" id="err_nav_30"></div>

<li>
	<div class="fieldset" id="itemtitle">
		<label class="caption required">商品标题：</label>
		<div class="fields-box">
			<input maxlength="50" name="name" value="${fn:escapeXml(productDto.name)}" class="text text-long" id="prodTitle">
		</div>
	</div>
</li>

<li>
	<div class="fieldset" id="subheading">
		<label class="caption" for="SubheadingID">商品卖点：</label>
		<div class="fields-box">
			<textarea cols="77" rows="3" maxlength="60"  name="brief" class="text-input" id="brief">${productDto.brief}</textarea>
		</div>
	</div>
</li>

<li style="height: 25px;"><label>售价：</label> <span> <em>*</em> <input type="text" id="cash" name="cash" value="<fmt:formatNumber value="${productDto.cash}" type="currency" pattern="0.00" />" maxlength="12" class="text text-short"
		style="width: 50px;">元 <span
			style="font-size: 12px;color: #BBB;">售价不能低于0.01元</span></span>
</li>

<li style="height: 25px;"><label>原价：</label> <span> <input type="text" id="price" name="price" value="<fmt:formatNumber value="${productDto.price}" type="currency" pattern="0.00" />" maxlength="12" class="text text-short" style="width: 50px;">元
</span>
</li>

<li class="fieldset"><label class="caption">库存预警值：</label>
	<div class="fields-box">
		<input type="text" id="stocksArm" name="stocksArm" value="${productDto.stocksArm==null?1:productDto.stocksArm}" maxlength="30" class="text text-short" style="margin-left: 0px;"> <span
			style="font-size: 12px;color: #BBB;">设置最低库存预警值,当库存低于预警值时商家中心商品列表页库存列红字提醒；默认没有库存预警为1</span>
	</div>
</li>
