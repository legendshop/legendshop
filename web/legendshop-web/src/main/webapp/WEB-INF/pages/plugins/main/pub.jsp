<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html>
<head>
    <title>公告列表-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
</head>
<body class="graybody">
<div id="bd">
    <%@include file='home/top.jsp' %>
    <div style="padding-top:10px;" class="yt-wrap">
        <div class="leftnews">
            <%@ include file="newsCategory.jsp" %>
        </div>
        <div class="right_newscon">
            <div class="index_left">
                <div class="news_wrap">

                    <div class="news_bor">
				         
                        <h3 class="tit">${pub.title}</h3>
                        <div class="nge"></div>
                        <p class="author"><fmt:formatDate value="${pub.recDate}" pattern="yyyy-MM-dd HH:MM"/></p>
                        <div class="newscontent">
                            ${pub.msg}
                        </div>

                        <div class="tishi">
                            <c:forEach items="${requestScope.pubList}" var="p" varStatus="status">
                                <p><a href="${contextPath}/pub/${p.id}">
                                    <c:if test="${p.id > pub.id}">上一篇:${p.title}</c:if>
                                    <c:if test="${p.id < pub.id}">下一篇：${p.title}</c:if>
                                </a>
                                </p>
                            </c:forEach>
                        </div>
                        </br>
                        <div class="pinglun">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>