<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
	<title>商品快照-${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<script type="text/javascript" src="/resources/templets/js/snapshot.js"></script>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="home/top.jsp"%>
		<div id="bd">
			<div class="infomain yt-wrap">

				<!-- 店铺信息 -->
				 <div class="info_u_r">
					<div class="qjdlogo">
						<a href="<ls:url address='/store/${shopDetail.shopId}'/>" style="text-align: center;"> <c:if test="${not empty shopDetail.shopPic}">
								<img width="190" height="63" style="border: 0;vertical-align: middle;" src="<ls:photo item='${shopDetail.shopPic}'/>">
							</c:if></a>
					</div>
					<div class="shopInfo">
						<a class="shopName" href="<ls:url address='/store/${shopDetail.shopId}'/>" target="_blank" title="${shopDetail.siteName}">${shopDetail.siteName}</a><c:if test="${shopDetail.shopType eq 2}">
						<span class="ziying">平台自营</span></c:if>
					</div>
					<ul>
						
					<li><span style="float:left;">综合评分：</span> <span class="scores_scroll"> <span class="scroll_gray"></span> <span class="scroll_red" style="width: 20%"></span> </span><font id="shopScoreNum" class="hide">${sum}</font></li>
                    <li><span style="float:left;">描述</span><span style="margin-left:50px">服务</span><span style="float:right;">物流</span></li>
                    <li><span style="float:left;">${prodAvg==null?'0.0':prodAvg}</span><span style="margin-left:60px">${shopAvg==null?'0.0':shopAvg}</span><span style="float:right;">${dvyAvg==null?'0.0':dvyAvg}</span></li>
<%--						<c:set var="contaceQQ" value="${fn:split(shopDetail.contactQQ, ',')[0]}"></c:set>--%>
<%--						<li>商城客服：<a href="http://wpa.qq.com/msgrd?v=3&uin=${contaceQQ}&site=qq&menu=yes" target="_blank">点击咨询</a>--%>
						<li>商城客服：<%-- <a href="http://wpa.qq.com/msgrd?v=3&uin=${contaceQQ}&site=qq&menu=yes" target="_blank">点击咨询</a></li> --%>
							<input type="hidden" id="user_name" value="${user_name}"/>
							<input type="hidden" id="user_pass" value="${user_pass}"/>
							<input type="hidden" id="shopId" value="${shopDetail.shopId}"/>
							<input type="hidden" id="skuId" value="${productSnapshot.skuId}"/>
							<a href="javascript:void(0)" id="loginIm" onclick="loginIm();">联系卖家</a>
						</li>

					</ul>
					<div class="col-shop-btns">
						<a href="<ls:url address='/store/${shopDetail.shopId}'/>" target="_blank" style="font-size:12px;color:#ffffff;width:80px;height:30px;background:#333333;border:1px solid #333;">进入店铺</a>
						<a class="col-btn" href="javascript:void(0);" onclick="collectShop('${shopDetail.shopId}')">收藏店铺</a>
					</div>
				</div>


				<div class="clearfix info_u_l">
					<div class="tb-property">
						<div class="tb-wrap">
							<div class="tb-detail-hd">
								<h1>
									<a href="javascript:void(0);" id="prodName"> ${productSnapshot.name}</a>
								</h1>
								<p class="newp">${productSnapshot.brief}</p>
							</div>
							<div class="tm-price" >
								￥<span id="prodCash" style="font-size:24px;color:#e5004f;">
								<fmt:formatNumber pattern="0.00" maxFractionDigits="2" groupingUsed="false" value="${productSnapshot.cash}" />
								</span>
							</div>

							<div class="tb-meta snap-meta">
							   <c:if test="${not empty productSnapshot.carriage}">
									<dl>
										<dt class="tb-metatit">运费</dt>
										<dd>
											￥ ${productSnapshot.carriage }
	
										</dd>
									</dl>
								</c:if>
								<c:if test="${not empty attrList}">
				                	<c:forEach items="${attrList}" var="attr">
				                		<dl>
						                    <dt class="tb-metatit" style="width: auto;">${attr.key} ： ${attr.value}</dt>
						                    <!-- <dd></dd> -->
						                </dl>
				                	</c:forEach>
				                </c:if>
								<dl>
									<dt class="tb-metatit">购买数量：</dt>
									<dd>
										${productSnapshot.prodCount }件
									</dd>
								</dl>
								<div style="padding-left:15px;">
								                您现在查看的是<b>商品快照</b><br>
					                <c:choose>
					                	<c:when test="${not empty isProdOnline}"><div  style="margin-top:15px;"><a style="background-color: #e5004f;border: 1px solid #e5004f;color: #fff;padding:5px 10px;" href="${contextPath}/views/${productSnapshot.prodId}">查看商品最新信息</a></div></c:when>
					                	<c:otherwise>但目前暂时不提供查看该商品的信息。</c:otherwise>
					                </c:choose>
				                </div>

							</div>
						</div>
					</div>

					<!--产品图片-->
					<div class="preview tb-gallery">
						<div class="tb-booth">
							    <a id="aView" title="${fn:escapeXml(productSnapshot.name)}" >
					   			<img id="imgView" style='max-width:450px;max-height:450px;' src="<ls:photo item='${productSnapshot.pic}' />" >
					   		</a>
						</div>
					</div>
					<!--产品图片end-->

				</div>
			</div>
			<div class="yt-wrap info_d clearfix">
				<div class="detail">
						<div class="pagetab2" id="productTab">
							<ul>
								<li class="on" id="prodContent"><span>参数</span></li>
							</ul>
						</div>
						
						<c:if test="${not empty paramList}">
							<div class="tabcon" id="prodSpecPane" style="background:#f6f6f6; zoom:1;">
						
							<table cellpadding="0" cellspacing="0" style="width:100%;">
								  <tbody>
							<c:forEach items="${paramList}" var="p">
								<tr><th style="width:150px;background: #f9f9f9;padding-right: 10px;">${p.key}</th>
								<td style="text-indent: 10px;">${p.value}</td></tr>
							</c:forEach>
							</tbody></table>
							</div>
						</c:if>
<%--					<c:choose>   暂时不支持分组
						<c:when test="${not empty paramList}">
							<table cellpadding="0" cellspacing="0">
								<tbody>
								<c:forEach items="${paramList}" var="paramGroup">
									<tr><th class="guige" colspan="2" style="background: #f9f9f9;">${paramGroup.groupName}</th></tr>
									<c:forEach items="${paramGroup.params}" var="p">
										<tr><th style="width:150px;background: #f9f9f9;padding-right: 10px;">${p.paramName}</th><td style="text-indent: 10px;">${p.paramValueName}</td></tr>
									</c:forEach>
								</c:forEach>
								</tbody></table>
						</c:when>
						<c:otherwise>
							<div>此商品没有参数</div>
						</c:otherwise>
					</c:choose>--%>

					<div style="text-align:center;" class="content" id="prodContentPane">${productSnapshot.content}</div>
				</div>
			</div>
		</div>
		<%@ include file="home/bottom.jsp"%>
	</div>
</body>

</html>