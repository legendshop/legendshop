<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@include file='/WEB-INF/pages/common/layer.jsp' %>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>登录绑定 - ${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/accountBinding${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
<div id="bd">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>

    <!----两栏---->
    <div class=" yt-wrap" style="padding-top:10px;">
        <%@include file='usercenterLeft.jsp' %>

        <!-----------right_con-------------->
        <div class="right_con" id="content">

            <div class="right">
                <div class="o-mt"><h2>登陆绑定</h2></div>
                <div class="pagetab2">
	                <ul>
	                    <li class="on"><span>登陆绑定</span></li>
	                </ul>
            	</div>
                <div id="addressList" class="o-m">

                    <c:forEach items="${requestScope.passportDtoList}" var="passportDto" varStatus="status">
 
                          <div class="m m4 <c:if test="${passportDto.type eq 'weixin' && not passportDto.binded }">hide</c:if>" id="addresssDiv" style="margin: 10px 0;">

                            <div class="mt" style="background: #f9f9f9;">
                                <h3>
                                    <span class="ftx-03" style="color: #333;">${passportDto.desc}</span>
                                </h3>
                            </div>
                            <div class="mc">
                                <div class="items new-items">
                                    <div class="item-lcol" style="width: 200px;height: 50px;">
                                        <img alt="图片" src="${contextPath}${passportDto.pic}" id="infoimg">
                                    </div>
                                    <div class="item-lcol" style="width: 400px;height: 50px;">
                                        <c:choose>
                                            <c:when test="${passportDto.binded}">
                                                <span class="bindspan">您已成功绑定  昵称：(${passportDto.nickName})</span>
                                                <div>
                                                    <a class="ncbtn" style="color: #005ea7;margin-left:20px;" onclick="unbinding('${passportDto.type}');" href="javascript:void(0)">解绑</a>
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="bindspan">账户关系绑定</span>
                                                <div><span class="bindspan">账户关系绑定 绑定成功后可使用${passportDto.desc}直接登录系统</span></div>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>

                                </div>

                                <div class="item-rcol" style="width: 300px;height: 50px;">
                                    <div class="ac">
                                        <c:choose>
                                            <c:when test="${passportDto.binded}">
                                                <img src="${contextPath}/resources/common/images/thirdlogin/binded.jpg" style="float: left;">
												<span class="bound_acc"> 
													   <b>账户授权</b>
												       <p>账户关系绑定</p>
												       <p>可使用${passportDto.desc}账号直接登录系统</p>
												</span>
                                            </c:when>
                                            <c:otherwise>
                                               <%-- 无需在这个页面绑定第三方登录，会和登录页面形成冲突
                                                <c:choose>
                                                    <c:when test="${passportDto.type eq 'qq'}">
                                                        <a class="ncbtn" href="javascript:void(0)" onclick="window.location='https://graph.qq.com/oauth2.0/authorize?client_id=${passportDto.appId}&amp;response_type=token&amp;scope=all&amp;redirect_uri=${passportDto.redirectUrl}'">立即绑定</a>
                                                    </c:when>
                                                    <c:when test="${passportDto.type eq 'weibo'}">
                                                        <a class="ncbtn" href="javascript:void(0)" onclick="window.location='<%=oauth.authorize("code","","") %>'">立即绑定</a>
                                                    </c:when>
                                                    <c:otherwise>

                                                    </c:otherwise>
                                                </c:choose>
                                                --%>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>

                </div>
                <div class="m m7">
					<div class="mc"> 
                                            请在登录页进行绑定，此页面可以做解绑动作。
					</div>
				</div>
            </div>

        </div>

        <div class="clear"></div>
    </div>
    <!----两栏end---->

    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<!--bd end-->
<script type="text/javascript">

    function unbinding(type) {
    	
    	layer.confirm("确定要解除绑定吗？", {
   		 icon: 3
   	     ,btn: ['确定','关闭'] //按钮
   	   }, function(){
   		  jQuery.ajax({
              url: "${contextPath}/p/unbinding",
              data: {"type": type},
              type: 'post',
              async: false, //默认为true 异步   
              dataType: 'json',
              success: function (result) {
                  $("#accountBinding").click();
              }
          });
   		   
   	   });
    }
    $(document).ready(function () {
        userCenter.changeSubTab("accountBinding");
    });
</script>
</body>
</html> 