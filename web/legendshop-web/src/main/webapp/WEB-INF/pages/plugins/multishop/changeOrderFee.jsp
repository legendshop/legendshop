<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
<title>调整订单金额</title>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script src="${contextPath}/resources/common/js/jquery.validate.js"
	type="text/javascript"></script>
<link type="text/css"
	href="<ls:templateResource item='/resources/templets/css/multishop/deliverAddressOverlay.css'/>"
	rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller${_style_}.css'/>" rel="stylesheet"/>
<style>
	.form label, .form input, .fl input {
		height: 27px;
		width: 225px;
	}
	.mc{
		text-align:center;
	}
	a{color:#fff;}
</style>
</head>
<body>
	<c:choose>
		<c:when test="${subDto.subType eq 'PRE_SELL'}"> <!-- 预售订单调整运费 -->
				<div class="m" id="edit-cont">
					<div class="mc">
						<div class="form">
							<div class="item">
								<span class="label"><em>*</em>用户：</span>
								<div class="fl">
									<input readonly="readonly" type="text" name="userName" id="userName" size="30" value="${subDto.userName}">
								</div>
								<div class="clr"></div>
								</span>
							</div>
							<div class="item">
								<span class="label"><em>*</em>订单号：</span>
								<div class="fl">
									<input readonly="readonly" type="text" name="subNumber" id="subNumber" size="30" value="${subDto.subNum}">
								</div>
								</span>
							</div>
							<div class="clr"></div>
			
							<div class="item">
								<span class="label"><em>*</em>定金金额：</span>
								<div class="fl">
									<input type="text" name="preDepositPrice" placeholder="小数位不能超过两位" maxlength="8"
										value="<fmt:formatNumber type="currency" value="${subDto.preDepositPrice}" pattern="#0.00"/>"
										id="preDepositPrice" size="30">
								</div>
								<div class="clr"></div>
							</div>
							<c:if test="${subDto.payPctType eq 1 }">
								<div class="item">
									<span class="label"><em>*</em>尾款金额：</span>
									<div class="fl">
										<input type="text" name="finalPrice" placeholder="小数位不能超过两位" maxlength="8"
											value="<fmt:formatNumber type="currency" value="${subDto.finalPrice}" pattern="#0.00"/>"
											id="finalPrice" size="30">
									</div>
									<div class="clr"></div>
								</div>
							</c:if>
			
							<div class="item">
								<span class="label" style="float: left;"><em>*</em>订单金额：</span>
								<div class="fl" style="float: left;">
									<span style="color: #F00; float: left; font-weight: bold;" id="preTotalPriceText"> 
										￥<fmt:formatNumber pattern="0.00" value="${subDto.actualTotal}" />
									</span> 
									<input type="hidden" name="totalPrice" id="preTotalPrice" value="${subDto.actualTotal}">
								</div>
								<div class="clr"></div>
							</div>
			
							<div class="btn-r">
								<a href="javascript:void(0);" id="orderFee" onclick="changePreOrderFee();" class="ncbtn">保存更改</a>
							</div>
						</div>
					</div>
				</div>
		</c:when>
		<c:otherwise> <!-- 普通订单调整运费 -->
			<div class="m" id="edit-cont">
				<div class="mc">
					<div class="form">
						<div class="item">
							<span class="label"><em>*</em>用户：</span>
							<div class="fl">
								<input readonly="readonly" type="text" name="userName" id="userName" size="30" value="${subDto.userName}">
							</div>
							<div class="clr"></div>
							</span>
						</div>
						<div class="item">
							<span class="label"><em>*</em>订单号：</span>
							<div class="fl">
								<input readonly="readonly" type="text" name="subNumber" id="subNumber" size="30" value="${subDto.subNum}">
							</div>
							</span>
						</div>
						<div class="clr"></div>
		
						<div class="item">
							<span class="label"><em>*</em>商品总价：</span>
							<div class="fl">
								<input type="text" name="order_fee_amount" placeholder="小数位不能超过两位" maxlength="8"
									value="<fmt:formatNumber type="currency" value="${subDto.actualTotal- subDto.freightAmount}" pattern="#0.00"/>"
									id="order_fee_amount" size="30">
							</div>
							<div class="clr"></div>
						</div>
						<div class="item">
							<span class="label"><em>*</em>配送金额：</span>
							<div class="fl">
								<input type="text" name="order_fee_ship_price" placeholder="小数位不能超过两位" maxlength="8"
									value="<fmt:formatNumber type="currency" value="${subDto.freightAmount}" pattern="#0.00"/>"
									id="order_fee_ship_price" size="30">
							</div>
							<div class="clr"></div>
						</div>
		
						<div class="item">
							<span class="label" style="float: left;"><em>*</em>订单金额：</span>
							<div class="fl" style="float: left;">
								<span style="color: #F00; float: left; font-weight: bold;" id="totalPriceText"> 
									<fmt:formatNumber pattern="0.00" value="${subDto.actualTotal}" />
								</span> 
								<input type="hidden" name="totalPrice" id="totalPrice" value="">
							</div>
							<div class="clr"></div>
						</div>
		
						<div class="btn-r">
							<a href="javascript:void(0);" id="orderFee" onclick="changeOrderFee();" class="ncbtn">保存更改</a>
						</div>
					</div>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</body>
<script type="text/javascript">
	var contextPath = "${contextPath}";
	var payPctType = '${payPctType}';
</script>
<script src="<ls:templateResource item='/resources/templets/js/changeOrderFee.js'/>" type="text/javascript"></script>
</html>