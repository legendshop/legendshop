<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@ include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title><c:choose>
        <c:when test="${not empty prod.metaTitle}">${prod.metaTitle}-${systemConfig.shopName}</c:when>
        <c:otherwise>${prod.name}-${systemConfig.shopName}</c:otherwise>
    </c:choose>
    </title>
    <c:choose>
        <c:when test="${not empty prod.keyWord}">
            <meta name="keywords" content="${prod.keyWord}"/>
        </c:when>
        <c:otherwise>
            <meta name="keywords" content="${systemConfig.keywords}"/>
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${not empty prod.metaDesc}">
            <meta name="description" content="${prod.metaDesc}"/>
        </c:when>
        <c:otherwise>
            <meta name="description" content="${systemConfig.description}"/>
        </c:otherwise>
    </c:choose>
    <style>
        .det-adv {
            padding: 6px 0;
        }

        .det-adv span {
            margin-left: 90px;
            color: #e5004f;
        }

    /*     #prodContentPane img {
            width: 10%;
        } */

        .coupon {
            position: relative;
            height: 16px;
            line-height: 15px;
            text-align: center;
            border-top: 1px solid #df3033;
            border-bottom: 1px solid #df3033;
            background: #ffdedf;
            white-space: nowrap;
            margin-right: 8px;
            cursor: pointer;
        }

        .coupon span {
            padding: 0 10px;
            color: #df3033;
        }

        .couponcenter {

            white-space: nowrap;
            margin-right: 8px;
            cursor: pointer;
        }

        .buynow-r {
            background-color: #FEF2F6 !important;
            font-size: 16px !important;
            color: #e5004f !important;
            width: 140px !important;
            height: 40px !important;
            cursor: pointer;
        }

        .buynow-n {
            background-color: #e5004f !important;
            font-size: 16px !important;
            color: #ffffff !important;
            width: 140px !important;
            height: 40px !important;
        }

        .buynow-g {
            font-size: 16px !important;
            color: #666666 !important;
            width: 90px !important;
            height: 40px !important;
        }

        .shopDetailSpan {
            text-align: center;
            width: 33.3%;
            display: inline-block;
        }

        .shopDetailNum {
            color: #e5004f;
            text-align: center;
            width: 33.3%;
            display: inline-block;
        }

        .bdshare-button-style0-16 a, .bdshare-button-style0-16 .bds_more{
            background-image: none !important;
            margin:0 !important;
            padding-left: 40px !important;
        }

    </style>
</head>
<%@ include file="included/rightSuspensionFrame.jsp" %>
<body class="graybody">
<div id="doc">
    <%@ include file="home/top.jsp" %>
    <input id="currSkuId" value="${prod.skuId}" type="hidden"/>
    <input id="selCityId" value="${cityId}" type="hidden"/>
    <input id="selProvinceId" value="${provinceId}" type="hidden"/>
    <input id="skuType" value="${prod.skuType}" type="hidden"/>
    <div id="bd">
        <!--crumb-->
        <div id="J_crumbs" class="crumb yt-wrap">
            <div class="crumbCon">
                <div class="crumbSlide">
                    <div class="crumbs-nav-item one-level">
                        <a class="crumbs-link" href="<ls:url address="/"/>"> 首页 </a><i class="crumbs-arrow">&gt;</i>
                    </div>

                    <c:choose>
                        <c:when test="${not empty param.keyword}">
                            <div class="crumbs-nav-item one-level">
                                <a class="crumbs-link" href="javascript:void(0);"><c:out value="${param.keyword}"></c:out>
                                </a> <i class="crumbs-arrow">&gt;</i>
                            </div>
                        </c:when>
                        <c:when test="${not empty treeNodes && (fn:length(treeNodes) > 1)}">
                            <c:forEach items="${treeNodes}" var="node" varStatus="n">
                                <div class="crumbs-nav-item">
                                    <c:choose>
                                        <c:when test="${!n.last}">
                                            <div class="menu-drop">
                                                <!-- <div class="trigger"> -->
                                                <span class="curr"><a title="${node.nodeName}" href="<ls:url address="/list?categoryId=${node.selfId}"/>">${node.nodeName}</a></span><!-- <i class="menu-drop-arrow"></i> -->
                                                <!-- </div> -->
                                                    <%-- <div class="menu-drop-main">
                                                        <c:if test="${not empty node.compatriots}">
                                                            <ul class="menu-drop-list">
                                                                <c:forEach items="${node.compatriots}" var="compatriot" varStatus="c">
                                                                    <li><a title="${compatriot.nodeName}" href="<ls:url address="/list?categoryId=${compatriot.selfId}"/>">${compatriot.nodeName}</a>
                                                                    </li>
                                                                </c:forEach>
                                                            </ul>
                                                        </c:if>
                                                    </div> --%>
                                            </div>
                                        </c:when>
                                        <c:when test="${n.last}">
                                            <a class="crumbs-link" href="javascript:void(0);">${node.nodeName}</a>
                                        </c:when>
                                    </c:choose>
                                    <c:if test="${!n.last}">
                                        <i class="crumbs-arrow">&gt;</i>
                                    </c:if>
                                </div>
                            </c:forEach>
                        </c:when>
                        <c:when test="${not empty treeNodes && (fn:length(treeNodes) <= 1)}">
                            <c:forEach items="${treeNodes}" var="node" varStatus="n">
                                <div class="crumbs-nav-item one-level">
                                    <a class="crumbs-link" href="javascript:void(0);"><c:out value="${node.nodeName}"></c:out>
                                    </a>
                                </div>
                            </c:forEach>
                        </c:when>
                        <c:otherwise></c:otherwise>
                    </c:choose>

                    <c:if test="${not empty  facetProxy.selections}">
                        <div class="crumbs-nav-item">
                            <i class="crumbs-arrow">&gt;</i>
                        </div>
                        <c:forEach items="${facetProxy.selections}" var="selections">
                            <div class="crumbs-nav-item crumbAttr">
                                <a href="javascript:void(0);">${fn:substring(selections.title,0,6)}:${selections.name}</a> <a href="javascript:void(0);" onclick="javascript:removeFacetData('${selections.url}');"
                                                                                                                              class="crumbDelete">X</a>
                            </div>
                        </c:forEach>
                    </c:if>
                </div>
            </div>
        </div>
        <!--crumb end-->

        <!--主要信息-->
        <div class="infomain yt-wrap">

            <!-- 店铺信息 -->
            <div class="info_u_r" <c:if test="${empty shopDetail.shopPic}">style="padding: 0px 7px;border:1px solid #eee;width: 190px;height: 249px;box-sizing: border-box;"</c:if> <c:if test="${!empty shopDetail.shopPic}">style="padding: 0px 7px;border:1px solid #eee;width: 190px;height: 323px;box-sizing: border-box;"</c:if>>
                <div class="qjdlogo" style="border-bottom:0px"  <c:if test="${empty shopDetail.shopPic}">style="border-bottom:0px;padding:0px"</c:if>
                    <a href="<ls:url address='/store/${shopDetail.shopId}'/>" style="text-align: center;"> <c:if test="${not empty shopDetail.shopPic}">
                        <img style="border: 0;vertical-align: middle; width: 180px;max-height: 75px" src="<ls:photo item='${shopDetail.shopPic}'/>">
                    </c:if></a>
                </div>
                <div class="shopInfo" style="padding:0px">
                    <a class="shopName" href="<ls:url address='/store/${shopDetail.shopId}'/>" target="_blank" title="${shopDetail.siteName}">${shopDetail.siteName}</a><c:choose>
                    <c:when test="${shopDetail.shopType == 0}"><strong style="float: right">专营店</strong></c:when>
                    <c:when test="${shopDetail.shopType == 1}"><strong style="float: right">旗舰店</strong></c:when>
                    <c:when test="${shopDetail.shopType == 2}"><span style="float: right" class="ziying">平台自营</span></c:when>
                </c:choose>
                </div>
                <ul>
                    <li><span style="float:left;">综合评分：</span> <span class="scores_scroll"> <span class="scroll_gray"></span> <span class="scroll_red" style="width: 20%"></span> </span><font id="shopScoreNum" class="hide">${sum}</font></li>
                    <li style="border-bottom: 0px;height:25px"><span class="shopDetailSpan">描述</span><span class="shopDetailSpan">服务</span><span class="shopDetailSpan">物流</span></li>
                     <li><span class="shopDetailNum">${prodAvg==null?'0.0':prodAvg}</span><span class="shopDetailNum">${shopAvg==null?'0.0':shopAvg}</span><span class="shopDetailNum">${dvyAvg==null?'0.0':dvyAvg}</span></li>
                    <c:set var="contaceQQ" value="${fn:split(shopDetail.contactQQ, ',')[0]}"></c:set>
                    <li>商城客服：<%-- <a href="http://wpa.qq.com/msgrd?v=3&uin=${contaceQQ}&site=qq&menu=yes" target="_blank">点击咨询</a> --%>
                        <input type="hidden" id="user_name" value="${user_name}"/>
                        <input type="hidden" id="user_pass" value="${user_pass}"/>
                        <input type="hidden" id="shopId" value="${shopDetail.shopId}"/>
                        <a href="javascript:void(0)" id="loginIm">联系卖家</a>
                    </li>
                </ul>
                <div class="col-shop-btns">
                    <a href="<ls:url address='/store/${shopDetail.shopId}'/>" target="_blank" style="font-size:12px;color:#ffffff;width:80px;height:30px;background:#333333;border:1px solid #333;">进入店铺</a>
                    <a class="col-btn" href="javascript:void(0);" onclick="collectShop('${shopDetail.shopId}')" style="font-size:12px;color:#333333;width:80px;height:30px;background:#f5f5f5;border:1px solid #cccccc;">收藏店铺</a>
                </div>
            </div>


            <div class="clearfix info_u_l">
                <div class="tb-property">
                    <div class="tb-wrap">
                        <div class="tb-detail-hd">
                            <h1>
                                <a href="javascript:void(0);" id="prodName" style="word-break: break-all; max-width:480px"> ${prod.name}</a>
                            </h1>
                            <p class="newp">${prod.brief}</p>
                        </div>
                        <div class="tm-price">
                            ￥<span id="prodCash" style="font-size:24px;color:#e5004f;"></span>
                            <c:if test="${not empty prod.price and prod.price ne 0}">
                                <span style="text-decoration: line-through;color: #999;font-size:12px; margin-left: -10px;">￥${prod.price}</span>
                            </c:if>
                        </div>
                        <c:if test="${not empty mailfeeStr}">
                            <div style="color: #ed004f; margin-left: 16px; font-weight: bold; height: 30px; line-height: 30px;">[${mailfeeStr }]</div>
                        </c:if>
                        <!-- 营销活动 -->
                        <div class="spe-dic" id="rules"></div>
                        <div class="spe-dic" id="skuActivity" style="margin-left: 0px;">
                      
                        </div>
                        <div class="tb-meta">
                            <c:if test="${not empty couponList}">
                                <dl id="J_RSPostageCont" class="store-hide tm-delivery-panel clearfix">
                                    <dt class="tb-metatit">优惠券</dt>
                                    <dd style="line-height: 36px;">
                                        <c:forEach items="${couponList }" var="coupon">
										<span class="coupon">
											<span onclick="getCoupon(${coupon.couponId})" title="领取">满${coupon.fullPrice}减${coupon.offPrice}</span>
										</span>
                                        </c:forEach>
                                        <c:if test="${fn:length(couponList) > 3}">
                                            <span class="couponcenter"><a href="${contextPath}/couponCenter" target="_blank" style="font-family: SimSun; color: #999;">更多>></a></span>
                                        </c:if>
                                    </dd>
                                </dl>
                            </c:if>

                            <c:if test="${not empty prod.brandName}">
                                <dl id="J_RSPostageCont" class="tm-delivery-panel clearfix">
                                    <dt class="tb-metatit">品牌</dt>
                                    <dd style="line-height: 36px;"><a href="${contextPath }/list?prop=brandId:${prod.brandId}">${prod.brandName}</a></dd>
                                </dl>
                            </c:if>

                            <!-- 门店 -->
                            
							<c:if test="${1 == prod.status && empty invalid && shopDetail.status eq 1}">
	                            <ls:plugin pluginId="store">
	                                <dl id="storeInfo" class="ncs-chain" style="display:none;"></dl>
	                            </ls:plugin>
                            </c:if>

                            <c:choose>
                                <c:when test="${1 == prod.status && empty invalid}">

                                    <!-- 商品属性 -->
                                    <div class="prop_main">
                                        <c:forEach items="${prod.prodPropDtoList}" var="prop" varStatus="psts">
                                            <dl class="clearfix prop_dl">
                                                <dt class="tb-metatit">${prop.propName}</dt>
                                                <dd>
                                                    <ul prop="${prop.propName}" class="clearfix prop_ul" id="prop_${psts.index}" index="${psts.index}">
                                                        <c:forEach items="${prop.productPropertyValueList}" var="propVal">
                                                            <c:choose>
                                                                <c:when test="${propVal.isSelected}">
                                                                    <li data-value="${prop.propId}:${propVal.valueId}" valId="${propVal.valueId}" class="li-show li-selected">
                                                                        <a>
                                                                        <c:if test="${not empty propVal.pic}">
                                                                            <img src="<ls:images item='${propVal.pic }' scale='3'/>">
                                                                        </c:if> <span>${propVal.name}</span> </a> <i></i>
                                                                    </li>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <li data-value="${prop.propId}:${propVal.valueId}" valId="${propVal.valueId}" class="li-show"><a> <c:if test="${not empty propVal.pic}">
                                                                        <img src="<ls:images item='${propVal.pic }' scale='3'/>">
                                                                    </c:if> <span>${propVal.name}</span> </a>
                                                                    </li>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:forEach>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </c:forEach>
                                    </div>
                                    <!-- 商品属性 end-->
                                    
                                    <!-- 配送方式 start -->
                                    <dl id="deliveryType" class="tm-delivery-panel clearfix" style="display: none">
                                        <dt class="tb-metatit">配送方式：</dt>
                                        <dd>
                                        	<div class="fl phoneContainer2">
												<label class="radio-wrapper radio-wrapper-checked">
													<span class="radio-item">
														<input type="radio" class="radio-input" value="EXPRESS" name="deliveryType" checked="checked"><label>快递配送</label>&nbsp;
														<span class="radio-inner"></span>
													</span>
												</label>
												<label class="radio-wrapper">
													<span class="radio-item">
														<input type="radio" class="radio-input" value="PICKUP" name="deliveryType"><label>门店自提</label>&nbsp;
														<span class="radio-inner"></span>
													</span>
												</label>
											</div>
                                        </dd>
                                    </dl>
                                    <!-- 配送方式 end -->
                                    
                                    <!-- 运费 -->
                                    <dl id="J_RSPostageCont" class="store-hide tm-delivery-panel clearfix">
                                        <dt class="tb-metatit">运费</dt>
                                        <dd>
                                            <div class="addrselector" id="storeSelector">
                                                <div class="text">
                                                    <div title="${ipProvince}${ipCity}${ipArea}">${ipProvince}${ipCity}${ipArea}</div>
                                                    <b></b>
                                                </div>
                                                <div class="content">
                                                    <div id="JD-stock" class="m JD-stock" data-widget="tabs">
                                                        <div class="mt">
                                                            <ul class="tab" id="stockTab">
                                                                <li data-index="0" class="" onclick="selectProvince(this);" provinceid="${provinceId}"><a class="" href="javascript:void(0);"><em>${ipProvince}</em><i></i> </a></li>
                                                                <li data-index="1" class="" onclick="selectCity(this);"><a class="" href="javascript:void(0);"><em>${ipCity}</em><i></i> </a></li>
                                                                <li data-index="2" class="curr"><a class="hover" href="javascript:void(0);"><em>${ipArea}</em><i></i> </a></li>
                                                            </ul>
                                                            <div class="stock-line"></div>
                                                        </div>
                                                        <div id="stock_province_item" data-widget="tab-content" data-area="0" class="mc" style="display: none;">
                                                            <ul class="area-list">
                                                                <c:forEach items="${provinces}" var="province">
                                                                    <li><a data-value="${province.id}" href="#none">${province.province}</a></li>
                                                                </c:forEach>
                                                            </ul>
                                                        </div>
                                                        <div id="stock_city_item" data-widget="tab-content" data-area="1" class="mc" style="display: none;">
                                                            <ul class="area-list">
                                                                <c:forEach items="${citys}" var="city">
                                                                    <li><a data-value="${city.id}" href="#none">${city.city}</a></li>
                                                                </c:forEach>
                                                            </ul>
                                                        </div>
                                                        <div id="stock_area_item" data-widget="tab-content" data-area="2" class="mc" style="display: block;">
                                                            <ul class="area-list">
                                                                <c:forEach items="${areas}" var="area">
                                                                    <li><a data-value="${area.id}" href="#none">${area.area}</a></li>
                                                                </c:forEach>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <span class="clr"></span>
                                                </div>
                                                <div onclick="javascript:closeStoreSelector();" class="close"></div>
                                            </div>
                                            <div class="frei_div"><span id="stocksText"></span><span id="freightText"></span></div>

                                        </dd>
                                    </dl>
                                    <!-- 运费 -->
                                    
                                    <!-- 选择门店  -->
                                    <dl id="selectShopStroe" class="tm-delivery-panel clearfix" style="display: none">
                                        <dt class="tb-metatit">选择门店</dt>
                                        <dd>
											<div class="fl phoneContainer2">
												<a href='javascript:void(0);' id="selectStore" onclick='shopStoreServer();' style="text-decoration:underline;">请选择门店</a>
												<input type="hidden" id="selectedStoreId" name="selectedStoreId" value=""/>
											</div>
                                        </dd>
                                    </dl>
                                    <!-- 选择门店  -->
                                    
                                </c:when>
                                <c:when test="${not empty invalid}">
                                    <dl>
                                        <h2 style="margin-left:15px;">即将上线</h2>
                                    </dl>
                                </c:when>
                                <c:when test="${3==prod.status}">
                                    <dl>
                                        <h2 style="margin-left:15px;">此商品待审核</h2>
                                    </dl>
                                </c:when>
                                <c:otherwise>
                                    <dl>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <h1 style="font-size:18px;margin-left:15px;">此商品已下架</h1>
                                    </dl>
                                </c:otherwise>
                            </c:choose>

                            <%--<div class="tb-prop tb-clear">
                        <span class="tb-metatit">服务</span>按时发货 &nbsp;&nbsp; 极速退款 &nbsp;&nbsp; 七天无理由退换
                    </div> --%>

                            <c:if test="${1 == prod.status && empty invalid}">
                                <dl class="tb-amount clearfix">
                                    <dt class="tb-metatit">数量</dt>
                                    <dd id="J_Amount">
											<span class="tb-amount-widget mui-amount-wrap"> 
											   <input type="tel" title="请输入购买量" id="pamount" onblur="validateInput();" maxlength="8" value="1" class="tb-text mui-amount-input">
												<span class="mui-amount-btn"> <span class="mui-amount-increase" onclick="addPamount();">∧</span> <span class="mui-amount-decrease" onclick="reducePamount();">∨</span> </span> <span
                                                    class="mui-amount-unit">件</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="skuStock"></span> </span>

                                            <%--<em style="display: inline;" class="tb-hidden" id="J_EmStock">库存223件</em>
                                    <span id="J_StockTips">（每人限购1件）</span>--%>
                                    </dd>
                                </dl>

                                <div id="presell-tips" class="det-adv" style="display: none;"><span>正在预售的商品,无法以正常模式下单,请您先预定</span></div>

                                <div class="tb-action clearfix">

                                    <c:choose>
                                        <c:when test="${shopDetail.status eq 1}">
                                            <c:choose>
                                                <c:when test="${prod.prodType eq 'P'}">

                                                    <div id="nowBuy" class="tb-btn-buy tb-btn-sku">
                                                        <a href="javascript:void(0);" onclick="javascript:buyNow();" id="J_LinkBuy" class="buynow-r">立刻购买</a>
                                                    </div>
                                                    <div class="tb-btn-basket tb-btn-sku ">
                                                        <a id="J_LinkBasket" href="javascript:void(0);" onclick="addShopCart()" class="buynow-n">加入购物车</a>
                                                        <!-- 灰色的按钮 -->
                                                        <a id="LinkBasket_disable" href="javascript:void(0);"
                                                           style="display:none;background-color: #ddd;border: 1px solid #ddd;width:140px" ;height:40px;>加入购物车</a>
                                                    </div>
                                                </c:when>
                                                <c:when test="${prod.prodType eq 'S'}">
                                                    <div class="tb-btn-buy tb-btn-sku">
                                                        <a href="${contextPath}/seckill/views/${prod.activeId}">参与秒杀</a>
                                                    </div>
                                                </c:when>
                                            </c:choose>

                                            <div id="phoneBuy" class="tb-btn-phonebuy tb-btn-sku">
                                                <div id="qrcode" class="phonebuy_scanning">
                                                </div>
                                                <a class="buynow-g">手机购买</a>
                                            </div>

                                            <!-- 商品为空显示到货通知 -->
                                            <div id="arrivalInform" class="tb-btn-buy tb-btn-sku" style="display: none">
                                                <a id="arrivalButton" href="javascript:void(0);">到货通知</a>
                                            </div>

                                        </c:when>
                                        <c:otherwise>
                                            <div class="shopdetailOff" style="margin: 30px;font-size: 18px;">
                                                <font color="black" style="font-weight: bold;">该店铺已经下线</font>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>

                <!--产品图片-->
                <%@include file="prodpics.jsp" %>
                <!--产品图片end-->

            </div>

        </div>

        <!--主要信息 end-->

        <div class="sha-col-rep">

            <a href="javascript:void(0);" onclick="alertAddInterestDiag(${prod.prodId});">收藏商品</a>
            <a href="${contextPath}/p/accusation/${prod.prodId}">举报</a>

            <!-- Baidu Button BEGIN -->
            <div class="bdsharebuttonbox" data-tag="share_1">
                <a href="#" class="bds_more" data-cmd="more">分享</a>
            </div>
            <!-- Baidu Button END -->

        </div>

        <!--更多信息-->
        <div class="yt-wrap info_d clearfix">
            <div class="left210_m" id="visitedprod">

            </div>

            <div class="right210_m">
                <div class="detail">
                    <div class="pagetab2" id="productTab">
                        <ul>
                            <li class="on" id="prodContent"><span>商品详情</span></li>
                            <li id="prodSpec"><span>参数</span></li>
                            <li id="prodComment"><span>商品评价</span></li>
                            <li id="prodService"><span>服务说明</span></li>
                            <c:if test="${not empty prod.afterSaleId && prod.afterSaleId!=0}">
                                <li id="prodAfterSale" ss="${prod.afterSaleId}"><span>售后服务</span></li>
                            </c:if>
                        </ul>
                    </div>

                    <div class="content" id="prodContentPane">
                        ${prod.content}
                    </div>

                    <div class="tabcon" id="prodSpecPane">
                    </div>

                    <!-------------------服务说明------------------------>
                    <div class="tabcon" id="prodServicePane" style="text-align: center;">

                    </div>
                    <!-------------------服务说明end------------------------>

                    <div class="content" id="prodAfterSalePane" style="display: none;word-wrap: break-word;word-break: normal; ">
                        <c:if test="${not empty  prod.afterSaleId && prod.afterSaleId!=0}">
                            <c:set var="afterSale" value="${prodApi:getProdAfterSale(prod.afterSaleId)}"></c:set>
                            ${afterSale.content}
                        </c:if>
                    </div>

                    <!----------------评论------------------------->
                    <input type="hidden" id="prodCommentCurPageNO" name="prodCommentCurPageNO" value="${curPageNO}"/>
                    <div style="margin-top:10px;" class="prod-service" id="prodCommentPagenationPane">

                    </div>
                    <!----------------评论end------------------------->

                    <!---------------咨询--------------------->
                    <input type="hidden" id="prodCousultCurPageNO"
                           name="prodCousultCurPageNO" value="${prodCousultCurPageNO}"/>
                    <div class="m10" id="prodCousultPagenationPane"></div>
                    <!---------------咨询-end-------------------->


                    <!--发表咨询-->
                    <div class="Review_Form">
                        <h5 id="writeConsult" name="writeConsult">发表咨询：</h5>
                        <div class="Re_Explain">声明：您可在购买前对产品包装、颜色、运输、库存等方面进行咨询，我们有专人进行回复！因厂家随时会更改一些产品的包装、颜色、产地等参数，所以该回复仅在当时对提问者有效，其他网友仅供参考！咨询回复的工作时间为：周一至周五，9:00至18:00，请耐心等待工作人员回复。</div>
                        <ul>
                            <li id="pointType"><span style="display: inline;">咨询类型：</span>
                                <input type="radio" name="pointType" value="1" checked="checked">
                                商品咨询 <input type="radio" name="pointType" value="2"> 库存配送 <input
                                        type="radio" name="pointType" value="3"> 售后咨询 <label
                                        for="pointType" style="display:none;" class="error">
                                    请选择咨询类型</label></li>
                            <li><span>咨询内容：</span> <textarea class="area1"
                                                             name="consultationContent" id="consultationContent"
                                                             style="font-size: 14px; "></textarea></li>
                            <li style="display:none;" id="column_refer_result"
                                name="column_refer_result">
                                <div class="column_refer_result" id="consult_refer_result"></div>
                            </li>
                            <li class="buttons">
                                <a href="javascript:void(0);" id="saveConsultation" onclick="javascript:saveConsult();" class="btn-comment" style="font-size:12px;color:#ffffff;width:56px;height:30px;background: #333333;border-width:2px;">提交</a>
                                <strong class="text1"></strong></li>
                        </ul>
                    </div>
                    <!--发表咨询结束-->

                    <!-- 猜你喜欢 -->
                    <div id="desgoods" style="width: 990px;">

                    </div>

                    <!--猜你喜欢结束  -->

                </div>
            </div>

        </div>

        <!--更多信息 end-->

    </div>
    <%@ include file="home/bottom.jsp" %>
</div>

<div id="boxy" style="display: none;">
    <div class="ncs-chain-show">
        <dl>
            <dt>门店所在地区：</dt>
            <dd>
                <select class="combox sele" id="provinceid" name="provinceid" requiredTitle="true" childNode="cityid"
                        retUrl="${contextPath}/common/loadProvinces">
                </select>
                <select class="combox sele" id="cityid" name="cityid" requiredTitle="true" showNone="true"
                        childNode="areaid" retUrl="${contextPath}/common/loadCities/{value}">
                </select>
                <select class="combox sele" id="areaid" name="areaid" requiredTitle="true" showNone="true"
                        retUrl="${contextPath}/common/loadAreas/{value}">
                </select>
            </dd>
        </dl>
        <div class="ncs-chain-list">
            <ul nctype="chain_see">
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript">
    var currProdId = '${prod.prodId}';
    var currProdSku = '${prod.skuId}';
    var prodLessMsg = '<fmt:message key="product.less" />';
    var failedOwnerMsg = '<fmt:message key="failed.product.owner" />';
    var failedBasketMaxMsg = '<fmt:message key="failed.product.basket.max" />';
    var failedBasketErrorMsg = '<fmt:message key="failed.product.basket.error" />';
    var skuDtoList = eval('${prod.skuDtoListJson}');
    var propValueImgList = eval('${prod.propValueImgListJson}');
    var allSelected = true;
    var prodName = '${fn:escapeXml(prod.name)}';
    var imgPath3 = "<ls:images item='' scale='3' />";
    var weight = '${prod.weight}';
    var volume = '${prod.volume}';
    var shopId = '${prod.shopId}';
    var transportId = '${prod.transportId}';
    var goods_transfee = '<c:out value="${prod.supportTransportFree}"></c:out>';
    var transport_type = '<c:out value="${prod.transportType}"></c:out>';
    var mail_trans_fee = '<c:out value="${prod.mailTransFee}"></c:out>';
    var express_trans_fee = '<c:out value="${prod.expressTransFee}"></c:out>';
    var ems_trans_fee = '<c:out value="${prod.emsTransFee}"></c:out>';
    var loginUserName = '${_userDetail.userName}'; //在header.jsp中定义
    var distUserName = '${uid}';
    var isGroup = '${prod.isGroup}';
    var distUrl = '${vueDomainName}/goodsDetail?goodsInfoId=' + currProdId + '&uid=' + loginUserName;
    var jiathis_config = {url: distUrl}
    var existPresellPlugin = false;
    var existAuctionPlugin = false;
    var transportId = '${prod.transportId}';

    /**
     * 处理sku信息（boxy回调）
     */
    function boxyCallback(){
        handleSkuMessage();
    }
</script>
<ls:plugin pluginId="presell">
    <script type="text/javascript">
        existPresellPlugin = true;
    </script>
</ls:plugin>
<ls:plugin pluginId="auction">
    <script type="text/javascript">
        existAuctionPlugin = true;
    </script>
</ls:plugin>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/views.js'/>"></script>
<script type="text/javascript" src="${contextPath}/resources/common/js/jquery.fly.min.js"></script>
<ls:plugin pluginId="store">
    <script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
    <link href="<ls:templateResource item='/resources/templets/css/store/store.css'/>" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/store/storeView.js'/>"></script>
</ls:plugin>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/qrcode/jquery.qrcode.min.js'/>"></script>
<script type="text/javascript">
    jQuery('#qrcode').qrcode({width: 153, height: 153, text: "${vueDomainName}/commonModules/goodsDetail/goodsDetail?goodsInfoId=${prod.prodId}"});
</script>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/baidushare/api/js/share.js?v=89860593.js?'/>"></script>
<script type="text/javascript">
    var contextPath = '${contextPath}';
    var mailfeeStr = '${mailfeeStr}';
    var selectSkuCash = 0;
    $(function () {
        $.ajax({
            url: "${contextPath}/like",
            type: "post",
            data: {"count": 5},
            dataType: 'html',
            async: false, //默认为true 异步
            success: function (result) {
                $("#desgoods").html(result);
            },
            error: function (e) {
            }
        });


        $("#loginIm").click(function () {
            var user_name = $("#user_name").val();
            var user_pass = $("#user_pass").val();
            var skuId = $("#currSkuId").val();
            if (user_name == '' || user_pass == '') {
                login();
            } else {
                window.open(contextPath + "/p/im/index/" + skuId, '_blank');
            }
        });

    });

    /* 百度分享 */
    window._bd_share_config = {
        common: {
            bdUrl: distUrl,
            bdText: "",
            bdMini: "2",
            bdMiniList: ['weixin', 'tsina', 'sqq', 'qzone'],
            bdPic: "",
            bdStyle: "0",
            bdSize: "4"
        },
        share : [{
            "bdSize" : 4
        }]
    };


</script>
</body>
</html>