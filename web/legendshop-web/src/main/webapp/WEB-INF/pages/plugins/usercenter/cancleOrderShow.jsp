<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/processOrder.css?1'/>" />
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<title>取消订单信息</title>
<style>
	table td{
		border:none;
	}
</style>
</head>
<body>
		<div style="position: absolute;  width: 410px;">
			<div class="white_box" style="margin-left:15px;">
				<%--<h1 style="cursor: move;">确认发货</h1>
					--%>
					<table width="100%" cellspacing="0" cellpadding="0" border="1" class="box_table" style="float:left;margin-top:20px;">
						<tbody>
							<tr>
								<td width="100" valign="top" align="right">订单号： 
									<input type="hidden" value="${subNumber}" readonly="readonly" id="subNumber"  name="subNumber">
								</td>
								<td align="left">${subNumber}</td>
							</tr>
							<tr>
								<td align="right">取消原因：</td>
								<td align="left">
									<select id="state_info" name="state_info">
								       <option value ="1">订单不能按预计时间送达</option>
								       <option value ="2">操作有误（商品、地址等选错）</option>
								       <option value ="4">重复下单/误下单</option>
								       <option value ="5">其他渠道价格更低</option>
								       <option value ="6">不想买了</option>
								       <option value ="7">商品无货</option>
								       <option value ="3">其他原因</option>
									</select>
								</td>
							</tr>
							<tr class="otherReson" style="display:none;">
								<td></td>
								<td align="left" style="display:none;" id="other_reason">
									<textarea style="height: 80px;" rows="5" cols="30" id="other_state_info" name="other_state_info" maxlength="50"></textarea>
								</td>
							</tr>
							<tr>
								<td align="center" style="text-align: center;" colspan="2">
								    <span class="inputbtn">
										<input type="button" class="${btnclass}" onclick="orderCancel();" style="cursor:pointer;" value="保存" >
							   		</span>
						       		<span class="inputbtn">
										<input type="button" class="${btnclass}" onclick="closeLayer();" style="cursor:pointer;" value="再想想" >
							   		</span>
								</td>
							</tr>
						</tbody>
					</table>
			</div>
		</div>
	<script type="text/javascript">	
		$("#state_info").live('change', function(){  	
			var value = $("#state_info option:selected").val();
			if(value == 3){
				$(".otherReson").attr("style", "");
				$("#other_reason").attr("style", "");
			}else {
				$(".otherReson").attr("style", "display:none;");
				$("#other_reason").attr("style", "display:none;");
			}
		});
		
		function closeLayer(){
			var index = parent.layer.getFrameIndex('cancleOrder'); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭
		}
		
		function orderCancel(){
			layer.confirm('确定取消该订单吗？',{icon:0,area:['400px','200px']}, function(){
				var reson = $("#state_info option:selected").val();
				var cancelReason = $("#state_info option:selected").text();
				var subNumber = $("#subNumber").val();
				if(reson==3){
					var otherReson = $("#other_state_info").val();
					if(otherReson==null || otherReson.length<=0){
						layer.tips('请输入取消原因', '#other_state_info');
						return;
					}
					cancelReason = $("#other_state_info").val();
				}
				$.ajax({
					url: "${contextPath}/p/order/cancleOrder", 
					data: {"subNumber":subNumber, "cancelReason":cancelReason},
					type: 'POST', 
					async: false, //默认为true 异步   
					dataType:'text',
					success:function(result){
						if(result=="OK"){
							var msg = "取消订单成功";
							layer.msg(msg, {icon: 1,time :2000},function(){
								window.location.reload();
							});
						}else if(result=="fail"){
							var msg = "取消订单失败";
							layer.msg(msg, {icon: 2,time :2000},function(){
								window.location.reload();
							});
						}
					}
				});
				parent.location.reload();
				var index = parent.layer.getFrameIndex('cancleOrder'); //先得到当前iframe层的索引
				parent.layer.close(index); //再执行关闭
			});
		}
	</script>
</body>
</html>