<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
 	<div style="text-align: center;">
 	  <label style="font-weight: bold;">库存记录</label>
   </div>
   <table style="width: 99%">
   	<input type="hidden" id="skuId" name="skuId" value="${skuId}" />
   	<thead>
		<tr>
		<td width="150">名称</td>
		<td width="110">销售库存变更前</td>
		<td width="110">销售库存变更后</td>
		<td width="110">实际库存变更前</td>
		<td width="110">实际库存变更后</td>
		<td width="110">变更时间</td>
		<td width="110">备注</td>
		</tr>
	</thead>
	<c:if test="${empty requestScope.listStockLog}">
		<tr><td colspan="6" style="text-align: center;">没有找到符合条件的商品</td></tr>
	</c:if>
	<c:forEach items="${requestScope.listStockLog}" var="stockLog">
		 <tr>
		 	<td style="text-align: center;">${stockLog.name}</td>
		 	<td style="text-align: center;">
				<c:choose>
					<c:when test="${stockLog.beforeStock  eq -1}">
							无
					</c:when>
					<c:otherwise>
						${stockLog.beforeStock}件
					</c:otherwise>
				</c:choose>
		 	</td>
		 	<td style="text-align: center;">${stockLog.afterStock}件</td>
		 	<td style="text-align: center;">
				<c:choose>
					<c:when test="${empty stockLog.beforeActualStock || stockLog.beforeActualStock  eq -1}">
							无
					</c:when>
					<c:otherwise>
						${stockLog.beforeActualStock}件
					</c:otherwise>
				</c:choose>
		 	</td>
		 	<td style="text-align: center;">
				<c:choose>
					<c:when test="${empty stockLog.afterActualStock}">
						${stockLog.afterStock}件
					</c:when>
					<c:otherwise>
						${stockLog.afterActualStock}件
					</c:otherwise>
				</c:choose>
			</td>
		 	<td style="text-align: center;"><fmt:formatDate value="${stockLog.updateTime}" type="both" /></td>
		 	<td style="text-align: center;">${stockLog.updateRemark}</td>
		 </tr>
	</c:forEach>
	<c:if test="${totalStockLog>5}">
		 <tr>
        	<td colspan="6">
        			<div style="margin-top:10px;" class="page clearfix">
	   			 <div class="p-wrap">
							<ls:page pageSize="${pageSizeStockLog}"  total="${totalStockLog}" curPageNO="${curPageNOStockLog}"  type="simple" actionUrl="javascript:stockLogPager"/> 
	   			 </div>
			</div>
        	</td>
        </tr>
      </c:if>
   </table>