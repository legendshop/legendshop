<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>被举报禁售-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-reported.css'/>" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">被举报禁售</span>
				</p>
			</div>
			<%@ include file="included/sellerLeft.jsp" %>
			<div class="seller-reported">
				<div class="pagetab2">
					<ul id="listType">
						<li class="on"><span>被举报禁售</span></li>
					</ul>
	   			</div>
				<div class="now-have sold-ser-no-bg" style="background: #f9f9f9;">
					当前有<span>${reportForbitTotal}</span>个商品被举报禁售，请您尽快处理
				</div>
				<table class="reported-table sold-table">
					<colgroup>
						<col style="width: 10%; min-width: 10%">
						<col style="width: 20%; min-width: 20%">
						<col style="width: 20%; min-width: 20%">
						<col style="width: 10%; min-width: 10%">
						<col style="width: 10%; min-width: 10%">
						<col style="width: 20%; min-width: 20%">
						<col style="width: 10%; min-width: 10%">
					</colgroup>
					<tr class="reported-tit">
						<th>商品</th>
						<th>举报类型</th>
						<th>举报主题</th>
						<th>举报时间</th>
						<th>状态</th>
						<th>处理意见</th>
						<th>操作</th>
					</tr>
					<c:choose>
			           <c:when test="${empty requestScope.list}">
			              <tr>
						 	 <td colspan="20">
						 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
						 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
						 	 </td>
						 </tr>
			           </c:when>
			           <c:otherwise>
			              <c:forEach items="${requestScope.list}" var="accusation" varStatus="status">
							<tr class="detail">
								<td class="w78"><a href="${contextPath}/views/${accusation.prodId}" title="${accusation.prodName}" target="_blank"><img src="<ls:images item='${accusation.prodPic}' scale='3' />" alt="${accusation.prodName}"></a></td>
								<td style="word-break:break-all;">${accusation.accuType}</td>
								<td style="word-break:break-all;">${accusation.title}</td>
								<td><fmt:formatDate value="${accusation.recDate}" type="date" /></td>
								<td>
									<c:choose>
	     								<c:when test="${accusation.status ==1}">已处理</c:when>
	     								<c:otherwise>未处理</c:otherwise>
	     							</c:choose>
								</td>
								<td class="del-sev" style="word-break:break-all;">
									<c:choose>
										<c:when test="${empty accusation.handleInfo}">暂无</c:when>
										<c:otherwise>${accusation.handleInfo}</c:otherwise>
									</c:choose>
								</td>
								<td>
									<a href='javascript:alertAccusation("${accusation.id}")' title="查看详情" class="btn-r">查看详情</a>
								</td>
							</tr>
						</c:forEach>
			           </c:otherwise>
		           </c:choose>
				</table>
				<div class="clear"></div>
					<div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
				            		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
				     			 </div>
		  		</div>	
			</div>
		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/reportForbit.js'/>"></script>
	<script type="text/javascript">
		var contextPath="${contextPath}";
	</script>
</body>
</html>