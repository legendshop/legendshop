<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss"
	var="nowDate" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<title>拍卖活动详情-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css"	href="<ls:templateResource item='/resources/templets/css/active/actives${_style_}.css'/>" rel="stylesheet">
<link type="text/css"	href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet">
<link type="text/css"	href="<ls:templateResource item='/resources/templets/css/bidding-activity${_style_}.css'/>" rel="stylesheet">

</head>
<body class="graybody">
	<div id="doc">
		<%@ include	file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置> <a href="${contextPath}/home">首页</a>> 
					<a	href="${contextPath}/sellerHome">卖家中心</a>> 
					<a  href="${contextPath}/s/auction/query">拍卖活动管理</a>>
					<span class="on">拍卖活动详情</span>
				</p>
			</div>
			<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp"%>
			<div id="rightContent" class="right_con">
				<!--page-->
				<!-- 拍卖活动详情 -->
				<div class="bidding-detail">
					<div class="bid-det-com">
						<p>运营结果</p>
						<div class="operate-statistics-det">
							<ul>
								<li>
									<c:choose>
										<c:when test="${auction.status eq 2}">
											成交金额：<span>￥${auction.curPrice}</span>
										</c:when>
										<c:otherwise>
											当前价：<span>￥${auction.curPrice}</span>
										</c:otherwise>
									</c:choose>
								</li>
								<li>
									出价次数：<span>${auction.biddingNumber}</span>
								</li>
								<li>
									参加人数：<span>${auction.enrolNumber}</span>
								</li>
								<li>
									活动状态：
									<c:choose>
										<c:when test="${auction.status eq -2}">
											<span>未通过</span>
										</c:when>
										<c:when test="${auction.status eq -1}">
											<span>审核中</span>
										</c:when>
										<c:when test="${auction.status eq 0}">
											<span>下线</span>
										</c:when>
										<c:when test="${auction.status eq 2}">
											<span>已完成</span>
										</c:when>
										<c:otherwise>
											<span>进行中</span>
										</c:otherwise>
									</c:choose>
								</li>
							</ul>
						</div>
					</div>
					<div class="bidding-des">
						<div class="bidding-des-tit" style="">
							<p>出价详情</p>
						</div>
						<div id="auctionDetail" class="bidding-des-cont" type="1"></div>
					</div>
					<div class="bidding-des">
						<div class="bidding-des-tit" style="">
							<p>保证金详情</p>
						</div>
						<div id="depositrecDetail" class="bidding-des-cont" type="2" style="margin-bottom:10px;"></div>
					</div>
					<div class="btn-box">
						<input type="button" class="btn-r big-btn" style="padding:0;" value="返回" onclick="window.history.go(-1);" />
					</div>
				</div>
				<!--page-->
			</div>
		</div>
<%@ include	file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
<script src="${contextPath}/resources/templets/js/shopAuctionDetail.js"></script>
	</div>
		<script type="text/javascript">
			var contextPath = '${contextPath}';
			var auctionId = '${auction.id}';
		</script>
</body>
</html>
