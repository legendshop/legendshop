<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

<table cellpadding="0" cellspacing="0" class="sold-table">
     <tr>
       <th width="200">订单编号</th>
       <th width="100">会员手机号</th>
       <th>会员ID</th>
       <th width="100">奖励</th>
       <th>奖励类型</th>
       <th width="100">奖励状态</th>
       <th width="250">获取奖励时间</th>
     </tr>
     <c:choose>
         <c:when test="${empty requestScope.list}">
            <tr>
			 	 <td colspan="20">
			 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
			 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
			 	 </td>
			 </tr>
         </c:when>
         <c:otherwise>
	         <c:forEach items="${requestScope.list }" var="item" >
		          <tr>
		            <td>${item.sn}</td>
		            <td>${item.userMobile}</td>
		            <td>${item.subUserName}</td>
		            <td>
		            	<c:if test="${item.commisType=='REDUCE_COMMIS'}">-</c:if>
		            	¥<fmt:formatNumber value="${item.amount}" pattern="#0.00#"/>
		            </td>
		            <td>
		            	<c:choose>
		            		<c:when test="${item.commisType=='FIRST_COMMIS'}" >
		            			直接下级奖励
		            		</c:when>
		            		<c:when test="${item.commisType=='SECOND_COMMIS'}" >
		            			下二级奖励
		            		</c:when>
		            		<c:when test="${item.commisType=='THIRD_COMMIS'}" >
		            			下三级奖励
		            		</c:when>
		            		<c:when test="${item.commisType=='REG_COMMIS'}" >
		            			推广注册奖励
		            		</c:when>
		            		<c:when test="${item.commisType=='REDUCE_COMMIS'}" >
		            			商品退款，回退奖励
		            		</c:when>
		            	</c:choose>
		            </td>
		            <td>
		            	<c:if test="${item.settleSts eq 0}">未结算</c:if>	
		            	<c:if test="${item.settleSts eq 1}">已结算</c:if>	
		            </td>
		            <td><fmt:formatDate value="${item.addTime }" pattern="yyyy-MM-dd HH:mm" /></td>
		          </tr>
		   </c:forEach>
         </c:otherwise>
      </c:choose>
 </table>
<div style="margin-top:10px;" class="page clearfix">
	<div class="p-wrap">
			<span class="p-num">
				<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
			</span>
	</div>
</div>