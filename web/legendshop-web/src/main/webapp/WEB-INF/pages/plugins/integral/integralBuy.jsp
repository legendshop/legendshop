<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
<title>兑换积分订单</title>

<style >
 .w1216{width: 620px;margin: 0px auto;padding: 0px;font-size: 12px;font-family: 瀹嬩綋;list-style: none;}
.delivery-address{overflow: hidden;}
.delivery-address table{width: 600px;text-align: center;border: 1px solid #e4e4e4;border-bottom: none;}
.delivery-address td{height: 30px;border-bottom: 1px solid #e4e4e4;}
.delivery-address .tit{background-color: #f9f9f9;}
.delivery-address .cho{width: 60px;}
.delivery-address .sub{border: 1px solid #e4e4e4;margin-top: 20px;width: 600px;}
.delivery-address .sub li{list-style: none;margin-top: 10px;overflow: hidden;}
.delivery-address .sub li span{color: red;font-size: 14px;font-weight: bold;}
.delivery-address .sub li button{display: block;background-color: #CD2626;text-decoration: none;color: #fff;height: 30px;width: 100px;border-radius: 1px;line-height: 30px;text-align: center;float: right;margin-right: 20px;border:none;cursor: pointer;}
</style>

<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/integral/integralBuy.js'/>"></script>
</head>
<body>

   <div class="w1216">
		<div class="delivery-address">
			<table cellpadding="0" cellspacing="0">
			    <tr class="tit">
					<td class="cho">选择</td>
					<td>收货人</td>
					<td>收货地址</td>
					<td>手机号码</td>
				</tr>
			    <c:forEach items="${userAddress}" var="address" varStatus="status">
                         <tr>
							<td class="cho">
							   <input type="radio" id="address" <c:if test="${address.commonAddr eq 1}">checked="checked" </c:if> name="address" value="${address.addrId}">
							</td>
							<td>${address.receiver}</td>
							<td>${address.subAdds}</td>
							<td>${address.mobile}</td>
						</tr>
                 </c:forEach>
			</table>
			<div class="sub">
				<ul>
					<li>商品数量：<span>${count}</span></li>
					<li>积分总数：<span>${totalScore}</span></li>
					<li>
						请输入支付密码：<input style="padding: 5px 0px; width: 150px;" type="password"  maxlength="35"  id="pay-password" name="password" value="" >
						还未设置密码？<a style="color: rgb(63, 109, 224);" target="_blank" href="<ls:url address='/p/security'/>" >马上设置</a></p>
					</li>
					<li>
					   <button type="button" id="submit" onclick="buy();" value="">提交订单</button>
					</li>
				</ul>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
		var contextPath = '${contextPath}';
		var count="${requestScope.count}";
		var prodId="${requestScope.prodId}";
		var token="${sessionScope.INTEGRAL_SESSION_TOKEN}";
</script>
</html>