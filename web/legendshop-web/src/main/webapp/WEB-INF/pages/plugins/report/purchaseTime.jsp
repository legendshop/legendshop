<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>购买分析-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-buyAnalysis${_style_}.css'/>" rel="stylesheet">
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
	<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
<div id="Content" class="w1190">
<div class="seller-cru">
					<p>
						您的位置>
						<a href="${contextPath}/home">首页</a>>
						<a href="${contextPath}/sellerHome">卖家中心</a>>
						<span class="on">购买分析</span>
					</p>
				</div>
	<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
	<div class="seller-buyAnalysis">
		<div class="seller-com-nav" >
			<ul>
				<li ><a href="${contextPath}/s/shopReport/purchaseAnalysis">订单价格销量</a></li>
				<li class="on"><a href="javascript:void(0)">购买时段</a></li>
			</ul>
		</div>
		<div class="alert" style="margin-bottom:0;">
			<ul class="mt5">
				<li>1、符合以下任何一种条件的订单即为有效订单：1）采用在线支付方式支付并且已付款；</li>
		    	<li>2、“购买时段分布”统计图展示符合搜索条件的有效订单在各个时间段的分布情况，为工作时间的合理安排提供依据</li>
		    </ul>
		</div>

<form:form  action="${contextPath}/s/shopReport/purchaseTime" method="post" id="from1">
		<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO ==null?1:curPageNO}"/>
		<div class="form search-01 sold-ser sold-ser-no-bg clearfix">
			<div class="fr">
				<div class="item">
					<select id="status" class="item-sel"  name="status">
			        	<ls:optionGroup type="select" required="true" cache="true" beanName="ORDER_STATUS" selectedValue="${reportRequest.status}"/>          			
			        </select>
				</div>
				<div class="item">
					<select id="queryTerms" class="item-sel"  name="queryTerms" onchange="change(this.value)"> 
						 <option <c:if test="${reportRequest.queryTerms == 0 }">selected</c:if> value="0">按照天统计</option>
						 <option <c:if test="${reportRequest.queryTerms == 3 }">selected</c:if> value="3">按照周统计</option>
						 <option <c:if test="${reportRequest.queryTerms == 1 }">selected</c:if> value="1">按照月统计</option>
			        </select>
			        <span id="range">
				 		  <input readonly="readonly"  name="selectedDate" id="selectedDate" class="Wdate item-sel" style="height:25px;" type="text" value='<fmt:formatDate value="${reportRequest.selectedDate}" pattern="yyyy-MM-dd"/>' />
					</span>	
					<input type="submit" class="btn-r" id="btn_keyword" value="查 询" />				
				</div>
			</div>
		</div>
</form:form>

<input type="hidden" id="reportJson" value='${reportJson}' /> 
<div id="main" style="margin-left: 10px;width:98%;height:400px"></div>

</div>
</div>
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
	<script src="<ls:templateResource item='/resources/plugins/ECharts/dist/echarts.js'/>" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/templets/js/multishop/shopReport.js'/>" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/templets/js/multishop/purchaseTime.js'/>" type="text/javascript"></script>
	<script type="text/javascript">
	var contextPath = '${contextPath}';
	var selectedYear = '${reportRequest.selectedYear}';
	var selectedMonth = '${reportRequest.selectedMonth}';
	var selectedDay = '<fmt:formatDate value="${reportRequest.selectedDate}" pattern="yyyy-MM-dd"/>';
	var selectedWeek = '${reportRequest.selectedWeek}';
	var nowYear = new Date().getFullYear();
	var nowMonth = new Date().getMonth() + 1;
	var commonYear = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];//平年月份的总天数
	var LeapYear = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];//闰年月份的总天数 
	$(function(){
		laydate.render({
			   elem: '#selectedDate',
			   calendar: true,
			   theme: 'grid',
			   trigger: 'click'
		  });
	});
	</script>
</body>
</html>
