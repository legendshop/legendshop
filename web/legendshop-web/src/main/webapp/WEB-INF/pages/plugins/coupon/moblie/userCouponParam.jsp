<!DOCTYPE html>
	<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
	<%@ include file='/WEB-INF/pages/frontend/templetGoat/common/taglib.jsp' %>
    <%@ include file='/WEB-INF/pages/frontend/templetGoat/common/common.jsp' %>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<title>我的礼券</title>
	<link rel="stylesheet" href="<ls:templateResource item='/resources/templets/css/userCoupon.css'/>">
</head>
<body>

<input type="hidden" id="pageCount" name="pageCount" value="${pageCount}"/>
<if test="${not empty requestScope.coupons}">
     <c:forEach items="${requestScope.coupons }" var="coupon" >
     <c:choose>
         	<c:when test="${useStatus==1}">
						<div class="pepper-con">
							<div class="pepper-w">
								<div class="pepper pepper-blue">
									     <c:if test="${coupon.couponType==1}">
										        <div class="pepper-l">
							    					<p class="pepper-l-num">
														<i>¥</i>
														<span><fmt:formatNumber type="currency" value="${coupon.offPrice }" pattern="0"/></span>
														（单笔满<fmt:formatNumber type="currency" value="${coupon.fullPrice }" pattern="0"/>可使用）
													</p>
							    					<p class="pepper-l-con">发行店铺：${coupon.shopName}</p>
							    					<p class="pepper-l-con">兑换码：${coupon.couponSn}</p>
							    				</div>
							    		  </c:if>	
							    		  <c:if test="${coupon.couponType==2}">
							    		         <div class="pepper-l">
							    					<p class="pepper-l-num">
														<i>¥</i>
														<span><fmt:formatNumber type="currency" value="${coupon.offPrice }" pattern="0"/></span>
														（全场可用 &nbsp;&nbsp;运费优惠券）
													</p>
							    					<p class="pepper-l-con">发行店铺：${coupon.shopName}</p>
							    					<p class="pepper-l-con">兑换码：${coupon.couponSn}</p>
							    				</div>
							    		  </c:if>
						    		  
						    		
						    				<!-- 卷信息显示 -->
						    				<div class="pepper-r">
						    					<span>有效期</span>
						    					<div>
													<fmt:formatDate value="${coupon.startDate}" pattern="yyyy-MM-dd HH:mm" />
						        					<p class="pepper-line">~</p>
						        					<fmt:formatDate value="${coupon.endDate}" pattern="yyyy-MM-dd HH:mm" />
						    					</div>
						    					 <!--  <i class="right-arrow"></i> -->
						        			</div>
								  </div>
								  <div class="pepper-b pepper-blue">
										<div class="pb-con"></div>  <!-- 一行蓝色 -->
										<div class="pb-border"></div>  <!-- 波浪图 -->
								  </div>	
										    <!-- 蓝色下滑波浪线 -->
							 </div>
						</div>
			  </c:when>
			  
			  <c:when test="${useStatus==2}">
			         <div class="pepper-con">
							<div class="pepper-w">
								<div class="pepper pepper-blue">
								    <c:if test="${coupon.couponType==1}">
									        <div class="pepper-l">
						    					<p class="pepper-l-num">
													<i>¥</i>
													<span><fmt:formatNumber type="currency" value="${coupon.offPrice }" pattern="0"/></span>
													（单笔满<fmt:formatNumber type="currency" value="${coupon.fullPrice }" pattern="0"/>可使用）
												</p>
						    					<p class="pepper-l-con">发行店铺：${coupon.shopName}</p>
						    					<p class="pepper-l-con">兑换码：${coupon.couponSn}</p>
						    				</div>
						    		</c:if>		
						    		<c:if test="${coupon.couponType==2}">
							    		         <div class="pepper-l">
							    					<p class="pepper-l-num">
														<i>¥</i>
														<span><fmt:formatNumber type="currency" value="${coupon.offPrice }" pattern="0"/></span>
														（全场可用 &nbsp;&nbsp;运费优惠券）
													</p>
							    					<p class="pepper-l-con">发行店铺：${coupon.shopName}</p>
							    					<p class="pepper-l-con">兑换码：${coupon.couponSn}</p>
							    				</div>
							    	</c:if>		
						    				<!-- 卷信息显示 -->
						    				<div class="pepper-r">
						    					<span>使用日期</span>
						    					<div>
													<fmt:formatDate value="${coupon.useTime}" pattern="yyyy-MM-dd HH:mm" />
						    					</div>
						        			</div>
						        			<!-- 东券 -->
								  </div>
										<div class="pepper-b pepper-blue">
											<div class="pb-con"></div>  <!-- 一行蓝色 -->
											<div class="pb-border"></div>  <!-- 波浪图 -->
										</div>	
										    <!-- 蓝色下滑波浪线 -->
							 </div>
						</div>
			  </c:when>
			  
			  <c:otherwise>
			        <div class="pepper-con">
							<div class="pepper-w">
								<div class="pepper pepper-blue">
						    		<c:if test="${coupon.couponType==1}">
									        <div class="pepper-l">
						    					<p class="pepper-l-num">
													<i>¥</i>
													<span><fmt:formatNumber type="currency" value="${coupon.offPrice }" pattern="0"/></span>
													（单笔满<fmt:formatNumber type="currency" value="${coupon.fullPrice }" pattern="0"/>可使用）
												</p>
						    					<p class="pepper-l-con">发行店铺：${coupon.shopName}</p>
						    					<p class="pepper-l-con">兑换码：${coupon.couponSn}</p>
						    				</div>
						    		</c:if>		
						    		<c:if test="${coupon.couponType==2}">
							    		         <div class="pepper-l">
							    					<p class="pepper-l-num">
														<i>¥</i>
														<span><fmt:formatNumber type="currency" value="${coupon.offPrice }" pattern="0"/></span>
														（全场可用 &nbsp;&nbsp;运费优惠券）
													</p>
							    					<p class="pepper-l-con">发行店铺：${coupon.shopName}</p>
							    					<p class="pepper-l-con">兑换码：${coupon.couponSn}</p>
							    				</div>
							    	</c:if>		
						    				<!-- 卷信息显示 -->
						    				<div class="pepper-r">
						    					<span>有效期</span>
						    					<div>
													<fmt:formatDate value="${coupon.startDate}" pattern="yyyy-MM-dd HH:mm" />
						        					<p class="pepper-line">~</p>
						        					<fmt:formatDate value="${coupon.endDate}" pattern="yyyy-MM-dd HH:mm" />
						    					</div>
						    					 <!--  <i class="right-arrow"></i> -->
						        			</div>
								  </div>
								  <div class="pepper-b pepper-blue">
										<div class="pb-con"></div>  <!-- 一行蓝色 -->
										<div class="pb-border"></div>  <!-- 波浪图 -->
								  </div>	
										    <!-- 蓝色下滑波浪线 -->
							 </div>
						</div>
			  </c:otherwise>
			  
		</c:choose>	
	</c:forEach>
</if>

<c:if test="${empty requestScope.coupons}">
   <div style="text-align: center;">
                   <strong>暂无数据</strong>
   </div>
</c:if>

</body>
</html>