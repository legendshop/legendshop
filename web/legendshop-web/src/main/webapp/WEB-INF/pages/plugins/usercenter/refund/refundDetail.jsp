<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>

<html>
  <head>
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>退款详情-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
	<style type="text/css">
		.ret-detail .ste-con ul li a:hover{color:#e5004f;}
	</style>
  </head>
<body class="graybody">
	   <div id="bd">
	   		 <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
			 <div class=" yt-wrap" style="padding-top:10px;"> 
       	 		<%@include file='../usercenterLeft.jsp'%>
		
			    <!-----------right_con-------------->
			     <div class="right_con">
			     	
			         <div class="o-mt"><h2>退款详情</h2></div>
				        <div class="alert">
				          <h4>提示：</h4>
				          <ul>
				            <li>1. 若提出申请后，商家拒绝退款，可再次提交申请或选择<em>“商品投诉”</em>，请求商城客服人员介入。
				            	<%-- <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&amp;uin=2465239493&amp;site=legendshop&amp;menu=yes">
									<img border="0" src="${contextPath }/resources/templets/images/qq_ico.png" alt="销售咨询">
								</a> --%>
								
								<!-- 如果商家拒绝退款 显示平台客服入口  -->
								<c:if test="${subRefundReturn.sellerState eq 3}">
									<a target="_blank" href="${contextPath}/p/im/customerAdmin/0"
				                   		style="width:100px;height:26px;background: #e5004f;line-height:26px;display: inline-block;text-align: center;color: #fff;border-radius: 3px;">
				                    	<i style="background: url('${contextPath}/resources/templets/images/chat-i.png');width: 19px;height: 18px;display: inline-block;vertical-align: middle;margin-right: 5px;margin-bottom: 2px;"></i>平台客服
				               		</a>
			               		</c:if>
				            </li>
				            <li>2. 成功完成退款；经过商城审核后，会将退款金额以<em>“预存款”</em>的形式返还到您的余额账户中或者会将退款金额以<em>“原路返回”</em>的形式退还。</li>
				          </ul>
				        </div>
				        <div class="ret-step">
				          <ul>
				            <li <c:if test="${not empty subRefundReturn.applyTime}">class="done"</c:if>><span>买家申请退款</span></li>
				            <li <c:if test="${not empty subRefundReturn.sellerTime}">class="done"</c:if>><span>商家处理退款申请</span></li>
				            <li <c:if test="${not empty subRefundReturn.adminTime}">class="done"</c:if>><span>平台审核，退款完成</span></li>
				          </ul>
				        </div>
				        <div class="ret-detail">
				         <div class="ste-tit"><b>我的退款申请</b></div>
				          <div class="ste-con">
				            <ul>
				              <li><span>退款编号：</span>${subRefundReturn.refundSn}  </li>
				              <li><span>申请时间：</span><fmt:formatDate value="${subRefundReturn.applyTime}" pattern="yyyy-MM-dd HH:mm"/> </li>
				              <li><span>订单编号：</span><a href="${contextPath }/p/orderDetail/${subRefundReturn.subNumber }" target="_blank">${subRefundReturn.subNumber}</a> </li>
				              <c:if test="${not empty subRefundReturn.outRefundNo}">
			               	  	<li><span>第三方支付流水号：</span>${subRefundReturn.outRefundNo}</li>
			               	  </c:if>
				              <li>
				              	<span>涉及商品：</span>
				              		<c:if test="${subRefundReturn.subItemId eq 0 }">
							       		<a href="${contextPath }/p/orderDetail/${subRefundReturn.subNumber }" target="_blank">${subRefundReturn.productName }</a>
									</c:if>       
						       		<c:if test="${subRefundReturn.subItemId ne 0 }">
							       		<a href="${contextPath}/views/${subRefundReturn.productId}" target="_blank">${subRefundReturn.productName }</a>
									</c:if>       
				              </li>
				              <li><span>退款原因：</span>${subRefundReturn.buyerMessage} </li>
				              <li><span>退款金额：</span>￥ ${subRefundReturn.refundAmount} </li>
				              <li><span>退款说明：</span>${subRefundReturn.reasonInfo}</li>
				              <c:if test="${not empty subRefundReturn.photoFile1}">
				              <li style="border: 0;">
				              	<span>凭证上传1：</span>
				              	<a href="<ls:photo item='${subRefundReturn.photoFile1}'/>" target="_blank"><img src="<ls:images item='${subRefundReturn.photoFile1}' scale='2'/>"/></a>
				              </li>
				              </c:if>
				              <c:if test="${not empty subRefundReturn.photoFile2}">
				              	  <li style="border: 0;">
					              	<span>凭证上传2：</span>
					              	<a href="<ls:photo item='${subRefundReturn.photoFile2}'/>" target="_blank"><img src="<ls:images item='${subRefundReturn.photoFile2}' scale='2'/>"/></a>
					              </li>
				              </c:if>
				               <c:if test="${not empty subRefundReturn.photoFile3}">
					              <li style="border: 0;">
					              <span>凭证上传3：</span>
					              	<a href="<ls:photo item='${subRefundReturn.photoFile3}'/>" target="_blank"><img src="<ls:images item='${subRefundReturn.photoFile3}' scale='2' />"/></a>
					              </li>
				              </c:if>
				            </ul>
				          </div>
				          <div class="ste-tit"><b>商家退款处理</b></div>
				          <div class="ste-con">
				            <ul>
				              <li>
				              	<span>审核状态：</span>
				              	<c:if test="${subRefundReturn.sellerState eq 1  && subRefundReturn.applyState eq -1}">
				              		已撤销
				              	</c:if>
				              	<c:if test="${subRefundReturn.sellerState eq 1  && subRefundReturn.applyState eq 1}">
				              		待审核
				              	</c:if>
				              	<c:if test="${subRefundReturn.sellerState eq 2}">
				              		同意
				              	</c:if>
				              	<c:if test="${subRefundReturn.sellerState eq 3}">
				              		不同意
				              	</c:if>
				              </li>
				              <li style="border: 0;">
				              	<span>商家备注：</span>
				              	${subRefundReturn.sellerMessage }
				              	<c:if test="${empty subRefundReturn.sellerMessage }">
				              		无
				              	</c:if>
				              </li>
				            </ul>
				          </div>
				          <c:if test="${subRefundReturn.sellerState eq 2}">
				          	<div class="ste-tit"><b>商城退款审核</b></div>
					          <div class="ste-con">
					            <ul>
					              <li>
					              	<span>平台确认：</span>
					              	<c:if test="${subRefundReturn.applyState eq 2}">
					              		待平台确认
					              	</c:if>
					              	<c:if test="${subRefundReturn.applyState eq 3}">
					              		已完成
					              	</c:if>
					              </li>
					              <li style="border: 0;">
						              <span>平台备注：</span>
						              <c:if test="${empty subRefundReturn.adminMessage}">暂无</c:if>
						              ${subRefundReturn.adminMessage}
					              </li>
					            </ul>
					          </div>
				          </c:if>
				          <c:if test="${subRefundReturn.applyState eq 3 }">
					          <div class="ste-tit"><b>退款详细</b></div>
					          <div class="ste-con">
					            <ul>
					              <li>
					              	<span>支付方式：</span>
					              	<c:if test="${subRefundReturn.payTypeId eq 'ALP' }">支付宝 </c:if>
					              	<c:if test="${subRefundReturn.payTypeId eq 'COIN' }">金币全额支付</c:if>
					              	<c:if test="${subRefundReturn.payTypeId eq 'PRED' }">预存款全额支付 </c:if>
					              	<c:if test="${subRefundReturn.payTypeId eq 'SIMULATE' }">商城模拟支付 </c:if>
					              </li>
					              <li><span>订单总额：</span>${subRefundReturn.subMoney }</li>
					            </ul>
					          </div>
					        </div>
				          </c:if>
				          <div class="ste-but" align="center">
				           <input type="button" value="返回" class="btn-g big-btn" style="height:30px;" onclick="window.location='${contextPath}/p/refundReturnList/${subRefundReturn.applyType }'"/>
				         </div>
				         <!-- 退款详情 -->
				        </div>
			     </div>
			     <!-----------right_con 结束-------------->
			     
			    <div class="clear"></div>
			 </div>
		   
		 	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
		</div>
		<script type="text/javascript">
			$(function(){
				userCenter.changeSubTab("myreturnorder");//高亮菜单
			});
		</script>
	</body>
</html>
