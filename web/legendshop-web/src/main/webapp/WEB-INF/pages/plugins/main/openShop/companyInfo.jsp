<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
  <title>公司信息-${systemConfig.shopName}</title>
  <meta name="keywords" content="${systemConfig.keywords}"/>
  <meta name="description" content="${systemConfig.description}" />
  <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/templets/css/errorform.css'/>" />

</head>

<body>
<div id="doc">
   
   <div id="hd">
   	 <%@ include file="../home/top.jsp" %>    
   </div>
   <div id="bd">
     
       <div class="yt-wrap" style="padding:0px 0 20px;">
            <div class="seller-cru" style="width:1190px;margin: 20px auto 20px;">
              <p>
                您的位置&gt;
                <a href="#">首页</a>&gt;
                <span style="color: #e5004f;">免费开店</span>
              </p>
            </div>
            <div class="pagetab2">
               <ul>           
                 <li><span>签订入驻协议</span></li>
                 <li class="on"><span>公司信息提交</span></li>
                 <li><span>店铺信息提交</span></li>   
                 <li><span>审核进度查询</span></li>   
               </ul>       
            </div>
            <div class="main">
                <div class="settled_box">
                  <h3>
                    <div class="settled_step">
                      <ul>
                        <li class="step5"><a href="#">入驻须知</a></li>
                        <li class="step7"><a href="#">公司信息认证</a></li>
                        <li class="step3"><a href="#">店铺信息认证</a></li>
                        <li class="step4"><a href="#">等待审核</a></li>
                      </ul>
                    </div>
                    <span class="settled_title">联系方式</span></h3>
                  <div class="settled_white">
                    <form:form  action="${contextPath}/p/saveCompanyInfo" method="post" id="companyInfo"  novalidate="novalidate"  enctype="multipart/form-data">
                     <input type="hidden" id="shopId" name="shopId" value="${shopCompanyDetail.shopId}">
                    <div class="settled_white_box">
                        <div class="settled_warning"><span>以下所需要上传电子版资质仅支持JPG、GIF、PNG格式的图片，大小不超过1M，且必须加盖企业彩色公章</span></div>
                        <div class="settled_form">
                          <h4>营业执照信息</h4>
                          <table class="settled_table" border="0" cellpadding="0" cellspacing="0" width="630" style="font-size: 12px;">
                            <tbody><tr>
                              <td align="right" valign="top" width="160"><span class="sred_span">
                                公司名称：</span><strong class="sred">*</strong></td>
                              <td><input class="size200" type="text" id="companyName" name="companyName" value="${shopCompanyDetail.companyName}" maxlength="100"><em></em></td>
                            </tr>
                            <tr>
                              <td align="right" valign="top"><span class="sred_span">注册号(营业执照号)：</span><strong class="sred">*</strong></td>
                              <td><input class="size200" type="text" id="licenseNumber" name="licenseNumber" value="${shopCompanyDetail.licenseNumber}" maxlength="50"><em></em></td>
                            </tr>
                            <tr>
                              <td align="right" valign="top"><span class="sred_span">法人身份证电子版：</span><strong class="sred">*</strong></td>
                              <td>
                                <span class="upload_button"><input type="file" oldFile="${shopCompanyDetail.legalIdCard}" id="legalIdCardPic" name="legalIdCardPic"><em></em> </span>
                              </td>
                            </tr>
                             <c:if test="${not empty shopCompanyDetail.legalIdCard}">
                            <tr>
                              <td></td>
                              <td>
                                <a target="_blank" href="<ls:photo item='${shopCompanyDetail.legalIdCard}' />"><img width="200" height="120" src="<ls:photo item='${shopCompanyDetail.legalIdCard}' />"></a>
                              </td>
                            </tr>  
                            </c:if>     
                            <tr>
                              <td align="right" valign="top"><span class="sred_span">营业执照所在地：</span><strong class="sred">*</strong></td>
                              <td>
                                   <select class="combox sele" id="licenseProvinceid" name="licenseProvinceid" requiredTitle="true" childNode="licenseCityid" selectedValue="${shopCompanyDetail.licenseProvinceid}"
									retUrl="${contextPath}/common/loadProvinces">
								</select> <select class="combox sele" id="licenseCityid" name="licenseCityid" requiredTitle="true" selectedValue="${shopCompanyDetail.licenseCityid}" showNone="false" parentValue="${shopCompanyDetail.licenseProvinceid}"
									childNode="licenseAreaid" retUrl="${contextPath}/common/loadCities/{value}">
								</select> <select class="combox sele" id="licenseAreaid" name="licenseAreaid" requiredTitle="true" selectedValue="${shopCompanyDetail.licenseAreaid}" showNone="false" parentValue="${shopCompanyDetail.licenseCityid}"
									retUrl="${contextPath}/common/loadAreas/{value}">
								</select>
								<em style="margin-left: 5px;"></em>
                               </td>
                        </tr>
                        <tr>
                          <td align="right" valign="top"><span class="sred_span">营业执照详细地址：</span><strong class="sred">*</strong></td>
                          <td><input class="size200" type="text" id="licenseAddr" name="licenseAddr" value="${shopCompanyDetail.licenseAddr}" maxlength="200"><em></em></td>
                        </tr>
                         <tr>
                          <td align="right" valign="top"><span class="sred_span">成立日期：</span><strong class="sred">*</strong></td>
                          <td><input class="size200" style="height:18px;" readonly="readonly"  pattern="yyyy-MM-dd HH:mm:ss" type="text" id="licenseEstablishDate" name="licenseEstablishDate" value="<fmt:formatDate value='${shopCompanyDetail.licenseEstablishDate}' pattern='yyyy-MM-dd HH:mm:ss' />"><em></em></td>
                        </tr>
                        <tr>
                          <td align="right" valign="top"><span class="sred_span">营业期限：</span><strong class="sred">*</strong></td>
                          <td><input class="size200" style="height:18px;width:100px;" readonly="readonly"  pattern="yyyy-MM-dd HH:mm:ss" type="text" style="width:100px;" id="licenseStartDate" name="licenseStartDate" value="<fmt:formatDate value='${shopCompanyDetail.licenseStartDate}' pattern='yyyy-MM-dd HH:mm:ss' />">&nbsp;—&nbsp;
                          <input class="size200 Wdate" style="height:18px;width:100px;" readonly="readonly"  pattern="yyyy-MM-dd HH:mm:ss" type="text" style="width:100px;" id="licenseEndDate" name="licenseEndDate" value="<fmt:formatDate value='${shopCompanyDetail.licenseEndDate}' pattern='yyyy-MM-dd HH:mm:ss' />"><em style="margin-left: 24px;"></em></td>
                        </tr>
                        <tr>
                          <td align="right" valign="top"><span class="sred_span">注册资本：</span><strong class="sred">*</strong></td>
                          <td><input class="size200" type="text" name="licenseRegCapital" id="licenseRegCapital" value="${shopCompanyDetail.licenseRegCapital}" maxlength="5" style="width:172px;">&nbsp;万元<em></em></td>
                        </tr>
                        <tr>
                          <td align="right" valign="top"><span class="sred_span">经营范围：</span><strong class="sred">*</strong></td>
                          <td><textarea class="settled_text" name="licenseBusinessScope" id="licenseBusinessScope" >${shopCompanyDetail.licenseBusinessScope}</textarea><em style="margin-left: 27px;"></em></td>
                        </tr>
                        <tr>
                          <td align="right" valign="top"><span class="sred_span">营业执照副本电子版：</span><strong class="sred">*</strong></td>
                          <td>
                           <span class="upload_button"><input type="file" oldFile="${shopCompanyDetail.licensePic}" id="licensePics" name="licensePics"><em></em></span>
                           
                          </td>
                        </tr> 
                         <c:if test="${not empty shopCompanyDetail.licensePic}">
                         	<tr>
                         		<td></td>
                         		<td><a target="_blank" href="<ls:photo item='${shopCompanyDetail.licensePic}' />"><img width="200" height="120" src="<ls:photo item='${shopCompanyDetail.licensePic}' />"></a></td>
                         	</tr>
                         </c:if>         
                        <tr>
                          <td align="right" valign="top"><span class="sred_span">公司所在地：</span><strong class="sred">*</strong></td>
                          <td>
                                        <select class="combox sele" id="companyProvinceid" name="companyProvinceid" requiredTitle="true" childNode="companyCityid" selectedValue="${shopCompanyDetail.companyProvinceid}"
									retUrl="${contextPath}/common/loadProvinces">
								</select> <select class="combox sele" id="companyCityid" name="companyCityid" requiredTitle="true" selectedValue="${shopCompanyDetail.companyCityid}" showNone="false" parentValue="${shopCompanyDetail.companyProvinceid}"
									childNode="companyAreaid" retUrl="${contextPath}/common/loadCities/{value}">
								</select> <select class="combox sele" id="companyAreaid" name="companyAreaid" requiredTitle="true" selectedValue="${shopCompanyDetail.companyAreaid}" showNone="false" parentValue="${shopCompanyDetail.companyCityid}"
									retUrl="${contextPath}/common/loadAreas/{value}">
								</select>
								<em style="margin-left: 5px;"></em>
                           </td>
                        </tr>
                        <tr>
                          <td align="right" valign="top"><span class="sred_span">公司详细地址：</span><strong class="sred">*</strong></td>
                          <td><input class="size200" name="companyAddress" id="companyAddress" value="${shopCompanyDetail.companyAddress}" type="text"><em></em></td>
                        </tr>
                        <tr>
                          <td align="right" valign="top"><span class="sred_span">公司电话：</span><strong class="sred">*</strong></td>
                          <td><input class="size200" name="companyTelephone" id="companyTelephone" value="${shopCompanyDetail.companyTelephone}" type="text">（020-12345678）<em></em></td>
                        </tr>
                        <!-- <tr>
                          <td align="right" valign="top"><span class="sred_span">公司紧急联系人：</span><strong class="sred">*</strong></td>
                          <td><input name="license_c_contact" class="size200" type="text"><em></em></td>
                        </tr>
                        <tr>
                          <td align="right" valign="top"><span class="sred_span">公司紧急联系人手机：</span><strong class="sred">*</strong></td>
                          <td><input class="size200" id="license_c_mobile" type="text"><em></em></td>
                        </tr> -->
                        <tr>
                          <td align="right" valign="top"><span class="sred_span">公司人数：</span><strong class="sred">*</strong></td>
                          <td><select id="companyNum" name="companyNum" class="sele">
									<ls:optionGroup type="select" required="false" cache="true" beanName="COMPANY_NUM" selectedValue="${shopCompanyDetail.companyNum}" />
							</select><em></em>
						 </td>
                        </tr>
                        <tr>
                          <td align="right" valign="top"><span class="sred_span">公司行业：</span><strong class="sred">*</strong></td>
                          <td><select id="companyIndustry" name="companyIndustry" class="sele">
										<ls:optionGroup type="select" required="false" cache="true" beanName="COMPANY_INDUSTRY" selectedValue="${shopCompanyDetail.companyIndustry}" />
								</select><em></em>
						 </td>
                        </tr>
                        <tr>
                          <td align="right" valign="top"><span class="sred_span">公司性质：</span><strong class="sred">*</strong></td>
                          <td><select id="companyProperty" name="companyProperty" class="sele">
										<ls:optionGroup type="select" required="false" cache="true" beanName="COMPANY_PROPERTIES" selectedValue="${shopCompanyDetail.companyProperty}" />
								</select><em></em>
						 </td>
                        </tr>
                      </tbody>
                      </table>
                      <span class="license">
                        <b><a href="<ls:templateResource item='/resources/templets/images/IDcard.jpg'/>" target="_blank"><em><img src="<ls:templateResource item='/resources/templets/images/IDcard.jpg'/>" width="310px" height="194"></em>查看大图</a></b>
                      </span>
                      <span class="license license_bottom">
                        <b><a href="<ls:templateResource item='/resources/templets/images/business-licences.jpg'/>" target="_blank"><em><img src="<ls:templateResource item='/resources/templets/images/business-licences.jpg'/>" width="310px" height="220px"></em>查看大图</a></b>
                      </span>
                        </div>
                       </div>
                      <div class="settled_bottom">
                        <span>
                          <a href="javaScript:void(0);" onclick="shopPrevious(1)" class="up_step_btn">上一步</a>
                          <a href="javaScript:void(0);" onclick="saveCompanyInfo()" class="settled_btn"><em>下一步，完善营业信息</em></a>
                        </span>
                      </div>
                    </form:form>
                  </div>
                </div>
            </div>
        </div>
   </div>
   
<%@ include file="../home/bottom.jsp" %>

</div>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>"  type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/openshop.js'/>"></script>
<script type="text/javascript">
	$("select.combox").initSelect();
		$(document).ready(function(){
		var type=$("#codeType").val();
			if(type==1){
				$("#old").show(500);
				$("#new").hide(500);
				$("#code1").attr("checked","checked");
				$("#socialCreditCode").addClass("ignore");
				$("#taxpayerId").removeClass("ignore");
				$("#taxRegistrationNum").removeClass("ignore");
				$("#taxRegistrationNumEPic").removeClass("ignore");
			}
			if(type==2){
				$("#new").show(500);
				$("#old").hide(500);
				$("#code2").attr("checked","checked");
				$("#taxpayerId").addClass("ignore");
				$("#taxRegistrationNum").addClass("ignore");
				$("#taxRegistrationNumEPic").addClass("ignore");
				$("#socialCreditCode").removeClass("ignore");
			}
			
			    laydate.render({
				   elem: '#licenseEstablishDate',
				   type:'datetime',
				   calendar: true,
				   theme: 'grid',
				   trigger: 'click'
			     });
				   
				 laydate.render({
				   elem: '#licenseStartDate',
				   type:'datetime',
				   calendar: true,
				   theme: 'grid',
				   trigger: 'click'
			    });
			
				laydate.render({
				   elem: '#licenseEndDate',
				   type:'datetime',
				   calendar: true,
				   theme: 'grid',
				   trigger: 'click'
				 });
	});
</script>
</body>
</html>
