<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<jsp:useBean id="nowTime" class="java.util.Date" />
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
	<c:set var="auctions" value="${list}"></c:set>
	<title>预售活动列表-${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/commonPage.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/auction.css'/>" rel="stylesheet"/>
</head>
<body>
	<div id='auc_page'>
		<div id="doc">
			<div id="hd">
				<%@ include
					file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
			</div>
			<div id="bd">
				<div class="seller-cru" style="width:1190px;margin: 20px auto 10px;">
					<p>
						您的位置&gt;
						<a href="#">首页</a>
						&gt; <span style="color: #c6171f;">预售活动</span>
					</p>
				</div>
				<!--两栏-->
				<div class=" yt-wrap " style="padding-top:10px;">
					<!--right_con-->

					<div class="bidding-list">
						<nav id="main-nav" class="sort-bar">
							<div class="nch-sortbar-array">
								排序方式：
								<ul>
									<form:form action="${contextPath}/presell/presellProd/list" id="form1" method="post">
										<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
										<input type="hidden" id="loadType" name="loadType" value="${searchParams.loadType}" />
									</form:form>
									<li id="default" ${(empty searchParams.orders) or(searchParams.orders== 'pre_sale_start,asc') ?'class="selected"':''}>
										<a title="默认排序" href="javascript:void(0);">默认排序</a>
									</li>
									<li id="startTime" ${searchParams.orders== 'pre_sale_start,desc' ||  searchParams.orders== 'pre_sale_start,asc'?'class="selected"':''}>
										<a title="点击按开始时间从近到远排序" href="javascript:void(0);"
											<c:choose><c:when test="${searchParams.orders=='pre_sale_start,desc'}">class='desc'</c:when><c:when test="${searchParams.orders=='pre_sale_start,asc'}">class='asc'</c:when></c:choose>>
											开始时间<i></i>
										</a>
									</li>
									<li id="floorPrice" ${searchParams.orders== 'pre_price,desc' ||  searchParams.orders== 'pre_price,asc'?'class="selected"':''}>
										<a title="点击按预售价格从高到低排序" href="javascript:void(0);"
											<c:choose><c:when test="${searchParams.orders=='pre_price,desc'}">class='desc'</c:when><c:when test="${searchParams.orders=='pre_price,asc'}">class='asc'</c:when></c:choose>>
											预售价格<i></i>
										</a>
									</li>
								</ul>
							</div>
						</nav>
						<!--page-->

						<div id="recommend" class="auction" style="display: block;border: 0">
							<div class="pro-list">
								<c:if test='${empty list}'>
									<div class="selection_box search_noresult">
										<p class="search_nr_img">
											<img src="<ls:templateResource item='/resources/templets/images/not_result.png'/>">
										</p>
										<p class="search_nr_desc">
											没有找到
											<c:if test="${not empty searchParams.keyword}">与“<span>${searchParams.keyword}</span>”</c:if>
											相关的活动信息
										</p>
									</div>
								</c:if>
								<ul class="clear">
									<c:forEach var="auction" items="${list}">
									<li id="id_${auction.id}" class="falg">
										<a href="${contextPath}/presell/views/${auction.id}" class="pro-img">
										<c:choose>
											<c:when test="${not empty auction.skuPic}"><img src="<ls:images item='${auction.skuPic}' scale='1'/>" style="vertical-align: middle;" /></c:when>
											<c:otherwise><img src="<ls:images item='${auction.prodPic}' scale='1'/>" style="vertical-align: middle;" /></c:otherwise>
										</c:choose>
										</a>
										<a href="${contextPath}/presell/views/${auction.id}" class="pro-name">
											<p>${auction.schemeName}</p>
<%-- 											<span>${auction.auctionsDesc }</span> ${auction.biddingNumber } --%>
										</a>
										<div class="pro-price clear" biddingNumber="">
											<p>
												<span class="p-price">￥<b>${auction.prePrice}</b>
												</span>
											</p>
										</div>
										<div>
											<p>
												预售时间:<fmt:formatDate value="${auction.preSaleStart}" pattern="yyyy-MM-dd HH-mm" />
											</p>
										</div>
										</li>
									</c:forEach>
								</ul>
							</div>
<!-- 							group end -->
						</div>
					</div>
<!-- 					right_con end -->
					<!--分页-->
					<div class="clear"></div>
					<div style="margin-top:10px;" class="page clearfix">
						<div class="p-wrap">
							<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
						</div>
					</div>

				</div>
				<!--两栏end-->
			</div>
		</div>

		<%@ include
			file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/presellProdList.js'/>"></script>
	<script>
	 	var contextPath="${contextPath}";
	 	var _page = "${_page}";
	 	var loadType = "${searchParams.loadType}";
	</script>
</body>
</html>