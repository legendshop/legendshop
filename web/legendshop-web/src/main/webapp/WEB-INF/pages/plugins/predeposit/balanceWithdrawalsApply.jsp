<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>预存款申请提现 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/pdCashManager${_style_}.css'/>" rel="stylesheet" />
	 
</head>
<style>
select, .select{
	border:1px solid #a9a9a9;
}
</style>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
     <%@ include file="/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp" %>
    <!-----------right_con-------------->
     <div class="right_con">
<div>
	<div class="o-mt">
		<h2>账户信息</h2>
	</div>
	<div class="pagetab2">
		<ul>
			<li><span><a href="<ls:url address='/p/predeposit/account_balance'/>">账户余额</a> </span></li>
			<li ><span><a href="<ls:url address='/p/predeposit/recharge_detail'/>">充值明细</a> </span></li>
			<li ><span><a href="<ls:url address='/p/predeposit/balance_withdrawal'/>">余额提现</a> </span></li>
			<li class="on"><span>申请提现</span></li>
		</ul>
	</div>

		<div class="alert alert-success">
			<h4>操作提示：</h4>
			<ul>
			   <!--  <li>1.必须绑定“绑定邮箱”或“绑定手机”其中一种验证方式,并且必须设置支付密码</li> -->
				<li>1. 请选择“绑定邮箱”或“绑定手机”方式其一作为安全校验码的获取方式并正确输入。</li>
				<li>2. 如果您的邮箱已失效，可以 <a
					href="<ls:url address='/p/establishIdentity/toUpdateMobile/2'/>">绑定手机</a>
					后通过接收手机短信完成验证。</li>
				<li>3. 如果您的手机已失效，可以 <a
					href="<ls:url address='/p/establishIdentity/toUpdateMail/1'/>">绑定邮箱</a>后通过接收邮件完成验证。</li>
				<li>4. 请正确输入下方图形验证码，如看不清可点击图片进行更换，输入完成后进行下一步操作。</li>
				<li>5. 收到安全验证码后，请在3分钟内完成验证。</li>
			</ul>
		</div>

	<div class="ncm-default-form">
		<form:form  action="#" id="auth_form" method="post">
		   <dl>
				<dt>
					可提现余额：
				</dt>
				<dd>
					<label style="font-weight: bold;font-family: '微软雅黑';color: #e5004f;font-size: 16px;" id="available_cash">
					  <fmt:formatNumber var="pre_amount" value="${availablePredeposit}" type="currency" pattern="0.00"></fmt:formatNumber>
					  ${pre_amount}
					</label>
				</dd>
			</dl>
			
			<dl>
				<dt>
					<i class="required">*</i>提现方式：
				</dt>
				<dd style="border-color:#a9a9a9;">
					<select id="bankName" name="bankName">
					   <option selected="selected" value="">--请选择提现方式--</option>
						<option value="支付宝">支付宝</option>
					</select>
					<input type="hidden" value="" id="region" name="region" >
				</dd>
			</dl>

			<dl>
				<dt>
					<i class="required">*</i>提款帐号：
				</dt>
				<dd>
					<input type="text" value="" name="bankNo" maxlength="30" id="bankNo" class="text w200" style="width:391px;border-color:#a9a9a9;">
				</dd>
			</dl>
			
			<dl>
				<dt>
					<i class="required">*</i>开户姓名：
				</dt>
				<dd>
					<input type="text" value="" name="bankUser" id="bankUser" maxlength="20" class="text" style="width:391px;border-color:#a9a9a9;">
				</dd>
			</dl>
			
			<dl>
				<dt>
					<i class="required">*</i>提现金额：
				</dt>
				<dd>
					<input type="text" maxlength="8" class="text w50" name="amount" style="width:391px;border-color:#a9a9a9; ">
					   <!-- <em class="add-on"><i class="icon-renminbi">元</i> </em> -->
					   <label class="error" generated="true" for="amount"></label>
					 <span></span>
				</dd>
			</dl>

			<dl>
				<dt>
					备注：
				</dt>
				<dd>
					<textarea class="textarea w400 h100" maxlength="100" rows="3" name="userNote" placeholder="请输入备注信息,内容不超过100字符"></textarea>
				</dd>
			</dl>
			
			<dl>
				<dt>
					<i class="required">*</i>图形验证码：
				</dt>
				<dd>
					<input type="text" autocomplete="off" size="10" maxlength="4" id="captcha" class="text" name="captcha" style="border-color:#a9a9a9;">
				 	<img border="0" class="ml5 vm" id="codeimage" name="codeimage" src="<ls:templateResource item='/validCoderRandom'/>" >
				 	<a onclick="javascript:changeRandImg('${contextPath}')" class="ml5 blue" href="javascript:void(0)">看不清？换张图</a> 
				 	<label class="error" generated="true" for="captcha"></label>
				</dd>
			</dl>

			<dl>
				<dt>
					<i class="required">*</i>选择认证方式：
				</dt>
				<dd>
					<p>
						<select id="authType" name="authType">
						 <option value="">---请选择认证方式---</option>
						  <c:if test="${not empty userPhone}">
						    <option value="phone">手机 [${userPhone}]</option>
						  </c:if>
						  <c:if test="${not empty userEmail}">
						   <option value="email">邮箱 [${userEmail}]</option>
						  </c:if>
						</select> 
						<a class="btn-r ml5" id="send_auth_code" href="javascript:void(0);" style="margin-top:3px;">
							<span style="display: none;" id="sending">正在</span>
							<span class="send_success_tips" style="display: none;"><strong class="mr5" id="show_times"></strong>秒后再次</span>
							获取安全验证码
						</a> 
					</p>
					<p class="send_success_tips hint mt10" style="display: none;">
						“安全验证码”已发出，请注意查收，请在<strong>“3分种”</strong>内完成验证。
					</p>
				</dd>
			</dl>
			
			<dl>
				<dt>
					<i class="required">*</i>安全验证码：
				</dt>
				<dd>
					<input type="text" autocomplete="off" id="authCode" size="10"
						name="authCode" value="" maxlength="6" class="text" style="border-color:#a9a9a9;"> <label
						class="error" generated="true" for="email"></label>
				</dd>
			</dl>
			<dl class="bottom">
				<dt>&nbsp;</dt>
				<dd>
					 <input style="height:30px;width:80px;" type="button"  value="提现申请" class="submit btn-r">
				</dd>
			</dl>
		</form:form>
	</div>

	<div class="clear"></div>
</div>
</div>
    <!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>" type="text/javascript"></script>
<script type="text/javascript">
    $('.send_success_tips').hide();
    var contextPath = '${contextPath}';
    var minAmount='${minAmount}';
</script> 
<script src="<ls:templateResource item='/resources/templets/js/withdrawals.js'/>" type="text/javascript"></script>

</body>
</html>
			
