<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>预存款账户余额 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/pdCashManager${_style_}.css'/>" rel="stylesheet" />
	 
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
     <%@ include file="/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp" %>
    <!-----------right_con-------------->
     <div class="right_con">
<div>
	<div class="o-mt">
		<h2>账户信息</h2>
	</div>
	<div class="pagetab2">
		<ul>
			<li class="on"><span>账户余额</span></li>
			<li ><span><a href="<ls:url address='/p/predeposit/recharge_detail'/>">充值明细</a> </span></li>
			<li ><span><a href="<ls:url address='/p/predeposit/balance_withdrawal'/>">余额提现</a> </span></li>
			<li ><span><a href="<ls:url address='/p/predeposit/balance_hoding'/>">冻结中余额</a> </span></li>
		</ul>
	</div>

	<div>
		<%-- <div class="tabmenu">
			<a class="ncbtn ncbtn-yellow" title="在线充值" href="<ls:url address='/p/predeposit/recharge_add'/>"
				style="right: 83px;width:53px;">在线充值</a>
			 <a class="ncbtn " href="<ls:url address='/p/predeposit/predepositApply'/>" style="right: -5px;width:53px;">
			 申请提现</a> 
		</div> --%>
		<div class="alert">
			<span class="mr30">可用金额：<strong class="mr5 red"
				style="font-size: 18px;"><fmt:formatNumber value="${predeposit.availablePredeposit}" pattern="#,##0.00#"/></strong>元</span><span>冻结金额：<strong
				class="mr5 blue" style="font-size: 18px;"><fmt:formatNumber value="${predeposit.freezePredeposit}" pattern="#,##0.00#"/></strong>元</span>
			<span style="float:right;margin-top:2px;">
				<a class="btn-r" title="在线充值" href="<ls:url address='/p/predeposit/recharge_add'/>"
					style="right: 83px;">在线充值</a>
				 <a class="btn-r" href="<ls:url address='/p/predeposit/predepositApply'/>" style="right: -5px;">
				 申请提现</a> 
			</span>
		</div>
		
		<div id="recommend" class="recommend" style="display: block;">
		<table class="ncm-default-table sold-table" cellpadding="0" cellspacing="0">
			<thead style="background: #f9f9f9 !important;">
				<tr>
					<th width="183" >创建时间</th>
					<th width="100" >收入(元)</th>
					<th width="100" >支出(元)</th>
					<th width="187" >流水号</th>
					<th width="250" style="border-right: 1px solid #e4e4e4;">说明</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
		           <c:when test="${empty requestScope.list}">
		              <tr>
					 	 <td colspan="20">
					 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
					 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
					 	 </td>
					 </tr>
		           </c:when>
		           <c:otherwise>
		              <c:forEach items="${predeposit.pdCashLogs.resultList}" var="pred" varStatus="status">
					      <tr>
					      <td><fmt:formatDate value="${pred.addTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					      <td>
					         <c:choose>
								<c:when test="${pred.amount>0}">
								   <fmt:formatNumber value="${pred.amount}" pattern="#,##0.00#"/>
								</c:when>
								<c:otherwise>
								   0.00
								</c:otherwise>
							 </c:choose>
					      </td>
					      <td>
					       <c:choose>
								<c:when test="${pred.amount<0}">
								   <fmt:formatNumber value="${pred.amount}" pattern="#,##0.00#"/>
								</c:when>
								<c:otherwise>
								   0.00
							</c:otherwise>
						  </c:choose>
					      </td>
					      <td>
					        ${pred.sn}
					      </td>
					      <td>
					         ${pred.logDesc}
					      </td>
					      </tr>
					   </c:forEach>
		           </c:otherwise>
	           </c:choose>
			</tbody>
		</table>
	   </div>
	</div>
		<!-- 页码条 -->
		<div class="page clearfix">
			<div class="p-wrap">
					<span class="p-num">
						<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
					</span>
			 </div>
		</div>
		
</div>
</div>
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
  <script type="text/javascript">
	 function pager(curPageNO){
	     window.location.href="${contextPath}/p/predeposit/account_balance?curPageNO="+curPageNO;
     }
	$(document).ready(function() {
		userCenter.changeSubTab("mydeposit");
  	});
</script>  
</body>
</html>
			