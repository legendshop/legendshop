<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

   <!----------------评论------------------------->
<div id="i-comment">
	<div class="rate">
		<strong><fmt:formatNumber type="percent" value="${prodCommCategory.highRate}" /></strong>
		<br> 好评度
	</div>
	<div class="percent" style="border-right:none">
		<dl>
			<dt>好评</dt>
			<dd class="d1">
				<div style="width: ${prodCommCategory.highRate * 150}px;"></div>
			</dd>
			<dd class="d2">
				<fmt:formatNumber type="percent" value="${prodCommCategory.highRate}" />
			</dd>
		</dl>
		<dl>
			<dt>中评</dt>
			<dd class="d1">
				<div style="width: ${prodCommCategory.mediumRate * 150}px;"></div>
			</dd>
			<dd class="d2">
				<fmt:formatNumber type="percent" value="${prodCommCategory.mediumRate}" />
			</dd>
		</dl>
		<dl>
			<dt>差评</dt>
			<dd class="d1">
				<div style="width: ${prodCommCategory.lowRate * 150}px;"></div>
			</dd>
			<dd class="d2">
				<fmt:formatNumber type="percent" value="${prodCommCategory.lowRate}" />
			</dd>
		</dl>
	</div>
<!-- 	<div class="btns"> -->
<!-- 		<div>我购买过此商品</div> -->
<%-- 		<a class="btn-comment" href="${contextPath}/p/prodcomment" target="_blank"> --%>
<!-- 			我要评价 -->
<!-- 		</a> -->
		<%--<div>发评价拿积分</div>--%>
<!-- 	</div> -->
	<div class="clear"></div>
</div>

<div class="pagetab" id="commentTab" name="commentTab">
	<ul>
		<li class="on" condition="all">
			<span>全部评价<strong id="cnum0">(${prodCommCategory.total})</strong></span>
		</li>
		<li condition="good">
			<span>好评<strong id="cnum1">(${prodCommCategory.high})</strong></span>
		</li>
		<li condition="medium">
			<span>中评<strong id="cnum2">(${prodCommCategory.medium})</strong></span>
		</li>
		<li condition="poor">
			<span>差评<strong id="cnum3">(${prodCommCategory.low})</strong></span>
		</li>
		<li condition="photo">
			<span>晒图<strong id="cnum3">(${prodCommCategory.photo})</strong></span>
		</li>
		<li condition="append">
			<span>追加<strong id="cnum3">(${prodCommCategory.append})</strong></span>
		</li>
	</ul>
</div>

<input type="hidden" id="condition" name="condition" />
<div id="comments-list" class="mc" style="display: block;">
	<%@include file='prodcommentlist.jsp'%>
</div>

<!--tabcon end-->
<div class="clear"></div>

<script language="javascript">

	$(document).ready(function() {
		$("#commentTab ul li").click(function() {
			$("#commentTab ul li").each(function(i) {
				$(this).removeClass("on");
			});
			$(this).addClass("on");
			var condition = $(this).attr("condition");
			queryProdCommentList("${prodCommCategory.prodId}", condition, 1);
		});
	});

	function queryProdCommentList(prodId, condition, curPageNO) {
		jQuery("#prodCommentCurPageNO").val(curPageNO);
		jQuery("#condition").val(condition);
		var data = {
			"curPageNO" : curPageNO,
			"prodId": prodId, 
			"condition" : condition
		};
		jQuery.ajax({
			url : "${contextPath}/productcomment/listcontent",
			data : data,
			type : "GET",
			async : true, //默认为true 异步   
			error : function(jqXHR, textStatus, errorThrown) {
				// alert(textStatus, errorThrown);
			},
			success : function(result) {
				$("#comments-list").html(result);
			}
		});
	}

	function commentPager(curPageNO) {
		var score = $("#condition").val();
		queryProdCommentList('${prodCommCategory.prodId}', score, curPageNO);
	}
</script>
