<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<div class="clear"></div>
<c:set var="advList" value="${floor.floorItems.RA}"></c:set>
<div class="yt-wrap bf_floor_new clearfix">
	<div class="adv-eight">
		<ul>
			<c:forEach items="${advList}" var="adv" varStatus="index" end="7">
				<li><a target="_blank" title="${adv.title}"
					href="${adv.linkUrl}"> <img class="lazy" src="${contextPath}/resources/common/images/loading1.gif" alt="${adv.title}" width="297" height="80"
                                                data-original="<ls:photo item='${adv.picUrl}'/>">
				</a></li>
			</c:forEach>
		</ul>
	</div>
</div>
<!--首页楼层样式 横条广告×8-->