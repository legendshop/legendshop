<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/public.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/decorate.css'/>" rel="stylesheet" />
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script src="${contextPath}/resources/templets/js/multishop/decorateModule.js" type="text/javascript"></script>

<div class="fiit_module_layout">
   <input type="hidden" id="layoutSeq" value="${layoutParam.seq}">
  <!--自定义模块-->
  <div style="display:;" class="fiit_module_box">
        <h2>请选择您想要编辑的模块类型</h2>
        <div class="fiit_notice"><i></i>由于布局模块大小限制，部分类型不可选择</div>
          <ul>
       		 <li>
       		    <a type="loyout_module_nav" layout="${layoutParam.layout}" mark="${layoutParam.layoutId}" onclick="module_set(this)" href="javascript:void(0);">
       		      <span class="img">
       		       <img width="88" height="88" src="<ls:templateResource item='/resources/templets/images/model_menu.png'/>">
       		      </span>
       		      <span class="name">导航模块</span>
       		    </a>
       		    <c:if test="${layoutParam.layout!='layout0' && layoutParam.layout!='layout1' }">
                        <span class="gray_span"><i></i><b></b></span>
				 </c:if>
       		 </li>
       		 
             <li>
                 <a type="loyout_module_banner" onclick="banner_module_set(this);" layout="${layoutParam.layout}" div="${layoutParam.layoutDiv}" divId="${layoutParam.divId}"  mark="${layoutParam.layoutId}" href="javascript:void(0);">
                    <span class="img">
                    <img width="88" height="88" src="<ls:templateResource item='/resources/templets/images/model_pic.png'/>">
                   </span>
                   <span class="name">幻灯模块</span>
                 </a>
                 <c:if test="${layoutParam.layoutDiv=='div1'}">
                        <span class="gray_span"><i></i><b></b></span>
				 </c:if>
             </li>
             
		 	<li>
			  <a type="loyout_module_category" onclick="module_set(this)" layout="${layoutParam.layout}" div="${layoutParam.layoutDiv}" divId="${layoutParam.divId}"  mark="${layoutParam.layoutId}"   href="javascript:void(0);">
			    <span class="img">
			       <img width="88" height="88" src="<ls:templateResource item='/resources/templets/images/model_ul.png'/>">
				</span><span class="name">分类模块</span>
			  </a>
			    <c:if test="${layoutParam.layoutDiv !='div1'}">
                        <span class="gray_span"><i></i><b></b></span>
				 </c:if>
			</li>
   	<li>
			  <a type="loyout_module_hot_prod" layout="${layoutParam.layout}" onclick="module_set(this)" div="${layoutParam.layoutDiv}" divId="${layoutParam.divId}"  mark="${layoutParam.layoutId}"   href="javascript:void(0);"><span class="img"><img
						width="88" height="88"
						src="<ls:templateResource item='/resources/templets/images/model_dl.png'/>">
				</span>
				<span class="name">热销排行</span>
			   </a>
			   <c:if test="${layoutParam.layoutDiv !='div1'}">
                   <span class="gray_span"><i></i><b></b></span>
			   </c:if>
			</li>

			<li><a type="loyout_module_shopinfo" onclick="module_set(this)"  layout="${layoutParam.layout}" div="${layoutParam.layoutDiv}" divId="${layoutParam.divId}"  mark="${layoutParam.layoutId}"   href="javascript:void(0);"><span class="img"><img
						width="88" height="88"
						src="<ls:templateResource item='/resources/templets/images/model_info.png'/>">
				    </span><span class="name">店铺信息</span>
			      </a>
			      
			     <c:if test="${layoutParam.layoutDiv !='div1'}">
                   <span class="gray_span"><i></i><b></b></span>
			   </c:if>
			</li>
			<li><a type="loyout_module_custom_prod" onclick="prod_module_set(this);" layout="${layoutParam.layout}" div="${layoutParam.layoutDiv}" divId="${layoutParam.divId}"  mark="${layoutParam.layoutId}"    href="javascript:void(0);"><span class="img"><img
						width="88" height="88"
						src="<ls:templateResource item='/resources/templets/images/model_sale.png'/>">
				</span><span class="name">自定义商品</span>
			</a>
			   <c:if test="${layoutParam.layout =='layout0' || layoutParam.layout =='layout1' || layoutParam.layoutDiv =='div1'}">
                   <span class="gray_span"><i></i><b></b></span>
			   </c:if>
			</li>
			<li>
			 <a type="loyout_module_hot" onclick="hot_module_set(this)"   layout="${layoutParam.layout}" div="${layoutParam.layoutDiv}" divId="${layoutParam.divId}"  mark="${layoutParam.layoutId}"  
				href="javascript:void(0);"><span class="img"><img
						width="88" height="88"
						src="<ls:templateResource item='/resources/templets/images/model_hot.png'/>">
				</span>
				<span class="name">热点模块</span>
			 </a>
			</li>

			<li>
			  <a type="loyout_module_custom_desc" layout="${layoutParam.layout}" div="${layoutParam.layoutDiv}" divId="${layoutParam.divId}"  mark="${layoutParam.layoutId}" onclick="content_module_set(this);"  href="javascript:void(0);"><span class="img"><img
						width="88" height="88"
						src="<ls:templateResource item='/resources/templets/images/model_self.png'/>">
				</span><span class="name">自定义模块</span>
			  </a>
			</li> 
		</ul>
       </div>
</div>
 <script type="text/javascript">
	var contextPath = '${contextPath}';
	var photoPath ="<ls:photo item='' />";
   </script>