<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>申请推广员</title>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/applyPromoter${_style_}.css'/>" rel="stylesheet" />
	<style type="text/css">
		.pm-ts{
			color: #666;
			overflow: hidden;
			height: 32px;
			line-height: 32px;
		}
	</style>
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;">     
       <%@include file='/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp'%>
    <!-----------right_con-------------->
     <div class="right_con">
     
         <div class="o-mt"><h2>申请成为推广员</h2></div>
         <div class="pm-ts" style="margin-left:20px;">温馨提示：成为推广员之后, 针对设置了分佣返点的商品在用户购买后给推广员计算佣金。</div>
         <div class="pagetab2">
                 <ul>           
                    <li class="on" id="favoriteShopList"><span>申请推广</span></li>
                 </ul>       
         </div>

         <div class="apply-promoter">
           <div class="wri-app">
             <form:form action="${contextPath}/p/promoter/submitPromoter" id="form1" method="post">
                 <input type="hidden" value="${userCommis.userId}" id="userId" name="userId"/>
                 <input type="hidden" value="${originDistSet.chkRealName}" id="chkRealName" name="chkRealName"/>
                 <input type="hidden" value="${originDistSet.chkMail}" id="chkMail" name="chkMail"/>
                 <input type="hidden" value="${originDistSet.chkMobile}" id="chkMobile" name="chkMobile"/>
                 <input type="hidden" value="${originDistSet.chkAddr}" id="chkAddr" name="chkAddr"/>
             
	             <ul>
	               <c:if test="${originDistSet.chkRealName eq 1}">
	                    <li><span>真实姓名：</span><input type="text" id="realName" name="realName" maxlength="20" value="${userDetail.realName}"><label></label></li>
	               </c:if>
	               
<%--	               <c:if test="${originDistSet.chkMail eq 1}">--%>
<%--	                    <li><span>电子邮箱：</span><input type="text" id="email" name="email" value="${userDetail.userMail}"><label></label></li>--%>
<%--	               </c:if>--%>
	               
	               <c:if test="${originDistSet.chkMobile eq 1}">
	                   <li><span>手机号码：</span><input type="text" id="mobile" name="mobile" value="${userDetail.userMobile}" readonly="readonly"><label></label></li>
	               </c:if>
	               <li><span>图形验证码：</span>
		               <input type="text" id="randNum" name="randNum" maxlength="4">
		               <img width="95" height="38" src="<ls:templateResource item='/validCoderRandom'/>" id="randImage" name="randImage">
		               <a href="javascript:void(0)" onclick="javascript:changeRandImg()" style="font-weight: bold;">换一张</a>
		               <label></label>
	               </li>
	               
	               <c:if test="${originDistSet.chkMobile eq 1}">
		               <li><span>短信验证码：</span>
		                   <input type="text" id="erifyCode" maxlength="6" name="mobileCode">
		                   <input type="button" value="获取验证码" onclick="sendMobileCode()" id="codeBtn" name="codeBtn">
		                   <label ></label>
		               </li>
	               </c:if>
	               
	               <c:if test="${originDistSet.chkAddr eq 1}">
		               <li><span>所在地区：</span>
		               <select class="combox sele"  id="provinceid"  requiredTitle="true"  childNode="cityid" selectedValue="${userDetail.provinceid}"  retUrl="${contextPath}/common/loadProvinces"  >
						</select>
						<select class="combox sele"   id="cityid" requiredTitle="true"  selectedValue="${userDetail.cityid}"   showNone="false"  parentValue="${userDetail.provinceid}" childNode="areaid" retUrl="${contextPath}/common/loadCities/{value}" >
						</select>
						<select class="combox sele"   id="areaid"  requiredTitle="true"  selectedValue="${userDetail.areaid}"    showNone="false"   parentValue="${userDetail.cityid}"  retUrl="${contextPath}/common/loadAreas/{value}">
						</select>
						 <label></label>
		               </li>
		               
		                <li><span>详细地址：</span><input style="width: 321px;" type="text" id="userAdds" name="userAdds" value="${userDetail.userAdds }"><label></label></li>
	               </c:if>
	             </ul>
	             <div class="sub"><input type="button" value="提交申请" onclick="javascript:submitTable();"></div>
             </form:form>
           </div>
           
           <!-- 填写申请 -->
           
         </div>
     </div>
    <!--right_con end-->

</div>

 <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>"  type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/common/js/randomimage.js'/>"></script> 
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/applyPromoter.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script> 	
	<script type="text/javascript">
    var contextPath="${contextPath}";
    $(document).ready(function() {
		userCenter.changeSubTab("applyPromoter");
	   $("select.combox").initSelect();
  	});
</script>
</body>
</html> 