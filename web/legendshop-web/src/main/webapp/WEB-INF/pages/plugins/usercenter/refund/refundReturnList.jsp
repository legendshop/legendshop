<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
  <head>
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>退款/退货列表-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/refund/ret-not${_style_}.css'/>" rel="stylesheet"/>
	<style>
		.mau{
			width:100px;
		}
	</style>
  </head>
<body class="graybody">
	<div id="doc">
	   <div id="bd">
	   		 <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
			 <div class=" yt-wrap" style="padding-top:10px;"> 
       	 		<%@include file='../usercenterLeft.jsp'%>
		
			    <!-----------right_con-------------->
			     <div class="right_con">
			     	 <div class="o-mt"><h2>退款/退货列表</h2></div>
				     <div class="seller-return">
				     	<div class="pagetab2">
							<ul id="listType">
								<li class="on" value="1">
									<span>退款记录</span>
								</li>
								<li value="2"><span>退货记录</span></li>
							</ul>
	        			</div>
					<!-- 退换货导航 -->
						<div class="ret-sea" style="margin: 0;">
							<form:form id="searchForm" method="GET">
								<input type="hidden" id="curPageNO" name="curPageNO"/>
								<div class="sold-ser sold-ser-no-bg clearfix">
									<div class="fr">
										<div class="item">
											申请时间：<input class="item-inp" type="text" id="startDate" name="startDate" readonly="readonly" placeholder="起始时间"/>
											 – 
											<input type="text" class="item-inp" id="endDate" name="endDate"  readonly="readonly" placeholder="结束时间"/></td>
										</div>
										<div class="item">
											处理状态：<select name="customStatus" id="refundOption" class="item-sel">
												<option value="">全部</option>
												<option value="1" <c:if test="${paramData.customStatus eq 1}">selected="selected"</c:if>>待审核</option>
												<option value="2" <c:if test="${paramData.customStatus eq 2}">selected="selected"</c:if>>已撤销</option>
												<option value="8" <c:if test="${paramData.customStatus eq 8}">selected="selected"</c:if>>同意</option>
												<option value="9" <c:if test="${paramData.customStatus eq 9}">selected="selected"</c:if>>不同意</option>
											</select>
											<select name="" style="display: none;" id="returnLOption" class="item-sel">
												<option value="">全部</option>
												<option value="1" <c:if test="${paramData.customStatus eq 1}">selected="selected"</c:if>>待审核</option>
												<option value="2" <c:if test="${paramData.customStatus eq 2}">selected="selected"</c:if>>已撤销</option>
												<option value="3" <c:if test="${paramData.customStatus eq 3}">selected="selected"</c:if>>待买家退货</option>
												<option value="4" <c:if test="${paramData.customStatus eq 4}">selected="selected"</c:if>>待收货</option>
												<option value="5" <c:if test="${paramData.customStatus eq 5}">selected="selected"</c:if>>未收到</option>
												<option value="6" <c:if test="${paramData.customStatus eq 6}">selected="selected"</c:if>>已收到</option>
												<option value="7" <c:if test="${paramData.customStatus eq 7}">selected="selected"</c:if>>弃货</option>
												<option value="9" <c:if test="${paramData.customStatus eq 9}">selected="selected"</c:if>>不同意</option>
											</select>
										</div>	 	
										<div class="item">
						   							<select id="numType" name="numType" class="item-sel">
														<option value="1" <c:if test="${paramData.numType eq 1}">selected="selected"</c:if>>订单编号</option>
														<option value="2" <c:if test="${paramData.numType eq 2}">selected="selected"</c:if>>退款（货）编号</option>
						 					 	   </select>
						   						<input type="text" id="number" name="number" value="${paramData.number }" class="item-inp" style="width: 200px;">
						   						<td width="70" align="center"><label class="submit-border"><input type="button" class="submit btn-r" value="搜索" onclick="search();"></label></td>
						   				</div>
						   			</div>
						   		 </div>
							</form:form>
						</div>
					
						<div id="listBox" style="display: block;border-width:0px 0px;">
							
					  	</div>
					  	
					</div>
				 </div>
			     <!-----------right_con 结束-------------->
			     
			    <div class="clear"></div>
			 </div>
		   
		 	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
		</div>
		<!-- bd 结束 -->
		
		</div>
		<!-- doc 结束 -->
		<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
		<script src="<ls:templateResource item='/resources/common/js/serialize-form.js'/>" type="text/javascript"></script>
		<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/refund/refund.js'/>"></script>
		<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/refund/refundReturnList.js'/>"></script>
		<script type="text/javascript">
			var contextPath = '${contextPath}';
			var applyType = Number('${applyType}');
			$(function(){
				laydate.render({
					   elem: '#startDate',
					   calendar: true,
					   theme: 'grid',
					   trigger: 'click'
				  });
		     
		     laydate.render({
					   elem: '#endDate',
					   calendar: true,
					   theme: 'grid',
					   trigger: 'click'
				  });
			});
		</script>
	</body>
</html>
