<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>店铺装修-商家后台</title>
	<meta http-equiv="keywords" content="店铺装修-商家后台">
	<meta http-equiv="description" content="店铺装修-商家后台">
	<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
	 <%@ include file="/WEB-INF/pages/common/layer.jsp" %>
	 <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/public.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/store_black.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/decorate.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/plugins/colorpicker/css/jquery.bigcolorpicker.css'/>" rel="stylesheet" />
	<c:set var="exist_bannerPic" value="${empty shopDecotate.shopDefaultInfo.bannerPic}"></c:set>
	<c:set var="exist_shopNavs" value="${empty shopDecotate.shopDefaultNavs}"></c:set>
	<c:set var="exist_shopBanners" value="${empty shopDecotate.shopDefaultBanners}"></c:set>
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
  </head>
  <body>
  <div class="fiit_top">
      <div class="fiit_top_box">
        <div class="main">
          <div class="f_top_left"></div>
          <div class="f_top_nav">
            <ul>
              <li  tab_target="layout_tab"  class="this"><a href="javascript:void(0);">布局模块设置</a></li>
              <li tab_target="module_tab" ><a href="javascript:void(0);">基础模块设置</a></li>
            <!--   <li tab_target="theme_tab"><a href="javascript:void(0);">主题设置</a></li>
              <li tab_target="background_tab"><a href="javascript:void(0);">背景设置</a></li> -->
            </ul>
          </div>
          <div class="f_top_right"> <a href="javascript:void(0);" class="save" onclick="subquite();">保存并退出</a> <a href="javascript:void(0);" onclick="closewin();" class="exit"><i></i>退出</a> <a href="javascript:void(0);" onclick="revocation();" class="pepeal"><i></i>撤销</a> <a target="_blank" href="<ls:url address='/s/shopDecotate/decorate/preview' />" class="pre"><i></i>预览</a> </div>
        </div>
      </div>
      <!--换颜色-->
      <div class="fiit_top_down">
        <!--换布局-->
        <div style="" class="fiit_layout" id="layout_tab" div_tab="decorate">
          <ul>
            <li layout="layout0" onclick="addShopDecLoyout(this);" ><a href="javascript:void(0);"><span class="img"><img width="100" height="120" src="<ls:templateResource item='/resources/templets/images/lay_00.jpg'/>"></span><span class="bt"></span><span class="name">通栏布局上</span></a></li>
             <li layout="layout1" onclick="addShopDecLoyout(this);" ><a href="javascript:void(0);"><span class="img"><img width="100" height="120" src="<ls:templateResource item='/resources/templets/images/lay_01.jpg'/>"></span><span class="bt"></span><span class="name">通栏布局下</span></a></li>
            <li layout="layout2" onclick="addShopDecLoyout(this);" ><a href="javascript:void(0);"><span class="img"><img width="100" height="120" src="<ls:templateResource item='/resources/templets/images/lay_02.jpg'/>"></span><span class="bt"></span><span class="name">居中布局</span></a></li>
             <li layout="layout3" onclick="addShopDecLoyout(this);" ><a href="javascript:void(0);"><span class="img"><img width="100" height="120" src="<ls:templateResource item='/resources/templets/images/lay_03.jpg'/>"></span><span class="bt"></span><span class="name">1:2布局</span></a></li>
            <li layout="layout4" onclick="addShopDecLoyout(this);" ><a href="javascript:void(0);"><span class="img"><img width="100" height="120" src="<ls:templateResource item='/resources/templets/images/lay_04.jpg'/>"></span><span class="bt"></span><span class="name">2:1布局</span></a></li> 
          </ul>
        </div>
        <!--基础模块-->
        <div  style="display:none;" class="fiit_module" id="module_tab" div_tab="decorate">
          <ul>
           <li><span class="img"><img width="90" height="100" alt="" src="<ls:templateResource item='/resources/templets/images/mk1.jpg'/>"></span><a mark="banner" onclick="set_decorate_base_module(this)"  href="javascript:void(0);" <c:choose>
				     <c:when test="${shopDecotate.isBanner==0}"> class="off" </c:when>
				     <c:when test="${shopDecotate.isBanner==1}"> class="on" </c:when>
			   </c:choose> ></a> </li> 
            <li> <span class="img"><img width="90" height="100" alt="" src="<ls:templateResource item='/resources/templets/images/mk2.jpg'/>"></span><a title="开启"  mark="info" onclick="set_decorate_base_module(this)" href="javascript:void(0);" <c:choose>
				     <c:when test="${shopDecotate.isInfo==0}"> class="off" </c:when>
				     <c:when test="${shopDecotate.isInfo==1}"> class="on" </c:when>
			   </c:choose> ></a> </li>
            <li> <span class="img"><img width="90" height="100" alt="" src="<ls:templateResource item='/resources/templets/images/mk3.jpg'/>"></span><a title="开启" mark="nav" onclick="set_decorate_base_module(this)" href="javascript:void(0);" <c:choose>
				     <c:when test="${shopDecotate.isNav==0}"> class="off" </c:when>
				     <c:when test="${shopDecotate.isNav==1}"> class="on" </c:when>
			   </c:choose> ></a> </li>
             <li> <span class="img"><img width="90" height="100" alt="" src="<ls:templateResource item='/resources/templets/images/mk4.jpg'/>"></span><a title="开启" mark="slide" onclick="set_decorate_base_module(this)" href="javascript:void(0);" <c:choose>
				     <c:when test="${shopDecotate.isSlide==0}"> class="off" </c:when>
				     <c:when test="${shopDecotate.isSlide==1}"> class="on" </c:when>
			   </c:choose> ></a> </li>
          </ul>
        </div>
        <!--换肤色-->
        <div style="display:none;" class="fiit_color" id="theme_tab" div_tab="decorate">
           <div class="fiit_tab"> <b>当前主题：蓝色</b></div>
          <!--换颜色-->
          <div class="fiit_color_ul">
            <ul>
              <li  class="this"><a href="javascript:void(0);"><span class="img"><img width="111" height="77" src="<ls:templateResource item='/resources/templets/images/black.jpg'/>"></span><span class="name">默认</span></a></li>
        <%--       <li><a href="javascript:void(0);"><span class="img"><img width="111" height="77" src="<ls:templateResource item='/resources/templets/images/blue.jpg'/>"></span><span class="name">蓝色</span></a></li> --%>
            </ul>
          </div>
          </div>
           
         
        
        <!--背景设置-->
        <form:form  method="post" action="" name="bg_form" id="bg_form">
          <div style="display:none;" div_tab="decorate" id="background_tab" class="fiit_bg_edit">
            <table cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <td width="15%" valign="top" align="right">背景图片：</td>
                        <td>
                            <input type="hidden" value="${shopDecotate.bgImgId}" id="bgImgId" name="bgImgId">  
	                    	<div class="file_box">
	                             <input type="text" id="bg_img_show" value="${shopDecotate.bgImgId}" class="ip180">  
	                             <input type="button" value="浏览..." class="btn">
	                             <input type="file" name="bg_img" id="bg_img" class="file">
	                        </div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right">背景颜色：</td>
                        <td><input type="text" value="${shopDecotate.bgColor}" name="bg_color" onblur="changeColor(this);" id="bgColor" class="ip100"><span style="background-color:#00FF56" class="color_block"></span></td>
                    </tr>
                    <tr>
                        <td valign="top" align="right">背景样式：</td>
                        <td>
                            <ul class="bg_style">
                                <li><label><input type="radio" value="repeat" checked="checked" name="repeat" id="repeat"><span>平铺</span></label></li>
                                <li><label><input type="radio" value="stretch" name="repeat" id="repeat"><span>拉伸</span></label></li>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="fiit_save_box"><input type="button"   onclick="bg_set('save')"  value="保存设置"><input type="button"  onclick="bg_set('cancle')" value="清空设置" class="del"></div>
					<div class="fiit_bg_prew">
						<table width="100%" height="100%">
							<tbody>
								<tr>
									<td width="100%" valign="middle" align="center" height="100%"
										style="padding:0px;">
										<c:choose>
										     <c:when test="${empty shopDecotate.bgImgId}">
										        <img src="<ls:templateResource item='/resources/templets/images/black.jpg'/>">
										     </c:when>
										      <c:when test="${not empty shopDecotate.bgImgId}">
										        <img src="<ls:photo item='${shopDecotate.bgImgId}' />">
										     </c:when>
									   </c:choose>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
        </form:form>
      </div>
    </div>
  
  
  
  
	<div class="fiit_height"></div>
	<div 
	    style='<c:if test="${not empty shopDecotate.bgImgId}">
	        background-image:url("<ls:photo item='${shopDecotate.bgImgId}' />"); 
	    </c:if>
	    <c:if test="${not empty shopDecotate.bgColor}">
	          background-color:${ shopDecotate.bgColor };
	    </c:if>
	     <c:if test="${not empty shopDecotate.bgStyle}">
	         background-repeat:${ shopDecotate.bgStyle };
	     </c:if>'  
    class="shop_main">
    
            <div style="display:none;"  id="store_info" class="store_head">
             <div class="store_top">
               <div class="store_main">
                 <!--店铺信息行-->
                 <div class="store_head_right">
                    <div id="store_head_box" class="store_head_box">
                      <div class="store_name">${shopDecotate.shopDefaultInfo.siteName}</div>
							<div class="shopscores">
								<%--<i class="smill"></i> --%>
								<b class="scores_scroll" style="float: left">
								<span class="shop_scroll_gray"> </span>
								<span style="width:10%" class="scroll_red"></span> </b>
							</div>
						</div>
                    <div class="scroll_scroll" style=" float: left;margin-top: 3px;margin-right: 10px;">${shopDecotate.shopDefaultInfo.credit}分</div>
					<div id="store_level" style="display:none" class="store_level">
						<ul style="display:none;" class="pro_shop_date">
							<li><span>评分详细</span>
							</li>
							<li><span>综合评分：<strong>${shopDecotate.shopDefaultInfo.credit}分</strong>
							</span><em class=""></em>
							</li>
						</ul>
						<div class="store_detail">
							<ul>
								<li><span class="company_name">店铺名称：</span><span
									class="red">${shopDecotate.shopDefaultInfo.siteName}</span>
								</li>
								<!-- 
								<li><span class="company_name">联系电话：</span><span
									class="company_r">${shopDecotate.shopDefaultInfo.contactMobile}</span>
								</li>
								 -->
								<li><span class="company_name">店铺地址：</span><span
									class="company_r">${shopDecotate.shopDefaultInfo.shopAddr}</span>
								</li>
								<li><span class="company_name">店铺简述：</span><span
									class="company_r">${shopDecotate.shopDefaultInfo.briefDesc}</span>
								</li>
							</ul>
						</div>
					</div>
					<div class="service"><a href="javascript:void(0);"></a></div>
		     </div>
                    
                </div>
            </div>
      </div>
    
	    <!--banner行-->
	    <div  style="background:url(<ls:photo item='${shopDecotate.shopDefaultInfo.bannerPic}' />) no-repeat top center ;display: none; " id="store_banner" class="store_logo">
		</div>
	     <!--banner行-->
	     
	    <!--导航行-->
	      <div id="store_nav" class="store_nav_width" style="display: none;">
			  <div class="main">
			    <div class="store_nav">
			      <ul>
			        <li>   
			         <a href="javascript:void(0);">店铺首页</a></li>
			         <c:forEach items="${shopDecotate.shopDefaultNavs}" var="nav" varStatus="s">
			             <li id="${nav.id}"><a 
										     <c:if test="${nav.winType=='new_win'}">
										       target="_blank"
										     </c:if>  href="//${nav.url}">${nav.title}</a>
	                    </li>
			         </c:forEach>
			       </ul>
			    </div>
			  </div>
		 </div>
	    <!--导航行-->
	    
	    <!--默认幻灯-->
	     <div id="store_slide"   style="display: none;" >
			<div id="default_fullSlide" class="fullSlide">
				<div id="default_bd" class="bd">
					<ul>
					   <c:forEach items="${shopDecotate.shopDefaultBanners}" var="banner" varStatus="b">
					     	<li id="${banner.id}">
						      <a href="${banner.url}" style="text-align:center;">
						      <img
								style="min-height: 500px;" 
								src="<ls:photo item='${banner.imageFile}' />">
						     </a>
						    </li>
					   </c:forEach>
					</ul>
				</div>
				<div id="default_hd" class="hd">
					<ul>
					   <c:if test="${fn:length(shopDecotate.shopDefaultBanners)>1}">
					       <c:forEach items="${shopDecotate.shopDefaultBanners}" var="banner" varStatus="b">
					     	 <li <c:if test="${b.count==1}">class="on"</c:if> >${b.count}</li>
					      </c:forEach>
					   </c:if>
					</ul>
				</div>
			</div>
		</div>
	    <!--默认幻灯-->
	    
	    
	    <!--装修布局内容-->
	    
	     <!--布局上-->
	      <c:forEach items="${shopDecotate.topShopLayouts}" var="layout_top" varStatus="l">
	          <div class="layout_one" location_mark="${layout_top.layoutId}" location="true">
		          <div style="text-align:center" layout="layout0" id="content" type="${layout_top.layoutModuleType}" mark="${layout_top.layoutId}" url="" >
		                  <h3 class="module_wide">
		                    <div class="main">通栏布局上</div>
		                  </h3> 
		          </div>  
		          
		          <div class="f_edit_box" option="${layout_top.layoutId}">
		               <a href="javascript:void(0);" seq="${layout_top.seq}" onclick="decorate_module(this);" layout="layout0"  mark="${layout_top.layoutId}" moduleType="${layout_top.layoutModuleType}"  class="f_edit"><i></i>编辑</a> 
		               <a layout="layout0" mark="${layout_top.layoutId}" moduleType="${layout_top.layoutModuleType}" onclick="dele_layout(this)" class="f_del"><i></i>删除</a>
		          </div>
		          
		       </div>
	      </c:forEach>
	      <!--布局上-->
	      <div style="clear: both;"></div>
	     <div id="main_layout" class="main ui-sortable"> 
	         <!--布局中-->
			    <c:forEach items="${shopDecotate.mainShopLayouts}" var="layout_main" varStatus="l">
                   <c:choose>
                     <c:when test="${layout_main.layoutType=='layout2'}">
						<div location_mark="${layout_main.layoutId}"  location="true" option="${layout_main.layoutId}" class="layout_two">
							<div mark="${layout_main.layoutId}" layout="layout2" type="${layout_main.layoutModuleType}"  id="content">
								<h3 class="module_big">居中布局</h3>
							</div>
						    <c:if test="${not empty layout_main.layoutModuleType &&  layout_main.layoutModuleType=='loyout_module_custom_prod'}">
						       <a onclick="decorate_module_set(this);" layout="layout2"  mark="${layout_main.layoutId}"  href="javascript:void(0);" class="f_set"><i></i>设置</a>
						    </c:if> 
							<a href="javascript:void(0);"  seq="${layout_main.seq}" onclick="decorate_module(this);" layout="layout2"  mark="${layout_main.layoutId}" moduleType="${layout_main.layoutModuleType}" class="f_edit"><i></i>编辑</a>
							<a layout="layout2" mark="${layout_main.layoutId}" moduleType="${layout_main.layoutModuleType}" onclick="dele_layout(this)" href="javascript:void(0);" class="f_del"><i></i>删除</a>
						</div>
					 </c:when>
					  <c:when test="${layout_main.layoutType=='layout3'}">
					     <c:if test="${not empty layout_main.shopLayoutDivDtos}">
					         <div location_mark="${layout_main.layoutId}" location="true" class="layout_three">
						       <c:forEach items="${layout_main.shopLayoutDivDtos}" var="layout_div" varStatus="d">
						         <c:choose>
										     <c:when test="${layout_div.layoutDiv=='div1'}">
										        <div class="fl">
													<div div="div1" option="${layout_main.layoutId}" class="module">
														<div div="div1" layout="layout3" mark="${layout_main.layoutId}" layoutDivId="${layout_div.layoutDivId}"  type="${layout_div.layoutModuleType}" id="content">
															<h3 class="module_small">1:2布局</h3>
														</div>
														<a href="javascript:void(0);" seq="${layout_main.seq}" onclick="decorate_module(this);" layoutDivId="${layout_div.layoutDivId}" div="div1" layout="layout3"  mark="${layout_main.layoutId}" moduleType="${layout_main.layoutModuleType}" class="f_edit"><i></i>编辑</a>
														<a  layoutDivId="${layout_div.layoutDivId}"
															layout="layout3" div="div1" mark="${layout_main.layoutId}" moduleType="${layout_div.layoutModuleType}" onclick="dele_layout(this)" href="javascript:void(0);"
															class="f_del"><i></i>删除</a>
													</div>
												</div>
										     </c:when>
										      <c:when test="${layout_div.layoutDiv=='div2'}">
										          <div class="fr">
													<div div="div2"  option="${layout_main.layoutId}" class="module">
														<div div="div2" mark="${layout_main.layoutId}" layout="layout3" layoutDivId="${layout_div.layoutDivId}" type="${layout_div.layoutModuleType}" id="content">
															<h3 class="module_mid">1:2布局</h3>
														</div>
														<a href="javascript:void(0);" seq="${layout_main.seq}" onclick="decorate_module(this);"  layoutDivId="${layout_div.layoutDivId}" div="div2" layout="layout3"  mark="${layout_main.layoutId}" moduleType="${layout_main.layoutModuleType}" class="f_edit"><i></i>编辑</a>
														<a layout="layout3" div="div2" mark="${layout_main.layoutId}" layoutDivId="${layout_div.layoutDivId}" onclick="dele_layout(this)" moduleType="${layout_div.layoutModuleType}" href="javascript:void(0);" class="f_del"><i></i>删除</a>
													</div>
												</div>
										     </c:when>
								</c:choose>
						     </c:forEach>
						   </div>
					     </c:if>
					  </c:when>
					  <c:when test="${layout_main.layoutType=='layout4'}">
					    <c:if test="${not empty layout_main.shopLayoutDivDtos}">
					        <div location_mark="${layout_main.layoutId}"  location="true" class="layout_four">
					         <c:forEach items="${layout_main.shopLayoutDivDtos}" var="layout_div" varStatus="d">
						           <c:choose>
										 <c:when test="${layout_div.layoutDiv=='div2'}">
										   <div class="fl">
									        <div div="div2" option="${layout_main.layoutId}" class="module">
									         <div div="div2" mark="${layout_main.layoutId}" layout="layout4" layoutDivId="${layout_div.layoutDivId}" type="${layout_div.layoutModuleType}" id="content">
									            <h3 class="module_mid">2:1布局</h3>
									            </div>
									            <a href="javascript:void(0);" seq="${layout_main.seq}" onclick="decorate_module(this);"  layoutDivId="${layout_div.layoutDivId}" div="div2" layout="layout4"  mark="${layout_main.layoutId}" moduleType="${layout_div.layoutModuleType}" class="f_edit"><i></i>编辑</a>
									            <a layout="layout4" div="div2" layoutDivId="${layout_div.layoutDivId}"  mark="${layout_main.layoutId}" moduleType="${layout_div.layoutModuleType}" onclick="dele_layout(this)" href="javascript:void(0);" class="f_del"><i></i>删除</a>
									        </div>
									    </div>
										 </c:when>
										  <c:when test="${layout_div.layoutDiv=='div1'}">
											    <div class="fr">
											    	<div div="div1" option="${layout_main.layoutId}" class="module">
											         <div div="div1" mark="${layout_main.layoutId}"  layout="layout4" layoutDivId="${layout_div.layoutDivId}" type="${layout_div.layoutModuleType}" id="content">
											            <h3 class="module_small">2:1布局</h3>
											            </div>
											            <a href="javascript:void(0);" seq="${layout_main.seq}" onclick="decorate_module(this);"  layoutDivId="${layout_div.layoutDivId}" div="div1" layout="layout4"  mark="${layout_main.layoutId}" moduleType="${layout_div.layoutModuleType}" class="f_edit"><i></i>编辑</a>
											            <a layout="layout4" layoutDivId="${layout_div.layoutDivId}" div="div1" mark="${layout_main.layoutId}"  moduleType="${layout_div.layoutModuleType}" onclick="dele_layout(this)" href="javascript:void(0);" class="f_del"><i></i>删除</a>
											        </div>
											    </div>
											 </c:when>
								   </c:choose>
							    </c:forEach>
							</div>
					    </c:if>
					  </c:when>
				</c:choose>
			  </c:forEach>
		    <!--布局中-->
         </div>
          <div style="clear: both;"></div>
         
         
           <!--布局下-->
	      <c:forEach items="${shopDecotate.bottomShopLayouts}" var="layout_bottom" varStatus="l">
	          <div class="layout_one" location_mark="${layout_bottom.layoutId}" location="true">
		          <div style="text-align:center" layout="layout1" id="content" type="${layout_bottom.layoutModuleType}" mark="${layout_bottom.layoutId}" url="" >
		                  <h3 class="module_wide">
		                    <div class="main">通栏布局下</div>
		                  </h3> 
		          </div>  
		          
		          <div class="f_edit_box" option="${layout_bottom.layoutId}">
		               <a href="javascript:void(0);" seq="${layout_main.seq}"  layout="layout1" onclick="decorate_module(this);"  mark="${layout_bottom.layoutId}" moduleType="${layout_bottom.layoutModuleType}" class="f_edit"><i></i>编辑</a> 
		               <a layout="layout1" mark="${layout_bottom.layoutId}" moduleType="${layout_bottom.layoutModuleType}" onclick="dele_layout(this)" class="f_del"><i></i>删除</a>
		          </div>
		          
		       </div>
	      </c:forEach>
	      <!--布局下-->
	      
	      
         
         <c:if test="${empty shopDecotate.topShopLayouts && empty shopDecotate.mainShopLayouts && empty shopDecotate.bottomShopLayouts  }">
	        <div class="no_decorate">
				<table>
			    	<tbody>
			        	<tr>
			            	<td width="48%" align="right"><img width="150" height="150" src="<ls:templateResource item='/resources/templets/images/no_decorate.jpg'/>"></td>
			                <td align="left">没有任何装修信息！</td>
			            </tr>
			        </tbody>
			    </table>
			</div>
		</c:if>
	   <!--装修布局内容-->
	</div>
	
	<div class="footer">
          <ul>
            <li class="hui2">&copy;Copyright Power By ${systemConfig.shopName} All Rights Reserved.&nbsp;<a target="_blank" href="http://www.miitbeian.gov.cn">${systemConfig.icpInfo}</a></li>
           </ul>
      </div>
    <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
	<script src="<ls:templateResource item='/resources/templets/js/multishop/jquery.SuperSlide.2.1.1.js'/>" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/common/js/jquery.cookie.js'/>" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/plugins/colorpicker/js/jquery.bigcolorpicker.min.js'/>" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/templets/js/multishop/decorate.js'/>" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/templets/js/score.js'/>" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/common/js/ajaxfileupload.js'/>" type="text/javascript"></script>
   <script type="text/javascript">
    	<c:if test="${shopDecotate.isInfo==1}">
    	    jQuery("#store_info").show();
    	</c:if>
    	 <c:if test="${shopDecotate.isNav==1}">
    	    jQuery("#store_nav").show();
    	 </c:if>
    	 <c:if test="${shopDecotate.isSlide==1}">
    	    jQuery("#store_slide").show();
    	 </c:if>
    	 <c:if test="${shopDecotate.isBanner==1}">
    	   jQuery("#store_banner").show();
    	 </c:if> 
   	    //jQuery("#store_banner").hide();
	
	var contextPath = '${contextPath}';
	var photoPath ="<ls:photo item='' />";
	var color_block="${shopDecotate.bgColor}";
	var decId="${shopDecotate.decId}";
    var exist_bannerPic="${exist_bannerPic}";
	var exist_shopNavs="${exist_shopNavs}";
	var exist_shopBanners="${exist_shopBanners}";
	var soc="${shopDecotate.shopDefaultInfo.credit}";
	var element=$(".scroll_red");
	progress(soc,element);
   </script>           
  </body>
</html>
