<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>配送方式-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/templets/css/multishop/seller-distribution.css'/>" />
	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">配送方式</span>
				</p>
			</div>
				<%@ include file="included/sellerLeft.jsp" %>
     <div class="seller-distribution">
		<div class="pagetab2" style="margin-bottom:15px;">
			<ul>
        		<li><a onclick="window.location.href='<ls:url address='/s/deliveryType'/>'">配送方式</a></li>
				<li class="on" >
					<a href="#">
						<c:if test="${empty deliveryType.name}">新增配送方式</c:if>
						<c:if test="${not empty deliveryType.name}">编辑配送方式</c:if>
					</a>
				</li>
			</ul>
		</div>
      <form:form action="${contextPath}/s/deliveryType/save" method="post" id="form1">
             <input id="dvyTypeId" name="dvyTypeId" value="${deliveryType.dvyTypeId}" type="hidden">
    		<div class="distribution-add">
					<p>
						<c:if test="${empty deliveryType.name}">新增配送方式：</c:if>
						<c:if test="${not empty deliveryType.name}">编辑配送方式：</c:if>
						<span>以下均为必填项，请认真填写</span>
					</p>
					<ul>
						<li>配送名称：<input type="text" style="width:349px;" name="name" id="name" maxlength="20" value="${deliveryType.name}"></li>
						<li>物流公司：<select id="dvyId" name="dvyId" style="margin-right: 96px;">
					            <ls:optionGroup type="select" required="true" cache="fasle" defaultDisp="-- 物流公司 --" sql="select t.dvy_id, t.name from ls_delivery t" 
					                   selectedValue="${deliveryType.dvyId}" />
							</select>
							<%--打印模板：<select id="printtempId" name="printtempId">--%>
					            <%--<ls:optionGroup type="select" required="true" cache="fasle" defaultDisp="-- 打印模版 --" --%>
					                <%--sql="select t.prt_tmpl_id, t.prt_tmpl_title from ls_print_tmpl t where t.shop_id=? or is_system=1 " param="${shopId}"  selectedValue="${deliveryType.printtempId}"--%>
					             <%--/>--%>
							<%--</select>--%>
						</li>
						<%-- <li>打印模板：<select id="printtempId" name="printtempId">
					            <ls:optionGroup type="select" required="true" cache="fasle" defaultDisp="-- 打印模版 --" 
					                sql="select t.prt_tmpl_id, t.prt_tmpl_title from ls_print_tmpl t where t.shop_id=? or is_system=1 " param="${shopId}"  selectedValue="${deliveryType.printtempId}"
					             />
							</select>
						</li> --%>
						<li>配送描述：<textarea name="notes" id="notes" cols="5" rows="5"  maxlength="150">${deliveryType.notes}</textarea></li>
					</ul>
					<input type="submit" value="保存" class="btn-r" style="margin-left:110px;"/>&nbsp;
                    <input type="button" value="返回" class="btn-g" onclick="window.location.href='${contextPath}/s/deliveryType'"/>
				</div>
				</form:form>
				</div>
     
</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/multishop/deliveryTypeConfigure.js'/>" /></script>
</body>
</html>