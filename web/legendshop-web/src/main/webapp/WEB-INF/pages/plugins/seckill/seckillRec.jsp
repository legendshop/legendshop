<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>秒杀记录-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
</head>
<style>
	.sold-table th {
			border-bottom:1px solid #e4e4e4;
		}
</style>
<body class="graybody">
<div id="doc">
   
   <div id="hd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
   </div><!--hd end-->
   
   
   <div id="bd">
            
	 <div class=" yt-wrap" style="padding-top:10px;"> 
    	  <%@ include file="/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp" %>
     <div class="right_con">
     
         <div class="o-mt"><h2>秒杀记录</h2></div>                
         
        <!--订单-->
         <div id="recommend" class="m10 recommend bidding-his" style="display: block;">
                 
                <table width="100%" cellspacing="0" cellpadding="0" class="buytable order sold-table" style="text-align: center;">
                    <tr>
                        <th width="40%">活动名称</th>
                        <th width="10%">秒杀时间</th>
                        <th width="10%">秒杀价</th>
                        <th width="10%">状态</th>
                        <th width="16%">操作</th>
                    </tr>
                    <c:if test="${empty list}">
                    	<tr>
                    		<td colspan="5">没有秒杀记录</td>
                    	</tr>
                    </c:if>
                      <c:if test="${not empty list}">
	                    <c:forEach items="${list}" var ="list" varStatus="status">
	                          <tr>
	                              <td>
	                                <a target="_blank" href="<ls:url address='/seckill/views/${list.seckillId}'/>">
	                                  <div><img src="<ls:images item='${list.seckillPic}' scale='3'/>" alt="" width="120" height="95"></div>
	                                  <div style="float: none;">
	                                    <span class="pm-com-nam" style="margin-top: 20px;">${list.seckillTitle}</span>
	                                  </div>
	                                </a>
	                              </td>
	                              <td><fmt:formatDate value="${list.seckillTime}"  pattern="yyyy-MM-dd HH:mm:ss"/></td>
	                              <td style="color: #e5004f;">¥${list.seckillPrice}</td>
	                                <td>
								<c:choose>
									<c:when test="${list.status eq 0}">等待下单</c:when>
									<c:when test="${list.status eq 1}">已下单</c:when>
								</c:choose>
							     </td>
	                              <td>
	                                <c:choose>
										<c:when test="${list.status eq 0}">
										  <input class="btn-r" type="button" value="提交订单" 
	                                       onclick="window.location.href='${contextPath}/p/seckill/order/${list.seckillId}/${list.secretKey}/${list.prodId}/${list.skuId}/success'" >
										</c:when>
										<c:when test="${list.status eq 1}">
										 <input class="btn-r" type="button" value="删除" 
	                                         onclick="deleteRecord(${list.id})" >
										</c:when>
									</c:choose>
	                                
	                              </td>
	                          </tr>
	                        </c:forEach>
                        </c:if>
                  </table>
				<!-- <div class="pm-ts" style="margin-left:20px">温馨提示：秒杀成功商品中，请在24小时完成下单和支付,否则失去秒杀资格。</div> -->
	     </div>
	     
	     
	     <div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
											<span class="p-num">
												<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/>
											</span>
				     			 </div>
		  				</div>
	     
     </div>
    <div class="clear"></div>
     
 	</div>
   </div>
    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
</div>
	<script type="text/javascript">
		function deleteRecord(id){
		   layer.confirm("是否确定删除该记录?",{icon: 3,btn: ['确定','关闭']},function(){
			   window.location.href="${contextPath}/p/seckill/record/deleteRecord/"+id;
		   });
		 }
		 function pager(curPageNO){
		     window.location.href="${contextPath}/p/seckill/record?curPageNO="+curPageNO;
		 }
	</script>
</body>
</html>
