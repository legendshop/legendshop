<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>分销的商品-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-prod${_style_}.css'/>" rel="stylesheet">
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">分销的商品</span>
				</p>
			</div>
				<%@ include file="included/sellerLeft.jsp" %>
			<div class="seller-selling">
				<div class="pagetab2">
					<ul id="listType">
						<li class="on"><span>分销的商品</span></li>
					</ul>
	   			</div>
				<form:form  action="${contextPath}/s/joinDistProd" method="post" id="from1">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
						<div class="sold-ser sold-ser-no-bg clearfix">
							<div class="fr">
								<div class="item">
									商品名称：
									<input type="text" class="item-inp" style="width:300px;" value="${prod.name}" name="name"  id ="name">
									<input type="button" onclick="search()" class="btn-r" id="btn_keyword" value="搜  索" >	
									<%--<input style="width:initial;padding:7px 10px;" type="button" onclick="loadWantDistProd();" class="serach" id="btn_keyword" value="添加分销商品" >	--%>
								</div>
							</div>
						</div>
				</form:form>
				
				<table class="selling-table sold-table">
					<tr class="selling-tit">
						<th class="check">
							<label class="checkbox-wrapper" style="float:left;margin-left:9px;"">
								<span class="checkbox-item">
									<input type="checkbox" id="checkbox" class="checkbox-input selectAll"/>
									<span class="checkbox-inner" style="margin-left:9px;""></span>
								</span>
					   		</label>
						</th>
						<th>商品</th>
						<th width="400px">商品名称</th>
						<th>价格</th>
						<th>佣金比例</th>
						<th width="120" style="border-right: 1px solid #e4e4e4">操作</td>
					</tr>
					<c:choose>
			           <c:when test="${empty requestScope.list}">
			              <tr>
						 	 <td colspan="20" style="border-right: 1px solid #e4e4e4">
						 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->	
						 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
						 	 </td>
						 </tr>
			           </c:when>
			           <c:otherwise>
			              <c:forEach items="${requestScope.list}" var="product" varStatus="status">
							<tr class="detail">
								<td class="check">
									<label class="checkbox-wrapper" style="float:left;margin:9px;">
										<span class="checkbox-item">
											<input type="checkbox" name="array" class="checkbox-input selectOne" value="${product.prodId}"  arg="${product.name}" onclick="selectOne(this);"/>
											<span class="checkbox-inner" style="margin:9px;"></span>
										</span>
								   	</label>
								</td>
								<td><a href="${contextPath}/views/${product.prodId}" target="_blank"><img src="<ls:images scale="3" item='${product.pic}'/>"></a></td>
								<td><a href="${contextPath}/views/${product.prodId}">${product.name}</a></td>
								<td>¥${product.cash}</td>
								<td><fmt:formatNumber type="number" value="${product.distCommisRate}" maxFractionDigits="2"/>%</td>
								<td>
									<!-- <ul class="mau"> -->
										<%-- <li><a style="cursor: pointer;" onclick="updateCommisRate('${product.prodId}','${product.distCommisRate}','${product.pic}')";   title="修改佣金" target="_blank" class="bot-gra">修改佣金</a></li> --%>
										<a onclick="quitDist('${product.prodId}');" href='javascript:void(0);' title="退出分销" class="btn-r" >退出分销</a>
									<!-- </ul> -->
								</td>	
							</tr>
						</c:forEach>
			           </c:otherwise>
		           </c:choose>
					
				</table>
					<div class="page clearfix">
						 <a class="btn-r" href="javascript:void(0);" onclick="batchQuitDist();" style="margin-top: 2px;">批量退出</a>
		     			 <div class="p-wrap">
		            		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
		     			 </div>
	  				</div>
			</div>
		</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/joinDistProd.js'/>"></script>
	<script type="text/javascript">
		var contextPath="${contextPath}";
	</script>
</body>
</html>