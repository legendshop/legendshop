<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<style type="text/css">
	html{font-size:13px;color:#333}
	.skulist{border:solid #e4e4e4; border-width:1px 0px 0px 1px;width: 1000px;}
	.skulist tr td{border:solid #e4e4e4; border-width:0px 1px 1px 0px; padding:10px 0px;text-align: center;font-size:13px;}
	.skulist tr td input[type="text"]{
		width:50px;height:30px;border:1px solid #ddd;color:#333;background-color: #fff;
	}
	.skulist tr td input[type="button"]{
		width:50px;height:30px;border:1px solid #ddd;color:#333;background-color: #fff;cursor: pointer;
	}
	
</style>
				<input type="hidden" name="storeProdId" value="${storeProdId }"/>
				<table id="skuList" class="skulist" existSku="${not empty skuList}">
					<c:if test="${empty skuList}">
						<c:if test="${not empty prod}">
							<tr >
								<td colspan="4"><b>商品信息</b></td>
							</tr>
							<tr>
								<td>商品名称</td>
								<td>商品价格</td>
								<td>库存数量</td>
							</tr>
								<tr>
									<input type="hidden"  name="prodId" value="${prod.prodId}"/>
									<td>${prod.name}</td>
									<td>${prod.price}</td>
									<td>
										<input type="text" name="prodStocks" value="${prod.actualStocks == null?0:prod.actualStocks}" 
										style="width:50px;height:30px;border:1px solid #ddd;background-color: #fff"/>
									</td>
								</tr>
						</c:if>
					</c:if>
					<c:if test="${not empty skuList}">
						<tr class="table-title">
							<td colspan="5"><b>单品信息</b></td>
						</tr>
						<tr class="table-head">
							<td>单品名称</td>
							<td>属性</td>
							<td>单品价格</td>
							<td>单品库存数量</td>
							<td>操作</td>
						</tr>
						<c:forEach items="${skuList}" var="sku" varStatus="status">
							<tr>
								<input type="hidden"  name="skuIds" value="${sku.skuId}"/>
								<td >${sku.name}</td>
								<td>${sku.cnProperties}</td>
								<td>${sku.price}</td>
								<td>
									<input type="text" name="skuStocks" value="${sku.actualStocks}" />
								</td>
								<td>
									<input type="button" value="移除" onclick="removeSku(this);"/>
								</td>
							</tr>
						</c:forEach>
					</c:if>
				</table>
				
				<c:if test="${skuList != null}">
					<c:if test="${skuList.size() == 0}">
						<div style="color:red;font-size:14px;text-align: center;margin-top:40px;">您已经把该商品的所有SKU添加到门店下了,请选择其他的商品添加!</div>
					</c:if>
				</c:if>

<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script type="text/javascript">
	changeOkBtnState('${skuList.size() == 0}');
	
	function removeSku(obj){
		if($(".table-head ~ tr").length>1){
			$(obj).parent().parent().remove();
		}else{
			alert("只剩下最后一个SKU,不能移除!如果您想还原,请重新选择当前商品!");
		}
	}
</script>
