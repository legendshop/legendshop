<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<style type="text/css">
	.buttonDiv {margin: 0 auto;width:130px;height:50px;}
	.myButton {float:left;margin: 20px auto;display: block;border-radius: 2px;text-align: center;background-color: #e5004f;color: #fff !important;padding: 5px 10px;font-size: 12px;border: 0px;cursor: pointer;}
	.inforInput{width: 212px;float: left;margin-top: 8px;margin-right: 20px;padding: 5px 5px;}
</style>
<table border="0" align="center" class="${tableclass}" id="col1">
    <form:form >
      	<input  type="hidden" id="skuId" value="${skuId}"/>
        <div align="center">
          <thead>
            <tr>
            	<!-- <th width="49%"><div align="center">sku名称</div></th> -->
            	<th width="49%"><div align="center">库存</div></th>
            </tr>
          </thead>
          <c:forEach var="item" items="${list}">
		  <tr>
	 		<!-- <td>
	 			
	       	</td> -->
	        <td>
	          	<div align="center"><input class="stocks" type="text" name="stocks" id="stocks" value="${item.stocks}"/></div>
	          	<input class="skuId" type="hidden" name="skuId" value="${item.skuId}"/>
	           	<input class="prodId" type="hidden" name="prodId" value="${item.prodId}"/>
	        </td>
		 </tr>
		 </c:forEach>
         </div>
   </form:form>
</table>
<div class="am-btn-group am-btn-group-xs" style="width:100%">
	<div class="buttonDiv">
	   	<button id="button" class="bat-oper-btn myButton" style="margin-left:10px;padding: 6px 10px;">提交</button>
	  	<button class="bat-oper-btn myButton" style="margin-left:20px;padding: 6px 10px;" onclick="layer.close(layer.index);">返回</button>
	</div>
</div>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/stockEdit.js'/>"></script>
<script type="text/javascript">
	var contextPath = "${contextPath}";
</script>

