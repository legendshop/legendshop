<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
      <div class="fl search-01" style="padding-left:5px;padding-top:10px;float:right;">
              <input class="bti btn-r" type="button" value="返回"  id="goback" onclick="goback();" style="cursor: pointer;"/>
             <input class="bti btn-g" type="button" value="删除"  id="delBtn" onclick="deleteMsg('${siteInformation.msgId}');" style="cursor: pointer;"/>
      </div> 
  <div class="clear"></div>
          
  <div id="dvReadHeadInfo" class="pk nui-bg-light"  style="background:#fff;margin-top: 8px ;margin-bottom: 8px; border: 1px solid #e6e6e6;padding:10px;">
       <div class="ny">
	          <h3 id="h1Subject" style="font-size:14px;" class="nui-fIBlock lv" tabindex="0" title="邮件标题" hidefocus="hidefocus">${siteInformation.title}</h3>
       </div>
          
  <div class="kz" style="background:#fff;padding:10px;">
     <ul id="ulFull">
          <%--<li class="fS dj" >
          <span class="fT">发件人：</span>
	          <span class="fU">
		          	<span id="mailAddr" class="nui-addr nui-addr-hasAdd" >
		          	<span class="nui-addr-name">${siteInformation.sendName}</span>
		          	</span>
		          </span>
	      
          </li>
          
          <li id="liReadOutto" class="fS dZ" >
		  <span class="fT">收件人：</span>
			  <span id="dvReadto" class="fU">
				  <span id="mailAddr" class="nui-addr nui-addr-hasAdd" >
					  <span class="nui-addr-name">${siteInformation.receiveName}</span>
				  </span>
			  </span>
		  </li>
		  
		  --%><li class="fS dk">
			  <span class="fT">时&nbsp;&nbsp;&nbsp;间：</span>
			  <span class="fU" ><fmt:formatDate value="${siteInformation.recDate}" pattern="yyyy-MM-dd HH:mm"/></span>
		  </li>

	</ul>
</div>
		  
	</div>

	<div style="min-height:250px;background:#fff;border: 1px solid #e6e6e6;padding:10px;">${siteInformation.text}</div>
	
	<div class="clear"></div>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/inBoxMailInfo.js'/>"></script>
 	<script type="text/javascript">
 		var contextPath = "${contextPath}";
 		var receName = '${siteInformation.sendName}';
 		var title = '${siteInformation.title}'
	</script>    