<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
	<ul>
     	<c:forEach items="${requestScope.categoryCommList}" var="categoryComm">
           <li>
            <a href="javascript:void(0);" onclick="load_commcat(this);">
				<c:forEach items="${categoryComm.treeNodes}" var="node" varStatus="n">
                    <span nodeId="${node.selfId}">
						<c:choose>
			                <c:when test="${!n.last}">
		                         ${node.nodeName}&gt;&gt;
			                </c:when>
			                <c:when test="${n.last}">
			           	           ${node.nodeName}
			                </c:when>
			              </c:choose>
                       </span>
				</c:forEach>
			</a>
            <span><img onclick="del_commcat(this,'${categoryComm.id}');" src="${contextPath}/resources/common/images/dele.gif" width="9" height="9" style="cursor:pointer;"></span> 
           </li>
          </c:forEach>
      </ul>
