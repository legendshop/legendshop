<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>

<!-- 我的订单 -->
<div class="my-order">
	<c:if test="${empty imOrderList}">
		<div class="no-record">
			<i class="rec-i"></i>
			<p class="rec-p">暂无数据</p>
		</div>
	</c:if>
	<c:forEach items="${imOrderList}" var="item">
		<div class="ord-item">
			<div class="ord-num">
				<span class="num-span">订单号：</span>
				<span class="num">${item.subNumber }</span>
				<a href="javascript:void(0)" class="btn sendOrder" data-number="${item.subNumber }" data-price='${item.actualTotal}' data-date='<fmt:formatDate value="${item.subDate }" pattern="yyyy-MM-dd HH:mm" type="date"/>'>
					发送
				</a>
			</div>
			<div class="ord-info clear">
				<c:choose>
					<c:when test="${fn:length(item.dtoList)==1}">
						<a href="${contextPath }/views/${item.dtoList[0].prodId}" class="pro-img" target="_blank" data-name="${item.dtoList[0].skuName }" data-prodId="${item.dtoList[0].prodId}">
							<img src="<ls:photo item="${item.dtoList[0].pic}"/>">
						</a>
						<a href="${contextPath }/views/${item.dtoList[0].prodId}" class="name" target="_blank"> ${item.dtoList[0].skuName }</a>
						<a href="javascript:void(0)" class="state">
							<c:choose>
								<c:when test="${item.dtoList[0].refundState  ==  1}">退款退货中</c:when>
								<c:when test="${item.dtoList[0].refundState  ==  2}">已退款</c:when>
								<c:when test="${item.dtoList[0].status == 1}">待付款</c:when>
								<c:when test="${item.dtoList[0].status == 2}">
									<c:choose>
										<c:when test="${not empty item.dtoList[0].mergeGroupStatus && item.dtoList[0].mergeGroupStatus eq 0}">拼团中</c:when>
										<c:otherwise>待发货</c:otherwise>
									</c:choose>
								</c:when>
								<c:when test="${item.dtoList[0].status == 3}">待收货</c:when>
								<c:when test="${item.dtoList[0].status == 4}">已完成</c:when>
								<c:when test="${item.dtoList[0].status == 5}">交易关闭</c:when>
							</c:choose>
						</a>
					</c:when>
					<c:otherwise>
						<c:forEach items="${item.dtoList}" var="subItem">
							<a href="${contextPath }/views/${subItem.prodId}" class="pro-img" target="_blank" data-name="${item.dtoList[0].skuName }" data-prodId="${subItem.prodId}">
								<img src="<ls:photo item="${subItem.pic}"/>">
							</a>
						</c:forEach>
						<a href="javascript:void(0)" class="state" style="float: left;line-height: 62px;width: 50px;text-align: center;">
							<c:choose>
								<c:when test="${item.refundState  ==  1}">退款退货中</c:when>
								<c:when test="${item.refundState  ==  2}">已退款</c:when>
								<c:when test="${item.status == 1}">待付款</c:when>
								<c:when test="${item.status == 2}">
									<c:choose>
										<c:when test="${not empty item.mergeGroupStatus && item.mergeGroupStatus eq 0}">拼团中</c:when>
										<c:otherwise>待发货</c:otherwise>
									</c:choose>
								</c:when>
								<c:when test="${item.status == 3}">待收货</c:when>
								<c:when test="${item.status == 4}">已完成</c:when>
								<c:when test="${item.status == 5}">交易关闭</c:when>
							</c:choose>
						</a>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="ord-han">
				<span class="han-left">订单金额：<span class="pir"><em class="font-family:Arial;">&yen;</em>${item.actualTotal}</span></span>
				<span class="han-right">
					 <fmt:formatDate value="${item.subDate }" pattern="yyyy-MM-dd HH:mm" type="date"/>  
				</span>
			</div>
		</div>
	</c:forEach>
</div>
<!-- /我的订单 -->
