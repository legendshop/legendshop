<!DOCTYPE html>
	<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
	<%@ include file='/WEB-INF/pages/frontend/templetGoat/common/taglib.jsp' %>
    <%@ include file='/WEB-INF/pages/frontend/templetGoat/common/common.jsp' %>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<title>我的礼券</title>
	<link rel="stylesheet" href="<ls:templateResource item='/resources/templets/css/userCoupon.css'/>">
</head>
<body>
<div class="fanhui_cou">
	<div class="fanhui_1"></div>
	<div class="fanhui_ding">顶部</div>
</div>

	<div class="w768">
		<header>
			<div class="back">
				<a href="${contextPath}/p/userhome"> 
				  <img src="<ls:templateResource item='/resources/templets/images/back.png'/>" alt=""> 
				</a>
			</div>
			<p>我的礼券</p>
		</header>
<!-- 头部 -->
		<div class="collection-nav" >
			<ul>
				<li <c:if test="${useStatus==1}">class="collection-nav-on"</c:if> onclick="useable(1);" id="useable1">
					<a href="javascript:void(0);">
						可使用
					</a>
				</li>
				<li <c:if test="${useStatus==2}">class="collection-nav-on"</c:if> onclick="useable(2);" id="useable2">
					<a href="javascript:void(0);" >
						已使用
					</a>
				</li>
				<li <c:if test="${useStatus==3}">class="collection-nav-on"</c:if> onclick="useable(3);" id="useable3">
					<a href="javascript:void(0);">
						已过期
					</a>
				</li>
			</ul>
		</div>
<!-- 导航 -->

<div class="couponSn">
     <input type="text" autocomplete="off" name="couponSn" id="couponSn" placeholder="在此输入兑换码">
        <a href="javascript:void(0);" onclick="checkCoupon();">激活</a>
</div>
<!-- 输入兑换码行 -->


<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
<input type="hidden" id="useStatus" name="useStatus" value="${useStatus}"/>

<div id="container">

</div>
<!-- 内容详情 -->

  </div>
  
 <!-- loading  -->
<section class="loading" id="loading" style="transform-origin: 0px 0px 0px;opacity: 1;transform: scale(1, 1);">
</section>

<!-- error 提示区 -->
<section class="error" style="transform-origin: 0px 0px 0px;opacity: 1;transform: scale(1, 1); display: none;">
    <div><p><span class="text"></span></p></div>
</section>
</body>

<script charset="utf-8"src="<ls:templateResource item='/resources/templets/js/userCoupon.js'/>"></script>
<script type="text/javascript">
  var pageContext="${contextPath}";
   var useStatus="${useStatus}";
</script>
</html>