<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
  <c:set var="subFloorItems" value="${floor.subFloorItems}"></c:set>
    <div class="clear"></div>
          <div class="yt-wrap bf_floor_new clearfix">        
          <div class="com-five">
            <div class="com-five-tit">
              <h4>${floor.name}</h4>
            </div>
            <div class="com-five-con">
                <ul>
                <c:forEach items="${subFloorItems}" var="subFloorItem"  varStatus="index" end="5">
                   <li>
                       <dl>
                     <dt>
                        <a href="${contextPath}/views/${subFloorItem.productId}">
                         <img class="lazy" alt="${subFloorItem.productName}" src="${contextPath}/resources/common/images/loading1.gif" data-original="<ls:images item='${subFloorItem.productPic}' scale='1' />" >
                        </a>
                     </dt>
                     <dd class="com-five-dd">
                        <a href="${contextPath}/views/${subFloorItem.productId}">${subFloorItem.productName}</a>
                     </dd>
                     <dd>
                       <em>¥${subFloorItem.productCash}</em>
                        <span>
                         <c:if test="${not empty subFloorItem.productPrice}">
					               		¥${subFloorItem.productPrice}
					     </c:if>
					    </span>
                     </dd>
                     </dl>
                    </li>
                  </c:forEach>
                 </ul> 
              </div>
          </div>
</div>