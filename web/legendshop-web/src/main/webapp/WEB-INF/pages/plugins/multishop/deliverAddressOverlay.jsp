<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<html>
<head>
<title>添加发货地址</title>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/deliverAddressOverlay.css'/>" rel="stylesheet"/>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
</head>
<body>
<div class="m" id="edit-cont">
	<div class="mc">
		<div class="form">
			<input type="hidden" id="id" value="${userArrress.addrId}">
			<div class="item">
				<span class="label"><em>*</em>联系人：</span>
				<div class="fl"><input id="contactName" class="text" value="${userArrress.contactName}" type="text" maxlength="10">
				 <span id="consigneeNameNote" class="error-msg"></span>
				</div>
				<div class="clr"></div>
			</div>
			<div class="item"> 
				<span class="label"><em>*</em>所在地区：</span>
				<div class="fl">
					<select class="combox sele"  id="provinceid"  requiredTitle="true"  childNode="cityid" selectedValue="${userArrress.provinceId}"  retUrl="${contextPath}/common/loadProvinces"  style="width: 100px; margin-right: 10px;">
					</select>
					<select class="combox sele"   id="cityid" requiredTitle="true"  selectedValue="${userArrress.cityId}"   showNone="false"  parentValue="${userArrress.provinceId}" childNode="areaid" retUrl="${contextPath}/common/loadCities/{value}"  style="width: 100px; margin-right: 10px;">
					</select>
					<select class="combox sele"   id="areaid"  requiredTitle="true"  selectedValue="${userArrress.areaId}"    showNone="false"   parentValue="${userArrress.cityId}"  retUrl="${contextPath}/common/loadAreas/{value}"  style="width: 100px;">
					</select>
					<span class="error-msg" id="areaNote"></span>
				</div>
				
				<div class="clr"></div>
			</div>
			<div class="item">
				<span class="label"><em>*</em>详细地址：</span>
				<div class="fl"><input id="subAdds" value="${userArrress.adds}" class="text text1" type="text"></div>
				<span class="error-msg" id="consigneeAddressNote" ></span>
				<div class="clr"></div>
			</div>
			<div class="item">
				<div class="fl">
					<span class="label"><em>*</em>手机号码：</span>
					<input id="mobile" value="${userArrress.mobile}" maxlength="11" class="text"  type="text">
					<span class="error-msg" id="consigneeMobileNote" ></span>
				</div>
				
				<div class="clr"></div>
			</div>
			<div class="btns"><a href="javascript:void(0);" onclick="addDeliverAddress();" class="ncbtn">保存发货地址</a></div>
		</div>
	</div>
</div>

<script type="text/javascript">
		var contextPath = '${contextPath}';
		$(document).ready(function() {
			$("select.combox").initSelect();
		});
</script>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script type="text/javascript"  src="<ls:templateResource item='/resources/templets/js/deliverAddressOverlay.js'/>"></script>
</body>
</html>