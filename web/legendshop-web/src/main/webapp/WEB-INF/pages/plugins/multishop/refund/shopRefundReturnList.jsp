<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
  <head>
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>退款/退货列表-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/refund/ret-not${_style_}.css'/>" rel="stylesheet"/>
  </head>
<body class="graybody">
	<div id="doc">
	  	 <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		 <div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置> <a href="${contextPath}/home">首页</a>> <a href="${contextPath}/sellerHome">卖家中心</a>> <span class="on">退款/退货管理</span>
				</p>
			</div>
	     	<%@ include file="../included/sellerLeft.jsp" %>
	     	<div id="rightContent" class="right_con">
     			<div class="seller-return" style="position: relative;">
			     	<div class="pagetab2">
						<ul id="listType">
							<li class="on" value="1">
								<span>退款记录</span>
							</li>
							<li value="2"><span>退货记录</span></li>
						</ul>
        			</div>
					<!-- 退换货导航 -->
					<div class="ret-sea">
						<form:form id="searchForm" method="GET">
							<input type="hidden" id="curPageNO" name="curPageNO" />
							<div class="sold-ser sold-ser-no-bg clearfix" >
								<div class="fr">
									<div class="item">
										申请时间:
										<input class="item-inp" type="text" id="startDate" name="startDate" readonly="readonly" value="${paramDto.startDate}" placeholder="起始时间"/>
										 – 
										 <input class="item-inp" type="text" id="endDate" name="endDate"  readonly="readonly" value="${paramDto.endDate}" placeholder="结束时间"/></td>
									</div>
									<div class="item">
										处理状态:
										<select name="customStatus" id="refundOption" class="item-inp">
											<option value="">全部</option>
											<option value="1" <c:if test="${paramData.customStatus eq 1}">selected="selected"</c:if>>待审核</option>
											<option value="2" <c:if test="${paramData.customStatus eq 2}">selected="selected"</c:if>>已撤销</option>
											<option value="8" <c:if test="${paramData.customStatus eq 8}">selected="selected"</c:if>>同意</option>
											<option value="9" <c:if test="${paramData.customStatus eq 9}">selected="selected"</c:if>>不同意</option>
										</select>
										<select name="" style="display: none;" id="returnLOption" class="item-inp">
											<option value="">全部</option>
											<option value="1" <c:if test="${paramData.customStatus eq 1}">selected="selected"</c:if>>待审核</option>
											<option value="2" <c:if test="${paramData.customStatus eq 2}">selected="selected"</c:if>>已撤销</option>
											<option value="3" <c:if test="${paramData.customStatus eq 3}">selected="selected"</c:if>>待买家退货</option>
											<option value="4" <c:if test="${paramData.customStatus eq 4}">selected="selected"</c:if>>待收货</option>
											<option value="5" <c:if test="${paramData.customStatus eq 5}">selected="selected"</c:if>>未收到</option>
											<option value="6" <c:if test="${paramData.customStatus eq 6}">selected="selected"</c:if>>已收到</option>
											<option value="7" <c:if test="${paramData.customStatus eq 7}">selected="selected"</c:if>>弃货</option>
											<option value="9" <c:if test="${paramData.customStatus eq 9}">selected="selected"</c:if>>不同意</option>
										</select>
									</div>
									<div class="item">
			   						   <select id="numType" name="numType" class="item-inp" style="width:110px;">
											<option value="1" <c:if test="${paramData.numType eq 1}">selected="selected"</c:if>>订单编号</option>
											<option value="2" <c:if test="${paramData.numType eq 2}">selected="selected"</c:if>>退款（货）编号</option>
			 					 	   </select>
				   					   <input class="item-inp" type="text" id="number" name="number" value="${paramData.number }" style="width: 250px;">
				   						<button type="button" class="btn-r" onclick="search()">搜索</button>
					 				</div>
					 			</div>
					   		</div>
						</form:form>
					</div>
				
					<!-- 列表内容 -->
					<div id="listBox"></div>
				  	
				</div>
			</div>
	    </div>
	 	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
		
	</div>
		<!-- doc 结束 -->
		<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
		<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/refund/refundReturnList.js'/>"></script>
		<script type="text/javascript">
			var contextPath = '${contextPath}';
			var applyType = Number('${applyType}');
			
			$(function(){
				userCenter.changeSubTab("myreturnorder");//高亮菜单
				laydate.render({
					   elem: '#startDate',
					   calendar: true,
					   theme: 'grid',
					   trigger: 'click'
				  });
		     
		     	laydate.render({
					   elem: '#endDate',
					   calendar: true,
					   theme: 'grid',
					   trigger: 'click'
				  });
			});
		</script>
	</body>
</html>
