<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>
		<c:if test="${not empty prodTag.id}">修改标签-${systemConfig.shopName}</c:if>
		<c:if test="${empty prodTag.id}">添加标签-${systemConfig.shopName}</c:if>
	</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-category${_style_}.css'/>" rel="stylesheet">
	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/plugins/select/divselect.css'/>" />
	 <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div id="Content" class="w1190">
			<div class="seller-cru">
						<p>
							您的位置>
							<a href="${contextPath}/home">首页</a>>
							<a href="${contextPath}/sellerHome">卖家中心</a>>
							<span class="on">
								<c:if test="${not empty prodTag.id}">修改标签</c:if>
								<c:if test="${empty prodTag.id}">添加标签</c:if>
							</span>
						</p>
					</div>
					<%@ include file="included/sellerLeft.jsp" %>
		         <div class="seller-category">
						<div class="seller-com-nav">
							<ul>
								<li id="tagManage"><a href="${contextPath}/s/tagManage">标签管理</a></li>
								<li class="on" id="shopCatConfig">
									<a href="javascript:void(0);">
										<c:if test="${not empty prodTag.id}">修改标签</c:if>
										<c:if test="${empty prodTag.id}">添加标签</c:if>
									</a>
								</li>
							</ul>
						</div>
			         <form:form action="${contextPath}/s/saveProdTag" method="post" id="form1" enctype="multipart/form-data">
			            <input id="id" name="id" value="${prodTag.id}" type="hidden">
			            <div align="center">
					        <table cellspacing="0" cellpadding="0" border="0" style=" margin-top:30px; font-size: 12px;" class="sendmes_table" >
					     	 <tr>
					        <th>
					          		标签名称：<font color="ff0000">*</font>
					       </th>
					        <td>
					           <input type="text" name="name" id="name" maxlength="30"  value="${prodTag.name}">
					        </td>
					      </tr>					      					     
					    	<tr>
					    		<th>
					    			右上标签图片：
					    		</th>
					    		<td>
					    			<input type="file" class="file" name="rightFile" id="rightFile" size="30"/>
					    			<input type="hidden" class="file" name="rightTag" id="rightTag" size="30" value="${prodTag.rightTag}"/>
					    		</td>
					    	</tr>		
							<c:if test="${not empty prodTag.rightTag}">
							    <tr>
							   		<th>原有图片:</th>
							     	<td >
							      	<img src="<ls:photo item='${prodTag.rightTag}'/>" height="50" width="150"/>
							      </td>
							    </tr>
			     			</c:if>
			     			
			     			<tr>
					    		<th>
					    			下横标签图片：
					    		</th>
					    		<td>
					    			<input type="file" class="file" name="bottomFile" id="bottomFile" size="30"/>
					    			<input type="hidden" class="file" name="bottomTag" id="bottomTag" size="30" value="${prodTag.bottomTag}"/>
					    		</td>
					    	</tr>		
							<c:if test="${not empty prodTag.bottomTag}">
							    <tr>
							   		<th>原有图片:</th>
							     	<td >
							      	<img src="<ls:photo item='${prodTag.bottomTag}'/>" height="50" width="150"/>
							      </td>
							    </tr>
			     			</c:if>
					
					                <tr>
					                	<th></th>
					                    <td>
					                            <a href="javascript:void(0)" onclick="submitTable();" class="btn btn-7"><s></s>提交</a>
					                            <a href="javascript:void(0)" onclick="tagManage();" class="btn btn-7"><s></s>返回</a>
					                    </td>
					                </tr>
					            </table>
			           </div>
			        </form:form>
		        </div>
		</div>
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select/divselect.js'/>"></script>
<script type="text/javascript">
	function tagManage(){
		window.location.href="${contextPath}/s/tagManage";
	}
	
	$(document).ready(function() {
		userCenter.changeSubTab("tagManage");
		$("#form1").validate({
			rules: {
				name:  "required",
				 seq: {
	              number: true
	            }
			},
			messages: {
				name: "请输入标签名称",
				seq: {
	                number: "请输入数字"
	            },
				file:"请上传标签图片"
			}
		}); 
			
		$("#tagManage").click(function(){//前往管理标签列表
			window.location.href="${contextPath}/s/tagManage";
		});	
	});
	 
	 function submitTable(){
		 	$("#form1").submit();
	  }	
</script>
</body>