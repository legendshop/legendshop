<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>团购商城-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
     <link rel="stylesheet" type="text/css" media='screen' href="${contextPath}/resources/templets/css/productsort.css" />
</head>

<body>
<div id="doc">
	<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
   <div id="bd">
      <div class="seller-cru" style="width:1190px;margin: 20px auto 10px;">
        <p>
          您的位置&gt;
          <a href="${contextPath}/home">首页</a>&gt;
          <span style="color: #e5004f;">团购</span>
        </p>
      </div>
	 <div class=" yt-wrap" style="padding-top:10px;"> 
         <%@include file="groupCategorys.jsp"%>
	    
		<div id="main-nav-holder"  >
            <%@ include file="groupProdList.jsp" %>
		</div>
	 </div>
   </div><!--bd end-->
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	<link rel="stylesheet" type="text/css" media='screen' href="${contextPath}/resources/templets/css/group.css" />
</div>
</body>
</html>