<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<!-- -----------------SSS 分类导航 -------------------->
		      <div class="leftm ">
		      	<div class="nch-module nch-module-style02">
			    <div class="title">
				  <h3>分类筛选</h3>
				 </div>
				<div class="content">
				 <c:if test="${not empty categorys}"> 
					<ul class="tree" id="files" role="tree">
					      <c:forEach items="${categorys}" var="firstNote" varStatus="first"> 
					          <li role="treeitem" >
					            <i class="tree-parent" tabindex="0"></i>
							         <a href="${contextPath}/integral/list?firstCid=${firstNote.id}"  <c:if test="${firstNote.id==param.firstCid}"> class="selected"</c:if> >${firstNote.name}</a>
							          <c:if test="${not empty firstNote.childrenList}"> 
							                 <ul role="group" <c:if test="${!first.first}"> class="tree-group-collapsed" </c:if> >
							                       <c:forEach items="${firstNote.childrenList}" var="twoNote" varStatus="two" > 
							                                <li role="treeitem"  >
																	 <i class="tree-parent" tabindex="-1"></i>
																	    <a href="${contextPath}/integral/list?twoCid=${twoNote.id}" <c:if test="${twoNote.id==param.twoCid}"> class="selected"</c:if> >${twoNote.name}</a>
																	    <c:if test="${not empty twoNote.childrenList}"> 
																	         <ul role="group" class="">
																		         <c:forEach items="${twoNote.childrenList}" var="thirdNote" varStatus="third"> 
																		              <li class="tree-parent tree-parent-collapsed" role="treeitem">
																			               <i tabindex="-1"></i>
																			               <a <c:if test="${thirdNote.id==param.thirdCid}"> class="selected"</c:if>  href="${contextPath}/integral/list?thirdCid=${thirdNote.id}">${thirdNote.name}</a>
																					   </li>
																		         </c:forEach>
																	         </ul>    
																	    </c:if>
							                                 </li>
							                       </c:forEach>
											 </ul>
							          </c:if>
							       </li>
					      </c:forEach>
					 </ul>
				   </c:if>
				</div>
				</div>
					<div id="exchangeList">
	        
	       			 </div>
			</div>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/category_menu.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/Integralcategorys.js'/>"></script>
<script type="text/javascript">
		var contextPath = '${contextPath}';
		var firstCid="${param.firstCid}";
		var twoCid="${param.twoCid}";
		var thirdCid="${param.thirdCid}";
</script>		
        
		