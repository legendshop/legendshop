<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/dialog.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<title>客服人员</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css" href="${contextPath}/resources/templets/css/multishop/server/service-staff.css" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>
					>
					<a href="${contextPath}/sellerHome">卖家中心</a>
					>
					<span class="on">客服人员</span>
				</p>
			</div>
			<%@ include file="../included/sellerLeft.jsp"%>
			<div class="right_con">
				<div class="o-mt">
					<h2>更改客服密码</h2>
				</div>
				<div class="service-staff">
					<!-- 更改客服人员登录密码 -->
					<form id="form">
					
						<input type="hidden" id="id" name="id" value="${customServerId}" />
						<div class="ser-add">
<%--							<div class="item clear">--%>
<%--								<span class="item-tit">旧客服密码：</span>--%>
<%--								<div class="item-con">--%>
<%--									<input id="password" name="password" type="password" value=""  size="2" />--%>
<%--								</div>--%>
<%--							</div>--%>
							<div class="item clear">
								<span class="item-tit">新客服密码：</span>
								<div class="item-con">
									<input id="newPassword" name="newPassword" type="password" value=""  size="2" />
								</div>
							</div>
							<div class="item clear">
								<span class="item-tit">确认密码：</span>
								<div class="item-con">
									<input id="newPassword2" name="newPassword2" type="password" value="" size="2" />
								</div>
							</div>
						
							<div class="item clear">
								<span class="item-tit">&nbsp;</span>
								<div class="item-con">
									<input type="submit" value="确认更改" class="item-btn">
									<a href="${contextPath}/s/customServer" class="item-btn gray-btn">取消</a>
								</div>
							</div>
						</div>
					</form>
					<!-- /更改客服人员登录密码 -->
				</div>
			</div>
		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<script type="text/javascript" src="${contextPath}/resources/common/js/jquery.js"></script>
	<script type="text/javascript" src="${contextPath}/resources/common/js/jquery.validate.js"></script>
	<script type="text/javascript">
		var contextPath = "${contextPath}";
		
		$(document).ready(function () {
			userCenter.changeSubTab("CustomServer"); //高亮菜单
		});

		$.validator.addMethod("password", function(value) {
			var regx = /(?!.*[\u4E00-\u9FA5\s])(?!^[a-zA-Z]+$)(?!^[\d]+$)(?!^[^a-zA-Z\d]+$)^.{6,15}$/;
			if (!regx.test(value)) {
				return false;
			}
			return true;
		});

		$("#form").validate({
			rules : {
				password : {
					required : true,
					password : 0
				},
				
				newPassword : {
					required : true,
					password : 0,
				},
				newPassword2 : {
					required : true,
					password : 0,
					equalTo: "#newPassword"
				},
			},
			messages : {
				password : {
					required : "请填写旧的客服密码 ",
					password : "密码由6-15位字母、数字或符号的两种及以上组成！",
				},
				
				newPassword : {
					required : "新密码不能为空",
					password : "密码由6-15位字母、数字或符号的两种及以上组成！",
				},
				newPassword2 : {
					required : "请确认新密码",
					password : "密码由6-15位字母、数字或符号的两种及以上组成！",
					equalTo: "两次密码输入不一致，请重新输入"
				},
			},
			submitHandler : function() {
				$.ajax({
					url : contextPath+"/s/customServer/updatePsw",
					type : "POST",
					dataType : "json",
					data : $("#form").serialize(),
					success : function(result) {
						if (result.status) {
							art.dialog.tips("更改成功！ ");
							setTimeout(function() {
								window.location.href = contextPath + "/s/customServer";
							}, 1000);
						} else {
							art.dialog.tips(result.msg);
						}
					}
				});
			}
		});
</script>
</body>
</html>