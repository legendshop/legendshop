<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>打印模板-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/templets/css/multishop/seller-distribution.css'/>" />
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">打印模板</span>
				</p>
			</div>
				<%@ include file="included/sellerLeft.jsp" %>
				<div class="seller-distribution">
				<div class="pagetab2" style="margin-bottom:15px;">
					<ul>
						<li class="on" ><a href="#">打印模板</a></li>
		        		<li><a onclick="window.location.href='<ls:url address='/s/printTmpl/load'/>'">新增打印模板</a></li>
					</ul>
				</div>
				<table class="distribution-table sold-table" style="margin-top: 5px;">
					<tr class="distribution-tit">
						<th width="190">模版名称</th>
						<th width="100">是否启用</th>
						<th width="100">宽</th>
						<th width="100">高</th>
						<th width="80">系统默认</th>
						<th width="200">操作</th>
					</tr>
					<c:forEach items="${requestScope.list}" var="item" varStatus="status">				
						<tr class="detail">
							<td>${item.prtTmplTitle}</td>
							<td>
							<c:choose>
							  <c:when test="${item.prtDisabled==1}">
							        是
							  </c:when>
							  <c:otherwise>
							          否
							  </c:otherwise>
							</c:choose>
							</td>
							
							<td>${item.prtTmplWidth}</td>
							<td>${item.prtTmplHeight}</td>
							<td>
							   <c:choose>
							     <c:when test="${item.isSystem=='1'}">是</c:when>
							     <c:when test="${item.isSystem=='0'}">否</c:when>
							   </c:choose>
							</td>
							<td>
							   <c:if test="${item.isSystem=='0'}">
							       <a class="btn-r" href='<ls:url address='/s/printTmpl/loadDesign/${item.prtTmplId}'/>' title="打印设计">打印设计</a>
								   <a class="btn-g" href='<ls:url address='/s/printTmpl/load/${item.prtTmplId}'/>' title="修改">修改</a>
					      		   <a class="btn-g" href='javascript:deleteById("${item.prtTmplId}")' title="删除">删除</a>
							  </c:if>
							</td>
						</tr>
					</c:forEach>
				</table>
				<div style="margin-top:10px;" class="page clearfix">
				     	<div class="p-wrap">
							<span class="p-num">
								<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
							</span>
				     	</div>
		  		</div>
<!-- 已创建配送方式 -->
				<div class="distribution-space"></div>
				
				<%-- <a class="ncbtn" style="padding:5px 10px;" href='<ls:url address='/s/printTmpl/load'/>'>新增打印模板</a> --%>
<!-- 空白区间 -->
<!-- 新增创建配送方式 -->				
			</div>
 		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
<script type="text/javascript">
 	$(document).ready(function() {
    	userCenter.changeSubTab("transportPrintTmpl");
	 });
    
   function deleteById(id) {
		
		layer.confirm('你确定删除该模板？',{
	  		 icon: 3
	  	     ,btn: ['确定','取消'] //按钮
	  	   }, function () {
	  		 window.location = "<ls:url address='/s/printTmpl/delete/" + id + "'/>";
		})
	}	
   
   function pager(curPageNO){
	   userCenter.loadPageByAjax("/s/printTmpl/query?curPageNO=" + curPageNO);
	}
</script>
</body>
</html>