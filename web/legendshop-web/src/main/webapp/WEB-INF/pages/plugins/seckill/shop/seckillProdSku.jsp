<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/prodSelectAlert.css'/>" rel="stylesheet"/>
<table border="0" id="skuList" cellpadding="0" cellspacing="0" style="margin: 30px 0;">
	<tbody>
	<tr style="background: #f9f9f9;">
		<td width="70px">
			<label class="checkbox-wrapper">
				<span class="checkbox-item">
					<input type="checkbox" id="checkbox" class="checkbox-input selectAll">
					<span class="checkbox-inner"></span>
				</span>
			</label>
		</td>
		<td width="120px">商品图片</td>
		<td width="270px">商品名称</td>
		<td width="100px">商品规格</td>
		<td width="100px">商品价格</td>
		<td width="100px">商品库存</td>
		<td width="100px">秒杀库存</td>
		<td width="100px">秒杀价格</td>
		<td width="80px">操作</td>
	</tr>
	<c:choose>
		<c:when test="${not empty skuList}">
			<c:forEach items="${skuList}" var="sku">
				<tr class="sku" >
					<td>
						<label class="checkbox-wrapper">
							<span class="checkbox-item">
								<input type="checkbox" name="array" class="checkbox-input selectOne" onclick="selectOne(this);">
								<span class="checkbox-inner"></span>
							</span>
						</label>
					</td>
					<td>
						<img src="<ls:images item='${sku.pic}' scale='3' />" >
					</td>
					<td>
						<span class="name-text">${sku.name}</span>
					</td>
					<td>
						<span class="name-text">${sku.cnProperties}</span>
					</td>
					<td>
						<span class="name-text">${sku.price}</span>
					</td>
					<td>
						<span class="name-text" class="skustocks">${sku.stocks}</span>
					</td>
					<td><input type="text" id="seckillStock" class="seckillStock" width="80px" maxlength="8"/></td>
					<td>
						<input type="text" class="seckillPrice" id="seckillPrice" name="seckillPrice" style="text-align: center" maxlength="10" value="${sku.price}">
						<input type="hidden" id="prodId" value="${sku.prodId}"/>
						<input type="hidden" id="skuId" value="${sku.skuId}"/>
					</td>
					<td><a href="#" class="removeSku">移除</a></td>
				</tr>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<tr class="first">
				<td colspan="7" >暂未选择商品</td>
			</tr>
		</c:otherwise>
	</c:choose>
	<tr class="noSku" style="display: none">
		<td colspan="7" >暂未选择商品</td>
	</tr>
	</tbody>
</table>