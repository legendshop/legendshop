<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="renderer" content="webkit"/>
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
    <title>商品成功加入购物车</title>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
    <style type="text/css">
        .add-success{overflow: hidden;padding: 100px 0px;text-align: center;width: 1190px;margin: 0px auto;}
		.add-success p{font-size: 18px;font-weight: bold;font-family: 'Microsoft Yahei';margin-bottom: 20px;}
		.add-success p img{vertical-align: middle;margin-right: 10px;}
		.add-success span{color: #666;}
		.add-success a{color: #3f6de0;}
		.add-success a.buy{cursor: pointer;background-color: #e5004f;padding: 10px 10px;width: 112px;text-align: center;color: #fff;border: none;margin-top: 20px;font-size: 14px;border-radius: 2px;}
    </style>
</head>
<body  class="graybody" style="background-color: #ffffff;">
	<div id="doc">
         <!--顶部menu-->
		   <%@ include file="home/top.jsp" %>
		   
	       
	        <div class="add-success">
          	<p><img src="<ls:templateResource item='/resources/templets/images/gou2.png'/>" alt="">商品已成功加入购物车！</p>
            <span>您已将商品成功加入购物车，现在您可以：</span><a href="${contextPath}/views/${basket.prodId}">继续购物</a>
            <br>
            <p style="margin-top: 20px;"><a class="buy" href="${contextPath}/shopCart/shopBuy">去购物车结算</a></p>
          </div>
	       
		 <!----foot---->
		 <%@ include file="home/bottom.jsp" %>
        <!----foot end---->
	</div>

</body>
</html>
