<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
 <%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<style>
.buytable{ border-collapse: collapse;
    border-style: solid;
	border-width: 0 1px 1px;
	border-color:#e4e4e4;
	font-size: 12px;
	}
.buytable th {
    background: #f9f9f9;
    color: #666666;
    font-weight: normal;
    height: 26px;
	padding:0 5px;
	border: 1px solid #e4e4e4;
}
.buytable td {
    background: #fff;
    color: #333;
    font-weight: normal;   
	border:1px solid #e4e4e4;
	padding:2px 5px;
	text-align:center;
}
.mailtab th{ text-align:center; border-left:0; border-right:0;}
.mailtab td{ text-align:center; padding:5px; border-left:0; border-right:0; line-height:2em;}
td, th {
    display: table-cell;
    vertical-align: inherit;
}
td a {
	color: #333;
	-webkit-transition: all 0.2s ease;
	-moz-transition: all 0.2s ease;
	transition: all 0.2s ease;
	text-decoration: none;
}
td a:hover {
	color: #e5004f;
}
</style>
<div id="Content">
	<div class="recommend" >	
		<table  style="width:100%" cellspacing="0" cellpadding="0" class="buytable mailtab">
		<tbody>
		<tr>
			<th width="250">购物券名称</th>
			<th width="190">礼券的类型</th>
			<th width="190">劵值满金额</th>
			<th width="190">劵值减金额</th>
			<th width="190">操作</th>
		</tr>
		<c:forEach items="${requestScope.coupons}" var="item" varStatus="status">
			<tr>
				<td>${item.couponName}</td>
				<td>
				   <c:choose>
				     <c:when test="${item.couponType=='common'}">通用券</c:when>
				     <c:when test="${item.couponType=='category'}">品类券</c:when>
					 <c:when test="${item.couponType=='product'}">商品券</c:when>
					 <c:when test="${item.couponType=='shop'}">店铺红包</c:when>
				   </c:choose>
				</td>
				<td>
				  ${item.fullPrice}
				</td>
				<td>
				  ${item.offPrice}
				</td>
				<td>
				    <a href="javascript:selectCoupon('${item.couponId}');" >选择</a>
				</td>
			</tr>
		</c:forEach>
		</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
   
   var shopId="${shopId}";
   function selectCoupon(_couponId){
   
	 parent.couponCallBack(_couponId,shopId);
	 layer.msg('已选择', {icon:1, time:1000}, function(){
		 var index = parent.layer.getFrameIndex(window.name);  
		 parent.layer.close(index);
	 });
   }
 
</script>
			


