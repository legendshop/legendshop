<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<%@include file='/WEB-INF/pages/common/layui.jsp'%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/addInvoice.css'/>" rel="stylesheet" />
    <link rel="stylesheet" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
    <div class="invoice-wrap">
        <div class="invoice">
            <div class="invoice-content">
                <div class="top-nav">
                    <div class="invoice-type item on" type="common">普通发票</div>
                    <div class="invoice-type item" type="tax">增值税发票</div>
                </div>

                <form class="layui-form" action="" id="commonForm">

                    <input type="hidden" id="title" name="title" value="${invoice.title}" />
                    <input type="hidden" id="id" name="id" value="${invoice.id}" />

                <!-- 普通发票 -->
                <div class="msg-bottom invoice common">
                    <!-- 基本信息 -->
                    <div class="ordinary">
                        <h2 class="title">基本信息</h2>
                        <div class="bottom">
                            <p>
                                <span class="form-field-title"><i class="required">*</i>普票类型：</span>
                                <span class="form-field-input layui-input-block">
                                    <select name="title" lay-filter="title">
                                        <option value="1" <c:if test="${empty invoice.title || invoice.title eq 1}" >selected="selected"</c:if> <c:if test="${invoice.title eq 1}" >disabled</c:if> >个人抬头</option>
                                        <option value="2" <c:if test="${invoice.title eq 2}" >selected="selected" disabled</c:if>>公司抬头</option>
                                    </select>
                                </span>

                            </p>
                            <p class="js-invoice-sub-type js-sub-type-2 js-sub-type-1">
                                <span class="form-field-title"><i class="required">*</i>发票抬头：</span>
                                <span class="form-field-input"><input id="common-company" name="company" class="form-fields" placeholder="请填写发票抬头" value="${invoice.company}"><label class="errorText"></label></span>
                            </p>
                            <p class="js-invoice-sub-type js-sub-type-2 invoice-hum-number" style="display: none;">
                                <span class="form-field-title"><i class="required">*</i>纳税人号：</span>
                                <span class="form-field-input"><input id="common-invoiceHumNumber" name="invoiceHumNumber" class="form-fields" placeholder="请填写纳税人识别码" value="${invoice.invoiceHumNumber}"><label class="errorText"></label></span>
                            </p>
                            <div class="js-invoice-sub-type js-sub-type-2 invoice-tips" style="display: none;"><span>公司抬头的发票必须录入正确的税号(15位或18位)，以免无法开票。</span></div>


                            <span class="set-default-normal">
                                <div class="layui-input-block marL" style="margin-left: 0px">
                                    <input type="checkbox" lay-skin="switch" id="commonInvoice" name="commonInvoice" lay-filter="commonInvoice" value="${invoice.commonInvoice}" >
                                    <label class="label">设为默认发票</label>
                                </div>
                            </span>

                        </div>
                    </div>
                    <!-- 基本信息 -->
                </div>
                <!-- 普通发票 -->
                </form>

                <%-- ------------------------------------------------------------------华丽分割线 -----------------------------------------------------------------------%>
                <form class="layui-form" action="" id="taxForm">

                <!-- 增值税发票 -->
                <div class="msg-bottom invoice tax" style="display: none;">
                    <!-- 基本信息 -->
                    <div class="ordinary">
                        <h2 class="title">基本信息</h2>
                        <div class="bottom flex">
                            <div class="item">
                                <p>
                                    <span class="form-field-title"><i class="required">*</i>公司名称：</span>
                                    <span class="form-field-input">
                                        <input id="tax-company" name="company" class="form-fields" placeholder="请填写公司名称" value="${invoice.company}">
                                        <label class="errorText"></label>
                                    </span>
                                </p>
                                <p>
                                    <span class="form-field-title"><i class="required">*</i>纳税人号：</span>
                                    <span class="form-field-input">
                                        <input id="tax-invoiceHumNumber" name="invoiceHumNumber" class="form-fields" placeholder="请填写纳税人识别码" value="${invoice.invoiceHumNumber}">
                                        <label class="errorText"></label>
                                    </span>
                                </p>
                            </div>

                            <div class="item">
                                <p class="js-invoice-sub-type js-sub-type-2 js-sub-type-1">
                                    <span class="form-field-title"><i class="required">*</i>注册地址：</span>
                                    <span class="form-field-input"><input id="registerAddr" name="registerAddr" class="form-fields" placeholder="请填写公司注册地址" value="${invoice.registerAddr}"><label class="errorText"></label></span>
                                </p>

                                <p class="js-invoice-sub-type js-sub-type-2" style="margin-bottom: 5px;">
                                    <span class="form-field-title"><i class="required">*</i>注册电话：</span>
                                    <span class="form-field-input"><input id="registerPhone" name="registerPhone" class="form-fields" placeholder="请填写公司注册电话" value="${invoice.registerPhone}"><label class="errorText"></label></span>
                                </p>
                            </div>

                            <div class="item">
                                <p class="js-invoice-sub-type js-sub-type-2 js-sub-type-1">
                                    <span class="form-field-title"><i class="required">*</i>开户银行：</span>
                                    <span class="form-field-input"><input id="depositBank" name="depositBank" class="form-fields" placeholder="请填写公司开户银行" value="${invoice.depositBank}"><label class="errorText"></label></span>
                                </p>

                                <p class="js-invoice-sub-type js-sub-type-2" style="margin-bottom: 5px;">
                                    <span class="form-field-title"><i class="required">*</i>银行账号：</span>
                                    <span class="form-field-input"><input id="bankAccountNum" name="bankAccountNum" class="form-fields" placeholder="请填写公司开户银行账号" value="${invoice.bankAccountNum}"><label class="errorText"></label></span>
                                </p>
                            </div>

                            <div class="js-invoice-sub-type js-sub-type-2 invoice-tips "><span>必须录入正确的税号(15位或18位)，以免无法开票。</span></div>

                            <span class="set-default-normal-tax">
                                 <div class="layui-input-block" style="margin-left: 0px">
                                    <input type="checkbox" lay-skin="switch" id="taxInvoice" name="commonInvoice" lay-filter="taxInvoice" value="${invoice.commonInvoice}">
                                    <label class="label">设为默认发票</label>
                                </div>
                            </span>

                        </div>
                    </div>
                    <!-- 基本信息 -->
                </div>
                <!-- 增值税发票 -->
                </form>

            </div>
            <div class="btns clear">
                <button class="confirm" type="button" onclick="saveInvoice();">保存</button>
                <button class="cancel" type="button" onclick="cancel();">取消</button>
            </div>
        </div>
    </div>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript"  src="<ls:templateResource item='/resources/templets/js/addInvoice.js'/>"></script>
<script type="text/javascript">
    var contextPath = "${contextPath}";
    var invoiceId = "${invoice.id}";
    var invoiceType = "${invoice.type}";
    var commonInvoice = "${invoice.commonInvoice}";
    var title = "${invoice.title}";

</script>
</body>
</html>