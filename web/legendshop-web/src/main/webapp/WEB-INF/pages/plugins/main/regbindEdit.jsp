<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>绑定账号-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="loginback">
<div>
    <div class="yt-wrap">
        <div class="lr_tab">
            <a class="on" href="javascript:void(0);">登 录</a><a href="${contextPath}/reg">注 册</a>
        </div>
        <div class="logintop"><a href="${contextPath}/"><img src="<ls:photo item="${systemConfig.logo}"/>" style="max-height:89px;"/>
        </a></div>
    </div>

    <!--login-->
    <div class="regbox">

        <div class="other_lg2" style="padding-top:30px;">
            <div class="v_align_mid"><p>
                <c:choose>
                    <c:when test="${passport.type=='qq'}">
                        QQ绑定：&nbsp;&nbsp;<img src="<ls:templateResource item='/resources/templets/images/qq.png'/>" />&nbsp;${passport.nickName}
                    </c:when>
                    <c:when test="${passport.type=='weibo'}">
                        微博绑定：&nbsp;&nbsp;<img src="<ls:templateResource item='/resources/templets/images/weibo.png'/>" />&nbsp;${passport.nickName}
                    </c:when>
                    <c:when test="${passport.type=='weixin'}">
                        微信绑定：&nbsp;&nbsp;<img src="<ls:templateResource item='/resources/templets/images/weixin.png'/>" />&nbsp;${passport.nickName}
                    </c:when>
                </c:choose>
            </p></div>
            <form:form id="bindAccountForm" method="post" action="${contextPath}/doExistedUserBinding" onsubmit="return validateRandNum('${contextPath}');">
                <div class="o-form">
                    <input type="hidden" id="rand" name="rand"/>
                    <input type="hidden" id="cannonull" name="cannonull" value='<fmt:message key="randomimage.errors.required"/>'/>
                    <input type="hidden" id="charactors4" name="charactors4" value='<ls:i18n key="randomimage.charactors.required" length="4"/>'/>
                    <input type="hidden" id="errorImage" name="errorImage" value='<fmt:message key="error.image.validation"/>'/>
                    <input type="hidden" id="returnUrl" name="returnUrl" value="${param.returnUrl}"/>
                    <h3>已有账号？直接绑定</h3>

                    <div class="item">
                        <span class="label">手机/用户名：</span>

                        <div class="item-ifo">
                            <input tabindex="6" class="text" onpaste="return false;" autocomplete="off" name="userName" id="loginname" type="text">
                            <label id="loginname_error" class=""></label>
                        </div>
                    </div>

                    <div class="item">
                        <span class="label">密码：</span>
                        <div class="item-ifo">
                            <input class="text" type="password" id="nloginpwd" tabindex="7" name="password" autocomplete="off">
                            <label id="nloginpwd_error" class=""></label>
                        </div>
                    </div>

                    <div class="item">
                        <span class="label">验证码：</span>

                        <div class="item-ifo v_align_mid">
                            <input id="randNum" name="randNum" maxlength="6" autocomplete="off" tabindex="8" class="text" type="text" style="width: 80px;">
                            <img id="randImage" name="randImage" src="<ls:templateResource item='/validCoderRandom'/>" width="95" height="38"/> &nbsp;<a href="javascript:void(0)" onclick="javascript:changeRandImg('${contextPath}')" style="font-weight: bold;"><fmt:message key="change.random2"/></a>
                            <span class="clr"></span>
                            <label id="randNum_error" class=""></label>
                        </div>
                    </div>


                    <div class="item" style="padding-top:20px;">
                        <input class="btn-img btn-regist" id="loginsubmit" onclick="bindnow();" tabindex="9" style="width:170px;" value="绑定账号" type="button">
                        <a href="<ls:url address="/retrievepassword"/>">忘记密码？</a>
                    </div>
                    <div class="fc-gray">
                    	    绑定后，您的QQ和账户都可以登录系统
                    </div>
                </div>
            </form:form>
        </div>

        <div class="reg_b">
            <div style="padding-bottom:20px;">
                <div class=" bighint">
              	      只差一步，即可完成登录设置
                </div>
                <p>快速完成账号创建</p>
            </div>
            <form:form id="bindRegForm" method="post" cssClass="u-info" action="${contextPath}/doCreateMobileUserBinding">
                <div class="item">
                    <span class="label">手机号：</span>
                    <div class="fl item-ifo">
                        <div class="r_inputdiv">
                            <input id="username" name="mobile" autocomplete="off" onpaste="return false;" type="text" class="text">
                            <ul id="intelligent-regName" class="hide"></ul>
                            <label id="username_succeed" class=""></label>
                            <label id="username_error" class=""></label>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <span class="label">登录密码：</span>
                    <div class="fl item-ifo">
                        <div class="r_inputdiv">
                            <input id="pwd" name="password" type="password" class="text" autocomplete="off" sta="0">
                            <label id="pwd_succeed" class=""></label>
                            <label id="pwd_error"></label>
                            <label class="hide" id="pwdstrength"><span class="fl">安全程度：</span><b style="margin-top: 8px;"></b></label>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <span class="label">确认密码：</span>
                    <div class="fl item-ifo">
                        <div class="r_inputdiv">
                            <input id="pwd2" name="passwordOld" type="password" class="text" autocomplete="off" onpaste="return false;" sta="0">
                            <label id="pwd2_succeed" class=""></label>
                            <label id="pwd2_error"></label>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <span class="label">短信验证码：</span>
                    <div class="fl item-ifo">
                        <div class="r_inputdiv">
                            <input type="text" class="text text-1" id="erifyCode" maxlength="6" name="mobileCode">
                            <!-- <a class="btn" href="javascript:void(0);" onclick="sendMobileCode();" id="sendMobileCode">
                                    <span id="dyMobileButton">获取短信验证码</span></a> -->
                            <input class="btn" type="button" value="获取短信校验码" onclick="sendMobileCode()" id="erifyCodeBtn"/>
                            <label id="erifyCode_succeed" class=""></label>
                            <label id="erifyCode_error"></label>
                        </div>
                    </div>
                    <span class="clr"></span>
                </div>

                <div class="item" style=" height:30px; padding-top:0;">
                    <span class="label">&nbsp;</span>
                    <div class="fl item-ifo">
                        <input type="checkbox" checked="checked" class="checkbox" id="readme">
                        <label for="protocol">我已阅读并同意<a id="protocol" class="blue" href="javascript:void(0)">《网络服务和使用协议》</a></label>
                    </div>
                </div>
                <div class="item">
                    <span class="label">&nbsp;</span>
                    <input onclick="regnow();" tabindex="5" type="button" class="btn-img btn-regist" value="立即注册">
                </div>
                <div>
                    <span class="clr"></span>
                </div>
            </form:form>
        </div>
    </div>
    <!--login end-->
    <%@ include file="home/simpleBottom.jsp" %>
</div>
</body>
<%@ include file="/WEB-INF/pages/common/jquery.jsp" %>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/common/js/randomimage.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/regbindEdit.js'/>"></script>
<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
<script type="text/javascript">
    var contextPath = '${contextPath}';
    var errorCode = '${bindAccountErrorCode}';
</script>
</html>
