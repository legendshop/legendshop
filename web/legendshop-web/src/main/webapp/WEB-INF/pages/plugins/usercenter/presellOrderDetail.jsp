<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<%@ taglib uri="http://www.legendesign.net/date-util"  prefix="dateUtil" %>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}"  pattern="yyyy-MM-dd HH:mm" var="nowDate"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>预售订单详情 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<style>
		.order tbody tr td{
   		    border-bottom: 1px solid #e7e7e7;
   			vertical-align: top;
    	}
    	.ncm-order-contnet tfoot td dl{
    		margin-top:2px;
    		float: left;
    	}
		.ncm-order-contnet tfoot td dl dt{
			width:100px;
			color:#666;
			text-align: right;
			margin-right:20px;
		}
		.ncm-order-contnet tfoot td dl dd{
			min-width:65px;
		}
		.ncm-order-contnet tfoot td .deposit dd{
			
		}
		.ncm-order-contnet tfoot td .final dd{
			
		}
		
		.ncm-order-contnet tfoot td .order-price dt{
			font-size:14px;
			font-weight: bold;
		}
		.ncm-order-contnet tfoot td .order-price dd{
			font-size:14px;
			font-weight: bold;
			color:#e5004f;
		}
		.goods-name2 dt a:hover {
			color: #e5004f !important;
		}
	</style>
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con">
<div class="right-layout">
	<div class="ncm-oredr-show">
		<div class="ncm-order-info">
			<div class="ncm-order-details">
				<div class="title">订单信息</div>
				<div class="content">
					<dl>
						<dt>收货地址：</dt>
						<dd>
							<span>
								<c:if test="${not empty order.userAddressSub}"> 
								   ${order.userAddressSub.receiver} ,
								   ${order.userAddressSub.mobile} ,
								   ${order.userAddressSub.detailAddress}
								</c:if>
							 </span>
						</dd>
					</dl>
					<dl>
						<dt>发&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;票：</dt>
						<dd>	
						<c:choose>
								<c:when test="${empty order.invoiceSub}"> 
								    不开发票
								</c:when>
								<c:otherwise>
									${order.invoiceSub.type==1?"普通发票":"增值税发票"}&nbsp;
									<c:choose>
										<c:when test="${order.invoiceSub.title==1}">[个人抬头]</c:when>
										<c:otherwise>[公司抬头]</c:otherwise>
									</c:choose>&nbsp;
									${order.invoiceSub.company}&nbsp;
									<c:if test="${order.invoiceSub.title ne 1}">(${order.invoiceSub.invoiceHumNumber})</c:if>
								</c:otherwise>
							</c:choose>
						</dd>
					</dl>
					<dl>
						<dt>买家留言：</dt>
						<dd>
							<c:choose>
								<c:when test="${empty order.orderRemark}">无</c:when>
								<c:otherwise>${order.orderRemark}</c:otherwise>
							</c:choose>
						</dd>
					</dl>
					 <dl>
                        <dt>支付方式：</dt>
						<dd>
						 <c:choose>
							<c:when test="${not empty order.payTypeName}"> 
							    ${order.payTypeName}
							</c:when>
							<c:otherwise>
							 	 未支付
							</c:otherwise>
						</c:choose>
						</dd>
                             </dl>
					<dl>
						<dt>订单编号：</dt>
						<dd>
							${order.subNo}
						</dd>
					</dl>
					<dl>
						<dt>商&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家：</dt>
						<dd>
							${order.shopName}
						</dd>
					</dl>
				</div>
			</div>
			<div class="ncm-order-condition">
				<dl>
					<dt>
						<i class="icon-ok-circle green"></i>订单状态：
					</dt>
					<dd>
					  <c:choose>
					  	<c:when test="${order.refundState eq 1}">
					  		退款退货中
					  	</c:when>
					  	<c:otherwise>
					  		<c:choose>
								 <c:when test="${order.status==1 }">
								        订单已经提交，等待买家付款
								 </c:when>
								 <c:when test="${order.status==2 && order.isPayDeposit eq 1 && order.isPayFinal eq 1 }">
								       待发货
								 </c:when>
								 <c:when test="${order.status==2 && order.payPctType eq 0}">待发货</c:when> 
								 <c:when test="${order.status==3 }">
								       待收货
								 </c:when>
								 <c:when test="${order.status==4 }">
								      已完成
								 </c:when>
								 <c:when test="${order.status==5 || order.status==-1 }">
								      交易关闭
								 </c:when>
								 <c:otherwise>
								 	待付尾款
								 </c:otherwise>
						  </c:choose>
					  	</c:otherwise>
					  </c:choose>
					</dd>
				</dl>
				<ul>
				<c:if test="${order.refundState ne 1}">
					<c:choose>
					   <c:when test="${order.status eq 1}">
				      	   <li>1. 您尚未对该订单进行支付，请进行支付以确保商家及时发货。</li>
<%--						   <li>2. 如果您不想购买此订单的商品，请选择<a class="ncbtn-mini" href="javascript:void(0);" onclick="cancleOrder('${order.subId}')">取消订单</a>操作。</li>--%>
<!-- 					   <li>3. 如果您未对该笔订单进行支付操作，系统将于 <time>2015-07-20 16:11:49</time>自动关闭该订单。</li>
 -->					</c:when>
                        <c:when test="${order.status eq 2}">
                            <li>1. 订单已提交商家进行备货发货准备。</li>
                             <!-- <li>2. 如果您想取消购买，请与商家沟通后对订单进行<a href="#order-step" class="ncbtn-mini">申请退款</a>操作。</li> -->
		        		</c:when>
				         <c:when test="${order.status eq 3}">
					        <li>1. 商品已发出；  物流公司：${order.delivery.delName}；  查看 <a class="blue" href="#order-step">“物流跟踪”</a> 情况。</li>
					        <li>2. 如果您已收到货，且对商品满意，您可以<a class="ncbtn-mini ncbtn-mint" href="#order-step">确认收货</a>完成交易。</li>
						    <c:if test="${not empty order.dvyDate }">
					           <c:set var="confirmTime"  value="${prodApi:getOrderConfirmTime(order.dvyDate)}"></c:set>
					           <li>3. 系统将于<time><fmt:formatDate value="${confirmTime}" type="both"/></time>自动完成“确认收货”，完成交易。</li>
					        </c:if>
				        </c:when>
				        <c:when test="${order.status eq 4}">
							<li>1. 如果收到货后出现问题，您可以联系商家协商解决。</li>
					<!-- 	<li>2. 如果商家没有履行应尽的承诺，您可以申请 <a class="red" href="#order-step">"投诉维权"</a>。</li> -->
	   						<li>2. 交易已完成，你可以对购买的商品及商家的服务进行<a class="ncbtn-mini ncbtn-aqua" href="#order-step">评价</a>及晒单。</li>
				        </c:when>

						<c:when test="${(order.status eq 5 || order.status eq -1) && order.refundState ne 2}">
							<li>买家 于<fmt:formatDate value="${order.updateDate}" type="both" /> 取消了订单 原因：${order.cancelReason}</li>
						</c:when>
						<c:when test="${(order.status eq 5 || order.status eq -1) && order.refundState eq 2}">
							<li>商家主动发起退款 取消了订单 原因：${order.cancelReason}</li>
						</c:when>
					  </c:choose>
				</c:if>
				</ul>
			</div>
		</div>
		
		<div class="ncm-order-step" id="order-step">
			<dl class="step-first current">
				<dt>生成订单</dt>
				<dd class="bg"></dd>
				 <dd title="下单时间" class="date"><fmt:formatDate value="${order.subCreateTime}" type="both" /></dd> 
			</dl>
<%--			<dl class="<c:if test="${order.isPayDeposit eq 1 && order.isPayFinal eq 1 || (order.payPctType eq 0 && order.status==2)}">current</c:if>">--%>
			<dl class="<c:if test="${order.isPayDeposit eq 1 && order.isPayFinal eq 1 || (order.payPctType eq 0 && order.isPayed==1)}">current</c:if>">
				<dt>完成付款</dt>
				<dd class="bg"></dd>
<%--				<c:if test="${order.isPayDeposit eq 1 && order.isPayFinal eq 1 || (order.payPctType eq 0 && order.status==2)}"><dd title="付款时间" class="date"><fmt:formatDate value="${order.payDate}" type="both" /></dd></c:if>--%>
				<c:if test="${order.isPayDeposit eq 1 && order.isPayFinal eq 1 || (order.payPctType eq 0 && order.isPayed==1)}"><dd title="付款时间" class="date"><fmt:formatDate value="${order.payDate}" type="both" /></dd></c:if>
			</dl>
			<dl class="<c:if test="${not empty order.dvyDate}">current</c:if>">
				<dt>商家发货</dt>
				<dd class="bg"></dd>
				<c:if test="${not empty order.dvyDate}"><dd title="发货时间" class="date"><fmt:formatDate value="${order.dvyDate}" type="both" /></dd></c:if>
			</dl>
			<dl class="<c:if test="${not empty order.finallyDate}">current</c:if>">
				<dt>确认收货</dt>
				<dd class="bg"></dd>
				<c:if test="${not empty order.finallyDate}"><dd title="确认收货时间" class="date"><fmt:formatDate value="${order.finallyDate}" type="both" /></dd></c:if>
			</dl>
			<dl class="<c:if test="${not empty order.finallyDate}">current</c:if>">
				<dt>完成</dt>
				<dd class="bg"></dd>
				<c:if test="${not empty order.finallyDate}"><dd title="完成时间" class="date"><fmt:formatDate value="${order.finallyDate}" type="both" /></dd></c:if>
			</dl>
		</div>
		
		<div class="ncm-order-contnet">
			<table class="ncm-default-table order sold-table">
				<thead>
					<tr>
						<th colspan="2">商品名称</th>
						<th class="w120 tl">单价（元）</th>
						<th class="w90">数量</th>
						<th class="w100">交易状态</th>
						<th class="w100" style="border-right:1px solid #e7e7e7">交易操作</th>
					</tr>
				</thead>
				<tbody>
				
				  <c:if test="${order.status==3 || order.status==4}">
				    <tr>
						<th style="border-right: none;" colspan="10">
							<div class="order-deliver">
								<span>物流公司： <a href="${order.delivery.delUrl}" target="_blank">${order.delivery.delName}</a></span>
								<span> 物流单号： ${order.delivery.dvyFlowId} </span>
								<span><a id="show_shipping" href="javascript:void(0);">物流跟踪<i class="icon-angle-down"></i>
										<div class="more">
											<span class="arrow"></span>
											<ul id="shipping_ul">
												<li>加载中...</li>
											</ul>
										</div> </a>
								</span>
							</div>
						</th>
					</tr>
				  </c:if>

					
					<!-- S 商品列表 -->
				<c:forEach items="${order.orderItems}" var="orderItem" varStatus="orderItemStatues">
				    <tr class="bd-line">
						<td class="w70">
							<div class="ncm-goods-thumb" style="margin-left: 10px;">
								<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">
									<img src="<ls:images item='${orderItem.prodPic}' scale='3'/>">
								</a>
							</div>
						</td>
						<td class="tl"><dl class="goods-name2">
							<dt>
								<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">${orderItem.prodName}</a>
								<c:if test="${not empty orderItem.snapshotId && orderItem.snapshotId!=0}" >
									<span class="rec">
									<a  href="<ls:url address='/snapshot/${orderItem.snapshotId}'/>" target="_blank">[交易快照]</a>
								    </span>
								 </c:if>
							</dt>
							<dd>${orderItem.attribute}</dd>
							<!-- 消费者保障服务 -->
						  </dl>
						</td>
						<td class="tl refund">
							<span id="actualTotal">${orderItem.prodCash}</span>
							<p class="green"></p>
						</td>
						<td>x${orderItem.basketCount}</td>
						<!-- <td></td> -->
						<!-- <td>
							退款 投诉
					   </td> -->

						<!-- S 合并TD -->
                    <c:if test="${orderItemStatues.index==0}">  
	                   <c:choose>
		                		<c:when test="${fn:length(order.orderItems)>0}">
		                			<!-- 未支付 -->
		                			<td rowspan="${fn:length(order.orderItems)}" class="bdl" colspan="${order.status ne 5?1:2}" style="border-right:1px solid #d8d8d8;">
		                		</c:when>
		                		<c:otherwise>
		                			<td class="bdl" style="border-right:1px solid #d8d8d8;">
		                		</c:otherwise>
		                </c:choose>
	                   <c:choose>
		                		<c:when test="${order.refundState ne 1}">
		                			 <c:choose>
										 <c:when test="${order.status==1 }">
										        待付款
										 </c:when>
										 <c:when test="${order.status==2 && order.isPayDeposit eq 1 && order.isPayFinal eq 1}">
										       待发货
										 </c:when>
										 <c:when test="${order.status==2 && order.payPctType eq 0}">待发货</c:when>
										 <c:when test="${order.status==3 }">
										       待收货
										 </c:when>
										 <c:when test="${order.status==4 }">
										      已完成
										 </c:when>
										 <c:when test="${order.status==5 || order.status==-1}">
										      交易关闭
										 </c:when>
										 <c:otherwise>
										     待付尾款
										 </c:otherwise>
									  </c:choose>
		                		</c:when>
		                		<c:otherwise>
		                			退款退货中
		                		</c:otherwise>
		                </c:choose>
		                </td>
						<c:if test="${order.status ne 5 }">
						  <c:choose>
		                		<c:when test="${fn:length(order.orderItems)>0}">
		                			<!-- 未支付 -->
		                			<td rowspan="${fn:length(order.orderItems)}" class="bdl" style="border-right:1px solid #d8d8d8;">
		                		</c:when>
		                		<c:otherwise>
		                			<td class="bdl" style="border-right:1px solid #d8d8d8;">
		                		</c:otherwise>
		                </c:choose>
							<!-- 取消订单 -->
							<p>
							<c:if test="${order.refundState ne 1}">
							   <c:choose>
								 <c:when test="${order.status eq 1 }">
								 	<c:choose>
								 		<c:when test="${order.isPayDeposit eq 0 && order.isPayFinal eq 0}">  <!-- 未支付 -->
								 			<a class="btn-r" href="javascript:void(0)" onclick="cancleOrder('${order.subNo}');">取消订单</a>
								 		</c:when>
								 	</c:choose>
								 </c:when>
								 <c:when test="${order.status eq 2 }">
								    <!--  <a class="ncbtn" href="index.php?act=member_refund&amp;op=add_refund_all&amp;order_id=335">订单退款</a> -->
								 </c:when>
								 <c:when test="${order.status eq 3 }">
								    <a class="btn-r" href="javascript:void(0)" onclick="javascript:orderReceive('${order.subNo}');">确认收货</a>
								 </c:when>
								 <c:when test="${order.status eq 4 }">
								       <a class="btn-r" href="${contextPath}/p/prodcomment">评论商品</a>
								 </c:when>
							  </c:choose>
							</c:if>
						</p>
						</td>
						</c:if>
					</c:if>
						<!-- E 合并TD -->
					</tr>
				</c:forEach>
				<tr style="color:#999;">
	                <td colspan="2">阶段1：定金</td>
	                <td colspan="1">
						<c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
						¥<span class="preDepositPrice"><fmt:formatNumber value="${order.depositPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
						(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
						</c:if>
						<c:if test="${order.payPctType eq 1}"><!-- 定金支付 -->
						¥<span class="preDepositPrice"><fmt:formatNumber value="${order.depositPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
						</c:if>
	                </td>
	                <c:if test="${order.status eq 5 }">
	                	<td colspan="3" style="border-right: 1px solid #e6e6e6;"></td>
	                </c:if>
	                <c:if test="${order.status ne 5 }">
		                <c:if test="${order.isPayDeposit eq 0}">
		                	<c:if test="${dateUtil:getOffsetMinutes(order.subCreateTime,nowTime) le 60}">
		                		<td colspan="2">需在<font color="#e5004f">${60 - dateUtil:getOffsetMinutes(order.subCreateTime,nowTime)}</font>分钟之内付款</td>
		                		<td style="border-right: 1px solid #e6e6e6;">
			                		<a href="${contextPath}/p/presell/order/payDeposit/1/${order.subNo}/${order.activeId}" class="a-block">付款</a>
			                	</td>
		                	</c:if>
		                	<c:if test="${dateUtil:getOffsetMinutes(order.subCreateTime,nowTime) gt 60}">
		                		<td colspan="3" style="border-right: 1px solid #e6e6e6;">已过期</td>
		                	</c:if>
		                </c:if>
		                <c:if test="${order.isPayDeposit eq 1 }">
		                	<td colspan="3" style="border-right: 1px solid #e6e6e6;">
		                	     <%--  <font color="green">已完成</font><br/>
		                	      <font color="green">${order.depositPayName}</font><br/>
		                	     <font color="green">${order.depositTradeNo}</font><br/> --%>
		                	     <div>
			                	     <span>已完成</span>
			                	     <span style="margin-left: 30px;">${order.depositPayName}</span>
		                	     </div>
				                 <div>${order.depositTradeNo}</div>
		                	</td>
		                </c:if>
	                </c:if>
	              </tr>
	              <tr style="color:#999;">
	                <td colspan="2">阶段2：尾款</td>
	                <td colspan="1">¥<span id="finalPrice" class="finalPrice">
						<c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
						¥<span class="finalPrice"><fmt:formatNumber value="${order.finalPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
						</c:if>
						<c:if test="${order.payPctType eq 1}"><!-- 定金支付 -->
						¥<span class="finalPrice"><fmt:formatNumber value="${order.finalPrice+order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber></span>
						(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
						</c:if>
					</td>
	                <c:if test="${order.status eq 5 }">
		               	<td colspan="4"></td>
		            </c:if>
	                <c:if test="${order.status ne 5 }">
	                		<c:if test="${order.isPayFinal eq 0}">
		                		<td colspan="2" >
				                	尾款支付时间: </br>
				                	<fmt:formatDate value="${order.finalMStart }" pattern="yyyy-MM-dd" /> - <fmt:formatDate value="${order.finalMEnd }" pattern="yyyy-MM-dd" />
				                </td>
				                 <td colspan="2" style="border-right: 1px solid #e6e6e6;">
					             <c:if test="${order.isPayFinal eq 0 && order.refundState ne 1}">
										<c:choose>
					                		<c:when test="${order.finalMStart gt nowTime }">
					                			未开始
					                		</c:when>
					                		<c:when test="${order.finalMStart le nowTime and order.finalMEnd gt nowTime}">
						                		<a href="${contextPath}/p/presell/order/payDeposit/2/${order.subNo}/${order.activeId}" class="a-block">付尾款</a>
					                		</c:when>
					                		<c:when test="${order.finalMEnd le nowTime }">
					                			已过期
					                		</c:when>
					                	</c:choose>
				                  </c:if>
			                  </td>
			               </c:if>
			                 <c:if test="${order.isPayFinal eq 1 }">
			                     <td colspan="4" style="border-right: 1px solid #e6e6e6;">
					                 	<font color="green">已完成</font><br/>
			                	        <font color="green">${order.finalPayName}</font><br/>
			                	        <font color="green">${order.finalTradeNo}</font><br/>
			                	        </td>
					        </c:if>
		             </c:if>
	              </tr>
				</tbody>
				<tfoot>
					<tr>
						<td></td><td></td><td></td>
						<td colspan="20">
<%-- 							<dl class="freight">
								<dt>运费：</dt>
								<dd>
									<c:choose>
										 <c:when test="${empty order.freightAmount || order.freightAmount==0}">
										    免运费
										 </c:when>
										 <c:otherwise>
										    +￥${order.freightAmount}
										 </c:otherwise>
									 </c:choose>
								</dd>
							 	</dl> --%>
								<c:if test="${not empty order.discountPrice && order.discountPrice!=0}">
									<dl><dd>促销优惠： -￥${order.discountPrice}</dd></dl>
								</c:if>
								<c:if test="${not empty order.couponOffPrice && order.couponOffPrice!=0}">
									<dl><dd>优惠券： -￥${order.couponOffPrice}</dd></dl>
								</c:if>
								<dl class="deposit">
									<dt>商品定金：</dt>
									<dd>
										<em>￥<span class="preDepositPrice"><fmt:formatNumber value="${order.depositPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span></em>元
										<c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
											(含运费￥<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
										</c:if>
									</dd>
								</dl>
								<dl class="final">
									<dt>商品尾款：</dt>
									<dd>
										<c:if test="${order.payPctType eq 0}">
											<em>￥<span class="finalPrice">0.00
										</c:if>
										<c:if test="${order.payPctType eq 1}">
											<em>￥<span class="finalPrice"><fmt:formatNumber value="${order.finalPrice + order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber></span></em>元
											(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
										</c:if>
									</dd>
								</dl>
								<dl class="order-price">
									<dt>订单金额：</dt>
									<dd>
										<%-- 全额支付 --%>
										<c:if test="${order.payPctType eq 0}">
											<em>￥<fmt:formatNumber value="${order.depositPrice}" type="currency" pattern="0.00"></fmt:formatNumber></em>元
										</c:if>
										<c:if test="${order.payPctType eq 1}">
											<em>￥<fmt:formatNumber value="${order.depositPrice+order.finalPrice+order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber></em>元
										</c:if>
									</dd>
								</dl>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
<div class="clear"></div>
   </div>
        <!-------------订单end---------------->
       
     </div>
    <!-----------right_con end-------------->
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/ToolTip.js'/>"></script>
<script type="text/javascript">
	//取消订单
	var cancleOrder=function(id){
		layer.open({
			title : '取消订单',
			id : 'cancleOrder',
			type : 2,
			content : [contextPath + "/p/order/cancleOrderShow?subNumber="+id, 'no'],
			area: ['430px', '210px'],
		});
	}
	
	//确认收货
		function orderReceive(_number){
		layer.open({
			title : '确认收货',
			id : 'cancleOrder',
			type : 1,
			content: '<div class="eject_con"><dl><dt>订单编号：</dt><dd>'+ _number +' <p class="hint">请注意： 如果你尚未收到货品请不要点击“确认”。大部分被骗案件都是由于提前确认付款被骗的，请谨慎操作！ </p>'
	            +" </dd></dl></div>",
			area: ['430px', '290px'],
			btn: ['确定','关闭'],
			yes: function(){
					$.ajax({
						url:"${contextPath}/p/orderReceive/"+_number, 
						type:'POST', 
						async : false, //默认为true 异步   
						dataType:'text',
						success:function(result){
							if(result=="OK"){
								var msg = "确认收货成功";
								layer.alert(msg,{icon: 1});
								window.location.reload();
							}else if(result=="fail"){
								var msg = "确认收货失败";
								layer.alert(msg,{icon:2});
							}
						}
					});
			  }
		});
	}
	
	var loaddely=false;
	$(function(){
		userCenter.changeSubTab("presellOrder");
		
	    $('#show_shipping').on('hover',function(){
	       var dvyFlowId="${order.delivery.dvyFlowId}";
	       var dvyTypeId="${order.delivery.dvyTypeId}";
	       var html="";
	       if(dvyFlowId=="" || queryUrl==""){
	          html="没有物流信息";
	       }
	       else{
	          if(!loaddely){
	             $.ajax({
					url:contextPath+"/p/order/getExpress", 
					data:{"dvyFlowId":dvyFlowId,"dvyId":dvyTypeId},
					type:'POST', 
					async : false, //默认为true 异步   
					dataType:'json',
					success:function(result){
					    if(result=="fail" || result=="" ){
					       html="没有物流信息";
					       loaddely=true;
						}else{
						     var list = eval(result);
							 for(var obj in list){ //第二层循环取list中的对象 
							    html+="<li>"+list[obj].context+"<li>";
							 } 
							 loaddely=true;
						}
					}
			      });
	           }
	       }
	       
		    $('#shipping_ul').html(html);
			$('#show_shipping').unbind('hover'); 
	    });
});
	// $(function () {
	// 	//初始化定金金额
	// 	var presellPrice =$("#actualTotal").text(); // 预售价
	// 	var preDepositPrice = $("#preDepositPrice");// 定金价
	// 	var finalPayment =$("#finalPrice"); // 尾款
	// 	var percentNum = parseFloat(preDepositPrice.text()/100);//定金百分比
	// 	var preVal = parseFloat(presellPrice*percentNum)
	// 	var finalPayVal = parseFloat(presellPrice-preVal);
	// 	$(".preDepositPrice").text(numberConvert(preVal));
	// 	$(".finalPrice").text(numberConvert(finalPayVal));
	// })
	function numberConvert(num) {

		var numConvert = Math.round(num * 1000) / 1000;
		var number = numConvert.toString();

		var index =  number.indexOf('.');
		var price = index != -1?number.substring(0,index+3):number;
		return price;
	}
</script>
</body>
</html>