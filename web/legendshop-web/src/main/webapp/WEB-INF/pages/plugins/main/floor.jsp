<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<c:set var="floorList"  value="${indexApi:findFloor()}"></c:set>
<c:forEach  items="${floorList}" var="floor"  varStatus="floorIndex">
   <c:choose>
      <c:when test="${floor.layout eq 'wide_blend'}">
           <%@include file='floor/floor_blend.jsp'%>
      </c:when>
       <c:when test="${floor.layout eq 'wide_goods'}">
          <%@include file='floor/floor_goods.jsp'%>
       </c:when>
       <c:when test="${floor.layout eq 'wide_adv_square_four'}">
          <%@include file='floor/floor_adv_square_four.jsp'%>
       </c:when>
       <c:when test="${floor.layout eq 'wide_adv_rectangle_four'}">
          <%@include file='floor/floor_adv_rectangle_four.jsp'%>
       </c:when>
       <c:when test="${floor.layout eq 'wide_adv_five'}">
          <%@include file='floor/floor_wide_adv_five.jsp'%>
       </c:when>
       <c:when test="${floor.layout eq 'wide_adv_eight'}">
          <%@include file='floor/floor_wide_adv_eight.jsp'%>
       </c:when>
        <c:when test="${floor.layout eq 'wide_adv_row'}">
          <%@include file='floor/floor_wide_adv_row.jsp'%>
        </c:when>
        <c:when test="${floor.layout eq 'wide_adv_brand'}">
           <%@include file='floor/floor_wide_adv_brand.jsp'%>
        </c:when>
   </c:choose>
</c:forEach>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/floor.js'/>"></script>
<script type="text/javascript" src="${contextPath}/resources/common/js/jquery.lazyload.min.js"></script>
<script>
$("img.lazy").lazyload({
    effect : "fadeIn"
});
</script>	  