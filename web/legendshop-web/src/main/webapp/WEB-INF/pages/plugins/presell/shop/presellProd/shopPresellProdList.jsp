<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss"
	var="nowDate" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<title>预售活动管理-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/active/actives${_style_}.css'/>" rel="stylesheet">
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css' /> rel="stylesheet">
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/presell/serller-presell-prod${_style_}.css'/>" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置> <a href="${contextPath}/home">首页</a>> <a href="${contextPath}/sellerHome">卖家中心</a>> <span class="on">预售活动管理</span>
				</p>
			</div>

			<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp"%>
		<div id="rightContent" class="right_con">
				<div id="statusTab" class="presell-status-nav" style="margin:20px 0px 15px 0px;">
					<span <c:if test="${empty searchType or searchType eq 'ALL'}">class="on"</c:if> value="ALL">全部活动</span>
					<span <c:if test="${searchType eq 'WAIT_AUDIT'}">class="on"</c:if> value="WAIT_AUDIT">待审核</span>
					<span <c:if test="${searchType eq 'NOT_PASS'}">class="on"</c:if> value="NOT_PASS">未通过</span>
					<span <c:if test="${searchType eq 'NOT_STARTED'}">class="on"</c:if> value="NOT_STARTED">未开始</span>
					<span <c:if test="${searchType eq 'ONLINE'}">class="on"</c:if> value="ONLINE">进行中</span>
					<span <c:if test="${searchType eq 'FINISHED'}">class="on"</c:if> value="FINISHED">已结束</span>
					<span <c:if test="${searchType eq 'EXPIRED'}">class="on"</c:if> value="EXPIRED">已失效</span>
				</div>
				<!--page-->
				<!-- 预售活动列表 -->
				<div class="seller-advance">
					<!-- 添加/搜索 -->
					<div class="search-add">
						<div class="set-up sold-ser sold-ser-no-bg clearfix" style="overflow:hidden">
							<div class="item">
								点击添加新的预售活动：<input class="btn-r big-btn add-btn" style="display:inline-block;" onclick="location='${contextPath}/s/presellProd/load'"  value="立即添加">
							</div>

							<form:form id="searchPresellProd" action="${contextPath }/s/presellProd/listContent" method="GET">
								<input type="hidden" id="curPageNO" name="curPageNO" value="1" />
								<input type="hidden" id="searchType" name="searchType" value="${searchType}"/>
								<div class="fr">
									<div class="item">
										活动名称：<input type="text" placeholder="请输入活动名称" name="schemeName" id="schemeName" value="${group.groupName}" class="text item-inp">
										<input type="button" value="搜索" class="submit btn-r" onclick="search();" style="display:inline-block">
										<input type="reset" value="清空" class="btn-g"/>
									</div>
								</div>
							</form:form>
						</div>
					</div>
					<!-- 添加/搜索 -->
					<!-- 搜索订单 -->
					<div id="presellList"></div>
					
				</div>
		</div>
	</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
</div>
	<script type="text/javascript">
		var contextPath = '${contextPath}';
		$(function(){
			laydate.render({
				   elem: '#preSaleStart',
				   calendar: true,
				   theme: 'grid',
				   trigger: 'click'
			  });
	     
	     	laydate.render({
				   elem: '#preSaleEnd',
				   calendar: true,
				   theme: 'grid',
				   trigger: 'click'
			  });
		});
	</script>
	<script src="<ls:templateResource item='/resources/common/js/serialize-form.js'/>" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/templets/js/presell/presellProdList.js'/>" type="text/javascript"></script>
</body>
</html>
