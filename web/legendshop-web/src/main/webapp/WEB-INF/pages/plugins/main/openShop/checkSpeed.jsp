<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
  <title>审核状态-${systemConfig.shopName}</title>
  <meta name="keywords" content="${systemConfig.keywords}"/>
  <meta name="description" content="${systemConfig.description}" />
<%--   <meta http-equiv="refresh" content="10;URL=${contextPath}/"> --%>
</head>

<body>
<div id="doc">
   
   <div id="hd">
   			<%@ include file="../home/top.jsp" %>
   </div>
   
   
   <div id="bd">
       <div class="yt-wrap" style="padding:0px 0 20px;">
            <div class="seller-cru" style="width:1190px;margin: 20px auto 20px;">
              <p>
                您的位置&gt;
                <a href="#">首页</a>&gt;
                <span style="color: #e5004f;">免费开店</span>
              </p>
            </div>
            <div class="main">
              <div class="settled_prompt">
                <ul>
                  <li style="font-weight: bold;">咨询审核</li>
                  <li style="margin-left: 50px;">1、审核时效：店铺审核会在7个工作日完成。 </li>
                  <li style="margin-left: 50px;">2、您可登录查看审核状态。</li>
                  <li style="margin-left: 50px;">3、店铺开通后，可以登录到卖家中心查看相关信息。</li>
                  </ul>
                </div>
              <div class="settled_box">
               <h3><span class="settled_title">店铺状态</span></h3>
             <div class="settled_white">
              <div class="settled_white_box">                     
                <div class="class_settle">

                  <table class="class_settle_table" border="0" cellpadding="0" cellspacing="0" style="font-size: 12px;">
                  <tr>
                  	<th>审核结果</th>
                  	<th width="200">时间</th>
                  	<th>审核意见</th>
                  	<th width="250">操作</th>
                  </tr>
                  <c:choose>
	                  <c:when test="${shopDetailInfo.status eq -1 && shopDetailInfo.opStatus eq 0}">
		                  <tr>
		                    <td align="center" valign="top" width="200">您已成功提交入驻申请</td> 
		                   	<td><fmt:formatDate value="${shopDetailInfo.modifyDate}" pattern="yyyy-MM-dd  HH:mm:ss"/></td>
		                    <td align="center" valign="top">等待审核</td>
		                    <td></td>
		                  </tr>
	                  </c:when>
	                   <c:when test="${shopDetailInfo.status eq -1 && shopDetailInfo.opStatus eq 4}">
		                  <tr>
		                    <td align="center" valign="top" width="200">等待审核</td> 
		                   <td><fmt:formatDate value="${shopDetailInfo.modifyDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
		                    <td align="center" valign="top">审核中</td>
		                    <td></td>
		                  </tr>
                      </c:when>
	                   <c:when test="${shopDetailInfo.status eq 1}">
	               		<td align="center" valign="top" width="300">开店成功</td>
	               		<td align="center"><fmt:formatDate value="${shopDetailInfo.modifyDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td> 
	               		<td></td>
		                <td align="center" valign="top">
				            <font color="green" style="float: left;margin-top: 5px;">开店成功，请&nbsp;</font><b><a class="settled_btn" href="${contextPath}/p/logout">退出重新登录！</a></b>
				        </td>
		                  <c:forEach items="${shopAuditList}" var="shopAudit"  varStatus="a">
		                  	<c:choose>
		                  		<c:when test="${shopAudit.status eq -4 }">
		                  			<tr>
					                    <td align="center" valign="top" width="300">提交联系人信息有误</td> 
					                   	<td align="center"><fmt:formatDate value="${shopAudit.modifyDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					                   	<td align="center">${shopAudit.auditOpinion}</td>
					                   	<td>
					                   	</td>
			               		 	</tr>
		                  		</c:when>
		                  		<c:when test="${shopAudit.status eq -5 }">
		                  			<tr>
					                    <td align="center" valign="top" width="300">提交公司信息有误</td> 
					                   	<td align="center"><fmt:formatDate value="${shopAudit.modifyDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					                   	<td align="center">${shopAudit.auditOpinion}</td>
					                   	<td>
					                   	</td>
			               		 	</tr>
		                  		</c:when>
		                  		<c:when test="${shopAudit.status eq -6 }">
		                  			<tr>
					                    <td align="center" valign="top" width="300">提交店铺信息有误</td> 
					                   	<td align="center"><fmt:formatDate value="${shopAudit.modifyDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					                   	<td align="center">${shopAudit.auditOpinion}</td>
					                   	<td>
					                   	</td>
			               		 	</tr>
		                  		</c:when>
		                  		<c:when test="${shopAudit.status eq -6 }">
		                  			<tr>
					                    <td align="center" valign="top" width="300">提交店铺信息有误</td> 
					                   	<td align="center"><fmt:formatDate value="${shopAudit.modifyDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					                   	<td align="center">${shopAudit.auditOpinion}</td>
					                   	<td>
					                   	</td>
			               		 	</tr>
		                  		</c:when>
		                  	</c:choose>
		                  </c:forEach>
	              	 </c:when>
	                 <c:otherwise>
		                  <c:forEach items="${shopAuditList}" var="shopAudit"  varStatus="a">
		                  <c:choose>
		                  	<c:when test="${a.first}">
					                 <c:choose>
					                 	<c:when test="${shopAudit.status eq -4 or shopAudit.status eq -5 or shopAudit.status eq -6}">
						                 	<tr>
												<c:choose>
													<c:when test="${shopAudit.status eq -4}">
														<td align="center" valign="top" width="200">提交联系人信息有误</td>
													</c:when>
													<c:when test="${shopAudit.status eq -5}">
														<td align="center" valign="top" width="200">提交公司信息有误</td>
													</c:when>
													<c:when test="${shopAudit.status eq -6}">
														<td align="center" valign="top" width="200">提交店铺信息有误</td>
													</c:when>
												</c:choose>
					                   		 <td align="center" width="200"><fmt:formatDate value="${shopAudit.modifyDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>

					                   		 <td align="center" valign="top">
									            <font color="red">${shopAudit.auditOpinion}</font>
					                   		 </td>
					                   		 <td width="100">
					                   		 	<a  href="javaScript:void(0);" onclick="rewrite('${shopAudit.status}')" class="settled_btn">重新填写</a>
					                   		 </td>
				                			 </tr>
					                 	</c:when>
					                 	<%-- <c:when test="${shopAudit.status eq -5}">
						                 	<tr>
					                    	<td align="center" valign="top" width="200">提交公司信息有误</td> 
					                   		 <td align="center" width="200"><fmt:formatDate value="${shopAudit.modifyDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					                   		 <td align="center" valign="top">
									         	<font color="red">${shopAudit.auditOpinion}</font>
					                   		 </td>
					                   		 <td width="100">
					                   		 	<a href="javaScript:void(0);" onclick="rewrite('${shopAudit.status}')" class="settled_btn">重新填写</a>
							                 </td>
				                			 </tr>
					                 	</c:when>
					                 	<c:when test="${shopAudit.status eq -6}">
						                 	<tr>
					                    	<td align="center" valign="top" width="200">提交店铺信息有误</td> 
					                   		 <td align="center" width="200"><fmt:formatDate value="${shopAudit.modifyDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					                   		 <td align="center" valign="top">
									                    <font color="red">${shopAudit.auditOpinion}</font>
								                    		
					                   		 </td>
					                   		 <td width="100"><a href="javaScript:void(0);" onclick="rewrite('${shopAudit.status}')" class="settled_btn">
							                            		重新填写
							                            	</a></td>
				                			 </tr>
					                 	</c:when>--%>
					                 	<c:when test="${shopAudit.status eq 0}">
						                 	<tr>
					                    	<td align="center" valign="top" width="200">您的店铺已经被平台下线！</td> 
					                   		 <td align="center" width="200"><fmt:formatDate value="${shopAudit.modifyDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					                   		 <td align="center" valign="top">
									                    <font color="red">${shopAudit.auditOpinion}</font>
					                   		 </td>
					                   		 <td></td>
				                			 </tr>
					                 	</c:when>
					                 </c:choose>
		                  	</c:when>
		                  	<c:otherwise>
		                  			 <c:choose>
					                 	<c:when test="${shopAudit.status eq -4}">
						                 	<tr>
					                    	<td align="center" valign="top" width="200">提交联系人信息有误</td> 
					                   		  <td align="center" ><fmt:formatDate value="${shopAudit.modifyDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					                   		  <td align="center" valign="top">
									                    <font color="red">${shopAudit.auditOpinion}</font>
					                   		 </td>
					                   		 <td></td>
				                			 </tr>
					                 	</c:when>
					                 	<c:when test="${shopAudit.status eq -5}">
						                 	<tr>
					                    	<td align="center" valign="top" width="200">提交公司信息有误</td> 
					                   		 <td align="center" width="200"><fmt:formatDate value="${shopAudit.modifyDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					                   		 	<td align="center" valign="top">
									                    <font color="red">${shopAudit.auditOpinion}</font>
					                   		 </td>
					                   		 <td></td>
				                			 </tr>
					                 	</c:when>
					                 	<c:when test="${shopAudit.status eq -6}">
						                 	<tr>
					                    	<td align="center" valign="top" width="200">提交店铺信息有误</td> 
					                   		  <td align="center" width="200"><fmt:formatDate value="${shopAudit.modifyDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					                   		  <td align="center" valign="top" >
									                    <font color="red">${shopAudit.auditOpinion}</font>
					                   		 </td>
					                   		 <td></td>
				                			 </tr>
					                 	</c:when>
					                 	<c:when test="${shopAudit.status eq 0}">
						                 	<tr>
					                    	<td align="center" valign="top" width="200">您的店铺已经被平台下线！</td> 
					                   		  <td align="center" width="200"><fmt:formatDate value="${shopAudit.modifyDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					                   		  <td align="center" valign="top">
									            <font color="red">${shopAudit.auditOpinion}</font>
					                   		 </td>
					                   		 <td></td>
				                			 </tr>
					                 	</c:when>
					                 </c:choose>
		                  	</c:otherwise>
		                  	</c:choose>
		                  </c:forEach>
	                 </c:otherwise>
                  </c:choose>
                </table>
                </div>
                  <div class="settled_bottom">
                      <span>
                      </span>
                  </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
   </div>
		<%@ include file="../home/bottom.jsp" %>
</div>
<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>"  type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/openshop.js'/>"></script>
</body>
</html>
