<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<div class="clear"></div>
<c:set var="advList" value="${floor.floorItems.RA}"></c:set>
<c:set var="BDfloorItem" value="${floor.floorItems.BD}"></c:set>
<div class="yt-wrap bf_floor_new clearfix">
	<div class="adv-bra">
		<div class="adv-bra-tit">
			<h4>${floor.name}</h4>
			<a href="${contextPath}/allbrands">全部品牌 &gt;&gt;</a>
		</div>
		<div class="adv-bra-con">
			<div class="adv-bra-con-left">
				<c:forEach items="${advList}" var="adv" varStatus="index" end="1">
					<a target="_blank" title="${adv.title}" href="${adv.linkUrl}">
						<img class="lazy" alt="${adv.title}" src="${contextPath}/resources/common/images/loading1.gif" data-original="<ls:photo item='${adv.picUrl}'/>"
                             alt="" width="320" height="421"> </a>
				</c:forEach>
			</div>
			<div class="adv-bra-con-right">
				<ul>
					<c:forEach items="${BDfloorItem}" var="brand" varStatus="status"
						end="40">
						<li><a target="_blank" title="${brand.brandName}"
							href="${contextPath}/list?prop=brandId:${brand.referId}">
								<img class="lazy" alt="${brand.brandName}" src="${contextPath}/resources/common/images/loading1.gif"
								data-original="<ls:images item='${brand.pic}' scale='2'/>"><em>${brand.brandName}</em>
						</a>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div>
</div>