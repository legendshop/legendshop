<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/pages/plugins/multishop/back-common.jsp" %>
<%@ include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>添加秒杀活动-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link rel="stylesheet" type="text/css" href="<ls:templateResource item='/resources/templets/css/seckill-activity.css'/>">
    <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />

</head>
<body class="graybody">
<div id="doc">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
    <div class="w1190">
        <div class="seller-cru">
            <p>
                您的位置>
                <a href="#">首页</a>>
                <a href="#">卖家中心</a>>
                <span class="on"> 编辑秒杀活动</span>
            </p>
            <%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>

            <div class="seckill-right">
                <form action="${contextPath}/s/seckillActivity/save" method="post" id="form1" enctype="multipart/form-data">

                    <input type="hidden" id="id" name="id" value="${seckillActivity.id}"/>
                    <input type="hidden" id="skus" name="skus" />
                    <!-- 基本信息 -->
                    <div class="basic-ifo">
                        <!-- 基本信息头部 -->
                        <div class="ifo-top">
                            基本信息
                        </div>
                        <div class="ifo-con">
                            <li style="margin-bottom: 5px;">
                                <span class="input-title"><span class="required">*</span>活动名称：</span>
                                <input type="text" class="active-name" name="seckillTitle" id="seckillTitle"
                                       value="${seckillActivity.seckillTitle}" maxLength="25"
                                       placeholder="请填写活动名称"><em></em>
                            </li>
                            <li>
                                <span class="input-title"></span>
                                <span style="color: #999999;margin-top: 10px;">活动名称最多为25个字符</span>
                            </li>
                            <li style="margin-bottom: 0;">
                                <span class="input-title"><span class="required">*</span>活动时间：</span>
                                <fmt:formatDate value="${seckillActivity.startTime}" pattern="yyyy-MM-dd HH:mm:ss"
                                                var="startTime"/>
                                <input type="text" class="active-time" readonly="readonly" name="startTime"
                                       id="startTime" value='${startTime}' placeholder="开始时间"/><em></em>
                                <span style="margin: 0 5px;">-</span>
                                <fmt:formatDate value="${seckillActivity.endTime}" pattern="yyyy-MM-dd HH:mm:ss"
                                                var="endTime"/>
                                <input type="text" class="active-time" readonly="readonly" name="endTime" id="endTime"
                                       value='${endTime}' placeholder="结束时间"/><em></em>
                            </li>
                        </div>
                    </div>
                    <!-- 选择活动商品 -->
                    <div class="basic-ifo" style="margin-top: 20px;">
                        <!-- 选择活动商品头部 -->
                        <div class="ifo-top">
                            选择活动商品
                        </div>

                        <div class="ifo-con">
                            <div class="red-btn" onclick="showProdlist()">选择商品</div>

                            <c:if test="${not empty seckillActivity.status && seckillActivity.status eq 1}">
                                <span style="color: #e2393b;">*该活动已审核通过，商品库存已锁定，不可修改秒杀库存</span>
                            </c:if>

                            <div class="check-shop prodList">
                                <c:if test="${not empty activityDtos}">
                                    <table border="0" id="skuList" cellpadding="0" cellspacing="0" style="margin: 30px 0;">
                                        <tbody>
                                        <tr style="background: #f9f9f9;">
                                            <td width="70px">
                                                <label class="checkbox-wrapper">
                                                    <span class="checkbox-item">
                                                        <input type="checkbox" id="checkbox" class="checkbox-input selectAll">
                                                        <span class="checkbox-inner"></span>
                                                    </span>
                                                </label>
                                            </td>
                                            <td width="120px">商品图片</td>
                                            <td width="300px">商品名称</td>
                                            <td width="150px">商品规格</td>
                                            <td width="100px">商品价格</td>
                                            <td width="100px">商品库存</td>
                                            <td width="100px">秒杀库存</td>
                                            <td width="100px">秒杀价格</td>
                                            <td width="80px">操作</td>
                                        </tr>
                                        <c:choose>
                                            <c:when test="${not empty activityDtos}">
                                                <c:forEach items="${activityDtos}" var="activityDto">
                                                    <tr class="sku" >
                                                        <td>
                                                            <label class="checkbox-wrapper">
                                                                <span class="checkbox-item">
                                                                    <input type="checkbox" name="array" class="checkbox-input selectOne" onclick="selectOne(this);">
                                                                    <span class="checkbox-inner"></span>
                                                                </span>
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <img src="<ls:images item='${activityDto.pic}' scale='3' />" >
                                                        </td>
                                                        <td>
                                                            <span class="name-text">${activityDto.prodName}</span>
                                                        </td>
                                                        <td>
                                                            <span class="name-text">${activityDto.cnProperties}</span>
                                                        </td>
                                                        <td>
                                                            <span class="name-text">${activityDto.skuPrice}</span>
                                                        </td>
                                                        <td>
                                                            <span class="name-text" class="skustocks">${activityDto.skuStock}</span>
                                                        </td>
                                                        <td><input type="text" id="seckillStock" value="${activityDto.stock}" style="width: 80px" maxlength="8" class="seckillStock" <c:if test="${not empty seckillActivity.status && seckillActivity.status eq 1}">disabled="disabled"</c:if> /></td>
                                                        <td>
                                                            <input type="text" class="seckillPrice" id="seckillPrice" name="seckillPrice" maxlength="10" style="text-align: center" value="${activityDto.price}">
                                                            <input type="hidden" id="prodId" value="${activityDto.prodId}"/>
                                                            <input type="hidden" id="skuId" value="${activityDto.skuId}"/>
                                                        </td>
                                                        <td><a href="#" class="removeSku">移除</a></td>
                                                    </tr>
                                                </c:forEach>
                                            </c:when>
                                            <c:otherwise>
                                                <tr class="first">
                                                    <td colspan="7" >暂未选择商品</td>
                                                </tr>
                                            </c:otherwise>
                                        </c:choose>
                                        <tr class="noSku" style="display: none">
                                            <td colspan="7" >暂未选择商品</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </c:if>
                            </div>
                            <div class="check-bto"  <c:if test="${empty activityDtos}">style="display: none;margin: -11px 54px 0 0;float: right"</c:if><c:if test="${not empty activityDtos}">style="display: inline-block; margin: -11px 54px 0 0;float: right"</c:if>>
                                批量设置秒杀价格: <input type="text" maxlength="10" id="batchPrice"> 元
                                <div class="red-btn" style="display: inline-block;margin-left: 10px" id="confim">确定</div>
                            </div>
                        </div>
                    </div>
                    <!-- 活动规则 -->
                    <div class="basic-ifo" style="margin-top: 20px;">
                        <!-- 活动规则头部 -->
                        <div class="ifo-top">
                            活动规则
                        </div>
                        <div class="ifo-con">
                            <li><font class="input-title"><b style="color: red">*</b>活动摘要：</font><input type="hidden" name="seckillLowPrice"
                                                                                    id="seckillLowPrice"
                                                                                    value="${seckillActivity.seckillLowPrice}"/><textarea
                                    name="seckillBrief" id="seckillBrief"
                                    maxlength="100">${seckillActivity.seckillBrief}</textarea><em></em></li>
                            <li><font class="input-title"><b style="color: red">*</b>活动图片：</font><input type="file" name="seckillPicFile"
                                                                                    id="seckillPicFile"/><em></em></li>
                            <c:if test="${not empty seckillActivity.seckillPic}">
                                <li><font class="input-title">原有图片：</font><a href="<ls:photo item='${seckillActivity.seckillPic}'/>"
                                                         target="_blank"><img
                                        src="<ls:photo item='${seckillActivity.seckillPic}'/>"
                                        style="max-height: 150px;max-width: 150px;"></a></li>
                            </c:if>
                            <li>
                                <span class="input-title"><span class="required"></span>秒杀规则：</span>
                                <div class="rich-text">
                                    <textarea name="seckillDesc" id="seckillDesc" cols="100" rows="8" style="width:700px;height:300px;visibility:hidden;" placeholder="（选填）">${seckillActivity.seckillDesc}</textarea>
                                </div>
                            </li>
                        </div>
                    </div>
                    <div class="bto-btn">
                        <input type="submit" class="red-btn" value="提交"/>
                        <div class="red-btn return-btn" onclick="javascript:history.go(-1);">返回</div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script charset="utf-8"
        src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
    var pic = "${seckillActivity.seckillPic}";
    var active = "${seckillActivity.id}";
    var status = "${seckillActivity.status}";

    var skuList = new Array();
    var paramData = {
        contextPath: "${contextPath}",
        cookieValue: "<c:out value="${cookie.SESSION.value}"/>"
    }

    $(document).ready(function () {
        userCenter.changeSubTab("seckillManage"); //高亮菜单
    });

    Date.prototype.toLocaleString = function () {
        0
        return this.getFullYear() + "-" + (this.getMonth() + 1) + "-" + this.getDate();
    };

    var myDate = new Date(new Date().getTime()).toLocaleString();

    /* 先初始化开始时间 */
    var start = laydate.render({
        elem: '#startTime',
        type: 'datetime',
        calendar: true,
        theme: 'grid',
        trigger: 'click',
        min: myDate + ' 09:00:00',
        format: 'yyyy-MM-dd HH:mm:ss',
        ready: function (date) {
            start.hint('注意：不能选择每天的0~9点');
        }, done: function (value, date, endDate) {
            if (date.hours <= 8) {
                layer.msg("不能选择每天的0点~8点", {icon: 0, time: 800});
                return false;
            }
            var startDate = getTimeStart(date.hours);
            var enddate = getTimePeriod(date.hours);
            end.config.min = {
                year: date.year,
                month: date.month - 1,
                date: date.date,
                hours: startDate,
                minutes: 00,
                seconds: 00
            };

            end.config.max = {
                year: date.year,
                month: date.month - 1,
                date: date.date,
                hours: enddate,
                minutes: 59,
                seconds: 59
            };
            $('#endTime').prop('disabled', false);
        }
    });

    var end = laydate.render({
        elem: '#endTime',
        type: 'datetime',
        min: myDate + ' 09:00:00',
        calendar: true,
        theme: 'grid',
        trigger: 'click',
        ready: function (date) {
            if (isBlank($("#startTime").val())) {
                end.hint("请先选择开始时间");
            }
        }, done: function (value, date, endDate) {
            if (date.hours <= 8) {
                layer.msg("不能选择每天的0点~8点", {icon: 0, time: 800});
                return false;
            }
        }
    });
    KindEditor.options.filterMode = false;
    KindEditor.create('textarea[name="seckillDesc"]', {
        cssPath: '${contextPath}/resources/plugins/kindeditor/plugins/code/prettify.css',
        uploadJson: '${contextPath}/editor/uploadJson/upload;jsessionid=${cookie.SESSION.value}',
        fileManagerJson: '${contextPath}/editor/uploadJson/fileManager',
        allowFileManager: true,
        resizeType: 0,
        afterBlur: function () {
            this.sync();
        },
        width: '100%',
        height: '350px',
        afterCreate: function () {
            var self = this;
            KindEditor.ctrl(document, 13, function () {
                self.sync();
                document.forms['example'].submit();
            });
            KindEditor.ctrl(self.edit.doc, 13, function () {
                self.sync();
                document.forms['example'].submit();
            });
        }
    });


</script>
<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
<script type="text/javascript" language="javascript"
        src="<ls:templateResource item='/resources/templets/js/seckill/seckillActivity.js'/>"></script>
</body>
</html>