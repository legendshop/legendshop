<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>商品详情-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <style type="text/css">
        .shopDetailSpan {
            text-align: center;
            width: 33.3%;
            display: inline-block;
        }

        .shopDetailNum {
            color: #e5004f;
            text-align: center;
            width: 33.3%;
            display: inline-block;
        }
    </style>
</head>
<body class="graybody">
	<div id="doc">
	        <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
            
		      <div id="bd">
		      
		      <div class="crumb yt-wrap" id="J_crumbs">
             <div class="crumbCon">
               </div>
            </div>
	            
                        <!--主要信息-->
             <div class="infomain yt-wrap" style="border-width: 1px 1px 1px 0px;">

                 <div class="infomain yt-wrap">

                     <!-- 店铺信息 -->
                     <div class="info_u_r" <c:if test="${empty shopDetail.shopPic}">style="padding: 0px 7px;border:1px solid #eee;width: 190px;height: 249px;box-sizing: border-box;"</c:if> <c:if test="${!empty shopDetail.shopPic}">style="padding: 0px 7px;border:1px solid #eee;width: 190px;height: 323px;box-sizing: border-box;"</c:if>>
                         <div class="qjdlogo" style="border-bottom:0px"  <c:if test="${empty shopDetail.shopPic}">style="border-bottom:0px;padding:0px"</c:if>
                         <a href="<ls:url address='/store/${shopDetail.shopId}'/>" style="text-align: center;"> <c:if test="${not empty shopDetail.shopPic}">
                             <img style="border: 0;vertical-align: middle; width: 180px;max-height: 75px" src="<ls:photo item='${shopDetail.shopPic}'/>">
                         </c:if></a>
                     </div>
                     <div class="shopInfo" style="padding:0px">
                         <a class="shopName" href="<ls:url address='/store/${shopDetail.shopId}'/>" target="_blank" title="${shopDetail.siteName}">${shopDetail.siteName}</a><c:choose>
                         <c:when test="${shopDetail.shopType == 0}"><strong style="float: right">专营店</strong></c:when>
                         <c:when test="${shopDetail.shopType == 1}"><strong style="float: right">旗舰店</strong></c:when>
                         <c:when test="${shopDetail.shopType == 2}"><span style="float: right" class="ziying">平台自营</span></c:when>
                     </c:choose>
                     </div>
                     <ul>
                         <li><span style="float:left;">综合评分：</span> <span class="scores_scroll"> <span class="scroll_gray"></span> <span class="scroll_red" style="width: 20%"></span> </span><font id="shopScoreNum" class="hide">${sum}</font></li>
                         <li style="border-bottom: 0px;height:25px"><span class="shopDetailSpan">描述</span><span class="shopDetailSpan">服务</span><span class="shopDetailSpan">物流</span></li>
                         <li><span class="shopDetailNum">${prodAvg==null?'0.0':prodAvg}</span><span class="shopDetailNum">${shopAvg==null?'0.0':shopAvg}</span><span class="shopDetailNum">${dvyAvg==null?'0.0':dvyAvg}</span></li>
                         <c:set var="contaceQQ" value="${fn:split(shopDetail.contactQQ, ',')[0]}"></c:set>
                         <li>商城客服：<%-- <a href="http://wpa.qq.com/msgrd?v=3&uin=${contaceQQ}&site=qq&menu=yes" target="_blank">点击咨询</a> --%>
                             <input type="hidden" id="user_name" value="${user_name}"/>
                             <input type="hidden" id="user_pass" value="${user_pass}"/>
                             <input type="hidden" id="shopId" value="${shopDetail.shopId}"/>
                             <a href="javascript:void(0)" id="loginIm">联系卖家</a>
                         </li>
                     </ul>
                     <div class="col-shop-btns">
                         <a href="<ls:url address='/store/${shopDetail.shopId}'/>" target="_blank" style="font-size:12px;color:#ffffff;width:80px;height:30px;background:#333333;border:1px solid #333;">进入店铺</a>
                         <a class="col-btn" href="javascript:void(0);" onclick="collectShop('${shopDetail.shopId}')" style="font-size:12px;color:#333333;width:80px;height:30px;background:#f5f5f5;border:1px solid #cccccc;">收藏店铺</a>
                     </div>
                 </div>

               <div class="clearfix info_u_l" style="overflow: hidden;width: 1000px;">
                   <div class="tb-property">
                       <div class="tb-wrap" style="border-left: 0;">
                            <div class="tb-detail-hd"  style="margin-left: 55px;">      
                                <h1><a href="javascirpt:void(0);" id="prodName">${seckill.prodDto.name}</a></h1>
                                <p class="newp">${seckill.prodDto.brief}</p>
                            </div>
                         <!--    <div class="tm-price" style="margin: 10px 55px 0px;">
                              <span style="margin-right: 35px;color: #888;">当前价：<em style="font-size: 24px; color: #c6171f;">¥899.00</em></span>
                              <span style="margin-right: 35px;color: #888;">商城价：<em style="text-decoration: line-through;">¥899.00</em></span>
                            </div>
                             
                       <!--     <div class="tm-price" style="margin: 40px 55px 0px;">
                              <span style="margin-right: 35px;color: #888;">当前价：<em style="font-size: 24px; color: #c6171f;">¥899.00</em></span>
                              <span style="margin-right: 35px;color: #888;">商城价：<em style="text-decoration: line-through;">¥899.00</em></span>
                            </div>

							<div class="sur-time" style="margin-left: 55px;">
								<img src="images/time.png" alt="">剩余时间：<span><em>10</em>时</span><span><em>10</em>分</span><span><em>10</em>秒</span>
							</div>
 -->
                            <div class="sur-time"   id="seckill_starting" style="margin-left: 55px;display: none;">
                              <img src="<ls:templateResource item='/resources/templets/images/time.png'/>" alt="">结束倒计时：
                              <div class="pm-status-time" style="display:inline-block;">
									<span><em>0</em>天<em>0</em>时<em>0</em>分<em>0</em>秒</span>
								</div>
                            </div>
                            
                            <div class="sur-time" id="seckill_end" style="margin-left: 55px;display: none;">
                              <img src="<ls:templateResource item='/resources/templets/images/time.png'/>" alt="">结束时间：
                              <div class="pm-status-time" style="display:inline-block;">
									<span><em>0</em>天<em>0</em>时<em>0</em>分<em>0</em>秒</span>
								</div>
                            </div>
                            
                            <div class="sur-time" id="seckill_ready" style="margin-left: 55px;display: none;">
                              <img src="<ls:templateResource item='/resources/templets/images/time.png'/>" alt="">开始倒计时：
                              <div class="pm-status-time" style="display:inline-block;">
									<span><em>0</em>天<em>0</em>时<em>0</em>分<em>0</em>秒</span>
								</div>
                            </div>
                            
                            
                            
                           <div class="tm-price" style="margin: 20px 55px 0px;">
                              <span style="margin-right: 35px;color: #888;">秒杀价：<em style="font-size: 24px; color: #c6171f;" id="seckPrice">¥${seckill.prodDto.seckPrice}</em></span>
                              <span style="margin-right: 35px;color: #888;">商城价：<em style="text-decoration: line-through;" id="prodCash">¥${seckill.prodDto.cash}</em></span>
                            </div>
                            
                            <div id="skuStock_div" class="tm-price" style="margin: 20px 55px 0px;">
                            	<span style="margin-right: 35px;color: #888;">库存：<em style="font-size: 12px; color: #888;" id="skuStock"></em></span>
                            </div>
							
                             <div class="tb-meta" style="margin-left: 40px;margin-top: 30px;color:#888;">
                                <c:forEach items="${seckill.prodDto.prodPropDtoList}" var="prop" varStatus="psts">
                                     <dl class="tb-prop tm-sale-prop clearfix ">
	                                    <dt class="tb-metatit">${prop.propName}</dt>
	                                    <dd>
	                                     <ul class="clearfix J_TSaleProp" prop="${prop.propName}" id="prop_${psts.index}" index="${psts.index}" >
	                                          <c:forEach items="${prop.productPropertyValueList}" var="propVal">
	                                                  <c:choose>
														      <c:when test="${propVal.isSelected}">
																		<li  data-value="${prop.propId}:${propVal.valueId}" valId="${propVal.valueId}"   class="tb-selected">
																		   <a href="javascript:void(0);" >
																		      <c:if test="${not empty propVal.pic}">
																				 <img src="<ls:images item='${propVal.pic }' scale='3'/>" style="margin: 1px 3px 1px 1px;display: inline-block;vertical-align: middle;width: 25px;height: 25px;">
																			  </c:if>
																               <span>${propVal.name}</span>
																		   </a>
																		</li>
																</c:when>
																<c:otherwise>
			 													    <li  data-value="${prop.propId}:${propVal.valueId}" valId="${propVal.valueId}" >
																      <a href="javascript:void(0);">
																         <%-- <img src="<ls:images item='${propVal.pic}' scale='3'/>"  style="margin: 1px 3px 1px 1px;display: inline-block;vertical-align: middle;width: 25px;height: 25px;"> --%>
																         <span>${propVal.name}</span>
																      </a>
																    </li>
																</c:otherwise>
														</c:choose>
	                                          </c:forEach>
	                                     </ul>
	                                    </dd>
	                                </dl>
                                </c:forEach>
                            </div>
                            
                            
                            <!-- <div class="ipu-price"  style="margin-left: 55px;margin-top: 20px;">
                              <p>商品数量：<a href="#">－</a><input type="text" style="width: 50px; text-align: center;" value="1"><a href="#">＋</a></p>
                            </div> -->
                            
                            <c:choose>
                                 <c:when test="${shopDetail.status eq 1}">
		                            <div class="bid-but" style="margin-top: 40px;">
		                               <input type="button" id="seckill-btn"  style="text-align: center; width: 170px; margin-left: 40px;display: none;">
		                            </div>
                            	</c:when>
                            	<c:otherwise>
                            		 <div class="bid-but" style="margin-top: 40px;">
		                               <input type="button" style="text-align: center; width: 170px; margin-left: 40px;" value="店铺已下线">
		                            </div>
                            	</c:otherwise>
                            </c:choose>
                       </div>
                   </div>
                   
                   <!--产品图片-->
					<%@include file="prodpics.jsp"%>
				  <!--产品图片end-->

               </div>
            </div>

            <!--更多信息-->
            <div class="yt-wrap info_d clearfix">
               <div class="right210_m" style="margin-left: 0;">
                    <div class="detail">
                                    <div class="pagetab2">
                                       <ul>           
                                         <li id="prodContent" class="on" onclick="showProdContent();"><span>商品详情</span></li>                  
                                         <li id="seckill" onclick="showSeckillContent();"><span>秒杀规则</span></li>         
                                       </ul>       
                                    </div>
                                    <div id="prodContentPanel" class="tabcon" style="background:#f6f6f6; zoom:1; ">
                                      ${seckill.prodDto.content}
                                    </div>
                                    <div  id="seckillPanel" class="content" style="text-align:center;display: none;">
                                      ${seckill.secDesc}
                                    </div>
                     </div>
               </div>
             
            </div>
            <!--更多信息 end-->
            
            <!--p_list-->
            <!--p_list end-->
            
            <!--分页-->
            <!--分页 end-->
   </div>
   <!--bd end-->

		<div class="sec-val" style="display: none;">
			<p>秒杀验证</p>
			<ul>
				<!-- <li>请输入正确的验证码</li> -->
				<li style="text-align: center;">输入验证码：
				   <input type="text" id="randNum" name="randNum" maxlength="4"  placeholder="图形验证码">
				   <img id="randImage"  style="cursor: pointer;"  src="<ls:templateResource item='/validCoderRandom'/>" onclick="changeRandImg();"  style="cursor: pointer;" alt="">
				</li>
				<li>
				   <a href="javascript:void(0);" onclick="validateRandNum();">确定</a>
				   <a href="javascript:void(0);" onclick="hideNum();">取消</a>
				</li>
			</ul>
		</div>
		
		<div class="pay-success" id="paidui" style="left: 26%;top:350.5px;z-index: 2500;position: absolute;width: 655px;padding: 25px 0px;background-color: #ffffff;display: none;">
            <p style="margin-bottom:0;"><img src="<ls:templateResource item='/resources/templets/images/wunai.png'/>" alt=""></p>
            <span style="font-size: 14px;color: #333;">排队中,正在奋力向前冲,请您耐心等待...</span><br>
            <input type="button" value="退出排队" onclick="hidePaidui();" style="margin-top: 15px;">  
         </div>
         
         <div class="pay-success" id="sale-out" style="left: 26%;top:350.5px;z-index: 2500;position: absolute;width: 655px;padding: 25px 0px;background-color: #ffffff;display: none;" >
          	<p style="margin-bottom: 0;"><img src="<ls:templateResource item='/resources/templets/images/sad.png'/>" alt=""></p>
            <span style="font-size: 14px;color: #333;">小伙伴太热情，您选择的商品售完了...</span><br>
            <input type="button" value="返回活动页面" onclick="hideSaleOut();" style="margin-top: 15px;">  
          </div>
          
          
        <div class="black_overlay" ></div> 

		<script type="text/javascript">
			var currSkuId=0; //当前选择的
			var seckillId=${seckill.sid}
			var prodId =${seckill.prodDto.prodId};
			var prodName = '${fn:escapeXml(seckill.prodDto.name)}';
			var skuDtoList = eval('${seckill.prodDto.skuDtoListJson}');
			var propValueImgList = eval('${seckill.prodDto.propValueImgListJson}');
			var imgPath3 = "<ls:images item='PIC_DEFAL' scale='3' />";
			var startTime=${seckill.startTime.time};
            var endTime=${seckill.endTime.time};
            var loginUserName="${loginUserName}";
            var _switch=0;
		</script>
		<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/seckill/legendshop.countdown.js'/>"></script>
        <script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/seckill/seckillView.js'/>"></script>
        <script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/seckill/seckill.js'/>"></script>
       <!----foot---->
	  <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	    
	</div>
</body>
