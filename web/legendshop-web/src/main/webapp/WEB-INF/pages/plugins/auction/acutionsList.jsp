<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<jsp:useBean id="nowTime" class="java.util.Date" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<c:set var="auctions" value="${list}"></c:set>
<title>拍卖活动列表-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/commonPage.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/auction.css'/>" rel="stylesheet"/>
</head>
<body>
	<div id='auc_page'>
		<div id="doc">
			<div id="hd">
				<%@ include
					file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
			</div>
			<div id="bd">
				<div class="seller-cru" style="width:1190px;margin: 20px auto 10px;">
					<p>
						您的位置&gt;
						<a href="#">首页</a>
						&gt; <span style="color: #c6171f;">拍卖活动</span>
					</p>
				</div>
				<!--两栏-->
				<div class=" yt-wrap " style="padding-top:10px;">
					<!--right_con-->

					<div class="bidding-list">
						<nav id="main-nav" class="sort-bar">
							<div class="nch-sortbar-array">
								排序方式：
								<ul>
									<form:form action="${contextPath}/auction/autionList" id="form1" method="post">
										<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
										<input type="hidden" id="loadType" name="loadType" value="${searchParams.loadType}" />
									</form:form>
									<li id="default" ${(empty searchParams.orders) or(searchParams.orders== 'T.status,asc') ?'class="selected"':''}>
										<a title="默认排序" href="javascript:void(0);">默认排序</a>
									</li>
									<li id="startTime" ${searchParams.orders== 'T.startTime,desc' ||  searchParams.orders== 'T.startTime,asc'?'class="selected"':''}>
										<a title="点击按开始时间从近到远排序" href="javascript:void(0);"
											<c:choose><c:when test="${searchParams.orders=='T.startTime,desc'}">class='desc'</c:when><c:when test="${searchParams.orders=='T.startTime,asc'}">class='asc'</c:when></c:choose>>
											开始时间<i></i>
										</a>
									</li>
									<li id="floorPrice" ${searchParams.orders== 'T.floorPrice,desc' ||  searchParams.orders== 'T.floorPrice,asc'?'class="selected"':''}>
										<a title="点击按竞拍价格从高到低排序" href="javascript:void(0);"
											<c:choose><c:when test="${searchParams.orders=='T.floorPrice,desc'}">class='desc'</c:when><c:when test="${searchParams.orders=='T.floorPrice,asc'}">class='asc'</c:when></c:choose>>
											竞拍价格<i></i>
										</a>
									</li>
								</ul>
							</div>
							<div class="auc-load-list">
								<em> <c:choose>
										<c:when test="${searchParams.loadType=='1'}">正在拍卖</c:when>
										<c:when test="${searchParams.loadType=='0'}">即将开拍</c:when>
										<c:when test="${searchParams.loadType=='2'}">竞拍完成</c:when>
										<c:otherwise>正在拍卖</c:otherwise>
									</c:choose> </em><span>查看活动：</span>
								<div class="bid-sec">
									<ul id="loadTypeSelect">
										<li>
											<a value="1" type="正在拍卖"
												<c:if test="${searchParams.loadType=='1'}"> selected="selected"</c:if>
												onclick="loadTypeClick(this)">正在拍卖</a>
										</li>
										<li>
											<a value="0" type="即将开拍"
												<c:if test="${searchParams.loadType=='0'}"> selected="selected"</c:if>
												onclick="loadTypeClick(this)">即将开拍</a>
										</li>
										<li>
											<a value="2" type="竞拍完成"
												<c:if test="${searchParams.loadType=='2'}"> selected="selected"</c:if>
												onclick="loadTypeClick(this)">竞拍完成</a>
										</li>
									</ul>
								</div>
								<!-- <div style="display:inline-block;margin-left:15px;">  
		        	查看活动：&nbsp;&nbsp;<div class="bid-sec">
			        <select id="loadTypeSelect">
			        	<option value="1" <c:if test="${searchParams.loadType=='1'}"> selected="selected"</c:if>>正在拍卖</option>
			        	<option value="0" <c:if test="${searchParams.loadType=='0'}"> selected="selected"</c:if>>即将开拍</option>
			        	<option value="2" <c:if test="${searchParams.loadType=='2'}"> selected="selected"</c:if>>竞拍完成</option>
			        </select>
		        	 
		        </div>  -->
							</div>
						</nav>
						<!--page-->

						<div id="recommend" class="auction" style="display: block;border: 0">
							<div class="pro-list">
								<c:if test='${empty list}'>
									 <div class="selection_box search_noresult">
										<p class="search_nr_img"><img src="<ls:templateResource item='/resources/templets/images/not_result.png'/>"></p> 
										<p style="text-align: center;font-size: 16px;margin-top: 10px;">没有找到<c:if test="${not empty searchParams.keyword}">与“<span>${searchParams.keyword}</span>”</c:if>相关的活动信息</p>
									</div>
								</c:if>
								<ul class="clear">
									<c:forEach var="auction" items="${list}">
									<li id="id_${auction.id}" class="falg">
										<a href="${contextPath}/auction/views/${auction.id}" class="pro-img"><img src="<ls:images item='${auction.prodPic}' scale='1'/>" style="vertical-align: middle;" /></a>
										<a href="${contextPath}/auction/views/${auction.id}" class="pro-name">
											<p>${auction.auctionsTitle }</p>
											<span>${auction.auctionsDesc }</span>
										</a>
										<div class="pro-price clear" biddingNumber="${auction.biddingNumber }">
											<p>
												<span class="p-price">￥<b>${auction.curPrice }</b>
											</span>
												<em>
													<c:choose>
														<c:when test="${empty auction.biddingNumber}">暂无出价</c:when>
														<c:otherwise>${auction.biddingNumber}次出价</c:otherwise>
													</c:choose>
												</em>
											</p>
												<c:choose>
												<c:when test="${auction.startTime<nowTime&&auction.endTime>nowTime&&auction.status == 1}">
														<a href="${contextPath}/auction/views/${auction.id}" class="rig-now">参与竞拍</a>
													</c:when>
													<c:otherwise>
														<a href="${contextPath}/auction/views/${auction.id}" class="not">去看看</a>
													</c:otherwise>
												</c:choose>
										</div>
											<div id="auctionProd_${auction.id}" endTime="${auction.endTimeStr}" startTime="${auction.startTimeStr}"
												<c:if test="${auction.status==2}">a="3" </c:if>
												<c:if test="${auction.startTime>nowTime}">a="0" r="${auction.startTimeStr-nowTime.time}" </c:if>
												<c:if test="${auction.startTime<nowTime&&auction.endTime>nowTime}">a="1" r="${auction.endTimeStr-nowTime.time}"</c:if>
												<c:if test="${auction.startTime<nowTime&&auction.endTime<nowTime}">a="2" </c:if>>
												<span>
												<div class="pro-time">
													剩余： <b>--</b> <span>时</span> <b>--</b> <span>分</span> <b>--</b><span>秒</span>
												</div>
												<div class="p-state">
													<span></span>
												</div>
											</div>
										</li>
									</c:forEach>
								</ul>
							</div>
							<!--group end-->
						</div>
					</div>
					<!--right_con end-->
					<!--分页-->
					<div class="clear"></div>
					<div style="margin-top:10px;" class="page clearfix">
						<div class="p-wrap">
							<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
						</div>
					</div>

				</div>
				<!--两栏end-->
			</div>
		</div>

		<%@ include
			file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<script type="text/javascript">
	var contextPath = "${contextPath}";
	var _page = "${_page}";
	var loadType = "${searchParams.loadType}";
	</script>
	<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/auction/auctionList.js'/>"></script>
</body>
</html>