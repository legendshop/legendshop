<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<!--pagenum-->
<c:if test="${not empty requestScope.list}">
	<div id="J_Filter" class="filter yt-wrap clearfix">
		<a id="default" class="fSort  ${(orders=='recDate,desc' || orders=='recDate,asc') ? 'fSort-cur':''}" href="javascript:void(0);">
			综合<i <c:choose><c:when test="${orders=='recDate,asc'}">class='f-ico-arrow-u'</c:when><c:otherwise>class='f-ico-arrow-d'</c:otherwise></c:choose>></i>
		</a>
		<a id="comments" class="fSort ${orders== 'comments,desc' || orders=='comments,asc'? 'fSort-cur':''}" href="javascript:void(0);" title="人气">
			人气<i <c:choose><c:when test="${orders=='comments,asc'}">class='f-ico-arrow-u'</c:when><c:otherwise>class='f-ico-arrow-d'</c:otherwise></c:choose>></i>
		</a>
		<a id="buys" class="fSort ${orders== 'buys,desc' || orders=='buys,asc'? 'fSort-cur' :''}" href="javascript:void(0);" title="销量">
			销量<i <c:choose><c:when test="${orders=='buys,asc'}">class='f-ico-arrow-u'</c:when><c:otherwise>class='f-ico-arrow-d'</c:otherwise></c:choose>></i>
		</a>
		<a id="cash" class="fSort ${orders== 'cash,desc' || orders=='cash,asc'? 'fSort-cur' :''}" href="javascript:void(0);" title="价格">
			价格<i <c:choose><c:when test="${orders=='cash,asc'}">class='f-ico-arrow-u'</c:when><c:otherwise>class='f-ico-arrow-d'</c:otherwise></c:choose>></i>
		</a>
		<p class="ui-page-s">
			<b class="ui-page-s-len">${curPageNO}/${pageCount}</b>
			<c:choose>
				<c:when test="${curPageNO<=1}">
					<b class="ui-page-s-prev" title="上一页">&lt;</b>
				</c:when>
				<c:otherwise>
					<a title="上一页" class="ui-page-s-prev" onclick="javascript:pager('${curPageNO-1}');" href="javascript:void(0);">&lt;</a>
				</c:otherwise>
			</c:choose>
			<c:choose>
				<c:when test="${requestScope.pageCount<=curPageNO}">
					<b class="ui-page-s-next" title="下一页">&gt;</b>
				</c:when>
				<c:otherwise>
					<a title="下一页" class="ui-page-s-next" onclick="javascript:pager('${curPageNO+1}');" href="javascript:void(0);">&gt;</a>
				</c:otherwise>
			</c:choose>
		</p>
	</div>
</c:if>
<!--pagenum end-->
<div class="yt-wrap clearfix ">
	<div class="p-list col4">
		<c:choose>
			<c:when test="${not empty requestScope.list}">
				<c:forEach items="${requestScope.list}" var="prodDetail" varStatus="status">
					<div class="p-listInfo">
						<div class="plistinfoboxin ">
							<div class="plistinfoboxin">
								<div class="p-listImgBig">
									<a title="${prodDetail.name}" target="_blank" href="<ls:url address='/views/${prodDetail.prodId}'/>">
										<input type="hidden" value="${prodDetail.prodId }" class="prodId" />
										<img style="max-width: 240px; max-height: 270px;" src="<ls:images item='${prodDetail.pic}' scale='0'/>">
									</a>
								</div>
								<div class="p-listPrice">
									<strong class="y-p">￥<fmt:formatNumber type="cash" value="${prodDetail.cash}" pattern="#0.00" /></strong>
									<span style="margin-left: 180px;color:#aaa">销量：${prodDetail.buys}</span>
								</div>
								<div class="p-listTxt">
									<p>
										<a title="${prodDetail.name}" target="_blank" href="<ls:url address='/views/${prodDetail.prodId}'/>">${prodDetail.name}</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<div class="selection_box search_noresult">
					<p class="search_nr_img">
						<img src="<ls:templateResource item='/resources/templets/images/not_result.png'/>">
					</p>
					<p class="search_nr_desc">
						商品正在马不停蹄上架中
					</p>
				</div>
			</c:otherwise>
		</c:choose>
	</div>
	<!--分页-->
	<div style="margin-top: 10px;" class="page yt-wrap clearfix">
		<div class="p-wrap">
			<ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="simple" />
		</div>
	</div>
	<!--分页 end-->
	<!--list end-->
	<!--page-->
</div>
<script type="text/javascript">
	var photoPath = "<ls:photo item= ''/>";
</script>
