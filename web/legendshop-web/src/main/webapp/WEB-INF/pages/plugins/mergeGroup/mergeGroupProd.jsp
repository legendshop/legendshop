<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/dialog.jsp"%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>商家拼团-选择商品</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/mergeGroup/mergeGroupEdit.css'/>" rel="stylesheet"/>
</head>
<style>
a{text-decoration: none}
</style>
<body>

	<!-- 添加商品（弹窗） -->
		<div class="add-goods" style="margin-top: -10px;">
			<div class="add-search" style="width: 800px;">
				<form id="myForm">
                	<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNo}">
					<span class="ser-item">
						<input type="text" name="prodName" id="prodName" placeholder="请输入商品名称">
						<a href="javascript:void(0)" class="sea-btn">搜索</a>
					</span>
				</form>
			</div>
			<div class="add-table" style="width:800px;">
				<%@ include file="mergeGroupProdList.jsp" %>
			</div>
		</div>
	<!-- /添加商品（弹窗） -->
</body>

<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/mergeGroup/mergeGroupProd.js'/>"></script>
<script type="text/javascript">	
var contextPath = "${contextPath}";
var ids = "${prodIds}";
if(isBlank(ids)){
	var prodIds = new Array;//必须在初始化之前
}else{
	var prodIds = ids.split(",");
}
</script>
</html>