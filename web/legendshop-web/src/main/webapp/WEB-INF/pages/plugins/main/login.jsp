<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>登录-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="loginback">
	  <div >
		<div class="yt-wrap">
			<div class="lr_tab">
				<a class="on" href="javascript:void(0);">登 录</a><a href="${contextPath}/reg">注 册</a>
			</div>
			<div class="logintop">
				<a href="${contextPath}/home"><img src="<ls:photo item="${systemConfig.logo}"/>"  style="max-height:89px;" />
				</a>
			</div>
		</div>

         <!--login-->
         <div class="lg-mid-b">
          <div style="margin:auto;width:1190px;overflow: hidden;">
         	<div style="float:left;margin-left:130px;margin-right:80px;">
         		<img alt="" src="${contextPath}/resources/templets/images/login-logo.png" style="height:400px;">
         	</div>
          <div class="loginb" style="float:left;">
             <div class="login-box">
                     <div class="tabmenu">
                            <ul class="tab">
                                <li class="active" id="mobileLoginTab" onclick="mobileLogin();"><span><a href="#none">短信登录</a></span>
                                </li>
                                <li class="" id="normalLoginTab" onclick="normalLogin();"><span><a href="#none">密码登录</a></span>
                                </li>
                            </ul>
                        </div>
                 <div class="form" id="normalForm" style="display: none;">
                 <c:choose>
						  <c:when test="${param.error == 1}">
						     <div class="mt">
		                         <label class="login_error" id="login_error"><fmt:message key="error.password.noright"/></label>
		                    </div>
						  </c:when>
						  <c:when test="${param.error == 2}">
						       <div class="mt">
		                         <label class="login_error"><fmt:message key="error.user.logined"/></label>
		                       </div>
						   </c:when>
						     <c:when test="${param.error == 3}">
						       <div class="mt" style="margin-bottom: 25px;">
		                         <label class="login_error">您已被迫下线，如果不是您本人操作，请及时修改密码!</label>
		                       </div>
						   </c:when>
						     <c:when test="${param.error == 4}">
						       <div class="mt" style="margin-bottom: 25px;">
		                         <label class="login_error">用户不存在或已被冻结!</label>
		                       </div>
						   </c:when>
						  	<c:otherwise>
						  	   <div id="pwdIncorrect" style="display: none;" class="mt">
		                           <label class="login_error"><fmt:message key="error.password.noright"/></label>
		                       </div>
						  	</c:otherwise>
				    </c:choose>
                     </div>


                    <div class="mc">
                        <div class="form">
                        	<input type="hidden" id="rand" name="rand"/>
							<input type="hidden" id="cannonull" name="cannonull" value='<fmt:message key="randomimage.errors.required"/>'/>
							<input type="hidden" id="charactors4" name="charactors4" value='<ls:i18n key="randomimage.charactors.required" length="4"/>'/>
							<input type="hidden" id="errorImage" name="errorImage" value='<fmt:message key="error.image.validation"/>'/>
							<input type="hidden" id="redirectIndicator" name="redirectIndicator" value="${contextPath}/login"/>
                            <form:form  action="${contextPath}/p/j_spring_security_check" method="post" id="formlogin" style="display:none" onsubmit="return loginForm()">
                           	   <input type="hidden" name="loginSource" value="PC"/>
                                <input type="hidden" id="loginType" name="loginType" value="1"/>
                                <div class="item item-fore1" style="margin-bottom: 25px;">
                                    <label class="login-label name-label" for="username"></label>
                                 	<input type="hidden" id="returnUrl" name="returnUrl" value="${param.returnUrl}"/>
                                    <input type="text" placeholder="用户名/手机号/邮箱"  tabindex="1"  id="username" type='text' name="username"  value="<lb:lastLogingUser/>" class="itxt" style="height: 19px">
	                                <label id="usernameError" style="line-height:25px;"></label>
                                </div>
                                <div class="item item-fore2" id="entry" style="margin-bottom: 25px;">
                                    <label for="password" class="login-label pwd-label"></label>
                                    <input id="pwd" type='password' tabindex="2" onload="this.value=''" name='password'   placeholder="密码" class="itxt itxt-error">
	                                 <label id="pwdError" style="line-height:25px;"></label>
                                </div>
                                <div class="item item-fore3">
                                    <div class="safe">
                                        <span>
                                            <input type="checkbox" id="_spring_security_remember_me" name="_spring_security_remember_me" class="jdcheckbox">
                                            <label for="_spring_security_remember_me">保存密码</label>
                                        </span>
                                        <span class="forget-pw-safe">
                                            <a target="_blank" class="" href="<ls:url address='/retrievepassword'/>">忘记密码?</a>
                                        </span>
                                    </div>
                                </div>
                                 <lb:userValidationImage>
	                                  <div class="item-fore6">
	                                    <input type="text" id="randNum" name="randNum" class="inputstyle" maxlength="4" tabindex="3" placeholder="输入验证码">
	                                    <img id="randImage" name="randImage" src="<ls:templateResource item='/validCoderRandom'/>" style="vertical-align: middle;" />
	                                    <a href="javascript:void(0)" onclick="javascript:changeRandImg('${contextPath}')"><fmt:message key="change.random2"/></a>
	                                </div>
	                                <label id="randNumError"></label>
                                </lb:userValidationImage>
                                <div class="item item-fore5">
                                    <div class="login-btn">
                                    <input class="btn-img btn-entry" value="登&nbsp;&nbsp;&nbsp;&nbsp;录" type="submit"/>
                                    </div>
                                </div>
                            </form:form>
                        </div>






                        <div class="form" id="moblieForm">
                            <input type="hidden" id="rand" name="rand"/>
                            <input type="hidden" id="redirectIndicator" name="redirectIndicator"
                                   value="${contextPath}/login"/>
                            <form:form action="${contextPath}/p/j_spring_security_check" method="post"
                                       id="moblieFormlogin" onsubmit="return mobileloginForm()">
                                <input type="hidden" id="loginType" name="loginType" value="2"/>
                                <div class="item item-fore1" style="margin-bottom: 25px;">
                                    <label class="login-label name-label" for="j_username"></label>
                                    <input type="hidden" id="mobileReturnUrl" name="returnUrl"
                                           value="${param.returnUrl}"/>
                                    <input type="text" placeholder="手机号码" tabindex="1" id="phone" type='text'
                                           name="j_username_mobile" maxlength="11" class="itxt" style="height: 19px">
                                    <label id="phoneError" style="line-height:25px;"></label>
                                </div>
                                <div class="item item-fore2" id="entry" style="margin-bottom: 25px;">
                                    <!-- <label for="j_password" class="login-label pwd-label"></label> -->
                                    <input id="SMSCode" type='text' tabindex="2" maxlength="6" name='j_password_mobile'
                                           placeholder="短信验证码" class="itxt itxt-error" style="width: 247px;"
                                          >
                                    <input class="smscode-button" type="button" value="获取短信校验码"
                                           onclick="sendMobileCode()" id="SMScodeBtn"
                                           style="position: absolute; right: 0;top: 0;height:100%;background: #f7f7f7;color: #666;"/>
                                    <label id="SMSCodeError" style="line-height:25px; display: block;"></label>
                                </div>
                                <%--这里如果用户没有注册将会注册, 否则登陆--%>
                                <%--<lb:userValidationImage>--%>
                                <div class="item item-fore6 login-model" style="display: none;">
                                    <input type="text" id="randNum2" name="randNum2" class="inputstyle" maxlength="4"
                                           tabindex="3" placeholder="输入图形验证码">
                                    <img id="randImage2" name="randImage2"
                                         src="<ls:templateResource item='/validCoderRandom'/>"
                                         style="vertical-align: middle;"/>
                                    <a href="javascript:void(0)"
                                       onclick="javascript:changeRandImg2('${contextPath}')"><fmt:message
                                            key="change.random2"/></a>
                                </div>
                                <label id="randNumError" class="login-model" style="display: none"></label>
                                <%--</lb:userValidationImage>--%>

                                <div style=" height:30px; padding-top:0; display: none" class="item login-model">
                                    <span class="label">&nbsp;</span>
                                    <div class="fl item-ifo">
                                        <input type="checkbox" class="checkbox" id="readme" checked="checked">
                                        <label for="protocol">我已阅读并同意<a id="protocol" class="blue"
                                                                        href="javascript:void(0)">《网络服务和使用协议》</a></label>
                                    </div>
                                </div>

                                <div class="item item-fore5">
                                    <div class="login-btn">
                                        <input class="btn-img btn-entry" id="submit" value="登&nbsp;&nbsp;&nbsp;&nbsp;录"
                                               type="submit"/>
                                    </div>
                                </div>
                            </form:form>
                        </div>
                        <c:if test="${indexApi:isQqLoginEnable() || indexApi:isWeiboLoginEnable() || indexApi:isWeiXinLoginEnable()}">
	                        <div class="coagent">
	                            <h5>使用合作网站账号登录：</h5>
	                            <ul>
									<c:if test="${indexApi:isWeiXinLoginEnable()}">
		                                 <li >
		                                 	<a  href="${contextPath }/thirdlogin/weixin/pc/authorize" >
								        		<img src="<ls:templateResource item='/resources/templets/images/lp_wx.jpg'/>">
								        	</a>
								        </li>
	                                </c:if>
	                                <c:if test="${indexApi:isQqLoginEnable()}">
		                                <li style="margin-left:5px;">
		                                    <a  href="${contextPath }/thirdlogin/qq/pc/authorize" >
		                                      <img  alt="QQ登录" src="<ls:templateResource item='/resources/templets/images/qq_onnect_logo_7.png'/>">
		                                    </a>
		                                </li>
	                                </c:if>
	                                <c:if test="${indexApi:isWeiboLoginEnable()}">
		                               <li style="margin-left:5px;">
		                                    <a  href="${contextPath }/thirdlogin/sinaweibo/pc/authorize" >
		                                    	<img  alt="" src="<ls:templateResource item='/resources/templets/images/loginButton_24.png'/>">
		                                    </a>
		                                </li>
	                                </c:if>
	                             </ul>
	                        </div>
                        </c:if>
                    </div>
                </div>
         </div>
         </div>
         </div>
		 <%@ include file="home/simpleBottom.jsp" %>
  </div>
</body>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/common/js/randomimage.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/login.js'/>"></script>


<script type="text/javascript">






    var error = '${param.error}';
    function normalLogin() {
        $("#normalLoginTab").attr("class", "active");
        $("#mobileLoginTab").removeAttr("class");
        $("#formlogin").show();
        $("#normalForm").css("display","block");
        $("#moblieForm").hide();
    }

    function mobileLogin() {
        $("#mobileLoginTab").attr("class", "active");
        $("#normalLoginTab").removeAttr("class");
        $("#normalForm").css("display","none");
        $("#moblieForm").show();
        $("#formlogin").hide();
    }

    var contextPath="${contextPath}";
</script>
</html>