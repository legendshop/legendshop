<!DOCTYPE html>
<html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<head>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>帮助中心-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
</head>

<body class="graybody">
<div id="doc">
    <div id="hd">
        <%@ include file="home/top.jsp" %>
    </div>
    <div id="bd">
        <div class="yt-wrap" style="border: 0px solid #ddd;">
            <div class="hc_pbox clearfix">
                <input type="hidden" id="userName" name="userName" value="${userName}">
                <c:forEach items="${newsCatList}" var="item">
                    <div class="hc_div" style="border: 0px solid #ddd;width:900px;height: 100%">
                        <h3>${item.key.value}</h3>
                        <div class="list_hc">
                            <ul>
                                <c:forEach items="${item.value}" var="news">
                                    <li style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;width:100%;">
                                        <a href="javaScript:void(0);" name="news" onclick="requestJump(${news.newsId})" title="${news.newsTitle}">${news.newsTitle}</a>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
    <%@ include file="home/bottom.jsp" %>
</div>
</body>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/helpCenter.js'/>"></script>
<script>
    var contextPath = "${contextPath}";
    var requestJumpType = 0;
    $(function () {
        var userName = $("#userName").val();
        if (userName) {
            requestJumpType = 1;
        }
    });

    function requestJump(id) {
        if (requestJumpType === 1) {
            window.location.href = contextPath + '/news/' + id;
        } else {
            login();
        }
    }

    function login() {
        layer.open({
            title: "本网站资讯仅供网站会员浏览，会员服务费2元/月",
            type: 2,
            id: "login",
            content: contextPath + '/loadLoginOverlay', //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
            area: ['440px', '540px']
        });
    }


</script>
</html>
