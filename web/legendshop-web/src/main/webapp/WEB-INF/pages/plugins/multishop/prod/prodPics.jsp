<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<li>
	<div class="fieldset multimage">
		<label class="caption ">商品图片:<em style="color: red;font-family: simsun;font-size: 12px;font-style: normal;line-height: 24px;margin: 0 0 0 6px;">*</em></label>
		<div class="fields-box">
			<div class="multimage-wrapper">
				<div class="multimage-tabs">
					<div class="tab actived">本地上传</div>
					<div type="remote-image" class="tab">图片空间</div>
				</div>
				<div class="multimage-panels">
					<div class="panel local-panel">
						<div class="upload-field">
							<label>选择本地图片：</label> <a class="btn-default" href="javascript:void(0);"> <span class="btn-txt">文件上传</span> <input type="file" id="file" name="files" onchange="javascript:uploadFile()"
								multiple="multiple"> </a>
						</div>
						<div class="multimage-tips">
							<div class="tip-title">提示：</div>
							<ol>
								<li>本地上传图片大小不能超过<strong class="bright">500K</strong>。</li>
								<li>本类目下您最多可以上传<strong class="bright"> 6 </strong>张图片。</li>
							</ol>
						</div>
					</div>
					<div class="panel remote-image hide"></div>
				</div>
				<div class="multimage-info ">
					<div class="info-wrapper">
						<div class="msg">
							<span class="bright">450*450</span> 以上的图片可以在商品详情页主图提供图片放大功能 <b>(图片可以拖动排序)</b>
						</div>
						<div class="multimage-gallery">
							<ul>
								<c:forEach items="${productDto.imgFileList}" var="item" varStatus="s">

									<c:choose>
										<c:when test="${rsImage!=1}">
											<li data-index="${s.index+1}" class="primary prodImg has-img haveImage">
										</c:when>
										<c:otherwise>
											<li data-index="${s.index+1}" class="prodImg">
										</c:otherwise>
									</c:choose>
									<div class="preview" style="display: block !important ">
										<c:if test="${empty isSimilar}">
											<img src="<ls:photo item='${item.filePath}'/>" />
											<input type="hidden" value="${item.filePath}" name="imgurl">
										</c:if>
									</div>
									<div class="operate">
										<i class="toleft" onclick="toleft(this);">左移</i> <i class="toright" onclick="toright(this);">右移</i> <i class="del" onclick="delImg(this);">删除</i>
									</div>
									</li>
									<c:set value="${imgcout+1}" var="imgcout"></c:set>
									<c:if test="${s.last}">
										<c:forEach begin="${imgcout}" end="5" varStatus="s">
											<li data-index="${s.index+1}" class="prodImg">
												<div class="preview"></div>
												<div class="operate">
													<i class="toleft" onclick="toleft(this);">左移</i> <i class="toright" onclick="toright(this);">右移</i> <i class="del" onclick="delImg(this);">删除</i>
												</div>
											</li>
										</c:forEach>
									</c:if>
								</c:forEach>
								<c:if test="${empty productDto.imgFileList}">
									<c:forEach begin="0" end="5" varStatus="s">
										<c:choose>
											<c:when test="${s.count ==1}">
												<li data-index="${s.count}" class="primary prodImg">
											</c:when>
											<c:otherwise>
												<li data-index="${s.count}" class="prodImg">
											</c:otherwise>
										</c:choose>
										<div class="preview"></div>
										<div class="operate">
											<i class="toleft" onclick="toleft(this);">左移</i> <i class="toright" onclick="toright(this);">右移</i> <i class="del" onclick="delImg(this);">删除</i>
										</div>
										</li>
									</c:forEach>
								</c:if>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
        <div class="multimage-panels panel">
            <div class="panel local-panel">
                <label class="caption ">商品视频:</label>
                <div class="upload-field">
                    <label>选择本地视频：</label> <a class="btn-default" href="javascript:void(0);"> <span class="btn-txt">文件上传</span>
                    <input type="file" id="video"   accept="audio/mp4, video/mp4,.avi" name="files" onchange="javascript:uploadVideoFile()"
                           multiple="multiple"></a>
                </div>
                <div class="multimage-tips">
                    <div class="tip-title">提示：</div>
                    <ol>
                        <li>本地上传大小不能超过<strong class="bright">30MB</strong>。</li>
                    </ol>
                </div>
                <div <c:if test="${empty productDto.proVideoUrl}">style="display: none;"</c:if> id="videoMain">
                    <label class="caption ">主图视频:</label>
                        <div class=" multimage-info" id="myVideo" >
                            <c:choose>
                                <c:when test="${not empty productDto.proVideoUrl}">
                                    <video style='width: 320px;height: 240px' controls='controls' type="file" src="<ls:photo item='${productDto.proVideoUrl}'/>"   accept="audio/mp4, video/mp4,.avi"></video>
                                    <a href='javascript:void(0)'><span onclick='delVideo()'>删除视频</span></a>
                                </c:when>
                            </c:choose>
                        </div>
                </div>
            </div>
        </div>
    </div>
</li>