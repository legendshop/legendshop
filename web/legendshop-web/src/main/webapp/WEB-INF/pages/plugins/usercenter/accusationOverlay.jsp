<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<html>
<head>
<title>举报信息</title>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/reportForbitOverlay.css'/>" rel="stylesheet"/>
</head>
<body>
<div class="detailsjb"> 
            <table width="100%" cellspacing="0" cellpadding="0" border="0" class="report_table">
               <tbody>
               		 <c:forEach items="${requestScope.list}" var="accusation" varStatus="status">
                  <tr>
                    <td width="114" valign="top" align="right">举报商品：</td>
	                  	<td valign="top">
	                  	<a href=" ${contextPath}/views/${accusation.prodId}"  target="_blank">${accusation.prodName}</a>
	                  	</td>
                	</tr>
				   <tr>
                    <td width="114" valign="top" align="right">举报类型：</td>
                    <td valign="top">${accusation.accuType}</td>
                  </tr>
                  <tr>
                    <td valign="top" align="right">举报主题：</td>
                    <td valign="top">${accusation.title}</td>
                  </tr>
                  <tr>
                    <td valign="top" align="right">举报时间：</td>
                    <td valign="top"><fmt:formatDate value="${accusation.recDate}" type="date" /></td>
                  </tr>
                  <tr>
                    <td valign="top" align="right">举报内容：</td>
                    <td valign="top"> ${accusation.content}</td>
                  </tr>
                  <tr>
                    <td valign="top" align="right">举报状态：</td>
                    <td valign="top">
                    	<c:choose>
     						<c:when test="${accusation.status ==1}">已处理</c:when>
     						<c:otherwise>未处理</c:otherwise>
     					</c:choose>
                    </td>
                  </tr>
                  <tr>
                    <td valign="top" align="right">处理结果：</td>
                     <td valign="top">
                     	<c:choose>
			     			<c:when test="${accusation.result ==1}">无效举报</c:when>
			     			<c:when test="${accusation.result ==2}">有效举报</c:when>
			     			<c:when test="${accusation.result ==3}">恶意举报</c:when>
			     			<c:otherwise></c:otherwise>
			     		</c:choose>
                     </td>
                  </tr>
                  <c:if test="${accusation.pic1 !=null}">
	                  <tr>
	                    <td valign="top" align="right">取证图片1：</td>
	                    <td valign="top">
	                                          <a href="<ls:photo item='${accusation.pic1}'/>" target="_blank"> <img src="<ls:images item='${accusation.pic1}' scale="2"/>"  /> </a>
	                    </td>
	                  </tr>
                  </c:if>
                  <c:if test="${accusation.pic2 !=null}">
	                  <tr>
	                    <td valign="top" align="right">取证图片2：</td>
	                    <td valign="top">
	                      					 <a href="<ls:photo item='${accusation.pic2}'/>" target="_blank"> <img src="<ls:images item='${accusation.pic2}' scale="2"/>"  /> </a>
	                      </td>
	                  </tr>
                  </c:if>
                  <c:if test="${accusation.pic3 !=null}">
	                  <tr>
	                    <td valign="top" align="right">取证图片3：</td>
	                    <td valign="top">
	                                          <a href="<ls:photo item='${accusation.pic3}'/>" target="_blank"> <img src="<ls:images item='${accusation.pic3}' scale="2"/>"  /></a>
	                      </td>
	                  </tr>
                  </c:if>
                  </c:forEach>
            </tbody></table>
          </div>
</body>
</html>