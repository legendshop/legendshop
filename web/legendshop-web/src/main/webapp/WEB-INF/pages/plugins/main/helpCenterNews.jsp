<!DOCTYPE>
<html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<head>
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<c:set var="navigationItemList"  value="${bottomApi:queryNavigationItem()}"></c:set>
	<title>帮助中心-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
     <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/helpCenter.css'/>" rel="stylesheet"/>
</head>
<body>
<div class="help-banner">
	 <div id="hd">
		       <%@ include file="home/header.jsp" %>
				<div id="hdmain_cart">
					<div class="yt-wrap">
						<h1 class="ilogo">
							<a href="${contextPath}/"><img src="<ls:photo item="${systemConfig.logo}"/>" style="max-height:89px;" /></a>
						</h1>
		
						<!--page tit-->
						<span class="p_t">帮助中心</span>
						<!--page tit end-->
					</div>
				</div>
			</div>

		 <div class="hc_sbox">
            <div class="hc_w"></div>
      	 <form:form  action="${contextPath}/helpCenter/search" method="get" id="searchform" name="searchform">
	            <div class="hc_s_news">
	               <input style="margin-top: 25px;" type="text" name="word" id="word" value="<c:out value="${param.word}"></c:out>" placeholder="请输入您的问题"/>
	               <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/> 
	               <a href="javascript:void(0);" onclick="searchNews();">搜索</a>
	            </div>       
        </form:form>  
        </div>
         <div class="clear"></div>
      </div>
	</div>
	<div style="width: 1200px; margin:auto;margin-top: 40px;">
	 <div class="news_wrap"  style="width: 900px;float:left;margin-right:15px;">
		<div class="content" align="left">
				<c:choose>
					<c:when test="${not empty requestScope.list}">
						<c:forEach items="${requestScope.list}" var="news" varStatus="status">
						<div class="result">
							<div class="content_title"><h3><a href="<ls:url address='/news/${news.newsId}' />" target="_blank">${news.newsTitle}</a></h3></div>
							<div class="content_summary">
								<c:choose>
									<c:when test="${fn:length(news.newsContent)>120}">
										<c:set var="newsContent" value="${fn:substring(news.newsContent,0,120)}"></c:set>
										<fmt:formatDate value="${news.newsDate}" pattern="yyyy-MM-dd" />&nbsp;-&nbsp;${newsContent}...
									</c:when>
									<c:otherwise>
										<fmt:formatDate value="${news.newsDate}" pattern="yyyy-MM-dd" />&nbsp;-&nbsp;${news.newsContent}
									</c:otherwise>
								</c:choose>
							</div>
						</div>	
						<p class="nabs"></p>
						</c:forEach>
					</c:when>
					<c:otherwise>
							<div class="content_date" align="center">没有找到'${param.keyword}'的内容</div>	
					</c:otherwise>
				</c:choose>
				 <div style="margin-top:10px;" class="page clearfix">
			     			 <div class="p-wrap">
			            		 
										<span class="p-num">
											<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
										</span>
									
			     			 </div>
	  				</div>
		</div>
		</div>
		<%@ include file="newsCategory.jsp"%>
		</div>
		<div class="clear"></div>
	
			<div style="margin-top: 20px;">
				<%@ include file="home/bottom.jsp" %>
			</div>
		<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
		<script type="text/javascript" src="${contextPath}/resources/common/js/jquery.textSearch-1.0.js"></script>
	</body>
<script>
function searchNews(){
	   var keyword = document.getElementById("word").value;
	   if(keyword == undefined || keyword==null ||keyword ==""){
	     alert("请输入搜索关键字！");
	     return ;
	   }
	   document.getElementById("searchform").submit();
	}
	
	$(function(){
		$(".result").hover(function(){
			$(this).addClass("hover");
		},function(){
			$(this).removeClass("hover");
		});
		if($(".content_summary").text().match("${param.keyword}")){
			$(".content_summary").textSearch("${param.keyword}",{markClass: "highlight"});
		}
		if($(".content_title").text().match("${param.keyword}")){
			$(".content_title").textSearch("${param.keyword}",{markClass: "highlightTitle"});
		}
		
	})
	function pager(curPageNO){
			window.location.href="${contextPath}/helpCenter/search?keyword=${param.keyword}&curPageNO="+curPageNO;
		}
	function changePager(curPageNO){
		document.getElementById("curPageNO").value=curPageNO;
		document.getElementById("searchform").submit();
	}
	
</script>
</html>