<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>个人资料 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con">
     
         <div class="o-mt"><h2>个人资料</h2></div>
         
         <div class="pagetab2">
                 <ul>           
              <li id="userInfo" class="on"><span>基本信息</span></li>      
                 </ul>
         </div>
         
         <div class="formbox m10" style="position:relative;">
         	 <input type="hidden" id="id" value="${MyPersonInfo.id}"> 
             <div class="item2">
                <span class="itemName">用户名：</span>
                <div class="fl phoneContainer2"><span>${MyPersonInfo.userName}</span>  </div>   
           	 </div>
             
             <div class="item">
                <span class="itemName">昵称：</span>
                <div class="fl yzm2">
                    ${MyPersonInfo.nickName}
                    <div class="prompt-06">
						<span id="petName_msg"></span>
					</div>
                </div>   
           	 </div>
             
             
             <div class="item2">
                <span class="itemName">性别：</span>
        
                <div class="fl phoneContainer2">
                	<c:choose>
                		<c:when test="${empty MyPersonInfo.sex}">
                			保密
                		</c:when>
                		<c:when test="${MyPersonInfo.sex=='M'}">
                			男
                		</c:when>
                		<c:when test="${MyPersonInfo.sex=='F'}">
                			女
                		</c:when>
                	</c:choose>
           	 	</div>
           	 </div>
             
             
             <div class="item2">
                <span class="itemName">手机号码：</span>
        
                <div class="fl phoneContainer2"><span>${empty MyPersonInfo.userMobile?'未绑定手机':MyPersonInfo.userMobile}<a href="${contextPath}/p/security" target="_blank">修改</a></span> </div>   
           	 </div>
             
             
             <div class="item2">
                <span class="itemName">邮箱：</span>
        
                <div class="fl phoneContainer2"><span>${empty MyPersonInfo.userMail?'未绑定邮箱':MyPersonInfo.userMail}<a href="${contextPath}/p/security" target="_blank">修改</a></span> </div>   
           	 </div>
             
             
             <div class="item">
                <span class="itemName">真实姓名：</span>
                <div class="fl yzm2">
                	${MyPersonInfo.realName}
                     
                   <c:if test="${empty MyPersonInfo.realName}">
                   		暂未设置
                   </c:if>
                </div>   
           	 </div>
             
             <div class="item">
                <span class="itemName">身份证号码：</span>
                <div class="fl yzm2" >
	                ${MyPersonInfo.idCard}
	                <c:if test="${empty MyPersonInfo.idCard}">
	                	暂未设置
	                </c:if>
                </div>   
           	 </div>
             
             
             <div class="item2">
                <span class="itemName">所在地：</span>
        
                <%-- <div class="fl phoneContainer2">
                	<c:if test="${not empty MyPersonInfo.provinceid }">
                	 <select disabled="disabled" class="combox sele" id="provinceid" name="provinceid"
									requiredTitle="true" childNode="cityid"
									selectedValue="${MyPersonInfo.provinceid}"
									retUrl="${contextPath}/common/loadProvinces">
								</select>
							<c:if test="${not empty MyPersonInfo.cityid }">
								 <select disabled="disabled" class="combox sele" id="cityid" name="cityid"
									requiredTitle="true" selectedValue="${MyPersonInfo.cityid}"
									showNone="false" parentValue="${MyPersonInfo.provinceid}"
									childNode="areaid"
									retUrl="${contextPath}/common/loadCities/{value}">
								</select> 
							</c:if>
							<c:if test="${not empty MyPersonInfo.areaid }">
								<select disabled="disabled" class="combox sele" id="areaid" name="areaid"
									requiredTitle="true" selectedValue="${MyPersonInfo.areaid}"
									showNone="false" parentValue="${MyPersonInfo.cityid}"
									retUrl="${contextPath}/common/loadAreas/{value}">
								</select>
							</c:if>
                	  </c:if>
                	  <!-- 详细地址 -->
				</div>  --%>
				<div class="fl yzm2">
	                ${MyPersonInfo.province}${MyPersonInfo.city}${MyPersonInfo.area}
	                <c:if test="${empty MyPersonInfo.province && empty MyPersonInfo.city && empty MyPersonInfo.area}">
	                	暂未设置
	                </c:if>
                </div>   
                	  
           	 </div>
           	 <div class="item">
                <span class="itemName">详细地址：</span>
                <div class="fl yzm2">
                    ${MyPersonInfo.userAdds}
	                <c:if test="${empty MyPersonInfo.userAdds}">
	                	暂未设置
	                </c:if>
                </div>   
           	 </div>
             <div class="item">
                <span class="itemName">QQ：</span>
                <div class="fl yzm2">
                    ${MyPersonInfo.qq}
	                <c:if test="${empty MyPersonInfo.qq}">
	                	暂未设置
	                </c:if>
                </div>   
           	 </div>
             
             <div class="item2">
                <span class="itemName">生日：</span>
        
                <div class="fl phoneContainer2">
	                <span>
						<fmt:formatDate
								value="${MyPersonInfo.birthDate}" type="both" dateStyle="long"
								pattern="yyyy-MM-dd" />
	                </span>
                </div>   
           	 </div>
             
             
             <div class="item2">
                <span class="itemName">婚姻状况：</span>
        
                <div class="fl phoneContainer2">
						<c:choose>
					     	<c:when test="${MyPersonInfo.marryStatus==1}">
					     		未婚
					     	</c:when>
					     	<c:when test="${MyPersonInfo.marryStatus==2}">
							     已婚
							</c:when>
							<c:when test="${MyPersonInfo.marryStatus==3}">
					     		保密
					     	</c:when>
					     	<c:otherwise>
					     		未设置
					     	</c:otherwise>
		     			</c:choose>
                </div>   
           	 </div>
             
              <div class="item2">
                <span class="itemName">月收入：</span>
        
                <div class="fl phoneContainer2">
                	<c:if test="${MyPersonInfo.incomeLevel == 0}">
						无收入
                	</c:if>
                	<c:if test="${MyPersonInfo.incomeLevel == 1}">
						2000以下
                	</c:if>
                	<c:if test="${MyPersonInfo.incomeLevel == 2}">
						2000~3999
                	</c:if>
                	<c:if test="${MyPersonInfo.incomeLevel == 3}">
						4000~5999
                	</c:if>
                	<c:if test="${MyPersonInfo.incomeLevel == 4}">
						6000~7999
                	</c:if>
                	<c:if test="${MyPersonInfo.incomeLevel == 5}">
						8000以上
                	</c:if>
				</div>   
           	 </div>
             
             
             <div class="item">
                <span class="itemName">兴趣爱好：</span>
                <div class="fl yzm2" style="width:697px;">
                	${MyPersonInfo.interest}
                  	<c:if test="${empty MyPersonInfo.interest}">
                  		暂未设置
                  	</c:if>
                </div>   
           	 </div>
             
             
             
             <div class="item" id="submitItem">
                <span class="itemName"></span>
                <div style="    width: 270px;margin: 0 auto;">
                    <input style="width: 270px;font-size:16px;border-radius:2px;" onclick="window.location='${contextPath}/p/user/edit'" type="button" class="submit" value="修改" clstag="pageclick|keycount|new_201511197|1" >
                </div>
            </div>
            
            <div class="userinfo-right-extra">
				<div class="m">

					<div class="mc">
						<div class="headpic">
							<div class="fore"></div>
							<div class="i-pic">
								<c:choose>
									<c:when test="${not empty MyPersonInfo.portraitPic }">
										<img alt="用户头像" src="<ls:photo item='${MyPersonInfo.portraitPic}' />" id="infoimg" style="width: 100px;height :100px;">
									</c:when>
									<c:otherwise>
										<img alt="用户头像" src="${contextPath}/resources/templets/images/no-img_mid_.jpg" id="infoimg" style="width: 100px;height :100px;">
									</c:otherwise>
								</c:choose>
							</div>
							<div class="fore1"></div>
						</div>
					</div>

				</div>
			</div>
            
         </div>
         
       
       
     </div>
    <!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script type="text/javascript">
	var contextPath = '${contextPath}';
	$(document).ready(function() {
		$("select.combox").initSelect();
		userCenter.changeSubTab("personinfo");
	});
</script>
</body>
</html>