<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<div class="tab-con"> 
	<c:forEach items="${requestScope.list}" var="productComment">
	    <div class="comment-item"> 
	     <div class="user-column"> 
	      <div class="user-info"> 
	      <c:choose>
	      	<c:when test="${empty productComment.portrait }">
		       <img src="${contextPath }/resources/templets/images/sculpture.png" width="80" height="80" alt="${productComment.userName }" class="avatar" />
		    </c:when>
		    <c:otherwise>
		    	<img src="<ls:images item='${productComment.portrait }' scale='1'/>" width="80" height="80" alt="${productComment.userName }" class="avatar" />
		    </c:otherwise>
	      </c:choose>
	      
	      </div> 
	      <div>
	      </div>
	      <div class="user-level"> 
	      
	      <p style="margin-bottom: 5px;color: #666;"> ${productComment.userName }</p>
	      	<c:choose>
	      		<c:when test="${productComment.gradeName eq '皇冠会员'}">
	      			<span style="color:#e1a10a">${productComment.gradeName }</span> 
	      		</c:when>
	      		<c:otherwise>
	      			<span style="color:#666">${productComment.gradeName }</span> 
	      		</c:otherwise>
	      	</c:choose>
	      </div> 
	     </div> 
		 
	     <div class="comment-column J-comment-column"> 
	      
		  <!-- 评论分数 -->
		
		  <div class="comment-star star${productComment.score }"></div> <span style="color:#458B00">订单号：${productComment.subNumber }</span>
	
		
	      
		  <!-- 评论内容 -->
		  <p class="comment-con">${productComment.content }</p> 
		  
		  <!-- 评论图片列表 -->
		  <c:if test="${not empty productComment.photoPaths}">
		      <div class="pic-list J-pic-list"> 
		      	<c:forEach items="${productComment.photoPaths}" var="photo">
				   <a class="J-thumb-img" href="#none" data="<ls:images item='${photo }' scale='0'/>"><img src="<ls:images item='${photo }' scale='1'/>" width="48" height="48" alt="" /></a> 
				</c:forEach>
			  </div>
			  <!-- 大图预览区域-->
		  	  <div class="J-pic-view-wrap clearfix">
		  	  	<img src="" style="margin-left: 0px; margin-top: 0px;">
		  	  </div>
		  </c:if>
		  
		  <!-- 商家回复 -->
		  <c:choose>
		  	<c:when test="${productComment.isReply}">
				  <div class="reply-box"> 
					 <div class="reply-con"> 
					  <span class="u-name">您的回复：</span> 
					  <span class="u-con">${productComment.shopReplyContent}</span> 
					 </div> 
					 <div class="reply-meta">
					   <fmt:formatDate value="${productComment.shopReplyTime}"  pattern="yyyy-MM-dd" />
					 </div> 
				  </div>
			  </c:when>
			  <c:otherwise>
			  	<div class="shop-reply">
			  		<a href="#none" class="shop-reply-btn" commentId="${productComment.id}" userName="${productComment.userName }" replyType="first">回复用户</a>
			  	</div>
			  </c:otherwise>
		  </c:choose>
		  
		  <c:if test="${productComment.isAddComm and productComment.addStatus eq 1}">
		  	  <div class="append-info">
		  	  	<c:choose>
		  	  		<c:when test="${productComment.appendDays eq 0}">用户当天追加评论</c:when>
		  	  		<c:otherwise>用户${productComment.appendDays}天后追加评论</c:otherwise>
		  	  	</c:choose>
		  	  </div>
		  	  
		  	  <!-- 评论内容 -->
			  <p class="comment-con">${productComment.addContent }</p> 
			  
			  <!-- 评论图片列表 -->
			  <c:if test="${not empty productComment.addPhotoPaths}">
			      <div class="pic-list J-pic-list"> 
			      	<c:forEach items="${productComment.addPhotoPaths}" var="photo">
					   <a class="J-thumb-img" href="#none" data="<ls:images item='${photo }' scale='0'/>"><img src="<ls:images item='${photo }' scale='1'/>" width="48" height="48" alt="" /></a> 
					</c:forEach>
				  </div>
				  <!-- 大图预览区域-->
		  	  	  <div class="J-pic-view-wrap clearfix">
		  	  	  	<img src="" style="margin-left: 0px; margin-top: 0px;">
		  	  	  </div>
			  </c:if>
			  
			  <!-- 商家回复追加评论 -->
			  <c:choose>
			  	<c:when test="${productComment.addIsReply}">
					  <div class="reply-box"> 
						 <div class="reply-con"> 
						  <span class="u-name">您的回复：</span> 
						  <span class="u-con">${productComment.addShopReplyContent}</span> 
						 </div> 
						 <div class="reply-meta">
						   <fmt:formatDate value="${productComment.addShopReplyTime}"  pattern="yyyy-MM-dd" />
						 </div> 
					  </div>
				  </c:when>
				  <c:otherwise>
				  	<div class="shop-reply">
				  		<a href="#none" class="shop-reply-btn" commentId="${productComment.id}" userName="${productComment.userName }" replyType="append">回复用户</a>
				  	</div>
				  </c:otherwise>
			  </c:choose>
		  </c:if>
		  
		  <!-- 其他信息 -->
	      <div class="comment-message"> 
	       <div class="buy-info">
	       	<c:if test="${not empty productComment.attribute }">
	       		<span>${productComment.attribute }</span>
	       	</c:if>
	        <span>购买时间: <fmt:formatDate value="${productComment.buyTime }"  pattern="yyyy-MM-dd HH:mm" /></span> 
	        <span>评论时间: <fmt:formatDate value="${productComment.addtime }"  pattern="yyyy-MM-dd HH:mm" /></span> 
	       </div> 
		   
		   <!-- 点赞和回复按钮-->
	       <div class="comment-op"> 
	        <a class="J-nice" prodCommId="${productComment.id }">
	        	<i class="sprite-praise"></i><span class="num">${productComment.usefulCounts }</span>
	        </a> 
	        <a href="${contextPath }/productcomment/productCommentDetail?prodComId=${productComment.id }" target="_blank">
	        	<i class="sprite-comment"></i><span class="num">${productComment.replayCounts }</span>
	        </a> 
	       </div> 
	      </div> 
	     </div> 
	</div>
	</c:forEach>
</div>

<div class="clear"></div>
<div style="margin-top:10px;" class="page clearfix">
   <div class="p-wrap">
      	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	</div>
</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/commonUtils.js'/>" /></script>
<script type="text/javascript">
	
	/** 放大评论图片 **/
	$(".J-thumb-img").click(function(){
		var $this = $(this);
		
		$this.siblings().removeClass("current");
		$this.addClass("current");
		
		var imagePath = $this.attr("data");
	    var imageView = $this.parent().next(".J-pic-view-wrap").find("img");
		imageView.attr("src", imagePath);
		
		return false;
	});
	
	/** 选中评论图片 **/
	$(".J-pic-view-wrap img").on("click", function(){
		var $this = $(this);
		$this.attr("src", "");
		
		var a = $this.parent().prev(".J-pic-list").find(".J-thumb-img");
		a.removeClass("current");
		a.siblings().removeClass("current");
		return false;
	});
	
	/** 隐藏或显示回复输入框 */
	var replyInput = null;
	$(".shop-reply-btn").on("click", function(){
		var $this = $(this);
		var replyTextarea = $this.parent().next(".reply-textarea");
		if(replyTextarea.length){//如果有
			if(replyTextarea.is(":visible")){//如果是显示状态
				replyTextarea.hide();
			}else{
				replyTextarea.find(".reply-input").val("");
				replyTextarea.show();
			}
		}else{
			var commentId = $this.attr("commentId");
			var userName = $this.attr("userName");
			var replyType = $this.attr("replyType");
			replyTextarea = buildReplyInput(commentId, userName, replyType);
			
			$this.parent().after(replyTextarea.show());
		}
		return false;
	});
	
	$(".comment-column").on("click", ".reply-submit", function(){
		var $this = $(this);
		var commentId = $this.attr("commentId");
		var replyContent = $this.parent().prev(".inner").find(".reply-input").val();
		
		if(!$.trim(replyContent)){
			layer.msg("亲, 回复内容不能为空哦!",{icon:0});
			return false;
		}
		
		if(replyContent.length > 250){
			layer.msg("亲, 回复内容不能超过250个字符哦!",{icon:0});
			return false;
		}
		
		var checkResult = checkSensitiveData(replyContent);
		if(checkResult && checkResult.length){
			layer.msg("亲, 您输入的内容中包含敏感字 " + checkResult + " !",{icon:0});
			return false;
		}
		
		var replyType = $this.attr("replyType");
		
		var url = contextPath + "/s/comments/replyComment";
		if(replyType === "append"){
			url = contextPath + "/s/comments/replyAddComment";
		}
		var params = {
			"prodComentId": commentId,
			"replyContent": replyContent,
		};
		
		$.ajaxUtils.sendPost(url, params, function(result){
			if(result === "OK"){
				$.remindUtils.showTips("回复成功!", function(){
					window.location.reload(true);
				});
			}else if(result === "fail"){
				layer.msg("对不起, 回复失败, 请联系平台客服!",{icon:0});
			}else{
				layer.msg(result,{icon:2});
			}
		});
		
		return false;
	});

</script>