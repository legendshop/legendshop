<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>添加拍卖活动-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/bidding-activity${_style_}.css'/>"
          rel="stylesheet">
</head>
<style>
    .prod-table {
        margin: 10px 0;
        border: solid #ddd;
        border-width: 1px 0px 0px 1px;
        margin-left: 160px;
    }

    .prod-table tr td {
        margin: 0px;
        border: solid #ddd;
        border-width: 0px 1px 1px 0px;
        font-size: 12px;
    }

    .prod-table thead tr td {
        text-align: center;
        background-color: #F9F9F9;
        padding: 8px;
        font-size: 12px;
    }

    .prod-table tbody tr td {
        padding: 8px 0px 8px 0px;
        font-size: 12px;
    }
    em{
        color: #8A1F11;
    }
</style>
<body class="graybody">
<div id="doc">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
    <div class="w1190">
        <div class="seller-cru">
            <p>
                您的位置> <a href="${contextPath}/home">首页</a>>
                <a href="${contextPath}/sellerHome">卖家中心</a>> <font class="on">拍卖活动管理</font>
            </p>
        </div>

        <%@ include
                file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
        <div class="auctions-right">
            <form action="${contextPath}/s/auction/save" method="post" id="form1" enctype="multipart/form-data">

                <input type="hidden" id="id" name="id" value="${auctions.id}"/>
                <!-- 基本信息 -->
                <div class="basic-ifo">
                    <!-- 基本信息头部 -->
                    <div class="ifo-top">
                        基本信息
                    </div>
                    <div class="ifo-con">
                        <li style="margin-bottom: 5px;">
                            <span class="input-title"><span class="required">*</span>活动名称：</span>
                            <input type="text" class="active-name" name="auctionsTitle" id="auctionsTitle" value="${auctions.auctionsTitle}" maxLength="25" placeholder="请填写活动名称"><em></em>
                        </li>
                        <li>
                            <span class="input-title"></span>
                            <span style="color: #999999;margin-top: 10px;">活动名称最多为25个字符</span>
                        </li>
                        <li style="margin-bottom: 0;">
                            <span class="input-title"><span class="required">*</span>活动时间：</span>
                            <fmt:formatDate value="${auctions.startTime}" pattern="yyyy-MM-dd HH:mm:ss" var="startTime"/>
                            <span><input type="text" class="active-time" readonly="readonly"  name="startTime"  id="startTime" value='${startTime}' placeholder="开始时间"/><em></em></span>
                            <span style="margin: 0 5px;">-</span>
                            <fmt:formatDate value="${auctions.endTime}" pattern="yyyy-MM-dd HH:mm:ss" var="endTime"/>
                            <span><input type="text" class="active-time" readonly="readonly"  name="endTime"  id="endTime" value='${endTime}' placeholder="结束时间"/><em></em></span>
                        </li>
                    </div>
                </div>
                <!-- 选择活动商品 -->
                <div class="basic-ifo" style="margin-top: 20px;">
                    <!-- 选择活动商品头部 -->
                    <div class="ifo-top">
                        选择活动商品
                    </div>
                    <div class="ifo-con">
                        <div class="red-btn" onclick="showProdlist()">选择商品</div>
                        <div class="check-shop prodList">
                            <c:if test="${not empty dto}">
                                <table border="0" cellpadding="0" cellspacing="0" style="margin: 30px 0;">
                                    <tbody>
                                    <tr style="background: #f9f9f9;">
                                        <td width="70px">商品图片</td>
                                        <td width="170px">商品名称</td>
                                        <td width="100px">商品价格</td>
                                        <td width="100px">商品规格</td>
                                    </tr>
                                    <tr class="first" >
                                        <td>
                                            <img src="<ls:photo item='${dto.prodPic}'/>" width="80px" >
                                        </td>
                                        <td>
                                            <span class="name-text">${dto.prodName}</span>
                                        </td>
                                        <td>${dto.prodPrice}</td>
                                        <td>${empty dto.cnProperties?'暂无':dto.cnProperties}</td>
                                    </tr>
                                    </tbody>
                                    <input type="hidden" id="skuId" name="skuId" value="${auctions.skuId}"/>
                                    <input type="hidden" value="${auctions.prodId}" name="prodId" id="prodId"/>
                                </table>
                            </c:if>
                        </div>
                    </div>
                </div>
                <!-- 活动规则 -->
                <div class="basic-ifo" style="margin-top: 20px;">
                    <!-- 活动规则头部 -->
                    <div class="ifo-top">
                        活动规则
                    </div>
                    <div class="ifo-con">
                        <li><font class="input-title"><b style="color: red">*</b>起拍价：</font><input type="text" maxLength="10" value="${auctions.floorPrice}"
                                                                               onkeyup="value=value.replace(/\.\d{2,}$/,value.substr(value.indexOf('.'),3))"
                                                                               name="floorPrice"
                                                                               id="floorPrice"><em></em></li>
                        <li><font class="input-title"><b style="color: red">*</b>是否封顶：</font>
                            <select class="isCeiling" name="isCeilingSelect">
                            <option value="">——请选择——</option>
                            <option value="0" <c:if test="${empty auctions.isCeiling || auctions.isCeiling==0}">selected</c:if>>不封顶</option>
                            <option value="1" <c:if test="${auctions.isCeiling==1}">selected</c:if>>封顶</option>
                        </select><input name="isCeiling" id="isCeiling" type="hidden" value="${auctions.isCeiling}"/><em></em></li>
                        <li><font class="input-title"><b style="color: red">*</b>是否隐藏起拍价：</font><select class="hideFloorPrice"
                                                                                    name="hideFloorPriceSelect">
                            <option value="">——请选择——</option>
                            <option value="0" <c:if test="${empty auctions.hideFloorPrice || auctions.hideFloorPrice==0}">selected</c:if>>不隐藏</option>
                            <option value="1" <c:if test="${auctions.hideFloorPrice==1}">selected</c:if>>隐藏</option>
                        </select><input name="hideFloorPrice" id="hideFloorPrice" type="hidden" value="${auctions.hideFloorPrice}"/><em></em></li>
                        <li class="fixedPrice" <c:if test="${auctions.isCeiling==0}">style="display: none" </c:if>><font class="input-title"><b style="color: red">*</b>一口价：</font><input type="text" value="${auctions.fixedPrice}"
                                                                                                  maxLength="10"
                                                                                                  <c:if test="${auctions.isCeiling==0}">class="ignore"</c:if>
                                                                                                  onkeyup="value=value.replace(/\.\d{2,}$/,value.substr(value.indexOf('.'),3))"
                                                                                                  name="fixedPrice"
                                                                                                  id="fixedPrice"><em></em>
                        </li>
                        <li><font class="input-title"><b style="color: red">*</b>最小加价幅度：</font><input type="text" maxLength="10" value="${auctions.minMarkupRange}"
                                                                                  onkeyup="value=value.replace(/\.\d{2,}$/,value.substr(value.indexOf('.'),3))"
                                                                                  name="minMarkupRange"
                                                                                  id="minMarkupRange"><em></em></li>
                        <li><font class="input-title"><b style="color: red">*</b>最大加价幅度：</font><input type="text" maxLength="10" value="${auctions.maxMarkupRange}"
                                                                                  onkeyup="value=value.replace(/\.\d{2,}$/,value.substr(value.indexOf('.'),3))"
                                                                                  name="maxMarkupRange"
                                                                                  id="maxMarkupRange"><em></em></li>
                        <li><font class="input-title"><b style="color: red">*</b>是否支付保证金：</font><select class="isSecurity"
                                                                                    name="isSecuritySelect">
                            <option value="">——请选择——</option>
                            <option value="0" <c:if test="${auctions.isSecurity==0}">selected</c:if>>不支付保证金</option>
                            <option value="1" <c:if test="${empty auctions.isSecurity || auctions.isSecurity==1}">selected</c:if>>支付保证金</option>
                        </select><input name="isSecurity" id="isSecurity" type="hidden" value="${auctions.isSecurity}"/><em></em></li>
                        <li class="securityPrice" <c:if test="${auctions.isSecurity==0}">style="display: none" </c:if>><font class="input-title"><b style="color: red">*</b>保证金：</font><input type="text" value="${auctions.securityPrice}"
                                                                                                     maxLength="10"
                                                                                                     <c:if test="${auctions.isSecurity==0}">class="ignore"</c:if>
                                                                                                     onkeyup="value=value.replace(/\.\d{2,}$/,value.substr(value.indexOf('.'),3))"
                                                                                                     name="securityPrice"
                                                                                                     id="securityPrice"><em></em>
                        </li>
                        <li class="delayTime"><font class="input-title"><b style="color: red">*</b>延时周期：</font><input type="text" value="${auctions.delayTime}"
                                                                                                  maxLength="3"
                                                                                                  name="delayTime"
                                                                                                  id="delayTime">分钟/次<em></em>
                        </li>
                        <li class="textarea"><font style="float: left;" class="input-title">拍品描述：</font><textarea name="auctionsDesc"
                                                                                              id="auctionsDesc"
                                                                                              cols="50" rows="8"
                                                                                              style="width:680px;height:300px;visibility:hidden;">${auctions.auctionsDesc}</textarea><em></em>
                        </li>
                    </div>
                </div>
                <div class="bto-btn">
                    <input type="submit" class="red-btn" value="提交" />
                    <div class="red-btn return-btn" onclick="javascript:history.go(-1);">返回</div>
                </div>
            </form>
        </div>


    </div>
    <%@ include
            file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script charset="utf-8"
        src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
<script src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
<script type="text/javascript">
    var auctions = "${auctions.id}";
    var paramData = {
        contextPath: "${contextPath}",
        cookieValue: "<c:out value="${cookie.SESSION.value}"/>",
    }

    $(document).ready(function () {
        userCenter.changeSubTab("auctionsManage"); //高亮菜单
        laydate.render({
            elem: '#startTime',
            calendar: true,
            theme: 'grid',
            type: 'datetime',
            min: '-1',
            trigger: 'click'
        });

        laydate.render({
            elem: '#endTime',
            calendar: true,
            theme: 'grid',
            type: 'datetime',
            min: '-1',
            trigger: 'click'
        });
    });

    KindEditor.options.filterMode=false;
    KindEditor.create('textarea[name="auctionsDesc"]', {
        cssPath : '${contextPath}/resources/plugins/kindeditor/plugins/code/prettify.css',
        uploadJson : '${contextPath}/editor/uploadJson/upload;jsessionid=${cookie.SESSION.value}',
        fileManagerJson : '${contextPath}/editor/uploadJson/fileManager',
        allowFileManager : true,
        resizeType: 0,
        afterBlur:function(){this.sync();},
        height:'350px',
        afterCreate : function() {
            var self = this;
            KindEditor.ctrl(document, 13, function() {
                self.sync();
                document.forms['example'].submit();
            });
            KindEditor.ctrl(self.edit.doc, 13, function() {
                self.sync();
                document.forms['example'].submit();
            });
        }
    });
</script>
<script type="text/javascript" language="javascript"
        src="<ls:templateResource item='/resources/templets/js/auction/addAuction.js'/>"></script>
</body>
</html>
