<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>商家包邮活动</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css" href="${contextPath}/resources/templets/css/shipping/shippingActive.css" rel="stylesheet" />
<link type="text/css" href="${contextPath}/resources/plugins/default/layer-user.css" rel="stylesheet" />
</head>
<body>
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
	</div>
	<div class="w1190">
		<div class="seller-cru">
			<p>
				您的位置>
				<a href="javascript:void(0)">首页</a>
				>
				<a href="javascript:void(0)">卖家中心</a>
				>
				<span class="on">包邮活动管理</span>
			</p>
		</div>
		<!-- /面包屑 -->
		<!-- 侧边导航 -->
		<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
		<!-- /侧边导航 -->
		<!-- 拼团列表 -->
		<div class="fight" id="shipContent">
			<div class="pagetab2">
				<ul id="listType">
					<li class="on"><span>包邮活动管理</span></li>
				</ul>
   			</div>
			<div class="set-up sold-ser sold-ser-no-bg">
				<div class="add-btn">
					<input type="button" value="添加店铺包邮" class="btn-r" style="cursor: pointer;" id="add-shop">
					<input type="button" value="添加商品包邮" class="btn-r" style="cursor: pointer;" id="add-prod">
				</div>
				<div class="searh-input">
					<form:form action="${contextPath}/s/shippingActive/query" id="myForm">
						活动名称:
						<input type="text" placeholder="请输入活动名称" name="name" id="name" value="${name}">
						<input type="hidden" name="curPageNO" value="${curPageNO}" id="curPageNO">
						
						<a href="javascript:void(0)" class="sea-btn" id="searchBtn">搜索</a>
					</form:form>
				</div>
			</div>
			<table class="sold-table" cellspacing="0" cellpadding="0" style="text-align:center;vertical-align:middle;">
				<tr class="fight-tit">
					<th>活动名称</th>
					<th width="180">活动时间</th>
					<th>活动类型</th>
					<th>活动信息</th>
					<th>状态</th>
					<th width="180">操作</th>
				</tr>
				<c:if test="${empty list}">
					<tr class="detail">
						<td colspan='7' height="60" style="text-align: center; color: #999;">没有符合条件的信息</td>
					</tr>
				</c:if>
				<c:forEach items="${list}" var="item" varStatus="status">
					<tr class="detail">
						<td>${item.name}</td>
						<td>
							<div class="time" style="padding: 0">
								<span>
									<fmt:formatDate value="${item.startDate}" pattern="yyyy-MM-dd HH:mm:ss" type="date"/>
									至
								</span>
								<span>
									<fmt:formatDate value="${item.endDate}" pattern="yyyy-MM-dd HH:mm:ss" type="date"/>
								</span>
							</div>
						</td>
						<td>
							<c:choose>
								<c:when test="${item.fullType eq 0}">
									店铺包邮
								</c:when>
								<c:otherwise>
									商品包邮
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							${item.description}
						</td>
						<td>
						
						<c:set var="time1"><fmt:formatDate value="${item.endDate}" pattern="yyyy-MM-dd HH:mm:ss" /></c:set>

						<c:set var="time3"><fmt:formatDate value="${item.startDate}" pattern="yyyy-MM-dd HH:mm:ss" /></c:set>

						<c:set var="time2"><fmt:formatDate value="${date}" pattern="yyyy-MM-dd HH:mm:ss" /></c:set>
						
						
						
							<c:choose>
								<c:when test="${item.status == true && time3 > time2}">
									<span class="fight-staing">未开始</span>
								</c:when>
								<c:when test="${item.status == true && time1 > time2}">
									<span class="fight-staing">进行中</span>
								</c:when>
								<c:otherwise>
									<span class="fight-staed">已下线</span>
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<c:if test="${item.status== true}">
								<a href="javascript:void(0);" class="edit btn-r" data-id="${item.id}" data-fullType="${item.fullType}">编辑</a>
								<a href="javascript:void(0);" class="cancel btn-g" data-id="${item.id}">撤销</a>
							</c:if>
							<c:if test="${item.status== false}">
								<!-- 下线或者结束状态，可以删除 -->
								<a href="javascript:void(0);" class="del btn-g" data-id="${item.id}">删除</a>
							</c:if>
						</td>
					</tr>
				</c:forEach>
			</table>
			<div style="margin-top: 10px;" class="page clearfix">
				<div class="p-wrap">
					<ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="simple" />
				</div>
			</div>
		</div>
	</div>
	<!--foot-->
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	<!--/foot-->
	<script type="text/javascript">
		var contextPath = "${contextPath}";
		var curPageNO = "${curPageNO}";
	</script>
	<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/layer/layer.js'/>"></script>
	<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/shipping/shippingActive.js'/>"></script>
</body>
</html>