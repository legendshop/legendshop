<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<div class="clear"></div>
<c:set var="advList" value="${floor.floorItems.RA}"></c:set>
<div class="yt-wrap bf_floor_new clearfix">
	<div class="adv5">
		<c:forEach items="${advList}" var="adv" varStatus="index">
			<div class="adv5-${index.count}">
				<a target="_blank" title="${adv.title}" href="${adv.linkUrl}"> <img
                        class="lazy" alt="${adv.title}" src="${contextPath}/resources/common/images/loading1.gif" data-original="<ls:photo item='${adv.picUrl}'/>"
                        style="width:222px;height: 300px;">
				</a>
			</div>
		</c:forEach>
	</div>
</div>