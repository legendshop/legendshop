<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
  <title>联系人信息-${systemConfig.shopName}</title>
  <meta name="keywords" content="${systemConfig.keywords}"/>
  <meta name="description" content="${systemConfig.description}" />
  <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/templets/css/errorform.css'/>" />

</head>
<body>
<div id="doc">
   <div id="hd">
		 <%@ include file="../home/top.jsp" %>
   </div>
   
   <div id="bd">
       <div class="yt-wrap" style="padding:0px 0 20px;">
            <div class="seller-cru" style="width:1190px;margin: 20px auto 20px;">
              <p>
                您的位置&gt;
                <a href="#">首页</a>&gt;
                <span style="color: #e5004f;">免费开店</span>
              </p>
            </div>
            <div class="pagetab2">
               <ul>           
                 <li><span>签订入驻协议</span></li>
                 	<li class="on"><span>联系人信息提交</span></li>
                  <c:if test="${shopType eq 1}">                
                	<li ><span>公司信息提交</span></li>
                 </c:if>
                 <li><span>店铺信息提交</span></li>   
                 <li><span>审核进度查询</span></li>   
               </ul>       
            </div>
            <div class="main">
                <div class="settled_box">
                  <h3>
                    <div class="settled_step">
                      <ul>
                     	<li class="step1"><a href="#">入驻选择</a></li>
                        <li class="step7"><a href="#">入驻须知</a></li>
                        <li class="step6"><a href="#">联系人信息认证</a></li>
                        <c:if test="${shopType == 1}">      
                       		 <li class="step2"><a href="#">公司信息认证</a></li>
                        </c:if>
                        <li class="step3"><a href="#">店铺信息认证</a></li>
                        <li class="step4"><a href="#">等待审核</a></li>
                      </ul>
                    </div>
                    <span class="settled_title">联系方式</span></h3>
                  <div class="settled_white">
                      <div class="settled_white_box">
                        <div class="settled_form">
                          <h4>商家入驻联系人信息<b>用于入驻过程中接收反馈的入驻通知，请务必认真填写</b></h4>
                          <form:form  action="${contextPath}/p/saveContactInfo" method="post" id="contactform"  novalidate="novalidate">
                          		<input  type="hidden" id="type" name="type" value="${shopType}">
	                          <table class="settled_table" border="0" cellpadding="0" cellspacing="0" width="760" style="font-size: 12px;">
	                            <tbody><tr>
	                              <td align="right" valign="top" width="150"><span class="sred_span">
	                              联系人姓名：</span><strong class="sred">*</strong></td>
	                              <td><input class="size200" type="text" id="contactName" name="contactName" value="${shopDetailInfo.contactName}" maxlength="20"><label></label></td>
	                            </tr>
	                            <tr>
	                              <td align="right" valign="top" width="150" ><span class="sred_span">联系人手机：</span><strong class="sred">*</strong></td>
	                              <td><input class="size200" type="text" id="contactMobile" name="contactMobile" value="${shopDetailInfo.contactMobile}" maxlength="20"><label></label></td>
	                            </tr>
	                            <tr>
	                              <td align="right" valign="top" width="150"><span class="sred_span">联系人电子邮箱：</span><strong class="sred">*</strong></td>
	                              <td><input class="size200" type="text" id="contactMail" name="contactMail" value="${shopDetailInfo.contactMail}" maxlength="100"><label></label></td>
	                            </tr>
	                            <tr>
	                              <td align="right" valign="top" width="150"><span class="sred_span">联系人QQ：</span></td>
	                              <td><input class="size200" type="text" id="contactQQ" name="contactQQ" value="${shopDetailInfo.contactQQ}" onkeyup="value=value.replace(/[\uFF00-\uFFFF]/g,'')"  onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\uFF00-\uFFFF]/g,''))" maxlength="50" autocomplete="off"><label></label></td>
	                            </tr>
	                            <tr>
	                            <td></td>
	                              <td><span style="color: #a2a2a2;">多个QQ请用,分割开</span></td>
	                            </tr>
	                          </tbody>
	                          </table>
                          </form:form>
                        </div>
                      </div>
                      <div class="settled_bottom"><span><a href="javascript:void(0);" onclick="saveContact()" class="settled_btn"><em>下一步，完善营业信息</em></a></span></div>
                  </div>
                </div>
            </div>
        </div>
   </div>
    <%@ include file="../home/bottom.jsp" %>
</div>
<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>"  type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/openshop.js'/>"></script>
<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
</body>
</html>
