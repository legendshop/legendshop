<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/dialog.jsp"%>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>预售活动运营</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/fight.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/mergeGroup/mergeGroupDetail.css'/>" rel="stylesheet"/>
</head>
<body>
<div id="doc">
	<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
</div>
<div class="w1190">
	<div class="seller-cru">
		<p>
			您的位置>
			<a href="javascript:void(0)">首页</a>
			>
			<a href="javascript:void(0)">卖家中心</a>
			>
			<span class="on">预售运营</span>
		</p>
	</div>
	<!-- /面包屑 -->
	<!-- 侧边导航 -->
	<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
	<!-- /侧边导航 -->
	<!-- 拼团运营信息 -->
	<div class="fight-operate">

		<!-- 运营统计数据 -->
		<div class="operate-statistics">
			<div class="operate-statistics-tit">
				<p>运营结果</p>
			</div>
			<div class="operate-statistics-det">
				<ul>

					<li>
						交易成功金额：<span><fmt:formatNumber value="${operateStatistics.succeedAmount}" type="currency" pattern="￥##.##"  minFractionDigits="2" /></span>
					</li>
					<li>
						交易成功数量：<span>${operateStatistics.succeedCount}</span>
					</li>
					<li>
						参加人数：<span>${operateStatistics.peopleCount}</span>
					</li>
				</ul>
			</div>
		</div>
		<!-- 运营统计数据 -->

		<table cellpadding="0" cellspacing="0">
			<tr class="tab-tit">
				<th width="60">编号</th>
				<th width="200">订单号</th>
				<th>买家</th>
				<th>数量</th>
				<th width="100">交易金额(元)</th>
				<th>定金</th>
				<th>尾款</th>
			</tr>
			<c:if test="${empty list}">
				<tr class="first-leve">
					<td colspan='7' height="60" style="text-align: center; color: #999;">暂无数据</td>
				</tr>
			</c:if>

			<c:forEach items="${list}" var="item" varStatus="itemStatus">
				<tr>
					<td>${itemStatus.index+1}</td>
					<td>${item.subNumber}</td>
					<td>${item.userName}</td>
					<td>${item.productNums}</td>
					<td>${item.actualTotal}</td>
					<td>${item.isPayDeposit eq 0?'未支付':'已支付'}</td>
					<c:choose>
						<c:when test="${presellProd.payPctType eq 0}">
							<td>无需支付尾款</td>
						</c:when>
						<c:otherwise>
							<td>${item.isPayFinal eq 0?'未支付':'已支付'}</td>
						</c:otherwise>
					</c:choose>
				</tr>
			</c:forEach>
		</table>
		<div style="margin-top: 10px;" class="page clearfix">
			<div class="p-wrap">
				<ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="simple" />
			</div>
		</div>

		<div class="btn-box">
			<input type="button" value="返回" onclick="window.history.go(-1);" />
		</div>

	</div>



</div>

<!-- end 拼团运营信息 -->
<!-- </div> -->
<!--foot-->
<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
<!--/foot-->
</body>
<script language="JavaScript" type="text/javascript">
    $(function(){
        userCenter.changeSubTab("presellManage"); //高亮菜单
    })
    var curPageNO = "${curPageNO}";
    var contextPath = "${contextPath}";
    var presellId = "${presellId}"
    function pager(curPageNO) {

        window.location.href = contextPath+"/s/presellProd/operatorPresellActivity/"+presellId+"?curPageNO="+curPageNO;
    }
</script>
</html>

