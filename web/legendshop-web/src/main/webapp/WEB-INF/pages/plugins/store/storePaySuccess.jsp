<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="renderer" content="webkit"/>
    <c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
    <title>门店订单-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
</head>
<body  class="graybody">
	<div id="doc">
         <!--顶部menu-->
		  <div id="hd">
		      <!--hdtop 开始-->
		       <%@ include file="/WEB-INF/pages/plugins/main/home/header.jsp" %>
		      <!--hdtop 结束-->
		    
		    
		       <!-- nav 开始-->
		        <div id="hdmain_cart">
		          <div class="yt-wrap">
		            <h1 class="ilogo">
		                <a href="${contextPath}/"><img src="<ls:photo item="${systemConfig.logo}"/>"  style="max-height:89px;" /></a>
		            </h1>
		            <!--page tit-->
		              <div class="cartnew-title">
		                        <ul class="step_h">
		                            <li class="done"><span>购物车</span></li>
		                            <li class=" done"><span>填写核对订单信息</span></li>
		                            <li class="on done"><span>提交订单</span></li>
		                        </ul>
		              </div>
		            <!--page tit end-->
		          </div>
		        </div>
		      <!-- nav 结束-->
		 </div><!--hd end-->
		<!--顶部menu end-->
    
        
         <div id="bd">
	      <!--order content -->
	         <div style="margin-top:20px;" class="yt-wrap ordermain">
	          	<div class="cart_done">
	          	   <fmt:formatNumber var="totalAmountFormat" value="${totalAmount}" type="currency" pattern="0.00"></fmt:formatNumber>
	               <div class="v_align_mid carthint">应付金额：<span>￥ ${totalAmountFormat}</span>下单成功，请在7日内前往门店提货，逾期订单将被取消。</div>      
	          	</div>
	            <div style="padding:5px 20px 20px 20px;">
	            <table class="ncm-default-table m10">
	            <thead>
	              <tr>
	                <th>订单号</th>
	                <th>支付方式</th>
	                <th>金额（元）</th>
	              </tr>
	            </thead>
	            <tbody>
	              <c:forEach items="${orderList}" var="sub" varStatus="status">
								      <tr>
										 <td><a target="_blank" href="${contextPath}/p/orderDetail/${sub.subNumber}">${sub.subNumber}</a></td>
										 <td>门店自提支付</td>
										 <td>${sub.actualTotal}</td>
									  </tr>
					 </c:forEach>
	            </tbody>
	          </table>
	          </div>
	        </div>
        </div>
        <!--order content end-->
      </div>
      
	   <!----foot---->
	   <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
      <!----foot end---->
	
	
	</div>
</body>
</html>
