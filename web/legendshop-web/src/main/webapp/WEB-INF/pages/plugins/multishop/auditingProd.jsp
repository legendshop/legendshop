<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<html>
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>待审核的商品-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-inventory${_style_}.css'/>" rel="stylesheet">
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
	<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">待审核的商品</span>
				</p>
			</div>
<!-- 面包屑 -->
		<%@ include file="included/sellerLeft.jsp" %>
			<div class="seller-inventory">
				<div class="seller-com-nav" cellspacing="0" cellpadding="0">
					<ul>
						<li ><a href="${contextPath}/s/prodInStoreHouse">仓库中的商品</a></li>
						<li><a href="${contextPath}/s/violationProd">违规的商品</a></li>
						<li class="on"><a href="${contextPath}/s/auditingProd">等待审核的商品</a></li>
						<li><a href="${contextPath}/s/auditFailProd">审核失败的商品</a></li>
					</ul>
				</div>
				<form:form  action="${contextPath}/s/auditingProd" method="get" id="from1">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
					<div class="sold-ser sold-ser-no-bg clearfix">
						<div class="fr">
							<div class="item">
								商品名称：
								<input type="text" class="item-inp" value="${prod.name}" name="name"  id ="name" style="width:300px;">
								<input type="button" onclick="search()" class="btn-r" id="btn_keyword" value="搜  索" >	
							</div>
						</div>
					</div>
				</form:form>
				<!-- 搜索 -->
				<table class="inventory-table sold-table" style="margin-top:0;" cellspacing="0" cellpadding="0">
					<tr class="inventory-tit">
						<!-- <td class="check"><input type="checkbox" id="checkbox" onclick="selAll();"></td> -->
						<th>商品</th>
						<th>商品名称</th>
						<th>价格</th>
						<th>库存</th>
						<th>订购数</th>
					</tr>
					 <c:choose>
			           <c:when test="${empty requestScope.list}">
			              <tr>
						 	 <td colspan="20" style="border-right: 1px solid #e4e4e4">
						 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
						 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
						 	 </td>
						 </tr>
			           </c:when>
			           <c:otherwise>
			               	<c:forEach items="${requestScope.list}" var="product" varStatus="status">
								<tr class="detail">
									<%-- <td class="check"><input name="strArray" type="checkbox" value="${product.prodId}"  arg="${product.name}"></td> --%>
									<td><a href="${contextPath}/views/${product.prodId}" target="_blank"><img src="<ls:images scale="3" item='${product.pic}'/>"></a></td>
									<td><a href="${contextPath}/views/${product.prodId}">${product.name}</a></td>
									<td>¥${product.cash}</td>
									<td>
										 <c:choose>
		                      				 <c:when test="${product.stocksArm ne 0 && (product.stocks le product.stocksArm) }">
		                         				 <span class="col-orange" style="font-weight:600;">${product.stocks}</span>
		                       				</c:when>
		                      				 <c:otherwise>
		                        				 ${product.stocks}
		                       				</c:otherwise>
		                   				 </c:choose>
									</td>
									<td>${product.buys}</td>
								</tr>
							</c:forEach>
			           </c:otherwise>
		             </c:choose>
				</table>
					<div class="clear"></div>
					<div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
				            		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
				     			 </div>
		  				</div>
			</div>
		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/auditingProd.js'/>"></script>
	<script type="text/javascript">
		var contextPath="${contextPath}";
		function search(){
		  	$("#from1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		  	$("#from1")[0].submit();
		}
	</script>
</body>
</html>