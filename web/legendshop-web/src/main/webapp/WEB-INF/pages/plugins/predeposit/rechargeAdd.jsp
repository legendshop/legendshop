<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>预存款充值 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/pdCashManager${_style_}.css'/>" rel="stylesheet" />
	 
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
     <%@ include file="/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp" %>
    <!-----------right_con-------------->
     <div class="right_con">
<div>
	<div class="o-mt">
		<h2>账户信息</h2>
	</div>
	<div class="pagetab2">
		<ul>
			<li><span><a href="<ls:url address='/p/predeposit/account_balance'/>">账户余额</a> </span></li>
			<li ><span><a href="<ls:url address='/p/predeposit/recharge_detail'/>">充值明细</a> </span></li>
			<li ><span><a href="<ls:url address='/p/predeposit/balance_withdrawal'/>">余额提现</a> </span></li>
			<li class="on"><span>在线充值</span></li>
		</ul>
	</div>

	<div class="ncm-default-form">
		<form:form  action="${contextPath}/p/predeposit/recharge_pay" id="recharge_form" method="post">
			<dl>
				<dt>
					<i class="required">*</i>充值金额：
				</dt>
				<dd>
					<input type="text" maxlength="5" id="pdrAmount" class="text w50" name="pdrAmount">
					<em class="add-on"><i class="icon-renminbi">元</i> </em>
					  <span></span>
				</dd>
			</dl>
			
			<dl>
				<dt>
					<i class="required">*</i>图形验证码：
				</dt>
				<dd>
					<input type="text" autocomplete="off" size="10" maxlength="4"
						id="captcha" class="text" name="captcha" >
				 	<img border="0" class="ml5 vm" id="codeimage" name="codeimage"
						src="<ls:templateResource item='/validCoderRandom'/>" >
						<a onclick="javascript:changecodeimage()" class="ml5 blue" href="javascript:void(0)">看不清？换张图</a> 
					<label class="error" generated="true" for="captcha"></label>
				</dd>
			</dl>
						
			<dl class="bottom">
				<dt>&nbsp;</dt>
				<dd>
					<label class="submit-border">
					<input type="button" value="充值" class="submit btn-r small-btn" style="padding: 0px 10px;">
					</label>
				</dd>
			</dl>
		</form:form>
	</div>
	
	<div class="m m1 alert" style="margin-top: 10px;padding:10px;">
   			<div class="mt">
   				<h3>温馨提示：</h3>
   			</div>
			<div class="mc">
				<p>
					1. 充值成功后，余额可能存在延迟现象，一般1到5分钟会内到账，如有问题，请咨询客服；
					<br>
					2. 充值金额输入值必须是不小于1且不大于99999的正整数；
					<br>
					3. 您只能用储蓄卡或支付宝在线支付充值金额的钱，如遇到任何支付问题可以查看在线支付帮助；
					<br>
					4. 充值完成后，您可以进入账户充值记录页面进行查看余额充值状态。
				</p>
			</div>
   		</div>
   		
	<div class="clear"></div>
</div>

</div>
    <!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/templets/js/rechargeAdd.js'/>" type="text/javascript"></script>
	<script type="text/javascript">
		var contextPath = "${contextPath}";
	</script>  
</body>
</html>
			