<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>账户安全 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/securityCenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/validateChanges.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;">     
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con"  id="content">
     	<div class="right">
			<c:choose>
			<c:when test="${mailVerifn eq 0 or mailVerifn == null }">
				<div class="o-mt"><h2>验证邮箱</h2></div>
				<div id="step2" class="step step01">
					<ul>
						<li class="fore1">1.验证身份<b></b></li><li class="fore2">2.验证邮箱<b></b></li><li class="fore3">3.完成</li>
					</ul>
				</div>
			</c:when>
			<c:otherwise>
				<div class="o-mt"><h2>修改邮箱</h2></div>
				<div id="step2" class="step step01">
					<ul>
						<li class="fore1">1.验证身份<b></b></li><li class="fore2">2.修改邮箱<b></b></li><li class="fore3">3.完成</li>
					</ul>
				</div>
			</c:otherwise>
			</c:choose>
		<div id="middle">
			<div class="m m1 safe-sevi" style="background:#fff;">
				<div class="mc">
			
        			<div class="form">
        				<div class="item"> 
        					<span class="label">我的邮箱：</span>
        					<div class="fl">
        					    <input name="oldPassword"  id="oldPassword"  type="hidden" value="${oldPassword }">
        					    <input name="securityCode"  id="securityCode"  type="hidden" value="${securityCode }">
        					    
        						<input tabindex="1" class="text" name="userEmail" id="userEmail" type="text" maxlength="25">
        						<div class="clr"></div>
								<div id="emailtext" class="msg-text" style="display:none">请输入您的常用邮箱</div>
								<div class="clr"></div>
        						<div id="email_error" class="msg-error"></div>
        					</div>
        					<div class="clr"></div>
            			</div>
        				<div class="item">
            				<span class="label">验证码：</span>
            				<div class="fl">
            					<input type="hidden" id="cannonull" name="cannonull" value='<fmt:message key="randomimage.errors.required"/>'/>
								<input type="hidden" id="charactors4" name="charactors4" value='<ls:i18n key="randomimage.charactors.required" length="4"/>'/> 
								<input type="hidden" id="errorImage" name="errorImage" value='<fmt:message key="error.image.validation"/>'/> 
												
								<input class="text" tabindex="2" name="randNum" id="randNum" type="text" maxlength="4">
								<label><img id="randImage" name="randImage" src="<ls:templateResource item='/validCoderRandom'/>"  style="vertical-align: middle;"/>看不清？
            					<a href="javascript:void(0)" onclick="javascript:changeRandImg('${contextPath}')" style="font-weight: bold;"><fmt:message key="change.random2"/></a>
            					</label>
            					<div class="clr"></div>
            					<div id="authCode_error" class="msg-error"></div>
            				</div>
            				<div class="clr"></div>
            			</div>
        				<div class="item">
        					<span class="label">&nbsp;</span>
        					<div class="fl">
								<a id="submitCode" class="btn-r small-btn ncbtn" onclick='javascript:toUpdateMail.sendNewMail("${code}", "${userName}");' href="javascript:void(0);"><s></s>提交</a>
								<a class="btn-r small-btn ncbtn" href="${contextPath}/p/security"><s></s>返回</a>
							</div>
        					<div class="clr"></div>
        				</div>
    				</div>
				</div>
			</div>
			<div class="m m7">
				<div class="mt"><h3>为什么要验证邮箱？</h3></div>
				<div class="mc">1.验证邮箱可加强账户安全，您可以使用已验证邮箱快速找回密码或支付密码；<br>2.已验证邮箱可用于账户余额变动提醒；</div>
			</div>
	</div>
	</div>
</div>
    <!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script type="text/javascript"  src="<ls:templateResource item='/resources/common/js/randomimage.js'/>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		toUpdateMail.doExecute();
	});
</script>
</body>
</html>