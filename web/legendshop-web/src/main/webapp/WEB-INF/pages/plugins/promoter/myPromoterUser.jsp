<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
<head>
	<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>我的订单 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
<div id="doc">
   <div id="bd">
      <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
     
<div class=" yt-wrap" style="padding-top:10px;">     
       <%@include file='/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp'%>
    
     <div class="right_con">
     
         <div class="o-mt"><h2>我推广的会员</h2></div>
         <div class="pagetab2">
               <ul>
                   <li class="on"><span>我推广的会员</span></li>
               </ul>
          </div><hr/>
         <div class="commission" style="margin-top:15px;">
           <div class="pro-add">
             <span><b>${monthTotal}</b><em>本月发展会员</em></span>
             <span><b>${totalUser}</b><em>累计发展会员</em></span>
             <span id="firstDistCommis"><b>${originUserCommis.firstDistCommis}</b><em>直接下级会员贡献佣金</em></span>
             <span><a href="${contextPath}/p/promoter/applyPromoter" style="margin-top: 15px;">我要推广</a></span>
           </div>

			<form:form action="${contextPath}/p/promoter/myPromoterUser" method="post" id="from1">
	           <div class="rew-sty">
	           	  <div class="fr">
		             <span>会员名：<input type="text" id="userName" name="userName" value="${userCommis.userName}" style="width:250px;"></span>
		             <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
		             <span>注册时间：<input style="width:120px;" type="text" value="<fmt:formatDate value="${userCommis.startParentBindingTime}" pattern="yyyy-MM-dd HH:mm:ss" />" readonly="readonly"   id="startParentBindingTime" name="startParentBindingTime" ></span>
		             <span>至：<input style="width:120px;" type="text" value="<fmt:formatDate value="${userCommis.endParentBindingTime}" pattern="yyyy-MM-dd HH:mm:ss" />" readonly="readonly"   id="endParentBindingTime" name="endParentBindingTime" ></span>
		             <span><input type="submit" class="btn-r small-btn" style="background-color:#e5004f;" value="查询"></span>
           		  </div>	
	           </div>
          </form:form>
          
           <div class="rew-lis">
             <table cellpadding="0" cellspacing="0" class="sold-table">
               <tr>
                 <th>会员名</th>
                 <th>会员手机号</th>
                 <th width="230">注册时间</th>
                 <th width="170">累计获得佣金</th>
               </tr>
               <c:set var="firstDistCommis" scope="session" value="0.00"/>
                 <c:choose>
		           <c:when test="${empty requestScope.list}">
		              <tr>
					 	 <td colspan="20">
					 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
					 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
					 	 </td>
					 </tr>
		           </c:when>
		           <c:otherwise>
		                <c:forEach items="${requestScope.list}" var="userCommis" varStatus="status">
			               <tr>
			                 <td>${userCommis.userName}</td>
			                 <td>${userCommis.userMobile}</td>
			                 <td><fmt:formatDate value="${userCommis.parentBindingTime}" type="both"/></td>
			                 <td>¥<fmt:formatNumber value="${userCommis.totalDistCommis}" type="currency" pattern="0.00"></fmt:formatNumber></td>
			               </tr>
		               </c:forEach>
		           </c:otherwise>
		         </c:choose>
             </table>
               <div class="clear"></div>
				<div style="margin-top:10px;" class="page clearfix">
	     			 <div class="p-wrap">
						<span class="p-num">
							<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
						</span>
	     			 </div>
  				</div>
           </div>      
         </div>
     </div>
    <div class="clear"></div>
 </div>
<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
</div>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/myPromoterUser.js'/>"></script>
<script type="text/javascript">
	var contextPath='${contextPath}';
	$(function(){
		laydate.render({
			   elem: '#startParentBindingTime',
			   calendar: true,
			   theme: 'grid',
			   trigger: 'click'
		  });
     
     	laydate.render({
			   elem: '#endParentBindingTime',
			   calendar: true,
			   theme: 'grid',
			   trigger: 'click'
		  });
	});
</script>
</body>
</html>
