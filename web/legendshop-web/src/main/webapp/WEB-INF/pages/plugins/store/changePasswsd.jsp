<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
	<head>
		<title>修改密码</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
		<title>修改密码-${systemConfig.shopName}</title>
   		 <meta name="keywords" content="${systemConfig.keywords}"/>
    	<meta name="description" content="${systemConfig.description}" />
    	<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
    	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/shopUser.css'/>" rel="stylesheet"> 
		<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>"  type="text/javascript"></script>
	</head>
	<body>
			<div>
				<table>
					<tbody>
					<tr>
						<td class="td-title"><b>*</b>密码</td><td> <input type="password" onfocus="clearErr(this);" onblur="checkPasswd();" id="password" name="password"><span class="err-msg hide"></span></td>
					</tr>
					<tr>
						<td class="td-title"><b>*</b>确认密码</td><td> <input type="password" onfocus="clearErr(this);" onblur="checkPasswd2();" id="password2"><span class="err-msg hide"></span></td>
					</tr>
				</tbody></table>
			</div>
			<div class="btn-content">
				<input onclick="changePwd();" type="button" value="保存" class="btn">
				<input onclick="closeDialog();" style="margin-left:10px;" type="button" value="取消" class="btn">
			</div>
		<script type="text/javascript">
			function clearErr(ths) {
				$(ths).parent().find(".err-msg").hide();
			}
			function checkPasswd() {
				var result = true;
				var password = $("#password").val();
								
			    var regx1= /^[0-9A-z`~!@#$%^&*()_\-+=<>?:"{}|,.\/;'\\[\]·~！@#￥%……&*（）——\-+={}|《》？：“”【】、；‘’，。、]+$/g;
			    var regx= /^(?![0-9]+$)(?![a-z]+$)(?![A-Z]+$)(?!([^(0-9a-zA-Z)]|[\(\)])+$)([^(0-9a-zA-Z)]|[\(\)]|[a-z]|[A-Z]|[0-9]){6,20}$/;
			    if(password.indexOf(" ") > -1){
			    	$("#password").parent().find(".err-msg").html("登录密码不能包含空格");
					$("#password").parent().find(".err-msg").show();
					result = false;
			    }
			    if (password == null || password == '' || password.length <= 0 || password == '请输入6-20位密码') {
					$("#password").parent().find(".err-msg").html("登录密码不能为空");
					$("#password").parent().find(".err-msg").show();
					result = false;
			    }
				if (password.length<6||password.length>20) {
					$("#password").parent().find(".err-msg").html("密码长度只能在 6-20 个字符之间");
					$("#password").parent().find(".err-msg").show();
					result = false;
				}
				if (!regx.test(password) || !regx1.test(password)) {
					$("#password").parent().find(".err-msg").html("密码为6-20位字母、数字和标点符号的组合");
					$("#password").parent().find(".err-msg").show();
					result = false;
				}
				return result;
			}
			function checkPasswd2() {
				var result = true;
				var password =  $.trim($("#password").val());
				var password2 =  $.trim($("#password2").val());
				if (password != password2) {
					$("#password2").parent().find(".err-msg").html("两次输入的密码不一致");
					$("#password2").parent().find(".err-msg").show();
					result = false;
				}
				return result;
			}
			function closeDialog() {
				var index = parent.layer.getFrameIndex("updateStorePwd"); //先得到当前iframe层的索引
				parent.layer.close(index); //再执行关闭   
			}
			
			function changePwd(){
				var errCount = 0;
				if(!checkPasswd()){
					errCount++;
				}
				if(!checkPasswd2()){
					errCount++;
				}
				if(errCount!=0){
					return;
				}
				var password = $.trim($("#password").val());
				$.ajax({
			        //提交数据的类型 POST GET
			        type:"POST",
			        data:{"password":password},
			        //提交的网址
			        url:"${contextPath}/s/store/changeStorePwd/${storeId}",
			        //提交的数据
			        async : false,  
			        //返回数据的格式
			        datatype: "json",
			        //成功返回之后调用的函数            
			        success:function(data){
			        	 var result=eval(data);
			        	 if(result=="OK"){
			        		 parent.layer.msg("修改成功!",{icon:1,time:1000},function(){
				        		 parent.layer.closeAll();
			        		 });
			        	 }else{
			        		 parent.layer.msg("修改失败!",{icon:2});
			        	 }
			        }
			    });
				
			}
		</script>
	</body>
</html>