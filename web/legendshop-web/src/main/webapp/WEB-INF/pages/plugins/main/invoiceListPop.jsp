<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<%@include file='/WEB-INF/pages/common/layui.jsp'%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/invoiceListPop.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
    <div class="invoice-wrap">
        <div class="invoice">
            <div class="invoice-content">
                <div class="top-nav">
                    <div class="invoice-type item <c:if test="${invoiceType eq 1}">on</c:if>" onclick="location='${contextPath}/p/invoiceListPop?invoiceType=1'">普通发票</div>
                    <div class="invoice-type item <c:if test="${invoiceType eq 2}">on</c:if>" onclick="location='${contextPath}/p/invoiceListPop?invoiceType=2'">增值税发票</div>
                    <div class="invoice-type item <c:if test="${invoiceType eq 3}">on</c:if>" onclick="location='${contextPath}/p/invoiceListPop?invoiceType=3'">不开发票</div>
                </div>

                <form action="" ></form>
                <!-- 发票列表 -->
                <div class="msg-bottom invoice">

                    <div class="list">
                        <c:choose>

                            <c:when test="${empty invoiceList}">
                                <div class="noList">
                                    <span>
                                        <c:choose>
                                            <c:when test="${invoiceType eq 3}">已选择不开发票</c:when>
                                            <c:otherwise>暂无数据</c:otherwise>
                                        </c:choose>
                                    </span>
                                </div>

                            </c:when>
                            <c:otherwise>
                                <ul class="invoice-list">
                                    <c:forEach items="${invoiceList}" var="invoice" varStatus="status">
                                        <li class="item" onclick="checkedInvoice('${invoice.id}',this);">
                                            <div class="left">
                                                <div class="content">
                                            <span class="title">
                                                <c:choose>
                                                    <c:when test="${invoice.title eq 1}">[个人抬头]</c:when>
                                                    <c:otherwise>[公司抬头]&nbsp;</c:otherwise>
                                                </c:choose>
                                                &nbsp;${invoice.company}
                                                <c:if test="${invoice.title ne 1}">
                                                    &nbsp;[税号] ${invoice.invoiceHumNumber}
                                                </c:if>
                                            </span>
                                                    <c:if test="${invoice.commonInvoice eq 1}">
                                                        <span class="default">默认</span>
                                                    </c:if>
                                                </div>
                                                <c:if test="${invoice.type eq 2}">
                                                    <div class="content">
                                                        <span class="title">[开户银行及账号] &nbsp;${invoice.depositBank} &nbsp;${invoice.bankAccountNum}</span>
                                                    </div>
                                                    <div class="content">
                                                        <span class="title">[注册地址及电话] &nbsp;${invoice.registerAddr} &nbsp;${invoice.registerPhone}</span>
                                                    </div>
                                                </c:if>
                                            </div>
                                            <div class="right">
                                                <c:if test="${invoice.commonInvoice ne 1}">
                                                    <span class="setDef" onclick="setDefault('${invoice.id}')">设为默认</span>
                                                </c:if>
                                                <span class="edit" onclick="location='${contextPath}/p/invoice/update?invoiceId=${invoice.id}'">编辑</span>
                                                <span class="delete" onclick="deleteById('${invoice.id}')">删除</span>
                                            </div>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </c:otherwise>
                        </c:choose>

                        <div class="addInvoice" onclick="location='${contextPath}/p/invoice/createPage'">+ 新增发票</div>
                    </div>

                </div>
                <!-- 发票列表 -->
            </div>
            <input type="hidden" id="checkedInvoice" name="checkedInvoice" value=""/>
            <div class="btns clear">
                <button class="confirm" type="button" onclick="choiceInvoice();">确定</button>
                <button class="cancel" type="button" onclick="cancel();">取消</button>
            </div>
        </div>
    </div>
<script type="text/javascript"  src="<ls:templateResource item='/resources/templets/js/invoiceListPop.js'/>"></script>
<script type="text/javascript">
    var contextPath = "${contextPath}";
    var invoiceType = "${invoiceType}";

</script>
</body>
</html>