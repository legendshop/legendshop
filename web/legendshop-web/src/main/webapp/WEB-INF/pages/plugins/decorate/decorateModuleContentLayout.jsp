<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
<link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.css'/>" />
<style>
  .fiit_save_box input {
    background-color: #23a9db;
    border: medium none;
    color: #fff;
    cursor: pointer;
    height: 30px;
    margin: 0 5px;
    width: 80px;
}
.fiit_save_box input.del {
    background-color: #eee;
    color: #333;
}

.fiit_save_box {
    float: left;
    padding-top: 10px;
    text-align: center;
    width: 100%;
}
.white_box {
    background: #fff none repeat scroll 0 0;
    margin-left: auto;
    margin-right: auto;
    overflow: hidden;
    padding-bottom: 30px;
    text-align: center;
}
</style>
<div class="white_box">
	<form:form  method="post" action="" name="theForm" id="theForm">
		   <input type="hidden" value="loyout_module_custom_prod" name="layoutModuleType" id="layoutModuleType">
		    <input type="hidden" value="${layoutParam.layoutId}" name="layoutId" id="layoutId">
		    <input type="hidden" value="${layoutParam.layout}" name="layout" id="layout">
		    <input type="hidden" value="${layoutParam.divId}" name="divId" id="divId">
		    <input type="hidden" value="${layoutParam.layoutDiv}" name="layoutDiv" id="layoutDiv">
		 <div style="width:auto; float:left; margin-left:22px;">
			<textarea  cols="100" rows="8" name="layoutContent" id="layoutContent" >${layoutContent}</textarea>
		 </div>
		<div class="fiit_save_box">
			<input type="button" onclick="saveContent();" value="确定" id="save">
			<input type="button" value="取消" onclick="javascript:cancel();" class="del">
		</div>
	</form:form>
</div>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
<script type="text/javascript">
	var contextPath = '${contextPath}';
	jQuery(document).ready(function(){
		KindEditor.options.filterMode=false;
	      var editor =	 KindEditor.create('textarea[name="layoutContent"]', {
					cssPath :contextPath + '/resources/plugins/kindeditor/plugins/code/prettify.css',
					uploadJson : contextPath + '/editor/uploadJson/upload;jsessionid=${cookie.SESSION.value}',
					fileManagerJson :contextPath + '/editor/uploadJson/fileManager',
					allowFileManager : true,
					afterBlur:function(){this.sync();},
					width : '755px',
					height:'350px',
					afterCreate : function() {
						var self = this;
						KindEditor.ctrl(document, 13, function() {
							self.sync();
							document.forms['example'].submit();
						});
						KindEditor.ctrl(self.edit.doc, 13, function() {
							self.sync();
							document.forms['example'].submit();
						});
					}
	      });
		  
	});
	
	function saveContent(){
	   var content=$("textarea[name='layoutContent'").val();
	   if(isBlank(content)){
	      parent.layer.alert("请输入文本内容", {icon: 0});
	      return false;
	   }
	   
	     var layoutDiv=$("#layoutDiv").val();
		 var layoutId=$("#layoutId").val();
		 var data={
		    "layoutContent":content,
		    "layoutModuleType":$("#layoutModuleType").val(),
		    "layoutId":layoutId,
		    "layout":$("#layout").val(),
		    "divId":$("#divId").val(),
		    "layoutDiv":layoutDiv
		}
		
		$("#save").attr("disabled","disabled");
	     $.ajax({
	         //提交数据的类型 POST GET
	         type:"POST",
	         //提交的网址
	         url:contextPath+"/s/shopDecotate/layout/saveLayoutContent",
	         //提交的数据
	         data: data,
	         async : true,  
	         //返回数据的格式
	         datatype: "json",
	         //成功返回之后调用的函数            
	         success:function(data){
	              if(data=='fail'){
	                 parent.layer.alert("保存失败", {icon: 2});
	        		  $("#save").removeAttr("disabled");
	              }else{
	                    $('#module_edit').remove();
						if (layoutDiv == "div1") {
							parent.$("div[mark=" + layoutId + "][div=div1][id='content']").html("");
							parent.$("div[mark=" + layoutId + "][div=div1][id='content']").html(content);
						}else if (layoutDiv == "div2") {
							parent.$("div[mark=" + layoutId + "][div=div2][id='content']").html("");
							parent.$("div[mark=" + layoutId + "][div=div2][id='content']").html(content);
						} else {
							parent.$("div[mark=" + layoutId + "][id='content']").html("");
							parent.$("div[mark=" + layoutId + "][id='content']").html(content);
							
						}
						$("#save").removeAttr("disabled");
					
						var index = parent.layer.getFrameIndex('decorateModuleContent'); //先得到当前iframe层的索引
						parent.layer.close(index); //再执行关闭   
	              }
	         },
	         //调用执行后调用的函数
	         complete: function(XMLHttpRequest, textStatus){
	         }      
	      });
	      
	      
		 
	}
	
	function isBlank(_value){
	  if(_value==null || _value=="" || _value==undefined){
		return true;
	  }
	  return false;
   }
	
	function cancel(){
	    parent.layer.confirm("要放弃本次编辑吗？",{
      		 icon: 3
      	     ,btn: ['确定','取消'] //按钮
      	   },function(index, layero){
      		 parent.layer.close(index);
	        var index = parent.layer.getFrameIndex('decorateModuleContent'); //先得到当前iframe层的索引
	        parent.layer.close(index); //再执行关闭   

	    });
	}
</script>
