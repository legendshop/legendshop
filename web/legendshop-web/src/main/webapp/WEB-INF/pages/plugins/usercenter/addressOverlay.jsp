<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller${_style_}.css'/>" rel="stylesheet"/>
 <!-- 新增收货地址 -->
<div class="new-address" style="margin: 20px;">
     <input type="hidden" id="id" value="${userArrress.addrId}">
       <table cellspacing="0" cellpadding="0">
          <tbody>
           <tr>
                <td style="width:100px;"><span>*</span>收&nbsp;货&nbsp;人：</td>
                    <td>
                      <input type="text" id="receiver" value="${userArrress.receiver}"  placeholder="长度不能超过25个字符" maxlength="25">
                      <br><label id="consigneeNameNote"  class="item-con_error hide">收货人不能为空</label>
               </td>
            </tr>
            
            <tr>
                    <td><span>*</span>所在地区：</td>
                    <td> 
                      <select  selectedValue="${userArrress.provinceId}"   id="provinceid" name="provinceid"  class="combox" requiredTitle="true"  childNode="cityid"   retUrl="${contextPath}/common/loadProvinces"  style="width: 100px; margin-right: 10px;">
						</select>
						<select  selectedValue="${userArrress.cityId}"  parentValue="${userArrress.provinceId}"  id="cityid"  name="cityid"  class="combox" requiredTitle="true"   showNone="false"  childNode="areaid"   retUrl="${contextPath}/common/loadCities/{value}"  style="width: 100px; margin-right: 10px;">
						</select>
						<select  selectedValue="${userArrress.areaId}"  parentValue="${userArrress.cityId}"  id="areaid"  name="areaid" class="combox"  requiredTitle="true"     showNone="false"   retUrl="${contextPath}/common/loadAreas/{value}"  style="width: 100px; margin-right: 10px;">
                        </select>
                       <br><span class="item-con_error hide" id="areaNote"></span>
                    </td>
                  </tr>
                  <tr>
                    <td><span>*</span>详细地址：</td>
                    <td><textarea  id="subAdds" placeholder="建议您如实填写详细收货地址，例如街道名称，门牌号码，楼层和房间号等信息" maxlength="100">${userArrress.subAdds}</textarea>
                    <br><span class="item-con_error hide" id="consigneeAddressNote" ></span>
                    </td>
                  </tr>
                  <tr>
                    <td><span>*</span>手机号码：</td>
                    <td><input maxlength="11" type="text" id="mobile" value="${userArrress.mobile}" placeholder="手机号码和固定电话必填一项">
                     <br><span class="item-con_error hide" id="consigneeMobileNote" ></span>
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;&nbsp;&nbsp;固定电话：</td>
                    <td><input type="text" id="telphone"  value="${userArrress.telphone}"   placeholder="手机号码和固定电话必填一项"><span>区号-电话号码</span>
                     <br><span class="item-con_error hide" id="consigneePhoneNote" ></span>
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;&nbsp;&nbsp;电子邮件：</td>
                    <td>
                     <input type="text"  id="email" value="${userArrress.email}"  placeholder="建议填写，方便您了解订单动态">
                      <br><span class="item-con_error hide" id="emailNote" ></span>
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;&nbsp;&nbsp;邮政编码：</td>
                    <td><input type="text" id="subPost" value="${userArrress.subPost}" placeholder="建议填写，有助于快速确定送货地址">
                     <br><span class="item-con_error hide" id="subPostNote" ></span>
                    </td>
                  </tr>
                  <tr>
                  	<td>&nbsp;&nbsp;&nbsp;地址别名：</td>
                  	<td>
                  		<input id="aliasAddr"  value="${userArrress.aliasAddr}"  class="text" maxlength="15" type="text" placeholder="设置一个易记的名称，如:'家里'、'公司'">
                  	</td>
                  </tr>
                  <input type="hidden" id="commonAddr" name="commonAddr"  value="${userArrress.commonAddr}">
          </tbody>
      </table>
              <div class="new-address-but">
              <input type="button"  onclick="addAddress123();" style="width:100px;height:30px;" value="保存收货信息" >
          </div>    
 </div>
          
<script src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script type="text/javascript"  src="<ls:templateResource item='/resources/templets/js/addressOverlay.js'/>"></script>
<script type="text/javascript">
  var contextPath = '${contextPath}';
  $(document).ready(function() {
       $("select.combox").initSelect();
  });
</script>  
