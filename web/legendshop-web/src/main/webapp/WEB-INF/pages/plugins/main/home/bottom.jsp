<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<c:set var="newsMap" value="${bottomApi:getNewsMap()}"></c:set>
<c:set var="externalLinkList" value="${indexApi:getExternalLinkList()}"></c:set>
<c:set var="wechatCodeVO" value="${indexApi:getWechatCodeVO()}"></c:set>
<%
    Integer offset = 1;
%>

<div style="margin-top: 20px;" align="center">
    <div style="width:1190px;margin:auto;">
        <img alt="" src="${contextPath}/resources/templets/images/bottom-adv.gif">
    </div>
    <div style="width:1190px;margin:auto;border-top:1px solid #ddd;margin-top: 20px;" class="clearfix">
        <c:forEach items="${newsMap}" var="entry" end="4" varStatus="stat">
            <div style="color:#333; width:144px; float:left; position:relative;">
                <p style="padding:49px 0 11px 0px; font-size:16px; font-family:黑体;">${entry.value.newsCategoryName}</p>
                <ul>
                    <c:forEach items="${entry.value.newsDtoList}" var="news" end="6">
                        <li style=" padding: 0 0 0 0px;line-height: 24px;"><a href="${contextPath}/news/${news.newsId}" target="_blank">${news.newsTitle}</a></li>
                    </c:forEach>
                </ul>
            </div>
        </c:forEach>
        <div style="float:left;padding: 40px 10px 10px 40px;text-align: center;">
            <div>
                <span style="height: 32px;display: inline-block;line-height: 32px;color: #ff272f;font-size: 21px;">${systemConfig.areaCode}-${systemConfig.telephone}</span>
            </div>
            <div>
                <span>周一至周日 8:00-18:00</span><br>
                <span>（或咨询在线客服）</span>
            </div>
            <div style="margin-top: 10px;">
           		<!-- 屏蔽掉qq客服，换成IM -->
                <%-- <a href="javascript:void(0);" onclick="qqLink('${systemConfig.qqNumber}');" --%>
                <a target="_blank" href="${contextPath}/p/im/customerAdmin/0"
                   style="width:138px;height:32px;background: #e5004f;line-height:32px;display: inline-block;text-align: center;color: #fff;border-radius: 3px;">
                    <i style="background: url('${contextPath}/resources/templets/images/chat-i.png');width: 19px;height: 18px;display: inline-block;vertical-align: middle;margin-right: 5px;margin-bottom: 2px;"></i>在线客服
                </a>
            </div>
        </div>
        <div style="float: right;padding: 40px 0px 10px 34px;text-align: center;">
            <div>
                <img src="<ls:photo item='${wechatCodeVO.wechatOrderCode}'/>" height="100px" width="100px;"/>
            </div>
            <div>
                <span>微信订阅号</span>
            </div>
        </div>
        <div style="float: right;padding: 40px 0px 10px 0px;text-align: center;">
            <div>
                <img src="<ls:photo item='${wechatCodeVO.wechatServiceCode}'/>" height="100px" width="100px;"/>
            </div>
            <div>
                <span>微信服务号</span>
            </div>
        </div>
    </div>
</div>
<!----foot---->
<div class="footline" style="margin-top:30px;" align="center">
    <c:if test="${not empty externalLinkList}">
        <div class="footline footnav" style="text-align:center;">
           <div class="yt-wrap">
                <c:forEach items="${externalLinkList}" var="el" varStatus="stat">
                 <a href="${el.url}">${el.wordlink}</a>
                 <c:if test="${!stat.last}">|</c:if>
                </c:forEach>
               <br />
           </div>
       </div>
   </c:if>

    <div style="text-align:center; padding:20px 0;">
        <div class="yt-wrap">
            &copy;Copyright Power By
            <a href="${systemConfig.url}" target="_blank" style="margin: 0 10px;">${systemConfig.shopName}</a>
            All Rights Reserved.
        </div>
        <div>${systemConfig.icpInfo}</div>

    </div>
</div>

<script>
    function qqLink(qqs) {
        if (qqs != undefined && qqs != "") {
            if (qqs.indexOf(",") > 0) {
                var qqList = qqs.split(",");
                window.location = "tencent://message/?uin=" + qqList[0] + "&Site=在线客服&Menu=yes";
            } else {
                window.location = "tencent://message/?uin=" + qqs + "&Site=在线客服&Menu=yes";
            }
        }
    }
</script>
<!----foot end---->