<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
  <head>
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>退货详情-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
	<style type="text/css">
		.required{color:red;}
		.ret-detail .ste-con ul li .tips{display: block;width: 379px;color: #666;margin-left: 120px;text-align: left;}
		.ret-detail .ste-con ul li .input-radio{width:120px;text-align: left;margin-left: 5px;}
		.ret-detail .ste-con ul li .radio{margin-right:5px;vertical-align: text-bottom;}
		.ret-detail .ste-con ul li .checkbox{margin-right:5px;vertical-align: text-bottom;}
		.ret-detail .ste-con ul li .textarea{width: 200px;border: 1px solid #ccc;margin-left: 10px;padding:4px;}
		.ret-detail .ste-con ul li:last-child{border-bottom:none;}
		.alert li{
			line-height: 30px;
		}
	</style>
  </head>
<body class="graybody">
	   <div id="doc">
	   		 <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
	   		 <div class="w1190">
				<div class="seller-cru" >
					<p>
						您的位置> <a href="${contextPath}/home">首页</a>> 
						<a	href="${contextPath}/sellerHome">卖家中心</a>> 
						<a  href="${contextPath}/s/refundReturnList/1/">退款及退货</a>>
						<span class="on">退货详情</span>
					</p>
				</div>
       	 	 	<%@ include file="../included/sellerLeft.jsp" %>
		
			    <!-----------right_con-------------->
			     <div class="right_con">
		     		<div class="o-mt" style="margin-top:20px;"><h2>退货详情</h2></div>
			        <div class="alert">
			          <h4>提示：</h4>
			          <ul>
			            <li>1. 若提出申请后，商家拒绝退款或退货，可再次提交申请或选择<em>“商品投诉”</em>，请求商城客服人员介入。</li>
						<li>2. 成功完成退款/退货；经过商城审核后，会将原路退回</li>
			          </ul>
			        </div>
			        <div class="ret-step-com">
			          <ul>
			            <li <c:if test="${not empty subRefundReturn.applyTime}">class="done"</c:if>><span>买家申请退货</span></li>
			            <li <c:if test="${not empty subRefundReturn.sellerTime}">class="done"</c:if>><span>商家处理退货申请</span></li>
			            <li <c:if test="${not empty subRefundReturn.shipTime}">class="done"</c:if>><span>买家退货给商家</span></li>
			            <li <c:if test="${not empty subRefundReturn.receiveTime}">class="done"</c:if>><span>确认收货，平台审核</span>
			          </ul>
			        </div>
			        <div class="ret-detail">
			         <div class="ste-tit"><b>买家的退货退款申请</b></div>
			          <div class="ste-con">
			            <ul>
			              <li><span class="span-item">用户名：</span>${subRefundReturn.userName}</li>
			              <li><span class="span-item">退货退款编号：</span>${subRefundReturn.refundSn}</li>
		              	  <li><span class="span-item">申请时间：</span><fmt:formatDate value="${subRefundReturn.applyTime}" pattern="yyyy-MM-dd HH:mm"/> </li>
			              <li><span class="span-item">订单编号：</span>${subRefundReturn.subNumber} </li>
			              <c:if test="${not empty subRefundReturn.outRefundNo}">
			               <li><span class="span-item">第三方支付流水号：</span>${subRefundReturn.outRefundNo}</li>
			               </c:if>
			              <li>
			              	<span class="span-item">涉及商品：</span>
						    <a href="${contextPath}/views/${subRefundReturn.productId}">${subRefundReturn.productName }</a>
			              </li>
			              <li>
			              	<span class="span-item">退款金额：</span>
			              	<c:choose>
			              		<c:when test="${subRefundReturn.isPresell && subRefundReturn.isRefundDeposit}">
			              			<c:if test="${empty subRefundReturn.refundAmount }">
			              				￥${subRefundReturn.depositRefund} 元
			              			</c:if>
			              			<c:if test="${not empty subRefundReturn.refundAmount }">
			              				￥${subRefundReturn.depositRefund + subRefundReturn.refundAmount} 元 （定金：${subRefundReturn.depositRefund} + 尾款：${subRefundReturn.refundAmount}）
			              			</c:if>
			              		</c:when>
			              		<c:when test="${subRefundReturn.isPresell && !subRefundReturn.isRefundDeposit}">
			              			￥${subRefundReturn.refundAmount} 元
			              		</c:when>
			              		<c:otherwise>
			              			￥${subRefundReturn.refundAmount} 元
			              		</c:otherwise>
			              	</c:choose>
			              </li>
			              <li><span class="span-item">退货数量：</span>x ${subRefundReturn.goodsNum}</li>
			              <li><span class="span-item">退货退款原因：</span>${subRefundReturn.buyerMessage} </li>
			              <li><span class="span-item">退款说明：</span>${subRefundReturn.reasonInfo}</li>
			              <c:if test="${not empty subRefundReturn.photoFile1}">
			              <li style="border: 0;">
			              	<span class="span-item">凭证上传1：</span>
			              	 <a href="<ls:photo item='${subRefundReturn.photoFile1}'/>" target="_blank">
			                    	<img src="<ls:images item='${subRefundReturn.photoFile1}' scale='2'/>"/>
			              	</a>
			              </li>
			              </c:if>
			              <c:if test="${not empty subRefundReturn.photoFile2}">
			              	  <li style="border: 0;">
				              	<span class="span-item">凭证上传2：</span>
				              	 <a href="<ls:photo item='${subRefundReturn.photoFile2}'/>" target="_blank">
				              	     <img src="<ls:images item='${subRefundReturn.photoFile2}' scale='2'/>"/>
				              	 </a>
				              </li>
			              </c:if>
			               <c:if test="${not empty subRefundReturn.photoFile3}">
				              <li style="border: 0;">
				              <span class="span-item">凭证上传3：</span>
				                 <a href="<ls:photo item='${subRefundReturn.photoFile3}'/>" target="_blank">
				               	     <img src="<ls:images item='${subRefundReturn.photoFile3}' scale='2' />"/>
				              	 </a>
				              </li>
			              </c:if>
			            </ul>
			          </div>
			          <div class="ste-tit"><b>我的处理意见</b></div>
				        <div class="ste-con">
				          <c:choose>
				          	<c:when test="${subRefundReturn.sellerState eq 1 && subRefundReturn.applyState eq 1}">
				          		<input type="hidden" id="refundId" name="refundId" value="${subRefundReturn.refundId }"/>
								<ul>
					              <li>
				              	  	<span class="span-item"><em class="required">*</em>是否同意：</span>
				              	  	<label class="radio-wrapper">
										<span class="radio-item">
											<input type="radio" id="agree" name="isAgree" value="1" class="radio-input"/>
											<span class="radio-inner"></span>
										</span>
										<span class="radio-txt">同意</span>
										<input type="checkbox" id=isJettison name="isJettison" value="1" class="checkbox" style="margin-left:20px;" disabled="disabled"/>弃货
									</label>
			              	  		<span class="tips">如果选择弃货买家将不用退回原商品,提交后直接由管理员退款!</span>
			              	  		<span class="span-item"></span>
			              	  		<label class="radio-wrapper">
										<span class="radio-item">
											<input type="radio" id="disagree" name="isAgree" value="0" class="radio-input" style="margin-left:20px;"/>
											<span class="radio-inner"></span>
										</span>
										<span class="radio-txt">不同意</span>
									</label>
					              </li>
								  <li class="is-hide">
									  <span class="span-item"><em class="required">*</em>退货地址：</span>
									  <label class="radio-wrapper">
										  <span class="radio-item">
											  <select class="combox " style="width:130px;" id="returnProvinceId" name="returnProvinceId" requiredTitle="true" childNode="returnCityId" selectedValue="${subRefundReturn.returnProvinceId}" retUrl="${contextPath}/common/loadProvinces"></select>
											  <select class="combox " style="width:116px;" id="returnCityId" name="returnCityId" requiredTitle="true" selectedValue="${subRefundReturn.returnCityId}" showNone="false" parentValue="${subRefundReturn.returnProvinceId}" childNode="returnAreaId" retUrl="${contextPath}/common/loadCities/{value}"></select>
											  <select class="combox "  id="returnAreaId" name="returnAreaId" requiredTitle="true" selectedValue="${subRefundReturn.returnAreaId}" showNone="false" parentValue="${subRefundReturn.returnCityId}" retUrl="${contextPath}/common/loadAreas/{value}"></select>
										  </span>
									  </label>
									  <span class="tips">可以前往“店铺设置”->“基本信息”里设置默认的退货地址哦！</span>
								  </li>
								  <li class="is-hide">
									  <span class="span-item"><em class="required">*</em>退货详细地址：</span>
									  <label class="radio-wrapper">
										  <input type="text" style="width:337px;" name="returnShopAddr" id="returnShopAddr" value="${subRefundReturn.returnShopAddr}" maxlength="300"/>
									  </label>
								  </li>
								  <li class="is-hide">
									  <span class="span-item"><em class="required">*</em>退货联系人：</span>
									  <label class="radio-wrapper">
										  <input type="text" style="width:337px;" name="returnContact" id="returnContact" value="${subRefundReturn.returnContact}" maxlength="20"/>
									  </label>
								  </li>
								  <li class="is-hide">
									  <span class="span-item"><em class="required">*</em>退货联系电话：</span>
									  <label class="radio-wrapper">
										  <input type="text" style="width:337px;" name="returnPhone" id="returnPhone" value="${subRefundReturn.returnPhone}" onkeyup="this.value=this.value.replace(/\D/g,'')"  oninput="if(value.length>11)value=value.slice(0,11)" maxlength="11"/>
									  </label>
								  </li>
					              <li style="border: 0;">
									<span class="span-item" style="float:left;"><em class="required">*</em>备注信息：</span>
					                <textarea id="sellerMessage" name="sellerMessage" rows="3" class="textarea" maxlength="150"></textarea>
					                <span class="tips">如果同意退款请及时关注买家的发货情况,并进行收货(发货5天后可以选择未收到,超过7天不处理按弃货处理)</span>
					              </li>
					            </ul>
				          	</c:when>
				          	<c:when test="${subRefundReturn.sellerState eq 1 && subRefundReturn.applyState eq -1}">
								<ul>
					              <li>
				              	  	<span>退货申请已撤销</span>
					              </li>
					            </ul>
				          	</c:when>
				          	<c:otherwise>
				          		<ul>
					              <li>
				              	  	<span class="span-item">审核状态：</span>
			              	  		<c:if test="${subRefundReturn.sellerState eq 1}">待审核</c:if>
					              	<c:if test="${subRefundReturn.sellerState eq 2}">同意</c:if>
					              	<c:if test="${subRefundReturn.sellerState eq 3}">不同意</c:if>
					              </li>
								  <c:if test="${subRefundReturn.sellerState eq 2}">
									  <c:if test="${not empty subRefundReturn.returnDetailAddress}">
										  <li>
											  <span class="span-item">退货详细地址：</span>
												  ${subRefundReturn.returnDetailAddress}
										  </li>
									  </c:if>
									  <c:if test="${not empty subRefundReturn.returnContact}">
										  <li>
											  <span class="span-item">退货联系人：</span>
												  ${subRefundReturn.returnContact}
										  </li>
									  </c:if>
									  <c:if test="${not empty subRefundReturn.returnPhone}">
										  <li>
											  <span class="span-item">退货联系电话：</span>
												  ${subRefundReturn.returnPhone}
										  </li>
									  </c:if>
								  </c:if>
					              <li style="border: 0;">
									<span class="span-item">备注信息：</span>${subRefundReturn.sellerMessage}
					              </li>
					            </ul>
				          	</c:otherwise>
				          </c:choose>

			          <c:if test="${subRefundReturn.returnType eq 2 and subRefundReturn.goodsState ne 1}">
				          <div class="ste-tit"><b>买家的退货发货信息</b></div>
				          <div class="ste-con">
				            <ul>
				              <li><span class="span-item">物流公司：</span>${subRefundReturn.expressName}</li>
				              <li style="border: 0;"><span>物流单号：</span>${subRefundReturn.expressNo}</li>
				            </ul>
				          </div>
					   </c:if>		          		
				          <c:if test="${subRefundReturn.sellerState eq 2 and subRefundReturn.returnType eq 2 and subRefundReturn.goodsState eq 2}">
					          <div class="ste-tit"><b>买家的退货发货信息</b></div>
					          <div class="ste-con">
					            <ul>
					              <li><span class="span-item">物流公司：</span>${subRefundReturn.expressName}</li>
					              <li style="border: 0;"><span class="span-item">物流单号：</span>${subRefundReturn.expressNo}</li>
					            </ul>
					          </div>
				          </c:if>
				        <!-- 商家收货信息 -->
				          <c:if test="${subRefundReturn.sellerState eq 2 and subRefundReturn.returnType eq 2 and subRefundReturn.goodsState eq 3}">
					          <div class="ste-tit"><b>买家的退货发货信息</b></div>
					          <div class="ste-con">
					            <ul>
					              <li><span class="span-item">物流状态：</span>未收货</li>
					            </ul>
					          </div>
				          </c:if>
				           <c:if test="${subRefundReturn.sellerState eq 2 and subRefundReturn.returnType eq 2 and subRefundReturn.goodsState eq 4}">
					          <div class="ste-tit"><b>商家收货信息</b></div>
					          <div class="ste-con">
					            <ul>
					                  <li><span class="span-item">物流状态：</span>已收货</li>
					                  <li style="border: 0;"><span class="span-item">收货备注：</span>${subRefundReturn.receiveMessage}</li>		              
					            </ul>
					          </div>
				          </c:if>
				        <!-- 商家收货信息 -->
				          <c:if test="${subRefundReturn.sellerState eq 2}">
					          <c:if test="${subRefundReturn.returnType eq 1 or (subRefundReturn.returnType eq 2 and subRefundReturn.goodsState eq 4)}">
					          	<div class="ste-tit"><b>商城退款审核</b></div>
						          <div class="ste-con">
						            <ul>
						              <li>
						              	<span class="span-item">平台确认：</span>
						              	<c:if test="${subRefundReturn.applyState eq 2}">
						              		待平台确认
						              	</c:if>
						              	<c:if test="${subRefundReturn.applyState eq 3}">
						              		已完成
						              	</c:if>
						              </li>
						              <li style="border: 0;"><span class="span-item">平台备注：</span>${subRefundReturn.adminMessage}</li>
						            </ul>
						          </div>
					          </c:if>
				          </c:if>
				          <c:if test="${subRefundReturn.applyState eq 3}">
					          <div class="ste-tit"><b>退款详细</b></div>
					          <div class="ste-con">
					            <ul>
					              <li><span class="span-item">支付方式：</span>${subRefundReturn.payTypeName} </li>
					              <li style="border-bottom: none;"><span class="span-item">订单总额：</span>${subRefundReturn.subMoney} </li>
					            </ul>
					          </div>
				          </c:if>
				        </div>
				          <div class="ste-but" align="center">
					          <c:if test="${subRefundReturn.sellerState eq 1 && subRefundReturn.applyState eq 1}">
						           	<input type="button" value="确定" class="btn-r big-btn" onclick="auditReturn();"/>
					           </c:if>
				           <input type="button" value="返回" class="btn-g big-btn" onclick="window.history.go(-1)"/>
				         </div>
				         <!-- 退货详情 -->
				 </div>
			 </div>
	   </div>
			     <!-----------right_con 结束-------------->
			     
			    <div class="clear"></div>
		 	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
		</div>
	    <script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
		<script type="text/javascript">
			var contextPath = '${contextPath}';
			$(function(){
				//三级联动
				$("select.combox").initSelect();
				userCenter.changeSubTab("myreturnorder");//高亮菜单
				
				$("#agree").click(function(){
					$("#agree").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper radio-wrapper-checked");
					$("#disagree").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper");
					$('.is-hide').show();
				});
				$("#disagree").click(function(){
					$("#disagree").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper radio-wrapper-checked");
					$("#agree").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper");
					$('.is-hide').hide();
				});

				$("#isJettison").click(function () {
					if ($('#isJettison').is(":checked")) {
						$('.is-hide').hide();
					} else {
						$('.is-hide').show();
					}
				})
			});
			
			//审核用户申请退货
			function auditReturn(){
				var refundId = $("#refundId").val();
				var isAgree = $("input[name='isAgree']:checked").val();
				var isJettison = $("input[name='isJettison']:checked").val()?1:0;
				var sellerMessage = $("#sellerMessage").val();
				var returnProvinceId = $("#returnProvinceId").val();
				var returnCityId = $("#returnCityId").val();
				var returnAreaId = $("#returnAreaId").val();
				var returnShopAddr = $("#returnShopAddr").val();
				var returnContact = $('#returnContact').val();
				var returnPhone = $('#returnPhone').val();
				// console.log("isAgree:"+isAgree +", isJettison:" + isJettison +", sellerMessage:" + sellerMessage);
				if(isBlank(refundId) || isBlank(isAgree) || isBlank(sellerMessage)){
					layer.msg('对不起,请您完善好审核信息信息后再确认!', {icon: 0});
					return;
				}
				var param = {
					"isAgree":isAgree,
					"isJettison":isJettison,
					"sellerMessage":sellerMessage
				};
				if (parseInt(isJettison) === 0 && parseInt(isAgree) === 1) {
					if (isBlank(returnProvinceId) || isBlank(returnCityId) || isBlank(returnAreaId) || isBlank(returnShopAddr) || isBlank(returnContact) || isBlank(returnPhone)) {
						console.log("123");
						layer.msg('对不起,请您完善好审核信息信息后再确认!', {icon: 0});
						return;
					}
					param.returnProvinceId = parseInt(returnProvinceId);
					param.returnCityId = parseInt(returnCityId);
					param.returnAreaId = parseInt(returnAreaId);
					param.returnShopAddr = returnShopAddr;
					param.returnContact = returnContact;
					param.returnPhone = returnPhone;
					param.returnDetailAddress = $("#returnProvinceId option:selected").text() + " " + $("#returnCityId option:selected").text() + " " + $("#returnAreaId option:selected").text() + " " + returnShopAddr;
				}
				console.log(param);
				var url = contextPath + "/s/auditItemReturn/" + refundId;
				sendReq(url,param);
			}
			
			//确认退货
			function confirmReturn(){
				var refundId = $("#refundId").val();
				var expressName = $("#expressName").val();
				var expressNo = $("#expressNo").val();
				
				if(isBlank(refundId) || isBlank(expressName) || isBlank(expressNo)){
					
					layer.alert('对不起,请您完善好物流信息后再确认!', {icon: 0});
					return;
				}
				var url = contextPath + "/p/returnGoods";
				var param = {"refundId":refundId,"expressName":expressName,"expressNo":expressNo};

				sendReq(url,param);
			}
			
			//发送请求
			function sendReq(url,param){
				$.ajax({
					url : url,
					type : "POST",
					data : param,
					dataType : "JSON",
					async : true,
					success: function(result,status,xhr){
						if(result == "OK"){
							layer.alert("恭喜你,操作成功!",{icon: 1},function(){
								window.location.href = contextPath + "/s/refundReturnList/" + '${subRefundReturn.applyType }';
							});
						}else{
							layer.alert(result,{icon: 2});
						}
					}
				});
			}
			
			$("input[name='isAgree']").on("click", function(){
				var result = $(this).val();
				//var result = $(".radio").val();
				if(result==1){
					$("#isJettison").removeAttr("disabled");
				}else{
					$("#isJettison").removeAttr("checked");
					$("#isJettison").attr("disabled", "disabled");
				}
			});
			
			function isBlank(value){
				return value == undefined || value == null || $.trim(value) === "";
			}
		</script>
	</body>
</html>
