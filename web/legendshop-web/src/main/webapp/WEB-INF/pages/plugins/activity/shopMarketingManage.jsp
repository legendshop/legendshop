<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>促销活动-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/active/actives${_style_}.css'/>"
          rel="stylesheet">
</head>
<body class="graybody">
<div id="doc">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
    <div class="w1190">
        <div class="seller-cru">
            <p>
                您的位置>
                <a href="${contextPath}/home">首页</a>>
                <a href="${contextPath}/sellerHome">卖家中心</a>>
                <span class="on">促销活动</span>
            </p>
        </div>

        <%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>


        <div id="rightContent" class="right_con">
            <div class="o-mt">
                <h2>促销活动管理</h2>
            </div>
            <table class="ncsc-default-table">
                <tbody>
                <tr>
                    <td class="w90 tr"><strong>名称：</strong></td>
                    <td class="w120 tl">${marketing.marketName}</td>
                    <td class="w90 tr"><strong>开始时间：</strong></td>
                    <td class="w120 tl"><fmt:formatDate value="${marketing.startTime}"
                                                        pattern="yyyy-MM-dd HH:mm:ss"/></td>
                    <td class="w90 tr"><strong>结束时间：</strong></td>
                    <td class="w120 tl"><fmt:formatDate value="${marketing.endTime}"
                                                        pattern="yyyy-MM-dd HH:mm:ss"/></td>
                    <td class="w90 tr"><strong>活动商品：</strong></td>
                    <td class="w90 tr">
                        <c:choose>
                            <c:when test="${marketing.isAllProds==1}">
                                全店商品
                            </c:when>
                            <c:when test="${marketing.isAllProds==0}">
                                部分商品
                            </c:when>
                        </c:choose>
                    </td>
                    <td class="w90 tr"><strong>规则：</strong></td>
                    <c:choose>
                        <c:when test="${marketing.type==0}">
                            <td class=" tl" style="width: 360px;">
                                <ul class="ncsc-mansong-rule-list">
                                    <c:forEach items="${marketing.marketingMjRules}" var="mj" varStatus="status">
                                        <li>
                                            单笔满<c:choose>
                                            <c:when test="${mj.calType==1}">
                                                <strong><fmt:formatNumber value="${mj.fullAmount}"
                                                                          pattern="#"/></strong>件，&nbsp;
                                            </c:when>
                                            <c:otherwise>
                                                <strong>${mj.fullAmount}</strong>元，&nbsp;
                                            </c:otherwise>
                                        </c:choose>

                                            立减现金<strong>${mj.offAmount}</strong>元
                                        </li>
                                    </c:forEach>
                                </ul>
                            </td>
                        </c:when>
                        <c:when test="${marketing.type==1}">
                            <td class=" tl" style="width: 360px;">
                                <ul class="ncsc-mansong-rule-list">
                                    <c:forEach items="${marketing.marketingMzRules}" var="mz" varStatus="status">
                                        <li>
                                            单笔满<c:choose>
                                            <c:when test="${mz.calType==1}">
                                                <strong><fmt:formatNumber value="${mz.fullAmount}"
                                                                          pattern="#"/></strong>件，&nbsp;
                                            </c:when>
                                            <c:otherwise>
                                                <strong>${mz.fullAmount}</strong>元，&nbsp;
                                            </c:otherwise>
                                        </c:choose>
                                            打<strong>${mz.offDiscount}</strong>折
                                        </li>
                                    </c:forEach>
                                </ul>
                            </td>
                        </c:when>

                        <c:when test="${marketing.type==4}">
                            <td class=" tl" style="width: 360px;">
                                <ul class="ncsc-mansong-rule-list">
                                    <c:forEach items="${marketing.marketingMjFulllRule}" var="mj" varStatus="status">
                                        <li>
                                            单笔满<strong>${mj.fullAmount}</strong>件，&nbsp;
                                            <strong>包邮</strong>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </td>
                        </c:when>
                        <c:when test="${marketing.type==5}">
                            <td class=" tl" style="width: 360px;">
                                <ul class="ncsc-mansong-rule-list">
                                    <c:forEach items="${marketing.marketingMyRule}" var="my" varStatus="status">
                                        <li>
                                            单笔满<strong>${my.fullAmount}</strong>元，&nbsp;
                                            <strong>包邮</strong>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </td>
                        </c:when>
                    </c:choose>
                </tr>
                </tbody>
            </table>
            <c:if test="${marketing.isAllProds==0}">
                <c:if test="${marketing.state==0}">
                    <a href="javascript:void(0);" style="float: right;margin-bottom: 10px;" id="btn_show_goods_select"
                       class="btn-r big-btn">添加商品</a>
                </c:if>

                <div style="display: none;" class="div-goods-select" id="div_goods_select">
                    <table class="search-form">
                        <tbody>
                        <tr>
                            <th class="w150"><strong>第一步：搜索店内商品</strong>
                            </th>
                            <td class="w160"><input type="text w150" value=""
                                                    name="goods_name" class="text" id="search_goods_name">
                            </td>
                            <td class="w70 tc">
                                <a class="btn-r small-btn" id="btn_search_goods" href="javascript:void(0);">搜索</a>
                            </td>
                            <td class="w10"></td>
                            <td><p class="hint">不输入名称直接搜索将显示店内所有普通商品，特殊商品不能参加。</p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="search-result" id="div_goods_search_result"></div>
                    <a href="javascript:void(0);" class="close" id="btn_hide_goods_select">X</a>
                </div>

                <div id="prodContent">

                </div>


                <div class="alert">
                    <strong>说明：</strong>
                    <ul>
                        <li>1、满促商品的时间段不能重叠</li>
                        <li>2、点击添加商品按钮可以搜索并添加参加活动的商品，点击删除按钮可以删除该商品</li>
                        <li>3、当移除所有的商品信息后,该活动会自动下线</li>
                    </ul>
                </div>
            </c:if>
            <a class="btn-r big-btn" href="/s/shopSingleMarketing">返回列表</a>
        </div>


    </div>
    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<script type="text/javascript"
        src="<ls:templateResource item='/resources/templets/js/actives/shopMarketingManage.js'/>"></script>
<script type="text/javascript">
    var contextPath = '${contextPath}';
    var marketId = '${marketing.id}';
    var state = '${marketing.state}';
</script>
</body>
</html>
