<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@page import="com.legendshop.config.PropertiesUtilManager"%>
<%@page import="com.legendshop.base.constants.SysParameterEnum"%>
  <div class="item fore1">
                <span>邮箱/用户名/已验证手机</span>
                <div class="item-ifo">
                    <input id="username" name="username" value='<lb:lastLogingUser/>' tabindex="1" class="text" type="text" maxlength="20" onfocus="userNameJudge();" onblur="userNameLeave();">
                    <i class="i-name" style="left: 245px;"></i>
                    <label id="loginname_succeed" class="blank invisible"></label>
                    <label id="loginname_error" class="hide" style="line-height: 16px;"></label>
                </div>
            </div>
            <div class="item fore2">
                <span>密码</span>
                <div class="item-ifo">

                    <input id="pwd" name="password" class="text" tabindex="2" autocomplete="off" type="password" maxlength="20" onfocus="pwdJudge();" onblur="pwdLeave();">
   
                    <label id="loginpwd_succeed" class="blank invisible"></label>

                    <label id="loginpwd_error" class="hide" style="line-height: 16px;"></label>
                </div>
            </div>
            
                <div class="item fore4" id="authcodeDiv">
                    <span>验证码</span>

                    <div class="item-ifo">
                        <input type="hidden" id="cannonull" name="cannonull" value='<fmt:message key="randomimage.errors.required"/>'/>
						<input type="hidden" id="charactors4" name="charactors4" value='<ls:i18n key="randomimage.charactors.required" length="4"/>'/> 
						<input type="hidden" id="errorImage" name="errorImage" value='<fmt:message key="error.image.validation"/>'/> 
            				
                        <input onfocus="randnumJudge();" id="randNum" class="text text-1" name="randNum" tabindex="6" autocomplete="off" type="text">
            			<span><img id="randImage" name="randImage" src="<ls:templateResource item='/validCoderRandom'/>" style="vertical-align: middle; margin-top: -3px;"/>看不清？
            			<a href="javascript:void(0)" onclick="javascript:changeRandImg('${contextPath}')" style="font-weight: bold;"><fmt:message key="change.random2"/></a>
            			</span>

                        <div class="clr"></div>
                        <label id="authcode_error" class="hide"></label>
                    </div>
                </div>
                
            <div id="autoentry" class="item fore4">
                <span class="label">&nbsp;</span>

                <div class="item-ifo">
                    <input class="checkbox" name="chkRememberMe" type="checkbox">
                    <label class="mar">自动登录</label>
                    <label><a href='<ls:url address="/retrievepassword"/>' target="_blank">忘记密码?</a></label>
                </div>
            </div>
            <div class="item">
                <input class="btn-img btn-login" id="loginsubmitframe" value="登&nbsp;录" tabindex="8" type="button" onclick="submitLoginOverlay();">
            </div>
                        <%-- <div class="item extra">
                使用合作网站账号登录：
                <div class="clr"></div>
                <span ><s></s><a href="javascript:void(0)" onclick="parent.window.location='https://graph.qq.com/oauth2.0/authorize?client_id=<%=PropertiesUtilManager.getPropertiesUtil().getObject(SysParameterEnum.QQ_APP_ID, String.class)%>&amp;response_type=token&amp;scope=all&amp;redirect_uri=<%=PropertiesUtilManager.getPropertiesUtil().getObject(SysParameterEnum.QQ_REDIRECT_URL, String.class)%>'"><img src="${contextPath}/resources/templets/images/qq_onnect_logo_7.png" width="63" height="24"  alt=""/></a></span>
                <span ><s></s><a href="javascript:void(0)" onclick="parent.window.location='<%=oauth.authorize("code","","") %>'"><img src="${contextPath}/resources/templets/images/loginButton_24.png" width="102" height="24"  alt=""/></a></span>
            </div> --%>
            
<script type="text/javascript">

	//焦点事件
	$(document).ready(function() {
		var userName = $("#username").val();
		if(userName.length ==0){
			$("#username").focus();
		}else{
			$("#pwd").focus();
		}
	});
</script>