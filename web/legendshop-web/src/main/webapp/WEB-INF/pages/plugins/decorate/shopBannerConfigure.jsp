<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>商家店铺轮播图片列表-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-nav.css'/>" rel="stylesheet">
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">增加店铺轮播图片</span>
				</p>
			</div>
		<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
			<div class="seller-brand">
				<div class="seller-com-nav" >
					<ul>
						<li ><a href='${contextPath}/s/shopBanner/list'>店铺轮播图片列表</a></li>
						<li class="on"><a href='javascript:void(0);'>新增轮播图片</a></li>
					</ul>
				</div>
				 <form:form  action="${contextPath}/s/shopBanner/save" method="post" id="form1" enctype="multipart/form-data">
						<input id="id" name="id" value="${bean.id}" type="hidden">
						<div class="brand-add">
							<ul>
								<li class="file"><em style="color: #e5004f;margin-right: 5px;">*</em>品牌大图：<input id="imageFile" name="file" type="file"/>
								   <c:if test="${not empty  bean.imageFile}">
		                			 <img src="<ls:photo item='${bean.imageFile}'/>" style="max-width: 800px;"/>
		                		   </c:if>
		                		    <c:if test="${ empty bean.imageFile}">
		                                                                                最佳尺寸为：1920px * 500px;
		                            </c:if>
								</li>
								<li class="textarea">
									<em style="color: #e5004f;margin-right: 5px;">*</em>URL：<textarea name="url" cols="60" maxlength="200" style="padding:5px;margin:1px;" rows="3" id="url" >${bean.url}</textarea>
								</li>
								<li><em style="color: #e5004f;margin-right: 5px;">*</em>序号：<input type="text" name="seq" id="seq" value="${bean.seq}" style="width:359px;"  maxlength="5" size="5"/>
								</li>
								
							</ul>
							<div class="bot-save" style="margin-bottom: 10px;">
							   <input type="submit" style="float: left;" value="提交申请" class="btn-r big-btn"/>
							   <input type="button" value="返回" onclick="location.href='${contextPath}/s/shopBanner/list'" class="btn-g big-btn"/>
							</div>
					</div>
				</form:form>
			</div>
		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script src="${contextPath}/resources/common/js/jquery.validate1.5.5.js" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
	<script type="text/javascript">
	 $(document).ready(function(e) {
				userCenter.changeSubTab("shopBanner"); //高亮菜单
			    <c:choose>
				     <c:when test="${not empty bean.id}">
				         jQuery("#form1").validate({
							rules: {
							    file:{
									 accept: "jpg,png,jpeg"
								},
								url:{
									"required":true,
									url:true
								},
								seq:{
								  "required":true,
								   digits:true 
								}
							},
							messages: {
							    file:{
									accept:"只能上传jpeg, png, jpg格式的图片"
								},
								url:{
									"required":"请输入URL",
									url:"请输入正确的url"
								},
								seq:{
								  "required":"请输入排序",
								   digits:"请输入整数"
								}
							}
		     			});
						     
				     </c:when>
				     <c:otherwise>
				     jQuery("#form1").validate({
								rules: {
									file:{
										"required":true,
										 accept: "jpg,png,jpeg"
									},
									url:{
										"required":true,
										url:true
									},
									seq:{
									  "required":true,
									   digits:true 
									}
									
								},
								messages: {
									file:{
										required:"请上传图片",
										accept:"只能上传jpeg, png, jpg格式的图片"
									},
									url:{
										"required":"请输入URL",
										url:"请输入正确的url"
									},
									seq:{
									  "required":"请输入排序",
									   digits:"请输入整数"
									}
								}
			     			});
				     </c:otherwise>
				   </c:choose>
		});
		
		$("input[name=file]").change(function(){
 			checkImgType(this);
			checkImgSize(this,1024);
 		});
    </script>
</body>
</html>