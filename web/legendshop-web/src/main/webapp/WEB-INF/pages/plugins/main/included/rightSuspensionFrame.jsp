<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/rightSuspension.css'/>" rel="stylesheet"/>
<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
<div class="ls-toolbar J-toolbar">
<div class="ls-toolbar-wrap">
     <div class="ls-toolbar-tabs">
        <div class="J-trigger ls-toolbar-tab ls-tbar-tab-cart">
        	 <a href="<ls:url address='/shopCart/shopBuy'/>" target="_blank">
           <i class="tab-ico"></i><em class="tab-text">购物车</em>
           </a>
        </div>
        <div class="J-trigger ls-toolbar-tab ls-tbar-tab-follow">
        <a href="<ls:url address='/p/favourite'/>" target="_blank">
           <i class="tab-ico"></i><em class="tab-text">我的收藏</em>
           </a>
        </div>
        <div class="J-trigger ls-toolbar-tab ls-tbar-tab-history">
          <a href="<ls:url address='/p/inbox'/>" target="_blank">
           <i class="tab-ico"></i><em class="tab-text">我的消息</em>
           </a>
        </div>
        <div class="J-trigger ls-toolbar-tab ls-tbar-tab-jimi">
          		<a href="<ls:url address='/p/myorder?uc=uc'/>" target="_blank">
           <i class="tab-ico"></i><em class="tab-text">我的订单</em>
           </a>
        </div>
        <div class="J-trigger ls-toolbar-tab ls-tbar-tab-jimi">
          		<a href="${contextPath}/p/im/customerAdmin/0" id="huanXinImg" target="_blank">
           			<i class="tab-ico" style="background-position: -190px -150px;"></i><em class="tab-text">我的客服</em>
           		</a>
        </div>
     </div>
     <div class="ls-toolbar-footer"> 
	     <div data-type="link" class="J-trigger ls-toolbar-tab ls-tbar-tab-top" id="top">
	    	 <a href="javascript:void(0);" onclick="gotop()">
	    	 	<i class="tab-ico"></i><em class="tab-text">返回顶部</em>
	    	 </a>
	     </div>
	     
	     <div data-type="link" class="J-trigger ls-toolbar-tab ls-tbar-tab-feedback">
	     	<a target="_blank" onclick="feedback()">    
	     	    <i class="tab-ico"></i><em class="tab-text">意见反馈</em>
	     </a>
	     </div>
     </div>
</div>
</div>
<script type="text/javascript" src="${contextPath}/resources/templets/js/feedback.js"></script>
