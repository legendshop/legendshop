<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss"
	var="nowDate" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<title>拍卖活动管理-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css"
	href="<ls:templateResource item='/resources/templets/css/active/actives${_style_}.css'/>"
	rel="stylesheet">
<link type="text/css"
	href="<ls:templateResource item='/resources/templets/css/base.css' /> rel="stylesheet">
<link type="text/css"
	href="<ls:templateResource item='/resources/templets/css/bidding-activity${_style_}.css'/>"
	rel="stylesheet">

</head>
<body class="graybody">
	<div id="doc">
		<%@ include
			file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置> <a href="${contextPath}/home">首页</a>> <a
						href="${contextPath}/sellerHome">卖家中心</a>> <span class="on">拍卖活动管理</span>
				</p>
			</div>

			<%@ include
				file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp"%>


			<div id="rightContent" class="right_con">
				<!--page-->
				<!-- 添加/搜索 -->
				<div id="statusTab" class="bidding-nav" style="margin-bottom: 0px;margin-top: 20px" >
						<span <c:if test="${empty bean.searchType}">class="on"</c:if> ><a
								href="<ls:url address="/s/auction/query"/>">全部活动</a></span>
					<span <c:if test="${bean.searchType eq 'WAIT_AUDIT'}">class="on"</c:if>><a
							href="<ls:url address="/s/auction/query?searchType=WAIT_AUDIT"/>">待审核</a></span>
					<span <c:if test="${bean.searchType eq 'NOT_PASS'}">class="on"</c:if> ><a
							href="<ls:url address="/s/auction/query?searchType=NOT_PASS"/>">未通过</a></span>
					<span <c:if test="${bean.searchType eq 'NOT_STARTED'}">class="on"</c:if> ><a
							href="<ls:url address="/s/auction/query?searchType=NOT_STARTED"/>">未开始</a></span>
					<span <c:if test="${bean.searchType eq 'ONLINE'}">class="on"</c:if>><a
							href="<ls:url address="/s/auction/query?searchType=ONLINE"/>">进行中</a></span>
					<span <c:if test="${bean.searchType eq 'FINISHED'}">class="on"</c:if> ><a
							href="<ls:url address="/s/auction/query?searchType=FINISHED"/>">已结束</a></span>
					<span <c:if test="${bean.searchType eq 'EXPIRED'}">class="on"</c:if>><a
							href="<ls:url address="/s/auction/query?searchType=EXPIRED"/>">已失效</a></span>
				</div>

				<!-- 拍卖活动列表 -->
				<div class="bidding-activity" style="margin-top: 15px">
					<!-- 添加/搜索 -->
					<div class="search-add">
						<div class="set-up sold-ser sold-ser-no-bg clearfix" style="overflow:hidden">
							<div class="item">
								点击添加新的拍卖活动：<input class="btn-r big-btn" style="display:inline-block;" type="submit" value="立即添加"
									onclick="window.location='${contextPath}/s/auction/addShopAuction'">
							</div>
							<form:form id="searchAuction" action="${contextPath}/s/auction/query" style="float:right;display:inline-block" method="get">
								<div class="fr">
									<div class="item">
										活动名称:
										<input type="text"	value="${auctions.auctionsTitle}" name="auctionsTitle"
											id="auctionsTitle" class="text item-inp" placeholder="请输入活动名称">
										<input type="hidden" class="text item-inp"
											value="${empty curPageNO?1:curPageNO}" name="curPageNO"
											id="curPageNO">
										<input type="hidden" value="${auctions.searchType}" name="searchType" id="searchType" />
										<input type="submit" value="搜索" class="submit btn-r" style="display:inline-block">
									</div>
								</div>
							</form:form>
						</div>
					</div>

					<table class="bidding-table sold-table">
						<tr class="bidding-tit">
							<th width="250">拍卖活动标题</th>
							<th width="150">活动时间</th>
							<th width="120">是否需要保证金</th>
							<th>起拍价(¥)</th>
							<th width="110">一口价(¥)</th>
							<th>活动状态</th>
							<th>操作</th>
						</tr>
						<tbody>
						 <c:choose>
				           <c:when test="${empty list}">
				              <tr>
							 	 <td colspan="20">
							 	   	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
							 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
							 	 </td>
							 </tr>
				           </c:when>
				           <c:otherwise>
				              <c:forEach items="${list}" var="auctions" varStatus="status">
								<tr class="detail" >
									<td>${auctions.auctionsTitle}</td>
									<fmt:formatDate value="${auctions.startTime}" var="startTime"
										pattern="yyyy-MM-dd HH:mm:ss" />
									<fmt:formatDate value="${auctions.endTime}" var="endTime"
									pattern="yyyy-MM-dd HH:mm:ss" />
									<td>${startTime}<span style="display: block">至</span>${endTime}</td>
									<td>${auctions.isSecurity==1?'需要':'不需要'}</td>
									<td>${auctions.floorPrice}</td>
									<td>${auctions.fixedPrice==null?'暂无':auctions.fixedPrice}</td>
									<td>
									<c:if test="${auctions.status==2}">
									  		 完成
									</c:if>
									<c:if test="${auctions.status!=2}">
										<c:choose>
											<c:when test="${auctions.status eq 2}">
												<span class="auctions-sta">已结束</span>
											</c:when>
											<c:when test="${empty auctions.prodStatus || auctions.prodStatus eq -1 || auctions.prodStatus eq -2}">
												<span class="auctions-sta">商品已删除</span>
											</c:when>
											<c:when test="${auctions.prodStatus eq 0}">
												<span class="auctions-sta">商品已下线</span>
											</c:when>
											<c:when test="${auctions.prodStatus eq 2}">
												<span class="auctions-sta">商品违规下线</span>
											</c:when>
											<c:when test="${auctions.prodStatus eq 3}">
												<span class="auctions-sta">商品待审核</span>
											</c:when>
											<c:when test="${auctions.prodStatus eq 4}">
												<span class="auctions-sta">商品审核失败</span>
											</c:when>
											<c:otherwise>
												<c:if test="${auctions.status eq -2}">
													<span class="auctions-sta">审核未通过</span>
													<span class="question-mark"></span>
													<input type="hidden" id="auditOpinion" value="${auctions.auditOpinion}">
													<span id="opinion" class="auctions-sta opinion" style="display:none;">
														<div id="history" class="more"></div>
													</span>
												</c:if>
												<c:if test="${auctions.status eq -1}">
													<span class="auctions-sta">审核中</span>
												</c:if>
												<c:if test="${auctions.status eq 0}">
													<span class="auctions-staed">已失效</span>
												</c:if>
												<c:if test="${auctions.status eq 1}">
													<c:choose>
														<c:when test="${auctions.startTime gt now}">
															<span class="auctions-sta">未开始</span>
														</c:when>
														<c:otherwise>
															<span class="auctions-staing">进行中</span>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:otherwise>
										</c:choose>
									</c:if>
									</td>
									<td>
											<%-- 已结束 或者商品状态异常 --%>
										<c:choose>
											<c:when test="${empty auctions.prodStatus || auctions.endTime < nowDate || auctions.prodStatus eq -1 || auctions.prodStatus eq -2 || auctions.prodStatus eq 2 }">
												<a href="${contextPath}/s/auction/shopAuctionDetail/${auctions.id}" class="btn-r">查看</a>
												<a href="${contextPath }/s/auction/shopAuctionOperation/${auctions.id}" class="btn-r">运营</a>

												<%-- 待审核 或  未通过 并且已结束  进行物理删除 --%>
												<c:if test="${auctions.status eq -1 || auctions.status eq -2}">
													<a href="javascript:void(0);" class="blue_button del btn-g" onclick="deleteShopAuction('${auctions.id}')">删除</a>
												</c:if>

												<%-- 进行中 或  已失效 并且已结束  进行逻辑删除 --%>
												<c:if test="${auctions.status eq 0 || auctions.status eq 1 || auctions.status eq 2}">
													<a href="javascript:void(0);" class="blue_button del btn-g" onclick="deleteShopAuction('${auctions.id}')">删除</a>
												</c:if>
											</c:when>
											<c:otherwise>

												<%-- 待审核 或  未通过 --%>
												<c:if test="${auctions.status eq -1 || auctions.status eq -2}">
													<a href="${contextPath}/s/auction/updateAuction/${auctions.id}" class="btn-g">编辑</a>
													<a href="javascript:void(0);" class="blue_button del btn-g" onclick="deleteShopAuction('${auctions.id}')">删除</a>
												</c:if>
												<%-- 待审核 或 未通过 --%>

												<%-- 未开始 --%>
												<c:if test="${auctions.status eq 1 && auctions.startTime > nowDate}">
													<a href="${contextPath}/s/auction/updateAuction/${auctions.id}" class="btn-g">编辑</a>
												</c:if>
												<%-- 未开始 --%>

												<%-- 进行中 --%>
												<c:if test="${auctions.status eq 1 && auctions.startTime < nowDate}">
													<a href="${contextPath}/s/auction/shopAuctionDetail/${auctions.id}" class="btn-r">查看</a>
													<a href="${contextPath }/s/auction/shopAuctionOperation/${auctions.id}" class="btn-r">运营</a>
													<%--<a href="javascript:void(0);" onclick="offlineShopAuction('${auctions.id}')" class=" btn-g">终止</a>--%>
												</c:if>
												<%-- 进行中 --%>

												<%-- 已失效 或已结束 --%>
												<c:if test="${auctions.status eq 0 || auctions.status eq 2}">
													<a href="${contextPath}/s/auction/shopAuctionDetail/${auctions.id}" class="btn-r">查看</a>
													<a href="${contextPath }/s/auction/shopAuctionOperation/${auctions.id}" class="btn-r">运营</a>
													<a href="javascript:void(0);" class="btn-g" onclick="deleteShopAuction('${auctions.id}')">删除</a>
												</c:if>
												<%-- 已失效  或已结束 --%>
											</c:otherwise>
										</c:choose>

									</td>
								</tr>
							</c:forEach>
				          </c:otherwise>
				         </c:choose>
						</tbody>
					</table>
				</div>
				<!-- 拍卖活动列表 end -->
				<!--page-->

				<div class="clear"></div>
				 <div class="page clearfix">
	     			 <div class="p-wrap">
	            		<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
	     			 </div>
		  		</div>
			</div>
		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<script src="${contextPath}/resources/templets/js/shopAuctionList.js"></script>
	<script type="text/javascript">
		var contextPath = '${contextPath}';
	</script>
<script src="<ls:templateResource item='/resources/templets/js/auction/auction.js'/>" type="text/javascript"></script>
</body>
</html>
