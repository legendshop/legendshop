<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<div class="confirm" id="confirm">
	<div class="price" style="margin-top: 20px;">
		<p>
			商品件数总计：<span id="allCount">${requestScope.userShopCartList.orderTotalQuanlity}</span>
		</p>
		<p>
			商品总价：<span id="allActualCash">￥<fmt:formatNumber value="${requestScope.userShopCartList.orderTotalCash}" type="currency" pattern="0.00"/></span>
		</p>
		<p>
			运费：<span id="allFreightAmount">￥<fmt:formatNumber value="${requestScope.userShopCartList.orderFreightAmount}" type="currency" pattern="0.00"/></span>
		</p>
		<p>
			促销优惠：<span id="allDiscount">-￥<fmt:formatNumber value="${requestScope.userShopCartList.allDiscount}" type="currency" pattern="0.00"/></span>
		</p>
		<p>
		   <fmt:formatNumber var="allCashFormat" value="${requestScope.userShopCartList.orderActualTotal}" type="currency" pattern="0.00" />
			订单支付总额：<b><span class="red" id="allCash" >￥${allCashFormat}</span></b>
		</p>
	</div>
	<c:if test="${payType eq 0}">
		<span id="confirm_totalPrice">应付金额：<b>￥<em id="allCashFormat">${allCashFormat}</em></b></span>
	</c:if>
	<c:if test="${payType eq 1}">
		<span id="confirm_totalPrice">应付定金金额：<b>￥<em id="preDepositPriceAmount">${preDepositPriceAmount}</em></b></span>
	</c:if>
    <button  class="on" id="submitOrder" onclick="submitOrder();">确认订单</button>
</div>