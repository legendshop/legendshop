<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!--当前点击的获取分类 -->
    <c:set var="currentNode" value="${indexApi:findTreeNode(param.categoryId)}"></c:set>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <c:choose>
        <c:when test="${not empty currentNode && not empty currentNode.obj}">
            <title>${empty currentNode.obj.title?currentNode.obj.name:currentNode.obj.title}-${systemConfig.shopName}</title>
            <meta name="keywords" content="${currentNode.obj.keyword}"/>
            <meta name="description" content="${currentNode.obj.catDesc}"/>
        </c:when>
        <c:otherwise>
            <title>${systemConfig.title}-${systemConfig.shopName}</title>
            <meta name="keywords" content="${systemConfig.keywords}"/>
            <meta name="description" content="${systemConfig.description}"/>
        </c:otherwise>
    </c:choose>
</head>

<body class="graybody" style="background: #ffffff;">
<div id="doc">
    <%@ include file="home/top.jsp" %>

    <div id="bd">

        <!-- 商品 属性 -->
        <div id="main-facet">            
        </div>

        <!-- 商品 列表-->
        <div id="main-nav-holder" style="height: 450px;">
        </div>
    </div>


    <form:form action="${contextPath}/prodParamList" method="post" id="list_param_form">
        <input type="hidden" name="categoryId" value="${searchParams.categoryId}"/>
        <input type="hidden" name="keyword" value="${searchParams.keyword}"/>
        <input type="hidden" name="prop" value="${searchParams.prop}"/>
        <input type="hidden" name="startPrice"  value="${searchParams.startPrice}" />
	    <input type="hidden" name="endPrice"  value="${searchParams.endPrice}" />
    </form:form>


    <form:form action="${contextPath}/prodlist" method="post" id="list_form">
        <input type="hidden" name="curPageNO" value="${_page}"/>
        <input type="hidden" name="categoryId" value="${searchParams.categoryId}"/>
        <input type="hidden" name="orders" value="${searchParams.orders}"/>
        <input type="hidden" name="hasProd" value="${searchParams.hasProd}"/>
        <input type="hidden" name="keyword" value="${searchParams.keyword}"/>
        <input type="hidden" name="prop" value="${searchParams.prop}"/>
        <input type="hidden" name="startPrice"  value="${searchParams.startPrice}" />
	    <input type="hidden" name="endPrice"  value="${searchParams.endPrice}" />
        <input type="hidden" name="tag" value="${searchParams.tag}"/>
    </form:form>

    <%@ include file="home/bottom.jsp" %>
</div>
</body>

<script type="text/javascript">
    var webPath = {
        webRoot: "${contextPath}",
        toatalPage: "<c:out value='${requestScope.pageCount}'/>",
        cookieNum: 0
    }

    jQuery(document).ready(function () {
        <ls:plugin pluginId='advanced_search'>
        ajaxFacetData();
        </ls:plugin>
        sendData();
    });
    var showUnSelections = function () {
        $(".unSelections").slideDown();
        $("#showUnSelections").hide();
        $("#hideUnSelections").show();
    };

    var hideUnSelections = function () {
        $(".unSelections:eq(3)").slideUp(200);
        $("#hideUnSelections").hide();
        $("#showUnSelections").show();
    };
</script>
<!--page end-->
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/map.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/productsort.js'/>"></script>

</html>
	    