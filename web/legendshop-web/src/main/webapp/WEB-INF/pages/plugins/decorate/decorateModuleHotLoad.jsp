<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<img border="0" src="<ls:photo item='${hots.imageFile}' />" usemap="#Map_<ls:photo item='${hots.imageFile}' />" onload="loadimg(this);">
<c:if test="${not empty hots.dataDtos}">
  <map  id="Map_<ls:photo item='${hots.imageFile}' />" name="Map_<ls:photo item='${hots.imageFile}' />">
     <c:forEach items="${hots.dataDtos}" var="data" varStatus="s">
         <area  shape="rect" coords="${data.coords}" href="${data.hotspotUrl}" target="_blank">
     </c:forEach>
  </map> 
</c:if>
<script type="text/javascript">
function loadimg(ths){
	if($(ths).parent().parent().hasClass("layout_one")){
	var h = $(ths).height();
	$(ths).parent().parent().css({"height":h+"px"});
	
	}
}

</script>
