<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>收件箱 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>
<style>
	.sold-table th {
			border-bottom:1px solid #e4e4e4;
		}
	.sold-table tr > td:last-child {
		text-align: center;
	}
	input[type='checkbox']{
		margin-left:5px;
	}
</style>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con">
	 <div class="o-mt"><h2>收件箱</h2></div>

     <div class="pagetab2">
                 <ul>           
                    <li class="on" id="inbox"><span>收件箱(${unReadInboxMsgNum})</span></li>
                    <li onclick="systemMessages();" id="systemMessages" ><span>系统通知(${unReadSystemMsgNum})</span></li>                  
                    <li onclick="imMessages();" id="imMessages" ><span>客服消息(${unReadCustomerMessage})</span></li>                  
                   </ul>       
         </div>
         
     <div id="mailInfo">
         <div id="recommend" class="recommend" style="display: block;">
                <input type="hidden" id="curPageNO" name="curPageNO" value="1"/>
                <table width="100%" cellspacing="0" cellpadding="0" class="buytable mailtab sold-table" id= "table">
                    <tbody><tr id = " ">
                        <th width="5%">
                        <!-- <input type="checkbox" id="checkbox" onclick="selAll();"/> -->
                        <span>
					           <label class="checkbox-wrapper" style="float:left;margin-left:9px;">
									<span class="checkbox-item">
										<input type="checkbox" id="checkbox" class="checkbox-input selectAll"/>
										<span class="checkbox-inner" style="margin-left:9px;"></span>
									</span>
							   </label>	
						</span>
                        </th>
                        <th width="3%"></th>
                        <th width="82%">标题</th>
                        <th width="10%" align="left" style="border-right: 1px solid #e4e4e4;">发送时间</th>                        
                    </tr>
                    	<c:choose>
				           <c:when test="${empty requestScope.list}">
				              <tr>
							 	 <td colspan="20">
							 	 	 
							 	 	 <div class="warning-option"><i></i><span style="text-align: center; ">没有符合条件的信息</span></div>
							 	 </td>
							 </tr>
				           </c:when>
				           <c:otherwise>
				                <c:forEach items="${requestScope.list}" var="siteInformation" varStatus="status">
			                    	 <tr id = "${siteInformation.msgId}"  style="vertical-align:middle;">
				                          <td>
				                          <%-- <input name="strArray" type="checkbox" value="${siteInformation.msgId}"/> --%>
				                          <span>
									           <label name="ie" class="checkbox-wrapper" style="float:left;margin:9px;">
													<span class="checkbox-item">
														<input align="middle" type="checkbox" id="strArray" class="checkbox-input selectOne" value="${siteInformation.msgId}" onclick="selectOne(this);"/>
														<span class="checkbox-inner" style="margin:9px;"></span>
													</span>
											   </label>	
										  </span>
				                          </td>
				                          <td>
				                          	<c:choose>
				                          		<c:when test="${siteInformation.status == 0}">
				                          			<img src="${contextPath}/resources/templets/images/mes_icon.jpg" width="16" height="11" />
				                          		</c:when>
				                          		<c:otherwise>
				                          			<img src="${contextPath}/resources/templets/images/read_msg_icon.png" width="16" height="14" />
				                          		</c:otherwise>
				                          	</c:choose>
				                          </td>
				                          <td><a onclick="mailinfo(${siteInformation.msgId});" href="javascript:void(0)">${siteInformation.title}</a></td>
				                          <td align="center" title="<fmt:formatDate value="${siteInformation.recDate}" pattern="yyyy-MM-dd HH:mm"/>"><fmt:formatDate value="${siteInformation.recDate}" type="date" /></td>
			                         </tr>
			                    </c:forEach>
				           </c:otherwise>
				         </c:choose>
                </tbody></table>				
			 	              		
	     </div>
	      	<div class="clear"></div>
	      	 <c:if test="${not empty requestScope.list }" >
		         		<div class="fl search-01">
                  <input class="bti btn-r" type="button" value="删除"  id="delBtn" onclick="deleteAction('deleteInboxMessage');" style="cursor: pointer;"/>
                  <input class="bti btn-g" type="button" value="清空"  id="clrBtn" onclick="clearAction();"  style="cursor: pointer;"/> 
              </div> 
              <div style="margin-top:10px;" class="page clearfix">
     			 <div class="p-wrap">
					<span class="p-num">
						<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
					</span>
     			 </div>
 			 </div>  
		   </c:if>	
         <!--user info end--> 
	  </div>
 </div>
        <!-------------订单end---------------->       
        
<div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/alternative.js'/>"></script>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/inbox.js'/>"></script>
 <script type="text/javascript">
 var contextPath = "${contextPath}";
   if(typeof JSON == 'undefined'){$('head').append($("<script type='text/javascript' src='${contextPath}/resources/common/js/json.js'>"));}
   
   window.onload=function (){

    	 $(function(){
    	       var UA = navigator.userAgent,
    	          isIE = UA.indexOf('MSIE') > -1,
    	          v = isIE ? /\d+/.exec(UA.split(';')[1]) : 'no ie';  
    	     if(v<=8)  //ie8及以下版本
    	      { 
    	    	 
    	    	 //var label=document.getElementsByName("lable_ie");
    	    	 var label = $("label[name='ie']");
    	    	 for(var i=0;i<label.length;i++)
    	    	 {
    	    		 label[i].style.cssText ='float:left;margin:9px;height:46px;';
    	    	 }
    	      } 
    	     });
   }
</script>
</body>
</html>
			