<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<div style="background-color:#fff" class="classify">
	<h1 style="background-color:">
		<span style="color:">热销排行</span>
	</h1>
	<div class="hot_rank">
	  <c:forEach items="${hotProds}" var="prod" varStatus="s">
	       <ul class="hot_rank_ul">
			<li class="hot_img"><span><a target="_blank" href="${contextPath}/views/${prod.prodId}"><img
						width="70" height="70" src="<ls:images item='${prod.pic}' scale='3'/>">
				</a>
			</span><i>${s.count}</i>
			</li>
			<li class="hot_name"><b><a target="_blank" href="${contextPath}/views/${prod.prodId}">${prod.prodName}</a>
			</b><span><strong>¥${prod.cash}</strong>
			</span>
			</li>
		</ul>
	  </c:forEach>
	</div>
</div>
