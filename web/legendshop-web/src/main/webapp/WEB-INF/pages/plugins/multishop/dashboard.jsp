<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/seller/seller-index.css'/>" rel="stylesheet"/>

			<div class="seller-index">
				<div class="shop-mes">
					<div class="head-scu">
						<a href="#"><img alt="" src="<ls:templateResource item='/resources/templets/images/seller-scu.png'/>"></a>
					</div>
					<div class="shop-mes-det">
						<div class="shop-name">
							<b>店主姓名</b>
							<span>卖家等级：普通会员</span>
						</div>
						<ul>
							<li>店铺名称：<span>苹果专卖店</span></li>
							<li>创建时间：2015-12-31 10:25:27</li>
							<li>上次登录：2015-12-31 10:25:27</li>
						</ul>
					</div>
					<div class="shop-mes-log">
						<p class="get-in"><a href="#">进入店铺</a></p>
						<p class="quit"><a href="#">退出登录</a></p>
					</div>
				</div>
<!-- 店铺信息 -->
				<div class="shop-tip">
					<div class="shop-tip-tit">
						<p>店铺提示</p>
					</div>
					<div class="shop-tip-det">
						<ul>
							<li>
								<a href="#">
									<p>咨询提示：</p>新增留言<span>(222)</span>
								</a>
							</li>
							<li>
								<a href="#">
									<p>投诉提示：</p>新增投诉<span>(311)</span>
								</a>
							</li>
							<li>
								<a href="#">
									出售中的商品：<span>(333)件</span>
								</a>
							</li>
							<li>
								<a href="#">
									仓库中的商品：<span>(110)件</span>
								</a>
							</li>
							<li>
								<a href="#">
									违规下架商品：<span>(257)件</span>
								</a>
							</li>
							<li>
								<a href="#">
									审核中的商品：<span>(783)件</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
<!-- 店铺提示 -->
				<div class="business-tip">
					<div class="business-tip-tit">
						<p>交易提示(最近一个月)</p>
					</div>
					<div class="business-tip-det">
						<ul>
							<li>
								<a href="#">
									交易中的订单：<span>(222)</span>
								</a>
							</li>
							<li>
								<a href="#">
									待付款订单：<span>(311)</span>
								</a>
							</li>
							<li>
								<a href="#">
									交易成功订单：<span>(333)</span>
								</a>
							</li>
							<li>
								<a href="#">
									已付款待发货：<span>(110)</span>
								</a>
							</li>
							<li>
								<a href="#">
									待买家收货：<span>(257)</span>
								</a>
							</li>
							<li>
								<a href="#">
									退后订单申请：<span>(783)</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
<!-- 交易提示 -->
				<div class="commodity-rank">
					<div class="commodity-rank-tit">
						<p>商品销售排行</p>
					</div>
					<table cellspacing="0" cellpadding="0">
						<tbody><tr class="table-tit">
							<td>商品排行</td>
							<td>商品名称</td>
							<td>商品销量</td>
						</tr>
						<tr>
							<td>
								<a href="#"><img alt="" src="images/com-img.png"></a>
							</td>
							<td>
								<a href="#">商家直邮 卡通黑红无线路由器 简约时尚爆款</a>
							</td>
							<td>151</td>
						</tr>
						<tr>
							<td>
								<a href="#"><img alt="" src="images/com-img.png"></a>
							</td>
							<td>
								<a href="#">商家直邮 卡通黑红无线路由器 简约时尚爆款</a>
							</td>
							<td>151</td>
						</tr>
						<tr>
							<td>
								<a href="#"><img alt="" src="images/com-img.png"></a>
							</td>
							<td>
								<a href="#">商家直邮 卡通黑红无线路由器 简约时尚爆款</a>
							</td>
							<td>151</td>
						</tr>
						<tr>
							<td>
								<a href="#"><img alt="" src="images/com-img.png"></a>
							</td>
							<td>
								<a href="#">商家直邮 卡通黑红无线路由器 简约时尚爆款</a>
							</td>
							<td>151</td>
						</tr>
						<tr>
							<td>
								<a href="#"><img alt="" src="images/com-img.png"></a>
							</td>
							<td>
								<a href="#">商家直邮 卡通黑红无线路由器 简约时尚爆款</a>
							</td>
							<td>151</td>
						</tr>
					</tbody></table>
				</div>
			</div>
<!-- 热卖商品排行 -->
			<div class="siade-nav">
				<div class="shop-balance">
					<ul>
						<li class="this-time">本次结算</li>
						<li>总销售金额：<span>1234元</span></li>
						<li>总退单金额：<span>1234元</span></li>
						<li>总订单金额：<span>1234元</span></li>
						<li>应结算金额：<span>1234元</span></li>
						<li class="bot">
							<a class="now" href="#">马上结算</a>
							<a class="before" href="#">已结算账单</a>
						</li>
					</ul>
				</div>
<!-- 本次结算 -->
				<div class="customer-service">
					<span>平台客服</span>
					<p>
						<a class="talk-fir" href="#"><img alt="" src="images/qq-tal.png"></a>
						<a class="talk-sec" href="#"><img alt="" src="images/qq-tal.png"></a>
					</p>
					<p>客服电话：4000453511</p>
				</div>
<!-- 平台客服 -->
				<div class="office-mes">
					<ul>
						<li>官方信息中心</li>
						<li><a href="#">卖家问题反馈中心</a><span>12-18</span></li>
						<li><a href="#">卖家问题反馈中心</a><span>12-18</span></li>
						<li><a href="#">卖家问题反馈中心</a><span>12-18</span></li>
						<li><a href="#">卖家问题反馈中心</a><span>12-18</span></li>
						<li><a href="#">卖家问题反馈中心</a><span>12-18</span></li>
						<li><a href="#">卖家问题反馈中心</a><span>12-18</span></li>
					</ul>
				</div>
<!-- 官方信息中心 -->
			</div>
<!-- 右侧导航栏 -->
