<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
  <table  style="width: 100%;margin: 0px; border-left: 0px;border-right: 0px; border-top:0px;" class="${tableclass}" id="col1">
         	<tr align="center">
         		<td colspan="6"><b>竞拍人记录</b></td>
         	</tr>
         	<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
         	<tr align="center">
         		<td>时间</td>
         		<td>出价人</td>
         		<td>保证金</td>
         		<!-- <td>订单状态</td>
         		<td>处理状态</td>
         		<td>操作</td> -->
         	</tr>
         	<c:forEach items="${list}" var="dep" varStatus="status">
         	  <c:if test="${dep.orderStatus == 1}">
         		<tr align="center">
         			<td><fmt:formatDate value="${dep.payTime}"  pattern="yyyy-MM-dd HH:mm:ss"/></td>
         			<td>
         			<c:choose>
         				<c:when test="${empty dep.nickName}">****${fn:substring(dep.userName,fn:length(dep.userName)-2,-1)}</c:when>
         				<c:otherwise>****${fn:substring(dep.nickName,fn:length(dep.nickName)-2,-1)}</c:otherwise>
         			</c:choose>
         			</td>
         			<td>${dep.payMoney}</td>
         			<%-- <td>
         				<c:choose>
	         				<c:when test="${dep.orderStatus eq 1}">
	         					已支付
	         				</c:when>
	         				<c:when test="${dep.orderStatus eq 0}">
	         					未支付
	         				</c:when>
         				</c:choose>
         			</td>
         			<td>
         				<c:choose>
         					<c:when test="${dep.orderStatus eq 0}">
         						未处理
         					</c:when>
         					<c:when test="${dep.orderStatus eq 1}">
         						<c:choose>
		         					<c:when test="${dep.flagStatus eq 0}">
			         					未处理
			         				</c:when>
			         				<c:when test="${dep.flagStatus eq 1}">
			         					<font color="red">已处理</font>
			         				</c:when>
		         				</c:choose>
         					</c:when>
	         				
         				</c:choose>
         			</td>
         			<td>
         			<c:choose>
         				<c:when test="${dep.orderStatus eq 0}">
         					<a href="javaScript:void(0);" onclick="update('${dep.id}','支付');">支付</a>
         				</c:when>
         				<c:when test="${dep.orderStatus eq 1 && dep.flagStatus eq 0}">
         				<a href="javaScript:void(0);" onclick="update('${dep.id}','退款');">退还保证金</a>
         				</c:when>
         			</c:choose>
         			</td> --%>
         		</tr>
         		</c:if>
         	</c:forEach>
         	<c:if test="${empty list}">
         		<tr align="center">
         			<td colspan="6"><b>暂无保证金记录</b></td>
         		</tr>
         	</c:if>
 </table>
 			<div class="clear"></div>
				 <div style="margin-top:10px;" class="page clearfix">
	     			 <div class="p-wrap">
	            		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  actionUrl="javaScript:depositRecListPage" type="simple"/> 
	     			 </div>
		  		</div>
