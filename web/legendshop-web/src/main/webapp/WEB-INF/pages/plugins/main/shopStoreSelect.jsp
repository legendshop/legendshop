<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet" />
<link href="<ls:templateResource item='/resources/templets/css/store/store.css'/>" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="boxy">
    <div class="ncs-chain-show">
        <dl>
            <dt>门店所在地区：</dt>
            <dd>
                <select class="combox sele" id="provinceid" name="provinceid" requiredTitle="true" childNode="cityid"
                        retUrl="${contextPath}/common/loadProvinces">
                </select>
                <select class="combox sele" id="cityid" name="cityid" requiredTitle="true" showNone="true"
                        childNode="areaid" retUrl="${contextPath}/common/loadCities/{value}">
                </select>
                <select class="combox sele" id="areaid" name="areaid" requiredTitle="true" showNone="true"
                        retUrl="${contextPath}/common/loadAreas/{value}">
                </select>
            </dd>
        </dl>
        <div class="ncs-chain-list">
            <ul nctype="chain_see">
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript">
	var contextPath = '${contextPath}';
	var currProdId = '${prodId}';
	var currSkuId = '${skuId}';
	var basketCount = '${basketCount}';
</script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/templets/js/shopStoreSelect.js'/>"></script>
</body>
</html>
