<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>我的订单 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>
<style>
.goods-name2 dt a:hover {
	color: #e5004f !important;
}
</style>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con" style="overflow:visible;">
<div class="right-layout">
	<div class="ncm-oredr-show">
		 <div class="ncm-order-info">
                        <div class="ncm-order-details">
                            <div class="title">订单信息</div>
                            <div class="content">
                            	<c:choose>
                            		<c:when test="${order.subType eq 'SHOP_STORE'}">
                            			<dl>
	                                        <dt>提货人：</dt>
	                                        <dd>
												<span>
				                                    ${storeOrder.reciverName}
												</span>
	                                        </dd>
	                                    </dl>
	                                    <dl>
	                                        <dt>联系电话：</dt>
	                                        <dd>
												 <span>
													${storeOrder.reciverMobile}
												 </span>
	                                        </dd>
	                                    </dl>
                            		</c:when>
                            		<c:otherwise>
                            			 <dl>
	                                        <dt>收货地址：</dt>
	                                        <dd>
												<span>
													<c:if test="${not empty order.userAddressSub}">
					                                    ${order.userAddressSub.receiver} ,
					                                    ${order.userAddressSub.detailAddress}
					                                </c:if>
												 </span>
	                                        </dd>
	                                    </dl>
	                                     <dl>
	                                        <dt>联系电话：</dt>
	                                        <dd>
											 <span>
												<c:if test="${not empty order.userAddressSub}">
				                                    ${order.userAddressSub.mobile} 
				                                </c:if>
											 </span>
	                                        </dd>
	                                    </dl>
	                                     <dl>
		                                    <dt>发&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;票：</dt>
		                                    <dd>
		                                        <c:choose>
		                                            <c:when test="${empty order.invoiceSub}">
		                                               	 不开发票
		                                            </c:when>
		                                            <c:otherwise>
		                                                ${order.invoiceSub.type==1?"普通发票":"增值税发票"}&nbsp;
														<c:choose>
															<c:when test="${order.invoiceSub.title==1}">[个人抬头]</c:when>
															<c:otherwise>[公司抬头]</c:otherwise>
														</c:choose>&nbsp;
														${order.invoiceSub.company}&nbsp;
														<c:if test="${order.invoiceSub.title ne 1}">(${order.invoiceSub.invoiceHumNumber})</c:if>
		                                            </c:otherwise>
		                                        </c:choose>
		                                    </dd>
		                                </dl>
                            		</c:otherwise>
                            	</c:choose>
                         
                      
                                <dl>
                                    <dt>买家留言：</dt>
									<dd>
									 <c:choose>
										<c:when test="${order.orderRemark=='null' or empty order.orderRemark}"> 
										     暂无
										</c:when>
										<c:otherwise>
										 	${order.orderRemark}
										</c:otherwise>
									</c:choose>
									</dd>
                                </dl>
                                <dl>
                                    <dt>支付方式：</dt>
									<dd>
									 <c:choose>
										<c:when test="${not empty order.payTypeName}"> 
										    ${order.payTypeName}
										</c:when>
										<c:otherwise>
										 	 未支付
										</c:otherwise>
									</c:choose>
									</dd>
                                </dl>
                                <dl>
                                    <dt>订单编号：</dt>
									<dd>
										${order.subNum}
									</dd>
                                </dl>
								<dl>
									<dt>商&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家：</dt>
									<dd>
										${order.shopName}
									</dd>
								</dl>
                            </div>
                        </div>
                        <div class="ncm-order-condition">
                            <dl>
								<dt>
									<i class="icon-ok-circle green"></i>订单状态：
								</dt>
								<dd>
								   <c:choose>
			                        	    <c:when test="${order.refundState == 1 }">
			                                                                                                                        退款退货中
			                                </c:when>
			                             <c:otherwise>
			                                 <c:choose>
			                                     <c:when test="${order.status==1 }">
			                                  	订单已经提交，等待买家付款
			                               </c:when>
			                               <c:when test="${order.status==2 }">
			                                 	  <c:choose>
														<c:when test="${order.mergeGroupStatus eq 0 && order.subType eq 'MERGE_GROUP'}">拼团中</c:when>
														<c:when test="${order.subType eq 'GROUP' && order.groupStatus eq 0}">团购中</c:when>
														<c:when test="${order.subType eq 'SHOP_STORE'}">待提货</c:when>
														<c:otherwise>待发货</c:otherwise>
													</c:choose>
			                               </c:when>
			                               <c:when test="${order.status==3 }">
			                                	   待收货
			                               </c:when>
			                               <c:when test="${order.status==4 }">
			                                  	 已完成
			                               </c:when>
			                               <c:when test="${order.status==5 || order.status==-1 }">
			                                                                                                                            交易关闭
			                               </c:when>
			                                 </c:choose>
			                             </c:otherwise>
			                         </c:choose>
								</dd>
							</dl>
							<ul>
							<c:choose>
								  <c:when test="${order.refundState eq 1}">
								  		<li>该订单正在退款/退货中...</li>
								  </c:when>
								  <c:otherwise>
									  <c:choose>
										   <c:when test="${order.status==1 && order.subType != 'SHOP_STORE'}">
										      		<li>1. 您尚未对该订单进行支付，请<a class="ncbtn-mini ncbtn-bittersweet" target="_blank"  href="<ls:url address="/p/orderSuccess?subNums=${order.subNum}" />" ><i></i>支付订单</a>以确保商家及时发货。</li>
												   <li>2. 如果您不想购买此订单的商品，请选择<a class="ncbtn-mini" href="#order-step">取消订单</a>操作。</li>
					<!-- 									<li>3. 如果您未对该笔订单进行支付操作，系统将于 <time>2015-07-20 16:11:49</time>
															自动关闭该订单。</li>
					 -->							 </c:when>
					                                <c:when test="${order.status==2 && order.subType != 'SHOP_STORE'}">
					                                     <c:choose>
															<c:when test="${order.subType ne 'MERGE_GROUP' && order.subType ne 'GROUP'}">
																 <li>1. 订单已提交商家进行备货发货准备。</li>
															</c:when>
															<c:when test="${order.subType eq 'GROUP'}">
																<c:choose>
																	<c:when test="${order.groupStatus eq 1}">
																		<li>1. 订单已提交商家进行备货发货准备。</li>
																	</c:when>
																	<c:otherwise>活动凑单中</c:otherwise>
																</c:choose>
															</c:when>
															<c:otherwise>
																<c:choose>
																	<c:when test="${order.mergeGroupStatus eq 1}">
																		<li>1. 订单已提交商家进行备货发货准备。</li>
																	</c:when>
																	<c:otherwise>订单拼团中</c:otherwise>
																</c:choose>
															</c:otherwise>
														</c:choose>
									        </c:when>
									         <c:when test="${order.status==3 && order.subType != 'SHOP_STORE'}">
												        <li>1. 商品已发出；  物流公司：${order.delivery.delName}；  查看 <a class="blue" href="#order-step">“物流跟踪”</a> 情况。</li>
												        <li>2. 如果您已收到货，且对商品满意，您可以<a class="ncbtn-mini ncbtn-mint" href="#order-step">确认收货</a>完成交易。</li>
												     <c:if test="${not empty order.dvyDate }">
												           <c:set var="confirmTime"  value="${prodApi:getOrderConfirmTime(order.dvyDate)}"></c:set>
												           <li>3. 系统将于<time><fmt:formatDate value="${confirmTime}" type="both"/></time>自动完成“确认收货”，完成交易。</li>
												        </c:if>
									        </c:when>
									        <c:when test="${order.status==4 && order.subType != 'SHOP_STORE'}">
					      								<li>1. 如果收到货后出现问题，您可以联系商家协商解决。</li>
					      							<!-- 	<li>2. 如果商家没有履行应尽的承诺，您可以申请 <a class="red" href="#order-step">"投诉维权"</a>。</li> -->
					           						    <li>2. 交易已完成，你可以对购买的商品及商家的服务进行<a class="ncbtn-mini ncbtn-aqua" href="#order-step">评价</a>及晒单。</li>
									        </c:when>
									          <c:when test="${order.status==5 || order.status==-1 && order.subType != 'SHOP_STORE'}">
									          		<c:choose>
									          			 <c:when test="${order.refundState eq 2}">
														  	<li>系统 于 <fmt:formatDate value="${order.updateDate}" type="both" /> 取消了订单 ( 商品全部退款完成取消订单 )</li>
														 </c:when>
														 <c:otherwise>
														 	<li>订单于<fmt:formatDate value="${order.updateDate}" type="both" /> 取消 :(${order.cancelReason})</li>
														 </c:otherwise>
									          		</c:choose>
									        </c:when>
									         <c:when test="${ order.subType == 'SHOP_STORE'}">
			      								<li>门店名：${store.name}</li>
			      								<li>门店地址：${store.shopAddr}</li>
			      								<li>营业时间：${store.businessHours}</li>
			      								<li>提货码：${storeOrder.dlyoPickupCode}</li>
									        </c:when>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</ul>
                        </div>
                        <!-- <div class="mall-msg">
                        	    有疑问可咨询<a href="javascript:void(0);"><i class="icon-comments-alt"></i>平台客服</a>
                        </div> -->
                    </div>
		<!-- 订单的退款状态不是已完成 或者 订单不是已取消的情况下 正常显示订单进度图 -->
		<c:if test="${order.refundState ne 2 || order.status ne 5}">
			<c:choose>
				<c:when test="${order.subType != 'SHOP_STORE'}">
					<div class="ncm-order-step" id="order-step">
						<dl class="step-first current">
							<dt>生成订单</dt>
							<dd class="bg"></dd>
							 <dd title="下单时间" class="date"><fmt:formatDate value="${order.subDate}" pattern="yyyy-MM-dd HH:mm:ss" type="both" /></dd> 
						</dl>
						<dl class="<c:if test="${not empty order.payDate}">current</c:if>">
							<dt>完成付款</dt>
							<dd class="bg"></dd>
							<c:if test="${not empty order.payDate}"><dd title="付款时间" class="date"><fmt:formatDate value="${order.payDate}" pattern="yyyy-MM-dd HH:mm:ss" type="both" /></dd></c:if>
						</dl>
						<dl class="<c:if test="${not empty order.dvyDate}">current</c:if>">
							<dt>商家发货</dt>
							<dd class="bg"></dd>
							<c:if test="${not empty order.dvyDate}"><dd title="发货时间" class="date"><fmt:formatDate value="${order.dvyDate}" pattern="yyyy-MM-dd HH:mm:ss" type="both" /></dd></c:if>
						</dl>
						<dl class="<c:if test="${not empty order.finallyDate}">current</c:if>">
							<dt>确认收货</dt>
							<dd class="bg"></dd>
							<c:if test="${not empty order.finallyDate}"><dd title="确认收货时间" class="date"><fmt:formatDate value="${order.finallyDate}" pattern="yyyy-MM-dd HH:mm:ss" type="both" /></dd></c:if>
						</dl>
						<dl class="<c:if test="${not empty order.finallyDate}">current</c:if>">
							<dt>完成</dt>
							<dd class="bg"></dd>
							<c:if test="${not empty order.finallyDate}"><dd title="完成时间" class="date"><fmt:formatDate value="${order.finallyDate}" pattern="yyyy-MM-dd HH:mm:ss" type="both" /></dd></c:if>
						</dl>
					</div>
				</c:when>
				<c:otherwise>
				</c:otherwise>
			</c:choose>
		</c:if>
		<div class="ncm-order-contnet">
			<table class="ncm-default-table order sold-table">
				<thead>
					<tr>
						<th class="w10"></th>
						<th colspan="2">商品名称</th>
						<th class="w20"></th>
						<th class="w120" align="center">单价（元）</th>
						<th class="w90">数量</th>
						<!-- <th class="w100">优惠活动</th> -->
						<th class="w100">售后维权</th>
						<th class="w100">交易状态</th>
						<th class="w100" style="border-right:1px solid #e4e4e4;">交易操作</th>
					</tr>
				</thead>
				<tbody>
				  
				    <tr>
						<td style="text-align: left;overflow: visible;" colspan="10">
							<div class="order-deliver">
							    <span>支付方式： ${order.payTypeName}</span>
							    
							    <c:if test="${(order.status==3 || order.status==4) && (order.subType != 'SHOP_STORE')}">
								<span>物流公司： <a href="${order.delivery.delUrl}" target="_blank">${order.delivery.delName}</a></span>
								<span> 物流单号： ${order.delivery.dvyFlowId} </span>
								<span><a id="show_shipping" href="javascript:void(0);">物流跟踪<i class="icon-angle-down"></i>
										<div class="more">
											<span class="arrow"></span>
											<ul id="shipping_ul">
												<li>加载中...</li>
											</ul>
										</div> </a>
								</span>
								 </c:if>
							</div>
						</td>
					</tr>
					
					<!-- S 商品列表 -->
				<c:forEach items="${order.subOrderItemDtos}" var="orderItem" varStatus="orderItemStatues">
					<c:if test="${orderItem.commSts==0}">
			    		<c:set value="${notCommedCount + 1}" var="notCommedCount" /> 
			    	</c:if>
				    <tr class="bd-line">
						<td>&nbsp;</td>
						<td class="w70"><div class="ncm-goods-thumb">
									<a target="_blank"
								href="<ls:url address='/views/${orderItem.prodId}'/>">
								<img src="<ls:images item='${orderItem.pic}' scale='3'/>">
								
							</a>
							</div>
						</td>
						<td class="tl a-left"><dl class="goods-name2">
							<dt>
								<a target="_blank"
									href="<ls:url address='/views/${orderItem.prodId}'/>">${orderItem.prodName}</a>
									<c:if test="${not empty orderItem.snapshotId && orderItem.snapshotId!=0 && order.subType != 'SHOP_STORE'}" >
										<span class="rec">
										<a  href="<ls:url address='/snapshot/${orderItem.snapshotId}'/>" target="_blank">[交易快照]</a>
									    </span>
									 </c:if>
							</dt>
							<dd>${orderItem.attribute}</dd>
							<!-- 消费者保障服务 -->
						  </dl>
						</td>
						<td></td>
						<td class="tl">
							<p style="text-align: center;"><fmt:formatNumber value="${orderItem.cash}" type="currency" pattern="0.00"/></p>
							<c:if test="${orderItem.refundState == 2}">
					         	<p style="color:#69AA46;text-align: center;">${orderItem.refundAmount}(退)</p>
					         </c:if>
						</td>
						<td>${orderItem.basketCount}</td>
						<td>
							<c:choose>
								<c:when test="${order.actualTotal ne 0}">
									<c:choose>
				                		<c:when test="${order.status gt 2 &&  order.status le 4 &&  orderItem.refundState le 0 && orderItem.inReturnValidPeriod}">
				                		     <!-- 订单处于 待发货、已完成状态 并且 没有处理过退款或者退款失败的 -->
				                		    <p><a  href="#" onclick="returnApply('${order.subId}','${orderItem.subItemId}','${order.subNum}');">退款/退货</a></p>
				                		</c:when>
				                		<c:when test="${orderItem.refundType==1 && (orderItem.refundState==1 || orderItem.refundState==2)}">
				                		    <p><a target="_blank" href="${contextPath}/p/refundDetail/${orderItem.refundId}">查看退款</a>
				                		</c:when>
				                		<c:when test="${orderItem.refundType==2 && (orderItem.refundState==1 || orderItem.refundState==2)}">
				                		    <p><a target="_blank" href="${contextPath}/p/returnDetail/${orderItem.refundId}">查看退货</a>
				                		</c:when>
			                		</c:choose>
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose>
						</td>
						<!-- <td>
							退款 投诉
					   </td> -->

						<!-- S 合并TD -->
                    <c:if test="${orderItemStatues.index==0}">  
	                     <c:choose>
		                		<c:when test="${fn:length(order.subOrderItemDtos)>0}">
		                			<!-- 未支付 -->
		                			<td rowspan="${fn:length(order.subOrderItemDtos)}" class="bdl">
		                		</c:when>
		                		<c:otherwise>
		                			<td class="bdl">
		                		</c:otherwise>
		                </c:choose>
		                     <c:if test="${order.refundState ne 1 }">
			                  <c:choose>
									 <c:when test="${order.status==1 }">
									        <span class="col-orange">待付款</span>
									 </c:when>
									 <c:when test="${order.status==2 }">
									    <c:choose>
											<c:when test="${order.mergeGroupStatus eq 0 && order.subType eq 'MERGE_GROUP'}"><span class="col-orange">拼团中</span></c:when>
											<c:when test="${order.subType eq 'GROUP' &&  order.groupStatus eq 0}"><span class="col-orange">团购中</span></c:when>
											<c:when test="${order.subType eq 'SHOP_STORE'}"><span class="col-orange">待提货</span></c:when>
											<c:otherwise><span class="col-orange">待发货</span></c:otherwise>
										</c:choose>
									 </c:when>
									 <c:when test="${order.status==3 }">
									       <span class="col-orange">待收货</span>
									 </c:when>
									 <c:when test="${order.status==4 }">
									      已完成
									 </c:when>
									 <c:when test="${order.status==5 || order.status==-1}">
									      交易关闭
									 </c:when>
							  </c:choose>
							 </c:if>
		                </td>
		                
					
						  <c:choose>
		                		<c:when test="${fn:length(order.subOrderItemDtos)>0}">
		                			<!-- 未支付 -->
		                			<td rowspan="${fn:length(order.subOrderItemDtos)}" class="bdl" style="border-right:1px solid #ddd;">
		                		</c:when>
		                		<c:otherwise>
		                			<td class="bdl" style="border-right:1px solid #ddd;">
		                		</c:otherwise>
		                </c:choose>
							<!-- 取消订单 -->
							<p>
								<c:if test="${order.refundState != 1 }">
								   <c:choose>
									 <c:when test="${order.status==1 }">
									    <a class="btn-r" href="javascript:void(0)" onclick="cancleOrder('${order.subNum}');"><i
										class="icon-ban-circle"></i> 取消订单</a>
									 </c:when>
									 <c:when test="${order.status==2 }">
									    <!--  <a class="ncbtn" href="index.php?act=member_refund&amp;op=add_refund_all&amp;order_id=335">订单退款</a> -->
									 </c:when>
									 <c:when test="${order.status==3 }">
									    <a class="btn-r" href="javascript:void(0)" onclick="javascript:orderReceive('${order.subNum}');"><i
										class="icon-ban-circle"></i> 确认收货</a>
									 </c:when>
									 <c:when test="${order.status==4&&notCommedCount>0 }">
									       <a class="btn-r" href="${contextPath}/p/prodcomment/${order.subId}"><i
										class="icon-ban-circle"></i>评论商品</a>
									 </c:when>
									 <c:when test="${order.status==4&&notCommedCount==undefined }">
									       <a class="btn-r" href="${contextPath}/p/prodcomment/${order.subId}"><i
										class="icon-ban-circle"></i>查看评论</a>
									 </c:when>
								  </c:choose>
							  </c:if>
							  <c:if test="${order.refundState == 1 }">
							  		<span class="col-orange">退款退货中</span>
							  </c:if>
						 </p>
						 <!-- 退款取消订单 --> 
						 <!-- 收货 --> 
						 <!-- 评价 --> 
						 <!-- 已经评价 --> 
						 <!-- 分享  -->
							<!-- <p>
								<a  href="javascript:void(0)">分享商品</a>
							</p> -->
						</td>
						
						</c:if>
						<!-- E 合并TD -->
					</tr>
				    
				</c:forEach>
				
					
					<!-- S 赠品列表 -->
					<!-- <tr class="bd-line">
						<td>&nbsp;</td>
						<td class="tl" colspan="7"><div class="ncm-goods-gift">
								<strong>赠品：</strong>
								<ul>
									<li><a
										href="#"
										title="赠品：【单拍无效】德国凯驰官方旗舰店 擦车洗车毛巾赠品 160cm*60cm * 1"
										target="_blank"><img onmouseout="toolTip()"
											onmouseover="toolTip('&lt;img src=&gt;')"
											src="#">
									</a>
									</li>
								</ul>
							</div>
						</td>
					</tr> -->
					<!-- E 赠品列表 -->

				</tbody>
				<tfoot>
				
				<!-- 	<tr>
						<th colspan="20">
							<dl class="nc-store-sales">
								<dt>满减</dt>
								<dd>
									满500减30 送<a target="_blank"
										title="【单拍无效】德国凯驰官方旗舰店 擦车洗车毛巾赠品 160cm*60cm"
										href="#">[赠品]</a><a
										href="#"
										target="_blank" title="【单拍无效】德国凯驰官方旗舰店 擦车洗车毛巾赠品 160cm*60cm"
										class="ncc-store-gift"><img
										src="#"
										alt="【单拍无效】德国凯驰官方旗舰店 擦车洗车毛巾赠品 160cm*60cm">
									</a>
								</dd>
							</dl></th>
					</tr> -->
					
					
					<tr>
						<td colspan="20" style="background: #fff;">
							<dl><dd>商品总额： <fmt:formatNumber value="${order.total}" type="currency" pattern="0.00"></fmt:formatNumber></em></dd></dl>
							<dl class="freight">
							<%-- <dd>
							<c:choose>
								 <c:when test="${order.dvyType == "express">
								   快递[1元]
								 </c:when>
								  <c:when test="${order.dvyType == "ems"}">
								   EMS[11元]
								 </c:when>
							 </c:choose>
							</dd>  --%>
							<dd>
							<c:choose>
								 <c:when test="${empty order.freightAmount || order.freightAmount==0}">
								    免运费
								 </c:when>
								 <c:otherwise>
								    运费： +<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>
								 </c:otherwise>
							 </c:choose>
							</dd>
							</dl>
							<c:if test="${not empty order.discountPrice && order.discountPrice!=0}">
								<dl><dd>促销优惠： -${order.discountPrice}</dd></dl>
							</c:if>
							<c:if test="${not empty order.couponOffPrice && order.couponOffPrice!=0}">
								<dl><dd>优惠券： -${order.couponOffPrice}</dd></dl>
							</c:if>
							<c:if test="${not empty order.redpackOffPrice && order.redpackOffPrice!=0}">
								<dl><dd>红包： -${order.redpackOffPrice}</dd></dl>
							</c:if>
							<c:if test="${not empty order.changedPrice && order.changedPrice!=0}">
								<c:choose>
									<c:when test="${order.changedPrice lt 0}">
										<dl><dd>商家改价： ${order.changedPrice}</dd></dl>
									</c:when>
									<c:otherwise>
										<dl><dd>商家改价：+${order.changedPrice}</dd></dl>
									</c:otherwise>
								</c:choose>
								
							</c:if>
							<dl class="sum">
								<dt></dt>
								<dd>
									订单应付金额：
									<em><fmt:formatNumber value="${order.actualTotal}" type="currency" pattern="0.00"></fmt:formatNumber></em>元
								</dd>
							</dl>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
<div class="clear"></div>
   </div>
        <!-------------订单end---------------->
        
       
     </div>
    <!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/ToolTip.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/myorder.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/userOrderDetail.js'/>"></script>
<script type="text/javascript">
var contextPath = "${contextPath}";
var dvyFlowId="${order.dvyFlowId}";
var dvyTypeId="${order.dvyTypeId}";
var reciverMobile="${order.userAddressSub.mobile}";
</script>
</body>
</html>