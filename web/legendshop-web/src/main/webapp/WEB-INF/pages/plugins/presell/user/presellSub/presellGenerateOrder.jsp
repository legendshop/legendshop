<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
 <%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%
	response.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT");   
	// 设置 HTTP/1.1 no-cache 头   
	response.setHeader("Cache-Control", "no-store,no-cache,must-revalidate");   
	// 设置 IE 扩展 HTTP/1.1 no-cache headers， 用户自己添加   
	response.addHeader("Cache-Control", "post-check=0, pre-check=0");   
	// 设置标准 HTTP/1.0 no-cache header.   
	response.setHeader("Pragma", "no-cache");
 %>
<!DOCTYPE html>
<html>
<head>
<!-- the same with indexFrame.jsp under red template -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="renderer" content="webkit"/>
    <c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
    <title>填写核对订单信息-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
    <link rel="stylesheet" href="<ls:templateResource item='/resources/common/css/jquery.autocomplete.css'/>">
    <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/plugins/showLoading/css/showLoading.css'/>" />
</head>
<body  class="graybody">
	<div id="doc">
         <!--顶部menu-->
		  <div id="hd">
		   
		      <!--hdtop 开始-->
		       <%@ include file="/WEB-INF/pages/plugins/main/home/header.jsp" %>
		      <!--hdtop 结束-->
		    
		       <!-- nav 开始-->
		        <div id="hdmain_cart">
		          <div class="yt-wrap">
		            <h1 class="ilogo">
		                <a href="${contextPath}/"><img src="<ls:photo item="${systemConfig.logo}"/>"  style="max-height:89px;" /></a>
		            </h1>
		            
		            <!--page tit-->
		              <div class="cartnew-title">
		                        <ul class="step_h">
		                            <li class="done"><span>购物车</span></li>
		                            <li class="on done"><span>填写核对订单信息</span></li>
		                            <li class=" "><span>提交订单</span></li>
		                        </ul>
		              </div>
		            <!--page tit end-->
		          </div>
		        </div>
		      <!-- nav 结束-->
		 </div><!--hd end-->
		<!--顶部menu end-->
    
       
         <div id="bd" style="margin-bottom: 30px;">
		      <div class="yt-wrap ptit2">填写并4635核对订单信息</div>
		      <!--order content -->
		         <div class="yt-wrap ordermain">
		          <!-- 收货地址 -->
		          <div id="path" class="path">
		            <h3 class="v_align_mid">收货地址 <a class="btnlink" href="javascript:addNewAddress();">新增地址</a></h3>
		            <div id="path_list">
		                  
		       	  	</div>
		          </div>
		          
		          <!-- 支付方式 -->
		          <div id="pay" class="pay">
		            <h3>支付方式<b style="display: none;" onclick="PayMethod.openModifyWindow();" class="update">修改</b><span id="pay_msg"></span></h3>
		              <div style="display: block;" id="pay_opened">
		              <ul>
		               
		               <c:choose>
							<c:when test="${requestScope.userShopCartList.cod}">
								       <li>
								       <input type="radio" value="1" id="hd_pay" name="pay_opened_pm_radio">
								        <label style="cursor:pointer;margin-right:80px;"  for="hd_pay">货到付款</label>
								          <em>送货上门后再收款，支持现金、POS机刷卡、支票支付</em>
								       </li>
							</c:when>
							 <c:otherwise>
								       <li>
								        <input type="radio" disabled="disabled"  value="1" id="hd_pay" name="pay_opened_pm_radio">
								        <label style="cursor:pointer;margin-right:80px;"  for="hd_pay">货到付款</label>
								        <em style="color: red;">该订单部分商品不支持货到付款</em>
								       </li>
							</c:otherwise>
						</c:choose>
		                <li>
		                   <input type="radio" checked="checked" value="2" id="online_pay" name="pay_opened_pm_radio">
		                   <label style="cursor:pointer;margin-right:80px;"  for="online_pay">在线支付</label>
		                   <em>支持绝大数银行卡及第三方在线支付</em>
		                </li>
		                <!-- <li><span style="cursor:pointer;" onclick="PayMethod.select(this,6);"><input type="radio" value="6" name="pay_opened_pm_radio">门店自提</span><select style="height: 28px;">
		                  <option>选择门店</option></select>&nbsp;&nbsp;<input type="text" placeholder="收货人" style="height: 26px;text-indent: 5px;">&nbsp;&nbsp;<input type="text" placeholder="手机号码" style="height: 26px;text-indent: 5px;">
		                </li> -->
		              </ul>
		            </div>
		          </div>
		          
		         <!--  
		           <div id="pay" class="pay">
		            <h3>快递方式<b style="display: none;" onclick="PayMethod.openModifyWindow();" class="update">修改</b><span id="pay_msg"></span></h3>
		             
		            <div style="display: block;" id="pay_opened">
		              <ul>
		                <li><span style="cursor:pointer;" onclick="PayMethod.select(this,33);"><input type="radio" checked="checked" value="33" name="pay_opened_pm_radio">普通快递</span><em style="color:#e5004f;">运费 ￥0.00</em></li>
		                <li><span style="cursor:pointer;" onclick="PayMethod.select(this,1,'1');"><input type="radio" ispos="1" value="1" name="pay_opened_pm_radio">顺丰速运</span><em style="color:#e5004f;">运费 ￥5.00</em>&#12288;(仅支持顺丰可配送的地区)</li>
		                
		              </ul>
		              <p>注：配送会由于天气，交通等不可抗拒的客观因素造成您收货时间延迟，请您</p>
		            </div>
		          </div>
		           -->
		          
		          <!-- 配送时间 -->
		      <!--     <div id="time" class="time">
		             <h3>快递配送时间</h3>
		            <div id="time_opened">
		              <dl class="on" value="1" >
		                <dt>不限时间</dt>
		              </dl>
		              <dl class="" value="2" >
		                <dt>周一至周五</dt>
		              </dl>
		              <dl class="" value="3" >
		                <dt>周六日/节假日</dt>
		              </dl>
		            </div>
		          </div> -->
		          <!-- 配送时间 -->
		          
		          <!-- 商品清单 -->
		          <div class="pro-list">
		            <h3 style="margin-bottom: -15px;">预售商品清单</h3>
		             <%--  <a style="float: right;margin-top: -20px;color: #3f6de0;" href="${contextPath}/shopCart/shopBuy">返回购物车修改</a> --%>
		            <div id="shoppinglistDiv">
		                <c:forEach items="${requestScope.userShopCartList.shopCarts}" var="carts" varStatus="status">
		                     <p style="margin-top: 15px;margin-bottom: -10px;">
				                                           店铺：<a style="color: #e5004f;" target="_blank" href="${contextPath}/store/${carts.shopId}" title="${carts.shopName}" >${carts.shopName}</a>
				             </p>
		                   <table class="cartnew_table">
								<thead style="border: 1px solid #e4e4e4;border-bottom: 0;">
									<tr>
										<th align="left" style="width:450px;">店铺宝贝</th>
										<th>单价</th>
										<th>数量</th>
										<th>状态</th>
										<th>配送方式</th>
									</tr>
								</thead>
								<tbody style="border: 1px solid #e4e4e4;border-top: 0;">
			                    <c:forEach items="${carts.cartItems}" var="basket" varStatus="s">
				                    <tr>
				                        <td class="textleft"> 
						                        <div class="cartnew-img">
						                           <a class="a_e" target="_blank" href="${contextPath}/views/${basket.prodId}">
						                           <img title="${basket.prodName}" src="<ls:images item='${basket.pic}' scale='3' />" > 
						                           </a>                            
						                        </div>
					                           <ul class="cartnew-ul" >
					                           	<li> <a href="${contextPath}/views/${basket.prodId}" target="_blank" class="a_e">${basket.prodName}</a></li>
				                                 <li>
				                                    <c:if  test="${not empty basket.attribute}" >
				                                       ${basket.attribute}
				                                    </c:if>
				                                 </li>
				                                 <c:if test="${not empty basket.promotionInfo }">
				                                     <li>
				                                       <span>优惠信息</span><em>${basket.promotionInfo}</em>
				                                     </li>
				                                  </c:if>
				                              </ul>
				                        </td>
				                         <td>
				                            <fmt:formatNumber var="cashFormat" value="${basket.promotionPrice}" type="currency" pattern="0.00"></fmt:formatNumber>
				                             <span style="font-size:14px; color:#e5004f;">￥ ${cashFormat}</span>
				                         </td>
				                         <td>
				                          x${basket.basketCount}
				                        </td>
				                        <td><c:choose>
				                        	<c:when test="${empty requestScope.userShopCartList.provinceId}">
				                        		<span>有货</span>
				                        	</c:when>
				                        	<c:when test="${empty basket.stocks || basket.stocks==0}">
				                        		<span class="stocksErr">无货</span>
				                        	</c:when>
				                        	<c:when test="${basket.stocks<basket.basketCount}">
				                        		 <span class="stocksErr">缺货，仅剩${basket.stocks}件</span>
				                        	</c:when>
				                        	<c:otherwise>
				                        		<span>有货</span>
				                        	</c:otherwise>
				                        </c:choose>
				                        </td>
				                          <c:if test="${s.index==0}">
				                              <td  rowspan="${fn:length(carts.cartItems)}">
				                                       <c:choose>
												        <c:when test="${carts.freePostage}">
												                         商家承担运费
												        </c:when>
												        <c:otherwise>
												             <select shopId="${carts.shopId}"  class="delivery">
									                             <c:choose>
															        <c:when test="${empty carts.transfeeDtos}">
															         <option value="-1" style="padding: 5px 0px;">请选择配送方式</option>
															        </c:when>
															        <c:otherwise>
															             <c:forEach items="${carts.transfeeDtos}" var="transtype" varStatus="s">
												                            <option style="padding: 5px 0px;" transtype="${transtype.freightMode}" value="${transtype.deliveryAmount}" >${transtype.desc}</option>
												                        </c:forEach>
															         </c:otherwise>
															        </c:choose>
									                        </select>
												        </c:otherwise>
												      </c:choose>
				                              </td>
				                          </c:if>
				                    </tr>  
			                    </c:forEach>
		                    </tbody>   	  
		                </table>
		                
		                
		                  <div class="shop-add">
                              <div class="shop-wor">给卖家留言：<input type="text" placeholder="限45个字" name="remark" shopId="${carts.shopId}"  maxlength="45"></div>
                      	<!-- 店铺优惠券 -->
					               <c:if test="${not empty carts.couponList}">
					               		  <div class="shop-fav">店铺优惠券：
								                   <select  shopid="${carts.shopId}" class="shop_coupon">
													 <c:forEach items="${carts.couponList}" var="coupon">
														<option couponid="${coupon.couponId}" value="${coupon.offPrice}" >${coupon.offPrice}元优惠券</option>
													 </c:forEach>
						               				 <option value="0.00">不使用优惠券</option>
												  </select>
								                <em class="coupon_offPrice">-0.00</em>
								           </div>
					               </c:if>
                         </div>
		              </c:forEach>


	            <div class="invoice">
	                <div class="invoice-tit invoice-open">
	                   <b>发票信息</b>
	                   <a onclick="showForm_invoice();" href="javascript:void(0)">[修改]</a>
	                </div>
	                 <div class="invoice-tit invoice-close" style="display: none;">
	                   <b>发票信息</b>
	                   <a onclick="close_invoice()" href="javascript:void(0)">[关闭]</a>
	                </div>
	                <c:choose>
	                    <c:when test="${empty userdefaultInvoice || userdefaultInvoice.contentId ==1}">
		                    <div class="invoice-no-spr">
		                      <p>不需要发票</p>
		                   </div>
	                    </c:when>
						<c:when test="${userdefaultInvoice.contentId ==2}">
						   <div class="invoice-no-spr">
			                   <p invoice="${userdefaultInvoice.id}">
			                    ${userdefaultInvoice.typeId ==1?'普通发票':''}&nbsp;&nbsp;            
			                    <c:choose><c:when test="${userdefaultInvoice.titleId ==1}">个人</c:when><c:when test="${userdefaultInvoice.titleId ==2}">单位</c:when></c:choose><c:if test="${not empty userdefaultInvoice.company}">( ${userdefaultInvoice.company} )</c:if>
			                    &nbsp;&nbsp;<c:if test="${userdefaultInvoice.contentId ==2}">明细</c:if>
			                   </p>
			                </div> 
						</c:when>
					</c:choose>
	               
	                <div class="invoice-spr"  style="display: none;">
	                  <div id="invoiceList">
	                  
	                  </div>
	                  
	                  <div class="invoice-spr-cho" style="margin-left: 20px;">
	                    <p>发票类型：
	                       <span>
	                         <input type="radio" value="1" checked="checked" id="invoince_InvoiceType" name="invoince_type">
	                       </span><label for="invoince_InvoiceType">普通发票(纸质)</label>
	                     </p>
	                     <p>发票抬头：
	                       <span>
	                         <input type="radio" value="1"  checked="checked"  id="invoince_pttt1"  name="invoince_pttt">
	                       </span><label for="invoince_pttt1">个人</label>
	                       <span>
	                         <input type="radio" value="2"  id="invoince_pttt2"  name="invoince_pttt"> 
	                       </span><label for="invoince_pttt2">单位</label>
	                     </p>
	                     <div id="invoice_unitNameTr" style="display: block;">
	                          <input type="text" placeholder="发票抬头" maxlength="20" style="width:230px;height: 25px;margin-left:60px;padding:0 5px;" id="invoice_Unit_TitName">
	                          <label id="titNameNote"  class="item-con_error hide"></label>
	                     </div>
	                    <p>发票内容：
	                      <span>
	                       <input type="radio" value="2"  checked="checked" name="invoince_content" id="invoince_content2">
	                       </span><label for="invoince_content2">明细</label>
	                     </p>
	                  </div> 
	                  
	                  <div class="invoice-but">
	                    <a href="javascript:void(0);" onclick="savePart_invoice();" class="red">保存发票信息</a>
	                    <a href="javascript:void(0);" onclick="no_invoice();" class="gray">不需要发票</a>
	                  </div>
	                  <div class="invoice-exp">您最多可以添加10条发票信息，请先删除部分不常用发票后再添加</div>
	                </div>
	           </div>

				<!-- 红包 -->
		            
		          <!-- 确认订单按钮 -->
		          <div class="confirm" id="confirm">
						<div class="price" style="margin-top: 20px;">
							<p>
								商品件数总计：<span id="allCount">${requestScope.userShopCartList.allCount}</span>
							</p>
							<p>
								商品总价：<span id="allActualCash">￥<fmt:formatNumber value="${requestScope.userShopCartList.allActualCash}" type="currency" pattern="0.00"></fmt:formatNumber></span>
							</p>
							<p>
								运费：<span id="allFreightAmount">￥<fmt:formatNumber value="${requestScope.userShopCartList.allFreightAmount}" type="currency" pattern="0.00"></fmt:formatNumber></span>
							</p>
							<p>
								促销优惠：<span id="allDiscount">-￥<fmt:formatNumber value="${requestScope.userShopCartList.allDiscount}" type="currency" pattern="0.00"></fmt:formatNumber></span>
							</p>
							<p>
							    <fmt:formatNumber var="allCashFormat" value="${requestScope.userShopCartList.allCash}" type="currency" pattern="0.00"></fmt:formatNumber>
								需支付总额：<b><span class="red" id="allCash" >￥${allCashFormat}</span></b>
							</p>
						</div>
						 <span id="confirm_totalPrice">应付金额：<b>￥<em id="allCashFormat">${allCashFormat}</em></b></span>
		                 <button  class="on" id="submitOrder" onclick="submitOrder();">确认订单</button>
		          </div>
		        </div>
		<!--order content end-->
		   </div>
	     
	     <form:form id="orderFormReload" action="${contextPath}/p/orderDetails" method="post">
		        <input id="shopCartItems" name="shopCartItems" value="${shopCartItems}"  type="hidden"/>
		</form:form>
		
		<form:form id="orderForm" action="${contextPath}/p/submitOrder" method="post">
		        <input id="basketStr" name="basketStr" value="${shopCartItems}" type="hidden"/>
		        <input id="remarkText" name="remarkText" type="hidden" />
		        <input id="invoiceIdStr" name="invoiceId" type="hidden" />
		        <input id="payManner" name="payManner" type="hidden" />
		        <input id="delivery" name="delivery" type="hidden"> 
		        <input id="couponStr" name="couponStr" type="hidden"> 
		        <input name="type" type="hidden" value="${requestScope.userShopCartList.type}"/>
		        <input name="token" type="hidden" value="${SESSION_TOKEN}"/>
		</form:form>
      </div>
     
	</div>
		 <!----foot---->
		 <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
        <!----foot end---->
	
	</div>
</body>
    <script type="text/javascript">
		var contextPath = '${contextPath}';
		var data = {allCount:'${requestScope.userShopCartList.allCount}'};
   </script>
   <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
   <script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>"></script>
   <script  charset="utf-8" src="<ls:templateResource item='/resources/plugins/showLoading/js/jquery.showLoading.min.js'/>"></script> 
   <script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/orderDetails.js'/>"></script>
   <script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/addressSlide.js'/>"></script>
</html>
