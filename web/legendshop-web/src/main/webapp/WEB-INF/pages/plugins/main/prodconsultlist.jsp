<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
   <!----------------咨询------------------------->
   <c:forEach items="${requestScope.prodCousultList}" var="consult" varStatus="status">
          <div class="consult-item <c:if test='${status.count % 2 eq 0 }'>odd</c:if>">
              <div class="user"><span class="u-name">网友：${consult.nickName}</span>                                    
              <span class="u-level"><font style="color:#999999"> ${consult.gradeName}</font></span>
               <span class="date-ask"><fmt:formatDate value="${consult.recDate}" pattern="yyyy-MM-dd HH:mm" /></span>
               </div>
               <dl class="ask">
                    <dt style="width: 80px;"><b></b>咨询：</dt>
                    <dd>${consult.content}</dd>
               </dl>
               <c:if test="${consult.answer != null}">
               <dl class="answer">
               		<div class="user"><span class="u-name">${consult.ansUserName}</span>                                    
	                <span class="date-ask"><fmt:formatDate value="${consult.answertime}" pattern="yyyy-MM-dd HH:mm" /></span>
	                <dl class="ask">
                        <dt style="width: 80px;">商家回复：</dt>
                        <dd style="color:#333">${consult.answer}</dd>
               		</dl>
                </dl>
                </c:if>
            </div>
   </c:forEach>
   
	<div class="clear"></div>
	<div style="margin-top:10px;" class="page clearfix">
	   <div class="p-wrap">
	      	<ls:page pageSize="${prodCousultPageSize}"  total="${prodCousultTotal}" curPageNO="${prodCousultCurPageNO}"  type="simple" actionUrl="javascript:consultPager"/> 
		</div>
	</div>
