<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

<div class="right">
				<div class="m m3  safe-sevi01">
						<div class="mc">
								<div class="fl">
										<h3 style="width: 700px;"><span class="ftx-02">已发送验证邮件至：</span>${userEmail}&nbsp;&nbsp;<span class="ftx-01">请立即完成验证，邮箱验证不通过则修改邮箱失败。</span></h3>
										<p class="ftx-03">验证邮件24小时内有效，请尽快登录您的邮箱点击验证链接完成验证。</p>
										<c:if test="${officialEmail != null }">
										<div class="btns"><a class="btn btn-3" href="${officialEmail }" target="_blank"><s></s>查看验证邮件</a></div>
										</c:if>
								</div>
						</div>
				</div>
				<div class="m m7">
						<div class="mt">
								<h3>没收到邮件？</h3>
						</div>
						<div class="mc">
							1.请检查您的垃圾箱或者广告箱，邮件有可能被误认为垃圾或者广告邮件；<br>
							2.如果垃圾箱或广告箱也没收到,请尝试将 ${applicationScope.SYSTEM_CONFIG.supportMail} 添加为收件人白名单（<a  href="${contextPath}/news/230" target="_blank"><s></s>可参考邮箱白名单设置</a>）
                        </div>
				</div>
 </div>
 
 <script type="text/javascript">
	
 </script>