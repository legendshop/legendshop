<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>热卖商品-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-prodAnalysis${_style_}.css'/>" rel="stylesheet">
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
	<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
<div id="Content" class="w1190">
	<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">热卖商品</span>
				</p>
			</div>
     <%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
	 <div class="seller-prodAnalysis" >
		<div class="seller-com-nav" >
			<ul>
				<li ><a href="${contextPath}/s/shopReport/prodSalesTrend">商品详情</a></li>
				<li class="on"><a href="javascript:void(0);">热卖商品</a></li>
			</ul>
		</div>
		<div class="alert" style="margin-bottom:20px;">
			<ul class="mt5">
				<li>1、符合以下任何一种条件的订单即为有效订单：1）采用在线支付方式支付并且已付款；</li>
				<li>2、以下列表为从昨天开始最近30天有效订单中的所有商品数据</li>
		        <li>3、近30天下单商品数：从昨天开始最近30天有效订单的某商品总销量</li>
		        <li>4、近30天下单金额：从昨天开始最近30天有效订单的某商品总销售额</li>
	        </ul>
		</div>
		<form:form action="${contextPath}/s/shopReport/prodAnalysis" method="post" id="from1">
			<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
		</form:form>
		<input type="hidden" id="prodCountsJson" value='${prodCountsJson}'/>
		<input type="hidden" id="prodCashJson" value='${prodCashJson}'/>
		<input type="hidden" id="resultSize" value='${resultSize}'/>
		<div id="main" style="margin-left: 10px;width:98%;height:400px"></div>
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="prodAnalysis-table sold-table" style="margin-top: 20px;">
               <tr class="prodAnalysis-tit">
                 <th>排行</th>
                 <th>商品</th>
                 <th>商品名称</th>
                 <th>商品销量</th>
                 <th>销售金额</th>
               </tr>
               <c:choose>
		           <c:when test="${empty requestScope.list}">
		              <tr>
					 	 <td colspan="20">
					 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
					 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
					 	 </td>
					 </tr>
		           </c:when>
		           <c:otherwise>
		           	    <c:forEach items="${requestScope.list}" var="prod" varStatus="index">
			                <tr class="detail">
			              	<td>${index.count}</td>
			              	<td><a href="${contextPath}/views/${prod.prodId}" target="_blank"><img src="<ls:images item='${prod.prodPic}' scale='3'/>" /></a></td>
			                  <td><a href="${contextPath}/views/${prod.prodId}" target="_blank">${prod.prodName}</a></td>
			              	<td>${prod.buys}</td>
			              	<td >${prod.totalCash}</td>
			               </tr>
			           	 </c:forEach>
		           </c:otherwise>
		        </c:choose>
    	</table>
		<div style="margin-top:10px;" class="page clearfix">
   			 <div class="p-wrap">
          		 <c:if test="${toolBar!=null}">
					<span class="p-num">
						<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
					</span>
				</c:if>
   			 </div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<script src="<ls:templateResource item='/resources/plugins/ECharts/dist/echarts.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/templets/js/multishop/prodAnalysis.js'/>" type="text/javascript"></script>
<script type="text/javascript">
var contextPath = '${contextPath}';
</script>
</body>
</html>
