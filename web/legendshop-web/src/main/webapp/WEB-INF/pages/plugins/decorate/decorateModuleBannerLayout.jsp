<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/public.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/decorate.css'/>" rel="stylesheet" />

<div class="white_box" style="padding-top: 20px;">
<form:form  method="post" action="" name="BannerForm" id="BannerForm">
  <div class="fiit_side_box">
    <!--幻灯编辑-->
    <div class="fiit_side_tab">
      <ul>
                 <c:choose>
                     <c:when test="${empty layoutBanners}">
                        <li count="1" class="this"><a href="javascript:void(0);">幻灯</a></li>
					 </c:when>
					 <c:otherwise>
					     <c:forEach items="${layoutBanners}" var="banner" varStatus="s">
				               <li count="${s.count}" <c:if test="${s.first}">class="this"</c:if> ><a href="javascript:void(0);">幻灯${s.count}</a></li>
					      </c:forEach>
					 </c:otherwise>
			</c:choose>
          
        </ul>
        <a onclick="addSlide();" href="javascript:void(0);" class="fiit_add_side"><b>+</b> 添加幻灯</a> </div>
        <div class="fiit_banner"> 
            <c:choose>
                     <c:when test="${empty layoutBanners}">
                          <dl count="1">
					        <dt><span class="fl">幻灯1：</span><span class="fr"></span></dt>
					        <dd> <span class="fl">上传图片：</span> <span class="fr">
					          <div class="file_box">
					            <input type="hidden" id="img_files_1" name="img_files"  >
					            <input type="text" id="img_file_1_show" class="ip180">
					            <input type="button" value="浏览..." class="btn">
					            <input type="file" count="1" name="img_file_1" id="img_file_1" class="file">
					          </div>
					            <span style="color: red;">不限制图片尺寸</span>
					          </span>
					          </dd>
					        <dd><span class="fl">图片：</span><span id="slide_img_1" class="fr img">
					        </span></dd>
					        <dd><span class="fl">图片url：</span><span class="fr">
					          <input type="text" value=""  placeholder="http://"  name="urls" id="url_1" class="ip300">
					          </span></dd>
					      </dl>
					 </c:when>
					 <c:otherwise>
					      <c:forEach items="${layoutBanners}" var="banner" varStatus="s">
				                   <dl <c:if test="${!s.first}">style="display: none;"</c:if>  count="${s.count}">
							         <dt>
							            <span class="fl">幻灯${s.count}：</span>
							            <c:if test="${!s.first}">
							              <span class="fr">
							                <a onclick="removeslide()" href="javascript:void(0);"><i></i>删除</a> 
							              </span>
							            </c:if>
							         
							         </dt>
							         <dd> <span class="fl">上传图片：</span> <span class="fr">
							          <div class="file_box">
							            <input type="hidden" id="img_files_${s.count}" name="img_files" value="${banner.imageFile}" >
							            <input type="text" id="img_file_${s.count}_show" class="ip180">
							            <input type="button" value="浏览..." class="btn">
							            <input type="file" count="${s.count}" name="img_file_${s.count}" id="img_file_${s.count}" class="file">
							          </div>
							          </span> </dd>
							         <dd><span class="fl">图片 ：</span><span id="slide_img_${s.count}" class="fr img">
							            <img id="${banner.id}" src="<ls:photo item='${banner.imageFile}' />"></span>
							         </dd>
							         <dd><span class="fl">图片url：</span><span class="fr">
							          <input type="text"   placeholder="http://" value="${banner.url}" name="urls" id="url_${s.count}" class="ip300">
							          </span>
							         </dd>
							      </dl>
					     </c:forEach>
					 </c:otherwise>
			</c:choose>
         </div>
  </div>
  <div class="fiit_save_box">
    <input type="hidden" value="loyout_module_banner" name="layoutModuleType" id="layoutModuleType">
    <input type="hidden" value="${layoutParam.layoutId}" name="layoutId" id="layoutId">
    <input type="hidden" value="${layoutParam.layout}" name="layout" id="layout">
    <input type="hidden" value="${layoutParam.divId}" name="divId" id="divId">
    <input type="hidden" value="${layoutParam.layoutDiv}" name="layoutDiv" id="layoutDiv">
    <input type="button" id="save" onclick="saveSlide();" value="保存">
  </div>
 </form:form>
</div>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script src="<ls:templateResource item='/resources/templets/js/multishop/decorateModuleBanner.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/ajaxfileupload.js'/>" type="text/javascript"></script>
 <script type="text/javascript">
	var contextPath = '${contextPath}';
	var photoPath ="<ls:photo item='' />";
   </script>