<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>二级域名-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-prod${_style_}.css'/>" rel="stylesheet">
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-sec.css'/>" rel="stylesheet">
	<style type="text/css">
		.search-div{
			width:300px;
		}
	</style>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">域名配置</span>
				</p>
			</div>
			<%@ include file="included/sellerLeft.jsp" %>
			<c:if test="${secDomainNameSetting.isEnable eq true}">
				<div class="seller-selling">
					<div>
						您已注册了以下域名: 
						<table class="selling-table">
							<tr class="selling-tit">
								<td>二级域名</td>
								<td>注册时间</td>
							</tr>
							<c:choose>
								<c:when test="${empty shopDetail.secDomainName}">
									<tr><td colspan='7' height="60">您还没有注册过二级域名呢,赶快免费注册一个吧!</td></tr>
								</c:when>
								<c:otherwise>
							        <tr class="detail">
										<td>${shopDetail.secDomainName }</td>
										<td><fmt:formatDate value="${shopDetail.secDomainRegDate }" type="both"/></td>
									</tr>
							    </c:otherwise>
							</c:choose>
						</table>
					</div>
					
					<div style="margin:15px 3px;;">
						注: 每个用户最多只能注册
						<span style="color:#e5004f">1</span>
						个二级域名,从注册时间算起
						<span style="color:#e5004f">${secDomainNameSetting.modifyCycle }</span>天后可以重新注册!
					</div>
					<c:if test="${isAllowModify eq true}">
						<div class="seller-sec">
							<div class="sec-state">
								<div class="che-ord">
									<input id="secDomainName" name="secDomainName" type="text"  placeholder="请输入您想注册的域名"/>
									<a href="javascript:search();" >搜索域名</a>
								</div>
							</div>
							<div class="sec-war">
								<span id="checkResult"></span>
							</div>
						</div>
					</c:if>
				</div>
			</c:if>
			<c:if test="${secDomainNameSetting.isEnable eq false}">
				<script type="text/javascript">
					alert("对不起,系统暂没开放二级域名功能!");
				</script>
			</c:if>
		</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/domainSet.js'/>"></script>
	<script type="text/javascript">
		var contextPath = '${contextPath}';
		var sizeRange = '${secDomainNameSetting.sizeRange}';
		var sizeRanges = sizeRange.split("-");
		var min = sizeRanges[0];
		var max = sizeRanges[1];
	</script>
</body>
</html>