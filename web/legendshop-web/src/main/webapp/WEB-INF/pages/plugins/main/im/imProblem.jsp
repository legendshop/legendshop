<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>

<!-- 常见问题 -->
<div class="common-problem">
	<div class="problem-box">
		<div class="problem">
			<div class="txt">我的地区支持哪些配送服务？</div>
			<div class="arr"><img src="<ls:templateResource item='/resources/templets/images/arrow.png'/>" alt=""></div>
		</div>
		<div class="answer">
			<p class="ans-p">配送到：<select class="address"><option>广东广州市海珠区</option></select></p>
		</div>
	</div>
	<div class="problem-box">
		<div class="problem">
			<div class="txt">怎么取消订单？</div>
			<div class="arr"><img src="<ls:templateResource item='/resources/templets/images/arrow.png'/>" alt=""></div>
		</div>
		<div class="answer">
			<p class="ans-p">取消未收货订单，您可进入<a href="#" class="ans-a">“我的订单”</a>，找到要取消的订单，点击“取消按钮”即可</p>
		</div>
	</div>
	<div class="problem-box click">
		<div class="problem">
			<div class="txt">怎么知道商品有没有优惠活动/赠品？</div>
			<div class="arr"><img src="<ls:templateResource item='/resources/templets/images/arrow.png'/>" alt=""></div>
		</div>
		<div class="answer">
			<p class="ans-p">您可以在商品页查看商品参与的优惠活动以及赠送的赠品。一般商品促销时，赠品数量有限，先到先得，送完为止，具体以您提交订单信息为准。非自营商品，还请您在商品页联系商家客服进行具体咨询哦~</p>
		</div>
	</div>
</div>
<!-- /常见问题 -->