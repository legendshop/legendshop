<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<html>
<head>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/public.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/store_black.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/decorate.css'/>" rel="stylesheet" />
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
    <title>商家首页-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
</head>
<style>
  .layout_one{margin-top:0;}
  .module_wide{background-color:#ffffff !important;border:none !important;}
</style>

<body  class="graybody">
	<div id="doc">
	
	
	    <!--顶部menu-->
			 <div id="hd">
			      <!--hdtop 开始-->
			       <%@ include file="/WEB-INF/pages/plugins/main/home/header.jsp" %>
			      <!--hdtop 结束-->
			      
			       <!-- hdmain 开始-->
                      <%@include file="/WEB-INF/pages/plugins/main/home/hdShopMain.jsp" %>
			      <!-- hdmain 结束-->
			 </div>
			 
	    
	    <div id="bd">
	      
	      
	         <div 
			    style='<c:if test="${not empty shopDecotate.bgImgId}">
			        background-image:url("<ls:photo item='${shopDecotate.bgImgId}' />"); 
			    </c:if>
			    <c:if test="${not empty shopDecotate.bgColor}">
			          background-color:${ shopDecotate.bgColor };
			    </c:if>
			     <c:if test="${not empty shopDecotate.bgStyle}">
			         background-repeat:${ shopDecotate.bgStyle };
			     </c:if> margin-bottom: 20px;'   class="shop_main">
			 
		          <!--店铺头部-->
			    <div style="display:none;"  id="store_info" class="store_head">
		             <div class="store_top">
		               <div class="store_main">
		                 <!--店铺信息行-->
				     <div class="store_head_right">
                    <div id="store_head_box" class="store_head_box">
						<a href="javascript:void(0);"><div class="store_name">${shopDecotate.shopDefaultInfo.siteName}</div></a>
							<div class="shopscores">
							<!-- 	<i class="smill"></i>
								<b class="scores_scroll">
									<span class="shop_scroll_gray"> </span>
									<span style="width:10%" class="scroll_red"></span>
								</b> -->
								 <li><span style="float:left;"></span> <span class="scores_scroll"> <span class="scroll_gray"></span> <span class="scroll_red" style="width: 20%"></span> </span><font id="shopScoreNum" class="hide">${shopDecotate.shopDefaultInfo.credit}</font></li>
							</div>
						</div>
					<div id="store_level" style="display:none" class="store_level">
						<ul style="display:" class="pro_shop_date">
							 <li><span style="float:left;">综合评分：</span> <span class="scores_scroll"> <span class="scroll_gray"></span> <span class="scroll_red" style="width: 20%"></span> </span><font id="shopScoreNum" class="hide">${shopDecotate.shopDefaultInfo.credit}</font></li>
						</ul>
						<div class="store_detail">
							<ul>
								<li><span class="company_name">店铺名称：</span><span
									class="red">${empty shopDecotate.shopDefaultInfo.siteName?'暂无':shopDecotate.shopDefaultInfo.siteName}</span>
								</li>
								<!-- 
								<li><span class="company_name">联系电话：</span><span
									class="company_r">${shopDecotate.shopDefaultInfo.contactMobile}</span>
								</li>
								 -->
								<li><span class="company_name">店铺地址：</span><span
									class="company_r">${empty shopDecotate.shopDefaultInfo.shopAddr?'暂无':shopDecotate.shopDefaultInfo.shopAddr}</span>
								</li>
								<li><span class="company_name">店铺简述：</span><span
									class="company_r">${empty shopDecotate.shopDefaultInfo.briefDesc?'暂无':shopDecotate.shopDefaultInfo.briefDesc}</span>
								</li>
								<li>
									<span id="clickMessage"></span>
								</li>
							</ul>
						</div>
					</div>
					<div class="service"><a target="_blank" href="${contextPath}/p/im/customerShop?shopId=${shopDecotate.shopDefaultInfo.shopId}"></a></div>
		     </div>
		                    
		                </div>
		            </div>
		      </div>
			   <!--店铺头部-->
			   
			      <!--banner行-->
			   <div  style="background:url(<ls:photo item='${shopDecotate.shopDefaultInfo.bannerPic}' />) no-repeat top center ;display:none; " id="store_banner" class="store_logo">
		         <%--     <div class="main">
		               <c:if test="${not empty shopDecotate.shopDefaultInfo.shopPic}">
		                 <span style="clear:both;">
		                   <img style="min-width: 540px;height: 50px;" src="<ls:photo item='${shopDecotate.shopDefaultInfo.shopPic}' />">
		                 </span>
		               </c:if>
		             </div> --%>
				</div>
			     <!--banner行-->
			     
			     	   
			   
			    <!--导航行-->
			      <div id="store_nav" style="display: none;" class="store_nav_width">
					  <div class="main">
					    <div class="store_nav">
					      <ul>
					        <li>   
					         <a id="current" href="javascript:void(0);">店铺首页</a></li>
					         <c:forEach items="${shopDecotate.shopDefaultNavs}" var="nav" varStatus="s">
					             <li ><a 
												     <c:if test="${nav.winType=='new_win'}">
												       target="_blank"
												     </c:if> href="${nav.url}">${nav.title}</a>
			                    </li>
					         </c:forEach>
					       </ul>
					    </div>
					  </div>
				 </div>
			    <!--导航行-->
			    
			    <!--默认幻灯-->
			     <div id="store_slide"  style="display: none;"  >
					<div id="default_fullSlide" class="fullSlide">
						<div id="default_bd" class="bd">
							<ul>
							   <c:forEach items="${shopDecotate.shopDefaultBanners}" var="banner" varStatus="b">
							     	<li id="${banner.id}" >
								     <a href="${banner.url}" style="text-align:center;">
								       <img  src="<ls:photo item='${banner.imageFile}' />">
								     </a>
								    </li>
							   </c:forEach>
							</ul>
						</div>
						<div id="default_hd" class="hd">
							<ul>
							    <c:if test="${fn:length(shopDecotate.shopDefaultBanners)>1}">
							      <c:forEach items="${shopDecotate.shopDefaultBanners}" var="banner" varStatus="b">
							       	<li <c:if test="${b.count==1}">class="on"</c:if> >${b.count}</li>
							       </c:forEach>
							   </c:if>
							</ul>
						</div>
					</div>
				</div>
			    <!--默认幻灯-->
			    
			    <!--装修布局内容-->
			          
			     <!--布局上-->
			      <c:forEach items="${shopDecotate.topShopLayouts}" var="layout_top" varStatus="l">
			         <c:if test="${not empty layout_top.layoutModuleType}">
			              <div  class="layout_one" location_mark="${layout_top.layoutId}" location="true" >
			                    <c:choose>
			                 	     <c:when test="${layout_top.layoutModuleType=='loyout_module_hot'}">
			                 	        <div layout="layout0"  id="content" type="${layout_top.layoutModuleType}" mark="${layout_top.layoutId}" url=""  style="text-align: -webkit-center;">
			                 	     </c:when>
						            <c:otherwise>   
						               <div  layout="layout0"  id="content" type="${layout_top.layoutModuleType}" mark="${layout_top.layoutId}" url=""  style="text-align: -webkit-center;">
						          </c:otherwise> 
					            </c:choose>
				                 </div>  
				          </div>
			         </c:if>
			      </c:forEach>
			     <!--布局上-->
			       <div style="clear: both;"></div> 
			       
			       <!--布局中--> 
			       <div id="main_layout" class="main ui-sortable">
			                <%@ include file="/WEB-INF/pages/plugins/decorate/decorateMainLayout.jsp" %>
			       </div> 
			      <!--布局中-->  
			      
			      
			      <div style="clear: both;"></div> 
			        
			      <!--布局下-->
			      <c:forEach items="${shopDecotate.bottomShopLayouts}" var="layout_bottom" varStatus="l">
			            <c:if test="${not empty layout_bottom.layoutModuleType}">
			                <div class="layout_one" location_mark="${layout_bottom.layoutId}" location="true">
						          <div style="text-align:center" layout="layout1" id="content" type="${layout_bottom.layoutModuleType}" mark="${layout_bottom.layoutId}" url="" >
						                  <h3 class="module_wide">
						                  </h3> 
						          </div>  
				             </div>
			            </c:if>
			            <div style="clear: both;"></div> 
			      </c:forEach>
			      <!--布局下-->
			       
			    <!--装修布局内容-->
		</div>
		
		 <!--返回最顶部-->
			 <div style="position: fixed; width: 40px; height: 118px; float: left; right: 20px; bottom: 150px; display: block;z-index:1000;" id="back_box">
			  <div class="back_index"><a title="返回主页" target="_blank" href="${contextPath}"></a></div>
			  <div class="back_top"><a target="_self" title="返回顶部" style="display: block;" bosszone="hometop" href="#" id="toTop"></a></div>
			</div>
			    
	    
		</div>
        
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
		
	</div>
</body>
<script src="${contextPath}/resources/templets/js/multishop/jquery.SuperSlide.2.1.1.js" type="text/javascript"></script>
<script src="${contextPath}/resources/templets/js/multishop/decoratePreview.js" type="text/javascript"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/score.js'/>"></script>
<script type="text/javascript">
	let shopId = ${shopDecotate.shopId};
	var shopType = null;
$(function(){
	
	$.each($(".scroll_red"),function(){
		var width=parseInt($(this).parents("li").find("font").text())*2+"0%";
		$(this).css("width",width);
	})
	$(".scroll_red").css("top","4px");
	$(".scroll_gray").css("top","4px");

})
      var contextPath = '${contextPath}';
		 var color_block = "${shopDecotate.bgColor}";
		
		 var soc="${shopDecotate.shopDefaultInfo.credit}";
		 var element=$(".scroll_red");
       
     jQuery(document).ready(function(){
         var screen_width=window.screen.width ;
         var p_height = 0;
         $("#default_bd ul li ").find("img").each(function (){
             var left_width=screen_width/2;
             $(this).attr("style","position: absolute;left: 50%;width: "+screen_width+"px;margin-left: -"+left_width+"px;");
             if(p_height==0){
	             p_height = $(this).height();
             }
         });
           $("#default_fullSlide").css("height",p_height+"px");
         
          $(".layout_one div[type=loyout_module_hot]").each(function (){
             var left_width=screen_width/2;
             $(this).attr("style","position: absolute;left: 50%;width: "+screen_width+"px;margin-left: -"+left_width+"px;");
         });
         
     });
         
    progress(soc,element);
     <c:choose>
        <c:when test="${preview eq true}">
	       var loadModuleUrl=contextPath+"/s/shopDecotate/lodingLayouModule";
        </c:when>
        <c:when test="${empty preview ||  preview eq false }">
	       var loadModuleUrl=contextPath+"/loading/module/${shopDecotate.shopId}";
        </c:when>
    </c:choose>
        var preview="${preview}";
    	   	<c:if test="${shopDecotate.isInfo==1}">
    	    jQuery("#store_info").show();
    	</c:if>
    	 <c:if test="${shopDecotate.isNav==1}">
    	    jQuery("#store_nav").show();
    	 </c:if>
    	 <c:if test="${shopDecotate.isSlide==1}">
    	    jQuery("#store_slide").show();
    	 </c:if>
    	 <c:if test="${shopDecotate.isBanner==1}">
    	   jQuery("#store_banner").show();
    	 </c:if>
    	  //jQuery("#store_banner").hide();
	</script>
</html>
	