<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<li>
	<div id="J_SellProperties" class="sku-style">
		<label class="sku-title" id="prod-attr-tab">商品属性：</label>
		<div class="sku-wrap">
			<input type="hidden" id="isAttrEditable" value=${productDto.isAttrEditable}>
			<c:forEach items="${productDto.prodPropDtoList}" var="propDto" varStatus="status">
				<div class="sku-group" id="sku-group-${status.index}" <c:if test="${propDto.isCustom}">type="custom"</c:if>>
					<c:choose>
						<c:when test="${propDto.isCustom}">
							<label class="sku-label " id="skuProp_${propDto.propId}" propName="${propDto.propName}" propType="${propDto.type}">${propDto.propName}：</label>
							<a href="javascript:void(0);" style="color:#005ea7" onclick="removeAttributeOption(this);">X</a>
						</c:when>
						<c:otherwise>
							<label class="sku-label " id="skuProp_${propDto.propId}" propName="${propDto.propName}" propType="${propDto.type}">${propDto.propName}：</label>
						</c:otherwise>
					</c:choose>
					<div class="sku-box  ">
						<ul class="sku-list">
							<c:forEach items="${propDto.productPropertyValueList}" var="propValue" varStatus="status">
								<li class="sku-item"><c:choose>
										<c:when test="${propValue.isSelected}">
											<input type="checkbox" id="sku_${propValue.valueId}" valueId="${propValue.valueId}" propId="${propValue.propId }" value="${propValue.name}" class="J_Checkbox" checked="checked">
											<c:if test="${not empty propValue.pic}">
												<label><img src="<ls:images item="${propValue.pic}" scale="3"/>" style="width: 15px; height: 15px;" /> </label>
											</c:if>
											<label title="${propValue.name}" class="labelname" id="labelInput_${propValue.valueId}"><input type="text" value="${propValue.name}" style="width: 60px; margin-top: 7px;"
												onBlur="propValueAlias(this);" /> </label>
											<label title="${propValue.name}" class="labelname" id="labelName_${propValue.valueId}" style="margin: auto;display:none;">${propValue.name}</label>
										</c:when>
										<c:otherwise>
											<input type="checkbox" id="sku_${propValue.valueId}" valueId="${propValue.valueId}" propId="${propValue.propId }" value="${propValue.name}" class="J_Checkbox">
											<c:if test="${not empty propValue.pic}">
												<label><img src="<ls:images item="${propValue.pic}" scale="3"/>" style="width: 15px; height: 15px;" /> </label>
											</c:if>
											<label title="${propValue.name}" class="labelname" style="display:none" id="labelInput_${propValue.valueId}"><input type="text" value="${propValue.name}"
												style="width: 60px; margin-top: 7px;" onBlur="propValueAlias(this);" /> </label>
											<label title="${propValue.name}" class="labelname" id="labelName_${propValue.valueId}" style="margin: auto;">${propValue.name}</label>
										</c:otherwise>
									</c:choose>
								</li>
							</c:forEach>
						</ul>
					</div>
				</div>
			</c:forEach>

			<div id="user_prop_List"></div>
			<div id="J_CustomSKUList" class="sku-customlist">
				<div class="sku-optbar">
					<a href="javascript:void(0);" onclick="selectAttribute(this,1);">选择模板</a>
				</div>
				<div class="clear"></div>
				<div class="sku-group sku-custom">
					<label class="sku-label">
						<div class="J_CustomCaption input-ph-wrap">
							<label class="label" style="display: none;">商品属性名称</label> <input type="text" id="paramKey" class="sku-caption text" maxlength="10">：
						</div> </label>
					<div class="sku-box">
						<ul class="sku-list">
							<li><input type="text" id="paramValue0" class="text" maxlength="10"></li>
							<li><input type="text" id="paramValue1" class="text" maxlength="10"></li>
							<li><input type="text" id="paramValue2" class="text" maxlength="10"></li>
							<li><input type="text" id="paramValue3" class="text" maxlength="10"></li>
							<li><input type="text" id="paramValue4" class="text" maxlength="10"></li>
							<li><input type="text" id="paramValue5" class="text" maxlength="10"></li>
							<li><input type="text" id="paramValue6" class="text" maxlength="10"></li>
							<li><input type="text" id="paramValue7" class="text" maxlength="10"></li>
							<li><input type="text" id="paramValue8" class="text" maxlength="10"></li>
							<li><input type="text" id="paramValue9" class="text" maxlength="10"></li>
							<li><input type="text" id="paramValue10" class="text" maxlength="10"></li>
							<li><input type="text" id="paramValue11" class="text" maxlength="10"></li>
						</ul>
						<div class="sku-custom-operations">
							<a title="删除" class="sku-delgroup J_SKUDelGroup" href="javascript:void(0);" onclick="delProperties(this);">X</a>
						</div>
					</div>
				</div>
			</div>

			<div class="sku-custombar">
				<button type="button" onclick="newProperties(this);" id="newPropertyButton">新建商品属性</button>
				<button type="button" onclick="toSaveAttribute(this);">保存模板</button>
				<button type="button" onclick="applicationProperty();" id="appPropButton">商品属性应用</button>
				<span class="sku-tip">商品属性总数最多4组</span>
			</div>
		</div>

		<table id="colorTable" class="prop-table-css" <c:if test="${empty productDto.propImageDtoList}">style='display:none;'</c:if>>
			<thead>
				<tr>
					<th style="width: 90px;">颜色</th>
					<th>图片（无图片可不填）(双击图片可删除)</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${productDto.propImageDtoList}" var="imageDto" varStatus="status">
					<tr id="colorId_${imageDto.productPropertyValueDto.valueId}">
						<td propId="${imageDto.productPropertyValueDto.propId}" propValueId="${imageDto.productPropertyValueDto.valueId}" propValueName="${imageDto.productPropertyValueDto.name}"
							class="pv_${imageDto.productPropertyValueDto.valueId}">${imageDto.productPropertyValueDto.name}</td>
						<c:choose>
							<c:when test="${not empty imageDto.prodPropImageList}">
								<td><a class="propImagebutton" href="javascript:void(0);" style="float:left;margin-right: 5px;"> <span class="btn-txt">文件上传</span> <input type="file"
										onchange="javascript:uploadPropImage(this);" id="valueImage_${status.index}" name="valueImageFiles" class="propImageInput" multiple="multiple"> </a> <a class="propImagebutton"
									href="javascript:void(0);" style="float:left;margin-right: 10px;"> <span class="btn-txt">图片空间</span> <input type="button" onchange="javascript:void(0);" id="imageSpace_${status.index}"
										class="propImageInput"> </a>

									<ul style="display: inline;width: 400px;" id="valueImageUL_${status.index}">
										<c:forEach items="${imageDto.prodPropImageList}" var="prodPropImage">
											<li class="propValueImageLi" style="margin-right: 10px;"><input propId="${imageDto.productPropertyValueDto.propId}" propValueName="${imageDto.productPropertyValueDto.name}"
												propvalueId="${imageDto.productPropertyValueDto.valueId}" value="${prodPropImage.url}" name="propValueImage" type="hidden" /> <img class="propValueImg"
												src="<ls:photo item='${prodPropImage.url}'/>" ondblclick="removeValueImage(this);" />
											</li>
										</c:forEach>
									</ul>
								</td>
							</c:when>
							<c:otherwise>
								<td><a class="propImagebutton" href="javascript:void(0);" style="float:left;margin-right: 5px;"> <span class="btn-txt">文件上传</span> <input type="file"
										onchange="javascript:uploadPropImage(this);" id="valueImage_${status.index}" name="valueImageFiles" class="propImageInput" multiple="multiple"> </a> <a class="propImagebutton"
									href="javascript:void(0);" style="float:left;margin-right: 10px;"> <span class="btn-txt">图片空间</span> <input type="button" onchange="javascript:void(0);" id="imageSpace_${status.index}"
										class="propImageInput"> </a>

									<ul style="display: inline;width: 400px;" id="valueImageUL_${status.index}"></ul>
								</td>
							</c:otherwise>
						</c:choose>
					</tr>
				</c:forEach>
			</tbody>
		</table>
</li>
