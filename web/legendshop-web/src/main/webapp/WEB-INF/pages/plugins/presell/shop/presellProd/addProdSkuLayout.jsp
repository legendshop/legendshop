<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<head>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/presell/add-prod-sku-layout.css'/>" rel="stylesheet">
	<style type="text/css"></style>
</head>
<body>
	<div class="main">
		  <!-- 搜索区域 -->
		  <div class="search-warp">
			 <form:form action="${contextPath}/s/presellProd/addProdSkuLayout" id="form1" method="GET">
			 	<input type="hidden" id="curPageNO" name="curPageNO" value="1" />
			 		<div class="search-box">
						<input type="text" name="name" id="name" maxlength="20" value="${prod.name}" size="20" placeholder="请输入商品名称" />
						<input type="submit" value="搜索" />
					</div>
			 </form:form>
		  </div>
		  
		  <!-- 商品列表 -->
	      <div class="prod-list-warp" >
	      	  <h3>商品列表</h3>
		      <ul class="prod-list-box">
				<c:forEach items="${requestScope.list}" var="prod" varStatus="status">
				   <li> 
				        <a class="prod-box"  onclick="loadProdSku('${prod.prodId}',this);" href="javascript:void(0);" title="${prod.name}">
					        <img class="prod-img" src="<ls:images item='${prod.pic}' scale='3' />" alt="${prod.name}">
					        <span class="prod-name">${prod.name}</span>
				       	</a> 
				  </li>
				</c:forEach>
				<c:if test="${empty requestScope.list}">
				    <li>暂无商品信息</li>
				</c:if>
			</ul>
	      	<div class="prod-list-page">
				<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
			</div>
	      </div>
	      
	      <!--  单品信息列表 -->
	      <div class="prod-sku-list">
	      	<iframe id="prodSkuListFrame" class="prod-sku-list-frame" frameborder="0" ></iframe>
	      </div>
      </div>
      <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/addProdSkuLayout.js'/>"></script>
	<script type="text/javascript">
	 	var contextPath="${contextPath}";
	</script>
</body>
