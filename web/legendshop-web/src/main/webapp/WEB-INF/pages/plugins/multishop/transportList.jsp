<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>运费模板-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
     <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
     <style>
     	table th,td{
			text-align: center;
     	}
     </style>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">运费模板</span>
				</p>
			</div>
			<%@ include file="included/sellerLeft.jsp" %>
		<div id="Content" style="width:960px;float:right;margin-top:20px;">
			 <div class="seller-com-nav">
				<ul>
					<li class="on" id="transportList"><a href="#">运费模板列表</a></li>
			        <li id="transportConfigure" onclick="javascript:transportList.loadTransport();"><a href="#">运费模板设置</a></li>
				</ul>
			 </div>
			<form:form action="${contextPath}/s/transport" id="myForm">
				<div class="sold-ser sold-ser-no-bg clearfix">
					<input type="hidden" id="curPageNO" name="curPageNO" value="1"/>
					<div class="fr">
						<div class="item">
							运费模板名称：
							<input type="text" style="width:300px;" class="item-inp" id="transName" name="transName" value="${transName}" placeholder="请输入运费模板名称">
							<input type="submit" class="btn-r" value="查询">
						</div>
					</div>
				</div>
			</form:form>
			
		     <c:choose>
		     <c:when test="${fn:length(list)>0 }">
		     <c:forEach items="${requestScope.list}" var="transport" varStatus="status">
				<table  style="width:100%;margin-bottom:0;text-align: center;" cellspacing="0" cellpadding="0" class="buytable transport-tab sold-table">
						<tr>
						 <td>${transport.transName}</td>
						 <td style="text-align:right;padding:8px;">
						 	添加运费时间：<fmt:formatDate value="${transport.recDate}" pattern="yyyy-MM-dd HH:mm" />&nbsp;&nbsp;
						 	<a href="javascript:void(0)" onclick="javascript:transportList.loadById(${transport.id});"> &nbsp;&nbsp;&nbsp;&nbsp;编辑&nbsp;&nbsp; |&nbsp;&nbsp; </a>
		<!-- 				 	<a href="javascript:void(0)" onclick="javascript:transportList.deleteById(${transport.id});">删除</a> -->
						 	<a href='javascript:deleteById("${transport.id}")' title="删除">删除</a>
						 </td>
						</tr>
				</table>
			
				<table id="item"  style="width:100%;margin-bottom: 15px;text-align: center;border-top: none;" cellspacing="0" cellpadding="0" class="buytable sold-table">
				<tbody>
				<c:choose>
					<c:when test="${transport.transType ==1}">
							<tr>
							<th width="100">配送方式</th>
							<th >配送区域</th>
							<th width="80">首件（件）</th>
							<th width="80">运费（￥）</th>
							<th width="80">续件（件）</th>
							<th width="80">运费（￥）</th>
							</tr>
					</c:when>
					<c:when test="${transport.transType ==2}">
							<tr>
							<th width="100">配送方式</th>
							<th >配送区域</th>
							<th width="80">首重（kg）</th>
							<th width="80">运费（￥）</th>
							<th width="80">续重（kg）</th>
							<th width="80">运费（￥）</th>
							</tr>
					</c:when>
					<c:otherwise>
							<tr>
							<th width="100">配送方式</th>
							<th >配送区域</th>
							<th width="80">首体积（m³）</th>
							<th width="80">运费（￥）</th>
							<th width="80">续体积（m³）</th>
							<th width="80">运费（￥）</th>
							</tr>
					</c:otherwise>
				</c:choose>
		
			<c:forEach  items="${transport.feeList}" var="transfee" varStatus="status">
		               <tr>
		                   <td>
		                   		<c:choose>
		                   			<c:when test="${transfee.freightMode == 'mail'}">平邮</c:when>
		                   			<c:when test="${transfee.freightMode == 'express'}">快递</c:when>
		                   			<c:otherwise>EMS</c:otherwise>
		                   		</c:choose>
		                   </td>
		                   <td>
		                       <c:forEach var="city" items="${transfee.cityNameList}" varStatus="status">
		                         <c:choose>
							    	<c:when test="${city != null}">${city}</c:when>
							    	<c:otherwise>全国</c:otherwise>
							    </c:choose>
		                       ; </c:forEach>
		                   </td>
		                   <td>${transfee.transWeight}</td>
		                   <td>${transfee.transFee}</td>
		                   <td>${transfee.transAddWeight}</td>
		                   <td>${transfee.transAddFee}</td>
		               </tr>
			</c:forEach>
				</tbody>
				</table>
			</c:forEach> 
			</c:when>
		     	<c:otherwise>
		     		<div class="warning-option"><span style="text-align: center; ">没有符合条件的信息</span></div>
		     	</c:otherwise>
		     </c:choose>
			<div class="clear"></div>
			
			<div style="margin-top:10px;" class="page clearfix">
     			 <div class="p-wrap">
            		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
     			 </div>
  			</div>
		</div>
	
</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type="text/javascript">
	$(document).ready(function(e) {
		transportList.doExecute();
		
	});
	
	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("myForm").submit();
	}
	
	function deleteById(id){
		layer.confirm("确定要删除吗？", {
      		 icon: 3
      	     ,btn: ['确定','取消'] //按钮
      	   },function () {
			$.ajax({
				url : contextPath+"/s/transport/delete/"+id,
				type:'post', 
				data: {"id" : id},
				dataType : 'json', 
				async : false, //默认为true 异步
				success:function(result){
					if(result == 'OK' ){
						window.location.reload();
					}else {
						layer.msg(result, {icon: 2});
					}
				}
			});
		});
	}
	</script>
</body>
</html>