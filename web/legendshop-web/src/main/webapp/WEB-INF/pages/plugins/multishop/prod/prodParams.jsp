<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<li class="spubox clearfix" id="itemproperties"><label>商品参数：</label> 
	<span>
		<div class="module-property module-form ">
			<div class="skin">
				<dl class="hint propertymsg">
					<dd>请认真填写您的商品参数！</dd>
				</dl>
				<ul style="margin-top: 8px; margin-bottom: 8px;">
					<c:forEach items="${publishProductDto.propertyDtoList}" var="productProperty" varStatus="status">
						<li style="margin-right: 30px; margin-bottom: 10px;" id="propertyLi_${status.index}"><label class="label-title" beRequired="${productProperty.required}"
							paramName="${productProperty.propName}" paramId="${productProperty.propId}" inputProp="${productProperty.inputProp}">${productProperty.propName}： <c:if
									test="${productProperty.required}">
									<span style="color:red;">*</span>
								</c:if> </label> <span>

								<div class="J_spu-property" style="margin: 0px;">
									<ul class="J_ul-single ul-select" style="padding-left: 2px; margin-top: 3px; margin-bottom: 3px;">
										<li>
											<div class="kui-combobox" role="combobox">
												<div class="kui-dropdown-trigger">

													<input <c:if test="${not empty productProperty.valueId}">readonly="true"</c:if> style="width: 160px; height: 24px;" maxlength="50" class="kui-combobox-caption"
														value="${productProperty.name}" paramValueId="${productProperty.valueId}">
													<div class="kui-icon-dropdown"></div>

													<div id="kui-list" class="kui-list kui-listbox"
														style="display: none; <c:if test='${fn:length(productProperty.productPropertyValueList) > 16}'>overflow-y:auto; overflow-x:hidden;  width:168px; height: 400px</c:if>">
														<div role="region" class="kui-header">
															<input type="text" class="kui-dropdown-search" style="width: 120px;" maxlength="50" autocomplete="off" propId="${productProperty.propId}"
																onkeypress="if(event.keyCode==13) {searchProperty(this);return false;}" />
														</div>
														<div class="kui-body" style="width: 150px;">
															<div class="kui-list kui-group">
																<div data-value="" class="kui-option" title=""></div>
																<c:if test="${productProperty.inputProp}">
																	<div data-value="-1" class="kui-option" title="自定义">自定义</div>
																</c:if>
																<c:forEach items="${productProperty.productPropertyValueList}" var="productPropertyValue">
																	<div class="kui-option" title="${productPropertyValue.name}" data-value="${productPropertyValue.valueId}">${productPropertyValue.name}</div>
																</c:forEach>
															</div>
														</div>
													</div>
												</div>
											</div> <select class="keyPropClass" style="display: none; visibility: hidden;">
												<option selected="selected"></option>
										</select>
										</li>
									</ul>
								</div> </span>
						</li>
					</c:forEach>

					<c:if test="${not empty publishProductDto.brandList}">
						<li id="productBrand" style="margin-right: 30px; margin-bottom: 10px;"><label class="label-title">品牌：</label> <span>
								<div class="J_spu-property" style="margin: 0px;">
									<ul class="J_ul-single ul-select" style="padding-left: 2px; margin-top: 3px; margin-bottom: 3px;">
										<li>
											<div class="kui-combobox" role="combobox">
												<div class="kui-dropdown-trigger">
													<input readonly="true" style="width: 160px; height: 24px;" class="kui-combobox-caption" value="${productDto.brandName}" brandId="${productDto.brandId}">
													<div class="kui-icon-dropdown"></div>
													<div id="kui-list" class="kui-list kui-listbox"
														style="display: none; <c:if test='${fn:length(publishProductDto.brandList) > 30}'>overflow-y:auto; overflow-x:hidden;  width:168px; height: 400px</c:if>">
														<div role="region" class="kui-header">
															<input type="text" class="kui-dropdown-search" style="width: 120px;" autocomplete="off" onkeypress="if(event.keyCode==13) {searchBrand(this);return false;}" />
														</div>
														<div class="kui-body" style="width: 150px;">
															<div class="kui-list kui-group">
																<div data-value="" class="kui-option" title=""></div>
																<c:forEach items="${publishProductDto.brandList}" var="BrandDto">
																	<div class="kui-option" title="${BrandDto.brandName}" data-value="${BrandDto.brandId}">${BrandDto.brandName}</div>
																</c:forEach>
															</div>
														</div>
													</div>
												</div>
											</div> <select class="keyPropClass" style="display: none; visibility: hidden;">
												<option selected="selected"></option>
										</select>
										</li>
									</ul>
								</div> </span>
						</li>
					</c:if>

					<div class="clear"></div>
					<li id="customprop" style="height: 270px;width: 650px;">
						<div class="triggerbar expanded">
							<a id="addAttrDefined" href="javascript:openProp(this);" class="hideprop" style="display: none;"> 添加自定义参数 <i class="arrow arrow-down"></i> </a> <a id="cancelAttrDefined"
								href="javascript:closeProp(this);" class="showprop"> 取消自定义参数 <i class="arrow arrow-up"></i> </a>
							<div id="prodPropDesc" class="tpl-hint">（没有您要的参数？您可以自由填写需要的商品参数）</div>
						</div>
						<div class="custom-list-wrap">
							<div class="headbox">
								<p class="tpl-hint">最多可添加5组自定义参数（非必填），如：镜头：15cm</p>
								<div class="optbar">
									<a href="javascript:void(0);" class="J_PickTrigger picktpl" onclick="selectUserParam(this,1);">选择模板</a>
								</div>
							</div>
							<ul class="custom-list">
								<li class="hasvalue">
									<div class="input-ph-wrap">
										<label>参数名</label> <input type="text" maxlength="10" class="text text-short" id="prop0">
									</div>：
									<div class="input-ph-wrap">
										<label>参数值</label> <input type="text" maxlength="20" class="text" id="propValue0">
									</div> <a href="javascript:void(0);" class="clear" onclick="clearData(this);">清空</a>
								</li>
								<li class="">
									<div class="input-ph-wrap">
										<label>参数名</label> <input type="text" maxlength="10" class="text text-short" id="prop1">
									</div>：
									<div class="input-ph-wrap">
										<label>参数值</label> <input type="text" maxlength="20" class="text" id="propValue1">
									</div> <a href="javascript:void(0);" class="clear" onclick="clearData(this);">清空</a>
								</li>
								<li class="">
									<div class="input-ph-wrap">
										<label>参数名</label> <input type="text" maxlength="10" class="text text-short" id="prop2">
									</div>：
									<div class="input-ph-wrap">
										<label>参数值</label> <input type="text" maxlength="20" class="text" id="propValue2">
									</div> <a href="javascript:void(0);" class="clear" onclick="clearData(this);">清空</a>
								</li>
								<li class="">
									<div class="input-ph-wrap">
										<label>参数名</label> <input type="text" maxlength="10" class="text text-short" id="prop3">
									</div>：
									<div class="input-ph-wrap">
										<label>参数值</label> <input type="text" maxlength="20" class="text" id="propValue3">
									</div> <a href="javascript:void(0);" class="clear" onclick="clearData(this);">清空</a>
								</li>
								<li class="">
									<div class="input-ph-wrap">
										<label>参数名</label> <input type="text" maxlength="10" class="text text-short" id="prop4">
									</div>：
									<div class="input-ph-wrap">
										<label>参数值</label> <input type="text" maxlength="20" class="text" id="propValue4">
									</div> <a href="javascript:void(0);" class="clear" onclick="clearData(this);">清空</a>
								</li>
							</ul>
							<div class="savebar">
								<button type="button" onclick="toSaveUserParam(this);">存为模板</button>
								<p class="tpl-hint">将以上参数存为模板，方便日后使用</p>
								<div class="msg-success">
									<i class="icon icon-success"></i>
									<p class="msg">保存为模板8-12模板，可在模板列表中使用</p>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div> <!--发布助手--> 
		<input type="hidden" data-feed="err_nav_categoryProperty" id="nav_categoryProperty">
		<div style="display:none" id="err_nav_categoryProperty"></div> 
	</span>
</li>