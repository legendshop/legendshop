<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/laydate.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>我的分销单 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con">
		<div class="o-mt"><h2>我的分销单</h2></div>
		<div class="pagetab2">
                 <ul>           
                   <li class="on"><span>分销单列表</span></li>                  
                 </ul>       
         </div>
		<div style="color:#00658A;text-align:left;margin-left:10px;margin-top:10px;"><span>每个月的${distDay}号 清算上个月完成的订单，佣金收入直接打款至预存款</span></div>
		<form:form  action="${contextPath}/p/myDistOrder" method="post" id="ListForm">
				<input type="hidden" value="${curPageNO==null?1:curPageNO}" name="curPageNO" id="curPageNO">
			
		 <div class="form search-01" style=" margin-top:15px;">
	         <div class="item">
	              <div class="fl fore"> </div>
	              <div class="fr">
	              	订单状态&nbsp;&nbsp;<select name="status" class="sele">
									<option  value=""  >所有订单</option>
								<option value="1"  <c:if test="${status==1}">selected="selected"</c:if>>待付款</option>
								<option value="2" <c:if test="${status==2}">selected="selected"</c:if>>待发货</option>
								<option value="3" <c:if test="${status==3}">selected="selected"</c:if>>待收货</option>
								<option value="4" <c:if test="${status==4}">selected="selected"</c:if>>已完成</option>
								<option value="5" <c:if test="${status==5}">selected="selected"</c:if>>已取消</option>
							</select>
	              		下单时间&nbsp;&nbsp;<input style="width:90px;height:15px;" type="text" value='<fmt:formatDate value="${startDate}" pattern="yyyy-MM-dd" />' id="startDate" name="startDate" class="text" readonly="readonly">
						-
						<input style="width:90px;height:15px;" type="text" value='<fmt:formatDate value="${endDate}" pattern="yyyy-MM-dd" />' id="endDate" name="endDate"   class="text Wdate" readonly="readonly">
	                 	分销流水号&nbsp;&nbsp;<input type="text" value="${subItemNum}" name="subItemNum" class="text" onblur="this.value=this.value.replace(/(^\s*)|(\s*$)/g, '')">
	                  <input type="submit" id="search_order" class="bti" value="查 询">
	              </div>
	              <div class="clear"></div>
	          </div></div>
		</form:form>
		<table class="ncm-default-table order distorder">
			<thead>
				<tr>
					<th style="width:180px;">分销流水号</th>
					<th >商品</th>
					<th class="w40">数量</th>
					<th class="w90">商品总金额</th>
					<th class="w90">预计佣金收入</th>
					<th class="w90">交易状态</th>
					<th class="w90">下单时间</th>
					<th class="w90">完成时间</th>
				</tr>
			</thead>
			 <tbody>
			 <c:forEach items="${requestScope.list}" var="subItem" varStatus="orderstatues">
			  <tr>
			  	<td>${subItem.subItemNumber}</td>
			  	<td style="padding:10px;"><a target="_blank" href="${contextPath}/views/${subItem.prodId}">${subItem.prodName}</a></td>
			  	<td>${subItem.basketCount}</td>
			  	<td>${subItem.productTotalAmout}</td>
			  	<td>${subItem.distCommisCash}</td>
			  	<td><c:choose>
					 <c:when test="${subItem.subStatus==1 }">
					        待付款
					 </c:when>
					 <c:when test="${subItem.subStatus==2 }">
					       待发货
					 </c:when>
					 <c:when test="${subItem.subStatus==3 }">
					       待收货
					 </c:when>
					 <c:when test="${subItem.subStatus==4 }">
					      已完成
					 </c:when>
					 <c:when test="${subItem.subStatus==5 }">
					      交易关闭
					 </c:when>
				</c:choose>
			  	</td>
			  	<td><fmt:formatDate value="${subItem.subItemDate}" pattern="yyyy-MM-dd" /></td>
			  	<td><fmt:formatDate value="${subItem.finallyDate}" pattern="yyyy-MM-dd" /></td>
			  </tr>
			 </c:forEach>
			 </tbody>
		</table>
		  <c:if test="${empty requestScope.list}">
		  		<!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
		        <div class="warning-option"><!-- <i></i> --><span>没有符合条件的信息</span></div>
		   </c:if>
		<div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
											<span class="p-num">
												<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
											</span>
				     			 </div>
		  				</div>
</div>
    <!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/ToolTip.js'/>"></script>
<script type="text/javascript">
	var path="${contextPath}";
	$(document).ready(function() {
		userCenter.changeSubTab("myDistOrder");
		
		 laydate.render({
			   elem: '#startDate',
			   calendar: true,
			   theme: 'grid',
			   trigger: 'click'
			  });
			   
			 laydate.render({
			   elem: '#endDate',
			   calendar: true,
			   theme: 'grid',
			   trigger: 'click'
		    });
  	});

	 function pager(curPageNO){
        document.getElementById("curPageNO").value=curPageNO;
        document.getElementById("ListForm").submit();
       }
</script>
</body>
</html>