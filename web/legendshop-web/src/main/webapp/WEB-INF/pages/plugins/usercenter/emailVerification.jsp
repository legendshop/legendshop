<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>账户安全 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/securityCenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/validateChanges.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;">     
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con"  id="content">
	<div class="right">
			<div class="o-mt"><h2>修改邮箱</h2></div>
			<div id="step1" class="step step01">
				<ul>
					<li class="fore1">1.验证身份<b></b></li><li class="fore2">2.修改邮箱<b></b></li><li class="fore3">3.完成</li>
				</ul>
			</div>
		<div id="middle">
			<div class="m m1 safe-sevi">
				<div class="mc">
			
        			<div class="form">
        				<div class="item item01">
        					<span class="label">已验证邮箱：</span>
        					<div class="fl"><strong id="mailSpan" class="ftx-un">${userMail}</strong></div>
        					<div class="clr"></div>
        				</div>
        				<div class="item">
            				<span class="label">验证码：</span>
            				<div class="fl">
            					<input type="hidden" id="cannonull" name="cannonull" value='<fmt:message key="randomimage.errors.required"/>'/>
								<input type="hidden" id="charactors4" name="charactors4" value='<ls:i18n key="randomimage.charactors.required" length="4"/>'/> 
								<input type="hidden" id="errorImage" name="errorImage" value='<fmt:message key="error.image.validation"/>'/> 
												
								<input class="text" tabindex="2" name="randNum" id="randNum" type="text">
								<label><img id="randImage" name="randImage" src="<ls:templateResource item='/validCoderRandom'/>"  style="vertical-align: middle;"/>看不清？
            						   <a href="javascript:void(0)" onclick="javascript:changeRandImg('${contextPath}')" style="font-weight: bold;"><fmt:message key="change.random2"/></a>
            					</label>
            					<div class="clr"></div>
            					<div id="authCode_error" class="msg-error"></div>
            				</div>
            				<div class="clr"></div>
            			</div>
        				
        				<div class="item">
        					<span class="label">&nbsp;</span>
							<input id="signedData" style="display:none" type="text">
							<input id="serialNumber" style="display:none" type="text">
        					<div class="fl" style="width: 450px">
	        					<a id="sendMail" class="ncbtn" href="javascript:void(0)" onclick="sendCode();"><s></s>发送验证邮件</a>
	        					<a class="ncbtn" href="${contextPath}/p/security"><s></s>返回</a>
	        					<div class="clr" style="margin-bottom: 5px;"></div>
        						<div id="countDown"  class="msg-text" style="display: none;">邮件已发出，请注意查收，如果没有收到，你可以在<strong id="timer" class="ftx-01"></strong> 秒要求系统重新发送</div>
        					</div>
        					<div class="clr"></div>
        				</div>
        				
        			</div>
				</div>
			</div>
			<div class="m m7">
				<div class="mt"><h3>为什么要进行身份验证？</h3></div>
				<div class="mc">
					<div>1. 为保障您的账户信息安全，在变更账户中的重要信息时需要进行身份验证，感谢您的理解和支持。</div>
					<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
					<div>2. 验证身份遇到问题？请发送用户名、手机号、历史订单发票至<a href="javascript:void(0);" class="flk-05">${systemConfig.supportMail}</a>，客服将尽快联系您。</div>
				</div>
			</div>
			
	 </div>	
		</div>
</div>

 <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script type="text/javascript"  src="<ls:templateResource item='/resources/common/js/randomimage.js'/>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		if('${toPage}'=="toUpdatePassword"){
			$(".o-mt").html("<h2>修改登录密码</h2>");
			$(".fore2").html("2.修改登录密码<b></b>");
		}else if('${toPage}'=="toUpdatePaymentpassword"){
			if(${userSecurity.paypassVerifn}==0){
				$(".o-mt").html("<h2>验证支付密码</h2>");
				$(".fore2").html("2.验证支付密码<b></b>");
			}else{
				$(".o-mt").html("<h2>修改支付密码</h2>");
				$(".fore2").html("2.修改支付密码<b></b>");
			}
		}
		//刷新验证码
		changeRandImg('${contextPath}');
		
	});
	
	//发送邮件
	function sendCode(){
		var page = '${toPage}';
		if(validateRandNum('${contextPath}')){
			$.ajax({
				url:"${contextPath}/p/confirmationMail",
				data:{"toPage":page},
				type:'post', 
				async : false,   
				success:function(retData){
					$("#middle").html(retData);
				}
			});	
		}	
	}
</script>
</body>
</html>