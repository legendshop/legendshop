<!DOCTYPE html>
	<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
	<%@ include file='/WEB-INF/pages/frontend/templetGoat/common/taglib.jsp' %>
	<%@ include file='/WEB-INF/pages/frontend/templetGoat/common/common.jsp' %>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<title>我的余额-账户明细</title>
	<link rel="stylesheet" href="<ls:templateResource item='/resources/templets/css/userProdeposit.css'/>">
</head>
<body>

<div class="fanhui_cou">
	<div class="fanhui_1"></div>
	<div class="fanhui_ding">顶部</div>
</div>
	<div class="w768">
		<header>
			<div class="back">
				<a href="${contextPath}/p/userhome">
					  <img src="<ls:templateResource item='/resources/templets/images/back.png'/>" alt=""> 
				</a>
			</div>
			<p>我的余额</p>
		</header>
		
		<div class="collection-nav">
			<ul>
				<li class="collection-nav-on">
					<a href="${contextPath}/p/m/predeposit/account_balance">
						账户记录
					</a>
				</li>
				<li>
					<a href="${contextPath}/p/m/predeposit/predepositIndex">
						充值明细
					</a>
				</li>
			</ul>
		</div>
		
<!-- 头部 -->
		<div class="my-poi">
		 
			<%-- <p>当前可用余额：<span>￥<fmt:formatNumber value="${userDetail.availablePredeposit}" pattern="0.00#"/></span></p> --%>
			<table cellspacing="0" cellpadding="0" id="integralTab">
				<tr>
					<td>收入</td>
					<td>支出</td>
					<td style="width:40%">操作时间</td>
					<td style="width:18%">操作</td>
				</tr>
			
				<c:forEach items="${predeposit.pdCashLogs.resultList}" var="pred">
					   <tr class="gra">
						    <td>
						          <c:choose>
										<c:when test="${pred.amount>0}">
										   +<fmt:formatNumber value="${pred.amount}" pattern="#,#0.00#"/>
										</c:when>
										<c:otherwise>
										   0.00
										</c:otherwise>
									</c:choose>
						    </td>
						    <td>
				                   <c:choose>
										<c:when test="${pred.amount<0}">
										   <fmt:formatNumber value="${pred.amount}" pattern="#,#0.00#"/>
										</c:when>
										<c:otherwise>
										   0.00
									    </c:otherwise>
								  </c:choose>
						    </td>
					        <td><fmt:formatDate value="${pred.addTime}" type="both"/></td>
							<td>
							<a class="commodity-prize-on" href="<ls:url address='/p/m/predeposit/logDetail/${pred.id}'/>">查看</a>
							</td>
					   </tr>
				 </c:forEach>
			
			</table>
			
			<input type="hidden" id="pageCount" name="pageCount"  value="${pageCount}"/>
			<input type="hidden" id="curPageNO" name="curPageNO"  value="${curPageNO}"/>	

	   </div>
 <!-- loading  -->
<section class="loading" id="loading" style="transform-origin: 0px 0px 0px;opacity: 1;transform: scale(1, 1);">
</section>

<!-- error 提示区 -->
<section class="error" style="transform-origin: 0px 0px 0px;opacity: 1;transform: scale(1, 1); display: none;">
    <div>
        <p>
            <span class="text"></span>
        </p>
    </div>
</section>
<%-- <%@ include file='/WEB-INF/pages/frontend/templetGoat/common/footer.jsp' %>
 --%></body>
<script type="text/javascript">
  var pageContext="${contextPath}";
</script>
<script charset="utf-8"src="<ls:templateResource item='/resources/templets/js/prodepostLog.js'/>"></script>
</html>