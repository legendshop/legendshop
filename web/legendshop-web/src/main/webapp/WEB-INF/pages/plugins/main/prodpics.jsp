<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/browser.js'/>"></script>
<script src="${contextPath}/resources/plugins/jqzoom/js/jquery.jqzoom-core.js" type="text/javascript"></script>
<link rel="stylesheet" href="${contextPath}/resources/plugins/jqzoom/css/jquery.jqzoom.css" type="text/css">
<script src="<ls:templateResource item='/resources/templets/js/prodpics.js'/>"></script>
<style>
    .preview-btn {
        position: absolute;
        z-index: 5;
        bottom: 62px;
        left: 0;
        width: 100%;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        justify-content: center;
        text-align: center;
        z-index: 99999;
    }
    .preview-btn li {
        display: inline-block;
        margin: 0 5px;
        vertical-align: text-top;
    }
    .preview-btn .video-icon {
        cursor: pointer;
        display: inline-block;
        width: 50px;
        height: 50px;
        background: url(/resources/common/images/main-circles.png) 0 -55px no-repeat;
    }
    .preview-btn .video-icon:hover {
        background: url(/resources/common/images/main-circles.png) -55px -55px no-repeat;
    }
    #close-video {
        position: absolute;
        right: 15px;
        top: 9px;
        width: 12px;
        height: 12px;
        background-image: url(/resources/common/images/sprite.png);
        background-position: -58px -40px;
    }
    .zoomWindow {
        left:460px!important;
    }
    .video-box {
        height:100%;
        display: flex;
        background: #000000;
        align-items: center; /*定义body的元素垂直居中*/
        justify-content: center; /*定义body的里的元素水平居中*/
    }
</style>
<div class="preview tb-gallery">
    <div class="tb-booth">
        <input type ="hidden" id = "vidoVal"  value="${prod.proVideoUrl}">
        <div class="video-box" style="display: none">
            <video id = "vidoUrl"  src="<ls:photo item='${prod.proVideoUrl}' />" <%--data-url = "http://192.168.0.148:3005/photoserver/photo/${prod.proVideoUrl}"--%> style='max-width:450px;max-height:450px'  controls="controls"/></video>
        </div>
      <a id="aView" href="<ls:photo item='${prod.pic}'/>" rel='gal1' target="_blank" title="${fn:escapeXml(prod.name)}">

          <img id="imgView" style='max-width:450px;max-height:450px;' src="<ls:photo item='${prod.pic}' />" >
          </a>
        <ul class="preview-btn J-preview-btn" style="display: flex;" id="playBtn">
            <li>
                <span class="video-icon J-video-icon" clstag="shangpin|keycount|product|picvideo" style=""></span>
            </li>
        </ul>
        <a href="#none" id="close-video" class="close-video J-close" clstag="shangpin|keycount|product|closepicvideo"></a>
      </div>
	
	
	<c:if test="${prod.prodPics != null && fn:length(prod.prodPics) > 0}">		
		<div class="imgnav clearfix">
			<div id="spec-list">
			<div class="control back" id="spec-left">&lt;</div>
			<div class="control forward" id="spec-right">&gt;</div>
			<div class="simgbox">
				<ul style="width:950px;overflow: hidden;" class="simglist">
                    <c:forEach items="${prod.prodPics}" var="pics">
                            <li onmouseover="viewPic(this);" data-url="${pics.filePath}" class="img-default">
				          	<table cellpadding='0' cellspacing='0' border='0'><tr><td  height='65' width='65' style='text-align: center;'>
				          		<img src="<ls:images item='${pics.filePath}' scale='3'/>" style="max-width:65px;max-height:65px;" />
                            </td></tr></table>
				         </li>
				         </c:forEach>
								         
					</ul>
				</div>
			</div>
		</div>
	</c:if>

</div>

<script type="text/javascript">

$(document).ready(function() {
	$('#aView').jqzoom({
            zoomType: 'standard',
            lens:true,
            preloadImages: false,
            alwaysOn:false,
            zoomWidth:450,
            zoomHeight:450
        });
        $('.zoomPad').css('z-index','auto');
        //小图片滚动
        $("#spec-list").infiniteCarousel();
     
     
     jQuery.ajax({
		url:contextPath+"/ajaxTag/${prod.prodId}",
		type:'get', 
		async : true, //默认为true 异步  
		dataType : 'json',  
		error:function(data){
			//alert("出错误咯！");
		},   
		success:function(data){
			if(data == "fail"){		
				//不做任何处理		
			}else{
				var tag = data.split(","); 
				//右角标
				if(tag[0] == ""){
					//不做任何处理
				}else{
					$(".tb-booth").append("<i><img src='<ls:photo item='"+tag[0]+"' />' alt=''></i>");						
				}
				
				//下横标
				if(tag[1] == ""){
					//不做任何处理
				}else{
					$(".tb-booth").append("<em><img src='<ls:photo item='"+tag[1]+"' />' alt=''></em>");
				}			
			}
			
		}   
	});

});

//商品小图 切换
function viewPic(obj){
	$(obj).addClass("focus").siblings().removeClass("focus");
	$("#imgView").attr("src","<ls:photo item='"+$(obj).attr("data-url")+"' />");
	$("#aView").attr("href","<ls:photo item='"+$(obj).attr("data-url")+"' />");
	changeImg();
}

$("#close-video").click(function(){
    $(".video-box").css("display","none");
    $("#aView").css("display","block");
    $("#close-video").css("display","none");
    $("#playBtn").show();
    $("#vidoUrl")[0].load();
});

$("#playBtn").click(function () {
    if($("#vidoUrl").attr("src")!=""){
        $(".video-box").css("display","flex");
        $("#aView").css("display","none");
        $("#close-video").css("display","block");
        $("#playBtn").hide();
        $("#vidoUrl")[0].play();
    }
})

// 视频播放完自动跳回图片切换
$("#vidoUrl").get(0).addEventListener("ended",function(){
    $(".video-box").css("display","none");
    $("#aView").css("display","block");
    $("#close-video").css("display","none");
    $("#playBtn").show();
    $("#vidoUrl")[0].load();
},false);

//切换放大镜大图
function changeImg() {  
    $("#aView").html($("#imgView"));
    $("#aView").unbind();
    $("#imgView").unbind();
	$("#aView").data("jqzoom",null);
    $("#aView").jqzoom({
        zoomType: 'standard',
        lens:true,
        preloadImages: false,
        alwaysOn:false,
        zoomWidth:450,
        zoomHeight:450
    });   
}
  /*  $("#vidoUrl").attr("src",$("#vidoUrl").attr("data-url"));
    $("#vidoUrl").attr("data-url","");*/
    $("#aView").css("display","block")
    $(".video-box").css("display","none")
    $("#close-video").css("display","none");
    $("#playBtn").hide()
    if($("#vidoVal").val()!=""){
        $("#playBtn").show()
    }



</script>