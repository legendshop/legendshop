<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/dialog.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>拼团活动管理</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/mergeGroup/mergeGroup.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/merge-group-activity.css'/>" rel="stylesheet" />
</head>
<body>
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
	</div>
	<div class="w1190">
		<div class="seller-cru">
			<p>
				您的位置>
				<a href="javascript:void(0)">首页</a>
				>
				<a href="javascript:void(0)">卖家中心</a>
				>
				<span class="on">拼团活动管理</span>
			</p>
		</div>
		<!-- /面包屑 -->
		<!-- 侧边导航 -->
		<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
		<!-- /侧边导航 -->
		
		<div id="rightContent" class="right_con">
			<!--page-->
			<!-- 拼团活动列表 -->
			<div class="bidding-activity">
				
				<!-- Tab -->
				<div id="statusTab" class="bidding-nav">
					<span <c:if test="${empty mergeGroup.searchType}">class="on"</c:if> ><a
							href="<ls:url address="/s/mergeGroupActivity/query"/>">全部活动</a></span>
					<span <c:if test="${mergeGroup.searchType eq 'WAIT_AUDIT'}">class="on"</c:if>><a
							href="<ls:url address="/s/mergeGroupActivity/query?searchType=WAIT_AUDIT"/>">待审核</a></span>
					<span <c:if test="${mergeGroup.searchType eq 'NOT_PASS'}">class="on"</c:if> ><a
							href="<ls:url address="/s/mergeGroupActivity/query?searchType=NOT_PASS"/>">未通过</a></span>
					<span <c:if test="${mergeGroup.searchType eq 'NOT_STARTED'}">class="on"</c:if> ><a
							href="<ls:url address="/s/mergeGroupActivity/query?searchType=NOT_STARTED"/>">未开始</a></span>
					<span <c:if test="${mergeGroup.searchType eq 'ONLINE'}">class="on"</c:if>><a
							href="<ls:url address="/s/mergeGroupActivity/query?searchType=ONLINE"/>">进行中</a></span>
					<span <c:if test="${mergeGroup.searchType eq 'FINISHED'}">class="on"</c:if> ><a
							href="<ls:url address="/s/mergeGroupActivity/query?searchType=FINISHED"/>">已结束</a></span>
					<span <c:if test="${mergeGroup.searchType eq 'EXPIRED'}">class="on"</c:if>><a
							href="<ls:url address="/s/mergeGroupActivity/query?searchType=EXPIRED"/>">已失效</a></span>
				</div>
				<!-- Tab -->
				
				<!-- 添加/搜索 -->
				<div class="search-add">
					<div class="set-up sold-ser sold-ser-no-bg clearfix" style="overflow:hidden">
						<div class="item">
							点击添加新的拼团活动：<input class="btn-r big-btn add-btn" style="display:inline-block;"  value="立即添加">
						</div>
						
						<form:form id="searchMergeGroup" action="${contextPath}/s/mergeGroupActivity/query" style="float:right;display:inline-block" method="get">
						<div class="fr">
							<div class="item">
								活动名称：<input type="text" placeholder="请输入活动名称" name="mergeName" id="mergeName" value="${mergeGroup.mergeName}" class="text item-inp">
								
								<input type="hidden" value="${empty curPageNO?1:curPageNO}" name="curPageNO" id="curPageNO" /> 
								<input type="hidden" value="${mergeGroup.searchType}" name="searchType" id="searchType" /> 
								
								<input type="submit" value="搜索" class="submit btn-r" style="display:inline-block">
							</div>
						</div>
						</form:form>
					</div>
				</div>
				<!-- 添加/搜索 -->
				
				<table class="fight-table sold-table" cellspacing="0" cellpadding="0">
					<tr class="fight-tit">
						<th>活动名称</th>
						<th width="180">活动时间</th>
						<th>成团人数</th>
						<th>成团订单</th>
						<th>团长是否免单</th>
						<th>活动状态</th>
						<th width="180" style="border-right: 1px solid #e4e4e4">操作</th>
					</tr>
					<c:if test="${empty list}">
						<tr class="detail">
							<td colspan='7' height="60" style="text-align: center; color: #999;">没有符合条件的信息</td>
						</tr>
					</c:if>
					<c:forEach items="${list }" var="item" varStatus="status">
						<tr class="detail">
							<td>${item.mergeName}</td>
							<td>
								<div class="time" style="padding: 0">
									<span>
										<fmt:formatDate value="${item.startTime}" pattern="yyyy-MM-dd HH:mm:ss" type="date" />
										至
									</span>
									<span>
										<fmt:formatDate value="${item.endTime}" pattern="yyyy-MM-dd HH:mm:ss" type="date" />
									</span>
								</div>
							</td>
							<td>
								<span class="fight-num">${item.peopleNumber}人团</span>
							</td>
							<td>
								<span class="fight-ord">${item.count}</span>
							</td>
							<td>
								<c:choose>
									<c:when test="${item.isHeadFree}">
										是
									</c:when>
									<c:otherwise>
										否
									</c:otherwise>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${item.endTime lt date}">
										<span class="fight-sta">已结束</span>
									</c:when>
									<c:when test="${empty item.prodStatus || item.prodStatus eq -1 || item.prodStatus eq -2}">
										<span class="fight-sta">商品已删除</span>
									</c:when>
									<c:when test="${item.prodStatus eq 0}">
										<span class="fight-sta">商品已下线</span>
									</c:when>
									<c:when test="${item.prodStatus eq 2}">
										<span class="fight-sta">商品违规下线</span>
									</c:when>
									<c:when test="${item.prodStatus eq 3}">
										<span class="fight-sta">商品待审核</span>
									</c:when>
									<c:when test="${item.prodStatus eq 4}">
										<span class="fight-sta">商品审核失败</span>
									</c:when>
									<c:otherwise>
										<c:if test="${item.status==-2}">
											<span class="fight-sta">审核不通过</span>
										</c:if>
										<c:if test="${item.status==-1}">
											<span class="fight-staed">已失效</span>
										</c:if>
										<c:if test="${item.status==0}">
											<span class="fight-sta">待审核</span>
										</c:if>
										<c:if test="${item.status==-3}">
											<span class="fight-sta">已结束</span>
										</c:if>
										<c:if test="${item.status==1}">
											<c:choose>
												<c:when test="${item.startTime> date}">
													<span class="fight-sta">未开始</span>
												</c:when>
												<c:otherwise>
													<span class="fight-staing">进行中</span>
												</c:otherwise>
											</c:choose>
										</c:if>
									</c:otherwise>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<%-- 已结束 或者商品状态异常 --%>
									<c:when test="${empty item.prodStatus || item.endTime < date || item.prodStatus eq -1 || item.prodStatus eq -2 || item.prodStatus eq 2 }">
									
										<a href="${contextPath }/s/mergeGroupActivity/mergeGroupDetail/${item.id}" class="blue_button del btn-r">查看</a>
										<a href="${contextPath }/s/mergeGroupActivity/operation/${item.id}" class="blue_button del btn-r">运营</a>
										
										<%-- 待审核 或  未通过 并且已结束  进行物理删除 --%>
										<c:if test="${item.status eq 0 || item.status eq -2}">
											<a href="javascript:void(0);" class="blue_button del btn-g" onclick="confirmDelete('${item.id }')">删除</a>
										</c:if>
										
										<%-- 进行中 或  已失效 并且已结束  进行逻辑删除 --%>
										<c:if test="${item.status eq 1 || item.status eq -1 || item.status eq -3}">
											<a href="javascript:void(0);" class="blue_button del btn-g" onclick="updateDeleteStatus('${item.id }')">删除</a>
										</c:if>
									</c:when>
									<c:otherwise>
										<%-- 待审核 或  未通过 --%>
										<c:if test="${item.status eq 0 || item.status eq -2}">
											<a href="${contextPath }/s/mergeGroupActivity/update/${item.id}" class="blue_button del btn-r">编辑</a>
											<a href="javascript:void(0);" class="blue_button del btn-g" onclick="confirmDelete('${item.id }')">删除</a>
										</c:if>
										<%-- 待审核 或 未通过 --%>
										
										<%-- 未开始 --%>
										<c:if test="${item.status eq 1 && item.startTime > date}">
											<a href="${contextPath }/s/mergeGroupActivity/update/${item.id}" class="blue_button del btn-r">编辑</a>
										</c:if>
										<%-- 未开始 --%>
										
										<%-- 进行中 --%>
										<c:if test="${item.status eq 1 && item.startTime < date}">
											<a href="${contextPath }/s/mergeGroupActivity/mergeGroupDetail/${item.id}" class="blue_button del btn-r">查看</a>
											<a href="${contextPath }/s/mergeGroupActivity/operation/${item.id}" class="blue_button del btn-r">运营</a>
											<%--<a href="javascript:void(0);" class="blue_button btn-g downline" itemId="${item.id}"  status="${item.status}">终止</a>--%>
										</c:if>
										<%-- 进行中 --%>
										
										<%-- 已失效 --%>
										<c:if test="${item.status eq -1}">
											<a href="${contextPath }/s/mergeGroupActivity/mergeGroupDetail/${item.id}" class="blue_button del btn-r">查看</a>
											<a href="${contextPath }/s/mergeGroupActivity/operation/${item.id}" class="blue_button del btn-r">运营</a>
											<a href="javascript:void(0);" class="blue_button del btn-g" onclick="updateDeleteStatus('${item.id }')">删除</a>
										</c:if>
										<%-- 已失效 --%>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
				</table>
			</div>
			<!-- 拼团活动列表 end -->
			<!--page-->
			
			<!-- 分页条 -->
			<div class="clear"></div>
			 <div class="page clearfix">
     			 <div class="p-wrap">
            		<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
     			 </div>
	  		</div>
	  		<!-- 分页条 -->
		</div>
	</div>
	<!--foot-->
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	<!--/foot-->
</body>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/mergeGroup/mergeGroupList.js'/>"></script>
<script type="text/javascript">
	var contextPath = "${contextPath}";
	var curPageNO = "${curPageNO}";
	
</script>
</html>