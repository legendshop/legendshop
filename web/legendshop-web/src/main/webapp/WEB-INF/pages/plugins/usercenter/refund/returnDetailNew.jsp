<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/dialog.jsp"%>

<html>
<head>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<title>退货详情-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/refund/return.css'/>" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="${contextPath}/resources/templets/css/lightbox.css">
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet" />
<style>
.express-select {
	width: 150px;
	height: 25px;
	line-height: 25px;
	border: 1px solid #ccc;
	background-color: #fff;
	color: #666;
}

.express-input {
	width: 300px;
	height: 25px;
	line-height: 25px;
	padding-left: 4px;
	border: 1px solid #ccc;
	background-color: #fff;
	color: #666;
}

.required {
	color: red;
	text-align: right;
}
</style>
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		<div class=" yt-wrap" style="padding-top: 10px;">
			<%@include file='../usercenterLeft.jsp'%>

			<!-----------right_con-------------->
			<div class="right_con">

				<div class="o-mt">
					<h2>退款详情</h2>
				</div>

				<div class="return">
                <!-- 步骤 -->
                <div class="return-step">
                    <div class="step clearfix">
                        <dl class="first doing">
                            <dd class="s-text">买家申请维权<s></s><b></b></dd>
                            <dt class="s-num">1</dt>
                            <dd class="s-time"><fmt:formatDate value="${subRefundReturn.applyTime}" pattern="yyyy-MM-dd HH:mm"/><s></s><b></b></dd>
                        </dl>
                        <dl class="normal <c:if test="${not empty subRefundReturn.sellerTime}">doing</c:if>">
                            <dd class="s-text">商家处理退款申请<s></s><b></b></dd>
                            <dt class="s-num">2</dt>
                            <c:if test="${not empty subRefundReturn.sellerTime}">
                            	<dd class="s-time"><fmt:formatDate value="${subRefundReturn.sellerTime}" pattern="yyyy-MM-dd HH:mm"/><s></s><b></b></dd></c:if>
                        </dl>
                        <dl class="normal <c:if test="${not empty subRefundReturn.shipTime}">doing</c:if>">
                            <dd class="s-text">买家退货给商家<s></s><b></b></dd>
                            <dt class="s-num">3</dt>
                            <c:if test="${not empty subRefundReturn.shipTime}">
                            <dd class="s-time"><fmt:formatDate value="${subRefundReturn.shipTime}" pattern="yyyy-MM-dd HH:mm"/><s></s><b></b></dd></c:if>
                        </dl>
                        <dl class="normal <c:if test="${not empty subRefundReturn.receiveTime}">doing</c:if>">
                            <dd class="s-text">确认收货，平台审核<s></s><b></b></dd>
                            <dt class="s-num">4</dt>
                            <c:if test="${not empty subRefundReturn.receiveTime}">
                            <dd class="s-time"><fmt:formatDate value="${subRefundReturn.receiveTime}" pattern="yyyy-MM-dd HH:mm"/><s></s><b></b></dd></c:if>
                        </dl>
                    </div>
                </div>
                <!-- /步骤 -->
                <div class="return-con clearfix">
                    <div class="return-pro">
                        <div class="item">
                            <div class="tit">售后维权</div>
                            <div class="pro-info">
                                <dl class="clearfix">
                                	<c:if test="${not empty subRefundReturn.productImage}">
                                    	<dt>
                                    		<a href="${contextPath}/views/${subRefundReturn.productId}"><img src="<ls:images item='${subRefundReturn.productImage}' scale='0'/>" alt=""></a>
                                    	</dt>
                                    </c:if>
                                    <dd><a href="${contextPath}/views/${subRefundReturn.productId}">${subRefundReturn.productName }</a></dd>
                                </dl>
                            </div>
                        </div>
                        <div class="item">
                            <p><em>期望结果：</em><span class="col-red">退款退货</span></p>
                            <p>
	                           	<em>退款金额：</em>
								<c:choose>
				              		<c:when test="${subRefundReturn.isPresell && subRefundReturn.isRefundDeposit}">
				              			<c:if test="${empty subRefundReturn.refundAmount }">
				              				<span class="col-red">&#65509;${subRefundReturn.depositRefund}</span>
				              			</c:if>
				              			<c:if test="${not empty subRefundReturn.refundAmount }">
				              				<span class="col-red">&#65509;${subRefundReturn.depositRefund + subRefundReturn.refundAmount}元 (定${subRefundReturn.depositRefund} + 尾${subRefundReturn.refundAmount})</span>
				              			</c:if>
				              		</c:when>
				              		<c:when test="${subRefundReturn.isPresell && !subRefundReturn.isRefundDeposit}">
				              			<span class="col-red">&#65509;${subRefundReturn.refundAmount}</span>
				              		</c:when>
				              		<c:otherwise>
				              			<span class="col-red">&#65509;${subRefundReturn.refundAmount}</span>
				              		</c:otherwise>
				              	</c:choose>
                            </p>
                            <p><em>维权原因：</em><span>${subRefundReturn.buyerMessage}</span></p>
                            <p><em>维权编号：</em><span>${subRefundReturn.refundSn}</span></p>
                            <p><em>订单编号：</em><span class="col-blu"><a href="${contextPath}/p/orderDetail/${subRefundReturn.subNumber}">${subRefundReturn.subNumber}</a></span></p>
                            <p><em>申请备注：</em><span>${subRefundReturn.reasonInfo}</span></p>
                            <p><em>申请时间：</em><span><fmt:formatDate value="${subRefundReturn.applyTime}" pattern="yyyy-MM-dd HH:mm"/></span></p>
                        	<p><em>凭证1：</em><span>
                        	<a href="<ls:images item='${subRefundReturn.photoFile1}' scale='0'/>" data-lightbox="example-1">
                        		<img src="<ls:images item='${subRefundReturn.photoFile1}' scale='0'/>" style="width:50px;height:50px;">
                        	</a>
                        	</span></p>
			              	<c:if test="${not empty subRefundReturn.photoFile2}">
                         		<p><em>凭证2：</em>
                         		<a href="<ls:images item='${subRefundReturn.photoFile2}' scale='0'/>" data-lightbox="example-2">
				              		<img src="<ls:images item='${subRefundReturn.photoFile2}' scale='0'/>" style="width:50px;height:50px;"/>
				              	</a>
				              	</p>
			              	</c:if>
			               	<c:if test="${not empty subRefundReturn.photoFile3}">
				              	<p><em>凭证3：</em>
				              		<a href="<ls:images item='${subRefundReturn.photoFile3}' scale='0'/>" data-lightbox="example-3"><img src="<ls:images item='${subRefundReturn.photoFile3}' scale='0' />" style="width:50px;height:50px;"/>
				              	</a></p>
			             	</c:if>
                        </div>
                    </div>
                    <div class="return-sta">
                        <!-- 审核退款 start -->
                        <c:if test="${subRefundReturn.sellerState eq 1 && subRefundReturn.applyState eq 1}">
	                        <div class="item">
	                            <div class="item-tit">
	                                <em class="returning"></em>
	                                <b>已申请,等待商家审核</b>
	                            </div>
	                       	</div>
	                       	<div class="item">
								<p>如商家确认：平台执行退款操作。   如商家拒绝：你可以选择重新发起申请<c:if test="${subRefundReturn.sellerState eq 3}">
									<a target="_blank" href="${contextPath}/p/im/customerAdmin/0"
									   style="width:100px;height:26px;background: #e5004f;line-height:26px;display: inline-block;text-align: center;color: #fff;border-radius: 3px;">
										<i style="background: url('${contextPath}/resources/templets/images/chat-i.png');width: 19px;height: 18px;display: inline-block;vertical-align: middle;margin-right: 5px;margin-bottom: 2px;"></i>平台客服
									</a>
								</c:if></p>
	                            <p>审核时间：若商家没有在<span class="col-red"><em id="timer" class="timer-simple-seconds" datetime="<fmt:formatDate value="${applyTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"><em class="day">0</em>天<em class="hour">0</em>时<em class="minute">0</em>分<em class="second">0</em>秒</em></span>内进行审核，则将自动同意退款</p>
	                        </div>
                        </c:if>
                        <!-- 审核退款 end -->
                        <!-- 撤销退款 start -->
                        <c:if test="${subRefundReturn.sellerState eq 1 && subRefundReturn.applyState eq -1}">
	                        <div class="item">
	                            <div class="item-tit">
	                                <em class="returning"></em>
	                                <b>退款申请已撤销</b>
	                            </div>
	                       	</div>
                        </c:if>
                        <!-- 撤销退款 end -->
                        <!-- 操作退款弃货物  start -->
                        <c:if test="${subRefundReturn.sellerState eq 2 && subRefundReturn.applyState eq 2}">
	                        <div class="item">
	                            <div class="item-tit">
	                                <em class="returning"></em>
	                                <b>商家同意退款，等待平台确认</b>
	                            </div>
	                       	</div>
                        </c:if>
                        <!-- 操作退款   end -->
                        <!-- 未收到货 申请平台介入  start -->
                        <c:if test="${subRefundReturn.sellerState eq 2 && subRefundReturn.applyState eq -2}">
	                        <div class="item">
	                            <div class="item-tit">
	                                <em class="returning"></em>
	                                <b>退款失败，商家长时间未收到货或买家未发货</b>
	                            </div>
	                       	</div>
	                       	<%-- <c:if test="${subRefundReturn.isIntervene eq 0}">
		                       	<div class="item">
									<p class="item-deal">
										<a href="${contextPath }/p/im/customerAdmin/${subRefundReturn.refundSn}" target="_blank" class="btn-blu">联系平台</a>
										<a href="javascript:void(0);" onclick="openApplyInterven('${subRefundReturn.refundSn}')"  class="btn-blu">申请平台介入</a>
									</p>
		                        </div>
							</c:if>
							<c:if test="${subRefundReturn.isIntervene eq 1}">
		                       	<div class="item">
									<p class="item-deal">
										<a href="${contextPath }/p/im/customerAdmin/${subRefundReturn.refundSn}" target="_blank" class="btn-blu">联系平台</a>
										<a href="javascript:void(0);" onclick="viewInterven('${subRefundReturn.refundSn}')" class="btn-blu">查看维权结果</a>
									</p>
		                        </div>
							</c:if> --%>
                        </c:if>
                        <!-- 未收到货  end -->
                         <!-- 操作退款完成   end -->
                        <c:if test="${subRefundReturn.sellerState eq 2 && subRefundReturn.applyState eq 3}">
	                        <div class="item">
	                            <div class="item-tit">
	                                <em class="returned"></em>
	                                <b>退款完成</b>
	                            </div>
	                        </div>
                        <div class="item">
                            <p>平台备注：<c:if test="${empty subRefundReturn.adminMessage}">暂无</c:if>${subRefundReturn.adminMessage}</p>
                            <p>支付方式：
								<c:if test="${subRefundReturn.payTypeId eq 'ALP' }">支付宝 </c:if>
								<c:if test="${subRefundReturn.payTypeId eq 'COIN' }">金币全额支付</c:if>
								<c:if test="${subRefundReturn.payTypeId eq 'FULL_PAY' }">预存款支付</c:if>
								<c:if test="${subRefundReturn.payTypeId eq 'SIMULATE' }">商城模拟支付</c:if>
								<c:if test="${subRefundReturn.payTypeId eq 'PRED' }">预存款全额支付 </c:if>
								<c:if test="${subRefundReturn.payTypeId eq 'WX_PAY' }">微信支付 </c:if>
							</p>
                            <p>订单总额：${subRefundReturn.subMoney }</p>
                        </div>
	                    </c:if>
                        <!-- 操作退款完成   end -->
                        
                        <!-- 发货信息   start -->
                        <c:if test="${subRefundReturn.sellerState eq 2 && subRefundReturn.returnType eq 2}">
	                       	<c:choose>
				          	  	<c:when test="${subRefundReturn.goodsState eq 1}">
				          	  	  <input type="hidden" id="refundId" name="refundId" value="${subRefundReturn.refundId }"/>
				          	  	  <div class="item">
		                            <div class="item-tit">
		                            	<em class="returning"></em>
		                                <b>商家已同意，请尽快发货</b>
		                            </div>
			                       </div>
			                       <div class="item">
			                       		<p>退货时间：若没有在<span class="col-red"><em id="timer" class="timer-simple-seconds" datetime="<fmt:formatDate value="${waitDelive}" pattern="yyyy-MM-dd HH:mm:ss"/>"><em class="day">0</em>天<em class="hour">0</em>时<em class="minute">0</em>分<em class="second">0</em>秒</em></span>内进行退货，则将自动放弃退款</p>
			                       </div>
									<c:if test="${not empty subRefundReturn.returnDetailAddress}">
										<div class="item">
											<div class="item-tit">
												<span><em class="required"></em>退货地址：</span>
												<span>${subRefundReturn.returnDetailAddress}</span>
											</div>
										</div>
									</c:if>
									<c:if test="${not empty subRefundReturn.returnContact}">
										<div class="item">
											<div class="item-tit">
												<span><em class="required"></em>退货联系人：</span>
												<span>${subRefundReturn.returnContact} ${subRefundReturn.returnPhone}</span>
											</div>
										</div>
									</c:if>
						          <div class="item">
							          <div class="item-tit">
							            <ul>
							              <li>
							              	  <span><em class="required">*</em>物流公司：</span>
								              <select id="expressName" name="expressName" class="express-select">
								              	<option value="">请选择物流公司</option>
								              	<option value="顺丰速递">顺丰速递</option>
								              	<option value="韵达快递">韵达快递</option>
								              	<option value="圆通快递">圆通快递</option>
								              	<option value="中通快递">中通快递</option>
								              	<option value="申通快递">申通快递</option>
								              	<option value="天天快递">天天快递</option>
								              	<option value="德邦快递">德邦快递</option>
								              	<option value="百世汇通快递">百世汇通快递</option>
								              </select>
							              </li>
							            </ul>
						            </div>
						          </div>
						           <div class="item">
			                            <div class="item-tit">
			                                <span><em class="required">*</em>物流单号：</span>
						              		<input id="expressNo" name="expressNo" maxlength="20" value="" class="express-input" placeholder="请输入物流单号"/>
			                            </div>
			                       	</div>
			                       	<div class="item">
			                       		<c:if test="${subRefundReturn.sellerState eq 2 and subRefundReturn.returnType eq 2 and subRefundReturn.goodsState eq 1}">
											<p class="item-deal"><a href="javascript:void(0);" id="submitBtn" onclick="confirmReturn();" class="btn-blu">确定退货</a></p>
										</c:if>
			                        </div>
				          	  	</c:when>
				          	  	<c:when test="${subRefundReturn.goodsState eq 2}">
						          	  	<div class="item">
			                            <div class="item-tit">
			                            	<em class="returning"></em>
			                                <b>已发货,等待商家确认收货</b>
			                            </div>
				                       </div>
				                       <div class="item">
				                       		<p>如商家确认：商家执行退款操作。 如商家拒绝：你可以选择重新发起申请</p>
				                       		<p>审核时间：若商家没有在<span class="col-red"><em id="timer" class="timer-simple-seconds" datetime="<fmt:formatDate value="${waitReceive}" pattern="yyyy-MM-dd HH:mm:ss"/>"><em class="day">0</em>天<em class="hour">0</em>时<em class="minute">0</em>分<em class="second">0</em>秒</em></span>内进行收货确认，则将自动同意退款</p>
				                       </div>
				                       <div class="item">
			                            <div class="item-tit">
			                            	<em></em>
			                                <b>我的物流信息</b>
			                            </div>
				                       </div>
				          	  		<div class="item">
							              <p>物流公司：${subRefundReturn.expressName}</p>
							              <p>物流单号：${subRefundReturn.expressNo}</p>
										  <c:if test="${not empty subRefundReturn.returnDetailAddress}">
							              	  <p>退货地址：${subRefundReturn.returnDetailAddress}</p>
										  </c:if>
										  <c:if test="${not empty subRefundReturn.returnContact}">
											  <p>退货联系人：${subRefundReturn.returnContact} ${subRefundReturn.returnPhone}</p>
										  </c:if>
							         </div>
				          	  	</c:when>
				          	  	<c:otherwise>
				          	  		<div class="item">
			                            <div class="item-tit">
			                            	<em></em>
			                                <b>我的物流信息</b>
			                            </div>
				                       </div>
							        <div class="item">
							            <p>物流公司：${subRefundReturn.expressName}</p>
							            <p>物流单号：${subRefundReturn.expressNo}</p>
										<c:if test="${not empty subRefundReturn.returnDetailAddress}">
											<p>退货地址：${subRefundReturn.returnDetailAddress}</p>
										</c:if>
										<c:if test="${not empty subRefundReturn.returnContact}">
											<p>退货联系人：${subRefundReturn.returnContact} ${subRefundReturn.returnPhone}</p>
										</c:if>
							         </div>
				          	  	</c:otherwise>
				          	  </c:choose>
                        </c:if>
                        <!-- 发货   end -->
                        
						<!-- 拒绝退款   start -->
                        <c:if test="${subRefundReturn.sellerState eq 3}">
	                        <div class="item">
	                            <div class="item-tit">
	                                <em class="returning"></em>
	                                <b>拒绝退款</b>
	                            </div>
	                        </div>
	                        <div class="item">
	                            <p>备注信息：${subRefundReturn.sellerMessage}</p>
	                        </div>
	                        <%-- <c:if test="${subRefundReturn.isIntervene eq 0}">
		                       	<div class="item">
									<p class="item-deal">
										<a href="${contextPath }/p/im/customerAdmin/${subRefundReturn.refundSn}" target="_blank" class="btn-blu">联系平台</a>
										<a href="javascript:void(0);" onclick="openApplyInterven('${subRefundReturn.refundSn}')" class="btn-blu">申请平台介入</a>
									</p>
		                        </div>
							</c:if>
							<c:if test="${subRefundReturn.isIntervene eq 1}">
		                       	<div class="item">
									<p class="item-deal">
										<a href="${contextPath }/p/im/customerAdmin/${subRefundReturn.refundSn}" target="_blank" class="btn-blu">联系平台</a>
										<a href="javascript:void(0);" onclick="viewInterven('${subRefundReturn.refundSn}')" class="btn-blu">查看维权结果</a>
									</p>
		                        </div>
							</c:if> --%>
	                    </c:if>
						<!-- 拒绝退款   start --> 
						                        
                        <div class="item-tip">
                            <p class="col-red">友情提示：</p>
                            <p>1. 若提出申请后，商家拒绝退款，可再次提交申请或选择<em>“申请维权”</em>，请求商城客服人员介入。
				            	<%--<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&amp;uin=2465239493&amp;site=legendshop&amp;menu=yes">--%>
									<%--<img border="0" src="${contextPath }/resources/templets/images/qq_ico.png" alt="销售咨询">--%>
								<%--</a>--%>
							</p>
                            <p>2. 成功完成退款；经过商城审核后，会将退款金额以<em>“预存款”</em>的形式返还到您的余额账户中或者会将退款金额以<em>“原路返回”</em>的形式退还</p>
                        </div>
                    </div>
                </div>
            </div>

				<div class="ste-but" align="center">
					<%-- <c:if test="${subRefundReturn.sellerState eq 1 && subRefundReturn.applyState eq 1}">
				    	<input type="button" id="edit_refund" value="编辑退款申请" class="re" onclick="window.location='${contextPath}/p/editRefundApply/${subRefundReturn.refundSn }'"/>
                    </c:if> --%>
					<input type="button" value="返回" class="btn-g" onclick="window.location='${contextPath}/p/refundReturnList/${subRefundReturn.applyType }'" />
				</div>
				<!-- 退货详情 -->
			</div>
		</div>
		<!-----------right_con 结束-------------->

		<div class="clear"></div>
	</div>

	<%@ include
		file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/refund/reciprocal.js'/>"></script>
	<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/refund/lightbox.min.js'/>"></script>
	<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/refund/refundDetail.js'/>"></script>
	<script type="text/javascript">
		var contextPath = '${contextPath}';
		$(function() {
			userCenter.changeSubTab("myreturnorder");//高亮菜单
		});
		var type = '${subRefundReturn.applyType}';

	</script>
</body>
</html>
