<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<div id="fullSlide_${layoutParam.layoutId}" layoutId="${layoutParam.layoutId}" class="fullSlide_layout">
	   <div id="bd_${layoutParam.layoutId}" class="bd">
			<ul>
			      <c:forEach items="${shopLayoutBanners}" var="banner" varStatus="s">
			             <li >
			               <a target="_blank" href="${banner.url}" style="text-align:center;">
			                   <img  src="<ls:photo item='${banner.imageFile}' />">
			               </a>
			            </li>
			     </c:forEach>
             </ul>
		</div>
		<div id="hd_${layoutParam.layoutId}" class="hd">
			<ul>
			       <c:if test="${fn:length(shopLayoutBanners)>1}">
					       <c:forEach items="${shopLayoutBanners}" var="banner" varStatus="s">
			                   <li <c:if test="${s.first}"> class="on"</c:if> >${s.count}</li>
			            </c:forEach>
				   </c:if>
             </ul>
	     </div>
</div>

<script type="text/javascript">
      jQuery(document).ready(function(){
	     jQuery("#fullSlide_${layoutParam.layoutId}").slide({ titCell:"#hd_${layoutParam.layoutId} li", mainCell:"#bd_${layoutParam.layoutId} ul", effect:"fade",autoPlay:true, delayTime:500 });
      });
</script>
