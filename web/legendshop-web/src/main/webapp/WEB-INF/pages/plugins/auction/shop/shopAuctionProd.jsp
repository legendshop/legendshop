<div id="group" class="tabcon" style="padding: 0">
             <div class="suits" style="width:100%;"> 
                 <div class="col4">                   
	                    <c:if test='${empty list}' >
	                    	<div class="selection_box search_noresult">
							     <p class="search_nr_img"><img src="<ls:templateResource item='/resources/templets/images/not_result.png'/>"></p> 
							     <p class="search_nr_desc">没有找到<c:if test="${not empty param.keyword}">与“<span>${param.keyword}</span>”</c:if>相关的活动信息</p>
							   </div>
	                    </c:if>
							 
						
                        <ul class="list-h">
                    			<c:forEach var="auction" items="${list}">
                    				<a style="position: relative;" href="${contextPath}/auction/views/${auction.id}" target="_blank">
                    				<li class="gl-item" id="id_${auction.id}">
		                                    <div class="gl-i-wrap j-sku-item">
		                                    <div class="p-img">
		                                    <span style="height: 100%; display: inline-block; vertical-align: middle;"></span>
		                                    <img alt="" style="vertical-align: middle" src="<ls:images item='${auction.prodPic}' scale='1'/>"  style="max-width: 240px;max-height: 270px;" >
		                                                   <div class="picon pi8"><b></b></div>
		                                                </div> 
		                                                <div class="p-name">
		                                                  <a  href="${contextPath}/auction/views/${auction.id}" target="_blank">${auction.auctionsTitle }</a>
		                                                </div>
		                                                <div id="auctionProd_${auction.id}" endTime="${auction.endTimeStr}" startTime="${auction.startTimeStr}" 
			                    						   <c:if test="${auction.status==2}">a="3" </c:if>
			                    						   <c:if test="${auction.startTime>nowTime}">a="0" r="${auction.startTimeStr-nowTime.time}" </c:if>
			                    							<c:if test="${auction.startTime<nowTime&&auction.endTime>nowTime}">a="1" r="${auction.endTimeStr-nowTime.time}"</c:if>
			                    							<c:if test="${auction.startTime<nowTime&&auction.endTime<nowTime}">a="2" </c:if>  >
		                                                <div class="p-time">剩余时间：<b>13</b><span>时</span><b>14</b><span>分</span><b>15</b><span>秒</span>
                                                         </div>
		                                                   <div class="p-state" ><span ></span>
		                                                  </div>
		                                                </div>
		                                                <div class="p-times" biddingNumber="${auction.biddingNumber }">出价次数：<span>
		                                                	<c:choose>
		                                                		<c:when test="${empty auction.biddingNumber}">暂无出价</c:when>
		                                                		<c:otherwise>${auction.biddingNumber}</c:otherwise>
		                                                	</c:choose>
		                                                
		                                                </span></div>
		                                                <div class="p-price">
		                                                	<a href="${contextPath}/auction/views/${auction.id}">
		                                                	<em class="p-curPrice" price="${auction.curPrice}">当前竞拍价：¥${auction.curPrice }</em>
		                                                	<img src="<ls:templateResource item='/resources/templets/images/bidding-s.png'/>" alt=""></a>
		                                                </div>
		                        	</li>  </a>
                    			</c:forEach>
		                     	
                      </ul>