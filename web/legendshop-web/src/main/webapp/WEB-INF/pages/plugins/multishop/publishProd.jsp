<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<title>商品编辑-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		<link type="text/css" href="<ls:templateResource item='/resources/templets/css/publishProd1${_style_}.css'/>" rel="stylesheet" />
		<link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
		<link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.css'/>" />
		<link type="text/css" href="<ls:templateResource item='/resources/templets/css/saveAttribute.css'/>" rel="stylesheet" />
		<link type="text/css" href="<ls:templateResource item='/resources/templets/css/prodSpecOverlay.css'/>" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/plugins/select/divselect.css'/>" />
		<div class="w box" id="item-publish" style="margin-top: 10px;">
			<!-- 右侧导航 -->
			<%@ include file="./prod/rightNav.jsp"%>

			<div class="hd">
				<ul class="common_info_tabs">
					<li id="panel-b-l" class="active"><a onclick="panelPrev();"
						href="javascript:void(0);">基本信息</a></li>
					<li id="panel-s-l"><a href="javascript:void(0);"
						onclick="panelNext();">单品信息</a></li>
				</ul>
			</div>
			<div class="bd">
				<!-- 基本信息 -->
				<div id="panel-base">
					<div data-spm="1000773" class="bd-sub">
						<div id="product-info" class="product-info">
							<div class="detail">
								<ul style="margin-left: 20px;">
									<li><c:if test="${not empty treeNodes }">
											<c:forEach items="${treeNodes}" var="node" varStatus="n">
												<c:choose>
													<c:when test="${!n.last}">
								                         ${node.nodeName}&gt;&gt;
									                </c:when>
													<c:when test="${n.last}">
														<span> ${node.nodeName} </span>
													</c:when>
												</c:choose>
											</c:forEach>
										</c:if> <c:choose>
											<c:when test="${empty isSimilar}">
												<input type="button" class="cat-edit" value="编辑类目"
													id="J_ReCategory"
													onclick="window.location.href='${contextPath}/s/publish/${empty productDto.prodId?0:productDto.prodId}?categoryId=${categoryId}'">
											</c:when>
											<c:otherwise>
												<input type="button" class="cat-edit" value="编辑类目"
													id="J_ReCategory"
													onclick="window.location.href='${contextPath}/s/publishSimilar/${productDto.prodId}?categoryId=${categoryId}'">
											</c:otherwise>
										</c:choose></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- end bd-sub -->
						<form:form action="${contextPath}/s/saveProd" method="post" id="form1" enctype="multipart/form-data">
						<c:if test="${empty isSimilar}">
							<input type="hidden" id="prodId" name="prodId" value="${productDto.prodId}" />
						</c:if>
						<input type="hidden" id="categoryId" value="${categoryId}" name="categoryId" />
						<input type="hidden" id="imagesPaths" name="imagesPaths" />
                        <input type="hidden" id="proVideo" name="proVideoUrl" value="${productDto.proVideoUrl}" />
						<input type="hidden" id="skuList" name="skus" />
						<input type="hidden" id="userParameter" name="userParameter" value='${productDto.userParameter}' />
						<input type="hidden" id="parameter" name="parameter" value='${productDto.parameter}' />
						<input type="hidden" id="userProperties" name="userProperties" value='${productDto.userPropList}' />
						<input type="hidden" id="deleteUserProperties" name="deleteUserProperties" />
						<input type="hidden" id="prodBrandId" name="brandId" value="${productDto.brandId}" />
						<input type="hidden" id="valueImages" name="valueImages" />
						<input type="hidden" id="valueAlias" name="valueAlias" value='${productDto.valueAliasList}' />
						<input type="hidden" id="stocks" name="stocks" value="${productDto.stocks}" />
						<input type="hidden" id="impId" value="${impId}" name="impId" />
						<!-- 是否是类似商品导入 -->
						<input type="hidden" id="isSimilar" value="${isSimilar}"
							name="isSimilar" />
						<div class="bd-main">
							<div class="form">
								<ul id="J_form">
									<span id="prod-param-tab">
										<h5>1. 商品基本参数</h5>
									</span>
									<!-- 商品参数 -->
									<%@ include file="./prod/prodParams.jsp"%>

									<span id="prod-base-tab">
										<h5 style="margin-top: 15px;">2. 商品基本信息</h5>
									</span>
									<!-- 基本信息 -->
									<%@ include file="./prod/baseInfo.jsp"%>

									<span id="prod-attr-tab">
										<h5 style="margin-top: 15px;">3. 商品属性</h5>
									</span>
									<!-- 商品属性 -->
									<%@ include file="./prod/prodAttrs.jsp"%>

									<span id="prod-pic-tab">
										<h5 style="margin-top: 15px;">4. 商品图片</h5>
									</span>
									<!-- 商品图片 -->
									<%@ include file="./prod/prodPics.jsp"%>
									<span id="prod-detail-tab">
										<h5 style="margin-top: 15px;">5. 详情描述</h5>
									</span>
									<!-- 详情描述 -->
									<%@ include file="./prod/prodCont.jsp"%>

									<span id="prod-other-tab">
										<h5 style="margin-top: 15px;">6. 其他信息</h5>
									</span>
									<!-- 其他信息 -->
									<%@ include file="./prod/otherInfo.jsp"%>
								</ul>
							</div>
						</div>
					</form:form>
					<div class="submit">
						<div class="float-submitbar">
							<button value="下一步" onclick="panelNext();"
								class="J_Submit btn btn-main-primary btn-submit" type="button">
								<span class="btn-txt">下一步</span>
							</button>
						</div>
					</div>
				</div>

				<!-- 单品信息 -->
				<div id="panel-sku" class="panel-sku" style="display: none;">
					<%@ include file="./prod/skuPanel.jsp"%>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<%@ include
			file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<input value="${edit}" type="hidden" id="isEdit">

	<link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/vediojs/video-js.css'/>" />
	<script src="<ls:templateResource item='/resources/plugins/vediojs/videojs-ie8.min.js'/>"></script>
	<script src="<ls:templateResource item='/resources/plugins/vediojs/video.js'/>"></script>
	<script src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
	<script src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
	<script src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>

	<script src="<ls:templateResource item='/resources/common/js/map.js'/>"></script>
	<script src="<ls:templateResource item='/resources/common/js/arrayExtend.js'/>"></script>
	<script src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
	<script src="<ls:templateResource item='/resources/common/js/ajaxfileupload.js'/>" type="text/javascript"></script>

	<script src="<ls:templateResource item='/resources/plugins/select/divselect.js'/>"></script>
	<script src="<ls:templateResource item='/resources/templets/js/publishProd.js'/>" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>

	<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.core.js'/>" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.widget.js'/>" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.mouse.js'/>" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.sortable.js'/>" type="text/javascript"></script>
	<script type="text/javascript">
		var paramData = {
			contextPath : "${contextPath}",
			specSize : "<c:out value="${requestScope.specSize}"/>",
			imagePrefix : "<c:out value="${imagePrefix}"/>",
			cookieValue : "<c:out value="${cookie.SESSION.value}"/>",
			photoServer : "<c:out value="${requestScope.photoServer}"/>",
			prodId : "<c:out value="${requestScope.productDto.prodId}"/>",
			photoPath : "<ls:photo item='' />"
		}

		var skudata = [];
		<c:forEach items="${productDto.skuDtoList}" var="sku">
		skudata.push('${sku.properties}');
		</c:forEach>

		var pluginId = "${pluginId}";

	</script>
</body>
</html>