<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/back-dialog.jsp"%>
<html>
<head>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>"></script>
<link type="text/css" rel="stylesheet"  href="<ls:templateResource item='/resources/plugins/ztree/zTreeStyle.css'/>" >
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.min.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.excheck-3.5.js'/>"></script>
</head>
<body>
	<fieldset style="padding-bottom: 30px; height:245px;">
		<legend>分类</legend>
		<div style="margin:10px 0px 10px 10px;height:245px;overflow-y:auto;" > 
   			<ul id="catTrees" class="ztree"></ul>
    	</div>
	</fieldset>
	<div align="center">
		<input style="margin:10px;padding:8px" type="button" class="s" onclick="javascript:selectCategory();" value="确定" id="Button1" name="Button1">
		<input style="margin:10px;padding:8px" type="button" class="s" onclick="javascript:clearCategory();" value="取消">
	</div>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.min.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.excheck-3.5.js'/>"></script>
<script type="text/javascript">

	$(document).ready(function(){
		//zTree设置
		var catTrees=JSON.parse('${catTrees}');
		var setting = {
				check:{
						enable: true,
						chkboxType: {'Y':'ps','N':'ps'},
						chkStyle: 'radio',
						radioType: 'all'
						}, 
				data:{
					simpleData:{ enable: true }
				}, callback:{
					beforeCheck: beforeCheck,
					onCheck: onCheck
				} };
		$.fn.zTree.init($("#catTrees"), setting, catTrees);
	});

	function selectCategory(){
			var zTree = $.fn.zTree.getZTreeObj("catTrees");
			var chkNodes = zTree.getCheckedNodes(true);
			if(chkNodes.length<1) {
				window.parent.layer.msg('请选择分类',{time: 1000});
				return;
			}
			var catId = chkNodes[0].id;
			var catName = chkNodes[0].name;
			
			var level= chkNodes[0].level;
			var first_cid=catId;
			var two_cid= "";
			var third_cid="";
			if(level==1){  //2级
			   var parent = chkNodes[0].getParentNode();
			   first_cid=parent.id;
			   two_cid=catId;
			}else if(level==2){ //3级
			   var parent = chkNodes[0].getParentNode();
			   var firstParent = parent.getParentNode();
			   first_cid=firstParent.id;
			   two_cid=parent.id;
			   third_cid=catId;
			}
			var type="${type}";
			if(type=="I" || type=="G"){
				window.parent.loadCategoryCallBack(first_cid,two_cid,third_cid,catName);
			}
			else{
				window.parent.loadCategoryCallBack(first_cid,two_cid,third_cid, catName);
			}
			var index = window.parent.layer.getFrameIndex(window.name); 
			window.parent.layer.close(index);
	}
	
	function clearCategory(){
		window.parent.clearCategoryCallBack();
		var index = window.parent.layer.getFrameIndex(window.name); 
		window.parent.layer.close(index);
	}
	
	function beforeCheck(treeId, treeNode, clickFlag) {
		/* var all="${all}";
		if(all!=undefined && all!=null && all!=""){
			return true;
		}
		if(treeNode.isParent){
			alert("请选择最底层分类");
		}
		return !treeNode.isParent;//当是父节点 返回false 不让选取 */
	}
	    
	function onCheck(){
	}
</script>
</html>

