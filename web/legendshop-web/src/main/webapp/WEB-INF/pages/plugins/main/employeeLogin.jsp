<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>登录-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="loginback">
	  <div >
		<div class="yt-wrap">
			<%-- <div class="lr_tab">
				<a class="on" href="javascript:void(0);">登 录</a>
				<a href="${contextPath}/reg">注 册</a>
			</div> --%>
			<div class="logintop">
				<a href="${contextPath}/home"><img src="<ls:photo item="${systemConfig.logo}"/>"  style="max-height:89px;" />
				</a>
			</div>
		</div>

         <!--login-->
         <div class="lg-mid-b">
          <div style="margin:auto;width:1190px;overflow: hidden;">
         	<div style="float:left;margin-left:130px;margin-right:80px;">
         		<img alt="" src="${contextPath}/resources/templets/images/login-logo.png" style="height:400px;">
         	</div>
          <div class="loginb" style="float:left;">
             <div class="login-box">
                    <div class="mt">
                        <h1>欢迎登录</h1>
                    </div>

                    <div class="mc">
                        <div class="form">
                        	<input type="hidden" id="rand" name="rand"/>
							<input type="hidden" id="cannonull" name="cannonull" value='<fmt:message key="randomimage.errors.required"/>'/>
							<input type="hidden" id="charactors4" name="charactors4" value='<ls:i18n key="randomimage.charactors.required" length="4"/>'/>
							<input type="hidden" id="errorImage" name="errorImage" value='<fmt:message key="error.image.validation"/>'/>
							<input type="hidden" id="redirectIndicator" name="redirectIndicator" value="${contextPath}/login"/>
                            <form:form  action="${contextPath}/p/j_spring_security_check" method="post" id="formlogin" onsubmit="return loginForm()">
                                 <div class="item item-fore1" style="margin-bottom: 25px;">
                                    <label class="login-label name-label" for="username"></label>
                                 	<input type="hidden" id="returnUrl" name="returnUrl" value="${param.returnUrl}"/>
                                    <input type="text" placeholder="商家的用户名"  tabindex="1"  id="shopName" type='text' name="shopName"  value="<lb:lastLogingUser/>" class="itxt">
                                    <c:choose>
						  				<c:when test="${param.error == 1}">
										     <div class="mt">
						                         <label class="login_error"><fmt:message key="error.password.noright"/></label>
						                    </div>
						  				</c:when>
									  <c:when test="${param.error == 2}">
									       <div class="mt">
					                         <label class="login_error"  ><fmt:message key="error.user.logined"/></label>
					                       </div>
									   </c:when>
									  	<c:otherwise>
									  	   <div id="pwdIncorrect" style="display: none;" class="mt">
					                           <label class="login_error" ><fmt:message key="error.password.noright"/></label>
					                       </div>
									  	</c:otherwise>
				    				</c:choose>
	                                <label id="loginError" style="line-height:25px;"></label>
                                </div>
                               <div class="item item-fore1" style="margin-bottom: 25px;">
                                    <label class="login-label name-label" for="username"></label>
                                 	<input type="hidden" id="returnUrl" name="returnUrl" value="${param.returnUrl}"/>
                                    <input type="text" placeholder="员工账号"  tabindex="1"  id="employeename" type='text' name="username"  value="" class="itxt">
	                                <label id="employeeNameError" style="line-height:25px;"></label>
                                </div>
                                <div class="item item-fore2" id="entry" style="margin-bottom: 25px;">
                                    <label for="password" class="login-label pwd-label"></label>
                                    <input id="pwd" type='password' tabindex="2" onload="this.value=''" name='password'   placeholder="密码" class="itxt itxt-error">
	                                 <label id="pwdError" style="line-height:25px;"></label>
                                </div>
                              <%--   <div class="item item-fore3">
                                    <div class="safe">
                                        <span>
                                            <input type="checkbox" id="_spring_security_remember_me" name="_spring_security_remember_me" class="jdcheckbox">
                                            <label for="_spring_security_remember_me">自动登录</label>
                                        </span>
                                        <span class="forget-pw-safe">
                                            <a target="_blank" class="" href="<ls:url address='/retrievepassword'/>">忘记密码?</a>
                                        </span>
                                    </div>
                                </div>  --%>
                                 <lb:userValidationImage>
	                                  <div class="item-fore6">
	                                    <input type="text" id="randNum" name="randNum" class="inputstyle" maxlength="4" tabindex="3" placeholder="输入验证码">
	                                    <img id="randImage" name="randImage" src="<ls:templateResource item='/validCoderRandom'/>" style="vertical-align: middle;" />
	                                    <a href="javascript:void(0)" onclick="javascript:changeRandImg('${contextPath}')"><fmt:message key="change.random2"/></a>
	                                </div>
	                                <label id="randNumError"></label>
                                </lb:userValidationImage>
                                <div class="item item-fore5">
                                    <div class="login-btn">
                                    <input class="btn-img btn-entry" value="登&nbsp;&nbsp;&nbsp;&nbsp;录" type="submit"/>
                                    </div>
                                </div>
                                <input type="hidden" id="loginType" name="loginType" value="1"/>
                            </form:form>
                        </div>
                    </div>
                </div>
         </div>
         </div>
         </div>
		 <%@ include file="home/simpleBottom.jsp" %>
  </div>
</body>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/common/js/randomimage.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/login.js'/>"></script>
<script type="text/javascript">
   var contextPath="${contextPath}";
</script>
</html>