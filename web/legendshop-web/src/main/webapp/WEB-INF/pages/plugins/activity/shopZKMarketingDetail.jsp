<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>促销活动-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/active/actives.css'/>" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">促销活动</span>
				</p>
			</div>
			
			<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>

             
			<div id="rightContent" class="right_con">
			<div class="o-mt">
				<h2>促销活动</h2>
			</div>
		
			<div class="ncm-default-form">
				<form:form  action="${contextPath}/s/addShopMarketing" id="add_form" method="post">
					<dl>
				      <dt><i class="required">*</i>活动名称：</dt>
				      <dd>
				        <input type="text" class="w400 text" maxlength="25" name="marketName" id="marketName" value="${marketing.marketName}" disabled>
				        <span class="error-message"></span>
				        <p class="hint">活动名称最多为25个字符</p>
				      </dd>
				    </dl>
				    
					<dl>
				      <dt><i class="required">*</i>开始时间：</dt>
				      <dd>
				        <input type="text" disabled value='<fmt:formatDate value="${marketing.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>'/>
				        <span class="error-message"></span>
				      </dd>
				    </dl>
				    
				    <dl>
				      <dt><i class="required">*</i>结束时间：</dt>
				      <dd>
				      <input type="text" disabled value='<fmt:formatDate value="${marketing.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>'/>
				        <span class="error-message"></span>
				        <p class="hint">
				        </p>
				      </dd>
				    </dl>
				    
				    
					<dl>
						<dt><i class="required">*</i>选择商品：</dt>
						<dd>
					   		全部商品
						</dd>
					</dl>
					
					<dl>
						<dt><i class="required">*</i>促销类型：</dt>
						<dd>
							<c:choose>
								<c:when test="${marketing.type == 2}">
									限时折扣
								</c:when>
								<c:otherwise>
									其他促销活动
								</c:otherwise>
							</c:choose>
						</dd>
					</dl>
					
					<dl>
				      <dt><i class="required">*</i>活动规则：</dt>
				       <c:forEach items="${marketing.marketingMzRules}" var="marketingMj" >
				      		<dd><span class="ncsc-zk-rule-dt">限时价格折扣  打 <strong>${marketingMj.offDiscount}</strong> 折</span></dd>	
				       </c:forEach>
				    </dl>
				    
				    <dl>
				      <dt>备注：</dt>
				      <dd>
				        <p class="hint">${marketing.remark}</p>
				      </dd>
				    </dl>
									
					<dl class="bottom">
						<dt>&nbsp;</dt>
						<dd>
							<label class="submit-border">
							
							<a href="javascript:history.go(-1);"><input type="button" value="返回"  class="submit btn-r big-btn"></a>
							</label>
						</dd>
					</dl>
				</form:form>
			</div>
		   		
			<div class="clear"></div>
		</div>
				  
		  
		</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
    <script src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>" type="text/javascript"></script>
    <script src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
    <script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/shopShowMansong.js'/>"></script>
    <script type="text/javascript">
    var contextPath = '${contextPath}';
    var isAllProd="${isAllProd}";
</script>  
</body>
</html>