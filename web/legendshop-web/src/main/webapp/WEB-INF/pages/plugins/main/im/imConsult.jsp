<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<div class="my-order">
	<c:if test="${empty skuDetailDto.skuId && empty subRefundReturn}">
		<c:if test="${empty imOrderList}">
			<div class="no-record">
				<i class="rec-i"></i>
				<p class="rec-p">暂无数据</p>
			</div>
		</c:if>
	</c:if>
	<c:forEach items="${imOrderList}" var="item">
		<div class="ord-item">
			<div class="ord-num">
				<span class="num-span">订单号：</span>
				<span class="num">${item.subNumber }</span>
				<a href="javascript:void(0)" class="btn sendOrder" data-number="${item.subNumber }" data-price='${item.actualTotal}' data-date='<fmt:formatDate value="${item.subDate }" pattern="yyyy-MM-dd HH:mm" type="date"/>' data-name="${item.dtoList[0].skuName }">
					发送
				</a>
			</div>
			<div class="ord-info clear">
				<c:choose>
					<c:when test="${fn:length(item.dtoList)==1}">
						<a href="${contextPath }/views/${item.dtoList[0].prodId}" class="pro-img" target="_blank" data-prodId="${item.dtoList[0].prodId}">
							<img src="<ls:photo item="${item.dtoList[0].pic}"/>">
						</a>
						<a href="${contextPath }/views/${item.dtoList[0].prodId}" class="name" target="_blank">
								${item.dtoList[0].skuName }
						</a>
					</c:when>
					<c:otherwise>
						<c:forEach items="${item.dtoList}" var="subItem">
							<a href="${contextPath }/views/${subItem.prodId}" class="pro-img" target="_blank" data-prodId="${subItem.prodId}">
								<img src="<ls:photo item="${subItem.pic}"/>">
							</a>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="ord-han">
				<span class="han-left">订单金额：<span class="pir"><em class="font-family:Arial;">&yen;</em>${item.actualTotal}</span></span>
				<span class="han-right">
					 <fmt:formatDate value="${item.subDate }" pattern="yyyy-MM-dd HH:mm" type="date"/>
				</span>
			</div>
		</div>
	</c:forEach>

		<c:if test="${not empty skuDetailDto.skuId || not empty subRefundReturn}">
			<div class="now-list">
				<div class="list-item">
					<div class="pro clear">
						<div class="pro-img">
							<c:choose>
							<c:when test="${not empty subRefundReturn.applyType && subRefundReturn.applyType==1}">
							<a href="${contextPath }/p/refundDetail/${subRefundReturn.refundId}" style="cursor: pointer;" target="_blank">
								</c:when>
								<c:when test="${not empty subRefundReturn.applyType && subRefundReturn.applyType==2}">
								<a href="${contextPath }/p/returnDetail/${subRefundReturn.refundId}" style="cursor: pointer;" target="_blank">
									</c:when>
									<c:otherwise>
									<a href="${contextPath }/views/${skuDetailDto.prodId}" style="cursor: pointer;" target="_blank">
										</c:otherwise>
										</c:choose>
										<c:choose>
											<c:when test="${not empty skuDetailDto.skuPic }"><img src="<ls:photo item='${skuDetailDto.skuPic}'/>"></c:when>
											<c:when test="${not empty skuDetailDto.prodPic }"><img src="<ls:photo item='${skuDetailDto.prodPic}'/>"></c:when>
											<c:otherwise><img src="${contextPath }/resources/templets/images/im/head-default.jpg"></c:otherwise>
										</c:choose>
									</a>
						</div>
						<div class="pro-txt">
							<p class="name" style="position: relative;">
								<c:choose>
								<c:when test="${not empty subRefundReturn.applyType && subRefundReturn.applyType==1}"><a href="${contextPath }/p/refundDetail/${subRefundReturn.refundId}" class="over-hiden" target="_blank"></c:when>
								<c:when test="${not empty subRefundReturn.applyType && subRefundReturn.applyType==2}"><a href="${contextPath }/p/returnDetail/${subRefundReturn.refundId}" class="over-hiden" target="_blank"></c:when>
									<c:otherwise><a href="${contextPath }/views/${skuDetailDto.prodId}" class="over-hiden" target="_blank"></c:otherwise>
											</c:choose><c:if test="${not empty skuDetailDto.skuName }">${skuDetailDto.skuName }</c:if><c:if test="${empty skuDetailDto.skuName }">${skuDetailDto.prodName }</c:if></a>
										${skuDetailDto.cnProperties }
									<c:if test="${not empty subRefundReturn }">
							<p style="font-size: 10px;position: absolute;top:20px;">退款编号：${subRefundReturn.refundSn }</p>
							</c:if>
							</p>
							<p class="price" data='<c:choose><c:when test="${not empty skuDetailDto.skuPrice }">${skuDetailDto.skuPrice }</c:when><c:otherwise>${subRefundReturn.refundAmount}</c:otherwise></c:choose>'>
								<c:if test="${not empty skuDetailDto.skuPrice }">
									<em class="font-family:Arial;">&yen;</em>${skuDetailDto.skuPrice}
								</c:if>
								<c:if test="${empty skuDetailDto.skuPrice }">
									<em class="font-family:Arial;">&yen;</em>${subRefundReturn.refundAmount}
								</c:if>
							</p>
						</div>
					</div>
					<div class="pro-btn">
						<a href="javascript:void(0)" class="btn" id="sendProduct" data-prodId="${skuDetailDto.prodId }" data-refundSn="${subRefundReturn.refundSn }" data-refundId="${subRefundReturn.refundId }" data-applyType="${subRefundReturn.applyType }">发送</a>
					</div>
				</div>
			</div>
		</c:if>



	<div class="oth-list">
		<div class="list-tab clear">
			<a href="javascript:void(0)" class="tab-a on">最近浏览</a>
		</div>
		<c:if test="${empty skuDetailDto.products }">
			<div class="no-record">
				<i class="rec-i"></i>
				<p class="rec-p">暂无数据</p>
			</div>
		</c:if>

		<c:if test="${not empty skuDetailDto.products }">
			<div class="list-con">
				<div class="list-box">
					<c:forEach items="${skuDetailDto.products}" var="item">
						<div class="list-item browse-history browse-history-${item.shopId}" >
							<div class="pro clear">
								<a href="${contextPath }/views/${item.prodId}"  target="_blank" class="pro-img">
									<img src="<ls:photo item='${item.pic}'/>">
								</a>
								<div class="pro-txt">
									<a href="${contextPath }/views/${item.prodId}" target="_blank" class="name">${item.name }</a>
									<p class="price" data="${item.cash}"><em class="font-family:Arial;">&yen;</em>${item.cash}</p>
								</div>
							</div>
							<div class="pro-btn"><a href="javascript:void(0)" class="btn sendVisitedProd" data-prodId="${item.prodId}">发送</a></div>
						</div>
					</c:forEach>
				</div>
			</div>
		</c:if>
	</div>
	</div>

</div>
