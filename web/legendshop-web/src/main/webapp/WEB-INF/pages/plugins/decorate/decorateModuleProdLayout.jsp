<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/public.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/decorate.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/common/css/pager.css'/>" rel="stylesheet" />
<div class="white_box">
<div class="box_floor">
  <div class="box_floor_six">
      <span class="floor_choose">已选推荐商品：</span>
    <div class="box_floor_prodel"> <em class="floor_warning">注释：双击删除已选商品信息，按下鼠标拖动商品次序</em>
      <div id="goods_info" class="floor_box_pls ui-sortable" style="max-height:220px; overflow: auto;">
          <c:forEach items="${requestScope.prods}" var="prod">
	        <ul  class="floor_pro" ondblclick="jQuery(this).remove();">
	          <li class="floor_pro_img"><img width="95" height="95" layout_prodid="${prod.layoutProdId}" goods_id="${prod.prodId}"  seq="${prod.seq}" goods_name="${prod.prodName}" cash="${prod.cash}" pic='${prod.pic}'  src="<ls:images item='${prod.pic}' scale='2' />"></li>
	        </ul>
		 </c:forEach>
      </div>
    </div>
    <span class="floor_choose">选择要展示的商品：</span>
    <div class="floor_choose_box">
      <span class="floor_search_sp">
      <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
      <input type="text" placeholder="商品名称关键字" class="ip180" id="prodName" value="${name}" name="prodName">
      </span>
      <input type="button" style="" onclick="productLoad();" value="搜索" class="floor_sear_btn">
       <div id="goods_list">
     
      </div>
    </div>
  </div>
  <!--图片开始-->
  <div class="fiit_save_box">
    <input type="hidden" value="loyout_module_custom_prod" name="layoutModuleType" id="layoutModuleType">
    <input type="hidden" value="${layoutParam.layoutId}" name="layoutId" id="layoutId">
    <input type="hidden" value="${layoutParam.layout}" name="layout" id="layout">
    <input type="hidden" value="${layoutParam.divId}" name="divId" id="divId">
    <input type="hidden" value="${layoutParam.layoutDiv}" name="layoutDiv" id="layoutDiv">
    <input type="hidden" value="${layoutParam.shopId}" name="shopId" id="shopId">
    <input type="hidden" value="${layoutParam.decId}" name="shopDecotateId" id="shopDecotateId">
    <input type="button" id="save" onclick="save_form( );" value="保存">
  </div>
</div>
</div>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.core.js'/>"  type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.widget.js'/>"  type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.mouse.js'/>"  type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.sortable.js'/>"  type="text/javascript"></script>
 <script type="text/javascript">
	var contextPath = '${contextPath}';
	jQuery(document).ready(function(){
		   jQuery("#goods_info").sortable();
		   jQuery("#goods_info").disableSelection();
		   
		    productLoad(1);
		   
            $("#prodName").bind('keypress',function(event){
              if(event.keyCode == "13")    
               {
                    productLoad(1);
               }
           });
		  
	});
	
	function productLoad(){
			var curPageNO = $("#curPageNO").val();
			productLoad(curPageNO);
	}
	
	function productLoad(curPageNO){
		var prodName = $("#prodName").val();
		var data = {
	             	"curPageNO": curPageNO,
	             	"name":prodName
	    };
		$.ajax({
			url:contextPath+"/s/shopDecotate/productLoad",
			data:data,
			type:'post', 
			async : true,  
			success:function(result){
				jQuery("#goods_list").empty().append(result);	
			}
		});
	}
	 var _seq=0;
	 function goods_list_set(_this){
	      var img_uri=jQuery(_this).attr("img_uri");
	      var name=jQuery(_this).attr("goods_name");
	      var productId=jQuery(_this).attr("goods_id");
	      var cash=jQuery(_this).attr("cash");
		  var count=jQuery(".floor_box_pls ul").length;
		  var seq=jQuery("ul li:last-child").find("img").attr("seq");
		  var add=0;
		  //无线添加商品
		  /* if(count>=6){
			  parent.art.dialog.alert("最多只能添加6件商品！");
			  add=1;
		  } */
		  jQuery(".floor_box_pls .floor_pro_img img").each(function(){
		     if(jQuery(this).attr("goods_id")== productId){
			   parent.layer.alert("已经存在该商品", {icon: 0});
			   add=2;
			 }
		  });
		  if(seq==undefined || seq=="" || seq==null){
		    seq=0;
		    _seq+=Number(seq);
		  } 
		   _seq++;
		  if(add==0){
		     var s="<ul ondblclick='jQuery(this).remove();' class='floor_pro'><li class='floor_pro_img'><img layout_prodid='' goods_id='"+productId+"' goods_name='"+name+"' cash='"+cash+"' seq='"+_seq+"' pic='"+img_uri+"' src='<ls:images item='"+img_uri+"' scale='2' />' /></li></ul>";
		     jQuery(".floor_box_pls").append(s);
		  }
	}
	
	function save_form(){
		var map = [];
		var seq=0;
		 var count=jQuery(".floor_box_pls ul").length;
		 if(count==0){
			  parent.layer.alert("请添加商品！");
			  return false;
		 }
		  //需要无线添加商品
		 /* if(count>6){
			  parent.art.dialog.alert("最多只能添加6件商品！");
			  return false;
		  } */
		 jQuery(".floor_box_pls ul img").each(function(){
			 var obj = new Object();
			 seq++;
			 obj.layoutProdId=jQuery(this).attr("layout_prodid");
			 obj.prodId = jQuery(this).attr("goods_id");
			 obj.prodName=jQuery(this).attr("goods_name");
			 obj.cash=jQuery(this).attr("cash");
			 obj.seq = seq;
			 obj.pic= jQuery(this).attr("pic");
			 obj.shopId= jQuery("#shopId").val();
			 obj.shopDecotateId= jQuery("#shopDecotateId").val();
			 obj.layoutId= jQuery("#layoutId").val();
			 map.push(obj);
		 });
		 var products = JSON.stringify(map);
		 var layoutDiv=$("#layoutDiv").val();
		 var layoutId=$("#layoutId").val();
		 var data={
		    "products":products,
		    "layoutModuleType":$("#layoutModuleType").val(),
		    "layoutId":layoutId,
		    "layout":$("#layout").val(),
		    "divId":$("#divId").val(),
		    "layoutDiv":layoutDiv
		 }
		 
		 $("#save").attr("disabled","disabled");
	     $.ajax({
	         //提交数据的类型 POST GET
	         type:"POST",
	         //提交的网址
	         url:contextPath+"/s/shopDecotate/layout/saveLayoutProduct",
	         //提交的数据
	         data: data,
	         async : false,  
	         //返回数据的格式
	         datatype: "text",
	         //成功返回之后调用的函数            
	         success:function(data){
	              var result=$.trim(data);
	        	  if(result.indexOf("fail")!=-1){
	        		  parent.layer.alert('保存失败', {icon: 2});
	        		  $("#save").removeAttr("disabled");
	        	  }
	        	  else if(result.indexOf("LAYOUT_ERROR")!=-1){
	        		  parent.layer.alert('保存失败', {icon: 2});
	        		  $("#save").removeAttr("disabled");
	        	  }
	        	  else{
						$('#module_edit').remove();
						if (layoutDiv != "") {
							parent.$("div[mark=" + layoutId + "][div=div2][id='content']").html("");
							parent.$("div[mark=" + layoutId + "][div=div2][id='content']").html(result);
						} else {
						     var html="<a  onclick='decorate_module_set(this);' layout='layout2'  mark='"+layoutId+"' moduleType='loyout_module_custom_prod' href='javascript:void(0);' class='f_set' ><i></i>设置</a>";	
						   
						    parent.$("div[option="+layoutId+"]").find("a:first").before(html); 
						    
							parent.$("div[mark=" + layoutId + "][id='content']").html("");
							parent.$("div[mark=" + layoutId + "][id='content']").html(result);
							
						}
						$("#save").removeAttr("disabled");
			
						var index = parent.layer.getFrameIndex('decorateModuleProd'); //先得到当前iframe层的索引
						parent.layer.close(index); //再执行关闭   

	        	  }
	         },
	         //调用执行后调用的函数
	         complete: function(XMLHttpRequest, textStatus){
	         }      
	      });
	   
		}
		
		
	
   </script>