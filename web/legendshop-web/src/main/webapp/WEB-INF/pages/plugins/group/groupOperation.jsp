<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/dialog.jsp"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>团购活动运营</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/fight.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/mergeGroup/mergeGroupDetail.css'/>" rel="stylesheet"/>
</head>
<body>
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
	</div>
	<div class="w1190">
		<div class="seller-cru">
			<p>
				您的位置>
				<a href="javascript:void(0)">首页</a>
				>
				<a href="javascript:void(0)">卖家中心</a>
				>
				<span class="on">团购</span>
			</p>
		</div>
		<!-- /面包屑 -->
		<!-- 侧边导航 -->
		<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
		<!-- /侧边导航 -->
		<!-- 拼团运营信息 -->
		<div class="fight-operate">
			
			<!-- 运营统计数据 -->
			<div class="operate-statistics">
				<div class="operate-statistics-tit">
					<p>运营结果</p>
				</div>
				<div class="operate-statistics-det">
					<ul>
					
						<li>
							交易总金额：<span><fmt:formatNumber value="${operateStatistics.totalAmount}" type="currency" pattern="￥##.##"  minFractionDigits="2" /></span>
						</li>
						<li>
							参团人数：<span>${operateStatistics.participants}</span>
						</li>
						<li>
							成团人数：<span>${operateStatistics.peopleCount}</span>
						</li>
						<li>
							交易成功金额：<span><fmt:formatNumber value="${operateStatistics.succeedAmount}" type="currency" pattern="￥##.##"  minFractionDigits="2" /></span>
						</li>
					</ul>
				</div>
			</div>
			<!-- 运营统计数据 -->
			
			<table cellpadding="0" cellspacing="0">
				<tr class="tab-tit">
					<th width="60">编号</th>
					<th width="200">订单号</th>
					<th>买家</th>
					<th>数量</th>
					<th width="100">交易金额(元)</th>
					<th>团购状态</th>
					<th>参团时间</th>
				</tr>
				<c:if test="${empty list}">
					<tr class="first-leve">
						<td colspan='7' height="60" style="text-align: center; color: #999;">暂无数据</td>
					</tr>
				</c:if>
				
				<%-- <tr>
					<td colspan="7">
						<div class="f-num">
							<c:choose>
								<c:when test="${merge.status == 2 }">
									<span class="sta">拼团成功</span>
								</c:when>
								<c:when test="${merge.status == 1 }">
										<span class="sta-wate">待成团</span>
								</c:when>
								<c:when test="${merge.status == 3 }">
									<span class="sta-fail">拼团失败</span>
								</c:when>
							</c:choose>
							${operateStatistics.peopleCount}人成团，${operateStatistics.participants}人已参加
						</div>
					</td>
				</tr> --%>
				<c:forEach items="${list}" var="item" varStatus="itemStatus">
					<tr>
						<td>${itemStatus.index+1}</td>
						<td>${item.subNumber}</td>
						<td>${item.userName}</td>
						<td>${item.productNums}</td>
						<td>${item.actualTotal}</td>
						<td>
							<c:choose>
								<c:when test="${item.groupStatus eq 0}">
								<span class="sta-wate">团购中</span>
								</c:when>
								<c:when test="${item.groupStatus eq 1}">
								<span class="sta">团购成功</span>
								</c:when>
								<c:when test="${item.groupStatus eq -1}">
								<span class="sta-fail">团购失败</span>
								</c:when>
								<c:when test="${item.groupStatus eq -2}">
								<span class="sta-fail">待支付</span>
								</c:when>
							</c:choose>
						</td>
						<td>
							<fmt:formatDate value="${item.payDate}" pattern="yyyy-MM-dd HH:mm:ss" type="date" />
						</td>
					</tr>
				</c:forEach>
			</table>
			<div style="margin-top: 10px;" class="page clearfix">
				<div class="p-wrap">
					<ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="simple" />
				</div>
			</div>
			
			<div class="btn-box">
				<input type="button" value="返回" onclick="window.history.go(-1);" />
			</div>
			
		</div>
		
		
		
	</div>

	<!-- end 拼团运营信息 -->
	<!-- </div> -->
	<!--foot-->
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	<!--/foot-->
</body>
<script language="JavaScript" type="text/javascript">
	$(function(){
		userCenter.changeSubTab("groupManage"); //高亮菜单
	})
	var curPageNO = "${curPageNO}";
	var contextPath = "${contextPath}";
	var groupId = "${groupId}"
	function pager(curPageNO) {
		
		window.location.href = contextPath+"/s/group/operation/"+groupId+"?curPageNO="+curPageNO;
	}
</script>
</html>

