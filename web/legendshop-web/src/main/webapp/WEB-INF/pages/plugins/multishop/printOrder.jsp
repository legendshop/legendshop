<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<link rel="stylesheet" type="text/css" href="<ls:templateResource item='/resources/common/css/template.css'/>" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML xmlns="http://www.w3.org/1999/xhtml">
<HEAD>
<TITLE>购物清单打印</TITLE>
</HEAD>
<body>
	<div class="print_box">
		<div class="print_left">
			<h1>${order.userName}-购物清单</h1>
			<ul>
			    <c:if test="${not empty order.userAddressSub}"> 
				   <li>收货人：${order.userAddressSub.receiver}</li>
				   <li>电话：
				   		<c:choose>
				   			<c:when test="${empty order.userAddressSub.telphone}">无</c:when>
				   			<c:otherwise>${order.userAddressSub.telphone}</c:otherwise>
				   		</c:choose>
				   </li>
				   <li>手机：${order.userAddressSub.mobile}</li>
				   <li>地址：${order.userAddressSub.detailAddress}</li>
				   <li>邮编：
				   		<c:choose>
				   			<c:when test="${empty order.userAddressSub.subPost}">无</c:when>
				   			<c:otherwise>${order.userAddressSub.subPost}</c:otherwise>
				   		</c:choose>
				   </li>
				</c:if>
				<li>发票信息：
					<c:choose>
						<c:when test="${empty order.invoiceSub}">
							不开发票
						</c:when>
						<c:otherwise>
							${order.invoiceSub.type==1?"普通发票":"增值税发票"}&nbsp;
							<c:choose>
								<c:when test="${order.invoiceSub.title==1}">[个人抬头]</c:when>
								<c:otherwise>[公司抬头]</c:otherwise>
							</c:choose>&nbsp;
							${order.invoiceSub.company}&nbsp;
							<c:if test="${order.invoiceSub.title ne 1}">(${order.invoiceSub.invoiceHumNumber})</c:if>
						</c:otherwise>
					</c:choose>
				</li>

				<li>订单号：${order.subNum}</li>
				<li>下单时间：<fmt:formatDate value="${order.subDate}" type="both" /></li>
				<li>发货单号：
					<c:choose>
						<c:when test="${empty order.dvyFlowId}">未发货</c:when>
						<c:otherwise>${order.dvyFlowId}</c:otherwise>
					</c:choose>
				</li>
			</ul>
			<div class="print_left_tb">
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
					<tbody>
						   <tr>
							<td width="7%" align="center">序号</td>
							<td width="55%">商品名称</td>
							<td width="11%" align="center">规格</td>
							<td width="10%" align="center">单价（元）</td>
							<td width="7%" align="center">数量</td>
							<td width="10%" align="center">小计（元）</td>
						   </tr>
						
						  <!-- S 商品列表 -->
							<c:forEach items="${order.subOrderItemDtos}" var="orderItem" varStatus="orderItemStatues">
							  <tr>
									<td height="38" align="center">${orderItemStatues.count}</td>
									<td>${orderItem.prodName}</td>
									<td align="center">${empty orderItem.attribute?"无":orderItem.attribute}<br>
									</td>
									<td align="center">¥${orderItem.cash}</td>
									<td align="center">${orderItem.basketCount}</td>
									<td align="center"><strong style="color:#FF3300">¥ ${orderItem.productTotalAmout}
									</strong>
									</td>
								</tr>
							</c:forEach>
						<tr>
							<td height="60" colspan="6"><ul>
									<li>商品总价:¥<fmt:formatNumber value="${order.total}" type="currency" pattern="0.00"></fmt:formatNumber></li>
									<li>运费:¥<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber></li>
									<li>订单总额：¥<fmt:formatNumber value="${order.actualTotal}" type="currency" pattern="0.00"></fmt:formatNumber></li>
								</ul>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="print_right">
			<h1>
				<span><a onclick="window.print()" href="javascript:void(0);">打印</a></span>
			</h1>
		</div>
	</div>


</body>
</HTML>
