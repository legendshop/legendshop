<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
<title>调整预售订单金额</title>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/deliverAddressOverlay.css'/>" rel="stylesheet"/>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller${_style_}.css'/>" rel="stylesheet"/>
<style type="">
  .form label, .form input,.fl input { 
     height:20px;
     width: 225px;
  }
  .mc{
		text-align:center;
	}
	a{color:#fff;}
</style>
</head>
<body>
<div class="m" id="edit-cont">
	<div class="mc">
		<div class="form">
		   	<div class="item">
				<span class="label"><em>*</em>用户：</span>
				    <div class="fl">
				  <input readonly="readonly" type="text" name="userName" id="userName" size="30" value="${sub.userName}">
				   </div>
				</span>
              </div>
			
		
			<div class="item">
				<span class="label"><em>*</em>订单号：</span>
				    <div class="fl">
				  <input readonly="readonly" type="text" name="subNumber" id="subNumber" size="30" value="${sub.subNumber}">
				   </div>
				</span>
              </div>
			
			<div class="item">
				<span class="label"><em>*</em>商品定金：</span>
				<div class="fl">
				  <input type="text" name="preDepositPrice"  value="${sub.preDepositPrice}"  id="preDepositPrice" size="30" >
				</div>
			</div>
			
			<div class="item">
				<span class="label"><em>*</em>商品尾款：</span>
				<div class="fl">
				  <input type="text" name="finalPrice"  value="${sub.finalPrice}"  id="finalPrice" size="30" >
				</div>
			</div>
			
			<div class="item">
				<span class="label"><em>*</em>配送金额：</span>
				<div class="fl">
				  <input type="text" name="freightAmount"  value="${sub.freightAmount}"  id="freightAmount" size="30" >
				</div>
			</div>
			
			<div class="item" >
				<span class="label"><em>*</em>合计：</span>
				<div class="fl">
				   <span style="color:#e5004f; font-weight:bold;font-size:12px;" id="totalPriceText">￥${sub.preDepositPrice + sub.finalPrice + sub.freightAmount}</span>
				   <input type="hidden" name="totalPrice" id="totalPrice" value="${sub.preDepositPrice + sub.finalPrice + sub.freightAmount}">
				</div>
			</div>
			
			<div class="btn-r" style="text-align: center;">
			     <a href="javascript:void(0);" id="orderFee" onclick="submitPreOrderFee();"  class="ncbtn">保存更改</a>
			</div>
			</div>
			</div>
		</div>
	</div>
</div>
</body>
	<script type="text/javascript">	
	   $(document).ready(function() {
	   		//当输入框的值改变时,改变订单金额
		   $("#preDepositPrice,#finalPrice,#freightAmount").change(function(){
		   		changePreOrderFee();//改变订单金额
		   });
	   });
	   
	   //改变订单金额
	   function changePreOrderFee(){
	   		 var preDepositPrice = $("#preDepositPrice").val();
	   		 var finalPrice = $("#finalPrice").val();
	   		 var freightAmount = $("#freightAmount").val();

	         var totalPrice = Number(preDepositPrice)+Number(finalPrice)+Number(freightAmount);
	         $("#totalPrice").val(totalPrice);
             $("#totalPriceText").text("￥" + totalPrice);
	   }
	   
	   //提交调整预售订单
	    function submitPreOrderFee(){
             var subNumber = $.trim($("#subNumber").val());
             var preDepositPrice = $("#preDepositPrice").val();
	   		 var finalPrice = $("#finalPrice").val();
	   		 var freightAmount = $("#freightAmount").val();
	   		 
	   		 if(validate(subNumber,preDepositPrice,finalPrice,freightAmount)){
		         $("#orderFee").attr("disabled","disabled");
	             $.ajax({
						url:"${contextPath}/s/changePreOrderFee", 
						data:{
							"subNumber" : subNumber,
							"preDepositPrice" : preDepositPrice,
							"finalPrice" : finalPrice,
							"freightAmount" : freightAmount
						},
						type : "POST", 
						async : false, //默认为true 异步   
						dataType : "JSON",
						error : function(jqXHR, textStatus, errorThrown) {
						    $("#orderFee").attr("disabled","");
						    return;
						},
						success:function(result){
							if(result=="OK"){
							      window.parent.location.reload();
							}else if(result=="fail"){
								  $("#orderFee").attr("disabled","");
								  layer.alert("调整订单费用失败",{icon:2});
						          return;
							}
						}
				});
	   		 }
      }
      
      //校验用户输入的数据
      function validate(subNumber,preDepositPrice,finalPrice,freightAmount){
      		 if(isBlank(subNumber)){
            	layer.msg("非法操作!",{icon:0});
                return false;
             }
      		 if(isBlank(preDepositPrice)){
	             layer.msg("请输入商品定金!",{icon:0});
	             return false;
	         }
	         if(isBlank(finalPrice)){
	             layer.msg("请输入商品定金!",{icon:0});
	             return false;
	         }
	         if(!isMoney(preDepositPrice) || preDepositPrice==0){
	             layer.msg("请输入正确的商品定金!",{icon:0});
				 return false;
	         }
	         if(!isMoney(finalPrice) || finalPrice==0){
	             layer.msg("请输入正确的商品尾款!",{icon:0});
				 return false;
	         }
	         if(!isMoney(freightAmount)){
	             layer.msg("请输入正确的配送运费!",{icon:0});
				 return false;
	         }
	         return true;
      }
      
      //是否是金额
      function isMoney(obj) {
		   if (!obj)
		   return false;
		   return (/^\d+(\.\d{1,2})?$/).test(obj);
      }
       
	  function isBlank(value){
	  	   return value == undefined || value == null || $.trim(value) == ""; 
	  }
  
	</script>
</html>