<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
</head>
<body>
     	<fieldset style="padding-bottom: 30px; height:245px;">
			<legend>分类</legend>
			<div  style="margin:10px 0px 10px 10px;height:245px;overflow-y:auto;" > 
		   		<ul id="catTrees" class="ztree"></ul>
	     	</div>
		</fieldset>
		<div align="center">
			<input style="margin:10px;padding:8px" type="button" class="s" onclick="javascript:selectCategory();" value="确定" id="Button1" name="Button1">
			<input style="margin:10px;padding:8px" type="button" class="s" onclick="javascript:clearCategory();" value="取消">
		</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>"></script>
<link type="text/css" rel="stylesheet"  href="<ls:templateResource item='/resources/plugins/ztree/zTreeStyle.css'/>" >
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.min.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.excheck-3.5.js'/>"></script>
<script>
	var type='${type}';
	var _catTrees = '${catTrees}';
	var all='${all}';
</script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/group/loadAllCategory.js'/>"></script>
</body>

</html>

