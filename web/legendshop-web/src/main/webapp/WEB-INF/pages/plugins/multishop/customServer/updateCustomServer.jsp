<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/dialog.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<title>客服人员</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css" href="${contextPath}/resources/templets/css/multishop/server/service-staff.css" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>
					>
					<a href="${contextPath}/sellerHome">卖家中心</a>
					>
					<span class="on">客服人员</span>
				</p>
			</div>
			<%@ include file="../included/sellerLeft.jsp"%>
			<div class="right_con">
				<div class="o-mt">
					<h2>客服人员</h2>
				</div>
				<div class="service-staff">
					<!-- 添加客服人员 -->
					<form id="form">
						<input name="id" type="hidden" value="${item.id}">
						<div class="ser-add">
							<div class="item clear">
								<span class="item-tit">客服名称：</span>
								<div class="item-con">
									<input  id="name" name="name" type="text" value="${item.name}" />
								</div>
							</div>
							<div class="item clear">
								<span class="item-tit">客服账号：</span>
								<div class="item-con">
									<input disabled name="account" type="text" value="${item.account}" autocomplete="new-password" />
								</div>
							</div>
							<div class="item clear">
								<span class="item-tit">备注：</span>
								<div class="item-con">
									<textarea name="remark">${item.remark}</textarea>
								</div>
							</div>
							<div class="item clear">
								<span class="item-tit">是否冻结：</span>
								<div class="item-con">
									<select id="frozen" name="frozen">
										<option value="false">否</option>
										<option value="true" >是</option>
									</select>
								</div>
							</div>
							<div class="item clear">
								<span class="item-tit">&nbsp;</span>
								<div class="item-con">
									<input type="submit" value="保存" class="item-btn">
									<a href="../../customServer" class="item-btn gray-btn">取消</a>
								</div>
							</div>
						</div>
					</form>
					<!-- /添加客服人员 -->
				</div>
			</div>
		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<script type="text/javascript" src="${contextPath}/resources/common/js/jquery.js"></script>
	<script type="text/javascript" src="${contextPath}/resources/common/js/jquery.validate.js"></script>
	<script type="text/javascript">
		var contextPath = "${contextPath}";

        var frozen = '${item.frozen}';
        if(frozen!=null&&frozen!=undefined&&frozen!=''){
            if(frozen=="true"){

                $("#frozen").val("true");
            }else{
                $("#frozen").val("false");
            }
        }else{
            $("#frozen").val("false");
        }
	</script>
	<script src="<ls:templateResource item='/resources/templets/js/multishop/customServer/updateCustomServer.js'/>"></script>
</body>
</html>