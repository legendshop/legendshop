<!DOCTYPE html>
	<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
	<%@ include file='/WEB-INF/pages/frontend/templetGoat/common/taglib.jsp' %>
	<%@ include file='/WEB-INF/pages/frontend/templetGoat/common/common.jsp' %>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<title>充值详情</title>
	<link rel="stylesheet" href="<ls:templateResource item='/resources/templets/css/userProdeposit.css'/>">
</head>
<body>

<div class="fanhui_cou">
	<div class="fanhui_1"></div>
	<div class="fanhui_ding">顶部</div>
</div>
	<div class="w768">
		<header>
			<div class="back">
				<a href="${contextPath}/p/m/predeposit/predepositIndex">
					  <img src="<ls:templateResource item='/resources/templets/images/back.png'/>" alt=""> 
				</a>
			</div>
			<p>充值详情</p>
		</header>
		
<!-- 头部 -->
		<div class="my-poi">
		 
		       <table cellspacing="0" cellpadding="0" id="detail">
					 <c:if test="${not empty recharge}">
							<tr>
								<td>充值单号:</td>
								<td class="le">${recharge.sn}</td>
							</tr>
						    <tr>
								<td>充值金额:</td>
								<td class="le"><span style="color:red">￥${recharge.amount}</span></td>
							</tr>
							<tr>
								<td>申请时间:</td>
								<td class="le"><fmt:formatDate value="${recharge.addTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
							</tr>
							<tr>
								<td>支付名称:</td>
								<td class="le">
								   <c:choose>
										<c:when test="${recharge.paymentState==0}">
										        未支付
										</c:when>
										<c:when test="${recharge.paymentState==1}">
										   ${recharge.paymentName}
										</c:when>
								  </c:choose>
								</td>
							</tr>
							<tr>
								<td>支付流水号:</td>
								<td class="le">
								    <c:choose>
											<c:when test="${recharge.paymentState==0}">
											         未支付
											</c:when>
											<c:when test="${recharge.paymentState==1}">
											    ${recharge.tradeSn}
											</c:when>
									</c:choose>
								</td>
							</tr>
							<tr>
								<td>处理状态:</td>
								<td class="le">
									  <c:choose>
										<c:when test="${recharge.paymentState==0}">
										  未支付
										</c:when>
										<c:when test="${recharge.paymentState==1}">
										   充值成功  
										</c:when>
									  </c:choose>
								</td>
							</tr>
					 </c:if>
					 
					  <c:if test="${empty recharge}">
					       <tr>
								<td>该数据有误！</td>
							</tr>
					  </c:if>
			</table>
			
			
	   </div>
<%@ include file='/WEB-INF/pages/frontend/templetGoat/common/footer.jsp' %>
</body>
</html>