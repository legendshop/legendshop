<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/common.jsp"%>
<style type="text/css">
/*定义滚动条宽高及背景，宽高分别对应横竖滚动条的尺寸*/
.con-con::-webkit-scrollbar{
    width: 10px;
    background-color: #fff;
}
/*定义滚动条的轨道，内阴影及圆角*/
.con-con::-webkit-scrollbar-track{
    background: #ebebeb!important;
    width: 10px!important;
    border-radius: 6px!important;
}
/*定义滑块，内阴影及圆角*/
.con-con::-webkit-scrollbar-thumb{
    border: #c7c7c7 1px solid;
    border-radius: 7px;
    background: #c7c7c7;
    cursor: pointer;
}
</style>
             <div class="con-tit">
                  <a href="javascript:void(0);" class="on">选择优惠券和红包</a>
                </div>
                <div class="con-con">
                  <div class="use-cou" id="activeCouList">
                    <ul>
                    	<!-- 可用红包和优惠券 -->
                    	<c:forEach items="${requestScope.usableUserCouponList}" var="coupon" varStatus="status">
                   		 <li>
	                        <div class="c-detail <c:if test="${coupon.couponProvider=='platform'}">red</c:if> <c:if test='${coupon.selectSts==1}'> on </c:if>" ucid="${coupon.userCouponId}" price="${coupon.offPrice}">
	                          <div class="c-detail-up">
	                            <div class="c-top-bg"></div>
	                            <div class="cancel-sel">取消勾选</div>
	                            <div class="c-price"><em>¥${coupon.offPrice}</em></div>
	                            <div class="c-limit"><span>满${coupon.fullPrice}</span></div>
	                            <div class="c-time c-time-dong"><span><fmt:formatDate value="${coupon.startDate}" pattern="yyyy-MM-dd HH:mm" /> - <fmt:formatDate value="${coupon.endDate}" pattern="yyyy-MM-dd HH:mm" /></span></div>
	                          </div>
	                          <div class="c-detail-down">
	                           <c:choose>
		                          		<c:when test="${coupon.couponProvider=='platform'}">
		                          			    <span class="c-type-l">[红包券]</span>    
		                          				<c:choose>
					                          		<c:when test="${coupon.couponType=='common'}">
					                          			 <span class="c-type-r">[全场通用]</span>
					                          		</c:when>
					                          		<c:when test="${coupon.couponType=='shop'}">
					                          			 <span class="c-type-r">[限店铺]</span>
					                          		</c:when>
					                          	</c:choose>
		                          		</c:when>
		                          		<c:when test="${coupon.couponProvider=='shop'}">
		                          			    <span class="c-type-l">[优惠券]</span>
		                          			    <c:choose>
					                          		<c:when test="${coupon.couponType=='common'}">
					                          			 <span class="c-type-r">[店铺通用]</span>
					                          		</c:when>
					                          		<c:when test="${coupon.couponType=='product'}">
					                          			 <span class="c-type-r">[限商品]</span>
					                          		</c:when>
					                          	</c:choose>
		                          		</c:when>
		                          	</c:choose>
	                           
	                          </div>
	                        </div>
	                      </li>
                    	</c:forEach>
                    </ul>
                  </div>
                  <div class="cou-hr"></div>
                  <!-- 不可用优惠 -->
                  <div class="use-cou">
                    <ul>
                    	<!-- 不可用 优惠券和红包 -->
                    	<c:forEach items="${requestScope.disableUserCouponList}" var="coupon" varStatus="status">
                    		  <li>
		                        <div class="c-detail gra">
		                          <div class="c-detail-up">
		                            <div class="c-top-bg"></div>
		                            <div class="cancel-sel">取消勾选</div>
		                            <div class="c-price"><em>¥${coupon.offPrice}</em></div>
		                            <div class="c-limit"><span>满${coupon.fullPrice}</span></div>
		                            <div class="c-time c-time-dong"><span><fmt:formatDate value="${coupon.startDate}" pattern="yyyy-MM-dd HH:mm" /> - <fmt:formatDate value="${coupon.endDate}" pattern="yyyy-MM-dd HH:mm" /></span></div>
		                          </div>
		                          <div class="c-detail-down">
		                            <c:choose>
		                          		<c:when test="${coupon.couponProvider=='platform'}">
		                          			    <span class="c-type-l">[红包券]</span>    
		                          				<c:choose>
					                          		<c:when test="${coupon.couponType=='common'}">
					                          			 <span class="c-type-r">[全场通用]</span>
					                          		</c:when>
					                          		<c:when test="${coupon.couponType=='shop'}">
					                          			 <span class="c-type-r">[限店铺]</span>
					                          		</c:when>
					                          	</c:choose>
		                          		</c:when>
		                          		<c:when test="${coupon.couponProvider=='shop'}">
		                          			    <span class="c-type-l">[优惠券]</span>
		                          			    <c:choose>
					                          		<c:when test="${coupon.couponType=='common'}">
					                          			 <span class="c-type-r">[店铺通用]</span>
					                          		</c:when>
					                          		<c:when test="${coupon.couponType=='product'}">
					                          			 <span class="c-type-r">[限商品]</span>
					                          		</c:when>
					                          	</c:choose>
		                          		</c:when>
		                          	</c:choose>
		                          </div>
		                        </div>
		                      </li>
                    	</c:forEach>
                    	
                    </ul>
                  </div>
                </div>
<script>
	var usableUserCouponList = '${fn:length(requestScope.usableUserCouponList)}';
	if(Number(usableUserCouponList)>0){
		$(".choose-coupon-tit i").removeClass("down")
		 $("#sel-cou-div").show();
	}
	var freightAmount = '${requestScope.freightAmount}';
	var usableUserCoupons = '${usableCouponList}';
</script>