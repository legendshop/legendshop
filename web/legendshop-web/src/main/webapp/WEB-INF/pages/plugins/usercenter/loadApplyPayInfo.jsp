<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/common.jsp"%>
<html>
<head>
<title>添加用户退款信息</title>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/addressOverlay${_style_}.css'/>" rel="stylesheet"/>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script type="text/javascript"  src="<ls:templateResource item='/resources/templets/js/loadApplyPayInfo.js'/>"></script>

<script type="text/javascript">
		var contextPath = '${contextPath}';
		$(document).ready(function() {
			$("select.combox").initSelect();
		});
</script>
</head>
<body>
<div class="m" id="edit-cont">
	<div class="mc">
		<div class="form">
			<input type="hidden" id="id" value="${prodRepayUserifo.repayUserifoId}">
			<div class="item">
				<span class="label"><em>*</em>用户开户银行：</span>
				<div class="fl">
				<select class="sele" name="bankCode" id="bankCode"   requiredTitle="true"    style="width: 100px; margin-right: 10px;">
                                    <option value="">请选择银行</option>
                                    <option value="城市商业银行" <c:if test="${prodRepayUserifo.bankCode=='城市商业银行'}">selected="selected"</c:if>>城市商业银行</option>
                                    <option value="广东发展银行" <c:if test="${prodRepayUserifo.bankCode=='广东发展银行'}">selected="selected"</c:if>>广东发展银行</option>
                                    <option value="交通银行" <c:if test="${prodRepayUserifo.bankCode=='交通银行'}">selected="selected"</c:if>>交通银行</option>
                                    <option value="浦东银行" <c:if test="${prodRepayUserifo.bankCode=='浦东银行'}">selected="selected"</c:if>>浦东银行</option>
                                    <option value="深圳发展银行" <c:if test="${prodRepayUserifo.bankCode=='深圳发展银行'}">selected="selected"</c:if>>深圳发展银行</option>
                                    <option value="兴业银行" <c:if test="${prodRepayUserifo.bankCode=='兴业银行'}">selected="selected"</c:if>>兴业银行</option>
                                    <option value="邮政储汇" <c:if test="${prodRepayUserifo.bankCode=='邮政储汇'}">selected="selected"</c:if>>邮政储汇</option>
                                    <option value="招商银行" <c:if test="${prodRepayUserifo.bankCode=='招商银行'}">selected="selected"</c:if>>招商银行</option>
                                    <option value="中国工商银行" <c:if test="${prodRepayUserifo.bankCode=='中国工商银行'}">selected="selected"</c:if>>中国工商银行</option>
                                    <option value="中国光大银行" <c:if test="${prodRepayUserifo.bankCode=='中国光大银行'}">selected="selected"</c:if>>中国光大银行</option>
                                    <option value="中国建设银行" <c:if test="${prodRepayUserifo.bankCode=='中国建设银行'}">selected="selected"</c:if>>中国建设银行</option>
                                    <option value="中国民生银行" <c:if test="${prodRepayUserifo.bankCode=='中国民生银行'}">selected="selected"</c:if>>中国民生银行</option>
                                    <option value="中国农业银行" <c:if test="${prodRepayUserifo.bankCode=='中国农业银行'}">selected="selected"</c:if>>中国农业银行</option>
                                    <option value="中国银行" <c:if test="${prodRepayUserifo.bankCode=='中国银行'}">selected="selected"</c:if>>中国银行</option>
                                    <option value="中信实业银行" <c:if test="${prodRepayUserifo.bankCode=='中信实业银行'}">selected="selected"</c:if>>中信实业银行</option>
			    </select>
				<span id="bankCodeNote" class="error-msg"></span></div>
				<div class="clr"></div>
			</div>
			
			
			<div class="item">
				<span class="label">所在地区：</span>
				<div class="fl">
					<select class="combox sele"  id="provinceid"  requiredTitle="true"  childNode="cityid" selectedValue="${prodRepayUserifo.provinceid}"  retUrl="${contextPath}/common/loadProvinces"  style="width: 100px; margin-right: 10px;">
					</select>
					<select class="combox sele"   id="cityid" requiredTitle="true"  selectedValue="${prodRepayUserifo.cityid}"   showNone="false"  parentValue="${prodRepayUserifo.provinceid}" childNode="areaid" retUrl="${contextPath}/common/loadCities/{value}"  style="width: 100px; margin-right: 10px;">
					</select>
					<select class="combox sele"   id="areaid"  requiredTitle="true"  selectedValue="${prodRepayUserifo.areaid}"    showNone="false"   parentValue="${prodRepayUserifo.cityid}"  retUrl="${contextPath}/common/loadAreas/{value}"  style="width: 100px;">
					</select>
					<span class="error-msg" id="areaNote"></span>
				</div>
				<div class="clr"></div>
			</div>
			
			<div class="item">
				<span class="label"><em>*</em>分行信息：</span>
				<div class="fl"><input id="txtbranch" value="${prodRepayUserifo.txtbranch}" class="text text1" type="text"></div>
				<span class="error-msg" id="txtbranchNote" ></span>
				<div class="clr"></div>
			</div>
			
			
			<div class="item">
				<span class="label"><em>*</em>银行用户姓名：</span>
				<div class="fl"><input id="bankUserName" value="${prodRepayUserifo.bankUserName}" class="text text1" type="text"></div>
				<span class="error-msg" id="bankUserNameNote" ></span>
				<div class="clr"></div>
			</div>
			
			<div class="item">
				<span class="label"><em>*</em>用户银行账号：</span>
				<div class="fl"><input id="bankAccount" value="${prodRepayUserifo.bankAccount}" class="text text1" type="text"></div>
				<span class="error-msg" id="bankAccountNote" ></span>
				<div class="clr"></div>
			</div>
			
			<div class="item">
				<span class="label"><em>*</em>再次输入账号：</span>
				<div class="fl">
				  <input id="bankAccountTwo" value="${prodRepayUserifo.bankAccount}" class="text text1" type="text">
				</div>
				<span class="error-msg" id="bankAccountTwoNote" ></span>
				<div class="clr"></div>
			</div>
			
			
			<div class="btns"><a href="javascript:void(0);" onclick="addPayInfo();" class="e-btn gray-btn save-btn">保存信息</a></div>
		</div>
	</div>
</div>
</body>
</html>