<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<div class="clear"></div>
<c:set var="advList" value="${floor.floorItems.RA}"></c:set>
<div class="yt-wrap bf_floor_new clearfix">
	<div class="dayhot">
		<c:forEach items="${advList}" var="adv" varStatus="index" end="1">
			<div class="add_big">
				<a target="_blank" title="${adv.title}" href="${adv.linkUrl}">
					<img class="lazy" alt="${adv.title}" src="${contextPath}/resources/common/images/loading1.gif" data-original="<ls:photo item='${adv.picUrl}'/>"
                         alt="" width="1190" height="300"> </a>
			</div>
		</c:forEach>
	</div>
</div>