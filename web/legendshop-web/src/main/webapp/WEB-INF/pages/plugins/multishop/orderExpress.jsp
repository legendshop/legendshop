<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>物流详情-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">物流详情</span>
				</p>
			</div>
				<%@ include file="included/sellerLeft.jsp" %>
				<div class="right_con" style="margin-top:21px;overflow: unset;">
<div class="right-layout">
	<div class="ncm-flow-layout" style="border: 1px solid #ddd;">
		<div class="ncm-flow-container">
			<div class="title">
				<h3>物流详情</h3>
			</div>
			<div class="alert alert-block alert-info">
				<ul>
				    <c:if test="${not empty order.userAddressSub}"> 
				     <li><strong>收货信息：</strong>${order.userAddressSub.receiver}&nbsp;&nbsp;&nbsp;&nbsp;${order.userAddressSub.mobile}&nbsp;&nbsp;&nbsp;&nbsp;${order.userAddressSub.detailAddress}</li>
					 <li>
					 <strong>买家留言：
					 <c:choose>
					 		<c:when test="${empty order.orderRemark}">无</c:when>
					 		<c:otherwise>${order.orderRemark}</c:otherwise>
				 	 </c:choose>
					 </strong>
					</li>
					</c:if>
					<!-- <li><strong>发货信息：</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li> -->
				</ul>
			</div>
			<div class="tabmenu">
				<ul class="tab pngFix">
					<li class="active"><a>物流动态</a>
					</li>
				</ul>
			</div>
			<ul id="express_list" class="express-log">
               <li class="loading">加载中...</li>
			</ul>
			<div class="alert">以上部分信息来自于第三方，仅供参考，如需准确信息可联系卖家或物流公司</div>
		</div>
		<div class="ncm-flow-item">
			<div class="title">订单信息</div>
			<div class="item-goods">
			  	<!-- S 商品列表 -->
				<c:forEach items="${order.subOrderItemDtos}" var="orderItem" varStatus="orderItemStatues">
					   <dl>
						<dt>
							<div class="ncm-goods-thumb-mini">
								<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">
								<img src="<ls:images item='${orderItem.pic}' scale='3'/>">
								</a>
							</div>
						</dt>
						<dd>
							<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">${orderItem.prodName}</a> <span class="rmb-price">${orderItem.cash}&nbsp;*&nbsp;${orderItem.basketCount}
							</span>
						</dd>
					 </dl>
				</c:forEach>
				
			</div>
			<div class="item-order">
				<dl>
					<dt>运费：</dt>
					<dd>${order.freightAmount}</dd>
				</dl>
				<dl>
					<dt>订单总额：</dt>
					<dd>
						<strong>¥${order.actualTotal}</strong>
					</dd>
				</dl>
				<dl>
					<dt>订单编号：</dt>
					<dd>
						<a href="${contextPath}/s/orderDetail/${order.subNum}">${order.subNum}</a><a class="a"
							href="javascript:void(0);">更多<i class="icon-angle-down"></i>
							<div class="more">
								<span class="arrow"></span>
								<ul>
									<li>支付方式：<span>${order.payManner==1?'货到付款':'在线支付'}</span></li>
									  <li>下单时间：<span><fmt:formatDate value="${order.subDate}" type="both" /></span></li>
									<c:if test="${not empty order.payDate}">
									  <li>付款时间：<span><fmt:formatDate value="${order.payDate}" type="both" pattern="yyyy-MM-dd"/></span></li>
									</c:if>
									<c:if test="${not empty order.dvyDate}">
									  <li>发货时间：<span><fmt:formatDate value="${order.dvyDate}" type="both" /></span></li>
									</c:if>
									<c:if test="${not empty order.finallyDate}">
									  <li>完成交易时间：<span><fmt:formatDate value="${order.finallyDate}" type="both" /></span></li>
									</c:if>
								</ul>
							</div> </a>
					</dd>
				</dl>
				  <c:if test="${not empty order.delivery}">
				    <dl>
					   <dt>物流单号：</dt>
					   <dd>
					  	${order.delivery.dvyFlowId}<a target="_blank" class="a" href="${order.delivery.delUrl}">${order.delivery.delName}</a>
					   </dd>
				      </dl>
				  </c:if>
				<dl>
					<dt>商&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家：</dt>
					<dd>
						${order.shopName}
					<!-- 	<a class="a" href="javascript:void(0);">更多<iclass="icon-angle-down"></i>
							<div class="more">
								<span class="arrow"></span>
								<ul>
									<li>所在地区：<span>xx</span>
									</li>
									<li>联系电话：<span></span>
									</li>
								</ul>
							</div> 
						</a> -->
						     <div>
						       <span member_id="2"></span>
						    </div>
					</dd>
				</dl>
			</div>
		</div>
	</div>
	
</div>
</div>
<div class="clear"></div>

 </div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script>
		$(function() {
		   userCenter.changeSubTab("ordersManage");
		   var dvyFlowId="${order.delivery.dvyFlowId}";
	       var dvyTypeId="${order.delivery.dvyTypeId}";
	       var reciverMobile="${order.userAddressSub.mobile}";
	       var html="";
	       if(dvyFlowId=="" || dvyTypeId ==""){
	          html="<li>没有物流信息</li>";
	       }
	       else{
	             $.ajax({
					url:"${contextPath}/s/order/getExpress", 
					data:{"dvyFlowId":dvyFlowId,"dvyId":dvyTypeId,"reciverMobile":reciverMobile},
					type:'POST', 
					async : false, //默认为true 异步   
					dataType:'json',
					success:function(result){
					    if(result=="fail" || result=="" ){
					       html="没有物流信息";
					       loaddely=true;
						}else{
						     var list = eval(result);
							 for(var obj in list){ //第二层循环取list中的对象 
							    html+="<li>"+list[obj].ftime+"&nbsp;&nbsp;"+list[obj].context+"<li>";
							 } 
						}
					}
			      });
	      }
	      $('#express_list').html(html);
	       
	       
		  
		});

	</script>
</body>
</html>