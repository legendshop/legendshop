<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>预存款余额提现 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/pdCashManager${_style_}.css'/>" rel="stylesheet" />
	 
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
   <%@ include file="/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp" %>
    <!-----------right_con-------------->
     <div class="right_con">
<div>
	<div class="o-mt">
		<h2>账户信息</h2>
	</div>
	<div class="pagetab2">
		<ul>
			<li><span><a href="<ls:url address='/p/predeposit/account_balance'/>">账户余额</a> </span></li>
			<li ><span><a href="<ls:url address='/p/predeposit/recharge_detail'/>">充值明细</a> </span></li>
			<li  class="on"><span>余额提现</span></li>
		    <li ><span><a href="<ls:url address='/p/predeposit/balance_hoding'/>">冻结中余额</a> </span></li>
		</ul>
	</div>

	<div>
		<%-- <div class="tabmenu">
			<a href="<ls:url address='/p/predeposit/predepositApply'/>" class="ncbtn" style="right: -5px;width:53px;">申请提现</a>
		</div> --%>
		<div class="alert">
			<span class="mr30">可用金额：<strong class="mr5 red"
				style="font-size: 18px;"><fmt:formatNumber value="${predeposit.availablePredeposit}" pattern="#,#0.00#"/></strong>元</span><span>冻结金额：<strong
				class="mr5 blue" style="font-size: 18px;"><fmt:formatNumber value="${predeposit.freezePredeposit}" pattern="#,#0.00#"/></strong>元</span>
			<span style="float:right;margin-top: 2px;">
				<a href="<ls:url address='/p/predeposit/predepositApply'/>" class="btn-r">申请提现</a>
			</span>
		</div>

		<div class="zhss">
		  <div style="float: left;margin-right: 5px;">
		       <span>状态：</span>
		      <select name="paystate_search" id="paystate_search">
				<option value="">-请选择-</option>
				<option value="0" <c:if test="${status==0}"> selected="selected" </c:if>>正在处理</option>
				<option value="1" <c:if test="${status==1}">selected="selected" </c:if>>已成功</option>
			  </select> <span>申请单号</span> 
			   <input type="hidden" value="${empty curPageNO?1:curPageNO}" name="curPageNO"  id="curPageNO"> 
			   <input type="text" value="${pdcSn}" name="pdcSn" id="pdcSn" class="text w150"> 
		   </div>
			<div style="float: left;">
		      <label class="submit-border">
				<input type="button" onclick="searcBalanceWithdrawal();" value="搜索" class="submit btn-r">
			  </label>
		   </div>
		</div>
		<div id="recommend" class="recommend" style="display: block;">
		<table class="ncm-default-table sold-table">
			<thead>
				<tr>
					<th width="250">申请单号</th>
					<th width="150">申请时间</th>
					<th width="100">提现金额（元）</th>
					<th width="100">状态</th>
					<th width="100">操作</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
		           <c:when test="${empty requestScope.list}">
		              <tr>
					 	 <td colspan="20">
					 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
					 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
					 	 </td>
					 </tr>
		           </c:when>
		           <c:otherwise>
		              <c:forEach items="${predeposit.pdCashs.resultList}" var="pdCash" varStatus="status">
						   <tr>
								<td>${pdCash.pdcSn}</td>
								<td><fmt:formatDate value="${pdCash.addTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
								<td><fmt:formatNumber value="${pdCash.amount}" type="currency"/></td>
								<td>
								<c:choose>
										<c:when test="${pdCash.paymentState==0}">
										     在处理过程中
										</c:when>
										<c:when test="${pdCash.paymentState==1}">
										 提现成功
										</c:when>
								</c:choose>
								</td>
								 <td>
								   <span>
									    <a class="btn-r" href="<ls:url address='/p/predeposit/withdrawalDetail/${pdCash.id}'/>">查看</a>
										<c:if test="${pdCash.paymentState==0}">
										   <a class="btn-g" onclick="cancelWithdrawals('${pdCash.id}');" href="javascript:void(0);">取消申请</a>
										</c:if>
									</span>
								</td>
							</tr>
						</c:forEach>
		           </c:otherwise>
	           </c:choose>
			</tbody>
		</table>
	</div>
	</div>
	<!-- 页码条 -->
		<div style="margin-top:10px;" class="page clearfix">
			<div class="p-wrap">
					<span class="p-num">
						<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
					</span>
			 </div>
		</div>
		<div class="clear"></div>
</div>

</div>
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
		<script src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/templets/js/withdrawals.js'/>" type="text/javascript"></script>
<script type="text/javascript">
      var contextPath = '${contextPath}';
	$(document).ready(function() {
		userCenter.changeSubTab("mydeposit");
	});
	
	function pager(curPageNO){
	     window.location.href=contextPath+"/p/predeposit/balance_withdrawal?curPageNO="+curPageNO;
     }
</script>  
</body>
</html>
			
