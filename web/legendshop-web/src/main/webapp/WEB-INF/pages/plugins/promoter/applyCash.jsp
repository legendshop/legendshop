<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>申请提现</title>
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;">     
       <%@include file='/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp'%>
    
     <div class="right_con">
     
         <div class="o-mt"><h2>申请提现</h2></div>
          
          <div class="pagetab2">
                 <ul id="switchTab">           
                    <li class="on" ><span><a href="${contextPath }/p/promoter/toApplyCash">申请提现</a></span></li>
                    <li><span><a href="${contextPath }/p/promoter/withdrawList">提现记录</a></span></li>
                 </ul>       
          </div>

         <div class="apply-withdraw">
           <div id="applyWithDraw" class="con">
	           <form:form id="form1" action="${ contextPath}/p/promoter/applyCash" method="POST">
	           	<input style="display:none;"/>
	             <ul>
	               <li><span><em>*</em>提现金额：</span><input type="text" id="money" name="money" value="<fmt:formatNumber value="${userCommis.settledDistCommis }" pattern="#0.00#"/>"/></li>
	               <li><span></span><i>温馨提示：</i>本次最多可提现<i><fmt:formatNumber value="${userCommis.settledDistCommis }" pattern="#0.00#"/></i>元 , 提现的款项会到您的预存款账户里.</li>
	               <li><span><em>*</em>支付密码：</span><input type="password" id="passwd" name="passwd" /></li>
	               <li><span></span>如果没有设置支付密码，请到 <a href="${contextPath}/p/security"><i>账户安全 </i></a>设置!</li>
	               <li style="margin-top: 15px;"><input id="withdrawBtn" type="submit" value="确认提现"></li>
	             </ul>
	           </form:form>
           </div>
         </div>
     </div>
    <!--right_con end-->
    
    <div class="clear"></div>
 </div>
    <!--right_con end-->
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>"  type="text/javascript"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script> 	
<script type="text/javascript">
  var contextPath = "${contextPath}";
  var settledDistCommis = parseFloat('${userCommis.settledDistCommis }');
    

</script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/applyCash.js'/>"></script>
</body>
</html> 