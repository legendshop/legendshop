<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

			<ul class="area-list">       
				<c:forEach items="${cityList}" var="city">
					<li><a data-value="${city.id}" href="#none">${city.city }</a></li>
				</c:forEach>
			</ul>

<script type="text/javascript">
jQuery(document).ready(function($) {
	  //配送至 选择框 城市选择
	  jQuery("#stock_city_item ul[class=area-list] li").bind("click", function() {
	 		var cityId = $(this).find("a").attr("data-value");
	 		var city =$(this).find("a").html();

	 		$("#selCityId").val(cityId);
	 		$("#stockTab li[data-index=1] a em").html(city);
	 		
	 		 jQuery.ajax({
					url:"${contextPath}/load/areaList/"+cityId,
					type:'post', 
					async : true, //默认为true 异步   
					success:function(result){
					  $("#stock_area_item").html(result);
					  
					  $("#stockTab li").each(function(i){
				    	 	$(this).removeClass("curr");
				    	 	$(this).find("a").removeClass("hover");
				    });
					 
					var areaTab = $("#stockTab li[data-index=2]");
					areaTab.addClass("curr");
					areaTab.find("a").addClass("hover");
					areaTab.find("em").html("请选择");
					areaTab.attr("style","display: block;");
					
					 $("#JD-stock div[class=mc]").each(function(){
						 $(this).attr("style","display: none;");
					 });
					 $("#stock_area_item").attr("style","display: block;");
					}
					});
	  });
});
</script>