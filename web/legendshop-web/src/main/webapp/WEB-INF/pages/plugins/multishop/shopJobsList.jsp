<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>职位管理-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-sub-account${_style_}.css'/>" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">职位管理</span>
				</p>
			</div>
			<%@ include file="included/sellerLeft.jsp" %>
			<div class="sub-account">
				<div class="pagetab2">
					<ul id="listType">
						<li class="on"><span>职位管理</span></li>
					</ul>
	   			</div>
				<div class="set-up sold-ser sold-ser-no-bg">
					<input type="button" class="btn-r" onclick="createJob();" value="创建职位">
				</div>
				<form:form  action="${contextPath}/s/shopJobs/query" method="post" id="from1">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
				</form:form>
				<table class="account-table sold-table">
					<tr class="account-tit">
						<th>职位名称</th>
						<th>创建时间</th>
						<th width="150">操作</th>
					</tr>
					<c:if test="${empty requestScope.list}">
						<tr><td colspan='3' height="40">请先创建职位</td></tr>
					</c:if>	
					<c:forEach items="${requestScope.list}" var="shopRole" varStatus="status">			
						<tr class="detail">
							<td>${shopRole.name}</td>
							<td><fmt:formatDate value="${shopRole.recDate}" pattern="yyyy-MM-dd"/></td>
							<td>
								<a href="javascript:void(0);" onclick="updateJob(${shopRole.id});" title="修改" class="btn-g" style="margin-right:10px;">修改</a><a href="javascript:void(0);" onclick="delJob(${shopRole.id});" title="删除" class="btn-g">删除</a>
							</td>
						</tr>
					</c:forEach>
				</table>
				<div class="clear"></div>
					<div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
				            		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
				     			 </div>
		  				</div>
			</div>
		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
		</div>
		<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/shopJobsList.js'/>"></script>
			<script type="text/javascript">
				var contextPath="${contextPath}";
			</script>
</body>
</html>