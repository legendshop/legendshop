<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>标签管理-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-prod${_style_}.css'/>" rel="stylesheet">
   	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<a href="${contextPath}/s/tagManage">标签管理</a>>
					<span class="on">绑定管理</span>
				</p>
			</div>
				<%@ include file="included/sellerLeft.jsp" %>
			<div class="seller-selling">
				<div class="seller-com-nav">
					<ul>
						<li id="tagManage"><a href="${contextPath}/s/tagManage">标签管理</a></li>
						<li class="on" id="shopCatConfig">
							<a href="javascript:void(0);">
								绑定管理
							</a>
						</li>
					</ul>
				</div>
				<form:form action="${contextPath}/s/bindProdManage/${prodTag.id }" method="post" id="from1">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
						<div class="selling-sea">
							<p>商品名称：</p>
							<input type="text" value="${prodName}" name="prodName"  id ="prodName">
							<input type="submit" class="serach" id="btn_keyword" value="搜  索" >	
						</div>						
				</form:form>									
				
<!-- 搜索 -->
				<table class="selling-table">
					<tr class="selling-tit">
						<td class="check"><input type="checkbox" id="checkbox" onclick="selAll();"></td>
						<td>商品图片</td>
						<td>商品名</td>														
					</tr>
					<c:if test="${empty requestScope.list}">
						<tr><td colspan='7' height="60">没有找到符合条件的商品</td></tr>
					</c:if>				
					<c:forEach items="${requestScope.list}" var="prodTagRel" varStatus="status">
						<tr class="detail">
							<td class="check">
                         		<input name="strArray" type="checkbox" value="${prodTagRel.prodId}"  arg="">
							</td>
							<td><a href="${contextPath }/views/${prodTagRel.prodId}"  target="_blank"><img src="<ls:images item='${prodTagRel.pic}' scale='3'/>" /></a></td>	 
							<td>${prodTagRel.prodName}</td>																	
						</tr>
					</c:forEach>
				</table>
				<div class="selling-cha">
					<span><a class="bat-oper-btn" href="javascript:void(0);" onclick="deleteAction();">删除绑定<img src="<ls:templateResource item='/resources/templets/images/delete.png'/>" alt=""></a></span>										
					<span><a class="bat-oper-btn" href="javascript:void(0);" onclick="bindProd('${prodTag.id}');">绑定商品</a></span>
				</div>
				<div class="clear"></div>
					<div style="margin-top:10px;" class="page clearfix">
			     			 <div class="p-wrap">
			            		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			     			 </div>
		  			</div>
			</div>
		</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/bindProdManage.js'/>"></script>
	<script type="text/javascript">
		var contextPath="${contextPath}";
	</script>
</body>
</html>