<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="renderer" content="webkit"/>
    <c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
     <title>订单提交成功-${systemConfig.shopName}-${systemConfig.title}</title>
	  <meta name="keywords" content="${systemConfig.keywords}"/>
	  <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
</head>
<body  class="graybody" style="background-color: #ffffff;">
	<div id="doc">
         <!--顶部menu-->
		   <%@ include file="home/top.jsp" %>
    
       <div id="bd" >
      
      <!--order content -->
     	   <center >
	         <div class="pay-success">
	          	<p><img alt="" src="<ls:templateResource item='/resources/templets/images/gou2.png'/>">支付成功！</p>
	            <span>恭喜您已经成功支付，现在您可以：</span>
				 
					<c:if test="${subSettlementType!='USER_PRED_RECHARGE' && subSettlementType!='PRESELL_ORDER_PAY'}">
						<a href="${contextPath}/p/myorder?uc=uc">返回我的订单</a>
					</c:if>
					
					<c:if test="${subSettlementType =='PRESELL_ORDER_PAY'}">
						<a href="${contextPath}/p/myorder?uc=uc">返回我的订单</a>
					</c:if>
	
					<c:if test="${subSettlementType=='USER_PRED_RECHARGE'}">
						 <a href="${contextPath}/p/predeposit/account_balance">返回我的预存款</a>
					</c:if>

	            <br>
	            <input type="text" onclick="window.location.href='../../../..'" value="继续逛逛">
	          </div>
          </center>
			<!--order content end-->
			      
	   </div>
		 <!----foot---->
		 <%@ include file="home/bottom.jsp" %>
        <!----foot end---->
	</div>
</body>
</html>
