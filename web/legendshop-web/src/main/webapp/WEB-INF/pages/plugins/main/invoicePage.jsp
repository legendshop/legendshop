<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
 <ul>
    <c:forEach  items="${requestScope.list}" var="invoice"   varStatus="status">
      <li>
	     <c:choose>
			<c:when test="${invoice.commonInvoice ==1}">
				<%-- <input type="radio" value="${invoice.id}" name="invoiceId" id="inv_${invoice.id}" checked="checked" /> --%>
				<label class="radio-wrapper radio-wrapper-checked">
					<span class="radio-item">
						<input type="radio" class="radio-input" value="${invoice.id}" name="invoiceId" id="inv_${invoice.id}" checked="checked">
						<span class="radio-inner"></span>
					</span>
				</label>
			</c:when>
			<c:otherwise>
				<%-- <input type="radio" value="${invoice.id}" name="invoiceId"  id="inv_${invoice.id}"  /> --%>
				<label class="radio-wrapper">
					<span class="radio-item">
						<input type="radio" class="radio-input" value="${invoice.id}" name="invoiceId"  id="inv_${invoice.id}">
						<span class="radio-inner"></span>
					</span>
				</label>
			</c:otherwise>
		 </c:choose>
	      <label for="inv_${invoice.id}">
	                  <c:if test="${invoice.type ==1}">普通发票</c:if>&nbsp;&nbsp;
	                                                  <c:choose><c:when test="${invoice.title ==1}">个人</c:when><c:otherwise>单位</c:otherwise></c:choose><c:if test="${not empty invoice.company}">( ${invoice.company} )</c:if><c:if test="${not empty invoice.invoiceHumNumber}">( ${invoice.invoiceHumNumber} )</c:if>&nbsp;&nbsp;
	                                                  <c:choose>
															<c:when test="${invoice.content==1}">不开发票</c:when>
															<c:otherwise>明细</c:otherwise>
					</c:choose>
	        </label>
				  <a onclick="DelInvoice('${invoice.id}');"  href="javascript:void(0);">[ 删除 ]</a>
	   </li>
    </c:forEach>
       <c:if test="${fn:length(requestScope.list)<=10}">
     <li>
	    <!-- <input type="radio" name="invoiceId" id="inv" in="new" > -->
	    <label class="radio-wrapper">
			<span class="radio-item">
				<input type="radio" class="radio-input" name="invoiceId" id="inv" in="new">
				<span class="radio-inner"></span>
			</span>
		</label>
	    
	    <label for="inv">使用新的发票信息</label>
	 </li>
   </c:if>
</ul>
<script type="text/javascript">
	 $("input:radio[name='invoiceId']").change(function(){
		 $("input:radio[name='invoiceId']").each(function(){
			 if(this.checked){
				 $(this).parent().parent().addClass("radio-wrapper-checked");
			 }else{
				 $(this).parent().parent().removeClass("radio-wrapper-checked");
			 }
	 	});
	     var _new=$(this).attr("in");
	     if(_new!=null && _new!="" && _new!=undefined){
	        $(".invoice-spr .invoice-spr-cho").show();
	        $(".invoice-spr .invoice-but").show();
	     }else{
	       $(".invoice-spr .invoice-spr-cho").hide();
	     }
	 });
	 
	 /*  $("input:radio[name='invoince_pttt']").change(function(){
	     
	     if($(this).val()==1){
	       $("#invoice_unitNameTr").hide();
	       $("#invoice_Unit_TitName").val("");
	     }else{
	        $("#invoice_unitNameTr").show();
	     }
	 }); */
	 
	 
</script>
