<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<html>
<head>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
	<style type="text/css">
		html{font-size:13px;}
		table{margin: 10px; border: 1px solid #ddd;}   
		table tbody tr{height:70px;}
		table tbody tr td{border-bottom:1px solid #ddd;text-align: center;}
		table thead tr td{border-bottom:1px solid #ddd;text-align: center;padding:5px;background: #f9f9f9;}
	</style>
</head>
<body>
	<form:form id="form1">
	 	<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
	 	<input type='hidden' id='tagId' value='${tagId}'/>	 	
	 	<div style="height:50px;background-color: #f9f9f9;border: 1px solid #ddd;font-size: 14px;margin: 10px;">
	 		<div style="padding-top:10px;padding-left:20px;">
			<input style="width:550px;border:1px solid #ddd;padding: 5px;"  type="text" name="name" id="name" maxlength="20" value="${prod.name}" size="20" placeholder="请输入商品名称" />
			<input type="button" id="submit" value="搜索" style="outline: none;background: #e5004f;color: #fff;padding: 5px 10px;border: 0px;cursor: pointer;"/>			
			</div>
		</div>		
	</form:form>
	
	<div id="prodListTable">
	 
	</div>
		<div style="height:250px; overflow:scroll;">
			<table class="prodList">			
				<thead>
					<tr><td width="150">图片</td><td width="450">名称</td><td width="110">操作</td></tr>
				</thead>
			</table>
			<div style="width:100px; margin:0px auto"><input type="button" value="保存" onclick="submitProd()" style="width:100px; height:30px;outline: none;background: #e5004f;color: #fff;padding: 5px 10px;border: 0px;cursor: pointer;"/></div>
		</div>
<script src="${contextPath}/resources/common/js/jquery-1.9.1.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/loadProdListByTag.js'/>"></script>
<script type="text/javascript">
	var contextPath="${contextPath}";
	function addProd(prodId,name,pic){

		if(prodId==null||prodId==""||prodId==undefined){
			alert("商品Id为空");
		}

		var config=true;
		$("table[class='prodList'] tr").each(function (index,domEle){

			var prod_id=$(this).find("#prodId").val();				
			if(prod_id==prodId){
				alert("该商品已存在");
				config=false;
			}
		});
		if(!config){
			return;
		}

		var tableLength =  $("table[class='prodList']").length;
		//拼接数据
		var str ="<tr>"+
		"<td><input type='hidden' id='prodId' value='"+prodId+"'/><img src='<ls:images item='"+pic+"' scale='3' />' ></td>"+
		"<td>"+name+"</td>"+
		"<td><a href='javascript:void(0)' onclick='deleteProd(this)' style='cursor: pointer;text-decoration: none;color:#333;padding: 5px;background: #999;color: #fff;'>"+
		"删除</a></td></tr>";

		//追加到Table

		$("table[class='prodList']").append(str);
		//parent.addProdTo(prodid,name,price);

	}
</script>
</body>
</html>