<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<jsp:useBean id="now" class="java.util.Date"/>
<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss" var="nowDate"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>秒杀活动管理-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css' />" rel="stylesheet">
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/active/actives${_style_}.css'/>"
          rel="stylesheet">
    <link type="text/css"
          href="<ls:templateResource item='/resources/templets/css/seckill/seckill-activity${_style_}.css'/>"
          rel="stylesheet">


</head>
<body class="graybody">
<div id="doc">
    <%@ include
            file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
    <div class="w1190">
        <div class="seller-cru">
            <p>
                您的位置> <a href="${contextPath}/home">首页</a>> <a
                    href="${contextPath}/sellerHome">卖家中心</a>> <span class="on">秒杀活动管理</span>
            </p>
        </div>

        <%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
        <div id="rightContent" class="right_con">
            <!--page-->
            <!-- 秒杀活动列表 -->
            <div class="seckill-activity">
                <div id="statusTab" class="seckill-nav">
                    <span <c:if test="${empty seckillActivity.searchType}">class="on"</c:if> ><a
                            href="<ls:url address="/s/seckillActivity/query"/>">全部活动</a></span>
                    <span <c:if test="${seckillActivity.searchType eq 'WAIT_AUDIT'}">class="on"</c:if>><a
                            href="<ls:url address="/s/seckillActivity/query?searchType=WAIT_AUDIT"/>">待审核</a></span>
                    <span <c:if test="${seckillActivity.searchType eq 'NOT_PASS'}">class="on"</c:if> ><a
                            href="<ls:url address="/s/seckillActivity/query?searchType=NOT_PASS"/>">未通过</a></span>
                    <span <c:if test="${seckillActivity.searchType eq 'NOT_STARTED'}">class="on"</c:if> ><a
                            href="<ls:url address="/s/seckillActivity/query?searchType=NOT_STARTED"/>">未开始</a></span>
                    <span <c:if test="${seckillActivity.searchType eq 'ONLINE'}">class="on"</c:if>><a
                            href="<ls:url address="/s/seckillActivity/query?searchType=ONLINE"/>">进行中</a></span>
                    <span <c:if test="${seckillActivity.searchType eq 'FINISHED'}">class="on"</c:if> ><a
                            href="<ls:url address="/s/seckillActivity/query?searchType=FINISHED"/>">已结束</a></span>
                    <span <c:if test="${seckillActivity.searchType eq 'EXPIRED'}">class="on"</c:if>><a
                            href="<ls:url address="/s/seckillActivity/query?searchType=EXPIRED"/>">已失效</a></span>
                </div>
                <div class="search-add" style="margin-top: 15px">
                    <div class="sold-ser sold-ser-no-bg clearfix" style="overflow:hidden">
                        <div class="item">
                            <input class="btn-r big-btn" type="submit" value="立即添加"
                                   onclick="window.location='${contextPath}/s/seckillActivity/addSeckillActivity'">
                        </div>

                        <form:form id="searchseckillActivity" action="${contextPath}/s/seckillActivity/query"
                                   style="float:right;display:inline-block" method="get">
                            <div class="fr">
                                <div class="item">
                                    活动名称:
                                   <input type="hidden" value="${empty curPageNO?1:curPageNO}" name="curPageNO"
                                           id="curPageNO">
                                    <input type="text" value="${seckillActivity.seckillTitle}" name="seckillTitle"
                                           id="seckillTitle" class="text item-inp" style="width:300px;" placeholder="请输入活动名称">
                                    <input type="submit" value="搜索" id="search" class="submit btn-r" style="display:inline-block">
                                </div>
                            </div>
                        </form:form>

                    </div>

                </div>
                <table class="seckill-table sold-table">
                    <tr class="seckill-tit">
                        <th width="250">秒杀活动标题</th>
                        <th width="150">活动时间</th>
                        <th width="180">活动状态</th>
                        <th width="150">操作</th>
                    </tr>
                    <tbody>
                    <c:choose>
                        <c:when test="${empty requestScope.list}">
                            <tr>
                                <td colspan="20">
                                    <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
                                    <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span>
                                    </div>
                                </td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <c:forEach items="${list}" var="seckill" varStatus="status">
                                <tr class="detail">
                                    <td style="min-width:200px;">${seckill.seckillTitle}</td>
                                    <fmt:formatDate value="${seckill.startTime}" var="startTime"
                                                    pattern="yyyy-MM-dd HH:mm:ss"/>
                                    <fmt:formatDate value="${seckill.endTime}" var="endTime"
                                                    pattern="yyyy-MM-dd HH:mm:ss"/>
                                    <td style="width:20%">${startTime}<span style="display: block">至</span>${endTime}</td>

                                    <td style="width:13%">
                                        <c:if test="${seckill.status==2}">
                                            完成
                                        </c:if>
                                        <c:if test="${seckill.status!=2}">
                                            <c:choose>
                                                <c:when test="${seckill.endTime lt now}">
                                                    <span class="seckill-sta">已结束</span>
                                                </c:when>
                                                <c:when test="${empty seckill.prodStatus || seckill.prodStatus eq -1 || seckill.prodStatus eq -2}">
                                                    <span class="seckill-sta">商品已删除</span>
                                                </c:when>
                                                <c:when test="${seckill.prodStatus eq 0}">
                                                    <span class="seckill-sta">商品已下线</span>
                                                </c:when>
                                                <c:when test="${seckill.prodStatus eq 2}">
                                                    <span class="seckill-sta">商品违规下线</span>
                                                </c:when>
                                                <c:when test="${seckill.prodStatus eq 3}">
                                                    <span class="seckill-sta">商品待审核</span>
                                                </c:when>
                                                <c:when test="${seckill.prodStatus eq 4}">
                                                    <span class="seckill-sta">商品审核失败</span>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:if test="${seckill.status eq -2}">
                                                        <span class="seckill-sta">审核未通过</span>
                                                        <span class="question-mark"></span>
                                                        <input type="hidden" id="auditOpinion" value="${seckill.auditOpinion}">
                                                        <span id="opinion" class="auctions-sta opinion" style="display:none;">
														<div id="history" class="more"></div>
													</span>
                                                    </c:if>
                                                    <c:if test="${seckill.status eq -1}">
                                                        <span class="seckill-sta">审核中</span>
                                                    </c:if>
                                                    <c:if test="${seckill.status eq 0}">
                                                        <span class="seckill-staed">已失效</span>
                                                    </c:if>
                                                    <c:if test="${seckill.status eq 1}">
                                                        <c:choose>
                                                            <c:when test="${seckill.startTime gt now}">
                                                                <span class="seckill-sta">未开始</span>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <span class="seckill-staing">进行中</span>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:if>
                                                    <c:if test="${seckill.status eq 4}">
                                                        <span class="seckill-sta">已终止</span>
                                                    </c:if>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:if>
                                    </td>

                                    <td>
                                        <ul class="mau" style="min-width:300px;">
                                            <c:choose>


                                                <c:when test="${empty seckill.prodStatus || seckill.endTime < nowDate}">
                                                    <a href="${contextPath}/s/seckillActivity/seckillActivityDetail/${seckill.id}" class="btn-r">查看</a>
                                                    <a href="${contextPath }/s/seckillActivity/operatorSeckillActivity/${seckill.id}" class="btn-r">运营</a>
                                                    <a href="javascript:void(0);" class="blue_button del btn-g" onclick="deleteShopseckillActivity('${seckill.id}')">删除</a>
                                                </c:when>
                                                <c:otherwise>

                                                    <%-- 待审核 或  未通过 --%>
                                                    <c:if test="${seckill.status eq -1 || seckill.status eq -2}">
                                                        <a href="${contextPath} /s/seckillActivity/editSeckillActivity/${seckill.id}" class="btn-g">编辑</a>
                                                        <a href="javascript:void(0);" class="blue_button del btn-g" onclick="deleteShopseckillActivity('${seckill.id}')">删除</a>
                                                    </c:if>
                                                    <%-- 待审核 或 未通过 --%>

                                                    <%-- 未开始 --%>
                                                    <c:if test="${seckill.status eq 1 && seckill.startTime > nowDate}">
                                                        <a href="${contextPath} /s/seckillActivity/editSeckillActivity/${seckill.id}" class="btn-g">编辑</a>
                                                    </c:if>
                                                    <%-- 未开始 --%>

                                                    <%-- 进行中 --%>
                                                    <c:if test="${seckill.status eq 1 && seckill.startTime < nowDate}">
                                                        <a href="/s/seckillActivity/seckillActivityDetail/${seckill.id}" class="btn-r">查看</a>
                                                        <a href="${contextPath }/s/seckillActivity/operatorSeckillActivity/${seckill.id}" class="btn-r">运营</a>
                                                        <%--<a href="javascript:void(0);" onclick="offlineShopseckillActivity('${seckill.id}')" class=" btn-g">终止</a>--%>
                                                    </c:if>
                                                    <%-- 进行中 --%>

                                                    <%-- 已失效 或已结束 --%>
                                                    <c:if test="${seckill.status eq 0 || seckill.status eq 2 || seckill.status eq 4}">
                                                        <a href="/s/seckillActivity/seckillActivityDetail/${seckill.id}" class="btn-r">查看</a>
                                                        <a href="${contextPath }/s/seckillActivity/operatorSeckillActivity/${seckill.id}" class="btn-r">运营</a>
                                                        <a href="javascript:void(0);" class="btn-g" onclick="deleteShopseckillActivity('${seckill.id}')">删除</a>
                                                    </c:if>
                                                    <%-- 已失效  或已结束 --%>
                                                </c:otherwise>
                                            </c:choose>

                                        </ul>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                    </tbody>
                </table>
            </div>
            <!-- 秒杀活动列表 end -->
            <!--page-->

            <div class="page clearfix">
                <div class="p-wrap">
				    <span class="p-num">
					    <ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="simple"/>
					</span>
                </div>
            </div>
        </div>


    </div>
    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<script type="text/javascript" language="javascript"
        src="<ls:templateResource item='/resources/templets/js/seckill/seckillActivityList.js'/>"></script>
<script type="text/javascript">
    var contextPath = '${contextPath}';
</script>
</body>
</html>
