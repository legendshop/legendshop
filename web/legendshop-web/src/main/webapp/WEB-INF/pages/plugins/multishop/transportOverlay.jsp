<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/templets/css/multishop/transportOverlay.css'/>" />
<div class="area_box" style="left: 124px">
    <div class="area_box_title">
        <span class="area_box_title_left">选择区域</span>
        <span class="area_box_title_right"><a href="javascript:void(0);" onclick="javascript:jQuery('.area_box').remove();">×</a></span>
    </div>
  <!--蓝色背景:area_bg_blue 白色背景:area_bg_white-->
   <c:forEach items="${requestScope.regionList}" var="region" varStatus="status">
  <div class="area_bg_white">
      <div class="area_before"><input name="group_${region.groupby}" id="group_${region.groupby}" type="checkbox" value="${region.groupby}" /><label style="padding-left:3px;font-weight: bold;" for="group_${region.groupby}">${region.groupby}</label></div>
        <div class="area_box_main">
            <ul>
               <c:forEach items="${region.provinces}" var="province" varStatus="status">
                <li>
                <input name="${province.province}" id="province_${province.id}" type="checkbox" value="${province.id}" />
                <span><label style="float:left;">${province.province}</label><b id="city_count_${city.id}" style="color:#F60"></b></span>
                     <c:if test="${province.cities != null}">
                     <div class="area_level" style="display:none;">
                        <ul>
                           <c:forEach items="${province.cities}" var="city" varStatus="status">
                            <li><label><input name="city_${city.city}"  id="city_${city.id}" city_name="${city.city}" type="checkbox" value="${city.id}" />${city.city}</label></li>
                           </c:forEach>
                            <li class="close"><input type="button" value="关闭" onclick="jQuery(this).parent().parent().parent().hide();jQuery('.area_box_main li').removeClass('this');" /></li>
                        </ul>
                    </div>
                    </c:if>
                </li>
               </c:forEach>
            </ul>
        </div>
  </div>
  </c:forEach>
	<div class="area_box_bottom">
        <input class="ncbtn" type="button" value="取消" onclick="jQuery('.area_box').remove();" />
    	<input class="ncbtn" type="button" value="确定" onclick="generic_area();" />
      <input name="trans_index" type="hidden" id="trans_index" value="${transIndex}" />
	  <input name="trans_city_type" type="hidden" id="trans_city_type" value="${transCityType}" />
	</div>
	
</div>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/transportOverlay.js'/>"></script>
<script type="text/javascript">
  var contextPath="${contextPath}";
</script>	
