<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="renderer" content="webkit"/>
    <c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
    <title>提交订单-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller${_style_}.css'/>" rel="stylesheet"/>
    <style type="text/css">
   		.sold-table td {
			text-align: center;
		}
   	</style>
</head>
<body  class="graybody">
	<div id="doc">
         <!--顶部menu-->
		  <div id="hd">
		   
		      <!--hdtop 开始-->
		       <%@ include file="/WEB-INF/pages/plugins/main/home/header.jsp" %>
		      <!--hdtop 结束-->
		    
		       <!-- nav 开始-->
		        <div id="hdmain_cart">
		          <div class="yt-wrap">
		            <h1 class="ilogo">
		                <a href="${contextPath}/"><img src="<ls:photo item="${systemConfig.logo}"/>"  style="max-height:89px;" /></a>
		            </h1>
		            
		            <!--page tit-->
		              <div class="cartnew-title">
		                        <ul class="step_h">
		                            <li class="done"><span>购物车</span></li>
		                            <li class=" done"><span>填写核对订单信息</span></li>
		                            <li class="on done"><span>提交预售订单</span></li>
		                        </ul>
		              </div>
		            <!--page tit end-->
		          </div>
		        </div>
		      <!-- nav 结束-->
		 </div><!--hd end-->
		<!--顶部menu end-->
    
        
         <div id="bd">
      
      <!--order content -->
         <div style="margin-top:20px;" class="yt-wrap ordermain">
          	<div class="cart_done">
          	   <fmt:formatNumber var="totalAmountFormat" value="${totalAmount}" type="currency" pattern="0.00"></fmt:formatNumber>
               <span style=" margin-bottom:10px;" class="bighint2 gou2_back">订单提交成功，请您尽快付款！</span>
               <div class="v_align_mid carthint">应付金额：<span>￥ 
               		 <c:if test="${payPctType eq 0 || payPctType eq 2}">
					   <fmt:formatNumber pattern="0.00" maxFractionDigits="2" groupingUsed="false" value=" ${totalAmount}" />
		             </c:if>
		             <c:if test="${payPctType eq 1}">
						<fmt:formatNumber pattern="0.00" maxFractionDigits="2" groupingUsed="false" value="${preDepositPrice}" />
		             </c:if>
				   <c:if test="${payPctType eq 0}">
					   请您在${cancelMins}分钟内完成支付，逾期订单将自动取消。
				   </c:if>
          	</div>
            <div style="padding:5px 20px 20px 20px;">
            <table class="ncm-default-table m10 sold-table">
            <thead>
              <tr>
                <th>订单号</th>
                <th>支付方式</th>
                <th>金额（元）</th>
              </tr>
            </thead>
            <tbody>
					<tr>
						<td>
						 <a target="_blank" href="${contextPath}/p/presellOrderDetail/${subNums}">${subNums}</a>
						</td>
						<td>在线支付</td>
						<td>
							<fmt:formatNumber pattern="0.00" maxFractionDigits="2" groupingUsed="false" value="${totalAmount}" /><c:if test="${not empty freightAmount}">（含运费：${freightAmount}）</c:if>
						</td>
					</tr>
			 </tbody>
          </table>
          </div>
           
          <!-- 支付方式 -->
          <%@ include file="/WEB-INF/pages/plugins/main/paylist.jsp" %>
          <!-- 支付方式 -->
		   
		  <form:form target="_blank" id="buy_form" method="POST" action="${contextPath}/p/payment/payto/PRESELL_ORDER_PAY">
			     <input type="hidden" id="payTypeId" name="payTypeId" >
			     <input type="hidden" name="subNumbers" value="${subNums}">
			     <input type="hidden" name="prePayTypeVal" id="prePayTypeVal" value="">
			     <input type="hidden" value="" name="passwordCallback" id="password_callback" />
			     <input type="hidden" value="true" name="presell" />
			     <input type="hidden" value="${payPctType}" name="payPctType"  />
			     <input type="hidden" id="submitPwd" name="submitPwd" value="" />
		          <!-- 确认订单按钮 -->
		          <div class="confirm" id="confirm">
		            <span id="confirm_totalPrice">余额支付金额：<b>￥<em id="predePrice">0.00</em></b>，在线应付金额：<b>￥<em id="totalPrice">
		             <c:if test="${payPctType eq 0 || payPctType eq 2}">
						 <fmt:formatNumber pattern="0.00" maxFractionDigits="2" groupingUsed="false" value="${totalAmount}" />
		             </c:if>  
		             <c:if test="${payPctType eq 1}">
						 <fmt:formatNumber pattern="0.00" maxFractionDigits="2" groupingUsed="false" value="${preDepositPrice}" />
		             </c:if>
		             </em></b></span>
		            <%-- <span id="confirm_totalPrice">应付金额：<b>￥<em id="totalPrice">${totalAmountFormat}</em></b></span> --%>
		            <button class="on" type="button" id="next_button">去付款</button>
		          </div>
          </form:form>
          
          
        <!--order content end-->
      </div>
	
		 <!----foot---->
		 <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
         <!----foot end---->
	</div>
</body>

<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/orderSuccess.js'/>"></script>
<script type="text/javascript">
        var $li = $('.onl-pay-tit div');
	    var $ul = $('.onl-pay .onl-pay-con');
	    var contextPath = '${contextPath}';
		var member_pd ="${availablePredeposit}";
		var pay_diff_amount = "${totalAmountFormat}";
		var coin = '${coin}';
</script>
</html>
