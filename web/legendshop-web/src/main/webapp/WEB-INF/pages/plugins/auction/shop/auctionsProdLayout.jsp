<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<head>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller${_style_}.css'/>" rel="stylesheet"/>
	<style type="text/css">
		body{font-size:13px;}
		a:hover{background-color:#d5e4fa;}
		.serachProd{border: 1px solid #d5e4fa;width: 100%;height: 190px;text-align: center;}
		.prod_list {width: 95%;float: left;overflow: hidden;margin-top: 10px;}
		.prod_list li {width: 80px;float: left;display: inline;position: relative;border: 1px solid #ddd;margin-top: 5px;margin-right: 10px;margin-left: 10px;}
		.prod {width: 70px;padding: 5px;overflow: hidden;display: block;}
		.prod_name {float: left;width: 70px;height: 15px;margin-top: 5px;color: #000;overflow: hidden;}
		.prod_page {width: 100%;float: left;margin-top: 10px;margin-bottom: 5px;text-align: center;}
		.prod_page a {color: #666;background: #f1f1f1;border: 1px solid #ccc;padding: 2px 5px;margin-right: 7px;text-decoration: none;}
		.prod_page a:hover{color:#fff; background:#1283C9; border:1px solid #2593FF}
		em {font-style: normal;}
	</style>
		<link type="text/css" href="<ls:templateResource item='/resources/templets/css/bidding-activity${_style_}.css'/>" rel="stylesheet">
</head>
<body scroll="no">
	 <form:form action="${contextPath}/s/auction/AuctionProdLayout" id="form1" method="post">
	 	<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
	 	<div style="height:50px;background-color: #f5f9ff;border-bottom:1px solid #d5e4fa;font-size: 14px;text-align: center;">
	 		<div style="padding-top:10px;padding-left:20px;">
			<input style="width:550px;height:25px;border:1px solid #d5e4fa"  type="text" name="name" id="name" maxlength="20" value="${prod.name}" size="20" placeholder="请输入商品名称" />
			<input type="submit" class="btn-r" value="搜索" style="height:25px;outline: none;"/>
			</div>
		</div>
	 </form:form>
      <div class="serachProd" >
      	<span style="display: block;margin-top: 5px;"><b>商品列表</b></span>
	      <ul class="prod_list">
			<c:forEach items="${requestScope.list}" var="prod" varStatus="status">
			   <li> 
			        <a class="prod"  onclick="loadProdSku('${prod.prodId}')" href="javascript:void(0);" title="${prod.name}"> <span class="floor_sear_img"> 
			         <img width="70" height="70" src="<ls:images item='${prod.pic}' scale='3' />" alt="${prod.name}"> </span> <span class="prod_name">${prod.name}</span> </a> 
			  </li>
			</c:forEach>
			
			<c:if test="${empty requestScope.list}">
			    暂无商品信息
			</c:if>
		</ul>
      <div class="prod_page" align="right">
			<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
		</div>
      </div>
      
      <div class="serachProdsku" style="margin-top: 10px;overflow-y: auto;overflow-x: hidden;height: 200px;">
      	
      </div>
      <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
      <script src="${contextPath}/resources/templets/js/auctionsProdLayout.js"></script>
      <script type="text/javascript">
		   var contextPath = '${contextPath}';
	</script>
</body>
