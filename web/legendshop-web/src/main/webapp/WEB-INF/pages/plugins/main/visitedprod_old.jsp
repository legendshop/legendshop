<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<!-- 浏览历史 -->
					
<c:if test="${visitedProd != null && fn:length(visitedProd) > 0}">
 <div class="sidebox">
       <h3>浏览历史</h3>
		<c:forEach items="${visitedProd}" var="prod" varStatus="status">
		<div class="sidecell" <c:if test="${status.last}">style="border-bottom:0px;"</c:if> >
			<div class="limg">
				<a href="${contextPath}/views/${prod.id}" target="_blank">
				<img src="<ls:images item='${prod.pic}' scale='2'/>"  alt="${prod.name}"  style="max-height:55px; max-width:55px;" />
				 </a>
			</div>
			<div class="txt">
				<div style="height:35px;overflow: hidden;"><a href="${contextPath}/views/${prod.id}"  target="_blank">${prod.name }</a></div> 
				<span class="lprice"><fmt:formatNumber type="currency" value="${prod.cash}" pattern="${CURRENCY_PATTERN}"/></span>
			</div>
		</div>
		</c:forEach>
  </div>  
</c:if>

