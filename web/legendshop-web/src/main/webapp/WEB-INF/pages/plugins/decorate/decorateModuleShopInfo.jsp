<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

<div class="store_left_box">
	<h1 style="color:; background-color:">店铺信息</h1>
	<div style="background-color:" class="store_infor">
		<span class="full_name">${shopInfo.siteName}</span>
		<ul style="display:;" class="pro_shop_date_c">
			<li><span style="float:left;">综合评分：</span> <span class="scores_scroll"> <span class="scroll_gray"></span> <span class="scroll_red" style="width: 20%"></span> </span><font id="shopScoreNum" class="hide">${shopInfo.credit}</font></li>
			</li>
		</ul>
		<ul class="information">
			<li><span>店铺名称：</span><b>${empty shopInfo.siteName?'暂无':shopInfo.siteName}</b>
			</li>
			<li><span>所在地：</span><b>${empty shopInfo.shopAddr?'暂无':shopInfo.shopAddr}</b>
			</li>
			<!-- 
			<li><span>联系电话：</span><b>${shopInfo.contactMobile}</b>
			</li>
			 -->
			<li><span>店铺简述：</span><b>${empty shopInfo.briefDesc?'暂无':shopInfo.briefDesc}</b>
			</li>
		</ul>
		<div class="shop_botm_hid">
			<ul class="shopboh">
				<li style="border-right:0px;" class="collection"><a
					id="store_fav" href="javascript:void(0);"><span>收藏店铺</span>
				</a></li>
			</ul>
		</div>
	</div>
</div>
