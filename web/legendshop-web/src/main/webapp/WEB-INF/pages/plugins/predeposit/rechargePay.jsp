<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>预存款支付提交 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<!--  <stye>
	 	background-position: -11px -118px;
	 </stye> -->
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
     <%@ include file="/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp" %>
    <!-----------right_con-------------->
     <div class="right_con">
     <div class="o-mt">
		<h2>支付提交</h2>
	</div>
<!----两栏---->
<div class="w" style="padding: 10px;background: #fff;">
	<!-----------right_con-------------->
	<div class="right_cartcon">
		<div class="ncc-wrapper">
			<div class="ncc-main">
			   	  <div class="ncc-title" style="padding:10px;">
			         <span>查看充值记录可以通过<a style="color:#e5004f;" target="_blank" href="<ls:url address='/p/predeposit/recharge_detail'/>">我的充值列表 </a>进行查看。</span>
			      </div>
			        <fmt:formatNumber var="totalAmountFormat" value="${pdRecharge.amount}" type="currency" pattern="0.00"></fmt:formatNumber>
			       <form:form  target="_blank" id="buy_form" method="POST" action="${contextPath}/p/payment/payto/USER_PRED_RECHARGE">
			            <input type="hidden" name="subNumbers"  value="${pdRecharge.sn}">
			            <input type="hidden" name="payTypeId" id="payTypeId" value="ALP">
		<div class="onl-pay">
            <h3>选择在线支付</h3>
            <div class="onl-pay-tit">
            </div>
				   <div class="onl-pay-con" >
                     <c:if test="${payTypesMap.ALP_PC==1}">
			              <div title="支付宝" class="ALP pay-selected" payment_code="ALP">
			                <span class="ALP ALI_PAY"></span>
			                <em></em>
			              </div>
                      </c:if>
                     <c:if test="${payTypesMap.WX_PAY==1}">
		               <div title="微信" class="WX_SACN_CODE " payment_code="WX_SACN_PAY">
		                 <span class="WX_SACN_CODE WEIXIN_PAY"></span>
		                 <em></em>
		                </div>
                      </c:if>
                      
                      <c:if test="${payTypesMap.TDP==1}">
			               <div title="微信" class="TEN_PAY " payment_code="TDP">
			                  <span class="TEN_PAY"></span>
			                  <em></em>
			                 </div>
                      </c:if>
                       
                        <c:if test="${payTypesMap.TONGLIAN_PAY==1}">
			               <div title="微信" class="TONGLIAN_PAY " payment_code="TONGLIAN_PAY">
			                  <span class="TONGLIAN_PAY"></span>
			                  <em></em>
			                 </div>
                      </c:if>
                      
                       <c:if test="${payTypesMap.SIMULATE==1}">
			               <div title="微信" class="SIMULATE " payment_code="SIMULATE">
			                  <span class="SIMULATE"></span>
			                  <em></em>
			                 </div>
                      </c:if>
                      
		            </div>
                   </div>
					
					<div class="ncc-bottom" style="text-align: center;">
							<a class="btn-r" id="next_button" href="javascript:void(0);">确认支付</a>
						</div>
			       
			       </form:form>
			</div>
		</div>
		<!----成功提交---->

	</div>
	<div class="clear"></div>
</div>

<div style="display: none;" id="pay_overlay">
        <div class="pop-up-dow" >
            <ul style="text-align: center; margin-top: 27px;">
                <li style="margin-bottom: 20px;">如已经成功支付，请点击<a style="padding:4px;border-radius: 2px;color: #1e9fff;"	href="${contextPath}/p/myorder" >已经完成支付！</a>
<%--                <li>如付款遇到问题，请点击<a style="padding: 4px;border-radius: 2px;color: #1e9fff;" href="javascript:void(0);">支付出现问题！</a>--%>
				<li>如付款遇到问题，请重新支付！</li>
            </ul>
        </div>
 </div>
</div>
    <!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
 <script type="text/javascript">
		$(document).ready(function() {

			$('#next_button').on('click',function() {
				var payment_code=$('#payTypeId').val();
				if ( payment_code == '' || payment_code== undefined || payment_code==null) {
					layer.msg("请选择一种在线支付方式",{icon:0});
					return;
				}
				layer.open({
					  title :"支付提醒",
					  type: 1, 
					  content: $("#pay_overlay").html(),
					  area: ['400px', '220px']
					  ,btn: ['我知道了'],
					  yes:function(index, layero){
						  layer.close(index); 
					  }
					}); 
				$("#buy_form").submit();
			});

			$('.onl-pay-con > div').on('click',function() {
				$('.onl-pay-con > div').removeClass('pay-selected');
				if ($('#payTypeId').val() != $(this).attr('payment_code')) {
					$('#payTypeId').val($(this).attr('payment_code'));
					$(this).addClass('pay-selected');
				} else {
					$('#payTypeId').val('');
				}
			});
		});

	</script>
</body>
</html>
			