
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!-- 底部 -->
<div class="foot">
	<div class="foot-down">
		<div class="item">
			<div class="item-con">
				<a href="#">关于我们</a>
				<em>|</em>
				<a href="#">联系我们</a>
				<em>|</em>
				<a href="#">人才招聘</a>
				<em>|</em>
				<a href="#">商家入驻</a>
				<em>|</em>
				<a href="#">友情链接</a>
			</div>
		</div>
		<div class="item">
			<div class="item-con">
				<span>Copyright©2004-2012</span>
				<span>All Rights Reserved</span>
				<span>粤ICP备15089754号-1</span>
			</div>
		</div>
	</div>
</div>
<!-- /底部 -->