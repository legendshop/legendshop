<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
			<div class="o-mt">
						<h2>安全中心</h2>
			</div>
		<div class="right">
			
			<div class="m m3  safe-sevi04">
				<div class="mc">
					<s class="icon-succ02"></s>
					<div class="jd-add">
						<h3><span class="ftx-02">恭喜您，修改成功！</span></h3>
    					<p class="ftx-03">最新安全评级：<strong class="rank-text ftx-02" id="secLevel">较高</strong><i class="icon-rank04"></i></p>
    					<p class="ftx-03">您的帐户安全级还能提升哦，快去<a href="${contextPath}/p/security">安全中心</a>完善其它安全设置提高评级吧！</p>											</div>
				</div>
			</div>
		</div>
<script type="text/javascript">
	$(document).ready(function() {
		if(${secLevel}==1){
			$("#secLevel").html("很危险");
			$("#secLevel").attr("style","color:#B22222");
			$(".icon-rank04").attr("style","background-position:0 -30px");
		}else if(${secLevel}==2){
			$("#secLevel").html("中级");
			$("#secLevel").attr("style","color:#FF6600");
			$(".icon-rank04").attr("style","background-position:0 -45px");
		}else if(${secLevel}==3){
			$("#secLevel").html("高级");
			$("#secLevel").attr("style","color:#009900");
			$(".icon-rank04").attr("style","background-position:0 -60px");
		}else if(${secLevel}==4){
			$("#secLevel").html("很安全");
			$("#secLevel").attr("style","color:#009900");
			$("#promptInfo").html("VeryGood!");
			$("#promptInfo").attr("style","color:#00C5CD");
			$(".icon-rank04").attr("style","background-position:0 -75px");
		}
	});	
</script>
