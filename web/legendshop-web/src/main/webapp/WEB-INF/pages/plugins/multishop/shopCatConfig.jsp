<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>类目配置-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-category${_style_}.css'/>" rel="stylesheet">
	<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
	<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/plugins/select/divselect.css'/>" />
	 <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc" align="center">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div id="Content" class="w1190">
			<div class="seller-cru">
						<p>
							您的位置>
							<a href="${contextPath}/home">首页</a>>
							<a href="${contextPath}/sellerHome">卖家中心</a>>
							<span class="on">商品类目</span>
						</p>
					</div>
					<%@ include file="included/sellerLeft.jsp" %>
		         <div class="seller-category">
						<div class="seller-com-nav">
							<ul>
								<li  id="shopCatList"><a href="javascript:void(0);">类目列表</a></li>
								<li class="on"  id="shopCatConfig"><a href="javascript:void(0);">一级类目配置</a></li>
							</ul>
						</div>
			         <form:form action="${contextPath}/s/saveShopCate" method="post" id="form1" enctype="multipart/form-data">
			            <input id="shopCatId" name="id" value="${shopCat.id}" type="hidden">
			            <input id="grade" name="grade" value="1" type="hidden">
			            <div align="center">
					        <table cellspacing="0" cellpadding="0" border="0" style=" margin-top:30px; font-size: 12px;" class="sendmes_table" >
					     	 <tr>
					        <th>
					          		类目名称：<font color="ff0000">*</font>
					       </th>
					        <td>
					           <input type="text" name="name" id="name" maxlength="30"  value="${shopCat.name}">
					        </td>
					      </tr>
					      <tr>
					        <th>
					          		次序：
					       </th>
					        <td>
					                <input type="text" name="seq" id="seq" maxlength="30"  value="${shopCat.seq}" style="width: 60px;">
					        </td>
					      </tr>
					      
					    	<%-- <tr>
					    		<th>
					    			图片：
					    		</th>
					    		<td>
					    			<input type="file" class="file" name="file" id="file" size="30"/>
					    			<input type="hidden" class="file" name="pic" id="pic" size="30" value="${shopCat.pic}"/>
					    		</td>
					    	</tr>		
									<c:if test="${not empty shopCat.pic}">
									    <tr>
									   		<th>原有图片:</th>
									     	<td >
									      	<img src="<ls:photo item='${shopCat.pic}'/>" height="50" width="150"/>
									      </td>
									    </tr>
					     			</c:if> --%>
					
					                <tr>
					                	<th></th>
					                    <td>
					                            <a href="javascript:void(0)" onclick="submitTable();" class="btn-r"><s></s>提交</a>
					                            <a href="javascript:void(0)" onclick="shopCatList();" class="btn-g"><s></s>返回</a>
					                    </td>
					                </tr>
					            </table>
			           </div>
			        </form:form>
		        </div>
		</div>
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select/divselect.js'/>"></script>
<script type="text/javascript">
	function shopCatList(){
		window.location.href="${contextPath}/s/shopCategory";
	}
	
	$(document).ready(function() {
		userCenter.changeSubTab("shopCategory");
		$("#form1").validate({
			rules: {
				name:  "required",
				 seq: {
	              number: true,
	              max:99999
	            }
			},
			messages: {
				name: "请输入类目名称",
				seq: {
	                number: "请输入数字",
	               	max:"次序不能超过99999"
	            },
				file:"请上传类目图片"
			}
		}); 
			
		$("#shopCatList").click(function(){//前往 一级类目列表
			window.location.href="${contextPath}/s/shopCategory";
		});	
	});
	 
	 function submitTable(){
		 	$("#form1").submit();
	  }	
</script>
</body>