<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://www.legendesign.net/date-util"  prefix="dateUtil" %>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}"  pattern="yyyy-MM-dd HH:mm" var="nowDate"/>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>我的订单 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>
<style>
.goods-name2 dt a:hover {
	color: #e5004f !important;
}
</style>
<body class="graybody">
	<div id="bd">
	<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
     <!----两栏---->
	 <div class=" yt-wrap" style="padding-top:10px;"> 
	    <%@include file='usercenterLeft.jsp'%>
	    <!-----------right_con-------------->
	     <div class="right_con">
	         <div class="o-mt"><h2>我的订单</h2></div>
	         <div class="pagetab2">
		         <ul>
		           <li class="on"><span>订单列表</span></li>
		           <li><span><a href="<ls:url address='/p/myorderdels'/>">回收站</a></span></li>
		         </ul>       
	         </div>
	         <!-- 订单搜索条 start -->
	         <form:form action="${contextPath }/p/myorder" method="get" id="order_search">  
		     	<input type="hidden" value="1" name="curPageNO">    
		        <div class="form">
			        <div class="sold-ser sold-ser-no-bg clearfix">
			        	<div class="fr">
				            <div class="item">
					                               订单类型：
					        	<select  name="sub_type" class="item-sel" style="width:83px;">
									  <option value="">所有类型</option>
									  <option value="NORMAL" <c:if test="${sub_type == 'NORMAL'}">selected="selected"</c:if>>普通订单</option>
									  <option value="GROUP" <c:if test="${sub_type == 'GROUP'}">selected="selected"</c:if>>团购订单</option>
									  <option value="SECKILL" <c:if test="${sub_type == 'SECKILL'}">selected="selected"</c:if>>秒杀订单</option>
									  <option value="AUCTIONS" <c:if test="${sub_type == 'AUCTIONS'}">selected="selected"</c:if>>拍卖订单</option>
									  <option value="SHOP_STORE" <c:if test="${sub_type == 'SHOP_STORE'}">selected="selected"</c:if>>门店订单</option>
									  <option value="MERGE_GROUP" <c:if test="${sub_type == 'MERGE_GROUP'}">selected="selected"</c:if>>拼团订单</option>
									  <option value="PRE_SELL" <c:if test="${sub_type == 'PRE_SELL'}">selected="selected"</c:if>>预售订单</option>
					            </select>
					        </div>
				            <div class="item">
				              	订单状态：
				              	<select name="state_type" class="item-sel" style="width:83px;">
									<option value=""  >所有订单</option>
									<option value="1" <c:if test="${state_type==1}">selected="selected"</c:if>>待付款</option>
									<option value="2" <c:if test="${state_type==2}">selected="selected"</c:if>>待发货</option>
									<option value="3" <c:if test="${state_type==3}">selected="selected"</c:if>>待收货</option>
									<option value="4" <c:if test="${state_type==4}">selected="selected"</c:if>>已完成</option>
									<option value="5" <c:if test="${state_type==5}">selected="selected"</c:if>>已取消</option>
								</select>
					        </div>
				            <div class="item">
				              	下单时间：
				              	<input class="item-inp" style="width:86px;" type="text" value='<fmt:formatDate value="${startDate}" pattern="yyyy-MM-dd" />' id="startDate" name="startDate" readonly="readonly">
								-
								<input class="item-inp" style="width:86px;" type="text" value='<fmt:formatDate value="${endDate}" pattern="yyyy-MM-dd" />' id="endDate" name="endDate" readonly="readonly">
					        </div>
				            <div class="item">                         
				                                        订单号：
				                <input type="text" class="item-inp" value="${order_sn}" name="order_sn" style="width:200px;">
				                <input type="button" class="btn-r" onclick="search()" id="search_order" value="搜索订单">
				        	</div>
			        	</div>
			    	</div>
		    	</div>
			</form:form>
	        <!-- 订单搜索条 end -->
	        
	        <!-------------订单---------------->
			<div id="recommend" class="m10 recommend" style="display: block;border-width:1px 0px;margin-top:0;">
				<!-- 合并付款 start -->
	           	<div class="operator" style="border: 1px solid #e7e7e7;border-width: 1px 1px 0;width: 964.6px;">
		           <!-- <span><input  class="selectAllNotPayOrder" type="checkbox"></span> -->
		           <span>
		           <label class="checkbox-wrapper" style="float:left;margin:9px;">
						<span class="checkbox-item">
							<input type="checkbox" class="checkbox-input selectAllNotPayOrder">
							<span class="checkbox-inner"></span>
						</span>
					</label>	
					</span>
		           <span style="float: left;">全选</span>
		           <span><a href="javascript:;" onclick="javascript:mergerOrder();" class="btn-g" style="line-height:17px !important;margin-top:6px !important;margin-left:15px !important;">合并付款</a></span>
	           	</div>
	           	<!-- 合并付款 end -->
	           	
	           	<!-- 订单表格 start -->
	           	<table class="ncm-default-table order sold-table" cellpading=0 cellspacing="0" style="border-collapse: collapse;">
					<thead>
						<tr class="sold-tit">
							<th width="10"></th>
							<th colspan="2">商品</th>
							<th>单价（元）</th>
							<th>数量</th>
							<th>售后维权</th>
							<th width="12%">订单金额</th>
							<th width="10%">交易状态</th>
							<th class="w120" style="border-right: 1px solid #e7e7e7;">交易操作</th>
						</tr>
					</thead>
					<c:choose>
					 	<c:when test="${empty requestScope.list}">
							<tbody>
						 	 	<tr>
						 	 		<td colspan="20">
						 	 			<!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
						 	 			<div class="warning-option"><!-- <i></i> --><span>没有符合条件的信息</span></div>
						 	 		</td>
						 	 	</tr>
					        </tbody>
					 	</c:when>
				 		<c:otherwise>
				 			<c:forEach items="${requestScope.list}" var="order" varStatus="orderstatues">
					 		 	<c:choose>
					 		 		<c:when test="${order.subType == 'PRE_SELL' }">
					 		 			<!-- -----------------预售订单  start---------------------  -->
					 		 			<tbody class="pay">
											<tr>
												<td class="sep-row" colspan="9"></td>
											</tr>
											<tr>
												<td colspan="9" style="background-color: #f9f9f9;text-align: left;border-top:1px solid #e4e4e4;">
													<span class="ml10">订单号：${order.subNum} </span>
													<span class="presellType">预售订单</span>
													<!-- 客服入口 --> 
													<span style="margin-left: 15px;">
														<a target="_blank" style="color: #006dc7;" href="${contextPath}/p/im/customerShop?subNum=${order.subNum}&shopId=${order.shopId}">联系商家</a>
													</span>
													<span>下单时间： <fmt:formatDate value="${order.subDate}" type="both" /></span> 
													<c:if test="${(order.status==5 or order.status==4) and order.refundState ne 1}">
														<a onclick="removeOrder('${order.subNum}');" class="order-trash fr" href="javascript:void(0);">删除订单</a>
													</c:if>
													<c:choose>
														<c:when test="${order.status == 1 and order.payPctType eq 0 and order.isPayDeposit eq 0}">
															<c:if test="${dateUtil:getOffsetMinutes(order.subDate, nowTime) ge order_cancel}">
																<a onclick="removeOrder('${order.subNum}');" class="order-trash fr" href="javascript:void(0);">删除订单</a>
															</c:if>
														</c:when>
														<c:when test="${order.status == 1 and order.payPctType eq 1 and order.isPayDeposit eq 0 and order.isPayFinal eq 0}">
															<c:if test="${dateUtil:getOffsetMinutes(order.subDate, nowTime) ge order_cancel}">
																<a onclick="removeOrder('${order.subNum}');" class="order-trash fr" href="javascript:void(0);">删除订单</a>
															</c:if>
														</c:when>
														<c:when test="${order.status == 1 and order.payPctType eq 1 and order.isPayDeposit eq 1 and order.isPayFinal eq 0}">
															<c:if test="${order.finalMEnd < nowTime}">
																<a onclick="removeOrder('${order.subNum}');" class="order-trash fr" href="javascript:void(0);">删除订单</a>
															</c:if>
														</c:when>
													</c:choose>
												</td>
											</tr>
											<c:forEach items="${order.subOrderItemDtos }" var="orderItem" varStatus="orderItemStatus">
												<c:set value="0" var="notCommedCount" /> 
												<c:if test="${orderItem.commSts eq 0}">
										    		<c:set value="${notCommedCount + 1}" var="notCommedCount" /> 
										    	</c:if>
												<!-- S 商品列表 -->
												<tr>
													<td>&nbsp;</td>
													<td class="w70" align="right">
														<div class="ncm-goods-thumb">
															<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">
																<img onmouseout="toolTip()" onmouseover="toolTip();" src="<ls:images item='${orderItem.pic}' scale='3'/>">
															</a>
														</div>
													</td>
													<td class="tl">
														<dl class="goods-name2">
															<dt><a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">${orderItem.prodName}</a></dt>
															<dd>${orderItem.attribute}</dd>
														</dl>
													</td>
													<td><p><span class="actualTotal">
														<fmt:formatNumber value="${orderItem.cash}" type="currency" pattern="0.00"></fmt:formatNumber>
													 </span> </p>
														<c:if test="${not empty orderItem.promotionInfo}">
												            <span class="sale-type">${orderItem.promotionInfo}</span>
												         </c:if>
												         <c:if test="${orderItem.refundState == 2}">
												         	<p style="color:#69AA46;"><fmt:formatNumber value="${orderItem.refundAmount}" type="currency" pattern="0.00"></fmt:formatNumber>(退)</p>
												         </c:if>
													</td>
													<td>x${orderItem.basketCount}</td>
													<td>
														<c:if test="${order.actualTotal > 0}">
															<c:choose>
											                	<c:when test="${order.status gt 2 &&  order.status le 4 &&  orderItem.refundState le 0 && orderItem.inReturnValidPeriod}">
										                		     <!-- 订单处于 待发货、已完成状态 并且 没有处理过退款或者退款失败的 -->
										                		    <p><a  href="#" onclick="returnApply('${order.subId}','${orderItem.subItemId}','${order.subNum}');">退款/退货</a></p>
										                		</c:when>
										                		<c:when test="${orderItem.refundType==1 && (orderItem.refundState==1 || orderItem.refundState==2)}">
										                		    <p><a target="_blank" href="${contextPath}/p/refundDetail/${orderItem.refundId}">查看退款</a>
										                		</c:when>
										                		<c:when test="${orderItem.refundType==2 && (orderItem.refundState==1 || orderItem.refundState==2)}">
										                		    <p><a target="_blank" href="${contextPath}/p/returnDetail/${orderItem.refundId}">查看退货</a>
										                		</c:when>
											                </c:choose>
											            </c:if>
													</td>
										           	<td class="bdl">
														<p class=""><strong><fmt:formatNumber value="${order.actualTotal}" type="currency" pattern="0.00"></fmt:formatNumber></strong></p>
														<p title="支付方式：">
															<c:choose>
										                		<c:when test="${order.payManner eq 1}">货到付款</c:when>
										                		<c:when test="${order.payManner eq 2}">在线支付</c:when>
										                		<c:when test="${order.payManner eq 2}">门店支付</c:when>
										                </c:choose>
													</td>
													<!-- 如果是第一行 -->
													<c:if test="${orderItemStatus.index eq 0 }">
														<c:choose>
															<c:when test="${fn:length(order.subOrderItemDtos) gt 0 }"><!-- 如果有多个订单项 -->
																<td class="bdl" rowspan="${fn:length(order.subOrderItemDtos) }">
															</c:when>
															<c:otherwise>
																<td class="bdl">
															</c:otherwise>
														</c:choose>
													    <p>
													    <c:if test="${order.refundState ne 1 }">
															<c:choose>
																 <c:when test="${order.status eq 1 }">
																    <span class="col-orange">待付款</span>
																 </c:when>
															     <c:when test="${order.status==2 && order.isPayDeposit eq 1 && order.isPayFinal eq 1 }">
															     	<span class="col-orange">待发货</span>
															     </c:when>
															     <c:when test="${order.status==2 && order.payPctType eq 0}">
															     	<span class="col-orange">待发货</span>
															     </c:when>
																 <c:when test="${order.status eq 3 }">
																 	<span class="col-orange">待收货</span>
																 </c:when>
																 <c:when test="${order.status eq 4 }">已完成</c:when>
																 <c:when test="${order.status eq 5 }">交易关闭</c:when>
																 <c:otherwise>待付尾款</c:otherwise>
															</c:choose>
														</c:if>
													    </p> 
													    <!-- 订单查看 -->
														<p><a target="_blank" href="${contextPath}/p/presellOrderDetail/${order.subNum}">订单详情</a></p>
														<c:if test="${ order.status eq 3 or order.status eq 4 }">
															<p><a target="_blank" href="${contextPath}/p/orderExpress/${order.subNum}" >查看物流</a></p>   
														</c:if>
														<c:if test="${order.status eq 4 }">
															<p style="display:none;"><a target="_blank" href="${contextPath}/p/myreturnOrder">退换货</a></p>
														</c:if>
														<!-- 物流跟踪 -->
													</td>
													<c:choose>
														<c:when test="${fn:length(order.subOrderItemDtos) gt 0 }"><!-- 如果有多个订单项 -->
															<td class="bdl bdr" rowspan="${fn:length(order.subOrderItemDtos) }">
														</c:when>
														<c:otherwise>
															<td class="bdl bdr">
														</c:otherwise>
													</c:choose>
													<p>
														<c:choose>
															<c:when test="${order.refundState ne 1 && order.refundState ne 2}">
																<c:choose>
																	<c:when test="${order.status eq 1 }">
																	    <c:if test="${order.isPayDeposit eq 1}">
																	    	<i class="icon-ban-circle"></i>等待尾款支付</a>
																        </c:if>
															         	<c:if test="${order.isPayDeposit eq 0}">
															            	<a class="btn-r" href="javascript:void(0)" onclick="cancleOrder('${order.subNum}');">
																        	取消订单</a>
											                         	</c:if>       
																	</c:when>
																	<c:when test="${order.status eq 2}">
																	    <a class="btn-r" href="javascript:void(0);" onclick="SendSiteMsg('${order.shopId}','${order.subNum}');"><i class="icon-ban-circle"></i>提醒发货</a>
															         	<c:if test="${order.payPctType eq 0}"> <!-- 预售订单全额支付 -->
																         	<a class="a-block" href="#" onclick="refundApply('${order.subId}');">
																					    <i class="icon-ban-circle"></i>订单退款
																			</a>      
															         	</c:if>
																	</c:when>
																	<c:when test="${order.status eq 3 }">
																	    <a class="a-block" href="javascript:void(0)" onclick="orderReceive('${order.subNum}');">
																	    确认收货</a>
																	</c:when>
																	<c:when test="${order.status eq 4 and notCommedCount gt 0}">
															       		<a class="a-block" href="${contextPath}/p/prodcomment/${order.subId}"><i class="icon-ban-circle"></i>评价商品</a>
																	</c:when>
																	<c:when test="${order.status==4&&notCommedCount==0 }">
																		<a class="a-block" href="${contextPath}/p/prodcomment/${order.subId}"><i class="icon-ban-circle"></i>查看评论</a>
																	</c:when>
																	<c:when test="${order.status eq 4 or order.status eq 5}">
																		<a class="a-block" href="#" onclick="addAnotherSub2('${order.subNum}')">再来一单</a>
																	</c:when>
																	<c:otherwise>&nbsp;</c:otherwise>
																</c:choose>
															</c:when>
															<c:when test="${order.refundState eq 1}">
																<i class="icon-ban-circle"></i>退款退货中</a>
															</c:when>
															<c:when test="${order.refundState eq 2}">
																<i class="icon-ban-circle"></i>退款完成</a>
															</c:when>
														</c:choose>
													</p>
												</td>
											</c:if>
										</tr>
									</c:forEach>
										<tr style="color:#999;">
								        	<td colspan="2">阶段1：定金</td>
								            <td colspan="2">
								            	<c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
													¥<span class="preDepositPrice"><fmt:formatNumber value="${order.preDepositPrice+order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber></span>
									            	(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
								            	</c:if>
												<c:if test="${order.payPctType eq 1}"><!-- 定金支付 -->
													¥<span class="preDepositPrice"><fmt:formatNumber value="${order.preDepositPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
												</c:if>
								            </td>
							                <c:if test="${order.status eq 5 }">
							                	<td colspan="5" style="border-right: 1px solid #e6e6e6;"></td>
							                </c:if>
							                <c:if test="${order.status ne 5 }">
								                <c:if test="${order.isPayDeposit eq 0}">
								                	<c:if test="${dateUtil:getOffsetMinutes(order.subDate,nowTime) le order_cancel}">
								                		<td colspan="3">需在<font color="#e5004f">${order_cancel - dateUtil:getOffsetMinutes(order.subDate,nowTime)}</font>分钟之内付款</td>
								                		<td colspan="2" style="border-right: 1px solid #e6e6e6;">
									                		<a href="${contextPath}/p/presell/order/payDeposit/1/${order.subNum}/${order.activeId}" class="a-block">付款</a>
									                	</td>
								                	</c:if>
								                	<c:if test="${dateUtil:getOffsetMinutes(order.subDate,nowTime) gt order_cancel}">
								                		<td colspan="5" style="border-right: 1px solid #e6e6e6;">已过期</td>
								                	</c:if>
								                </c:if>
								                <c:if test="${order.isPayDeposit eq 1 }">
								                	<td colspan="5" style="border-right: 1px solid #e6e6e6;">
								                	     <div><span>已完成</span><span style="margin-left: 30px;">${order.depositPayName}</span></div>
				                	     				<div>${order.depositTradeNo}</div>
								                	</td>
								                </c:if>
							                </c:if>
								       	</tr>
								       	<c:if test="${0 ne order.finalPrice}">
								       		<tr style="color:#999;">
								            	<td colspan="2">阶段2：尾款</td>
								                <td colspan="2">
													<c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
														¥<span class="finalPrice"><fmt:formatNumber value="${order.finalPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
													</c:if>
													<c:if test="${order.payPctType eq 1}"><!-- 定金支付 -->
														¥<span class="finalPrice"><fmt:formatNumber value="${order.finalPrice+order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber></span>
														(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
													</c:if>
												</td>
								                <c:if test="${order.status eq 5 }">
									               	<td colspan="5"></td>
									            </c:if>
								                <c:if test="${order.status ne 5 && order.isPayDeposit eq 1 }">
								                	<c:if test="${order.isPayFinal eq 0}">
														<c:choose>
															<c:when test="${empty order.finalMStart}">
																<td colspan="3"></td>
															</c:when>
															<c:otherwise>
																<td colspan="3" >
																	尾款支付时间:<br/> <fmt:formatDate value="${order.finalMStart }" pattern="yyyy-MM-dd HH:mm:ss" /> <br/>- <br/><fmt:formatDate value="${order.finalMEnd }" pattern="yyyy-MM-dd HH:mm:ss" />
																</td>
																<%--/admin/order/orderPrint--%>
															</c:otherwise>
														</c:choose>
										            </c:if>
										            <td colspan="5" style="border-right: 1px solid #e6e6e6;">
											        	<c:if test="${order.isPayFinal eq 0}">
															<c:choose>
											               		<c:when test="${order.finalMStart gt nowTime }">未开始</c:when>
											                	<c:when test="${order.finalMStart le nowTime and order.finalMEnd gt nowTime and order.refundState eq 0}">
												                	<a href="${contextPath}/p/presell/order/payDeposit/2/${order.subNum}/${order.activeId}" target="_blank" class="a-block">付尾款</a>
											                	</c:when>
											                	<c:when test="${order.finalMEnd le nowTime }">已过期</c:when>
											                </c:choose>
										                </c:if>
									                 	<c:if test="${order.isPayFinal eq 1 }">
										                 	<font color="green">已完成</font><br/>
								                	        <font color="green">${order.finalPayName}</font><br/>
								                	        <font color="green">${order.finalTradeNo}</font><br/>
											        	</c:if>
									                </td>
									             </c:if>
									             <c:if test="${order.status ne 5 && order.isPayDeposit eq 0 }">
								                		<c:if test="${order.isPayFinal eq 0}">
									                		<td colspan="3" >
											                	尾款支付时间: <fmt:formatDate value="${order.finalMStart}" pattern="yyyy-MM-dd" /> - <fmt:formatDate value="${order.finalMEnd }" pattern="yyyy-MM-dd" />
											                </td>
										                </c:if>
										               <td colspan="5" style="border-right: 1px solid #e6e6e6;">
												                                   等待定金支付
										               </td>
									             </c:if>
								              </tr>
								              </c:if>
										</tbody>
					 		 		<!-- -----------------预售订单  end---------------------  -->
					 		 		</c:when>
					 		 		<c:otherwise>
					 		 		<!-- -----------------其他订单  start---------------------  -->
					 		 			<tbody class="pay">
											<tr>
												<td class="sep-row" colspan="9"></td>
											</tr>
											<tr>
												<td class="pay-td" colspan="9">
												<%-- <c:if test="${!order.ispay}">
													<input type="checkbox"  value="${order.subNum}"  class="selector" <c:if test="${order.status!=1}">disabled="disabled" </c:if> >
												</c:if> --%>
												<c:if test="${order.status ==1}">
													<%-- <input type="checkbox"  value="${order.subNum}"  class="selector"> --%>
													 <span>
											           <label class="checkbox-wrapper" style="float:left;margin:9px;">
															<span class="checkbox-item">
																<input type="checkbox" class="checkbox-input selector" value="${order.subNum}" onclick="selector(this)">
																<span class="checkbox-inner"></span>
															</span>
														</label>	
													 </span>
												</c:if>
												<span class="ml15">在线支付金额：<em class="col-black">￥<fmt:formatNumber pattern="0.00" value=" ${order.actualTotal}" /></em>
												</span> 
												<c:choose>
													<c:when test="${order.subType == 'NORMAL'}">
														<span style="margin-left: 35px;">普通订单</span>
													</c:when>
													<c:when test="${order.subType == 'SECKILL'}">
														<span style="margin-left: 35px;">秒杀订单</span>
													</c:when>
													<c:when test="${order.subType == 'AUCTIONS'}">
														<span style="margin-left: 35px;"><img src="${contextPath}/resources/templets/images/auctions_order.png"/>&nbsp;&nbsp;拍卖订单</span>
													</c:when>
													<c:when test="${order.subType == 'GROUP'}">
														<span style="margin-left: 35px;">团购订单</span>
													</c:when>
													<c:when test="${order.subType == 'SHOP_STORE'}">
														<span style="margin-left: 35px;">门店订单</span>
													</c:when>
													<c:when test="${order.subType == 'MERGE_GROUP'}">
														<span style="margin-left: 35px;">拼团订单</span>
													</c:when>
												</c:choose>
												
												<!-- 客服入口  -->
												<span style="margin-left: 15px;">
													<a target="_blank" style="color: #006dc7;" href="${contextPath}/p/im/customerShop?subNum=${order.subNum}&shopId=${order.shopId}">联系商家</a>
												</span>
												<c:choose>
													<c:when test="${order.status==1 }">
															 <c:choose>
														         <c:when test="${order.subType == 'SHOP_STORE' && order.payManner==2}">
														         	<a target="_blank" style="margin-right: 23px !important;" href="<ls:url address="/p/orderSuccess?subNums=${order.subNum}" />" class="btn-r fr mr15">订单支付</a>
														         	<span style="height: auto;line-height: 24px;padding: 0;color: #aaa;margin: 5px;"  class="countdown fr" datetime="<fmt:formatDate value="${order.subDate}" type="both" />" >
															            <i class="icon-countdown"></i>剩余<lable data-cd-type="minutes">0</lable>分<lable  data-cd-type="second">0</lable>秒
														            </span>
														         </c:when>
														          <c:when test="${order.subType == 'SHOP_STORE' && order.payManner==3}">
														         </c:when>
														         <c:when test="${order.subType == 'AUCTIONS'}">
														        	 <a target="_blank" style="margin-right: 23px !important;" href="<ls:url address="/p/orderSuccess?subNums=${order.subNum}" />" class="btn-r fr mr15">订单支付</a>
														            <span style="height: auto;line-height: 24px;padding: 0;color: #aaa;margin: 5px;"  class="auctionCountdown fr" datetime="<fmt:formatDate value="${order.subDate}" type="both" />" >
															            <i class="icon-countdown"></i>剩余<lable data-cd-type="minutes">0</lable>分<lable  data-cd-type="second">0</lable>秒
														            </span>
														         </c:when>
														         <c:otherwise>
														 			<a target="_blank" style="margin-right: 23px !important;" href="<ls:url address="/p/orderSuccess?subNums=${order.subNum}" />" class="btn-r fr mr15">订单支付</a>
														            <span style="height: auto;line-height: 24px;padding: 0;color: #aaa;margin: 5px;"  class="countdown fr" datetime="<fmt:formatDate value="${order.subDate}" type="both" />" >
															            <i class="icon-countdown"></i>剩余<lable data-cd-type="minutes">0</lable>分<lable  data-cd-type="second">0</lable>秒
														            </span>
														         </c:otherwise>
														     </c:choose>
													</c:when>
												  </c:choose>
												</td>
											</tr>
											<tr>
												<td colspan="19" style="text-align: left;">
													<span class="ml10"> 订单号：${order.subNum} </span>  
													<span>下单时间： <fmt:formatDate value="${order.subDate}" type="both" /></span> 
													<span></span> 
													 <c:if test="${(order.status==5 or order.status==4) and order.refundState ne 1}">
														<a onclick="removeOrder('${order.subNum}');" class="order-trash fr" href="javascript:void(0);">删除订单</a>
													 </c:if>
												</td>
											</tr>
							
											<!-- S 商品列表 -->
											<c:set value="0" var="notCommedCount" /> 
											<c:forEach items="${order.subOrderItemDtos}" var="orderItem" varStatus="orderItemStatues">
												<c:if test="${orderItem.commSts==0}">
										    		<c:set value="${notCommedCount + 1}" var="notCommedCount" /> 
										    	</c:if>
											   <tr>
												<td class="bdl">&nbsp;</td>
												<td class="w70">
													<div class="ncm-goods-thumb">
														<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">
															<img onmouseout="toolTip()" onmouseover="toolTip();" src="<ls:images item='${orderItem.pic}' scale='3'/>">
														</a>
													</div>
												</td>
												<%-- <td class="tl">
													<dl class="goods-name2">
														<dt>
															<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>" class="name">${orderItem.prodName}</a>
															<c:if test="${not empty orderItem.snapshotId && orderItem.snapshotId!=0}" >
																<span class="rec">
																<c:if test="${order.subType ne 'AUCTIONS' && order.subType ne 'SHOP_STORE' && order.subType ne 'SECKILL'}">
																     <a  href="<ls:url address='/snapshot/${orderItem.snapshotId}'/>" target="_blank">[交易快照]</a>
															    </c:if>
															    </span>
															 </c:if>
														</dt>
														<c:if test="${not empty orderItem.attribute}">
							                              <dd>${orderItem.attribute}</dd>
							                           	</c:if>
														<!-- 消费者保障服务 -->
													</dl>
												</td> --%>
												<td class="tl">
														<dl class="goods-name2">
															<dt style="max-width：230px">
																<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">
																	<span style="word-break: break-all; max-width:300px">${orderItem.prodName}</span>
																</a>
<%--																<c:if test="${not empty orderItem.snapshotId && orderItem.snapshotId!=0}" >--%>
																<a  href="<ls:url address='/snapshot/${orderItem.snapshotId}'/>" target="_blank">
																	<span class="table-btn-r"> [交易快照]</span>
																</a>
<%--																</c:if>--%>
																<!-- 所有订单类型都要有交易快照 所以注释掉这个if  -->
																<br>
															</dt>
															<dd>${orderItem.attribute}</dd>
															<!-- 消费者保障服务 -->
														</dl>
													</td>
												<td>
													<p> <fmt:formatNumber value="${orderItem.cash}" type="currency" pattern="0.00"></fmt:formatNumber></p>
											         <c:if test="${not empty orderItem.promotionInfo}">
											            <span class="sale-type">${orderItem.promotionInfo}</span>
											         </c:if>
											         <c:if test="${orderItem.refundState == 2}">
											         	<p style="color:#69AA46;"><fmt:formatNumber value="${orderItem.refundAmount}" type="currency" pattern="0.00"></fmt:formatNumber>(退)</p>
											         </c:if>
										        </td>
												<td>x${orderItem.basketCount} </td>
												<td>
													<c:if test="${order.actualTotal > 0}">
													     <c:choose>
										                		<c:when test="${order.status gt 2 &&  order.status le 4 &&  orderItem.refundState le 0 && orderItem.inReturnValidPeriod}">
										                		     <!-- 订单处于 待发货、已完成状态 并且 没有处理过退款或者退款失败的 -->
										                		    <p><a href="#" onclick="returnApply('${order.subId}','${orderItem.subItemId}','${order.subNum}');">退款/退货</a></p>
										                		</c:when>
										                		<c:when test="${orderItem.refundType==1 && (orderItem.refundState==1 || orderItem.refundState==2)}">
										                		    <p><a target="_blank" href="${contextPath}/p/refundDetail/${orderItem.refundId}">查看退款</a>
										                		</c:when>
										                		<c:when test="${orderItem.refundType==2 && (orderItem.refundState==1 || orderItem.refundState==2)}">
										                		    <p><a target="_blank" href="${contextPath}/p/returnDetail/${orderItem.refundId}">查看退货</a>
										                		</c:when>
										                </c:choose>
										            </c:if>
												</td>
							                    
							                    <!-- S 合并TD -->
							                    <c:if test="${orderItemStatues.index==0}">
							                       <c:choose>
									                		<c:when test="${fn:length(order.subOrderItemDtos)>0}">
									                			<!-- 未支付 -->
									                			<td rowspan="${fn:length(order.subOrderItemDtos)}" class="bdl">
									                		</c:when>
									                		<c:otherwise>
									                			<td class="bdl">
									                		</c:otherwise>
									                </c:choose>
												    <p class="">
												    	<c:choose>
															<c:when test="${order.refundState == 1 or ( order.status != 4 && order.status != 5 )  }">
																<li class="col-black"><fmt:formatNumber pattern="0.00" value="${order.actualTotal}" /></li>
															</c:when>
															<c:otherwise>
																<li><fmt:formatNumber pattern="0.00" value="${order.actualTotal}" /></li>
															</c:otherwise>
														</c:choose>
													</p>
													<p class="goods-freight"><c:if test="${not empty order.freightAmount}">(含运费<fmt:formatNumber pattern="0.00" value="${order.freightAmount}" />)</c:if></p>
													<p title="支付方式：">
													<c:choose>
								                		<c:when test="${order.payManner==1}">货到付款</c:when>
								                		<c:when test="${order.payManner==2}">在线支付</c:when>
								                		<c:when test="${order.payManner==3}">门店支付</c:when>
									                </c:choose>
												</td>
												<c:choose>
							                		<c:when test="${fn:length(order.subOrderItemDtos)>0}">
							                			<!-- 未支付 -->
							                			<td rowspan="${fn:length(order.subOrderItemDtos)}" class="bdl">
							                		</c:when>
							                		<c:otherwise>
							                			<td class="bdl">
							                		</c:otherwise>
									             </c:choose>
												 <p>
												 <c:if test="${order.refundState ne 1 }">
													<c:choose>
														<c:when test="${order.status==1 }">
															<c:choose>
																<c:when test="${order.subType == 'SHOP_STORE' && order.payManner==2}">
																    <span class="col-orange">门店付款自提</span>
																</c:when>
															    <c:when test="${order.subType == 'SHOP_STORE' && order.payManner==3}">
															    	<span class="col-orange">门店付款自提</span>
															    </c:when>
															    <c:otherwise>
															    	<span class="col-orange">待付款</span>
															    </c:otherwise>
															</c:choose>
														</c:when>
														<c:when test="${order.status==2 }">
															<c:choose>
														    	<c:when test="${order.subType eq 'MERGE_GROUP' && order.mergeGroupStatus eq 0}">
														    		<span class="col-orange">拼团中</span>
														    	</c:when>
														     	<c:when test="${order.subType eq 'GROUP' && order.groupStatus eq 0}">
														     		<span class="col-orange">团购中</span>
														     	</c:when>
															    <c:when test="${order.subType == 'SHOP_STORE' && order.payManner==2}">
															   		<span class="col-orange"> 待提货</span>
															    </c:when>
															    <c:otherwise>
															   	 	<span class="col-orange">待发货</span>
															   	</c:otherwise>
															</c:choose>
														</c:when>
														<c:when test="${order.status==3 }">
															<span class="col-orange">待收货</span>
														</c:when>
														<c:when test="${order.status==4 }">已完成</c:when>
														<c:when test="${order.status==5 }">交易关闭</c:when>
													</c:choose>
												</c:if>
												</p> <!-- 订单查看 -->
												<p><a target="_blank" href="${contextPath}/p/orderDetail/${order.subNum}">订单详情</a></p>
												<c:if test="${ (order.status eq 3 || order.status eq 4 ) && order.subType ne 'SHOP_STORE'}">
													<p><a target="_blank" href="${contextPath}/p/orderExpress/${order.subNum}" >查看物流</a></p>   
												</c:if>
													 <!-- 物流跟踪 -->
												</td>
													<c:choose>
									                		<c:when test="${fn:length(order.subOrderItemDtos)>0}">
									                			<!-- 未支付 -->
									                			<td rowspan="${fn:length(order.subOrderItemDtos)}" class="bdl bdr">
									                		</c:when>
									                		<c:otherwise>
									                			<td class="bdl bdr">
									                		</c:otherwise>
									             </c:choose>
													<!-- 永久删除 --> <!-- 锁定--> <!-- 取消订单 -->
													<p>
														<c:choose>
															<c:when test="${order.refundState != 1 && order.refundState != 2}">
																<c:choose>
																	 <c:when test="${order.status==2&&order.actualTotal!=0.0}">
																	    <c:choose>
																			<c:when test="${order.subType ne 'MERGE_GROUP' && order.subType ne 'GROUP'}">
																			    <c:if test="${sub_type != 'SHOP_STORE' && !order.remindDelivery&&order.refundState != 1 && order.subType ne 'SHOP_STORE'}">
																			    	<a class="btn-r" href="javascript	:void(0);" onclick="SendSiteMsg('${order.shopId}','${order.subNum}');">提醒发货</a>
																			    </c:if>
																				<a class="a-block" href="#" onclick="addAnotherSub2('${order.subNum}')">再来一单</a>
																				<a class="a-block" href="#" <c:if test="${fn:length(order.subOrderItemDtos)>0}"> onclick="refundApply('${order.subId}');"</c:if>>
																			    订单退款</a>
																			</c:when>
																			<c:when test="${order.subType eq 'GROUP' && order.groupStatus eq 1}">
																			    <c:if test="${sub_type != 'SHOP_STORE' && !order.remindDelivery&&order.refundState != 1}">
																			    	<a class="btn-r" href="javascript:void(0);" onclick="SendSiteMsg('${order.shopId}','${order.subNum}');">提醒发货</a>
																			    </c:if>
																			    <a class="a-block" href="#" onclick="addAnotherSub2('${order.subNum}')">再来一单</a>
																				<a class="a-block" href="#" <c:if test="${fn:length(order.subOrderItemDtos)>0}">onclick="refundApply('${order.subId}');"</c:if>>
																			    订单退款</a>
																			</c:when>
																			<c:otherwise>
																				<c:if test="${order.mergeGroupStatus eq 1}">
																				    <c:if test="${!order.remindDelivery && order.refundState != 1}">
																				    	<a class="btn-r" href="javascript:void(0);" onclick="SendSiteMsg('${order.shopId}','${order.subNum}');">提醒发货</a>
																				    </c:if>
																				   	<a class="a-block" href="#" onclick="addAnotherSub2('${order.subNum}')">再来一单</a>
																					<a class="a-block" href="#"
																				    <c:if test="${fn:length(order.subOrderItemDtos)>0}">onclick="refundApply('${order.subId}');"</c:if>>
																				    订单退款</a>
																				</c:if>
																			</c:otherwise>
																		</c:choose>
																	   
																	 </c:when>
																	 <c:when test="${order.status==1 && order.subType ne 'AUCTIONS' }">
																	 	<a class="btn-r" href="#" onclick="addAnotherSub2('${order.subNum}')">再来一单</a>
																	    <a class="a-block" href="javascript:void(0)" onclick="cancleOrder('${order.subNum}');">取消订单</a>
																	 </c:when>
																	  <c:when test="${order.status==2}">
																	  	<a class="btn-r" href="javascript:void(0);" onclick="SendSiteMsg('${order.shopId}','${order.subNum}');">提醒发货</a>
																	 	<a class="a-block" href="#" onclick="addAnotherSub2('${order.subNum}')">再来一单</a>
																	    <c:if test="${fn:length(order.subOrderItemDtos)>0 && order.actualTotal > 0}">
																	    	<a class="a-block" href="#" onclick="refundApply('${order.subId}');">订单退款</a>
																		</c:if>
																	   </c:when>
																	 <c:when test="${order.status==3 }">
																	 	<a class="btn-r" href="#" onclick="addAnotherSub2('${order.subNum}')">再来一单</a>	
																	    <a class="a-block" href="javascript:void(0)" onclick="orderReceive('${order.subNum}');">确认收货</a>
																	 </c:when>
																	 <c:when test="${order.status==4 && notCommedCount>0}">
																	 	<a class="btn-r" href="#" onclick="addAnotherSub2('${order.subNum}')">再来一单</a>
																	    <a class="a-block" href="${contextPath}/p/prodcomment/${order.subId}" onclick="">评价商品</a>
																	 </c:when>
																	 <c:when test="${order.status==4&&notCommedCount==0 }">
																		 <a class="btn-r" href="#" onclick="addAnotherSub2('${order.subNum}')">再来一单</a>
																	       <a class="a-block" href="${contextPath}/p/prodcomment/${order.subId}">查看评论</a>
																	 </c:when>
																	 <c:otherwise><a class="btn-r" href="#" onclick="addAnotherSub2('${order.subNum}')">再来一单</a></c:otherwise>
																  </c:choose>
															</c:when>
															<c:when test="${order.refundState eq 1}">
																<a class="btn-r" href="#" onclick="addAnotherSub2('${order.subNum}')">再来一单</a>
													  			<span class="a-block col-orange">退款退货中</span>
															</c:when>
															<c:otherwise><a class="btn-r" href="#" onclick="addAnotherSub2('${order.subNum}')">再来一单</a></c:otherwise>
														</c:choose>
													</p> <!-- 退款取消订单 --> <!-- 收货 --> <!-- 评价 --> <!-- 追加评价 -->
												</td>
												<!-- E 合并TD -->
							                    </c:if>
												
											</tr>
											</c:forEach>
										</tbody>
					 		 		<!-- -----------------其他订单  end---------------------  -->
					 		 		</c:otherwise>
					 		 	</c:choose>
						 </c:forEach>
				 	</c:otherwise>
			     </c:choose>
			</table>
			<!-- 订单表格 end -->
			
			<!-- 分页条 start -->
			<div style="margin-top:10px;" class="page clearfix">
		 		<div class="p-wrap">
	     			<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
		 		</div>
			</div>
			<!-- 分页条 end -->              		
		    </div>
	     </div>
	    <!-----------right_con end-------------->
	    <div class="clear"></div>
	 </div>
	<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/jquery.countdown.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/ToolTip.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/myorder.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/userOrderList.js'/>"></script>
<script type="text/javascript">
	var path="${contextPath}";
	var failedOwnerMsg = '<fmt:message key="failed.product.owner" />';
	var failedBasketMaxMsg = '<fmt:message key="failed.product.basket.max" />';
	var failedBasketErrorMsg = '<fmt:message key="failed.product.basket.error" />';
	var order_cancel="${order_cancel}";
	var auction_order_cancel="${auction_order_cancel}";
	
	$(function(){
		laydate.render({
			   elem: '#startDate',
			   calendar: true,
			   theme: 'grid',
			   trigger: 'click'
		  });
     
     	laydate.render({
			   elem: '#endDate',
			   calendar: true,
			   theme: 'grid',
			   trigger: 'click'
		  });
	});
	
</script>
</body>
</html>