<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ taglib uri="http://www.legendesign.net/date-util"  prefix="dateUtil" %>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}"  pattern="yyyy-MM-dd HH:mm" var="nowDate"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>预售订单 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>
<style>
.goods-name2 dt a:hover {
	color: #e5004f !important;
}
</style>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con">
     
         <div class="o-mt"><h2>预售订单</h2></div>
         
         <div class="pagetab2">
             <ul>           
               <li class="on"><span>订单列表</span></li>                  
               <li><span><a href="<ls:url address='/p/presellOrderDels'/>">回收站</a></span></li>      
             </ul>       
         </div>
          <form:form action="${contextPath}/p/presellOrder" method="get" id="order_search">  
	          <input type="hidden" value="${empty paramDto.curPageNO?1:paramDto.curPageNO}" name="curPageNO" />    
	          <div class="sold-ser clearfix" style=" margin-top:10px;">
		         <div class="item">
	              		订单状态：<select name="status" class="item-sel">
							<option  value=""  >所有订单</option>
							<option value="1"  <c:if test="${paramDto.status eq 1}">selected="selected"</c:if>>待付款</option>
							<option value="2" <c:if test="${paramDto.status eq 2}">selected="selected"</c:if>>待发货</option>
							<option value="3" <c:if test="${paramDto.status eq 3}">selected="selected"</c:if>>待收货</option>
							<option value="4" <c:if test="${paramDto.status eq 4}">selected="selected"</c:if>>已完成</option>
							<option value="5" <c:if test="${paramDto.status eq 5}">selected="selected"</c:if>>已取消</option>
						</select>
				 </div>
				 <div class="item">
	                 	订单号：<input type="text" value="${paramDto.subNumber}" name="subNumber" class="item-inp" style="width:200px;">
		         </div>
		         <div class="item">
	              		下单时间：<input class="item-inp" type="text" value="<fmt:formatDate value="${paramDto.startDate}" pattern="yyyy-MM-dd"/>" id="startDate" name="startDate" class="text Wdate" readonly="readonly">
						-
						<input class="item-inp" type="text" value="<fmt:formatDate value="${paramDto.endDate}" pattern="yyyy-MM-dd"/>" id="endDate" name="endDate"  class="text Wdate" readonly="readonly">
	                    <input type="button" class="btn-r" onclick="search()" id="search_order" class="bti" value="查 询">
		         </div>
	          </div>
        </form:form>
        
        <!-------------订单---------------->
         <div id="recommend" class="m10 recommend" style="display: block;border-width:0px 0px;">
            <table class="ncm-default-table order sold-table" cellpading=0 cellspacing="0">
			<thead>
				<tr>
					<th class="w10"></th>
					<th colspan="1">商品</th>
					<th class="w90">单价（元）</th>
					<th class="w90">数量</th>
					<th class="w110">订单金额</th>
					<th class="w90">交易状态</th>
					<th colspan="3" class="w120" style="border-right: 1px solid #e7e7e7;">交易操作</th>
				</tr>
			</thead>
			
			<c:choose>
				<c:when test="${empty requestScope.list}">
	     			<tbody>
				 	 	<tr>
				 	 		<td colspan="20">
				 	 			<!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
				 	 			<div class="warning-option"><!-- <i></i> --><span>没有符合条件的信息</span></div>
				 	 		</td>
				 	 	</tr>
			        </tbody>
				</c:when>
				<c:otherwise>
					<c:forEach items="${requestScope.list}" var="order" varStatus="orderstatues">
					    <tbody class="pay">
						<tr>
							<td class="sep-row" colspan="8"></td>
						</tr>
						<tr>
							<th colspan="8" style="height: 35px;background-color: #f9f9f9;">
								<span class="ml10">订单号：${order.subNo} </span>
								<span>下单时间： <fmt:formatDate value="${order.subCreateTime}" type="both" /></span> 
								<span>
								</span> 
								<c:if test="${order.status eq 5 or order.status eq 4}">
									<a onclick="removeOrder('${order.subNo}');" class="order-trash" href="javascript:void(0);"><i class="icon-trash"></i>删除</a>
								 </c:if>
								</th>
						</tr>
						
						<c:forEach items="${order.orderItems }" var="orderItem" varStatus="orderItemStatus">
						
					    <c:set value="0" var="notCommedCount" /> 
						<c:if test="${orderItem.commSts eq 0}">
				    		<c:set value="${notCommedCount + 1}" var="notCommedCount" /> 
				    	</c:if>
						<!-- S 商品列表 -->
						  <tr>
							<td class="w70" align="right">
								<div class="ncm-goods-thumb" style="    margin-left: 10px;">
									<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">
										<img onmouseout="toolTip()" onmouseover="toolTip();" src="<ls:images item='${orderItem.prodPic}' scale='3'/>">
									</a>
								</div>
							</td>
							<td class="tl"><dl class="goods-name2">
									<dt>
										<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">${orderItem.prodName}</a>
									</dt>
									<dd>${orderItem.attribute}</dd>
								</dl>
							</td>
							<td><p> <fmt:formatNumber value="${orderItem.prodCash}" type="currency" pattern="0.00"></fmt:formatNumber></p></td>
							<td>x${orderItem.basketCount}</td>
				           <td class="bdl">
							    <p class="">
									<strong><fmt:formatNumber value="${order.actualTotalPrice}" type="currency" pattern="0.00"></fmt:formatNumber></strong>
								</p>
								<p title="支付方式：">
								  <c:choose>
				                		<c:when test="${order.payManner eq 1}">
				                			货到付款
				                		</c:when>
				                		<c:when test="${order.payManner eq 2}">
				                			在线支付
				                		</c:when>
				                		<c:when test="${order.payManner eq 2}">
				                			门店支付
				                		</c:when>
				                </c:choose>
							</td>
							<!-- 如果是第一行 -->
							<c:if test="${orderItemStatus.index eq 0 }">
								<c:choose>
									<c:when test="${fn:length(order.orderItems) gt 0 }"><!-- 如果有多个订单项 -->
										<td class="bdl" rowspan="${fn:length(order.orderItems) }">
									</c:when>
									<c:otherwise>
										<td class="bdl">
									</c:otherwise>
								</c:choose>
							    <p>
								 <c:choose>
									 <c:when test="${order.status eq 1 }">
									 	<span class="col-orange">待付款</span>
									 </c:when>
								     <c:when test="${order.status==2 && order.isPayDeposit eq 1 && order.isPayFinal eq 1 }">
								    	<span class="col-orange">待发货</span>
								     </c:when>
								     <c:when test="${order.status==2 && order.payPctType eq 0}">
								    	<span class="col-orange">待发货</span>
								     </c:when>
									 <c:when test="${order.status eq 3 }">
									 	<span class="col-orange">待收货</span>
									 </c:when>
									 <c:when test="${order.status eq 4 }">已完成</c:when>
									 <c:when test="${order.status eq 5 }">交易关闭</c:when>
									 <c:otherwise>待付尾款</c:otherwise>
								</c:choose>
							    </p> <!-- 订单查看 -->
								<p>
									  <a target="_blank" href="${contextPath}/p/presellOrderDetail/${order.subNo}">订单详情</a>
								</p>
								
								<c:if test="${ order.status eq 3 or order.status eq 4 }">
									   	<p><a target="_blank" href="${contextPath}/p/orderExpress/${order.subNo}" >查看物流</a></p>   
								</c:if>
								 <c:if test="${order.status eq 4 }">
									    <p style="display:none;"><a target="_blank" href="${contextPath}/p/myreturnOrder">退换货</a></p>
								</c:if>
								 <!-- 物流跟踪 -->
							</td>
							
							<c:choose>
								<c:when test="${fn:length(order.orderItems) gt 0 }"><!-- 如果有多个订单项 -->
									<td class="bdl bdr" rowspan="${fn:length(order.orderItems) }" colspan="2" style="min-width: 150px;">
								</c:when>
								<c:otherwise>
									<td class="bdl bdr" colspan="2" style="min-width: 150px;">
								</c:otherwise>
							</c:choose>
								<p>
								   <c:choose>
									 <c:when test="${order.status eq 1 }">
									     <c:if test="${order.isPayDeposit eq 1 }">
									         <i class="icon-ban-circle"></i>已支付(等待尾款支付)</a>
								         </c:if>
								         <c:if test="${order.isPayDeposit eq 0}">
								             <a class="ncbtn ncbtn-grapefruit" href="javascript:void(0)" onclick="cancleOrder('${order.subNo}');">
									         <i class="icon-ban-circle"></i> 取消订单</a>
				                         </c:if>       
									 </c:when>
									 <c:when test="${order.status eq 3 }">
									    <a class="ncbtn ncbtn-grapefruit" href="javascript:void(0)" onclick="orderReceive('${order.subNo}');">
									    <i class="icon-ban-circle"></i> 确认收货</a>
									 </c:when>
									 <c:when test="${order.status eq 4 and notCommedCount gt 0}">
									       <a class="ncbtn ncbtn-grapefruit" href="${contextPath}/p/prodcomment/${order.subId}">
									       <i class="icon-ban-circle"></i>评价商品</a>
									 </c:when>
									 <c:when test="${order.status==4&&notCommedCount==0 }">
											       <a class="ncbtn ncbtn-grapefruit" href="${contextPath}/p/prodcomment/${order.subId}"><i
												class="icon-ban-circle"></i>查看评论</a>
										 </c:when>
									 <c:otherwise>&nbsp;</c:otherwise>
								  </c:choose>
								</p>
							</td>
							</c:if>
						</tr>
						</c:forEach>
						 <tr style="color:#999;">
			                <td colspan="2">阶段1：定金</td>
			                <td colspan="2">¥<fmt:formatNumber value="${order.depositPrice }" type="currency" pattern="0.00"></fmt:formatNumber>(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)</td>
			                <c:if test="${order.status eq 5 }">
			                	<td colspan="4" style="border-right: 1px solid #e6e6e6;"></td>
			                </c:if>
			                <c:if test="${order.status ne 5 }">
				                <c:if test="${order.isPayDeposit eq 0}">
				                	<c:if test="${dateUtil:getOffsetMinutes(order.subCreateTime,nowTime) le 60}">
				                		<td colspan="3">需在<font color="#e5004f">${60 - dateUtil:getOffsetMinutes(order.subCreateTime,nowTime)}</font>分钟之内付款</td>
				                		<td style="border-right: 1px solid #e6e6e6;">
					                		<a href="${contextPath}/p/presell/order/payDeposit/1/${order.subNo}/${order.activeId}" style="color:#e5004f" class="ncbtn ncbtn-grapefruit">付款</a>
					                	</td>
				                	</c:if>
				                	<c:if test="${dateUtil:getOffsetMinutes(order.subCreateTime,nowTime) gt 60}">
				                		<td colspan="4" style="border-right: 1px solid #e6e6e6;">已过期</td>
				                	</c:if>
				                </c:if>
				                <c:if test="${order.isPayDeposit eq 1 }">
				                	<td colspan="4" style="border-right: 1px solid #e6e6e6;">
				                	 	 <div><span>已完成</span><span style="margin-left: 30px;">${order.depositPayName}</span></div>
				                	     <div>${order.depositTradeNo}</div>
				                	</td>
				                </c:if>
			                </c:if>
			              </tr>
			              <c:if test="${0 ne order.finalPrice}">
			              <tr style="color:#999;">
			                <td colspan="2">阶段2：尾款</td>
			                <td colspan="2">¥<fmt:formatNumber value="${order.finalPrice}" type="currency" pattern="0.00"></fmt:formatNumber></td>
			                <c:if test="${order.status eq 5 }">
				               	<td colspan="4"></td>
				            </c:if>
			                <c:if test="${order.status ne 5 && order.isPayDeposit eq 1 }">
			                		<c:if test="${order.isPayFinal eq 0}">
				                		<td colspan="3" >
						                	尾款支付时间: <fmt:formatDate value="${order.finalMStart }" pattern="yyyy-MM-dd" /> - <fmt:formatDate value="${order.finalMEnd }" pattern="yyyy-MM-dd" />
						                </td>
					               </c:if>
					               <td colspan="4" style="border-right: 1px solid #e6e6e6;">
							             <c:if test="${order.isPayFinal eq 0}">
												<c:choose>
							                		<c:when test="${order.finalMStart gt nowDate }">
							                			未开始
							                		</c:when>
							                		<c:when test="${order.finalMStart le nowDate and order.finalMEnd gt nowDate}">
								                		<a href="${contextPath}/p/presell/order/payDeposit/2/${order.subNo}/${order.activeId}" target="_blank" style="color:#e5004f" class="ncbtn ncbtn-grapefruit">付款</a>
							                		</c:when>
							                		<c:when test="${order.finalMEnd le nowDate }">
							                			已过期
							                		</c:when>
							                	</c:choose>
						                  </c:if>
					                 	  <c:if test="${order.isPayFinal eq 1 }">
							                 	<font color="green">已完成</font><br/>
					                	        <font color="green">${order.finalPayName}</font><br/>
					                	        <font color="green">${order.finalTradeNo}</font><br/>
							              </c:if>
					                </td>
				             </c:if>
				             <c:if test="${order.status ne 5 && order.isPayDeposit eq 0 }">
			                		<c:if test="${order.isPayFinal eq 0}">
				                		<td colspan="3" >
						                	尾款支付时间: <fmt:formatDate value="${order.finalMStart }" pattern="yyyy-MM-dd" /> - <fmt:formatDate value="${order.finalMEnd }" pattern="yyyy-MM-dd" />
						                </td>
					                </c:if>
					               <td colspan="4" style="border-right: 1px solid #e6e6e6;">
							                                   等待定金支付
					               </td>
				             </c:if>
			              </tr>
			              </c:if>
					</tbody>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</table>
		<div style="margin-top:10px;" class="page clearfix">
   			<div class="p-wrap">
          		<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
   			 </div>
		</div>	              		
     </div>
        
     </div>
    <!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/ToolTip.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/presellOrder.js'/>"></script>
<script type="text/javascript">
	var path="${contextPath}";
	var failedOwnerMsg = '<fmt:message key="failed.product.owner" />';
	var failedBasketMaxMsg = '<fmt:message key="failed.product.basket.max" />';
	var failedBasketErrorMsg = '<fmt:message key="failed.product.basket.error" />';
	
	$(function(){
		laydate.render({
			   elem: '#startDate',
			   calendar: true,
			   theme: 'grid',
			   trigger: 'click'
		  });
     
     laydate.render({
			   elem: '#endDate',
			   calendar: true,
			   theme: 'grid',
			   trigger: 'click'
		  });
	});
	
	function search(){
	  	$("#order_search #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	  	$("#order_search")[0].submit();
	}
</script>
</body>
</html>