<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
	<title>门店管理-${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/store/store.css'/>" rel="stylesheet"/>
    <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
    <style>
    	.add-btn{
    		width: 80px;
			height: 25px;
			background-color: rgb(229, 0, 79);
			border: medium none;
			color: rgb(255, 255, 255);
			cursor: pointer;
    	}
    </style>
</head>
<body>
 
    <header class="ncsc-head-layout" style="display: block;">
        <div class="wrapper">
          <div class="ncsc-admin">
            <dl>
              <dt>${store.name}</dt>
              <dd>${store.province}&nbsp;${store.city}&nbsp;${store.area}&nbsp;${store.address}</dd>
              <dd>${store.contactTel}</dd>
            </dl>
<!--              <div class="pic">
		      		<img src=""   />
            </div> -->
          </div>
          <div class="center-logo">
             <a href="<ls:url address='/'/>" target="_blank">
                <img src="${contextPath }/resources/templets/images/store/logo-store.png"  style="max-height:89px;" />
                <h1>门店管理</h1>
            </a>
          </div>
          <ul class="ncsc-nav">
            <li> <a href="javascript:void(0);" class="current">门店商品库存</a> </li>
            <li> <a href="<ls:url address='/m/store/order'/>" >门店取货订单</a> </li>
            <li> <a href="javascript:void(0);" onclick="loginOut();">安全退出</a> </li>
          </ul>
        </div>
    </header>
    <div class="ncsc-layout wrapper">
        <div class="main-content"  >
                <div class="alert">
		            <ul>
		              <li>1、根据线上在售商品列表内容设置门店库存量；门店库存默认值为“0”时，该商品详情页面“门店自提”选项将不会出现您的门店信息，只有当您按所在门店的实际库存情况与线上商品对照设置库存后，才可作为线上销售门店自取点候选。</li>
		              <li>2、选择“库存设置”按钮，如该商品具有多项规格值，请根据规格值内容进行逐一“门店库存”设置，并保存提交。</li>
		              <li>3、如您的门店某商品线下销售引起库存不足，请及时手动调整该商品的库存量，以免消费者在线上下单后到门店自提时产生交易纠纷。</li>
		              <li>4、特殊商品不能设置为门店自提商品（如：虚拟商品、定金预售商品、F码商品等）</li>
		            </ul>
		          </div>
                    
                    
                    <form:form>
		            <table class="search-form">
			          <tbody>
			          	<tr>
			                <td><input class="add-btn"  type="button" value="添加商品" onclick="showProdlist();"></td>
			                <th>
			                  <select id="searchType">
			                    <option value="productName">商品名称</option>
<%--			                    <option value="productId">商品编号</option>--%>
			                  </select>
			                </th>
			                <td class="w160"><input class="text w150" type="text" id="searchName"></td>
			                <td class="tc w70">
			                  <label class="submit-border">
			                    <input class="submit"  onclick="search();" type="button" value="搜索">
			                  </label>
			                </td>
			              </tr>
			            </tbody>
		            </table>
		          </form:form>
		          
		          
                   <div  id="mainContent">
                 
                   </div>
                
         </div>
      <div class="ncsc-layout-right"> </div>
    </div>


   <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>

    <div style="position: absolute; top: 0px; left: 0px; background-color: rgb(0, 0, 0); opacity: 0.25; z-index: 999; width: 100%; height: 100%;display: none;"></div>
<!-- 弹窗 -->
</body>
<script type="text/javascript">
    var contextPath="${contextPath}";
	function loginOut(){
		layer.confirm("是否退出?", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   window.location.href=contextPath+"/m/logout";
		   });
	}
	
	function search(){
	   var searchType=$("#searchType").val();
	   var productName;
	   var productId;
	   var value=$.trim($("#searchName").val());
	   if(value==null || value==""){
		  $("div[id=mainContent]").load(contextPath+"/m/store/products/content",{"curPageNO" : 1});
	      return;
	   }
	   if("productName"==searchType){
	      productName=value;
	      productId=null;
	   }else if("productId"==searchType){
	      productId=value;
	      productName=null;
	      if(isNaN(productId)){
	        layer.alert("请输入正确的格式",{icon: 0});
	        return;
	      }
	   }                                   																				 
	   $("div[id=mainContent]").load(contextPath+"/m/store/products/content",{"curPageNO" : 1,"productname":productName,"productId":productId});
	}
	
	function pager(curPageNO){
	   var searchType=$("#searchType").val();
	   var productName;
	   var productId;
	   var value=$.trim($("#searchName").val());
	   if("productName"==searchType){
	      productName=value;
	      productId=null;
	   }else if("productId"==searchType){
	      productId=value;
	      productName=null;
	      if(!isNaN(productId)){
	        layer.alert("请输入正确的格式",{icon: 1});
	        return;
	      }
	   }
	   $("div[id=mainContent]").load(contextPath+"/m/store/products/content",{"curPageNO" : curPageNO,"productname":productName,"productId":productId});
	}
	
	
	$("div[id=mainContent]").load(contextPath+"/m/store/products/content",{"curPageNO" : 1});
	
	//弹窗显示商品列表
	function showProdlist(){
		layer.open({
			  title :"选择商品",
			  id: "prodDialog",
			  type: 2, 
			  content: contextPath+"/m/store/storeProdLayout", //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
			  area: ['1020px', '520px']
			}); 
	}
	
	//弹出设置库存窗口
	function showSetStocks(prodId){
		var url = contextPath + "/m/store/loadStoreProd/" + prodId;

        jQuery.ajax({
            url : url,
            type : 'get',
            async : false, // 默认为true 异步
            error : function(){
                layer.alert("该门店商品已被修改或删除，请删除后重新添加！！！",{icon:0});
            },
            success : function(result) {
                layer.open({
                    title :"设置库存",
                    area: ['700px','1rem'],
                    id: "setStockDialog",
                    type: 2,
                    maxHeight:500,
                    skin:'dialogStyle',
                    content: [url.toString(),'yes'], //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
                    success: function(layero, index) {
                        layer.iframeAuto(index);
                    }
                });
            }
        });

	}
	
	//发送设置商品库存请求
	function setStock(storeProd){
		$.ajax({
			url: contextPath + "/m/store/setStock",
			type: "POST",
			data: {
				"storeProd":storeProd
			},
			dataType: "JSON",
			success: function(result){
			   if("OK"==result){
			     	layer.msg("恭喜你,库存设置成功!",{icon: 1,time: 800},function(){
			     		var index =layer.getFrameIndex("setStockDialog");
			     		layer.close(index);  
		                $("div[id=mainContent]").load(contextPath+"/m/store/products/content",{"curPageNO" : 1});
					});
			   }else{
			     layer.alert(result,{icon: 2});
			   }
			}
		});
	}
	
	function deleteProd(storeProdId){
		
		layer.confirm("确定把该商品从当前门店中移除?", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   $.ajax({
					url: contextPath + "/m/store/deleteProd/" + storeProdId,
					type: "POST",
					dataType: "JSON",
					success: function(result){
						if(result == "OK"){
							$("div[id=mainContent]").load(contextPath+"/m/store/products/content",{"curPageNO" : 1});
							layer.msg("恭喜你,删除成功!",{icon: 1});
						}else if(result == "fail"){
							layer.msg("服务器异常 , 删除失败!",{icon: 2});
						}else{
							layer.alert(result,{icon: 2});
						}
					}
				});
			   
		   });
	}
		
		
	//删除SKU
	function removeSku(storeSkuId,obj,isFinal){
		var msg = "确定将该SKU从当前商品中移除,一旦移除将不可恢复?";
		if(isFinal){
			msg = "移除该SKU将会删除商品,您确定要删除?";
		}
		layer.confirm(msg, {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   $.ajax({
				url: contextPath + "/m/store/deleteSku/" + storeSkuId,
				type: "POST",
				dataType: "JSON",
				success: function(result){
					if(result == "OK"){
						if(isFinal){
							var index = layer.getFrameIndex("setStockDialog"); //先得到当前iframe层的索引
							layer.close(index); //再执行关闭   
							$("div[id=mainContent]").load(contextPath+"/m/store/products/content",{"curPageNO" : 1});
						}else{
							$(obj).parent().parent().remove();
							layer.msg("恭喜你,移除成功!",{icon: 1},function(){
								$("div[id=mainContent]").load(contextPath+"/m/store/products/content",{"curPageNO" : 1});
							});
						}
					}else if(result == "fail"){
						layer.alert("服务器异常 , 移除失败!",{icon: 2});
					}else{
						layer.alert(result,{icon: 2});
					}
				}
			});
		   });
		
	}
	
	function ajaxLoadProd(){
		$("div[id=mainContent]").load(contextPath+"/m/store/products/content",{"curPageNO" : 1});
	}
</script>

</html>
<style>
    .dialogStyle {
        top:50%!important;
        max-height: 600px !important;
        overflow-y: auto!important;
        transform: translateY(-50%)!important;
    }
</style>