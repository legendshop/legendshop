<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>物流信息 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con" style="overflow: unset;">
<div class="right-layout">
	<div class="ncm-flow-layout" style="border: 1px solid #ddd;">
		<div class="ncm-flow-container">
			<div class="title">
				<h3>物流详情</h3>
			</div>
			<div class="alert alert-block alert-info">
                 <ul>
				    <c:if test="${not empty order.userAddressSub}"> 
				     <li><strong>收货信息：</strong>${order.userAddressSub.receiver}&nbsp;&nbsp;&nbsp;&nbsp;${order.userAddressSub.mobile}&nbsp;&nbsp;&nbsp;&nbsp;${order.userAddressSub.detailAddress}</li>
					 <li>
					 	<strong>卖家留言：
					 	<c:choose>
					 		<c:when test="${empty order.orderRemark}">无</c:when>
					 		<c:otherwise>${order.orderRemark}</c:otherwise>
					 	</c:choose>
					 	</strong>
					</li>
					</c:if>
					<!-- <li><strong>发货信息：</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>  -->
				</ul>
			</div>
			<div class="tabmenu">
				<ul class="tab pngFix">
					<li class="active"><a>物流动态</a>
					</li>
				</ul>
			</div>
			<ul id="express_list" class="express-log">
               <li class="loading">加载中...</li>
			</ul>
			<div class="alert">以上部分信息来自于第三方，仅供参考，如需准确信息可联系卖家或物流公司</div>
		</div>
		<div class="ncm-flow-item">
			<div class="title">订单信息</div>
			<div class="item-goods">
			  	<!-- S 商品列表 -->
				<c:forEach items="${order.subOrderItemDtos}" var="orderItem" varStatus="orderItemStatues">
					   <dl>
						<dt>
							<div class="ncm-goods-thumb-mini">
								<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">
								<img src="<ls:images item='${orderItem.pic}' scale='3'/>">
								</a>
							</div>
						</dt>
						<dd>
							<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">${orderItem.prodName}</a> <span class="rmb-price">${orderItem.cash}&nbsp;*&nbsp;${orderItem.basketCount}
							</span>
						</dd>
					 </dl>
				</c:forEach>
				
			</div>
			<div class="item-order">
				<dl>
					<dt>运费：</dt>
					<dd>${order.freightAmount}</dd>
				</dl>
				<dl>
					<dt>订单总额：</dt>
					<dd>
						<strong>¥${order.actualTotal}</strong>
					</dd>
				</dl>
				<dl>
					<dt>订单编号：</dt>
					<dd>
						<a href="javascript:void(0);">${order.subNum}</a><a class="a"
							href="javascript:void(0);">更多<i class="icon-angle-down"></i>
							<div class="more">
								<span class="arrow"></span>
								<ul>
									<li>支付方式：<span>${order.payManner==1?'货到付款':'在线支付'}</span></li>
									  <li>下单时间：<span><fmt:formatDate value="${order.subDate}" type="both" /></span></li>
									<c:if test="${not empty order.payDate}">
									  <li>付款时间：<span><fmt:formatDate value="${order.payDate}" type="both" /></span></li>
									</c:if>
									<c:if test="${not empty order.dvyDate}">
									  <li>发货时间：<span><fmt:formatDate value="${order.dvyDate}" type="both" /></span></li>
									</c:if>
									<c:if test="${not empty order.finallyDate}">
									  <li>完成交易时间：<span><fmt:formatDate value="${order.finallyDate}" type="both" /></span></li>
									</c:if>
								</ul>
							</div> </a>
					</dd>
				</dl>
				  <c:if test="${not empty order.delivery}">
				    <dl>
					   <dt>物流单号：</dt>
					   <dd>
					  	${order.delivery.dvyFlowId}<a target="_blank" class="a" href="${order.delivery.delUrl}">${order.delivery.delName}</a>
					   </dd>
				      </dl>
				  </c:if>
				<dl>
					<dt>商&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家：</dt>
					<dd>
						${order.shopName}
					<!-- 	<a class="a" href="javascript:void(0);">更多<iclass="icon-angle-down"></i>
							<div class="more">
								<span class="arrow"></span>
								<ul>
									<li>所在地区：<span>xx</span>
									</li>
									<li>联系电话：<span></span>
									</li>
								</ul>
							</div> 
						</a> -->
						     <div>
						       <span member_id="2"></span>
						    </div>
					</dd>
				</dl>
			</div>
		</div>
	</div>
	
</div>
 </div>
    <!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
<script>
		$(function() {
		  userCenter.changeSubTab("myorder");
		  
		   var dvyFlowId="${order.dvyFlowId}";
		   var dvyTypeId="${order.dvyTypeId}";
		   var reciverMobile="${order.userAddressSub.mobile}";
	       var html="";
	       if(dvyFlowId=="" || dvyTypeId ==""){
	          html="<li>没有物流信息</li>";
	       }
	       else{
	             $.ajax({
					url:"${contextPath}/p/order/getExpress", 
					data:{"dvyFlowId":dvyFlowId,"dvyTypeId":dvyTypeId,"reciverMobile":reciverMobile},
					type:'POST', 
					async : false, //默认为true 异步   
					dataType:'json',
					success:function(result){
					    if(result=="fail" || result=="" ){
					       html="没有物流信息";
					       loaddely=true;
						}else{
						     var list = eval(result);
							 for(var obj in list){ //第二层循环取list中的对象 
							    html+="<li>"+list[obj].ftime+"&nbsp;&nbsp;"+list[obj].context+"<li>";
							 } 
						}
					}
			      });
	      }
	      $('#express_list').html(html);
		});

	</script>
</body>
</html>