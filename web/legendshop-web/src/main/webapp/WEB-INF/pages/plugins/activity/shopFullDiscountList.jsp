<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<jsp:useBean id="now" class="java.util.Date" /> 
<fmt:formatDate value="${now}"  pattern="yyyy-MM-dd HH:mm:ss" var="nowDate"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>全场满折促销-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/active/actives${_style_}.css'/>" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">全场满折促销</span>
				</p>
			</div>
			
			<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
            <div id="rightContent" class="right_con">
            <div class="pagetab2" style="margin-top: 20px;">
				<ul id="listType">
					<li class="on"><span>全场满折促销</span></li>
				</ul>
			</div>
			<div class="sold-ser sold-ser-no-bg clearfix">
				<div style="float: left;">
					<a href="<ls:url address='/s/addShopMarketing/1'/>" style="margin:0;" class="btn-r big-btn">添加活动</a>
				</div>
				<form:form  action="${contextPath}/s/shopFullCourtMarketing" method="post" id="form1">
					<div style="float: right;">
						<span>活动状态：</span>
				    	<select name="state">
		                	<option value="" <c:if test="${empty marketing.state}">selected="selected"</c:if>>全部</option>
		                    <option value="0" <c:if test="${marketing.state eq 0}">selected="selected"</c:if>>下线</option>
		                    <option value="1" <c:if test="${marketing.state eq 1}">selected="selected"</c:if>>正常</option>
		                    <option value="2" <c:if test="${marketing.state eq 2}">selected="selected"</c:if>>已过期</option>
		              	</select>
					    <span>活动名称：</span> 
						<input type="hidden" value="${empty curPageNO?1:curPageNO}" name="curPageNO"  id="curPageNO"> 
						<input type="text" value="${marketing.marketName}" name="marketName" id="marketName" class="text w150"> 
						<input type="submit"  value="搜索" class="submit btn-r">
					</div>
				</form:form>
			</div>
			<table class="ncm-default-table sold-table">
				<thead>
					<tr>
						<th class="w10">活动名称</th>
						<th class="w150 tl">开始时间</th>
						<th class="w150 tl">结束时间</th>
						<th class="w150 tl">参与商品</th>
						<th class="w150 tl">状态</th>
						<th class="w200 tl" style="border-right: 1px solid #e4e4e4">操作</th>
					</tr>
				</thead>
				<tbody>
				 <c:choose>
		           <c:when test="${empty requestScope.list}">
		              <tr>
					 	 <td colspan="20">
					 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
					 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
					 	 </td>
					 </tr>
		           </c:when>
		           <c:otherwise>
		           	   <c:forEach items="${list}" var="ms" varStatus="status">
						   <tr>
								<td>${ms.marketName}</td>
								<fmt:formatDate value="${ms.startTime}" var="startTime" pattern="yyyy-MM-dd HH:mm:ss"/>
								<td>${startTime}</td>
								<fmt:formatDate value="${ms.endTime}" var="endTime"  pattern="yyyy-MM-dd HH:mm:ss"/>
								<td>${endTime}</td>
								<td>
								<c:choose>
										<c:when test="${ms.isAllProds==0}">
										       部分商品
										</c:when>
										<c:when test="${ms.isAllProds==1}">
										       全部商品
										</c:when>
								</c:choose>
								</td>
								<td>
								<c:choose>
										<c:when test="${nowDate gt endTime}">
										       已过期
										</c:when>
										<c:when test="${ms.state==0}">
										       未发布
										</c:when>
										<c:when test="${ms.state==1}">
										    <c:choose>
										      <c:when test="${nowDate lt startTime}">
										                        将要进行
										      </c:when>
										      <c:otherwise>
										                   正常状态
										      </c:otherwise>
										    </c:choose>  
										     
										</c:when>
								</c:choose>
								</td>
								<td>
								   <span>
								   		<c:if test="${nowDate lt endTime}">
									       <c:choose>
												<c:when test="${ms.state eq 0}">
												    <a class="btn-r" onclick="onlineShopMarketing('${ms.id}');" href="javascript:void(0);">发布</a>
												</c:when>
												<c:when test="${ms.state eq 1}">
												      <a class="btn-g" onclick="offlineShopMarketing('${ms.id}');" href="javascript:void(0);">下线</a>
												</c:when>
									        </c:choose>
								   		</c:if>
								   		<a class="btn-g" href="${contextPath}/s/shopMarketing/${ms.id}">查看</a>
								   		<c:if test="${nowDate gt endTime || ms.state==0}">
						   					<a class="btn-g" onclick="deleteShopMarketingMansong('${ms.id}');" href="javascript:void(0);">删除</a>
						   				</c:if>
									</span>
								</td>
							</tr>
						</c:forEach>
		           </c:otherwise>
		           </c:choose>
				</tbody>
			</table>
			<!--page-->
					
			 <div style="margin-top:10px;" class="page clearfix">
				  <div class="p-wrap">
				        <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
				  </div>
		  	</div>
		  	
			<div class="clear"></div>
			</div>
		  
		</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	 <script src="<ls:templateResource item='/resources/templets/js/shopFullDiscountList.js'/>"></script>
	<script type="text/javascript">
    var contextPath = '${contextPath}';
</script>  
</body>
</html>
