	<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
	<%@ include file='/WEB-INF/pages/frontend/templetGoat/common/taglib.jsp' %>
	
	<c:forEach items="${requestScope.list}" var="recharge">
	   <tr class="gra">
		    <td><fmt:formatNumber value="${recharge.amount}" pattern="#,#0.00#"/></td>
		    <td>
		                    <c:choose>
								<c:when test="${recharge.paymentState==0}">
								   --------
								</c:when>
								<c:when test="${recharge.paymentState==1}">
								     ${recharge.paymentName}
								</c:when>
							 </c:choose>
		    </td>
			<td>
			  <fmt:formatDate value="${recharge.addTime}"  type="both"/>
			</td>
			<td>
				<c:choose>
					<c:when test="${recharge.paymentState==0}">
					   <a href="javascript:void(0);" class="commodity-prize-on" onclick="deleteRechargeDetail('${recharge.sn}');">删除</a>
					</c:when>
					<c:otherwise>
					   <a href="<ls:url address='/p/m/predeposit/rechargeDetail/${recharge.id}'/>" class="commodity-prize-on">查看</a>
					</c:otherwise>
				 </c:choose>
			</td>
	   </tr>
	 </c:forEach>

