<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<style type="text/css">
	html{font-size:13px;color:#333;}
	.prod-info{height:40px;}
	.prod-info img{float:left;}
	.prod-info h3{float:left;padding-left:20px;line-height:40px;}
	.skulist{border:solid #e4e4e4; border-width:1px 0px 0px 1px;width: 100%;}
	.skulist tr td{border:solid #e4e4e4; border-width:0px 1px 1px 0px; padding:10px 0px;text-align: center;font-size:13px;}
	.skulist tr td input[type="text"]{
		width:50px;height:30px;border:1px solid #ddd;color:#333;background-color: #fff;
	}
	.skulist tr td input[type="button"]{
		width:50px;height:30px;border:1px solid #ddd;color:#333;background-color: #fff;cursor: pointer;
	}
	.submit-btn-box{
		width:100%;
		height:40px;
		padding-top: 10px;
	}
	.submit-btn-on{
		width:80px;
		height:20px;
		line-height:20px;
		background-color:rgb(229,0,79);
		border:1px solid #eee;
		color:#fff;
		cursor: pointer;
		margin:0 auto;
		display:block;
		text-decoration: none;
		padding:5px 10px;;
	}
</style>
<c:if test="${not empty storeProductDto}">
	<div class="prod-info">
		<input type="hidden" name="storeProdId" value="${storeProductDto.id }"/>
		<input type="hidden" name="prodId" value="${storeProductDto.productId }"/>
		<img src="<ls:images item='${storeProductDto.pic}' scale='3'/>" alt="商品图片"/>
		<h3>${storeProductDto.productName }</h3>
	</div>
	<table id="skuList" class="skulist" existSku="${not empty storeProductDto.storeSkuDto}">
		<c:if test="${empty storeProductDto.storeSkuDto}">
			<tr >
				<td colspan="4"><b>商品信息</b></td>
			</tr>
			<tr>
				<td>商品名称</td>
				<td>商品价格</td>
				<td>库存数量</td>
			</tr>
				<tr>
					<td>${storeProductDto.productName}</td>
					<td>${storeProductDto.cash}</td>
					<td>
						<input type="text" name="prodStocks" value="${storeProductDto.stock}" 
						style="width:50px;height:30px;border:1px solid #ddd;background-color: #fff"/>
					</td>
				</tr>
		</c:if>
		<c:if test="${not empty storeProductDto.storeSkuDto}">
			<tr >
					<td colspan="5"><b>商品SKU信息</b></td>
				</tr>
			<tr class="table-head">
				<td>单品名称</td>
				<td>属性</td>
				<td>单品价格</td>
				<td>单品库存数量</td>
				<td>操作</td>
			</tr>
			<c:forEach items="${storeProductDto.storeSkuDto}" var="sku" varStatus="status">
				<tr>
					<input type="hidden"  name="storeSkuIds" value="${sku.id}"/>
					<input type="hidden"  name="skuIds" value="${sku.skuId}"/>
					<td >${sku.skuName}</td>
					<td>${sku.properties}</td>
					<td>${sku.cash}</td>
					<td>
						<input type="text" name="skuStocks" value="${sku.stock}" 
							style="width:50px;height:30px;border:1px solid #ddd;background-color: #fff"/>
					</td>
					<td>
						<input type="button" value="移除" onclick="removeSku('${sku.id}',this);"/>
					</td>
				</tr>
			</c:forEach>
		</c:if>
	</table>
</c:if>
<div class="submit-btn-box">
	<a class="submit-btn-on" href="javascript:setStocks();">提交库存设置</a>
</div>
<script type="text/javascript">
		 var contextPath="${contextPath}";
			
		 function setStocks(){
		 	
		 	 var storeProd = new Object();
			 storeProd.id = $("input[name='storeProdId']").val();
			 storeProd.productId =  $("input[name='prodId']").val();
			 var existsku=$("table[id='skuList']").attr("existsku");
			 var tr=$("table[id='skuList'] tbody ").find("tr:gt(1)");
		 	 var storeSkuDto=new Array();
		     for(var i = 0;i<tr.length;i++){
		            if(existsku=="true"){
		               var  storeSkuIdsInputs = $(tr[i]).find("input[name='storeSkuIds']").val();
			           var _sku =$(tr[i]).find("input[name='skuIds']").val();
			           if(isBlank(_sku)){
			              layer.alert("参数有误",{icon:0});
			              return;
			           }
			           var stocks=$(tr[i]).find("input[name='skuStocks']").val();
			           if(isBlank(stocks)){
			              layer.alert("请输入库存信息",{icon:0});
			              return;
			           }else if(isNaN(stocks)){
			               layer.alert("请输入正确的库存信息",{icon:0});
			               return;
			           }
			            var storeSku = new Object();
					    storeSku.id = storeSkuIdsInputs;
						storeSku.skuId =_sku;
						storeSku.productId = storeProd.productId;
						storeSku.stock = stocks;
			            storeSkuDto.push(storeSku);
		           }else{
		               var stocks=$(tr[i]).find("input[name='prodStocks']").val(); 
		               if(isBlank(stocks)){
			              layer.alert("请输入库存信息",{icon:0});
			              return;
			           }else if(isNaN(stocks)){
			               layer.alert("请输入正确的库存信息",{icon:0});
			               return;
			           }
			            storeProd.stock = stocks;
		           }
		    }
		    storeProd.storeSkuDto=storeSkuDto;
		    var str=JSON.stringify(storeProd);
		     if(isBlank(str)){
		           layer.alert("请输入正确信息",{icon:0});
		           return;
		     }
			this.parent.setStock(str);
	 }
	 
	function isBlank(val){
	   if(val==null || val=="" || val==undefined){
	      return true;
	   }
	   return false;
	}
	
	function removeSku(storeSkuId,obj){
		var num = $(".table-head ~ tr").length;
		if(num <= 1){
			this.parent.removeSku(storeSkuId,obj,true);
		}else{
			this.parent.removeSku(storeSkuId,obj,false);
		}
	}
</script>