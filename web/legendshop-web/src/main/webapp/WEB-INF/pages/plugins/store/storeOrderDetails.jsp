<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
 <%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%
	response.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT");   
	// 设置 HTTP/1.1 no-cache 头   
	response.setHeader("Cache-Control", "no-store,no-cache,must-revalidate");   
	// 设置 IE 扩展 HTTP/1.1 no-cache headers， 用户自己添加   
	response.addHeader("Cache-Control", "post-check=0, pre-check=0");   
	// 设置标准 HTTP/1.0 no-cache header.   
	response.setHeader("Pragma", "no-cache");
 %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="renderer" content="webkit"/>
    <c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
    <title>填写核对订单信息-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
    <link rel="stylesheet" href="<ls:templateResource item='/resources/common/css/jquery.autocomplete.css'/>">
    <link rel="stylesheet" href="<ls:templateResource item='/resources/templets/css/store/storeOrderDetails.css'/>">
    <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/plugins/showLoading/css/showLoading.css'/>" />
</head>
<body  class="graybody">
	<div id="doc">
         <!--顶部menu-->
		  <div id="hd">
		   
		      <!--hdtop 开始-->
		       <%@ include file="/WEB-INF/pages/plugins/main/home/header.jsp" %>
		      <!--hdtop 结束-->
		    
		       <!-- nav 开始-->
		        <div id="hdmain_cart">
		          <div class="yt-wrap">
		            <h1 class="ilogo">
		                <a href="${contextPath}/"><img src="<ls:photo item="${systemConfig.logo}"/>"  style="max-height:89px;" /></a>
		            </h1>
		            
		            <!--page tit-->
		              <div class="cartnew-title">
		                        <ul class="step_h">
		                            <li class="done"><span>购物车</span></li>
		                            <li class="on done"><span>填写核对门店订单信息</span></li>
		                            <li class=" "><span>提交门店订单</span></li>
		                        </ul>
		              </div>
		            <!--page tit end-->
		          </div>
		        </div>
		      <!-- nav 结束-->
		 </div><!--hd end-->
		<!--顶部menu end-->
    
       
         <div id="bd" style="margin-bottom: 30px;">
		      <div class="yt-wrap ptit2">填写并核对门店订单信息</div>
		      <!--order content -->
		         <div class="yt-wrap ordermain">
		          
		          <!-- 支付方式 -->
		          <div id="pay" class="pay">
		            <h3>支付方式<b style="display: none;" onclick="PayMethod.openModifyWindow();" class="update">修改</b><span id="pay_msg"></span></h3>
		              <div style="display: block;" id="pay_opened">
		              <ul>
		               
		                <li>
		                   <input type="radio"  value="2" id="online_pay" name="pay_opened_pm_radio" checked="checked">
		                   <label style="cursor:pointer;margin-right:80px;"  for="online_pay">在线支付</label>
		                   <em>暂支持绝大数银行卡及第三方在线支付</em>
		                </li>
		                 <!-- <li>
		                   <input type="radio" checked="checked" value="3" id="online_pa2" name="pay_opened_pm_radio">
		                   <label style="cursor:pointer;margin-right:80px;"  for="online_pay2">门店支付</label>
		                   <em>支持商家线下门店支付</em>
		                </li> -->
		              </ul>
		            </div>
		          </div>
		          
		          <!-- 商品清单 -->
		          <div class="pro-list">
		          	<c:forEach items="${requestScope.userShopCartList.shopCarts}" var="shopCarts" varStatus="status">
		            <h3 style="margin-bottom: -15px;">${shopCarts.shopName}-门店商品清单</h3>
		              <a style="float: right;margin-top: -20px;color: #3f6de0;" href="${contextPath}/shopCart/shopBuy">返回购物车修改</a>
		            <div id="shoppinglistDiv">
		            	<div id="shopping">
						<c:forEach items="${shopCarts.storeCarts}" var="storeCarts" varStatus="status">
							<div class="shoppinglist">
								<div class="dis-modes">
									<p>
										门店：
										<font style="color: #e5004f;">${storeCarts.storeName}</font>
									</p>
                                    <div id="deliveryMode" style="margin-top:15px;">
										提货地址：
										${storeCarts.storeAddr}
									</div>
                                    <div class="shop-add">
										<div class="shop-wor">
											<input type="text" style="height: 26px;text-indent: 5px;" name="buyerName" value="${storeCarts.buyerName}" storeId="${storeCarts.storeId}"  maxlength="10" placeholder="提货人">
										</div>
									</div>
                                    <div class="shop-add">
										<div class="shop-wor">
											<input type="text" style="height: 26px;text-indent: 5px;" name="telPhone" value="${storeCarts.telphone}" storeId="${storeCarts.storeId}" maxlength="11" placeholder="手机号码">
										</div>
									</div>
									<div class="shop-add">
										<div class="shop-wor">
											<input type="text" placeholder="给卖家留言,限45个字" name="remark"  storeId="${storeCarts.storeId}" maxlength="45">
										</div>
									</div>
								</div>
                                <table class="cartnew_table" style="    width: 820px;float: right; margin-top: 0px;">
								<thead style="border: 1px solid #e4e4e4;border-bottom: 0;">
									<tr>
										<th align="left" style="width:450px;">店铺宝贝</th>
										<th>数量</th>
										<th>单价</th>
									</tr>
								</thead>
								<tbody style="border: 1px solid #e4e4e4;border-top: 0;">
			                    <c:forEach items="${storeCarts.cartItems}" var="basket" varStatus="s">
				                    <tr>
				                        <td class="textleft"> 
						                        <div class="cartnew-img">
						                           <input type="hidden" value="${basket.prodId}" skuId="${basket.skuId==null?0:basket.skuId}" count="${basket.basketCount}" name="goods_id[]">
						                           <a class="a_e" target="_blank" href="${contextPath}/views/${basket.prodId}">
						                           <img title="${basket.prodName}" src="<ls:images item='${basket.pic}' scale='3' />" > 
						                           </a>                            
						                        </div>
					                           <ul class="cartnew-ul" >
					                           	<li> <a href="${contextPath}/views/${basket.prodId}" target="_blank" class="a_e">${basket.prodName}</a></li>
				                                <li>
				                                    <c:if  test="${not empty basket.cnProperties}" >
				                                       ${basket.cnProperties}
				                                    </c:if>
				                                 </li>
				                                  <c:if test="${not empty basket.cartMarketRules}">
						                                 <c:forEach items="${basket.cartMarketRules}" var="rule" >
						                                     <li><span>优惠信息</span><em>${rule.promotionInfo}</em></li>
						                                 </c:forEach>
						                           </c:if>
				                              </ul>
				                        </td>
				                        <td>
				                          	${basket.basketCount}
				                        </td>
				                         <td>
			                               <c:if test="${basket.price-basket.promotionPrice > 0}">
					                          <del style="color:#999;">￥ <fmt:formatNumber pattern="0.00" maxFractionDigits="2" groupingUsed="false" value="${basket.price}"/></del><br>   	 
							               </c:if>
							               <span style="font-size:14px; color:#e5004f;" >
							                                                     ￥<fmt:formatNumber pattern="0.00" maxFractionDigits="2" groupingUsed="false" value="${basket.promotionPrice}"/>
							               </span>
				                           <br/><span class="no_send_tpl">无货</span>
				                         </td>
				                    </tr>  
			                    </c:forEach>
		                    </tbody>   	  
		                </table>
								<div class="clr"></div>
							</div>
						</c:forEach>
						</div>
		             <!-- 确认订单按钮 -->
			         <%@ include file="/WEB-INF/pages/plugins/main/orderConfirm.jsp" %>
			           
		        </div>
		      </c:forEach>
		   		<!--order content end-->
		   </div>
	     
	     <form:form id="orderFormReload" action="${contextPath}/p/store/order/storeOrderDetails" method="post">
		       <input name="ids" value="${shopCartItems}"  type="hidden"/>
		       <input name="shopId" value="${shopId}"  type="hidden"/>
		</form:form>
		
      </div>
	</div>
	
		 <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
</body>
    <script type="text/javascript">
		var contextPath = '${contextPath}';
		/* var data = {allCount:"${requestScope.userShopCartList.orderTotalQuanlity}"}; */
		var allCount = "${requestScope.userShopCartList.orderTotalQuanlity}";
		var shopId="${shopId}";
		var token = '${SESSION_TOKEN}';
		
		
		var shoppinglist =  $('.shoppinglist');
		var cartnew_table = $('.cartnew_table');
		
		for(var i = 0; i< shoppinglist.length; i++) {
			
			var Height = shoppinglist[i].offsetHeight;
				cartnew_table[i].style.height = Height + 'px'; 
						
		}
		
		
		
		
   </script>
   <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
   <script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>"></script>
   <script  charset="utf-8" src="<ls:templateResource item='/resources/plugins/showLoading/js/jquery.showLoading.min.js'/>"></script> 
   <script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/store/storeOrderDetails.js'/>"></script>
   <script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
</html>
