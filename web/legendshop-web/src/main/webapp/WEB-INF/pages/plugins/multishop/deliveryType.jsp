<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>配送方式-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/templets/css/multishop/seller-distribution.css'/>" />
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">配送方式</span>
				</p>
			</div>
				<%@ include file="included/sellerLeft.jsp" %>
				<div class="seller-distribution">
					<div class="pagetab2" style="margin-bottom:15px;">
						<ul>
							<li class="on" ><a href="#">配送方式</a></li>
			        		<li><a onclick="window.location.href='<ls:url address='/s/deliveryType/load'/>'">新增配送方式</a></li>
						</ul>
				 	</div>
					<%-- <div class="set-up">
						<a class="btn-r" style="width:100px;height:30px;line-height: 30px;"" onclick="window.location.href='<ls:url address='/s/deliveryType/load'/>'">新增配送方式</a>
					</div> --%>
					<table class="distribution-table sold-table" style="width: 100%;max-width: 1024px; TBBLE-LBYOUT: fixed; margin-top:5px;">
						<tr class="distribution-tit">
							<th>配送方式</th>
							<th>系统默认</th>
							<th>描述</th>
							<th width="150">操作</th>
						</tr>
						 <c:choose>
				           <c:when test="${empty requestScope.list}">
				              <tr>
							 	 <td colspan="20">
							 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
							 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
							 	 </td>
							 </tr>
				           </c:when>
				           <c:otherwise>
				              <c:forEach items="${requestScope.list}" var="item" varStatus="status">				
								<tr class="detail">
									<td style="max-width: 100px">${item.name}</td>
									<td style="max-width: 100px"><c:choose>
									     <c:when test="${item.isSystem=='1'}">是</c:when>
									     <c:when test="${item.isSystem=='0'}">否</c:when>
									   </c:choose></td>
									<td style="max-width: 300px; min-width: 200px; word-break:break-all;">${item.notes}</td>
									<td>
										<c:if test="${item.isSystem=='0'}">
										<ul class="mau">
											<li><a href="<ls:url address='/s/deliveryType/load/${item.id}'/>"  class="btn-r">编辑</a></li>
											<li><a href='javascript:deleteById("${item.id}")' class="btn-g">删除</a></li>
										</ul>
										</c:if>
									</td>
								</tr>
							</c:forEach>
				           </c:otherwise>
			           </c:choose>
					</table>
					<div style="margin-top:10px;" class="page clearfix">
					     	<div class="p-wrap">
								<span class="p-num">
									<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
								</span>
					     	</div>
			  		</div>
				<!-- 已创建配送方式 -->
				<div class="distribution-space"></div>
				<!-- 新增创建配送方式 -->	
			</div>
 		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
<script type="text/javascript">
 
 $(document).ready(function() {
    userCenter.changeSubTab("deliveryType");
 });
    
   function deleteById(id) {
			      
	      layer.confirm('你确定删除该配送方式？',{
		  		 icon: 3
		  	     ,btn: ['确定','取消'] //按钮
		  	   }, function () {
			     	
		  		 window.location = "<ls:url address='/s/deliveryType/delete/" + id + "'/>";
			})
	}	
   function pager(curPageNO){
	   userCenter.loadPageByAjax("/s/deliveryType?curPageNO=" + curPageNO);
	}
</script>
</body>
</html>