<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<c:set var="_userDetail" value="${shopApi:getSecurityUserDetail(pageContext.request)}"></c:set>
<c:set var="orderCountMap" value="${indexApi:calPendingOrderCount(_userDetail.userId)}"></c:set>
<c:set var="predepositAmount" value="${indexApi:calPredepositAmount(_userDetail.userId)}"></c:set>
<c:set var="intergalAmount" value="${indexApi:calIntergalAmount(_userDetail.userId)}"></c:set>
<c:set var="unuseCouponCount" value="${indexApi:calUnuseCouponCount(_userDetail.userId)}"></c:set>
<c:set var="redPacketCount" value="${indexApi:calRedPacketCount(_userDetail.userId)}"></c:set>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>会员中心 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
</head>
<style>
	.sold-table td{
		text-align:center;
	}
</style>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>

		<!----两栏---->
		<div class=" yt-wrap" style="padding-top:10px;">
			
			<%@include file='usercenterLeft.jsp'%>
			
			<!-----------right_con-------------->
			<div class="right_con">

				<!--user info-->
				<div id="userinfo" class="m3">
					<div class="user">
						<div class="u-icon">
							<c:choose>
								<c:when test="${not empty portraitPic }">
									<img alt="用户头像" src="<ls:photo item='${portraitPic}' />"  id="infoimg" style="width: 100px;height :100px;">
								</c:when>
								<c:otherwise>
									<img src="${contextPath}/resources/templets/images/no-img_mid_.jpg"  id="infoimg" style="width: 100px;height :100px;">
								</c:otherwise>
							</c:choose>
						</div>
						<div class="extra">
							<a href="javascript:void(0);" id="uploadthickbox">修改头像</a>
						</div>
					</div>
					<!--user-->
					<div id="i-userinfo">
						<div class="username">
							<div id="userinfodisp" class="fl ftx-03">
								<strong>${sessionScope.SPRING_SECURITY_LAST_USERNAME}</strong>,欢迎您！<c:if test="${not empty lastLoginTime }">上一次登录时间：<fmt:formatDate value="${lastLoginTime}" pattern="yyyy-MM-dd HH:mm:ss"/></c:if>
							</div>
							<div id="jdTask" class="extra"></div>
						</div>
						<!--
						<div class="account">
							<div class="member" id="memberInfo">
								<div class="rank r3">
									<s></s><a href="#" class="flk-04" target="_blank">${sessionScope.LAST_USERGRADE }</a>
								</div>
								<div class="memb-info">
									今年已消费￥4569，再消费￥431可升级到 <a href="#" class="flk-04" target="_blank">银牌用户</a>
								</div>
							</div>

						</div>
						-->
						<div class="acco-safe">
							<span class="fl">账户安全：</span> 
							<c:choose>
								<c:when test="${userSecurity.phoneVerifn==1}">
									<a class="teln flk-04 safe" href="${contextPath}/p/security"><s></s>手机已验证</a>
								</c:when>
								<c:otherwise>
									<a class="teln flk-04" href="${contextPath}/p/security"><s></s>手机未验证</a>
								</c:otherwise>
							</c:choose>
							<c:choose>
								<c:when test="${userSecurity.mailVerifn==1}">
									<a class="teln flk-04 safe" href="${contextPath}/p/security"><s></s>邮箱已验证</a>
								</c:when>
								<c:otherwise>
									<a class="teln flk-04" href="${contextPath}/p/security"><s></s>邮箱未验证</a>
								</c:otherwise>
							</c:choose>
						</div>


						<!--end-->
						<div id="remind">
							<div class="oinfo">
								<dl class="fore">

									<dt>订单提醒：</dt>
									<dd>
										<a href="${contextPath}/p/myorder?state_type=1" class="flk-03">待付款订单<span  style="color:#e5004f;">(${orderCountMap.unPayOrderCount})</span></a>
									</dd>
									<dd>
										<a href="${contextPath}/p/myorder?state_type=3" class="flk-03">待收货订单<span style="color:#e5004f;">(${orderCountMap.consmOrderCount})</span></a>
									</dd>
									<dd>
										<a href="${contextPath}/p/prodcomment" class="flk-03">待评价商品<span style="color:#e5004f;">(${orderCountMap.unCommCount})</span></a>
									</dd>
								</dl>
								<dl>

									<dt>消息精灵：</dt>
									<dd>
										<a href="${contextPath}/p/inbox" class="flk-03">提醒/通知：<span style="color:#e5004f;" class="msgCount">0</span></a>
									</dd>
								</dl>
							</div>
							<div class="ainfo">
								<dl class="fore">

									<dt>账户余额：</dt>
									<dd>
										<a class="flk-04" href="${contextPath}/p/predeposit/account_balance"><span id=" ">￥${predepositAmount}</span>
										</a>
									</dd>
								</dl>
								<dl>
									<dt>我的积分：</dt>
									<dd>
										<a class="flk-04" href="${contextPath}/p/integral"><span id=" " class="ftx-04">${intergalAmount}</span>分</a>
									</dd>
								</dl>
								<dl>
									<dt>优惠券：</dt>
									<dd class="fore1">
										<a href="${contextPath}/p/coupon?couPro=shop" id=" " class="flk-04"><span class="ftx-04" id="CouponCount">${unuseCouponCount}</span>张</a>
									</dd>
								</dl>
								<dl>
									<dt>红包：</dt>
									<dd class="fore1">
										<a href="${contextPath}/p/coupon?couPro=platform" id=" " class="flk-04"><span class="ftx-04" id="redPacketCount">${redPacketCount}</span>张</a>
									</dd>
								</dl>
							</div>
						</div>
					</div>
				</div>
				<!--user info end-->


				<!-------------订单---------------->
				<div id="recommend" class="m10 recommend" style="display: block;">
					<div class="pagetab2">
						<ul>
							<li class="on"><span style="font-size: 13px;">最近订单</span>
							</li>
						</ul>
					</div>

					<table width="100%" style="margin-top:15px;" cellspacing="0" cellpadding="0" class="buytable seller-table sold-table">
						<tbody>
							<tr>
								<th width="25%">订单编号</th>
								<th width="25%">订单商品</th>
								<th>订单金额</th>
								<th width="12%">下单时间</th>
								<th width="12%">订单状态</th>
								<th width="12%" style="border-right: 1px solid #e4e4e4;">操作</th>
							</tr>
							 <c:choose>
					           <c:when test="${empty requestScope.list}">
					              <tr>
								 	 <td colspan="20">
								 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
								 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
								 	 </td>
								 </tr>
					           </c:when>
					           <c:otherwise>
					           		<c:forEach items="${requestScope.list}" var="order" varStatus="status">
										<tr>
											<td>
												<c:choose>
													<c:when test="${order.subType eq 'PRE_SELL'}">
														<a href='${contextPath}/p/presellOrderDetail/${order.subNumber}' target="_blank"  title="查看订单详情">${order.subNumber}</a>
													</c:when>
													<c:otherwise>
														<a href='${contextPath}/p/orderDetail/${order.subNumber}' target="_blank"  title="查看订单详情">${order.subNumber}</a>
													</c:otherwise>
												</c:choose>
											</td>
											<td>
												<c:forTokens items="${order.prodName}" delims="," var="name">  
											       <c:out value="${name}"/><p>  
											    </c:forTokens>
											</td>
											<td>
												<fmt:formatNumber type="currency" value="${order.actualTotal}" pattern="${CURRENCY_PATTERN}" /><br>${order.payTypeName}
											</td>
											<td><span class="ftx-03"> 
											    <fmt:formatDate value="${ order.subDate}" type="date" /><br /> 
												<fmt:formatDate value="${ order.subDate}" type="time" /> </span>
											</td>
											<td><span class="ftx-03">
											    <c:choose>
												<c:when test="${order.refundState ==1 }">
													<span class="col-orange">退货/退款中</span>
												</c:when>
												 <c:when test="${order.status==1 }">
												     <span class="col-orange">等待买家付款</span> 
												 </c:when>
												 <c:when test="${order.status==2 }">
												       <c:choose>
												       	  	<c:when test="${order.mergeGroupStatus eq 0 && order.subType eq 'MERGE_GROUP'}">
												       	  		<span class="col-orange">拼团中</span>
												       	  	</c:when>
															<c:when test="${order.subType eq 'GROUP' && order.groupStatus eq 0}">
																<span class="col-orange">团购中</span>
															</c:when>
															<c:otherwise><span class="col-orange">待发货</span></c:otherwise>
														</c:choose>
												 </c:when>
												 <c:when test="${order.status==3 }">
												       <span class="col-orange">待收货</span>
												 </c:when>
												 <c:when test="${order.status==4 }">
												      已完成
												 </c:when>
												 <c:when test="${order.status==5 || order.status==-1 }">
												      交易关闭
												 </c:when>
										  		</c:choose>
											</span>
											</td>
											<td class="order-doi">
											<c:choose>
												<c:when test="${order.status==1 }">
													<c:if test="${order.subType eq 'PRE_SELL'}">
										        		<a href='${contextPath}/p/presellOrderDetail/${order.subNumber}' target="_blank" title="查看订单详情" class="btn-r">订单详情</a> 
													</c:if>
													<c:if test="${order.subType ne 'PRE_SELL'}">
										        		<a href='${contextPath}/p/orderDetail/${order.subNumber}' target="_blank" title="查看订单详情" class="btn-r">订单详情</a> 
										        		<a target="_blank"  href="<ls:url address="/p/orderSuccess?subNums=${order.subNumber}" />" title="订单支付"  class="a-block">订单支付</a>
													</c:if>
												 </c:when>
												 <c:when test="${order.status==2 }">
												       <c:choose>
												       	  	<c:when test="${order.mergeGroupStatus eq 0 && order.subType eq 'MERGE_GROUP'}">拼团中</c:when>
															<c:when test="${order.subType eq 'GROUP' && order.groupStatus eq 0}">团购中</c:when>
															<%-- <c:otherwise>待发货</c:otherwise> --%>
															<c:otherwise>
																<a class="btn-r" href="javascript:void(0);" onclick="SendSiteMsg('${order.shopId}','${order.subNumber}');">
																<i class="icon-ban-circle"></i>提醒发货</a>
															</c:otherwise>
														</c:choose>
												 </c:when>
												 <c:when test="${order.status==3 }">
												     <a href='${contextPath}/p/orderDetail/${order.subNumber}' target="_blank" title="查看订单详情" class="btn-r">订单详情</a> 
												     <a target="_blank"  href="<ls:url address="/p/orderExpress/${order.subNumber}" />"
												   title="查看物流" class="a-block">查看物流</a>
												 </c:when>
												 <c:when test="${order.status==4 }">
												      <a href='<ls:url address="/p/orderDetail/${order.subNumber}" />' target="_blank" title="查看订单详情" class="btn-r">订单详情</a> 
												      <a target="_blank"  href="<ls:url address="/p/orderExpress/${order.subNumber}" />" title="查看物流" class="a-block">查看物流</a>
												      <a target="_blank"  href="<ls:url address="/p/prodcomment" />" title="评价" class="a-block">评价</a>
												 </c:when>
											  </c:choose>
											</td>
										</tr>
										</c:forEach>
					           </c:otherwise>
					         </c:choose>
						</tbody>
					</table>
				</div>
				
				<!-------------订单end---------------->



				<div id="recommend" class="m10 recommend" style="display: block;">
					<div class="pagetab2">
						<ul>
							<li class="on"><span>最近收藏的商品</span>
							</li>
						</ul>
					</div>

					<div id="group" class="tabcon">

						<div class="suits" style="width:100%;">
							<ul class="list-h">
								<c:set var="favorProds"  value="${prodApi:getRencFavorProd(pageContext.request,5)}"></c:set>
								 <c:choose>
						           <c:when test="${empty favorProds}">
						           		 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
								 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
									 </tr>
						           </c:when>
						           <c:otherwise>
						           		<c:forEach items="${favorProds}" var="favorProd">
											<li class="gl-item">
											<div class="gl-i-wrap j-sku-item">
												<div class="p-img">
													<a style="position: relative;" target="_blank" href="${contextPath}/views/${favorProd.prodId}">
														<span style="height: 100%;vertical-align: middle;display: inline-block;margin-left: -4px;"></span>
														<img style="max-width:180px;max-height:180px;"
														src="<ls:images item='${favorProd.pic}' scale='1' />"> 
														</a>
													<div class="picon pi8">
														<b></b>
													</div>
												</div>
												<div class="p-price">
													<strong class="J_price">￥${favorProd.cash}</strong>
												</div>
												<div class="p-name">
													<a target="_blank" href="${contextPath}/views/${favorProd.prodId}">${favorProd.name}</a>
												</div>
												<div class="listbuy_btn">
													<a href="${contextPath}/views/${favorProd.prodId}" class="graybtn_list">去看看</a>
												</div>
											</div></li>
										</c:forEach>
						           </c:otherwise>
						          </c:choose>
							</ul>
						</div>
						
						<div class="clear"></div>
					</div>
					<!--group end-->
				</div>
				
			</div>
			<!-----------right_con end-------------->


			<div class="clear"></div>
		</div>
		<!----两栏end---->

		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/alternative.js'/>"></script>
	<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/randomimage.js'/>"></script>
	<script type="text/javascript">
		userCenter.changeSubTab("usercenterhomeWrap");
		$("#uploadthickbox").bind("click",function(){
			layer.open({
				title :"头像编辑",
				  type: 2, 
				  id:"touxiang",
				  content: contextPath+'/p/loadphotoverlay',
				  area: ['520px', '490px']
				}); 
			});
		//提醒发货
		function SendSiteMsg(shopId,subNum){
			$.ajax({
				url : contextPath+"/p/sendShipMsg",
				type : 'post',
				data : {"shopId":shopId,"subNumber":subNum},
				success : function(result){
					layer.msg(result,{icon: 1,time:1000},function(){
						location.reload()
					});
				}
			});
		}
	</script>
</body>
</html>