<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
<link rel="stylesheet" href="<ls:templateResource item='/resources/common/css/jquery.autocomplete.css'/>">
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/plugins/showLoading/css/showLoading.css'/>" />
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<!--顶部menu-->
 <div id="hd">
   
      <!--hdtop 开始-->
       <%@ include file="home/header.jsp" %>
      <!--hdtop 结束-->
       <!-- nav 开始-->
        <div id="hdmain_cart">
          <div class="yt-wrap">
            <h1 class="ilogo">
                <a href="${contextPath}/">LegendShop</a>
            </h1>
            
            <!--page tit-->
              <div class="cartnew-title">
                       <ul class="step_h">
                           <li class=""><span>购物车</span></li>
                           <li class=""><span>填写核对订单信息</span></li>
                           <li class=" "><span>提交订单</span></li>
                       </ul>
              </div>
              
            <!--page tit end-->
          </div>
        </div>
      <!-- nav 结束-->
 </div><!--hd end-->
<!--顶部menu end-->

