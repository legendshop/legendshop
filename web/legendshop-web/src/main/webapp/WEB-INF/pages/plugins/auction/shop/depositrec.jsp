<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
  <table  style="width: 100%" class="${tableclass}" id="col1">
         	<tr align="center">
         		<td colspan="4"><b>竞拍人记录</b></td>
         	</tr>
         	<tr>
         		<td>订单号:</td>
         		<td>
         			${auctionDepositRec.subNumber}
				</td>
         	</tr>
         	<tr>
         		<td>时间:</td>
         		<td><fmt:formatDate value="${auctionDepositRec.payTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
         	</tr>
         	<tr>
         		<td>状态:</td>
         		<td>
					<c:choose>
         				<c:when test="${auctionDepositRec.orderStatus eq 0}">未支付</c:when>
         				<c:when test="${auctionDepositRec.orderStatus eq 1}">已支付</c:when>
         			</c:choose>
				</td>
         	</tr>
         	<tr>
         		<td>保证金:</td>
         		<td>
         			${auctionDepositRec.payMoney}
				</td>
         	</tr>
         	<tr>
         		<td>状态:</td>
         		<td>
					<c:choose>
         				<c:when test="${auctionDepositRec.flagStatus eq 0}">未处理</c:when>
         			</c:choose>
				</td>
         	</tr>
 </table>
