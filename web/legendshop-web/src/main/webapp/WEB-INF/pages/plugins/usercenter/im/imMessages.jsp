<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/dialog.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>客服消息 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/im/imMessage.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
        <!----两栏---->
		<div class=" yt-wrap" style="padding-top:10px;"> 
		    <%@include file='../usercenterLeft.jsp'%>
		    <!-----------right_con-------------->
		    <div class="right_con">
				<div class="o-mt"><h2>客服消息</h2></div>
			    <div class="pagetab2">
		            <ul>           
		               	<li id="inbox" onclick="inbox();"><span>收件箱(${unReadInboxMsgNum})</span></li>
		               	<li id="systemMessages" onclick="systemMessages();"><span>系统通知(${unReadSystemMsgNum})</span></li>                
		               	<li class="on" id="imMessages" ><span>客服消息(${unReadCustomerMessage})</span></li>                
		            </ul>       
			    </div>
			    <div id="mailInfo">
			   		<div class="offline-message">
			    		<c:forEach items="${dialogues}" var="dialogue">
				        	<div class="item mes-item" customId="${dialogue.customId}" shopId="${dialogue.shopId}" consultType="${dialogue.consultType}" skuId="${dialogue.skuDetailDto.skuId}" refundSn="${dialogue.subRefundReturn.refundSn}">
					            <span class="img">
					            	<c:choose>
					            		<c:when test="${not empty dialogue.shopPic}">
					            			<c:choose>
						            			<c:when test="${dialogue.shopId eq 0 }">
						            				<img src='${contextPath}/resources/templets/images/im/head-default.jpg'/>
						            			</c:when>
						            			<c:otherwise>
						            				<img src="<ls:photo item="${dialogue.shopPic}"/>" alt="">
						            			</c:otherwise>
					            			</c:choose>
					            			<c:choose>
						            			<c:when test="${not empty dialogue.offLineCount && dialogue.offLineCount > 0 && dialogue.offLineCount <= 99}">
						            				<i class="sub">${dialogue.offLineCount}</i>
						            			</c:when>
						            			<c:when test="${not empty dialogue.offLineCount && dialogue.offLineCount > 99}">
						            				<i class="sub">99+</i>
						            			</c:when>
					            			</c:choose>
					            		</c:when>
					            		<c:otherwise>
					            			<img src='${contextPath}/resources/templets/images/im/head-default.jpg'/>
					            			<c:choose>
						            			<c:when test="${not empty dialogue.offLineCount && dialogue.offLineCount > 0 && dialogue.offLineCount <= 99}">
						            				<i class="sub">${dialogue.offLineCount}</i>
						            			</c:when>
						            			<c:when test="${not empty dialogue.offLineCount && dialogue.offLineCount > 99}">
						            				<i class="sub">99+</i>
						            			</c:when>
					            			</c:choose>
					            		</c:otherwise>
					            	</c:choose>
					            	<c:choose>
				            			<c:when test="${dialogue.shopId eq 0 }">
				            				<em class="mes-pingtai-tag">平台</em>
				            			</c:when>
				            			<c:otherwise>
				            				<em class="mes-shop-tag">商家</em>
				            			</c:otherwise>
			            			</c:choose>
					            </span>
				            	<dl class="item-dl">
					            	<dt class="name">
						             	<c:choose>
					            			<c:when test="${dialogue.shopId eq 0 }">
					            				平台客服
					            			</c:when>
					            			<c:otherwise>
					            				${dialogue.shopName}_${dialogue.name}
					            			</c:otherwise>
				            			</c:choose>
					            	</dt>
					            	<dd class="txt"></dd>
<%--					            	<dd class="txt">${dialogue.offLineContent}</dd>--%>
					            </dl>
				        	</div>
			    		</c:forEach>
			        </div>
				 </div>
			</div>
		    <div class="clear"></div>
		</div>
		<!----两栏end---->
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/im/imMessages.js'/>"></script>
	<script type="text/javascript">	
		var contextPath = '${contextPath}';
	</script>        
</body>
</html>