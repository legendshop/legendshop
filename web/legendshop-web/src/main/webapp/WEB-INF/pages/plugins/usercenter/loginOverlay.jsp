<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
 		//used by reg.js
		var contextPath = '${contextPath}';
</script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/reg.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/loginOverlay.js'/>"></script>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/loginOverlay.css'/>" rel="stylesheet"/>
</head>
<body>
<div id="regist">
    <div class="mt">
        <ul class="tab">
            <li class="curr" id="login"><a onclick="login();" id="login" href="#">登&nbsp;录</a></li>
            <li><a href="#"  id="register" onclick="register();">注&nbsp;册</a></li>
        </ul>
    </div>

        <div class="mc form" id="loginOverlayDiv"></div>
</div>

</body>
</html>

<script type="text/javascript">
       	jQuery(document).ready(function($) {
       		login();
       			//键盘事件
		 $("#pwd").keydown(function(event){
		 if(event.keyCode==13){
			 $("#loginsubmitframe").click();
		 }
       });
       	});
</script>