<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/store/store.css'/>" rel="stylesheet"/>
<html>
<body>
      <div style="position: relative;" class="dialog_body">
        <div style="margin: 0px; padding: 0px;" class="dialog_content">
          <style>
            .eject_con .error { font-size: 14px; min-height: 27px; padding: 0px; }
          </style>
          <div class="eject_con">
                  <div class="content">
                    <div class="order-handle">
                      <div class="title">
                        <h3>提货验证</h3>
                        <c:if test="${storeOrder.dlyoSatus==0}">
                       		<div class="no-pay">该笔尚未付款，需支付<strong>${actualTotal}元</strong>,门店收款后再进行提货验证</div>
                        </c:if>
                      </div>
                      <label>
                        <input class="text w200 vm" maxlength="8" id="pickup_code" name="pickup_code" placeholder="请输入买家提供的验证码" autocomplete="off" type="text">
                        <span></span>
                        <input class="btn" value="保存" onclick="validateCode()" type="button">
                      </label>
                      <p>该验证码为商城订单生成时，自动发送给收货人手机及买家订单详情中的提供的“6位验证码”。</p>
                    </div>
                    <div class="order-info">
                      <ul class="tabs-nav ">
                        <li class="tabs-selected"><a href="javascript:void(0);">收货人信息</a></li>
                      </ul>
                      <div class="tabs-panel">
                        <dl>
                          <dt>买家姓名：</dt>
                          <dd>${storeOrder.reciverName}</dd>
                        </dl>
                        <dl>
                          <dt>联系电话：</dt>
                          <dd>${storeOrder.reciverMobile}</dd>
                        </dl>
                        <!-- <dl>
                          <dt>自提店地址：</dt>
                          <dd>天津 天津市 红桥区 红桥湘潭路分店(天津市红桥区湘潭路22号)</dd>
                        </dl> -->
                      </div>
                    </div>
                </div>
          </div>
        </div>
      </div>
      <div style="clear:both; display:block;"></div>
</body>
<script type="text/javascript">
    var contextPath="${contextPath}";
	function loginOut(){
		layer.confirm("是否退出?", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   window.location.href=contextPath+"/m/logout";
		   });
	}
	
	function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("from1").submit();
	}
	
		function validateCode(){
			var code=$("#pickup_code").val();
			var subNumber='${storeOrder.orderSn}';
			if(isBlank(code)){
				layer.msg("提货码不能为空",{icon: 0});
				return false;
			}
			if(isBlank(subNumber)){
				layter.msg("订单异常",{icon: 2});
				return false;
			}
			$.ajax({
					url: contextPath+"/m/store/validateCode", 
					data: {"pickup_code":code,"subNumber":subNumber},
					type:'post', 
					async : true, //默认为true 异步   
					dataType:'json',
					success:function(data){
						if(data=="OK"){
							layer.msg("提货成功",{icon: 1},function(){
								parent.location.reload()
							});
						}else {
							layer.msg(data,{icon: 2});
						}
					}   
				});
		}
		
		function isBlank(_value){
		   if(_value==null || _value=="" || _value==undefined){
		     return true;
		   }
		   return false;
	}
</script>
</html>