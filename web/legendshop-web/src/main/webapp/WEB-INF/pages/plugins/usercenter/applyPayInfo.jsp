<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<script type="text/javascript"  src="<ls:templateResource item='/resources/templets/js/loadApplyPayInfo.js'/>"></script>

<div style="background: #fff none repeat scroll 0 0;
    border: 1px solid #dfdfdf;margin-top: 15px;padding-bottom: 20px;">
    
    <div class="o-mb">
		<h3 style="margin-top: 30px;margin-left: 10px;">
			<a onclick="alertApplyPayAddressDiag(0)" class="e-btn add-btn yellow-btn" href="javascript:void(0)">新增退款信息</a>
	    </h3>
</div>
    
		<c:forEach items="${prodRepayUserifos}" var="info" varStatus="orderstatues">
		     <div class="banks">
				<ul>
					<li><input type="radio" name="userInfo" value="${info.repayUserifoId}"></li>
					<li style="width: 70px;">${info.bankUserName}</li>
					<li style="width: 100px;">${info.bankCode}</li>
					<li style="width: 160px;">${info.bankAccount}</li>
					<li style="width: 100px;">
					   <a href="javascript:void(0);" onclick="alertApplyPayAddressDiag(${info.repayUserifoId});">修改信息</a> |
                      <a href="javascript:void(0);" class="del" onclick="delApplyPayInfo(${info.repayUserifoId});">删除</a>
					</li>
				</ul>
			</div>
			<div style="clear: both;">
		  </c:forEach>
  </div>
		
	<div style="clear: both;">
</div>
</div>
