<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<style type="text/css">
		html{font-size:13px;}
		.skulist{border:solid #d5e4fa; border-width:1px 0px 0px 1px;width: 100%;}
		.skulist tr td{border:solid #d5e4fa; border-width:0px 1px 1px 0px; padding:10px 0px;text-align: center;font-size:13px;}
	</style>
	<table class="skulist">
		<tr>
			<td colspan="5"><b>商品信息</b></td>
		</tr>
		<tr>
			<td>sku名称</td>
			<td>属性</td>
			<td>sku价格</td>
			<td>sku库存数量</td>
			<td>操作</td>
		</tr>
		<c:if test="${empty skuList}">
			<tr>
				<td colspan="5" align="center">暂无可用sku商品</td>
			</tr>
		</c:if>
		<c:if test="${not empty skuList}">
			<c:forEach items="${skuList}" var="sku" varStatus="status">
				<tr>
					<td>${sku.name}</td>
					<td>${sku.property}</td>
					<td>${sku.price}</td>
					<td>${sku.stocks}</td>
					<td>
						<c:choose>
							<c:when test="${ sku.stocks le 0}">
								<font color="red"><b>库存不足</b></font>
							</c:when>
							<c:otherwise>
								<input type="button" value="选择" onclick="parent.addProd('${sku.prodId}','${sku.skuId}','${sku.name}','${sku.price}','${sku.cnProperties}');"/>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</c:forEach>
		</c:if>
	</table>
