<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>限时促销活动-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/active/actives${_style_}.css'/>" rel="stylesheet">
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/active/promotion.css'/>"
		  rel="stylesheet">
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/prodSelectAlert.css'/>" rel="stylesheet"/>

</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">限时促销活动</span>
				</p>
			</div>
			
			<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
			  
			 <div id="rightContent" class="right_con">
				 <div class="ncm-default" style="margin-top: 20px;">
					 <form:form action="#" id="add_form" method="post">
						 <div class="promotion-right">
							 <!-- 基本信息 -->
							 <div class="basic-ifo">
								 <!-- 基本信息头部 -->
								 <div class="ifo-top">
									 基本信息
								 </div>
								 <div class="ifo-con">
									 <li style="margin-bottom: 5px;">
                                    <span class="input-title" style="display: inline-block;width: 83px"><span
											style="color: #E5004F;">*</span>活动名称:</span>
										 <input type="hidden" class="id" name="id" value="${marketing.id}"/>
										 <input type="text" class="active-name" maxlength="25" name="marketName"
												id="marketName" disabled value="${marketing.marketName}">
										 <span class="error-message"></span>
									 </li>
									 <li>
										 <span class="input-title"></span>
										 <span style="color: #999999;margin-top: 10px;">活动名称最多为25个字符</span>
									 <li>
									 <li style="margin-bottom: 0;">
                                    <span class="input-title" style="display: inline-block;width: 83px"><span
											style="color: #E5004F;">*</span>活动时间:</span>
										 <input type="text" class="active-time"
												readonly="readonly" placeholder="开始时间" disabled value="${marketing.startTime}">
										 <span class="error-message"></span>
										 <span style="margin: 0 5px;">-</span>
										 <input type="text" class="active-time"
												readonly="readonly" placeholder="结束时间" disabled value="${marketing.endTime}">
										 <span class="error-message"></span>
										 <p class="hint"></p>
									 </li>
								 </div>
							 </div>
							 <!-- 选择活动商品 -->
							 <div class="basic-ifo" style="margin-top: 20px;">
								 <!-- 选择活动商品头部 -->
								 <div class="ifo-top">
									 选择活动商品
								 </div>
								 <div style="margin:15px 60px">
									 <dl>
										 <dt style="display: inline-block"><span style="color: #E5004F;">*</span>选择商品：</dt>
										 <dd style="display: inline-block">

											 <label  class="radio-wrapper">
									<span class="radio-item">
									<input type="radio" id="goods_status_1" value="1" disabled name="isAllProds"  <c:if test="${empty marketing.isAllProds || marketing.isAllProds eq 1}">checked</c:if>
										   class="radio-input"/>
									<span class="radio-inner"></span>
									</span>
												 <span class="radio-txt">全部商品</span>
											 </label>
											 <label class="radio-wrapper">
									<span class="radio-item">
										<input type="radio" id="goods_status_0" value="0" disabled name="isAllProds" <c:if test="${marketing.isAllProds eq 0}">checked</c:if>
											   class="radio-input"/><%----%>
										<span class="radio-inner"></span>
									</span>
												 <span class="radio-txt">部分参与商品</span>
											 </label>
										 </dd>
									 </dl>
								 </div>
								 <div class="ifo-con" id="prods" style="padding:0px 60px 30px 60px;">
									 <div class="tab-shop">
										 <table border="0" id="skuList" cellpadding="0" cellspacing="0" style="margin: 30px 0;">
											 <tbody>
											 <tr style="background: #f9f9f9;">
												 <td width="70px">商品图片</td>
												 <td width="170px">商品名称</td>
												 <td width="100px">商品规格</td>
												 <td width="100px">商品价格</td>
												 <td width="100px">可销售库存</td>
											 </tr>
											 <c:choose>
												 <c:when test="${not empty marketing.marketingProds && marketing.marketingProds.size()>0}">
													 <c:forEach items="${marketing.marketingProds}" var="marketingProdDto">
														 <tr class="sku" >
															 <input type="hidden" class="skuId" name="skuIds" value="${marketingProdDto.skuId}"/>
															 <td>
																 <img src="<ls:images item='${marketingProdDto.pic}' scale='3' />" >
															 </td>
															 <td>
																 <span class="name-text">${marketingProdDto.prodName}</span>
															 </td>
															 <td>
																 <span class="name-text">${marketingProdDto.cnProperties}</span>
															 </td>
															 <td>
																 <span class="name-text">${marketingProdDto.cash}</span>
															 </td>
															 <td>${marketingProdDto.stock}</td>
														 </tr>
													 </c:forEach>
												 </c:when>
												 <c:otherwise>
													 <tr class="first">
														 <td colspan="5" >暂未选择商品</td>
													 </tr>
												 </c:otherwise>
											 </c:choose>
											 <tr class="noSku" style="display: none"><td colspan='5'>暂未选择商品</td></tr>
											 </tbody>
										 </table>
									 </div>
								 </div>
							 </div>
							 <!-- 活动规则 -->
							 <div class="basic-ifo" style="margin-top: 20px;">
								 <!-- 活动规则头部 -->
								 <div class="ifo-top">
									 活动规则
								 </div>
								 <div class="ifo-con">
									 <div style="margin:-6px 0px 40px 0px">
										 <dl>
											 <dt style="display: inline-block;vertical-align: middle"><span style="color: #E5004F;">*</span>商品折扣：</dt>
											 <dd style="display: inline-block;vertical-align: middle;margin-top: 25px" id="zk_rule" >
												 <input type="text" class="text" style="width:121px;" id="manze_discount" disabled value="${marketing.discount}" name="discount"><em class="add-on">折</em>
												 <span class="error-message"></span>
												 <p class="hint">（价格低于0.1元时，商品不打折）</p>
											 </dd>
										 </dl>
									 </div>
									 <div style="margin-top: 30px">
										 <dl>
											 <dt style="margin-left: 24px">备注：</dt>
											 <dd style="display: inline-block; margin:-16px 0px 0px 80px">
                                            <textarea disabled class="textarea" style="width:478px;" maxlength="100" id="remark"
													  rows="10" name="remark" value="${marketing.remark}">${marketing.remark}</textarea>
												 <p class="hint">活动备注最多为100个字符</p>
											 </dd>
										 </dl>
									 </div>
								 </div>
							 </div>
							 <div class="bto-btn">
								 <dl class="bottom">
									 <dt>&nbsp;</dt>
									 <dd>
										 <label class="submit-border">
											 <a class="red-btn" href="/s/shopZkMarketing"
												style="background: #F9F9F9;color: #000000;border:1px solid rgba(221,221,221,1);">返回列表</a>
										 </label>
									 </dd>
								 </dl>
							 </div>
						 </div>
					 </form:form>
				 </div>

				<div class="m m1" style="margin: 65px;">
			   			<div class="mt">
			   				<h3>温馨提示：</h3>
			   			</div>
						<div class="mc">
							<p>
							<br>
								1. 假设商品参与限时促销活动后，商品价格小于0.01，则该促销会失效，商品不参与该限时折扣！
								<br>
								2. 选择商品分为全场商品和部分商品[部分商品需要在列表进行管理添加商品信息]
								<br>
								3. 选择全场商品,需要设定全部商品的折扣价格
							</p>
						</div>
			   		</div>
				<div class="clear"></div>
			</div>

		</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
</body>
 <script src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>" type="text/javascript"></script>
 <script src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
 <script src="<ls:templateResource item='/resources/templets/js/shopAddZKMarketing.js'/>"></script>
<script type="text/javascript">
    var contextPath = '${contextPath}';
    $(function(){
		$("#goods_status_0").click(function(){
			$("#goods_status_0").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper radio-wrapper-checked");
			$("#goods_status_1").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper");
		});
		$("#goods_status_1").click(function(){
			$("#goods_status_1").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper radio-wrapper-checked");
			$("#goods_status_0").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper");
		});
	});
</script>  

</html>