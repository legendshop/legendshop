<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/prodSelectAlert.css'/>" rel="stylesheet"/>
	<table border="0" id="skuList" cellpadding="0" cellspacing="0" style="margin: 30px 0;">
		<tbody>
		<tr style="background: #f9f9f9;">
			<td width="70px">商品图片</td>
			<td width="170px">商品名称</td>
			<td width="100px">商品规格</td>
			<td width="100px">商品价格</td>
			<td width="100px">可销售库存</td>
			<td width="100px">操作</td>
		</tr>
		<c:choose>
			<c:when test="${not empty skuList}">
			<c:forEach items="${skuList}" var="sku">
				<tr class="sku" >
                    <input type="hidden" class="skuId" name="skuIds" value="${sku.skuId}"/>
                    <td>
						<img src="<ls:images item='${sku.pic}' scale='3' />" >
					</td>
					<td>
						<span class="name-text">${sku.name}</span>
					</td>
					<td>
						<span class="name-text">${sku.cnProperties}</span>
					</td>
					<td>
						<span class="name-text">${sku.price}</span>
					</td>
					<td>${sku.stocks}</td>
					<td><a href="#" class="removeSku">移除</a></td>
				</tr>
			</c:forEach>
            </c:when>
            <c:otherwise>
                <tr class="first">
					<td colspan="6" >暂未选择商品</td>
				</tr>
            </c:otherwise>
        </c:choose>
                  <tr class="noSku" style="display: none"><td colspan='6'>暂未选择商品</td></tr>
        </tbody>
	</table>