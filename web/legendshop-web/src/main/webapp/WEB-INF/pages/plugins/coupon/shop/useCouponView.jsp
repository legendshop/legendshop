<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>礼券使用情况-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/coupons.css'/>" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">礼券使用情况</span>
				</p>
			</div>
			<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
			
			
			<div class="seller-selling">
				<div class="selling-sea">
					<form:form  action="${contextPath}/s/coupon/usecoupon_view/${couponId}" method="post" id="form1">
					    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
						<span class="ser-nick">
							用户名:<input type="text" name="userName" maxlength="50" value="${userName}" >
							<a class="but-serch" href="javascript:void(0);" onclick='window.location="<ls:url address='/s/coupon/query'/>"'  >返回</a>
							<a class="but-batch" href="javascript:seacher();">搜索</a>
						</span>
					</form:form>
				</div>
				
				
				<table class="selling-table">
					<tbody>
					<tr class="selling-tit">
						<th width="100">券号</th>
						<th width="150">领取用户</th>
						<th width="100">领取时间</th>
						<th width="100">使用时间</th>
						<th width="100">抵扣订单号</th>
						<th width="80">订单总金额</th>
						<th width="80">用户使用状态</th>
						<th width="80">是否绑定</th>
					</tr>				
					<c:forEach items="${requestScope.list}" var="item" varStatus="status">
				            <tr>
				               <td >${item.couponSn}</td>
				               <td height="30"><c:choose>
							  		<c:when test="${empty item.userName || item.userName==''}">
							  		     未领取
							  		</c:when>
							  		<c:otherwise>
							  		 ${item.userName}
							  		</c:otherwise>
							  	</c:choose></td>
				               <td > <c:choose>
							  		<c:when test="${empty item.getTime || item.getTime==''}">
							  		     未领取
							  		</c:when>
							  		<c:otherwise>
							  		  <fmt:formatDate value="${item.getTime}" dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" />
							  		</c:otherwise>
							  	</c:choose></td>
				               <td > <c:choose>
							  		<c:when test="${empty item.useTime || item.useTime==''}">
							  		     未使用
							  		</c:when>
							  		<c:otherwise>
							  		   <fmt:formatDate value="${item.useTime}" dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" />
							  		</c:otherwise>
							  	</c:choose></td>
				               <td ><c:choose>
							  		<c:when test="${empty item.orderNumber}">
							  		     无
							  		</c:when>
							  		<c:otherwise>
							  		   ${item.orderNumber}
							  		</c:otherwise>
							  	</c:choose></td>
				               <td ><c:choose>
							  		<c:when test="${empty item.orderPrice}">
							  		   无
							  		</c:when>
							  		<c:otherwise>
							  		   ${item.orderPrice}
							  		</c:otherwise>
							  	</c:choose></td>
				               <td ><c:choose>
							  		<c:when test="${item.useStatus == 1}">
							  		     未使用
							  		</c:when>
							  	     <c:when test="${item.useStatus==2}">
							  		     已使用
							  		</c:when>
							  		<c:otherwise>
							  		</c:otherwise>
							  	</c:choose></td>
				               <td > <c:choose>
							  		<c:when test="${empty item.userId}">
							  		   未绑定
							  		</c:when>
							  		<c:otherwise>
							  		    已绑定
							  		</c:otherwise>
							  	</c:choose></td>
				             </tr>
				         </c:forEach>
				</tbody>
			 </table>
			 
			 
			 <div class="clear"></div>
			  <div style="margin-top:10px;" class="page clearfix">
	     			 <div class="p-wrap">.
							<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	     			 </div>
		  		</div>
			 
			 
			</div>
		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
</body>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script src="<ls:templateResource item='/common/default/js/alternative.js'/>" type="text/javascript"></script>
<script type="text/javascript">
	
	userCenter.changeSubTab("couponManage");
	
	function pager(curPageNO){
	    document.getElementById("curPageNO").value=curPageNO;
	    document.getElementById("form1").submit();
	}
	
	function seacher(){
	   document.getElementById("form1").submit();
	}
</script>

</html>
