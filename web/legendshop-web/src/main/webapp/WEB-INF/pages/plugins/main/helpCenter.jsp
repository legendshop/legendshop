<!DOCTYPE html>
<html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<head>
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>帮助中心-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	 <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
     <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
</head>
<body  class="graybody">
	<div id="doc">
	
	       <div id="hd">
		      <%@ include file="home/top.jsp" %>
				<%-- <div id="hdmain_cart">
					<div class="yt-wrap">
						<h1 class="ilogo">
							<a href="${contextPath}/"><img src="<ls:photo item="${systemConfig.logo}"/>"  style="max-height:89px;" /></a>
						</h1>
		
						<!--page tit-->
						<span class="p_t">帮助中心</span>
						<!--page tit end-->
					</div>
				</div> --%>
			</div>
			<div id="bd">
         <div class="hc_sbox">
            <div class="hc_w"></div>
            <form action="${contextPath}/helpCenter/search" method="get" id="search" name="search">
	            <div class="hc_s">
	               <input type="text" name="word" id="word" value="<c:out value="${param.keyword}"></c:out>" placeholder="请输入您的问题"/>
	               <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/> 
	               <a href="javascript:void(0);" onclick="searchNews();">搜索</a>
	            </div>     
            </form>    
         </div>
         
         
		<div class="help-center-con"><div class="yt-wrap">
            <p>快捷入口</p>
           <dl>
               <dt>
                   <a href="<ls:url address='/p/myorder?uc=uc'/>"><img width="90" height="90" src="<ls:templateResource item='/resources/templets/images/hc_p1.png'/>"></a>
               </dt>
               <dd>
                   <a href="<ls:url address='/p/myorder?uc=uc'/>">订单查询</a>
               </dd>
           </dl>
           <dl>
               <dt>
                   <a href="<ls:url address='/p/myorder'/>"><img width="90" height="90" src="<ls:templateResource item='/resources/templets/images/hc_p2.png'/>"></a>
               </dt>
               <dd>
                   <a href="<ls:url address='/p/myorder'/>">申请退换货</a>
               </dd>
           </dl>
            <dl>
               <dt>
                   <a href="<ls:url address='/p/inbox'/>"><img width="90" height="90" src="<ls:templateResource item='/resources/templets/images/hc_p3.png'/>"></a>
               </dt>
               <dd>
                   <a href="<ls:url address='/p/inbox'/>">我的消息</a>
               </dd>
           </dl>            
           <dl>
               <dt>
                   <a href="<ls:url address='/p/predeposit/account_balance'/>"><img width="90" height="90" src="<ls:templateResource item='/resources/templets/images/hc_p4.png'/>"></a>
               </dt>
               <dd>
                   <a href="<ls:url address='/p/predeposit/account_balance'/>">余额查询</a>
               </dd>
           </dl>
           <dl>
               <dt>
                   <a href="<ls:url address='/p/myorder?uc=uc'/>"><img width="90" height="90" src="<ls:templateResource item='/resources/templets/images/hc_p5.png'/>"></a>
               </dt>
               <dd>
                   <a href="<ls:url address='/p/myorder?uc=uc'/>">取消订单</a>
               </dd>
           </dl>
           <dl>
               <dt>
                   <a href="<ls:url address='/p/security'/>"><img width="90" height="90" src="<ls:templateResource item='/resources/templets/images/hc_p6.png'/>"></a>
               </dt>
               <dd>
                   <a href="<ls:url address='/p/security'/>">账户安全</a>
               </dd>
           </dl>
         </div></div>
         
         
         <div class="yt-wrap">
          
              <%-- <div class="helpmenu">
                <ul>
                 <c:forEach items="${tagMap}" var="item">
                	 <li>${item.key}</li>
                 </c:forEach>
                </ul>              
              </div> --%>
              
              <%-- <div class="hc_c2 clearfix">
                 <div class="hc_c2r"><img width="670" height="250" src="<ls:templateResource item='/resources/templets/images/hc_32.jpg'/>"></div>
                 <div class="help-center-list">
	                 <c:forEach items="${tagMap}" var="item">
		                 <div class="list_hc hc_c2l" style="display: none">
		                   <ul>
		                   	<c:forEach items="${item.value}" var="news" end="15">
		                   		<li>
		                   			<img src="${contextPath}/resources/templets/images/help-indicate-blue.png" alt="">
									<a target="_blank" href="<ls:url address='/news/${news.newsId}'/>">${news.newsTitle}</a>
		                   		</li>
		                   	</c:forEach>
		                   </ul>
		                 </div>
		                 </c:forEach>
                 </div>
              </div> --%>
              
              
              <div class="hc_pbox clearfix">
		            <c:forEach items="${newsCatList}" var="item">
		                  <div class="hc_div">
		                     <h3>${item.key.value}</h3>
		                     <div class="list_hc">
		                       <ul>
		                        <c:forEach items="${item.value}" var="news" end="9">
		                        	<li style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">
										<a title="${news.newsTitle}" href="<ls:url address='/news/${news.newsId}'/>">${news.newsTitle}</a>
									</li>
		                        </c:forEach>                
		                       </ul>
		                     </div>
		                  </div>
		             </c:forEach>
              </div>
         </div>
        </div>
		<%@ include file="home/bottom.jsp" %>
	</div>
</body>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/helpCenter.js'/>"></script>
</html>
