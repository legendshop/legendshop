<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>申请提现详情 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/pdCashManager${_style_}.css'/>" rel="stylesheet" />
	 <style>
	 	.form-table tr:last-child > td{
	 		border-bottom:none !important;
	 	}
	 </style>
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
     <%@ include file="/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp" %>
    <!-----------right_con-------------->
     <div class="right_con">
<div>
	<div class="o-mt">
		<h2>账户信息</h2>
	</div>
	<div class="pagetab2">
		<ul>
				<li><span><a href="<ls:url address='/p/predeposit/account_balance'/>">账户余额</a> </span></li>
			<li ><span><a href="<ls:url address='/p/predeposit/recharge_detail'/>">充值明细</a> </span></li>
			<li ><span><a href="<ls:url address='/p/predeposit/balance_withdrawal'/>">余额提现</a> </span></li>
			<li class="on"><span>申请提现详情</span></li>
		</ul>
	</div>

	<table class="form-table" style="background:#fff;">
		<tbody>
			<tr class="bd-dotline">
				<td class="tit">提现单号:</td>
				<td>${withdrawCash.pdcSn}</td>
			</tr>
			<tr class="bd-dotline">
				<td class="tit">提款方式:</td>
				<td>${withdrawCash.bankName}</td>
			</tr>
			<tr class="bd-dotline">
				<td class="tit">提款账号:</td>
				<td>${withdrawCash.bankNo}</td>
			</tr>
				<tr class="bd-dotline">
				<td class="tit">开户人姓名:</td>
				<td>${withdrawCash.bankUser}</td>
			</tr>
			<tr class="bd-dotline">
				<td class="tit">提现金额:</td>
				<td>${withdrawCash.amount}</td>
			</tr>
			<tr class="bd-dotline">
				<td class="tit">申请时间:</td>
				<td><fmt:formatDate value="${withdrawCash.addTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
			</tr>
			<tr class="bd-dotline">
				<td class="tit">处理状态:</td>
				<td>
				  		<c:choose>
							<c:when test="${withdrawCash.paymentState==0}">
							   在处理过程中
							</c:when>
							<c:when test="${withdrawCash.paymentState==1}">
							 提现成功  
							</c:when>
						 </c:choose>
				</td>
			</tr>
			<tr class="bd-dotline">
				<td class="tit">&nbsp;</td>
				<td><input type="button" onclick="javascript:window.history.go(-1);" class="pinkbtn" value="返回">
				</td>
			</tr>
		</tbody>
	</table>
	<div class="clear"></div>
</div>
 
</div>
    <!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
<script type="text/javascript">
	var contextPath = '${contextPath}';
	$(document).ready(function() {
		userCenter.changeSubTab("mydeposit");
	});

	function cancelWithdrawals(_id) {
		layer.confirm("是否确认删除该提现申请?", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   $.ajax({
					type : "POST",
					//提交的网址
					url : contextPath + "/p/predeposit/withdrawalApplyCancel/"+ _id,
					//提交的数据
					async : false,
					//返回数据的格式
					datatype : "json",
					//成功返回之后调用的函数            
					success : function(date) {
						var result = eval(date);
						if ("NOT_USER" == result) {
							layer.alert("取消提现申请失败,没有找到该帐号",{icon:2});
						} else if ("NON_COMPLIANCE" == result) {
							layer.alert("取消提现申请失败,该申请正在操作过程中,请稍候重试！",{icon:2});
						} else if ("OK" == result) {
							layer.alert("取消申请提现成功",{icon:1},function(){
								window.location.reload(true);
							});
						} else {
							layer.alert(result,{icon:2});
						}
					},
					//调用出错执行的函数
					error : function() {
						layer.alert("取消提现失败，请稍后重试!",{icon:2});
					}
				});
		   });
	}
</script>  
</body>
</html>
			
