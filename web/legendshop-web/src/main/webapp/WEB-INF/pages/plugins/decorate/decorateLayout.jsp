<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<c:choose>
  <c:when test="${shopLayout.layoutType=='layout0'}">
	    <div class="layout_one" location_mark="${shopLayout.layoutId}" location="true">
		          <div style="text-align:center" layout="layout0" id="content" mark="${shopLayout.layoutId}" url="" >
		                  <h3 class="module_wide">
		                    <div class="main">通栏布局上</div>
		                  </h3> 
		          </div>  
		          
		          <div class="f_edit_box" option="${shopLayout.layoutId}">
		               <a href="javascript:void(0);" seq="${shopLayout.seq}" onclick="decorate_module(this);" layout="layout0"  mark="${shopLayout.layoutId}" moduleType=""  class="f_edit"><i></i>编辑</a> 
		               <a layout="layout0" mark="${shopLayout.layoutId}" moduleType="" onclick="dele_layout(this)" class="f_del"><i></i>删除</a>
		          </div>
	  </div>
  </c:when>
   <c:when test="${shopLayout.layoutType=='layout1'}">
         <div class="layout_one" location_mark="${shopLayout.layoutId}" location="true">
		          <div style="text-align:center" layout="layout1" id="content" mark="${shopLayout.layoutId}" url="" >
		                  <h3 class="module_wide">
		                    <div class="main">通栏布局下</div>
		                  </h3> 
		          </div>  
		          <div class="f_edit_box" option="${shopLayout.layoutId}">
		               <a href="javascript:void(0);" seq="${shopLayout.seq}"  layout="layout1" onclick="decorate_module(this);"  mark="${shopLayout.layoutId}" moduleType="" class="f_edit"><i></i>编辑</a> 
		               <a layout="layout1" mark="${shopLayout.layoutId}" moduleType="" onclick="dele_layout(this)" class="f_del"><i></i>删除</a>
		          </div>
		          
		    </div>
   </c:when>
    <c:when test="${shopLayout.layoutType=='layout2'}">
         <div location_mark="${shopLayout.layoutId}"  location="true" option="${shopLayout.layoutId}" class="layout_two">
							<div mark="${shopLayout.layoutId}" layout="layout2" type="${shopLayout.layoutModuleType}"  id="content">
								<h3 class="module_big">居中布局</h3>
							</div>
							<a href="javascript:void(0);"  seq="${shopLayout.seq}" onclick="decorate_module(this);" layout="layout2"  mark="${shopLayout.layoutId}" moduleType="${shopLayout.layoutModuleType}" class="f_edit"><i></i>编辑</a>
							<a layout="layout2" mark="${shopLayout.layoutId}" moduleType="${shopLayout.layoutModuleType}" onclick="dele_layout(this)" href="javascript:void(0);" class="f_del"><i></i>删除</a>
		   </div>
    </c:when>
     <c:when test="${shopLayout.layoutType=='layout3'}">
              <div location_mark="${shopLayout.layoutId}" location="true" class="layout_three">
						       <c:forEach items="${shopLayout.shopLayoutDivDtos}" var="layout_div" varStatus="d">
						         <c:choose>
										     <c:when test="${layout_div.layoutDiv=='div1'}">
										        <div class="fl">
													<div div="div1" option="${shopLayout.layoutId}" class="module">
														<div div="div1" layout="layout3" mark="${shopLayout.layoutId}"  type="" id="content">
															<h3 class="module_small">1:2布局</h3>
														</div>
														<a href="javascript:void(0);" seq="${shopLayout.seq}" onclick="decorate_module(this);" layoutDivId="${layout_div.layoutDivId}" div="div1" layout="layout3"  mark="${shopLayout.layoutId}" moduleType="${shopLayout.layoutModuleType}" class="f_edit"><i></i>编辑</a>
														<a  layoutDivId="${layout_div.layoutDivId}" layout="layout3" div="div1" mark="${shopLayout.layoutId}" moduleType="" onclick="dele_layout(this)" href="javascript:void(0);" class="f_del"><i></i>删除</a>
													</div>
												</div>
										     </c:when>
										      <c:when test="${layout_div.layoutDiv=='div2'}">
										          <div class="fr">
													<div div="div2"  option="${shopLayout.layoutId}" class="module">
														<div div="div2" mark="${shopLayout.layoutId}" layout="layout3" type="" id="content">
															<h3 class="module_mid">1:2布局</h3>
														</div>
														<a href="javascript:void(0);" seq="${shopLayout.seq}" onclick="decorate_module(this);"  layoutDivId="${layout_div.layoutDivId}" div="div2" layout="layout3"  mark="${shopLayout.layoutId}" moduleType="" class="f_edit"><i></i>编辑</a>
														<a layout="layout3" div="div2" mark="${shopLayout.layoutId}" layoutDivId="${layout_div.layoutDivId}" onclick="dele_layout(this)" moduleType="" href="javascript:void(0);" class="f_del"><i></i>删除</a>
													</div>
												</div>
										     </c:when>
								</c:choose>
						     </c:forEach>
		  </div>
    </c:when>
     <c:when test="${shopLayout.layoutType=='layout4'}">
         <div location_mark="${shopLayout.layoutId}"  location="true" class="layout_four">
					         <c:forEach items="${shopLayout.shopLayoutDivDtos}" var="layout_div" varStatus="d">
						           <c:choose>
										 <c:when test="${layout_div.layoutDiv=='div2'}">
										   <div class="fl">
									        <div div="div2" option="${shopLayout.layoutId}" class="module">
									         <div div="div2" mark="${shopLayout.layoutId}" layout="layout4" type="" id="content">
									            <h3 class="module_mid">2:1布局</h3>
									            </div>
									            <a href="javascript:void(0);" seq="${shopLayout.seq}" onclick="decorate_module(this);"  layoutDivId="${layout_div.layoutDivId}" div="div2" layout="layout4"  mark="${shopLayout.layoutId}" moduleType="" class="f_edit"><i></i>编辑</a>
									            <a layout="layout4" div="div2" layoutDivId="${layout_div.layoutDivId}"  mark="${shopLayout.layoutId}" moduleType="" onclick="dele_layout(this)" href="javascript:void(0);" class="f_del"><i></i>删除</a>
									        </div>
									    </div>
										 </c:when>
										  <c:when test="${layout_div.layoutDiv=='div1'}">
											    <div class="fr">
											    	<div div="div1" option="${shopLayout.layoutId}" class="module">
											         <div div="div1" mark="${shopLayout.layoutId}"  layout="layout4" type="${shopLayout.layoutModuleType}" id="content">
											            <h3 class="module_small">2:1布局</h3>
											            </div>
											            <a href="javascript:void(0);" seq="${shopLayout.seq}" onclick="decorate_module(this);"  layoutDivId="${layout_div.layoutDivId}" div="div1" layout="layout4"  mark="${shopLayout.layoutId}" moduleType="" class="f_edit"><i></i>编辑</a>
											            <a layout="layout4" layoutDivId="${layout_div.layoutDivId}" div="div1" mark="${shopLayout.layoutId}"  moduleType="" onclick="dele_layout(this)" href="javascript:void(0);" class="f_del"><i></i>删除</a>
											        </div>
											    </div>
											 </c:when>
								   </c:choose>
				 </c:forEach>
			</div>
    </c:when>
</c:choose>




 
 