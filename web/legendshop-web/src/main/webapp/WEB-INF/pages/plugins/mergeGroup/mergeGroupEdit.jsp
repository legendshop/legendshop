<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/dialog.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>商家拼团活动</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/mergeGroup/mergeGroupEdit.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/prodSelectAlert.css'/>" rel="stylesheet"/>
    <script src="<ls:templateResource item='/resources/plugins/My97DatePicker/WdatePicker.js'/>" type="text/javascript"></script>
</head>
<body>
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
	</div>
	<div class="w1190">
		<div class="seller-cru">
			<p>
				您的位置>
				<a href="javascript:void(0)">首页</a>>
				<a href="javascript:void(0)">卖家中心</a>>
				<span class="on">拼团活动</span>
			</p>
		</div>
		<!-- 侧边导航 -->
		<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
		<!-- /侧边导航 -->

		<div class="merge-group-right">
			<form id="mergeForm">
				<!-- 基本信息 -->
				<div class="basic-ifo">
					<!-- 基本信息头部 -->
					<div class="ifo-top">
						基本信息
					</div>
					<div class="ifo-con">
						<li style="margin-bottom: 5px;">
							<span class="input-title" style="display: inline-block;width: 96px"><span class="required">*</span>活动名称：</span>
                            <input type="hidden" value="${mergeGroup.id}" name="id"/>
							<input type="text" name="mergeName" id="mergeName" class="active-name" value="${mergeGroup.mergeName}" placeholder="请填写活动名称" maxlength="25">
						</li>
						<li>
							<span class="input-title"></span>
							<span style="color: #999999;margin:10px 0px 0px 5px;">活动名称最多为25个字符</span>
						</li>
						<li style="margin-bottom: 0;">
							<span class="input-title" style="display: inline-block;width: 96px"><span class="required">*</span>活动时间：</span>
							<input type="text" class="active-time" readonly="readonly"  name="startTime" value="${fn:replace(mergeGroup.startTime,'.0','')}"  id=startTime placeholder="开始时间" />
							<span style="margin: 0 5px;">-</span>
							<input type="text" class="active-time" readonly="readonly"  name="endTime" value="${fn:replace(mergeGroup.endTime,'.0','')}" id="endTime" placeholder="结束时间" />
						</li>
					</div>
				</div>
				<!-- 选择活动商品 -->
				<div class="basic-ifo" style="margin-top: 20px;">
					<!-- 选择活动商品头部 -->
					<div class="ifo-top">
						选择活动商品
					</div>
					<div class="ifo-con">
						<div class="red-btn" id="selProd" style="display: inline-block">选择商品</div>

						<!-- <div class="check-shop">
							<div class="tab-shop"></div>
							<div class="check-bto">
								批量设置拼团价格: <input type="text">元
								<div class="red-btn" style="display: inline-block;">确定</div>
							</div>
						</div> -->
						<div class="tab-shop">
                            <c:if test="${not empty prodLists}">
							<table border="0" id="skuList" cellpadding="0" cellspacing="0" style="margin: 30px 0;">
								<tbody>
								<tr style="background: #f9f9f9;">
                                    <td width="70px">
                                        <label class="checkbox-wrapper">
                                            <span class="checkbox-item">
                                                <input type="checkbox" id="checkbox" class="checkbox-input selectAll">
                                                <span class="checkbox-inner"></span>
                                            </span>
                                        </label>
                                    </td>
									<td width="70px">商品图片</td>
									<td width="170px">商品名称</td>
									<td width="100px">商品规格</td>
									<td width="100px">商品价格</td>
									<td width="100px">可销售库存</td>
									<td width="100px">拼团价(元)</td>
								</tr>
								<c:choose>
									<c:when test="${not empty prodLists}">
										<c:forEach items="${prodLists}" var="prod">
                                            <c:forEach items="${prod.dtoList}" var="sku">
											<tr class="sku" >
												<td>
													<label class="checkbox-wrapper">
                                                        <span class="checkbox-item">
                                                            <input type="checkbox" name="array" class="checkbox-input selectOne" onclick="selectOne(this);">
                                                            <span class="checkbox-inner"></span>
                                                        </span>
													</label>
												</td>
												<td>
                                                    <img src="<ls:photo item='${sku.skuPic}'/>" width="60px" alt="">
												</td>
												<td>
													<span class="name-text">${sku.skuName}</span>
												</td>
												<td>
													<span class="name-text">${sku.cnProperties}</span>
												</td>
												<td>
													<span class="name-text">${sku.skuPrice}</span>
												</td>
												<td>${sku.skuStocks}</td>
												<td>
													<input type="text" class="set-input mergePrice" id="mergePrice" name="mergePrice" value="${sku.mergePrice}">
													<input type="hidden" class="mergeProdId" value="${sku.prodId}">
													<input type="hidden" class="mergeSkuId" value="${sku.skuId}">
													<input type="hidden" class="mergeSkuPrice" value="${sku.skuPrice}">
												</td>
											</tr>
                                            </c:forEach>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<tr class="first">
											<td colspan="7" >暂未选择商品</td>
										</tr>
									</c:otherwise>
								</c:choose>
								</tbody>
							</table>
                            </c:if>
						</div>
						<div class="check-bto" <c:if test="${empty prodLists}">style="display: none;margin:-11px 54px 0 0;float: right"</c:if> <c:if test="${not empty prodLists}">style="display: inline-block;margin: -11px 54px 0 0;float: right"</c:if>>
							批量设置拼团价格: <input type="text" id="batchPrice" maxlength="10">元
							<div class="red-btn" style="display: inline-block;margin-left: 10px" id="confim">确定</div>
						</div>
					</div>

				</div>
				<!-- 活动规则 -->
				<div class="basic-ifo" style="margin-top: 20px;">
					<!-- 活动规则头部 -->
					<div class="ifo-top">
						活动规则
					</div>
					<div class="ifo-con">
						<li>
							<span class="input-title" style="display: inline-block;width: 96px"><span class="required">*</span>成团人数：</span>
							<input type="text" class="active-time" name="peopleNumber" value="${mergeGroup.peopleNumber}" id="peopleNumber" placeholder="请填写成团人数"><span style="color: #999999;">（满足设定人数即可成团）</span>
						</li>
						<li>
							<span class="input-title" style="display: inline-block;width: 96px"><span class="required">*</span>团长免单：</span>
							<span class="act-limit">
								<input type="hidden" name="isHeadFree" id="isHeadFree" value="false">
								<a href="javascript:void(0)" class="lim-btn noFree on">否</a><a href="javascript:void(0)" class="lim-btn free">是</a>
							</span>
						</li>
						<li>
							<span class="input-title" style="display: inline-block;width: 96px"><span class="required">*</span>活动图片：</span>
							<div class="con-input" style="margin-left: 93px;margin-top: -15px;">
								<input type="file" name="file" id="file" class="upload-file"/>
								<c:if test="${empty mergeGroup.pic}">
								<a href="javascript:void(0)" class="add-img addphoto">+添加照片</a>
								</c:if>
								<img class="img-preview add-img" src="<ls:photo item='${mergeGroup.pic}'/>" alt="" <c:if test="${empty mergeGroup.pic}"> style="opacity: 0;filter:Alpha(opacity=0);"</c:if>>
								<span style="color:#e2393b;display:none" class="fileTip">请上传活动图片</span>
							</div>
						</li>
					</div>
				</div>
			</form>
			<div class="bto-btn">
				<div class="red-btn" onclick="saveMergeGroup();">提交</div>
				<div class="red-btn return-btn" onclick="window.location='/s/mergeGroupActivity/query'">返回</div>
			</div>
		</div>
	</div>
	<!--foot-->
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	<!--/foot-->
</body>

<script src="${contextPath}/resources/common/js/jquery.validate.js"></script>
<script src="${contextPath}/resources/common/js/jquery.img.preview.js"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/mergeGroup/mergeGroupEdit.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/common/js/checkImage.js'/>"></script>
<script type="text/javascript">
	var contextPath = "${contextPath}";
	var photoPath = "<ls:photo item=''/>";
	var photo = "${mergeGroup.pic}";
	$(function(){
        var rs = "${mergeGroup.isHeadFree}";
        if("${mergeGroup.isHeadFree}"!=null){
			if (rs=="true"){
				$(".free").addClass("on");
                $(".noFree").removeClass("on");
			}
		}
	})
</script>
</html>