<!DOCTYPE HTML>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://www.legendesign.net/date-util"  prefix="dateUtil" %>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss" var="nowDate" />
<style type="text/css">
 body{margin:0;padding:0;}
 #receiveWin{margin:0 auto;padding:0px 20px 10px 20px;width: 390px;
/*     height: 260px; */
 	font-size: 12px;
 	color:#666;
 }
  #receiveWin .row{
  	width:100%;
  	margin: 10px 0;
  }
  #receiveWin .row .label{
  	display:inline-block;
  	width:80px;
  	height:30px;
  	line-height:30px;
  	font-weight: bold;
  	text-align: right;
  	margin-right:10px;
  }
  #receiveWin .row .content{
  	display:inline-block;
  	height:30px;
  	line-height:30px;
  }
  #receiveWin .row .input-select{
  	display:inline-block;
  	width:60px;
  	height:30px;
  	line-height:30px;
  }
  #receiveWin .row .input-select select{
  	width:100px;
  	height: 25px;
  	border: 1px solid #ccc;
  	color:#666;
  }
  #receiveWin .row .tips{
  	display:block;
  	color:#999;
  	margin-left:95px;
  	line-height: 20px;
  	width:295px;
  }
  #receiveWin .row .input-area{
  	display:inline-block;
  	width: 200px;
  	margin-left:5px;
  }
  #receiveWin .row .input-area textarea{
  	width: 200px;
  	height: 60px;
  	border: 1px solid #ccc;
  	color:#666;
  	font-size: 13px;
  	padding:4px;
  }
  .required{
  	color:red;
  	display: inline-block;
    padding: 3px;
    font-weight: normal;
  }
 .btn-box{
    	width: 100%;
    	padding: 10px 0;
    	border-top:1px solid #ddd;
    	background-color: #F5F5F5;
    }
 .btn-box .btn{
   		width:100px;
   		height:30px;
   		border:none;
   		border-radius:3px;
   		text-align: center;
   		color:#fff;
   		font-size:14px;
   		font-weight: bold;
   		background-color: #e5004f;
   		cursor: pointer;
   }
   .btn-box .btn-wrap{
   		width:100px;
   		margin: 0 auto;
   }
</style>

<!-- 确认收货窗口 -->
<div id="receiveWin">
	<input type="hidden" id="refundId" name="refundId" value="${refund.refundId}"/>
	<div class="row">
		<span class="label">发货时间:</span>
		<span class="content"><fmt:formatDate value='${refund.shipTime}' pattern="yyyy-MM-dd HH:mm"/></span>
	</div>
	<div class="row">
		<span class="label">物流公司:</span>
		<span class="content">${refund.expressName}</span>
	</div>
	<div class="row">
		<span class="label">物流单号:</span>
		<span class="content">${refund.expressNo}</span>
	</div>
	<div class="row" style="height:40px;">
		<span class="label"><em class="required">*</em>收货情况:</span>
		<span class="input-select">
			<select id="goodsState" name="goodsState">
				<option value="">请选择</option>
				<c:if test="${dateUtil:rollDay(refund.shipTime,5) lt nowDate }">
					<option value="3">未收到</option>
				</c:if>
				<option value="4">已收到</option>
			</select>
		</span>
		<!-- 小芳要求注释掉的 -->
		<!-- <span class="tips">
			如果暂时没收到请联系买家，发货 5 天后可以选择未收到，买家可以延长时间，超过 7 天不处理按弃货处理。
		</span> -->
	</div>
	<div class="row">
		<span class="label" style="float: left;">收货备注:</span>
		<span class="input-area">
			<textarea id="receiveMessage" name="receiveMessage" rows="3"></textarea>
		</span>
	</div>
</div>
<div class="btn-box">
	<div class="btn-wrap">
		<input type="button" value="确定" class="btn" onclick="receiveGoods()"/>
	</div>
</div>
<script type="text/javascript">
	var contextPath = '${contextPath}';
	//确认收货
	function receiveGoods(){
		var refundId = $("#refundId").val();
		var goodsState = $("#goodsState").val();
		var receiveMessage = $("#receiveMessage").val();
		
		if(isBlank(refundId) || isBlank(goodsState)){
			layer.msg("对不起,请您选择收货情况后再提交!",{icon:0});
			return;
		}
		
		$.ajax({
			url : contextPath + "/s/receiveGoods/" + refundId,
			type : "POST",
			data : {"goodsState":goodsState,"receiveMessage":receiveMessage},
			dataType : "JSON",
			async : true,
			error : function(xhr,status,error){
				layer.alert("对不起,网络错误,请稍后重试!",{icon:2});
			},
			success: function(result,status,xhr){
				if(result == "OK"){
					layer.alert("恭喜你,操作成功!",{icon:1},function(){
						window.parent.location.href = contextPath + "/s/refundReturnList/" + '${refund.applyType}';
					});
				}else{
					layer.alert(result,{icon:2});
				}
			}
		});
	}
	
	function isBlank(value){
		return value == undefined || value == null || $.trim(value) === "";
	}
</script>
