<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss" var="nowDate" />
	
<table class="sold-table">
<colitem>
	<col style="width: 28%; min-width: 28%">
	<col style="width: 28%; min-width: 28%">
	<col style="width: 15%; min-width: 15%">
	<col style="width: 29%; min-width: 29%">
</colitem>
	<tr class="sold-tit">
		<th>方案名称</th>
		<th>促销时间</th>
		<th>促销状态</th>
		<th>操作</th>
	</tr>
	 <c:choose>
        <c:when test="${empty requestScope.list}">
           <tr>
		 	 <td colspan="20">
		 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
		 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
		 	 </td>
 			</tr>
        </c:when>
        <c:otherwise>
           <c:forEach items="${requestScope.list }" var="item">
			<tr class="detail">
				<td width="80" style="padding: 0 5px;word-break:break-all;">
					<c:choose>
						<c:when test="${item.preSaleEnd lt nowDate || item.status eq -3 || item.status eq 2}">
							${item.schemeName}
						</c:when>
						<c:when test="${item.status eq 0}">
							<a href="${contextPath}/presell/views/${item.id}" target="_blank">${item.schemeName }</a>
						</c:when>
						<c:otherwise>
							${item.schemeName}
						</c:otherwise>
					</c:choose>
				</td>
				<td>
					<fmt:formatDate value='${item.preSaleStart}' pattern='yyyy-MM-dd HH:mm'/>
					</br>↓</br>
					 <fmt:formatDate value='${item.preSaleEnd}' pattern='yyyy-MM-dd HH:mm'/>
				</td>
				<!-- 促销状态 -->
				<td>
					<c:choose>
						<c:when test="${item.preSaleEnd lt nowDate || item.status eq -3 || item.status eq 2}">已过期</c:when>
						<c:when test="${item.status eq 3}">已终止</c:when>
						<c:when test="${item.status eq -1 and item.preSaleEnd gt nowDate}">待审核</c:when>
						<c:when test="${item.status eq 1 and item.preSaleEnd gt nowDate}">
							已拒绝
							<span class="question-mark"></span>
							<input type="hidden" id="auditOpinion" value="${item.auditOpinion}">
							<span id="opinion" class="auctions-sta opinion" style="display:none;">
														<div id="history" class="more"></div>
													</span>
						</c:when>
						<c:when test="${item.status eq 0}">
							<c:choose>
								<c:when test="${empty item.prodStatus || item.prodStatus eq -1 || item.prodStatus eq -2}">
									商品已删除
								</c:when>
								<c:when test="${item.prodStatus eq 0}">
									商品已下线
								</c:when>
								<c:when test="${item.prodStatus eq 2}">
									商品违规下线
								</c:when>
								<c:when test="${item.prodStatus eq 3}">
									商品待审核
								</c:when>
								<c:when test="${item.prodStatus eq 4}">
									商品审核失败
								</c:when>
								<c:when test="${item.preSaleStart le nowDate and item.preSaleEnd gt nowDate}">
									<font color="#e5004f">进行中</font>
								</c:when>
								<c:when test="${item.preSaleStart gt nowDate}">
									未开始
								</c:when>
							</c:choose>
						</c:when>
<%--						<c:when test="${item.status eq 2 }">已完成</c:when>--%>
					</c:choose>
				</td>
				<td align="center" >
					<c:if test="${item.status ne -1 && item.status ne -2 && item.status ne 1 && item.preSaleStart < nowDate}">
						<a href="${contextPath}/s/presellProd/details/${item.id}" class="btn-r">查看</a>
					</c:if>
					<c:if test="${item.status ne -1 && item.status ne -2 && item.status ne 1 && item.preSaleStart < nowDate}">
						<a href="${contextPath }/s/presellProd/operatorPresellActivity/${item.id}" class="btn-r">运营</a>
					</c:if>
					<c:if test="${item.status eq -1 || item.status eq 1 || item.preSaleStart > nowDate}">
						<a href="${contextPath}/s/presellProd/edit/${item.id}"class="btn-r">编辑</a>
					</c:if>
					<c:if test="${item.status ne 0 && item.preSaleStart < nowDate || item.status eq -1 || item.status eq 1 }">
						<a href="javascript:void(0);" class="blue_button del btn-g" onclick="deletePresellProd('${item.id}')">删除</a>
					</c:if>


<%--						&lt;%&ndash; 已结束 或者商品状态异常 &ndash;%&gt;--%>
<%--					<c:choose>--%>
<%--						<c:when test="${empty item.prodStatus || item.preSaleEnd < nowDate || item.prodStatus eq -1 || item.prodStatus eq -2 || item.prodStatus eq 2 || item.status eq -3}">--%>
<%--							<a href="${contextPath}/s/presellProd/details/${item.id}" class="btn-r">查看</a>--%>
<%--							<a href="${contextPath }/s/presellProd/operatorPresellActivity/${item.id}" class="btn-r">运营</a>--%>

<%--							&lt;%&ndash; 待审核 或  未通过 并且已结束  进行物理删除 &ndash;%&gt;--%>
<%--							<c:if test="${item.status eq -1 || item.status eq -2}">--%>
<%--								<a href="javascript:void(0);" class="blue_button del btn-g" onclick="deletePresellProd('${item.id}')">删除</a>--%>
<%--							</c:if>--%>

<%--							&lt;%&ndash; 进行中 或  已失效 并且已结束  进行逻辑删除 &ndash;%&gt;--%>
<%--							<c:if test="${item.status eq 0 || item.status eq 1 || item.status eq 2}">--%>
<%--								<a href="javascript:void(0);" class="blue_button del btn-g" onclick="deletePresellProd('${item.id }')">删除</a>--%>
<%--							</c:if>--%>
<%--						</c:when>--%>
<%--						<c:otherwise>--%>
<%--							&lt;%&ndash; 待审核 或  未通过 &ndash;%&gt;--%>
<%--							<c:if test="${item.status eq -1 || item.status eq -2}">--%>
<%--								<a href="${contextPath}/s/presellProd/edit/${item.id}"class="btn-g">编辑</a>--%>
<%--								<a href="javascript:void(0);" class="blue_button del btn-g" onclick="deletePresellProd('${item.id}')">删除</a>--%>
<%--							</c:if>--%>
<%--							&lt;%&ndash; 待审核 或 未通过 &ndash;%&gt;--%>

<%--							&lt;%&ndash; 未开始 &ndash;%&gt;--%>
<%--							<c:if test="${item.status eq 1 && item.preSaleStart > nowDate}">--%>
<%--								<a  href="${contextPath}/s/presellProd/edit/${item.id}" class="btn-g">编辑</a>--%>
<%--							</c:if>--%>
<%--							&lt;%&ndash; 未开始 &ndash;%&gt;--%>

<%--							&lt;%&ndash; 进行中 &ndash;%&gt;--%>
<%--							<c:if test="${item.status eq 1 && item.preSaleStart < nowDate}">--%>
<%--								<a href="${contextPath}/s/presellProd/details/${item.id}" class="btn-r">查看</a>--%>
<%--								<a href="${contextPath }/s/presellProd/operatorPresellActivity/${item.id}" class="btn-r">运营</a>--%>
<%--								&lt;%&ndash;<a href="javascript:void(0);" onclick="offlineShopitem('${item.id}')" class=" btn-g">终止</a>&ndash;%&gt;--%>
<%--							</c:if>--%>
<%--							&lt;%&ndash; 进行中 &ndash;%&gt;--%>

<%--							&lt;%&ndash; 已失效 或已结束 &ndash;%&gt;--%>
<%--							<c:if test="${item.status eq 0 || item.status eq 2}">--%>
<%--								<a href="${contextPath}/s/presellProd/details/${item.id}" class="btn-r">查看</a>--%>
<%--								<a href="${contextPath }/s/presellProd/operatorPresellActivity/${item.id}" class="btn-r">运营</a>--%>
<%--								<a href="javascript:void(0);" class="btn-g" onclick="deletePresellProd('${item.id }')">删除</a>--%>
<%--							</c:if>--%>
<%--							&lt;%&ndash; 已失效  或已结束 &ndash;%&gt;--%>
<%--						</c:otherwise>--%>
<%--					</c:choose>--%>
				</td>	
			</tr>
		</c:forEach>
       </c:otherwise>
      </c:choose>
</table>
<div class="clear"></div>
 <div style="margin-top:10px;" class="page clearfix">
  	<div class="p-wrap">
       	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
   </div>
</div>
<script>
	$(".question-mark").mouseover(function () {
		var auditOpinion = $("#auditOpinion").val();
		var html = "<ul><li>" + auditOpinion + "</li></ul>";
		$("#history").html(html);
		$("#opinion").css("display", "block")
	});

	$(".question-mark").mouseout(function () {
		$("#history").html("");
		$("#opinion").css("display", "none")
	});

</script>
