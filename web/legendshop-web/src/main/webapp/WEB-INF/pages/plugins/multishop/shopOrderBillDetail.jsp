<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>商家结算详情-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link rel="stylesheet" href="<ls:templateResource item='/resources/templets/css/multishop/shopOrderBill.css'/>" />
	 <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-orderBillList${_style_}.css'/>" rel="stylesheet">
	<style>
		.tips{
			color: #e5004f !important;
		}
	</style>
</head>
<body class="graybody">
	<div id="doc">
	<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
   <div id="Content" class="w1190">
		<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">商家结算详情</span>
				</p>
			</div>
				<%@ include file="included/sellerLeft.jsp" %>
	<div class="seller-orderBillList">
	<div class="now-have">
		<div class="bill_alert">
	    	<c:if test="${shopOrderBill.status==1}">
		    	<div style="position:absolute;right:8px;bottom:8px;">
		        <a href="javascript:void(0);" onclick="confirmShopBill(${shopOrderBill.id});" class="confirm_btn"><span style="color: white;">本期结算无误，我要确认</span></a>
		        </div>
	        </c:if>
		    <div style="width:800px"><h3 style="float:left;font-size: 22px;">本期结算</h3>
		    	<div style="clear:both"></div>
		    </div>
		    <ul>
		      <li>档期：${shopOrderBill.flag} </li>
		      <li>结算单号：${shopOrderBill.sn} </li>
		      <li>起止时间：<fmt:formatDate value="${shopOrderBill.startDate}" pattern="yyyy-MM-dd"/>&nbsp;至&nbsp;<fmt:formatDate value="${shopOrderBill.endDate}" pattern="yyyy-MM-dd"/></li>
		      <li>出账时间：<fmt:formatDate value="${shopOrderBill.createDate}" pattern="yyyy-MM-dd"/></li>
		      <li>本期应收：<fmt:formatNumber  pattern="#.##" value="${shopOrderBill.resultTotalAmount}" /> =
				  <fmt:formatNumber  pattern="#.##" value="${shopOrderBill.orderAmount}" /> <a href="javascript:void(0);" id="orderAmount" class="tips">(订单金额)</a> -
				  ${shopOrderBill.orderReturnTotals} <a href="javascript:void(0);" id="refundAmount" class="tips">(退单金额)</a> -
				  ${shopOrderBill.commisTotals}<a href="javascript:void(0);" id="commisTotals" class="tips">(平台佣金)</a> -
				  ${shopOrderBill.distCommisTotals}<a href="javascript:void(0);" id="distCommisTotals" class="tips">(分销佣金)</a> +
				  ${shopOrderBill.redpackTotals}<a href="javascript:void(0);" id="redpackTotals" class="tips">(红包总额)</a>
			  </li>
			  <li style="margin-left: 65px;">
				  - ${shopOrderBill.orderReturnRedpackTotals}<a href="javascript:void(0);" id="returnRedpackTotals" class="tips">(退单红包总额)</a>
				  <%-- <c:if test=" ${shopOrderBill.preDepositPriceTotal ne 0}">--%>
				  + ${shopOrderBill.preDepositPriceTotal}<a href="javascript:void(0);" id="preDepositPriceTotal" class="tips">(预售定金)</a>
				  <%-- </c:if>--%>
				  <%-- <c:if test=" ${shopOrderBill.auctionDepositTotal ne 0}">--%>
				  + ${shopOrderBill.auctionDepositTotal}<a href="javascript:void(0);" id="auctionDepositTotal" class="tips">(拍卖保证金)</a>
				  <%-- </c:if>--%>
			  </li>
			  <li>本期平台佣金比例：${shopOrderBill.commisRate}%</li>
		      <li>结算状态：<c:choose>
	                		<c:when test="${shopOrderBill.status==1}">已出账</c:when>
	                		<c:when test="${shopOrderBill.status==2}">商家已确认</c:when>
	                		<c:when test="${shopOrderBill.status==3}">平台已审核</c:when>
	                		<c:when test="${shopOrderBill.status==4}">结算完成</c:when>
	                	</c:choose>
	          </li>
		    </ul>
	  	</div>
  	</div>
	<div class="pagetab" style="border: 0">
		<ul>
			<li <c:if test="${empty type||type==1}">class="on"</c:if> onclick="javascript:window.location='${contextPath}/s/shopOrderBill/load/${shopOrderBill.id}?type=1'"><span>订单列表</span></li>
			<li <c:if test="${type==2}">class="on"</c:if> onclick="javascript:window.location='${contextPath}/s/shopOrderBill/load/${shopOrderBill.id}?type=2'"><span>退款订单</span></li>
			<li <c:if test="${type==3}">class="on"</c:if> onclick="javascript:window.location='${contextPath}/s/shopOrderBill/load/${shopOrderBill.id}?type=3'"><span>分销订单</span></li>
			<li <c:if test="${type==4}">class="on"</c:if> onclick="javascript:window.location='${contextPath}/s/shopOrderBill/load/${shopOrderBill.id}?type=4'"><span>预售定金</span></li>
			<li <c:if test="${type==5}">class="on"</c:if> onclick="javascript:window.location='${contextPath}/s/shopOrderBill/load/${shopOrderBill.id}?type=5'"><span>拍卖保证金</span></li>
		</ul>
	</div>
	<div class="orderBillList-sea clearfix">
		<form:form  action="${contextPath}/s/shopOrderBill/load/${shopOrderBill.id}" method="post" id="from1" cssStyle="display: inline-block">
		<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
		<input type="hidden" id="type" name="type" value="${type}" />
		 <c:if test="${empty type || type eq 1 || type eq 2}">
			<div class="">
				<div class="item">订单编号:
					<input type="text" value="${subNumber}" class="item-inp text"  name="subNumber">
					<input type="submit" class="btn-r serach" id="btn_keyword" value="查 询" >
				</div>
			</div>
		</c:if>
		<div class="clear"></div>
	</form:form>
		<button id="exprot" onclick="formsubmit();" class="btn-r">导出数据</button>
	</div>


	<form:form  action="${contextPath}/s/shopOrderBill/outputOrder/${shopOrderBill.id}" method="post" id="from2" cssStyle="display: none">
		<input type="hidden"  name="curPageNO" value="${curPageNO}"/>
		<input type="hidden"  name="type" value="${type}" />
		<c:if test="${type ne 5}">
			<div class="orderBillList-sea clearfix">
				<div class="item">订单编号:
					<input type="text" value="${subNumber}" class="item-inp text"  name="subNumber">
					<input type="submit" class="btn-r serach"  id="formsubmit" value="查 询" >
				</div>
			</div>
		</c:if>
		<div class="clear"></div>
	</form:form>
	
	 	<c:choose>
        	<c:when test="${empty type||type eq 1}">
        		<table id="shopOrderBill"  style="" cellspacing="0" cellpadding="0" class="orderBillList-table">
					<tr class="orderBillList-tit sold-table">
					<td>订单编号</td>
					<td>下单时间</td>
					<td>成交时间</td>
					<td>订单金额</td>
					<td>运费</td>
					<td>红包金额</td>
					<td>操作</td>
					</tr>
				   <c:choose>
			           <c:when test="${empty requestScope.list}">
			              <tr>
						 	 <td colspan="20">
						 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
						 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
						 	 </td>
						 </tr>
			           </c:when>
			           <c:otherwise>
			              <c:forEach items="${requestScope.list}" var="sub" varStatus="status">
				            <tr class="detail">
				                <td height="35">${sub.subNumber}</td>
				                
				                <td><fmt:formatDate value="${sub.payDate}" pattern="yyyy-MM-dd"/></td>
				                <td><fmt:formatDate value="${sub.finallyDate}" pattern="yyyy-MM-dd"/></td>
				                <td><fmt:formatNumber value="${sub.actualTotal}" type="currency" pattern="￥##.##"  minFractionDigits="2" /></td>
				                <td><fmt:formatNumber value="${sub.freightAmount}" type="currency" pattern="￥##.##"  minFractionDigits="2" /></td>
				                <td><fmt:formatNumber value="${sub.redpackOffPrice}" type="currency" pattern="￥##.##"  minFractionDigits="2" /></td>
				                <td>
				 				<a target="_blank" href='${contextPath}/s/orderDetail/${sub.subNumber}' title="查看" class="btn-r" ><span>查看</span></a>
				                </td>
				            </tr>
				         </c:forEach> 
			           </c:otherwise>
		           </c:choose>
				</table>
        	</c:when>
			<c:when test="${type eq 2}">
				<table id="shopOrderBill"  style="width:100%" cellspacing="0" cellpadding="0" class="orderBillList-table">
					<tr class="orderBillList-tit sold-table">
						<td>订单编号</td>
						<td >类型</td>
						<td >退款金额</td>
						<td >退款红包金额</td>
						<td >退单时间(申请)</td>
						<td >完成时间</td>
						<td>操作</th>
					</tr>
					<c:choose>
						<c:when test="${empty requestScope.list}">
							<tr>
								<td colspan="20">
									<!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
									<div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach items="${requestScope.list}" var="prodReturn" varStatus="status">
								<tr class="detail">
									<td height="35">${prodReturn.subNumber}</td>
									<td> <c:if test="${prodReturn.applyType eq 1}">退款</c:if>
										<c:if test="${prodReturn.applyType eq 2}">退款退货</c:if></td>
									<td><fmt:formatNumber value="${prodReturn.refundAmount}" type="currency" pattern="￥##.##"  minFractionDigits="2" /></td>
									<td><fmt:formatNumber value="${prodReturn.returnRedpackOffPrice}" type="currency" pattern="￥##.##"  minFractionDigits="2" /></td>
									<td><fmt:formatDate value="${prodReturn.applyTime}" pattern="yyyy-MM-dd"/></td>
									<td><fmt:formatDate value="${prodReturn.adminTime}" pattern="yyyy-MM-dd"/></td>
									<td>
										<c:if test="${prodReturn.applyType eq 1}">
											<a target="_blank" href='${contextPath}/s/refundDetail/${prodReturn.refundId}' title="查看" class="btn-r"><span>查看</span></a>
										</c:if>
										<c:if test="${prodReturn.applyType eq 2}">
											<a target="_blank" href='${contextPath}/s/returnDetail/${prodReturn.refundId}' title="查看" class="btn-r"><span>查看</span></a>
										</c:if>
									</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</table>
			</c:when>
			<c:when test="${type eq 3}">
				<table id="shopOrderBill"  style="width:100%" cellspacing="0" cellpadding="0" class="orderBillList-table">
					<tr class="orderBillList-tit sold-table">
						<td>订单编号</td>
						<td>订单项编号</td>
						<td >直接上级分销佣金</td>
						<td >上二级分销佣金</td>
						<td >上三级分销佣金</td>
						<td >总分销佣金</td>
					</tr>
					<c:choose>
						<c:when test="${empty requestScope.list}">
							<tr>
								<td colspan="20">
									<!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
									<div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach items="${requestScope.list}" var="item" varStatus="status">
								<tr class="detail">
									<td height="35">${item.subNumber}</td>
									<td>${item.subItemNumber}</td>
									<td><fmt:formatNumber value="${item.distUserCommis}" type="currency" pattern="￥##.##"  minFractionDigits="2" /></td>
									<td><fmt:formatNumber value="${item.distSecondCommis}" type="currency" pattern="￥##.##"  minFractionDigits="2" /></td>
									<td><fmt:formatNumber value="${item.distThirdCommis}" type="currency" pattern="￥##.##"  minFractionDigits="2" /></td>
									<td><fmt:formatNumber value="${item.distCommisCash}" type="currency" pattern="￥##.##"  minFractionDigits="2" /></td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</table>
			</c:when>
			<c:when test="${type eq 4}">
				<table id="shopOrderBill"  style="width:100%" cellspacing="0" cellpadding="0" class="orderBillList-table">
					<tr class="orderBillList-tit sold-table">
						<td>订单编号</td>
						<td>支付方式</td>
						<td >定金金额</td>
						<td >定金支付时间</td>
						<td >尾款支付</td>
						<td >尾款支付结束时间</td>
						<td>操作</td>
					</tr>
					<c:choose>
						<c:when test="${empty requestScope.list}">
							<tr>
								<td colspan="20">
									<!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
									<div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach items="${requestScope.list}" var="item" varStatus="status">
								<tr class="detail">
									<td height="35">${item.subNumber}</td>
									<td>
										<c:choose>
											<c:when test="${item.payPctType eq 0}">全额</c:when>
											<c:otherwise>定金</c:otherwise>
										</c:choose>
									</td>
									<td><fmt:formatNumber value="${item.preDepositPrice}" type="currency" pattern="￥##.##"  minFractionDigits="2" /></td>
									<td><fmt:formatDate value="${item.depositPayTime}" pattern="yyyy-MM-dd"/></td>
									<td>
										<c:choose>
											<c:when test="${item.isPayFinal eq 0}">未支付</c:when>
											<c:otherwise>已支付</c:otherwise>
										</c:choose>
									</td>
									<td><fmt:formatDate value="${item.finalMEnd}" pattern="yyyy-MM-dd"/></td>
									<td>
										<a target="_blank" href='${contextPath}/s/orderDetail/${item.subNumber}' title="查看" class="btn-r" ><span>查看</span></a>
									</td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</table>
			</c:when>
        	<c:otherwise>
				<table id="shopOrderBill"  style="width:100%" cellspacing="0" cellpadding="0" class="orderBillList-table">
					<tr class="orderBillList-tit sold-table">
						<td>活动名称</td>
						<td>保证金额</td>
						<td>支付名称</td>
						<td>支付时间</td>
					</tr>
					<c:choose>
						<c:when test="${empty requestScope.list}">
							<tr>
								<td colspan="20">
									<!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
									<div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
							<c:forEach items="${requestScope.list}" var="item" varStatus="status">
								<tr class="detail">
									<td height="35">${item.auctionsTitle}</td>
									<td><fmt:formatNumber value="${item.payMoney}" type="currency" pattern="￥##.##"  minFractionDigits="2" /></td>
									<td>${item.payName}</td>
									<td><fmt:formatDate value="${item.payTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
								</tr>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</table>
        	</c:otherwise>
        </c:choose>	
		<div class="clear"></div>
		<div style="margin-top:10px;" class="page clearfix">
 			 <div class="p-wrap">
        		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }" type="simple"/> 
 			 </div>
	   </div>		    
   </div>
   </div>
   </div>
      <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
   </div>
   </body>
</html>
   	<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
	<script src="<ls:templateResource item='/resources/common/js/recordstatus.js'/>" type="text/javascript"></script>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/shopOrderBillDetail.js'/>"></script>
	<script type="text/javascript">
		var contextPath = "${contextPath}";
		var shopOrderBill = "${shopOrderBill.id}";
	</script>
