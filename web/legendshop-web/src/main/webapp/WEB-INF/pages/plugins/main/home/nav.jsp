<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
       <%@include file="hdmain.jsp" %>
      <!--hdmenu 结束-->
      <ls:cache key="NAV" cacheName="PAGE_CACHE">
      <c:set var="rootTreeNodeList"  value="${indexApi:findRootTreeNode()}"></c:set>
     <c:set var="headMenuList"  value="${indexApi:getHeadmemnu('pc')}"></c:set>
       <div id="hdmenu">
            <div class="yt-wrap top-menu">
                <div class="menu-cate default-expand-menu">
                       <a class="menu-cate-title" target="_blank" href="${contextPath}/allCategorys">全部分类</a>
                     <c:if test="${not empty rootTreeNodeList && not empty rootTreeNodeList.childList }"> 
                        <div class="menu-cate-all-out showlist" style="display: none;">
	                     <c:forEach items="${rootTreeNodeList.childList}" var="category" end="8">
		                      <dl class="dl-${status}"> 
	                            <dt>
	                              <a class="category" href="${contextPath}/list?categoryId=${category.selfId}">
	                              	<em>
	                              	<c:if test="${not empty category.obj.icon}">
	                              		<img src="<ls:photo item='${category.obj.icon}' />" />
	                              	</c:if>
	                              	</em>${category.nodeName}</a>
	                               <span></span>
	                            </dt>
	                               <dd style="display: ;">
                                        <div class="subcat">
                                          	<c:if test="${not empty category.obj.recommConDto.brandList || not empty category.obj.recommConDto.advList}">
                                             <div class="sc-aside">
                                                <h3>热门品牌</h3>
                                                <ul class="brand">
                                                	<c:if test="${not empty category.obj.recommConDto.brandList}">
	                                                	<c:forEach items="${category.obj.recommConDto.brandList}" var="b"  end="8">
	                                                	<c:if test="${not empty b.brandPic}">
	                                                     <li class="brand-item">
	                                                       <a href="${contextPath}/list?prop=brandId:${b.brandId}" target="_blank" title="${b.brandName}">
	                                                       <img src="<ls:photo item='${b.brandPic}'/>" width="80" height="50"></a>
	                                                     </li>
	                                                     </c:if>
	                                                     </c:forEach>
                                                     </c:if>
                                                 </ul>
                                                 <c:if test="${not empty category.obj.recommConDto.advList}">
	                                                 <c:forEach items="${category.obj.recommConDto.advList}" var="adv"  end="1">
	                                                 	<c:if test="${not empty adv.advPic }">
		                                                	 <a class="promote" href="${adv.link}" target="_blank">
		                                                	 <img src="<ls:photo item='${adv.advPic}'/>" width="280" />
		                                                	</a>
		                                                 </c:if>
	                                                 </c:forEach>
                                                 </c:if>
                                               </div>
                                               </c:if>
                                                <c:if test="${not empty category && not empty category.childList}">
                                                    <div class="sc-main">
                                                       <c:forEach items="${category.childList}" var="child" > 
                                                         <c:if test="${not empty child}">
                                                               <div class="subcell">
                                                                       <h3><a class="sc-item" href="${contextPath}/list?categoryId=${child.selfId}" >${child.nodeName}</a></h3>
                                                                       <c:if test="${not empty child.childList}">
                                                                            <p>
                                                                                <c:forEach items="${child.childList}" var="threeChild" >
                                                                                	<c:if test="${not empty threeChild}">
			                          	                                            	<a class="sc-item" href="${contextPath}/list?categoryId=${threeChild.selfId}" >${threeChild.nodeName}</a>
			                          	                                            </c:if>
			                                                                     </c:forEach>
		                                                                    </p>
                                                                       </c:if>
                                                              </div>
                                                            </c:if>
                                                       </c:forEach>
                                                    </div>
                                                 </c:if> 
                                          </div>                            
                                  </dd>
	                        </dl>
	                     </c:forEach>
                          <dl>
                            <dt>
                                <a class="a_categ" target="_blank" href="${contextPath}/allCategorys">全部商品分类</a> 
                            </dt>
                           </dl>
                        </div>
                     </c:if>
                </div>
                 <ul class="nav-hr" style="" data-clk="actwatch|navi|F">
					<c:if test="${not empty headMenuList}">
	                    <c:forEach items="${headMenuList}" var="headmenu">
	                    	<c:if test="${headmenu.status == 1 }">
		                    	<c:choose>
		                    		<c:when test="${headmenu.url eq '#' }">
		                    			  <li style="overflow:hidden" ><a  href="javascript:void(0);" onclick="developing();"><span>${headmenu.name}</span></a></li>
		                    		</c:when>
		                    		<c:otherwise>
		                    			<li style="overflow:hidden" ><a  href="${contextPath}${headmenu.url}" ><span>${headmenu.name}</span></a></li>
		                    		</c:otherwise>
		                    	</c:choose>
		                    </c:if>
	                  	</c:forEach>
                  	</c:if>
                </ul>
            </div>
        </div>
   </ls:cache>
<script>
	function developing(){
		layer.alert("正在开发中，敬请期待...", {icon: 6});
	}
</script>