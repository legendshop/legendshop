<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>账户安全 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/securityCenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/validateChanges.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;">     
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con"  id="rightContent">

	<div class="right">
			<c:choose>
			<c:when test="${paypassVerifn ==0}">
				<div class="o-mt"><h2>验证支付密码</h2></div>
				<div id="step2" class="step step01">
					<ul>
						<li class="fore1">1.验证身份<b></b></li><li class="fore2">2.验证支付密码<b></b></li><li class="fore3">3.完成</li>
					</ul>
				</div>
			</c:when>
			<c:otherwise>
				<div class="o-mt"><h2>修改支付密码</h2></div>
				<div id="step2" class="step step01">
					<ul>
						<li class="fore1">1.验证身份<b></b></li><li class="fore2">2.修改支付密码<b></b></li><li class="fore3">3.完成</li>
					</ul>
				</div>
			</c:otherwise>
			</c:choose>
			
			<div class="m m3 safe-sevi04" id="explainsss">
    			<div class="mc">
    				<div class="form">
    					<div class="item">
    						<span class="label"><em style="color:red;">*</em>设置支付密码：</span>
							<div class="fl">
								<input name="oldPassword"  id="oldPassword"  type="hidden" value="${oldPassword }" />
							    <input name="securityCode"  id="securityCode"  type="hidden" value="${securityCode }" />
							    <input name="emailCode"  id="emailCode"  type="hidden" value="${code }" />
							    <input name="payStrength"  id="payStrength"  type="hidden" />
								<input class="text" tabindex="1" name="payPwd" id="payPwd" type="password" maxlength="20">
								<div class="clr"></div>
								<div class="msg-text" id="payPwdMessage" style="display: none;">支付密码由6-20位字母、数字或符号(除空格)组成</div>
								<div class="clr"></div>
								<div id="payPwd_error" class="msg-error"></div>
								<div class="clr"></div>
								<div id="strength" class="ir icon-s-00"></div>
								<div class="clr"></div>
								<div class="msg-error" id="strength_error"></div>
    							<input id="passwordLevel" name="passwordLevel" type="hidden">
							</div>
    						<div class="clr"></div>
    					</div>
						
    					<div class="item">
    						<span class="label"><em style="color:red;">*</em>重新输入支付密码：</span>
							<div class="fl">
								<input class="text" tabindex="2" name="rePayPwd" id="rePayPwd" type="password" maxlength="20">
								<div class="clr"></div>
								<div id="rePayPwd_error" class="msg-error" style="display: none;"></div>
							</div>
    						<div class="clr"></div>
    					</div>
    					
						<div class="item">
            				<span class="label"><em style="color:red;">*</em>验证码：</span>
            				<div class="fl">
            					<input type="hidden" id="cannonull" name="cannonull" value='<fmt:message key="randomimage.errors.required"/>'/>
								<input type="hidden" id="charactors4" name="charactors4" value='<ls:i18n key="randomimage.charactors.required" length="4"/>'/> 
								<input type="hidden" id="errorImage" name="errorImage" value='<fmt:message key="error.image.validation"/>'/> 
												
								<input class="text" tabindex="2" name="randNum" id="randNum" type="text">
								<label><img id="randImage" name="randImage" src="<ls:templateResource item='/validCoderRandom'/>"  style="vertical-align: middle;"/>看不清？
            					<a href="javascript:void(0)" onclick="javascript:changeRandImg('${contextPath}')" style="font-weight: bold;"><fmt:message key="change.random2"/></a>
            					</label>
            					<div class="clr"></div>
            					<div id="authCode_error" class="msg-error"></div>
            				</div>
            				<div class="clr"></div>
            			</div>
						
						<div class="item btns">
        					<span class="label">&nbsp;</span>
							<div class="fl">
								<a id="submitCode" class="btn-r small-btn ncbtn" onclick="updatePayPwd();" href="javascript:void(0);"><s></s>提交</a>
								<a class="btn-r small-btn ncbtn" href="${contextPath}/p/security"><s></s>返回</a>
							</div>
        					<div class="clr"></div>
        				</div>
						
    				</div>
    			</div>
    		</div>
			
		</div>

</div>

 <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script type="text/javascript"  src="<ls:templateResource item='/resources/common/js/randomimage.js'/>"></script>
	<script type="text/javascript">
		var contextPath = '${contextPath}';
	</script>
<script  src="<ls:templateResource item='/resources/templets/js/updatePaymentpassword.js'/>" type="text/javascript"></script>
</body>
</html>