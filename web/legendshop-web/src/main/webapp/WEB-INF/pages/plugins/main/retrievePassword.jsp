<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
<title>找回密码-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}"/>
<meta name="description" content="${systemConfig.description}" />
</head>
<body  class="graybody">
<div id="doc">
	<%@include file='home/top.jsp'%>
   <div id="bd">
      <div class="yt-wrap ptit2">忘记密码</div>
      <!--order content -->
         <div class="yt-wrap ordermain" id="entry">
          <!-- 收货地址 -->
         	  <div class="cartnew-title clearfix">
                        <ul class="step_psw">
                            <li class="act"><span>填写账户名</span></li>
                            <li class=" "><span>验证身份</span></li>
                            <li class=" "><span>设置新密码</span></li>
                            <li class=" "><span>完成</span></li>
                        </ul>
              </div>
              
              <div class="reg_b getpsw">
              
                  <div class="item">
                        <span class="label">账户名：</span>
                        <div class="fl item-ifo">
                            <div class="r_inputdiv">
                                <input tabindex="1" id="name" name="name" type="text" class="text" autocomplete="off" onfocus="setfocus();" onblur="setblur();"> 
								<label id="name_error" calss=""></label>
                            </div>
                        </div>
                  </div>
                  
                  <div class="item" style="height:80px">
                        <span class="label"> 验证码：</span>
                        <div class="fl item-ifo">
                            <div class="r_inputdiv v_align_mid">
                            <input type="hidden" id="cannonull" name="cannonull" value='<fmt:message key="randomimage.errors.required"/>'/>
									<input type="hidden" id="charactors4" name="charactors4" value='<ls:i18n key="randomimage.charactors.required" length="4"/>'/> 
									<input type="hidden" id="errorImage" name="errorImage" value='<fmt:message key="error.image.validation"/>'/> 
                                <input type="text" tabindex="2" name="randNum" id="randNum" class="text" autocomplete="off" maxlength="4" style="width: 100px" onfocus="randNumSetFocus();" onblur="randNumSetBlur();"> 
                                <img id="randImage" name="randImage" src="<ls:templateResource item='/validCoderRandom'/>" width="95" height="38" style="vertical-align: middle;" />
                               <a href="javascript:void(0)" onclick="javascript:changeRandImg('${contextPath}')" style="font-weight: bold;"><fmt:message key="change.random2"/></a>
                            </div>
                            <span class="clr"></span>
								<label id="authCode_error" class="msg-error"></label>
                        </div>
                  </div>
                  
                  <div class="item">
                        <span class="label">&nbsp;</span>
                        <input tabindex="4" onclick="doIndex();" type="button" id="findPwdSubmit" class="btn-img btn-regist" value="下一步" >
                  </div>
              </div>
        </div>
   </div>
   <%@ include file="home/bottom.jsp" %>
</div>
</div>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/common/js/randomimage.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/retrievePassword.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/proveStatus.js'/>"></script>
<script type="text/javascript">
		var contextPath = '${contextPath}';
			var subTab = '<%=request.getParameter("t") %>';
				var userName = '<%=request.getParameter("userName")%>';
				var code = '<%=request.getParameter("code")%>';
				
				if(subTab != 'null'){
					window.location.href = contextPath + "/setNewPwd/"+userName+"/"+code;
				}
</script>
</body>
</html>