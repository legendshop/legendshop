<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<title>评价管理-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/comments/comment-list.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置> <a href="${contextPath}/home">首页</a>> 
					<a href="${contextPath}/sellerHome">卖家中心</a>> <span class="on">评价管理</span>
				</p>
			</div>
			
			<%@ include file="../included/sellerLeft.jsp"%>
			
			<div class="main" style="width:960px;float:right;margin-top:20px;">
				<div class="seller-com-nav">
					<ul>
						<li  class="on"><a href="javascript:void(0);">评论商品列表</a></li>
					</ul>
				</div>

				<div class="pagetab" id="commentTab" name="commentTab" style="margin-top: 20px;">
					<ul>
						<li class="on" condition="all">
							<span>全部评价<strong id="cnum0">(${prodCommCategory.total})</strong></span>
						</li>
						<li condition="waitReply">
							<span>待回复<strong id="cnum1">(${prodCommCategory.waitReply})</strong></span>
						</li>
						<li condition="good">
							<span>好评<strong id="cnum1">(${prodCommCategory.high})</strong></span>
						</li>
						<li condition="medium">
							<span>中评<strong id="cnum2">(${prodCommCategory.medium})</strong></span>
						</li>
						<li condition="poor">
							<span>差评<strong id="cnum3">(${prodCommCategory.low})</strong></span>
						</li>
						<li condition="photo">
							<span>晒图<strong id="cnum3">(${prodCommCategory.photo})</strong></span>
						</li>
						<li condition="append">
							<span>追加<strong id="cnum3">(${prodCommCategory.append})</strong></span>
						</li>
					</ul>
				</div>
				
				<input type="hidden" id="condition" name="condition" value="${params.condition}"/>
				
				<div id="comments-list" class="mc" style="display: block;">
					 <%@include file='commentListContent.jsp'%> 
				</div>
			</div>
		</div>
	</div>
	
	<script language="javascript">
		var contextPath = "${contextPath}";
		var prodId = "${prodCommCategory.prodId}";
	</script>
	<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/commonUtils.js'/>" /></script>
	<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/multishop/comments/commentList.js'/>" /></script>
	
	</body>
</html>
	
