<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>冻结中的余额信息 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/pdCashManager${_style_}.css'/>" rel="stylesheet" />
	 
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
   <%@ include file="/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp" %>
    <!-----------right_con-------------->
     <div class="right_con">
<div>
	<div class="o-mt">
		<h2>账户信息</h2>
	</div>
	<div class="pagetab2">
		<ul>
			<li><span><a href="<ls:url address='/p/predeposit/account_balance'/>">账户余额</a> </span></li>
			<li ><span><a href="<ls:url address='/p/predeposit/recharge_detail'/>">充值明细</a> </span></li>
			<li><span><a href="<ls:url address='/p/predeposit/balance_withdrawal'/>">余额提现</a> </span></li>
		    <li class="on" ><span><a >冻结中余额</a> </span></li>
		</ul>
	</div>

	<div>
		<div class="zhss">
		  <div style="float: left;margin-right: 5px;">
		      <span>类型：</span>
		      <select name="paystate_search" id="paystate_search">
				<option value="">-请选择-</option>
				<option value="withdraw" <c:if test="${type=='withdraw'}"> selected="selected" </c:if>>提现</option>
				<option value="order_freeze" <c:if test="${type=='order_freeze'}">selected="selected" </c:if>>下单</option>
				<option value="auctions_freeze" <c:if test="${type=='auctions_freeze'}">selected="selected" </c:if>>拍卖保证金</option>
			  </select> <span>申请单号：</span> 
			   <input type="hidden" value="${empty curPageNO?1:curPageNO}" name="curPageNO"  id="curPageNO"> 
			   <input type="text" value="${sn}" name="sn" id="sn" class="text w150"> 
		   </div>
			<div style="float: left;">
		      <label class="submit-border">
				<input type="button" onclick="searcBalanceWithdrawal();" value="搜索" class="submit btn-r">
			  </label>
		   </div>
		</div>
			<div id="recommend" class="recommend" style="display: block;">
		<table class="ncm-default-table sold-table">
			<thead>
				<tr>
					<th width="19%" >冻结时间</th>
					<th width="10%"  >冻结金额(元)</th>
					<th width="10%"  >标示流水号</th>
					<th width="21%" >类型</th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
		           <c:when test="${empty requestScope.list}">
		              <tr>
					 	 <td colspan="20">
					 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
					 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
					 	 </td>
					 </tr>
		           </c:when>
		           <c:otherwise>
		               <c:forEach items="${predeposit.cashHodings.resultList}" var="hoding" varStatus="status">
					      <tr>
					      <td><fmt:formatDate value="${hoding.addTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					      <td>
					         <fmt:formatNumber value="${hoding.amount}" type="currency"/>
					      </td>
					      <td>
					        ${hoding.sn}
					      </td>
					      <td>
					        <c:choose>
								<c:when test="${hoding.type=='withdraw'}">
								    提现冻结
								</c:when>
								<c:when test="${hoding.type=='order_freeze'}">
								   下单冻结
								</c:when>
								<c:when test="${hoding.type=='auctions_freeze'}">
								   拍卖保证金冻结
								</c:when>
							 </c:choose>
					      </td>
					      </tr>
					   </c:forEach>
		           </c:otherwise>
	           </c:choose>
			</tbody>
		</table>
	   </div>
	</div>
		<!--page-->
		<div style="margin-top:10px;" class="page clearfix">
			<div class="p-wrap">
				<span class="p-num">
					<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
				</span>
			</div>
		</div>
		<div class="clear"></div>
</div>

</div>
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
</div>
	<!--bd end-->
<script type="text/javascript">
	 var contextPath = '${contextPath}';
	 $(document).ready(function() {
		 userCenter.changeSubTab("mydeposit");
	 });
	 function pager(curPageNO){
	     window.location.href=contextPath+"/p/predeposit/balance_hoding?curPageNO="+curPageNO;
     }
     function searcBalanceWithdrawal(){
       var curPageNO=$("#curPageNO").val();
       var sn=$.trim($("#sn").val());
       var type=$("#paystate_search").find("option:selected").val();
        window.location.href=contextPath+"/p/predeposit/balance_hoding?curPageNO="+curPageNO+"&sn="+sn+"&type="+type;
     }
</script>  
</body>
</html>
			
