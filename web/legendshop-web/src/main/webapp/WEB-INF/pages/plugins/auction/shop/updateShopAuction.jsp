<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<title>添加拍卖活动-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/bidding-activity${_style_}.css'/>" rel="stylesheet">
</head>
<style>
	.prod-table{margin:10px 0;border:solid #ddd;border-width:1px 0px 0px 1px;margin-left: 160px;}
	.prod-table tr td{margin:0px;border:solid #ddd;border-width:0px 1px 1px 0px;font-size:12px;}
	.prod-table thead tr td{
		text-align: center;
		background-color:#F9F9F9;
		padding:8px;
		font-size:12px;
	}
	.prod-table tbody tr td{
		padding:8px 0px 8px 0px;
		font-size:12px;
	}
	
</style>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置> <a href="${contextPath}/home">首页</a>> <a
						href="${contextPath}/sellerHome">卖家中心</a>> <font class="on">拍卖活动管理</font>
				</p>
			</div>

			<%@ include
				file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp"%>
			<div id="rightContent" class="right_con">
				<!--page-->
				<!-- 添加活动 -->
				<form:form action="${contextPath}/s/auction/save" method="post" id="form1">
					<div class="bidding-add" >
						<ul>
							<c:if test="${auctions.status eq -2}">
								<li><font><b style="color: red">*</b>审核理由：${auctions.auditOpinion}</font></li>
							</c:if>
							<li><font><b style="color: red">*</b>活动名称：</font><input type="text" maxLength="50" name="auctionsTitle" id="auctionsTitle" value="${auctions.auctionsTitle }"><em></em></li>
							<li>
								<font><b style="color: red">*</b>拍卖商品：</font>
								<input type="button" value="修改商品  " class="file-btn" onclick="showProdlist();"/>
								<input type="hidden" value="${auctions.prodId}" name="prodId" id="prodId"/>
								<input type="hidden" value="${auctions.skuId}" name="skuId" id="skuId"/><em></em>
							</li>
							<li class="prodInfo">
								<table class="prod-table">
					        		<thead>
						        		<tr>
						        			<td width="60%">单品名称</td>
						        			<td width="20%">属性</td>
						        			<td width="20%">价格(元)</td>
						        		</tr>
					        		</thead>
					        		<tbody>
						        		<tr>
						        			<td style="padding-left:20px;width:380px;color:#999;" id="skuName" >${auctions.prodName}</td>
						        			<td id="cnProperties" style="text-align: center;color:#999;">${empty auctions.cnProperties?'无':auctions.cnProperties}</td>
						        			<td id="skuPrice" style="text-align: center;color:#999;">${auctions.prodPrice}</td>
						        		</tr>
					        		</tbody>
					        	</table>
							</li>
							
							<li><font><b style="color: red">*</b>开始时间：</font><input readonly="readonly"  name="startTime"  id="startTime" class="Wdate" type="text" value="<fmt:formatDate value='${auctions.startTime }' pattern='yyyy-MM-dd HH:mm:ss' type='date'/>" /><em></em></li>
							<li><font><b style="color: red">*</b>结束时间：</font><input readonly="readonly"  name="endTime"  id="endTime" class="Wdate" type="text" value="<fmt:formatDate value='${auctions.endTime }' pattern='yyyy-MM-dd HH:mm:ss' type='date'/>" /><em></em></li>
							<li><font><b style="color: red">*</b>起拍价：</font><input type="text" maxLength="10" onkeyup="value=value.replace(/\.\d{2,}$/,value.substr(value.indexOf('.'),3))" name="floorPrice" id="floorPrice" value="${auctions.floorPrice }"><em></em></li>
							
							<li><font><b style="color: red">*</b>是否封顶：</font>
								<select class="isCeiling" name="isCeilingSelect">
									<option value="">——请选择——</option>
									<option <c:if test="${auctions.isCeiling==0}">selected</c:if> value="0">不封顶</option>
									<option <c:if test="${auctions.isCeiling==1}">selected</c:if> value="1">封顶</option>
								</select>
							<input name="isCeiling" id="isCeiling" type="hidden" value="${auctions.isCeiling }"/><em></em></li>
							<li class="fixedPrice"><font><b style="color: red">*</b>一口价：</font><input type="text" maxLength="10" onkeyup="value=value.replace(/\.\d{2,}$/,value.substr(value.indexOf('.'),3))" name="fixedPrice" id="fixedPrice" value="${auctions.fixedPrice}"><em></em></li>
							<li><font><b style="color: red">*</b>最小加价幅度：</font><input type="text" maxLength="10" onkeyup="value=value.replace(/\.\d{2,}$/,value.substr(value.indexOf('.'),3))" name="minMarkupRange" id="minMarkupRange" value="${auctions.minMarkupRange }"><em></em></li>
							<li><font><b style="color: red">*</b>最大加价幅度：</font><input type="text" maxLength="10" onkeyup="value=value.replace(/\.\d{2,}$/,value.substr(value.indexOf('.'),3))" name="maxMarkupRange" id="maxMarkupRange" value="${auctions.maxMarkupRange }"><em></em></li>
							<li><font><b style="color: red">*</b>是否支付保证金：</font>
							<select class="isSecurity" name="isSecuritySelect" value="${auctions.isSecurity }">
								<option value="">——请选择——</option>
								<option <c:if test="${auctions.isSecurity==0}">selected</c:if> value="0">不支付保证金</option>
								<option <c:if test="${auctions.isSecurity==1}">selected</c:if> value="1">支付保证金</option>
							</select>
							<input name="isSecurity" id="isSecurity" type="hidden" value="${auctions.isSecurity}"/><em></em></li>
							<li class="securityPrice"><font><b style="color: red">*</b>保证金：</font><input type="text" maxLength="10" name="securityPrice" id="securityPrice" value="${auctions.securityPrice }"><em></em></li>
							<li class="delayTime"><font><b style="color: red">*</b>延时周期：</font><input type="text" maxLength="3" name="delayTime" id="delayTime" value="${auctions.delayTime }">分钟/次<em></em></li>
							<li><font><b style="color: red">*</b>是否隐藏起拍价：</font>
							<select class="hideFloorPrice" name="hideFloorPriceSelect">
								<option value="">——请选择——</option>
								<option <c:if test="${auctions.hideFloorPrice==1}">selected</c:if> value="1">隐藏起拍价</option>
								<option <c:if test="${auctions.hideFloorPrice==0}">selected</c:if> value="0">不隐藏起拍价</option>
							</select>
							<input name="hideFloorPrice" id="hideFloorPrice" type="hidden"  value="${auctions.hideFloorPrice}"/><em></em></li>
							<li class="textarea"><font style="float: left;">拍品描述：</font><textarea name="auctionsDesc" id="auctionsDesc" cols="50" rows="8" style="width:680px;height:300px;visibility:hidden;">${auctions.auctionsDesc }</textarea><em></em><input id="id" name="id" type="hidden" value="${auctions.id }"/>  
						<input id="status" name="status" type="hidden" value="${auctions.status}"/> </li>
						</ul>
						 
						<div class="bid-sub" style="text-align: center;">
							<a href="javaScript:void(0);" style="display:inline-block" name="sure"><input type="submit" value="确定修改" class="btn-r big-btn" /></a>
							<a  href="${contextPath}/s/auction/query" style="display:inline-block;margin-left:10px"><input type="button" class="btn-g big-btn" value="取消" /></a></div>
						</div>
			</form:form>
				<!--page-->
			</div>

		</div>
		<%@ include
			file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
	<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
	<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
	
 	<script src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
	<script type="text/javascript">
		 var isCeiling = "${auctions.isCeiling}";
		 var isSecurity = "${auctions.isSecurity}";
		 
		 var paramData={
			contextPath:"${contextPath}",
			cookieValue:"<c:out value="${cookie.SESSION.value}"/>",
 		 }
		 
		 laydate.render({
	   		 elem: '#startTime',
	   		 type:'datetime',
	   		 calendar: true,
	   		 theme: 'grid',
	   		 trigger: 'click'
	   	  });
	   	   
	   	  laydate.render({
	   	     elem: '#endTime',
	   	     type:'datetime',
	   	     calendar: true,
	   	     theme: 'grid',
	   		 trigger: 'click'
	      });
	</script>
 	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/auction/updateAuction.js'/>"></script>
</body>
</html>
