<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.legendesign.net/tags" prefix="ls"%>
<%
  request.setAttribute("contextPath", request.getContextPath());
%>

<table class="ret-tab sold-table">
	<tr class="tit">
		<th width="10"></th>
		<th colspan="2">商品/订单号/退款号</th>
		<th width="70">退款金额</th>
		<th width="90">买家会员名</th>
		<th width="120">申请时间</th>
		<th width="80">处理状态</th>
		<th width="80">平台确认</th>
		<th width="90" style="border-right: 1px solid #e4e4e4">操作</th>
    </tr>
    <c:choose>
          <c:when test="${empty requestScope.list}">
             <tr>
		 	 <td colspan="20" style="border-right: 1px solid #e4e4e4">
		 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
		 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
		 	 </td>
		 </tr>
          </c:when>
          <c:otherwise>
          	 <c:forEach items="${requestScope.list }" var="refundReturn">
	    		<tr class="detail">
					<td width="10"></td>
					<c:if test="${refundReturn.subItemId ne 0}">
						<td width="75">
							<div class="pic-thumb">
								<a href="${contextPath }/views/${refundReturn.productId}" target="_blank">
									<img src="<ls:images item='${refundReturn.productImage}' scale='3'/>"/>
								</a>
							</div>
						</td>
					</c:if>
					<td colspan="${refundReturn.subItemId ne 0?1:2}" style="text-align: left;padding-right: 10px;">
						<dl class="goods-name">
						    <dt>
						    	<c:if test="${refundReturn.productId ne 0}">
						    		<a href="${contextPath }/views/${refundReturn.productId}" target="_blank">${refundReturn.productName }</a>
						    	</c:if>
						    	<c:if test="${refundReturn.productId eq 0}">
						    		<a href="${contextPath }/s/orderDetail/${refundReturn.subNumber }" target="_blank">${refundReturn.productName }</a>
						    	</c:if>
						    </dt>
				        	<dd>订单编号：<a href="${contextPath }/s/orderDetail/${refundReturn.subNumber}" target="_blank">${refundReturn.subNumber }</a></dd>
				        	<dd>退款编号：${refundReturn.refundSn }</dd>
				        </dl>
				    </td>
				    <td>
				    	<c:choose>
				    		<c:when test="${refundReturn.isRefundDeposit eq true}"><!-- 退定金 -->
				    			¥${refundReturn.refundAmount + refundReturn.depositRefund}
				    		</c:when>
				    		<c:otherwise>
				    			¥${refundReturn.refundAmount}
				    		</c:otherwise>
				    	</c:choose>
				    </td>
				    <td>${refundReturn.userName}</td>
				    <td><fmt:formatDate value="${refundReturn.applyTime}" pattern="yyyy-MM-dd HH:mm" /></td>
				    <td>
				    	<c:if test="${refundReturn.sellerState eq 1 && refundReturn.applyState eq 1}">
			    			<span class="col-orange">待审核</span>
				    	</c:if>
				    	<c:if test="${refundReturn.sellerState eq 1 && refundReturn.applyState eq -1}">
				    		已撤销
				    	</c:if>
				    	<c:if test="${refundReturn.sellerState eq 2}">
				    		同意
				    	</c:if>
				    	<c:if test="${refundReturn.sellerState eq 3}">
				    		不同意
				    	</c:if>
				    </td>
				    <td>
					    <c:choose>
				    		<c:when test="${refundReturn.applyState eq 2}">
			    				待确认
			    			</c:when>
			    			<c:when test="${refundReturn.applyState eq 3}">
			    				已完成
			    			</c:when>
			    			<c:otherwise>
			    				无
			    			</c:otherwise>
					    </c:choose>
				    </td>
				    <td >
				    	<c:choose>
				    		<c:when test="${refundReturn.sellerState eq 1 && refundReturn.applyState eq 1}">
				    			<a href="${contextPath }/s/refundDetail/${refundReturn.refundId}" class="btn">处理</a>
				    		</c:when>
				    		<c:otherwise>
				    			<a href="${contextPath }/s/refundDetail/${refundReturn.refundId}" class="btn">查看</a>
				    		</c:otherwise>
				    	</c:choose>
				    </td>
				</tr>
	    	</c:forEach>
          </c:otherwise>
     </c:choose>
</table>
<div class="clear"></div>
<!-- 页码条 -->
  <div style="margin-top:10px;" class="page clearfix">
	 <div class="p-wrap">
		<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
	 </div>
</div>