<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

<div class="tab-con"> 
	<c:forEach items="${requestScope.list}" var="productComment">
	    <div class="comment-item"> 
	     <div class="user-column"> 
	      <div class="user-info">
		      <c:choose>
		      	<c:when test="${empty productComment.portrait }">
			       <img src="${contextPath }/resources/templets/images/sculpture.png" width="25" height="25" alt="${productComment.userName }" class="avatar" />
			    </c:when>
			    <c:otherwise>
			    	<img src="<ls:images item='${productComment.portrait }' scale='1'/>" width="25" height="25" alt="${productComment.userName }" class="avatar" />
			    </c:otherwise>
		      </c:choose> 
	       	  ${productComment.userName }
	      </div> 
	      <div class="user-level"> 
	      	<c:choose>
	      		<c:when test="${productComment.gradeName eq '皇冠会员'}">
	      			<span style="color:##e1a10a">${productComment.gradeName }</span> 
	      		</c:when>
	      		<c:otherwise>
	      			<span style="color:#666">${productComment.gradeName }</span> 
	      		</c:otherwise>
	      	</c:choose>
	      </div> 
	     </div> 
		 
	     <div class="comment-column J-comment-column"> 
	      
		  <!-- 评论分数 -->
		  宝贝评分：<div style="display:inline-block;" class="comment-star star${productComment.score }"></div> 
	      
		  <!-- 评论内容 -->
		  <p class="comment-con">${productComment.content }</p> 
		  
		  <!-- 评论图片列表 -->
		  <c:if test="${not empty productComment.photoPaths}">
		      <div class="pic-list J-pic-list"> 
		      	<c:forEach items="${productComment.photoPaths}" var="photo">
				   <a class="J-thumb-img" href="#none" data="<ls:images item='${photo }' scale='0'/>"><img src="<ls:images item='${photo }' scale='1'/>" width="48" height="48" alt="" /></a> 
				</c:forEach>
			  </div>
		  	  <div class="J-pic-view-wrap clearfix">
		  	  	<img src="" style="margin-left: 0px; margin-top: 0px;">
		  	  </div>
		  </c:if>
		  
		  <!-- 大图预览区域-->
		  
		  <c:if test="${productComment.isReply}">
			  <div class="reply-box"> 
				 <div class="reply-con"> 
				  <span class="u-name">掌柜回复：</span> 
				  <span class="u-con">${productComment.shopReplyContent}</span> 
				 </div> 
				 <div class="reply-meta">
				   <fmt:formatDate value="${productComment.shopReplyTime}"  pattern="yyyy-MM-dd" />
				 </div> 
			  </div>
		  </c:if>
		  
		  <c:if test="${productComment.isAddComm and productComment.addStatus eq 1}">
		  
		  	  <div class="append-info">
		  	  	<c:choose>
		  	  		<c:when test="${productComment.appendDays eq 0}">用户当天追加评论</c:when>
		  	  		<c:otherwise>用户${productComment.appendDays}天后追加评论</c:otherwise>
		  	  	</c:choose>
		  	  </div>
		  	  
		  	  <!-- 评论内容 -->
			  <p class="comment-con">${productComment.addContent }</p> 
			  
			  <!-- 评论图片列表 -->
			  <c:if test="${not empty productComment.addPhotoPaths}">
			      <div class="pic-list J-pic-list"> 
			      	<c:forEach items="${productComment.addPhotoPaths}" var="photo">
					   <a class="J-thumb-img" href="#none" data="<ls:images item='${photo }' scale='0'/>"><img src="<ls:images item='${photo }' scale='1'/>" width="48" height="48" alt="" /></a> 
					</c:forEach>
				  </div>
				  <!-- 大图预览区域-->
		  	  	  <div class="J-pic-view-wrap clearfix">
		  	  	  	<img src="" style="margin-left: 0px; margin-top: 0px;">
		  	  	  </div>
			  </c:if>
			  <c:if test="${productComment.addIsReply}">
				  <div class="reply-box"> 
					 <div class="reply-con"> 
					  <span class="u-name">掌柜回复：</span> 
					  <span class="u-con">${productComment.addShopReplyContent}</span> 
					 </div> 
					 <div class="reply-meta">
					   <fmt:formatDate value="${productComment.addShopReplyTime}"  pattern="yyyy-MM-dd" />
					 </div> 
				  </div>
			  </c:if>
		  </c:if>
		  
		  <!-- 其他信息 -->
	      <div class="comment-message"> 
	       <div class="buy-info">
	       	<c:if test="${not empty productComment.attribute }">
	       		<span>${productComment.attribute }</span>
	       	</c:if>
	        <span>购买时间: <fmt:formatDate value="${productComment.buyTime }"  pattern="yyyy-MM-dd HH:mm" /></span> 
	        <span>评论时间: <fmt:formatDate value="${productComment.addtime }"  pattern="yyyy-MM-dd HH:mm" /></span> 
	       </div> 
		   
		   <!-- 点赞和回复按钮-->
	       <div class="comment-op"> 
	        <a class="J-nice" prodCommId="${productComment.id }">
	        	<i class="sprite-praise"></i><span class="num">${productComment.usefulCounts }</span>
	        </a> 
	        <a href="${contextPath }/productcomment/productCommentDetail?prodComId=${productComment.id }" target="_blank">
	        	<i class="sprite-comment"></i><span class="num">${productComment.replayCounts }</span>
	        </a> 
	       </div> 
	      </div> 
	     </div> 
	</div>
	</c:forEach>
</div>

<div class="clear"></div>
<div style="margin-top:10px;" class="page clearfix">
   <div class="p-wrap">
      	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple" actionUrl="javascript:commentPager"/> 
	</div>
</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/commonUtils.js'/>" /></script>
<script type="text/javascript">
	
	/** 选中图片 */
	$(".J-thumb-img").click(function(){
		var $this = $(this);
		
		$this.siblings().removeClass("current");
		$this.addClass("current");
		
		var imagePath = $this.attr("data");
	    var imageView = $this.parent().next(".J-pic-view-wrap").find("img");
		imageView.attr("src", imagePath);
		
		return false;
	});
	
	/** 取消选中评论图片 */
	$(".J-pic-view-wrap img").on("click", function(){
		var $this = $(this);
		$this.attr("src", "");
		
		var a = $this.parent().prev(".J-pic-list").find(".J-thumb-img");
		a.removeClass("current");
		a.siblings().removeClass("current");
		return false;
	});
	
	/** 点赞 */
	$(".J-nice").on("click", function(){
	
		if(isLogin()){
			login();
			return false;
		}
		
		var $this = $(this);
		var usefulCountSpan = $this.find(".num");
		var usefulCount = parseInt(usefulCountSpan.text());
		
		var prodComId = $this.attr("prodCommId");
		
		var url = contextPath + "/productcomment/updateUsefulCounts";
		var params = {"prodComId": prodComId};
		$.ajaxUtils.sendPost(url, params, function(result){
			if(result === "OK"){
				usefulCount++;
				usefulCountSpan.text(usefulCount);
				layer.msg("恭喜您, 点赞成功!");
			}else{
				layer.msg(result);
			}
		});
		return false;
	});
</script>