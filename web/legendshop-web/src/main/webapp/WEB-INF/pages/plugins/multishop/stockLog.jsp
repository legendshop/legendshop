<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>${prodName} - 库存记录</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css"
          href="<ls:templateResource item='/resources/templets/css/multishop/seller-prod${_style_}.css'/>"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
          href="<ls:templateResource item='/resources/plugins/showLoading/css/showLoading.css'/>"/>
    <style type="text/css">
        .seller-selling table {
            font-size: 13px;
            margin: 10px;
            border: 1px solid #ddd;
            border-bottom: 0px;
            width: 956px;
        }

        .seller-selling table tbody tr {
            height: 70px;
        }

        .seller-selling table tbody tr td {
            border-bottom: 1px solid #ddd;
            text-align: center;
            padding: 5px;
            font-size: 12px;
        }

        .seller-selling table thead tr td {
            border-bottom: 1px solid #ddd;
            text-align: center;
            padding: 5px;
            background: #f9f9f9;
        }
    </style>
</head>
<body class="graybody">
<div id="doc">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
    <div class="w1190">
        <div class="seller-cru">
            <p>
                您的位置>
                <a href="${contextPath}/home">首页</a>>
                <a href="${contextPath}/sellerHome">卖家中心</a>>
                <a href="${contextPath}/s/sellingProd">出售中的商品</a>>
                <span class="on">库存记录</span>
            </p>
        </div>
        <%@ include file="included/sellerLeft.jsp" %>
        <div class="seller-selling">
            <table>
                <thead>
                <tr>
                    <td width="50"><input type="checkbox" id="checkAll"/></td>
                    <td width="600">名称</td>
                    <td width="350">属性</td>
                    <td width="200">价格</td>
                    <td width="200">销售库存</td>
                    <td width="200">实际库存</td>
                    <td width="350">操作</td>
                </tr>
                </thead>
                <tbody>
                <c:if test="${empty requestScope.listSku}">
                    <tr>
                        <td colspan="7" style="text-align: center;">没有找到符合条件的商品</td>
                    </tr>
                </c:if>
                <c:forEach items="${requestScope.listSku}" var="sku">
                    <tr>
                        <td style="text-align: center;" class="sid"><input class="skuid" value="${sku.skuId}" type="checkbox"/></td>
                        <td style="text-align: center;">${sku.name}</td>
                        <td style="text-align: center;">${sku.cnProperties}</td>
                        <td style="text-align: center;"><font color="red">￥${sku.price}</font></td>
                        <td style="text-align: center;" class="stocks">${sku.stocks}</td>
                        <td style="text-align: center;" class="actualStocks">${sku.actualStocks}</td>
                        <td style="text-align: center;">
                            <button style="margin-right: 8px" class="btn-r"
                                    onclick="stockEdit(this)">修改库存
                            </button>
                            <button class="btn-r" onclick="loadStockLog('${prodId}','${sku.skuId}')">查看历史</button>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>

                <c:if test="${totalSku >7}">
                    <tr>
                        <td colspan="7">
                            <div style="margin-top:10px;" class="page clearfix">
                                <div class="p-wrap">
                                    <ls:page pageSize="${pageSizeSku}" total="${totalSku}" curPageNO="${curPageNOSku}"
                                             type="simple" actionUrl="javascript:skuPager"/>
                                </div>
                            </div>
                        </td>
                    </tr>
                </c:if>
            </table>
            <button style="margin:10px 0px 0px 10px" class="btn-g" onclick="batchEdit();">批量修改库存</button>
            <button style="margin:10px 0px 0px 810px" class="btn-r"><a style="color:white" href="/s/sellingProd">返回</a>
            </button>
            <div id="stoackLogPanl">

            </div>
        </div>
    </div>
    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<script charset="utf-8"
        src="<ls:templateResource item='/resources/plugins/showLoading/js/jquery.showLoading.min.js'/>"></script>
<script type="text/javascript" language="javascript"
        src="<ls:templateResource item='/resources/templets/js/multishop/stockLog.js'/>"></script>
<script type="text/javascript">
    var contextPath = "${contextPath}";
    var prodId = "${prodId}";
</script>
</body>
</html>