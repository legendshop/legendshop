<!DOCTYPE html>
	<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
	<%@ include file='/WEB-INF/pages/frontend/templetGoat/common/taglib.jsp' %>
	<%@ include file='/WEB-INF/pages/frontend/templetGoat/common/common.jsp' %>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<title>我的积分</title>
	<link rel="stylesheet" href="<ls:templateResource item='/resources/templets/css/userIntegral.css'/>">
</head>
<body>

<div class="fanhui_cou">
	<div class="fanhui_1"></div>
	<div class="fanhui_ding">顶部</div>
</div>
	<div class="w768">
		<header>
			<div class="back">
				<a href="${contextPath}/p/userhome">
					  <img src="<ls:templateResource item='/resources/templets/images/back.png'/>" alt=""> 
				</a>
			</div>
			<p>我的积分</p>
		</header>
		
<!-- 头部 -->
		<div class="my-poi">
			<p>当前积分：<span>${score}</span><b>分</b></p>
			<table cellspacing="0" cellpadding="0" id="integralTab">
				<tr>
					<td class="tim">时间</td>
					<td>积分</td>
					<td>备注</td>
				</tr>
			
				<c:forEach items="${requestScope.list}" var="log">
				   <tr class="gra">
					<td><fmt:formatDate value="${log.addTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<td>${log.integralNum}</td>
					<td>${log.logDesc}</td>
				   </tr>
				 </c:forEach>
			
			</table>
			
			<input type="hidden" id="pageCount" name="pageCount"  value="${pageCount}"/>
			<input type="hidden" id="curPageNO" name="curPageNO"  value="${curPageNO}"/>	

	   </div>
	</div>
	   <!-- loading  -->
<section class="loading" id="loading" style="transform-origin: 0px 0px 0px;opacity: 1;transform: scale(1, 1);">
</section>
<!-- 我的积分 -->
<%@ include file='/WEB-INF/pages/frontend/templetGoat/common/footer.jsp' %>
</body>
<script charset="utf-8"src="<ls:templateResource item='/resources/templets/js/userIntegral.js'/>"></script>
<script type="text/javascript">
  var pageContext="${contextPath}";
</script>
</html>