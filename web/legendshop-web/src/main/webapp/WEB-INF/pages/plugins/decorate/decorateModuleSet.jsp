<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/public.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/decorate.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/plugins/colorpicker/css/jquery.bigcolorpicker.css'/>" rel="stylesheet" />

<style>
.black_overlay{ z-index:100}
.white_content{ z-index:101}
.white_box{padding-top: 20px;}
.ip100 {
    width: 169px !important;
}
</style>
<div class="white_box">

   <form:form  action="${contextPath}/s/shopDecotate/layout/decorateModuleSetSave" name="theForm"  method="post"  enctype="multipart/form-data" id="theForm" >
		<input type="hidden" value="loyout_module_custom_prod" name="layoutModuleType" id="layoutModuleType">
	    <input type="hidden" value="${layoutParam.layoutId}" name="layoutId" id="layoutId">
	    <input type="hidden" value="${layoutParam.layout}" name="layout" id="layout">
        <input type="hidden" value="${fontImg}" name="fontImg" id="fontImg">
      <div class="fiit_module_edit">
    	<table cellspacing="0" cellpadding="0">
        	<tbody>
            	<tr>
                	<td width="40%" valign="top" align="right">标题名称：</td>
                     <td align="left"><input type="text"  value="${empty title?'商品列表':title}" name="title" id="title" class="ip100" maxlength="25"></td> 
                 </tr>
                <tr>
                	<td valign="top" align="right">标题背景：</td>
                     <td align="left">
                     <input type="file" name="file" id="file" class="ip100">
                     <img width="100px" height="25px" style="display:none" id="font_img_show">
                     <img src="<ls:photo item='${fontImg}' />" style="display:none" id="waiting"></td>
                </tr>
                 <tr>
                	<td width="40%" valign="top" align="right">标题颜色：</td>
                     <td align="left"><input type="text" value="${fontColor}" name="fontColor" id="fontColor" class="ip100"> 
                     <span style="background-color:" class="color_block"></span></td> 
                 </tr>
                 <tr>
                	<td valign="top" align="right">背景颜色：</td>
                    <td align="left"><input type="text" value="${backColor}" name="backColor" onblur="changeColor(this);" id="backColor" class="ip100">
                    <span style="background-color:" class="color_block"></span></td>
                     </tr>
              </tbody>
        </table>
     </div>
     <div class="fiit_save_box">
     <input type="button" onclick="decorateModuleSetSave();" id="save" value="保存">
  </div>
   </form:form>
 </div>
 <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
  <script src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>" type="text/javascript"></script>
  <script src="<ls:templateResource item='/resources/plugins/colorpicker/js/jquery.bigcolorpicker.min.js'/>" type="text/javascript"></script>
  
  <script>
    var contextPath = '${contextPath}';
    //背景颜色	
    $(document).ready(function(){
       $("#fontColor").bigColorpicker(function(el,color){
	    jQuery(el).val(color);
	    jQuery(el).parent().find(".color_block").css("background-color",color);
	   });
	
		$("#backColor").bigColorpicker(function(el,color){
		    jQuery(el).val(color);
		    jQuery(el).parent().find(".color_block").css("background-color",color);
		});
		
		var fontColor="${fontColor}";
		var backColor="${backColor}";
		if(fontColor!=""){
			$("#fontColor").parent().find(".color_block").css("background-color",fontColor);
		}
		if(backColor!=""){
			$("#backColor").parent().find(".color_block").css("background-color",backColor);
		}
	    
    });						
	
	
	function changeColor(_this){
	   var color=$(_this).val();
	   if(color==null || color=="" || color==undefined){
		return ;
	    }
	    $("#fontColor").parent().find(".color_block").css("background-color",color);
     }

	
  
    function decorateModuleSetSave(){
           var filepath = jQuery("input[name='file']").val();
           if(filepath!="" && filepath!=null ){
               var extStart = filepath.lastIndexOf(".");
			   var ext = filepath.substring(extStart, filepath.length).toUpperCase();
			   if (ext != ".BMP" && ext != ".PNG" && ext != ".GIF" && ext != ".JPG"
					&& ext != ".JPEG") {
				 parent.layer.alert("图片限于bmp,png,gif,jpeg,jpg格式", {icon: 0});
				 return false;
			   }
           }else{
              jQuery("input[name='file']").remove();
           }
           var title = $("#title").val();
           if(title==null || title=="" || title==undefined){
        	   layer.msg("请填写标题名称",{icon:0});
        	   return;
           }
           if(title.length > 20){
        	   layer.msg("标题名称过长，最大为20个字符",{icon:0});
        	   return;
           }
          $("#theForm").ajaxForm().ajaxSubmit({
			   success:function(result) {
			      var data=eval(result);
			      if(data=="OK"){
			           parent.window.location.reload();
			           var index = parent.layer.getFrameIndex('decorateModuleSet'); //先得到当前iframe层的索引
			           parent.layer.close(index); //再执行关闭   

			          return true;
			      }else{
			          $("#font_img_show").parent().append("<input type='file' class='ip100' id='file' name='file'>");
			          parent.layer.alert("修改失败", {icon: 2});
			          return false;
			      }
			   },
			   error:function(XMLHttpRequest, textStatus,errorThrown) {
				 return false;
			  }	
	      });
	       
    }
    
    
  
  </script>
