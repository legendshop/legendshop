<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<html>
<head>
<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
<title>文章列表-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}"/>
<meta name="description" content="${systemConfig.description}" />
</head>
<body  class="graybody">
<div id="bd">
<%@include file='home/top.jsp'%>
       <!----两栏---->
         <div style="padding-top:10px;" class="yt-wrap"> 
             <div class="leftnews">
           		 <%@ include file="newsCategory.jsp"%>
            </div>
             <div class="right_newscon">
             
                 <div class="news_bor">
                 <c:forEach items="${requestScope.list}" var="news" varStatus="status">
                 	 <p class="new_title">
                 	 <c:choose>
 						<c:when test="${news.highLine == 1}"> 
 							<span class="ntitleHighLine"></c:when>
 						<c:otherwise><span class="ntitle"></c:otherwise>
 					</c:choose>
                       <span class="ntitle"><a href="${contextPath}/news/${news.newsId}">${news.newsTitle}</a></span>
                       <span class="ntime">作者：${news.userName}  <span class="ndate"><fmt:formatDate value="${news.newsDate}" pattern="yyyy-MM-dd"/></span></span>
                   </p>
                    <p class="nabs">${news.newsBrief}</p>
                 </c:forEach>
	                 <div style="margin-top:10px;" class="page clearfix">
			     			 <div class="p-wrap">
			            		 <c:if test="${toolBar!=null}">
										<span class="p-num">
											<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
										</span>
									</c:if>
			     			 </div>
	  				</div>
                 </div>
             </div>
            <div class="clear"></div>
         </div>
   </div>
   </body>
   <script type="text/javascript">
		function pager(curPageNO){
			window.location.href="${contextPath}/allnews?curPageNO="+curPageNO;
		}
		
		function changePager(curPageNO){
			document.getElementById("curPageNO").value=curPageNO;
			document.getElementById("form1").submit();
		}
</script>
</html>