<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
 <%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/public.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/decorate.css'/>" rel="stylesheet" />
<script src="<ls:templateResource item='/resources/common/js/ajaxfileupload.js'/>" type="text/javascript"></script>
 <script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/templets/js/multishop/decorateHot.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/templets/js/multishop/decorateHotMaps.js'/>" type="text/javascript"></script>
<style>
  .map-position-bg .this {
    background-color: #000000 !important;
    border-color: #000000;
 }


</style>

<c:set value="${hots.dataDtos}" var="dataDtos"></c:set>
<div class="white_box" style="padding-top: 10px;">
<div class="fiit_hot_box">
	 <div class="fiit_hot_hd">
        <div class="fiit_hot_tab">
        	<ul>
            	<li tar="img" class="this"><a href="javascript:void(0);">图片设置</a></li>
                <li tar="hotspot" class=""><a href="javascript:void(0);">热点设置</a></li>
            </ul>
        </div>
        <div id="tar_img" style="display: block;">
    	<dl class="fiit_hot_upload">
        	<dt>背景图片：</dt>
            <dd>
            	<div class="file_box">
                     <input type="text" name="img_show" id="img_show" class="ip180">  
                     <input type="button" value="浏览..." class="btn">
                     <input type="file" name="bg_img" id="bg_img" class="file">
                </div>
            </dd>
             <dd>
            	图片尺寸不限制,大小不能超过5M
            </dd>
            
        </dl>
        </div>
        <div id="tar_hotspot" style="display: none;">
           <div class="add_hotpints">
        	<a id="add_hotspot" href="javascript:void(0);" class="add_hotpints_btn"><b>+</b>新增热点</a>
            <ul id="hotspots">
              <%--  <c:forEach items="${dataDtos}" var="data" varStatus="s">
                   <li mark="${s.count}"><a href="javascript:void(0);">热点${s.count}</a><i onclick="cancleHotspot(${s.count});"></i></li>
               </c:forEach> --%>
            </ul>
           </div>
           <dl class="fiit_hot_upload">
        	<dt>热点<b id="url_count">1</b>地址：</dt>
            <dd>
                <input type="text" placeholder="http://" class="ip300" name="hotspot_url" id="hotspot_url">
                <input type="hidden" value="" name="apply_mark" id="apply_mark">
                
                 <input type="button" onclick="selectShopCoupon();" value="选择购物券" class="hot_btn">
                 <input type="button" onclick="applyHotspot();" value="应用" class="hot_btn">
            </dd>
           </dl>
       </div>
    </div>
    <div style="clear:both;"></div>
     
     <div class="fiit_hot_img" id="imgMap">
	     <span>
	        <c:if test="${not empty hots.imageFile}">
	           <img  ref="imageMaps"  id="${hots.imageFile}" usemap="#Map"  src="<ls:photo item='${hots.imageFile}' />">
	        </c:if>
	        <c:if test="${not empty hots.dataDtos}">
			  <map   name="Map">
			     <c:forEach items="${hots.dataDtos}" var="data" varStatus="s">
			         <area count="${s.count}" shape="rect" coords="${data.coords}" href="${data.hotspotUrl}" target="_blank">
			     </c:forEach>
			  </map> 
			</c:if>
	     </span>
    </div>
</div>

  <div class="fiit_save_box">
   <input type="hidden" value="loyout_module_hot" name="layoutModuleType" id="layoutModuleType">
    <input type="hidden" value="${layoutParam.layoutId}" name="layoutId" id="layoutId">
    <input type="hidden" value="${layoutParam.layout}" name="layout" id="layout">
    <input type="hidden" value="${layoutParam.divId}" name="divId" id="divId">
    <input type="hidden" value="${layoutParam.layoutDiv}" name="layoutDiv" id="layoutDiv">
    <input type="hidden" value="${hots.hotDatas}" name="hotDatas" id="hotDatas">
    <input type="hidden" value="${hots.hotId}" name="hotId" id="hotId">
    <input type="button" id="save" onclick="save_form();" value="保存">
  </div></div>

<script type="text/javascript">
	var contextPath = '${contextPath}';
	var photoPath ="<ls:photo item='' />";
    $(function(){
         var array=new Array();
	    <c:forEach items="${dataDtos}" var="data" varStatus="s">
	       var _obj=new Object();
	       _obj.x1="${data.x1}";
	        _obj.y1="${data.y1}";
	         _obj.x2="${data.x2}";
	          _obj.y2="${data.y2}";
	          _obj.hotspotUrl="${data.hotspotUrl}";
	          array.push(_obj);
	    </c:forEach>
    
		$('#imgMap').imageMaps();
	});
	

    
</script>
