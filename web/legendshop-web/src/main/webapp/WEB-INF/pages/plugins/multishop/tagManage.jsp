<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>标签管理-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-prod${_style_}.css'/>" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">标签管理</span>
				</p>
			</div>
				<%@ include file="included/sellerLeft.jsp" %>
			<div class="seller-selling">
				<form:form action="${contextPath}/s/tagManage" method="get" id="from1">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
						<div class="selling-sea">
							<p>标签名称：</p>
							<input type="text" value="${prodTag.name}" name="name"  id ="name">
							<input type="button" onclick="search()" class="serach" id="btn_keyword" value="搜  索" >	
						</div>						
				</form:form>									
				
<!-- 搜索 -->
				<table class="selling-table">
					<tr class="selling-tit">
						<td class="check"><input type="checkbox" id="checkbox" onclick="selAll();"></td>
						<td>标签名</td>
						<td>右上标签图片</td>
						<td>下横标签图片</td>
						<td>状态</td>							
						<td>操作</td>
					</tr>
					<c:if test="${empty requestScope.list}">
						<tr><td colspan='7' height="60">没有找到符合条件的商品</td></tr>
					</c:if>				
					<c:forEach items="${requestScope.list}" var="tag" varStatus="status">
						<tr class="detail">
							<td class="check">
                         		<input name="strArray" type="checkbox" value="${tag.id}"  arg="">
							</td>
							<td>${tag.name}</td>
							<td>
								<c:if test="${tag.rightTag eq '' }">
									您木有上传图片噢！
								</c:if>
								<c:if test="${tag.rightTag ne '' }">
									<img src="<ls:photo item='${tag.rightTag}'/>" height="50" width="50"/>
								</c:if>
							</td>	 
							<td>
								<c:if test="${tag.bottomTag eq '' }">
									您木有上传图片噢！
								</c:if>
								<c:if test="${tag.bottomTag ne '' }">
									<img src="<ls:photo item='${tag.bottomTag}'/>" height="30" width="200"/>
								</c:if>							
							</td>
							<c:if test="${tag.status == 1}">
								<td>上线</td>							
							</c:if>
							<c:if test="${tag.status == 0}">
								<td>下线</td>						
							</c:if>
								
							<td>
								<ul class="mau" style="width: 180px;">
								    <li><a href="${contextPath}/s/updateProdTagUI/${tag.id}" title="修改" target="_blank" class="bot-gra">修改</a></li>
								    <c:choose>								    
                      				 <c:when test="${tag.status eq 1}">
                         				  <li><a href="javascript:void(0);" onclick="pullOff(this);" id="${tag.id}"  name="${tag.name}"  status="${tag.status}" title="下线" class="bot-gra">下线</a></li>									      
									      <%-- <li><a href='javascript:confirmDelete("${product.prodId}","${product.name}")' title="删除" class="bot-gra">删除</a></li> --%>
                       				 </c:when>
                      				 <c:otherwise>
                      				 	<li><a href="javascript:void(0);" onclick="pullOff(this);" id="${tag.id}"  name="${tag.name}"  status="${tag.status}" title="上线" class="bot-gra">上线</a></li>
                        				 <%-- <li><a href="${contextPath}/views/${product.prodId}" title="查看详情" target="_blank" class="bot-gra">查看详情</a></li> --%>
                       				 </c:otherwise>
                   				   </c:choose>
                   				   <li><a href="${contextPath}/s/bindProdManage/${tag.id}" title="绑定管理" class="bot-gra">绑定管理</a></li>
								</ul>
							</td>	
						</tr>
					</c:forEach>
				</table>
				<div class="selling-cha">
					<span><a class="bat-oper-btn" href="javascript:void(0);" onclick="deleteAction();">删除<img src="<ls:templateResource item='/resources/templets/images/delete.png'/>" alt=""></a></span>					
					<span><a class="bat-oper-btn" href="javascript:void(0);" onclick="batchOffline();">下线<img src="<ls:templateResource item='/resources/templets/images/distance.png'/>" alt=""></a></span>
					<span><a class="bat-oper-btn" href="${contextPath}/s/saveProdTagUI">添加新标签 </a></span>
				</div>
				<div class="clear"></div>
					<div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
				            		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
				     			 </div>
		  				</div>
			</div>
		</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/prodTag.js'/>"></script>
	<script type="text/javascript">
		var contextPath="${contextPath}";
		function search(){
		  	$("#from1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		  	$("#from1")[0].submit();
		}
	</script>
</body>
</html>