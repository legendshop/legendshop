<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
	<title>评价详情-${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="home/top.jsp"%>
		<div id="bd">
 <!--crumb-->
	<div id="J_crumbs" class="crumb yt-wrap">
		<div class="crumbCon">
			<div class="crumbSlide">
				<div class="crumbs-nav-item one-level">
					<a class="crumbs-link" href="<ls:url address="/"/>"> 首页 </a><i class="crumbs-arrow">&gt;</i>
				</div>
				<div class="crumbs-nav-item one-level">
					<a class="crumbs-link" href="<ls:url address="/productcomment/query/${prod.id}"/>"> 该商品所有评论 </a><i class="crumbs-arrow">&gt;</i>
				</div>
				<div class="crumbs-nav-item one-level">
					<a class="crumbs-link" href="<ls:url address="/productcomment/load/${prod.id}/${productComment.subItemId}"/>"> 商品评论 </a><i class="crumbs-arrow"></i>
				</div>
				
			</div>
		</div>
	</div>
	<!--crumb end-->
			
 <div class="w main" style="width:1190px;margin:auto;">
        <div class="left" style="float:left;border: 1px solid #ddd;background: #fff;width: 230px;">
            <div class="m" id="pinfo" >
                <div class="mt" style="background: #f9f9f9;padding: 10px;">
                    <h2>商品信息</h2>
                </div>
		        <div class="mc" style="padding: 10px 15px;">
					<div class="p-img2" style="text-align: center;"><a href="${contextPath}/views/${prod.id}" target="_blank">
					   <img alt=""  src="<ls:images item='${prod.pic}' scale="1"/>"  height="150px" width="150px" ></a></div>
				    <div class="clear"></div>
					<div class="p-name" style="margin-top:10px;">商品：<a href="${contextPath}/views/${prod.id}" target="_blank">${prod.name}</a></div>
					<div class="p-price" style="margin-top:5px;">价格：<strong style="color:#e5004f;"><fmt:formatNumber type="currency" value="${prod.cash}" pattern="${CURRENCY_PATTERN}"/></strong></div>
					<div class="p-grade clearfix" style="margin-top:5px;">
						<span class="fl">评价得分：</span>
						<div class="fl">
							<div class="star sa${prod.reviewScores}"></div>(${prod.reviewScores}分)						
						</div>
					</div>
					<div class="num-comment" style="margin-top:5px;">评论数：${prod.comments}条</div>
				</div>
     	</div>
            <!--pinfo end-->
     </div>
        <!--left end-->
    <div class="right-extra" style="float:right;background: #fff;border: 1px solid #ddd;width: 940px;" >
     		<div id="comment-detail" class="m m1">
            <div class="mt" style="background: #f9f9f9;padding: 10px;">
                <h2>评价详情</h2>
            </div>
            <div id="comments-list" class="mc prod-comment" >
                <div class="item">
                    <div class="user">
	                      <div class="icon">  
		                        <a rel="nofollow" title="查看TA的主页" href="#" target="_blank">
		                         	 <c:choose>
											<c:when test="${not empty productComment.photo }">
												<img alt="用户头像" src="<ls:photo item='${productComment.photo}' />"  id="infoimg" style="width: 50px;height :50px;">
											</c:when>
											<c:otherwise>
												<img alt="用户头像" src="${contextPath}/resources/templets/images/no-img_mid_.jpg" id="infoimg" style="width: 50px;height :50px;">
											</c:otherwise>
									</c:choose> 
								</a> 
	                       </div>
                       		<div class="u-name"> <a href="#" rel="nofollow" target="_blank"> ${productComment.userName}</a></div>
                       		<%--<span class="u-level"><span style="color:">${productComment.gradeName}</span></span>
                    --%></div>
                    <div class="i-item">
                        <div class="o-topic">
                             <strong class="topic"><a href="#" target="_blank"></a></strong>
                        	 <span class="star sa${productComment.score}"></span> 
                        	 <span class="date-comment"><fmt:formatDate value="${productComment.addtime}" pattern="yyyy-MM-dd HH:mm:ss"  /></span>
                        </div>
                        <div class="comment-content">
                            <dl>
                                <dt>心　　得：</dt>
                                <dd>${productComment.content}</dd>
                            </dl>             
                        </div>
                       <div class="btns">
							<a class="btn-reply" href="#none" title="0"
								onclick="replay('${productComment.id}');">回复(<span>${productComment.replayCounts}</span>)</a>
							<div class="useful">
								<a name="agree" class="btn-agree" href="#none"
									onclick="agree(${productComment.usefulCounts},'${productComment.id}');">有用(<span
									id="useful_${productComment.id}">${productComment.usefulCounts}</span>)</a>
							</div>
						</div>
						 <div class="comment-content">
                            <dl>
                                <ul class="smallPic">
                                	<c:if test="${not empty productComment.photoFile1}">
                                		<li style="border: 2px solid #f2f2f2;float: left;"><img src=<ls:photo item='${productComment.photoFile1 }'/>/></li>
                                	</c:if>
                                	<c:if test="${not empty productComment.photoFile2}">
                                		<li style="border: 2px solid #f2f2f2;float: left;"><img src=<ls:photo item='${productComment.photoFile2 }'/>/></li>
                                	</c:if>
                                	<c:if test="${not empty productComment.photoFile3}">
                                		<li style="border: 2px solid #f2f2f2;float: left;"><img src=<ls:photo item='${productComment.photoFile3 }'/>/></li>
                                	</c:if>
                                	<c:if test="${not empty productComment.photoFile4}">
                                		<li style="border: 2px solid #f2f2f2;float: left;"><img src=<ls:photo item='${productComment.photoFile4 }'/>/></li>
                                	</c:if>
                                	<c:if test="${not empty productComment.photoFile5}">
                                		<li style="border: 2px solid #f2f2f2;float: left;"><img src=<ls:photo item='${productComment.photoFile5 }'/>/></li>
                                	</c:if>
								</ul>
                            </dl>             
                        </div>
                          
                     <div id="replyItemList">
				<div class="item-reply reply-lz  hide"
					id="replyItem_${productComment.id}">
					<div class="reply-list">
						<!-- 弹出回复层 -->
						<div class="replay-form">
							<div class="arrow">
								<em>◆</em><span>◆</span>
							</div>
							<div class="reply-wrap">
								<p>
									<em>回复</em> <span class="u-name">
										${productComment.userName} ：</span>
								</p>
								<div class="reply-input">
									<div class="fl">
										<input type="text" id="replyInput">
									</div>
									<a href="#none" class="reply-btn btn-gray"
										onclick="replyMessage('${productComment.userName}','${productComment.id}',this);">回复</a>
									<div class="clr"></div>
								</div>
							</div>
						</div>
						<!-- 弹出回复层 -->
					</div>
				</div>
				<c:forEach items="${requestScope.list}" var="productReply" varStatus="status">
						<div class="item-reply" id="replyItem${productReply.id}" onmouseover="showReplayBtn(${productReply.id});" onmouseout="hideReplayBtn(${productReply.id});">
							<strong>${lastOffset-status.index}</strong>
							<div class="reply-list">
								<div class="reply-con">
									<span class="u-name"> <a rel="nofollow" href="javascript:void(0);" target="_blank">${productReply.replyName}</a> <em>回复</em> <c:choose>
											<c:when test="${productReply.parentReplyName !=null}">
												<a rel="nofollow" target="_blank" href="javascript:void(0);">${productReply.parentReplyName}</a>：
                                                	</c:when>
											<c:otherwise>
												<a rel="nofollow" target="_blank" href="javascript:void(0);">${productReply.commName}</a>：
                                                	</c:otherwise>
										</c:choose> </span> <span class="u-con">${productReply.replyContent}</span>
								</div>
								<div class="reply-meta">
									<span class="reply-left fl" style="float: left;"><fmt:formatDate value="${productReply.replyTime}" pattern="yyyy-MM-dd HH:mm:ss" />
									</span> <a style="visibility: hidden;" href="javascript:void(0);" id="replayBtn${productReply.id}" onclick="replayBtn(this);">回复</a>
								</div>
								<div class="replay-form hide">
									<div class="arrow">
										<em>◆</em><span>◆</span>
									</div>
									<div class="reply-wrap">
										<p>
											<em>回复</em> <span class="u-name">${productReply.replyName}：</span>
										</p>
										<div class="reply-input">
											<div class="fl">
												<input value="" type="text">
											</div>
											<a href="javascript:void(0);" class="reply-btn btn-gray"
												onclick="replyParent('${productReply.replyName}','${productReply.replyId}','${productReply.id}','${productReply.prodcommId}',this);">回复</a>
											<div class="clr"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</c:forEach>
			</div>
                        
                     <div class="clear"></div>
			      	<div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
				            		 <c:if test="${toolBar!=null}">
											<span class="p-num">
												<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
											</span>
										</c:if>
				     			 </div>
		  				</div>	

                    </div>
                    <div class="corner tl"></div>
                </div>
             </div>
        </div>   
    </div>
    <div class="clear"></div>
 </div>

 </div>
		<%@ include file="home/bottom.jsp"%>
	</div>
	 <script type="text/javascript">
	 var contextPath = '${contextPath}';
 function pager(curPageNO){
	 window.location.href = "${contextPath}/productcomment/load/"+${prod.id}+"/"+${productComment.subItemId}+"?curPageNO="+curPageNO ;
 }
 </script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/productComment.js'/>"></script>
</body>
</html>