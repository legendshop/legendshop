<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>账户安全 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/securityCenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/validateChanges.css'/>" rel="stylesheet" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller.css'/>" rel="stylesheet"/>

</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;">     
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con"  id="content">
		<div class="right">
				<div class="o-mt">
						<h2>修改登录密码</h2>
				</div>
				<div id="step1" class="step step01">
						<ul>
								<li class="fore1">1.验证身份<b></b></li>
								<li class="fore2">2.修改登录密码<b></b></li>
								<li class="fore3">3.完成</li>
						</ul>
				</div>
				<form:form  id="passwordForm1" method="post" >
				<div class="m m1 safe-sevi">
						<div class="mc">
								<div class="form">
										<div class="item">
											<span class="label">请输入登录密码：</span>
											<div class="fl">
												<input class="text" tabindex="1"  name="oldPassword" id="password" type="password" maxlength="20">
												<div class="clr"></div>
												<div id="password_error" class="msg-error"></div>
											</div>
											<div class="clr"></div>
										</div>
										<div class="item">
											<span class="label">验证码：</span>
											<div class="fl">
												<input type="hidden" id="cannonull" name="cannonull" value='<fmt:message key="randomimage.errors.required"/>'/>
												<input type="hidden" id="charactors4" name="charactors4" value='<ls:i18n key="randomimage.charactors.required" length="4"/>'/> 
												<input type="hidden" id="errorImage" name="errorImage" value='<fmt:message key="error.image.validation"/>'/> 
												
												<input class="text" tabindex="2" name="randNum" id="randNum" type="text" maxlength="4">
												<label><img id="randImage" name="randImage" src="<ls:templateResource item='/validCoderRandom'/>"  style="vertical-align: middle;"/>看不清？
            									<a href="javascript:void(0)" onclick="javascript:changeRandImg('${contextPath}')" style="font-weight: bold;"><fmt:message key="change.random2"/></a>
            									</label>
												<div class="clr"></div>
												<div id="authCode_error" class="msg-error"></div>
											</div>
											<div class="clr"></div>
										</div>
										<div class="item">
											<span class="label">&nbsp;</span>
											<input id="signedData" style="display:none" type="text">
											<input id="serialNumber" style="display:none" type="text">
											<div class="fl">
												<a id="passworda" class="btn-r small-btn" href="javascript:void(0);" onclick='javascript:passwordVerification.validCode("${toPage}");'><s></s>提交</a>
												<a class="btn-r small-btn" href="${contextPath}/p/security"><s></s>返回</a>
											</div>
											<div class="clr"></div>
										</div>
								</div>
						</div>
				</div>
				</form:form>
				<div class="m m7">
    				<div class="mt"><h3>为什么要进行身份验证？</h3></div>
    				<div class="mc">
    					<div>1. 为保障您的账户信息安全，在变更账户中的重要信息时需要进行身份验证，感谢您的理解和支持。</div>
    					<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
    					<div>2. 验证身份遇到问题？请发送用户名、手机号、历史订单发票至<a href="javascript:void(0);" class="flk-05">${systemConfig.supportMail}</a>，客服将尽快联系您。</div>
    				</div>
    			</div>
		</div>
	</div>	

   
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script type="text/javascript"  src="<ls:templateResource item='/resources/common/js/randomimage.js'/>"></script>
<script type="text/javascript">
	$(document).ready(function() {
		passwordVerification.doExecute('${toPage}', '${userSecurity.mailVerifn}', '${userSecurity.phoneVerifn}', '${userSecurity.paypassVerifn}');
	});
	
</script>
</body>
</html>