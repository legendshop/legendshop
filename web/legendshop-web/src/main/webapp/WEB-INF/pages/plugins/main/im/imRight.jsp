<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>

<div class="right-sidebar">
	<div class="sid-tab clear">
		<a href="javascript:void(0)" class="tab-item current"><span class="item-span" data-name="imConsult">正在咨询</span></a>
		<a href="javascript:void(0)" class="tab-item"><span class="item-span" data-name="imOrder">我的订单</span></a>
		<a href="javascript:void(0)" class="tab-item"><span class="item-span" data-name="imShopDetail">店铺信息</span></a>
		<!-- <a href="javascript:void(0)" class="tab-item"><span class="item-span" data-name="imProblem">常见问题</span></a> -->
	</div>
	<!-- 正在咨询 -->
	<div class="tab-con tab-show rightContext">
	</div>
	<!-- /正在咨询 -->
</div>
