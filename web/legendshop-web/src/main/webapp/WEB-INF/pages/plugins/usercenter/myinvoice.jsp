<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>我的发票 - ${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
<div id="bd">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>

    <!----两栏---->
    <div class=" yt-wrap" style="padding-top:10px;">
        <%@include file='usercenterLeft.jsp' %>

        <!-----------right_con-------------->
        <div class="right_con">

            <div class="o-mt"><h2>我的发票</h2></div>

            <div class="pagetab2">
                <ul>
                    <li class="on"><span>发票列表</span></li>
                    <li><a href="${contextPath}/p/invoice/invoiceList">开票信息</a></li>
                </ul>
            </div>
            <form:form action="${contextPath }/p/invoice/myInvoice" method="get" id="order_search">
                <input type="hidden" value="1" name="curPageNO">
                <div class="form search-01" style=" margin-top:15px;display:none">
                    <div class="item">
                        <div class="fl fore"></div>
                        <div class="fr">
                            订单类型 <select name="sub_type" class="sele">
                            <option value="">所有类型</option>
                            <option value="NORMAL" <c:if test="${sub_type == 'NORMAL'}">selected="selected"</c:if>>普通订单</option>
                            <option value="GROUP" <c:if test="${sub_type == 'GROUP'}">selected="selected"</c:if>>团购订单</option>
                            <option value="SECKILL" <c:if test="${sub_type == 'SECKILL'}">selected="selected"</c:if>>秒杀订单</option>
                            <option value="AUCTIONS" <c:if test="${sub_type == 'AUCTIONS'}">selected="selected"</c:if>>拍卖订单</option>
                            <option value="SHOP_STORE" <c:if test="${sub_type == 'SHOP_STORE'}">selected="selected"</c:if>>门店订单</option>
                        </select>
                            订单状态&nbsp;&nbsp;<select name="state_type" class="sele">
                            <option value="">所有订单</option>
                            <option value="1" <c:if test="${state_type==1}">selected="selected"</c:if>>待付款</option>
                            <option value="2" <c:if test="${state_type==2}">selected="selected"</c:if>>待发货</option>
                            <option value="3" <c:if test="${state_type==3}">selected="selected"</c:if>>待收货</option>
                            <option value="4" <c:if test="${state_type==4}">selected="selected"</c:if>>已完成</option>
                            <option value="5" <c:if test="${state_type==5}">selected="selected"</c:if>>已取消</option>
                        </select>
                            下单时间&nbsp;&nbsp;<input style="width:90px;height:15px;" type="text" value='<fmt:formatDate value="${startDate}" pattern="yyyy-MM-dd" />' id="startDate" name="startDate" class="text"
                                                    readonly="readonly">
                            -
                            <input style="width:90px;height:15px;" type="text" value='<fmt:formatDate value="${endDate}" pattern="yyyy-MM-dd" />' id="endDate" name="endDate"  class="text" readonly="readonly">
                            订单号123&nbsp;&nbsp;<input type="text" value="${order_sn}" name="order_sn" class="text">
                            <input type="button" onclick="search()" id="search_order" class="bti" value="查 询">
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </form:form>

            <!-------------订单---------------->
            <div id="recommend" class="m10 recommend" style="display: block;border-width:1px 0px;">
               <%-- <%@ include file="orderInvoice.jsp" %>--%>
                <table class="ncm-default-table order sold-table" cellpading=0 cellspacing="0">
                    <thead>
                    <tr>
                        <th class="w10"></th>
                        <th colspan="2">商品</th>
                        <th class="w90">单价（元）</th>
                        <th class="w90">数量</th>
                        <th class="w90">售后维权</th>
                        <th class="w110">订单金额</th>
                        <th class="w90">交易状态</th>
                        <th class="w120 last-td">交易操作</th>
                    </tr>
                    </thead>
                    
                     <c:choose>
					 	<c:when test="${empty requestScope.list}">
						 	 <tbody>
						 	 	<tr>
						 	 		<td colspan="20">
						 	 			<!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
						 	 			<div class="warning-option"><!-- <i></i> --><span>没有符合条件的信息</span></div>
						 	 		</td>
						 	 	</tr>
					        </tbody>
					 	</c:when>
					 	<c:otherwise>
					 		<c:forEach items="${requestScope.list}" var="order" varStatus="orderstatues">
		                        <tbody class="pay">
		                        <tr>
		                            <td class="sep-row" colspan="19"></td>
		                        </tr>
		                        <tr>
		                            <td class="pay-td" colspan="19">
		                                <%-- <input type="checkbox" style="margin-left:10px;" value="${order.subId}" class="selector"
		                                       <c:if test="${order.status!=1}">disabled="disabled" </c:if> > --%>
		                                <c:choose>
		                                    <c:when test="${order.subType == 'NORMAL'}">
		                                        <span style="margin-left: 35px;">普通订单</span>
		                                    </c:when>
		                                    <c:when test="${order.subType == 'SECKILL'}">
		                                        <span style="margin-left: 35px;">秒杀订单</span>
		                                    </c:when>
		                                    <c:when test="${order.subType == 'AUCTIONS'}">
		                                        <span style="margin-left: 35px;"><img src="${contextPath}/resources/templets/images/auctions_order.png"/>&nbsp;&nbsp;拍卖订单</span>
		                                    </c:when>
		                                    <c:when test="${order.subType == 'GROUP'}">
		                                        <span style="margin-left: 35px;">团购订单</span>
		                                    </c:when>
		                                    <c:when test="${order.subType == 'SHOP_STORE'}">
		                                        <span style="margin-left: 35px;">门店订单</span>
		                                    </c:when>
		                                </c:choose>
		                                <c:choose>
		                                    <c:when test="${order.status==1 }">
		                                        <c:choose>
		                                            <c:when test="${order.subType == 'SHOP_STORE' && order.payManner==2}">
		                                            </c:when>
		                                            <c:when test="${order.subType == 'SHOP_STORE' && order.payManner==3}">
		                                            </c:when>
		                                            <c:otherwise>
		                                                <a target="_blank" href="<ls:url address="/p/orderSuccess?subNums=${order.subId}" />" class="ncbtn ncbtn-bittersweet fr mr15"><i class="icon-shield"></i>订单支付</a>
		                                            </c:otherwise>
		                                        </c:choose>
		                                    </c:when>
		                                </c:choose>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td colspan="19" style="text-align: left;"><span class="ml10"> <!-- order_sn -->
									订单号：${order.subNum} </span> <!-- order_time --> <span>下单时间： <fmt:formatDate value="${order.subDate}" type="both"/></span> <!-- store_name --> <span>
							</span> <!-- QQ -->
		
		                            </td>
		                        </tr>
		
		                        <!-- S 商品列表 -->
		                        <c:set value="0" var="notCommedCount"/>
		                        <c:forEach items="${order.subOrderItemDtos}" var="orderItem" varStatus="orderItemStatues">
		                            <c:if test="${orderItem.commSts==0}">
		                                <c:set value="${notCommedCount + 1}" var="notCommedCount"/>
		                            </c:if>
		                            <tr>
		                                <td class="bdl">&nbsp;</td>
		                                <td class="w70">
		                                    <div class="ncm-goods-thumb">
		                                        <a target="_blank"
		                                           href="<ls:url address='/views/${orderItem.prodId}'/>">
		                                            <img onmouseout="toolTip()" onmouseover="toolTip();"
		                                                 src="<ls:images item='${orderItem.pic}' scale='3'/>">
		
		                                        </a>
		                                    </div>
		                                </td>
		                                <td class="tl">
		                                    <dl class="goods-name2">
		                                        <dt>
		                                            <a target="_blank"
		                                               href="<ls:url address='/views/${orderItem.prodId}'/>">${orderItem.prodName}</a>
		                                            <c:if test="${not empty orderItem.snapshotId && orderItem.snapshotId!=0}">
												<span class="rec">
												<c:if test="${order.subType ne 'AUCTIONS' && order.subType ne 'SHOP_STORE' && order.subType ne 'SECKILL'}">
		                                            <a href="<ls:url address='/snapshot/${orderItem.snapshotId}'/>" target="_blank">[交易快照]</a>
		                                        </c:if>
											    </span>
		                                            </c:if>
		                                        </dt>
		                                        <c:if test="${not empty orderItem.attribute}">
		                                            <dd>${orderItem.attribute}</dd>
		                                        </c:if>
		                                        <!-- 消费者保障服务 -->
		                                    </dl>
		                                </td>
		                                <td>
		                                    <p> ${orderItem.cash}</p>
		                                    <c:if test="${not empty orderItem.promotionInfo}">
		                                        <span class="sale-type">${orderItem.promotionInfo}</span>
		                                    </c:if>
		                                    <c:if test="${orderItem.refundState == 2}">
		                                        <p style="color:#69AA46;">${orderItem.refundAmount}(退)</p>
		                                    </c:if>
		                                </td>
		                                <td>x${orderItem.basketCount} </td>
		                                <td>
		                                    <c:if test="${order.actualTotal > 0}">
		                                    <c:choose>
		                                    <c:when test="${order.status gt 2 &&  order.status le 4 &&  orderItem.refundState le 0 }">
		                                        <!-- 订单处于 待发货、已完成状态 并且 没有处理过退款或者退款失败的 -->
		                                        <p><a target="_blank" href="${contextPath}/p/return/apply/${order.subId}/${orderItem.subItemId}?subNum=${order.subNum}">退款/退货</a></p>
		                                    </c:when>
		                                    <c:when test="${orderItem.refundType==1 && (orderItem.refundState==1 || orderItem.refundState==2)}">
		                                    <p><a target="_blank" href="${contextPath}/p/refundDetail/${orderItem.refundId}">查看退款</a>
		                                        </c:when>
		                                        <c:when test="${orderItem.refundType==2 && (orderItem.refundState==1 || orderItem.refundState==2)}">
		                                    <p><a target="_blank" href="${contextPath}/p/returnDetail/${orderItem.refundId}">查看退货</a>
		                                        </c:when>
		                                        </c:choose>
		                                        </c:if>
		                                            <%--  <p><a target="_blank" href="${contextPath}">交易投诉</a></p> --%>
		                                </td>
		
		
		                                <!-- <td><span class="goods-type">限时折扣</span></td>
		                                    <td>退款 投诉</td> -->
		
		
		                                <!-- S 合并TD -->
		                                <c:if test="${orderItemStatues.index==0}">
		                                    <c:choose>
		                                        <c:when test="${fn:length(order.subOrderItemDtos)>0}">
		                                            <!-- 未支付 -->
		                                            <td rowspan="${fn:length(order.subOrderItemDtos)}" class="bdl">
		                                        </c:when>
		                                        <c:otherwise>
		                                            <td class="bdl">
		                                        </c:otherwise>
		                                    </c:choose>
		                                    <p class="">
		                                        <strong>${order.actualTotal}</strong>
		                                    </p>
		                                    <p class="goods-freight"><c:if test="${not empty order.freightAmount}">(含运费${order.freightAmount})</c:if></p>
		                                    <p title="支付方式：">
		                                        <c:choose>
		                                        <c:when test="${order.payManner==1}">
		                                       		 货到付款
		                                        </c:when>
		                                        <c:when test="${order.payManner==2}">
		                                       		 在线支付
		                                        </c:when>
		                                        <c:when test="${order.payManner==2}">
		                                    		    门店支付
		                                        </c:when>
		                                        </c:choose>
		                                    </td>
		                                    <c:choose>
		                                        <c:when test="${fn:length(order.subOrderItemDtos)>0}">
		                                            <!-- 未支付 -->
		                                            <td rowspan="${fn:length(order.subOrderItemDtos)}" class="bdl">
		                                        </c:when>
		                                        <c:otherwise>
		                                            <td class="bdl">
		                                        </c:otherwise>
		                                    </c:choose>
		                                    <p>
		                                        <c:choose>
		                                            <c:when test="${order.status==1 }">
		                                                <c:choose>
		                                                    <c:when test="${order.subType == 'SHOP_STORE' && order.payManner==2}">
		                                                  	     <span class="a-block col-orange">门店付款自提</span>
		                                                    </c:when>
		                                                    <c:when test="${order.subType == 'SHOP_STORE' && order.payManner==3}">
		                                                       	 <span class="a-block col-orange">门店付款自提</span>
		                                                    </c:when>
		                                                    <c:otherwise>
		                                                     	 <span class="a-block col-orange">待付款</span>
		                                                    </c:otherwise>
		                                                </c:choose>
		                                            </c:when>
		                                            <c:when test="${order.status==2 }">
		                                                <c:choose>
		                                                    <c:when test="${order.subType == 'SHOP_STORE' && order.payManner==2}">
		                                                   		<span class="a-block col-orange"> 待提货</span>
		                                                    </c:when>
		                                                    <c:otherwise>
		                                             		    <span class="a-block col-orange">待发货</span>
		                                                    </c:otherwise>
		                                                </c:choose>
		                                            </c:when>
		                                            <c:when test="${order.status==3 }">
		                                   			    <span class="a-block col-orange">待收货</span>
		                                            </c:when>
		                                            <c:when test="${order.status==4 }">
		                                    		            已完成
		                                            </c:when>
		                                           <c:when test="${order.status==5 }">
		                                       		            交易关闭
		                                            </c:when> 
		                                        </c:choose>
		
		                                    </p>
											<p>
												<c:if test="${order.refundState == 0 && (order.status==2 || order.status==3 || order.status==4)}">
													<c:if test="${order.hasInvoice eq 0 && order.isNeedInvoice == 'true'}">
			                                        	已申请发票
		                                       		</c:if>
		                                       		<c:if test="${order.hasInvoice eq 1}">
			                                        	已开发票
		                                       		</c:if>
												</c:if>
											</p>
		                                    </td>
		                                    <c:choose>
		                                        <c:when test="${fn:length(order.subOrderItemDtos)>0}">
		                                            <td rowspan="${fn:length(order.subOrderItemDtos)}" class="bdl bdr">
		                                        </c:when>
		                                        <c:otherwise>
		                                            <td class="bdl bdr">
		                                        </c:otherwise>
		                                    </c:choose>
		                                    <c:if test="${order.refundState == 0 && (order.status==2 || order.status==3 || order.status==4)}">
		                                        <c:if test="${order.hasInvoice eq 0 && order.isNeedInvoice == 'false'}">
			                                        <a class="ncbtn btn-r" herf="#" onclick="getInvoice('${order.subNum}');">
			                                            <i class="icon-ban-circle"></i> 补开发票
			                                        </a>
			                                        
		                                        </c:if>
		                                    </c:if>
		                                    <%-- ${order.status} --%>
		                                    </td>
		                                    <!-- E 合并TD -->
		                                </c:if>
		
		                            </tr>
		
		
		                        </c:forEach>
		
		
		                        </tbody>
		                   
		                    </c:forEach>
					 	</c:otherwise>
					 </c:choose>
                </table>
                <div style="margin-top:10px;" class="page clearfix">
                    <div class="p-wrap">
                        <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
                    </div>
                </div>
            </div>

        </div>
        <!-----------right_con end-------------->


        <div class="clear"></div>
    </div>
    <!----两栏end---->

    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<!--bd end-->
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/ToolTip.js'/>"></script>
<script type="text/javascript">
    var path = "${contextPath}";
    var failedOwnerMsg = '<fmt:message key="failed.product.owner" />';
    var failedBasketMaxMsg = '<fmt:message key="failed.product.basket.max" />';
    var failedBasketErrorMsg = '<fmt:message key="failed.product.basket.error" />';

    $(document).ready(function () {
        userCenter.changeSubTab("myinvoice");
        
        laydate.render({
     	   elem: '#startDate',
     	   calendar: true,
     	   theme: 'grid',
		   trigger: 'click'
     	  });
     	   
     	 laydate.render({
     	   elem: '#endDate',
     	   calendar: true,
     	   theme: 'grid',
		   trigger: 'click'
         });
    });

	//补开发票
	function getInvoice(subNumber) {

		layer.confirm("将使用默认发票信息补开发票?", {
			icon: 3
			,btn: ['确定','取消'] //按钮
		}, function(index){
			data = {"subNumber": subNumber};
			$.ajax({
				"url": contextPath + "/p/invoice/getInvoice",
				type: 'post',
				dataType: 'json',
				data: data,
				async: false,
				success: function (result) {
					if (result.status == 1) {
						layer.msg('发票申请成功', {icon: 1,time: 1000}, function(){
							window.location.href = path + "/p/invoice/myInvoice";
						});
					}
					if (result.status == 0) {
						layer.alert(result.msg,{icon: 2})
					}
				}
			});
			layer.close(index); //再执行关闭
		});


	}
</script>
</body>
</html>