
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ taglib uri="http://www.legendesign.net/date-util"  prefix="dateUtil" %>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}"  pattern="yyyy-MM-dd HH:mm" var="nowDate"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>商品订单-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-sold${_style_}.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">商品订单</span>
				</p>
			</div>
			<%@ include file="included/sellerLeft.jsp" %>
			
			<!-- 搜索订单模块 start -->
			<div class="seller-sold">
				<div class="seller-com-nav">
					<ul>
						<li <c:if test="${empty paramDto.status}">class="on"</c:if>  ><a href="<ls:url address="/s/ordersManage"/>">所有订单</a></li>
                      <li <c:if test="${paramDto.status==1}">class="on"</c:if>  ><a href="<ls:url address="/s/ordersManage?status=1"/>">已经提交</a></li>
                      <li <c:if test="${paramDto.status==2}">class="on"</c:if>  ><a href="<ls:url address="/s/ordersManage?status=2"/>">已经付款</a></li>
                      <li <c:if test="${paramDto.status==3}">class="on"</c:if>  ><a href="<ls:url address="/s/ordersManage?status=3"/>">已经发货</a></li>
                      <li <c:if test="${paramDto.status==4}">class="on"</c:if>  ><a href="<ls:url address="/s/ordersManage?status=4"/>">已经完成</a></li>
                      <li <c:if test="${paramDto.status==5}">class="on"</c:if>  ><a href="<ls:url address="/s/ordersManage?status=5"/>">已经取消</a></li>
					</ul>
				</div>
				<form:form id="ListForm" method="get"  action="${contextPath}/s/ordersManage" >
					<input type="hidden" value="${paramDto.curPageNO==null?1:paramDto.curPageNO}" id="curPageNO" name="curPageNO">
		            <input type="hidden" value="${paramDto.status}" id="status" name="status">
					<input type="hidden" value="${paramDto.shopId}"  name="shopId">
					<div class="sold-ser clearfix" style="margin:15px 0;">
						<span class="item">
							下单时间：<input type="text" class="item-inp"  value='<fmt:formatDate value="${paramDto.startDate}" pattern="yyyy-MM-dd" />'  readonly="readonly"  id="startDate" name="startDate"  class="hasDatepicker" >
							&nbsp;—&nbsp;<input type="text"  class="item-inp" value='<fmt:formatDate value="${paramDto.endDate}" pattern="yyyy-MM-dd" />' id="endDate" name="endDate" class="text  hasDatepicker" readonly="readonly">
						</span>
						<span class="item">
							付款时间：<input type="text"  class="item-inp" value='<fmt:formatDate value="${paramDto.payStartDate}" pattern="yyyy-MM-dd" />'  readonly="readonly"  id="payStartDate" name="payStartDate"  class="hasDatepicker" >
							&nbsp;—&nbsp;<input type="text" class="item-inp" value='<fmt:formatDate value="${paramDto.payEndDate}" pattern="yyyy-MM-dd" />' id="payEndDate" name="payEndDate" class="text  hasDatepicker" readonly="readonly">
						</span>
						<span class="item">
							订单编号：<input type="text" class="item-inp"  id="subNumber" value="${paramDto.subNumber}"  name="subNumber" style="width: 200px;">
						</span>
						<span class="item">
							订单类型：
							<select id="subType" name="subType" class="item-sel" >
								<option value="">全部</option>
								<option value="NORMAL" <c:if test="${paramDto.subType eq 'NORMAL'}">selected = "selected"</c:if>>普通订单</option>
								<option value="GROUP" <c:if test="${paramDto.subType eq 'GROUP'}">selected = "selected"</c:if>>团购订单</option>
								<option value="AUCTIONS" <c:if test="${paramDto.subType eq 'AUCTIONS'}">selected = "selected"</c:if>>拍卖订单</option>
								<option value="SECKILL" <c:if test="${paramDto.subType eq 'SECKILL'}">selected = "selected"</c:if>>秒杀订单</option>
								<option value="MERGE_GROUP" <c:if test="${paramDto.subType eq 'MERGE_GROUP'}">selected = "selected"</c:if>>拼团订单</option>
								<option value="PRE_SELL" <c:if test="${paramDto.subType eq 'PRE_SELL'}">selected = "selected"</c:if>>预售订单</option>
								<option value="SHOP_STORE" <c:if test="${paramDto.subType eq 'SHOP_STORE'}">selected = "selected"</c:if>>门店订单</option>
							</select>
						</span>
						<span class="item">
							支付状态：
							<select id="isPayed" name="isPayed" class="item-sel">
								<option value="">全部</option>
								<option value="1" <c:if test="${paramDto.isPayed eq 1}">selected = "selected"</c:if>>已支付</option>
								<option value="0" <c:if test="${paramDto.isPayed eq 0}">selected = "selected"</c:if>>待支付</option>
							</select>
						</span>
						<span class="item">
							维权状态：
							<select id="refundStatus" name="refundStatus" class="item-sel">
								<option value="">全部</option>
								<option value="1" <c:if test="${paramDto.refundStatus eq 1}">selected = "selected"</c:if>>退货/退款处理中</option>
								<option value="2" <c:if test="${paramDto.refundStatus eq 2}">selected = "selected"</c:if>>退货/退款完成</option>
							</select>
						</span>
						<span class="item">
							备注状态：
							<select id="isShopRemak" name="isShopRemak" class="item-sel">
								<option value="">全部</option>
								<option value="0" <c:if test="${paramDto.isShopRemak eq 0}">selected = "selected"</c:if>>未备注</option>
								<option value="1" <c:if test="${paramDto.isShopRemak eq 1}">selected = "selected"</c:if>>已备注</option>
							</select>
						</span>
						<span class="item">
							买家电话：
							<input type="text" value="${paramDto.reciverMobile}"  id="reciverMobile" name="reciverMobile" class="item-inp" style="width: 150px;">
							
						</span>
						<span class="item">
							买家：
							<input type="text" value="${paramDto.reciverName}"  id="reciverName" name="reciverName" class="item-inp" style="width: 150px;">
							<input type="button" onclick="search()" value="搜索订单" class="btn-r">
							<c:if test="${!hideAction}">
								<input type="button" onclick="exportOrder()" value="导出搜索数据" class="btn-r">
							</c:if>
						</span>
					</div>
				</form:form> 
				<!-- 搜索订单模块 end -->
				
				<table class="sold-table">
					<thead>
						<tr class="sold-tit">
							<th colspan="2" width="300">商品</th>
							<th width="90px;">总价（元）</th>
							<th width="35">数量</th>
							<th width="100">买家</th>
							<th width="100">订单金额</th>
							<th width="100">交易状态</th>
							<th width="100">交易操作</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
				           	<c:when test="${empty requestScope.list}">
				              <tr>
							 	 <td colspan="20">
							 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
							 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
							 	 </td>
							 </tr>
				           	</c:when>
				           <c:otherwise>
				           	<c:forEach items="${requestScope.list}" var="order" varStatus="orderstatues">
								<tbody class="sub">
								<tr>
									<td colspan="8" class="table-space"></td>
								</tr>
								<tr class="num-tim">
									<td colspan="8">
										<span>订单编号：${order.subNum}</span>
										<span>下单时间：<fmt:formatDate value="${order.subDate}" type="both" /></span>
										<c:choose>
											<c:when test="${order.subType == 'AUCTIONS'}"><span>订单类型：拍卖订单</span></c:when>
											<c:when test="${order.subType == 'SHOP_STORE'}"><span>订单类型：门店订单</span></c:when>
									        <c:when test="${order.subType == 'SECKILL'}"><span>订单类型：秒杀订单</span></c:when>
									        <c:when test="${order.subType == 'GROUP'}"><span>订单类型：团购订单</span></c:when>
									        <c:when test="${order.subType == 'MERGE_GROUP'}"><span>订单类型：拼团订单</span></c:when>
									        <c:when test="${order.subType == 'PRE_SELL'}"><span class="presellType">订单类型：预售订单</span></c:when>
									        <c:otherwise><span>订单类型：普通订单</span></c:otherwise>
										</c:choose>
										<!-- 客服入口  -->
										<span style="margin-left: 15px;">
											<a target="_blank" style="color: #006dc7;" href="${contextPath}/customerService/conversation?contactId=${order.userId}">联系用户</a>
										</span>
										<%--<c:if test="${order.status==3|| order.status==4}">--%>
							                 <%--<span class="fr" style="margin: 0 20px 0 0;">--%>
								              <%--<a title="打印发货单" href="javascript:void(0)" class="printInvoice" data-subId="${order.subId}" data-dvyTypeId="${order.dvyTypeId}">--%>
												<%--打印发货单</a>--%>
									         <%--</span>--%>
							            <%--</c:if>--%>
							      		<span class="fr" style="margin: 0 20px 0 0;">
							              <a title="打印订单" target="_blank" href="<ls:url address="/s/orderPrint/${order.subNum}"/>">
											打印订单</a>
							         	</span>
									</td>
								</tr>
								
								<c:forEach items="${order.subOrderItemDtos}" var="orderItem" varStatus="orderItemStatues">
									<tr class="detail" >
										<td>
											<a href="<ls:url address='/views/${orderItem.prodId}'/>" target="_blank">
												<img alt="" src="<ls:images item='${orderItem.pic}' scale='3'/>">
											</a>
										</td>
										<td class="a-left">
											<a href="<ls:url address='/views/${orderItem.prodId}'/>"   target="_blank"  >
												<span style="word-break: break-all; max-width:300px">${orderItem.prodName}</span>
											</a>
											<!-- 所有订单类型都要有交易快照 所以注释掉这个if  -->
											<%-- <c:if test="${order.subType ne 'AUCTIONS' && order.subType ne 'SHOP_STORE' && order.subType ne 'SECKILL'}"> --%>
												<c:if test="${not empty orderItem.snapshotId and orderItem.snapshotId gt 0}">
													<a href="<ls:url address='/snapshot/${orderItem.snapshotId}'/>" target="_blank">
														<span class="table-btn-r"> [交易快照]</span>
													</a>
												</c:if>
											<%-- </c:if> --%>
											<br>
											<%-- ${orderItem.attribute} --%>
										</td>
										<td><fmt:formatNumber pattern="0.00" value="${orderItem.cash}" />
											<p style="background-color:#ff875a;color: #FFF;">${orderItem.promotionInfo}</p>
											<c:if test="${orderItem.refundState == 2}">
									         	<p style="color:#69AA46;"><fmt:formatNumber pattern="0.00" value="${orderItem.refundAmount}" />(退)</p>
									        </c:if>
										</td>
										<td>${orderItem.basketCount}</td>
										<c:set var="subItemCount"  value="${fn:length(order.subOrderItemDtos)}"></c:set> 
										<c:if test="${orderItemStatues.index==0}">
											<td class="buyer-td col-bla <c:if test="${subItemCount>1}">b-left</c:if>" rowspan="${subItemCount}">
												 <p>${order.reciverName}</p>
												 <p>${order.reciverMobile}</p>
											</td>
											<td class="<c:if test="${subItemCount>1}">b-left</c:if>" rowspan="${subItemCount}">
												<ul>
													<c:choose>
														<c:when test="${order.refundState == 1 or ( order.status != 4 && order.status != 5 )  }">
															<li class="col-black"><span class="actualTotal"><fmt:formatNumber pattern="0.00" value="${order.actualTotal}" /></span></li>
														</c:when>
														<c:otherwise>
															<li><span class="actualTotal"><fmt:formatNumber pattern="0.00" value="${order.actualTotal}" /></span></li>
														</c:otherwise>
													</c:choose>
													<li>
														 <c:choose>
									                		<c:when test="${not empty order.freightAmount}">
									                		  (含运费<fmt:formatNumber pattern="0.00" value="${order.freightAmount}" />)
									                		</c:when>
									                		<c:otherwise>
									               			（免运费）
									                  		</c:otherwise>
									                 	</c:choose>
													</li>
													<li>${order.payManner==1?"货到付款":"在线支付"}</li>
												</ul>
											</td>
											<td class="<c:if test="${subItemCount>1}">b-left</c:if>" rowspan="${subItemCount}">
												<ul>
													<li class="col-bla">
													 <c:if test="${order.refundState ne 1 }">
														<c:choose>
															 <c:when test="${order.status==1 }">
															 	<c:choose>
															         <c:when test="${order.subType == 'SHOP_STORE'}">
															         	<span class="col-orange">门店待付款</span>
															         </c:when>
															         <c:otherwise>
															         	<span class="col-orange">待付款</span>
															         </c:otherwise>
									     						</c:choose>
															 </c:when>
															 <c:when test="${order.status==2 }">
															       <c:choose>
															         <c:when test="${order.subType == 'SHOP_STORE' && order.ispay eq true}">
															         	<span class="col-orange">待买家提货</span>
															         </c:when>
															         <c:when test="${order.subType == 'SHOP_STORE' && order.ispay eq false}">
															         	<span class="col-orange">待买家付款自提</span>
															         </c:when>
															         <c:otherwise>
																			<c:choose>
																				<c:when test="${order.mergeGroupStatus eq 0}">
																					<span class="col-orange">拼团中</span>
																				</c:when>
																				<c:when test="${order.subType eq 'GROUP' && order.groupStatus eq 0}">
																					<span class="col-orange">拼团中</span>
																				</c:when>
																				<c:otherwise>
																					<span class="col-orange">待发货</span>
																				</c:otherwise>
																			</c:choose>
															         </c:otherwise>
									     						</c:choose>
															 </c:when>
															 <c:when test="${order.status==3 }">
															 	<span class="col-orange">待收货</span>
															 </c:when>
															 <c:when test="${order.status==4 }">
															      已完成
															 </c:when>
															 <c:when test="${order.status==5 }">
															      交易关闭
															 </c:when>
														  </c:choose>
													  </c:if>
													</li>
													<li>
														<c:choose>
															<c:when test="${order.subType eq 'PRE_SELL'}"><!-- 预售订单  -->
																<a href="${contextPath}/s/presellOrderDetail/${order.subNum}">订单详情</a>
															</c:when>
															<c:otherwise><!-- 其他订单  -->
																<a href="${contextPath}/s/orderDetail/${order.subNum}">订单详情</a>
															</c:otherwise>
														</c:choose>
													</li>
													<li>
														<c:choose>
															<c:when test="${order.isShopRemarked eq true }">
																<a style="" href="javascript:void(0)" onclick="readRemark(this);" 
																	shopRemark="${order.shopRemark}" subNumber="${order.subNum}" shopRemarkDate='<fmt:formatDate value="${order.shopRemarkDate}" pattern="yyyy-MM-dd HH:mm:ss" ></fmt:formatDate>'>
																	查看备注
																</a>
															</c:when>
															<c:otherwise>
																<c:if test="${!hideAction}">
																	<a href="javascript:void(0)" onclick="addRemark('${order.subNum}');">
																		添加备注
																	</a>
																</c:if>
															</c:otherwise>
														</c:choose>
													</li>
													<c:if test="${(order.status eq 3 || order.status eq 4 ) && order.subType ne 'SHOP_STORE'}">
													   	<li><a target="_blank" href="${contextPath}/s/orderExpress/${order.subNum}" >查看物流</a></li>  
												    </c:if>
												</ul>
											</td>
											<td  class="<c:if test="${subItemCount>1}">b-left</c:if>" rowspan="${subItemCount}" >
												<ul class="mau">
													<c:if test="${order.refundState != 1 }">
														<c:choose>
														 <c:when test="${order.status==1 }">
														 	<c:choose>
														 		<c:when test="${order.subType eq 'PRE_SELL'}"><!-- 预售订单  -->
														 			<c:if test="${order.isPayDeposit eq 0 }">
																      <li><a class="btn-r" href="javascript:void(0)" onclick="cancleOrder('${order.subNum}');">
																	       取消订单</a></li>
																	   <li><a class="table-btn-g" href="javascript:void(0)" onclick="changeOrderFee('${order.subNum}');">
																	         调整订单</a></li>
															        </c:if>
														 		</c:when>
														 		<c:otherwise>
																    <li><a class="btn-r" href="javascript:void(0)" onclick="cancleOrder('${order.subNum}');">
																	取消订单</a></li>
																	<li><a class="table-btn-g" href="javascript:void(0)" onclick="changeOrderFee('${order.subNum}');">
																	调整订单</a></li>
														 		</c:otherwise>
														 	</c:choose>
														 </c:when>
														 <c:when test="${order.status==2 }">
															 <c:choose>
															 		<c:when test="${order.subType == 'SHOP_STORE'}">
															                            待买家提货
															         </c:when>
																	<c:when test="${order.subType ne 'MERGE_GROUP' && order.subType ne 'GROUP'}">
																			<li>
																				<a class="btn-r" href="javascript:void(0)" onclick="deliverGoods('${order.subNum}');">确认发货</a>
																			</li>
																	</c:when>
															 		<c:when test="${order.subType eq 'GROUP' && order.groupStatus eq 1}">
																		<li>
																			<a class="btn-r" href="javascript:void(0)" onclick="deliverGoods('${order.subNum}');">确认发货</a>
																		</li>
																	</c:when>
																	<c:otherwise>
																		<c:if test="${order.mergeGroupStatus eq 1}">
																				<li>
																					<a class="btn-r" href="javascript:void(0)" onclick="deliverGoods('${order.subNum}');">确认发货</a>
																				</li>
																		</c:if>
																	</c:otherwise>
															 </c:choose>
														 </c:when>
													  </c:choose>
												  </c:if>
												  <c:if test="${order.refundState == 1 }">
												  		<span class="col-orange">退货/退款处理中</span>
												  </c:if>
												  <c:if test="${order.refundState == 2 }">
												  		<span class="col-orange">退货/退款已完成</span>
												  </c:if>
											  </ul>
											</td>
										</c:if>	
									</tr>			
			      				</c:forEach>
			      				
			      				<c:if test="${order.subType == 'PRE_SELL'}"><!-- 预售订单 -->
			      				<tr class="presell-info">
					                <td colspan="2" align="center">阶段1：定金</td>
					                <td colspan="3" align="center">
										<c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
										¥<span class="preDepositPrice"><fmt:formatNumber value="${order.preDepositPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
										(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
										</c:if>
										<c:if test="${order.payPctType eq 1}"><!-- 定金支付 -->
										¥<span class="preDepositPrice"><fmt:formatNumber value="${order.preDepositPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
										</c:if>
					                </td>
					                <c:if test="${order.status eq 5}">
					                	<td colspan="4"></td>
					                </c:if>
					                <c:if test="${order.status ne 5}">
						                <c:if test="${order.isPayDeposit eq 0 }">
							                <c:if test="${dateUtil:getOffsetMinutes(order.subDate,nowTime) le cancelMins}">
						                		<td colspan="2" align="center">用户可付定金时间剩余${cancelMins - dateUtil:getOffsetMinutes(order.subDate,nowTime)}分钟</td>
						                		<td align="center" >买家未付款</td>
						                	</c:if>
						                	<c:if test="${dateUtil:getOffsetMinutes(order.subDate,nowTime) gt cancelMins}">
						                		<td colspan="4" align="center">已过期</td>
						                	</c:if>
						                </c:if>
						                <c:if test="${order.isPayDeposit eq 1 }">
						                	<c:choose>
						                		<c:when test="${order.status eq 1 and order.payPctType eq 1}">
						                			<td colspan="2" align="center" >
									                	买家已付款
									                </td>
									                <td colspan="2" align="center" >
									                	<c:if test="${order.refundState eq 0}">
										                	<a style="color: #006dc7;" href="javascript:void(0)" onclick="shopRefundDeposit('${order.id}', '${order.userId}')">退还定金</a>
									                	</c:if>
									                </td>
						                		</c:when>
						                		<c:otherwise>
						                			<td colspan="4" align="center" >
									                	买家已付款
									                </td>
						                		</c:otherwise>
						                	</c:choose>
						                </c:if>
					                </c:if>
					              </tr>
					              <tr class="presell-info">
					                <td colspan="2" align="center">阶段2：尾款</td>
									  <td colspan="3" align="center">
										  <c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
										  ¥<span class="finalPrice"><fmt:formatNumber value="${order.finalPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
										  </c:if>
										  <c:if test="${order.payPctType eq 1}"><!-- 定金支付 -->
										  ¥<span class="finalPrice"><fmt:formatNumber value="${order.finalPrice+order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber></span>
										  (含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
										  </c:if>
									  </td>
					                <c:if test="${order.status eq 5}">
					                	<td colspan="4"></td>
					                </c:if>
					                <c:if test="${order.status ne 5}">
					                	<c:if test="${order.isPayFinal eq 0 }">
											<c:choose>
												<c:when test="${empty order.finalMStart}">
													<td colspan="4"></td>
												</c:when>
												<c:otherwise>
													<td colspan="2" align="center">
														尾款支付时间: <br /><fmt:formatDate value="${order.finalMStart }" pattern="yyyy-MM-dd HH:mm:ss" /><br /> - <br /><fmt:formatDate value="${order.finalMEnd }" pattern="yyyy-MM-dd HH:mm:ss" />
													</td>
													<td colspan="2" align="center" >
														<c:choose>
															<c:when test="${order.finalMStart gt nowDate }">未开始</c:when>
															<c:when test="${order.finalMStart le nowDate and order.finalMEnd gt nowDate}">买家未付款</c:when>
															<c:when test="${order.finalMEnd le nowDate }">已过期</c:when>
															<c:otherwise>活动已删除</c:otherwise>
														</c:choose>
													</td>
												</c:otherwise>
											</c:choose>
						                </c:if>
						                <c:if test="${order.isPayFinal eq 1}">
					                	  <td colspan="4" align="center" >
						                	 买家已付款
						                	 <c:if test="${order.status eq 2 && order.refundState eq 0}">
							                	 <a style="color: #006dc7;" href="javascript:void(0)" onclick="shopRefundDeposit('${order.id}', '${order.userId}')">主动退款</a>
						                	 </c:if>
						                  </td>
						                </c:if>	
					                </c:if>
					              </tr>

					            </c:if>
								</tbody>
							</c:forEach>
				           </c:otherwise>
			         </c:choose>
				</tbody></table>
				<div class="clear"></div>
				
				<!-- 分页条 -->
				<div style="margin-top:10px;" class="page clearfix">
    			 <div class="p-wrap">
           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
    			 </div>
  				</div>
			</div>
			<form:form id="exportForm" method="get" action="${contextPath}/s/orderMange/export">
				<input name="status" type="hidden" value="${paramDto.status}">
				<input name="shopName" type="hidden" value="${paramDto.shopName}">
				<input name="subNumber" type="hidden" value="${paramDto.subNumber}">
				<input name="userName" type="hidden" value="${paramDto.userName}">
				<%--<input name="startDate" type="hidden" value="${paramDto.startDate}">--%>
				<%--<input name="endDate" type="hidden" value="${paramDto.endDate}">--%>

				<input name="startDate" type="hidden" value='<fmt:formatDate value="${paramDto.startDate}" pattern="yyyy-MM-dd" />'>
				<input name="endDate" type="hidden" value='<fmt:formatDate value="${paramDto.endDate}" pattern="yyyy-MM-dd" />'>

				<input name="payStartDate" type="hidden" value='<fmt:formatDate value="${paramDto.payStartDate}" pattern="yyyy-MM-dd" />'>
				<input name="payEndDate" type="hidden" value='<fmt:formatDate value="${paramDto.payEndDate}" pattern="yyyy-MM-dd" />'>

				<input name="subType" type="hidden" value="${paramDto.subType}">
				<input name="isPayed" type="hidden" value="${paramDto.isPayed}">
				<input name="refundStatus" type="hidden" value="${paramDto.refundStatus}">
				<input name="isShopRemak" type="hidden" value="${paramDto.isShopRemak}">
				<input name="userMobile" type="hidden" value="${paramDto.userMobile}">
				<input name="reciverMobile" type="hidden" value="${paramDto.reciverMobile}">
				<input name="reciverName" type="hidden" value="${paramDto.reciverName}">
			</form:form>           
    </div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type="text/javascript">
		var contextPath="${contextPath}";
		$(function(){
			
			laydate.render({
				   elem: '#startDate',
				   calendar: true,
				   theme: 'grid',
				   trigger: 'click'
			  });
	     
	     	laydate.render({
				   elem: '#endDate',
				   calendar: true,
				   theme: 'grid',
				   trigger: 'click'
			  });
	     	
	     	laydate.render({
				   elem: '#payStartDate',
				   calendar: true,
				   theme: 'grid',
				   trigger: 'click'
			  });
	     	
	     	laydate.render({
				   elem: '#payEndDate',
				   calendar: true,
				   theme: 'grid',
				   trigger: 'click'
			  });
		});
		function search(){
		  	$("#ListForm #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		  	$("#ListForm")[0].submit();
		};
		//导出订单数据
		function exportOrder() {
			$("#exportForm").submit();
		}
	</script>
	<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/ToolTip.js'/>"></script>
	<script src="<ls:templateResource item='/resources/templets/js/orderManage.js'/>" type="text/javascript"></script>
</body>
</html>