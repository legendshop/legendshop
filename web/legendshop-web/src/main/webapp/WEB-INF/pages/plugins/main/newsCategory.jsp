<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<link rel="stylesheet" type="text/css"  href="<ls:templateResource item='/resources/templets/css/newsCategory.css'/>" />

         <div class="uctit m10" id="uc_left_nav">
                 <c:forEach items="${newsCatList}"  var="newsCat">
		         <h3 class="open">
					<div>
					    <a href="${contextPath}/allnews?newsCategoryId=${newsCat.key.key}">${newsCat.key.value}</a>
					</div>
					<ul>
					  <c:forEach var="news" items="${newsCat.value}"> 
		         		 <li><a id="catlist_${news.newsId}" class="news-catlist-a" href='<ls:url address="/news/${news.newsId }"/>'>${news.newsTitle}</a></li>
		              </c:forEach>
					</ul>
				 </h3>
               </c:forEach>	
                
       </div> 
    
    
    
    
    
 <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
 <script type="text/javascript">
    $(document).ready(function() {
	    $(document).delegate("#uc_left_nav h3 div","click", function(){
		var $thisParent = $(this).parent();
		var $thisNext = $(this).next();
		if($thisParent.hasClass("open")){
			$thisParent.removeClass("open");
			$thisNext.hide();
		}else{
			$thisParent.addClass("open");
			$thisNext.show();
		}
	});
    });
 </script>