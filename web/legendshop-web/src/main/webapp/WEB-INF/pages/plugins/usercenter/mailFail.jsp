<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>账户安全 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/securityCenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/validateChanges.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;">     
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con"  id="content">
<div class="right">
			<div class="o-mt">
						<h2>错误页面</h2>
			</div>
	<div class="w" style="background:#fff;"> 
	    <div class="login_left wrap">
	      <div class="news_wrap">
	         <div class="news_bor" id="failpage">
				    <div class="mc" style="height: 300px;">
	  					<i class="reg-error"></i>
						
	        			<div class="reg-tips-info" style="color: #CC0000;margin-top: 100px;font-weight: bold;">抱歉，验证链接已失效！</div>
	       				<div class="reg-nickname-tips"> 您可以前往<a href="${contextPath}/p/security" style="color: #005EA7;">安全中心</a>重新输入。</div>
	   				 </div> 
	        </div>      
	      </div>                                  
	    </div>
	    <!----左边end---->
	    
	   <div class="clear"></div>
	</div>
</div> 
</div>

 <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
</body>
</html> 