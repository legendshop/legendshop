<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/photoOverlay.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/plugins/jcrop/jquery.Jcrop.css'/>" rel="stylesheet" />
<script src="<ls:templateResource item='/resources/plugins/jcrop/jquery.Jcrop.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/ajaxfileupload.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/templets/js/photoOverlay.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
<script type="text/javascript">
	var contextPath = '${contextPath}';
	var photoPath = "<ls:photo item='' />";
</script>

<form:form  id="savePhotoForm" method="post" enctype="multipart/form-data">
	<div class="perfect">
		<div class="perfect_top">
			<img
				src="${contextPath}/resources/templets/images/perfect_top.jpg">
		</div>

		<div class="main">
			<div id="bigImage">
				<c:choose>
					<c:when test="${not empty portraitPic }">
						<img
							src="<ls:photo item='${portraitPic}' />"
							id="target" name="target" alt="[用户头像]"
							style="width: 300px;height:300px;">
					</c:when>
					<c:otherwise>
						<img
							src="${contextPath}/resources/templets/images/no-img_mid_.jpg"
							id="target" name="target" alt="[用户头像]"
							style="width: 300px;height:300px;">
					</c:otherwise>
				</c:choose>
			</div>
			<div>
				<input type="file" name="file" id="file" value="选择图片" height="30px;" />
				<img id="tempimg" src="" style="display:none" />
				<input id="uploadImg" type="button" value="点击预览" />
				<span style="color:#a2a2a2;">大小不超过512K</span>
			</div>
			<div id="preview-pane" style="width: 100px;height: 100px; ">
				<div class="preview-container">
					<c:choose>
						<c:when test="${not empty portraitPic }">
							<img
								src="<ls:photo item='${portraitPic}' />"
								alt="预览" class="jcrop-preview" id="smallImage">
						</c:when>
						<c:otherwise>
							<img
								src="${contextPath}/resources/templets/images/no-img_mid_.jpg"
								alt="预览" class="jcrop-preview" id="smallImage">
						</c:otherwise>
					</c:choose>

				</div>
				<input type="hidden" id="width" name="width" /> <input
					type="hidden" id="height" name="height" /> <input type="hidden"
					id="marginLeft" name="marginLeft" /> <input type="hidden"
					id="marginTop" name="marginTop" />
				<c:choose>
					<c:when test="${not empty portraitPic }">
						<input type="button" id="cutImg" value="保存"
							style="margin-top: 40px; width: 100px;" onclick="cropPhoto();" />
					</c:when>
					<c:otherwise>
						<input type="button" id="cutImg" value="保存"
							style="margin-top: 40px; width: 100px;" onclick="cropPhoto();"
							disabled="disabled" />
					</c:otherwise>
				</c:choose>

			</div>
		</div>

		<div class="clr"></div>
	</div>
</form:form>