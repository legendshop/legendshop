<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
		         <table class="ncsc-default-table">
		              <tbody>
		                <tr>
		                <th style="width: 15px;">&nbsp;</th>
		                <th class="w60">&nbsp;</th>
		                <th>商品名称</th>
		                <th class="w150">商品价格</th>
		                <th class="w120">门店库存</th>
		                <th class="w120" style="border-right:solid 1px #e4e4e4;">操作</th>
		              </tr>
		              </tbody>
		              
		              <c:choose>
		                   <c:when test="${not empty list}">
		                          <c:forEach items="${list}" var="item" varStatus="status">
		                                 <tr>
		                                <td></td>
						                <td>
						                  <div class="pic-thumb">
						                    <a href="<ls:url address='/views/${item.productId}'/>" target="_blank">
						                     <img src="<ls:images item='${item.pic}' scale='3'/>"></a>
						                  </div>
						                </td>
						                <td class="tl">
						                  <dl class="goods-name">
						                    <dt style="max-width: 732px !important;"> <a href="<ls:url address='/views/${item.productId}'/>" target="_blank">${item.productName}</a></dt>
						                  </dl>
						                </td>
						                <td><span>￥${item.cash}</span></td>
						                <td><span>${item.stock}件</span></td>
						                <td class="nscs-table-handle">
						                  <span>
						                    <a href="javascript:showSetStocks('${item.productId}')" class="btn-bluejeans">设置库存</a> 
						                    |
						                    <a href="javascript:deleteProd('${item.id}')" class="btn-bluejeans">删除</a>
						                  </span>
						                </td>
						              </tr>
		                          </c:forEach>
		                   </c:when>
		                   <c:otherwise>
		                        <tr>
		                        <td colspan="8" height="60">暂无门店商品</td>
		                      </tr>
		                   </c:otherwise>
		              </c:choose>
 </table>  
        
        
          <div class="tfoot">
             <div class="pagination">
                <div class="clear"></div>
				 <div style="margin-top:10px;" class="page clearfix">
	     			 <div class="p-wrap">
								<span class="p-num">
									<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
								</span>
	     			 </div>
		  		</div>
             </div>
          </div>
