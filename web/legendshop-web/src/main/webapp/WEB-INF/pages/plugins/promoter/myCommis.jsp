<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>我的佣金</title>
	
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;">     
       <%@include file='/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp'%>
    
     <div class="right_con">
     
         <div class="o-mt"><h2>我的佣金</h2></div>
         <div class="pagetab2">
                <ul>
                    <li class="on"><span>我的佣金</span></li>
                </ul>
         </div><hr/>
         <div class="commission" style="margin-top:15px;">
           <div class="pro-add">
             <span><b><fmt:formatNumber value="${userCommis.totalDistCommis }" pattern="#0.00#"/></b><em>累计赚得</em></span>
             <span><b><fmt:formatNumber value="${userCommis.settledDistCommis }" pattern="#0.00#"/></b><em>已结算奖励</em></span>
             <span><b><fmt:formatNumber value="${userCommis.unsettleDistCommis }" pattern="#0.00#"/></b><em>未结算奖励</em></span>
             <span><a href="${contextPath }/p/promoter/toApplyCash">申请提现</a><em><fmt:formatNumber value="${userCommis.settledDistCommis }" pattern="#0.00#"/>可提现</em></span>
           </div>

           <div class="rew-sty">
	           <form:form id="searchForm" method="GET" >
	           	<input type="hidden" value="${curPageNO}" name="curPageNO" id="curPageNO"/>
	           	<div class="fr">
		             <span>奖励类型：
		             	<select id="commisType" name="commisType">
		             		<option value="">所有</option>
		             		<option value="FIRST_COMMIS">直接下级奖励</option>
		             		<option value="SECOND_COMMIS">下二级奖励</option>
		             		<option value="THIRD_COMMIS">下三级奖励</option>
		             		<option value="REG_COMMIS">推广注册奖励</option>
		             	</select>
		             </span>
		             <span>起始时间：<input type="text" id="startTime" name="startTime" style="width:120px;" /></span>
		             <span>至：<input type="text" id="endTime" name="endTime" style="width:150px;"/></span>
		             <span><input class="btn-r" type="button" value="查询" onclick="searchAward();"></span>
	           	</div>
		       </form:form>
           </div>

			<!-- 奖励记录列表 -->
           <div id="awardList" class="rew-lis">
           		<%@ include file="awardList.jsp"%>
           </div>      
         </div>
         <!-- 我的佣金 -->
     </div>
    <!--right_con end-->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script> 	
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/myCommis.js'/>"></script>
<script type="text/javascript">
    var contextPath="${contextPath}";
</script>
</body>
</html> 