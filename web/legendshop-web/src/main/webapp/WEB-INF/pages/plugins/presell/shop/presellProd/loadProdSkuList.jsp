<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/prodSelectAlert.css'/>" rel="stylesheet"/>
<table border="0" id="skuList" cellpadding="0" cellspacing="0" style="margin: 30px 40px;">
	<tbody>
	<tr style="background: #f9f9f9;">
		<td width="70px">
			<label class="checkbox-wrapper">
								<span class="checkbox-item">
									<input type="checkbox" id="checkbox" class="checkbox-input selectAll">
									<span class="checkbox-inner"></span>
								</span>
			</label>
		</td>
		<td width="70px">商品图片</td>
		<td width="170px">商品名称</td>
		<td width="100px">商品规格</td>
		<td width="100px">商品价格</td>
		<td width="100px">可销售库存</td>
		<td width="100px">预售价(元)</td>
	</tr>
	<c:choose>
		<c:when test="${not empty skuList}">
			<c:forEach items="${skuList}" var="sku">
				<tr class="sku" >
					<td>
						<label class="checkbox-wrapper">
								<span class="checkbox-item">
									<input type="checkbox" name="array" class="checkbox-input selectOne" onclick="selectOne(this);">
									<span class="checkbox-inner"></span>
								</span>
						</label>
					</td>
					<td>
						<img src="<ls:images item='${sku.pic}' scale='3' />" >
					</td>
					<td>
						<span class="name-text">${sku.name}</span>
					</td>
					<td>
						<span class="name-text">${empty sku.cnProperties?'暂无':sku.cnProperties}</span>
					</td>
					<td>
						<span class="name-text">${sku.price}</span>
					</td>
					<td>${sku.stocks}</td>
					<td>
						<input type="text" class="set-input prsellPrice" id="prsellPrice" name="prsellPrice" style="text-align: center" value="${sku.price}">
						<input type="hidden" class="prsellProdId"  value="${sku.prodId}">
						<input type="hidden" class="prsellSkuId" value="${sku.skuId}">
						<input type="hidden" name="prodId" id="prodId" class="prodId" value="${sku.prodId}">
					</td>
				</tr>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<tr class="first">
				<td colspan="7" >暂未选择商品</td>
			</tr>
		</c:otherwise>
	</c:choose>
	</tbody>
</table>