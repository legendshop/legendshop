<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<div id="doc">
   <input type="hidden" value="${userInfo.userName}" id="userName"/>
   <div id="hd">
      <!--order content -->
         <div class="yt-wrap ordermain">
          <!-- 收货地址 -->
         	  <div class="cartnew-title clearfix">
                        <ul class="step_psw">
                            <li class="done"><span>填写账户名</span></li>
                            <li class="act"><span>验证身份</span></li>
                            <li class=" "><span>设置新密码</span></li>
                            <li class=""><span>完成</span></li>
                        </ul>
              </div>
              
          <!-- 支付方式 -->
              <div class="reg_b getpsw">
              
                  <div class="item">
                        <span class="label">验证方式：</span>
                        <div class="fl item-ifo">
                            <div class="r_inputdiv">
	                               <select class="selected sel" id="type" onchange="selectVerifyType(this.options[selectedIndex].value);">
										  <option value="mobile">已验证手机</option>
										  <option value="email">邮箱</option>
								 </select>
                            </div>
                        </div>
                  </div>
                  <div id="mobileDiv">
                  <c:if test="${not empty userInfo.nickName}">
	        			<div class="item">
							<span class="label">昵称：</span>
	        				<div class="fl">
								<label class="text-ifo text-two">${userInfo.nickName}</label>
	        					<label class="blank invisible" id="username_succeed"></label>
	        					<span class="clr"></span>
	        				</div>
	        			</div>
        			</c:if>
                  <div class="item">
                        <span class="label"> 已验证手机：</span>
                        <div class="fl item-ifo">
                            <div class="r_inputdiv v_align_mid">
                                 <%-- ${fn:substring(userInfo.userMobile,0,3)}****${fn:substring(userInfo.userMobile,7,11)} --%>
                                 ${mobileScreen}
                            </div>
                        </div>
                  </div>
                  
                   <div class="item" style="height:80px">
                        <span class="label"> 验证码：</span>
                        <div class="fl item-ifo">
                            <div class="r_inputdiv v_align_mid">
                            <input type="hidden" id="cannonull" name="cannonull" value='<fmt:message key="randomimage.errors.required"/>'/>
									<input type="hidden" id="charactors4" name="charactors4" value='<ls:i18n key="randomimage.charactors.required" length="4"/>'/> 
									<input type="hidden" id="errorImage" name="errorImage" value='<fmt:message key="error.image.validation"/>'/> 
                                <input type="text" tabindex="2" name="randNum" id="randNum" class="text" autocomplete="off" maxlength="4" style="width: 100px" onfocus="randNumSetFocus();" onblur="randNumSetBlur();"> 
                                <img id="randImage" name="randImage" src="<ls:templateResource item='/validCoderRandom'/>" width="95" height="38" style="vertical-align: middle;" />
                               <a href="javascript:void(0)" onclick="javascript:changeRandImg('${contextPath}')" style="font-weight: bold;"><fmt:message key="change.random2"/></a>
                            </div>
                            <span class="clr"></span>
								<label id="authCode_error" class="msg-error"></label>
                        </div>
                  </div>
				          
                  <div class="item">
                        <span class="label">短信验证码：</span>
                        <div class="fl item-ifo">
                            <div class="r_inputdiv">
                                <input tabindex="6" maxlength="6" onfocus="codeFocus();" onblur="codeBlur();" id="code" disabled="disabled" type="text" class="text text-1" style="float: inherit">
                                <input class="btn" type="button" value="获取短信校验码" onclick="sendSMSCode()" id="sendMobileCode" name="sendMobileCode"/>
                            <span class="clr"></span>
								<label id="code_error" class="msg-error"></label>
                            </div>
                        </div>
                  </div>
                  
                  <div class="item">
                        <span class="label">&nbsp;</span>
                        <input type="button"  tabindex="8" id="submitCode" onclick="validFindPwdCode();" class="btn-img btn-regist" value="下一步" >
                  </div>
              </div>
              <div id="emailDiv" style="display:none">
        			<div class="item">
						<span class="label">昵称：</span>
        				<div class="fl">
							<label class="text-ifo text-two">${userInfo.nickName}</label>
        					<label class="blank invisible" id="username_succeed"></label>
        					<span class="clr"></span>
        				</div>
        			</div>
        			<div class="item">
        				<span class="label">邮箱地址：</span>
        				<div class="fl">
        					<label class="text-ifo text-two">${userInfo.userMail}</label>
        				</div>
        			</div>
        			<div class="item">
        				<span class="label">&nbsp;</span>
        				<input type="button"  tabindex="8" id="sendMail" onclick="sendFindPwdEmail();" class="btn-img btn-regist" value="发送验证邮件" >
        			</div>
    			</div>
        </div>
        <span class="clr"></span>
   </div>
</div>
</div>
<script type=text/javascript>
	var contextPath="${contextPath}";
	var phone = "${userInfo.userMobile}";
	var wait;
var timer = null;
var interVal = 60;
$(document).ready(function() {
	var pVoption = "<option values='mobile'>已验证手机</option>";
	var mVoption = "<option values='email'>邮箱</option>";
	var phoneVerifn = "${userInfo.phoneVerifn}";
	var mailVerifn = "${userInfo.mailVerifn}";
	if(phoneVerifn != 1 || mailVerifn != 1){
		if(phoneVerifn == 1){
			$("#type").html(pVoption);
		}else{
			$("#mobileDiv").remove(); 
		}
		
		if(mailVerifn == 1){
			$("#type").html(mVoption);
			$("#emailDiv").show();
		}else{
			$("#emailDiv").remove(); 
		}
	}

	$("#sendMobileCode").bind("click",function(){
		timer = sendSMSCode();
	});
	calRemainTime();
});
</script>
