<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/dialog.jsp"%>
<html>
<head>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<title>退款详情-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/refund/return.css'/>" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="${contextPath}/resources/templets/css/lightbox.css">
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet" />
	<style type="text/css">
.ret-detail .ste-con ul li a:hover {
	color: #e5004f;
}
</style>
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		<div class=" yt-wrap" style="padding-top: 10px;">
			<%@include file='../usercenterLeft.jsp'%>
			<!-----------right_con-------------->
			<div class="right_con">
				<div class="o-mt">
					<h2>退款详情</h2>
				</div>
				<div class="return">
					<!-- 步骤 -->
					<div class="return-step">
						<div class="step clearfix" style="width: 75%;">
							<dl class="first <c:if test="${not empty subRefundReturn.applyTime}">doing</c:if>" style="width: 33.3%;">
								<dd class="s-text">
									买家申请退款<s></s><b></b>
								</dd>
								<dt class="s-num">1</dt>
								<c:if test="${not empty subRefundReturn.applyTime}">
									<dd class="s-time">
										<fmt:formatDate value="${subRefundReturn.applyTime}" pattern="yyyy-MM-dd HH:mm" />
										<s></s><b></b>
									</dd>
								</c:if>
							</dl>
							<dl class="normal <c:if test="${not empty subRefundReturn.sellerTime}">doing</c:if>" style="width: 33.3%;">
								<dd class="s-text">
									商家处理退款申请<s></s><b></b>
								</dd>
								<dt class="s-num">2</dt>
								<c:if test="${not empty subRefundReturn.sellerTime}">
									<dd class="s-time">
										<fmt:formatDate value="${subRefundReturn.sellerTime}" pattern="yyyy-MM-dd HH:mm" />
										<s></s><b></b>
									</dd>
								</c:if>
							</dl>
							<dl class="normal <c:if test="${not empty subRefundReturn.adminTime}">doing</c:if>" style="width: 33.3%;">
								<dd class="s-text">
									平台审核，退款完成<s></s><b></b>
								</dd>
								<dt class="s-num">3</dt>
								<c:if test="${not empty subRefundReturn.adminTime}">
									<dd class="s-time">
										<fmt:formatDate value="${subRefundReturn.adminTime}" pattern="yyyy-MM-dd HH:mm" />
										<s></s><b></b>
									</dd>
								</c:if>
							</dl>
						</div>
					</div>
					<!-- /步骤 -->
					<div class="return-con clearfix">
						<div class="return-pro">
							<div class="item">
								<div class="tit">售后维权</div>
								<div class="pro-info">
									<dl class="clearfix">
										<c:if test="${not empty subRefundReturn.productImage}">
											<dt>
												<a href="${contextPath}/views/${subRefundReturn.productId}"><img src="<ls:images item='${subRefundReturn.productImage}' scale='0'/>" alt=""></a>
											</dt>
										</c:if>
										<c:choose>
											<c:when test="${not empty subRefundReturn.productId && subRefundReturn.productId ne 0 }">
												<dd><a href="${contextPath}/views/${subRefundReturn.productId}">${subRefundReturn.productName }</a></dd><!--订单项退款  -->
											</c:when>
											<c:otherwise>
												<dd>${subRefundReturn.productName }</dd><!--整单退款  -->
											</c:otherwise>
										</c:choose>
									</dl>
								</div>
							</div>
							<div class="item">
								<p>
									<em>期望结果：</em>
									<span class="col-red">仅退款</span>
								</p>
								<p>
									<em>退款金额：</em>
									<c:choose>
					              		<c:when test="${subRefundReturn.isPresell && subRefundReturn.isRefundDeposit}">
					              			<c:if test="${empty subRefundReturn.refundAmount }">
					              				<span class="col-red">&#65509;${subRefundReturn.depositRefund}</span>
					              			</c:if>
					              			<c:if test="${not empty subRefundReturn.refundAmount }">
					              				<span class="col-red">&#65509;${subRefundReturn.depositRefund + subRefundReturn.refundAmount}元 (${info})</span>
					              			</c:if>
					              		</c:when>
					              		<c:when test="${subRefundReturn.isPresell && !subRefundReturn.isRefundDeposit}">
					              			<span class="col-red">&#65509;${subRefundReturn.refundAmount}</span>
					              		</c:when>
					              		<c:otherwise>
					              			<span class="col-red">&#65509;${subRefundReturn.refundAmount}</span>
					              		</c:otherwise>
					              	</c:choose>
								</p>
								<p>
									<em>维权原因：</em>
									<span>${subRefundReturn.buyerMessage}</span>
								</p>
								<p>
									<em>维权编号：</em>
									<span>${subRefundReturn.refundSn}</span>
								</p>
								<p>
									<em>订单编号：</em>
									<span class="col-blu">
										<a href="${contextPath}/p/orderDetail/${subRefundReturn.subNumber}">${subRefundReturn.subNumber}</a>
									</span>
								</p>
								<p>
									<em>申请备注：</em>
									<span>${subRefundReturn.reasonInfo}</span>
								</p>
								<p>
									<em>申请时间：</em>
									<span>
										<fmt:formatDate value="${subRefundReturn.applyTime}" pattern="yyyy-MM-dd HH:mm" />
									</span>
								</p>
								<c:if test="${not empty subRefundReturn.photoFile1}">
									<p>
										<em>凭证1：</em>
										<span>
											<a href="<ls:images item='${subRefundReturn.photoFile1}' scale='0'/>" data-lightbox="example-1">
												<img src="<ls:images item='${subRefundReturn.photoFile1}' scale='2'/>" style="width: 50px; height: 50px;">
											</a>
										</span>
									</p>
								</c:if>
								<c:if test="${not empty subRefundReturn.photoFile2}">
									<p>
										<em>凭证2：</em>
										<span>
											<a href="<ls:images item='${subRefundReturn.photoFile2}' scale='0'/>" data-lightbox="example-2">
												<img src="<ls:images item='${subRefundReturn.photoFile2}' scale='2'/>" style="width: 50px; height: 50px;" />
											</a>
										</span>
									</p>
								</c:if>
								<c:if test="${not empty subRefundReturn.photoFile3}">
									<p>
										<em>凭证3：</em>
										<span>
											<a href="<ls:images item='${subRefundReturn.photoFile3}' scale='0'/>" data-lightbox="example-3">
												<img src="<ls:images item='${subRefundReturn.photoFile3}' scale='2' />" style="width: 50px; height: 50px;" />
											</a>
										</span>
									</p>
								</c:if>
							</div>
						</div>
						<div class="return-sta">
							<!-- 审核退款 start -->
							<c:if test="${subRefundReturn.sellerState eq 1 && subRefundReturn.applyState eq 1}">
								<div class="item">
									<div class="item-tit">
										<em class="returning"></em>
										<b>已申请,等待商家审核</b>
									</div>
								</div>
								<div class="item">
									<p>如商家确认：平台执行退款操作。 如商家拒绝：你可以选择重新发起申请</p>
									<p>
										审核时间：若商家没有在
										<span class="col-red">
											<em id="timer" class="timer-simple-seconds" datetime="<fmt:formatDate value="${applyTime}" pattern="yyyy-MM-dd HH:mm:ss"/>">
												<em class="day">0</em>
												天
												<em class="hour">0</em>
												时
												<em class="minute">0</em>
												分
												<em class="second">0</em>
												秒
											</em>
										</span>
										内进行审核，则将自动同意退款
									</p>
								</div>
							</c:if>
							<!-- 审核退款 end -->
							<!-- 撤销退款 start -->
							<c:if test="${subRefundReturn.sellerState eq 1 && subRefundReturn.applyState eq -1}">
								<div class="item">
									<div class="item-tit">
										<em class="returning"></em>
										<b>退款申请已撤销</b>
									</div>
								</div>
							</c:if>
							<!-- 撤销退款 end -->
							<!-- 操作退款   start -->
							<c:if test="${subRefundReturn.sellerState eq 2 && subRefundReturn.applyState eq 2}">
								<div class="item">
									<div class="item-tit">
										<em class="returning"></em>
										<b>商家同意退款，等待平台确认</b>
									</div>
								</div>
							</c:if>
							<!-- 操作退款   end -->
							<!-- 操作退款完成   end -->
							<c:if test="${subRefundReturn.sellerState eq 2 && subRefundReturn.applyState eq 3}">
								<div class="item">
									<div class="item-tit">
										<em class="returned"></em>
										<b>退款完成</b>
									</div>
								</div>
								<div class="item">
									<p>
										平台备注：
										<c:if test="${empty subRefundReturn.adminMessage}">暂无</c:if>${subRefundReturn.adminMessage}</p>
									<p>
										支付方式：
										<c:if test="${subRefundReturn.payTypeId eq 'ALP' }">支付宝 </c:if>
										<c:if test="${subRefundReturn.payTypeId eq 'COIN' }">金币全额支付</c:if>
										<c:if test="${subRefundReturn.payTypeId eq 'FULL_PAY' }">预存款支付</c:if>
										<c:if test="${subRefundReturn.payTypeId eq 'SIMULATE' }">商城模拟支付</c:if>
										<c:if test="${subRefundReturn.payTypeId eq 'PRED' }">预存款全额支付 </c:if>
										<c:if test="${subRefundReturn.payTypeId eq 'WX_PAY' }">微信支付 </c:if>
									</p>
									<p>订单总额：${subRefundReturn.subMoney }</p>
								</div>
							</c:if>
							<!-- 操作退款完成   end -->
							<!-- 拒绝退款   start -->
							<c:if test="${subRefundReturn.sellerState eq 3}">
								<div class="item">
									<div class="item-tit">
										<em class="returning"></em>
										<b>商家拒绝退款</b>
									</div>
								</div>
								<div class="item">
									<p>备注信息：${subRefundReturn.sellerMessage}</p>
								</div>
								<%-- <c:if test="${subRefundReturn.isIntervene eq 0}">
									<div class="item">
										<p class="item-deal">
											<a href="${contextPath }/p/im/customerAdmin/${subRefundReturn.refundSn}" target="_blank" class="btn-blu">联系平台</a>
											<a href="javascript:void(0);" onclick="openApplyInterven('${subRefundReturn.refundSn}')" class="btn-blu">申请平台介入</a>
										</p>
									</div>
								</c:if>
								<c:if test="${subRefundReturn.isIntervene eq 1}">
									<div class="item">
										<p class="item-deal">
											<a href="${contextPath }/p/im/customerAdmin/${subRefundReturn.refundSn}" target="_blank" class="btn-blu">联系平台</a>
											<a href="javascript:void(0);" onclick="viewInterven('${subRefundReturn.refundSn}')" class="btn-blu">查看维权结果</a>
										</p>
									</div>
								</c:if> --%>
							</c:if>
							<!-- 拒绝退款   start -->
							<div class="item-tip">
								<p class="col-red">友情提示：</p>
								<p>
									1. 若提出申请后，商家拒绝退款，可再次提交申请
									<em>“申请退款”</em>
									，请求商城客服人员介入。
									<%-- <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&amp;uin=2465239493&amp;site=legendshop&amp;menu=yes">
										<img border="0" src="${contextPath }/resources/templets/images/qq_ico.png" alt="销售咨询">
									</a> --%>
								</p>
								<p>
									2. 成功完成退款；经过商城审核后，会将退款金额以
									<em>“预存款”</em>
									的形式返还到您的余额账户中或者会将退款金额以
									<em>“原路返回”</em>
									的形式退还
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="ste-but" align="center">
					<%-- <c:if test="${subRefundReturn.sellerState eq 1 && subRefundReturn.applyState eq 1}">
						<input type="button" id="edit_refund" value="编辑退款申请" class="re" onclick="window.location='${contextPath}/p/editRefundApply/${subRefundReturn.refundSn }'" />
					</c:if> --%>
					<input type="button" value="返回" class="btn-g" onclick="window.location='${contextPath}/p/refundReturnList/${subRefundReturn.applyType }'" />
				</div>
				<!-- 退款详情 -->
			</div>
		</div>
		<!-----------right_con 结束-------------->
		<div class="clear"></div>
	</div>
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/refund/lightbox.min.js'/>"></script>
	<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/refund/reciprocal.js'/>"></script>
	<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/refund/refundDetail.js'/>"></script>
	</div>
	<script type="text/javascript">
		$(function() {
			userCenter.changeSubTab("myreturnorder");//高亮菜单
		});
	</script>
</body>
</html>
