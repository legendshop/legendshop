<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.legendesign.net/tags" prefix="ls"%>
<%
  request.setAttribute("contextPath", request.getContextPath());
%>
<table class="ret-tab sold-table">
	<tr class="tit">
		<th width="10"></th>
		<th colspan="2">商品/订单号/退款号</th>
		<th width="70">退款金额</th>
		<th width="70">退货数量</th>
		<th width="90">买家会员名</th>
		<th width="120">申请时间</th>
		<th width="80">处理状态</th>
		<th width="80">平台确认</th>
		<th width="90" style="border-right: 1px solid #e4e4e4">操作</th>
    </tr>
    <c:choose>
         <c:when test="${empty requestScope.list}">
           <tr>
		 	 <td colspan="20" style="border-right: 1px solid #e4e4e4">
		 		 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
		 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
		 	 </td>
	       </tr>
         </c:when>
         <c:otherwise>
            <c:forEach items="${requestScope.list }" var="refundReturn">
	    		<tr class="detail">
					<td width="10"></td>
					<td width="75">
						<div class="pic-thumb">
							<a href="${contextPath }/views/${refundReturn.productId}">
								<img src="<ls:images item='${refundReturn.productImage}' scale='3'/>"/>
							</a>
						</div>
					</td>
					<td style="text-align: left;padding-right: 10px;">
						<dl class="goods-name">
						    <dt><a href="${contextPath }/views/${refundReturn.productId}">${refundReturn.productName }</a></dt>
				        	<dd>订单编号：<a href="${contextPath }/s/orderDetail/${refundReturn.subNumber}">${refundReturn.subNumber }</a></dd>
				        	<dd>退货编号：${refundReturn.refundSn }</dd>
				        </dl>
				    </td>
				    <td>
				    	<c:choose>
				    		<c:when test="${refundReturn.isRefundDeposit}"><!-- 退定金 -->
				    			¥${refundReturn.refundAmount + refundReturn.depositRefund}
				    		</c:when>
				    		<c:otherwise>
				    			¥${refundReturn.refundAmount}
				    		</c:otherwise>
				    	</c:choose>
				    </td>
				    <td>x ${refundReturn.goodsNum }</td>
				    <td>${refundReturn.userName }</td>
				    <td><fmt:formatDate value="${refundReturn.applyTime}" pattern="yyyy-MM-dd HH:mm" /></td>
				    <td>
				    	<c:if test="${refundReturn.sellerState eq 1 && refundReturn.applyState eq -1}">已撤销</c:if>
				    	<c:if test="${refundReturn.sellerState eq 1 && refundReturn.applyState eq 1}"><span class="col-orange">待审核</span></c:if>
				    	<c:if test="${refundReturn.sellerState eq 2}">
				    		<c:if test="${refundReturn.returnType eq 2}">
				    			<c:if test="${refundReturn.goodsState eq 1}">待买家退货</c:if>
				    			<c:if test="${refundReturn.goodsState eq 2}">待收货</c:if>
				    			<c:if test="${refundReturn.goodsState eq 3}">未收到</c:if>
				    			<c:if test="${refundReturn.goodsState eq 4}">已收到</c:if>
				    		</c:if>
				    		<c:if test="${refundReturn.returnType eq 1}">弃货</c:if>
				    	</c:if>
				    	<c:if test="${refundReturn.sellerState eq 3}">不同意</c:if>
				    </td>
				    <td>
				    	<c:choose>
			    			<c:when test="${refundReturn.applyState eq 2}">待平台确认</c:when>
			    			<c:when test="${refundReturn.applyState eq 3}">已完成</c:when>
			    			<c:otherwise>无</c:otherwise>
				    	</c:choose>
				    </td>
				    <td>
				    	<c:choose>
				    		<c:when test="${refundReturn.sellerState eq 1 && refundReturn.applyState eq 1}">
						    	<a href="${contextPath }/s/returnDetail/${refundReturn.refundId}" class="btn">处理</a>
						    </c:when>
				    		<c:when test="${refundReturn.sellerState eq 2 and  refundReturn.returnType eq 2 and refundReturn.goodsState eq 2}">
						    	<a href="javascript:void(0);" class="btn" onclick="showReceiveWin('${refundReturn.refundId}');">收货</a>
						    </c:when>
						    <c:otherwise>
						    	<a href="${contextPath }/s/returnDetail/${refundReturn.refundId}" class="btn">查看</a>
						    </c:otherwise>
				    	</c:choose>
				    </td>
				</tr>
	    	</c:forEach>
         </c:otherwise>
    </c:choose>
</table>
<div class="clear"></div>
<!-- 页码条 -->
  <div style="margin-top:10px;" class="page clearfix">
	 <div class="p-wrap">
     	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	 </div>
</div>
