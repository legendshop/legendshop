<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
  <head>
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>发票管理-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/refund/ret-not${_style_}.css'/>" rel="stylesheet"/>
  </head>
<body class="graybody">
	<div id="doc">
	  	 <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		 <div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置> <a href="${contextPath}/home">首页</a>> <a href="${contextPath}/sellerHome">卖家中心</a>> <span class="on">发票管理</span>
				</p>
			</div>
	     	<%@ include file="included/sellerLeft.jsp" %>
	     	<div id="rightContent" class="right_con">
     			<div class="seller-return">
			     	<div class="pagetab2">
						<ul id="listType">
							<li <c:if test="${empty invoiceSubDto.hasInvoice}">class="on"</c:if> ><a href="<ls:url address="/s/invoiceManager"/>"><span>发票记录</span></a></li>
							<li <c:if test="${invoiceSubDto.hasInvoice eq 1}">class="on"</c:if> ><a href="<ls:url address="/s/invoiceManager?hasInvoice=1"/>"><span>已开发票</span></a></li>
							<li <c:if test="${invoiceSubDto.hasInvoice eq 0}">class="on"</c:if> ><a href="<ls:url address="/s/invoiceManager?hasInvoice=0"/>"><span>未开发票</span></a></li>
						</ul>
        			</div>
<!-- 退换货导航 -->
					<div class="ret-sea">
						<%-- <form:form id="form1" method="GET">
							<input type="hidden" id="curPageNO" name="curPageNO" />
							
						</form:form> --%>
						<form:form action="${contextPath}/s/invoiceManager" id="form1" method="POST">
							<input type="hidden" id="curPageNO" name="curPageNO" />
							<input type="hidden" id="hasInvoice" name="hasInvoice"  value="${invoiceSubDto.hasInvoice }" />
							<div class="sold-ser sold-ser-no-bg clearfix" >
								<div class="item" style="float:right;">
									订单号:
									<input type="text" class="item-inp" id="subNumber" name="subNumber" value="${invoiceSubDto.subNumber }" style="width: 200px;">
					   				<button type="button" class="btn-r" onclick="search()">搜索</button>
								</div>
							</div>
						</form:form>
					</div>
				
					<!-- 列表内容 -->
					<div id="listBox">

					<table class="ret-tab sold-table">
						<tbody>
						<tr class="tit">
							<th width="250">订单号</th>
							<th width="250">发票信息</th>
							<th width="250">买家会员名</th>
							<th width="200">购买时间</th>
							<!-- <th width="200">申请时间</th> -->
							<th width="90" style="border-right: 1px solid #e4e4e4">操作</th>
					    </tr>
					    
					    <c:choose>
				           <c:when test="${empty requestScope.list}">
				              <tr>
							 	 <td colspan="20" style="border-right: 1px solid #e4e4e4">
							 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
							 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
							 	 </td>
							 </tr>
				           </c:when>
				           <c:otherwise>
				              <c:forEach items="${requestScope.list}" var="order" varStatus="orderstatues">		    
								<tr class="detail">
									<td style="text-align: center;">
										<dl class="goods-name">
								        	<a href="${contextPath}/s/orderDetail/${order.subNumber}" target="_blank">${order.subNumber}</a>
								        </dl>
								    </td>
								    <td style="text-align: center;padding-right: 10px;">
										${order.type==1?"普通发票":"增值税发票"}&nbsp;
										<c:choose>
											<c:when test="${order.title==1}">[个人抬头]</c:when>
											<c:otherwise>[公司抬头]</c:otherwise>
										</c:choose>&nbsp;
											${order.company}&nbsp;
										<c:if test="${order.title ne 1}">(${order.invoiceHumNumber})</c:if>
								    </td>
								    <td>
								    	${order.userName}</td>
								    <td>
										<fmt:formatDate value="${order.buyDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" />
									</td>
								    <td>
								    	<c:if test="${order.hasInvoice ==0 || empty order.hasInvoice}">
								    	<a href="#" class="btn" onclick="addInvoice('${order.subNumber}')">开发票</a>
								    	</c:if>
								    	<c:if test="${order.hasInvoice ==1}">
								    		已开发票
								    	</c:if>
								    </td>
								</tr>
								</c:forEach>
				           </c:otherwise>
			           </c:choose>
			 </tbody></table>
					    <div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
				            		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
				     			 </div>
		  				</div>
					</div>
				  	
				</div>
			</div>
	    </div>
	 	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
		
	</div>
		<!-- doc 结束 -->
		<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
		<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/invoiceManager.js'/>"></script>
		<script type="text/javascript">
			var contextPath = '${contextPath}';
			 userCenter.changeSubTab("shopInvoice"); 
			var applyType = Number('${applyType}');
		</script>
	</body>
</html>
