<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>发布商品-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-taobao.css'/>" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		  <div class="w1190">
			<div class="taobao-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/taobaoHome">卖家中心</a>>
					<span class="on">发布商品</span>
				</p>
			</div>
		
		    <%@ include file="included/sellerLeft.jsp" %>
			<div class="taobao-set">
				<div class="ste-set">
					<h3>批量导入说明:</h3>
					<ul>
						<li><i>1</i>标记<span>*</span>为必填项</li>
						<li><i>2</i>请用最新版的淘宝助手导出cvs文件,然后在此进行导入!</li>
						<li><i>3</i>如果需要手动填写商品信息进行导入,请下载导入模板,并根据模板填写。<a href="#">下载批量导入模板</a></li>
						<li><i>4</i>单次上传的商品条目不得超于100条,如果超出请分多次导入!</li>
						<li><i>5</i>导入的csv数据包最好小于1M,否则可能导入失败!</li>
					</ul>
				</div>
				<div class="set-ord">
					<ul class="set-tit">
						<li>商品类目：</li>
						<li><span>*</span>淘宝数据包：</li>
						<li>店铺分类：</li>
						<li style="height:100px;line-height:100px;"><span>*</span>商品导入状态：</li>
						<li style="width:160px;">保存宝贝描述中的图片：</li>
					</ul>
					<form:form id="importForm" action="${contextPath}/s/taobaoImport/import" method="post" enctype="multipart/form-data">
						<input type="hidden" name="formId" id="formId" value="${formId}"/>
						<input type="hidden" name="categoryId" id="categoryId" value="${categoryId}"/>
						<input type="hidden" name="startDate" id="startDate" />
						<ul class="set-con">
							<li>
								<c:if test="${not empty treeNodes }">
							      <c:forEach items="${treeNodes}" var="node" varStatus="n">
						              <c:choose>
						                <c:when test="${!n.last}">
					                         ${node.nodeName}&gt;&gt;
						                </c:when>
						                <c:when test="${n.last}">
						                    <span>
						           	           ${node.nodeName}
					                        </span>
						                </c:when>
						              </c:choose>
							      </c:forEach>
							  </c:if>
							</li>
							<li class="ban2">
							  <p style="width: 475px;float: left;"><input type="file" name="csvFile" id="csvFile"/></p>
							</li>
							<li>
								<div class="select">
									<select class="combox "  id="" name="sellerCids"  requiredTitle="true"  childNode="cityid" selectedValue=""  retUrl=""></select>
								</div>
							</li>	 
							<li style="height:100px;line-height:100px;">
								<div>
									<span class="vertical-radio">
										<input type="radio" id="publishStatus_0" value="1" name="publishStatus" checked="checked"/><label>立刻上架</label>
									</span>
									<span class="vertical-radio">
										<!-- <span> -->
											<input type="radio" id="publishStatus_1" value="2" name="publishStatus"/><label>设定时间</label>
											<select  class="sele" style="margin-left: 10px;" id="year"  onchange="timeOption(this);"></select>
											<select  class="sele" id="hours"></select>时
											<select  class="sele" id="minute"></select>分
										<!-- </span> -->
									</span class="vertical-radio">
									<span class="vertical-radio">
										<input type="radio" id="publishStatus_2" value="0" name="publishStatus"/><label>放入仓库</label>
									</span>
								</div>
						    </li>
							<li>
								<div>
							    	<span class="horizontal-radio">
							    		<input type="radio" id="isSavePicture_0" value="true" name="isSavePicture"/><label>是</label>
									</span>
									<span class="horizontal-radio">
										<input type="radio" id="isSavePicture_1" value="false" name="isSavePicture"/><label>否</label>
						    		</span>
						    	</div>
						    </li>
						</ul>
						<div class="set-save">
							<a href="javascript:importProd();">确认导入</a>
						</div> 
					</form:form> 
				</div>	
			</div>
		</div>
		
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/taobaoPropImport.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>"  type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/shop_set.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/taobaoProdImport.js'/>"></script>
<script type="text/javascript">
  var contextPath="${contextPath}";
</script>	
</body>
</html>