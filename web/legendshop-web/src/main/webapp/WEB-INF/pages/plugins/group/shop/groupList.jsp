<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<jsp:useBean id="now" class="java.util.Date"/>
<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss" var="nowDate"/>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>团购活动管理-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <%-- <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/groupon${_style_}.css'/>" rel="stylesheet"> --%>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/group-activity.css'/>" rel="stylesheet">
</head>
<body class="graybody">
<div id="doc">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
    <div class="w1190">
        <div class="seller-cru">
            <p>
            	您的位置>
                <a href="#">首页</a>>
                <a href="#">卖家中心</a>>
                <span class="on">团购活动管理</span>
            </p>
        </div>
        <%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
       
    	<div id="rightContent" class="right_con">
			
			<div class="bidding-activity">
			
		    	<!-- Tab -->
				<div id="statusTab" class="bidding-nav">
					<span <c:if test="${empty group.searchType}">class="on"</c:if> ><a
							href="<ls:url address="/s/group/query"/>">全部活动</a></span>
					<span <c:if test="${group.searchType eq 'WAIT_AUDIT'}">class="on"</c:if>><a
							href="<ls:url address="/s/group/query?searchType=WAIT_AUDIT"/>">待审核</a></span>
					<span <c:if test="${group.searchType eq 'NOT_PASS'}">class="on"</c:if> ><a
							href="<ls:url address="/s/group/query?searchType=NOT_PASS"/>">未通过</a></span>
					<span <c:if test="${group.searchType eq 'NOT_STARTED'}">class="on"</c:if> ><a
							href="<ls:url address="/s/group/query?searchType=NOT_STARTED"/>">未开始</a></span>
					<span <c:if test="${group.searchType eq 'ONLINE'}">class="on"</c:if>><a
							href="<ls:url address="/s/group/query?searchType=ONLINE"/>">进行中</a></span>
					<span <c:if test="${group.searchType eq 'FINISHED'}">class="on"</c:if> ><a
							href="<ls:url address="/s/group/query?searchType=FINISHED"/>">已结束</a></span>
					<span <c:if test="${group.searchType eq 'EXPIRED'}">class="on"</c:if>><a
							href="<ls:url address="/s/group/query?searchType=EXPIRED"/>">已失效</a></span>
				</div>
				<!-- Tab -->
	            
	            <!-- 添加/搜索 -->
				<div class="search-add">
					<div class="set-up sold-ser sold-ser-no-bg clearfix" style="overflow:hidden">
						<div class="item">
							点击添加新的团购活动：<input class="btn-r big-btn add-btn" style="display:inline-block;" onclick="location='${contextPath}/s/group/load'"  value="立即添加">
						</div>
						
						<form:form id="searchGroup" action="${contextPath}/s/group/query" style="float:right;display:inline-block" method="get">
						<div class="fr">
							<div class="item">
								活动名称：<input type="text" placeholder="请输入活动名称" name="groupName" id="groupName" value="${group.groupName}" class="text item-inp">
								
								<input type="hidden" value="${empty curPageNO ? 1:curPageNO}" name="curPageNO" id="curPageNO" /> 
								<input type="hidden" value="${group.searchType}" name="searchType" id="searchType" /> 
								
								<input type="submit" value="搜索" class="submit btn-r" style="display:inline-block">
							</div>
						</div>
						</form:form>
					</div>
				</div>
				<!-- 添加/搜索 -->
	            
	            <table class="groupon-table sold-table" style="text-align: center;">
	                <tr class="groupon-tit">
	                    <th>活动名称</th>
	                    <th width="180">活动时间</th>
	                    <th>成团人数</th>
	                    <th>参团订单</th>
	                    <th>限购数量</th>
<%--	                    <th>团购价格</th>--%>
	                    <th>活动状态</th>
	                    <th width="180">操作</th>
	                </tr>
	                 <c:choose>
			           <c:when test="${empty requestScope.list}">
			              <tr>
						 	 <td colspan="20">
						 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
						 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
						 	 </td>
						 </tr>
			           </c:when>
			           <c:otherwise>
			           		 <c:forEach items="${requestScope.list}" var="group" varStatus="status">
			                    <tr class="detail">
			                        <td><a href="${contextPath}/group/view/${group.id}" target="_blank">${group.groupName}</a></td>
			                        <td>
			                            <div class="time" style="padding: 0">
											<span style="display: block;">
												<fmt:formatDate value="${group.startTime}" pattern="yyyy-MM-dd HH:mm:ss" type="date" />
												至
											</span>
											<span style="display: block;">
												<fmt:formatDate value="${group.endTime}" pattern="yyyy-MM-dd HH:mm:ss" type="date" />
											</span>
										</div>
			                        </td>
			                        <td>${group.peopleCount}</td>
			                        <td>${group.buyerCount}</td>
			                        <td>${group.buyQuantity}</td>
<%--			                        <td>${group.groupPrice}</td>--%>
			                        <td>
										<c:choose>
											<c:when test="${group.endTime lt now}">
												<span class="group-sta">已结束</span>
											</c:when>
											<c:when test="${empty group.prodStatus || group.prodStatus eq -1 || group.prodStatus eq -2}">
												<span class="group-sta">商品已删除</span>
											</c:when>
											<c:when test="${group.prodStatus eq 0}">
												<span class="group-sta">商品已下线</span>
											</c:when>
											<c:when test="${group.prodStatus eq 2}">
												<span class="group-sta">商品违规下线</span>
											</c:when>
											<c:when test="${group.prodStatus eq 3}">
												<span class="group-sta">商品待审核</span>
											</c:when>
											<c:when test="${group.prodStatus eq 4}">
												<span class="group-sta">商品审核失败</span>
											</c:when>
											<c:otherwise>
												<c:if test="${group.status eq -2}">
													<span class="group-sta">审核未通过</span>
													<span class="question-mark"></span>
													<input type="hidden" id="auditOpinion" value="${group.auditOpinion}">
													<span id="opinion" class="auctions-sta opinion" style="display:none;">
														<div id="history" class="more"></div>
													</span>
												</c:if>
												<c:if test="${group.status eq -1}">
													<span class="group-sta">审核中</span>
												</c:if>
												<c:if test="${group.status eq 0}">
													<span class="group-staed">已失效</span>
												</c:if>
												<c:if test="${group.status eq 1}">
													<c:choose>
														<c:when test="${group.startTime gt now}">
															<span class="group-sta">未开始</span>
														</c:when>
														<c:otherwise>
															<span class="group-staing">进行中</span>
														</c:otherwise>
													</c:choose>
												</c:if>
											</c:otherwise>
										</c:choose>
			                        </td>
			                        <td>
			                        	<%-- 已结束 或者商品状态异常 --%>
			                        	<c:choose>
			                        		<c:when test="${empty group.prodStatus || group.endTime < nowDate || group.prodStatus eq -1 || group.prodStatus eq -2 || group.prodStatus eq 2 }">
	                        					<a href="${contextPath}/s/group/groupView/${group.id}" class="btn-r">查看</a>
												<a href="${contextPath }/s/group/operation/${group.id}" class="btn-r">运营</a>
		                        				
		                        				<%-- 待审核 或  未通过 并且已结束  进行物理删除 --%>
												<c:if test="${group.status eq -1 || group.status eq -2}">
													<a href="javascript:void(0);" class="blue_button del btn-g" onclick="deleteShopGroup('${group.id}')">删除</a>
												</c:if>
												
												<%-- 进行中 或  已失效 并且已结束  进行逻辑删除 --%>
												<c:if test="${group.status eq 0 || group.status eq 1 || group.status eq 2}">
													<a href="javascript:void(0);" class="blue_button del btn-g" onclick="updateDeleteStatus('${group.id }')">删除</a>
												</c:if>
			                        		</c:when>
			                        		<c:otherwise>
			                        			<%-- 待审核 或  未通过 --%>
												<c:if test="${group.status eq -1 || group.status eq -2}">
													<a onclick="updateShopGroup('${group.id}')" href="javascript:void(0);" class="btn-g">编辑</a>
													<a href="javascript:void(0);" class="blue_button del btn-g" onclick="deleteShopGroup('${group.id}')">删除</a>
												</c:if>
												<%-- 待审核 或 未通过 --%>
												
												<%-- 未开始 --%>
												<c:if test="${group.status eq 1 && group.startTime > nowDate}">
													<a onclick="updateShopGroup('${group.id}')" href="javascript:void(0);" class="btn-g">编辑</a>
												</c:if>
												<%-- 未开始 --%>
												
												<%-- 进行中 --%>
												<c:if test="${group.status eq 1 && group.startTime < nowDate}">
													<a href="${contextPath}/s/group/groupView/${group.id}" class="btn-r">查看</a>
													<a href="${contextPath }/s/group/operation/${group.id}" class="btn-r">运营</a>
													<%--<a href="javascript:void(0);" onclick="offlineShopGroup('${group.id}')" class=" btn-g">终止</a>--%>
												</c:if>
												<%-- 进行中 --%>
												
												<%-- 已失效 或已结束 --%>
												<c:if test="${group.status eq 0 || group.status eq 2}">
													<a href="${contextPath}/s/group/groupView/${group.id}" class="btn-r">查看</a>
													<a href="${contextPath }/s/group/operation/${group.id}" class="btn-r">运营</a>
													<a href="javascript:void(0);" class="btn-g" onclick="updateDeleteStatus('${group.id }')">删除</a>
												</c:if>
												<%-- 已失效  或已结束 --%>
			                        		</c:otherwise>
			                        	</c:choose>
			                        </td>
			                    </tr>
			                </c:forEach>
			           </c:otherwise>
			          </c:choose>
	            </table>
	            <div style="margin-top:10px;" class="page clearfix">
	                <div class="p-wrap">
						<span class="p-num">
							<ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="simple"/>
						</span>
	                </div>
	            </div>
            </div>
		</div>	
    </div>
    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/group/groupList.js'/>"></script>
<script type="text/javascript">
    var contextPath = '${contextPath}';
</script>
</body>
</html>