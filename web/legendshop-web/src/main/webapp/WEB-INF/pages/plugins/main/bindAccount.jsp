<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>绑定账号-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
    <style type="text/css">
    	input.error, .text.error {
    		border: 1px dashed red;
    	}
    	label.error {
    		color: red;
    	}
    </style>
</head>
<body class="loginback">
<div>
    <div class="yt-wrap">
        <div class="lr_tab">
            <a class="on" href="javascript:void(0);">登 录</a><a href="${contextPath}/reg">注 册</a>
        </div>
        <div class="logintop"><a href="${contextPath}/"><img src="<ls:photo item="${systemConfig.logo}"/>" style="max-height:89px;"/>
        </a></div>
    </div>

    <!--login-->
    <div class="regbox">

        <div class="other_lg2" style="padding-top:30px;">

        </div>

        <div class="reg_b" style="width:100%;">
            <div style="padding-bottom:20px;height:86px;line-height:86px;">
                <c:choose>
                    <c:when test="${passport.type=='qq'}">
                        <img src="<ls:templateResource item='/resources/templets/images/qq.png'/>" style="float: left;"/>
                    </c:when>
                    <c:when test="${passport.type=='sinaweibo'}">
                        <img src="<ls:templateResource item='/resources/templets/images/weibo.png'/>" style="float: left;"/>
                    </c:when>
                    <c:when test="${passport.type=='weixin'}">
                        <img src="<ls:templateResource item='/resources/templets/images/weixin.png'/>" style="float: left;"/>
                    </c:when>
                </c:choose>

                <div class=" bighint" style="float: left;margin-left: 20px;">
                	    只差一步，即可完成绑定设置
                    <c:choose>
                        <c:when test="${passport.type=='qq'}">
                            (当前用户名：${passport.nickName})
                        </c:when>
                        <c:when test="${passport.type=='sinaweibo'}">
                            (当前用户名：${passport.nickName})
                        </c:when>
                        <c:when test="${passport.type=='weixin'}">
                            (当前用户名： ${passport.nickName})
                        </c:when>
                    </c:choose>
                </div>
            </div>
            <form:form id="bindAccountForm" method="POST" action="${contextPath}/thirdlogin/bind">
                <input type="hidden" id="passportIdKey" name="passportIdKey" value="${passportIdKey }"/>
                <input type="hidden" id="returnUrl" name="returnUrl" value="${param.returnUrl}"/>

                <div class="item">
                    <span class="label" style="width: 125px;">手机号：</span>

                    <div class="fl item-ifo">
                        <div class="r_inputdiv">
                            <input type="text" class="text"  id="phone"  name="phone"  autocomplete="off"  tabindex="6"  style="width:200px;">
                        </div>
                        <div class=" error-tips"></div>
                    </div>
                </div>

                <div class="item">
                    <span class="label"> 验证码：</span>
                    <div class="fl item-ifo">
                        <div class="r_inputdiv v_align_mid">
                            <input type="text" class="text text-1" id="randNum" name="randNum" maxlength="4">
                            <img width="95" height="38" src="<ls:templateResource item='/validCoderRandom'/>" id="randImage" name="randImage">
                            <a href="javascript:void(0)" onclick="javascript:changeRandImg()" style="font-weight: bold;">换一张</a>
                            <label></label>
                        </div>
                    </div>
                </div>

                <div class="item">
                        <span class="label" style="width: 125px;">短信验证码：</span>
                    	<div class="fl item-ifo">
                    		<div class="r_inputdiv">
	                            <input type="text"  id="code" name="code"   class="text" style="width: 80px;" maxlength="6" autocomplete="off" tabindex="8">
	                            <input class="btn" type="button" value="获取短信验证码" onclick="sendMobileCode()" id="erifyCodeBtn"/>
	                        </div>
	                        <div class="error-tips"></div>
                    </div>
                </div>



                <div class="item" style="padding-top:20px;margin-left: 135px">
                    <div class="r_inputdiv">
                        <input type="submit" value="确认绑定" class="btn-img btn-regist"  style="width:170px;cursor: pointer;" tabindex="9"/>
                    </div>
                </div>
                <div class="fc-gray" style="margin-left: 135px">
                	绑定后，您的
                	<c:choose>
	                    <c:when test="${passport.type=='qq'}">QQ</c:when>
	                    <c:when test="${passport.type=='sinaweibo'}">新浪微博</c:when>
	                    <c:when test="${passport.type=='weixin'}">微信</c:when>
	                </c:choose>
                  	  和账户密码都可以登录系统
                </div>
            </form:form>

        </div>
    </div>
    <!--login end-->
    <%@ include file="./home/simpleBottom.jsp" %>
</div>
</body>

<%@ include file="/WEB-INF/pages/common/jquery.jsp" %>

<script type="text/javascript">
    var contextPath = '${contextPath}';
</script>
<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
<script src=" <ls:templateResource item='/resources/common/js/additional-methods.js'/>" type="text/javascript"></script>
<script src=" <ls:templateResource item='/resources/common/js/commonUtils.js'/>" type="text/javascript"></script>
<script type="text/javascript">

var formValidator = null;

$(function(){
	// 在键盘按下并释放及提交后验证提交表单
	formValidator = $("#bindAccountForm").validate({
		errorPlacement: function(error, element) {  
			var place = element.parent().siblings(".error-tips");
		    error.appendTo(place);  
		},
	    rules: {
	      phone: {
	        required: true,
	        phone: true
	      },
	      code: {
	        required: true,
	      }
	    },
	    messages: {
	    	phone: {
		        required: "请输入手机号",
		        phone: "您输入的手机号不正确"
		      },
		      code: {
		        required: "请输入短信验证码"
		      }
	     },
	     submitHandler: function(form){
	    	 
	    	 $.formUtils.ajaxSubmit(form, function(res){
	    		 if(res == "OK"){
	    			 layer.alert("恭喜您, 绑定成功~", function(){
	    				 
	    				 // 跳转首页
	    				 window.location.href = contextPath + "/home";
	    			 });
	    		 }else{
	    			 layer.alert(res);
	    		 }
	    	 });
	     }
	});
	
	// 用户注册协议
	$("#protocol").bind("click",function(){
		layer.open({
			title :"用户注册协议",
		    type: 2, 
		    id :"agreement",
		    content: ['agreementoverlay','no'],
		    area: ['950px', '480px']
			}); 
	});
});

function changeRandImg() {
    var obj = document.getElementById("randImage");
    obj.src = contextPath + "/validCoderRandom\?d=" + new Date().getTime();
}

/**
 * 发送短信验证码
 */
function sendMobileCode() {
    checkedRandNum();


	if(formValidator.element($("#phone"))){
		
		var phone = $("#phone").val();
		var randNum = $("#randNum").val();

		var data = {"tel": phone,"randNum":randNum};
		
		$.ajax({
			url: contextPath + "/sendSMSCodeByThirdReg", 
			type: 'POST', 
			data: data,
			async : true, //默认为true 异步   
		    dataType : 'JSON', 
			success:function(retData){
				 if(!retData=="OK"){
					layer.msg('短信发送失败',{icon:2});
					   $("#erifyCodeBtn").val("重新发送");
				 }else{
					layer.msg('短信发送成功',{icon:1},function(){
						time($("#erifyCodeBtn"));
					});
				}
			}
		});	
	}
}

function checkedRandNum() {
    var randNumValue = jQuery("#randNum").val();
    var randNumInput = $("#randNum");
    var tipText = randNumInput.parent().find("label");
    if (randNumValue == null || randNumValue == '' || randNumValue.length <= 0 || randNumValue == '请输入图片中的文字') {
        tipText.attr("class", "con_error");
        tipText.html("验证码不能为空");
        randNumInput.attr("class", "text-1 text highlight2");
        return false;
    }
    if (randNumValue.length < 4) {
        tipText.attr("class", "con_error");
        tipText.html("验证码必须是4个字符");
        randNumInput.attr("class", "text-1 text highlight2");
        return false;
    }
    tipText.attr("class", "con_succeed");
    tipText.html("");
    randNumInput.css('color', '#000');
    randNumInput.attr("class", "text-1 text");
    return true;
}


// 验证码发送后的倒计时
var wait=60;
function time(btn){
	 $btn = $(btn);
	 if (wait == 0) {
		 $btn.removeAttr("disabled");
		 $btn.val("获取短信校验码");
         wait = 60;
     } else {
    	 $btn.attr("disabled", true);
    	 $btn.val(wait+"秒后重新获取");
         wait--;
         setTimeout(function () {
             time($btn);
         },
         1000)
     }
}

</script>
</html>
