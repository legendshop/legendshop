<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>

<div class="goods-tit-table">
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th width="40">
            	<div class="goods-checkbox">
            		<input type="checkbox" id="checkedAll" style="cursor: pointer;">
            	</div>
            </th>
            <th width="110"><div class="goods-img">图片</div></th>
            <th width="200">商品名称/规格</th>
            <!-- <th width="149">SKU编码</th> -->
            <th width="110">商品价格(元)</th>
            <th width="110">可销售库存</th>
            <th width="100">拼团价(元)</th>
            <th width="60">操作</th>
        </tr>
    </table>
</div>
<div class="goods-con-table">
    <table cellpadding="0" cellspacing="0" class="prodTable">
    	<c:if test="${empty prodLists}">
			<tr class="first-leve"><td colspan='8' height="60" style="text-align: center;color: #999;">暂无数据</td></tr>
		</c:if>
		
		<c:forEach items="${prodLists}" var="prod">
	        <tr class="first-leve" data-id="${prod.prodId}">
	            <td width="40">
	            	<div class="goods-checkbox">
	            		<input type="checkbox" class="checkedProd">
	            	</div>
	            </td>
	            <td width="110">
	                <div class="goods-img">
	                <span class="switch open">-</span>
	                <img src="<ls:photo item='${prod.prodPic}'/>" alt="">
	                </div>
	            </td>
	            <td width="200"><div class="goods-name">${prod.prodName}</div></td>
	           <!--  <td width="149">--</td> -->
	            <td width="110">${prod.cash}</td>
	            <td width="110">${prod.stocks}</td>
	            <td width="100"></td>
	            <td width="60"><a href="javascript:void(0)" class="del" data-id="${prod.prodId}">删除</a></td>
	        </tr>
	        
	        <c:forEach items="${prod.dtoList}" var="sku">
		        <tr class="second-leve" data-id="${prod.prodId}">
		            <td width="40"><div class="goods-checkbox"></div></td>
		            <td width="110">
		                <div class="goods-img">
		            		<input type="checkbox" class="second-check checkedSku" data-prodId="${prod.prodId}">
		                 <img src="<ls:photo item='${sku.skuPic}'/>" alt="">
		                </div>
		            </td>
		            <td width="200">
		            	<div class="goods-name">
		            		<c:if test="${empty sku.cnProperties}">默认规格</c:if>
		            		<c:if test="${not empty sku.cnProperties}">${sku.cnProperties }</c:if>
		            	</div>
		            </td>
		            <%-- <td width="149">${sku.partyCode}</td> --%>
		            <td width="110">${sku.skuPrice}</td>
		            <td width="110">${sku.skuStocks}</td>
		            <td width="100">
		            	<input type="text" class="set-input mergePrice" id="mergePrice" name="mergePrice" value="${sku.skuPrice}">
		            	<input type="hidden" class="mergeProdId" value="${prod.prodId}">
		            	<input type="hidden" class="mergeSkuId" value="${sku.skuId}">
		            	<input type="hidden" class="mergeSkuPrice" value="${sku.skuPrice}">
		            </td>
		            <td width="60"></td>
		        </tr>
		     </c:forEach>
		     
	     </c:forEach>
        
    </table>
</div>

<script type="text/javascript">
$(function(){
	//删除
	$(".goods-con-table").on("click",".del",function(){
		var prodId = $(this).attr("data-id");
		var index = _prodIds.indexOf(prodId);
		if(index>=0){//_prodIds中存在
			_prodIds.splice(index, 1);
			$(this).parents(".first-leve").nextUntil(".first-leve").remove();
			$(this).parents(".first-leve").remove();
		}	
		if(_prodIds.length==0){
			$(".prodTable").append('<tr class="first-leve"><td colspan="8" height="60" style="text-align: center;color: #999;">暂无数据</td></tr>');
		}
	})
	
	
	//用于点击全选
	$("#checkedAll").on("click", function() {
		if($(this).is(":checked")) {//选择
			$("table :checkbox").prop("checked", true);
		}else {//取消选择
			$("table :checkbox").prop("checked", false);
		}
			
	});
	
	//点击商品选择
	$(".goods-con-table").on("click",".checkedProd",function(){
		var prodId = $(this).parents(".first-leve").attr("data-id");
		if($(this).is(":checked")) {//选择
			$(".checkedSku[data-prodId='"+prodId+"']").prop("checked", true);
		}else {//取消选择
			$(".checkedSku[data-prodId='"+prodId+"']").prop("checked", false);
		}
	})
	
	
	//点击商品-
	$(".goods-con-table").on("click",".switch",function(){
		var str = $(this).text();
		if(str == "-"){
			var prodId = $(this).parents(".first-leve").attr("data-id");
			$(".second-leve[data-id='"+prodId+"']").css("display","none");
			$(this).text("+");
		}
		if(str == "+"){
			var prodId = $(this).parents(".first-leve").attr("data-id");
			$(".second-leve[data-id='"+prodId+"']").css("display","");
			$(this).text("-");
		}
		
	})
})
</script>