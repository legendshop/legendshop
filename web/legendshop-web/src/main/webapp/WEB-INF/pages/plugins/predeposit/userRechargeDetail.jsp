<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>预存款充值明细 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/pdCashManager${_style_}.css'/>" rel="stylesheet" />
	 
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
     <%@ include file="/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp" %>
    <!-----------right_con-------------->
     <div class="right_con">
<div>
	<div class="o-mt">
		<h2>账户信息</h2>
	</div>
	<div class="pagetab2">
		<ul>
			<li><span><a href="<ls:url address='/p/predeposit/account_balance'/>">账户余额</a> </span></li>
			<li  class="on"><span>充值明细</span></li>
			<li ><span><a href="<ls:url address='/p/predeposit/balance_withdrawal'/>">余额提现</a> </span></li>
		    <li ><span><a href="<ls:url address='/p/predeposit/balance_hoding'/>">冻结中余额</a> </span></li>
		</ul>
	</div>

	<div>
		<%-- <div class="tabmenu">
			<a class="ncbtn" href="<ls:url address='/p/predeposit/recharge_add'/>" style="right: -5px;width:53px;">
			 在线充值</a>
		</div> --%>
		<div class="alert">
			<span class="mr30">可用金额：<strong class="mr5 red"
				style="font-size: 18px;"><fmt:formatNumber value="${predeposit.availablePredeposit}" pattern="#,#0.00#"/></strong>元</span><span>冻结金额：<strong
				class="mr5 blue" style="font-size: 18px;"><fmt:formatNumber value="${predeposit.freezePredeposit}" pattern="#,#0.00#"/></strong>元</span>
			<span style="float:right;margin-top: 2px;">
				<a class="btn-r" href="<ls:url address='/p/predeposit/recharge_add'/>" >
				 在线充值</a>
			</span>
		</div>

		<div class="zhss">
			<input type="hidden" value="${empty curPageNO?1:curPageNO}" name="curPageNO"  id="curPageNO"> 
		   <div style="float: left;margin-right: 5px;">
		      <span>充值单号：</span> 
			  <input type="text" value="${pdrSn}" name="pdrSn" id="pdrSn" class="text w150" placeholder="请输入充值单号" maxlength="50">
		   </div>
			<div style="float: left;">
		      <label class="submit-border">
				<input type="button" onclick="searcRrechargeDetails();" value="搜索" class="submit btn-r">
			</label>
		   </div>
		</div>
	<div id="recommend" class="recommend" style="display: block;">
		<table class="ncm-default-table sold-table">
			<thead>
				<tr>
					<th style="width:174px;">充值单号</th>
					<th style="width:104px;">创建时间</th>
					<th style="width:100px;">支付方式</th>
					<th style="width:152px;">支付交易号</th>
					<th style="width:139px;">充值金额(元)</th>
					<th style="width:100px;">状态</th>
					<th style="width:142px;">操作</th>
				</tr>
			</thead>
			<tbody>
			  	<c:choose>
	           <c:when test="${empty requestScope.list}">
	              <tr>
				 	 <td colspan="20">
				 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
				 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
				 	 </td>
				 </tr>
	           </c:when>
	           <c:otherwise>
	           		 <c:forEach items="${predeposit.rechargeDetails.resultList}" var="recharge" varStatus="status">
				      <tr>
				      <td>${recharge.sn}</td>
				       <td><fmt:formatDate value="${recharge.addTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				      <td>
				         <c:choose>
							<c:when test="${recharge.paymentState==0}">
							   --------
							</c:when>
							<c:when test="${recharge.paymentState==1}">
							     ${recharge.paymentName}
							</c:when>
						 </c:choose>
				      </td>
				      <td>
				         <c:choose>
							<c:when test="${recharge.paymentState==0}">
							   --------
							</c:when>
							<c:when test="${recharge.paymentState==1}">
							    ${recharge.tradeSn}
							</c:when>
						 </c:choose>
				      </td>
				      <td>
				        <fmt:formatNumber value="${recharge.amount}" type="currency"/>
				      </td>
				      <td>
				         <c:choose>
							<c:when test="${recharge.paymentState==0}">
							   未支付
							</c:when>
							<c:when test="${recharge.paymentState==1}">
							   已支付
							</c:when>
						 </c:choose>
				      </td>
				        <td>
				        <span>
				         <c:choose>
							<c:when test="${recharge.paymentState==0}">
							   <a class="btn-r" href="<ls:url address='/p/predeposit/recharge_pay/${recharge.sn}'/>">去支付</a>
							   <a class="btn-g" href="javascript:void(0);" onclick="deleteRechargeDetail('${recharge.sn}');" style="color:#666;">删除</a>
							</c:when>
							<c:otherwise>
							   <a class="btn-r" href="<ls:url address='/p/predeposit/rechargeDetail/${recharge.id}'/>">查看</a>
							</c:otherwise>
						 </c:choose>
						</span>
				      </td>
				      </tr>
				   </c:forEach>
	           </c:otherwise>
	           </c:choose>
			</tbody>
		</table>
	   </div>

	</div>
	<!--page-->
		 <div style="margin-top:10px;" class="page clearfix">
		 <div class="p-wrap">
			<span class="p-num">
				<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
			</span>
		</div>
		</div> 
		<div class="clear"></div>
</div>
  
</div>
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
  <script type="text/javascript">
	 function pager(curPageNO){
	     window.location.href=contextPath+"/p/predeposit/recharge_detail?curPageNO="+curPageNO;
     }
     function searcRrechargeDetails(){
       var curPageNO=$("#curPageNO").val();
       var pdrSn=$.trim($("#pdrSn").val());
        window.location.href=contextPath+"/p/predeposit/recharge_detail?curPageNO="+curPageNO+"&pdrSn="+pdrSn;
     }
     
     function deleteRechargeDetail(_pdrSn){
    	 
    	 layer.confirm("是否删除 ["+_pdrSn+"]充值单?", {
    		 icon: 3
    	     ,btn: ['确定','关闭'] //按钮
    	   }, function(){
    		   $.ajax({
    		        //提交数据的类型 POST GET
    		        type:"POST",
    		        //提交的网址
    		        url:contextPath+"/p/predeposit/deleteRechargeDetail/"+_pdrSn,
    		        //提交的数据
    		        async : false,  
    		        //返回数据的格式
    		        datatype: "json",
    		        //成功返回之后调用的函数            
    		        success:function(data){
    		        	 var result=eval(data);
    		        	 if(result=="OK"){
    		        		 layer.msg("删除成功", {icon:1, time:500}, function(){
	    		        		 window.location.reload(true);
    		        		 });
    		        	 }else{
    		        		 layer.msg("删除失败", {icon:2});
    		        	 }
    		        },
    		        //调用执行后调用的函数
    		        complete: function(XMLHttpRequest, textStatus){
    		        }     
    		    });
    	   });
    }
     
	$(document).ready(function() {
		userCenter.changeSubTab("mydeposit");
  	});
</script>
</body>
</html>
			