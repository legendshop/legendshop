<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<jsp:useBean id="now" class="java.util.Date" /> 
<fmt:formatDate value="${now}"  pattern="yyyy-MM-dd HH:mm:ss" var="nowDate"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>限时折扣活动管理-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/active/actives${_style_}.css'/>" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">限时折扣活动管理</span>
				</p>
			</div>
			
			<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>

           
            <div id="rightContent" class="right_con">
            	<div class="o-mt">
					<h2>限时折扣活动管理</h2>
				</div>
				<table class="ncsc-default-table">
				  <tbody>
				    <tr>
				      <td class="w90 tr"><strong>名称：</strong></td>
				      <td class="w120 tl">${marketing.marketName}</td>
				      <td class="w90 tr"><strong>开始时间：</strong></td>
				      <td class="w120 tl"><fmt:formatDate value="${marketing.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				      <td class="w90 tr"><strong>结束时间：</strong></td>
				      <td class="w120 tl"><fmt:formatDate value="${marketing.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				      <td class="w90 tr"><strong>状态：</strong></td>
				      <td class="w90 tr">
				          <c:choose>
				          		<c:when test="${nowDate gt marketing.endTime}">
								       已过期
								</c:when>
								<c:when test="${marketing.state==0}">
								       未发布
								</c:when>
								<c:when test="${marketing.state==1}">
								       正常状态
								</c:when>
						 </c:choose>
					</td>
					<td class="w90 tr"><strong>活动商品：</strong></td>
				      <td class="w90 tr">
				         <c:choose>
										<c:when test="${marketing.isAllProds==1}">
										       全店商品
										</c:when>
										<c:when test="${marketing.isAllProds==0}">
										       部分商品
										</c:when>
						</c:choose>
					</td>
					<c:if test="${marketing.type==2}">
					     <td class="w90 tr"><strong>规则：</strong></td>
							  <td class=" tl" style="width: 250px;">
					                   <ul class="ncsc-mansong-rule-list">
			                              <c:forEach items="${marketing.marketingMzRules}" var="xianshi" varStatus="status" end="0">
				                              <li>
				                                                                               限时价格折扣 打<strong>${xianshi.offDiscount}</strong>折
				                              </li>
			                              </c:forEach>
			                            </ul>
			                   </td>
					</c:if>	
				    </tr>
				</tbody>
			   </table>
				<c:if test="${marketing.isAllProds==0}">
				   <c:if test="${marketing.state==0}">
						 <a href="javascript:void(0);" style="float: right;margin-bottom: 10px;" id="btn_show_goods_select" class="btn-r">添加商品</a>
					</c:if>
				  
					<div style="display: none;" class="div-goods-select" id="div_goods_select">
						<table class="search-form">
							<tbody>
								<tr>
									<th class="w150"><strong>第一步：搜索店内商品</strong>
									</th>
									<td class="w160"><input type="text w150" value=""
										name="goods_name" class="text" id="search_goods_name">
									</td>
									<td class="w70 tc">
										&nbsp;<a class="btn-r small-btn" id="btn_search_goods" href="javascript:void(0);">搜索</a>
									</td>
									<td class="w10"></td>
									<td><p class="hint">不输入名称直接搜索将显示店内所有普通商品，特殊商品不能参加。</p>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="search-result" id="div_goods_search_result"></div>
						<a href="javascript:void(0);" class="close" id="btn_hide_goods_select">X</a>
					</div>
			
					<div id="prodContent">
				   </div>
				   
				    
					<div class="alert">
					  <strong>说明：</strong>
					  <ul>
					    <li>1、满促商品的时间段不能重叠</li>
					    <li>2、点击添加商品按钮可以搜索并添加参加活动的商品，点击删除按钮可以删除该商品</li>
					    <li>3、当移除所有的商品信息后,该活动会自动下线</li>
					  </ul>
					</div>
				</c:if>
				<a class="btn-r big-btn" href="/s/shopZkMarketing">返回列表</a>
			</div>
		</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
</body>

<script type="text/javascript">
     var contextPath = '${contextPath}';
     var marketId='${marketing.id}';
     var state='${marketing.state}'; 
	 jQuery(document).ready(function(){
	     $("#prodContent").load(contextPath+"/s/marketingZkRuleProds/"+marketId+"?curPage=1&&state="+state);
	     userCenter.changeSubTab("zk_Marketing"); //高亮菜单
	       //现实商品搜索
        $('#btn_show_goods_select').on('click', function() {
            $('#div_goods_select').show();
        });
        //隐藏商品搜索
        $('#btn_hide_goods_select').on('click', function() {
            $('#div_goods_select').hide();
        });
        $('#btn_search_goods').on('click', function() {
            var url =contextPath +"/s/marketingProds/"+marketId+"?curPage=1&type=2";
            url += '&' + $.param({name: $('#search_goods_name').val()});
            $('#div_goods_search_result').load(url);
        });
        $('#div_goods_search_result').on('click', 'a.demo', function() {
            $('#div_goods_search_result').load($(this).attr('href'));
            return false;
        });
	 });
</script>  
</html>
