<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>我的积分 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>

<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
    <%@include file='/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
   <div class="right_con">

		<div class="o-mt">
			<h2 style="font-size:14px;font-weight: bold;">我的订单</h2>
		</div>
		<div class="pagetab2">
			<ul >
				<li ><span><a href="<ls:url address='/p/integral'/>">积分明细</a> </span></li>
			<li class="on"><span>积分兑换记录</span></li>
			</ul>
		</div>
		<form:form  action="${contextPath}/p/integral/list" method="post" id="order_search">
				<input type="hidden" value="${curPageNO==null?1:curPageNO}" name="curPageNO" id="curPageNO">
		<div class="sold-ser sold-ser-no-bg clearfix">
	         <div class="fr">
	              <div class="item">
	              	订单状态：<select id="status" name="status" class="item-sel">
								<option value="">-请选择-</option>
								<option value="0" <c:if test="${paramDto.status == 0}"> selected="selected" </c:if>>已提交</option>
								<option value="1" <c:if test="${paramDto.status == 1}"> selected="selected" </c:if>>已发货</option>
								<option value="2" <c:if test="${paramDto.status == 2}"> selected="selected" </c:if>>已完成</option>
								<option value="3" <c:if test="${paramDto.status == 3}"> selected="selected" </c:if>>已取消</option>
							</select>
				  </div>
				  <div class="item">
	              		下单时间：<input type="text" value='<fmt:formatDate value="${paramDto.startDate}" pattern="yyyy-MM-dd" />' id="startDate" name="startDate" class="text Wdate item-inp" readonly="readonly">
						-
						<input type="text" value='<fmt:formatDate value="${paramDto.endDate}" pattern="yyyy-MM-dd" />' id="endDate" name="endDate" class="text Wdate item-inp" readonly="readonly">
				  </div>
				  <div class="item">
	                 	订单号：<input style="width: 200px;" type="text" value="${paramDto.subNumber}"" name="subNumber" class="text w150 item-inp" placeholder="订单编号">
	                  <input type="submit" id="search_order" class="bti btn-r" value="查 询">
	              </div>
	          </div>
	        </div>
		</form:form>
		<table class="ncm-default-table order sold-table">
			<thead>
				<tr>
					<th class="w10">商品</th>
					<th class="w90">积分</th>
					<th class="w120" style=" border-right: 1px solid #e4e4e4;">交易操作</th>
				</tr>
			</thead>
			<c:choose>
	           <c:when test="${empty requestScope.list}">
	              <tr>
				 	 <td colspan="20">
				 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
				 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
				 	 </td>
				 </tr>
	           </c:when>
               <c:otherwise>
                  <c:forEach items="${requestScope.list}" var="order" varStatus="orderstatues">
					    <tbody class="pay">
						<tr>
							<td class="sep-row" colspan="19"></td>
						</tr>
						<tr>
							<td class="pay-td" colspan="19">
							<span class="ml15">编号：${orderstatues.count}</span> 
							<span>订单号：${order.orderSn}</span>
		                     <span>下单时间：<fmt:formatDate value="${order.addTime}" type="both" /></span>
		                        
		                         <span>订单状态：<strong style="color:#F00">
		                         <c:choose>
									 <c:when test="${order.orderStatus==0 }">
									        已提交
									 </c:when>
									 <c:when test="${order.orderStatus==1 }">
									       已发货
									 </c:when>
									 <c:when test="${order.orderStatus==2 }">
									       已完成
									 </c:when>
									 <c:when test="${order.orderStatus==3 }">
									      已取消
									 </c:when>
								  </c:choose>
								 </strong>
								</span> 
							</td>
						</tr>
						<!-- S 商品列表 -->
						
						   <tr>
							   <td style="vertical-align: middle;">
									<c:forEach items="${order.orderItemDtos}" var="orderItem" varStatus="orderItemStatues">
										<div class="order_img" style="padding: 0px;width: 100%;">
												<div style="width: 70px;float: left;">
												<a target="_blank"
													href="<ls:url address='/integral/view/${orderItem.prodId}'/>">
													<img width="60" height="60" onmouseout="toolTip()" onmouseover="toolTip();"
													src="<ls:images item='${orderItem.prodPic}' scale='3'/>">
												</a>
												</div>
												<div style="width: 380px;float: left;margin-left:5px;">
													<a target="_blank"
														href="<ls:url address='/integral/view/${orderItem.prodId}'/>">${orderItem.prodName}</a><br>
								                            <span>数量：${orderItem.basketCount}</span> <br>
								                            <span>积分：${orderItem.exchangeIntegral}</span> 
														</div>
								                            <div style="clear: both;"></div>
												</div>
									</c:forEach>
							</td>
						<td width="21%" align="center"  style="vertical-align: middle;">
		 						<span class="order_sp">总积分:${order.integralTotal}</span>
		                	</td>	
		                	 <td align="center"  style="vertical-align: middle;border-right: 1px solid #e4e4e4;">
									 <a  class="btn-r" href="<ls:url address="/p/integral/orderDetail/${order.orderSn}"/>">查看订单</a>
									 <br>
									 <c:if test="${order.orderStatus==0 }">
											 <a class="a-block" href="javascript:void(0);"  onclick="orderCancel('${order.orderSn}');">取消订单</a>
		                 			<br>
		                 			</c:if>
		                 			<c:if test="${order.orderStatus==1 }">
		                                    <a  href="javascript:void(0);" class="a-block" onclick="confirmReceipt('${order.orderSn}')"> 确定收货</a>
		                                    <br>
									 </c:if>
		                 			<c:if test="${order.orderStatus==3 }">
		                                    <a  href="javascript:void(0);" class="a-block" onclick="orderDel('${order.orderSn}');"> 删除订单</a>
		                                    <br>
									 </c:if>
		                </td>			
					</tr>
				</tbody>
				</c:forEach>
               </c:otherwise>
            </c:choose> 
		</table>
		<div style="margin-top:10px;" class="page clearfix">
   			 <div class="p-wrap">
          		<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
   			 </div>
		</div>
</div>
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/ToolTip.js'/>"></script>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/userIntegralList.js'/>"></script>
<script type="text/javascript">
	var path="${contextPath}";
	var failedOwnerMsg = '<fmt:message key="failed.product.owner" />';
	var failedBasketMaxMsg = '<fmt:message key="failed.product.basket.max" />';
	var failedBasketErrorMsg = '<fmt:message key="failed.product.basket.error" />';
	$(document).ready(function() {
		userCenter.changeSubTab("integralOrder");
		laydate.render({
			   elem: '#startDate',
			   calendar: true,
			   theme: 'grid',
			   trigger: 'click'
		  });
  
  		laydate.render({
			   elem: '#endDate',
			   calendar: true,
			   theme: 'grid',
			   trigger: 'click'
		  });
  	});
</script>
</body>
</html>