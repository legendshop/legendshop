<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/processOrder.css?1'/>" />
<title>发货</title>
</head>
<body>
		<div style="position: absolute;  width: 410px;">
			<div class="white_box" style="margin-left:15px;">
				<%--<h1 style="cursor: move;">确认发货</h1>
					--%><table width="100%" cellspacing="0" cellpadding="0" border="1"
						class="box_table" style="float:left;margin-top:20px;">
						<tbody>
							<tr>
								<td valign="top" align="left" colspan="2" style="font-weight: bold;">请输入您退货的物流信息</td>
								 <input type="hidden" id="returnId" name="returnId" value="${returnId}">
							</tr>
							<tr>
								<td align="right">物流公司：</td>
								<td align="left">
									<select id="courierId" name="courierId">
									       <c:forEach items="${deliveryTypes}" var="dt">
	                                           <option value="${dt.dvyTypeId}" company="${dt.name}" <c:if test="${returnPo.logisticsCompany==dt.name}" > selected="selected" </c:if>>${dt.name}</option>
									       </c:forEach>
									</select>
								</td>
							</tr>
							<tr>
								<td align="right">物流单号：</td>
								<td align="left">
								    <input type="text" value="${returnPo.logisticsOrderCode}" size="24" id="courierNumber" name="courierNumber" maxlength="24">
								</td>
							</tr>
			
							<tr>
								<td align="center" colspan="2">
								<span class="inputbtn" style="margin-left: 10px;">
									      <input type="button" onclick="fahuo();" class="white_btn" style="cursor:pointer;" value="保存" > 
								</span>
								</td>
							</tr>
						</tbody>
					</table>
			</div>
		</div>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/chooseLogistics.js'/>"></script>
	<script type="text/javascript">	
	//发货
	var contextPath = "${contextPath}";
	</script>
</body>
</html>