<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>流量统计-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-trafficStatistics.css'/>" rel="stylesheet">
</head>
<body class="graybody">
<div id="doc">
	<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
<div id="Content" class="w1190">
<div class="seller-cru">
	<p>
		您的位置>
		<a href="${contextPath}/home">首页</a>>
		<a href="${contextPath}/sellerHome">卖家中心</a>>
		<span class="on">流量分析</span>
	</p>
</div>
<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
<div class="seller-trafficStatistics">
<div class="pagetab2">
	<ul id="listType">
		<li class="on"><span>流量分析</span></li>
	</ul>
</div>
<form:form  action="${contextPath}/s/shopReport/trafficStatistics" method="get" id="from1">
	<div class="form search-01 sold-ser sold-ser-no-bg clearfix">
		<div class="fr">
			<div class="item">
				<select id="queryTerms" class="sele item-sel"  name="queryTerms" onchange="change(this.value)"> 
					 <option <c:if test="${reportRequest.queryTerms == 0 }">selected</c:if> value="0">按照天统计</option>
					 <option <c:if test="${reportRequest.queryTerms == 3 }">selected</c:if> value="3">按照周统计</option>
					 <option <c:if test="${reportRequest.queryTerms == 1 }">selected</c:if> value="1">按照月统计</option>
		        </select>
			</div>
	        
	        <div class="item">
		        <span id="range">
			 		  <input readonly="readonly"  name="selectedDate" id="selectedDate" class="Wdate sele item-sel"  type="text" value='<fmt:formatDate value="${reportRequest.selectedDate}" pattern="yyyy-MM-dd"/>' />
				</span>
			</div>
			
			<div class="item">
				<input type="button" onclick="search()" class="btn-r" id="btn_keyword" value="查 询"/>				
			</div>
		</div>
	</div>
</form:form>

<input type="hidden" id="reportJson" value='${reportJson}' /> 

<div id="main" style="margin-left: 10px;width:98%;height:400px"></div>	

</div>
</div>
<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
	<script src="<ls:templateResource item='/resources/plugins/ECharts/dist/echarts.js'/>" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/templets/js/multishop/shopReport.js'/>" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/templets/js/multishop/trafficStatistics.js'/>" type="text/javascript"></script>
	<script type="text/javascript">
	var contextPath = '${contextPath}';
	var selectedYear = '${reportRequest.selectedYear}';
	var selectedMonth = '${reportRequest.selectedMonth}';
	var selectedDay = '<fmt:formatDate value="${reportRequest.selectedDate}" pattern="yyyy-MM-dd"/>';
	var selectedWeek = '${reportRequest.selectedWeek}';
	var nowYear = new Date().getFullYear();
	var nowMonth = new Date().getMonth() + 1;
	var commonYear = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];//平年月份的总天数
	var LeapYear = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];//闰年月份的总天数 
	function search(){
	  	$("#from1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	  	$("#from1")[0].submit();
	}
	$(function(){
		laydate.render({
			   elem: '#selectedDate',
			   calendar: true,
			   theme: 'grid',
			   trigger: 'click'
		  });
	});
	</script>
</body>
</html>
