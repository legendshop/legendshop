<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>打印模板-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/templets/css/multishop/seller-distribution.css'/>" />
	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
	<style type="text/css">
		input[type="radio"]:checked {
		    width: 15px;
		    height: 15px;
		    border: 6px solid #e5004f;
		    background-color: #a9a9a9;
		}
	</style>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">打印模板</span>
				</p>
			</div>
				<%@ include file="included/sellerLeft.jsp" %>
			<div class="seller-distribution">
				<div class="pagetab2" style="margin-bottom:15px;">
					<ul>
		        		<li><a onclick="window.location.href='<ls:url address='/s/printTmpl/query'/>'">打印模板</a></li>
						<li class="on" ><a href="#">新增打印模板</a></li>
					</ul>
				</div>
				<form:form  enctype="multipart/form-data" action="${contextPath}/s/printTmpl/save" method="post" id="form1"  onsubmit="return sub(this);">
				<div class="distribution-add">
					<p>新增打印模板：<span>以下均为必填项，请认真填写</span></p> 
					<ul>
						<li><i style="color:red">*</i>&nbsp;单据名称：<input type="text" name="prtTmplTitle" id="prtTmplTitle" /><em></em></li>
						<li><i style="color:red">*</i>&nbsp;单据宽度：<input type="text"   maxlength="3" name="prtTmplWidth" id="prtTmplWidth" /><span style="margin-left: 10px;font-size: 12px;color: #e5004f;">建议：980px</span>
						<em></em>
						</li>
						<li><i style="color:red">*</i>&nbsp;单据高度：<input type="text"  maxlength="3" name="prtTmplHeight" id="prtTmplHeight" /><span style="margin-left: 10px;font-size: 12px;color: #e5004f;">建议：590px</span>
							<em></em>
						</li>
						<li><i style="color:red">*</i>&nbsp;单据背景：<input type="file"  name="bgimageFile" id="bgimageFile" size="45" /><em></em></li>
						<li><i style="color:red">*</i>&nbsp;是否启用：
				            <label class="radio-wrapper radio-wrapper-checked">
								<span class="radio-item">
									<input type="radio" checked="checked" value="1" name="prtDisabled" id="waybill_usable_1" class="radio-input"/>
									<span class="radio-inner"></span>
								</span>
								<span class="radio-txt">是</span>
							</label>
							<label class="radio-wrapper">
								<span class="radio-item">
									<input type="radio" value="0" name="prtDisabled" id="waybill_usable_0" class="radio-input"/>
									<span class="radio-inner"></span>
								</span>
								<span class="radio-txt">否</span>
							</label>
			            </li>
					</ul>
					<input type="submit" value="保存" class="btn-r big-btn" style="margin-left:110px;"/>&nbsp;
                    <input type="button" value="返回" class="btn-g big-btn" onclick="window.location.href='${contextPath}/s/printTmpl/query'"/>
				</div>
				</form:form>
			</div>
 		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
	<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/printmplAdd.js'/>"></script>
	<script type="text/javascript">
		$(function(){
			$("#waybill_usable_1").click(function(){
				$("#waybill_usable_1").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper radio-wrapper-checked");
				$("#waybill_usable_0").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper");
			});
			$("#waybill_usable_0").click(function(){
				$("#waybill_usable_0").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper radio-wrapper-checked");
				$("#waybill_usable_1").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper");
			});
		});
	</script>
</body>
</html>