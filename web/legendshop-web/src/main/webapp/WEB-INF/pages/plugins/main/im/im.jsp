<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>客服聊天</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0"> 
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/im/im-base.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/im/im-service.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/plugins/layer/theme/default/layer.css'/>" rel="stylesheet" />
</head>
<body>
	<!-- 客服聊天页面 -->
	<div class="service">
		<div class="service-box">
			<div class="service-con">
				<!-- 左边栏 -->
				<div class="left-sidebar">
					<div class="user-info">
						<div class="info-con clear">
							<a href="${contextPath }/p/user" class="u-img" target="_blank">
								<c:choose>
									<c:when test="${not empty userDetail.portraitPic }">
										<img src="<ls:photo item='${userDetail.portraitPic}'/>">
									</c:when>
									<c:otherwise>
										<img src="<ls:templateResource item='/resources/templets/images/im/head-default.jpg'/>">
									</c:otherwise>
								</c:choose>
							</a>
							<a href="${contextPath }/p/user" class="u-mes" target="_blank">
								<span class="u-name">
									<c:choose>
										<c:when test="${not empty userDetail.nickName }">${userDetail.nickName }</c:when>
										<c:when test="${not empty userDetail.userMobile }">${userDetail.userMobile }</c:when>
										<c:otherwise>${userDetail.userName }</c:otherwise>
									</c:choose>
								</span>
								<span class="u-level"><em class="level">${userGrade.name}</em></span>
							</a>
						</div>
						<div class="user-search">
							<a href="javascript:void(0)" class="ser-icon"></a>
							<input type="text" name="seachShopName" id="seachShopName" class="ser-input">
						</div>
					</div>
					<div class="shop-list">
						
					</div>
				</div>
				<!-- /左边栏 -->
				<!-- 右半部分内容 -->
				<div class="service-right">
					<div class="rig-head">
						<div class="hea-shop">
							<em class="shop-sign">商家</em>
							<span class="shop-name currentShopName"></span>
							<i class="shop-on on"></i>
						</div>
					</div>
					<div class="chat-window">
						<!-- 对话窗口 -->
						<div class="chat-list">
							<!-- <div class="chat-more">
								<a href="#" class="more">点击加载更多</a>
								<span class="txt">没有更多了</span>
							</div> -->
							<!-- 对话内容 -->
							<%-- <div class="chat-tip clear">
								<div class="chat-mes suc">
									<i class="tip-i"></i>
									<p class="tip-con">您好，<strong>志高车品旗舰店</strong>客服很高兴为您服务！</p>
								</div>
							</div>
							<div class="chat-time"><p class="time">11:35</p></div>
							--%>
							<!-- 对话内容 -->
						</div>
						<!-- /对话窗口 -->
						<!-- 回复区 -->
						<div class="replay-area">
							<!-- 工具栏 -->
							<div class="rep-toolbar">
								<div class="cho-too clear">
									<a href="javascript:void(0)" class="too-a rep-face"><i class="too-i"></i></a>
									<a href="javascript:void(0)" class="too-a rep-img" style="position: relative;cursor: pointer;">
										<i class="too-i"></i>
										<input type="file" id="sendImgFile" name="sendImgFile" accept="image/*" style="position: absolute; opacity: -1;width: 22px;height: 22px;left: 0;  top: 0;z-index: 11;overflow: hidden;">
									</a>
									<a href="javascript:void(0)" class="too-a rep-star hide" id="degreeButton" style="z-index: 99">
										<i class="too-i"></i><span class="txt">评价客服</span>
									</a>
									<a href="javascript:void(0)" class="too-a rep-rule hide"><i class="too-i"></i><span class="txt">怎么发截图</span></a>
								</div>
								<div class="pop-rule hide">
									<p>将您已截好的图片直接粘贴至输入框中即可（说明：目前暂不支持IE浏览器）</p>
								</div>
								<div class="pop-star hide" id="to_comment">
									<div class="tit">请对此次服务进行评价<span class="cloose"></span></div>
									<div class="inf">
										<span class="sta-inf01"></span>
										<span class="sta-inf02"></span>
										<span class="sta-inf03"></span>
										<span class="sta-inf04"></span>
										<span class="sta-inf05"></span>
									</div>
									<div class="sub">
										<a href="javascript:void(0)" class="sub-btn submitCom">提交</a>
									</div>
								</div>
							</div>
							<!-- /工具栏 -->
							<div class="edit-rep clear">
								<ul class="emoji hide">
				                    <li class="webim-emoji-item"><img key="U1F401" src="<ls:templateResource item='/resources/templets/images/faces/ee_1.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F402" src="<ls:templateResource item='/resources/templets/images/faces/ee_2.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F403" src="<ls:templateResource item='/resources/templets/images/faces/ee_3.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F404" src="<ls:templateResource item='/resources/templets/images/faces/ee_4.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F405" src="<ls:templateResource item='/resources/templets/images/faces/ee_5.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F406" src="<ls:templateResource item='/resources/templets/images/faces/ee_6.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F407" src="<ls:templateResource item='/resources/templets/images/faces/ee_7.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F408" src="<ls:templateResource item='/resources/templets/images/faces/ee_8.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F409" src="<ls:templateResource item='/resources/templets/images/faces/ee_9.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F410" src="<ls:templateResource item='/resources/templets/images/faces/ee_10.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F411" src="<ls:templateResource item='/resources/templets/images/faces/ee_11.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F412" src="<ls:templateResource item='/resources/templets/images/faces/ee_12.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F413" src="<ls:templateResource item='/resources/templets/images/faces/ee_13.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F414" src="<ls:templateResource item='/resources/templets/images/faces/ee_14.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F415" src="<ls:templateResource item='/resources/templets/images/faces/ee_15.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F416" src="<ls:templateResource item='/resources/templets/images/faces/ee_16.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F417" src="<ls:templateResource item='/resources/templets/images/faces/ee_17.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F418" src="<ls:templateResource item='/resources/templets/images/faces/ee_18.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F419" src="<ls:templateResource item='/resources/templets/images/faces/ee_19.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F420" src="<ls:templateResource item='/resources/templets/images/faces/ee_20.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F421" src="<ls:templateResource item='/resources/templets/images/faces/ee_21.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F422" src="<ls:templateResource item='/resources/templets/images/faces/ee_22.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F423" src="<ls:templateResource item='/resources/templets/images/faces/ee_23.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F424" src="<ls:templateResource item='/resources/templets/images/faces/ee_24.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F425" src="<ls:templateResource item='/resources/templets/images/faces/ee_25.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F426" src="<ls:templateResource item='/resources/templets/images/faces/ee_26.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F427" src="<ls:templateResource item='/resources/templets/images/faces/ee_27.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F428" src="<ls:templateResource item='/resources/templets/images/faces/ee_28.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F429" src="<ls:templateResource item='/resources/templets/images/faces/ee_29.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F430" src="<ls:templateResource item='/resources/templets/images/faces/ee_30.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F431" src="<ls:templateResource item='/resources/templets/images/faces/ee_31.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F432" src="<ls:templateResource item='/resources/templets/images/faces/ee_32.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F433" src="<ls:templateResource item='/resources/templets/images/faces/ee_33.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F434" src="<ls:templateResource item='/resources/templets/images/faces/ee_34.png'/>"></li>
				                    <li class="webim-emoji-item"><img key="U1F435" src="<ls:templateResource item='/resources/templets/images/faces/ee_35.png'/>"></li>
				            </ul>
        	 
								<div class="edit-box" id="context" contenteditable="true"></div>
							</div>
							<div class="edit-btn">
								<input type="hidden" value="${consultCount}" id="consultCount"/>
								<a href="javascript:void(0)" class="send-btn" id="send">发送</a>
							</div>
							<div class="star-tip hide" id="to_comment_tip">
								<div class="sub">
									<div class="close"><a href="javascript:void(0)"></a></div>
									<p class="sub-p">聊完记得给客服MM一个评价哟</p>
								</div>
							</div>
						</div>
						<!-- /回复区 -->
					</div>
					<!-- 右边栏 -->
					<%@ include file="imRight.jsp"%>
					<!-- /右边栏 -->
				</div>
				<!-- /右半部分内容 -->
			</div>
		</div>
	</div>	
	<!-- /客服聊天页面 -->
</body>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<c:set var="imagesPrefix" value="${shopApi:getPhotoPrefix()}"></c:set>
<c:set var="imagesSuffix" value="${shopApi:getImagesSuffix(1)}"></c:set>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/layer/layer.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/common/js/randomimage.js'/>"></script>  
<script type="text/javascript">
	var contextPath = "${contextPath}";
	var sign = "${sign}";
	var consultId = "${consultId}";
	var consultType = "${consultType}";
	var dialogueCustomId = "${customId}"; //从历史会话列表传过来的客服ID
	var shopId = "${shopId}";//当前聊天的商家ID
	var userId = "${userId}";//当前登录用户ID
	var userName = "${userName}";
	var skuId = "${skuId}";
	var subNum = "${subNum}";
	var consultSource = "${consultSource}";
	var imagesPrefix = '${imagesPrefix}';
    var imagesSuffix = '${imagesSuffix}';
    var initShopId = "${shopId}";//初始化商家ID,固定不变
    var IM_BIND_ADDRESS = '${IM_BIND_ADDRESS}';

    var offset = 0; //分页偏移量
    var count = 50; //聊天记录展示记录条数
    
    var refundSn ="${refundSn_On}";//咨询平台
    
	var websocket = null;//定义的websocket对象
	//屏蔽textarea中回车默认换行
	var myTextArea = document.getElementById("context");  
	myTextArea.onkeydown = function(e){  
		var code;  
	    if (!e) var  e = window.event;  
	    if (e.keyCode) code = e.keyCode;  
	    else if (e.which) code = e.which;  
	    if(code==13 && window.event){  
	        e.returnValue = false;  
	    }else if(code==13){  
	        e.preventDefault();  
	    }   
	}  
</script>
<script charset="utf-8" src="<ls:templateResource item='/resources/templets/js/im/im.js?v=222' />"></script>

<style>
	.un-read{
		position: absolute;
		left: -5px;
		cursor: pointer;
		margin-top: 10px;
		vertical-align: top;
		border-radius: 5px;
		color: #999;
	}
</style>
</html>