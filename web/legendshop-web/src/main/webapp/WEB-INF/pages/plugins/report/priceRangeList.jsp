<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>价格区间-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link rel="stylesheet" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-priceRangeList${_style_}.css'/>" rel="stylesheet">
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
	<style type="text/css">
		.sold-table td {
			text-align: center;
		}
		.sold-table td input{
			text-align: center;
			border: 1px solid #e4e4e4;
		}
	</style>
</head>
<body class="graybody">
	<div id="doc">
	<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
<div id="Content" class="w1190">
	<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">价格区间</span>
				</p>
			</div>
		<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
	 <div class="seller-priceRangeList" >
	 	<div class="seller-com-nav" >
			<ul>
				<li onclick="selectLi(1)" <c:if test='${type == 1}'>class="on"</c:if> ><a href="javascript:void(0);">订单金额区间</a></li>
				<li onclick="selectLi(0)" <c:if test='${type == 0}'> class="on"</c:if> ><a href="javascript:void(0);">商品价格区间</a></li>
			</ul>
		</div>
		<div class="alert" style="margin-bottom:0;">
			<ul class="mt5">
				<li>1、设置商品价格区间，当对商品价格进行相关统计时按照以下设置的价格区间进行统计和显示</li>
		        <li>2、设置价格区间的几点建议：<br>（1）建议设置的第一个价格区间起始额为0；<br>（2）价格区间应该设置完整，不要缺少任何一个起始额和结束额；<br>（3）价格区间数值应该连贯例如0~100,100~200,200~300.</li>
	    	</ul>
		</div>
		<div class="form search-01">
			<div class="fr">
				<input type="button" class="addprice btn-r small-btn" id="add" value="添加" style="margin:10px 0; " onclick="addPriceRange()"/>
			</div>
		</div>
		<form:form  method="post"  action="${contextPath}/s/priceRange/save" id="from1">
		<input type="hidden" value="${type}" id="type" name="type"/>
		<table  style="width:100%;margin-bottom:0;" cellspacing="0" cellpadding="0" class="sold-table">
			<thead>
				<tr class="priceRangeList-tit" style="border-bottom: 1px solid #e4e4e4;">
					<th>起始额</th>
					<th>结束额</th>
					<th width="120">操作</th>
				</tr>
			</thead>
			
			<tbody>
				<c:forEach items="${requestScope.list}" var="priceRange" varStatus="status">
					<tr class="detail">
						<input type="hidden" name="priceRangeList[${status.index}].id" value="${priceRange.id}" />
						<input type="hidden" name="priceRangeList[${status.index}].type" value="${priceRange.type}" />
						<td>
							<input type="number" id="startPrice_${status.index}" name="priceRangeList[${status.index}].startPrice" value="<fmt:formatNumber value="${priceRange.startPrice}" pattern="0"/>" class="sele" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" oninput="if(value.length>13)value=value.slice(0,13)"/>
						</td>
						<td>
							<input type="number" id="endPrice_${status.index}"  name="priceRangeList[${status.index}].endPrice" value="<fmt:formatNumber value="${priceRange.endPrice}" pattern="0"/>"  class="sele" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" oninput="if(value.length>13)value=value.slice(0,13)"/>
						</td>
						<td>
								<a href="javascript:void(0)" onclick="deletePriceRange(this,'${priceRange.id}');" title="删除" class="btn-r">删除</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<a href="javascript:void(0)" class="btn-r small-btn" onclick="submitTable();" style="margin-top:10px;">提交</a>
		</form:form>
</div>

</div>
<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>" type="text/javascript"></script>
</body>
</html>

<script type="text/javascript">
var contextPath = '${contextPath}';
var oldLength
$(function(){
oldLength =  $("table[class='priceRangeList-table buytable mailtab'] tbody tr").length;
	priceRange.doExecute();
	userCenter.changeSubTab("priceRange");
});

//删除一行
function deletePriceRange(obj,id){
	if(!isBlank(id)){
		$.ajax({
			url: contextPath +"/s/priceRange/delete",
			data: {"id":id},
			type:'post', 
			async : false, //默认为true 异步   
			dataType : 'json',
			success:function(result){
				if(result == "OK"){
					$(obj).parents("tr").remove();
				}else{
					layer.alert("删除失败",{icon:2});
				}
			}
		});
	}else{
		$(obj).parents("tr").remove();
	}
}

//增加一行
function addPriceRange(){
	var tableLength =  $("table[class='sold-table'] tbody tr").length;
	if(tableLength < 15){
	var type = $("#type").val();
	var str ='<tr>'+
				'<input type="hidden" name="priceRangeList['+ tableLength +'].type" value="'+ type +'" />'+
				'<td><input type="text" value="0" id="startPrice_'+ tableLength +'" name="priceRangeList['+ tableLength +'].startPrice" class="sele"/></td>'+
				'<td><input type="text" value="0" id="endPrice_'+ tableLength +'" name="priceRangeList['+ tableLength +'].endPrice"  class="sele" /></td>'+
				'<td><a href="javascript:void(0)" onclick="deletePriceRange(this,null);" title="删除" class="btn-g small-btn">'+
				'删除</a></td>'+
			 '</tr>';
	$("table[class='sold-table'] tbody").append(str);
	}else{
		layer.msg('最多可添加15行！', {icon: 0});
	}
}

//提交
function submitTable(){
	var tableLength =  $("table[class='sold-table'] tbody tr").length;
	if(tableLength<=0){
		layer.msg('请先添加一行数据', {icon: 0});
		return;
	}else{
		var flag = true;
		
		for(var x=0;x<tableLength;x++){
			
			var startPrice = $("#startPrice_"+x).val();
			var endPrice = $("#endPrice_"+x).val();
			if(isBlank(startPrice) || isBlank(endPrice)){
				layer.msg('价格不能为空！', {icon: 0});
				flag = false;
				return;
			} 
		   if(startPrice.indexOf(".") !=-1 || endPrice.indexOf(".") !=-1){
			layer.msg('价格只能为正整数！', {icon: 0});
			flag = false;
			return;
		   } 
			if(isNaN(startPrice) || isNaN(endPrice)){
				layer.msg('价格只能为正整数！', {icon: 0});
				flag = false;
				return;
			}
			if(Number(startPrice) < 0 || Number(endPrice) < 0){
				layer.msg('价格只能为正整数！', {icon: 0});
				flag = false;
				return;
			}
			if(Number(startPrice) >= Number(endPrice)){
				layer.msg('第'+(x+1)+'行的起始额必须小于结束额，请重新输入！', {icon: 0});
				flag = false;
				return;
			}
			if (endPrice > 100000000){
				layer.msg('结束额不能超过1亿！', {icon: 0});
				flag = false;
				return;
			}
		}
		if(flag){
			 $("#from1").ajaxForm().ajaxSubmit({
				   success:function(data) {
				      if(data=='"OK"'){
				        layer.msg('添加成功',{icon: 1,time:500},function(){
				        	location.reload();
				        });
				        return;
				      }else{
				         layer.alert(data,{icon:2});
				         return;
				      }
				   },
				   error:function(XMLHttpRequest, textStatus,errorThrown) {
				     layer.alert('保存失败！',{icon:2});
					 return false;
				  }	
		        });
			
		}else{
			layer.msg('价格只能为数字且不能为空！', {icon: 0});
		}
	}
}

//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}

//tag 切换
function selectLi(type){
	if(type == 1){
		window.location.href = contextPath + "/s/priceRange/query/1";
	}else{
		window.location.href = contextPath + "/s/priceRange/query/0";
	}
}
</script>
