<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>推荐浏览器</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
     <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/browser.css'/>" rel="stylesheet"/>
    
</head>
<body class="graybody">
    <div class="container">
        <div class="header">
            <i></i>
            <p>为保证最佳购物体验，建议您使用以下浏览器的最新版本</p>
        </div>
        <div class="bs">
            <span class="bs-block bsi-chrome">
                <i class="bsi-icon"></i>
                <span class="bsi-name">Chrome</span>
                <a href="http://www.google.cn/chrome/" target="_blank">官方下载</a>
            </span>
            <span class="bs-block bsi-firefox">
                <i class="bsi-icon"></i>
                <span class="bsi-name">Firefox</span>
                <a href="https://www.mozilla.org/zh-CN/firefox/new/" target="_blank">官方下载</a>
            </span>
        </div>
    </div>
</body>
</html>