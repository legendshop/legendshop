<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

<li>
	<div class="fieldset">
		<label class="caption">服务说明：</label>
		<div class="fields-box">
			<textarea name="serviceShow" id="serviceShow" style="width: 495px;margin: 0px;">${productDto.serviceShow}</textarea>
		</div>
	</div>
</li>

<li>
	<div class="fieldset">
		<label class="caption">网页标题：</label>
		<div class="fields-box">
			<input maxlength="120" style="width: 495px;margin: 0px;" name="metaTitle" value="${productDto.metaTitle}" class="text text-long" id="metaTitle">
		</div>
	</div>
</li>



<li>
	<div class="fieldset">
		<label class="caption">网页关键字：</label>
		<div class="fields-box">
			<textarea cols="77" rows="8" maxlength="250" onkeyup="this.value = this.value.substring(0, 250)" onpaste="onPasteHandler(this,250);" name="keyWord" class="text-input" id="keyWord">${productDto.keyWord}</textarea>
		</div>
	</div></li>

<li>
	<div class="fieldset">
		<label class="caption" for="SubheadingID">网页描述：</label>
		<div class="fields-box">
			<textarea cols="77" rows="8" maxlength="250" onkeyup="this.value = this.value.substring(0, 250)" onpaste="onPasteHandler(this,250);" name="metaDesc" class="text-input" id="metaDesc">${productDto.metaDesc}</textarea>
		</div>
	</div></li>

<h3 id="logisticsTit">商品物流信息</h3>
<div class="form">
	<ul>
		<li class="fieldset"><label>运费方式：</label>
			<span>
				<ul class="ul-radio">
					<li>
						<input type="radio" id="goods_transfee1" <c:if test="${empty productDto.supportTransportFree || productDto.supportTransportFree==1}">checked="checked"</c:if> class="radio" value="1" name="supportTransportFree">
						<label for="goods_transfee1">商家承担运费</label>
					</li>
					<li>
						<input type="radio" id="goods_transfee2" <c:if test="${productDto.supportTransportFree==0}">checked="checked"</c:if> class="radio" value="0" name="supportTransportFree"> 
						<label for="goods_transfee2">买家承担运费</label>
					</li>
				</ul> 
			</span>
		</li>
		<div
			style="width: 59%; background: rgb(248, 248, 248) none repeat scroll 0% 0%; border: 1px solid rgb(204, 204, 204);padding-top:20px; margin-top: 12px; padding-left: 20px; margin-left:95px;  <c:if test="${empty productDto.supportTransportFree || productDto.supportTransportFree==1}">display: none;</c:if> "
			id="buyer_transport_info">
			<ul style="line-height:30px;">
			<li style="margin-bottom: 10px;"><label style="padding-top:5px;"> <input type="radio"
					<c:if test="${empty productDto.transportType||productDto.transportType==0}">checked="checked"</c:if> value="0" id="transport_type1" name="transportType">运费模板 </label>
				<div class="fields-box" id="transport_template_select" <c:if test="${productDto.transportType==0}">display: block;</c:if> style="height: 35px;">
					<div class="transport-item logis">
						<select name="transportId" id="transportId" class="sele">
							<option value="-1" selected="selected">请选择运费模板</option>
							<c:forEach items="${transportList}" var="transport" varStatus="status">
								<c:choose>
									<c:when test="${not empty productDto.transportId}">
										<c:choose>
											<c:when test="${transport.id == productDto.transportId}">
												<option value="${transport.id}" selected="selected">${transport.transName}</option>
											</c:when>
											<c:otherwise>
												<option value="${transport.id}">${transport.transName}</option>
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<option value="${transport.id}">${transport.transName}</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select> <input type="button" value="新建运费模板" style="margin-top: 3px;padding: 5px;background: #e5004f;border: 0px;color: #fff;" onclick="toTransport();"> <input type="button" value="刷新运费模板"
							style="margin-top: 3px;padding: 5px;background: #e5004f;border: 0px;color: #fff;" onclick="RefreshTp();">
					</div>
				</div>
			</li>
			<li><label style="padding-top:5px;"> <input type="radio" value="1" <c:if test="${productDto.transportType==1}">checked="checked"</c:if> id="transport_type2" name="transportType">固定运费

			</label> <%-- 平邮 <input type="text" size="10" value="${productDto.mailTransFee}" id="mail_trans_fee" name="mailTransFee"> 元, --%>快递 <input type="text" size="10" value="${productDto.expressTransFee}"
				id="express_trans_fee" name="expressTransFee"> 元<%-- ,EMS <input type="text" size="10" value="${productDto.emsTransFee}" id="ems_trans_fee" name="emsTransFee">元 --%></li>

		</ul>
		</div>
		<%-- <li class="extend-properties" style="margin-top:10px;"><label class="extend-name ">货运规格：</label>
			<div class="extend-info extend-border">
				<div class="extend-box">
					<div class="extend-field">
						<label>物流体积(立方米)：</label> <input type="text" class="text" id="volume" name="volume" value="${productDto.volume}">
					</div>
					<div class="extend-field">
						<label>物流重量(千克)：</label> <input type="text" class="text" id="weight" name="weight" value="${productDto.weight}">
					</div>
				</div>
			</div></li> --%>
	</ul>
</div>

<h3>售后保障信息</h3>
<div class="form J_DetectTrigger">

	<li><label>售后说明：</label> <span> <input type="hidden" name="afterSaleId" id="afterSaleId" value="${productDto.afterSaleId}">
				<button type="button" id="selectAfterSale" onclick="afterSale();" style="border: 0 none;height: 25px;width: 104px;background: #e5004f;color: #fff;">添加说明</button> <%--				<button class="hidden" type="button" id="afterSaleClear" onclick="clearAfterSale();" style="border: 0 none;height: 25px;width: 104px;">取消说明</button>--%>

				<span <c:if test='${empty productDto.afterSaleId}' >class="hidden"</c:if> id="afterSaleClear"> 使用模板：<span id="afterSaleName">${productDto.afterSaleName}</span>				
					<button type="button" onclick="clearAfterSale();" style="border: 0 none;height: 25px;width: 104px;background: #aaa;color: #fff;">取消说明</button> </span> <span class="prop-tips"> 填写售后说明，让买家更清楚售后保障，减少纠纷，如需换行，请在后面加上换行标签 - "br"</span>
	</span></li>
</div>

<h3>其他信息</h3>
<div class="form J_DetectTrigger">
	<ul>
		<li style="height: 28px;"><label style="margin-top: 5px;">本店类目：</label>
			<span>
				<ul class="ul-radio ul-radio-vertical">

						<li><select class="combox sele" id="catId" name="shopFirstCatId" requiredTitle="-- 一级分类 -- " childNode="nextCatId" selectedValue="${productDto.shopFirstCatId}"
						retUrl="${contextPath}/shopCat/loadCat"></select> </li>
						<li><select class="combox sele" id="nextCatId" name="shopSecondCatId" requiredTitle="-- 二级分类 --" showNone="false"
						parentValue="${productDto.shopFirstCatId}" selectedValue="${productDto.shopSecondCatId}" childNode="subCatId" retUrl="${contextPath}/shopCat/loadNextCat/{value}"></select> </li>
						<li><select
						class="combox sele" id="subCatId" name="shopThirdCatId" requiredTitle="-- 三级分类 --" showNone="false"
						parentValue="${productDto.shopSecondCatId}" selectedValue="${productDto.shopThirdCatId}"
						retUrl="${contextPath}/shopCat/loadSubCat/{value}"></select></li>
				</ul>
			</span>
		</li>
		<li><label>库存计数：</label>
			<span>
				<ul class="ul-radio ul-radio-vertical">
					<c:choose>
						<c:when test="${not empty productDto.stockCounting}">
							<li><input type="radio" value="0" name="stockCounting" class="radio" <c:if test="${productDto.stockCounting == 0}">checked="checked"</c:if>><label>拍下减库存</label></li>
							<li><input type="radio" value="1" name="stockCounting" class="radio" <c:if test="${productDto.stockCounting == 1}">checked="checked"</c:if>><label>付款减库存<span style="color: red">(使用付款减库存有超卖风险，请谨慎选择！)</span></label></li>
						</c:when>
						<c:otherwise>
							<li><input type="radio" value="0" name="stockCounting" class="radio" checked="checked"><label>拍下减库存</label></li>
							<li><input type="radio" value="1" name="stockCounting" class="radio"><label>付款减库存<span style="color: red">(使用付款减库存有超卖风险，请谨慎选择！)</span></label></li>
						</c:otherwise>
					</c:choose>
				</ul> 
			</span>
		</li>

		<li><label>开始时间：</label>
			<span id="J_publish-date"> <input type="hidden" id="publishStatus" value="${productDto.publishStatus}" /> 
			<input type="hidden" id="setUpTime" name="setUpTime" value="${productDto.setUpTime}" />
				<ul class="ul-radio ul-radio-vertical">
					<li><input type="radio" id="now0" value="1" name="publishStatus" class="radio" <c:if test="${productDto.publishStatus == 1}">checked="checked"</c:if>><label>立刻</label></li>
					<div class="clear"></div>
					<li><input type="radio" id="now1" value="2" name="publishStatus" class="radio" <c:if test="${productDto.publishStatus == 2}">checked="checked"</c:if>> <label for="_now1">设定</label> <input
						type="hidden" id="selectDays" value="${productDto.selectDays}" /> <input type="hidden" id="selectHours" value="${productDto.selectHours}" /> <input type="hidden" id="selectMinutes"
						value="${productDto.selectMinutes}" /> <select class="sele" style="margin-left: 10px;" id="year" onchange="timeOption(this);"></select> <select class="sele" id="hours"></select>时 <select
						class="sele" id="minute"></select>分</li>
					<div class="clear"></div>
					<li><input type="radio" id="inStock" value="0" name="publishStatus" class="radio" <c:if test="${productDto.publishStatus == 0}">checked="checked"</c:if>><label for="inStock">放入仓库</label>
					</li>
				</ul> 
			</span>
		</li>

		 <ls:plugin pluginId="promoter">
	 		<c:set var="distSet" value="${indexApi:getDistSet()}"></c:set>
		 	<c:set var="pluginId" scope="session" value="promoter"/>
			<li class="fieldset" >
			    <label class="caption">开启分销：</label>
			    <div class="fields-box">
			        	<input type="radio" name="supportDist" value="0" class="radio" <c:if test="${productDto.supportDist == 0 || empty productDto.supportDist}">checked="checked"</c:if> > <font>否</font>
			        	<input type="radio" name="supportDist" value="1" class="radio" <c:if test="${productDto.supportDist == 1}">checked="checked"</c:if> > <font>是</font><br/>
				 	<div id="level_rate" <c:if test="${productDto.supportDist == 0 || empty productDto.supportDist}">style="display: none;"</c:if>>
					 	<div style="margin-bottom: 5px;">
					 		<font>会员直接上级分佣比例：</font><input placeholder="${distSet.firstLevelRate}" type="text" id="firstLevelRate" name="firstLevelRate" value="${empty productDto.firstLevelRate?distSet.firstLevelRate:productDto.firstLevelRate}" maxlength="5" onchange="checkInputNumForNormal(this,2);" onkeyup="checkInputNumForNormal(this,2);" class="text text-short" style="margin-left: 0px;">%<br/>
					 	</div>
					 	<c:if test="${distSet.supportDist==1}">
						 	<div style="margin-bottom: 5px;">
						 		<font>会员上二级分佣比例：&nbsp;&nbsp;&nbsp;</font><input placeholder="${distSet.secondLevelRate}" type="text" id="secondLevelRate" name="secondLevelRate" value="${empty productDto.secondLevelRate?distSet.secondLevelRate:productDto.secondLevelRate}" maxlength="5" onchange="checkInputNumForNormal(this,2);" onkeyup="checkInputNumForNormal(this,2);" class="text text-short" style="margin-left: 0px;">%<br/>
						 	</div>
						 	<div>
						 		<font>会员上三级分佣比例：&nbsp;&nbsp;&nbsp;</font><input placeholder="${distSet.thirdLevelRate}" type="text" id="thirdLevelRate" name="thirdLevelRate" value="${empty productDto.thirdLevelRate?distSet.thirdLevelRate:productDto.thirdLevelRate}" maxlength="5" onchange="checkInputNumForNormal(this,2);" onkeyup="checkInputNumForNormal(this,2);" class="text text-short" style="margin-left: 0px;">%<br/>
						 	</div>
					 	</c:if>
					 	<div>
					 		<font style="color:#999">(分佣总比例不能超过50%)</font>
					 	</div>
				 	</div>
				 </div>
			</li>
		</ls:plugin>
	</ul>
</div>