<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>商品咨询 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con">
	<div class="o-mt"><h2>商品咨询</h2></div>
	<div class="pagetab2">
         <ul>           
            <li  class="on" ><span>商品咨询</span></li>                
         </ul>       
     </div>
         <div id="recommend" class="m10 recommend" style="display: block;">                
                
                <table width="100%" cellspacing="0" cellpadding="0" class="buytable sold-table">
                    <tbody><tr>
                        <th width="30" style="text-align: center;">
                        <!-- <input type="checkbox" id="checkbox" onclick="selAll();"/> -->
                         <span>
					           <label class="checkbox-wrapper" style="float:left;margin-left:9px;">
									<span class="checkbox-item">
										<input type="checkbox" id="checkbox" class="checkbox-input selectAll"/>
										<span class="checkbox-inner" style="margin-left:9px;"></span>
									</span>
							   </label>	
						</span>
                        </th>
                        <th width="80" style="text-align: center;">咨询商品</th>
                        <th width="280" style="text-align: center;">商品名称</th>
                        <th style="border-right: 1px solid #e4e4e4;">咨询回复</th>
                    </tr>
                     <c:choose>
			           <c:when test="${empty requestScope.list}">
			              <tr>
						 	 <td colspan="20">
						 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
						 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
						 	 </td>
						 </tr>
			           </c:when>
			           <c:otherwise>
			               <c:forEach items="${requestScope.list}" var="ProductConsult" varStatus="status">
	                    	 <tr>
	                          <td height="70">
	                          <%-- <input style="margin-left:8px;" name="strArray" type="checkbox" value="${ProductConsult.consId}"/> --%>
	                           <span>
						           <label class="checkbox-wrapper" style="float:left;margin:9px;">
										<span class="checkbox-item">
											<input type="checkbox" id="strArray" class="checkbox-input selectOne" value="${ProductConsult.consId}" onclick="selectOne(this);"/>
											<span class="checkbox-inner" style="margin:9px;"></span>
										</span>
								   </label>	
							   </span>
	                          </td>
	                          <td><a href="${contextPath}/views/${ProductConsult.prodId}" target="_blank"><img src="<ls:images item='${ProductConsult.pic}' scale='3' />" /></a></td>
	                          <td><a href="${contextPath}/views/${ProductConsult.prodId}" target="_blank">${ProductConsult.prodName}</a></td>
	                          <td>
								<div class="col3">
									<div class="col3q">
										<span title="${ProductConsult.content}" class="fore2">我的咨询：${ProductConsult.content}</span>
										<span class="ftx-03 time"><fmt:formatDate value="${ProductConsult.recDate}"  pattern="yyyy-MM-dd HH:mm:ss" /></span>
									</div>
									<div class="col3a mt5">
									    <c:choose>
									     <c:when test="${not empty ProductConsult.answer}">
											<span class="ftx04 col3a-lcol">商家回复：</span>
											<span class="col3a-rcol">
												${ProductConsult.answer}
												<span class="ftx-03 time"><fmt:formatDate value="${ProductConsult.answertime}"  pattern="yyyy-MM-dd HH:mm:ss" /></span>
											</span>
										</c:when>
									    	<c:otherwise>(暂无回复)</c:otherwise>
									    </c:choose>
										 </div>
								</div>
							</td>
	                         </tr>
	                    </c:forEach> 
			           </c:otherwise>
			         </c:choose>
                </tbody>
                </table>	
                		
	     </div>  
	         <c:if test="${not empty requestScope.list }" >
	           <div class="fl search-01">
                 <input class="bti btn-r" type="button" value="删除"  id="delBtn" onclick="deleteAction('deletemulticonsult');" style="cursor: pointer;"/>
               </div> 
               <div style="margin-top:10px;" class="page clearfix">
     			 <div class="p-wrap">
					<span class="p-num">
						<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
					</span>
     			 </div>
		  	    </div>     
	       </c:if>
		  	
 </div>
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/alternative.js'/>"></script>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/prodcons.js'/>"></script>
 <script type="text/javascript">
 	var contextPath = "${contextPath}";
	   if(typeof JSON == 'undefined'){$('head').append($("<script type='text/javascript' src='${contextPath}/resources/common/js/json.js'>"));}
	//页面的删除
</script>
</body>
</html>
			