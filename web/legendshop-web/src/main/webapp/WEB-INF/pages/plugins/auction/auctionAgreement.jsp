<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>竞拍协议-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
</head>

<body class="graybody">
<div id="doc">
   <div id="hd">
   		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
   </div>
   
   <div id="bd">
	 <div class=" yt-wrap" style="padding-top:10px;"> 
	      <div class="pm-agreement">
	          <div class="mt">
	              <h1>用户竞拍服务协议</h1>
	          </div>
	          <div class="mc">
	             ${systemConfig.auctionsPayProtocolTemplate}
	          </div>
	          <div class="mb">
	              <div class="form-checkbox">
	                  <input name="1" class="ui-form-checkbox" id="check" type="checkbox" onclick="rsCheck()">
	                  <label for="check"><em>我已阅读并同意《用户竞拍服务协议》，知晓拍卖流程及保证金须知</em></label>
	              </div>
	              <a href="javaScript:void(0);" onclick="next()" id="ok" class="submit">下一步</a>
	          </div>
	      </div>
	     </div>
    <div class="clear"></div>
 </div>
   
   </div>
<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
<script type="text/javascript">
	function next(){
		if($("#check").is(':checked')){
			//跳转支付页面
			window.location.href="<ls:url address='/p/paimai/paySecurity/pay/${paimaiId}' />";
		}else{
			layer.alert("请认真阅读并同意协议",{icon:0});
		}
		
	}
	$(function(){
		$("#check").attr("checked",false)
		$("#ok").css("background-color","gray");
	})
	
	function rsCheck(){
		if($("#check").is(':checked')){
			$("#ok").css("background-color","#e5004f")
		}else{
			$("#ok").css("background-color","gray")
		}
	}
</script>
</div>

</body>
</html>
