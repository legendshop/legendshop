<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/common.jsp"%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>我的${couponName} - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link charset="utf-8" rel="stylesheet" type="text/css" href="<ls:templateResource item='/resources/templets/css/my-red.css'/>"/>
</head>
<body class="graybody">
<div id="bd">
<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
 <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
   <%@include file='/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp'%>
	<div class="seller-selling">		
    <!-----------right_con-------------->
	<!--我的礼券-->
	<div class="right_con">
		<form:form  action="${contextPath}/p/coupon" id="form1" method="post">
			<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
			<input type="hidden" id="couPro" name="couPro" value="${couPro}"/>
			<input type="hidden" id="useStatus" name="useStatus" value="${useStatus}"/>
		</form:form>
     
         <div class="o-mt"><h2>我的${couponName}</h2></div>
         
         <div class="pagetab2">
                 <ul id="listType">           
                   <li class="on" value="1"><span>可使用</span></li>                  
                   <li value="2"><span>已使用</span></li>  
                   <li value="3"><span>已过期</span></li>
                   <li value="4"><span>领取${couponName}</span></li>    
                 </ul>       
         </div>
                
         <div id="recommend" class="m10 recommend" style="display: block;">
                <table width="100%" cellspacing="0" cellpadding="0" class="ret-table">
                    <tr>
                        <th width="10"></th>
                        <th width="70">图片</th>
                        <th width="170">
                        <c:if test="${couPro eq 'shop'}">商家店名</c:if>
                        <c:if test="${couPro eq 'platform'}">红包类型</c:if>
                        </th>
                        <th width="100">${couponName}金额（元）</th>
                        <th width="100">有效日期</th>
                    </tr>
                     <c:choose>
					 	<c:when test="${empty requestScope.list}">
						 	 <tbody>
						 	 	<tr>
						 	 		<td colspan="20">
						 	 			<!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
						 	 			<div class="warning-option"><!-- <i></i> --><span>没有符合条件的信息</span></div>
						 	 		</td>
						 	 	</tr>
					        </tbody>
					 	</c:when>
					 	<c:otherwise>
			 		      <c:forEach items="${requestScope.list}" var="coupon">
			                   <tr class="bd-line">
			                     <td class="bdl"></td>
			                     <td align="center">
			                     <c:if test="${empty coupon.couponPicture}">
			                     	<c:if test="${couPro eq 'shop'}">
			                     		<a href="${contextPath}/p/coupon/couponProds/${coupon.couponId}">
			                     			<img src="${contextPath}/resources/templets/images/coupon/coupon-default.png"  style="width:65px;height:65px;"/>
			                     		</a>
			                     	</c:if>
			                     	<c:if test="${couPro eq 'platform'}">
			                     		<img src="${contextPath}/resources/templets/images/coupon/red-default.png" style="width:65px;height:65px;"/>
			                     	</c:if>
			                     </c:if>
			                     <c:if test="${not empty coupon.couponPicture}">
			                     	<c:if test="${couPro eq 'shop'}">
			                     		<a href="${contextPath}/p/coupon/couponProds/${coupon.couponId}">
			                     			<img src="<ls:images item='${coupon.couponPicture}' scale='3'/>" />
			                     		</a>
			                     	</c:if>
			                     	<c:if test="${couPro eq 'platform'}">
			                     		<img src="<ls:images item='${coupon.couponPicture}' scale='3'/>"/>
			                     	</c:if>
			                     </c:if>
			                     </td>
			                     <td class="tl">
			                       <dl class="goods-name" style="text-align: center;">
			                         <dt style="text-align: center;">
			                         <c:if test="${couPro eq 'shop'}">
				                         <a href="${contextPath}/p/coupon/couponProds/${coupon.couponId}">
				                         	${coupon.siteName}
				                         	<c:choose>
			                        	 		<c:when test="${coupon.couponType=='common'}">
			                        	 			&nbsp;[店铺通用券]
			                        	 		</c:when>
			                        	 		<c:when test="${coupon.couponType=='product'}">
			                        	 			&nbsp;[限商品]
			                        	 		</c:when>
			                        	 	</c:choose>
				                         </a>
			                         </c:if>
		                        	 <c:if test="${couPro eq 'platform'}">
		                        	 	<c:choose>
		                        	 		<c:when test="${coupon.couponType=='common'}">
		                        	 			全平台红包
		                        	 		</c:when>
		                        	 		<c:when test="${coupon.couponType=='shop'}">
		                        	 			限店铺红包
		                        	 		</c:when>
		                        	 	</c:choose>
		                        	 </c:if>
			                         </dt>
			                         <dd align="center" style="max-width: 100%;">（使用条件：订单满${coupon.fullPrice }元）</dd>
			                       </dl>
			                     </td>
			                     <td align="center">${coupon.offPrice}</td>
			                     <td align="center">
			                     <fmt:formatDate value="${coupon.startDate}" pattern="yyyy-MM-dd HH:mm:ss" /><br>
			                     ~
			                     </br><fmt:formatDate value="${coupon.endDate}" pattern="yyyy-MM-dd HH:mm:ss" />
			                     </td>
			                   </tr>
		                    </c:forEach>
					 	</c:otherwise>
					 </c:choose>
                </table>
                <!-- 分页 -->
                <div style="margin-top:10px;" class="page clearfix">
   					<div class="p-wrap">
           				<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
   			 		</div>
				</div>
	            <!-- 我的红包 -->
	         </div>
     </div>
    <!--right_con end-->
    <div class="clear"></div>
 </div>
 </div>
<!----两栏end---->
		
<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
</div>
<script charset="utf-8"src="<ls:templateResource item='/resources/templets/js/coupon.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/userIntegralList.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/couponIndex.js'/>"></script>
<script>
	var curUseStatus;
	var contextPath = "${contextPath}";
	var _couPor = "${couPro}";
	
	if(_couPor == "shop"){
		userCenter.changeSubTab("myCoupon");
	}else{
		userCenter.changeSubTab("myRedPacket");
	}
</script>
</body>
</html>