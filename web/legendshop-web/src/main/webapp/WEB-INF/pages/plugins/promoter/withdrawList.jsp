<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>申请提现</title>
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;">     
       <%@include file='/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp'%>
    
     <div class="right_con">
     
         <div class="o-mt"><h2>申请提现</h2></div>
          
          <div class="pagetab2">
                 <ul id="switchTab">           
                    <li><span><a href="${contextPath }/p/promoter/toApplyCash">申请提现</a></span></li>
                    <li class="on" ><span><a href="${contextPath }/p/promoter/withdrawList">提现记录</a></span></li>
                 </ul>       
          </div>

         <div class="apply-withdraw">
           <div class="con">
           	<table cellspacing="0" cellpadding="0">
			  <tr>
			    <th>提现金额</th>
			    <th>提现账号</th>
			    <th>申请日期</th>
			    <th>提现状态</th>
			  </tr>
			  <c:forEach items="${requestScope.list }" var="item" >
				  <tr>
				    <td>¥${item.amount }</td>
				    <td>提现至预存款</td>
				    <td><fmt:formatDate value="${item.addTime }" pattern="yyyy-MM-dd HH:mm" /></td>
				    <td>提现成功</td>
				  </tr>
			  </c:forEach>
			   <c:if test="${empty requestScope.list}">
			   		<tr>
			   			<td colspan="6" style="height: 80px;color: #666;">对不起,暂时没有符合条件的记录!</td>
			   		</tr>
			   </c:if>
			</table>
			<div style="margin-top:10px;" class="page clearfix">
    			 <div class="p-wrap">
           		 <c:if test="${toolBar!=null}">
						<span class="p-num">
							<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
						</span>
					</c:if>
    			 </div>
			</div>
           </div>
         </div>
     </div>
    <!--right_con end-->
    
    
    <div class="clear"></div>
 </div>
    <!--right_con end-->
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>"  type="text/javascript"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script> 	
	<script type="text/javascript">
    var contextPath="${contextPath}";
  	 $(document).ready(function() {
  		    userCenter.changeSubTab("myCommis");
	  		$("select.combox").initSelect();
	  });
	  
	  function pager(curPageNO){
		window.location.href = contextPath+"/p/promoter/withdrawList?curPageNO=" + curPageNO;	
	  }
</script>
</body>
</html> 