<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>

<div class="alert">
         <h4>操作提示：</h4>
          <ul>
            <li>1. 请输入已获得的${couponName}卡密领取${couponName}</li>
            <li>2. 领取${couponName}后可以在购买商品下单时选择符合使用条件的${couponName}抵扣订单金额</li>
          </ul>
 </div>
        
        <div class="wri-red-pas">
           <input type="hidden" id="couPro" value="${couPro}"/>
           <ul>
             <li><span><em>*</em>请输入${couponName}卡密：</span><input type="text" id="couponPwd" name="couponPwd"></li>
             <li><span><em>*</em>请输入验证码：</span>
             	<input type="text" autocomplete="off" size="10" maxlength="4"
					id="captcha" class="text" name="verifyCode" >
				 <img border="0" class="ml5 vm" id="codeimage" name="codeimage"	src="<ls:templateResource item='/validCoderRandom'/>" >
				
					<a onclick="changecodeimage()" class="ml5 blue" href="javascript:void(0);">看不清？换张图</a> 
				
				<label class="error" generated="true" for="captcha"></label></li>
           </ul>
         </div>
		 
		 <div class="ste-but">
         <button onclick="activateCoupon()" class="btn-r big-btn" style="margin-left: 160px;">确认激活</button>
		 </div>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>"  type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/common/js/randomimage.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/getCoupon.js'/>"></script>
<script>
 	var contextPath="${contextPath}";
 	var couponName = "${couponName}";
</script>
