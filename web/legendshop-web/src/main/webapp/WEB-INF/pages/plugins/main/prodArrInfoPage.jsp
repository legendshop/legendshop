<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/multishop/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
<style type="text/css">
	.buttonDiv {margin: 0 auto;width:180px;height:50px;}
	.myButton {float:left;margin: 20px auto;display: block;border-radius: 5px;text-align: center;background-color: #e5004f;color: #fff !important;padding: 5px 10px;font-size: 12px;border: 0px;cursor: pointer;}
	.inforInput{width: 212px;float: left;margin-top: 8px;margin-right: 20px;padding: 5px 5px;font-size: 12px;}
</style>
<table border="0" align="center" class="selling-table" id="col1" style="padding: 13px;">
<form:form >
<input  type="hidden" id="shopId" value="${shopId}"/>
<input  type="hidden" id="prodId" value="${prodId}"/>
<input  type="hidden" id="skuId" value="${skuId}"/>
<div align="center">
     <tr class="detail">
		<td width="30%">
         	<div align="center">手机</div>
      	</td>
        <td width="70%">
          	<input id="mobilePhone" placeholder="请填写您的手机号码" class="inforInput" type="text"/>
        </td>
    </tr>
    <tr  class="detail">
		<td>
         	<div align="center">邮箱</div>
      	</td>
       <td>
          	<input id="email" class="inforInput" placeholder="请填写您的邮箱" type="text"/>
       </td>
    </tr>
</div>
 </form:form>
</table>
<div class="am-btn-group am-btn-group-xs">
	<div class="buttonDiv">
	   	<button class="myButton bat-oper-btn" style="margin-left:10px;padding: 6px 10px;" onclick="close9();">继续购物</button>
	  	<button class="myButton bat-oper-btn" style="margin-left:20px;padding: 6px 10px;" onclick="addInform();">&nbsp;&nbsp;确&nbsp;&nbsp;定&nbsp;&nbsp;</button>
	</div>
</div>
<script language="javascript">
	(function($){
  		$.isBlank = function(obj){
    		return(!obj || $.trim(obj) === "");
  		};
	})(jQuery);
	 
	function close9(){
		var index = window.parent.layer.getFrameIndex('prodArrInfoPage'); //先得到当前iframe层的索引
		window.parent.layer.close(index); //再执行关闭 
	}
	//添加到货通知	
	function addInform(){
		var mobilePhone = $("#mobilePhone").val();
		
		var isPhone=/^[1][0-9]{10}$/;
		if($.isBlank($("#mobilePhone").val())){

			layer.msg("请输入手机号码！", {icon:3});
			return;
		}
		if(!isPhone.test(mobilePhone)){

	layer.msg("请输入正确的手机号码！", {icon:3});

			return;
		}
		
		var email = $("#email").val();
		var isEmail=/^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/;
		if(!$.isBlank(email) && !isEmail.test(email)){


			layer.msg("请输入正确的邮箱！", {icon:3});

			return;
		}
		var jsondata = JSON.stringify(getTableData());
		$.ajax({
			type: "post",
			data:{"jsondata":jsondata},
            url: "${contextPath}/views/prodArrInform",
            dataType: "json",
            async : true, //默认为true 异步   
            success: function(msg){
            	if(msg == "OK"){
					layer.msg("到货通知添加成功！", {icon:1, time:1000}, function(){
						var index = window.parent.layer.getFrameIndex('prodArrInfoPage'); //先得到当前iframe层的索引
						window.parent.layer.close(index); //再执行关闭 
					});

            	} else if(msg == "LR"){

            		layer.msg("请登录后再添加到货通知！", {icon:3, time:1000}, function(){
	            		var index = window.parent.layer.getFrameIndex('prodArrInfoPage'); //先得到当前iframe层的索引
	            		window.parent.layer.close(index); //再执行关闭 
            		});

            	}else{

            		layer.msg("未知错误！", {icon:2, time:1000}, function(){
	            		var index = window.parent.layer.getFrameIndex('prodArrInfoPage'); //先得到当前iframe层的索引
	            		window.parent.layer.close(index); //再执行关闭 
            		});

            	}
            }
		});	
	}

	//获取表单数据
	function getTableData(){
		var data = {};
		var shopId = $("#shopId").val();
		var prodId = $("#prodId").val();
		var skuId = $("#skuId").val();
		var mobilePhone = $("#mobilePhone").val();
		var email = $("#email").val();
		data["shopId"] = shopId;
		data["prodId"] = prodId;
		data["skuId"] = skuId;
		data["mobilePhone"] = mobilePhone;
		data["email"] = email;
		return data;
	}
</script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>

