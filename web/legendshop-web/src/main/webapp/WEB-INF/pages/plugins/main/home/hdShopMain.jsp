<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<link rel="stylesheet" href="<ls:templateResource item='/resources/common/css/jquery.autocomplete.css'/>">
<c:set var="shopCartCounts"  value="${indexApi:getShopCartCounts(pageContext.request)}"></c:set>
<c:set var="hotSearchList"  value="${indexApi:findHotSearch()}"></c:set>
 <!--hdmain 结束-->
    <div id="hdmain">
        <div class="yt-wrap top-main">
            <h1 class="ilogo">
                <a href="<ls:url address='/'/>">
		      		<img src="<ls:photo item="${systemConfig.logo}"/>"  style="max-height:89px;" />
                </a>
            </h1>

            <div id="J-MiniCart" class="icart cur-icart">
                <a href="${contextPath}/shopCart/shopBuy" class="minishop J-MiniShop">
                    <span>购物车<strong>${shopCartCounts>99?'99+':shopCartCounts}</strong>件</span>
                </a>
            </div>
            <div class="isearch">
                <form:form action="${contextPath}/list" method="get" id="searchform" name="searchform">
	                <div class="searchbox" style="float: left;width: 379px;">
	                    <input type="text" autocomplete="off" style="height: 41px;width: 382px;" value="<c:out value="${param.keyword}"></c:out>" name="keyword" id="_keyword"   class="place-holder">
                   </div>
	                <div style ="float: left;">
	                  	<a  href="javascript:searchPShoproduct();" style="background: #333;display: block;float: left;height:45px;width: 85px;color: #fff;font-size: 14px;text-align: center; line-height: 45px;" >本店搜索</a>
	                   	<a  href="javascript:searchproduct();" style="margin-left: 15px;background: #333;display: block;float: left;height:45px;width: 85px;color: #fff;font-size: 14px;text-align: center; line-height: 45px;">商城搜索</a>
	                </div>
                 </form:form>
                <div class="hotkeyword">
                 <c:forEach items="${hotSearchList}" var="hot" varStatus="status">
	         	   <c:choose>
	         	     <c:when test="${status.index%2==0}">
                         <a class="yt_hotword" href="javascript:goHotLink('${hot.msg}')">${hot.msg}</a>
                         <%--<a class="yt_hotword" href="${contextPath}/list?keyword=${hot.msg}">${hot.msg}</a>--%>
	         	     </c:when>
	         	     <c:otherwise>
                         <a class=""  href="javascript:goHotLink('${hot.msg}')">${hot.msg}</a>
                         <%--<a class=""  href="${contextPath}/list?keyword=${hot.msg}">${hot.msg}</a>--%>
	         	     </c:otherwise>
	         	  </c:choose>
	              </c:forEach>
                </div>
            </div>
        </div>
      </div>
 <!--hdmain 结束-->
 <script type="text/javascript" src="<ls:templateResource item='/resources/common/js/jquery.autocomplete.js'/>"></script>
 <script>
	function searchproduct(){
	   $("#searchform").attr("action","${contextPath}/list");
	   document.getElementById("searchform").submit();
	}
	function searchPShoproduct(){
	   $("#searchform").attr("action","${contextPath}/store/list/${shopId}");
	   document.getElementById("searchform").submit();
	}
    function goHotLink(msg){
        msg = encodeURI(msg);
        window.location.href = "${contextPath}/list?keyword="+msg;
    }
</script>
