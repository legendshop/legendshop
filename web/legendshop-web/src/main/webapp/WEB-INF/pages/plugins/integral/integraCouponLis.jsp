<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>积分中心-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link rel="stylesheet" type="text/css" media='screen' href="${contextPath}/resources/templets/css/integral-mall${_style_}.css" />
</head>

<body  style="background: #ffffff;">
	<div id="doc">
	    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
			<div id="bd">
            <!--crumb-->
      <div class="seller-cru" style="width:1190px;margin: 20px auto 10px;">
        <p>
          您的位置&gt;
          <a href="${contextPath}/home">首页</a>&gt;
          <a href="${contextPath}/integral/integralCenter">积分中心</a>&gt;
          <span style="color: #e5004f;">更多</span>
        </p>
      </div>   
            <!--crumb end-->
      <div class="integral-mall">
<!-- 会员已登录 -->
		
		<!-- 代金券 -->
        <div class="integral-list">
        
          <ul class="int-con">
          
          
          <c:forEach items="${list}" var="ls">
          	<c:if test="${ls.couponProvider eq 'shop'}">
            <li>
              <div class="int-con-lef">
                <div class="cut"></div>
                <div class="info">
                  <a href="#" class="store">${ls.shopName}</a>
                  <div class="pic">
                   <c:if test="${empty ls.couponPic }">
                  		<img src="${contextPath}/resources/templets/images/coupon/coupon-default.png"/>
                  	</c:if>
                  	<c:if test="${not empty ls.couponPic }">
                    	<img src="<ls:photo item='${ls.couponPic}'/>">
                    </c:if>
                  </div>
                </div>
                <dl class="value">
                  <dt>¥<em>${ls.offPrice}</em></dt>
                  <dd>购物满${ls.fullPrice}元可用</dd>
                  <dd class="time">有效期至<fmt:formatDate value="${ls.endDate}" pattern="yyyy-MM-dd HH:mm"/></dd>
                </dl>
                <div class="point">
                  <p class="required">需<em>${ls.needPoints}</em>积分</p>
                  <p><em>${ls.bindCouponNumber}</em>人已兑换</p>
                </div>
                <div class="button">
                <a href="javascript:void(0);" >立即兑换</a>
                <input type="hidden" value="${ls.couponId}"/>
                </div>
              </div>
            </li>
            </c:if>
            </c:forEach>
            
            
          </ul>
        </div>
        
		<!-- 红包 -->
        <div class="integral-list">
          
          <ul class="int-con">
          
          <c:forEach items="${list}" var="p">
          	<c:if test="${p.couponProvider eq 'platform'}">
            <li>
              <div class="int-con-lef">
                <div class="cut"></div>
                <div class="info">
                  <a href="#" class="store">${p.couponName}</a>
                  <p class="sto-cla">${p.name}</p>
                  <p class="sto-cla"></p>
                  <div class="pic">
                   <c:if test="${empty p.couponPic}">
                  	<img src="${contextPath}/resources/templets/images/coupon/red-default.png"/>
                  </c:if>
                  <c:if test="${not empty p.couponPic }">
                    <img src="<ls:photo item='${p.couponPic}'/>">
                    </c:if>
                  </div>
                </div>
                <dl class="value">
                  <dt>¥<em>${p.offPrice}</em></dt>
                  <dd>购物满${p.fullPrice}元可用</dd>
                  <dd class="time">有效期至<fmt:formatDate value="${p.endDate}" pattern="yyyy-MM-dd HH:mm"/></dd>
                </dl>
                <div class="point">
                  <p class="required">需<em>${p.needPoints}</em>积分</p>
                  <p><em>${p.bindCouponNumber}</em>人已兑换</p>
                </div>
                <div class="button">
                	<a href="javascript:void(0);" value="1" >立即兑换</a>
                	<input type="hidden" value="${p.couponId}"/>
                </div>
              </div>
            </li>
            </c:if>
          </c:forEach>  
          </ul>
            
		<div style="margin-top:10px;" class="page clearfix">
		<form:form action="${contextPath}/integral/integralCenter/list/${type}" method="post" id="from1">
			<input type="hidden" id="curPageNO" name="curPageNO"/>
				  <div class="p-wrap">
					<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
     			 </div>
     	</form:form>
	     </div>
        </div>

      </div>
   </div> 
		  
			       
		 <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	    
	</div>
	
	<script type="text/javascript">
		var contextPath="${contextPath}";
		$(function(){
			$(".button").bind("click",function(){
				var id=$(this).children("input[type='hidden']").val();
				var need=$(this).siblings("div[class='point']").children("p[class='required']").children("em").text();
				 
				layer.confirm("确定要兑换吗？需要'"+need+"'积分", {
					 icon: 3
				     ,btn: ['确定','关闭'] //按钮
				   }, function(){
					   var index=layer.load(1);
					   $.ajax({
							url:contextPath + "/p/coupon/receive/" + id,
					        type:"post",
					        dataType:'json',
					        async : false, //默认为true 异步   
					        success:function(result){
					        	if("OK"==result){
					        		layer.msg("恭喜您领取成功！",{icon:1,time:1000},function(){
						        		window.location.reload();
						        	});
					        	}else{
					        		layer.close(index);
					        		layer.msg(result,{icon:0,time:1000})
					        	}
					        },
					        error:function(e){
					        	window.location.href=contextPath + "/login";
					        }
						});
				   });
			});
		});
		
		
	function pager(curPageNO){
		document.getElementById("curPageNO").value=curPageNO;
		document.getElementById("from1").submit();
	};
	</script>
</body>
