<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>礼券领取- ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />

    <link rel="stylesheet" href="<ls:templateResource item='/resources/templets/css/applyCoupon.css'/>" />
    <link rel="stylesheet" href="<ls:templateResource item='/resources/templets/css/integral-mall${_style_}.css'/>" />
</head>
<body class="graybody">
	<div id="doc">
		   <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		   
		   <div id="hd">
				      <div class="integral-mall">

        <div class="integral-list" style="margin-top: 0;">
          <ul class="int-con" style="background: #fff;">
            <li style="margin: 20px 0;">
              <div class="int-con-lef">
                <div class="cut"></div>
                <div class="info">
                <c:if test="${coupon.couponProvider eq 'platform' }">
                  <a href="#" class="store">${coupon.couponName}</a>
                </c:if>
                <c:if test="${coupon.couponProvider eq 'shop' }">
                  <a href="${contextPath}/store/${coupon.shopId}" class="store">${coupon.shopName}</a>
                </c:if>
                  <div class="pic">
                  	<c:if test="${coupon.couponProvider eq 'shop' }">
                   	 	<c:if test="${empty coupon.couponPic }">
                  			<img src="${contextPath}/resources/templets/images/coupon/coupon-default.png"/>
                  		 </c:if>
                  		<c:if test="${not empty coupon.couponPic }">
                    		<img src="<ls:photo item='${coupon.couponPic}'/>">
                    	</c:if>
                    </c:if>
                    
                    <c:if test="${coupon.couponProvider eq 'platform' }">
                    	<c:if test="${empty coupon.couponPic }">
                  			<img src="${contextPath}/resources/templets/images/coupon/red-default.png"/>
                  		</c:if>
                  		<c:if test="${not empty coupon.couponPic }">
                    		<img src="<ls:photo item='${coupon.couponPic}'/>">
                    	</c:if>
                    </c:if>
                  </div>
                </div>
                <dl class="value">
                  <dt>¥<em>${coupon.offPrice}</em></dt>
                  <dd>购物满${coupon.fullPrice}元可用</dd>
                  <dd class="time">有效期至<fmt:formatDate value="${coupon.endDate}" pattern="yyyy-MM-dd HH:mm"/></dd>
                </dl>
                <div class="point">
                  <p><em>${coupon.bindCouponNumber}</em>人已兑换</p>
                </div>
                <div class="button"><a href="javascript:void(0);">确认领取</a></div>
                <input type="hidden" value="${coupon.couponId}" id="couponId">
              </div>
            </li>
          </ul>
        </div>
<!-- 热门代金券 -->

      </div>
				
		   </div>
		   
		   	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
</body>
<script>
    var contextPath = '${contextPath}';
</script>
<script src="<ls:templateResource item='/resources/templets/js/applyCoupon.js'/>"></script>
</html>
