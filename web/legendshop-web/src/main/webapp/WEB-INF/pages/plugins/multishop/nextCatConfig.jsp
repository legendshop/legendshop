<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>二级类目配置-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-category${_style_}.css'/>" rel="stylesheet">
	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/plugins/select/divselect.css'/>" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
	<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
<div id="Content" class="w1190">
				<div class="seller-cru">
						<p>
							您的位置>
							<a href="${contextPath}/home">首页</a>>
							<a href="${contextPath}/sellerHome">卖家中心</a>>
							<span class="on">
							<c:choose>
								<c:when test="${nsort.parentNsortId == null}">二级商品类目配置</c:when>
							<c:otherwise>三级商品类目配置</c:otherwise>
							</c:choose>
							</span>
						</p>
					</div>
					<%@ include file="included/sellerLeft.jsp" %>
					 <div class="seller-category">
				<div class="seller-com-nav">
					<ul>
						<li width="150px;" id="shopCatList"><a href="javascript:void(0);">一级类目列表</a></li>
						<li width="150px;"  id="nextCatList"><a href="javascript:void(0);">二级类目列表</a></li>
						<li width="150px;" id="nextCatConfigure" class="on"><a href="javascript:void(0);">二级类目配置</a></li>
					</ul>
				</div>
         <form:form action="${contextPath}/s/nextCat/save" method="post" id="form1" enctype="multipart/form-data">
         	<input id="nextShopCatId" name="id" value="${nextShopCat.id}" type="hidden">
            <input id="parentId" name="parentId" value="${shopCatId}" type="hidden">
            <input id="grade" name="grade" value="2" type="hidden">
        <div align="center">
        <table cellspacing="0" cellpadding="0" border="0" style=" margin-top:30px;" class="sendmes_table" >
     	 <tr>
        <th>
          		类目名称: <font color="ff0000">*</font>
       </th>
        <td>
           <input type="text" name="name" id="name" maxlength="30"  value="${nextShopCat.name}" style="width: 200px;">
        </td>
      </tr>
      <tr>
        <th>
          		次序：
       </th>
        <td>
                <input type="text" name="seq" id="seq" maxlength="30"  value="${nextShopCat.seq}" style="width: 60px;">
        </td>
      </tr>
      
     <!-- 二级类目的图片暂时没有地方用到 先注释掉上传  -->
     <%--  <tr>
    		<th>
    			图片：
    		</th>
    		<td>
    			<input type="file" name="file" id="file" size="30"/>
    			<input type="hidden" name="pic" id="pic" size="30" value="${nextShopCat.pic}"/>
    		</td>
    	</tr>	 --%>	
		<c:if test="${nextShopCat.pic!=null && nextShopCat.pic!='' }">
	    <tr>
	   		<th>原有图片</th>
	     	<td >
	      	<a href="<ls:photo item='${nextShopCat.pic}'/>" target="_blank"><img src="<ls:photo item='${nextShopCat.pic}'/>" height="50" width="50"/></a>
	      </td>
	    </tr>
   		</c:if>
       <tr>
       	<th></th>
           <td>
           		<a href="javascript:void(0)" onclick="submitTable();" class="btn-r"><s></s>提交</a>
				<a href="${contextPath}/s/nextShopCat/query?shopCatId=${shopCatId}" class="btn-g"><s></s>返回</a>
           </td>
       </tr>
      </table>
     </div>
  </form:form>
  </div>
</div>
<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select/divselect.js'/>"></script>
<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/nextCatConfig.js'/>"></script>
<script type="text/javascript">
	var contextPath="${contextPath}";
	var shopCatId = "${shopCatId}";
</script>
