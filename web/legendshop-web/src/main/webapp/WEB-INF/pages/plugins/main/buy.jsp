<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<title>购物车-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/buy.css'/>" rel="stylesheet" />
<link rel="stylesheet" href="<ls:templateResource item='/resources/common/css/jquery.autocomplete.css'/>">
</head>
<body>
	<div id="doc">
		<div id="hd">

			<!--hdtop 开始-->
			<%@ include file="home/header.jsp"%>
			<!--hdtop 结束-->

			<div id="hdmain_cart">
				<div class="yt-wrap">
					<h1 class="ilogo">
						<a href="${contextPath}/">
							<img src="<ls:photo item="${systemConfig.logo}"/>" style="max-height:89px;" />
						</a>
					</h1>

					<div class="cartnew-title">
						<ul class="step_h">
							<li class="done"><span>购物车</span></li>
							<li class=""><span>填写核对订单信息</span></li>
							<li class=" "><span>提交订单</span></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!--hd end-->

		<div id="bd">
			<%@ include file="buyTable.jsp"%>
		</div>
		<!--bd end-->
		
		<!--猜你喜欢 -->
		<div id="desgoods" style="width: 1190px;margin: auto; text-align: right;"></div>
		<!--猜你喜欢end -->

		<!----foot---->
		<%@ include file="home/bottom.jsp"%>
		<!----foot end---->

	</div>
	<img src="<ls:url address='/resources/common/images/indicator.gif'/>" class="editAdd_load" />
	<div class="black_overlay"></div>
<script charset="utf-8" src="<ls:templateResource item='/resources/templets/js/shopcart.js'/>"></script>
<script type="text/javascript">
	var loginUserName = "${userName}";
	var contextPath = "${contextPath}";
	var tips;
	$(function() {
		$.ajax({
			url : "${contextPath}/like",
			type : "post",
			data : {
				"count" : 6
			},
			dataType : 'html',
			async : false, //默认为true 异步   
			success : function(result) {
				$("#desgoods").html(result);
			},
			error : function(e) {
			}
		});

	});
</script>
</body>
</html>
