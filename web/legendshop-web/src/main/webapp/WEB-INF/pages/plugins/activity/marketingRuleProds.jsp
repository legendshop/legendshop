<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
 <table class="ncm-default-table">
			<thead>
				<tr>
					<th class="w200 tl">商品名称</th>
					<th class="w150 tl">商品价格</th>
					<th class="w150 tl">商品库存</th>
					<th class="w150 tl">操作</th>
				</tr>
			</thead>
			<tbody id="ruleProds">
				<c:forEach items="${list}" var="prods" varStatus="status">
							   <tr>
									<td>
										<a style="width: 390px;padding: 0 5px;background: #fff !important;color: #323333 !important;" target="_blank" href="<ls:url address='/views/${prods.prodId}'/>">${prods.prodName}</a>
										<input type="hidden" value="${prods.skuId }" class="skuId">
						  			</td>
									<td>
										<fmt:formatNumber value="${prods.cash}" pattern="#0.00#"/>
									</td>
									<td>${prods.stock}</td>

									 <td>
										 <c:if test="${state!=1}">
									   <span>
										    <a href="javascript:void(0);" id="${prods.id}" onclick="deletemark(this);">移除</a>
										</span>
										 </c:if>
									</td>
								</tr>
				</c:forEach>
			</tbody>
</table>

    
			 <div style="margin-top:10px;" class="page clearfix">
				  <div class="p-wrap">
				        <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/>
				  </div>
		  	</div>
		  	
			<div class="clear"></div>
