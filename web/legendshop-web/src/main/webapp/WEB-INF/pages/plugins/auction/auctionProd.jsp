<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/prodSelectAlert.css'/>" rel="stylesheet"/>	
	<table border="0" cellpadding="0" cellspacing="0" style="margin: 30px 0;">
			<tbody>
				<tr style="background: #f9f9f9;">
					<td width="70px">商品图片</td>
					<td width="170px">商品名称</td>
					<td width="100px">商品价格</td>
					<td width="100px">可销售库存</td>
				</tr>
				<tr class="first" >
					<td>
						<img src="<ls:images item='${sku.pic}' scale='3' />" >
					</td>
					<td>
						<span class="name-text">${sku.name}</span>
					</td>
					<td>${sku.price}</td>
					<td>${sku.stocks}</td>
				</tr>
			</tbody>
			<input type="hidden" id="skuId" name="skuId" value="${sku.skuId}"/>
			<input type="hidden" value="${sku.prodId}" name="prodId" id="prodId"/>
	</table>
