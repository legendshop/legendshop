<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
 <c:set var="leftAdv" value="${floor.floorItems.LA}"></c:set>
      <c:set var="STfloorItem" value="${floor.floorItems.ST}"></c:set>
       <c:set var="subFloors" value="${floor.subFloors}"></c:set>
          <!--首页楼层样式 广告+分类+商品列表-->
          <div class="clear"></div>
            <div class="yt-wrap bf_floor_new clearfix">
             <div class="pro-left">
               <div class="pro-left-tit">
                 <h4>${floor.name}</h4>
               </div>
               <div class="pro-left-con">
                 <div class="pro-left-adv">
                   <c:forEach items="${leftAdv}" var="adv" varStatus="index">
				      	<a target="_blank" title="${adv.title}" href="${adv.linkUrl}">
				      		<img class="lazy" alt="${adv.title}" src="${contextPath}/resources/common/images/loading1.gif" data-original="<ls:photo item='${adv.picUrl}'/>" style="width: 210px;height: 280px;">
				      	</a>
			      	</c:forEach>
                 </div>
                 <div class="pro-left-rec">
                   <ul>
                      <c:forEach items="${STfloorItem}" var="floorItem" varStatus="index">
      		 		     <li><a target="_blank" title="${floorItem.categoryName}" href="${contextPath}/list?categoryId=${floorItem.referId}" style="text-align: center;">${floorItem.categoryName}</a></li>
      		 		     <c:forEach items="${floorItem.floorSubItemSet}" var="floorSubItem">
				             <li><a target="_blank" title="${floorSubItem.nsortName}" href="${contextPath}/list?categoryId=${floorSubItem.subSortId}" style="text-align: center;">${floorSubItem.nsortName}</a></li>
				         </c:forEach>
      		          </c:forEach>
                    </ul>
                 </div>
               </div>
             </div>
             <div class="pro-right">
               <div class="pro-right-tit">
                 <ul>
                    <c:forEach items="${subFloors}" var="subFloor"  varStatus="index" end="4">
		    			<li  <c:if test="${index.index eq 0 }">class="arrow"</c:if>  id="${subFloor.sfId}">
		    			<i></i>
		    			<h3>${subFloor.name}</h3></li>
		            </c:forEach>
                 </ul>
               </div>
                <c:forEach items="${subFloors}" var="subFloor"  varStatus="index" end="4">
                   <div class="pro-right-con" <c:if test="${index.index ne 0 }">style="display: none;"</c:if>  >
                      <c:forEach items="${subFloor.subFloorItems}" var="subFloorItem"  varStatus="sub_index" end="9">
	                      <ul>
	                         <li>
			                    <dl>
			                     <dt>
			                        <a href="${contextPath}/views/${subFloorItem.productId}">
			                          <img class="lazy" alt="${subFloorItem.productName}" src="${contextPath}/resources/common/images/loading1.gif" data-original="<ls:images item='${subFloorItem.productPic}' scale='1' />" >
			                        </a>
			                     </dt>
			                     <dd class="pro-right-dd">
			                        <a href="${contextPath}/views/${subFloorItem.productId}">${subFloorItem.productName}</a>
			                     </dd>
			                     <dd style="height:21px;">
			                        <em>¥${subFloorItem.productCash}</em>
			                        <span> <c:if test="${not empty subFloorItem.productPrice}">
					               		¥${subFloorItem.productPrice}
					               </c:if></span>
			                     </dd>
			                   </dl>
			                </li>
	                    </ul>
                    </c:forEach>
                   </div>
                </c:forEach>
             </div>
 </div>
        <!--首页楼层样式 广告+分类+商品列表-->