<!doctype html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>申请退换货-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/return.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
<div id="doc">
   <div id="hd">
   		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
   </div>
   <div id="bd">
 <div class=" yt-wrap" style="padding-top:10px;"> 
    	  <%@include file='usercenterLeft.jsp'%>
     <div class="right_con">
     
         <div class="o-mt"><h2>退换货记录</h2></div>
         
         <div class="pagetab2">
                 <ul>           
                   <li><span><a href="${contextPath}/p/myreturnOrder">退货记录</a></span></li>                  
                   <li  class="on"><span>申请退换货</span></li>      
                 </ul>       
         </div>
        
         <div id="recommend" class="m10 recommend" style="display: block;">
                 
                <table width="100%" cellspacing="0" cellpadding="0" class="buytable">
                    <tbody><tr>
                        <th style="text-align: center;">订单编号</th>
                        <th style="text-align: center;">商品名称</th>
                        <th style="text-align: center;">退换货数量</th>
                        <th style="text-align: center;">商品单价</th>
                    </tr>
               
                    <tr>
                          <td><a href="${contextPath}/p/orderDetail/${subItem.subNumber}" target="_blank">${subItem.subNumber}</a></td>
                          <td><div class="img-list"><a target="_blank" href="${contextPath}/views/${subItem.prodId}"><img  alt="${subItem.prodName}" src="<ls:images item='${subItem.pic}' scale='3'/>"/></a></div>${subItem.prodName}</td>
                          <td>${subItem.basketCount}</td>
                          <td class="order-doi">￥${subItem.cash}</td>
                    </tr>
               
           </tbody></table>
				
				              		
	     </div>
        
        
        
                 <div class="formbox m10">
             <div class="item">
                <span class="itemName">服务类型：</span>
                <div class="fl phoneContainer tui">
                    退货
               </div>   
           	 </div>
             
            <div class="item">
                <span class="itemName">退款金额：</span>
                <div>
                  <input type="text" name="orderTotalPrice" id="orderTotalPrice" value="${cash }" style="border: #CCC 1px solid;height:25px;">
                  &nbsp;&nbsp;(退款上限:<span style="color:red;">${cash }</span>)
                  <input type="hidden" name="cash" id="cash" value="${cash }">
                </div>   
           	 </div>
             
             
             <div class="item">
                <span class="itemName">问题描述：</span>
        
                <div class="">
                  <textarea name="desc" id="desc" class="textarea_t" maxlength="500"></textarea>
                </div>   
           	 </div>
           	 
             <div class="item" id="submitItem">
                <span class="itemName"></span>
                <div>
                   <input type="file" id="file" name="file" onchange="javascript:uploadFile(this)">
                  <p class="t_tishi">图片信息：为了帮助我们更好的解决问题，请你上传图片（大小少于1M）</p>
                </div>
            </div>
            
             <div class="item">
                <span class="itemName">图片凭证：</span>
                <div class="img-files-img">
                	<ul>
                	</ul>
               </div>  
        	 </div>
        	  <div id="payinfo">
	   		 </div>
         
         <div class="item">
                <span class="itemName"></span>
        
                <div class="fl phoneContainer tui">
                    <input type="button" onclick="saveReturnOrder();" class="submit" value="保存" o>
               </div>  
            
         </div>
     </div>
 </div>
   </div>
<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>"></script>
<script src="<ls:templateResource item='/resources/common/js/ajaxfileupload.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/applyOrderReturn.js'/>"></script>
<script type="text/javascript">
	var contextPath="${contextPath}";
	$(document).ready(function() {
		userCenter.changeSubTab("myreturnorder");
	});
	var  prods='${prods}';
 	var subNum='${subNum}';
		 //宝贝图片处的文件上传，通过Ajax方式上传图片
				 function uploadFile(ths){
				 		var result = checkImgType(ths) && checkImgSize(ths,1024);
				  		if(!result){
				  			return false;
				  		}
					    var uploadNumber=$(".img-files-img ul li").length;
						if (uploadNumber == 3) {
							alert("您最多可以上传3张图片");
							return false;
						}else{
							  $.ajaxFileUpload({
							      url: contextPath+'/p/uploadReturnOrderFile',             
							      secureuri:false,
							      fileElementId:'file',                         
							      dataType: 'json',
							      error: function (data, status, e){
							          alert(data);
							       },
							       success: function (data, status){  
							     	  if(data =="fail"){
							     		  alert(data);
							     	 }else{
							     		var url = "<ls:photo item='"+data+"' />";
					                    $(".img-files-img ul").append("<li><img style='max-width:150px;max-height:150px;' src='"+url+"' alt=''><a  fileName='"+data+"' href='javascript:void(0);' onclick='delImagesPath(this);'>删除</a><input name='imgurl' value='"+data+"' type='hidden' ></input></li>");
							     	 }
							       }
							    });
						}
				 }
				 
</script>
</body>
</html>
