<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<html>
<head>
<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
<title>商品分类-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}"/>
<meta name="description" content="${systemConfig.description}" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
</head>
<body>
<div id="bd">
<%@include file='home/top.jsp'%>
       <div style="padding:30px 0 20px;" class="yt-wrap">
            <div class="pagetab2">
               <ul>           
                 <li class="on"><span>全部商品分类</span></li>                  
                 <li><span><a href="<ls:url address='/allbrands'/>">全部品牌</a></span></li>   
               </ul>       
            </div>
            
            <div class="i-w">
            	<div class="text">更多特价产品，请进入以下频道页面</div>
	    		<div class="tab clearfix">
	        		<ul style="padding:0;">
	                	<c:forEach  items="${sortList}" var="sort" >
				 			<li class="l1"><a href='<ls:url address="/list?categoryId=${sort.categoryId }"/>'>${sort.categoryName}</a></li>
						</c:forEach>
					</ul>
               	</div> 
            	<div class="clear"></div>
           	</div>
           
			<div style="padding-top:20px;" id="allsort" class="w">
             <!----左手边---->
				<div class='fl'>
					<c:if test="${halfSize >= 0}">
	 					<c:forEach  items="${sortList}" var="sort"  begin="0" end="${halfSize - 1}">
	 		   				<div class="m">
						        <div class="mt">
							        <h2><a href='<ls:url address="/list?categoryId=${sort.categoryId }"/>'>${sort.categoryName}${status.index}</a></h2>
						        </div>
					        	<div class="mc">
					          		<c:if test="${not empty sort.childrens}">
							        	<c:forEach  items="${sort.childrens}" var="children" >
							        		<dl class="fore">
								                <dt><a href='<ls:url address="/list?categoryId=${children.categoryId }'" />'>${children.categoryName }</a></dt>
								                <dd>
								                	<c:if test="${not empty children.childrens}">
									                    <c:forEach  items="${children.childrens }" var="third" >
									                	<em><a href='<ls:url address="/list?categoryId=${third.categoryId }'" />'>${third.categoryName}</a></em>
									                    </c:forEach>
								                	</c:if>
								                </dd>
						                	</dl>
							           </c:forEach>
					          		</c:if>
					        	</div>
				       	 	</div>
	 					</c:forEach>
					</c:if>
  				</div>
                <!-- 右手 -->
                <div class='fr'>
					<c:forEach  items="${sortList}" var="sort"  begin="${halfSize}">
						<div class="m">
					        <div class="mt">
						        <h2><a href='<ls:url address="/list?categoryId=${sort.categoryId }"/>'>${sort.categoryName}${status.index}</a></h2>
					        </div>
					        <div class="mc">
	                        	<c:if test="${not empty sort.childrens}">
						        	<c:forEach  items="${sort.childrens}" var="children" >
							         	<dl class="fore">
							            	<dt><a href='<ls:url address="/list?categoryId=${children.categoryId }'" />'>${children.categoryName }</a></dt>
							                <dd>
								                <c:if test="${not empty children.childrens}">
								                    <c:forEach  items="${children.childrens }" var="third" >
								                		<em><a href='<ls:url address="/list?categoryId=${third.categoryId }'" />'>${third.categoryName}</a></em>
								                    </c:forEach>
								                </c:if>
							                </dd>
						                </dl>
						           </c:forEach>
					        	</c:if>
					        </div>
						</div>
					</c:forEach>
  				</div>
       		</div>
            <div class="clear"></div>
		</div>
 	</div>
  	<%@include file='home/bottom.jsp'%>
</div>
</body>
</html>