	<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
	<%@ include file='/WEB-INF/pages/frontend/templetGoat/common/taglib.jsp' %>
	
		<c:forEach items="${predeposit.pdCashLogs.resultList}" var="pred">
					   <tr class="gra">
						    <td>
						          <c:choose>
										<c:when test="${pred.amount>0}">
										   +<fmt:formatNumber value="${pred.amount}" pattern="#,#0.00#"/>
										</c:when>
										<c:otherwise>
										   0.00
										</c:otherwise>
									</c:choose>
						    </td>
						    <td>
				                   <c:choose>
										<c:when test="${pred.amount<0}">
										   <fmt:formatNumber value="${pred.amount}" pattern="#,#0.00#"/>
										</c:when>
										<c:otherwise>
										   0.00
									    </c:otherwise>
								  </c:choose>
						    </td>
					        <td><fmt:formatDate value="${pred.addTime}" type="both"/></td>
							<td>
							<a class="commodity-prize-on" href="<ls:url address='/p/m/predeposit/logDetail/${pred.id}'/>">查看</a>
							</td>
					   </tr>
		</c:forEach>

