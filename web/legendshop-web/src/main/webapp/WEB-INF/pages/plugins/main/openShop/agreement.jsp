<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>店铺协议-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
</head>
<body>
<div id="doc">

    <div id="hd">
        <%@ include file="../home/top.jsp" %>

    </div>

    <div id="bd">
        <div class="yt-wrap" style="padding:0px 0 20px;">
            <div class="seller-cru" style="width:1190px;margin: 20px auto 20px;">
                <p>
                    您的位置&gt;
                    <a href="#">首页</a>&gt;
                    <span style="color: #e5004f;">免费开店</span>
                </p>
            </div>
            <div class="pagetab2">
                <ul>
                    <li class="on"><span>签订入驻协议</span></li>
                    <li><span>入驻类型选择</span></li>
                    <li><span>店铺信息提交</span></li>
                    <li><span>审核进度查询</span></li>
                </ul>
            </div>
            <div class="main">
                <div class="settled_box">
                    <h3><span class="settled_title">协议确定</span></h3>
                    <div class="settled_inbox">
                        <div class="settled_p">
                    	<textarea style="width: 100%;height: 400px;" readonly="readonly">
                            ${systemConfig.openShopProtocolTemplate}
                        </textarea>
                        </div>
                    </div>
                    <div class="settled_bottom"><span><a href="${contextPath}/p/selectShopType" class="settled_btn"><em>同意以上协议，下一步</em></a></span></div>
                </div>
            </div>
        </div>

    </div>
    <%@ include file="../home/bottom.jsp" %>
</div>
</body>
</html>
