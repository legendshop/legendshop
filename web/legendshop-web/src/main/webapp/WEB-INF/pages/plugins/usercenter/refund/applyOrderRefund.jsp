<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>

<html>
  <head>
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>申请订单退款-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
  </head>
<body class="graybody">
	   <div id="bd">
	   		 <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
			 <div class=" yt-wrap" style="padding-top:10px;"> 
       	 		<%@include file='../usercenterLeft.jsp'%>
		
			    <!-----------right_con-------------->
			     <div class="right_con">
			     	
			         <div class="o-mt"><h2>申请退款</h2></div>
			         <!-- 操作类型选择 -->
			         <div class="pagetab2">
			                 <ul>           
				                   <li class="on"><span>仅退款</span></li>
			                 </ul>
			         </div>
			
					<!-- 操作提示 -->
			         <div class="alert">
			            <h4>操作提示：</h4>
			            <ul>
			              <li>1. 若您未收到货，或已收到货且与商家达成一致不退货仅退款时，请选择<em>“仅退款”</em>选项。</li>
			              <li>2. 若为商品问题，或者不想要了且与商家达成一致退货，请选择<em>“退货退款”</em>选项，退货后请保留物流底单。</li>
			              <li>3. 若提出申请后，商家拒绝退款或退货，可再次提交申请或选择<em><a href="javascript:void(0);" onclick="qqLink('${systemConfig.qqNumber}');" >“商品投诉”</a></em>，请求商城客服人员介入。</li>
						  <li>4. 成功完成退款/退货；经过商城审核后，退款金额原路退回。</li>
			            </ul>
			        </div>
			        
			        <!-- 申请退款块 -->
			        <div id="refundBox" >
				        <!-- 申请进度 -->
				        <div class="ret-step">
				          <ul>
				            <li class="done"><span>买家申请退款</span></li>
				            <li><span>商家处理退款申请</span></li>
				            <li><span>平台审核，退款完成</span></li>
				          </ul>
				        </div>
			        
				        <!-- 申请"仅退款"表单 -->
				        <form:form id="refundForm" action="${contextPath }/p/refund/save" method="POST" enctype="multipart/form-data">
				        	<input type="hidden" id="orderId" name="orderId" value="${refund.orderId}"/>
					        <div class="write-apply">
					          <div class="ste-tit"><b>请填写退款申请</b></div>
					          <div class="ste-con">
					            <ul id="formContent">
					              <li>
					              	<span><em>*</em>退款原因：</span>
					              	<select id="buyerMessage" name="buyerMessage">
					              		<option value="">请选择退款原因</option>
					              		<option value="不能按时发货">不能按时发货</option>
					              		<option value="不想要了">不想要了</option>
					              		<option value="信息填写错误，重新拍">信息填写错误，重新拍</option>
					              	</select>
					              </li>
					              <li>
					              	<input type="hidden" id="refundAmount" name="refundAmount" value="${refund.refundAmount }"/>
					              	<span><em>*</em>退款金额：</span><em style="color: #e5004f;height: 30px;line-height: 30px;">￥&nbsp;<fmt:formatNumber value="${refund.refundAmount}" type="currency" pattern="0.00"></fmt:formatNumber></em>
					              </li>
					              <li>
					              	<span><em>*</em>退款说明：</span>
					              	<textarea id="reasonInfo" name="reasonInfo" rows="3" cols="80"></textarea>
					              </li>
					              <c:if test="${order.status ne 2}">
						              <li>
						              	<span style="line-height: 20px;">上传凭证1：</span>
						              	<input type="file" id="photoFile1" name="photoFile1" onchange="checkImgFile(this);"/>
						              	<i>（图片不能超过5M）</i> 
						              	<input type="button" class="btn-g" id="addBtn" value="添加" onclick="addFile(this);"/>
						              </li>
					              </c:if>
					            </ul>
					          </div>
					        </div>
					         <br>
					         <div class="ste-but" style="margin-left:111px;">
					           <input type="submit" value="确认提交" class="btn-r" />&nbsp;
					           <input type="button" value="取消并返回" class="btn-g" onclick="window.location.href='${contextPath}/p/myorder'"/>
					         </div>
				         </form:form>
				     </div>
				    <!-- 申请退款块 结束 -->
			     </div>
			     <!-----------right_con 结束-------------->
			     
			    <div class="clear"></div>
			 </div>
		   
		 	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
		</div>
		<script type="text/javascript">
			var maxRefundMoney = maxRefundMoney = Number('${refund.refundAmount }');//最多退款金额
		</script>
		<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
		<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/refund/refund.js'/>"></script>
		<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/refund/applyOrderRefund.js'/>"></script>
	</body>
</html>
