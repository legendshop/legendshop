<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>包邮设置-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/active/actives${_style_}.css'/>" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">包邮设置</span>
				</p>
			</div>
			
			<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>

             
			<div id="rightContent" class="right_con">
			<div class="o-mt">
				<h2>包邮设置</h2>
			</div>
		
			<div class="ncm-default-form">
					<dl>
						<dt><i class="required">*</i>是否开启包邮：</dt>
						<dd>
					  		<c:choose>
					  			<c:when test="${shopDetail.mailfeeSts==1}">
					  				<label for="mailfeeSts_1">
					  					<input type="radio" checked="checked" id="mailfeeSts_1" value="1" name="mailfeeSts">是
					  				</label>
								   <label for="mailfeeSts_0"  style="margin-left: 5px;"> 
								   		<input type="radio" id="mailfeeSts_0" value="0" name="mailfeeSts">否
									</label>
					  			</c:when>
					  			<c:otherwise>
					  				<label for="mailfeeSts_1">
					  					<input type="radio" id="mailfeeSts_1" value="1" name="mailfeeSts">是
					  				</label>
								   <label for="mailfeeSts_0"  style="margin-left: 5px;"> 
								   		<input type="radio" checked="checked" id="mailfeeSts_0" value="0" name="mailfeeSts">否
									</label>
					  			</c:otherwise>
					  		</c:choose>
						</dd>
					</dl>
					
					<dl id="mailfeeType_all" >
						<dt><i class="required">*</i>促销类型：</dt>
						<dd>
							<c:choose>
					  			<c:when test="${shopDetail.mailfeeType==2}">
					  				<label for="mailfeeType_1"> 
							        <input type="radio" id="mailfeeType_1" value="1" name="mailfeeType" >满金额包邮
							       </label>
								    <label for="mailfeeType_2"  style="margin-left: 10px;"> 
							           <input type="radio" id="mailfeeType_2" value="2" name="mailfeeType" checked="checked">满件包邮
							        </label>
					  			</c:when>
					  			<c:otherwise>
					  				<label for="mailfeeType_1"> 
							        <input type="radio" id="mailfeeType_1" value="1" name="mailfeeType" checked="checked">满金额包邮
							       </label>
								    <label for="mailfeeType_2"  style="margin-left: 10px;"> 
							           <input type="radio" id="mailfeeType_2" value="2" name="mailfeeType" >满件包邮
							        </label>
					  			</c:otherwise>
					  		</c:choose>
						  
					         <span class="error-message"></span>
						</dd>
					</dl>
					
					<dl id="mailfeeCon_rule_all">
				      <dt><i class="required">*</i>包邮规则：</dt>
				      <dd>
				           <div  id="mailfeeCon_rule_1" <c:if test="${shopDetail.mailfeeType==2}">style="display:none;"</c:if> >
				            <div class="ncsc-mansong-rule">
				              <span>满&nbsp;<input type="text" class="text w50" name="mailfeeCon" maxlength="8" id="mailfeeCon_1" 
				              	<c:if test="${shopDetail.mailfeeSts==1 && shopDetail.mailfeeType==1}">value="<fmt:formatNumber pattern="0.00" value="${shopDetail.mailfeeCon}"/>"</c:if>>
				              	<em class="add-on">元</em></span>
				              <span><em class="add-on">包邮</em></span> 
				                <div class="gift" id="mansong_goods_item"></div>
				                </div>
				             </div>
				            <div id="mailfeeCon_rule_2" <c:if test="${empty shopDetail.mailfeeType or shopDetail.mailfeeType==1}">style="display:none;"</c:if> >
				                <div class="ncsc-mansong-rule">
				                 	<span>满&nbsp;<input type="text" class="text w50" name="mailfeeCon" id="mailfeeCon_2" 
				                 	<c:if test="${shopDetail.mailfeeSts==1 && shopDetail.mailfeeType==2}">value="<fmt:formatNumber pattern="0" value="${shopDetail.mailfeeCon}"/>" </c:if>maxlength="5" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')">
									<em class="add-on">件</em></span>
				                  <span><em class="add-on">包邮</em></span>
				                  <div class="gift" id="mansong_goods_item"></div>
				                </div>
				             </div>
				            <span class="error-message"></span>
				      </dd>
				    </dl>
				    
					<dl class="bottom">
						<dt>&nbsp;</dt>
						<dd>
							<label class="submit-border">
							<input type="button" value="保存"  class="submit" onclick="saveMailingFee();">
							</label>
						</dd>
					</dl>
			</div>
			
			<div class="clear"></div>
		</div>
				  
		</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
    <script src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>" type="text/javascript"></script>
    <script src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/maillingFeeSetting.js'/>"></script>
	<script type="text/javascript">
		var contextPath="${contextPath}";
	</script>
</body>
</html>