<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>我的订单 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>
<style>
.goods-name2 dt a:hover {
	color: #e5004f !important;
}
</style>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            <!----两栏---->
			 <div class=" yt-wrap" style="padding-top:10px;"> 
			    <%@include file='usercenterLeft.jsp'%>
			    <!-----------right_con-------------->
			     <div class="right_con">
			         <div class="o-mt"><h2>订单中心</h2></div>
			         <div class="pagetab2">
		                 <ul>           
		                   <li ><span><a href="<ls:url address='/p/myorder'/>">订单列表</a></span></li>                  
		                   <li class="on" ><span>回收站</span></li>      
		                 </ul>       
			         </div>
			          <form:form  action="${contextPath}/p/myorderdels" method="post" id="order_search">  
				          <input type="hidden" value="${curPageNO==null?1:curPageNO}" name="curPageNO">    
				         <div class="sold-ser sold-ser-no-bg clearfix">
				         	<div class="fr">
					         	<div class="item">
				             		 订单状态：
				             		 <select name="state_type" class="item-sel">
											<option value="" >所有订单</option>
											<option value="4" <c:if test="${state_type==4}">selected="selected"</c:if>>已完成</option>
											<option value="5" <c:if test="${state_type==5}">selected="selected"</c:if>>已取消</option>
									</select>
								</div>
								<div class="item">			
				              		下单时间：<input type="text" value='<fmt:formatDate value="${paramDto.startDate}" pattern="yyyy-MM-dd" />' id="startDate" name="startDate" class="text Wdate item-inp" readonly="readonly">
									-
									<input type="text" value='<fmt:formatDate value="${paramDto.endDate}" pattern="yyyy-MM-dd" />' id="endDate" name="endDate" class="text Wdate item-inp" readonly="readonly">
								</div>		
								<div class="item">	
				                 	订单号：<input type="text" value="${paramDto.subNumber}" name="order_sn" class="item-inp" style="width:200px;">
				                  <input type="submit" id="search_order" class="btn-r" value="查 询">
						        </div>
					         </div>
				         	</div>
			        </form:form>
					<table class="ncm-default-table order sold-table">
						<thead>
							<tr>
								<th class="w10"></th>
								<th colspan="2">商品</th>
								<th class="w90">单价（元）</th>
								<th class="w90">数量</th>
								<th class="w110">订单金额</th>
								<th class="w90">交易状态</th>
								<th class="w120" style="border-right: 1px solid #e4e4e4">交易操作</th>
							</tr>
						</thead>
						<c:choose>
				           <c:when test="${empty requestScope.list}">
					              <tr>
								 	 <td colspan="20">
								 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
								 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
								 	 </td>
								 </tr>
				           </c:when>
				           <c:otherwise>
				             <c:forEach items="${requestScope.list}" var="order" varStatus="orderstatues">
				             	<c:choose>
				             		<c:when test="${order.subType eq 'PRE_SELL'}">
				             			<!-- ############################## 预售订单 start ########################## -->
				             			<tbody class="pay">
											<tr>
												<td class="sep-row" colspan="8"></td>
											</tr>
											<tr>
												<th colspan="8" style="height: 35px;background-color: #f9f9f9;">
													<span class="ml10">订单号：${order.subNum} </span>
													<span class="ml10">
														<c:if test="${order.subType eq 'SECKILL'}">秒杀订单</c:if>
										            	<c:if test="${order.subType eq 'NORMAL'}">普通订单</c:if>
										            	<c:if test="${order.subType eq 'GROUP'}">团购订单</c:if>
										            	<c:if test="${order.subType eq 'MERGE_GROUP'}">拼团订单</c:if>
										            	<c:if test="${order.subType eq 'PRE_SELL'}">预售订单</c:if>
													</span>
													<span>下单时间： <fmt:formatDate value="${order.subDate}" type="both" /></span> 
													<span>
													     <a title="${order.shopName}" href="<ls:url address="/store/${order.shopId}" />">${order.shopName}</a>
													</span> 
													<a onclick="restoreOrder('${order.subNum}');" class="order-trash" href="javascript:void(0);"><i class="icon-trash"></i>还原订单</a>
													<!-- 放入回收站 --> <!-- 还原订单 -->
												</th>
											</tr>
											
											<c:forEach items="${order.subOrderItemDtos }" var="orderItem" varStatus="orderItemStatus">
											
										    <c:set value="0" var="notCommedCount" /> 
											<c:if test="${orderItem.commSts eq 0}">
									    		<c:set value="${notCommedCount + 1}" var="notCommedCount" /> 
									    	</c:if>
											<!-- S 商品列表 -->
											  <tr>
												<td class="w70" align="right">
													<div class="ncm-goods-thumb" style="    margin-left: 10px;">
														<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">
															<img onmouseout="toolTip()" onmouseover="toolTip();" src="<ls:images item='${orderItem.pic}' scale='3'/>">
														</a>
													</div>
												</td>
												<td class="tl"><dl class="goods-name2">
														<dt>
															<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">${orderItem.prodName}</a>
														</dt>
														<dd>${orderItem.attribute}</dd>
													</dl>
												</td>
												<td><p> ${orderItem.cash}</p></td>
												<td>x${orderItem.basketCount}</td>
									           <td class="bdl">
												    <p class="">
														<strong>${order.actualTotal}</strong>
													</p>
													<p title="支付方式：">
													  <c:choose>
									                		<c:when test="${order.payManner eq 1}">货到付款</c:when>
									                		<c:when test="${order.payManner eq 2}">在线支付</c:when>
									                		<c:when test="${order.payManner eq 2}">门店支付</c:when>
									                </c:choose>
												</td>
												<!-- 如果是第一行 -->
												<c:if test="${orderItemStatus.index eq 0 }">
													<c:choose>
														<c:when test="${fn:length(order.subOrderItemDtos) gt 0 }"><!-- 如果有多个订单项 -->
															<td class="bdl" rowspan="${fn:length(order.subOrderItemDtos) }">
														</c:when>
														<c:otherwise>
															<td class="bdl">
														</c:otherwise>
													</c:choose>
												    <p>
													<c:choose>
														 <c:when test="${order.status eq 1 }">
														 	<span class="col-orange">待付款</span>
														 </c:when>
													     <c:when test="${order.status eq 2 }">
													    	<span class="col-orange"> 待发货</span>
													     </c:when>
														 <c:when test="${order.status eq 3 }">
														 	<span class="col-orange">待收货</span>
														 </c:when>
														 <c:when test="${order.status eq 4 }">已完成</c:when>
														 <c:when test="${order.status eq 5 }">交易关闭</c:when>
													</c:choose>
												    </p> <!-- 订单查看 -->
													<p>
														  <a target="_blank" href="${contextPath}/p/orderDetail/${order.subNum}">订单详情</a>
													</p>
												</td>
												
												<c:choose>
													<c:when test="${fn:length(order.subOrderItemDtos) gt 0 }"><!-- 如果有多个订单项 -->
														<td class="bdl bdr" rowspan="${fn:length(order.subOrderItemDtos)}" colspan="2">
													</c:when>
													<c:otherwise>
														<td class="bdl bdr" colspan="2">
													</c:otherwise>
												</c:choose>
													<p>
													   	<a class="btn-r" onclick="dropOrder('${order.subNum}');" href="javascript:void(0);">永久删除</a>
													</p> 
												</td>
												</c:if>
											</tr>
											</c:forEach>
											 <tr style="color:#999;">
								                <td colspan="2">阶段1：定金</td>
								                <td colspan="2">
													<c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
													¥<span class="preDepositPrice"><fmt:formatNumber value="${order.preDepositPrice+order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber></span>
													(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
													</c:if>
													<c:if test="${order.payPctType eq 1}"><!-- 定金支付 -->
													¥<span class="preDepositPrice"><fmt:formatNumber value="${order.preDepositPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
													</c:if>
												</td>
								                <td colspan="4" style="border-right: 1px solid #e6e6e6;">
								                	<c:if test="${order.isPayDeposit eq 0 }">未完成</c:if>
								                	<c:if test="${order.isPayDeposit eq 1 }">已完成</c:if>
								                </td>
								              </tr>
								              <tr style="color:#999;">
								                <td colspan="2">阶段2：尾款</td>
								                <td colspan="2">
													<c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
													¥<span class="finalPrice"><fmt:formatNumber value="${order.finalPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
													</c:if>
													<c:if test="${order.payPctType eq 1}"><!-- 定金支付 -->
													¥<span class="finalPrice"><fmt:formatNumber value="${order.finalPrice+order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber></span>
													(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
													</c:if>
												</td>
								                <td colspan="4" style="border-right: 1px solid #e6e6e6;">
								                	<c:choose>
								                		<c:when test="${order.isPayFinal eq 0}">未完成</c:when>
								                		<c:when test="${order.isPayFinal eq 1}">已完成</c:when>
								                	</c:choose>
								                </td>
								              </tr>
										</tbody>
				             			<!-- ############################## 预售订单  end ########################## -->
				             		</c:when>
				             		<c:otherwise>
				             			<!-- ############################## 其他订单 start ########################## -->
				             			<tbody class="pay">
											<tr>
												<td class="sep-row" colspan="19"></td>
											</tr>
											<tr>
												<td class="pay-td" colspan="19">
												<span class="ml15">在线支付金额：<em>￥${order.actualTotal}</em></span> 
													<c:choose>
														<c:when test="${order.status==1 }">
															<a target="_blank"
																href="<ls:url address="/p/orderSuccess?subNums=${order.subId}" />"
																class="ncbtn ncbtn-bittersweet fr mr15"><i
																class="icon-shield"></i>订单支付</a>
														</c:when>
													</c:choose>
												</td>
											</tr>
											<tr>
												<td colspan="19" style="text-align: left;">
													<span class="ml10"> <!-- order_sn -->订单号：${order.subNum}</span>
													<span class="ml10"> 
														<c:if test="${order.subType eq 'SECKILL'}">秒杀订单</c:if>
										            	<c:if test="${order.subType eq 'NORMAL'}">普通订单</c:if>
										            	<c:if test="${order.subType eq 'GROUP'}">团购订单</c:if>
										            	<c:if test="${order.subType eq 'MERGE_GROUP'}">拼团订单</c:if>
										            	<c:if test="${order.subType eq 'PRE_SELL'}">预售订单</c:if>
													</span>
													<span>下单时间：<fmt:formatDate value="${order.subDate}" type="both" /></span> 
													<span><a title="平台自营" href="#">${order.shopName}</a></span> 
													<a onclick="restoreOrder('${order.subNum}');" class="order-trash fr" href="javascript:void(0);">还原订单</a>
												</td>
											</tr>
						
											<!-- S 商品列表 -->
											<c:forEach items="${order.subOrderItemDtos}" var="orderItem" varStatus="orderItemStatues">
												<tr>
													<td class="bdl"></td>
													<td class="w70">
														<div class="ncm-goods-thumb">
															<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>"> 
																<img onmouseout="toolTip()" onmouseover="toolTip();" src="<ls:images item='${orderItem.pic}' scale='3'/>">
															</a>
														</div>
													</td>
													<td class="tl">
														<dl class="goods-name2">
															<dt>
																<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">${orderItem.prodName}</a>
																<c:if test="${not empty orderItem.snapshotId && orderItem.snapshotId!=0}" >
																	<span class="rec">
																		<a  href="<ls:url address='/snapshot/${orderItem.snapshotId}'/>" target="_blank">[交易快照]</a>
																    </span>
																 </c:if>
															</dt>
															<dd>${orderItem.attribute}</dd>
															<!-- 消费者保障服务 -->
														</dl>
													</td>
													<td>${orderItem.productTotalAmout}<p class="green"></p></td>
													<td>${orderItem.basketCount}</td>
													<!-- S 合并TD -->
													<c:if test="${orderItemStatues.index==0}">
													<c:choose>
														<c:when test="${fn:length(order.subOrderItemDtos)>0}">
															<!-- 未支付 -->
															<td rowspan="${fn:length(order.subOrderItemDtos)}" class="bdl">
														</c:when>
														<c:otherwise>
															<td class="bdl">
														</c:otherwise>
													</c:choose>
													<p class="">
														<c:choose>
															<c:when test="${order.refundState == 1 or ( order.status != 4 && order.status != 5 )  }">
																<strong><fmt:formatNumber pattern="0.00" value="${order.actualTotal}" /></strong>
															</c:when>
															<c:otherwise>
																<strong style="color:#999;"><fmt:formatNumber pattern="0.00" value="${order.actualTotal}" /></strong>
															</c:otherwise>
														</c:choose>
													</p>
													<p class="goods-freight">
														<c:if test="${not empty order.freightAmount}">(含运费${order.freightAmount})</c:if>
													</p>
													<p title="支付方式：">${order.payManner==1?"货到付款":"在线支付"}</p>
													</td>
													<c:choose>
														<c:when test="${fn:length(order.subOrderItemDtos)>0}">
															<!-- 未支付 -->
															<td rowspan="${fn:length(order.subOrderItemDtos)}" class="bdl">
														</c:when>
														<c:otherwise>
															<td class="bdl">
														</c:otherwise>
													</c:choose>
													<p>
														<c:choose>
															<c:when test="${order.status==1 }">
																<span class="col-orange">待付款</span>
															</c:when>
														 	<c:when test="${order.status==2 }">
														 		<span class="col-orange">待发货</span>
														 	</c:when>
														 	<c:when test="${order.status==3 }">
														 		<span class="col-orange">待收货</span>
														 	</c:when>
														 	<c:when test="${order.status==4 }">已完成</c:when>
															<c:when test="${order.status==5 }">交易关闭</c:when>
													  	</c:choose>
												  	</p>
													<!-- 订单查看 -->
													<p><a target="_blank" href="${contextPath}/p/orderDetail/${order.subNum}">订单详情</a></p>
													<!-- 物流跟踪 -->
													</td>
													<c:choose>
														<c:when test="${fn:length(order.subOrderItemDtos)>0}">
															<!-- 未支付 -->
															<td rowspan="${fn:length(order.subOrderItemDtos)}" class="bdl bdr">
														</c:when>
														<c:otherwise>
															<td class="bdl">
														</c:otherwise>
													</c:choose>
													<p>
														<a class="btn-r" onclick="dropOrder('${order.subNum}');" href="javascript:void(0);">永久删除</a>
													</p>
													</td>
													</c:if>
												</tr>
											</c:forEach>
										</tbody>
				             			<!-- ############################## 其他订单  end ########################## -->
				             		</c:otherwise>
				             	</c:choose>
							</c:forEach>
				           </c:otherwise>
				         </c:choose>
					</table>
				  <div style="margin-top:10px;" class="page clearfix">
		   			 <div class="p-wrap">
		          		<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
		   			 </div>
				 </div>	              		
				     </div>
			     </div>
    <!-----------right_con end-------------->
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/ToolTip.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/myorder.js'/>"></script>
<script type="text/javascript">
	var path="${contextPath}";
	var failedOwnerMsg = '<fmt:message key="failed.product.owner" />';
	var failedBasketMaxMsg = '<fmt:message key="failed.product.basket.max" />';
	var failedBasketErrorMsg = '<fmt:message key="failed.product.basket.error" />';
	$(function(){
		laydate.render({
			   elem: '#startDate',
			   calendar: true,
			   theme: 'grid',
			   trigger: 'click'
		  });
     
     laydate.render({
			   elem: '#endDate',
			   calendar: true,
			   theme: 'grid',
			   trigger: 'click'
		  });
	});
	
</script>
</body>
</html>