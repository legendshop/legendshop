<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/dialog.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--当前点击的获取分类 -->
<c:set var="currentNode" value="${indexApi:findTreeNode(param.categoryId)}"></c:set>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<c:choose>
	<c:when test="${not empty currentNode && not empty currentNode.obj}">
		<title>${empty currentNode.obj.title?currentNode.obj.name:currentNode.obj.title}-${systemConfig.shopName}</title>
		<meta name="keywords" content="${currentNode.obj.keyword}" />
		<meta name="description" content="${currentNode.obj.catDesc}" />
	</c:when>
	<c:otherwise>
		<title>${systemConfig.title}-${systemConfig.shopName}</title>
		<meta name="keywords" content="${systemConfig.keywords}" />
		<meta name="description" content="${systemConfig.description}" />
	</c:otherwise>
</c:choose>
</head>
<body class="graybody" style="background: #ffffff;">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div id="bd">
			<!-- 商品 属性 -->
			<div id="main-facet">
				<div id="J_crumbs" class="crumb yt-wrap">
		             <div class="crumbCon">
	                      <div class="crumbSlide">
	                        <div class="crumbs-nav-item one-level">
			                    <a class="crumbs-link" href="/legendshop-web/"> 首页 </a><i class="crumbs-arrow">&gt;</i>
			                </div>
			               <div class="crumbs-nav-item one-level">
	                               <a class="crumbs-link" href="javascript:void(0);">优惠券商品</a>
	                               <i class="crumbs-arrow">&gt;</i>
	                            </div>
						   </div>
		               </div>
           		 </div>
			</div>
			<!-- 商品 列表-->
			<div id="main-nav-holder" style="height: 450px;"></div>
		</div>
		
		<form:form action="${contextPath}/p/coupon/couponProdList" method="post" id="list_form">
			<input type="hidden" name="couponId" value="${coupon.couponId}" />
			<input type="hidden" name="couponProvider" value="${coupon.couponProvider}" />
			<input type="hidden" name="couponType" value="${coupon.couponType}" />
			<input type="hidden" name="shopId" value="${coupon.shopId}" />
			<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
			<input type="hidden" id="orders" name="orders" value="${orders}" />
		</form:form>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
</body>
<script type="text/javascript">
	var webPath = {
		webRoot : "${contextPath}",
		toatalPage : "<c:out value='${requestScope.pageCount}'/>",
		cookieNum : 0
	}

	jQuery(document).ready(function() {
		sendData();
	});
	var showUnSelections = function() {
		$(".unSelections").slideDown();
		$("#showUnSelections").hide();
		$("#hideUnSelections").show();
	};

	var hideUnSelections = function() {
		$(".unSelections:eq(3)").slideUp(200);
		$("#hideUnSelections").hide();
		$("#showUnSelections").show();
	};
</script>
<!--page end-->
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/map.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/coupon/couponProd.js'/>"></script>
</html>
