<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/pages/plugins/multishop/back-common.jsp" %>
<%@ include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>添加秒杀活动-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/seckill-activity.css'/>"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css"/>
</head>
<body class="graybody">
<div id="doc">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
    <div class="w1190">
        <div class="seller-cru">
            <p>
                您的位置>
                <a href="#">首页</a>>
                <a href="#">卖家中心</a>>
                <span class="on"> 编辑秒杀活动</span>
            </p>
            <%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>

            <div class="seckill-right">
                <form action="${contextPath}/s/seckillActivity/save" method="post" id="form1" enctype="multipart/form-data">
                    <!-- 基本信息 -->
                    <div class="basic-ifo">
                        <!-- 基本信息头部 -->
                        <div class="ifo-top">
                            基本信息
                        </div>
                        <div class="ifo-con">
                            <li style="margin-bottom: 5px;">
                                <span class="input-title"><span class="required">*</span>活动名称：</span>
                                <input type="text" class="active-name" name="seckillTitle" id="seckillTitle" disabled
                                       value="${seckillActivity.seckillTitle}" maxLength="50"
                                       placeholder="请填写活动名称"><em></em>
                            </li>
                            <li>
                                <span class="input-title"></span>
                                <span style="color: #999999;margin-top: 10px;">活动名称最多为20个字符</span>
                            </li>
                            <li style="margin-bottom: 0;">
                                <span class="input-title"><span class="required">*</span>活动时间：</span>
                                <fmt:formatDate value="${seckillActivity.startTime}" pattern="yyyy-MM-dd HH:mm:ss"
                                                var="startTime"/>
                                <input type="text" class="active-time" readonly="readonly" name="startTime" disabled
                                       id="startTime" value='${startTime}' placeholder="开始时间"/><em></em>
                                <span style="margin: 0 5px;">-</span>
                                <fmt:formatDate value="${seckillActivity.endTime}" pattern="yyyy-MM-dd HH:mm:ss"
                                                var="endTime"/>
                                <input type="text" class="active-time" readonly="readonly" name="endTime" id="endTime" disabled
                                       value='${endTime}' placeholder="结束时间"/><em></em>
                            </li>
                        </div>
                    </div>
                    <!-- 选择活动商品 -->
                    <div class="basic-ifo" style="margin-top: 20px;">
                        <!-- 选择活动商品头部 -->
                        <div class="ifo-top">
                            选择活动商品
                        </div>
                        <div class="ifo-con">
                            <div class="check-shop prodList">
                                <c:if test="${not empty activityDtos}">
                                    <table border="0" id="skuList" cellpadding="0" cellspacing="0" style="margin: 30px 0;">
                                        <tbody>
                                        <tr style="background: #f9f9f9;">
                                            <td width="120px">商品图片</td>
                                            <td width="360px">商品名称</td>
                                            <td width="100px">商品规格</td>
                                            <td width="100px">商品价格</td>
                                            <td width="100px">秒杀库存</td>
                                            <td width="100px">秒杀价格</td>
                                        </tr>
                                        <c:choose>
                                            <c:when test="${not empty activityDtos}">
                                                <c:forEach items="${activityDtos}" var="activityDto">
                                                    <tr class="sku" >
                                                        <td>
                                                            <img src="<ls:images item='${activityDto.pic}' scale='3' />" >
                                                        </td>
                                                        <td>
                                                            <span class="name-text">${activityDto.prodName}</span>
                                                        </td>
                                                        <td>
                                                            <span class="name-text">${empty activityDto.cnProperties?'暂无':activityDto.cnProperties}</span>
                                                        </td>
                                                        <td>
                                                            <span class="name-text">${activityDto.skuPrice}</span>
                                                        </td>
                                                        <td>${activityDto.activityStock}</td>
                                                        <td>
                                                                ${activityDto.price}
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </c:when>
                                            <c:otherwise>
                                                <tr class="first">
                                                    <td colspan="6" >暂未选择商品</td>
                                                </tr>
                                            </c:otherwise>
                                        </c:choose>
                                        </tbody>
                                    </table>
                                </c:if>
                            </div>
                        </div>
                    </div>
                    <!-- 活动规则 -->
                    <div class="basic-ifo" style="margin-top: 20px;">
                        <!-- 活动规则头部 -->
                        <div class="ifo-top">
                            活动规则
                        </div>
                        <div class="ifo-con">
                            <li><font class="input-title"><b style="color: red">*</b>活动摘要：</font><input type="hidden" name="seckillLowPrice" disabled readonly
                                                                                    id="seckillLowPrice"
                                                                                    value="${seckillActivity.seckillLowPrice}"/><textarea disabled
                                    name="seckillBrief" id="seckillBrief"
                                    maxlength="100">${seckillActivity.seckillBrief}</textarea><em></em></li>
                            <c:if test="${not empty seckillActivity.seckillPic}">
                                <li><font class="input-title">活动图片：</font><a href="<ls:photo item='${seckillActivity.seckillPic}'/>"
                                                         target="_blank"><img
                                        src="<ls:photo item='${seckillActivity.seckillPic}'/>"
                                        style="max-height: 150px;max-width: 150px;"></a></li>
                            </c:if>
                            <li>
                                <span class="input-title"><span class="required"></span>秒杀规则：</span>
                                <div class="rich-text">
                                    <textarea name="seckillDesc" id="seckillDesc" cols="100" rows="8" style="width:700px;height:300px;visibility:hidden;" placeholder="（选填）">${seckillActivity.seckillDesc}</textarea>
                                </div>
                            </li>
                        </div>
                    </div>
                    <div class="bto-btn">
                        <div class="red-btn return-btn" onclick="window.location='/s/seckillActivity/query'">返回</div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<script charset="utf-8"
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script charset="utf-8"
        src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
    var pic = "${seckillActivity.seckillPic}";
    var paramData = {
        contextPath: "${contextPath}",
        cookieValue: "<c:out value="${cookie.SESSION.value}"/>"
    }

    $(document).ready(function () {
        userCenter.changeSubTab("seckillManage"); //高亮菜单
    });

    Date.prototype.toLocaleString = function () {
        0
        return this.getFullYear() + "-" + (this.getMonth() + 1) + "-" + this.getDate();
    };
    KindEditor.options.filterMode = false;
    KindEditor.create('textarea[name="seckillDesc"]', {
        cssPath: '${contextPath}/resources/plugins/kindeditor/plugins/code/prettify.css',
        uploadJson: '${contextPath}/editor/uploadJson/upload;jsessionid=${cookie.SESSION.value}',
        fileManagerJson: '${contextPath}/editor/uploadJson/fileManager',
        allowFileManager: true,
        resizeType: 0,
        afterBlur: function () {
            this.sync();
        },
        width: '100%',
        height: '350px',
        afterCreate: function () {
            var self = this;
            KindEditor.ctrl(document, 13, function () {
                self.sync();
                document.forms['example'].submit();
            });
            KindEditor.ctrl(self.edit.doc, 13, function () {
                self.sync();
                document.forms['example'].submit();
            });
        }
    });
    var myDate = new Date(new Date().getTime()).toLocaleString();

</script>
</body>
</html>