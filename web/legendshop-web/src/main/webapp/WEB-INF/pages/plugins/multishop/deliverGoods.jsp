<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<html>
<head>
<title>添加发货物流信息</title>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/deliverAddressOverlay${_style_}.css'/>" rel="stylesheet"/>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller${_style_}.css'/>" rel="stylesheet"/>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
</head>
<body>
<div class="m" id="edit-cont">
	<div class="mc">
		<div class="form">
			<div class="item">
				<span class="label fr" style="width: 90px;text-align:right;"><em>*</em>订单号：</span>
				    <div class="fl">
				  <input readonly="readonly" type="text" name="subNumber" id="subNumber" size="30" value="${sub.subNumber}"  style="height: 20px">
				   </div>
				</span>
				<div class="clr"></div>
              </div>
			
			<div class="item">
				<span class="label" style="width: 90px;text-align:right;"><em>*</em>物流公司：</span>
				<select id="ecc_id" name="ecc_id"  style="height: 22px">
					<c:forEach items="${deliveryTypes}" var="dt">
							 <option value="${dt.dvyTypeId}" <c:if test="${dt.dvyTypeId==sub.dvyTypeId}" > selected="selected" </c:if> >${dt.name}</option>
					 </c:forEach>
				</select>
				<div class="clr"></div>
			</div>
			<div class="item">
				<span class="label" style="width: 90px;text-align:right;"><em>*</em>物流单号：</span>
				<div class="fl">
				  <input type="text" name="dvyFlowId"  value="${sub.dvyFlowId}"  id="dvyFlowId" size="30"  style="height: 20px" maxlength="20">
				</div>
				<div class="clr"></div>
			</div>
			
			<div class="btns" style="width: 180px">
			    <a href="javascript:void(0);" onclick="deliverGoods();"  class="ncbtn btn-r">保存发货信息</a>
			    <c:if test="${sub.status==2}">
			       <a href="javascript:void(0);" onclick="fahuo();"  class="ncbtn btn-r">发货</a>
				</c:if>
			</div>
			</div>
		</div>
	</div>
</div>
</body>
<script src="<ls:templateResource item='/resources/templets/js/deliveGoods.js'/>" type="text/javascript"></script>
 <script type="text/javascript">
 var contextPath="${contextPath}";
 </script>
</html>