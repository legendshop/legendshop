<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
	<head>
		<title>修改密码</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
		<title>修改密码-${systemConfig.shopName}</title>
   		 <meta name="keywords" content="${systemConfig.keywords}"/>
    	<meta name="description" content="${systemConfig.description}" />
		<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/shopUser.css'/>" rel="stylesheet"/>
		<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller.css'/>" rel="stylesheet"/>
		<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
	</head>
	<body>
		<form:form  action="${contextPath}/s/shopUsers/updatePwd" method="post" id="from1" target="_parent">
			<input type="hidden" value="${shopUser.id}" name="id">
			<div>
				<table>	
					<tr>
						<td class="td-title"><b>*</b>密码</td><td> <input type="password" onfocus="clearErr(this);" onblur="checkPasswd();" id="password" name="password" maxlength="20"><span class="err-msg hide"></span></td>
					</tr>
					<tr>
						<td class="td-title"><b>*</b>确认密码</td><td> <input type="password" onfocus="clearErr(this);" onblur="checkPasswd2();" id="password2" maxlength="20"><span class="err-msg hide"></span></td>
					</tr>
				</table>
			</div>
			<div class="btn-content">
				<input onclick="saveUser();" type="button" value="修改" class="btn-r small-btn">
				<input onclick="closeDialog();" style="margin-left:10px;" type="button" value="取消" class="btn-g small-btn">
			</div>
		</form:form>
		<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/updatePwd.js'/>"></script>
		<script type="text/javascript">
		var contextPath = '${contextPath}'
			
		</script>
	</body>
</html>