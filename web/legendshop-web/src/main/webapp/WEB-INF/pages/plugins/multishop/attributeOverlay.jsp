<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
   <div class="area_box"  style="width:420px; margin: 10px; ">
       <div class="area_box_title" style="width:400px;">
        <span class="area_box_title_left">销售属性模板</span>
        <span class="area_box_title_right"><a href="javascript:void(0);" onclick="javascript:jQuery('.area_box').remove();">×</a></span>
    </div>
      <div class="area_bg_white" style="width:360px; margin: 20px;">
<form:form  action="" id="form1">
  <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
    <div align="center" id="overlayContent" class="tpl-list"> 
		    <table class="prodspec-table" >
		    	<thead>
		    		<tr >
		    			<th class="alignc">模板名称</th>
		    			<th class="alignc">操作</th>
		    		</tr>
		    	</thead>
		    	<c:forEach  items="${requestScope.list}" var="prodUserAttribute"  varStatus="status">	
		    	
		    	<tr id="ppa${prodUserAttribute.id}" class='<c:if test="${status.index % 2 == 1 }">odd1 </c:if>'>
		    		<td class="alignc">${prodUserAttribute.name}</td>
		    		<td class="alignc"><a href="javascript:useProdspec('${prodUserAttribute.id}');"  class="loadtpl">使用</a><a href="javascript:deleteSpecById('${prodUserAttribute.id}');" class="deltpl">删除</a></td>
		    	</tr>
		    	</c:forEach>
		    </table>
    		 <div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
				            		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/>
				     			 </div>
		  				</div>
		</div>
</form:form>
<script src="<ls:templateResource item='/resources/templets/js/attributeOverlay.js'/>" type="text/javascript"></script>
 <script type="text/javascript">
 var contextPath="${contextPath}";
 </script>
 </div>
 </div>
    

