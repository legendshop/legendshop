<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<jsp:useBean id="nowTime" class="java.util.Date" />
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<c:set var="seckills"  value="${list}"></c:set>
	<title>秒杀活动列表-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <jsp:useBean id="nowDate" class="java.util.Date" />
    <fmt:formatDate value="${nowDate}"  pattern="HH" var="nowHour" />
    <style type="text/css">
    .sec-tag:hover{
    	background:#c6171f !important;
    	color:#fff !important;
    }
    </style>
    <link href="<ls:templateResource item='/resources/templets/css/spike.css'/>" rel="stylesheet" type="text/css">
</head>
<body>
<div id="doc">
   <!--顶部-->   
   <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
   <!--顶部--> 
   <!--导航--> 
   <div id="bd">
      <div class="seller-cru" style="width:1190px;margin: 20px auto 10px;">
        <p>  您的位置&gt;
          <a href="${contextPath}/home">首页</a>&gt;
          <span style="color: #c6171f;">秒杀活动</span>
        </p>
      </div>
    </div>
   <!--导航--> 
    <!--两栏-->
  <div class=" yt-wrap " style="padding-top:10px;"> 
   	 <!--right_con-->
     <!-- page-->
      <div class="seckill-list">
	        <div class="sec-lis-tit">
	          <ul>
	            <li><p>今日秒杀</p></li>
	            <li class="<c:if test="${9 <= nowHour and nowHour lt 12 }"> sec-arrow now </c:if>sec-tag" onclick="tabClick(this)"><span>09:00-12:00</span><em></em></li>
	            <li class="<c:if test="${12 <= nowHour and nowHour lt 15 }"> sec-arrow now </c:if>sec-tag" onclick="tabClick(this)"><span>12:00-15:00</span><em></em></li>
	            <li class="<c:if test="${15 <= nowHour and nowHour lt 18 }"> sec-arrow now </c:if>sec-tag" onclick="tabClick(this)"><span>15:00-18:00</span><em></em></li>
	            <li class="<c:if test="${18 <= nowHour and nowHour lt 21 }"> sec-arrow now </c:if>sec-tag" onclick="tabClick(this)"><span>18:00-21:00</span><em></em></li>
	            <li class="<c:if test="${21 <= nowHour and nowHour lt 24 }"> sec-arrow now </c:if>sec-tag" onclick="tabClick(this)"><span>21:00-24:00</span><em></em></li>
	          </ul>
	        </div>
	        
	        <div class="sec-lis-tim">
	          <span>限时限量 疯狂抢购 </span>
	          <div class="down-time" >
	            <p>距离本场结束</p>
	            <ul>
	              <li>0</li>
	              <li>0</li>
	              <li>0</li>
	            </ul>
	          </div>
	        </div>
	        
        <div class="spike">
         
	      <div class="seckillProdList pro-list"><!-- 秒杀商品列表 -->
         	  
         	      
          </div>
        </div>
      </div>
    <!--right_con end-->
    <!-- page-->
   
  </div>

   </div><!--bd end-->
 <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>


</div>
<script text="javascript">
var pageNo ='${curPageNO}';
function pager(curPageNO){
   	document.getElementById("curPageNO").value=curPageNO;
   	var startTime=$("#startTime").val();
   	var url = contextPath+"/seckillActivity/querySeckillList?startTime="+startTime+"&&curPageNO="+curPageNO;
  	$(".seckillProdList").load(url);
}
</script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/seckill/seckillList.js'/>"></script>
</body>
</html>