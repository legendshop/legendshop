<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>店铺收藏 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>
<style>
.sold-table th {
	padding: 8px 0;
	line-height: 22px;
	font-weight: 400;
	color: #333;
	background: #f9f9f9;
	border-bottom:1px solid #e4e4e4;
}
.sold-table td{
	text-align: center;
}
</style>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con">
	<div class="o-mt">
		<h2>店铺收藏</h2>
	</div>
	
	<div class="pagetab2">
        <ul>           
           <li class="on" id="favoriteShopList"><span>店铺收藏列表</span></li>
        </ul>       
     </div>
     
      <div id="recommend" class="recommend" style="display: block;">
     <table id="item"  style="width:100%" cellspacing="0" cellpadding="0" class="buytable sold-table">
		<tr>
			<th width="50" style="text-align: center;">
			 <span>
		           <label class="checkbox-wrapper" style="float:left;margin-left:9px;">
						<span class="checkbox-item">
							<input type="checkbox" id="checkbox" class="checkbox-input selectAll"/>
							<span class="checkbox-inner" style="margin-left:9px;"></span>
						</span>
				   </label>	
			</span>
			</th>
			<th style="text-align: center;">店铺名称</th>
			<th style="text-align: center;">店铺图片</th>
			<th style="text-align: center;">店铺等级</th>
			<th style="text-align: center;">收藏时间</th>
			<th style="text-align: center;border-right: 1px solid #e4e4e4;">操作</th>
		</tr>
		<c:choose>
           <c:when test="${empty requestScope.list}">
              <tr>
			 	 <td colspan="20">
			 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
			 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
			 	 </td>
			 </tr>
           </c:when>
           <c:otherwise>
              <c:forEach items="${requestScope.list}" var="favoriteShop" varStatus="status">
	               <tr>
	                   <td height="60">
	                   <%-- <input name="strArray" type="checkbox" value="${favoriteShop.fsId}"/> --%>
	                   <span>
				           <label class="checkbox-wrapper" style="float:left;margin-left:9px;">
								<span class="checkbox-item">
									<input type="checkbox" id="strArray" class="checkbox-input selectOne" value="${favoriteShop.fsId}" onclick="selectOne(this);"/>
									<span class="checkbox-inner" style="margin-left:9px;"></span>
								</span>
						   </label>	
						</span>
	                   </td>
	                   <td><a href=" ${contextPath}/store/${favoriteShop.shopId}"  target="_blank">${favoriteShop.siteName}</a></td>
	                   <td>
	                   			<c:choose> 
								  <c:when test="${empty favoriteShop.shopPic}">  
								    <span>暂无图片</span> 
								  </c:when> 
								  <c:otherwise>   
	                   				<a href="${contextPath}/store/${favoriteShop.shopId}"  target="_blank"><img src="<ls:images item='${favoriteShop.shopPic}' scale='3'/>"  style="max-height: 60px; max-width: 198px"/></a>
								  </c:otherwise> 
								</c:choose> 
	                   </td>
	                   <td>${favoriteShop.gradeName}</td>
	                   <td><fmt:formatDate value="${favoriteShop.recDate}"  pattern="yyyy-MM-dd"/></td>
	                   <td> 
				        		<a href='javascript:deleteById("${favoriteShop.fsId}")' title="删除" class="btn-r small-btn">删除</a>
	                   </td>
	               </tr>
	           </c:forEach> 
            </c:otherwise>
        </c:choose>
		</table>
		  
		</div>
		 <c:if test="${not empty requestScope.list }">
		    <div class="fl search-01">
                 <input class="bti btn-r" type="button" value="批量删除"  id="delBtn" onclick="deleteAction();" style="cursor: pointer;"/>
                 <input class="bti btn-g" type="button" value="清空"  id="clrBtn" onclick="clearAction();"  style="cursor: pointer;"/>
              </div>
	         <div style="margin-top:10px;" class="page clearfix">
     			 <div class="p-wrap">
					<span class="p-num">
						<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
					</span>
     			 </div>
 			 </div>
		  </c:if>
		
    </div>
    <!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/alternative.js'/>"></script>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/favoriteShop.js'/>"></script>
  <script type="text/javascript">
  	var contextPath = '${contextPath}';
   </script>
</body>
</html>
