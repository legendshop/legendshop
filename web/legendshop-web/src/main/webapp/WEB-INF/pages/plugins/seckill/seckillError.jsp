<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
	<title>${systemConfig.shopName}
	</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
</head>
<body class="graybody">
	<div id="doc">
	   <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		
		<div id="bd">
		      <!--order content -->
		         <div style="margin-top:20px;" class="yt-wrap ordermain">
		              <div class="clearfix box404">
		              <p style="padding-bottom:30px; padding-top:20px; color:#666;" class="bighint2">${message}</p>
						<a style="padding:10px 80px; font-size:1.6em;" href="<ls:url address='/'/>" class=" btnlink">首  页</a>
		             </div>
		        </div>
		<!--order content end-->
		   </div>
		
		
	   <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
</body>
</html>