<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}"  pattern="yyyy-MM-dd HH:mm" var="nowDate"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>预售订单 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>
<style>
.goods-name2 dt a:hover {
	color: #e5004f !important;
}
</style>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con">
     
         <div class="o-mt"><h2>预售订单</h2></div>
         
         <div class="pagetab2">
             <ul>           
               <li><span><a href="<ls:url address='/p/presellOrder'/>">订单列表</a></span></li>                  
               <li class="on"><span>回收站</span></li>      
             </ul>       
         </div>
          <form:form action="${contextPath}/p/presellOrderDels" method="get" id="order_search">  
	          <input type="hidden" value="${empty paramDto.curPageNO?1:paramDto.curPageNO}" name="curPageNO">    
	          <div class="sold-ser clearfix" style=" margin-top:10px;">
		         <div class="item">
		              		订单状态：<select name="status" class="item-sel">
								<option  value="" >所有订单</option> 
								<%-- <option value="1"  <c:if test="${paramDto.status eq 1}">selected="selected"</c:if>>待付款</option>
								<option value="2" <c:if test="${paramDto.status eq 2}">selected="selected"</c:if>>待发货</option>
								<option value="3" <c:if test="${paramDto.status eq 3}">selected="selected"</c:if>>待收货</option> --%>
								<option value="4" <c:if test="${paramDto.status eq 4}">selected="selected"</c:if>>已完成</option>
								<option value="5" <c:if test="${paramDto.status eq 5}">selected="selected"</c:if>>已取消</option>
							</select>
			     </div>
			     <div class="item">
		                 	订单号：<input type="text" class="item-inp" value="${paramDto.subNumber}" name="subNumber" style="width:200px;"/>
		         </div>
		         <div class="item">
		              		下单时间：<input type="text" value="${paramDto.startDate}" id="startDate" name="startDate" class="text Wdate item-inp" readonly="readonly">
							-
							<input type="text" value="${paramDto.endDate}" id="endDate" name="endDate"  class="text Wdate item-inp" readonly="readonly">
		                    <input type="button" class="btn-r" onclick="search()" id="search_order" class="bti" value="查 询">
		         </div>
	          </div>
        </form:form>
        
        <!-------------订单---------------->
         <div id="recommend" class="m10 recommend" style="display: block;border-width:0px 0px;">
            <table class="ncm-default-table order sold-table" cellpading=0 cellspacing="0">
			<thead>
				<tr>
					<th class="w10"></th>
					<th colspan="2">商品</th>
					<th class="w90">单价（元）</th>
					<th class="w90">数量</th>
					<th class="w110">订单金额</th>
					<th class="w90">交易状态</th>
					<th class="w120" style="border-right: 1px solid #e7e7e7;">交易操作</th>
				</tr>
			</thead>
			<c:choose>
	           <c:when test="${empty requestScope.list}">
	              <tr>
				 	 <td colspan="20">
				 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
				 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
				 	 </td>
				 </tr>
	           </c:when>
	           <c:otherwise>
	              <c:forEach items="${requestScope.list}" var="order" varStatus="orderstatues">
				    <tbody class="pay">
					<tr>
						<td class="sep-row" colspan="8"></td>
					</tr>
					<tr>
						<th colspan="8" style="height: 35px;background-color: #f9f9f9;">
							<span class="ml10">订单号：${order.subNo} </span>
							<span>下单时间： <fmt:formatDate value="${order.subCreateTime}" type="both" /></span> 
							<span>
							     <a title="${order.shopName}" href="<ls:url address="/store/${order.shopId}" />">${order.shopName}</a>
							</span> 
							<a onclick="restoreOrder('${order.subNo}');" class="order-trash" href="javascript:void(0);"><i class="icon-trash"></i>还原订单</a>
							<!-- 放入回收站 --> <!-- 还原订单 -->
							</th>
					</tr>
					
					<c:forEach items="${order.orderItems }" var="orderItem" varStatus="orderItemStatus">
					
				    <c:set value="0" var="notCommedCount" /> 
					<c:if test="${orderItem.commSts eq 0}">
			    		<c:set value="${notCommedCount + 1}" var="notCommedCount" /> 
			    	</c:if>
					<!-- S 商品列表 -->
					  <tr>
						<td class="w70" align="right">
							<div class="ncm-goods-thumb" style="    margin-left: 10px;">
								<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">
									<img onmouseout="toolTip()" onmouseover="toolTip();" src="<ls:images item='${orderItem.prodPic}' scale='3'/>">
								</a>
							</div>
						</td>
						<td class="tl"><dl class="goods-name2">
								<dt>
									<a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">${orderItem.prodName}</a>
								</dt>
								<dd>${orderItem.attribute}</dd>
							</dl>
						</td>
						<td><p> ${orderItem.prodCash}</p></td>
						<td>x${order.basketCount}</td>
			           <td class="bdl">
						    <p class="">
								<strong>${order.actualTotalPrice}</strong>
							</p>
							<p title="支付方式：">
							  <c:choose>
			                		<c:when test="${order.payManner eq 1}">
			                			货到付款
			                		</c:when>
			                		<c:when test="${order.payManner eq 2}">
			                			在线支付
			                		</c:when>
			                		<c:when test="${order.payManner eq 2}">
			                			门店支付
			                		</c:when>
			                </c:choose>
						</td>
						<!-- 如果是第一行 -->
						<c:if test="${orderItemStatus.index eq 0 }">
							<c:choose>
								<c:when test="${fn:length(order.orderItems) gt 0 }"><!-- 如果有多个订单项 -->
									<td class="bdl" rowspan="${fn:length(order.orderItems) }">
								</c:when>
								<c:otherwise>
									<td class="bdl">
								</c:otherwise>
							</c:choose>
						    <p>
							<c:choose>
								 <c:when test="${order.status eq 1 }">
								 	<span class="col-orange">待付款</span>
								 </c:when>
							     <c:when test="${order.status eq 2 }">
							        <span class="col-orange">待发货</span>
							     </c:when>
								 <c:when test="${order.status eq 3 }">
								 	<span class="col-orange">待收货</span>
								 </c:when>
								 <c:when test="${order.status eq 4 }">已完成</c:when>
								 <c:when test="${order.status eq 5 }">交易关闭</c:when>
							</c:choose>
						    </p> <!-- 订单查看 -->
							<p>
								  <a target="_blank" href="${contextPath}/p/orderDetail/${order.subNo}">订单详情</a>
							</p>
						</td>
						
						<c:choose>
							<c:when test="${fn:length(order.orderItems) gt 0 }"><!-- 如果有多个订单项 -->
								<td class="bdl bdr" rowspan="${fn:length(order.orderItems) }" colspan="2">
							</c:when>
							<c:otherwise>
								<td class="bdl bdr" colspan="2">
							</c:otherwise>
						</c:choose>
							<p>
							   	<a class="ncbtn ncbtn-grapefruit mt5" onclick="dropOrder('${order.subNo}');" href="javascript:void(0);"><i class="icon-trash"></i>永久删除</a>
							</p> 
						</td>
						</c:if>
					</tr>
					</c:forEach>
					 <tr style="color:#999;">
		                <td colspan="2">阶段1：定金</td>
		                <td colspan="2">
							<c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
							¥<span class="preDepositPrice"><fmt:formatNumber value="${order.depositPrice+order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber></span>
							(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
							</c:if>
							<c:if test="${order.payPctType eq 1}"><!-- 定金支付 -->
							¥<span class="preDepositPrice"><fmt:formatNumber value="${order.depositPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
							</c:if>
						</td>
		                <td colspan="4" style="border-right: 1px solid #e6e6e6;">
		                	<c:if test="${order.isPayDeposit eq 0 }">未完成</c:if>
		                	<c:if test="${order.isPayDeposit eq 1 }">已完成</c:if>
		                </td>
		              </tr>
		              <tr style="color:#999;">
		                <td colspan="2">阶段2：尾款</td>
		                <td colspan="2">
							<c:if test="${order.payPctType eq 0}"><!-- 全额支付 -->
							¥<span class="finalPrice"><fmt:formatNumber value="${order.finalPrice}" type="currency" pattern="0.00"></fmt:formatNumber></span>
							</c:if>
							<c:if test="${order.payPctType eq 1}"><!-- 定金支付 -->
							¥<span class="finalPrice"><fmt:formatNumber value="${order.finalPrice+order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber></span>
							(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
							</c:if>
						</td>
		                <td colspan="4" style="border-right: 1px solid #e6e6e6;">
		                	<c:choose>
		                		<c:when test="${order.isPayFinal eq 0}">
		                			未完成
		                		</c:when>
		                		<c:when test="${order.isPayFinal eq 1}">
		                			已完成
		                		</c:when>
		                	</c:choose>
		                </td>
		              </tr>
				</tbody>
				</c:forEach>
           </c:otherwise>
        </c:choose>
	</table>
		<div style="margin-top:10px;" class="page clearfix">
   			<div class="p-wrap">
          		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
   			 </div>
		</div>	              		
     </div>
     </div>
    <!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/ToolTip.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/presellOrder.js'/>"></script>
<script type="text/javascript">
	var path="${contextPath}";
	var failedOwnerMsg = '<fmt:message key="failed.product.owner" />';
	var failedBasketMaxMsg = '<fmt:message key="failed.product.basket.max" />';
	var failedBasketErrorMsg = '<fmt:message key="failed.product.basket.error" />';
	
	$(function(){
		laydate.render({
			   elem: '#startDate',
			   calendar: true,
			   theme: 'grid',
			   trigger: 'click'
		  });
     
     laydate.render({
			   elem: '#endDate',
			   calendar: true,
			   theme: 'grid',
			   trigger: 'click'
		  });
	});
	
	function search(){
	  	$("#order_search #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	  	$("#order_search")[0].submit();
	}
</script>
</body>
</html>