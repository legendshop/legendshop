<!DOCTYPE html>
	<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
	<%@ include file='/WEB-INF/pages/frontend/templetGoat/common/taglib.jsp' %>
	<%@ include file='/WEB-INF/pages/frontend/templetGoat/common/common.jsp' %>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<title>账户记录详情</title>
	<link rel="stylesheet" href="<ls:templateResource item='/resources/templets/css/userProdeposit.css'/>">
</head>
<body>

<div class="fanhui_cou">
	<div class="fanhui_1"></div>
	<div class="fanhui_ding">顶部</div>
</div>
	<div class="w768">
		<header>
			<div class="back">
				<a href="javascript:history.back();">
					  <img src="<ls:templateResource item='/resources/templets/images/back.png'/>" alt=""> 
				</a>
			</div>
			<p>余额记录详情</p>
		</header>
		
<!-- 头部 -->
		<div class="my-poi">
		 
		       <table cellspacing="0" cellpadding="0" id="detail">
					 <c:if test="${not empty cashLog}">
							<tr>
								<td style="width: 80px;text-align: right;padding-right: 8px;">交易流水号:</td>
								<td class="le">${cashLog.sn}</td>
							</tr>
							<tr>
								<td style="width: 80px;text-align: right;padding-right: 8px;">收入:</td>
								<td class="le">
								   <c:choose>
										<c:when test="${cashLog.amount>0}">
										   <span style="color: red;">+<fmt:formatNumber value="${cashLog.amount}" pattern="#,#0.00#"/></span>
										</c:when>
										<c:otherwise>
										   0.00
										</c:otherwise>
								   </c:choose>
								</td>
							</tr>
						    <tr>
								<td style="width: 80px;text-align: right;padding-right: 8px;">支出:</td>
								<td class="le">
								   <c:choose>
										<c:when test="${cashLog.amount<0}">
										   <span style="color: red;"><fmt:formatNumber value="${cashLog.amount}" pattern="#,#0.00#"/></span>
										</c:when>
										<c:otherwise>
										   0.00
								 	    </c:otherwise>
								    </c:choose>
								</td>
							</tr>
							<tr>
								<td style="width: 80px;text-align: right;padding-right: 8px;">操作时间:</td>
								<td class="le"><fmt:formatDate value="${cashLog.addTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
							</tr>
							<tr>
								<td style="width: 80px;text-align: right;padding-right: 8px;">说明:</td>
								<td style="text-align: left;">
									  ${cashLog.logDesc}
								</td>
							</tr>
					 </c:if>
					 
					  <c:if test="${empty cashLog}">
					       <tr>
								<td>该数据有误！</td>
							</tr>
					  </c:if>
			</table>
			
			
	   </div>
<%@ include file='/WEB-INF/pages/frontend/templetGoat/common/footer.jsp' %>
</body>
</html>