<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>添加商家导航-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-nav.css'/>" rel="stylesheet">
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">添加商家导航</span>
				</p>
			</div>
			
			<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
			
			<div class="seller-brand">
				<div class="seller-com-nav" >
					<ul>
						<li><a href='${contextPath}/s/shopNav/list'>头部菜单列表</a></li>
						<li class="on">
						<a id="brandConfigure" >
							<c:choose>
								<c:when test="${empty bean}">
									新增头部菜单
								</c:when>
								<c:otherwise>
									编辑头部菜单
								</c:otherwise>
							</c:choose>
						</a>
						</li>
					</ul>
				</div>
				 <form:form  action="${contextPath}/s/shopNav/save" method="post" id="form1" >
						<input id="id" name="id" value="${bean.id}" type="hidden">
						<div class="brand-add">
							<ul>
							<div>
								<li><em style="color: #e5004f;margin-right: 5px;">*</em>导航名称：
								  <input name="title" style="width:359px;" maxlength="20" type="text" id="title"  value="${bean.title}">
								  <span class="error-span"></span>
								</li>
							</div>	
							
								<div>
									<li class="textarea"><em style="color: #e5004f;margin-right: 5px;">*</em>导航URL：
									  <textarea name="url" cols="60" style=" padding:5px;margin:0 1px;" rows="5" id="url" maxlength="250">${bean.url}</textarea>
			                          <span>例如商品列表：http://www.域名.com/store/list/53</span>
			                          <span class="error-span"></span>
									</li>
								</div>
								
								<div>
									<li><em style="color: #e5004f;margin-right: 5px;">*</em>次&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;序：
									  <input type="text" style="width:359px;margin-left: 7px;" name="seq" id="seq" value="${bean.seq}"   maxlength="5" size="5"/>
									  <span class="error-span"></span>
									</li>
								</div>
								
								<li class="check"><em style="color: #e5004f;margin-right: 5px;">*</em>是否启用：
									<label class="radio-wrapper <c:if test="${bean.status==1 or empty bean.status}">radio-wrapper-checked</c:if>">
										<span class="radio-item">
											<input type="radio" id="enable" name="status" <c:if test="${bean.status==1 or empty bean.status}">checked="checked"</c:if> value="1" class="radio-input"/>
											<span class="radio-inner"></span>
										</span>
										<span class="radio-txt">是</span>
										<span class="error-span fr"></span>
									</label>
									<label class="radio-wrapper <c:if test="${bean.status==0}">radio-wrapper-checked</c:if>">
										<span class="radio-item">
											<input type="radio" id="notEnable" name="status" <c:if test="${bean.status==0}">checked="checked"</c:if> value="0" class="radio-input" style="margin-left:20px;"/>
											<span class="radio-inner"></span>
										</span>
										<span class="radio-txt">否</span>
									</label>
								</li>
								
								<li class="check"><em style="color: #e5004f;margin-right: 5px;">*</em>打开方式：
									<label class="radio-wrapper <c:if test="${bean.winType=='cur_win' or empty bean.winType}">radio-wrapper-checked</c:if>">
										<span class="radio-item">
											<input type="radio" id="cur_win" name="winType" <c:if test="${bean.winType=='cur_win' or empty bean.winType}">checked="checked"</c:if> value="cur_win" class="radio-input"/>
											<span class="radio-inner"></span>
										</span>
										<span class="radio-txt">本页面</span>
										<span class="error-span fr"></span>
									</label>
									<label class="radio-wrapper <c:if test="${bean.winType=='new_win'}">radio-wrapper-checked</c:if>">
										<span class="radio-item">
											<input type="radio" id="new_win" name="winType" <c:if test="${bean.winType=='new_win'}">checked="checked"</c:if> value="new_win" class="radio-input" style="margin-left:20px;"/>
											<span class="radio-inner"></span>
										</span>
										<span class="radio-txt">新窗口</span>
									</label>
								</li>
							</ul>
							<div class="bot-save" style="margin-bottom: 10px;">
							   <input type="submit" style="float: left;" value="提交申请" class="btn-r big-btn"/>
							   <input type="button" value="返回" onclick="location.href='${contextPath}/s/shopNav/list'" class="btn-g big-btn"/>
							</div>
					</div>
				</form:form>
			</div>
			
		</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script src="${contextPath}/resources/common/js/jquery.validate1.5.5.js" type="text/javascript"></script>
	  <script type="text/javascript">
     	$(document).ready(function(e) {
				userCenter.changeSubTab("shopNav"); //高亮菜单
				
				$("#enable").click(function(){
					$("#enable").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper radio-wrapper-checked");
					$("#notEnable").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper");
				});
				$("#notEnable").click(function(){
					$("#notEnable").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper radio-wrapper-checked");
					$("#enable").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper");
				});
				
				$("#cur_win").click(function(){
					$("#cur_win").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper radio-wrapper-checked");
					$("#new_win").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper");
				});
				$("#new_win").click(function(){
					$("#new_win").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper radio-wrapper-checked");
					$("#cur_win").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper");
				});
				
				jQuery("#form1").validate({
					errorPlacement: function(error, element) {
	    			//element.parent().find("span").html("");
					error.appendTo(element.parent().parent().find(".error-span"));
					},	
					rules: {
						title:{
							"required":true,
							maxlength:10
						},
						url:{
							"required":true
						},
						seq:{
						  "required":true,
						   digits:true 
						},
						status:{
							"required":true
						},
						winType:{
							"required":true,
						}
					},
					messages: {
						title:{
							"required":"请输入导航名称",
							maxlength:"菜单名过长，最长为10个字符"
						},
						url:{
							"required":"请输入URL"
						},
						seq:{
						  "required":"请输入导航排序",
						  digits:"请输入整数"
						},
						status:{
							 "required":"请选择是否启用"
						},
						winType:{
							 "required":"请选择打开方式"
						}
					}
     			});
		});
    </script>
	
</body>
</html>