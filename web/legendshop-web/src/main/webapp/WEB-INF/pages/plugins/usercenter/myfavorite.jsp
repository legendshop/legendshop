<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>商品收藏 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>
<style>
.sold-table th {
	border-bottom:1px solid #e4e4e4;
}
.sold-table td {
	text-align: center;
}

</style>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con">


    <div class="o-mt">
		<h2>商品收藏</h2>
	</div>   
	
		<div class="pagetab2">
              <ul>           
                 <li  id="myFavourite" class="on" ><span>商品收藏</span></li>                
              </ul>       
       </div>
                        
         <div id="recommend" class="recommend" style="display: block;">                
                <table width="100%" cellspacing="0" cellpadding="0" class="buytable sold-table">
                    <tr>
                        <th width="50"  style="text-align: center;">
                        <!-- <input name="" type="checkbox" value=""  id="selectAllCheck"/> -->
                         <span>
					           <label class="checkbox-wrapper" style="float:left;margin-left:9px;">
									<span class="checkbox-item">
										<input type="checkbox" class="checkbox-input selectAll"/>
										<span class="checkbox-inner" style="margin-left:9px;"></span>
									</span>
							   </label>	
						</span>
                        </th>
                        <th >商品图片</td>
                        <th >商品名称</td>
                        <th >收藏时间</td>
                        <th >价格</td>
                        <th style="border-right: 1px solid #e4e4e4;">操作</td>
                    </tr>
                    <c:choose>
			           <c:when test="${empty requestScope.list}">
			              <tr>
						 	 <td colspan="20">
						 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
						 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
						 	 </td>
						 </tr>
			           </c:when>
			           <c:otherwise>
			                <c:forEach items="${requestScope.list}" var="fav" varStatus="status">
					             <tr>
		                          <td height="65">
		                          <%-- <input style="margin-left:18px;" name="favCheck"  type="checkbox"  value=""  id="${fav.id}"/> --%>
		                           <span>
							           <label class="checkbox-wrapper" style="float:left;margin:9px;">
											<span class="checkbox-item">
												<input type="checkbox" id="${fav.id}" name="favCheck" class="checkbox-input selectOne" value="" onclick="selectOne(this);"/>
												<span class="checkbox-inner" style="margin:9px;"></span>
											</span>
									   </label>	
									</span>
		                          </td>
		                          <td><a href="${contextPath}/views/${fav.prodId}" target="_blank"><img src="<ls:images item='${fav.pic}' scale="3"/>"  /></a></td>
		                          <td width="500px" style="text-align: left;">
		                              <a href="${contextPath}/views/${fav.prodId}" target="_blank">${fav.prodName}</a>
		                          </td>  
		                          <td><fmt:formatDate value="${ fav.addtime}" type="date" /></td> 
		                          <td>
		                          	<fmt:formatNumber type="currency" value="${fav.cash}" pattern="${CURRENCY_PATTERN}" />
		                          	<%--<span class="s-change-price-text"> 省${CURRENCY_PATTERN}</span>--%>
		                          </td>
		                          <td><a class="bti btn-r" href="javascript:void(0);" onclick="delFavById(${fav.id},this);" class="ncbtn">删除</a></td>
		                          </tr>
						     </c:forEach>
			           </c:otherwise>
			        </c:choose>
                              
               </table>
         				
	     </div> 
	      <c:if test="${not empty requestScope.list }" >
         	<div class="fl search-01">
                 <input class="bti btn-r" type="button" value="批量删除"  id="delBtn" style="cursor: pointer;"/>
                 <input class="bti btn-g" type="button" value="清空"  id="clrBtn"  style="cursor: pointer;"/>
              </div> 
	      <div style="margin-top:10px;" class="page clearfix">
   			 <div class="p-wrap">
				<span class="p-num">
					<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
				</span>
   			 </div>
 		  </div>
         </c:if>     
	       
            
 </div>
    <!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/alternative.js'/>"></script>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/myFavorite.js'/>"></script>
  <script type="text/javascript">
  var contextPath = "${contextPath}";
</script>
</body>
</html>
			