<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<!-- 发票信息 -->
<div class="invoice">
	                <div class="invoice-tit">
	                   <b>发票信息</b>
	                </div>
	                <c:choose>
	                    <c:when test="${empty userdefaultInvoice}">
							<div class="invoice-no-spr">
		                      <p>不需要发票  <a href="javascript:void(0);" onclick="selectInvoice();" style="text-decoration: underline !important;color: #e5004f !important;">请选择</a></p>
		                    </div>
	                    </c:when>
						<c:otherwise>
							<div class="invoice-no-spr">
								<div class="invoice-info">
									<dl>
										<dt>发票类型：</dt>
										<dd class="">
											<c:choose>
												<c:when test="${userdefaultInvoice.type eq 1}">
													普通发票
												</c:when>
												<c:when test="${userdefaultInvoice.type eq 2}">
													增值税发票
												</c:when>
											</c:choose>
											<a href="javascript:void(0);" onclick="selectInvoice();">请选择</a>
										</dd>
										<dt>发票抬头：</dt>
										<dd>
											<c:choose>
												<c:when test="${userdefaultInvoice.title eq 1}">[个人抬头]&nbsp;${userdefaultInvoice.company}</c:when>
												<c:when test="${userdefaultInvoice.title eq 2}">[公司抬头]&nbsp;${userdefaultInvoice.company}</c:when>
												<c:otherwise>&nbsp;${userdefaultInvoice.company}</c:otherwise>
											</c:choose>
										</dd>
										<c:if test="${userdefaultInvoice.title ne 1}">
										<dt>纳税人号：</dt><dd>${userdefaultInvoice.invoiceHumNumber}</dd>
										</c:if>
									</dl>
								</div>
							</div>
						</c:otherwise>
					</c:choose>
	           </div>

<!-- 发票信息 -->
<script type="text/javascript">
	var contextPath = "${contextPath}";
</script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/orderInvoice.js'/>"></script>
