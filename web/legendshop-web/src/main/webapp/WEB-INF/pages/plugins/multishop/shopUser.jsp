<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
	<head>
		<title>新增员工</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
		<title>新增员工-${systemConfig.shopName}</title>
   		 <meta name="keywords" content="${systemConfig.keywords}"/>
    	<meta name="description" content="${systemConfig.description}" />
		<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/shopUser.css'/>" rel="stylesheet"/>
		<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller.css'/>" rel="stylesheet"/>
		<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
	</head>
	<body>
		<form:form  action="${contextPath}/s/shopUsers/save" method="post" id="from1" target="_parent">
			<input type="hidden" value="${shopUser.id}" name="id">
			<div>
				<table>
					<c:if test="${empty uType || uType == 1}">
					<tr>
						<td class="td-title"><b>*</b>员工账号</td><td> <input value="${shopUser.name}" srcval="${shopUser.name}" maxlength="20" onfocus="clearErr(this);" onblur="checkName();" id="shopUserName" name="name" ><span class="err-msg hide"></span></td>
					</tr>
					</c:if>
					<c:if test="${empty uType || uType == 2 }">
					<tr>
						<td class="td-title"><b>*</b>密码</td><td> <input type="password" onfocus="clearErr(this);" onblur="checkPasswd();" id="password" name="password" maxlength="20"><span class="err-msg hide"></span></td>
					</tr>
					<tr>
						<td class="td-title"><b>*</b>确认密码</td><td> <input type="password" onfocus="clearErr(this);" onblur="checkPasswd2();" id="password2" maxlength="20"><span class="err-msg hide"></span></td>
					</tr>
					</c:if>
					<c:if test="${empty uType || uType == 1}">
					<tr>
						<td class="td-title"><b>*</b>员工姓名</td><td> <input value="${shopUser.realName}" maxlength="20" onfocus="clearErr(this);" onblur="checkRealName();" id="realName" name="realName" maxlength="20"><span class="err-msg hide"></span></td>
					</tr>
					<tr>
						<td class="td-title"><b>*</b>职位</td>
						<td> 
							<c:choose>
								<c:when test="${not empty requestScope.shopRoles}">
									<select name="shopRoleId" id="shopRoleId">
										<c:forEach items="${requestScope.shopRoles}" var="shopRole" varStatus="status">
											<option value="${shopRole.id}" <c:if test="${shopRole.id==shopUser.shopRoleId}" >selected = "selected"</c:if> >${shopRole.name}</option>
										</c:forEach>
									</select>
								</c:when>
								<c:otherwise>
									<div style="color: #aaa;font-size: 12px;margin-left: 5px;">
									您还没有创建职位，请先创建职位<a href="${contextPath}/s/shopJobs/query" target=_parent  style="background: #e5004f;color: #ffffff;font-size: 13px;text-decoration: none;border-radius: 3px;margin-left: 5px;">去创建</a>
									</div>
								</c:otherwise>
							</c:choose>
							
						</td>
					</tr>
					</c:if>
				  	<!-- <input type="hidden"  id="type" name="type"> -->
				</table>
			</div>
			<div class="btn-content">
				<input onclick="saveUser();" type="button" value="保存" class="btn-r small-btn">
				<input onclick="closeDialog();" style="margin-left:10px;" type="button" value="取消" class="btn-g small-btn">
			</div>
		</form:form>
		<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/shopUser.js'/>"></script>
		<script type="text/javascript">
			var contextPath = '${contextPath}';
			var uType = '${uType}';
		</script>
	</body>
</html>