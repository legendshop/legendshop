<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>商品类目-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-category${_style_}.css'/>" rel="stylesheet">
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
<div id="doc">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
    <div class="w1190">
        <div class="seller-cru">
            <p>
                您的位置>
                <a href="${contextPath}/home">首页</a>>
                <a href="${contextPath}/sellerHome">卖家中心</a>>
                <span class="on">商品类目</span>
            </p>
        </div>
        <%@ include file="included/sellerLeft.jsp" %>
        <div class="seller-category">

            <!-- 搜索 -->
            <div class="seller-com-nav">
                <ul>
                    <li class="on" id="shopCatList"><a href="javascript:void(0);">一级类目列表</a></li>
                </ul>
            </div>
            <form:form action="${contextPath}/s/shopCategory" method="get" id="from1">
                <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
                <div class="sold-ser sold-ser-no-bg clearfix">
                	<div class="fr">
	                	<div class="item">
		                                                     商品类目：
		                    <input type="text" class="item-inp" value="${shopCate.name}" id="name" name="name" style="width:200px;">
		                    <input type="button" onclick="search()" class="bti btn-r" id="btn_keyword" value="查询类目">
		                    <input type="button" class="bti btn-r" id="shopCatConfig" value="添加一级类目">
                            <input type="button" class="bti btn-r" id="expireShopCategory" value="清除类目缓存">
	                    </div>
                	</div>
                </div>	
            </form:form>
            <table class="category-table sold-table">
                <tr class="category-tit">
                    <th>名称</th>
                    <th>次序</th>
                    <th width="150">操作</th>
                </tr>
                <c:choose>
		           <c:when test="${empty requestScope.list}">
		              <tr>
					 	 <td colspan="20" style="border-right: 1px solid #e4e4e4">
					 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
					 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
					 	 </td>
					 </tr>
		           </c:when>
		           <c:otherwise>
		             <c:forEach items="${requestScope.list}" var="category" varStatus="status">
	                    <tr class="detail">
	                        <td>${category.name}</td>
	                        <td>${category.seq}</td>
	                        <td>
	                            <ul class="mau">
	                                <c:choose>
	                                    <c:when test="${category.status == 1}">
	                                        <li><a href="javascript:void(0);" class="btn-r" name="statusImg" catId="${category.id}" catName="${category.name}" status="${category.status}">下线</a></li>
	                                    </c:when>
	                                    <c:otherwise>
	                                        <li><a href="javascript:void(0);" class="btn-r" name="statusImg" catId="${category.id}" catName="${category.name}" status="${category.status}">上线</a></li>
	                                    </c:otherwise>
	                                </c:choose>
	                                <li><a href='javascript:nextCatQuery("${category.id}");' title="二级类目" class="btn-g">子类目</a></li>
	                                <li><a href='javascript:updateCat("${category.id}");' title="修改" class="btn-g">修改</a></li>
	                                <li><a href='javascript:deleteCat("${category.id}","${category.name}")' title="删除" class="btn-g">删除</a></li>
	                            </ul>
	                        </td>
	                    </tr>
	                </c:forEach>
		           </c:otherwise>
		         </c:choose>
            </table>
            <div class="clear"></div>
            <div style="margin-top:10px;" class="page clearfix">
                <div class="p-wrap">
                    <ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="simple"/>
                </div>
            </div>
        </div>
    </div>
    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/shopCat.js'/>"></script>
<script type="text/javascript">
    var contextPath = "${contextPath}";
    function search() {
        $("#from1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
        $("#from1")[0].submit();
    }
</script>
</body>
</html>