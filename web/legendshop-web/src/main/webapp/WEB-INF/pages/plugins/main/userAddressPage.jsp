<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<c:if test="${not empty requestScope.list}">
<c:if test="${fn:length(requestScope.list)>4}">
  <div id="spec-left" class="control  back" > &lt; </div>
  <div id="spec-right" class="control forward"  > &gt; </div>
</c:if>
<div style="position: relative; overflow: hidden; z-index: 1; width: 1112px; height: 190px;margin-left:39px;" id="deliveryAddress">
<ul style="width: 5600px; overflow: hidden; position: absolute; left: 0pt; top: 0pt; margin-top: 0pt; margin-left: 0pt;">
<c:forEach  items="${requestScope.list}" var="deliveryAddress" end="19"  varStatus="status">
        <c:choose>
			<c:when test="${deliveryAddress.commonAddr ==1}">  
				<dl class="on" addrId="${deliveryAddress.addrId}">
			</c:when>
			<c:otherwise>
			   <dl addrId="${deliveryAddress.addrId}">
			</c:otherwise>
		</c:choose>
			<dt>
				<span class="tit"  style="overflow:hidden;text-overflow: ellipsis;white-space: nowrap;" title="${deliveryAddress.province}${deliveryAddress.city}">${deliveryAddress.province}${deliveryAddress.city}</span>
				<span  style="overflow:hidden;text-overflow: ellipsis;white-space: nowrap;width: 130px;float: left;" title="（${deliveryAddress.receiver}收）">
				<c:choose>
					<c:when test="${fn:length(deliveryAddress.receiver) >7}">
						（${fn:substring(deliveryAddress.receiver,0,7)}...收）
					</c:when>
					<c:otherwise>
						（${deliveryAddress.receiver}收）
					</c:otherwise>
				</c:choose>	
				
				</span>
				<c:choose>
					<c:when test="${deliveryAddress.commonAddr ==1}">
						<div class="defaultTip">默认地址</div>
					</c:when>
					<c:otherwise>
					  	<span class="def"  onclick="javascript:setDefaultAddr('${deliveryAddress.addrId}');"  >设为默认</span>	
					</c:otherwise>
				</c:choose>
			</dt>
			<dd class="addr-con">
			<span class="tel">${deliveryAddress.mobile}&nbsp;&nbsp;&nbsp;<font style="font-weight: 700;">${deliveryAddress.aliasAddr}</font></span></br>
			   <span style="overflow:hidden; text-overflow:ellipsis;display:-webkit-box; -webkit-box-orient:vertical;-webkit-line-clamp:2;">
			   	 ${deliveryAddress.area}-${deliveryAddress.subAdds}
			   </span>
			</dd>
			<dd class="ubtn">
				  <b class="del"  onclick="editAddress('${deliveryAddress.addrId}',event);" style="color: #e63;">修改</b>
			      <c:if test="${deliveryAddress.commonAddr !=1}">
			          <b class="del"  onclick="delAddress('${deliveryAddress.addrId}',event);"  >删除</b>
			      </c:if>
			</dd>
			<dd class="sel-on"></dd>
	</dl>
</c:forEach>
</ul>
</div>
</c:if>
