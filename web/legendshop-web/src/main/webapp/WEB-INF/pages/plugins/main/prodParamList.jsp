<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<c:set var="treeNodes"  value="${indexApi:findTreeNodeNavigation(searchParams.categoryId)}"></c:set>

           <!--crumb-->
            <div id="J_crumbs" class="crumb yt-wrap">
             <div class="crumbCon">
                      <div class="crumbSlide">
                        <div class="crumbs-nav-item one-level">
		                    <a class="crumbs-link" href="<ls:url address="/"/>"> 首页 </a><i class="crumbs-arrow">&gt;</i>
		                </div>
		                
		               <c:choose>
						  <c:when test="${not empty treeNodes && (fn:length(treeNodes) >= 1)}">
						     <c:forEach items="${treeNodes}" var="node" varStatus="n">
						          <div class="crumbs-nav-item">
		                            <%-- <c:choose>
						                 <c:when test="${!n.last}"> --%>
				                            <div class="menu-drop">
				                              <div class="trigger"><span class="curr">${node.nodeName}</span><i class="menu-drop-arrow"></i></div>
				                              <div class="menu-drop-main">
				                                   <c:if test="${not empty node.compatriots}">
								                        <ul class="menu-drop-list">
								                    		<c:forEach items="${node.compatriots}" var="compatriot" varStatus="c">
								                    		     <li><a title="${compatriot.nodeName}" href="<ls:url address="/list?categoryId=${compatriot.selfId}"/>">${compatriot.nodeName}</a></li>
								                        	</c:forEach>
								                        </ul>
								                    </c:if>
				                              </div>
				                            </div>
		                               <%-- </c:when>
						                <c:when test="${n.last}">
						                    <a class="crumbs-link" href="javascript:void(0);">${node.nodeName}</a>
						                </c:when>
						              </c:choose> --%>
						                <c:if test="${!n.last}"><i class="crumbs-arrow">&gt;</i></c:if>
		                          </div>
						     </c:forEach>
						  </c:when>
					      <c:when test="${not empty treeNodes && (fn:length(treeNodes) < 1)}">
							<c:forEach items="${treeNodes}" var="node" varStatus="n">
							       <div class="crumbs-nav-item one-level">
                                      <a class="crumbs-link" href="javascript:void(0);"><c:out value="${node.nodeName}"></c:out></a>
                                   </div>  
						    </c:forEach>
					      </c:when>
					     <c:otherwise></c:otherwise>
				      </c:choose>
				      
				       <c:if test="${not empty  facetProxy.selections}">
                            <div class="crumbs-nav-item">
                                <i class="crumbs-arrow">&gt;</i>
                           </div>
                           <c:forEach items="${facetProxy.selections}" var="selections">
                             <div class="crumbs-nav-item crumbAttr">
                                <a href="javascript:void(0);">${fn:substring(selections.title,0,6)}:${selections.name}</a>
                                <a href="javascript:void(0);" onclick="javascript:removeFacetData('${selections.url}');" class="crumbDelete">X</a>
                             </div>
			               </c:forEach>
                         </c:if>
                     </div>
               </div>
            </div>            
            <!--crumb end-->
            <c:set var="_unSelections" value="${facetProxy.unSelections}" />
            
            <div class="selector wrapper">
            <c:if test="${not empty searchParams.keyword}">
	            <div class="title">
			      <h3><em>搜索关键字 - <strong class="c-danger f14">“${searchParams.keyword}”</strong> - </em>筛选结果</h3>
			    </div>
		    </c:if>
            <c:if test="${not empty _unSelections&&fn:length(_unSelections) > 0}">
                 <!--search-->
	            <div style="display:block" class="attrs yt-wrap">
	            <c:forEach items="${_unSelections}" var="unSelections" varStatus="s">
                    <c:if test="${fn:length(unSelections.couts) > 0}">
                       <c:choose>
	                       <c:when test="${unSelections.title=='品牌'}">
	                          <c:if test="${not empty unSelections.couts}">
	                              <div class="brandAttr unSelections"  >
						            <div class="j_Brand attr">
						                 <div class="attrKey" style="width: 8%;"><strong>品牌：</strong></div>
						                 <div class="attrValues showLogo" style="height: auto;">
						                     <div class="j_BrandSearch av-search clearfix">
						                        <input type="text" value="" placeholder="搜索&nbsp;品牌名称" style="color: rgb(191, 191, 191);">
						                     </div>
						                      <ul class="av-collapse" >
						                      <c:forEach items="${unSelections.couts}" var="count" varStatus="c">
						                          <c:set var="brand"  value="${indexApi:findBrand(count.value)}"></c:set>
						                          <c:if test="${not empty brand }">
						                             <li <c:if test="${c.count>8}">nc_type='none' style='display: none;'</c:if> >
						                                 <a rel="nofollow" data-f="spu-brand-qp" data-c="brand-qp" href="javascript:void(0);" onclick="javascript:onclickFacetData('${count.url}');" title="${brand.brandName}" >
						                                 <i></i>
						                                 <img src="<ls:photo item="${brand.brandPic}"></ls:photo>" width="102" height="36" />${brand.brandName}</a>
						                              </li>
						                          </c:if> 
												</c:forEach>
						                      </ul>
						                      <c:if test="${(fn:length(unSelections.couts) > 8)}">
							                  	   <div class="av-options">
						                              <a class="j_More avo-more ui-more-drop-l" href="javascript:;" style="display: inline;" nc_type="show" >更多∨ </a>
						                           </div>
									            </c:if>
						                 </div>
						            </div>
						         </div>
	                          </c:if>
	                       </c:when>
        	               <c:otherwise>
        	                  <c:if test="${not empty unSelections.couts}">
			                    <div class="propAttrs unSelections" <c:if test="${s.count>4}">style='display: none;'</c:if> >
							      <div class="j_Prop attr hotspot">
							        <div class="attrKey" style="width: 8%;">${unSelections.title}：</div>
							        <div class="attrValues">
							            <ul class="av-collapse">
							               <c:forEach items="${unSelections.couts}" var="count">
							                   <li><a style="cursor: pointer; padding: 1px 7px 1px 0px;" onclick="onclickFacetData('${count.url}');" >${count.name}</a></li>
							               </c:forEach>
							            </ul>
							        </div>
							     </div>
							   </div>
			                 </c:if>
	                       </c:otherwise>
	                     </c:choose>
                    </c:if>
                 </c:forEach>
                 
                  <div class="attrExtra"  >
				        <div class="attrExtra-border"></div>
				          <c:if test="${fn:length(_unSelections) > 4}">
				             <a class="attrExtra-more j_MoreAttrs " id="showUnSelections"   onclick="showUnSelections();" ><i></i>更多选项</a>
				             <a class="attrExtra-more j_MoreAttrs attrExtra-more-drop" style="display: none;" id="hideUnSelections" onclick="hideUnSelections();"  ><i></i>精简选项</a>  
				          </c:if>
				   </div> 
	            </div>
            </c:if>
            </div>
      <!-- 商品 属性 -->
      
      
<script type="text/javascript" >
   jQuery(document).ready(function() {
   
	   $('a[nc_type="show"]').click(function(){
		var s = $(this).parent().parent().find('li[nc_type="none"]');
		if($(s).css('display') == 'none'){
			$(s).show();
			$(this).html('收起∧');
		}else {
			$(s).hide();
			$(this).html('更多∨ ');
		}
	   });
    
	});
	

    	
	   $(".crumbs-nav-item .menu-drop").mouseenter(function(mevent){
		    $(this).addClass("z-menu-drop-open");
       });
    
       $(".crumbs-nav-item .menu-drop").mouseleave(function(mevent){
           $(this).removeClass("z-menu-drop-open");
       })
</script>

