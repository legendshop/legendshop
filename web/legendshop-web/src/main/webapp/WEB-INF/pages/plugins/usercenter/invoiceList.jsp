<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>开票信息 - ${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}" />
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />

    <style type="text/css">
        .sold-table td{
            max-width: 185px !important;
            white-space: nowrap !important;
            padding-left: 5px !important;
        }

    </style>
</head>
<body class="graybody">
<div id="bd">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
    <!----两栏---->
    <div class=" yt-wrap" style="padding-top:10px;">
        <%@include file='usercenterLeft.jsp'%>

        <!-----------right_con-------------->
        <div class="right_con"  id="content">
            <div class="o-mt">
                <h2>我的发票</h2>
            </div>
            <div class="pagetab2">
                <ul>
                    <li ><a href="${contextPath}/p/invoice/myInvoice">发票列表</a></li>
                    <li class="on"><span>开票信息</span></li>
                </ul>
            </div>
            <div class="o-mb" style="margin: 10px 0;">

                <h3>
                    <a href="javascript:void(0)" class="btn-r" style="width:100px;margin-left:0;" onclick="addInvoice();">新增发票信息</a>
                </h3>
            </div>
            <div id="recommend" class="recommend" style="display: block;">
                <table id="item" style="width:100%;text-align: center;" cellspacing="0" cellpadding="0" class="buytable mailtab sold-table">
                    <tbody>
                    <tr>
                        <%--<th width="30" >序号</th>--%>
                        <th width="70">发票类型</th>
                        <th width="60">发票抬头</th>
                        <th width="80">税号</th>
                        <th width="100">注册地址</th>
                        <th width="100">开户银行及账号</th>
                        <th width="100">操作</th>
                    </tr>
                    <c:choose>
                        <c:when test="${empty requestScope.list}">
                            <tr>
                                <td colspan="6">
                                    <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
                                    <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
                                </td>
                            </tr>
                        </c:when>
                        <c:otherwise>
                            <c:forEach items="${requestScope.list}" var="invoice" varStatus="status">
                                <tr>
                                    <%--<td>${status.count}</td>--%>
                                    <td>
                                        <c:choose>
                                            <c:when test="${invoice.type eq 1}">
                                                普通-<c:choose>
                                                <c:when test="${invoice.title eq 1}">个人</c:when>
                                                <c:when test="${invoice.title eq 2}">公司</c:when>
                                            </c:choose>
                                            </c:when>
                                            <c:when test="${invoice.type eq 2}">增值税</c:when>
                                        </c:choose>
                                        <c:if test="${invoice.commonInvoice eq 1}">
                                            <span class="common" style="margin-left: 5px;background-color: #E5004F;color: honeydew;padding-left: 3px;">默认</span>
                                        </c:if>
                                    </td>
                                    <td>${invoice.company}</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${empty invoice.invoiceHumNumber}">无</c:when>
                                            <c:otherwise>${invoice.invoiceHumNumber}</c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${empty invoice.registerAddr}">无</c:when>
                                            <c:otherwise>${invoice.registerAddr}</c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${empty invoice.depositBank}">无</c:when>
                                            <c:otherwise>${invoice.registerAddr}<br /> ${invoice.bankAccountNum}</c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td>
                                        <a class="btn-r" href='javascript:updateById("${invoice.id}")' title="修改">修改</a>
                                        <a class="btn-g" href='javascript:deleteById("${invoice.id}")' title="删除">删除</a>
                                        <c:if test="${invoice.commonInvoice ne 1}">
                                            <a class="btn-g" href='javascript:setDefault("${invoice.id}")' title="设为默认">设为默认</a>
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                    </tbody>
                </table>
            </div>
            <c:if test="${not empty requestScope.list }">
                <div style="margin-top:10px;" class="page clearfix">
                    <div class="p-wrap">
						<span class="p-num">
							<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
						</span>
                    </div>
                </div>
            </c:if>
        </div>
        <div class="clear"></div>
    </div>
    <!----两栏end---->

    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
</div>
<!--bd end-->
<script type="text/javascript"  src="<ls:templateResource item='/resources/templets/js/invoiceList.js'/>"></script>
<script type="text/javascript">
    var contextPath = "${contextPath}";

</script>
</body>
</html>