<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>商家结算-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-orderBillList${_style_}.css'/>" rel="stylesheet">
</head>
    <body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
   <div id="Content" class="w1190">
	<div class="seller-cru">
				<p class="clearfix">
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">商家结算</span>
				</p>
			</div>
				<%@ include file="included/sellerLeft.jsp" %>
	<div class="seller-orderBillList">
			<!-- <div class="now-have" style="margin-bottom:20px;">
					<span style="color:#FF3300;">当前与平台结算日为：每周星期一</span>
			</div> -->
		<div class="pagetab2">
			<ul id="listType">
				<li class="on"><span>商家结算</span></li>
			</ul>
		</div>
		<form:form  action="${contextPath}/s/shopOrderBill/query" method="get" id="from1">
			<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
			<div class="sold-ser sold-ser-no-bg clearfix">
				<div class="fr">
					<div class="item">账单状态:&nbsp; <select name="status" class="item-sel text">
						<ls:optionGroup type="select" required="false" cache="true"
			                beanName="SHOP_BILL_STATUS" selectedValue="${shopOrderBill.status}"/>
					</select>
					</div>
					
					<div class="item">结算单号:&nbsp; 
						<input type="text item-inp" style="width:200px;" value="${shopOrderBill.sn}" class="item-inp text"  name="sn">
						<input type="button" onclick="search()" class="btn-r" id="btn_keyword" value="查询" >
					</div>	
				</div>
			</div>
		</form:form>
	
		<table id="shopOrderBill"  class="orderBillList-table sold-table">
		<tr class="orderBillList-tit">
			<th width="100">档期</th>
			<th width="220">结算单号</th>
			<th width="200">起止时间</th>
			<th width="160">本期应收</th>
			<th width="100">结算状态</th>
			<th width="100">付款日期</th>
			<th width="100">操作</th>
		</tr>
		  <c:choose>
	           <c:when test="${empty requestScope.list}">
	              <tr>
				 	 <td colspan="20">
				 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
				 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
				 	 </td>
				 </tr>
	           </c:when>
	           <c:otherwise>
	              <c:forEach items="${requestScope.list}" var="shopOrderBill" varStatus="status">
		            <tr class="detail">
		            	<td height="60">${shopOrderBill.flag}</td>
		                <td height="60">${shopOrderBill.sn}</td>
		                
		                <td><fmt:formatDate value="${shopOrderBill.startDate}" pattern="yyyy-MM-dd"/>&nbsp;至&nbsp;<fmt:formatDate value="${shopOrderBill.endDate}" pattern="yyyy-MM-dd"/></td>
		                <td>¥<fmt:formatNumber  pattern="#.##" value="${shopOrderBill.resultTotalAmount}" /></td>
		                <td>
		                	<c:choose>
		                		<c:when test="${shopOrderBill.status==1}">已出账</c:when>
		                		<c:when test="${shopOrderBill.status==2}">商家已确认</c:when>
		                		<c:when test="${shopOrderBill.status==3}">平台已审核</c:when>
		                		<c:when test="${shopOrderBill.status==4}">结算完成</c:when>
		                	</c:choose>
		                </td>
		                <td><fmt:formatDate value="${shopOrderBill.payDate}" pattern="yyyy-MM-dd"/></td>
		                <td>
		 					<a target="_blank" href='${contextPath}/s/shopOrderBill/load/${shopOrderBill.id}' title="查看" class="btn-r"><span>查看</span></a>
		                </td>
		            </tr>
		         </c:forEach> 
	           </c:otherwise>
           </c:choose>
		</table>
	
		<div class="clear"></div>
	      	<div style="margin-top:10px;" class="page clearfix">
    			 <div class="p-wrap">
           		  <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
    			 </div>
			</div>
  		 </div>
  	 </div>
   <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
   </div>
   <script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
	<script src="<ls:templateResource item='/resources/common/js/recordstatus.js'/>" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
		 //三级联动
  	  $("select.combox").initSelect();
  	userCenter.changeSubTab("orderBill"); 
 });
 
function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("from1").submit();
}
function search(){
  	$("#from1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
  	$("#from1")[0].submit();
}		
</script>
</body>
 </html>
