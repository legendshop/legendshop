<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<div id="doc">
<%@include file='home/top.jsp'%>
   <div id="bd">
      <!--order content -->
         <div class="yt-wrap ordermain" id="entry">
          <!-- 收货地址 -->
         	  <div class="cartnew-title clearfix">
                        <ul class="step_psw">
                            <li class="done"><span>填写账户名</span></li>
                            <li class="done"><span>验证身份</span></li>
                            <li class="act "><span>设置新密码</span></li>
                            <li class=""><span>完成</span></li>
                        </ul>
              </div>
              
          <!-- 支付方式 -->
              <div class="reg_b getpsw">
	              <div class="item" style="display:none">
					<span class="label">历史收货人手机号码：</span>
					<div class="fl">
						<input tabindex="1" onfocus="mobileFocus();" onblur="mobileBlur();" class="text " id="mobile" type="text">
						<label class="blank invisible" id="username_succeed"></label>
						<span class="clr"></span>
						<label id="mobile_error" class=""></label>
					</div>
				</div>
					<input class="text" id="code" name="code" type="hidden" value="${code}"/>
					<input class="text" id="userName" name="userName" type="hidden" value="${userName}"/>
                  <div class="item"  style="height: 90px;">
                        <span class="label">新密码：</span>
                        <div class="fl item-ifo">
                            <div class="r_inputdiv">
                                <input type="password" id="password" class="text" maxlength="16" maxlength="18" tabindex="1">
                                <span class="clr"></span>
								<label id="pwdstrength" class="hide" ><span class="fl">安全程度：</span><b></b></label>
								<span class="clr"></span>
								<label id="password_error" class=""></label>
								<span class="clr"></span>
                            </div>
                        </div>
                  </div>
                  
                  <div class="item">
                        <span class="label">确认密码：</span>
                        <div class="fl item-ifo">
                            <div class="r_inputdiv">
                                <input type="password" class="text" maxlength="16" id="repassword" value="" tabindex="2">
                                <span class="clr"></span>
								<label id="repassword_error" class=""></label> 
                            </div>
                        </div>
                  </div>
                  
                  <div class="item">
                        <span class="label">&nbsp;</span>
                        <input type="button" class="btn-img btn-regist" value="保存" onclick="updatePassword();" tabindex="8">
                  </div>
              </div>
        </div>
   </div>
   <%@include file='home/bottom.jsp'%>
</div>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/setNewPwd.js'/>"></script> 
