<!DOCTYPE html>
	<%@ page language="java" pageEncoding="UTF-8"%>
	<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
	<jsp:useBean id="now" class="java.util.Date"/>
	<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss" var="nowDate"/>
<html>
<head>
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>团购活动管理-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/groupon${_style_}.css'/>" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="#">首页</a>>
					<a href="#">卖家中心</a>>
					<span class="on">查看团购活动</span>
				</p>
			</div>
			<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
			<div class="group-right">
					
				<!-- 基本信息 -->
				<div class="basic-ifo">
					<!-- 基本信息头部 -->
					<div class="ifo-top">
						基本信息
					</div>
					<div class="ifo-con">
						<li style="margin-bottom: 5px;">
							<span class="input-title"><span class="required">*</span>活动名称：</span>
							<input type="text" disabled="disabled" class="active-name" name="groupName" id="groupName" value="${group.groupName}" maxLength="50" placeholder="请填写活动名称"><em></em>
						</li>
						<li>
							<span class="input-title"></span>
						</li>
						<li style="margin-bottom: 0;">
							<span class="input-title"><span class="required">*</span>活动时间：</span>
							<input type="text" disabled="disabled" class="active-time" readonly="readonly"  name="startTime"  id="startTime" value='<fmt:formatDate value="${group.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>' placeholder="开始时间"/><em></em>
							<span style="margin: 0 5px;">-</span> 
							<input type="text" disabled="disabled" class="active-time" readonly="readonly"  name="endTime"  id="endTime" value='<fmt:formatDate value="${group.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>' placeholder="结束时间"/><em></em>
						</li>
					</div>
				</div>
				<!-- 选择活动商品 -->
				<div class="basic-ifo" style="margin-top: 20px;">
					<!-- 选择活动商品头部 -->
					<div class="ifo-top">
						选择活动商品
					</div>
					<div class="ifo-con">
						
						<div class="check-shop prodList">
							<c:if test="${not empty prodLists}">
								<table border="0" id="skuList" cellpadding="0" cellspacing="0" style="margin: 30px 0;">
									<tbody>
									<tr style="background: #f9f9f9;">
										<td width="70px">商品图片</td>
										<td width="170px">商品名称</td>
										<td width="100px">商品规格</td>
										<td width="100px">商品价格</td>
										<td width="100px">可销售库存</td>
										<td width="100px">团购价(元)</td>
									</tr>
									<c:choose>
										<c:when test="${not empty prodLists}">
											<c:forEach items="${prodLists}" var="SkuList">
												<c:forEach items="${SkuList.dtoList}" var="groupSku">
													<tr class="sku" >
														<td>
															<img src="<ls:images item='${groupSku.prodPic}' scale='3' />" >
														</td>
														<td>
															<span class="name-text">${groupSku.prodName}</span>
														</td>
														<td>
															<span class="name-text">${empty groupSku.cnProperties?'暂无':groupSku.cnProperties}</span>
														</td>
														<td>
															<span class="name-text">${groupSku.skuPrice}</span>
														</td>
														<td class="prodStocks_p1">${groupSku.skuStocks}</td>
														<td>
															<input type="text" disabled class="set-input groupPrice" id="groupPrice" name="groupPrice" style="text-align: center" value="${groupSku.groupPrice}">
														</td>
													</tr>
												</c:forEach>
											</c:forEach>

										</c:when>
										<c:otherwise>
											<tr class="first">
												<td colspan="7" >暂未选择商品</td>
											</tr>
										</c:otherwise>
									</c:choose>
									</tbody>
								</table>
							</c:if>
							<c:if test="${empty prodLists}">
								<div style="text-align: center">商品不存在!</div>
							</c:if>
						</div>
					</div>
					<input type="hidden" name="productId" id="productId" value="${group.productId}"/>
				</div>
				<!-- 活动规则 -->
				<div class="basic-ifo" style="margin-top: 20px;">
					<!-- 活动规则头部 -->
					<div class="ifo-top">
						活动规则
					</div>
					<div class="ifo-con">
						
						<c:if test="${not empty group.groupImage}">
							<span class="input-title"><span class="required"></span>活动图片：</span>
							<img  src="<ls:photo item='${group.groupImage}'/>" style="max-height: 100px;max-width: 100px;vertical-align: middle;">
						</c:if>
						<li>
							<span class="input-title"><span class="required">*</span>每人限购：</span>
							<span class="act-limit">
								<input type="text" disabled="disabled" class="active-time" maxLength="5" onchange="checkInputNumForNormal(this,0);" onkeyup="checkInputNumForNormal(this,0);" name="buyQuantity" id="buyQuantity" value="${group.buyQuantity}" placeholder="此次活动可以购买的商品总数量"/>
							</span>
						</li>
						<li>
							<span class="input-title"><span class="required">*</span>团购价格：</span>
							<span class="act-limit">
								<input type="text" disabled="disabled" class="active-time"  maxLength="8" onchange="checkInputNumForNormal(this,2);" onkeyup="checkInputNumForNormal(this,2);" name="groupPrice" id="groupPrice" value="${group.groupPrice}" placeholder="请填写团购价格"/><em></em>
							</span>
						</li>
						<li>
							<span class="input-title"><span class="required">*</span>成团人数：</span>
							<span class="act-limit">
								<input type="text" disabled="disabled" class="active-time" maxLength="8" name="peopleCount" id="peopleCount" value="${group.peopleCount}" placeholder="请填写成团人数" /><em></em>
							</span>
						</li>
						<li>
							<span class="input-title"><span class="required">*</span>活动分类：</span>
							<span class="act-limit">
								<input type="text" disabled="disabled" class="active-time" style="cursor: pointer;" id="categoryName" name="categoryName" value="${group.categoryName}" onclick="loadCategoryDialog();"   placeholder="请选择团购分类" readonly="readonly"/><em></em>
							</span>
						</li>
						<li>
							<span class="input-title"><span class="required"></span>活动描述：</span>
							<div class="rich-text">
								<textarea name="groupDesc" id="groupDesc" cols="100" rows="8" style="width:700px;height:300px;visibility:hidden;" placeholder="（选填）">${group.groupDesc}</textarea>
							</div>
						</li>
					</div>
				</div>
				<div class="bto-btn">
					<div class="red-btn return-btn" onclick="javascript:history.go(-1);">返回</div>
				</div>
				
			</div>
		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script type="text/javascript">
	jQuery(document).ready(function(){
  		userCenter.changeSubTab("groupManage"); 
  		
  		KindEditor.options.filterMode=false;
		 KindEditor.create('textarea[name="groupDesc"]', {
			cssPath : '${contextPath}/resources/plugins/kindeditor/plugins/code/prettify.css',
			uploadJson : '${contextPath}/editor/uploadJson/upload;jsessionid=${cookie.SESSION.value}',
			fileManagerJson : '${contextPath}/editor/uploadJson/fileManager',
			allowFileManager : true,
			resizeType: 0,
			afterBlur:function(){this.sync();},
			width : '100%',
			height:'350px',
			afterCreate : function() {
				var self = this;
				KindEditor.ctrl(document, 13, function() {
					self.sync();
					document.forms['example'].submit();
				});
				KindEditor.ctrl(self.edit.doc, 13, function() {
					self.sync();
					document.forms['example'].submit();
				});
			}
		});
 	});
</script>
</body>
</html>