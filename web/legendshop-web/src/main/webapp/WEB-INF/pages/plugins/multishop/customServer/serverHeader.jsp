
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!-- 头部 -->
<div class="u-head">
	<div class="head-box clear">
		<div class="logo fl">
			<a href="${contextPath}/customerService/conversation" class="logo-img">
				<img src="${OssDomainName}/${logo}" alt="">
			</a>
			<span class="logo-txt">客服端</span>
		</div>
		<div class="log-reg fr">
			<a href="${contextPath}/customerService/login">
				<img src="${contextPath}/resources/templets/images/server/per-icon.png" alt="">
				退出登录
			</a>
		</div>
		<div class="nav fr clear">
			<span class="nav-item wel">欢迎您，${serverAccount.name}</span>
			<!-- <a href="javascript:void(0)" class="nav-item state">
				<span class="now-state">
					<i class="sta-icon online"></i>在线
				</span>
				<span class="other-state">
					<span class="sta-item">
						<i class="sta-icon online"></i>在线
					</span>
					<span class="sta-item">
						<i class="sta-icon busy"></i>忙碌
					</span>
					<span class="sta-item">
						<i class="sta-icon offline"></i>离线
					</span>
				</span>
			</a> -->
			<a href="${contextPath}/customerService/setting" class="nav-item set">账号设置</a>
		</div>
	</div>
</div>
<!-- /头部 -->
