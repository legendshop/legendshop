<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<script src="${contextPath}/resources/plugins/jqzoom/js/jquery.jqzoom-core.js" type="text/javascript"></script>
<link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/jqzoom/css/jquery.jqzoom.css'/>" />
<script src="<ls:templateResource item='/resources/templets/js/prodpics.js'/>"></script>
<div class="preview tb-gallery">
	<div class="tb-booth">
		    <a id="aView" href="<ls:photo item='${auction.prodPic}'/>" class="jqzoom" rel='gal1'  title="${fn:escapeXml(auction.prodName)}" >
   			<img id="imgView" style='max-width:450px;max-height:450px;' src="<ls:photo item='${auction.prodPic}' />" >
   		</a>
	</div>
	
	<c:if test="${not empty auction.skuPicList}">		
		<div class="imgnav clearfix">
			<div id="spec-list">
			<div class="control back" id="spec-left">&lt;</div>
			<div class="control forward" id="spec-right">&gt;</div>
			<div class="simgbox">
				<ul style="width:950px;overflow: hidden;" class="simglist">
						
						<c:forEach items="${auction.skuPicList}" var="pics">
				         <li onmouseover="viewPic(this);" data-url="${pics}" class="img-default">
				          	<table cellpadding='0' cellspacing='0' border='0'><tr><td  height='65' width='65' style='text-align: center;'>
				          	<img src="<ls:images item='${pics}' scale='3'/>" style="max-width:65px;max-height:65px;" />
				          	</td></tr></table>
				         </li>
				         </c:forEach>
								         
					</ul>
				</div>
			</div>
		</div>
	</c:if>

</div>
<script type="text/javascript">

$(document).ready(function() {
	$('#aView').jqzoom({
            zoomType: 'standard',
            lens:true,
            preloadImages: false,
            alwaysOn:false,
            zoomWidth:450,
            zoomHeight:450
        });
        $('.zoomPad').css('z-index','auto');
        //小图片滚动
        $("#spec-list").infiniteCarousel();
});

//商品小图 切换
function viewPic(obj){
	$(obj).addClass("focus").siblings().removeClass("focus");
	$("#imgView").attr("src","<ls:photo item='"+$(obj).attr("data-url")+"' />");
	$("#aView").attr("href","<ls:photo item='"+$(obj).attr("data-url")+"' />");
	changeImg();
}
//切换放大镜大图
function changeImg() {  
    $("#aView").html($("#imgView"));
    $("#aView").unbind();
    $("#imgView").unbind();
	$("#aView").data("jqzoom",null);
    $("#aView").jqzoom({
        zoomType: 'standard',
        lens:true,
        preloadImages: false,
        alwaysOn:false,
        zoomWidth:450,
        zoomHeight:450
    });   
} 
</script>