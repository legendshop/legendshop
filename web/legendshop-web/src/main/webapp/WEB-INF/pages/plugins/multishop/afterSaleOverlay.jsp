<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<html>
<head>
	<title>售后弹框页面</title>
</head>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>

<link type="text/css" href="<ls:templateResource item='/resources/templets/css/afterSaleOverlay.css'/>" rel="stylesheet"/>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
<body>
	<div id="content">
	    <div class="c-t"></div>
		<div class="title">
		    <h1>售后说明:</h1>
		    <button id="J_NewTemplate" type="button" onclick="newTemplate();">新增说明模板</button>
	    </div>
	    <div id="J_TplBox" class="templates">
	    		<c:forEach items="${requestScope.list}" var="afterSale" varStatus="status">
			    			<div id="J_Tpl_${afterSale.id}" class="J_Tpl template box saved"  afterSaleId ="${afterSale.id}" afterSaleName ="${afterSale.title}">
				                <div class="hd">
				                    <h2>
				    					<input maxlength="20" title="${afterSale.title}" id="J_NameEdit" value="${afterSale.title}" class="name">
				    					<span id="J_NameShow">${afterSale.title}</span>
				    				</h2>
				                </div>
				
				                <div class="bd">
										<div id="J_ConShow" class="result">${afterSale.content}</div>
				                    	<textarea title="填写承诺内容" id="J_ConEdit" class="content J_ConEdit">${afterSale.content}</textarea>
					                    <div class="apply">
					    					<a class="J_Apply" href="javascript:void(0);" onclick="appTemplate(this);">
					    						<span>应用该模板</span>
					    					</a>
					    				</div>
					                    <div class="action">
					    					<button id="J_Cancel" class="J_Cancel" type="button" onclick="cancelEdit(this);">取消</button>
					    					<button class="J_Save" id="J_Save" type="button" onclick="saveEdit(this);">保存</button>
					    				</div>
				                </div>
				
				                <div class="ft">
				                    <span class="notice">卖家承诺所填写的内容，不得与国家相关法律，法规及本商城相关规定有冲突！</span>
				                    <div class="action">
				                        <a class="J_Edit" href="javascript:void(0);" onclick="editTemplate(this);">修改</a> - <a type="submit" class="J_Delete" href="javascript:void(0);" onclick="delTemplate(this);">删除</a>
				                    </div>
				                </div>
			    		</div>
	    		</c:forEach>
	    		
	    </div>
	    <div class="c-b"></div>
	    	<div class="clear"></div>
				 <div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
				            		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
				     			 </div>
		  				</div>  

	</div>
<script src="<ls:templateResource item='/resources/templets/js/afterSaleOverlay.js'/>" type="text/javascript"></script>
<script type="text/javascript">
var contextPath="${contextPath}";	
</script>
</body>

</html>

