<!DOCTYPE html>
	<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
	<%@ include file='/WEB-INF/pages/frontend/templetGoat/common/taglib.jsp' %>
    <%@ include file='/WEB-INF/pages/frontend/templetGoat/common/common.jsp' %>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<title>选择可用礼券</title>
	<link rel="stylesheet" href="<ls:templateResource item='/resources/templets/css/userCoupon.css'/>">
</head>
<body>

<div class="w768">
		<header>
			<div class="back">
				<a href="javascript:history.back();"> 
				  <img src="<ls:templateResource item='/resources/templets/images/back.png'/>" alt=""> 
				</a>
			</div>
			<p>请选择优惠券</p>
		</header>

		
		<if test="${not empty requestScope.couponList}">
		     <c:forEach items="${requestScope.couponList }" var="coupon" >
		     <c:choose>
		         	<c:when test="${useStatus==1}">
								<div class="pepper-con" onclick="chooseCoupon('${coupon.couponId}');">
									<div class="pepper-w">
										<div class="pepper pepper-blue">
											     <c:if test="${coupon.couponType==1}">
												        <div class="pepper-l">
									    					<p class="pepper-l-num">
																<i>¥</i>
																<span><fmt:formatNumber type="currency" value="${coupon.offPrice }" pattern="0"/></span>
																（单笔满<fmt:formatNumber type="currency" value="${coupon.fullPrice }" pattern="0"/>可使用）
															</p>
									    					<p class="pepper-l-con">发行店铺：${coupon.shopName}</p>
									    					<p class="pepper-l-con">兑换码：${coupon.couponSn}</p>
									    				</div>
									    		  </c:if>	
									    		  <c:if test="${coupon.couponType==2}">
									    		         <div class="pepper-l">
									    					<p class="pepper-l-num">
																<i>¥</i>
																<span><fmt:formatNumber type="currency" value="${coupon.offPrice }" pattern="0"/></span>
																（全场可用 &nbsp;&nbsp;运费优惠券）
															</p>
									    					<p class="pepper-l-con">兑换码：${coupon.couponSn}</p>
									    				</div>
									    		  </c:if>
								    		  
								    		
								    				<!-- 卷信息显示 -->
								    				<div class="pepper-r">
								    					<span>有效期</span>
								    					<div>
															<fmt:formatDate value="${coupon.startDate}" pattern="yyyy-MM-dd HH:mm" />
								        					<p class="pepper-line">~</p>
								        					<fmt:formatDate value="${coupon.endDate}" pattern="yyyy-MM-dd HH:mm" />
								    					</div>
								    					 <!--  <i class="right-arrow"></i> -->
								        			</div>
										  </div>
										  <div class="pepper-b pepper-blue">
												<div class="pb-con"></div>  <!-- 一行蓝色 -->
												<div class="pb-border"></div>  <!-- 波浪图 -->
										  </div>	
												    <!-- 蓝色下滑波浪线 -->
									 </div>
								</div>
					  </c:when>
					  
					  <c:when test="${useStatus==2}">
					         <div class="pepper-con" onclick="chooseCoupon('${coupon.couponId}');">
									<div class="pepper-w">
										<div class="pepper pepper-blue">
										    <c:if test="${coupon.couponType==1}">
											        <div class="pepper-l">
								    					<p class="pepper-l-num">
															<i>¥</i>
															<span><fmt:formatNumber type="currency" value="${coupon.offPrice }" pattern="0"/></span>
															（单笔满<fmt:formatNumber type="currency" value="${coupon.fullPrice }" pattern="0"/>可使用）
														</p>
								    					<p class="pepper-l-con">兑换码：${coupon.couponSn}</p>
								    				</div>
								    		</c:if>		
								    		<c:if test="${coupon.couponType==2}">
									    		         <div class="pepper-l">
									    					<p class="pepper-l-num">
																<i>¥</i>
																<span><fmt:formatNumber type="currency" value="${coupon.offPrice }" pattern="0"/></span>
																（全场可用 &nbsp;&nbsp;运费优惠券）
															</p>
									    					<p class="pepper-l-con">兑换码：${coupon.couponSn}</p>
									    				</div>
									    	</c:if>		
								    				<!-- 卷信息显示 -->
								    				<div class="pepper-r">
								    					<span>使用日期</span>
								    					<div>
															<fmt:formatDate value="${coupon.useTime}" pattern="yyyy-MM-dd HH:mm" />
								    					</div>
								        			</div>
								        			<!-- 东券 -->
										  </div>
												<div class="pepper-b pepper-blue">
													<div class="pb-con"></div>  <!-- 一行蓝色 -->
													<div class="pb-border"></div>  <!-- 波浪图 -->
												</div>	
												    <!-- 蓝色下滑波浪线 -->
									 </div>
								</div>
					  </c:when>
					  
					  <c:otherwise>
					        <div class="pepper-con" onclick="chooseCoupon('${coupon.couponId}');">
									<div class="pepper-w">
										<div class="pepper pepper-blue">
								    		<c:if test="${coupon.couponType==1}">
											        <div class="pepper-l">
								    					<p class="pepper-l-num">
															<i>¥</i>
															<span><fmt:formatNumber type="currency" value="${coupon.offPrice }" pattern="0"/></span>
															（单笔满<fmt:formatNumber type="currency" value="${coupon.fullPrice }" pattern="0"/>可使用）
														</p>
								    					<p class="pepper-l-con">兑换码：${coupon.couponSn}</p>
								    				</div>
								    		</c:if>		
								    		<c:if test="${coupon.couponType==2}">
									    		         <div class="pepper-l">
									    					<p class="pepper-l-num">
																<i>¥</i>
																<span><fmt:formatNumber type="currency" value="${coupon.offPrice }" pattern="0"/></span>
																（全场可用 &nbsp;&nbsp;运费优惠券）
															</p>
									    					<p class="pepper-l-con">兑换码：${coupon.couponSn}</p>
									    				</div>
									    	</c:if>		
								    				<!-- 卷信息显示 -->
								    				<div class="pepper-r">
								    					<span>有效期</span>
								    					<div>
															<fmt:formatDate value="${coupon.startDate}" pattern="yyyy-MM-dd HH:mm" />
								        					<p class="pepper-line">~</p>
								        					<fmt:formatDate value="${coupon.endDate}" pattern="yyyy-MM-dd HH:mm" />
								    					</div>
								    					 <!--  <i class="right-arrow"></i> -->
								        			</div>
										  </div>
										  <div class="pepper-b pepper-blue">
												<div class="pb-con"></div>  <!-- 一行蓝色 -->
												<div class="pb-border"></div>  <!-- 波浪图 -->
										  </div>	
												    <!-- 蓝色下滑波浪线 -->
									 </div>
								</div>
					  </c:otherwise>
					  
				</c:choose>	
			</c:forEach>
		</if>
		
		<c:if test="${empty requestScope.couponList}">
		   <div style="text-align: center;">
		                   <strong>暂无数据</strong>
		   </div>
		</c:if>
</div>

 <input id="basketStr" name="basketStr" value="${shopCartItems}" type="hidden"/>
</body>

 <script type="text/javascript">
	var contextPath = '${contextPath}';
	function chooseCoupon(id){
	    if(isBlank(id)){
	       alert("请选择优惠卷");
	       return false;
	    }
	    var shopCartItems = $("#basketStr").val();
	    abstractForm(contextPath+'/p/orderDetails', shopCartItems,id);
	}
		
		
    function abstractForm(URL, shopCartIds,couponId){
		   var temp = document.createElement("form");        
		   temp.action = URL;        
		   temp.method = "post";        
		   temp.style.display = "none";        
		   var opt = document.createElement("textarea");        
		   opt.name = 'shopCartItems';        
		   opt.value = shopCartIds;  
		   var opt1 = document.createElement("input");        
		   opt1.name = 'couponId';        
		   opt1.value = couponId;      
		   temp.appendChild(opt);    
		   temp.appendChild(opt1);        
		   document.body.appendChild(temp);        
		   temp.submit();        
		   return temp;  
	}
		
		/**判断是否为空**/
	function isBlank(_value){
		if(_value==null || _value=="" || _value==undefined){
			return true;
		}
		return false;
	}
 </script>

</html>