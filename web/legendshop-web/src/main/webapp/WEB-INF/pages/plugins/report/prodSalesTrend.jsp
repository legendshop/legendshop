<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>商品分析-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-prodAnalysis${_style_}.css'/>" rel="stylesheet">
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
	<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
<div id="Content" class="w1190">
	<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">商品分析</span>
				</p>
			</div>
		<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
    <div class="seller-prodAnalysis" >
    	<div class="seller-com-nav">
			<ul>
				<li class="on"><a href="javascript:void(0);">商品详情</a></li>
				<li><a href="${contextPath}/s/shopReport/prodAnalysis">热卖商品</a></li>
			</ul>
		</div>
	    <div class="alert" style="margin-bottom:0;">
			<ul class="mt5">
				<li>1、符合以下任何一种条件的订单即为有效订单：1）采用在线支付方式支付并且已付款；</li>
		        <li>2、以下关于订单和订单商品近30天统计数据的依据为：从昨天开始最近30天的有效订单</li>
		    </ul>
		</div>
		<form:form action="${contextPath}/s/shopReport/prodSalesTrend" method="get" id="from1">
			<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
			<div class="sold-ser sold-ser-no-bg clearfix">
				<div class="fr">
					<div class="item">
						商品名称：
						<input type="text" class="item-inp" value="${prodName}" style="width:300px;" name="prodName"  id ="prodName">
						<input type="button" onclick="search()" class="btn-r" id="btn_keyword" value="搜  索" >	
					</div>
				</div>
			</div>
		</form:form>
    	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="prodAnalysis-table sold-table" style="margin-top: 0;">
               <tr class="prodAnalysis-tit">
               	<th>商品</th>
                 <th width="430">商品名称</th>
                 <th>当前价格</th>
                 <th>近30天下单商品数</th>
                 <th>近30天下单金额</th>
                 <th width="80">操作</th>
               </tr>
               <c:choose>
		           <c:when test="${empty requestScope.list}">
		              <tr>
					 	 <td colspan="20">
					 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
					 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
					 	 </td>
					 </tr>
		           </c:when>
		           <c:otherwise>
	                 <c:forEach items="${requestScope.list}" var="prod" varStatus="index">
		                <tr class="detail">
		                <td><a href="${contextPath}/views/${prod.prodId}" target="_blank"><img src="<ls:images item='${prod.prodPic}' scale='3'/>" /></a></td>
		                  <td><a href="${contextPath}/views/${prod.prodId}" target="_blank">${prod.prodName}</a></td>
		                <td><fmt:formatNumber value="${prod.prodCash}" type="currency" pattern="0.00" /></td>
		              	<td>${prod.buys}</td>
		              	<td><fmt:formatNumber value="${prod.totalCash}" type="currency" pattern="0.00" /></td>
		              	<td><a onclick="viewTrend('${prod.prodId}',this)" href="javascript:void(0);" data="${prod.prodName}">查看走势图</a></td>
		               </tr>
		           	 </c:forEach>
		           </c:otherwise>
		        </c:choose>
    	</table>
    
		<div id="main" name="main" style="margin-left: 10px;width:98%;height:400px;margin-top: 20px;"></div>
		<div style="margin-top:10px;" class="page clearfix">
  			 <div class="p-wrap">
				<span class="p-num">
					<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
				</span>
  			 </div>
		</div>
	</div>
</div>
</div>
<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
<script src="<ls:templateResource item='/resources/plugins/ECharts/dist/echarts.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/templets/js/multishop/prodSalesTrend.js'/>" type="text/javascript"></script>
<script type="text/javascript">
	function search(){
	  	$("#from1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	  	$("#from1")[0].submit();
	}
</script>
</body>
</html>
