<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>品牌申请-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-brand.css'/>" rel="stylesheet">
	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/templets/css/errorform.css" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">品牌申请</span>
				</p>
			</div>
		<%@ include file="included/sellerLeft.jsp" %>
			<div class="seller-brand">
				<div class="seller-com-nav" >
					<ul>
						<li ><a href="javascript:void(0);" onclick="loadbrandList();">品牌列表</a></li>
						<li class="on"><a id="brandConfigure" href="javascript:void(0);">品牌申请</a></li>
					</ul>
				</div>
				 <form:form action="${contextPath}/s/applyBrand/save" method="post" id="form1" enctype="multipart/form-data">
						 <input id="brandId" name="brandId" value="${bean.brandId}" type="hidden">
          				  <input id="shopId" name="shopId" value="${bean.shopId}" type="hidden">
					     <input id="commend" name="commend" value="${bean.commend}" type="hidden">
					 	 <input id="applyTime" name="applyTime" value='<fmt:formatDate value="${bean.applyTime}" pattern="yyyy-MM-dd HH:mm:ss" />' type="hidden">
          				  <c:if test="${not empty bean.checkOpinion}">
					        <p style="text-align: center;"><strong>审核失败原因：</strong><font style="color: red;">${bean.checkOpinion}</font></p>
		      			  </c:if>
						<div class="brand-add">
							<ul>
								<li><font style="color: red;font-size: 14px;">*</font>品牌名称 <input name="brandName" id="brandName" type="text" value="${bean.brandName}" maxlength="30"></li>
								<li class="file"><font style="color: red;font-size: 14px;">*</font>品牌LOGO（PC）<br><span style="color:#999999;">(建议尺寸：138*46)&nbsp;</span><input name="file" id="file" type="file"/>
									  <c:if test="${not empty bean.brandPic}">
		                				<img src="<ls:photo item='${bean.brandPic}'/>" style="max-height: 48px;max-width: 138px"/>
		                			  </c:if>
								</li>
								<li class="file"><font style="color: red;font-size: 14px;">*</font>品牌LOGO（手机）<br><span style="color:#999999;">(建议尺寸：110*110)&nbsp;</span><input name="mobileLogoFile" id="mobileLogoFile" type="file"/>
									  <c:if test="${not empty bean.brandPicMobile}">
		                				<img src="<ls:photo item='${bean.brandPicMobile}'/>" style="max-height: 110px;max-width: 110px"/>
		                			  </c:if>
								</li>
								<li class="file">品牌大图 <br><span style="color:#999999;">(建议尺寸：400*200)&nbsp;</span><input id="bigImagefile" name="bigImagefile" type="file"/>
								      <c:if test="${not empty bean.bigImage}">
		                				<img src="<ls:photo item='${bean.bigImage}'/>" style="max-height: 200px;max-width: 400px"/>
		                			  </c:if>
								</li>
								<li class="file"><font style="color: red;font-size: 14px;">*</font>品牌产品授权书原件扫描件 <input name="brandAuthorizefile"  id="brandAuthorizefile" type="file">
									  <c:if test="${not empty bean.brandAuthorize}">
		                				<img src="<ls:photo item='${bean.brandAuthorize}'/>" style="max-height: 500px;max-width: 150px"/>
		                  			  </c:if>
								</li>
								<li class="textarea">
									<font style="color: red;font-size: 14px;">*</font>品牌介绍 <textarea name="brief" id="brief">${bean.brief}</textarea>
								</li>
							</ul>
							<div class="bot-save"><input type="submit" value="提交申请" class="seller_save"/></div>
							<ul class="annotation">
								<li class="ann-det"><span>注：</span>申请品牌填完此申请表后还需要将以下证件寄往本公司审核通过方可申请成功</li>
								<li>1、营业执照复印件</li>
								<li>2、组织机构代码复印件</li>
								<li>3、品牌授权书复印件</li>
								<li>4、品牌申请表（加盖公章）</li>
								<li>5、带<font style="color: red;font-size: 14px;">*</font>必填</li>
							</ul>
					</div>
				</form:form>
			</div>
		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script src="${contextPath}/resources/common/js/jquery.validate1.5.5.js" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/applyBrandConfigure.js'/>"></script>
	
	<script type="text/javascript">
		var contextPath="${contextPath}";
		var brandPic="${bean.brandPic}";
		var brandPicMobile="${bean.brandPicMobile}";
		var bigImage="${bean.bigImage}";
		var brandAuthorize="${bean.brandAuthorize}";
		
	</script>
</body>
</html>