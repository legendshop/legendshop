<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>秒杀记录-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
</head>
<style>
.sold-table th {
    border-bottom:1px solid #e4e4e4;
}
</style>
<body class="graybody">
<div id="doc">
   
   <div id="hd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
   </div><!--hd end-->
   
   
   <div id="bd">
            
	 <div class=" yt-wrap" style="padding-top:10px;"> 
    	  <%@ include file="/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp" %>
     <div class="right_con">
     
         <div class="o-mt"><h2>我参与的秒杀</h2></div>                
         
         <div class="pagetab2">
                <ul>
                    <li class="on"><span>秒杀列表</span></li>
                </ul>
         </div>
         
        <!--订单-->
         <div id="recommend" class="m10 recommend bidding-his" style="display: block;">
                 
                <table width="100%" cellspacing="0" cellpadding="0" class="buytable sold-table" style="text-align:center;">
                    <tr>
                    	<th width="20%">图片</th>
                        <th width="30%">秒杀活动名称</th>
                        <th width="10%">秒杀时间</th>
                        <th width="10%">秒杀价</th>
                        <th width="10%">状态</th>
                        <th width="16%" style="border-right: 1px solid #e4e4e4;">操作</th>
                    </tr>
                    <c:choose>
                    	<c:when test="${empty list}">
                    	    <tr>
					 	 		<td colspan="20">
					 	 			<!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
					 	 			<div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
					 	 		</td>
					 	 	</tr>
                    	</c:when>
                    	<c:otherwise>
                  		    <c:forEach items="${list}" var ="list" varStatus="status">
	                          <tr>
	                              <td>
	                                <a target="_blank" href="<ls:url address='/seckill/views/${list.seckillId}'/>">
	                                  <img src="<ls:images item='${list.seckillPic}' scale='3'/>" alt="" >
	                                </a>
	                              </td>
	                              <td style="width:20%;text-align:center;" class="a-left">
	                                <a target="_blank" href="<ls:url address='/seckill/views/${list.seckillId}'/>">
	                                   ${list.seckillTitle}
	                                </a>
	                              </td>
	                              <td><fmt:formatDate value="${list.seckillTime}"  pattern="yyyy-MM-dd HH:mm:ss"/></td>
	                              <td>¥${list.seckillPrice}</td>
	                              <td>
									<c:choose>
										<c:when test="${list.status eq 0}">等待下单</c:when>
										<c:when test="${list.status eq 1}">已下单</c:when>
										<c:when test="${list.status eq -1}">已取消</c:when>
									</c:choose>	
							 	 </td>
								 <td>
								 	<c:choose>
										<c:when test="${list.status eq 0}">
											<input type="button" value="提交订单" class="btn-r" onclick="window.location.href='${contextPath}/p/seckill/order/${list.seckillId}/${list.secretKey}/${list.prodId}/${list.skuId}/success'" >
										</c:when>
										<c:when test="${list.status eq 1}">
											<input type="button" value="我的订单" class="btn-r" onclick="window.location.href='${contextPath}/p/myorder'" >
										</c:when>
										<c:when test="${list.status eq -1}">
											<input type="button" value="删除" class="btn-r" onclick="deleteRecord(${list.id})" >
										</c:when>
									</c:choose>
								 </td>
	                          </tr>
	                        </c:forEach>
                    	</c:otherwise>
                    </c:choose>
                </table>
				              		
	     </div>
     </div>
    <form:form action="${contextPath}/p/seckill/record" id="form1" method="post">
		<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
	</form:form>
    <div class="clear"></div>
	     <div style="margin-top:10px;" class="page clearfix">
   			 <div class="p-wrap">
          		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
   			 </div>
		</div>
 	</div>
           
   </div>
 <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
</div>
<script type="text/javascript">
	userCenter.changeSubTab("mySeckill");
	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}
	
	function deleteRecord(id){
		layer.confirm('是否确定删除该记录？',{icon:3,btn: ['确定','关闭']}, function(){
			window.location.href="${contextPath}/p/seckill/record/deleteRecord/"+id;
		});
	}
</script>
</body>
</html>
