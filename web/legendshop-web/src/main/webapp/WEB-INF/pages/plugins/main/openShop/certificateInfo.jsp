<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
  <title>证书信息-${systemConfig.shopName}</title>
  <meta name="keywords" content="${systemConfig.keywords}"/>
  <meta name="description" content="${systemConfig.description}" />
  <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/templets/css/errorform.css'/>" />
</head>

<body>
    <div id="doc">
       
       <div id="hd">
       		<%@ include file="../home/top.jsp" %>
          
       </div>
       <div id="bd">
          
         
           <div class="yt-wrap" style="padding:0px 0 20px;">
                <div class="seller-cru" style="width:1190px;margin: 20px auto 20px;">
                  <p>
                    您的位置&gt;
                    <a href="#">首页</a>&gt;
                    <span style="color: #e5004f;">免费开店</span>
                  </p>
                </div>
                <div class="pagetab2">
                   <ul>           
                     <li><span>签订入驻协议</span></li>
                     <li class="on"><span>公司信息提交</span></li>
                     <li><span>店铺信息提交</span></li>   
                     <li><span>审核进度查询</span></li>   
                   </ul>       
                </div>
                <div class="main">
                    <div class="settled_box">
                      <h3>
                        <div class="settled_step">
                          <ul>
                            <li class="step5"><a href="#">入驻须知</a></li>
                            <li class="step7"><a href="#">公司信息认证</a></li>
                            <li class="step3"><a href="#">店铺信息认证</a></li>
                            <li class="step4"><a href="#">等待审核</a></li>
                          </ul>
                        </div>
                        <span class="settled_title">公司信息提交</span></h3>
                      <div class="settled_white">
                         <form:form  action="${contextPath}/p/saveCertificateInfo" method="post" id="cretificateInfo"  novalidate="novalidate"  enctype="multipart/form-data">
                            
                            <div class="settled_white_box" >
                              <div class="settled_warning">
                                <span>以下所需要上传电子版资质仅支持JPG、GIF、PNG格式的图片，大小不超过1M，且必须加盖企业彩色公章</span>
                              </div>
                              <div class="settled_form" style="height:60px;">
                                <h4>商事制度</h4>
                                <input type="hidden" value="${shopCompanyDetail.codeType}" name="codeType" id="codeType">
                                  <div style="float:left;">
                                    <label style="float:left;"><input style="margin:2px 5px 0 45px; float:left;" value="1" type="radio" name="code" id="code1">旧版三证 <em></em></label>
                                    <label style="float:left; margin-left:10px;"><input style="margin:2px 5px 0 45px; float:left;" value="2" type="radio" name="code" id="code2">一证一码  <em></em></label>
                                  </div>
                              </div>
                              <div>
                              <div class="settled_form" id="old">
                                <h4>税务登记证<b></b></h4>
                                <table class="settled_table" border="0" cellpadding="0" cellspacing="0" width="630" style="font-size: 12px;margin-top: 30px;">
                                  <tr>
                                    <td align="right" valign="top" width="150"><span class="sred_span">
                               			       纳税人识别号：</span><strong class="sred">*</strong>
                                    </td>
                                    <td>
                                      <input class="size200" type="text" id="taxpayerId" name="taxpayerId" maxlength="50" value="${shopCompanyDetail.taxpayerId}"><em style="margin-left: 15px;"></em>
                                    </td>
                                  </tr>
                                   <tr>
                                    <td align="right" valign="top" width="150"><span class="sred_span">
                               			      税务登记证号：</span><strong class="sred">*</strong>
                                    </td>
                                    <td>
                                      <input class="size200" type="text" id="taxRegistrationNum" name="taxRegistrationNum" maxlength="50" value="${shopCompanyDetail.taxRegistrationNum}"><em style="margin-left: 15px;"></em>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="right" valign="top" width="150">
                                      <span class="sred_span">税务登记证电子版：</span><strong class="sred">*</strong>
                                    </td>
                                    <td>
                                        <span class="upload_button"><input type="file" oldFile="${shopCompanyDetail.taxRegistrationNumE}" id="taxRegistrationNumEPic" name="taxRegistrationNumEPic"> <em style="margin-left: 23px;"></em> </span>
                                    </td>
                                  </tr>
                                   <c:if test="${not empty shopCompanyDetail.taxRegistrationNumE}">
				                        <tr>
				                        	<td></td>
				                        	<td>
				                        		<a target="_blank" href="<ls:photo item='${shopCompanyDetail.taxRegistrationNumE}' />"><img width="200" height="120" src="<ls:photo item='${shopCompanyDetail.taxRegistrationNumE}' />"></a>
				                        	</td>
				                        </tr>
                       			 </c:if>
                              </table>
                             <div style="float: right;width: 390px;">
                              <span class="license">
                                  <b><a href="<ls:templateResource item='/resources/templets/images/3.jpg'/>" target="_blank"><em><img src="<ls:templateResource item='/resources/templets/images/3.jpg'/>" width="250" height="170"></em>查看大图</a></b>
                                </span>
                            </div> 
                            </div>
                             <div class="settled_form" id="new" style="float:left; display: none" >
                        <h4>统一社会信用代码<b></b></h4>
                                <table class="settled_table" border="0" cellpadding="0" cellspacing="0" width="630" style="font-size: 12px;">
                                  <tr>
                                    <td align="right" valign="top" width="150"><span class="sred_span">
                               			       统一社会信用代码号：</span><strong class="sred">*</strong>
                                    </td>
                                    <td>
                                      <input class="size200" type="text" id="socialCreditCode" name="socialCreditCode" maxlength="50" value="${shopCompanyDetail.socialCreditCode}"><em style="margin-left: 15px;"></em>
                                    </td>
                                  </tr>
                          </table>
                        </div>
                        </div>
                        <div class="settled_form">
                                <h4>开户银行许可证<b></b></h4>
                                <table class="settled_table" border="0" cellpadding="0" cellspacing="0" width="630" style="font-size: 12px;">
                                  <tbody><tr>
                                    <td align="right" valign="top" width="150"><span class="sred_span">银行开户名：</span><strong class="sred">*</strong></td>
                                    <td><input class="size200" type="text" name="bankAccountName" id="bankAccountName" value="${shopCompanyDetail.bankAccountName}"><em style="margin-left: 25px;"></em></td>
                                  </tr>
                                  <tr>
                                    <td align="right" valign="top" width="150"><span class="sred_span">公司银行账号：</span><strong class="sred">*</strong></td>
                                    <td><input class="size200" type="text" name="bankCard" id="bankCard" value="${shopCompanyDetail.bankCard}"><em style="margin-left: 25px;"></em></td>
                                  </tr>
                                  <tr>
                                    <td align="right" valign="top" width="150">
                                      <span class="sred_span">开户银行支行名称：</span><strong class="sred">*</strong>
                                    </td>
                                    <td>
                                      <input class="size200" type="text" name="bankName" id="bankName" value="${shopCompanyDetail.bankName}"><em style="margin-left: 25px;"></em></td>
                                  </tr>
                                  <tr>
                                    <td align="right" valign="top" width="150">
                                      <span class="sred_span">开户行支行联行号：</span><strong class="sred">*</strong>
                                    </td>
                                    <td><input class="size200" type="text" name="bankLineNum" id="bankLineNum" value="${shopCompanyDetail.bankLineNum}"><em style="margin-left: 25px;"></em></td>
                                  </tr>
                                  <tr>
                                    <td align="right" valign="top" width="150">
                                      <span class="sred_span">开户银行支行所在地：</span><strong class="sred">*</strong>
                                    </td>
                                    <td>
                                    <select class="combox sele" id="bankProvinceid" name="bankProvinceid" requiredTitle="true" childNode="bankCityid" selectedValue="${shopCompanyDetail.bankProvinceid}"
									retUrl="${contextPath}/common/loadProvinces">
								</select> <select class="combox sele" id="bankCityid" name="bankCityid" requiredTitle="true" selectedValue="${shopCompanyDetail.bankCityid}" showNone="false" parentValue="${shopCompanyDetail.bankProvinceid}"
									childNode="bankAreaid" retUrl="${contextPath}/common/loadCities/{value}">
								</select> <select class="combox sele" id="bankAreaid" name="bankAreaid" requiredTitle="true" selectedValue="${shopCompanyDetail.bankAreaid}" showNone="false" parentValue="${shopCompanyDetail.bankCityid}"
									retUrl="${contextPath}/common/loadAreas/{value}">
								</select>
								<em style="margin-left: 17px;"></em>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="right" valign="top" width="160">
                                      <span class="sred_span">银行开户许可证电子版：</span><strong class="sred">*</strong>
                                    </td>
                                    <td>
                                      <span class="upload_button"><input type="file" oldFile="${shopCompanyDetail.bankPermitImg}" id="bankPermitPic" name="bankPermitPic"><em style="margin-left: 33px;"></em></span>
                                    </td>
                                  </tr>
                                   <c:if test="${not empty shopCompanyDetail.bankPermitImg}">
				                        <tr>
				                        	<td></td>
				                        	<td>
				                        		<a target="_blank" href="<ls:photo item='${shopCompanyDetail.bankPermitImg}' />"><img width="200" height="120" src="<ls:photo item='${shopCompanyDetail.bankPermitImg}' />"></a>
				                        	</td>
				                        </tr>
                       			 </c:if>
                                </tbody></table>
                                  <div style="float: right;width: 390px;">
	                              <span class="license">
	                                  <b><a href="<ls:templateResource item='/resources/templets/images/4.jpg'/>" target="_blank"><em><img src="<ls:templateResource item='/resources/templets/images/4.jpg'/>" width="250" height="170"></em>查看大图</a></b>
	                                </span>
                            </div>
                              </div>
                        </div> 
                           </form:form>
                        
                            <div class="settled_bottom">
                              <span>
                                <a href="javaScript:void(0)" onclick="shopPrevious(2)" class="up_step_btn">上一步</a>
                                <a href="javaScript:void(0)" onclick="saveCretificateInfo()" class="settled_btn">
                                <em>下一步，完善店铺信息</em></a>
                              </span>
                            </div>
                        </div>
                      </div>
                  </div>
            </div>
       </div>
		 <%@ include file="../home/bottom.jsp" %>
    </div>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>"  type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/openshop.js'/>"></script>
<script type="text/javascript">
	$("select.combox").initSelect();
	$(document).ready(function(){
		var type=$("#codeType").val();
			if(type==1){
				$("#old").show(500);
				$("#new").hide(500);
				$("#code1").attr("checked","checked");
				$("#socialCreditCode").addClass("ignore");
				$("#taxpayerId").removeClass("ignore");
				$("#taxRegistrationNum").removeClass("ignore");
				$("#taxRegistrationNumEPic").removeClass("ignore");
				$("#socialCreditCode").val("");
				
			}
			if(type==2){
				$("#new").show(500);
				$("#old").hide(500);
				$("#code2").attr("checked","checked");
				$("#taxpayerId").addClass("ignore");
				$("#taxRegistrationNum").addClass("ignore");
				$("#taxRegistrationNumEPic").addClass("ignore");
				$("#socialCreditCode").removeClass("ignore");
				$("#taxpayerId").val("");
				$("#taxRegistrationNum").val();
				$("#taxRegistrationNumEPic").attr("oldFile","");
			}
	});
</script>
</body>
</html>
