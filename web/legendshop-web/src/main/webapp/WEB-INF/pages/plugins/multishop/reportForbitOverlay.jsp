<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/reportForbitOverlay.css'/>" rel="stylesheet"/>
<div class="detailsjb"> 
            <table width="100%" cellspacing="0" cellpadding="0" border="0" class="report_table" style="font-size: 13px; font-family: 微软雅黑;">
               <tbody>
               		 <c:forEach items="${requestScope.list}" var="accusation" varStatus="status">
                  <tr>
                    <td width="114" valign="top" align="right">举报商品：</td>
	                  	<td valign="top">
	                  	<a href=" ${contextPath}/views/${accusation.prodId}"  target="_blank" style="text-decoration: none;">${accusation.prodName}</a>
	                  	</td>
                	</tr>
				   <tr>
                    <td width="114" valign="top" align="right">举报类型：</td>
                    <td valign="top">${accusation.accuType}</td>
                  </tr>
                  <tr>
                    <td valign="top" align="right">举报主题：</td>
                    <td valign="top">${accusation.title}</td>
                  </tr>
                  <tr>
                    <td valign="top" align="right">举报时间：</td>
                    <td valign="top"><fmt:formatDate value="${accusation.recDate}" type="date" /></td>
                  </tr>
                  <tr>
                    <td valign="top" align="right">举报内容：</td>
                    <td valign="top"> ${accusation.content}</td>
                  </tr>
                  <tr>
                    <td valign="top" align="right">举报状态：</td>
                    <td valign="top">
                    	<c:choose>
     						<c:when test="${accusation.status ==1}">已处理</c:when>
     						<c:otherwise>未处理</c:otherwise>
     					</c:choose>
                    </td>
                  </tr>
                  <tr>
                    <td valign="top" align="right">用户是否删除：</td>
                    <td valign="top">
                    	<c:if test="${accusation.userDelStatus == 1}">是</c:if>
                    	<c:if test="${accusation.userDelStatus == 0}">否</c:if>
                    </td>
                  </tr>
                  <tr>
                    <td valign="top" align="right">处理结果：</td>
                     <td valign="top">
                     	<c:choose>
			     			<c:when test="${accusation.result ==1}">无效举报</c:when>
			     			<c:when test="${accusation.result ==2}">有效举报</c:when>
			     			<c:when test="${accusation.result ==3}">恶意举报</c:when>
			     			<c:otherwise></c:otherwise>
			     		</c:choose>
                     </td>
                  </tr>
                  <c:if test="${accusation.pic1 !=null}">
	                  <tr>
	                    <td valign="top" align="right">取证图片1：</td>
	                    <td valign="top">
	                                          <a href="<ls:photo item='${accusation.pic1}'/>" target="_blank"> <img src="<ls:images item='${accusation.pic1}' scale="2"/>"  /> </a>
	                    </td>
	                  </tr>
                  </c:if>
                  <c:if test="${accusation.pic2 !=null}">
	                  <tr>
	                    <td valign="top" align="right">取证图片2：</td>
	                    <td valign="top">
	                      					 <a href="<ls:photo item='${accusation.pic2}'/>" target="_blank"> <img src="<ls:images item='${accusation.pic2}' scale="2"/>"  /> </a>
	                      </td>
	                  </tr>
                  </c:if>
                  <c:if test="${accusation.pic3 !=null}">
	                  <tr>
	                    <td valign="top" align="right">取证图片3：</td>
	                    <td valign="top">
	                                          <a href="<ls:photo item='${accusation.pic3}'/>" target="_blank"> <img src="<ls:images item='${accusation.pic3}' scale="2"/>"  /></a>
	                      </td>
	                  </tr>
                  </c:if>
                  </c:forEach>
            </tbody></table>
          </div>