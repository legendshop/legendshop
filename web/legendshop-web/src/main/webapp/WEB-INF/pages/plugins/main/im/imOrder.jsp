<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>

<!-- 我的订单 -->
<div class="my-order">
	<c:if test="${empty imOrderList}">
		<div class="no-record">
			<i class="rec-i"></i>
			<p class="rec-p">暂无数据</p>
		</div>
	</c:if>
	
	<c:forEach items="${imOrderList}" var="item">
		<div class="ord-item">
			<div class="ord-num">
				<span class="num-span">订单号：</span>
				<span class="num">${item.subNumber }</span>
				<a href="javascript:void(0)" class="btn sendOrder" data-number="${item.subNumber }" data-price='${item.actualTotal}' data-date='<fmt:formatDate value="${item.subDate }" pattern="yyyy-MM-dd HH:mm" type="date"/>' data-name="${item.dtoList[0].skuName }">
					发送
				</a>
			</div>
			<div class="ord-info clear">
				<c:choose>
					<c:when test="${fn:length(item.dtoList)==1}">
						<a href="${contextPath }/views/${item.dtoList[0].prodId}" class="pro-img" target="_blank" data-prodId="${item.dtoList[0].prodId}">
							<img src="<ls:photo item="${item.dtoList[0].pic}"/>">
						</a>
						<a href="${contextPath }/views/${item.dtoList[0].prodId}" class="name" target="_blank">
							${item.dtoList[0].skuName }
						</a>
					</c:when>
					<c:otherwise>
						<c:forEach items="${item.dtoList}" var="subItem">
							<a href="${contextPath }/views/${subItem.prodId}" class="pro-img" target="_blank" data-prodId="${subItem.prodId}">
								<img src="<ls:photo item="${subItem.pic}"/>">
							</a>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</div>
			<div class="ord-han">
				<span class="han-left">订单金额：<span class="pir"><em class="font-family:Arial;">&yen;</em>${item.actualTotal}</span></span>
				<span class="han-right">
					 <fmt:formatDate value="${item.subDate }" pattern="yyyy-MM-dd HH:mm" type="date"/>  
				</span>
			</div>
		</div>
	</c:forEach>
</div>
<!-- /我的订单 -->
