<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>申请订单项退款/退货-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
  </head>
<body class="graybody">
	   <div id="bd">
	   		 <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
			 <div class=" yt-wrap" style="padding-top:10px;"> 
       	 		<%@include file='../usercenterLeft.jsp'%>
		
			    <!-----------right_con-------------->
			     <div class="right_con">
			     	
			         <div class="o-mt"><h2>申请退款</h2></div>
			         <!-- 操作类型选择 -->
			         <div class="pagetab2">
			                 <ul id="applyTypeSelect">           
				                   <li class="on" value="2"><span>退货退款</span></li>
				                   <li value="1"><span>仅退款</span></li>
			                 </ul>
			         </div>
			
					<!-- 操作提示 -->
			         <div class="alert">
			            <h4>操作提示：</h4>
			            <ul>
			              <li>1. 若您未收到货，或已收到货且与商家达成一致不退货仅退款时，请选择<em>“仅退款”</em>选项。</li>
			              <li>2. 若为商品问题，或者不想要了且与商家达成一致退货，请选择<em>“退货退款”</em>选项，退货后请保留物流底单。</li>
			              <li>3. 若提出申请后，商家拒绝退款或退货，可再次提交申请或选择<em>“商品投诉”</em>，请求商城客服人员介入。</li>
						  <li>4. 成功完成退款/退货；经过商城审核后，退款金额原路退回。</li>
			            </ul>
			        </div>
			        
			        <!-- 申请退款块 -->
			        <div id="refundBox" style="display:none;">
				        <!-- 申请进度 -->
				        <div class="ret-step">
				          <ul>
				            <li class="done"><span>买家申请退款</span></li>
				            <li><span>商家处理退款申请</span></li>
				            <li><span>平台审核，退款完成</span></li>
				          </ul>
				        </div>
			        
				        <!-- 申请"仅退款"表单 -->
				        <form:form id="refundForm" action="${contextPath }/p/itemRefund/save" method="POST" enctype="multipart/form-data">
				        	<input type="hidden" id="applyType" name="applyType" value="${applyType}"/>
				        	<input type="hidden" id="orderId" name="orderId" value="${refund.orderId}"/>
				        	<input type="hidden" id="orderItemId" name="orderItemId" value="${refund.orderItemId}"/>
					        <div class="write-apply">
					          <div class="ste-tit"><b>请填写退款申请</b></div>
					          <div class="ste-con">
					            <ul id="formContent">
					              <li>
					              	<span><em>*</em>退款原因：</span>
					              	<select id="buyerMessage" name="buyerMessage">
					              		<option value="">请选择退款原因</option>
					              		<option value="不能按时发货">不能按时发货</option>
					              		<option value="不想要了">不想要了</option>
					              		<option value="信息填写错误，重新拍">信息填写错误，重新拍</option>
					              	</select>
					              </li>
					              <c:choose>
						              	<c:when test="${refund.subType eq 'PRE_SELL' and refund.payPctType != 0}"> <!-- 预售订单定金支付才有定金，全额支付没有定金 -->
						              		<li>
								              	<input type="hidden" id="depositRefundAmount" name="depositRefundAmount" value="${refund.depositRefundAmount }" />
								              	<span><em>*</em>定金金额：</span>
								              	<div style="line-height: 30px;">
								              		<div style="line-height: 30px; display: inline-block;"><em class="amountCount">${refund.depositRefundAmount}</em>元</i></div>
									              	<label class="checkbox-wrapper checkbox-wrapper-checked" style="margin-left: 10px; vertical-align: top;">
														<div class="checkbox-item">
															<input type="checkbox" class="checkbox-input" id="isRefundDeposit" value="true" name="isRefundDeposit" checked="checked">
															<em class="checkbox-inner"></em>
														</div>
														是否退定金
													</label>
												</div>
								              </li>
						              	</c:when>
						              	<c:otherwise>
						              		<input type="hidden" id="depositRefundAmount" name="depositRefundAmount" value="0" />
						              		<input type="hidden" id="isRefundDeposit" name="isRefundDeposit" value="false" />
						              	</c:otherwise>
						              </c:choose>
					              <li>
					              	<span><em>*</em>退款金额：</span>
					              	<input type="text" onblur="che()" id="refundAmount" name="refundAmount" value="${refund.refundAmount }" /><b>元</b><i>（本次最多退款<em>${refund.refundAmount }</em>元）</i>
					              </li>
					              <li>
					              	<span><em>*</em>退款说明：</span>
					              	<textarea id="reasonInfo" name="reasonInfo" rows="3" cols="80"></textarea>
					              </li>
					              <li id="fileLi1">
					              	<span style="line-height: 20px;">上传凭证1：</span>
					              	<input type="file" id="photoFile1" name="photoFile1" class="photoFile" onchange="checkImgFile(this);"/>
					              	<i>（图片不能超过5MB）</i>
					              	<input id='addBtn' type="button" value="添加" onclick="addFile(this);"/>
					              </li>
					            </ul>
					          </div>
					        </div>
					         
					         <div class="ste-but">
					           <input type="submit" value="确认提交" class="re" />
					           <input type="button" value="取消并返回" class="gr" onclick="window.location.href='${contextPath}/p/myorder'"/>
					         </div>
				         </form:form>
				     </div>
				     <!-- 申请退款块 结束 -->
				     
			         <!-- 退款及退货块 -->
			         <div id="refundReturnBox">
			         	<!-- 申请进度 -->
			         	<div class="ret-step-com">
				          <ul>
				            <li class="done"><span>买家申请退货</span></li>
				            <li><span>商家处理退货申请</span></li>
				            <li><span>买家退货给商家</span></li>
				            <li><span>确认收货，平台审核</span></li>
				          </ul>
				        </div>
				         <form:form id="refundReturnForm" action="${contextPath }/p/itemReturn/save" method="POST" enctype="multipart/form-data">
				         		<input type="hidden" id="applyType" name="applyType" value="2"/>
				         		<input type="hidden" id="orderId" name="orderId" value="${refund.orderId}"/>
				         		<input type="hidden" id="orderItemId" name="orderItemId" value="${refund.orderItemId}"/>
						        <div class="write-apply">
						          <div class="ste-tit"><b>请填写退货退款申请</b></div>
						          <div class="ste-con">
						            <ul id="formContent" style="padding-left:20px;">
						              <li>
						              	<span><em>*</em>退货原因：</span>
						              	<select id="buyerMessage" name="buyerMessage">
						              		<option value="">请选择退货原因</option>
						              		<option value="质量差，产品有问题">质量差，产品有问题</option>
						              		<option value="不想要了">不想要了</option>
						              		<option value="已过期，是假货">已过期，是假货</option>
						              	</select>
						              </li>
						              <c:choose>
						              	<c:when test="${refund.subType eq 'PRE_SELL' and refund.payPctType != 0}"> <!-- 预售订单定金支付才有定金，全额支付没有定金 -->
						              		<li>
								              	<input type="hidden" id="depositRefundAmount" name="depositRefundAmount" value="${refund.depositRefundAmount }" />
								              	<span><em>*</em>定金金额：</span>
								              	<div style="line-height: 30px;">
								              		<div style="line-height: 30px; display: inline-block;"><em class="amountCount">${refund.depositRefundAmount }</em>元</i></div>
									              	<label class="checkbox-wrapper checkbox-wrapper-checked" style="margin-left: 10px; vertical-align: top;">
														<div class="checkbox-item">
															<input type="checkbox" class="checkbox-input" id="isRefundDeposit" value="true" name="isRefundDeposit" checked="checked">
															<em class="checkbox-inner"></em>
														</div>
														是否退定金
													</label>
												</div>
								              </li>
						              	</c:when>
						              	<c:otherwise>
						              		<input type="hidden" id="depositRefundAmount" name="depositRefundAmount" value="0" />
						              		<input type="hidden" id="isRefundDeposit" name="isRefundDeposit" value="false" />
						              	</c:otherwise>
						              </c:choose>
						              <li>
						              	<span><em>*</em>退款金额：</span>
						              	<input type="text" onblur="check()" id="refundAmount" name="refundAmount" value="<fmt:formatNumber value="${refund.refundAmount}" type="currency" pattern="0.00"/>" /><b>元</b><i>（本次最多退款<em class="amountCount">${refund.refundAmount }</em>元）</i>
						              </li>
						              <li>
						              	<span><em>*</em>退货数量：</span>
						              	<input type="text" onblur="checkGoodsNum()" id="goodsNum" name="goodsNum" value="${refund.goodsCount }" /><b>件</b><i>（本次最多货<em>${refund.goodsCount }</em>件）</i>
						              </li>
						              <li>
						              	<span><em>*</em>退货退款说明：</span>
						              	<textarea id="reasonInfo" name="reasonInfo" rows="3" cols="50"></textarea>
						              </li>
						              <li id="fileLi1">
						              	<span style="line-height: 20px;"><em>*</em>上传凭证1：</span>
						              		<input type="file" id="photoFile1" name="photoFile1" class="photoFile" onchange="checkImgFile(this);"/>
						              		<i>（图片不能超过5MB）</i>
					              			<input id="addBtn" type="button" value="添加" onclick="addFile(this);"/>
						              </li>
						            </ul>
						          </div>
						        </div>
						         
						         <div class="ste-but">
						            <input type="submit" value="确认提交" class="re" />
						           <input type="button" value="取消并返回" class="gr" onclick="window.location.href='${contextPath}/p/myorder'"/>
						         </div>
				         </form:form>
			         </div>
			         <!-- 退款及退货块 结束 -->
			         
			     </div>
			     <!-----------right_con 结束-------------->
			     
			    <div class="clear"></div>
			 </div>
		   
		 	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
		</div>
	   	<script charset="utf-8" src="<ls:templateResource item='/resources/common/js/compress.js'/>"></script>
		<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
		<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/refund/refund.js'/>"></script>
		<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/refund/applyItemReturn.js'/>"></script>
		<script type="text/javascript">
			var contextPath = '${contextPath}';
			var maxRefundMoney =  Number('${refund.refundAmount }');//最多退款金额
			var maxGoodsCount = Number('${refund.goodsCount }');//最多退货数量
			var finalRefundMoney = maxRefundMoney;//最后的退款价格
			var oStatus = '${oder.status}';
			var reg=/^[0-9]*(\.[0-9]{1,2})?$/;
			var patrn = /^([+]?)(\d+)$/;

			function che() {
				var refundAmount = $("input[name=refundAmount]").val();
				if (refundAmount > maxRefundMoney || !reg.test(refundAmount) || refundAmount==null || refundAmount == "") {
					$("input[name=refundAmount]").css("border", "1px dashed #ed5564");
				} else {
					$("input[name=refundAmount]").css("border", "1px solid #e4e4e4");
				}
			}
			
			function check() {
				var refundAmount = $("input[name=refundAmounts]").val();
				if (refundAmount > maxRefundMoney || !reg.test(refundAmount) || refundAmount==null || refundAmount == "") {
					$("input[name=refundAmounts]").css("border", "1px dashed #ed5564");
				} else {
					$("input[name=refundAmounts]").css("border", "1px solid #e4e4e4");
				}
			}
			
			function checkGoodsNum() {
				var goodsCount = $("#goodsNum").val();
				if(goodsCount<=maxGoodsCount && patrn.test(goodsCount)){
					finalRefundMoney = Number(maxRefundMoney)/Number(maxGoodsCount)*Number(goodsCount);
					$(".amountCount").html(finalRefundMoney);
					$("#goodsNum").css("border", "1px solid #e4e4e4");

					reload();
				}else{
					$("#goodsNum").css("border", "1px dashed #ed5564");
				}
			}
		</script>
	</body>
</html>
