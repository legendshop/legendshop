<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%
	response.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT");   
	// 设置 HTTP/1.1 no-cache 头   
	response.setHeader("Cache-Control", "no-store,no-cache,must-revalidate");   
	// 设置 IE 扩展 HTTP/1.1 no-cache headers， 用户自己添加   
	response.addHeader("Cache-Control", "post-check=0, pre-check=0");   
	// 设置标准 HTTP/1.0 no-cache header.   
	response.setHeader("Pragma", "no-cache");
 %>
<!DOCTYPE html>
<html>
<head>
<!-- the same with indexFrame.jsp under red template -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="renderer" content="webkit"/>
    <c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
    <title>填写核对订单信息-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
    <link rel="stylesheet" href="<ls:templateResource item='/resources/common/css/jquery.autocomplete.css'/>">
    <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/plugins/showLoading/css/showLoading.css'/>" />
</head>
<body  class="graybody">
	<div id="doc">
         <!--顶部menu-->
		  <div id="hd">
		   
		      <!--hdtop 开始-->
		      <%@ include file="/WEB-INF/pages/plugins/main/home/header.jsp" %>
		      <!--hdtop 结束-->
		    
		       <!-- nav 开始-->
		        <div id="hdmain_cart">
		          <div class="yt-wrap">
		            <h1 class="ilogo">
		                <a href="${contextPath}/"><img src="<ls:photo item="${systemConfig.logo}"/>"  style="max-height:89px;" /></a>
		            </h1>
		            
		            <!--page tit-->
		              <div class="cartnew-title">
		                        <ul class="step_h">
		                            <li class="done"><span>购物车</span></li>
		                            <li class="on done"><span>填写核对订单信息</span></li>
		                            <li class=" "><span>提交订单</span></li>
		                        </ul>
		              </div>
		            <!--page tit end-->
		          </div>
		        </div>
		      <!-- nav 结束-->
		 </div><!--hd end-->
		<!--顶部menu end-->
    
       
         <div id="bd" style="margin-bottom: 30px;">
		      <div class="yt-wrap ptit2">填写并核对订单信息</div>
		     
		    
		      
		      <!--order content -->
		      <div class="yt-wrap ordermain">
		          
		         <%@ include file="/WEB-INF/pages/plugins/main/orderAddr.jsp" %>
		          
		          
		          <!-- 商品清单 -->
		          <div class="pro-list">
		            <h3 style="margin-bottom: -15px;">商品清单</h3>
					  <%--<a style="float: right;margin-top: -20px;color: #3f6de0;" href="${contextPath}/presell/views/${presellId}">返回购物车修改</a>--%>
		              <%-- <a style="float: right;margin-top: -20px;color: #3f6de0;" href="${contextPath}/shopCart/shopBuy">返回购物车修改</a> --%>
		            <div id="shoppinglistDiv">
		                <c:forEach items="${requestScope.userShopCartList.shopCarts}" var="carts" varStatus="status">
		                     <p style="margin-top: 15px;margin-bottom: -10px;">
				                                           店铺：<a style="color: #e5004f;" target="_blank" href="${contextPath}/store/${carts.shopId}" title="${carts.shopName}" >${carts.shopName}</a>
				             </p>
		                   <table class="cartnew_table">
								<thead style="border: 1px solid #e4e4e4;border-bottom: 0;">
									<tr>
										<th align="left" style="width:450px;">店铺宝贝</th>
										<th>单价</th>
										<th>数量</th>
										<th>状态</th>
										<th>配送方式</th>
									</tr>
								</thead>
								<tbody style="border: 1px solid #e4e4e4;border-top: 0;">
			                    <c:forEach items="${carts.cartItems}" var="basket" varStatus="s">
				                    <tr>
				                        <td class="textleft"> 
						                        <div class="cartnew-img">
						                           <a class="a_e" target="_blank" href="${contextPath}/views/${basket.prodId}">
						                           <img title="${basket.prodName}" src="<ls:images item='${basket.pic}' scale='3' />" > 
						                           </a>                            
						                        </div>
					                           <ul class="cartnew-ul" >
					                             <li> <a href="${contextPath}/presell/views/${presellId}" target="_blank" class="a_e">${basket.prodName}</a></li>
				                                 <li>
				                                    <c:if  test="${not empty basket.cnProperties}" >
				                                       ${basket.cnProperties}
				                                    </c:if>
				                                 </li>
				                              </ul>
				                        </td>
				                         <td>
				                          <c:choose>
				                            <c:when test="${basket.payPctType==0}">
				                                <fmt:formatNumber var="cashFormat" value="${basket.promotionPrice}" type="currency" pattern="0.00"></fmt:formatNumber>
				                                <span style="font-size:14px; color:#e5004f;">￥ ${cashFormat}</span>
				                            </c:when>
				                            <c:otherwise>
												<input type="hidden" value="${carts.shopTotalCash}" id="orderTotalCash">
				                               <span class="preDepositPrice" style="display: block;line-height: 14px;">定金：¥${basket.preDepositPrice}</span>
					                           <em style="color: #666;">+</em>
					                           <span class="finalPayment" style="display: block;line-height: 14px;">尾款：¥${basket.prePrice-basket.preDepositPrice}</span>
				                            </c:otherwise>
				                          </c:choose>
				                        </td>
				                         <td>
				                          ${basket.basketCount}
				                        </td>
				                        <td>
				                        <c:choose>
				                        	<c:when test="${empty requestScope.userShopCartList.provinceId}">
				                        		<span>有货</span>
				                        	</c:when>
				                        	<c:when test="${empty basket.stocks || basket.stocks==0}">
				                        		<span class="stocksErr">无货</span>
				                        	</c:when>
											<%-- 未对区域限售情况做处理 -->
				                        	<%--<c:when test="${basket.stocks<basket.basketCount}">--%>
				                        		 <%--<span class="stocksErr">缺货，仅剩${basket.stocks}件</span>--%>
				                        	<%--</c:when>--%>
											<c:when test="${basket.stocks == -1}">
												<span class="stocksErr">区域限售</span>
											</c:when>
											<c:when test="${basket.stocks > 0 && basket.stocks<basket.basketCount}">
												<span class="stocksErr">缺货，仅剩${basket.stocks}件</span>
											</c:when>
				                        	<c:otherwise>
				                        		<span>有货</span>
				                        	</c:otherwise>
				                         </c:choose>
				                        </td>
				                         <c:if test="${s.index==0}">
				                              <td  rowspan="${fn:length(carts.cartItems)}">
				                                       <c:choose>
												        <c:when test="${carts.freePostage}">
												                            商家承担运费
												        </c:when>
												        <c:otherwise>
												             <select shopId="${carts.shopId}"  class="delivery">
									                             <c:choose>
															        <c:when test="${empty carts.transfeeDtos}">
															         <option value="-1" style="padding: 5px 0px;">请选择配送方式</option>
															        </c:when>
															        <c:otherwise>
															             <c:forEach items="${carts.transfeeDtos}" var="transtype" varStatus="s">
												                            <option style="padding: 5px 0px;" transtype="${transtype.freightMode}" value="${transtype.deliveryAmount}" >${transtype.desc}</option>
												                        </c:forEach>
															         </c:otherwise>
															        </c:choose>
									                        </select>
												        </c:otherwise>
												      </c:choose>
				                                </td>
				                          </c:if>
				                    </tr>
			                    </c:forEach>
		                    </tbody>
		                </table>
		                  <div class="shop-add">
                              <div class="shop-wor">给卖家留言：<input type="text" placeholder="限45个字" name="remark" shopId="${carts.shopId}"  maxlength="45"></div>
                         </div>
		              </c:forEach>

                     <%@ include file="/WEB-INF/pages/plugins/main/orderInvoice.jsp" %>


		               <!-- 确认订单按钮 -->
			           <%@ include file="/WEB-INF/pages/plugins/presell/user/presellOrderConfirm.jsp" %>

		        </div>
		<!--order content end-->
		   </div>

	       <form:form id="orderFormReload" action="${contextPath}/p/presell/order/details/${presellId}/${payType}/${skuId}" method="post">
			   <input type="hidden" id="invoiceId" name="invoiceId" value="${userdefaultInvoice.id}" >
		   </form:form>

		<form:form id="orderForm" action="${contextPath}/p/presell/order/submitOrder/${presellId}" method="post">
		        <input id="remarkText" name="remarkText" type="hidden" />
				<input id="skuId" value="${skuId}" name="skuId" type="hidden" />
		        <input id="invoiceIdStr" name="invoiceId" type="hidden" value="${userdefaultInvoice.id}" />
		        <input id="delivery" name="delivery" type="hidden">
		        <input name="token" type="hidden" value="${SESSION_TOKEN}"/>
		        <input name="payType" type="hidden" value="${payType}"/>
		</form:form>

      </div>

	</div>

		 <!----foot---->
		 <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
        <!----foot end---->

	</div>
</body>
   <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
   <script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>"></script>
   <script  charset="utf-8" src="<ls:templateResource item='/resources/plugins/showLoading/js/jquery.showLoading.min.js'/>"></script>
   <script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/presell/preSellOrder.js'/>"></script>
   <script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/addressSlide.js'/>"></script>
   <script type="text/javascript">
		var contextPath = '${contextPath}';
		var data = {allCount:"${requestScope.userShopCartList.orderTotalQuanlity}"};
		var presellId='${presellId}';
		var payType = '${payType}';
   </script>
</html>
