<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

<style>
 .dialog_content dl { font-size: 0; *word-spacing:-1px/*IE6、7*/; line-height: 20px; display: block; clear: both; overflow:hidden;}
 .dialog_content dl dt { font-size: 12px; line-height: 32px; vertical-align: top; letter-spacing: normal; word-spacing: normal; text-align: right; display: inline-block; *display: inline/*IE6,7*/; width: 29%; padding: 10px 1% 10px 0; margin: 0; zoom: 1;}
 .dialog_content dl dt i.required { font: 12px/16px Tahoma; color: #F30; vertical-align: middle; margin-right: 4px; }
</style>
 <link type="text/css" href="<ls:templateResource item='/resources/templets/css/active/actives${_style_}.css'/>" rel="stylesheet">
<c:choose>
	<c:when test="${empty list}">
	    <div style="border-top: 1px solid #ededed;padding: 10px 0;color: #666;text-align: center;">暂无符合条件的数据记录</div>
	</c:when>
	<c:when test="${not empty list}">
	    <ul class="goods-list">
	       <c:forEach items="${list}" var="prods" varStatus="status">
	          <li>
				 <div class="goods-thumb">
					<a target="_blank" href="<ls:url address='/views/${prods.prodId}'/>" title="${prods.prodName}">
					   <img src="<ls:images item='${prods.pic}' scale='2'/>">
					</a>
				 </div>
				 <dl class="goods-info">
					<dt>
						<a target="_blank" href="<ls:url address='/views/${prods.prodId}'/>" title="${prods.prodName}">
						  ${prods.prodName}
						</a>
					</dt>
					<dd>销售价格：¥<fmt:formatNumber value="${prods.cash}" pattern="#0.00#"/></dd>
				 </dl> 
				   <a title="${prods.prodName}" style="margin-left: 33px;" class="btn-g big-btn" onclick="selectProd('${prods.prodId}','${prods.skuId}','${prods.cash}');" id="btn_add_xianshi_goods">选择商品</a>
				</li>
		   </c:forEach>
		</ul>
			 <div style="margin-top:10px;" class="page clearfix">
				  <div class="p-wrap">
				         <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple" actionUrl="javascript:prodPager"/> 
				  </div>
		  	</div>
		  	
			<div class="clear"></div>
		
	</c:when>
</c:choose>
<script src="<ls:templateResource item='/resources/templets/js/goodsSearchResult.js'/>"></script>
<script type="text/javascript">
		var contextPath = "${contextPath}";
	   var marketId=${marketId};
	   var contextPath = '${contextPath}';
	   var type="${type}";
</script>