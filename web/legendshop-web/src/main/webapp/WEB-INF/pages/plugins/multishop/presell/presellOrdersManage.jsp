<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>

<%@ taglib uri="http://www.legendesign.net/date-util"  prefix="dateUtil" %>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}"  pattern="yyyy-MM-dd HH:mm" var="nowDate"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>预售订单-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-sold${_style_}.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
    
</head>
<style type="text/css">
   .sold-table .presell-info{
	   	color:#666;
	   	font-size:12px;
	   	height:40px;
	   }
</style>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">预售订单</span>
				</p>
			</div>
				<%@ include file="../included/sellerLeft.jsp" %>

	<div class="seller-sold">
				<form:form id="ListForm" action="${contextPath}/s/presellOrdersManage" method="get">
					<input type="hidden" value="${paramDto.curPageNO==null?1:paramDto.curPageNO}" id="curPageNO" name="curPageNO">
		            <input type="hidden" value="${paramDto.status}" id="status" name="status">
					<input type="hidden" value="${paramDto.shopId}"  name="shopId">
					<div class="sold-ser clearfix">
						<span class="item">
							下单时间：<input type="text"  value="<fmt:formatDate value="${paramDto.startDate}" pattern="yyyy-MM-dd"/>" readonly="readonly"  id="startDate" name="startDate"  class="hasDatepicker item-inp" >
							&nbsp;—&nbsp;<input type="text"  value="<fmt:formatDate value="${paramDto.endDate}" pattern="yyyy-MM-dd"/>" id="endDate" name="endDate" class="text  hasDatepicker item-inp" readonly="readonly">
						</span>
						<span class="item">
							订单编号：<input type="text" class="item-inp" style="width:200px;" id="subNumber" value="${paramDto.subNumber}"  name="subNumber">
						</span>
						<span class="item">
							买家：
							<input type="text" class="item-inp" value="${paramDto.userName}"  id="userName" name="userName"  style="width:150px;">
							<input type="button" onclick="search()" value="搜索订单" class="btn-r">
						</span>
					</div>
				</form:form>
<!-- 搜索订单 -->
				
				<div class="seller-com-nav" style="margin-top:20px;">
					<ul>
					  <li <c:if test="${empty paramDto.status}">class="on"</c:if>  ><a href="<ls:url address="/s/presellOrdersManage"/>">所有订单</a></li>
                      <li <c:if test="${paramDto.status eq 1}">class="on"</c:if>  ><a href="<ls:url address="/s/presellOrdersManage?status=1"/>">已经提交</a></li>
                      <li <c:if test="${paramDto.status eq 2}">class="on"</c:if>  ><a href="<ls:url address="/s/presellOrdersManage?status=2"/>">已经付款</a></li>
                      <li <c:if test="${paramDto.status eq 3}">class="on"</c:if>  ><a href="<ls:url address="/s/presellOrdersManage?status=3"/>">已经发货</a></li>
                      <li <c:if test="${paramDto.status eq 4}">class="on"</c:if>  ><a href="<ls:url address="/s/presellOrdersManage?status=4"/>">已经完成</a></li>
                      <li <c:if test="${paramDto.status eq 5}">class="on"</c:if>  ><a href="<ls:url address="/s/presellOrdersManage?status=5"/>">已经取消</a></li>
					</ul>
				</div>
				
				<table class="sold-table"  cellspacing="0" cellpadding="0">
					<tbody><tr class="sold-tit">
						<td colspan="2" width="300">商品</td>
						<td width="80">单价（元）</td>
						<td width="35">数量</td>
						<td width="100">买家</td>
						<td width="100">订单金额</td>
						<td width="100">交易状态</td>
						<td width="100" style="border-right:1px solid #e4e4e4;">交易操作</td>
					</tr>
					<c:choose>
			           <c:when test="${empty requestScope.list}">
			              <tr>
						 	 <td colspan="20">
						 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
						 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
						 	 </td>
						 </tr>
			           </c:when>
			           <c:otherwise>
			               <c:forEach items="${requestScope.list}" var="order" varStatus="orderstatues">				
								<tr class="num-tim">
									<td colspan="8" style="border-right:1px solid #e4e4e4;">
										<span>订单编号：${order.subNo}</span>
										<span>下单时间：<fmt:formatDate value="${order.subCreateTime}" type="both" /></span>
										<%--<c:if test="${order.status eq 3 || order.status eq 4}">--%>
							                 <%--<span class="fr">--%>
												<%--<a title="打印发货单" href="javascript:void(0)" class="bot-dis printInvoice" data-subId="${order.subId}" data-dvyTypeId="${order.dvyTypeId}">--%>
											<%--打印发货单</a>--%>
									         <%--</span>--%>
							            <%--</c:if>--%>
							      		<span class="fr">
							              <a title="打印订单" target="_blank" class="bot-dis"
										     href="<ls:url address="/s/orderPrint/${order.subNo}"/>">
											打印订单</a>
							         	</span>
									</td>
								</tr>
								
								<c:forEach items="${order.orderItems}" var="orderItem" varStatus="orderItemStatues">
									<tr class="detail">
										<td>
											<a href="<ls:url address='/views/${orderItem.prodId}'/>" target="_blank">
												<img alt="" src="<ls:images item='${orderItem.prodPic}' scale='3'/>">
											</a>
										</td>
										<td>
											<a href="<ls:url address='/views/${orderItem.prodId}'/>" target="_blank">
												${orderItem.prodName}
											</a>
											<br>
											${orderItem.attribute}
										</td>
										<td>￥<fmt:formatNumber value="${orderItem.prodCash}" type="currency" pattern="0.00"></fmt:formatNumber></td>
										<td>${orderItem.basketCount}</td>
										
										<c:set var="subItemCount"  value="${fn:length(order.orderItems)}"></c:set> 
										<c:if test="${orderItemStatues.index==0}">
											<td class="buyer-td col-bla <c:if test="${subItemCount>1}">b-left</c:if>" rowspan="${subItemCount}">
												 <span>${order.userName}</span>
												 <c:if test="${not empty order.userAddressSub}">
							                       <div class="buyer-info">
													<em></em>
													<div class="con">
														<h3>
															<span>买家信息</span>
														</h3>
														<div>
															<span>姓名：${order.userAddressSub.receiver}</span>
														</div>
														<c:if test="${not empty order.userAddressSub.telphone}">
															<div>
																<span>固话：${order.userAddressSub.telphone}</span>
															</div>
														</c:if>
														<c:if test="${not empty order.userAddressSub.mobile}">
															<div>
																<span>手机：${order.userAddressSub.mobile}</span>
															</div>
														</c:if>
														<div>
															<span>地址：${order.userAddressSub.province}&nbsp;${order.userAddressSub.city}&nbsp;${order.userAddressSub.area}&nbsp;${order.userAddressSub.subAdds}</span>
														</div>
													</div>
												  </div>
							                    </c:if>
											</td>
											<td class="<c:if test="${subItemCount>1}">b-left</c:if>" rowspan="${subItemCount}">
												<ul>
													<li class="col-red">￥<fmt:formatNumber value="${order.actualTotalPrice}" type="currency" pattern="0.00"></fmt:formatNumber></li>
													<li>
														 <c:choose>
									                		<c:when test="${not empty order.freightAmount}">
									                		  (含运费￥<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)
									                		</c:when>
									                		<c:otherwise>
									                			（免运费）
									                  		</c:otherwise>
									                 	</c:choose>
													</li>
													<li>${order.payManner==1?"货到付款":"在线支付"}</li>
												</ul>
											</td>
											<td class="<c:if test="${subItemCount>1}">b-left</c:if>" rowspan="${subItemCount}">
												<ul>
													<li>
														<c:choose>
															 <c:when test="${order.status eq 1 }">待付款</c:when>
															 <c:when test="${order.status eq 2 }"><span class="col-orange">待发货</span></c:when>
															 <c:when test="${order.status eq 3 }">待收货</c:when>
															 <c:when test="${order.status eq 4 }">已完成</c:when>
															 <c:when test="${order.status eq 5 }">交易关闭</c:when>
														  </c:choose>
													</li>
													<li><a href="${contextPath}/s/presellOrderDetail/${order.subNo}">订单详情</a></li>
													<c:if test="${order.status eq 3 || order.status eq 4}">
													   	<li><a target="_blank" href="${contextPath}/s/orderExpress/${order.subNo}" >查看物流</a></li>  
												    </c:if>
												</ul>
											</td>
											<td class="<c:if test="${subItemCount>1}">b-left</c:if>" rowspan="${subItemCount}" style="border-right:1px solid #e4e4e4;">
												<ul class="mau">
												<c:choose>
													  <c:when test="${order.status==1 }">
													    <c:if test="${order.isPayDeposit eq 0 }">
													      <li><a class="btn-r" href="javascript:void(0)" onclick="cancleOrder('${order.subNo}');">
														       取消订单</a></li>
														   <li><a href="javascript:void(0)" onclick="changeOrderFee('${order.subNo}');">
														         调整订单</a></li>
												        </c:if>
													  </c:when>
													  <c:when test="${order.status==2}">
													      <c:if test="${order.isPayFinal eq 0 && order.payPctType eq 1}">
											                  	<span class="col-orange">等待用户支付尾款</span>
											              </c:if>	
											              <c:if test="${order.isPayFinal eq 1 && order.payPctType eq 1}">
											                  <li><a class="btn-r" href="javascript:void(0)" onclick="deliverGoods('${order.subNo}');" >确认发货</a></li>
											              </c:if>
											              <c:if test="${order.payPctType eq 0}">
											                  <li><a class="btn-r" href="javascript:void(0)" onclick="deliverGoods('${order.subNo}');" >确认发货</a></li>
											              </c:if>		
													  </c:when>
												  </c:choose>
											  </ul>
											</td>
										</c:if>	
									</tr>			
			      				</c:forEach>
			      				<tr class="presell-info">
					                <td colspan="2" align="center">阶段1：定金</td>
					                <td colspan="3" align="center">¥<fmt:formatNumber value="${order.depositPrice }" type="currency" pattern="0.00"></fmt:formatNumber>(含运费<fmt:formatNumber value="${order.freightAmount}" type="currency" pattern="0.00"></fmt:formatNumber>)</td>
					                <c:if test="${order.status eq 5}">
					                	<td colspan="4"></td>
					                </c:if>
					                <c:if test="${order.status ne 5}">
						                <c:if test="${order.isPayDeposit eq 0 }">
							                <c:if test="${dateUtil:getOffsetMinutes(order.subCreateTime,nowTime) le 60}">
						                		<td colspan="2" align="center">用户可付定金时间剩余<font color="#e5004f">${60 - dateUtil:getOffsetMinutes(order.subCreateTime,nowTime)}</font>分钟</td>
						                		<td align="center" >买家未付款</td>
						                	</c:if>
						                	<c:if test="${dateUtil:getOffsetMinutes(order.subCreateTime,nowTime) gt 60}">
						                		<td colspan="4" align="center">已过期</td>
						                	</c:if>
						                </c:if>
						                <c:if test="${order.isPayDeposit eq 1 }">
						                	<td  colspan="4" align="center" >
							                	<span class="col-orange">买家已付款</span>
							                </td>
						                </c:if>
					                </c:if>
					              </tr>
					              <tr class="presell-info">
					                <td colspan="2" align="center">阶段2：尾款</td>
					                <td colspan="3" align="center">¥<fmt:formatNumber value="${order.finalPrice }" type="currency" pattern="0.00"></fmt:formatNumber></td>
					                <c:if test="${order.status eq 5}">
					                	<td colspan="4"></td>
					                </c:if>
					                <c:if test="${order.status ne 5}">
					                	<c:if test="${order.isPayFinal eq 0}">
							                <td colspan="2" align="center">
							                	尾款支付时间: </br><fmt:formatDate value="${order.finalMStart }" pattern="yyyy-MM-dd" /> - <fmt:formatDate value="${order.finalMEnd }" pattern="yyyy-MM-dd" />
							                </td>
							                <td colspan="2" align="center" >
							                   <c:choose>
							                		<c:when test="${order.finalMStart gt nowDate }">未开始</c:when>
							                		<c:when test="${order.finalMStart le nowDate and order.finalMEnd gt nowDate}">买家未付款</c:when>
							                		<c:when test="${order.finalMEnd le nowDate }">已过期</c:when>
							                	</c:choose>
							                 </td>
						                </c:if>
						                <c:if test="${order.isPayFinal eq 1}">
					                	  <td colspan="4" align="center" >
						                	  <span class="col-orange">买家已付款</span>
						                   </td>
						                </c:if>	
					                </c:if>
					              </tr>
								<tr>
									<td colspan="8" class="table-space"></td>
								</tr>
							</c:forEach>
			           </c:otherwise>
			         </c:choose>
				</tbody></table>
				<div class="clear"></div>
				 <div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
				            		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
				     			 </div>
		  				</div>
			</div>
</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type="text/javascript">
		var contextPath="${contextPath}";
		$(function(){
			laydate.render({
				   elem: '#startDate',
				   calendar: true,
				   theme: 'grid',
				   trigger: 'click'
			  });
	     
	     	laydate.render({
				   elem: '#endDate',
				   calendar: true,
				   theme: 'grid',
				   trigger: 'click'
			  });
		});
		function search(){
		  	$("#ListForm #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		  	$("#ListForm")[0].submit();
		}
	</script>
	<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/ToolTip.js'/>"></script>
	<script src="<ls:templateResource item='/resources/templets/js/presellOrderManage.js'/>" type="text/javascript"></script>
</body>
</html>