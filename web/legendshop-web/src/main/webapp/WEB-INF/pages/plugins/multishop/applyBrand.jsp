<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>品牌列表-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-brand.css'/>" rel="stylesheet">
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">品牌列表</span>
				</p>
			</div>
		<%@ include file="included/sellerLeft.jsp" %>
			<div class="seller-brand">
				<div class="seller-com-nav" >
					<ul>
						<li class="on"><a href="javascript:void(0);">品牌列表</a></li>
						<li><a id="brandConfigure" href="javascript:void(0);" onclick="loadConfigure();">品牌申请</a></li>
					</ul>
				</div>
					<input type="hidden" id="curPageNO" name="curPageNO" value="1"/>
				<table class="brand-table sold-table" style="margin-top:15px;">
					<tr class="brand-tit">
						<th width="250" style="max-width:250px;">品牌名称</th>
						<th width="130">品牌图片</th>
						<th width="400" style="max-width:400px;">品牌介绍</th>
						<th width="90">状态</th>
						<th width="125">操作</th>
					</tr>
					<c:choose>
			           <c:when test="${empty requestScope.list}">
			              <tr>
						 	 <td colspan="20" style="border-right: 1px solid #e4e4e4">
						 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
						 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
						 	 </td>
						 </tr>
			           </c:when>
			           <c:otherwise>
			              <c:forEach items="${requestScope.list}" var="item" varStatus="status">
								<tr class="detail">
									<td style="padding:8px 5px;">${item.brandName}</td>
									<td><img src="<ls:images scale="3" item='${item.brandPic}'  />"></td>
									<td style="word-break:break-all;padding:8px 5px;" width="400px">${item.brief}</td>
									<c:choose>
										<c:when test="${item.status==2}">
											<td>审核通过</td>
										</c:when>
										<c:when test="${item.status==-2}">
											<td class="col-orange">审核失败</td>
										</c:when>
										<c:when test="${item.status==-1}">
											<td>审核中</td>
										</c:when>
										<c:when test="${item.status==0}">
											<td>下线</td>
										</c:when>
										<c:otherwise>
											<td>上线</td>
										</c:otherwise>
									</c:choose>
									<td>
											<c:if test="${item.status eq -2}">
												<a href="${contextPath}/s/applyBrand/update/${item.brandId}" title="修改" target="_blank" class="btn-r" style="margin-right: 5px;">修改</a>
											</c:if>
											<%--<c:if test="${item.status eq 1}">--%>
												<%--<a href='javascript:brandDown(${item.brandId})' title="下线"   class="col-gra">下线</a>--%>
											<%--</c:if>--%>
											<%--<c:if test="${item.status eq 0}">--%>
												<%--<a href='javascript:brandUp(${item.brandId})' title="上线"  class="col-gra">上线</a>--%>
											<%--</c:if>--%>
											<c:if test="${item.status eq -2 || item.status eq -1}">
												<a href="javascript:deleteById('${item.brandId}');" title="删除" class="btn-g">删除</a>
											</c:if>
									</td>
								</tr>
							</c:forEach>
			           </c:otherwise>
		           </c:choose>
				</table>
				<div class="clear"></div>
					<div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
				            		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
				     			 </div>
		  				</div>
				<!-- <div class="brand-add">
					<ul>
						<li>品牌名称：<input type="text"></li>
						<li class="file">品牌logo：<input type="file"</li>
						<li class="file">品牌大图：<input type="file"</li>
						<li class="file">品牌产品授权书原件扫描件：<input type="file"></li>
						<li class="textarea">
							品牌介绍：<textarea></textarea>
						</li>
					</ul>
					<div class="bot-save"><a href="#">提交申请</a></div>
					<ul class="annotation">
						<li class="ann-det"><span>注：</span>申请品牌填完此申请表后还需要将以下证件寄往本公司审核通过方可申请成功</li>
						<li>1、营业执照复印件</li>
						<li>2、组织机构代码复印件</li>
						<li>3、品牌授权书复印件</li>
						<li>4、品牌申请表（加盖公章）</li>
					</ul>
				</div> -->
			</div>
		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/applyBrand.js'/>"></script>
	<script type="text/javascript">
		var contextPath="${contextPath}";
	</script>
</body>
</html>