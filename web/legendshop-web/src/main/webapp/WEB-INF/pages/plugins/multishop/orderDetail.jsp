<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>订单详情-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/templets/css/multishop/orderdetail.css'/>"/>
</head>
<body class="graybody">
<div id="doc">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
    <div class="w1190">
        <div class="seller-cru">
            <p>
                	您的位置>
                <a href="${contextPath}/home">首页</a>>
                <a href="${contextPath}/sellerHome">卖家中心</a>>
                <span class="on">订单详情</span>
            </p>
        </div>
        <%@ include file="included/sellerLeft.jsp" %>
        <div class="right_con">
            <div class="right-layout">
                <div class="ncm-oredr-show">
                    <div class="ncm-order-info">
                        <div class="ncm-order-details" style="border-bottom:1px solid #ddd;">
                            <div class="title">订单信息</div>
                            <div class="content">
                          		<c:choose>
                            		<c:when test="${order.subType eq 'SHOP_STORE'}">
                            			<dl>
	                                        <dt>提货人：</dt>
	                                        <dd>
												<span>
				                                    ${storeOrder.reciverName}
												</span>
	                                        </dd>
	                                    </dl>
	                                    <dl>
	                                        <dt>联系电话：</dt>
	                                        <dd>
												 <span>
													${storeOrder.reciverMobile}
												 </span>
	                                        </dd>
	                                    </dl>
                            		</c:when>
                            		<c:otherwise>
                            			 <dl>
	                                        <dt>收货地址：</dt>
	                                        <dd>
												<span>
													<c:if test="${not empty order.userAddressSub}">
					                                    ${order.userAddressSub.receiver} ,
					                                    ${order.userAddressSub.detailAddress}
					                                </c:if>
												 </span>
	                                        </dd>
	                                    </dl>
	                                     <dl>
	                                        <dt>联系电话：</dt>
	                                        <dd>
											 <span>
												<c:if test="${not empty order.userAddressSub}">
				                                    ${order.userAddressSub.mobile} 
				                                </c:if>
											 </span>
	                                        </dd>
	                                    </dl>
                                        <dl>
                                            <dt>发&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;票：</dt>
                                            <dd>
                                                <c:choose>
                                                    <c:when test="${empty order.invoiceSub}">
                                                        不开发票
                                                    </c:when>
                                                    <c:otherwise>
                                                        ${order.invoiceSub.type==1?"普通发票":"增值税发票"}
                                                        <c:choose>
                                                            <c:when test="${order.invoiceSub.type eq 2}">
                                                                <a onclick="showInvoiceSub();" style="text-decoration:underline;cursor:pointer;float: none;" >查看详情</a>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:choose>
                                                                    <c:when test="${order.invoiceSub.title==1}">[个人抬头]</c:when>
                                                                    <c:otherwise>[公司抬头]</c:otherwise>
                                                                </c:choose>&nbsp;
                                                                ${order.invoiceSub.company}&nbsp;
                                                                <c:if test="${order.invoiceSub.title ne 1}">
                                                                    (${order.invoiceSub.invoiceHumNumber})
                                                                </c:if>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:otherwise>
                                                </c:choose>
                                            </dd>
                                        </dl>

                            		</c:otherwise>
                            	</c:choose>
                                <dl>
                                    <dt>买家留言：</dt>
                                    <dd>
                                        <c:choose>
                                            <c:when test="${empty order.orderRemark}">无</c:when>
                                            <c:otherwise>
                                                ${order.orderRemark}
                                            </c:otherwise>
                                        </c:choose>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>订单编号：</dt>
                                    <dd>
                                        ${order.subNum}
                                        <a href="javascript:void(0);">更多
                                            <div id="history" class="more">
                                            </div>
                                        </a>
                                    </dd>
                                </dl>

                            </div>
                        </div>
                        <div class="ncm-order-condition">
                            <dl>
                                <dt>
                                    <i class="icon-ok-circle green"></i>订单状态：
                                </dt>
                                <dd>
                                    <c:choose>
                                   	    <c:when test="${order.refundState == 1 }">
                                           <span class="col-orange">退款退货中</span>
                                        </c:when>
                                        <c:otherwise>
                                            <c:choose>
                                                <c:when test="${order.status==1 }">
		                                           	订单已经提交，等待买家付款
		                                        </c:when>
		                                        <c:when test="${order.status==2 }">
			                                        <c:choose>
														<c:when test="${order.mergeGroupStatus eq 0 && order.subType eq 'MERGE_GROUP'}">拼团中</c:when>
														<c:when test="${order.subType eq 'GROUP' && order.groupStatus eq 0}">团购中</c:when>
														<c:when test="${order.subType eq 'SHOP_STORE'}">待提货</c:when>
														<c:otherwise><span class="col-orange">待发货</span></c:otherwise>
													</c:choose>
		                                        </c:when>
		                                        <c:when test="${order.status==3 }">
		                                         	<span class="col-orange">待收货</span>
		                                        </c:when>
		                                        <c:when test="${order.status==4 }">
		                                           	 已完成
		                                        </c:when>
		                                        <c:when test="${order.status==5 || order.status==-1 }">
		                                                                                                                                     交易关闭
		                                        </c:when>
                                            </c:choose>
                                        </c:otherwise>
                                    </c:choose>
                                </dd>
                            </dl>
                            <ul>
                             <c:if test="${order.refundState ne 1 }">
                                <c:choose>
                                    <c:when test="${order.status==1 && order.subType != 'SHOP_STORE'}">
                                        <li>1. 等待卖家购买此订单的商品，请选择<a class="ncbtn-mini" href="#order-step">取消订单</a>操作。</li>
                                        <!-- <li>3. 如果您未对该笔订单进行支付操作，系统将于 <time>2015-07-20 16:11:49</time>
                                       	 自动关闭该订单。</li>
                                        --> </c:when>
                                    <c:when test="${order.status==2 && order.subType != 'SHOP_STORE'}">
                                        <c:choose>
											<c:when test="${order.subType ne 'MERGE_GROUP' && order.subType ne 'GROUP'}">
												 <li>1. 订单已提交商家进行备货发货准备,对订单进行<a href="#order-step" class="btn-r">商品发货</a>操作。</li></li>
											</c:when>
											<c:when test="${order.subType eq 'GROUP'}">
												<c:choose>
													<c:when test="${order.groupStatus eq 1}">
														<li>1. 订单已提交商家进行备货发货准备,对订单进行<a href="#order-step" class="btn-r">商品发货</a>操作。</li>
													</c:when>
													<c:otherwise>活动凑单中</c:otherwise>
												</c:choose>
											</c:when>
											<c:otherwise>
												<c:choose>
													<c:when test="${order.mergeGroupStatus eq 1}">
														<li>1. 订单已提交商家进行备货发货准备,对订单进行<a href="#order-step" class="btn-r">商品发货</a>操作。</li></li>
													</c:when>
													<c:otherwise>订单拼团中</c:otherwise>
												</c:choose>
											</c:otherwise>
										</c:choose>
                                    </c:when>
                                    <c:when test="${order.status==3 && order.subType != 'SHOP_STORE'}">
                                        <li>1. 商品已发出； 物流公司：${order.delivery.delName}； 查看 <a class="blue" href="#order-step">“物流跟踪”</a> 情况。</li>
                                    </c:when>
                                    <%--   <c:when test="${order.status==4}">
                                                    <li>1. 如果收到货后出现问题，您可以联系商家协商解决。</li>
                                                <!-- 	<li>2. 如果商家没有履行应尽的承诺，您可以申请 <a class="red" href="#order-step">"投诉维权"</a>。</li> -->
                                                     <li>2. 交易已完成，你可以对购买的商品及商家的服务进行<a class="ncbtn-mini ncbtn-aqua" href="#order-step">评价</a>及晒单。</li>
                                      </c:when> --%>
                                    <c:when test="${order.status==4}">
                                        <li>1. 交易已完成，买家可以对购买的商品及服务进行评价。</li>
                                        <li>2. 评价后的情况会在商品详细页面中显示，以供其它会员在购买时参考。</li>
                                    </c:when>
                                    <c:when test="${order.status==5 || order.status==-1 && order.subType != 'SHOP_STORE'}">
                                        <c:choose>
                                            <c:when test="${order.refundState eq 2}">
                                                <li>系统 于 <fmt:formatDate value="${order.updateDate}" type="both"/> 取消了订单 ( 商品全部退款完成取消订单 )</li>
                                            </c:when>
                                            <c:otherwise>
                                                <li>订单于<fmt:formatDate value="${order.updateDate}" type="both"/> 取消<if test="${not empty order.cancelReason}">(${order.cancelReason})</if></li>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <c:when test="${order.subType == 'SHOP_STORE'}">
                                        <li>该订单为门店订单</li>
	      								<li>门店名：${store.name}</li>
	      								<li>门店地址：${store.shopAddr}</li>
                                    </c:when>
                                </c:choose>
                             </c:if>
                            </ul>
                        </div>
                        <!-- <div class="mall-msg">
                        	    有疑问可咨询<a href="javascript:void(0);"><i class="icon-comments-alt"></i>平台客服</a>
                        </div> -->
                    </div>
                    <!-- 订单的退款状态不是已完成 或者 订单不是已取消的情况下 正常显示订单进度图 -->
                    <c:if test="${order.refundState ne 2 || order.status ne 5}">
                        <c:choose>
                            <c:when test="${order.subType != 'SHOP_STORE'}">
                                <div class="ncm-order-step" id="order-step">
                                    <dl class="step-first current">
                                        <dt>生成订单</dt>
                                        <dd class="bg"></dd>
                                        <dd title="下单时间" class="date"><fmt:formatDate value="${order.subDate}" type="both"/></dd>
                                    </dl>
                                    <dl class="<c:if test="${not empty order.payDate}">current</c:if>">
                                        <dt>完成付款</dt>
                                        <dd class="bg"></dd>
                                        <c:if test="${not empty order.payDate}">
                                            <dd title="付款时间" class="date"><fmt:formatDate value="${order.payDate}" type="both"/></dd>
                                        </c:if>
                                    </dl>
                                    <dl class="<c:if test="${not empty order.dvyDate}">current</c:if>">
                                        <dt>商家发货</dt>
                                        <dd class="bg"></dd>
                                        <c:if test="${not empty order.dvyDate}">
                                            <dd title="发货时间" class="date"><fmt:formatDate value="${order.dvyDate}" type="both"/></dd>
                                        </c:if>
                                    </dl>
                                    <dl class="<c:if test="${not empty order.finallyDate}">current</c:if>">
                                        <dt>确认收货</dt>
                                        <dd class="bg"></dd>
                                        <c:if test="${not empty order.finallyDate}">
                                            <dd title="确认收货时间" class="date"><fmt:formatDate value="${order.finallyDate}" type="both"/></dd>
                                        </c:if>
                                    </dl>
                                    <dl class="<c:if test="${not empty order.finallyDate}">current</c:if>">
                                        <dt>完成</dt>
                                        <dd class="bg"></dd>
                                        <c:if test="${not empty order.finallyDate}">
                                            <dd title="完成时间" class="date"><fmt:formatDate value="${order.finallyDate}" type="both"/></dd>
                                        </c:if>
                                    </dl>
                                </div>
                            </c:when>
                            <c:otherwise>
                            </c:otherwise>
                        </c:choose>
                    </c:if>
                    <div class="ncm-order-contnet">
                        <table class="ncm-default-table order sold-table">
                            <thead>
                            <tr>
                                <th class="w10"></th>
                                <th colspan="2">商品名称</th>
                                <th class="w20"></th>
                                <th class="w120" align="center">单价（元）</th>
                                <th class="w90">数量</th>
                                <!-- <th class="w100">优惠活动</th> -->
                                <!-- <th class="w100">售后维权</th> -->
                                <th class="w100">交易状态</th>
                                <th class="w100">交易操作</th>
                            </tr>
                            </thead>
                            <tbody>

                            
                              <tr>
                                  <td style="text-align: left;" colspan="10">
                                      <div class="order-deliver">
                                      	<span>支付方式： ${order.payTypeName}</span>
                                      	
                                      	<c:if test="${(order.status==3 || order.status==4) && (order.subType != 'SHOP_STORE')}">
									<span>物流公司： <a href="${order.delivery.delUrl}"target="_blank">${order.delivery.delName}</a></span>
									<span> 物流单号： ${order.delivery.dvyFlowId} </span>
									<span><a id="show_shipping" href="<ls:url address='/s/orderExpress/${order.subNum}'/>" target="_blank">物流跟踪<i class="icon-angle-down"></i>
                                  <div class="more">
                                      <span class="arrow"></span>
                                      <ul id="shipping_ul">
                                          <li>加载中...</li>
                                      </ul>
                                      </c:if>
                                  </div>
                              </a>
						</span>
                                      </div>
                                  </td>
                              </tr>
                            


                            <!-- S 商品列表 -->
                            <c:forEach items="${order.subOrderItemDtos}" var="orderItem" varStatus="orderItemStatues">
                                <tr class="bd-line">
                                    <td class="bdl"></td>
                                    <td width="80">
                                        <div class="ncm-goods-thumb">
                                            <a target="_blank"
                                               href="<ls:url address='/views/${orderItem.prodId}'/>">
                                                <img onmouseout="toolTip()" onmouseover="toolTip();"
                                                     src="<ls:images item='${orderItem.pic}' scale='3'/>">

                                            </a>
                                        </div>
                                    </td>
                                    <td class="tl">
                                        <dl class="goods-name">
                                            <dt>
                                                <a target="_blank"
                                                   href="<ls:url address='/views/${orderItem.prodId}'/>">${orderItem.prodName}</a>
                                            </dt>
                                            <dd>${orderItem.attribute}</dd>
                                            <!-- 消费者保障服务 -->
                                        </dl>
                                    </td>
                                    <td></td>
                                    <td class="refund">
                                        <p><fmt:formatNumber pattern="0.00" value="${orderItem.cash}"/></p>
                                        <c:if test="${orderItem.refundState == 2}">
                                            <p style="color:#69AA46;"><fmt:formatNumber pattern="0.00" value="${orderItem.refundAmount}"/>(退)</p>
                                        </c:if>
                                    </td>
                                    <td style="border-right: 1px solid #e7e7e7">${orderItem.basketCount}</td>
                                    <!-- <td></td> -->
                                    <!-- <td>
                                     	   退款 投诉
                                   </td> -->

                                    <!-- S 合并TD -->
                                    <c:if test="${orderItemStatues.index==0}">
                                        <c:choose>
                                            <c:when test="${fn:length(order.subOrderItemDtos)>0}">
                                                <!-- 未支付 -->
                                                <td rowspan="${fn:length(order.subOrderItemDtos)}" class="bdl">
                                            </c:when>
                                            <c:otherwise>
                                                <td class="bdl">
                                            </c:otherwise>
                                        </c:choose>
                                         <c:if test="${order.refundState ne 1 }">
	                                        <c:choose>
	                                            <c:when test="${order.status==1 }">
	                                             	<span class="col-orange">待付款</span>
	                                            </c:when>
	                                            <c:when test="${order.status==2 }">
	                                              <c:choose>
														<c:when test="${order.mergeGroupStatus eq 0}">拼团中</c:when>
														<c:when test="${order.subType eq 'GROUP' &&  order.groupStatus eq 0}">团购中</c:when>
														<c:when test="${order.subType eq 'SHOP_STORE'}">待提货</c:when>
														<c:otherwise><span class="col-orange">待发货</span></c:otherwise>
													</c:choose>
	                                            </c:when>
	                                            <c:when test="${order.status==3 }">
	                                             	   等待买家确认收货
	                                            </c:when>
	                                            <c:when test="${order.status==4 }">
	                                               	 已完成
	                                            </c:when>
	                                            <c:when test="${order.status==5 || order.status==-1}">
	                                               	 交易关闭
	                                            </c:when>
	                                        </c:choose>
                                       </c:if>
                                        </td>

                                        <c:choose>
                                            <c:when test="${fn:length(order.subOrderItemDtos)>0}">
                                                <!-- 未支付 -->
                                                <td rowspan="${fn:length(order.subOrderItemDtos)}" class="bdl" style="border-right: 1px solid #ddd;">
                                            </c:when>
                                            <c:otherwise>
                                                <td class="bdl" style="border-right: 1px solid #ddd;">
                                            </c:otherwise>
                                        </c:choose>
                                        <!-- 取消订单 -->
                                        <p>
                                            <c:if test="${order.refundState != 1 }">
                                                <c:choose>
                                                    <c:when test="${order.status==1 }">
                                                        <a class="ncbtn ncbtn-grapefruit" href="javascript:void(0)" onclick="cancleOrder('${order.subNum}');">
                                                            <i class="icon-ban-circle"></i> 取消订单</a>
                                                    </c:when>
                                                    <c:when test="${order.status==2 }">
                                                        <c:choose>
															<c:when test="${order.subType ne 'MERGE_GROUP' and order.subType ne 'GROUP' and order.subType ne 'SHOP_STORE'}">
																<a class="btn-r" onclick="deliverGoods('${order.subNum}');" >
													     		<i class="icon-ban-circle"></i>商品发货</a>
															</c:when>
															<c:when test="${order.subType eq 'GROUP' && order.groupStatus eq 1}">
																<a class="btn-r" onclick="deliverGoods('${order.subNum}');" >
													     		<i class="icon-ban-circle"></i>商品发货</a>
															</c:when>
															<c:otherwise>
																<c:if test="${order.mergeGroupStatus eq 1}">
																	   <a class="btn-r" onclick="deliverGoods('${order.subNum}');" >
													     			   <i class="icon-ban-circle"></i>商品发货</a>
																</c:if>
															</c:otherwise>
														</c:choose>
                                                    </c:when>
                                                    <c:when test="${order.status==3 }">
                                                        <a class="ncbtn ncbtn-grapefruit" onclick="changeDeliverNum('${order.subNum}');">
                                                            <i class="icon-ban-circle"></i>修改物流单号</a>
                                                    </c:when>
                                                </c:choose>
                                            </c:if>
                                            <c:if test="${order.refundState == 1 }">
                                              	  <span class="col-orange">退款退货中</span>
                                            </c:if>
                                        </p>
                                        <!-- 退款取消订单 -->
                                        <!-- 收货 -->
                                        <!-- 评价 -->
                                        <!-- 已经评价 -->
                                        <!-- 分享 -->
                                        <!-- <p>
                                        <a href="javascript:void(0)">分享商品</a>
                                        </p> -->
                                        </td>

                                    </c:if>
                                    <!-- E 合并TD -->
                                </tr>

                            </c:forEach>


                            <!-- S 赠品列表 -->
                            <!-- <tr class="bd-line">
                                <td>&nbsp;</td>
                                <td class="tl" colspan="7"><div class="ncm-goods-gift">
                                        <strong>赠品：</strong>
                                        <ul>
                                            <li><a
                                                href="#"
                                                title="赠品：【单拍无效】德国凯驰官方旗舰店 擦车洗车毛巾赠品 160cm*60cm * 1"
                                                target="_blank"><img onmouseout="toolTip()"
                                                    onmouseover="toolTip('&lt;img src=&gt;')"
                                                    src="#">
                                            </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr> -->
                            <!-- E 赠品列表 -->

                            </tbody>
                            <tfoot>

                            <!-- 	<tr>
                                    <th colspan="20">
                                        <dl class="nc-store-sales">
                                            <dt>满减</dt>
                                            <dd>
                                              	  满500减30 送<a target="_blank"
                                                    title="【单拍无效】德国凯驰官方旗舰店 擦车洗车毛巾赠品 160cm*60cm"
                                                    href="#">[赠品]</a><a
                                                    href="#"
                                                    target="_blank" title="【单拍无效】德国凯驰官方旗舰店 擦车洗车毛巾赠品 160cm*60cm"
                                                    class="ncc-store-gift"><img
                                                    src="#"
                                                    alt="【单拍无效】德国凯驰官方旗舰店 擦车洗车毛巾赠品 160cm*60cm">
                                                </a>
                                            </dd>
                                        </dl></th>
                                </tr> -->


                            <tr>
                                <td colspan="20">
                                    <dl class="freight">

                                        <%-- <dd>
                                        <c:choose>
                                             <c:when test="${order.dvyType == 'express'}">
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  快递:
                                             </c:when>
                                              <c:when test="${order.dvyType == 'ems'}">
                                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  EMS:
                                             </c:when>
                                         </c:choose>
                                        </dd>  --%>
                                        <dd>
                                          	   商品总金额：¥<fmt:formatNumber value="${order.total}" type="currency" pattern="0.00"/><br>
											 -满减金额：¥${order.discountPrice}<br>
											 -优惠券金额：¥ ${order.couponOffPrice}<br>
											 -红包金额：¥ ${order.redpackOffPrice}<br>
                                            <c:choose>
                                                <c:when test="${empty order.freightAmount || order.freightAmount==0}">
                                                  	  免运费<br>
                                                </c:when>
                                                <c:otherwise>
												      +运费：¥${order.freightAmount}(
												      <c:choose>
		                                                    <c:when test="${order.dvyType == 'express'}">
		                                                      	快递
		                                                    </c:when>
		                                                    <c:when test="${order.dvyType == 'ems'}">
		                                                        EMS
		                                                    </c:when>
                                                	  </c:choose>)<br>
                                                </c:otherwise>
                                            </c:choose>
                                            <c:if test="${not empty order.changedPrice && order.changedPrice!=0}">
												<c:choose>
													<c:when test="${order.changedPrice lt 0}">
														<dl><dd>商家改价： ${order.changedPrice}</dd></dl>
													</c:when>
													<c:otherwise>
														<dl><dd>商家改价：+${order.changedPrice}</dd></dl>
													</c:otherwise>
												</c:choose>
												
											</c:if>
                                        </dd>
                                    </dl>
                                    <dl class="sum">
                                        <dt></dt>
                                        <dd>
                                        	订单应付金额：
                                            <em><fmt:formatNumber pattern="0.00" value="${order.actualTotal}"/></em>元
                                        </dd>
                                    </dl>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>

    </div>
    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/ToolTip.js'/>"></script>
<script src="<ls:templateResource item='/resources/templets/js/orderManage.js'/>" type="text/javascript"></script>
<script type="text/javascript">
    var contextPath = "${contextPath}";
    var dvyFlowId="${order.delivery.dvyFlowId}";
    var dvyTypeId="${order.delivery.dvyTypeId}";

    // 增值税发票信息
    var company = "${order.invoiceSub.company}";
    var invoiceHumNumber = "${order.invoiceSub.invoiceHumNumber}";
    var registerAddr = "${order.invoiceSub.registerAddr}";
    var registerPhone = "${order.invoiceSub.registerPhone}";
    var depositBank = "${order.invoiceSub.depositBank}";
    var bankAccountNum = "${order.invoiceSub.bankAccountNum}";

    $.ajax({
        url: contextPath + "/s/findSubHisInfo",
        data: {"subId": "${order.subId}"},
        type: 'POST',
        async: false, //默认为true 异步   
        dataType: 'json',
        error: function (jqXHR, textStatus, errorThrown) {
            return;
        },
        success: function (result) {
            if (result == "fail" || result == "") {
                $("#history").after("");
            } else {
                var list = eval("(" + result + ")");
                var html = "<ul>";
                for (var obj in list) { //第二层循环取list中的对象
                    html += "<li>" + list[obj].value + "</li>";
                }
                html += "</ul>";
                $("#history").html(html);
            }
        }
    });
</script>
</body>
</html>