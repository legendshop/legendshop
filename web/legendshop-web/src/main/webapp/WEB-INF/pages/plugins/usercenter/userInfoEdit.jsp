<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>个人资料 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<style>
		.formbox input.textinput{
			width: 315px !important;
		}
		.w135{
			width: 135px !important;
		}
		.formbox .phoneContainer2 select {
		    margin-right: 12px;
		    border-color: #ddd;
		}
	</style>
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con">
     
         <div class="o-mt"><h2>账户信息</h2></div>
         
         <div class="pagetab2">
                 <ul>           
              <li id="userInfo" class="on"><span>基本信息</span></li>      
                 </ul>
         </div>
         
         <div class="formbox m10" style="position:relative;">
         		<input type="hidden" id="id" value="${MyPersonInfo.id}"> 
         		<input type="hidden" id="user_name" value="${MyPersonInfo.userName}">
             <div class="item2">
                <span class="itemName">用户名：</span>
        
                <div class="fl phoneContainer2"><span>${MyPersonInfo.userName}</span>  </div>   
           	 </div>
             
             <div class="item">
                <span class="itemName">昵称：</span>
        
                <div class="fl yzm2">
                    <input type="text" name="nick_name" id="petName" maxlength="20" class="textinput w326" value="${MyPersonInfo.nickName}">
                    <div class="prompt-06">
						<span id="petName_msg"></span>
					</div>
                </div>   
           	 </div>
             
             
             <div class="item2">
                <span class="itemName">性别：</span>
                <div class="fl phoneContainer2">
					<label class="radio-wrapper <c:if test="${MyPersonInfo.sex=='M'}">radio-wrapper-checked</c:if>">
						<span class="radio-item">
							<input type="radio" class="radio-input" value="M" name="sex"><label>男</label>&nbsp;
							<span class="radio-inner"></span>
						</span>
					</label>
					<label class="radio-wrapper <c:if test="${MyPersonInfo.sex=='F'}">radio-wrapper-checked</c:if>">
						<span class="radio-item">
							<input type="radio" class="radio-input" value="F" name="sex"><label>女</label>&nbsp;
							<span class="radio-inner"></span>
						</span>
					</label>
					<label class="radio-wrapper <c:if test="${empty MyPersonInfo.sex}">radio-wrapper-checked</c:if>">
						<span class="radio-item">
							<input type="radio" class="radio-input" value="" name="sex"><label>保密</label>&nbsp;
							<span class="radio-inner"></span>
						</span>
					</label>
				</div>   
           	 </div>
             
             
             <div class="item2">
                <span class="itemName">手机号码：</span>
        
                <div class="fl phoneContainer2"><span>${empty MyPersonInfo.userMobile?'未绑定手机':MyPersonInfo.userMobile}<a href="${contextPath}/p/security" target="_blank">修改</a></span> </div>   
           	 </div>
             
             
             <div class="item2">
                <span class="itemName">邮箱：</span>
        
                <div class="fl phoneContainer2"><span>${empty MyPersonInfo.userMail?'未绑定邮箱':MyPersonInfo.userMail}<a href="${contextPath}/p/security" target="_blank">修改</a></span> </div>   
           	 </div>
             
             
             <div class="item">
                <span class="itemName">真实姓名：</span>
        
                <div class="fl yzm2">
                    <input type="text" name="real_name" id="real_name" class="textinput w326" maxlength="20" value="${MyPersonInfo.realName}">
                    <div class="prompt-06">
						<span id="userName_msg"></span>
					</div>
                </div>   
           	 </div>
             
             
             <div class="item">
                <span class="itemName">身份证号码：</span>
        
                <div class="fl yzm2">
                    <input type="text" name="id_card" id="id_card" class="textinput w326" maxlength="18" value="${MyPersonInfo.idCard}">
                    <div class="prompt-06">
						<span id="ucid_msg"></span>
					</div>
                </div>   
           	 </div>
             
             
             <div class="item2">
                <span class="itemName">所在地：</span>
        
                <div class="fl phoneContainer2">
					<span>
						<select class="combox sele w135" id="provinceid" name="provinceid"
										requiredTitle="true" childNode="cityid"
										selectedValue="${MyPersonInfo.provinceid}"
										retUrl="${contextPath}/common/loadProvinces">
						</select>
						<select class="combox sele" id="cityid" name="cityid"
										requiredTitle="true" selectedValue="${MyPersonInfo.cityid}"
										showNone="false" parentValue="${MyPersonInfo.provinceid}"
										childNode="areaid"
										retUrl="${contextPath}/common/loadCities/{value}">
						</select>
						<select class="combox sele" id="areaid" name="areaid"
										requiredTitle="true" selectedValue="${MyPersonInfo.areaid}"
										showNone="false" parentValue="${MyPersonInfo.cityid}"
										retUrl="${contextPath}/common/loadAreas/{value}">
						</select>
					</span>
								<div class="prompt-06">
									<span id="province_msg"></span>
								</div>
					</div>   
           	 </div>
             
             <div class="item">
                <span class="itemName">详细地址：</span>
        
                <div class="fl yzm2">
                    <input type="text" id="user_adds" class="textinput w326" maxlength="200" value="${MyPersonInfo.userAdds}">
                    <div class="prompt-06">
						<span id="uaddr_msg"></span>
					</div>
                </div>   
           	 </div>
             
             
             <div class="item">
                <span class="itemName">QQ：</span>
        
                <div class="fl yzm2">
                    <input type="text" id="user_qq" class="textinput w326" maxlength="15" value="${MyPersonInfo.qq}">
                </div>   
           	 </div>
             
             
             <div class="item2" >
                <span class="itemName">生日：</span>
                <div class="fl phoneContainer2">

			        <div class="layui-input-inline"  >
			        <input type="text" style="border: 1px solid #ddd;padding-left:5px;height: 25px" class="layui-input" name="birthDate" id="birthDate" placeholder="年-月-日"
			        value="<fmt:formatDate
								value="${MyPersonInfo.birthDate}" type="both" dateStyle="long"
								pattern="yyyy-MM-dd" />">
			      </div>
			      </div>
                <%-- <div class="fl phoneContainer2"><span>
                	<select id="birthdayYear" name="" class="sele"></select><label>年</label>
					<select id="birthdayMonth" name="" class="sele"></select><label>月</label>
					<select id="birthdayDay" name="" class="sele"></select><label>日</label>
					<input type="hidden" value="${MyPersonInfo.birthDate}"
								id="birth_date">
                </span></div>   --%> 
                
           	 </div>
             
             
             <div class="item2">
                <span class="itemName">婚姻状况：</span>
        
                <div class="fl phoneContainer2">
						<label class="radio-wrapper <c:if test="${MyPersonInfo.marryStatus==1}">radio-wrapper-checked</c:if>">
						<span class="radio-item">
							<input type="radio" class="radio-input" value="1" name="marryStatus"><label>未婚</label>&nbsp;
							<span class="radio-inner"></span>
						</span>
						</label>
						<label class="radio-wrapper <c:if test="${MyPersonInfo.marryStatus==2}">radio-wrapper-checked</c:if>">
							<span class="radio-item">
								<input type="radio" class="radio-input" value="2" name="marryStatus"><label>已婚</label>&nbsp;
								<span class="radio-inner"></span>
							</span>
						</label>
						<label class="radio-wrapper <c:if test="${MyPersonInfo.marryStatus==3}">radio-wrapper-checked</c:if>">
							<span class="radio-item">
								<input type="radio" class="radio-input" value="3" name="marryStatus"><label>保密</label>&nbsp;
								<span class="radio-inner"></span>
							</span>
						</label>
                </div>   
           	 </div>
             
              <div class="item2">
                <span class="itemName">月收入：</span>
        
                <div class="fl phoneContainer2">
					<select id="incomeLevel" name="incomeLevel" class="sele w135" style="height: 25px;">
						<ls:optionGroup type="select" required="true" cache="true"
							beanName="INCOME_LEVEL"
							selectedValue="${MyPersonInfo.incomeLevel}" />
					</select>
					
				</div>   
           	 </div>
             
             
             <div class="item">
                <span class="itemName">兴趣爱好：</span>
        
                <div class="">
                  <textarea name="interest" id="interest" class="textarea_t" maxlength="100" style="margin-left:30px;">${MyPersonInfo.interest}</textarea>
                </div>   
           	 </div>
             
             
             
             <div class="item" id="submitItem">
                <span class="itemName"></span>
                <div>
                    <input onclick="updateUserInfo()" type="button" class="btn-r small-btn" value="保存" clstag="pageclick|keycount|new_201511197|1"  style="margin-left:130px;margin-right:-15px;">
                    <input type="button" class="btn-r small-btn" value="返回" onclick="window.history.back();" style="margin-left:20px;">
                </div>
            </div>
            
            <div class="userinfo-right-extra">
				<div class="m">

					<div class="mc">
						<div class="headpic">
							<div class="fore"></div>
							<div class="i-pic">
								<c:choose>
									<c:when test="${not empty MyPersonInfo.portraitPic }">
										<img alt="用户头像"
											src="<ls:photo item='${MyPersonInfo.portraitPic}' />"
											id="infoimg" style="width: 100px;height :100px;">
									</c:when>
									<c:otherwise>
										<img alt="用户头像"
											src="${contextPath}/resources/templets/images/no-img_mid_.jpg"
											id="infoimg" style="width: 100px;height :100px;">
									</c:otherwise>
								</c:choose>
							</div>
							<div class="fore1"></div>
						</div>
						<div class="btns">
							<a title="头像上传" id="uploadthickbox" href="javascript:void(0);">修改头像</a>
							<div id="messages"
								style="border:1px solid #DB9A9A;background-color:#FFE8E8;color:#CC0000;text-align: left;display: none">审核未通过，请重新上传头像</div>
						</div>
					</div>

				</div>
			</div>
            
         </div>
       
     </div>
    <!-----------right_con end-------------->
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/idcardCheck.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/userinfo.js'/>"></script>
<script type="text/javascript">
	var contextPath = '${contextPath}';
	var originalPetName ='${MyPersonInfo.nickName}';
	$(document).ready(function() {
		initUserInfo();
	});
	
	$("input:radio[name='sex']").change(function(){
		 $("input:radio[name='sex']").each(function(){
			 if(this.checked){
				 $(this).parent().parent().addClass("radio-wrapper-checked");
			 }else{
				 $(this).parent().parent().removeClass("radio-wrapper-checked");
			 }
	 	});
	 });
	$("input:radio[name='marryStatus']").change(function(){
		 $("input:radio[name='marryStatus']").each(function(){
			 if(this.checked){
				 $(this).parent().parent().addClass("radio-wrapper-checked");
			 }else{
				 $(this).parent().parent().removeClass("radio-wrapper-checked");
			 }
	 	});
	 });
	 
</script>
</body>
</html>