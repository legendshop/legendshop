<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-prod${_style_}.css'/>" rel="stylesheet">
<style type="text/css">
	.informButton {margin: 0 auto;}
</style>
	<form:form action="${contextPath}/s/prod/selectArrival" method="post" id="arrivalForm">
		<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
		<input type="hidden" id="skuId" name="skuId" value="${skuId}"/>
	</form:form>

	<table class="selling-table" style="width:700px;margin-left:10px;">
		<tr class="selling-tit">
			<td>用户名称</td>
			<td>手机号码</td>
			<td>邮箱</td>
			<td>添加时间</td>
		</tr>
		<c:if test="${empty requestScope.list}">
			<tr><td colspan='7' height="60">没有找到符合条件的商品</td></tr>
		</c:if>				
		<c:forEach items="${requestScope.list}" var="product" varStatus="status">
			<tr class="detail">
				<input type="hidden" name="prodArrivalInformId" value="${product.prodArrivalInformId}"/>
				<input type="hidden" name="skuId" value="${product.skuId}"/>
				<td>${product.userName}</td>
				<td>${product.mobilePhone}</td>
				<td>
				<c:choose>
					<c:when test="${empty product.email}">
						-
					</c:when>
					<c:otherwise>
						${product.email}
					</c:otherwise>
				</c:choose>
				</td>
				<td><fmt:formatDate value="${product.createTime}" pattern="yyyy-MM-dd" /></td>
			</tr>
		</c:forEach>
	</table>
	<div style="margin-top:10px;" class="page clearfix">
   		<div class="p-wrap">
          	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
   		</div>
	</div>
<div class="clear"></div>
<script type="text/javascript">
	var contextPath="${contextPath}";
	function pager(curPageNO){
        document.getElementById("curPageNO").value=curPageNO;
        document.getElementById("arrivalForm").submit();
    }
    
    function selectArr(skuId){
    
    }
</script>
