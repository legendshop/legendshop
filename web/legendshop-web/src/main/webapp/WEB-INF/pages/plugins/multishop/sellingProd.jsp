<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>出售中的商品-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-prod${_style_}.css'/>" rel="stylesheet">
</head>
<body class="graybody">
<div id="doc">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
    <div class="w1190">
        <div class="seller-cru">
            <p>
                                      您的位置>
                <a href="${contextPath}/home">首页</a>>
                <a href="${contextPath}/sellerHome">卖家中心</a>>
                <span class="on">出售中的商品</span>
            </p>
        </div>
        <%@ include file="included/sellerLeft.jsp" %>
        <div class="seller-selling">
        	<div class="pagetab2">
				<ul id="listType">
					<li class="on"><span>出售中的商品</span></li>
				</ul>
   			</div>
            <form:form action="${contextPath}/s/sellingProd" method="post" id="from1">
                <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
                <div class="sold-ser sold-ser-no-bg clearfix">
                	<div class="fr">
                	
	                	<div class="item">
		                                                    商品名称：
		                    <input type="text" class="item-inp" value="${prod.name}" style="width: 300px;" name="name" id="name">
	                	</div>
	                	<div class="item" style="margin:0px">
		                   	 商品分类：
							<input type="text" class="item-inp" style="cursor: pointer;width: 240px;" id="prodCatName" name="categoryName" value="${prod.categoryName }" onclick="loadCategoryDialog();" placeholder="商品分类" readonly="readonly" />
							<input type="hidden" id="shopFirstCatId" name="shopFirstCatId" value="${prod.shopFirstCatId }" />
							<input type="hidden" id="shopSecondCatId" name="shopSecondCatId" value="${prod.shopSecondCatId }" />
							<input type="hidden" id="shopThirdCatId" name="shopThirdCatId" value="${prod.shopThirdCatId }" />
		                    <input type="submit" class="serach btn-r" id="btn_keyword" value="搜  索">
	                	</div>
	                	<div class="item" style="margin-right:20px">
		                       <a type="submit" class="btn-r" onclick="batchEdit()"   id="edits" >批量修改店铺类目</a>
	                	</div>
                	</div>
                </div>
            </form:form>
            <!-- 搜索 -->
            <table class="selling-table sold-table">
                <tr class="selling-tit" style="font-size: 13px;">
                    <th class="check">
                    	<label class="checkbox-wrapper" style="float:left;margin-left:9px;">
							<span class="checkbox-item">
								<input type="checkbox" id="checkbox" class="checkbox-input selectAll"/>
								<span class="checkbox-inner" style="margin-left:9px;"></span>
							</span>
					   </label>
                    </th>
                    <th>商品</th>
                    <th width="370">名称</th>
                    <th>价格</th>
                    <th>实际库存</th>
                    <th>销售库存</th>
                    <th style="min-width: 50px;">订购数</th>
                    <th style="border-right: 1px solid #e4e4e4">操作</th>
                </tr>
                <c:choose>
		           <c:when test="${empty requestScope.list}">
		              <tr>
					 	 <td colspan="20" style="border-right: 1px solid #e4e4e4">
					 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
					 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
					 	 </td>
					 </tr>
		           </c:when>
		           <c:otherwise>
		               <c:forEach items="${requestScope.list}" var="product" varStatus="status">
		                    <tr class="detail">
		                        <td class="check">
		                            <c:choose>
		                                <c:when test="${product.prodType eq 'P'}">
		                                    <label class="checkbox-wrapper" style="float:left;margin:9px;">
												<span class="checkbox-item">
													<input type="checkbox" name="array" class="checkbox-input selectOne" value="${product.prodId}" arg="${product.name}" onclick="selectOne(this);"/>
													<span class="checkbox-inner" style="margin:9px;"></span>
												</span>
										   	</label>
		                                </c:when>
		                                <c:otherwise>
		                                    <label class="checkbox-wrapper" style="float:left;margin:9px;">
												<span class="checkbox-item">
													<input type="checkbox" disabled="disabled" name="array" class="checkbox-input selectOne" value="${product.prodId}" arg="${product.name}" onclick="selectOne(this);"/>
													<span class="checkbox-inner" style="margin:9px;"></span>
												</span>
										   	</label>
		                                </c:otherwise>
		                            </c:choose>
		                        </td>
		                        <td>
		                            <a href="${contextPath}/views/${product.prodId}" target="_blank"><img src="<ls:images scale="3" item='${product.pic}'/>"></a>
		                        </td>
		                        <td><a href="${contextPath}/views/${product.prodId}" style="display: block; width: 350px; text-align: center;padding: 0 10px;">${product.name}</a></td>
		                        <td>¥<fmt:formatNumber value="${product.cash}" type="currency" pattern="0.00" /></td>
		                        <td>
		                            <c:choose>
		                                <c:when test="${product.stocksArm ne 0 && (product.actualStocks le product.stocksArm) }">
		                                    <span style="color: red;font-weight: bold;font-size: 15px;">${product.actualStocks}</span>
		                                </c:when>
		                                <c:otherwise>
		                                    ${product.actualStocks}
		                                </c:otherwise>
		                            </c:choose>
		                        </td>
		                        <td>${product.stocks}</td>
		
		                        <td>${product.buys}</td>
		                        <td>
	                                <a href="javascript:void(0);" onclick="pullOff(this);" productId="${product.prodId}" productName="${product.name}" status="${product.status}" title="下线" class="btn-r">下线</a> 
                                    <a href="javascript:void(0);" title="修改"  class="btn-g" onclick="updateProd(${product.prodId})">修改</a>
                                    <a href="javascript:void(0);" onclick="updateStotcks(${product.prodId});" title="库存" class="btn-g">库存</a>
		                        </td>
		                    </tr>
		                </c:forEach>
		           </c:otherwise>
			     </c:choose>
            </table>
            <div class="page clearfix">
            	<a class="btn-r" style="margin-top:2px;" href="javascript:void(0);" onclick="batchOffline();">批量下线</a>
                <div class="p-wrap">
                    <ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="simple"/>
                </div>
            </div>
        </div>
    </div>
    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/sellingProd.js'/>"></script>
<script type="text/javascript">
    var contextPath = "${contextPath}";
</script>
</body>
</html>