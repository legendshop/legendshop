<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>商家首页-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>"
          rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/public.css'/>"
          rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/store_black.css'/>"
          rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/decorate.css'/>"
          rel="stylesheet"/>
    <%@ include file="/WEB-INF/pages/common/jquery.jsp" %>
</head>
<style>
    .main {
        width: 1190px !important;
    }

    .shopindex_right {
        width: 985px !important;
    }

    .store_goods_list {
        border: none !important;
        width: auto;
    }
</style>

<body class="graybody">
<div id="doc">

    <!--顶部menu-->
    <div id="hd">
        <!--hdtop 开始-->
        <%@ include file="/WEB-INF/pages/plugins/main/home/header.jsp" %>
        <!--hdtop 结束-->

        <!-- hdmain 开始-->
        <%@include file="/WEB-INF/pages/plugins/main/home/hdShopMain.jsp" %>
        <!-- hdmain 结束-->
    </div>


    <div id="bd">
        <div style='
        <c:if test="${not empty shopDecotate.bgImgId}">
                background-image:url("<ls:photo item='${shopDecotate.bgImgId}'/>");
        </c:if>
        <c:if test="${not empty shopDecotate.bgColor}">
                background-color:${ shopDecotate.bgColor };
        </c:if>
        <c:if test="${not empty shopDecotate.bgStyle}">
                background-repeat:${ shopDecotate.bgStyle };
                </c:if>'
             class="shop_main">

            <div class="store_head" style="margin-top: 10px;">
                <div class="store_top">
                    <div class="store_main">
                        <div class="store_head_right">
                            <div id="store_head_box" class="store_head_box">
                                <a href="javascript:void(0);">
                                    <div class="store_name">${shopDecotate.shopDefaultInfo.siteName}</div>
                                </a>
                                <div class="shopscores">
                                    <%--											<i class="smill"></i><b class="scores_scroll"> --%>
                                    <%--											<span class="shop_scroll_gray"> </span>--%>
                                    <%--												 <span class="scroll_red"></span> </b>--%>
                                    <li><span style="float:left;"></span> <span class="scores_scroll"> <span
                                            class="scroll_gray"></span> <span class="scroll_red"
                                                                              style="width: 20%"></span> </span><font
                                            id="shopScoreNum" class="hide">${sum}</font></li>
                                </div>
                            </div>
                            <%--			                    <div class="scroll_scroll">${shopDecotate.shopDefaultInfo.credit}分</div>--%>
                            <div id="store_level" style="display:none" class="store_level">
                                <ul style="display:;" class="pro_shop_date">
                                    <li><span style="float:left;">综合评分：</span> <span class="scores_scroll"> <span
                                            class="scroll_gray"></span> <span class="scroll_red"
                                                                              style="width: 20%"></span> </span><font
                                            id="shopScoreNum" class="hide">${sum}</font></li>
                                </ul>
                                <div class="store_detail">
                                    <ul>
                                        <li><%--<i class="value_normal">-----</i>--%><span
                                                class="company_name">店铺名称：</span><span
                                                class="red">${empty shopDecotate.shopDefaultInfo.siteName?'暂无':shopDecotate.shopDefaultInfo.siteName}</span>
                                        </li>
                                        <!--
											<li><span class="company_name">联系电话：</span><span
												class="company_r">${shopDecotate.shopDefaultInfo.contactMobile}</span>
											</li>
											 -->
                                        <%-- <li><span class="company_name">店铺地址：</span><span
                                            class="company_r">${shopDecotate.shopDefaultInfo.shopAddr}</span>
                                        </li> --%>
                                        <li><span class="company_name">店铺地址：</span><span
                                                class="company_r">${empty shopDecotate.shopDefaultInfo.shopAddr?'暂无':shopDecotate.shopDefaultInfo.shopAddr}</span>
                                        </li>
                                        <li><span class="company_name">店铺简述：</span><span
                                                class="company_r">${empty shopDecotate.shopDefaultInfo.briefDesc?'暂无':shopDecotate.shopDefaultInfo.briefDesc}</span>
                                        </li>
                                        <li>
                                            <span id="clickMessage"></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <%-- <div class="service"><a  href="http://wpa.qq.com/msgrd?v=3&uin=${shopDecotate.shopDefaultInfo.qq}&site=legendshop&menu=yes" target="_blank"></a></div> --%>
                            <div class="service"><a target="_blank"
                                                    href="${contextPath}/p/im/customerShop?shopId=${shopDecotate.shopDefaultInfo.shopId}"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--logo行-->


            <!--导航-->
            <div class="store_nav_width">
                <div class="main clearfix">
                    <div class="store_nav">
                        <ul>
                            <li><a href="${contextPath}/store/${shopDecotate.shopId}">商家主页</a></li>
                            <c:forEach items="${shopDecotate.shopDefaultNavs}" var="nav" varStatus="s">
                                <li><a
                                        <c:if test="${nav.winType=='new_win'}">target="_blank"
                                </c:if> href="${nav.url}">${nav.title}</a>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                </div>
            </div>


            <div class="main clearfix" style="margin-top:10px;">
                <div class="store_left">
                    <!--店铺信息-->
                    <div class="store_left_box">
                        <h1 style="color:; background-color:">店铺信息</h1>
                        <div style="background-color:" class="store_infor">
                            <span class="full_name"
                                  style="text-overflow: ellipsis;overflow: hidden;white-space: nowrap;">${shopDecotate.shopDefaultInfo.siteName}</span>
                            <ul style="display:;" class="pro_shop_date_c">
                                <li><span style="float:left;">综合评分：</span> <span class="scores_scroll"> <span
                                        class="scroll_gray"></span> <span class="scroll_red" style="width: 20%"></span> </span><font
                                        id="shopScoreNum" class="hide">${sum}</font></li>
                            </ul>
                            <ul class="information">
                                <li>
                                    <span>店铺名称：</span><b>${empty shopDecotate.shopDefaultInfo.siteName?'暂无':shopDecotate.shopDefaultInfo.siteName}</b>
                                </li>
                                <li>
                                    <span>所在地：</span><b>${empty shopDecotate.shopDefaultInfo.shopAddr?'暂无':shopDecotate.shopDefaultInfo.shopAddr}</b>
                                </li>
                                <!--  要显示平台的电话
								<li><span>联系电话：</span><b>${shopDecotate.shopDefaultInfo.contactMobile}</b>
								</li>
								 -->
                                <li>
                                    <span>店铺简述：</span><b>${empty shopDecotate.shopDefaultInfo.briefDesc?'暂无':shopDecotate.shopDefaultInfo.briefDesc}</b>
                                </li>
                            </ul>
                            <!--
                            <div class="shop_botm_hid">
                                <ul class="shopboh">
                                    <li style="border-right:0px;" class="collection"><a
                                        id="store_fav" href="javascript:void(0);"><span>收藏店铺</span>
                                    </a></li>
                                </ul>
                            </div>
                             -->
                        </div>
                    </div>


                    <!--商品分类-->
                    <%--  <c:if test="${not empty shopDecotate.cateogryDtos}">
                        <div class="classify">
                        <h1><span>商品分类</span></h1>
                           <div class="classify_list">
                                <c:forEach items="${shopDecotate.cateogryDtos}" var="category" varStatus="s">
                                        <div class="level_li">
                                        <div style="background-color:" class="level_one">
                                            <i class="i_cut" id="i_10">
                                            </i>
                                            <span style="cursor:pointer" <c:if test="${not empty category.childrentDtos}"> child_show="true"</c:if> id="span_${category.id}">
                                            <a onclick="fireCat1(${category.id});"  href="javascript:void(0);" >${category.name}</a>
                                            </span>
                                        </div>
                                           <c:if test="${not empty category.childrentDtos}">
                                            <div class="level_second">
                                              <ul>
                                                 <c:forEach items="${category.childrentDtos}" var="chilren" varStatus="c">
                                                  <li>
                                                      <a onclick="secondCat1(${chilren.id});"   href="javascript:void(0);" >${chilren.name}</a></li>
                                                       <c:if test="${not empty chilren.childrentDtos}">
                                                         <div class="level_third">
                                                              <ul>
                                                                  <c:forEach items="${chilren.childrentDtos}" var="chilren2" varStatus="c2">
                                                                       <li><a onclick="thirdCat1(${chilren2.id});"   href="javascript:void(0);" >${chilren2.name}</a></li>
                                                                  </c:forEach>
                                                              </ul>
                                                          </div>
                                                       </c:if>

                                                 </c:forEach>
                                               </ul>
                                            </div>
                                        </c:if>
                                       </div>
                              </c:forEach>
                          </div>
                        </div>
                     </c:if> --%>


                    <!--商品分类-->
                    <c:if test="${not empty shopDecotate.cateogryDtos}">
                        <div class="classify">
                            <h1><span>商品分类</span></h1>
                            <div class="classify_list">
                                <c:forEach items="${shopDecotate.cateogryDtos}" var="category" varStatus="s">
                                    <div class="level_li">
                                        <div style="background-color:" class="level_one">
                                            <i class="i_cut" id="i_one_${category.id}" child_show="true">
                                            </i>
                                            <span style="cursor:pointer" <c:if
                                                    test="${not empty category.childrentDtos}"> child_show="true"</c:if>
                                                  id="span_${category.id}">
											   <a href="<ls:url address='/store/list/${shopDecotate.shopId}?fireCat=${category.id}'/>"
                                                  class="cat_${category.id}" cat-level="one">${category.name}</a>
											</span>
                                        </div>
                                        <c:if test="${not empty category.childrentDtos}">
                                            <div class="level_second">
                                                <ul>
                                                    <c:forEach items="${category.childrentDtos}" var="chilren"
                                                               varStatus="c">
                                                        <li>
                                                            <i class="i_cut" id="i_two_${chilren.id}"
                                                               child_show="true"></i>
                                                            <a href="<ls:url address='/store/list/${shopDecotate.shopId}?secondCat=${chilren.id}'/>"
                                                               class="cat_${chilren.id}"
                                                               cat-level="two">${chilren.name}</a></li>
                                                        <c:if test="${not empty chilren.childrentDtos}">
                                                            <div class="level_third">
                                                                <ul>
                                                                    <c:forEach items="${chilren.childrentDtos}"
                                                                               var="chilren2" varStatus="c2">
                                                                        <li>
                                                                            <a href="<ls:url address='/store/list/${shopDecotate.shopId}?thirdCat=${chilren2.id}'/>"
                                                                               class="cat_${chilren2.id}"
                                                                               cat-level="third">${chilren2.name}</a>
                                                                        </li>
                                                                    </c:forEach>
                                                                </ul>
                                                            </div>
                                                        </c:if>
                                                    </c:forEach>
                                                </ul>
                                            </div>
                                        </c:if>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </c:if>


                    <!--热销排行-->
                    <c:if test="${not empty shopDecotate.hotProds}">
                        <div class="classify">
                            <h1><span>热销排行</span></h1>
                            <div class="hot_rank">
                                <c:forEach items="${shopDecotate.hotProds}" var="prod" varStatus="s">
                                    <ul class="hot_rank_ul">
                                        <li class="hot_img"><span style="text-align:center;"><a target="_blank"
                                                                                                href="<ls:url address="/views/${prod.prodId}" />"><img
                                                style="max-width:70px;max-height:70px;"
                                                src="<ls:images item='${prod.pic}' scale='3'/>">
											</a>
										</span><i>${s.count}</i>
                                        </li>
                                        <li class="hot_name"><b><a target="_blank"
                                                                   href="<ls:url address="/views/${prod.prodId}" />">${prod.prodName}</a>
                                        </b><span><strong>¥${prod.cash}</strong>
										</span>
                                        </li>
                                    </ul>
                                </c:forEach>
                            </div>
                        </div>
                    </c:if>

                </div>


                <div class="shopindex_right">
                    <div class="nav_content" id="store_prod_list">

                    </div>
                    <div style="clear: both;"></div>
                </div>


            </div>
        </div>
    </div>
</div>

<input type="hidden" id="catagoryType1" value="">
<input type="hidden" id="catagoryType2" value="">
<input type="hidden" id="catagoryType3" value="">
<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>

</div>
</body>


<!--返回最顶部-->
<div style="position: fixed; width: 40px; height: 118px; float: left; right: 20px; bottom: 150px; display: block;z-index:1000;"
     id="back_box">
    <div class="back_index"><a title="返回主页" target="_blank" href="${contextPath}"></a></div>
    <div class="back_top"><a target="_self" title="返回顶部" style="display: block;" bosszone="hometop" href="#"
                             id="toTop"></a></div>
</div>

<script type="text/javascript">
    var contextPath = '${contextPath}';
    var _shopId = '${shopDecotate.shopId}';
    var soc = "${shopDecotate.shopDefaultInfo.credit}";
    var fireCat = "${fireCat}";
    var secondCat = "${secondCat}";
    var thirdCat = "${thirdCat}";
    var keyword = "${keyword}";
    var order = "${order}";
    var shopType = null;
    $(function () {
        $.each($(".scroll_red"), function () {
            var width = parseInt($(this).parents("li").find("font").text()) * 2 + "0%";
            $(this).css("width", width);
        })
        $(".scroll_red").css("top", "4px");
        $(".scroll_gray").css("top", "4px");
    })
    //营业执照信息跳转
    $(function () {
        var data = {
            "shopId": _shopId
        };
        jQuery.ajax({
            url: contextPath + "/productcomment/business/flag",
            data: data,
            type: "GET",
            async: true, //默认为true 异步
            success: function (result) {
                shopType = result;
                if (result == 1){
                    $("#clickMessage").html("注：点击可查看商家证照信息");
                }
            }
        });

        $(".store_name").click(function () {
            if (shopType == 1) {
                var url = contextPath + "/productcomment/business/message?shopId=" + _shopId;
                location.href = url;
            }
        });
    });

</script>
<script src="<ls:templateResource item='/resources/templets/js/multishop/jquery.SuperSlide.2.1.1.js'/>"
        type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/templets/js/multishop/storeIndex.js'/>"
        type="text/javascript"></script>
<%--<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/score.js'/>"></script>--%>
</html>



