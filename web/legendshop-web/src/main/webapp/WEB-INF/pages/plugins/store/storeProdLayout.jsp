<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<head>
<style type="text/css">
*{
	margin:0;
	padding:0;
}
body {
	font-size: 13px;
}

a:hover {
	background-color: #d5e4fa;
}

.serachProd {
/* 	border: 1px solid #e4e4e4; */
	width: 100%;
	height: 190px;
	text-align: center;
}

.serachProd h4{
	width:1000px;
 	padding:10px 0;
 	border: 1px solid #e4e4e4;
 	color:#5D5C5C;
}

.prod_list {
	width: 1000px;
	float: left;
	overflow: hidden;
	margin: 10px auto;
	font-size: 13px;
}

.prod_list li {
	width: 89px;
	float: left;
	display: inline;
	position: relative;
	border: 1px solid #ddd;
	margin-top: 5px;
	margin-right: 10px;
	margin-left: 10px;
}

.prod {
	width: 80px;
	padding: 5px;
	overflow: hidden;
	display: block;
}

.prod_name {
	float: left;
	width: 80px;
	height: 20px;
	line-height: 20px;
	margin-top: 5px;
	color: #000;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
	color:#333;
}

.prod_page {
	width: 978px;
	margin-bottom:10px;
	float: left;
	text-align: center;
    padding: 10px;
    border: 1px solid #e4e4e4;
}

.prod_page a {
	color: #666;
	background: #f1f1f1;
	border: 1px solid #ccc;
	padding: 2px 5px;
	margin-right: 7px;
	text-decoration: none;
}

.prod_page a:hover {
	color: #fff;
	background: #1283C9;
	border: 1px solid #2593FF
}

.prod_page a em {
	font-style: normal;
} 

.prod_page .curr {
	color:#e4393c;
}

.btn-box{
	width:100%;
	height:40px;
	padding-top: 5px;
}
.ok-btn-on{
	width:80px;
	height:30px;
	background-color:rgb(229,0,79);
	border:1px solid #eee;
	color:#fff;
	cursor: pointer;
	margin:0 0 0 436px;;
}
.ok-btn-off{
	width:80px;
	height:30px;
	background-color:#ddd;
	border:1px solid #999;
	color:#666;
	cursor: auto;
	margin:0 0 0 450px;;
}
.cancel-btn{
	width:80px;
	height:30px;
	background-color:rgb(229,0,79);
	border:1px solid #eee;
	color:#fff;
	cursor: pointer;
	margin-left:20px;
}

.prod-selected{
	background-color: #d5e4fa;
}

.search-box{
	width:605px;
	margin: 0px auto;
	padding:10px;
}
.search-box input[type="text"]{
	width:550px;height:30px;border:1px solid #ddd;background-color: #fff;
}
.search-box input[type="submit"]{
	width: 48px;
    background-color: #f9f9f9;
    border: 1px solid #e4e4e4;
	height:30px;
	outline: none;
	cursor: pointer;
}
</style>
<link type="text/css"
	href="<ls:templateResource item='/resources/templets/css/bidding-activity${_style_}.css'/>"
	rel="stylesheet">
</head>
<body scroll="no">
	<form:form action="${contextPath}/m/store/storeProdLayout" id="form1"
		method="post">
		<input type="hidden" id="curPageNO" name="curPageNO"
			value="${curPageNO}" />
		<div class="search-box">
			<input type="text" name="name" id="name" maxlength="20"
				value="${prod.name}" size="20" placeholder="请输入商品名称" />
			<input type="submit" value="搜索" />
		</div>
	</form:form>
	<div class="serachProd">
		<h4>商品列表</h4>
		<ul class="prod_list">
			<c:forEach items="${requestScope.list}" var="prod" varStatus="status">
				<li>
					<a class="prod" onclick="loadProdSku('${prod.prodId}',this)" href="javascript:void(0);" title="${prod.name}"> 
						<span class="floor_sear_img"> 
							<img width="70" height="70" src="<ls:images item='${prod.pic}' scale='3' />" alt="${prod.name}"> </span> <span class="prod_name">${prod.name}
						</span>
					</a>
				</li>
			</c:forEach>
			<c:if test="${empty requestScope.list}">
			    <span style="display: -moz-inline-box;color: rgb(153, 153, 153);margin-top: 10px;">暂无商品信息</span>
			</c:if>
		</ul>
		
			<div class="prod_page" align="right">
				<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
			</div>
	</div>

	<div class="serachProdsku" style="margin-top: 10px;"></div>
	<div id="btn-box" class="btn-box" style="display:none;">
		<button id="ok-btn" class="ok-btn-on" onclick="addProdSku();">确定添加</button>
		<button class="cancel-btn" onclick="cancel();">取消</button>
	</div>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
	<script type="text/javascript">
		$(function(){
			
		});
		var beforeProd = null;
		var globalProdId = null;
		
		var contextPath = '${contextPath}';
		
		function pager(curPageNO) {
			document.getElementById("curPageNO").value = curPageNO;
			document.getElementById("form1").submit();
		}

		function loadProdSku(prodId,thisObj) {
			//给上一个选中去除样式
			$(beforeProd).removeClass("prod-selected");
			
			if (prodId == null || prodId == "" || prodId == undefined) {
				layer.alert("商品Id为空",{icon: 0});
			}
			var url = contextPath + "/m/store/storeProdSku/"
					+ prodId;
			$(".serachProdsku").load(url);
			
			globalProdId = prodId;
			
			//给选中的商品加样式
			$(thisObj).addClass("prod-selected");
			beforeProd = thisObj;
			
		}
		 function addProdSku(){
		       var storeProd = new Object();
			   storeProd.id = $("input[name='storeProdId']").val();
			   storeProd.productId = globalProdId;
			   //storeProd.stock = null;
			   var existsku=$("table[id='skuList']").attr("existsku");
		       var tr=$("table[id='skuList'] tbody ").find("tr:gt(1)");
		       var storeSkuDto=new Array();
		       for(var i = 0;i<tr.length;i++){
		            if(existsku=="true"){
		               var _sku =$(tr[i]).find("input[name='skuIds']").val();
			           if(isBlank(_sku)){
			              layer.alert("参数有误",{icon: 0});
			              return;
			           }
			           var stocks=$(tr[i]).find("input[name='skuStocks']").val();
			           if(isBlank(stocks)){
			              layer.alert("请输入库存信息",{icon: 0});
			               return;
			           }else if(isNaN(stocks)){
			               layer.alert("请输入正确的库存信息",{icon: 0});
			               return;
			           }
			           
			            var storeSku = new Object();
						//storeSku.id = null;
						storeSku.skuId =_sku;
						storeSku.productId = globalProdId;
						storeSku.stock = stocks;
			            storeSkuDto.push(storeSku);
		           }else{
		               var stocks=$(tr[i]).find("input[name='prodStocks']").val(); 
		               if(isBlank(stocks)){
			             layer.alert("请输入库存信息",{icon: 0});
			              return;
			           }else if(isNaN(stocks)){
			        	   layer.alert("请输入正确的库存信息",{icon: 0});
			               return;
			           }
			            storeProd.stock = stocks;
		           }
		       }
		       storeProd.storeSkuDto=storeSkuDto;
		       var str=JSON.stringify(storeProd);
		       if(isBlank(str)){
		           layer.alert("请输入正确信息",{icon: 0});
		           return;
		       }
			   addProd(str);
		 }
		 
		 function cancel(){
			 parent.layer.closeAll();
		 }
		 
		 function changeOkBtnState(b){
		 	if(b == "true"){
		 		$("#btn-box").css("display","none");
		 		//$("#ok-btn").attr("class","ok-btn-off");
		 		//$("#ok-btn").attr("disabled","disabled");
		 	}else{
		 		$("#btn-box").css("display","block");
		 		//$("#ok-btn").removeAttr("disabled");
		 		//$("#ok-btn").attr("class","ok-btn-on");
		 	}
		}
		
		//发送添加商品请求
	function addProd(storeProd){
		$.ajax({
			url: contextPath + "/m/store/addProd",
			type: "POST",
			data: {
				"storeProd": storeProd
			},
			dataType: "json",
			success: function(result){
				if(result == "OK"){
						parent.ajaxLoadProd();
						layer.confirm("添加商品成功,继续添加?", {
							 icon: 1
						     ,btn: ['确定','关闭'] //按钮
						   }, function(index){
							   var url = contextPath + "/m/store/storeProdSku/"+ globalProdId;
								$(".serachProdsku").load(url);
								layer.close(index);
						   },function(){
							   parent.layer.closeAll();
						   });
				}else{
					layer.alert(result,{icon: 2});
				}
			}
		});
	}
		
		
	function isBlank(val){
	   if(val==null || val=="" || val==undefined){
	      return true;
	   }
	   return false;
	}
	</script>
</body>
