<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>注册-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
	<style type="text/css">
		 .btn-regist {width: 270px; height: 36px; color: #FFF; font-family: "microsoft yahei"; font-size: 14px; font-weight: bold ; overflow: hidden; background: #d1d1d1; display: inline-block; border: 0 none; text-align: center;}
	</style>
</head>
<body class="loginback">
<div>
    <div class="yt-wrap">
        <div class="lr_tab">
            <a href="${contextPath}/login">登 录</a><a href="javascript:void(0);" class="on">注 册</a>
        </div>
        <div class="logintop">
            <a href="${contextPath}/home"><img src="<ls:photo item="${systemConfig.logo}"/>" style="max-height:89px;"/>
            </a>
        </div>
    </div>
    <div class="lg-mid-b">
        <div class="regbox">

            <div class="other_lg">
                <h3>已有${systemConfig.shopName}账号</h3>
                <div><a href="${contextPath}/login"><img src="<ls:templateResource item='/resources/templets/images/lp1.jpg'/>"></a></div>
                
                        <c:if test="${indexApi:isQqLoginEnable() || indexApi:isWeiboLoginEnable() || indexApi:isWeiXinLoginEnable()}">
	                        <div class="coagent">
	                            <h5>使用合作网站账号登录：</h5>
	                            <ul>
	                                <c:if test="${indexApi:isWeiXinLoginEnable()}">
		                                 <li > 
		                                 	<a  href="${contextPath }/thirdlogin/weixin/pc/authorize" >
								        		<img src="<ls:templateResource item='/resources/templets/images/lp_wx.jpg'/>">
								        	</a>
								        </li>
	                                </c:if>
	                                <c:if test="${indexApi:isQqLoginEnable()}">
		                                <li style="margin-left:5px;">
		                                    <a  href="${contextPath }/thirdlogin/qq/pc/authorize" >
		                                      <img  alt="QQ登录" src="<ls:templateResource item='/resources/templets/images/qq_onnect_logo_7.png'/>">
		                                    </a>
		                                </li>
	                                </c:if>
	                                <c:if test="${indexApi:isWeiboLoginEnable()}">
		                               <li style="margin-left:5px;"> 
		                                    <a  href="${contextPath }/thirdlogin/sinaweibo/pc/authorize" >
		                                    	<img  alt="" src="<ls:templateResource item='/resources/templets/images/loginButton_24.png'/>">
		                                    </a>
		                                </li> 
	                                </c:if>
	                             </ul>
	                        </div>
                        </c:if>
            </div>

            <div class="reg_b">
                <div class="reg_tab">
                    <a style="margin-left:121px;" href="${contextPath}/login">登录</a><a class="on" href="${contextPath}/reg">注 册</a>
                </div>
                <form:form action="${contextPath}/userRegByPhone" method="post" name="userRegForm" id="userRegForm" enctype="multipart/form-data">
                    <input type="hidden" value="${uid}" name="parentUserName">
                    
                    <div class="item">
                        <span class="label">用户名：</span>
                        <div class="fl item-ifo">
                            <div class="r_inputdiv">
                                <input type="text" class="text" placeholder="例如：abc_123" id="userName" name="userName" maxlength="16">
                                <label></label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="item">
                        <span class="label">手机号：</span>
                        <div class="fl item-ifo">
                            <div class="r_inputdiv">
                                <input type="text" class="text" id="telephone" maxlength="11" name="mobile">
                                <label></label>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <span class="label">登录密码：</span>
                        <div class="fl item-ifo">
                            <div class="r_inputdiv">
                                <input type="password" class="text" id="password" name="password" maxlength="20">
                                <label></label>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <span class="label">确认密码：</span>
                        <div class="fl item-ifo">
                            <div class="r_inputdiv">
                                <input type="password" class="text" id="password2" maxlength="20">
                                <label></label>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <span class="label"> 验证码：</span>
                        <div class="fl item-ifo">
                            <div class="r_inputdiv v_align_mid">
                                <input type="text" class="text text-1" id="randNum" name="randNum" maxlength="4">
                                <img width="95" height="38" src="<ls:templateResource item='/validCoderRandom'/>" id="randImage" name="randImage">
                                <a href="javascript:void(0)" onclick="javascript:changeRandImg()" style="font-weight: bold;">换一张</a>
                                <label></label>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <span class="label">短信验证码：</span>
                        <div class="fl item-ifo">
                            <div class="r_inputdiv">
                                <input type="text" class="text text-1" id="erifyCode" maxlength="6" name="mobileCode">
                                <input class="button" type="button" value="获取短信校验码" onclick="sendMobileCode()" id="erifyCodeBtn" name="erifyCodeBtn"/>
                                <label></label>
                            </div>
                        </div>
                    </div>

                    <div style=" height:30px; padding-top:0;" class="item">
                        <span class="label">&nbsp;</span>
                        <div class="fl item-ifo">
                            <input type="checkbox" class="checkbox" id="readme">
                            <label for="protocol">我已阅读并同意<a id="protocol" class="blue" href="javascript:void(0)">《网络服务和使用协议》</a></label>
                        </div>
                    </div>
                    <div class="item">
                        <span class="label">&nbsp;</span>
                        <input id="regBtn" type="button" value="请阅读并同意网络服务和使用协议" class="submit btn-img btn-regist">
                    </div>
                </form:form>
            </div>
        </div>
    </div>
    <%@ include file="home/simpleBottom.jsp" %>
</div>
<%@ include file="/WEB-INF/pages/common/jquery.jsp" %>
<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/common/js/randomimage.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/reg.js'/>"></script>
<script type="text/javascript">
    var contextPath = "${contextPath}";
</script>
</body>
</html>