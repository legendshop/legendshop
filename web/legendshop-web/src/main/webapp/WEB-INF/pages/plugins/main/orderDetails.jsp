<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%
	response.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT");
	// 设置 HTTP/1.1 no-cache 头
	response.setHeader("Cache-Control", "no-store,no-cache,must-revalidate");
	// 设置 IE 扩展 HTTP/1.1 no-cache headers， 用户自己添加
	response.addHeader("Cache-Control", "post-check=0, pre-check=0");
	// 设置标准 HTTP/1.0 no-cache header.
	response.setHeader("Pragma", "no-cache");

	response.setHeader("Pragma","No-cache");
	response.setHeader("Cache-Control","no-cache");
	response.setDateHeader("Expires", 0);
 %>
<!DOCTYPE html>
<html>
<head>
<!-- the same with indexFrame.jsp under red template -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="renderer" content="webkit" />
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<title>填写核对订单信息-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet" />
<link rel="stylesheet" href="<ls:templateResource item='/resources/common/css/jquery.autocomplete.css'/>">
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/plugins/showLoading/css/showLoading.css'/>" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/orderCoupon.css'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/buy.css?v=1'/>" rel="stylesheet" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller${_style_}.css'/>" rel="stylesheet"/>

</head>
<body class="graybody">
	<div id="doc">
		<!--顶部menu-->
		<div id="hd">

			<!--hdtop 开始-->
			<%@ include file="home/header.jsp"%>
			<!--hdtop 结束-->

			<!-- nav 开始-->
			<div id="hdmain_cart">
				<div class="yt-wrap">
					<h1 class="ilogo">
						<a href="${contextPath}/">
							<img src="<ls:photo item="${systemConfig.logo}"/>" style="max-height:89px;" />
						</a>
					</h1>

					<!--page tit-->
					<div class="cartnew-title">
						<ul class="step_h">
							<li class="done"><span>购物车</span>
							</li>
							<li class="on done"><span>填写核对订单信息</span>
							</li>
							<li class=" "><span>提交订单</span>
							</li>
						</ul>
					</div>
					<!--page tit end-->
				</div>
			</div>
			<!-- nav 结束-->
		</div>
		<!--hd end-->
		<!--顶部menu end-->


		<div id="bd" style="margin-bottom: 30px;">
			<div class="yt-wrap ptit2">填写并核对订单信息</div>
			<!--order content -->
			<div class="yt-wrap ordermain">
				<!-- 收货地址 -->
				<%@ include file="orderAddr.jsp"%>

				<!-- 支付方式 -->
				<div id="pay" class="pay" style="display: none;">
					<h3>
						支付方式<b style="display: none;" onclick="PayMethod.openModifyWindow();" class="update">修改</b>
						<span id="pay_msg"></span>
					</h3>
					<div style="display: block;" id="pay_opened">
						<ul>

							<c:choose>
								<c:when test="${requestScope.userShopCartList.cod}">
									<li><input type="radio" value="1" id="hd_pay" name="pay_opened_pm_radio"> <label style="cursor:pointer;margin-right:80px;" for="hd_pay">货到付款</label> <em>送货上门后再收款，支持现金、POS机刷卡、支票支付</em>
									</li>
								</c:when>
								<c:otherwise>
									<li><input type="radio" disabled="disabled" value="1" id="hd_pay" name="pay_opened_pm_radio"> <label style="cursor:pointer;margin-right:80px;" for="hd_pay">货到付款</label> <em
										style="color: red;">该订单部分商品不支持货到付款</em></li>
								</c:otherwise>
							</c:choose>
							<li><input type="radio" checked="checked" value="2" id="online_pay" name="pay_opened_pm_radio"> <label style="cursor:pointer;margin-right:80px;" for="online_pay">在线支付</label> <em>支持绝大数银行卡及第三方在线支付</em>
							</li>
						</ul>
					</div>
				</div>
				<!-- 商品清单 -->
				<div class="pro-list">
					<h3 style="margin-bottom: -15px;">商品清单</h3>
					<c:if test="${requestScope.userShopCartList.type eq 'NORMAL' and (empty isBuyNow or !isBuyNow)}">
						<a style="float: right;margin-top: -20px;color: #3f6de0;" href="${contextPath}/shopCart/shopBuy">返回购物车修改</a>
					</c:if>
					<div id="shoppinglistDiv">
						<div id="shopping">
						<c:forEach items="${requestScope.userShopCartList.shopCarts}" var="carts" varStatus="status">
							<div class="shoppinglist">
								<div class="dis-modes">
									<p>
										店铺：
										<a style="color: #e5004f;" target="_blank" href="${contextPath}/store/${carts.shopId}" title="${carts.shopName}">${carts.shopName}</a>
										<c:if test="${not empty carts.mailfeeStr}">
											<span style="color:#e5004f;">[${carts.mailfeeStr}]</span>
										</c:if>
									</p>

									<div id="deliveryMode" style="margin-top:15px;">
										配送方式：
										<c:choose>
											<c:when test="${carts.regionalSales}">
									           <font color="#e5004f;">有地区限售商品存在</font>
									        </c:when>
											<c:when test="${carts.freePostage}">
									                               商家承担运费
									        </c:when>
											<c:otherwise>
												<select shopId="${carts.shopId}" class="delivery">
													<c:choose>
														<c:when test="${empty carts.transfeeDtos}">
															<option value="" style="padding: 5px 0px;">请选择配送方式</option>
														</c:when>
														<c:otherwise>
															<c:forEach items="${carts.transfeeDtos}" var="transtype" varStatus="s">
																<option style="padding: 5px 0px;" transtype="${transtype.freightMode}" value="${transtype.deliveryAmount}">${transtype.desc}</option>
															</c:forEach>
														</c:otherwise>
													</c:choose>
												</select>
											</c:otherwise>
										</c:choose>
									</div>
									<div class="shop-add">
										<div class="shop-wor">
											<input type="text" placeholder="给卖家留言,限45个字" name="remark" shopId="${carts.shopId}" maxlength="45">
										</div>
									</div>
								</div>
								<table class="cartnew_table" style="width:820px;float:right;margin-top:0px;">
									<thead style="border: 1px solid #e4e4e4;border-bottom: 0;">
										<tr>
											<th align="left" style="width:450px;">店铺宝贝</th>
											<th>单价</th>
											<th>数量</th>
											<th>状态</th>
										</tr>
									</thead>
									<tbody style="border: 1px solid #e4e4e4;border-top: 0;">

										<%-- 循环商家的每个活动 --%>
										<c:forEach items="${carts.marketingDtoList}" var="marketing" varStatus="markSts">
											<c:if test="${not empty marketing.hitCartItems}">
												<c:if test="${not empty marketing.promotionInfo}">
													<tr>
														<td colspan="4">
															<div class="item-header">
																<div class="f-txt">
																	<span class="full-icon">
																		<c:choose>
																			<c:when test="${marketing.type==0}">
																				满减
																			</c:when>
																			<c:when test="${marketing.type==1}">
																				满折
																			</c:when>
																			<c:when test="${marketing.type==2}">
																				限时
																			</c:when>
																		</c:choose>
																	</span>
																	${marketing.promotionInfo}
																</div>
																<c:if test="${marketing.totalOffPrice>0}">
																	<div class="f-price">
																		<strong>¥${marketing.hitProdTotalPrice}</strong>
																		<div class="ar">
																			<span class="ftx-01">减：¥${marketing.totalOffPrice}</span>
																		</div>
																	</div>
																</c:if>
															</div></td>
													</tr>
												</c:if>
												<%-- 循环活动下的每个商品 --%>
												<c:forEach items="${marketing.hitCartItems}" var="basket" varStatus="s">
													<tr>
														<td class="textleft">
															<div class="cartnew-img">
																<a class="a_e" target="_blank" href="${contextPath}/views/${basket.prodId}">
																	<img title="${basket.prodName}" src="<ls:images item='${basket.pic}' scale='3' />">
																</a>
															</div>
															<ul class="cartnew-ul">
																<li><a href="${contextPath}/views/${basket.prodId}" target="_blank" class="a_e">${basket.prodName}</a>
																<c:if test="${!empty basket.activIntro and basket.transportFree ne 1}"><font color="red" style="margin-left: 6px">* 单品包邮(${basket.activIntro})</font></c:if>
																</li>
																<li><c:if test="${not empty basket.cnProperties}">
							                                       ${basket.cnProperties}
							                                    	</c:if>
							                                    </li>
							                                    <li style="color: #e5004f;">
																	<c:if test="${basket.transportFree eq 1}">
																		${basket.shippingStr}
																	</c:if>
							                                    </li>
																<c:if test="${not empty basket.cartMarketRules}">
																	<c:forEach items="${basket.cartMarketRules}" var="rule">
																		<li><span>优惠信息</span><em>${rule.promotionInfo}</em>
																		</li>
																	</c:forEach>
																</c:if>
															</ul>
														</td>
														<td><c:if test="${basket.price-basket.promotionPrice > 0}">
																<del style="color:#999;">
																	￥<fmt:formatNumber pattern="0.00" maxFractionDigits="2" groupingUsed="false" value="${basket.price}" />
																</del>
																<br>
															</c:if> <span style="font-size:14px; color:#e5004f;">
																￥<fmt:formatNumber pattern="0.00" maxFractionDigits="2" groupingUsed="false" value="${basket.promotionPrice}" />
															</span></td>
														<td>x${basket.basketCount}</td>
														<td><c:choose>
																<c:when test="${empty requestScope.userShopCartList.provinceId}">
																	<span>有货</span>
																</c:when>
																<c:when test="${empty basket.stocks || basket.stocks==0}">
																	<span class="stocksErr">无货</span>
																</c:when>
																<c:when test="${basket.stocks==-1}">
																	<span class="stocksErr">该地区限售</span>
																</c:when>
																<c:when test="${basket.stocks<basket.basketCount}">
																	<span class="stocksErr">缺货，仅剩${basket.stocks}件</span>
																</c:when>
																<c:otherwise>
																	<span>有货</span>
																</c:otherwise>
															</c:choose></td>
													</tr>
												</c:forEach>
											</c:if>
										</c:forEach>
									</tbody>
								</table>
								<div class="clr"></div>
							</div>
						</c:forEach>
						</div>

					<!--  发票 -->
					<c:if test="${switchInvoice eq 1}">
						<%@ include file="orderInvoice.jsp" %>
					</c:if>

						<!-- 优惠券和红包 -->
						<div class="choose-coupon">
							<div class="choose-coupon-tit">
								<b style="font-size:16px;">使用优惠</b><i class="down"></i>
							</div>
							<div id="sel-cou-div" style="display:none;">
								<div class="choose-coupon-con" id="orderCouponList"></div>
								<div class="virtual-usedcont">
									<span class="virtual-usedcont-price">
										金额抵用<em>￥<span id="couponOffPrice">0.00</span>
										</em>
									</span>
									<ul>
										<li style="display: block;">优惠券<em id="couponCount">0</em>张，优惠<em id="couponOffPrice2">0.00</em>元</li>
									</ul>
								</div>
							</div>
						</div>

						<!-- 确认订单按钮 -->
						<div class="confirm" id="confirm">
							<div class="price" style="margin-top: 20px;">
								<p>
									商品件数总计：
									<span id="allCount">${requestScope.userShopCartList.orderTotalQuanlity}</span>
								</p>
								<p>
									商品总价：
									<span id="allActualCash">
										￥<fmt:formatNumber value="${requestScope.userShopCartList.orderTotalCash}" type="currency" pattern="0.00" />
									</span>
								</p>
								<p>
									运费：
									<span id="allFreightAmount">
										￥<fmt:formatNumber value="${requestScope.userShopCartList.orderFreightAmount}" type="currency" pattern="0.00" />
									</span>
								</p>
								<p>
									促销优惠：
									<span id="allDiscount">
										-￥<fmt:formatNumber value="${requestScope.userShopCartList.allDiscount}" type="currency" pattern="0.00" />
									</span>
								</p>
								<p>
									优惠券：
									<span id="allDiscount">
										-￥<e id="couponOffPrice3">0</e>
									</span>
								</p>
								<p>
									<fmt:formatNumber var="allCashFormat" value="${requestScope.userShopCartList.orderActualTotal}" type="currency" pattern="0.00" />
									需支付总额：<b><span class="red" id="allCash">￥${allCashFormat}</span>
									</b>
								</p>
							</div>
							<span id="confirm_totalPrice">
								应付金额：<b>￥<em id="allCashFormat">${allCashFormat}</em>
								</b>
							</span>
							<button class="on" id="submitOrder" onclick="submitOrder();">确认订单</button>
						</div>
					</div>
					<!--order content end-->
				</div>

				<form:form id="orderFormReload" action="${contextPath}/p/orderDetails" method="POST">
					<input id="shopCartItems" name="shopCartItems" value="${shopCartItems}" type="hidden" />
					<input name="type" type="hidden" value="${requestScope.userShopCartList.type}" />
					<input  id="buyNow" name="buyNow" type="hidden" value="false" />
					<input id="prodId" name="prodId" type="hidden"  />
					<input id="skuId" name="skuId" type="hidden" />
					<input id="count" name="count" type="hidden" />
					<input id="invoiceId" name="invoiceId" type="hidden" />
				</form:form>

				<form:form id="orderForm" action="${contextPath}/p/submitOrder" method="post">
					<input id="basketStr" name="basketStr" value="${shopCartItems}" type="hidden" />
					<input id="remarkText" name="remarkText" type="hidden" />
					<input id="invoiceIdStr" name="invoiceId" type="hidden" value="${userdefaultInvoice.id}"/>
					<input id="payManner" name="payManner" type="hidden" />
					<input id="delivery" name="delivery" type="hidden">
					<input id="couponStr" name="couponStr" type="hidden">
					<input name="type" type="hidden" value="${requestScope.userShopCartList.type}" />
					<input name="token" type="hidden" value="${SESSION_TOKEN}" />
					<input id="switchInvoice" name="switchInvoice" type="hidden" value="${switchInvoice}" />
				</form:form>

			</div>

		</div>

		<!----foot---->
		<%@ include file="home/bottom.jsp"%>
		<!----foot end---->
	</div>

</body>
<script type="text/javascript">
		var contextPath = '${contextPath}';
		var data = {allCount:"${requestScope.userShopCartList.orderTotalQuanlity}"};
        var prodId = '${reqParams.prodId}';
        var skuId = '${reqParams.skuId}';
        var count = '${reqParams.count}';
        var isBuyNow = '${isBuyNow}';
        var token='${SESSION_TOKEN}';
		var type = "${type}";
</script>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/showLoading/js/jquery.showLoading.min.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/orderDetails.js'/>"></script>
<script src="<ls:templateResource item='/resources/templets/js/addressSlide.js'/>"></script>

</html>
