<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>举报管理 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-nav.css'/>" rel="stylesheet">
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con">
	<div class="o-mt"><h2>举报管理</h2></div>
	
	<div class="pagetab2">
                 <ul>           
                    <li  id="accusationList"  onclick="loadAccusationList();"><span>举报列表</span></li>
                    <li  class="on"  id="accusationConfigure" ><span>举报设置</span></li>                
                 </ul>       
     </div>
		
	<form:form  action="${contextPath}/p/accusation/save" method="post" id="form1" enctype="multipart/form-data">
            <input id="id" name="id" value="${accusation.id}" type="hidden">
            <input id="prodId" name="prodId" value="${product.prodId}" type="hidden">
            <div align="center" style="background:#fff;" class="brand-add">
            <table cellspacing="0" cellpadding="0" border="0" style=" padding-top:30px;" class="sendmes_table">
        	<tr>
		        <th>
		          	产品名称: <font color="ff0000">*</font>
		       	</th>
		        <td>
			        <c:choose>
				        <c:when test="${not empty accusation.prodName}">${accusation.prodName}</c:when>
				        <c:otherwise>${product.name}</c:otherwise>
			        </c:choose>
		        </td>
			</tr>
			<tr>
		        <th>
		          	举报类型: <font color="ff0000">*</font>
		       	</th>
		        <td>
		           	<select class="combox sele"  id="typeId" name="typeId"  requiredTitle="true" childNode="subjectId"  selectedValue="${accusation.typeId}"  retUrl="${contextPath}/p/AccusationType"></select>
		           	<span></span>
		        </td>
		</tr>
		<tr>
		        <th>
		          	举报主题: <font color="ff0000">*</font>
		       	</th>
		        <td>
		           	 <select  class="combox sele"   id="subjectId" name="subjectId"   requiredTitle="true"  selectedValue="${accusation.subjectId}"  parentValue="${accusation.typeId}"  retUrl="${contextPath}/p/AccusationSubject/{value}"></select>
					 <span></span>
		        </td>
		</tr>
		<tr>
		        <th>
		          	上传凭证: <font color="ff0000">*</font>
		       	</th>
		        <td>
		        	<c:choose>
		        		<c:when test="${not empty accusation.pic1}">
		        			<img src="<ls:images item='${accusation.pic1}' scale="3"/>"  />
		        		</c:when>
		        		<c:otherwise>
		        			<input type="file" name="pic1File" id="pic1File"/>
		        		</c:otherwise>
		        	</c:choose>
		        </td>
		</tr>
		<tr>
		        <th>
		          	上传凭证2: 
		       	</th>
		        <td>
		        	<c:choose>
		        		<c:when test="${not empty accusation.pic2}">
		        			<img src="<ls:images item='${accusation.pic2}' scale="3"/>"  />
		        		</c:when>
		        		<c:otherwise>
		        			<input type="file" name="pic2File" id="pic2File"/>
		        		</c:otherwise>
		        	</c:choose>
		        </td>
		</tr>
		<tr>
		        <th>
		          	上传凭证3: 
		       	</th>
		        <td>
		        	<c:choose>
		        		<c:when test="${not empty accusation.pic3}">
		        			<img src="<ls:images item='${accusation.pic3}' scale="3"/>"  />
		        		</c:when>
		        		<c:otherwise>
		        			<input type="file" name="pic3File" id="pic3File"/>
		        		</c:otherwise>
		        	</c:choose>
		        </td>
		</tr>
		<tr>
		        <th>
		          	举报内容: <font color="ff0000">*</font>
		       	</th>
		        <td>
		           	<textarea name="content" id="content" cols="100" rows="8" style="width:700px;height:300px;visibility:hidden;">${accusation.content}</textarea>
		           	<span></span>
		        </td>
		</tr>
                <tr>
                		<th></th>
                    <td colspan="2">  
		                    <c:if test="${empty accusation.id}">
		                    	<a class="btn-r" href="javascript:void(0);" onclick="submitTable();"><s></s>提交</a>
		                    </c:if>
							<a class="btn-g" href="javascript:void(0);"  onclick="loadAccusationList();" ><s></s>返回</a>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
     
</div>


    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
	<link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
<script src="${contextPath}/resources/common/js/jquery.validate1.5.5.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/accusation.js'/>"></script>
 <script type="text/javascript">
 var contextPath = "${contextPath}";
 var UUID = "${cookie.SESSION.value}";
</script>
</body>
</html>