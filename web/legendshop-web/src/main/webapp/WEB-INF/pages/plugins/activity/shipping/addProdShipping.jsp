<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/dialog.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>商家包邮活动</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/mergeGroup/mergeGroupEdit.css'/>" rel="stylesheet"/>
    <%-- <link type="text/css" href="<ls:templateResource item='/resources/templets/css/shipping/addShipping.css'/>" rel="stylesheet"/> --%>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/shipping/addProdFull.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/shipping/addFull.css'/>" rel="stylesheet"/>
    <script src="<ls:templateResource item='/resources/plugins/My97DatePicker/WdatePicker.js'/>" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/plugins/default/layer.css" />
</head>
<body>
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
	</div>
	<div class="w1190">
		<div class="seller-cru">
			<p>
				您的位置>
				<a href="javascript:void(0)">首页</a>>
				<a href="javascript:void(0)">卖家中心</a>>
				<span class="on">商品包邮</span>
			</p>
		</div>
		<!-- 侧边导航 -->
		<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
		<!-- /侧边导航 -->
		<div class="add-fight">
			<form id="prodShipping">
				<!-- 活动信息 -->
				<div class="add-item">
					<div class="add-tit"><span>活动信息</span></div>
					<div class="activity-mes">
						<span class="act-tit"><em>*</em>活动名称：</span>
						<input type="text" name="name" id="name" class="act-name" placeholder="请填写活动名称" maxlength="20">
						<span class="act-des">（2-20）字</span>
						<div class="ant-form-explain" name="namePrompt" style="margin-left: 14%;display: none">活动名称限制2-20个字</div>
					</div>
					<div class="activity-mes">
						<span class="act-tit"><em>*</em>活动时间：</span>
						<span class="act-time">
							<lable><em style="color: #e5004f;margin-right: 5px;"></em></lable>
							<input type="text" readonly="readonly" placeholder="开始时间" name="startDate" size="21" id="startDate" value='<fmt:formatDate value="${active.startDate}" pattern="yyyy-MM-dd"/>' type="text" /><span></span>
							&nbsp;-&nbsp;
							<lable><em style="color: #e5004f;margin-right: 5px;"></em></lable>
							<input readonly="readonly" placeholder="结束时间" name="endDate" size="21" id="endDate" value='<fmt:formatDate value="${active.endDate}" pattern="yyyy-MM-dd"/>' type="text"  /><span></span>
						</span>
					</div>
				</div>
				<!-- /活动信息 -->
				<!-- 活动规则 -->
				<div class="add-item">
					<div class="add-tit"><span>活动规则</span></div>
					<div class="activity-mes">
						<span class="act-tit"><em>*</em>满：</span>
						<input type="text" max="999999.99" autocomplete="off" name="fullValue" id="fullValue" style="width: 60px;">
						<select id="reduceType" name="reduceType">
							<option value="1">元</option>
							<option value="2">件</option>
						</select>
					</div>
					
					<div class="prodContext">
						<div class="add-tit" style="padding-bottom: 10px;border-bottom: 1px solid #f9f9f9;"><span>选择活动商品</span></div>
						
						<div class="form-box">
							<div class="ant-tabs ant-tabs-top ant-tabs-line">
								<div role="tablist" class="ant-tabs-bar" tabindex="0">
									<div class="ant-tabs-nav-container">
										<div class="ant-tabs-nav-wrap">
											<div class="ant-tabs-nav-scroll">
												<div class="ant-tabs-nav ant-tabs-nav-animated">
													<div class="ant-tabs-ink-bar ant-tabs-ink-bar-animated" style="display: block; transform: translate3d(0px, 0px, 0px); width: 96px;"></div>
													<div role="tab" aria-disabled="false" aria-selected="true" data-type="select" class="ant-tabs-tab-active ant-tabs-tab">选择商品</div>
													<div role="tab" aria-disabled="false" aria-selected="false" data-type="setting" class=" ant-tabs-tab">已选商品</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="ant-tabs-content ant-tabs-content-animated" style="margin-left: 0%;">
									<div role="tabpanel" aria-hidden="true" class="all-prod ant-tabs-tabpane ant-tabs-tabpane-active all">
									</div>
									
									<div role="tabpanel" aria-hidden="false" class="join-prod ant-tabs-tabpane ant-tabs-tabpane-active">
										
										<div class=" clearfix">
											<div class="ant-spin-nested-loading">
												<div class="ant-spin-container">
													<div class="ant-table ant-table-large ant-table-without-column-header ant-table-scroll-position-left">
														<div class="ant-table-content">
															<div class="ant-table-body">
																<table class="">
																	<colgroup>
																		<col>
																		<col style="width: 200px; min-width: 200px;">
																		<col style="width: 200px; min-width: 200px;">
																		<col style="width: 150px; min-width: 150px;">
																		<col style="width: 100px; min-width: 100px;">
																	</colgroup>
																	<thead class="ant-table-thead">
																		<tr>
																			<th class="ant-table-selection-column">
																			</th>
																			<th class=""><span>商品</span></th>
																			<th class=""><span>价格(元)</span></th>
																			<th class=""><span>库存</span></th>
																			<th class=""><span>操作</span></th>
																		</tr>
																	</thead>
																	<tbody class="ant-table-tbody selected">
																	
																	</tbody>
																</table>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
	            		</div>
	           	 </div>
					
					<div class="activity-mes" style="margin: 35px 190px;">
						<span class="act-tit"></span>
						<a href="javascript:void(0)" class="btn-r big-btn" id="commit">确认保存</a>&nbsp;
						<a href="javascript:history.go(-1)" class="btn-g big-btn">返回列表</a>
					</div>
				</div>
				<!-- /活动规则 -->
			</form> 
		</div>
	</div>
	<!--foot-->
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	<!--/foot-->
</body>

<script src="${contextPath}/resources/common/js/jquery.validate.js"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/layer/layer.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/shipping/addProdShipping.js'/>"></script>
<script type="text/javascript">	
$(document).ready(function() {
	laydate.render({
		   elem: '#startDate',
		   calendar: true,
		   theme: 'grid',
		   type:'date',
		   min:'-1',
		   trigger: 'click'
	  });

	laydate.render({
		   elem: '#endDate',
		   calendar: true,
		   theme: 'grid',
		   type:'date',
		   min:'-1',
		   trigger: 'click'
	  });
});
var contextPath = "${contextPath}";
$(function() {
	$(".all-prod").load(contextPath + "/s/shippingActive/fullProd");
	//定义prodId数组
	prodIdList = new Array;
	//定义参加数组
	joinProdIdList = new Array;
});

</script>
</html>