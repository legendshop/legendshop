<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/dialog.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<title>客服人员</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css" href="${contextPath}/resources/templets/css/multishop/server/service-staff.css" rel="stylesheet">
<link type="text/css" href="${contextPath}/resources/plugins/default/layer-user.css" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>
					>
					<a href="${contextPath}/sellerHome">卖家中心</a>
					>
					<span class="on">客服人员</span>
				</p>
			</div>
			<%@ include file="../included/sellerLeft.jsp"%>
			<!-- <div class="o-mt">
			</div> -->
			<div class="service-staff">
				<div class="pagetab2">
					<ul id="listType">
						<li class="on"><span>客服人员</span></li>
					</ul>
  					</div>
				<!-- 筛选 -->
				<div class="form search-01">
					<div class="item clear">
						<a href="customServer/add" class="btn-r">新增客服</a>
						<a href="${contextPath}/customerService/login" class="btn-r" target="_blank" style="margin-left: 10px;">客服登录</a>
						<div class="fr clear">
							客服名称&nbsp;&nbsp;
							<input id="name" type="text" class="text">
							客服账号&nbsp;&nbsp;
							<input id="account" type="text" class="text">
							<a href="#" id="search" href="" class="btn-y">搜索</a>
						</div>
					</div>
				</div>
				<!-- /筛选 -->
				<!-- 客服人员列表 -->
				<div id="list" class="ser-table"></div>
				<!-- /客服人员列表 -->
			</div>
		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<script type="text/javascript">
		var contextPath = "${contextPath}";
	</script>
	<script type="text/javascript" src="${contextPath}/resources/plugins/layer/layer.js"></script>
	<script type="text/javascript" src="${contextPath}/resources/templets/js/multishop/customServer/customServer.js"></script>
	
</body>
</html>