<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

<c:choose>
	<c:when test="${layoutParam.layout=='layout2'}">
		<div style="background-color:${backColor};" class="recommend">
	      <h1 style="color:${fontColor}; <c:if test='${not empty fontImg}'>background:url(<ls:photo item='${fontImg}' />);</c:if> ">${empty title?'商品列表':title}</h1>
	      <div class="recomm_list">  
	      <c:forEach items="${requestScope.prods}" var="product" varStatus="status">
	           <ul class="recomm_ul">
	             <li class="recomm_pic"><a target="_blank" href="<ls:url address="/views/${product.prodId}" />"><img width="300" height="300" src="<ls:images item='${product.pic}' scale='0' />"></a></li>
	             <li class="recomm_name"><a target="_blank" href="<ls:url address="/views/${product.prodId}" />">${product.prodName}</a></li>
	             <li class="recomm_bgred">
	               <div class="recomm_price"><span>价格：<b>¥${product.cash}</b></span></div>
	               <div class="recomm_btn"><a target="_blank" href="<ls:url address="/views/${product.prodId}" />">立即抢购</a></div>
	             </li>
	           </ul>
	      </c:forEach>                       
	         </div>
	    </div>
	</c:when>
	<c:otherwise>
	  <div mark="${layoutParam.layoutId}" class="store_goods_list">
	     <c:forEach items="${requestScope.prods}" var="product" varStatus="status">
	      <ul class="goods_box">
	         <li><a target="_blank" href="<ls:url address="/views/${product.prodId}" />">
	           <img width="200" height="200" src="<ls:images item='${product.pic}' scale='0' />"></a></li>
	         <li class="goods_name"><a target="_blank" href="<ls:url address="/views/${product.prodId}" />">${product.prodName}</a></li>
	         <li class="goods_price"><b>¥${product.cash}</b></li>
	     </ul>
	     </c:forEach>
	   </div>
	</c:otherwise>
</c:choose>
<script type="text/javascript">


</script>

