<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<div class="right_con" style="overflow: hidden;width: 965px;">
    <div class="shop-mall-ser">
        <div class="shop-mall-ran"></div>
        <div class="shop-mall-search">
            积分范围： <input type="text" id="txtStartPonit" maxlength="5">&nbsp;—&nbsp;<input type="text" maxlength="5" id="txtEndPoint">
            &nbsp;&nbsp;<input type="text" id="txtGoodsName" maxlength="30" placeholder="搜索商品">
            <a href="javascript:void(0);" name="btnSearch" onclick="searchIntegral();" id="btnSearch">搜索</a>
            <a href="javascript:void(0);" name="btnSearch" onclick="clearCondition();" id="btnClear">清除条件</a>
        </div>
    </div>
    <nav id="main-nav" class="sort-bar">
        <div class="nch-sortbar-array">
            排序方式：
            <ul>
                <li id="default" ${empty param.orders?'class="selected"':''}>
                    <a title="默认排序" href="javascript:void(0);">默认</a>
                </li>
                <li id="saleNum" ${param.orders== 'saleNum,desc' ||  param.orders==
                        'saleNum,asc'?'class="selected"':''}>
                    <a title="点击按兑换量从高到低排序" href="javascript:void(0);"
                       <c:choose>
                       <c:when test="${param.orders=='saleNum,desc'}">class='desc' </c:when>
                           <c:when test="${param.orders=='saleNum,asc'}">class='asc'</c:when>
                    </c:choose> >兑换量<i></i></a>
                </li>
                <li id="exchangeIntegral" ${param.orders== 'exchangeIntegral,desc' ||  param.orders==
                        'exchangeIntegral,asc'?'class="selected"':''}>
                    <a title="点击按积分数从高到低排序" href="javascript:void(0);"
                       <c:choose>
                       <c:when test="${param.orders=='exchangeIntegral,desc'}">class='desc' </c:when>
                           <c:when test="${param.orders=='exchangeIntegral,asc'}">class='asc'</c:when>
                    </c:choose>  >积分数<i></i> </a>
                </li>
            </ul>
        </div>
    </nav>
    <div id="recommend" class="m10 recommend integral-prod" style="display: block;overflow: hidden;border:0;">
        <div id="group" class="tabcon" style="padding: 0px 0px;">

            <div class="suits" style="width:100%;">
                <ul class="list-h" style="margin-left:-11px;">
                    <c:choose>
                        <c:when test="${not empty requestScope.list}">
                            <c:forEach items="${requestScope.list}" var="prod">
                                <li class="gl-item">
                                    <div class="gl-i-wrap j-sku-item">
                                        <div class="p-img">
                                            <a title="${prod.name}" target="_blank" style="position: relative;" href="<ls:url address='/integral/view/${prod.id}'/>" target="_blank">
                                                <img class="integralImg" title="${prod.name}" alt="${prod.name}" src="<ls:images item='${prod.prodImage}' scale='4'/>"></a>
                                            <div class="picon pi8"><b></b></div>
                                        </div>
                                        <div class="p-price">
                                            积分：<strong class="J_price" title="积分：${prod.exchangeIntegral}">${prod.exchangeIntegral}</strong>
                                        </div>
                                        <div class="p-name">
                                            <a title="${prod.name}" href="<ls:url address='/integral/view/${prod.id}'/>" target="_blank">${prod.name}</a>
                                        </div>
                                </li>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            <center>
                                <div style="height: 100px;">没有找到相关的商品！</div>
                            </center>
                        </c:otherwise>
                    </c:choose>
                </ul>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div style="margin-top:10px;" class="page clearfix">
        <div class="p-wrap">
            <ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="product"/>
        </div>
    </div>
</div>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/IntegralProdList.js'/>"></script>
<script type='text/javascript'>
	var contextPath = '${contextPath}';
    var webPath = {
        webRoot: "${contextPath}",
        toatalPage: "<c:out value='${requestScope.pageCount}'/>",
        cookieNum: 0
    }
    var data = {
        curPageNO: "${_page}",
        firstCid: "<c:out value='${param.firstCid}'/>",
        twoCid: "<c:out value='${param.twoCid}'/>",
        thirdCid: "<c:out value='${param.thirdCid}'/>",
        startIntegral: "<c:out value='${param.startIntegral}'/>",
        endIntegral: "<c:out value='${param.endIntegral}'/>",
        keyWord: "<c:out value='${param.integralKeyWord}'/>",
        orders: "<c:out value='${param.orders}'/>"
    }
</script>