<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/dialog.jsp" %>
<table class="sold-table" cellspacing="20px" cellpadding="20px">
    <tbody>
    <tr>
        <th cellpadding="20px" width="120">客服名称</th>
        <th width="160">客服账号</th>
        <th>备注</th>
        <th width="150">创建时间</th>
        <th width="80">是否冻结</th>
        <th width="200">操作</th>
    </tr>
    <c:forEach items="${list}" var="item">
        <tr>
            <td>${item.name}</td>
            <td>${item.account}</td>
            <td>${item.remark}</td>
            <td>
                <fmt:formatDate value="${item.createTime}" pattern="yyyy-MM-dd HH:mm"/>
            </td>
            <td>${item.frozen ? "是" : "否"}</td>
            <td>
                <a href="customServer/update/${item.id}" class="tab-a btn-r">修改</a>
                <a href="customServer/updatePassword/${item.id}" class="tab-a btn-r">更改密码</a>
                <a href="javascript:void(0);" value="${item.id}" class="delete tab-a btn-g">删除</a>
            </td>
        </tr>
    </c:forEach>
    <c:if test="${list.size() == 0}">
        <tr class="no-record">
            <td colspan="6">暂无数据</td>
        </tr>
    </c:if>
    </tbody>
</table>
<div style="margin-top: 10px;" class="page clearfix">
    <div class="p-wrap">
        <ls:page pageSize="${pageSize}" total="${total}" curPageNO="${curPageNO }" type="simple"/>
    </div>
</div>
<script>
    $(".delete").click(function () {
        $this = $(this);
        layer.confirm('确定要删除该客服吗', {
            btn: ['确定', '取消'],
            skin: ['user-btn-class']
        }, function () {
            $.ajax({
                url: "customServer/delete",
                type: "POST",
                dataType: "json",
                data: {
                    id: $this.attr("value")
                },
                success: function (result) {
                    if (result.status == 1) {
                        layer.msg('删除成功', {time: 1000});
                        setTimeout(function () {
                            location.reload();
                        }, 1000)
                    }
                    else {
                        layer.msg(result.msg, {time: 1000});
                    }
                },
                error: function () {
                }
            });
        }, function () {
        });
    });
</script>