<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<c:set var="_userDetail" value="${shopApi:getSecurityUserDetail(pageContext.request)}"></c:set>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/top.js'/>"></script>
<script>
	if(typeof JSON == 'undefined'){$('head').append($("<script type='text/javascript' src='${contextPath}/resources/common/js/json.js'>"));}
	var contextPath="${contextPath}";
</script>
   <!--hdtop 结束-->
    <div id="hdtop">
        <div class="yt-wrap top-side cf">
            <ul class="fl lh">
				<li><a href="${contextPath}/home">商城首页</a></li>
				<c:choose>
					<c:when test="${sessionScope.SPRING_SECURITY_LAST_SHOP_ID eq 0 || empty sessionScope.SPRING_SECURITY_LAST_SHOP_ID}">
						<li><a href="${contextPath}/p/shopAgreement">免费开店</a></li>
					</c:when>
					<c:otherwise>
						<li><a href="${contextPath}/sellerHome">进入店铺</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
			<input type="hidden" id="huanXin" value="${sessionScope.huanXin}">
            <ul class="fr lh">
                <span id="user_info">
                <!-- 已经登录的用户 -->
                <ls:auth ifAnyGranted="F_USER">
					  <li id="loginbar"  class="ld">
					  		<input type="hidden" id="userName" value="${sessionScope.SPRING_SECURITY_LAST_USERNAME}">
							<font color="#FF3300" style="font-weight: bold;" id="userNickName">${sessionScope.SPRING_SECURITY_LAST_USERNAME}</font>
							<span><a href="${contextPath}/p/logout" target="_parent">[退出]</a></span>
						</li>
                   </li>
			     <li>
					<a href="${contextPath}/p/inbox">
						<img src="${contextPath}/resources/templets/images/msg_icon.png" />
								<span style="margin-left:1px;">消息</span>
								<span style="color: #FF3300!important;margin-left:2px;" id="msgCount">0</span>
					</a>
			    </li>
		     </ls:auth>

		        <!-- 尚未登录的用户 -->
				<ls:auth ifNotGranted="F_USER">
	                  <li class="ld" id="loginbar">
	                                <span><a href="${contextPath}/login">[登录]</a> &nbsp;&nbsp;
	                        <a href="${contextPath}/reg">[注册]</a></span>
	                   </li>
				</ls:auth>

                 </span>
                 <li><a href="<ls:url address='/allCategorys'/>">商品分类</a></li>
                 <c:if test="${not empty _userDetail.userName && sessionScope.userType != 'E'}">
                	 <li><a href="<ls:url address='/p/uchome'/>">会员中心</a></li>
                  </c:if>

				   <li class="menu" style="width: 80px;">
                          <div style="width: 76px;">
                          <dt class="ld">
                                <a href="<ls:url address='/sellerHome'/>">卖家中心</a><i></i>
                          </dt>
                          </div>
                          <div style="right: -2px;position: absolute;top: 30px;">
                             <dl style="width: 80px;">
                                   <dd>
                                   <c:choose>
                                   		<c:when test="${sessionScope.SPRING_SECURITY_LAST_SHOP_ID eq 0 || empty sessionScope.SPRING_SECURITY_LAST_SHOP_ID}">
											<div><a href="<ls:url address='/p/shopAgreement'/>">免费开店</a></div>
										</c:when>
										<c:when test="${not empty sessionScope.SPRING_SECURITY_LAST_SHOP_STATUS && sessionScope.SPRING_SECURITY_LAST_SHOP_STATUS eq 1}">
											<div><a href="<ls:url address='/s/ordersManage'/>">已卖出的产品</a><div>
											<div><a href="<ls:url address='/s/sellingProd'/>">销售中的产品</a></div>
										</c:when>
										<c:when test="${empty sessionScope.SPRING_SECURITY_LAST_SHOP_STATUS || sessionScope.SPRING_SECURITY_LAST_SHOP_STATUS ne 1}">
											<div><a href="<ls:url address='/shopStatusAbnormal'/>">店铺状态异常</a><div>
										</c:when>
										<%-- <c:otherwise>
											<div><a href="<ls:url address='/s/ordersManage'/>">已卖出的产品</a><div>
											<div><a href="<ls:url address='/s/sellingProd'/>">销售中的产品</a></div>
										</c:otherwise> --%>
									</c:choose>
								   </dd>
                             </dl>
                            </div>
                  </li>
                <li><a href="${contextPath}/helpcenter">帮助中心</a></li>
		</ul>
        </div>
    </div>
