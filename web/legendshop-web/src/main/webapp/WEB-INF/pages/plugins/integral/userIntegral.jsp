<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>我的积分 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
<%--    <%@include file='/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp'%>--%>
    <%@include file='/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp'%>

    <!-----------right_con-------------->
   <div class="right_con">
	
	<div class="o-mt">
		<h2>交易信息</h2>
	</div>
	<div class="pagetab2">
		<ul>
			<li class="on"><span>积分明细 </span></li>
			<li ><span><a href="<ls:url address='/p/integral/list'/>">积分兑换记录</a> </span></li>
		</ul>
	</div>

	<div>
		<div class="alert" style="margin-bottom:0;">
			<span class="mr30">我的积分：<strong class="mr5 red"
				style="font-size: 18px;"><fmt:formatNumber value="${score}"/></strong>分</span>
		</div>
		
		 <div class="form">
	         <div class="sold-ser sold-ser-no-bg clearfix">
	         	<div class="fr">
	              <div class="item">
	              	<input type="hidden" value="${empty curPageNO?1:curPageNO}" name="curPageNO"  id="curPageNO"> 
	              	获得时间：
	              	<input type="text"  style="width: 150px;"  class="item-inp" id="startDate" name="startDate" readonly="readonly"  placeholder="起始时间" value="<fmt:formatDate value="${startDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" />"/>
											 – 
					<input class="item-inp" type="text" style="width: 150px;" id="endDate" name="endDate"  readonly="readonly" placeholder="结束时间" value="<fmt:formatDate value="${endDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" />"/></td>
				  </div>
				  <div class="item">
	              	操作：<select id="logType" name="logType" class="item-sel">
					  	<option value="">-请选择-</option>
<%-- 					  	<option value="0" <c:if test="${logType==0}"> selected="selected" </c:if>>系统</option> --%>
					  	<option value="1" <c:if test="${logType==1}"> selected="selected" </c:if>>注册</option>
					  	<option value="2" <c:if test="${logType==2}"> selected="selected" </c:if>>登录</option>
					<%--   	<option value="3" <c:if test="${logType==3}"> selected="selected" </c:if>>充值</option> --%>
					  	<option value="4" <c:if test="${logType==4}"> selected="selected" </c:if>>订单兑换</option>
					  	<option value="5" <c:if test="${logType==5}"> selected="selected" </c:if>>完善资料</option>
					  	<option value="6" <c:if test="${logType==6}"> selected="selected" </c:if>>订单评论</option>
					  	<%-- <option value="7" <c:if test="${logType==4}"> selected="selected" </c:if>>推荐用户</option> --%>
<%-- 					  	<option value="8" <c:if test="${logType==4}"> selected="selected" </c:if>>晒单</option> --%>
<%-- 					  	<option value="9" <c:if test="${logType==4}"> selected="selected" </c:if>>订单消费</option> --%>
					  </select>
				   </div>
				   <div class="item">
	              		描述：<input type="text" value="${logDesc}" name="logDesc" id="logDesc"  class="item-inp" placeholder="请输入描述" maxlength="50" style="width:288px;">
	                  <input type="button" value="搜索" onclick="search(1)" class="bti btn-r">
	               </div>
	         	</div>
             </div>
          </div>
	   </div>
	          
		<table class="ncm-default-table sold-table">
			<thead>
				<tr >
					<th width="19%" >添加时间</th>
					<th width="10%" >积分变更</th>
					<th width="10%" >操作</th>
					<th width="21%">描述</th>
				</tr>
			</thead>
			<tbody>
				   <c:forEach items="${requestScope.list}" var="log">
				      <tr>
				      	<td><fmt:formatDate value="${log.addTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				      	<td><c:if test="${log.integralNum>0}">+</c:if>
				      	${log.integralNum}</td>
				      	<td>
							<c:choose>
				         		<c:when test="${log.logType eq 0}">系统</c:when>
				         		<c:when test="${log.logType eq 1}">注册</c:when>
				         		<c:when test="${log.logType eq 2}">登录</c:when>
				         		<c:when test="${log.logType eq 3}">充值</c:when>
				         		<c:when test="${log.logType eq 4}">订单兑换</c:when>
				         		<c:when test="${log.logType eq 5}">完善资料</c:when>
				         		<c:when test="${log.logType eq 6}">订单评论</c:when>
				         		<c:when test="${log.logType eq 7}">推荐用户</c:when>
				         		<c:when test="${log.logType eq 8}">晒单</c:when>
				         		<c:when test="${log.logType eq 9}">订单消费</c:when>
         					</c:choose>
						</td>
						<td>${log.logDesc}</td>
				      </tr>
				   </c:forEach>
			</tbody>
		</table>
	   <c:if test="${empty requestScope.list}">
	   		<!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
	        <div class="warning-option"><!-- <i></i> --><span>没有符合条件的信息</span></div>
	   </c:if>
		<c:if test="${releSetting.status ne 0}">
				<div class="m m1">
		   			<div class="mt">
		   				<h3>积分获得规则：</h3>
		   			</div>
					<div class="mc">
							<p>
								1. 成功注册会员：增加<font style="color: red;font-weight: bold;">${releSetting.reg==null?0:releSetting.reg}</font style="color: red;font-weight: bold;">积分；会员每天登录：增加<font style="color: red;font-weight: bold;">${releSetting.login==null?0:releSetting.login}</font>积分；评价完成订单：增加<font style="color: red;font-weight: bold;">${releSetting.productReview==null?0:releSetting.productReview}</font>积分。
								<br>
							</p>
					</div>
		   		</div>
	   		</c:if>
	   		<!--page-->
	   		 <div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
				            		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>  
				     			 </div>
		  				</div>    		
	</div>
	<!--page-->
		
		
		<div class="clear"></div>
</div>
<!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/userIntegral.js'/>"></script>
<script type="text/javascript">
$(function(){
	laydate.render({
		   elem: '#startDate',
		   calendar: true,
		   theme: 'grid',
		   trigger: 'click'
	  });
 
 laydate.render({
		   elem: '#endDate',
		   calendar: true,
		   theme: 'grid',
		   trigger: 'click'
	  });
});
</script>  
</body>
</html>
