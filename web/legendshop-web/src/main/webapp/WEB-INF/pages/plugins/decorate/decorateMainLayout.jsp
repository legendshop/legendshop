<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<c:forEach items="${shopDecotate.mainShopLayouts}" var="layout_main" varStatus="l">
        <c:choose>
               <c:when test="${layout_main.layoutType=='layout2'}">
                    <c:if test="${not empty layout_main.layoutModuleType}">
                         <div location_mark="${layout_main.layoutId}"  location="true" option="${layout_main.layoutId}" class="layout_two">
							<div mark="${layout_main.layoutId}" layout="layout2" type="${layout_main.layoutModuleType}"  id="content">
							</div>
						 </div>
                    </c:if>
			   </c:when>
			    
			    
			     <c:when test="${layout_main.layoutType=='layout3'}">
					     <c:if test="${not empty layout_main.shopLayoutDivDtos}">
					         <div location_mark="${layout_main.layoutId}" location="true" class="layout_three">
						       <c:forEach items="${layout_main.shopLayoutDivDtos}" var="layout_div" varStatus="d">
						         <c:choose>
										     <c:when test="${layout_div.layoutDiv=='div1'}">
										        <c:if test="${not empty layout_div.layoutModuleType}">
										          <div class="fl">
													 <div div="div1" option="${layout_main.layoutId}" class="module">
														<div div="div1" layout="layout3" mark="${layout_main.layoutId}" layoutDivId="${layout_div.layoutDivId}"  type="${layout_div.layoutModuleType}" id="content">
														</div>
													 </div>
											    	</div>
                                                </c:if>
										     </c:when>
										      <c:when test="${layout_div.layoutDiv=='div2'}">
										          <c:if test="${not empty layout_div.layoutModuleType}">
										               <div class="fr">
													     <div div="div2"  option="${layout_main.layoutId}" class="module">
														    <div div="div2" mark="${layout_main.layoutId}" layout="layout3" layoutDivId="${layout_div.layoutDivId}" type="${layout_div.layoutModuleType}" id="content">
														  </div>
													    </div>
												       </div>
                                                  </c:if>
										     </c:when>
								</c:choose>
						     </c:forEach>
						   </div>
						   <div style="clear: both;"></div>
					     </c:if>
				 </c:when>
				 
				 
				 <c:when test="${layout_main.layoutType=='layout4'}">
					    <c:if test="${not empty layout_main.shopLayoutDivDtos}">
					         <div location_mark="${layout_main.layoutId}"  location="true" class="layout_four">
						         <c:forEach items="${layout_main.shopLayoutDivDtos}" var="layout_div" varStatus="d">
							           <c:choose>
											 <c:when test="${layout_div.layoutDiv=='div2'}">
											   <c:if test="${not empty layout_div.layoutModuleType}"> 
											     <div class="fl">
										           <div div="div2" option="${layout_main.layoutId}" class="module">
										              <div div="div2" mark="${layout_main.layoutId}" layout="layout4" layoutDivId="${layout_div.layoutDivId}" type="${layout_div.layoutModuleType}" id="content">
										              </div>
										           </div>
										         </div>
	                                            </c:if>
											 </c:when>
											  
											  <c:when test="${layout_div.layoutDiv=='div1'}">
											     <c:if test="${not empty layout_div.layoutModuleType}">
											         <div class="fr">
												    	<div div="div1" option="${layout_main.layoutId}" class="module">
												            <div div="div1" mark="${layout_main.layoutId}"  layout="layout4" layoutDivId="${layout_div.layoutDivId}" type="${layout_div.layoutModuleType}" id="content">
												            </div>
												        </div>
												      </div>
	                                               </c:if>
											 </c:when>
									   </c:choose>
								 </c:forEach>
							 </div>
							<div style="clear: both;"></div> 
					    </c:if>
				</c:when>
				
			    
		</c:choose>
</c:forEach>