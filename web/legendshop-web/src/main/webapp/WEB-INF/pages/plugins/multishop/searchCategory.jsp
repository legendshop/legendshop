<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<c:set var="resultSize" value="${fn:length(requestScope.catDtoList)}" ></c:set>
<span class="search_cat_title"><b>匹配到${resultSize>10?10:resultSize}个类目</b>&nbsp;&nbsp;<a onclick="closeSearchCat();" style="background-color:#e5004f;"><b>X</b>&nbsp; 关闭</a></span>
<div class="search_cat_content" >
	<ul>
     	<c:forEach items="${requestScope.catDtoList}" var="catDto" end="9">
           <li onclick="selSearchCategory('${catDto.categoryId}','${catDto.navigationName}',this);"><a href="javascript:void(0);">
				<span>${catDto.navigationName}</span>
			</a></li>
          </c:forEach>
      </ul>
</div>
