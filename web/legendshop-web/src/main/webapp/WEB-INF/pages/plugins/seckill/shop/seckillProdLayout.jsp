<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/prodSelectAlert.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/active/promotionAddProds.css'/>" rel="stylesheet">
</head>
<body>
<div class="check-box">
	<form:form action="${contextPath}/s/seckillActivity/seckillProdLayout" id="form1" method="post">
		<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
		<div class="check-search">
			<input class="search-shop"  type="text" name="name" id="prodName" maxlength="20" value="${prod.name}" size="20" placeholder="请输入商品名称" />
			<input  class="red-btn" type="submit" value="搜索" />
		</div>
	</form:form>
	<div style="position: relative;display: block;">
		<table border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto;">
			<tbody>
			<tr style="background: #f9f9f9;">
				<td width="35px"></td>
				<td width="70px">图片</td>
				<td width="170px">商品名称</td>
				<td width="100px">规格</td>
				<td width="100px">可销售库存</td>
			</tr>
			<c:if test="${empty requestScope.list}">
				<tr><td colspan="5" style="text-align: center;">没有找到符合条件的商品</td></tr>
			</c:if>
			<c:forEach items="${requestScope.list}" var="product" varStatus="status">
				<tr class="first" prodId="${product.prodId}" >
					<td class="prodCheck">
						<label class="radio-wrapper">
								<span class="radio-item">
									<input type="radio" class="radio-input" value="${product.prodId}" name="prodId" />
									<span class="radio-inner"></span>
								</span>
						</label>
					</td>
					<td>
						<img src="<ls:images item='${product.pic}' scale='3' />" >
					</td>
					<td>
						<span class="name-text">${product.name}</span>
					</td>
					<td class="chooseSku"><span class="c skuNum">选择规格</span> <span class="addSku"style="display:none;color:red;font-size: 18px">+</span>
					<td>
						<div class="suc-icon" style="display: none">
							<span class="txt"></span>
						</div>
							${product.stocks}
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
		<div class="check-arr cur" style="display: none;">
			<table border="0" cellpadding="0" cellspacing="0"
				   style="width: 100%; margin: 0px;border: 0px solid #ddd;background:rgba(255,255,255,1)">
				<tbody class="skuList">
				<tr style="background: #FEF2F6; height: 40px">
					<th width="15%">
						<label class="checkbox-wrapper">
								<span class="checkbox-item">
									<input type="checkbox" id="checkbox" class="checkbox-input skuAll">
									<span class="checkbox-inner"></span>
								</span>
						</label>
					</th>
					<th width="50%">规格</th>
					<th width="25%">价格</th>
					<th width="25%">库存</th>
				</tr>
				</tbody>
			</table>
			<div class="bto-btn">
				<div class="red-btn skuSubmit">提交</div>
				<div class="red-btn close"
					 style="background: #F9F9F9;color: #000000;border:1px solid rgba(221,221,221,1)" ;>返回
				</div>
			</div>
		</div>
	</div>
	<div class="alert-btom">
		<div class="btom-page">
			<div style="margin-top:10px;" class="page clearfix">
				<div class="p-wrap">
					<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
				</div>
			</div>
		</div>
		<div class="btom-right">
			<%--<div>已选商品(2)</div>--%>
			<div class="red-btn" onclick="confirm();">确定</div>
			<div class="red-btn cancel" onclick="cancel();">取消</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    var contextPath = "${contextPath}";
</script>
<script type="text/javascript" src="/resources/templets/js/seckill/seckillProdLayOut.js"></script>
</body>
</html>