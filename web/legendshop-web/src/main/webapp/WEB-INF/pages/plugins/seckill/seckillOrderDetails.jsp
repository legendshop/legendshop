<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
 <%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%
	response.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT");   
	// 设置 HTTP/1.1 no-cache 头   
	response.setHeader("Cache-Control", "no-store,no-cache,must-revalidate");   
	// 设置 IE 扩展 HTTP/1.1 no-cache headers， 用户自己添加   
	response.addHeader("Cache-Control", "post-check=0, pre-check=0");   
	// 设置标准 HTTP/1.0 no-cache header.   
	response.setHeader("Pragma", "no-cache");
 %>
<!DOCTYPE html>
<html>
<head>
<!-- the same with indexFrame.jsp under red template -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="renderer" content="webkit"/>
    <c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
    <title>填写核对订单信息-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
    <link rel="stylesheet" href="<ls:templateResource item='/resources/common/css/jquery.autocomplete.css'/>">
    <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/plugins/showLoading/css/showLoading.css'/>" />
   
</head>
<body  class="graybody">
	<div id="doc">
         <!--顶部menu-->
		  <div id="hd">
		   
		      <!--hdtop 开始-->
		       <%@ include file="/WEB-INF/pages/plugins/main/home/header.jsp" %>
		      <!--hdtop 结束-->
		    
		       <!-- nav 开始-->
		        <div id="hdmain_cart">
		          <div class="yt-wrap">
		            <h1 class="ilogo">
		                <a href="${contextPath}/"><img src="<ls:photo item="${systemConfig.logo}"/>"  style="max-height:89px;" /></a>
		            </h1>
		            
		            <!--page tit-->
		              <div class="cartnew-title">
		                        <ul class="step_h">
		                            <li class="done"><span>购物车</span></li>
		                            <li class="on done"><span>填写核对订单信息</span></li>
		                            <li class=" "><span>提交订单</span></li>
		                        </ul>
		              </div>
		            <!--page tit end-->
		          </div>
		        </div>
		      <!-- nav 结束-->
		 </div><!--hd end-->
		<!--顶部menu end-->
		      
		 </div><!--hd end-->
		<!--顶部menu end-->
    
       
         <div id="bd" style="margin-bottom: 30px;">
              <div class="yt-wrap ordermain">
                    <div class="cart_done">
		               <span class="bighint2 gou2_back" style=" margin-bottom:10px;color: #c6171f;">温馨提示：您只有点击“确认订单”按钮之后，才会生成订单，提交订单前，请勿关闭本页面</span>
		          	</div>
		          	
                   <!-- 收货地址 -->
		           <%@ include file="/WEB-INF/pages/plugins/main/orderAddr.jsp" %>
		          
                     <!-- 支付方式 -->
		          <div id="pay" class="pay" style="display: none;">
		               <h3>支付方式</h3>
		              <div style="display: block;" id="pay_opened">
		              <ul>
		                <li>
		                   <input type="radio" checked="checked" value="33" id="online_pay" name="pay_opened_pm_radio">
		                   <label style="cursor:pointer;margin-right:80px;"  for="online_pay">在线支付</label>
		                   <em>支持绝大数银行卡及第三方在线支付</em>
		                </li>
		              </ul>
		            </div>
		          </div>
                  
                    <div class="pro-list">
		            <h3 style="margin-bottom: -15px;">商品清单</h3>
		            <div id="shoppinglistDiv">
		                <c:forEach items="${requestScope.userShopCartList.shopCarts}" var="carts" varStatus="status">
		                     <p style="margin-top: 15px;margin-bottom: -10px;">
				                                           店铺：<a style="color: #c6171f;" target="_blank" href="${contextPath}/store/${carts.shopId}" title="${carts.shopName}" >${carts.shopName}</a>
				             </p>
		                   <table class="cartnew_table">
								<thead style="border: 1px solid #e4e4e4;border-bottom: 0;">
									<tr>
										<th align="left" style="width:450px;">店铺宝贝</th>
										<th>数量</th>
										<th>秒杀价</th>
										<th>状态</th>
										<th>配送方式</th>
									</tr>
								</thead>
								<tbody style="border: 1px solid #e4e4e4;border-top: 0;">
			                    <c:forEach items="${carts.cartItems}" var="basket" varStatus="s">
				                    <tr>
				                        <td class="textleft"> 
						                        <div class="cartnew-img">
						                           <a class="a_e" target="_blank" href="${contextPath}/views/${basket.prodId}">
						                             <img title="${basket.prodName}" src="<ls:images item='${basket.pic}' scale='3' />" > 
						                           </a>                            
						                        </div>
					                           <ul class="cartnew-ul" >
					                           	<li> <a href="javascript:void(0);" target="_blank" class="a_e">${basket.prodName}</a></li>
				                                 <li>
				                                    <c:if  test="${not empty basket.cnProperties}" >
				                                       ${basket.cnProperties}
				                                    </c:if>
				                                 </li>
				                              </ul>
				                        </td>
				                        <td>
				                          ${basket.basketCount}
				                        </td>
				                         <td>
								               <span style="font-size:14px; color:#e5004f;" >
								                                                     ￥<fmt:formatNumber pattern="0.00" maxFractionDigits="2" groupingUsed="false" value="${basket.promotionPrice}"/>
								               </span>
				                         </td>

				                          <td>
											  <c:choose>
												  <c:when test="${empty requestScope.userShopCartList.provinceId}">
													  <span>有货</span>
												  </c:when>
												  <c:when test="${empty basket.stocks || basket.stocks==0}">
													  <span class="stocksErr">无货</span>
												  </c:when>
												  <c:when test="${basket.stocks == -1}">
													  <span class="stocksErr">区域限售</span>
												  </c:when>
												  <c:when test="${basket.stocks > 0 && basket.stocks<basket.basketCount}">
													  <span class="stocksErr">缺货，仅剩${basket.stocks}件</span>
												  </c:when>
												  <c:otherwise>
													  <span>有货</span>
												  </c:otherwise>
											  </c:choose>
				                        </td>

				                         <c:if test="${s.index==0}">
				                              <td  rowspan="${fn:length(carts.cartItems)}">
				                                       <c:choose>
												        <c:when test="${carts.freePostage}">
												                            商家承担运费
												        </c:when>
												        <c:otherwise>
												             <select shopId="${carts.shopId}"  class="delivery">
									                             <c:choose>
															        <c:when test="${empty carts.transfeeDtos}">
															         <option value="-1" style="padding: 5px 0px;">请选择配送方式</option>
															        </c:when>
															        <c:otherwise>
															             <c:forEach items="${carts.transfeeDtos}" var="transtype" varStatus="s">
												                            <option style="padding: 5px 0px;" transtype="${transtype.freightMode}" value="${transtype.deliveryAmount}" >${transtype.desc}</option>
												                        </c:forEach>
															         </c:otherwise>
															        </c:choose>
									                        </select>
												        </c:otherwise>
												      </c:choose>
				                                </td>
				                          </c:if>
				                    </tr>  
			                    </c:forEach>
		                    </tbody>   	  
		                 </table>
		                
		                  <div class="shop-add">
                              <div class="shop-wor">给卖家留言：<input type="text" placeholder="限45个字" name="remark" shopId="${carts.shopId}"  maxlength="45"></div>
                         </div>
		              </c:forEach>

		            <%--  暂时去掉
                     include file="/WEB-INF/pages/plugins/main/orderInvoice.jsp"
                      --%>
		       
		             <!-- 确认订单按钮 -->
			        <%@ include file="/WEB-INF/pages/plugins/main/orderConfirm.jsp" %>
		        </div>
		        <!--order content end-->
		       </div>
              </div>
	     </div>
	     
	     <form:form id="orderForm" action="${contextPath}/p/seckill/order/submit/${seckillId}/${md5}" method="post">
	            <input  name="payManner"  value="2" type="hidden"> 
		        <input id="remarkText" name="remarkText" type="hidden" />
		        <input id="invoiceIdStr" name="invoiceId" type="hidden" />
		        <input id="delivery" name="delivery" type="hidden"> 
		        <input  name="prodId"  value="${prodId}" type="hidden"> 
		        <input  name="skuId"  value="${skuId}" type="hidden"> 
		        <input name="token" type="hidden" value="${SECKILL_SESSION_TOKEN}"/>
		  </form:form>
		  
		  <form:form id="orderFormReload" action="${contextPath}/p/seckill/order/${seckillId}/${md5}/${prodId}/${skuId}/goOrder" method="GET">
		  </form:form>
		
	
		 <!----foot---->
		 <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
        <!----foot end---->
	</div>
</body>
   <script type="text/javascript">
		var contextPath = '${contextPath}';
   </script>
   <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
   <script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>"></script>
   <script  charset="utf-8" src="<ls:templateResource item='/resources/plugins/showLoading/js/jquery.showLoading.min.js'/>"></script> 
   <script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/seckill/seckillOrder.js'/>"></script>
   <script src="<ls:templateResource item='/resources/templets/js/addressSlide.js'/>"></script>
</html>
