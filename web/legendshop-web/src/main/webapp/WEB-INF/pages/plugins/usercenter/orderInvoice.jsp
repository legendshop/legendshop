<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/myInvoice.js'/>"></script>
<!-- 发票地址 -->
<div class="invoice" style="margin-top: 3px !important;">
	                <div class="invoice-tit invoice-open">
	                   <b>发票信息</b>
	                   <a onclick="showForm_invoice();" href="javascript:void(0)">[修改]</a>
	                </div>
	                 <div class="invoice-tit invoice-close" style="display: none;">
	                   <b>发票信息</b>
	                   <a onclick="close_invoice()" href="javascript:void(0)">[关闭]</a>
	                </div>
	                <c:choose>
	                    <c:when test="${empty userdefaultInvoice || userdefaultInvoice.content eq 1}">
		                    <div class="invoice-no-spr">
		                      <p>不需要发票</p>
		                   </div>
	                    </c:when>
						<c:when test="${userdefaultInvoice.content eq 2}">
						   <div class="invoice-no-spr">
			                   <p invoice="${userdefaultInvoice.id}">
			                    ${userdefaultInvoice.type eq 1 ? '普通发票' : ''}&nbsp;&nbsp;
			                    <c:choose><c:when test="${userdefaultInvoice.title ==1}">个人</c:when>
			                    <c:when test="${userdefaultInvoice.title ==2}">单位</c:when></c:choose>
			                    <c:if test="${not empty userdefaultInvoice.company}">( ${userdefaultInvoice.company} )</c:if>
			                    &nbsp;&nbsp;<c:if test="${userdefaultInvoice.content ==2}">明细</c:if>
			                   </p>
			                </div> 
						</c:when>
					</c:choose>
	               
	                <div class="invoice-spr"  style="display: none;">
	                  <div id="invoiceList">
	                  
	                  </div>
	                  
	                  <div class="invoice-spr-cho" style="margin-left: 20px;">
	                    <p>发票类型：
	                       <span>
		                         <!-- <input type="radio" value="1" checked="checked" id="invoince_InvoiceType" name="invoince_type"> -->
		                         <label class="radio-wrapper radio-wrapper-checked">
									<span class="radio-item">
										<input type="radio" class="radio-input" value="1" name="invoince_type" id="invoince_InvoiceType" checked="checked">
										<span class="radio-inner"></span>
									</span>
								</label>
	                       </span>
	                       <label for="invoince_InvoiceType">普通发票(纸质)</label>
	                     </p>
	                     <p>发票抬头：
	                       <span>
	                         <!-- <input type="radio" value="1"  checked="checked"  id="invoince_pttt1"  name="invoince_pttt" onclick="onPttt1();"> -->
	                          <label class="radio-wrapper radio-wrapper-checked">
									<span class="radio-item">
										<input type="radio" class="radio-input" value="1" name="invoince_pttt" id="invoince_pttt1" checked="checked" onclick="onPttt1(this);">
										<span class="radio-inner"></span>
									</span>
							  </label>
	                       </span>
	                       <label for="invoince_pttt1">个人</label>
	                       <span>
	                         <!-- <input type="radio" value="2"  id="invoince_pttt2"  name="invoince_pttt" onclick="onPttt2();">  -->
	                          <label class="radio-wrapper">
									<span class="radio-item">
										<input type="radio" class="radio-input" value="2" name="invoince_pttt" id="invoince_pttt2" onclick="onPttt2(this);">
										<span class="radio-inner"></span>
									</span>
							  </label>
	                       </span>
	                       <label for="invoince_pttt2">单位</label>
	                     </p>
	                     <div id="invoice_unitNameTr" style="display: block;">
	                          <input type="text" placeholder="发票抬头" maxlength="30" style="width:230px;height: 25px;margin-left:60px;padding:0 5px;" id="invoice_Unit_TitName">
	                          <label id="titNameNote"  class="item-con_error hide"></label>
	                     </div>
	                     <div id="invoice_humnumber" style="display: none;margin-top:10px">
	                          <input type="text" placeholder="纳税人识别号" maxlength="200" style="width:230px;height: 25px;margin-left:60px;padding:0 5px;" id="invoice_hum_number">
	                          <label id="titNameNote"  class="item-con_error hide"></label>
	                     </div>
	                    <p>发票内容：
	                      <span>
	                       <!-- <input type="radio" value="2"  checked="checked" name="invoince_content" id="invoince_content2"> -->
		                        <label class="radio-wrapper radio-wrapper-checked">
									<span class="radio-item">
										<input type="radio" class="radio-input" value="2" name="invoince_content" id="invoince_content2" checked="checked">
										<span class="radio-inner"></span>
									</span>
								</label>
	                       </span>
	                       <label for="invoince_content2">明细</label>
	                     </p>
	                  </div> 
	                  
	                  <div class="invoice-but">
	                    <a href="javascript:void(0);" onclick="savePart_invoice();" class="btn-r big-btn">保存发票信息</a>
	                    <a href="javascript:void(0);" onclick="no_invoice();" class="gray">不需要发票</a>
	                  </div>
	                  <div class="invoice-exp">您最多可以添加10条发票信息，请先删除部分不常用发票后再添加</div>
	                </div>
	           </div>

<!-- 发票地址 -->