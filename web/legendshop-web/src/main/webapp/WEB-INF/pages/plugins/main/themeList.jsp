<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>专题列表-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
</head>
<body class="graybody">   
    <%@ include file="home/top.jsp" %>
	<div id="doc">
      <c:if test="${empty requestScope.list}">
			<div style="height: 200px;text-align: center;">
			<p style="margin: auto;margin-top: 100px;font-size: 25px;">暂时没有专题活动，敬请期待...</p>
			</div>
		</c:if>
        <div class="shop-topic">
          <ul class="shop-topic-lis">
          <c:forEach items="${requestScope.list}" var="theme" varStatus="status">
	            <li>
	              <a href="javascript:void(0);" onclick="toDetail('${theme.themeId}');">
	                <dl>
	                  <dt><img width="500" height="300" src="<ls:photo item='${theme.themePcImg }'/>" alt="${theme.title}" onclick="toDetail('${theme.themeId}');"></dt>
	                  <dd><p style='color: ${theme.titleColor};font-size: 20px;line-height: 65px;'  align="center">${theme.title}</p></dd>
	                </dl>
	              </a>
	            </li>
            </c:forEach>
          </ul>
        <div class="clear"></div>
					<div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
				            		 
											<span class="p-num">
												<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
											</span>
										
				     			 </div>
		  				</div>
        </div>
        <%@ include file="home/bottom.jsp" %>
</div>
<script type="text/javascript">
	$(function(){
		$("#themeLi").addClass("focus");
	});
	function pager(curPageNO){
		window.location.href = "${contextPath}/theme/allThemes?curPageNO="+curPageNO;
	}
	
	function changePager(curPageNO){
		 window.location.href  = "${contextPath}/theme/allThemes?curPageNO="+curPageNO;
	}
	
	function toDetail(themeId){
		window.location.href= "${contextPath}/theme/themeDetail/"+themeId;
	}
	</script>
</body>
</html>