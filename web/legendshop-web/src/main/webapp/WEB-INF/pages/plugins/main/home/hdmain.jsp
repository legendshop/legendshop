<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<link rel="stylesheet" href="<ls:templateResource item='/resources/common/css/jquery.autocomplete.css'/>">
<c:set var="shopCartCounts"  value="${indexApi:getShopCartCounts(pageContext.request)}"></c:set>
<c:set var="hotSearchList"  value="${indexApi:findHotSearch()}"></c:set>
<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
 <!--hdmain 结束-->
    <div id="hdmain">
        <div class="yt-wrap top-main">
            <h1 class="ilogo">
                <a href="<ls:url address='/'/>">
		      		<img src="<ls:photo item="${systemConfig.logo}"/>"  style="max-height:89px;" />
                </a>
            </h1>

            <div id="J-MiniCart" class="icart cur-icart">
                <a href="${contextPath}/shopCart/shopBuy" class="minishop J-MiniShop">
                    <span>购物车<strong id="shppingItemCt"><c:if test="${shopCartCounts gt 99}">99+</c:if><c:if test="${shopCartCounts le 99}">${shopCartCounts}</c:if></strong>件</span>
                </a>
                <i id="J-MiniCart"></i>
            </div>
            <div class="isearch">
                <form:form  action="${contextPath}/list" method="get" id="searchform" name="searchform">
	                <div class="searchbox">
	                    <input type="text" autocomplete="off" value="<c:out value="${param.keyword}"></c:out>" name="keyword" id="_keyword"   class="place-holder">

	                    <button type="button" title="搜索" onclick="searchproduct();" id="J_SearchBtn">搜索</button>
	                </div>
                 </form:form>
                <div class="hotkeyword">
                 <c:forEach items="${hotSearchList}" var="hot" varStatus="status">
	         	   <c:choose>
	         	     <c:when test="${status.index%2==0}">
	         	        <a class="yt_hotword" href="javascript:goHotLink('${hot.msg}')">${hot.msg}</a>
	         	     </c:when>
	         	     <c:otherwise>
<%--	         	        <a class=""  href="${contextPath}/list?keyword=${hot.msg}">${hot.msg}</a>--%>
						 <a class="" href="javascript:goHotLink('${hot.msg}')">${hot.msg}</a>
	         	     </c:otherwise>
	         	  </c:choose>
	              </c:forEach>
                </div>
                <div class="search-tip" id="search-tip" style="display: none;">
		          <div class="search-history">
		            <div class="title">历史纪录<a href="javascript:void(0);" id="search-his-del">清除</a></div>
		            <ul id="search-his-list">

		            </ul>
		          </div>
		          <div class="search-hot">
		            <div class="title">热门搜索...</div>
		            <ul>
		            <c:forEach items="${hotSearchList}" var="hot" varStatus="status">
		              <li><a data-hot-value="${hot.msg}" hotsearch="" href="${contextPath}/list?keyword=${hot.msg}">${hot.msg}</a></li>
	                </c:forEach>
		            </ul>
		          </div>
		        </div>
            </div>
        </div>
      </div>
 <!--hdmain 结束-->
 <script type="text/javascript" src="<ls:templateResource item='/resources/common/js/jquery.autocomplete.js'/>"></script>
 <script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/hdmain.js'/>"></script>
 <script type="text/javascript" >

	 <%--var contextPth="${contextPath}";--%>

	 function goHotLink(msg){
		 msg = encodeURI(msg);
		 window.location.href = "${contextPath}/list?keyword="+msg;
	 }

  </script>
