<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<jsp:useBean id="nowTime" class="java.util.Date"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <c:set var="auctions" value="${list}"></c:set>
    <title>咨询(收费)-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/commonPage.css'/>" rel="stylesheet"/>
</head>

<style>
    body,
    div,
    span,
    header,
    footer,
    nav,
    section,
    aside,
    article,
    ul,
    dl,
    dt,
    dd,
    li,
    a,
    p,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    i,
    b,
    textarea,
    button,
    input,
    select,
    figure,
    figcaption {
        padding: 0;
        margin: 0;
        list-style: none;
        font-style: normal;
        text-decoration: none;
        border: none;
        font-family: "Microsoft Yahei", sans-serif;
        -webkit-tap-highlight-color: transparent;
        -webkit-font-smoothing: antialiased;

    }


    .main {
    }

    .header {
        width: 100%;
    }

    .main .bread {
        font-size: 12px;
        margin-left: 88px;
        margin-top: 18px;
    }

    .main .bread span {
        color: #E70553;
    }

    .content {
        width: 1088px;
        margin: 0 auto;
    }

    .title {
        text-align: center;
        margin-top: 15px;
        margin-bottom: 30px;
    }

    .title .title-line {
        width: 32px;
        height: 2px;
        background-color: #D73454;
        display: inline-block;
        margin-bottom: 28px;
    }

    .title h3 {
        font-size: 25px;
        color: #000;
    }

    .info {
        margin-bottom: 25px;
        clear: both;
        overflow: hidden;
    }

    .info .one-left {
        float: left;
        width: 360px;
    }

    .info .one-left img {
        width: 100%;
    }

    .info .one-right {
        float: right;
        width: 710px;
        margin-left: 18px;
    }

    .right-item {
        clear: both;
        overflow: hidden;
    }

    .one-right .right-top {
        margin-bottom: 18px;
    }

    .right-top .top-left {
        width: 255px;
        float: left;
    }

    .right-top .top-right {
        width: 416px;
        float: left;
        margin-left: 18px;
    }

    .bottom-top .bottom-left {
        float: left;
        width: 417px;
    }

    .bottom-top .bottom-right {
        float: left;
        width: 254px;
        margin-left: 18px;
    }

    .info-two img {
        float: left;
        width: 253px;
    }

    .footer {
        margin-top: 200px;
        text-align: center;
        padding: 17px;
        background-color: #2a282b;
    }

    .footer p {
        color: #ccc;
        font-size: 14px;
    }

    .footer p span {
        margin: 0 30px;
    }

    .footer .company {
        margin-bottom: 38px;
    }

    .textClass1 {
        position: absolute;
        bottom: 3px;
        text-align: center;
        width: 100%;
        line-height: 38px;
        font-size: 24px;
        background-color: rgba(0, 0, 0, 0.2);
        color: #FFF;
    }
    .textClass2 {
        position: absolute;
        bottom: 0px;
        text-align: center;
        width: 100%;
        line-height: 38px;
        font-size: 24px;
        background-color: rgba(0, 0, 0, 0.2);
        color: #FFF;
    }
    .textClass3 {
        position: absolute;
        bottom: 1px;
        text-align: center;
        width: 100%;
        line-height: 38px;
        font-size: 24px;
        background-color: rgba(0, 0, 0, 0.2);
        color: #FFF;
        left: 18px;
        overflow: hidden;
    }
    .textClass4 {
        position: absolute;
        bottom: 0px;
        text-align: center;
        width: 399px;
        line-height: 38px;
        font-size: 24px;
        background-color: rgba(0, 0, 0, 0.2);
        color: #FFF;
        left: 18px;
        overflow: hidden;
    }
    .textClass5 {
        position: absolute;
        bottom: 1px;
        text-align: center;
        width: 100%;
        line-height: 38px;
        font-size: 24px;
        background-color: rgba(0, 0, 0, 0.2);
        color: #FFF;
        left: 18px;
        overflow: hidden;
    }
    .textClass6 {
        position: absolute;
        bottom: 0px;
        text-align: center;
        width: 100%;
        line-height: 38px;
        font-size: 24px;
        background-color: rgba(0, 0, 0, 0.2);
        color: #FFF;
    }

</style>

<body>
<div id='auc_page'>
    <div id="doc">
        <div id="hd">
            <%@ include
                    file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
        </div>
        <div id="bd">
            <div class="seller-cru" style="width:1190px;margin: 20px auto 10px;">
                <p>
                    您的位置&gt;
                    <a href="#">首页</a>
                    &gt; <span style="color: #c6171f;">咨询(收费)</span>
                </p>
            </div>
            <!--两栏-->
            <div class=" yt-wrap " style="padding-top:0px;">
                <div class="content">
                    <div class="title">
                        <span class="title-line"></span>
                        <h3>INFORMATION (PAID)</h3>
                    </div>
                    <div class="info">
                        <div class="one-left" style="position: relative;float: left;">
                            <img class="msg" src="<ls:templateResource item='/resources/templets/images/consult/consult1.png'/>"/>
                            <div class="textClass1">多用户端搭建</div>
                        </div>
                        <div class="one-right">
                            <div class="right-item right-top">
                                <div style="position: relative;float: left;overflow: hidden;">
                                    <img class="top-left msg" src="<ls:templateResource item='/resources/templets/images/consult/consult2.png'/>"/>
                                    <div class="textClass2">商城营销布局</div>
                                </div>
                                <div style="position: relative;float: left;overflow: hidden;">
                                    <img class="top-right msg" src="<ls:templateResource item='/resources/templets/images/consult/consult3.png'/>"/>
                                    <div class="textClass3">微信小程序工具</div>
                                </div>
                            </div>
                            <div class="right-item bottom-top">
                                <div style="position: relative;width: 417px;float: left;">

                                    <img class="bottom-left msg" src="<ls:templateResource item='/resources/templets/images/consult/consult4.png'/>"/>
                                    <div class="textClass4">社交营销思维</div>
                                </div>
                                <div style="position: relative;width: 255px;float: left;">

                                    <img class="bottom-right msg" src="<ls:templateResource item='/resources/templets/images/consult/consult5.png'/>"/>
                                    <div class="textClass5">私有云应用</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="title">
                        <span class="title-line"></span>
                        <h3>INFORMATION (PAID)</h3>
                    </div>
                    <div class="info info-two" style="position: relative;">
                        <div style="position: relative;width: 255px;float: left; margin-left: 0px">
                            <img class="msg" src="<ls:templateResource item='/resources/templets/images/consult/consult21.png'/>"/>
                            <div class="textClass6">积分商城应用</div>
                        </div>
                        <div style="position: relative;width: 255px;float: left;margin-left: 18px">
                            <img class="msg" src="<ls:templateResource item='/resources/templets/images/consult/consult22.png'/>"/>
                            <div class="textClass6">大数据营销支持</div>
                        </div>
                        <div style="position: relative;width: 255px;float: left;margin-left: 18px">
                            <img class="msg" src="<ls:templateResource item='/resources/templets/images/consult/consult23.png'/>"/>
                            <div class="textClass6">手机用户分析指导</div>
                        </div>
                        <div style="position: relative;width: 255px;float: left;margin-left: 18px">
                            <img class="msg" src="<ls:templateResource item='/resources/templets/images/consult/consult24.png'/>"/>
                            <div class="textClass6">90后消费主力分析</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--两栏end-->
        </div>
    </div>

    <%@ include
            file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
</body>

<script type="text/javascript">
    var loginUserName = '${loginUserName}';
    $(function () {
        $(".msg").click(function () {
            layer.alert('付费后可观看，每条两元！！！', {icon: 0}, function () {
                if (isLogin()) {
                    login();
                    return;
                }
                layer.closeAll();
            });
        })

    })

    function isLogin() {
        if (loginUserName == "" || loginUserName == undefined || loginUserName == null) {
            return true;
        }
        return false;
    }

    function login() {
        layer.open({
            title: "登录",
            type: 2,
            content: contextPath + '/loadLoginOverlay', //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
            area: ['440px', '530px']
        });
    }

</script>
</html>