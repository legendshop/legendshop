<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>分销卖出的商品-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-sold${_style_}.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">分销卖出的商品</span>
				</p>
			</div>
				<%@ include file="included/sellerLeft.jsp" %>
				
	<div class="seller-sold">
		<div class="pagetab2">
				<ul id="listType">
					<li class="on"><span>分销卖出的商品</span></li>
				</ul>
   			</div>
		<div style="text-align:left;padding: 20px;border: 1px solid #e4e4e4;background: #f9f9f9;margin-top:15px;">
			<span>每个月的${distDay}号 清算上个月完成的订单，用户的佣金收入直接打款至预存款</span>
		</div>
		
		<form:form  id="ListForm" method="post"  action="${contextPath}/s/shopDistOrder" >
			<input type="hidden" value="${curPageNO==null?1:curPageNO}" name="curPageNO" id="curPageNO">
			<div class="sold-ser sold-ser-no-bg clearfix">
				<div class="fr">
					<span class="item">
						订单状态：<select name="status" class="item-sel" style="width: 86px;">
								<option  value=""  >所有订单</option>
								<%-- <option value="1"  <c:if test="${status==1}">selected="selected"</c:if>>待付款</option> --%>
								<option value="2"  <c:if test="${status==2}">selected="selected"</c:if>>待发货</option>
								<option value="3"  <c:if test="${status==3}">selected="selected"</c:if>>待收货</option>
								<option value="4"  <c:if test="${status==4}">selected="selected"</c:if>>已完成</option>
								<option value="5"  <c:if test="${status==5}">selected="selected"</c:if>>已取消</option>
						</select>
					</span>
					<span class="item">
						下单时间：<input type="text" style="width: 86px;" value='<fmt:formatDate value="${startDate}" pattern="yyyy-MM-dd" />'  readonly="readonly"  id="startDate" name="startDate"  class="hasDatepicker item-inp" >
						&nbsp;—&nbsp;<input type="text" style="width: 86px;" value='<fmt:formatDate value="${endDate}" pattern="yyyy-MM-dd" />' id="endDate" name="endDate" class="text  hasDatepicker item-inp" readonly="readonly">
					</span>
					<span class="item">
						分销用户：<input type="text" style="width: 86px;" value="${distUserName}" name="distUserName" class="item-inp">
					</span>
					<span class="item">
						流&nbsp;水&nbsp;号：
						<input type="text" value="${subItemNum}" name="subItemNum" class="item-inp" style="width:188px;" onblur="this.value=this.value.replace(/(^\s*)|(\s*$)/g, '')">
						<input type="button" onclick="search()" id="search_order" value="搜索" class="btn-r">
					</span>
				</div>
			</div>
		</form:form>
		
		<table class="sold-table">
			<tbody>
				<tr class="sold-tit">
					<th width="195">分销流水号</th>
					<th>商品</th>
					<th width="40">数量</th>
					<th width="90">商品总金额</th>
					<th width="100">分销用户</th>
					<th width="100">预计佣金支出</th>
					<th width="60">交易状态</th>
					<th width="90">下单时间</th>
					<th width="90" style="border-right: 1px solid #e4e4e4">完成时间</th>
				</tr>
			    <c:choose>
			           <c:when test="${empty requestScope.list}">
			              <tr>
						 	 <td colspan="20">
						 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
						 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
						 	 </td>
						 </tr>
			           </c:when>
			           <c:otherwise>
			               <c:forEach items="${requestScope.list}" var="subItem" varStatus="orderstatues">
							  <tr class="detail">
							  	<td style="padding: 8px 10px;">${subItem.subItemNumber}</td>
							  	<td><a target="_blank" href="${contextPath}/views/${subItem.prodId}">${subItem.prodName}</a></td>
							  	<td>${subItem.basketCount}</td>
							  	<td>${subItem.productTotalAmout}</td>
							  	<td>${subItem.distUserName}</td>
							  	<td>${subItem.distCommisCash}</td>
							  	<td><c:choose>
									 <c:when test="${subItem.subStatus==1 }">
									        待付款
									 </c:when>
									 <c:when test="${subItem.subStatus==2 }">
									       <span class="col-orange">待发货</span>
									 </c:when>
									 <c:when test="${subItem.subStatus==3 }">
									       待收货
									 </c:when>
									 <c:when test="${subItem.subStatus==4 }">
									      已完成
									 </c:when>
									 <c:when test="${subItem.subStatus==5 }">
									      交易关闭
									 </c:when>
								</c:choose>
							  	</td>
							  	<td><fmt:formatDate value="${subItem.subItemDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
							  	<td><fmt:formatDate value="${subItem.finallyDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
							  </tr>
							 </c:forEach>
			           </c:otherwise>
			       </c:choose>
			 </tbody>
		</table>
		<div style="margin-top:10px;" class="page clearfix">
			<div class="p-wrap">
				<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			</div>
		</div>
</div>

</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type="text/javascript">
	 var contextPath="${contextPath}";
	 $(document).ready(function() {
			userCenter.changeSubTab("shopDistOrder");
			laydate.render({
				   elem: '#startDate',
				   calendar: true,
				   theme: 'grid',
				   trigger: 'click'
			  });
	     
	     	laydate.render({
				   elem: '#endDate',
				   calendar: true,
				   theme: 'grid',
				   trigger: 'click'
			  });
	  	});
	 
	 function pager(curPageNO){
         document.getElementById("curPageNO").value=curPageNO;
         document.getElementById("ListForm").submit();
        }
        
    function search(){
	  	$("#ListForm #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	  	$("#ListForm")[0].submit();
	}
 </script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/ToolTip.js'/>"></script>
</body>
</html>
