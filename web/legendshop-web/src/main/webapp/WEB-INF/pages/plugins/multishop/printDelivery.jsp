<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%
	response.setHeader("cache-control","max-age=5,public,must-revalidate"); //one day
	response.setDateHeader("expires", -1);
%>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta http-equiv="Cache-Control" content="no-store" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/flash/swfobject.js'/>"></script>
   <title>${printData.title}</title>
</head>

<body>
	<div class="print-title">
		<span style="color:#d0d0d0">|</span>&nbsp;&nbsp;&nbsp;&nbsp;<Strong>${printData.title}</Strong>&nbsp;&nbsp;&nbsp;&nbsp;<span
			style="color:#d0d0d0">|</span>
	</div>
	<br>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<div align="center">
					<div id="dly_printer"
						style="height:${printData.height}px;width:${printData.width}px;">
						<div id="dly_printer_flash"></div>
					</div>
				</div></td>
		</tr>
	</table>
	   	<c:set var="bgimage" value="${printData.backgroung}"/>     
<script>
	window.onload = function(){
		if(!isHaveFalsh()){
	 		if(confirm("检测到您没有安装flash插件,导致不能正常打印发货单, 是否前往下载安装?")){
	 			window.open("https://get2.adobe.com/cn/flashplayer","_blank");
	 		}
	 	}
	}

    var xmldata = "${printData.xmlData}";
	var swfVersionStr = "10.0.0";
    var flashvars = {};
    
    flashvars.data = xmldata;
	flashvars.bg = "${bgimage}";
    var params = {};
    params.quality = "high";
    params.bgcolor = "#ffffff";
    params.allowscriptaccess = "always";
    params.allowfullscreen = "true";
    var attributes = {};
    attributes.id = "orderDesign";
    attributes.name = "orderDesign";
    swfobject.embedSWF(
        "<ls:templateResource item='/resources/common/js/flash/OrderPrint.swf'/>","dly_printer_flash",
        "100%", "100%", 
        swfVersionStr, {}, 
        flashvars, params, attributes);
        
    function isHaveFalsh(){
	  var flash = null;
	  if(typeof window.ActiveXObject != "undefined"){
	    flash = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
	  }else{
	    flash = navigator.plugins['Shockwave Flash'];
	  }
	  
	  return Boolean(flash);
	}
</script>
</body>
</html>