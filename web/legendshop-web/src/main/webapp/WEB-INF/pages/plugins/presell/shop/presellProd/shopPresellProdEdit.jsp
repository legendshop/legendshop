<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>编辑预售活动-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/presell/edit-presell-prod.css'/>"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css"/>
    <style type="text/css">
        .winput {
            width: 384px !important;
        }
    </style>
</head>
<body class="graybody">
<div id="doc">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
    <div class="w1190">
        <div class="seller-cru">
            <p>
                您的位置> <a href="${contextPath}/home">首页</a>> <a href="${contextPath}/sellerHome">卖家中心</a>> <font
                    class="on">添加预售活动</font>
            </p>
        </div>

        <%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
        <div class="presell-right">
            <form action="${contextPath}/s/presellProd/save" method="post" id="form1" enctype="multipart/form-data">

                <input type="hidden" value="${presellProd.id }" name="id" id="id"/>
                <!-- 基本信息 -->
                <div class="basic-ifo">
                    <!-- 基本信息头部 -->
                    <div class="ifo-top">
                        基本信息
                    </div>
                    <div class="ifo-con">
                        <li style="margin-bottom: 5px;">
                            <span class="input-title"><span class="required">*</span>活动名称：</span>
                            <input type="text" class="active-name" id="schemeName" name="schemeName"
                                   value="${presellProd.schemeName }" maxLength="25"
                                   placeholder="请填写活动名称"><em></em>
                        </li>
                        <li>
                            <span class="input-title"></span>
                            <span style="color: #999999;margin-top: 10px;">活动名称最多为25个字符</span>
                        </li>
                        <li style="margin-bottom: 0;">
                            <span class="input-title"><span class="required">*</span>活动时间：</span>
                            <fmt:formatDate value="${presellProd.preSaleStart}" pattern="yyyy-MM-dd HH:mm:ss"
                                            var="startTime"/>
                            <input type="text" class="active-time" readonly="readonly" name="preSaleStart"
                                   id="preSaleStart" value='${startTime}' placeholder="开始时间"/><em></em>
                            <span style="margin: 0 5px;">-</span>
                            <fmt:formatDate value="${presellProd.preSaleEnd}" pattern="yyyy-MM-dd HH:mm:ss"
                                            var="endTime"/>
                            <input type="text" class="active-time" readonly="readonly" name="preSaleEnd" id="preSaleEnd"
                                   value='${endTime}' placeholder="结束时间"/><em></em>
                        </li>
                    </div>
                </div>
                <!-- 选择活动商品 -->
                <div class="basic-ifo" style="margin-top: 20px;">
                    <!-- 选择活动商品头部 -->
                    <div class="ifo-top">
                        选择活动商品
                    </div>

                    <div class="ifo-con">
                        <div class="red-btn" onclick="showProdlist()" style="margin-left: 40px">选择商品</div>
                        <div class="check-shop prodList">
                            <c:if test="${not empty prodLists}">
                                <table border="0" id="skuList" cellpadding="0" cellspacing="0" style="margin: 30px 40px;">
                                    <tbody>
                                    <tr style="background: #f9f9f9;">
                                        <td width="70px">
                                            <label class="checkbox-wrapper">
								<span class="checkbox-item">
									<input type="checkbox" id="checkbox" class="checkbox-input selectAll">
									<span class="checkbox-inner"></span>
								</span>
                                            </label>
                                        </td>
                                        <td width="70px">商品图片</td>
                                        <td width="170px">商品名称</td>
                                        <td width="100px">商品规格</td>
                                        <td width="100px">商品价格</td>
                                        <td width="100px">可销售库存</td>
                                        <td width="100px">预售价(元)</td>
                                    </tr>
                                    <c:choose>
                                        <c:when test="${not empty prodLists}">
                                            <c:forEach items="${prodLists}" var="sku">
                                                <tr class="sku">
                                                    <td>
                                                        <label class="checkbox-wrapper">
								<span class="checkbox-item">
									<input type="checkbox" name="array" class="checkbox-input selectOne"
                                           onclick="selectOne(this);">
									<span class="checkbox-inner"></span>
								</span>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <img src="<ls:images item='${sku.skuPic}' scale='3' />">
                                                    </td>
                                                    <td>
                                                        <span class="name-text">${sku.skuName}</span>
                                                    </td>
                                                    <td>
                                                        <span class="name-text">${empty sku.cnProperties?'暂无':sku.cnProperties}</span>
                                                    </td>
                                                    <td>
                                                        <span class="name-text">${sku.prsellPrice}</span>
                                                    </td>
                                                    <td>${sku.stocks}</td>
                                                    <td>
                                                        <input type="text" class="set-input prsellPrice" id="prsellPrice" name="prsellPrice" style="text-align: center" value="${sku.prsellPrice}">
                                                        <input type="hidden" class="prsellProdId"  value="${sku.prodId}">
                                                        <input type="hidden" class="prsellSkuId" value="${sku.skuId}">
                                                        <input type="hidden" name="prodId" id="prodId" class="prodId" value="${sku.prodId}">
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </c:when>
                                        <c:otherwise>
                                            <tr class="first">
                                                <td colspan="7">暂未选择商品</td>
                                            </tr>
                                        </c:otherwise>
                                    </c:choose>
                                    </tbody>
                                </table>
                            </c:if>
                        </div>
                        <div class="check-bto" <c:if test="${empty prodLists}">style="display: none;margin:-11px 13px 0 0;float: right"</c:if> <c:if test="${not empty prodLists}">style="display: inline-block;margin: -11px 13px 0 0;float: right"</c:if>>
                            批量设置预售价格: <input type="text" id="batchPrice" maxlength="10">元
                            <div class="red-btn" style="display: inline-block;margin-left: 10px" id="confim">确定</div>
                        </div>
                    </div>
                </div>
                <!-- 活动规则 -->
                <div class="basic-ifo" style="margin-top: 20px;">
                    <!-- 活动规则头部 -->
                    <div class="ifo-top">
                        活动规则
                    </div>
                    <div class="ifo-con">
                        <input class="active-name winput" type="hidden" id="prePrice" name="prePrice" value="${presellProd.prePrice}">
                        <li>
                            <span class="input-title"><span class="required">*</span>支付设置：</span>
                            <em>
                                <%-- <input type="radio" id="payPctType-0" name="payPctType" value="0" <c:if test="${empty presellProd.payPctType or presellProd.payPctType eq 0}">checked="checked"</c:if>/><label for="payPctType-0">全额支付</label> --%>
                                <label class="radio-wrapper <c:if test="${empty presellProd.payPctType or presellProd.payPctType eq 0}">radio-wrapper-checked</c:if>">
									<span class="radio-item">
										<input type="radio" id="payPctType-0" name="payPctType" value="0"
                                               <c:if test="${empty presellProd.payPctType or presellProd.payPctType eq 0}">checked="checked"</c:if>
                                               class="radio-input"/>
										<span class="radio-inner"></span>
									</span>
                                    <span class="radio-txt">全额支付</span>
                                </label>
                                <%-- <input type="radio" id="payPctType-1" name="payPctType" value="1" <c:if test="${presellProd.payPctType eq 1}">checked="checked"</c:if>/><label for="payPctType-1">定金支付</label> --%>
                                <label class="radio-wrapper <c:if test="${presellProd.payPctType eq 1}">radio-wrapper-checked</c:if>">
									<span class="radio-item">
										<input type="radio" id="payPctType-1" name="payPctType" value="1"
                                               <c:if test="${presellProd.payPctType eq 1}">checked="checked"</c:if>
                                               class="radio-input"/>
										<span class="radio-inner"></span>
									</span>
                                    <span class="radio-txt">定金支付</span>
                                </label>
                            </em>
                            <div id="depositPay" class="deposit"
                                 <c:if test="${empty presellProd.payPctType or presellProd.payPctType eq 0}">style="display:none;"</c:if>>
                                <div><i><font color="red">*</font>定金百分比：</i> <input type="text" class="active-name"
                                                                                    style="width: 130px" maxlength="6"
                                                                                    id="preDepositPrice"
                                                                                    name="preDepositPrice"
                                                                                    value="${presellProd.preDepositPrice }"
                                                                                    <c:if test="${empty presellProd.payPctType or presellProd.payPctType eq 0}">disabled="disabled"</c:if>/> %
                                    <%--&nbsp;&nbsp;&nbsp;&nbsp;定金占百分比:--%>
                                    <%--<span id="payPctSpan" style="color:#e5004f;width:50px;"><fmt:formatNumber--%>
                                    <%--value="${presellProd.payPct*100 }" pattern="#0.0#"/></span>&nbsp;%--%>
                                </div>
                                <div>
                                    <i><font color="red">*</font>尾款支付时间：</i>
                                    <input readonly="readonly" class="active-time" type="text" id="finalMStart"
                                           name="finalMStart"
                                           value="<fmt:formatDate value='${presellProd.finalMStart}' pattern='yyyy-MM-dd HH:mm:ss'/>"
                                           <c:if test="${empty presellProd.payPctType or presellProd.payPctType eq 0}">disabled="disabled"</c:if>
                                           placeholder="请输入开始时间"/>
                                    -
                                    <input readonly="readonly" class="active-time" type="text" id="finalMEnd"
                                           name="finalMEnd"
                                           value="<fmt:formatDate value='${presellProd.finalMEnd}' pattern='yyyy-MM-dd HH:mm:ss'/>"
                                           <c:if test="${empty presellProd.payPctType or presellProd.payPctType eq 0}">disabled="disabled"</c:if>
                                           placeholder="请输入结束时间"/>
                                </div>
                            </div>
                        </li>
                        <li><span class="input-title"><span class="required">*</span>发货时间：</span><input
                                readonly="readonly" type="text" id="preDeliveryTimeStr" name="preDeliveryTimeStr"
                                value="<fmt:formatDate value='${presellProd.preDeliveryTime}' pattern='yyyy-MM-dd'/>"
                                class="active-time"/></li>
                    </div>
                </div>
                <div class="bto-btn">
                    <input type="submit" class="red-btn" value="提交"/>
                    <div class="red-btn return-btn" onclick="javascript:history.go(-1);">返回</div>
                </div>
            </form>
            <div class="add-info">
                <h3>添加预售说明:</h3>
                <ul>
                    <li><i>1</i>标记<span>*</span>为必填项</li>
                    <li><i>2</i>预售时间：活动开始时间不早于当前时间，结束时间与开始时间必须相隔24小时以上，前台生效后可以在此时间段付定金。</li>
                    <li><i>3</i>尾款时间：尾款支付开始时间为方案设置的预售结束时间， 3天后的第一个24点即结束时间，不可编辑！</li>
                    <li><i>4</i>发货时间： 晚于预售结束时间后的第一个24点,如果是定金支付则晚于尾款支付结束时间的第一个24点。!</li>
                    <li><i>5</i>如使用定金预售形式，建议先把要参加活动的商品库存设置为零，活动发布成功后，再把库存修改成正常库存。这样可防止商品被提前购买</li>
                </ul>
            </div>
        </div>
    </div>
    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<script src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
<script type="text/javascript">
    var contextPath = '${contextPath}';
    var oldPayPacType = '${presellProd.payPctType}';
    var oldDeliveryTime = '<fmt:formatDate value='${presellProd.preDeliveryTime}' pattern='yyyy-MM-dd HH:mm'/>';
    //方案名
    var schemeName = '${presellProd.schemeName}';
    $(function () {
        $("#payPctType-0").click(function () {
            $("#payPctType-0").parent(".radio-item").parent(".radio-wrapper").attr("class", "radio-wrapper radio-wrapper-checked");
            $("#payPctType-1").parent(".radio-item").parent(".radio-wrapper").attr("class", "radio-wrapper");
        });
        $("#payPctType-1").click(function () {
            $("#payPctType-1").parent(".radio-item").parent(".radio-wrapper").attr("class", "radio-wrapper radio-wrapper-checked");
            $("#payPctType-0").parent(".radio-item").parent(".radio-wrapper").attr("class", "radio-wrapper");
        });
    });
</script>
<script type="text/javascript" language="javascript"
        src="<ls:templateResource item='/resources/templets/js/presell/editPresellProd.js'/>"></script>
</body>
</html>
