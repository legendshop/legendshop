<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/sellerHome${_style_}.css'/>" rel="stylesheet"/>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller${_style_}.css'/>" rel="stylesheet"/>
<c:set var="shopMenuGroups"  value="${shopApi:getShopMenuGroups(pageContext.request)}"></c:set>
		<div class="seller-nav">
				<c:forEach items="${shopMenuGroups}" var="shopMenuGroup" varStatus="status">
							<ul>
								<li class="tra-man">
									<div class="tra-man-tit">
										<span><img alt="" src="<ls:templateResource item='/resources/templets/images/${shopMenuGroup.label}.png'/>">${shopMenuGroup.name}</span>
										<a class="sh" href="javascript:void(0);"><img alt="" src="<ls:templateResource item='/resources/templets/images/opt-dow.png'/>"></a>
									</div>
									<ol>
										<c:forEach items="${shopMenuGroup.shopMenuList}" var="shopMenu" varStatus="sts">
											<li id="${shopMenu.htmlId}"><a target="${shopMenu.target}" href="${contextPath}${shopMenu.urlList[0]}">${shopMenu.name}</a></li>
										</c:forEach>
									</ol>
								</li>
							</ul>
				</c:forEach>
			</div>
			<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/seller.js'/>"></script>
			<script src="<ls:templateResource item='/resources/common/js/alternative.js'/>" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$(".sh").click(function(){
		var ol= $(this).parent().next();
		if($(this).attr("status")!="1"){
			$(this).attr("status","1");
			ol.css("display","none");
			$(this).find("img").attr("src","<ls:templateResource item='/resources/templets/images/opt-rig.png'/>");
		}else{
			$(this).attr("status","0");
			ol.css("display","");
			$(this).find("img").attr("src","<ls:templateResource item='/resources/templets/images/opt-dow.png'/>");
		}
	});
	var orderBillLength=$("#orderBill").length;
	if(orderBillLength<=0){
		$("#shop-order-bill").hide();
	}
});
</script>