<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

<div class="item-reply" id="replyItem${productReply.id}" onmouseover ="showReplayBtn(${productReply.id});"  onmouseout="hideReplayBtn(${productReply.id});">
			                            		<strong></strong>
			                            	<div class="reply-list"> 
			                                	<div class="reply-con">
				                                    <span class="u-name">
				                                               <a rel="nofollow" href="javascript:void(0);" target="_blank">${productReply.replyName}</a>
				                                                <em>回复</em>
				                                                <c:choose>
				                                                	<c:when test="${productReply.parentReplyName !=null}">
				                                                		<a rel="nofollow" target="_blank" href="javascript:void(0);">${productReply.parentReplyName}</a>：
				                                                	</c:when>
				                                                	<c:otherwise>
				                                                		<a rel="nofollow" target="_blank" href="javascript:void(0);">${productReply.commName}</a>：
				                                                	</c:otherwise>
				                                                </c:choose>
				                                    </span>
			                                    	<span class="u-con">${productReply.replyContent}</span>
			                                	</div>
				                                <div class="reply-meta">
				                                    <span class="reply-left fl" style="float: left;"><fmt:formatDate value="${productReply.replyTime}" pattern="yyyy-MM-dd HH:mm:ss"  /></span>
				                                    <a style="visibility: hidden;" href="javascript:void(0);"  id="replayBtn${productReply.id}"  onclick="replayBtn(this);">回复</a>
				                                </div>
			                                	<div class="replay-form hide"  >
			                                    	<div class="arrow">
			                                        	<em>◆</em><span>◆</span>
			                                    	</div>
			                                    	<div class="reply-wrap">
			                                        	<p><em>回复</em> <span class="u-name">${productReply.replyName}：</span></p>
			                                        	<div class="reply-input">
				                                            <div class="fl"><input value="" type="text"></div>
				                                            <a href="javascript:void(0);" class="reply-btn btn-gray"  onclick="replyParent('${productReply.replyName}','${productReply.replyId}','${productReply.id}','${productReply.prodcommId}',this);">回复</a>
				                                            <div class="clr"></div>
			                                        	</div>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>