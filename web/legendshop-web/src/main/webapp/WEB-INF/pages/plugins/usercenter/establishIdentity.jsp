<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>账户安全 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/securityCenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/validateChanges.css'/>" rel="stylesheet" />
</head>
<style>
.btn-q {
    border-radius: 2px;
    text-align: center;
    background-color: #e5004f;
    color: #fff !important;
    padding: 0 10px;
    height: 28px;
    line-height: 28px;
    border: 0;
    outline: none;
    font-size: 12px;
    display: inline-block;
    vertical-align: top;
    box-sizing: border-box;
    -webkit-transition: all 0.2s ease;
    -moz-transition: all 0.2s ease;
    transition: all 0.2s ease;
    width: 110px;
}
</style>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con" id="content">
			<div class="o-mt"><h2>修改登录密码</h2></div>
	<div >
		<div class="right">
			<div id="step1" class="step step01">
				<ul>
					<li class="fore1">1.验证身份<b></b></li><li class="fore2">2.修改登录密码<b></b></li><li class="fore3">3.完成</li>
				</ul>
			</div>
			<form:form  id="smsCodeForm1" method="post" >
			<div class="m m1 safe-sevi" style="background:#fff;">
				<div class="mc">
        			
        			<div class="form form01">
        				<div class="item item01">
        					<span class="label">已验证手机：</span>
        					<div class="fl"><strong class="ftx-un" id="mobileSpan">${userSecMobile}</strong>
	        					<%--<a class="mailValA" href="${contextPath}/p/mailIdentity/${toPage}">通过已验证邮箱验证</a>&nbsp;&nbsp;--%>
								<%--<a href="javascript:void(0)">通过支付密码验证</a>&nbsp;&nbsp;--%>
				            	<div class="clr"></div>
        					</div>
        					<div class="clr"></div>
        				</div>
        				
        				<div class="item">
            				<span class="label">验证码：</span>
            				<div class="fl">
            					<input type="hidden" id="cannonull" name="cannonull" value='<fmt:message key="randomimage.errors.required"/>'/>
								<input type="hidden" id="charactors4" name="charactors4" value='<ls:i18n key="randomimage.charactors.required" length="4"/>'/> 
								<input type="hidden" id="errorImage" name="errorImage" value='<fmt:message key="error.image.validation"/>'/> 
												
								<input class="text" tabindex="2" name="randNum" id="randNum" type="text" maxlength="4" >
								<label><img id="randImage" name="randImage" src="<ls:templateResource item='/validCoderRandom'/>"  style="vertical-align: middle;"/>看不清？
            					<a href="javascript:void(0)" onclick="javascript:changeRandImg('${contextPath}')" style="font-weight: bold;"><fmt:message key="change.random2"/></a>
            					</label>
            					<div class="clr"></div>
            					<div id="authCode_error" class="msg-error"></div>
            				</div>
            				<div class="clr"></div>
            			</div>
        				
        				<div class="item">
        					<span class="label">&nbsp;</span>
        					<div class="fl" style="width: 450px">
        						<a class="btn-q" id="sendMobileCode" href="javascript:void(0)"><s></s>获取短信校验码</a>
        						<div class="clr" style="margin-bottom: 5px;"></div>
        						<div id="countDown"  class="msg-text" style="display: none;">校验码已发出，请注意查收短信，如果没有收到，你可以在<strong id="timer" class="ftx-01"></strong> 秒要求系统重新发送</div>
        						<div class="clr"></div>
        						<div class="msg-error" id="sendCode_error" style="display: none;"></div>
        					</div>
							<div class="clr"></div>
        				</div>
        				
        			</div>
        			
        			<div class="form">
        				
        				<div class="item">
        					<span class="label">请填写手机校验码：</span>
        					<div class="fl">
        						<input class="text" style="background: #f5f5f5;border:1px solid #a9a9a9;" name="code" tabindex="1" id="code" type="text" disabled="disabled" maxlength="6">
        						<input name="securityCode" id="securityCode" type='hidden' value=""/>
        						<div class="clr"></div>
        						<div id="code_error" class="msg-error" style="display:none"></div>
        					</div>
        					<div class="clr"></div>
        				</div>
        				
        				
        				
        				<div class="item">
        					<span class="label">&nbsp;</span>
							<input id="signedData" style="display:none" type="text">
							<input id="serialNumber" style="display:none" type="text">
        					<div class="fl">
        					<a id="submitCode" class="btn-r small-btn" href="javascript:void(0);" onclick="validCode();"><s></s>提交</a>
        					<a class="btn-r small-btn" href="${contextPath}/p/security"><s></s>返回</a>
        					</div>
        					<div class="clr"></div>
        				</div>
        			</div>
        		</div>
        	</div>
        	</form:form>
			<div class="m m7">
				<div class="mt"><h3>为什么要进行身份验证？</h3></div>
				<div class="mc">
					<div>1. 为保障您的账户信息安全，在变更账户中的重要信息时需要进行身份验证，感谢您的理解和支持。</div>
					<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
					<div>2. 验证身份遇到问题？请发送用户名、手机号、历史订单发票至<a href="javascript:void(0);" class="flk-05">${systemConfig.supportMail}</a>，客服将尽快联系您。</div>
				</div>
			</div>
		</div>
	</div>

</div>
		
 <!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script type="text/javascript"  src="<ls:templateResource item='/resources/common/js/randomimage.js'/>"></script>
	<script type="text/javascript"  src="<ls:templateResource item='/resources/templets/js/establishIdentity.js'/>"></script>
<script type="text/javascript">
	var wait;
	var timer = null;
	var interVal = 60;
	var contextPath = "${contextPath}";
	var userMobile = "${userMobile}";
	var securityCode = "${securityCode}";
	var mailVerifn = "${userSecurity.mailVerifn}";
	var phoneVerifn = "${userSecurity.phoneVerifn}";
	var paypassVerifn = "${userSecurity.paypassVerifn}";
	var _toPage = "${toPage}";
</script>
</body>
</html>