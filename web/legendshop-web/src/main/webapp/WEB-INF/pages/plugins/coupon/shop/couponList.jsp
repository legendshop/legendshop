<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<html>
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>优惠券管理-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/coupons.css'/>" rel="stylesheet">
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/coupon-admin${_style_}.css'/>" rel="stylesheet">
	<link rel="stylesheet" href="<ls:templateResource item='/resources/templets/css/multishop/couponTip.css'/>">
	<link rel="stylesheet" href="<ls:templateResource item='/resources/templets/css/multishop/tooltips.css'/>">
	<style type="text/css">
		.btn-r {
			width:auto !important;
			border-radius: 2px !important;
			height: 28px !important;
			line-height: 28px !important;
			font-size:12px !important;
		}
		.sold-table td{
			color:#999 !important;
		}
	</style>
</head>
<%@ page import="java.util.Date"%>
<!-- 获取当前日期 -->
<c:set var="nowDate">  
    <fmt:formatDate value="<%=new Date()%>" pattern="yyyy-MM-dd HH:mm:ss" type="date"/>  
</c:set> 
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">优惠券管理</span>
				</p>
			</div>
			<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
			
		<div class="seller-coupon-admin">
				<div class="pagetab2">
					<ul>
						<li class="on"><span>优惠券管理</span></li>
						<li><span><a class="but-serch" href="javascript:void(0);" onclick='window.location="<ls:url address='/s/coupon/load'/>"'>新增优惠券</a></span></li>      
					</ul>
        		</div>
<!-- 退换货导航 -->
				<div class="alert">
		            <h4>操作提示：</h4>
		            <ul>
		              <li>1. 手工设置优惠券失效以后，用户将不能领取该优惠券，但已领取的优惠券仍可以使用</li>
		              <li>2. 优惠券模板和已发放的优惠券过期后自动失效</li>
		              <li>3. 相关费用会在店铺的<em>“账期结算”</em>中扣除</li>
		            </ul>
		        </div>
				<div class="ret-sea">
					<form:form  action="${contextPath}/s/coupon/query" method="get" id="form1">
						<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
						<div class="sold-ser sold-ser-no-bg clearfix">
							<div class="fr">
								<div class="item">
									状态
									<select class="item-sel" name="status" id="status">
										<option value="" selected="">请选择</option>
										<option value="1">有效</option>
										<option value="0">失效</option>
										<option value="-1">过期</option>
									</select>
								</div>
								<div class="item">
									领取方式
									<select class="item-sel" name="getType" id="getType">
										<option value="" selected="">请选择</option>
										<option value="free">免费领取</option>
										<option value="pwd">卡密兑换</option>
										<option value="points">积分兑换</option>
		   					 	   </select>
								</div>
								<div class="item">
									优惠券名称
									<input type="text" class="item-inp" name="couponName" value="${coupon.couponName }"style="width: 200px;">
									<input type="submit" class="submit btn-r" value="搜索"/>
								</div>
							</div>
						</div>
      			  </form:form>
				</div>
				<table class="ret-tab sold-table">
					<tbody><tr class="tit">
						<th width="10"></th>
						<th>图片</th>
						<th>优惠券名称</th>
						<th width="90">消费金额</th>
						<th width="50">面额</th>
						<th width="200">有效期</th>
						<th width="90">领取方式</th>
						<th width="80">状态</th>
						<th width="220" style="border-right: 1px solid #e4e4e4">操作</th>
				    </tr>
				     <c:choose>
			           <c:when test="${empty requestScope.list}">
			              <tr>
						 	 <td colspan="20">
						 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
						 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
						 	 </td>
						 </tr>
			           </c:when>
			           <c:otherwise>
			              <c:forEach items="${requestScope.list}" var="item" varStatus="status">	
					    	<tr class="detail">
							<td width="10"></td>
							<td width="50" >
								<div class="pic-thumb">
									<c:choose>
										<c:when test="${empty item.couponPic}">
											<img  style="width:65px;height:48px;" src="${contextPath}/resources/templets/images/coupon/coupon-default.png" alt="">
										</c:when>
										<c:otherwise>
											<img src="<ls:images item='${item.couponPic}' scale='3'/>"/>
										</c:otherwise>
									</c:choose>
								</div>
							</td>
							<td>${item.couponName}</td>
						    <td>￥${item.fullPrice}</td>
						    <td>￥${item.offPrice}</td>
						    <td><fmt:formatDate value="${item.startDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" />~<fmt:formatDate value="${item.endDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd" /></td>
						    <td>
						    	<c:choose>
						            	<c:when test="${item.getType == 'free'}">免费领取</c:when>
						                <c:when test="${item.getType == 'points'}">积分兑换</c:when>
						                <c:when test="${item.getType == 'pwd'}">卡密兑换</c:when>
	            					</c:choose>
							</td>
						    <td>
						    	<c:choose>
		   							<c:when test="${nowDate<item.endDate}">
					   					<c:choose>
					            			<c:when test="${1==item.status}"><span class="col-orange">有效</span></c:when>
					                		<c:when test="${0==item.status}">失效</c:when>
					            		</c:choose>
		   							</c:when>
	   								<c:otherwise>过期</c:otherwise>
	   							</c:choose>
						    </td>
						    <td style="text-align:left;">
						    <a href="${contextPath}/s/coupon/watchCoupon/${item.couponId}" class="btn-r">查看</a>
						    <c:if test="${nowDate<=item.endDate}">
							    <c:choose>
							    	<c:when test="${item.status == 1}">
							    		<a href='javascript:confirmOffLine("${item.couponName }","${item.couponId }")' class="btn-g">下线</a>
							    		<c:if test="${item.getType ne 'pwd' && item.isDsignatedUser ne 1}">
								    		<a href="javascript:void(0)" data-id="${item.couponId }" class="btn-g share">推广</a>
							    		</c:if>
							    	</c:when>
							    	<c:otherwise>
							    		<a href='javascript:confirmOnLine("${item.couponName }","${item.couponId }")' class="btn-g">上线</a>
							    	</c:otherwise>
							    </c:choose>
						    </c:if>
						    <a href='javascript:confirmDelete("${item.couponId}","${fn:escapeXml(fn:replace(item.couponName,"' '","&acute;"))}")' style="margin-right:0px;" class="btn-g">删除</a>
						    </td>
						</tr>
						</c:forEach>		
			           </c:otherwise>
		           </c:choose>
				</tbody>
				</table>
				<div class="clear"></div>
				 <div style="margin-top:10px;" class="page clearfix">
	     			 <div class="p-wrap">
	            		<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	     			 </div>
  				</div>
				
			</div>
		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	
<img src="<ls:url address='/resources/common/images/indicator.gif'/>" class="editAdd_load" />

<script src="<ls:templateResource item='/resources/templets/js/multishop/jquery.pure.tooltips.js'/>"></script>
<script src="<ls:templateResource item='/resources/templets/js/multishop/qrcode.js'/>"></script>
<script src="<ls:templateResource item='/resources/templets/js/multishop/clipboard.min.js'/>"></script>
<script src="<ls:templateResource item='/resources/templets/js/multishop/couponList.js'/>"></script>

<script language="JavaScript" type="text/javascript">
   var vueDomainName = "${vueDomainName}";
   function confirmOnLine(name,id){
   		layer.confirm("'"+ name + "'优惠券上线？",{
	  		 icon: 3
	  	     ,btn: ['确定','取消'] //按钮
	  	   }, function () {
	  		 onLine(id);
		});
   }
   
   function confirmOffLine(name,id){
   		layer.confirm("'"+ name + "'优惠券下线？",{icon: 3, btn: ['确定','取消']}, function () {
	  		 offLine(id);
		});
   }

	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}
    
  $(document).ready(function(){
	  $("#status").val('${coupon.status}');
	  $("#getType").val('${coupon.getType}');
  });
	
    var contextPath="${contextPath}";
		
	function deleteById(id) {
		layer.confirm('你确定要删除该优惠券？',{
	  		 icon: 3
	  	     ,btn: ['确定','取消'] //按钮
	  	   }, function () {
	  		 window.location = "<ls:url address='/s/coupon/delete/" + id + "'/>";
		});
	}
</script>
</body>
</html>