<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
 <link href="<ls:templateResource item='/resources/templets/css/store/store.css'/>" rel="stylesheet" type="text/css">
 <script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/store/storeView.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
        <div class="ncs-chain-show">
                  <dl>
                    <dt>门店所在地区：</dt>
                    <dd>
                    <select class="combox sele" id="provinceid" name="provinceid" requiredTitle="true" childNode="cityid"  retUrl="${contextPath}/common/loadProvinces"></select> 
                     <select class="combox sele" id="cityid" name="cityid" requiredTitle="true"  showNone="false"  childNode="areaid" retUrl="${contextPath}/common/loadCities/{value}"></select>          
                    <select class="combox sele" id="areaid" name="areaid" requiredTitle="true"  showNone="false" retUrl="${contextPath}/common/loadAreas/{value}"></select>
				</dd>
                  </dl>
                  <div class="ncs-chain-list">
                    <ul nctype="chain_see">
                    </ul>
                 </div>
       </div>
<script type="text/javascript">
    var contextPath="${contextPath}";
    var currProdId = '${currProdId}';
 jQuery(document).ready(function($) {
	// call ajax by prodId
	var data={"poductId":currProdId}
		 $.ajax({
		        //提交数据的类型 POST GET
		        type:"GET",
		        //提交的网址
		        url:contextPath+"/storeProductCitys",
		        //提交的数据
		        async : true,  
		        //返回数据的格式
		        datatype: "json",
		        data:data,
		        //成功返回之后调用的函数            
		        success:function(data){
		        	fillHtml(data);
		        }       
		    });
	
   $("select.combox").initSelect();
   
   $("#areaid").change(function(){
	   var areaid=$(this).children('option:selected').val();  
	   if(areaid != '' && areaid!=null && areaid!=undefined){  
		   var provinceid=$("#provinceid").children('option:selected').val(); 
		   var cityid=$("#cityid").children('option:selected').val();
		   var areaid=$("#areaid").children('option:selected').val();
		   var data={"poductId":currProdId,"province":provinceid,"city":cityid,"areaid":areaid}
		   $.ajax({
		        //提交数据的类型 POST GET
		        type:"GET",
		        //提交的网址
		        url:contextPath+"/storeProductCitys",
		        //提交的数据
		        async : true,  
		        //返回数据的格式
		        datatype: "json",
		        data:data,
		        //成功返回之后调用的函数            
		        success:function(data){
		        	fillHtml(data);
		        }       
		    });
	   }  
   });
   
});

//展示页面
function fillHtml(data){
		        	var result=eval(data);
		        	var html="";
		        	$.each(result, function(i,val){      
		        		html+=" <li><div class='handle'><a href='javascript:void(0);'  onclick='buyNowStore("+val.storeId+");' >马上自提&gt;</a></div>";
		        		html+=" <h5><i></i><a target='_blank' href='javascript:void(0);'>"+val.storeName+"</a></h5>";
		        		html+="  <p>"+val.address+"</p></li>";
		            });
		        	$("ul[nctype='chain_see']").html(html);
}

</script>

</html>