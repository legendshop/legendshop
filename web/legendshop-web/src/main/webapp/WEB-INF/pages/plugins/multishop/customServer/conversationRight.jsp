<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>

<!-- 右边栏 -->
<div class="right-sidebar">
	<div class="sid-tab clear">
		<a href="javascript:void(0)" class="tab-item current">
			<span class="item-span" data-name="imConsult">正在咨询</span>
		</a>
		<a href="javascript:void(0)" class="tab-item">
			<span class="item-span" data-name="imOrder">本店订单</span>
		</a>
		<a href="javascript:void(0)" class="tab-item">
			<span class="item-span" data-name="imProd">本店商品</span>
		</a>
	</div>
	<!-- 正在咨询 -->
	<div class="tab-con tab-show rightContext">
		<div class="no-record">
			<i class="rec-i"></i>
			<p class="rec-p">暂无数据</p>
		</div>
	</div>
</div>
<!-- /右边栏 -->
