<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>商家导航列表-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-nav.css'/>" rel="stylesheet">
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<% Integer offset1 = 1; %>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">导航列表</span>
				</p>
			</div>
			
			<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
			
			
				<div class="seller-brand">
				<div class="seller-com-nav" style="margin-bottom:15px;">
					<ul>
						<li class="on"><a href="javascript:void(0);">头部菜单列表</a></li>
						<li><a id="brandConfigure"  href='${contextPath}/s/shopNav/load' >新增头部菜单</a></li>
					</ul>
				</div>
					<input type="hidden" id="curPageNO" name="curPageNO" value="1"/>
				<table class="brand-table sold-table"  cellspacing="0" cellpadding="0" >
					<tr class="brand-tit">
						<th>顺序</th>
						<th>导航标题</th>
						<th>状态</th>
						<th>打开方式</th>
						<th width="120">操作</th>
					</tr>
					  <c:choose>
				           <c:when test="${empty requestScope.list}">
				              <tr>
							 	 <td colspan="20">
							 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
							 	 	 <div class="warning-option"><!-- <i>&nbsp;</i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
							 	 </td>
							 </tr>
				           </c:when>
				           <c:otherwise>
				              	<c:forEach items="${requestScope.list}" var="item" varStatus="status">
									<tr class="detail">
										<td><%=offset1++ %></td>
										<td>${item.title}</td>
										<td>
										 <c:choose>
											<c:when test="${'1' eq item.status}">
												<img src="<ls:templateResource item='/resources/common/images/right.png'/> ">上线
											</c:when>
											<c:otherwise>
												<img src="<ls:templateResource item='/resources/common/images/wrong.png'/> ">下线
											</c:otherwise>
										</c:choose>
										</td>
										<td>
										 <c:choose>
					                		 <c:when test="${'cur_win' eq item.winType}">本页面</c:when>
					                		 <c:when test="${'new_win' eq item.winType}">新窗口</c:when>
					           			</c:choose>
										</td>
										<td>
											<a href='${contextPath}/s/shopNav/load/${item.id}'  title="修改" class="btn-r">修改</a>
											<a href='javascript:deleteById("${item.id}")' title="删除" class="btn-g">删除</a>
										</td>
									</tr>
								</c:forEach>
				           </c:otherwise>
			           </c:choose>	
				</table>
				<div class="clear"></div>
			    <div style="margin-top:10px;" class="page clearfix">
	     			 <div class="p-wrap">
	            		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	     			 </div>
		  	   </div>
		</div>
		</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type="text/javascript">
				var contextPath="${contextPath}";
			function pager(curPageNO){
			       location.href="${contextPath}/s/shopNav/list?curPageNO=" + curPageNO;
			 }
			function deleteById(id){
				 layer.confirm('确定删除该头部菜单 ?',{
			  		 icon: 3
			  	     ,btn: ['确定','取消'] //按钮
			  	   }, function () {
			  		 location.href="${contextPath}/s/shopNav/delete/"+id;
				})
			}
			
			$(document).ready(function(e) {
							userCenter.changeSubTab("shopNav"); //高亮菜单
			});
    </script>
</body>
</html>