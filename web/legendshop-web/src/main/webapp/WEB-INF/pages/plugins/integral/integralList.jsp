<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>积分商城-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link rel="stylesheet" type="text/css" media='screen' href="${contextPath}/resources/templets/css/productsort.css" />
</head>
<style>
.integralImg{ 
	width: 231px;
	height: 231px;
}
</style>
<body  style="background: #ffffff;">
	<div id="doc">
	    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
	    
			 <div class="seller-cru" style="width:1190px;margin: 20px auto 10px;">
	        <p>
	          您的位置&gt;
	          <a href="${contextPath}/home">首页</a>&gt;
	          <span style="color: #e5004f;">积分商城</span>
	        </p>
	      </div>
            
		   <div class=" yt-wrap" style="padding-top:10px;overflow: hidden;"> 
			            <%@ include file="/WEB-INF/pages/plugins/integral/integralCategorys.jsp" %>
			            
					<div id="main-nav-holder"  >
			            <%@ include file="integralProdList.jsp" %>
					</div>
		   </div> 
		  
			       
		 <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	    
	</div>
</body>
