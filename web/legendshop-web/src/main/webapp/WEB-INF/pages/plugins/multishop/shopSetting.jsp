<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>店铺设置-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-set-green.css'/>"
          rel="stylesheet">
    <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/layuiv2.3.0/css/layui.css'/>"
          media="all">
    <style type="text/css">
        #demo2 img {
            width: 600px;
            height: 300px;
        }
    </style>
</head>
<body class="graybody">
<div id="doc">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
    <div class="w1190">
        <div class="seller-cru">
            <p>
                您的位置>
                <a href="${contextPath}/home">首页</a>>
                <a href="${contextPath}/sellerHome">卖家中心</a>>
                <span class="on">店铺设置</span>
            </p>
        </div>

        <%@ include file="included/sellerLeft.jsp" %>
        <div class="seller-set">
            <div class="pagetab2">
                <ul id="listType">
                    <li class="on"><span>店铺设置</span></li>
                </ul>
            </div>
            <div class="ste-set">
                请设置您的店铺信息，标记<span>*</span>为必填项
            </div>
            <div class="set-ord">
                <form:form action="${contextPath}/s/shopSetting/update" method="post" id="form1"
                           enctype="multipart/form-data">
                    <input type="hidden" name="shopId" id="shopId" value="${shopSetting.shopId}"/>
                    <input type="hidden" name="province" id="province"/>
                    <input type="hidden" name="city" id="city"/>
                    <input type="hidden" name="area" id="area"/>
                    <table>
                        <tr>
                            <td class="table-t">用户名：</td>
                            <td><span>${shopSetting.userName}</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="table-t">店铺类型：</td>
                            <td><c:if test="${shopSetting.type != null}">
										<span><ls:optionGroup type="label" required="false" cache="true"
                                                              beanName="SHOP_TYPE" selectedValue="${shopSetting.type}"/>
										</span>
                            </c:if></td>
                        </tr>
                        <tr>
                            <td class="table-t">创建时间：</td>
                            <td><c:if test="${shopSetting.recDate!=null}">
										<span><fmt:formatDate value="${shopSetting.recDate}"
                                                              pattern="yyyy-MM-dd HH:mm"/>
										</span>
                            </c:if></td>
                        </tr>
                        <tr>
                            <td class="table-t"><span>*</span>店铺名称：</td>
                            <td><input type="text" style="width:337px;" name="siteName" id="siteName"
                                       value="${shopSetting.siteName}" maxlength="20"/></td>
                        </tr>

                        <tr>
                            <td class="table-t">店铺地址：</td>
                            <td>
                                <div class="select">
                                    <select class="combox " style="width:130px;" id="provinceid" name="provinceid"
                                            requiredTitle="true" childNode="cityid"
                                            selectedValue="${shopSetting.provinceid}"
                                            retUrl="${contextPath}/common/loadProvinces">
                                    </select> <select class="combox " style="width:116px;" id="cityid" name="cityid"
                                                      requiredTitle="true" selectedValue="${shopSetting.cityid}"
                                                      showNone="false" parentValue="${shopSetting.provinceid}"
                                                      childNode="areaid"
                                                      retUrl="${contextPath}/common/loadCities/{value}">
                                </select> <select class="combox " id="areaid" name="areaid" requiredTitle="true"
                                                  selectedValue="${shopSetting.areaid}" showNone="false"
                                                  parentValue="${shopSetting.cityid}"
                                                  retUrl="${contextPath}/common/loadAreas/{value}">
                                </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="table-t">详细地址：</td>
                            <td><input type="text" style="width:337px;" name="shopAddr" id="shopAddr"
                                       value="${shopSetting.shopAddr}" maxlength="300"/></td>
                        </tr>

                        <tr>
                            <td class="table-t">退货地址：</td>
                            <td>
                                <div class="select">
                                    <select class="combox " style="width:130px;" id="returnProvinceId"
                                            name="returnProvinceId" requiredTitle="true" childNode="returnCityId"
                                            selectedValue="${shopSetting.returnProvinceId}"
                                            retUrl="${contextPath}/common/loadProvinces"></select>
                                    <select class="combox " style="width:116px;" id="returnCityId" name="returnCityId"
                                            requiredTitle="true" selectedValue="${shopSetting.returnCityId}"
                                            showNone="false" parentValue="${shopSetting.returnProvinceId}"
                                            childNode="returnAreaId"
                                            retUrl="${contextPath}/common/loadCities/{value}"></select>
                                    <select class="combox " id="returnAreaId" name="returnAreaId" requiredTitle="true"
                                            selectedValue="${shopSetting.returnAreaId}" showNone="false"
                                            parentValue="${shopSetting.returnCityId}"
                                            retUrl="${contextPath}/common/loadAreas/{value}"></select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="table-t">退货详细地址：</td>
                            <td><input type="text" style="width:337px;" name="returnShopAddr" id="returnShopAddr"
                                       value="${shopSetting.returnShopAddr}" maxlength="300"/></td>
                        </tr>
                        <tr>
                            <td class="table-t">营业时间：</td>
                            <td><input type="text" style="width:337px;" name="businessHours" id="businessHours"
                                       value="${shopSetting.businessHours}" maxlength="20"/> <span class="gray-span">格式：时:分:秒-时:分:秒</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="table-t"><span>*</span>联系人姓名：</td>
                            <td><input type="text" style="width:337px;" name="contactName" id="contactName"
                                       value="${shopSetting.contactName}" maxlength="20"/> <span class="gray-span">不能超过20个字符</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="table-t"><span>*</span>手机号码：</td>
                            <td><input type="text" style="width:337px;" name="contactMobile" id="contactMobile"
                                       value="${shopSetting.contactMobile}" maxlength="11"/></td>
                        </tr>
                        <tr>
                            <td class="table-t">固定电话：</td>
                            <td><input type="text" style="width:337px;" name="contactTel" id="contactTel"
                                       value="${shopSetting.contactTel}" maxlength="20"/><span class="gray-span">格式：区号-电话号码</span>
                            </td>
                        </tr>
                        <tr>
                            <td class="table-t">联系邮箱：</td>
                            <td><input type="text" style="width:337px;" name="contactMail" id="contactMail"
                                       value="${shopSetting.contactMail}" class="input_template" maxlength="100"/></td>
                        </tr>
                        <tr>
                            <td class="table-t">QQ号码：</td>
                            <td><input type="text" style="width:337px;" name="contactQQ" id="contactQQ"
                                       value="${shopSetting.contactQQ}"
                                       onkeyup="value=value.replace(/[\uFF00-\uFFFF]/g,'')"
                                       onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\uFF00-\uFFFF]/g,''))"
                                       maxlength="50"/> <span class="gray-span">多个qq请用英文逗号","隔开</span>
                            </td>
                        </tr>

                        <tr>
                            <td class="table-t">简要描述：</td>
                            <td><input type="text" style="width:337px;" name="briefDesc" id="briefDesc"
                                       value="${shopSetting.briefDesc}" maxlength="300"></td>
                        </tr>

                        <tr>
                            <td class="table-t">是否开启发票：</td>
                            <td>
                                <label class="radio-wrapper <c:if test="${shopSetting.switchInvoice eq 1}">radio-wrapper-checked</c:if>">
										<span class="radio-item">
											<input type="radio" id="open" name="switchInvoice" value="1"
                                                   class="radio-input"/>
											<span class="radio-inner"></span>
										</span>
                                    <span class="radio-txt">开</span>
                                </label>
                                <label class="radio-wrapper <c:if test="${shopSetting.switchInvoice ne 1}">radio-wrapper-checked</c:if>">
										<span class="radio-item">
											<input type="radio" id="close" name="switchInvoice" value="0"
                                                   class="radio-input" style="margin-left:20px;"/>
											<span class="radio-inner"></span>
										</span>
                                    <span class="radio-txt">关</span>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td class="table-t">详情描述：</td>
                            <td><textarea rows="5" cols="50" name="detailDesc" id="detailDesc"
                                          maxlength="300">${shopSetting.detailDesc}</textarea></td>
                        </tr>

                        <tr class="businessTable">
                            <td align="center" class="table-t"><b>营业证照信息</b></td>
                        </tr>
                        <tr class="businessTable">
                            <td class="table-t">企业名称：</td>
                            <td>${shopSetting.companyName}</td>
<%--                            <input type="text" style="width:337px;" name="enterpriseName" id="enterpriseName"--%>
<%--                                   maxlength="50" value="${shopSetting.enterpriseName}">--%>
                        </tr>
                        <tr class="businessTable">
                            <td class="table-t">营业执照注册号：</td>
                            <td><input type="text" style="width:337px;" name="registerNumber" id="registerNumber"
                                       maxlength="100" value="${shopSetting.registerNumber}"></input></td>
                        </tr>
                        <tr class="businessTable">
                            <td class="table-t">法定代表人姓名：</td>
                            <td><input type="text" style="width:337px;" name="representative" id="representative"
                                       maxlength="50" value="${shopSetting.representative}"></input></td>
                        </tr>
                        <tr class="businessTable">
                            <td class="table-t">营业执照所在地：</td>
                            <td><input type="text" style="width:337px;" name="businessAddress" id="businessAddress"
                                       maxlength="50" value="${shopSetting.businessAddress}"></input></td>
                        </tr>
                        <tr class="businessTable">
                            <td class="table-t">企业注册资金：</td>
                            <td><input type="text" style="width:337px;" name="registerAmount" id="registerAmount"
                                       maxlength="50" value="${shopSetting.registerAmount}"></input></td>
                        </tr>
                        <tr class="businessTable">
                            <td class="table-t">营业执照有效期：</td>
                            <td><input type="text" style="width:337px;" name="availableTime" id="availableTime"
                                       maxlength="50" value="${shopSetting.availableTime}"></input></td>
                        </tr>
                        <tr class="businessTable">
                            <td class="table-t">营业执照经营范围：</td>
                            <td><textarea rows="5" cols="50" name="businessRange" id="businessRange"
                                          maxlength="300">${shopSetting.businessRange}</textarea></td>
                        </tr>

<%--                        <tr>--%>
<%--                            <td class="table-t">营业执照图片：</td>--%>
<%--                            <td><p style="width: 510px;float: left;line-height: 94px;">--%>
<%--                                <a href="javascript:void(0);" class="file-i">选择文件--%>
<%--                                    <input type="file" id="businessImageFile" name="businessImageFile" value="选择文件"--%>
<%--                                           style="width:152px;">--%>
<%--                                </a>--%>
<%--                                <span class="gray-span">（建议大小198*60） 用于店铺营业执照图片展示 </span>--%>
<%--                            </p>--%>
<%--                            </td>--%>
<%--                        </tr>--%>

<%--                        <c:choose>--%>
<%--                            <c:when test="${not empty shopSetting.businessImage}">--%>
<%--                                <tr>--%>
<%--                                    <td class="table-t"></td>--%>
<%--                                    <td>--%>
<%--                                        <p style="width: 200px;height: 74px;">--%>
<%--                                            <img id="images5" src="<ls:photo item='${shopSetting.businessImage}'/>"--%>
<%--                                                 style="height: 74px;"/>--%>
<%--                                        </p>--%>
<%--                                    </td>--%>
<%--                                </tr>--%>
<%--                            </c:when>--%>
<%--                            <c:otherwise>--%>
<%--                                <tr>--%>
<%--                                    <td class="table-t"></td>--%>
<%--                                    <td>--%>
<%--                                        <img id="images5" src="" style="width:74px; display: none;">--%>
<%--                                    </td>--%>
<%--                                </tr>--%>
<%--                            </c:otherwise>--%>
<%--                        </c:choose>--%>


                            <%--							<fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">--%>
                            <%--								<a href="javascript:void(0);" class="file-i">选择文件--%>
                            <%--								</a>--%>
                            <%--								<legend>上传多张图片</legend>--%>
                            <%--							</fieldset>--%>

                            <%--                            <tr>--%>
                            <%--                                <td class="table-t">营业执照图片：</td>--%>
                            <%--                                <td><p style="width: 510px;float: left;line-height: 94px;">--%>
                            <%--                                    <a href="javascript:void(0);" class="file-i">选择文件--%>
                            <%--                                        <input type="file" id="businessImageFile"  name="businessImageFile" value="选择文件" style="width:152px;" >--%>
                            <%--                                    </a>--%>
                            <%--                                    <span class="gray-span">（建议大小198*60） 用于店铺营业执照图片展示 </span>--%>
                            <%--                                </p>--%>
                            <%--                                </td>--%>
                            <%--                            </tr>--%>

                            <%--							<c:choose>--%>
                            <%--								<c:when test="${not empty shopSetting.businessImage}">--%>
                            <%--									<tr>--%>
                            <%--										<td class="table-t"></td>--%>
                            <%--										<td>--%>
                            <%--											<p style="width: 200px;height: 74px;">--%>
                            <%--												<img  id="images5" src="<ls:photo item='${shopSetting.businessImage}'/>" style="height: 74px;" />--%>
                            <%--											</p>--%>
                            <%--										</td>--%>
                            <%--									</tr>--%>
                            <%--								</c:when>--%>
                            <%--								<c:otherwise>--%>
                            <%--									<tr>--%>
                            <%--										<td class="table-t"></td>--%>
                            <%--										<td>--%>
                            <%--											<img  id="images5" src=""   style="width:74px; display: none;">--%>
                            <%--										</td>--%>
                            <%--									</tr>--%>
                            <%--								</c:otherwise>--%>
                            <%--							</c:choose>--%>


                        <tr>
                            <td class="table-t">商城图片：</td>
                            <td><p style="width: 510px;float: left;line-height: 94px;">
                                <a href="javascript:void(0);" class="file-i">选择文件
                                    <input type="file" id="file" name="file" value="选择文件" style="width:152px;">
                                </a>
                                <span class="gray-span">（建议大小198*60） 用于商品详情页面店铺Logo展示 </span>
                            </p>
                            </td>
                        </tr>


                        <c:choose>
                            <c:when test="${not empty shopSetting.shopPic}">
                                <tr>
                                    <td class="table-t"></td>
                                    <td>
                                        <p style="width: 200px;height: 74px;">
                                            <img id="images1" src="<ls:photo item='${shopSetting.shopPic}'/>"
                                                 style="height: 74px;"/>
                                        </p>
                                    </td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr>
                                    <td class="table-t"></td>
                                    <td>
                                        <img id="images1" src="" style="width:74px; display: none;">
                                    </td>
                                </tr>
                            </c:otherwise>
                        </c:choose>


                        <tr>
                            <td class="table-t">商城图片2：</td>
                            <td>
                                <p style="width: 510px;float: left;line-height: 94px;">
                                    <a href="javascript:void(0);" class="file-i">选择文件
                                        <input type="file" name="file2" id="file2" style="width:152px;"/>
                                    </a>
                                    <span class="gray-span">（建议大小100*100）用于卖家中心首页Logo展示 </span>
                                </p>
                            </td>
                        </tr>

                        <c:choose>
                            <c:when test="${not empty shopSetting.shopPic2}">
                                <tr>
                                    <td class="table-t"></td>
                                    <td>
                                        <p style="width: 200px;height: 74px;">
                                            <img id="images2" src="<ls:photo item='${shopSetting.shopPic2}'/>"
                                                 style="height: 74px;"/>
                                        </p>
                                    </td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr>
                                    <td class="table-t"></td>
                                    <td>
                                        <img id="images2" src="" style="width:74px; display: none;">
                                    </td>
                                </tr>
                            </c:otherwise>
                        </c:choose>


                        <tr>
                            <td class="table-t">商城图片banner：</td>
                            <td>
                                <p style="width: 510px;float: left;line-height: 94px;">
                                    <a href="javascript:void(0);" class="file-i">选择文件
                                        <input type="file" name="bannerfile" id="bannerfile" style="width:152px;"/>
                                    </a>
                                    <span class="gray-span">（建议大小1920*96）用于店铺装修</span>
                                </p>
                            </td>
                        </tr>

                        <c:choose>
                            <c:when test="${not empty shopSetting.bannerPic}">
                                <tr>
                                    <td class="table-t"></td>
                                    <td>
                                        <p style="width: 400px;height: 74px;">
                                            <img id="images3" src="<ls:photo item='${shopSetting.bannerPic}'/>"
                                                 style="width:100%;height: 74px;"/>
                                        </p>
                                    </td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr>
                                    <td class="table-t"></td>
                                    <td>
                                        <img id="images3" src="" style="width:74px; display: none;">
                                    </td>
                                </tr>
                            </c:otherwise>
                        </c:choose>

                        <tr class="businessTable">
                            <td class="table-t">营业执照及其他证照：</td>
                            <td><p style="width: 510px;float: left;line-height: 94px;">
                                <div class="layui-upload">
                                    <a href="javascript:void(0);" id="uploadimg" class="file-i">选择图片
                                    </a>
                                    <a href="javascript:void(0);" id="deleteimg" class="file-i">清空
                                    </a>
                                    <input type="text" name="businessImage" id="businessImage" style="width:152px;display: none;"value="${shopSetting.businessImage}"/>
                                    <blockquote class="layui-elem-quote layui-quote-nm" style="margin-top: 10px;">
                                        预览图：
                                        <div class="layui-upload-list" id="demo2"></div>
                                    </blockquote>
                                </div>
                                </p>
                            </td>
                        </tr>

                        <tr style="">
                            <td class="table-t"></td>
                            <td>
                                <div class="set-save" style="text-align: center;">
                                    <a href="javascript:void(0);" class="btn-r"
                                       style="width: 270px;margin: 20px 44px;font-size:16px;"
                                       onclick="saveShop();">保存设置</a>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <input type="hidden" id="h_switchInvoice" name="switchInvoice" value="${shopSetting.switchInvoice}">
                </form:form>

            </div>
        </div>

    </div>

    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>


<script charset="utf-8" src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
<script type="text/javascript" language="javascript"
        src="<ls:templateResource item='/resources/templets/js/multishop/shop_set.js'/>"></script>
<script src="<ls:templateResource item='/resources/plugins/layuiv2.3.0/layui.js'/>" charset="utf-8"></script>
<script type="text/javascript">
    var oldSiteName = "${shopSetting.siteName}";
    var imgUrls = "${shopSetting.businessImage}";
    var shopType = "${shopSetting.type}";
    $(function () {
        $("#open").click(function () {
            $("#open").parent(".radio-item").parent(".radio-wrapper").attr("class", "radio-wrapper radio-wrapper-checked");
            $("#close").parent(".radio-item").parent(".radio-wrapper").attr("class", "radio-wrapper");
        });
        $("#close").click(function () {
            $("#close").parent(".radio-item").parent(".radio-wrapper").attr("class", "radio-wrapper radio-wrapper-checked");
            $("#open").parent(".radio-item").parent(".radio-wrapper").attr("class", "radio-wrapper");
        });
    });

    $(function () {
        if (shopType == 0){
        $(".businessTable").hide();
        }
    })

    var urls = new Array();
    var i = 0;
    $(function () {
        if (imgUrls!=null&&imgUrls!=""){
        urls = imgUrls.split(",");
        i = urls.length;
        for (var x=0;x<urls.length;x++)
        {
            var txt = urls[x];
            $('#demo2').append("<img src='<ls:photo item='"+txt+"' />' alt=''>");
        }
        }
        $('#deleteimg').click(function (){
        $('#demo2').empty();
        urls = new Array();
        i = 0;
    })
    })
    <%--var url = "<ls:photo item='"+data+"' />";--%>
    <%--$(".img-files-img ul").append("<li><img style='max-width:150px;max-height:150px;' src='"+url+"' alt=''><a  fileName='"+data+"' href='javascript:void(0);' onclick='delImagesPath(this);'>删除</a><input name='imgurl' value='"+data+"' type='hidden' ></input></li>");--%>
    layui.use('upload', function () {
        var $ = layui.jquery
            , upload = layui.upload;
        //多图片上传
        upload.render({
            elem: '#uploadimg'
            , url: contextPath + '/s/uploadImg' //改成您自己的上传接口
            , auto: true
            , accept: "images"
            , multiple: true
            , before: function (obj) {
            }
            , done: function (res) {
                // i = urls.length
                //上传完毕
                var url = res.url;
                $('#demo2').append("<img src='<ls:photo item='"+url+"' />' alt=''>");
                urls[i++] = url;
            }
        });
    });
</script>
</body>
</html>
