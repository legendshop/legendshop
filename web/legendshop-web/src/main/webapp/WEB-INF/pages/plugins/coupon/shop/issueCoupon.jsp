<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<html>
<head>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<style>
.shadowBoxWhite {
	background: none repeat scroll 0 0 #fff;
	box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.3);
	margin-top: 10px;
	width: 550px;
	height: 120px;
	
}

body {
	color: #333333;
	font-family: "微软雅黑", "宋体";
	font-size: 12px;
	line-height: 180%;
	text-align: left;
}

.input_text,.lvPrice,.ipt {
	border: 1px solid #d7d7d7;
	border-radius: 3px;
	display: inline-block;
	font-size: 12px;
	line-height: 14px;
	padding: 7px 0 7px 5px;
	width: 300px;
}

input {
	outline: medium none;
}


.button, a.l-btn span.l-btn-left {
    background-color: #f7f7f7;
    border: 1px solid #d1d1d1;
    border-radius: 3px;
    color: #666666;
    cursor: pointer;
    display: inline-block;
    height: 28px;
    line-height: 28px;
    margin-right: 5px;
    padding: 0 20px;
}

.loading-indicator-bars {
		background-image: url('<ls:templateResource item='/plugins/showLoading/images/loading-bars.gif'/>');
		width: 150px;
}

.editAdd_load{position: absolute;top:50%;left:50%;margin:-16px 0 0 -16px;z-index: 99;display: none;}
</style>
</head>
<body>
	<div style="padding: 10px 0px 10px 10px;" class="shadowBoxWhite  whiteBox">
			<label>发放数量：</label> 
			<input type="text" class="input_text" name="createnum" id="createnum">
			 <a onclick="submitForm()" style="padding-left: 30px;" class="easyui-linkbutton l-btn" href="javascript:void(0)" group="" id="">
				<span class="l-btn-left">
					<span class="l-btn-text">确认发放</span> 
				</span> 
			</a>
	</div>
	 <img src="<ls:url address='/resources/common/images/indicator.gif'/>" class="editAdd_load"/>
	 <script src="<ls:templateResource item='/resources/templets/js/multishop/issueCoupon.js'/>"></script>
<script>
var contextPath="${contextPath}";
</script>
</body>

</html>