<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
	<title>
		<c:choose>
			<c:when test="${not empty prod.metaTitle}">${prod.metaTitle}-${systemConfig.shopName}</c:when>
			<c:otherwise>${prod.name}-${systemConfig.shopName}</c:otherwise>
		</c:choose> 
	</title>
	<c:choose>
		<c:when test="${not empty prod.keyWord}"><meta name="keywords" content="${prod.keyWord}" /></c:when>
		<c:otherwise><meta name="keywords" content="${systemConfig.keywords}" /></c:otherwise>
	</c:choose> 
	<c:choose>
		<c:when test="${not empty prod.metaDesc}"><meta name="description" content="${prod.metaDesc}" /></c:when>
		<c:otherwise><meta name="description" content="${systemConfig.description}" /></c:otherwise>
	</c:choose> 
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/productCommentsDetail.css'/>" rel="stylesheet"/>
</head>
<%@ include file="included/rightSuspensionFrame.jsp" %>
<body class="graybody">
	<div id="doc">
		<%@ include file="home/top.jsp"%>
		<div id="bd">
		
			<!--crumb-->
			<div id="J_crumbs" class="crumb yt-wrap">
				<div class="crumbCon">
					<div class="crumbSlide">
						<div class="crumbs-nav-item one-level">
							<a class="crumbs-link" href="<ls:url address="/"/>"> 首页 </a><i class="crumbs-arrow">&gt;</i>
						</div>

						<c:choose>
							<c:when test="${not empty param.keyword}">
								<div class="crumbs-nav-item one-level">
									<a class="crumbs-link" href="javascript:void(0);"><c:out value="${param.keyword}"></c:out>
									</a> <i class="crumbs-arrow">&gt;</i>
								</div>
							</c:when>
							<c:when test="${not empty treeNodes && (fn:length(treeNodes) > 1)}">
								<c:forEach items="${treeNodes}" var="node" varStatus="n">
									<div class="crumbs-nav-item">
										<c:choose>
											<c:when test="${!n.last}">
												<div class="menu-drop">
													<!-- <div class="trigger"> -->
														<span class="curr"><a title="${node.nodeName}" href="<ls:url address="/list?categoryId=${node.selfId}"/>">${node.nodeName}</a></span><!-- <i class="menu-drop-arrow"></i> -->
													<!-- </div> -->
													<%-- <div class="menu-drop-main">
														<c:if test="${not empty node.compatriots}">
															<ul class="menu-drop-list">
																<c:forEach items="${node.compatriots}" var="compatriot" varStatus="c">
																	<li><a title="${compatriot.nodeName}" href="<ls:url address="/list?categoryId=${compatriot.selfId}"/>">${compatriot.nodeName}</a>
																	</li>
																</c:forEach>
															</ul>
														</c:if>
													</div> --%>
												</div>
											</c:when>
											<c:when test="${n.last}">
												<a class="crumbs-link" href="javascript:void(0);">${node.nodeName} &nbsp;商品评价 </a>
											</c:when>
										</c:choose>
										<c:if test="${!n.last}">
											<i class="crumbs-arrow">&gt;</i>
										</c:if>
									</div>
								</c:forEach>
							</c:when>
							<c:when test="${not empty treeNodes && (fn:length(treeNodes) <= 1)}">
								<c:forEach items="${treeNodes}" var="node" varStatus="n">
									<div class="crumbs-nav-item one-level">
										<a class="crumbs-link" href="javascript:void(0);"><c:out value="${node.nodeName}"></c:out>
										</a>
									</div>
								</c:forEach>
							</c:when>
							<c:otherwise></c:otherwise>
						</c:choose>

						<c:if test="${not empty  facetProxy.selections}">
							<div class="crumbs-nav-item">
								<i class="crumbs-arrow">&gt;</i>
							</div>
							<c:forEach items="${facetProxy.selections}" var="selections">
								<div class="crumbs-nav-item crumbAttr">
									<a href="javascript:void(0);">${fn:substring(selections.title,0,6)}:${selections.name}</a> <a href="javascript:void(0);" onclick="javascript:removeFacetData('${selections.url}');"
										class="crumbDelete">X</a>
								</div>
							</c:forEach>
						</c:if>
					</div>
				</div>
			</div>
			<!--crumb end-->
			
			<div class="comment-container"> 
			   <div class="w clearfix"> 
			   
			    <div class="g-a1"> 
			     <div class="m" id="pinfo"> 
			      <input type="hidden" id="productId" value="3512431" /> 
			      <div class="mt"> 
			       <h2>商品信息</h2> 
			      </div> 
			      <div class="mc"> 
			       <div class="p-img"> 
			        <a href="${contextPath }/views/${product.prodId}" target="_blank"> <img alt="" src="<ls:images item='${product.pic }' scale='1' />" /> </a> 
			       </div> 
			       <div class="p-name">
			                     商品名称：
			        <a href="${contextPath }/views/${product.prodId}" target="_blank">${product.name}</a> 
			       </div> 
			       <div class="p-price">
			                     商城价： 
			        <strong class="c-J_3512431">￥${product.cash}</strong> 
			       </div> 
			       <div class="p-grade clearfix"> 
			        <span class="fl">评价得分：</span> 
			        <div class="fl"> 
			         <div id="product_star" class="star sa${productComment.score }"></div> (
			         <span id="product_star_score">${productComment.score }</span>分) 
			        </div> 
			       </div> 
			       <div class="num-comment">
			         评论数： 
			        <span id="p-num-comment">${product.comments }条</span>
			       </div> 
			      </div> 
			     </div> 
			
			    </div> 
			    
			    <div class="main-comments clearfix"> 
			     <div class="m"> 
			      <div class="mc"> 
			       <div class="column"> 
			        <div class="user-item clearfix"> 
			          <c:choose>
				      	<c:when test="${empty productComment.portrait }">
					       <img src="${contextPath }/resources/templets/images/sculpture.png" width="25" height="25" alt="${productComment.userName }" class="avatar" />
					    </c:when>
					    <c:otherwise>
					    	<img src="<ls:images item='${productComment.portrait }' scale='1' />" width="25px" height="25px" alt="${productComment.userName }" class="user-ico" /> 
					    </c:otherwise>
				      </c:choose> 
			          <div class="user-name">${productComment.userName }</div> 
			        </div> 
			        <div class="type-item"> 
				        <c:choose>
				      		<c:when test="${productComment.gradeName eq '皇冠会员'}">
				      			<span class="u-vip-level" style="color:##e1a10a">${productComment.gradeName }</span> 
				      		</c:when>
				      		<c:otherwise>
				      			<span class="u-vip-level" style="color:#666">${productComment.gradeName }</span> 
				      		</c:otherwise>
				      	</c:choose>
			        </div> 
			       </div> 
			       <div class="cont"> 
			        <!-- 评论分数 -->
		  			<div class="comment-star star${productComment.score }"></div> 
			        <div class="dt-text">
			          ${productComment.content }
			        </div> 
			        <div class="dt-def"> 
			         <c:if test="${not empty productComment.attribute }">
			         	<span class="txt">${productComment.attribute }</span> 
			         </c:if>
			         <span class="buy-time">购买时间: <fmt:formatDate value="${productComment.buyTime }"  pattern="yyyy-MM-dd HH:mm" /></span> 
			        </div> 
			        
			        <div class="dt-content">
			         <c:if test="${not empty productComment.photoPaths}">
			         	<c:forEach items="${productComment.photoPaths}" var="photo">
			         		<img src="<ls:images item='${photo }' scale='0'/>" /> </br>
			         	</c:forEach>
			         </c:if>
			         
			         <c:if test="${productComment.isReply}">
						  <div class="reply-box"> 
							 <div class="reply-con"> 
							  <span class="u-name">掌柜回复：</span> 
							  <span class="u-con">${productComment.shopReplyContent}</span> 
							 </div> 
							 <div class="reply-meta">
							   <fmt:formatDate value="${productComment.shopReplyTime}"  pattern="yyyy-MM-dd" />
							 </div> 
						  </div>
					 </c:if>
					 
			         <c:if test="${productComment.isAddComm and productComment.addStatus eq 1}">
			         	<div class="append">
			                <div class="addition">
			                	<c:choose>
						  	  		<c:when test="${productComment.appendDays eq 0}">用户当天追加评论</c:when>
						  	  		<c:otherwise>用户${productComment.appendDays}天后追加评论</c:otherwise>
						  	  	</c:choose>
			                </div>
			                <div class="def">${productComment.addContent}</div>
			             </div>
			             <c:if test="${not empty productComment.addPhotoPaths}">
				         	<c:forEach items="${productComment.addPhotoPaths}" var="photo">
				         		<img src="<ls:images item='${photo }' scale='0'/>" /> </br>
				         	</c:forEach>
				         </c:if>
				         <c:if test="${productComment.addIsReply}">
							  <div class="reply-box"> 
								 <div class="reply-con"> 
								  <span class="u-name">掌柜回复：</span> 
								  <span class="u-con">${productComment.addShopReplyContent}</span> 
								 </div> 
								 <div class="reply-meta">
								   <fmt:formatDate value="${productComment.addShopReplyTime}"  pattern="yyyy-MM-dd" />
								 </div> 
							  </div>
						  </c:if>
			         </c:if>
			         <div class="opts"> 
			          <a id="agree" href="#none" class="a_zan" prodCommId="${productComment.id }"> <span class="icon"></span> <span class="num"> ${productComment.usefulCounts }</span> </a>| 
			          <a id="reply" href="#none" class="a_comment"> <span class="icon"></span> <span class="num">${productComment.replayCounts }</span> </a>| 
			         </div> 
			         
			         <div class="reply-textarea J-reply-con"> 
			          <div class="inner"> 
			           <div class="wrap-textarea"> 
			            <textarea class="reply-input f-textarea" placeholder="回复  ${productComment.userName }："></textarea> 
			           </div> 
			          </div> 
			          <div class="operate-box"> 
			           <!-- <span class="txt-countdown">还可以输入<em>200</em>字</span>-->
			           <a id="replyCommentBtn" class="reply-submit" prodComId="${productComment.id }" href="javascript:;" target="_self">提交</a> 
			          </div> 
			         </div> 
			         
				     <div class="reply-items"> 
				         <c:forEach items="${requestScope.list}" var="productReply" varStatus="status">
				          <div class="item"> 
				           <div class="tt"> 
				            <span class="name">${productReply.replyUserName }</span>: 
				            <c:if test="${not empty productReply.parentReplyId }">
				            	回复&nbsp;<span class="name">${productReply.parentUserName }</span>
				            </c:if>
				            <span class="con">${productReply.replyContent }</span>
				           </div> 
				           <div class="tc clearfix"> 
				            <a href="#none" class="a_reply" parentId="${productReply.id }" parentUserName="${productReply.replyUserName }">回复</a> 
				            <span class="reply-time"><fmt:formatDate value="${productReply.replyTime }"  pattern="yyyy-MM-dd HH:mm" /></span> 
				           </div> 
				          </div> 
				         </c:forEach>
				     </div> 
				     <div class="clear"></div>
				     <!--分页-->
					 
						<div class="page yt-wrap clearfix" style="margin-top:20px;">
							<div class="p-wrap">
								<ul class="p-num">
									<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
								</ul>
							</div>
					 
			        </div> 
			       </div> 
			      </div> 
			     </div> 
			    
			  </div> 
			 </div> 
			</div> 
		<%@ include file="home/bottom.jsp"%>
	</div>
</div>
<script type="text/javascript">
	var contextPath = "${contextPath}";
	var prodComId = "${productComment.id}";
	var loginUserName = '${loginUserName}';
	var prodId='${product.prodId}';
</script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/commonUtils.js'/>" /></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/productComment.js'/>"></script>

</body>
</html>