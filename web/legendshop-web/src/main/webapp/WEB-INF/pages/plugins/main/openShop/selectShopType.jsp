<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
  <title>选择店铺类型-${systemConfig.shopName}</title>
  <meta name="keywords" content="${systemConfig.keywords}"/>
  <meta name="description" content="${systemConfig.description}" />
</head>
<body>
<div id="doc">
   <div id="hd">
   			 <%@ include file="../home/top.jsp" %>
   </div>
   
   <div id="bd">
       <div class="yt-wrap" style="padding:0px 0 20px;">
            <div class="seller-cru" style="width:1190px;margin: 20px auto 20px;">
              <p>
                您的位置&gt;
                <a href="#">首页</a>&gt;
                <span style="color: #e5004f;">免费开店</span>
              </p>
            </div>
            <div class="pagetab2">
               <ul>           
                 <li><span>签订入驻协议</span></li>
                 <li class="on"><span>入驻类型选择</span></li>
                 <!-- <li><span>联系人信息提交</span></li> -->                  
                 <li><span>店铺信息提交</span></li>   
                 <li><span>审核进度查询</span></li>   
               </ul>       
            </div>
            <div class="main">
                <div class="settled_box">
                  <h3>
                    <div class="settled_step">
                      <ul>
                        <li class="step7"><a href="#">签订入驻协议</a></li>
                        <li class="step1"><a href="#">入驻类型选择</a></li>
                         <!-- <li class="step2"><a href="#">联系人信息认证</a></li> -->
                        <li class="step2"><a href="#">店铺信息提交</a></li>
                        <!-- <li class="step3"><a href="#">店铺信息认证</a></li> -->
                        <li class="step4"><a href="#">审核进度查询</a></li>
                      </ul>
                    </div>
                    <span class="settled_title">入驻类型选择</span></h3>
                  <div class="settled_white">
                    <form:form >
                      <div class="settled_white_box">
                        <div class="settled_form">
                          <div class="cho-sty">
                              <a href="javascript:void(0);" id="personal" type="0"><dl><dt class="cho-sty-p"></dt><dd>个人入驻</dd></dl></a>
                              <a href="javascript:void(0);" id="enterprise" type="1"><dl><dt class="cho-sty-c"></dt><dd>企业入驻</dd></dl></a>
                          </div>
                        </div>
                      </div>
                      <div class="settled_bottom">
                      		<span>
                      			<a href = "javascript:history.back(-1);" onclick="" class="up_step_btn">上一步</a>
                      			<a href="javascript:void(0);" class="settled_btn" id="settledInfo"><em>下一步，店铺信息提交</em></a>
                      		</span>
                      </div>
                    </form:form>
                  </div>
                </div>
            </div>
        </div>
   </div>
  	 <%@ include file="../home/bottom.jsp" %>
</div>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/openshop.js'/>"></script>
<script>
	var contextPath='${contextPath}';
</script>
</body>
</html>
