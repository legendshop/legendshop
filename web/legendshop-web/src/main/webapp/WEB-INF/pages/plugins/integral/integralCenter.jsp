<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>积分中心-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link rel="stylesheet" type="text/css" media='screen' href="${contextPath}/resources/templets/css/integral-mall${_style_}.css?v=1" />
</head>

<body  style="background: #ffffff;">
	<div id="doc">
	    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
	    
			<div id="bd">
      
     
            <!--crumb-->
      <div class="seller-cru" style="width:1190px;margin: 20px auto 10px;">
        <p>
          您的位置&gt;
          <a href="${contextPath}/home">首页</a>&gt;
          <span style="color: #e5004f;">积分中心</span>
        </p>
      </div>   
      <div class="integral-mall">
		<c:if test="${empty users}">
        <div class="integral-banner">
          <div class="int-ban-left">
            <div class="int-mem-login">
              <div class="mem-ent">
                <a href="${contextPath}/login">立即登录</a>
                <p>登录积分中心查看积分详情</p>
              </div>
              <div class="int-cou" style="border: none;">
                <i class="cou-img"></i>
                <dl>
                  <dt>登录送积分</dt>
                  <dd>每天登录赠送<font style="color: red;font-weight: bold;">${releSetting.login==null?0:releSetting.login}</font>积分；</dd>
                </dl>
              </div>
              <div class="int-red">
                <i class="red-img" style="margin-top:7px;"></i>
                <dl>
                  <dt>购物、评论送积分</dt>
                  <dd>评价完成订单送<font style="color: red;font-weight: bold;">${releSetting.productReview==null?0:releSetting.productReview}</font>积分</dd>
                </dl>
              </div>
              <div class="int-gif">
                <i class="gif-img"></i>
                <dl>
                  <dt>积分兑换礼品</dt>
                  <dd>可使用积分兑换商城超值礼品</dd>
                </dl>
              </div>
            </div>
          </div>
          <div class="int-ban-right"><a href="" title="积分列表页中部广告位">
          	<img src="${contextPath}/resources/templets/images/coupon/integral-banner.jpg" border="0"></a>
          	</div>
        </div>
        </c:if>
<!-- 未登录时 -->
<c:if test="${not empty users}">

<div style="height: 10px;"></div>
        <div class="integral-banner">
          <div class="int-ban-left">
            <div class="int-mem-login">
              <div class="mem-log-ed">
                <div class="avatar">
                <a href="${contextPath}/p/uchome">
                <c:if test="${empty users.portraitPic}">
                	<img src="${contextPath}/resources/templets/images/no-img_mid_.jpg">
                </c:if>
                <c:if test="${not empty users.portraitPic}">
                	<img src="<ls:photo item='${users.portraitPic}'/>">
                </c:if>
                </a>
                </div>
                <dl>
                  <dt>Hi, ${users.userName }</dt>
                  <dd>当前积分：<strong><a href="${contextPath}/p/integral">${users.score }</a></strong></dd>
                </dl>
              </div>
              <div class="int-cou" style="border: none;padding: 18px 0;">
                <i class="cou-img"></i>
                <dl>
                  <dt>登录送积分</dt>
                  <dd>每天登录赠送<font style="color: red;font-weight: bold;">${releSetting.login==null?0:releSetting.login}</font>积分；</dd>
                </dl>
              </div>
              <div class="int-red" style="padding: 18px 0;">
                <i class="red-img" style="margin-top:7px;"></i>
                 <dl>
                  <dt>购物、评论送积分</dt>
                  <dd>评价完成订单送<font style="color: red;font-weight: bold;">${releSetting.productReview==null?0:releSetting.productReview}</font>积分</dd>
                </dl>
              </div>
              <div class="int-gif" >
                <i class="gif-img"></i>
                <dl>
                  <dt>积分兑换礼品</dt>
                  <dd>可使用积分兑换商城超值礼品</dd>
                </dl>
              </div>
            </div>
          </div>
          <div class="int-ban-right"><a href="" title="积分列表页中部广告位"><img src="${contextPath}/resources/templets/images/coupon/integral-banner.jpg" alt="" border="0"></a></div>
        </div>
        </c:if>
<!-- 会员已登录 -->
	<c:if test="${not empty lists}">
        <div class="integral-list">
          <div class="int-tit">
            <h3><i class="int-cou-tit"></i>优惠券</h3>
            <span><a href="javascript:void(0);" id="coupon">更多&gt;&gt;</a></span>
          </div>
          <ul class="int-con">
          
          
          <c:forEach items="${lists}" var="ls">
            <li>
              <div class="int-con-lef">
                <div class="cut"></div>
                <div class="info">
                  <a href="${contextPath}/store/${ls.shopId}" class="store">${ls.shopName}</a>
                  <div class="pic">
                  	<c:if test="${empty ls.couponPic }">
                  		<img src="${contextPath}/resources/templets/images/coupon/coupon-default.png"/>
                  	</c:if>
                  	<c:if test="${not empty ls.couponPic }">
                    	<img src="<ls:photo item='${ls.couponPic}'/>">
                    </c:if>
                  </div>
                </div>
                <dl class="value">
                  <dt>¥<em>${ls.offPrice}</em></dt>
                  <dd>购物满${ls.fullPrice}元可用</dd>
                  <dd class="time">有效期至<fmt:formatDate value="${ls.endDate}" pattern="yyyy-MM-dd HH:mm"/></dd>
                </dl>
                <div class="point">
                  <p class="required">需<em>${ls.needPoints}</em>积分</p>
                  <p><em>${ls.bindCouponNumber}</em>人已兑换</p>
                </div>
                <div class="button">
                <a href="javascript:void(0);" >立即兑换</a>
                <input type="hidden" value="${ls.couponId}"/>
                </div>
              </div>
            </li>
            </c:forEach>
            
            
            
          </ul>
        </div>
    </c:if>
<!-- 热门代金券 -->
	<c:if test="${not empty listp}">
        <div class="integral-list">
          <div class="int-tit">
            <h3><i class="int-red-tit"></i>热门红包</h3>
            <span><a href="javascript:void(0);" id="redPack">更多&gt;&gt;</a></span>
          </div>
          <ul class="int-con">
          
          <c:forEach items="${listp}" var="p">
            <li>
              <div class="int-con-lef">
                <div class="cut"></div>
                <div class="info">
                  <a href="javascript:void(0);" class="store">${p.couponName}</a>
                  <p class="sto-cla">${p.name}</p>
                  <div class="pic">
                  <c:if test="${empty p.couponPic}">
                  	<img src="${contextPath}/resources/templets/images/coupon/red-default.png"/>
                  </c:if>
                  <c:if test="${not empty p.couponPic }">
                    <img src="<ls:photo item='${p.couponPic}'/>">
                    </c:if>
                  </div>
                </div>
                <dl class="value">
                  <dt>¥<em>${p.offPrice}</em></dt>
                  <dd>购物满${p.fullPrice}元可用</dd>
                  <dd class="time">有效期至<fmt:formatDate value="${p.endDate}" pattern="yyyy-MM-dd HH:mm"/></dd>
                </dl>
                <div class="point">
                  <p class="required">需<em>${p.needPoints}</em>积分</p>
                  <p><em>${p.bindCouponNumber}</em>人已兑换</p>
                </div>
                <div class="button">
                	<a href="javascript:void(0);" value="1" >立即兑换</a>
                	<input type="hidden" value="${p.couponId}"/>
                </div>
              </div>
            </li>
            
          </c:forEach>  
          </ul>
        </div>
   </c:if>     
<!-- 热门红包 -->
	<c:if test="${not empty integralProds}">
        <div class="integral-list">
          <div class="int-tit">
            <h3><i class="int-gif-tit"></i>积分商品</h3>
            <span><a href="${contextPath}/integral/list">更多&gt;&gt;</a></span>
          </div>
          <ul class="int-con-list">
          <c:forEach items="${integralProds}" var="ll">
            <li>
              <div class="gift-pic"><a href="${contextPath}/integral/view/${ll.id}"> <img src="<ls:photo item='${ll.prodImage}'/>"> </a></div>
              <div class="gift-name"><a href="${contextPath}/integral/view/${ll.id}">${ll.name}</a></div>
              <div class="exchange-rule">
                <span class="pgoods-price">价格：<em>¥${ll.price}</em></span>
                <span class="pgoods-points">积分兑换：<strong>${ll.exchangeIntegral}</strong>积分</span>
              </div>
            </li>
            </c:forEach>
          </ul>
        </div>
    </c:if>   
<!-- 热门礼品 -->
      </div>
   </div> 
		 <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/IntegralCenter.js'/>"></script>
	<script type="text/javascript">
			var contextPath = '${contextPath}';
	</script>
</body>
