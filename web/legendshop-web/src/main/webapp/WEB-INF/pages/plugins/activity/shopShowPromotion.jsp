<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>满减满折-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/active/actives.css'/>"
          rel="stylesheet">
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/active/promotion.css'/>"
          rel="stylesheet">
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/prodSelectAlert.css'/>" rel="stylesheet"/>
    <style>
        .w150 {
            width: 150px !important;
        }

        .w168 {
            width: 168px !important;
        }

        .btn-r {
            margin-right: 5px;
        }
    </style>
</head>
<body class="graybody">
<div id="doc">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
    <div class="w1190">
        <div class="seller-cru">
            <p>
                您的位置>
                <a href="${contextPath}/home">首页</a>>
                <a href="${contextPath}/sellerHome">卖家中心</a>>
                <span class="on">满减满折</span>
            </p>
        </div>

        <%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>


        <div id="rightContent" class="right_con" style="margin-top: 20px">
            <div class="ncm-default">
                <form:form action="${contextPath}/s/addShopMarketing" id="add_form" method="post">
                    <div class="promotion-right">
                        <!-- 基本信息 -->
                        <div class="basic-ifo">
                            <!-- 基本信息头部 -->
                            <div class="ifo-top">
                                基本信息
                            </div>
                            <div class="ifo-con">
                                <li style="margin-bottom: 5px;">
                                    <span class="input-title" style="display: inline-block;width: 83px"><span
                                            style="color: #E5004F;">*</span>活动名称:</span>
                                    <input type="hidden" class="id" name="id" value="${marketing.id}"/>
                                    <input type="text" class="active-name" maxlength="25" name="marketName"
                                           disabled id="marketName" value="${marketing.marketName}">
                                    <span class="error-message"></span>
                                </li>
                                <li>
                                    <span class="input-title"></span>
                                    <span style="color: #999999;margin-top: 10px;">活动名称最多为25个字符</span>
                                <li>
                                <li style="margin-bottom: 0;">
                                    <span class="input-title" style="display: inline-block;width: 83px"><span
                                            style="color: #E5004F;">*</span>活动时间:</span>
                                    <input type="text" class="active-time"
                                           disabled="disabled" placeholder="开始时间" value="${marketing.startTime}" disabled>
                                    <span class="error-message"></span>
                                    <span style="margin: 0 5px;">-</span>
                                    <input type="text" class="active-time"
                                           disabled="disabled" placeholder="结束时间" value="${marketing.endTime}" disabled>
                                    <span class="error-message"></span>
                                    <p class="hint"></p>
                                </li>
                            </div>
                        </div>
                        <!-- 选择活动商品 -->
                        <div class="basic-ifo" style="margin-top: 20px;">
                            <!-- 选择活动商品头部 -->
                            <div class="ifo-top">
                                选择活动商品
                            </div>
                            <div style="margin:15px 60px">
                                <dl>
                                    <dt style="display: inline-block"><span style="color: #E5004F;">*</span>选择商品：</dt>
                                    <dd style="display: inline-block">

                                        <label  class="radio-wrapper">
									<span class="radio-item">
									<input type="radio" id="goods_status_1" value="1" name="isAllProds"  <c:if test="${empty marketing.isAllProds || marketing.isAllProds eq 1}">checked</c:if>
                                           class="radio-input" disabled/>
									<span class="radio-inner"></span>
									</span>
                                            <span class="radio-txt">全部商品</span>
                                        </label>
                                        <label class="radio-wrapper">
									<span class="radio-item">
										<input type="radio" id="goods_status_0" value="0" name="isAllProds" <c:if test="${marketing.isAllProds eq 0}">checked</c:if>
                                               class="radio-input" disabled/><%----%>
										<span class="radio-inner"></span>
									</span>
                                            <span class="radio-txt">部分参与商品</span>
                                        </label>
                                    </dd>
                                </dl>
                            </div>
                            <div class="ifo-con" id="prods" style="padding:0px 60px 30px 60px;">
                                <div class="tab-shop">
                                    <table border="0" id="skuList" cellpadding="0" cellspacing="0" style="margin: 30px 0;">
                                        <tbody>
                                        <tr style="background: #f9f9f9;">
                                            <td width="70px">商品图片</td>
                                            <td width="170px">商品名称</td>
                                            <td width="100px">商品规格</td>
                                            <td width="100px">商品价格</td>
                                            <td width="100px">可销售库存</td>
                                        </tr>
                                        <c:choose>
                                            <c:when test="${not empty marketing.marketingProds && marketing.marketingProds.size()>0}">
                                                <c:forEach items="${marketing.marketingProds}" var="marketingProdDto">
                                                    <tr class="sku" >
                                                        <input type="hidden" class="skuId" name="skuIds" value="${marketingProdDto.skuId}"/>
                                                        <td>
                                                            <img src="<ls:images item='${marketingProdDto.pic}' scale='3' />" >
                                                        </td>
                                                        <td>
                                                            <span class="name-text">${marketingProdDto.prodName}</span>
                                                        </td>
                                                        <td>
                                                            <span class="name-text">${marketingProdDto.cnProperties}</span>
                                                        </td>
                                                        <td>
                                                            <span class="name-text">${marketingProdDto.cash}</span>
                                                        </td>
                                                        <td>${marketingProdDto.stock}</td>
                                                    </tr>
                                                </c:forEach>
                                            </c:when>
                                            <c:otherwise>
                                                <tr class="first">
                                                    <td colspan="6" >暂未选择商品</td>
                                                </tr>
                                            </c:otherwise>
                                        </c:choose>
                                        <tr class="noSku" style="display: none"><td colspan='6'>暂未选择商品</td></tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- 活动规则 -->
                        <div class="basic-ifo" style="margin-top: 20px;">
                            <!-- 活动规则头部 -->
                            <div class="ifo-top">
                                活动规则
                            </div>
                            <div class="ifo-con">
                                <div style="margin:-6px 0px 40px 0px">
                                    <dl>
                                        <dt style="display: inline-block"><span style="color: #E5004F;">*</span>促销类型：
                                        </dt>
                                        <dd style="display: inline-block;margin-left: 15px">
                                            <label class="radio-wrapper">
								<span class="radio-item">
									<input type="radio"  id="goods_type_0" value="0" name="type"  class="radio-input" <c:if test="${empty marketing.type || marketing.type eq 0}">checked</c:if> disabled/>
									<span class="radio-inner"></span>
								</span>
                                                <span class="radio-txt">满减促销</span>
                                            </label>
                                            <label class="radio-wrapper" >
								<span class="radio-item">
									<input type="radio" id="goods_type_1" value="1" name="type" class="radio-input" <c:if test="${marketing.type eq 1}">checked</c:if> disabled/>
									<span class="radio-inner"></span>
								</span>
                                                <span class="radio-txt">满折促销</span>
                                            </label>
                                            <span class="error-message"></span>
                                        </dd>
                                    </dl>
                                </div>
                                <div>
                                    <dl>
                                        <dt style="display: inline-block"><span style="color: #E5004F;">*</span>活动规则：</dt>
                                        <dd style="display: inline-block; margin:0px 0px 0px 10px">
                                            <input type="hidden" name="rule_count" id="mansong_rule_count">
                                            <span class="error-message"></span>
                                            <ul class="ncsc-mansong-rule-list" id="manjian_rule_list">
                                            </ul>

                                            <ul class="ncsc-mansong-rule-list" id="manze_rule_list">
                                            </ul>
                                            <div id="add_manjian_rule">
                                                <div class="ncsc-mansong-rule">
				              <span>满&nbsp;<input type="text" class="text w150" id="manjian_price" maxlength="8"><select
                                      id="manjian_calType" class="add-on" style="width:45px;height:30px;">
				              	<option value="0">元</option>
				              	<option value="1">件</option>
				              </select>
				              	，
				              </span>
                                                    <span>立减现金&nbsp;<input type="text" class="text w150"
                                                                           id="manjian_discount" maxlength="8"><em
                                                            class="add-on">元</em></span>
                                                    <div class="gift" id="mansong_goods_item"></div>
                                                </div>
                                                <div style="display:none;" id="mansong_rule_error">请至少选择一种促销方式</div>
                                                <div class="mt10">
                                                    <a class="btn-r" style="height:30px;line-height: 30px;"
                                                       id="save_mj_rule" href="javascript:void(0);">确定规则设置</a>
                                                    <a class="btn-g big-btn" id="btn_cancel_mj_rule"
                                                       href="javascript:void(0);">取消</a></div>
                                            </div>
                                            <div style="display:none;" id="add_manze_rule">
                                                <div class="ncsc-mansong-rule">
				                  <span style="display: inline-block">满&nbsp;<input type="text" class="text w168"
                                                                                    id="manze_price" style=""><select
                                          id="manze_calType" class="add-on" style="width:45px;height:30px;">
				              			<option value="0">元</option>
				              			<option value="1">件</option>
				              	 	</select>，
				              	  </span>
                                                    <span>打&nbsp;<input type="text" class="text w168"
                                                                        id="manze_discount"><em class="add-on">折</em>（价格低于0.1元时，商品不打折）</span>
                                                    <div class="gift" id="mansong_goods_item"></div>
                                                </div>
                                                <div style="display:none;" id="mansong_rule_error">请至少选择一种促销方式</div>
                                                <div class="mt10">
                                                    <a class="btn-r" style="height:30px;line-height: 30px;"
                                                       id="save_mz_rule" href="javascript:void(0);">确定规则设置
                                                    </a>
                                                    <a class="btn-g big-btn" id="btn_cancel_mz_rule"
                                                       href="javascript:void(0);">
                                                        取消</a>
                                                </div>
                                            </div>
                                            <span class="error-message"></span>
                                        </dd>
                                    </dl>

                                </div>
                                <div style="margin-top: 30px">
                                    <dl>
                                        <dt style="margin-left: 24px">备注：</dt>
                                        <dd style="display: inline-block; margin:-16px 0px 0px 80px">
                                            <textarea class="textarea" disabled style="width:478px;" maxlength="100" id="remark"
                                                      rows="10" name="remark" value="${marketing.remark}">${marketing.remark}</textarea>
                                            <p class="hint">活动备注最多为100个字符</p>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                        <div class="bto-btn">
                            <dl class="bottom">
                                <dt>&nbsp;</dt>
                                <dd>
                                    <label class="submit-border">
                                        <a class="red-btn" href="/s/shopSingleMarketing"
                                           style="background: #F9F9F9;color: #000000;border:1px solid rgba(221,221,221,1);">返回列表</a>
                                    </label>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </form:form>
            </div>


            <div class="m m1" style="margin: 65px;">
                <div class="mt">
                    <h3>温馨提示：</h3>
                </div>
                <div class="mc">
                    <p style="line-height:22px;">
                        1. 满减活动可选店铺所有商品或只选择部分商品参加，活动时间不能和已有活动重叠
                        <br>
                        2. 每个满减活动最多可以设置3个价格级别，点击新增级别按钮可以增加新的级别，价格级别应该由低到高
                        <br>
                        3. 选择商品分为全场商品和部分商品[部分商品需要在列表进行管理添加商品信息]
                        <br>
                        4. 活动类型分为满减促销规则[满X送X元],满折促销[满X元打折X折扣],至少需要选择一种
                        <br>
                        5. 单品满折促销与全场满折促销是互斥关系,以单品满折促销优先
                    </p>
                </div>
            </div>


            <div class="clear"></div>
        </div>


    </div>
    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<script src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/templets/js/shopAddMansong.js'/>"></script>
<script type="text/javascript">
    var contextPath = '${contextPath}';
    var isAllProd = "${isAllProd}";
    var prods = new Array();
    var skus = new Map();
    var marketingMjRules = "${marketing.marketingMjRules}";
    var marketingMzRules = "${marketing.marketingMzRules}";
    var fullAmount;
    var offAmount;
    var calType;
    var offDiscount;
    if (marketingMjRules != null && marketingMjRules !=""){
        <c:forEach items="${marketing.marketingMjRules}" var="marketingMjRule">
            fullAmount="${marketingMjRule.fullAmount}";
            offAmount="${marketingMjRule.offAmount}";
            calType="${marketingMjRule.calType}";
            loadType(fullAmount,offAmount,calType);
        </c:forEach>
    }else if (marketingMzRules != null && marketingMzRules != "") {
        <c:forEach items="${marketing.marketingMzRules}" var="marketingMzRule">
        fullAmount="${marketingMzRule.fullAmount}";
        offDiscount="${marketingMzRule.offDiscount}";
        calType="${marketingMzRule.calType}";
        loadType(fullAmount,offDiscount,calType);
        </c:forEach>
    }

    $(function () {
        $("#goods_type_0").click(function () {
            $("#goods_type_0").parent(".radio-item").parent(".radio-wrapper").attr("class", "radio-wrapper radio-wrapper-checked");
            $("#goods_type_1").parent(".radio-item").parent(".radio-wrapper").attr("class", "radio-wrapper");
        });
        $("#goods_type_1").click(function () {
            $("#goods_type_1").parent(".radio-item").parent(".radio-wrapper").attr("class", "radio-wrapper radio-wrapper-checked");
            $("#goods_type_0").parent(".radio-item").parent(".radio-wrapper").attr("class", "radio-wrapper");
        });
        $(".btn-r").css("display","none")
    });
</script>
</body>
</html>