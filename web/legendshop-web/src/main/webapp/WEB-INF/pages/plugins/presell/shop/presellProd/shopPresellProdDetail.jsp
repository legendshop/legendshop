<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss" var="nowDate" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<title>预售活动详情-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css"	href="<ls:templateResource item='/resources/templets/css/active/actives${_style_}.css'/>" rel="stylesheet">
<link type="text/css"	href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet">
<link type="text/css"	href="<ls:templateResource item='/resources/templets/css/presell/presell-actives.css'/>" rel="stylesheet">

</head>
<body class="graybody">
	<div id="doc">
		<%@ include	file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置> <a href="${contextPath}/home">首页</a>> 
					<a	href="${contextPath}/sellerHome">卖家中心</a>> 
					<a  href="${contextPath}/s/presellProd/query">预售活动管理</a>>
					<span class="on">预售活动详情</span>
				</p>
			</div>
			<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp"%>
			<div id="rightContent" class="right_con">
				<!--page-->
				<!-- 预售活动详情 -->
				<div class="presell-detail">
					<div class="bid-det-com">
						<p>预售详情</p>

						<div class="com-left">
							<a target="_blank" href="${contextPath}/views/${presellProd.prodId}">
								<!-- 如果sku没有图片,就显示商品的图图片 -->
							    <img  src="<ls:images item='${empty presellProd.skuPic?presellProd.prodPic:presellProd.skuPic}' scale='1'/>" alt="单品图片"/>
							</a>
						</div>
						<div class="com-right">
							<ul>
								<li><b>方案名称：</b>&nbsp;&nbsp;<a target="_blank" href="${contextPath}/presell/views/${presellProd.id}">${presellProd.schemeName }</a></li>
								<li><b>商品名称：</b>&nbsp;&nbsp;<a target="_blank" href="${contextPath}/views/${presellProd.prodId}">${presellProd.skuName}</a></li>
								<li><b>商品属性：</b>&nbsp;&nbsp;${empty presellProd.cnProperties?'无':presellProd.cnProperties}</li>
								<li><b>商品价格：</b>&nbsp;&nbsp;￥${presellProd.skuPrice}</li>
								<li><b>预售价格：</b>&nbsp;&nbsp;￥${presellProd.prePrice}</li>
								<li><b>预售时间：</b>&nbsp;&nbsp;
								<fmt:formatDate value="${presellProd.preSaleStart}" pattern="yyyy-MM-dd HH:mm"></fmt:formatDate>&nbsp;-&nbsp;
								<fmt:formatDate value="${presellProd.preSaleEnd}" pattern="yyyy-MM-dd HH:mm" ></fmt:formatDate>
								</li>
								<li><b>发货时间：</b>&nbsp;&nbsp;
								<fmt:formatDate value="${presellProd.preDeliveryTime}" pattern="yyyy-MM-dd HH:mm"></fmt:formatDate>
								</li>
								<li>
									<b>支付方式：</b>&nbsp;&nbsp; 
									<c:if test="${presellProd.payPctType eq 0}">
        								全额支付
        							</c:if>
									<c:if test="${presellProd.payPctType eq 1}">
        								定金支付
        							</c:if>
								</li>	
								<c:if test="${presellProd.payPctType eq 1}">						
									<li>
										<b>定金金额：</b>&nbsp;&nbsp; ￥${presellProd.preDepositPrice}
										&nbsp;&nbsp; <b>定金占百分比：</b>&nbsp;&nbsp; <fmt:formatNumber value='${presellProd.payPct*100}' pattern='#0.0#'/>&nbsp;%
									</li>
									<li>
										<b>尾款时间：</b>&nbsp;&nbsp; 
										<fmt:formatDate value="${presellProd.finalMStart}" pattern="yyyy-MM-dd HH:mm"></fmt:formatDate>&nbsp;-&nbsp;
										<fmt:formatDate value="${presellProd.finalMEnd}" pattern="yyyy-MM-dd HH:mm" ></fmt:formatDate>
									</li>
								</c:if>
							</ul>
						</div>
					</div>
					<div class="presell-des paimai-status" >
						<div class="presell-des-tit">
							<p>预售状态</p>
						</div>
						<div style="padding: 20px;">
						<p>审核结果：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				     		<c:if test="${presellProd.status eq -1 }">审核中</c:if>
				     		<c:if test="${presellProd.status eq -2 }">未提审</c:if>
				     		<c:if test="${presellProd.status eq 0  or presellProd.status eq 2 or presellProd.status eq 3}">审核通过</c:if>
				     		<c:if test="${presellProd.status eq 1 }">被拒绝</c:if>
						</p>
						<p>审核意见：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					     	<c:choose>
					     		<c:when test="${empty presellProd.auditOpinion}">暂无</c:when>
					     		<c:otherwise>
					     			${presellProd.auditOpinion}
					     			<p>审核时间：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					     				<fmt:formatDate value="${presellProd.auditTime}" pattern="yyyy-MM-dd HH:mm"/> 
					     			</p>
					     		</c:otherwise>
					 		</c:choose>
					 	</p>
						 <div> 预售状态：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<%-- <c:choose>
								<c:when test="${(presellProd.status le -1 or presellProd.status eq 1) and presellProd.preSaleStart gt nowDate}">未激活</c:when>
								<c:when test="${presellProd.status eq 0 and presellProd.preSaleStart gt nowDate}">即将开始</c:when>
								<c:when test="${presellProd.status eq 0 and presellProd.preSaleStart le nowDate and presellProd.preSaleEnd gt nowDate}">正在进行中</c:when>
								<c:when test="${presellProd.status eq 2}">已完成</c:when>
								<c:when test="${presellProd.status eq 1}">已拒绝</c:when>
								<c:when test="${presellProd.status le -1 and presellProd.preSaleStart le nowDate}">已过期</c:when>
								<c:when test="${presellProd.status eq 3}">已终止</c:when>
								<c:otherwise>未知情况</c:otherwise>
							</c:choose> --%>
							<c:if test="${presellProd.status eq -2 and item.preSaleStart gt nowDate}">未提审</c:if>		
							<c:if test="${presellProd.status eq -1 and item.preSaleStart gt nowDate}">待审核</c:if>		
							<c:if test="${presellProd.status eq 1 and item.preSaleStart gt nowDate}">已拒绝</c:if>
							<c:if test="${presellProd.status eq 0}">
								<c:choose>
									<c:when test="${empty presellProd.prodStatus || presellProd.prodStatus eq -1 || presellProd.prodStatus eq -2}">
										商品已删除
									</c:when>
									<c:when test="${presellProd.prodStatus eq 0}">
										商品已下线
									</c:when>
									<c:when test="${presellProd.prodStatus eq 2}">
										商品违规下线
									</c:when>
									<c:when test="${presellProd.prodStatus eq 3}">
										商品待审核
									</c:when>
									<c:when test="${presellProd.prodStatus eq 4}">
										商品审核失败
									</c:when>
									<c:when test="${presellProd.preSaleStart le nowDate and presellProd.preSaleEnd gt nowDate}">
										<font color="#e5004f">进行中</font>
									</c:when>
									<c:when test="${presellProd.preSaleStart gt nowDate}">
										未开始
									</c:when>
								</c:choose>
								<c:if test="">未开始</c:if>
							</c:if>
							<c:if test="${presellProd.status eq 2 }">已完成</c:if>
							<c:if test="${(presellProd.status le -1 or presellProd.status eq 1) and (presellProd.preSaleStart le nowDate or presellProd.preSaleEnd le nowDate)}">已过期</c:if>
							<c:if test="${presellProd.status eq 3}">已终止</c:if>
						</div>
						</div>
					</div>
				</div>
				<div class="btn-box">
					<input type="button" value="返回" onclick="window.history.go(-1);" />
<%-- 					<c:choose>
	                	<c:when test="${presellProd.status le -1 and (presellProd.preSaleStart le nowDate or presellProd.preSaleEnd le nowDate)}">
	                		<input type="button" value="返回" onclick="window.location='${contextPath}/s/presellProd/query'" />
	                	</c:when>
	                	<c:otherwise>
	                		<input type="button" value="返回" onclick="window.location='${contextPath}/s/presellProd/query?status=${presellProd.status eq 3?'':presellProd.status}'" />
	                	</c:otherwise>
	                </c:choose> --%>
				</div>
			</div>
		</div>
		<%@ include	file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
		<script type="text/javascript">
			var contextPath = '${contextPath}';
				$(document).ready(function() {
				userCenter.changeSubTab("presellManage"); //高亮菜单
			});
		</script>
</body>
</html>
