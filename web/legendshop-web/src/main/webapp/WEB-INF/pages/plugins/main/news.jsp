<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<html>
<head>
<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
<title>文章列表-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}"/>
<meta name="description" content="${systemConfig.description}" />

    <style type="text/css">
        .newscontent img{
            width: 97%;
        }
    </style>

</head>
<body  class="graybody">
<div id="bd">
      <%@include file='home/top.jsp'%>
         <div style="padding-top:10px;" class="yt-wrap"> 
             <div class="leftnews">
              	 <%@ include file="newsCategory.jsp"%>
            </div>		
             <div class="right_newscon">
     
                 <div class="news_bor">
                 
                    <h3 class="tit">${news.newsTitle}</h3>
                    <div class="nge"></div>
                    <p class="author">作者: ${news.userName}  &nbsp;&nbsp;<fmt:formatDate value="${news.newsDate}" pattern="yyyy-MM-dd HH:MM"/></p>
                     <c:if test="${ not empty news.newsBrief}">
	                    <p class="abstract" style="word-break: break-all;">
	                    <strong>摘要：</strong>${news.newsBrief}.......
	                    </p>
          			  </c:if>
                    <div class="newscontent" style="word-break: break-all;">
                        ${news.newsContent}
                   </div>
                 
                   
                     <div class="tishi">
                     	<c:forEach items="${requestScope.newsList}" var="news" varStatus="status">
		               		<p><a href="${contextPath}/news/${news.newsId}">
			               		<c:if test="${status.first}">上一篇:</c:if>
			               		<c:if test="${status.last}">下一篇：</c:if>${news.newsTitle}
		               			</a>
		               		</p>
             			  </c:forEach>  
		             </div>
                </div>
             </div>
            <div class="clear"></div>
         </div>
         
          <%@ include file="home/bottom.jsp" %>
   </div>
   </body>
   <script type="text/javascript">
		var newsId = '${news.newsId}';
		$("#catlist_"+newsId).css({"color":"rgb(229, 0, 79)"});
		$("#catlist_"+newsId).parent().parent().show();
	</script>
   </html>