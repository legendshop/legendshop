<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller${_style_}.css'/>" rel="stylesheet"/>
<style>
    .office-mes {
        overflow: hidden;
        margin-top: 20px;
        font-size: 14px;
    }

    .office-mes li {
        height: 25px;
    }

    .office-mes li a {
        color: #0074ce;
        font-size: 12px;
        float: left;
        margin-top: 10px;
        display: block;
        height: 14px;
        overflow: hidden;
        text-overflow: ellipsis;
        -o-text-overflow: ellipsis;
        white-space: nowrap;
        width: 120px;
    }

    .office-mes li span {
        color: #666;
        float: right;
        font-size: 12px;
        margin-top: 10px;
        line-height: 14px;
    }

    /*官方信息中心*/
</style>

<!-----------leftm-------------->
<div class="leftm">

    <div class="uc_minb">
        <h3>
            <span>${sessionScope.SPRING_SECURITY_LAST_USERNAME}</span>欢迎您！
        </h3>
        <p>会员等级：${sessionScope.LAST_USERGRADE }</p>
    </div>
    
    <c:if test="${not empty pubList}">
        <div class="uc_minb m10">
            <div class="office-mes">
                <h4>官方信息中心</h4>
                <ul>
                    <c:forEach items="${pubList}" var="pub" varStatus="index">
                        <li><a href="${contextPath}/pub/${pub.id}" title="${pub.title}" target="_blank">${pub.title}</a><span><fmt:formatDate value="${pub.recDate}" pattern="MM-dd"/></span></li>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </c:if>

    <div class="uctit m10" id="uc_left_nav">
        <h3 style="font-weight: 700;" id="usercenterhomeWrap">
            <a href="javascript:void(0)" id="usercenterhome">个人中心首页</a>
        </h3>
        <h3 class="open">
            <div style="font-weight: 700;">交易信息</div>
            <ul>
                <li id="myorder"><a href="javascript:void(0)">我的订单</a></li>
                <li id="integralOrder"><a href="javascript:void(0)">积分订单</a></li>
                <li id="myreturnorder"><a href="javascript:void(0)">退款及退货</a></li>
                <ls:plugin pluginId="integral">
                    <li id="myIntegral"><a href="javascript:void(0)">我的积分</a></li>
                </ls:plugin>
                <ls:plugin pluginId="coupon">
                    <li id="myCoupon"><a href="javascript:void(0)">我的优惠券</a></li>
                    <li id="myRedPacket"><a href="javascript:void(0)">我的红包</a></li>
                </ls:plugin>
                <li id="myinvoice"><a href="javascript:void(0)">我的发票</a></li>
                <li id="myPartAution"><a href="javascript:void(0)">我参与的拍卖</a></li>
                <ls:plugin pluginId="auction">
                    <li id="myAuction"><a href="javascript:void(0)">拍卖中标记录</a></li>
                </ls:plugin>
                <li id="mySeckill"><a href="javascript:void(0)">我参与的秒杀</a></li>
            </ul>
        </h3>
        <h3 class="open">
            <div style="font-weight: 700;">我的收藏</div>
            <ul>
                <li id="favoriteShop"><a href="javascript:void(0)">店铺收藏</a></li>
                <li id="myfavourite"><a href="javascript:void(0)">商品收藏</a></li>
                <li id="prodcomment"><a href="javascript:void(0)">商品评论</a></li>
                <li id="prodcons"><a href="javascript:void(0)">商品咨询</a></li>
                <li id="accusation"><a href="javascript:void(0)">举报管理</a></li>
                <li id="toAccusation" style="display:none"></li>
            </ul>
        </h3>
        <h3 class="open">
            <div style="font-weight: 700;">我的消息</div>
            <ul>
                <li id="inbox"><a href="javascript:void(0)">收件箱</a></li>
                <!-- <li id="systemMessages"><a href="javascript:void(0)">系统通知</a></li> -->
            </ul>
        </h3>
        <h3 class="open">
            <div style="font-weight: 700;">个人资料</div>
            <ul>
                <li id="personinfo"><a href="javascript:void(0)">个人资料</a></li>
                <li id="security"><a href="javascript:void(0)">账户安全</a></li>
                <%-- 				<ls:plugin pluginId="overseas">
                                    <li id="myidcards"><a href="javascript:void(0)">我的实名认证</a></li>
                                 </ls:plugin>  --%>
<%--                 <c:if test="${indexApi:isQqLoginEnable() || indexApi:isWeiboLoginEnable() || indexApi:isWeiXinLoginEnable()}">
                    <li id="accountBinding"><a href="javascript:void(0)">登陆绑定</a></li>
                </c:if> --%>
                <li id="receiveaddress"><a href="javascript:void(0)">收货地址</a></li>
                <li id="expensesRecord"><a href="javascript:void(0)">消费记录</a></li>
            </ul>
        </h3>
        <ls:plugin pluginId='predeposit'>
            <h3 class="open last">
                <div style="font-weight: 700;">预存款</div>
                <ul>
                    <!-- <li id="adddeposit"><a href="javascript:void(0)">预存款充值</a></li> -->
                    <li id="mydeposit"><a href="javascript:void(0)">我的预存款</a></li>
                    <!-- <li id="myCoin"><a href="javascript:void(0)">我的金币</a></li> -->
                </ul>
            </h3>
        </ls:plugin>

        <ls:plugin pluginId='promoter'>
            <c:set var="isPromoter" value="${indexApi:isPromoter(_userDetail.userId)}"></c:set>
            <h3 class="open last">
                <div style="font-weight: 700;">我的分销</div>
                <ul>
                    <c:choose>
                        <c:when test="${isPromoter}">
                            <li id="applyPromoter"><a href="javascript:void(0)">我要推广</a></li>
                            <li id="myPromoterUser"><a href="javascript:void(0)">我推广的会员</a></li>
                            <li id="myCommis"><a href="javascript:void(0)">我的佣金</a></li>
                        </c:when>
                        <c:otherwise>
                            <li id="applyPromoter"><a href="javascript:void(0)">申请成为推广员</a></li>
                        </c:otherwise>
                    </c:choose>

                </ul>
            </h3>
        </ls:plugin>
    </div>


</div>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/usercenter.js'/>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        openHideForUsercenterLeft();
    });
</script>
<!-----------leftm end-------------->