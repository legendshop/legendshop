<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<!doctype html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<title>团购详情-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
	<style type="text/css">
		.shopDetailSpan {
			text-align: center;
			width: 33.3%;
			display: inline-block;
		}

		.shopDetailNum {
			color: #e5004f;
			text-align: center;
			width: 33.3%;
			display: inline-block;
		}
	</style>
</head>
<body class="graybody">
<div id="doc">
	<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
   <div id="bd">
      <div class="seller-cru" style="width:1190px;margin: 20px auto 10px;">
        <p>
          您的位置&gt;
          <a href="#">首页</a>&gt;
          <a href="${contextPath}/group/list">团购列表</a>&gt;
          <span style="color: #e5004f;">团购详情</span>
        </p>
      </div>
            <!--主要信息-->
             <div class="infomain yt-wrap">

				 <div class="infomain yt-wrap">

					 <!-- 店铺信息 -->
					 <div class="info_u_r" <c:if test="${empty shopDetail.shopPic}">style="padding: 0px 7px;border:1px solid #eee;width: 190px;height: 249px;box-sizing: border-box;"</c:if> <c:if test="${!empty shopDetail.shopPic}">style="padding: 0px 7px;border:1px solid #eee;width: 190px;height: 323px;box-sizing: border-box;"</c:if>>
						 <div class="qjdlogo" style="border-bottom:0px"  <c:if test="${empty shopDetail.shopPic}">style="border-bottom:0px;padding:0px"</c:if>
						 <a href="<ls:url address='/store/${shopDetail.shopId}'/>" style="text-align: center;"> <c:if test="${not empty shopDetail.shopPic}">
							 <img style="border: 0;vertical-align: middle; width: 180px;max-height: 75px" src="<ls:photo item='${shopDetail.shopPic}'/>">
						 </c:if></a>
					 </div>
					 <div class="shopInfo" style="padding:0px">
						 <a class="shopName" href="<ls:url address='/store/${shopDetail.shopId}'/>" target="_blank" title="${shopDetail.siteName}">${shopDetail.siteName}</a><c:choose>
						 <c:when test="${shopDetail.shopType == 0}"><strong style="float: right">专营店</strong></c:when>
						 <c:when test="${shopDetail.shopType == 1}"><strong style="float: right">旗舰店</strong></c:when>
						 <c:when test="${shopDetail.shopType == 2}"><span style="float: right" class="ziying">平台自营</span></c:when>
					 </c:choose>
					 </div>
					 <ul>
						 <li><span style="float:left;">综合评分：</span> <span class="scores_scroll"> <span class="scroll_gray"></span> <span class="scroll_red" style="width: 20%"></span> </span><font id="shopScoreNum" class="hide">${sum}</font></li>
						 <li style="border-bottom: 0px;height:25px"><span class="shopDetailSpan">描述</span><span class="shopDetailSpan">服务</span><span class="shopDetailSpan">物流</span></li>
						 <li><span class="shopDetailNum">${prodAvg==null?'0.0':prodAvg}</span><span class="shopDetailNum">${shopAvg==null?'0.0':shopAvg}</span><span class="shopDetailNum">${dvyAvg==null?'0.0':dvyAvg}</span></li>
						 <c:set var="contaceQQ" value="${fn:split(shopDetail.contactQQ, ',')[0]}"></c:set>
						 <li>商城客服：<%-- <a href="http://wpa.qq.com/msgrd?v=3&uin=${contaceQQ}&site=qq&menu=yes" target="_blank">点击咨询</a> --%>
							 <input type="hidden" id="user_name" value="${user_name}"/>
							 <input type="hidden" id="user_pass" value="${user_pass}"/>
							 <input type="hidden" id="shopId" value="${shopDetail.shopId}"/>
							 <a href="javascript:void(0)" id="loginIm">联系卖家</a>
						 </li>
					 </ul>
					 <div class="col-shop-btns">
						 <a href="<ls:url address='/store/${shopDetail.shopId}'/>" target="_blank" style="font-size:12px;color:#ffffff;width:80px;height:30px;background:#333333;border:1px solid #333;">进入店铺</a>
						 <a class="col-btn" href="javascript:void(0);" onclick="collectShop('${shopDetail.shopId}')" style="font-size:12px;color:#333333;width:80px;height:30px;background:#f5f5f5;border:1px solid #cccccc;">收藏店铺</a>
					 </div>
				 </div>

               <div class="clearfix info_u_l" style="width: 1000px;">
                 	<input id="currSkuId" value="${groupdto.productDto.skuId}" type="hidden" />
                   <div class="tb-property groupon-det">
                       <div class="tb-wrap">
                            <div class="tb-detail-hd">
                                <h1><a target="_blank" href="#">${groupdto.groupName}</a></h1>
                                <p class="newp">${groupdto.productDto.name}</p>
                            </div>
                            <div class="tm-price">
                              <div class="lef">
								  <span>团购价</span>¥<font id="groupPrice" style="color: #e5004f;font-size:24px;font-family:Verdan">${groupdto.groupPrice}</font>
                              </div>
                             <%--  <div class="rig">
                                <img src="${contextPath}/resources/templets/images/time.png" />
                                	剩余时间：<span><em>10</em>时</span><span><em>10</em>分</span><span><em>10</em>秒</span>
                              </div> --%>
                              	 <div class="rig" id="group_ready" style="display: none;">
	                              	 <img src="${contextPath}/resources/templets/images/time.png" />
	                             	 <div class="pm-status-time" style="display:inline-block;">
										<span><em>0</em>天<em>0</em>时<em>0</em>分<em>0</em>秒</span>
									</div>
                           		 </div>

                           		 <div class="rig" id="group_starting" style="display: none;">
	                              	 <img src="${contextPath}/resources/templets/images/time.png" />
	                             	 <div class="pm-status-time" style="display:inline-block;">
										<span><em>0</em>天<em>0</em>时<em>0</em>分<em>0</em>秒</span>
									</div>
                           		 </div>

                           		  <div class="rig" id="group_end" style="display: none;">
	                              	 <img src="${contextPath}/resources/templets/images/time.png" />
	                             	 <div class="pm-status-time" style="display:inline-block;">
										<span><em>0</em>天<em>0</em>时<em>0</em>分<em>0</em>秒</span>
									</div>
                           		 </div>
                            </div>
                            <div class="tb-meta">
                                <!-- <dl class="tm-delivery-panel clearfix" id="J_RSPostageCont">
                                    <dt class="tb-metatit">配送至</dt>
                                    <dd>
                                        <div class="tb-postAge">
                                            <select>
                                              <option>广东 广州市 海珠区</option>
                                            </select>
                                            <div class="tb-postAge-info"><p><span>快递费：0.00</span></p></div>
                                        </div>
                                    </dd>
                                 </dl> -->
                               <c:if test="${1 == groupdto.productDto.status && empty invalid}">
										<!-- 商品属性 -->
										<div class="prop_main">
											<c:forEach items="${groupdto.productDto.prodPropDtoList}" var="prop" varStatus="psts">
												<dl class="clearfix prop_dl">
													<dt class="tb-metatit">${prop.propName}</dt>
													<dd>
														<ul prop="${prop.propName}" class="clearfix prop_ul" id="prop_${psts.index}" index="${psts.index}">
															<c:forEach items="${prop.productPropertyValueList}" var="propVal">
																<c:choose>
																	<c:when test="${propVal.isSelected}">
																		<li data-value="${prop.propId}:${propVal.valueId}" valId="${propVal.valueId}" class="li-show li-selected"><a> <c:if test="${not empty propVal.pic}">
																					<img src="<ls:images item='${propVal.pic }' scale='3'/>">
																				</c:if> <span>${propVal.name}</span> </a> <i></i>
																		</li>
																	</c:when>
																	<c:otherwise>
																		<li data-value="${prop.propId}:${propVal.valueId}" valId="${propVal.valueId}" class="li-show"><a> <c:if test="${not empty propVal.pic}">
																					<img src="<ls:images item='${propVal.pic }' scale='3'/>">
																				</c:if> <span>${propVal.name}</span> </a>
																		</li>
																	</c:otherwise>
																</c:choose>
															</c:forEach>
														</ul>
													</dd>
												</dl>
											</c:forEach>
										</div>
										</c:if>
										<!-- 商品属性 end-->
                                <!--  <div class="tb-prop tb-clear">
                                    <span class="tb-metatit">服务</span>按时发货 &nbsp;&nbsp; 极速退款 &nbsp;&nbsp;  七天无理由退换
                                 </div> -->

                                 <dl class="tb-amount clearfix">
                                    <dt class="tb-metatit">数量</dt>
                                    <dd id="J_Amount">
                                        <span class="tb-amount-widget mui-amount-wrap">

                                            <input class="tb-text mui-amount-input" style="height: 31px;" value="1" maxlength="8" id="count" onblur="validateInput();" name="count" title="请输入购买量" type="text" />
                                            <span class="mui-amount-btn">
                                                <span class="mui-amount-increase" onclick="addPamount();">∧</span>
                                                <span class="mui-amount-decrease" onclick="reducePamount();">∨</span>
                                            </span>
                                            <span class="mui-amount-unit">件</span>
                                        </span>

                                        <span style="margin-left: 10px;color: #838383;" id="stockSpan">
								            	    库存<span style="color: #005aa0;" id="stocts"> ${groupdto.productDto.stocks} </span>件
									   </span>
                                        <span id="J_StockTips">
                                        	<c:choose>
                                        		<c:when test="${groupdto.buyQuantity eq 0}">
                                        			该商品不限购
                                        		</c:when>
                                        		<c:otherwise>
                                        			（每人限购${groupdto.buyQuantity}件）
                                        		</c:otherwise>
                                        	</c:choose>
                                        	</span>
                                    </dd>
                                    <dd id="J_Amount" style="margin-top: 5px;">
                                        <span id="J_StockTips" style="color: #e2393b;">
                                        	满&nbsp;${groupdto.peopleCount}&nbsp;人成团，现购买人数为&nbsp;${groupdto.buyerCount}
                                        </span>
                                    </dd>
                                </dl>

                                 <dl class="tb-amount clearfix">
                                    <dt class="tb-metatit"></dt>
                                    <dd id="J_Amount">
                                        <a style="color: #06c" target="_blank" href="${contextPath}/views/${groupdto.productId}">对比原商品>></a>
                                    </dd>
                                </dl>
                                <c:choose>
                                 <c:when test="${shopDetail.status eq 1}">
		                              <div class="tb-action clearfix">
	                                    <div class="tb-btn-basket tb-btn-sku "><a  id="J_LinkBasket">马上参团</a></div>
	                                 </div>
	                             </c:when>
	                             <c:otherwise>
	                             	<div class="tb-action clearfix">
	                                    <div class="tb-btn-sku "><a>店铺已下线</a></div>
	                                 </div>
	                             </c:otherwise>

                                 </c:choose>

                            </div>
                       </div>
                   </div>
                   <!--产品图片-->
						<%@include file="prodpics.jsp"%>
				   <!--产品图片end-->
               </div>

            </div>
           <!--  <div class="sha-col-rep">
              <a href="#">分享</a><a href="#">收藏商品</a><a href="#">举报</a>
            </div> -->

            <!--更多信息-->
            <div class="yt-wrap info_d clearfix">
               <div class="right210_m" style="margin-left: 0">
                    <div class="detail">
                                    <div class="pagetab2">
                                       <ul>
                                         <li class="on"><span>商品详情</span></li>
                                         <!-- <li><span>参数</span></li>
                                         <li><span>累计评价<strong>(483)</strong></span></li>   -->
                                       </ul>
                                    </div>

                                    <div class="content" style="text-align:center;">
                                    		${groupdto.groupDesc}
                                    </div>
                     </div>
               </div>

            </div>
   </div>
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/group/jquery.countdown.js'/>"></script>
	<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/group/groupDetail.js'/>"></script>
	<script type="text/javascript">
	    var groupId='${groupdto.id}';
		var currProdId = '${groupdto.productDto.prodId}';
		var skuDtoList = eval('${groupdto.productDto.skuDtoListJson}');
		var propValueImgList = eval('${groupdto.productDto.propValueImgListJson}');
		var imgPath3 = "<ls:images item='PIC_DEFAL' scale='3' />";
		var startTime=${groupdto.startTime.time};
		var endTime=${groupdto.endTime.time};
		var loginUserName="${loginUser}";
		var allSelected = true;
		var limitNum="${groupdto.buyQuantity}";
		var prodStock="${groupdto.productDto.stocks}";
		var status="${groupdto.status}";
	</script>

</div>
</body>
</html>