<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<c:if test="${exchangeList != null && fn:length(exchangeList) > 0}">
       <!--浏览历史-->
        <div class="nch-module nch-module-style03">
			<div class="title">
				<h3>积分竞兑排行榜</h3>
			</div>
			<div class="content">
				<div id="nchSidebarViewed" class="nch-sidebar-viewed ps-container ps-active-y">
                  <ul>
                      <c:forEach items="${exchangeList}" var="prod" varStatus="status">
                             <li class="nch-sidebar-bowers">
		                       <div class="goods-pic"><a target="_blank" href="<ls:url address="/integral/view/${prod.id}"/>">
		                       <img alt="${prod.name}" title="${prod.name}"  max-height="60" max-width="60" src="<ls:images item='${prod.prodImage}' scale='2'/>"></a></div>
					              <dl>
					                <dt><a target="_blank" href="<ls:url address="/integral/view/${prod.id}"/>">${prod.name}</a></dt>
					                <dd>兑换积分:${prod.exchangeIntegral}</dd>
					              </dl>
		            		</li>
                      </c:forEach>
              </ul>
			</div>
		</div>
		<!--浏览历史-->
</c:if>

