<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
  <title>填写店铺详情-${systemConfig.shopName}</title>
  <meta name="keywords" content="${systemConfig.keywords}"/>
  <meta name="description" content="${systemConfig.description}" />
</head>
<body>
<div id="doc">
   
   <div id="hd">
   		 <%@ include file="../home/top.jsp" %>
   </div>
   <div id="bd">
     
       <div class="yt-wrap" style="padding:0px 0 20px;">
            <div class="seller-cru" style="width:1190px;margin: 20px auto 20px;">
              <p>
                您的位置&gt;
                <a href="#">首页</a>&gt;
                <span style="color: #e5004f;">免费开店</span>
              </p>
            </div>
            <div class="pagetab2">
               <ul>           
                 <li><span>签订入驻协议</span></li>
                 <li><span>入驻类型选择</span></li> 
                  <c:if test="${shopType eq 1}"> 
                 	<li><span>公司信息提交</span></li>
                  </c:if>  
                   <c:if test="${shopType eq 0}"> 
                   <li class="on"><span>店铺信息提交</span></li>
                   </c:if>               
                 <!-- <li class="on"><span>店铺信息提交</span></li>   --> 
                 <li><span>审核进度查询</span></li>   
               </ul>       
            </div>
            <div class="main">
                <div class="settled_box">
                  <h3>
                    <div class="settled_step">
                      <ul>
                         <li class="step7"><a href="#">签订入驻协议</a></li>
                     	 <li class="step7"><a href="#">入驻类型选择</a></li>
                         <!-- <li class="step6"><a href="#">联系人信息认证</a></li> -->
                         <c:if test="${shopType == 1}">  
                        	<li class="step6"><a href="#">公司信息提交</a></li>
                         </c:if>
                         <li class="step1"><a href="#">店铺信息提交</a></li>
                         <li class="step4"><a href="#">审核进度查询</a></li>
                      </ul>
                    </div>
                    <span class="settled_title">店铺信息</span></h3>
                  <div class="settled_white">
                  <div class="settled_white_box">
                    <div class="settled_form">
                      <h4>店铺基本信息<b>联系人信息用于入驻过程中接收反馈的入驻通知，请务必认真填写</b></h4>
                      <form:form  action="${contextPath}/p/saveShopInfo" method="post" id="shopDetaiInfo"  novalidate="novalidate"  enctype="multipart/form-data"> 
                      	<%-- <input type="hidden" value="${shopId}"/> --%>
                      	<input  type="hidden" id="type" name="type" value="${shopType}">
                      	<input  type="hidden" id="status" name="status" value="${shopDetailInfo.status}">
                      <table class="settled_table" border="0" cellpadding="0" cellspacing="0" width="760" style="font-size: 12px;">
                       <tr>
	                         <td align="right" valign="top" width="150"><span class="sred_span">
	                              负责人：</span><strong class="sred">*</strong></td>
	                          <td><input class="size200" type="text"  value="${shopDetailInfo.contactName}" maxlength="20"  id="responsible" name="responsible"><em style="margin-left: 25px;"></em></td>
	                    </tr>
                        <tr>
                        	 <td align="right" valign="top" width="150"><span class="sred_span">负责人身份证正面：</span><strong class="sred">*</strong></td>
                        	<td>
								<input id="idCardPicFile" oldFile="${shopDetailInfo.idCardPic}" name="idCardPicFile" type="file" class="inputstyle" /> <em style="margin-left: 25px;"></em>
							</td>
                        </tr>
                        <c:if test="${not empty shopDetailInfo.idCardPic}">
	                        <tr>
	                        	<td></td>
	                        	<td>
	                        		<a target="_blank" href="<ls:photo item='${shopDetailInfo.idCardPic}' />"><img width="200" height="120" src="<ls:photo item='${shopDetailInfo.idCardPic}' />"></a>
	                        	</td>
	                        </tr>
                        </c:if>
						<tr>
							<td align="right" valign="top" width="150"><span class="sred_span">负责人身份证反面：</span><strong class="sred">*</strong></td>
							<td>
								<input id="idCardBackPicFile" oldFile="${shopDetailInfo.idCardBackPic}" name="idCardBackPicFile" type="file" class="inputstyle" /> <em style="margin-left: 25px;"></em>
								</td>
						</tr>
						 <c:if test="${not empty shopDetailInfo.idCardBackPic}">
	                        <tr>
	                        	<td></td>
	                        	<td>
	                        		<a target="_blank" href="<ls:photo item='${shopDetailInfo.idCardBackPic}' />"><img width="200" height="120" src="<ls:photo item='${shopDetailInfo.idCardBackPic}' />"></a>
	                        	</td>
	                        </tr>
                        </c:if>
						 <tr>
                        	<td></td>
                        	<td><span style="color:#a2a2a2;">图片仅支持JPG、GIF、PNG、JPEG、BMP格式.大小不超过512K</span></td>
                       </tr>
                       <tr>
                          <td align="right" valign="top" width="150"><span class="sred_span">
                         		   店铺名称：</span><strong class="sred">*</strong></td>
                          <td><input class="size200" type="text" id="siteName" name="siteName" value="${shopDetailInfo.siteName}" maxlength="50"><em style="margin-left: 25px;"></em></td>
                        </tr>
                        <tr>
                          <td align="right" valign="top" width="150"><span class="sred_span">店铺类型</span><strong class="sred">*</strong></td>
                          <td>
                          	<span class="settled_select">
                              <select style="width: 105px;" id="shopType" name="shopType">
                               <option value="" >--请选择--</option>
                                <option value="0" <c:if test="${shopDetailInfo.shopType eq 0}">selected="selected"</c:if>>专营店</option>
                                <option value="1" <c:if test="${shopDetailInfo.shopType eq 1}">selected="selected"</c:if>>旗舰店</option>
                              </select>
                         		 <em style="margin-left: 25px;"></em></td>
                              </span>
                        </tr>                       
                        <tr>
                          <td align="right" valign="top" width="150"><span class="sred_span">所在地区：</span></td>
                          <td>
                           <select class="combox sele" id="provinceid" name="provinceid"  requiredTitle="true" childNode="cityid" selectedValue="${shopDetailInfo.provinceid}"
									retUrl="${contextPath}/common/loadProvinces">
								</select> <select class="combox sele" id="cityid" name="cityid" requiredTitle="true" selectedValue="${shopDetailInfo.cityid}" showNone="false" parentValue="${shopDetailInfo.provinceid}"
									childNode="areaid" retUrl="${contextPath}/common/loadCities/{value}">
								</select> <select class="combox sele" id="areaid" name="areaid" requiredTitle="true" selectedValue="${shopDetailInfo.areaid}" showNone="false" parentValue="${shopDetailInfo.cityid}"
									retUrl="${contextPath}/common/loadAreas/{value}">
								</select>
                          </td>
                        </tr>
                         <tr>
                          <td align="right" valign="top" width="150"><span class="sred_span">
                         		   店铺详细地址：</span></td>
                          <td><input class="size200" type="text" id="shopAddr" name="shopAddr" value="${shopDetailInfo. shopAddr}" maxlength="300"></td>
                        </tr>
                        <tr>
	                         <td align="right" valign="top" width="150"><span class="sred_span">
	                              联系人姓名：</span><strong class="sred">*</strong></td>
	                          <td><input class="size200" type="text" id="contactName" name="contactName" value="${shopDetailInfo.contactName}" maxlength="20"><em style="margin-left: 25px;"></em></td>
	                    </tr>
	                            <tr>
	                              <td align="right" valign="top" width="150" ><span class="sred_span">联系人手机：</span><strong class="sred">*</strong></td>
	                              <td><input class="size200" type="text" id="contactMobile" name="contactMobile" value="${shopDetailInfo.contactMobile}" maxlength="20"><em style="margin-left: 25px;"></em></td>
	                            </tr>
	                            <tr>
	                              <td align="right" valign="top" width="150"><span class="sred_span">联系人电子邮箱：</span><strong class="sred">*</strong></td>
	                              <td><input class="size200" type="text" id="contactMail" name="contactMail" value="${shopDetailInfo.contactMail}" maxlength="100"><em style="margin-left: 25px;"></em></td>
	                            </tr>
	                            <tr>
	                              <td align="right" valign="top" width="150"><span class="sred_span">联系人QQ：</span></td>
	                              <td><input class="size200" type="text" id="contactQQ" name="contactQQ" value="${shopDetailInfo.contactQQ}" onkeyup="value=value.replace(/[\uFF00-\uFFFF]/g,'')"  onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\uFF00-\uFFFF]/g,''))" maxlength="50" autocomplete="off"><label></label></td>
	                            </tr>
	                            <tr>
	                            <td></td>
	                              <td><span style="color: #a2a2a2;">多个QQ请用,分割开</span></td>
	                        </tr>
                      </table>
                    </form:form>
                  </div>
                  </div>
                  <div class="settled_bottom">
                          <span>
                            <a href = "javascript:history.back(-1);" onclick="" class="up_step_btn">上一步</a>
                            <a href="javascript:void(0);" onclick="saveShopInfo()" class="settled_btn">
                            <em>下一步，等待审核</em></a>
                          </span>
                        </div>
                </div>
            </div>
         </div>  
      </div>
   </div>
 <%@ include file="../home/bottom.jsp" %>
</div>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>"  type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/openshop.js'/>"></script>
<script type="text/javascript">
	var oldSiteName = "${shopDetailInfo.siteName}";
	$("select.combox").initSelect();
	</script>
</body>
</html>
