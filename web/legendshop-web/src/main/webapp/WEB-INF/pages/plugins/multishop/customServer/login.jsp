<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/dialog.jsp"%>
<html lang="zh-CN">
<head>
<meta charset="UTF-8">
<title>登录</title>
<link rel="stylesheet" href="${contextPath}/resources/templets/css/server/common.css">
<link rel="stylesheet" href="${contextPath}/resources/templets/css/server/login.css">
</head>
<body class="gray-body">
	<!-- 头部 -->
	<div class="u-head">
		<div class="head-box clear">
			<div class="logo fl">
				<a href="#" class="logo-img">
					<img style="height:50px" src="${OssDomainName}/${logo}" alt="">
				</a>
				<span class="logo-txt">客服端</span>
			</div>
		</div>
	</div>
	<!-- /头部 -->
	<div class="login">
		<div class="login-box">
			<div class="login-meanu">
				<span>登录客服端</span>
			</div>
			<div class="login-forms">
				<form id="form">
					<div class="user-name clear">
						<span>商家手机号</span>
						<input name="shopAccount" type="text" value="${param.phone }" placeholder="请填写商家注册手机号码">
						<input name="contactId" type="hidden" value="${contactId}">
					</div>
					<div class="user-name clear">
						<span>客服账号</span>
						<input name="account" type="text" placeholder="请填写客服账号" autocomplete="new-password">
					</div>
					<div class="user-password clear">
						<span>密码</span>
						<input name="password" type="password" placeholder="请填写密码" autocomplete="new-password">
					</div>
					<div class="code clear">
						<span>验证码</span>
						<input type="text" name="vcode" id="vcode">
						<img id="randImage" name="randImage" onclick="javascript:changeRandImg('${contextPath}')" src="${contextPath}/validCoderRandom" alt="点击刷新验证码">
					</div>
					<div class="login-button">
						<input type="submit" value="登录">
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="login-foot w1200">
		<span>copyright©2010-2020</span>
		<span>All Rights Reserved</span>
	</div>
	 <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
	<script type="text/javascript" src="${contextPath}/resources/common/js/jquery.validate.js"></script>
	<script type="text/javascript" src="${contextPath}/resources/common/js/randomimage.js"></script>
	<script type="text/javascript" src="${contextPath}/resources/templets/js/server/login.js"></script>
	<script type="text/javascript">
		var contextPath = "${contextPath}";
	</script>
</body>
</html>
