<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<jsp:useBean id="now" class="java.util.Date" /> 
<fmt:formatDate value="${now}"  pattern="yyyy-MM-dd HH:mm:ss" var="nowDate"/>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>满减满折-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/active/actives${_style_}.css'/>" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">满减满折</span>
				</p>
			</div>
			
			<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>

			<div id="rightContent" class="right_con" style="width: 960px;">
				<div class="pagetab2" style="margin-top: 20px;">
					<ul id="listType">
						<li <c:if test="${empty marketing.searchType || marketing.searchType eq 'ALL'}">class="on"</c:if>><span><a
								href="<ls:url address="/s/shopSingleMarketing?searchType=ALL"/>">全部活动</a></span></li>
						<li <c:if test="${marketing.searchType eq 'NO_PUBLISH'}">class="on"</c:if>><span><a
								href="<ls:url address="/s/shopSingleMarketing?searchType=NO_PUBLISH"/>">未发布</a></span></li>
						<li <c:if test="${marketing.searchType eq 'NOT_STARTED'}">class="on"</c:if>><span><a
								href="<ls:url address="/s/shopSingleMarketing?searchType=NOT_STARTED"/>">未开始</a></span></li>
						<li <c:if test="${marketing.searchType eq 'ONLINE'}">class="on"</c:if>><span><a
								href="<ls:url address="/s/shopSingleMarketing?searchType=ONLINE"/>">进行中</a></span></li>
						<li <c:if test="${marketing.searchType eq 'PAUSE'}">class="on"</c:if>><span><a
								href="<ls:url address="/s/shopSingleMarketing?searchType=PAUSE"/>">已暂停</a></span></li>
						<li <c:if test="${marketing.searchType eq 'FINISHED'}">class="on"</c:if>><span><a
								href="<ls:url address="/s/shopSingleMarketing?searchType=FINISHED"/>">已结束</a></span></li>
						<li <c:if test="${marketing.searchType eq 'EXPIRED'}">class="on"</c:if>><span><a
								href="<ls:url address="/s/shopSingleMarketing?searchType=EXPIRED"/>">已失效</a></span></li>
					</ul>
   				</div>
				<div class="search-add" style="margin-top: 15px">
				<div class="set-up sold-ser sold-ser-no-bg clearfix" style="padding:15px 18px">
					<div style="float: left;">
						<span style="display: inline-block; margin-top: 6px">点击添加新的满减满折活动：</span>
				 			<a href="<ls:url address='/s/addShopMarketing/0'/>" style="margin:0;" class="btn-r big-btn">添加活动</a>
				     </div>
						<form:form  action="${contextPath}/s/shopSingleMarketing" id="form2">
							<div style="float: right;">
								<input type="hidden" value="${marketing.searchType}" name="searchType" id="searchType" />
								<span style="display:inline-block ;margin-top: 6px">活动名称：</span>
							   	<input type="hidden" value="${empty curPageNO?1:curPageNO}" name="curPageNO"  id="curPageNO"> 
							   	<input type="text" value="${marketing.marketName}" name="marketName" id="marketName" class="text item-inp" placeholder="请输入活动名称">
								<input type="submit" value="搜索" class="submit btn-r">
							</div>
						</form:form>
					</div>
				</div>
			<table class="ncm-default-table sold-table">
			<thead>
				<tr>
					<th class="w10">活动名称</th>
					<th class="w150 tl">活动时间</th>
					<th class="w150 tl">满折/满减</th>
					<th class="w150 tl">参与商品</th>
					<th class="w150 tl">活动状态</th>
					<th class="w150 tl" style="border-right: 1px solid #e4e4e4">操作</th>
				</tr>
			</thead>
			<tbody>
			 <c:choose>
		           <c:when test="${empty list}">
		              <tr>
					 	 <td colspan="20">
					 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
					 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
					 	 </td>
					 </tr>
		           </c:when>
		           <c:otherwise>
		           	   <c:forEach items="${list}" var="ms" varStatus="status">
						   <tr>
								<td>${ms.marketName}</td>
							   <td>
							   <div class="time" style="padding: 0">
									<span>
										<fmt:formatDate value="${ms.startTime}" pattern="yyyy-MM-dd HH:mm:ss" type="date" />
									</span>
								   <span style="display: block">
									   至
									</span>
								   <span>
										<fmt:formatDate value="${ms.endTime}" pattern="yyyy-MM-dd HH:mm:ss" type="date" />
									</span>
							   </div>
							   </td>
							   <td>
								   <c:choose>
									   <c:when test="${ms.type==0}">
										   满减
									   </c:when>
									   <c:when test="${ms.type==1}">
										   满折
									   </c:when>
								   </c:choose>
							   </td>
								<td>
								<c:choose>
										<c:when test="${ms.isAllProds==0}">
										       部分商品
										</c:when>
										<c:when test="${ms.isAllProds==1}">
										       全部商品
										</c:when>
								</c:choose>
								</td>
								<td>
								<c:choose>
									<c:when test="${ms.state==0 && ms.endTime gt nowDate}">
										<span style="color:#e5004f">未发布</span>
									</c:when>
									<c:when test="${ms.startTime gt nowDate}">
										未开始
									</c:when>
									<c:when test="${ms.endTime lt nowDate}">
										已结束
									</c:when>
									<c:when test="${ms.state==1}">
										进行中
									</c:when>
									<c:when test="${ms.state==2}">
										已暂停
									</c:when>
									<c:when test="${ms.state==3}">
										已失效
									</c:when>
								</c:choose>
								</td>
								<td width="17%">
								   <span>
									   <c:if test="${ms.state==0 && nowDate lt ms.endTime || ms.state==2 || nowDate lt ms.startTime}">
										   <a class="btn-r" href="<ls:url address='/s/shopMarketingManage/${ms.id}'/>">编辑</a>
									   </c:if>
									    <c:if test="${ms.state!=0 && ms.state!=2 && nowDate gt ms.startTime || nowDate gt ms.endTime}">
											<a class="btn-r" href="<ls:url address='/s/shopMarketing/${ms.id}'/>">查看</a>
										</c:if>
								   		<c:if test="${nowDate lt ms.endTime}">
												<c:if test="${ms.state==0}">
												    <a onclick="onlineShopMarketing('${ms.id}','发布');" class="btn-r" href="javascript:void(0);">发布</a>
												</c:if>
												<c:if test="${ms.state==2}">
													<a onclick="onlineShopMarketing('${ms.id}','恢复');" class="btn-r" href="javascript:void(0);">恢复</a>
												</c:if>
												<c:if test="${ms.state==1 && nowDate gt ms.startTime}">
													<a onclick="suspendShopMarketing('${ms.id}');" class="btn-g" href="javascript:void(0);">暂停</a>
												</c:if>
												<c:if test="${ms.state==1 && nowDate gt ms.startTime || ms.state==2}">
												      <a onclick="offlineShopMarketing('${ms.id}');" class="btn-g" href="javascript:void(0);">终止</a>
												</c:if>
								   		</c:if>

										<c:if test="${ms.state==0  || ms.state==3 || nowDate gt ms.endTime || ms.state==1 && nowDate lt ms.startTime }">
										    <a onclick="deleteShopMarketingMansong('${ms.id}');" class="btn-g" href="javascript:void(0);" >删除</a>
										</c:if>
									</span>
								</td>
							</tr>
						</c:forEach>
		           </c:otherwise>
		      </c:choose>
			</tbody>
		</table>
			 <div style="margin-top:10px;" class="page clearfix">
				  <div class="p-wrap">
				         <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
				  </div>
		  	</div>
		  	
			<div class="clear"></div>
		  </div>
		  
		</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/shopSingleDiscountList.js'/>"></script>
<script type="text/javascript">
    var contextPath = '${contextPath}';
    var chang=${marketing.type};
    $(document).ready(function() {
    	if(chang>=4){
    		userCenter.changeSubTab("FULL_MAIL"); //高亮菜单
    	}else{

    		userCenter.changeSubTab("SINGLE_MARKETING"); //高亮菜单
    	}
    });
</script> 
</html>