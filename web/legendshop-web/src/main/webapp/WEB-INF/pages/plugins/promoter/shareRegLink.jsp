<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@include file='/WEB-INF/pages/common/layer.jsp' %>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>我要推广</title>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/applyPromoter${_style_}.css'/>" rel="stylesheet"/>
</head>
<style>
    .promote .pro-net input {
        margin: -5px 8px -3px;
    }
</style>
<body class="graybody">
<div id="bd">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>

    <!----两栏---->
    <div class=" yt-wrap" style="padding-top:10px;">
        <%@include file='/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp' %>

        <!-----------right_con-------------->
        <div class="right_con">
            <div class="o-mt"><h2>我要推广</h2></div>
            <div class="pagetab2">
                <ul>
                    <li class="on"><span>我要推广</span></li>
                </ul>
            </div>
            <div class="promote">
                <div class="pro-step">
                    <ul>
                        <li><span>分享二维码或链接<br>邀请好友注册</span></li>
                        <li><span>好友后续消费<br>获取佣金</span></li>
                        <li><span>好友邀请来的以及好友的好友<br>邀请来的，您也能获取佣金</span></li>
                    </ul>
                </div>
                <div class="pro-net">推广链接：${domainName}/reg?uid=${userName}
                    <!--去掉之前依赖浏览器flash插件的复制方式  -->
                    <!-- <input type="button" value="复制" id="copy_input"/>

                    <span style="color: red;margin-left: 10px;" id="success"></span> -->

                    <input type="button" value="复制" id="copydomainName" class="copybtn btn-r small-btn" data-clipboard-text="${domainName}/reg?uid=${userName}"/>
                </div>
                <div class="pro-cod">推广二维码：
                    <div id="qrcode" style="margin-left: 75px;"></div>
                </div>
                <div class="pro-rul">
                    <div class="rul-tit">活动规则</div>
                    <c:if test="${originDistSet.regCommis>0}">
                        <p>每邀请一个好友完成注册，您可以获得&nbsp;￥${originDistSet.regCommis}&nbsp;作为奖励</p>
                    </c:if>
                    <p>通过您分享的链接完成注册的会员，后续消费您都能获得消费金额的&nbsp;${originDistSet.firstLevelRate}%&nbsp;作为奖励</p>
                    <c:if test="${originDistSet.supportDist==1}">
                        <p>您所邀请的会员，后续再邀请了其他会员，您能获得这部分会员消费金额的&nbsp;${originDistSet.secondLevelRate}%&nbsp;作为奖励</p>
                        <p>您的下级会员所发展的会员，后续邀请了其他会员，您能获得这部分会员消费金额的&nbsp;${originDistSet.thirdLevelRate}%&nbsp;作为奖励</p>
                    </c:if>
                    <p>奖励自动结算，一键提现，自动转入您的预存款</p>
                </div>
            </div>
        </div>
        <!--right_con end-->

    </div>

    <div class="clear"></div>
</div>
<!----两栏end---->

<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<%@ include file="/WEB-INF/pages/common/jquery.jsp" %>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/qrcode/jquery.qrcode.min.js'/>"></script>
<%-- <script type="text/javascript" src="<ls:templateResource item='/resources/plugins/zclip/jquery.zclip.min.js'/>"></script> --%>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/clipboard/clipboard.min.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/applyPromoter.js'/>"></script>
<script type="text/javascript">
    var contextPath = "${contextPath}";
    $(document).ready(function () {
        userCenter.changeSubTab("applyPromoter");
        jQuery('#qrcode').qrcode({width: 150, height: 150, text: "${domainName}/reg?uid=${userName}"});
    });
    var clipboard = new Clipboard('.copybtn');
    clipboard.on('success', function (e) {
        layer.msg("复制成功", {icon: 1});
    });
</script>
</body>
</html> 