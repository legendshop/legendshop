<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>运营报告-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-report.css'/>" rel="stylesheet">
	<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/templets/css/multishop/shopOrders.css'/>" />
	<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/pagination.css'/>" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
	<div id="Content" class="w1190">
	<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">运营报告</span>
				</p>
			</div>
		<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
		<div class="seller-report">
		 	<div class="seller-com-nav" >
					<ul>
							<li class="on"><a href="javascript:void(0);">销售统计</a></li>
							<li ><a href="${contextPath}/s/shopReport/regionalDistribution">区域分布</a></li>
					</ul>
			</div>
			<form:form  action="${contextPath}/s/shopReport/operatingReport" method="get" id="from1">
				<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO ==null?1:curPageNO}"/>
				<div class="form search-01 sold-ser sold-ser-no-bg clearfix" >
					<div class="fr">
						<div class="item" style="margin-right:0;">
							<select id="queryTerms" class="sele item-sel"  name="queryTerms" onchange="change(this.value)"> 
								 <option <c:if test="${reportRequest.queryTerms == 0 }">selected</c:if> value="0">按照天统计</option>
								 <option <c:if test="${reportRequest.queryTerms == 3 }">selected</c:if> value="3">按照周统计</option>
								 <option <c:if test="${reportRequest.queryTerms == 1 }">selected</c:if> value="1">按照月统计</option>
					        </select>
						</div>        
						<div class="item" style="margin-right:10px;">
					        <span id="range">
						 		  <input readonly="readonly"  name="selectedDate" id="selectedDate" class="item-inp" type="text" onClick="WdatePicker()" value='<fmt:formatDate value="${reportRequest.selectedDate}" pattern="yyyy-MM-dd"/>' />
							</span>
						</div> 
						<div class="item" style="margin-right:0;">		
							<select id="status" class="sele item-sel"  name="status">
					        	<ls:optionGroup type="select" required="true" cache="true" beanName="ORDER_STATUS" selectedValue="${reportRequest.status}"/>          			
					        </select>
							<input type="button" onclick="search()" class="btn-r" id="btn_keyword" value="查 询" />
						</div>
					</div>
				</div>
			</form:form>
	<input type="hidden" id="reportJson" value='${reportJson}' /> 
	
	<div id="main" style="margin-left: 10px;width:98%;height:400px"></div>	

	<table class="ncsc-default-table order report-table sold-table" style="margin-top:15px;">
				<tr class="report-tit">
					<th class="w10"></th>
					<th colspan="2">商品</th>
					<th class="w100">单价（元）</th>
					<th class="w40">数量</th>
					<th class="w100">买家</th>
					<th class="w100">订单金额</th>
					<th class="w90">交易状态</th>
				</tr>
				<c:choose>
		           <c:when test="${empty requestScope.list}">
		              <tr>
					 	 <td colspan="20">
					 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
					 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
					 	 </td>
					 </tr>
		           </c:when>
		           <c:otherwise>
		              <c:forEach items="${requestScope.list}" var="order" varStatus="orderstatues">
					       <tr>
							 <td class="sep-row" colspan="20"></td>
						    </tr>
						  <tr class="detail">
							<th colspan="20">
								<span class="ml10">订单编号：<em>${order.subNum}</em></span>
								<span>下单时间：<em class="goods-time"><fmt:formatDate value="${order.subDate}" type="both" /></em></span>
								<span>订单类型：
									<em class="goods-time">
										<c:choose>
											<c:when test="${order.subType == 'AUCTIONS'}">拍卖订单</c:when>
											<c:when test="${order.subType == 'SHOP_STORE'}">门店订单</c:when>
											<c:when test="${order.subType == 'SECKILL'}">秒杀订单</c:when>
											<c:when test="${order.subType == 'GROUP'}">团购订单</c:when>
											<c:when test="${order.subType == 'MERGE_GROUP'}">拼团订单</c:when>
											<c:when test="${order.subType == 'PRE_SELL'}">预售订单</c:when>
											<c:otherwise>普通订单</c:otherwise>
										</c:choose>
									</em>
								</span> 
							</th>
						  </tr>
						 <c:forEach items="${order.subOrderItemDtos}" var="orderItem" varStatus="orderItemStatues">
						    <tr class="detail">
							  <td class="bdl"></td>
							  <td class="w70">
							     <div class="ncsc-goods-thumb">
									<a target="_blank"
										href="<ls:url address='/views/${orderItem.prodId}'/>">
										<img src="<ls:images item='${orderItem.pic}' scale='3'/>">
									</a>
								</div>
							  </td>
								<td class="tl">
								   <dl class="goods-name">
										<dt>
											<a href="<ls:url address='/views/${orderItem.prodId}'/>"
												target="_blank">${orderItem.prodName}</a><a
												href="<ls:url address='/views/${orderItem.prodId}'/>"
												class="blue ml5" target="_blank">[交易快照]</a>
										</dt>
										<dd>${orderItem.attribute}</dd>
										<!-- S消费者保障服务 -->
										<!-- E消费者保障服务 -->
									</dl>
								</td>
							<td><p>${orderItem.productTotalAmout}</p></td>
							<td>${orderItem.basketCount}</td>
		
							<!-- S 合并TD -->
							 <c:if test="${orderItemStatues.index==0}">
							    <c:choose>
				                		<c:when test="${fn:length(order.subOrderItemDtos)>0}">
				                			<!-- 未支付 -->
				                			<td rowspan="${fn:length(order.subOrderItemDtos)}" class="bdl">
				                		</c:when>
				                		<c:otherwise>
				                			<td class="bdl">
				                		</c:otherwise>
				                </c:choose>
				                <div class="buyer">
									${order.userName}
									 <c:if test="${not empty order.userAddressSub}">
				                       <div class="buyer-info">
										<em></em>
										<div class="con">
											<h3>
												<i></i><span>买家信息</span>
											</h3>
											<dl>
												<dt>姓名：</dt>
												<dd>${order.userAddressSub.receiver}</dd>
											</dl>
											<dl>
												<dt>电话：</dt>
												<dd>${order.userAddressSub.telphone}</dd>
											</dl>
											<dl>
												<dt>电话：</dt>
												<dd>${order.userAddressSub.mobile}</dd>
											</dl>
											<dl>
												<dt>地址：</dt>
												<dd>${order.userAddressSub.province}&nbsp;${order.userAddressSub.city}&nbsp;${order.userAddressSub.area}&nbsp;${order.usrAddrSubDto.subAdds}</dd>
											</dl>
										</div>
									  </div>
				                    </c:if>
								</div>
								</td>
							 </c:if>
							 
							   <c:if test="${orderItemStatues.index==0}">
							     <c:choose>
				                		<c:when test="${fn:length(order.subOrderItemDtos)>0}">
				                			<!-- 未支付 -->
				                			<td rowspan="${fn:length(order.subOrderItemDtos)}" class="bdl">
				                		</c:when>
				                		<c:otherwise>
				                			<td class="bdl">
				                		</c:otherwise>
				                </c:choose>
				                <p class="ncsc-order-amount">${order.actualTotal}</p>
								<p class="goods-freight">
								  <c:choose>
				                		<c:when test="${not empty order.freightAmount}">
				                		  (含运费${order.freightAmount})
				                		</c:when>
				                		<c:otherwise>
				                			（免运费）
				                  		</c:otherwise>
				                 </c:choose>
								</p>
								<p  class="goods-pay">${order.payManner==1?"货到付款":"在线支付"}</p>
								</td>
							  </c:if>
							  
							   <c:if test="${orderItemStatues.index==0}">
							        <c:choose>
				                		<c:when test="${fn:length(order.subOrderItemDtos)>0}">
				                			<!-- 未支付 -->
				                			<td rowspan="${fn:length(order.subOrderItemDtos)}" class="bdl bdr">
				                		</c:when>
				                		<c:otherwise>
				                			<td class="bdl bdr">
				                		</c:otherwise>
				                   </c:choose>
				                   <p>
							     <c:choose>
									 <c:when test="${order.status==1 }">
									        待付款
									 </c:when>
									 <c:when test="${order.status==2 }">
									       待发货
									 </c:when>
									 <c:when test="${order.status==3 }">
									       待收货
									 </c:when>
									 <c:when test="${order.status==4 }">
									      已完成
									 </c:when>
									 <c:when test="${order.status==5 }">
									      交易关闭
									 </c:when>
								  </c:choose>
									  </p> <!-- 订单查看 -->
										<p>
											<a target="_blank"  href="${contextPath}/s/orderDetail/${order.subNum}">订单详情</a>
										</p> 
								
				                   </td>
							   </c:if>
						     </tr>
						  </c:forEach> 
					 </c:forEach>
		           </c:otherwise>
		         </c:choose>
	</table>
	</div>
	
	 <div style="margin-top:10px;" class="page clearfix">
          <div class="p-wrap">
              <ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="simple"/>
          </div>
      </div>
</div>
<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<script src="<ls:templateResource item='/resources/plugins/ECharts/dist/echarts.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/templets/js/multishop/shopReport.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/templets/js/multishop/operatingReport.js'/>" type="text/javascript"></script>
<script type="text/javascript">
var contextPath = '${contextPath}';
var selectedYear = '${reportRequest.selectedYear}';
var selectedMonth = '${reportRequest.selectedMonth}';
var selectedDay = '<fmt:formatDate value="${reportRequest.selectedDate}" pattern="yyyy-MM-dd"/>';
var selectedWeek = '${reportRequest.selectedWeek}';
var nowYear = new Date().getFullYear();
var nowMonth = new Date().getMonth() + 1;
var commonYear = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];//平年月份的总天数
var LeapYear = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];//闰年月份的总天数 
function search(){
  	$("#from1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
  	$("#from1")[0].submit();
}
$(function(){
	laydate.render({
		   elem: '#selectedDate',
		   calendar: true,
		   theme: 'grid',
		   trigger: 'click'
	  });
});
</script>
</body>
</html>
