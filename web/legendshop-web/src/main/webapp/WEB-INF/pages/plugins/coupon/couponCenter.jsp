<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>领券中心-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link rel="stylesheet" href="<ls:templateResource item='/resources/templets/css/integral-mall${_style_}.css'/>" />
    <style type="text/css">
        .disable {
            background-color: #bcbbbd;
            font: normal 12px/20px "microsoft yahei", arial;
            color: #FFF;
            text-align: center;
            vertical-align: middle;
            display: inline-block;
            height: 20px;
            padding: 5px 10px;
            border-radius: 3px;
            pointer-events: none;
            cursor: not-allowed;
        }
    </style>
</head>

<body  style="background: #ffffff;">
	<div id="doc">
	    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
	    
		<div id="bd">
            <!--crumb-->
          <div class="seller-cru" style="width:1190px;margin: 20px auto 10px;">
            <p>您的位置&gt;
              <a href="${contextPath}/home">首页</a>&gt;
              <span style="color: #e5004f;">领券中心</span>
            </p>
          </div>
            <!--crumb end-->


      <div class="integral-mall">
        <c:if test='${empty couponList and empty redpackList}' >
	        	<div class="selection_box search_noresult">
					<p class="search_nr_img"><img src="<ls:templateResource item='/resources/templets/images/not_result.png'/>"></p> 
					<p style="text-align: center;font-size: 16px;margin-top: 10px;">没有找到<c:if test="${not empty param.keyword}">与“<span>${param.keyword}</span>”</c:if>相关的优惠信息</p>
				</div>
		 </c:if>
      
      
      
	<c:if test="${not empty couponList}">
        <div class="integral-list">
          <div class="int-tit">
            <h3><i class="int-cou-tit"></i>热门优惠券</h3>
            <span><a href="javascript:void(0);" id="coupon">更多&gt;&gt;</a></span>
          </div>
          <ul class="int-con">
          
          
          <c:forEach items="${couponList}" var="coupon">
            <li>
              <div class="int-con-lef">
                <div class="cut"></div>
                <div class="info">
                <c:if test="${coupon.couponProvider eq 'platform' }">
                  <%--<a href="#" >${coupon.couponName}</a>--%>
                </c:if>
                <c:if test="${coupon.couponProvider eq 'shop' }">
                  <a href="${contextPath}/store/${coupon.shopId}" class="store">${coupon.shopName}</a>

                </c:if>
                  <div class="pic">
                  	<c:if test="${coupon.couponProvider eq 'shop' }">
                   	 	<c:if test="${empty coupon.couponPic }">
                  			<img src="${contextPath}/resources/templets/images/coupon/coupon-default.png"/>
                  		 </c:if>
                  		<c:if test="${not empty coupon.couponPic }">
                    		<img src="<ls:photo item='${coupon.couponPic}'/>">
                    	</c:if>
                    </c:if>
                    
                    <c:if test="${coupon.couponProvider eq 'platform' }">
                    	<c:if test="${empty coupon.couponPic }">
                  			<img src="${contextPath}/resources/templets/images/coupon/red-default.png"/>
                  		</c:if>
                  		<c:if test="${not empty coupon.couponPic }">
                    		<img src="<ls:photo item='${coupon.couponPic}'/>">
                    	</c:if>
                    </c:if>

                  </div>
                    <a class="store" >${coupon.couponName}</a>
                </div>
                <dl class="value">
                  <dt>¥<em>${coupon.offPrice}</em></dt>
                  <dd>购物满${coupon.fullPrice}元可用</dd>
                  <dd class="time">有效期至<fmt:formatDate value="${coupon.endDate}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>
                </dl>
                <div class="point">
                  <p><em>${coupon.bindCouponNumber}</em>人已兑换</p>
                </div>
                <c:choose>
                    <c:when test="${coupon.couponNumber > coupon.bindCouponNumber}">
                        <div class="button" couponId="${coupon.couponId}"><a href="javascript:void(0);">确认领取</a></div>
                    </c:when>
                    <c:otherwise>
                        <div class="button" style="pointer-events: none;cursor: not-allowed">
                            <span class="disable">已领完</span>
                        </div>
                    </c:otherwise>
                </c:choose>
              </div>
            </li>
            </c:forEach>
            
          </ul>
        </div>
    </c:if>
<!-- 热门代金券 -->
	<c:if test="${not empty redpackList}">
        <div class="integral-list">
          <div class="int-tit">
            <h3><i class="int-red-tit"></i>热门红包</h3>
            <span><a href="javascript:void(0);" id="redPack">更多&gt;&gt;</a></span>
          </div>
          <ul class="int-con">
          
          <c:forEach items="${redpackList}" var="coupon">
            <li>
              <div class="int-con-lef">
                <div class="cut"></div>
                <div class="info">
                <c:if test="${coupon.couponProvider eq 'platform' }">
                  <a href="#" class="store">${coupon.couponName}</a>
                </c:if>
                <c:if test="${coupon.couponProvider eq 'shop' }">
                  <a href="${contextPath}/store/${coupon.shopId}" class="store">${coupon.shopName}</a>
                </c:if>
                  <div class="pic">
                  	<c:if test="${coupon.couponProvider eq 'shop' }">
                   	 	<c:if test="${empty coupon.couponPic }">
                  			<img src="${contextPath}/resources/templets/images/coupon/coupon-default.png"/>
                  		 </c:if>
                  		<c:if test="${not empty coupon.couponPic }">
                    		<img src="<ls:photo item='${coupon.couponPic}'/>">
                    	</c:if>
                    </c:if>
                    
                    <c:if test="${coupon.couponProvider eq 'platform' }">
                    	<c:if test="${empty coupon.couponPic }">
                  			<img src="${contextPath}/resources/templets/images/coupon/red-default.png"/>
                  		</c:if>
                  		<c:if test="${not empty coupon.couponPic }">
                    		<img src="<ls:photo item='${coupon.couponPic}'/>">
                    	</c:if>
                    </c:if>
                  </div>
                </div>
                <dl class="value">
                  <dt>¥<em>${coupon.offPrice}</em></dt>
                  <dd>购物满${coupon.fullPrice}元可用</dd>
                  <dd class="time">有效期至<fmt:formatDate value="${coupon.endDate}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>
                </dl>
                <div class="point">
                  <p><em>${coupon.bindCouponNumber}</em>人已兑换</p>
                </div>
                  <c:choose>
                      <c:when test="${coupon.couponNumber > coupon.bindCouponNumber}">
                          <div class="button" couponId="${coupon.couponId}"><a href="javascript:void(0);">确认领取</a></div>
                      </c:when>
                      <c:otherwise>
                          <div class="button" style="pointer-events: none;cursor: not-allowed">
                              <span class="disable">已领完</span>
                          </div>
                      </c:otherwise>
                  </c:choose>
              </div>
            </li>
            
          </c:forEach>  
            
          </ul>
        </div>
      </c:if>     
      </div>
   </div> 
	 <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
    <script src="<ls:templateResource item='/resources/templets/js/couponCenter.js'/>"></script>
	</body>
</html>