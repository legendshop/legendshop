<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>错误页面</title>
    <style type="text/css">
      .seller-selling {
		    float: right;
		    overflow: hidden;
		    width: 966px;
		    margin-top: 20px;
		    background-color: #ffffff;
	}
      .selling-sea{height: 54px;border: 1px solid #e4e4e4;width: 964px;line-height: 54px;background-color: #ffffff;text-align: center;height: 130px;}
    </style>
    
</head>

<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            <!----两栏---->
     <div class=" yt-wrap" style="padding-top:10px;"> 
     <%@ include file="/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp" %>
    <!-----------right_con-------------->
     <div class="right_con">
        <div class="o-mt">
			<h2>错误页面</h2>
		</div>
           <div class="seller-selling">
			    	<div class="selling-sea">
							<div class="reg-tips-info" style="color: #CC0000;font-weight: bold;">抱歉，操作失败！</div>
					       				<div class="reg-nickname-tips">${messgae}</div>
						</div>
			    	
			</div>
     
     </div>
    <!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
</body>
</html>
