<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>账户安全 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/securityCenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/validateChanges.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;">     
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con"  id="rightContent">
<div class="right">
		<c:choose>
		<c:when test="${phoneVerifn ==0}">
			<div class="o-mt"><h2>验证手机</h2></div>
			<div id="step2" class="step step01">
				<ul>
					<li class="fore1">1.验证身份<b></b></li><li class="fore2">2.验证手机<b></b></li><li class="fore3">3.完成</li>
				</ul>
			</div>
		</c:when>
		<c:otherwise>
			<div class="o-mt"><h2>修改手机</h2></div>
			<div id="step2" class="step step01">
				<ul>
					<li class="fore1">1.验证身份<b></b></li><li class="fore2">2.修改手机<b></b></li><li class="fore3">3.完成</li>
				</ul>
			</div>
		</c:otherwise>
		</c:choose>
		
		<div class="m m1 safe-sevi">
			<div class="mc">
		
    			<div class="form form01">
    				<div class="item">
    					<span class="label">我的手机号：</span>
    					<div class="fl">
    						<input name="oldPassword"  id="oldPassword"  type="hidden" value="${oldPassword }">
							<input name="securityCode"  id="securityCode"  type="hidden" value="${securityCode }">
    						<input class="text" tabindex="1" name="userMobile" id="userMobile" type="text" maxlength="15">
							<div class="clr"></div>
							<div class="msg-text" id="mobiletext" style="display:none">请填写正确的手机号码，我们将免费把校验码发送到您的手机</div>
							<div class="clr"></div>
							<div id="mobile_error" class="msg-error"></div>
    					</div>
    					<div class="clr"></div>
    				</div>
    				
    				<div class="item">
        				<span class="label">验证码：</span>
        				<div class="fl">
        					<input type="hidden" id="cannonull" name="cannonull" value='<fmt:message key="randomimage.errors.required"/>'/>
							<input type="hidden" id="charactors4" name="charactors4" value='<ls:i18n key="randomimage.charactors.required" length="4"/>'/> 
							<input type="hidden" id="errorImage" name="errorImage" value='<fmt:message key="error.image.validation"/>'/> 
												
							<input class="text" tabindex="2" name="randNum" id="randNum" type="text">
							<label><img id="randImage" name="randImage" src="<ls:templateResource item='/validCoderRandom'/>"  style="vertical-align: middle;"/>看不清？
            				<a href="javascript:void(0)" onclick="javascript:changeRandImg('${contextPath}')" style="font-weight: bold;"><fmt:message key="change.random2"/></a>
            				</label>
        					<div class="clr"></div>
        					<div id="authCode_error" class="msg-error"></div>
        				</div>
        				<div class="clr"></div>
        			</div>
        			
    				<div class="item">
    					<span class="label">&nbsp;</span>
    					<div class="fl" style="width: 450px">
							<a class="btn-r ncbtn" id="sendMobileCode" href="javascript:void(0)"  ><s></s>获取短信校验码</a>
								<div class="clr" style="margin-bottom: 5px;"></div>
        						<div id="countDown"  class="msg-text" style="display: none;">校验码已发出，请注意查收短信，如果没有收到，你可以在<strong id="timer" class="ftx-01"></strong> 秒要求系统重新发送</div>
        						<div class="clr"></div>
        						<div class="msg-error" id="sendCode_error" style="display: none;"></div>
						</div>
    					<div class="clr"></div>
    				</div>
				</div>
				
				<div class="form">
    				<div class="item">
    					<span class="label">请填写手机校验码：</span>
    					<div class="fl">
    						<input class="text" name="code"  tabindex="2" id="code" disabled="disabled" type="text">
							<div class="clr"></div>
    						<div id="code_error" class="msg-error"></div>
    					</div>
    					<div class="clr"></div>
    				</div>
    				
    				
    				
    				<div class="item">
    					<span class="label">&nbsp;</span>
    					<div class="fl">
								<a id="submitCode" class="btn-r small-btn ncbtn" onclick="javascript:updateMobile.bindMobile()" href="javascript:void(0);"><s></s>提交</a>
								<a class="btn-r small-btn ncbtn" href="${contextPath}/p/security"><s></s>返回</a>
						</div>
    					<div class="clr"></div>
    				</div>
				</div>
			</div>
		</div>
		<div class="m m7">
			<div class="mt"><h3>为什么要验证手机？</h3></div>
			<div class="mc">1.验证手机可加强账户安全，您可以使用已验证手机快速找回密码或支付密码；<br>2.已验证手机可用于接收账户余额变动提醒；</div>
		</div>
	</div>

 </div>
    <!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script type="text/javascript"  src="<ls:templateResource item='/resources/common/js/randomimage.js'/>"></script>
<script type="text/javascript">
	var wait;
	var timer = null;
	var interVal = 60;
	var userMobile = '${userMobile}';
	var securityCode = '${securityCode}';
	var contextPath = '${contextPath}';
	$(document).ready(function() {
		updateMobile.doExecute();
		
		$("#userMobile").blur(function(){
			var mobile = $("#userMobile").val();
			var mobileError = $("#mobile_error");
			if(userMobile == mobile){
				mobileError.show();
				mobileError.text("请更换与当前手机号码不一致的手机号码");
			}
		});
		
	});
</script>
</body>
</html>