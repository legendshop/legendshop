<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
	<title>${systemConfig.shopName}
	</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="home/top.jsp"%>
		<div id="bd">
<div class="w" > 
    <div class="login_left wrap">
      <div>
         <div>
         <div>
				<div style="width:500px;margin:auto;text-align: left;margin-bottom:100px;">
				    <div style="margin-top: 50px;"><center><img src="${contextPath}/resources/templets/images/not_prod.png"/></center></div>
	                <div style="margin-top:50px;">
	                   <center> <b style="font-size:16px;color:#404040;">很抱歉，您查看的商品不存在，可能已下架或者被删除。</b></center>
	                </div>
	                <div style="margin-top:20px;margin-left:50px;">
	                	<ol style="margin-top:10px;">
						 <li>1. 该商品可能已经下架，您可以联系商家找找商品。</li>
						 <li>2. 在顶部搜索框重新输入关键词搜索</li>
						</ol>
	                </div>
				</div>
         <div class="clear"></div>
     </div>   
        </div>      
      </div>                                  
    </div>
    <!----左边end---->
    
   <div class="clear"></div>
</div>
</div>
		<%@ include file="home/bottom.jsp"%>
	</div>
</body>
</html>