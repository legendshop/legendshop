<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>商品发布-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
<link href="<ls:templateResource item="/resources/templets/css/category-min${_style_}.css" />" rel="stylesheet" type="text/css" media="screen" />
<link href="<ls:templateResource item="/resources/templets/css/core.css" />" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/showLoading.css'/>" />

</head>
<body  class="graybody">
	<div id="doc">
		 <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		  <div class="w1190">
		  	<div class="seller-cru">
				<p>
					您的位置&gt;
					<a href="${contextPath}/sellerHome">首页</a>&gt;
					<span class="on">商品发布</span>
				</p>
			</div>
			
			<%@ include file="included/sellerLeft.jsp" %>
			
			<div id="layout" style="margin-top:20px;float:right;width:960px;font-size: 12px;">
				<div class="tabsPage" id="navTab">
					<div class="tabsPageContent" style="border: 0px;">
							
							<div class="pagetab2" >
								<ul>
									<li class="on" onclick="switchFocus(this,0);">选择商品总分类</li>
									<li onclick="importSimilarProd();">类似商品导入</li>
									<!-- <li onclick="switchFocus(this,1);">批量导入淘宝商品</li> -->
                                    <li onclick="batchImportProd(this);">导入本地商品</li>

                                </ul>
							</div>
                        <div id="content_div">
							<div class="search_cat_div">
								<span class="search_cat_span">类目搜索:</span>
								<div class="search_cat_main">
									<input class="search_cat_input" type="text" >
									<div class="search_cat_btns" onclick="searchCategory();">
								      <a><span>快速找到类目</span></a>
								    </div>
								</div>
							</div>
							
							<div class="comm_cat_div">
								<span class="comm_cat_span">您常用的商品分类:</span>
								<div class="comm_cat_main">
									<input class="comm_cat_input" onclick="cci_show();" type="text" readonly="readonly" >
									<div class="comm_category" style="display:none;">
								      <ul>
								      	<c:forEach items="${requestScope.categoryCommList}" var="categoryComm">
								           <li>
								            <a href="javascript:void(0);" onclick="load_commcat(this);">
												<c:forEach items="${categoryComm.treeNodes}" var="node" varStatus="n">
								                    <span nodeId="${node.selfId}">
														<c:choose>
											                <c:when test="${!n.last}">
										                         ${node.nodeName}&gt;&gt;
											                </c:when>
											                <c:when test="${n.last}">
											           	           ${node.nodeName}
											                </c:when>
											              </c:choose>
								                       </span>
												</c:forEach>
											</a>
								            <span><img onclick="del_commcat(this,'${categoryComm.id}');" src="${contextPath}/resources/common/images/dele.gif" width="9" height="9" style="cursor:pointer;"></span> 
								           </li>
								          </c:forEach>
						              </ul>
								    </div>
								</div>
							</div>
							
							<div class="pageContent" style="padding:5px">
								<div class="cate-main">
									<div id="cate-cascading" style="z-index:1;">
										<div id="chooseCategoryPane" class="cc-listwrap">
											<ol class="cc-list" id="cc-list" style="left: 0px;height:270px;">
												<li class="cc-list-item" tabindex="-1" id="category_${level}">
													<div class="cc-cbox-filter" >
														<label for="cc-cbox-filter_${level}" >输入商品分类</label>
														<input id="cc-cbox-filter_${level}" class="lebelinput" name="cc-cbox-filter1" style="width: 192px;">
													</div>
													<div class="cc-tree">
														<ul  class="cc-tree-cont">
															<li class="cc-tree-group  cc-tree-expanded">
															   <ul class="cc-tree-gcont" id="sortList" name="sortList">
																	<c:forEach items="${requestScope.categoryList}" var="category" >
																		<li  id="${category.id}" class='cc-tree-item <c:if test="${fn:length(category.childrenList)>0}">cc-hasChild-item</c:if>'>${category.name}</li>
																	</c:forEach>
																</ul>
															</li>
														</ul>
													</div>
												</li>
		
		<%--										<li id="category2" class="cc-list-item" tabindex="-1"></li>--%>
		<%--										<li id="category3" class="cc-list-item" tabindex="-1"></li>--%>
		<%--										<li id="category4" class="cc-list-item" tabindex="-1"></li>--%>
											</ol>
										</div>
										<div id="searchCategoryPane" style="display:none;">
											
										</div>
									</div>
									<div class="cate-path">
										<dl>
											<div class="clearfix">
												<dt>您当前选择的是：</dt>
												<dd>
													<ol id="selected-category" class="category-path" style="float:left;">
													</ol>
													<ol id="add-comm-category" style="display:none;float:left;margin-left:10px;">
														<a onclick="add_commcat();" style="color: #e5004f;cursor: pointer;text-decoration: none;">
														【添加到常用分类】
														</a>
													</ol>
												</dd>
											</div>
										</dl>
									</div>
									<form:form  name="mainform" action="${contextPath}/s/publishProd" method="post" id="mainform">
									<input type="hidden" id="categoryId" name="categoryId" />
									<input type="hidden" id="prodId" name="prodId" value="${prodId}"/>
									<input type="hidden" name="impId" value="${impId }" />
										<fieldset>
											<div class="cateBottom" style="padding-bottom: 10px;">
												<span class="cateBtn catePublish">
													<!-- <input type="submit" value="我已阅读以下规则，现在发布"/> -->
													<button type="button" style="padding-right: 0;">我已阅读以下规则，现在发布</button>
												</span> 
											</div>
										</fieldset>
									</form:form>
		
									<div class="agreement"  style="height: 180px; margin-top: 10px;background: #fff;">
										<div class="notice">
											<h5>发布须知：</h5>
											禁止发布侵犯他人知识产权的商品，请确认商品符合知识产权保护的规定
										</div>
										<h5 align="center">规则</h5>
												用户应遵守国家法律、行政法规、部门规章等规范性文件。对任何涉嫌违反国家法律、行政法规、部门规章等规范性文件的行为，
												本平台有权酌情处理。但平台对用户的处理不免除其应尽的法律责任。
												用户在平台的任何行为，应同时遵守与平台及其关联公司所签订的各项协议。 平台有权随时变更本规则并在网站上予以公告。
												若用户不同意相关变更，应立即停止使用平台的相关服务或产品。平台有权对用户行为及应适用的规则进行单方认定，并据此处理。
									</div>
								</div>
							</div>
                        </div>
					</div>
                    <div id="batch_import_div" class="batchImporDiv" style="display:none">
                        <div class="order_progress">
                            <div class="clearfix order_progress_con">
                                <ul>
                                    <li class="check">
                                        <span style="background-color: #e5004f;color: #ebebeb">1<i></i></span>
                                        <p>导入Excel文件</p>
                                    </li>
                                    <li style="">
                                        <span>2<i></i></span>
                                        <p>完善商品信息</p>
                                    </li>
                                </ul>
                                <div class="line"></div>
                            </div>
                        </div>
                        <div id="importLocalProdDiv" style="display: none;">
                            <form id="importForm" enctype="multipart/form-data">
                                <div class="filebox" align="center" style="margin-top: 20px">
                                    上传商品数据:
                                    <input type="text" name="copyFile" id = "textbox1" class="textbox" hidden="hidden" readonly="readonly"/>
                                    <input type="file" id="uploadFile" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" name="uploadFile" onchange="getFile(this,'copyFile')" />
                                    <a class="btn-r" href="javascript:void(0)"; onclick="importProd(0)">确认导入</a>
                                </div>
                            </form>
                            <div align="center" style="margin-top: 20px;color: #808080">
                                请选择xlsx或xls格式文件,若无已导出的商品文件,请下载
                                <a style="color: #e5004f" href="javascript:void(0)" onclick="downLoadExcel()">空白excel文件</a>
                                和
                                <a style="color: #e5004f" href="javascript:void(0)" onclick="downLoadCateGoryExcel(0)">平台类目文件</a>
                                和
                                <a style="color: #e5004f" href="javascript:void(0)" onclick="downLoadCateGoryExcel(1)">店铺类目文件</a>
                            </div>
                        </div>
                        <div id="importTenderProdDiv" style="display: none;">
                            <form id="importTenderProdForm" enctype="multipart/form-data">
                                <div class="filebox" align="center" style="margin-top: 20px">
                                    上传商品数据:
                                    <input type="text" name="copyTenderFile" id = "textbox2"  class="textbox" hidden="hidden" readonly="readonly"/>
                                    <input type="file" id="uploadTenderFile" name="uploadFile" onchange="getFile(this,'copyTenderFile')" />
                                    <a class="btn-r" href="javascript:void(0)"; onclick="importProd(1)">确认导入</a>
                                </div>
                            </form>
                            <div align="center" style="margin-top: 20px;color: #808080">
                                请选择xlsx或xls格式文件,若无已导出的商品文件,请下载
                                <select id="tenderer">
                                    <option value="">-请选择-</option>
                                    <c:if test="${not empty tendererList}">
                                        <c:forEach items="${tendererList}" var="mainBody" varStatus="mainBodyStatus">
                                            <option value="${mainBody.tenderMainBodyId}">${mainBody.tenderMainbodyName}</option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                                <a style="color: #e5004f" href="javascript:void(0)" onclick="downLoadTenderProdExcel()">客户招标商品</a>
                                和
                                <a style="color: #e5004f" href="javascript:void(0)" onclick="downLoadCateGoryExcel(1)">店铺类目文件</a>
                            </div>
                        </div>
                        <div style="border: 0px;margin-top: 60px;padding-left: 40px;color: #808080">
                            温馨提示:<br>
							1、建议商品条数<=500,文件大小<=10M,数据量过多将可能影响导入时的性能;<br>
                            2、带"*"为必填项,红色字段不允许修改;<br>
							3、sku是否上架:默认下架;<br>
							4、店铺分类请参考店铺分类文件,填写相应的店铺分类,如果没有对应的店铺分类,则会创建相对应的店铺分类;<br>
							5、系统会按照填写的规格生成未补全的单品,默认下架状态;<br>
                            6、请严格按照要求填写;
                        </div>
                        <div id="errorProdFormDiv" style="display: none;">
                            <form:form action="${contextPath}/s/prodExecl/downLoadImportErrorProdList" method="post" id="errorProdForm" enctype="multipart/form-data">
                                <input type="text" name="listJson" id = "listJson" value="" class="textbox" hidden="hidden" readonly="readonly"/>
                            </form:form>
                        </div>
                        <div id="errorTenderProdFormDiv" style="display: none;">
                            <form:form action="${contextPath}/s/prodExecl/downLoadImportErrorTenderProdList" method="post" id="errorTenderProdForm" enctype="multipart/form-data">
                                <input type="text" name="listJson" id = "listTenderJson" value="" class="textbox" hidden="hidden" readonly="readonly"/>
                            </form:form>
                        </div>
                    </div>
                </div>
				</div>
			</div>
			</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
    <script type="text/javascript" src="<ls:templateResource item='/resources/plugins/showLoading/js/jquery.showLoading.min.js'/>"></script>
</body>
</html>
	<script type="text/javascript">
	  
		//$(document).ready(function(e) {
				//publish.doExecute();
		//});
		//标记,0代表是普通的发布商品,1代表批量导入淘宝商品
		var flag = 0;
		
		$(document).ready(function(e) {
			var level = '${level}';
			
			$("#category_"+level).find("li.cc-tree-item").on('click',function() {
					$this = $(this);
					var cateId = $this.attr("id");
					$("#categoryId").val(cateId);
					
					$("#cc-list li[class='cc-list-item']").not(":eq(0)").remove();
					
					if($this.hasClass("cc-hasChild-item")){
						var targetUrl = "${contextPath}/s/publish2/" +cateId ;
						retrieveNext(targetUrl);
					}
					$("li.cc-selected").removeClass("cc-selected");
					$this.addClass("cc-selected");

					selectedCategory();
		    });

			    $('.lebelinput').on('focus', function(){
					$(this).prev("label").hide();
				});
			    
				$('.lebelinput').on('blur', function(){
				   $this = $(this);
					if( $this.val()=='')
					{
						 $this.prev("label").show();
					}
				});
						
				$('#cc-cbox-filter_'+level).off('keypress').on('keypress', function(e){
				   var valueText = $(this).val();
					 if(e.which == 13) {
		               $.post("${contextPath}/s/loadCategory", 
					     {
				            "cateName": valueText,
				            "level": level
					     },
					    function(retData) {
				        	  $("#category_"+level).find("li.cc-tree-expanded ul").html(retData);
				        	  $("#cc-list li[class='cc-list-item']").not(":eq(0)").remove();
						});
				    }
				});
				
				$(".comm_category").mouseleave(function(){
					$(".comm_category").hide();
				});
				
				$(".cateBottom").on('click', '.cateCanBtn', function(e){
					publishProd();
				});
				
				//编辑分类时回显
				var treeNodeStr  = '${treeNodeStr}';
				if(treeNodeStr!=undefined && treeNodeStr!=""){
					setTimeout("echoCategory('"+treeNodeStr+"')",100);
				}
			});
		
		
		//获取下一页
		function retrieveNext(targetUrl){
				jQuery.ajax({
						url: targetUrl, 
						type:'post', 
						async : false, //默认为true 异步   
						dataType : 'html', 
						error:function(data){
						  layer.alert("系统操作异常！");   
						},   
						success:function(data){
							$("#cc-list").append(data);
						}   
					});  
			}
		
		//选中 分类拼接
		function selectedCategory(){
			$("#selected-category").empty();
			$(".catePublish").removeClass("cateCanBtn");
			var lastSelectedLi;
			$("#cc-list li[class='cc-list-item']").each(function(index,element){
				var selectedLi = $(element).find("ul li[class*='cc-selected']");
			    var selectedText = $(selectedLi).text();
			    var selectedLiStr = "";
			    if(!isBlank(selectedText)){
					lastSelectedLi = selectedLi;
			    	if(index == 0){
			    		selectedLiStr = "<li>" + selectedText + "</li>";
				    }else{
				    	selectedLiStr = "<li> > " + selectedText + "</li>";
				    }
				    $("#selected-category").append(selectedLiStr);
			    }
			 });
			if($(lastSelectedLi).hasClass("cc-hasChild-item")){
				$("#add-comm-category").hide();
			}else{
				$("#add-comm-category").show();
				$(".catePublish").addClass("cateCanBtn");
			}
		}
		
		
		//方法，判断是否为空
		function isBlank(_value){
			if(_value==null || _value=="" || _value==undefined){
				return true;
			}
			return false;
		}
        function downLoadExcel(){
            window.location.href = contextPath+"/s/prodExecl/exportLocalProduct";
        }
//
        function downLoadCateGoryExcel(type){
            window.location.href=contextPath+"/s/cateGoryExecl/exportCateGory/"+type;
        }
        /**
         获取文件名
         */
        function getFile(obj,inputName){
            var fileName = $(obj).val();
            var index = fileName.lastIndexOf(".");
            var suffix = fileName.substring(index).toLowerCase();
            if(!isBlank(fileName) && (".xlsx"==suffix||".xls"==suffix)){
                $("input[name='"+inputName+"']").val(fileName);
            }
            else{
                $("input[name='"+inputName+"']").val(fileName);
                layer.msg("文件格式不对，请选择xls或xlsx格式！",{icon:0});
            }
        }

        /**
         上传Excel(招标商品)type 0:普通商品 1:招标商品,
         */
        function importProd(type){
            var fileName = $("#textbox2").val();
            var url = "/s/product/importTenderProdByExcel";
            var formData = new FormData($("#importTenderProdForm")[0]);
            if(type == 0){
                fileName = $("#textbox1").val();
                url = "/s/product/importProdByExcel";
                formData = new FormData($("#importForm")[0]);
            }
            var index = fileName.lastIndexOf(".");
            var suffix = fileName.substring(index).toLowerCase();
            if(isBlank(fileName)){
                layer.msg("文件为空",{icon:0});
                return false;
            }

            if(suffix!=".xlsx" &&  suffix!=".xls"){
                layer.msg("文件格式不对，请选择xls或xlsx格式！",{icon:0});
                return false;
            }
            $.ajax({
                url: url,
                timeout : 300000,
                type: 'post',
                data:formData,
                processData: false,
                contentType: false,
                dataType: 'json',
                beforeSend: function(xhr){
                    $("body").showLoading();
                },
                complete: function(xhr,status){
                    $("body").hideLoading();
                },
                success: function(result){
                    if(result.status=="-1"){
                        layer.msg(result.msg,{icon:2});
                    }else if(result.status=="0"){
                        var msg = result.msg+",成功导入"+result.succCount+"条,失败"+result.errorCount+"条";
                        partImportSuc(msg,result.errorList,type);
                    }else if(result.status=="1"){
                        var msg = result.msg+",成功导入"+result.succCount+"条,失败"+result.errorCount+"条";
                        importSuc(msg);
                    }else{
                        layer.msg("程序异常,请联系管理员",{icon:2});
                    }
                }
            });
        }
        /**导入成功*/
        function importSuc(msg){
            layer.confirm(msg, {
                icon: 1
                ,btn: ['前往商品列表查看','继续导入'] //按钮
            }, function(){
                window.location.href=contextPath+"/s/prodInStoreHouse";
            });
        }

		/** 部分导入成功*/
		function partImportSuc(msg,errorList,type){
			layer.confirm(msg, {
				icon: 1
				,btn: ['下载导入失败的数据','继续导入'] //按钮
			}, function(){
				if(type == 1){
					downLoadImportErrorTenderProdList(errorList);
				}else{
					downLoadImportErrorProdList(errorList)
				}
			});
		}


		/** 下载导入失败的数据(普通商品) */
        function downLoadImportErrorProdList(errorList){
            var listJson = JSON.stringify(errorList);
            $("#listJson").val(listJson);
            $("#errorProdForm").submit();
        }

		/** 下载导入失败的数据(中标商品) */
		function downLoadImportErrorTenderProdList(errorList){
			var listJson = JSON.stringify(errorList);
			$("#listTenderJson").val(listJson);
			$("#errorTenderProdForm").submit();
		}


		//提交
		function publishProd(){
			var categoryId = $("#categoryId").val();
			if(isBlank(categoryId)){
				layer.msg("请选定分类",{icon: 0});
			}else{
				var prodId = $("#prodId").val();
				var isSimilar = '${isSimilar}';
				if(flag == 1){
					$("#mainform").attr("action","${contextPath}/s/taobaoImport/page");
				}else if(isSimilar!=undefined && isSimilar!=""){
					$("#mainform").attr("action","${contextPath}/s/similarProd/"+prodId);
				}else if(prodId!=undefined && prodId!="" && prodId!=0){
					$("#mainform").attr("action","${contextPath}/s/updateProd/"+prodId);
				}else if(prodId==0){
					self.parent.window.location="${contextPath}/admin/product/load?categoryId="+categoryId;
				}
				$("#mainform").submit();
			}
		}
		
		function cci_show(){
			$(".comm_category").show();
		}
		
		//编辑分类回显
		function echoCategory(treeNodeStr){
			var treeNodeArr = treeNodeStr.split(",");
			for(var i=0;i<treeNodeArr.length;i++){
				var nodeId = treeNodeArr[i];
				var catLiList = $("#category_"+(i+1)+" li").get();
				for(var j=0;j<catLiList.length;j++){
					if($(catLiList[j]).attr("id")==nodeId){
						$(catLiList[j]).click();
					}
				}
			}
		}
		
		//应用常用分类
		function load_commcat(obj){
			$(".comm_category").hide();
			var spanList  = $(obj).find("span").get();
			var spanString = "";
			for(var i=0;i<spanList.length;i++){
				var nodeId = $(spanList[i]).attr("nodeId");
				var catLiList = $("#category_"+(i+1)+" li").get();
				for(var j=0;j<catLiList.length;j++){
					if($(catLiList[j]).attr("id")==nodeId){
						$(catLiList[j]).click();
					}
				}
				spanString = spanString+$.trim($(spanList[i]).text());
			}
			$(".comm_cat_input").val(spanString); 
		}
		
		//删除常用分类
		function del_commcat(obj,id){
			$(obj).parent().parent().remove();
			jQuery.ajax({
				url: "${contextPath}/s/delCategoryComm/"+id, 
				type : "POST", 
				async : true, //默认为true 异步   
				dataType : "JSON", 
				error:function(data){
				},   
				success:function(data){
					$(".comm_cat_input").val(""); 
				}   
			});  
		}
		
		//添加常用分类
		function add_commcat(){
			var categoryId = $("#categoryId").val();
			jQuery.ajax({
				url: "${contextPath}/s/saveCategoryComm/"+categoryId,
				type : "POST",
				async : true, //默认为true 异步   
				dataType : "JSON", 
				success:function(data){
					if(data=="OK"){
						layer.msg('添加成功!',{icon:1,time:800},function(){
							loadCategoryComm();
						});
					}else if(data=="EXIST"){
						layer.msg('该常用分类已存在!',{icon:0});
					}
				}   
			});  
		}
		
		function loadCategoryComm(){
			jQuery.ajax({
				url: "${contextPath}/s/loadCategoryComm", 
				type: "GET", 
				async : true, //默认为true 异步   
				cache : false, //不缓存
				dataType : "HTML", 
				success:function(data){
					$(".comm_category").html(data);
				}   
			});  
		}
		
		//类似商品导入
		function importSimilarProd(){
			layer.open({
			  title :"选择要导入的商品",
			  type: 2, 
			  content: '${contextPath}/s/loadProdListPage', //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
			  area: ['700px', '560px']
			}); 
		}
		
		//搜索类目
		function searchCategory(){
			var text = $(".search_cat_input").val().trim();
			if(text!=""){
				jQuery.ajax({
					url: "${contextPath}/s/searchCategory", 
					type:'post', 
					data:{"categoryName":text},
					async : true, //默认为true 异步   
					dataType : 'html', 
					error:function(data){
						layer.alert("网络异常,请稍后重试！",{icon:2});
					},   
					success:function(data){
						$("#searchCategoryPane").html(data);
						$("#searchCategoryPane").show();
						$("#chooseCategoryPane").hide();
						$("#searchCategoryPane .search_cat_content li:first-child").click();
					}   
				});  
			}
		}
		//关闭，返回类目
		function closeSearchCat(){
			$("#searchCategoryPane").hide();
			$("#chooseCategoryPane").show();
			$("#searchCategoryPane").html("");
			$("#category_1 li.cc-tree-item:first-child").click();
		}
		
		function selSearchCategory(id,name,obj){
			$(obj).addClass("selected").siblings().removeClass("selected");
			$("#selected-category").html(name);
			$("#categoryId").val(id);
			$("#add-comm-category").show();
			$(".catePublish").addClass("cateCanBtn");
		}
		
		$(document).ready(function() {
			userCenter.changeSubTab("publish");
		});
	
		//切换焦点,改变标记
		function switchFocus(ele,newFlag){
            $("#content_div").show();
            $("#layout ul li[class='on']").removeClass("on");
            $("#batch_import_div").css("display","none");
            $(ele).addClass("on");
            flag = newFlag;
		}

        //批量导入本地商品
        function batchImportProd(ele){
            $("#importTenderProdDiv").css("display","none");
            $("#batch_import_div").css("display","block");
            $("#importLocalProdDiv").css("display","block");
            $("#layout ul li[class='on']").removeClass("on");
            $(ele).addClass("on");
            $("#content_div").hide();
        }


    </script>
