<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
 <!-- 支付方式 -->
	          
	 <ls:plugin pluginId='predeposit'>
					<div id="pre-deposit" style="padding: 20px 20px 26px 20px;">
			            <h3>使用余额支付</h3>
			            <div>
			              <ul>
			                <li><input type="checkbox"  name="pd_pay" id="pd_pay_input" value="1" > 
			                   <label style="font-size:14px;"  for="pd_pay_input"><font color="#ff0000">使用预存款支付 （可用余额： ${availablePredeposit}元）</font></label>
			                    <p style="line-height:22px;color:#999; padding-left:25px;">同时勾选时，系统将优先使用预存款支付，不足时扣除预存款，目前还需在线支付 <strong id="api_pay_amount1">${totalAmountFormat}</strong>元。</p>
			                    <p style="color:#999; padding-left:25px;" class="bluelink">余额不足？<a  target="_blank"  href="<ls:url address='/p/predeposit/recharge_add'/>">马上充值</a></p>
			                </li>
			               <%--  <li><input type="checkbox"  name="pd_pay" id="pd_pay_input" value="2" > 
			                   <label style="font-size:14px;"  for="pd_pay_input">使用金币支付 （可用余额： ${coin}元）</label>
			                    <p style="color:#999; padding-left:25px;"> 同时勾选时，系统将优先使用金币支付 ，不足时扣除预存款，目前还需在线支付 <strong id="api_pay_amount2">${totalAmountFormat}</strong>元。</p>
			                </li> --%>
			              </ul>
			            </div>
			            
			             <div class="pay-pas" id="pd_password" style="display: none;">
			                <p style="color: rgb(153, 153, 153); padding: 10px 0px 0px 15px;">请输入支付密码：
			                <input style="padding: 5px 0px; width: 150px;" type="password"  maxlength="35"  id="pay-password" name="password" value="" >
			                <a style="background-color: rgb(229, 0, 79); padding: 5px 10px; margin: 0px 10px; color: rgb(255, 255, 255); border-radius: 3px;"  id="pd_pay_submit"  href="javascript:void(0);">确认支付</a>还未设置密码？
			                <a style="color: rgb(63, 109, 224);" target="_blank" href="<ls:url address='/p/security'/>" >马上设置</a></p>
			              </div>
		          </div>
	</ls:plugin>

    <div class="onl-pay">
            <h3>在线支付</h3>
            <div class="onl-pay-tit">
            	<div class="pay-pla on">平台支付</div>
            </div>
            <div class="onl-pay-con" >
               <c:if test="${payTypesMap.ALP_PC==1}">
	              <div title="支付宝" class="ALI_PAY" payment_code="ALP" >
	                <span class="ALI_PAY"></span>
	                <em></em>
	              </div>
              </c:if>
              
              <c:if test="${payTypesMap.TDP==1}">
				   <div title="财付通支付" class="TEN_PAY" payment_code="TDP" >
		                <span class="TEN_PAY"></span>
		                <em></em>
		              </div>
			   </c:if>
			   
			    
              <c:if test="${payTypesMap.WX_PAY==1}">
				     <div title="微信支付" class="WEIXIN_PAY" payment_code="WX_SACN_PAY"  >
		                <span class="WEIXIN_PAY"></span>
		                <em></em>
		             </div>
			  </c:if>
				        
              <c:if test="${payTypesMap.UNION_PAY==1}">
	              <div title="在线支付" class="UNION_PAY"  payment_code="UNION_PAY" >
	                <span class="UNION_PAY"></span>
	                <em></em>
	              </div>
              </c:if>
              
             <c:if test="${payTypesMap.TONGLIAN_PAY==1}">
                 <div title="通联支付" class="TONGLIAN_PAY"  payment_code="TONGLIAN_PAY" >
                  <span class="TONGLIAN_PAY"></span>
                   <em></em>
                 </div>
             </c:if>
             
              <c:if test="${payTypesMap.SIMULATE==1}">
				  <div title="模拟支付" class="SIMULATE"  payment_code="SIMULATE" >
                  <span class="SIMULATE"></span>
                   <em></em>
                  </div>
		       </c:if>
             
             
        </div>
   </div>
   <div style="display: none;" id="pay_overlay">
        <div class="pop-up-dow" >
            <ul style="text-align: center; margin-top: 27px;">
                <li style="margin-bottom: 20px;">如已经成功支付，请点击<a style="padding:4px;border-radius: 2px;color: #1e9fff;"	href="${contextPath}/p/myorder" >已经完成支付！</a>
<%--                <li>如付款遇到问题，请点击<a style="padding: 4px;border-radius: 2px;color: #1e9fff;" href="javascript:void(0);">支付出现问题！</a>--%>
				<li>如付款遇到问题，请重新支付！</li>
            </ul>
        </div>
 </div>
