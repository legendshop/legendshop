<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>错误页面-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <style type="text/css">
      .seller-selling {
		    float: right;
		    overflow: hidden;
		    width: 966px;
		    margin-top: 20px;
	}
.selling-sea{height: 54px;border: 1px solid #e4e4e4;width: 964px;line-height: 54px;background-color: #f9f9f9;text-align: center;height: 160px;}
    </style>
    
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">错误提示页面</span>
				</p>
			</div>
			
			<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
         

             <div class="seller-selling">
			    	<div class="selling-sea">
						<div class="reg-tips-info" style="color: #CC0000;font-weight: bold;">抱歉，操作失败！</div>
					    <div class="reg-nickname-tips">${messgae}</div>
					    <div><input type="button" onclick="history.go(-1)" value="返回"/></div>
					</div>
					
			    	
			</div>
		  
		</div>
			<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
</body>
</html>
