<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<%@include file='/WEB-INF/pages/common/layui.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>商品评论 - ${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
<style>
	.sold-table th,td {
		text-align: center;
	}
	.sold-table th {
    line-height: 22px;
    font-weight: 400;
    color: #333;
    background: #f9f9f9;
	}
	.fls{
		width:240px;
		margin-left:110px;
	}
	.fls input{
		margin-top:10px;
	}
</style>
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>

		<!----两栏---->
		<div class=" yt-wrap" style="padding-top: 10px;">
			<%@include file='usercenterLeft.jsp'%>

			<!-----------right_con-------------->
			<div class="right_con">
				<div class="o-mt">
					<h2><strong>商品评价</strong></h2>
				</div>
				<div class="pagetab2">
					<ul>
						<li class="on" id="favoriteShopList"><span>商品评价</span></li>
					</ul>
				</div>
				<div class="tb-void">
					<div id="recommend" class="recommend" style="display: block;">
						<table width="100%" cellspacing="0" cellpadding="0" border="0"
							class="buytable mailtab sold-table">
							<tbody>
								<tr >
									<th class="bdl"></th>
									<th>图片</th>
									<th >商品信息</th>
									<th>订单编号</th>
									<th >购买时间</th>
									<th style="border-right: 1px solid #e4e4e4;">评价状态</th>
								</tr>
								<c:choose>
									<c:when test="${empty requestScope.list}">
										<tr>
											<td colspan="20">
												<!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
												<div class="warning-option"><!-- <i></i> --><span style="text-align: center;">没有符合条件的信息</span>
												</div>
											</td>
										</tr>
									</c:when>
									<c:otherwise>
										<c:forEach items="${requestScope.list}" var="productComment" varStatus="status">
											<tr>
												<td class="bdl"></td>
												<td height="65" width="75">
													<a href="${contextPath}/views/${productComment.prodId}" target="_blank"> 
														<img src="<ls:images item='${productComment.pic}' scale='3' />" />
													</a>
												</td>
												<td>
													<a href="${contextPath}/views/${productComment.prodId}" target="_blank"> ${productComment.prodName} </a>
													<p>${productComment.attribute}</p>
												</td>
												<td>${productComment.subNumber}</td>
												<td>
													<span class="ftx-03">
														<fmt:formatDate value="${productComment.buyTime}" type="date" />
													</span>
												</td>
												<td>
													<c:choose>
														<c:when test="${not empty productComment.id}">
															<c:choose>
																<c:when test="${productComment.isAddComm}">
																	<div class="fore4 forem">
																		<a href="${contextPath}/productcomment/productCommentDetail?prodComId=${productComment.id}" target="_blank">已追加评价</a>
																	</div>
																</c:when>
																<c:when test="${productComment.status eq 1}">
																	<div class="fore4 forem">
																		<a href="#none" class="ncbtn comments-btn">追加评价</a>
																		<div class="fore4 forem">
																			<a href="${contextPath}/productcomment/productCommentDetail?prodComId=${productComment.id}" class="ncbtn" target="_blank">查看评价</a>
																		</div>
																	</div>
																</c:when>
																<c:when test="${productComment.status eq -1}">
																	已评价<!-- 审核不通过 -->
																</c:when>
																<c:otherwise>已评论</c:otherwise>
															</c:choose>
														</c:when>
														<c:otherwise>
															<div class="fore4 forem">
																<a href="#none" class="ncbtn comments-btn">发表评价</a>
															</div>
														</c:otherwise>
													</c:choose>
												</td>
											</tr>

											<c:choose>
												<c:when test="${empty productComment.id }">
													<tr class="prompt01 hide">
														<td colspan="6">
															<form action="${contextPath }/p/comments/save" id="faddComm-${productComment.prodId}-${productComment.subItemId}" class="comments-form" enctype="multipart/form-data" method="POST">
																<input name="prodId" type="hidden" value="${productComment.prodId }" /> 
																<input name="subItemId" type="hidden" value="${productComment.subItemId }" />
																<div class="form">
																	<div class="item">
																		<span class="label">宝贝评分：</span>
																		<div class="fl">
																			<div id="prodScore_${status.index}"></div>
																			<input id="prodScore" name="prodScore" value="0" type="hidden" />
																		</div>
																	</div>
																	<div class="clr"></div>

																	<div class="item">
																		<span class="label">店铺服务：</span>
																		<div class="fl">
																			<div id="shopScore_${status.index}"></div>
																			<input id="shopScore" name="shopScore" value="0" type="hidden" />
																		</div>
																	</div>
																	<div class="clr"></div>

																	<div class="item">
																		<span class="label">物流服务：</span>
																		<div class="fl">
																			<div id="logisticsScore_${status.index}"></div>
																			<input id="logisticsScore" name="logisticsScore" value="0" type="hidden" />
																		</div>
																	</div>
																	<div class="clr"></div>

																	<div class="item">
																		<span class="label">心得：</span>
																		<div class="fl">
																			<textarea  name="content" class="area"></textarea>
																		</div>
																		<div class="clr"></div>
																	</div>
																	<div class="item">
																		<span class="label">图片：</span>
																		<div class="fls">
																			<input class="photoFile" type="file" name="photos" />
																			<input class="photoFile" type="file" name="photos" />
																			<input class="photoFile" type="file" name="photos" />
																			<input class="photoFile" type="file" name="photos" />
																			<input class="photoFile" type="file" name="photos" />
																		</div>
																	</div>
																	<div class="item item02">
																		<span class="label">&nbsp;</span>
																		<div class="fl" style="margin-top: 10px;">
																			<a href="#none" class="btn-r btn-6 ncbtn commit-comment-btn"><s></s><span>提交评价</span></a>
																			<label class="checkbox-wrapper" style="float:left;padding-bottom:10px;">
																				<span class="checkbox-item">
																					<input name="isAnonymous" type="checkbox" id="isAnonymous_${productComment.subItemId }" class="checkbox-input" onclick="changeChecked(this);" />
																					<span class="checkbox-inner"></span>
																				</span>
																				<span>匿名评价</span>
																		   	</label>
																			<!-- <label class="anon-l"> 
																				<input name="isAnonymous" type="checkbox">匿名评价 
																			</label> -->
																		</div>
																		<div class="clr"></div>
																	</div>
																</div>
																<input type="hidden" name="test">
															</form>
														</td>
													</tr>
												</c:when>
												<c:otherwise>
													<!-- 如果未追加评论 -->
													<c:if test="${!productComment.isAddComm }">
														<tr class="prompt01 hide">
															<td colspan="4">
																<form action="${contextPath }/p/comments/addComm" id="addComm-${productComment.id}" class="add-comments-form" enctype="multipart/form-data" method="POST">
																	<input id="prodCommId" name="prodCommId" type="hidden" value="${productComment.id }" />
																	<div class="form">
																		<div class="item">
																			<span class="label">心得：</span>
																			<div class="fl">
																				<textarea id="content" name="content" class="area"></textarea>
																			</div>
																			<div class="clr"></div>
																		</div>
																		<div class="item">
																			<span class="label">图片：</span>
																			<div class="fls">
																				<input class="photoFile" type="file" name="photos" />
																				<input class="photoFile" type="file" name="photos" />
																				<input class="photoFile" type="file" name="photos" />
																				<input class="photoFile" type="file" name="photos" />
																				<input class="photoFile" type="file" name="photos" />
																			</div>
																		</div>
																		<div class="item item02">
																			<span class="label">&nbsp;</span>
																			<div class="fl" style="margin-top: 10px;">
																				<a href="#none" class="btn-r btn-6 ncbtn commit-add-comment-btn"><s></s><span>提交评价</span></a>
																			</div>
																			<div class="clr"></div>
																		</div>
																	</div>
																</form>
															</td>
														</tr>
													</c:if>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>
					<c:if test="${not empty requestScope.list }">
						<div style="margin-top: 10px;" class="page clearfix">
							<div class="p-wrap">
								<span class="p-num"> <ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="simple" />
								</span>
							</div>
						</div>
					</c:if>
				</div>
			</div>
			<!-----------right_con end-------------->

			<div class="clear"></div>
		</div>
		<!----两栏end---->

		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>

	<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/commonUtils.js'/>"></script>
	<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.img.preview.js'/>"></script>
	<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/ajaxfileupload.js'/>"></script>
	<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/prodcomment.js'/>"></script>
	<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/compress.js'/>"></script>
	<script type="text/javascript">
 		var contextPath = "${contextPath}";
 		var urls = {
 		 		saveComment: contextPath + "/p/comments/save",
 		 		saveAddComment: contextPath + "/p/comments/addComm"
 		 	};
 		
 		/*单个复选框点击操作*/
 		function changeChecked(ele){
 			var id = ele.id;
 			var is_checked = $("#"+id).attr("checked");
 			  if(!is_checked){
 				  $("#"+id).removeAttr("checked");
 				  $("#"+id).parent().parent().attr("class","checkbox-wrapper");
 			  }else{
 				  $("#"+id).attr("checked","checked");
 				  $("#"+id).parent().parent().attr("class","checkbox-wrapper checkbox-wrapper-checked");
 			  }
 		}
	</script>
</body>
</html>