<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>满折促销-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet">
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style.css'/>" rel="stylesheet">
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/active/promotionAddProds.css'/>" rel="stylesheet">
</head>
<body class="graybody">
<div class="check-box" style="padding-bottom: 10px">
	<div class="check-box">
		<form action="/s/addShopMarketingProds" id="form1">
			<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />

			<div class="check-search">
				<input class="search-shop"  type="text" name="prodName" id="prodName" maxlength="20" value="${prodName}" size="20" placeholder="请输入商品名称" />
				<input  class="red-btn" type="submit" value="搜索" />
			</div>
		</form>
		<div style="position: relative;display: block;">
			<table border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto;">
				<tbody>
				<tr style="background: #f9f9f9;">
					<td width="35px">
						<label class="checkbox-wrapper">
								<span class="checkbox-item">
									<input type="checkbox" id="checkbox" class="checkbox-input selectAll"/>
									<span class="checkbox-inner"></span>
								</span>
						</label>
					</td>
					<td width="70px">图片</td>
					<td width="170px">商品名称</td>
					<td width="100px">规格</td>
					<td width="100px">可销售库存</td>
				</tr>
				<c:if test="${empty requestScope.list}">
					<tr><td colspan="5" style="text-align: center;">没有找到符合条件的商品</td></tr>
				</c:if>
				<c:forEach items="${requestScope.list}" var="product">
					<tr class="first">
						<td class="prodCheck">
							<label class="checkbox-wrapper">
								<span class="checkbox-item">
									<input type="checkbox" name="array" class="checkbox-input selectOne prodId" value="${product.prodId}" arg="${product.name}" onclick="selectOne(this);"/>
									<span class="checkbox-inner"></span>
								</span>
							</label>
						</td>
						<td><img src="<ls:images item='${product.pic}' scale='3' />" ></td>
						<td><span class="name-text">${product.name}</span></td>
						<td class="chooseSku"><span class="skuNum">选择规格</span> <span class="addSku" style="display:none;color:red;font-size:18px">+</span></td>
						<td class="stocks">${product.stocks}</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
			<div class="check-arr cur" style="display: none;">
				<table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin: 0px;border: 0px solid #ddd;background:rgba(255,255,255,1)">
					<tbody class="skuList">
					<tr style="background: #FEF2F6; height: 40px">
						<td width="10%">
							<label class="checkbox-wrapper">
								<span class="checkbox-item">
									<input type="checkbox" id="checkbox" class="checkbox-input skuAll"/>
									<span class="checkbox-inner"></span>
								</span>
							</label>
						</td>
						<td width="50%">规格</td>
						<td width="25%">价格</td>
						<td width="25%">库存</td>
					</tr>
					</tbody>
				</table>
				<div class="bto-btn">
					<div class="red-btn skuSubmit">提交</div>
					<div class="red-btn close" style="background: #F9F9F9;color: #000000;border:1px solid rgba(221,221,221,1)";>返回</div>
				</div>
			</div>
		</div>
		<div class="alert-btom">
			<div class="btom-page">
				<div style="margin-top:10px;" class="page clearfix">
					<div class="p-wrap">
						<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
					</div>
				</div>
			</div>
			<div class="btom-right">
				<div>已选商品(<span class="prodNum"></span>)</div>
				<div class="red-btn" onclick="submit();">确定</div>
				<div class="red-btn cancel" onclick="cancel();">取消</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript" src="/resources/templets/js/shopAddMansongProd.js"></script>
</html>