<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
 <div class="right_con">
        <div class="pagetab2">
            <ul>
              <li <c:if test="${searchParams.timeTab eq 1 || empty searchParams.timeTab}">class="on"</c:if> tab="1"><span>正在进行</span></li>
              <li <c:if test="${searchParams.timeTab eq 0}">class="on"</c:if> tab="0"><span>即将开始</span></li>
              <li <c:if test="${searchParams.timeTab eq 2}">class="on"</c:if> tab="2"><span>已经结束</span></li>
            </ul>
        </div>
        <nav id="main-nav" class="sort-bar">
          <div class="nch-sortbar-array">
            排序：
           <ul>
              <li id="default" ${empty param.orders?'class="selected"':''}>
                <a title="默认排序" href="javascript:void(0);">默认</a>
              </li>
              <li id="start_time" ${searchParams.orders== 'start_time,desc' ||  searchParams.orders==
				'start_time,asc'?'class="selected"':''}>
                 <a title="点击按时间从高到低排序" href="javascript:void(0);"
					 <c:choose><c:when test="${searchParams.orders=='start_time,desc'}">class='desc'</c:when><c:when test="${searchParams.orders=='start_time,asc'}">class='asc'</c:when></c:choose> >最新<i></i></a>
              </li>
              <li id="group_price"  ${searchParams.orders== 'group_price,desc' ||  searchParams.orders==
				'group_price,asc'?'class="selected"':''}>
                <a title="点击按价格从高到低排序" href="javascript:void(0);"
                <c:choose><c:when test="${searchParamsorders=='group_price,desc'}">class='desc'</c:when><c:when test="${searchParams.orders=='group_price,asc'}">class='asc'</c:when></c:choose> >价格<i></i></a>
              </li>
             <!--  <li id="exchangeIntegral">
                 <a title="点击按销量从高到低排序" href="javascript:void(0);">销量<i></i></a>
              </li> -->
            </ul>
          </div>
       </nav>
        <div id="recommend" class="m10 recommend" style="display: block;">
        <div id="group" class="tabcon groupon">
                   
                   <div class="suits" style="width:100%;"> 
                     <ul class="list-h">
                     
                     <c:choose>
                     	<c:when test="${not empty requestScope.list}">
                     		<c:forEach items="${requestScope.list}" var="group">
	                     		 <li class="gl-item">
	                              <div class="gl-i-wrap j-sku-item">
	                                <div class="p-img">
	                                   <a style="position: relative;" href="<ls:url address='/group/view/${group.id}'/>" target="_blank"><img alt="" src="<ls:images item='${group.groupImage}' scale='4'/>" width="180" height="210"></a>
	                                   <div class="picon pi8"><b></b></div>
	                                </div>            
	                                <div class="p-name">
	                                  <a title="${group.groupName}" href="<ls:url address='/group/view/${group.id}'/>"" target="_blank">${group.groupName}</a>
	                                </div>
	                                <div class="p-price">
	                                  <small>¥</small><strong class="J_price">${group.groupPrice}</strong><span><b>${group.buyerCount}</b>件已购买</span>
	                                </div>
	                           </li>
                           </c:forEach>
                     	</c:when>
                     	 <c:otherwise>
                    	 	    	 <div class="selection_box search_noresult">
										<p class="search_nr_img"><img src="<ls:templateResource item='/resources/templets/images/not_result.png'/>"></p> 
										<p style="text-align: center;font-size: 16px;margin-top: 10px;">没有找到相关的商品！</p>
									</div>
                     	 </c:otherwise>
                     </c:choose>
                         </ul>
                  </div>                   
                   <div class="clear"></div>  
          </div>              
     </div>
    <div class="clear"></div>
            <div style="margin-top:10px;" class="page clearfix">
	     		<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/>
 			</div>
     </div>
     	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/groupProdList.js'/>"></script>
     	<script type='text/javascript'>
     		var contextPath="${contextPath}";
     		$(document).ready(function() {
     			data = {
   					curPageNO : "${_page}",
   					firstCid : "<c:out value='${searchParams.firstCid}'/>",
   					twoCid:"<c:out value='${searchParams.twoCid}'/>",
   					thirdCid:"<c:out value='${searchParams.thirdCid}'/>",
   					orders:"<c:out value='${searchParams.orders}'/>",
   					timeTab:"<c:out value='${empty searchParams.timeTab?1:searchParams.timeTab}'/>",
     			}
     		});
		</script>