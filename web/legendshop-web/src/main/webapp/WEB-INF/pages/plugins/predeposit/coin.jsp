<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>我的金币 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/pdCashManager${_style_}.css'/>" rel="stylesheet" />
	 
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
     <%@ include file="/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp" %>
    <!-----------right_con-------------->
     <div class="right_con">
<div>
	<div class="o-mt">
		<h2>账户信息</h2>
	</div>
	<div class="pagetab2">
		<ul>
			<li class="on"><span>账户余额</span></li>
		</ul>
	</div>

	<div>
		<div class="alert">
			<span class="mr30">可用余额：<strong class="mr5 red"
				style="font-size: 18px;"><fmt:formatNumber value="${coinVo.coin}" pattern="#,#00.00#"/></strong>元</span>
		</div>
		
		<div id="recommend" class="recommend" style="display: block;">
		<table class="ncm-default-table" cellpadding="0" cellspacing="0" style="border: 0px;">
			<thead>
				<tr>
					<th width="183" >创建时间</th>
					<th width="100" >收入(元)</th>
					<th width="100" >支出(元)</th>
					<th width="187" >流水号</th>
					<th width="250" >说明</th>
				</tr>
			</thead>
			<tbody>
				   <c:forEach items="${coinVo.coinLog.resultList}" var="pred" varStatus="status">
				      <tr>
				      <td><fmt:formatDate value="${pred.addTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				      <td>
				         <c:choose>
							<c:when test="${pred.amount>0}">
							   +<fmt:formatNumber value="${pred.amount}" pattern="#,#00.00#"/>
							</c:when>
							<c:otherwise>
							   0.00
							</c:otherwise>
						 </c:choose>
				      </td>
				      <td>
				       <c:choose>
							<c:when test="${pred.amount<0}">
							   <fmt:formatNumber value="${pred.amount}" pattern="#,#00.00#"/>
							</c:when>
							<c:otherwise>
							   0.00
						</c:otherwise>
					  </c:choose>
				      </td>
				      <td>
				        ${pred.sn}
				      </td>
				      <td>
				      	<c:if test="${pred.logType eq 'recharge'}">
				      		充值，${pred.logDesc}
				      	</c:if>
				         <c:if test="${pred.logType eq 'order_finally'}">
				      		下单，${pred.logDesc}
				      	</c:if>
				      </td>
				      </tr>
				   </c:forEach>
				
			</tbody>
		</table>
	
	   <c:if test="${empty coinVo.coinLog.resultList}">
	   		<!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
	        <div class="warning-option"><!-- <i></i> --><span>没有符合条件的信息</span></div>
	   </c:if>
	   </div>
	 
	</div>
	<!--page-->
		<div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
				            		 <c:if test="${coinVo.coinLog.toolBar!=null}">
											<span class="p-num">
												<c:out value="${coinVo.coinLog.toolBar}" escapeXml="${coinVo.coinLog.toolBar}"></c:out>
											</span>
										</c:if>
				     			 </div>
		  				</div>  
		<div class="clear"></div>
</div>
</div>
    <!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
  <script type="text/javascript">
	 function pager(curPageNO){
	     window.location.href="${contextPath}/p/predeposit/coin?curPageNO="+curPageNO;
     }
	$(document).ready(function() {
		userCenter.changeSubTab("myCoin");
  	});
</script>  
</body>
</html>
			