<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
<html>
<head>
  <title>错误页面</title>
  <meta http-equiv="refresh" content="3;URL=${contextPath}/home">
</head>
<body class="graybody">
<div id="doc">
       <%@include file='home/top.jsp'%>
          <div id="bd">
		      <!--order content -->
		         <div style="margin-top:20px;" class="yt-wrap ordermain">
		              <div class="clearfix box404">
		              <span><img src="${contextPath}/resources/templets/images/404.png"/></span>
		              <c:choose>
						<c:when test="${User_Messages.title != null}">
						  <p style="padding-bottom:30px; padding-top:20px; color:#666;" class="bighint2">${User_Messages.title}</p>
						</c:when>
						<c:when test="${mess != null}">
						  <p style="padding-bottom:30px; padding-top:20px; color:#666;" class="bighint2">${mess}</p>
						</c:when>
						<c:otherwise>
						  <p style="padding-bottom:30px; padding-top:20px; color:#666;" class="bighint2">抱歉...没有找到页面...</p>
						  
						  <p>3秒后回到首页</p>
						</c:otherwise>
		                </c:choose>
		             </div>
		        </div>
		<!--order content end-->
		   </div>
       <%@include file='home/bottom.jsp'%>
   </div>
   </body>
 </html>
