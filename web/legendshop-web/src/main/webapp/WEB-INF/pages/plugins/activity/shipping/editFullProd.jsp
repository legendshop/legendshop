<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
	<input type="hidden" value="${editActiveId}" name="editActiveId" id="editActiveId">
	<input type="hidden" value="${check}" name="isCheck" id="isCheck">
	<div class="ant-row">
		<div class="ant-col-12"></div>
		<div class="ant-col-12">
		<form class="ant-form">
			<div class="sold-ser clearfix sold-ser-no-bg">
				<div class="fr">
					<div class="item">
						<span class="ant-input-preSuffix-wrapper">
							<input type="text" id="prodName" value="${prodName }" class="item-inp" style="width:300px;" placeholder="请输入商品名称">
							<input type="button" id="seach" onclick="search()" value="搜索" class="btn-r">
						</span>
					</div>
				</div>
			</div>
			<%-- <div class="ant-row" style="margin-left: -8px; margin-right: -8px;">
				<div class="ant-col-12" style="padding-left: 8px; padding-right: 8px;"></div>
				<div class="ant-col-12" style="padding-left: 8px; padding-right: 8px;">
					<div class="ant-row ant-form-item">
						<div class="ant-col-24">
							<div class="ant-form-item-control ">
								<div class="ant-input-search-wrapper percent100">
									<span class="ant-input-preSuffix-wrapper">
										<input type="text" id="prodName" value="${prodName}" class="ant-input ant-input-lg ant-input-search" placeholder="请输入商品名称">
										<span class="ant-input-suffix" id="seach"><i class="anticon anticon-search ant-input-search-icon"></i></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> --%>
		</form>
	</div>
</div>
<div class=" clearfix">
	<div class="ant-spin-nested-loading">
		<div class="ant-spin-container">
			<div class="ant-table ant-table-large ant-table-without-column-header ant-table-scroll-position-left">
				<div class="ant-table-content">
					<div class="ant-table-body">
						<table class="prodTable">
							<colgroup>
								<col>
								<col style="width: 200px; min-width: 200px;">
								<col style="width: 200px; min-width: 200px;">
								<col style="width: 150px; min-width: 150px;">
								<col style="width: 100px; min-width: 100px;">
							</colgroup>
							<thead class="ant-table-thead">
								<tr>
									<th class="ant-table-selection-column allSel">
										<c:if test="${empty check }">
											<span>
											<label class="ant-checkbox-wrapper ">
												<span class="ant-checkbox" optional="true">
													<span class="ant-checkbox-inner"></span>
													<input type="checkbox" class="ant-checkbox-input" value="on">
												</span>
											</label>
										</span>
										</c:if>
									</th>
									<th class=""><span>商品</span></th>
									<th class=""><span>价格(元)</span></th>
									<th class=""><span>库存</span></th>
									<th class=""><span>操作</span></th>
								</tr>
							</thead>
							<tbody class="ant-table-tbody">
								<c:if test="${empty requestScope.list}">
									<tr><td colspan="5"><div style="text-align: center;font-size: 14px;">您还没有可用的商品</div></td></tr>
								</c:if>
								<c:if test="${not empty requestScope.list}">
									<c:forEach var="item" items="${requestScope.list}">
										<tr class="ant-table-row  ant-table-row-level-0" data-id="${item.prodId}">
											<td class="ant-table-selection-column">
												<c:if test="${empty check }">
													<span>
														<label class="ant-checkbox-wrapper">
															<span class="ant-checkbox" data-id="${item.prodId}" <c:if test="${empty item.activeId}">optional="true"</c:if>>
																<span class="ant-checkbox-inner"></span>
																<input type="checkbox" class="ant-checkbox-input" value="on" <c:if test="${not empty item.activeId}">disabled="disabled"</c:if>>
															</span>
														</label>
													</span>
												</c:if>
											</td>
											<td class="">
												<span class="ant-table-row-indent indent-level-0" style="padding-left: 0px;"></span>
												<span class="good-info">
													<img width="50" height="50" src="<ls:photo item='${item.pic}'/>" class="ant-table-row-img" alt="${item.prodName }">
													<span class="name"><span>${item.prodName }</span></span>
												</span>
											</td>
											<td class=""><span class="t-grayer">${item.price }</span></td>
											<td class=""><span class="t-grayer">${item.actualStocks }</span></td>

											<%--隐藏判断字段--%>
											<input type="hidden" id="isProd" value="${isProd}">
											<td class="caozuo">
												<c:if test="${empty check }">
													<c:choose>
														<c:when test="${not empty item.hook && item.hook==1 }">
															<input type="hidden" value="${now}">
															<input type="hidden" value="${startTime}">
															已选择<c:if test="${startTime>now}"><a onclick="cancelProd(${item.prodId})" class='pushl' data-id='${item.prodId}'>取消参加</a></c:if>
														</c:when>
														<c:otherwise>
															<c:if test="${empty item.activeId }">
																<button type="button" class="ant-btn ant-btn-primary  btn-r" data-id="${item.prodId }" name="join"><span>参 加</span></button>
															</c:if>
															<c:if test="${not empty item.activeId }">
																<span>已参加其他商品包邮活动</span>
															</c:if>
														</c:otherwise>
													</c:choose>
												</c:if>
												<c:if test="${not empty check }">
													<c:choose>
														<c:when test="${not empty item.hook && item.hook==1 }">
															已选择
														</c:when>
														<c:otherwise>
															<c:if test="${empty item.activeId }">
																<button type="button" class="ant-btn ant-btn-primary" disabled="disabled"><span>参 加</span></button>
															</c:if>
															<c:if test="${not empty item.activeId }">
																<span>已参加其他商品包邮活动</span>
															</c:if>
														</c:otherwise>
													</c:choose>
												</c:if>
											</td>
										</tr>
									</c:forEach>
								</c:if>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
		
	<div class="clear"></div>
		 <div style="margin-top:10px;" class="page clearfix">
		 	<c:if test="${empty check }">
		 		<button type="button" class="ant-btn ant-btn-default batch-add-prod batchJoin  btn-r">
					<span>批量参加</span>
				</button>
			</c:if>
   			 <div class="p-wrap">
          		<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
   			 </div>
		</div>
	</div>
<script src="<ls:templateResource item='/resources/templets/js/shipping/editfullSelProd.js'/>"></script>
