<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>专题详情-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
     <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">   
       <%@ include file="home/header.jsp" %>
	<div id="doc">
        <div class="shop-topic-det01" style="background-color: ${theme.backgroundColor};background-image:url('<ls:photo item='${theme.backgroundPcImg}' />');">
            <c:choose >
            	<c:when test="${not empty theme.bannerPcImg}">
            		<div class="topic-det-ban" style="background-color: ${theme.bannerImgColor};">
				        <c:if test="${not empty theme.bannerPcImg}">
             			    <img src="<ls:photo item='${theme.bannerPcImg}' />" style="display:block;z-index:1;width: 100%;">
				        </c:if>
            		</div>
            	</c:when>
            	<c:otherwise>
            		<div class="topic-det-ban" style="background-color: ${theme.bannerImgColor};">
            	    	<div class="banner_details" style="text-align: center;">
							<c:if test="${theme.isTitleShow==1}">
								<h1 style='color: ${theme.titleColor};font-size: 20px;line-height: 65px;'  align="center">${theme.title}</h1>
							</c:if>
							<c:if test="${theme.isIntroShow==1}">
								<p style="color:${theme.introColor}">${theme.intro}</p>
							</c:if>
				        </div>
            		</div>
            	</c:otherwise>
            </c:choose>
            <div class="topic-det-bg" style="background-color: ${theme.backgroundColor};">
            <div class="topic-det-con">
              <c:forEach items="${requestScope.themeModuleMap}" var="themeModule">
              <div class="topic-det-con01" style="background-color: ${theme.backgroundColor};">
                	<div>
<%--                	<c:choose>--%>
<%--                		<c:when test="${themeModule.key.isTitleShow==1}">--%>
<%--	<c:if test="${not empty themeModule.key.titleBgPcimg}">background:url('<ls:photo item='${themeModule.key.titleBgPcimg}' />') no-repeat center</c:if>--%>
                			<p class="" style="height: 80px; line-height: 80px; text-align: center; font-size: 28px; letter-spacing: 0.5em;color:${themeModule.key.titleColor};">${themeModule.key.title}</p>
<%--                		</c:when>--%>
<%--                		<c:otherwise>--%>
<%--                		<p class="topic-det-tit" style="height: 200px;background-color:${themeModule.key.titleBgColor};<c:if test="${not empty themeModule.key.titleBgPcimg}">background:url('<ls:photo item='${themeModule.key.titleBgPcimg}' />') no-repeat center</c:if>"></p>--%>
<%--                		</c:otherwise>--%>
<%--                	</c:choose>--%>
                	</div>
		                <ul style="overflow: hidden;">
		                	<c:forEach items="${themeModule.value}" var="modulePrd">
				                  <li>
				                    <a href="${contextPath}/views/${modulePrd.prodId}"  target="_blank">
				                      <dl>
				                        <dt style="padding: 10px 10px 0 10px;">
				                          <img width="100%" src="<ls:images item='${modulePrd.img }' scale='4' />" alt="${modulePrd.prodName}">
				                        </dt>
				                        <dd style="padding: 5px 9px;height: 35px;">${modulePrd.prodName}</dd>
				                      </dl>
				                      <div><span>¥${modulePrd.cash}</span><em>立即购买</em></div>
				                    </a>
				                  </li>
		                 </c:forEach>
		                  </ul>
				  	<div style="display: block; height: 30px; line-height: 30px; text-align: center;"><a href="${themeModule.key.moreUrl}">查看更多 &gt;&gt;</a></div>
				    <c:if test="${ not empty themeModule.key.titleBgPcimg}">
						<div style="width: 100%;text-align: center; color: white; font-weight: bold;">
							<a href="${themeModule.key.adPcUrl}" target="_blank">
								<img width="100%" src="<ls:images item='${themeModule.key.titleBgPcimg}' scale='2'/>">
							</a>
						</div>
					</c:if>

              </div>
              </c:forEach>
              <c:if test="${not empty theme.customContent }">
				<div class="ThemeModuleProd_custom">
					<div class="custom_con"><c:out value="${theme.customContent}" escapeXml="${theme.customContent}"></c:out> </div>
				</div>
			</c:if>
			<div width: 1190px;>
				<c:forEach items="${themeRelated}" var="themerelated">
					<a target="_blank" onclick="toDetail('${themerelated.relatedThemeId}')"><img style="display: block;width: 100%;" src='<ls:photo item='${themerelated.img}'/>'/></a>
				</c:forEach>
			</div>
            <div class="topic-det-end" style="margin-bottom: 55px;"><a href="${contextPath}/theme/allThemes">查看更多专题 >></a></div>
            </div>
          </div>
          <%@ include file="home/bottom.jsp" %>
        </div>
</div>
<script type="text/javascript">
   function toDetail(themeId){
	   window.open("${contextPath}/theme/themeDetail/"+themeId,"_blank");
	   <%--window.location.href= "${contextPath}/theme/themeDetail/"+themeId;--%>
   }
</script>
</body></html>