<!DOCTYPE html>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
  <head>
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>退款详情-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
	<style type="text/css">
		.ste-con ul li .required{
			color:red;
		}
		.ret-detail .ste-con ul li a:hover{color:#e5004f;}
		.ret-detail .ste-con ul li .radio{margin-right:5px;vertical-align: text-bottom;}
		.ret-detail .ste-con ul li .checkbox{margin-right:5px;vertical-align: text-bottom;}
		.ret-detail .ste-con ul li .textarea{width: 200px;border: 1px solid #ccc;margin-left: 5px;padding:4px;}
		.ret-detail .ste-con ul li:last-child{border-bottom:none;}
		.alert li{
			line-height: 30px;
		}
	</style>
  </head>
<body class="graybody">
	   <div id="doc">
	   		 <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
	   		 <div class="w1190">
				<div class="seller-cru">
					<p>
						您的位置> <a href="${contextPath}/home">首页</a>> 
						<a	href="${contextPath}/sellerHome">卖家中心</a>> 
						<a  href="${contextPath}/s/refundReturnList/1">退款及退货</a>>
						<span class="on">退款详情</span>
					</p>
				</div>
       	 	 	<%@ include file="../included/sellerLeft.jsp" %>
			    <!-----------right_con-------------->
			     <div class="right_con">
			     	<div class="o-mt" style="margin-top:20px;"><h2>退款详情</h2></div>
			     		<div class="alert" >
				          <h4>提示：</h4>
				          <ul>
							  <%-- 	2.需你同意退款申请，买家才能退货给你； 买家退货后你需再次确认收货后，退款将自动原路退回至买家付款账户；
									3.建议你与买家协商后，再确定是否拒绝退款。如你拒绝退款后，买家可修改退款申请协议重新发起退款。 也可直接发起维权申请，将会由有赞客满介入处理。
								--%>
							  <li>1.收到买家仅退款申请，请在<em>
								  <span id="day_show">0天</span>
								  <span id="hour_show">0时</span>
								  <span id="minute_show">0分</span>
								  <span id="second_show">0秒</span></em>
								  处理本次退款，如逾期未处理，将<em>自动同意退款</em>。</li>
							  <li>2. 需你同意退款申请，买家才能退货给你。买家退货后你需再次<em>确认收货</em>后，退款将自动原路退回至买家付款账户。</li>
				            <li>3. 建议你与买家协商后，再确定是否拒绝退款。如你拒绝退款后，买家可修改退款申请协议重新发起退款。 也可直接发起维权申请，将会由有商城客服介入处理。</li>
				          </ul>
				        </div>
				        <div class="ret-step">
				          <ul>
				            <li <c:if test="${not empty subRefundReturn.applyTime}">class="done"</c:if>><span>买家申请退款</span></li>
				            <li <c:if test="${not empty subRefundReturn.sellerTime}">class="done"</c:if>><span>商家处理退款申请</span></li>
				            <li <c:if test="${not empty subRefundReturn.adminTime}">class="done"</c:if>><span>平台审核，退款完成</span></li>
				          </ul>
				        </div>
				        <div class="ret-detail">
					          <div class="ste-tit"><b>买家退款申请</b></div>
					          <div class="ste-con">
					            <ul>
					              <li><span class="span-item">用户名：</span>${subRefundReturn.userName}</li>
					              <li><span class="span-item">退款编号：</span>${subRefundReturn.refundSn}</li>
					              <li><span class="span-item">申请时间：</span><fmt:formatDate value="${subRefundReturn.applyTime}" pattern="yyyy-MM-dd HH:mm"/> </li>
					              <li><span class="span-item">订单编号：</span><a href="${contextPath }/s/orderDetail/${subRefundReturn.subNumber}" target="_blank">${subRefundReturn.subNumber} </a></li>
					               <c:if test="${not empty subRefundReturn.outRefundNo}">
			               <li><span class="span-item">第三方支付流水号：</span>${subRefundReturn.outRefundNo}</li>
			               </c:if>
					              <li>
					              	<span class="span-item">涉及商品：</span>
					              		<c:if test="${subRefundReturn.subItemId eq 0 }">
								       		<a href="${contextPath }/s/orderDetail/${subRefundReturn.subNumber}" target="_blank">${subRefundReturn.productName }</a>
										</c:if>       
							       		<c:if test="${subRefundReturn.subItemId ne 0 }">
								       		<a href="${contextPath}/views/${subRefundReturn.productId}" target="_blank">${subRefundReturn.productName }</a>
										</c:if>       
					              </li>
					              <li><span class="span-item">退款原因：</span>${subRefundReturn.buyerMessage} </li>
					              <li>
					              	<span class="span-item">退款金额：</span>
					              	<c:choose>
					              		<c:when test="${subRefundReturn.isPresell && subRefundReturn.isRefundDeposit}">
					              			<c:if test="${empty subRefundReturn.refundAmount }">
					              				&#65509;${subRefundReturn.depositRefund}
					              			</c:if>
					              			<c:if test="${not empty subRefundReturn.refundAmount }">
												&#65509;${subRefundReturn.depositRefund + subRefundReturn.refundAmount}元 (${info})
					              			</c:if>
					              		</c:when>
					              		<c:when test="${subRefundReturn.isPresell && !subRefundReturn.isRefundDeposit}">
					              			&#65509;${subRefundReturn.refundAmount}
					              		</c:when>
					              		<c:otherwise>
					              			&#65509;${subRefundReturn.refundAmount}
					              		</c:otherwise>
					              	</c:choose>
					              </li>
					              <li><span class="span-item">退款说明：</span>${subRefundReturn.reasonInfo}</li>
					              <c:if test="${not empty subRefundReturn.photoFile1}">
					              <li style="border: 0;">
					              	<span class="span-item" style="vertical-align: top;">凭证上传1：</span>
					              	 <a href="<ls:photo item='${subRefundReturn.photoFile1}'/>" target="Bank">
					                  	<img src="<ls:images item='${subRefundReturn.photoFile1}' scale='2'/>"/>
					                 </a>
					              </li>
					              </c:if>
					              <c:if test="${not empty subRefundReturn.photoFile2}">
					              	  <li style="border: 0;">
						              	<span class="span-item" style="vertical-align: top;">凭证上传2：</span>
						              	 <a href="<ls:photo item='${subRefundReturn.photoFile2}'/>" target="Bank">
						                  	<img src="<ls:images item='${subRefundReturn.photoFile2}' scale='2'/>"/>
						              	 </a>
						              </li>
					              </c:if>
					               <c:if test="${not empty subRefundReturn.photoFile3}">
						              <li style="border: 0;">
						              <span  class="span-item" style="vertical-align: top;">凭证上传3：</span>
						                <a href="<ls:photo item='${subRefundReturn.photoFile3}'/>" target="Bank">
						              	    <img src="<ls:images item='${subRefundReturn.photoFile3}' scale='2' />"/>
						              	</a>
						              </li>
					              </c:if>
					            </ul>
					          </div>
					          <c:if test="${subRefundReturn.sellerState ne 1 }">
						          <div class="ste-tit"><b>我的处理意见</b></div>
							          <div class="ste-con">
							            <ul>
							              <li>
						              	  	<span  class="span-item">审核状态：</span>
					              	  		<c:if test="${subRefundReturn.sellerState eq 1}">待审核</c:if>
							              	<c:if test="${subRefundReturn.sellerState eq 2}">同意</c:if>
							              	<c:if test="${subRefundReturn.sellerState eq 3}">不同意</c:if>
							              </li>
							              <li style="border: 0;">
											<span  class="span-item">备注信息：</span>${subRefundReturn.sellerMessage}
							              </li>
							            </ul>
							      </div>
						      </c:if>
							  <c:if test="${subRefundReturn.sellerState eq 1 && subRefundReturn.applyState eq 1 }">
					          	  <input type="hidden" id="refundId" name="refundId" value="${subRefundReturn.refundId }"/>
						          <div class="ste-tit"><b style="color:#e5004f;">我的处理意见</b></div>
						          <div class="ste-con">
						            <ul>
						              <li>
						              	  	<span  class="span-item"><em class="required">*</em>是否同意：</span>
						              	  	<div style="width:150px;display: inline-block;">
						              	  		<label class="radio-wrapper">
													<span class="radio-item">
														<input type="radio" id="agree" name="isAgree" value="1" class="radio-input"/>
														<span class="radio-inner"></span>
													</span>
													<span class="radio-txt">同意</span>
												</label>
												<label class="radio-wrapper">
													<span class="radio-item">
														<input type="radio" id="disagree" name="isAgree" value="0" class="radio-input" style="margin-left:20px;"/>
														<span class="radio-inner"></span>
													</span>
													<span class="radio-txt">不同意</span>
												</label>
					              	  		</div>
						              </li>
						              <li style="border: 0;">
										<span  class="span-item" style="float:left;"><em class="required">*</em>备注信息：</span>
						                <textarea id="sellerMessage" name="sellerMessage" rows="3" class="textarea" maxlength="150"></textarea>
						              </li>
						            </ul>
						          </div>
					          </c:if>
					        <!-- 撤销退款 start -->
							<c:if test="${subRefundReturn.sellerState eq 1 && subRefundReturn.applyState eq -1}">
								<div class="ste-tit"><b style="color:#e5004f;">我的处理意见</b></div>
								<div class="ste-con">
						            <ul>
						              <li style="border-bottom:none;">
						              	  	<span>退款申请已撤销</span>
						              </li>
						            </ul>
						          </div>
							</c:if>
							<!-- 撤销退款 end -->
					          <c:if test="${subRefundReturn.sellerState eq 2}">
					          	<div class="ste-tit"><b>商城退款审核</b></div>
						          <div class="ste-con">
						            <ul>
						              <li>
						              	<span>平台确认：</span>
						              	<c:if test="${subRefundReturn.applyState eq 2}">待平台确认</c:if>
						              	<c:if test="${subRefundReturn.applyState eq 3}">已完成</c:if>
						              </li>
						              <li style="border: 0;"><span>平台备注：</span>
						              	<c:if test="${empty subRefundReturn.adminMessage}">暂无</c:if>
						              	${subRefundReturn.adminMessage}
						              </li>
						            </ul>
						          </div>
					          </c:if>
					          <c:if test="${subRefundReturn.applyState eq 3 }">
						          <div class="ste-tit"><b>退款详细</b></div>
						          <div class="ste-con">
							            <ul>
							              <li><span>支付方式：</span>${subRefundReturn.payTypeName}</li>
							              <li><span>订单总额：</span>${subRefundReturn.subMoney}</li>
							            </ul>
						          </div>
					          </c:if>
				        </div>
				        <div class="ste-but" align="center">
				           <c:if test="${subRefundReturn.sellerState eq 1 && subRefundReturn.applyState eq 1}">
				           		<input type="button" value="确定" class="btn-r big-btn" onclick="auditRefund();"/>
				           </c:if>
				           <input type="button" value="返回" class="btn-g big-btn" onclick="window.location='${contextPath}/s/refundReturnList/${subRefundReturn.applyType }'"/>
				        </div>
				         <!-- 退款详情 -->
				 </div>
			 <!-----------right_con 结束-------------->
			 </div>
			 <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<script type="text/javascript">
		var contextPath = '${contextPath}';
		$(function(){
			userCenter.changeSubTab("myreturnorder");//高亮菜单
			
			$("#agree").click(function(){
				$("#agree").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper radio-wrapper-checked");
				$("#disagree").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper");
			});
			$("#disagree").click(function(){
				$("#disagree").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper radio-wrapper-checked");
				$("#agree").parent(".radio-item").parent(".radio-wrapper").attr("class","radio-wrapper");
			});
		});
		
		//审核用户申请退款
		function auditRefund(){
			var refundId = $("#refundId").val();
			var isAgree = $("input[name='isAgree']:checked").val();
			var sellerMessage = $("#sellerMessage").val();
			
			if(isBlank(refundId) || isBlank(isAgree) || isBlank(sellerMessage)){
				
				layer.msg('对不起,请您完善好审核信息信息后再确认!', {icon: 0});
				return;
			}
			
			var param = {"isAgree":isAgree,"sellerMessage":sellerMessage};
			$.ajax({
				url : contextPath + "/s/auditRefund/" + refundId,
				type : "POST",
				data : param,
				dataType : "JSON",
				async : true,
				success: function(result,status,xhr){
					if(result == "OK"){
						layer.alert("恭喜你,操作成功!",{icon: 1},function(){
							window.location.href = contextPath + "/s/refundReturnList/" + ${subRefundReturn.applyType};
						});
					}else{
						layer.alert(result,{icon: 2});
					}
				}
			});
		}
			
		function isBlank(value){
			return value == undefined || value == null || $.trim(value) === "";
		}

		var endTime = '${endTime}';
		<%--var timestamp=new Date();--%>
		<%--var endTime = timestamp - endTime;--%>


		var intDiff = parseInt(endTime/1000);

		function timer(intDiff){
			window.setInterval(function(){
				var day=0,
						hour=0,
						minute=0,
						second=0;//时间默认值
				if(intDiff > 0){
					day = Math.floor(intDiff / (60 * 60 * 24));
					hour = Math.floor(intDiff / (60 * 60)) - (day * 24);
					minute = Math.floor(intDiff / 60) - (day * 24 * 60) - (hour * 60);
					second = Math.floor(intDiff) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60);
				}
				if (minute <= 9) minute = '0' + minute;
				if (second <= 9) second = '0' + second;
				$('#day_show').html(day+"天");
				$('#hour_show').html('<s id="h"></s>'+hour+'时');
				$('#minute_show').html('<s></s>'+minute+'分');
				$('#second_show').html('<s></s>'+second+'秒');
				intDiff--;
			}, 1000);
		}
		$(function(){
			timer(intDiff);
		});

	</script>
	</body>
</html>
