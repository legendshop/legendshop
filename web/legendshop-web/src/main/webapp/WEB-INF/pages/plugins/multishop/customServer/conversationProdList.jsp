<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<input type="hidden" id="totalPage" value="${pageCount }">
<c:forEach items="${requestScope.list}" var="item" varStatus="status">
	<div class="list-item">
		<div class="pro clear">
			<a href="${contextPath }/views/${item.prodId}" class="pro-img" target="_blank">
				<img src="<ls:photo item='${item.pic}'/>">
			</a>
			<div class="pro-txt">
				<a href="${contextPath }/views/${item.prodId}" class="name" target="_blank" data-number="${item.prodId}">${item.name}</a>
				<p class="price" data="${item.cash}"><em class="font-family:Arial;">&yen;</em>${item.cash}</p>
			</div>
		</div>
		<div class="pro-btn">
			<a href="javascript:void(0)" class="btn sendProd" data-prodId="${item.prodId }">发送</a>
		</div>
	</div>
</c:forEach>