<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<div class="seller_right_nav">
	<div class="seller_right_nav_center">
		<ul>
			<li class="seller_right_nav_top">页面导航</li>
			<li><a href="javascript:void(0);" target_id="prod-param-tab" class="this">商品参数</a></li>
			<li><a href="javascript:void(0);" target_id="prod-base-tab" class="">基本信息</a></li>
			<li><a href="javascript:void(0);" target_id="prod-attr-tab" class="">商品属性</a></li>
			<li><a href="javascript:void(0);" target_id="prod-pic-tab" class="">商品图片</a></li>
			<li><a href="javascript:void(0);" target_id="prod-detail-tab" class="">详情描述</a></li>
			<li><a href="javascript:void(0);" target_id="prod-other-tab">其他信息</a></li>
			<li class="seller_right_nav_backtop"><a href="javascript:void(0);" id="toTop" target_id="">返回顶部</a></li>
		</ul>
	</div>
</div>