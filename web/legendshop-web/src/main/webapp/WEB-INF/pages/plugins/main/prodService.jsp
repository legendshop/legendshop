<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<c:set var="prodService" value="${show}"></c:set>
<c:choose>
    <c:when test="${not empty prodService}">
        <table cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <th class="guige" colspan="2" style="background: #f9f9f9;">服务说明</th>
            </tr>
            <tr>
                <td style="text-indent: 10px;padding-left: 100px;padding-right: 100px;">${prodService}</td>
            </tr>
            </tbody>
        </table>
    </c:when>
    <c:otherwise>
        <div>此商品没有服务说明</div>
    </c:otherwise>
</c:choose>