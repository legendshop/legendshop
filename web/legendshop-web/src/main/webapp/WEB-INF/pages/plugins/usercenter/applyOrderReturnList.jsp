<!doctype html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>申请退换货-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
<div id="doc">
   <div id="hd">
   		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
   </div>
   <div id="bd">
 <div class=" yt-wrap" style="padding-top:10px;"> 
            	  <%@include file='usercenterLeft.jsp'%>
     <div class="right_con">
     
         <div class="o-mt"><h2>退换货记录</h2></div>
         
         <div class="pagetab2">
                 <ul>           
                   <li><span><a href="${contextPath}/p/myreturnOrder">退货记录</a></span></li>                  
                   <li  class="on"><span>申请退换货</span></li>      
                 </ul>       
         </div>
         <div id="recommend" class="m10 recommend" style="display: block;">
                <table width="100%" cellspacing="0" cellpadding="0" class="buytable">
                    <tbody><tr>
                        <th style="text-align: center;">订单编号</th>
                        <th style="text-align: center;">商品名称</th>
                        <th style="text-align: center;">完成时间</th>
                        <th style="text-align: center;">操作</th>
                    </tr>
                     <c:forEach items="${requestScope.list}" var="order" varStatus="orderstatues">
                          <tr>
                          <td><a href="${contextPath}/p/orderDetail/${order.subNumber}" target="_blank">${order.subNumber}</a></td>
                          <td>
                          	<div class="img-list">
                          			 <input type="checkbox" style="float: left;" subNumber="${order.subNumber}" subItemId="${order.subItemId}" skuId="${order.skuId}" prodId="${order.productId}" >
                          			<a target="_blank" href="${contextPath}/views/${order.productId}"><img src="<ls:images item='${order.pic}' scale='3'/>"  alt=""/></a>
                          	</div>
                          </td>
                          <td><span class="ftx-03"><fmt:formatDate value="${order.finallyDate}" type="both" /></span></td>
                          <td class="order-doi"><a  target="_blank" onclick="applyreturn('${order.subNumber}');" href="javascript:void(0);"   class="btn-again2">申请退换货</a></td>
                          </tr>
                     </c:forEach> 
                </tbody></table>
	     </div>
	     <c:if test="${empty requestScope.list }" >
      			<div class="warning-option"><i>&nbsp;</i><span>暂无数据记录</span></div>
      		</c:if>
	      <div class="clear"></div>
     <div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
				            		 <c:if test="${toolBar!=null}">
											<span class="p-num">
												<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
											</span>
										</c:if>
				     			 </div>
		  				</div>
		  				
		  				
     </div>
    <div class="clear"></div>
 </div>
   </div>
   <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/applyOrderReturnList.js'/>"></script>
<script type="text/javascript">
	var contextPath="${contextPath}";
</script>
</body>
</html>
