<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<title>发货地址管理-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-address.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置> <a href="${contextPath}/home">首页</a>> 
					<a href="${contextPath}/sellerHome">卖家中心</a>> 
					<span class="on">发货地址管理</span>
				</p>
			</div>
			<%@ include file="included/sellerLeft.jsp"%>
<div class="seller-address">
				<div class="set-up">
				<a href="javascript:void(0)" class="ncbtn" style="padding:5px 10px;" onclick="alertDeliverAddAddressDiag(0)">新增发货地址</a>
					您已创建<span>${total}</span>个地址，最多可创建<span>5</span>个
				</div>
				<table class="address-table">
					<tbody><tr class="address-tit">
						<td>设为默认</td>
						<td>联系人</td>
						<%--<td>所在地区</td>--%>
						<td>街道地址</td>
						<td>手机号码</td>
						<td>操作</td>
					</tr>	
					
					<c:forEach  items="${requestScope.list}" var="deliveryAddress"   varStatus="status">			
						<tr class="detail">
							<td>
								<c:choose>
									<c:when test="${deliveryAddress.detault eq 0}">
				                         <a href="javascript:void(0);" class="col-red"  onclick="makeDeliverAddressDefault(${deliveryAddress.addrId});">设为默认</a>
									</c:when>
									<c:otherwise>
										<span style="color:#999;">默认地址</span>
									</c:otherwise>
								</c:choose>
							</td>
							<td>${deliveryAddress.contactName}</td>
							<%--<td>广东省广州市海珠区</td>--%>
							<td>${deliveryAddress.adds}</td>
							<td>${deliveryAddress.mobile}</td>
							<td>
								<ul class="mau">
									<li><a class="col-red" href="javascript:void(0);" onclick="alertDeliverAddAddressDiag(${deliveryAddress.addrId});">编辑</a></li>
									<li><a class="col-gra" href="javascript:void(0);" onclick="delDeliverAddress(${deliveryAddress.addrId});">删除</a></li>
								</ul>
							</td>
						</tr>
					</c:forEach>
					
				</tbody></table>
<!-- 已创建地址 -->
</div>
</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
<script src="<ls:templateResource item='/resources/templets/js/deliveGoodsAddress.js'/>" type="text/javascript"></script>
<script type="text/javascript">
 	var contextPath="${contextPath}";
</script>
</body>
</html>
