<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 	<meta http-equiv="refresh" content="3;URL=${contextPath}/home">
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>注册成功-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="loginback"> 
<div>
<div class="yt-wrap">
			<div class="lr_tab">
				<a class="on" href="javascript:void(0);">登 录</a><a href="${contextPath}/reg">注 册</a>
			</div>
			<div class="logintop">
				<a href="${contextPath}/home"><img src="<ls:photo item="${systemConfig.logo}"/>"  style="max-height:89px;" />
				</a>
			</div>
		</div>
<div class="regbox">
     
      <div class="reg_done">
      		<input type="hidden" id="userName" name="userName" value="${userName }"/>
            <span class="bighint gou2_back" style=" margin-bottom:20px;">恭喜您注册成功！</span>
            <div class="v_align_mid"><a href="${contextPath}/home" class="btnlink">开始购物</a>  &nbsp;&nbsp;&nbsp; 3秒后回到首页</div>      
      </div>
</div>  
 <%@ include file="home/simpleBottom.jsp" %>
</div></body>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/huanxin/jquery-1.11.1.js'/>"></script>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/huanxin/strophe-custom-2.0.1.js'/>"></script>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/huanxin/json2.js'/>"></script>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/huanxin/easemob.im-1.0.7.js'/>"></script>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/huanxin/bootstrap.js'/>"></script>
<script type="text/javascript">
	var xmppURL = null;
	var apiURL = null;
	var curUserId = null;
	var curChatUserId = null;
	var conn = null;
	var curRoomId = null;
	var msgCardDivId = "chat01";
	var talkToDivId = "talkTo";
	var talkInputId = "talkInputId";
	var fileInputId = "fileInput";
	var bothRoster = [];
	var toRoster = [];
	var maxWidth = 200;
	var groupFlagMark = "group--";
	var groupQuering = false;
	var textSending = false;
	var appkey = "legendshop#legendshop-dev";
	var time = 0;
	function regist(){
		var user = $("#userName").val();
		var pass = user+"123456";
		var nickname =user;
		var options = {
			username : user,
			password : pass,
			nickname : nickname,
			appKey : appkey,
			success : function(result) {
			},
			error : function(e) {
			},
			apiUrl : apiURL
		};
		Easemob.im.Helper.registerUser(options);
	};
	regist();
</script>
</html>
