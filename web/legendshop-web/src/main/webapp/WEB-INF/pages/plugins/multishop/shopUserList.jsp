<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>员工管理-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-sub-staff${_style_}.css'/>" rel="stylesheet">
    <style>
    	.set-up a:hover{
    		color: #e5004f;
    	}
    </style>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">员工管理</span>
				</p>
			</div>
			<%@ include file="included/sellerLeft.jsp" %>
			<div class="sub-staff">
				<div class="pagetab2">
					<ul id="listType">
						<li class="on"><span>员工管理</span></li>
					</ul>
	   			</div>
				 <form:form  action="${contextPath}/s/shopUsers/query" method="post" id="from1">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
				 </form:form>
				<div class="set-up sold-ser sold-ser-no-bg">
					<input type="text" class="btn-r" onclick="createShopUser();" value="新增员工"/>
					<a href="${contextPath}/employeeLogin?" class="btn-r" target="_blank" style="margin-left: 10px;">子账号登录</a>
				</div>
				<table class="staff-table sold-table">
					<tr class="staff-tit">
						<th>员工账号</th>
						<th>职位</th>
						<th>员工姓名</th>
						<th>状态</th>
						<th width="250">操作</th>
					</tr>
					<c:if test="${empty requestScope.list}">
						<tr><td colspan='5' height="40">请先添加员工</td></tr>
					</c:if>	
					<c:forEach items="${requestScope.list}" var="shopUser" varStatus="status">			
						<tr class="detail">
							<td>${shopUser.name}</td>
							<td>${shopUser.roleName}</td>
							<td>${shopUser.realName}</td>
							<td>
								<c:choose>
                					<c:when test="${shopUser.enabled}">启用</c:when>
                					<c:otherwise>停用</c:otherwise>
                				</c:choose>
							</td>
							<td>
								<a href="javascript:void(0);" onclick="updateShopUser(${shopUser.id},1);" title="修改" class="btn-r">修改</a>
								<a href="javascript:void(0);" onclick="updatePwd(${shopUser.id});" title="修改密码" class="btn-g">修改密码</a>
									<c:choose>
										<c:when test="${shopUser.enabled}">
											<a href="javascript:void(0);" onclick="stopUse(${shopUser.id});" title="停用" class="btn-g">停用</a>
										</c:when>
										<c:otherwise>
											<a href="javascript:void(0);" onclick="beUse(${shopUser.id});" title="启用" class="btn-g">启用</a>
										</c:otherwise>
									</c:choose>
									<a href="javascript:void(0);" onclick="delUser(${shopUser.id});" title="删除" class="btn-g">删除</a>
							</td>
						</tr>
					</c:forEach>
				</table>
				<div class="clear"></div>
					<div style="margin-top:10px;" class="page clearfix">
			     			 <div class="p-wrap">
								<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			     			 </div>
		  				</div>
			</div>
		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/shopUserList.js'/>"></script>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/common/js/cookieUtil.js'/>"></script>
			<script type="text/javascript">
				var contextPath="${contextPath}";
				var host = window.location.host;
				var employeeLoginPath=host+contextPath+"/employeeLogin"
				$("#employeeLoginPath").text(employeeLoginPath);
			</script>
</body>
</html>