<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<div class="chat-list" id="chat-list">
	<c:forEach var="item" items="${groupMsgRecordMap}">
		<c:set var="dayDate" value="${item.key}"></c:set>
		<c:set var="chatBodyLists" value="${item.value}"></c:set>
			<!-- 对话内容 -->
			<div class="chat-time"><p class="time">${dayDate}</p></div>
			<c:forEach var="chatBody" items="${chatBodyLists}">
				<!-- IM消息中，有单纯是一个时间的，不在消息记录中显示，需要排除 -->
				<c:if test="${ chatBody.msgType ne 7}">
					<div class="chat-txt clear">
						 <!-- 判断是用户还是商家样式 -->
						 <c:choose>
						 	<c:when test="${fromUserId eq chatBody.from}">
						 		<c:set value="cus" var="temp"></c:set>
						 	</c:when>
						 	<c:otherwise>
						 		<c:set value="sho" var="temp"></c:set>
						 	</c:otherwise>
						 </c:choose>
						<div class="chat-area ${temp}">
							<p class="name">
								<span class="user-img"><img src='<ls:photo item="${chatBody.userImg}"/>' alt=""></span>
								${chatBody.userName}<span class="txt-time"><fmt:formatDate value="${chatBody.createTime}" pattern="HH:mm:ss"/> </span>
							</p>
							<c:choose>
								<c:when test="${chatBody.msgType eq 0 }"> <!-- 文本消息 -->
									<div class="dialog">
										<i class="dia-arrow"></i>
										<div class="dia-con"><p class="dia-con-p">${chatBody.content}</p></div>
									</div>
								</c:when>
								<c:when test="${chatBody.msgType eq 1 }"><!-- 图片消息 -->
									<div class="dialog">
										<i class="dia-arrow"></i>
										<div class="dia-con">
											<a href="javascript:void(0)" class="dia-goods clear">
												<span class="g-img" style="width:150px;margin-right:0px;height: inherit;">
													<img src='<ls:photo item="${chatBody.content}"/>'>
												</span>
												<span class="g-info" style="padding-left:0px">
												</span>
											</a>
										</div>
									</div>
								</c:when>
								<c:when test="${chatBody.msgType eq 5 }"><!-- 商品消息 -->
									<div class="dialog">
										<i class="dia-arrow"></i>
										<div class="dia-con order-box">
											<a href="#" class="dia-goods clear">
												<span class="g-img"><img src="${chatBody.imMsgOrderOrProdDto.pic}" alt=""></span>
												<span class="g-info">
													<span class="g-num">商品名称：${chatBody.imMsgOrderOrProdDto.prodName}</span>
													<span class="g-nam">商品价格：${chatBody.imMsgOrderOrProdDto.price}</span>
												</span>
											</a>
										</div>
									</div>
								</c:when>
								<c:when test="${chatBody.msgType eq 6 }"><!-- 订单消息 -->
									<div class="dialog">
										<i class="dia-arrow"></i>
										<div class="dia-con order-box">
											<a href="javascript:void(0);" class="dia-goods clear" target="_blank">
												<span class="g-img"><img src='${chatBody.imMsgOrderOrProdDto.pic}'></span>
												<span class="g-info">
													<span class="g-num">订单编号：${chatBody.imMsgOrderOrProdDto.subNumber}</span>
													<span class="g-nam">订单金额：${chatBody.imMsgOrderOrProdDto.price}</span>
													<span class="g-nam">下单时间：${chatBody.imMsgOrderOrProdDto.orderDate}</span>
												</span>
											</a>
										</div>
									</div>
								</c:when>
							</c:choose>
						</div>
					</div>
				</c:if>
			</c:forEach>
			<!-- 对话内容 -->
	</c:forEach>
	</div>
	<!-- /对话窗口 -->
<div style="margin-top: 15px;" class="page clearfix">
	<div class="p-wrap">
		<ls:page pageSize="${pageSize}" total="${total}" curPageNO="${curPageNO }" type="simple" />
	</div>
</div>				