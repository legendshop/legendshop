<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
	<title>评价详情-${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="home/top.jsp"%>
		<div id="bd">
 
 <!--crumb-->
	<div id="J_crumbs" class="crumb yt-wrap">
		<div class="crumbCon">
			<div class="crumbSlide">
				<div class="crumbs-nav-item one-level">
					<a class="crumbs-link" href="<ls:url address="/"/>"> 首页 </a><i class="crumbs-arrow">&gt;</i>
				</div>
				<div class="crumbs-nav-item one-level">
					<a class="crumbs-link" href="<ls:url address="/productcomment/query/${prod.id}"/>"> 该商品所有评论 </a>
				</div>
				
			</div>
		</div>
	</div>
	<!--crumb end-->
 
 <div class="w main" style="width:1190px;margin:auto;">
        <div class="left" style="float:left;border: 1px solid #ddd;background: #fff;width: 230px;">
            <div class="m" id="pinfo">
                <div class="mt" style="background: #f9f9f9;padding: 10px;">
                    <h2>商品信息</h2>
                </div>
		        <div class="mc" style="padding: 10px 15px;">
					<div class="p-img2" style="text-align: center;"><a href="${contextPath}/views/${prod.id}" target="_blank">
					   <img alt=""  src="<ls:images item='${prod.pic}' scale="1"/>"  height="150px" width="150px" ></a></div>
				    <div class="clear"></div>
					<div class="p-name" style="margin-top:10px;">商品：<a href="${contextPath}/views/${prod.id}" target="_blank">${prod.name}</a></div>
					<div class="p-price" style="margin-top:5px;">价格：<strong style="color:#e5004f;"><fmt:formatNumber type="currency" value="${prod.cash}" pattern="${CURRENCY_PATTERN}"/></strong></div>
					<div class="p-grade clearfix" style="margin-top:5px;">
						<span class="fl">评价得分：</span>
						<div class="fl">
							<div class="star sa${prod.reviewScores}"></div>(${prod.reviewScores}分)						
						</div>
					</div>
					<div class="num-comment" style="margin-top:5px;">评论数：${prod.comments}条</div>
				</div>
     	</div>
            <!--pinfo end-->
     </div>
        <!--left end-->
    <div class="right-extra" style="float:right;background: #fff;border: 1px solid #ddd;width: 940px;" >
     		<div id="comment-detail" class="m m1">
            <div class="mt" style="background: #f9f9f9;padding: 10px;">
                <h2>评价详情</h2>
            </div>
            <div id="comments-list" class="mc prod-comment" >
            		<%@include file='prodcommentlist.jsp'  %>
        	</div>   
    </div>
    <div class="clear"></div>
 </div>
 

</div>
		<%@ include file="home/bottom.jsp"%>
	</div>
	 <script type="text/javascript">
 var contextPath = '${contextPath}';
 function pager(curPageNO){
	 window.location.href = "${contextPath}/productcomment/query/"+${prod.id}+"?curPageNO="+curPageNO ;
 }
 </script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/productComment.js'/>"></script>
</body>
</html>