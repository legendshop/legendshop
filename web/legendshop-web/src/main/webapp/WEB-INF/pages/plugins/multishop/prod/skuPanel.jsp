<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<div class="sku-p">
	<div class="sku-p-l">
		<ul>
			<c:forEach items="${productDto.skuDtoList}" var="skuDto" varStatus="skusts">
				<c:if test="${not empty skuDto.properties}">
					<li <c:if test="${skusts.first}">class="on"</c:if> propValIds="${skuDto.propValueIds}" onclick="changeSkuTab(this);">
					<span>
					<c:forEach items="${skuDto.propertiesNameList}" var="prop" varStatus="sts">
						<c:if test="${!sts.first}"></c:if>
						<span class="pv_${prop.key}">${prop.value}</span>
					</c:forEach>
					</span>
					</li>
				</c:if>
			</c:forEach>
		</ul>
	</div>
	
	<div class="sku-p-m">
	
		<c:forEach items="${productDto.skuDtoList}" var="skuDto" varStatus="skusts">
			<c:if test="${not empty skuDto.properties}">
				<div class="sku-p-d" id="row_${skuDto.propValueIds}" <c:if test="${!skusts.first}">style="display:none;"</c:if> >
					<div class="sku-f">
						<label class="name-t"><span class="text-danger">*</span>单品标题：</label>
						<div class="val-t">
							<input value="${skuDto.name}" id="row_${skuDto.propValueIds}_name" onBlur="recordSkuValue(this);" type="text" class="val-i title" maxlength="50" minlength="3" >
						</div>
					</div>
					<div class="sku-f">
						<label class="name-t"><span class="text-danger">*</span>单品价格：</label>
						<div class="val-t">
							<input value="<fmt:formatNumber groupingUsed='false' type='number' value='${skuDto.price}'/>" id="row_${skuDto.propValueIds}_cash" onBlur="recordSkuValue(this);" type="text" class="val-i price" maxlength="12" minlength="3" >
						</div>
					</div>
					<div class="sku-f">
						<label class="name-t">商家编码：</label>
						<div class="val-t">
							<input value="${skuDto.partyCode}" id="row_${skuDto.propValueIds}_code" onBlur="recordSkuValue(this);" type="text" class="val-i code" maxlength="30" >
						</div>
					</div>
					<div class="sku-f">
						<label class="name-t">商品条形码：</label>
						<div class="val-t">
							<input value="${skuDto.modelId}" id="row_${skuDto.propValueIds}_model" onBlur="recordSkuValue(this);" type="text" class="val-i model" maxlength="30" >
						</div>
					</div>
					<div class="sku-f">
						<label class="name-t">物流体积(立方米)：</label>
						<div class="val-t">
							<input value="${skuDto.volume}" id="row_${skuDto.propValueIds}_volume" onBlur="recordSkuValue(this);" type="text" class="val-i volume" maxlength="50" >
						</div>
					</div>
					<div class="sku-f">
						<label class="name-t">物流重量(千克)：</label>
						<div class="val-t">
							<input value="${skuDto.weight}" id="row_${skuDto.propValueIds}_weight" onBlur="recordSkuValue(this);" type="text" class="val-i weight" maxlength="50" >
						</div>
					</div>
					<div class="sku-f">
						<label class="name-t"><span class="text-danger">*</span>单品库存：</label>
						<div class="val-t">
							<input value="${skuDto.stocks}" id="row_${skuDto.propValueIds}_stock" onBlur="recordSkuValue(this);" type="text" class="val-i stock" maxlength="8" >
						</div>
						
						
					</div>
					<div class="sku-f">
						<label class="name-t"><span class="text-danger">*</span>是否上架：</label>
						<div class="val-t">
							<c:choose>
								<c:when test="${skuDto.status == 0}">
									<label class="radio-inline"> <input type="radio" name="pro_status_${skuDto.propValueIds}" onBlur="recordSkuValue(this);" class="pro_status up_yes" value="1" > 上架 </label> 
									<label class="radio-inline"> <input type="radio" name="pro_status_${skuDto.propValueIds}" onBlur="recordSkuValue(this);" class="pro_status up_not" value="0" checked="checked"> 下架 </label>
								</c:when>
								<c:otherwise>
									<label class="radio-inline"> <input type="radio" name="pro_status_${skuDto.propValueIds}" onBlur="recordSkuValue(this);" class="pro_status up_yes" value="1" checked="checked"> 上架 </label> 
									<label class="radio-inline"> <input type="radio" name="pro_status_${skuDto.propValueIds}" onBlur="recordSkuValue(this);" class="pro_status up_not" value="0" > 下架 </label>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
					<div class="sku-f">
						<label class="name-t">应用设置：</label>
						<div class="val-t">
							<a class="copy_ware_to_other_spec_ctrl" href="javascript:;" onclick="copyWareToOtherSpec(this)" propValueIds="${skuDto.propValueIds}">将此设置应用到其他单品</a>
						</div>
					</div>
				</div>
			</c:if>
		</c:forEach>
	</div>
	
	<!-- 没有选中规格属性时，显示 -->
	<div class="sku-default">
		<c:forEach items="${productDto.skuDtoList}" var="skuDto" varStatus="skusts">
			<c:if test="${empty skuDto.properties}">
				<div class="sku-f">
					<label class="name-t">商家编码：</label>
					<div class="val-t">
						<input value="${skuDto.partyCode}" type="text" class="val-i model partyCode" maxlength="30" >
					</div>
				</div>
				<div class="sku-f">	
					<label class="name-t">商品条形码：</label>
					<div class="val-t">
						<input value="${skuDto.modelId}" type="text" class="val-i model modelId" maxlength="30" >
					</div>
				</div>
				<div class="sku-f">	
					<label class="name-t">物流体积(立方米)：</label>
					<div class="val-t">
						<input value="${skuDto.volume}" type="text" class="val-i model volume" maxlength="50" >
					</div>
				</div>
				<div class="sku-f">	
					<label class="name-t">物流重量(千克):</label>
					<div class="val-t">
						<input value="${skuDto.weight}" type="text" class="val-i model weight" maxlength="50" >
					</div>
				</div>
				<div class="sku-f">
					<label class="name-t"><span class="text-danger">*</span>商品库存：</label>
					<div class="val-t">
							<input value="${skuDto.stocks}"  type="text" class="val-i model stock"  maxlength="8" >
						</div>
				</div>	
			</c:if>
		</c:forEach>
		<c:if  test="${empty productDto.skuDtoList}">
			<div class="sku-f">
				<label class="name-t">商家编码：</label>
				<div class="val-t">
					<input value="${productDto.partyCode}"  type="text" class="val-i model partyCode" maxlength="30" >
				</div>
			</div>
			<div class="sku-f">
				<label class="name-t">商品条形码：</label>
				<div class="val-t">
					<input value="${productDto.modelId}"  type="text" class="val-i model modelId" maxlength="30" >
				</div>
			</div>
			<div class="sku-f">	
				<label class="name-t">物流体积(立方米)：</label>
				<div class="val-t">
					<input value="${skuDto.volume}" type="text" class="val-i model volume" maxlength="50" >
				</div>
			</div>
			<div class="sku-f">	
				<label class="name-t">物流重量(千克):</label>
				<div class="val-t">
					<input value="${skuDto.weight}" type="text" class="val-i model weight" maxlength="50" >
				</div>
			</div>
			<div class="sku-f"> 
				<label class="name-t"><span class="text-danger">*</span>商品库存：</label>
				<div class="val-t">
					 <input value="${skuDto.stocks}"  type="text" class="val-i model stock" maxlength="8" >
						</div>
			</div>	
		</c:if>
	</div>
	
</div>
<div class="submit">
	<div class="float-submitbar">
		<button value="上一步" class="btn-prev J_Submit btn btn-main-primary btn-submit" type="button" onclick="panelPrev();">
			<span class="prev-txt">上一步</span>
		</button>
		<button value="发布" id="event_submit_do_publish" class="J_Submit btn btn-main-primary btn-submit" type="button" onclick="submitTable();">
			<span class="btn-txt">发布商品</span>
		</button>
	</div>
</div>

<!-- 空的单品信息  --用于js新增SKU -->
<div id="blankSkuDetial" style="display:none;">
	<div class="sku-p-d hide" id="row_#propValueIds#">
		<div class="sku-f">
			<label class="name-t"><span class="text-danger">*</span>单品标题：</label>
			<div class="val-t">
				<input value="#skuName#" id="row_#propValueIds#_name" onBlur="recordSkuValue(this);" type="text" class="val-i title" maxlength="50" minlength="3" >
			</div>
		</div>
		<div class="sku-f">
			<label class="name-t"><span class="text-danger">*</span>单品价格：</label>
			<div class="val-t">
				<input value="#skuPrice#" id="row_#propValueIds#_cash" onBlur="recordSkuValue(this);" type="text" class="val-i price" maxlength="12" minlength="3" >
			</div>
		</div>
		<div class="sku-f">
			<label class="name-t">商家编码：</label>
			<div class="val-t">
				<input value="#partyCode#" id="row_#propValueIds#_code" onBlur="recordSkuValue(this);" type="text" class="val-i code" maxlength="50" >
			</div>
		</div>
		<div class="sku-f">
			<label class="name-t">商品条形码：</label>
			<div class="val-t">
				<input value="#modelId#" id="row_#propValueIds#_model" onBlur="recordSkuValue(this);" type="text" class="val-i model" maxlength="50" >
			</div>
		</div>
		<div class="sku-f">	
			<label class="name-t">物流体积(立方米)：</label>
			<div class="val-t">
				<input value="${skuDto.volume}" type="text" class="val-i model volume" maxlength="50" >
			</div>
		</div>
		<div class="sku-f">	
			<label class="name-t">物流重量(千克):</label>
			<div class="val-t">
				<input value="${skuDto.weight}" type="text" class="val-i model weight" maxlength="50" >
			</div>
		</div>
		<div class="sku-f">
			<label class="name-t"><span class="text-danger">*</span>单品库存：</label>
			<div class="val-t">
				<input value="#stock#" id="row_#propValueIds#_stock" onBlur="recordSkuValue(this);" type="text" class="val-i stock" maxlength="8" >
			</div>
		</div>
		<div class="sku-f">
			<label class="name-t"><span class="text-danger">*</span>是否上架：</label>
			<div class="val-t">
				<label class="radio-inline"> <input type="radio" name="pro_status_#propValueIds#" onBlur="recordSkuValue(this);" class="pro_status up_yes" value="1" checked="checked"> 上架 </label> 
				<label class="radio-inline"> <input type="radio" name="pro_status_#propValueIds#" onBlur="recordSkuValue(this);" class="pro_status up_not" value="0" > 下架 </label>
			</div>
		</div>
		<div class="sku-f">
			<label class="name-t">应用设置：</label>
			<div class="val-t">
				<a class="copy_ware_to_other_spec_ctrl" href="javascript:;" onclick="copyWareToOtherSpec(this)" propValueIds="#propValueIds#">将此设置应用到其他单品</a>
			</div>
		</div>
	</div>
</div>

<!-- 空的单品信息  --用于js去除所有属性时，默认的SKU -->
<div id="blankSkuDefault" style="display:none;">
	<div class="sku-f">
		<label class="name-t">商家编码：</label>
		<div class="val-t">
			<input value="" type="text" class="val-i model partyCode" maxlength="50" >
		</div>
	</div>
	<div class="sku-f">
		<label class="name-t">商品条形码：</label>
		<div class="val-t">
			<input value="" type="text" class="val-i model modelId" maxlength="50" >
		</div>
	</div>
	<div class="sku-f">	
		<label class="name-t">物流体积(立方米)：</label>
		<div class="val-t">
			<input value="${skuDto.volume}" type="text" class="val-i model volume" maxlength="50" >
		</div>
	</div>
	<div class="sku-f">	
		<label class="name-t">物流重量(千克):</label>
		<div class="val-t">
			<input value="${skuDto.weight}" type="text" class="val-i model weight" maxlength="50" >
		</div>
	</div>
	<div class="sku-f">
		<label class="name-t"><span class="text-danger">*</span>商品库存：</label>
		<div class="val-t">
			<input  value=""  type="text" class="val-i model stock" maxlength="8" >
		</div>
	</div>	
</div>