<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<li class='cc-list-item' id="category_${level}">
	<div class="cc-cbox-filter">
		<label  for="cc-cbox-filter_${level}" >输入商品分类</label><input id="cc-cbox-filter_${level}" class="lebelinput"  style="width: 188px;">
	</div>
	<ul role="listbox" class="cc-cbox-cont" >
		<c:forEach items="${requestScope.categoryList }" var="category" >
				<li  id="${category.id}" class='cc-cbox-item <c:if test="${fn:length(category.childrenList)>0}">cc-hasChild-item</c:if>'>
						 ${category.name }
				</li>
		</c:forEach>
	</ul>
</li>
<script type="text/javascript">
	//$(document).ready(function(e) {
	//		publish2.doExecute();
	//});
	
	$(document).ready(function(e) {
		var level = '${level}';
		$("#category_"+level).find("ul li").off('click').on('click',function() {
			$this = $(this);
			var cateId = $this.attr("id");
			$("#categoryId").val(cateId);
			
			$("#category_"+level).nextAll().remove();
			
			if($this.hasClass("cc-hasChild-item")){
				var targetUrl = "${contextPath}/s/publish2/" +cateId ;
				retrieveNext(targetUrl);
			}
			
			$this.siblings().removeClass("cc-selected");
			$this.addClass("cc-selected");

			selectedCategory();
		});
		
		$('#cc-cbox-filter_'+level).off('keypress').on('keypress', function(e){
			   var valueText = $(this).val();
			   var parentId = $(this).parent().parent().prev().find("li[class*='cc-selected']").attr("id");
			   if(!isBlank(parentId)){
				   if(e.which == 13) {
		               $.post("${contextPath}/s/loadCategory", 
					        {
					            "cateName": valueText,
					            "level": level,
					            "parentId":parentId
					         },
					        function(retData) {
					        	  $("#category_"+level).find("ul[class='cc-cbox-cont'").html(retData);
							});
				    }
			   }else{
				   alert("请选中上一级！");
			   }
			   
		});
	});
</script>