<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/dialog.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>商家包邮活动</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/mergeGroup/mergeGroupEdit.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/shipping/addShipping.css'/>" rel="stylesheet"/>
    <script src="<ls:templateResource item='/resources/plugins/My97DatePicker/WdatePicker.js'/>" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/plugins/default/layer.css" />
</head>
<body>
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
	</div>
	<div class="w1190">
		<div class="seller-cru">
			<p>
				您的位置>
				<a href="javascript:void(0)">首页</a>>
				<a href="javascript:void(0)">卖家中心</a>>
				<span class="on">店铺包邮</span>
			</p>
		</div>
		<!-- 侧边导航 -->
		<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
		<!-- /侧边导航 -->
		<div class="add-fight">
			<form id="shopShippingForm">
				<input type="hidden" name="id" value="${shippingActive.id}">
				<input type="hidden" name="description" id="description" value="${shippingActive.description}">
				<!-- 活动信息 -->
				<div class="add-item">
					<div class="add-tit"><span>活动信息</span></div>
					<div class="activity-mes">
						<span class="act-tit"><em>*</em>活动名称：</span>
						<input type="text" name="name" id="name" class="act-name" placeholder="请填写活动名称" value="${shippingActive.name}" maxlength="20">
						<span class="act-des">（2-20）字</span>
					</div>
					<div class="activity-mes">
						<span class="act-tit"><em>*</em>活动时间：</span>
						<span class="act-time">
							<c:choose>
								<c:when test="${not empty shippingActive }">
									<lable><em style="color: #e5004f;margin-right: 5px;"></em></lable>
									<input type="text" readonly="readonly" placeholder="开始时间" name="startDate" disabled="disabled" size="21" id="startDate" value='<fmt:formatDate value="${shippingActive.startDate}" pattern="yyyy-MM-dd"/>'/><span></span>
									&nbsp;-&nbsp;
									<lable><em style="color: #e5004f;margin-right: 5px;"></em></lable>
									<input readonly="readonly" placeholder="结束时间" name="endDate" disabled="disabled" size="21" id="endDate" value='<fmt:formatDate value="${shippingActive.endDate}" pattern="yyyy-MM-dd"/>' type="text" /><span></span>
								</c:when>
								<c:otherwise>
									<lable><em style="color: #e5004f;margin-right: 5px;"></em></lable>
									<input type="text" readonly="readonly" placeholder="开始时间" name="startDate" size="21" id="startDate" value='<fmt:formatDate value="${shippingActive.startDate}" pattern="yyyy-MM-dd"/>'/><span></span>
									&nbsp;-&nbsp;
									<lable><em style="color: #e5004f;margin-right: 5px;"></em></lable>
									<input readonly="readonly" placeholder="结束时间" name="endDate" size="21" id="endDate" value='<fmt:formatDate value="${shippingActive.endDate}" pattern="yyyy-MM-dd"/>' type="text" /><span></span>
								</c:otherwise>
							</c:choose>
						</span>
						<span class="act-des">（编辑无法修改时间）</span>
					</div>
				</div>
				<!-- /活动信息 -->
				<!-- 活动规则 -->
				<div class="add-item">
					<div class="add-tit"><span>活动规则</span></div>
					<div class="activity-mes">
						<span class="act-tit"><em>*</em>满：</span>
						<c:choose>
							<c:when test="${not empty  shippingActive}">
								<c:if test="${shippingActive.reduceType eq 1}">
									<input type="text" max="999999.99" autocomplete="off" name="fullValue" id="fullValue" style="width: 60px;" value="${shippingActive.fullValue}" disabled="disabled" style="cursor:not-allowed;">
									<select id="reduceType" name="reduceType" disabled="disabled" style="cursor:not-allowed;">
										<option value="1" selected="selected">元</option>
									</select>
								</c:if>
								<c:if test="${shippingActive.reduceType eq 2}">
									<input type="text" max="999999.99" autocomplete="off" name="fullValue" id="fullValue" style="width: 60px;" value="<fmt:formatNumber value="${shippingActive.fullValue}" pattern="0"></fmt:formatNumber>" disabled="disabled" style="cursor:not-allowed;">
									<select id="reduceType" name="reduceType" disabled="disabled" style="cursor:not-allowed;">
										<option value="2" selected="selected">件</option>
									</select>
								</c:if>
							</c:when>
							<c:otherwise>
								<input type="text" max="999999.99" autocomplete="off" name="fullValue" id="fullValue" style="width: 60px;" value="">
								<select id="reduceType" name="reduceType">
									<option value="1">元</option>
									<option value="2">件</option>
								</select>
							</c:otherwise>
						</c:choose>
						<span class="act-des">（满多少 元 | 件 包邮）</span>
					</div>
					<div class="activity-mes" style="margin-top: 35px;">
						<span class="act-tit"></span>
						<a href="javascript:void(0)" class="btn-r big-btn" id="save">确认保存</a>&nbsp;
						<a href="javascript:history.go(-1);" class="btn-g big-btn">返回列表</a>
					</div>
				</div>
				<!-- /活动规则 -->
			</form> 
		</div>
	</div>
	<!--foot-->
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	<!--/foot-->
	<script src="${contextPath}/resources/common/js/jquery.validate.js"></script>
	<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/layer/layer.js'/>"></script>
	<script type="text/javascript">	
	
	 $(document).ready(function() {
			laydate.render({
				   elem: '#startDate',
				   calendar: true,
				   theme: 'grid',
				   type:'date',
				   min:'-1',
				   trigger: 'click'
				   
			  });
	  
	  		laydate.render({
				   elem: '#endDate',
				   calendar: true,
				   theme: 'grid',
				   type:'date',
				   min:'-1',
				   trigger: 'click'
			  });
		});
	 
		var contextPath = "${contextPath}";
	</script>
	<script src="<ls:templateResource item='/resources/templets/js/shipping/addShopShipping.js'/>"></script>
</body>
</html>