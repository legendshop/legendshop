<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.legendesign.net/tags" prefix="ls"%>
<%
  request.setAttribute("contextPath", request.getContextPath());
%>
<table class="ret-tab" cellpading="0" cellspacing="0" style="margin: 0;">
	<tr class="tit">
		<th width="10"></th>
		<th colspan="2" width="35%" >商品/订单号/退款号</th>
		<th width="10%">退款金额</th>
		<th width="15%">申请时间</th>
		<th width="14%">处理状态</th>
		<th width="15%">平台确认</th>
		<th width="15%">操作</th>
    </tr>
    <c:choose>
    	<c:when test="${empty requestScope.list}">
	    	 <tr>
	 	 		<td colspan="20">
	 	 			<!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
	 	 			<div class="warning-option"><!-- <i></i> --><span>没有符合条件的信息</span></div>
	 	 		</td>
	 	 	</tr>
    	</c:when>
    	<c:otherwise>
    	    <c:forEach items="${requestScope.list }" var="refundReturn">
	    		<tr class="detail">
					<td width="10"></td>
					<c:if test="${refundReturn.subItemId ne 0}">
						<td width="75">
							<div class="pic-thumb">
								<a href="${contextPath }/views/${refundReturn.productId}" target="_blank">
									<img src="<ls:images item='${refundReturn.productImage}' scale='3'/>"/>
								</a>
							</div>
						</td>
					</c:if>
					<td colspan="${refundReturn.subItemId ne 0?1:2}" style="text-align: left;padding-right: 10px;">
						<dl class="goods-name">
						    <dt>
						    	<c:if test="${refundReturn.productId ne 0}">
						    		<a href="${contextPath }/views/${refundReturn.productId}" target="_blank">${refundReturn.productName }</a>
						    	</c:if>
						    	<c:if test="${refundReturn.productId eq 0}">
						    		<a href="${contextPath }/p/orderDetail/${refundReturn.subNumber }" target="_blank">${refundReturn.productName }</a>
						    	</c:if>
						    </dt>
				        	<dd>订单编号：<a href="${contextPath }/p/orderDetail/${refundReturn.subNumber }" target="_blank">${refundReturn.subNumber }</a></dd>
				        	<dd>
				        	退款编号：${refundReturn.refundSn}
				        	<!-- IM商城客服入口 --> 
							<span style="margin-left: 15px;">
								<a target="_blank" style="color: #006dc7;" href="${contextPath}/p/im/customerShop/${refundReturn.refundSn}/${refundReturn.shopId}">联系商家</a>
							</span>
				        	</dd>
				        </dl>
				    </td>
				    <td>
				    	<c:choose>
					    	<c:when test="${refundReturn.isRefundDeposit}">
					    		¥${refundReturn.refundAmount + refundReturn.depositRefund}
					    	</c:when>
					    	<c:otherwise>
					    		¥${refundReturn.refundAmount}
					    	</c:otherwise>
				    	</c:choose>
				    </td>
				    <td><fmt:formatDate value="${refundReturn.applyTime}" pattern="yyyy-MM-dd HH:mm" /></td>
				    <td>
				    	<c:if test="${refundReturn.sellerState eq 1 && refundReturn.applyState eq 1}">
			    			待审核
				    	</c:if>
				    	<c:if test="${refundReturn.sellerState eq 1 && refundReturn.applyState eq -1}">
				    		已撤销
				    	</c:if>
				    	<c:if test="${refundReturn.sellerState eq 2}">
			    			同意
				    	</c:if>
				    	<c:if test="${refundReturn.sellerState eq 3}">
				    		不同意
				    	</c:if>
				    </td>
				    <td>
				    	<c:choose>
					    	<c:when test="${refundReturn.applyState eq 2}">
					    		待平台确认
					    	</c:when>
			    			<c:when test="${refundReturn.applyState eq 3}">
			    				已完成
			    			</c:when>
			    			<c:otherwise>
			    				无
			    			</c:otherwise>
					    </c:choose>
				    </td>
				    <td style="text-align: left;" >
				    	<a href="${contextPath }/p/refundDetail/${refundReturn.refundId}" class="btn-r" target="_blank">查看</a>
				    	<c:if test="${refundReturn.sellerState eq 3}">
				    		<a href="javascript:void(0);" class="btn-g" onclick="deleteAction('${refundReturn.refundId}');" style="color:#666;">删除</a>
				    	</c:if>
				    	<c:if test="${refundReturn.sellerState eq 1 && refundReturn.applyState eq 1}">
				    	  <a href="javascript:void(0);" class="btn-g" onclick="candelAction('${refundReturn.refundSn}');" style="color:#666;">撤销</a>
				    	</c:if>
				    </td>
				</tr>
	    	</c:forEach>
    	</c:otherwise>
    </c:choose>
</table>
<!-- 页码条 -->
<div style="margin-top:10px;" class="page clearfix">
	<div class="p-wrap">
  		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
	 </div>
</div>