<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

 <!--pagenum-->
 <c:if test="${not empty requestScope.list}">
  <div id="J_Filter" class="filter yt-wrap clearfix">
  <div class="f-line top">
     	<div class="f-sort">
	       <a id="default"   class="fSort  ${(searchParams.orders=='recDate,desc' || searchParams.orders=='recDate,asc') ? 'fSort-cur':''}"
		   href="javascript:void(0);" >综合<i <c:choose><c:when test="${searchParams.orders=='recDate,asc'}">class='f-ico-arrow-u'</c:when><c:otherwise>class='f-ico-arrow-d'</c:otherwise></c:choose> ></i></a>
		   
		   <a id="comments"   class="fSort ${searchParams.orders== 'comments,desc' ||  searchParams.orders=='comments,asc'? 'fSort-cur':''}"
		   href="javascript:void(0);"  title="人气">人气<i <c:choose><c:when test="${searchParams.orders=='comments,asc'}">class='f-ico-arrow-u'</c:when><c:otherwise>class='f-ico-arrow-d'</c:otherwise></c:choose> ></i></a>
		   
		   <a id="buys" class="fSort ${searchParams.orders== 'buys,desc' || param.orders=='buys,asc'? 'fSort-cur' :''}"
		   href="javascript:void(0);" title="销量">销量<i <c:choose><c:when test="${searchParams.orders=='buys,asc'}">class='f-ico-arrow-u'</c:when><c:otherwise>class='f-ico-arrow-d'</c:otherwise></c:choose> ></i></a>
		   
		   <a id="cash" class="fSort ${searchParams.orders== 'cash,desc' || searchParams.orders=='cash,asc'? 'fSort-cur' :''}"
		   href="javascript:void(0);" title="价格">价格<i <c:choose><c:when test="${searchParams.orders=='cash,asc'}">class='f-ico-arrow-u'</c:when><c:otherwise>class='f-ico-arrow-d'</c:otherwise></c:choose>></i></a>
	   </div>
	   <div id="J_selectorPrice" class="f-price">
						<div class="f-price-set">
							<div class="fl"><input type="text" class="input-txt" id="queryStartPrice" value="${searchParams.startPrice}" autocomplete="off" style="color: rgb(51, 51, 51);" placeholder="¥"></div>
							<em>-</em>
							<div class="fl"><input type="text" class="input-txt" id="queryEndPrice" value="${searchParams.endPrice}" autocomplete="off" style="color: rgb(51, 51, 51)" placeholder="¥"></div>
						</div>
						<div class="f-price-edit">
							<a href="javascript:emptyDate();" class="item1 J-price-cancle" >清空</a>
							<a href="javascript:confirmSubmit();" class="item2 J-price-confirm">确定</a>
						</div>
					</div>
			   <p class="ui-page-s">
			    <b class="ui-page-s-len" style="float: left">共<font color="#333">${total}</font>件相关商品</b>   
		           <b class="ui-page-s-len"><font color="#e4393c" style="font-weight: 700">${curPageNO}</font>/${toatalPage}</b>
			       <c:choose>
						 <c:when test="${curPageNO<=1}">
									       <b class="ui-page-s-prev" title="上一页">&lt;</b>
									    </c:when>
									    <c:otherwise>
									     <a title="上一页" class="ui-page-s-prev" onclick="javascript:pager('${curPageNO-1}');" href="javascript:void(0);">&lt;</a>
									    </c:otherwise>
									  </c:choose>
									  <c:choose>
									    <c:when test="${requestScope.pageCount<=curPageNO}">
									       <b class="ui-page-s-next" title="下一页">&gt;</b>
									    </c:when>
									    <c:otherwise>
									       <a title="下一页" class="ui-page-s-next" onclick="javascript:pager('${curPageNO+1}');" href="javascript:void(0);" >&gt;</a>
							 </c:otherwise>
					</c:choose>
					
			     </p>  
	     </div>     
   </div>
</c:if>
<!--pagenum end-->

<div class="yt-wrap clearfix ">
  <div class="p-list col4">
       <c:choose>
			<c:when test="${not empty requestScope.list}">
			     <c:forEach items="${requestScope.list}" var="prodDetail" varStatus="status" >
			     	
			         <div class="p-listInfo">
						<div class="plistinfoboxin ">
						    <div class="plistinfoboxin">
						      	<div class="p-listImgBig">						      		
									<a title="${prodDetail.name}" target="_blank" href="<ls:url address='/views/${prodDetail.prodId}'/>">
	                                	<input type="hidden" value="${prodDetail.prodId }" class="prodId"/>
	                                	<img style="max-width: 240px;max-height: 270px;"  src="<ls:images item='${prodDetail.pic}' scale='0'/>"> 	                                	
	                                </a>	                                	                                
								</div>
								<div class="p-listPrice">
									 <strong class="y-p">￥<fmt:formatNumber type="cash" value="${prodDetail.cash}" pattern="#0.00"/></strong>
								</div>
								<div class="p-listTxt">
									 <p>
	                                  <a  title="${prodDetail.name}" target="_blank" href="<ls:url address='/views/${prodDetail.prodId}'/>">${prodDetail.name}</a>
	                                 </p>
								</div>
								<!-- <div class="p-listCustomIcon">新款到货，9折优惠</div> -->
								<%--<div class="p-buy">
									  <a class="ui-btn-addcart" href="<ls:url address='/views/${prodDetail.prodId}'/>">加入购物车</a>  
	                                 <a class="ui-btn-addfocus" href="javascript:alertAddInterestDiag(${prodDetail.prodId});">收藏</a>
								</div>
								--%> 
						    </div>
						</div>
				     </div>
			     </c:forEach>
			</c:when>
			<c:otherwise>
			  <div class="selection_box search_noresult">
			     <p class="search_nr_img"><img src="<ls:templateResource item='/resources/templets/images/not_result.png'/>"></p> 
			    <p class="search_nr_desc">没有找到<c:if test="${not empty param.keyword}">与“<span>${searchParams.keyword}</span>”</c:if>相关的商品</p>
			   </div>
			</c:otherwise>
	   </c:choose>
	</div>
	
	


<!--list end-->
<!--page-->
	
</div>
<!--分页-->

<div style="margin:-13px 483px 30px 0px;float: right" class="page yt-wrap clearfix">
	<div class="p-wrap">
		<!--    <span class="p-num">
               <a class="pn-prev disabled"><i>&lt;</i><em>上一页</em></a>
               <a class="curr" href="#">1</a>
               <b class="pn-break hide">…</b>
               <a class=" " href="#">2</a>
               <a class=" " href="#">3</a>
               <b class="pn-break">…</b>
               <a href="#">31</a>
               <a class="pn-next" href="#">下一页<i>&gt;</i></a>
           </span> -->
		<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>

	</div>
</div>


<!--分页 end-->
<form:form   action="${contextPath}/prodlist" method="post" id="list_form">
	<input type="hidden" id="curPageNO" name="curPageNO"  value="${_page}"/>
	<input type="hidden"  id="categoryId" name="categoryId" value="${searchParams.categoryId}" />
	<input type="hidden" id="orders" name="orders"  value="${searchParams.orders}"/>
	<input type="hidden" id="hasProd" name="hasProd"  value="${searchParams.hasProd}" />
    <input type="hidden" id="keyword" name="keyword"  value="${searchParams.keyword}" />
	<input type="hidden" id="prop" name="prop"  value="${searchParams.prop}" />
	<input type="hidden" name="startPrice" id="startPrice" value="${searchParams.startPrice}" />
	<input type="hidden" name="endPrice"  id="endPrice" value="${searchParams.endPrice}" />
	<input type="hidden" name="tag"  value="${searchParams.tag}" />
</form:form>
	

<script type="text/javascript">
	var photoPath="<ls:photo item= ''/>";
</script>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/productsort.js'/>"></script>

