<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/interestOverlay.css'/>" rel="stylesheet"/>


<title>已收藏</title>
</head>
<body>
	<div class="tips" id="has">
	   <h2><span id="error_msg"><font color="#ff8000">已收藏过该商品！</font></span></h2>
	    <p style="margin-top: 0px; margin-bottom: 5px;">
	    	<em>您已收藏了<span id="productCount">${favoriteLength}</span>个商品，
		    <a target="_blank" href="${contextPath}/p/favourite">
		       <font color="blue">查看我的收藏&gt;&gt;</font>
		     </a>
		     </em>
	   </p>
   </div>
   	<c:if test="${not empty requestScope.products}">
   	<div class="warp" style="height: 10px;">
		<div id="attention-reco" style="width: 415px;">
		    <div class="scroll-title" style="margin-top: 10px; border-top-width: 1px; height: 30px;"> 
		           <h4 style="margin-top: 15px;">收藏此商品的人还收藏</h4> 
		    </div>    
		<div class="scroll"> 
	        <div style="width: 405px; position: relative; overflow: hidden; height: 325px; margin-top: 5px;" id="scroll-inner">    
	           <ul style="position: absolute; width: 405px; height: 315px; top: 0px; left: -5px; padding-left: 0px; right: 0px;">   
	           	  <c:forEach items="${requestScope.products}" var="product">
	               <li class="fore1" style="width: 135px;margin-bottom: 5px;"">  
	                    <div class="p-img">
	                    	 <a target="_blank" title="${product.prodName}" href="${contextPath}/views/${product.prodId}"> 
	                       		<img  src="<ls:images item='${product.pic}' scale='3' />"></a>   
	                      </div>              
	                    <div class="p-name" style="padding-left: 15px;">
	                          <a target="_blank"  href="${contextPath}/views/${product.prodId}">${product.prodName}</a>
	                     </div>   
	                     <div class="p-price"> 
	                          <strong class="J-p-977998">￥${product.cash}</strong>
	                     </div>
	                     <div class="p-btn"> 
	                           <a href="#" onclick="addMarks('${product.prodId}')" class="btn-gray">加收藏</a>
	                      </div> 
	                 </li>   
	                </c:forEach>          
	             </ul>
	           </div>       
	   	</div>
	   </div>          
	</div>
	</c:if>
</body>
</html>
<script type="text/javascript">
	function addMarks(id){
		jQuery.ajax({
			url:"${contextPath}/p/addMarks",
			data: {"prodId":id},
			type:'post', 
			dataType : 'json', 
			async : true, //默认为true 异步   
			success:function(result){
			  if('OK' == result){
				  layer.msg("收藏商品成功!",{icon:1});
			  }else{
				  layer.msg("您已经收藏过该商品!",{icon:0});
			  }
			}
			});
	}
</script>
