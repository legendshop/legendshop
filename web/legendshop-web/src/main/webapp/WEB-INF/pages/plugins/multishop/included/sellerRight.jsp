<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
		
<!-- 热卖商品排行 -->
			<div class="siade-nav">
				 <c:choose>
	               	<c:when test="${empty shopOrderBill}">
		               	<div class="shop-balance">
			               	<ul>
								<li class="this-time">本次结算</li>
								<li>总销售金额：<span>0元</span></li>
								<li>总退单金额：<span>0元</span></li>
								<li>总订单金额：<span>0元</span></li>
								<li>应结算金额：<span>0元</span></li>
								<li class="bot">
									<a href="${contextPath}/s/shopOrderBill/query" class="now">马上结算</a>
									<a href="${contextPath}/s/shopOrderBill/query" class="before">已结算账单</a>
								</li>
							</ul>
							</div>
	               	</c:when>
	               	<c:otherwise>
	               			<div class="shop-balance">
	               				<ul>
									<li class="this-time">本次结算</li>
									<li>总销售金额：<span><fmt:formatNumber  pattern="#.##" value="${shopOrderBill.orderAmount}" />元</span></li>
									<li>总退单金额：<span>${shopOrderBill.orderReturnTotals}元</span></li>
									<li>总订单佣金：<span>${shopOrderBill.commisTotals}元</span></li>
									<li>应结算金额：<span><fmt:formatNumber  pattern="#.##" value="${shopOrderBill.resultTotalAmount}" />元</span></li>
										<li class="bot" id="shop-order-bill">
											<a href="${contextPath}/s/shopOrderBill/load/${shopOrderBill.id}" class="now">马上结算</a>
											<a href="${contextPath}/s/shopOrderBill/query" class="before">已结算账单</a>
										</li>
								</ul>
	               			</div>
	               	</c:otherwise>
               	</c:choose>
<!-- 本次结算 -->
				<div class="customer-service">
				<!-- IM客服替换qq客服 -->
					<%-- <span>平台客服</span>
					<p style="overflow: hidden;">
						<c:forEach items="${qqNumberList}" var="qq">
						
							<a href="http://wpa.qq.com/msgrd?v=3&uin=${qq}&site=qq&menu=yes"  style="width: 76px;" class="talk-fir"><img src="<ls:templateResource item='/resources/templets/images/qq-tal.png'/>" alt=""></a>
						
						</c:forEach>
					</p> --%>
					
					<!-- IM客服入口  -->
					 <a target="_blank" href="${contextPath}/p/im/customerAdmin/0"
                   		style="width:100px;height:26px;background: #e5004f;line-height:26px;display: inline-block;text-align: center;color: #fff;border-radius: 3px;">
                    	<i style="background: url('${contextPath}/resources/templets/images/chat-i.png');width: 19px;height: 18px;display: inline-block;vertical-align: middle;margin-right: 5px;margin-bottom: 2px;"></i>平台客服
               		 </a>
					<p style="margin-top: 10px;">客服电话：${telephone}</p>
				</div>
<!-- 平台客服 -->
				<c:if test="${not empty pubList}">
					<div class="office-mes">
							<h4>官方信息中心</h4>
						<ul>
							<c:forEach items="${pubList}" var="pub" varStatus="index">
									<li><a href="${contextPath}/pub/${pub.id}" title="${pub.title}" target="_blank">${pub.title}</a><span><fmt:formatDate value="${pub.recDate}" pattern="MM-dd"/></span></li>
							</c:forEach>
						</ul>
					</div>
				</c:if>
<!-- 官方信息中心 -->
			</div>