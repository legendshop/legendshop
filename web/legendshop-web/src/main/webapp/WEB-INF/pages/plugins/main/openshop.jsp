<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
  <title>商家开店-${systemConfig.shopName}</title>
  <meta name="keywords" content="${systemConfig.keywords}"/>
  <meta name="description" content="${systemConfig.description}" />
  <link rel="stylesheet" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
</head>
 <%@ include file="home/top.jsp" %>

<div class="w"> 
    <div class="login_left wrap">
      <div class="news_wrap">
         <div class="news_bor">
         <div class="loginwrap">
        <div class="leftr">
           <img id="tempimg" src="" style="display:none" />
           <form:form  action="${contextPath}/p/doOpenShop" method="post" name="userRegForm" id="userRegForm" enctype="multipart/form-data">
           <input type="hidden" name="shopId" value="${shopDetail1.shopId}">
           <div class="formtable" style="text-align:center;">
           <div>
           <c:if test="${not empty shopDetail1}">
	           	<table   style="margin:0 auto;font-size: 12px;" width="700px">
	           			<th width="150"><strong>审核意见：</strong></th><td>
			           <c:choose>
			           		<c:when test="${empty shopDetail1 || shopDetail1.status==-2}">
			           			 <c:if test="${not empty shopDetail1.auditOpinion}">
						           			审核不通过， 原因是<br>
						           			<p style="color: red">${shopDetail1.auditOpinion}</p>
				          		</c:if>
							</c:when>
			           		<c:when test="${shopDetail1.status==-1}">
			           			审核中
			           		</c:when>
			           		<c:when test="${shopDetail1.status==-3}">
			           			您的店铺已被关闭，请联系客服
			           		</c:when>
			           		<c:when test="${shopDetail1.status==0}">
			           			您的店铺已下线
			           		</c:when>
			           </c:choose>
	          	</td></tr>
				</table>
			</c:if>
			
			 <c:if test="${empty shopDetail1}">
				 <table   style="margin:0 auto;font-size: 12px" width="700px">
	           			<th width="150"></th>
	           			<td style="color: red;">
			           您尚未开店哦，填写以下材料马上申请店铺，审核时间为1-3个工作日。
	          	</td></tr>
				</table>
			</c:if>
          	<table id="personalTable" style="margin:0 auto;font-size: 12px" width="700px">
						<tbody>
							<th width="150" ><strong>联系人信息：</strong>
							</th>
							<td>&nbsp;</td>
							<td class="hint">&nbsp;</td>
							</tr>
							<tr>
								<th width="150" ><i class="redstar">*</i>联系人姓名：</th>
								<td><input id="contactName" name="contactName" type="text" value="${shopDetail1.contactName}" autocomplete="off" class="inputstyle" maxlength="50" />
								</td>
								<td class="hint"></td>
							</tr>
							<tr>
								<th><i class="redstar">*</i>手机：</th>
								<td><input id="contactMobile" name="contactMobile" value="${shopDetail1.contactMobile}" type="text" autocomplete="off" class="inputstyle" maxlength="20"/>
								</td>
								<td class="hint">&nbsp;</td>
							</tr>
							<tr>
								<th>固定电话：</th>
								<td><input id="contactTel" name="contactTel" type="text" value="${shopDetail1.contactTel}" autocomplete="off" class="inputstyle" maxlength="20" />  格式为：020-12345678
								</td>
								<td class="hint">&nbsp;</td>
							</tr>
							<tr>
								<th>联系人邮箱：</th>
								<td><input id="contactMail" value="${shopDetail1.contactMail}" name="contactMail" type="text" autocomplete="off" class="inputstyle"  maxlength="100"/>
								</td>
								<td class="hint">&nbsp;</td>
							</tr>
							<tr>
								<th>QQ号码：</th>
								<td><input id="contactQQ" name="contactQQ" value="${shopDetail1.contactQQ}" type="text" autocomplete="off" class="inputstyle"  maxlength="50" onkeyup="value=value.replace(/[\uFF00-\uFFFF]/g,'')"  onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\uFF00-\uFFFF]/g,''))"/>
									     多个QQ号码请用英文逗号“,”隔开
								</td>
								<td class="hint">&nbsp;</td>
							</tr>
						</tbody>
					</table>

					<!--企业用户-->
					<table id="businessTable" style="margin:0 auto;font-size: 12px;" width="700px">
						<tbody>
							<tr>
								<th width="150"><strong>商城信息：</strong>
								</th>
								<td></td>
								<td class="hint">&nbsp;</td>
							</tr>
							<tr>
								<th><i class="redstar">*</i>店铺名字：</th>
								<td><input id="siteName" name="siteName" type="text" value="${shopDetail1.siteName}" class="inputstyle"  maxlength="50"/> 注意:店铺名不能为空且不能重复
								</td>
								<td class="hint">&nbsp;</td>
							</tr>
							<tr>
								<th>店铺地址：</th>
								<td><input id="shopAddr" name="shopAddr" type="text" value="${shopDetail1. shopAddr}" class="inputstyle"  maxlength="300"/>&nbsp; 
								</td>
								<td class="hint">&nbsp;</td>
							</tr>
						<%-- 	<tr>
								<th>公司名字：</th>
								<td><input id="companyName" name="companyName" value="${shopDetail1.companyName}" type="text" class="inputstyle" maxlength="50"/>
								</td>
								<td class="hint">&nbsp;</td>
							</tr> --%>
							<tr>
								<th>公司地址：</th>
								<td><select class="combox sele" id="provinceid" name="provinceid" requiredTitle="true" childNode="cityid" selectedValue="${shopDetail1.provinceid}"
									retUrl="${contextPath}/common/loadProvinces">
								</select> <select class="combox sele" id="cityid" name="cityid" requiredTitle="true" selectedValue="${shopDetail1.cityid}" showNone="false" parentValue="${shopDetail1.provinceid}"
									childNode="areaid" retUrl="${contextPath}/common/loadCities/{value}">
								</select> <select class="combox sele" id="areaid" name="areaid" requiredTitle="true" selectedValue="${shopDetail1.areaid}" showNone="false" parentValue="${shopDetail1.cityid}"
									retUrl="${contextPath}/common/loadAreas/{value}">
								</select></td>
								</td>
								<td class="hint">&nbsp;</td>
							</tr>
							<tr>
								<th>公司人数：</th>
								<td><select id="companyNum" name="companyNum" class="sele">
										<ls:optionGroup type="select" required="false" cache="true" beanName="COMPANY_NUM" selectedValue="${shopDetail1.companyNum}" />
								</select></td>
								<td class="hint">&nbsp;</td>
							</tr>
							<tr>
								<th>公司行业：</th>
								<td><select id="companyIndustry" name="companyIndustry" class="sele">
										<ls:optionGroup type="select" required="false" cache="true" beanName="COMPANY_INDUSTRY" selectedValue="${shopDetail1.companyIndustry}" />
								</select></td>
								<td class="hint">&nbsp;</td>
							</tr>
							<tr>
								<th>公司性质：</th>
								<td><select id="companyProperty" name="companyProperty" class="sele">
										<ls:optionGroup type="select" required="false" cache="true" beanName="COMPANY_PROPERTIES" selectedValue="${shopDetail1.companyProperty}" />
								</select></td>
								<td class="hint">&nbsp;</td>
							</tr>


							<tr>
								<th>商城类型：</th>
								<td>
								<c:choose>
									<c:when test="${shopDetail1.type==0}">
										<fmt:message key="type.individual" /> <input type="radio" checked="checked" name="type" value="0" onclick="javascript:changeType(0)" />
									</c:when>
									<c:when test="${shopDetail1.type==1}">
										<fmt:message key="type.business" /> <input type="radio" checked="checked" name="type" id="type" value="1" onclick="javascript:changeType(1)" />&nbsp; 
									</c:when>
									<c:when test="${empty shopDetail1.type }">
										<fmt:message key="type.business" /> <input type="radio" checked="checked"  name="type" id="type" value="1" onclick="javascript:changeType(1)" />&nbsp; 
										<fmt:message key="type.individual" /> <input type="radio"  name="type" value="0" onclick="javascript:changeType(0)" />
									</c:when>
								</c:choose>
								</td>
								<td class="hint">&nbsp;</td>
							</tr>
							<tr>
								<th><i class="redstar">*</i>身份证号码：</th>
								<td><input id="idCardNum" name="idCardNum" type="text" value="${shopDetail1.idCardNum}" class="inputstyle" maxlength="20" />
								</td>
								<td class="hint">&nbsp;</td>
							</tr>
							<tr>
								<th><i class="redstar">*</i>上传身份证正面：</th>
								<td><c:if test="${not empty shopDetail1.idCardPic}"><a target="_blank" href="<ls:photo item='${shopDetail1.idCardPic}' />"><img width="50" height="50" src="<ls:photo item='${shopDetail1.idCardPic}' />"></a></c:if>
								<input id="idCardPicFile" oldFile="${shopDetail1.idCardPic}" name="idCardPicFile" type="file" class="inputstyle" />
								</td>
								<td class="hint">&nbsp;</td>
							</tr>
							<tr>
								<th><i class="redstar">*</i>上传身份证反面：</th>
								<td><c:if test="${not empty shopDetail1.idCardBackPic}"><a target="_blank" href="<ls:photo item='${shopDetail1.idCardBackPic}' />"><img width="50" height="50" src="<ls:photo item='${shopDetail1.idCardBackPic}' />"></a></c:if>
								<input id="idCardBackPicFile" oldFile="${shopDetail1.idCardBackPic}" name="idCardBackPicFile" type="file" class="inputstyle" /><span style="color:#a2a2a2;position:absolute;margin-left:10px;">图片仅支持JPG、GIF、PNG、JPEG、BMP格式<br>大小不超过512K</span>
								</td>
								<td class="hint">&nbsp;</td>
							</tr>
							<c:if test="${shopDetail1.type ne 0 }">
								<tr id="trafficPicDiv">
									<th><i class="redstar">*</i>上传营业执照照片：</th>
									<td><c:if test="${not empty shopDetail1.trafficPic}"><a target="_blank" href="<ls:photo item='${shopDetail1.trafficPic}' />"><img width="50" height="50" src="<ls:photo item='${shopDetail1.trafficPic}' />"></a></c:if>
									<input id="trafficPicFile" oldFile="${shopDetail1.trafficPic}" name="trafficPicFile" type="file" class="inputstyle" />
									</td>
									<td class="hint">&nbsp;</td>
								</tr>
							</c:if>
						</tbody>
					</table>
					<c:if test="${empty shopDetail1 || shopDetail1.status!=-1}">
					<table style="margin:0 auto;font-size: 12px;" width="700px">
						<tbody>
							<tr>
								<th width="150">验证码：</th>
								<td><input type="text" id="randNum" name="randNum" class="inputstyle" maxlength="4" style="font-size:11pt; width: 50px;height: 22px" tabindex="3"> <label for="randNum"
									style="display: none;" id="errorImage" class="error"><fmt:message key="error.image.validation" />
								</label> <img id="randImage" name="randImage" src="<ls:templateResource item='/validCoderRandom'/>" style="vertical-align: middle;" /> &nbsp;<a href="javascript:void(0)"
									onclick="javascript:changeRandImg('${contextPath}')" style="font-weight: bold;"><fmt:message key="change.random2" />
								</a></td>
								<td class="hint">&nbsp;</td>
							</tr>
						</tbody>
					</table>
						<table style="margin:0 auto;font-size:12px; width: 700px;">
							<tbody>
								<tr>
									<th style="height:20px;width:70px;">&nbsp;</th>
									<td>
										<div id="item">
											<span class="label">&nbsp;</span>
											<div class="fl item-ifo">
												<input class="checkbox" checked="checked" id="readme" type="checkbox"> <label>我已阅读并同意<a href="#" class="blue" id="protocol">《用户注册协议》</a>
												</label> <span class="clr"></span> <label id="protocol_error" class="error hide">请接受服务条款</label>
											</div>
										</div></td>
								</tr>
								<tr>
									<th height="73">&nbsp;</th>
									<td>
										<div class="btnt">
											<input class="btn-img" id="registnow" value="立即注册" type="submit" style="width:270px; height:36px;">
										</div></td>
								</tr>
	
							</tbody>
	
						</table>
					</c:if>
          	 </div>
           </div>
        </form:form>
        </div>     
        
        <div class="clear"></div>
     </div>   
        </div>
      </div>
    </div>
    <!----左边end---->
    
   <div class="clear"></div>
    <%@ include file="home/bottom.jsp" %>
</div>
</html>
<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>"  type="text/javascript"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/openshop.js'/>"></script>
<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
 <script language="javascript">
	 var contextPath = '${contextPath}';
	
  	jQuery.validator.addMethod("stringCheck", function(value, element) {
     return value.isAlpha();}, '<fmt:message key="user.reg.username"/>'); 

   jQuery.validator.addMethod("checkRandImage", function(value, element) {
	 return checkRandImage(value);}, '<fmt:message key="error.image.validation"><fmt:param value=""/></fmt:message>'); 
   
   jQuery.validator.addMethod("checkFile", function(value, element) {
		 return checkFile(value,element);}, '<fmt:message key="errors.required"><fmt:param value=""/></fmt:message>'); 
   
   
   function checkFile(val,element){
	   if(val=="" && $(element).attr("oldFile")==""){
		   return false;
	   }
	   return true;
   }
     
     
  function isMobile(str){
	var reg = /^1\d{10}$/;
	return reg.test(str);
	}
	function isTel(str){
	var reg = /^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/;
	return reg.test(str);
	}
	function isIdCard(str){
	var reg = /(^\d{15}$)|(^\d{17}([0-9]|X)$)/;
	return reg.test(str);
	}
	
jQuery.validator.addMethod("isMobile", function(value, element) {
     return isMobile(value);}, '请输入正确的手机号码'); 
jQuery.validator.addMethod("isTel", function(value, element) {
	if(value!=null&&value!=''){
     	return isTel(value);
     	}else{
     		return true
     	}
     }, '请输入正确的固定电话'); 
var oldSiteName = "${shopDetail1.siteName}";
$.validator.addMethod("checkVal",function(value,element){
		var config=true;
			if(value==oldSiteName){
				return config;
			}else{
				$.ajax({
					type: "POST",
					url: "${contextPath}/isSiteNameExist",
					async:false,
					data:{"siteName":value},
					dataType: "json",
					success:function(data){
						if(!data)
							config=false;
					}
				});
				return config;
			}
	},'店铺名已存在');

$.validator.addMethod("checkSiteName",function(value,element){
	var regex = new RegExp("^([a-zA-Z0-9_]|[\\u4E00-\\u9FFF])+$", "g");
	//特殊字符过滤
	//var regex = /[`~{}《》<>\.!@#\$\^*()，。、：；——"‘“\[\]【】（）\+\-%&',:;\/=?\\\|]+/;
	if(regex.test(value)){
		return true;
	}else{
		return false;
	}
},'店铺名字只能由汉字、数字、字母、下划线组成');

jQuery.validator.addMethod("isIdCard", function(value, element) {
     return isIdCard(value);}, '请输入正确的身份证号码'); 
	
jQuery(document).ready(function() {
	jQuery("#userRegForm").validate({
		rules: {
			contactName: {
				required: true
			},
			contactMobile: {
				required: true,
				isMobile:true
			},
			contactTel: {
				isTel:true
			},
			contactMail: {
				email:true
			},
			"siteName":  {
				required: true,
				minlength: 2,
				checkVal:true,
				checkSiteName:true
			}, 
			
            "postAddr": {
				minlength: 2
			},
		    "idCardNum": {
				required: true,
				minlength: 15,
				isIdCard:true
			},
		    "type": {
				required: true
			},
		    "idCardPicFile": {
		    	checkFile: true
			},
			"idCardBackPicFile": {
				checkFile: true
			},	
		    "trafficPicFile": {
		    	checkFile: true
			},
			randNum:{
				required: true
			}
			
		},
		messages: {
            contactName: {
                required: '<fmt:message key="errors.required"><fmt:param value=""/></fmt:message>'
            },
            contactMobile: {
                required: '<fmt:message key="errors.required"><fmt:param value=""/></fmt:message>'
            },
            contactMail: {
                email:'<fmt:message key="errors.email"><fmt:param value=""/></fmt:message>'
            },
            
            "siteName": {
               required: '<fmt:message key="errors.required"><fmt:param value=""/></fmt:message>',
               minlength: '<fmt:message key="errors.minlength"><fmt:param value=""/><fmt:param value="2"/></fmt:message>',
            },
            "postAddr":{
               minlength: '<fmt:message key="errors.minlength"><fmt:param value=""/><fmt:param value="2"/></fmt:message>'
            }, 
            "idCardNum": {
               required: '<fmt:message key="errors.required"><fmt:param value=""/></fmt:message>',
               minlength: '<fmt:message key="errors.minlength"><fmt:param value=""/><fmt:param value="15"/></fmt:message>',
            }, 
            "type": {
               required: '<fmt:message key="errors.required"><fmt:param value=""/></fmt:message>'
            },             
 		    "idCardPicFile":  {
				required: '<fmt:message key="errors.required"><fmt:param value=""/></fmt:message>'
			},	
			"idCardBackPicFile":  {
				required: '<fmt:message key="errors.required"><fmt:param value=""/></fmt:message>'
			},
 		    "trafficPicFile": {
				required: '<fmt:message key="errors.required"><fmt:param value=""/></fmt:message>'
			},
			randNum:{
				required: '<fmt:message key="randomimage.errors.required"><fmt:param value=""/></fmt:message>'
			}
        },
        focusInvalid : false,
        onkeyup : false,
        submitHandler : function(form) {
             var validate=validateRandNum(contextPath);
             var select=$("#readme").prop("checked"); 
             if(!validate){
               return false;
             }
              if(!select){
              	layer.alert("您没有阅读并同意《用户注册协议》",{icon: 0});
               return false;
             }
              $("#registnow").val("提交中...");
             $("#registnow").attr("disabled",true); 
            form.submit();
        }
        
	});
	
	var openshop = '${param.openshop}';
 	initReg(openshop);
 	
 	$("input[type=file]").change(function(){
 		checkImgType(this);
		checkImgSize(this,512);
 	});
});
</script>
