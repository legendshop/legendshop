<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.legendesign.net/tags" prefix="ls"%>
<%
  request.setAttribute("contextPath", request.getContextPath());
%>
<table class="ret-tab">
	<tr class="tit">
		<th width="10"></th>
		<th colspan="2">商品/订单号/退货号</th>
		<th width="70">退款金额</th>
		<th width="70">退货数量</th>
		<th width="120">申请时间</th>
		<th width="80">处理状态</th>
		<th width="80">平台确认</th>
		<th width="90">操作</th>
    </tr>
    
  	<c:choose>
         <c:when test="${empty requestScope.list}">
            <tr>
			 	 <td colspan="20">
			 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
			 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
			 	 </td>
		    </tr>
         </c:when>
         <c:otherwise>
           <c:forEach items="${requestScope.list }" var="refundReturn">
    		<tr class="detail">
				<td width="10"></td>
				<td width="60" style="margin-right:5px;margin-left:5px;">
					<div class="pic-thumb">
						<a href="${pageContext.request.contextPath}/views/${refundReturn.productId}">
							<img src="<ls:images item='${refundReturn.productImage}' scale='3'/>"/>
						</a>
					</div>
				</td>
				<td style="text-align: left;padding-right: 10px;padding-left: 10px;width: 208px;">
					<dl class="goods-name">
					    <dt><a href="${pageContext.request.contextPath}/views/${refundReturn.productId}">${refundReturn.productName }</a></dt>
			        	<dd>订单编号：<a href="${pageContext.request.contextPath}/p/orderDetail/${refundReturn.subNumber }">${refundReturn.subNumber }</a></dd>
			        	<dd>
			        	退货编号：${refundReturn.refundSn}
			        	<!-- IM商城客服入口 --> 
						<span style="margin-left: 15px;">
							<a target="_blank" style="color: #006dc7;" href="${contextPath}/p/im/customerShop/${refundReturn.refundSn}/${refundReturn.shopId}">联系商家</a>
						</span>
			        	</dd>
			        </dl>
			    </td>
			    <td>
			    	<c:choose>
				    	<c:when test="${refundReturn.isRefundDeposit}">
				    		¥${refundReturn.refundAmount + refundReturn.depositRefund}
				    	</c:when>
				    	<c:otherwise>
				    		¥${refundReturn.refundAmount}
				    	</c:otherwise>
			    	</c:choose>
			    </td>
			    <td>x ${refundReturn.goodsNum }</td>
			    <td><fmt:formatDate value="${refundReturn.applyTime}" pattern="yyyy-MM-dd HH:mm" /></td>
			    <td>
			    	<c:if test="${refundReturn.sellerState eq 1 && refundReturn.applyState eq -1}">
			    		已撤销
			    	</c:if>
			    	<c:if test="${refundReturn.sellerState eq 1 && refundReturn.applyState eq 1}">
			    		待审核
			    	</c:if>
			    	<c:if test="${refundReturn.sellerState eq 2}">
			    		<c:if test="${refundReturn.returnType eq 2}">
			    			<c:if test="${refundReturn.goodsState eq 1}">
			    				待退货
			    			</c:if>
			    			<c:if test="${refundReturn.goodsState eq 2}">
			    				待商家收货
			    			</c:if>
			    			<c:if test="${refundReturn.goodsState eq 3}">
			    				商家未收到
			    			</c:if>
			    			<c:if test="${refundReturn.goodsState eq 4}">
			    				商家已收货
			    			</c:if>
			    		</c:if>
			    		<c:if test="${refundReturn.returnType eq 1}">
								商家弃货
			    		</c:if>
			    	</c:if>
			    	<c:if test="${refundReturn.sellerState eq 3}">
			    		不同意
			    	</c:if>
			    </td>
			    <td>
				    <c:choose>
				    	<c:when test="${refundReturn.applyState eq 2}">
				    		待平台确认
				    	</c:when>
		    			<c:when test="${refundReturn.applyState eq 3}">
		    				已完成
		    			</c:when>
		    			<c:otherwise>
		    				无
		    			</c:otherwise>
				    </c:choose>
			    </td>
			    <td style="text-align:left;">
			    	<a href="<ls:url address="/p/returnDetail/${refundReturn.refundId}"/>" class="btn-r">查看</a>
			    	<c:choose>
			    		<c:when test="${refundReturn.sellerState eq 2 and  refundReturn.returnType eq 2 and refundReturn.goodsState eq 1}">
					    	<a href="${pageContext.request.contextPath}/p/returnDetail/${refundReturn.refundId}" class="btn-g" style="color:#666;">退货</a>
					    </c:when>
					    <c:when test="${refundReturn.sellerState eq 1 && refundReturn.applyState eq 1}">
					    	<a href="javascript:void(0);" class="btn-g" onclick="candelAction('${refundReturn.refundSn}');" style="color:#666;">撤销</a>
					    </c:when>
			    	</c:choose>
			    </td>
			</tr>
    	</c:forEach>
      </c:otherwise>
    </c:choose>			
</table>
<!-- 页码条 -->
<div style="margin-top:10px;" class="page clearfix">
	<div class="p-wrap">
  		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
	 </div>
</div>