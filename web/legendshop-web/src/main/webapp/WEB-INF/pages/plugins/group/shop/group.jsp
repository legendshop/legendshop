<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>团购活动管理-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/groupon${_style_}.css'/>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>
<body class="graybody">
<input type="hidden" id="prodStocks_p2" value="${productDetail.stocks}"/>
	<div id="doc">
	<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="#">首页</a>>
					<a href="#">卖家中心</a>>
					<lable class="on">添加团购活动</lable>
				</p>
			</div>
			<%@ include file="/WEB-INF/pages/plugins/multishop/included/sellerLeft.jsp" %>
			
			<div class="group-right">
			<form action="${contextPath}/s/group/save" method="post" id="form1" enctype="multipart/form-data">
				
				<input type="hidden" id="id" name="id" value="${group.id}"/>
				<!-- 基本信息 -->
				<div class="basic-ifo">
					<!-- 基本信息头部 -->
					<div class="ifo-top">
						基本信息
					</div>
					<div class="ifo-con">
						<li style="margin-bottom: 5px;">
							<span class="input-title"><span class="required">*</span>活动名称：</span>
							<input type="text" class="active-name" name="groupName" id="groupName" value="${group.groupName}" maxLength="25" placeholder="请填写活动名称"><em></em>
						</li>
						<li>
							<span class="input-title"></span>
							<span style="color: #999999;margin-top: 10px;">活动名称最多为25个字符</span>
						</li>
						<li style="margin-bottom: 0;">
							<span class="input-title"><span class="required">*</span>活动时间：</span>
							<span><input type="text" class="active-time" readonly="readonly"  name="startTime"  id="startTime" value='<fmt:formatDate value="${group.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>' placeholder="开始时间"/><em></em></span>
							<span style="margin: 0 5px;">-</span> 
							<span><input type="text" class="active-time" readonly="readonly"  name="endTime"  id="endTime" value='<fmt:formatDate value="${group.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>' placeholder="结束时间"/><em></em></span>
						</li>
					</div>
				</div>
				<!-- 选择活动商品 -->
				<div class="basic-ifo" style="margin-top: 20px;">
					<!-- 选择活动商品头部 -->
					<div class="ifo-top">
						选择活动商品
					</div>
					<div class="ifo-con">
						<div class="red-btn" onclick="showProdlist()">选择商品</div>
						<div class="check-shop prodList">
							<c:if test="${not empty prodLists}">
								<table border="0" id="skuList" cellpadding="0" cellspacing="0" style="margin: 30px 0;">
									<tbody>
									<tr style="background: #f9f9f9;">
										<td width="70px">
											<label class="checkbox-wrapper">
								<span class="checkbox-item">
									<input type="checkbox" id="checkbox" class="checkbox-input selectAll">
									<span class="checkbox-inner"></span>
								</span>
											</label>
										</td>
										<td width="70px">商品图片</td>
										<td width="170px">商品名称</td>
										<td width="100px">商品规格</td>
										<td width="100px">商品价格</td>
										<td width="100px">可销售库存</td>
										<td width="100px">团购价(元)</td>
									</tr>
									<c:choose>
										<c:when test="${not empty prodLists}">
											<c:forEach items="${prodLists}" var="SkuList">
												<c:forEach items="${SkuList.dtoList}" var="groupSku">
													<tr class="sku" >
													<td>
														<label class="checkbox-wrapper">
								<span class="checkbox-item">
									<input type="checkbox" name="array" class="checkbox-input selectOne" onclick="selectOne(this);">
									<span class="checkbox-inner"></span>
								</span>
														</label>
													</td>
													<td>
														<img src="<ls:images item='${groupSku.prodPic}' scale='3' />" >
													</td>
													<td>
														<span class="name-text">${groupSku.prodName}</span>
													</td>
													<td>
														<span class="name-text">${empty groupSku.cnProperties?'暂无':groupSku.cnProperties}</span>
													</td>
													<td>
														<span class="name-text">${groupSku.skuPrice}</span>
													</td>
													<td class="prodStocks_p1">${groupSku.skuStocks}</td>
													<td>
														<input type="text" class="set-input groupPrice" id="groupPrice" name="groupPrice" style="text-align: center" value="${groupSku.groupPrice}">
														<input type="hidden" class="groupProdId"  value="${groupSku.prodId}">
														<input type="hidden" class="groupSkuId" value="${groupSku.skuId}">
														<input type="hidden" class="groupSkuPrice"  value="${groupSku.skuPrice}">
													</td>
												</tr>
												</c:forEach>
											</c:forEach>

										</c:when>
										<c:otherwise>
											<tr class="first">
												<td colspan="7" >暂未选择商品</td>
											</tr>
										</c:otherwise>
									</c:choose>
									</tbody>
								</table>
							</c:if>
						</div>
						<div class="check-bto" <c:if test="${empty prodLists}">style="display: none;margin:-11px 54px 0 0;float: right"</c:if> <c:if test="${not empty prodLists}">style="display: inline-block;margin: -11px 54px 0 0;float: right"</c:if>>
							批量设置团购价格: <input type="text" id="batchPrice" maxlength="10">元
							<div class="red-btn" style="display: inline-block;margin-left: 10px" id="confim">确定</div>
						</div>
					</div>
					<input type="hidden" name="productId" id="productId" value="${group.productId}"/>
				</div>
				<!-- 活动规则 -->
				<div class="basic-ifo" style="margin-top: 20px;">
					<!-- 活动规则头部 -->
					<div class="ifo-top">
						活动规则
					</div>
					<div class="ifo-con">
						<li>
							<span class="input-title"><span class="required">*</span>活动图片：</span>
							<input type="file" name="groupPicFile"  id="groupPicFile" style="display: inline;color: #999999;"/><em></em>
						</li>
						<c:if test="${not empty group.groupImage}">
							<span class="input-title"><span class="required"></span>原有图片：</span>
							<img  src="<ls:photo item='${group.groupImage}'/>" style="max-height: 100px;max-width: 100px;vertical-align: middle;">
						</c:if>
						<li>
							<span class="input-title"><span class="required">*</span>每人限购：</span>
							<span class="act-limit">
								<input type="text" class="active-time" maxLength="5" onchange="checkInputNumForNormal(this,0);" onkeyup="checkInputNumForNormal(this,0);" name="buyQuantity" id="buyQuantity" value="${group.buyQuantity}" placeholder="此次活动可以购买的商品总数量"/>
								<%-- <input type="text" class="active-time" name="buyQuantity" id="buyQuantity" value="${group.buyQuantity}" maxlength="5" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')" placeholder="此次活动可以购买的商品总数量"> --%>
								<span style="color: #999999;"> 注:为0则不限制购买 </span><em></em>
							</span>
						</li>
<%--						<li>--%>
<%--							<span class="input-title"><span class="required">*</span>团购价格：</span>--%>
<%--							<span class="act-limit">--%>
<%--								<input type="text" class="active-time"  maxLength="8" onchange="checkInputNumForNormal(this,2);" onkeyup="checkInputNumForNormal(this,2);" name="groupPrice" id="groupPrice" value="${group.groupPrice}" placeholder="请填写团购价格"/><em></em>--%>
<%--							</span>--%>
<%--						</li>--%>
						<li>
							<span class="input-title"><span class="required">*</span>成团人数：</span>
							<span class="act-limit">
								<input type="text" class="active-time" maxLength="8" name="peopleCount" id="peopleCount" value="${group.peopleCount}" placeholder="请填写成团人数" /><em></em>
							</span>
						</li>
						<li>
							<span class="input-title"><span class="required">*</span>活动分类：</span>
							<span class="act-limit">
								<input type="text" class="active-time" style="cursor: pointer;" id="categoryName" name="categoryName" value="${group.categoryName}" onclick="loadCategoryDialog();"   placeholder="请选择团购分类" readonly="readonly"/><em></em>
							    <input type="hidden" id="firstCatId" name="firstCatId"  value="${group.firstCatId }"/>
				                <input type="hidden" id="secondCatId" name="secondCatId"  value="${group.secondCatId}"/>
				                <input type="hidden" id="thirdCatId" name="thirdCatId"  value="${group.thirdCatId }"/>
							</span>
						</li>
						<li>
							<span class="input-title"><span class="required"></span>活动描述：</span>
							<div class="rich-text">
								<textarea name="groupDesc" id="groupDesc" cols="100" rows="8" style="width:700px;height:300px;visibility:hidden;" placeholder="（选填）">${group.groupDesc}</textarea>
							</div>
						</li>
					</div>
				</div>
				<div class="bto-btn">
					<input type="submit" id="submitGroup" class="red-btn" value="提交" />
					<div class="red-btn return-btn" onclick="javascript:history.go(-1);">返回</div>
				</div>
			</form>
		</div>
<!-- 新增团购活动 -->
		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
 <script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
 <script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
 <script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
 <script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/group/group.js'/>"></script>
 <script type="text/javascript">
	 
	 var contextPath='${contextPath}';
	 KindEditor.options.filterMode=false;
		 KindEditor.create('textarea[name="groupDesc"]', {
			cssPath : '${contextPath}/resources/plugins/kindeditor/plugins/code/prettify.css',
			uploadJson : '${contextPath}/editor/uploadJson/upload;jsessionid=${cookie.SESSION.value}',
			fileManagerJson : '${contextPath}/editor/uploadJson/fileManager',
			allowFileManager : true,
			resizeType: 0,
			afterBlur:function(){this.sync();},
			width : '100%',
			height:'350px',
			afterCreate : function() {
				var self = this;
				KindEditor.ctrl(document, 13, function() {
					self.sync();
					document.forms['example'].submit();
				});
				KindEditor.ctrl(self.edit.doc, 13, function() {
					self.sync();
					document.forms['example'].submit();
				});
			}
		});
	var groupImage="${group.groupImage}";
	
	$("input[id=groupPicFile]").change(function(){
		checkImgType(this);
		checkImgSize(this,512);
	});
	
	//限制上传图片大小
	function checkSize(){
		checkImgType(this);
		checkImgSize(this,512);
	}
</script>
</body>
</html>