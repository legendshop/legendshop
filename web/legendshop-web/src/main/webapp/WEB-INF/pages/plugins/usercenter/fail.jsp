<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

<div class="right">
			<div class="o-mt">
						<h2>错误页面</h2>
			</div>
	<div class="w"> 
	    <div class="login_left wrap">
	      <div class="news_wrap" style="width: 810px;height: 300px;">
	         <div class="news_bor" id="failpage">
				    <div class="mc" style="height: 300px;">
	  					<i class="reg-error"></i>
						
	        			<div class="reg-tips-info" style="color: #CC0000;margin-top: 100px;font-weight: bold;">抱歉，数据更新失败！</div>
	       				<div class="reg-nickname-tips"> 您可以前往<a href="${contextPath}/p/security" style="color: #005EA7;">安全中心</a>重新输入。</div>
	   				 </div> 
	        </div>      
	      </div>                                  
	    </div>
	    <!----左边end---->
	    
	   <div class="clear"></div>
	</div>
</div>  