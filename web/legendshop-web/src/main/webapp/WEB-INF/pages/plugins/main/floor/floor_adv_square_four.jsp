<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<c:set var="advList" value="${floor.floorItems.RA}"></c:set>
    <div class="clear"></div>
    <!--    首页楼层样式 横排方形广告图×4 -->
     <div class="yt-wrap bf_floor_new clearfix">
		 <div class="adv4">
		 <c:forEach items="${advList}" var="adv" varStatus="index">
		     <div class="adv0${index.index}">
		         <a target="_blank" title="${adv.title}" href="${adv.linkUrl}" >
		            <img class="lazy" alt="${adv.title}" src="${contextPath}/resources/common/images/loading1.gif" data-original="<ls:photo item='${adv.picUrl}'/>" style="width: 287px;height: 287px;" >
		         </a>
		       </div>
		   </c:forEach>
	 </div>
 </div>