<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>

<html>
  <head>
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>退货详情-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
	<style>
		.express-select{
			width : 110px;
			height : 25px;
			line-height : 25px;
			border: 1px solid #ccc;
			background-color:#fff;
			color:#666;
		}
		.express-input{
			width : 180px;
			height : 25px;
			line-height: 25px;
		    padding-left: 4px;
			border: 1px solid #ccc;
			background-color:#fff;
		    color:#666;
		}
		.required{
			color:red;
		}
	</style>
  </head>
<body class="graybody">
	   <div id="bd">
	   		 <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
			 <div class=" yt-wrap" style="padding-top:10px;"> 
       	 		<%@include file='../usercenterLeft.jsp'%>
		
			    <!-----------right_con-------------->
			     <div class="right_con">
			     	
			         <div class="o-mt"><h2>退款详情</h2></div>
				        <div class="alert">
				          <h4>提示：</h4>
				          <ul>
				            <li>1. 若提出申请后，商家拒绝退款或退货，可再次提交申请或选择<em>“商品投诉”</em>，请求商城客服人员介入。
				            	<!-- 如果商家拒绝退款 显示平台客服入口  -->
								<c:if test="${subRefundReturn.sellerState eq 3}">
									<a target="_blank" href="${contextPath}/p/im/customerAdmin/0"
				                   		style="width:100px;height:26px;background: #e5004f;line-height:26px;display: inline-block;text-align: center;color: #fff;border-radius: 3px;">
				                    	<i style="background: url('${contextPath}/resources/templets/images/chat-i.png');width: 19px;height: 18px;display: inline-block;vertical-align: middle;margin-right: 5px;margin-bottom: 2px;"></i>平台客服
				               		</a>
			               		</c:if>
				            </li>
							<li>2. 成功完成退款/退货；经过商城审核后，会将原路退回</li>
				          </ul>
				        </div>
				        <div class="ret-step-com">
				          <ul>
				            <li <c:if test="${not empty subRefundReturn.applyTime}">class="done"</c:if>><span>买家申请退货</span></li>
				            <li <c:if test="${not empty subRefundReturn.sellerTime}">class="done"</c:if>><span>商家处理退货申请</span></li>
				            <li <c:if test="${not empty subRefundReturn.shipTime}">class="done"</c:if>><span>买家退货给商家</span></li>
				            <li <c:if test="${not empty subRefundReturn.receiveTime}">class="done"</c:if>><span>确认收货，平台审核</span></li>
				          </ul>
				        </div>
				        <div class="ret-detail">
				         <div class="ste-tit"><b>我的退货退款申请</b></div>
				          <div class="ste-con">
				            <ul>
				              <li><span>退货退款编号：</span>${subRefundReturn.refundSn} </li>
				              <li><span>申请时间：</span><fmt:formatDate value="${subRefundReturn.applyTime}" pattern="yyyy-MM-dd HH:mm"/> </li>
				              <li><span>订单编号：</span>${subRefundReturn.subNumber} </li>
				              <c:if test="${not empty subRefundReturn.outRefundNo}">
			               <li><span>第三方支付流水号：</span>${subRefundReturn.outRefundNo}</li>
			               </c:if>
				              <li>
				              	<span>涉及商品：</span>
							    <a href="${contextPath}/views/${subRefundReturn.productId}">${subRefundReturn.productName }</a>
				              </li>
				              <li><span>退款金额：</span>￥${subRefundReturn.refundAmount} 元</li>
				              <li><span>退货数量：</span>x ${subRefundReturn.goodsNum}</li>
				              <li><span>退款退货原因：</span>${subRefundReturn.buyerMessage} </li>
				              <li><span>退款退货说明：</span>${subRefundReturn.reasonInfo}</li>
				              <li style="border: 0;">
				              	<span>凭证上传1：</span>
				              	<img src="<ls:images item='${subRefundReturn.photoFile1}' scale='2'/>"/>
				              </li>
				              <c:if test="${not empty subRefundReturn.photoFile2}">
				              	  <li style="border: 0;">
					              	<span>凭证上传2：</span>
					              	<img src="<ls:images item='${subRefundReturn.photoFile2}' scale='2'/>"/>
					              </li>
				              </c:if>
				               <c:if test="${not empty subRefundReturn.photoFile2}">
					              <li style="border: 0;">
					              <span>凭证上传3：</span>
					              	<img src="<ls:images item='${subRefundReturn.photoFile3}' scale='2' />"/>
					              </li>
				              </c:if>
				            </ul>
				          </div>
				          <div class="ste-tit"><b>商家退货退款处理</b></div>
				          <div class="ste-con">
				            <ul>
				              <li>
				              	<span>审核状态：</span>
				              	<c:if test="${subRefundReturn.sellerState eq 1 && subRefundReturn.applyState eq -1}">
				              		已撤销
				              	</c:if>
				              	<c:if test="${subRefundReturn.sellerState eq 1 && subRefundReturn.applyState eq 1 }">
				              		待审核
				              	</c:if>
				              	<c:if test="${subRefundReturn.sellerState eq 2}">
				              		同意
				              	</c:if>
				              	<c:if test="${subRefundReturn.sellerState eq 3}">
				              		不同意
				              	</c:if>
				              </li>
				              <li style="border: 0;">
				              	<span>商家备注：</span>
				              	${subRefundReturn.sellerMessage }
				              	<c:if test="${empty subRefundReturn.sellerMessage }">
				              		无
				              	</c:if>
				              </li>
				            </ul>
				          </div>
				          <c:if test="${subRefundReturn.sellerState eq 2 and subRefundReturn.applyState eq 2}">
					          	<div class="ste-tit"><b>商城退款审核</b></div>
						          <div class="ste-con">
						            <ul>
						              <li>
						              	<span>平台确认：</span>
						              	<c:if test="${subRefundReturn.applyState eq 2}">
						              		待平台确认
						              	</c:if>
						              	<c:if test="${subRefundReturn.applyState eq 3}">
						              		已完成
						              	</c:if>
						              </li>
						              <li style="border: 0;"><span>平台备注：</span>${subRefundReturn.adminMessage}</li>
						            </ul>
						          </div>
				          </c:if>
				          <c:if test="${subRefundReturn.applyState eq 3}">
					          <div class="ste-tit"><b>退款详细</b></div>
					          <div class="ste-con">
					            <ul>
					              <li>
					              	<span>支付方式：</span>
					              	<c:if test="${subRefundReturn.payTypeId eq 'ALP' }">支付宝 </c:if>
					              	<c:if test="${subRefundReturn.payTypeId eq 'COIN' }">金币全额支付</c:if>
					              	<c:if test="${subRefundReturn.payTypeId eq 'PRED' }">预存款全额支付 </c:if>
					              	<c:if test="${subRefundReturn.payTypeId eq 'SIMULATE' }">商城模拟支付 </c:if>
					              	<c:if test="${subRefundReturn.payTypeId eq 'FULL_PAY' }">全款支付 </c:if>
					              </li>
					              <li><span>订单总额：</span>${subRefundReturn.subMoney }</li>
					            </ul>
					          </div>
				          </c:if>
				          
				          <c:if test="${subRefundReturn.sellerState eq 2 and subRefundReturn.returnType eq 2}">
				          	  <c:choose>
				          	  	<c:when test="${subRefundReturn.goodsState eq 1}">
				          	  	  <input type="hidden" id="refundId" name="refundId" value="${subRefundReturn.refundId }"/>
						          <div class="ste-tit"><b style="color:#e5004f;">我的退货发货信息</b></div>
						          <div class="ste-con">
						            <ul>
						              <li>
						              	  <span><em class="required">*</em>物流公司：</span>
							              <select id="expressName" name="expressName" class="express-select">
							              	<option value="">请选择物流公司</option>
							              	<option value="顺丰速递">顺丰速递</option>
							              	<option value="韵达快递">韵达快递</option>
							              	<option value="圆通快递">圆通快递</option>
							              	<option value="中通快递">中通快递</option>
							              	<option value="申通快递">申通快递</option>
							              	<option value="天天快递">天天快递</option>
							              	<option value="德邦快递">德邦快递</option>
							              	<option value="百世汇通快递">百世汇通快递</option>
							              </select>
						              </li>
						              <li style="border: 0;">
						              	<span><em class="required">*</em>物流单号：</span>
						              	<input id="expressNo" name="expressNo" value="" class="express-input" placeholder="请输入物流单号"/>
						              </li>
						            </ul>
						          </div>
				          	  	</c:when>
				          	  	<c:otherwise>
				          	  		<div class="ste-tit"><b>我的退货发货信息</b></div>
							        <div class="ste-con">
							            <ul>
							              <li><span>物流公司：</span>${subRefundReturn.expressName}</li>
							              <li style="border: 0;"><span>物流单号：</span>${subRefundReturn.expressNo}</li>
							            </ul>
							         </div>
				          	  	</c:otherwise>
				          	  </c:choose>
				          </c:if>
				        </div>
				          <div class="ste-but" align="center">
				           <c:if test="${subRefundReturn.sellerState eq 2 and subRefundReturn.returnType eq 2 and subRefundReturn.goodsState eq 1}">
				           		<input type="button" id="submitBtn" value="确定退货" class="re" onclick="confirmReturn();"/>
				           </c:if>
				           <input type="button" value="返回" class="btn-g big-btn" style="height:30px;" onclick="window.location='${contextPath}/p/refundReturnList/${subRefundReturn.applyType }'"/>
				         </div>
				         <!-- 退货详情 -->
				        </div>
			     </div>
			     <!-----------right_con 结束-------------->
			     
			    <div class="clear"></div>
			 </div>
		   
		 	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
		</div>
		<script type="text/javascript">
			var contextPath = '${contextPath}';
			$(function(){
				userCenter.changeSubTab("myreturnorder");//高亮菜单
			});
			
			//确认退货
			function confirmReturn(){
				var refundId = $("#refundId").val();
				var expressName = $("#expressName").val();
				var expressNo = $("#expressNo").val();
				
				if(isBlank(refundId) || isBlank(expressName) || isBlank(expressNo)){
					layer.msg("对不起,请您完善好物流信息后再确认!",{icon:0});
					return;
				}
				var code =/^(?!_)(?!.*?_$)[a-zA-Z0-9_]+$/;
				if(!code.test(expressNo) || expressNo.indexOf(" ")>-1 || expressNo.length>50){
			    	layer.msg("物流单号格式错误，请重新输入",{icon:0});
				    return ;
			     }
				
				var param = {"refundId":refundId,"expressName":expressName,"expressNo":expressNo};
				$.ajax({
					url : "${contextPath}/p/returnGoods",
					type : "POST",
					data : param,
					dataType : "JSON",
					async : true,
					beforeSend : function(xhr){
						$("#submitBtn").attr("disabled",true);
					},
					complete : function(xhr,status){
						$("#submitBtn").attr("disabled",false);
					},
					success: function(result,status,xhr){
						if(result == "OK"){
							layer.alert("恭喜你,操作成功!",{icon:1},function(){
								window.location.href = contextPath + "/p/refundReturnList/"+${subRefundReturn.applyType };
							});
						}else{
							layer.alert(result,{icon:2});
						}
					}
				});
			}
			
			function isBlank(value){
				return value == undefined || value == null || $.trim(value) === "";
			}
		</script>
	</body>
</html>
