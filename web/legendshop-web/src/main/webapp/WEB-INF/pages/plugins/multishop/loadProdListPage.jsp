<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<html>
<head>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller${_style_}.css'/>" rel="stylesheet"/>
	<style type="text/css">
		html{font-size:13px;}
		table{    margin: 10px;
    border: 1px solid #ddd;}
		table tbody tr{height:70px;}
		table tbody tr td{border-bottom:1px solid #ddd;text-align: center;}
		table thead tr td{border-bottom:1px solid #ddd;text-align: center;padding:5px;background: #f9f9f9;}
	</style>
</head>
<body>
	 <form:form  action="${contextPath}/s/loadProdListPage" id="form1" method="post">
	 	<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
	 	<div style="height:50px;background-color: #f9f9f9;border: 1px solid #ddd;font-size: 14px;margin: 10px;">
	 		<div style="padding-top:10px;padding-left:20px;">
			<input style="width:550px;border:1px solid #ddd;padding: 5px;"  type="text" name="name" id="name" maxlength="20" value="${prod.name}" size="20" placeholder="请输入商品名称" />
			<input type="submit" value="搜索" style="outline: none;background: #e5004f;color: #fff;padding: 5px 10px;border: 0px;"/>
			</div>
		</div>
	 </form:form>
	<table>
		<thead>
			<tr><td width="150">图片</td><td width="450">名称</td><td width="110">操作</td></tr>
		</thead>
		<tbody>
		<c:if test="${empty requestScope.list}">
			<tr><td colspan="3" style="text-align: center;">没有找到符合条件的商品</td></tr>
		</c:if>
     	<c:forEach items="${requestScope.list}" var="product">
          <tr>
          	<td><img src="<ls:images item='${product.pic}' scale='3' />" ></td>
          	<td>${product.name}</td>
          	<td><a target="_blank" class="btn-r" href="${contextPath}/views/${product.prodId}">查看</a>
          	&nbsp;<button class="btn-g" onclick="chooseProd('${product.prodId}')">选择</button></td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
      <div style="margin-top:10px;" class="page clearfix">
     			 <div class="p-wrap">
            		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
     			 </div>
	 </div>
      <script type="text/javascript">
	      function pager(curPageNO){
	          document.getElementById("curPageNO").value=curPageNO;
	          document.getElementById("form1").submit();
	      }
	      function chooseProd(prodId){
	    	  self.parent.window.location="${contextPath}/s/similarProd/"+prodId;
	      }
      </script>
</body>
</html>