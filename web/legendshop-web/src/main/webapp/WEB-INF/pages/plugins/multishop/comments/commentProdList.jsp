<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<title>评价管理-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}" />
<meta name="description" content="${systemConfig.description}" />
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置> <a href="${contextPath}/home">首页</a>> 
					<a href="${contextPath}/sellerHome">卖家中心</a>> <span class="on">评价管理</span>
				</p>
			</div>
			
			<%@ include file="../included/sellerLeft.jsp"%>
			
			<div id="Content" style="width:960px;float:right;margin-top:20px;">
				<div class="seller-com-nav">
					<ul>
						<li  class="on"><a href="javascript:void(0);">评论商品列表</a></li>
					</ul>
				</div>
				
				<div id="recommend" class="recommend" style="display: block;">
					<table id="item" style="width:100%;text-align: center;" cellspacing="0" cellpadding="0"
						class="buytable prod-comment sold-table">
						<tbody>
							<tr>
								<th width="120">商品</th>
								<th width="350">商品名称</th>
								<th width="70">评价数</th>
								<th width="70">评价平均得分</th>
								<th width="100" style="border-right: 1px solid #e4e4e4">操作</th>
							</tr>
							<c:choose>
					           <c:when test="${empty requestScope.list}">
					              <tr>
								 	 <td colspan="20">
								 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
								 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
								 	 </td>
								 </tr>
					           </c:when>
					           <c:otherwise>
					              <c:forEach items="${requestScope.list}" var="comm" varStatus="status">
										<tr>
											<td height="70"><a
												href="${contextPath}/views/${comm.prodId}" target="_blank"><img
													src="<ls:images item='${comm.pic}' scale='3' />" />
											</a>
											</td>
											<td><a href="${contextPath}/views/${comm.prodId}"
												target="_blank">${comm.prodName}</a>
											</td>
											<td>${comm.comments}</td>
											<td>
												<c:choose>
													<c:when
														test="${comm.comments != null and comm.comments != 0 }">
														<fmt:formatNumber pattern="0.00"
															value="${comm.reviewScores/comm.comments}" />
													</c:when>
													<c:otherwise>0</c:otherwise>
												</c:choose>
											</td>
											<td>
												<a class="btn-g" style="margin-right:5px;" href="${contextPath}/s/comments/commentList?prodId=${comm.prodId}&curPageNO=0">查看</a>
												<a class="btn-r" href="${contextPath}/s/comments/commentList?prodId=${comm.prodId}&curPageNO=0&condition=waitReply">
													<c:choose>
														<c:when test="${not empty comm.waitReply }">
														待回复(${comm.waitReply})
														</c:when>
														<c:otherwise>待回复(0)</c:otherwise>
													</c:choose>
												</a>
											</td>
										</tr>
									</c:forEach>
					           </c:otherwise>
				           </c:choose>
						</tbody>
					</table>
				</div>
				<div class="clear"></div>
				<div style="margin-top:10px;" class="page clearfix">
		     			 <div class="p-wrap">
		            		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
		     			 </div>
  				</div>
				<div style="margin-top:10px;" class="page clearfix">
					<div class="p-wrap">
						<c:if test="${toolBar!=null}">
							<span class="p-num"> <c:out value="${toolBar}"
									escapeXml="${toolBar}"></c:out> </span>
						</c:if>
					</div>
				</div>
			</div>

		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<script type="text/javascript">
		function pager(curPageNO) {
			sendData("${contextPath}/s/comments/commentProdList?curPageNO=" + curPageNO);
		}

		$(document).ready(function(e) {
			prodComment.doExecute();
		});
	</script>
</body>
</html>