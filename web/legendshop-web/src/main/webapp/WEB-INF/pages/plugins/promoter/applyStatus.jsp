<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>申请推广员</title>
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/applyPromoter${_style_}.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;">     
       <%@include file='/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp'%>
    
     <!--right_con-->
     <div class="right_con">
     
         <div class="o-mt"><h2>申请成为推广员</h2></div>

         <div class="pagetab2">
                 <ul>           
                    <li class="on" id="favoriteShopList"><span>申请推广</span></li>
                 </ul>       
         </div>

         <div class="apply-promoter">
	            <c:if test="${userCommis.promoterSts eq -2}">
	               <div class="rev-app">
		             <dl>
		               <dt><img src="<ls:templateResource item='/resources/templets/images/applying.png'/>" alt=""></dt>
		               <dd>您提交的申请正在审核中，请耐心等待审核结果。</dd>
		             </dl>
		           </div>
	           </c:if>
           <!-- 申请中 -->
           
	            <c:if test="${userCommis.promoterSts eq -1}">
		           <div class="ref-app">
		             <dl>
		               <dt><img src="<ls:templateResource item='/resources/templets/images/refuse-apply.png'/>" alt=""></dt>
		               <dd>对不起，您提交的申请已被拒绝。</dd>
		               <dd><span>理由：${userCommis.promoterAuditComm }</span></dd>
		               <dd><input type="button" value="重新申请" onclick="resetInfo();"></dd>
		             </dl>
		           </div>
	            </c:if>
           <!-- 申请被拒绝 -->
         </div>
     </div>
    <!--right_con end-->

</div>

 <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script src=" <ls:templateResource item='/resources/common/js/jquery.validate.js'/>"  type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/common/js/randomimage.js'/>"></script> 
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/applyPromoter.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script> 	
	<script type="text/javascript">
    var contextPath="${contextPath}";
    $(document).ready(function() {
		userCenter.changeSubTab("applyPromoter");
	   $("select.combox").initSelect();
  	});
  	
  	function resetInfo(){
  	   window.location.href="${contextPath}/p/promoter/applyPromoter?reset=0";
  	}
</script>
</body>
</html> 