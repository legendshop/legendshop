<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>拍卖中标记录-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
</head>
<style>
.sold-table th{
 	border-bottom:1px solid #e4e4e4;
}

</style>
<body class="graybody">
<div id="doc">
   
   <div id="hd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
   </div><!--hd end-->
   
   
   <div id="bd">
            
	 <div class=" yt-wrap" style="padding-top:10px;"> 
    	  <%@ include file="/WEB-INF/pages/plugins/usercenter/usercenterLeft.jsp" %>
     <div class="right_con">
     
         <div class="o-mt"><h2>我参与的竞拍</h2></div>                
         
         <div class="pagetab2">
                <ul>
                    <li class="on"><span>竞拍列表</span></li>
                </ul>
         </div>
         
        <!--订单-->
         <div id="recommend" class="m10 recommend bidding-his" style="display: block;">
                 
                <table width="100%" cellspacing="0" cellpadding="0" class="buytable sold-table" style="text-align:center;">
                    <tr>
                    	<th>图片</th>
                        <th>竞拍商品名称</th>
                        <th width="10%">交易时间</th>
                        <th width="10%">订单金额</th>
                        <th width="10%" style="border-right: 1px solid #e4e4e4;">状态</th>
<!--                         <th width="10%">支付状态</th> -->
<!--                         <th width="16%">操作</th> -->
                    </tr>
                    <c:choose>
                    	<c:when test="${empty list}">
                    	    <tr>
					 	 		<td colspan="20">
					 	 			<!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
					 	 			<div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
					 	 		</td>
					 	 	</tr>
                    	</c:when>
                    	<c:otherwise>
                  		    <c:forEach items="${list}" var ="list" varStatus="status">
	                          <tr>
	                              <td style="width:15%;text-align: center;">
	                                <a target="_blank" href="<ls:url address='/auction/views/${list.AId}'/>">
	                                  <img src="<ls:images item='${list.prodPic}' scale='3'/>" alt="" >
	                              </td>
	                              <td style="width:20%; text-align: center;" class="a-left">
	                                <a target="_blank" href="<ls:url address='/auction/views/${list.AId}'/>">
	                                   ${list.prodName}
	<%--                                   <span class="pm-com-num">竞拍订单号：${list.subNumber}</span> --%>
	                                </a>
	                              </td>
	                              <td><fmt:formatDate value="${list.bitTime}"  pattern="yyyy-MM-dd HH:mm:ss"/></td>
	                              <td>¥${list.price}</td>
	                              <td>
									<c:choose>
										<c:when test="${list.first eq 1 }"><em class="a-block col-orange">领先</em></c:when>
										<c:when test="${list.first eq 0 }">出局</c:when>
										<c:otherwise>-</c:otherwise>
									</c:choose>		
							  </td>
	<!--                               <td> -->
	<%-- 								<c:choose> --%>
	<%-- 								    <c:when test="${list.status eq -2}">已关闭</c:when> --%>
	<%-- 									<c:when test="${list.status eq -1}">未转订单</c:when> --%>
	<%-- 									<c:when test="${list.status eq 0}">等待支付</c:when> --%>
	<%-- 									<c:when test="${list.status eq 1}">已支付</c:when> --%>
	<%-- 								</c:choose> --%>
	<!-- 							 </td> -->
	<!--                              <td> -->
	<%-- 	                              <c:choose> --%>
	<%-- 	                              	<c:when test="${list.status eq -1}"> <input type="button" value="提交订单" onclick="changeOrder('${list.id}');"></td></c:when> --%>
	<%-- 	                              	<c:when test="${list.status eq 0}"> <input type="button" value="去支付"  onclick="window.location.href='<ls:url address="/p/orderSuccess?subNums=${list.subId}" />'" ></td></c:when> --%>
	<%-- 	                              	<c:when test="${list.status eq 1}" ><a href="<ls:url address="/p/orderDetail/${list.subNumber}" />">已支付完成，查看订单</a></c:when> --%>
	<%-- 	                              </c:choose> --%>
	<!--                               </td> -->
	                          </tr>
	                        </c:forEach>
                    	</c:otherwise>
                    </c:choose>
                </table>
				              		
	     </div>
     </div>
    <form:form action="${contextPath}/p/userPartAution" id="form1" method="post">
		<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
		<input type="hidden" id="loadType" name="loadType" value="${param.loadType}" />
	</form:form>
    <div class="clear"></div>
	     <div style="margin-top:10px;" class="page clearfix">
   			 <div class="p-wrap">
          		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
   			 </div>
		</div>
 	</div>
           
   </div>
 <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
</div>
<script type="text/javascript">
	userCenter.changeSubTab("myPartAution");
	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}
</script>
</body>
</html>
