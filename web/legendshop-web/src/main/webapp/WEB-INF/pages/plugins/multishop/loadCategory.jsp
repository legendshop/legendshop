<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
	<c:forEach items="${requestScope.cateList}" var="category" >
		<li  id="${category.id}" class='cc-tree-item <c:if test="${fn:length(category.childrenList)>0}">cc-hasChild-item</c:if> ' >${category.name}</li>
	</c:forEach>
