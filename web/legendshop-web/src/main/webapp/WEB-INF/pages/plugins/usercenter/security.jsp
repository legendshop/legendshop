<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>账户安全 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/securityCenter.css'/>" rel="stylesheet" />
</head>
<style>
.btn-c{
	border-radius: 2px;
	text-align: center;
	background-color: #e5004f;
	color: #fff !important;
	padding: 0 10px;
	height: 30px;
	line-height: 30px;
	border: 0;
	outline: none;
	font-size: 12px;
	display: inline-block;
	vertical-align: top;
	box-sizing: border-box;
	-webkit-transition: all 0.2s ease;
	-moz-transition: all 0.2s ease;
	transition: all 0.2s ease;
	width:100px;
}
</style>
<body class="graybody">
	<div id="bd">
	<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;"> 
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con">
				<div class="o-mt">
						<h2>账户安全</h2>
				</div>
				
				<div class="pagetab2">
	                <ul>
	                    <li class="on"><span>安全设置</span></li>
	                </ul>
           		</div>

				<div class="m m3" id="safe04" style="margin-top: 10px; background: #f9f9f9;">
					<div class="mc">
						<strong class="fore fore1">安全级别：</strong>
						<strong class="rank-text ftx-04" id="secLevel">危险</strong>
						<i class="icon-rank04"></i>
						<span id="promptInfo">提示：建议您启动全部安全设置，以保障账户及资金安全。</span>
					</div>
				</div>

				<div class="m m5" id="safe05" style="margin-top: 10px;">
					<div class="mc">
						<div class="fore1"><s class="icon-01"></s><strong>登录密码</strong></div>
						<div class="fore2"><span class="ftx-03"></span><span style="color:#999;">互联网账号存在被盗风险，建议您定期更改密码以保护账户安全。</span></div>
						<div class="fore3"><a id="toUpdatePassword" style="margin-left:172px;" href="javascript:void(0)" class="btn-c">修改</a></div>
					</div>
					
				<c:choose>
					<c:when test="${userInfo.mailVerifn ==1}">
						<div class="mc">
							<div class="fore1"><s class="icon-01"></s><strong>邮箱验证</strong></div>
							<div class="fore2"><span class="ftx-03">您验证的邮箱:</span><strong class="ftx-06">${userInfo.userMail}</strong></div>
	    					<c:choose>
							   <c:when test="${userInfo.mailVerifn ==1 || userInfo.phoneVerifn ==1}">
							     <div class="fore3"><a id="toUpdateMail" style="margin-left:172px;"  href="javascript:void(0)" class="btn-c">修改</a></div>
							   </c:when>
							   <c:otherwise>
							      <div><a href="javascript:void(0)" class="ncbtn">请绑定手机或邮箱</a></div>
							   </c:otherwise>
					 		 </c:choose>	
						</div>
					</c:when>
					<c:otherwise>
					<div class="mc">
						<div class="fore1"><s class="icon-02"></s><strong>邮箱验证</strong></div>
						<div class="fore2"><span class="ftx-03">验证后，可用于快速找回登录密码，接收账户余额变动提醒</span></div>
    					<div class="fore3"><a id="toUpdateMail" style="margin-left:172px;" href="javascript:void(0)" class="btn-c"><s></s>立即启用</a></div>						
					</div>
					</c:otherwise>
				</c:choose>	
			
				<c:choose>
					<c:when test="${userInfo.phoneVerifn ==1}">
						<div class="mc" id="userMobileDiv">
							<div class="fore1"><s class="icon-01" ></s><strong>手机验证</strong></div>
							<div class="fore2"><span class="ftx-03">您验证的手机：</span><strong class="ftx-06">${userInfo.userMobile}</strong></div>
							<div class="fore3"><a id="toUpdateMobile" style="margin-left:172px;" href="javascript:void(0)" class="btn-c">修改</a></div>
						</div>		
					</c:when>
					<c:otherwise>
					<div class="mc" id="userMobileDiv">
						<div class="fore1"><s class="icon-02" ></s><strong>手机验证</strong></div>
						<div class="fore2"><span class="ftx-03">验证后，可用于快速找回登录密码及支付密码，接收账户余额变动提醒。</span></div>
						<div class="fore3"><a id="toUpdateMobile" style="margin-left:172px;" href="javascript:void(0)" class="btn-c"><s></s>立即启用</a></div>
					</div>		
					</c:otherwise>	
				</c:choose>
				
				<c:choose>
					<c:when test="${userInfo.paypassVerifn ==1}">	
						<div class="mc" id="usedFlagOpenDiv">
							<div class="fore1"><s class="icon-01" ></s><strong>支付密码</strong></div>
	    					<div class="fore2">	<span class="ftx-03">安全程度：</span><i class="ir icon-s-0${userInfo.payStrength}"></i><span class="ftx-03">建议您设置更高强度的密码。</span></div>
	    					<div class="fore3"><a id="toUpdatePaymentpassword" style="margin-left:172px;" href="javascript:void(0)" class="btn-c">修改</a></div>
						</div>
					</c:when>
					<c:otherwise>
						<div class="mc" id="usedFlagOpenDiv">
							<div class="fore1"><s class="icon-02" ></s><strong>支付密码</strong></div>
	    					<div class="fore2"><span class="ftx-03">启用支付密码后，在使用账户中余额或优惠劵等资产时，需输入支付密码。</span></div>
	    					<div class="fore3"><a id="toUpdatePaymentpassword" style="margin-left:172px;" href="javascript:void(0)" class="btn-c"><s></s>支付密码管理</a></div>
						</div>
					</c:otherwise>
				</c:choose>
			</div>
				
				<div class="m m7">
					<div class="mt"><h3>安全服务提示</h3></div>
					<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
					<div class="mc"> 
                        1.确认您登录的是"${systemConfig.shopName}"网址<a href="${systemConfig.url}">${systemConfig.url}</a>，注意防范进入钓鱼网站，不要轻信各种即时通讯工具发送的商品或支付链接，谨防网购诈骗。<br />
                        2.建议您安装杀毒软件，并定期更新操作系统等软件补丁，确保账户及交易安全。
					</div>
				</div>
				 
		</div>
		
 <!-----------right_con end-------------->
    
    
    <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
<script type="text/javascript">
				$(document).ready(function() {
					security.doExecute('${userInfo.secLevel}', '${userInfo.phoneVerifn}');
				});	
		</script>
</body>
</html>