<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<link type="text/css"
      href="<ls:templateResource item='/resources/templets/css/publishProd1${_style_}.css'/>"
      rel="stylesheet"/>
<link type="text/css"
      href="<ls:templateResource item='/resources/templets/css/multishop/seller.css'/>"
      rel="stylesheet"/>

<form:form action="${contextPath}/s/shopUsers/save" method="post"
           id="from1" target="_parent">
    <input type="hidden" value="${skuIds}" name="id" id="skuIds">
    <div style="margin: 65px 0px 0px 63px">销售库存数：<input type="text" id="stock" name="stock" maxlength="8" /></div>
    <div style="margin: 65px 0px 0px 63px">实际库存数：<input type="text" id="actualStock" name="actualStock" maxlength="8" /></div>

    <div class="btn-content" style="margin: 25px 0px 0px 133px">
        <input onclick="batchEdit();" type="button" value="保存"
               class="btn-r small-btn"/> <input onclick="closeDialog();"
                                                style="margin-left: 20px;" type="button" value="取消"
                                                class="btn-g small-btn"/>
    </div>
</form:form>


<script
        src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script type="text/javascript">

    function closeDialog() {
        var index = parent.layer.getFrameIndex('batchEditStock');
        parent.layer.close(index);
    }

    function batchEdit() {
        var  stock=$("#stock");
        var  actualStock=$("#actualStock");
        if (stock.val() == "" || actualStock.val() == "") {
            layer.alert("库存数必须填写");
            return false;
        }
        if (isNaN(stock.val()) || isNaN(actualStock.val())) {
            layer.alert("库存数必须为数字");
            return false;
        }

        if (!(/(^[1-9]\d*$)/.test(stock.val())) || !(/(^[1-9]\d*$)/.test(actualStock.val()))) {
            layer.alert("库存数必须为正整数")
            return false;
        }
        var load = layer.load(1, {
            shade: false
        })
        $("input[type=button]").css('background-color', "gray")
        $("input[type=button]").attr('disabled', true)
        $.ajax({
            url: '/s/batchEditUpdateStocks',
            data: {
                "skuIds": $("#skuIds").val(),
                "stock": stock.val(),
                "actualStock": actualStock.val()
            },
            type: 'post',
            dataType: 'json',
            async: true, //默认为true 异步
            success: function (result) {
                if (result == 'true') {
                    layer.close(load);
                    parent.layer.msg('修改成功', {
                        icon: 1,
                        time: 1500
                        //2秒关闭（如果不配置，默认是3秒）
                    }, function () {
                        parent.window.location.reload();
                    });
                    return;
                } else {
                    layer.close(load);
                    parent.layer.alert('数据错误，修改失败！', {
                        icon: 2,
                        time: 6000
                        //2秒关闭（如果不配置，默认是3秒）
                    }, function () {
                        parent.window.location.reload();
                    });
                    return;
                }

            }
        });

    }
</script>


