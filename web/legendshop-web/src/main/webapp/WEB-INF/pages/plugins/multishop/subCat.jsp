<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>三级商品类目-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-category${_style_}.css'/>" rel="stylesheet">
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
<div id="doc">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
    <div id="Content" class="w1190">
        <div class="seller-cru">
            <p>
                您的位置>
                <a href="${contextPath}/home">首页</a>>
                <a href="${contextPath}/sellerHome">卖家中心</a>>
                <span class="on">三级商品类目列表</span>
            </p>
        </div>
        <%@ include file="included/sellerLeft.jsp" %>
        <div class="seller-category">

            <div class="seller-com-nav">
                <ul>
                    <li width="150px;" id="shopCatList"><a href="javascript:void(0);">一级类目列表</a></li>
                    <li width="150px;" id="nextCatList"><a href="javascript:void(0);">二级类目列表</a></li>
                    <li width="150px;" class="on" id="subCatList"><a href="javascript:void(0);">三级类目列表</a></li>
                </ul>
            </div>
            <form:form action="${contextPath}/s/subShopCat/query?shopCatId=${shopCat.shopCatId}&nextCatId=${shopCat.nextCatId}" method="post" id="from1">
                <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
                <div class="sold-ser sold-ser-no-bg clearfix">
                	<div class="fr">
	                	<div class="item">
		                                                     商品类目：
		                    <input type="text" class="item-inp" value="${shopCat.name}" id="name" name="name" style="width:200px;">
		                    <input type="submit" class="bti btn-r" id="btn_keyword" value="查询类目">
		                    <input type="button" class="bti btn-r" id="subCatConfigure" value="添加三级类目">
	                    </div>
                	</div>
                </div>
            </form:form>
            <table style="width:100%" cellspacing="0" cellpadding="0" class="category-table sold-table">
                <tr class="category-tit">
                    <th>三级商品类目</th>
                    <th>次序</th>
                    <th width="250">操作</th>
                </tr>
                <c:if test="${empty requestScope.list}">
                    <tr>
                        <td colspan='4' height="40">无商品类目信息</td>
                    </tr>
                </c:if>
                <c:forEach items="${requestScope.list}" var="nextCat" varStatus="status">
                    <tr class="detail">
                        <td>${nextCat.name}</td>
                        <td>${nextCat.seq}</td>
                        <td>
                            <ul class="mau">
                                <c:choose>
                                    <c:when test="${nextCat.status == 1}">
                                        <li><a href="javascript:void(0);" class="btn-r" name="statusImg" itemId="${nextCat.id}" itemName="${nextCat.name}" status="${nextCat.status}">下线</a></li>
                                    </c:when>
                                    <c:otherwise>
                                        <li><a href="javascript:void(0);" class="btn-r" name="statusImg" itemId="${nextCat.id}" itemName="${nextCat.name}" status="${nextCat.status}">上线</a></li>
                                    </c:otherwise>
                                </c:choose>
                                <li><a href='${contextPath}/s/subShopCat/load?shopCatId=${shopCat.shopCatId}&nextCatId=${shopCat.nextCatId}&subCatId=${nextCat.id}' title="修改" class="btn-g">修改</a></li>
                                <li><a href='javascript:deleteSubCategory("${nextCat.id}","${nextCat.name}","${shopCat.shopCatId}")' title="删除" class="btn-g">删除</a></li>
                            </ul>
                        </td>
                    </tr>
                </c:forEach>
            </table>

            <div class="clear"></div>
            <div style="margin-top:10px;" class="page clearfix">
                <div class="p-wrap">
                    <c:if test="${toolBar!=null}">
											<span class="p-num">
												<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
											</span>
                    </c:if>
                </div>
            </div>

        </div>
    </div>
    <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/nexeShopCat_status.js'/>"></script>
<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/nextShopCat.js'/>"></script>
<script type="text/javascript">
    var contextPath = "${contextPath}";
    var nextCatId = '${shopCat.nextCatId}';
    $("#subCatConfigure").click(function () {//前往三级类目配置
        window.location.href = contextPath + "/s/subShopCat/load?shopCatId=${shopCat.shopCatId}&nextCatId=${shopCat.nextCatId}";
    });
    $("#nextCatList").click(function () {//前往二级类目列表
        window.location.href = contextPath + "/s/nextShopCat/query?shopCatId=${shopCat.shopCatId}";
    });
    $("#subCatList").click(function () {//前往三级类目列表
        window.location.href = contextPath + "/s/subShopCat/query?shopCatId=${shopCat.shopCatId}&nextCatId=${shopCat.nextCatId}";
    });
    //删除三级分类
    function deleteSubCategory(subCatId, nextCatName, shopCatId) {
        
        layer.confirm("确定要删除类目["+nextCatName+"]?",{
   		 icon: 3
   	     ,btn: ['确定','取消'] //按钮
   	   }, function () {
   		   
   		$.ajax({
			url:contextPath+"/s/subCat/delete",
			data:{"subCatId":subCatId},
			type:'get', 
			dataType : 'json', 
			async : true, //默认为true 异步   
			success:function(result){
				if(result=="0"){
					layer.alert('用户无效状态，不可进行删除操作 !', {icon: 2});
				} else if(result=="fail"){
					layer.alert('该分类关联商品，无法删除!', {icon: 0});
				}else{
					window.location.reload();
				}
			}
		});
   		
   	  });
    }
    userCenter.changeSubTab("shopCategory");
</script>
</body>