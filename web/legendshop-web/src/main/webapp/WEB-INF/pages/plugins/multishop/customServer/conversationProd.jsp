<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>

<!-- 本店商品 -->
<div class="my-asking">
	<div class="oth-list" style="border-top: 0; padding-top: 0px;">
		<div class="choose-goods">
			<div class="item">
				<span class="item-tit">商品标题：</span>
				<input type="text" class="goods-tit" name="name" id="name" value="${prod.name }">
				<a href="javascript:void(0)" class="checkProd">搜索</a>
			</div>
		</div>
		<div class="list-con">
			<div class="list-box">
				<c:forEach items="${requestScope.list}" var="item" varStatus="status">
					<div class="list-item">
						<div class="pro clear">
							<a href="${contextPath }/views/${item.prodId}" class="pro-img" target="_blank">
								<img src="<ls:photo item='${item.pic}'/>">
							</a>
							<div class="pro-txt">
								<a href="${contextPath }/views/${item.prodId}" class="name" target="_blank">${item.name}</a>
								<p class="price" data="${item.cash}"><em class="font-family:Arial;">&yen;</em>${item.cash}</p>
							</div>
						</div>
						<div class="pro-btn">
							<a href="javascript:void(0)" class="btn sendProd" data-prodId="${item.prodId }">发送</a>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
</div>
<!-- /本店商品 -->
<script type="text/javascript">
var curPageNo = 1;//当前页,初始化为第一页
var totalPage = parseInt('${pageCount}');//总页数var curPageNo = 1;//当前页,初始化为第一页
$(function(){
	//下拉分页
	$(".list-box").scroll(function(){
        if ( $(this)[0].scrollTop + $(".list-box").height()>= $(this)[0].scrollHeight){
        	curPageNo = curPageNo + 1;
			if(curPageNo <= totalPage){
				var name = $.trim($("#name").val());
				 $.ajax({
					url: contextPath + "/customerService/prod", 
					type: "post", 
					data: {"curPageNO": curPageNo, "flag":1,"name":name},
					dataType: "html",
					success:function(result){
					    $(".list-box").append(result); 
					}
				});
			}
        }
    });
	
	$(".checkProd").click(function(){
		$(".list-box").scrollTop(0, 0);//必须回到顶部原位
		var name = $.trim($("#name").val());
		$.ajax({
			url: contextPath + "/customerService/prod", 
			data: {"curPageNO": 1, "flag":1,"name":name},
			type: "post",
			dataType: "html",
			success: function(result) {
				$(".list-box").html(result); 
				curPageNo = 1;
				totalPage = $("#totalPage").val();
			}
		});
		
	});
	
	
})
</script>