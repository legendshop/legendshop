<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>账户安全 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/securityCenter.css'/>" rel="stylesheet" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/validateChanges.css'/>" rel="stylesheet" />
</head>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;">     
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con"  id="content">
			<div class="o-mt">
						<h2>安全中心</h2>
			</div>
		<div class="right">
			
			<div class="m m3  safe-sevi04">
				<div class="mc">
					<s class="icon-succ02"></s>
					<div class="jd-add">
						<h3><span class="ftx-02">恭喜您，修改成功！</span></h3>
    					<p class="ftx-03">最新安全评级：<strong class="rank-text ftx-02" id="secLevel">较高</strong><i class="icon-rank04"></i></p>
    					<p class="ftx-03">您的帐户安全级还能提升哦，快去<a href="${contextPath}/p/security">安全中心</a>完善其它安全设置提高评级吧！</p>											</div>
				</div>
			</div>
		</div>

</div>

 <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
<script type="text/javascript">
	$(document).ready(function() {
		if(${secLevel}==1){
			$("#secLevel").html("很危险");
			$("#secLevel").attr("style","color:#B22222");
			$(".icon-rank04").attr("style","background-position:0 -30px");
		}else if(${secLevel}==2){
			$("#secLevel").html("中级");
			$("#secLevel").attr("style","color:#FF6600");
			$(".icon-rank04").attr("style","background-position:0 -45px");
		}else if(${secLevel}==3){
			$("#secLevel").html("高级");
			$("#secLevel").attr("style","color:#009900");
			$(".icon-rank04").attr("style","background-position:0 -60px");
		}else if(${secLevel}==4){
			$("#secLevel").html("很安全");
			$("#secLevel").attr("style","color:#009900");
			$("#promptInfo").html("VeryGood!");
			$("#promptInfo").attr("style","color:#00C5CD");
			$(".icon-rank04").attr("style","background-position:0 -75px");
		}
	});	
</script>
</body>
</html>