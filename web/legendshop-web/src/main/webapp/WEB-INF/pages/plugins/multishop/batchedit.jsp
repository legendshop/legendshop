<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<link type="text/css"
	href="<ls:templateResource item='/resources/templets/css/publishProd1${_style_}.css'/>"
	rel="stylesheet" />
<link type="text/css"
	href="<ls:templateResource item='/resources/templets/css/multishop/seller.css'/>"
	rel="stylesheet" />

<form:form action="${contextPath}/s/shopUsers/save" method="post"
	id="from1" target="_parent">
	<input type="hidden" value="${prodIds}" name="id" id="prodIds">
	<div class="form J_DetectTrigger">
		<ul style="list-style: none; margin: 34px 0px 0px 40px; line-height:40px"
			class="ul-radio ul-radio-vertical">
			<li>一级分类：<select style="box-sizing: border-box;height:30px;width:158px;margin-left: 10px" class="combox sele" id="catId"
				name="shopFirstCatId" requiredTitle="-- 一级分类 -- "
				childNode="nextCatId" selectedValue="${productDto.shopFirstCatId}"
				retUrl="${contextPath}/shopCat/loadCat"></select></li>
				
				<li>二级分类：<select
				style="box-sizing: border-box;height:30px;width:158px; margin-left: 10px" class="combox sele"
				id="nextCatId" name="shopSecondCatId" requiredTitle="-- 二级分类 --"
				showNone="false" parentValue="${productDto.shopFirstCatId}"
				selectedValue="${productDto.shopSecondCatId}" childNode="subCatId"
				retUrl="${contextPath}/shopCat/loadNextCat/{value}"></select></li>
				<li>三级分类：<select
				style="box-sizing: border-box;height: 30px;width:158px;margin-left: 10px" class="combox sele"
				id="subCatId" name="shopThirdCatId" requiredTitle="-- 三级分类 --"
				showNone="false" parentValue="${productDto.shopSecondCatId}"
				selectedValue="${productDto.shopThirdCatId}"
				retUrl="${contextPath}/shopCat/loadSubCat/{value}"></select></li>
		</ul>
	</div>
	<div class="btn-content" style="margin: 25px 0px 0px 140px">
		<input onclick="batchEdit();" type="button" value="保存"
			class="btn-r small-btn" /> <input onclick="closeDialog();"
			style="margin-left: 20px;" type="button" value="取消"
			class="btn-g small-btn" />
	</div>
</form:form>


<script
	src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script type="text/javascript">
	var prodIds = '${selAry}';
	$(function() {
		//三级联动
		$("select.combox").initSelect();
	});
	function closeDialog() {
		var index = parent.layer.getFrameIndex('batchEdit');
		parent.layer.close(index);
	}

	function batchEdit() {
		var catId = $("#catId").val() == null || $("#catId").val() == "" ? -1
				: $("#catId").val();
		var nextCatId = $("#nextCatId").val() == null
				|| $("#nextCatId").val() == "" ? -1 : $("#nextCatId").val();
		var subCatId = $("#subCatId").val() == null
				|| $("#subCatId").val() == "" ? -1 : $("#subCatId").val();
		if (catId == -1) {
			layer.alert("一级类目必须填写")
			return false;
		}
		var load = layer.load(1, {
			shade : false
		})
		$("input[type=button]").css('background-color', "gray")
		$("input[type=button]").attr('disabled', true)
		$.ajax({
			url : '/s/batchEditUpdate',
			data : {
				"prodIds" : $("#prodIds").val(),
				"shopFirstCatId" : catId,
				"shopSecondCatId" : nextCatId,
				"shopThirdCatId" : subCatId,
			},
			type : 'post',
			dataType : 'json',
			async : true, //默认为true 异步   
			error : function(jqXHR, textStatus, errorThrown) {
				alert(textStatus, errorThrown);
			},
			success : function(result) {
				if (result == 'true') {
					layer.close(load);
					parent.layer.msg('修改成功', {
						icon : 1,
						time : 1500
					//2秒关闭（如果不配置，默认是3秒）
					}, function() {
						parent.window.location.reload();
					});
					return;
				} else {
					layer.close(load);
					parent.layer.alert('数据错误，修改失败！', {
						icon : 2,
						time : 6000
					//2秒关闭（如果不配置，默认是3秒）
					}, function() {
						parent.window.location.reload();
					});
					return;
				}

			}
		});

	}
</script>


