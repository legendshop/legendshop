<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<div id="bd">
			<div class="yt-wrap cartnew-main ">
				<div class="my-car-tit">
					<p>我的购物车</p>
					<span>查看您的购物车商品清单，勾选商品，进入下一步操作</span>
				</div>
				<table class="cartnew_table">
					<thead>
						<tr>
							<c:if test="${not empty requestScope.userShopCartList || not empty requestScope.userShopCartList.shopCarts}">
								<th align="left" class="th1"><input type="checkbox" checked="checked" id="checkbox" class="selectAll" style="margin-left: 15px;"> 全选</th>
							</c:if>
							<th class="th2">商品</th>
							<th class="th4">数量</th>
							<th class="th3">单价（元）</th>
							<th class="th5">小计（元）</th>
							<th class="th6">操作</th>
						</tr>
					</thead>
				</table>
				<div id="cart-table-div">
					<c:choose>
						<c:when test="${empty requestScope.userShopCartList || empty requestScope.userShopCartList.shopCarts}">
							<div class="car-empty">
								<img src="../resources/templets/images/car-empty.png" alt="">
								<p>亲，您的购物车空空如也，快去挑选商品吧！</p>
								<a href="${contextPath}/">去逛逛</a>
							</div>
						</c:when>
						<c:otherwise>
							<%-- 循环每个商家 --%>
							<c:forEach items="${requestScope.userShopCartList.shopCarts}" var="carts" varStatus="status">
								<div class="sho-cha">
									<p>
										<c:choose>
											<c:when test="${carts.shopStatus==0}">
												<input type="checkbox" disabled="disabled" style="margin-left: 15px;">
											</c:when>
											<c:otherwise>
												<input type="checkbox" name="steelShoppingItms" id="steelShoppingItms_${status.index}" class="select_shop" for="${status.index}" style="margin-left: 15px;">
											</c:otherwise>
										</c:choose>
										店铺：
										<a target="_blank" title="${carts.shopName}" href="${contextPath}/store/${carts.shopId}">${carts.shopName}</a>
					
										<c:if test="${not empty carts.mailfeeStr}">
											<span style="color:#e5004f;background: #fff;">[${carts.mailfeeStr}]</span>
										</c:if>
									</p>
								</div>
								<%-- 循环商家的每个活动 --%>
								<c:forEach items="${carts.marketingDtoList}" var="marketing" varStatus="markSts">
									<c:if test="${not empty marketing.hitCartItems}">
									<c:if test="${not empty marketing.promotionInfo}">
										<div class="item-header">
											<div class="f-txt">
												<span class="full-icon">
													<c:choose>
													<c:when test="${marketing.type==0}">
														满减
													</c:when>
													<c:when test="${marketing.type==1}">
														满折
													</c:when>
													<c:when test="${marketing.type==2}">
														限时
													</c:when>
													</c:choose>
												</span>
												${marketing.promotionInfo}
											</div>
											<c:if test="${marketing.totalOffPrice>0}">
												<div class="f-price">
													<strong>¥${marketing.hitProdTotalPrice}</strong>
													<div class="ar">
														<span class="ftx-01">减：¥${marketing.totalOffPrice}</span>
													</div>
												</div>
											</c:if>
										</div>
									</c:if>
									<table cellspacing="0" cellpadding="0" class="cartnew_table-two steelShoppingCarTableIds_${status.index}">
										<tbody>
											<%-- 循环活动下的每个商品 --%>
											<c:forEach items="${marketing.hitCartItems}" var="cart" varStatus="itemStatus">
					
												<c:choose>
													<c:when test="${carts.shopStatus==0 || cart.status!=1 || cart.basketCount <= 0}">
														<tr style="background: #e2e2e2 none repeat scroll 0 0;">
													</c:when>
													<c:when test="${itemStatus.last}">
														<tr class="i-last">
													</c:when>
													<c:when test="${not empty marketing.promotionInfo}">
														<tr class="i-normal">
													</c:when>
													<c:otherwise>
														<tr>
													</c:otherwise>
												</c:choose>
												<td style="width: 650px;" class="textleft">
													<c:choose>
														<c:when test="${carts.shopStatus==0 || cart.status!=1 || cart.basketCount <= 0 || cart.isFailure}">
															<input name="dd" type="checkbox" disabled="disabled" class="steelShoppingCarIds_${status.index}">
														</c:when>
														<c:otherwise>
															<input name="dd" <c:if test="${cart.checkSts==1}">checked="true"</c:if>
																class="select_prod steelShoppingCarIds_${status.index}" for="${status.index}" type="checkbox"  itemkey="${cart.basketId}" prodId="${cart.prodId}"
																skuId="${cart.skuId}" supportStore="${cart.supportStore}" storeId ="${cart.storeId}" >
														</c:otherwise>
													</c:choose> 
													<a class="a_e" target="_blank" href="${contextPath}/views/${cart.prodId}">
														<img style="max-width: 121px;min-height: 95px;max-height: 121px;" src="<ls:images item="${cart.pic}" scale="2"/>">
													</a>
													<ul>
														<li><a class="a_e" target="_blank" href="${contextPath}/views/${cart.prodId}">${cart.prodName}</a></li>
														<li><c:if test="${not empty cart.cnProperties}">${cart.cnProperties}</c:if></li>
														<c:choose>
															<c:when test="${cart.isFailure }">
																<li><span class="notChecked">宝贝失效</span></li>
															</c:when>
															<c:when test="${cart.basketCount <= 0}">
																<li><span class="notChecked">库存不足</span></li>
															</c:when>
														</c:choose>
														<c:if test="${cart.transportFree eq 1}">
															<li item="supportStore" style="color: #e5004f;">${cart.shippingStr}</li>
														</c:if>
														<c:if test="${cart.supportStore}">
															<li item="supportStore"><i>*</i>(该商品支持<strong style="color: #e5004f">门店自提</strong>服务)   
																<c:choose>
																	<c:when test="${not empty cart.storeName}">
																		已选择<a class="storeInfo" href="javascript:void(0)" onclick="shopStoreServer('${cart.prodId}','${cart.skuId}','${cart.basketCount}');" storeAddr="${cart.storeAddr}">${cart.storeName}</a>
																	</c:when>
																	<c:otherwise>
																		<a class="storeInfo" href="javascript:void(0)" onclick="shopStoreServer('${cart.prodId}','${cart.skuId}','${cart.basketCount}');" storeAddr="${cart.storeAddr}">选择门店</a>
																	</c:otherwise>
																</c:choose>
															</li>
														</c:if>
														<li id="rule_${cart.prodId}_${cart.skuId}"><c:if test="${not empty cart.cartMarketRules}">
																<c:forEach items="${cart.cartMarketRules}" var="rule">
																	<span>优惠信息</span>
																	<em>${rule.promotionInfo}</em>
																	<br>
																</c:forEach>
															</c:if>
														</li>
													</ul>
												</td>
												<td style="width: 120px;text-align: center;"><c:choose>
														<c:when test="${carts.shopStatus==0 || cart.status!=1 || cart.basketCount <=0}">
															<span class="tb-amount-widget mui-amount-wrap">
																<input type="text" readonly="readonly" value="${cart.basketCount}" maxlength="4" class="tb-text mui-amount-input">
																<span class="mui-amount-btn">
																	<span class="mui-amount-increase">∧</span>
																	<span class="mui-amount-decrease">∨</span>
																</span>
																<span class="mui-amount-unit">件</span>
															</span>
														</c:when>
														<c:otherwise>
															<span class="tb-amount-widget mui-amount-wrap">
																<input type="text" title="请输入购买量" discountPrice="${cart.discountPrice}" id="number_${cart.prodId}_${cart.skuId}"
																	onblur="blurCalculate(this,'${cart.prodId}','${cart.skuId}','${cart.stocks}');" value="${cart.basketCount}" maxlength="4" class="tb-text mui-amount-input"  onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')">
																<span class="mui-amount-btn">
																	<span class="mui-amount-increase" onclick="addQuanlity(this,'${cart.prodId}','${cart.skuId}','${cart.stocks}');">∧</span>
																	<span class="mui-amount-decrease" onclick="subQuanlity(this,'${cart.prodId}','${cart.skuId}','${cart.stocks}');">∨</span>
																</span>
																<span class="mui-amount-unit">件</span>
															</span>
														</c:otherwise>
													</c:choose>
												</td>
												<td style="width: 120px; text-align: center;"><c:if test="${cart.price-cart.promotionPrice > 0}">
														<del style="color:#999;">
															￥
															<fmt:formatNumber pattern="0.00" maxFractionDigits="2" groupingUsed="false" value="${cart.price}" />
														</del>
														<br>
													</c:if> <span class="_price">
														￥
														<fmt:formatNumber pattern="0.00" maxFractionDigits="2" groupingUsed="false" value="${cart.promotionPrice}" />
													</span>
												</td>
												<td style="width: 140px; text-align: center;"><span style="font-size:14px; color:#e5004f;" class="subtotal" id="total_money_${cart.prodId}_${cart.skuId}">
														￥
														<fmt:formatNumber pattern="0.00" maxFractionDigits="2" groupingUsed="false" value="${cart.totalMoeny}" />
													</span>
												</td>
												<td style="text-align: center;" class="bluelink"><a href="javascript:void(0);" onclick="deleteShopCart('${cart.prodId}','${cart.skuId}')">
														<span>删除</span>
													</a></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
									</c:if>
								</c:forEach>
					
					
					
								<c:if test="${carts.supportStore}">
									<div class="ncc-chain-tip">
										<strong>*</strong>该订单中包含支持“门店自提”服务的商品，如需到店自提，请选择提交<i class="icon-truck"></i>
										<a nc_type="chain" href="javascript:void(0)" shopId="${carts.shopId}">门店自提订单</a>
									</div>
								</c:if>
							</c:forEach>
					
							<div class="cartnew-sum">
								<div class="left batch_div">
									<input type="checkbox" checked="checked" style="margin-left: 15px;" class="put selectAll" id="checkbox2">
									<span style="margin-left:5px;margin-right:20px;">全选</span>
									<a class="btn_h" href="javascript:clearShopCart();">清空购物车</a>
								</div>
								<div class="right">
									<div class="price-sum">
										<div>
											<span class="p-txt">总价：</span>
											<strong id="allProductTotalAmount">￥<fmt:formatNumber value="${requestScope.userShopCartList.orderActualTotal}" type="currency" pattern="0.00"></fmt:formatNumber> </strong> 
											<c:if test="${requestScope.userShopCartList.allDiscount>0}">
												<br>
												<span class="p-txt">已节省：</span>
												<span class="price totalRePrice">-￥<fmt:formatNumber value="${requestScope.userShopCartList.allDiscount}" type="currency" pattern="0.00"></fmt:formatNumber> </span>
											</c:if>
										</div>
									</div>
									<div class="prod-sum">
										<span>
											已选择<strong id="allCount" style="font-size:14px;">${requestScope.userShopCartList.orderTotalQuanlity}</strong>件商品
										</span>
									</div>
								</div>
							</div>
							<div class="cartnew-submit">
								<a href="javascript:void(0);" id="submitOrder" onclick="summitShopCart();" class="topay">去结算&nbsp;&nbsp;>></a>
								<a href="${contextPath}/" class="buy"><<&nbsp;&nbsp;继续购物</a>
							</div>
						</c:otherwise>
					</c:choose>
		</div>
</div>