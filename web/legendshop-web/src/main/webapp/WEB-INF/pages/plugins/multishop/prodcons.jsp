<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>商品咨询-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-consult${_style_}.css'/>" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
			<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">商品咨询</span>
				</p>
			</div>
		<%@ include file="included/sellerLeft.jsp" %>
			<div class="seller-consult">
				<div class="pagetab2">
					<ul id="listType">
						<li class="on"><span>商品咨询</span></li>
					</ul>
	   			</div>
				<!-- 搜索 -->		
				<form:form action="${contextPath}/s/prodcons" method="get" id="from1">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
					<div class="sold-ser sold-ser-no-bg clearfix">
						<div class="fr">
							<div class="item">
								<span>回复状态：</span>
								<select name="replySts" class="text item-sel" style="width:100px;height:30px;">
									 <ls:optionGroup type="select" required="false" cache="true"
				               			beanName="YES_NO" selectedValue="${productConsult.replySts}"/>
								</select>
							</div>
							<div class="item">
								<span>咨询类型：</span>
								<select name="pointType" class="text item-sel" style="width:100px;height:30px;">
									<ls:optionGroup type="select" required="false" cache="true"
			               				beanName="CONSULT_TYPE" selectedValue="${productConsult.pointType}"/>
								</select>
							</div>
							<div class="item">
								<span>用户名：</span>
								<input type="text" value="${productConsult.askUserName}" class="text item-inp"  name="askUserName" style="width:110px;">
							</div>
							<div class="item">
								<span>商品名称：</span>
								<input type="text" value="${productConsult.prodName}" class="text item-inp"  name="prodName" style="width:225px;">
								<input type="button" onclick="search()" class="search bti btn-r" id="btn_keyword" value="查 询" >
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</form:form>
				<table class="consult-table sold-table">
					<tr class="consult-tit">
						<th class="check">
							<label class="checkbox-wrapper" style="float:left;margin-left:9px;">
								<span class="checkbox-item">
									<input type="checkbox" id="checkbox" class="checkbox-input selectAll"/>
									<span class="checkbox-inner" style="margin-left:9px;"></span>
								</span>
						   </label>
						</th>
						<th width="150">咨询商品</th>
						<th width="210">商品名称</th>
					 	<th width="60">咨询类型</th>
                        <th width="100">用户名</th>
						<th width="210" style="word-wrap:break-word;word-break:break-all;max-width: 300px">咨询内容</th>
						<th width="240" style="word-wrap:break-word;word-break:break-all;max-width: 300px;">回复内容</th>
					</tr>				
					<c:choose>
			           <c:when test="${empty requestScope.list}">
			              <tr>
						 	 <td colspan="20">
						 	 	 <div class="warning-option"><span style="text-align: center; ">没有符合条件的信息</span></div>
						 	 </td>
						 </tr>
			           </c:when>
			           <c:otherwise>
			           		<c:forEach items="${requestScope.list}" var="ProductConsult" varStatus="status">
								<tr class="detail">
									<td class="check" height="80" >
										<label class="checkbox-wrapper" style="float:left;margin:9px;">
											<span class="checkbox-item">
												<input type="checkbox" name="array" class="checkbox-input selectOne" value="${ProductConsult.consId}" onclick="selectOne(this);"/>
												<span class="checkbox-inner" style="margin:9px;"></span>
											</span>
									   	</label>
									</td>
									<td class="det-img"><a href="${contextPath}/views/${ProductConsult.prodId}" target="_blank"><img src="<ls:images item='${ProductConsult.pic}' scale='3' />"></a></td>
									<td><a href="${contextPath}/views/${ProductConsult.prodId}" target="_blank">${ProductConsult.prodName}</a></td>	
									<td><ls:optionGroup type="label" required="false" cache="true"  beanName="CONSULT_TYPE" selectedValue="${ProductConsult.pointType}" defaultDisp=""/></td>
		                            <td>${ProductConsult.askUserName}</td>
								    <td style="word-wrap:break-word;word-break:break-all">${ProductConsult.content}(<fmt:formatDate value="${ProductConsult.recDate}"  pattern="yyyy-MM-dd HH:mm:ss" />)</td>
								    <td class="mal"  style="word-wrap:break-word;word-break:break-all">
										<c:choose>
											<c:when test="${ProductConsult.replySts==1}">
												${ProductConsult.answer} (<fmt:formatDate value="${ProductConsult.answertime}"  pattern="yyyy-MM-dd HH:mm:ss" />)
											</c:when>
											<c:otherwise>
												<center><input class="btn-r" type="button" value="回复" onclick="reply(${ProductConsult.consId})"/></center>
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</c:forEach>
			           </c:otherwise>
		           </c:choose>
				</table>
				<div class="page clearfix">
					<c:if test="${not empty requestScope.list}">
						<a class="btn-r" href="javascript:void(0);" onclick="deleteAction('deletemulticonsult');">批量删除</a>
					</c:if>
	     			 <div class="p-wrap">
	            		 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	     			 </div>
  				</div>
			</div>
		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/prodcons.js'/>"></script>
	<script type="text/javascript">
		var contextPath="${contextPath}";
		function search(){
		  	$("#from1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		  	$("#from1")[0].submit();
		}
	</script>
</body>
</html>