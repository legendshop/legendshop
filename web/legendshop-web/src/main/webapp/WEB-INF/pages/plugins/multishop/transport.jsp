<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file='/WEB-INF/pages/common/taglib.jsp' %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <title>运费模板-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-common${_style_}.css'/>" rel="stylesheet"/>
</head>
<style>
    .layui-layer-btn .layui-layer-btn0 {
        border-color: #e5004f !important;
        background-color: #e5004f !important;
    }
</style>
<body class="graybody">
<div id="doc">
    <%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
    <div class="w1190" style="overflow:visible">
        <div class="seller-cru">
            <p>
                您的位置>
                <a href="${contextPath}/home">首页</a>>
                <a href="${contextPath}/sellerHome">卖家中心</a>>
                <span class="on">运费模板</span>
            </p>
        </div>
        <%@ include file="included/sellerLeft.jsp" %>

        <div id="Content" style="width:960px;float:right;margin-top:20px;background: #fff;position:relative;">
            <div class="main"></div>
            <div class="seller-com-nav">
                <ul>
                    <li id="ulansportList"><a href="${contextPath}/s/transport">运费模板列表</a></li>
                    <li class="on" id="ulansportConfigure"><a href="#">运费模板设置</a></li>
                </ul>
            </div>

            <form:form action="${contextPath}/s/transport/save" method="post" id="form1">
            <input id="id" name="id" value="${transport.id}" type="hidden">
            <input type="hidden" name="mail_city_count" id="mail_city_count"/>
            <input type="hidden" name="express_city_count" id="express_city_count"/>
            <input type="hidden" name="ems_city_count" id="ems_city_count"/>
            <div align="center">
                <table cellspacing="0" cellpadding="0" border="0" style=" margin-top:30px;" class="sendmes_table">
                    <tr>
                        <td width="80" align="right">
                            模板名称: <font color="ff0000">*</font>
                        </td>
                        <td align="left" width="720">
                            <input id="transName" name="transName" value="${transport.transName}" type="text" style="padding:5px;">
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            发货时间: <font color="ff0000">*</font>
                        </td>
                        <td align="left">
                            <select id="transTime" name="transTime" class="combox sele">
                                <ls:optionGroup type="select" required="true" cache="true" beanName="TRANSPORT_TIME" selectedValue="${transport.transTime}"/>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            区域限售: <font color="ff0000">*</font>
                        </td>
                        <td align="left" width="720">
                            <label class="radio-wrapper <c:if test="${transport.isRegionalSales eq 0 || empty transport.isRegionalSales}">radio-wrapper-checked</c:if>">
					<span class="radio-item">
						<input type="radio" onclick="radioChecked(this);" name="isRegionalSales" id="isRegionalSales_0"
                               <c:if test="${transport.isRegionalSales eq 0 || empty transport.isRegionalSales}">checked="checked"</c:if> value="0" class="radio-input"/>
						<span class="radio-inner"></span>
					</span>
                                <span class="radio-txt">不支持</span>
                            </label>
                            <label class="radio-wrapper <c:if test="${transport.isRegionalSales eq 1 }">radio-wrapper-checked</c:if>">
					<span class="radio-item">
						<input type="radio" onclick="radioChecked(this);" name="isRegionalSales" id="isRegionalSales_1"
                               <c:if test="${transport.isRegionalSales eq 1 }">checked="checked "</c:if> value="1" class="radio-input"/>
						<span class="radio-inner"></span>
					</span>
                                <span class="radio-txt">支持</span>
                            </label>
                                <%-- <input id="transName" name="transName" value="${transport.transName}" type="text"  style="padding:5px;"> --%>
                                <%-- <input type="radio" name="isRegionalSales" <c:if test="${transport.isRegionalSales eq 0}">checked="checked"</c:if>  value="0"/>不支持
                                 <input type="radio" name="isRegionalSales" <c:if test="${transport.isRegionalSales eq 1 }">checked="checked "</c:if> value="1"/>支持 --%>
                            <span class="salesEroor"></span></br>
                            <span>如果支持区域限售，商品只能在设置了运费的指定地区城市销售</span>
                        </td>
                    </tr>

                    <tr>
                        <td align="right">
                            计价方式: <font color="ff0000">*</font>
                        </td>
                        <td align="left">
                            <input type="hidden" value="${transport.transType}" id="transType"/>
                            <c:choose>
                                <c:when test="${ not empty transport.transType}">
                                    <%-- <input type="radio"  name="transType" value="1" <c:if test="${transport.transType ==1}">checked="checked"</c:if> />按件数 &nbsp;
                                       <input type="radio"  name="transType" value="2" <c:if test="${transport.transType ==2}">checked="checked"</c:if> />按重量&nbsp;
                                       <input type="radio"  name="transType" value="3" <c:if test="${transport.transType ==3}">checked="checked"</c:if> />按体积 --%>
                                    <label class="radio-wrapper <c:if test="${transport.transType ==1}">radio-wrapper-checked</c:if>">
								<span class="radio-item">
									<input type="radio" onclick="radioChecked(this);" name="transType" value="1" id="transType_1"
                                           <c:if test="${transport.transType ==1}">checked="checked"</c:if> value="1" class="radio-input"/>
									<span class="radio-inner"></span>
								</span>
                                        <span class="radio-txt">按件数 &nbsp;</span>
                                    </label>
                                    <label class="radio-wrapper <c:if test="${transport.transType ==2}">radio-wrapper-checked</c:if>">
								<span class="radio-item">
									<input type="radio" onclick="radioChecked(this);" name="transType" id="transType_2"
                                           <c:if test="${transport.transType ==2}">checked="checked"</c:if> value="2" class="radio-input"/>
									<span class="radio-inner"></span>
								</span>
                                        <span class="radio-txt">按重量&nbsp;</span>
                                    </label>
                                    <label class="radio-wrapper <c:if test="${transport.transType ==3}">radio-wrapper-checked</c:if>">
								<span class="radio-item">
									<input type="radio" onclick="radioChecked(this);" name="transType" id="transType_3"
                                           <c:if test="${transport.transType ==3}">checked="checked"</c:if> value="3" class="radio-input"/>
									<span class="radio-inner"></span>
								</span>
                                        <span class="radio-txt">按体积</span>
                                    </label>
                                </c:when>
                                <c:otherwise>
                                    <!-- <input type="radio" name="transType" value="1" checked="checked" />按件数 &nbsp;
                                    <input type="radio" name="transType" value="2" />按重量&nbsp;
                                    <input type="radio" name="transType" value="3" />按体积 -->
                                    <label class="radio-wrapper radio-wrapper-checked">
								<span class="radio-item">
									<input type="radio" onclick="radioChecked(this);" name="transType" value="1" id="transType_1" checked="checked" value="1" class="radio-input"/>
									<span class="radio-inner"></span>
								</span>
                                        <span class="radio-txt">按件数 &nbsp;</span>
                                    </label>
                                    <label class="radio-wrapper">
								<span class="radio-item">
									<input type="radio" onclick="radioChecked(this);" name="transType" id="transType_2" value="2" class="radio-input"/>
									<span class="radio-inner"></span>
								</span>
                                        <span class="radio-txt">按重量&nbsp;</span>
                                    </label>
                                    <label class="radio-wrapper">
								<span class="radio-item">
									<input type="radio" onclick="radioChecked(this);" name="transType" id="transType_3" value="3" class="radio-input"/>
									<span class="radio-inner"></span>
								</span>
                                        <span class="radio-txt">按体积</span>
                                    </label>
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top">
                            运送方式:
                        </td>
                        <td>
                            <label class="checkbox-wrapper <c:if test="${transport.transExpress}">checkbox-wrapper-checked</c:if>" style="float:left;padding-bottom:10px;">
						<span class="checkbox-item">
							<input name="transExpress" type="checkbox" id="transExpress" class="checkbox-input" trans="trans"
                                   <c:if test="${transport.transExpress}">checked="checked"</c:if>  />
							<span class="checkbox-inner"></span>
						</span>
                                <span>快递</span>
                            </label>
                            <div id="trans_detail">
                                <!--平邮 开始-->
                                    <%-- <div class="db_box_main" >
                               <div class="db_box_main_input">
                                 <label>
                                   <input name="transMail" type="checkbox" id="transMail"  class="freightMode" trans="trans" <c:if test="${transport.transMail}">checked="checked"</c:if> /> 平邮
                                  </label>
                               </div>
                               <c:choose>
                                   <c:when test="${transport.transMail}">
                                       <div class="db_box_main_rdinary" id="transMail_info" style="display: block;">
                                   </c:when>
                                   <c:otherwise>
                                       <div class="db_box_main_rdinary" id="transMail_info" style="display: none;">
                                   </c:otherwise>
                               </c:choose>
                              <div class="db_box_main_rdinary" id="transMail_info" style="display: none;">
                                 <div class="rdinary_top">默认运费：
                                   <input name="mailList[0].id" value="${transport.mailList[0].id}" type="hidden" />
                                   <input name="mailList[0].transWeight" style="width: 43px;height: 17px;" type="text" id="mailTransWeight" value="${transport.mailList[0].transWeight}" size="5" /><span>件内，</span>
                                   <input name="mailList[0].transFee" style="width: 60px;height: 17px;" type="text" id="mailTransFee" value="${transport.mailList[0].transFee}" size="8" /><span> 元， 每增加</span>
                                   <input name="mailList[0].transAddWeight"  style="width: 43px;height: 17px;" type="text" id="mailTransAddWeight" value="${transport.mailList[0].transAddWeight}" size="5" /><span>件，增加运费</span>
                                   <input name="mailList[0].transAddFee"  style="width: 60px;height: 17px;" type="text" id="mailTransAddFee" value="${transport.mailList[0].transAddFee}" size="8" /><span>元</span>
                                 </div>
                               <div  id="mail_trans_city_info" >
                                   <table id="mailTable" style="width:100%; border-collapse: collapse;margin-top: 10px;border: 1px solid #ddd;" class="col-table-css">
                                   <thead>
                                     <tr>
                                           <td width="40%" style="background-color: #f2f2f2;">运送到</td>
                                           <td width="12%" style="background-color: #f2f2f2;" id="mailTransWeightType">首件(件)</td>
                                           <td width="12%" style="background-color: #f2f2f2;">首费(元)</td>
                                           <td width="12%" style="background-color: #f2f2f2;" id="mailTransAddWeightType">续件(件)</td>
                                           <td width="12%" style="background-color: #f2f2f2;">续费(元)</td>
                                           <td width="7%" style="background-color: #f2f2f2;">操作</td>
                                     </tr>
                                     </thead>
                                     <tbody>
                                             <c:forEach  items="${transport.mailList}" var="transfee" varStatus="status" begin="1">
                                                 <tr index="${status.index}">
                                                 <input name="mailList[${status.index}].id" value="${transfee.id}" type="hidden" />
                                                 <td>
                                                         <span class="width2">
                                                             <i><input id="trans_ck_${status.index}" name="trans_ck_${status.index}" type="checkbox" value="" style="display:none;" /></i>
                                                             <input id="mail_city_ids${status.index}" name="mailList[${status.index}].cityIdList" type="hidden" value='${transfee.cityIds}' />
                                                             <a  href="javascript:void(0);" onclick="edit_trans_city(this);" trans_city_type="mail">编辑</a>
                                                         </span>
                                                         <span class="width1" id="mail${status.index}">
                                                             <c:forEach var="city" items="${transfee.cityNameList}" varStatus="citysStatus">${city}、</c:forEach>
                                                         </span>
                                                 </td>
                                                 <td>
                                                         <input type="text" value='${transfee.transWeight}' class="in" id="mail_trans_weight${status.index}" name="mailList[${status.index}].transWeight" />
                                                 </td>
                                                 <td>
                                                     <input type="text" value='${transfee.transFee}' class="in" id="mail_trans_fee${status.index}" name="mailList[${status.index}].transFee" />
                                                 </td>
                                                 <td>
                                                         <input type="text" value='${transfee.transAddWeight}' class="in" id="mail_trans_add_weight${status.index}" name="mailList[${status.index}].transAddWeight" />
                                                 </td>
                                                 <td>
                                                         <input type="text" value='${transfee.transAddFee}' class="in" id="mail_trans_add_fee${status.index}" name="mailList[${status.index}].transAddFee" />
                                                 </td>
                                                 <td>
                                                         <span class="width3"><a href="javascript:void(0);" onclick="if(confirm('确认要删除当前地区的设置么？'))remove_trans_city(this)">删除</a></span>
                                                 </td>
                                                 </tr>
                                             </c:forEach>
                                     </tbody>
                                   </table>
                                  </div>
                                 <div class="rdinary_ul_bottom" style="display:none;" id="mail_trans_city_op">
                                   <label>
                                     <input name="mail_trans_all" id="mail_trans_all" type="checkbox" value="" />全选
                                    </label>
                                   &nbsp; <a href="javascript:void(0);" id="batch_config_mail" style="display:none;">批量设置</a> &nbsp; <a href="javascript:void(0);" id="batch_del_mail">批量删除</a>
                                 </div>
                                     <div class="rdinary_ul_bottom" >
                                         <a href="javascript:void(0);" onclick="trans_city('mail')">为指定地区城市设置运费</a>&nbsp;
                                         <a  href="javascript:void(0);" id="batch_set_mail" trans_type="mail">批量操作</a>&nbsp;
                                         <a href="javascript:void(0);" id="batch_cancle_mail" trans_type="mail" style="display:none;">取消批量</a>
                                     </div>
                                   </div> --%>
                            </div>
                            <!--平邮 结束-->

                            <!--快递 开始-->
                            <span class="transError"></span>
                            <div class="db_box_main" style="margin-top:21px;">
                                <c:choose>
                                <c:when test="${transport.transExpress}">
                                <div class="db_box_main_rdinary" id="transExpress_info" style="display: block;margin-top: 21px;">
                                    </c:when>
                                    <c:otherwise>
                                    <div class="db_box_main_rdinary" id="transExpress_info" style="display: none;">
                                        </c:otherwise>
                                        </c:choose>
                                            <%--                    <div class="db_box_main_rdinary" id="transExpress_info" style="display: none;">--%>
                                        <div class="rdinary_top">默认运费：
                                            <input name="expressList[0].id" value="${transport.expressList[0].id}" type="hidden"/>
                                            <input name="expressList[0].transWeight" style="width: 43px;height: 17px;" type="text" id="expressTransWeight"
                                                   value="${empty transport.expressList[0].transWeight?0:transport.expressList[0].transWeight}" size="5"/><span>件内，</span>
                                            <input name="expressList[0].transFee" style="width: 60px;height: 17px;" type="text" id="expressTransFee"
                                                   value="${empty transport.expressList[0].transFee?0:transport.expressList[0].transFee}" size="8"/><span>元， 每增加</span>
                                            <input name="expressList[0].transAddWeight" style="width: 43px;height: 17px;" type="text" id="expressTransAddWeight"
                                                   value="${empty transport.expressList[0].transAddWeight?0:transport.expressList[0].transAddWeight}" size="5"/><span>件，增加运费</span>
                                            <input name="expressList[0].transAddFee" style="width: 60px;height: 17px;" type="text" id="expressTransAddFee"
                                                   value="${empty transport.expressList[0].transAddFee?0:transport.expressList[0].transAddFee}" size="8"/><span>元</span>
                                        </div>
                                        <div id="express_trans_city_info">
                                            <table id="expressTable" style="width:100%;border-collapse: collapse;margin-top: 10px;border: 1px solid #ddd;" class="col-table-css">
                                                <thead>
                                                <tr>
                                                    <td width="40%" align="center" style="background-color: #f2f2f2;">运送到</td>
                                                    <td width="12%" style="background-color: #f2f2f2;" id="expressTransWeightType">首件(件)</td>
                                                    <td width="12%" style="background-color: #f2f2f2;">首费(元)</td>
                                                    <td width="12%" style="background-color: #f2f2f2;" id="expressTransAddWeightType">续件(件)</td>
                                                    <td width="12%" style="background-color: #f2f2f2;">续费(元)</td>
                                                    <td width="7%" style="background-color: #f2f2f2;">操作</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:forEach items="${transport.expressList}" var="transfee" varStatus="status" begin="1">
                                                    <tr index="${status.index}">
                                                        <input name="expressList[${status.index}].id" value="${transfee.id}" type="hidden"/>
                                                        <td>
                          					<span class="width2">
	                          					<i><input id="trans_ck_${status.index}" name="trans_ck_${status.index}" type="checkbox" value="" style="display:none;"/></i>
	                          					<input id="express_city_ids${status.index}" name="expressList[${status.index}].cityIdList" type="hidden" value='${transfee.cityIds}'/>
	                          					<input id="select_id1" value="0" type="hidden" />
	                          					<a id="edit_trans_city1" href="javascript:void(0);" onclick="edit_trans_city(this);" trans_city_type="express">编辑</a>
                          					</span>
                                                            <span class="width1" id="express${status.index}">
                          						<c:forEach var="city" items="${transfee.cityNameList}" varStatus="cityStatus">${city}、</c:forEach>
                          					</span>
                                                        </td>
                                                        <td>
                                                            <input type="text" value='${transfee.transWeight}' class="in" id="express_trans_weight${status.index}"
                                                                   name="expressList[${status.index}].transWeight"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" value='${transfee.transFee}' class="in" id="express_trans_fee${status.index}"
                                                                   name="expressList[${status.index}].transFee"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" value='${transfee.transAddWeight}' class="in" id="express_trans_add_weight${status.index}"
                                                                   name="expressList[${status.index}].transAddWeight"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" value='${transfee.transAddFee}' class="in" id="express_trans_add_fee${status.index}"
                                                                   name="expressList[${status.index}].transAddFee"/>
                                                        </td>
                                                        <td>
                                                            <span class="width3"><a href="javascript:void(0);" onclick="if(confirm('确认要删除当前地区的设置么？'))remove_trans_city(this)">删除</a></span>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="rdinary_ul_bottom" style="display:none;" id="express_trans_city_op">
                                            <label>
                                                <input name="express_trans_all" id="express_trans_all" type="checkbox" value=""/>
                                                全选 </label>
                                            &nbsp; <a href="javascript:void(0);" id="batch_config_express" style="display:none;">批量设置</a> &nbsp; <a href="javascript:void(0);" id="batch_del_express">批量删除</a>
                                        </div>
                                        <div class="rdinary_ul_bottom">
                                            <a href="javascript:void(0);" onclick="trans_city('express')">为指定地区城市设置运费</a>&nbsp;
                                            <a href="javascript:void(0);" id="batch_set_express" trans_type="express">批量操作</a>&nbsp;
                                            <a href="javascript:void(0);" id="batch_cancle_express" trans_type="express" style="display:none;">取消批量</a>
                                        </div>
                                    </div>
                                </div>
                                <!--快递 结束-->

                                <!--EMS 开始-->
                                    <%-- <div class="db_box_main">
                               <div class="db_box_main_input">
                                 <label>
                                   <input name="transEms" type="checkbox" id="transEms"   class="freightMode" trans="trans" <c:if test="${transport.transEms}">checked="checked"</c:if> /> EMS
                                  </label>
                               </div>

                                <c:choose>
                                   <c:when test="${transport.transEms}">
                                       <div class="db_box_main_rdinary" id="transEms_info" style="display: block;">
                                   </c:when>
                                   <c:otherwise>
                                       <div class="db_box_main_rdinary" id="transEms_info" style="display: none;">
                                   </c:otherwise>
                               </c:choose>
                              <div class="db_box_main_rdinary" id="transEms_info"  style="display: none;">
                                 <div class="rdinary_top">默认运费：
                                         <input name="emsList[0].id" value="${transport.emsList[0].id}" type="hidden" />
                                       <input name="emsList[0].transWeight" style="width: 43px;height: 17px;" type="text" id="emsTransWeight" value="${transport.emsList[0].transWeight}" size="5" /><span>件内，</span>
                                       <input name="emsList[0].transFee" style="width: 60px;height: 17px;" type="text" id="emsTransFee" value="${transport.emsList[0].transFee}" size="8" /><span>元， 每增加</span>
                                       <input name="emsList[0].transAddWeight"  style="width: 43px;height: 17px;" type="text" id="emsTransAddWeight" value="${transport.emsList[0].transAddWeight}" size="5" /> <span>件，增加运费</span>
                                       <input name="emsList[0].transAddFee"  style="width: 60px;height: 17px;" type="text" id="emsTransAddFee" value="${transport.emsList[0].transAddFee}" size="8" /><span>元</span>
                                 </div>
                               <div id="ems_trans_city_info">
                                   <table id="emsTable" style="width:100%;border-collapse: collapse;margin-top: 10px;border: 1px solid #ddd;" class="col-table-css" >
                                   <thead>
                                     <tr>
                                           <td width="40%" align="center" style="background-color: #f2f2f2;">运送到</td>
                                           <td width="12%" style="background-color: #f2f2f2;" id="emsTransWeightType">首件(件)</td>
                                           <td width="12%" style="background-color: #f2f2f2;">首费(元)</td>
                                           <td width="12%" style="background-color: #f2f2f2;" id="emsTransAddWeightType">续件(件)</td>
                                           <td width="12%" style="background-color: #f2f2f2;">续费(元)</td>
                                           <td width="7%" style="background-color: #f2f2f2;">操作</td>
                                     </tr>
                                     </thead>
                                     <tbody>
                                             <c:forEach  items="${transport.emsList}" var="transfee" varStatus="status" begin="1">
                                                 <tr index="${status.index}">
                                                     <input name="emsList[${status.index}].id" value="${transfee.id}" type="hidden" />
                                                     <td>
                                                         <span class="width2">
                                                             <i><input id="trans_ck_${status.index}" name="trans_ck_${status.index}" type="checkbox" value="" style="display:none;" /></i>
                                                             <input id="ems_city_ids${status.index}" name="emsList[${status.index}].cityIdList" type="hidden" value='${transfee.cityIds}' />
                                                             <a  href="javascript:void(0);" onclick="edit_trans_city(this);" trans_city_type="ems">编辑</a>
                                                             </span>
                                                             <span class="width1" id="ems${status.index}">
                                                                 <c:forEach var="city" items="${transfee.cityNameList}" varStatus="cityStatus">${city}、</c:forEach>
                                                             </span>
                                                     </td>
                                                     <td>
                                                             <input type="text" value='${transfee.transWeight}' class="in" id="ems_trans_weight${status.index}" name="emsList[${status.index}].transWeight" />
                                                     </td>
                                                     <td>
                                                             <input type="text" value='${transfee.transFee}' class="in" id="ems_trans_fee${status.index}" name="emsList[${status.index}].transFee" />
                                                     </td>
                                                     <td>
                                                             <input type="text" value='${transfee.transAddWeight}' class="in" id="ems_trans_add_weight${status.index}" name="emsList[${status.index}].transAddWeight" />
                                                     </td>
                                                     <td>
                                                             <input type="text" value='${transfee.transAddFee}' class="in" id="ems_trans_add_fee${status.index}" name="emsList[${status.index}].transAddFee" />
                                                     </td>
                                                     <td>
                                                         <span class="width3"><a href="javascript:void(0);" onclick="if(confirm('确认要删除当前地区的设置么？'))remove_trans_city(this)">删除</a></span>
                                                     </td>
                                                 </tr>
                                             </c:forEach>
                                     </tbody>
                                   </table>
                                 </div>
                                 <div class="rdinary_ul_bottom" style="display:none;" id="ems_trans_city_op">
                                   <label>
                                     <input name="ems_trans_all" id="ems_trans_all" type="checkbox" value="" />
                                     全选 </label>
                                   &nbsp; <a href="javascript:void(0);" id="batch_config_ems" style="display:none;">批量设置</a> &nbsp; <a href="javascript:void(0);" id="batch_del_ems">批量删除</a>
                                 </div>
                                     <div class="rdinary_ul_bottom">
                                         <a href="javascript:void(0);" onclick="trans_city('ems')">为指定地区城市设置运费</a>&nbsp;
                                         <a  href="javascript:void(0);" id="batch_set_ems" trans_type="ems">批量操作</a>&nbsp;
                                         <a href="javascript:void(0);" id="batch_cancle_ems" trans_type="ems" style="display:none;">取消批量</a>
                                     </div>
                                   </div> --%>
                            </div>
                            <!--EMS 结束-->
            </div>
            </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <a class="btn-r big-btn" href="javascript:void(0);" onclick="submitTable();"><s></s>提交</a>&nbsp;
                    <a class="btn-g big-btn" href="javascript:void(0);" onclick="loadTransportList();"><s></s>返回</a>
                </td>
            </tr>
            </table>
        </div>
        </form:form>
    </div>


</div>
<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
</div>
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css"/>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>"/></script>
<script type = "text/javascript" src = "<ls:templateResource item='/resources/templets/js/transport.js'/>" ></script>
<script type = "text/javascript" src = "<ls:templateResource item='/resources/templets/js/transportOverlay.js'/>" ></script>
<script type="text/javascript">
    userCenter.changeSubTab("transport");
    var paramData = {
        contextPath: "${contextPath}"
    }
    var checkType = null;
    $(function () {
        rsChecked();
        checkType = $('input[name="transType"]:checked').val();
        $("#transExpress").click(function () {
            var is_checked = $("#transExpress").attr("checked");
            if (!is_checked) {
                $("#transExpress").removeAttr("checked");
                $("#transExpress").parent().parent().attr("class", "checkbox-wrapper");
                $("#transExpress_info").hide();
            } else {
                $("#transExpress").attr("checked", "checked");
                $("#transExpress").parent().parent().attr("class", "checkbox-wrapper checkbox-wrapper-checked");
                $("#transExpress_info").show();
            }
        });
        $("[name=isRegionalSales]").change(function () {
            rsChecked();
        });


    })

    function radioChecked(ele) {
        $("#" + ele.id).parent(".radio-item").parent(".radio-wrapper").attr("class", "radio-wrapper radio-wrapper-checked");
        $("#" + ele.id).attr("checked", "checked");
        var items = $("#" + ele.id).parent().parent().parent().find("input[type='radio']");
        items.each(function () {
            if (this.id != ele.id) {
                $("#" + this.id).removeAttr("checked");
                $("#" + this.id).parent(".radio-item").parent(".radio-wrapper").attr("class", "radio-wrapper");
            }
        });
    }

    function rsChecked() {
        $("[name=isRegionalSales]:checked").each(function () {
            if (this.value == 0) {
                $(".rdinary_top").show();
            }
            if (this.value == 1) {
                $(".rdinary_top").hide();
            }
        })
    }


</script>
</body>
</html>