<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>

<!-- 店铺信息 -->
<div class="shop-mes">

	<c:if test="${empty shopDetail}">
		<div class="no-record">
			<i class="rec-i"></i>
			<p class="rec-p">暂无数据</p>
		</div>
	</c:if>
	
	<c:if test="${not empty shopDetail}">
		<p class="item-p">店铺名称：<a href="${contextPath }/shop/index/${shopDetail.shopId }" class="s-name" target="_blank">${shopDetail.siteName }</a></p>
		<p class="item-p clear"><span style="float: left">综合评分：</span>
			<b class="scores_scroll"> 
				<span class="shop_scroll_gray"> </span>
				<span class="scroll_red"></span> 
			</b>
			<span class="s-sco">${shopDetail.credit}分</span>
		</p>
		<p class="item-p">联系电话：
			<span class="s-num">
				<c:if test="${empty shopDetail.contactTel }">暂无信息</c:if>
				<c:if test="${not empty shopDetail.contactTel }">${shopDetail.contactTel }</c:if>
			</span>
		</p>
		<div class="shop-con">
			<table class="s-con-table" cellpadding="0" cellspacing="0">
				<tr>
					<td width="70">商品数量</td>
					<td>${shopDetail.productNum}件</td>
				</tr>
				<tr>
					<td width="70">店铺地址</td>
					<td>
						<span style="width: 250px;display: block;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
						${shopDetail.shopAddr}</span>
					</td>
				</tr>
				<tr>
					<td width="70">店铺描述</td>
					<td>
						<span style="width: 250px;line-height: 20px;font-size: 14px;max-height: 100px; overflow : hidden;word-break: break-all;text-overflow: ellipsis; display: -webkit-box;-webkit-line-clamp: 5;-webkit-box-orient: vertical;">
						${shopDetail.briefDesc }</span>
					</td>
				</tr>
			</table>
		</div>
	</c:if>
	
	
	
</div>
<!-- /店铺信息 -->


<script type="text/javascript">
//计算评分
var soc="${shopDetail.credit}";
var element=$(".scroll_red");
progress(soc,element);
function progress(score,element){
	var w = (score/5)*100;
	element.css("width",w+"%");
}
</script>