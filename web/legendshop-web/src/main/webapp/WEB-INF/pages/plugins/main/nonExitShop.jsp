<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%@ page contentType="text/html;charset=UTF-8" language="java"%>
    <%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
	<title>${systemConfig.shopName}
	</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="home/top.jsp"%>
		<div id="bd">
<div class="w" > 
    <div class="login_left wrap">
      <div>
         <div>
         <div>
				<div style="width:500px;margin:auto;text-align: left;margin-bottom:100px;">
				    <div style="margin-top: 50px;"><center><img src="${contextPath}/resources/templets/images/not_prod.png"/></center></div>
	                <div style="margin-top:50px;">
	                   <center> <b style="font-size:16px;color:#404040;">很抱歉，该店铺已经下线或关闭，请联系平台客服审核店铺。</b></center>
	                </div>
	                <div style="margin-top:20px;margin-left:50px;">
	                	<ol style="margin-top:10px;">
						 <li>1. 该店铺可能已经下线或关闭，您可以联系平台客服审核。</li>
						 <li>2. 该店铺的商品不能购买和搜索。</li>
						</ol>
	                </div>
				</div>
         <div class="clear"></div>
     </div>   
        </div>      
      </div>                                  
    </div>
    <!----左边end---->
    
   <div class="clear"></div>
</div>
</div>
		<%@ include file="home/bottom.jsp"%>
	</div>
</body>
</html>