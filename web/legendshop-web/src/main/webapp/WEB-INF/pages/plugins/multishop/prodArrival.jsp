<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	<title>到货通知-${systemConfig.shopName}</title>
    <meta name="keywords" content="${systemConfig.keywords}"/>
    <meta name="description" content="${systemConfig.description}" />
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-prod${_style_}.css'/>" rel="stylesheet">
</head>
<body class="graybody">
	<div id="doc">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp" %>
		<div class="w1190">
			<div class="seller-cru">
				<p>
					您的位置>
					<a href="${contextPath}/home">首页</a>>
					<a href="${contextPath}/sellerHome">卖家中心</a>>
					<span class="on">到货通知</span>
				</p>
			</div>
			<%@ include file="included/sellerLeft.jsp" %>
			<!-- 搜索 -->
			<div class="seller-selling">
				<div class="pagetab2">
					<ul id="listType">
						<li class="on"><span>到货通知</span></li>
					</ul>
	   			</div>
				<form:form action="${contextPath}/s/prod/arrival" method="get" id="arrivalForm">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
						<div class="sold-ser sold-ser-no-bg clearfix">
							<div class="fr">
								<div class="item">
									商品名称：
									<input type="text" class="item-inp" style="width:300px;" value="${searchProName}" name="productName"  id ="productName">
									<input type="button" onclick="search()" class="btn-r" id="btn_keyword" value="搜  索" >
								</div>
							</div>
						</div>
				</form:form>

				<table class="selling-table sold-table">
					<tr class="selling-tit">
						<th>商品</th>
						<th>商品名称</th>
						<th>属性</th>
						<th>价格</th>
						<th>操作</th>
					</tr>
					<c:choose>
			           <c:when test="${empty requestScope.list}">
			              <tr>
						 	 <td colspan="20" class="last-td">
						 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
						 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
						 	 </td>
						 </tr>
			           </c:when>
			           <c:otherwise>
			               <c:forEach items="${requestScope.list}" var="product" varStatus="status">
							<tr class="detail">
								<input type="hidden" name="prodId" value="${product.prodId}"/>
								<input type="hidden" name="skuId" value="${product.skuId}"/>
								<td>
								<c:if test="${not empty product.picture}">
									<img src="<ls:photo item='${product.picture}'/>" style="max-width: 80px;max-height: 80px;">
								</c:if>
								</td>
								<td>${product.productName}</td>
								<td>${product.cnProperties}</td>
								<td>¥${product.price}</td>
								<td class="last-td">
								<div class="am-btn-group am-btn-group-xs">
			                  		<button class="updateButton bat-oper-btn btn-r" onclick="selectArr('${product.skuId}','${product.productName}','${product.cnProperties}')">
			                  		<span>查看</span></button>
			             		</div>
								</td>
							</tr>
						</c:forEach>
			           </c:otherwise>
		           </c:choose>
				</table>
				<div class="selling-cha">
				</div>
				<div class="clear"></div>
					<div style="margin-top:10px;" class="page clearfix">
		     			 <div class="p-wrap">
		            		<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
		     			 </div>
	  				</div>
			</div>
		</div>
		<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
	<script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/js/multishop/prodArrival.js'/>"></script>
	<script type="text/javascript">
	var contextPath="${contextPath}";
	function pager(curPageNO){
        document.getElementById("curPageNO").value=curPageNO;
        document.getElementById("arrivalForm").submit();
    }
    
    function selectArr(skuId,productName,cnProperties){
    	layer.open({
  		  title :productName+" - "+ cnProperties,
  		  id: "selectArr",
  		  type: 2, 
  		  resize: false,
  		  content: ["${contextPath}/s/prod/selectArrival?skuId="+skuId,'no'],//这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
  		  area: ['730px', '600px']
  		}); 
    }
    
    function search(){
	  	$("#arrivalForm #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	  	$("#arrivalForm")[0].submit();
	}
	</script>
</body>
</html>