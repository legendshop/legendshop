<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/dialog.jsp"%>
<html lang="zh-CN">
<head>
<meta charset="UTF-8">
<title>首页</title>
<link rel="stylesheet" href="${contextPath}/resources/templets/css/server/common.css">
<link rel="stylesheet" href="${contextPath}/resources/templets/css/server/index.css">
</head>
<body class="gray-body">
	<%@ include file="serverHeader.jsp"%>
	<div class="index clear">
		<div class="index-left">
			<div class="index-nav">
				<a href="conversation" class="nav-item">当前会话</a>
				<a href="${contextPath}/customerService/setting" class="nav-item cur">其他信息</a>
			</div>
		</div>
		<div class="index-right">
			<!-- 其他信息 -->
			<div class="other-msg">
				<div id="nav" class="msg-nav">
					<a href="javascript:void(0)" class="item cur">个人资料</a>
					<a href="javascript:void(0)" class="item">商家信息</a>
					<a href="javascript:void(0)" class="item">修改密码</a>
				</div>
				<div>
					<!-- 个人资料 -->
					<div class="msg-con">
						<form id="personal">
							<div class="item">
								<span class="item-tit">姓名：</span>
								<div class="item-con">
									<input id="name" name="name" type="text" value="${serverAccount.name}" maxlength="10" readonly>
								</div>
							</div>
							<div class="item">
								<span class="item-tit">账号：</span>
								<div class="item-con">
									<input type="text"  maxlength="20" value="${serverAccount.account}" disabled>
								</div>
							</div>
							<div class="item">
								<span class="item-tit">描述备注：</span>
								<div class="item-con">
									<textarea id="remark" maxlength="50" name="remark" readonly>${serverAccount.remark}</textarea>
								</div>
							</div>
							<div class="item">
								<div class="item-con">
									<a id="edit" href="#" class="item-btn">编辑</a>
									<input type="submit" value="保存" class="item-btn" style="display: none;">
									<a href="" class="item-btn gray-btn" style="display: none;">取消</a>
								</div>
							</div>
						</form>
					</div>
					<!-- /个人资料 -->
					<!-- 商家信息 -->
					<div class="msg-con" style="display: none;">
						<div class="item">
							<span class="item-tit">店铺名：</span>
							<div class="item-con">
								<input value="${serverShopAccount.siteName}" readonly>
							</div>
						</div>
						<div class="item">
							<span class="item-tit">联系电话：</span>
							<div class="item-con">
								<input value="${serverShopAccount.contactMobile}" readonly>
							</div>
						</div>
						<div class="item">
							<span class="item-tit">联系地址</span>
							<div class="item-con">
								<input value="${serverShopAccount.shopAddr}" readonly>
							</div>
						</div>
					</div>
					<!-- /商家信息 -->
					<!-- 修改密码 -->
					<div class="msg-con" style="display: none;">
						<form id="password">
							<div class="item">
								<span class="item-tit">原密码：</span>
								<div class="item-con">
									<input name="old" type="password" maxlength="20">
								</div>
							</div>
							<div class="item">
								<span class="item-tit">新密码：</span>
								<div class="item-con">
									<input id="new" name="password" type="password" maxlength="20">
								</div>
							</div>
							<div class="item">
								<span class="item-tit">确认密码：</span>
								<div class="item-con">
									<input name="confirm" type="password" maxlength="20">
								</div>
							</div>
							<div class="item">
								<span class="item-tit">&nbsp;</span>
								<div class="item-con">
									<input type="submit" value="保存" class="item-btn">
									<a href="" class="item-btn gray-btn">取消</a>
								</div>
							</div>
						</form>
					</div>
					<!-- /修改密码 -->
				</div>
			</div>
			<!-- /其他信息 -->
		</div>
	</div>
	<%-- <%@ include file="serverFooter.jsp"%> --%>
	<script type="text/javascript">
	  var contextPath="${contextPath}";
	  var sign = "${sign}";
	  var customId = "${customId}";//当前登录的客服ID
	  var shopId = "${shopId}";//当前登录的商家ID
	  var name = "${name}";//客服名称
	  var IM_BIND_ADDRESS = '${IM_BIND_ADDRESS}';
	  var websocket = null;//定义的websocket对象
		//初始化对象
	  var chat = {};
	  
	</script>
	<script type="text/javascript" src="${contextPath}/resources/common/js/jquery.js"></script>
	<script type="text/javascript" src="${contextPath}/resources/common/js/jquery.validate.js"></script>
	<script type="text/javascript" src="${contextPath}/resources/templets/js/load-transition.js"></script>
	<script type="text/javascript" src="${contextPath}/resources/templets/js/server/conversation_head.js"></script>
	<script type="text/javascript" src="${contextPath}/resources/templets/js/server/setting.js"></script>
</body>
</html>