<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>消费记录 - ${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/usercenter.css'/>" rel="stylesheet" />
</head>
<style>
input[type='checkbox']{
	margin-left:5px;
}
.sold-table th{
	border-bottom:1px solid #e4e4e4;
}
</style>
<body class="graybody">
	<div id="bd">
		<%@ include file="/WEB-INF/pages/plugins/main/home/top.jsp"%>
            <!----两栏---->
 <div class=" yt-wrap" style="padding-top:10px;">     
    <%@include file='usercenterLeft.jsp'%>
    
    <!-----------right_con-------------->
     <div class="right_con"  id="content">
	<div class="o-mt">
		<h2>消费记录</h2>
	</div>
	<div class="pagetab2">
           <ul>           
              <li class="on" id="accusationList"><span>消费记录列表</span></li>
           </ul>       
     </div>
     <div id="recommend" class="recommend" style="display: block;">
     <table id="item"  style="width:100%" cellspacing="0" cellpadding="0" class="buytable mailtab sold-table">
		<tbody>
		<tr>
			<th width="30">
			<!-- <input type="checkbox" id="checkbox" onclick="selAll();"/> -->
			<span>
		           <label class="checkbox-wrapper" style="float:left;margin-left:9px;">
						<span class="checkbox-item">
							<input type="checkbox" id="checkbox" class="checkbox-input selectAll"/>
							<span class="checkbox-inner" style="margin-left:9px;"></span>
						</span>
				   </label>	
			</span>
			</th>
			<th >记录时间</th>
			<th >记录金额</th>
			<th >备注</th>
			<th width="100" style="border-right: 1px solid #e4e4e4;">操作</th>
		</tr>
			<c:choose>
	           <c:when test="${empty requestScope.list}">
	              <tr>
				 	 <td colspan="20">
				 	 	 <!--这里注释掉i标签，i标签是带图片显示的空数据提醒  -->
				 	 	 <div class="warning-option"><!-- <i></i> --><span style="text-align: center; ">没有符合条件的信息</span></div>
				 	 </td>
				 </tr>
	           </c:when>
	           <c:otherwise>
             	  <c:forEach items="${requestScope.list}" var="expenses" varStatus="status">
		               <tr>
		               	   <td>
		               	   <%-- <input name="strArray" type="checkbox" value="${expenses.id}"/> --%>
		               	   <span>
					           <label class="checkbox-wrapper" style="float:left;margin:9px;">
									<span class="checkbox-item">
										<input type="checkbox" id="strArray" class="checkbox-input selectOne" value="${expenses.id}" onclick="selectOne(this);"/>
										<span class="checkbox-inner" style="margin:9px;"></span>
									</span>
							   </label>	
							</span>
		               	   </td>
		               	   <td><fmt:formatDate value="${expenses.recordDate}" type="both" /></td>
		               	   <td>${expenses.recordMoney}</td>
		               	   <td>${expenses.recordRemark}</td>
		                   <td> 
				        		<a class="btn-r small-btn" href='javascript:deleteById("${expenses.id}")' title="删除">删除</a>
		                   </td>
		               </tr>
		           </c:forEach> 
	           </c:otherwise>
	           </c:choose>
		</tbody>
		</table>
		</div>
		 <c:if test="${not empty requestScope.list }">
		    <div class="fl search-01">
                <input class="bti btn-r" type="button" value="批量删除"  id="delBtn" onclick="deleteAction();" style="cursor: pointer;"/>
                 <input class="bti btn-g" type="button" value="清空"  id="clrBtn" onclick="clearAction();"  style="cursor: pointer;"/>
              </div> 
	          <div style="margin-top:10px;" class="page clearfix">
	     			 <div class="p-wrap">
						<span class="p-num">
							<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
						</span>
	     			 </div>
 			  </div>
		  </c:if>
     </div>
 <div class="clear"></div>
 </div>
<!----两栏end---->
		
	<%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
	</div>
	<!--bd end-->
	<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/alternative.js'/>"></script>
	<script type="text/javascript"  src="<ls:templateResource item='/resources/templets/js/expensesRecordList.js'/>"></script>
	<script type="text/javascript">
		var contextPath = "${contextPath}";
     </script>
</body>
</html> 