<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
	<table>
		<thead>
			<tr><td width="150">图片</td><td width="450">名称</td><td width="110">操作</td></tr>
		</thead>
		<tbody>
		<c:if test="${empty requestScope.list}">
			<tr><td colspan="3" style="text-align: center;">没有找到符合条件的商品</td></tr>
		</c:if>
     	<c:forEach items="${requestScope.list}" var="product">
          <tr>
          	<td><img src="<ls:images item='${product.pic}' scale='3' />" ></td>
          	<td>${product.prodName}</td>
          	<td><button style="border:0px;"><a style="text-decoration: none;color:#333;padding: 5px;background: #999;color: #fff;" target="_blank" href="${contextPath}/views/${product.prodId}">查看</a></button>
          	&nbsp;<button style="padding: 5px;background: #e5004f;color: #fff;border: 0px;cursor: pointer;" onclick="addProd('${product.prodId}','${product.prodName}','${product.pic}')">选择</button></td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
            
      <div style="margin-top:10px;" class="page clearfix">
  			 <div class="p-wrap">
				<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
  			 </div>
		</div>
		
		