<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<c:set var="prodParams"  value="${prodApi:getProdParameter(prodId)}"></c:set>
<c:choose>
	<c:when test="${not empty prodParams}">
		<table cellpadding="0" cellspacing="0">
		  <tbody>
		  	<c:forEach items="${prodParams}" var="paramGroup">
		  		<tr><th class="guige" colspan="2" style="background: #f9f9f9;">${paramGroup.groupName}</th></tr>
			  	<c:forEach items="${paramGroup.params}" var="p">
				  <tr><th style="width:150px;background: #f9f9f9;padding-right: 10px;">${p.paramName}</th><td style="text-indent: 10px;">${p.paramValueName}</td></tr>
				 </c:forEach>
			</c:forEach>
		</tbody></table>
	</c:when>
	<c:otherwise>
		<div>此商品没有参数</div>
	</c:otherwise>
</c:choose>