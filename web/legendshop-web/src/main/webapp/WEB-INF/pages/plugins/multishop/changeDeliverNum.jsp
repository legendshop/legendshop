<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/common.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
<title>修改物流信息</title>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/change-deliver-num.css'/>" rel="stylesheet"/>
</head>
<body>
<div class="m" id="edit-cont">
	<div class="mc">
		<div class="form">
			<div class="item">
				<span class="label"><em>*</em>订单号：</span>
				    <div class="fl">
				  	<input readonly="readonly" type="text" name="subNumber" id="subNumber" size="30" value="${sub.subNumber}"  style="height: 20px">
				   </div>
				</span>
              </div>
			<div class="item">
				<span class="label"><em>*</em>物流公司：</span>
				<select id="ecc_id" name="ecc_id"  style="height: 22px;">
					<c:forEach items="${deliveryTypes}" var="dt">
							 <option value="${dt.dvyTypeId}" <c:if test="${dt.dvyTypeId==sub.dvyTypeId}" > selected="selected" </c:if> >${dt.name}</option>
					 </c:forEach>
				</select>
			</div>
			<div class="item">
				<span class="label"><em>*</em>物流单号：</span>
				<div class="fl">
				  <input type="text" name="dvyFlowId"  value="${sub.dvyFlowId}"  id="dvyFlowId" size="30"  style="height: 20px" maxlength="20">
				</div>
			</div>
			
			<div class="btns">
			    <a href="javascript:void(0);" onclick="changeDeliverNum();"  class="e-btn gray-btn save-btn" style="background: #e5004f; color: #fff;">保存</a> 
			    </br><font color="#e5004f">注意：发货一个小时内可以修改物流信息</font>
			</div>
		</div>
	</div>
</div>
</body>
	<script type="text/javascript">	
	
	function changeDeliverNum(){
	     var deliv=$("#ecc_id").find("option:selected").val();
	     
	     if(deliv==undefined || deliv==null || deliv==""){
	        layer.msg('请去设置配送方式', {icon: 0});
	        return ;
	     }
	     
	     var dvyFlowId=$.trim($("#dvyFlowId").val());
	     if(dvyFlowId==undefined || dvyFlowId==null || dvyFlowId==""){
	        layer.msg('请输入物流单号', {icon: 0});
	        return ;
	     }
	     var state_info="";
	     var subNumber=$.trim($("#subNumber").val());
	     if(subNumber==undefined || subNumber==null || subNumber==""){
	        return;
	     }
	   layer.confirm("请确认发货单是否正确？", {
      		 icon: 3
      	     ,btn: ['确定','取消'] //按钮
      	   },function () {
		    $.post("${contextPath}/s/order/changeDeliverNum" , 
				 {"subNumber": subNumber, "deliv": deliv,"dvyFlowId":dvyFlowId,"info":state_info},
			        function(retData) {
				       if(retData == 'OK' ){
				    	   layer.closeAll('iframe');
				    	   window.parent.location.reload();
				       }else if(retData == 'fail'){
				           layer.msg('修改物流单号失败，订单号 ' + subNumber, {icon: 2});
				       }else{
				          layer.msg(retData, {icon: 0});
				       }
			 },'json');
		 });
	}
	
   
	</script>
	
</html>