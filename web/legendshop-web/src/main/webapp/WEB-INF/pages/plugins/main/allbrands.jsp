<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<html>
<head>
<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
<title>全部品牌-${systemConfig.shopName}</title>
<meta name="keywords" content="${systemConfig.keywords}"/>
<meta name="description" content="${systemConfig.description}" />
</head>
<body>
<div id="bd">
<%@include file='home/top.jsp'%>
       <div style="padding:30px 0 20px;" class="yt-wrap">
            <div class="pagetab2">
               <ul>           
                 <li><span><a href="<ls:url address='/allCategorys'/>">全部商品分类</a></span></li>                  
                 <li class="on"><span>全部品牌</span></li>   
               </ul>       
            </div>
            
            
            <div class="brandbox">
                    <div class="mt">
                        <h2>推荐品牌</h2>
                    </div>
                    <div class="brandslist clearfix">
                                <ul class="list-h">
                                    <c:forEach items="${requestScope.allbrands}" var="brand">
										<li>
											<div>
											<c:choose>
												<c:when test="${empty brand.brandPic}">
													<span class="b-img">
														<a href="<ls:url address="/list?prop=brandId:${brand.brandId}"/>" ><img width="138" height="46" src="${contextPath}/resources/templets/images/brandicon.png" /></a>
													</span>
												</c:when>
												<c:otherwise>
													<span class="b-img">
														<a href="<ls:url address="/list?prop=brandId:${brand.brandId}"/>" ><img width="138" height="46" src="<ls:photo item='${brand.brandPic}'/>" /></a>
													</span>
												</c:otherwise>
											</c:choose>
												<span class="b-name">
													<a href="<ls:url address="/list?prop=brandId:${brand.brandId}"/>" >${brand.brandName}</a>
												</span>
											</div>
										</li>
										</c:forEach>
                                    </ul></div>
                    <div class="clear"></div>
                    <div class="mt">
                        <h2>全部品牌</h2>
                    </div>
                <div class="brandslist clearfix" name="BrandList" id="BrandList">
                                <ul class="list-h">
                  </ul></div>
              </div> 
       <script type="text/javascript" src="<ls:templateResource item='/resources/templets/js/allbrands.js'/>"></script>
       </div>
   <%@include file='home/bottom.jsp'%>
   </div>
   </body>
   </html>
   <script type=text/javascript>
	var contextPath="${contextPath}";
</script>