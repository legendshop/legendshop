<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
	<title>门店管理-${systemConfig.shopName}</title>
	<meta name="keywords" content="${systemConfig.keywords}" />
	<meta name="description" content="${systemConfig.description}" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/css/store/store.css'/>" rel="stylesheet"/>
</head>
<body>
 
    <header class="ncsc-head-layout" style="display: block;">
        <div class="wrapper">
          <div class="ncsc-admin">
            <dl>
              <dt>${store.name}</dt>
              <dd>${store.province}&nbsp;${store.city}&nbsp;${store.area}&nbsp;${store.address}</dd>
              <dd>${store.contactTel}</dd>
            </dl>
<!--              <div class="pic">
		      		<img src=""   />
            </div> -->
          </div>
          <div class="center-logo">
             <a href="<ls:url address='/'/>" target="_blank">
                <img src="${contextPath }/resources/templets/images/store/logo-store.png"  style="max-height:89px;" />
            </a>
            <h1>门店管理</h1>
          </div>
          <ul class="ncsc-nav">
            <li> <a href="<ls:url address='/m/store/products'/>">门店商品库存</a> </li>
            <li> <a href="#" class="current">门店取货订单</a> </li>
            <li> <a href="javascript:void(0);" onclick="loginOut();">安全退出</a> </li>
          </ul>
        </div>
    </header>
    <div class="ncsc-layout wrapper">
        <div class="main-content" id="mainContent">
            <div class="alert alert-block mt10">
              <ul class="mt5">
                <li>该列表可以查看待自提和已经自提的订单，对于到门店付款的自提订单，请确保收到款后再进行自提出货操作。</li>
              </ul>
            </div>
            <form:form id="from1"  method="post" action="${contextPath}/m/store/order">
              <table class="search-form">
                <tbody>
                  <tr>
                    <td>&nbsp;</td>
                    <th>订单状态</th>
                    <td class="w100">
                      <select name="orderStatus">
                      <option value="">请选择</option>
                        <option <c:if test="${paramdto.orderStatus eq 1}">selected="selected"</c:if>  value="1">未自提</option>
                        <option <c:if test="${paramdto.orderStatus eq 2}">selected="selected"</c:if> value="2">已自提</option>
                      </select>
                    </td>
                    <th>
                      <select name="paramName">
                       <option value="">请选择</option>
                        <option <c:if test="${paramdto.paramName eq 'order_sn'}">selected="selected"</c:if> value="order_sn">订单号</option>
                        <option <c:if test="${paramdto.paramName eq 'chain_code'}">selected="selected"</c:if> value="chain_code">提货码</option>
                        <option <c:if test="${paramdto.paramName eq 'buyer_phone'}">selected="selected"</c:if> value="buyer_phone">手机号</option>
                         <option <c:if test="${paramdto.paramName eq 'reciver_name'}">selected="selected"</c:if> value="reciver_name">提货人</option>
                      </select>
                    </th>
                    <td class="w160"><input class="text w150" type="text" value="${paramdto.paramValue}" name="paramValue">
                    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/></td>
                    <td class="tc w70">
                        <label class="submit-border">
                          <input class="submit" value="搜索" type="submit">
                        </label>
                    </td>
                </tr>
              </tbody>
            </table>
          </form:form>
          <table class="ncsc-default-table">
            <thead>
              <tr nc_type="table_header">
              <th class="w20"></th>
                <th colspan="2">商品</th>
                <th class="w150">价格（元）</th>
                <th class="w60">数量</th>
                <th class="w150">订单金额（元）</th>
                <th class="w180">提货人</th>
                <th class="w150">订单状态</th>
                <th class="w120" style="border-right:solid 1px #e4e4e4;">操作</th>
              </tr>
            </thead>
            <c:forEach items="${requestScope.list}" var="storeSub" varStatus="status">
              <c:set var="storeLength" value="${fn:length(storeSub.storeOrderItemDtoList)}"/>
            <tbody>
              <tr>
                <th colspan="20" class="tl" style="background: #fff;"><span class="ml10">订单编号：${storeSub.orderSn}</span><span class="ml20">下单时间：<fmt:formatDate value="${storeSub.dlyoAddtime}"  pattern="yyyy-MM-dd HH:mm:ss"/></span></th>
              </tr>
              <c:forEach items="${storeSub.storeOrderItemDtoList}" var="item" varStatus="status">
              <tr>
              	<td style="width: 10px;"></td>
	                <td class="w70">
	                    <div class="goods-thumb">
	                      <a href="${contextPath}/views/${item.prodId}" target="_blank"><img src="<ls:photo item='${item.pic}'/>"></a>
	                    </div>
	                </td>
	                <td>
	                    <dl class="goods-info">
	                      <dt class="goods-name"><a href="${contextPath}/views/${item.prodId}" target="_blank">${item.prodName}</a><br>${item.attribute}</dt>
	                      <dd class="goods-spec"></dd>
	                      <dd class="goods-type"></dd>
	                    </dl>
	                </td>
	                <td><em class="goods-price">${item.cash}</em></td>
	                <td>${item.basketCount}</td>
	                <c:if test="${status.index==0}">
	                	 <td rowspan="${storeLength}" style="border-left: 1px solid #e6e6e6"><em class="order-amount">${storeSub.actualTotal}</em></td>
	                	 <td width="150px;" rowspan="${storeLength}" style="border-left: 1px solid #e6e6e6"><p>提货人：${storeSub.reciverName}</p><p style="white-space: nowrap;">电话：${storeSub.reciverMobile}</p></td>
	                	  <td rowspan="${storeLength}" style="border-left: 1px solid #e6e6e6">
						  	<c:choose>
						  		<c:when test="${storeSub.refundType eq 1}">退款中</c:when>
						  		<c:when test="${storeSub.refundType eq 2}">退货/退款中</c:when>
						  		<c:when test="${storeSub.dlyoSatus eq 1}">已付款待提货订单</c:when>
						  		<c:when test="${storeSub.dlyoSatus eq 2}">已自提</c:when>
						  	</c:choose>
						  </td>
	                	  <td rowspan="${storeLength}" style="border-left: 1px solid #e6e6e6" class="nscs-table-handle bdl bdr">
		                  <span>
							  <c:choose>
							  	<c:when test="${storeSub.refundType ne 0}">
<%--						  			<a href="javaScript:cancelOrder('${storeSub.orderSn}')">取消订单</a>--%>
						  		</c:when>
						  		<c:when test="${storeSub.dlyoSatus eq 0}">
						  			<a href="javaScript:taken('${storeSub.orderSn}','${storeSub.actualTotal}')" class="btn-bluejeans"><p>付款自提</p></a>
						  			<a href="javaScript:cancelOrder('${storeSub.orderSn}')">取消订单</a>
						  		</c:when>
						  		<c:when test="${storeSub.dlyoSatus eq 1}">
						  			<a href="javaScript:taken('${storeSub.orderSn}','${storeSub.actualTotal}')" class="btn-bluejeans"><p>立即提货</p></a>
<%--						  			<a href="javaScript:cancelOrder('${storeSub.orderSn}')">取消订单</a>--%>
						  		</c:when>
						  		<c:when test="${storeSub.dlyoSatus eq 2}">已自提</c:when>
						  	</c:choose>
						  	
		                  </span>
              		  </td>
	                </c:if>
              </tr>
                </c:forEach>
              <tr style="display:none;">
                <td colspan="20"><div class="ncsc-goods-sku ps-container"></div></td>
              </tr>
                <tr>
              	<td colspan="10"></td>
              </tr>
              </tbody>
              </c:forEach>
          </table>
         <div class="clear"></div>
					<div style="margin-top:10px;" class="page clearfix">
				     			 <div class="p-wrap">
											<span class="p-num">
												<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
											</span>
				     			 </div>
		  				</div>
      </div>
      <div class="ncsc-layout-right"> </div>
    </div>
   <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp"%>
</body>
<script type="text/javascript">
    var contextPath="${contextPath}";
	function loginOut(){
		layer.confirm("是否退出?", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   window.location.href=contextPath+"/m/logout";
		   });
	}
	
	function pager(curPageNO){
		document.getElementById("curPageNO").value=curPageNO;
		document.getElementById("from1").submit();
	}
	
	function taken(order,actualTotal){
		layer.open({
			  title :"付款并自提",
			  id: "orderDialog",
			  type: 2, 
			  content: contextPath+"/m/store/shopStoreTakenLayout?subNumer="+order+"&actualTotal="+actualTotal, //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
			  area: ['900px', '315px']
			}); 
	}
	
	function cancelOrder(orderSn) {
		var url = contextPath + "/m/store/shopStoreCancelOrder/" +orderSn;
		
		layer.confirm("确认取消该订单吗？", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   $.ajax({
					url: url,
					type: "post",
					dataType: "json",
					success: function(result) {
						if(result == "OK") {
							layer.msg("取消成功",{icon: 1,time: 800},function(){
								location.reload();
							});
						} else {
							layer.msg("对不起，你操作的数据有误",{icon: 2});
						}
					}
				});
		   });
	}
	
</script>

</html>