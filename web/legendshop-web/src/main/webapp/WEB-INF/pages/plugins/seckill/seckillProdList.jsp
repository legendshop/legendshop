<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
    <jsp:useBean id="nowDate" class="java.util.Date" />    
         <c:if test='${empty list}' >
	        	<div class="selection_box search_noresult">
					<p class="search_nr_img"><img style="width: 5;height: 10px" src="<ls:templateResource item='/resources/templets/images/not_result.png'/>"></p> 
					<p style="text-align: center;font-size: 16px;margin-top: 10px;">没有找到<c:if test="${not empty param.keyword}">与“<span>${param.keyword}</span>”</c:if>相关的活动信息</p>
				</div>
		    </c:if>
		    
		    
        <ul class="clear"> 
            <c:forEach var="seckill" items="${list}">           
            <li>
              <a class="pro-img" href="${contextPath}/seckill/views/${seckill.id }">
	              <img src="<ls:images item='${seckill.seckillPic}' scale='4' />" style="vertical-align: middle;">
	              <c:if test="${seckill.stockTotal eq 0}">
	              	<em>已抢光</em>
	              </c:if>
              </a>
              <a href="${contextPath}/seckill/views/${seckill.id }" class="pro-name">
				<p>${seckill.seckillTitle }</p>
				<span>${seckill.seckillBrief}</span>
			  </a>
			  <div class="pro-stock clear">
				<p class="numBar"><em style="width: ${seckill.stockTotal}px;"></em></p>
				<span>还剩${seckill.stockTotal}件</span>
			  </div>
			  <div class="pro-price clear">
				<p>￥<span>${seckill.seckillLowPrice}</span></p>
				
				<c:choose>
					<c:when test="${seckill.stockTotal eq 0}">
						<a href="${contextPath}/seckill/views/${seckill.id }" class="not">已抢光</a>
					</c:when>
					<c:when test="${seckill.endTime<nowDate }">
						<a href="${contextPath}/seckill/views/${seckill.id }" class="not">活动已结束</a>
					</c:when>
					<c:when test="${nowDate>=seckill.startTime and nowDate<=seckill.endTime and seckill.stockTotal gt 0}">
						<a href="${contextPath}/seckill/views/${seckill.id }" class="rig-now">立即抢购</a>
					</c:when>
					<c:when test="${nowDate<seckill.startTime}">
						<a href="${contextPath}/seckill/views/${seckill.id }" class="not">去看看</a>
					</c:when>
				</c:choose>
				<%-- <c:if test="${seckill.stockTotal eq 0}">
					<a href="${contextPath}/seckill/views/${seckill.id }" class="not">已抢光</a>
				</c:if>
				<c:if test="${seckill.endTime<nowDate }"><a href="${contextPath}/seckill/views/${seckill.id }" class="not">活动已结束</a></c:if>
				<c:if test="${nowDate>=seckill.startTime and nowDate<=seckill.endTime and seckill.stockTotal gt 0}"><a href="${contextPath}/seckill/views/${seckill.id }" class="rig-now">立即抢购</a></c:if>
				<c:if test="${nowDate<seckill.startTime}"><a href="${contextPath}/seckill/views/${seckill.id }" class="not">去看看</a></c:if> --%>
			 </div>
			 
            </li>
            </c:forEach>
          </ul>
         <!-- querySeckill -->
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
					<input type="hidden" id="startTime" name="startTime" value="${startTime}" />
		   <%--  <form:form action="${contextPath}/seckillActivity/querySeckillList"	id="form1" method="get">
			</form:form> --%>
		<div class="clear"></div>
	 	<div style="margin-top:10px;" class="page clearfix">
    			 <div class="p-wrap">
						<span class="p-num">
							<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/>
						</span>
    			 </div>
 			</div>
