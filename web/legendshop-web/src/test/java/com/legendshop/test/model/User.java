package com.legendshop.test.model ;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
/**
 * 
 * @author Legendshop
 * @version 1.0.0
 *
 */

// 
public class User implements Serializable{

	private static final long serialVersionUID = -5704121142885073072L;

	// 
	private Long id ; 
		
	// 
	private String name ; 
		
	// 
	private String passwd ; 
		
	// 
	private String cssStyleName ; 
		
	// 
	private Date date ; 
		
	// 
	private Date dateTime ; 
		
	// 
	private java.sql.Time time ; 
		
	// 
	private String text ; 
		
	// 
    byte[] blob;
		
	// 
	private Double doubleName ; 
		
	
    public User() {}

    public User(Long id, String name, String passwd,String cssStyleName) {
        this.id = id;
        this.name = name;
        this.passwd = passwd;
        this.cssStyleName = cssStyleName;
        this.date = new Date();
        this.time = new java.sql.Time(date.getTime());
        this.dateTime = new Timestamp(new Date().getTime());
        this.text = "text " + System.currentTimeMillis();
        blob = new byte[1];
        blob[0] = 'a';
        doubleName = 100d;
    }
    
    public User(String name, String passwd,String cssStyleName) {
       this(null,name,passwd,cssStyleName );
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getCssStyleName() {
		return cssStyleName;
	}

	public void setCssStyleName(String cssStyleName) {
		this.cssStyleName = cssStyleName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public java.sql.Time getTime() {
		return time;
	}

	public void setTime(java.sql.Time time) {
		this.time = time;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public byte[] getBlob() {
		return blob;
	}

	public void setBlob(byte[] blob) {
		this.blob = blob;
	}

	public Double getDoubleName() {
		return doubleName;
	}

	public void setDoubleName(Double doubleName) {
		this.doubleName = doubleName;
	}

	
	

} 
