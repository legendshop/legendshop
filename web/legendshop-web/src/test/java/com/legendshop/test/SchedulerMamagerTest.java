package com.legendshop.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.legendshop.processor.auction.scheduler.SchedulerConstant;
import com.legendshop.processor.auction.scheduler.SchedulerMamager;

import java.util.Calendar;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:spring/applicationContext.xml"})
public class SchedulerMamagerTest {

    @Test
    public void addJob() throws InterruptedException {

        Date now = new Date();
        Date newDate = addSeconds(now, 5);

//       SchedulerMamager.getInstance().addJob("jobname",SchedulerConstant.JOB_GROUP_NAME,SchedulerConstant.TRIGGER_GROUP_NAME, AuctionsSchedulerJob.class,newDate, "");

       System.out.println("addJob sleep!");

        Thread.sleep(20000);

        System.out.println("addJob success!");

    }


    private static Date addSeconds(Date date, int seconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.SECOND, seconds);
        return calendar.getTime();
    }



}
