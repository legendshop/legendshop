package com.legendshop.test;

import java.util.Arrays;

public class QuickSort {

	public static void main(String[] args) {

		int[] arr = { 9, 4, 34, 6, 5, 234, 3, 2, 98, 12 };

		quickSort(arr, 0, arr.length - 1);

		System.out.println("排序结果:" + Arrays.toString(arr));

	}
	
	private static int count=1;

	private static void quickSort(int array[], int head, int tail) {

		if (head > tail) {
			return;
		}

		// i,j指向头和尾巴
		int i = head;
		
		int j = tail;
		
		int iPivot = array[i];
		/** < 选取枢轴 */

		while (i < j) {
			
			// 使用j,从序列最右端开始扫描,直到遇到比枢轴小的数
			while ((i < j) && (iPivot <= array[j])) {
				j--;
			}
			
			// 交换位置
			if (i < j) {
				array[i] = array[j];
				System.out.println("第"+count+"次交换; result="+Arrays.toString(array) +" i="+i +" 互换 j="+j);
				count++;
				i++;
			}

			// 使用i,从序列最左端开始扫描,直到遇到比枢轴小的数枢轴大的数
			while ((i < j) && (array[i] <= iPivot)) {
				i++;
			}
			// 交换位置
			if (i < j) {
				array[j] = array[i];
				System.out.println("第"+count+"次交换; result="+Arrays.toString(array) +" i="+i +" 互换 j="+j);
				count++;
				j--;
			}
		}

		// 最后填入枢轴位置
		array[j] = iPivot;
		
		// 这里就是对枢轴两边序列进行排序的递归调用
		quickSort(array, head, i - 1);
		
		quickSort(array, i + 1, tail);
		
	}

}
