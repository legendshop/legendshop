package com.legendshop.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.legendshop.model.ResultCustomPropertyDto;
import com.legendshop.model.entity.ProductProperty;
import com.legendshop.model.entity.ProductPropertyValue;

public class FastJsonTeset {
 public static void main(String[] args) {
	 List<ResultCustomPropertyDto> list = new ArrayList<ResultCustomPropertyDto>();
		ResultCustomPropertyDto dto1 = new ResultCustomPropertyDto();
		ProductProperty productProperty1 = new ProductProperty();
		productProperty1.setGroupId(1l);
		productProperty1.setGroupName("groupName");
		productProperty1.setId(1l);
		productProperty1.setIsCustom(2);
		productProperty1.setModifyDate(new Date());
		productProperty1.setRecDate(new Date());
		
		
		dto1.setProductProperty(productProperty1);
		
		
		List<ProductPropertyValue> propertyValueList = new ArrayList<ProductPropertyValue>();
		
		ProductPropertyValue value1 = new ProductPropertyValue();
		value1.setModifyDate(new Date());
		value1.setFile(null);
		value1.setId(2l);
		value1.setSequence(3l);
		
		ProductPropertyValue value2 = new ProductPropertyValue();
		value2.setModifyDate(new Date());
		value2.setFile(null);
		value2.setId(22l);
		value2.setSequence(31l);
		
		propertyValueList.add(value1);
		propertyValueList.add(value2);
		dto1.setPropertyValueList(propertyValueList);
		
		list.add(dto1);
	 String jsonString = JSON.toJSONString(list); 
	 System.out.println(jsonString);
	 
	 List<ResultCustomPropertyDto> list2 = JSON.parseArray(jsonString, ResultCustomPropertyDto.class);
	 
	 System.out.println(list2);
}
}
