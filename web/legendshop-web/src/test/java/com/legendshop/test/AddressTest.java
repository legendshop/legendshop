package com.legendshop.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.calcite.sql.type.SqlTypeName;

import com.alibaba.fastjson.JSON;
import com.legendshop.test.model.AddressProve;
import com.legendshop.test.model.AddressResult;


public class AddressTest {
	
	public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException {
		   String driver = "com.mysql.jdbc.Driver";
		    String url = "jdbc:mysql://192.168.0.148:3306/legendshop_sr1";
		    String username = "legendshop_sr1";
		    String password = "legendshop_sr1123";
		    Connection conn = null;
		    PreparedStatement pstm =null;
		    ResultSet rt = null;
		    try {
		        Class.forName(driver); //classLoader,加载对应驱动
		        conn = (Connection) DriverManager.getConnection(url, username, password);
		        
		        
		        //String sql = "INSERT INTO ls_provinces(id,provinceid,province,groupby,short_name) VALUES(?,?,?,?,?)";//省份
		        
		       // String sql = "INSERT INTO ls_cities(id,cityid,city,provinceid) VALUES(?,?,?,?)";//城市
		        
		        String sql = "INSERT INTO ls_areas(id,areaid,area,cityid) VALUES(?,?,?,?)";//地区
		        
		        
		        pstm = conn.prepareStatement(sql);
		        conn.setAutoCommit(false);
		        File file = new File("E:/address/citys.txt");
				String result=txt2String(file).trim();
				AddressResult addressResult=JSON.parseObject(result, AddressResult.class);
				
				//pstm=insertcity(391,pstm,addressResult.getResult(),35);//初始化城市
				
				pstm=insertarea(3672,pstm,addressResult.getResult(),389);//初始化地区
				
				pstm.executeBatch();
		        conn.commit();
	            conn.close();
		    } catch (ClassNotFoundException e) {
		        e.printStackTrace();
		    } catch (SQLException e) {
		        e.printStackTrace();
		    }
		
	}
	
	
	public static  PreparedStatement insertcity(Integer start,PreparedStatement pstm,List<AddressProve> list,Integer provinceId) throws SQLException{
		for (int i = 0; i <list.size(); i++) {
			AddressProve prove= list.get(i);
			pstm.setInt(1, ++start);
            pstm.setInt(2,Integer.valueOf(prove.getCode()));
            pstm.setString(3,prove.getName());
            pstm.setInt(4,provinceId);
            pstm.addBatch();
		}
		return pstm;
	}
	
	public static  PreparedStatement insertarea(Integer start,PreparedStatement pstm,List<AddressProve> list,Integer cityId) throws SQLException{
		for (int i = 0; i <list.size(); i++) {
			AddressProve prove= list.get(i);
			pstm.setInt(1, ++start);
            pstm.setInt(2,Integer.valueOf(prove.getCode()));
            pstm.setString(3,prove.getName());
            pstm.setInt(4, cityId);
           // pstm.setNull(5,java.sql.Types.NULL);
            pstm.addBatch();
		}
		return pstm;
	}
	
	 /**
     * 读取txt文件的内容
     * @param file 想要读取的文件对象
     * @return 返回文件内容
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
     */
    public static String txt2String(File file) throws UnsupportedEncodingException, FileNotFoundException{
        StringBuilder result = new StringBuilder();
        
        InputStreamReader read = new InputStreamReader(
                new FileInputStream(file), "UTF-8");
        try{
            BufferedReader br = new BufferedReader(read);//构造一个BufferedReader类来读取文件
            String s = null;
            while((s = br.readLine())!=null){//使用readLine方法，一次读一行
                result.append(System.lineSeparator()+s);
            }
            br.close();    
        }catch(Exception e){
            e.printStackTrace();
        }
        return result.toString();
    }
    
}
