package com.legendshop.test.buffer;

import java.nio.ByteBuffer;

public class BufferTest {

	public static void main(String[] args) {
		// reset();
		// duplicate();
		// slice();
		asReadOnlyBuffer();
	}

	private static void asReadOnlyBuffer() {
		ByteBuffer b=ByteBuffer.allocate(15);
		for(int i=0;i<10;i++){
		    b.put((byte)i);
		}
		ByteBuffer readOnly=b.asReadOnlyBuffer();            //创建只读缓冲区
		readOnly.flip();
		while(readOnly.hasRemaining()){
		    System.out.print(readOnly.get()+"");
		}
		System.out.println();
		b.put(2, (byte)20);                                //修改原始缓冲区的数据
		readOnly.flip();
		while(readOnly.hasRemaining()){
		    System.out.print(readOnly.get()+"");            //新的改动，在只读缓冲区内可见
		}
		readOnly.put((byte)10); //ReadOnlyBufferException
	}

	private static void slice() {
		// 从堆中分配
		ByteBuffer b = ByteBuffer.allocate(15);
		for (int i = 0; i < 10; i++) {
			b.put((byte) i);
		}
		b.position(2);
		b.limit(6);
		ByteBuffer subBuffer = b.slice(); // 生成子缓冲区
		System.out.println(b);
		System.out.println(subBuffer);
		for (int i = 0; i < subBuffer.capacity(); i++) {
			byte bb = subBuffer.get(i);
			bb *= 3; // 在子缓冲区中，将每一个元素都乘以3
			subBuffer.put(i, bb);
		}
		b.position(0);
		b.limit(b.capacity()); // 重置父缓冲区，并查看父缓冲区的数据
		while (b.hasRemaining()) {
			System.out.print(b.get() + "");
		}
	}

	private static void reset() {
		// 从堆中分配
		ByteBuffer b = ByteBuffer.allocate(15);
		// 从既有的数组中创建
		// byte array[] = new byte[1024];
		// ByteBuffer buffer = ByteBuffer.wrap(array);
		for (int i = 0; i < 10; i++) {
			b.put((byte) i);
		}
		b.flip(); // 准备读取数据
		for (int i = 0; i < b.limit(); i++) {
			System.out.print(b.get());
			if (i == 4) {
				b.mark(); // 在第4个位置做mark
				System.out.print("( mark at " + i + "  )");
			}
		}
		b.reset(); // 回到mark的位置，并处理后续数据
		System.out.println("\nreset to mark");
		while (b.hasRemaining()) { // 后续所有数据都将处理
			System.out.print(b.get());
		}
	}

	private static void duplicate() {
		// 从堆中分配
		ByteBuffer b = ByteBuffer.allocate(15);
		for (int i = 0; i < 10; i++) {
			b.put((byte) i);
		}

		ByteBuffer copy = b.duplicate();
		System.out.println("After b.duplicate()");
		System.out.println(b);
		System.out.println(copy);
		System.out.println(b.get());

		copy.flip();
		System.out.println("After copy.flip()");
		System.out.println(b);
		System.out.println(copy);

		copy.put(9, (byte) 100);
		System.out.println("After copy.put()");
		System.out.println(b);
		System.out.println(copy);
		System.out.println(b.get(9));
		System.out.println(copy.get(9));

	}
}
