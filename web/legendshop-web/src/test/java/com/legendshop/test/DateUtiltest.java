package com.legendshop.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.legendshop.util.DateUtil;
import com.legendshop.util.DateUtils;


public class DateUtiltest {

	/**
	 * @param args
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws ParseException {
		int value = DateUtil.daysInMonth("2013", "11");
		System.out.println(value);
		Date startDate = DateUtil.getDate("2013", "01", "01");
		Date endDate = DateUtil.getYear(startDate, -1);
		System.out.println(startDate);
		System.out.println(endDate);
		System.out.println(DateUtil.getMonth());
		System.out.println(DateUtil.getNowYear());
		
		
		SimpleDateFormat dateFormat1=new SimpleDateFormat("yyyy-M-d");
		System.out.println(dateFormat1.parse("2015-03-3"));
	}

}
