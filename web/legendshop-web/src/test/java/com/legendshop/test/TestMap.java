package com.legendshop.test;


public class TestMap {
	
	public static void main(String[] args) {
		test();
	}

	public static void test(){
        String[][] lists = new String[][]{{"a","b","c"},{"d","c"},{"e","f","g"}};
        String[] result = getString(lists);
        for(String str :result){
            System.out.println(str);
        }
    }
     
    public static String[] getString(String[][] lists){
        if(lists.length==1){
            return lists[0];
        }
        String[] temp = lists[lists.length-1];
         
        String[][] subLists = new String[lists.length-1][];
        for(int i=0;i<subLists.length;i++){
            subLists[i]=lists[i];
        }
        String[] pre = getString(subLists);
         
        String[] result = new String[temp.length*pre.length];
        int index = 0;
        for(String obj : temp){
            for(String re : pre){
                result[index++]=re+obj;
            }
        }
        return result;
    }
}
