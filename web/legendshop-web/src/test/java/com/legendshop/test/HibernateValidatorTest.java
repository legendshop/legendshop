package com.legendshop.test;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import com.legendshop.model.entity.DeliverGoodsAddr;

public class HibernateValidatorTest {

	 public static void main(String[] args) {    
	        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();    
	        Validator validator = factory.getValidator();    
	    
	        DeliverGoodsAddr entity = new DeliverGoodsAddr();    
	        entity.setAddrId(4l);
	        entity.setContactName("act");
	        Set<ConstraintViolation<DeliverGoodsAddr>> constraintViolations = validator.validate(entity);    
	        for (ConstraintViolation<DeliverGoodsAddr> constraintViolation : constraintViolations) {    
	            System.out.println("对象属性:"+constraintViolation.getPropertyPath());    
	            System.out.println("国际化key:"+constraintViolation.getMessageTemplate());    
	            System.out.println("错误信息:"+constraintViolation.getMessage());    
	        }    
	    
	    }    
}
