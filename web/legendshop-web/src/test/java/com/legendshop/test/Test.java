package com.legendshop.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Test {
	
	private static final String USER_AGENT_VALUE = "Mozilla/4.0 (compatible; MSIE 6.0; Windows XP)";
	public static void main(String[] args) {
		StringBuffer strBuf = new StringBuffer();
		BufferedReader reader = null;
		int timeout = 10 * 1000; // 5秒
		HttpURLConnection httpConnection = null;
		int responseCode = 0;
		try {
			httpConnection = getHttpURLConnection("http://tmall.gxyclub.com/user/service/register");
			
			/*httpConnection = getHttpURLConnection("http://192.168.0.105:8080/legendshop/user/service/register");*/
			
			// 以post方式通信
			httpConnection.setRequestMethod("POST");
			// 设置连接超时时间
			httpConnection.setConnectTimeout(10 * 1000);
			httpConnection.setReadTimeout(10 * 1000);

			// User-Agent
			httpConnection.setRequestProperty("User-Agent", USER_AGENT_VALUE);
			httpConnection.setRequestProperty("Accept-Charset", "UTF-8");
			httpConnection.setUseCaches(true);

			// 允许输入输出
			httpConnection.setDoInput(true);
			httpConnection.setDoOutput(true);

			StringBuffer params = new StringBuffer();

			params.append("userName").append("=").append("ytest020").append("&");
			params.append("password").append("=").append("bh32dm2OuNeAbP2hMKk%2BID9TsoonnMLQ8luTNkYasJ1p%2FnvWCAIp2g%3D%3D").append("&");
			params.append("mobile").append("=").append("19989698956").append("&");
			params.append("email").append("=").append("").append("&");
			params.append("token").append("=").append("c8b75a107c3bedd816d8d100e1de15c4").append("&");
			params.append("jsessionId").append("=").append("449E5CDEC16A8BD063C98B13396BB2C6");

			byte[] bypes = params.toString().getBytes("utf-8");

			httpConnection.getOutputStream().write(bypes);// 输入参数

			reader = new BufferedReader(new InputStreamReader(
					httpConnection.getInputStream(), "utf-8"));// 转码。
			String line = null;
			while ((line = reader.readLine()) != null) {
				strBuf.append(line + " ");
			}
			try {
				if (httpConnection != null) {
					responseCode = httpConnection.getResponseCode();
				}
			} catch (IOException e) {
			}
			System.out.println(strBuf.toString());
		} catch (MalformedURLException e) {
		} catch (IOException e) {
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
				}
			}
			if (httpConnection != null) {
				httpConnection.disconnect();
			}
		}
	}

	/**
	 * get HttpURLConnection
	 * 
	 * @param strUrl
	 *            url地址
	 * @return HttpURLConnection
	 * @throws IOException
	 */
	public static HttpURLConnection getHttpURLConnection(String strUrl)
			throws IOException {
		URL url = new URL(strUrl);
		HttpURLConnection httpURLConnection = (HttpURLConnection) url
				.openConnection();
		return httpURLConnection;
	}

	public static void main1(String[] args) {
		//创建一个属性对象
		Properties props=new Properties();
		//指定SMTP服务器
		props.put("mail.smtp.host","smtp.163.com");
		//指定是否需要ＳＭＴＰ验证
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port","25");
		props.put("mail.transport.protocol","smtp");
		//创建会话
		Session session=Session.getInstance(props);
		session.setDebug(true);
		//创建一个Message对象
		Message mess=new MimeMessage(session);
		try{
		mess.setFrom(new InternetAddress("zcs8665757@163.com"));//发件人
		//收件人
		mess.setRecipient(RecipientType.TO, new InternetAddress("872143192@qq.com"));
		mess.setSentDate(new Date());
		mess.setSubject("晚上好啊");
		mess.setText(new String("晚上好！".getBytes(),"UTF-8"));//发送内容
		//指定邮件的优先级 1：紧急 3:普通  5：缓慢
		mess.setHeader("X-Priority", "1");
		//创建一个传输对象
		Transport trans=session.getTransport("smtp");
		//连接SMTP服务器   
		trans.connect("smtp.163.com","zcs8665757@163.com","*******");//这里是发件人的邮箱用户名和密码（我的密码就先隐藏啦）
		trans.sendMessage(mess,mess.getAllRecipients());
		trans.close();
		}catch(Exception e){
		e.printStackTrace();
		}
		}  
}
