package com.legendshop.test.model;

public class AddressProve {

    private String code;
    private String name;
    private String parentCode;
    private String post;
    public void setCode(String code) {
         this.code = code;
     }
     public String getCode() {
         return code;
     }

    public void setName(String name) {
         this.name = name;
     }
     public String getName() {
         return name;
     }

    public void setParentCode(String parentCode) {
         this.parentCode = parentCode;
     }
     public String getParentCode() {
         return parentCode;
     }

    public void setPost(String post) {
         this.post = post;
     }
     public String getPost() {
         return post;
     }

}