package com.legendshop.test;

import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
 
public class AntPathMatcherTest{
	
	public static void main(String[] args) {
		
        PathMatcher matcher = new AntPathMatcher();  
         
        //完全路径匹配  
        //String requestPath="/user/list.htm?username=aaa&id=2&no=1&page=20";
        //String patternPath="/user/list.htm**";
    
        //不完整路径匹配  
        //String requestPath="/app/pub/login.do";
        //String patternPath="/**/login.do";
          
        //模糊路径匹配  
        //String requestPath="/app/pub/login.do";
        //String patternPath="/**/*.do";
          
        //模糊单字符路径匹配  
//        String requestPath = "/app/pub/login.do";
//        String patternPath = "/**/lo?in.do";
  
      String requestPath = "/admin/adminUser/update/1";
      String patternPath = "/admin/adminUser/update";
        
          
        boolean result = matcher.match(patternPath, requestPath);  
         
       System.out.println(result);
    }  
 
}