package com.legendshop.test;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailUtil {
	static int port = 25;
	static String server = "smtp.qq.com";
	static String from = "15151191@qq.com";
	static String user = "15151191";
	static String password = "xxx";

	public static void main(String[] args) {
		MailUtil.sendEmail("gmhwq@126.com", "subject", "body");
	}

	public static void sendEmail(String email, String subject, String body) {
		try {
			Properties props = new Properties();
			props.setProperty("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", server);
			props.put("mail.smtp.port", String.valueOf(port));
			props.put("mail.smtp.auth", "true");
			// Transport transport=null;
			MyAuthenticator myauth = new MyAuthenticator(user, password);
			Session session = Session.getDefaultInstance(props, myauth);
			session.setDebug(true);
			// transport=session.getTransport("smtp");
			// transport.connect(server,user,password);
			MimeMessage msg = new MimeMessage(session);
			msg.setSentDate(new Date());
			InternetAddress fromAddress = new InternetAddress(from);
			msg.setFrom(fromAddress);
			InternetAddress[] toAddress = new InternetAddress[1];
			toAddress[0] = new InternetAddress(email);
			msg.setRecipients(Message.RecipientType.TO, toAddress);
			msg.setSubject(subject, "GBK");
			msg.setText(body, "GBK");
			msg.saveChanges();
			// transport.sendMessage(msg,msg.getAllRecipients());System.out.print("ok");
			Transport.send(msg);
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
}

class MyAuthenticator extends javax.mail.Authenticator {
	private String strUser;
	private String strPwd;

	public MyAuthenticator(String user, String password) {
		this.strUser = user;
		this.strPwd = password;
	}

	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(strUser, strPwd);
	}
}
