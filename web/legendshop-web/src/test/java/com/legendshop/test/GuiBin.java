package com.legendshop.test;


public class GuiBin {

	public void Merge(int[] array, int left, int center, int right) {

		System.out.println("left=" + left + " center=" + center + " right="
				+ right);
		int i = left; // i是第一段序列的下标
		int j = center + 1; // j是第二段序列的下标
		int k = 0; // k是临时存放合并序列的下标
		int[] array2 = new int[right - left + 1]; // array2是临时合并序列

		System.out.println("i=" + i + " j=" + j + " k=" + k);

		// 扫描第一段和第二段序列，直到有一个扫描结束
		while (i <= center && j <= right) {
			// 判断第一段和第二段取出的数哪个更小，将其存入合并序列，并继续向下扫描
			if (array[i] <= array[j]) {
				array2[k] = array[i];
				i++;
				k++;
			} else {
				array2[k] = array[j];
				j++;
				k++;
			}
		}

		// 若第一段序列还没扫描完，将其全部复制到合并序列
		while (i <= center) {
			array2[k] = array[i];
			i++;
			k++;
		}

		// 若第二段序列还没扫描完，将其全部复制到合并序列
		while (j <= right) {
			array2[k] = array[j];
			j++;
			k++;
		}

		// 将合并序列复制到原始序列中
		for (k = 0, i = left; i <= right; i++, k++) {
			array[i] = array2[k];
		}
	}

	public void MergePass(int[] array, int gap, int length) {
		int i = 0;

		// 归并gap长度的两个相邻子表
		for (i = 0; i + 2 * gap - 1 < length; i = i + 2 * gap) {
			System.out.println(i + 2 * gap - 1);
			Merge(array, i, i + gap - 1, i + 2 * gap - 1);
		}

		// 余下两个子表，后者长度小于gap
		if (i + gap - 1 < length) {
			Merge(array, i, i + gap - 1, length - 1);
		}
	}

	public int[] sort(int[] list) {
		for (int gap = 1; gap < list.length; gap = 2 * gap) {
			MergePass(list, gap, list.length);
			System.out.print("gap = " + gap + ":\t");
			this.printAll(list);
		}
		return list;
	}

	// 打印完整序列
	public void printAll(int[] list) {
		for (int value : list) {
			System.out.print(value + "\t");
		}
		System.out.println();
	}

	public static void main(String[] args) {
		/*
		 * int[] array = { 57, 68, 59, 52, 72, 28, 96, 33 };
		 * 
		 * GuiBin merge = new GuiBin(); System.out.print("排序前:\t\t");
		 * merge.printAll(array); merge.sort(array);
		 * System.out.print("排序后:\t\t"); merge.printAll(array);
		 */
		
		
	}

}
