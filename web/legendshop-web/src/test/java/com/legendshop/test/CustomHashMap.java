package com.legendshop.test;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Objects;


public class CustomHashMap<K,V>{

	
	@SuppressWarnings("unchecked")
	private LinkedList<Entry<K,V>>[]  arr  = new LinkedList[9]; //Map的底层结构就是：数组+链表!
	
	private int size;
	
	public void put(K key,V value){
		Entry<K, V> entry=new Entry<K, V>(key, value);
		//Hash key
		int hashcode=key.hashCode();
		hashcode=hashcode<0?-hashcode:hashcode;
		int index=hashcode%arr.length;
		LinkedList<Entry<K,V>> object=arr[index];
		if(object==null){
			LinkedList<Entry<K,V>> list=new LinkedList<Entry<K,V>>();
			list.add(entry);
			arr[index]=list;
		}else{
			for (int i = 0; i < object.size(); i++) {
				Entry<K,V> e=object.get(i);
				if(e.getKey().equals(key)){
					e.setValue(value);
					return;  //键值重复直接覆盖！
				}
			}
			object.add(entry);
		}
		size++;
	}
	
	public V get(K key){
		int index=key.hashCode()%arr.length;
		LinkedList<Entry<K,V>> object=arr[index];
		if(object!=null){
			for (int i = 0; i < object.size(); i++) {
				Entry<K,V> e=object.get(i);
				if(e.getKey().equals(key)){
					return e.getValue();
				}
			}
		}
		return null;
	}
	
	public Entry<K,V> remove(K key){
		if (size == 0) {
	       return null;
	    }
		 
		int index=key.hashCode()%arr.length;
		LinkedList<Entry<K,V>> array=arr[index];
		Entry<K,V> entry=null;
		if(array!=null){
			if(array.size()==1){ //说明只有一个值
				Entry<K,V> e=array.get(0);
				if(e.getKey().equals(key)){
					entry=e;
					array=null;
					arr[index]=null;
					size--;
				}
			}else{
				for (Iterator<Entry<K, V>> iterator = array.iterator(); iterator.hasNext();) {
					Entry<K, V> e = (Entry<K, V>) iterator.next();
					if(e.getKey().equals(key)){
						entry=e;
						iterator.remove();
						break;
					}
				}
			}
		}
		return entry;
	}
	
	
	public boolean containsKey(K key){
		int index=key.hashCode()%arr.length;
		LinkedList<Entry<K,V>> array=arr[index];
		for(int i=0;i<array.size();i++){
			Entry<K,V> e=array.get(i);
			if(e.getKey().equals(key)){
				return true;
			}
		}
		return false;
	}
	
	public boolean containsValue(V value){
		for (int i = 0; i < size; i++) {
			LinkedList<Entry<K,V>> array=arr[i];
			if(array!=null){
				for (int j = 0; j < array.size(); j++) {
					Entry<K,V> entry=array.get(j);
					if(entry!=null){
						if (value.equals(entry.value))
		                    return true; 
					}
				}
			}
		}
		return false;
	}
	
	
	
	class Entry<K,V>{
		
		
		private K key;
		
		private V value;

		public K getKey() {
			return key;
		}

		public void setKey(K key) {
			this.key = key;
		}

		public V getValue() {
			return value;
		}

		public void setValue(V value) {
			this.value = value;
		}

		public Entry(K key, V value) {
			super();
			this.key = key;
			this.value = value;
		}

		@Override
		public int hashCode() {
			  return Objects.hashCode(getKey()) ^ Objects.hashCode(getValue());
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Entry other = (Entry) obj;
			if (key == null) {
				if (other.key != null)
					return false;
			} else if (!key.equals(other.key))
				return false;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}
		
	}
	
	public static void main(String[] args) {
		CustomHashMap<String,String> m = new CustomHashMap<String,String>();
		m.put("测试", "谢霆锋");
		m.put("测试", "刘德华");
		m.put("测试2", "周润发");
		System.out.println(m.size);
		String w = m.get("测试");
		m.remove("测试");
	    w = m.get("测试");
		System.out.println(w); 
	}
	
	
}
