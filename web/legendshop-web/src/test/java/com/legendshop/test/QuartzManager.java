package com.legendshop.test;

import java.util.Date;

import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class QuartzManager {
	
	private static Logger log = LoggerFactory.getLogger(QuartzManager.class);

	private static SchedulerFactory gSchedulerFactory = new StdSchedulerFactory(); 
	
    private static String JOB_GROUP_NAME = "EXTJWEB_JOBGROUP_NAME";  
    
    private static String TRIGGER_GROUP_NAME = "EXTJWEB_TRIGGERGROUP_NAME";  
    
    
    /** 
     * 添加一个定时任务，使用默认的任务组名，触发器名，触发器组名 
     * 
     * @param jobName 
     *            任务名 
     * @param jobClass 
     *            任务 
     * @param time 
     *            时间设置，参考quartz说明文档 
     * @throws SchedulerException 
     * @throws ParseException 
     */  
    public static void addJob(String jobName, Class<? extends Job> jobClass, Date _time) {  
        try {  
            Scheduler sched = gSchedulerFactory.getScheduler();  
            
            //用于描叙Job实现类及其他的一些静态信息，构建一个作业实例
            JobDetail jobDetail = JobBuilder.newJob(jobClass).withIdentity(jobName, JOB_GROUP_NAME).build();
            jobDetail.getJobDataMap().put("love", "I love you very much!");
            
            //构建一个触发器，规定触发的规则
            Trigger trigger = TriggerBuilder.newTrigger()//创建一个新的TriggerBuilder来规范一个触发器
            		 .withIdentity(jobName, TRIGGER_GROUP_NAME)//给触发器起一个名字和组名
            		 .startAt(_time)
                     .build();
            
            //向Scheduler中添加job任务和trigger触发器
            sched.scheduleJob(jobDetail, trigger);
            
            // 启动  
            if (!sched.isShutdown()){  
                sched.start();  
            }  
        } catch (SchedulerException e) {  
            e.printStackTrace();  
            log.error("添加作业=> [作业名称：" + jobName + " 作业组：" + JOB_GROUP_NAME + "]=> [失败]");
        }  
    }  
    
    /**
     * 暂停作业
     * @param name
     */
    public void pauseJob(String name){
        try {
        	Scheduler sched = gSchedulerFactory.getScheduler();  
            JobKey jobKey = JobKey.jobKey(name, JOB_GROUP_NAME);
            sched.pauseJob(jobKey);
            log.info("暂停作业=> [作业名称：" + name + " 作业组：" + JOB_GROUP_NAME + "] ");
        } catch (SchedulerException e) {
            e.printStackTrace();
            log.error("暂停作业=> [作业名称：" + name + " 作业组：" + JOB_GROUP_NAME + "]=> [失败]");
        }
    }
    
     
    /**
     * 恢复作业
     * @param name
     * @param group
     */
    public void resumeJob(String name, String group){
        try {
        	Scheduler scheduler = gSchedulerFactory.getScheduler();
            JobKey jobKey = JobKey.jobKey(name, JOB_GROUP_NAME);         
            scheduler.resumeJob(jobKey);
            log.info("恢复作业=> [作业名称：" + name + " 作业组：" + JOB_GROUP_NAME + "] ");
        } catch (SchedulerException e) {
            e.printStackTrace();
            log.error("恢复作业=> [作业名称：" + name + " 作业组：" + JOB_GROUP_NAME + "]=> [失败]");
        }       
    }
    
    
    
    /** 
     * 修改一个任务的触发时间(使用默认的任务组名，触发器名，触发器组名) 
     * 
     * @param jobName 
     * @param time 
     */  
    public static void modifyJobTime(String jobName, Date _time) {  
    	   try {
    		   Scheduler sched = gSchedulerFactory.getScheduler();  
               //构建一个触发器，规定触发的规则
               Trigger trigger = sched.getTrigger(new TriggerKey(jobName, TRIGGER_GROUP_NAME));
               if(trigger == null) {  
                   return;  
               } 
               
    		   if(!trigger.getStartTime().equals(_time)){
    			   JobDetail jobDetail = sched.getJobDetail(new JobKey(jobName,JOB_GROUP_NAME));
    			   Class<? extends Job> objJobClass = jobDetail.getJobClass();  
                   removeJob(jobName);
                   addJob(jobName, objJobClass, _time);  
                   System.out.println("-----------");
    		   }
               log.info("修改作业触发时间=> [作业名称：" + jobName + " 作业组：" + JOB_GROUP_NAME + "] ");
           } catch (SchedulerException e) {
               e.printStackTrace();
               log.error("修改作业触发时间=> [作业名称：" + jobName + " 作业组：" + JOB_GROUP_NAME + "]=> [失败]");
           }
    }  
  
    
    
    /** 
     * 移除一个任务(使用默认的任务组名，触发器名，触发器组名) 
     * 
     * @param jobName 
     */  
    public static void removeJob(String jobName) {  
        try {  
            Scheduler sched = gSchedulerFactory.getScheduler(); 
          
            sched.pauseTrigger(new TriggerKey(jobName, JOB_GROUP_NAME));// 停止触发器  
            sched.unscheduleJob(new TriggerKey(jobName, JOB_GROUP_NAME));// 移除触发器  
            // 删除任务  
            sched.deleteJob(new JobKey(jobName, JOB_GROUP_NAME));
        } catch (Exception e) {  
            e.printStackTrace();  
            throw new RuntimeException(e);  
        }  
    }  
    
    
  
   
    
    
    
    
}
