package com.legendshop.test;


public class TimerTest {
	    public static void main(String[] args)  {
	    	  int b=2;
	    	  TimerTest test=new TimerTest();
	          System.out.println(test.test(b));
	          b=4;
	          System.out.println("--------------");
	          System.out.println(b);
	    }
	    public int test(final int b) {
	        final int a = 10;
	        new Thread(){
	            public void run() {
	            	try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
	                System.out.println(a);
	                System.out.println(b);
	            };
	        }.start();
	        return b;
	    }
}
