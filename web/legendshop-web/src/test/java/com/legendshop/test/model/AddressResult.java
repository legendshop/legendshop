package com.legendshop.test.model;

import java.util.List;

public class AddressResult {  
	
	private List<AddressProve> result;

	public List<AddressProve> getResult() {
		return result;
	}

	public void setResult(List<AddressProve> result) {
		this.result = result;
	}
}