package com.legendshop.test;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.annotation.Resource;

import com.legendshop.util.AppUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.legendshop.model.entity.Attachment;
import com.legendshop.uploader.service.AttachmentService;

/**
 * 注意AttachmentDownloadUtil.xml必须在resources目录，否则会找不到
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/AttachmentDownloadUtil.xml")
public class AttachmentDownloadUtil extends AbstractJUnit4SpringContextTests{

	//图片服务器的地址
	private String PHOTO_SERVER_ADDR = "http://192.168.0.148:3005/photoserver/photo/";

	@Resource(name="attachmentService")
	AttachmentService attachmentService;
	
	@Before
	public void setUp() throws Exception {
		System.out.println("start setUp with " + PHOTO_SERVER_ADDR);
	}
	
	/**
	 * 从110下载图片到本地
	 */
	@Test
	public void downloadFromPhotoServer(){
		System.out.println("start to down images");
		/*List<Attachment> attachments = attachmentService.getAttachmentByShopId(53l);*/
		List<Attachment> attachments = attachmentService.getAllAttachment();
		
		System.out.println("start to down images " + attachments.size());
		System.out.println(attachments.size());
		String downpath = "c:/down_pic/";
		URL url = null;
        HttpURLConnection conn = null;
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        File f = null;
		for (Attachment attachment : attachments) {
			try {
				String filePath = attachment.getFilePath();

				if(AppUtils.isBlank(filePath)){
					System.out.println("filePath is null");
					continue;
				}

				System.out.println(filePath);
				f = new File(downpath+attachment.getFilePath());
				String path = filePath.substring(0,filePath.lastIndexOf("/"));
				File p = new File(downpath+path);
				p.mkdirs();
				url = new URL(PHOTO_SERVER_ADDR + filePath);  //TODO 使用前先更改对应的地址
				conn = (HttpURLConnection)url.openConnection();  
				conn.connect();
				bis = new BufferedInputStream(conn.getInputStream());
				bos = new BufferedOutputStream(new FileOutputStream(f));
				int len = 2048;
				byte[] b = new byte[len];
				while ((len = bis.read(b)) != -1)
				{
				    bos.write(b, 0, len);
				}
				bos.flush();
				bis.close();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}finally{
				conn.disconnect();
			}
    	}
		
		System.out.println("finish down images");
	}
	
}
