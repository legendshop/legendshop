package com.legendshop.test;

import org.junit.Test;

import com.alibaba.druid.filter.config.ConfigTools;

/**
 * Created by xuliugen on 2017/6/25.
 */
public class ConfigToolsDemo {

	// 上述生成的私钥
	private static final String PRIVATE_KEY_STRING = "MIIBVQIBADANBgkqhkiG9w0BAQEFAASCAT8wggE7AgEAAkEAifb3LGEarlLHWXuiT+lJDH9HLjIxEhfGz24fB8GI7BOyw2lFj2xc8aR9DsxWs/i0P9nL8psds5s+kaYo4Jd9DQIDAQABAkBgtDQi7lmWUT2cUlW+L6XdVaWeEt5kcTgQk366oASKfGitNh4gyUJU+FVKvgO9gFqlRQ3VRNC5Vj2k2j/+q5ThAiEAwC6LU1cqCKVCm54DfGT0QtQsf8ywpMziktbB5tWJHlUCIQC3x2cZAeCH22q+3b5EabBA0PnvSTGqXATXNA0ybbKr2QIhAKbuRvsnNU25Lrg5ctG0Zy89WUHRLIaZqRzfCiJVzPrZAiEAoX26jdIFcLyRp5kJGerZc4tvJSLnXrEvm7/knNyidbkCIHfEXsdrF5WRiqCmieQrSdX0fg5hDrUSGZ65/qAZMiCK";

	@Test
	public void demo() throws Exception {
		// 密码明文，也就是数据库的密码
		String plainText = "legendshop_dev123";
		System.out.printf(ConfigTools.encrypt(PRIVATE_KEY_STRING, plainText));
	}
}