package com.legendshop.test;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class TestJobOne implements Job {
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap dataMap = context.getJobDetail().getJobDataMap();
		String strData = dataMap.getString("love");
		
		System.out.println(Thread.currentThread().getId()+"--------------------"+strData);
		System.out.println("你指定2013-11-27号15:34:01分执行已经触发！-------------------");	
	}
}
