package com.legendshop.test;

import java.io.File;
import java.util.List;
import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

/**
 * 发送QQ企业邮件
 * 
 */
public class SendQQMailTest3 {

	public static void main(String[] args) {
		SendQQMailTest3 test = new SendQQMailTest3();
		test.sendAttachMessage("15151191@qq.com", "gzhjwtech@sina.com", "测试", "conText", false, null, null);
	}

	/**
	 * 发送HTML信息并有附件的邮件. <br>
	 */
	public void sendAttachMessage(String toAddr, String fromAddr, String subject, String conText, boolean isHtml,
			List<File> inLineImgs, List<File> attachments) {
		 Properties properties = new Properties();
	      properties.put("mail.smtp.host", "smtp.sina.com");
	       properties.put("mail.smtp.port", 25);
	       properties.put("mail.smtp.auth", "true");

		try {
			System.out.println("start to send mail");
			JavaMailSenderImpl senderImpl = new JavaMailSenderImpl();
			senderImpl.setJavaMailProperties(properties);
			senderImpl.setUsername("gzhjwtech@sina.com");
			senderImpl.setPassword("=[;._PL<");
			
			  // Authenticator authenticator = new Email_Authenticator("gzhjwtech@sina.com", "=[;._PL<");
		        //javax.mail.Session sendMailSession = javax.mail.Session.getDefaultInstance(properties,authenticator);
			
			MimeMessage mailMessage = senderImpl.createMimeMessage();
		      //MimeMessage mailMessage = new MimeMessage(sendMailSession);
			MimeMessageHelper messageHelper = new MimeMessageHelper(mailMessage, true, "utf-8");

			messageHelper.setTo(toAddr);
			messageHelper.setFrom(fromAddr);
			messageHelper.setSubject(subject);
			messageHelper.setText(conText, isHtml);

//			// 附图
//			for (File file : inLineImgs) {
//				messageHelper.addInline(MimeUtility.encodeWord(file.getName()), file);
//			}
			// 附件内容
//			for (File file : attachments) {
//				messageHelper.addAttachment(MimeUtility.encodeWord(file.getName()), file);
//			}
			senderImpl.send(mailMessage);
			senderImpl.send(mailMessage);
			//Transport.send(mailMessage);
			System.out.println("finish send mail");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
