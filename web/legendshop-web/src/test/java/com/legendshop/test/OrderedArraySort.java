package com.legendshop.test;


/**
 * 将两个有序数组归并为一个升序数组-Java实现
 * 
 * @author Tony
 * 
 */
public class OrderedArraySort {

	public static void main(String[] args) {

		int[] arrayA = { 1, 3, 5, 7, 9, 11, 12, 13 };
		int[] arrayB = { 2, 3, 6, 8, 10 };
		int i = 0; // 数组m的起始位置,用于数组m下标计数
		int j = 0; // 数组n的起始位置,用于数组n下标计数
		int[] temps = new int[arrayA.length + arrayB.length];
		
		while (i < arrayA.length && j < arrayB.length) {
			if (arrayA[i] <= arrayB[j]) {
				temps[i + j] = arrayA[i];
				i++;
			} else {
				temps[i + j] = arrayB[j];
				j++;
			}
		}

		while (i < arrayA.length) {
			temps[i + j] = arrayA[i];
			i++;
		}
		while (j < arrayB.length) {
			temps[i + j] = arrayB[i];
			j++;
		}

		for (int k = 0; k < temps.length; k++) {
			System.out.print(temps[k] + "\t");
		}

	}

}
