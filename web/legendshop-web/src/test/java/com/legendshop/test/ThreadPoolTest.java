package com.legendshop.test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * 
 * @author Tony
 *
 */
public class ThreadPoolTest {

	
	public static void main(String[] args) {
		ExecutorService executorService=Executors.newFixedThreadPool(3);
		for(int i=0;i<10;i++){
			final int task=i;
			executorService.execute(new Runnable() {
				
				@Override
				public void run() {
					for (int j = 0; j < 10; j++) {
						try {
							Thread.sleep(20);
						} catch (Exception e) {
							e.printStackTrace();
						}
						System.out.println(Thread.currentThread().getName()
								+ "jingxin" + j + "第" + task + "任务");
					}
				}
			});
		}
		
		executorService.shutdown();
		System.out.println(executorService+"----");
		
		System.out.println("完毕");
		
		
	}
}
