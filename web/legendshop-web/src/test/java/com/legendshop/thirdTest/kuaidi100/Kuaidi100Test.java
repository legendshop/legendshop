package com.legendshop.thirdTest.kuaidi100;

import com.legendshop.base.util.ExpressMD5;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath*:spring/applicationContext.xml"})
public class Kuaidi100Test {

    /**
     * 手机号码错误次数太多，会造成锁单情况，注意。
     */
    @Test
    public void QueryTest(){
        String param = "{\"com\":\"shunfeng\",\"num\":\"245421654924\",\"phone\":\"13035892831\"}";
        String customer = "F1610C08E8D4A82D7B53C9F217C72E0F";
        String key = "FZlGeZwE692";
        String sign = ExpressMD5.encode(param + key + customer);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("param", param);
        params.put("sign", sign);
        params.put("customer", customer);
        String resp;
        try {
            resp = HttpRequest.postData("http://poll.kuaidi100.com/poll/query.do", params, "utf-8").toString();
            if(!resp.startsWith("{\"message\":\"ok\""))
                Assert.fail();
            System.out.println(resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test(){

	}
}
