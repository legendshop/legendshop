package com.legendshop.business.service.seckill;

import com.legendshop.LegendShopApplication;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.spi.service.SeckillService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LegendShopApplication.class)
public class SeckillServiceImplTest {

	@Autowired
	private SeckillService seckillService;
	
    @Test
    public void getOperateStatistics() {
		OperateStatisticsDTO operateStatistics = seckillService.getOperateStatistics(1L);
		System.out.println(operateStatistics);
	}
}