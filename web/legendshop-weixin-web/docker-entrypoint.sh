#!/bin/sh
set -e

#日志
legendshop_log() {
        local type="$1"; shift
        printf '%s [%s] [Entrypoint]: %s\n' "$(date --rfc-3339=seconds)" "$type" "$*"
}

#错误输出
legendshop_error() {
        legendshop_log ERROR "$@" >&2
        exit 1
}

#检查配置项
legendshop_check_config() {
   if [ -z "$MYSQL_CONFIG" -o -z "$REDIS_CONFIG" -o -z "$COMMON_CONFIG" -o -z "$WECHAT_CONFIG" -o -z "$JOB_CONFIG" ]; then
       legendshop_error $'项目并未初始化,您需要指定MYSQL_CONFIG,REDIS_CONFIG,COMMON_CONFIG,WECHAT_CONFIG,JOB_CONFIG'
   fi
}

#初始化参数
initParam=""
legednshop_init_param() {
   mysqlParam=""
   for mysql in $(echo $MYSQL_CONFIG);do
     mysqlParam="${mysqlParam} -D$mysql"
   done
   echo "mysql 参数为：${mysqlParam}"

   redisParam=""
   for redis in $(echo $REDIS_CONFIG);do
     redisParam="${redisParam} -D$redis"
   done
   echo "redis 参数为：${redisParam}"

   commonParam=""
   for common in $(echo $COMMON_CONFIG);do
     commonParam="${commonParam} -D$common"
   done
   echo "common 参数为：${commonParam}"

   wechatParam=""
   for wechat in $(echo $WECHAT_CONFIG);do
     wechatParam="${wechatParam} -D$wechat"
   done
   echo "wechat 参数为：${wechatParam}"

   jobParam=""
   for job in $(echo $JOB_CONFIG);do
     jobParam="${jobParam} -D$job"
   done
   echo "job 参数为：${jobParam}"

   memoryParam=""
   memoryParam=$(echo $MEMORY_PARAM)
   echo "memory 参数为：${memoryParam}"

   initParam="${mysqlParam}${redisParam}${commonParam}${wechatParam}${jobParam}"
}

#主方法
_main() {
        legendshop_check_config "$@"
        legednshop_init_param "$@"
}

_main "$@"

java ${memoryParam} -Djava.security.egd=file:/dev/./urandom ${initParam} -jar legendshop-weixin-web.jar
