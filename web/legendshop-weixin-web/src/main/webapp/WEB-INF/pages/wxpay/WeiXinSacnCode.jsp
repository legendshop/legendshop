<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%
 String appContext = request.getContextPath();
 String basePath = request.getScheme()+"://"+request.getServerName()+":"+ request.getServerPort() + appContext;
%>
<%
response.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT");   
// 设置 HTTP/1.1 no-cache 头   
response.setHeader("Cache-Control", "no-store,no-cache,must-revalidate");   
// 设置 IE 扩展 HTTP/1.1 no-cache headers， 用户自己添加   
response.addHeader("Cache-Control", "post-check=0, pre-check=0");   
// 设置标准 HTTP/1.0 no-cache header.   
response.setHeader("Pragma", "no-cache");
 %>
<!DOCTYPE>
<html>
<head>
    <meta charset="utf-8">
  <meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
   <meta name="author" content="qinuoli">
   <title>微信支付</title>
   <meta name="apple-mobile-web-app-capable" content="yes">
   <meta name="apple-mobile-web-app-status-bar-style" content="black">
   <meta name="format-detection" content="telephone=no">
   <script src="<c:url value="/resources/jquery.js"/>"></script>
   <script src="<c:url value="/resources/jquery.qrcode.min.js"/>"></script>
</head>
  <style>
    
.c-wechat-payment {
    margin-bottom: 50px;
    margin-top: 30px;
}
.wechat-payment-header {
    margin-bottom: 20px;
}
.wechat-payment-title {
    text-align: center;
}
.icon-wechat-payment {
    background-image: url("${contextPath}/resources/images/wechat-pay01.png");
    background-position: -59px -4px;
    width: 161px;
    height: 41px;
}
.u-icon {
    display: inline-block;
    vertical-align: middle;
    line-height: 999px;
    overflow: hidden;
}
.wechat-payment-body {
    padding-top: 60px;
    padding-bottom: 80px;
    background-color: #fff;
    border-top: 4px solid #e5004f;
}
.c-wechat-qr {
    position: relative;
    margin-right: auto;
    margin-left: auto;
    width: 260px;
    z-index: 1;
}
.is-wechat-qr-data .wechat-qr-loading, .is-wechat-qr-error .wechat-qr-loading {
    /*! display: none; */
}
.wechat-qr-error, .wechat-qr-loading {
    width: 258px;
    height: 258px;
    color: #333;
    text-align: center;
    border: 1px solid #c2c2c2;
}
.wechat-qr-loading .wechat-qr-inner {
    padding-top: 114px;
}
.wechart-qr-error-icon, .wechat-qr-loader {
    margin-right: 10px;
    vertical-align: top;
    line-height: 1;
}
.ii-loading-pink-32x32 {
    width: 32px;
    height: 32px;
    background-image: url("${contextPath}/resources/images/wechat-loading.gif");
}
.ii-loading, .ii-loading-gray-16x16, .ii-loading-gray-24x24, .ii-loading-gray-32x32, .ii-loading-pink-16x16, .ii-loading-pink-24x24, .ii-loading-pink-32x32 {
    display: inline-block;
    vertical-align: top;
    background-repeat: no-repeat;
    overflow: hidden;
}
.wechat-qr-error-text, .wechat-qr-loading-text {
    display: inline-block;
    font-size: 18px;
    line-height: 32px;
    vertical-align: top;
}
.is-wechat-qr-data .wechat-qr-img, .is-wechat-qr-error .wechat-qr-error {
    display: block;
}
.wechat-qr-img {
    display: none;
    width: 260px;
    height: 260px;
}
.wechat-qr-error {
    /*! display: none; */
}
.wechat-qr-error .wechat-qr-inner {
    padding-top: 95px;
}
.cod-def{
	  background-image: url("${contextPath}/resources/images/wechat-pay01.png");
    background-position: -313px -9px;
    width: 32px;
    height: 32px; 
	  display: inline-block;
	  \*display: inline;
	  zoom: 1;
	  margin-right: 5px;
	  vertical-align: middle;
}
.wechat-qr-operate {
    margin-top: 15px;
}
.wechat-qr-operate a {
    font-size: 14px;
	  color: #3f6de0;
    text-decoration: none;
}
.wechat-qr-phone-img {
    opacity: 0;
    visibility: hidden;
    transition: left .5s linear,visibility .5s linear,opacity .5s linear;
    width: 193px;
    height: 260px;
    position: absolute;
    left: 67px;
    top: 0;
    background: url("${contextPath}/resources/images/pic-guide-wechat-scan.png") no-repeat 100% 0;
}
.wechat-qr-explain {
    margin: 25px auto 0;
    width: 260px;
}
.c-scan-tips {
    padding: 18px 22px;
    width: 214px;
    border: 1px solid #e5004f;
    background-color: #fff;
    border-radius: 4px;
    overflow: hidden;
    zoom: 1;
    font-family: "微软雅黑";
}
.scan-tips-body-default {
    color: #e5004f;
}
.scan-tips-icon {
    float: left;
    margin-right: 10px;
}
.icon-qr-scan {
    background-image: url("${contextPath}/resources/images/wechat-pay01.png");
    background-position: 0px 0px;
    width: 49px;
    height: 49px;
}
.scan-tips-text {
    padding-top: 1px;
    font-size: 16px;
    white-space: nowrap;
    vertical-align: top;
    overflow: hidden;
    zoom: 1;
}
.scan-tips-body-success {
    /*display: none;*/
    color: #666;
}
.icon-success-large {
    background-image: url("${contextPath}/resources/images/wechat-pay01.png");
    background-position: -229px -2px;
    width: 48px;
    height: 48px;
}
.wechat-order-info {
    font-family:"微软雅黑";
    padding-bottom: 12px;
}
.wechat-notice, .wechat-order-info {
    margin: 8px auto 0;
    width: 578px;
    text-align: center;
}
.wechat-order-total {
    font-size: 22px;
}
.wechat-order-total .m-price {
    font-size: 50px;
}
.u-highlight {
    color: #e5004f !important;
}
.wechat-order-total .m-price .u-yen {
    margin-right: 8px;
}
.u-yen {
    padding: 0 2px;
    font-family: Arial !important;
}
.u-price {
    font-family: Arial !important;
}
.wechat-notice {
    padding-top: 11px;
    border-top: 1px solid #e6e5e5;
}
.wechat-notice-text {
  font-family:"微软雅黑";
    *zoom: 1;
    width: 165px;
    margin: 0 auto;
    text-align: left;
}
.wechat-notice-text::after, .wechat-notice-text::before {
    display: table;
    content: '';
}
.wechat-notice-text .wechat-tips-text {
    float: left;
}
.wechat-notice-text .c-trigger-tooltips {
    float: left;
    margin-top: 18px;
    position: relative;
    margin-left: 5px;
}
.if-query {
    background-image: url("${contextPath}/resources/images/wechat-pay01.png");
    background-position: -287px -17px;
    width: 16px;
    height: 17px; 
    display: inline-block;
    *display: inline;
    zoom: 1;
    vertical-align: middle;
}
.wechat-tips-tooltips {
    width: 259px;
    left: -56px;
    top: 27px;
}
.ui-tooltips {
    visibility: hidden;
    opacity: 0;
    -webkit-transition: -webkit-transform ease-out .15s,opacity ease-out .15s;
    transition: transform ease-out .15s,opacity ease-out .15s;
}
.ui-tooltips-top-arrow, .ui-tooltips-top-left-arrow, .ui-tooltips-top-right-arrow {
    -webkit-transform: translateY(10px);
    -ms-transform: translateY(10px);
    transform: translateY(10px);
}
.ui-tips-pop, .ui-tooltips {
    position: absolute;
    max-width: 800px;
    border: 1px solid #dbdada;
    border-radius: 2px;
    box-shadow: 0 0 3px rgba(0,0,0,.1);
    font: 12px/18px tahoma,arial,Hiragino Sans GB,WenQuanYi Micro Hei,'\5FAE\8F6F\96C5\9ED1','\5B8B\4F53',sans-serif;
    color: #4d4d4d;
    z-index: 50;
}
.ui-tooltips-top-arrow .ui-tooltips-arrow, .ui-tooltips-top-left-arrow .ui-tooltips-arrow, .ui-tooltips-top-right-arrow .ui-tooltips-arrow {
    left: 0;
    top: -11px;
    width: 100%;
    height: 10px;
}
.ui-tooltips-arrow {
    position: absolute;
}
.ui-tooltips-top-left-arrow .arrow, .ui-tooltips-top-left-arrow .arrow-out {
    left: 25%;
}
.ui-tooltips-arrow .arrow-out {
    color: #dbdada;
    text-shadow: 0 0 4px rgba(0,0,0,.15);
}
.ui-tooltips-top-arrow .arrow-out, .ui-tooltips-top-left-arrow .arrow-out, .ui-tooltips-top-right-arrow .arrow-out {
    margin-left: -7px;
    bottom: -10px;
}
.ui-tooltips-top-arrow .arrow, .ui-tooltips-top-left-arrow .arrow, .ui-tooltips-top-right-arrow .arrow {
    margin-left: -7px;
    bottom: -11px;
}
.ui-tips-pop .arrow, .ui-tooltips .arrow {
    position: absolute;
    width: 14px;
    text-align: left;
    height: 14px;
    *overflow: hidden;
    font: 400 12px/14px 宋体,Hiragino Sans GB;
    _line-height: 16px;
    color: #fff;
}
.ui-tooltips-content {
    position: relative;
    background-color: #fff;
    padding: 6px 15px;
}
.ui-tooltips-msg {
    padding-left: 26px;
}
.ui-tooltips-content a{
	color: #3f6de0;
}
.wechat-notice-text::after {
    clear: both;
}
/*微信支付*/

.z-ui-tooltips-in, .z-ui-tooltips-in .ui-tooltips {
    visibility: visible;
    -webkit-transform: translate(0,0);
    -ms-transform: translate(0,0);
    transform: translate(0,0);
    opacity: 1;
    -webkit-transition-duration: .25s;
    transition-duration: .25s;
}

</style>
</head>


<body>
<div id="doc">
   <div id="bd">
      
     
       <div class="yt-wrap" style="padding:0px 0 20px;">
            
          <div class="c-wechat-payment J_wechat_payment">
                <div class="wechat-payment-header">
                    <div class="wechat-payment-title">
                        <div class="u-icon  icon-wechat-payment"></div>
                    </div>
                </div>
                <div class="wechat-payment-body">
                    <div class="c-wechat-qr  J_wechat_qr is-wechat-qr-data" data-hover="c-wechat-qr-hover" data-touch="z-touch">
                        <div class="wechat-qr-loading">
                            <div class="wechat-qr-inner">
                                <span class="ii-loading-pink-32x32 wechat-qr-loader"></span><span class="wechat-qr-loading-text">生成二维码中...</span>
                            </div>
                        </div>
                        <div class="wechat-qr-img J_wechat_qr_img" style="display: none;">
                        </div>
                        <!-- <div class="wechat-qr-phone-img" id="J_wechat_phone_img"></div> -->
                    </div>

                    <div class="wechat-qr-explain">
                        <div class="c-scan-tips J_scan_tips">
                            <div class="scan-tips-body  scan-tips-body-default">
                                <div class="u-icon  icon-qr-scan  scan-tips-icon"></div>
                                <p class="scan-tips-text">请使用微信“扫一扫”<br>扫描二维码支付</p>
                            </div>
                            <div class="scan-tips-body  scan-tips-body-success" style="display: none;">
                                <div class="u-icon  icon-success-large  scan-tips-icon"></div>
                                <p class="scan-tips-text">扫码成功！<br>请在微信上完成支付</p>
                            </div>
                        </div>
                    </div>

                    <div class="wechat-order-info">
                        <span class="wechat-order-total">
                            <span class="m-price  u-highlight">
                                <span class="u-yen">¥</span><span class="u-price">${totalFee}元</span>
                            </span>
                        </span>
                    </div>

                    <div class="wechat-notice">
                        <div class="wechat-notice-text" data-toggle="tooltip">
                            <p class="wechat-tips-text">完成支付没有提示？</p>
                            <div class="c-trigger-tooltips">
                                <span class="if-query"></span>
                                <div class="ui-tooltips  ui-tooltips-top-left-arrow wechat-tips-tooltips" id="">
                                    <div class="ui-tooltips-arrow">
                                        <span class="arrow arrow-out">◆</span>
                                        <span class="arrow">◆</span>
                                    </div>
                                    <div class="ui-tooltips-content">
                                        <p class="ui-tooltips-msg"></p><p class="wechat-text-title">温馨提示：</p>如您已完成微信支付，但页面无任何提示，请至
                                        <a href="${show_url}" target="_blank"  rel="nofollow">订单管理</a>
                                                                                                                        查看支付结果。<p></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- / .wechat-payment-body -->
            </div>


        </div>
   </div><!--bd end-->

</div>

</div>
</body>
 <script>
	var contextPath="${contextPath}";
	var token="${token}";
	var secret="${secret}";
	var validateTime="${validateTime}";
	var totalFee="${totalFee}";
	var outTradeNo="${outTradeNo}";
	var userId="${userId}";
	var openId="${openId}";
	var subject="${subject}";
	var SEND_URL="${show_url}";
	var url = "${code_url}";
	
	$(document).ready(function () {
	   $(".wechat-qr-loading").show();
	   $(".J_wechat_qr_img").hide();
	   $(".J_wechat_qr_img").qrcode({
			render: "table",
			width: 260,
			height:260,
			text: url
	    });
	   $(".wechat-qr-loading").hide();
	   $(".J_wechat_qr_img").show();
	    
	   $(".c-trigger-tooltips").mouseover(function(){
          $(".ui-tooltips").addClass("z-ui-tooltips-in");
       }).mouseout(function(){
           $(".ui-tooltips").removeClass("z-ui-tooltips-in");
       }); 
    
        setInterval("ajaxstatus()", 5000);    
    });
        
    function ajaxstatus() {
        if(userId!="" && token!=""){
              var data={
	              "token":token,
	              "secret":secret,
	              "validateTime":validateTime,
	              "outTradeNo":outTradeNo,
	              "userId":userId,
	              "openId":openId,
	              "subject":subject,
	              "showUrl":SEND_URL
              };
              $.ajax({
                    url:contextPath+"/wxpay/weixinJSBridge/GetOrderStatus",
                    type: "POST",
                    dataType:"json",
                    data: data,
                    success: function (data) {
                        if (data =="OK") { //订单状态为1表示支付成功
                           $(".scan-tips-body-default").hide();
                           $(".scan-tips-body-success").show();
                           setTimeout(function(){
                              window.location.href = SEND_URL; //页面跳转
                           }
                           ,5000);
                        }else{
                           console.log(data);
                        }
                    },
                    error: function () {
                    }
             });
        }
     }  
 </script>
 
</html>