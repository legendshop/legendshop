<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%
    String appContext = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+ request.getServerPort() + appContext;
%>
<%
    response.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT");
// 设置 HTTP/1.1 no-cache 头   
    response.setHeader("Cache-Control", "no-store,no-cache,must-revalidate");
// 设置 IE 扩展 HTTP/1.1 no-cache headers， 用户自己添加   
    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
// 设置标准 HTTP/1.0 no-cache header.
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-Control","no-cache");
    response.setDateHeader("Expires", -10);
%>
<!DOCTYPE>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="author" content="qinuoli">
    <title>微信支付</title>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <script src="<c:url value="/resources/jquery.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/resources/weui/weui.min.css"/>" />
</head>
<style type="text/css">
    body {
        padding: 0px;
        margin: 0px auto;
    }

    body {
        background-color: #EEE;
    }

    .i-assets-container {
        border: 1px solid #E1E1E1;
        vertical-align: top;
        display: block;
        background-color: #FFF;
        margin: 5px 10px;
        color: #A1A1A1 !important;
    }

    #container.ui-container {
        color: #666;
    }

    .i-assets-content {
        margin: 10px 10px 10px 20px;
    }

    .fn-clear:after {
        visibility: hidden;
        display: block;
        font-size: 0px;
        content: " ";
        clear: both;
        height: 0px;
    }

    .i-assets-header h3 {
        font-size: 14px;
    }

    .fn-left {
        display: inline;
        float: left;
    }

    h3 {
        margin: 0px;
        padding: 0px;
        font: 12px/1.5 tahoma, arial, "Hiragino Sans GB", "Microsoft Yahei",
        "宋体";
    }

    .i-assets-balance-amount {
        line-height: 24px;
        margin-right: 20px;
    }

    .amount {
        font-size: 24px;
        font-weight: 400;
        color: #666;
        margin-left: 25px;
    }

    .amount .fen {
        font-size: 18px;
    }

    #wx_bottom {
        display: flex;
    }

    #wx_bottom {
        overflow: hidden;
        margin: 15px 0px;
    }

    #wx_bottom {
        display: box;
        display: -ms-box;
        display: -webkit-box;
        display: flex;
        display: -ms-flexbox;
        display: -webkit-flex;
    }

    #wx_bottom  .a {
        width: 100%;
        height: 40px;
        line-height: 40px;
        margin: 0px 10px;
        border: 1px solid #DDD;
        text-align: center;
        border-radius: 3px;
        color: #666;
        background: #fff;
        text-decoration: none;
    }

    .WX_search {
        background-color: #EFEFEF;
        height: 40px;
        line-height: 40px;
        position: relative;
        border-bottom: 1px solid #DDD;
        text-align: center;
    }

    .pay_buttom {
        margin: 15px 0px;
        width: 100%;
        display: box;
        display: -ms-box;
        display: -webkit-box;
        display: flex;
        display: -ms-flexbox;
        display: -webkit-flex;
        width: 100%;
    }

    .pay_buttom a {
        height: 40px;
        line-height: 40px;
        margin: 0px 10px;
        border: 1px solid #DDD;
        text-align: center;
        border-radius: 3px;
        color: #666;
        background: #fff;
        text-decoration: none;
        width: 100%;
        display: block;
        flex: 1;
        -ms-flex: 1;
        -webkit-flex: 1;
        box-flex: 1;
        -ms-box-flex: 1;
        -webkit-box-flex: 1;
    }
    /* 弹窗 */
    .pop-up {
        position: fixed;
        bottom: 0;
        width: 100%;
        height: 100%;
        background: rgba(0,0,0,0.4);
        z-index: 200;
        display: none;
    }
    .loader {
        width: 30%;
        height: 200px;
        margin-bottom: 20px;
        position: relative;
        box-sizing: border-box;
        display: flex;
        align-items: center;
        justify-content: center;
        top: 35%;
        left: 35%;
    }
    @-webkit-keyframes loading-3{
        50%{ transform:scale(0.4); opacity:.4;}
        100%{ transform:scale(1); opacity:1;}
    }
    .loading-3 { position:relative;}
    .loading-3 i{ display:block; width:15px;height:15px;border-radius:50%; background-color:#fff; position:absolute;}
    .loading-3 i:nth-child(1){top:25px;left:0;-webkit-animation:loading-3 1s ease 0s infinite;}
    .loading-3 i:nth-child(2){top:17px;left:17px;-webkit-animation:loading-3 1s ease -0.12s infinite;}
    .loading-3 i:nth-child(3){top:0px;left:25px;-webkit-animation:loading-3 1s ease -0.24s infinite;}
    .loading-3 i:nth-child(4){top:-17px;left:17px;-webkit-animation:loading-3 1s ease -0.36s infinite;}
    .loading-3 i:nth-child(5){top:-25px;left:0;-webkit-animation:loading-3 1s ease -0.48s infinite;}
    .loading-3 i:nth-child(6){top:-17px;left:-17px;-webkit-animation:loading-3 1s ease -0.6s infinite;}
    .loading-3 i:nth-child(7){top:0px;left:-25px;-webkit-animation:loading-3 1s ease -0.72s infinite;}
    .loading-3 i:nth-child(8){top:17px;left:-17px;-webkit-animation:loading-3 1s ease -0.84s infinite;}
</style>
<body>

<div class="WX_search">
    <p>订单支付信息确认</p>
</div>
<div class="i-assets-container ui-bookblock-item">
    <div class="i-assets-content">
        <div class="i-assets-header fn-clear">
            <h3 class="fn-left">订单号</h3>
        </div>
        <div class="i-assets-body fn-clear">
            <div class="i-assets-balance-amount fn-left">
                <strong class="amount"><span
                        style="font-size: 15px;">${outTradeNo}</span></strong>
            </div>
        </div>
    </div>
    <div class="i-assets-content">
        <div class="i-assets-header fn-clear">
            <h3 class="fn-left">商品名称</h3>
        </div>
        <div class="i-assets-body fn-clear">
            <div class="i-assets-balance-amount fn-left">
                <strong class="amount"><span
                        style="font-size: 15px;">${subject}</span></strong>
            </div>
        </div>
    </div>
</div>

<div class="i-assets-container ui-bookblock-item">
    <div class="i-assets-content">
        <div class="i-assets-header fn-clear">
            <h3 class="fn-left">您需要支付金额</h3>
        </div>
        <div class="i-assets-body fn-clear">
            <div class="i-assets-balance-amount fn-left"
                 style="color: #F37800;">
                <strong class="amount" style="color: #F37800;">${totalFee}</strong>元
            </div>
        </div>
    </div>
</div>
<div class='pay_buttom' >
    <a href="#" id="pay" style="background: #06C; color: #fff;" onclick="dopay();">确认支付</a>
    <a href="#" id="cancel_apy" style="background: #cccccc; color: #fff;display: none;" >确认支付</a>
</div>

<div class="pop-up" id="loader">
    <div class="loader">
        <div class="loading-3">
            <i></i>
            <i></i>
            <i></i>
            <i></i>
            <i></i>
            <i></i>
            <i></i>
            <i></i>
        </div>
    </div>
</div>
<script src="<c:url value="/resources/weui/weui.min.js"/>"></script>
<%--    <script type="text/javascript" src="${contextPath}/resources/weui/weui.min.js"></script>--%>
<script type="text/javascript">

    var payObj=JSON.parse('${payment_result}');
    var returnUrl="${show_url}";

    function dopay() {
        if(!isWeiXin()){
            alert("请使用微信浏览器的函数");
            return;
        }
        if (typeof WeixinJSBridge == "undefined"){
            console.log(" WeixinJSBridge undefined ");
            if( document.addEventListener ){
                document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
            }else if (document.attachEvent){
                document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
                document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
            }
        }else{
            onBridgeReady();
        }
    }

    //判断是否是微信浏览器的函数
    function isWeiXin(){
        //window.navigator.userAgent属性包含了浏览器类型、版本、操作系统类型、浏览器引擎类型等信息，这个属性可以用来判断浏览器类型
        var ua = window.navigator.userAgent.toLowerCase();
        //通过正则表达式匹配ua中是否含有MicroMessenger字符串
        if(ua.match(/MicroMessenger/i) == 'micromessenger'){
            return true;
        }else{
            return false;
        }
    }


    function onBridgeReady(){
        $(".pop-up").show();

        WeixinJSBridge.invoke('getBrandWCPayRequest',{
            "appId" : payObj.appId, //公众号名称，由商户传入
            "timeStamp" : payObj.timeStamp, //时间戳，自 1970 年以来的秒数
            "nonceStr" : payObj.nonceStr, //随机串
            "package" : payObj.packageValue, //商品包信息
            "signType" : payObj.signType, //微信签名方式:
            "paySign" : payObj.paySign //微信签名
        },function(res) {
            //对于支付结果，res对象的err_msg值主要有3种，含义如下：(当然，err_msg的值不只这3种)
            //1、get_brand_wcpay_request:ok   //支付成功后，微信服务器返回的值
            //2、get_brand_wcpay_request:cancel   //用户手动关闭支付控件，取消支付，微信服务器返回的值
            //3、get_brand_wcpay_request:fail   //支付失败，微信服务器返回的值
            //-可以根据返回的值，来判断支付的结果。
            // -注意：res对象的err_msg属性名称，是有下划线的，与chooseWXPay支付里面的errMsg是不一样的。而且，值也是不同的。
            if (res.err_msg == 'get_brand_wcpay_request:ok') {
                alert("支付成功！");
                //支付成功,通知商户地址
                window.location.href = returnUrl;
            } else if (res.err_msg == "get_brand_wcpay_request:cancel") {
                $(".pop-up").hide();
                alert("支付取消！");
                window.location.href = returnUrl;
            } else {
                $(".pop-up").hide();
                alert(JSON.stringify(payObj));

                var jsonStr = JSON.stringify(res);
                alert(jsonStr);
                floatNotify.simple("订单支付失败, 请重试或联系商城客服!");
                window.location.href = returnUrl;
            }
        });
    }

    /**
     * 弹出支付提示框
     */
    function returnPayDialog(){
        weui.dialog({
            title: '支付提示',
            content: '请确认您的支付是否已完成？',
            buttons: [
                //     {
                //     label: '未完成，重新支付',
                //     type: 'default',
                //     onClick: function () {
                //         //confirmPay();
                //         return true;
                //     }
                // },
                {
                    label: '已完成支付',
                    type: 'primary',
                    onClick: function () {
                        // finishPay();
                        window.location.href = returnUrl;
                        return true;
                    }
                }]
        });
    }

    $(function () {
        if(payObj.appId == ""){
            returnPayDialog();
        }
    });

</script>

</body>
</html>