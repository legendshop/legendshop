<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<style>
<!--
   
body {
    background-color: #e1e0de;
}
body, h1, h2, h3, h4 {
    margin: 0;
}
body {
    font-family: "Helvetica Neue",Helvetica,"Microsoft YaHei",Arial,Tahoma,sans-serif;
    line-height: 1.6;
}

.page_msg {
    font-size: 16px;
    padding-left: 23px;
    padding-right: 23px;
    text-align: center;
}
.page_msg .inner {
    padding-bottom: 40px;
    padding-top: 40px;
}

.page_msg .msg_icon_wrp {
    display: block;
    padding-bottom: 22px;
}

.icon80_smile {
    background: transparent url("https://res.wx.qq.com/connect/zh_CN/htmledition/images/icon80_smile181c98.png") no-repeat scroll 0 0;
    display: inline-block;
    height: 80px;
    vertical-align: middle;
    width: 80px;
}
.page_msg {
    font-size: 16px;
    text-align: center;
}

.page_msg .msg_content h4 {
    color: #000000;
    font-weight: 400;
}
-->
</style>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    </head>
    <body>
       <div class="page_msg"><div class="inner"><span class="msg_icon_wrp"><i class="icon80_smile"></i></span><div class="msg_content"><h4>请在微信客户端打开链接</h4></div></div></div>
    </body>
</html>
