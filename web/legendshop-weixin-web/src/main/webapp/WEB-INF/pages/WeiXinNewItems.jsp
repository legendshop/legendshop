<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%
 String appContext = request.getContextPath();
 String basePath = request.getScheme()+"://"+request.getServerName()+":"+ request.getServerPort() + appContext;
%>
<!DOCTYPE html>
<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
            ${newsitem.title}
        </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="format-detection" content="telephone=no">
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/weixin/wx-article/page_mp_article_improve2318b8.css"/>">
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/weixin/wx-article/page_mp_article_improve_combo231ee1.css"/>">
        <link rel="stylesheet" type="text/css" href="">
        <style>
		.list-paddingleft-2 {
			padding-left: 30px;
		}
		blockquote {
			margin: 0;
			padding-left: 10px;
			border-left: 3px solid #DBDBDB;
		}
		</style>
    </head>
    
    <body id="activity-detail" class="zh_CN ">
	<div id="js_article"  class="rich_media">
		<div id="js_top_ad_area" class="top_banner"></div>
		<div class="rich_media_inner">
			<h2 class="rich_media_title" id="activity-name">
				${newsitem.title}</h2>
			<div class="rich_media_meta_list">
				<em id="post-date" class="rich_media_meta rich_media_meta_text"><fmt:formatDate value='${newsitem.createDate}' type="date" pattern="yyyy-MM-dd"/></em>
				<em class="rich_media_meta rich_media_meta_text">${newsitem.author}</em>
				<!-- <a id="post-user" href="javascript:void(0);" class="rich_media_meta rich_media_meta_link rich_media_meta_nickname">LegendShop电商平台</a> -->
			</div>
			<div id="page-content">
				<div id="img-content">
				    
				    <c:if test="${not empty newsitem.imagepathContent && newsitem.imagepathContent==1}">
					    <div class="rich_media_thumb_wrp" id="media">
					          <img src="${newsitem.imagepath}" onerror="this.parentNode.removeChild(this)" id="js_cover" class="rich_media_thumb">
					    </div>
				    </c:if>
					<div class="rich_media_content" id="js_content">
					    ${newsitem.content}
					</div>
					<!-- <div class="rich_media_tool" id="js_toobar">
						<a class="media_tool_meta meta_primary" id="js_view_source" href="">阅读原文</a>
						<div id="js_read_area" class="media_tool_meta link_primary meta_primary" style="display: ;"> 阅读 <span id="readNum">1</span></div>
					</div> -->
				</div>
				<div id="js_bottom_ad_area"></div>
				<div id="js_iframetest" style="display: none;"></div>
			</div>
			<!-- 
			<div id="js_pc_qr_code" class="qr_code_pc_outer"
				style="display: none;">
				<div class="qr_code_pc_inner">
					<div class="qr_code_pc">
						<img id="js_pc_qr_code_img" class="qr_code_pc_img">
						<p>
							微信扫一扫<br>获得更多内容
						</p>
					</div>
				</div>
			</div> -->
		</div>
	</div>
	<script type="text/javascript">
	var accountId = "8a792db34dc73687014ddc43819f5029";
	var newsId = "8a792db34f2b3675014f2b5763940083";
	var openid = "";
     var imageUrl = "upload/weixin/images/20150814163309qHbjhTGL.jpg";
     var newsUrl = "http://www.jeewx.com/jeewx/newsItemController.do?goContent&amp;id=8a792db34f2b3675014f2b5763940083&amp;accountid=8a792db34dc73687014ddc43819f5029";
     var title = "测试图文";
     var description = "测试测试测试测试测试测试";
     var content = "测试";
     
     window.shareData = {  
        "imgUrl": imageUrl, 
        "timeLineLink": newsUrl,
        "sendFriendLink": newsUrl,
        "weiboLink": newsUrl,
        "tTitle": title,
        "tDescription":description,
        "tContent": content
    };
    
	document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
	
	        WeixinJSBridge.on('menu:share:appmessage', function (argv) {
 //发送给朋友  
	            WeixinJSBridge.invoke('sendAppMessage', { 
	                "img_url": window.shareData.imgUrl,
	                "img_width": "640",
	                "img_height": "640",
	                "link": window.shareData.sendFriendLink,
	                "desc": window.shareData.tDescription,
	                "title": window.shareData.tTitle
	            }, function (res) {

	            	if(res.err_msg=="send_app_msg:ok"){
	            		//分享统计
	            		sendMessage("share");
	            	}else if(res.err_msg=="send_app_msg:confirm"){
	            		//分享统计
						sendMessage("share");
					}
	                _report('send_msg', res.err_msg);
	            })
	        });
  //发送到朋友圈  
	        WeixinJSBridge.on('menu:share:timeline', function (argv) {
	            WeixinJSBridge.invoke('shareTimeline', {
	                "img_url": window.shareData.imgUrl,
	                "img_width": "640",
	                "img_height": "640",
	                "link": window.shareData.sendFriendLink,
	                "desc": window.shareData.tDescription,
	                "title": window.shareData.tTitle
	            }, function (res) {
	            	if(res.err_msg=="share_timeline:ok"){
	            		//分享统计
	            		sendMessage("share"); 
	            	}
	                _report('timeline', res.err_msg);
	            });
	        });

          //分享到微博  
	        WeixinJSBridge.on('menu:share:weibo', function (argv) {
	            WeixinJSBridge.invoke('shareWeibo', {
	                "content": window.shareData.tContent,
	                "url": window.shareData.sendFriendLink,
	            }, function (res) {
	            	if(res.err_msg=="share_weibo:ok"){
	            		//分享统计
	            		sendMessage("share");  
	            	}
	                _report('weibo', res.err_msg);
	            });
	        });
        }, false);
       
     function sendMessage(symbol){
     
        return;
    	var accountId = "8a792db34dc73687014ddc43819f5029";
        var newsId = "8a792db34f2b3675014f2b5763940083";
        var destUrl = "shareTotalController.do?save&amp;accountid="+accountId+"&amp;newsId="+newsId+"&amp;symbol="+symbol;
        $.ajax({
        	url:destUrl,
        	method:"GET",
        	dataType:"JSON",
        	success:function(){}
        });
    }
    function doPraise(){
        var url = "newsItemController.do?doPraise&amp;messageid="+newsId+"&amp;accountid="+accountId+"&amp;openid="+openid;
        $.ajax({
        	url:url,
        	method:"GET",
        	dataType:"JSON",
        	success:function(data){
        		if(data.success){
        	  		//改变点赞数量
            		var oc = $("#praisecount").val();
            		var newc = parseInt(oc)+1;
            		$("#praisecount").val(newc);
            		$("#likeNum").html(newc);
        		}else{
        			alert(data.msg);
        		}
        	}
        });
    }
    function showComplain(){
        var url = "newsItemController.do?goComplain&amp;messageid="+newsId+"&amp;accountid="+accountId+"&amp;complainOpenid="+openid;
    	location.href=url;
    }
    //阅读统计
	sendMessage("read");   
</script>
<!-- 微信分享功能 -->
<script src="<c:url value="/resources/jquery.js"/>"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script src="<c:url value="/resources/weixin/share/weixinshare.js"/>"></script>
<script type="text/javascript">
   WeiXinShareApi.share("测试图文","测试测试测试测试测试测试","upload/weixin/images/20150814163309qHbjhTGL.jpg");
</script>
<!-- 微信分享功能 -->

</body>

</html>