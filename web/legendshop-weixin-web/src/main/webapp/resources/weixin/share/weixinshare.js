WeiXinShareApi = {};
WeiXinShareApi.ShareData = {};
WeiXinShareApi.share = function(title,desc,imgUrl){
	WeiXinShareApi.ShareData.title = title;
	WeiXinShareApi.ShareData.desc = desc;
	WeiXinShareApi.ShareData.imgUrl = imgUrl;
	WeiXinShareApi.ShareData.shareTimelineCallback = function(){};
	WeiXinShareApi.ShareData.shareAppMessageCallback = function(){};
	WeiXinShareApi.ShareData.shareQQCallback = function(){};
	WeiXinShareApi.ShareData.shareWeiboCallback = function(){};
};
WeiXinShareApi.buildShareUrl = function (url,youshareId){
	var purl = url.substring(url.indexOf("?")+1,url.length)+"&";
	var accountid = parseParam(purl,'accountid');
	if(!accountid){
		return null;
	}
	var openid = parseParam(purl,'openid');
	url = url.replace("&openid="+openid,'');
	//防止openId串号,兼容系统的两种写法
	var openId = parseParam(purl,'openId');
	url = url.replace("&openId="+openId,'');
	//防止openId串号,兼容系统的两种写法
	var shareId = parseParam(purl,'shareId');
	url = url.replace("&shareId="+shareId,'');
	url = url+"&shareId="+youshareId;
	var domain = url.substring(0,url.lastIndexOf("/")+1);
	url = url.replace(new RegExp('&','g'),'@');
	url = url.replace(new RegExp('=','g'),'---');
	url = domain + 'sharePageOauth2Controller.do?sharePage&shareurl=' + url;
	return url;
};

$(document).ready(function(){

	//TODO jsapiTicket


	return;

	var url = window.location.href;
	var accountid = parseParam(url,'accountid');
	$.ajax({
		url : 'newsItemController.do?jsapiTicket',
		type : 'post',
		data : {
			url : url,
			accountid:accountid
		},
		cache : false,
		success : function(data) {
			var d = $.parseJSON(data);
			if (d.appId) {
				doWeixinShare(d.appId,d.timestamp,d.nonceStr,d.signature);
			}
		}
	});
	//var     url="http://localhost/newsItemController.do?goContent4&id=9c9a9d984d5af00b014d5af2b6af0008&sharetype=tuwen&openid=oGCDRjvr9L1NoqxbyXLReCVYVyV0&shareId=1432136840042";
	//successShareCallback(url,'测试标题',(new Date()).getTime());
	//url = 'http://localhost/newsItemController.do?goContent4&id=9c9a9d984d5af00b014d5af2b6af0008&sharetype=tuwen&openid=oGCDRjvr9L1NoqxbyXLReCVYVyV0&accountid=9090&shareId=';

	readShareRecord(url);
	//var buildShareId = (new Date()).getTime();
	//successShareCallback(url,'titlesdfjkdfj',buildShareId);
	//var url = WeiXinShareApi.buildShareUrl(url,buildShareId); //清空自身的openid/别人分享给自己的shareId和加入自己分享给别人的sha
});

function doWeixinShare(appId,timestamp,nonceStr,signature){
	wx.config({
		//开发模式把下面注释去掉
		debug: false,
		appId: appId,
		timestamp: timestamp,
		nonceStr: nonceStr,
		signature: signature,
		jsApiList: [
			'checkJsApi',
			'onMenuShareTimeline',
			'onMenuShareAppMessage',
			'onMenuShareQQ',
			'onMenuShareWeibo'
		]
	});
	wx.ready(function () {
		shareFriend();
	});
}

function shareFriend() {
	var title =  WeiXinShareApi.ShareData.title || $('title').text();
	var desc = WeiXinShareApi.ShareData.desc || $("meta[name=description]").attr("content");
	var imgUrl = WeiXinShareApi.ShareData.imgUrl;
	var buildShareId = (new Date()).getTime();
	var selfurl = window.location.href;
	var url = WeiXinShareApi.buildShareUrl(window.location.href,buildShareId); //清空自身的openid/别人分享给自己的shareId和加入自己分享给别人的shareId
	//1.0  分享到朋友
	wx.onMenuShareAppMessage({
		title: title,
		desc: desc,
		link: url,
		imgUrl: imgUrl,
		success: function () { // 用户确认分享后执行的回调函数
			successShareCallback(selfurl,title,buildShareId);
			WeiXinShareApi.ShareData.shareAppMessageCallback && WeiXinShareApi.ShareData.shareAppMessageCallback();
		},
		cancel: function () { // 用户取消分享后执行的回调函数

		}
	});
	//2.0 分享到朋友圈
	wx.onMenuShareTimeline({
		title: title, // 分享标题
		link: url, // 分享链接
		imgUrl: imgUrl, // 分享图标
		success: function () { // 用户确认分享后执行的回调函数
			successShareCallback(selfurl,title,buildShareId);
			WeiXinShareApi.ShareData.shareTimelineCallback && WeiXinShareApi.ShareData.shareTimelineCallback();
		},
		cancel: function () { // 用户取消分享后执行的回调函数

		}
	});
	//3.0 分享到QQ空间
	wx.onMenuShareQQ({
		title: title, // 分享标题
		desc: desc, // 分享描述
		link: url, // 分享链接
		imgUrl: imgUrl,
		success: function () { // 用户确认分享后执行的回调函数
			successShareCallback(selfurl,title,buildShareId);
			WeiXinShareApi.ShareData.shareQQCallback && WeiXinShareApi.ShareData.shareQQCallback();
		},
		cancel: function () { // 用户取消分享后执行的回调函数

		}
	});
	//4.0 分享到微博
	wx.onMenuShareWeibo({
		title: title, // 分享标题
		desc: desc, // 分享描述
		link: url, // 分享链接
		imgUrl: imgUrl,
		success: function () { // 用户确认分享后执行的回调函数
			successShareCallback(selfurl,title,buildShareId);
			WeiXinShareApi.ShareData.shareWeiboCallback && WeiXinShareApi.ShareData.shareWeiboCallback();
		},
		cancel: function () { // 用户取消分享后执行的回调函数

		}
	});
};
// 自己的openid和自己分享给别人的shareId
function successShareCallback(url,title,myShareId){
	var purl = url.substring(url.indexOf("?")+1,url.length)+"&";
	var sharetype = parseParam(purl,'sharetype');
	var shareId = parseParam(purl,'shareId');
	var openid = parseParam(purl,'openid');
	var id = parseParam(purl,'id');
	var accountid = parseParam(purl,'accountid');
	//tmpurl = purl.substring(purl.indexOf("shareId=")+8);
	//var shareId = tmpurl.substring(0,tmpurl.indexOf("&"));
	$.ajax({
		url : 'weixinShareRecordController.do?doAdd',
		type : 'post',
		data : {
			fromshareid : shareId,
			sharetype : sharetype,
			openid:openid,
			articleid:id,
			articletitle:title,
			id:myShareId,
			accountId:accountid
		},
		cache : false,
		success : function(data) {
		}
	});
}

// 自己的openid和别人分享的shareId
function readShareRecord(url){
	var purl = url.substring(url.indexOf("?")+1,url.length)+"&";
	var tmpurl = "";
	var shareId = parseParam(purl,'shareId');
	var openid = parseParam(purl,'openid');
	var id = parseParam(purl,'id');
	var accountid = parseParam(purl,'accountid');
	/**
	 * 微信分享阅读统计
	 *  $.ajax({
			url : 'weixinShareReadrecordController.do?doAdd',
			type : 'post',
			data : {
				sharerecordid:shareId,
				readeropenid:openid,
				articleid:id,
				accountId:accountid
			},
			cache : false,
			success : function(data) {
			}
		});
	 */

}
function parseParam(purl,paraname){
	purl = purl+'&';
	var tmpurl = "";
	var v = "";
	paraname = "&"+paraname+"=";
	if(purl.indexOf(paraname) > -1){
		tmpurl = purl.substring(purl.indexOf(paraname)+paraname.length);
		v = tmpurl.substring(0,tmpurl.indexOf("&"));
	}
	return v;
} 