package com.legendshop.business.util;

import com.legendshop.util.EnvironmentConfig;
import com.legendshop.util.SystemUtil;

/*
 * 文件后缀
 */
public class WxFileContentTypePropertiesUtils extends SystemUtil {

	public static final String ConfigFile = "/spring/file-content-type.properties";

	public static String getFileContentType(String fileSuffix) {

		String value = EnvironmentConfig.getInstance().getValueFromFile(ConfigFile, fileSuffix);
		if (value != null) {
			return value.trim();
		}
		return null;
	}

}
