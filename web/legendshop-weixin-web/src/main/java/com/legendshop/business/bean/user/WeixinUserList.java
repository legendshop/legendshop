package com.legendshop.business.bean.user;

import java.util.List;

import com.legendshop.business.bean.WeiXinMsg;



/**
 * 关注用户列表
 * @author tony
 *
 */
public class WeixinUserList extends WeiXinMsg {

	// 公众账号的总关注用户数
	private int total;
	// 获取的OpenID个数
	private int count;
	// OpenID列表
	private List<WeiXinUserInfo> userInfos;
	// 拉取列表的后一个用户的OPENID
	private String nextOpenId;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	
	public String getNextOpenId() {
		return nextOpenId;
	}

	public void setNextOpenId(String nextOpenId) {
		this.nextOpenId = nextOpenId;
	}

	public List<WeiXinUserInfo> getUserInfos() {
		return userInfos;
	}

	public void setUserInfos(List<WeiXinUserInfo> userInfos) {
		this.userInfos = userInfos;
	}
}
