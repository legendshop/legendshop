package com.legendshop.business.bean.pay;

import com.legendshop.util.AppUtils;

/**
 * 微信支付参数
 * 
 */
public class WeiXinPayParams {

	private String token;

	private String secret;

	private Long validateTime;

	private String productName;

	private String outTradeNo;

	private Double totalPrice;

	private String userId;

	private String openId;

	private String notifyUrl;

	private String sendUrl;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public boolean validate() {
		boolean config = true;
		if (AppUtils.isBlank(productName) || AppUtils.isBlank(outTradeNo)
				|| AppUtils.isBlank(totalPrice) || AppUtils.isBlank(userId)
				|| AppUtils.isBlank(openId)) {
			config = false;
		}
		return config;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getSendUrl() {
		return sendUrl;
	}

	public void setSendUrl(String sendUrl) {
		this.sendUrl = sendUrl;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public Long getValidateTime() {
		return validateTime;
	}

	public void setValidateTime(Long validateTime) {
		this.validateTime = validateTime;
	}

}
