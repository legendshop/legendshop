package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.JobStatusDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.JobStatus;

@Repository
public class JobStatusDaoImpl  extends GenericDaoImpl<JobStatus, Long>  implements JobStatusDao {
	
	public int deleteJobStatus(JobStatus jobStatus){
    	return delete(jobStatus);
    }
	
	public Long saveJobStatus(JobStatus jobStatus){
		return save(jobStatus);
	}
	
	public int updateJobStatus(JobStatus jobStatus){
		return update(jobStatus);
	}

	@Override
	public JobStatus getJobStatusByJobName(String jobName) {
		return this.getByProperties(new EntityCriterion().eq("jobName", jobName));
	}

	@Override
	public void deleteJobStatus(String name) {
		update("delete from ls_job_status where job_name=?", name);
	}

}
