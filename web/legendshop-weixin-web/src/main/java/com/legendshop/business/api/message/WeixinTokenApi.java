package com.legendshop.business.api.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.business.bean.WeixinAccessTokenResponse;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.util.HttpUtil;

/**
 * 获取微信公众号accessToken
 */
public class WeixinTokenApi {
	
	 private static Logger log = LoggerFactory.getLogger(WeixinTokenApi.class);
	 
	/**
	 * 获取微信公众号accessToken
	 * @param appId
	 * @param appSecret
	 * @return
	 */
	public static WeixinAccessTokenResponse getAccessToken(String appId, String appSecret) {
		
		String requestUrl = WxConfigUtil.TOKEN_URL.replace("APPID", appId)
				.replace("APPSECRET", appSecret);
		
		String text=HttpUtil.httpsRequest(requestUrl, "GET",null);
		if(null == text){
			return null;
		}
		
		WeixinAccessTokenResponse token =JSONObject.parseObject(text, WeixinAccessTokenResponse.class);
		
		return token;
	}
	
}
