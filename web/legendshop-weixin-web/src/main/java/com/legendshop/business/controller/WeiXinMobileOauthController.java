package com.legendshop.business.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.config.dto.WeixinPropertiesDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.business.api.message.UsersManageApi;
import com.legendshop.business.bean.user.WebAuthInfo;
import com.legendshop.business.bean.user.WeiXinUserInfo;
import com.legendshop.business.service.WeiXinSubService;
import com.legendshop.business.service.WeixinGzuserInfoService;
import com.legendshop.business.util.WeiXinLog;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.business.util.WxSignUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;
import com.legendshop.util.AppUtils;
import com.legendshop.util.HttpUtil;
import com.legendshop.util.JSONUtil;
import com.legendshop.web.util.WeiXinUtil;


/**
 * 网页授权获取用户基本信息, 用于mobile端
 * http://mp.weixin.qq.com/wiki/17/c0f37d5704f0b64713d5d2c37b468d75.html
 */
@Controller
public class WeiXinMobileOauthController {
    /**
     * The log.
     */
    private final Logger log = LoggerFactory.getLogger(WeiXinMobileOauthController.class);

    private static Map<String, Lock> locks = new ConcurrentHashMap<String, Lock>(500);

    private final static String OAUTH2_SNS = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";

    @Autowired
    private WeixinGzuserInfoService weixinGzuserInfoService;
    
    @Autowired
    private WeiXinSubService weiXinSubService;

    @Autowired
	private WeixinPropertiesDto weixinPropertiesDto;

    /**
     * 普通用户同意授权后 跳转回调redirect_uri
     * 通过code换取网页授权access_token和openId
     * @param code
     * @param state
     * @param oauth2url
     * @param time
     * @param validateCode
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/weixin/oauth", method = RequestMethod.GET)
    public String weixinOauth(HttpServletRequest request,
                              HttpServletResponse response,
                              @RequestParam String code, //code作为换取access_token的票据，每次用户授权带上的code将不一样，code只能使用一次，5分钟未被使用自动过期。
                              @RequestParam String state,  //重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
                              @RequestParam String oauth2url, //weixin mobile oauth callback回调地址
                              @RequestParam String time, // weixin mobile response validate time
                              @RequestParam String validateCode, // weixin mobile response validateCode
                               String refrenceURL)throws IOException {
    	
        // 判断是否微信环境, 5.0 之后的支持微信支付
        boolean isweixin = WeiXinUtil.isWeiXin(request);
        if (!isweixin) {
            log.error("不是微信 浏览器 code = {}, oauth2url = {}", code, oauth2url);
            return null;
        }
        //检查有效性
        if (!checkSignature(oauth2url, time, validateCode)) {
            log.error("无效的参数 code = {}, oauth2url = {}, validateCode = {}", new Object[]{code, oauth2url, validateCode});
            return null;
        }

        PayType payType = weiXinSubService.getPayTypeById(PayTypeEnum.WX_PAY.value());
        PaymentLog.log("############# WechatController 支付参数  {} ########## ", JSONUtil.getJson(payType));
        if (payType == null) {
            WeiXinLog.info(" WechatController 支付方式不存在,请设置支付方式! by PayTypeId={}", PayTypeEnum.WX_PAY.value());
        } else if (payType.getIsEnable() == Constants.OFFLINE) {
            WeiXinLog.info("WechatController 该支付方式没有启用! by PayTypeById={} ", PayTypeEnum.WX_PAY.value());
        }
        JSONObject jsonObject = JSONObject.parseObject(payType.getPaymentConfig());

        //通过APPID, APPSECRET以及授权认证通过后的CODE换取openID
        String oauth2_sns = OAUTH2_SNS.replaceAll("APPID", jsonObject.getString(WxConfigUtil.APPID)).replaceAll("SECRET", jsonObject.getString(WxConfigUtil.APPSECRET)).replaceAll("CODE", code);
        String text = HttpUtil.httpsRequest(oauth2_sns, "GET", null);
        log.info("weixin sns oauth2 result = {} ", text);
        if (AppUtils.isNotBlank(text)) {
            WebAuthInfo authInfo = JSONUtil.getObject(text, WebAuthInfo.class);
            if (authInfo.getErrcode() == null && authInfo.getOpenid() != null) {
                String openId = authInfo.getOpenid();
                String unionid = "";
                // callback
                Lock lock = locks.get(openId);
                if (lock == null) {
                    lock = new ReentrantLock(false);
                    locks.put(openId, lock);
                }
                lock.lock();//锁一下
                try {
                	
                    //授权成功, 获取微信用户基本信息接口
                	WeiXinUserInfo weiXinUserInfo = UsersManageApi.snsCallBackUserInfo(authInfo.getAccess_token(), openId);
                	
                	//记录或更新微信关注用户信息
                    WeixinGzuserInfo weixinGzuserInfo = weixinGzuserInfoService.getWeixinGzuserInfo(openId);//一旦关注就会有该用户存在
                    if (AppUtils.isBlank(weixinGzuserInfo)) {
                        weixinGzuserInfo = new WeixinGzuserInfo();
                        weixinGzuserInfo.setSubscribe("N");
                        weixinGzuserInfo.setOpenid(openId);
                        weixinGzuserInfo.setSubscribeTime(new Date());
                        weixinGzuserInfo.setAddtime(new Date());
                        if (weiXinUserInfo != null) {
                            weixinGzuserInfo.setNickname(AppUtils.removeYanText(weiXinUserInfo.getNickname()));
                            weixinGzuserInfo.setProvince(AppUtils.removeYanText(weiXinUserInfo.getProvince()));
                            weixinGzuserInfo.setCity(AppUtils.removeYanText(weiXinUserInfo.getCity()));
                            weixinGzuserInfo.setCountry(AppUtils.removeYanText(weiXinUserInfo.getCountry()));
                            weixinGzuserInfo.setHeadimgurl(weiXinUserInfo.getHeadimgurl());
                            weixinGzuserInfo.setSex(weiXinUserInfo.getSex());
                            weixinGzuserInfo.setUnionid(weiXinUserInfo.getUnionid());
                            
                            unionid = weiXinUserInfo.getUnionid();
                        }
                        
                        weixinGzuserInfoService.saveWeixinGzuserInfo(weixinGzuserInfo);
                    }else{
                    	weixinGzuserInfo.setNickname(AppUtils.removeYanText(weiXinUserInfo.getNickname()));
                        weixinGzuserInfo.setProvince(AppUtils.removeYanText(weiXinUserInfo.getProvince()));
                        weixinGzuserInfo.setCity(AppUtils.removeYanText(weiXinUserInfo.getCity()));
                        weixinGzuserInfo.setCountry(AppUtils.removeYanText(weiXinUserInfo.getCountry()));
                        weixinGzuserInfo.setHeadimgurl(weiXinUserInfo.getHeadimgurl());
                        weixinGzuserInfo.setSex(weiXinUserInfo.getSex());
                        weixinGzuserInfo.setUnionid(weiXinUserInfo.getUnionid());
                        
                        weixinGzuserInfoService.updateWeixinGzuserInfo(weixinGzuserInfo);
                    }
                    
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    locks.remove(openId);
                    lock.unlock();
                }
                //callback
                StringBuilder sb = new StringBuilder("redirect:");
                sb.append(oauth2url)
                        .append("?openId=")
                        .append(openId)
                        .append("&unionid=")
                        .append(unionid)
                        .append("&oauth2url=").append(URLEncoder.encode(oauth2url, "UTF-8"))
                        .append("&time=").append(time)
                        .append("&validateCode=").append(URLEncoder.encode(validateCode, "UTF-8"))
                        .append("&refrenceURL=").append(refrenceURL);
                return sb.toString();
            } else {
                log.error("weixin oauth2 access_token error by authInfo inf  ={} ", JSONUtil.getJson(authInfo));
            }
        }
        
        return "/systemerror";
    }

    /**
     * 检查调用者的有效性
     *
     * @param oauth2url
     * @param time
     * @param validateCode
     * @return
     */
    private boolean checkSignature(String oauth2url, String time, String validateCode) {
        return WxSignUtil.checkSignature(oauth2url, validateCode, time, weixinPropertiesDto.getWeixinKey());
    }
}
