package com.legendshop.business.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.business.service.WechatService;
import com.legendshop.business.service.WeiXinSubService;
import com.legendshop.business.util.WeiXinLog;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.business.util.WxSignUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;


/**
 * 微信服务调度中心
 * @author tony
 *
 */
@Controller
@RequestMapping("/weixin")
public class WechatController {

	@Autowired
	private WechatService wechatService;
	
	@Autowired
	private  WeiXinSubService weiXinSubService;
	
    /**
     * 验证,需要在微信中配置
     * 请填写接口配置信息，此信息需要你拥有自己的服务器资源。填写的URL需要正确响应微信发送的Token验证
     * 参见 https://mp.weixin.qq.com/wiki
     * 测试 https://mp.weixin.qq.com/debug/
     *
     * @param signature
     * @param timestamp
     * @param nonce
     * @param echostr
     * @return
     * @throws IOException 
     */
    @RequestMapping(value = "/wechat", method = RequestMethod.GET)
    public void wechatGet(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "signature") String signature,
			@RequestParam(value = "timestamp") String timestamp,
			@RequestParam(value = "nonce") String nonce,
			@RequestParam(value = "echostr") String echostr
			) throws IOException  {
        String outPut = "public/error";
    	if (AppUtils.isNotBlank(signature) && AppUtils.isNotBlank(timestamp) && AppUtils.isNotBlank(nonce)) {
    		
    		PayType payType = weiXinSubService.getPayTypeById(PayTypeEnum.WX_PAY.value());
    		PaymentLog.log("############# WechatController 支付参数  {} ########## ",JSONUtil.getJson(payType));
    		if (payType == null) {
    			WeiXinLog.info(" WechatController 支付方式不存在,请设置支付方式! by PayTypeId={}",PayTypeEnum.WX_PAY.value());
    			response.getWriter().print(outPut);
    			return ;
    		} else if (payType.getIsEnable() == Constants.OFFLINE) {
    			WeiXinLog.info("WechatController 该支付方式没有启用! by PayTypeById={} ",PayTypeEnum.WX_PAY.value());
    			response.getWriter().print(outPut);
    			return ;
    		}
    		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
    		
    		String token = jsonObject.getString(WxConfigUtil.TOKEN);
    		
			boolean validate = WxSignUtil.checkSignature(token, signature,	timestamp, nonce);
        	if (validate) {
        		 WeiXinLog.info("请求验证通过");
        		 outPut = echostr;
            }
        }
    	System.out.println(outPut);
    	response.getWriter().print(outPut);
    }


    /**
     * 微信通知消息
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "/wechat", method = RequestMethod.POST)
	public void wechatPost(HttpServletRequest request,HttpServletResponse response) throws IOException {
		 // 将请求、响应的编码均设置为UTF-8（防止中文乱码）  
        request.setCharacterEncoding("UTF-8");  
        response.setCharacterEncoding("UTF-8");  
		String respMessage = wechatService.wxCoreService(request);
		PrintWriter out = response.getWriter();
		out.print(respMessage);
		out.close();
	}


}
