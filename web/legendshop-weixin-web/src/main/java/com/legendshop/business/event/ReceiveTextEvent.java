package com.legendshop.business.event;

import com.legendshop.framework.event.SystemEvent;
import com.legendshop.model.entity.weixin.WeixinReceivetext;

public class ReceiveTextEvent extends SystemEvent<WeixinReceivetext> {

	public ReceiveTextEvent(WeixinReceivetext receivetext) {
		super(WeiXinEventId.RECEIVE_TEXT_EVENT);
		this.setSource(receivetext);
	}

}
