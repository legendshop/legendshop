/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.UserDetail;

/**
 * The Interface UserDetailDao.
 */
public interface UserDetailDao  extends GenericDao<UserDetail, String>{

	public abstract UserDetail getUserDetailById(String userId);
	
	/**
	 * 更新账户可用预付款
	 * @param updatePredeposit 要更新的预付款金额
	 * @param originalPredeposit 原来旧的预付款金额
	 * @param userId 用户ＩＤ
	 * @return
	 */
	public abstract int updateAvailablePredeposit(Double updatePredeposit,Double originalPredeposit, String userId);
	
	/**
	 * 更改金币
	 * @param updateUserCoin  更改数
	 * @param originalUserCoin 原来数
	 * @param userId
	 * @return
	 */
	public abstract int updateUserCoin(Double updateUserCoin, Double originalUserCoin,String userId);
	
	
}