/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.SystemParameter;

/**
 * The Interface SystemParameterDao.
 */
public interface SystemParameterDao extends Dao<SystemParameter, String>{

	List<SystemParameter> loadAll();

}