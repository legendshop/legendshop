/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.business.service;

/**
 * 系统初始化参数.
 */
public interface SystemParameterService {
	/**
	 * 初始化系统参数
	 */
	public void initSystemParameter();

}
