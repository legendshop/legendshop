/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinGzuserInfoDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;
import com.legendshop.util.AppUtils;

/**
 * The Class WeixinGzuserInfoDaoImpl.
 */

@Repository
public class WeixinGzuserInfoDaoImpl extends GenericDaoImpl<WeixinGzuserInfo, Long> implements WeixinGzuserInfoDao  {
     
    public WeixinGzuserInfo getWeixinGzuserInfo(String openid){
   		List<WeixinGzuserInfo>  infos= this.queryByProperties(new EntityCriterion().eq("openid", openid));
   		if(AppUtils.isNotBlank(infos)){
   			return infos.get(0);
   		}
		return null;
    }

	public WeixinGzuserInfo getWeixinGzuserInfo(Long id){
		return getById(id);
	}
	
    public int deleteWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo){
    	return delete(weixinGzuserInfo);
    }
	
	public Long saveWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo){
		return save(weixinGzuserInfo);
	}
	
	@Override
	public void saveWeixinGzuserInfo(List<WeixinGzuserInfo> entities){
		save(entities);
	}
	
	
	public int updateWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo){
		return update(weixinGzuserInfo);
	}
	
	public PageSupport getWeixinGzuserInfo(CriteriaQuery cq){
		return queryPage(cq);
	}

	@Override
	public void cancelsubscribe(String fromUserName) {
		this.update("update ls_weixin_gzuserinfo set   ls_weixin_gzuserinfo.subscribe = 'N' where ls_weixin_gzuserinfo.openId=? ", fromUserName);
	}

/*	@Override
	public WeixinGzuserInfo getWeixinGzuserInfoByUserId(String userId) {
		List<WeixinGzuserInfo>  infos= this.queryByProperties(new EntityCriterion().eq("userId", userId));
   		if(AppUtils.isNotBlank(infos)){
   			return infos.get(0);
   		}
		return null;
	}*/

/*	@Override
	public String getUserName(String userId) {
		return get("select name from ls_user where id=? ", String.class, userId);
	}*/
	
 }
