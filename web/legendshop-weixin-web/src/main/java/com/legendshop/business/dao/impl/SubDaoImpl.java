package com.legendshop.business.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.SubDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.entity.PdRecharge;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.entity.SubSettlementItem;
import com.legendshop.util.AppUtils;

/**
 * TODO  微信订单服务 残留历史问题,需要把 legendshop_weixin_server 去掉
 */
@Repository
public class SubDaoImpl extends GenericDaoImpl<Sub, Long> implements SubDao{
	/** The log. */
	private static Logger log = LoggerFactory.getLogger(SubDaoImpl.class);
	
	@Override
	public List<SubSettlementItem> getSubSettlementItems(String subSettlementSn) {
		List<SubSettlementItem> items=query("SELECT  *  FROM ls_sub_settlement_item WHERE sub_settlement_sn = ? ", SubSettlementItem.class, subSettlementSn);
		return items;
	}
	

	@Override
	public List<Sub> getSubBySubNumberByUserId(List<String> subNumbers, String userId) {
		EntityCriterion entityCriterion=new EntityCriterion().eq("userId", userId);
		entityCriterion.in("subNumber", subNumbers.toArray());
		return queryByProperties(entityCriterion);
	}
	
	

	@Override
	public SubSettlement getSubSettlement(String out_trade_no) {
		SubSettlement settlements= get("SELECT  *  FROM ls_sub_settlement WHERE sub_settlement_sn = ? ORDER BY create_time DESC", new Object[] { out_trade_no }, new SubSettlementRowMapper());
		return settlements;
	}
	
	class SubSettlementRowMapper implements RowMapper<SubSettlement> {

		@Override
		public SubSettlement mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			
			SubSettlement settlement=new SubSettlement();
			settlement.setSubSettlementId(rs.getLong("sub_settlement_id"));
			settlement.setSubSettlementSn(rs.getString("sub_settlement_sn"));
			settlement.setFlowTradeNo(rs.getString("flow_trade_no"));
			settlement.setCashAmount(rs.getDouble("cash_amount"));
			settlement.setPayTypeId(rs.getString("pay_type_id"));
			settlement.setPayTypeName(rs.getString("pay_type_name"));
			settlement.setUserId(rs.getString("user_id"));
			settlement.setIsClearing(rs.getBoolean("is_clearing"));
			settlement.setPayUserInfo(rs.getString("pay_user_info"));
			settlement.setPdAmount(rs.getDouble("pd_amount"));
			settlement.setPrePayType(rs.getInt("pre_pay_type"));
			settlement.setCreateTime(rs.getDate("create_time"));
			settlement.setClearingTime(rs.getDate("clearing_time"));
			settlement.setPresell(rs.getBoolean("is_presell"));
			settlement.setType(rs.getString("type"));
			settlement.setSettlementFrom(rs.getString("settlement_from"));
			settlement.setFullPay(rs.getBoolean("full_pay"));
			return settlement;
		}
		
	}

	@Override
	public int updateSubSettlementForPay(SubSettlement subSettlement) {
		return update("update ls_sub_settlement set is_clearing = ?, clearing_time= ?, pay_user_info = ?,flow_trade_no = ?,pay_type_id= ?,pay_type_name= ? " +
				" where sub_settlement_id= ? and user_id=? and  is_clearing = 0 ", 
				
				new Object[]{
				     subSettlement.getIsClearing(), 
				     subSettlement.getClearingTime(), 
				     subSettlement.getPayUserInfo(),
				     subSettlement.getFlowTradeNo(), 
				     subSettlement.getPayTypeId(),
				     subSettlement.getPayTypeName(),
				     subSettlement.getSubSettlementId(),
				     subSettlement.getUserId()
	              }
			    );
	}
	

	@Override
	public int updateSubForPay(Sub sub) {
		return update("update ls_sub set pay_id=?,pay_type_id=?,pay_type_name=?,pay_date=?,update_date=?,status=?,is_payed=?,flow_trade_no = ?,sub_settlement_sn=? where sub_id=? and user_id=? and status= ? ", 
				new Object[]{
					sub.getPayId(),
					sub.getPayTypeId(),
					sub.getPayTypeName(),
					sub.getPayDate(),
					sub.getUpdateDate(),
					sub.getStatus(),
					true,
					sub.getFlowTradeNo(),
					sub.getSubSettlementSn(),
					sub.getSubId(),
					sub.getUserId(),
					OrderStatusEnum.UNPAY.value()
		          }
				);
	}


	@Override
	public Product getProduct(Long prodId) {
		return this.get("select prod_id as prodId,buys,stock_counting as stockCounting from ls_prod where prod_id=? ", Product.class, prodId);
	}

	
	/**
	 * 付款回来减库存，
	 * 应该减仓库库存
	 */
	@Override
	public boolean addHold(Long prodId, Long skuId, long basketCount) {
		Integer stocks = this.getStocksByLockMode(skuId);
		// sku库存
		if (stocks == null) {
			stocks = 0;
		}
		if (stocks - basketCount < 0) {
			return false;
		} else {
			this.updateSkuStocks(skuId, stocks - basketCount);
			return true;
		}
		
	}

	@Override
	public void updateBuys(long buys, Long prodId) {
		update("update ls_prod set buys = ? where prod_id = ?", buys, prodId);
	}

	@Override
	public SubSettlement getSubSettlement(String outTradeNo, String userId) {
		List<SubSettlement> settlements= query("SELECT  *  FROM ls_sub_settlement WHERE sub_settlement_sn = ? and user_id=?  ORDER BY create_time DESC", new Object[] { outTradeNo,userId }, new SubSettlementRowMapper());
		if(AppUtils.isNotBlank(settlements)){
			return settlements.get(0);
		}
		return null;
	}

	@Override
	public PdRecharge findRecharge(String userId, String outTradeNo) {
		PdRecharge pdRecharge= get("SELECT * FROM ls_pd_recharge WHERE sn = ? and user_id=? ", PdRecharge.class,new Object[] { outTradeNo,userId });
		return pdRecharge;
	}

	@Override
	public PdRecharge findRecharge(String outTradeNo) {
				
//		PdRecharge pdRecharge= get("SELECT * FROM ls_pd_recharge WHERE sn = ?  ", PdRecharge.class,new Object[] { outTradeNo });

		PdRecharge pdRecharge= get("SELECT ls_pd_recharge.* FROM ls_pd_recharge LEFT JOIN  ls_sub_settlement_item ON ls_pd_recharge.sn " +
				"= ls_sub_settlement_item.sub_number WHERE ls_sub_settlement_item.sub_settlement_sn = ?  ", PdRecharge.class,new Object[] { outTradeNo });
				
		return pdRecharge;
	}

	
	
	/**
	 * 获取sku
	 * @param skuId
	 * @return
	 */
	public Integer getStocksByLockMode(Long skuId) {
		return get("select stocks from ls_sku where sku_id = ? for update", Integer.class, skuId);
	}
	
	/**
	 * 更新库存
	 */
	@CacheEvict(value = "Sku", key = "#skuId")
	public void updateSkuStocks(Long skuId, long stocks) {
		update("update ls_sku set stocks = ? where sku_id = ?", stocks, skuId);
	}
	

	@Override
	public PayType getPayTypeById(String value) {
		return get("select pay_id as payId,pay_type_id as payTypeId,pay_type_name as payTypeName,is_enable as isEnable,payment_config as paymentConfig from ls_pay_type where pay_type_id=? ", PayType.class, value);
	}


	

}
