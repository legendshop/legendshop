/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service;

import java.util.List;

import com.legendshop.model.entity.weixin.WeixinGzuserInfo;

/**
 * The Class WeixinGzuserInfoService.
 */
public interface WeixinGzuserInfoService  {

    public WeixinGzuserInfo getWeixinGzuserInfo(String openid);
    /*
    public WeixinGzuserInfo getWeixinGzuserInfoByUserID(String userId);*/
    
    boolean existWeiXinUser(String opendId);
    
    public Long saveWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo);

    public void updateWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo);

    /**
     * 取消用户关注
     * @param fromUserName
     */
	public void cancelsubscribe(String fromUserName);

	/**
	 * 微信同步 删除微信用户信息
	 */
	public void deleteGzUserInfos();

	public void batchUserInfo(List<WeixinGzuserInfo> infos);

}
