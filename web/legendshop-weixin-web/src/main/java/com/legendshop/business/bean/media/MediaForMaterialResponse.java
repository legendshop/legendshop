package com.legendshop.business.bean.media;

/**
 * 新增其他类型永久素材
 * 
 * @author tony
 * 
 */
public class MediaForMaterialResponse {

	/** 新增的图片素材的图片URL */
	private String url;
	/** 媒体资源ID */
	private String media_id;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMedia_id() {
		return media_id;
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

}
