/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.weixin.WeixinNewsitem;

/**
 * The Class WeixinNewsitemDao.
 */

public interface WeixinNewsitemDao extends GenericDao<WeixinNewsitem, Long> {
     
    public abstract List<WeixinNewsitem> getWeixinNewsitemByTemplateId(Long templateId);

	public abstract WeixinNewsitem getWeixinNewsitem(Long id);
	
    public abstract int deleteWeixinNewsitem(WeixinNewsitem weixinNewsitem);
	
	public abstract Long saveWeixinNewsitem(WeixinNewsitem weixinNewsitem);
	
	public abstract int updateWeixinNewsitem(WeixinNewsitem weixinNewsitem);
	
 }
