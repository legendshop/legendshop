/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.WeixinNewsitemDao;
import com.legendshop.business.service.WeixinNewsitemService;
import com.legendshop.model.entity.weixin.WeixinNewsitem;

/**
 * The Class WeixinNewsitemServiceImpl.
 */
@Service("weixinNewsitemService")
public class WeixinNewsitemServiceImpl  implements WeixinNewsitemService{
	
	@Autowired
    private WeixinNewsitemDao weixinNewsitemDao;


    public WeixinNewsitem getWeixinNewsitem(Long id) {
        return weixinNewsitemDao.getWeixinNewsitem(id);
    }

}
