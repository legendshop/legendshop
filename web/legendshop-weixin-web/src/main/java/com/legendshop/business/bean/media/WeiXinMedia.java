package com.legendshop.business.bean.media;

import java.util.Date;

import com.legendshop.business.bean.WeiXinMsg;

/**
 * 1、对于临时素材，每个素材（media_id）会在开发者上传或粉丝发送到微信服务器3天后自动删除（所以用户发送给开发者的素材，若开发者需要，应尽快下载到本地），以节省服务器资源。
   2、media_id是可复用的。
   3、素材的格式大小等要求与公众平台官网一致。具体是，图片大小不超过2M，支持bmp/png/jpeg/jpg/gif格式，语音大小不超过5M，长度不超过60秒，支持mp3/wma/wav/amr格式
   4、需使用https调用本接口。
 * @author tony
 *
 */
public class WeiXinMedia extends WeiXinMsg {

	// 媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb，主要用于视频与音乐格式的缩略图）
	private String type;

	// 媒体文件上传后，获取时的唯一标识
	private String media_id;

	// 媒体文件上传时间戳
	private Date created_at;


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMedia_id() {
		return media_id;
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public WeiXinMedia() {

	}

	public WeiXinMedia(String type, String media_id, Date created_at) {
		this.type = type;
		this.media_id = media_id;
		this.created_at = created_at;
	}

}
