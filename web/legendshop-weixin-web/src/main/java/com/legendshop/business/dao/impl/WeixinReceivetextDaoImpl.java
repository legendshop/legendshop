/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinReceivetextDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.weixin.WeixinReceivetext;

/**
 * The Class WeixinReceivetextDaoImpl.
 */

@Repository
public class WeixinReceivetextDaoImpl extends GenericDaoImpl<WeixinReceivetext, Long> implements WeixinReceivetextDao  {
     
    public List<WeixinReceivetext> getWeixinReceivetext(String userName){
   		return this.queryByProperties(new EntityCriterion().eq("userName", userName));
    }

	public WeixinReceivetext getWeixinReceivetext(Long id){
		return getById(id);
	}
	
    public int deleteWeixinReceivetext(WeixinReceivetext weixinReceivetext){
    	return delete(weixinReceivetext);
    }
	
	public Long saveWeixinReceivetext(WeixinReceivetext weixinReceivetext){
		return save(weixinReceivetext);
	}
	
	public int updateWeixinReceivetext(WeixinReceivetext weixinReceivetext){
		return update(weixinReceivetext);
	}
	
 }
