package com.legendshop.business.bean.media;

import java.util.ArrayList;
import java.util.List;

/**
 * 上传图文消息素材
 * 
 * @author tony
 * 
 */
public class WeiXinMediaArticlesRequest {

	List<WeiXinMediaArticle> articles = new ArrayList<WeiXinMediaArticle>();

	public List<WeiXinMediaArticle> getArticles() {
		return articles;
	}

	public void setArticles(List<WeiXinMediaArticle> articles) {
		this.articles = articles;
	}
	
	@Override
	public String toString() {
		return "WxArticlesRequest [articles=" + articles + "]";
	}

}
