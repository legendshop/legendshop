package com.legendshop.business.bean.media;

/**
 * 永久素材修改
 * 
 * @author tony
 * 
 */
public class WeiXinUpdateArticle {

	private String media_id;

	private int index;

	private WeiXinMediaArticle article = new WeiXinMediaArticle();

	public String getMedia_id() {
		return media_id;
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public WeiXinMediaArticle getArticle() {
		return article;
	}

	public void setArticle(WeiXinMediaArticle article) {
		this.article = article;
	}

}
