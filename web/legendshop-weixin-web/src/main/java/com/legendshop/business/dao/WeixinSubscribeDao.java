/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.weixin.WeixinSubscribe;

/**
 * The Class WeixinSubscribeDao.
 */

public interface WeixinSubscribeDao extends GenericDao<WeixinSubscribe, Long> {
     

	public abstract List<WeixinSubscribe> getWeixinSubscribe();
	
 }
