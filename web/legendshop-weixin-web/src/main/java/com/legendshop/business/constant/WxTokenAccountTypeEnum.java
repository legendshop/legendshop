package com.legendshop.business.constant;

import com.legendshop.util.constant.StringEnum;


/**
 * 微信账号类型
 */
public enum WxTokenAccountTypeEnum  implements  StringEnum{
	
	/** 微信公众号 */
	OA("OA"), 
	
	/** 微信小程序 */
	MP("mp"); 
	
	/** The value. */
	private final String value;
	
	/**
	 * Instantiates a new function enum.
	 * 
	 * @param value
	 *            the value
	 */
	private WxTokenAccountTypeEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}
}
