package com.legendshop.business.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.business.service.WeiXinSubService;
import com.legendshop.business.service.WeixinTokenService;
import com.legendshop.business.util.WeiXinLog;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.dto.weixin.JsApiInfo;
import com.legendshop.model.entity.PayType;
import com.legendshop.util.JSONUtil;
import com.legendshop.web.util.RequestHandler;

@Controller
public class JsApiDemoController {

	@Autowired
	private WeixinTokenService weixinTokenService;
	
	@Autowired
	private WeiXinSubService weiXinSubService;
	
	@RequestMapping(value = "/jsapi",method=RequestMethod.GET)
	public String jsapi(HttpServletRequest request,
			HttpServletResponse response) {

		 String clientUrl = request.getScheme() + "://"
	                + request.getServerName() + request.getRequestURI();
		
		System.out.println("clientUrl====="+clientUrl);
		
		  // 获取到ticket凭证之后，需要进行一次签名
        String nonceStr=RandomStringUtils.randomAlphabetic(16);
        long timestamp = System.currentTimeMillis();// 时间戳
        String jsapi_ticket= weixinTokenService.getJsapiTicket();
        WeiXinLog.info("接口调用凭证jsapi_ticket：：" + jsapi_ticket);
        
        //该签名是用于前端js中wx.config配置中的signature值。
        RequestHandler handler=new RequestHandler();
        String signature  =  handler.createSign_wx_config(jsapi_ticket,nonceStr,timestamp,clientUrl);
        
        PayType payType = weiXinSubService.getPayTypeById(PayTypeEnum.WX_PAY.value());
		PaymentLog.log("############# WechatController 支付参数  {} ########## ",JSONUtil.getJson(payType));
		if (payType == null) {
			WeiXinLog.info(" WechatController 支付方式不存在,请设置支付方式! by PayTypeId={}",PayTypeEnum.WX_PAY.value());
		} else if (payType.getIsEnable() == Constants.OFFLINE) {
			WeiXinLog.info("WechatController 该支付方式没有启用! by PayTypeById={} ",PayTypeEnum.WX_PAY.value());
		}
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		request.setAttribute("appId",jsonObject.getString(WxConfigUtil.APPID));
		request.setAttribute("timeStamp",  timestamp);
		request.setAttribute("nonceStr", nonceStr);
		request.setAttribute("signature", signature);
		return "/jsapidemo";
	}
	
	
	
	/**
	 * 获取JS-SDK的配置信息
	 * @param request
	 * @param response
	 * @param url 当前网页的URL，不包含#及其后面部分
	 * @return
	 */
	@RequestMapping(value = "/getJsApiInfo", method = RequestMethod.POST)
	public @ResponseBody JsApiInfo getJsApiInfo(HttpServletRequest request,
			HttpServletResponse response,@RequestParam String url) {
		String nonceStr=RandomStringUtils.randomAlphabetic(16);
	    long timestamp = System.currentTimeMillis()/1000;// 时间戳(秒)
	    String jsapiTicket= weixinTokenService.getJsapiTicket();
	    
	    //该签名是用于前端js中wx.config配置中的signature值。
        RequestHandler handler=new RequestHandler();
        String signature  =  handler.createSign_wx_config(jsapiTicket,nonceStr,timestamp,url);
        
        PayType payType = weiXinSubService.getPayTypeById(PayTypeEnum.WX_PAY.value());
		PaymentLog.log("############# WechatController 支付参数  {} ########## ",JSONUtil.getJson(payType));
		if (payType == null) {
			WeiXinLog.info(" WechatController 支付方式不存在,请设置支付方式! by PayTypeId={}",PayTypeEnum.WX_PAY.value());
		} else if (payType.getIsEnable() == Constants.OFFLINE) {
			WeiXinLog.info("WechatController 该支付方式没有启用! by PayTypeById={} ",PayTypeEnum.WX_PAY.value());
		}
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
        
	    JsApiInfo jsApiInfo=new JsApiInfo();
		jsApiInfo.setAppId(jsonObject.getString(WxConfigUtil.APPID));
		jsApiInfo.setNonceStr(nonceStr);
		jsApiInfo.setTimestamp(String.valueOf(timestamp));
		jsApiInfo.setSignature(signature);
	     
		return jsApiInfo;
	}

}
