package com.legendshop.business.service;

import com.legendshop.model.dto.PredepositPaySuccess;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.entity.PdRecharge;
import com.legendshop.model.entity.SubSettlement;

/**
 * TODO  残留历史问题,需要把 legendshop_weixin_server 去掉
 */
public interface WeiXinSubService {

	SubSettlement getSubSettlement(String outTradeNo);
	
	SubSettlement getSubSettlement(String outTradeNo, String userId);

	String doBankCallback(SubSettlement subSettlement);

	PdRecharge findRecharge(String userId, String outTradeNo);
	
	PdRecharge findRecharge(String outTradeNo);

	
	/**
	 * 预付款成功充值
	 * @param pdRecharge
	 * @return
	 */
	String rechargeForPaySucceed(PdRecharge pdRecharge);
	
	/**
	 * 微信支付成功后，余额处理
	 * @param paySuccess
	 * @return
	 */
	int predepositForPaySucceed(PredepositPaySuccess paySuccess);

	/**
	 * 获取支付方式
	 * @param value
	 * @return
	 */
	PayType getPayTypeById(String value);
	
}

