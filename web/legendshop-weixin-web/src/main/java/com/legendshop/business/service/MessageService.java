package com.legendshop.business.service;

import com.legendshop.model.SiteMessageInfo;

/**
 * 消息服务
 *
 */
public interface MessageService {
	/**
	 * 发送站内信
	 */
	public void sendSiteMessage(SiteMessageInfo msg);

}
