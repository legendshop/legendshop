/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service;

import java.util.List;

import com.legendshop.model.entity.weixin.WeixinReceivetext;

/**
 * The Class WeixinReceivetextService.
 */
public interface WeixinReceivetextService  {

    public List<WeixinReceivetext> getWeixinReceivetext(String userName);

    public WeixinReceivetext getWeixinReceivetext(Long id);
    
    public void deleteWeixinReceivetext(WeixinReceivetext weixinReceivetext);
    
    public Long saveWeixinReceivetext(WeixinReceivetext weixinReceivetext);

    public void updateWeixinReceivetext(WeixinReceivetext weixinReceivetext);
}
