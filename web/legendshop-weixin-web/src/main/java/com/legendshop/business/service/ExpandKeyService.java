package com.legendshop.business.service;

import javax.servlet.http.HttpServletRequest;

import com.legendshop.business.bean.message.resp.BaseMessage;

public interface ExpandKeyService {

	

	/**
	 * 获取接口关键字，例如："翻译"
	 * @return
	 */
    String getKey();
    
    
    /**
     * 针对关键字的功能处理方法
     * @param content      请求文本
     * @param textMessage  默认回复此文本消息
     * @param request      请求
     * @return
     */
 	String excute(HttpServletRequest request,String content,BaseMessage defaultMessageResp);
 	
 	
}
