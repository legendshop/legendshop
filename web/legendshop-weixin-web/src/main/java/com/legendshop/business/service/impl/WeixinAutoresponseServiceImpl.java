/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.WeixinAutoresponseDao;
import com.legendshop.business.service.WeixinAutoresponseService;
import com.legendshop.business.util.WeiXinLog;
import com.legendshop.model.entity.weixin.WeixinAutoresponse;
import com.legendshop.util.AppUtils;

/**
 * The Class WeixinAutoresponseServiceImpl.
 */
@Service("weixinAutoresponseService")
public class WeixinAutoresponseServiceImpl  implements WeixinAutoresponseService{
	
	@Autowired
    private WeixinAutoresponseDao weixinAutoresponseDao;

    public List<WeixinAutoresponse> getWeixinAutoresponse(String userName) {
        return weixinAutoresponseDao.getWeixinAutoresponse(userName);
    }

    public WeixinAutoresponse getWeixinAutoresponse(Long id) {
        return weixinAutoresponseDao.getWeixinAutoresponse(id);
    }

    public void deleteWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse) {
        weixinAutoresponseDao.deleteWeixinAutoresponse(weixinAutoresponse);
    }

    public Long saveWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse) {
        if (!AppUtils.isBlank(weixinAutoresponse.getId())) {
            updateWeixinAutoresponse(weixinAutoresponse);
            return weixinAutoresponse.getId();
        }
        return weixinAutoresponseDao.saveWeixinAutoresponse(weixinAutoresponse);
    }

    public void updateWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse) {
        weixinAutoresponseDao.updateWeixinAutoresponse(weixinAutoresponse);
    }

	@Override
	public WeixinAutoresponse findAutoResponseMsgTriggerType(String msgTriggerType) {
		WeixinAutoresponse autoresponse=weixinAutoresponseDao.findAutoResponseMsgTriggerType(msgTriggerType);
	/*	List<WeixinAutoresponse> weixinAutoresponses=weixinAutoresponseDao.getWeixinAutoresponse();
		if(AppUtils.isBlank(weixinAutoresponses)){
			for (WeixinAutoresponse r : weixinAutoresponses) {
				// 如果包含关键字
				String kw = r.getKeyword();
				String[] allkw = kw.split(",");
				for (String k : allkw) {
					if (k.equals(content)) {
						WeiXinLog.info("-------------------- 客户端：" + content + ", 精确匹配到关键字：" + k + " --------------------");
						return r;
					}
				}
			}
			WeiXinLog.info("-------------------- 客户端：" + content + ", 精确匹配不到关键字 --------------------");
			return matcheKey(content,weixinAutoresponses);
		}else{
			WeiXinLog.info("-------------------- 后台服务端没有设置关键字回复 --------------------");
		}*/
		return autoresponse;
	}
	
	/**
	 * 模糊匹配关键字
	 * 
	 * @param content
	 *            用户输入的内容
	 * @param autoResponses
	 *            自动回复列表
	 * @author ZhongYun Fan
	 * @return
	 */
	private WeixinAutoresponse matcheKey(String content, List<WeixinAutoresponse> autoResponses) {
		/*WeiXinLog.info("-------------------- 客户端：" + content + ", 开始模糊匹配关键字 --------------------");
		if (autoResponses != null && autoResponses.size() > 0) {
			for (WeixinAutoresponse autoResponse : autoResponses) {
				String keyWord = autoResponse.getKeyword();
				String[] keys = keyWord.split(",");
				for (String keyRegex : keys) {
					if (keyRegex.length() == 1 && keyRegex.equals("?")) {
						continue;
					}
					content = content.toLowerCase();
					keyRegex = keyRegex.toLowerCase();
					if (content.equals(keyRegex) || content.matches(keyRegex)) {
						WeiXinLog.info("-------------------- 客户端：" + content + ", 模糊匹配到关键字：" + keyRegex + " --------------------");
						return autoResponse;
					}
				}
			}
		}*/
		WeiXinLog.info("-------------------- 客户端：" + content + ", 精确/模糊 匹配不到关键字 --------------------");
		return null;
	}
	
	
}
