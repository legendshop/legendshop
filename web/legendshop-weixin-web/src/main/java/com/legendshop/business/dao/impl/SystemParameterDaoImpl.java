/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.SystemParameterDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.SystemParameter;

/**
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * ----------------------------------------------------------------------------
 * 提示：在未取得LegendShop商业授权之前，您不能将本软件应用于商业用途，否则LegendShop将保留追究的权力。
 * ----------------------------------------------------------------------------
 * 官方网站：http://www.legendesign.net
 * ----------------------------------------------------------------------------
 */

@Repository
public class SystemParameterDaoImpl extends GenericDaoImpl<SystemParameter, String> implements SystemParameterDao {


	@Override
	public List<SystemParameter> loadAll() {
		return queryAll();
	}


}
