package com.legendshop.business.service;


/**
 * 微信消息处理类
 * @author tony
 *
 */
public interface WeixinMessageService {
	
	/**
	 * 
	 * @param resMsgType　//消息类型 news text
	 * @param toUserName  // 接收方帐号（收到的OpenID）
	 * @param fromUserName  // 开发者微信号
	 * @param templateId  // 消息模版ＩＤ
	 * @return
	 */
	public abstract String getMsg(String resMsgType,String toUserName,String fromUserName,Long templateId);

}
