package com.legendshop.business.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.SubHistory;

public interface SubHistoryDao extends GenericDao<SubHistory, Long> {
	
	public abstract void saveSubHistory(SubHistory subHistory);
	
	
}
