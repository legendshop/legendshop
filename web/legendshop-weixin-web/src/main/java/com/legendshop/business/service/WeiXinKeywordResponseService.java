/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service;

import java.util.List;

import com.legendshop.business.bean.message.resp.TextMessageResp;
import com.legendshop.model.entity.weixin.WeiXinKeywordResponse;

/**
 * The Class WeiXinKeywordResponseService.
 */
public interface WeiXinKeywordResponseService  {

	public List<WeiXinKeywordResponse> getWeiXinKeywordResponse();

	/**
	 * 处理微信关键字自动回复
	 * @param content
	 * @param textMessageResp
	 * @return
	 */
	public String disposeKeywordResponse(String content,
			TextMessageResp textMessageResp);
}
