package com.legendshop.business.dao;

import org.springframework.stereotype.Repository;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.coin.CoinLog;

/**
 * 金币操作日志
 * @author weibf
 */
@Repository
public interface CoinLogDao extends GenericDao<CoinLog, Long>{
	
	public Long saveCoinLog(CoinLog entity);
	
}
