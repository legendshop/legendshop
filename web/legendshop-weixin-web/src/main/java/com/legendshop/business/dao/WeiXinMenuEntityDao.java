/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.business.entity.MenuEntity;
import com.legendshop.dao.GenericJdbcDao;

/**
 * The Class WeixinMenuDao.
 */

public interface WeiXinMenuEntityDao extends GenericJdbcDao {
     
    public abstract List<MenuEntity> getWeixinMenu();

    public abstract MenuEntity findUniqueByProperty(String menu_key);

 }
