package com.legendshop.business.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.entity.PdRecharge;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.entity.SubSettlementItem;
/**
 * TODO  微信订单服务 残留历史问题,需要把 legendshop_weixin_server 去掉
 */
public interface SubDao extends GenericDao<Sub, Long> {

	SubSettlement getSubSettlement(String out_trade_no);
	
	SubSettlement getSubSettlement(String out_trade_no, String userId);

	int updateSubSettlementForPay(SubSettlement subSettlement);

	int updateSubForPay(Sub sub);

	Product getProduct(Long prodId);

	/**
	 * 微信付款回来减库存
	 * @param prodId
	 * @param skuId
	 * @param basketCount
	 * @param provinceId 省份Id
	 * @return
	 */
	boolean addHold(Long prodId, Long skuId, long basketCount);

	void updateBuys(long buys, Long prodId);

	PdRecharge findRecharge(String userId, String outTradeNo);

	PdRecharge findRecharge(String outTradeNo);

	/**
	 * 获取支付方式
	 * @param value
	 * @return
	 */
	PayType getPayTypeById(String value);

	List<SubSettlementItem> getSubSettlementItems(String subSettlementSn);

	List<Sub> getSubBySubNumberByUserId(List<String> subNumbers, String userId);
}
