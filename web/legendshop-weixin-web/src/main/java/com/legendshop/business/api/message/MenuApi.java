package com.legendshop.business.api.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.util.HttpUtil;

/**
 * 菜单API
 * @author tony
 *
 */
public class MenuApi {
	
	 private static Logger log = LoggerFactory.getLogger(MenuApi.class);
	 
	 /**
     * 创建菜单
     *
     * @param menu        菜单实例
     * @param accessToken 凭证
     * @return true成功 false失败
     */
    public static JSONObject createMenu(String menu, String accessToken) {
         String url =WxConfigUtil.MENU_CREATE_URL.replace("ACCESS_TOKEN", accessToken);
         String text=  HttpUtil.httpsRequest(url, "POST", menu);
         if(text!=null){
         	 JSONObject jsonObject=JSONObject.parseObject(text);
         	 return jsonObject;
 		}
		return null;
    }
    
    
    /**
     * 查询菜单
     *
     * @param accessToken 凭证
     * @return
     */
    public static String getMenu(String accessToken) {
        String result = null;
        String requestUrl = WxConfigUtil.MENU_GET_URL.replace("ACCESS_TOKEN", accessToken);
        // 发起GET请求查询菜单
        String text=  HttpUtil.httpsRequest(requestUrl, "GET", null);
        
        if(text!=null){
        	JSONObject jsonObject=JSONObject.parseObject(text);
        	 if(jsonObject!=null ){
        		 result = jsonObject.toString();
            }
		}
        return result;
    }
    
    /**
     * 删除菜单
     *
     * @param accessToken 凭证
     * @return true成功 false失败
     */
    public static boolean deleteMenu(String accessToken) {
         boolean result = false;
         String requestUrl =WxConfigUtil.MASS_DELETE_URL.replace("ACCESS_TOKEN", accessToken);
         // 发起GET请求删除菜单
         String text=  HttpUtil.httpsRequest(requestUrl, "GET", null);
        
         if(text!=null){
         	JSONObject jsonObject=JSONObject.parseObject(text);
         	 if(jsonObject!=null ){
         		int errorCode = jsonObject.getIntValue("errcode");
                String errorMsg = jsonObject.getString("errmsg");
                if (0 == errorCode) {
                    result = true;
                } else {
                    result = false;
                    log.error("删除菜单失败 errcode:{} errmsg:{}", errorCode, errorMsg);
                }
             }
 		}
        return result;
    }
}
