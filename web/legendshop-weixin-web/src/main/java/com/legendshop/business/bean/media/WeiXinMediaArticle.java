package com.legendshop.business.bean.media;

import com.legendshop.business.bean.WeiXinMsg;

/**
 * 1、新增的永久素材也可以在公众平台官网素材管理模块中看到
 * 2、永久素材的数量是有上限的，请谨慎新增。图文消息素材和图片素材的上限为5000，其他类型为1000
 * 3、素材的格式大小等要求与公众平台官网一致。具体是，图片大小不超过2M
 * ，支持bmp/png/jpeg/jpg/gif格式，语音大小不超过5M，长度不超过60秒，支持mp3/wma/wav/amr格式
 * 4、调用该接口需https协议
 * 
 * @author tony 永久素材图文信息类
 */
public class WeiXinMediaArticle extends WeiXinMsg {

	/** 图文消息缩略图的media_id */
	private String thumb_media_id;
	/** 图文消息的作者 */
	private String author;
	/** 图文消息的标题 */
	private String title;
	/** 在图文消息页面点击“阅读原文”后的页面 */
	private String content_source_url;
	/** 图文消息页面的内容，支持HTML标签 */
	private String content;
	/** 图文消息的描述 */
	private String digest;
	/** 是否显示封面，1为显示，0为不显示 */
	private String show_cover_pic;

	private String fileName;

	private String filePath;

	public String getThumb_media_id() {
		return thumb_media_id;
	}

	public void setThumb_media_id(String thumb_media_id) {
		this.thumb_media_id = thumb_media_id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent_source_url() {
		return content_source_url;
	}

	public void setContent_source_url(String content_source_url) {
		this.content_source_url = content_source_url;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDigest() {
		return digest;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}

	public String getShow_cover_pic() {
		return show_cover_pic;
	}

	public void setShow_cover_pic(String show_cover_pic) {
		this.show_cover_pic = show_cover_pic;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Override
	public String toString() {
		return "WeixinMediaArticle [thumb_media_id=" + thumb_media_id + ", author="
				+ author + ", title=" + title + ", content_source_url="
				+ content_source_url + ", content=" + content + ", digest="
				+ digest + ", show_cover_pic=" + show_cover_pic + ", fileName="
				+ fileName + ", filePath=" + filePath + "]";
	}

}
