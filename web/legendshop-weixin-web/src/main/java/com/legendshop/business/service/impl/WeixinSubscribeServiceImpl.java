/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.api.message.UsersManageApi;
import com.legendshop.business.bean.message.event.SubscribeEvent;
import com.legendshop.business.bean.message.resp.TextMessageResp;
import com.legendshop.business.bean.user.WeiXinUserInfo;
import com.legendshop.business.dao.WeixinSubscribeDao;
import com.legendshop.business.service.WeixinGzuserInfoService;
import com.legendshop.business.service.WeixinMessageService;
import com.legendshop.business.service.WeixinSubscribeService;
import com.legendshop.business.service.WeixinTokenService;
import com.legendshop.business.util.MessageUtil;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;
import com.legendshop.model.entity.weixin.WeixinSubscribe;
import com.legendshop.util.AppUtils;

/**
 * The Class WeixinSubscribeServiceImpl.
 */

@Service("weixinSubscribeService")
public class WeixinSubscribeServiceImpl  implements WeixinSubscribeService{
	
	@Autowired
    private WeixinSubscribeDao weixinSubscribeDao;
    
	@Autowired
    private WeixinMessageService weixinMessageService;
    
	@Autowired
    private WeixinGzuserInfoService weixinGzuserInfoService;
    
	@Autowired
    private WeixinTokenService weixinTokenService;

	@Override
	public String subscribeResponse(SubscribeEvent subscribeEvent) {
		/**
		 * 第一次关注
		 */
		WeixinGzuserInfo gzuserInfo=weixinGzuserInfoService.getWeixinGzuserInfo(subscribeEvent.getFromUserName());
		if(AppUtils.isNotBlank(gzuserInfo)){
			String accessToken=weixinTokenService.getAccessToken();
			String openId=subscribeEvent.getFromUserName();
			WeiXinUserInfo userInfo=UsersManageApi.getUserInfo(accessToken, openId);
			if(AppUtils.isNotBlank(userInfo)){
				gzuserInfo.setNickname(AppUtils.removeYanText(userInfo.getNickname()));
				gzuserInfo.setSex(userInfo.getSex());
				gzuserInfo.setCity(AppUtils.removeYanText(userInfo.getCity()));
				gzuserInfo.setProvince(AppUtils.removeYanText(userInfo.getProvince()));
				gzuserInfo.setCountry(AppUtils.removeYanText(userInfo.getCountry()));
				gzuserInfo.setHeadimgurl(userInfo.getHeadimgurl());
				gzuserInfo.setBzname(userInfo.getRemark());
				gzuserInfo.setGroupid(userInfo.getGroupid());
			}
			gzuserInfo.setSubscribe("Y");
			gzuserInfo.setSubscribeTime(new Date());
			weixinGzuserInfoService.updateWeixinGzuserInfo(gzuserInfo);
		}else{
			gzuserInfo=new WeixinGzuserInfo();
			String accessToken=weixinTokenService.getAccessToken();
			String openId=subscribeEvent.getFromUserName();
			WeiXinUserInfo userInfo=UsersManageApi.getUserInfo(accessToken, openId);
			if(AppUtils.isNotBlank(userInfo)){
				gzuserInfo.setNickname(AppUtils.removeYanText(userInfo.getNickname()));
				gzuserInfo.setSex(userInfo.getSex());
				gzuserInfo.setCity(AppUtils.removeYanText(userInfo.getCity()));
				gzuserInfo.setProvince(AppUtils.removeYanText(userInfo.getProvince()));
				gzuserInfo.setCountry(AppUtils.removeYanText(userInfo.getCountry()));
				gzuserInfo.setHeadimgurl(userInfo.getHeadimgurl());
				gzuserInfo.setBzname(userInfo.getRemark());
				gzuserInfo.setGroupid(userInfo.getGroupid());
			}
			gzuserInfo.setSubscribe("Y");
			gzuserInfo.setOpenid(subscribeEvent.getFromUserName());
			gzuserInfo.setSubscribeTime(new Date());
			gzuserInfo.setAddtime(new Date());
			weixinGzuserInfoService.saveWeixinGzuserInfo(gzuserInfo);
			
			/**
			 * 发送系统为微信自动注册用户的事件通知 
			 */
			
		}
		
		List<WeixinSubscribe> subscribes=weixinSubscribeDao.getWeixinSubscribe();
		String message=null;
		if(AppUtils.isNotBlank(subscribes)){
			WeixinSubscribe subscribe=subscribes.get(0);
			String resMsgType = subscribe.getMsgType();
		    Long templateId=subscribe.getTemplateId();
		    if (WxConfigUtil.REQUEST_TEXT.equals(resMsgType)) {
		    	TextMessageResp textMessageResp=new TextMessageResp();
				textMessageResp.setToUserName(subscribeEvent.getFromUserName());
				textMessageResp.setFromUserName(subscribeEvent.getToUserName());
				textMessageResp.setCreateTime(new Date().getTime());
				textMessageResp.setMsgType(WxConfigUtil.REQUEST_TEXT);
				textMessageResp.setContent(subscribe.getContent());
				 message= MessageUtil.textMessageToXml(textMessageResp);
		    }else{
		    	 message=weixinMessageService.getMsg(resMsgType,subscribeEvent.getFromUserName(),subscribeEvent.getToUserName(), templateId);
		    }
		}
		if(AppUtils.isBlank(message)){
			TextMessageResp textMessageResp=new TextMessageResp();
			textMessageResp.setToUserName(subscribeEvent.getFromUserName());
			textMessageResp.setFromUserName(subscribeEvent.getToUserName());
			textMessageResp.setCreateTime(new Date().getTime());
			textMessageResp.setMsgType(WxConfigUtil.REQUEST_TEXT);
			textMessageResp.setContent(MessageUtil.getWelCome());
			return MessageUtil.textMessageToXml(textMessageResp);
		}
		return message;
	}
    
}
