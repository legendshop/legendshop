/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinNewstemplate;

/**
 * The Class WeixinNewstemplateDao.
 */

public interface WeixinNewstemplateDao extends Dao<WeixinNewstemplate, Long> {
     
    public abstract List<WeixinNewstemplate> getWeixinNewstemplate(String shopName);

	public abstract WeixinNewstemplate getWeixinNewstemplate(Long id);
	
    public abstract int deleteWeixinNewstemplate(WeixinNewstemplate weixinNewstemplate);
	
	public abstract Long saveWeixinNewstemplate(WeixinNewstemplate weixinNewstemplate);
	
	public abstract int updateWeixinNewstemplate(WeixinNewstemplate weixinNewstemplate);
	
	public abstract PageSupport getWeixinNewstemplate(CriteriaQuery cq);
	
 }
