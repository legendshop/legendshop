package com.legendshop.business.util;


/**
 * 微信模糊匹配度
 * @author tony
 *
 */
public class MatchUtil {

	
	/**
	 * 百分之多少之内匹配错误可以接受 a与ab匹配为百分之50的错误率。
	 * 
	 * @param percent
	 *            设置匹配百分比
	 * @param src
	 *            字符串1
	 * @param dest
	 *            字符串2
	 * @param hander
	 *            匹配规则
	 * @return
	 */
	public static boolean match(double percent, String src, String dest,
			MatchHander hander) {
		char[] csrc = src.toCharArray();
		char[] cdest = dest.toCharArray();
		double score = 0;
		int max = csrc.length > cdest.length ? csrc.length : cdest.length;
		score = cal(csrc, 0, cdest, 0, hander, 0,
				(int) Math.ceil((1 - percent) * max));
		return score / max > percent;
	}
	
	/**
	 * 
	 * @param src
	 * @param dest
	 * @return
	 */
	public static boolean match(double percent, String src, String dest) {
		return match(percent, src, dest, new MatchHander() {

			@Override
			public boolean compare(int a, int b) {
				return a == b;
			}
		});
	}
	

	/**
	 * 几个错误的字符可以接受 a与ab为1个字符错误可以接受
	 * 
	 * @param percent
	 *            设置匹配百分比
	 * @param src
	 *            字符串1
	 * @param dest
	 *            字符串2
	 * @param hander
	 *            匹配规则
	 * @return
	 */
	public static boolean match(int errorNum, String src, String dest,
			MatchHander hander) {
		char[] csrc = src.toCharArray();
		char[] cdest = dest.toCharArray();
		int score = 0;
		score = cal(csrc, 0, cdest, 0, hander, 0, errorNum);
		int max = csrc.length > cdest.length ? csrc.length : cdest.length;
		return max - score <= errorNum;
	}

	
	
	/**
	 * 使用递归方法匹配字符串
	 * 
	 * @param csrc
	 * @param i
	 * @param cdest
	 * @param j
	 * @param hander
	 * @return
	 */
	private static int cal(char[] csrc, int i, char[] cdest, int j,
			MatchHander hander, int curdeep, int maxdeep) {
		int score = 0;
		if (curdeep > maxdeep || i >= csrc.length || j >= cdest.length)
			return 0;
		boolean ismatch = hander.compare(csrc[i], cdest[j]);
		if (ismatch) {
			score++;
			if (i + 1 < csrc.length && j + 1 < cdest.length)
				score += cal(csrc, i + 1, cdest, j + 1, hander, 0, maxdeep);
		} else {
			int temp1 = 0;
			int temp2 = 0;
			int temp3 = 0;
			temp1 += cal(csrc, i, cdest, j + 1, hander, curdeep + 1, maxdeep);
			temp2 += cal(csrc, i + 1, cdest, j, hander, curdeep + 1, maxdeep);
			temp3 += cal(csrc, i + 1, cdest, j + 1, hander, curdeep + 1,
					maxdeep);
			int temp4 = Math.max(temp1, temp2);
			score += Math.max(temp3, temp4);
		}
		return score;
	}

	
	
}
