package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.ExpensesRecordDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.ExpensesRecord;

@Repository
public class ExpensesRecordDaoImpl extends GenericDaoImpl<ExpensesRecord, Long> implements ExpensesRecordDao {

	@Override
	public Long saveExpensesRecord(ExpensesRecord expensesRecord) {
		return save(expensesRecord);
	}

}
