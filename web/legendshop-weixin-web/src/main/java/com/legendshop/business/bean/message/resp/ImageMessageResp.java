package com.legendshop.business.bean.message.resp;

/**
 * 图片消息   [响应用户]

 */
public class ImageMessageResp extends BaseMessage {
	
	private Image Image;

	public Image getImage() {
		return Image;
	}

	public void setImage(Image image) {
		Image = image;
	}
}
