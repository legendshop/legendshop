package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.PdCashLogDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.PdCashLog;

@Repository
public class PdCashLogDaoImpl extends GenericDaoImpl<PdCashLog, Long> implements PdCashLogDao{

	@Override
	public Long savePdCashLog(PdCashLog pdCashLog) {
		return save(pdCashLog);
	}

}
