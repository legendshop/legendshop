package com.legendshop.business.constant;

import com.legendshop.util.constant.StringEnum;


/**
 * 媒体类型
 * @author tony
 *
 */
public enum WeiXinMediaTypeEnum implements StringEnum {

	IMAGE("image"), VOICE("voice"), VIDEO("video"), THUMB("thumb")

	; // 事件点击

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new function enum.
	 * 
	 * @param value
	 *            the value
	 */
	private WeiXinMediaTypeEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}

}
