package com.legendshop.business.api.message;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.util.WeiXinErrcodeUtil;
import com.legendshop.business.bean.media.MediaForMaterialResponse;
import com.legendshop.business.bean.media.OtherMediaForMaterial;
import com.legendshop.business.bean.media.WeiXinMedia;
import com.legendshop.business.bean.media.WeiXinMediaArticle;
import com.legendshop.business.bean.media.WeiXinMediaArticlesRequest;
import com.legendshop.business.bean.media.WeiXinMediaArticlesRespponse;
import com.legendshop.business.bean.media.WeiXinMediaCount;
import com.legendshop.business.bean.media.WeiXinMediaNewItems;
import com.legendshop.business.bean.media.WeiXinMediaNews;
import com.legendshop.business.bean.media.WeiXinUpdateArticle;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.business.util.WxFileContentTypePropertiesUtils;
import com.legendshop.util.AppUtils;
import com.legendshop.util.HttpUtil;
import com.legendshop.util.JSONUtil;
import com.legendshop.web.util.WeiXinUtil;


/**
 * 
 *  ------> 新增永久素材多媒体文件上传下载API
 * @author tony
 *
 */
public class ForeverMediaManageApi {

	
	private static Logger log = LoggerFactory.getLogger(ForeverMediaManageApi.class);
	
	
	/**
	 * 上传新增永久图文素材
	 * 
	 * @param accesstoken
	 * @param wxArticles
	 *            图文集合，数量不大于10
	 * @return WeiXinMedia 上传图文消息素材返回结果
	 * @throws Exception 
	 */
	public static WeiXinMedia uploadArticlesByMaterial(String accesstoken, List<WeiXinMediaArticle> wxArticles) throws Exception{
		WeiXinMedia weixinMedia=new WeiXinMedia();
		if (AppUtils.isBlank(wxArticles)) {
			log.error("没有上传的图文消息");
			weixinMedia.setErrmsg("没有上传的图文消息");
		} else if (wxArticles.size() > 10) {
			log.error("图文消息最多为10个图文");
			weixinMedia.setErrmsg("图文消息最多为10个图文");
		} else {
			 for (int i = 0; i < wxArticles.size(); i++) {
				 WeiXinMediaArticle article=wxArticles.get(i);
				 if (article.getFileName() != null && article.getFileName().length() > 0) {
					 try {
							String mediaId = getFileMediaId(accesstoken, article);
							article.setThumb_media_id(mediaId);

						} catch (Exception e) {
							throw new Exception(e);
						}
				 }
			}
		   String requestUrl = WxConfigUtil.MATERIAL_ADD_NEWS_URL.replace("ACCESS_TOKEN", accesstoken);
		   WeiXinMediaArticlesRequest wxArticlesRequest=new WeiXinMediaArticlesRequest();
		   wxArticlesRequest.setArticles(wxArticles);
		   String content=JSONUtil.getJson(wxArticlesRequest);
		   String result = HttpUtil.httpsRequest(requestUrl, "POST",content); 
		   if(result!=null){
			   JSONObject jsonObject=JSONObject.parseObject(result);
			   if(jsonObject!=null){
				   if(jsonObject.containsKey("errcode")){
					    log.error("上传图文消息失败！errcode=" + jsonObject.getString("errcode") + ",errmsg = " + jsonObject.getString("errmsg"));
					 	weixinMedia.setErrmsg("没有上传的图文消息");
				   }else{
					    weixinMedia.setMedia_id(jsonObject.getString("media_id"));
					    weixinMedia.setType(jsonObject.getString("type"));
					    weixinMedia.setCreated_at(new Date(jsonObject.getLong("created_at") * 1000));
				   }
			   }
		   }
		}
		return weixinMedia;
	}
	
	/**
	 * 新增其他类型永久素材
	 * 
	 * @param filePath
	 * @param fileName
	 * @param type
	 *            媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
	 * @return
	 * @throws Exception
	 */
	public static MediaForMaterialResponse uploadMediaFileByMaterial(String accesstoken, OtherMediaForMaterial otherMedia) {
		MediaForMaterialResponse mediaResource = null;
		String requestUrl = WxConfigUtil.MATERIAL_ADD_MATERIAL_URL.replace(
				"ACCESS_TOKEN", accesstoken);
		File file = new File(otherMedia.getFilePath()
				+ otherMedia.getFileName());
		String contentType = WxFileContentTypePropertiesUtils.getFileContentType(otherMedia.getFileName().substring(otherMedia.getFileName().lastIndexOf(".") + 1));
		String result = WeiXinUtil.uploadMediaByMediaFile(requestUrl, file,contentType);
		if (result != null) {
			JSONObject jsonObject = JSONObject.parseObject(result);
			if ("video" == otherMedia.getType()) {
				JSONObject wx = new JSONObject();
				wx.put("title", otherMedia.getTitle());
				wx.put("introduction", otherMedia.getIntroduction());
				HttpUtil.httpsRequest(requestUrl, "POST", wx.toJSONString());
			}
			if (jsonObject.containsKey("errcode")) {
				log.error("上传媒体资源失败！errcode=" + jsonObject.getString("errcode")
						+ ",errmsg = " + jsonObject.getString("errmsg"));
			} else {
				// {"type":"TYPE","media_id":"MEDIA_ID","created_at":123456789}
				mediaResource = new MediaForMaterialResponse();
				mediaResource.setMedia_id(jsonObject.getString("media_id"));
				mediaResource.setUrl(jsonObject.getString("url"));
			}
		}
		return mediaResource;
	}
	
	
	/**
	 * 获取素材总数
	 * 1.永久素材的总数，也会计算公众平台官网素材管理中的素材
      * 2.图片和图文消息素材（包括单图文和多图文）的总数上限为5000，其他素材的总数上限为1000
	 * @param accesstoken
	 * @return  素材数目返回结果
	 */
	public static WeiXinMediaCount getMediaCount(String accesstoken) {
		WeiXinMediaCount mediaCount = null;
		String requestUrl = WxConfigUtil.MATERIAL_GET_MATERIALCOUNT_URL
				.replace("ACCESS_TOKEN", accesstoken);
		String result = HttpUtil.httpsRequest(requestUrl, "POST", null);
		if (result != null) {
			JSONObject jsonObject = JSONObject.parseObject(result);
			if (jsonObject != null) {
				mediaCount = new WeiXinMediaCount();
				if (jsonObject.containsKey("errcode")) {
					log.error("获取失败图文消息失败！errcode="
							+ jsonObject.getString("errcode") + ",errmsg = "
							+ jsonObject.getString("errmsg"));
					mediaCount.setErrmsg("获取失败图文消息失败！errcode="
							+ jsonObject.getString("errcode") + ",errmsg = "
							+ jsonObject.getString("errmsg"));
				} else {
					mediaCount.setImage_count(jsonObject
							.getString("image_count"));
					mediaCount
							.setNews_count(jsonObject.getString("news_count"));
					mediaCount.setVideo_count(jsonObject
							.getString("video_count"));
					mediaCount.setVoice_count(jsonObject
							.getString("voice_count"));
				}
			}
		}
		return mediaCount;
	}
	
	/**
	 * 获取永久素材
	 * 
	 * @param accesstoken
	 * @param wxArticles
	 *            图文集合，数量不大于10
	 * @return  上传图文消息素材返回结果
	 */
	public static WeiXinMediaArticlesRespponse getArticlesByMaterial(String accesstoken,String mediaId) {
		
		String requestUrl = WxConfigUtil.MATERIAL_GET_MATERIAL_URL.replace("ACCESS_TOKEN", accesstoken);
		
		JSONObject parObject=new JSONObject();
		parObject.put("mediaId", mediaId);
		
		String result = HttpUtil.httpsRequest(requestUrl, "POST", parObject.toJSONString());
		WeiXinMediaArticlesRespponse articlesRespponse =new WeiXinMediaArticlesRespponse();
		if (result != null) {
			JSONObject jsonObject = JSONObject.parseObject(result);
			if (jsonObject != null) {
				if (jsonObject.containsKey("errcode")) {
					log.error("获取失败图文消息失败！errcode="
							+ jsonObject.getString("errcode") + ",errmsg = "
							+ jsonObject.getString("errmsg"));
					articlesRespponse.setErrcode(jsonObject.getInteger("errcode"));
					articlesRespponse.setErrmsg("获取失败图文消息失败！errcode="
							+ jsonObject.getString("errcode") + ",errmsg = "
							+ jsonObject.getString("errmsg"));
					return articlesRespponse;
				} else {
					JSONArray array = jsonObject.getJSONArray("news_item");
					int iSize = array.size();
					List<WeiXinMediaArticle> articles=new ArrayList<WeiXinMediaArticle>(iSize);
     				for (int i = 0; i < iSize; i++) {
     					WeiXinMediaArticle article=array.getObject(i, WeiXinMediaArticle.class);
     					articles.add(article);
     				}
     				articlesRespponse.setNews_item(articles);
				}
			}
		}
		return articlesRespponse;
	}
	
	
	/**
	 * 删除永久素材
	 * 
	 * @param accesstoken
	 * @param mediaId
	 *            图文集合，数量不大于10
	 * @return  上传图文消息素材返回结果
	 * @throws Exception 
	 */
	public static void deleteArticlesByMaterial(String accesstoken,String mediaId) throws Exception
	{
		
		String requestUrl = WxConfigUtil.MATERIAL_DEL_MATERIAL_URL.replace("ACCESS_TOKEN", accesstoken);
		JSONObject jsonObject=new JSONObject();
		jsonObject.put("mediaId", mediaId);
		String result = HttpUtil.httpsRequest(requestUrl, "POST", jsonObject.toJSONString());
		if(result!=null){
			
			JSONObject object=JSONObject.parseObject(result);
			if (object.containsKey("errcode")&&object.getIntValue("errcode")!=0 ) {
				log.error("删除消息失败！errcode=" + object.getIntValue("errcode") + ",errmsg = " + object.getString("errmsg"));
				throw new Exception("删除消息失败！errcode=" + object.getIntValue("errcode") + ",errmsg = " + object.getString("errmsg"));
			}
		}
	}
	
	/**
	 * 修改永久素材
	 * 
	 * @param accesstoken
	 * @param wxUpdateArticle
	 */
	public static void updateArticlesByMaterial(String accesstoken,WeiXinUpdateArticle wxUpdateArticle) throws Exception {
		if (accesstoken != null) {
			String requestUrl = WxConfigUtil.MATERIAL_UPDATE_NEWS_URL.replace("ACCESS_TOKEN", accesstoken);
			String obj=JSONUtil.getJson(wxUpdateArticle);
			String result = HttpUtil.httpsRequest(requestUrl, "POST", obj);
			if(result!=null){
				JSONObject object=JSONObject.parseObject(result);
				if (object.containsKey("errcode")&&object.getIntValue("errcode")!=0 ) {
					log.error("修改永久素材失败！errcode=" + object.getIntValue("errcode") + ",errmsg = " + object.getString("errmsg"));
					throw new Exception("修改永久素材失败！errcode=" + object.getIntValue("errcode") + ",errmsg = " + object.getString("errmsg"));
				}
			}
		}
	}
	
	
	/**
	 * 获取素材列表
	 * 
	 * @param accesstoken,type,offset,count
	 * @param WxNews
	 */
	public static WeiXinMediaNews queryArticlesByMaterial(String accesstoken,String type,int offset,int count) throws Exception {
		WeiXinMediaNews wn = null;
		if (accesstoken != null) {
			String requestUrl = WxConfigUtil.MATERIAL_BATCHGET_MATERIAL_URL.replace("ACCESS_TOKEN", accesstoken);
			JSONObject obj = new JSONObject();
			obj.put("type", type);
			obj.put("offset", offset);
			obj.put("count", count);
			String result = HttpUtil.httpsRequest(requestUrl, "POST", obj.toJSONString());
			
			if(result!=null){
				JSONObject object=JSONObject.parseObject(result);
				if (object.containsKey("errcode")&&object.getIntValue("errcode")!=0 ) {
					log.error("获取素材列表失败！errcode=" + object.getIntValue("errcode") + ",errmsg = " + object.getString("errmsg"));
					throw new Exception("获取素材列表失败！errcode=" + object.getIntValue("errcode") + ",errmsg = " + object.getString("errmsg"));
				}else{
					wn=new WeiXinMediaNews();
					wn.setItem_count(object.getString("item_count"));
					wn.setTotal_count(object.getString("total_count"));
					WeiXinMediaNewItems items=object.getObject("item", WeiXinMediaNewItems.class);
					/*JSONArray array = object.getJSONArray("item");
					int iSize = array.size();
					List<WeiXinMediaNewItems> newItems=new ArrayList<WeiXinMediaNewItems>(iSize);
     				for (int i = 0; i < iSize; i++) {
     					WeiXinMediaNewItems item=array.getObject(i, WeiXinMediaNewItems.class);
     					newItems.add(item);
     				}*/
     				wn.setItem(items);
				}
			}
		}
		return wn;
	}
	
	
	
	/**
	 * 获取文件上传文件的media_id
	 * 
	 * @param accesstoken
	 * @param article
	 * @return
	 * @throws WexinReqException
	 */
	public static String getFileMediaId(String accesstoken, WeiXinMediaArticle article) throws Exception {

		WeiXinMedia response = uploadMediaFile(accesstoken, article.getFilePath(), article.getFileName(), "image");
		if (response != null) {
			return response.getMedia_id();
		}
		throw new Exception("获取文件的media_id失败");

	}
	
	/**
	 * 上传媒体资源
	 * 
	 * @param filePath
	 * @param fileName
	 * @param type
	 *            媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
	 * @return
	 * @throws Exception
	 */
	private static WeiXinMedia uploadMediaFile(String accesstoken, String filePath, String fileName, String type)  {
		WeiXinMedia media = null;
		if (accesstoken != null) {
			String requestUrl = WxConfigUtil.UPLOAD_MEDIA_URL.replace("ACCESS_TOKEN", accesstoken).replace("TYPE", type);
			File file = new File(filePath + fileName);
			String contentType = WxFileContentTypePropertiesUtils.getFileContentType(fileName.substring(fileName.lastIndexOf(".") + 1));
			String  result = WeiXinUtil.uploadMediaByMediaFile(requestUrl, file, contentType);
			if(AppUtils.isNotBlank(result)){
				JSONObject jsonObject=JSONObject.parseObject(result);
				if(jsonObject.containsKey("errcode")){
					Integer errcode=jsonObject.getInteger("errcode");
					String errmsg=WeiXinErrcodeUtil.getErrorMsg(errcode);
					String msg="上传媒体资源失败！errcode=" + errcode + ",errmsg = " +errmsg;
					log.error(msg);
				}
				else {
					// {"type":"TYPE","media_id":"MEDIA_ID","created_at":123456789}
					media = new WeiXinMedia();
					media.setMedia_id(jsonObject.getString("media_id"));
					media.setType(jsonObject.getString("type"));
					media.setCreated_at(new Date(jsonObject.getLongValue("created_at") * 1000));
				}
			}
		}
		return media;
	}
	
}
