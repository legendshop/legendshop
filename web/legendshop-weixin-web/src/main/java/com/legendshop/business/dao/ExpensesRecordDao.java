package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.ExpensesRecord;

public interface ExpensesRecordDao extends Dao<ExpensesRecord, Long>  {

	public abstract Long saveExpensesRecord(ExpensesRecord expensesRecord);
	
}
