package com.legendshop.business.handle.impl;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.legendshop.business.bean.message.req.BaseMessage;
import com.legendshop.business.bean.message.req.ImageMessage;
import com.legendshop.business.handle.MsgTypeState;
import com.legendshop.util.JSONUtil;

@Component("newsMessageHandle")
public class NewsMessageHandle implements MsgTypeState {

	public String handle(HttpServletRequest request,BaseMessage message) {
		
		ImageMessage textMessage=(ImageMessage)message;
		
		System.out.println(JSONUtil.getJson(textMessage));
		 
		return "您发送的是图片消息！";
	}
	

}
