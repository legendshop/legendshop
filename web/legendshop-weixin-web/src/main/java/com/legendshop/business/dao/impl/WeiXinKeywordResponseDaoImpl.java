/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeiXinKeywordResponseDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.weixin.WeiXinKeywordResponse;

/**
 * The Class WeiXinKeywordResponseDaoImpl.
 */
@Repository
public class WeiXinKeywordResponseDaoImpl extends GenericDaoImpl<WeiXinKeywordResponse, Long> implements WeiXinKeywordResponseDao  {
     
    public List<WeiXinKeywordResponse> getWeiXinKeywordResponse(String userName){
   		return this.queryByProperties(new EntityCriterion().eq("userName", userName));
    }

	@Override
	//@Cacheable(value="WeiXinKeywordResponseList")
	public List<WeiXinKeywordResponse> getWeiXinKeywordResponse() {
		return this.queryLimit("select * from ls_weixin_keywordresponse ", WeiXinKeywordResponse.class, 0, 200);
	}

 }
