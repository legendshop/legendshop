/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.constant.WxTokenAccountTypeEnum;
import com.legendshop.business.dao.WeixinTokenDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.weixin.WeixinToken;
import com.legendshop.util.AppUtils;

/**
 * The Class WeixinTokenDaoImpl.
 */

@Repository
public class WeixinTokenDaoImpl extends GenericDaoImpl<WeixinToken, Long> implements WeixinTokenDao  {
     
    public WeixinToken getWeixinToken(String tonkenType){
    	EntityCriterion ec = new EntityCriterion();
    	ec.eq("tonkenType", tonkenType);

    	List<WeixinToken> tokens= this.queryByProperties(ec);
    	if(AppUtils.isNotBlank(tokens)){
    		return tokens.get(0);
    	}
		return null;
    }

	public WeixinToken getWeixinToken(Long id){
		return getById(id);
	}
	
    public int deleteWeixinToken(WeixinToken weixinToken){
    	return delete(weixinToken);
    }
	
	public Long saveWeixinToken(WeixinToken weixinToken){
		return save(weixinToken);
	}
	
	public int updateWeixinToken(WeixinToken weixinToken){
		return update(weixinToken);
	}
	
 }
