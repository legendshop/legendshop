/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.WeixinGzuserInfoDao;
import com.legendshop.business.service.WeixinGzuserInfoService;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;
import com.legendshop.util.AppUtils;

/**
 * The Class WeixinGzuserInfoServiceImpl.
 */
@Service("weixinGzuserInfoService")
public class WeixinGzuserInfoServiceImpl  implements WeixinGzuserInfoService{
	
	@Autowired
    private WeixinGzuserInfoDao weixinGzuserInfoDao;

    public WeixinGzuserInfo getWeixinGzuserInfo(Long id) {
        return weixinGzuserInfoDao.getWeixinGzuserInfo(id);
    }

    public Long saveWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo) {
        if (!AppUtils.isBlank(weixinGzuserInfo.getId())) {
            updateWeixinGzuserInfo(weixinGzuserInfo);
            return weixinGzuserInfo.getId();
        }
        return weixinGzuserInfoDao.saveWeixinGzuserInfo(weixinGzuserInfo);
    }

    public void updateWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo) {
        weixinGzuserInfoDao.updateWeixinGzuserInfo(weixinGzuserInfo);
    }


	@Override
	//@Cacheable(value="WeixinGzuserInfo",key="#openid")
	public WeixinGzuserInfo getWeixinGzuserInfo(String openid) {
		return weixinGzuserInfoDao.getWeixinGzuserInfo(openid);
	}


	@Override
	public void cancelsubscribe(String fromUserName) {
		weixinGzuserInfoDao.cancelsubscribe(fromUserName);
	}


	/*@Override
	@Cacheable(value="WeixinGzuserInfo",key="#userId")
	public WeixinGzuserInfo getWeixinGzuserInfoByUserID(String userId) {
		return weixinGzuserInfoDao.getWeixinGzuserInfoByUserId(userId);
	}*/


	@Override
	public boolean existWeiXinUser(String opendId) {
		Long num= weixinGzuserInfoDao.getLongResult("select count(pass_port_id) from ls_passport where open_id=? and type='weixin' ", opendId);
		return num>0;
	}


	@Override
	public void deleteGzUserInfos() {
		weixinGzuserInfoDao.update("delete from ls_weixin_gzuserinfo");
	}


	@Override
	public void batchUserInfo(List<WeixinGzuserInfo> infos) {
		weixinGzuserInfoDao.save(infos);
	}

}
