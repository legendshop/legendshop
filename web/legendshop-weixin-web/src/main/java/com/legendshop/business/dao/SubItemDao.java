/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.SubItem;

/**
 * The Class SubItemDao.
 */

public interface SubItemDao extends GenericDao<SubItem, Long> {
     
    public abstract List<SubItem> getSubItem(String subNumber,String userId);

	
 }
