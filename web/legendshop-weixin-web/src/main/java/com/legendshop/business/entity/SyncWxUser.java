package com.legendshop.business.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.business.api.message.UsersManageApi;
import com.legendshop.business.bean.user.WeiXinUserInfo;
import com.legendshop.business.bean.user.WeixinUserList;
import com.legendshop.business.service.JobStatusService;
import com.legendshop.business.service.WeixinGzuserInfoService;
import com.legendshop.model.constant.JobStatusEnum;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 微信同步本地用户
 * 
 * @author Tony
 * 
 */
public class SyncWxUser implements Runnable {
	
	private static Logger logger=LoggerFactory.getLogger(SyncWxUser.class);

	private String accesstoken;

	private WeixinGzuserInfoService weixinGzuserInfoService;

	private JobStatusService jobStatusService;

	private String nextOpenId;

	@Override
	public void run() {
		try {
			logger.info("Sync Wx User start..... nextOpenId ={}",nextOpenId);
			WeixinUserList list = UsersManageApi.getAllWxuser(accesstoken,nextOpenId); //10000
			if(logger.isDebugEnabled()){
				logger.debug("Wx User result = {} ",JSONUtil.getJson(list));
			}
			if (AppUtils.isBlank(list) || AppUtils.isBlank(list.getUserInfos()) ) {
				return;
			}
			// 移除所有的微信用户
			weixinGzuserInfoService.deleteGzUserInfos();
			boolean flag=true;
			while(flag){
				if (AppUtils.isBlank(list) || AppUtils.isBlank(list.getUserInfos()) ) {
					break;
				}
				flag=list.getCount()>=10000;
				batchUserInfo(list);
				if(flag){
					list = UsersManageApi.getAllWxuser(accesstoken,list.getNextOpenId()); //10000
				}
			}
		} catch (Exception e) {
			logger.error("Sync WxUser error by {} ",e);
		} finally {
			jobStatusService.deleteJobStatus(JobStatusEnum.SYNCHRONOUS_USERINFO.name());
			logger.info("Sync Wx User success ");
		}
	}
	
	
    private void batchUserInfo(WeixinUserList list){
		List<WeiXinUserInfo> userInfos = list.getUserInfos();
		int init = 100;// 每隔100条循环一次
		int total = userInfos.size();
		/**
		 * 批量入库
		 */
		int cycelTotal = total / init;
		if (total % init != 0) {
			cycelTotal += 1;
			if (total < init) {
				init = userInfos.size();
			}
		}
		int index = 0;
		for (int i = 0; i < cycelTotal; i++) {
			List<WeixinGzuserInfo> infos = new ArrayList<WeixinGzuserInfo>();
			for (int j = 0; j < init; j++) {
				if (index >= total) {
					break;
				}
				WeiXinUserInfo user = userInfos.get(index);
				if (user == null) {
					break;
				}
				WeixinGzuserInfo info = build(user);
				infos.add(info);
				index = index + 1;
			}
			weixinGzuserInfoService.batchUserInfo(infos);
			logger.info("Sync Wx User put in storage cycelTotal={}",cycelTotal);
			infos = null;
		}
		list=null;
	 }
 	

	private WeixinGzuserInfo build(WeiXinUserInfo dtoj) {
		WeixinGzuserInfo gzuser = new WeixinGzuserInfo();
		if (dtoj.getSubscribe() != null && dtoj.getSubscribe() == 1) {
			gzuser.setSubscribe("Y");
		} else {
			gzuser.setSubscribe("N");
		}
		gzuser.setOpenid(dtoj.getOpenid());
		if (dtoj.getNickname() != null && dtoj.getNickname() != "") {
			StringBuilder nicksb = new StringBuilder();
			int l = dtoj.getNickname().length();
			for (int i = 0; i < l; i++) {
				char _s = dtoj.getNickname().charAt(i);
				if (isEmojiCharacter(_s)) {
					nicksb.append(_s);
				}
			}
			gzuser.setNickname(nicksb.toString());
		}
		gzuser.setSex(dtoj.getSex());
		gzuser.setCity(dtoj.getCity());
		gzuser.setProvince(dtoj.getProvince());
		gzuser.setCountry(dtoj.getCountry());
		gzuser.setGroupid(dtoj.getGroupid());
		gzuser.setHeadimgurl(dtoj.getHeadimgurl());
		gzuser.setBzname(dtoj.getRemark());
		gzuser.setSubscribeTime(new Date(Long.valueOf(dtoj.getSubscribe_time())));
		gzuser.setAddtime(new Date());
		dtoj = null;
		return gzuser;
	}

	private boolean isEmojiCharacter(char codePoint) {
		return (codePoint == 0x0) ||

		(codePoint == 0x9) ||

		(codePoint == 0xA) ||

		(codePoint == 0xD) ||

		((codePoint >= 0x20) && (codePoint <= 0xD7FF)) ||

		((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) ||

		((codePoint >= 0x10000) && (codePoint <= 0x10FFFF));

	}

	public String getAccesstoken() {
		return accesstoken;
	}

	public void setAccesstoken(String accesstoken) {
		this.accesstoken = accesstoken;
	}

	public void setWeixinGzuserInfoService(
			WeixinGzuserInfoService weixinGzuserInfoService) {
		this.weixinGzuserInfoService = weixinGzuserInfoService;
	}

	public void setJobStatusService(JobStatusService jobStatusService) {
		this.jobStatusService = jobStatusService;
	}

	public void setNextOpenId(String nextOpenId) {
		this.nextOpenId = nextOpenId;
	}
}
