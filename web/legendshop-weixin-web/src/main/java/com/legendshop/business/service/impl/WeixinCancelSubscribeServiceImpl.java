package com.legendshop.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.service.WeixinCancelSubscribeService;
import com.legendshop.business.service.WeixinGzuserInfoService;
import com.legendshop.business.util.WeiXinLog;

@Service("weixinCancelSubscribeService")
public class WeixinCancelSubscribeServiceImpl implements WeixinCancelSubscribeService {
	
	@Autowired
    private WeixinGzuserInfoService weixinGzuserInfoService;

	@Override
	public String cancelsubscribeResponse(String fromUserName) {
		weixinGzuserInfoService.cancelsubscribe(fromUserName);
		WeiXinLog.info(" fromusername {} cancle subscribe weixin ",fromUserName);
		return null;
	}

}
