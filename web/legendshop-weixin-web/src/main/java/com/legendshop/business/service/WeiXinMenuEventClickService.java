package com.legendshop.business.service;

import javax.servlet.http.HttpServletRequest;

import com.legendshop.business.bean.message.event.MenuEvent;

public interface WeiXinMenuEventClickService {

	public abstract String getMenuEventClick(HttpServletRequest request,MenuEvent menuEvent);
}
