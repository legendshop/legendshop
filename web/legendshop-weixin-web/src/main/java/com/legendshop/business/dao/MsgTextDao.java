package com.legendshop.business.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.MsgText;

public interface MsgTextDao extends GenericDao<MsgText, Long> {

}
