package com.legendshop.business.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.config.dto.WeixinPropertiesDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.business.api.message.UsersManageApi;
import com.legendshop.business.bean.user.WebAuthInfo;
import com.legendshop.business.bean.user.WeiXinUserInfo;
import com.legendshop.business.service.WeiXinSubService;
import com.legendshop.business.service.WeixinGzuserInfoService;
import com.legendshop.business.util.WeiXinLog;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.business.util.WxHttpUtil;
import com.legendshop.business.util.WxSignUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.web.util.WeiXinUtil;


/**
 * 微信网站网页授权
 */
@Controller
public class WeiXinWebOauthController {
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(WeiXinWebOauthController.class);
	

	private static Map<String, Lock> locks=new ConcurrentHashMap<String, Lock>(500);
	

	@Autowired
	private WeixinGzuserInfoService weixinGzuserInfoService;
	
	@Autowired
	private WeiXinSubService weiXinSubService;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Autowired
	private WeixinPropertiesDto weixinPropertiesDto;
	
	
	 @RequestMapping(value = "/weixin/web/oauth",method = RequestMethod.GET)
	 public String weixinCallback(HttpServletRequest request, HttpServletResponse response,
				@RequestParam String code, //code作为换取access_token的票据，每次用户授权带上的code将不一样，code只能使用一次，5分钟未被使用自动过期。
				@RequestParam String state,  //重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
				@RequestParam String oauth2url, //weixin mobile oauth callback回调地址
				@RequestParam String time, // weixin mobile response validate time
				@RequestParam String validateCode // weixin mobile response validateCode
			 ) throws Exception {
		 
		 
		 	System.out.println("<!---- 微信回调回来啦 code="+code);
		 
			//检查有效性
			if(!checkSignature(oauth2url, time, validateCode)){
				request.setAttribute("message", "非法访问");
				return "/systemerror" ; 
			}
			
			//code不为空则代表是从微信调整过来的
			if(AppUtils.isBlank(code)){
				log.error("weixin oauth2 access_token error  code  is null ");
				request.setAttribute("message", "微信网页授权失败！");
				return "/systemerror" ; 
			}
			
			String appId = systemParameterUtil.getWeixinLoginAppId();
			String AppSecret = systemParameterUtil.getWeixinLoginAppsecret();
			
			String urlString = WxConfigUtil.OAUTH2_URL.replaceAll("APPID", appId);
			urlString = urlString.replaceAll("SECRET",AppSecret);
			urlString = urlString.replaceAll("CODE", code);
		    String text=WxHttpUtil.httpsRequest(urlString,"GET",null);
		    log.info("weixin oauth 返回参数=======>>>>>{}",text);
		    if(AppUtils.isNotBlank(text)){
		    	WebAuthInfo authInfo=JSONUtil.getObject(text, WebAuthInfo.class);
		    	if(authInfo.getErrcode()==null && authInfo.getOpenid() !=null){
		    		String openId=authInfo.getOpenid();
	 				//callback
	 				 Lock lock= locks.get(openId);
	 				 if(lock==null){
	 		        	lock = new ReentrantLock(false);
	 		        	locks.put(openId,lock);
	 		         }
	 				 lock.lock();//锁一下
						try {
							// 授权成功
							WeixinGzuserInfo weixinGzuserInfo = weixinGzuserInfoService.getWeixinGzuserInfo(openId);// 一旦关注就会有该用户存在
							if (AppUtils.isBlank(weixinGzuserInfo)) {
								WeiXinUserInfo weiXinUserInfo = UsersManageApi.snsCallBackUserInfo(authInfo.getAccess_token(), openId);
								weixinGzuserInfo = new WeixinGzuserInfo();
								weixinGzuserInfo.setSubscribe("N");
								weixinGzuserInfo.setOpenid(openId);
								weixinGzuserInfo.setSubscribeTime(new Date());
								weixinGzuserInfo.setAddtime(new Date());
								if (weiXinUserInfo != null) {
									weixinGzuserInfo.setNickname(weiXinUserInfo.getNickname());
									weixinGzuserInfo.setProvince(weiXinUserInfo.getProvince());
									weixinGzuserInfo.setCity(weiXinUserInfo.getCity());
									weixinGzuserInfo.setCountry(weiXinUserInfo.getCountry());
									weixinGzuserInfo.setHeadimgurl(weiXinUserInfo.getHeadimgurl());
									weixinGzuserInfo.setSex(weiXinUserInfo.getSex());
								}
								weixinGzuserInfoService.saveWeixinGzuserInfo(weixinGzuserInfo);
							}
						} catch (Exception e) {
							e.printStackTrace();
						} finally {
							locks.remove(openId);
							lock.unlock();
						}
						
						 //callback
		 				 StringBuilder sb = new StringBuilder("redirect:");
		 				 sb.append(oauth2url)
		 				.append("?openId=")
		 				.append(openId)
		 				.append("&oauth2url=").append(URLEncoder.encode(oauth2url, "UTF-8"))
		 				.append("&time=").append(time)
		 				.append("&validateCode=").append(URLEncoder.encode(validateCode, "UTF-8"));
						/*.append("&loginToRedirectUrl=").append(loginToRedirectUrl);*/
		 				 
		 				 System.out.println(sb.toString());
		 				 
		 				 return sb.toString(); 
		    	}
		    	else{
					log.error("weixin oauth2 access_token error by authInfo inf  ={} ",JSONUtil.getJson(authInfo));
			     }
			 }
		    request.setAttribute("message", "微信网页授权失败！");
		    return "/systemerror" ; 
	 }
	
	
	/**
	 *  用户同意授权后 跳转回调redirect_uri
	 *  通过code换取网页授权access_token和openId
	 * @param request
	 * @param response
	 * @param code
	 * @param state
	 * @param oauth2url
	 * @param time
	 * @param validateCode
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/weixin/webOauth", method = RequestMethod.GET)
	public String weixinOauth(HttpServletRequest request,
			HttpServletResponse response,
			@RequestParam String code, //code作为换取access_token的票据，每次用户授权带上的code将不一样，code只能使用一次，5分钟未被使用自动过期。
			@RequestParam String state,  //重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
			@RequestParam String oauth2url, //weixin mobile oauth callback回调地址
			@RequestParam String time, // weixin mobile response validate time
			@RequestParam String validateCode  // weixin mobile response validateCode
			)
			throws IOException {
		// 判断是否微信环境, 5.0 之后的支持微信支付
		boolean isweixin = WeiXinUtil.isWeiXin(request);
		 if (!isweixin) {
			return "/error" ; 
		 }
		//检查有效性
		if(!checkSignature(oauth2url, time, validateCode)){
			request.setAttribute("message", "非法访问");
			return "/systemerror" ; 
		}
		//code不为空则代表是从微信调整过来的
		if(AppUtils.isBlank(code)){
			log.error("weixin oauth2 access_token error  code  is null ");
			request.setAttribute("message", "微信网页授权失败！");
			return "/systemerror" ; 
		}
	    PayType payType = weiXinSubService.getPayTypeById(PayTypeEnum.WX_PAY.value());
		PaymentLog.log("############# WechatController 支付参数  {} ########## ", JSONUtil.getJson(payType));
		if (payType == null) {
			WeiXinLog.info(" WechatController 支付方式不存在,请设置支付方式! by PayTypeId={}", PayTypeEnum.WX_PAY.value());
		} else if (payType.getIsEnable() == Constants.OFFLINE) {
			WeiXinLog.info("WechatController 该支付方式没有启用! by PayTypeById={} ", PayTypeEnum.WX_PAY.value());
		}
		JSONObject jsonObject = JSONObject.parseObject(payType.getPaymentConfig());
			
		String urlString="https://api.weixin.qq.com/sns/oauth2/access_token?appid="+jsonObject.getString(WxConfigUtil.APPID)  + "&secret=" + jsonObject.getString(WxConfigUtil.APPSECRET) +"&code=" + code + "&grant_type=authorization_code";
	    String text=WxHttpUtil.httpsRequest(urlString,"GET",null);
 		if(AppUtils.isNotBlank(text)){
 			WebAuthInfo authInfo=JSONUtil.getObject(text, WebAuthInfo.class);
 			if(authInfo.getErrcode()==null && authInfo.getOpenid() !=null){
 				String openId=authInfo.getOpenid();
 				//callback
 				 Lock lock= locks.get(openId);
 				 if(lock==null){
 		        	lock = new ReentrantLock(false);
 		        	locks.put(openId,lock);
 		         }
 				 lock.lock();//锁一下
 				
 				 try {
 					 //授权成功
 	 				WeixinGzuserInfo weixinGzuserInfo= weixinGzuserInfoService.getWeixinGzuserInfo(openId);//一旦关注就会有该用户存在
 	 				if(AppUtils.isBlank(weixinGzuserInfo)){
 	 					WeiXinUserInfo weiXinUserInfo=UsersManageApi.snsCallBackUserInfo(authInfo.getAccess_token(), openId);
 	 					weixinGzuserInfo=new WeixinGzuserInfo();
 	 					weixinGzuserInfo.setSubscribe("N");
 	 					weixinGzuserInfo.setOpenid(openId);
 	 					weixinGzuserInfo.setSubscribeTime(new Date());
 	 					weixinGzuserInfo.setAddtime(new Date());
 	 					if(weiXinUserInfo!=null){
 							weixinGzuserInfo.setNickname(weiXinUserInfo.getNickname());
 							weixinGzuserInfo.setProvince(weiXinUserInfo.getProvince());
 							weixinGzuserInfo.setCity(weiXinUserInfo.getCity());
 							weixinGzuserInfo.setCountry(weiXinUserInfo.getCountry());
 							weixinGzuserInfo.setHeadimgurl(weiXinUserInfo.getHeadimgurl());
 							weixinGzuserInfo.setSex(weiXinUserInfo.getSex());
 	 					}
 	 					weixinGzuserInfoService.saveWeixinGzuserInfo(weixinGzuserInfo);
 	 				 }
				 } catch (Exception e) {
					 e.printStackTrace();
				 }finally{
					locks.remove(openId);
				    lock.unlock();
				 }
 				 //callback
 				 StringBuilder sb = new StringBuilder("redirect:");
 				 sb.append(oauth2url)
 				.append("?openId=")
 				.append(openId)
 				.append("&oauth2url=").append(URLEncoder.encode(oauth2url, "UTF-8"))
 				.append("&time=").append(time)
 				.append("&validateCode=").append(URLEncoder.encode(validateCode, "UTF-8"));
 				return sb.toString(); 
 			}else{
 				log.error("weixin oauth2 access_token error by authInfo inf  ={} ",JSONUtil.getJson(authInfo));
 			}
 		}
 		request.setAttribute("message", "微信网页授权失败！");
		return "/systemerror" ; 
	}
	
	
	/**
	 * 检查调用者的有效性
	 * @param oauth2url
	 * @param time
	 * @param validateCode
	 * @return
	 */
	private boolean checkSignature(String oauth2url, String time, String validateCode){
		return WxSignUtil.checkSignature(oauth2url, validateCode, time, weixinPropertiesDto.getWeixinKey());
	}
	
}
