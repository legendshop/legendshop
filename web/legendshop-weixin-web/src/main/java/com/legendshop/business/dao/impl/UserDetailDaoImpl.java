/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import org.springframework.stereotype.Repository;

import com.legendshop.business.cache.UserDetailUpdate;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.business.util.WeiXinLog;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.UserDetail;

@Repository
public class UserDetailDaoImpl extends GenericDaoImpl<UserDetail, String> implements UserDetailDao {
	
	@Override
	public UserDetail getUserDetailById(String userId) {
		return this.getByProperties(new EntityCriterion().eq("userId", userId));
	}

	@Override
	@UserDetailUpdate
	public int updateAvailablePredeposit(Double updatePredeposit,Double originalPredeposit, String userId) {
		int result = update("update ls_usr_detail set available_predeposit=? where user_id=? and available_predeposit=? ", new Object[]{updatePredeposit,userId,originalPredeposit});
		 WeiXinLog.log("Update predeposit status : {},  userId: {},  originalPredeposit : {}, updatePredeposit: {} ",result, userId ,originalPredeposit, updatePredeposit);
		return result;
	}

	@Override
	@UserDetailUpdate
	public int updateUserCoin(Double updateUserCoin,Double originalUserCoin, String userId) {
		int result = update("update ls_usr_detail set user_coin=? where user_id=? and user_coin=? ", new Object[]{updateUserCoin,userId,originalUserCoin});
		WeiXinLog.log("Update coin status : {},  userId: {},  originalPredeposit : {}, updatePredeposit: {} ",result, userId ,originalUserCoin, updateUserCoin);
		return result;
	}
	
	
}
