/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinNewstemplateDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinNewstemplate;

/**
 * The Class WeixinNewstemplateDaoImpl.
 */

@Repository
public class WeixinNewstemplateDaoImpl extends GenericDaoImpl<WeixinNewstemplate, Long> implements WeixinNewstemplateDao  {
     
    public List<WeixinNewstemplate> getWeixinNewstemplate(String userName){
   		return this.queryByProperties(new EntityCriterion().eq("userName", userName));
    }

	public WeixinNewstemplate getWeixinNewstemplate(Long id){
		return getById(id);
	}
	
    public int deleteWeixinNewstemplate(WeixinNewstemplate weixinNewstemplate){
    	return delete(weixinNewstemplate);
    }
	
	public Long saveWeixinNewstemplate(WeixinNewstemplate weixinNewstemplate){
		return save(weixinNewstemplate);
	}
	
	public int updateWeixinNewstemplate(WeixinNewstemplate weixinNewstemplate){
		return update(weixinNewstemplate);
	}
	
	public PageSupport getWeixinNewstemplate(CriteriaQuery cq){
		return queryPage(cq);
	}
	
 }
