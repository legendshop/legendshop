package com.legendshop.business.service.impl;

import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.bean.message.event.MenuEvent;
import com.legendshop.business.bean.message.event.SubscribeEvent;
import com.legendshop.business.bean.message.req.BaseMessage;
import com.legendshop.business.bean.message.req.TextMessage;
import com.legendshop.business.bean.message.resp.TextMessageResp;
import com.legendshop.business.handle.MsgTypeState;
import com.legendshop.business.service.WechatService;
import com.legendshop.business.service.WeiXinMenuEventClickService;
import com.legendshop.business.service.WeixinCancelSubscribeService;
import com.legendshop.business.service.WeixinSubscribeService;
import com.legendshop.business.util.WeiXinLog;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.web.util.WeiXinUtil;

@Service("wechatService")
public class WechatServiceImpl implements WechatService {
	
	@Resource(name="textMessageHandle")
	private MsgTypeState textMessageHandle;  //文本消息处理
	
	@Autowired
	private WeixinSubscribeService weixinSubscribeService;
	
	@Autowired
	private WeiXinMenuEventClickService weiXinMenuEventClickService;
	
	@Autowired
	private WeixinCancelSubscribeService weixinCancelSubscribeService;
	

	public String wxCoreService(HttpServletRequest request) {
		String respContent = "请求处理异常，请稍候尝试";
		try {
			// 默认返回的文本消息内容
			Map<String, String> requestMap = WeiXinUtil.parseXml(request);
			// 发送方帐号（open_id）来源用户
			String fromUserName = requestMap.get("FromUserName");
			// 公众帐号
			String toUserName = requestMap.get("ToUserName");
			// 消息类型
			String msgType = requestMap.get("MsgType");
			
			String msgId = requestMap.get("MsgId");
			
			String content = requestMap.get("Content");
			//消息内容
			WeiXinLog.info("------------微信客户端发送请求---------------------   |   fromUserName:"+fromUserName+"   |   ToUserName:"+toUserName+"   |   msgType:"+msgType+"   |   msgId:"+msgId+"   |   content:"+content);
			//根据微信ID,获取配置的全局的数据权限ID
            if(WxConfigUtil.RECRIVE_TEXT.equals(msgType)){
            	WeiXinLog.info("------------微信客户端发送请求------------------【微信触发类型】文本消息---");
            	TextMessage textMessage = new TextMessage();
                textMessage.setToUserName(fromUserName);
                textMessage.setFromUserName(toUserName);
                textMessage.setCreateTime(new Date().getTime());
                textMessage.setMsgType(WxConfigUtil.REQUEST_TEXT);
                textMessage.setContent(content);
                BaseMessage message=(BaseMessage)textMessage;
                respContent= textMessageHandle.handle(request,message);
            }else if(WxConfigUtil.RECRIVE_IMAGE.equals(msgType)){
            	WeiXinLog.info("------------微信客户端发送请求------------------【微信触发类型】图片消息---");
            	TextMessageResp textMessageResp=new TextMessageResp();
				textMessageResp.setToUserName(fromUserName);
				textMessageResp.setFromUserName(toUserName);
				textMessageResp.setCreateTime(new Date().getTime());
				textMessageResp.setMsgType(WxConfigUtil.REQUEST_TEXT);
				textMessageResp.setContent("暂时不提供图片服务");
				//return MessageUtil.textMessageToXml(textMessageResp); //暂时去掉，不要回复
				return "";//不要回复
            	/*ImageMessage imageMessage = new ImageMessage();
            	imageMessage.setToUserName(fromUserName);
            	imageMessage.setFromUserName(toUserName);
            	imageMessage.setCreateTime(new Date().getTime());
            	imageMessage.setMsgType(WxConfigUtil.REQUEST_IMAGE);
            	imageMessage.setPicUrl(requestMap.get("picUrl"));
                BaseMessage message=(BaseMessage)imageMessage;
                respContent=newsMessageHandle.handle(request,message);*/
            }else if(msgType.equals(WxConfigUtil.RECRIVE_EVENT)){
            	 String eventType = requestMap.get("Event");
            	 WeiXinLog.info("------------微信客户端发送请求------------------【微信触发类型】事件推送--- EventType"+eventType);
                 if (eventType.equals(WxConfigUtil.EVENT_SUBSCRIBE)) {
                	  SubscribeEvent subscribeEvent=new SubscribeEvent();
                	  subscribeEvent.setCreateTime(new Date().getTime());
                	  subscribeEvent.setEvent(eventType);
                	  subscribeEvent.setFromUserName(fromUserName);
                	  subscribeEvent.setToUserName(toUserName);
                	  subscribeEvent.setMsgType(msgType);
                	  respContent=weixinSubscribeService.subscribeResponse(subscribeEvent);
                	  request.getSession().setAttribute("openid", subscribeEvent.getFromUserName());
                	  System.out.println("subscribe message "+respContent);
                 }
                 // 取消订阅
 				else if (eventType.equals(WxConfigUtil.EVENT_UNSUBSCRIBE)) {
 					
 					weixinCancelSubscribeService.cancelsubscribeResponse(fromUserName);
 					
 					// TODO 取消订阅后用户再收不到公众号发送的消息，因此不需要回复消息
 				}else if(WxConfigUtil.EVENT_LOCATION.equals(eventType)){
 				// TODO 事件类型：LOCATION(上报地理位置事件)
 				}else if(WxConfigUtil.EVENT_CLICK.equals(eventType)){
 					MenuEvent menuEvent=new MenuEvent();
 					menuEvent.setCreateTime(new Date().getTime());
 					menuEvent.setEvent(eventType);
 					menuEvent.setFromUserName(fromUserName);
 					menuEvent.setToUserName(toUserName);
 					menuEvent.setMsgType(msgType);
 					String EventKey = requestMap.get("EventKey"); //事件KEY值，与自定义菜单接口中KEY值对应
 					menuEvent.setEventKey(EventKey);
 					respContent=weiXinMenuEventClickService.getMenuEventClick(request,menuEvent);
 				}
            }
		}  catch (Exception e) {
			e.printStackTrace();
		}
		return respContent;
	}
	
}
