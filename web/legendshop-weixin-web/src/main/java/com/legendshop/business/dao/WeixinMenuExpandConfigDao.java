/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.weixin.WeixinMenuExpandConfig;

/**
 * The Class WeixinMenuExpandConfigDao.
 */

public interface WeixinMenuExpandConfigDao extends Dao<WeixinMenuExpandConfig, Long> {
     
    public abstract List<WeixinMenuExpandConfig> getWeixinMenuExpandConfig(String shopName);

	public abstract WeixinMenuExpandConfig getWeixinMenuExpandConfig(Long id);
	
    public abstract int deleteWeixinMenuExpandConfig(WeixinMenuExpandConfig weixinMenuExpandConfig);
	
	public abstract Long saveWeixinMenuExpandConfig(WeixinMenuExpandConfig weixinMenuExpandConfig);
	
	public abstract int updateWeixinMenuExpandConfig(WeixinMenuExpandConfig weixinMenuExpandConfig);
	
	public abstract List<WeixinMenuExpandConfig> getWeixinMenuExpandConfig();
	
 }
