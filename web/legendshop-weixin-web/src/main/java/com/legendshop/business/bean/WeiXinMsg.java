package com.legendshop.business.bean;

/**
 * {"errcode": 0, "errmsg": "ok"}
 * {"errcode":40013,"errmsg":"invalid appid"}
 * @author tony
 *
 */
public class WeiXinMsg {
	
	public static final int SUCCESS = 0;

	private Integer errcode;
	
	private String errmsg;

	public Integer getErrcode() {
		return errcode;
	}

	public void setErrcode(Integer errcode) {
		this.errcode = errcode;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	public boolean isSuccess(){
		return errcode != null && errcode  == SUCCESS;
	}
}
