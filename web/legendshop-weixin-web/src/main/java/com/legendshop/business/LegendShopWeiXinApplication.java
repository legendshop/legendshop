package com.legendshop.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ImportResource;


/**
 * 系统启动类.
 */
@EnableCaching
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class,
        RedisAutoConfiguration.class, AopAutoConfiguration.class,
        AopAutoConfiguration.class }, scanBasePackages = { "com.legendshop"})
@ImportResource({ "classpath:spring/applicationContext.xml" })
public class LegendShopWeiXinApplication {


    /** 日志. */
    private final static Logger log = LoggerFactory.getLogger(LegendShopWeiXinApplication.class);

    /**
     * 主方法
     */
    public static void main(String[] args) {
        SpringApplication.run(LegendShopWeiXinApplication.class, args);
        // 将hook线程添加到运行时环境中去
        Runtime.getRuntime().addShutdownHook(new CleanWorkThread());
    }

    /**
     * hook线程,关闭时调用
     */
    static class CleanWorkThread extends Thread {
        @Override
        public void run() {
            log.info(" LegendShop Frontend System shutdowned.");
        }
    }

}
