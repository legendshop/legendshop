package com.legendshop.business.event;

import com.legendshop.framework.event.BaseEventId;

public enum WeiXinEventId  implements BaseEventId{

	// 
	RECEIVE_TEXT_EVENT("RECEIVE_TEXT_EVENT");
	
	/** The value. */
	private final String value;

	public String getEventId() {
		return this.value;
	}

	private WeiXinEventId(String value) {
		this.value = value;
	}

	public boolean instance(String name) {
		WeiXinEventId[] eventIds = values();
		for (WeiXinEventId eventId : eventIds) {
			if (eventId.name().equals(name)) {
				return true;
			}
		}
		return false;
	}


}
