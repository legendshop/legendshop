package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.exception.ErrorCodes;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.business.dao.CoinLogDao;
import com.legendshop.business.dao.ExpensesRecordDao;
import com.legendshop.business.dao.PdCashLogDao;
import com.legendshop.business.dao.SubDao;
import com.legendshop.business.dao.SubHistoryDao;
import com.legendshop.business.dao.SubItemDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.business.service.WeiXinSubService;
import com.legendshop.business.util.WeiXinLog;
import com.legendshop.model.constant.BiddersWinStatusEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.PdCashLogEnum;
import com.legendshop.model.constant.PreDepositErrorEnum;
import com.legendshop.model.constant.PrePayTypeEnum;
import com.legendshop.model.constant.StockCountingEnum;
import com.legendshop.model.constant.SubHistoryEnum;
import com.legendshop.model.constant.SubTypeEnum;
import com.legendshop.model.dto.PredepositPaySuccess;
import com.legendshop.model.entity.ExpensesRecord;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.model.entity.PdRecharge;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubHistory;
import com.legendshop.model.entity.SubItem;
import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.entity.SubSettlementItem;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.coin.CoinLog;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import com.legendshop.util.JSONUtil;

/**
 * TODO  微信订单服务 残留历史问题,需要把 legendshop-weixin-server 去掉
 */
@Service("weiXinSubService")
public class WeiXinSubServiceImpl implements WeiXinSubService {

	/** The sub dao. */
	@Autowired
	private SubDao subDao;
	
	@Autowired
	private SubHistoryDao subHistoryDao;
	
	@Autowired
	private SubItemDao subItemDao;
	
	@Autowired
	private UserDetailDao userDetailDao;
	
	@Autowired
	private ExpensesRecordDao expensesRecordDao;
	
	@Autowired
	private PdCashLogDao pdCashLogDao;
	
	@Autowired
	private CoinLogDao coinLogDao;

	@Override
	public SubSettlement getSubSettlement(String out_trade_no) {
		return subDao.getSubSettlement(out_trade_no);
	}

	/**
	 * 微信支付回调, 已过时， 支付callback都在payment工程处理
	 */
	@Deprecated
	@Override
	public String doBankCallback(SubSettlement subSettlement) {
		if (subSettlement.getIsClearing()) { // 没有清算
			return Constants.SUCCESS;
		}
		
		 //没有付款
		PaymentLog.info("###################### doBankCallback subSettlementSn={} 开始处理 订单支付业务 ########################",
				subSettlement.getSubSettlementSn());
		Date now = new Date();
		subSettlement.setClearingTime(now);
		subSettlement.setIsClearing(true);
		int settlementResult = subDao.updateSubSettlementForPay(subSettlement);
		if (settlementResult == 0) {
			WeiXinLog.info("Update SubSettlement failed, FlowTradeNo is {}, subSettlementId is {}",
					subSettlement.getFlowTradeNo(), subSettlement.getSubSettlementId());
			return Constants.FAIL;
		}
		
		List<SubSettlementItem> subSettlementItems = subDao.getSubSettlementItems(subSettlement.getSubSettlementSn());

		if (subSettlementItems == null) {
			PaymentLog.error("支付异常,订单缺失, doBankCallback error subs is null by subSettlementSn={}",
					subSettlement.getSubSettlementSn());
			throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
		}

		List<String> subNumbers = new ArrayList<String>();
		for (SubSettlementItem settlementItem : subSettlementItems) {
			subNumbers.add(settlementItem.getSubNumber());
		}
		  
		// 第二步更新Sub订单动作
		PaymentLog.info("############################## 处理订单信息{} ###########################", subNumbers);
		List<Sub> subs = subDao.getSubBySubNumberByUserId(subNumbers, subSettlement.getUserId());
		if (AppUtils.isBlank(subs)) {
			PaymentLog.error("支付异常,订单缺失, doBankCallback error subs is null by subNumbers={}", subNumbers);
			throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
		}
		
		if(subSettlement.getPdAmount()>0){ //部分使用钱包
		    PaymentLog.info("############################## 使用部分的钱包支付 ###########################");
			PredepositPaySuccess paySuccess = new PredepositPaySuccess(subSettlement.getSubSettlementSn(), subSettlement.getPdAmount(), subSettlement.getUserId(),subSettlement.getPayPctType());
			// predepositPaySuccessProcessor.process(paySuccess);
			String result = paySuccess.getResult();
			if (!PreDepositErrorEnum.OK.value().equals(result)) {
				PaymentLog.error("OrderCallbackStrategyImpl callback  fail 预付款支付失败  pay result={} ", result);
				throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
			}
	    }
		
		 for (Sub sub : subs) {
				PaymentLog.info("###################### 开始 处理 sub by subNumber={} ########################",sub.getSubNumber());
				sub.setPayed(true);// 已经支付成功
				sub.setPayDate(now);
				sub.setPayTypeId(subSettlement.getPayTypeId());
				sub.setPayTypeName(subSettlement.getPayTypeName());
				sub.setStatus(OrderStatusEnum.PADYED.value());
				sub.setFlowTradeNo(subSettlement.getFlowTradeNo());
				sub.setSubSettlementSn(subSettlement.getSubSettlementSn());
				int result = subDao.updateSubForPay(sub); // 更新为支付状态
				if(result==0){
					PaymentLog.error(" 更新订单为支付状态失败, by subNumber ={},", sub.getSubNumber());
					throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
				}
				PaymentLog.info("###################### sub by subNumber={} 处理支付成功 ########################",sub.getSubNumber());
				List<SubItem> items = subItemDao.getSubItem(sub.getSubNumber(), sub.getUserId());
				if(AppUtils.isBlank(items)){
					PaymentLog.error(" 更新订单为支付状态失败, by subNumber ={},", sub.getSubNumber());
					throw new BusinessException("提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
				}
				
				String firstDistUserName = null; 
				Map<Long, Long> map=new HashMap<Long, Long>();
				for (Iterator<SubItem> iterator = items.iterator(); iterator.hasNext();) {
					SubItem subItem = (SubItem) iterator.next();
					if(subItem==null)
						iterator.remove();
					Product product=subDao.getProduct(subItem.getProdId());
					if(StockCountingEnum.STOCK_BY_PAY.value().equals(product.getStockCounting())){
						  PaymentLog.info("###################### 付款减库存 by subItemId={},productId={} ########################",subItem.getSubItemId(),subItem.getProdId());
						  boolean config=subDao.addHold(product.getProdId(), subItem.getSkuId(), subItem.getBasketCount());
						  if(!config){
							    PaymentLog.error("支付出现超卖,请人工处理 , 订单号是: {},支付方式是:{},外部流水:{}",sub.getSubNumber(),subSettlement.getPayTypeId(),subSettlement.getFlowTradeNo());
								throw new BusinessException(subItem.getProdName()+"库存不足,造成订单失败,如若已经支付请联系客服进行退款操作！", ErrorCodes.BUSINESS_ERROR);
						  }
					 }
					 if(map.containsKey(product.getProdId())){
						 Long target=  map.get(product.getProdId());
						 map.put(product.getProdId(), target+subItem.getBasketCount());
					  }else{
						  map.put(product.getProdId(), subItem.getBasketCount());
					  }
					 if(firstDistUserName==null && AppUtils.isNotBlank(subItem.getDistUserName())){
						 firstDistUserName = subItem.getDistUserName();
					 }
				}
				
				//绑定分销上级    
			    if(AppUtils.isNotBlank(firstDistUserName)){
			    	PaymentLog.info("###################### 绑定分销上级该订单存在分销情况   by subNumber={},firstDistUserName={} ########################",sub.getSubNumber(),firstDistUserName);
					String userId = sub.getUserId();
					UserDetail userDetail = userDetailDao.getUserDetailById(userId);
					//如果之前没有被绑定过，则绑定现在的分销上级
					if(userDetail!=null && AppUtils.isBlank(userDetail.getParentUserName())){
						userDetailDao.update("update ls_usr_detail ud set ud.parent_user_name=?,ud.parent_binding_time=? where ud.user_id=?", firstDistUserName,new Date(),userId);
					}
			     }
			    
			     //if paimai order;
				 if(SubTypeEnum.auctions.value().equals(sub.getSubType())){
					 PaymentLog.info("###################### 该订单是拍卖订单   by subNumber={} 处理成功 ########################",sub.getSubNumber());
					 subDao.update("update ls_bidders_win set status=? where sub_number=? ", new Object[]{BiddersWinStatusEnum.PAYMENT.value(),sub.getSubNumber()});
				 }
				 
					//如果是门店订单
			     if(SubTypeEnum.SHOP_STORE.value().equals(sub.getSubType())){
			    	 PaymentLog.info("###################### 该订单是门店订单   by subNumber={} 处理成功 ########################",sub.getSubNumber());
			    	 subDao.update("update ls_store_order set dlyo_satus=? where order_sn=? and user_id=? and pay_type=2 ", new Object[]{1,sub.getSubNumber(),sub.getUserId()});
			     }
				
				 saveOrderHistory(sub);
				 
				 /**
				  * 可以异步处理 未免影响主流程
				  */
				 for (Entry<Long, Long> entry1: map.entrySet()) {
					 subDao.updateBuys(entry1.getValue(),entry1.getKey());
			     }
				 
				 //异步处理
				 ExpensesRecord expensesRecord=new ExpensesRecord();
				 expensesRecord.setRecordDate(now);
				 expensesRecord.setRecordMoney(-sub.getActualTotal());
				 expensesRecord.setRecordRemark("下单,账户支付金额为:"+sub.getActualTotal()+",订单流水号:"+sub.getSubNumber());
				 expensesRecord.setUserId(sub.getUserId());
				 expensesRecordDao.saveExpensesRecord(expensesRecord);
				 PaymentLog.info("###################### sub by subNumber={} 处理成功 ########################",sub.getSubNumber());
		}
		PaymentLog.info("###################### doBankCallback subSettlementSn={} 处理成功 ########################",subSettlement.getSubSettlementSn());
		return Constants.SUCCESS;
	}
	
	
	/* 
	 * 余额处理，扣减
	 */
	@Override
	public int predepositForPaySucceed(PredepositPaySuccess paySuccess) {
		WeiXinLog.info("PredepositPaySuccess paySuccess={}",JSONUtil.getJson(paySuccess));
		UserDetail userDetail = userDetailDao.getUserDetailById(paySuccess.getUserId());
		Double availablePredeposit=0d;
		int count=0;
		if(PrePayTypeEnum.PREDEPOSIT.value().equals(paySuccess.getPrePayType())&&AppUtils.isNotBlank(userDetail)){ //预付款
	    	  WeiXinLog.info("Using the advance payment[Predeposit] by subSettlementSn={}",paySuccess.getSubSettlementSn());
	    	  availablePredeposit=userDetail.getAvailablePredeposit();//预付款
    		  if(availablePredeposit<=0 || availablePredeposit < paySuccess.getPdAmount()){ //可用金额为0
    			  WeiXinLog.log(" User cash insufficient amount by sn= {}, userId = {}  ------- !>  ",paySuccess.getSubSettlementSn(), paySuccess.getUserId());
    			  throw new BusinessException("支付失败， 账户余额不足 ");
    		  }
    		  
    		  Double updatefreePredeposit=Arith.sub(availablePredeposit, paySuccess.getPdAmount()); //hold_amount - 下单预付款金额
    		  WeiXinLog.log("updateAvailablePredeposit start to calculate，  {}={}-{}  ------- !>  ",updatefreePredeposit, availablePredeposit,paySuccess.getPdAmount());
    		  count=  userDetailDao.updateAvailablePredeposit(updatefreePredeposit, availablePredeposit, paySuccess.getUserId());
    		  
    		  if(count==0){
    			  WeiXinLog.log("update user predeposit fail,  availablePredeposit: {} => {}  by userId = {} ------- !>  ",availablePredeposit,updatefreePredeposit, paySuccess.getUserId());
    			  throw new BusinessException("支付失败，请联系管理员！   update user predeposit fail");
    		  }
    		  
			    WeiXinLog.log("insert PdCashLog by sn= {}, userId = {}  ------- !>",paySuccess.getSubSettlementSn(),paySuccess.getUserId());
			    PdCashLog pdCashLog = new PdCashLog();
	  			pdCashLog.setUserId(paySuccess.getUserId());
	  			pdCashLog.setUserName(userDetail.getUserName());
	  			pdCashLog.setLogType(PdCashLogEnum.ORDER_PAY_FINALLY.value());
	  			pdCashLog.setAmount(-paySuccess.getPdAmount());
	  			pdCashLog.setAddTime(new Date());
	  			StringBuilder sb = new StringBuilder();
	  			sb.append("账户(微信联合)支付金额为:").append(paySuccess.getPdAmount()).append(",订单结算单据流水号: ").append(paySuccess.getSubSettlementSn());
	  			pdCashLog.setLogDesc(sb.toString());
	  			pdCashLog.setSn(paySuccess.getSubSettlementSn());
	  			pdCashLogDao.savePdCashLog(pdCashLog);
	  			
	    }else if(PrePayTypeEnum.COIN.value().equals(paySuccess.getPrePayType())&&AppUtils.isNotBlank(userDetail)){  //金币
	    	  WeiXinLog.info("Using the advance payment[Cion] by SettlementNumbers={}",paySuccess.getSubSettlementSn()); 
	    	  availablePredeposit=userDetail.getUserCoin();//金币
	    	  if(availablePredeposit<=0 || availablePredeposit < paySuccess.getPdAmount() ){ //可用金额为0
    			  WeiXinLog.log(" User cash insufficient amount by sn= {}, userId = {}  ------- !>  ",paySuccess.getSubSettlementSn(), paySuccess.getUserId());
    			throw new BusinessException("支付失败， 金币不足 ");
    		  }
	    	  
	    	  Double updatefreePredeposit=Arith.sub(availablePredeposit, paySuccess.getPdAmount()); //hold_amount - 下单预付款金额
	    	  WeiXinLog.log("updateCoin start to calculate, updatefreePredeposit: {}= availablePredeposit: {}- pdAmount: {}  ------- !>  ",updatefreePredeposit, availablePredeposit,paySuccess.getPdAmount());
	    	  count=  userDetailDao.updateUserCoin(updatefreePredeposit, availablePredeposit, paySuccess.getUserId());
	    	  
	    	  if(count==0){
    			  WeiXinLog.log("update user Coin failed,  coin: {} => {}  by userId = {} ------- !>  ",availablePredeposit,updatefreePredeposit, paySuccess.getUserId());
    			  throw new BusinessException("支付失败，请联系管理员！   update user coin fail");
    		  }
	    	  
	    	    WeiXinLog.log("insert coinLog by sn= {}, userId = {}  ------- !>",paySuccess.getSubSettlementSn(),paySuccess.getUserId());
	    	    CoinLog coinLog=new CoinLog();
				coinLog.setUserId(paySuccess.getUserId());
				coinLog.setUserName(userDetail.getUserName());
				coinLog.setSn(paySuccess.getSubSettlementSn());
				coinLog.setLogType(PdCashLogEnum.ORDER_PAY_FINALLY.value());
				coinLog.setAmount(-paySuccess.getPdAmount());
				coinLog.setAddTime(new Date());
				StringBuilder sb = new StringBuilder();
				sb.append("金币(微信联合)支付金额为:").append(paySuccess.getPdAmount()).append(",订单结算单据流水号: ").append(paySuccess.getSubSettlementSn());
				coinLog.setLogDesc(sb.toString());
				coinLogDao.saveCoinLog(coinLog);
	    }
		return count;
	}
	
	private void saveOrderHistory(Sub sub) {
		SubHistory subHistory = new SubHistory();
		Date date = new Date();
		String time = subHistory.DateToString(date);
		subHistory.setRecDate(date);
		subHistory.setSubId(sub.getSubId());
		subHistory.setStatus(SubHistoryEnum.ORDER_PAY.value());
		StringBuilder sb=new StringBuilder();
		sb.append(sub.getUserName()).append("于").append(time).append("完成订单支付--微信支付 ");
		subHistory.setUserName(sub.getUserName());
		subHistory.setReason(sb.toString());
		subHistoryDao.saveSubHistory(subHistory);
	}

	@Override
	public SubSettlement getSubSettlement(String outTradeNo, String userId) {
		return subDao.getSubSettlement(outTradeNo,userId);
	}

	@Override
	public PdRecharge findRecharge(String userId, String outTradeNo) {
		return subDao.findRecharge(userId,outTradeNo);
	}

	@Override
	public PdRecharge findRecharge(String outTradeNo) {
		return subDao.findRecharge(outTradeNo);
	}

	@Override
	public String rechargeForPaySucceed(PdRecharge pdRecharge) {
		String result=null;
		int count=subDao.update("update ls_pd_recharge set payment_state=?,payment_code=?,payment_name=?,trade_sn=?,payment_time=? where id=? and payment_state=0  ", 
				new Object[]{1,pdRecharge.getPaymentCode(),pdRecharge.getPaymentName(),pdRecharge.getTradeSn(),pdRecharge.getPaymentTime() ,pdRecharge.getId()});
		if(count==0){
			result=PreDepositErrorEnum.OK.value();
			return result;  //支付宝并发问题 ,不需要抛出异常 ,说明已经被处理过该充值单
		}
		String userId=pdRecharge.getUserId();
		UserDetail userDetail=userDetailDao.getUserDetailById(userId);
		/*
		 * update user predeposit 
		 */
		Double updatePredeposit=Arith.add(userDetail.getAvailablePredeposit(), pdRecharge.getAmount());
		count = userDetailDao.update("update ls_usr_detail set available_predeposit=? where user_id=? ", new Object[]{updatePredeposit,userId});
		if(count==0){
			throw new BusinessException("预付款充值失败  ，请联系管理员！");
		}
		
		/*
		 * insert PdCashLog 
		 */
		PdCashLog pdCashLog = new PdCashLog();
		pdCashLog.setUserId(userId);
		pdCashLog.setUserName(pdRecharge.getUserName());
		pdCashLog.setLogType(PdCashLogEnum.RECHARGE.value());
		pdCashLog.setAmount(pdRecharge.getAmount());
		pdCashLog.setAddTime(pdRecharge.getPaymentTime());
		StringBuilder sb = new StringBuilder();
		sb.append("充值，充值单号: ").append(pdRecharge.getSn());
		pdCashLog.setLogDesc(sb.toString());
		pdCashLog.setSn(pdRecharge.getSn());
		pdCashLogDao.savePdCashLog(pdCashLog);
		/*
		 * insert expensersRecored
		 */
		ExpensesRecord expensesRecord=new ExpensesRecord();
		expensesRecord.setRecordDate(pdRecharge.getPaymentTime());
		expensesRecord.setRecordMoney(pdRecharge.getAmount());
		expensesRecord.setRecordRemark("预存款充值"+pdRecharge.getAmount()+"元,充值单号："+pdRecharge.getSn());
		expensesRecord.setUserId(userId);
		expensesRecordDao.saveExpensesRecord(expensesRecord);
		return PreDepositErrorEnum.OK.value();
		
	}

	@Override
	public PayType getPayTypeById(String value) {
		return subDao.getPayTypeById(value);
	}
	
}
