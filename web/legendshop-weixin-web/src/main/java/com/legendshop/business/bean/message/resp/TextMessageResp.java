package com.legendshop.business.bean.message.resp;

/**
 * 文本消息     [响应用户]
 */
public class TextMessageResp extends BaseMessage {
    // 回复的消息内容
    private String Content;

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }
}
