package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.legendshop.business.bean.message.resp.Article;
import com.legendshop.business.bean.message.resp.BaseMessage;
import com.legendshop.business.bean.message.resp.NewsMessageResp;
import com.legendshop.business.service.ExpandKeyService;
import com.legendshop.business.util.MessageUtil;
import com.legendshop.business.util.WxConfigUtil;

public class ExpandKeyTestServiceImpl implements ExpandKeyService {

	@Override
	public String getKey() {
		return "针对关键测试";
	}

	@Override
	public String excute(HttpServletRequest request, String content,
			BaseMessage defaultMessageResp) {
		
		NewsMessageResp newsMessage = new NewsMessageResp();
		newsMessage.setToUserName(defaultMessageResp.getFromUserName());
		newsMessage.setFromUserName(defaultMessageResp.getToUserName());
		newsMessage.setCreateTime(new Date().getTime());
		newsMessage.setMsgType(WxConfigUtil.REQUEST_NEWS);
          
		List<Article> articleList = new ArrayList<Article>();
		Article article = new Article();
		article.setTitle("针对关键测试");
		article.setDescription("针对关键测试咯");
	    article.setPicUrl("http://img10.360buyimg.com/n0/jfs/t733/175/1058659678/125804/611755ba/55167acaNbdc2ba28.jpg");
        article.setUrl("http://test.legendshop.cn/wxpay/weixinJSSDK/wxPay?showwxpaytitle=1");
		articleList.add(article);
		
		Article article2= new Article();
		article2.setTitle("图文文章预览测试");
		article2.setDescription("图文文章预览测试");
	    article2.setPicUrl("http://img10.360buyimg.com/n0/jfs/t1435/328/787292909/243449/946950b8/55aa0e97Na5177247.jpg");
        article2.setUrl("http://test.legendshop.cn/news/view?itemId=1");
		articleList.add(article2);
		
		Article article3= new Article();
		article3.setTitle("JSAPIDEMO测试");
		article3.setDescription("JSAPIDEMO测试");
		article3.setPicUrl("http://img10.360buyimg.com/n0/jfs/t1435/328/787292909/243449/946950b8/55aa0e97Na5177247.jpg");
		article3.setUrl("http://test.legendshop.cn/jsapi");
		articleList.add(article3);
		
		
		 // 设置图文消息个数
        newsMessage.setArticleCount(articleList.size());
        // 设置图文消息包含的图文集合
        newsMessage.setArticles(articleList);
        // 将图文消息对象转换成xml字符串
        return  MessageUtil.newsMessageToXml(newsMessage);
        
	}

}
