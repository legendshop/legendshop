package com.legendshop.business.util;

import com.legendshop.business.service.ExpandKeyService;

public class ExpandConfigsFactory {
	
	public static ExpandKeyService getKeyService(String className){
		ExpandKeyService keyService=null;
		try {
			keyService=(ExpandKeyService) Class.forName(className).newInstance();
		} catch (Exception e) {
			WeiXinLog.error("ExpandKeyService class key [ "+className+" ]is not instance ....");
			e.printStackTrace();
		}
		return keyService;
	}
}
