package com.legendshop.business.service;

import com.legendshop.model.entity.JobStatus;

public interface JobStatusService {
	
	//查询当前job的状态
	JobStatus getJobStatusByJobName(String jobName);

    public void deleteJobStatus(JobStatus jobStatus);
    
    public Long saveJobStatus(JobStatus jobStatus);

    public void updateJobStatus(JobStatus jobStatus);

	void deleteJobStatus(String name);
    
}
