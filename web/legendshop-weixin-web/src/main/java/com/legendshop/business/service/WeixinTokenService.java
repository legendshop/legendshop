/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service;


/**
 * 微信Token
 * @author tony
 *
 */
public interface WeixinTokenService  {

	/**
	 * 获取微信公众号access_token
	 * @return
	 */
    public String getAccessToken();
    
    /**
     * jsapi_ticket是公众号用于调用微信JS接口的临时票据
     * @return
     */
    public String getJsapiTicket();
    
}
