/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.bean.message.resp.TextMessageResp;
import com.legendshop.business.constant.WxMenuInfoypeEnum;
import com.legendshop.business.dao.WeiXinKeywordResponseDao;
import com.legendshop.business.service.WeiXinKeywordResponseService;
import com.legendshop.business.service.WeixinMessageService;
import com.legendshop.business.util.MatchUtil;
import com.legendshop.business.util.MessageUtil;
import com.legendshop.business.util.WeiXinLog;
import com.legendshop.model.entity.weixin.WeiXinKeywordResponse;
import com.legendshop.util.AppUtils;

/**
 * The Class WeiXinKeywordResponseServiceImpl.
 */
@Service("weiXinKeywordResponseService")
public class WeiXinKeywordResponseServiceImpl  implements WeiXinKeywordResponseService{
	
	@Autowired
    private WeiXinKeywordResponseDao weiXinKeywordResponseDao;
    
	@Autowired
    private WeixinMessageService weixinMessageService;

	@Override
	public List<WeiXinKeywordResponse> getWeiXinKeywordResponse() {
		return weiXinKeywordResponseDao.getWeiXinKeywordResponse();
	}

	@Override
	public String disposeKeywordResponse(String content,
			TextMessageResp message) {
		List<WeiXinKeywordResponse> keywordResponses=weiXinKeywordResponseDao.getWeiXinKeywordResponse();
		if(AppUtils.isNotBlank(keywordResponses)){
			WeiXinLog.info("-------------------- [A] 关键字客户端： --------------------");
			List<WeiXinKeywordResponse> fitKeyWords=new ArrayList<WeiXinKeywordResponse>();
			for (int i = 0; i < keywordResponses.size(); i++) {
				boolean seachFig=false;
				WeiXinKeywordResponse response=keywordResponses.get(i);
				if(response.getPptype()==1){
					if(MatchUtil.match(0.49999,content,response.getKeyword())){ //模糊匹配模式
						seachFig=true;
					}
				}else if(response.getPptype()==2){
					if(content.equals(response.getKeyword())){ //完全匹配模式
						seachFig=true;
					}
				}
				if(seachFig){
					fitKeyWords.add(response);
				}
			}
			if(AppUtils.isBlank(fitKeyWords)){
				return null;
			}
			WeiXinKeywordResponse autoResponse=null;
			int n=fitKeyWords.size();
			if(n>1){ //说明有多个关键字规则模版 ,随机获取一套模版
				Random random=new Random();
				int index=random.nextInt(n);
				autoResponse=fitKeyWords.get(index);
			}else{
				autoResponse=fitKeyWords.get(0);
			}
			String resMsgType = autoResponse.getMsgtype();
			if (WxMenuInfoypeEnum.TEXT.value().equals(resMsgType)) {
				message.setContent(autoResponse.getContent());
				return MessageUtil.textMessageToXml(message);
			} else {
				String messageResp = weixinMessageService.getMsg(resMsgType,
						message.getToUserName(), message.getFromUserName(),
						autoResponse.getTemplateid());
				return messageResp;
			}
		}
		return null;
	}

}
