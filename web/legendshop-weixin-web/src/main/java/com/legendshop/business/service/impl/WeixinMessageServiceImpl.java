package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.config.dto.WeixinPropertiesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.util.PhotoPathResolver;
import com.legendshop.business.bean.message.resp.Article;
import com.legendshop.business.bean.message.resp.NewsMessageResp;
import com.legendshop.business.bean.message.resp.TextMessageResp;
import com.legendshop.business.dao.WeixinNewsitemDao;
import com.legendshop.business.dao.WeixinTexttemplateDao;
import com.legendshop.business.service.WeixinMessageService;
import com.legendshop.business.util.MessageUtil;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.model.entity.weixin.WeixinNewsitem;
import com.legendshop.model.entity.weixin.WeixinTexttemplate;
import com.legendshop.util.AppUtils;

@Service("weixinMessageService")
public class WeixinMessageServiceImpl implements WeixinMessageService {

	@Autowired
	private WeixinTexttemplateDao weixinTexttemplateDao;

	@Autowired
	private WeixinNewsitemDao weixinNewsitemDao;

	@Autowired
	private WeixinPropertiesDto weixinPropertiesDto;
	
	@Override
	public String getMsg(String resMsgType, String toUserName,
			String fromUserName, Long templateId) {
		if(AppUtils.isBlank(templateId)){
			return null;
		}
		if (WxConfigUtil.REQUEST_TEXT.equals(resMsgType)) {
			TextMessageResp textMessageResp = new TextMessageResp();
			textMessageResp.setToUserName(toUserName);
			textMessageResp.setFromUserName(fromUserName);
			textMessageResp.setCreateTime(new Date().getTime());
			textMessageResp.setMsgType(WxConfigUtil.REQUEST_TEXT);
			WeixinTexttemplate textTemplate = weixinTexttemplateDao.getWeixinTexttemplate(templateId);
			textMessageResp.setContent(textTemplate.getContent());
			return MessageUtil.textMessageToXml(textMessageResp);
		} else if (WxConfigUtil.REQUEST_NEWS.equals(resMsgType)) {
			
			List<WeixinNewsitem> newsitems = weixinNewsitemDao.getWeixinNewsitemByTemplateId(templateId);
			if (AppUtils.isNotBlank(newsitems)) {
				NewsMessageResp newsMessageResp = new NewsMessageResp();
				newsMessageResp.setToUserName(toUserName);
				newsMessageResp.setFromUserName(fromUserName);
				newsMessageResp.setCreateTime(new Date().getTime());
				newsMessageResp.setMsgType(WxConfigUtil.REQUEST_NEWS);
				// 事件类型
				List<Article> articleList = new ArrayList<Article>(newsitems.size());
				for (WeixinNewsitem item : newsitems) {
					Article article = new Article();
					article.setTitle(item.getTitle());
					String path= PhotoPathResolver.getInstance().calculateFilePath(item.getImagepath()).getFilePath();
					article.setPicUrl(path);
					String url = "";
					if (AppUtils.isBlank(item.getUrl())) {
						url = weixinPropertiesDto.getWeixinDomainName()
								+ "/news/view/"
								+ item.getId();
					} else {
						url = item.getUrl();
					}
					article.setUrl(url);
					article.setDescription(item.getDesction());
					articleList.add(article);
				}
				newsMessageResp.setArticleCount(articleList.size());
				newsMessageResp.setArticles(articleList);
				String xml=MessageUtil.newsMessageToXml(newsMessageResp);
				System.out.println(xml);
				return xml;
			}
		}/*else if (WxConfigUtil.REQUEST_IMAGE.equals(resMsgType)) {
			ImageMessageResp imageMessageResp = new ImageMessageResp();
			imageMessageResp.setToUserName(toUserName);
			imageMessageResp.setFromUserName(fromUserName);
			imageMessageResp.setCreateTime(new Date().getTime());
			imageMessageResp.setMsgType(WxConfigUtil.REQUEST_IMAGE);
			
			imageMessageResp.setImage(image);
			return MessageUtil.imageMessageToXml(imageMessageResp);
		}*/
		return null;
	}

}
