/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service;

import javax.servlet.http.HttpServletRequest;


/**
 * 微信服务处理中心.
 *
 * @author tony
 */
public interface WechatService {

	public String wxCoreService(HttpServletRequest request);
	
}
