/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinNewsitemDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.weixin.WeixinNewsitem;

/**
 * The Class WeixinNewsitemDaoImpl.
 */

@Repository
public class WeixinNewsitemDaoImpl extends GenericDaoImpl<WeixinNewsitem, Long> implements WeixinNewsitemDao  {

	@Cacheable(value="WeixinNewsitem",key="#id")
	public WeixinNewsitem getWeixinNewsitem(Long id){
		return getById(id);
	}
	
    public int deleteWeixinNewsitem(WeixinNewsitem weixinNewsitem){
    	return delete(weixinNewsitem);
    }
	
	public Long saveWeixinNewsitem(WeixinNewsitem weixinNewsitem){
		return save(weixinNewsitem);
	}
	
	public int updateWeixinNewsitem(WeixinNewsitem weixinNewsitem){
		return update(weixinNewsitem);
	}

	@Override
	//@Cacheable(value="WeixinNewsitemList",key="#templateId")
	public List<WeixinNewsitem> getWeixinNewsitemByTemplateId(Long templateId) {
		return this.queryByProperties(new EntityCriterion().eq("newsTemplateid", templateId).addAscOrder("orders"));
	}
	
 }
