package com.legendshop.business.service.impl;

import org.springframework.stereotype.Service;

import com.legendshop.business.dao.JobStatusDao;
import com.legendshop.business.service.JobStatusService;
import com.legendshop.model.entity.JobStatus;
import com.legendshop.util.AppUtils;

@Service("jobStatusService")
public class JobStatusServiceImpl implements JobStatusService {

	private JobStatusDao jobStatusDao;

	public void deleteJobStatus(JobStatus jobStatus) {
		jobStatusDao.deleteJobStatus(jobStatus);
	}

	public Long saveJobStatus(JobStatus jobStatus) {
		if (!AppUtils.isBlank(jobStatus.getId())) {
			updateJobStatus(jobStatus);
			return jobStatus.getId();
		}
		return jobStatusDao.saveJobStatus(jobStatus);
	}


	@Override
	public JobStatus getJobStatusByJobName(String jobName) {
		return jobStatusDao.getJobStatusByJobName(jobName);
	}

	@Override
	public void updateJobStatus(JobStatus jobStatus) {
		jobStatusDao.updateJobStatus(jobStatus);
	}

	@Override
	public void deleteJobStatus(String name) {
		jobStatusDao.deleteJobStatus(name);
	}
	

}
