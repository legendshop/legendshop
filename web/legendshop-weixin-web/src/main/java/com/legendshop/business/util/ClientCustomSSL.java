package com.legendshop.business.util;

import java.io.InputStream;
import java.security.KeyStore;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 * This example demonstrates how to create secure connections with a custom SSL
 * context.
 */
public class ClientCustomSSL {

	/**
	 * 注意PKCS12证书 是从微信商户平台-》账户设置-》 API安全 中下载的
	 */
	
	public static final String ConfigFile = "//apiclient_cert.p12";
	
	private static Map<String,String> errorCode=new HashMap<String,String>();
	
	static{
		errorCode.put("SYSTEMERROR", "系统超时,请重试");
		errorCode.put("USER_ACCOUNT_ABNORMAL", "用户帐号异常或注销,此状态代表退款申请失败，商户可自行处理退款。");
		errorCode.put("INVALID_TRANSACTIONID", "请求参数错误，检查原交易号是否存在或发起支付交易接口返回失败");
		errorCode.put("PARAM_ERROR", "请求参数错误，请重新检查再调用退款申请");
		errorCode.put("APPID_NOT_EXIST", "请检查APPID是否正确");
		errorCode.put("MCHID_NOT_EXIST", "请检查MCHID是否正确");
		errorCode.put("APPID_MCHID_NOT_MATCH", "请确认appid和mch_id是否匹配");
		errorCode.put("REQUIRE_POST_METHOD", "请检查请求参数是否通过post方法提交");
		errorCode.put("SIGNERROR", "请检查签名参数和方法是否都符合签名算法要求");
		errorCode.put("XML_FORMAT_ERROR", "请检查XML参数格式是否正确");
	}
	
	/**
	 * 微信退款
	 * @param url
	 * @param data
	 * @param mchId
	 * @return
	 * @throws Exception
	 */
	public static String doRefund(String url, String data,String mchId) throws Exception {
		 KeyStore keyStore  = KeyStore.getInstance("PKCS12");
		 InputStream instream  = ClientCustomSSL.class.getResourceAsStream(ConfigFile);
		  
		try {
			/**
			 * 此处要改
			 * */
			keyStore.load(instream, mchId.toCharArray());// 这里写密码..默认是你的MCHID
		 } finally {
			 if(instream!=null){
				 instream.close();
			 }
		 }
		
		 // Trust own CA and all self-signed certs
		  SSLContext sslcontext = SSLContexts.custom()
	                .loadKeyMaterial(keyStore, mchId.toCharArray())//这里也是写密码的  
	                .build();
		
		   // Allow TLSv1 protocol only
	        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
	                sslcontext,
	                new String[] { "TLSv1" },
	                null,
	                SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
	        CloseableHttpClient httpclient = HttpClients.custom()
	                .setSSLSocketFactory(sslsf)
	                .build();
	        try {
	        	HttpPost httpost = new HttpPost(url); // 设置响应头信息
	        	httpost.addHeader("Connection", "keep-alive");
	        	httpost.addHeader("Accept", "*/*");
	        	httpost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
	        	httpost.addHeader("Host", "api.mch.weixin.qq.com");
	        	httpost.addHeader("X-Requested-With", "XMLHttpRequest");
	        	httpost.addHeader("Cache-Control", "max-age=0");
	        	httpost.addHeader("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) ");
	    		httpost.setEntity(new StringEntity(data, "UTF-8"));
	            CloseableHttpResponse response = httpclient.execute(httpost);
	            try {
	                HttpEntity entity = response.getEntity();
	                String jsonStr = EntityUtils.toString(entity, "UTF-8");
	                Map<String,String> map=MessageUtil.parseXml(jsonStr);
	                
	                String return_code=map.get("return_code");  //返回状态码
	                if("SUCCESS".equals(return_code)){
	                	String result_code=map.get("result_code");
	                	if("SUCCESS".equals(return_code)){
	                		String out_trade_no=map.get("transaction_id"); //微信订单号(支付后微信返回的流水号)
	                		String out_refund_no=map.get("out_refund_no"); //商户退款单号
	                		String refund_id=map.get("out_refund_no"); //微信退款单号
	                		
	                		
	                		
	                		
	                	}else{
	                		String err_code=map.get("err_code");
	                		String err_code_des=errorCode.get(err_code);
	                		if(err_code_des!=null)
	                		  return err_code_des;
	                		return "退款失败 err_code="+err_code;
	                	}
	                }else{
	                	String return_msg=map.get("return_msg");
	                	return return_msg;
	                }
	                
	               return jsonStr;
	            } finally {
	                response.close();
	            }
	        } finally {
	            httpclient.close();
	        }
	}

}
