package com.legendshop.business.service;

import java.util.List;

import com.legendshop.model.dto.PredepositPaySuccess;
import com.legendshop.model.entity.PdRecharge;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubSettlement;

public interface SubService {

	List<Sub> getSubBySubNumberByUserId(Long [] subIds, String userId);
	
	List<Sub> getSubByOutTradeNo(String outTradeNo, String userId);

	SubSettlement getSubSettlement(String out_trade_no);
	
	SubSettlement getSubSettlement(String out_trade_no, String userId);

	void doBankCallback(SubSettlement subSettlement);

	PdRecharge findRecharge(String userId, String outTradeNo);
	
	PdRecharge findRecharge(String outTradeNo);

	
	/**
	 * 预付款成功充值
	 * @param pdRecharge
	 * @return
	 */
	String rechargeForPaySucceed(PdRecharge pdRecharge);
	
	/**
	 * 微信支付成功后，余额处理
	 * @param paySuccess
	 * @return
	 */
	int predepositForPaySucceed(PredepositPaySuccess paySuccess);
	
	
	
}

