/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinAutoresponseDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.weixin.WeixinAutoresponse;
import com.legendshop.util.AppUtils;

/**
 * The Class WeixinAutoresponseDaoImpl.
 */

@Repository
public class WeixinAutoresponseDaoImpl extends GenericDaoImpl<WeixinAutoresponse, Long> implements WeixinAutoresponseDao  {
     
    public List<WeixinAutoresponse> getWeixinAutoresponse(String userName){
   		return this.queryByProperties(new EntityCriterion().eq("userName", userName));
    }
    
    /**
     * 查询前20条关键数据
     * @return
     */
    public List<WeixinAutoresponse> getWeixinAutoresponse(){
   		return this.queryLimit(" SELECT a.id as id,a.keyword as keyword,a.msgType as msgType,a.templateId as templateId,a.templateName as templateName,a.create_date as createDate from ls_weixin_autoresponse as a ",WeixinAutoresponse.class , 0, 20);
    }
    
    

	public WeixinAutoresponse getWeixinAutoresponse(Long id){
		return getById(id);
	}
	
    public int deleteWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse){
    	return delete(weixinAutoresponse);
    }
	
	public Long saveWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse){
		return save(weixinAutoresponse);
	}
	
	public int updateWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse){
		return update(weixinAutoresponse);
	}

	@Override
	public WeixinAutoresponse findAutoResponseMsgTriggerType(
			String msgTriggerType) {
		List<WeixinAutoresponse> list= this.queryByProperties(new EntityCriterion().eq("msgTriggerType", msgTriggerType).addDescOrder("id"));
		if(AppUtils.isNotBlank(list)){
			return list.get(0);
		}
		return null;
	}
	
 }
