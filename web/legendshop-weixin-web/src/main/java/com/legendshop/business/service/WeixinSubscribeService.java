/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service;

import com.legendshop.business.bean.message.event.SubscribeEvent;

/**
 * The Class WeixinSubscribeService.
 */
public interface WeixinSubscribeService  {


	public String subscribeResponse(SubscribeEvent subscribeEvent);
}
