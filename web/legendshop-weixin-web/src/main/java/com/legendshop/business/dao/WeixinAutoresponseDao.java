/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.weixin.WeixinAutoresponse;

/**
 * The Class WeixinAutoresponseDao.
 */

public interface WeixinAutoresponseDao extends Dao<WeixinAutoresponse, Long> {
     
    public abstract List<WeixinAutoresponse> getWeixinAutoresponse(String shopName);

	public abstract WeixinAutoresponse getWeixinAutoresponse(Long id);
	
    public abstract int deleteWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse);
	
	public abstract Long saveWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse);
	
	public abstract int updateWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse);
	
	List<WeixinAutoresponse> getWeixinAutoresponse();

	public abstract WeixinAutoresponse findAutoResponseMsgTriggerType(
			String msgTriggerType);
	
 }
