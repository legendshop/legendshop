/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service;

import com.legendshop.model.entity.weixin.WeixinNewsitem;

/**
 * The Class WeixinNewsitemService.
 */
public interface WeixinNewsitemService  {

    public WeixinNewsitem getWeixinNewsitem(Long id);
    
}
