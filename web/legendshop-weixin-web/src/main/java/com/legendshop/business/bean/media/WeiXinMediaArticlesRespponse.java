package com.legendshop.business.bean.media;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.business.bean.WeiXinMsg;

/**
 * 获得永久素材媒体信息
 * 
 * @author tony
 * 
 */
public class WeiXinMediaArticlesRespponse extends WeiXinMsg {
	List<WeiXinMediaArticle> news_item = new ArrayList<WeiXinMediaArticle>();

	public List<WeiXinMediaArticle> getNews_item() {
		return news_item;
	}

	public void setNews_item(List<WeiXinMediaArticle> news_item) {
		this.news_item = news_item;
	}
	
	

}
