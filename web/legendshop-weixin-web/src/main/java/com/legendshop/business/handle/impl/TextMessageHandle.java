package com.legendshop.business.handle.impl;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.legendshop.config.dto.WeixinPropertiesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.business.bean.message.req.BaseMessage;
import com.legendshop.business.bean.message.req.TextMessage;
import com.legendshop.business.bean.message.resp.TextMessageResp;
import com.legendshop.business.dao.WeixinMenuExpandConfigDao;
import com.legendshop.business.event.ReceiveTextEvent;
import com.legendshop.business.handle.MsgTypeState;
import com.legendshop.business.service.ExpandKeyService;
import com.legendshop.business.service.TulingApiProcessService;
import com.legendshop.business.service.WeiXinKeywordResponseService;
import com.legendshop.business.service.WeixinAutoresponseService;
import com.legendshop.business.service.WeixinMessageService;
import com.legendshop.business.util.ExpandConfigsFactory;
import com.legendshop.business.util.MessageUtil;
import com.legendshop.business.util.WeiXinLog;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.framework.event.EventHome;
import com.legendshop.model.entity.weixin.WeixinAutoresponse;
import com.legendshop.model.entity.weixin.WeixinMenuExpandConfig;
import com.legendshop.model.entity.weixin.WeixinReceivetext;
import com.legendshop.util.AppUtils;

@Component("textMessageHandle")
public class TextMessageHandle implements MsgTypeState {
	
	@Autowired
	private WeixinAutoresponseService autoresponseService;
	
	@Autowired
	private WeixinMessageService weixinMessageService;
	
	@Autowired
	private WeixinMenuExpandConfigDao expandConfigDao;
	
	@Autowired
	private TulingApiProcessService tulingApiProcessService;
	
	@Autowired
	private WeiXinKeywordResponseService weiXinKeywordResponseService;

	@Autowired
	private WeixinPropertiesDto weixinPropertiesDto;
	

	public String handle(HttpServletRequest request, BaseMessage message) {
		
		WeiXinLog.info("  ####################### 文本消息处理 ################ ");

		TextMessage textMessagereq = (TextMessage) message;
		TextMessageResp textMessageResp = new TextMessageResp();
		textMessageResp.setToUserName(message.getToUserName());
		textMessageResp.setFromUserName(message.getFromUserName());
		textMessageResp.setCreateTime(new Date().getTime());
		textMessageResp.setMsgType(WxConfigUtil.REQUEST_TEXT);

		if (weixinPropertiesDto.getRecordReceivetext()) {
			WeiXinLog.info("  ####################### 启用了文本消息记录  ################ ");
			sendReceiveText(textMessagereq);
		}
		
		// =================================================================================================================
		/*
		 * 1、是否符合关键字自动回复规则
		 * 2、不满足1条件、是否符合系统自身的关键业务指令
		 * 3、不满足1 2 条件，是否启动智能机器人
		 * 4、不满足1 2  3 条件 是否启动微信自动回复功能
		 * 5、不满足1 2 3 4 条件 回复默认欢迎语
		 */
		// Step.1 判断关键字信息中是否管理该文本内容。有的话优先采用数据库中的回复
		String content = textMessagereq.getContent();
		
		WeiXinLog.info("  ############### 客户端消息内容 {}  ############# ",content);
		String keywordResponses = weiXinKeywordResponseService.disposeKeywordResponse(content, textMessageResp);
		if (AppUtils.isNotBlank(keywordResponses)) {
			WeiXinLog.info("  ############### 关键字消息自动回复规则  ############# ");
			WeiXinLog.info("  ############### keywordResponses ={}   ############# ",keywordResponses);
			return keywordResponses;
		}
		WeiXinLog.info("  ############### [B] 系统自身的关键业务指令客户端：{} [ 非关键字 ] ############# ",content);
		
		// 系统指令切入
		List<WeixinMenuExpandConfig> expandConfigs = expandConfigDao.getWeixinMenuExpandConfig();
		if (AppUtils.isNotBlank(expandConfigs)) {
			WeiXinLog.info("  ############### [B] 系统自身的关键业务指令扩展接口规则 ############# ");
			boolean findflag = false;// 是否找到关键字信息
			String respMessage = null;
			for (int i = 0; i < expandConfigs.size(); i++) {
				WeixinMenuExpandConfig cf = expandConfigs.get(i);
				// 如果已经找到关键字并处理业务，结束循环。
				if (findflag) {
					break;// 如果找到结束循环
				}
				String[] keys = cf.getKeyword().split(",");
				for (String k : keys) {
					if (content.indexOf(k) != -1) {
						String className = cf.getClassService();
						WeiXinLog.info("-------------------- [B] 系统自身的关键业务指令扩展接口  className {} --------------------",	className);
						if (className != null) {
							ExpandKeyService expandKeyService = ExpandConfigsFactory
									.getKeyService(className);
							if (expandKeyService != null) {
								respMessage = expandKeyService.excute(request,
										content, textMessageResp);
							}
							findflag = true;// 改变标识，已经找到关键字并处理业务，结束循环。
							break;// 当前关键字信息处理完毕，结束当前循环
						}
					}
				}
			}
			if (respMessage != null) {
				return respMessage;
			}
		}

		/*
		 * 是否启动智能机器人
		 */
		if (weixinPropertiesDto.getStartIntelligentRobot()) { //
			WeiXinLog.info("  ############### [C] 智能机器人客户端  ############# ");
			/**
			 * 通过智能机器人处理
			 */
			String tuling = tulingApiProcessService.getTulingResult(content);
			textMessageResp.setContent(tuling);
			return MessageUtil.textMessageToXml(textMessageResp);
		} else {
			WeiXinLog.info("  ###############  [D] 自动回复客户端规则   ############# ");
			WeixinAutoresponse autoResponse = autoresponseService.findAutoResponseMsgTriggerType(WxConfigUtil.RECRIVE_TEXT);
			if (AppUtils.isNotBlank(autoResponse)) {
				String resMsgType = autoResponse.getMsgType();
				WeiXinLog.info("-------------------- [D] 客户端 [ 消息模版类型 {}  ] --------------------",resMsgType);
				if (WxConfigUtil.REQUEST_TEXT.equals(resMsgType)) {
					textMessageResp.setContent(autoResponse.getContent());
					 return MessageUtil.textMessageToXml(textMessageResp);
			    }
				String messageResp = weixinMessageService.getMsg(resMsgType,message.getToUserName(),message.getFromUserName(), autoResponse.getTemplateId());
				if (AppUtils.isNotBlank(messageResp)) {
					return messageResp;
				} else {
					return "";
				}
			}
		}
		if(AppUtils.isBlank(textMessageResp.getContent())){
			return "";
		}
		return MessageUtil.textMessageToXml(textMessageResp);
	}
	
	
	private void sendReceiveText(TextMessage message) {

		// 保存接收到的信息
		WeixinReceivetext receiveText = new WeixinReceivetext();
		
		receiveText.setCreatetime(new Date());
		receiveText.setFromusername(message.getFromUserName());
		receiveText.setTousername(message.getToUserName());
		receiveText.setMsgid(String.valueOf(message.getMsgId()));
		receiveText.setMsgtype(message.getMsgType());
		receiveText.setResponse("0");
		receiveText.setContent(message.getContent());

		// 发布接受用户发送记录事件
		EventHome.publishEvent(new ReceiveTextEvent(receiveText));
		
	}

	public void setAutoresponseService(WeixinAutoresponseService autoresponseService) {
		this.autoresponseService = autoresponseService;
	}


	public void setWeixinMessageService(WeixinMessageService weixinMessageService) {
		this.weixinMessageService = weixinMessageService;
	}


	public void setExpandConfigDao(WeixinMenuExpandConfigDao expandConfigDao) {
		this.expandConfigDao = expandConfigDao;
	}


	public void setTulingApiProcessService(
			TulingApiProcessService tulingApiProcessService) {
		this.tulingApiProcessService = tulingApiProcessService;
	}

	public void setWeiXinKeywordResponseService(
			WeiXinKeywordResponseService weiXinKeywordResponseService) {
		this.weiXinKeywordResponseService = weiXinKeywordResponseService;
	}


}
