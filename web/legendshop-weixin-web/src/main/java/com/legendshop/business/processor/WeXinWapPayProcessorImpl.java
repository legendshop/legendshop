package com.legendshop.business.processor;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.RandomStringUtils;
import com.legendshop.web.util.RequestHandler;

/**
 * 微信内置浏览器公众号支付 JSAPI方式
 * @author Tony
 */
@Component("weXinWapPayProcessor")
public class WeXinWapPayProcessorImpl {
  
    @Autowired
    private PropertiesUtil propertiesUtil;

	public Map<String, Object> payto(String paymentConfig,SysPaymentForm payParams) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", false);

		JSONObject jsonObject=JSONObject.parseObject(paymentConfig);
		/** 微信公众号APPID */
		String appid = jsonObject.getString(WxConfigUtil.APPID);
		/** 微信公众号APPID */
		String token = jsonObject.getString(WxConfigUtil.TOKEN);
		/** 微信公众号绑定的商户号 */
		String mch_id = jsonObject.getString(WxConfigUtil.MCH_ID);
		String appSecret=jsonObject.getString(WxConfigUtil.APPSECRET);
		String partnerkey = jsonObject.getString(WxConfigUtil.PARTNERKEY);

		if (AppUtils.isBlank(appid) || AppUtils.isBlank(mch_id) || AppUtils.isBlank(partnerkey)) {
			map.put("message", "请设置支付的密钥信息!");
			return map;
		}

		String subject = payParams.getSubject().replace(" ", "");// 注意标题一定去空格
		String totalPrice = String.valueOf(new BigDecimal(payParams.getTotalAmount()).setScale(2, BigDecimal.ROUND_HALF_UP)
				.multiply(new BigDecimal(100)).intValue());

		SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();

		/** 公众号APPID */
		parameters.put("appid", appid);
		/** 商户号 */
		parameters.put("mch_id", mch_id);
		/** 随机字符串 */
		parameters.put("nonce_str", RandomStringUtils.randomNumeric(8));// 随机字符串
		/** 商品名称 */
		parameters.put("body", subject);
		/** 订单号 */
		parameters.put("out_trade_no", payParams.getSubSettlementSn());
		/** 订单金额以分为单位，只能为整数 */
		parameters.put("total_fee", totalPrice);
		/** 客户端本地ip */
		parameters.put("spbill_create_ip", payParams.getIp());

		/** The notify_url 交易过程中服务器通知的页面 要用 http://格式的完整路径，不允许加?id=123这类自定义参数. */
		//String notifyUrl =WxPropertiesUtils.getWeiXinServiceDomainName()+ "/wxpay/weixinJSBridge/notify";
		
		String notifyUrl = propertiesUtil.getPcDomainName()  + "/payNotify/notify/"+ PayTypeEnum.WX_PAY.value();

		parameters.put("notify_url", notifyUrl);

		/** 支付方式为APP支付 */
		parameters.put("trade_type","JSAPI");

		/** 用户微信的openid，当trade_type为JSAPI的时候，该属性字段必须设置 */
		parameters.put("openid", payParams.getOpenId());
		parameters.put("attach", payParams.getSubSettlementSn());
		
		RequestHandler reqHandler = new RequestHandler();
		reqHandler.init(token, appid, appSecret, partnerkey);

		// 计算签名
		String sign = reqHandler.createSign(parameters);
		parameters.put("sign", sign);

		Map<String, String> reqHandlerMap = reqHandler.getPrepayId(parameters);
		PaymentLog.info(" ########## WeXinWAPPay getPrepayId reqHandlerMap {} ##########",JSONUtil.getJson(reqHandlerMap));
		
		if (AppUtils.isBlank(reqHandlerMap)) {
			map.put("message", "微信统一下单集成失败！");
			return map;
		}

		String return_code = reqHandlerMap.get("return_code"); // 返回状态码
																// SUCCESS/FAIL
		if ("SUCCESS".equals(return_code)) {
			// 参数
			SortedMap<Object, Object> paraMap = new TreeMap<Object, Object>();
			// 参数
			String timeStamp = System.currentTimeMillis() + "";

			paraMap.put("appId",appid);
			paraMap.put("timeStamp", timeStamp);
			paraMap.put("nonceStr", RandomStringUtils.randomNumeric(8));
			/**
			 * 获取预支付单号prepay_id后，需要将它参与签名。 微信支付最新接口中，要求package的值的固定格式为prepay_id=...
			 */
			String packageValue="prepay_id=" + reqHandlerMap.get("prepay_id");
			paraMap.put("package",packageValue);
			
			/** 微信支付新版本签名算法使用MD5，不是SHA1 */
			paraMap.put("signType", "MD5");
			
			/**
			 * 获取预支付prepay_id之后，需要再次进行签名，参与签名的参数有：appId、timeStamp、nonceStr、package、
			 * signType. 主意上面参数名称的大小写. 该签名用于前端js中WeixinJSBridge.invoke中的paySign的参数值
			 */
			// 要签名
			String paySign = reqHandler.createSign(paraMap);
			paraMap.put("paySign", paySign);
			paraMap.put("packageValue", packageValue);
	
			System.out.println(JSONUtil.getJson(paraMap));
			
			map.put("result", true);
			map.put("message", JSONUtil.getJson(paraMap));
			return map;
		}else{
			map.put("message", reqHandlerMap.get("return_msg"));
		}
		return map;
	}

}
