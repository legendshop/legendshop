package com.legendshop.business.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.business.service.WeixinTokenService;
import com.legendshop.model.dto.weixin.JsApiInfo;
import com.legendshop.web.util.RequestHandler;

/**
 * 获取微信的基本信息
 *
 */
@Controller
@RequestMapping("/admin/weixin/base")
public class WeiXinBaseManageController {
	
	@Autowired
	private WeixinTokenService weixinTokenService;
	
	/**
	 * 获取JS-SDK的配置信息
	 * @param request
	 * @param response
	 * @param url 当前网页的URL，不包含#及其后面部分
	 * @return
	 */
	@RequestMapping(value = "/getJsApiInfo", method = RequestMethod.GET)
	public @ResponseBody JsApiInfo getJsApiInfo(HttpServletRequest request, HttpServletResponse response,@RequestParam String url) {
		
		String nonceStr=RandomStringUtils.randomAlphabetic(16);
	    long timestamp = System.currentTimeMillis();// 时间戳
	    String jsapiTicket= weixinTokenService.getJsapiTicket();
	    
	    //该签名是用于前端js中wx.config配置中的signature值。
        RequestHandler handler=new RequestHandler();
        String signature  =  handler.createSign_wx_config(jsapiTicket,nonceStr,timestamp,url);
        
	    JsApiInfo jsApiInfo=new JsApiInfo();
		jsApiInfo.setAppId("");
		jsApiInfo.setNonceStr(nonceStr);
		jsApiInfo.setTimestamp(String.valueOf(timestamp));
		jsApiInfo.setSignature(signature);
	     
		return jsApiInfo;
	}



}
