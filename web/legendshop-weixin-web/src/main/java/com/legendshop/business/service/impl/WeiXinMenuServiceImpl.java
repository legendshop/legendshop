package com.legendshop.business.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.WeiXinMenuEntityDao;
import com.legendshop.business.entity.MenuEntity;
import com.legendshop.business.service.WeiXinMenuService;
import com.legendshop.util.AppUtils;

@Service("weiXinMenuService")
public class WeiXinMenuServiceImpl implements WeiXinMenuService {
	
	@Autowired
	private WeiXinMenuEntityDao weiXinMenuEntityDao;

	public List<MenuEntity> getMenuEntities() {
		List<MenuEntity> entities=weiXinMenuEntityDao.getWeixinMenu();
		if(AppUtils.isNotBlank(entities)){
			Map<Long, MenuEntity> menuMap = new LinkedHashMap<Long, MenuEntity>();
			for (int i = 0; i < entities.size(); i++) {
				MenuEntity entity=entities.get(i);
				if (entity.getLevel() == 1) { // for 顶级菜单
					menuMap.put(entity.getId(), entity);
				} else if (entity.getLevel() == 2) { // 二级菜单
					// 拿到一级菜单先
					MenuEntity menuLevel1 = menuMap.get(entity.getParentId());
					entity.setParentMenu(menuLevel1);
					if (menuLevel1 == null) {
						throw new NullPointerException(entity.getParentId() + " menu can not load level1 menu");
						// continue; // 可能是由于上下线的关系
					}
					menuLevel1.addSubMenu(entity);
				}
		  }
		  if(AppUtils.isNotBlank(menuMap)){
			  return new ArrayList<MenuEntity>(menuMap.values()) ;
		  }
		}
		return null;
	}

	public void setWeiXinMenuEntityDao(WeiXinMenuEntityDao weiXinMenuEntityDao) {
		this.weiXinMenuEntityDao = weiXinMenuEntityDao;
	}

	@Override
	@Cacheable(value="MenuEntity",key="#key")
	public MenuEntity findUniqueByProperty( String key) {
		return weiXinMenuEntityDao.findUniqueByProperty(key);
	}

	
	
}
