/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.weixin.WeixinGzuserInfo;

/**
 * The Class WeixinGzuserInfoDao.
 */

public interface WeixinGzuserInfoDao extends GenericDao<WeixinGzuserInfo, Long> {
     
    public abstract WeixinGzuserInfo getWeixinGzuserInfo(String openid);

	public abstract WeixinGzuserInfo getWeixinGzuserInfo(Long id);
	
    public abstract int deleteWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo);
	
	public abstract Long saveWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo);
	
	public abstract int updateWeixinGzuserInfo(WeixinGzuserInfo weixinGzuserInfo);
	
	public abstract PageSupport getWeixinGzuserInfo(CriteriaQuery cq);

	public abstract void cancelsubscribe(String fromUserName);

    /*	public abstract WeixinGzuserInfo getWeixinGzuserInfoByUserId(String userId);*/

    /*	public abstract String getUserName(String userId);*/

	void saveWeixinGzuserInfo(List<WeixinGzuserInfo> entities);
	
 }
