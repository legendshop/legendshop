/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.WeixinReceivetextDao;
import com.legendshop.business.service.WeixinReceivetextService;
import com.legendshop.model.entity.weixin.WeixinReceivetext;
import com.legendshop.util.AppUtils;

/**
 * The Class WeixinReceivetextServiceImpl.
 */
@Service("weixinReceivetextService")
public class WeixinReceivetextServiceImpl  implements WeixinReceivetextService{
	
	@Autowired
    private WeixinReceivetextDao weixinReceivetextDao;

    public List<WeixinReceivetext> getWeixinReceivetext(String userName) {
        return weixinReceivetextDao.getWeixinReceivetext(userName);
    }

    public WeixinReceivetext getWeixinReceivetext(Long id) {
        return weixinReceivetextDao.getWeixinReceivetext(id);
    }

    public void deleteWeixinReceivetext(WeixinReceivetext weixinReceivetext) {
        weixinReceivetextDao.deleteWeixinReceivetext(weixinReceivetext);
    }

    public Long saveWeixinReceivetext(WeixinReceivetext weixinReceivetext) {
        if (!AppUtils.isBlank(weixinReceivetext.getId())) {
            updateWeixinReceivetext(weixinReceivetext);
            return weixinReceivetext.getId();
        }
        return weixinReceivetextDao.saveWeixinReceivetext(weixinReceivetext);
    }

    public void updateWeixinReceivetext(WeixinReceivetext weixinReceivetext) {
        weixinReceivetextDao.updateWeixinReceivetext(weixinReceivetext);
    }
}
