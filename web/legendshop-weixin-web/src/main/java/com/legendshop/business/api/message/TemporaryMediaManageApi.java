package com.legendshop.business.api.message;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.util.WeiXinErrcodeUtil;
import com.legendshop.business.bean.media.WeiXinMedia;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.web.util.WeiXinUtil;


/**
 *  ------> 新增临时素材多媒体文件上传下载API
 * @author tony
 *
 */
public class TemporaryMediaManageApi {

	 private static Logger log = LoggerFactory.getLogger(TemporaryMediaManageApi.class);
	 
	 
	    /**
	     * 上传多媒体文件  ----> 通过网络文件上传多媒体文件到微信公众平台
	     * @param accessToken  调用接口凭证
	     * @param type         媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
	     * @param mediaFileUrl 媒体文件url(如：192.168.0.66:8080/test/upload/music.mp3)
	     * @return WeixinMedia
	     */
	    public static WeiXinMedia uploadMediaByMediaFileUrl(String accessToken, String type, String mediaFileUrl) {
	    	 WeiXinMedia weixinMedia = null;
	    	 String uploadMediaUrl = WxConfigUtil.UPLOAD_MEDIA_URL.replace("ACCESS_TOKEN", accessToken).replace("TYPE", type);
	    	  //定义数据分隔符
	         try {
	        	 String text= WeiXinUtil.uploadMediaByMediaFileUrl(uploadMediaUrl,mediaFileUrl);
	        	 if(text!=null){
	        		 JSONObject jsonObject=JSONObject.parseObject(text);
	        		 weixinMedia = new WeiXinMedia();
		             try {
		                 weixinMedia.setType(jsonObject.getString("type"));
		                 // type等于thumb时的返回结果和其他类型不一样
		                 if ("thumb".equals(type)) {
		                     weixinMedia.setMedia_id(jsonObject.getString("thumb_media_id"));
		                 } else {
		                     weixinMedia.setMedia_id(jsonObject.getString("media_id"));
		                     weixinMedia.setCreated_at(new Date(jsonObject.getLongValue("created_at") * 1000));
		                 }
		             } catch (JSONException e) {
		                 weixinMedia.setErrcode(jsonObject.getInteger("errcode"));
		                 weixinMedia.setErrmsg(WeiXinErrcodeUtil.getErrorMsg(jsonObject.getInteger("errcode")));
		                 log.error("errcode:{} errmsg:{}", jsonObject.getString("errcode") + ":" + jsonObject.getString("errmsg"));
		             }
	        	 }
	         } catch (Exception e) {
	             weixinMedia = null;
	             log.error("上传媒体文件失败:{}", e);
	             e.printStackTrace();
	         }
	         return weixinMedia;
	    }
	    
	    
	    /**
	     * 下载多媒体文件 [公众号可调用本接口来获取多媒体文件 视频文件不支持下载]
	     *
	     * @param accessToken 调用接口凭证
	     * @param mediaId     媒体文件ID
	     * @param savePath    保存路径
	     * @return String 保存文件路径
	     */
	    public static String downMedia(String accessToken, String mediaId, String savePath) {
	        String filePath = null;
	        String requestUrl = WxConfigUtil.DOWNLOAD_MEDIA_URL.replace("ACCESS_TOKEN",accessToken).replace("MEDIA_ID", mediaId);
	        HttpURLConnection conn = null;
	        BufferedInputStream bis = null;
	        FileOutputStream fos = null;
	        try {
	            URL url = new URL(requestUrl);
	            conn = (HttpURLConnection) url.openConnection();
	            conn.setDoInput(true);
	            conn.setRequestMethod("GET");
	            //savePath===>192.168.0.66:8080/test/upload/
	            if (!savePath.endsWith("/")) {
	                savePath += "/";
	            }
	            // 根据内容类型获取扩展名
	            String fileExt = WeiXinUtil.getDownFileExt(conn.getHeaderField("Content-Type"));
	            // 将mediaId作为文件名
	            filePath = savePath + mediaId + fileExt;

	            bis = new BufferedInputStream(conn.getInputStream());
	            File sfile = new File(filePath);
	           /* if(!sfile.exists()){
	                sfile.mkdirs();
	            }*/
	            fos = new FileOutputStream(new File(filePath));
	            byte[] buf = new byte[8096];
	            int size = 0;
	            while ((size = bis.read(buf)) != -1) {
	                fos.write(buf, 0, size);
	            }
	            log.info("下载媒体文件成功,filePath=" + filePath);
	        } catch (Exception e) {
	            filePath = null;
	            log.error("下载媒体文件失败:{}", e);
	            e.printStackTrace();
	        } finally {
	            try {
	                if (fos != null) {
	                    fos.close();
	                    fos = null;
	                } else if (bis != null) {
	                    bis.close();
	                    bis = null;
	                }
	            } catch (IOException e) {
	            	log.error("下载媒体文件失败:{}", e);
	                e.printStackTrace();
	            }
	            if (conn != null) {
	                conn.disconnect();
	            }
	        }
	        return filePath;
	    }
	 
}
