package com.legendshop.business.handle;

import javax.servlet.http.HttpServletRequest;

import com.legendshop.business.bean.message.req.BaseMessage;

/**
 * 微信消息类型
 * 
 * 
 */
public interface MsgTypeState {

	String handle(HttpServletRequest request,BaseMessage message);

}
