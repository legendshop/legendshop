package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.JobStatus;

public interface JobStatusDao  extends Dao<JobStatus, Long> {

    public abstract int deleteJobStatus(JobStatus jobStatus);
	
	public abstract Long saveJobStatus(JobStatus jobStatus);
	
	public abstract int updateJobStatus(JobStatus jobStatus);
	

	public abstract JobStatus getJobStatusByJobName(String jobName);

	public abstract void deleteJobStatus(String name);
}
