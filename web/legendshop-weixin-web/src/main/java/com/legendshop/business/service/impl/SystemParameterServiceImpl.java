/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.config.SystemParameterProvider;
import com.legendshop.business.dao.SystemParameterDao;
import com.legendshop.business.service.SystemParameterService;
import com.legendshop.model.entity.SystemParameter;

/**
 * 系统参数
 */
@Service("systemParameterService")
public class SystemParameterServiceImpl implements SystemParameterService, InitializingBean {

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(SystemParameterServiceImpl.class);
	
	@Autowired
	private SystemParameterProvider systemParameterProvider;

	/** The base dao. */
	@Autowired
	private SystemParameterDao systemParameterDao;

	public void initSystemParameter() {
		System.out.println("init");  
		List<SystemParameter> list = systemParameterDao.loadAll();
		systemParameterProvider.setParameter(list);
		log.info("System Parameter size = {}", list.size());
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		log.info("Start to init system parameter");
		initSystemParameter();
	}
}
