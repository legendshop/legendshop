package com.legendshop.business.api.message;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.util.WeiXinErrcodeUtil;
import com.legendshop.business.bean.user.WeiXinUserInfo;
import com.legendshop.business.bean.user.WeixinUserList;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.util.AppUtils;
import com.legendshop.util.HttpUtil;

/**
 * 获取用户基本信息的操作API
 * 
 * @author tony
 */
public class UsersManageApi {
	
	 private static Logger log = LoggerFactory.getLogger(UsersManageApi.class);
	 
	 

	/**
	 * 获取用户信息
	 * 
	 * @param accessToken
	 *            接口访问凭证
	 * @param openId
	 *            用户标识
	 * @return WeixinUserInfo
	 */
	public static WeiXinUserInfo getUserInfo(String accessToken, String openId) {
		WeiXinUserInfo weixinUserInfo = null;
		// 拼接请求地址
		String requestUrl = WxConfigUtil.GET_PERSONALINF_URL.replace("ACCESS_TOKEN",
				accessToken).replace("OPENID", openId);
		// 获取用户信息
		 String text=HttpUtil.httpsRequest(requestUrl,	"GET", null);
	 	 if(text!=null){
	         JSONObject jsonObject=JSONObject.parseObject(text);
	         if(jsonObject!=null ){
	        	  try {
		                weixinUserInfo = new WeiXinUserInfo();
		                // 用户的标识
		                weixinUserInfo.setOpenid(jsonObject.getString("openid"));
		                // 关注状态（1是关注，0是未关注），未关注时获取不到其余信息
		                weixinUserInfo.setSubscribe(jsonObject.getIntValue("subscribe"));
		                // 用户关注时间
		                weixinUserInfo.setSubscribe_time(jsonObject.getString("subscribe_time"));
		                // 昵称
		                weixinUserInfo.setNickname(jsonObject.getString("nickname"));
		                // 用户的性别（1是男性，2是女性，0是未知）
		                int sexv=jsonObject.getIntValue("sex");
		                String sex="未知";
		                if(sexv==1){
		                	sex="男性";
		                }else if(sexv==2){
		                	sex="女性";
		                }
		                weixinUserInfo.setSex(sex);
		                // 用户所在国家
		                weixinUserInfo.setCountry(jsonObject.getString("country"));
		                // 用户所在省份
		                weixinUserInfo.setProvince(jsonObject.getString("province"));
		                // 用户所在城市
		                weixinUserInfo.setCity(jsonObject.getString("city"));
		                // 用户的语言，简体中文为zh_CN
		                weixinUserInfo.setLanguage(jsonObject.getString("language"));
		                // 用户头像
		                weixinUserInfo.setHeadimgurl(jsonObject.getString("headimgurl"));
		                
		                weixinUserInfo.setUnionid(jsonObject.getString("unionid"));
		                
		                weixinUserInfo.setRemark(jsonObject.getString("remark"));
		                
		                weixinUserInfo.setGroupid(jsonObject.getString("groupid"));
		                
		            } catch (Exception e) {
		                if (0 == weixinUserInfo.getSubscribe()) {
		                    log.error("用户{}已取消关注", weixinUserInfo.getOpenid());
		                } else {
		                    int errorCode = jsonObject.getIntValue("errcode");
		                    log.error("获取用户信息失败 errcode:{} errmsg:{}", errorCode, WeiXinErrcodeUtil.getErrorMsg(errorCode));
		                }
		            }
	         }
	 	}
		return weixinUserInfo;
	}
	
	
	/**
	 * 获取所有关注用户信息信息
	 * @return
	 * @throws WexinReqException 
	 */
	public static WeixinUserList getAllWxuser(String accesstoken,String next_openid) {
		 WeixinUserList list=new WeixinUserList();;
		 if (null == next_openid)
			 next_openid = "";
		// 获取所有的用户列表信息 ; 默认微信是10000条数据
	    String requestUrl = WxConfigUtil.GET_USERLIST_URL.replace("ACCESS_TOKEN",accesstoken).replace("NEXT_OPENID", next_openid);
	    // 获取用户信息
	 	String text=HttpUtil.httpsRequest(requestUrl,	"GET", null);
	 	if(AppUtils.isBlank( text)){
	 		return null;
	 	}
	 	JSONObject jsonObject=JSONObject.parseObject(text);
	 	if(jsonObject.containsKey("errcode")){
     		list.setErrcode(jsonObject.getInteger("errcode"));
     		list.setErrmsg(WeiXinErrcodeUtil.getErrorMsg(jsonObject.getInteger("errcode")));
     		return list;
    	}
		int count = jsonObject.getIntValue("count");
		if (count > 0) { //判断有没有数据
         	list.setNextOpenId(jsonObject.getString("next_openid"));
			JSONObject dataObject = (JSONObject) jsonObject.get("data");
			JSONArray lstOpenid = dataObject.getJSONArray("openid");
			if(lstOpenid.isEmpty()){
				return null;
			 }
			List<String> openids=new ArrayList<String>();
			for (Object object : lstOpenid) {
				if(object!=null){
					openids.add(String.valueOf(object));
				}
			}
			int iSize = openids.size();
		
			List<WeiXinUserInfo> userInfos=new ArrayList<WeiXinUserInfo>(); //所有关注微信的用户信息
			
			System.out.println("------同步获取到微信用户数据 ---------"+count);
			int init = 100;// 每隔100条循环一次
			int cycelTotal = iSize / init;  //循环多少次  
	        if (iSize % init != 0) {  
	        	cycelTotal += 1;  
	            if (iSize < init) {  
	                init = iSize;  
	            }  
	        }  
	        System.out.println("----批量获取用户的次数："+cycelTotal);//循环多少次 
	        int index=0;
	        int total=0;
	        for (int i = 0; i < cycelTotal; i++) {  
	        	JSONArray jsonArray=new JSONArray();
	        	for (int j = 0; j < init; j++) {  
	        		 if(index>=iSize){
	        			break;
	        		 }
	        		 String openId = openids.get(index); //IndexOutOfBoundsException
	        		 JSONObject object = new JSONObject();
					 object.put("openid", openId);
					 object.put("lang", "zh-CN");
					 jsonArray.add(object);
					 index=index+1;
					 System.out.println("----批量获取index  -------="+ index +"-------");
	            }  
	        	JSONObject  user_list= new JSONObject();
				user_list.put("user_list", jsonArray);
			    jsonArray=null;
			    
			    System.out.println("----批量获取次数="+ i +"-------");
			    //批量获取用户信息内容
				 String  result=HttpUtil.httpsRequest(WxConfigUtil.GET_BATCHGET_USERLIST_URL.replace("ACCESS_TOKEN",accesstoken),	"POST", user_list.toString());
				 if(AppUtils.isBlank(result)){
					 continue;
				 }
				 JSONObject userObject=JSONObject.parseObject(result);
				 if(userObject.containsKey("errcode")){
			     	continue;
			     }
				 JSONArray info_list = userObject.getJSONArray("user_info_list");
				 if(info_list.isEmpty()){
					 continue;
				 }
				 for (Iterator<Object> iterator = info_list.iterator(); iterator.hasNext();) {
					 JSONObject object2 = (JSONObject) iterator.next();
					 WeiXinUserInfo userInfo=buildWeiXinUserInfo(object2);
					 userInfos.add(userInfo);
					 total++;
				 }
				 info_list=null;
	        }
        	list.setCount(total);
        	list.setTotal(total);
        	list.setUserInfos(userInfos);
		}
		return list;
	}
	
	
	
	private static WeiXinUserInfo buildWeiXinUserInfo(JSONObject jsonObject){
		
		WeiXinUserInfo weixinUserInfo = new WeiXinUserInfo();
         // 用户的标识
         weixinUserInfo.setOpenid(jsonObject.getString("openid"));
         // 关注状态（1是关注，0是未关注），未关注时获取不到其余信息
         weixinUserInfo.setSubscribe(jsonObject.getIntValue("subscribe"));
         // 用户关注时间
         weixinUserInfo.setSubscribe_time(jsonObject.getString("subscribe_time"));
         // 昵称
         weixinUserInfo.setNickname(jsonObject.getString("nickname"));
         // 用户的性别（1是男性，2是女性，0是未知）
         int sexv=jsonObject.getIntValue("sex");
         String sex="未知";
         if(sexv==1){
         	sex="男性";
         }else if(sexv==2){
         	sex="女性";
         }
         weixinUserInfo.setSex(sex);
         // 用户所在国家
         weixinUserInfo.setCountry(jsonObject.getString("country"));
         // 用户所在省份
         weixinUserInfo.setProvince(jsonObject.getString("province"));
         // 用户所在城市
         weixinUserInfo.setCity(jsonObject.getString("city"));
         // 用户的语言，简体中文为zh_CN
         weixinUserInfo.setLanguage(jsonObject.getString("language"));
         // 用户头像
         weixinUserInfo.setHeadimgurl(jsonObject.getString("headimgurl"));
         
         weixinUserInfo.setUnionid(jsonObject.getString("unionid"));
         
         weixinUserInfo.setRemark(jsonObject.getString("remark"));
         
         weixinUserInfo.setGroupid(jsonObject.getString("groupid"));
         
		 return weixinUserInfo; 
	}
	
	
	/**
	 * 设置用户备注名
	 * @param openid 用户openid
	 * @param remark 新的备注名，长度必须小于30字符
	 * @return
	 */
	public static String updateRemark(String accesstoken,String openId,String remark){
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("openid", openId);
		jsonObject.put("remark", remark);
		// 拼接请求地址
	    String requestUrl = WxConfigUtil.USER_UPDATE_REMARK_POST_URL.replace("ACCESS_TOKEN",accesstoken);
	    // 获取用户信息
	 	String text=HttpUtil.httpsRequest(requestUrl,	"POST", jsonObject.toJSONString());
	    return text;
	}


	/**
	 * 微信网页授权成功后，获取用户信息, 授权认证时的scope=snsapi_userinfo才能获取到用户信息
	 * @param accessToken
	 * @param openId
	 * @return 
	 */
	public static WeiXinUserInfo snsCallBackUserInfo(String accessToken, String openId) {
		WeiXinUserInfo weixinUserInfo = null;
		// 拼接请求地址
		String requestUrl = WxConfigUtil.SNS_USERINFO.replace("ACCESS_TOKEN",accessToken).replace("OPENID", openId);
		// 获取用户信息
		 String text=HttpUtil.httpsRequest(requestUrl,"GET", null);
	 	 if(text!=null){
	         JSONObject jsonObject=JSONObject.parseObject(text);
	         if(jsonObject!=null ){
	        	  try {
		                weixinUserInfo = new WeiXinUserInfo();
		                // 用户的标识
		                weixinUserInfo.setOpenid(jsonObject.getString("openid"));
		                // 关注状态（1是关注，0是未关注），未关注时获取不到其余信息
		                weixinUserInfo.setSubscribe(0);
		                // 昵称
		                weixinUserInfo.setNickname(jsonObject.getString("nickname"));
		                // 用户的性别（1是男性，2是女性，0是未知）
		                int sexv=jsonObject.getIntValue("sex");
		                String sex="未知";
		                if(sexv==1){
		                	sex="男性";
		                }else if(sexv==2){
		                	sex="女性";
		                }
		                weixinUserInfo.setSex(sex);
		                // 用户所在国家
		                weixinUserInfo.setCountry(jsonObject.getString("country"));
		                // 用户所在省份
		                weixinUserInfo.setProvince(jsonObject.getString("province"));
		                // 用户所在城市
		                weixinUserInfo.setCity(jsonObject.getString("city"));
		                // 用户的语言，简体中文为zh_CN
		                weixinUserInfo.setLanguage(jsonObject.getString("language"));
		                // 用户头像
		                weixinUserInfo.setHeadimgurl(jsonObject.getString("headimgurl"));
		                
		                weixinUserInfo.setUnionid(jsonObject.getString("unionid"));
		                
		                weixinUserInfo.setNickname(jsonObject.getString("nickname"));
		                
		                weixinUserInfo.setRemark(jsonObject.getString("remark"));
		                
		                weixinUserInfo.setGroupid(jsonObject.getString("groupid"));
		                
		            } catch (Exception e) {
		            	int errorCode = jsonObject.getIntValue("errcode");
	                    log.error("sns 获取用户信息失败 errcode:{} errmsg:{}", errorCode, WeiXinErrcodeUtil.getErrorMsg(errorCode));
		            }
	         }
	 	}
		return weixinUserInfo;
	}
	
	
}
