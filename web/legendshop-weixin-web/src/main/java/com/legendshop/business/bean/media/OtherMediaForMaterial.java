package com.legendshop.business.bean.media;

public class OtherMediaForMaterial extends MediaForMaterial {

	/** 视频素材的标题 */
	private String title;
	/** 视频素材的描述 */
	private String introduction;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

}
