package com.legendshop.business.bean.message.resp;

/**
 * 语音消息  [响应用户]
 */
public class VoiceMessageResp extends BaseMessage {
    // 语音
    private Voice Voice;

    public Voice getVoice() {
        return Voice;
    }

    public void setVoice(Voice voice) {
        Voice = voice;
    }
}
