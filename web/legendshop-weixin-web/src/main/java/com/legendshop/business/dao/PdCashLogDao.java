package com.legendshop.business.dao;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.PdCashLog;

public interface PdCashLogDao extends Dao<PdCashLog, Long> {

	public abstract Long savePdCashLog(PdCashLog pdCashLog);
	
}
