package com.legendshop.business.util;

public class WxConfigUtil {
	

    // 接收消息类型：文本
    public final static String RECRIVE_TEXT = "text";
    // 接收消息类型：图片
    public final static String RECRIVE_IMAGE = "image";
    // 接收消息类型：语音
    public final static String RECRIVE_VOICE = "voice";
    // 接收消息类型：视频
    public final static String RECRIVE_VIDEO = "video";
    // 接收消息类型：地理位置
    public final static String RECRIVE_LOCATION = "location";
    // 接收消息类型：链接
    public final static String RECRIVE_LINK = "link";
    // 接收消息类型：推送
    public final static String RECRIVE_EVENT = "event";

    // 回复消息类型：文本
    public final static String REQUEST_TEXT = "text";
    // 回复消息类型：图片
    public final static String REQUEST_IMAGE = "image";
    // 回复消息类型：语音
    public final static String REQUEST_VOICE = "voice";
    // 回复消息类型：视频
    public final static String REQUEST_VIDEO = "video";
    // 回复消息类型：音乐
    public final static String REQUEST_MUSIC = "music";
    // 回复消息类型：图文
    public final static String REQUEST_NEWS = "news";

    // 事件类型：subscribe(订阅)
    public final static String EVENT_SUBSCRIBE = "subscribe";
    // 事件类型：unsubscribe(取消订阅)
    public final static String EVENT_UNSUBSCRIBE = "unsubscribe";
    // 事件类型：LOCATION(上报地理位置事件)
    public final static String EVENT_LOCATION = "LOCATION";
    // 事件类型：CLICK(自定义菜单点击事件)
    public final static String EVENT_CLICK = "CLICK";
    

    /**
     * 扫描带参数二维码事件
     * 用户扫描带场景值二维码时，可能推送以下两种事件：
     * 1.如果用户还未关注公众号，则用户可以关注公众号，关注后微信会将带场景值关注事件推送给开发者。
     * 2.如果用户已经关注公众号，则微信会将带场景值扫描事件推送给开发者。
     */
    // 事件类型：subscribe(用户未关注时，进行关注后的事件推送)
    public final static String EVENT_QRCODE_SUBSCRIBE = "subscribe";
    // 事件类型：scan(用户已关注时的事件推送)
    public final static String EVENT_QRCODE_SCAN = "scan";
    
    
	
	/**
	 * 微信基础接口地址
	 */
	// 获取access_token的接口地址（GET） 限200（次/天）
	public final static String TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
	// oauth2授权接口(GET)
	public final static String OAUTH2_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
	// 刷新access_token接口（GET）
	public final static String REFRESH_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=APPID&grant_type=refresh_token&refresh_token=REFRESH_TOKEN";
	// 菜单创建接口（POST）
	public final static String MENU_CREATE_URL = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";
	// 菜单查询（GET）
	public final static String MENU_GET_URL = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=ACCESS_TOKEN";
	// 菜单删除（GET）
	public final static String MENU_DELETE_URL = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN";
	
	// 创建分组（GET）
	public final static String GROUP_CREATE_URL = "https://api.weixin.qq.com/cgi-bin/groups/create?access_token=ACCESS_TOKEN";
	
	// 查询所有分组（GET）
	public final static String GROUP_GET_URL = "https://api.weixin.qq.com/cgi-bin/groups/get?access_token=ACCESS_TOKEN";
	
	//查询用户所在分组
	public final static String GROUP_GETID_URL = "https://api.weixin.qq.com/cgi-bin/groups/getid?access_token=ACCESS_TOKEN";
	
	//修改分组名
	public final static String GROUP_UPDATE_URL = "https://api.weixin.qq.com/cgi-bin/groups/update?access_token=ACCESS_TOKEN";
	
	//移动用户分组
	public final static String GROUP_MOVE_URL = "https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=ACCESS_TOKEN";
	
	public final static String GROUP_DELETE_URL = "https://api.weixin.qq.com/cgi-bin/groups/delete?access_token=ACCESS_TOKEN";
	
	//批量更新用户组信息
	public final static String GROUP_BATCH_UPDATE_URL = "https://api.weixin.qq.com/cgi-bin/groups/members/batchupdate?access_token=ACCESS_TOKEN";
	
	
	
	
	/**
	 * 微信支付接口地址
	 */
	// 微信支付统一接口(POST)
	public final static String UNIFIED_ORDER_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
	// 微信退款接口(POST)
	public final static String REFUND_URL = "https://api.mch.weixin.qq.com/secapi/pay/refund";
	// 订单查询接口(POST)
	public final static String CHECK_ORDER_URL = "https://api.mch.weixin.qq.com/pay/orderquery";
	// 关闭订单接口(POST)
	public final static String CLOSE_ORDER_URL = "https://api.mch.weixin.qq.com/pay/closeorder";
	// 退款查询接口(POST)
	public final static String CHECK_REFUND_URL = "https://api.mch.weixin.qq.com/pay/refundquery";
	// 对账单接口(POST)
	public final static String DOWNLOAD_BILL_URL = "https://api.mch.weixin.qq.com/pay/downloadbill";
	// 短链接转换接口(POST)
	public final static String SHORT_URL = "https://api.mch.weixin.qq.com/tools/shorturl";
	// 接口调用上报接口(POST)
	public final static String REPORT_URL = "https://api.mch.weixin.qq.com/payitil/report";
	
	/**
	 * ------------------------------------------高级群发接口---------------------------------------------------
	 */
	//客服接口-发消息
	public static final String MESSAGE_URL = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=ACCESS_TOKEN";

	/*
	 * 根据分组进行群发【订阅号与服务号认证后均可用】 
	 */
	public static final String MASS_SENDALL_URL = "https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=ACCESS_TOKEN";
	
	/*
	 * 根据OpenID列表群发【订阅号不可用，服务号认证后可用】 
	 */
	public static final String MASS_SEND_URL = "https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=ACCESS_TOKEN";
	
	/*
	 * 删除群发【订阅号与服务号认证后均可用】 
	 */
	public static final String MASS_DELETE_URL = "https://api.weixin.qq.com//cgi-bin/message/mass/delete?access_token=ACCESS_TOKEN";
	
	/*
	 * 预览接口【订阅号与服务号认证后均可用】 
	 * 为了满足第三方平台开发者的需求，在保留对openID预览能力的同时，增加了对指定微信号发送预览的能力，但该能力每日调用次数有限制（100次），请勿滥用
	 */
	public static final String MASS_PREVIEW = "https://api.weixin.qq.com/cgi-bin/message/mass/preview?access_token=ACCESS_TOKEN";
	
	/*
	 * 查询群发消息发送状态【订阅号与服务号认证后均可用】 
	 */
	public static final String MASS_GET = "https://api.weixin.qq.com/cgi-bin/message/mass/get?access_token=ACCESS_TOKEN";
	
	/**
	 * ------------------------------------------高级群发接口---------------------------------------------------
	 */
	
	/**
	 * ------------------------------------------模板消息接口---------------------------------------------------
	 */
	
	/*
	 * 发送模板消息 
	 */
	public static final String TEMPLATE_SEND_URL = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";

	/*
	 * 设置所属行业 
	 */
	public static final String TEMPLATE_API_SET_INDUSTRY = "https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token=ACCESS_TOKEN";

	
	/*
	 * 获得模板ID 
	 */
	public static final String TEMPLATE_API_ADD_TEMPLATE = "https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token=ACCESS_TOKEN";
	
	/**
	 * ------------------------------------------模板消息接口---------------------------------------------------
	 */
	
	/**
	 *  ------------------------------------------订单发货接口---------------------------------------------------
	 */
	/*
	 * 发货通知
	 */
	public static final String DELIVERNOTIFY_URL = "https://api.weixin.qq.com/pay/delivernotify?access_token=ACCESS_TOKEN";


    // 开发者可通过OpenID来获取用户基本信息 url
	public final static String GET_PERSONALINF_URL = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";

    // 获取关注者列表url
	public final static String GET_USERLIST_URL = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&next_openid=NEXT_OPENID";

	public final static String GET_BATCHGET_USERLIST_URL = "https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=ACCESS_TOKEN";

	//设置用户备注名
	public static final String USER_UPDATE_REMARK_POST_URL="https://api.weixin.qq.com/cgi-bin/user/info/updateremark?access_token=ACCESS_TOKEN";
    
	
	  // 上传多媒体文件url
	public final static String UPLOAD_MEDIA_URL = "http://file.api.weixin.qq.com/cgi-bin/media/upload?access_token=ACCESS_TOKEN&type=TYPE";
    // 下载多媒体文件url
	public final static String DOWNLOAD_MEDIA_URL = "http://file.api.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID";
    
	// 新增永久图文素材
	public static String MATERIAL_ADD_NEWS_URL ="https://api.weixin.qq.com/cgi-bin/material/add_news?access_token=ACCESS_TOKEN";
	// 新增其他类型永久素材
	public static String MATERIAL_ADD_MATERIAL_URL = "https://api.weixin.qq.com/cgi-bin/material/add_material?access_token=ACCESS_TOKEN";
	// 获取永久素材
	public static String MATERIAL_GET_MATERIAL_URL = "https://api.weixin.qq.com/cgi-bin/material/get_material?access_token=ACCESS_TOKEN";
	// 获取素材总数
	public final static String MATERIAL_GET_MATERIALCOUNT_URL = "https://api.weixin.qq.com/cgi-bin/material/get_materialcount?access_token=ACCESS_TOKEN";
	// 修改永久图文素材
	public final static String MATERIAL_UPDATE_NEWS_URL = "https://api.weixin.qq.com/cgi-bin/material/update_news?access_token=ACCESS_TOKEN";
	// 获取素材列表
	public final static String MATERIAL_BATCHGET_MATERIAL_URL = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=ACCESS_TOKEN";
	
	//
	public final static String MATERIAL_DEL_MATERIAL_URL ="https://api.weixin.qq.com/cgi-bin/material/del_material?access_token=ACCESS_TOKEN";
	
	public final static String SNS_USERINFO = "https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";
	
	public  final static  String createOrderURL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
	
	/** 微信公众号APPID */
	public static String APPID = "APPID";
	/** 微信公众号绑定的商户号 */
	public static String MCH_ID = "MCH_ID";
	public static String APPSECRET = "APPSECRET";
	public static String PARTNERKEY = "PARTNERKEY";
	public static String TOKEN = "TOKEN";
	
	
}
