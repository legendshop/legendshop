package com.legendshop.business.api.message;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.business.bean.message.resp.Article;
import com.legendshop.business.bean.message.resp.Music;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.util.HttpUtil;
import com.legendshop.util.JSONUtil;

/**
 * 客服消息接口
 * 
 * @Tony
 * @description 当用户主动发消息给公众号的时候（包括发送信息、点击自定义菜单、订阅事件、扫描二维码事件、支付成功事件、用户维权），
 *              微信将会把消息数据推送给开发者，开发者在一段时间内（目前修改为48小时）可以调用客服消息接口，
 *              通过POST一个JSON数据包来发送消息给普通用户，在48小时内不限制发送次数。
 *              此接口主要用于客服等有人工消息处理环节的功能，方便开发者为用户提供更加优质的服务。
 */
public class MessageApi {

	/**
	 * 发送文本客服消息
	 * 
	 * @param openId
	 * @param text
	 * @throws Exception
	 */
	public static String sendText(String accessToken, String openId, String text)
			throws Exception {

		text = text.replace("\"", "\\\"");
		String jsonMsg = "{\"touser\":\"%s\",\"msgtype\":\"text\",\"text\":{\"content\":\"%s\"}}";
		String content = String.format(jsonMsg, openId, text);
		/*
		 * Map<String, Object> textObj = new HashMap<String, Object>();
		 * textObj.put("content", text);//文本消息内容 json.put("touser", openId);
		 * //普通用户openid json.put("msgtype", "text");
		 * //消息类型，文本为text，图片为image，语音为voice
		 * ，视频消息为video，音乐消息为music，图文消息为news，卡券为wxcard json.put("text", textObj);
		 * String result = sendMsg(accessToken, json);
		 */
		return sendCustomMessage(accessToken, content);
	}

	/**
	 * 组装图片客服消息
	 * 
	 * @param openId
	 *            消息发送对象
	 * @param mediaId
	 *            媒体文件id
	 * @return
	 */
	public static String sendImageCustomMessage(String accessToken,
			String openId, String mediaId) {
		String jsonMsg = "{\"touser\":\"%s\",\"msgtype\":\"image\",\"image\":{\"media_id\":\"%s\"}}";
		String content = String.format(jsonMsg, openId, mediaId);
		return sendCustomMessage(accessToken, content);
	}

	/**
	 * 组装语音客服消息
	 * 
	 * @param openId
	 *            消息发送对象
	 * @param mediaId
	 *            媒体文件id
	 * @return
	 */
	public static String sendVoiceCustomMessage(String accessToken,
			String openId, String mediaId) {
		String jsonMsg = "{\"touser\":\"%s\",\"msgtype\":\"voice\",\"voice\":{\"media_id\":\"%s\"}}";
		String content = String.format(jsonMsg, openId, mediaId);
		return sendCustomMessage(accessToken, content);
	}

	/**
	 * 组装视频客服消息
	 * 
	 * @param openId
	 *            消息发送对象
	 * @param mediaId
	 *            媒体文件id
	 * @param thumbMediaId
	 *            视频消息缩略图的媒体id
	 * @return
	 */
	public static String sendVideoCustomMessage(String accessToken,
			String openId, String mediaId, String thumbMediaId) {
		String jsonMsg = "{\"touser\":\"%s\",\"msgtype\":\"video\",\"video\":{\"media_id\":\"%s\",\"thumb_media_id\":\"%s\"}}";
		String content = String.format(jsonMsg, openId, mediaId, thumbMediaId);
		return sendCustomMessage(accessToken, content);
	}

	/**
	 * 组装音乐客服消息
	 * 
	 * @param openId
	 *            消息发送对象
	 * @param music
	 *            音乐对象
	 * @return
	 */
	public static String sendMusicCustomMessage(String accessToken,
			String openId, Music music) {
		String jsonMsg = "{\"touser\":\"%s\",\"msgtype\":\"music\",\"music\":%s}";
		jsonMsg = String.format(jsonMsg, openId, JSONUtil.getJson(music));
		// 将jsonMsg中的thumbmediaid替换为thumb_media_id
		jsonMsg = jsonMsg.replace("thumbmediaid", "thumb_media_id");
		return sendCustomMessage(accessToken, jsonMsg);
	}

	/**
	 * 组装图文客服消息
	 * 
	 * @param openId
	 *            消息发送对象
	 * @param articleList
	 *            图文消息列表
	 * @return
	 */
	public static String makeNewsCustomMessage(String accessToken,
			String openId, List<Article> articleList) {
		String jsonMsg = "{\"touser\":\"%s\",\"msgtype\":\"news\",\"news\":{\"articles\":%s}}";
		jsonMsg = String.format(jsonMsg, openId, JSONUtil.getJson(articleList)
				.replaceAll("\"", "\\\""));
		// 将jsonMsg中的picUrl替换为picurl
		jsonMsg = jsonMsg.replace("picUrl", "picurl");
		return sendCustomMessage(accessToken, jsonMsg);
	}

	/**
	 * 发送客服消息
	 * 
	 * @param accessToken
	 *            接口访问凭证
	 * @param jsonMsg
	 *            json格式的客服消息（包括touser、msgtype和消息内容）
	 * @return true | false
	 */
	private static String sendCustomMessage(String accessToken, String jsonMsg) {
		// 拼接请求地址
		String requestUrl = WxConfigUtil.MESSAGE_URL.replace("ACCESS_TOKEN",
				accessToken);
		// 发送客服消息
		// 获取用户信息
		String text= HttpUtil.httpsRequest(requestUrl, "POST",jsonMsg);
		return text;
	}

	/**
	 * 发送客服消息
	 * 
	 * @param accessToken
	 * @param message
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	private static String sendMsg(String accessToken,
			Map<String, Object> messageparam) throws Exception {
		String message_url = WxConfigUtil.MESSAGE_URL.concat(accessToken);

		String message = JSONObject.toJSONString(messageparam);

		String result = JSONObject.toJSONString(HttpUtil.httpsRequest(
				message_url, "POST", message));

		return result;
	}

}
