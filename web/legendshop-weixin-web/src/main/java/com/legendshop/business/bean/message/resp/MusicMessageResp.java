package com.legendshop.business.bean.message.resp;

/**
 * 音乐消息  [响应用户]
 */
public class MusicMessageResp extends BaseMessage {
    // 音乐
    private Music Music;

    public Music getMusic() {
        return Music;
    }

    public void setMusic(Music music) {
        Music = music;
    }
}
