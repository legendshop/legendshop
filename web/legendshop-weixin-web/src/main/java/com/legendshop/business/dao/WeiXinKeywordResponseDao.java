/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.weixin.WeiXinKeywordResponse;

/**
 * The Class WeiXinKeywordResponseDao.
 */

public interface WeiXinKeywordResponseDao extends GenericDao<WeiXinKeywordResponse, Long> {
     
	public abstract List<WeiXinKeywordResponse> getWeiXinKeywordResponse();
	
 }
