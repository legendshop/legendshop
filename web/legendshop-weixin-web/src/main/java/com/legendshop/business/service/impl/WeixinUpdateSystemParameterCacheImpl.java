/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.business.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.framework.cache.listener.UpdateSystemParameterCache;
import com.legendshop.base.config.SystemParameterProvider;
import com.legendshop.framework.cache.caffeine.CacheMessage;
import com.legendshop.model.constant.SystemCacheEnum;
import com.legendshop.model.dto.SystemParameterDto;

/**
 * 更新本地缓存.
 */
@Service("weixinUpdateCache")
public class WeixinUpdateSystemParameterCacheImpl implements UpdateSystemParameterCache {
	
	private final Logger logger = LoggerFactory.getLogger(WeixinUpdateSystemParameterCacheImpl.class);
	
	/** 系统配置. */
	@Autowired
	private SystemParameterProvider systemParameterProvider;

	/**
	 * 更新本地缓存的操作
	 */
	@Override
	public void onMessage(CacheMessage cacheMessage) {
		if(SystemCacheEnum.PROPERTIES.value().equals(cacheMessage.getCacheName())){
			SystemParameterDto keyValue = (SystemParameterDto)cacheMessage.getKey();
			logger.warn("notify other node to update their cache {}, key {}", cacheMessage.getCacheName(), keyValue.getKey());
			systemParameterProvider.refreshLocalCache(keyValue.getGroupId(), keyValue.getKey(), keyValue.getValue(), true);
		}
	}
	
}
