/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service;

import java.util.List;

import com.legendshop.model.entity.weixin.WeixinAutoresponse;

/**
 * The Class WeixinAutoresponseService.
 */
public interface WeixinAutoresponseService  {

    public List<WeixinAutoresponse> getWeixinAutoresponse(String userName);

    public WeixinAutoresponse getWeixinAutoresponse(Long id);
    
    public void deleteWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse);
    
    public Long saveWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse);

    public void updateWeixinAutoresponse(WeixinAutoresponse weixinAutoresponse);

    /**
     * 遍历关键字管理中是否存在用户输入的关键字信息
     * @param msgTriggerType
     * @return
     */
	public WeixinAutoresponse findAutoResponseMsgTriggerType(String msgTriggerType);
	
	
	
}
