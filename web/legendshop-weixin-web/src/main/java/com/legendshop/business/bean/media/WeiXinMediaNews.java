package com.legendshop.business.bean.media;


/**
 * 获取素材列表
 * @author tony
 *
 */
public class WeiXinMediaNews {

	
	/** 媒体id */
	private String total_count;
	/** 图文消息的作者 */
	private WeiXinMediaNewItems item;
	/** 文件名称 */
	private String item_count;
	public String getTotal_count() {
		return total_count;
	}
	public void setTotal_count(String total_count) {
		this.total_count = total_count;
	}
	public WeiXinMediaNewItems getItem() {
		return item;
	}
	public void setItem(WeiXinMediaNewItems item) {
		this.item = item;
	}
	public String getItem_count() {
		return item_count;
	}
	public void setItem_count(String item_count) {
		this.item_count = item_count;
	}

	@Override
	public String toString() {
		return "WxArticle [total_count=" + total_count + ", item_count=" + item_count + ", item=" + item + "]";
	}

	
	
	
}
