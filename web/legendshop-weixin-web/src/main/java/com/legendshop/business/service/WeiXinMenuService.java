package com.legendshop.business.service;

import java.util.List;

import com.legendshop.business.entity.MenuEntity;



/**
 * 
 * @author tony
 *
 */
public interface WeiXinMenuService {
	
	/**
	 * 获取所有的菜单分类
	 * @return
	 */
	List<MenuEntity> getMenuEntities();


	MenuEntity findUniqueByProperty(String key);

}
