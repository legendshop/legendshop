package com.legendshop.business.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 微信服务-退款控制器
 * @author Tony
 * https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=9_4
 * @deprecated 
 */
@Controller
@RequestMapping("/wxpay/refund")
public class WeiXinServerReturnController {



	/*
	private  final  static String createOrderURL = "https://api.mch.weixin.qq.com/secapi/pay/refund";

	*//**
	 * 微信退款
	 * @param request
	 * @param response
	 * @param outTradeNo
	 * @param userId
	 * @param token
	 * @param _method
	 * @return
	 *//*
	@RequestMapping(value = "/request",method=RequestMethod.GET)
	public @ResponseBody String request(HttpServletRequest request, HttpServletResponse response){
		
		 WeiXinLog.info("WeiXinServerReturnController: request  微信退款开始");
		
		Date date=new Date(); //2016082388541146
		String out_refund_no ="2016082388541146"; // 商户系统内部的退款单号，商户系统内部唯一，同一退款单号多次请求只退一笔
		String transaction_id = "4007322001201608232042288739";// 微信生成的订单号，在支付通知中有返回
		String total_fee = "1";// 总金额
		String refund_fee = "1";// 退款金额
		// 随机字符串
	    String nonce_str = RandomStringUtils.randomNumeric(3,12); //随机字符串，不长于32位
		//String op_user_id = "";//就是MCHID
		//String partnerkey = "";//商户平台上的那个KEY
		SortedMap<String, String> packageParams = new TreeMap<String, String>();
		packageParams.put("appid", WxConfigUtil.APPID);
		packageParams.put("mch_id", WxConfigUtil.MCH_ID);
		packageParams.put("nonce_str", nonce_str);
		packageParams.put("transaction_id", transaction_id);
		packageParams.put("out_refund_no", out_refund_no);
		packageParams.put("total_fee", total_fee);
		packageParams.put("refund_fee", refund_fee);
		packageParams.put("op_user_id", WxConfigUtil.MCH_ID);
		RequestHandler reqHandler = new RequestHandler(null, null);
		reqHandler.init(WxConfigUtil.TOKEN,WxConfigUtil.APPID, WxConfigUtil.APPSECRET, WxConfigUtil.PARTNERKEY);
		
		String sign = reqHandler.createSign(packageParams);
		
		StringBuilder builder=new StringBuilder("<xml>");
		builder.append("<appid>").append(WxConfigUtil.APPID).append("</appid>");
		builder.append("<mch_id>").append(WxConfigUtil.MCH_ID).append("</mch_id>");
		builder.append("<nonce_str>").append(nonce_str).append("</nonce_str>");
		builder.append("<op_user_id>").append(WxConfigUtil.MCH_ID).append("</op_user_id>");
		builder.append("<out_refund_no>").append(out_refund_no).append("</out_refund_no>");
		builder.append("<refund_fee>").append(refund_fee).append("</refund_fee>");
		builder.append("<total_fee>").append(total_fee).append("</total_fee>");
		builder.append("<transaction_id>").append(transaction_id).append("</transaction_id>");
		builder.append("<sign>").append(sign).append("</sign>");
		builder.append("</xml>");
		
		try {
			String s= ClientCustomSSL.doRefund(createOrderURL, builder.toString(),WxConfigUtil.MCH_ID);
			 WeiXinLog.info("WeiXinServerReturnController: request  微信退款开始 {}",s);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "FAIL";
	}
	*/
	
	
}
