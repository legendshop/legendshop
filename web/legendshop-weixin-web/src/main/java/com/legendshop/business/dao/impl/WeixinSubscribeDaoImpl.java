/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinSubscribeDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.weixin.WeixinSubscribe;

/**
 * The Class WeixinSubscribeDaoImpl.
 */

@Repository
public class WeixinSubscribeDaoImpl extends GenericDaoImpl<WeixinSubscribe, Long> implements WeixinSubscribeDao  {

	@Override
	//@Cacheable(value="WeixinSubscribeList")
	public List<WeixinSubscribe> getWeixinSubscribe() {
		return this.queryLimit("select sc.addtime as addtime, sc.msgType as msgtype,sc.templateid as templateId,sc.templatename as templatename,sc.content as content from ls_weixin_subscribe as sc order by addtime desc", WeixinSubscribe.class, 0, 1);
	}
     
    
 }
