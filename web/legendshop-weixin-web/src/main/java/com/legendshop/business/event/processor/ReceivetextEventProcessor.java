package com.legendshop.business.event.processor;

import com.legendshop.business.service.WeixinReceivetextService;
import com.legendshop.framework.event.processor.ThreadProcessor;
import com.legendshop.model.entity.weixin.WeixinReceivetext;
import com.legendshop.util.AppUtils;

public class ReceivetextEventProcessor extends ThreadProcessor<WeixinReceivetext> {

	
	private WeixinReceivetextService weixinReceivetextService;
	
	@Override
	public void process(WeixinReceivetext receivetext) {
		if(AppUtils.isNotBlank(receivetext)){
			weixinReceivetextService.saveWeixinReceivetext(receivetext);
		}
	}

	public void setWeixinReceivetextService(
			WeixinReceivetextService weixinReceivetextService) {
		this.weixinReceivetextService = weixinReceivetextService;
	}
	
	

}
