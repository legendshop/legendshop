package com.legendshop.business.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.legendshop.business.service.WeixinNewsitemService;
import com.legendshop.model.entity.weixin.WeixinNewsitem;
import com.legendshop.util.AppUtils;
import com.legendshop.web.util.WeiXinUtil;

@Controller
@RequestMapping("/news")
public class WeiXinNewsItemController {
	
	@Autowired
	private WeixinNewsitemService weixinNewsitemService;

	@RequestMapping(value = "/view/{itemId}",method=RequestMethod.GET)
	public String view(HttpServletRequest req, HttpServletResponse resp,@PathVariable Long itemId) {
		if(AppUtils.isBlank(itemId)){
			return "/systemerror";  
		}
		boolean weixin=WeiXinUtil.isWeiXin(req);
		boolean mobile=WeiXinUtil.isMoblie(req);
		if(!weixin || !mobile){
			return "/error";  
		}
		WeixinNewsitem newsitem= weixinNewsitemService.getWeixinNewsitem(itemId);
		if(AppUtils.isBlank(newsitem)){
			return "/systemerror";  
		}
		else{
			req.setAttribute("newsitem", newsitem);
			return "/WeiXinNewItems";  
		}
	}
}
