package com.legendshop.business.constant;

import com.legendshop.util.constant.StringEnum;


/**
 * 微信Token类型
 * @author tony
 *
 */
public enum WxTokenTypeEnum  implements  StringEnum{
	
	ACCESS_TOKEN("access_token"), //access_token是公众号的全局唯一票据
	
	JSAPI_TICKET("jsapi_ticket"); //jsapi_ticket是公众号用于调用微信JS接口的临时票据
	
	/** The value. */
	private final String value;
	/**
	 * Instantiates a new function enum.
	 * 
	 * @param value
	 *            the value
	 */
	private WxTokenTypeEnum(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.StringEnum#value()
	 */
	public String value() {
		return this.value;
	}
}
