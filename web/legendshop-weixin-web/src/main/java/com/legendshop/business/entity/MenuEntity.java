package com.legendshop.business.entity;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;


public class MenuEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 菜单编号 */
	private Long id;

	/** 菜单名称 */
	private String name;

	/** 菜单类型 */
	private String menuType;

	/** 菜单Url */
	private String menuUrl;

	/** 菜单文本类型 */
	private String infoType;

	/** 菜单素材模版ID */
	private Long templateId;

	/** 菜单级别 */
	private Integer level;

	/** 父节点 */
	private Long parentId;

	/** 排序 */
	private Integer seq;

	/** 文本内容 */
	private String content;

	// 子菜单
	/** The sub menu. */
	private Set<MenuEntity> subMenu = new TreeSet<MenuEntity>(new MenuEntityComparator());

	// 父菜单
	private MenuEntity parentMenu;

	private String menuKey; //TODO 菜单标识 到时可以通过模版控制

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMenuType() {
		return menuType;
	}

	public void setMenuType(String menuType) {
		this.menuType = menuType;
	}

	public String getMenuUrl() {
		return menuUrl;
	}

	public void setMenuUrl(String menuUrl) {
		this.menuUrl = menuUrl;
	}

	public String getInfoType() {
		return infoType;
	}

	public void setInfoType(String infoType) {
		this.infoType = infoType;
	}

	public Long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Set<MenuEntity> getSubMenu() {
		return subMenu;
	}

	public void setSubMenu(Set<MenuEntity> subMenu) {
		this.subMenu = subMenu;
	}

	public MenuEntity getParentMenu() {
		return parentMenu;
	}

	public void setParentMenu(MenuEntity parentMenu) {
		this.parentMenu = parentMenu;
	}

	/**
	 * @param subMenu
	 *            the subMenu to set
	 */
	public void addSubMenu(MenuEntity menu) {
		if (!subMenu.contains(menu)) {
			this.subMenu.add(menu);
		}
	}
	
	public String getMenuKey() {
		return menuKey;
	}

	public void setMenuKey(String menuKey) {
		this.menuKey = menuKey;
	}

}
