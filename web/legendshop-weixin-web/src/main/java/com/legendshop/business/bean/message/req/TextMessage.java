package com.legendshop.business.bean.message.req;

/**
 * 文本消息 【用户的请求】
 */
public class TextMessage extends BaseMessage {
    // 消息内容
    private String Content;

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }
}