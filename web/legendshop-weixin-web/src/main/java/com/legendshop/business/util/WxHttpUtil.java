package com.legendshop.business.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

/**
 * 微信网络链接
 * 
 * @author tony
 * 
 */
public class WxHttpUtil {
	
	private static Logger logger = LoggerFactory.getLogger(WxHttpUtil.class);
	
	

	/**
	 * 发送https请求
	 * 
	 * @param requestUrl
	 *            请求地址
	 * @param requestMethod
	 *            请求方式（GET、POST）
	 * @param outputStr
	 *            提交的数据
	 * @return 返回微信服务器响应的信息
	 */
	public static String httpsRequest(String requestUrl, String requestMethod,
			String outputStr) {
		logger.debug("*********HTTPREQUEST START********");
		logger.debug("*********requestUrl is "+
				requestUrl+" END AND requestMethod IS"
				+requestMethod + " END AND  outputStr" 
				+outputStr +" END ********");
		StringBuffer buffer = new StringBuffer();
		try {
			TrustManager[] tm = { new MyX509TrustManager() };
			SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
			sslContext.init(null, tm, new SecureRandom());

			SSLSocketFactory ssf = sslContext.getSocketFactory();

			URL url = new URL(requestUrl);
			HttpsURLConnection httpUrlConn = (HttpsURLConnection) url
					.openConnection();
			httpUrlConn.setSSLSocketFactory(ssf);

			httpUrlConn.setDoOutput(true);
			httpUrlConn.setDoInput(true);
			httpUrlConn.setUseCaches(false);
			httpUrlConn.setRequestMethod(requestMethod);
			httpUrlConn.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; Android 5.0; SM-N9100 Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36 MicroMessenger/6.0.2.56_r958800.520 NetType/WIFI");
			if ("GET".equalsIgnoreCase(requestMethod)) {
				httpUrlConn.connect();
			}

			if (null != outputStr) {
				OutputStream outputStream = httpUrlConn.getOutputStream();

				outputStream.write(outputStr.getBytes("UTF-8"));
				outputStream.close();
			}

			InputStream inputStream = httpUrlConn.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(
					inputStream, "utf-8");
			BufferedReader bufferedReader = new BufferedReader(
					inputStreamReader);

			String str = null;
			while ((str = bufferedReader.readLine()) != null) {
				buffer.append(str);
			}
			bufferedReader.close();
			inputStreamReader.close();

			inputStream.close();
			inputStream = null;
			httpUrlConn.disconnect();
			return buffer.toString();
		} catch (ConnectException ce) {
			System.out.println("Weixin server connection timed out.");
		} catch (Exception e) {
			System.out.println("https request error:{}" + e.getMessage());
		}
		return null;
	}
	
    public static String httpPost(String urlAddress,Map<String, String> paramMap){
        if(paramMap==null){
            paramMap = new HashMap<String, String>();
        }
        String [] params = new String[paramMap.size()];
        int i = 0;
        for(String paramKey:paramMap.keySet()){
            String param = null;
            try {
                param = paramKey+"="+ URLEncoder.encode(paramMap.get(paramKey).toString(), "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            params[i] = param;
            i++;
        }
        return httpPost(urlAddress, params);
    }
    
    public static String httpPost(String urlAddress,List<String> paramList){
        if(paramList==null){
            paramList = new ArrayList<String>();
        }
        return httpPost(urlAddress, paramList.toArray(new String[0]));
    }
    
    
    public static String httpPost(String urlAddress,String []params){
        URL url = null;
        HttpURLConnection con  =null;
        BufferedReader in = null;
        StringBuffer result = new StringBuffer();
        try {
            url = new URL(urlAddress);
            con  = (HttpURLConnection) url.openConnection();
            con.setUseCaches(false);
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded;charset=UTF-8");
            con.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; Android 5.0; SM-N9100 Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36 MicroMessenger/6.0.2.56_r958800.520 NetType/WIFI");
            String paramsTemp = "";
            for(String param:params){
                if(param!=null&&!"".equals(param.trim())){
                    paramsTemp+="&"+param;
                }
            }
            DataOutputStream out = new DataOutputStream(con.getOutputStream());
            out.writeBytes(paramsTemp);
            out.flush();
            out.close();
            in = new BufferedReader(new InputStreamReader(con.getInputStream(),"utf-8"));
            while (true) {
                String line = in.readLine();
                if (line == null) {
                    break;
                }
                else {
                    result.append(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                if(in!=null){
                    in.close();
                }
                if(con!=null){
                    con.disconnect();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result.toString();
    }
    
    public static Map<String, String> httpPostXML(String urlAddress,String xml) {
        URL url = null;
        HttpURLConnection con  =null;
        BufferedReader in = null;
        try {
        	byte[] bb=xml.getBytes(); 
            url = new URL(urlAddress);
            con  = (HttpURLConnection) url.openConnection();
            con.setUseCaches(false);
            con.setDoInput(true);  
            con.setDoOutput(true);//如果通过post提交数据，必须设置允许对外输出数据 
            
            con.setRequestProperty("Connection", "keep-alive");
            con.setRequestProperty("Accept", "*/*");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            con.setRequestProperty("Host", "api.mch.weixin.qq.com");
            con.setRequestProperty("X-Requested-With", "XMLHttpRequest");
            con.setRequestProperty("Cache-Control", "max-age=0");
            con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) ");
            
            con.connect();
           
            DataOutputStream out = new DataOutputStream(con  
                    .getOutputStream());  
            out.writeBytes(xml); //写入请求的字符串  
            out.flush();  
            out.close();  
            
            InputStream inputStream = con.getInputStream();
    		Map<String, String> map = null;
    		XmlPullParser pullParser = XmlPullParserFactory.newInstance().newPullParser();
    		pullParser.setInput(inputStream, "UTF-8"); // 为xml设置要解析的xml数据
    		int eventType = pullParser.getEventType();
    		while (eventType != XmlPullParser.END_DOCUMENT) {
    			switch (eventType) {
    			case XmlPullParser.START_DOCUMENT:
    				map = new HashMap<String, String>();
    				break;
    			case XmlPullParser.START_TAG:
    				String key = pullParser.getName();
    				if (key.equals("xml"))
    					break;
    				String value = pullParser.nextText();
    				map.put(key, value);
    				break;
    			case XmlPullParser.END_TAG:
    				break;
    			}
    			eventType = pullParser.next();
    		}
    		return map;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
			e.printStackTrace();
		}finally{
            try {
                if(in!=null){
                    in.close();
                }
                if(con!=null){
                    con.disconnect();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    
    

    
    public static String httpGet(String urlAddress){
        URL url = null;
        HttpURLConnection con  =null;
        BufferedReader in = null;
        StringBuffer result = new StringBuffer();
        try {
            url = new URL(urlAddress);
            con  = (HttpURLConnection) url.openConnection();
            con.setUseCaches(false);
            con.setDoOutput(true);
            con.setConnectTimeout(5000); //设置连接超时为5秒
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; Android 5.0; SM-N9100 Build/LRX21V) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile Safari/537.36 MicroMessenger/6.0.2.56_r958800.520 NetType/WIFI");
            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            while (true) {
                String line = in.readLine();
                if (line == null) {
                    break;
                }
                else {
                    result.append(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                if(in!=null){
                    in.close();
                }
                if(con!=null){
                    con.disconnect();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result.toString();
    }

    
    
    /**
     * URL编码(utf-8)
     *
     * @param source
     * @return String
     */
    public static String urlEncodeUTF8(String source) {
        String result = source;
        try {
            result = URLEncoder.encode(source, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * 根据类型判断文件扩展名
     *
     * @param contentType 内容类型
     * @return String
     */
    public static String getUploadFileExt(String contentType) {
        String fileExt = "";
        if (contentType == null || contentType == "") {
            return fileExt;
        }
        String[] content = contentType.split(";");
        if (content[0] != null) {
            if ("image/jpeg".equals(content[0])) {
                fileExt = ".jpg";
            } else if ("audio/mpeg".equals(content[0])) {
                fileExt = ".mp3";
            } else if ("audio/amr".equals(content[0])) {
                fileExt = ".amr";
            } else if ("video/mp4".equals(content[0])) {
                fileExt = ".mp4";
            } else if ("video/mpeg4".equals(content[0])) {
                fileExt = ".mp4";
            }
        }
        return fileExt;
    }

    /**
     * 根据类型判断文件扩展名
     *
     * @param contentType 内容类型
     * @return String
     */
    public static String getDownFileExt(String contentType) {
        String fileExt = "";
        if ("image/jpeg".equals(contentType)) {
            fileExt = ".jpg";
        } else if ("audio/mpeg".equals(contentType)) {
            fileExt = ".mp3";
        } else if ("audio/amr".equals(contentType)) {
            fileExt = ".amr";
        } else if ("video/mp4".equals(contentType)) {
            fileExt = ".mp4";
        } else if ("video/mpeg4".equals(contentType)) {
            fileExt = ".mp4";
        }
        return fileExt;
    }

    
    /**
     * 通过网络文件上传多媒体文件到微信公众平台
     * @param uploadMediaUrl http://file.api.weixin.qq.com/cgi-bin/media/upload?access_token=ACCESS_TOKEN&type=TYPE
     * @param mediaFileUrl 媒体文件url(如：http://c.hiphotos.baidu.com/zhidao/pic/item/14ce36d3d539b600286c7dd4eb50352ac65cb736.jpg)
     * @return
     */
    public static String uploadMediaByMediaFileUrl(String uploadMediaUrl,String mediaFileUrl){
    	
    	 String result=null;
    	
    	 BufferedReader bufferedReader = null;
    	 InputStream inputStream=null;
    	 InputStreamReader inputStreamReader=null;
    	 HttpURLConnection uploadConn=null;
    	//定义数据分隔符
        String boundary = "---------------------------" + System.currentTimeMillis();
        try {
            URL uploadUrl = new URL(uploadMediaUrl);
             uploadConn = (HttpURLConnection) uploadUrl.openConnection();
            uploadConn.setDoOutput(true);
            uploadConn.setDoInput(true);
            uploadConn.setUseCaches(false);
            uploadConn.setRequestMethod("POST");

            // 设置请求头信息
            uploadConn.setRequestProperty("Connection", "Keep-Alive");
            // 设置请求头Content-Type
            uploadConn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);


            // 获取媒体文件上传的输出流（往微信服务器写数据）
            OutputStream outputStream = uploadConn.getOutputStream();
            URL mediaUrl = new URL(mediaFileUrl);
            HttpURLConnection mediaConn = (HttpURLConnection) mediaUrl.openConnection();
            mediaConn.setDoOutput(true);
            mediaConn.setRequestMethod("GET");


            // 从请求头获取内容类型
            String contentType = mediaConn.getHeaderField("Content-Type");
            // 根据内容类型判断文件扩展名
            String fileExt = WxHttpUtil.getUploadFileExt(contentType);
            // 请求体开始
            outputStream.write(("--" + boundary + "\r\n").getBytes());
            outputStream.write(String.format("Content-Disposition: form-data; name=\"media\";filename=\"file1%s\"\r\n", fileExt).getBytes());
            outputStream.write(String.format("Content-Type: %s\r\n\r\n", contentType).getBytes());

            // 获取媒体文件的输入流(读取文件)
            BufferedInputStream bis = new BufferedInputStream(mediaConn.getInputStream());
            byte[] buf = new byte[8096];
            int size = 0;
            while ((size = bis.read(buf)) != -1) {
                // 将媒体文件写到输出流（往微信服务器写数据）
                outputStream.write(buf, 0, size);
            }

            // 请求体结束
            outputStream.write(("\r\n--" + boundary + "--\r\n").getBytes());
            outputStream.close();
            bis.close();
            mediaConn.disconnect();

            // 获取媒体文件上传的输入流（从微信服务器读数据）
             inputStream = uploadConn.getInputStream();
             inputStreamReader = new InputStreamReader(inputStream, "utf-8");
             bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer buffer = new StringBuffer();
            String str = null;
            while ((str = bufferedReader.readLine()) != null) {
                buffer.append(str);
            }
            result=buffer.toString();
        }catch (Exception e) {
        	e.printStackTrace();
        	 logger.error("上传媒体文件失败:{}", e);
		}finally{
			try {
				if (bufferedReader != null) {
					bufferedReader.close();
				}
				if (inputStreamReader != null) {
					inputStreamReader.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
				bufferedReader=null;
				inputStreamReader=null;
				inputStream = null;
				uploadConn.disconnect();
			} catch (IOException e) {
				e.printStackTrace();
				 logger.error("上传媒体文件失败:{}", e);
			}
		 }
		return result;
    }
    
    
	/**
	 * 通过本地文件上传多媒体文件到微信公众平台
	 *
	 * @param requestUrl
	 * 
	 * @param file
	 *            文件
	 * @param content_type
	 *            文件类型
	 * @return 返回的字符串
	 * @throws Exception
	 */
	public static String uploadMediaByMediaFile(String requestUrl, File file,String content_type) {

		String result=null;
		StringBuffer bufferStr = new StringBuffer();
		String end = "\r\n";
		String twoHyphens = "--"; // 用于拼接
		String boundary = "*****"; // 用于拼接 可自定义
		URL submit = null;
		DataOutputStream dos = null;
		BufferedInputStream bufin = null;
		BufferedReader bufferedReader = null;
		try {
			submit = new URL(requestUrl);
			HttpURLConnection conn = (HttpURLConnection) submit
					.openConnection();
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setUseCaches(false);

			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("Content-Type",
					"multipart/form-data;boundary=" + boundary);

			// 获取输出流对象，准备上传文件
			dos = new DataOutputStream(conn.getOutputStream());
			dos.writeBytes(twoHyphens + boundary + end);
			dos.writeBytes("Content-Disposition: form-data; name=\"" + file
					+ "\";filename=\"" + file.getName() + ";Content-Type=\""
					+ content_type + end);
			dos.writeBytes(end);
			// 对文件进行传输
			bufin = new BufferedInputStream(new FileInputStream(file));
			byte[] buffer = new byte[8192]; // 8k
			int count = 0;
			while ((count = bufin.read(buffer)) != -1) {
				dos.write(buffer, 0, count);
			}

			bufin.close(); // 关闭文件流

			dos.writeBytes(end);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + end);
			dos.flush();

			// 读取URL链接返回字符串
			InputStream inputStream = conn.getInputStream();
			InputStreamReader inputStreamReader = new InputStreamReader(
					inputStream, "utf-8");
			bufferedReader = new BufferedReader(inputStreamReader);

			String str = null;
			while ((str = bufferedReader.readLine()) != null) {
				bufferStr.append(str);
			}
			result=bufferStr.toString();
			System.out.println("-------------读取URL链接返回字符串--------------"+result);
		} catch (Exception e) {
			 logger.error("上传媒体文件失败:{}", e);
			e.printStackTrace();
			// throw new Exception("微信服务器连接错误！" + e.toString());
		} finally {
			try {
				if (dos != null) {
					dos.close();
				}
				if (bufferedReader != null) {
					bufferedReader.close();
				}

			} catch (Exception e2) {
				  logger.error("上传媒体文件失败:{}", e2);
			}
		}
		// 获取到返回Json请自行根据返回码获取相应的结果
		return result;
	}
	
	
	
	
	
	
    
    
    
    
    

}
