package com.legendshop.business.service.impl;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.bean.message.event.MenuEvent;
import com.legendshop.business.bean.message.resp.TextMessageResp;
import com.legendshop.business.constant.WxMenuInfoypeEnum;
import com.legendshop.business.dao.WeixinMenuExpandConfigDao;
import com.legendshop.business.entity.MenuEntity;
import com.legendshop.business.service.ExpandKeyService;
import com.legendshop.business.service.WeiXinMenuEventClickService;
import com.legendshop.business.service.WeiXinMenuService;
import com.legendshop.business.service.WeixinMessageService;
import com.legendshop.business.util.ExpandConfigsFactory;
import com.legendshop.business.util.MessageUtil;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.model.entity.weixin.WeixinMenuExpandConfig;
import com.legendshop.util.AppUtils;

@Service("weiXinMenuEventClickService")
public class WeiXinMenuEventClickServiceImpl implements WeiXinMenuEventClickService {
	
	@Autowired
	private WeiXinMenuService weiXinMenuService;
	
	@Autowired
	private WeixinMessageService weixinMessageService;
	
	@Autowired
	private WeixinMenuExpandConfigDao weixinMenuExpandConfigDao;

	@Override
	public String getMenuEventClick(HttpServletRequest request,MenuEvent menuEvent) {
		String key = menuEvent.getEventKey();
		//自定义菜单CLICK类型
		MenuEntity menuEntity = weiXinMenuService.findUniqueByProperty(key);
		TextMessageResp textMessageResp=new TextMessageResp();
		textMessageResp.setToUserName(menuEvent.getFromUserName());
		textMessageResp.setFromUserName(menuEvent.getToUserName());
		textMessageResp.setCreateTime(new Date().getTime());
		textMessageResp.setMsgType(WxConfigUtil.REQUEST_TEXT);
		textMessageResp.setContent(MessageUtil.getWelCome());
		
		if(AppUtils.isNotBlank(menuEntity)){
			String type = menuEntity.getInfoType();
			if(WxMenuInfoypeEnum.TEXT.value().equals(type)){
				textMessageResp.setContent(menuEntity.getContent());
				return MessageUtil.textMessageToXml(textMessageResp);
			}
			else if(WxMenuInfoypeEnum.EXPAND.value().equals(type)){
				if(AppUtils.isNotBlank(menuEntity.getTemplateId())){
					WeixinMenuExpandConfig config= weixinMenuExpandConfigDao.getWeixinMenuExpandConfig(menuEntity.getTemplateId());
					if(AppUtils.isNotBlank(config)){
						String className=config.getClassService();
						ExpandKeyService expandKeyService= ExpandConfigsFactory.getKeyService(className);
						if(expandKeyService!=null){
							String respMessage=expandKeyService.excute(request, "", textMessageResp);
							return respMessage;
						}
					}
				}
			}
			else {
				if(AppUtils.isNotBlank(menuEntity.getTemplateId())){
					return weixinMessageService.getMsg(type,menuEvent.getFromUserName(),  menuEvent.getToUserName(),  menuEntity.getTemplateId());
				}
			}
		}
		return MessageUtil.textMessageToXml(textMessageResp);
	}

}
