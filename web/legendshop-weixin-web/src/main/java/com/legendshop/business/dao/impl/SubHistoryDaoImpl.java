/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.SubHistoryDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.SubHistory;

/**
 * 保存订单历史.
 *
 */
@Repository
public class SubHistoryDaoImpl extends GenericDaoImpl<SubHistory, Long> implements SubHistoryDao {
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(SubHistoryDaoImpl.class);

	@Override
	public void saveSubHistory(SubHistory subHistory) {
		save(subHistory);
	}


}
