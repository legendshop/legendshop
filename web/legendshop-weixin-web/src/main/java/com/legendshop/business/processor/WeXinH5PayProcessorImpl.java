package com.legendshop.business.processor;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import com.legendshop.config.dto.WeixinPropertiesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.RandomStringUtils;
import com.legendshop.web.util.RequestHandler;

/**
 * 微信H5支付
 * https://pay.weixin.qq.com/wiki/doc/api/H5.php?chapter=15_4
 * 
 * @author Tony
 *
 */
@Component("weXinH5PayProcessor")
public class WeXinH5PayProcessorImpl  {
  
    @Autowired
    private PropertiesUtil propertiesUtil;

    @Autowired
	private WeixinPropertiesDto weixinPropertiesDto;

	public Map<String, Object> payto(String paymentConfig,SysPaymentForm payParams) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", false);

		
		JSONObject jsonObject=JSONObject.parseObject(paymentConfig);
		/** 微信公众号APPID */
		String appid = jsonObject.getString(WxConfigUtil.APPID);
		/** 微信公众号APPID */
		String token = jsonObject.getString(WxConfigUtil.TOKEN);
		/** 微信公众号绑定的商户号 */
		String mch_id = jsonObject.getString(WxConfigUtil.MCH_ID);
		String appSecret=jsonObject.getString(WxConfigUtil.APPSECRET);
		String partnerkey = jsonObject.getString(WxConfigUtil.PARTNERKEY);

		String subject = payParams.getSubject().replaceAll(" ", "").replaceAll("/", "");// 注意标题一定去空格
		String totalPrice = String.valueOf(new BigDecimal(payParams.getTotalAmount()).setScale(2, BigDecimal.ROUND_HALF_UP)
				.multiply(new BigDecimal(100)).intValue());

		SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();

		/** 公众号APPID */
		parameters.put("appid", appid);
		/** 商户号 */
		parameters.put("mch_id", mch_id);
		/** 随机字符串 */
		parameters.put("nonce_str", RandomStringUtils.randomNumeric(8));// 随机字符串
		/** 商品名称 */
		parameters.put("body", subject);
		/** 订单号 */
		parameters.put("out_trade_no", payParams.getSubSettlementSn());
		/** 订单金额以分为单位，只能为整数 */
		parameters.put("total_fee", totalPrice);
		/** 客户端本地ip */
		parameters.put("spbill_create_ip", payParams.getIp());
		
		String notifyUrl = propertiesUtil.getPcDomainName()  + "/payNotify/notify/"+ PayTypeEnum.WX_PAY.value();

		/** The return_url. 付完款后跳转的页面 要用 http://格式的完整路径，不允许加?id=123这类自定义参数 */
		String returnUrl = payParams.getShowUrl();

		parameters.put("notify_url", notifyUrl);

		/** 支付方式为APP支付 */
		parameters.put("trade_type", "MWEB");

		
		StringBuilder scene_info=new StringBuilder();
		scene_info.append("{\"h5_info\": {\"type\":\"Wap\",\"wap_url\": \""+ weixinPropertiesDto.getWeixinServer() +"\",\"wap_name\": \"订单支付\"}}");
		
		parameters.put("scene_info", scene_info.toString());
		
		RequestHandler reqHandler = new RequestHandler();
		reqHandler.init(token, appid, appSecret, partnerkey);

		// 计算签名
		String sign = reqHandler.createSign(parameters);
		parameters.put("sign", sign);

		Map<String, String> reqHandlerMap = reqHandler.getPrepayId(parameters);
		
		PaymentLog.info(" ########## WeXinH5ePay getPrepayId reqHandlerMap {} ##########",JSONUtil.getJson(reqHandlerMap));
		
		if (AppUtils.isBlank(reqHandlerMap)) {
			map.put("message", "微信统一下单集成失败！");
			return map;
		}

		String return_code = reqHandlerMap.get("return_code"); // 返回状态码
		String result_code= reqHandlerMap.get("result_code"); 
		if("FAIL".equals(return_code)){
			map.put("message", reqHandlerMap.get("return_msg"));
			return map;
		}
		
		if("FAIL".equals(result_code)){
			map.put("message", reqHandlerMap.get("err_code_des"));
			return map;
		}
		
		System.out.println(JSONUtil.getJson(reqHandlerMap));
		String mweb_url= reqHandlerMap.get("mweb_url");
		
		try {
			mweb_url=mweb_url+"&redirect_url="+URLEncoder.encode(returnUrl,"utf-8") ;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		StringBuffer sbHtml = new StringBuffer(300);
		sbHtml.append("<form id='payForm' action= '").append(mweb_url).append("' method='post'> ");
		sbHtml.append("</form>");
		sbHtml.append("<script>document.forms['payForm'].submit();</script>");
		
		map.put("message",sbHtml.toString());
		map.put("result", true);
		return map;
	}
	

}
