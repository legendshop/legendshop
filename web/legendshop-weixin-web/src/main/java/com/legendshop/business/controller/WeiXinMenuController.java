package com.legendshop.business.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.config.dto.WeixinPropertiesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.base.util.WeiXinErrcodeUtil;
import com.legendshop.business.api.message.MenuApi;
import com.legendshop.business.bean.menu.Button;
import com.legendshop.business.bean.menu.ClickButton;
import com.legendshop.business.bean.menu.ComplexButton;
import com.legendshop.business.bean.menu.ViewButton;
import com.legendshop.business.bean.menu.WxMenu;
import com.legendshop.business.entity.MenuEntity;
import com.legendshop.business.service.WeiXinMenuService;
import com.legendshop.business.service.WeiXinSubService;
import com.legendshop.business.service.WeixinTokenService;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.dto.weixin.AjaxJson;
import com.legendshop.model.entity.PayType;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.MD5Util;

/**
 * 微信菜单管理
 *
 * @author Tony
 */
@Controller
@RequestMapping("/admin/weixin/menu")
public class WeiXinMenuController {

    @Autowired
    private WeiXinMenuService weiXinMenuService;

    @Autowired
    private WeixinTokenService weixinTokenService;

    @Autowired
    private WeiXinSubService weiXinSubService;

    @Autowired
	private WeixinPropertiesDto weixinPropertiesDto;
    /**
     * 微信菜单同步接口
     *
     * @param request
     * @param response
     * @param token    token
     * @param secret   密钥摘要
     * @return
     */
    @RequestMapping(value = "/synchronous", method = RequestMethod.POST)
    public @ResponseBody
    AjaxJson synchronous(HttpServletRequest request, HttpServletResponse response, @RequestParam String token, @RequestParam String secret) {
        AjaxJson j = new AjaxJson();
        String _secret = MD5Util.toMD5(token + weixinPropertiesDto.getWeixinKey());
        if (!_secret.equals(secret)) { //如果双方密钥摘要不一致
            j.setSuccess(false);
            j.setMsg("非法访问!");
            return j;
        }

        PayType payType = weiXinSubService.getPayTypeById(PayTypeEnum.WX_PAY.value());
        PaymentLog.log("############# 支付参数  {} ########## ", JSONUtil.getJson(payType));

        if (payType == null) {
            j.setSuccess(false);
            j.setMsg("微信支付服务不存在,请设置微信支付服务!");
            return j;
        }

        JSONObject jsonObject = JSONObject.parseObject(payType.getPaymentConfig());
        /** 微信公众号APPID */
        String appid = jsonObject.getString(WxConfigUtil.APPID);
        String appSecret = jsonObject.getString(WxConfigUtil.APPSECRET);

        if (AppUtils.isBlank(appid) || AppUtils.isBlank(appSecret)) {
            j.setSuccess(false);
            j.setMsg("请设置APPID和APPSECRET!");
            return j;
        }
        List<MenuEntity> entities = weiXinMenuService.getMenuEntities();
        if (AppUtils.isBlank(entities)) {
            j.setSuccess(false);
            j.setMsg("同步菜单不能为空!");
            return j;
        }
        WxMenu menu = new WxMenu();
        Button firstArr[] = new Button[entities.size()];
        for (int i = 0; i < entities.size(); i++) {
            MenuEntity entity = entities.get(i);
            Set<MenuEntity> subMenu = entity.getSubMenu();
            /*
             * //判断二级菜单数量，大于0为含有二级菜单，进入里面添加二级菜单， // 如果没有，就直接添加一级菜单信息
             */
            if (AppUtils.isBlank(subMenu)) {
                if ("view".equals(entity.getMenuType())) {
                    ViewButton viewButton = new ViewButton();
                    viewButton.setName(entity.getName());
                    viewButton.setType(entity.getMenuType());
                    viewButton.setUrl(entity.getMenuUrl());
                    firstArr[i] = viewButton;
                } else if ("click".equals(entity.getMenuType())) {
                    ClickButton cb = new ClickButton();
                    cb.setKey(entity.getMenuKey());
                    cb.setName(entity.getName());
                    cb.setType(entity.getMenuType());
                    firstArr[i] = cb;
                }
            } else {
                ComplexButton complexButton = new ComplexButton();
                complexButton.setName(entity.getName());
                Button[] secondARR = new Button[subMenu.size()];
                List<MenuEntity> list = new ArrayList<MenuEntity>(subMenu);
                for (int k = 0; k < list.size(); k++) {
                    MenuEntity menuEntity = list.get(k);
                    String type = menuEntity.getMenuType();
                    if ("view".equals(type)) {
                        ViewButton viewButton = new ViewButton();
                        viewButton.setName(menuEntity.getName());
                        viewButton.setType(menuEntity.getMenuType());
                        viewButton.setUrl(menuEntity.getMenuUrl());
                        secondARR[k] = viewButton;
                    } else if ("click".equals(type)) {
                        ClickButton cb1 = new ClickButton();
                        cb1.setName(menuEntity.getName());
                        cb1.setType(menuEntity.getMenuType());
                        cb1.setKey(menuEntity.getMenuKey());
                        secondARR[k] = cb1;
                    }
                }
                complexButton.setSub_button(secondARR);
                firstArr[i] = complexButton;

            }

        }
        menu.setButton(firstArr);
        String resultStr = JSONUtil.getJson(menu);
        String accessToken = weixinTokenService.getAccessToken();
        jsonObject = MenuApi.createMenu(resultStr, accessToken);
        String message;
        if (jsonObject != null) {
            if (0 == jsonObject.getIntValue("errcode")) {
                message = "同步菜单信息数据成功！";
                j.setSuccess(true);
            } else {
                String errmsg = WeiXinErrcodeUtil.getErrorMsg(jsonObject.getIntValue("errcode"));
                message = "同步菜单信息数据失败！错误码为：" + jsonObject.getIntValue("errcode") + "错误信息为：" + errmsg;
                j.setSuccess(false);
            }
        } else {
            message = "同步菜单信息数据失败！同步自定义菜单URL地址不正确。";
            j.setSuccess(false);
        }
        j.setMsg(message);
        return j;
    }


}
