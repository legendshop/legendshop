package com.legendshop.business.config;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.legendshop.config.PropertiesUtil;
import com.legendshop.framework.event.EventHome;
import com.legendshop.framework.event.SysInitEvent;
import com.legendshop.util.AppUtils;

@WebListener
public class WeiXinInitSysListener implements ServletContextListener {

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(WeiXinInitSysListener.class);

	@Autowired
	private PropertiesUtil propertiesUtil;
	

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

	}

	@Override
	public void contextInitialized(ServletContextEvent event) {

		// 发送系统启动事件
		EventHome.publishEvent(new SysInitEvent(propertiesUtil.getWeiXinDomainName()));
	}

	/**
	 * 打印spring context
	 * 
	 * @param ctx
	 */
	private void printBeanFactory(ApplicationContext ctx) {
		if (log.isDebugEnabled()) {
			int i = 0;
			String[] beans = ctx.getBeanDefinitionNames();
			StringBuffer sb = new StringBuffer("系统配置的Spring Bean [ \n");
			if (!AppUtils.isBlank(beans)) {
				for (String bean : beans) {
					sb.append(++i).append(" ").append(bean).append("\n");
				}
				sb.append(" ]");
				log.debug(sb.toString());
			}
		}

	}
	
}
