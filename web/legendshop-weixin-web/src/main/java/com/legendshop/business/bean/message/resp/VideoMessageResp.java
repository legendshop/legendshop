package com.legendshop.business.bean.message.resp;

/**
 * 视频消息  [响应用户]
 */
public class VideoMessageResp extends BaseMessage {
    // 视频
    private Video Video;

    public Video getVideo() {
        return Video;
    }

    public void setVideo(Video video) {
        Video = video;
    }
}

