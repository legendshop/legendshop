package com.legendshop.business.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.config.dto.WeixinPropertiesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.base.util.WeiXinErrcodeUtil;
import com.legendshop.business.api.message.GroupApi;
import com.legendshop.business.api.message.UsersManageApi;
import com.legendshop.business.bean.user.WeiXinGroups;
import com.legendshop.business.entity.SyncWxUser;
import com.legendshop.business.service.JobStatusService;
import com.legendshop.business.service.WeiXinSubService;
import com.legendshop.business.service.WeixinGzuserInfoService;
import com.legendshop.business.service.WeixinTokenService;
import com.legendshop.business.util.WeiXinLog;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.model.constant.JobStatusEnum;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.dto.weixin.AjaxJson;
import com.legendshop.model.entity.JobStatus;
import com.legendshop.model.entity.PayType;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.MD5Util;

@Controller
@RequestMapping("/admin/weixin/usersManage")
public class WeiXinUsersManageController {

	@Autowired
	private WeixinTokenService weixinTokenService;
	
	@Autowired
	private WeixinGzuserInfoService weixinGzuserInfoService;
	
	@Autowired
    private JobStatusService jobStatusService;
	
	@Autowired
	private WeiXinSubService weiXinSubService;

	@Autowired
	private WeixinPropertiesDto weixinPropertiesDto;
	

	/**
	 * 同步微信用户
	 * @param request
	 * @param response
	 * @param nextOpenId 第一个拉取的OPENID，不填默认从头开始拉取
	 * @return
	 */
	@RequestMapping(value = "/synchronousUserinfo", method = RequestMethod.POST)
	public @ResponseBody AjaxJson synchronousUserinfo(HttpServletRequest request,HttpServletResponse response,
			@RequestParam String token,@RequestParam String secret,
			String nextOpenId) {
		AjaxJson j = new AjaxJson();
		String _secret=MD5Util.toMD5(token + weixinPropertiesDto.getWeixinKey());
		if(!_secret.equals(secret)){ //如果双方密钥摘要不一致
			j.setSuccess(false);
			WeiXinLog.info("非法访问! _secret ={}, secret = {}" + _secret, secret);
			j.setMsg("非法访问!");
			return j;
		}
		
		PayType payType = weiXinSubService.getPayTypeById(PayTypeEnum.WX_PAY.value());
		PaymentLog.log("############# 支付参数  {} ########## ",JSONUtil.getJson(payType));
		
		if (payType == null) {
			j.setSuccess(false);
			j.setMsg("微信支付服务不存在,请设置微信支付服务!");
			return j;
		} 
		
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		
		String appid = jsonObject.getString(WxConfigUtil.APPID);
		String appSecret=jsonObject.getString(WxConfigUtil.APPSECRET);
		
		if (AppUtils.isBlank(appid) || AppUtils.isBlank(appSecret)) {
			j.setSuccess(false);
			WeiXinLog.info("请设置APPID和APPSECRET!");
			j.setMsg("请设置APPID和APPSECRET!");
			return j;
		}
		//查询当前job的状态
		JobStatus jobStatus = jobStatusService.getJobStatusByJobName(JobStatusEnum.SYNCHRONOUS_USERINFO.name());
		if(AppUtils.isNotBlank(jobStatus)){
			if(jobStatus.getStatus().intValue()==1){
				// 判断有效时间 是否超过1小时
				java.util.Date end = new java.util.Date();
				java.util.Date start = new java.util.Date(jobStatus.getUpdateTime().getTime());
				if ((end.getTime() - start.getTime()) / 1000 / 3600 >= 1) {  //1个小时取一次，防止过期
					//重新启用JoB
					jobStatus.setStatus(1L);
					jobStatus.setUpdateTime(new Date());
					jobStatusService.updateJobStatus(jobStatus);
				}else{
					j.setSuccess(false);
					j.setMsg("同步操作还在运行过程中,请稍候操作!");
					WeiXinLog.info("同步操作还在运行过程中,请稍候操作!");
					return j;
				}
			}else{
				//重新启用JoB
				jobStatus.setStatus(1L);
				jobStatus.setUpdateTime(new Date());
				jobStatusService.updateJobStatus(jobStatus);
			}
		}else{
			//如果没有该JOB的记录，则插入，否则 更改JOB的状态为 running
			jobStatus = new JobStatus();
			jobStatus.setJobName(JobStatusEnum.SYNCHRONOUS_USERINFO.name());
			jobStatus.setStatus(1L);
			jobStatus.setUpdateTime(new Date());
			Long jobStatusId = jobStatusService.saveJobStatus(jobStatus);
			jobStatus.setId(jobStatusId);
		}
		final String accesstoken = weixinTokenService.getAccessToken();
		if (AppUtils.isBlank(accesstoken)) {
			j.setSuccess(false);
			j.setMsg("accesstoken is null !");
			return j;
		}
		SyncWxUser syncWxUser=new SyncWxUser();
		syncWxUser.setAccesstoken(accesstoken);
		syncWxUser.setJobStatusService(jobStatusService);
		syncWxUser.setNextOpenId(nextOpenId);
		syncWxUser.setWeixinGzuserInfoService(weixinGzuserInfoService);
		ExecutorService executorService=Executors.newSingleThreadExecutor();
		try {
			executorService.execute(syncWxUser);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			executorService.shutdown();
		}
		j.setSuccess(true);
		j.setMsg("同步成功");
		return j;
	}
	
	
	/**
	 * 下拉微信微信分组
	 * @param request
	 * @param response
	 * @param token
	 * @param secret 密钥摘要
	 * @return
	 */
	@RequestMapping(value = "/synchronousGroup", method = RequestMethod.POST)
	public @ResponseBody AjaxJson synchronousGroup(HttpServletRequest request,HttpServletResponse response,
			@RequestParam String token,@RequestParam String secret
			) {
		AjaxJson j = new AjaxJson();
		String _secret=MD5Util.toMD5(token + weixinPropertiesDto.getWeixinKey());
		if(!_secret.equals(secret)){ //如果双方密钥摘要不一致
			j.setSuccess(false);
			j.setMsg("非法访问!");
			return j;
		}
		
		PayType payType = weiXinSubService.getPayTypeById(PayTypeEnum.WX_PAY.value());
		PaymentLog.log("############# 支付参数  {} ########## ",JSONUtil.getJson(payType));
		if (payType == null) {
			j.setSuccess(false);
			j.setMsg("微信支付服务不存在,请设置微信支付服务!");
			return j;
		} 
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		/** 微信公众号APPID */
		String appid = jsonObject.getString(WxConfigUtil.APPID);
		String appSecret=jsonObject.getString(WxConfigUtil.APPSECRET);
		
		if (AppUtils.isBlank(appid) || AppUtils.isBlank(appSecret)) {
			j.setSuccess(false);
			j.setMsg("请设置APPID和APPSECRET!");
			return j;
		}
		String accesstoken = weixinTokenService.getAccessToken();
		if (AppUtils.isBlank(accesstoken)) {
			j.setSuccess(false);
			j.setMsg("accesstoken is null !");
			return j;
		}
		String result =GroupApi.getAllGroup(accesstoken);
		List<WeiXinGroups> groups=null;
		if (AppUtils.isNotBlank(result)) {
			jsonObject = JSONObject.parseObject(result);
			if(!jsonObject.containsKey("errcode")){
				JSONArray array = jsonObject.getJSONArray("groups");
				if (array != null) {
					groups = new ArrayList<WeiXinGroups>(array.size());
					for (int i = 0; i < array.size(); i++) {
						WeiXinGroups group = array.getObject(i, WeiXinGroups.class);
						/*if (!"未分组".equals(group.getName())) {*/
							groups.add(group);
						/*}*/
					}
				}
			}else{
				String errmsg="操作失败 ，errcode="+jsonObject.getInteger("errcode") +",errmsg ="+WeiXinErrcodeUtil.getErrorMsg(jsonObject.getInteger("errcode"));
				j.setSuccess(false);
				j.setMsg(errmsg);
				return j;
			}
		}
		j.setSuccess(true);
		j.setMsg("同步成功");
		j.setSuccessValue(JSONUtil.getJson(groups));
		return j;
	}

	
	/**
	 * 创建分组
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/createGroups", method = RequestMethod.POST)
	public @ResponseBody AjaxJson createGroups(HttpServletRequest request,HttpServletResponse response ,
			@RequestParam String token,@RequestParam String secret,
			@RequestParam String groupName) {

		AjaxJson j = new AjaxJson();

		String _secret=MD5Util.toMD5(token + weixinPropertiesDto.getWeixinKey());
		if(!_secret.equals(secret)){ //如果双方密钥摘要不一致
			j.setSuccess(false);
			j.setMsg("非法访问!");
			return j;
		}

		PayType payType = weiXinSubService.getPayTypeById(PayTypeEnum.WX_PAY.value());
		PaymentLog.log("############# 支付参数  {} ########## ",JSONUtil.getJson(payType));
		if (payType == null) {
			j.setSuccess(false);
			j.setMsg("微信支付服务不存在,请设置微信支付服务!");
			return j;
		}
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		/** 微信公众号APPID */
		String appid = jsonObject.getString(WxConfigUtil.APPID);
		String appSecret=jsonObject.getString(WxConfigUtil.APPSECRET);


		if (AppUtils.isBlank(appid) || AppUtils.isBlank(appSecret)) {
			j.setSuccess(false);
			j.setMsg("请设置APPID和APPSECRET!");
			return j;
		}

		String accesstoken = weixinTokenService.getAccessToken();
		if (AppUtils.isBlank(accesstoken)) {
			j.setSuccess(false);
			j.setMsg("accesstoken is null !");
			return j;
		}

		WeiXinGroups groups=GroupApi.createGroups(accesstoken, groupName);
		if(AppUtils.isNotBlank(groups.getErrcode()) ){
			String errmsg="操作失败 ，errcode="+groups.getErrcode() +",errmsg ="+WeiXinErrcodeUtil.getErrorMsg(groups.getErrcode());
			j.setSuccess(false);
			j.setMsg(errmsg);
		    return j;
		}

		j.setSuccess(true);
		j.setMsg("同步成功");
		j.setSuccessValue(JSONUtil.getJson(groups));
		return j;
	}
	
	
	/**
	 * 修改分组名称
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateGroup", method = RequestMethod.POST)
	public @ResponseBody
	AjaxJson updateGroup(HttpServletRequest request,HttpServletResponse response ,
			@RequestParam String groupId,
			@RequestParam String groupName,
			@RequestParam String token,@RequestParam String secret
			) {
		AjaxJson j = new AjaxJson();
		
		String _secret=MD5Util.toMD5(token + weixinPropertiesDto.getWeixinKey());
		if(!_secret.equals(secret)){ //如果双方密钥摘要不一致
			j.setSuccess(false);
			j.setMsg("非法访问!");
			return j;
		}
		PayType payType = weiXinSubService.getPayTypeById(PayTypeEnum.WX_PAY.value());
		PaymentLog.log("############# 支付参数  {} ########## ",JSONUtil.getJson(payType));
		if (payType == null) {
			j.setSuccess(false);
			j.setMsg("微信支付服务不存在,请设置微信支付服务!");
			return j;
		} 
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		/** 微信公众号APPID */
		String appid = jsonObject.getString(WxConfigUtil.APPID);
		String appSecret=jsonObject.getString(WxConfigUtil.APPSECRET);
		
		
		if (AppUtils.isBlank(appid) || AppUtils.isBlank(appSecret)) {
			j.setSuccess(false);
			j.setMsg("请设置APPID和APPSECRET!");
			return j;
		}
		String accesstoken = weixinTokenService.getAccessToken();
		if (AppUtils.isBlank(accesstoken)) {
			j.setSuccess(false);
			j.setMsg("请设置APPID和APPSECRET!");
			return j;
		}
		
		j=GroupApi.updateGroup(accesstoken,groupId, groupName);
		return j;
	}
	
	
	
	
	/**
	 * 移动用户分组
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/updateUserGroup", method = RequestMethod.POST)
	public @ResponseBody
	AjaxJson updateUserGroup(HttpServletRequest request,HttpServletResponse response ,
			@RequestParam String openId,
			@RequestParam int groupId,
			@RequestParam String token,@RequestParam String secret
			) {
		AjaxJson j = new AjaxJson();
		
		String _secret=MD5Util.toMD5(token + weixinPropertiesDto.getWeixinKey());
		if(!_secret.equals(secret)){ //如果双方密钥摘要不一致
			j.setSuccess(false);
			j.setMsg("非法访问!");
			return j;
		}
		
		PayType payType = weiXinSubService.getPayTypeById(PayTypeEnum.WX_PAY.value());
		PaymentLog.log("############# 支付参数  {} ########## ",JSONUtil.getJson(payType));
		if (payType == null) {
			j.setSuccess(false);
			j.setMsg("微信支付服务不存在,请设置微信支付服务!");
			return j;
		} 
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		/** 微信公众号APPID */
		String appid = jsonObject.getString(WxConfigUtil.APPID);
		String appSecret=jsonObject.getString(WxConfigUtil.APPSECRET);
		
		
		if (AppUtils.isBlank(appid) || AppUtils.isBlank(appSecret)) {
			j.setSuccess(false);
			j.setMsg("请设置APPID和APPSECRET!");
			return j;
		}
		String accesstoken = weixinTokenService.getAccessToken();
		if (AppUtils.isBlank(accesstoken)) {
			j.setSuccess(false);
			j.setMsg("请设置APPID和APPSECRET!");
			return j;
		}
		String result=GroupApi.updateUserGroup(accesstoken, openId, groupId);
		if(AppUtils.isNotBlank(result)){
			jsonObject=JSONObject.parseObject(result);
			if(jsonObject.containsKey("errcode")){
				if(jsonObject.getInteger("errcode") ==0){
					j.setSuccess(true);
					j.setMsg("更新用户备注成功");
				}else{
					String errmsg="操作失败 ，errcode="+jsonObject.getInteger("errcode") +",errmsg ="+WeiXinErrcodeUtil.getErrorMsg(jsonObject.getInteger("errcode"));
					j.setSuccess(false);
					j.setMsg(errmsg);
				}
			}
		}
		return j;
	}
	
	
	/**
	 * 删除
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/deleteGroup", method = RequestMethod.POST)
	public @ResponseBody
	AjaxJson deleteGroup(HttpServletRequest request,HttpServletResponse response ,@RequestParam int groupId,
			@RequestParam String token,@RequestParam String secret
			) {
		AjaxJson j = new AjaxJson();
		
		String _secret=MD5Util.toMD5(token + weixinPropertiesDto.getWeixinKey());
		if(!_secret.equals(secret)){ //如果双方密钥摘要不一致
			j.setSuccess(false);
			j.setMsg("非法访问!");
			return j;
		}
		
		PayType payType = weiXinSubService.getPayTypeById(PayTypeEnum.WX_PAY.value());
		PaymentLog.log("############# 支付参数  {} ########## ",JSONUtil.getJson(payType));
		if (payType == null) {
			j.setSuccess(false);
			j.setMsg("微信支付服务不存在,请设置微信支付服务!");
			return j;
		} 
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		/** 微信公众号APPID */
		String appid = jsonObject.getString(WxConfigUtil.APPID);
		String appSecret=jsonObject.getString(WxConfigUtil.APPSECRET);
		
		if (AppUtils.isBlank(appid) || AppUtils.isBlank(appSecret)) {
			j.setSuccess(false);
			j.setMsg("请设置APPID和APPSECRET!");
			return j;
		}
		String accesstoken = weixinTokenService.getAccessToken();
		if (AppUtils.isBlank(accesstoken)) {
			j.setSuccess(false);
			j.setMsg("请设置APPID和APPSECRET!");
			return j;
		}
		j=GroupApi.deleteGroup(accesstoken, groupId);
		return j;
	}
	
	
	/**
	 * 更新用户备注
	 * @param request
	 * @param response
	 * @param openId
	 * @param remark
	 * @return
	 */
	@RequestMapping(value = "/updateRemark", method = RequestMethod.POST)
	public @ResponseBody
	AjaxJson updateRemark(HttpServletRequest request,HttpServletResponse response ,
			@RequestParam String token,@RequestParam String secret,
			@RequestParam String openId,
			@RequestParam String remark) {
		AjaxJson j = new AjaxJson();
		
		String _secret=MD5Util.toMD5(token + weixinPropertiesDto.getWeixinKey());
		if(!_secret.equals(secret)){ //如果双方密钥摘要不一致
			j.setSuccess(false);
			j.setMsg("非法访问!");
			return j;
		}
		PayType payType = weiXinSubService.getPayTypeById(PayTypeEnum.WX_PAY.value());
		PaymentLog.log("############# 支付参数  {} ########## ",JSONUtil.getJson(payType));
		if (payType == null) {
			j.setSuccess(false);
			j.setMsg("微信支付服务不存在,请设置微信支付服务!");
			return j;
		} 
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		/** 微信公众号APPID */
		String appid = jsonObject.getString(WxConfigUtil.APPID);
		String appSecret=jsonObject.getString(WxConfigUtil.APPSECRET);
		
		
		if (AppUtils.isBlank(appid) || AppUtils.isBlank(appSecret)) {
			j.setSuccess(false);
			j.setMsg("请设置APPID和APPSECRET!");
			return j;
		}
		String accesstoken = weixinTokenService.getAccessToken();
		if (AppUtils.isBlank(accesstoken)) {
			j.setSuccess(false);
			j.setMsg("请设置APPID和APPSECRET!");
			return j;
		}
		String result=UsersManageApi.updateRemark(accesstoken, openId, remark);
		if(AppUtils.isNotBlank(result)){
			jsonObject=JSONObject.parseObject(result);
			if(jsonObject.containsKey("errcode")){
				if(jsonObject.getInteger("errcode") ==0){
					j.setSuccess(true);
					j.setMsg("更新用户备注成功");
				}else{
					String errmsg="操作失败 ，errcode="+jsonObject.getInteger("errcode") +",errmsg ="+WeiXinErrcodeUtil.getErrorMsg(jsonObject.getInteger("errcode"));
					j.setSuccess(false);
					j.setMsg(errmsg);
				}
			}
		}
		return j;
	}
	
	
}
