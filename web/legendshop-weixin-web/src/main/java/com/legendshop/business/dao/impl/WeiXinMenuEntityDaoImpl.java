package com.legendshop.business.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeiXinMenuEntityDao;
import com.legendshop.business.entity.MenuEntity;
import com.legendshop.dao.impl.GenericJdbcDaoImpl;

/**
 * 
 * @author tony
 *
 */
@Repository
public class WeiXinMenuEntityDaoImpl extends GenericJdbcDaoImpl implements
		WeiXinMenuEntityDao {

	public List<MenuEntity> getWeixinMenu() {
		return this.query("SELECT * FROM ls_weixin_menu ORDER BY GRADE ASC ", null,new MenuEntityRowMapper());
	}

	class MenuEntityRowMapper implements RowMapper<MenuEntity> {

		public MenuEntity mapRow(ResultSet rs, int arg1) throws SQLException {

			MenuEntity entity = new MenuEntity();
			
			entity.setId(rs.getLong("id"));
			entity.setName(rs.getString("name"));
			entity.setMenuType(rs.getString("menuType"));
			entity.setMenuUrl(rs.getString("menuUrl"));
			entity.setInfoType(rs.getString("infoType"));
			entity.setTemplateId(rs.getLong("templateid")==0?null:rs.getLong("templateid"));
			entity.setLevel(rs.getInt("grade"));
			entity.setParentId(rs.getLong("parent_id")==0?null:rs.getLong("parent_id"));
			entity.setSeq(rs.getInt("seq"));
			entity.setContent(rs.getString("content"));
			entity.setMenuKey(rs.getString("menu_key"));

			return entity;
		}

	}

	@Override
	public MenuEntity findUniqueByProperty(String menu_key) {
		return this.get("select distinct m.name as name,m.menuType as menuType,m.menuUrl as menuUrl,m.content AS content," +
				"m.infoType as infoType,m.templateId as templateId,m.menu_key as menuKey from ls_weixin_menu m where m.menu_key=? ", MenuEntity.class, menu_key);
	}

}
