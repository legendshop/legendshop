/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service;


/**
 * The Class WeixinSubscribeService.
 */
public interface WeixinCancelSubscribeService  {


	public String cancelsubscribeResponse(String fromUserName);
}
