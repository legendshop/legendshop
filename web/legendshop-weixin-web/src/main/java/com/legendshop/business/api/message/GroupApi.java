package com.legendshop.business.api.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.business.bean.WeiXinMsg;
import com.legendshop.business.bean.user.WeiXinGroups;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.model.dto.weixin.AjaxJson;
import com.legendshop.util.AppUtils;
import com.legendshop.util.HttpUtil;
import com.legendshop.util.JSONUtil;

public class GroupApi {

	private static Logger log = LoggerFactory.getLogger(GroupApi.class);

	/**
	 * 创建分组信息
	 * 
	 * @param accesstoken
	 * @param groupName
	 * @return
	 * @throws WexinReqException
	 */
	public static WeiXinGroups createGroups(String accesstoken, String groupName) {

		String requestUrl = WxConfigUtil.GROUP_CREATE_URL.replace("ACCESS_TOKEN",accesstoken);

		JSONObject nameJson = new JSONObject();
		JSONObject groupJson = new JSONObject();
		nameJson.put("name", groupName);
		groupJson.put("group", nameJson);
		// 发起GET请求删除菜单
		String text = HttpUtil.httpsRequest(requestUrl, "POST",
				groupJson.toJSONString());
		if (text != null) {
			JSONObject jsonObject=JSONObject.parseObject(text);
			if(!jsonObject.containsKey("errcode")){
				WeiXinGroups groups=jsonObject.getObject("group", WeiXinGroups.class);
				return groups;
			}
		}
		return null;
	}

	/**
	 * 获取所有的分组信息
	 * 
	 * @param accesstoken
	 * @return
	 */
	public static String getAllGroup(String accesstoken) {
		String requestUrl = WxConfigUtil.GROUP_GET_URL.replace("ACCESS_TOKEN",
				accesstoken);
		String result = HttpUtil.httpsRequest(requestUrl, "GET", null);
		return result;
	}

	/**
	 * 获取用户分组id
	 * 
	 * @param accesstoken
	 * @return
	 */
	public static String getUserGroupId(String accesstoken, String openid) {
		String requestUrl = WxConfigUtil.GROUP_GETID_URL.replace(
				"ACCESS_TOKEN", accesstoken);

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("openid", openid);

		String result = HttpUtil.httpsRequest(requestUrl, "POST",
				jsonObject.toString());
		if (AppUtils.isNotBlank(result)) {
			JSONObject resultJsonObject = JSONObject.parseObject(result);
			if (resultJsonObject != null) {
				return resultJsonObject.getString("groupid");
			}
		}
		return null;
	}

	/**
	 * 修改分组名
	 * 
	 * @param groupId
	 *            分组id
	 * @param name
	 *            分组名称
	 * @throws WeChatException
	 */
	public static AjaxJson updateGroup(String accesstoken, String groupId,
			String groupNewName) {
		JSONObject nameJson = new JSONObject();
		JSONObject groupJson = new JSONObject();
		nameJson.put("id", groupId);
		nameJson.put("name", groupNewName);
		groupJson.put("group", nameJson);

		String requestUrl = WxConfigUtil.GROUP_UPDATE_URL.replace(
				"ACCESS_TOKEN", accesstoken);
		String result = HttpUtil.httpsRequest(requestUrl, "POST",
				groupJson.toJSONString());
		if (AppUtils.isNotBlank(result)) {
			AjaxJson json=new AjaxJson();
			WeiXinMsg msg=JSONUtil.getObject(result, WeiXinMsg.class);
				if(AppUtils.isNotBlank(msg.getErrcode())){
					if(msg.getErrcode()==0){
						json.setSuccess(true);
						json.setMsg("修改分组成功");
						return json;
					}else{
						json.setSuccess(false);
						json.setMsg(msg.getErrmsg());
						return json;
					}
				}
		}
		return null;
	}
	
	
	/**
	 * 移动用户分组
	 * @param openid 用户的OpenID
	 * @param groupId 分组id
	 */
	public static String updateUserGroup(String accesstoken, String openId,int groupId){
		JSONObject paraObject = new JSONObject();
		paraObject.put("openid", openId);
		paraObject.put("to_groupid ", groupId);
		
		String requestUrl = WxConfigUtil.GROUP_MOVE_URL.replace("ACCESS_TOKEN", accesstoken);
		String result = HttpUtil.httpsRequest(requestUrl, "POST",paraObject.toJSONString());
		return result;
	}
	
	/**
	 *  批量移动用户分组
	 * @param openids 用户唯一标识符openid的列表（size不能超过50）
	 * @param toGroupid 分组id
	 */
	public static String datchUpdateGroup(String accesstoken, String [] openIds,int groupId){
		JSONObject paraObject = new JSONObject();
		paraObject.put("openid_list", openIds);
		paraObject.put("to_groupid", groupId);
		String requestUrl = WxConfigUtil.GROUP_BATCH_UPDATE_URL.replace("ACCESS_TOKEN", accesstoken);
		String result = HttpUtil.httpsRequest(requestUrl, "POST",paraObject.toJSONString());
		if (AppUtils.isNotBlank(result)) {
			JSONObject jsonObject = JSONObject.parseObject(result);
			if (jsonObject != null) {
				return jsonObject.getString("errmsg");
			}
		}
		return result;
	}
	
	/**
	 * 删除分组
	 * @param groupId
	 */
	public static AjaxJson deleteGroup(String accesstoken,int groupId){
		JSONObject paraObject = new JSONObject();
		paraObject.put("id", groupId);
		JSONObject groupJson = new JSONObject();
		groupJson.put("group", paraObject);
		String requestUrl = WxConfigUtil.GROUP_DELETE_URL.replace("ACCESS_TOKEN", accesstoken);
		String result = HttpUtil.httpsRequest(requestUrl, "POST",groupJson.toJSONString());
		if (AppUtils.isNotBlank(result)) {
			AjaxJson json=new AjaxJson();
			WeiXinMsg msg=JSONUtil.getObject(result, WeiXinMsg.class);
				if(AppUtils.isNotBlank(msg.getErrcode())){
					if(msg.getErrcode()==0){
						json.setSuccess(true);
						json.setMsg("删除分组成功");
						return json;
					}else{
						json.setSuccess(false);
						json.setMsg(msg.getErrmsg());
						return json;
					}
				}else{
					json.setSuccess(true);
					return json;
				}
		}
		return null;
	}

}
