/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinTexttemplateDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.weixin.WeixinTexttemplate;

/**
 * The Class WeixinTexttemplateDaoImpl.
 */

@Repository
public class WeixinTexttemplateDaoImpl extends GenericDaoImpl<WeixinTexttemplate, Long> implements WeixinTexttemplateDao  {
     
    public List<WeixinTexttemplate> getWeixinTexttemplate(String userName){
   		return this.queryByProperties(new EntityCriterion().eq("userName", userName));
    }

    @Cacheable(value="WeixinTexttemplate",key="#id")
	public WeixinTexttemplate getWeixinTexttemplate(Long id){
		return getById(id);
	}
	
    public int deleteWeixinTexttemplate(WeixinTexttemplate weixinTexttemplate){
    	return delete(weixinTexttemplate);
    }
	
	public Long saveWeixinTexttemplate(WeixinTexttemplate weixinTexttemplate){
		return save(weixinTexttemplate);
	}
	
	public int updateWeixinTexttemplate(WeixinTexttemplate weixinTexttemplate){
		return update(weixinTexttemplate);
	}
	
 }
