package com.legendshop.business.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.legendshop.base.log.PaymentLog;
import com.legendshop.business.processor.WeXinH5PayProcessorImpl;
import com.legendshop.business.processor.WeXinSacnCodePayProcessorImpl;
import com.legendshop.business.processor.WeXinWapPayProcessorImpl;
import com.legendshop.business.service.WeiXinSubService;
import com.legendshop.business.util.WeiXinLog;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.constant.PaymentProcessorEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.MD5Util;


/**
 * 微信支付中心
 * @author Tony哥
 *
 */
@Controller
@RequestMapping("/wxpay/weixinJSBridge")
public class WeiXinServerPayController {

	@Autowired
	private WeiXinSubService weiXinSubService;

	@Autowired
	private PropertiesUtil propertiesUtil;

	@Autowired
	private  WeXinH5PayProcessorImpl weXinH5PayProcessor;

	@Autowired
	private  WeXinSacnCodePayProcessorImpl weXinSacnCodePayProcessor;

	@Autowired
	private  WeXinWapPayProcessorImpl weXinWapPayProcessor;


	/**
	 * 移动端手动返回请求处理
	 * */
	@GetMapping(value = "/{payWayCode}")
	public String payWayCode(HttpServletRequest request){
		String vueDomainName = propertiesUtil.getVueDomainName();
		SortedMap<Object, Object> parameters = new TreeMap<>();
		parameters.put("appId","");
		request.setAttribute("show_url",vueDomainName + "/orderModules/orderList/orderList");
		request.setAttribute("payment_result", JSONUtil.getJson(parameters));
		return "/wxpay/WeixinJSBridge";
	}

	/**
	 *
	 * @param request
	 * @param response
	 * @param payWayCode
	 * @param secret
	 * @param token
	 * @param validateTime
	 * @param outTradeNo
	 * @param userId
	 * @param openId
	 * @param subjects
	 * @param showUrl
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = "/{payWayCode}",method=RequestMethod.POST)
	public String payWayCode(HttpServletRequest request, HttpServletResponse response,
							 @PathVariable("payWayCode") String payWayCode,
							 @RequestParam(required=true) String secret,
							 @RequestParam(required=true) String token,
							 @RequestParam(required=true) Long validateTime,
							 @RequestParam(required=true) String outTradeNo,
							 @RequestParam(required=true) String userId,
							 @RequestParam(required=true) String subjects,
							 @RequestParam(required=true) String showUrl,
							 String openId
	)throws UnsupportedEncodingException {

		WeiXinLog.info("##################### 微信支付中心请求 {} #######################",payWayCode);

		String ip = "119.131.117.242";
		PaymentLog.info(" ip={} ",ip);

		// 判断是否微信环境, 5.0 之后的支持微信支付
		Long nowTime = System.currentTimeMillis();
		if ((nowTime - validateTime) > 600000) {
			request.setAttribute("message", "链接已失效!");
			WeiXinLog.error("链接已失效! token = {}，userId = {} ", token, userId);
			return "/systemerror";
		}

		try {
			subjects = URLDecoder.decode(subjects, "utf-8");
		} catch (UnsupportedEncodingException e) {
			WeiXinLog.error("decode 失败， subjects = {} ", subjects);
		}

		SortedMap<String,Object> paramsMap=new TreeMap<String,Object>();
		paramsMap.put("token", token);
		paramsMap.put("outTradeNo", outTradeNo);
		paramsMap.put("userId", userId);
		if(AppUtils.isNotBlank(openId)){
			paramsMap.put("openId", openId);
		}
		paramsMap.put("validateTime", validateTime);
		paramsMap.put("subjects",subjects);
		paramsMap.put("key",propertiesUtil.getWeiXinKey());
		paramsMap.put("showUrl",showUrl);

		System.out.println("-----微信支付请求加密参数:"+JSONUtil.getJson(paramsMap));

		String _secret =MD5Util.createSign(paramsMap);

		WeiXinLog.info("##################### 微信支付中心请求参数 {} #######################",JSONUtil.getJson(paramsMap));
		if(!_secret.equals(secret)){ //如果双方密钥摘要不一致
			WeiXinLog.error("secret签名失败， 非法访问! " );
			WeiXinLog.error("_secret="+_secret);
			WeiXinLog.error("secret="+secret);
			request.setAttribute("message", "secret签名失败,非法访问!");
			return "/systemerror" ;
		}

		SubSettlement subSettlement=weiXinSubService.getSubSettlement(outTradeNo, userId);
		if(AppUtils.isBlank(subSettlement)){
			WeiXinLog.info("找不到支付单据 by outTradeNo={},userId={}",outTradeNo, userId);
			request.setAttribute("message", "找不到支付单据!");
			return "/systemerror" ;
		}
		if(subSettlement.getIsClearing()){
			WeiXinLog.info("请勿重复支付! by outTradeNo={},userId={}",outTradeNo, userId);
			request.setAttribute("message", "请勿重复支付!");
			return "/systemerror" ;
		}

		if (!PayTypeEnum.WX_PAY.value().equals(subSettlement.getPayTypeId())) {
			WeiXinLog.info("该单据不是微信支付方式,不能操作 ! by outTradeNo={},userId={}",outTradeNo, userId);
			request.setAttribute("message", "该单据不是微信支付方式,不能操作 !");
			return "/systemerror" ;
		}

		PayType payType = weiXinSubService.getPayTypeById(PayTypeEnum.WX_PAY.value()); //这种方式是卖家中心调用支付服务
		if (payType == null) {
			request.setAttribute("message", "微信支付方式不存在,请设置支付方式!");
			WeiXinLog.error("微信支付方式不存在,请设置支付方式 payTypeId = {} " ,PayTypeEnum.WX_PAY.value());
			return "/systemerror" ;
		} else if (payType.getIsEnable() == Constants.OFFLINE) {
			request.setAttribute("message", "支付方式不存在,请设置支付方式!");
			WeiXinLog.error("微信支付方式没有启用 payTypeId = {} " ,PayTypeEnum.WX_PAY.value());
			return "/systemerror" ;
		}

		SysPaymentForm paymentForm=new SysPaymentForm();
		paymentForm.setUserId(userId);
		paymentForm.setOpenId(openId);
		paymentForm.setSubSettlementSn(outTradeNo);;
		paymentForm.setTotalAmount(subSettlement.getCashAmount());
		paymentForm.setOrderDatetime(subSettlement.getCreateTime());
		paymentForm.setShowUrl(showUrl);
		paymentForm.setSource(subSettlement.getSettlementFrom());
		paymentForm.setSubject(subjects);
		paymentForm.setIp(ip);

		/**
		 * 获取支付数据信息
		 */
		Map<String, Object> payment_result = null;
		if(PaymentProcessorEnum.WX_H5_PAY.value().equals(payWayCode)){
			payment_result=weXinH5PayProcessor.payto(payType.getPaymentConfig(), paymentForm);
		}else if(PaymentProcessorEnum.WX_SACN_PAY.value().equals(payWayCode)){
			payment_result=weXinSacnCodePayProcessor.payto(payType.getPaymentConfig(), paymentForm);
		}else if(PaymentProcessorEnum.WX_PAY.value().equals(payWayCode)){
			payment_result=weXinWapPayProcessor.payto(payType.getPaymentConfig(), paymentForm);
		}else{
			System.out.println("------ 参数有误 -----");
			return "/systemerror" ;
		}

		if (!Boolean.parseBoolean(payment_result.get("result").toString())) {
			WeiXinLog.error(payment_result.get("message").toString());
			request.setAttribute("message", payment_result.get("message").toString());
			return "/systemerror" ;
		}

		WeiXinLog.info("##################### 微信支付请求参数 payment_result= {} #######################",JSONUtil.getJson(payment_result));

		request.setAttribute("payment_result", payment_result.get("message"));
		request.setAttribute("token", token);
		request.setAttribute("secret", secret);
		request.setAttribute("validateTime", validateTime);
		request.setAttribute("totalFee", paymentForm.getTotalAmount());
		request.setAttribute("outTradeNo", subSettlement.getSubSettlementSn());
		request.setAttribute("userId",userId);
		request.setAttribute("openId",openId);
		request.setAttribute("subject",subjects);
		request.setAttribute("show_url",showUrl);

		if(PaymentProcessorEnum.WX_SACN_PAY.value().equals(payWayCode)){
			request.setAttribute("code_url", payment_result.get("code_url"));
			return "/wxpay/WeiXinSacnCode";
		}

		return "/wxpay/WeixinJSBridge";
	}


	/**
	 * 查询订单状态
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/GetOrderStatus")
	public @ResponseBody String GetOrderStatus(HttpServletRequest request, HttpServletResponse response,
											   @RequestParam(required=true) String token,
											   @RequestParam(required=true) String secret,
											   @RequestParam(required=true) Long validateTime,
											   @RequestParam(required=true) String outTradeNo,
											   @RequestParam(required=true) String userId,
											   @RequestParam(required=true) String openId,
											   @RequestParam(required=true) String subject,
											   @RequestParam(required=true) String showUrl
	) throws IOException{

		// 判断是否微信环境, 5.0 之后的支持微信支付
		Long nowTime = System.currentTimeMillis();
		if ((nowTime - validateTime) > 600000) {
			return "链接已失效!";
		}

		SortedMap<String,Object> paramsMap=new TreeMap<String,Object>();
		paramsMap.put("token", token);
		paramsMap.put("outTradeNo", outTradeNo);
		paramsMap.put("userId", userId);
		if(AppUtils.isNotBlank(openId)){
			paramsMap.put("openId", openId);
		}
		paramsMap.put("validateTime", validateTime);
		paramsMap.put("subjects",subject);
		paramsMap.put("key",propertiesUtil.getWeiXinKey());
		paramsMap.put("showUrl",showUrl);

		String _secret =MD5Util.createSign(paramsMap);

		WeiXinLog.info("##################### 微信支付中心请求参数 {} #######################",JSONUtil.getJson(paramsMap));
		if(!_secret.equals(secret)){ //如果双方密钥摘要不一致
			return "secret签名失败,非法访问!";
		}

		SubSettlement subSettlement=weiXinSubService.getSubSettlement(outTradeNo, userId);
		if(AppUtils.isBlank(subSettlement)){
			return "找不到支付单据!" ;
		}
		if(subSettlement.getIsClearing()){
			return Constants.SUCCESS ;
		}
		return Constants.FAIL;
	}


}






