/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao;
 
import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.weixin.WeixinToken;

/**
 * The Class WeixinTokenDao.
 */

public interface WeixinTokenDao extends GenericDao<WeixinToken, Long> {
    
	/**
	 * 获取微信公众号的token
	 * @param tonenType 
	 * @return
	 */
    public abstract WeixinToken getWeixinToken(String tonenType);

	public abstract WeixinToken getWeixinToken(Long id);
	
    public abstract int deleteWeixinToken(WeixinToken weixinToken);
	
	public abstract Long saveWeixinToken(WeixinToken weixinToken);
	
	public abstract int updateWeixinToken(WeixinToken weixinToken);
	
 }
