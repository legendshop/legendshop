/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.service.impl;

import com.legendshop.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.business.api.message.WeixinTokenApi;
import com.legendshop.business.bean.WeixinAccessTokenResponse;
import com.legendshop.business.constant.WxTokenTypeEnum;
import com.legendshop.business.dao.WeixinTokenDao;
import com.legendshop.business.service.WeiXinSubService;
import com.legendshop.business.service.WeixinTokenService;
import com.legendshop.business.util.WeiXinLog;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.entity.weixin.WeixinToken;

/**
 * @author tony
 */
@Service("weixinTokenService")
public class WeixinTokenServiceImpl  implements WeixinTokenService{
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(WeixinTokenServiceImpl.class);
	
	private final static int JSAPI_TICKET_EXPIRESIN=7200; //7200秒之内的值
	
	@Autowired
    private WeixinTokenDao weixinTokenDao;
    
	@Autowired
    private WeiXinSubService weiXinSubService;

	public String getAccessToken() {
		WeixinToken weixinToken = weixinTokenDao.getWeixinToken(WxTokenTypeEnum.ACCESS_TOKEN.value());
		if (AppUtils.isNotBlank(weixinToken)) {
			// 判断有效时间 是否超过2小时
			java.util.Date end = new java.util.Date();
			java.util.Date start = new java.util.Date(weixinToken.getValidateTime().getTime());
			if ((end.getTime() - start.getTime()) / 1000 / 3600 >= 1) {//1个小时取一次，防止过期，一天最多200次调用
				return getToken(weixinToken);
			}else{
				return weixinToken.getToken();
			}
		} else {
            return getToken(null);
		}
	}
	
	private String getToken(WeixinToken weixinToken){
		
		PayType payType = weiXinSubService.getPayTypeById(PayTypeEnum.WX_PAY.value());
		
		log.info("############# 支付参数  {} ########## ",JSONUtil.getJson(payType));
		if (payType == null) {
			return null;
		} 
		
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		String appid = jsonObject.getString(WxConfigUtil.APPID);
		String appSecret=jsonObject.getString(WxConfigUtil.APPSECRET);
		
		WeixinAccessTokenResponse weixinAccessToken = WeixinTokenApi.getAccessToken(appid, appSecret);
		if(!weixinAccessToken.isSuccess()){
			log.info("############## 获取微信公众号 accessToken 错误: {} #################", weixinAccessToken.getErrmsg());
			return null;
		}
		
		String accessToken = weixinAccessToken.getAccess_token();
		int  expiresIn = weixinAccessToken.getExpires_in();

		if(AppUtils.isNotBlank(weixinToken)){
			weixinToken.setValidateTime(DateUtil.getNowTime());
			weixinToken.setToken(accessToken);
			weixinToken.setExpiresIn(expiresIn);
			weixinTokenDao.update(weixinToken);
		}else{
			 weixinToken =new WeixinToken();
			weixinToken.setExpiresIn(expiresIn);
			weixinToken.setValidateTime(DateUtil.getNowTime());
			weixinToken.setTonkenType(WxTokenTypeEnum.ACCESS_TOKEN.value());
			weixinToken.setToken(accessToken);
			weixinTokenDao.saveWeixinToken(weixinToken);
		}

		return accessToken;
	}

	@Override
	public String getJsapiTicket() {
		WeixinToken ticket = weixinTokenDao.getWeixinToken(WxTokenTypeEnum.JSAPI_TICKET.value());
		if (AppUtils.isNotBlank(ticket)) {
			// 判断有效时间 是否超过2小时
			java.util.Date end = new java.util.Date();
			java.util.Date start = new java.util.Date(ticket.getValidateTime().getTime());
			if ((end.getTime() - start.getTime()) / 1000 / 3600 >= 2) {
				return getJsapiTicket(ticket);
			}else{
				return ticket.getToken();
			}
		} else {
            return getJsapiTicket(null);
		}
	}
	
	private String getJsapiTicket(WeixinToken weixinToken){
		String jsapi_ticket = "";
		String accessToken=getAccessToken();
		String requestUrl = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=" + accessToken;
		String text=HttpUtil.httpsRequest(requestUrl, "GET",null);
		if(text!=null){
        	JSONObject jsonStr=JSONObject.parseObject(text);
        	if (null != jsonStr) {
    			try {
    				jsapi_ticket =jsonStr.getString("ticket");
    				
    				if(AppUtils.isNotBlank(weixinToken)){
        				weixinToken.setValidateTime(DateUtil.getNowTime());
        				weixinToken.setToken(jsapi_ticket);
        				weixinTokenDao.update(weixinToken);
    				}else{
    					weixinToken =new WeixinToken();
    					weixinToken.setExpiresIn(JSAPI_TICKET_EXPIRESIN);
        				weixinToken.setValidateTime(DateUtil.getNowTime());
        				weixinToken.setTonkenType(WxTokenTypeEnum.JSAPI_TICKET.value());
        				weixinToken.setToken(jsapi_ticket);
        				weixinTokenDao.saveWeixinToken(weixinToken);
    				}
    				/* 
    			     * 重置token
    				 */
    				 WeiXinLog.info("接口调用凭证ticket：" + jsapi_ticket);
    				return jsapi_ticket;
    			} catch (Exception e) {
    				e.printStackTrace();
    				jsapi_ticket = null;
    				// 获取token失败
    				String wrongMessage = "获取jsapi_ticket失败 errcode:{} errmsg:{}"
    						+ jsonStr.getIntValue("errcode")
    						+ jsonStr.getString("errmsg");
    				System.out.println(wrongMessage);
    			}
    		}
        }
		return text;
	}
 
}
