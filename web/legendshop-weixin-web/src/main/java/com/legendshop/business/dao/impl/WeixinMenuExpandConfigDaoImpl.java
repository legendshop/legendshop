/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.WeixinMenuExpandConfigDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.weixin.WeixinMenuExpandConfig;

/**
 * The Class WeixinMenuExpandConfigDaoImpl.
 */

@Repository
public class WeixinMenuExpandConfigDaoImpl extends GenericDaoImpl<WeixinMenuExpandConfig, Long> implements WeixinMenuExpandConfigDao  {
     
    public List<WeixinMenuExpandConfig> getWeixinMenuExpandConfig(String userName){
   		return this.queryByProperties(new EntityCriterion().eq("userName", userName));
    }

    @Cacheable(value="WeixinMenuExpandConfig",key="#id")
	public WeixinMenuExpandConfig getWeixinMenuExpandConfig(Long id){
		return getById(id);
	}
	
    public int deleteWeixinMenuExpandConfig(WeixinMenuExpandConfig weixinMenuExpandConfig){
    	return delete(weixinMenuExpandConfig);
    }
	
	public Long saveWeixinMenuExpandConfig(WeixinMenuExpandConfig weixinMenuExpandConfig){
		return save(weixinMenuExpandConfig);
	}
	
	public int updateWeixinMenuExpandConfig(WeixinMenuExpandConfig weixinMenuExpandConfig){
		return update(weixinMenuExpandConfig);
	}

	@Override
	public List<WeixinMenuExpandConfig> getWeixinMenuExpandConfig() {
		return this.queryLimit("select ex.id as id,ex.name as name ,ex.keyword as keyword,ex.class_service as classService from ls_weixin_menu_expandconfig as ex", WeixinMenuExpandConfig.class, 0, 100);
	}
	
 }
