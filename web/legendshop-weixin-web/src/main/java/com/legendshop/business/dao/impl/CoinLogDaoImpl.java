/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.business.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.CoinLogDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.coin.CoinLog;

/**
 * The Class CouponDaoImpl.
 */
@Repository
public class CoinLogDaoImpl extends GenericDaoImpl<CoinLog, Long> implements CoinLogDao  {
	
	/**
	 * 保存金币日志
	 */
	@Override
	public Long saveCoinLog(CoinLog entity){
		return this.save(entity);
	}

}







