package com.legendshop.business.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.business.service.TulingApiProcessService;
import com.legendshop.util.HttpUtil;

@Service("tulingApiProcessService")
public class TulingApiProcessServiceImpl  implements TulingApiProcessService{

	private static String APIKEY = "4740cadb86fd959b4a8750d4f0918941";
	
	@Override
	public String getTulingResult(String content) {
		System.out.println("-------------------机器人处理--------------------------");
		String info="";
		try {
			 info=URLEncoder.encode(content,"utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String urlAddress="http://www.tuling123.com/openapi/api?key="+APIKEY+"&info="+info;
		String result=HttpUtil.httpGet(urlAddress);
		 /** 请求失败处理 */ 
		if(null==result){ 
		    return "对不起，你说的话真是太高深了……"; 
		}
		JSONObject json = JSONObject.parseObject(result);
		int code= json.getIntValue("code");
		if(100000==code){
			result = json.getString("text"); 
		}else if(code==40004){
			result="对不起，你说的话真是太高深了……";
		}
		return result;
	}

}
