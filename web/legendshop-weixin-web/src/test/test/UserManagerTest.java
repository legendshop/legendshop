package test;

import java.util.Iterator;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.legendshop.business.bean.user.WeixinUserList;
import com.legendshop.business.util.WxConfigUtil;
import com.legendshop.util.HttpUtil;
import com.legendshop.util.WeiXinErrcodeUtil;

public class UserManagerTest {

	
	public static void main1(String[] args) {
		// 拼接请求地址
	    String requestUrl = WxConfigUtil.GET_USERLIST_URL.replace("ACCESS_TOKEN","_HDTvzWJetzY378thGN7lPrHBud2Kaxwio5RsuwIBI0blyYTWD3PAsA3wmmp0Aps6Vn5_Swz2YSG0AWzJhCDI_f1fx_ox6-6Yc_Ca5q3DtsFVCaADAQGN").replace("NEXT_OPENID", "");
	 
	 // 拼接请求地址
	    String GET_BATCHGET_USERLIST_URL = WxConfigUtil.GET_BATCHGET_USERLIST_URL.replace("ACCESS_TOKEN","_HDTvzWJetzY378thGN7lPrHBud2Kaxwio5RsuwIBI0blyYTWD3PAsA3wmmp0Aps6Vn5_Swz2YSG0AWzJhCDI_f1fx_ox6-6Yc_Ca5q3DtsFVCaADAQGN");
	 
	    
	    // 获取用户信息
	 	String text=HttpUtil.httpsRequest(requestUrl,	"GET", null);
	 	 WeixinUserList list=null;
	 	 if(text!=null){
	         JSONObject jsonObject=JSONObject.parseObject(text);
	         if(jsonObject!=null ){
	        	 try {
	        			list=new WeixinUserList();
		             	if(jsonObject.containsKey("errcode")){
		             		list.setErrcode(jsonObject.getInteger("errcode"));
		             		list.setErrmsg(WeiXinErrcodeUtil.getErrorMsg(jsonObject.getInteger("errcode")));
			        	 }
		             	int total = jsonObject.getIntValue("total");
		     			int count = jsonObject.getIntValue("count");
		             	list.setTotal(total);
		             	list.setCount(count);
		             	list.setNextOpenId(jsonObject.getString("next_openid"));
		                JSONObject dataObject = (JSONObject) jsonObject.get("data");
		                JSONArray lstOpenid = dataObject.getJSONArray("openid");
		                int iSize = lstOpenid.size();
		                if(iSize>0){
		                	JSONArray jsonArray=new JSONArray();
		                	for (int i = 1; i <= iSize; i++) {
		                		  JSONObject object = new JSONObject();
		                		  String openId = lstOpenid.getString(i-1);
								  object.put("openid", openId);
								  object.put("lang", "zh-CN");
								  jsonArray.add(object);
		                		 if(i%100==0){   //每100次提交一次
									 /*JsoN*/
									 JSONObject xx = new JSONObject();
									 xx.put("user_list", jsonArray);
									 String  result=HttpUtil.httpsRequest(GET_BATCHGET_USERLIST_URL,	"POST", xx.toString());
									 JSONObject userObject=JSONObject.parseObject(result);
									 JSONArray info_list = userObject.getJSONArray("user_info_list");
									 for (Iterator<Object> iterator = info_list.iterator(); iterator.hasNext();) {
										 JSONObject object2 = (JSONObject) iterator.next();
									}
									 jsonArray=null;
									 jsonArray=new JSONArray();
								}
							}
		                }
		                System.out.println(dataObject.toString());
	     				/*for (int i = 0; i < iSize; i++) {
	     					String openId = lstOpenid.getString(i);
	     					System.out.println("openId------>"+openId);
	     				}*/
	             } catch (JSONException e) {
	                 int errorCode = jsonObject.getIntValue("errcode");
	             }
	         }
	 	 }
	}
	
	public static void main2(String[] args) {
		  JSONObject dataObject1 = new JSONObject();
		  dataObject1.put("openid", "otvxTs4dckWG7imySrJd6jSi0CWE");
		  dataObject1.put("lang", "zh-CN");
		  
		  JSONObject dataObject2 = new JSONObject();
		  dataObject2.put("openid", "otvxTs4dckWG7imySrJd6jSi0CWE");
		  dataObject2.put("lang", "zh-CN");
		  
		  JSONArray jsonArray=new JSONArray();
		  jsonArray.add(dataObject1);
		  jsonArray.add(dataObject2);
		  
		  JSONObject xx = new JSONObject();
		  xx.put("user_list", jsonArray);
		  
		  System.out.println(xx.toString());

	}
	
	public static void main(String[] args) {
        int init = 2364;// 每隔1000条循环一次  
  
        int total = 1000;  
        int cycelTotal = total / init; 
        if (total % init != 0) {  
            cycelTotal += 1;  
        }  
        System.out.println("循环保存的次数："+cycelTotal);//循环多少次  
	}
}
