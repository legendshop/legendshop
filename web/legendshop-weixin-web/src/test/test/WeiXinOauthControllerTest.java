package test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.business.util.WeiXinUtil;
import com.legendshop.business.util.WxPropertiesUtils;
import com.legendshop.util.AppUtils;
import com.legendshop.util.HttpUtil;

public class WeiXinOauthControllerTest {

	public static void main(String[] args) {
		
	}
	
	@RequestMapping(value = "/wxPay",method=RequestMethod.GET)
	public String WxPay(HttpServletRequest req, HttpServletResponse resp)
			throws UnsupportedEncodingException {
		// 判断是否微信环境, 5.0 之后的支持微信支付
		boolean isweixin = WeiXinUtil.isWeiXin(req);
		if (!isweixin) {
			return "/public/error";
		}
		String openid = (String) req.getSession().getAttribute("openid");
		String oauth2url="http://test.legendshop.cn/wxpay/weixinJSBridge/wxPay";
		String appId = WxPropertiesUtils.getAppId();
		System.out.println("openid:"+openid);
		if (openid == null) {
			String oautoURL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid="
					+ appId
					+ "&redirect_uri="
					+ URLEncoder.encode("http://test.legendshop.cn/oauth?oauth2url="+oauth2url, "UTF-8")
					+ "&response_type=code&scope=snsapi_base&state=patient#wechat_redirect";
			System.out.println("前往网页授权！");
			return "redirect:" + oautoURL; 
		}

		return "/wxpay/WeixinJSBridge";  
	}
	
	
	
	 public String oauth(HttpServletRequest request,HttpServletResponse response, 
			 @RequestParam String code,
			 @RequestParam String state,
			 @RequestParam String oauth2url
			 ) throws IOException {
		// 判断是否微信环境, 5.0 之后的支持微信支付
		boolean isweixin = WeiXinUtil.isWeiXin(request);
		if (!isweixin) {
			return "/public/error";
		}
			
		//code不为空则代表是从微信调整过来的
	   if (code != null && AppUtils.isNotBlank(code)) {
		   String urlString="https://api.weixin.qq.com/sns/oauth2/access_token?appid="+WxPropertiesUtils.getAppId() + "&secret=" + WxPropertiesUtils.getAppSecret()+"&code=" + code + "&grant_type=authorization_code";
		   System.out.println(urlString);
		    String text=HttpUtil.httpsRequest(urlString,"GET",null);
	 		if(text!=null){
	         	JSONObject jsonObject=JSONObject.parseObject(text);
	         	 if(jsonObject!=null ){
	         		 request.getSession().setAttribute("openid", jsonObject.get("openid").toString());
	             }
	 		}
	    }
	   return "redirect:" + oauth2url; 
    }
	 
	 
}
