package com.legendshop.model.dto.app ;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *手机端楼层内容关联表
 */
@ApiModel("楼层内容Dto")
public class MobileFloorItemDto implements Serializable {
	private static final long serialVersionUID = -1859704659429743361L;

	/** 唯一标识id */
	@ApiModelProperty(value = "唯一标识id")
	private Long id; 
		
	/** 楼层id */
	@ApiModelProperty(value = "楼层id")
	private Long parentId; 
		
	/** 顺序 */
	@ApiModelProperty(value = "顺序")
	private Long seq; 
		
	/** 标题 **/
	@ApiModelProperty(value = "标题")
	private String title;
	
	/** 链接 **/
	@ApiModelProperty(value = "链接")
	private String linkPath;
	
	/** 图片 **/
	@ApiModelProperty(value = "图片")
	private String pic;
		
	
	public MobileFloorItemDto() {
    }


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getParentId() {
		return parentId;
	}


	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}


	public Long getSeq() {
		return seq;
	}


	public void setSeq(Long seq) {
		this.seq = seq;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getLinkPath() {
		return linkPath;
	}


	public void setLinkPath(String linkPath) {
		this.linkPath = linkPath;
	}


	public String getPic() {
		return pic;
	}


	public void setPic(String pic) {
		this.pic = pic;
	}
	

} 
