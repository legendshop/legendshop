/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.app;


/**
 * 查询参数
 * 
 */
public class ProductSearchParms {

	/**
	 * 排序
	 */
	private String orders;
	/**
	 * 分类ID
	 */
	private Long categoryId;
	/**
	 * 页码
	 */
	private Integer page;
	/**
	 * 搜索关键字
	 */
	private String keyword;

	/** 是否推荐产品. */
	private String commend;
	
	/** 是否热门产品. */
	private String hot;
	
}
