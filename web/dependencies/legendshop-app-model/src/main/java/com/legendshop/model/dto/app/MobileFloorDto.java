package com.legendshop.model.dto.app ;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *手机端首页楼层表
 */
@ApiModel("首页楼层Dto")
public class MobileFloorDto implements Serializable {
	private static final long serialVersionUID = -2406079399510715363L;

	/** 唯一标识id */
	@ApiModelProperty(value = "唯一标识id")
	private Long id; 
		
	/** 楼层名称 */
	@ApiModelProperty(value = "楼层名称")
	private String name; 
		
	/** 顺序 */
	@ApiModelProperty(value = "顺序")
	private Long seq; 

	/** 所属店铺ID */
	@ApiModelProperty(value = "所属店铺ID")
	private Long shopId; 
		
	/** 链接 */
	@ApiModelProperty(value = "链接")
	private String url; 
		
	/** 开关；0：楼层左边大；1楼层左边小 */
	@ApiModelProperty(value = "开关；0：楼层左边大；1楼层左边小")
	private Long style; 
	
	/** 楼层内容列表 */
	@ApiModelProperty(value = "楼层内容列表")
	List<MobileFloorItemDto> floorItemList = new ArrayList<MobileFloorItemDto>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getSeq() {
		return seq;
	}

	public void setSeq(Long seq) {
		this.seq = seq;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getStyle() {
		return style;
	}

	public void setStyle(Long style) {
		this.style = style;
	}

	public List<MobileFloorItemDto> getFloorItemList() {
		return floorItemList;
	}

	public void setFloorItemList(List<MobileFloorItemDto> floorItemList) {
		this.floorItemList = floorItemList;
	}
} 
