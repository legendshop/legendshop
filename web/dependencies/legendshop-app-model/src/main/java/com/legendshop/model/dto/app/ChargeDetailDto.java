package com.legendshop.model.dto.app;

import java.io.Serializable;

/**
 * 充值明细
 * @author Administrator
 *
 */
public class ChargeDetailDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private String addTime;
	private String adminUserName;
	private String adminUserNote;
	private String amount;
	private String id;
	private String nickName;
	private String paymentCode;
	private String paymentName;
	private String paymentState;
	private long paymentTime;
	private String sn;
	private String tradeSn;
	private String userId;
	private String userName;
	public String getAddTime() {
		return addTime;
	}
	public void setAddTime(String addTime) {
		this.addTime = addTime;
	}
	public String getAdminUserName() {
		return adminUserName;
	}
	public void setAdminUserName(String adminUserName) {
		this.adminUserName = adminUserName;
	}
	public String getAdminUserNote() {
		return adminUserNote;
	}
	public void setAdminUserNote(String adminUserNote) {
		this.adminUserNote = adminUserNote;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getPaymentCode() {
		return paymentCode;
	}
	public void setPaymentCode(String paymentCode) {
		this.paymentCode = paymentCode;
	}
	public String getPaymentName() {
		return paymentName;
	}
	public void setPaymentName(String paymentName) {
		this.paymentName = paymentName;
	}
	public String getPaymentState() {
		return paymentState;
	}
	public void setPaymentState(String paymentState) {
		this.paymentState = paymentState;
	}
	public long getPaymentTime() {
		return paymentTime;
	}
	public void setPaymentTime(long paymentTime) {
		this.paymentTime = paymentTime;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getTradeSn() {
		return tradeSn;
	}
	public void setTradeSn(String tradeSn) {
		this.tradeSn = tradeSn;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
}
