package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 订单物流信息
 */
public class OrderLogisticsDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String dvyFlowId;//物流号
	private String logs;//物流信息详情
	private String deyName;//w物流公司
	public String getDeyName() {
		return deyName;
	}

	public void setDeyName(String deyName) {
		this.deyName = deyName;
	}

	public String getDvyFlowId() {
		return dvyFlowId;
	}

	public void setDvyFlowId(String dvyFlowId) {
		this.dvyFlowId = dvyFlowId;
	}

	public String getLogs() {
		return logs;
	}

	public void setLogs(String logs) {
		this.logs = logs;
	}	

}
