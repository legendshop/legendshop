package com.legendshop.model.dto.app;

import java.io.Serializable;

public class RefundBankInfoDto implements Serializable{

	/**
	 * 退款银行账号信息
	 */
	private static final long serialVersionUID = 1L;
	private String applyUserId;	 
	private String applyUserName;
	private int  areaid;
	private String areaName;
	private String bankAccount;
	private String bankCode;
	private String bankUserName;
	private int cityid;
	private String cityName;
	private long createTime;
	private long id;
	private int provinceid;
	private String provinceName;
	private long repayUserifoId;
	private String txtbranch;
	private int version;
	public String getApplyUserId() {
		return applyUserId;
	}
	public void setApplyUserId(String applyUserId) {
		this.applyUserId = applyUserId;
	}
	public String getApplyUserName() {
		return applyUserName;
	}
	public void setApplyUserName(String applyUserName) {
		this.applyUserName = applyUserName;
	}
	public int getAreaid() {
		return areaid;
	}
	public void setAreaid(int areaid) {
		this.areaid = areaid;
	}
	
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public String getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getBankUserName() {
		return bankUserName;
	}
	public void setBankUserName(String bankUserName) {
		this.bankUserName = bankUserName;
	}
	public int getCityid() {
		return cityid;
	}
	public void setCityid(int cityid) {
		this.cityid = cityid;
	}
	public long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getProvinceid() {
		return provinceid;
	}
	public void setProvinceid(int provinceid) {
		this.provinceid = provinceid;
	}
	public long getRepayUserifoId() {
		return repayUserifoId;
	}
	public void setRepayUserifoId(long repayUserifoId) {
		this.repayUserifoId = repayUserifoId;
	}
	public String getTxtbranch() {
		return txtbranch;
	}
	public void setTxtbranch(String txtbranch) {
		this.txtbranch = txtbranch;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	
	

}
