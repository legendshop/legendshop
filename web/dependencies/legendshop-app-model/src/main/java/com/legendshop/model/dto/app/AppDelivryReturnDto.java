/**
 * 
 */
package com.legendshop.model.dto.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author quanzc
 * 查看快递返回
 */
@ApiModel(value="AppDelivryReturnDto查看快递返回") 
public class AppDelivryReturnDto {
    
	/**物流单号**/
	@ApiModelProperty(value="物流单号")
	private String dvyFlowId;
	
	/**物流公司**/
	@ApiModelProperty(value="物流公司")
	private String deyName;
	
	/**物流信息**/
	@ApiModelProperty(value="物流信息")
	private String logs;
	
	public String getLogs() {
		return logs;
	}



	public void setLogs(String logs) {
		this.logs = logs;
	}



	public String getDvyFlowId() {
		return dvyFlowId;
	}



	public void setDvyFlowId(String dvyFlowId) {
		this.dvyFlowId = dvyFlowId;
	}



	public String getDeyName() {
		return deyName;
	}



	public void setDeyName(String deyName) {
		this.deyName = deyName;
	}





/*	public static class Status {
		private String dateString;
		private Date date;
		private String content;

		public String getDateString() {
			return dateString;
		}

		public void setDateString(String dateString) {
			this.dateString = dateString;
		}

		public Date getDate() {
			return date;
		}

		public void setDate(Date date) {
			this.date = date;
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = content;
		}
	}*/
}
