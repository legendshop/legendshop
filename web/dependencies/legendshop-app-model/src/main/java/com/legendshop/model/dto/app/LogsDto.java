package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 订单物流信息
 */
public class LogsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String dateString;
	private Date date;
	private String content;
	public String getDateString() {
			return dateString;
		}
		public void setDateString(String dateString) {
			this.dateString = dateString;
		}
		public Date getDate() {
			return date;
		}
		public void setDate(Date date) {
			this.date = date;
		}
		public String getContent() {
			return content;
		}
		public void setContent(String content) {
			this.content = content;
		}
}
