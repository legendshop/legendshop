package com.legendshop.model.dto.app.cartorder;

import java.io.Serializable;
import java.util.List;

import com.legendshop.model.dto.app.shopcart.ShopCartItemDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * APP 购物车项组
 * 一个商家购物车，包含多个都购物车项组(默认有个none组,)，一个组包含多个购物车项
 * @author Tony
 *
 */
@ApiModel("购物车项组Dto(包含多个都购物车项组(默认有个none组,)，一个组包含多个购物车项)")
public class ShopGroupCartOrderItemDto implements Serializable{

	private static final long serialVersionUID = -4634013345653223942L;
	
	/** 满减 / 满折 / 限时 / none */
	@ApiModelProperty(value = "满减 / 满折 / 限时 / none")
	private String type;
	
	/** 促销详情 */
	@ApiModelProperty(value = "促销详情 ")
	private String  promotionInfo;
	
	/** 商品列表 **/
	@ApiModelProperty(value = "商品列表")
	private List<ShopCartItemDto> cartItems;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPromotionInfo() {
		return promotionInfo;
	}

	public void setPromotionInfo(String promotionInfo) {
		this.promotionInfo = promotionInfo;
	}

	public List<ShopCartItemDto> getCartItems() {
		return cartItems;
	}

	public void setCartItems(List<ShopCartItemDto> cartItems) {
		this.cartItems = cartItems;
	}
	
	
}
