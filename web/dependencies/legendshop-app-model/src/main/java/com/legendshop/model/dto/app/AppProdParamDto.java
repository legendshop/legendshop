/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.app;

import java.io.Serializable;
/**
 * 商品 参数属性dto
 *
 */
public class AppProdParamDto implements Serializable  {

	private static final long serialVersionUID = 6757664890894682948L;

	private String paramName;//参数名
	
	private String paramValueName;//参数值
	
	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValueName() {
		return paramValueName;
	}

	public void setParamValueName(String paramValueName) {
		this.paramValueName = paramValueName;
	}

	
}
