/**
 * 
 */
package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.Date;

/**
 * @author quanzc
 * 优惠卷
 */
public class AppCouponDto implements Serializable{

	private static final long serialVersionUID = 5183723647743857778L;

	/** 礼券 ID */
	private Long couponId; 
		
	/** 礼券 名称 */
	private String couponName; 
		
	/** 礼券 号码 前缀 */
	private String couponNumPrefix; 
		
	/** 礼券状态 */
	private Integer status; 
		
	/** 劵值满多少金额 */
	private Double fullPrice; 
		
	/** 劵值减多少金额 */
	private Double offPrice; 
		
	/** 活动开始时间 */
	private Date startDate; 
		
	/** 活动结束时间 */
	private Date endDate; 
		
	/** 领取频率 */
	private Double frequency; 
		
	/** 礼券的类型 */
	private Integer couponType; 
		
	/** 如何发放此类型红包 */
	private Integer sendType; 
		
	/** 领取上限 */
	private Long getLimit; 
		
	/** 初始化券数量 */
	private Long couponNumber; 
		
	/** 绑定礼券数量 */
	private Long bindCouponNumber; 
		
	/** 发放礼券数量 */
	private Long sendCouponNumber; 
		
	/** 使用礼券数量 */
	private Long useCouponNumber; 
		
	/** 创建时间 */
	private Date createDate; 
		
	/** 描述 */
	private String description; 
		
	/** 通知方式[站内信,邮件,短信] */
	private String messageType; 
	
	/** 礼券用户ID */
	private Long couponUsrId;  
	
	/**优惠礼券券号**/
	private String couponSn;
	
	/**使用日期**/
	private Date useTime;
	
	/**
	 * 活动的时间状态(1.已过期 2.进行中 3,将要开始)
	 */
	private int timeStatus;
	/** * 剩余时间*/
	private String surplusTime;
	
	/**券值*/
	private String couponPrice;
	
	private Long shopId;
	
	private String shopName;
	private boolean isSelected;//是否选中
	
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	public Long getCouponId() {
		return couponId;
	}

	public void setCouponId(Long couponId) {
		this.couponId = couponId;
	}

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public String getCouponNumPrefix() {
		return couponNumPrefix;
	}

	public void setCouponNumPrefix(String couponNumPrefix) {
		this.couponNumPrefix = couponNumPrefix;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Double getFullPrice() {
		return fullPrice;
	}

	public void setFullPrice(Double fullPrice) {
		this.fullPrice = fullPrice;
	}

	public Double getOffPrice() {
		return offPrice;
	}

	public void setOffPrice(Double offPrice) {
		this.offPrice = offPrice;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Double getFrequency() {
		return frequency;
	}

	public void setFrequency(Double frequency) {
		this.frequency = frequency;
	}

	public Integer getCouponType() {
		return couponType;
	}

	public void setCouponType(Integer couponType) {
		this.couponType = couponType;
	}

	public Integer getSendType() {
		return sendType;
	}

	public void setSendType(Integer sendType) {
		this.sendType = sendType;
	}

	public Long getGetLimit() {
		return getLimit;
	}

	public void setGetLimit(Long getLimit) {
		this.getLimit = getLimit;
	}

	public Long getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(Long couponNumber) {
		this.couponNumber = couponNumber;
	}

	public Long getBindCouponNumber() {
		return bindCouponNumber;
	}

	public void setBindCouponNumber(Long bindCouponNumber) {
		this.bindCouponNumber = bindCouponNumber;
	}

	public Long getSendCouponNumber() {
		return sendCouponNumber;
	}

	public void setSendCouponNumber(Long sendCouponNumber) {
		this.sendCouponNumber = sendCouponNumber;
	}

	public Long getUseCouponNumber() {
		return useCouponNumber;
	}

	public void setUseCouponNumber(Long useCouponNumber) {
		this.useCouponNumber = useCouponNumber;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public Long getCouponUsrId() {
		return couponUsrId;
	}

	public void setCouponUsrId(Long couponUsrId) {
		this.couponUsrId = couponUsrId;
	}

	public String getCouponSn() {
		return couponSn;
	}

	public void setCouponSn(String couponSn) {
		this.couponSn = couponSn;
	}

	public Date getUseTime() {
		return useTime;
	}

	public void setUseTime(Date useTime) {
		this.useTime = useTime;
	}

	public int getTimeStatus() {
		return timeStatus;
	}

	public void setTimeStatus(int timeStatus) {
		this.timeStatus = timeStatus;
	}

	public String getSurplusTime() {
		return surplusTime;
	}

	public void setSurplusTime(String surplusTime) {
		this.surplusTime = surplusTime;
	}

	public String getCouponPrice() {
		return couponPrice;
	}

	public void setCouponPrice(String couponPrice) {
		this.couponPrice = couponPrice;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	
	
	
	
}
