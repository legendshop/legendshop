package com.legendshop.model.dto.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用户追加评论参数
 */
@ApiModel("用户追加评论参数")
public class AppUserAddCommParamsDto{
	
	/** 评论Id */
	@ApiModelProperty(value = "评论Id")
	private Long prodCommId;
	
	/** 追加评论内容 */
	@ApiModelProperty(value = "追加评论内容")
	private String content;
	
	/** 评论图片, 多个已逗号隔开 */
	@ApiModelProperty(value = "评论图片, 多个已逗号隔开")
	private String photos;
	
	/** vue上传的图片 */
	@ApiModelProperty(value = "vue上传的图片")
	private String[] vuePhotos;

	public Long getProdCommId() {
		return prodCommId;
	}

	public void setProdCommId(Long prodCommId) {
		this.prodCommId = prodCommId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPhotos() {
		return photos;
	}

	public void setPhotos(String photos) {
		this.photos = photos;
	}
	
	public String[] getVuePhotos() {
		return vuePhotos;
	}

	public void setVuePhotos(String[] vuePhotos) {
		this.vuePhotos = vuePhotos;
	}
	
}
