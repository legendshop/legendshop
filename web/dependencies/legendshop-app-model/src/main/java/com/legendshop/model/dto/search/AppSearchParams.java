/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.search;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app搜索参数.
 */
@ApiModel("搜索参数")
public class AppSearchParams {
	
	/** 当前页码. */
	@ApiModelProperty(value = "当前页码")
	protected Integer curPageNO;
	
	/** 每页条数  初始化10条. */
	@ApiModelProperty(value = "每页条数  初始化10条")
	protected Integer pageSize;
	
	/** 用户输入的关键字. */
	@ApiModelProperty(value = "用户输入的关键字")
	protected String keyword;
	
	/** 排序方式. */
	@ApiModelProperty(value = "排序方式")
	protected String orderWay;
	
	/** 排序方向. */
	@ApiModelProperty(value = "排序方向")
	protected String orderType;

	/**
	 * The Constructor.
	 */
	public AppSearchParams() {
		
	}

	/**
	 * Gets the cur page no.
	 *
	 * @return the cur page no
	 */
	public Integer getCurPageNO() {
		if(null == curPageNO){
			curPageNO = 1;
		}
		return curPageNO;
	}

	/**
	 * Sets the cur page no.
	 *
	 * @param curPageNO the cur page no
	 */
	public void setCurPageNO(Integer curPageNO) {
		this.curPageNO = curPageNO;
	}

	/**
	 * Gets the page size.
	 *
	 * @return the page size
	 */
	public Integer getPageSize() {
		if(null == pageSize){
			pageSize = 10; 
		}
		return pageSize;
	}

	/**
	 * Sets the page size.
	 *
	 * @param pageSize the page size
	 */
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * Gets the keyword.
	 *
	 * @return the keyword
	 */
	public String getKeyword() {
		return keyword;
	}

	/**
	 * Sets the keyword.
	 *
	 * @param keyword the keyword
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	/**
	 * Gets the order way.
	 *
	 * @return the order way
	 */
	public String getOrderWay() {
		return orderWay;
	}

	/**
	 * Sets the order way.
	 *
	 * @param orderWay the order way
	 */
	public void setOrderWay(String orderWay) {
		this.orderWay = orderWay;
	}

	/**
	 * Gets the order type.
	 *
	 * @return the order type
	 */
	public String getOrderType() {
		return orderType;
	}

	/**
	 * Sets the order type.
	 *
	 * @param orderType the order type
	 */
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
}