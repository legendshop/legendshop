package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//运输费用
public class TransfeeDto implements Serializable{

	private static final long serialVersionUID = -6958074103059256416L;

	private Long id ; 
	
	//运费模板id
	private Long transportId;
	
	//运输模式
	private String freightMode;
	
	//续件运费
	private Double transAddFee;
	
	//首件运费
	private Double transFee;
	
	//续件
	private Integer transAddWeight;
	
	//首件
	private Integer transWeight;
	
	//状态
	private Integer status;
	
	private String city;
	
	private Long cityId;
	
	private List<String> cityNameList; 
	
	private List<Long> cityIdList = new ArrayList<Long>();
	
	/** 城市ID **/
	private StringBuilder cityIds;
	
	//城市id
	private List<TransCityDto> transCityList;
	
	private Set<Integer> cityList=new HashSet<Integer>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTransportId() {
		return transportId;
	}

	public void setTransportId(Long transportId) {
		this.transportId = transportId;
	}

	public String getFreightMode() {
		return freightMode;
	}

	public void setFreightMode(String freightMode) {
		this.freightMode = freightMode;
	}

	public Double getTransAddFee() {
		return transAddFee;
	}

	public void setTransAddFee(Double transAddFee) {
		this.transAddFee = transAddFee;
	}

	public Double getTransFee() {
		return transFee;
	}

	public void setTransFee(Double transFee) {
		this.transFee = transFee;
	}

	public Integer getTransAddWeight() {
		return transAddWeight;
	}

	public void setTransAddWeight(Integer transAddWeight) {
		this.transAddWeight = transAddWeight;
	}

	public Integer getTransWeight() {
		return transWeight;
	}

	public void setTransWeight(Integer transWeight) {
		this.transWeight = transWeight;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public List<String> getCityNameList() {
		return cityNameList;
	}

	public void setCityNameList(List<String> cityNameList) {
		this.cityNameList = cityNameList;
	}

	public List<Long> getCityIdList() {
		return cityIdList;
	}

	public void setCityIdList(List<Long> cityIdList) {
		this.cityIdList = cityIdList;
	}

	public StringBuilder getCityIds() {
		return cityIds;
	}

	public void setCityIds(StringBuilder cityIds) {
		this.cityIds = cityIds;
	}

	public List<TransCityDto> getTransCityList() {
		return transCityList;
	}

	public void setTransCityList(List<TransCityDto> transCityList) {
		this.transCityList = transCityList;
	}

	public Set<Integer> getCityList() {
		return cityList;
	}

	public void setCityList(Set<Integer> cityList) {
		this.cityList = cityList;
	}

	
}
