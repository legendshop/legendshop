/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.app;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;




/**
 * 首页轮换图片.
 */
@ApiModel(value="首页轮换图片Dto") 
public class IndexjpgDto implements Serializable {

	private static final long serialVersionUID = 7774127098015903420L;

	/** The img. */
	@ApiModelProperty(value="图片")  
	private String img;

	/** The title. */
	@ApiModelProperty(value="标题")  
	private String title;

	/** The link. */
	@ApiModelProperty(value="链接")  
	private String link;

	@ApiModelProperty(value="店铺id")  
	private Long shopId;

	/**
	 * default constructor.
	 */
	public IndexjpgDto() {
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	
	
}