package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.List;

/**商品属性值下具体图片集合Dto**/
public class PropValueImageDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private List<String> imgList;//图片集合
    private long valueId   ;   //属性值ID
	public List<String> getImgList() {
		return imgList;
	}
	public void setImgList(List<String> imgList) {
		this.imgList = imgList;
	}
	public long getValueId() {
		return valueId;
	}
	public void setValueId(long valueId) {
		this.valueId = valueId;
	}
         
         
}
