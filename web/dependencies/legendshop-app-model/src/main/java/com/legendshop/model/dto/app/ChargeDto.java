package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.List;

/**
 * 充值明细
 * @author Administrator
 *
 */
public class ChargeDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private String totalCount;
	private int currPage;
	private int pageCount;
	private int pageSize;
	private List<ChargeDetailDto>resultList;
	public String getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}
	public int getCurrPage() {
		return currPage;
	}
	public void setCurrPage(int currPage) {
		this.currPage = currPage;
	}
	public int getPageCount() {
		return pageCount;
	}
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public List<ChargeDetailDto> getResultList() {
		return resultList;
	}
	public void setResultList(List<ChargeDetailDto> resultList) {
		this.resultList = resultList;
	}
}
