package com.legendshop.model.dto.app;

import com.legendshop.util.AppUtils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * APP端商品评论查询参数 
 * @author 开发很忙
 *
 */
@ApiModel("商品评论查询参数")
public class AppQueryProductCommentsParams {
	
	/** 商品Id */
	@ApiModelProperty(value = "商品Id")
	private Long prodId;
	
	/** 当前页码 */
	@ApiModelProperty(value = "当前页码")
	private Integer curPageNO;
	
	/** 条件 全部: all, 好评: good, 中评: medium, 差评: poor, 有图: photo, 追评: append*/
	@ApiModelProperty(value = "条件 全部: all, 好评: good, 中评: medium, 差评: poor, 有图: photo, 追评: append")
	private String condition;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Integer getCurPageNO() {
		if(null == curPageNO){
			curPageNO = 1;
		}
		return curPageNO;
	}

	public void setCurPageNO(Integer curPageNO) {
		this.curPageNO = curPageNO;
	}

	public String getCondition() {
		if(AppUtils.isBlank(condition)){
			condition = "all";
		}
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}
}
