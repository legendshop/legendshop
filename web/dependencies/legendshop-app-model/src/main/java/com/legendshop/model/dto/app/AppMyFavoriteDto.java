package com.legendshop.model.dto.app;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value="我收藏的商品Dto") 
public class AppMyFavoriteDto implements Serializable {
	
	private static final long serialVersionUID = 7192977060765292035L;

	/** 唯一标识id. */
	@ApiModelProperty(value="唯一标识id")  
	private Long id;

	/** 商品id. */
	@ApiModelProperty(value="商品id")  
	private Long prodId;

	/** 商品名称. */
	@ApiModelProperty(value="商品名称")  
	private String prodName;

	/** 商品图片 */
	@ApiModelProperty(value="商品图片")  
	private String pic;
	
	/** 商品原价 */
	@ApiModelProperty(value="商品原价")  
	private Double price;
	
	/** 商品现价 */
	@ApiModelProperty(value="商品现价")
	private Double cash;
	
	/** 已收藏数量. */
	@ApiModelProperty(value="已收藏数量") 
	private Long favoriteCount;
	
	/** 商品评论数. */
	@ApiModelProperty(value="商品评论数") 
	private Integer comments;
	
	/** 商品库存. */
	@ApiModelProperty(value="商品库存") 
	private Integer stocks;
	
	/** 商品购买数. */
	@ApiModelProperty(value="商品购买数") 
	private Integer buys;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public Long getFavoriteCount() {
		return favoriteCount;
	}

	public void setFavoriteCount(Long favoriteCount) {
		this.favoriteCount = favoriteCount;
	}

	public Integer getComments() {
		return comments;
	}

	public void setComments(Integer comments) {
		this.comments = comments;
	}

	public Integer getStocks() {
		return stocks;
	}

	public void setStocks(Integer stocks) {
		this.stocks = stocks;
	}

	public Integer getBuys() {
		return buys;
	}

	public void setBuys(Integer buys) {
		this.buys = buys;
	}
}
