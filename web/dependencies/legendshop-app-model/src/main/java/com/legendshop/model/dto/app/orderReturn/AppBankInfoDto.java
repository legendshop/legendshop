package com.legendshop.model.dto.app.orderReturn ;
import java.io.Serializable;
import java.util.Date;

/**
 * 退换货银行账户信息dto
 */
public class AppBankInfoDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9057925602493687823L;

	/**  */
	private Long repayUserifoId; 
		
	/** 申请人姓名 */
	private String applyUserName; 
		
	/** 申请人Id */
	private String applyUserId; 
		
	/** 开户银行 */
	private String bankCode; 
		
	/** 分行地址省份 */
	private Integer provinceid; 
		
	/** 分行地址市 */
	private Integer cityid; 
		
	/** 分行地址区 */
	private Integer areaid; 
		
	/** 分行信息 */
	private String txtbranch; 
		
	/** 银行用户姓名 */
	private String bankUserName; 
		
	/** 银行账号 */
	private String bankAccount; 
		
	/** 创建时间 */
	private Date createTime; 
		
	/** version */
	private Long version;

	public Long getRepayUserifoId() {
		return repayUserifoId;
	}

	public void setRepayUserifoId(Long repayUserifoId) {
		this.repayUserifoId = repayUserifoId;
	}

	public String getApplyUserName() {
		return applyUserName;
	}

	public void setApplyUserName(String applyUserName) {
		this.applyUserName = applyUserName;
	}

	public String getApplyUserId() {
		return applyUserId;
	}

	public void setApplyUserId(String applyUserId) {
		this.applyUserId = applyUserId;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public Integer getProvinceid() {
		return provinceid;
	}

	public void setProvinceid(Integer provinceid) {
		this.provinceid = provinceid;
	}

	public Integer getCityid() {
		return cityid;
	}

	public void setCityid(Integer cityid) {
		this.cityid = cityid;
	}

	public Integer getAreaid() {
		return areaid;
	}

	public void setAreaid(Integer areaid) {
		this.areaid = areaid;
	}

	public String getTxtbranch() {
		return txtbranch;
	}

	public void setTxtbranch(String txtbranch) {
		this.txtbranch = txtbranch;
	}

	public String getBankUserName() {
		return bankUserName;
	}

	public void setBankUserName(String bankUserName) {
		this.bankUserName = bankUserName;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	} 
		
	
	


} 
