package com.legendshop.model.dto.app.cartorder;

import java.io.Serializable;
import java.util.List;

import com.legendshop.model.dto.TransfeeDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 购物车订单详情Dto
 */
@ApiModel("购物车订单详情Dto")
public class ShopCartOrderDto implements Serializable {

	private static final long serialVersionUID = 7608309839997831622L;
	
	 /** 商家ID */
	@ApiModelProperty(value = "商家ID")
	private Long shopId;
	
	 /** 店铺名称 */
	@ApiModelProperty(value = "店铺名称")
	private String shopName;
	
	/** 商家状态 */
	@ApiModelProperty(value = "商家状态")
	private int shopStatus=1;
	
	/** 总价  */
	@ApiModelProperty(value = "总价")
	private Double shopTotalCash=0d;
	
	/** 店铺实际支付总额  */
	@ApiModelProperty(value = "店铺实际支付总额 ")
	private Double shopActualTotalCash=0d;
    
    /** 商品总重量  */
	@ApiModelProperty(value = "商品总重量")
    private Double shopTotalWeight;
    
    /** 商品总体积 */
	@ApiModelProperty(value = "商品总体积")
    private Double shopTotalVolume;
    
    /** 商品总数量 */
	@ApiModelProperty(value = "商品总数量")
    private int totalcount;

   /** 折扣价 */
	@ApiModelProperty(value = "折扣价")
    private Double discountPrice=0d;
    
    /** 当前店铺的运费 */
	@ApiModelProperty(value = "当前店铺的运费 ")
    private Double freightAmount=0d;
    
    /** 是否承担运费  */
	@ApiModelProperty(value = "是否承担运费")
	private Boolean isFreePostage;
	
	/** 获得积分 */
	@ApiModelProperty(value = "获得积分")
    private Double totalIntegral = 0D; 
    
    /** 商品列表  [满减: shopCartItemA,shopCartItemB,none:shopCartItemC,满折:xx,xxb]   **/
	@ApiModelProperty(value = "商品列表  [满减: shopCartItemA,shopCartItemB,none:shopCartItemC,满折:xx,xxb]")
    private List<ShopGroupCartOrderItemDto> groupCartItems; 

	/** 商家配送方式[不分单情况],分单则通过每个商品来处理配送方式 */
	@ApiModelProperty(value = "商家配送方式[不分单情况],分单则通过每个商品来处理配送方式")
	private List<TransfeeDto>  transfeeDtos;
	
	/** 当前订单选中的配送模版[不分单情况] */
	@ApiModelProperty(value = "当前订单选中的配送模版[不分单情况]")
	private TransfeeDto currentSelectTransfee;
	
    /** 卖家留言 */
	@ApiModelProperty(value = "卖家留言")
    private String remark;
    
    /** 是否支持门店 */
	@ApiModelProperty(value = "是否支持门店")
    private boolean supportStore=false;
    
    /** 包邮信息*/
	@ApiModelProperty(value = "包邮信息")
    private String mailfeeStr;
    
    /** 优惠券优惠的金额 */
	@ApiModelProperty(value = "优惠券优惠的金额")
    private double couponOffPrice;
    
    /** 红包优惠的金额 */
	@ApiModelProperty(value = "红包优惠的金额")
    private double redpackOffPrice;

	/** 预售订单skuId */
	@ApiModelProperty(value = "商家ID")
	private Long presellSkuId;


	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public int getShopStatus() {
		return shopStatus;
	}

	public void setShopStatus(int shopStatus) {
		this.shopStatus = shopStatus;
	}

	public Double getShopTotalCash() {
		return shopTotalCash;
	}

	public void setShopTotalCash(Double shopTotalCash) {
		this.shopTotalCash = shopTotalCash;
	}

	public Double getShopActualTotalCash() {
		return shopActualTotalCash;
	}

	public void setShopActualTotalCash(Double shopActualTotalCash) {
		this.shopActualTotalCash = shopActualTotalCash;
	}

	public Double getShopTotalWeight() {
		return shopTotalWeight;
	}

	public void setShopTotalWeight(Double shopTotalWeight) {
		this.shopTotalWeight = shopTotalWeight;
	}

	public Double getShopTotalVolume() {
		return shopTotalVolume;
	}

	public void setShopTotalVolume(Double shopTotalVolume) {
		this.shopTotalVolume = shopTotalVolume;
	}

	public int getTotalcount() {
		return totalcount;
	}

	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}

	public Double getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}

	public Double getFreightAmount() {
		return freightAmount;
	}

	public void setFreightAmount(Double freightAmount) {
		this.freightAmount = freightAmount;
	}

	public Boolean getIsFreePostage() {
		return isFreePostage;
	}

	public void setIsFreePostage(Boolean isFreePostage) {
		this.isFreePostage = isFreePostage;
	}

	public Double getTotalIntegral() {
		return totalIntegral;
	}

	public void setTotalIntegral(Double totalIntegral) {
		this.totalIntegral = totalIntegral;
	}

	public List<ShopGroupCartOrderItemDto> getGroupCartItems() {
		return groupCartItems;
	}

	public void setGroupCartItems(List<ShopGroupCartOrderItemDto> groupCartItems) {
		this.groupCartItems = groupCartItems;
	}

	public List<TransfeeDto> getTransfeeDtos() {
		return transfeeDtos;
	}

	public void setTransfeeDtos(List<TransfeeDto> transfeeDtos) {
		this.transfeeDtos = transfeeDtos;
	}

	public TransfeeDto getCurrentSelectTransfee() {
		return currentSelectTransfee;
	}

	public void setCurrentSelectTransfee(TransfeeDto currentSelectTransfee) {
		this.currentSelectTransfee = currentSelectTransfee;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public boolean isSupportStore() {
		return supportStore;
	}

	public void setSupportStore(boolean supportStore) {
		this.supportStore = supportStore;
	}

	public String getMailfeeStr() {
		return mailfeeStr;
	}

	public void setMailfeeStr(String mailfeeStr) {
		this.mailfeeStr = mailfeeStr;
	}

	public double getCouponOffPrice() {
		return couponOffPrice;
	}

	public void setCouponOffPrice(double couponOffPrice) {
		this.couponOffPrice = couponOffPrice;
	}

	public double getRedpackOffPrice() {
		return redpackOffPrice;
	}

	public void setRedpackOffPrice(double redpackOffPrice) {
		this.redpackOffPrice = redpackOffPrice;
	}

	public Long getPresellSkuId() {
		return presellSkuId;
	}

	public void setPresellSkuId(Long presellSkuId) {
		this.presellSkuId = presellSkuId;
	}
}
