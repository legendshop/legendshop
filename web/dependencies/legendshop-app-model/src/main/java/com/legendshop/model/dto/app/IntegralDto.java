package com.legendshop.model.dto.app;

import java.io.Serializable;

/**
 * 积分
 * @author Administrator
 *
 */
public class IntegralDto implements Serializable{

	private static final long serialVersionUID = 1L;
	public long addTime;
	public String id;
	public String integralNum;
	public String logType;
	public String logDesc;
	public String nickName;
	public String updateUserId;
	public String userId;
	public String userMobile;
	
	public long getAddTime() {
		return addTime;
	}
	public void setAddTime(long addTime) {
		this.addTime = addTime;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIntegralNum() {
		return integralNum;
	}
	public void setIntegralNum(String integralNum) {
		this.integralNum = integralNum;
	}
	public String getLogDesc() {
		return logDesc;
	}
	public void setLogDesc(String logDesc) {
		this.logDesc = logDesc;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserMobile() {
		return userMobile;
	}
	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	

}
