package com.legendshop.model.dto.app;

import java.io.Serializable;

import com.legendshop.model.dto.constant.MarketingTypeEnum;


/**
 * The Class MarketingRuleDto.
 *
 * @author Tony
 */
public class AppMarketingRuleDto implements Serializable {

	private static final long serialVersionUID = 2497952035024149729L;

	/** 营销活动编号 */
	private Long marketId; 
	
	/** 店铺编号 */
	private Long shopId;
	
	/** The rule id. */
	private Long ruleId;
	
	/** The market rule nm. */
	private String marketRuleNm; //规则描述
	
	/** The off amount. */
	private Double offAmount; //满减优惠
	
	/** The full price. */
	private Double fullPrice; //购买金额满多少金额开始触发规则
	
	private Float offDiscount;
	
    /** The cut amount. */
    private Double cutAmount; //直降金额,对应的type为直降促销
    
    /** The type. */
    private Integer type;
	
	/**
	 * Gets the rule id.
	 *
	 * @return the rule id
	 */
	public Long getRuleId() {
		return ruleId;
	}

	/**
	 * Sets the rule id.
	 *
	 * @param ruleId the new rule id
	 */
	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}

	/**
	 * Gets the off amount.
	 *
	 * @return the off amount
	 */
	public Double getOffAmount() {
		return offAmount;
	}

	/**
	 * Sets the off amount.
	 *
	 * @param offAmount the new off amount
	 */
	public void setOffAmount(Double offAmount) {
		this.offAmount = offAmount;
	}

	/**
	 * Gets the full price.
	 *
	 * @return the full price
	 */
	public Double getFullPrice() {
		return fullPrice;
	}

	/**
	 * Sets the full price.
	 *
	 * @param fullPrice the new full price
	 */
	public void setFullPrice(Double fullPrice) {
		this.fullPrice = fullPrice;
	}

	/**
	 * Gets the cut amount.
	 *
	 * @return the cut amount
	 */
	public Double getCutAmount() {
		return cutAmount;
	}

	/**
	 * Sets the cut amount.
	 *
	 * @param cutAmount the new cut amount
	 */
	public void setCutAmount(Double cutAmount) {
		this.cutAmount = cutAmount;
	}

	/**
	 * Gets the market rule nm.
	 *
	 * @return the market rule nm
	 */
	public String getMarketRuleNm() {
		StringBuilder sb=new StringBuilder();
		if(MarketingTypeEnum.MAN_JIAN.value().equals(type)){
			sb.append(" 满").append(fullPrice).append(" 减").append(offAmount).append("元");
		}else if(MarketingTypeEnum.MAN_ZE.value().equals(type)){
			sb.append(" 满").append(fullPrice).append(" 打").append(offDiscount).append("折");
		}
		else if(MarketingTypeEnum.XIANSHI_ZE.value().equals(type)){
			sb.append("限时折扣");
		}
		return sb.toString();
	}

	/**
	 * Sets the market rule nm.
	 *
	 * @param marketRuleNm the new market rule nm
	 */
	public void setMarketRuleNm(String marketRuleNm) {
		this.marketRuleNm = marketRuleNm;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	public Long getMarketId() {
		return marketId;
	}

	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Float getOffDiscount() {
		return offDiscount;
	}

	public void setOffDiscount(Float offDiscount) {
		this.offDiscount = offDiscount;
	}


	

	
}
