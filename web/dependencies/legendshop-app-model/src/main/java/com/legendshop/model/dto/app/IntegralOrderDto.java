package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class IntegralOrderDto  implements Serializable {
	private long id; 
	private Long orderId;
	
	private String orderSn;
	
    private String userName;
	
	private String userId;
	
	private Integer productNums;
	
	private Integer integralTotal;
	
	private Long dvyTypeId;
	
	private String  dvyFlowId;
	
	private Integer orderStatus;
	
	private Long addrOrderId;
	
	private Date addTime;
	
	private Date dvyTime;
	
	private Date finallyTime;
	
	private String orderDesc;
	
	private List<IntegralOrderItemDto> orderItemDtos;
	
	private IntegralOrderItemDto orderItemDto;
	/**
	 * 用户的订单地址
	 */
	private AddressDto usrAddrSubDto;
	
	private DeliveryDto  delivery;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public String getOrderSn() {
		return orderSn;
	}
	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getProductNums() {
		return productNums;
	}
	public void setProductNums(Integer productNums) {
		this.productNums = productNums;
	}
	public Integer getIntegralTotal() {
		return integralTotal;
	}
	public void setIntegralTotal(Integer integralTotal) {
		this.integralTotal = integralTotal;
	}
	public Long getDvyTypeId() {
		return dvyTypeId;
	}
	public void setDvyTypeId(Long dvyTypeId) {
		this.dvyTypeId = dvyTypeId;
	}
	public String getDvyFlowId() {
		return dvyFlowId;
	}
	public void setDvyFlowId(String dvyFlowId) {
		this.dvyFlowId = dvyFlowId;
	}
	public Integer getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}
	public Long getAddrOrderId() {
		return addrOrderId;
	}
	public void setAddrOrderId(Long addrOrderId) {
		this.addrOrderId = addrOrderId;
	}
	public Date getAddTime() {
		return addTime;
	}
	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	public Date getDvyTime() {
		return dvyTime;
	}
	public void setDvyTime(Date dvyTime) {
		this.dvyTime = dvyTime;
	}
	public Date getFinallyTime() {
		return finallyTime;
	}
	public void setFinallyTime(Date finallyTime) {
		this.finallyTime = finallyTime;
	}
	public String getOrderDesc() {
		return orderDesc;
	}
	public void setOrderDesc(String orderDesc) {
		this.orderDesc = orderDesc;
	}
	public List<IntegralOrderItemDto> getOrderItemDtos() {
		return orderItemDtos;
	}
	public void setOrderItemDtos(List<IntegralOrderItemDto> orderItemDtos) {
		this.orderItemDtos = orderItemDtos;
	}
	public IntegralOrderItemDto getOrderItemDto() {
		return orderItemDto;
	}
	public void setOrderItemDto(IntegralOrderItemDto orderItemDto) {
		this.orderItemDto = orderItemDto;
	}
	public AddressDto getUsrAddrSubDto() {
		return usrAddrSubDto;
	}
	public void setUsrAddrSubDto(AddressDto usrAddrSubDto) {
		this.usrAddrSubDto = usrAddrSubDto;
	}
	public DeliveryDto getDelivery() {
		return delivery;
	}
	public void setDelivery(DeliveryDto delivery) {
		this.delivery = delivery;
	}


}
