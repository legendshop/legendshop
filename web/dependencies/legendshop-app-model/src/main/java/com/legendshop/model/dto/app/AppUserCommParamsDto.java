package com.legendshop.model.dto.app;

import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用户评论参数 
 */
@ApiModel("用户评论参数")
public class AppUserCommParamsDto{
	
	/** 商品ID */
	@ApiModelProperty(value = "商品ID")
	private Long prodId;
	
	/** 评论内容 */
	@ApiModelProperty(value = "评论内容 ")
	private String content;
	
	/** 宝贝评论得分 */
	@ApiModelProperty(value = "宝贝评论得分")
	private Integer prodScore;
	
	/** 卖家服务得分 */
	@ApiModelProperty(value = "卖家服务得分")
	private Integer shopScore;
	
	/** 物流服务得分 */
	@ApiModelProperty(value = "物流服务得分")
	private Integer logisticsScore;
	
	/**  订单项id */
	@ApiModelProperty(value = "订单项id")
	private Long subItemId;
	
	/** 是否匿名 */
	@ApiModelProperty(value = "是否匿名")
	private Boolean isAnonymous;
	
	/** 评论图片 格式[{"fileName": "file1", "base64Code": "dkjfghfj"}, {"fileName": "file2", "base64Code": "dkjfghfj"}]*/
	@ApiModelProperty(value = "评论图片 格式[{'fileName': 'file1', 'base64Code': 'dkjfghfj'}, {'fileName': 'file2', 'base64Code': 'dkjfghfj'}]")
	private String photos;
	
	/** 评论图片 格式[{"fileName": "file1", "base64Code": "dkjfghfj"}, {"fileName": "file2", "base64Code": "dkjfghfj"}]*/
	@ApiModelProperty(value = "评论图片 格式[{'fileName': 'file1', 'base64Code': 'dkjfghfj'}, {'fileName': 'file2', 'base64Code': 'dkjfghfj'}]")
	private MultipartFile[] photos2;
	
	/** vue已经上传图片的路径数据 */
	@ApiModelProperty(value = "vue已经上传图片的路径数据")
	private String[] vuePhotos;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getProdScore() {
		return prodScore;
	}

	public void setProdScore(Integer prodScore) {
		this.prodScore = prodScore;
	}

	public Integer getShopScore() {
		return shopScore;
	}

	public void setShopScore(Integer shopScore) {
		this.shopScore = shopScore;
	}

	public Integer getLogisticsScore() {
		return logisticsScore;
	}

	public void setLogisticsScore(Integer logisticsScore) {
		this.logisticsScore = logisticsScore;
	}

	public Long getSubItemId() {
		return subItemId;
	}

	public void setSubItemId(Long subItemId) {
		this.subItemId = subItemId;
	}

	public Boolean getIsAnonymous() {
		if(null == isAnonymous){
			isAnonymous = false;
		}
		return isAnonymous;
	}

	public void setIsAnonymous(Boolean isAnonymous) {
		this.isAnonymous = isAnonymous;
	}

	public String getPhotos() {
		return photos;
	}

	public void setPhotos(String photos) {
		this.photos = photos;
	}

	public MultipartFile[] getPhotos2() {
		return photos2;
	}

	public void setPhotos2(MultipartFile[] photos2) {
		this.photos2 = photos2;
	}
	
	public String[] getVuePhotos() {
		return vuePhotos;
	}

	public void setVuePhotos(String[] vuePhotos) {
		this.vuePhotos = vuePhotos;
	}
}
