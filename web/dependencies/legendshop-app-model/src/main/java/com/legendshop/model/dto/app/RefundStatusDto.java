package com.legendshop.model.dto.app;

import java.io.Serializable;

/**
 * 退款状态Dto
 * @author Administrator
 *
 */
public class RefundStatusDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long createTime;
	private int freightPrice;
	private long id;
	private long lastTime;
	private String lastUserId;
	private String lastUserName;
	private String logisticsCompany;
	private String logisticsOrderCode;
	private String orderTotalPrice;

	private long repayUserIfoId;
	private String returnDescription;
	private long returnId;
	private long shopId;
	private String shopName;
	private String subNumber;
	private String photoFile3;
	private String photoFile2;
	private String photoFile1;
	
	private int status;
	private String userId;
	private String userName;
	public long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}
	public int getFreightPrice() {
		return freightPrice;
	}
	public void setFreightPrice(int freightPrice) {
		this.freightPrice = freightPrice;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getLastTime() {
		return lastTime;
	}
	public void setLastTime(long lastTime) {
		this.lastTime = lastTime;
	}
	public String getLastUserId() {
		return lastUserId;
	}
	public void setLastUserId(String lastUserId) {
		this.lastUserId = lastUserId;
	}
	public String getLastUserName() {
		return lastUserName;
	}
	public void setLastUserName(String lastUserName) {
		this.lastUserName = lastUserName;
	}
	public String getLogisticsCompany() {
		return logisticsCompany;
	}
	public void setLogisticsCompany(String logisticsCompany) {
		this.logisticsCompany = logisticsCompany;
	}
	public String getLogisticsOrderCode() {
		return logisticsOrderCode;
	}
	public void setLogisticsOrderCode(String logisticsOrderCode) {
		this.logisticsOrderCode = logisticsOrderCode;
	}
	public String getOrderTotalPrice() {
		return orderTotalPrice;
	}
	public void setOrderTotalPrice(String orderTotalPrice) {
		this.orderTotalPrice = orderTotalPrice;
	}
	public long getRepayUserIfoId() {
		return repayUserIfoId;
	}
	public void setRepayUserIfoId(long repayUserIfoId) {
		this.repayUserIfoId = repayUserIfoId;
	}
	public String getReturnDescription() {
		return returnDescription;
	}
	public void setReturnDescription(String returnDescription) {
		this.returnDescription = returnDescription;
	}
	public long getReturnId() {
		return returnId;
	}
	public void setReturnId(long returnId) {
		this.returnId = returnId;
	}
	public long getShopId() {
		return shopId;
	}
	public void setShopId(long shopId) {
		this.shopId = shopId;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getSubNumber() {
		return subNumber;
	}
	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}
	public String getPhotoFile3() {
		return photoFile3;
	}
	public void setPhotoFile3(String photoFile3) {
		this.photoFile3 = photoFile3;
	}
	public String getPhotoFile2() {
		return photoFile2;
	}
	public void setPhotoFile2(String photoFile2) {
		this.photoFile2 = photoFile2;
	}
	public String getPhotoFile1() {
		return photoFile1;
	}
	public void setPhotoFile1(String photoFile1) {
		this.photoFile1 = photoFile1;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}


}
