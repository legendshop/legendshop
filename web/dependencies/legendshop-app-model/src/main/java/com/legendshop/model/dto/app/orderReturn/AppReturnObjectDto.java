package com.legendshop.model.dto.app.orderReturn;

import java.io.Serializable;

/**
 * @author quanzc
 * 去往退货页面相关数据DTO
 */
public class AppReturnObjectDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 171884632200664164L;
	
	/** 产品SkuID */
	private Long skuId; 
	
	/** 产品ID */
	private Long prodId; 
	
	/** 订单项ID */
	private Long subItemId; 
	
	/** 订单sub_number */
	private String subNumber; 
	
	/** 商品总金额 */
	private Double productTotalAmout; 
	
	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getSubItemId() {
		return subItemId;
	}

	public void setSubItemId(Long subItemId) {
		this.subItemId = subItemId;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public Double getProductTotalAmout() {
		return productTotalAmout;
	}

	public void setProductTotalAmout(Double productTotalAmout) {
		this.productTotalAmout = productTotalAmout;
	}

}
