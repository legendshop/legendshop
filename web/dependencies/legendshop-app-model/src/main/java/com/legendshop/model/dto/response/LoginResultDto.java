package com.legendshop.model.dto.response;

import com.legendshop.model.dto.app.AppTokenDto;
import com.legendshop.model.dto.app.ResultDto;

import io.swagger.annotations.ApiModel;

@ApiModel(value="登录接口")
public class LoginResultDto extends ResultDto{

	private static final long serialVersionUID = 4804051436311398640L;
	
	private AppTokenDto result;

	public AppTokenDto getResult() {
		return result;
	}

	public void setResult(AppTokenDto result) {
		this.result = result;
	}

}
