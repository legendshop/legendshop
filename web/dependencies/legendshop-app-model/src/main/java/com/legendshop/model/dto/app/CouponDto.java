package com.legendshop.model.dto.app;

import java.io.Serializable;

/**
 * 优惠券
 * @author Administrator
 *
 */
public class CouponDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public String bindCouponNumber;
	public String couponId;
	public String couponName;
	public String couponNumPrefix;
	public String couponNumber;
	
	public String couponPrice;
	public String couponSn;//兑换码
	public String couponType;//优惠券类型 1.单店满减;2.全场通用
	public String couponUsrId;
	public long createDate;
   
	public String description;
	public String frequency;
	public String fullPrice;//满减
	public String getLimit;
	public long endDate;
 
	public String id;
	public String messageType;
	public String offPrice;//优惠额度
	public String sendCouponNumber;
	public String sendType;
 
	public String shopId;
	public String shopName;
	public int status;
	public String surplusTime;
	public long startDate;
 
	public String useCouponNumber;
	public long timeStatus;
	public long useTime;
	
	public String getBindCouponNumber() {
		return bindCouponNumber;
	}
	public void setBindCouponNumber(String bindCouponNumber) {
		this.bindCouponNumber = bindCouponNumber;
	}
	public String getCouponId() {
		return couponId;
	}
	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}
	public String getCouponName() {
		return couponName;
	}
	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}
	public String getCouponNumPrefix() {
		return couponNumPrefix;
	}
	public void setCouponNumPrefix(String couponNumPrefix) {
		this.couponNumPrefix = couponNumPrefix;
	}
	public String getCouponNumber() {
		return couponNumber;
	}
	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}
	public String getCouponPrice() {
		return couponPrice;
	}
	public void setCouponPrice(String couponPrice) {
		this.couponPrice = couponPrice;
	}
	public String getCouponSn() {
		return couponSn;
	}
	public void setCouponSn(String couponSn) {
		this.couponSn = couponSn;
	}
	public String getCouponType() {
		return couponType;
	}
	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}
	public String getCouponUsrId() {
		return couponUsrId;
	}
	public void setCouponUsrId(String couponUsrId) {
		this.couponUsrId = couponUsrId;
	}
	public long getCreateDate() {
		return createDate;
	}
	public void setCreateDate(long createDate) {
		this.createDate = createDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getFullPrice() {
		return fullPrice;
	}
	public void setFullPrice(String fullPrice) {
		this.fullPrice = fullPrice;
	}
	public String getGetLimit() {
		return getLimit;
	}
	public void setGetLimit(String getLimit) {
		this.getLimit = getLimit;
	}
	public long getEndDate() {
		return endDate;
	}
	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getOffPrice() {
		return offPrice;
	}
	public void setOffPrice(String offPrice) {
		this.offPrice = offPrice;
	}
	public String getSendCouponNumber() {
		return sendCouponNumber;
	}
	public void setSendCouponNumber(String sendCouponNumber) {
		this.sendCouponNumber = sendCouponNumber;
	}
	public String getSendType() {
		return sendType;
	}
	public void setSendType(String sendType) {
		this.sendType = sendType;
	}
	public String getShopId() {
		return shopId;
	}
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getSurplusTime() {
		return surplusTime;
	}
	public void setSurplusTime(String surplusTime) {
		this.surplusTime = surplusTime;
	}
	public long getStartDate() {
		return startDate;
	}
	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}
	public String getUseCouponNumber() {
		return useCouponNumber;
	}
	public void setUseCouponNumber(String useCouponNumber) {
		this.useCouponNumber = useCouponNumber;
	}
	public long getTimeStatus() {
		return timeStatus;
	}
	public void setTimeStatus(long timeStatus) {
		this.timeStatus = timeStatus;
	}
	public long getUseTime() {
		return useTime;
	}
	public void setUseTime(long useTime) {
		this.useTime = useTime;
	}
	
	
    

}
