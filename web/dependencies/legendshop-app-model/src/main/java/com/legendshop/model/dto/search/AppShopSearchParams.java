/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.search;

import com.legendshop.model.constant.ShopOrderWayEnum;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * APP店铺搜索参数
 */
@ApiModel("店铺搜索参数")
public class AppShopSearchParams extends AppSearchParams{

	/**
	 * 排序方式:
	 * synthesize: 综合
	 * sales: 月销量
	 * comments: 评论数
	 */
	@ApiModelProperty(value = "排序方式:synthesize: 综合sales: 月销量comments: 评论数")
	private String orderWay;
	
	public String getOrderWay() {
		if(null == orderWay || orderWay.trim().equals("")){
			orderWay = ShopOrderWayEnum.SYNTHESIZE.value();
		}
		return orderWay;
	}

	public void setOrderWay(String orderWay) {
		this.orderWay = orderWay;
	}

}
