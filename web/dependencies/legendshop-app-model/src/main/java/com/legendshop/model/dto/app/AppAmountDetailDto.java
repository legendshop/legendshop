package com.legendshop.model.dto.app;

import java.io.Serializable;


/**
 * @author quanzc
 * 价格明细内容
 */
public class AppAmountDetailDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6890054062157822918L;
	
	private Double promotionPrice; // 促销价格
	/**
	 * 商品原价
	 */
	private Double price;
	/**
	 * 商品现价
	 */
	private Double cash;
	
	
	private Integer num; // 数量
	
	private Double totalAmount; // 总价
	
	private String amountNm; // 名称
	
	private Integer type;


	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getAmountNm() {
		return amountNm;
	}

	public void setAmountNm(String amountNm) {
		this.amountNm = amountNm;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public Double getPromotionPrice() {
		return promotionPrice;
	}

	public void setPromotionPrice(Double promotionPrice) {
		this.promotionPrice = promotionPrice;
	}
	
	
	

}
