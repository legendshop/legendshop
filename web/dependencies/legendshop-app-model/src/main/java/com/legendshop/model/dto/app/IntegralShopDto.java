package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.List;

public class IntegralShopDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
private List<IntegralShopProductDto> resultList;
	
	private Long pageCount;
	
	private Integer currPage;
	
	private Long totalCount;

	public List<IntegralShopProductDto> getResultList() {
		return resultList;
	}

	public void setResultList(List<IntegralShopProductDto> resultList) {
		this.resultList = resultList;
	}

	public Long getPageCount() {
		return pageCount;
	}

	public void setPageCount(Long pageCount) {
		this.pageCount = pageCount;
	}

	public Integer getCurrPage() {
		return currPage;
	}

	public void setCurrPage(Integer currPage) {
		this.currPage = currPage;
	}

	public Long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Long totalCount) {
		this.totalCount = totalCount;
	}

}
