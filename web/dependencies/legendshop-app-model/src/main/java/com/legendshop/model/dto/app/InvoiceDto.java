package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.Date;

import com.legendshop.model.entity.Invoice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value="InvoiceDto发票") 
public class InvoiceDto implements Serializable{
	
	private static final long serialVersionUID = 5425457124793567498L;

	/** 发票id **/
	@ApiModelProperty(value="发票id")  
	private Long id;
	
	/** 发票类型 */
	@ApiModelProperty(value="发票类型，1为普通发票 2:增值税发票")
	private Integer type;
	
	/** 普通发票类型，1为个人，2为单位 */
	@ApiModelProperty(value="普通发票类型，1为个人，2为单位")
	private Integer title;
	
	/** 普通个人：发票抬头信息 普通公司：公司名称 增值税发票：单位名称 **/
	@ApiModelProperty(value="普通个人：发票抬头信息 普通公司：公司名称 增值税发票：单位名称")
	private String company;
	
	/** 发票内容 */
	@ApiModelProperty(value="发票内容,1为不开发票，2为商品明细")
	private Integer content;
	
	/** 用户ID **/
	@ApiModelProperty(value="用户ID")  
	private String userId;
	
	/** 用户名 **/
	@ApiModelProperty(value="用户名")  
	private String userName;
	
	/** 默认使用 **/
	@ApiModelProperty(value="默认使用,1为默认")  
	private Integer commonInvoice;
	
	/** 纳税人号 */
	@ApiModelProperty(value="纳税人号")  
	private String invoiceHumNumber;

	/** 注册地址（增值税发票） */
	@ApiModelProperty(value="注册地址（增值税发票）")
	private String registerAddr;

	/** 注册电话（增值税发票） */
	@ApiModelProperty(value="注册电话（增值税发票）")
	private String registerPhone;

	/** 开户银行（增值税发票） */
	@ApiModelProperty(value="开户银行（增值税发票）")
	private String depositBank;

	/** 开户银行账号（增值税发票） */
	@ApiModelProperty(value="开户银行账号（增值税发票）")
	private String bankAccountNum;



	public InvoiceDto() {
	}

	public InvoiceDto(Invoice invoice) {
		super();
		this.id = invoice.getId();
		this.type = invoice.getType();
		this.title = invoice.getTitle();
		this.company = invoice.getCompany();
		this.content = invoice.getContent();
		this.userId = invoice.getUserId();
		this.userName = invoice.getUserName();
		this.commonInvoice = invoice.getCommonInvoice();
		this.invoiceHumNumber = invoice.getInvoiceHumNumber();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getTitle() {
		return title;
	}

	public void setTitle(Integer title) {
		this.title = title;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Integer getContent() {
		return content;
	}

	public void setContent(Integer content) {
		this.content = content;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getCommonInvoice() {
		return commonInvoice;
	}

	public void setCommonInvoice(Integer commonInvoice) {
		this.commonInvoice = commonInvoice;
	}

	public String getInvoiceHumNumber() {
		return invoiceHumNumber;
	}

	public void setInvoiceHumNumber(String invoiceHumNumber) {
		this.invoiceHumNumber = invoiceHumNumber;
	}

	public String getRegisterAddr() {
		return registerAddr;
	}

	public void setRegisterAddr(String registerAddr) {
		this.registerAddr = registerAddr;
	}

	public String getRegisterPhone() {
		return registerPhone;
	}

	public void setRegisterPhone(String registerPhone) {
		this.registerPhone = registerPhone;
	}

	public String getDepositBank() {
		return depositBank;
	}

	public void setDepositBank(String depositBank) {
		this.depositBank = depositBank;
	}

	public String getBankAccountNum() {
		return bankAccountNum;
	}

	public void setBankAccountNum(String bankAccountNum) {
		this.bankAccountNum = bankAccountNum;
	}
}
