package com.legendshop.model.dto.app ;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 *第三方登录回调通行证，记录会员与其他社区的帐户关联关系
 */
@ApiModel("第三方登录回调通行证")
public class AppPassportDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4301687660413655062L;

	/** 昵称，QQ或微博名字 */
	@ApiModelProperty(value = "昵称，QQ或微博名字")
	private String nickName; 
	
	/** 类型: [ qq , weibo , weixin  ]**/
	@ApiModelProperty(value = "类型: [ qq , weibo , weixin  ]")
	private String callbackType;
		
	/** openId */
	@ApiModelProperty(value = "openId")
	private String openId; 
	
	/** token信息 **/
	@ApiModelProperty(value = "token信息")
	private String accessToken;
		
	/** 属性 */
	@ApiModelProperty(value = "属性")
	private String props;
	
	/** 设备唯一号 **/
	@ApiModelProperty(value = "设备唯一号 ")
	private String deviceId;
	
	/**设备平台类型, H5: 手机网页, ANDROID: 安卓APP, IOS: 苹果APP, MP: 小程序"**/
	@ApiModelProperty(value = "设备平台类型, H5: 手机网页, ANDROID: 安卓APP, IOS: 苹果APP, MP: 小程序")
	private String platform;
	
	/**版本**/
	@ApiModelProperty(value = "版本")
	private String verId;
	
	/**访问注册用户IP**/
	@ApiModelProperty(value = "访问注册用户IP")
	private String regIP;
	
	/**性别**/
	@ApiModelProperty(value = "性别")
	private String sex;
	
	/**头像照片地址**/
	@ApiModelProperty(value = "头像照片地址")
	private String headimgurl;
	
	public String getRegIP() {
		return regIP;
	}

	public void setRegIP(String regIP) {
		this.regIP = regIP;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getVerId() {
		return verId;
	}

	public void setVerId(String verId) {
		this.verId = verId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getCallbackType() {
		return callbackType;
	}

	public void setCallbackType(String callbackType) {
		this.callbackType = callbackType;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getProps() {
		return props;
	}

	public void setProps(String props) {
		this.props = props;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getHeadimgurl() {
		return headimgurl;
	}

	public void setHeadimgurl(String headimgurl) {
		this.headimgurl = headimgurl;
	} 
		
	
		
	

} 
