package com.legendshop.model.dto.app;

import java.io.Serializable;

/**退换货商品详情**/
public class RefundDetailDto implements Serializable {
	private static final long serialVersionUID = 1L;
	private String applyName;
	private String bankAccount;
	private String bankCode;
	private String bankInfo;
	private String bankUserName;
	private String createTime;
	private String logisticsCompany;
	private String logisticsOrderCode;
	private String orderTotalPrice;
	private String photoFile1;
	private String photoFile2;
	private String photoFile3;
	private String returnDescription;
	private int  status;
	public String getApplyName() {
		return applyName;
	}
	public void setApplyName(String applyName) {
		this.applyName = applyName;
	}
	public String getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getBankInfo() {
		return bankInfo;
	}
	public void setBankInfo(String bankInfo) {
		this.bankInfo = bankInfo;
	}
	public String getBankUserName() {
		return bankUserName;
	}
	public void setBankUserName(String bankUserName) {
		this.bankUserName = bankUserName;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getLogisticsCompany() {
		return logisticsCompany;
	}
	public void setLogisticsCompany(String logisticsCompany) {
		this.logisticsCompany = logisticsCompany;
	}
	public String getLogisticsOrderCode() {
		return logisticsOrderCode;
	}
	public void setLogisticsOrderCode(String logisticsOrderCode) {
		this.logisticsOrderCode = logisticsOrderCode;
	}
	public String getOrderTotalPrice() {
		return orderTotalPrice;
	}
	public void setOrderTotalPrice(String orderTotalPrice) {
		this.orderTotalPrice = orderTotalPrice;
	}
	public String getPhotoFile1() {
		return photoFile1;
	}
	public void setPhotoFile1(String photoFile1) {
		this.photoFile1 = photoFile1;
	}
	public String getPhotoFile2() {
		return photoFile2;
	}
	public void setPhotoFile2(String photoFile2) {
		this.photoFile2 = photoFile2;
	}
	public String getPhotoFile3() {
		return photoFile3;
	}
	public void setPhotoFile3(String photoFile3) {
		this.photoFile3 = photoFile3;
	}
	public String getReturnDescription() {
		return returnDescription;
	}
	public void setReturnDescription(String returnDescription) {
		this.returnDescription = returnDescription;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

 
}
