package com.legendshop.model.dto.app.shopcart;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 购物车商品详情Dto
 * @author Tony
 *
 */
@ApiModel("购物车商品详情Dto")
public class ShopCartItemDto implements Serializable{

	private static final long serialVersionUID = -4634013345653223942L;

	/** 购物车ID */
	@ApiModelProperty(value = "购物车ID")
    private Long basketId;
    
    /**选中状态 */
	@ApiModelProperty(value = "选中状态")
    private int checkSts;

    /** 商品ID */
	@ApiModelProperty(value = "商品ID")
    private Long prodId;
    
    /** 店铺ID  */
	@ApiModelProperty(value = "店铺ID")
    private Long shopId;
	
    /** skuId */
	@ApiModelProperty(value = "skuId")
	private Long skuId;
    
	/** 商品原价*/
	@ApiModelProperty(value = "商品原价")
	private Double price=0d;
	
	/** 促销价格  */
	@ApiModelProperty(value = "促销价格")
	private Double promotionPrice; 
    
	/** sku图片  */
	@ApiModelProperty(value = " sku图片  ")
    private String pic;

    /** 购物车商品数量  */
	@ApiModelProperty(value = "购物车商品数量")
    private Integer basketCount=0;

    /** 商品名称  */
	@ApiModelProperty(value = "商品名称 ")
    private String prodName;
    
    /** sku名称  */
	@ApiModelProperty(value = "sku名称")
    private String skuName;
    
	 /** 商品状态 */
	@ApiModelProperty(value = "商品状态 ")
    private Integer status;

    /** 库存*/
	@ApiModelProperty(value = "库存")
    private Integer stocks=0;
    
    /** 实际库存*/
	@ApiModelProperty(value = "实际库存")
	protected Integer actualStocks=0;
	
    /** 物流重量(千克)  */
	@ApiModelProperty(value = "物流重量(千克)")
    private Double weight=0d;
    
    /** 物流体积(立方米)  */
	@ApiModelProperty(value = "物流体积(立方米)")
    private Double volume=0d;
	
	 /** 货到付款; 0:普通商品 , 1:货到付款商品   */
	@ApiModelProperty(value = "货到付款; 0:普通商品 , 1:货到付款商品")
	private Boolean isSupportCod;
	
	 /** 是否参加团购    */
	@ApiModelProperty(value = "是否参加团购")
	private int isGroup;
	
    /** 销售属性组合（中文）  */
	@ApiModelProperty(value = "销售属性组合（中文）")
    private String cnProperties;
    
    /** 属性Id组合 */
	@ApiModelProperty(value = "属性Id组合")
    private String properties;
    
	/** 商品总费用  商品总价格   单价*数量 */
	@ApiModelProperty(value = "商品总费用  商品总价格   单价*数量")
	private Double total;
	
    /** 商品实际总费用  商品总价格  + 运费  - 促销费用*/
	@ApiModelProperty(value = "商品实际总费用  商品总价格  + 运费  - 促销费用")
	private Double totalMoeny;
	
	/** 总重量 */
	@ApiModelProperty(value = "总重量")
	private Double totalWeight=0d;
	
	/** 总大小 */
	@ApiModelProperty(value = "总大小")
	private Double totalVolume=0d;
	
	/** 是否支持门店购买  */
	@ApiModelProperty(value = "是否支持门店购买")
	private boolean supportStore=false; 
	
    /** 是否失效 */
	@ApiModelProperty(value = "是否失效 ")
    private Boolean isFailure; 
	
	/** -------------------------营销活动信息----------------------  */
	   
	/** 优惠价价格 */
	@ApiModelProperty(value = "优惠价价格")
	private Double discountPrice=0d; 
	
	/** 折扣 */
	@ApiModelProperty(value = "折扣")
	private Double discount=0d;

	/** 所选门店是否存在该商品（false为不存在 ）  用于门店自提 */
	@ApiModelProperty(value = "所选门店是否存在该商品（false为不存在 ）")
	private Boolean storeIsExist;

	/** 门店ID */
	@ApiModelProperty(value = "门店ID")
	private Long storeId;

	/** 门店名称  */
	@ApiModelProperty(value = "门店名称")
	private String storeName;

	/** 门店详细地址  */
	@ApiModelProperty(value = "门店详细地址")
	private String storeAddr;
	
	public Long getBasketId() {
		return basketId;
	}

	public void setBasketId(Long basketId) {
		this.basketId = basketId;
	}

	public int getCheckSts() {
		return checkSts;
	}

	public void setCheckSts(int checkSts) {
		this.checkSts = checkSts;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getPromotionPrice() {
		return promotionPrice;
	}

	public void setPromotionPrice(Double promotionPrice) {
		this.promotionPrice = promotionPrice;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Integer getBasketCount() {
		return basketCount;
	}

	public void setBasketCount(Integer basketCount) {
		this.basketCount = basketCount;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getStocks() {
		return stocks;
	}

	public void setStocks(Integer stocks) {
		this.stocks = stocks;
	}

	public Integer getActualStocks() {
		return actualStocks;
	}

	public void setActualStocks(Integer actualStocks) {
		this.actualStocks = actualStocks;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Boolean getIsSupportCod() {
		return isSupportCod;
	}

	public void setIsSupportCod(Boolean isSupportCod) {
		this.isSupportCod = isSupportCod;
	}

	public int getIsGroup() {
		return isGroup;
	}

	public void setIsGroup(int isGroup) {
		this.isGroup = isGroup;
	}

	public String getCnProperties() {
		return cnProperties;
	}

	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getTotalMoeny() {
		return totalMoeny;
	}

	public void setTotalMoeny(Double totalMoeny) {
		this.totalMoeny = totalMoeny;
	}

	public Double getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(Double totalWeight) {
		this.totalWeight = totalWeight;
	}

	public Double getTotalVolume() {
		return totalVolume;
	}

	public void setTotalVolume(Double totalVolume) {
		this.totalVolume = totalVolume;
	}

	public Double getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public boolean isSupportStore() {
		return supportStore;
	}

	public void setSupportStore(boolean supportStore) {
		this.supportStore = supportStore;
	}

	public Boolean getIsFailure() {
		return isFailure;
	}

	public void setIsFailure(Boolean isFailure) {
		this.isFailure = isFailure;
	}

	public Boolean getStoreIsExist() {
		return storeIsExist;
	}

	public void setStoreIsExist(Boolean storeIsExist) {
		this.storeIsExist = storeIsExist;
	}
	
	/** -------------------------营销活动信息----------------------  */


	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStoreAddr() {
		return storeAddr;
	}

	public void setStoreAddr(String storeAddr) {
		this.storeAddr = storeAddr;
	}
}
