/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 订单各个状态的数量统计Dto
 */
@ApiModel("订单统计Dto")
public class AppSubCountsDto {

	/** 订单状态[ 1:待付款 2：已付款 3：已发货 4：交易成功 5：交易失败] */
	@ApiModelProperty(value = "订单状态 类型 [1:待付款  2：已付款  3：已发货  4：交易成功  5：交易失败] ")
	private Integer status;
	
	/** 订单数. */
	@ApiModelProperty(value = "订单数 [超过100条显示为99+]")
	private Integer subCounts;

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getSubCounts() {
		return subCounts;
	}

	public void setSubCounts(Integer subCounts) {
		this.subCounts = subCounts;
	}

}
