package com.legendshop.model.dto.app;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.util.JSONUtil;


/**
 * 用于 批量改变 购物车项 的选中状态
 * 
 * @author Tony
 * 
 */
public class ChgBasketDto {

	/** 购物车ＩＤ */
	private Long basketId;

	/** 是否选中　1=是   0=否  */
	private int checkSts;

	public Long getBasketId() {
		return basketId;
	}

	public void setBasketId(Long basketId) {
		this.basketId = basketId;
	}

	public int getCheckSts() {
		return checkSts;
	}

	public void setCheckSts(int checkSts) {
		this.checkSts = checkSts;
	}
	
	public static void main(String[] args) {
		List<ChgBasketDto> list=new ArrayList<ChgBasketDto>();
		ChgBasketDto dto=new ChgBasketDto();
		dto.setBasketId(1005L);
		dto.setCheckSts(1);
		
		ChgBasketDto dto2=new ChgBasketDto();
		dto2.setBasketId(1005L);
		dto2.setCheckSts(1);
		
		list.add(dto);
		list.add(dto2);
		
		System.out.println(JSONUtil.getJson(list));
		
	}

}
