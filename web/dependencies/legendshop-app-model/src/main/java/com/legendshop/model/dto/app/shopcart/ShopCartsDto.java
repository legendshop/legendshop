package com.legendshop.model.dto.app.shopcart;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 购物车详情Dto
 */
@ApiModel("购物车详情Dto")
public class ShopCartsDto implements Serializable {

	private static final long serialVersionUID = 7608309839997831622L;

	/** 商家ID */
	@ApiModelProperty(value = "商家ID")
	private Long shopId;

	/** 店铺名称 */
	@ApiModelProperty(value = "店铺名称")
	private String shopName;
	
	/** 店铺总共有多少个优惠券 */
	@ApiModelProperty(value = "店铺总共有多少个优惠券",name = "shopCouponCount")
	private Long ShopCouponCount;

	/** 商家状态 */
	@ApiModelProperty(value = "商家状态")
	private int shopStatus = 1;

	/** 总价 */
	@ApiModelProperty(value = "总价")
	private Double shopTotalCash = 0d;

	/** 店铺实际支付总额 */
	@ApiModelProperty(value = "店铺实际支付总额 ")
	private Double shopActualTotalCash = 0d;

	/** 商品总重量 */
	@ApiModelProperty(value = "商品总重量")
	private Double shopTotalWeight;

	/** 商品总体积 */
	@ApiModelProperty(value = "商品总体积")
	private Double shopTotalVolume;

	/** 商品总数量 */
	@ApiModelProperty(value = "商品总数量")
	private int totalcount;

	/** 折扣价 */
	@ApiModelProperty(value = "折扣价")
	private Double discountPrice = 0d;
	
    /** 包邮信息*/
	@ApiModelProperty(value = "包邮信息")
    private String mailfeeStr;
    
    /** 商品列表  [满减: shopCartItemA,shopCartItemB,none:shopCartItemC,满折:xx,xxb]   **/
	@ApiModelProperty(value = "商品列表  [满减: shopCartItemA,shopCartItemB,none:shopCartItemC,满折:xx,xxb]")
    private List<ShopGroupCartItemDto> groupCartItems; 
	
	 /**
     * 是否支持门店
     */
	@ApiModelProperty(value = "是否支持门店 true为支持 false为支持")
    private boolean supportStore=false;

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public int getShopStatus() {
		return shopStatus;
	}

	public void setShopStatus(int shopStatus) {
		this.shopStatus = shopStatus;
	}

	public Double getShopTotalCash() {
		return shopTotalCash;
	}

	public void setShopTotalCash(Double shopTotalCash) {
		this.shopTotalCash = shopTotalCash;
	}

	public Double getShopActualTotalCash() {
		return shopActualTotalCash;
	}

	public void setShopActualTotalCash(Double shopActualTotalCash) {
		this.shopActualTotalCash = shopActualTotalCash;
	}

	public Double getShopTotalWeight() {
		return shopTotalWeight;
	}

	public void setShopTotalWeight(Double shopTotalWeight) {
		this.shopTotalWeight = shopTotalWeight;
	}

	public Double getShopTotalVolume() {
		return shopTotalVolume;
	}

	public void setShopTotalVolume(Double shopTotalVolume) {
		this.shopTotalVolume = shopTotalVolume;
	}

	public int getTotalcount() {
		return totalcount;
	}

	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}

	public Double getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}

	public String getMailfeeStr() {
		return mailfeeStr;
	}

	public void setMailfeeStr(String mailfeeStr) {
		this.mailfeeStr = mailfeeStr;
	}

	public List<ShopGroupCartItemDto> getGroupCartItems() {
		return groupCartItems;
	}

	public void setGroupCartItems(List<ShopGroupCartItemDto> groupCartItems) {
		this.groupCartItems = groupCartItems;
	}

	public Long getShopCouponCount() {
		return ShopCouponCount;
	}

	public void setShopCouponCount(Long shopCouponCount) {
		this.ShopCouponCount = shopCouponCount;
	}

	public boolean isSupportStore() {
		return supportStore;
	}

	public void setSupportStore(boolean supportStore) {
		this.supportStore = supportStore;
	}
	
	
}
