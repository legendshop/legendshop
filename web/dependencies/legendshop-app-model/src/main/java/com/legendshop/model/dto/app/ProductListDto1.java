package com.legendshop.model.dto.app;

import java.io.Serializable;


public class ProductListDto1 implements Serializable{

	private static final long serialVersionUID = 6000534916754164142L;

	/** 商品Id */
	private Long prodId;
	
	/** 商品图片 */
	private String pic;
	
	/** 商品名称 */
	private String name;
	
	/** 产品原价，市场价. */
	private Double price;
	
	/** 产品现价，客户最终价格. */
	private Double cash;
	
	/** 观看人数 */
	private Integer views;
	
	/** 评论总得分 **/
	private Integer reviewScores;
	
	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Integer getViews() {
		return views;
	}

	public void setViews(Integer views) {
		this.views = views;
	}

	public Integer getReviewScores() {
		return reviewScores;
	}

	public void setReviewScores(Integer reviewScores) {
		this.reviewScores = reviewScores;
	}
	
	
}
