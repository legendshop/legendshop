package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.List;

/**
 * 帮助中心
 * @author Administrator
 *
 */
public class HelpCenterDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String file;
	private int id;
	private long newsCategoryDate;
	private int newsCategoryId;
	private String newsCategoryName;
	private String pic;
	private String seq;
	private int shopId;
	private int status;
	private List<NewsDetailDto>subNews;
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public long getNewsCategoryDate() {
		return newsCategoryDate;
	}
	public void setNewsCategoryDate(long newsCategoryDate) {
		this.newsCategoryDate = newsCategoryDate;
	}
	public int getNewsCategoryId() {
		return newsCategoryId;
	}
	public void setNewsCategoryId(int newsCategoryId) {
		this.newsCategoryId = newsCategoryId;
	}
	public String getNewsCategoryName() {
		return newsCategoryName;
	}
	public void setNewsCategoryName(String newsCategoryName) {
		this.newsCategoryName = newsCategoryName;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String pic) {
		this.pic = pic;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public List<NewsDetailDto> getSubNews() {
		return subNews;
	}
	public void setSubNews(List<NewsDetailDto> subNews) {
		this.subNews = subNews;
	}
    
}
