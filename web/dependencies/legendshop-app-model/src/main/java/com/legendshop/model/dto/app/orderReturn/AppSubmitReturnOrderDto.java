package com.legendshop.model.dto.app.orderReturn;

import java.io.Serializable;


/**
 *  申请退换货提交表单dto
 */
public class AppSubmitReturnOrderDto implements Serializable{
	private static final long serialVersionUID = 9050681229231201931L;
	private String subNum;  //订单号
	private Long subItemId; //子订单Id
	private Long returnBankId;  //退款信息ID
	private String returnDesc;   //退换货描述
	private Double orderTotalPrice;//退款金额
	
	private String fileName1;  //凭证1名称
	private String photo1;  //凭证1 base64 编码
	private String fileName2;  //凭证2名称
	private String photo2; //凭证2 base64 编码
	private String fileName3; //凭证3名称
	private String photo3; //凭证3 base64 编码
	public String getSubNum() {
		return subNum;
	}
	public void setSubNum(String subNum) {
		this.subNum = subNum;
	}
	public Long getSubItemId() {
		return subItemId;
	}
	public void setSubItemId(Long subItemId) {
		this.subItemId = subItemId;
	}

	public Long getReturnBankId() {
		return returnBankId;
	}
	public void setReturnBankId(Long returnBankId) {
		this.returnBankId = returnBankId;
	}
	public String getReturnDesc() {
		return returnDesc;
	}
	public void setReturnDesc(String returnDesc) {
		this.returnDesc = returnDesc;
	}
	public Double getOrderTotalPrice() {
		return orderTotalPrice;
	}
	public void setOrderTotalPrice(Double orderTotalPrice) {
		this.orderTotalPrice = orderTotalPrice;
	}
	public String getFileName1() {
		return fileName1;
	}
	public void setFileName1(String fileName1) {
		this.fileName1 = fileName1;
	}
	public String getPhoto1() {
		return photo1;
	}
	public void setPhoto1(String photo1) {
		this.photo1 = photo1;
	}
	public String getFileName2() {
		return fileName2;
	}
	public void setFileName2(String fileName2) {
		this.fileName2 = fileName2;
	}
	public String getPhoto2() {
		return photo2;
	}
	public void setPhoto2(String photo2) {
		this.photo2 = photo2;
	}
	public String getFileName3() {
		return fileName3;
	}
	public void setFileName3(String fileName3) {
		this.fileName3 = fileName3;
	}
	public String getPhoto3() {
		return photo3;
	}
	public void setPhoto3(String photo3) {
		this.photo3 = photo3;
	}


	
}
