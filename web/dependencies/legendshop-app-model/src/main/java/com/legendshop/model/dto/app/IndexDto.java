/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 首页Dto.
 */
@ApiModel(value="首页Dto") 
public class IndexDto implements Serializable {

	private static final long serialVersionUID = -8236638610187624344L;

	@ApiModelProperty(value="图片List集合")  
	private List<IndexjpgDto> indexjpgDto;

	@ApiModelProperty(value="楼层List集合")  
	private List<MobileFloorDto> mobileFloorList;

	
	
	public List<IndexjpgDto> getIndexjpgDto() {
		return indexjpgDto;
	}

	public void setIndexjpgDto(List<IndexjpgDto> indexjpgDto) {
		this.indexjpgDto = indexjpgDto;
	}

	public List<MobileFloorDto> getMobileFloorList() {
		return mobileFloorList;
	}

	public void setMobileFloorList(List<MobileFloorDto> mobileFloorList) {
		this.mobileFloorList = mobileFloorList;
	}
	
}