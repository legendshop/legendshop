package com.legendshop.model.dto.app;

import java.io.Serializable;

/**
 * 充值
 * @author Administrator
 *
 */
public class ChargeResultDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private String availablePredeposit;
	private String freezePredeposit;
	private String pdCashLogs;
	private ChargeDto recharges;
	public String getAvailablePredeposit() {
		return availablePredeposit;
	}
	public void setAvailablePredeposit(String availablePredeposit) {
		this.availablePredeposit = availablePredeposit;
	}
	public String getFreezePredeposit() {
		return freezePredeposit;
	}
	public void setFreezePredeposit(String freezePredeposit) {
		this.freezePredeposit = freezePredeposit;
	}
	public String getPdCashLogs() {
		return pdCashLogs;
	}
	public void setPdCashLogs(String pdCashLogs) {
		this.pdCashLogs = pdCashLogs;
	}
	public ChargeDto getRecharges() {
		return recharges;
	}
	public void setRecharges(ChargeDto recharges) {
		this.recharges = recharges;
	}
}
