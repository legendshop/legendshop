package com.legendshop.model.dto.app;

import java.io.Serializable;

/**
 * 配送方式
 */
public class FreightDto implements Serializable {

	private static final long serialVersionUID = -7987212612655385395L;

	/** The delivery amount. */
	private Double deliveryAmount;
	
	/** The freight mode. */
	private String freightMode;

	/** The desc. */
	private String desc;

	private boolean isSelected;
	
	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public Double getDeliveryAmount() {
		return deliveryAmount;
	}

	public void setDeliveryAmount(Double deliveryAmount) {
		this.deliveryAmount = deliveryAmount;
	}

	public String getFreightMode() {
		return freightMode;
	}

	public void setFreightMode(String freightMode) {
		this.freightMode = freightMode;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	
}
