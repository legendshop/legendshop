package com.legendshop.model.dto.app;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("文章Dto")
public class NewsDto implements Serializable{

	private static final long serialVersionUID = -934436841539828830L;

	/** 文章ID **/
	@ApiModelProperty(value = "文章ID")
	private Long newsId;
	
	/** 文章名称 **/
	@ApiModelProperty(value = "文章名称")
	private String newsTitle;

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public String getNewsTitle() {
		return newsTitle;
	}

	public void setNewsTitle(String newsTitle) {
		this.newsTitle = newsTitle;
	}
	
	
}
