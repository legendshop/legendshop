package com.legendshop.model.dto.app ;
import java.io.Serializable;

/**
 *每个城市的运费设置
 */
public class TransCityDto implements Serializable{

	private static final long serialVersionUID = -820611429961876423L;

	/** 运费ID */
	private Long transfeeId; 
		
	/**  */
	private Long cityId; 
		
	
	public TransCityDto() {
    }
		
	public Long  getTransfeeId(){
		return transfeeId;
	} 
		
	public void setTransfeeId(Long transfeeId){
			this.transfeeId = transfeeId;
		}
		
	public Long  getCityId(){
		return cityId;
	} 
		
	public void setCityId(Long cityId){
			this.cityId = cityId;
	}
	
} 
