/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.app;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 保存地址.
 */
@ApiModel(value="AddressRequestDto") 
public class AddressRequestDto implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8008194649513962171L;

	/** 地址ID  *. */
	@ApiModelProperty(value="地址Id") 
	private Long addrId;
	
	/** 接收人. */
	@ApiModelProperty(value="接收人") 
	private String receiver;

	/** 详细地址. */
	@ApiModelProperty(value="详细地址") 
	private String subAdds;
	
	/** 省份 id. */
	@ApiModelProperty(value="省份 id") 
	private Integer provinceId;

	/** 城市 id. */
	@ApiModelProperty(value="省份Id") 
	private Integer cityId;

	/** 地区 id. */
	@ApiModelProperty(value="地区Id") 
	private Integer areaId;

	/** 手机号码. */
	@ApiModelProperty(value="手机号码") 
	private String mobile;
	
	/** 是否(默认)常用地址， 0：非默认，1： 默认。 */
	@ApiModelProperty(value="是否(默认)常用地址， 0：非默认，1： 默认。") 
	private String commonAddr;

	/**
	 * Gets the addr id.
	 *
	 * @return the addr id
	 */
	public Long getAddrId() {
		return addrId;
	}

	/**
	 * Sets the addr id.
	 *
	 * @param addrId the new addr id
	 */
	public void setAddrId(Long addrId) {
		this.addrId = addrId;
	}

	/**
	 * Gets the receiver.
	 *
	 * @return the receiver
	 */
	public String getReceiver() {
		return receiver;
	}

	/**
	 * Sets the receiver.
	 *
	 * @param receiver the new receiver
	 */
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	/**
	 * Gets the sub adds.
	 *
	 * @return the sub adds
	 */
	public String getSubAdds() {
		return subAdds;
	}

	/**
	 * Sets the sub adds.
	 *
	 * @param subAdds the new sub adds
	 */
	public void setSubAdds(String subAdds) {
		this.subAdds = subAdds;
	}

	/**
	 * Gets the province id.
	 *
	 * @return the province id
	 */
	public Integer getProvinceId() {
		return provinceId;
	}

	/**
	 * Sets the province id.
	 *
	 * @param provinceId the new province id
	 */
	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	/**
	 * Gets the city id.
	 *
	 * @return the city id
	 */
	public Integer getCityId() {
		return cityId;
	}

	/**
	 * Sets the city id.
	 *
	 * @param cityId the new city id
	 */
	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	/**
	 * Gets the area id.
	 *
	 * @return the area id
	 */
	public Integer getAreaId() {
		return areaId;
	}

	/**
	 * Sets the area id.
	 *
	 * @param areaId the new area id
	 */
	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	/**
	 * Gets the mobile.
	 *
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * Sets the mobile.
	 *
	 * @param mobile the new mobile
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCommonAddr() {
		return commonAddr;
	}

	public void setCommonAddr(String commonAddr) {
		this.commonAddr = commonAddr;
	}
}
