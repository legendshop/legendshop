package com.legendshop.model.dto.app.shopcart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * APP 用户的购物车信息
 * @author Tony
 *
 */
@ApiModel(value="用户购物车列表Dto") 
public class UserShopCartListDto implements Serializable{

	private static final long serialVersionUID = 790433533026959806L;

	@ApiModelProperty(value="用户Id")  
    private  String userId;
	
	@ApiModelProperty(value="用户名")  
	private String userName;
	
	@ApiModelProperty(value="订单类型 [拍卖、普通、秒杀、门店订单]")  
	private String type; 
	
	@ApiModelProperty(value="订单商品总数量")  
    private int orderTotalQuanlity;
    
	@ApiModelProperty(value="订单商品总重量")  
    private double orderTotalWeight;
    
	@ApiModelProperty(value="订单商品总体积")  
    private double orderTotalVolume;
    
	@ApiModelProperty(value="商品总价")  
    private double orderTotalCash; 
    
	@ApiModelProperty(value="实际总值   商品总价+运费-优惠 -优惠券")  
    private double orderActualTotal; 
    
	@ApiModelProperty(value=" 订单优惠总价")  
    private double allDiscount;
	
	@ApiModelProperty(value="购物车的list集合")  
	private List<ShopCartsDto> shopCarts = new ArrayList<ShopCartsDto>();
	
	@ApiModelProperty(value="购物车失效商品的list集合")  
	private List<ShopCartItemDto> invalidCartItems;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getOrderTotalQuanlity() {
		return orderTotalQuanlity;
	}

	public void setOrderTotalQuanlity(int orderTotalQuanlity) {
		this.orderTotalQuanlity = orderTotalQuanlity;
	}

	public double getOrderTotalWeight() {
		return orderTotalWeight;
	}

	public void setOrderTotalWeight(double orderTotalWeight) {
		this.orderTotalWeight = orderTotalWeight;
	}

	public double getOrderTotalVolume() {
		return orderTotalVolume;
	}

	public void setOrderTotalVolume(double orderTotalVolume) {
		this.orderTotalVolume = orderTotalVolume;
	}

	public double getOrderTotalCash() {
		return orderTotalCash;
	}

	public void setOrderTotalCash(double orderTotalCash) {
		this.orderTotalCash = orderTotalCash;
	}

	public double getOrderActualTotal() {
		return orderActualTotal;
	}

	public void setOrderActualTotal(double orderActualTotal) {
		this.orderActualTotal = orderActualTotal;
	}

	public double getAllDiscount() {
		return allDiscount;
	}

	public void setAllDiscount(double allDiscount) {
		this.allDiscount = allDiscount;
	}

	public List<ShopCartsDto> getShopCarts() {
		return shopCarts;
	}

	public void setShopCarts(List<ShopCartsDto> shopCarts) {
		this.shopCarts = shopCarts;
	}

	public List<ShopCartItemDto> getInvalidCartItems() {
		return invalidCartItems;
	}

	public void setInvalidCartItems(List<ShopCartItemDto> invalidCartItems) {
		this.invalidCartItems = invalidCartItems;
	}
	
}
