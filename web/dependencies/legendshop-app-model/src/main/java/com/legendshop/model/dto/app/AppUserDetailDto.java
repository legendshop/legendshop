package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value="用户详情Dto") 
public class AppUserDetailDto implements Serializable {

	private static final long serialVersionUID = 8008194649513962171L;

	
	/** 用户ID */
	@ApiModelProperty(value="用户ID")  
	private String userId;
	
	/** 用户名称 */
	@ApiModelProperty(value="用户名称")  
	private String userName;

	/** 昵称 */
	@ApiModelProperty(value="昵称，加判断如果为空显示'暂无昵称'")  
	private String nickName;
	
	/** 性别  **/
	@ApiModelProperty(value="性别")  
	private String sex;
	
	/** 生日  **/
	@ApiModelProperty(value="生日,加判断如果为空显示'暂未设置'")  
	private Date birthDate;
	
	 /** 头像图片 **/
	@ApiModelProperty(value="头像图片")  
    private String portraitPic;
    
    /**可用余额 **/
	@ApiModelProperty(value="可用余额")  
    private Double availablePredeposit;
    
    /**默认地址 **/
	@ApiModelProperty(value="默认地址")  
    private String commonAddr;
    
    /**用户手机号码*/
	@ApiModelProperty(value="用户手机号码")  
    private String mobile;
    
    /**支付密码**/
	@ApiModelProperty(value="支付密码")  
    private String payPassword;
	
	/**姓名**/
	@ApiModelProperty(value="姓名，加判断如果为空显示'暂无'")  
	private String realName ;

	/** 用户名称 */
	@ApiModelProperty(value="用户签名")
	private String userSignature;

	/** 用户等级 **/
	@ApiModelProperty(value="用户等级")
	private String userGradeName;
    
	public Double getAvailablePredeposit() {
		return availablePredeposit;
	}

	public void setAvailablePredeposit(Double availablePredeposit) {
		this.availablePredeposit = availablePredeposit;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPortraitPic() {
		return portraitPic;
	}

	public void setPortraitPic(String portraitPic) {
		this.portraitPic = portraitPic;
	}

	public String getPayPassword() {
		return payPassword;
	}

	public void setPayPassword(String payPassword) {
		this.payPassword = payPassword;
	}

	public String getCommonAddr() {
		return commonAddr;
	}

	public void setCommonAddr(String commonAddr) {
		this.commonAddr = commonAddr;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getUserSignature() {
		return userSignature;
	}

	public void setUserSignature(String userSignature) {
		this.userSignature = userSignature;
	}

	public String getUserGradeName() {
		return userGradeName;
	}

	public void setUserGradeName(String userGradeName) {
		this.userGradeName = userGradeName;
	}
}
