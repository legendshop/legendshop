package com.legendshop.model.dto.app;

import java.io.Serializable;

/**
 * 退换货商品Dto
 * @author Administrator
 *
 */
public class RefundGoodsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String prodId;
    private String productTotalAmout;
    private String skuId;
    private String subItemId;
    private String subNumber;
	public String getProdId() {
		return prodId;
	}
	public void setProdId(String prodId) {
		this.prodId = prodId;
	}
	public String getProductTotalAmout() {
		return productTotalAmout;
	}
	public void setProductTotalAmout(String productTotalAmout) {
		this.productTotalAmout = productTotalAmout;
	}
	public String getSkuId() {
		return skuId;
	}
	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}
	public String getSubItemId() {
		return subItemId;
	}
	public void setSubItemId(String subItemId) {
		this.subItemId = subItemId;
	}
	public String getSubNumber() {
		return subNumber;
	}
	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}
	
	
}
