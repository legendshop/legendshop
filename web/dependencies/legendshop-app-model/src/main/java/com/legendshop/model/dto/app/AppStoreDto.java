package com.legendshop.model.dto.app;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="AppStoreDto门店") 
public class AppStoreDto implements Serializable{

	private static final long serialVersionUID = 5929517790042228708L;

	/** 门店Id */
	@ApiModelProperty(value="门店Id")  
	private Long storeId;
	
	/** 门店名称 */
	@ApiModelProperty(value="门店名称")  
    private String name;
    
    /** 省份 */
	@ApiModelProperty(value="省份")  
    private String province;
    
    /** 城市 */
	@ApiModelProperty(value="城市")  
    private String city;
    
    /** 地区 */
	@ApiModelProperty(value="地区")  
    private String area;
    
    /** 详细地址 */
	@ApiModelProperty(value="详细地址")  
    private String address;
	
	/** 经度 */
	@ApiModelProperty(value="经度")
	private Double lng;
	
	/** 纬度 */
	@ApiModelProperty(value="纬度")
	private Double lat;
	
	/** 距离  */
	@ApiModelProperty(value="距离")
	private Double distance;
	
	/** 营业时间  */
	@ApiModelProperty(value="营业时间")
	private String businessHours; 
	
	
	
	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public String getBusinessHours() {
		return businessHours;
	}

	public void setBusinessHours(String businessHours) {
		this.businessHours = businessHours;
	}
	
}
