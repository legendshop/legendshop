package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.List;

/**
 * 消费记录
 * @author Administrator
 *
 */
public class PdCashLogsDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private int currPage;
	private int pageCount;
	private int pageSize;
	private List<SpendDetailDto> resultList;
	public int getCurrPage() {
		return currPage;
	}
	public void setCurrPage(int currPage) {
		this.currPage = currPage;
	}
	public int getPageCount() {
		return pageCount;
	}
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public List<SpendDetailDto> getResultList() {
		return resultList;
	}
	public void setResultList(List<SpendDetailDto> resultList) {
		this.resultList = resultList;
	}
	
}
