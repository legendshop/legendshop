/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.model.dto.app.store;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class AppShopIndexDto.
 *
 * @author quanzc
 * 店铺主页
 */
@ApiModel(value="店铺主页Dto") 
public class AppShopIndexDto implements Serializable{

	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3694116696622238554L;
	
	/** 店铺图片. */
	@ApiModelProperty(value="店铺图片")  
	private String shopPic;
	
	/** 店铺名称. */
	@ApiModelProperty(value="店铺名称")  
	private String siteName;
	
	/** 店铺综合评分. */
	@ApiModelProperty(value="店铺综合评分")  
	private String score;

	/** 商家描述评分 */
	@ApiModelProperty(value="商家描述评分")
	private String prodscore;

	/** 商家物流评分 */
	@ApiModelProperty(value="商家物流评分")
	private String logisticsScore;

	/** 商家服务评分 */
	@ApiModelProperty(value="商家服务评分")
	private String shopScore;
	
	/** 是否收藏 0：未    1：收藏. */
	@ApiModelProperty(value="是否收藏 0：未    1：收藏")  
	private Integer isCollection;
	
	/**
	 * Gets the shop pic.
	 *
	 * @return the shop pic
	 */
	public String getShopPic() {
		return shopPic;
	}
	
	/**
	 * Sets the shop pic.
	 *
	 * @param shopPic the shop pic
	 */
	public void setShopPic(String shopPic) {
		this.shopPic = shopPic;
	}
	
	/**
	 * Gets the site name.
	 *
	 * @return the site name
	 */
	public String getSiteName() {
		return siteName;
	}
	
	/**
	 * Sets the site name.
	 *
	 * @param siteName the site name
	 */
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	
	/**
	 * Gets the score.
	 *
	 * @return the score
	 */
	public String getScore() {
		return score;
	}
	
	/**
	 * Sets the score.
	 *
	 * @param score the score
	 */
	public void setScore(String score) {
		this.score = score;
	}
	
	/**
	 * Gets the is collection.
	 *
	 * @return the checks if is collection
	 */
	public Integer getIsCollection() {
		return isCollection;
	}
	
	/**
	 * Sets the is collection.
	 *
	 * @param isCollection the checks if is collection
	 */
	public void setIsCollection(Integer isCollection) {
		this.isCollection = isCollection;
	}

	public String getProdscore() {
		return prodscore;
	}

	public void setProdscore(String prodscore) {
		this.prodscore = prodscore;
	}

	public String getLogisticsScore() {
		return logisticsScore;
	}

	public void setLogisticsScore(String logisticsScore) {
		this.logisticsScore = logisticsScore;
	}

	public String getShopScore() {
		return shopScore;
	}

	public void setShopScore(String shopScore) {
		this.shopScore = shopScore;
	}
}
