package com.legendshop.model.dto.app;

import java.io.Serializable;

public class IntegralSubmitDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private AddressDto defaultAddress;
	private IntegralShopProductDto integralProd;
	public AddressDto getDefaultAddress() {
		return defaultAddress;
	}
	public void setDefaultAddress(AddressDto defaultAddress) {
		this.defaultAddress = defaultAddress;
	}
	public IntegralShopProductDto getIntegralProd() {
		return integralProd;
	}
	public void setIntegralProd(IntegralShopProductDto integralProd) {
		this.integralProd = integralProd;
	}
	
}
