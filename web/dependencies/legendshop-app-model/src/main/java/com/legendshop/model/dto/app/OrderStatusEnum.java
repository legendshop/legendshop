/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.app;



/**
 * 订单状态枚举
 */
public enum OrderStatusEnum implements IntegerEnum {
	/*
	 * WAIT_BUYER_PAY(表示买家已在支付宝交易管理中产生了交易记录，但没有付款);
	 * 
	 * WAIT_SELLER_SEND_GOODS(表示买家已在支付宝交易管理中产生了交易记录且付款成功，但卖家没有发货);
	 * 
	 * WAIT_BUYER_CONFIRM_GOODS(表示卖家已经发了货，但买家还没有做确认收货的操作);
	 * 
	 * TRADE_FINISHED(表示买家已经确认收货，这笔交易完成);
	 */
	// 货到付款(is_code字段)
	/** 没有付款. */
	UNPAY(1),

	/** 已经付款,但卖家没有发货. */
	PADYED(2),

	/** 发货，导致实际库存减少. */
	CONSIGNMENT(3),

	/** 交易成功，购买数增加1. */
	SUCCESS(4),

	/** 交易失败.,还原库存 */
	CLOSE(5);
	

	

	/** The num. */
	private Integer num;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.IntegerEnum#value()
	 */
	public Integer value() {
		return num;
	}

	/**
	 * Instantiates a new order status enum.
	 * 
	 * @param num
	 *            the num
	 */
	OrderStatusEnum(Integer num) {
		this.num = num;
	}
	
	public static OrderStatusEnum instance(Integer value) {
		OrderStatusEnum[] enums = values();
		for (OrderStatusEnum statusEnum : enums) {
			if (statusEnum.value().equals(value)) {
				return statusEnum;
			}
		}
		return null;
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		System.out.println(OrderStatusEnum.SUCCESS.value());
		System.out.println(OrderStatusEnum.SUCCESS);
	}
}
