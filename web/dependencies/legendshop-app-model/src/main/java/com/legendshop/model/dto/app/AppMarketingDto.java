package com.legendshop.model.dto.app;

import java.io.Serializable;

public class AppMarketingDto implements Serializable {
	private static final long serialVersionUID = -3520581385310911684L;

	/** 营销活动编号  */
	private Long marketId;
	
	/** 触发条件*/
	private Double trigger;
	
	/**规则编号   */
	private Long ruleId;
	
	/**营销活动 类型 1: 满减促销 ; 2: 满折促销 3：限时促销  */
	private Integer marketType;
	
	/** 促销信息备注   */
	private String promotionInfo;

	public Long getMarketId() {
		return marketId;
	}

	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}

	public Double getTrigger() {
		return trigger;
	}

	public void setTrigger(Double trigger) {
		this.trigger = trigger;
	}

	public Long getRuleId() {
		return ruleId;
	}

	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}

	public Integer getMarketType() {
		return marketType;
	}

	public void setMarketType(Integer marketType) {
		this.marketType = marketType;
	}

	public String getPromotionInfo() {
		return promotionInfo;
	}

	public void setPromotionInfo(String promotionInfo) {
		this.promotionInfo = promotionInfo;
	}
}
