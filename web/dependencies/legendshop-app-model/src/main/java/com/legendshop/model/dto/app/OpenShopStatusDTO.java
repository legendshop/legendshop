/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.model.dto.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 商家入驻状态DTO
 * @author Joe
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "商家入驻状态DTO")
public class OpenShopStatusDTO {

	@ApiModelProperty(value = "商家ID")
	private Long shopId;

	@ApiModelProperty(value = "入驻状态：[-10：未申请 -1：等待审核 -2：拒绝 -3：关闭店铺 -4：联系人信息失败 -5:公司信息失败 -6:店铺信息失败 0:店铺下线 1：正常营业]")
	private Integer status;

	@ApiModelProperty(value = "审核类型")
	private String auditType;

	@ApiModelProperty(value = "审核意见")
	private String auditOpinion;

	@ApiModelProperty(value = "其他，暂时用来接收审核成功后，返回的PC端域名")
	private String other;

}
