package com.legendshop.model.dto.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 支付请求参数
 */
@ApiModel(value="支付请求参数")
public class AppPaytoParamsDto {

	/** 支付的订单IDS */
	@ApiModelProperty(value="支付的订单ID数组",required=true)
	private String [] subNumbers;
	
	/** 支付类型  PaymentProcessorEnum */
	@ApiModelProperty(value="支付类型, ALP: 支付宝, SIMULATE: 模拟支付, WX_PAY: 微信公众号支付(JSAPI), WX_H5_PAY: 微信H5支付, WX_APP_PAY: 微信APP支付, WX_MP_PAY: 小程序支付")
	private String payTypeId;
	
	@ApiModelProperty(value="支付来源: PC: PC端, WAP: wap端(普通浏览器), WEIXIN: 微信端浏览器, APP: 原生APP, WX_MP: 微信小程序")
	private String paySource;
	
	/** 余额支付方式(1:预付款  2：金币) */
	@ApiModelProperty(value="余额支付方式(1:预付款  2：金币, 默认预存款)")
	private Integer prePayTypeVal = 1;
	
	/** 支付通过支付密码验证 */
	@ApiModelProperty(value="混合支付使用到，确认余额支付密码是否正确, 付通过支付密码验证,如果密码正确则传字符串1")
	private String passwordCallback;
	
	/** 是否预售  */
	@ApiModelProperty(value="是否预售")
	private boolean presell=false;
	
	/** 预售支付:尾款/定金  */
	@ApiModelProperty(value="预售支付:尾款/定金")
	private Integer payPctType;
	
	/** 微信openId, 不应该是在用户端传过来的 */
	@ApiModelProperty(value="微信用户openId")
	private String openId;
	
	public String[] getSubNumbers() {
		return subNumbers;
	}

	public void setSubNumbers(String[] subNumbers) {
		this.subNumbers = subNumbers;
	}

	public String getPayTypeId() {
		return payTypeId;
	}

	public void setPayTypeId(String payTypeId) {
		this.payTypeId = payTypeId;
	}

	public String getPasswordCallback() {
		return passwordCallback;
	}

	public void setPasswordCallback(String passwordCallback) {
		this.passwordCallback = passwordCallback;
	}

	public Integer getPrePayTypeVal() {
		return prePayTypeVal;
	}

	public void setPrePayTypeVal(Integer prePayTypeVal) {
		this.prePayTypeVal = prePayTypeVal;
	}

	public Integer getPayPctType() {
		return payPctType;
	}

	public void setPayPctType(Integer payPctType) {
		this.payPctType = payPctType;
	}

	public boolean isPresell() {
		return presell;
	}
	
	public void setPresell(boolean presell) {
		this.presell = presell;
	}

	public String getPaySource() {
		return paySource;
	}

	public void setPaySource(String paySource) {
		this.paySource = paySource;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}
}
