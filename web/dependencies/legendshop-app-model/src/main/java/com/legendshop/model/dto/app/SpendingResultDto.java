package com.legendshop.model.dto.app;

import java.io.Serializable;

/**
 * 消费记录
 * @author Administrator
 *
 */
public class SpendingResultDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private String availablePredeposit;
	private String freezePredeposit;
	private PdCashLogsDto pdCashLogs;
	public String getAvailablePredeposit() {
		return availablePredeposit;
	}
	public void setAvailablePredeposit(String availablePredeposit) {
		this.availablePredeposit = availablePredeposit;
	}
	public String getFreezePredeposit() {
		return freezePredeposit;
	}
	public void setFreezePredeposit(String freezePredeposit) {
		this.freezePredeposit = freezePredeposit;
	}
	public PdCashLogsDto getPdCashLogs() {
		return pdCashLogs;
	}
	public void setPdCashLogs(PdCashLogsDto pdCashLogs) {
		this.pdCashLogs = pdCashLogs;
	}
	
}
