package com.legendshop.model.dto.app;

import java.io.Serializable;

public class ReturnGoodsDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private int basketCount;
	private double cash;
	private long finallyDate;
	private String id;
	private String pic;
	private double price;
	private String productId;
	private String productName;
	private String skuId;
	private String subItemId;
	private String subNumber;
	
	
	public int getBasketCount() {
		return basketCount;
	}
	public void setBasketCount(int basketCount) {
		this.basketCount = basketCount;
	}
	public double getCash() {
		return cash;
	}
	public void setCash(double cash) {
		this.cash = cash;
	}
	public long getFinallyDate() {
		return finallyDate;
	}
	public void setFinallyDate(long finallyDate) {
		this.finallyDate = finallyDate;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String pic) {
		this.pic = pic;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getSkuId() {
		return skuId;
	}
	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}
	public String getSubItemId() {
		return subItemId;
	}
	public void setSubItemId(String subItemId) {
		this.subItemId = subItemId;
	}
	public String getSubNumber() {
		return subNumber;
	}
	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}
	
	
	
}
