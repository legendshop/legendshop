package com.legendshop.model.dto.app;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value="InvoiceSubDto发票") 
public class InvoiceSubDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 发票类型：普通发票" : "增值税" */
	@ApiModelProperty(value="发票类型：普通发 : '增值税' ")
	private String type;

	/** 发票抬头 */
	@ApiModelProperty(value="发票抬头")
	private String title;

	/** 单位 ：个人 单位 */
	@ApiModelProperty(value="单位 ：个人 单位")
	private String company;

	/** 发票内容 是否明细 */
	@ApiModelProperty(value="发票内容 是否明细")
	private String content;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
