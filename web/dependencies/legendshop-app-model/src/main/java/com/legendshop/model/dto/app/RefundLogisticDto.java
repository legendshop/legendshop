package com.legendshop.model.dto.app;

import java.io.Serializable;

public class RefundLogisticDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long createTime;
	private int dvyId;
	private int dvyTypeId;
 
	private long modifyTime;
	private int isSystem;
	private int id;
 
	private String name;
	private String notes;
	private int printtempId;
	
	private String userId;
	private String ems;
	private String userName;
	private int shopId;
	public long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}
	public int getDvyId() {
		return dvyId;
	}
	public void setDvyId(int dvyId) {
		this.dvyId = dvyId;
	}
	public int getDvyTypeId() {
		return dvyTypeId;
	}
	public void setDvyTypeId(int dvyTypeId) {
		this.dvyTypeId = dvyTypeId;
	}
	public long getModifyTime() {
		return modifyTime;
	}
	public void setModifyTime(long modifyTime) {
		this.modifyTime = modifyTime;
	}
	public int getIsSystem() {
		return isSystem;
	}
	public void setIsSystem(int isSystem) {
		this.isSystem = isSystem;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public int getPrinttempId() {
		return printtempId;
	}
	public void setPrinttempId(int printtempId) {
		this.printtempId = printtempId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEms() {
		return ems;
	}
	public void setEms(String ems) {
		this.ems = ems;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
 
	
}
