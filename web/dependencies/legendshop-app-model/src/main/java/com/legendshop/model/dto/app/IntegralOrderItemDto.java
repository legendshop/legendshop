package com.legendshop.model.dto.app;

import java.io.Serializable;


public class IntegralOrderItemDto  implements Serializable{
    private long  id;
	private Long orderItemId;
	
	private Long prodId;
	
	private String prodName;
	
	private String prodPic;
	
	private Integer basketCount;
	
	private Integer exchangeIntegral;
	
	private Double totalIntegral;
	
	private Double price;
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public Long getOrderItemId() {
		return orderItemId;
	}



	public void setOrderItemId(Long orderItemId) {
		this.orderItemId = orderItemId;
	}



	public Long getProdId() {
		return prodId;
	}



	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}



	public String getProdName() {
		return prodName;
	}



	public void setProdName(String prodName) {
		this.prodName = prodName;
	}



	public String getProdPic() {
		return prodPic;
	}



	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}



	public Integer getBasketCount() {
		return basketCount;
	}



	public void setBasketCount(Integer basketCount) {
		this.basketCount = basketCount;
	}



	public Integer getExchangeIntegral() {
		return exchangeIntegral;
	}



	public void setExchangeIntegral(Integer exchangeIntegral) {
		this.exchangeIntegral = exchangeIntegral;
	}



	public Double getTotalIntegral() {
		return totalIntegral;
	}



	public void setTotalIntegral(Double totalIntegral) {
		this.totalIntegral = totalIntegral;
	}



	public Double getPrice() {
		return price;
	}



	public void setPrice(Double price) {
		this.price = price;
	}


}
