package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.List;

public class OrderDetailDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String actualTotal;
	private String addrOrderId;
	private boolean cod;
	private String couponOffPrice;
	private int deleteStatus;
	private DeliveryDto delivery;
	private String discountPrice;
	private String distCommisAmount;
	private long dvyDate;
	private String dvyFlowId;
	private String dvyType;
	private String dvyTypeId;
	private String finallyDate;
	private String freightAmount;
	private String id;
	private OrderDetailInvoiceDto invoiceSubDto;
	private String invoiceSubId;
	private String isNeedInvoice;
	private String ispay;
	private String orderRemark;
	private String other;
	private long payDate;

	private int payManner;
	private String payTypeId;
	private String payTypeName;
	private String prodName;
	private int productNums;
	private int score;

	private int shopId;
	private String shopName;
	private int status;
	private long subDate;
	private String subHistorys;
	private String subId;

	private String subNum;
	private List<OrderItemDto> subOrderItemDtos;
	private String subType;
	private String total;
	private String updateDate;
	private AddressDto usrAddrSubDto;
	private String userId;
	private String userName;
	private String volume;
	private String weight;
	public String getActualTotal() {
		return actualTotal;
	}
	public void setActualTotal(String actualTotal) {
		this.actualTotal = actualTotal;
	}
	public String getAddrOrderId() {
		return addrOrderId;
	}
	
	public OrderDetailInvoiceDto getInvoiceSubDto() {
		return invoiceSubDto;
	}
	public void setInvoiceSubDto(OrderDetailInvoiceDto invoiceSubDto) {
		this.invoiceSubDto = invoiceSubDto;
	}
	public void setAddrOrderId(String addrOrderId) {
		this.addrOrderId = addrOrderId;
	}
	public boolean isCod() {
		return cod;
	}
	public void setCod(boolean cod) {
		this.cod = cod;
	}
	public String getCouponOffPrice() {
		return couponOffPrice;
	}
	public void setCouponOffPrice(String couponOffPrice) {
		this.couponOffPrice = couponOffPrice;
	}
	public int getDeleteStatus() {
		return deleteStatus;
	}
	public void setDeleteStatus(int deleteStatus) {
		this.deleteStatus = deleteStatus;
	}
	public DeliveryDto getDelivery() {
		return delivery;
	}
	public void setDelivery(DeliveryDto delivery) {
		this.delivery = delivery;
	}
	public String getDiscountPrice() {
		return discountPrice;
	}
	public void setDiscountPrice(String discountPrice) {
		this.discountPrice = discountPrice;
	}
	public String getDistCommisAmount() {
		return distCommisAmount;
	}
	public void setDistCommisAmount(String distCommisAmount) {
		this.distCommisAmount = distCommisAmount;
	}
	public long getDvyDate() {
		return dvyDate;
	}
	public void setDvyDate(long dvyDate) {
		this.dvyDate = dvyDate;
	}
	public String getDvyFlowId() {
		return dvyFlowId;
	}
	public void setDvyFlowId(String dvyFlowId) {
		this.dvyFlowId = dvyFlowId;
	}
	public String getDvyType() {
		return dvyType;
	}
	public void setDvyType(String dvyType) {
		this.dvyType = dvyType;
	}
	public String getDvyTypeId() {
		return dvyTypeId;
	}
	public void setDvyTypeId(String dvyTypeId) {
		this.dvyTypeId = dvyTypeId;
	}
	public String getFinallyDate() {
		return finallyDate;
	}
	public void setFinallyDate(String finallyDate) {
		this.finallyDate = finallyDate;
	}
	public String getFreightAmount() {
		return freightAmount;
	}
	public void setFreightAmount(String freightAmount) {
		this.freightAmount = freightAmount;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getInvoiceSubId() {
		return invoiceSubId;
	}
	public void setInvoiceSubId(String invoiceSubId) {
		this.invoiceSubId = invoiceSubId;
	}
	public String getIsNeedInvoice() {
		return isNeedInvoice;
	}
	public void setIsNeedInvoice(String isNeedInvoice) {
		this.isNeedInvoice = isNeedInvoice;
	}
	public String getIspay() {
		return ispay;
	}
	public void setIspay(String ispay) {
		this.ispay = ispay;
	}
	public String getOrderRemark() {
		return orderRemark;
	}
	public void setOrderRemark(String orderRemark) {
		this.orderRemark = orderRemark;
	}
	public String getOther() {
		return other;
	}
	public void setOther(String other) {
		this.other = other;
	}
	public long getPayDate() {
		return payDate;
	}
	public void setPayDate(long payDate) {
		this.payDate = payDate;
	}
	public int getPayManner() {
		return payManner;
	}
	public void setPayManner(int payManner) {
		this.payManner = payManner;
	}
	public String getPayTypeId() {
		return payTypeId;
	}
	public void setPayTypeId(String payTypeId) {
		this.payTypeId = payTypeId;
	}
	public String getPayTypeName() {
		return payTypeName;
	}
	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}
	public String getProdName() {
		return prodName;
	}
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	public int getProductNums() {
		return productNums;
	}
	public void setProductNums(int productNums) {
		this.productNums = productNums;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getSubDate() {
		return subDate;
	}
	public void setSubDate(long subDate) {
		this.subDate = subDate;
	}
	public String getSubHistorys() {
		return subHistorys;
	}
	public void setSubHistorys(String subHistorys) {
		this.subHistorys = subHistorys;
	}
	public String getSubId() {
		return subId;
	}
	public void setSubId(String subId) {
		this.subId = subId;
	}
	public String getSubNum() {
		return subNum;
	}
	public void setSubNum(String subNum) {
		this.subNum = subNum;
	}
	public List<OrderItemDto> getSubOrderItemDtos() {
		return subOrderItemDtos;
	}
	public void setSubOrderItemDtos(List<OrderItemDto> subOrderItemDtos) {
		this.subOrderItemDtos = subOrderItemDtos;
	}
	public String getSubType() {
		return subType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public AddressDto getUsrAddrSubDto() {
		return usrAddrSubDto;
	}
	public void setUsrAddrSubDto(AddressDto usrAddrSubDto) {
		this.usrAddrSubDto = usrAddrSubDto;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}

	
}
