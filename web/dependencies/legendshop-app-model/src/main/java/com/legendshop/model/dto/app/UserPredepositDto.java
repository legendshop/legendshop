package com.legendshop.model.dto.app;

import java.io.Serializable;

import com.legendshop.model.entity.ExpensesRecord;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.model.entity.PdRecharge;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * 
 *@author Tony
 *
 */
@ApiModel(value="余额帐Dto") 
public class UserPredepositDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="可用余额  Pre-deposit")  
	private Double availablePredeposit=0d;
	
	@ApiModelProperty(value="冻结余额  Pre-deposit")  
	private Double freezePredeposit=0d;
	
	/**余额账户记录**/
	@ApiModelProperty(value="余额账户记录")  
	private AppPageSupport<PdCashLog> pdCashLogs;
	
	/**充值记录**/
	@ApiModelProperty(value="充值记录")  
	private AppPageSupport<PdRecharge> recharges;
	
	/** 消费记录 */
	@ApiModelProperty(value="消费记录")  
	private AppPageSupport<ExpensesRecord> expensesRecord;
	
	public Double getAvailablePredeposit() {
		return availablePredeposit;
	}

	public void setAvailablePredeposit(Double availablePredeposit) {
		this.availablePredeposit = availablePredeposit;
	}

	public Double getFreezePredeposit() {
		return freezePredeposit;
	}

	public void setFreezePredeposit(Double freezePredeposit) {
		this.freezePredeposit = freezePredeposit;
	}

	public AppPageSupport<PdCashLog> getPdCashLogs() {
		return pdCashLogs;
	}

	public void setPdCashLogs(AppPageSupport<PdCashLog> pdCashLogs) {
		this.pdCashLogs = pdCashLogs;
	}

	public AppPageSupport<PdRecharge> getRecharges() {
		return recharges;
	}

	public void setRecharges(AppPageSupport<PdRecharge> recharges) {
		this.recharges = recharges;
	}

	public AppPageSupport<ExpensesRecord> getExpensesRecord() {
		return expensesRecord;
	}

	public void setExpensesRecord(AppPageSupport<ExpensesRecord> expensesRecord) {
		this.expensesRecord = expensesRecord;
	}

}
