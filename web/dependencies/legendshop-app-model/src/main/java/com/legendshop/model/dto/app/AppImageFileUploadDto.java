package com.legendshop.model.dto.app;

/**
 * APP图片文件上传DTO
 * @author 开发很忙
 */
public class AppImageFileUploadDto {
	
	/** 文件名 */
	private String fileName;
	
	/** bease64编码 */
	private String base64Code;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getBase64Code() {
		return base64Code;
	}

	public void setBase64Code(String base64Code) {
		this.base64Code = base64Code;
	}
	
	
}
