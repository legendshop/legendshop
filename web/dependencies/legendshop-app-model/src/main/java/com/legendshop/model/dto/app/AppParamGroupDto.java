package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.List;

public class AppParamGroupDto implements Serializable {

	private static final long serialVersionUID = 587454756801592173L;

	private String groupName;//分组名称
	
	private List<AppProdParamDto> params;//参数列表

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<AppProdParamDto> getParams() {
		return params;
	}

	public void setParams(List<AppProdParamDto> params) {
		this.params = params;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof AppParamGroupDto){
			if(this.groupName.equals(((AppParamGroupDto)obj).getGroupName()))
				return true;
		}
		return false;
	}
	
	public int hashCode() {
		return this.groupName.hashCode();
	}

}
