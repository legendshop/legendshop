package com.legendshop.model.dto.app.store;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 店铺商品搜索参数
 * @author linzh
 */
@ApiModel("店铺商品查询参数")
public class AppShopProdSearchParams{
	
	/** 当前页码. */
	@ApiModelProperty(value = "当前页码")
	private String curPageNO;
	
	/** 每页条数  初始化10条. */
	@ApiModelProperty(value = "每页条数  初始化10条")
	private Integer pageSize;
	
	/** 店铺id */
	@ApiModelProperty(value = "店铺id")
	private Long shopId;
	
	/** (商家小分类)一级分类id */
	@ApiModelProperty(value = "(商家小分类)一级分类id")
	private Long fireCat;
	
	/** (商家小分类)二级分类id */
	@ApiModelProperty(value = "(商家小分类)二级分类id")
	private Long secondCat;
	
	/**  (商家小分类)三级分类id */
	@ApiModelProperty(value = "(商家小分类)三级分类id")
	private Long thirdCat;
	
	/** 排序方式  格式：（排序名称,排序方向）类型String [recDate,desc:默认  sales: 购买数  good: 评论数   price: 价格] */
	@ApiModelProperty(value = " 排序方式  格式：（排序名称,排序方向） [ buys: 销量  comments: 评论数   cash: 价格]  示例： order = 'buys，asc'") 
	private String order;

	/** 用户输入的关键字. */
	@ApiModelProperty(value = "用户输入的关键字")
	private String keyword;

	public String getCurPageNO() {
		return curPageNO;
	}

	public void setCurPageNO(String curPageNO) {
		this.curPageNO = curPageNO;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getFireCat() {
		return fireCat;
	}

	public void setFireCat(Long fireCat) {
		this.fireCat = fireCat;
	}

	public Long getSecondCat() {
		return secondCat;
	}

	public void setSecondCat(Long secondCat) {
		this.secondCat = secondCat;
	}

	public Long getThirdCat() {
		return thirdCat;
	}

	public void setThirdCat(Long thirdCat) {
		this.thirdCat = thirdCat;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

}
