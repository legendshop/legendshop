/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app收藏的店铺Dto.
 */
@ApiModel("收藏的店铺Dto")
public class AppFavoriteShopDto implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -788070528527848996L;

	/** 唯一标识id  */
	@ApiModelProperty(value = "唯一标识id")
	private Long fsId;

	/** 店铺ID */
	@ApiModelProperty(value = "店铺ID")
	private Long shopId;

	/** 收藏时间  */
	@ApiModelProperty(value = "收藏时间")
	private Date recDate;

	/** 用户ID  */
	@ApiModelProperty(value = "用户ID")
	private String userId;

	/** 用户名称 */
	@ApiModelProperty(value = "用户名称")
	private String userName;

	/** 店铺名称  */
	@ApiModelProperty(value = "店铺名称")
	private String siteName;

	/** 店铺图片  */
	@ApiModelProperty(value = "店铺图片")
	private String shopPic;

	/** 店铺等级   */
	@ApiModelProperty(value = "店铺等级")
	private String gradeName;
	
	/** 店铺类型[0:专营,1:旗舰,2:自营]   */
	@ApiModelProperty(value = "店铺类型[0:专营,1:旗舰,2:自营]")
	private Integer shopType;
	
	/** 店铺信誉评分 */
	@ApiModelProperty(value ="店铺信誉评分")
	private Double credit;
	
	/** 已收藏数量 */
	@ApiModelProperty(value ="已收藏数量")
	private Long favoriteCount;

	public Long getFsId() {
		return fsId;
	}

	public void setFsId(Long fsId) {
		this.fsId = fsId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getShopPic() {
		return shopPic;
	}

	public void setShopPic(String shopPic) {
		this.shopPic = shopPic;
	}

	public String getGradeName() {
		return gradeName;
	}

	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}

	public Integer getShopType() {
		return shopType;
	}

	public void setShopType(Integer shopType) {
		this.shopType = shopType;
	}

	public Double getCredit() {
		return credit;
	}

	public void setCredit(Double credit) {
		this.credit = credit;
	}

	public Long getFavoriteCount() {
		return favoriteCount;
	}

	public void setFavoriteCount(Long favoriteCount) {
		this.favoriteCount = favoriteCount;
	}
}
