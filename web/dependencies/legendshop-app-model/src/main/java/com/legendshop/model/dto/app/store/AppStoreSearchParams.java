package com.legendshop.model.dto.app.store;

import com.legendshop.model.dto.search.AppSearchParams;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("店铺商品查询参数")
public class AppStoreSearchParams extends AppSearchParams{
	
	/** 店铺id */
	@ApiModelProperty(value = "店铺id")
	private Long shopId;
	
	/**
	 * 排序方式:
	 * synthesize: 综合
	 * sales: 购买数
	 * good: 评论数
	 * price: 价格
	 */
	@ApiModelProperty(value = " 排序方式: synthesize: 综合  sales: 购买数  good: 评论数   price: 价格")
	private String orderWay;
	
	/**
	 * 排序方向：
	 * ASC:升序
	 * DESC:降序
	 */
	@ApiModelProperty(value = "排序方向： ASC:升序  DESC:降序")
	private String orderType;

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getOrderWay() {
		return orderWay;
	}

	public void setOrderWay(String orderWay) {
		this.orderWay = orderWay;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
}
