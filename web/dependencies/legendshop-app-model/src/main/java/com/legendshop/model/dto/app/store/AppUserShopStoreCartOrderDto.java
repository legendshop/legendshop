package com.legendshop.model.dto.app.store;

import com.legendshop.model.dto.app.AppStoreDto;
import com.legendshop.model.dto.app.cartorder.ShopCartOrderDto;
import com.legendshop.model.entity.Invoice;
import com.legendshop.model.entity.UserAddress;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * APP 用户的购物车订单信息
 */
@ApiModel(value="门店购物车订单信息")
public class AppUserShopStoreCartOrderDto implements Serializable{

	private static final long serialVersionUID = -1834367556553151778L;

	@ApiModelProperty(value="用户Id")
	private  String userId;
	
	@ApiModelProperty(value="用户名") 
	private String userName;
	
	@ApiModelProperty(value="订单类型 [拍卖、普通、秒杀、门店订单]") 
	private String type;
	
	@ApiModelProperty(value="支付方式") 
	private Integer payManner;  
	
	@ApiModelProperty(value="预付款支付金额") 
	private Double pdAmount=0d; 
	
	@ApiModelProperty(value="订单商品总数量") 
    private int orderTotalQuanlity;
    
	@ApiModelProperty(value="订单商品总重量") 
    private double orderTotalWeight;
    
	@ApiModelProperty(value="订单商品总体积") 
    private double orderTotalVolume;
    
	@ApiModelProperty(value="商品总价") 
    private double orderTotalCash; 
    
	@ApiModelProperty(value="实际总值   商品总价+运费-优惠 -优惠券") 
    private double orderActualTotal; 
    
	@ApiModelProperty(value="订单运费") 
    private double orderFreightAmount;
    
	@ApiModelProperty(value="订单优惠总价") 
    private double allDiscount;
	
	@ApiModelProperty(value="用户地址") 
	private UserAddress userAddress;  
	
	@ApiModelProperty(value="订单是按店铺区分， 对应的店铺的购物车信息") 
    private List<ShopStoreCartOrderDto> shopCarts = new ArrayList<ShopStoreCartOrderDto>();

	@ApiModelProperty(value="orderToken") 
	private String orderToken;
	
	/** token， 防止重复提交. */
	@ApiModelProperty(value = "token， 防止重复提交")
	private String token;


	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getPayManner() {
		return payManner;
	}

	public void setPayManner(Integer payManner) {
		this.payManner = payManner;
	}

	public Double getPdAmount() {
		return pdAmount;
	}

	public void setPdAmount(Double pdAmount) {
		this.pdAmount = pdAmount;
	}

	public int getOrderTotalQuanlity() {
		return orderTotalQuanlity;
	}

	public void setOrderTotalQuanlity(int orderTotalQuanlity) {
		this.orderTotalQuanlity = orderTotalQuanlity;
	}

	public double getOrderTotalWeight() {
		return orderTotalWeight;
	}

	public void setOrderTotalWeight(double orderTotalWeight) {
		this.orderTotalWeight = orderTotalWeight;
	}

	public double getOrderTotalVolume() {
		return orderTotalVolume;
	}

	public void setOrderTotalVolume(double orderTotalVolume) {
		this.orderTotalVolume = orderTotalVolume;
	}

	public double getOrderTotalCash() {
		return orderTotalCash;
	}

	public void setOrderTotalCash(double orderTotalCash) {
		this.orderTotalCash = orderTotalCash;
	}

	public double getOrderActualTotal() {
		return orderActualTotal;
	}

	public void setOrderActualTotal(double orderActualTotal) {
		this.orderActualTotal = orderActualTotal;
	}

	public double getOrderFreightAmount() {
		return orderFreightAmount;
	}

	public void setOrderFreightAmount(double orderFreightAmount) {
		this.orderFreightAmount = orderFreightAmount;
	}

	public double getAllDiscount() {
		return allDiscount;
	}

	public void setAllDiscount(double allDiscount) {
		this.allDiscount = allDiscount;
	}

	public UserAddress getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(UserAddress userAddress) {
		this.userAddress = userAddress;
	}

	public List<ShopStoreCartOrderDto> getShopCarts() {
		return shopCarts;
	}

	public void setShopCarts(List<ShopStoreCartOrderDto> shopCarts) {
		this.shopCarts = shopCarts;
	}

	public String getOrderToken() {
		return orderToken;
	}

	public void setOrderToken(String orderToken) {
		this.orderToken = orderToken;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
