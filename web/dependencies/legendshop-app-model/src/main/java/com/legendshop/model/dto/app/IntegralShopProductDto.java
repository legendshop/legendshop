package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.Date;

public class IntegralShopProductDto implements Serializable {

	private static final long serialVersionUID = 1L;
	/** 自增编号 */
	private Long id;

	/** 积分商品名称 */
	private String name;

	/** 商品价格 */
	private Double price;

	/** 兑换积分 */
	private Integer exchangeIntegral;

	/** 商品库存 */
	private Long prodStock;

	/** 商品编号 */
	private String prodNumber;

	/** 商品图片 */
	private String prodImage;

	/** 状态 */
	private Integer status;

	/** 是否推荐商品 */
	private Integer isCommend;

	/** 销售数量 */
	private Integer saleNum;

	/** 浏览次数 */
	private Integer viewNum;

	/** 是否限制购买数量 */
	private Integer isLimit;

	/** 限制数量 */
	private Integer limitNum;

	/** 商品描述 */
	private String prodDesc;

	/** SEO关键字 */
	private String seoKeyword;

	/** SEO description */
	private String seoDesc;

	/** 添加时间 */
	private Date addTime;

	/** 商城 分类 **/
	protected Long firstCid;

	/** 商城 分类 **/
	protected Long twoCid;

	/** 商城 分类 **/
	protected Long thirdCid;

	/** 商城 分类 **/
	protected String categoryName;
	
	private String twoCategoryName;
	
	private String thirdCategoryName;

	private String userName;
	/**购买数量**/
	private String buyCount;
	
	public String getBuyCount() {
		return buyCount;
	}

	public void setBuyCount(String buyCount) {
		this.buyCount = buyCount;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getExchangeIntegral() {
		return exchangeIntegral;
	}

	public void setExchangeIntegral(Integer exchangeIntegral) {
		this.exchangeIntegral = exchangeIntegral;
	}

	public Long getProdStock() {
		return prodStock;
	}

	public void setProdStock(Long prodStock) {
		this.prodStock = prodStock;
	}

	public String getProdNumber() {
		return prodNumber;
	}

	public void setProdNumber(String prodNumber) {
		this.prodNumber = prodNumber;
	}

	public String getProdImage() {
		return prodImage;
	}

	public void setProdImage(String prodImage) {
		this.prodImage = prodImage;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getIsCommend() {
		return isCommend;
	}

	public void setIsCommend(Integer isCommend) {
		this.isCommend = isCommend;
	}

	public Integer getSaleNum() {
		return saleNum;
	}

	public void setSaleNum(Integer saleNum) {
		this.saleNum = saleNum;
	}

	public Integer getViewNum() {
		return viewNum;
	}

	public void setViewNum(Integer viewNum) {
		this.viewNum = viewNum;
	}

	public Integer getIsLimit() {
		return isLimit;
	}

	public void setIsLimit(Integer isLimit) {
		this.isLimit = isLimit;
	}

	public Integer getLimitNum() {
		return limitNum;
	}

	public void setLimitNum(Integer limitNum) {
		this.limitNum = limitNum;
	}

	public String getProdDesc() {
		return prodDesc;
	}

	public void setProdDesc(String prodDesc) {
		this.prodDesc = prodDesc;
	}

	public String getSeoKeyword() {
		return seoKeyword;
	}

	public void setSeoKeyword(String seoKeyword) {
		this.seoKeyword = seoKeyword;
	}

	public String getSeoDesc() {
		return seoDesc;
	}

	public void setSeoDesc(String seoDesc) {
		this.seoDesc = seoDesc;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public Long getFirstCid() {
		return firstCid;
	}

	public void setFirstCid(Long firstCid) {
		this.firstCid = firstCid;
	}

	public Long getTwoCid() {
		return twoCid;
	}

	public void setTwoCid(Long twoCid) {
		this.twoCid = twoCid;
	}

	public Long getThirdCid() {
		return thirdCid;
	}

	public void setThirdCid(Long thirdCid) {
		this.thirdCid = thirdCid;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getTwoCategoryName() {
		return twoCategoryName;
	}

	public void setTwoCategoryName(String twoCategoryName) {
		this.twoCategoryName = twoCategoryName;
	}

	public String getThirdCategoryName() {
		return thirdCategoryName;
	}

	public void setThirdCategoryName(String thirdCategoryName) {
		this.thirdCategoryName = thirdCategoryName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
}
