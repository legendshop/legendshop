/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.search;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app商品查询参数
 * 
 */

@ApiModel("商品查询参数")
public class AppProdSearchParms extends AppSearchParams{

	/** 分类ID */
	@ApiModelProperty(value = "分类ID")
	protected Long categoryId;
	
	/** 品牌ID */
	@ApiModelProperty(value = "品牌ID")
	protected Long brandId;

	/** 起始价格 */
	@ApiModelProperty(value = "起始价格")
	protected Double startPrice;
	
	/** 结束价格 */
	@ApiModelProperty(value = "结束价格")
	protected Double endPrice;
	
	/** 是否有库存 */
	@ApiModelProperty(value = "是否有库存")
	protected Boolean isHasProd;
	
	/** 是否支持分销 */
	@ApiModelProperty(value = "是否支持分销")
	protected Boolean isSupportDist;
	
	/** 商品所属的店铺类型 0专营,1旗舰,2自营 */
	@ApiModelProperty(value = "商品所属的店铺类型 0专营,1旗舰,2自营")
	protected Integer shopType;

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getBrandId() {
		return brandId;
	}

	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	public Double getStartPrice() {
		return startPrice;
	}

	public void setStartPrice(Double startPrice) {
		this.startPrice = startPrice;
	}

	public Double getEndPrice() {
		return endPrice;
	}

	public void setEndPrice(Double endPrice) {
		this.endPrice = endPrice;
	}

	public Boolean getIsHasProd() {
		return isHasProd;
	}

	public void setIsHasProd(Boolean isHasProd) {
		this.isHasProd = isHasProd;
	}

	public Boolean getIsSupportDist() {
		return isSupportDist;
	}

	public void setIsSupportDist(Boolean isSupportDist) {
		this.isSupportDist = isSupportDist;
	}

	public Integer getShopType() {
		return shopType;
	}

	public void setShopType(Integer shopType) {
		this.shopType = shopType;
	}
	
}
