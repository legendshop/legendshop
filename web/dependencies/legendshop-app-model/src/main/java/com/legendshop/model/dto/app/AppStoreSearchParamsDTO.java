package com.legendshop.model.dto.app;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("app门店列表查询参数")
public class AppStoreSearchParamsDTO {

	
	@ApiModelProperty(value = "商品Id",required = true)
	private Long prodId;
	
	@ApiModelProperty(value = "skuId",required = true)
	private Long skuId;
	
	@ApiModelProperty(value="经度",required = true)
	private Double lng;
	
	@ApiModelProperty(value="纬度",required = true)
	private Double lat;
	
	@ApiModelProperty(value = "当前查询页码")
	private String curPageNO;
	
	@ApiModelProperty(value = "查询页数")
	private Integer pageSize;
	
	@ApiModelProperty(value = "店铺Id")
	private Long shopId;
	
	@ApiModelProperty(value = "省级")
	private String province;
	
	@ApiModelProperty(value = "城市")
	private String city;
	
	@ApiModelProperty(value = "区域")
	private String area;
	
	@ApiModelProperty(value="门店名称")
	private String storeName;


	public Long getProdId() {
		return prodId;
	}


	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}


	public Long getSkuId() {
		return skuId;
	}


	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}


	public Double getLng() {
		return lng;
	}


	public void setLng(Double lng) {
		this.lng = lng;
	}


	public Double getLat() {
		return lat;
	}


	public void setLat(Double lat) {
		this.lat = lat;
	}


	public String getCurPageNO() {
		return curPageNO;
	}


	public void setCurPageNO(String curPageNO) {
		this.curPageNO = curPageNO;
	}


	public Integer getPageSize() {
		return pageSize;
	}


	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}


	public Long getShopId() {
		return shopId;
	}


	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getProvince() {
		return province;
	}


	public void setProvince(String province) {
		this.province = province;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}


	public void setArea(String area) {
		this.area = area;
	}


	public String getStoreName() {
		return storeName;
	}


	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	
}
