package com.legendshop.model.dto.app;

import java.io.Serializable;


/**
 * 参与活动的
 * @author Tony
 *
 */
public class AppMarketingProdDto  implements Serializable{

	private static final long serialVersionUID = 8593739419180545688L;

	
	/** 营销活动编号 */
	private Long marketId; 
	
	
	/** 店铺编号 */
	private Long shopId;
	
	/**  */
	private Long id; 
		
	/** 商品ID */
	private Long prodId; 
	
	private String prodName;
	
	private String skuName;
	
	private Double cash;
	
	private Double skuCash;
	
	private Integer stock;
	
	private Integer skuStock;
	
	private Long skuId;
	
	private String pic;
	
	private Float discount;
	
	private Double discountPrice;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public Double getSkuCash() {
		return skuCash;
	}

	public void setSkuCash(Double skuCash) {
		this.skuCash = skuCash;
	}

	public Integer getSkuStock() {
		return skuStock;
	}

	public void setSkuStock(Integer skuStock) {
		this.skuStock = skuStock;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Float getDiscount() {
		return discount;
	}

	public void setDiscount(Float discount) {
		this.discount = discount;
	}

	public Double getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}

	public Long getMarketId() {
		return marketId;
	}

	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	
	
	
	
	
}
