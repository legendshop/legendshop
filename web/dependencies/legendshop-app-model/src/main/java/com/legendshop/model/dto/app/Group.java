package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.Date;

/**
 * 团购商品列表DTO
 */

public class Group implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5454972528384380307L;

	/** id */
	private Long id;

	/** 团购名称 */
	private String groupName;

	/** 开始时间 */
	private long startTime;

	/** 结束时间 */
	private long endTime;

	/** 商品ID */
	private Long productId;

	/** 店铺ID */
	private Long shopId;

	/** 店铺名称 */
	private String shopName;

	/** 团购价格 */
	private double groupPrice;

	/** 已购买人数 */
	private Long buyerCount;

	/** 限购人数 */
	private Integer buyQuantity;

	/** 活动的状态有：未提审、已提审、上线（审核通过）、审核不通过。 */
	private Integer status;

	/** 分类名称 */
	private String categoryName;

	/** 团购1级分类ID */
	private Long firstCatId;

	/** 团购2级分类ID */
	private Long secondCatId;

	/** 团购3级分类ID */
	private Long thirdCatId;

	/** 团购图片 */
	private String groupImage;

	/** 团购详情 */
	private String groupDesc;

	private String auditOpinion;

	private long auditTime;

	private long overdue;// 查询过期标识

	private long overdate;// 过滤过期

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public double getGroupPrice() {
		return groupPrice;
	}

	public void setGroupPrice(double groupPrice) {
		this.groupPrice = groupPrice;
	}

	public Long getBuyerCount() {
		return buyerCount;
	}

	public void setBuyerCount(Long buyerCount) {
		this.buyerCount = buyerCount;
	}

	public Integer getBuyQuantity() {
		return buyQuantity;
	}

	public void setBuyQuantity(Integer buyQuantity) {
		this.buyQuantity = buyQuantity;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Long getFirstCatId() {
		return firstCatId;
	}

	public void setFirstCatId(Long firstCatId) {
		this.firstCatId = firstCatId;
	}

	public Long getSecondCatId() {
		return secondCatId;
	}

	public void setSecondCatId(Long secondCatId) {
		this.secondCatId = secondCatId;
	}

	public Long getThirdCatId() {
		return thirdCatId;
	}

	public void setThirdCatId(Long thirdCatId) {
		this.thirdCatId = thirdCatId;
	}

	public String getGroupImage() {
		return groupImage;
	}

	public void setGroupImage(String groupImage) {
		this.groupImage = groupImage;
	}

	public String getGroupDesc() {
		return groupDesc;
	}

	public void setGroupDesc(String groupDesc) {
		this.groupDesc = groupDesc;
	}

	public String getAuditOpinion() {
		return auditOpinion;
	}

	public void setAuditOpinion(String auditOpinion) {
		this.auditOpinion = auditOpinion;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public long getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(long auditTime) {
		this.auditTime = auditTime;
	}

	public long getOverdue() {
		return overdue;
	}

	public void setOverdue(long overdue) {
		this.overdue = overdue;
	}

	public long getOverdate() {
		return overdate;
	}

	public void setOverdate(long overdate) {
		this.overdate = overdate;
	}

}
