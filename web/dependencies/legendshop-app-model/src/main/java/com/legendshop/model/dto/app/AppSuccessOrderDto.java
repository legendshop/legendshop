/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.legendshop.model.entity.InvoiceSub;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * App用于封装提交成功普通订单数据.
 */

@ApiModel("提交成功订单后数据")
public class AppSuccessOrderDto implements Serializable{

	private static final long serialVersionUID = -2864810122569245142L;

	/** 订单ID **/
	@ApiModelProperty(value = "订单ID")
	private Long subId;

	/** 商品名称 **/
	@ApiModelProperty(value = "商品名称")
	private String prodName;
	
	/** 用户名称 **/
	@ApiModelProperty(value = "用户名称 ")
	private String userName;
	
	/** 下订单的时间 **/
	@ApiModelProperty(value = "下订单的时间 ")
	private Date subDate;

	/** 支付的时间  **/
	@ApiModelProperty(value = "支付的时间")
	private Date payDate;
	
	/** 订单号 **/
	@ApiModelProperty(value = "订单号")
	private String subNumber;
	
	/** 订单商品原价  **/
	@ApiModelProperty(value = "订单商品原价")
	private Double total;

	/** 商品订单实际价格(运费 折扣 促销),如果是充值则为充值金额 **/
	@ApiModelProperty(value = "商品订单实际价格(运费 折扣 促销),如果是充值则为充值金额 ")
	private Double actualTotal;
	
	/** 支付方式名称 **/
	@ApiModelProperty(value = "支付方式名称 ")
	private String payTypeName;
	
	/** 详细收货地址 */
	@ApiModelProperty(value="详细收货地址") 
	private String detailAddress;
	
	/** 收货人 **/
	@ApiModelProperty(value = "收货人")
	private String receiver;
	
	/** 联系电话 **/
	@ApiModelProperty(value = "联系电话")
	private String mobile;
	
	/** 订单单据类型 **/
	@ApiModelProperty(value = "订单单据类型")
	private String subSettlementType;
	
	/** 订单类型 **/
	@ApiModelProperty(value = "订单类型")
	private String  subType;
	
	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getSubDate() {
		return subDate;
	}

	public void setSubDate(Date subDate) {
		this.subDate = subDate;
	}

	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getActualTotal() {
		return actualTotal;
	}

	public void setActualTotal(Double actualTotal) {
		this.actualTotal = actualTotal;
	}

	public String getPayTypeName() {
		return payTypeName;
	}

	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getDetailAddress() {
		return detailAddress;
	}

	public void setDetailAddress(String detailAddress) {
		this.detailAddress = detailAddress;
	}

	public String getSubSettlementType() {
		return subSettlementType;
	}

	public void setSubSettlementType(String subSettlementType) {
		this.subSettlementType = subSettlementType;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	
}
