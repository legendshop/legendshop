package com.legendshop.model.dto.app;

import java.io.Serializable;

public class PropertyInfoDto implements Serializable{

	private static final long serialVersionUID = -8859472540374903923L;

	/** 属性名 **/
	private String attrName;

	/** 属性值 **/
	private String attrValue;

	public String getAttrName() {
		return attrName;
	}

	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	public String getAttrValue() {
		return attrValue;
	}

	public void setAttrValue(String attrValue) {
		this.attrValue = attrValue;
	}
	
}
