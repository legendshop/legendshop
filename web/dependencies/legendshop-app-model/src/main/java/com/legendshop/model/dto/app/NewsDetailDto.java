package com.legendshop.model.dto.app;

import java.io.Serializable;
/**
 * 常见问题详情
 * @author Administrator
 *
 */
public class NewsDetailDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int highLine;
	private int id;
	private String newsBrief;
	private int newsCategoryId;
	private String newsCategoryName;
	
	private long newsDate;
	private int newsId;
	private String newsCategoryPic;
	private String newsTags;
	private String newsContent;
	
	private int positionId;
	private int position;
	private String positionTags;
	private String seq;
	private String newsTitle;
    
	private int shopId;
	private int status;
	private String userId;
	private String userName;
	public int getHighLine() {
		return highLine;
	}
	public void setHighLine(int highLine) {
		this.highLine = highLine;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNewsBrief() {
		return newsBrief;
	}
	public void setNewsBrief(String newsBrief) {
		this.newsBrief = newsBrief;
	}
	public int getNewsCategoryId() {
		return newsCategoryId;
	}
	public void setNewsCategoryId(int newsCategoryId) {
		this.newsCategoryId = newsCategoryId;
	}
	public String getNewsCategoryName() {
		return newsCategoryName;
	}
	public void setNewsCategoryName(String newsCategoryName) {
		this.newsCategoryName = newsCategoryName;
	}
	public long getNewsDate() {
		return newsDate;
	}
	public void setNewsDate(long newsDate) {
		this.newsDate = newsDate;
	}
	public int getNewsId() {
		return newsId;
	}
	public void setNewsId(int newsId) {
		this.newsId = newsId;
	}
	public String getNewsCategoryPic() {
		return newsCategoryPic;
	}
	public void setNewsCategoryPic(String newsCategoryPic) {
		this.newsCategoryPic = newsCategoryPic;
	}
	public String getNewsTags() {
		return newsTags;
	}
	public void setNewsTags(String newsTags) {
		this.newsTags = newsTags;
	}
	public String getNewsContent() {
		return newsContent;
	}
	public void setNewsContent(String newsContent) {
		this.newsContent = newsContent;
	}
	public int getPositionId() {
		return positionId;
	}
	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public String getPositionTags() {
		return positionTags;
	}
	public void setPositionTags(String positionTags) {
		this.positionTags = positionTags;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getNewsTitle() {
		return newsTitle;
	}
	public void setNewsTitle(String newsTitle) {
		this.newsTitle = newsTitle;
	}
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

   
}
