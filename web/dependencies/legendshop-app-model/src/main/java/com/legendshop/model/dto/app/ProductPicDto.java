package com.legendshop.model.dto.app;

import java.io.Serializable;

public class ProductPicDto implements Serializable{
	
	private static final long serialVersionUID = -5932906048458899140L;
	
	/** 图片路径 **/
	private String filePath;

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
}
