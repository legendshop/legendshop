package com.legendshop.model.dto.app.store;

import com.legendshop.model.dto.TransfeeDto;
import com.legendshop.model.dto.app.cartorder.ShopGroupCartOrderItemDto;
import com.legendshop.model.dto.buy.StoreCarts;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 购物车订单详情Dto
 */
@ApiModel("门店购物车订单详情")
public class ShopStoreCartOrderDto implements Serializable {


	private static final long serialVersionUID = 8559930644586032899L;

	/** 商家ID */
	@ApiModelProperty(value = "商家ID")
	private Long shopId;
	
	 /** 店铺名称 */
	@ApiModelProperty(value = "店铺名称")
	private String shopName;
	
	/** 商家状态 */
	@ApiModelProperty(value = "商家状态")
	private int shopStatus=1;
	
	/** 总价  */
	@ApiModelProperty(value = "总价")
	private Double shopTotalCash=0d;
	
	/** 店铺实际支付总额  */
	@ApiModelProperty(value = "店铺实际支付总额 ")
	private Double shopActualTotalCash=0d;
    
    /** 商品总重量  */
	@ApiModelProperty(value = "商品总重量")
    private Double shopTotalWeight;
    
    /** 商品总体积 */
	@ApiModelProperty(value = "商品总体积")
    private Double shopTotalVolume;
    
    /** 商品总数量 */
	@ApiModelProperty(value = "商品总数量")
    private int totalcount;

   /** 折扣价 */
	@ApiModelProperty(value = "折扣价")
    private Double discountPrice=0d;
	
	/** 获得积分 */
	@ApiModelProperty(value = "获得积分")
    private Double totalIntegral = 0D;
    
    /** 是否支持门店 */
	@ApiModelProperty(value = "是否支持门店")
    private boolean supportStore=false;

	/** 门店购物车分组  （门店自提用） */
	@ApiModelProperty(value = "门店购物车分组")
	private List<StoreCartsDto> storeCarts;


	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public int getShopStatus() {
		return shopStatus;
	}

	public void setShopStatus(int shopStatus) {
		this.shopStatus = shopStatus;
	}

	public Double getShopTotalCash() {
		return shopTotalCash;
	}

	public void setShopTotalCash(Double shopTotalCash) {
		this.shopTotalCash = shopTotalCash;
	}

	public Double getShopActualTotalCash() {
		return shopActualTotalCash;
	}

	public void setShopActualTotalCash(Double shopActualTotalCash) {
		this.shopActualTotalCash = shopActualTotalCash;
	}

	public Double getShopTotalWeight() {
		return shopTotalWeight;
	}

	public void setShopTotalWeight(Double shopTotalWeight) {
		this.shopTotalWeight = shopTotalWeight;
	}

	public Double getShopTotalVolume() {
		return shopTotalVolume;
	}

	public void setShopTotalVolume(Double shopTotalVolume) {
		this.shopTotalVolume = shopTotalVolume;
	}

	public int getTotalcount() {
		return totalcount;
	}

	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}

	public Double getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}

	public Double getTotalIntegral() {
		return totalIntegral;
	}

	public void setTotalIntegral(Double totalIntegral) {
		this.totalIntegral = totalIntegral;
	}

	public boolean isSupportStore() {
		return supportStore;
	}

	public void setSupportStore(boolean supportStore) {
		this.supportStore = supportStore;
	}

	public List<StoreCartsDto> getStoreCarts() {
		return storeCarts;
	}

	public void setStoreCarts(List<StoreCartsDto> storeCarts) {
		this.storeCarts = storeCarts;
	}
}
