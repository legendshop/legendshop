package com.legendshop.model.dto.app.orderReturn;

import java.io.Serializable;
import java.util.Date;

/**
 * @author quanzc
 * 退换货详情数据DTO
 */
public class AppReturnOrderDetailDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8858976663198526578L;

	/**申请人**/
	private String applyName;
	
	/**开户银行**/
	private String bankCode;
	
	/**退换原因**/
	private String returnDescription;
	
	/**持卡人姓名**/
	private String bankUserName;
	
	/**银行卡号**/
	private String bankAccount;
	
	/**银行信息(地址)**/
	private String bankInfo;
	
	/**物流公司**/
	private String logisticsCompany;
	
	/**物流单号**/
	private String logisticsOrderCode;
	
	/** 文件凭证1 */
	private String photoFile1; 
		
	/** 文件凭证2 */
	private String photoFile2; 
		
	/** 文件凭证3 */
	private String photoFile3; 
	
	/** 申请时间 */
	private Date createTime;
	
	/**退款金额**/
	private double orderTotalPrice;
	
	/**状态0:等待处理  1: 同意退货  -1:拒绝**/
	private Integer status;

	public String getApplyName() {
		return applyName;
	}

	public void setApplyName(String applyName) {
		this.applyName = applyName;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBankUserName() {
		return bankUserName;
	}

	public void setBankUserName(String bankUserName) {
		this.bankUserName = bankUserName;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getBankInfo() {
		return bankInfo;
	}

	public void setBankInfo(String bankInfo) {
		this.bankInfo = bankInfo;
	}

	public String getLogisticsCompany() {
		return logisticsCompany;
	}

	public void setLogisticsCompany(String logisticsCompany) {
		this.logisticsCompany = logisticsCompany;
	}

	public String getLogisticsOrderCode() {
		return logisticsOrderCode;
	}

	public void setLogisticsOrderCode(String logisticsOrderCode) {
		this.logisticsOrderCode = logisticsOrderCode;
	}

	public String getPhotoFile1() {
		return photoFile1;
	}

	public void setPhotoFile1(String photoFile1) {
		this.photoFile1 = photoFile1;
	}

	public String getPhotoFile2() {
		return photoFile2;
	}

	public void setPhotoFile2(String photoFile2) {
		this.photoFile2 = photoFile2;
	}

	public String getPhotoFile3() {
		return photoFile3;
	}

	public void setPhotoFile3(String photoFile3) {
		this.photoFile3 = photoFile3;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getReturnDescription() {
		return returnDescription;
	}

	public void setReturnDescription(String returnDescription) {
		this.returnDescription = returnDescription;
	}

	public double getOrderTotalPrice() {
		return orderTotalPrice;
	}

	public void setOrderTotalPrice(double orderTotalPrice) {
		this.orderTotalPrice = orderTotalPrice;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	} 

}
