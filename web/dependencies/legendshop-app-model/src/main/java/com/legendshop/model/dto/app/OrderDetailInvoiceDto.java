package com.legendshop.model.dto.app;

import java.io.Serializable;

public class OrderDetailInvoiceDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String invBankAccout;
	private String invIdfctMark;
	private String invOpenBank;
	private String invRegAddr;
	private String invRegPhone;
	private String title;
	private String titleId;
	private String type;
	private String company;
	private String content;
	private int typeId;
	private String id;
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getInvBankAccout() {
		return invBankAccout;
	}
	public void setInvBankAccout(String invBankAccout) {
		this.invBankAccout = invBankAccout;
	}
	public String getInvIdfctMark() {
		return invIdfctMark;
	}
	public void setInvIdfctMark(String invIdfctMark) {
		this.invIdfctMark = invIdfctMark;
	}
	public String getInvOpenBank() {
		return invOpenBank;
	}
	public void setInvOpenBank(String invOpenBank) {
		this.invOpenBank = invOpenBank;
	}
	public String getInvRegAddr() {
		return invRegAddr;
	}
	public void setInvRegAddr(String invRegAddr) {
		this.invRegAddr = invRegAddr;
	}
	public String getInvRegPhone() {
		return invRegPhone;
	}
	public void setInvRegPhone(String invRegPhone) {
		this.invRegPhone = invRegPhone;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitleId() {
		return titleId;
	}
	public void setTitleId(String titleId) {
		this.titleId = titleId;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

  
	
}
