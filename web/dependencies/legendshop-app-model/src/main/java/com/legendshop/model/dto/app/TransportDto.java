package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.Date;

//运费模板

public class TransportDto implements Serializable{

	private static final long serialVersionUID = -6780023177956831214L;

	private Long id ; 
	
	//记录时间
	private Date recDate ; 
	
	//状态
	private Integer status ; 
	
	//运费名称
	private String transName;
	
	//商家id
	private Long shopId;
	
	//配送时间
	private Integer transTime;
	
	//计价方式
	private String transType;
	
	
	private boolean transMail;
	
	private boolean transExpress;
	
	private boolean transEms;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getTransName() {
		return transName;
	}

	public void setTransName(String transName) {
		this.transName = transName;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getTransTime() {
		return transTime;
	}

	public void setTransTime(Integer transTime) {
		this.transTime = transTime;
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public boolean isTransMail() {
		return transMail;
	}

	public void setTransMail(boolean transMail) {
		this.transMail = transMail;
	}

	public boolean isTransExpress() {
		return transExpress;
	}

	public void setTransExpress(boolean transExpress) {
		this.transExpress = transExpress;
	}

	public boolean isTransEms() {
		return transEms;
	}

	public void setTransEms(boolean transEms) {
		this.transEms = transEms;
	}
	
	

	
}
