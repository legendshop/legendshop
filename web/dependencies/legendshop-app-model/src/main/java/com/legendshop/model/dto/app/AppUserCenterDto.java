package com.legendshop.model.dto.app;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 个人中心首页信息Dto
 */
@ApiModel(value="个人中心首页信息Dto") 
public class AppUserCenterDto implements Serializable{

	private static final long serialVersionUID = -7955702893356854664L;

	@ApiModelProperty(value="用户id") 
	private String userId;
	
	@ApiModelProperty(value="用户名") 
	private String userName;
	
    /** 头像图片**/
	@ApiModelProperty(value="头像图片") 
    private String portraitPic;
	
	/** 用户等级 **/
	@ApiModelProperty(value="用户等级") 
	private String userGradeName;
	
    /**商品收藏数量**/
	@ApiModelProperty(value="商品收藏数量") 
    private Long favoriteLength;
	
	/**店铺收藏数量**/
	@ApiModelProperty(value="店铺收藏数量") 
	private Long favoriteShopLength;
	
    /**订单状态列表**/
	@ApiModelProperty(value="订单状态列表") 
    private List<AppSubCountsDto> subCountsDtoList;
    
    /**可用余额**/
	@ApiModelProperty(value="可用余额") 
    private Double availablePredeposit; //可用余额  Pre-deposit
    
    /** The score. 总积分*/
	@ApiModelProperty(value="总积分") 
	private Integer score;
	
	/**未读消息数量**/
	@ApiModelProperty(value="未读消息数量 [超过100条显示为99+]") 
	private Long messageCount;
	
	/**已完成未评价的订单数量**/
	@ApiModelProperty(value="已完成未评价的订单数量 [超过100条显示为99+]") 
	private Integer unCommCount;
	
	/** 我的收藏统计数量 **/
	@ApiModelProperty(value="我的收藏统计数量 [超过100条显示为99+]") 
	private Long totalFavoriteCount;
	
	/** 我的足迹数量  **/
	@ApiModelProperty(value="我的足迹统计数量 [超过100条显示为99+]") 
	private Long visitLogCount;
	
    public Long getMessageCount() {
		return messageCount;
	}

	public void setMessageCount(Long messageCount) {
		this.messageCount = messageCount;
	}

	public Double getAvailablePredeposit() {
		return availablePredeposit;
	}

	public void setAvailablePredeposit(Double availablePredeposit) {
		this.availablePredeposit = availablePredeposit;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public AppUserCenterDto(){
    	
    }

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPortraitPic() {
		return portraitPic;
	}

	public void setPortraitPic(String portraitPic) {
		this.portraitPic = portraitPic;
	}

	public List<AppSubCountsDto> getSubCountsDtoList() {
		return subCountsDtoList;
	}

	public void setSubCountsDtoList(List<AppSubCountsDto> subCountsDtoList) {
		this.subCountsDtoList = subCountsDtoList;
	}

	public Long getFavoriteLength() {
		return favoriteLength;
	}

	public void setFavoriteLength(Long favoriteLength) {
		this.favoriteLength = favoriteLength;
	}

	public Long getFavoriteShopLength() {
		return favoriteShopLength;
	}

	public void setFavoriteShopLength(Long favoriteShopLength) {
		this.favoriteShopLength = favoriteShopLength;
	}

	public Integer getUnCommCount() {
		return unCommCount;
	}

	public void setUnCommCount(Integer unCommCount) {
		this.unCommCount = unCommCount;
	}

	public String getUserGradeName() {
		return userGradeName;
	}

	public void setUserGradeName(String userGradeName) {
		this.userGradeName = userGradeName;
	}

	public Long getTotalFavoriteCount() {
		return totalFavoriteCount;
	}

	public void setTotalFavoriteCount(Long totalFavoriteCount) {
		this.totalFavoriteCount = totalFavoriteCount;
	}

	public Long getVisitLogCount() {
		return visitLogCount;
	}

	public void setVisitLogCount(Long visitLogCount) {
		this.visitLogCount = visitLogCount;
	}
}
