package com.legendshop.biz.model.dto.productAttribute;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * app 商家品牌dto
 * @author 27158
 */
@ApiModel(value = "app 商家品牌dto")
public class AppBizBrandDto implements Serializable {

	private static final long serialVersionUID = -8062784587183802815L;
	/**品牌ID */
	@ApiModelProperty(value = "品牌ID")
	private Long brandId;

	/** 品牌名称 */
	@ApiModelProperty(value = "品牌名称")
	private String brandName;
}
