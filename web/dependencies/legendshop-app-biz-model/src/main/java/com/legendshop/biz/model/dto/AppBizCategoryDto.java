package com.legendshop.biz.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 *  商家的商品类目Dto
 * @author 27158
 */
@ApiModel(value="商家的商品类目Dto")
public class AppBizCategoryDto {
	private static final long serialVersionUID = -2066089194686839543L;

	/** 产品类目ID */
	@ApiModelProperty(value = "产品类目ID")
	private Long id;

	/** 父节点 */
	@ApiModelProperty(value = "父节点")
	private Long parentId;


	/** 是否有下一个节点 **/
	@ApiModelProperty(value = "是否有下一个节点，1 有，-1 没有")
	private Integer isNext;

	/** 产品类目名称 */
	@ApiModelProperty(value = "产品类目名称")
	private String name;

	/** 类目的显示图片 */
	@ApiModelProperty(value = "类目的显示图片")
	private String pic;

	/** 排序 */
	@ApiModelProperty(value = "排序")
	private Integer seq;

	/** 默认是1，表示正常状态,0为下线状态 */
	@ApiModelProperty(value = "默认是1，表示正常状态,0为下线状态")
	private Integer status;

	/** 类型ID */
	@ApiModelProperty(value = "类型ID")
	private Long shopId;

	/** 记录时间 */
	@ApiModelProperty(value = "记录时间")
	private Date recDate;

	/** 分类层级 */
	@ApiModelProperty(value = "分类层级")
	private Integer grade;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}


	public Integer getIsNext() {
		return isNext;
	}

	public void setIsNext(Integer isNext) {
		this.isNext = isNext;
	}
}
