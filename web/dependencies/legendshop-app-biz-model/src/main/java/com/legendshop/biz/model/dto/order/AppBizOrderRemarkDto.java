package com.legendshop.biz.model.dto.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

@ApiModel("订单备注Dto")
public class AppBizOrderRemarkDto {

	@ApiModelProperty(value = "订单ID")
	private Long subId;

	@ApiModelProperty(value = "订单号")
	private String subNumber;

	@ApiModelProperty(value = "备注")
	private String shopRemark;

	@ApiModelProperty(value = "备注时间")
	private Date shopRemarkDate;

	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public String getShopRemark() {
		return shopRemark;
	}

	public void setShopRemark(String shopRemark) {
		this.shopRemark = shopRemark;
	}

	public Date getShopRemarkDate() {
		return shopRemarkDate;
	}

	public void setShopRemarkDate(Date shopRemarkDate) {
		this.shopRemarkDate = shopRemarkDate;
	}
}
