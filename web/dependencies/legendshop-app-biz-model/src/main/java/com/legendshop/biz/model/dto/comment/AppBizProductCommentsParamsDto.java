/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.biz.model.dto.comment;

import com.legendshop.util.AppUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 商家端App商品评论查询参数
 * @author linzh
 *
 */
@ApiModel(value="商品评论查询参数")
public class AppBizProductCommentsParamsDto implements Serializable {

	/** 商品ID */
	@ApiModelProperty(value = "商品ID")
	private Long prodId;

	/** 当前页码 */
	@ApiModelProperty(value = "当前页码")
	private String curPageNO;
	
	/** 查询条件 全部: all, 好评: good, 中评: medium, 差评: poor, 有图: photo, 追评: append */
	@ApiModelProperty(value = "查询条件 全部: all, 好评: good, 中评: medium, 差评: poor, 有图: photo, 追评: append,待回复: waitReply")
	private String condition;



	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getCurPageNO() {
		if(null == curPageNO){
			curPageNO = "1";
		}
		return curPageNO;
	}

	public void setCurPageNO(String curPageNO) {
		this.curPageNO = curPageNO;
	}

	public String getCondition() {
		if(AppUtils.isBlank(condition)){
			condition = "all";
		}
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}
}
