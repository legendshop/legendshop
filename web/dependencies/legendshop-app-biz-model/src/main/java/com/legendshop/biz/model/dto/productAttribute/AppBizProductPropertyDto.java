package com.legendshop.biz.model.dto.productAttribute;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * 平台商品参数Dto
 * @author 27158
 */
@ApiModel(value = "平台商品参数Dto")
public class AppBizProductPropertyDto implements Serializable {
	private static final long serialVersionUID = 1255683546615848177L;
	/**
	 * 属性ID
	 */
	@ApiModelProperty(value="属性ID")
	private Long propId;

	/**
	 * 属性名称
	 */
	@ApiModelProperty(value="属性名称")
	private String propName;

	/**
	 * 别名
	 */
	@ApiModelProperty(value="属性 别名")
	private String memo;

	/**
	 * 是否必须
	 */
	@ApiModelProperty(value="是否必须")
	private boolean isRequired;

	/**
	 * 是否多选
	 */
	@ApiModelProperty(value="是否多选")
	private boolean isMulti;

	/**
	 * 排序
	 */
	@ApiModelProperty(value="排序")
	private Long sequence;

	/**
	 * 状态
	 */
	@ApiModelProperty(value="状态")
	private Short status;

	/**
	 * 属性类型，1：有图片，0：文字
	 */
	@ApiModelProperty(value="属性类型，1：有图片，0：文字")
	private Short type;

	/**
	 * 修改时间
	 */
	@ApiModelProperty(value="修改时间")
	private Date modifyDate;

	/**
	 * 记录时间
	 */
	@ApiModelProperty(value="记录时间")
	private Date recDate;

	/**
	 * 规则属性：（参数属性：2，销售属性：1, 关键属性：3, 参见ProductPropertyTypeEnum）
	 */
	@ApiModelProperty(value="规则属性：（参数属性：2，销售属性：1, 关键属性：3, 参见ProductPropertyTypeEnum）")
	private Integer isRuleAttributes;

	/**
	 * 是否可以搜索
	 */
	@ApiModelProperty(value="是否可以搜索")
	private boolean isForSearch;

	/**
	 * 是否可以自定义输入属性
	 */
	@ApiModelProperty(value="是否可以自定义输入属性 true 可以 false 不可以")
	private boolean isInputProp;

	/**
	 * 用户名
	 */
	@ApiModelProperty(value="用户名")
	private String userName;

	/**
	 * 已经选择的选项值
	 */
	@ApiModelProperty(value="已经选择的选项值")
	private String selectedValue;

	/**
	 * 属性来源
	 */
	@ApiModelProperty(value="'1:用户自定义，0 系统自带', ")
	private Integer isCustom;

	/**
	 * 属性来源
	 */
	@ApiModelProperty(value="'1:用户自定义，0 系统自带', ")
	private String name;

	public Long getPropId() {
		return propId;
	}

	public void setPropId(Long propId) {
		this.propId = propId;
	}

	public String getPropName() {
		return propName;
	}

	public void setPropName(String propName) {
		this.propName = propName;
	}

	public boolean isRequired() {
		return isRequired;
	}

	public void setRequired(boolean required) {
		isRequired = required;
	}

	public boolean getIsMulti() {
		return isMulti;
	}

	public void setIsMulti(boolean isMulti) {
		isMulti = isMulti;
	}

	public Long getSequence() {
		return sequence;
	}

	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	public Short getType() {
		return type;
	}

	public void setType(Short type) {
		this.type = type;
	}

	public Integer getIsRuleAttributes() {
		return isRuleAttributes;
	}

	public void setIsRuleAttributes(Integer isRuleAttributes) {
		this.isRuleAttributes = isRuleAttributes;
	}

	public boolean getIsForSearch() {
		return isForSearch;
	}

	public void setIsForSearch(boolean forSearch) {
		isForSearch = forSearch;
	}

	public boolean getIsInputProp() {
		return isInputProp;
	}

	public void setInputProp(boolean inputProp) {
		isInputProp = inputProp;
	}

	public String getSelectedValue() {
		return selectedValue;
	}

	public void setSelectedValue(String selectedValue) {
		this.selectedValue = selectedValue;
	}


	public Integer getIsCustom() {
		return isCustom;
	}

	public void setIsCustom(Integer isCustom) {
		this.isCustom = isCustom;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
