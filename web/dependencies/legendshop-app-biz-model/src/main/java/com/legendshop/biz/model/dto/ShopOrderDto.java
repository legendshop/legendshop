/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.biz.model.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.legendshop.model.dto.order.DeliveryDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * 用于封装查询订单数据.
 */
@ApiModel(value="ShopOrderDto") 
public class ShopOrderDto implements Serializable{

	private static final long serialVersionUID = -2864810122569245142L;

	/** 订单ID **/
	@ApiModelProperty(value="订单ID")
	private Long subId;

	/** 商品名称 **/
	@ApiModelProperty(value="商品名称")
	private String prodName;
	
	/** 用户ID **/
	@ApiModelProperty(value="用户ID")
	private String userId;

	/** 用户名称 **/
	@ApiModelProperty(value="用户名称")
	private String userName;
	
	/** 下订单的时间 **/
	@ApiModelProperty(value="下订单的时间")
	private Date subDate;

	/** 支付的时间  **/
	@ApiModelProperty(value="支付的时间")
	private Date payDate;
	
	/** 更新时间  **/
	@ApiModelProperty(value="更新时间")
	private Date updateDate;

	/** 订单流水号 **/
	@ApiModelProperty(value="订单流水号")
	private String subNumber;
	
	/** 订单商品原价  **/
	@ApiModelProperty(value="订单商品原价")
	private Double total;

	/** 订单商品实际价格(运费 折扣 促销) **/
	@ApiModelProperty(value="订单商品实际价格(运费 折扣 促销)")
	private Double actualTotal;
	
	/** 支付ID **/
	@ApiModelProperty(value="支付ID")
	private Long payId;

	/** 付款类型ID **/
	@ApiModelProperty(value="付款类型ID")
	private String payTypeId;

	/** 订单状态 **/
	@ApiModelProperty(value="订单状态")
	private Integer status;

	/** 支付方式名称 **/
	@ApiModelProperty(value="支付方式名称")
	private String payTypeName;
	
	/** 其他备注 **/
	@ApiModelProperty(value="其他备注")
	private String other;

	/** 商城ID **/
	@ApiModelProperty(value="商城ID")
	private Long shopId;
	
	/** 商城名称 **/
	@ApiModelProperty(value="订单ID")
	private String shopName;
	
	/** 配送类型  **/
	@ApiModelProperty(value="配送类型")
	private String dvyType;
	
	/** 配送方式ID **/
	@ApiModelProperty(value="配送方式ID")
	private Long dvyTypeId;
	
	/** 物流单号 **/
	@ApiModelProperty(value="物流单号")
	private String dvyFlowId;
	
	/** 发票单号 **/
	@ApiModelProperty(value="发票单号")
	private Long invoiceSubId;
	
	/** 物流费用 **/
	@ApiModelProperty(value="物流费用")
	private Double freightAmount;
	
	/** 给卖家留言 **/
	@ApiModelProperty(value="给卖家留言")
	private String orderRemark;
	
	/** 用户订单地址ID **/
	@ApiModelProperty(value="用户订单地址ID")
	private Long addrOrderId;
	
	/** 是否需要发票(0:不需要;1:需要) **/
	@ApiModelProperty(value="是否需要发票(0:不需要;1:需要)")
	private Integer isNeedInvoice;
	
	/** 支付方式(1:货到付款;2:在线支付) **/
	@ApiModelProperty(value="支付方式(1:货到付款;2:在线支付)")
	private Integer payManner;
	
	/** 物流重量(千克) **/
	@ApiModelProperty(value="物流重量(千克)")
	private Double weight;
	
	/** 物流体积(立方米) **/
	@ApiModelProperty(value="物流体积(立方米)")
	private Double volume;
	
	/** 优惠总金额 **/
	@ApiModelProperty(value="优惠总金额")
	private Double discountPrice;
	
	/** 分销的佣金总额 **/
	@ApiModelProperty(value="分销的佣金总额 ")
	private Double distCommisAmount;
	
	/** 订单商品总数 **/
	@ApiModelProperty(value="订单商品总数")
	private Integer productNums;
	
	/** 发货时间 */
	@ApiModelProperty(value="发货时间")
	private Date dvyDate;
	
	/** 完成时间/确认收货时间  **/
	@ApiModelProperty(value="完成时间/确认收货时间")
	private Date finallyDate;
	
	/** 是否货到付款 **/
	@ApiModelProperty(value="是否货到付款")
	private Integer isCod;
	
	/** 是否已经支付 **/
	@ApiModelProperty(value="是否已经支付")
	private Integer isPayed;
	
	/** 用户订单删除状态，0：没有删除， 1：回收站， 2：永久删除 **/
	@ApiModelProperty(value="用户订单删除状态，0：没有删除， 1：回收站， 2：永久删除")
	private Integer deleteStatus;
	
	/** 订单项列表 **/
	@ApiModelProperty(value="订单项列表 ")
	private List<ShopOrderItemDto> subItems;
	
	/** 用户收货地址  **/
	@ApiModelProperty(value="用户收货地址",reference="商家收货地址Dto")
	private ShopAddressDto addressDto;
	
	/** 物流信息  **/
	@ApiModelProperty(value="物流信息")
	private DeliveryDto deliveryDto;
	
	/**订单类型**/
	@ApiModelProperty(value="订单类型")
	private String subType;

	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getSubDate() {
		return subDate;
	}

	public void setSubDate(Date subDate) {
		this.subDate = subDate;
	}

	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getActualTotal() {
		return actualTotal;
	}

	public void setActualTotal(Double actualTotal) {
		this.actualTotal = actualTotal;
	}

	public Long getPayId() {
		return payId;
	}

	public void setPayId(Long payId) {
		this.payId = payId;
	}

	public String getPayTypeId() {
		return payTypeId;
	}

	public void setPayTypeId(String payTypeId) {
		this.payTypeId = payTypeId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getPayTypeName() {
		return payTypeName;
	}

	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getDvyType() {
		return dvyType;
	}

	public void setDvyType(String dvyType) {
		this.dvyType = dvyType;
	}

	public Long getDvyTypeId() {
		return dvyTypeId;
	}

	public void setDvyTypeId(Long dvyTypeId) {
		this.dvyTypeId = dvyTypeId;
	}

	public String getDvyFlowId() {
		return dvyFlowId;
	}

	public void setDvyFlowId(String dvyFlowId) {
		this.dvyFlowId = dvyFlowId;
	}

	public Long getInvoiceSubId() {
		return invoiceSubId;
	}

	public void setInvoiceSubId(Long invoiceSubId) {
		this.invoiceSubId = invoiceSubId;
	}

	public Double getFreightAmount() {
		return freightAmount;
	}

	public void setFreightAmount(Double freightAmount) {
		this.freightAmount = freightAmount;
	}

	public String getOrderRemark() {
		return orderRemark;
	}

	public void setOrderRemark(String orderRemark) {
		this.orderRemark = orderRemark;
	}

	public Long getAddrOrderId() {
		return addrOrderId;
	}

	public void setAddrOrderId(Long addrOrderId) {
		this.addrOrderId = addrOrderId;
	}

	public Integer getIsNeedInvoice() {
		return isNeedInvoice;
	}

	public void setIsNeedInvoice(Integer isNeedInvoice) {
		this.isNeedInvoice = isNeedInvoice;
	}

	public Integer getPayManner() {
		return payManner;
	}

	public void setPayManner(Integer payManner) {
		this.payManner = payManner;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Double getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}

	public Double getDistCommisAmount() {
		return distCommisAmount;
	}

	public void setDistCommisAmount(Double distCommisAmount) {
		this.distCommisAmount = distCommisAmount;
	}

	public Integer getProductNums() {
		return productNums;
	}

	public void setProductNums(Integer productNums) {
		this.productNums = productNums;
	}

	public Date getDvyDate() {
		return dvyDate;
	}

	public void setDvyDate(Date dvyDate) {
		this.dvyDate = dvyDate;
	}

	public Date getFinallyDate() {
		return finallyDate;
	}

	public void setFinallyDate(Date finallyDate) {
		this.finallyDate = finallyDate;
	}

	public Integer getIsCod() {
		return isCod;
	}

	public void setIsCod(Integer isCod) {
		this.isCod = isCod;
	}

	public Integer getIsPayed() {
		return isPayed;
	}

	public void setIsPayed(Integer isPayed) {
		this.isPayed = isPayed;
	}

	public Integer getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(Integer deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public List<ShopOrderItemDto> getSubItems() {
		return subItems;
	}

	public void setSubItems(List<ShopOrderItemDto> subItems) {
		this.subItems = subItems;
	}

	public ShopAddressDto getAddressDto() {
		return addressDto;
	}

	public void setAddressDto(ShopAddressDto addressDto) {
		this.addressDto = addressDto;
	}

	public DeliveryDto getDeliveryDto() {
		return deliveryDto;
	}

	public void setDeliveryDto(DeliveryDto deliveryDto) {
		this.deliveryDto = deliveryDto;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}
	
	
}
