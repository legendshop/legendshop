package com.legendshop.biz.model.dto.productAttribute;

import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.dto.ProductDto;
import com.legendshop.model.dto.TreeNode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 *商家修改商品Dto
 */
@ApiModel("商家修改商品Dto")
public class AppBizUpdateProdDto {

	@ApiModelProperty("平台类目id")
	private Long publishCategoryId;

	@ApiModelProperty("商品Dto")
	private ProductDto productDto;

	@ApiModelProperty("规格属性Dto")
	private AppBizPublishProductDto appBizPublishProductDto;

	@ApiModelProperty("平台类目id")
    private List<AppBizTreeNode> treeNodes;

	public Long getPublishCategoryId() {
		return publishCategoryId;
	}

	public void setPublishCategoryId(Long publishCategoryId) {
		this.publishCategoryId = publishCategoryId;
	}

	public ProductDto getProductDto() {
		return productDto;
	}

	public void setProductDto(ProductDto productDto) {
		this.productDto = productDto;
	}

	public AppBizPublishProductDto getAppBizPublishProductDto() {
		return appBizPublishProductDto;
	}

	public void setAppBizPublishProductDto(AppBizPublishProductDto appBizPublishProductDto) {
		this.appBizPublishProductDto = appBizPublishProductDto;
	}

	public List<AppBizTreeNode> getTreeNodes() {
		return treeNodes;
	}

	public void setTreeNodes(List<AppBizTreeNode> treeNodes) {
		this.treeNodes = treeNodes;
	}
}
