/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.biz.model.dto;

import java.io.Serializable;
import java.util.List;

import com.legendshop.model.entity.ProductReply;
/**
 * 产品回复列表Dto.
 */

public class ProductReplyListDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6096991135857312669L;

	private  List<ProductReply> productReplyList;
	
	private Long productReplyCount;

	public List<ProductReply> getProductReplyList() {
		return productReplyList;
	}

	public void setProductReplyList(List<ProductReply> productReplyList) {
		this.productReplyList = productReplyList;
	}

	public Long getProductReplyCount() {
		return productReplyCount;
	}

	public void setProductReplyCount(Long productReplyCount) {
		this.productReplyCount = productReplyCount;
	}
	
}