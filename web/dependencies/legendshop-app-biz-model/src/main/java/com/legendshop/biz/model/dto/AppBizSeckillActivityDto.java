package com.legendshop.biz.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 27158
 */
@ApiModel(value = "秒杀活动Dto")
public class AppBizSeckillActivityDto  implements Serializable {

	private static final long serialVersionUID = -5488304913161671354L;

	/** 活动ID */
	@ApiModelProperty(value = "活动ID")
	private Long id;

	/** 用户编号 */
	@ApiModelProperty(value = "用户编号")
	private String userId;

	/** 商家编号 */
	@ApiModelProperty(value = "商家ID")
	private Long shopId;

	/** 秒杀活动店铺名称 */
	@ApiModelProperty(value = "秒杀活动店铺名称")
	private String shopName;

	/** 活动名称 */
	@ApiModelProperty(value = "活动名称",required = true)
	private String seckillTitle;

	/** 开始时间 */
	@ApiModelProperty(value = "开始时间",required = true)
	private Date startTime;

	/** 结束时间 */
	@ApiModelProperty(value = "结束时间",required = true)
	private Date endTime;

	/** 活动状态 */
	@ApiModelProperty(value = "活动状态 未通过 -2 审核中 -1  下线（审核通过）0 上线（审核通过）1 秒杀活动结束释放sku 2 转换订单结束 3 秒杀活动支付结束 4")
	private Long status;

	/** 秒杀活动描述 */
	@ApiModelProperty(value = "秒杀活动描述")
	private String seckillDesc;

	/** 秒杀活动图片 */
	@ApiModelProperty(value = "秒杀活动图片",required = true)
	private String seckillPic;

	/** 秒杀活动摘要 */
	@ApiModelProperty(value = "秒杀活动摘要")
	private String seckillBrief;

	/** 秒杀活动价格 */
	@ApiModelProperty(value = "秒杀活动价格",required = true)
	private BigDecimal seckillLowPrice;

	/** 审核意见 */
	@ApiModelProperty(value = "审核意见")
	private String auditOpinion;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getSeckillTitle() {
		return seckillTitle;
	}

	public void setSeckillTitle(String seckillTitle) {
		this.seckillTitle = seckillTitle;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getSeckillDesc() {
		return seckillDesc;
	}

	public void setSeckillDesc(String seckillDesc) {
		this.seckillDesc = seckillDesc;
	}

	public String getSeckillPic() {
		return seckillPic;
	}

	public void setSeckillPic(String seckillPic) {
		this.seckillPic = seckillPic;
	}

	public String getSeckillBrief() {
		return seckillBrief;
	}

	public void setSeckillBrief(String seckillBrief) {
		this.seckillBrief = seckillBrief;
	}

	public BigDecimal getSeckillLowPrice() {
		return seckillLowPrice;
	}

	public void setSeckillLowPrice(BigDecimal seckillLowPrice) {
		this.seckillLowPrice = seckillLowPrice;
	}

	public String getAuditOpinion() {
		return auditOpinion;
	}

	public void setAuditOpinion(String auditOpinion) {
		this.auditOpinion = auditOpinion;
	}
}
