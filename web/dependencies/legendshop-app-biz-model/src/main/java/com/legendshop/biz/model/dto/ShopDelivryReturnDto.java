/**
 * 
 */
package com.legendshop.biz.model.dto;


/**
 * @author liyuan
 * 查看快递返回
 */
public class ShopDelivryReturnDto {
    
	/**物流单号**/
	private String dvyFlowId;
	
	/**物流公司**/
	private String deyName;
	
	/**物流信息**/
	private String logs;
	
	public String getLogs() {
		return logs;
	}
	public void setLogs(String logs) {
		this.logs = logs;
	}



	public String getDvyFlowId() {
		return dvyFlowId;
	}



	public void setDvyFlowId(String dvyFlowId) {
		this.dvyFlowId = dvyFlowId;
	}



	public String getDeyName() {
		return deyName;
	}



	public void setDeyName(String deyName) {
		this.deyName = deyName;
	}
}
