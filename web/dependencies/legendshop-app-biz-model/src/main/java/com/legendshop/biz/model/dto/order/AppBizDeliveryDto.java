package com.legendshop.biz.model.dto.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel("物流信息Dto")
public class AppBizDeliveryDto implements Serializable {

	private static final long serialVersionUID = -311964482554727044L;

	/** 配送方式ID **/
	@ApiModelProperty(value = "配送方式ID")
	private Long dvyTypeId;
	
	/** 物流单号 **/
	@ApiModelProperty(value = "物流单号")
	private String dvyFlowId;
	
	/** 物流名称 **/
	@ApiModelProperty(value = "物流名称")
	private String delName;
	
	/** 邮编 **/
	@ApiModelProperty(value = "邮编")
	private String delUrl;
	
	/** 物流查询接口 **/
	@ApiModelProperty(value = "物流查询接口")
	private String queryUrl;

	public String getDelName() {
		return delName;
	}

	public void setDelName(String delName) {
		this.delName = delName;
	}

	public String getDelUrl() {
		return delUrl;
	}

	public void setDelUrl(String delUrl) {
		this.delUrl = delUrl;
	}

	public String getQueryUrl() {
		return queryUrl;
	}

	public void setQueryUrl(String queryUrl) {
		this.queryUrl = queryUrl;
	}

	public Long getDvyTypeId() {
		return dvyTypeId;
	}

	public void setDvyTypeId(Long dvyTypeId) {
		this.dvyTypeId = dvyTypeId;
	}

	public String getDvyFlowId() {
		return dvyFlowId;
	}

	public void setDvyFlowId(String dvyFlowId) {
		this.dvyFlowId = dvyFlowId;
	}
	
}
