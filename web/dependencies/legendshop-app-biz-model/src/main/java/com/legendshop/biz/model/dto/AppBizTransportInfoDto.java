package com.legendshop.biz.model.dto;

import com.legendshop.dao.persistence.*;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.entity.Transfee;
import com.legendshop.util.AppUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 运费模板
 */
@ApiModel("运费模板")
public class AppBizTransportInfoDto{

	@ApiModelProperty("ID")
	private Long id ;

	@ApiModelProperty("记录时间")
	private Date recDate ;

	@ApiModelProperty("状态")
	private Integer status ;

	@ApiModelProperty("运费名称")
	private String transName;

	@ApiModelProperty("商家id")
	private Long shopId;
	
	@ApiModelProperty("配送时间")
	private Integer transTime;
	
	@ApiModelProperty("计价方式 1按件数 2按重量 3按体积")
	private String transType;

	@ApiModelProperty("是否区域限售  1.支持  0.不支持")
	private int isRegionalSales;

	@ApiModelProperty("配送地址集合")
	private List<Transfee> expressList = new ArrayList<Transfee>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getTransName() {
		return transName;
	}

	public void setTransName(String transName) {
		this.transName = transName;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getTransTime() {
		return transTime;
	}

	public void setTransTime(Integer transTime) {
		this.transTime = transTime;
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public int getIsRegionalSales() {
		return isRegionalSales;
	}

	public void setIsRegionalSales(int isRegionalSales) {
		this.isRegionalSales = isRegionalSales;
	}


	public List<Transfee> getExpressList() {
		return expressList;
	}

	public void setExpressList(List<Transfee> expressList) {
		this.expressList = expressList;
	}


}
