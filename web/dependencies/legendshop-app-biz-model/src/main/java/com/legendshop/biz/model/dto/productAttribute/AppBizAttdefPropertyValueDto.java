package com.legendshop.biz.model.dto.productAttribute;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 商品属性值Dto
 * @author 27158
 */
@ApiModel(value = "商品属性值Dto")
public class AppBizAttdefPropertyValueDto  implements Serializable {

	private static final long serialVersionUID = 5245649588447180687L;

	/**
	 * 属性值ID
	 */
	@ApiModelProperty(value="属性值ID")
	private Long valueId;

	/**
	 * 属性ID
	 */
	@ApiModelProperty(value="属性ID")
	private Long propId;

	/**
	 * 属性值名称
	 */
	@ApiModelProperty(value="属性值名称")
	private String name;

	/**
	 * 属性值名称 别名
	 */
	@ApiModelProperty(value="属性值名称 别名")
	private String alias;

	/**
	 * 图片路径
	 */
	@ApiModelProperty(value="图片路径")
	private String pic;

	/**
	 * 是否被sku选中
	 */
	@ApiModelProperty(value="是否被sku选中")
	private boolean isSelected;

	public Long getValueId() {
		return valueId;
	}

	public void setValueId(Long valueId) {
		this.valueId = valueId;
	}

	public Long getPropId() {
		return propId;
	}

	public void setPropId(Long propId) {
		this.propId = propId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean selected) {
		isSelected = selected;
	}
}

