/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.biz.model.dto.order;

import com.legendshop.model.entity.InvoiceSub;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * App用于封装查询订单数据.
 */

@ApiModel("用户订单列表Dto")
public class AppBizOrderDetailDto implements Serializable{

	private static final long serialVersionUID = -2864810122569245142L;

	/** 订单ID **/
	@ApiModelProperty(value = "订单ID")
	private Long subId;

	/** 商品名称 **/
	@ApiModelProperty(value = "商品名称")
	private String prodName;

	/** 用户ID **/
	@ApiModelProperty(value = "用户ID")
	private String userId;

	/** 用户名称 **/
	@ApiModelProperty(value = "用户名称 ")
	private String userName;

	/** 下订单的时间 **/
	@ApiModelProperty(value = "下订单的时间 ")
	private Date subDate;

	/** 支付的时间  **/
	@ApiModelProperty(value = "支付的时间")
	private Date payDate;

	/** 更新时间  **/
	@ApiModelProperty(value = "更新时间 ")
	private Date updateDate;

	/** 订单号 **/
	@ApiModelProperty(value = "订单号")
	private String subNumber;

	/** 订单商品原价  **/
	@ApiModelProperty(value = "订单商品原价")
	private Double total;

	/** 订单商品实际价格(运费 折扣 促销) **/
	@ApiModelProperty(value = "订单商品实际价格(运费 折扣 促销)")
	private Double actualTotal;

	/** 支付ID **/
	@ApiModelProperty(value = "支付ID")
	private Long payId;

	/** 付款类型ID **/
	@ApiModelProperty(value = "付款类型ID")
	private String payTypeId;

	/** 订单状态 **/
	@ApiModelProperty(value = "订单状态[待付款: 1 ，待发货：2 ，待收货：3 ，已完成：4 ， 已取消：5]")
	private Integer status;

	/** 支付方式名称 **/
	@ApiModelProperty(value = "支付方式名称 ")
	private String payTypeName;

	/** 其他备注 **/
	@ApiModelProperty(value = "其他备注")
	private String other;

	/** 商城ID **/
	@ApiModelProperty(value = "商城ID")
	private Long shopId;

	/** 商城名称 **/
	@ApiModelProperty(value = "商城名称")
	private String shopName;

	/** 配送类型  **/
	@ApiModelProperty(value = "配送类型")
	private String dvyType;

	/** 配送方式ID **/
	@ApiModelProperty(value = "配送方式ID")
	private Long dvyTypeId;

	/** 物流单号 **/
	@ApiModelProperty(value = "物流单号 ")
	private String dvyFlowId;

	/** 发票单号 **/
	@ApiModelProperty(value = "发票单号")
	private Long invoiceSubId;

	/** 物流费用 **/
	@ApiModelProperty(value = "物流费用")
	private Double freightAmount;

	/** 给卖家留言 **/
	@ApiModelProperty(value = "给卖家留言")
	private String orderRemark;

	/** 用户订单地址ID **/
	@ApiModelProperty(value = "用户订单地址ID")
	private Long addrOrderId;

	/** 是否需要发票(0:不需要;1:需要) **/
	@ApiModelProperty(value = "是否需要发票(0:不需要;1:需要)")
	private Integer isNeedInvoice;

	/** 支付方式(1:货到付款;2:在线支付) **/
	@ApiModelProperty(value = "支付方式(1:货到付款;2:在线支付)")
	private Integer payManner;

	/** 物流重量(千克) **/
	@ApiModelProperty(value = "物流重量(千克)")
	private Double weight;

	/** 物流体积(立方米) **/
	@ApiModelProperty(value = "物流体积(立方米)")
	private Double volume;

	/** 促销优惠总金额 **/
	@ApiModelProperty(value = "促销优惠总金额")
	private Double discountPrice;

	/** 使用红包支付金额 */
	@ApiModelProperty(value="使用红包支付金额")
	private Double redpackOffPrice;

	/** 使用优惠券支付金额 */
	@ApiModelProperty(value="使用优惠券支付金额")
	private Double couponOffPrice;

	/** 分销的佣金总额 **/
	@ApiModelProperty(value = "分销的佣金总额")
	private Double distCommisAmount;

	/** 订单商品总数 **/
	@ApiModelProperty(value = "订单商品总数")
	private Integer productNums;

	/** 发货时间 */
	@ApiModelProperty(value = "发货时间")
	private Date dvyDate;

	/** 完成时间/确认收货时间  **/
	@ApiModelProperty(value = "完成时间/确认收货时间 ")
	private Date finallyDate;

	/** 是否货到付款 **/
	@ApiModelProperty(value = "是否货到付款")
	private Integer isCod;

	/** 是否已经支付 **/
	@ApiModelProperty(value = "是否已经支付")
	private Integer isPayed;

	/** 订单类型[普通订单：NORMAL，秒杀订单：SECKILL  门店订单：SHOP_STORE 预售订单：PRE_SELL 团购：GROUP 拼团：MERGE_GROUP 拍卖；AUCTIONS  示例：subType=“NORMAL”] **/
	@ApiModelProperty(value = "订单类型[普通订单：NORMAL，秒杀订单：SECKILL  门店订单：SHOP_STORE 预售订单：PRE_SELL 团购：GROUP 拼团：MERGE_GROUP 拍卖；AUCTIONS  示例：subType=“NORMAL”]")
	private String subType;

	/** 用户订单删除状态，0：没有删除， 1：回收站， 2：永久删除 **/
	@ApiModelProperty(value = "用户订单删除状态，0：没有删除， 1：回收站， 2：永久删除")
	private Integer deleteStatus;

	/** 是否提醒过发货  */
	@ApiModelProperty(value = "是否提醒过发货")
	private boolean remindDelivery;

	/** 提醒发货时间  */
	@ApiModelProperty(value = "提醒发货时间")
	private Date remindDeliveryTime;

	/** 订单项列表 **/
	@ApiModelProperty(value = "订单项列表")
	private List<AppBizOrderItemDto> subOrderItemDtos;

	/** 用户收货地址  **/
	@ApiModelProperty(value = "用户收货地址")
	private AppBizAddressDto addressDto;

	/** 物流信息  **/
	@ApiModelProperty(value = "物流信息")
	private AppBizDeliveryDto deliveryDto;

	/** 退款状态 0:默认,1:在处理,2:处理完成 */
	@ApiModelProperty(value = "退款状态 0:没有退款退货状态,1:退款退货中,2:退款退货处理完成")
	private Long refundState;

	@ApiModelProperty(value="用户的订单发票")
	private InvoiceSub invoiceSub;

	@ApiModelProperty(value="用于倒计时的时间,待付款时是付款截止时间， 待成团是成团截止倒计时, 待收货时是确认收货截止时间")
	private Date countDownTime;

	@ApiModelProperty("活动ID, 如拼团活动ID")
	private Long activeId;

	@ApiModelProperty(value="团购状态")
	private Integer groupStatus;

	@ApiModelProperty(value="拼团状态")
	private Integer mergeGroupStatus;

	@ApiModelProperty("拼团活动, 已开团的团编号")
	private String addNumber;

	@ApiModelProperty("拼团成团人数")
	private Integer mergerGroupPeopleNum;

	@ApiModelProperty("已参团人数")
	private Integer mergerGroupAlreadyAddNum;

	@ApiModelProperty("门店订单提货码")
	private String dlyoPickupCode;

	@ApiModelProperty("门店信息")
	private AppBizStoreDto store;

	@ApiModelProperty(value="订单已调整的金额[负数为减，正数为加需要添加'+'显示]")
	private Double changedPrice;

	//预售订单字段
	/** 支付方式,0:全额,1:定金 */
	@ApiModelProperty(value="支付方式  0:全额,1:定金")
	private Integer payPctType;

	/** 定金金额 */
	@ApiModelProperty(value="定金金额")
	private java.math.BigDecimal preDepositPrice;

	/** 尾款金额 */
	@ApiModelProperty(value="尾款金额 ")
	private java.math.BigDecimal finalPrice;

	/** 尾款支付开始时间 */
	@ApiModelProperty(value="尾款支付开始时间 ")
	private Date finalMStart;

	/** 尾款支付结束时间 */
	@ApiModelProperty(value="尾款支付结束时间 ")
	private Date finalMEnd;

	/** 定金支付名称 */
	@ApiModelProperty(value="定金支付名称 ")
	private String depositPayName;

	/** 定金支付流水号 */
	@ApiModelProperty(value="定金支付流水号 ")
	private String depositTradeNo;

	/** 定金支付时间 */
	@ApiModelProperty(value="定金支付时间 ")
	private Date depositPayTime;

	/** 是否支付定金 */
	@ApiModelProperty(value="是否支付定金 ")
	private Integer isPayDeposit;

	/** 尾款支付名称 */
	@ApiModelProperty(value="尾款支付名称 ")
	private String finalPayName;

	/** 尾款支付时间 */
	@ApiModelProperty(value="尾款支付时间  ")
	private Date finalPayTime;

	/** 尾款流水号 */
	@ApiModelProperty(value="尾款流水号  ")
	private String finalTradeNo;

	/** 是否支付尾款 */
	@ApiModelProperty(value="是否支付尾款")
	private Integer isPayFinal;

	/** 订单自动取消剩余时间（分钟） */
	@ApiModelProperty(value="订单自动取消剩余时间（分钟）")
	private Integer orderCancelMinute;

	/**是否已经备注**/
	@ApiModelProperty(value="是否已经备注")
	private Boolean isShopRemarked;

	/**退款id**/
	@ApiModelProperty(value="退款id")
	private Long refundId;


	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getSubDate() {
		return subDate;
	}

	public void setSubDate(Date subDate) {
		this.subDate = subDate;
	}

	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getActualTotal() {
		return actualTotal;
	}

	public void setActualTotal(Double actualTotal) {
		this.actualTotal = actualTotal;
	}

	public Long getPayId() {
		return payId;
	}

	public void setPayId(Long payId) {
		this.payId = payId;
	}

	public String getPayTypeId() {
		return payTypeId;
	}

	public void setPayTypeId(String payTypeId) {
		this.payTypeId = payTypeId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getPayTypeName() {
		return payTypeName;
	}

	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getDvyType() {
		return dvyType;
	}

	public void setDvyType(String dvyType) {
		this.dvyType = dvyType;
	}

	public Long getDvyTypeId() {
		return dvyTypeId;
	}

	public void setDvyTypeId(Long dvyTypeId) {
		this.dvyTypeId = dvyTypeId;
	}

	public String getDvyFlowId() {
		return dvyFlowId;
	}

	public void setDvyFlowId(String dvyFlowId) {
		this.dvyFlowId = dvyFlowId;
	}

	public Long getInvoiceSubId() {
		return invoiceSubId;
	}

	public void setInvoiceSubId(Long invoiceSubId) {
		this.invoiceSubId = invoiceSubId;
	}

	public Double getFreightAmount() {
		return freightAmount;
	}

	public void setFreightAmount(Double freightAmount) {
		this.freightAmount = freightAmount;
	}

	public String getOrderRemark() {
		return orderRemark;
	}

	public void setOrderRemark(String orderRemark) {
		this.orderRemark = orderRemark;
	}

	public Long getAddrOrderId() {
		return addrOrderId;
	}

	public void setAddrOrderId(Long addrOrderId) {
		this.addrOrderId = addrOrderId;
	}

	public Integer getIsNeedInvoice() {
		return isNeedInvoice;
	}

	public void setIsNeedInvoice(Integer isNeedInvoice) {
		this.isNeedInvoice = isNeedInvoice;
	}

	public Integer getPayManner() {
		return payManner;
	}

	public void setPayManner(Integer payManner) {
		this.payManner = payManner;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Double getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}

	public Double getDistCommisAmount() {
		return distCommisAmount;
	}

	public void setDistCommisAmount(Double distCommisAmount) {
		this.distCommisAmount = distCommisAmount;
	}

	public Double getRedpackOffPrice() {
		return redpackOffPrice;
	}

	public void setRedpackOffPrice(Double redpackOffPrice) {
		this.redpackOffPrice = redpackOffPrice;
	}

	public Double getCouponOffPrice() {
		return couponOffPrice;
	}

	public void setCouponOffPrice(Double couponOffPrice) {
		this.couponOffPrice = couponOffPrice;
	}

	public Integer getProductNums() {
		return productNums;
	}

	public void setProductNums(Integer productNums) {
		this.productNums = productNums;
	}

	public Date getDvyDate() {
		return dvyDate;
	}

	public void setDvyDate(Date dvyDate) {
		this.dvyDate = dvyDate;
	}

	public Date getFinallyDate() {
		return finallyDate;
	}

	public void setFinallyDate(Date finallyDate) {
		this.finallyDate = finallyDate;
	}

	public Integer getIsCod() {
		return isCod;
	}

	public void setIsCod(Integer isCod) {
		this.isCod = isCod;
	}

	public Integer getIsPayed() {
		return isPayed;
	}

	public void setIsPayed(Integer isPayed) {
		this.isPayed = isPayed;
	}

	public Integer getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(Integer deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public boolean isRemindDelivery() {
		return remindDelivery;
	}

	public void setRemindDelivery(boolean remindDelivery) {
		this.remindDelivery = remindDelivery;
	}

	public Date getRemindDeliveryTime() {
		return remindDeliveryTime;
	}

	public void setRemindDeliveryTime(Date remindDeliveryTime) {
		this.remindDeliveryTime = remindDeliveryTime;
	}

	public Long getRefundState() {
		return refundState;
	}

	public void setRefundState(Long refundState) {
		this.refundState = refundState;
	}

	public InvoiceSub getInvoiceSub() {
		return invoiceSub;
	}

	public void setInvoiceSub(InvoiceSub invoiceSub) {
		this.invoiceSub = invoiceSub;
	}

	public Date getCountDownTime() {
		return countDownTime;
	}

	public void setCountDownTime(Date countDownTime) {
		this.countDownTime = countDownTime;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public String getAddNumber() {
		return addNumber;
	}

	public void setAddNumber(String addNumber) {
		this.addNumber = addNumber;
	}

	public Integer getGroupStatus() {
		return groupStatus;
	}

	public void setGroupStatus(Integer groupStatus) {
		this.groupStatus = groupStatus;
	}

	public Integer getMergeGroupStatus() {
		return mergeGroupStatus;
	}

	public void setMergeGroupStatus(Integer mergeGroupStatus) {
		this.mergeGroupStatus = mergeGroupStatus;
	}

	public Integer getMergerGroupPeopleNum() {
		return mergerGroupPeopleNum;
	}

	public void setMergerGroupPeopleNum(Integer mergerGroupPeopleNum) {
		this.mergerGroupPeopleNum = mergerGroupPeopleNum;
	}

	public Integer getMergerGroupAlreadyAddNum() {
		return mergerGroupAlreadyAddNum;
	}

	public void setMergerGroupAlreadyAddNum(Integer mergerGroupAlreadyAddNum) {
		this.mergerGroupAlreadyAddNum = mergerGroupAlreadyAddNum;
	}

	public String getDlyoPickupCode() {
		return dlyoPickupCode;
	}

	public void setDlyoPickupCode(String dlyoPickupCode) {
		this.dlyoPickupCode = dlyoPickupCode;
	}

	public Double getChangedPrice() {
		return changedPrice;
	}

	public void setChangedPrice(Double changedPrice) {
		this.changedPrice = changedPrice;
	}

	public Integer getPayPctType() {
		return payPctType;
	}

	public void setPayPctType(Integer payPctType) {
		this.payPctType = payPctType;
	}

	public java.math.BigDecimal getPreDepositPrice() {
		return preDepositPrice;
	}

	public void setPreDepositPrice(java.math.BigDecimal preDepositPrice) {
		this.preDepositPrice = preDepositPrice;
	}

	public java.math.BigDecimal getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(java.math.BigDecimal finalPrice) {
		this.finalPrice = finalPrice;
	}

	public Date getFinalMStart() {
		return finalMStart;
	}

	public void setFinalMStart(Date finalMStart) {
		this.finalMStart = finalMStart;
	}

	public Date getFinalMEnd() {
		return finalMEnd;
	}

	public void setFinalMEnd(Date finalMEnd) {
		this.finalMEnd = finalMEnd;
	}

	public String getDepositPayName() {
		return depositPayName;
	}

	public void setDepositPayName(String depositPayName) {
		this.depositPayName = depositPayName;
	}

	public String getDepositTradeNo() {
		return depositTradeNo;
	}

	public void setDepositTradeNo(String depositTradeNo) {
		this.depositTradeNo = depositTradeNo;
	}

	public Date getDepositPayTime() {
		return depositPayTime;
	}

	public void setDepositPayTime(Date depositPayTime) {
		this.depositPayTime = depositPayTime;
	}

	public Integer getIsPayDeposit() {
		return isPayDeposit;
	}

	public void setIsPayDeposit(Integer isPayDeposit) {
		this.isPayDeposit = isPayDeposit;
	}

	public String getFinalPayName() {
		return finalPayName;
	}

	public void setFinalPayName(String finalPayName) {
		this.finalPayName = finalPayName;
	}

	public Date getFinalPayTime() {
		return finalPayTime;
	}

	public void setFinalPayTime(Date finalPayTime) {
		this.finalPayTime = finalPayTime;
	}

	public String getFinalTradeNo() {
		return finalTradeNo;
	}

	public void setFinalTradeNo(String finalTradeNo) {
		this.finalTradeNo = finalTradeNo;
	}

	public Integer getIsPayFinal() {
		return isPayFinal;
	}

	public void setIsPayFinal(Integer isPayFinal) {
		this.isPayFinal = isPayFinal;
	}

	public Integer getOrderCancelMinute() {
		return orderCancelMinute;
	}

	public void setOrderCancelMinute(Integer orderCancelMinute) {
		this.orderCancelMinute = orderCancelMinute;
	}

	public List<AppBizOrderItemDto> getSubOrderItemDtos() {
		return subOrderItemDtos;
	}

	public void setSubOrderItemDtos(List<AppBizOrderItemDto> subOrderItemDtos) {
		this.subOrderItemDtos = subOrderItemDtos;
	}

	public AppBizAddressDto getAddressDto() {
		return addressDto;
	}

	public void setAddressDto(AppBizAddressDto addressDto) {
		this.addressDto = addressDto;
	}

	public AppBizDeliveryDto getDeliveryDto() {
		return deliveryDto;
	}

	public void setDeliveryDto(AppBizDeliveryDto deliveryDto) {
		this.deliveryDto = deliveryDto;
	}

	public AppBizStoreDto getStore() {
		return store;
	}

	public void setStore(AppBizStoreDto store) {
		this.store = store;
	}

	public Boolean getIsShopRemarked() {
		return isShopRemarked;
	}

	public void setIsShopRemarked(Boolean isShopRemarked) {
		this.isShopRemarked = isShopRemarked;
	}

	public Long getRefundId() {
		return refundId;
	}

	public void setRefundId(Long refundId) {
		this.refundId = refundId;
	}
}
