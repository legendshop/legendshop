package com.legendshop.biz.model.dto.prod;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/** 商品属性 **/
@ApiModel("商品属性")
public class AppBizProductPropertyDto implements Serializable{

	private static final long serialVersionUID = -6991917665604921238L;
	
	/** 属性ID **/
	@ApiModelProperty(value = "属性ID")
	private Long propId;

	/** 属性名称 **/
	@ApiModelProperty(value = "属性名称")
	private String propName;

	// 别名
	@ApiModelProperty(value = "别名")
	private String memo;

	// 是否必须
	@ApiModelProperty(value = "是否必须")
	private boolean isRequired;

	// 是否多选
	@ApiModelProperty(value = "是否多选")
	private boolean isMulti;

	// 排序
	@ApiModelProperty(value = "排序")
	private Long sequence;

	// 状态
	@ApiModelProperty(value = "状态")
	private Short status;

	// 属性类型，1：有图片，0：文字
	@ApiModelProperty(value = "属性类型，1：有图片，0：文字")
	private Integer type;

	// 修改时间
	@ApiModelProperty(value = "修改时间")
	private Date modifyDate;

	// 记录时间
	@ApiModelProperty(value = "记录时间")
	private Date recDate;

	// 规则属性：（1:销售属性; 2:参数属性; 3:关键属性）
	@ApiModelProperty(value = "规则属性：（1:销售属性; 2:参数属性; 3:关键属性）")
	private Integer isRuleAttributes;

	// 是否可以搜索
	@ApiModelProperty(value = "是否可以搜索")
	private boolean isForSearch;

	// 是否输入属性
	@ApiModelProperty(value = "是否输入属性")
	private boolean isInputProp;

	//用户名
	@ApiModelProperty(value = "用户名")
	private String userName;

	//商品 id
	@ApiModelProperty(value = "商品 id")
	private Long prodId;

	//1： 用户自定义   0：系统自带
	@ApiModelProperty(value = "1： 用户自定义   0：系统自带")
	private Integer isCustom;

	@ApiModelProperty(value = "顺序")
	private Integer seq;

	/** 商品属性值 **/
	@ApiModelProperty(value = "商品属性值列表")
	private List<AppBizProductPropertyValueDto> prodPropValList;

	public Long getPropId() {
		return propId;
	}

	public void setPropId(Long propId) {
		this.propId = propId;
	}

	public String getPropName() {
		return propName;
	}

	public void setPropName(String propName) {
		this.propName = propName;
	}

	public List<AppBizProductPropertyValueDto> getProdPropValList() {
		return prodPropValList;
	}

	public void setProdPropValList(List<AppBizProductPropertyValueDto> prodPropValList) {
		this.prodPropValList = prodPropValList;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public boolean isRequired() {
		return isRequired;
	}

	public void setRequired(boolean required) {
		isRequired = required;
	}

	public boolean isMulti() {
		return isMulti;
	}

	public void setMulti(boolean multi) {
		isMulti = multi;
	}

	public Long getSequence() {
		return sequence;
	}

	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	public Integer getIsRuleAttributes() {
		return isRuleAttributes;
	}

	public void setIsRuleAttributes(Integer isRuleAttributes) {
		this.isRuleAttributes = isRuleAttributes;
	}

	public boolean isForSearch() {
		return isForSearch;
	}

	public void setForSearch(boolean forSearch) {
		isForSearch = forSearch;
	}

	public boolean isInputProp() {
		return isInputProp;
	}

	public void setInputProp(boolean inputProp) {
		isInputProp = inputProp;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Integer getIsCustom() {
		return isCustom;
	}

	public void setIsCustom(Integer isCustom) {
		this.isCustom = isCustom;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}
}
