/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.biz.model.dto.order;

import com.legendshop.model.dto.order.SubOrderItemDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * App用于封装查询订单数据.
 */

@ApiModel("订单Dto")
public class AppBizOrderDto implements Serializable{

	private static final long serialVersionUID = -2864810122569245142L;

	/** 订单ID **/
	@ApiModelProperty(value = "订单ID")
	private Long subId;

	/** 商品名称 **/
	@ApiModelProperty(value = "商品名称")
	private String prodName;
	
	/** 用户ID **/
	@ApiModelProperty(value = "用户ID")
	private String userId;

	/** 用户头像 **/
	@ApiModelProperty(value = "用户头像")
	private String portraitPic;

	/** 用户名称 **/
	@ApiModelProperty(value = "用户名称 ")
	private String userName;
	
	/** 下订单的时间 **/
	@ApiModelProperty(value = "下订单的时间 ")
	private Date subDate;

	/** 订单号 **/
	@ApiModelProperty(value = "订单号")
	private String subNumber;


	/** 订单状态 **/
	@ApiModelProperty(value = "订单状态[待付款: 1 ，待发货：2 ，待收货：3 ，已完成：4 ， 已取消：5]")
	private Integer status;

	/** 商城ID **/
	@ApiModelProperty(value = "商城ID")
	private Long shopId;

	/** 订单商品总数 **/
	@ApiModelProperty(value = "订单商品总数")
	private Integer productNums;

	
	/** 是否已经支付 **/
	@ApiModelProperty(value = "是否已经支付")
	private Integer isPayed;
	
	/** 订单类型[普通订单：NORMAL，秒杀订单：SECKILL  门店订单：SHOP_STORE 预售订单：PRE_SELL 团购：GROUP 拼团：MERGE_GROUP 拍卖；AUCTIONS  示例：subType=“NORMAL”] **/
	@ApiModelProperty(value = "订单类型[普通订单：NORMAL，秒杀订单：SECKILL  门店订单：SHOP_STORE 预售订单：PRE_SELL 团购：GROUP 拼团：MERGE_GROUP 拍卖；AUCTIONS  示例：subType=“NORMAL”]")
	private String subType; 
	
	/** 用户订单删除状态，0：没有删除， 1：回收站， 2：永久删除 **/
	@ApiModelProperty(value = "用户订单删除状态，0：没有删除， 1：回收站， 2：永久删除")
	private Integer deleteStatus;
	
	/** 是否提醒过发货  */
	@ApiModelProperty(value = "是否提醒过发货")
	private boolean remindDelivery;

	/** 订单商品实际价格(运费 折扣 促销) **/
	@ApiModelProperty(value = "订单商品实际价格(运费 折扣 促销)")
	private Double actualTotal;
	
	/** 订单项列表 **/
	@ApiModelProperty(value = "订单项列表")
	private List<AppBizOrderItemDto> subOrderItemDtos=new ArrayList<>();

	/** 退款状态 0:默认,1:在处理,2:处理完成 */
	@ApiModelProperty(value = "退款状态 0:没有退款退货状态,1:退款退货中,2:退款退货处理完成")
	private Long refundState;

	@ApiModelProperty(value="用于倒计时的时间,待付款时是付款截止时间， 待成团是成团截止倒计时, 待收货时是确认收货截止时间")
	private Date countDownTime;
	
	@ApiModelProperty("活动ID, 如拼团活动ID")
	private Long activeId;

	@ApiModelProperty("拼团活动, 已开团的团编号")
	private String addNumber;
	
	@ApiModelProperty("门店订单提货码")
	private String dlyoPickupCode;	

	/** 是否支付定金 */
	@ApiModelProperty(value="是否支付定金 ") 
	private Integer isPayDeposit; 

		
	/** 是否支付尾款 */
	@ApiModelProperty(value="是否支付尾款") 
	private Integer isPayFinal; 
	
	/** 订单自动取消剩余时间（分钟） */
	@ApiModelProperty(value="订单自动取消剩余时间（分钟）") 
	private Integer orderCancelMinute;

	/** 支付方式,0:全额,1:订金 */
	@ApiModelProperty(value="支付方式  0:全额,1:订金 ")
	private Integer payPctType;

	@ApiModelProperty(value="团购状态 -2待支付 0:团购中 -1:团购失败 1：团购成功")
	private Integer groupStatus;

	/** 拼团状态 */
	@ApiModelProperty(value="拼团状态 -2待支付 0:拼团中 -1:拼团失败 1：拼团成功")
	private Integer mergeGroupStatus;


	//预售订单字段
	/** 定金金额 */
	@ApiModelProperty(value="定金金额")
	private BigDecimal preDepositPrice;

	/** 尾款金额 */
	@ApiModelProperty(value="尾款金额 ")
	private BigDecimal finalPrice;

	/** 尾款支付开始时间 */
	@ApiModelProperty(value="尾款支付开始时间 ")
	private Date finalMStart;

	/** 尾款支付结束时间 */
	@ApiModelProperty(value="尾款支付结束时间 ")
	private Date finalMEnd;

	/** 定金支付时间 */
	@ApiModelProperty(value="定金支付时间 ")
	private Date depositPayTime;

	/** 尾款支付时间 */
	@ApiModelProperty(value="尾款支付时间  ")
	private Date finalPayTime;

	/** 支付的时间  **/
	@ApiModelProperty(value = "支付的时间")
	private Date payDate;

	/** 配送类型  **/
	@ApiModelProperty(value = "配送类型")
	private String dvyType;

	/** 配送方式ID **/
	@ApiModelProperty(value = "配送方式ID")
	private Long dvyTypeId;

	/** 物流单号 **/
	@ApiModelProperty(value = "物流单号 ")
	private String dvyFlowId;

	/** 发票单号 **/
	@ApiModelProperty(value = "发票单号")
	private Long invoiceSubId;

	/** 物流费用 **/
	@ApiModelProperty(value = "物流费用")
	private Double freightAmount;

	/** 是否需要发票(0:不需要;1:需要) **/
	@ApiModelProperty(value = "是否需要发票(0:不需要;1:需要)")
	private Integer isNeedInvoice;

	/** 支付方式(1:货到付款;2:在线支付) **/
	@ApiModelProperty(value = "支付方式(1:货到付款;2:在线支付)")
	private Integer payManner;

	/** 促销优惠总金额 **/
	@ApiModelProperty(value = "促销优惠总金额")
	private Double discountPrice;

	/** 发货时间 */
	@ApiModelProperty(value = "发货时间")
	private Date dvyDate;

	/** 完成时间/确认收货时间  **/
	@ApiModelProperty(value = "完成时间/确认收货时间 ")
	private Date finallyDate;

	/** 是否货到付款 **/
	@ApiModelProperty(value = "是否货到付款")
	private Integer isCod;

	/**是否已经备注**/
	@ApiModelProperty(value="是否已经备注")
	private Boolean isShopRemarked;

	/**退款id**/
	@ApiModelProperty(value="退款id")
	private Long refundId;

	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getSubDate() {
		return subDate;
	}

	public void setSubDate(Date subDate) {
		this.subDate = subDate;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getProductNums() {
		return productNums;
	}

	public void setProductNums(Integer productNums) {
		this.productNums = productNums;
	}

	public Integer getIsPayed() {
		return isPayed;
	}

	public void setIsPayed(Integer isPayed) {
		this.isPayed = isPayed;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public Integer getDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(Integer deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public boolean isRemindDelivery() {
		return remindDelivery;
	}

	public void setRemindDelivery(boolean remindDelivery) {
		this.remindDelivery = remindDelivery;
	}

	public List<AppBizOrderItemDto> getSubOrderItemDtos() {
		return subOrderItemDtos;
	}

	public void setSubOrderItemDtos(List<AppBizOrderItemDto> subOrderItemDtos) {
		this.subOrderItemDtos = subOrderItemDtos;
	}

	public Long getRefundState() {
		return refundState;
	}

	public void setRefundState(Long refundState) {
		this.refundState = refundState;
	}

	public Date getCountDownTime() {
		return countDownTime;
	}

	public void setCountDownTime(Date countDownTime) {
		this.countDownTime = countDownTime;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public String getAddNumber() {
		return addNumber;
	}

	public void setAddNumber(String addNumber) {
		this.addNumber = addNumber;
	}

	public String getDlyoPickupCode() {
		return dlyoPickupCode;
	}

	public void setDlyoPickupCode(String dlyoPickupCode) {
		this.dlyoPickupCode = dlyoPickupCode;
	}

	public Integer getIsPayDeposit() {
		return isPayDeposit;
	}

	public void setIsPayDeposit(Integer isPayDeposit) {
		this.isPayDeposit = isPayDeposit;
	}

	public Integer getIsPayFinal() {
		return isPayFinal;
	}

	public void setIsPayFinal(Integer isPayFinal) {
		this.isPayFinal = isPayFinal;
	}

	public Integer getOrderCancelMinute() {
		return orderCancelMinute;
	}

	public void setOrderCancelMinute(Integer orderCancelMinute) {
		this.orderCancelMinute = orderCancelMinute;
	}

	public String getPortraitPic() {
		return portraitPic;
	}

	public void setPortraitPic(String portraitPic) {
		this.portraitPic = portraitPic;
	}

	public Double getActualTotal() {
		return actualTotal;
	}

	public void setActualTotal(Double actualTotal) {
		this.actualTotal = actualTotal;
	}

	public Integer getPayPctType() {
		return payPctType;
	}

	public void setPayPctType(Integer payPctType) {
		this.payPctType = payPctType;
	}

	public Integer getGroupStatus() {
		return groupStatus;
	}

	public void setGroupStatus(Integer groupStatus) {
		this.groupStatus = groupStatus;
	}

	public Integer getMergeGroupStatus() {
		return mergeGroupStatus;
	}

	public void setMergeGroupStatus(Integer mergeGroupStatus) {
		this.mergeGroupStatus = mergeGroupStatus;
	}

	public BigDecimal getPreDepositPrice() {
		return preDepositPrice;
	}

	public void setPreDepositPrice(BigDecimal preDepositPrice) {
		this.preDepositPrice = preDepositPrice;
	}

	public BigDecimal getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(BigDecimal finalPrice) {
		this.finalPrice = finalPrice;
	}

	public Date getFinalMStart() {
		return finalMStart;
	}

	public void setFinalMStart(Date finalMStart) {
		this.finalMStart = finalMStart;
	}

	public Date getFinalMEnd() {
		return finalMEnd;
	}

	public void setFinalMEnd(Date finalMEnd) {
		this.finalMEnd = finalMEnd;
	}

	public Date getDepositPayTime() {
		return depositPayTime;
	}

	public void setDepositPayTime(Date depositPayTime) {
		this.depositPayTime = depositPayTime;
	}

	public Date getFinalPayTime() {
		return finalPayTime;
	}

	public void setFinalPayTime(Date finalPayTime) {
		this.finalPayTime = finalPayTime;
	}

	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	public String getDvyType() {
		return dvyType;
	}

	public void setDvyType(String dvyType) {
		this.dvyType = dvyType;
	}

	public Long getDvyTypeId() {
		return dvyTypeId;
	}

	public void setDvyTypeId(Long dvyTypeId) {
		this.dvyTypeId = dvyTypeId;
	}

	public String getDvyFlowId() {
		return dvyFlowId;
	}

	public void setDvyFlowId(String dvyFlowId) {
		this.dvyFlowId = dvyFlowId;
	}

	public Long getInvoiceSubId() {
		return invoiceSubId;
	}

	public void setInvoiceSubId(Long invoiceSubId) {
		this.invoiceSubId = invoiceSubId;
	}

	public Double getFreightAmount() {
		return freightAmount;
	}

	public void setFreightAmount(Double freightAmount) {
		this.freightAmount = freightAmount;
	}

	public Integer getIsNeedInvoice() {
		return isNeedInvoice;
	}

	public void setIsNeedInvoice(Integer isNeedInvoice) {
		this.isNeedInvoice = isNeedInvoice;
	}

	public Integer getPayManner() {
		return payManner;
	}

	public void setPayManner(Integer payManner) {
		this.payManner = payManner;
	}

	public Double getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Double discountPrice) {
		this.discountPrice = discountPrice;
	}

	public Date getDvyDate() {
		return dvyDate;
	}

	public void setDvyDate(Date dvyDate) {
		this.dvyDate = dvyDate;
	}

	public Date getFinallyDate() {
		return finallyDate;
	}

	public void setFinallyDate(Date finallyDate) {
		this.finallyDate = finallyDate;
	}

	public Integer getIsCod() {
		return isCod;
	}

	public void setIsCod(Integer isCod) {
		this.isCod = isCod;
	}

	public Boolean getIsShopRemarked() {
		return isShopRemarked;
	}

	public void setIsShopRemarked(Boolean isShopRemarked) {
		this.isShopRemarked = isShopRemarked;
	}

	public Long getRefundId() {
		return refundId;
	}

	public void setRefundId(Long refundId) {
		this.refundId = refundId;
	}

	public void addItem(AppBizOrderItemDto mySubItemDto) {
		subOrderItemDtos.add(mySubItemDto);
	}
}
