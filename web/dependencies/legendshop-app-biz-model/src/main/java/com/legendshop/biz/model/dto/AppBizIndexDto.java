package com.legendshop.biz.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value="店铺主页Dto")
public class AppBizIndexDto implements Serializable{

	private static final long serialVersionUID = 8008194649513962171L;
	/**
	 * 店铺ID
	 */
	@ApiModelProperty(value="店铺ID")
	private Long shopId;
	/**
	 * 店铺名
	 */
	@ApiModelProperty(value="店铺名")
	private String shopName;

	/**
	 * 联系人电话
	 */
	@ApiModelProperty(value="联系人电话")
	private String contactMobile;

	/**
	 * logo 图片
	 * @return
	 */
	@ApiModelProperty(value="logo 图片")
	private String logoPic;

	/**
	 * 用户订购数
	 */
	@ApiModelProperty(hidden = true)
	private  Integer userNum;

	/**
	 *  当日订单数
	 */
	@ApiModelProperty(value="当日订单数")
	private  Integer subNum;

	/**
	 *  当日销售金额
	 */
	@ApiModelProperty(value="当日销售金额")
	private  Integer actualTotal;

	/**
	 * 客单价
	 * @return
	 */
	@ApiModelProperty(value="客单价")
	private Integer perCapitaNumber;

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}


	public String getContactMobile() {
		return contactMobile;
	}

	public void setContactMobile(String contactMobile) {
		this.contactMobile = contactMobile;
	}


	public String getLogoPic() {
		return logoPic;
	}

	public void setLogoPic(String logoPic) {
		this.logoPic = logoPic;
	}

	public Integer getUserNum() {
		return userNum;
	}

	public void setUserNum(Integer userNum) {
		this.userNum = userNum;
	}

	public Integer getSubNum() {
		return subNum;
	}

	public void setSubNum(Integer subNum) {
		this.subNum = subNum;
	}

	public Integer getActualTotal() {
		return actualTotal;
	}

	public void setActualTotal(Integer actualTotal) {
		this.actualTotal = actualTotal;
	}

	public Integer getPerCapitaNumber() {
		return perCapitaNumber;
	}

	public void setPerCapitaNumber(Integer perCapitaNumber) {
		this.perCapitaNumber = perCapitaNumber;
	}


}
