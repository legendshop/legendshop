package com.legendshop.biz.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 平台分销推广设置dto
 * @author Joe
 */
@ApiModel(value = "平台分销推广设置")
@Data
public class DistSetDto implements Serializable {


	@ApiModelProperty(value = "是否开启多级分销")
	private Integer supportDist;

	@ApiModelProperty(value = "会员直接上级分佣比例")
	private Double firstLevelRate;

	@ApiModelProperty(value = "会员上二级分佣比例")
	private Double secondLevelRate;

	@ApiModelProperty(value = "会员上三级分佣比例")
	private Double thirdLevelRate;

}
