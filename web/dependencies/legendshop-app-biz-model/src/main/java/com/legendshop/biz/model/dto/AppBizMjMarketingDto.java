package com.legendshop.biz.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * 滿減满折 活动Dto
 * @author 27158
 */
@ApiModel(value = "滿減满折 活动Dto")
public class AppBizMjMarketingDto {
	/**
	 * 满折规格属性
	 */
	@ApiModelProperty(value ="满折规格属性",required = true)
	private String[] manzeRule;
	/**
	 * 满减规格
	 */
	@ApiModelProperty(value ="满减规格属性",required = true)
	private String[] manjianRule;

}
