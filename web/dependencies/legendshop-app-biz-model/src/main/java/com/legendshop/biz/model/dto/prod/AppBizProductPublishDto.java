/*
 * LegendShop 多用户商城系统
 *
 *  版权所有, 并保留所有权利。
 *
 */
package com.legendshop.biz.model.dto.prod;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * app 商家端商品发布Dto.
 * @author 27158
 */
@ApiModel(value = "商品发布Dto")
public class AppBizProductPublishDto implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -7571396124663475715L;

	/**
	 * 商品Id
	 */
	@ApiModelProperty(value = "商品id")
	private Long prodId;

	/**
	 * 商品名称.
	 */
	@ApiModelProperty(value = "商品名称")
	private String name;

	/**
	 * 产品现价，客户最终价格.
	 */
	@ApiModelProperty(value = "产品现价，客户最终价格")
	private Double cash;

	/** 产品原价，市场价. */
	@ApiModelProperty(value = "产品原价，市场价")
	private Double price;

	/**
	 * 商品详情
	 */
	@ApiModelProperty(value = "商品详情 ")
	private String content;

	/**
	 * 商品详情
	 */
	@ApiModelProperty(value = "移动端详情 ")
	private String contentM;

	/**
	 * 库存.
	 */
	@ApiModelProperty(value = "库存 所有商品下sku 商品 数量总和")
	private Integer stocks;



	/**
	 * 平台参数属性列表
	 * inputParam 为 平台参数是否可以自定义 可以为 true 不可以 为 false
	 */
	@ApiModelProperty(value = "平台参数属性列表，数组JSON串，例子inputParam 为 平台参数是否可以自定义 可以为 true 不可以 为 false：[{\"inputParam\":\"true\",\"paramId\":\"1947\",\"paramName\":\"功能\",\"paramValueId\":\"\",\"paramValueName\":\"清洁\"},...]")
	private String parameter;

	/**
	 * 用户自定义的参数属性列表, key:value 格式
	 */
	@ApiModelProperty(value = "用户自定义的参数属性列表，数组JSON串 自定义参数必填，例子：[{\"key\":\"自定义参数1\",\"value\":\"参数值\"},{\"key\":\"自定义参数2\",\"value\":\"参数值2\"}, ... ]")
	private String userParameter;

	/**
	 * 品牌Id.
	 */
	@ApiModelProperty(value = "品牌Id ; 选择品牌时 必填")
	private Long brandId;


	/**
	 * 库存计数方式，0：拍下减库存，1：付款减库存
	 **/
	@ApiModelProperty(value = "库存计数方式，0：拍下减库存，1：付款减库存")
	private int stockCounting;

	/**
	 * 商品平台分类ID
	 **/
	@ApiModelProperty(value = "商品平台分类ID")
	private Long categoryId;

	/**
	 * (店铺分类) 一级分类
	 **/
	@ApiModelProperty(value = "(店铺分类ID) 一级分类 选择一级分类 必填")
	private Long shopFirstCatId;

	/**
	 * (店铺分类) 二级分类
	 **/
	@ApiModelProperty(value = "(店铺分类ID) 二级分类 选择 二级分类 必填")
	private Long shopSecondCatId;

	/**
	 * (店铺分类) 三级分类
	 **/
	@ApiModelProperty(value = "(店铺分类ID) 三级分类 选择 三级分类 必填")
	private Long shopThirdCatId;

	/**
	 * 运费模板ID
	 **/
	@ApiModelProperty(value = "运费模板ID 使用运费模板时 必填")
	private Long transportId;

	/**
	 * 是否免运费
	 **/
	@ApiModelProperty(value = "1:商家承担运费;0: 买家承担运费 ")
	private Integer supportTransportFree;

	/**
	 * 运费类型选择 ，参见TransportTypeEnum
	 **/
	@ApiModelProperty(value = "'是否固定运费[0:使用运费模板;1:固定运费],在使用买家承担运费时 必填")
	private Integer transportType;

	/**
	 * 快递运费
	 **/
	@ApiModelProperty(value = "快递运费 固定运费时必填")
	private Double expressTransFee;

	/**
	 * 是否支持分销
	 **/
	@ApiModelProperty(value = "是否支持分销 0 （默认）不支持 1 支持")
	private int supportDist = 0;

	/**
	 * 会员直接上级分佣比例
	 **/
	@ApiModelProperty(value = "会员直接上级分佣比例 支持分销必填  分佣总比列不可大于50% 默认50%")
	private Double firstLevelRate;

	/**
	 * 会员上二级分佣比例
	 **/
	@ApiModelProperty(value = "会员上二级分佣比例  支持分销必填 分佣总比列不可大于50% ")
	private Double secondLevelRate;

	/**
	 * 会员上三级分佣比例
	 **/
	@ApiModelProperty(value = "会员上三级分佣比例 支持分销必填 分佣总比列不可大于50% ")
	private Double thirdLevelRate;

	/**
	 * 商品图片
	 **/
	@ApiModelProperty(value = "商品图片 数组json串, 格式： [url1,url2,url3]")
	private String imagesPaths;

	/**
	 * 商品sku数据
	 **/
	@ApiModelProperty(value = "商品sku数据，必填")
	private List<AppBizSkusDto> skuDtoList;

	/**
	 * 用户自定义属性
	 **/
	@ApiModelProperty(value = "用户自定义属性, JSON 数字 字符串 自定义属性必填 参数为 保存商家自定义规格属性 接口返回的参数" )
	private String userProperties;


	/**
	 * 商品属性值图片
	 **/
	@ApiModelProperty(value = "商品sku图片, 数组JSON字符串 选择图片时必填，例:[{\"imgList\":[\"2019/09/18/2954acb6-9e8a-483c-9504-3a6eeb8b208e.jpg\"],\"valueId\":\"861\",\"valueName\":\"黑色\",\"propId\":\"277\"},...]")
	private String valueImages;



	/**
	 * 设定商品发布后状态 1：上线，2：设定：有记录开始时间，0：放入仓库
	 **/
	@ApiModelProperty(value = "设定商品发布后状态 1：上线，2：设定：有记录开始时间，0：放入仓库")
	private Integer publishStatus;

	/**
	 * 建立时间
	 **/
	@ApiModelProperty(value = "商品开始有效时间 品发布后状态 为 2 时必填，格式 yyyy-MM-dd HH:mm:ss ")
	private String setUpTime;

	/**
	 * 商品视频地址
	 */
	@ApiModelProperty(value = "商品视频地址 上传视频必填")
	private String proVideoUrl;

	/**
	 * 货到付款; 0:普通商品 , 1:货到付款商品
	 **/
	@ApiModelProperty(value = "货到付款; 0:普通商品 , 1:货到付款商品 默认 0")
	private int isSupportCod=0;

	/** 商品预警库存. */
	protected Integer stocksArm;

	/**
	 * 服务说明
	 */
	@ApiModelProperty(value = "服务说明")
	private String serviceShow;

	public String getServiceShow() {
		return serviceShow;
	}

	public void setServiceShow(String serviceShow) {
		this.serviceShow = serviceShow;
	}



	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public int getSupportDist() {
		return supportDist;
	}

	public void setSupportDist(int supportDist) {
		this.supportDist = supportDist;
	}


	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getStocks() {
		return stocks;
	}

	public void setStocks(Integer stocks) {
		this.stocks = stocks;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getUserParameter() {
		return userParameter;
	}

	public void setUserParameter(String userParameter) {
		this.userParameter = userParameter;
	}

	public Long getBrandId() {
		return brandId;
	}

	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}



	public int getStockCounting() {
		return stockCounting;
	}

	public void setStockCounting(int stockCounting) {
		this.stockCounting = stockCounting;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getShopFirstCatId() {
		return shopFirstCatId;
	}

	public void setShopFirstCatId(Long shopFirstCatId) {
		this.shopFirstCatId = shopFirstCatId;
	}

	public Long getShopSecondCatId() {
		return shopSecondCatId;
	}

	public void setShopSecondCatId(Long shopSecondCatId) {
		this.shopSecondCatId = shopSecondCatId;
	}

	public Long getShopThirdCatId() {
		return shopThirdCatId;
	}

	public void setShopThirdCatId(Long shopThirdCatId) {
		this.shopThirdCatId = shopThirdCatId;
	}


	public Long getTransportId() {
		return transportId;
	}

	public void setTransportId(Long transportId) {
		this.transportId = transportId;
	}

	public Integer getSupportTransportFree() {
		return supportTransportFree;
	}

	public void setSupportTransportFree(Integer supportTransportFree) {
		this.supportTransportFree = supportTransportFree;
	}

	public Integer getTransportType() {
		return transportType;
	}

	public void setTransportType(Integer transportType) {
		this.transportType = transportType;
	}

	public Double getExpressTransFee() {
		return expressTransFee;
	}

	public void setExpressTransFee(Double expressTransFee) {
		this.expressTransFee = expressTransFee;
	}

	public Double getFirstLevelRate() {
		return firstLevelRate;
	}

	public void setFirstLevelRate(Double firstLevelRate) {
		this.firstLevelRate = firstLevelRate;
	}

	public Double getSecondLevelRate() {
		return secondLevelRate;
	}

	public void setSecondLevelRate(Double secondLevelRate) {
		this.secondLevelRate = secondLevelRate;
	}

	public Double getThirdLevelRate() {
		return thirdLevelRate;
	}

	public void setThirdLevelRate(Double thirdLevelRate) {
		this.thirdLevelRate = thirdLevelRate;
	}

	public String getImagesPaths() {
		return imagesPaths;
	}

	public void setImagesPaths(String imagesPaths) {
		this.imagesPaths = imagesPaths;
	}

	public List<AppBizSkusDto> getSkuDtoList() {
		return skuDtoList;
	}

	public void setSkuDtoList(List<AppBizSkusDto> skuDtoList) {
		this.skuDtoList = skuDtoList;
	}

	public String getUserProperties() {
		return userProperties;
	}

	public void setUserProperties(String userProperties) {
		this.userProperties = userProperties;
	}


	public String getValueImages() {
		return valueImages;
	}

	public void setValueImages(String valueImages) {
		this.valueImages = valueImages;
	}



	public Integer getPublishStatus() {
		return publishStatus;
	}

	public void setPublishStatus(Integer publishStatus) {
		this.publishStatus = publishStatus;
	}

	public String getSetUpTime() {
		return setUpTime;
	}

	public void setSetUpTime(String setUpTime) {
		this.setUpTime = setUpTime;
	}

	public String getProVideoUrl() {
		return proVideoUrl;
	}

	public void setProVideoUrl(String proVideoUrl) {
		this.proVideoUrl = proVideoUrl;
	}


	public int getIsSupportCod() {
		return isSupportCod;
	}

	public void setIsSupportCod(int isSupportCod) {
		this.isSupportCod = isSupportCod;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getContentM() {
		return contentM;
	}

	public void setContentM(String contentM) {
		this.contentM = contentM;
	}

	public Integer getStocksArm() {
		return stocksArm;
	}

	public void setStocksArm(Integer stocksArm) {
		this.stocksArm = stocksArm;
	}
}