/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.biz.model.dto;

import java.util.Date;

/**
 * @Description 
 * @author 关开发
 */
public class ShopRefundSearchParamDto {
	/** 订单编号 */
	public static final Long ORDER_NO = 1L; 
	/** 退款编号 */
	public static final Long REFUND_NO = 2L; 
	/** 会员名称 */
	public static final Long USER_NAME = 3L; 
	/** 商家名称 */
	public static final Long SHOP_NAME = 4L; 
	
	/** 申请类型 1:退款,2:退款且退货 */
	private Long applyType;
	/** 开始时间 */
	private Date startDate;
	/** 结束时间 */
	private Date endDate;
	/** 卖家处理状态 */
	private Long sellerState;
	/** 编号类型 1:订单编号,2:退款编号,3:会员名称 */
	private Long numType;
	/** 编号 */
	private String number;
	/** 申请状态 */
	private Long applyState;
	
	/**
	 * @return the applyType
	 */
	public Long getApplyType() {
		return applyType;
	}
	/**
	 * @param applyType the applyType to set
	 */
	public void setApplyType(Long applyType) {
		this.applyType = applyType;
	}
	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return the endDate
	 */
	public Date getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return the sellerState
	 */
	public Long getSellerState() {
		return sellerState;
	}
	/**
	 * @param sellerState the sellerState to set
	 */
	public void setSellerState(Long sellerState) {
		this.sellerState = sellerState;
	}
	/**
	 * @return the numType
	 */
	public Long getNumType() {
		return numType;
	}
	/**
	 * @param numType the numType to set
	 */
	public void setNumType(Long numType) {
		this.numType = numType;
	}
	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * @return the applyState
	 */
	public Long getApplyState() {
		return applyState;
	}
	/**
	 * @param applyState the applyState to set
	 */
	public void setApplyState(Long applyState) {
		this.applyState = applyState;
	}
}
