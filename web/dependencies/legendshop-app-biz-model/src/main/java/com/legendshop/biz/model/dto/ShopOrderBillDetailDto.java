package com.legendshop.biz.model.dto;

import java.io.Serializable;

import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.ShopOrderBill;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="ShopOrderBillDetailDto") 
public class ShopOrderBillDetailDto implements Serializable {

	private static final long serialVersionUID = 7716125422170092455L;
	
	@ApiModelProperty(value="商品评论",example="PageSupportDto<ShopProductCommentDto>") 
	private AppPageSupport<ShopProductCommentDto> dto;
	
	@ApiModelProperty(value="商家账单", example="商家账单") 
	private ShopOrderBill shopOrderBill;

	public AppPageSupport<ShopProductCommentDto> getDto() {
		return dto;
	}

	public void setDto(AppPageSupport<ShopProductCommentDto> dto) {
		this.dto = dto;
	}

	public ShopOrderBill getShopOrderBill() {
		return shopOrderBill;
	}

	public void setShopOrderBill(ShopOrderBill shopOrderBill) {
		this.shopOrderBill = shopOrderBill;
	}
	
}
