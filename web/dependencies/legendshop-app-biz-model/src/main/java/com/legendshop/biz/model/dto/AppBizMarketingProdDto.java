package com.legendshop.biz.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author 27158
 */
@ApiModel(value = "活动的商品Dto")
public class AppBizMarketingProdDto implements Serializable {
	private static final long serialVersionUID = -1241057870087373923L;

	/** 商品ID */
	@ApiModelProperty(value = "商品ID")
	private Long prodId;

	/** 商品name */
	@ApiModelProperty(value = "商品name")
	private String prodName;

	/**
	 * 商品价格
	 */
	@ApiModelProperty(value = "商品价格")
	private Double cash;
	/**
	 * 商品图片
	 */
	@ApiModelProperty(value = "商品图片")
	private String pic;
	/**
	 * 商品库存数
	 */
	@ApiModelProperty(value = "商品库存数")
	private Integer stocks;
	/**
	 * sku商品集合
	 */
	@ApiModelProperty(value = "sku商品集合")
	private List<AppBizMarketingProdSkuDto> marketingProdSkuDtos;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}


	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Integer getStocks() {
		return stocks;
	}

	public void setStocks(Integer stocks) {
		this.stocks = stocks;
	}

	public List<AppBizMarketingProdSkuDto> getMarketingProdSkuDtos() {
		return marketingProdSkuDtos;
	}

	public void setMarketingProdSkuDtos(List<AppBizMarketingProdSkuDto> marketingProdSkuDtos) {
		this.marketingProdSkuDtos = marketingProdSkuDtos;
	}
}
