package com.legendshop.biz.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 *
 * 营销活动Dto
 *
 * @author 27158
 */
@ApiModel(value="营销活动Dto")
public class AppBizShowMarketingDto extends  AppBizMarketingDto{
	private static final long serialVersionUID = 5239584562995762595L;

	/**
	 * 活动规则list集合
	 */
	@ApiModelProperty(value ="活动规则集合")
	private List<AppBizMarketingRuleDto> marketingRuleDto;

	/** 关联商品*/
	@ApiModelProperty(value ="关联商品sku集合")
	private List<AppBizMarketingProdSkuDto> MarketingProdSkus;

	public List<AppBizMarketingRuleDto> getMarketingRuleDto() {
		return marketingRuleDto;
	}

	public void setMarketingRuleDto(List<AppBizMarketingRuleDto> marketingRuleDto) {
		this.marketingRuleDto = marketingRuleDto;
	}

	public List<AppBizMarketingProdSkuDto> getMarketingProdSkus() {
		return MarketingProdSkus;
	}

	public void setMarketingProdSkus(List<AppBizMarketingProdSkuDto> marketingProdSkus) {
		MarketingProdSkus = marketingProdSkus;
	}
}
