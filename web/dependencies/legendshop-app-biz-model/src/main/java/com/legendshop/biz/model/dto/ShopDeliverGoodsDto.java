package com.legendshop.biz.model.dto;

import java.io.Serializable;
import java.util.List;

import com.legendshop.model.entity.DeliveryType;
import com.legendshop.model.entity.Sub;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value="ShopDeliverGoodsDto店铺配送商品")
public class ShopDeliverGoodsDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1817715713145062366L;

	@ApiModelProperty(value="物流配送方式")
	private List<DeliveryType> deliveryTypes;
	
	@ApiModelProperty(value="订单对象")
	private Sub sub;

	public List<DeliveryType> getDeliveryTypes() {
		return deliveryTypes;
	}

	public void setDeliveryTypes(List<DeliveryType> deliveryTypes) {
		this.deliveryTypes = deliveryTypes;
	}

	public Sub getSub() {
		return sub;
	}

	public void setSub(Sub sub) {
		this.sub = sub;
	}

	
}
