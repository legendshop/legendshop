package com.legendshop.biz.model.dto.productAttribute;

import com.legendshop.dao.persistence.*;
import com.legendshop.dao.support.GenericEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 *商家自定义商品属性
 */
@ApiModel("商家自定义商品属性")
public class AppBizProdShopAttribute implements GenericEntity<Long> {

	private static final long serialVersionUID = 4391628675142755560L;

	/** 主键 */
	@ApiModelProperty(value = "主键")
	private Long id;

	/** 动态模板名称 */
	@ApiModelProperty(value = "动态模板名称")
	private String name;

	/** 内容 */
	@ApiModelProperty(value = "内容")
	private String content;

	/** 用户名 */
	@ApiModelProperty(value = "用户名")
	private String userId;

	/** 加入时间 */
	@ApiModelProperty(value = "加入时间")
	private Date recDate;

	/** 商家id */
	@ApiModelProperty(value = "商家id")
	private  Long shopId;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
}
