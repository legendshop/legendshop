package com.legendshop.biz.model.dto.productAttribute;

import com.legendshop.dao.support.GenericEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 *商家自定义商品参数
 */
@ApiModel("商家自定义商品参数Dto")
public class AppBizProdShopParamDto implements GenericEntity<Long> {

	private static final long serialVersionUID = -6273562045198816196L;

	/** 主键 */
	@ApiModelProperty(value = "主键")
	private Long id;

	/** 动态模板名称 */
	@ApiModelProperty(value = "参数名称")
	private String name;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
