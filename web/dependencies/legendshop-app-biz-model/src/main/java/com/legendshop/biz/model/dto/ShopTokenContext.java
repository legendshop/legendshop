/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.biz.model.dto;

import com.legendshop.model.dto.app.AppTokenDto;

/**
 * The Class ShopTokenContext.
 */
public class ShopTokenContext {

	/** The request holder. */
	private static ThreadLocal<AppTokenDto> tokenHolder = new ThreadLocal<AppTokenDto>();


	public static AppTokenDto getToken(){
		return tokenHolder.get();
	}
	
	public static void setToken(AppTokenDto token){
		 tokenHolder.set(token);
	}
	
	/**
	 * Clean.
	 */
	public static void clean() {
		if (tokenHolder.get() != null) {
			tokenHolder.remove();
		}
	}

	/**
	 * 是否开始请求.
	 * 
	 * @return true, if successful
	 */
	public static boolean requestStarted() {
		return tokenHolder.get() != null;
	}

}
