/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.biz.model.dto.comment;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 商家端商品评论列表
 * @author linzh
 */
@ApiModel(value="商家的商品类目Dto")
public class AppBizProductCommentDto implements Serializable {


	@ApiModelProperty(value = "商品ID")
	/** 商品ID */
	private Long prodId;

	@ApiModelProperty(value = "产品类目ID")
	/** 商品名称 */
	private String prodName;

	@ApiModelProperty(value = "产品类目ID")
	/** 商品图片 */
	private String prodPic;

	@ApiModelProperty(value = "产品类目ID")
	/** 评价数 */
	private Integer comments;

	@ApiModelProperty(value = "产品类目ID")
	/** 平均得分 */
	private float avgScore;

	@ApiModelProperty(value = "产品类目ID")
	/** 待回复数 */
	private Integer waitReply;


	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getProdPic() {
		return prodPic;
	}

	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}

	public Integer getComments() {
		return comments;
	}

	public void setComments(Integer comments) {
		this.comments = comments;
	}

	public float getAvgScore() {
		return avgScore;
	}

	public void setAvgScore(float avgScore) {
		this.avgScore = avgScore;
	}

	public Integer getWaitReply() {
		return waitReply;
	}

	public void setWaitReply(Integer waitReply) {
		this.waitReply = waitReply;
	}
}