package com.legendshop.biz.model.dto.productAttribute;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * app 商家自定义规格属性Dto
 * 一个属性有多个属性值
 * @author 27158
 */
@ApiModel(value = "商家自定义规格属性Dto")
public class AppBizAttdefDto implements Serializable {
	private static final long serialVersionUID = 6515395649995115663L;

	/**
	 * 属性名
	 */
	@ApiModelProperty(value = "属性名")
	private AppBizProductPropertyDto  appBizProductPropertyDto;
	/**
	 * 属性值 lsit集合
	 */
	@ApiModelProperty(value = "属性值")
	private List<AppBizProductPropertyValueDto> propertyValueList;

	public AppBizProductPropertyDto getAppBizProductPropertyDto() {
		return appBizProductPropertyDto;
	}

	public void setAppBizProductPropertyDto(AppBizProductPropertyDto appBizProductPropertyDto) {
		this.appBizProductPropertyDto = appBizProductPropertyDto;
	}

	public List<AppBizProductPropertyValueDto> getPropertyValueList() {
		return propertyValueList;
	}

	public void setPropertyValueList(List<AppBizProductPropertyValueDto> propertyValueList) {
		this.propertyValueList = propertyValueList;
	}
}
