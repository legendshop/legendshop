package com.legendshop.biz.model.dto.prod;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value="AppProdDto")
public class AppProdDto implements Serializable{
	private static final long serialVersionUID = 5049919089101634020L;

	@ApiModelProperty(value="商品id")
	private Long prodId;//
	
	@ApiModelProperty(value="商品名称")
	private String name;
	
	@ApiModelProperty(value="图片")
	private String pic;
	
	@ApiModelProperty(value="商品价格")
	private Double price;
	
	@ApiModelProperty(value="库存")
	private Integer stocks;
	
	@ApiModelProperty(value="销量")
	private Long buys;
	
	@ApiModelProperty(value="状态")
	private Integer status;
	
	@ApiModelProperty(value="分享路劲")
	private String shareUrl;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	
	public Integer getStocks() {
		return stocks;
	}

	public void setStocks(Integer stocks) {
		this.stocks = stocks;
	}

	public Long getBuys() {
		return buys;
	}

	public void setBuys(Long buys) {
		this.buys = buys;
	}

	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	

	@Override
	public String toString() {
		return "AppProdDto [prodId=" + prodId + ", name=" + name + ", pic=" + pic + ", price=" + price + ", stocks=" + stocks + ", buys=" + buys + "]";
	}

	public String getShareUrl() {
		return shareUrl;
	}

	public void setShareUrl(String shareUrl) {
		this.shareUrl = shareUrl;
	}
}
