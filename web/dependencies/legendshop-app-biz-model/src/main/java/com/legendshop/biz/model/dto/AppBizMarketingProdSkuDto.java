package com.legendshop.biz.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author 27158
 */
@ApiModel(value = "活动的商品SKUDto")
public class AppBizMarketingProdSkuDto implements Serializable {

	private static final long serialVersionUID = 8057522623886932642L;
	/** sku ID*/
	@ApiModelProperty(value = "sku ID")
	private Long skuId;

	/** 商品ID */
	@ApiModelProperty(value = "商品ID")
	private Long prodId;

	/** 商品name */
	@ApiModelProperty(value = "商品name")
	private String name;
	/**
	 * 商品属性规格
	 */
	@ApiModelProperty(value = "商品属性规格")
	private String cnProperties;
	/**
	 * 商品价格
	 */
	@ApiModelProperty(value = "商品价格")
	private Double price;
	/**
	 * 商品图片
	 */
	@ApiModelProperty(value = "商品图片")
	private String pic;
	/**
	 * 商品库存数
	 */
	@ApiModelProperty(value = "商品库存数")
	private Integer stocks;

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCnProperties() {
		return cnProperties;
	}

	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Integer getStocks() {
		return stocks;
	}

	public void setStocks(Integer stocks) {
		this.stocks = stocks;
	}
}
