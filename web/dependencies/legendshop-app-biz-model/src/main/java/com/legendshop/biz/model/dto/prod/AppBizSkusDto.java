package com.legendshop.biz.model.dto.prod;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 发布商品 SkusDto
 * @author 27158
 */
@ApiModel(value = "发布商品 SkusDto")
public class AppBizSkusDto implements Serializable {

	/** sku的销售属性组合字符串（颜色，大小，等等，可通过类目API获取某类目下的销售属性）,格式是p1:v1;p2:v2. */
	@ApiModelProperty(value = "sku的销售属性组合字符串（颜色，大小，等等，可通过类目API获取某类目下的销售属性）,格式是p1:v1;p2:v2.",required = true)
	private String properties ;

	/** 中文 销售属性组合. */
	@ApiModelProperty(value = "中文 销售属性组合",required = true)
	private String cnProperties;

	/** 价格. */
	@ApiModelProperty(value = "价格",required = true)
	private Double price ;

	/** SKU商品名称. */
	@ApiModelProperty(value = "SKU商品名称",required = true)
	private String name ;

	/** 商品在付款减库存的状态下，该sku上未付款的订单数量. */
	@ApiModelProperty(value = "商品sku库存",required = true)
	private Long stocks ;


	/** 实际库存. */
	@ApiModelProperty(value = "实际库存 ",required =true)
	private Long actualStocks ;


	/** sku状态。 1l:正常 ；0:删除. */
	@ApiModelProperty(value = "sku状态 默认正常 1 ",required = true)
	private Integer status ;

	/** 商家编码. */
	@ApiModelProperty(value = "商家编码")
	private String partyCode;

	/** 商品条形码. */
	@ApiModelProperty("商品条形码")
	private String modelId;

	/** 图片. */
	@ApiModelProperty(value = "sku图片 选择图片时必填",required = true)
	private String pic;

	/**物流体积(立方米)*/
	@ApiModelProperty("物流体积(立方米)")
	private Double volume;

	/**物流重量(千克)*/
	@ApiModelProperty("物流重量(千克)")
	private Double weight;

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}

	public String getCnProperties() {
		return cnProperties;
	}

	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Long getStocks() {
		return stocks;
	}

	public void setStocks(Long stocks) {
		this.stocks = stocks;
	}

	public Long getActualStocks() {
		return actualStocks;
	}

	public void setActualStocks(Long actualStocks) {
		this.actualStocks = actualStocks;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}
}
