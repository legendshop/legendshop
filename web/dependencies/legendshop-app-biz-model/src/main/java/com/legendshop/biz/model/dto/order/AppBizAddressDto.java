/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.biz.model.dto.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 地址Dto.
 */
@ApiModel(value="收货地址Dto") 
public class AppBizAddressDto implements Serializable {

	private static final long serialVersionUID = 8008194649513962171L;
	
	/** 地址ID  **/
	@ApiModelProperty(value="地址Id")  
	private Long addrId;
	
	/** 接收人 */
	@ApiModelProperty(value="接收人")  
	private String receiver;
	
	/** 用户ID */
	@ApiModelProperty(value="用户ID")  
	private String userId;
	
	/**版本号**/
	@ApiModelProperty(value="版本号")  
	private int version;
	
	/** 用户名称 */
	@ApiModelProperty(value="用户名称")  
	private String userName;
	
	@ApiModelProperty(value="主键")  
	private long id;
	
	/** 地址 */
	@ApiModelProperty(value="地址")  
	private String subAdds;
	
	/** 邮编 */
	@ApiModelProperty(value="邮编")  
	private String subPost;
	
	/** 详细收货地址  */
	@ApiModelProperty(value="详细收货地址")  
	private String detailAddress;

	/** 省份 */
	@ApiModelProperty(value="省份") 
	private String province;

	/** 城市 */
	@ApiModelProperty(value="城市") 
	private String city;

	/** 地区 */
	@ApiModelProperty(value="地区") 
	private String area;
	
	/** 省份Id */
	@ApiModelProperty(value="省份Id") 
	private Integer provinceId;

	/** 城市Id */
	@ApiModelProperty(value="城市Id") 
	private Integer cityId;

	/** 地区Id */
	@ApiModelProperty(value="地区Id") 
	private Integer areaId;

	/** 手机号码 */
	@ApiModelProperty(value="地区Id") 
	private String mobile;

	/** 固话号码 */
	@ApiModelProperty(value="固话号码") 
	private String telphone;

	/** 常用邮箱 */
	@ApiModelProperty(value="email") 
	private String email;

	/** 常用地址 */
	@ApiModelProperty(value="常用地址") 
	private String commonAddr;

	/** 地址别名 **/
	@ApiModelProperty(value="地址别名") 
	private String aliasAddr;

	public Long getAddrId() {
		return addrId;
	}

	public void setAddrId(Long addrId) {
		this.addrId = addrId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getSubAdds() {
		return subAdds;
	}

	public void setSubAdds(String subAdds) {
		this.subAdds = subAdds;
	}

	public String getSubPost() {
		return subPost;
	}

	public void setSubPost(String subPost) {
		this.subPost = subPost;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCommonAddr() {
		return commonAddr;
	}

	public void setCommonAddr(String commonAddr) {
		this.commonAddr = commonAddr;
	}

	public String getAliasAddr() {
		return aliasAddr;
	}

	public void setAliasAddr(String aliasAddr) {
		this.aliasAddr = aliasAddr;
	}

	public Integer getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public String getDetailAddress() {
		return detailAddress;
	}

	public void setDetailAddress(String detailAddress) {
		this.detailAddress = detailAddress;
	}
	
	
	
}
