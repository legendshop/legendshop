package com.legendshop.biz.model.dto.productAttribute;

import com.legendshop.dao.support.GenericEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *接受返回KeyValue的Dto
 */
@ApiModel("KeyValueDto")
public class AppBizKeyValueDto{

	private static final long serialVersionUID = -6273562045198816196L;

	/** 主键 */
	@ApiModelProperty(value = "主键")
	private String key;

	/** 动态模板名称 */
	@ApiModelProperty(value = "动态模板名称")
	private String value;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
