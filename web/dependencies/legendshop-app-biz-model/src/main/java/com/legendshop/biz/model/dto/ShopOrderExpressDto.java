package com.legendshop.biz.model.dto;

import java.io.Serializable;

import com.legendshop.model.entity.UserAddressSub;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value="ShopOrderExpressDto") 
public class ShopOrderExpressDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1817715713145062366L;

	@ApiModelProperty(value="查看快递返回")
	private ShopDelivryReturnDto delivryReturnDto;
	
	@ApiModelProperty(value="用户配送地址")
	private UserAddressSub userAddressSub;
	
	@ApiModelProperty(value="订单备注")
	private String orderRemark;

	public ShopDelivryReturnDto getDelivryReturnDto() {
		return delivryReturnDto;
	}

	public void setDelivryReturnDto(ShopDelivryReturnDto delivryReturnDto) {
		this.delivryReturnDto = delivryReturnDto;
	}

	public UserAddressSub getUserAddressSub() {
		return userAddressSub;
	}

	public void setUserAddressSub(UserAddressSub userAddressSub) {
		this.userAddressSub = userAddressSub;
	}

	public String getOrderRemark() {
		return orderRemark;
	}

	public void setOrderRemark(String orderRemark) {
		this.orderRemark = orderRemark;
	}
	
	
}
