/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.biz.model.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * 收货地址
 *
 */
@ApiModel(value="商家收货地址Dto") 
public class ShopAddressDto implements Serializable {

	private static final long serialVersionUID = 8008194649513962171L;
	
	@ApiModelProperty(value="地址Id") 
	private Long addrId; 
		
	@ApiModelProperty(value="联系人") 
	private String contactName; 
		
	@ApiModelProperty(value="省份Id") 
	private Integer provinceId; 
		
	@ApiModelProperty(value="城市Id") 
	private Integer cityId; 
		
	@ApiModelProperty(value="地区Id") 
	private Integer areaId; 
		
	@ApiModelProperty(value="联系手机") 
	private String mobile; 
		
	@ApiModelProperty(value="地址") 
	private String adds; 
	
	@ApiModelProperty(value="是否默认地址") 
	private Integer detault;
	
	@ApiModelProperty(value="省份") 
	private String province;
	
	@ApiModelProperty(value="城市") 
	private String city;
	
	/** The area. */
	@ApiModelProperty(value="区域") 
	private String area;

	public Long getAddrId() {
		return addrId;
	}

	public void setAddrId(Long addrId) {
		this.addrId = addrId;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public Integer getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAdds() {
		return adds;
	}

	public void setAdds(String adds) {
		this.adds = adds;
	}

	public Integer getDetault() {
		return detault;
	}

	public void setDetault(Integer detault) {
		this.detault = detault;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}
}
