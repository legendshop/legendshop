package com.legendshop.biz.model.dto.productAttribute;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * app商家端 专门用于发布界面，装载商品分类下的属性，参数和品牌
 * @author 27158
 */
@ApiModel(value ="app商家端 专门用于发布界面，装载商品分类下的属性，参数和品牌" )
public class AppBizPublishProductDto implements Serializable {

	private static final long serialVersionUID = -2390487522090049545L;
	/**
	 * 属性是否可以编辑  1 可以 0 不可以
	 */
	@ApiModelProperty(value = "属性是否可以编辑  1 可以 0 不可以")
	private Integer attrEditable;
	/**
	 * 参数是否可以编辑 1 可以 0 不可以
	 */
	@ApiModelProperty(value = "参数是否可以编辑  1 可以 0 不可以")
	private Integer paramEditable;

	/**
	 * 参数列表，
	 */
	@ApiModelProperty(value = "参数列表")
	private List<AppBizAttdefPropertyDto> propertyDtoList;

	/**
	 * 属性列表
	 */
	@ApiModelProperty(value = "属性列表")
	private List<AppBizAttdefPropertyDto> specDtoList;

	/**
	 * 品牌列表
	 */
	@ApiModelProperty(value = "品牌列表")
	private List<AppBizBrandDto> brandList;


	public Integer getAttrEditable() {
		return attrEditable;
	}

	public void setAttrEditable(Integer attrEditable) {
		this.attrEditable = attrEditable;
	}

	public Integer getParamEditable() {
		return paramEditable;
	}

	public void setParamEditable(Integer paramEditable) {
		this.paramEditable = paramEditable;
	}

	public List<AppBizAttdefPropertyDto> getPropertyDtoList() {
		return propertyDtoList;
	}

	public void setPropertyDtoList(List<AppBizAttdefPropertyDto> propertyDtoList) {
		this.propertyDtoList = propertyDtoList;
	}

	public List<AppBizAttdefPropertyDto> getSpecDtoList() {
		return specDtoList;
	}

	public void setSpecDtoList(List<AppBizAttdefPropertyDto> specDtoList) {
		this.specDtoList = specDtoList;
	}

	public List<AppBizBrandDto> getBrandList() {
		return brandList;
	}

	public void setBrandList(List<AppBizBrandDto> brandList) {
		this.brandList = brandList;
	}
}
