package com.legendshop.biz.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * 营销活动Dto
 *
 * @author 27158
 */
@ApiModel(value="营销活动Dto")
public class AppBizMarketingDto implements Serializable {

	private static final long serialVersionUID = -4237611290216242129L;
	/** 营销活动编号 */
	@ApiModelProperty(value ="营销活动编号,编辑时必填")
	private Long id;

	/** 活动名称 */
	@ApiModelProperty(value ="活动名称")
	private String marketName;

	/** 活动开始时间 */
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value ="活动开始时间")
	private Date startTime;

	/** 活动结束时间 */
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value ="活动结束时间")
	private Date endTime;

	/** 用户编号 */
	@ApiModelProperty(value ="用户编号")
	private String userId;

	/** 店铺ID */
	@ApiModelProperty(value ="店铺ID")
	private Long shopId;

	/** 0: 满减促销 ; 1: 满折促销 2：限时促销 */
	@ApiModelProperty(value ="活动类型 0: 满减促销 ; 1: 满折促销 2：限时促销")
	private Integer type;

	/** 是否全部商品[全店] 部分商品[需要添加活动商品] */
	@ApiModelProperty(value ="1：全部 0 ：部分")
	private Integer isAllProds;

	/** (0-未发布/1-正在进行/2-暂停/3-下线)', */
	@ApiModelProperty(value ="活动状态 0-未发布/1-正在进行/2-暂停/3-下线")
	private Integer state;


	/** skuid集合*/
	@ApiModelProperty(value ="skuid集合,选择全部商品 skuid 为 null ")
	private Long[] skuIds;

	/**
	 * 满折规格属性
	 */
	@ApiModelProperty(value ="满折规格属性,选择满折：必填，参数模板: 金额,折数,计算类型：0:按金额  1：按件")
	private String[] manzeRule;
	/**
	 * 满减规格属性
	 */
	@ApiModelProperty(value ="满减规格属性,选择满减：必填，参数模板: 金额,优惠数,计算类型：0:按金额  1：按件")
	private String[] manjianRule;

	/**
	 * 限时活动商品折扣数
	 */
	@ApiModelProperty(value ="限时活动商品折扣数：数值范围 1-9.9 ,限时折扣活动，必填 满折规格属性为 null")
	private Float discount;

	/** 活动备注 */
	@ApiModelProperty(value ="活动备注")
	private String remark;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMarketName() {
		return marketName;
	}

	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getIsAllProds() {
		return isAllProds;
	}

	public void setIsAllProds(Integer isAllProds) {
		this.isAllProds = isAllProds;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}


	public Long[] getSkuIds() {
		return skuIds;
	}

	public void setSkuIds(Long[] skuIds) {
		this.skuIds = skuIds;
	}


	public String[] getManzeRule() {
		return manzeRule;
	}

	public void setManzeRule(String[] manzeRule) {
		this.manzeRule = manzeRule;
	}

	public String[] getManjianRule() {
		return manjianRule;
	}

	public void setManjianRule(String[] manjianRule) {
		this.manjianRule = manjianRule;
	}

	public Float getDiscount() {
		return discount;
	}

	public void setDiscount(Float discount) {
		this.discount = discount;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
