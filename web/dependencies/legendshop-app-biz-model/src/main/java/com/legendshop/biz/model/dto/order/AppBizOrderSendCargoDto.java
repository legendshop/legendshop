/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.biz.model.dto.order;

import com.legendshop.model.entity.InvoiceSub;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * App用于封装查询订单数据.
 */

@ApiModel("用户订单列表Dto")
public class AppBizOrderSendCargoDto implements Serializable{

	private static final long serialVersionUID = -2864810122569245142L;

	/** 订单ID **/
	@ApiModelProperty(value = "订单ID")
	private Long subId;


	/** 用户ID **/
	@ApiModelProperty(value = "用户ID")
	private String userId;

	/** 下订单的时间 **/
	@ApiModelProperty(value = "下订单的时间 ")
	private Date subDate;

	/** 支付的时间  **/
	@ApiModelProperty(value = "支付的时间")
	private Date payDate;


	/** 订单号 **/
	@ApiModelProperty(value = "订单号")
	private String subNumber;

	/** 订单商品原价  **/
	@ApiModelProperty(value = "订单商品原价")
	private Double total;

	/** 订单商品实际价格(运费 折扣 促销) **/
	@ApiModelProperty(value = "订单商品实际价格(运费 折扣 促销)")
	private Double actualTotal;

	/** 商城ID **/
	@ApiModelProperty(value = "商城ID")
	private Long shopId;

	/** 配送类型  **/
	@ApiModelProperty(value = "配送类型")
	private String dvyType;

	/** 配送方式ID **/
	@ApiModelProperty(value = "配送方式ID")
	private Long dvyTypeId;

	/** 物流单号 **/
	@ApiModelProperty(value = "物流单号 ")
	private String dvyFlowId;

	/** 物流费用 **/
	@ApiModelProperty(value = "物流费用")
	private Double freightAmount;


	/** 用户订单地址ID **/
	@ApiModelProperty(value = "用户订单地址ID")
	private Long addrOrderId;


	/** 订单商品总数 **/
	@ApiModelProperty(value = "订单商品总数")
	private Integer productNums;

	/** 发货时间 */
	@ApiModelProperty(value = "发货时间")
	private Date dvyDate;


	/** 是否货到付款 **/
	@ApiModelProperty(value = "是否货到付款")
	private Integer isCod;

	/** 是否已经支付 **/
	@ApiModelProperty(value = "是否已经支付")
	private Integer isPayed;


	/** 订单项列表 **/
	@ApiModelProperty(value = "订单项列表")
	private List<AppBizOrderItemDto> subOrderItemDtos;

	/** 用户收货地址  **/
	@ApiModelProperty(value = "用户收货地址")
	private AppBizAddressDto addressDto;

	/** 物流信息  **/
	@ApiModelProperty(value = "物流信息")
	private AppBizDeliveryDto deliveryDto;


	@ApiModelProperty(value="用于倒计时的时间,待付款时是付款截止时间， 待成团是成团截止倒计时, 待收货时是确认收货截止时间")
	private Date countDownTime;


	@ApiModelProperty(value="订单已调整的金额[负数为减，正数为加需要添加'+'显示]")
	private Double changedPrice;

	//预售订单字段
	/** 支付方式,0:全额,1:定金 */
	@ApiModelProperty(value="支付方式  0:全额,1:定金")
	private Integer payPctType;

	/** 定金金额 */
	@ApiModelProperty(value="定金金额")
	private BigDecimal preDepositPrice;

	/** 尾款金额 */
	@ApiModelProperty(value="尾款金额 ")
	private BigDecimal finalPrice;

	/** 尾款支付开始时间 */
	@ApiModelProperty(value="尾款支付开始时间 ")
	private Date finalMStart;

	/** 尾款支付结束时间 */
	@ApiModelProperty(value="尾款支付结束时间 ")
	private Date finalMEnd;

	/** 定金支付名称 */
	@ApiModelProperty(value="定金支付名称 ")
	private String depositPayName;

	/** 定金支付流水号 */
	@ApiModelProperty(value="定金支付流水号 ")
	private String depositTradeNo;

	/** 定金支付时间 */
	@ApiModelProperty(value="定金支付时间 ")
	private Date depositPayTime;

	/** 是否支付定金 */
	@ApiModelProperty(value="是否支付定金 ")
	private Integer isPayDeposit;

	/** 尾款支付名称 */
	@ApiModelProperty(value="尾款支付名称 ")
	private String finalPayName;

	/** 尾款支付时间 */
	@ApiModelProperty(value="尾款支付时间  ")
	private Date finalPayTime;

	/** 尾款流水号 */
	@ApiModelProperty(value="尾款流水号  ")
	private String finalTradeNo;

	/** 是否支付尾款 */
	@ApiModelProperty(value="是否支付尾款")
	private Integer isPayFinal;

	/**是否已经备注**/
	@ApiModelProperty(value="是否已经备注")
	private Boolean isShopRemarked;

	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getSubDate() {
		return subDate;
	}

	public void setSubDate(Date subDate) {
		this.subDate = subDate;
	}

	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getActualTotal() {
		return actualTotal;
	}

	public void setActualTotal(Double actualTotal) {
		this.actualTotal = actualTotal;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getDvyType() {
		return dvyType;
	}

	public void setDvyType(String dvyType) {
		this.dvyType = dvyType;
	}

	public Long getDvyTypeId() {
		return dvyTypeId;
	}

	public void setDvyTypeId(Long dvyTypeId) {
		this.dvyTypeId = dvyTypeId;
	}

	public String getDvyFlowId() {
		return dvyFlowId;
	}

	public void setDvyFlowId(String dvyFlowId) {
		this.dvyFlowId = dvyFlowId;
	}

	public Double getFreightAmount() {
		return freightAmount;
	}

	public void setFreightAmount(Double freightAmount) {
		this.freightAmount = freightAmount;
	}

	public Long getAddrOrderId() {
		return addrOrderId;
	}

	public void setAddrOrderId(Long addrOrderId) {
		this.addrOrderId = addrOrderId;
	}

	public Integer getProductNums() {
		return productNums;
	}

	public void setProductNums(Integer productNums) {
		this.productNums = productNums;
	}

	public Date getDvyDate() {
		return dvyDate;
	}

	public void setDvyDate(Date dvyDate) {
		this.dvyDate = dvyDate;
	}

	public Integer getIsCod() {
		return isCod;
	}

	public void setIsCod(Integer isCod) {
		this.isCod = isCod;
	}

	public Integer getIsPayed() {
		return isPayed;
	}

	public void setIsPayed(Integer isPayed) {
		this.isPayed = isPayed;
	}

	public List<AppBizOrderItemDto> getSubOrderItemDtos() {
		return subOrderItemDtos;
	}

	public void setSubOrderItemDtos(List<AppBizOrderItemDto> subOrderItemDtos) {
		this.subOrderItemDtos = subOrderItemDtos;
	}

	public AppBizAddressDto getAddressDto() {
		return addressDto;
	}

	public void setAddressDto(AppBizAddressDto addressDto) {
		this.addressDto = addressDto;
	}

	public AppBizDeliveryDto getDeliveryDto() {
		return deliveryDto;
	}

	public void setDeliveryDto(AppBizDeliveryDto deliveryDto) {
		this.deliveryDto = deliveryDto;
	}

	public Date getCountDownTime() {
		return countDownTime;
	}

	public void setCountDownTime(Date countDownTime) {
		this.countDownTime = countDownTime;
	}

	public Double getChangedPrice() {
		return changedPrice;
	}

	public void setChangedPrice(Double changedPrice) {
		this.changedPrice = changedPrice;
	}

	public Integer getPayPctType() {
		return payPctType;
	}

	public void setPayPctType(Integer payPctType) {
		this.payPctType = payPctType;
	}

	public BigDecimal getPreDepositPrice() {
		return preDepositPrice;
	}

	public void setPreDepositPrice(BigDecimal preDepositPrice) {
		this.preDepositPrice = preDepositPrice;
	}

	public BigDecimal getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(BigDecimal finalPrice) {
		this.finalPrice = finalPrice;
	}

	public Date getFinalMStart() {
		return finalMStart;
	}

	public void setFinalMStart(Date finalMStart) {
		this.finalMStart = finalMStart;
	}

	public Date getFinalMEnd() {
		return finalMEnd;
	}

	public void setFinalMEnd(Date finalMEnd) {
		this.finalMEnd = finalMEnd;
	}

	public String getDepositPayName() {
		return depositPayName;
	}

	public void setDepositPayName(String depositPayName) {
		this.depositPayName = depositPayName;
	}

	public String getDepositTradeNo() {
		return depositTradeNo;
	}

	public void setDepositTradeNo(String depositTradeNo) {
		this.depositTradeNo = depositTradeNo;
	}

	public Date getDepositPayTime() {
		return depositPayTime;
	}

	public void setDepositPayTime(Date depositPayTime) {
		this.depositPayTime = depositPayTime;
	}

	public Integer getIsPayDeposit() {
		return isPayDeposit;
	}

	public void setIsPayDeposit(Integer isPayDeposit) {
		this.isPayDeposit = isPayDeposit;
	}

	public String getFinalPayName() {
		return finalPayName;
	}

	public void setFinalPayName(String finalPayName) {
		this.finalPayName = finalPayName;
	}

	public Date getFinalPayTime() {
		return finalPayTime;
	}

	public void setFinalPayTime(Date finalPayTime) {
		this.finalPayTime = finalPayTime;
	}

	public String getFinalTradeNo() {
		return finalTradeNo;
	}

	public void setFinalTradeNo(String finalTradeNo) {
		this.finalTradeNo = finalTradeNo;
	}

	public Integer getIsPayFinal() {
		return isPayFinal;
	}

	public void setIsPayFinal(Integer isPayFinal) {
		this.isPayFinal = isPayFinal;
	}


	public Boolean getShopRemarked() {
		return isShopRemarked;
	}

	public void setShopRemarked(Boolean shopRemarked) {
		isShopRemarked = shopRemarked;
	}
}
