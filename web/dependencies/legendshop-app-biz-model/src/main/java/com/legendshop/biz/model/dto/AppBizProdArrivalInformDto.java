package com.legendshop.biz.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * 商家端APP 到货通知
 * @author linzh
 */
@ApiModel(value ="到货通知详情")
public class AppBizProdArrivalInformDto implements Serializable{


	private static final long serialVersionUID = 8490970701812933204L;

	/** 到货通知ID */
	@ApiModelProperty(value = "到货通知ID")
	private Long prodArrivalInformId;

	/** 商品ID */
	@ApiModelProperty(value = "商品ID")
	private Long prodId;

	/** skuId */
	@ApiModelProperty(value = "skuId")
	private Long skuId;

	/** 商品名称 */
	@ApiModelProperty(value = "商品名称")
	private String productName;

	/** 商品价格 */
	@ApiModelProperty(value = "商品价格")
	private String price;

	/** 商品属性 */
	@ApiModelProperty(value = "商品属性")
	private String cnProperties;

	/** 商品图片 */
	@ApiModelProperty(value = "商品图片")
	private String picture;

	/** 用户名称 */
	@ApiModelProperty(value = "用户名称")
	private String userName;

	/** 手机号码 */
	@ApiModelProperty(value = "手机号码")
	private String mobilePhone;

	/** 邮箱 */
	@ApiModelProperty(value = "邮箱")
	private String email;

	/** 添加到货通知的时间 */
	@ApiModelProperty(value = "添加到货通知的时间")
	private Date createTime;


	public Long getProdArrivalInformId() {
		return prodArrivalInformId;
	}

	public void setProdArrivalInformId(Long prodArrivalInformId) {
		this.prodArrivalInformId = prodArrivalInformId;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getCnProperties() {
		return cnProperties;
	}

	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}
}
