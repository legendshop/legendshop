package com.legendshop.biz.model.dto.prod;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;


/**
 * 商品分类DTO
 */
@ApiModel(value = "商品分类Dto")
public class AppBizProdCategoryDto implements Serializable {
	private static final long serialVersionUID = 7536585172554342091L;

	/**
	 * 产品类目ID
	 */
	@ApiModelProperty(value = "产品类目ID")
	private Long id;

	/**
	 * 父节点
	 */
	@ApiModelProperty(value = "父节点")
	private Long parentId;

	/**
	 * 产品类目名称
	 */
	@ApiModelProperty(value = "产品类目名称")
	private String name;

	/**
	 * 分类小图标
	 */
	@ApiModelProperty(value = "分类小图标")
	private String icon;

	/**
	 * 类目的显示图片
	 */
	@ApiModelProperty(value = "类目的显示图片")
	private String pic;

	@ApiModelProperty(value = "产品类目类型,普通商品订单 P,团购商品订单 G,二手商品订单 S,打折商品 D,积分商品 I,秒杀 S")
	private String type;

	/**
	 * 排序
	 */
	@ApiModelProperty(value = "排序")
	private Integer seq;

	/**
	 * 默认是1，表示正常状态,0为下线状态
	 */
	@ApiModelProperty(value = "默认是1，表示正常状态,0为下线状态")
	private Integer status;

	/**
	 * 类型ID
	 */
	@ApiModelProperty(value = "类型ID")
	private Integer typeId;

	/**
	 * 类型名称
	 */
	@ApiModelProperty(value = "类型名称")
	private String typeName;

	/**
	 * 分类层级
	 */
	@ApiModelProperty(value = "分类层级")
	private Integer grade;

	/**
	 * 是否有下级分类
	 */
	@ApiModelProperty(value = "是否有下级分类")
	private Integer isNext;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public Integer getIsNext() {
		return isNext;
	}

	public void setIsNext(Integer isNext) {
		this.isNext = isNext;
	}
}
