package com.legendshop.biz.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * app 运费模板DTo
 * @author 27158
 */
@ApiModel(value = "app 运费模板DTo")
public class AppBizTransportDto implements Serializable {
	private static final long serialVersionUID = 4172917886430589889L;
	/**
	 * 运费模板Id
	 */
	@ApiModelProperty(value = "运费模板Id")
	private Long id ;

	/**
	 * 运费模板名称
	 */
	@ApiModelProperty(value = "运费模板名称")
	private String transName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTransName() {
		return transName;
	}

	public void setTransName(String transName) {
		this.transName = transName;
	}
}
