package com.legendshop.biz.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * app优惠卷Dto
 */
@ApiModel(value = "商家优惠卷Dto")
public class AppBizCouponDto implements Serializable {

	private static final long serialVersionUID = 7904309491810013385L;

	/** 优惠券 ID */
	@ApiModelProperty(value="优惠券 ID")
	private Long couponId;

	/** 优惠券 名称 */
	@ApiModelProperty(value="优惠券名称")
	private String couponName;

	/** 优惠券状态 */
	@ApiModelProperty(value="优惠券状态 1 有效 0 失效")
	private Integer status;

	/** 劵值满多少金额 */
	@ApiModelProperty(value="劵值满多少金额")
	private Double fullPrice;

	/** 劵值减多少金额 */
	@ApiModelProperty(value="劵值减多少金额")
	private Double offPrice;

	/** 活动开始时间 */
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value="活动开始时间")
	private Date startDate;

	/** 活动结束时间 */
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value="活动结束时间")
	private Date endDate;

	/** 优惠券提供方：平台: platform，店铺:shop */
	@ApiModelProperty(value="优惠券提供方：平台: platform，店铺:shop")
	private String couponProvider;

	/** 领取方式：积分兑换:points，卡密兑换:pwd，免费领取:free */
	@ApiModelProperty(value="领取方式：积分兑换:points，卡密兑换:pwd，免费领取:free,大转盘红包:draw")
	private String getType;

	/** 兑换所需积分（领取方式为"积分兑换"时生效） **/
	@ApiModelProperty(value="兑换所需积分（领取方式为'积分兑换' 必填）")
	private Integer needPoints;

	/** 优惠券图片 **/
	@ApiModelProperty(value="优惠券图片")
	private String couponPic;

	/** 优惠券的类型 */
	@ApiModelProperty(value="礼券类型：通用券:common，品类券:category，指定商品券:product")
	private String couponType;

	/** 领取上限 */
	@ApiModelProperty(value="领取上限,不可为0 如 指定用户为 null")
	private Long getLimit;

	/** 初始化券数量 */
	@ApiModelProperty(value="初始化券数量 不可为0 ,如指定用户为 null")
	private Long couponNumber;

	/** 描述 */
	@ApiModelProperty(value="描述")
	private String description;

	/** 店铺id */
	@ApiModelProperty(value="店铺id")
	private Long shopId;

	/** 店铺名称 */
	@ApiModelProperty(value="店铺名称")
	private String shopName;

	/** 商品id列表 */
	@ApiModelProperty(value="商品id列表,选择商品卷必填格式：659,667,658 ")
	private String prodidList;

	/**是否对指定用户发放*/
	@ApiModelProperty(value="是否指定用户,1:指定用户，0：没有指定用户 状态为 1 时 领取方式只能是免费领取:free ")
	private Integer isDsignatedUser;

	/**发布体验劵时,所选的用户id*/
	@ApiModelProperty(value="指定用户发放ID 指定时必填 格式 用户ID以,分隔")
	private String userIdLists;

	public Long getCouponId() {
		return couponId;
	}

	public void setCouponId(Long couponId) {
		this.couponId = couponId;
	}

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Double getFullPrice() {
		return fullPrice;
	}

	public void setFullPrice(Double fullPrice) {
		this.fullPrice = fullPrice;
	}

	public Double getOffPrice() {
		return offPrice;
	}

	public void setOffPrice(Double offPrice) {
		this.offPrice = offPrice;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getCouponProvider() {
		return couponProvider;
	}

	public void setCouponProvider(String couponProvider) {
		this.couponProvider = couponProvider;
	}

	public String getGetType() {
		return getType;
	}

	public void setGetType(String getType) {
		this.getType = getType;
	}

	public Integer getNeedPoints() {
		return needPoints;
	}

	public void setNeedPoints(Integer needPoints) {
		this.needPoints = needPoints;
	}

	public String getCouponPic() {
		return couponPic;
	}

	public void setCouponPic(String couponPic) {
		this.couponPic = couponPic;
	}

	public String getCouponType() {
		return couponType;
	}

	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}

	public Long getGetLimit() {
		return getLimit;
	}

	public void setGetLimit(Long getLimit) {
		this.getLimit = getLimit;
	}

	public Long getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(Long couponNumber) {
		this.couponNumber = couponNumber;
	}



	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getProdidList() {
		return prodidList;
	}

	public void setProdidList(String prodidList) {
		this.prodidList = prodidList;
	}

	public Integer getIsDsignatedUser() {
		return isDsignatedUser;
	}

	public void setIsDsignatedUser(Integer isDsignatedUser) {
		this.isDsignatedUser = isDsignatedUser;
	}

	public String getUserIdLists() {
		return userIdLists;
	}

	public void setUserIdLists(String userIdLists) {
		this.userIdLists = userIdLists;
	}

}
