package com.legendshop.biz.model.dto.prod;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

@ApiModel("商品属性值Dto")
public class AppBizProductPropertyValueDto implements Serializable {

	private static final long serialVersionUID = 6598280148952180408L;

	/**
	 * 属性值ID
	 **/
	@ApiModelProperty(value = "属性值ID")
	private Long valueId;

	/**
	 * 属性ID
	 **/
	@ApiModelProperty(value = "属性ID")
	private Long propId;

	/**
	 * 属性值名称
	 **/
	@ApiModelProperty(value = "属性值名称")
	private String name;

	//属性值别名
	@ApiModelProperty(value = "属性值别名")
	private String alias;

	// 状态
	@ApiModelProperty(value = "状态")
	private Short status;

	// 图片路径
	@ApiModelProperty(value = "图片路径")
	private String pic;

	// 排序
	@ApiModelProperty(value = "排序")
	private Long sequence;

	// 修改时间
	@ApiModelProperty(value = "修改时间")
	private Date modifyDate;

	// 记录时间
	@ApiModelProperty(value = "记录时间")
	private Date recDate;

	/**
	 * 是否被sku选中
	 **/
	@ApiModelProperty(value = "是否被sku选中")
	private boolean isSelected = false;

	public Long getValueId() {
		return valueId;
	}

	public void setValueId(Long valueId) {
		this.valueId = valueId;
	}

	public Long getPropId() {
		return propId;
	}

	public void setPropId(Long propId) {
		this.propId = propId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Long getSequence() {
		return sequence;
	}

	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean selected) {
		isSelected = selected;
	}
}
