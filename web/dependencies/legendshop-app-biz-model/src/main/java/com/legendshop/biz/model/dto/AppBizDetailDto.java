package com.legendshop.biz.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 商家详情Dto
 *@author 27158
 */
@ApiModel(value="商家详情Dto")
public class AppBizDetailDto {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9153182932362623738L;

	/** 商家Id，全局唯一. */
	@ApiModelProperty(value = "商家Id，全局唯一",required = true)
	protected Long shopId;

	/** 用户Id. */
	@ApiModelProperty(value = "用户Id")
	protected String userId;

	/** 用户名. */
	@ApiModelProperty(value = "用户名")
	protected String userName;

	/** 商城名字. */
	@ApiModelProperty(value = "商城名字")
	protected String siteName;

	/** logo图片. */
	@ApiModelProperty(value = "logo图片")
	protected String logoPic;

	/** 联系人姓名. */
	@ApiModelProperty(value = "联系人姓名")
	protected String contactName;

	/** 联系人手机. */
	@ApiModelProperty(value = "联系人手机")
	protected String contactMobile;

	/** 省份Id. */
	@ApiModelProperty(value = "省份Id",required = true)
	protected Integer provinceId;

	/** 城市Id. */
	@ApiModelProperty(value = "城市Id",required = true)
	protected Integer cityId;

	/** 地区Id. */
	@ApiModelProperty(value = "地区Id",required = true)
	protected Integer areaId;

	/** 省份. */
	@ApiModelProperty(value ="省份" )
	protected String province;

	/** 城市. */
	@ApiModelProperty(value = "城市")
	protected String city;

	/** 地区. */
	@ApiModelProperty(value = "地区")
	protected String area;

	/** 店铺地址. */
	@ApiModelProperty(value = "店铺地址",required = true)
	protected String shopAddr;


	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getShopAddr() {
		return shopAddr;
	}

	public void setShopAddr(String shopAddr) {
		this.shopAddr = shopAddr;
	}

	public String getLogoPic() {
		return logoPic;
	}

	public void setLogoPic(String logoPic) {
		this.logoPic = logoPic;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactMobile() {
		return contactMobile;
	}

	public void setContactMobile(String contactMobile) {
		this.contactMobile = contactMobile;
	}

	public Integer getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

}
