package com.legendshop.biz.model.dto.productAttribute;

import com.legendshop.model.dto.TreeNodeComparator;
import com.legendshop.model.entity.Category;
import com.legendshop.util.AppUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 商品分类树结构.
 *
 * @author tony
 */
@ApiModel("商品分类树结构")
public class AppBizTreeNode{

	@ApiModelProperty(value = "id")
	private long id;

	@ApiModelProperty(value = "父id")
	private long parentId;

	@ApiModelProperty(value = "节点名")
	protected String nodeName;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}
}
