package com.legendshop.biz.model.dto.productAttribute;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 *	平台参数dto
 * @author 27158
 */
@ApiModel(value = "平台商品参数Dto")
public class AppBizAttdefPropertyDto extends AppBizProductPropertyDto implements Serializable {

	private static final long serialVersionUID = 8598279170195522460L;

	/**
	 * 属性对应的属性值集合
	 */
	@ApiModelProperty(value="属性对应的属性值集合")
	private List<AppBizAttdefPropertyValueDto>  PropertyValueList;


	public List<AppBizAttdefPropertyValueDto> getPropertyValueList() {
		return PropertyValueList;
	}

	public void setPropertyValueList(List<AppBizAttdefPropertyValueDto> propertyValueList) {
		PropertyValueList = propertyValueList;
	}
}
