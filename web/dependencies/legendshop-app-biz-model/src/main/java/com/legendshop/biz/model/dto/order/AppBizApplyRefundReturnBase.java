/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.biz.model.dto.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

/**
 * 地址Dto.
 */
@ApiModel(value="预售退定金Dto")
public class AppBizApplyRefundReturnBase {
	
	/** 订单ID **/
	@ApiModelProperty(value = "订单ID")
	protected Long orderId;

	/** 原因 */
	@ApiModelProperty(value = "原因")
	protected String buyerMessage;
	
	/** 退款金额 */
	@ApiModelProperty(value = "退款金额")
	protected Double refundAmount;
	
	/**预售订单，订金退款金额*/
	@ApiModelProperty(value = "预售订单，订金退款金额")
	protected Double depositRefundAmount;
	
	/**是否退订金*/
	@ApiModelProperty(value = "是否退订金")
	protected Boolean isRefundDeposit;
	
	/** 原因说明 */
	@ApiModelProperty(value = "原因说明")
	protected String reasonInfo;
	
	/** 凭证图片1 */
	@ApiModelProperty(value = "凭证图片1")
	protected String photoFile1;
	
	/** 凭证图片2 */
	@ApiModelProperty(value = "凭证图片2")
	protected String photoFile2;
	
	/** 凭证图片3 */
	@ApiModelProperty(value = "凭证图片3")
	protected String photoFile3;

	/** 订单项 ID */
	@ApiModelProperty(value = "订单项ID")
	private Long orderItemId;


	public String getBuyerMessage() {
		return buyerMessage;
	}


	public void setBuyerMessage(String buyerMessage) {
		this.buyerMessage = buyerMessage;
	}
	

	public Long getOrderId() {
		return orderId;
	}


	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}


	public Double getRefundAmount() {
		return refundAmount;
	}


	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}


	public String getReasonInfo() {
		return reasonInfo;
	}


	public void setReasonInfo(String reasonInfo) {
		this.reasonInfo = reasonInfo;
	}

	public Double getDepositRefundAmount() {
		return depositRefundAmount;
	}

	public void setDepositRefundAmount(Double depositRefundAmount) {
		this.depositRefundAmount = depositRefundAmount;
	}

	public Boolean getIsRefundDeposit() {
		return isRefundDeposit;
	}

	public void setIsRefundDeposit(Boolean isRefundDeposit) {
		this.isRefundDeposit = isRefundDeposit;
	}

	public Long getOrderItemId() {
		return orderItemId;
	}

	public void setOrderItemId(Long orderItemId) {
		this.orderItemId = orderItemId;
	}

	public String getPhotoFile1() {
		return photoFile1;
	}

	public void setPhotoFile1(String photoFile1) {
		this.photoFile1 = photoFile1;
	}

	public String getPhotoFile2() {
		return photoFile2;
	}

	public void setPhotoFile2(String photoFile2) {
		this.photoFile2 = photoFile2;
	}

	public String getPhotoFile3() {
		return photoFile3;
	}

	public void setPhotoFile3(String photoFile3) {
		this.photoFile3 = photoFile3;
	}
}
