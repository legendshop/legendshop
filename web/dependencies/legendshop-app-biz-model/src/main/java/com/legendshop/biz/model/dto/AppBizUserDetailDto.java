package com.legendshop.biz.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author 27158
 */
@ApiModel(value="用户详情Dto")
public class AppBizUserDetailDto implements Serializable {

	private static final long serialVersionUID = 8008194649513962171L;

	/** 用户ID */
	@ApiModelProperty(value="用户ID")  
	private String userId;
	
	/** 用户名称 */
	@ApiModelProperty(value="用户名称")  
	private String userName;

	/** 昵称 */
	@ApiModelProperty(value="昵称，加判断如果为空显示'暂无昵称'")  
	private String nickName;
	
	/** 性别  **/
	@ApiModelProperty(value="性别")  
	private String sex;
	
	 /** 头像图片 **/
	@ApiModelProperty(value="头像图片")  
    private String portraitPic;

    /**登入手机号码*/
	@ApiModelProperty(value="登入手机号码")
    private String mobile;

	/**姓名**/
	@ApiModelProperty(value="姓名，加判断如果为空显示'暂无'")  
	private String realName ;


	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPortraitPic() {
		return portraitPic;
	}

	public void setPortraitPic(String portraitPic) {
		this.portraitPic = portraitPic;
	}
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

}

