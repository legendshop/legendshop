package com.legendshop.biz.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * app活动规则Dto
 * @author 27158
 */
@ApiModel(value = "活动规则Dto")
public class AppBizMarketingRuleDto implements Serializable {

	private static final long serialVersionUID = 7159038737227406494L;
	/** 金额触发规则 */
	@ApiModelProperty(value="金额触发规则")
	private Double fullPrice;
	/**
	 * 满折优惠
	 */
	@ApiModelProperty(value = "满折优惠")
	private Float offDiscount;

	/** 满减优惠 */
	@ApiModelProperty(value = "满减优惠")
	private Double offAmount;

	/** 计算类型：0:按金额  1：按件 */
	@ApiModelProperty(value = "计算类型：0:按金额  1：按件")
	private Integer calType;

	public Double getFullPrice() {
		return fullPrice;
	}

	public void setFullPrice(Double fullPrice) {
		this.fullPrice = fullPrice;
	}

	public Float getOffDiscount() {
		return offDiscount;
	}

	public void setOffDiscount(Float offDiscount) {
		this.offDiscount = offDiscount;
	}

	public Double getOffAmount() {
		return offAmount;
	}

	public void setOffAmount(Double offAmount) {
		this.offAmount = offAmount;
	}

	public Integer getCalType() {
		return calType;
	}

	public void setCalType(Integer calType) {
		this.calType = calType;
	}
}
