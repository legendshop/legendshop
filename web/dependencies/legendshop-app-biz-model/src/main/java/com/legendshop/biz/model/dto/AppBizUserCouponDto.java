package com.legendshop.biz.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * app用户优惠券Dto
 * @author Joe
 */
@ApiModel(value = "用户优惠券")
@Data
public class AppBizUserCouponDto implements Serializable {


	@ApiModelProperty(value="用户优惠券ID")
	private Long userCouponId;

	@ApiModelProperty(value="优惠券名称")
	private String couponName;

	@ApiModelProperty(value="劵号")
	private String couponSn;

	@ApiModelProperty(value="卡密")
	private String couponPwd;

	@ApiModelProperty(value="用户名")
	private String userName;

	@ApiModelProperty(value="领取时间")
	private Date getTime;

	@ApiModelProperty(value="使用时间")
	private Date useTime;

	@ApiModelProperty(value="优惠券使用状态 1:可使用  2:已使用")
	private Integer useStatus;


}
