package com.legendshop.biz.model.dto.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

@ApiModel("订单项Dto")
public class AppBizOrderItemDto implements Serializable {

	private static final long serialVersionUID = 5354203715512352213L;

	/** 订单项ID */
	@ApiModelProperty(value = "订单项ID")
	private Long subItemId; 
	
	/** 订单号 */
	@ApiModelProperty(value = "订单号")
	private String subNumber; 
	
	/** 订单项流水号 */
	@ApiModelProperty(value = "订单项流水号")
	private String subItemNumber; 
		
	/** 产品ID */
	@ApiModelProperty(value = "产品ID")
	private Long prodId; 
		
	/** 产品SkuID */
	@ApiModelProperty(value = "产品SkuID")
	private Long skuId; 

		
	/** 购物车产品个数 */
	@ApiModelProperty(value = "购物车产品个数 ")
	private long basketCount; 
		
	/** 产品名称 */
	@ApiModelProperty(value = "产品名称")
	private String prodName; 

	/** 产品主图片路径 */
	@ApiModelProperty(value = "产品主图片路径")
	private String pic; 
		
	/** 产品原价 */
	@ApiModelProperty(value = "产品原价")
	private Double price; 
		
	/** 产品现价 */
	@ApiModelProperty(value = "产品现价")
	private Double cash; 
		
	/** 用户名称 */
	@ApiModelProperty(value = "用户名称 ")
	private String userId; 
		
	/** 商品总金额 */
	@ApiModelProperty(value = "商品总金额")
	private Double productTotalAmout;

	/** 商品属性 */
	@ApiModelProperty(value = "商品属性")
	private String attribute;
		
	/** 获得积分 */
	@ApiModelProperty(value = "获得积分")
	private Integer obtainIntegral; 
		
	/** 购物时间 */
	@ApiModelProperty(value = "购物时间")
	private Date subItemDate; 
	
	/** 物流重量*/
	@ApiModelProperty(value = "物流重量")
	private Double weight;
	
	/** 物流体积*/
	@ApiModelProperty(value = "物流体积")
	private Double volume;
	
	/** 是否已经评价 */
	@ApiModelProperty(value = "是否已经评价")
	private Integer commSts;
	
	/** 退款/退货记录ID */
	@ApiModelProperty(value = "退款/退货记录ID")
	private Long refundId;
	
	/** 退款/退货状态    0:没发起过退款,1:在处理,2:处理完成,-1:不同意 */
	@ApiModelProperty(value = "退款/退货状态    0:没发起过退款,1:在处理,2:处理完成,-1:不同意")
	private Long refundState;
	
	/** 退款金额 */
	@ApiModelProperty(value = "退款金额 ")
	private Double refundAmount;
	
	/** 申请类型:1:仅退款,2:退款退货 */
	@ApiModelProperty(value = "申请类型:1:仅退款,2:退款退货")
	private Long refundType;
	
	/** 退货数量 */
	@ApiModelProperty(value = "退货数量")
	private Long refundCount;
	
	/** 是否在退换货有效时间内 */
	@ApiModelProperty(value = "是否在退换货有效时间内（逻辑暂未实现）")
	private Boolean isInReturnValidPeriod;

	public Long getSubItemId() {
		return subItemId;
	}

	public void setSubItemId(Long subItemId) {
		this.subItemId = subItemId;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public String getSubItemNumber() {
		return subItemNumber;
	}

	public void setSubItemNumber(String subItemNumber) {
		this.subItemNumber = subItemNumber;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}



	public long getBasketCount() {
		return basketCount;
	}

	public void setBasketCount(long basketCount) {
		this.basketCount = basketCount;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}


	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Double getProductTotalAmout() {
		return productTotalAmout;
	}

	public void setProductTotalAmout(Double productTotalAmout) {
		this.productTotalAmout = productTotalAmout;
	}

	public Integer getObtainIntegral() {
		return obtainIntegral;
	}

	public void setObtainIntegral(Integer obtainIntegral) {
		this.obtainIntegral = obtainIntegral;
	}

	public Date getSubItemDate() {
		return subItemDate;
	}

	public void setSubItemDate(Date subItemDate) {
		this.subItemDate = subItemDate;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Integer getCommSts() {
		return commSts;
	}

	public void setCommSts(Integer commSts) {
		this.commSts = commSts;
	}

	public Long getRefundId() {
		return refundId;
	}

	public void setRefundId(Long refundId) {
		this.refundId = refundId;
	}

	public Long getRefundState() {
		return refundState;
	}

	public void setRefundState(Long refundState) {
		this.refundState = refundState;
	}

	public Double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}

	public Long getRefundType() {
		return refundType;
	}

	public void setRefundType(Long refundType) {
		this.refundType = refundType;
	}

	public Long getRefundCount() {
		return refundCount;
	}

	public void setRefundCount(Long refundCount) {
		this.refundCount = refundCount;
	}

	public Boolean getIsInReturnValidPeriod() {
		return isInReturnValidPeriod;
	}

	public void setIsInReturnValidPeriod(Boolean isInReturnValidPeriod) {
		this.isInReturnValidPeriod = isInReturnValidPeriod;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
}
