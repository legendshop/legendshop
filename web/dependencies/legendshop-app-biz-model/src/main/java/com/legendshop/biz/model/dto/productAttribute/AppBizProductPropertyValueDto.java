package com.legendshop.biz.model.dto.productAttribute;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author 27158
 */
@ApiModel(value="商品属性值Dto")
public class AppBizProductPropertyValueDto implements Serializable {
	private static final long serialVersionUID = -18160800891749344L;
	/**
	 * 属性值ID
	 */
	@ApiModelProperty(value="属性值ID")
	private Long valueId;

	/**
	 * 属性ID
	 */
	@ApiModelProperty(value="属性ID")
	private Long propId;

	/**
	 * 属性值名称
	 */
	@ApiModelProperty(value="属性值名称")
	private String name;

	/**
	 * 属性值名称 别名
	 */
	@ApiModelProperty(value="属性值名称 别名")
	private String alias;

	/**
	 * 状态
	 */
	@ApiModelProperty(value="状态")
	private Short status;

	/**
	 * 图片路径
	 */
	@ApiModelProperty(value="图片路径")
	private String pic;

	/**
	 * 排序
	 */
	@ApiModelProperty(value="排序")
	private String sequence;

	/**
	 * 修改时间
	 */
	@ApiModelProperty(value="修改时间")
	private Date modifyDate;

	/**
	 * 记录时间
	 */
	@ApiModelProperty(value="记录时间")
	private Date recDate;


	public Long getValueId() {
		return valueId;
	}

	public void setValueId(Long valueId) {
		this.valueId = valueId;
	}

	public Long getPropId() {
		return propId;
	}

	public void setPropId(Long propId) {
		this.propId = propId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}
}
