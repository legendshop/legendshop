package com.legendshop.biz.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * app 商家端 包邮活动Dto
 * @author 27158
 */
@ApiModel(value = "包邮活动Dto")
public class AppBizShippingActiveDto implements Serializable {
	private static final long serialVersionUID = -1942690008570069757L;
	/**
	 * 活动ID
	 */
	@ApiModelProperty(value = "包邮活动ID,编辑必填",required = true)
	private Long id;

	/**
	 * 商家ID
	 */
	@ApiModelProperty(value = "商家ID")
	private Long shopId;

	/**
	 * 活动名称
	 */
	@ApiModelProperty(value = "活动名称",required = true)
	private String name;

	/**
	 * 状态有效:1 下线：0
	 */
	@ApiModelProperty(value = "状态有效:1 下线：0")
	private Boolean status;

	/**
	 * 店铺:0, 商品:1
	 */
	@ApiModelProperty(value = "商品 全部:0, 部分:1",required = true)
	private Integer fullType;

	/**
	 * 类型1：元 2 件
	 **/
	@ApiModelProperty(value = "规则类型 1 元 2 件",required = true)
	private Integer reduceType;

	/**
	 * 满足的金额或件数
	 ***/
	@ApiModelProperty(value = "满足的金额或件数",required = true)
	private Double fullValue;


	/**
	 * 开始时间
	 */
	@ApiModelProperty(value = "开始时间",required = true)
	private Date startDate;

	/**
	 * 结束时间
	 */
	@ApiModelProperty(value = "结束时间",required = true)
	private Date endDate;

	/**
	 * 活动商品ID 集合
	 */
	@ApiModelProperty(value = "活动商品ID，选择全部为 null",required = true)
	private Long [] prodIds;
	/**
	 * 关联商品
	 */
	@ApiModelProperty(value = "关联商品dto")
	private List<AppBizMarketingProdDto> marketingProdDtos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Integer getFullType() {
		return fullType;
	}

	public void setFullType(Integer fullType) {
		this.fullType = fullType;
	}

	public Integer getReduceType() {
		return reduceType;
	}

	public void setReduceType(Integer reduceType) {
		this.reduceType = reduceType;
	}

	public Double getFullValue() {
		return fullValue;
	}

	public void setFullValue(Double fullValue) {
		this.fullValue = fullValue;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Long[] getProdIds() {
		return prodIds;
	}

	public void setProdIds(Long[] prodIds) {
		this.prodIds = prodIds;
	}

	public List<AppBizMarketingProdDto> getMarketingProdDtos() {
		return marketingProdDtos;
	}

	public void setMarketingProdDtos(List<AppBizMarketingProdDto> marketingProdDtos) {
		this.marketingProdDtos = marketingProdDtos;
	}
}
