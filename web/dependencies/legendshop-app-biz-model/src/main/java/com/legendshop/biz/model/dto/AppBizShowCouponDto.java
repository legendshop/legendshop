package com.legendshop.biz.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * app优惠卷Dto
 * @author 27158
 */
@ApiModel(value = "商家优惠卷Dto")
public class AppBizShowCouponDto extends  AppBizCouponDto {
	private static final long serialVersionUID = -5202075487533600920L;
	/** 绑定优惠券数量 */
	@ApiModelProperty(value="优惠卷领取数")
	private Long bindCouponNumber;

	/** 领取人数 */
	@ApiModelProperty(value="领取人数")
	private Integer drawUserNum;

	/** 使用优惠券数量 */
	@ApiModelProperty(value="使用优惠券数量")
	private Long useCouponNumber;

	/**关联商品 */
	@ApiModelProperty(value ="关联商品集合")
	private List<AppBizMarketingProdDto> marketingProd;

	public Long getBindCouponNumber() {
		return bindCouponNumber;
	}

	public void setBindCouponNumber(Long bindCouponNumber) {
		this.bindCouponNumber = bindCouponNumber;
	}

	public Integer getDrawUserNum() {
		return drawUserNum;
	}

	public void setDrawUserNum(Integer drawUserNum) {
		this.drawUserNum = drawUserNum;
	}

	public Long getUseCouponNumber() {
		return useCouponNumber;
	}

	public void setUseCouponNumber(Long useCouponNumber) {
		this.useCouponNumber = useCouponNumber;
	}

	public List<AppBizMarketingProdDto> getMarketingProd() {
		return marketingProd;
	}

	public void setMarketingProd(List<AppBizMarketingProdDto> marketingProd) {
		this.marketingProd = marketingProd;
	}
}
