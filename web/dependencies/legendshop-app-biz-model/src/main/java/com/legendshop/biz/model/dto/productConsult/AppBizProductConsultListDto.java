package com.legendshop.biz.model.dto.productConsult;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;


/**
 * 商家端商品咨询列表Dto
 * @author linzh
 */
@ApiModel(value="商品咨询列表")
public class AppBizProductConsultListDto implements Serializable{

	private static final long serialVersionUID = 8008194649513962171L;

	/** 咨询ID */
	@ApiModelProperty(value="咨询ID")
	private Long consId;

	/** 商品ID */
	@ApiModelProperty(value="商品ID ")
	private Long prodId;

	/** 商品名称 */
	@ApiModelProperty(value="商品名称")
	private String prodName;

	/** 商品图片 */
	@ApiModelProperty(value="商品图片")
	private String prodPic;

	/** 用户ID */
	@ApiModelProperty(value="用户ID")
	private String userId;

	/** 咨询用户名 */
	@ApiModelProperty(value="咨询用户名")
	private String askUserName;

	/** 咨询的用户头像 为空显示默认头像 */
	@ApiModelProperty(value="咨询的用户头像 为空显示默认头像")
	private String portraitPic;

	/** 咨询的用户昵称 */
	@ApiModelProperty(value="咨询的用户昵称")
	private String nickName;

	/** 咨询类型,1: 商品咨询, 2:库存配送, 3:售后咨询 */
	@ApiModelProperty(value="咨询类型,1: 商品咨询, 2:库存配送, 3:售后咨询")
	private Integer pointType;

	/**回复状态：0 未回复 1 已回复*/
	@ApiModelProperty(value="回复状态：0 未回复 1 已回复")
	private Integer replySts;



	public Long getConsId() {
		return consId;
	}

	public void setConsId(Long consId) {
		this.consId = consId;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getProdPic() {
		return prodPic;
	}

	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAskUserName() {
		return askUserName;
	}

	public void setAskUserName(String askUserName) {
		this.askUserName = askUserName;
	}

	public String getPortraitPic() {
		return portraitPic;
	}

	public void setPortraitPic(String portraitPic) {
		this.portraitPic = portraitPic;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Integer getPointType() {
		return pointType;
	}

	public void setPointType(Integer pointType) {
		this.pointType = pointType;
	}

	public Integer getReplySts() {
		return replySts;
	}

	public void setReplySts(Integer replySts) {
		this.replySts = replySts;
	}
}
