package com.legendshop.biz.model.dto.order;

import com.legendshop.util.Arith;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

@ApiModel("订单价格Dto")
public class AppBizOrderPriceDto {

	/** 订单ID **/
	@ApiModelProperty(value = "订单ID")
	private Long subId;

	/** 订单号 **/
	@ApiModelProperty(value = "订单号")
		private String subNumber;

	/** 订单商品原价  **/
	@ApiModelProperty(value = "订单商品原价")
	private Double total;

	/** 订单商品实际价格(运费 折扣 促销) **/
	@ApiModelProperty(value = "订单商品实际价格(运费 折扣 促销)")
	private Double actualTotal;

	/** 物流费用 **/
	@ApiModelProperty(value = "物流费用")
	private Double freightAmount;

	/** 订单类型[普通订单：NORMAL，秒杀订单：SECKILL  门店订单：SHOP_STORE 预售订单：PRE_SELL 团购：GROUP 拼团：MERGE_GROUP 拍卖；AUCTIONS  示例：subType=“NORMAL”] **/
	@ApiModelProperty(value = "订单类型[普通订单：NORMAL，秒杀订单：SECKILL  门店订单：SHOP_STORE 预售订单：PRE_SELL 团购：GROUP 拼团：MERGE_GROUP 拍卖；AUCTIONS  示例：subType=“NORMAL”]")
	private String subType;

	/** 定金金额 */
	@ApiModelProperty(value="定金金额")
	private BigDecimal preDepositPrice;

	/** 尾款金额 */
	@ApiModelProperty(value="尾款金额 ")
	private BigDecimal finalPrice;

	/** 支付方式,0:全额,1:订金 */
	@ApiModelProperty(value="支付方式  0:全额,1:订金 ")
	private Integer payPctType;

	/** 用户名 */
	@ApiModelProperty(value="用户名")
	private String userName;

	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public Double getTotal() {

		return Arith.sub(actualTotal,freightAmount);
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getActualTotal() {
		return actualTotal;
	}

	public void setActualTotal(Double actualTotal) {
		this.actualTotal = actualTotal;
	}

	public Double getFreightAmount() {
		return freightAmount;
	}

	public void setFreightAmount(Double freightAmount) {
		this.freightAmount = freightAmount;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public BigDecimal getPreDepositPrice() {
		return preDepositPrice;
	}

	public void setPreDepositPrice(BigDecimal preDepositPrice) {
		this.preDepositPrice = preDepositPrice;
	}

	public BigDecimal getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(BigDecimal finalPrice) {
		this.finalPrice = finalPrice;
	}

	public Integer getPayPctType() {
		return payPctType;
	}

	public void setPayPctType(Integer payPctType) {
		this.payPctType = payPctType;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
