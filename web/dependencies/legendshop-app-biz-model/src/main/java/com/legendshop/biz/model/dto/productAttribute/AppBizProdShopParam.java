package com.legendshop.biz.model.dto.productAttribute;

import com.legendshop.dao.persistence.*;
import com.legendshop.dao.support.GenericEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 *商家自定义商品参数
 */
@ApiModel("商家自定义商品参数")
public class AppBizProdShopParam implements GenericEntity<Long> {

	private static final long serialVersionUID = -6273562045198816196L;

	/** 主键 */
	@ApiModelProperty(value = "主键")
	private Long id;

	/** 动态模板名称 */
	@ApiModelProperty(value = "动态模板名称")
	private String name;

	/** 内容 */
	@ApiModelProperty(value = "内容")
	private String content;

	/** 用户名 */
	@ApiModelProperty(value = "用户id")
	private String userId;

	@ApiModelProperty(value = "商家id")
	private Long shopId;

	/** 加入时间 */
	@ApiModelProperty(value = "加入时间")
	private Date recDate;


	public AppBizProdShopParam() {
    }

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}
}
