/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.biz.model.dto;

import com.legendshop.dao.persistence.*;
import com.legendshop.dao.support.GenericEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.Date;

/**
 * 商家端 商品举报
 * @author linzh
 */
@ApiModel(value = "商品举报")
public class AppBizAccusationDto implements Serializable {

	private static final long serialVersionUID = -2153571302559983720L;

	/* 列表所需字段 */
	/** 举报ID */
	@ApiModelProperty(value = "举报ID")
	private Long id;

	/** 举报类型 */
	@ApiModelProperty(value = "举报类型")
	private String accuType;

	/** 处理结果 1:无效举报 2：有效举报  3：恶意举报 */
	@ApiModelProperty(value = "处理结果 1:无效举报 2：有效举报  3：恶意举报")
	private Long result;

	/** 举报状态 0:未处理， 1:已经处理 */
	@ApiModelProperty(value = "举报状态 0:未处理， 1:已经处理")
	private Long status;

	/** 商品Id */
	@ApiModelProperty(value = "商品ID")
	private Long prodId;

	/** 商品名称 */
	@ApiModelProperty(value = "商品名称")
	private String prodName;

	/** 商品图片 */
	@ApiModelProperty(value = "商品图片")
	private String prodPic;

	/* 列表所需字段 */

	/** 举报主题 */
	@ApiModelProperty(value = "举报主题")
	private String title;

	/** 举报时间 */
	@ApiModelProperty(value = "举报时间")
	private Date recDate ;

	/** 举报内容 */
	@ApiModelProperty(value = "举报内容")
	private String content ;

	/** 用户是否删除,0未删除,1已经删除 */
	@ApiModelProperty(value = "用户是否删除,0未删除,1已经删除")
	private Integer userDelStatus;

	/** 处理意见 */
	@ApiModelProperty(value = "处理意见")
	private String handleInfo ;

	/** 处理时间 */
	@ApiModelProperty(value = "处理时间")
	private Date handleTime ;

	/** 商品是否已下架 0：否 1：是  */
	@ApiModelProperty(value = "商品是否已下架 0：否 1：是")
	private Integer illegalOff;

	/** 取证图片 pic1 */
	@ApiModelProperty(value = "取证图片 pic1")
	private String pic1 ;

	/** 取证图片 pic2 */
	@ApiModelProperty(value = "取证图片 pic2")
	private String pic2 ;

	/** 取证图片 pic3 */
	@ApiModelProperty(value = "取证图片 pic3")
	private String pic3 ;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccuType() {
		return accuType;
	}

	public void setAccuType(String accuType) {
		this.accuType = accuType;
	}

	public Long getResult() {
		return result;
	}

	public void setResult(Long result) {
		this.result = result;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getProdPic() {
		return prodPic;
	}

	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getUserDelStatus() {
		return userDelStatus;
	}

	public void setUserDelStatus(Integer userDelStatus) {
		this.userDelStatus = userDelStatus;
	}

	public String getHandleInfo() {
		return handleInfo;
	}

	public void setHandleInfo(String handleInfo) {
		this.handleInfo = handleInfo;
	}

	public Date getHandleTime() {
		return handleTime;
	}

	public void setHandleTime(Date handleTime) {
		this.handleTime = handleTime;
	}

	public Integer getIllegalOff() {
		return illegalOff;
	}

	public void setIllegalOff(Integer illegalOff) {
		this.illegalOff = illegalOff;
	}

	public String getPic1() {
		return pic1;
	}

	public void setPic1(String pic1) {
		this.pic1 = pic1;
	}

	public String getPic2() {
		return pic2;
	}

	public void setPic2(String pic2) {
		this.pic2 = pic2;
	}

	public String getPic3() {
		return pic3;
	}

	public void setPic3(String pic3) {
		this.pic3 = pic3;
	}
}
