package com.legendshop.advanced.search.util;

import java.util.ArrayList;

import org.apache.commons.lang.math.NumberUtils;

import com.legendshop.advanced.search.entity.ProductIndexEntity;
import com.legendshop.model.dto.search.AttrParam;
import com.legendshop.util.AppUtils;

/**
 * 
 * @author tony
 *
 */
public class SearchUrlUtils {

	/**
	 * 移除所有品牌参数
	 * 
	 * @param urlQueryString
	 * @return
	 */
	public static String removeAllBrand(String urlQueryString) {
		return removeAllParams(urlQueryString, ProductIndexEntity.BRAND_ID);
	}

	/**
	 * 将原来的值全部替换成新的
	 * 
	 * @param urlQueryString
	 * @param field
	 * @param value
	 * @return
	 */
	public static String replaceAllParams(String urlQueryString, String field, String value) {
		if (value != null) {
			return appendParams(removeAllParams(urlQueryString, field), field, value);
		}
		return urlQueryString;
	}

	public static String removeAllParams(String urlQueryString, String field) {
		SearchParam searchParam = decodeByUrl(urlQueryString);
		AttrParam[] params = new AttrParam[searchParam.getParams().size()];
		searchParam.getParams().toArray(params);
		for (AttrParam param : params) {
			if (param.getField().equals(field)) {
				searchParam.getParams().remove(param);
			}
		}
		return searchParam.getUrlQueryString();
	}

	public static String appendParams(String urlQueryString, String key, String value) {
		if (AppUtils.isBlank(key) || AppUtils.isBlank(value)) {
			return null;
		}
		AttrParam param = new AttrParam(key, value);
		String currUrlQueryString = urlQueryString == null ? "" : urlQueryString;
		if (!currUrlQueryString.contains(key + ":" + value)) {
			if (AppUtils.isBlank(currUrlQueryString)) {
				return currUrlQueryString + param.getUrlQueryString();
			} else {
				return currUrlQueryString + ";" + param.getUrlQueryString();
			}
		}
		return currUrlQueryString;
	}

	public static SearchParam decodeByUrl(String urlQueryString) {
		if (AppUtils.isBlank(urlQueryString)) {
			return new SearchParam();
		}
		SearchParam searchParam = new SearchParam();
		String[] urlParams = urlQueryString.split(";");
		ArrayList<AttrParam> params = new ArrayList<AttrParam>();
		if(AppUtils.isNotBlank(urlParams)){
			for(int i=0;i<urlParams.length;i++){
				AttrParam param = decodeAttrParam(urlParams[i]);
				if (param != null) {
					params.add(param);
				}
			}
		}
		searchParam.setParams(params);
		return searchParam;
	}

	public static AttrParam decodeAttrParam(String urlQueryString) {
		String[] keyPair = urlQueryString.split(":");
		
		if (keyPair.length != 2 || AppUtils.isBlank(keyPair[1])) {
			return null;
		}
		//防止Integer参数被攻击
		if(ProductIndexEntity.BRAND_ID.equals(keyPair[0].trim())){
			String number=keyPair[1].trim();
			if(!NumberUtils.isNumber(number)){
				return null;
			}
		}

		AttrParam param = new AttrParam();
		ArrayList<String> paramValues = new ArrayList<String>();
		for (String value : keyPair[1].split(",")) {
			paramValues.add(value.trim());
		}
		param.setField(keyPair[0].trim());
		param.setValues(paramValues);
		return param;
	}

}
