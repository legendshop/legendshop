package com.legendshop.advanced.search.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.stereotype.Service;

import com.legendshop.advanced.search.entity.IndexEntity;
import com.legendshop.advanced.search.server.IndexServerManager;
import com.legendshop.advanced.search.service.IndexBuildProvider;
import com.legendshop.advanced.search.service.IndexBuilderService;
import com.legendshop.advanced.search.util.IndexDocUtil;
import com.legendshop.advanced.search.util.IndexLog;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.IndexObjectTypeCodeEnum;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;


/**
 * 
 * @author tony
 *
 */
@Service("indexBuilderService")
public class IndexBuilderServiceImpl implements IndexBuilderService {

	@Override
	public void commit(List<Long> ids, IndexBuildProvider provider,IndexObjectTypeCodeEnum core) {
		if (ids.isEmpty()) {
			return;
		}
		SolrClient server = IndexServerManager.getInstance().getSearchServer(core,Constants.SOLR_OPERATION_CREATE);
		List<IndexEntity> beans = new ArrayList<IndexEntity>();
		List<String> addIds = new ArrayList<String>();
		List<String> deleteIds = new ArrayList<String>();
		for (Long id : ids) {
			IndexEntity bean = provider.getEntity(id);
			if (AppUtils.isBlank(bean)) {
				deleteIds.add(String.valueOf(id));
				continue;
			}
			beans.add(bean);
			addIds.add(String.valueOf(id));
		}
		try {
			// 更新索引
			if (beans.size() > 0) {
				IndexDocUtil.addBeans(server, beans);
				IndexLog.log("add beans [ {} ] ", JSONUtil.getJson(addIds));
			}
			if (deleteIds.size() > 0) {
				server.deleteById(deleteIds);
				IndexLog.log("delete solr index [ {} ]",JSONUtil.getJson(deleteIds));
			}
			synchronized (server) {
				server.commit();
			}
		} catch (SolrServerException e) {
			IndexLog.error("多条索引创建 失败",e);
		} catch (IOException e) {
			IndexLog.error("多条索引创建 失败",e);
		}
		IndexLog.log("多条索引创建----执行批量" + core + "索引数据，批量数量有" + beans.size());
	}

	@Override
	public void commit(Long id, IndexEntity entity, IndexObjectTypeCodeEnum core) {
		if (id == null) {
			return;
		}
		SolrClient server = IndexServerManager.getInstance().getSearchServer(core,Constants.SOLR_OPERATION_CREATE);
		try {
			if (entity == null) {
				server.deleteById(String.valueOf(id));
				IndexLog.log("delete solr index [ {} ]",id);
			} else {
				IndexLog.log("add bean [ {} ] ",id);
				IndexDocUtil.addBean(server, entity);
			}
			synchronized (server) {
				server.commit();
			}
		} catch (SolrServerException e) {
			IndexLog.error("单条索引创建失败:",e);
		} catch (IOException e) {
			IndexLog.error("单条索引创建失败:",e);
		}
		IndexLog.log("单条索引创建----执行" + core + "索引数据，ObjectId=" + id);
	}

	@Override
	public void batchSaveOrUpdateCommit(List<IndexEntity> beans,IndexObjectTypeCodeEnum core) {
		if (AppUtils.isBlank(beans)) {
			return;
		}
		try {
			SolrClient server = IndexServerManager.getInstance().getSearchServer(core,Constants.SOLR_OPERATION_CREATE);
			List<IndexEntity> newbeans = new ArrayList<IndexEntity>();
			for (IndexEntity searchEntity : beans) {
				if (AppUtils.isBlank(searchEntity)) {
					continue;
				}
			    newbeans.add(searchEntity);
			}
			// 更新索引
			if (newbeans.size() > 0) {
				IndexDocUtil.addBeans(server, newbeans);
			}
			synchronized (server) {
				server.commit();
			}
		} catch (SolrServerException e) {
			IndexLog.error("批量更新保存失败",e);
		} catch (IOException e) {
			IndexLog.error("批量更新保存失败",e);
		}
		IndexLog.log("批量更新保存----执行批量" + core + "索引数据，批量数量有" + beans.size());
	}

	@Override
	public boolean batchDeleteCommit(List<String> ids,IndexObjectTypeCodeEnum core) {
		boolean success = false;
		try {
			SolrClient server = IndexServerManager.getInstance().getSearchServer(core,Constants.SOLR_OPERATION_CREATE);
			server.deleteById(ids);
			IndexLog.log("delete solr index [ {} ]",JSONUtil.getJson(ids));
			synchronized (server) {
				server.commit();
			}
			success = true;
		} catch (SolrServerException e) {
			IndexLog.error("执行批量删除索引数据" + core + "失败",e);
			success = false;
		} catch (IOException e) {
			IndexLog.error("执行批量删除索引数据" + core + "失败",e);
			success = false;
		}
		return success;
	}

	/**
	 * 删除所有的索引
	 */
	@Override
	public boolean rebuileBytype(IndexObjectTypeCodeEnum core) {
		boolean success = false;
		IndexLog.log("执行" + core + "块的全部索引重建");
		SolrClient server = IndexServerManager.getInstance().getSearchServer(core,Constants.SOLR_OPERATION_CREATE);
		try {
			server.deleteByQuery("*:*");
			success=true;
		} catch (SolrServerException e) {
			IndexLog.error("删除索引文件" + core + "失败",e);
		} catch (IOException e) {
			IndexLog.error("删除索引文件" + core + "失败",e);
		}
		return success;
	}

}
