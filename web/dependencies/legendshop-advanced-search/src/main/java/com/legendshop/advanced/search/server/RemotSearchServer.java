package com.legendshop.advanced.search.server;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.BinaryRequestWriter;
import org.apache.solr.client.solrj.impl.HttpSolrClient;

import com.legendshop.advanced.search.util.IndexLog;
import com.legendshop.model.constant.IndexObjectTypeCodeEnum;

/**
 * 索引服务远程模式
 * @author tony
 */
public class RemotSearchServer implements ISearchServerFactory{

	private final Map<String, SolrClient> searchServerMap =new ConcurrentHashMap<String, SolrClient>();
	
	public RemotSearchServer(String solrUrl){
		IndexObjectTypeCodeEnum [] types=IndexObjectTypeCodeEnum.values();
		for (IndexObjectTypeCodeEnum _enum : types) {
			String searchName=_enum.value();
			String coreUrl=solrUrl; //http://192.168.1.1:8080/solr/product
			//http://localhost:8080/solr/product/select?indent=on&q=*:*&wt=json
			if(coreUrl.endsWith("/")){
				 coreUrl = coreUrl + searchName.toLowerCase();
			}else{
				  coreUrl =  coreUrl + "/" + searchName.toLowerCase();
			}
			try {
				IndexLog.log("start to create HttpSolrServer coreUrl" + coreUrl);
				HttpSolrClient solrClient = new HttpSolrClient.Builder(coreUrl).build();
				solrClient.setConnectionTimeout(3000);
				solrClient.setSoTimeout(60000);
				solrClient.setMaxTotalConnections(1000);  
				solrClient.setDefaultMaxConnectionsPerHost(100);
				solrClient.setRequestWriter(new BinaryRequestWriter());
		        searchServerMap.put(searchName, solrClient);
		        
			} catch (Exception e) {
				IndexLog.error("solr 远程链接异常:"+e);
			}
		}
	}
	
	@Override
	public SolrClient getSearchServer(IndexObjectTypeCodeEnum searchName) {
		return searchServerMap.get(searchName.value());
	}
	
	

}
