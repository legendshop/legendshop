/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.advanced.search.dao;

import java.util.Date;
import java.util.List;

import com.legendshop.model.entity.ProductDetail;

/**
 * 
 * @author tony
 *
 */
public interface IndexDao {

	
	/**
	 * First post id by date.
	 * 
	 * @param entityType
	 *            the entity type
	 * @param fromDate
	 *            the from date
	 * @return the long
	 */
	public abstract long getFirstPostIdByDate(String entityType, Date fromDate);
	
	/**
	 * Last post id by date.
	 * 
	 * @param entityType
	 *            the entity type
	 * @param toDate
	 *            the to date
	 * @return the long
	 */
	public abstract long getLastPostIdByDate(String entityType, Date toDate);
	
	
	/**
	 * Gets the posts to index.
	 *
	 * @param firstPostId the first post id
	 * @param toPostId the to post id
	 * @param status the status
	 * @return the posts to index
	 */
	List<ProductDetail> getProductToIndex(long firstPostId, long toPostId,Integer status);

	
	
	
	/**
	 * Gets the first post id.
	 *
	 * @return the first post id
	 */
	public abstract long getFirstPostId();

	/**
	 * Gets the last post id.
	 *
	 * @return the last post id
	 */
	public abstract long getLastPostId();

	/**
	 * Gets the product to index2.
	 *
	 * @param firstPostId the first post id
	 * @param toPostId the to post id
	 * @param status the status
	 * @return the product to index2
	 */
	List<ProductDetail> getProductToIndex2(long firstPostId, long toPostId,Integer status);

	/**
	 * Gets the index prod detail.
	 *
	 * @param prodId the prod id
	 * @return the index prod detail
	 */
	public abstract  ProductDetail getIndexProdDetail(Long prodId);


	
}
