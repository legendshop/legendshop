package com.legendshop.advanced.search.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.legendshop.advanced.search.service.IndexBuildManager;
import com.legendshop.advanced.search.service.IndexBuildProvider;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.model.constant.IndexObjectTypeCodeEnum;
import com.legendshop.model.dto.ProductIdDto;
import com.legendshop.util.AppUtils;

/**
 * 商品保存触发索引保存
 */
@Component("productIndexSaveProcessor")
public class ProductIndexSaveProcessor implements EventProcessor<ProductIdDto> {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(ProductIndexSaveProcessor.class);
	
	@Autowired
	private IndexBuildManager indexBuildManager;
	
	@Autowired
	private IndexBuildProvider indexBuildProvider;

	@Override
	@Async
	public void process(ProductIdDto product) {
		if(AppUtils.isBlank(product)){
			return ;
		}
		try {
			for (Long prodId : product.getProdIdList()) {
				log.debug("Product updated by  id {} ", prodId);
				indexBuildManager.saveOrUpdate(prodId,indexBuildProvider, IndexObjectTypeCodeEnum.PRODUCT);
			}
			
		} catch (Exception e) {
			log.error("update product event failed", e);
		}
	}
}
