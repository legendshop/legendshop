package com.legendshop.advanced.search.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.advanced.search.entity.IndexEntity;
import com.legendshop.advanced.search.service.IndexBuildManager;
import com.legendshop.advanced.search.service.IndexBuildProvider;
import com.legendshop.advanced.search.service.IndexBuilderService;
import com.legendshop.advanced.search.util.IndexLog;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.constant.IndexObjectTypeCodeEnum;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 实时建立索引
 * @author tony
 *
 */
@Service("indexBuildManager")
public class RealTimeIndexManagerImpl implements IndexBuildManager {

	@Autowired
	private IndexBuilderService indexBuilderService;
	
	@Override
	public boolean rebuileBytype(IndexObjectTypeCodeEnum codeEnum) {
		return indexBuilderService.rebuileBytype(codeEnum);
	}

	@Override
	public void batchSave(List<GenericEntity> entitys,IndexBuildProvider provider, IndexObjectTypeCodeEnum core) {
		if (AppUtils.isBlank(entitys)) {
			return;
		}
		List<IndexEntity> addEntitys = new ArrayList<IndexEntity>();
		List<String> delids = new ArrayList<String>();
		List<String> addids=new ArrayList<String>();
		for (GenericEntity entity : entitys) {
			  IndexEntity searchEntity = provider.convertEntity(entity);
		      if(AppUtils.isBlank(searchEntity)){
		    	  delids.add(String.valueOf(entity.getId()));
			   }else{
				  addids.add(String.valueOf(entity.getId()));
				  addEntitys.add(searchEntity);
			  }
		}
		if (AppUtils.isNotBlank(addEntitys)) {
			IndexLog.log("add Index beans [ {} ] ",JSONUtil.getJson(addids));
			indexBuilderService.batchSaveOrUpdateCommit(addEntitys, core);
		}
		if(AppUtils.isNotBlank(delids)){
			indexBuilderService.batchDeleteCommit(delids, core);
		}
		addEntitys=null;
		delids=null;
		addids=null;
	}

	@Override
	public void batchDelete(List<String> deleteIds, IndexObjectTypeCodeEnum core) {
		if (AppUtils.isBlank(deleteIds)) {
			return;
		}
	   indexBuilderService.batchDeleteCommit(deleteIds, core);
	}

	@Override
	public void delete(Long id, IndexObjectTypeCodeEnum core) {
		indexBuilderService.commit(id, null, core);		
	}

	@Override
	public void saveOrUpdate(Long id, IndexBuildProvider indexBuildProvider,IndexObjectTypeCodeEnum core) {
		IndexEntity searchEntity = indexBuildProvider.getEntity(id);
		indexBuilderService.commit(id, searchEntity, core);
	}
	
	
	
}
