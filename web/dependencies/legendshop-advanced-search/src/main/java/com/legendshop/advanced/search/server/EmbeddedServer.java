package com.legendshop.advanced.search.server;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.embedded.EmbeddedSolrServer;
import org.apache.solr.core.CoreContainer;

import com.legendshop.model.constant.IndexObjectTypeCodeEnum;

/**
 * 本地访问模式
 * @author tony
 *
 */
public class EmbeddedServer implements ISearchServerFactory {
	
	private Map<String, SolrClient> serverMap = Collections.synchronizedMap(new HashMap<String, SolrClient>());
	
	public EmbeddedServer(String seachHome) {
		CoreContainer container = new CoreContainer("C://web-server//apache-tomcat-8.5.23-solr//solr_home");
		container.load( );
		
		IndexObjectTypeCodeEnum [] codeEnums=IndexObjectTypeCodeEnum.values();
		for(IndexObjectTypeCodeEnum codeEnum:codeEnums){
			EmbeddedSolrServer server = new EmbeddedSolrServer( container, codeEnum.value());
			serverMap.put(codeEnum.value(), server);
		}
	}
	
	@Override
	public SolrClient getSearchServer(IndexObjectTypeCodeEnum typeCode) {
		return serverMap.get(typeCode.value());
	}

}
