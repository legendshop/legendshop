package com.legendshop.advanced.search.service;

import java.util.List;

import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.constant.IndexObjectTypeCodeEnum;

/**
 * 索引构建服务
 * @author tony
 *
 */
public interface IndexBuildManager {

	/**
	 * 清除索引数据
	 * @param codeEnum
	 * @return
	 */
    public abstract boolean rebuileBytype(IndexObjectTypeCodeEnum codeEnum);

    /**
     * 批量保存数据
     * @param entitys
     * @param provider
     * @param code
     */
    public abstract void batchSave(List<GenericEntity> entitys, IndexBuildProvider provider, IndexObjectTypeCodeEnum core);

    /**
     * 批量删除索引数据
     * @param deleteIds
     * @param code
     */
	public abstract void batchDelete(List<String> deleteIds,IndexObjectTypeCodeEnum core);

	/**
	 * 删除单条索引数据
	 * @param id
	 * @param product
	 */
	public abstract void delete(Long id, IndexObjectTypeCodeEnum product);

	/**
	 * 保存或更新索引数据
	 * @param id
	 * @param indexBuildProvider
	 * @param product
	 */
	public abstract void saveOrUpdate(Long id,IndexBuildProvider indexBuildProvider,IndexObjectTypeCodeEnum product);

    
}
