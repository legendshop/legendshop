package com.legendshop.advanced.search.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.legendshop.model.dto.search.AttrParam;

/**
 * 
 * @author luojt
 *
 */
public class SearchParam  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<AttrParam> params = new ArrayList<AttrParam>();

	public List<AttrParam> getParams() {
		return params;
	}

	public void setParams(List<AttrParam> params) {
		this.params = params;
	}

	public String getUrlQueryString() {
		StringBuffer buff = new StringBuffer();
		for (int i = 0; i < params.size(); i++) {
			buff.append(params.get(i).getUrlQueryString());
			if (i < params.size() - 1) {
				buff.append(";");
			}
		}
		return buff.toString();
	}

	public String getSolrQueryString() {
		QueryBuilder builder = QueryBuilder.start(Operator.AND);
		Collections.sort(params, new Comparator<AttrParam>() {
			@Override
			public int compare(AttrParam o1, AttrParam o2) {
				return o1.getField().endsWith("_t") ? 0 : 1;
			}
		});
		for (AttrParam param : params) {
			if (param.getValues().size() == 1) {
				builder.addQuery(param.getField(), param.getField().endsWith("_t") ? "\"" + SolrUtil.escape(param.getValues().get(0)) + "\"" : SolrUtil.escape(param.getValues().get(0)));
			} else {
				QueryBuilder subQuery = QueryBuilder.start();
				for (String p : param.getValues()) {
					subQuery.addQuery(param.getField(), param.getField().endsWith("_t") ? "\"" + SolrUtil.escape(SolrUtil.escape(p)) + "\"" : SolrUtil.escape(SolrUtil.escape(p)));
				}
				builder.addSubQuery(subQuery);
			}
		}
		return builder.getQueryString();
	}

	public boolean isSelected(String field, String value) {
		if (StringUtils.isBlank(field) || StringUtils.isBlank(value)) {
			return false;
		}
		for (AttrParam param : getParams()) {
			if (param == null) {
				continue;
			}
			if (param.getField().equals(field) && param.getValues().contains(value)) {
				return true;
			}
		}
		return false;
	}

}
