package com.legendshop.advanced.search.util;


/**
 * 
 * @author tony
 *
 */
public class SolrUtil {
	static final int M_MASK = 0x8765fed1;

	private SolrUtil() {
	}

	public static String escape(String value) {
		char[] dst = new char[value.length()];
		value.getChars(0, value.length(), dst, 0);
		StringBuilder builder = new StringBuilder();
		if (dst.length > 0) {
			for (char c : dst) { // && ||
				switch (c) {
				case '+':
				case '-':
				case '!':
				case '(':
				case ')':
				case '{':
				case '}':
				case '[':
				case ']':
				case '^':
				case '"':
				case '~':
				case '*':
				case '?':
				case ':':
				case '\\':
					builder.append("\\");
					break;
				default:
					break;
				}
				builder.append(c);
			}
		}
		return builder.toString().replace("&&", "\\&&").replace("||", "\\||");
	}

	public static int hash(String key) {
		int hash, i;
		for (hash = 0, i = 0; i < key.length(); ++i) {
			hash += key.charAt(i);
			hash += (hash << 10);
			hash ^= (hash >> 6);
		}
		hash += (hash << 3);
		hash ^= (hash >> 11);
		hash += (hash << 15);
		return hash;
	}

	public static int[] hash(String[] keys) {
		int[] result = new int[keys.length];
		for (int i = 0; i < keys.length; i++) {
			result[i] = hash(keys[i]);
		}
		return result;
	}

	public static String hashString(String key) {
		return String.valueOf(hash(key));
	}

	public static String[] hashString(String[] keys) {
		String[] result = new String[keys.length];
		for (int i = 0; i < keys.length; i++) {
			result[i] = hashString(keys[i]);
		}
		return result;
	}
}
