package com.legendshop.advanced.search.entity;

import java.io.Serializable;

/**
 * 
 * @author tony
 *
 */
public class ProdIndexParam implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long paramId;// 参数id

	private Long paramValueId; // 参数值Id

	private Boolean inputParam;// 参数名

	public Long getParamId() {
		return paramId;
	}

	public void setParamId(Long paramId) {
		this.paramId = paramId;
	}

	public Long getParamValueId() {
		return paramValueId;
	}

	public void setParamValueId(Long paramValueId) {
		this.paramValueId = paramValueId;
	}

	public Boolean getInputParam() {
		return inputParam;
	}

	public void setInputParam(Boolean inputParam) {
		this.inputParam = inputParam;
	}


}
