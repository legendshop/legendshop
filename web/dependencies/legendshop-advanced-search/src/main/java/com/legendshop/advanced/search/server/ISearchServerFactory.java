package com.legendshop.advanced.search.server;

import org.apache.solr.client.solrj.SolrClient;

import com.legendshop.model.constant.IndexObjectTypeCodeEnum;

/**
 * 
 * @author tony
 *
 */
public interface ISearchServerFactory {

	/**
	 * 获取SolrServer
	 * @param typeCodeEnum 索引对象类型
	 * @return
	 */
	SolrClient getSearchServer(IndexObjectTypeCodeEnum typeCodeEnum);
}
