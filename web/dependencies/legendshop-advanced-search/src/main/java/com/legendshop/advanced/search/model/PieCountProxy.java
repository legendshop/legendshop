package com.legendshop.advanced.search.model;

/**
 * 
 * @author tony
 *
 */
public class PieCountProxy extends PieField.PieCount {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String title;
	
	private String url;

	private PieCountProxy(String name, String value, long count, String field, boolean isSelected) {
		super(name, value, count, field, isSelected);
	}

	public PieCountProxy(PieField.PieCount count, String title) {
		super(count.getName(), count.getValue(), count.getCount(), count.getField(), count.isSelected());
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "PieCountProxy [title=" + title + ", url=" + url + "]";
	}
	
	
	
	
	
	
}
