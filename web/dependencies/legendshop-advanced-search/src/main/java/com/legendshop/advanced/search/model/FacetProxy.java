package com.legendshop.advanced.search.model;

import java.util.List;

import com.legendshop.util.AppUtils;

/**
 * 用于前台组装属性列表对象
 */
public class FacetProxy {

	private List<PieCountProxy> selections;

	private List<PieFieldProxy> unSelections;

	private List<PieFieldProxy> allSelections;
	
	private boolean resultlist;  //是否拥有属性数据
	

	public FacetProxy() {
	}

	public List<PieCountProxy> getSelections() {
		return selections;
	}

	public void setSelections(List<PieCountProxy> selections) {
		this.selections = selections;
	}

	public List<PieFieldProxy> getUnSelections() {
		return unSelections;
	}

	public void setUnSelections(List<PieFieldProxy> unSelections) {
		this.unSelections = unSelections;
	}

	public List<PieFieldProxy> getAllSelections() {
		return allSelections;
	}

	public void setAllSelections(List<PieFieldProxy> allSelections) {
		this.allSelections = allSelections;
	}

	public boolean getResultlist() {
		if(AppUtils.isNotBlank(getSelections()) || AppUtils.isNotBlank(getUnSelections())){
			return true;
		}else{
			return false;
		}
	}

	public void setResultlist(boolean resultlist) {
		this.resultlist = resultlist;
	}
	
	
	

}
