package com.legendshop.advanced.search.service.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.advanced.search.dao.IndexDao;
import com.legendshop.advanced.search.entity.IndexEntity;
import com.legendshop.advanced.search.entity.ProdIndexParam;
import com.legendshop.advanced.search.entity.ProductIndexEntity;
import com.legendshop.advanced.search.service.IndexBuildProvider;
import com.legendshop.advanced.search.util.IndexLog;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.dao.TagItemProdDao;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.dto.TreeNode;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.TagItemProd;
import com.legendshop.spi.service.CategoryManagerService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 商品索引提供器
 * 
 * @author luojt
 * 
 */
@Component("indexBuildProvider")
public class ProductIndexBuildProvider implements IndexBuildProvider {

	@Autowired
	private IndexDao indexDao;
	
	@Autowired
	private TagItemProdDao tagItemProdDao;
	
	@Autowired
	private CategoryManagerService categoryManagerService;
	
	@Override
	public IndexEntity convertEntity(@SuppressWarnings("rawtypes") GenericEntity genericentity) {
		ProductDetail product = (ProductDetail) genericentity;
		ProductIndexEntity entity=null;
		if (AppUtils.isBlank(product)) {
			IndexLog.log("ProductIndexProvider convertEntity  exception prod is  empty "); 
			return entity;
		}
		else if(!ProductStatusEnum.PROD_ONLINE.value().equals(product.getStatus())){
			return entity;
		}
		else if(AppUtils.isBlank(product.getCategoryId()) || product.getCategoryId()==0){
			return entity;
		}
		try {
			 entity =buildProductEntity(product);
		} catch (Exception e) {
			IndexLog.error("ProductIndexProvider exception",e);
		}
		return entity;
	}

	@Override
	public IndexEntity getEntity(Long id) {
		ProductDetail productDetail = indexDao.getIndexProdDetail(id);
		ProductIndexEntity entity=null;
		if(AppUtils.isBlank(productDetail)){
			return entity;
		}else if(!ProductStatusEnum.PROD_ONLINE.value().equals(productDetail.getStatus())){
			return entity;
		}
		else if(AppUtils.isBlank(productDetail.getCategoryId()) || productDetail.getCategoryId()==0){
			return entity;
		}
		try {
			 entity =buildProductEntity(productDetail);
		} catch (Exception e) {
			IndexLog.error("ProductIndexProvider exception",e);
		}
		return entity;
	}
	
	private ProductIndexEntity buildProductEntity(ProductDetail product) {
		 if(AppUtils.isBlank(product.getProdId())){
	        	IndexLog.error("ProductIndexProvider buildProductEntity error  params ProdId  Wrong");
				throw new BusinessException("ProductIndexProvider buildProductEntity error  params ProdId Wrong ");
	     }
		 if(AppUtils.isBlank(product.getCash())||product.getCash()<=0){
			 IndexLog.error("ProductIndexProvider buildProductEntity error  params Cash Wrong");
			  throw new BusinessException("ProductIndexProvider buildProductEntity error  params Cash Wrong ");
		  }
		 String [] categorys =getCategoryIds(product.getCategoryId());
		 if(AppUtils.isBlank(categorys)){
			 IndexLog.error("missing  required field categorys by product Id "+product.getProdId());
			 return null;
		 }
		 
		 ProductIndexEntity entity = new ProductIndexEntity();
		 entity.setProdId(product.getProdId());
		 entity.setName(product.getName());
		 entity.setProdName(product.getName());
		 entity.setShopId(product.getShopId());
		 entity.setBrief(product.getBrief());
		 if(AppUtils.isNotBlank(product.getBrandId())&&product.getBrandId()!=0){
		 		entity.setBrandId(product.getBrandId());
			    entity.setBrandName(product.getBrandName());
		   }
		 entity.setPic(product.getPic());
		 entity.setCash(product.getCash());
		 entity.setPrice(product.getPrice());
		 entity.setContent(product.getContent());
		 entity.setCategorys(categorys);
		 entity.setCategoryName(product.getCategoryName());
		 entity.setShopCategorys(getShopCategorys(product));
		 entity.setStatus(product.getStatus());
		 entity.setSupportDist(product.getSupportDist());
		 entity.setActualStocks(product.getActualStocks());
		 entity.setStocks(product.getStocks());
		 entity.setRecDate(product.getRecDate());
		 entity.setViews( product.getViews());
		 entity.setBuys(product.getBuys());
		 entity.setComments(product.getComments());
		 entity.setProdType(product.getProdType());
		 entity.setShopType(product.getShopType());
		 entity.setSearchWeight(product.getSearchWeight());
			//设置商品Tags
		// entity.setTags(getTags(product.getProdId()));
		 
		 setProdParam(entity,product);
		return entity;
	}

	private String[] getTags(Long prodId) {
		String[] tags = null;
		List<TagItemProd> tagItemProds=tagItemProdDao.getTagItemProdsByPid(prodId);
		if(AppUtils.isNotBlank(tagItemProds)){
			int size=tagItemProds.size();
			tags=new String[size];
			for(int i=0;i<size;i++){
				String tem_name=tagItemProds.get(i).getTagName();
				tags[i]=tem_name;
			}
		}
		return tags;
	}

	private void setProdParam(ProductIndexEntity entity, ProductDetail product) {
		if(AppUtils.isBlank(product.getParameter())){
			return ;
		}
		Map<String, String[]> params = null;
		try {
			//[{"inputParam":"true","paramId":"14","paramName":"货号","paramValueId":"","paramValueName":""},{"inputParam":"false","paramId":"2","paramName":"屏幕尺寸","paramValueId":"3","paramValueName":"15寸"},{"inputParam":"false","paramId":"10","paramName":"分辨率","paramValueId":"79","paramValueName":"950X540"},{"inputParam":"false","paramId":"13","paramName":"面板类型","paramValueId":"89","paramValueName":"CPA面板"},{"inputParam":"false","paramId":"9","paramName":"最佳观看距离","paramValueId":"44","paramValueName":"1.1-1.5米"},{"inputParam":"false","paramId":"12","paramName":"HDMI接口数量","paramValueId":"83","paramValueName":"3个"}]
			List<ProdIndexParam> paramDtos =  JSONUtil.getArray(product.getParameter(), ProdIndexParam.class);
			if(AppUtils.isBlank(paramDtos)){
				return;
			}
			params=new HashMap<String, String[]>();	
			for (int i = 0; i < paramDtos.size(); i++) {
				ProdIndexParam dto=paramDtos.get(i);
				if(dto==null)
					continue;
				
				if(!dto.getInputParam()){//如果是输入属性，则不能参与搜索
					long key = dto.getParamId();
					String[] values = new String[1];
					String innerCode = key+ "_t";
					values[0] = key+"_"+ dto.getParamValueId();
					params.put(innerCode, values);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(params!=null){
			entity.setParamValues(params);
		}
	}

	private String[] getShopCategorys(ProductDetail product) {
		Set<String> categorySet = new HashSet<String>();
		if(AppUtils.isNotBlank(product.getShopFirstCatId())){
			categorySet.add(String.valueOf(product.getShopFirstCatId()));
		}
		if(AppUtils.isNotBlank(product.getShopSecondCatId())){
			categorySet.add(String.valueOf(product.getShopSecondCatId()));
		}
		if(AppUtils.isNotBlank(product.getShopThirdCatId())){
			categorySet.add(String.valueOf(product.getShopThirdCatId()));
		}
		String[] categoryPaths = new String[categorySet.size()];
		categorySet.toArray(categoryPaths);
		return categoryPaths;
	}
	
	

	private String[] getCategoryIds(Long categoryId) {
		TreeNode treeNode = categoryManagerService.getAllTreeNodeById(categoryId);
		if(treeNode==null){
			IndexLog.info("treeNode is  empty  for categoryId" + categoryId); 
			return null;
		}
		Set<String> categorySet = new HashSet<String>();
		List<TreeNode>  nodes=treeNode.getElders();
		if(AppUtils.isNotBlank(nodes)){
			for (int i = nodes.size()-1 ; i >= 0; i--) {
				TreeNode node=nodes.get(i);
				if(node!=null){
					categorySet.add(String.valueOf(node.getSelfId()));
				}
			}
		}
		categorySet.add(String.valueOf(categoryId));
		/*
		Set<String> categorySet = new HashSet<String>();
		if (AppUtils.isNotBlank(categoryId)) {
			List<Category> categories=categoryDao.getParentCategories(categoryId);
			if(AppUtils.isNotBlank(categories)){
				for (int i = 0; i < categories.size(); i++) {
					Category category=categories.get(i);
					if(category==null)
						continue;
					categorySet.add(String.valueOf(category.getId()));
				}
			}
		}
		*/
		String[] categoryPaths = new String[categorySet.size()];
		categorySet.toArray(categoryPaths);
		return categoryPaths;
	}

}
