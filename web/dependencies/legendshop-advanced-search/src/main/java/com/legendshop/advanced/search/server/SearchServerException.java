package com.legendshop.advanced.search.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.base.exception.BusinessException;

/**
 * 
 * @author tony
 *
 */
public class SearchServerException extends BusinessException {
	
	private final static Logger log = LoggerFactory.getLogger(SearchServerException.class);
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SearchServerException(String message) {
		super("errors.search.server.all", message);
		log.error(message);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

}
