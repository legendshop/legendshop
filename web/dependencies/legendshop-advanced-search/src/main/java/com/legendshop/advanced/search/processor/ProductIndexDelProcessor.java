package com.legendshop.advanced.search.processor;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.legendshop.advanced.search.service.IndexBuildManager;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.model.constant.IndexObjectTypeCodeEnum;
import com.legendshop.model.dto.ProductIdDto;
import com.legendshop.util.AppUtils;

/**
 * 商品删除触犯索引删除
 */
@Component
public class ProductIndexDelProcessor implements EventProcessor<ProductIdDto> {

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(ProductIndexDelProcessor.class);
	
	@Resource(name="indexBuildManager")
	private IndexBuildManager indexBuildManager;

	@Override
	@Async
	public void process(ProductIdDto product) {
		if(AppUtils.isBlank(product)){
			return ;
		}
		try {
			for (Long prodId : product.getProdIdList()) {
				log.debug("Product Deleteed by  id {} ", prodId);
				indexBuildManager.delete(prodId, IndexObjectTypeCodeEnum.PRODUCT);
			}
			//logUserEvent(product, OperationTypeEnum.DELETE);
		} catch (Exception e) {
			log.error("Product delete action error",e);
		}
	}
}
