package com.legendshop.advanced.search.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.advanced.search.server.IndexServerManager;
import com.legendshop.framework.event.processor.BaseProcessor;

/**
 * 
 * 初始化系统的solr索引
 *
 */
public class IndexServiceInitProcessor extends BaseProcessor<String>{
	private static Logger log = LoggerFactory.getLogger(IndexServiceInitProcessor.class);
	
	@Override
	public void process(String task) {
		log.info(" index  service start .....");
		IndexServerManager.getInstance();
		log.info(" index  service end .....");
	}

}
