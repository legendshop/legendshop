package com.legendshop.advanced.search.model;


import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author tony
 *
 */
public class PieFieldProxy {
    //标题
    private String title;
    //字段编码
    private String field;
    
    private boolean isBrand=false;

    private List<PieCountProxy> couts = new ArrayList<PieCountProxy>();

 /*   public PieFieldProxy(PieField field) {
        this.title = field.getTitle();
        this.field = field.getField();
        for (PieField.PieCount count : field.getCouts()) {
            couts.add(new PieCountProxy(count,title));
        }
    }*/
    
    public PieFieldProxy() {
      /*  this.title = field.getTitle();
        this.field = field.getField();*/
     /*   for (PieField.PieCount count : field.getCouts()) {
            couts.add(new PieCountProxy(count,title));
        }*/
    }

    public List<PieCountProxy> getCouts() {
        return couts;
    }

    public void setCouts(List<PieCountProxy> couts) {
        this.couts = couts;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

	public boolean getIsBrand() {
		return isBrand;
	}

	public void setIsBrand(boolean isBrand) {
		this.isBrand = isBrand;
	}
    
    
}
