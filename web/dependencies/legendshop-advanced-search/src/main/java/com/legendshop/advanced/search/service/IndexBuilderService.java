package com.legendshop.advanced.search.service;

import java.util.List;

import com.legendshop.advanced.search.entity.IndexEntity;
import com.legendshop.model.constant.IndexObjectTypeCodeEnum;
/**
 * 索引服务
 * @author tony
 *
 */
public interface IndexBuilderService {


	/**
	 * 根据Ids 批量添加索引
	 * @param ids
	 * @param provider
	 * @param core
	 */
	public abstract void commit(List<Long> ids, IndexBuildProvider provider, IndexObjectTypeCodeEnum core);

	/**
	 * 根据Id 和 实体对象添加索引
	 * @param id
	 * @param entity
	 * @param core
	 */
	public abstract void commit(Long id, IndexEntity entity , IndexObjectTypeCodeEnum core);
	

	/**
	 * 批量更新保存索引
	 * @param beans
	 * @param core
	 */
	void batchSaveOrUpdateCommit(List<IndexEntity> beans, IndexObjectTypeCodeEnum core);

	/**
	 * 批量删除索引数据
	 * @param ids
	 * @param core
	 * @return
	 */
	boolean batchDeleteCommit(List<String> ids, IndexObjectTypeCodeEnum core);
	
	

	/**
	 * 清除索引数据
	 * @param core
	 * @return
	 */
	boolean rebuileBytype(IndexObjectTypeCodeEnum core);
    
    
}
