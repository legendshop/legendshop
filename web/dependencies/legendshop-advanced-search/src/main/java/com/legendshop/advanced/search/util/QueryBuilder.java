package com.legendshop.advanced.search.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.util.DateUtil;
import org.springframework.util.Assert;

import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;

/**
 * 建立一个快速生成lucene查询条件的工具类 详细的使用方法请参考QueryBuilderTest
 * 
 * @author luojt
 * 
 */
public class QueryBuilder {
	private Operator operator = Operator.OR;// 默认是OR的查询
	private List<Field> fields = new ArrayList<Field>();
	private List<QueryBuilder> subFields = new ArrayList<QueryBuilder>();
	private List<String> extQuery = new ArrayList<String>();

	private static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	private static final String MIN_DATE = "1777-01-01T01:01:01.0001Z";
	private static final String MAX_DATE = "2099-01-01T01:01:01.0001Z";
	private static final String TO = " TO ";

	private QueryBuilder() {
	}

	public static QueryBuilder start() {
		return new QueryBuilder();
	}

	public static QueryBuilder start(Operator operator) {
		Assert.notNull(operator);
		QueryBuilder queryBuilder = new QueryBuilder();
		queryBuilder.setOperator(operator);
		return queryBuilder;
	}

	public QueryBuilder addQuery(String field, String value) {
        if (AppUtils.isBlank(value) || AppUtils.isBlank(field)) {
            return this;
        }
        fields.add(new Field(field, value));
        return this;
    }

	public QueryBuilder addQuery(String field, Integer value) {
		if (AppUtils.isBlank(field) || value == null) {
			return this;
		}
		fields.add(new Field(field, String.valueOf(value)));
		return this;
	}
	
	public QueryBuilder addQuery(String field, Long value) {
		if (AppUtils.isBlank(field) || value == null) {
			return this;
		}
		fields.add(new Field(field, String.valueOf(value)));
		return this;
	}

	public QueryBuilder addSubQuery(QueryBuilder query) {
		if (query != null) {
			subFields.add(query);
		}
		return this;
	}

	/**
	 * 不等于 如 -field:value
	 * 
	 * @param field
	 * @param value
	 * @return
	 */
	public QueryBuilder notEquals(String field, String value) {
		if (AppUtils.isBlank(field) || AppUtils.isBlank(value)) {
			return this;
		}
		return addQuery(" NOT " + field, value);
	}

	public QueryBuilder gt(String field, Number value) {
		String rangeValue = "[" + value + TO + "* ]";
		return addQuery(field, rangeValue);
	}

	public QueryBuilder between(String field, Number min, Number max) {
		if (min == null || max == null) {
			return this;
		}
		String rangeValue = "[" + min + TO + max + "]";
		return addQuery(field, rangeValue);
	}

	public QueryBuilder between(String field, Date start, Date end) {
		if (start == null || end == null) {
			return this;
		}
		String startValue = DateUtil.DateToString(start, DATE_TIME_FORMAT);
		String endValue = DateUtil.DateToString(end, DATE_TIME_FORMAT);
		String rangeValue = "[" + startValue + TO + endValue + "]";
		return addQuery(field, rangeValue);
	}

	public QueryBuilder lt(String field, Date value) {
		if (value == null) {
			return this;
		}
		String maxValue = DateUtil.DateToString(value, DATE_TIME_FORMAT);

		String rangeValue = "[" + MIN_DATE + TO + maxValue + "]";
		return addQuery(field, rangeValue);
	}
	
	public QueryBuilder lt(String field, Number value) {
      String rangeValue = "[ *" + TO + value + "]";
      return addQuery(field, rangeValue);
  }

	public QueryBuilder gt(String field, Date value) {
		if (value == null) {
			return this;
		}
		String minValue = DateUtil.DateToString(value, DATE_TIME_FORMAT);

		String rangeValue = "[" + minValue + TO + MAX_DATE + "]";
		return addQuery(field, rangeValue);
	}

	public QueryBuilder appendQueryString(String queryString) {
		if (AppUtils.isNotBlank(queryString)) {
			extQuery.add(queryString);
		}
		return this;
	}

	public String getQueryString() {
		StringBuilder buff = new StringBuilder();
		for (int i = 0; i < fields.size(); i++) {
			buff.append(fields.get(i).toString());
			if (i < fields.size() - 1) {
				buff.append(operator.toCode());
			}
		}

		for (QueryBuilder queryBuilder : subFields) {
			String subQueryString = queryBuilder.getQueryString();
			if (AppUtils.isNotBlank(subQueryString)) {
				buff.append(operator.toCode()).append("(").append(subQueryString).append(")");
			}
		}

		if (fields.size() == 0) {
			String temp = buff.toString().replaceFirst(operator.toCode(), "");
			buff.setLength(0);
			buff.append(temp);
		}

		for (String query : extQuery) {
			if (AppUtils.isNotBlank(query)) {
				//TODO
				buff.append(query);
				//buff.append(operator.toCode()).append("(").append(query).append(")");
			}
		}
		return buff.toString();
	}

	public String toString() {
		return getQueryString();
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	private static class Field {
		private String field;
		private String value;

		private Field(String field, String value) {
			this.field = field;
			this.value = value;
		}

		public String toString() {
			StringBuilder builder = new StringBuilder();
			return builder.append(field).append(":").append(value).toString();
		}
	}

}
