package com.legendshop.advanced.search.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrServerException;

import com.legendshop.advanced.search.entity.ProductIndexEntity;
import com.legendshop.advanced.search.util.Operator;
import com.legendshop.advanced.search.util.QueryBuilder;
import com.legendshop.advanced.search.util.SearchParam;
import com.legendshop.advanced.search.util.SearchUrlUtils;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.model.dto.ProductPropertyDto;
import com.legendshop.util.AppUtils;

/**
 * 面包屑的工具
 * @author tony
 *
 */
public class FacetSearchBuilder implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	private Long categoryId;

	// 统计的属性结果
	private List<PieField> dictFacets = new ArrayList<PieField>();

	// 统计的品牌结果
	private PieField brandFacets;

	// solr的搜索条件，由参数传入
	private String urlQueryString;

	// 根据 urlQueryString 序列化后的数据结构，方便计算一个值是否被选中
	private SearchParam searchParam;
	
	private String keyword;
	
	private boolean noResultlist;  //是否拥有属性数据

	// 查询需要统计的属性结果
	private Map<String, ProductPropertyDto> facetDict = new HashMap<String, ProductPropertyDto>();

	public FacetSearchBuilder(Long categoryId,String urlQueryString,String keyword) throws BusinessException, SolrServerException {
		// 将查询条件反序列化成对象结构，方便接下来的使用
		this.categoryId=categoryId;
		this.urlQueryString = urlQueryString;
		searchParam = SearchUrlUtils.decodeByUrl(urlQueryString);
		this.keyword=keyword;
	}

	/**
	 * 生成条件查询语句
	 * @return
	 */
	public String getQueryString() {
		QueryBuilder builder = QueryBuilder.start(Operator.AND);
		if (AppUtils.isNotBlank(categoryId) ) {
			builder.addQuery(ProductIndexEntity.CATEGORYS,this.getCategoryId());
		} 
		builder.addQuery(ProductIndexEntity.STATUS, 1);
		
		builder.addQuery(ProductIndexEntity.KEY_WORDS, keyword);
		
		return builder.getQueryString();
	}

	/**
	 * 生成条件查询语句FilterQuery
	 * 
	 * @return
	 */
	public String getFilterKeyWordQueryString() {
		QueryBuilder queryBuilder = QueryBuilder.start(Operator.AND);
		if (AppUtils.isNotBlank(keyword)) {
			queryBuilder.addQuery(ProductIndexEntity.KEY_WORDS, keyword);
		}
		return queryBuilder.getQueryString();
	}
	
	/**
	 * 拼接keyword
	 * @return
	 */
	public String getFilterQueryString() {
		if (StringUtils.isNotBlank(urlQueryString)) {
			QueryBuilder builder = QueryBuilder.start();
			String solrQueryString = searchParam.getSolrQueryString();
			if (!StringUtils.isBlank(solrQueryString)) {
				builder.appendQueryString(solrQueryString);
			}
			return builder.getQueryString();
		}
		return "";
	}
	

	public List<PieField> getDictFacets() {
		return dictFacets;
	}

	public PieField getBrandFacets() {
		return brandFacets;
	}

	public void setBrandFacets(PieField brandFacets) {
		this.brandFacets = brandFacets;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}


	public Map<String, ProductPropertyDto> getFacetDict() {
		return facetDict;
	}

	public void setFacetDict(Map<String, ProductPropertyDto> facetDict) {
		this.facetDict = facetDict;
	}

	public SearchParam getSearchParam() {
		return searchParam;
	}

	public void setSearchParam(SearchParam searchParam) {
		this.searchParam = searchParam;
	}


	public String getUrlQueryString() {
		return urlQueryString;
	}

	public void setUrlQueryString(String urlQueryString) {
		this.urlQueryString = urlQueryString;
	}

	public void setDictFacets(List<PieField> dictFacets) {
		this.dictFacets = dictFacets;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
	public boolean isNoResultlist() {
		return noResultlist;
	}

	public void setNoResultlist(boolean noResultlist) {
		this.noResultlist = noResultlist;
	}

	
}
