/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.advanced.search.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.advanced.search.model.FacetProxy;
import com.legendshop.model.dto.search.ProductSearchParms;

/**
 * 商品搜索服务
 * @author tony
 *
 */
public interface SearchService {

	/**
	 * 异步查询
	 * @param request
	 * @param response
	 * @param parms
	 * @return
	 */
	public abstract JSONObject prodlist(Integer curPageNO, Integer pageSize,ProductSearchParms parms);

	/**
	 * 异步商品属性查询
	 * @param request
	 * @param response
	 * @param parms
	 * @return
	 */
	public abstract FacetProxy prodParamList(ProductSearchParms parms);

	
	/**
	 * 建议词搜索
	 * @param keyword
	 * @return
	 */
	public abstract List<String> suggest(String keyword);
}
