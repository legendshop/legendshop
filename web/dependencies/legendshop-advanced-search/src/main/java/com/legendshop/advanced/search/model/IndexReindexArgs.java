package com.legendshop.advanced.search.model;

import java.util.Date;

public class IndexReindexArgs {

	/** The Constant TYPE_UNKNOWN. */
	public static final int TYPE_UNKNOWN = 0;

	/** The Constant TYPE_DATE. */
	public static final int TYPE_DATE = 1;

	/** The Constant TYPE_MESSAGE. */
	public static final int TYPE_MESSAGE = 2;

	/** The from date. */
	private Date fromDate;

	/** The to date. */
	private Date toDate;

	/** The first post id. */
	private long firstPostId;

	/** The last post id. */
	private long lastPostId;

	/** The type. */
	private int type;

	/** The entity type. */
	private String entityType;

	/**
	 * Instantiates a new lucene reindex args.
	 * 
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @param firstPostId
	 *            the first post id
	 * @param lastPostId
	 *            the last post id
	 * @param avoidDuplicated
	 *            the avoid duplicated
	 * @param type
	 *            the type
	 * @param entityType
	 *            the entity type
	 */
	public IndexReindexArgs(Date fromDate, Date toDate, int firstPostId,
			int lastPostId, int type, String entityType) {
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.firstPostId = firstPostId;
		this.lastPostId = lastPostId;
		this.type = type;
		this.entityType = entityType;
	}

	/**
	 * Gets the from date.
	 * 
	 * @return the from
	 */
	public Date getFromDate() {
		return this.fromDate;
	}

	/**
	 * Gets the to date.
	 * 
	 * @return the to
	 */
	public Date getToDate() {
		return this.toDate;
	}

	/**
	 * Gets the first post id.
	 * 
	 * @return the fetchStart
	 */
	public long getFirstPostId() {
		return this.firstPostId;
	}

	/**
	 * Gets the last post id.
	 * 
	 * @return the fetchEnd
	 */
	public long getLastPostId() {
		return this.lastPostId;
	}

	/**
	 * Filter by date.
	 * 
	 * @return true, if successful
	 */
	public boolean filterByDate() {
		return this.type == TYPE_DATE && this.getFromDate() != null
				&& this.getToDate() != null;
	}

	/**
	 * Filter by message.
	 * 
	 * @return true, if successful
	 */
	public boolean filterByMessage() {
		return this.type == TYPE_MESSAGE
				&& this.getLastPostId() > this.getFirstPostId();
	}

	/**
	 * Gets the entity type.
	 * 
	 * @return the entity type
	 */
	public String getEntityType() {
		return entityType;
	}

	/**
	 * Sets the entity type.
	 * 
	 * @param entityType
	 *            the new entity type
	 */
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
