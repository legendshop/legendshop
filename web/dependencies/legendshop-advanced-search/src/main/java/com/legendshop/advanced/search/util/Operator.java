package com.legendshop.advanced.search.util;

/**
 * 
 * @author tony
 *
 */
public enum Operator {
	OR(" OR "),
    AND(" AND ");
    private String operator;

    private Operator(String operator) {
        this.operator = operator;
    }
    public String toCode(){
        return operator;
    }
}
