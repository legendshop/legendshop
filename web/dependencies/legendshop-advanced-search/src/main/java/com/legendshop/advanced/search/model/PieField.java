package com.legendshop.advanced.search.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.legendshop.util.AppUtils;

/**
 * 面包屑对象
 */
public class PieField implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// 标题
	private String title;
	// 字段编码
	private String field;

	// 字段对应的所有面包屑值
	private List<PieCount> couts = new ArrayList<PieCount>();

	public PieField(String title, String field, List<PieCount> couts) {
        this.title = title;
        this.field = field;
        this.couts = couts;
    }

	public List<PieCount> getCouts() {
		return couts;
	}

	public void setCouts(List<PieCount> couts) {
		this.couts = couts;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public static class PieCount implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		// 面包屑名称
		private String name;
		// 面包屑值
		private String value;
		// 统计商品数量
		private long count;
		// 字段
		private String field;

		private boolean isSelected;

		public PieCount(String name, String value, long count, String field, boolean isSelected) {
			this.name = name;
			this.value = value;
			this.count = count;
			this.field = field;
			this.isSelected = isSelected;
		}

		public boolean isSelected() {
			return isSelected;
		}

		public void setSelected(boolean selected) {
			isSelected = selected;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public long getCount() {
			return count;
		}

		public void setCount(long count) {
			this.count = count;
		}

		public String getField() {
			return field;
		}

		public void setField(String field) {
			this.field = field;
		}

		@Override
		public String toString() {
			return "PieCount [name=" + name + ", value=" + value + ", count=" + count + ", field=" + field + ", isSelected=" + isSelected + "]";
		}
		
		
	}

	@Override
	public String toString() {
		StringBuilder sbBuilder=new StringBuilder();
		sbBuilder.append("PieField [title=" + title + ", field=" + field + ", couts=" + couts + ""); 
		if(AppUtils.isNotBlank(couts)){
			for(PieCount pieCount:couts){
				sbBuilder.append(pieCount.toString());
			}
		}
		sbBuilder.append("]");
		return sbBuilder.toString();
	}
	
	

}
