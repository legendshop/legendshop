/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.advanced.search.service;

import com.legendshop.advanced.search.model.FacetSearchBuilder;

/**
 * 搜索服务.
 *
 * @author tony
 */
public interface FacetSearchService {

	/**
	 * Gets the facet search builder.
	 *
	 * @param categoryId the category id
	 * @param urlQueryString the url query string
	 * @param keyword the keyword
	 * @return the facet search builder
	 */
	public abstract FacetSearchBuilder getFacetSearchBuilder(Long categoryId, String urlQueryString,String keyword);
	
	
}
