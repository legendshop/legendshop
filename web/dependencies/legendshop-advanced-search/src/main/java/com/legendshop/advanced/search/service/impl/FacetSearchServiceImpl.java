package com.legendshop.advanced.search.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.advanced.search.entity.ProductIndexEntity;
import com.legendshop.advanced.search.model.FacetSearchBuilder;
import com.legendshop.advanced.search.model.PieField;
import com.legendshop.advanced.search.model.PieField.PieCount;
import com.legendshop.advanced.search.server.IndexServerManager;
import com.legendshop.advanced.search.service.FacetSearchService;
import com.legendshop.advanced.search.util.Operator;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.IndexObjectTypeCodeEnum;
import com.legendshop.model.dto.ProductPropertyDto;
import com.legendshop.model.dto.ProductPropertyValueDto;
import com.legendshop.model.entity.Brand;
import com.legendshop.spi.service.BrandService;
import com.legendshop.spi.service.ProductPropertyService;
import com.legendshop.util.AppUtils;

/**
 * 搜索
 * @author tony
 *
 */
@Service
public class FacetSearchServiceImpl implements FacetSearchService {

	@Autowired
	private BrandService brandService;
	
	@Autowired
	private ProductPropertyService productPropertyService;
	
	

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(FacetSearchServiceImpl.class);

	@Override
	public FacetSearchBuilder getFacetSearchBuilder(Long categoryId,String urlQueryString, String keyword) {
		FacetSearchBuilder builder = null;
		try {
			builder = new FacetSearchBuilder(categoryId, urlQueryString,keyword);
			doFacetStat(builder);
		} catch (BusinessException e) {
			log.error(" getFacetSearchBuilder error ", e);
		} catch (SolrServerException e) {
			log.error(" getFacetSearchBuilder error SolrServerException ", e);
		}
		return builder;
	}

	private void doFacetStat(FacetSearchBuilder builder) throws SolrServerException {

		SolrClient server = IndexServerManager.getInstance().getSearchServer(IndexObjectTypeCodeEnum.PRODUCT,
						Constants.SOLR_OPERATION_QUERY);

		SolrQuery query = joinAnalyzerSolrQuery(builder);

		// 执行搜索统计，并将结果分别存放在全局变量 dictFacets、brandFacets、categoryFacets中
		QueryResponse  response = null;
		try {
			response = server.query(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (response == null) {
			return;
		}
		
		// 获取动态FacetFields
		List<FacetField> facetFields = response.getFacetFields();
		if (!facetCount(facetFields)) {
			convertBuilder(facetFields, builder);
		}

	}

	/** 判断是否统计到结果 **/
	private boolean facetCount(List<FacetField> facetFields) {
		/*
		 * 判断是否找到 没有再去查询一下
		 */
		int fzise = facetFields.size();
		int replycount = 0;
		for (int i = 0; i < fzise; i++) {
			List<Count> counts = facetFields.get(i).getValues();
			if (AppUtils.isBlank(counts)) {
				replycount++;
				continue;
			}
		}
		if (replycount == fzise) {
			return true;
		}
		return false;
	}

	/** 把统计结果转换成 FacetSearchBuilder **/
	private void convertBuilder(List<FacetField> facetFields,FacetSearchBuilder builder) {
		int fzise = facetFields.size();
		for (int i = 0; i < fzise; i++) {
			FacetField field = facetFields.get(i);
			List<PieField.PieCount> pieCounts = new ArrayList<PieField.PieCount>();
			List<Count> counts = field.getValues();
			if (AppUtils.isBlank(counts)) {
				continue;
			}
			for (int c = 0; c < counts.size(); c++) {
				Count count = counts.get(c);
				if (StringUtils.equalsIgnoreCase(field.getName(),ProductIndexEntity.CATEGORYS)) {
					pieCounts.add(new PieField.PieCount(count.getName(), count.getName(), count.getCount(), field.getName(),
							isNosortSelected(String.valueOf(builder.getCategoryId()),count.getName())));
				}else if (StringUtils.equalsIgnoreCase(field.getName(),ProductIndexEntity.BRAND_ID)) {
					pieCounts.add(new PieField.PieCount(count.getName(), count.getName(), count.getCount(), field.getName(), builder.getSearchParam().isSelected(field.getName(), count.getName())));
				}
				else {
					String[] propvalue=count.getName().split("_");
					if(propvalue.length==2){
						Long propId=Long.valueOf(propvalue[0]);
						Long propValueId=Long.valueOf(propvalue[1]);
						if(AppUtils.isNotBlank(propId) && AppUtils.isNotBlank(propValueId)){
							Map<String, ProductPropertyDto> map=builder.getFacetDict();
							ProductPropertyDto dto=map.get(field.getName());
							ProductPropertyValueDto valueDto=getProductPropertyValueDto(dto.getProductPropertyValueList(),propValueId);
							if(dto!=null && valueDto != null){
								pieCounts.add(new PieField.PieCount(valueDto.getName(), count.getName(), count.getCount(), field.getName(),builder.getSearchParam().isSelected(field.getName(), count.getName())));
							}
						}
					}
				}
			}

			// 设置品牌的名称
			if (StringUtils.equalsIgnoreCase(field.getName(),ProductIndexEntity.BRAND_ID)) {
				setBrandFacets(pieCounts, builder);
			}
			//设置其他属性信息
			else {
				if (builder.getFacetDict().get(field.getName()) != null) {
					Map<String, ProductPropertyDto> map=builder.getFacetDict();
					ProductPropertyDto dto=map.get(field.getName());
					if(dto!=null){
						String propName=dto.getPropName();
						builder.getDictFacets().add(new PieField(propName, field.getName(), pieCounts));
					}
					
				}
			}

		}
	}

	private ProductPropertyValueDto getProductPropertyValueDto(List<ProductPropertyValueDto> productPropertyValueList,Long valueId){
		if(AppUtils.isNotBlank(productPropertyValueList)){
			for (int i = 0; i < productPropertyValueList.size(); i++) {
				ProductPropertyValueDto propertyValueDto=productPropertyValueList.get(i);
				if(valueId.equals(propertyValueDto.getValueId())){
					return propertyValueDto;
				}
			}
		}
		return null;
	}
	
	private void setBrandFacets(List<PieCount> pieCounts,
			FacetSearchBuilder builder) {
		List<PieField.PieCount> newCounts = new ArrayList<PieField.PieCount>();
		for (int i = 0; i < pieCounts.size();i++) {
			PieField.PieCount count = pieCounts.get(i);
			Brand brand = brandService.getBrand(Long.valueOf((count.getValue())));
			if (AppUtils.isNotBlank(brand) && brand.getStatus().intValue() == 1) {
				count.setName(brand.getBrandName());
				newCounts.add(count);
			}
		}
		PieField field = new PieField("品牌", ProductIndexEntity.BRAND_ID, newCounts);
		builder.setBrandFacets(field);		
	}

	public boolean isNosortSelected(String field, String value) {
		if (StringUtils.isBlank(field) || StringUtils.isBlank(value)) {
			return false;
		}
		if (field.equals(value)) {
			return true;
		}
		return false;
	}

	private SolrQuery joinSolrQuery(FacetSearchBuilder builder) {
		StringBuilder queryStr = new StringBuilder(builder.getQueryString());
		String keySql = builder.getFilterKeyWordQueryString();
		if (AppUtils.isNotBlank(keySql)) {
			queryStr.append(" ").append(Operator.AND).append(" ")
					.append(keySql);
		}
		SolrQuery query = new SolrQuery(queryStr.toString());

		String filterSql = builder.getFilterQueryString();

		query.addFilterQuery(filterSql);

		query.setFacet(true)
				// 设置使用facet
				.addFacetField(ProductIndexEntity.CATEGORYS)
				.addFacetField(ProductIndexEntity.BRAND_ID) // facet的字段
				.setFacetMinCount(1) // // 设置facet最少的统计数量
				.setFacetLimit(500); // facet结果的返回行数
		
		//动态属性Fact统计
		this.appendDictFacetField(query, builder);
		
		return query;	
	}
	
	/**
	 * 用于关键字分词后再去查询
	 * @param builder
	 * @return
	 * @since 2015-6-3 上午10:53:50
	 * @version v1.0
	 */
	private  SolrQuery joinAnalyzerSolrQuery(FacetSearchBuilder builder){
		
		StringBuilder queryStr=new StringBuilder(builder.getQueryString());
		
		SolrQuery query = new SolrQuery(queryStr.toString());

		String filterSql=builder.getFilterQueryString();
		
		query.addFilterQuery(filterSql);
		
		query.setFacet(true)
				// 设置使用facet
				.addFacetField(ProductIndexEntity.CATEGORYS)
				.addFacetField(ProductIndexEntity.BRAND_ID) // facet的字段
		  .setFacetMinCount(1)  //// 设置facet最少的统计数量
		  .setFacetLimit(500); // facet结果的返回行数
		//动态属性Fact统计
		this.appendDictFacetField(query, builder);
    
		return query;
	}
	

	private void appendDictFacetField(SolrQuery query,FacetSearchBuilder builder) {
		if(AppUtils.isNotBlank(builder.getCategoryId()) && !Constants.PRODUCT_CATEGORY_ROOT.equals(builder.getCategoryId())){
			if(productPropertyService!=null){
				List<ProductPropertyDto> productPropertyDtos = productPropertyService.findProductProper(builder.getCategoryId());
				if(AppUtils.isNotBlank(productPropertyDtos)){
					for (int i = 0; i < productPropertyDtos.size(); i++) {
						ProductPropertyDto propertyDto = productPropertyDtos.get(i);
						if (propertyDto != null && propertyDto.isForSearch()) {
							query.addFacetField(propertyDto.getPropId() + "_t");
							builder.getFacetDict().put(propertyDto.getPropId() + "_t", propertyDto);
						}
					}
				}
			}
		}
	}

}
