package com.legendshop.advanced.search.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.advanced.search.dao.IndexDao;
import com.legendshop.dao.GenericJdbcDao;
import com.legendshop.model.constant.IndexObjectTypeCodeEnum;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.util.AppUtils;

/**
 * 
 * solr 索引文件
 *
 */
@Repository
public class IndexDaoImpl implements IndexDao {
	
	/** The log. */
	private static Logger log = LoggerFactory.getLogger(IndexDaoImpl.class);
	
	@Autowired
	private GenericJdbcDao genericJdbcDao;
	
	private final static String QUERY_PRODUCTS= "SELECT prod.prod_id AS prodId, prod.shop_id as shopId,prod.category_id AS categoryId,c.name AS categoryName, prod.shop_first_cat_id AS shopFirstCatId,prod.shop_second_cat_id AS shopSecondCatId,prod.support_dist as supportDist,"+
                                                "prod.shop_third_cat_id AS shopThirdCatId,prod.name AS name,prod.pic AS pic,prod.price AS price,prod.cash AS cash,prod.views AS views,prod.buys,prod.comments AS comments ,prod.prod_type AS prodType,"+
                                                "prod.rec_date AS recDate,prod.actual_stocks AS actualStocks,prod.stocks AS stocks,prod.brief AS brief,prod.search_weight as searchWeight, " +
												"sd.shop_type AS shopType "+
                                                ",prod.content AS content, prod.status AS STATUS,prod.parameter AS parameter,prod.brand_id AS brandId ,brand.brand_name AS brand_name FROM ls_prod prod "+
                                                "LEFT JOIN ls_shop_detail sd ON prod.shop_id = sd.shop_id " +
                                                "LEFT JOIN ls_brand brand ON prod.brand_id = brand.brand_id "+
                                                "LEFT JOIN ls_category  c ON c.id=prod.category_id WHERE 1=1  AND prod.shop_id !=0 ";
	
	private final String PRODUCT_TO_DEATAIL= QUERY_PRODUCTS+ " AND prod.prod_id=?";
	
	private final String PRODUCT_TO_INDEX= QUERY_PRODUCTS+" AND prod.prod_id >= ? AND prod.prod_id < ?";
	
	private final String PRODUCT_TO_INDEX2=QUERY_PRODUCTS+ " AND prod.prod_id >= ? and prod.prod_id <= ? ";
	

	@Override
	public long getFirstPostIdByDate(String entityType, Date fromDate) {
		if (fromDate != null) {
			if (IndexObjectTypeCodeEnum.PRODUCT.value().equals(entityType)) {
				List<Long> result = genericJdbcDao.queryLimit("select prod_id from ls_prod where modify_date >= ?", Long.class, 0, 1, fromDate);
				if (AppUtils.isNotBlank(result)) {
					return result.iterator().next();
				}
			} else if (IndexObjectTypeCodeEnum.ARTICLE.equals(entityType)) {
				// TODO 文章 重建
			} else if (IndexObjectTypeCodeEnum.USER.equals(entityType)) {
				// TODO 用户 重建
			}
		}
		return 0;
	}

	@Override
	public long getLastPostIdByDate(String entityType, Date toDate) {
		if (toDate != null) {
			log.debug("lastPostIdByDate, entityType = {},toDate = {} ", entityType, toDate);
			if (IndexObjectTypeCodeEnum.PRODUCT.value().equals(entityType)) {
				List<Long> result = genericJdbcDao.queryLimit("select prod_id from ls_prod where modify_date < ? order by prod_id desc", Long.class, 0, 1, toDate);
				if (AppUtils.isNotBlank(result)) {
					return result.iterator().next();
				}
			} else if (IndexObjectTypeCodeEnum.ARTICLE.equals(entityType)) {
				// TODO 文章 重建
			} else if (IndexObjectTypeCodeEnum.USER.equals(entityType)) {
				// TODO 用户 重建
			}
		}
		return 0;
	}

	@Override
	public List<ProductDetail> getProductToIndex(long firstPostId,long toPostId,Integer status) {
		StringBuilder sql=new StringBuilder();
		sql.append(PRODUCT_TO_INDEX);
		List<Object> objects=new ArrayList<Object>();
		objects.add(firstPostId);
		objects.add(toPostId);
		if(AppUtils.isNotBlank(status)){
			sql.append("  AND prod.status = ?  ");
			objects.add(status);
		}
		return genericJdbcDao.query(sql.toString(), objects.toArray(), new ProductRowMapper());
	}

	private final static String FISTSQL="SELECT  ls_prod.prod_id FROM ls_prod   ORDER BY ls_prod.prod_id ASC";
	
	private final static String  LASTSQL="SELECT  ls_prod.prod_id FROM ls_prod   ORDER BY ls_prod.prod_id DESC";
	
	
	@Override
	public long getFirstPostId() {
		List<Long> result=genericJdbcDao.queryLimit(FISTSQL,Long.class,0,1);
		if(AppUtils.isNotBlank(result)){
			return result.iterator().next();
		}
		return 0;
	}

	@Override
	public long getLastPostId() {
		List<Long> result=genericJdbcDao.queryLimit(LASTSQL,Long.class,0,1);
		if(AppUtils.isNotBlank(result)){
			return result.iterator().next();
		}
		return 0;
	}
	
	

	@Override
	public List<ProductDetail> getProductToIndex2(long firstPostId,long toPostId,Integer status) {
		StringBuilder sql=new StringBuilder();
		sql.append(PRODUCT_TO_INDEX2);
		List<Object> objects=new ArrayList<Object>();
		objects.add(firstPostId);
		objects.add(toPostId);
		if(AppUtils.isNotBlank(status)){
			sql.append("  AND prod.status = ?  ");
			objects.add(status);
		}
		return genericJdbcDao.query(sql.toString(), objects.toArray(), new ProductRowMapper());
	}

	@Override
	public ProductDetail getIndexProdDetail(Long prodId) {
		List<ProductDetail> list = null;
		list = genericJdbcDao.query(PRODUCT_TO_DEATAIL, new Object[] { prodId }, new ProductRowMapper());
		if (AppUtils.isBlank(list)) {
			return null;
		}
		return list.get(0);
	}

	class ProductRowMapper implements RowMapper<ProductDetail> {

		@Override
		public ProductDetail mapRow(ResultSet rs, int arg1) throws SQLException {
			ProductDetail product = new ProductDetail();
			product.setProdId(rs.getLong("prodId"));
			product.setShopId(rs.getLong("shopId"));
			product.setName(rs.getString("name"));
			product.setCash(rs.getDouble("cash"));
			product.setPrice(rs.getDouble("price"));
			product.setBrief(rs.getString("brief"));
			product.setContent(rs.getString("content"));
			product.setViews(rs.getInt("views"));
			product.setBuys(rs.getInt("buys"));
			product.setComments(rs.getInt("comments"));
			product.setRecDate(rs.getDate("recDate"));
			product.setPic(rs.getString("pic"));
			product.setStatus(rs.getInt("status"));
			product.setStocks(rs.getInt("stocks"));
			product.setActualStocks(rs.getInt("actualStocks"));
			product.setParameter(rs.getString("parameter"));
			product.setBrandId(rs.getLong("brandId"));
			product.setBrandName(rs.getString("brand_name"));
			product.setCategoryId(rs.getLong("categoryId"));
			product.setCategoryName(rs.getString("categoryName"));
			product.setShopFirstCatId(rs.getLong("shopFirstCatId"));
			product.setShopSecondCatId(rs.getLong("shopSecondCatId"));
			product.setShopThirdCatId(rs.getLong("shopThirdCatId"));
			product.setSupportDist(rs.getInt("supportDist"));
			product.setProdType(rs.getString("prodType"));
			product.setShopType(rs.getInt("shopType"));
			product.setSearchWeight(rs.getDouble("searchWeight"));
			return product;
		}

	}
	
	
	

}
