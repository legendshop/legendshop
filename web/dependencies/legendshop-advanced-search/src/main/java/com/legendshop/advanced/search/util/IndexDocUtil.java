package com.legendshop.advanced.search.util;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.beans.DocumentObjectBinder;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.advanced.search.entity.DynField;

/**
 * 索引转换工具类
 * @author luojt
 *
 */
public class IndexDocUtil {
	
	private final static Logger logger = LoggerFactory.getLogger(IndexDocUtil.class);

	private IndexDocUtil() {
	}

	public static void  addBeans(SolrClient server, List beans) throws IOException, SolrServerException {
		DocumentObjectBinder binder = server.getBinder();
		List<SolrInputDocument> docs = new ArrayList<SolrInputDocument>();
		for (Object bean : beans) {
			docs.add(toSolrInputDocument(binder, bean));
		}
		server.add(docs);
	}

	public static void addBean(SolrClient server, Object bean) throws IOException, SolrServerException {
		DocumentObjectBinder binder = server.getBinder();
		SolrInputDocument document = toSolrInputDocument(binder, bean);
		server.add(document);
	}

	public static SolrInputDocument toSolrInputDocument(DocumentObjectBinder binder, Object obj) {
		SolrInputDocument document = binder.toSolrInputDocument(obj);
		try {
			Map<String, String[]> dynFieldValues = getDynFieldValue(obj);
			if (dynFieldValues != null) {
				for (Map.Entry<String, String[]> entry : dynFieldValues.entrySet()) {
					document.setField(entry.getKey(), entry.getValue());
				}
			}
		} catch (IllegalAccessException e) {
			logger.error(e.getMessage(), e);
		}
		return document;
	}

	private static Map<Class, List<Field>> cache = new HashMap();

	private synchronized static List<Field> collectDynFields(Class type) { // NOSONAR
		List<Field> result = cache.get(type);
		if (result == null) {
			result = new ArrayList<Field>();
			Class superClazz = type;
			ArrayList<Field> members = new ArrayList<Field>();
			while (superClazz != null && superClazz != Object.class) {
				members.addAll(Arrays.asList(superClazz.getDeclaredFields()));
				superClazz = superClazz.getSuperclass();
			}
			for (Field member : members) {
				if (member.isAnnotationPresent(DynField.class)) {
					member.setAccessible(true);
					result.add(member);
				}
			}
			cache.put(type, result);
		}
		return result;
	}

	private static Map<String, String[]> getDynFieldValue(Object obj) throws IllegalAccessException {
		Map<String, String[]> dynFieldValues = new HashMap<String, String[]>();
		Class type = obj.getClass();
		List<Field> members = collectDynFields(type);
		for (Field member : members) {
			Object dynObject = member.get(obj);
			if (dynObject instanceof Map) {
				Map<String, String[]> valueMp = (Map<String, String[]>) dynObject;
				for (Map.Entry<String, String[]> entry : valueMp.entrySet()) {
					dynFieldValues.put(entry.getKey(), entry.getValue());
				}
			}
		}
		return dynFieldValues;
	}

}
