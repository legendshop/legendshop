package com.legendshop.advanced.search.entity;

import java.util.HashMap;
import java.util.Map;

import org.apache.solr.client.solrj.beans.Field;


/**
 * 商品索引数据文件
 * @author tony
 *
 */
public class ProductIndexEntity implements IndexEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String KEY_WORDS = "keywords";
	public static final String PRODID = "prodId";
	public static final String SHOPID = "shopId";
	public static final String NAME = "name"; // 商品名称 //分词索引
	public static final String PRODNAME = "prodName"; // 商品名称 //分词索引
	public static final String BRIEF = "brief"; // 简要描述,卖点等
	public static final String BRAND_ID = "brandId"; // 品牌
	public static final String BRAND_NM = "brandName"; // 品牌名称
	public static final String CASH = "cash"; // 商品价格
	public static final String PRICE = "price"; // 商品价格
	public static final String CONTENT = "content"; // 商品内容
	public static final String PIC = "pic"; // 商品图片
	public static final String CATEGORYS = "categorys";
	public static final String SHOPCATEGORYS = "shopCategorys";
	public static final String CATEGORYNAME = "categoryName";
	public static final String STATUS = "status"; // 状态
	public static final String ACTUALSTOCKS = "actualStocks"; // 实际库存
	public static final String STOCKS = "stocks"; // 库存
	public static final String VIEWS = "views";// 浏览数量
	public static final String BUYS = "buys";// 购买数量
	public static final String COMMENTS = "comments";// 评论数
	public static final String SUPPORTDIST="supportDist"; //是否可以分销
	public static final String REC_DATE = "recDate";// 修改时间
	public static final String PRODUCT_TAG = "tags"; // 商品Tags
	public static final String PRODUCT_TYPE = "prodType"; // 商品类型
	public static final String SHOP_TYPE = "shopType";// 店铺类型
	public static final String SEARCH_WEIGHT = "searchWeight";// 搜索权重
	
	@Field(PRODID)
	public Long prodId;
	
	@Field(SHOPID)
	public Long shopId;

	@Field(NAME)
	public String name;
	
	@Field(PRODNAME)
	public String prodName;

	@Field(BRIEF)
	public String brief;

	@Field(BRAND_ID)
	public java.lang.Long brandId;

	@Field(BRAND_NM)
	public String brandName;

	@Field(CASH)
	public Double cash;
	
	@Field(PRICE)
	public Double price;

	@Field(CONTENT)
	public String content;

	@Field(PIC)
	public String pic;

	@Field(CATEGORYS)
	public String[] categorys;

	@Field(CATEGORYNAME)
	public String categoryName;

	@Field(SHOPCATEGORYS)
	public String[] shopCategorys;

	@Field(STATUS)
	public Integer status;

	@Field(ACTUALSTOCKS)
	public Integer actualStocks;
	
	@Field(STOCKS)
	public Integer stocks;

	@Field(VIEWS)
	public Long views;

	@Field(BUYS)
	public Long buys;

	@Field(COMMENTS)
	public Long comments;
	
	@Field(SUPPORTDIST)
	public Integer supportDist;

	@Field(REC_DATE)
	private java.util.Date recDate;

	@Field(PRODUCT_TAG)
	private java.lang.String[] tags;
	
	@Field(PRODUCT_TYPE)
	public String prodType;
	
	@Field(SHOP_TYPE)
	public Integer shopType;

	@Field(SEARCH_WEIGHT)
	public Double searchWeight;

	@DynField
	private Map<String, String[]> paramValues = new HashMap<String, String[]>();

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}
	
	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public java.lang.Long getBrandId() {
		return brandId;
	}

	public void setBrandId(java.lang.Long brandId) {
		this.brandId = brandId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}
	
	

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String[] getCategorys() {
		return categorys;
	}

	public void setCategorys(String[] categorys) {
		this.categorys = categorys;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String[] getShopCategorys() {
		return shopCategorys;
	}

	public void setShopCategorys(String[] shopCategorys) {
		this.shopCategorys = shopCategorys;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	

	public Long getViews() {
		return views;
	}

	public void setViews(Long views) {
		this.views = views;
	}

	public Long getBuys() {
		return buys;
	}

	public void setBuys(Long buys) {
		this.buys = buys;
	}

	public Long getComments() {
		return comments;
	}

	public void setComments(Long comments) {
		this.comments = comments;
	}

	public java.util.Date getRecDate() {
		return recDate;
	}

	public void setRecDate(java.util.Date recDate) {
		this.recDate = recDate;
	}

	public java.lang.String[] getTags() {
		return tags;
	}

	public void setTags(java.lang.String[] tags) {
		this.tags = tags;
	}

	public Map<String, String[]> getParamValues() {
		return paramValues;
	}

	public void setParamValues(Map<String, String[]> paramValues) {
		this.paramValues = paramValues;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getActualStocks() {
		return actualStocks;
	}

	public void setActualStocks(Integer actualStocks) {
		this.actualStocks = actualStocks;
	}
	
	public Integer getStocks() {
		return stocks;
	}

	public void setStocks(Integer stocks) {
		this.stocks = stocks;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public void setSupportDist(Integer supportDist) {
		this.supportDist = supportDist;
	}

	public Integer getSupportDist() {
		return supportDist;
	}
	
	public Integer getShopType() {
		return shopType;
	}

	public void setShopType(Integer shopType) {
		this.shopType = shopType;
	}

	public Double getSearchWeight() {
		return searchWeight;
	}

	public void setSearchWeight(Double searchWeight) {
		this.searchWeight = searchWeight;
	}
}
