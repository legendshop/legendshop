package com.legendshop.advanced.search.server;

import java.io.File;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.solr.client.solrj.SolrClient;

import com.legendshop.advanced.search.util.IndexLog;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.config.PropertiesUtilManager;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.IndexObjectTypeCodeEnum;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SystemUtil;

/**
 * 搜索服务器
 * @author tony
 *
 */
public final class IndexServerManager {

	private static IndexServerManager solrServerManager;

	private static  Object lockedObject = new Object();
	
	private  Map<String, ISearchServerFactory> serverFactoryMap = new ConcurrentHashMap<String, ISearchServerFactory>();
	
	private static String mastersolrUrl; // 主从服务器构建索引数据

	private static String slaveSolrUrl; // 从服务器辅助搜索
	
	private static final String LOCAL_PATH = "/plugins/advanced_search/solr";
	
	
	
	
	private IndexServerManager() {
	  
	  PropertiesUtil propertiesUtil= PropertiesUtilManager.getPropertiesUtil();
		mastersolrUrl = propertiesUtil.getMasterSolrUrl();
		slaveSolrUrl= propertiesUtil.getSlaveSolrUrl();
		if (AppUtils.isBlank(serverFactoryMap)) {
			if(AppUtils.isNotBlank(mastersolrUrl)&&AppUtils.isNotBlank(slaveSolrUrl)){
				try {
					ISearchServerFactory  serverFactory = new RemotSearchServer(mastersolrUrl);
					serverFactoryMap.put(Constants.SOLR_OPERATION_CREATE, serverFactory);
				} catch (Exception e) {
					IndexLog.error("*****master solrServce error "+e.getMessage());
				}
				try {
					ISearchServerFactory serverFactory = new RemotSearchServer(slaveSolrUrl);
					serverFactoryMap.put(Constants.SOLR_OPERATION_QUERY, serverFactory);
				} catch (Exception e) {
					IndexLog.error("*****Slave solrServce error "+e.getMessage());
				}
				
			}else if(AppUtils.isNotBlank(mastersolrUrl)&&AppUtils.isBlank(slaveSolrUrl)){
				try {
					ISearchServerFactory serverFactory = new RemotSearchServer(mastersolrUrl);
					serverFactoryMap.put(Constants.SOLR_OPERATION_CREATE, serverFactory);
					serverFactoryMap.put(Constants.SOLR_OPERATION_QUERY, serverFactory);
				} catch (Exception e) {
					IndexLog.error("*****Index remote standalone pattern error "+e.getMessage());
				}
				
			}else{
				String webInfPath = SystemUtil.getSystemRealPath();
				if (webInfPath != null) {
					webInfPath +=  "WEB-INF";
				} else {
					URL url = IndexServerManager.class.getClassLoader().getResource("");
					webInfPath = new File(url.getFile()).getParentFile().getPath();
				}
				try {
					String path=webInfPath + File.separatorChar + LOCAL_PATH;
					ISearchServerFactory serverFactory = new EmbeddedServer(path);
					serverFactoryMap.put(Constants.SOLR_OPERATION_CREATE, serverFactory);
					serverFactoryMap.put(Constants.SOLR_OPERATION_QUERY, serverFactory);
				} catch (Exception e) {
					e.printStackTrace();
					IndexLog.error("*****Index native solrServce error "+e.getMessage());
				}
				
			}
		}
	}
	
	
	
	public static IndexServerManager getInstance() {
		if (solrServerManager == null) {
			synchronized (lockedObject) {
				if (solrServerManager == null) {
					solrServerManager = new IndexServerManager();
				}
			}
		}
		return solrServerManager;
	}



	public SolrClient getSearchServer(IndexObjectTypeCodeEnum core,
			String solrOperationCreate) {
		if(solrOperationCreate==null){
			throw new SearchServerException("Operation can not null");
		}
		ISearchServerFactory serverFactory=serverFactoryMap.get(solrOperationCreate);
		
		if(serverFactory==null){
			throw new SearchServerException("Solr Server IS  not null");
		}
		return serverFactory.getSearchServer(core);
	}
	
	
	
}
