package com.legendshop.advanced.search.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author tony
 *
 */
public class IndexLog {


	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(IndexLog.class);
	
	public static void log(String value, Object ... params){
		log.debug(value,params);
	}

	public static void info(String value){
		log.info(value);
	}
	
	public static void error(String message){
		log.error(String.valueOf(message));
	}
	
	 public static void error(String s, Throwable throwable){
		 log.error(s, throwable);
	 }

	public static void debug(String value, Object ... params) {
		 log.debug(value, params);
	}
	 
	
	
}
