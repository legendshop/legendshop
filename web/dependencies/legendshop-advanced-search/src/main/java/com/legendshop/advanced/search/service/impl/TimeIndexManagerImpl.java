package com.legendshop.advanced.search.service.impl;

import java.util.List;

import com.legendshop.advanced.search.service.IndexBuildManager;
import com.legendshop.advanced.search.service.IndexBuildProvider;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.constant.IndexObjectTypeCodeEnum;

/**
 * 定时建立索引
 * 暂时没用,目前是采用实时计算的
 *
 */
public class TimeIndexManagerImpl  implements IndexBuildManager {

	@Override
	public boolean rebuileBytype(IndexObjectTypeCodeEnum codeEnum) {
		return false;
	}

	@Override
	public void batchSave(List<GenericEntity> entitys,
			IndexBuildProvider provider, IndexObjectTypeCodeEnum core) {
		
	}

	@Override
	public void batchDelete(List<String> deleteIds, IndexObjectTypeCodeEnum core) {
		
	}

	@Override
	public void delete(Long id, IndexObjectTypeCodeEnum product) {
		
	}

	@Override
	public void saveOrUpdate(Long id, IndexBuildProvider indexBuildProvider,
			IndexObjectTypeCodeEnum product) {
		
	}

}
