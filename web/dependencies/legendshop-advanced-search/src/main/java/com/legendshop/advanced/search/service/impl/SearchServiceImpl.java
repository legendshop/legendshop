/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.advanced.search.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.advanced.search.entity.ProductIndexEntity;
import com.legendshop.advanced.search.model.*;
import com.legendshop.advanced.search.model.PieField.PieCount;
import com.legendshop.advanced.search.server.IndexServerManager;
import com.legendshop.advanced.search.service.FacetSearchService;
import com.legendshop.advanced.search.service.SearchService;
import com.legendshop.advanced.search.util.Operator;
import com.legendshop.advanced.search.util.QueryBuilder;
import com.legendshop.advanced.search.util.SearchParam;
import com.legendshop.advanced.search.util.SearchUrlUtils;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.IndexObjectTypeCodeEnum;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.util.AppUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SpellCheckResponse;
import org.apache.solr.client.solrj.response.SpellCheckResponse.Suggestion;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.Map.Entry;

/**
 * 
 * 搜索服务
 *
 */
@Service("searchService")
public class SearchServiceImpl implements SearchService {
	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(SearchServiceImpl.class);
	
	@Autowired
	private FacetSearchService facetSearchService;

	@Override
	public JSONObject prodlist(Integer curPageNO, Integer pageSize,ProductSearchParms parms) {
		String keyword=parms.getKeyword();
		SolrQuery query = new SolrQuery();
		query.setStart((curPageNO - 1) * pageSize);
		query.setRows(pageSize);
		
		QueryBuilder queryBuilder = QueryBuilder.start(Operator.AND);
		queryBuilder.addQuery(ProductIndexEntity.PRODUCT_TAG, parms.getTag());
		queryBuilder.addQuery(ProductIndexEntity.CATEGORYS,parms.getCategoryId());
		queryBuilder.addQuery(ProductIndexEntity.SHOPCATEGORYS, parms.getShopCategoryId());
		queryBuilder.addQuery(ProductIndexEntity.STATUS, 1);
		if(AppUtils.isNotBlank(keyword)){
			queryBuilder.addQuery(ProductIndexEntity.KEY_WORDS, keyword);
		}
		if (AppUtils.isNotBlank(parms.getHasProd()) && parms.getHasProd()) {
			queryBuilder.gt(ProductIndexEntity.STOCKS, 1);
		}
		if (AppUtils.isNotBlank(parms.getSupportDist()) && parms.getSupportDist()) {
			queryBuilder.addQuery(ProductIndexEntity.SUPPORTDIST, 1);
		}
		if(AppUtils.isNotBlank(parms.getStartPrice()) && AppUtils.isNotBlank(parms.getEndPrice())){
			queryBuilder.between(ProductIndexEntity.CASH, parms.getStartPrice(), parms.getEndPrice());
		}else{
			if(AppUtils.isNotBlank(parms.getStartPrice())){
				queryBuilder.gt(ProductIndexEntity.CASH,  parms.getStartPrice());
			}
			if(AppUtils.isNotBlank(parms.getEndPrice())){
				queryBuilder.lt(ProductIndexEntity.CASH,  parms.getStartPrice());
			}
		}
		
		if(AppUtils.isNotBlank(parms.getBrandId())){
			queryBuilder.addQuery(ProductIndexEntity.BRAND_ID, parms.getBrandId());
		}
		
		query.setQuery(queryBuilder.getQueryString());
		
		if(AppUtils.isNotBlank(parms.getProp())){
			SearchParam searchParam = SearchUrlUtils.decodeByUrl(parms.getProp());
			QueryBuilder f2queryBuilder=QueryBuilder.start(Operator.AND);
			f2queryBuilder.appendQueryString(searchParam.getSolrQueryString());
			String fq2String=f2queryBuilder.getQueryString();
			query.setFilterQueries(fq2String);
		}
		
		query.addField(ProductIndexEntity.PRODID);
		query.addField(ProductIndexEntity.PRODNAME);
		query.addField(ProductIndexEntity.COMMENTS);
		query.addField(ProductIndexEntity.BRAND_NM);
		query.addField(ProductIndexEntity.CASH);
		query.addField(ProductIndexEntity.PRICE);
		query.addField(ProductIndexEntity.PIC);
		query.addField(ProductIndexEntity.PRODUCT_TYPE);
		query.addField(ProductIndexEntity.SHOP_TYPE);
		query.addFacetField(ProductIndexEntity.SEARCH_WEIGHT);
		String[] order = StringUtils.split(parms.getOrders(), ",");

		if (order != null && order.length == 2) {
			if (StringUtils.equals("asc", order[1])) {
				query.setSort(order[0], SolrQuery.ORDER.asc);
			} else if(StringUtils.equals("desc", order[1])) {//防止xss攻击
				query.setSort(order[0], SolrQuery.ORDER.desc);
			}
		}
		
		/**
		 * 搜索权重
		 * defType: 指定query parser，常用defType=lucene, defType=dismax, defType=edismax
		 */
		query.set("defType","edismax");
		query.set("qf","name^4 brandName^1 ");
		// Boost Functions。用函数的方式计算boost，sum(商品的权重值),计算最新发布商品的时间
		query.set("bf","sum(sum(searchWeight),recip(rord(recDate),1,1000,1000))");
		SolrDocumentList documentList = null;
		ArrayList<ProductDetail> result =null;
		try {
			SolrClient solrServer = IndexServerManager.getInstance().getSearchServer(IndexObjectTypeCodeEnum.PRODUCT,Constants.SOLR_OPERATION_QUERY);
			QueryResponse qrsp = solrServer.query(query);
			documentList = qrsp.getResults();
			int docsize = documentList.size();
			if(AppUtils.isNotBlank(docsize)){
				result = new ArrayList<ProductDetail>();
				for (int i = 0; i < docsize; i++) {
					ProductDetail productDetail =new ProductDetail();
					SolrDocument solrDocument = documentList.get(i);
					Long productId =Long.valueOf(String.valueOf(solrDocument.get(ProductIndexEntity.PRODID)));
					String prod_name=String.valueOf(solrDocument.get(ProductIndexEntity.PRODNAME));
					String brandName = String.valueOf(solrDocument.get(ProductIndexEntity.BRAND_NM));
					Double cash=Double.valueOf(String.valueOf(solrDocument.get(ProductIndexEntity.CASH)));
					Double price=Double.valueOf(String.valueOf(solrDocument.get(ProductIndexEntity.PRICE)));
					String pic=String.valueOf(solrDocument.get(ProductIndexEntity.PIC));
					String prodType=String.valueOf(solrDocument.get(ProductIndexEntity.PRODUCT_TYPE));
					//Integer shopType = Integer.valueOf(String.valueOf(solrDocument.get(ProductIndexEntity.SHOP_TYPE)));
					//修改不按评论数排序的Bug
					String commentStr=String.valueOf(solrDocument.get(ProductIndexEntity.COMMENTS));
					if(AppUtils.isBlank(commentStr)||"null".equals(commentStr)){
						commentStr="0";
					}
					Long comments=Long.valueOf(commentStr);
					productDetail.setComments(comments);
					productDetail.setProdId(productId);
					productDetail.setName(prod_name);
					productDetail.setBrandName(brandName);
					productDetail.setCash(cash);
					productDetail.setPic(pic);
					productDetail.setPrice(price);
					productDetail.setProdType(prodType);
					//productDetail.setShopType(shopType);
					result.add(productDetail);
				}
			}
		} catch (Exception e) {
			log.error(" ProductIndexQueryService searchProduct Error ", e);
			return null;
		}
		JSONObject jsonObject=new JSONObject();
		long numFound = 0l;
		if (documentList != null) {
			numFound = documentList.getNumFound();
		}
		jsonObject.put("numFound", numFound);
		jsonObject.put("result", result);
		return jsonObject;
	}

	/**
	 * 面包屑属性列表
	 */
	@Override
	public FacetProxy prodParamList(ProductSearchParms parms) {
		String ev = parms.getProp();
		String keyword=parms.getKeyword();
		Long categoryId=parms.getCategoryId();
		if(AppUtils.isBlank(ev) && AppUtils.isBlank(keyword) && AppUtils.isBlank(categoryId)){
			return null;
		}
		FacetSearchBuilder facetSearchBuilder =facetSearchService.getFacetSearchBuilder(categoryId, ev,keyword);
		FacetProxy facetProxy = new FacetProxy();
		if (AppUtils.isNotBlank(facetSearchBuilder) && !facetSearchBuilder.isNoResultlist()) {
			facetProxy.setSelections(getSelections(parms,facetSearchBuilder));
			facetProxy.setUnSelections(getUnSelections(parms,facetSearchBuilder));
		}
		return facetProxy;
	}

	private List<PieFieldProxy> getUnSelections(ProductSearchParms parms, FacetSearchBuilder facetSearchBuilder) {
		List<PieFieldProxy> unSelections = new ArrayList<PieFieldProxy>();
		boolean existBrand = false;
		// 统计的品牌结果
		PieField pfField = facetSearchBuilder.getBrandFacets();
		if (pfField != null) {
			List<PieCount> pieCounts = pfField.getCouts();
			if (AppUtils.isNotBlank(pieCounts)) {
				for (int i = 0; i < pieCounts.size(); i++) {
					if (pieCounts.get(i).isSelected()) {
						existBrand = true;
						break;
					}
				}
			}
		}	
		if (!existBrand) {
			if (AppUtils.isNotBlank(pfField)) {
				List<PieCount> pieCounts = pfField.getCouts();
				if (AppUtils.isNotBlank(pieCounts)) {
					PieFieldProxy pieFieldProxy = new PieFieldProxy();
					pieFieldProxy.setTitle(pfField.getTitle());
					pieFieldProxy.setField(pfField.getField());
					pieFieldProxy.setIsBrand(true);
					for (int i = 0; i < pieCounts.size(); i++) {
						PieCount count = pieCounts.get(i);
						PieCountProxy pieCountProxy = new PieCountProxy(count, pfField.getTitle());
						try {
							pieCountProxy.setUrl(getUrl(parms,count));
						} catch (UnsupportedEncodingException e) {
							log.error("pieCountProxy url encode FacetProxyServiceImpl error:", e);
						}
						pieFieldProxy.getCouts().add(pieCountProxy);
					}
					unSelections.add(pieFieldProxy);
				}
			}
		}
		

		// 统计的属性结果
		List<PieField> dictFacets = facetSearchBuilder.getDictFacets();
		if (AppUtils.isNotBlank(dictFacets)) {
			for (int i = 0; i < dictFacets.size(); i++) {
				PieField field = dictFacets.get(i);
				boolean isSelected = false;
				List<PieCount> pieCounts = field.getCouts();
				for (int j = 0; j < pieCounts.size(); j++) {
					PieCount count = pieCounts.get(j);
					if (count.isSelected()) {
						isSelected = true;
						continue;
					}
				}
				if (!isSelected) {
					if (AppUtils.isNotBlank(field)) {
						List<PieCount> counts = field.getCouts();
						if(AppUtils.isNotBlank(counts)){
							PieFieldProxy pieFieldProxy = new PieFieldProxy();
							pieFieldProxy.setTitle(field.getTitle());
							pieFieldProxy.setField(field.getField());
							for (int k = 0; k < counts.size(); k++) {
								PieCount count = counts.get(k);
								PieCountProxy pieCountProxy = new PieCountProxy(count, field.getTitle());
								try {
									pieCountProxy.setUrl(getUrl(parms,count));
								} catch (UnsupportedEncodingException e) {
									log.error("pieCountProxy url encode FacetProxyServiceImpl error:", e);
								}
								pieFieldProxy.getCouts().add(pieCountProxy);

							}
							unSelections.add(pieFieldProxy);
						}
					}
				}
			}
		}
		return unSelections;
	}


	/**
	 * Gets the selections.
	 *
	 * @param request the request
	 * @param builder the builder
	 * @return the selections
	 */
	private List<PieCountProxy> getSelections(ProductSearchParms parms,FacetSearchBuilder builder) {
		List<PieCountProxy> selections = new ArrayList<PieCountProxy>();
		// 统计的品牌结果
		PieField pfField = builder.getBrandFacets();
		if (pfField != null) {
			//字段对应的所有面包屑值
			List<PieCount> pieCounts = pfField.getCouts();
			if (AppUtils.isNotBlank(pieCounts)) {
				int psize = pieCounts.size();
				for (int i = 0; i < psize; i++) {
					PieCount count = pieCounts.get(i);
					if (count.isSelected()) {
						PieCountProxy pieCountProxy = new PieCountProxy(count, pfField.getTitle());
						try {
							pieCountProxy.setUrl(getUrl(parms,count));
						} catch (UnsupportedEncodingException e) {
							log.error("pieCountProxy url encode FacetProxyServiceImpl error:", e);
						}
						selections.add(pieCountProxy);
						break;
					}
				}
			}
		}

		// 统计的属性结果
		List<PieField> dictFacets = builder.getDictFacets();
		if (AppUtils.isNotBlank(dictFacets)) {
			for (int i = 0; i < dictFacets.size(); i++) {
				PieField field=dictFacets.get(i);
				List<PieCount> pieCounts = field.getCouts();
				for (int j = 0; j < pieCounts.size(); j++) {
					PieCount count = pieCounts.get(j);
					if (count.isSelected()) {
						PieCountProxy pieCountProxy = new PieCountProxy(count, dictFacets.get(i).getTitle());
						try {
							pieCountProxy.setUrl(getUrl(parms,count));
						} catch (UnsupportedEncodingException e) {
							log.error("pieCountProxy url encode FacetProxyServiceImpl error:", e);
						}
						selections.add(pieCountProxy);
					}
				}
			}
		}
		return selections;
	}


	/**
	 * 获取属性的路径地址
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public String getUrl(ProductSearchParms parms,PieField.PieCount count) throws UnsupportedEncodingException {
		Map<String, String[]> parameters = parms.getRequestParams();
		Set<Entry<String, String[]>> entries = parameters.entrySet();
		Iterator<Entry<String, String[]>> it = entries.iterator();
		StringBuilder reqUrlBuilder = new StringBuilder();
		
		boolean isFirst = true;
		while (it.hasNext()) {
			Entry<String, String[]> entry =it.next();
			String name = (String) entry.getKey();
			String[] value = (String[]) entry.getValue();
			String[] temp = new String[value.length];
			System.arraycopy(value, 0, temp, 0, value.length);
			for (int i = 0; i < value.length; i++) {
				//temp[i] = URLEncoder.encode(temp[i], "utf-8");
				temp[i]=new String(temp[i]);
			}
			if (!name.equalsIgnoreCase("page") && !name.equalsIgnoreCase("prop")) {
				for (int j = 0; j < temp.length; j++) {
					if (StringUtils.isNotEmpty(temp[j])) {
						if (isFirst) {
							isFirst = false;
							reqUrlBuilder.append(name + "=" + temp[j]);
							//reqUrlBuilder.append("?" + name + "=" + temp[j]);
						} else {
							reqUrlBuilder.append("&").append(name).append("=").append(temp[j]);
						}
					}
				}
			}
		}
		String q = parms.getProp();
		q = q == null ? "" : q;

		String queryString = "";
		if (count.isSelected()) {
			queryString = SearchUrlUtils.removeAllParams(q, count.getField());
		} else {
			queryString = SearchUrlUtils.replaceAllParams(q, count.getField(), count.getValue());
		}
		if (reqUrlBuilder == null || "".equals(reqUrlBuilder.toString())) {
			reqUrlBuilder.append("?prop=").append(queryString);
		} else {
			reqUrlBuilder.append("&prop=").append(queryString);
		}

		return reqUrlBuilder.toString();
	}


	public void setFacetSearchService(FacetSearchService facetSearchService) {
		this.facetSearchService = facetSearchService;
	}


	@Override
	public List<String> suggest(String keyword) {
		SolrQuery query = new SolrQuery();
		ModifiableSolrParams params = new ModifiableSolrParams();
		params.set("qt", "/suggest");// spellhandler
		params.set("q", keyword);
		params.set("spellcheck", "on");
		params.set("spellcheck.build", "true");
		params.set("spellcheck.count", "10");
		query.add(params);
		List<String> suggestWordList = new ArrayList<String>();
		try {
			SolrClient solrServer = IndexServerManager.getInstance().getSearchServer(IndexObjectTypeCodeEnum.PRODUCT,Constants.SOLR_OPERATION_QUERY);
			QueryResponse qrsp = solrServer.query(query);
			SpellCheckResponse spellCheckResponse = qrsp.getSpellCheckResponse();
			if (spellCheckResponse != null) {
				List<Suggestion> suggestionList = spellCheckResponse.getSuggestions();
				for (Suggestion suggestion : suggestionList) {
					if (suggestion.getNumFound() != 0) {
						suggestWordList.addAll(suggestion.getAlternatives());
					}
				}
			}
		} catch (Exception e) {
			log.error("SearchServiceImpl searchProduct Error",e);
		}
		return suggestWordList;
	}
	

}
