package com.legendshop.advanced.search.service;

import com.legendshop.advanced.search.dao.IndexDao;
import com.legendshop.advanced.search.model.IndexReindexArgs;
import com.legendshop.advanced.search.util.IndexLog;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.dao.support.GenericEntity;
import com.legendshop.model.constant.IndexObjectTypeCodeEnum;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * 重建Lucene索引
 *
 */
@Component
public class IndexBuilderHandler {
	
	@Autowired
	private IndexDao indexDao;
	
	@Autowired
	private IndexBuildManager indexBuildManager;
	
	@Autowired
	private IndexBuildProvider indexBuildProvider;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	public void startIndexBuildProcess(final IndexReindexArgs args, final boolean recreate) {
		Runnable indexingJob = new Runnable() {
			public void run() {
				rebuildIndex(args, recreate);
			}
		};
		Thread thread = new Thread(indexingJob);
		thread.start();
	}
	
	/**
	 * 重建索引
	 */
	private void rebuildIndex(final IndexReindexArgs args, final boolean recreate) {
		long fetchCount = 50; // 20
		String entityType =args.getEntityType();
		try {
			boolean hasMorePosts = true;
			// break if had started before 不要再随便终止这个行为
//			if ("0".equals(systemParameterUtil.getLuceneCurrentlyIndexing())) {
//				    System.out.println("*********已经有一个线程在运行中,请稍候操作 solr rebuildIndex ************");
//					hasMorePosts = false;
//					return;
//			 }
			long processStart = System.currentTimeMillis();
			long firstPostId = 0 ;
			long lastPostId = 0;
            if(args.getType()==3){ //3 为全部重建索引文件
            	firstPostId=indexDao.getFirstPostId();
            	lastPostId=indexDao.getLastPostId();
            }else{
            	//filterByMessage 是否按依实体Id重建  否则按照依日期重建
            	firstPostId=args.filterByMessage() ? args.getFirstPostId() : indexDao.getFirstPostIdByDate(entityType, args.getFromDate()); // 110
            	lastPostId=args.filterByMessage() ? args.getLastPostId() : indexDao.getLastPostIdByDate(entityType, args.getToDate()); // 200
            }
			int counter = 1; // 批量更新

			int indexTotal = 0; // 索引数据条数
			
			List<ProductDetail> errorProduct=new ArrayList<ProductDetail>();  //记录商品为Null 或者 下线的商品
			
			long indexRangeStart = System.currentTimeMillis();


			if (recreate||args.getType()==3) {
				//1. 优先删除所有的索引
				indexBuildManager.rebuileBytype(IndexObjectTypeCodeEnum.fromCode(entityType));
			}

			if (firstPostId != 0 && lastPostId != 0) {
				
				while (hasMorePosts) {
					long toPostId = firstPostId + fetchCount < lastPostId ? firstPostId + fetchCount : lastPostId;
					List<ProductDetail> beans = new ArrayList<ProductDetail>();
					if (IndexObjectTypeCodeEnum.PRODUCT.value().equals(entityType)) {
						
						if (counter >= 5000) {
							long end = System.currentTimeMillis();
							IndexLog.info("Indexed ~5000 documents in " + (end - indexRangeStart) + " ms (" + indexTotal + " so far)");
							indexRangeStart = end;
							counter = 0;
						}
						
						List<GenericEntity> genericEntities = new ArrayList<GenericEntity>();
						
						List<String> deleteIds = new ArrayList<String>();
						//获取数据用户信息
						beans = getProducts(args, recreate, firstPostId,lastPostId, toPostId, beans);
						
						if (AppUtils.isNotBlank(beans)) {
							for (Iterator<ProductDetail> iter = beans.iterator(); iter.hasNext();) {
								counter++;
								indexTotal++;
								ProductDetail obj = (ProductDetail) iter.next();
								if(obj==null || ProductStatusEnum.PROD_OFFLINE.value().equals(obj.getStatus())){
									errorProduct.add(obj);
									deleteIds.add(String.valueOf(obj.getProdId()));
									continue;
								}
								genericEntities.add(obj);
							}
						}
						if (AppUtils.isNotBlank(genericEntities)) {
							indexBuildManager.batchSave(genericEntities, indexBuildProvider,IndexObjectTypeCodeEnum.PRODUCT);
						    genericEntities=null;
						    beans=null;
						}
						if(AppUtils.isNotBlank(deleteIds)){
							indexBuildManager.batchDelete(deleteIds,IndexObjectTypeCodeEnum.PRODUCT);
							deleteIds=null;
						}
					}
					else if (IndexObjectTypeCodeEnum.USER.value().equals(entityType)) {
						System.out.println("获取用户数据");
					} else if (IndexObjectTypeCodeEnum.ARTICLE.value().equals(entityType)) {
						System.out.println("获取文章数据");
					}
					firstPostId += fetchCount;
					hasMorePosts = hasMorePosts && toPostId != lastPostId;
				}
				long end = System.currentTimeMillis();
				IndexLog.log("**** operation {} index builder ****",IndexObjectTypeCodeEnum.PRODUCT);
				IndexLog.log("**** Total: {}  ms, Total  index = {} ****",(end - processStart),indexTotal);
				IndexLog.log("**** deleteProduct total: {} *****************" ,errorProduct.size());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<ProductDetail> getProducts(final IndexReindexArgs args,
			final boolean recreate, long firstPostId, long lastPostId,
			long toPostId, List<ProductDetail> beans) {
		if(toPostId==lastPostId){
			if(recreate||args.getType()==3){
				beans = indexDao.getProductToIndex2(firstPostId, toPostId,1);
			}else{
				beans = indexDao.getProductToIndex2(firstPostId, toPostId,null);
			}
		}else{
		    if(recreate||args.getType()==3){
		    	beans = indexDao.getProductToIndex(firstPostId, toPostId,1);
			}else{
				beans = indexDao.getProductToIndex(firstPostId, toPostId,null);
			}
		}
		return beans;
	}

	
}
