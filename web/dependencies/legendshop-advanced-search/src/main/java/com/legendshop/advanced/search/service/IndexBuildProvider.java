package com.legendshop.advanced.search.service;

import com.legendshop.advanced.search.entity.IndexEntity;
import com.legendshop.dao.support.GenericEntity;

/**
 * 
 * 建立索引
 *
 */
public interface IndexBuildProvider {

	public abstract IndexEntity convertEntity(GenericEntity genericentity);

    public abstract IndexEntity getEntity(Long long1);
    
}
