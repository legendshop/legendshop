/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.permission.service.impl;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.UserDto;
import com.legendshop.model.entity.Function;
import com.legendshop.model.entity.Permission;
import com.legendshop.permission.dao.FunctionDao;
import com.legendshop.permission.service.FunctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 权限服务.
 */
@Service("functionService")
public class FunctionServiceImpl implements FunctionService {

    @Autowired
    private FunctionDao functionDao;

    /**
     * 保存权限菜单
     */
    @Override
    public boolean saveFuncMenu(String funcId, String menuId) {
        return functionDao.saveFuncMenu(funcId, menuId);
    }

    /**
     * 根据权限Id获取菜单列表
     */
    @Override
    public List<Long> getMenuIdSByFuncId(String funcId) {
        return functionDao.getMenuIdSByFuncId(funcId);
    }

    /**
     * 根据菜单ID拿到权限列表
     */
    @Override
    @Cacheable(value = "GrantedFunctionList", key = "'menu_'+#menuId")
    public List<Function> getFunctionsByMenuId(Long menuId) {
        return functionDao.getFunctionsByMenuId(menuId);
    }


    /**
     * 获取所有的权限
     */
    @Override
    public List<Function> getAllFunction() {
        return functionDao.getAllFunction();
    }

    /**
     * 根据URL地质获取权限
     *
     * @param url
     * @return
     */
    public List<Function> getFunctionsByUrl(String url) {
        return functionDao.getFunctionsByUrl(url);
    }

    /**
     * 根据用户ID查询用户拥有的系统权限信息
     *
     * @param userId
     * @return 权限
     */
    @Cacheable(value = "GrantedFunctionList", key = "'sysuser_'+#userId")
    public List<Function> getSysFunctionByUserId(String userId) {
        return functionDao.getSysFunctionByUserId(userId);
    }

    /**
     * 根据用户ID查询用户拥有的权限信息
     *
     * @param userId
     * @return
     */
    @Cacheable(value = "GrantedFunctionList", key = "'user_'+#userId")
    public List<Function> getFunctionByUserId(String userId) {
        return functionDao.getFunctionByUserId(userId);
    }


    @Override
    public PageSupport<UserDto> loadUserByFunction(String curPageNO, String userName, String funcId) {
        return functionDao.loadUserByFunction(curPageNO, userName, funcId);
    }

    @Override
    public Function getFunctionsById(String funcId) {
        return functionDao.getFunctionById(funcId);
    }

    @Override
    public List<Permission> getPermissionByRoleId(String roleId) {
        return functionDao.getPermissionByRoleId(roleId);
    }

    @Override
    public PageSupport<UserDto> loadUserByRole(String curPageNO, String userName, String roleId) {
        return functionDao.loadUserByRole(curPageNO, userName, roleId);
    }

    @Override
    public PageSupport<UserDto> loadAdminByRole(String curPageNO, String userName, String roleId) {
        return functionDao.loadAdminByRole(curPageNO, userName, roleId);
    }

    @Override
    public List<Function> getAllFunction(String appNo) {
        return functionDao.getAllFunction(appNo);
    }

    @Override
    public List<Function> getFunctionByUserId(String userId, String appNo) {
        return functionDao.getFunctionByUserId(userId, appNo);
    }

    @Override
    public List<Function> getSysFunctionByUserId(String userId, String appNo) {
        return functionDao.getSysFunctionByUserId(userId, appNo);
    }

    @Override
    public List<Function> getSysFunctionByAppNo(String appNo) {
        return functionDao.getSysFunctionByAppNo(appNo);
    }

    @Override
    public boolean deleteFunctionById(String functionId) {
        return functionDao.deleteFunction(functionId);
    }

    @Override
    public List<Function> findFunctionByRoleId(String roleId) {
        return functionDao.FindFunctionByRoleId(roleId);
    }

    @Override
    public String saveFunction(Function function) {
        return functionDao.saveFunction(function);
    }

    @Override
    public void saveFunctionsToRole(List<Permission> permissions) throws Exception {
        functionDao.savePermissions(permissions);
    }

    @Override
    public void updateFunction(Function function) {
        functionDao.updateFunction(function);

    }

    @Override
    public void deleteFunctionsFromRole(List<Permission> permissions) {
        functionDao.DeletePermissionByFunctionId(permissions);

    }

    @Override
    public PageSupport<Function> findAllFunction(String curPageNO, int pageSize, String name, Integer category, String appNo, Long menuId) {
        return functionDao.queryFunction(curPageNO, pageSize, name, category, appNo, menuId);
    }

    @Override
    public PageSupport<Function> findOtherFunctionByHql(String curPageNO, String roleId, int pageSize) {

        return functionDao.findOtherFunctionByRoleId(curPageNO, roleId, pageSize);
    }


}
