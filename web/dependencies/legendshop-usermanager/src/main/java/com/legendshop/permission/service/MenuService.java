/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.permission.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Menu;

/**
 * The Class MenuService.
 */
public interface MenuService  {

	/**
	 * 根据用户名拿到菜单
	 * @param userName
	 * @return
	 */
    public List<Menu> getMenu(String userName);
    
    /**
     * 拿到菜单，调用MenuManagerService
     * @return
     */
    public List<Menu> getManagedMenu();

    /**
     * 根据菜单ID查找
     * @param id
     * @return
     */
    public Menu getMenu(Long id);
    
    public List<KeyValueEntity> getParentMenu(Long parentMenuId);
    
    public void deleteMenu(Menu menu);
    
    public Long saveMenu(Menu menu);
    
    /**
     * 是否有子菜单
     * @param menuId
     * @return
     */
    public boolean hasSubMenu(Long menuId);

    public void updateMenu(Menu menu);

    /**
     * 获取菜单
     * @param pageSize
     * @param grade
     * @param menu
     * @return
     */
	public PageSupport<Menu> getMenu(int pageSize,String curPageNO, int grade, Menu menu, DataSortResult result);

	public PageSupport<Menu> getMenu(String curPageNO, int pageSize, Long parentId, DataSortResult result);

	//
	public PageSupport<Menu> getMenu(String curPageNO, int pageSize, Long parentId, DataSortResult result, Menu menu);

    
}
