/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.permission.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Menu;
import com.legendshop.permission.dao.MenuDao;
import com.legendshop.permission.service.MenuService;
import com.legendshop.spi.service.MenuManagerService;
import com.legendshop.util.AppUtils;

/**
 * The Class MenuServiceImpl.
 */
@Service("menuService")
public class MenuServiceImpl  implements MenuService{
	
	@Autowired
    private MenuDao menuDao;
    
	@Autowired
    private MenuManagerService menuManagerService;

    public List<Menu> getMenu(String userName) {
        return menuDao.getMenu(userName);
    }
    
	@Override
	public List<Menu> getManagedMenu() {
		return menuManagerService.getMenu();
	}


    public Menu getMenu(Long id) {
    	if(id != null && id > 0){
    		return menuDao.getMenu(id);
    	}else{
    		return null;
    	}
        
    }

    public void deleteMenu(Menu menu) {
        menuDao.deleteMenu(menu);
    }

    public Long saveMenu(Menu menu) {
        if (!AppUtils.isBlank(menu.getMenuId())) {
            updateMenu(menu);
            return menu.getMenuId();
        }
        Long parentId = menu.getParentId();
        Integer grade = 1;
        if(parentId != null){
        	Menu parent = menuDao.getMenu(parentId);
        	grade = parent.getGrade() + 1;
        }
        menu.setGrade(grade);
        return  menuDao.saveMenu(menu);
    }

    public void updateMenu(Menu menu) {
        menuDao.updateMenu(menu);
    }
    
	public boolean hasSubMenu(Long menuId) {
		return menuDao.hasSubMenu(menuId);
	}

    //得到所有父节点
	public List<KeyValueEntity> getParentMenu(Long parentMenuId) {
	     List<KeyValueEntity> result = null;
        Menu parentMenu = menuDao.getMenu(parentMenuId) ;
        int i = 0;
        if(parentMenu != null){
        	 result = new ArrayList<KeyValueEntity>();
            result.add(new KeyValueEntity(parentMenu.getId().toString(),parentMenu.getName()));
            while(parentMenu.getParentId() != null && parentMenu.getParentId() > 0){
            	i ++;
            	if(i > 10){//防止数据错误引起系统无限循环
            		break;
            	}
            	parentMenu= menuDao.getMenu(parentMenu.getParentId()) ;
            	if(parentMenu != null){
            		result.add(new KeyValueEntity(parentMenu.getId().toString(),parentMenu.getName()));
            	}
            }
        }
        if(AppUtils.isNotBlank(result)){
            int length = result.size();
            if(length > 1){
            	//反转 reverse the data
            	List<KeyValueEntity> reveseResult =  new ArrayList<KeyValueEntity>();
            	for (int j = length -1; j >= 0; j--) {
            		reveseResult.add(result.get(j));
				}
            	return reveseResult;
            }else{//只有一个结果不需要反转
            	  return result;
            }
          
        }else{
        	return null;
        }
	}

	@Override
	public PageSupport<Menu> getMenu(int pageSize, String curPageNO,int grade, Menu menu, DataSortResult result) {
		return menuDao.getMenu(pageSize, curPageNO, grade,menu, result);
	}
	
	@Override
	public PageSupport<Menu> getMenu(String curPageNO, int pageSize, Long parentId, DataSortResult result) {
		return menuDao.getMenu(curPageNO,pageSize,parentId,result);
	}

	@Override
	public PageSupport<Menu> getMenu(String curPageNO, int pageSize, Long parentId, DataSortResult result, Menu menu) {
		return menuDao.getMenu(curPageNO,pageSize,parentId,result,menu);
	}

}
