package com.legendshop.permission.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendMailEvent;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.MailInfo;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.constant.MailCategoryEnum;
import com.legendshop.model.dto.SendMessageDto;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.permission.page.SecurityAdminPage;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;

@Controller
public class SendMessageAdminController {
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Resource(name = "sendMailProcessor")
    private EventProcessor<MailInfo> sendMailProcessor;
	
	/**
	 * 发送站内信
	 */
	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;
	
	@RequestMapping("/admin/sendmessage/send")
	public String send(HttpServletRequest request, HttpServletResponse response,SendMessageDto messageDto){
		if(AppUtils.isNotBlank(messageDto)){
			if(AppUtils.isNotBlank(messageDto.getType())){
				Integer messageTyp=messageDto.getType();
				if(messageTyp==1){
					sendSiteMessageProcessor.process(new SendSiteMsgEvent("系统", messageDto.getUserName(),messageDto.getTitle() , messageDto.getText()).getSource());
				}if(messageTyp==2){
					sendMailProcessor.process(new SendMailEvent(messageDto.getEmail(),messageDto.getTitle(),messageDto.getText(),"系统",MailCategoryEnum.SYSTEM.value()).getSource());
				}else{
					//TODO TODO短信提醒  Jason 2018-2-26
					//EventHome.publishEvent(new SendSMSEvent(messageDto.getMobile(),messageDto.getText(),messageDto.getUserName(),SMSTypeEnum.SYSTEM));
				}
			}
			request.setAttribute("msg", "succ");
			return PathResolver.getPath(SecurityAdminPage.SENDMESSAGE);
		}
		request.setAttribute("msg", "fail");
		return PathResolver.getPath(SecurityAdminPage.SENDMESSAGE);
	}
	
	@RequestMapping("/admin/sendmessage/load/{userId}")
	public String load(HttpServletRequest request, HttpServletResponse response,@PathVariable String userId,Long type){
		UserDetail userDetail=this.userDetailService.getUserDetailById(userId);
		request.setAttribute("userDetail", userDetail);
		request.setAttribute("type", type);
		return PathResolver.getPath(SecurityAdminPage.SENDMESSAGE);
	}
}

