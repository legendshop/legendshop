/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.permission.dao.impl;

import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.legendshop.business.dao.UserRoleDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.User;
import com.legendshop.model.entity.UserRole;
import com.legendshop.permission.dao.UserDao;
import com.legendshop.util.AppUtils;

/**
 * 用户Dao
 * 
 */
@Repository
public class UserDaoImpl extends GenericDaoImpl<User, String> implements UserDao {

	/** The Constant logger. */
	private static final Log logger = LogFactory.getLog(UserDaoImpl.class);

	@Autowired
	private UserRoleDao userRoleDao;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.permission.dao.UserDao#DeleteUserRole(java.util.List)
	 */
	public void DeleteUserRole(List<UserRole> userRoles) {
		logger.info("DeleteUserRole with size " + userRoles.size());
		userRoleDao.deleteUserRole(userRoles);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.permission.dao.UserDao#findByName(java.lang.String)
	 */
	public List<User> findByName(String name) {
		return this.queryByProperties(new EntityCriterion().eq("name", name));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.permission.dao.UserDao#findUserRoleByRoleId(java.lang.
	 * String)
	 */
	public List<UserRole> findUserRoleByRoleId(String roleId) {
		return userRoleDao.getUserRoleByRoleId(roleId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.permission.dao.UserDao#findUserRoleByUserId(java.lang.
	 * String)
	 */
	public List<UserRole> findUserRoleByUserId(String userId) {
		return userRoleDao.getUserRoleByUserId(userId);
	}

	/**
	 * 更改用户,密码需要Md5加盐加密
	 * 
	 */
	public void updateUser(User user) {
		update(user);
	}
	

	public User getUserByUserId(String userId) {
		return getById(userId);
	}

	@Override
	public PageSupport<User> queryUser(String curPageNO, String name, String enabled) {
		CriteriaQuery cq = new CriteriaQuery(User.class, curPageNO);
		cq.setPageSize(systemParameterUtil.getPageSize());
		if (AppUtils.isNotBlank(name)) {
			cq.like("name", name + "%");// 1
		}
		if (AppUtils.isNotBlank(enabled)) {
			cq.eq("enabled", enabled);
		}
		return queryPage(cq);
	}
}
