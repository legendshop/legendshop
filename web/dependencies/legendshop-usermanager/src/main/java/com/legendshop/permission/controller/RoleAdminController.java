/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.permission.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.config.SystemParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.compare.DataListComparer;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.FunctionEnum;
import com.legendshop.model.dto.UserDto;
import com.legendshop.model.entity.Function;
import com.legendshop.model.entity.Menu;
import com.legendshop.model.entity.Permission;
import com.legendshop.model.entity.PerssionId;
import com.legendshop.model.entity.Role;
import com.legendshop.permission.common.PermissionComparator;
import com.legendshop.permission.form.FunctionForm;
import com.legendshop.permission.form.FunctionMenuDto;
import com.legendshop.permission.form.FunctionMenuListDto;
import com.legendshop.permission.page.SecurityAdminPage;
import com.legendshop.permission.page.SecurityBackPage;
import com.legendshop.permission.page.SecurityFowardPage;
import com.legendshop.permission.page.SecurityRedirectPage;
import com.legendshop.permission.service.FunctionService;
import com.legendshop.permission.service.RoleService;
import com.legendshop.security.UserManager;
import com.legendshop.security.menu.MenuManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;
import com.legendshop.util.StringUtil;

/**
 * 角色管理器.
 */
@Controller
@RequestMapping("/admin/member/role")
public class RoleAdminController extends BaseController {

    /**
     * The log.
     */
    private final static Logger log = LoggerFactory.getLogger(RoleAdminController.class);

    @Autowired
    private FunctionService functionService;

    @Autowired
    private MenuManager menuManager;

    @Autowired
    private RoleService roleService;

    @Resource(name = "updateMenuProcessor")
    private EventProcessor<Object> updateMenuProcessor;

    @Autowired
    private SystemParameterUtil systemParameterUtil;


    /**
           *  删除角色
     */
    @SystemControllerLog(description = "删除角色")
    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable String id) {

        roleService.deleteRoleById(id);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
        return PathResolver.getPath(SecurityRedirectPage.ALL_ROLE);
    }

    /**
     *
          *  加载页面
     */

    @RequestMapping("/load")
    public String load(HttpServletRequest request, HttpServletResponse response) {
        return PathResolver.getPath(SecurityAdminPage.SAVE_ROLE);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.legendshop.core.base.AdminController#query(javax.servlet.http.
     * HttpServletRequest, javax.servlet.http.HttpServletResponse,
     * java.lang.String, java.lang.Object)
     */

    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Role role) {
        int pageSize = systemParameterUtil.getPageSize();
        PageSupport<Role> ps = roleService.findAllRolePage(curPageNO, pageSize, role.getName(), role.getEnabled(),
                role.getAppNo(), role.getCategory());
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("bean", role);
        return PathResolver.getPath(SecurityAdminPage.ROLE_LIST);
    }

    /*
     * 保存角色
     */
    @SystemControllerLog(description = "保存角色")
    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, Role role) {
        if (StringUtil.isEmpty(role.getId())) {
            roleService.saveRole(role);
        } else {
            roleService.updateRole(role);
        }
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
        return PathResolver.getPath(SecurityRedirectPage.ALL_ROLE);
    }

    /*
     * 更新角色
     */

    @RequestMapping(value = "/update/{id}")
    public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable String id) {
        log.info("Action update with id  " + id);
        Role role = roleService.findRoleById(id);
        request.setAttribute("bean", role);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
        return PathResolver.getPath(SecurityAdminPage.SAVE_ROLE);
    }

    /**
     * 查询该角色拥有的用户 userName: 改为昵称
     */
    @RequestMapping(value = "/loadUserByRole")
    public String loadUserByRole(HttpServletRequest request, HttpServletResponse response, String curPageNO,
                                 String userName, String roleId) {
        if (AppUtils.isBlank(roleId)) {
            return null;
        }
        PageSupport<UserDto> ps = null;

         //判断是管理员还是普通用户--通过roleId查询ls_user中有没有值，判断是否为管理员
        Role role = roleService.findRoleById(roleId);

        //ROLE_SUPPLIER -- 供应商
        //ROLE_USER -- 普通用户
        if ("FRONT_END".equals(role.getAppNo())) {
            //查询角色为用户角色
            ps = functionService.loadUserByRole(curPageNO, userName, roleId);
        } else {
            //查询角色为管理员角色
            ps = functionService.loadAdminByRole(curPageNO, userName, roleId);

        }

        ///// 分页查询 ////////

        PageSupportHelper.savePage(request, ps);
        request.setAttribute("roleId", roleId);
        request.setAttribute("userName", userName);
        return PathResolver.getPath(SecurityAdminPage.ROLE_USER_LIST_PAGE);
    }

    /**
     * 根据角色分配权限
     *
     * @param request  the request
     * @param response the response
     * @param roleId       the roleId
     * @return the string
     */
    @RequestMapping(value = "/addFunctions/{roleId}")
    public String addFunctions(HttpServletRequest request, HttpServletResponse response, @PathVariable String roleId) {
        log.info("addFunctions for roleId  " + roleId);
        
        SecurityUserDetail user = UserManager.getUser(request);
        
        List<Function> authorizeFunctions;

        Role role = roleService.findRoleById(roleId);
        request.setAttribute("bean", role);
        String appNo = role.getAppNo();

        // 判断是否有VIEW_ALL_DATA权限，如果有，则加载所有的权限
        if (UserManager.hasFunction(user, FunctionEnum.FUNCTION_VIEW_ALL_DATA.value())) {
            authorizeFunctions = functionService.getAllFunction(appNo);
        } else {
            authorizeFunctions = functionService.getFunctionByUserId(user.getUserId(), appNo);
        }

        // 组装包装类, 存储同一个菜单Menu_Id下的权限数据, 格式为： 二级menuId: List<Function>
        Map<Menu, List<Function>> menuFunctionList = new HashMap<Menu, List<Function>>();

        for (Function function : authorizeFunctions) {
            Long menuId = function.getMenuId();
            Menu top2Menu = menuManager.getParentMenu(menuId); // 取第二级菜单
            Menu currentMenu = menuManager.getCurrentMenu(menuId); // 取当前及的菜单
            if (top2Menu != null && currentMenu != null && currentMenu.getGrade().equals(3)) {// 取第三级菜单
                if (menuFunctionList.containsKey(top2Menu)) {
                    menuFunctionList.get(top2Menu).add(function);
                } else {
                    List<Function> list = new ArrayList<Function>();
                    list.add(function);
                    menuFunctionList.put(top2Menu, list);
                }
            }

        }

        // 有相同父节点的权限。 格式： topMenuName: List<FunctionMenuDto>
        Map<Menu, List<FunctionMenuDto>> menuNameFunctionMap = new HashMap<Menu, List<FunctionMenuDto>>();
        List<FunctionMenuDto> resultFuncList = new ArrayList<FunctionMenuDto>();
        List<FunctionMenuListDto> goupList = new ArrayList<FunctionMenuListDto>();

        for (Map.Entry<Menu, List<Function>> entry : menuFunctionList.entrySet()) {
            Menu menu = entry.getKey();
            List<Function> funcList = entry.getValue();
            FunctionMenuDto funcWrapper = new FunctionMenuDto();
            funcWrapper.setFuncList(funcList);
            funcWrapper.setMenuName(menu.getName());
            funcWrapper.setMenuId(menu.getMenuId());

            resultFuncList.add(funcWrapper);

            if (menuNameFunctionMap.containsKey(menu)) {
                menuNameFunctionMap.get(menu).add(funcWrapper);
            } else {
                List<FunctionMenuDto> list = new ArrayList<FunctionMenuDto>();
                list.add(funcWrapper);
                menuNameFunctionMap.put(menu, list);
            }
        }

        for (Map.Entry<Menu, List<FunctionMenuDto>> entry : menuNameFunctionMap.entrySet()) {
            Menu menu = entry.getKey();
            List<FunctionMenuDto> funcList = entry.getValue();
            FunctionMenuListDto groupWrapper = new FunctionMenuListDto();
            groupWrapper.setFuncWrapperList(funcList);
            groupWrapper.setMenuName(menu.getName());
            goupList.add(groupWrapper);
        }

        // 已经有的权限
        List<Function> selectedList = functionService.findFunctionByRoleId(roleId);

        request.setAttribute("list", selectedList);

        // 普通权限
        request.setAttribute("resultFuncList", goupList);

        // 授权的系统权限
        List<Function> authorizeSysFunctions = functionService.getSysFunctionByUserId(user.getUserId(), appNo);

        // 判断是否有VIEW_ALL_DATA权限，如果有，则加载所有的权限
        if (UserManager.hasFunction(user, FunctionEnum.FUNCTION_VIEW_ALL_DATA.value())) {
            authorizeSysFunctions = functionService.getSysFunctionByAppNo(appNo);
        } else {
            authorizeSysFunctions = functionService.getSysFunctionByUserId(user.getUserId(), appNo);
        }

        request.setAttribute("authorizeSysFunctions", authorizeSysFunctions);

        // 根据角色分配权限
        return PathResolver.getPath(SecurityAdminPage.ROLE_FUNCTION);
    }

    /**
     * Other functions.
     *
     * @param request   the request
     * @param response  the response
     * @param curPageNO the cur page no
     * @param id        the id
     * @return the string
     */
    @RequestMapping(value = "/otherFunctions/{id}")
    public String otherFunctions(HttpServletRequest request, HttpServletResponse response, String curPageNO,
                                 @PathVariable String id) {

        Role role = roleService.findRoleById(id);
        int pageSize = systemParameterUtil.getFrontPageSize();
        PageSupport<Function> ps = functionService.findOtherFunctionByHql(curPageNO, id, pageSize);
        PageSupportHelper.savePage(request, ps);

        request.setAttribute("bean", role);

        return PathResolver.getPath(SecurityBackPage.FIND_OTHER_FUNCTION_LIST);
    }

    /**
     * 保存权限到角色功能.
     *
     * @throws Exception
     */
    @SystemControllerLog(description = "保存权限到角色功能")
    @RequestMapping("/saveFunctionsToRole")
    public String saveFunctionsToRole(HttpServletRequest request, HttpServletResponse response, String roleId,
                                      String funcId) throws Exception {
        if (AppUtils.isBlank(funcId)) {
            return PathResolver.getForwardPath(SecurityFowardPage.FIND_FUNCTION_BY_ROLE.getNativeValue() + "/" + roleId);
        }

        DataListComparer<Permission, Permission> comparer = new DataListComparer<Permission, Permission>(
                new PermissionComparator());

        List<Permission> dbList = functionService.getPermissionByRoleId(roleId);
        List<Permission> permDtoList = constructPermission(roleId, funcId);
        Map<Integer, List<Permission>> compareResult = comparer.compare(permDtoList, dbList, null);

        // List<Permission> lsUpdateSkuList = compareResult.get(0); //不会有update
        // 列表，直接更新
        List<Permission> delPermList = compareResult.get(-1);
        List<Permission> addPermList = compareResult.get(1);

        if (AppUtils.isNotBlank(addPermList)) {
            functionService.saveFunctionsToRole(addPermList);
        }

        if (AppUtils.isNotBlank(delPermList)) {
            functionService.deleteFunctionsFromRole(delPermList);
        }

        // 发布更新菜单指令
        updateMenuProcessor.process(null);
        request.setAttribute("saveStatus", "1");

        String path = PathResolver.getPath(SecurityFowardPage.FIND_FUNCTION_BY_ROLE.getNativeValue() + "/" + roleId, SecurityFowardPage.FIND_FUNCTION_BY_ROLE);
        return path;
    }

    /**
     * 组建外部传过来的权限
     *
     * @param roleId
     * @param funcIds
     * @return
     */
    private List<Permission> constructPermission(String roleId, String funcIds) {
        if (AppUtils.isBlank(funcIds)) {
            return null;
        }
        String[] funcList = funcIds.split(",");
        if (AppUtils.isBlank(funcList)) {
            return null;
        }

        List<Permission> permList = new ArrayList<Permission>();
        for (String funcId : funcList) {
            Permission p = new Permission(roleId, funcId);
            permList.add(p);
        }
        return permList;
    }

    /**
     * Delete permission by role id.
     * 通过角色id删除权限
     *
     * @param request      the request
     * @param response     the response
     * @param roleId       the role id
     * @param functionForm the function form
     * @return the string
     */
    @SystemControllerLog(description = "删除角色权限")
    @RequestMapping("/deletePermissionByRoleId")
    public String deletePermissionByRoleId(HttpServletRequest request, HttpServletResponse response, String roleId,
                                           FunctionForm functionForm) {
        log.info("Action deletePermissionByRoleId with roleId {}", roleId);
        List<Permission> permissions = new ArrayList<Permission>();
        String[] strArray = functionForm.getStrArray();
        for (int i = 0; i < strArray.length; i++) {
            Permission permission = new Permission();
            PerssionId perssionId = new PerssionId();
            perssionId.setRoleId(roleId);
            perssionId.setFunctionId(strArray[i]);
            permission.setId(perssionId);
            permissions.add(permission);
        }
        functionService.deleteFunctionsFromRole(permissions);
        // return
        return PathResolver.getForwardPath(SecurityRedirectPage.FIND_FUNCTION_BY_ROLE.getNativeValue() + "/" + roleId);
    }

}
