/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.permission.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.User;
import com.legendshop.model.entity.UserRole;

/**
 * 用户权限管理.
 */
public interface UserDao extends Dao<User, String>{

	/**
	 * Delete user role.
	 * 
	 * @param userRoles
	 *            the user roles
	 */
	public abstract void DeleteUserRole(List<UserRole> userRoles);

	/**
	 * Find by name.
	 * 
	 * @param name
	 *            the name
	 * @return the list
	 */
	public abstract List<User> findByName(String name);

	/**
	 * Find user role by role id.
	 * 
	 * @param roleId
	 *            the role id
	 * @return the list
	 */
	public abstract List<UserRole> findUserRoleByRoleId(String roleId);

	/**
	 * Find user role by user id.
	 * 
	 * @param userId
	 *            the user id
	 * @return the list
	 */
	public abstract List<UserRole> findUserRoleByUserId(String userId);

	/**
	 * Update user.
	 * 
	 * @param user
	 *            the user
	 */
	public abstract void updateUser(User user);
	

	/**
	 * Gets the user by user id.
	 *
	 * @param userId the user id
	 * @return the user by user id
	 */
	public abstract User getUserByUserId(String userId);

	/**
	 * Query user.
	 *
	 * @param curPageNO the cur page no
	 * @param name the name
	 * @param enabled the enabled
	 * @return the page support< user>
	 */
	public abstract PageSupport<User> queryUser(String curPageNO, String name, String enabled);

}