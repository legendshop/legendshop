/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.permission.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.config.SystemParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.exception.ErrorCodes;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.ApplicationEnum;
import com.legendshop.model.entity.Function;
import com.legendshop.model.entity.Role;
import com.legendshop.model.entity.User;
import com.legendshop.model.entity.UserRole;
import com.legendshop.model.entity.UserRoleId;
import com.legendshop.permission.form.UserRoleForm;
import com.legendshop.permission.page.SecurityAdminPage;
import com.legendshop.permission.page.SecurityBackPage;
import com.legendshop.permission.page.SecurityFowardPage;
import com.legendshop.permission.page.SecurityRedirectPage;
import com.legendshop.permission.service.RoleService;
import com.legendshop.permission.service.UserManagerService;
import com.legendshop.permission.service.UserRoleService;
import com.legendshop.spi.service.AdminUserService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.StringUtil;

/**
 * 用户权限管理
 */
@Controller
@RequestMapping("/admin/member/user")
public class UserAdminController extends BaseController {
	
	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(UserAdminController.class);

	/** The basket service. */
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private UserRoleService userRoleService;
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private UserManagerService userManagerService;
	
	/**
	 * 管理员服务器
	 */
	@Autowired
	private AdminUserService adminUserService;

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	

	/**
	 * 删除用户角色.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param userRoleForm
	 *            the user role form
	 * @return the string
	 */
	@SystemControllerLog(description="删除用户角色")
	@RequestMapping("/deleteUserRoleByUserId")
	public String deleteUserRoleByUserId(HttpServletRequest request, HttpServletResponse response, UserRoleForm userRoleForm,String appNo) {
		String userId = request.getParameter("userId");
		log.info("Action deleteUserRoleByUserId with userId  " + userId);
		
		if(!ApplicationEnum.instance(appNo)){
			throw new BusinessException( "appNo is missing");
		}
		
		List<UserRole> userRoles = new ArrayList<UserRole>();
		String[] strArray = userRoleForm.getStrArray();
		for (String element : strArray) {
			UserRole userRole = new UserRole();
			UserRoleId userRoleId = new UserRoleId();
			userRoleId.setUserId(userId);
			userRoleId.setAppNo(appNo);
			userRoleId.setRoleId(element);
			userRole.setId(userRoleId);
			userRoles.add(userRole);
		}
		userRoleService.deleteUserRole(userRoles);

		request.setAttribute("userId", userId);
		return PathResolver.getRedirectPath(SecurityRedirectPage.FIND_USER_ROLES.getNativeValue() + "/" + userId + "?appNo=" + appNo);
	}

	/**
	 * Save user.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param usersForm
	 *            the users form
	 * @return the action forward
	 */
	@RequestMapping("/saveUser")
	public String saveUser(HttpServletRequest request, HttpServletResponse response, User user) {
		if(userDetailService.isUserExist(user.getName())){
			return PathResolver.getPath("/common/error", SecurityAdminPage.VARIABLE);
		}
		
		String id = userManagerService.saveUser(user);
		
		log.info("success saveUser,id = " + id);

		return PathResolver.getPath(SecurityFowardPage.USERS_LIST);
	}


	/**
	 * Update user status.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param usersForm
	 *            the users form
	 * @return the action forward
	 */
	@RequestMapping("/updateUserStatus")
	public String updateUserStatus(HttpServletRequest request, HttpServletResponse response, User user) {
		log.debug("UserAction updateUserStatus");
		String enabled = user.getEnabled();
		String userId = user.getId();

		User olduser = userManagerService.getUser(userId);
		
		olduser.setEnabled(enabled);
		olduser.setNote(user.getNote());
		userManagerService.updateUser(olduser);
		return PathResolver.getPath(SecurityFowardPage.USERS_LIST);
	}
	
	

	/**
	 * Preupdate status.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	@Deprecated
	@RequestMapping("/preupdateStatus")
	public String preupdateStatus(HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		log.info("Action preupdateStatus with id  " + id);
		
		User user = userManagerService.getUser(id);
		request.setAttribute("user", user);
		return PathResolver.getPath(SecurityBackPage.UPDATE_USER_STATUS);
	}
	

	/**
	 * Update user passowrd.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param usersForm
	 *            the users form
	 * @return the action forward
	 */
	@RequestMapping("/updateUserPassowrd")
	public String updateUserPassowrd(HttpServletRequest request, HttpServletResponse response, User user) {
		log.info("UserAction updateUserById");
		String passwordag = user.getPasswordag();
		if ((user.getPassword() == null) || (user.getPassword() == "")) {
			return PathResolver.getPath("/common/error", SecurityAdminPage.VARIABLE);
		}
		if (!passwordag.endsWith(user.getPassword())) {// 2次密码要相等
			return PathResolver.getPath("/common/error", SecurityAdminPage.VARIABLE);
		}
		//加密密码再传到后台
		if (user.getPassword() != null) {
			user.setPassword(passwordEncoder.encode(user.getPassword()));
		}
		userManagerService.updateUserPassword(user);
		return PathResolver.getPath(SecurityFowardPage.USERS_LIST);
	}

	/**
	 * Find user by id.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	@RequestMapping("/findUserById")
	public String findUserById(HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		log.info("Action findUserById with id  " + id);

		
		User user = userManagerService.getUser(id);
		request.setAttribute("user", user);

		return PathResolver.getPath(SecurityBackPage.UPDATE_USER_PASSWORD);
	}

	/**
	 * Save role to user.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param usersForm
	 *            the users form
	 * @return the action forward
	 */
	@RequestMapping("/saveRoleToUser")
	public String saveRoleToUser(HttpServletRequest request, HttpServletResponse response, String[] strArray) {
		String userId = request.getParameter("userId");
		String appNo = request.getParameter("appNo");
		if(!ApplicationEnum.instance(appNo)){
			throw new BusinessException( "appNo is missing");
		}
		
		List<UserRole> userRoles = new ArrayList<UserRole>();
		for (String element : strArray) {
			UserRole userRole = new UserRole();
			UserRoleId userRoleId = new UserRoleId();
			userRoleId.setRoleId(element);
			userRoleId.setUserId(userId);
			userRoleId.setAppNo(appNo);
			userRole.setId(userRoleId);
			userRoles.add(userRole);
		}

		userRoleService.saveUserRoleList(userRoles);
		return PathResolver.getRedirectPath(SecurityRedirectPage.FIND_USER_ROLES.getNativeValue() + "/" + userId + "?appNo=" + appNo);
	}

	/**
	 * Find other role by user.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	@RequestMapping("/findOtherRoleByUser")
	public String findOtherRoleByUser(HttpServletRequest request, HttpServletResponse response,String appNo) {
		if(AppUtils.isBlank(appNo)){
			throw new BusinessException("appNo is missing");
		}
		
		String curPageNO = request.getParameter("curPageNO");
		String userId = request.getParameter("userId");

		PageSupport<Role>ps = roleService.findOtherRoleByUser(curPageNO, userId, appNo);
		PageSupportHelper.savePage(request, ps);
		
		//装载用户
		if(ApplicationEnum.FRONT_END.value().equals(appNo)){
			request.setAttribute("user", userManagerService.getUser(userId));
		}else if(ApplicationEnum.BACK_END.value().equals(appNo)){
			request.setAttribute("user", adminUserService.getAdminUser(userId));
		}else{
			throw new BusinessException(appNo + " appNo incorrect");
		}

		return PathResolver.getPath(SecurityBackPage.FIND_OTHER_ROLE_BY_USER);
	}

	/**
	 * Find function by user.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	@RequestMapping("/findFunctionByUser")
	public String findFunctionByUser(HttpServletRequest request, HttpServletResponse response, String appNo) {
		String userId = request.getParameter("userId");
		log.info("UserAction findFunctionByUser : " + userId);
		User user = userManagerService.getUser(userId);
		
		List<Function> functions = roleService.findFunctionByUser(userId, appNo);
		request.setAttribute("functions",functions);
		request.setAttribute("user", user);

		return PathResolver.getPath(SecurityBackPage.FIND_FUNCTION_BY_USER);
	}

	/**
	 * Users list.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param curPageNO
	 *            the cur page no
	 * @return the action forward
	 */
	@RequestMapping("/usersList")
	public String usersList(HttpServletRequest request, HttpServletResponse response, String curPageNO) {

		String name = request.getParameter("search") == null ? "" : request.getParameter("search");//搜索内容
		String enabled = request.getParameter("enabled") == null ? "" : request.getParameter("enabled");
		Integer pageSize =  systemParameterUtil.getPageSize();
		
		PageSupport<User> ps = userManagerService.queryUser(curPageNO, name, enabled,pageSize);
		PageSupportHelper.savePage(request, ps);
		
		request.setAttribute("name", name);
		request.setAttribute("enabled", enabled);

		return PathResolver.getPath(SecurityBackPage.USER_LIST_PAGE);
	}

	/**
	 * Find role by user.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the action forward
	 */
	@RequestMapping("/findRoleByUser")
	public String findRoleByUser(HttpServletRequest request, HttpServletResponse response) {
		String userId = request.getParameter("userId");
		String appNo = request.getParameter("appNo");
		log.info("UserAction findOtherRoleByUser : " + userId);
		User user = userManagerService.getUser(userId);
		
		List<Role> roles = roleService.findRolesByUser(userId, appNo);
		request.setAttribute("roles", roles);
		request.setAttribute("user", user);

		return PathResolver.getPath(SecurityBackPage.FIND_ROLE_BY_USER_PAGE);
	}

	// new interface

	/**
	 * Load.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the string
	 */

	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(SecurityBackPage.MODIFY_USER);
	}

	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable String id) {
		return null;
	}

	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, User user) {

		String name = user.getName();
		String enabled = user.getEnabled();
		String mobile = user.getUserMobile();
		Integer pageSize =  systemParameterUtil.getPageSize();
		
		PageSupport<User> ps = userManagerService.queryUser(curPageNO, name, enabled, mobile, pageSize);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("bean", user);
		return PathResolver.getPath(SecurityAdminPage.USER_LIST_PAGE);
	} 

	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, User user) {
		
		if (userDetailService.isUserExist(user.getUserName())) {
			throw new BusinessException("not login yet", ErrorCodes.BUSINESS_ERROR);
		}

		String id = user.getId();

		if (StringUtil.isEmpty(id)) {
			id = userManagerService.saveUser(user);
		} else {
			userManagerService.updateUser(user);
		}
		log.info("success saveUser,id = " + id);

		return PathResolver.getPath(SecurityFowardPage.USERS_LIST);
	}

	@RequestMapping(value = "/updatePassword")
	public String updatePassword(HttpServletRequest request, HttpServletResponse response, User user, String appNo) {

		log.info("UserAction updateUserById");
		String passwordag = user.getPasswordag();
		if ((user.getPassword() == null) || (user.getPassword() == "" || user.getPassword().length() < 6)) {
			return PathResolver.getPath(SecurityAdminPage.VARIABLE);
		}
		if (!passwordag.equals(user.getPassword())) {// 2次密码要相等
			return PathResolver.getPath("/common/error", SecurityAdminPage.VARIABLE);
		}
		
		//更新密码
		if(ApplicationEnum.FRONT_END.value().equals(appNo)){
			User originUser = userManagerService.getUser(user.getId());
			if (user.getPassword() != null) {
				originUser.setPassword(passwordEncoder.encode(user.getPassword()));
			}
			userManagerService.updateUserPassword(originUser);

			return PathResolver.getPath(SecurityRedirectPage.USERINFO_LIST);//更新普通用户密码
		}else if(ApplicationEnum.BACK_END.value().equals(appNo)){
			adminUserService.updateAdminUserPassword(user.getId(),passwordEncoder.encode(user.getPassword()));//更新管理员密码
			return PathResolver.getPath(SecurityRedirectPage.ADMIN_USERS_LIST);
		}else{
			throw new BusinessException(appNo + " appNo incorrect");
		}

	}
	/**
	 * 修改用户信息
	 * @param request
	 * @param response
	 * @param id
	 * @param appNo
	 * @return
	 */
	@RequestMapping(value = "/update/{id}")
	@SystemControllerLog(description="修改用户信息")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable String id, String appNo) {
		log.info("Action findUserById with id  " + id);
		if(!ApplicationEnum.instance(appNo)){
			throw new BusinessException( "appNo is missing");
		}
		//装载用户
		if(ApplicationEnum.FRONT_END.value().equals(appNo)){
			request.setAttribute("type","user");//添加标识
			User user = userManagerService.getUser(id);
			request.setAttribute("bean", user);
		}else if(ApplicationEnum.BACK_END.value().equals(appNo)){
			request.setAttribute("type","admin");//添加标识
			request.setAttribute("bean", adminUserService.getAdminUser(id));
		}else{
			throw new BusinessException(appNo + " appNo incorrect");
		}
		return PathResolver.getPath(SecurityAdminPage.UPDATE_USER_PASSWORD);
	}

	@RequestMapping(value = "/roles/{id}")
	public String roles(HttpServletRequest request, HttpServletResponse response, @PathVariable String id, String appNo) {
		log.info("UserAction findOtherRoleByUser : " + id);
		
		if(!ApplicationEnum.instance(appNo)){
			throw new BusinessException( "appNo is missing");
		}
		
		List<Role> roles = roleService.findRolesByUser(id, appNo);
		request.setAttribute("list", roles);
		
		//装载用户
		if(ApplicationEnum.FRONT_END.value().equals(appNo)){
			User user = userManagerService.getUser(id);
			request.setAttribute("type","user");
			request.setAttribute("bean", user);
		}else if(ApplicationEnum.BACK_END.value().equals(appNo)){
			request.setAttribute("type","admin");
			request.setAttribute("bean", adminUserService.getAdminUser(id));
		}else{
			throw new BusinessException(appNo + " appNo incorrect");
		}
		request.setAttribute("appNo", appNo);
		return PathResolver.getPath(SecurityAdminPage.FIND_ROLE_BY_USER_PAGE);
	}
	
	/**
	 * 增加用户角色
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param id
	 * @param appNo
	 * @return
	 */
	@SystemControllerLog(description="增加用户角色")
	@RequestMapping(value = "/otherRoles/{id}")
	public String otherRoles(HttpServletRequest request, HttpServletResponse response, String curPageNO, @PathVariable String id, String appNo) {
		if(!ApplicationEnum.instance(appNo)){
			throw new BusinessException( "appNo is missing");
		}
		PageSupport<Role> ps = roleService.findOtherRoleByUser(curPageNO, id, appNo);
		
		PageSupportHelper.savePage(request, ps);
		//装载用户
		if(ApplicationEnum.FRONT_END.value().equals(appNo)){
			request.setAttribute("bean", userManagerService.getUser(id));
		}else if(ApplicationEnum.BACK_END.value().equals(appNo)){
			request.setAttribute("bean", adminUserService.getAdminUser(id));
		}else{
			throw new BusinessException(appNo + " appNo incorrect");
		}

		return PathResolver.getPath(SecurityAdminPage.FIND_OTHER_ROLE_BY_USER);
	}

	@Deprecated
	@RequestMapping(value = "/deleteRoles/{id}")
	public String deleteRoles(HttpServletRequest request, HttpServletResponse response, String[] strArray, @PathVariable String id) {

		log.info("Action deleteUserRoleByUserId with userId  " + id);
		List<UserRole> userRoles = new ArrayList<UserRole>();
		for (String element : strArray) {
			UserRole userRole = new UserRole();
			UserRoleId userRoleId = new UserRoleId();
			userRoleId.setUserId(id);
			userRoleId.setRoleId(element);
			userRole.setId(userRoleId);
			userRoles.add(userRole);
		}

		userRoleService.deleteUserRole(userRoles);
		
		return PathResolver.getPath(SecurityFowardPage.FIND_ROLE_BY_USER);
	}

	@RequestMapping(value = "/functions/{id}")
	public String functions(HttpServletRequest request, HttpServletResponse response, @PathVariable String id, String appNo) {
		log.info("UserAction findFunctionByUser : " + id);
		if(!ApplicationEnum.instance(appNo)){
			throw new BusinessException( "appNo is missing");
		}
		List<Function> functions = roleService.findFunctionByUser(id, appNo);
		request.setAttribute("list", functions);
		//装载用户
		if(ApplicationEnum.FRONT_END.value().equals(appNo)){

			request.setAttribute("type","user");//添加标识
			request.setAttribute("bean", userManagerService.getUser(id));
		}else if(ApplicationEnum.BACK_END.value().equals(appNo)){
			request.setAttribute("type","admin");//添加标识
			request.setAttribute("bean", adminUserService.getAdminUser(id));
		}else{
			throw new BusinessException(appNo + " appNo incorrect");
		}
		return PathResolver.getPath(SecurityAdminPage.FIND_FUNCTION_BY_USER);
	}
	
	@RequestMapping(value = "/isUserExist")
	@ResponseBody
	public String isUserExist(HttpServletRequest request, HttpServletResponse response, String userName) {
		User user = userDetailService.getUserByName(userName);
		if (AppUtils.isNotBlank(user)) {
			return user.getId();
		}else{
			return "";
		}

	}
}
