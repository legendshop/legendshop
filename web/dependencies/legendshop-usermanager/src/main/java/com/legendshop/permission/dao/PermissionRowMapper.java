package com.legendshop.permission.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.legendshop.model.entity.Permission;
import com.legendshop.model.entity.PerssionId;

public 	class PermissionRowMapper implements RowMapper<Permission>{
	
	public Permission mapRow(ResultSet rs, int rowNum) throws SQLException {
		Permission p = new Permission();
		PerssionId id = new PerssionId();
		id.setFunctionId(rs.getString("function_id"));
		id.setRoleId(rs.getString("role_id"));
		p.setId(id);
		return p;
	}
}
