package com.legendshop.permission.service;

import java.util.List;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Function;
import com.legendshop.model.entity.Role;

public interface RoleService {
	
	/**
	 * 
	 * @param userId 用户ID
	 * @param appNo ApplicationEnum
	 * @return
	 */
	List<String> findRolesNameByUser(String userId, String appNo);

	/**
	 * 
	 * @param id
	 * @param value
	 * @return
	 */
	List<String> findFunctionsByUser(String id, String value);

	/**
	 * get perm from shop role perm 获取子账号的权限
	 * @param shopRoleId
	 * @return
	 */
	List<String> getPermByShopAccountRole(Long shopRoleId);

	Role findRoleById(String roleId);

	void deleteRoleById(String id);

	PageSupport<Role> findAllRole(String search, String myaction, String curPageNO);

	List<Role> findRoleByFunction(String functionId);

	String saveRole(Role role);

	void updateRole(Role role);

	PageSupport<Role> findAllRolePage(String curPageNO, int pageSize, String name, String enabled, String appNo,Integer category);

	PageSupport<Role> findOtherRoleByUser(String curPageNO, String userId, String appNo);

	List<Function> findFunctionByUser(String userId, String appNo);

	List<Role> findRolesByUser(String userId, String appNo); 

	List<String> findRoleIdsByUser(String userId, String appNo); 

}
