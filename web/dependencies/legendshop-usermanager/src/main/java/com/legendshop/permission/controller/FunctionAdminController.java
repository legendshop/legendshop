/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.permission.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.config.SystemParameterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.UserDto;
import com.legendshop.model.entity.Function;
import com.legendshop.model.entity.Menu;
import com.legendshop.model.entity.Permission;
import com.legendshop.model.entity.PerssionId;
import com.legendshop.model.entity.Role;
import com.legendshop.permission.form.MenuTree;
import com.legendshop.permission.page.SecurityAdminPage;
import com.legendshop.permission.page.SecurityBackPage;
import com.legendshop.permission.page.SecurityFowardPage;
import com.legendshop.permission.service.FunctionService;
import com.legendshop.permission.service.MenuService;
import com.legendshop.permission.service.RoleService;
import com.legendshop.spi.service.MenuManagerService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.StringUtil;

/**
 * 权限管理.
 */
@Controller
@RequestMapping("/admin/member/right")
public class FunctionAdminController extends BaseController{
	
	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(FunctionAdminController.class);
	
	@Autowired
	private MenuService menuService;
	
	@Autowired
	private FunctionService functionService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private MenuManagerService menuManagerService;
	
	@Resource(name="updateMenuProcessor")
	private EventProcessor<Object> updateMenuProcessor;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * Delete function by id.
	 * 删除权限
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param curPageNO
	 *            the cur page no
	 * @param roleId
	 *            the role id
	 * @param functionForm
	 *            the function form
	 * @return the string
	 */
	@SystemControllerLog(description="删除权限")
	@RequestMapping("/deleteFunctionById")
	public String deleteFunctionById(HttpServletRequest request, HttpServletResponse response,String id) {
		log.info("Struts Action deleteFunctionById with id  " + id);
		functionService.deleteFunctionById(id);
		return PathResolver.getPath(SecurityFowardPage.FUNCTION_LIST_QUERY);
	}

	/**
	 * Find function by id.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param id
	 *            the id
	 * @return the string
	 */
	@RequestMapping("/findFunctionById")
	public String findFunctionById(HttpServletRequest request, HttpServletResponse response, String id) {
		Function function = functionService.getFunctionsById(id);
		request.setAttribute("function", function);
		return PathResolver.getPath(SecurityBackPage.UPDATE_FUNCTION);
	}

	/**
	 * Find function by role.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param roleId
	 *            the role id
	 * @return the string
	 */
	@RequestMapping("/findFunctionByRole")
	public String findFunctionByRole(HttpServletRequest request, HttpServletResponse response, String roleId) {
		Role role = roleService.findRoleById(roleId);
		List<Function> list = functionService.findFunctionByRoleId(roleId);
		request.setAttribute("list", list);
		request.setAttribute("role", role);

		return PathResolver.getForwardPath(SecurityFowardPage.FIND_FUNCTION_BY_ROLE.getNativeValue() + "/" + roleId);
	}

	/**
	 * Save function.
	 * 保存权限
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param functionForm
	 *            the function form
	 * @return the string
	 */
	@SystemControllerLog(description="保存权限")
	@RequestMapping("/saveFunction")
	public String saveFunction(HttpServletRequest request, HttpServletResponse response, Function function) {
		String id = functionService.saveFunction(function);
		log.info("success saveFunction,id = " + id);
		return PathResolver.getPath(SecurityFowardPage.FUNCTION_LIST_QUERY);
	}

	/**
	 * Save functions to role list.
	 * 保存权限给角色
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param roleId
	 *            the role id
	 * @param permissionForm
	 *            the permission form
	 * @return the string
	 */
	@SystemControllerLog(description="保存权限给角色")
	@RequestMapping("/saveFunctionsToRoleList")
	public String saveFunctionsToRoleList(HttpServletRequest request, HttpServletResponse response, String roleId,
			String functionId,String[] strArray) {
		List<Permission> permissions = new ArrayList<Permission>();
		for (int i = 0; i < strArray.length; i++) {
			Permission permission = new Permission();
			PerssionId perssionId = new PerssionId();
			perssionId.setRoleId(roleId);
			perssionId.setFunctionId(strArray[i]);
			permission.setId(perssionId);
			permissions.add(permission);
		}
		try {
			functionService.saveFunctionsToRole(permissions);
		} catch (Exception e) {
			e.printStackTrace();
		}

		//发布更新菜单指令
		updateMenuProcessor.process(null);
		return PathResolver.getForwardPath(SecurityFowardPage.FIND_FUNCTION_BY_ROLE.getNativeValue() + "/" + roleId);
	}


	/**
	 * Update function by id.
	 * 更新权限
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param functionForm
	 *            the function form
	 * @return the string
	 */
	@SystemControllerLog(description="更新权限")
	@RequestMapping("/updateFunctionById")
	public String updateFunctionById(HttpServletRequest request, HttpServletResponse response, Function function) {
		log.debug("Struts FunctionAction updateFunctionById");
		functionService.updateFunction(function);
		return PathResolver.getPath(SecurityFowardPage.FUNCTION_LIST_QUERY);
	}

	// ////////////////////////////Role Controller
	/**
	 * 根据角色id删除权限
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param curPageNO
	 *            the cur page no
	 * @param roleId
	 *            the role id
	 * @param functionForm
	 *            the function form
	 * @return the string
	 */
	@SystemControllerLog(description="根据角色id删除权限")
	@RequestMapping("/deletePermissionByRoleId")
	public String deletePermissionByRoleId(HttpServletRequest request, HttpServletResponse response, String curPageNO,
			String roleId, String[] strArray) {
		List<Permission> permissions = new ArrayList<Permission>();
		for (int i = 0; i < strArray.length; i++) {
			Permission permission = new Permission();
			PerssionId perssionId = new PerssionId();
			perssionId.setRoleId(roleId);
			perssionId.setFunctionId(strArray[i]);
			permission.setId(perssionId);
			permissions.add(permission);
		}
		functionService.deleteFunctionsFromRole(permissions);
		request.setAttribute("roleId", roleId);
		return PathResolver.getForwardPath(SecurityFowardPage.FIND_FUNCTION_BY_ROLE.getNativeValue() + "/" + roleId);
	}

	/**
	 * Find role by id.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param id
	 *            the id
	 * @return the string
	 */
	@RequestMapping("/findRoleById")
	public String findRoleById(HttpServletRequest request, HttpServletResponse response, String id) {
		Role role = roleService.findRoleById(id);
		request.setAttribute("role", role);
		return PathResolver.getPath(SecurityFowardPage.FIND_ALL_ROLE);
	}

	/**
	 * Delete role by id.
	 * 删除角色
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param id
	 *            the id
	 * @return the string
	 */
	@SystemControllerLog(description="删除角色")
	@RequestMapping("/deleteRoleById")
	public String deleteRoleById(HttpServletRequest request, HttpServletResponse response, String id) {
		
		roleService.deleteRoleById(id);

		return PathResolver.getPath(SecurityFowardPage.FIND_ALL_ROLE);
	}

	/**
	 * Find all role.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the string
	 */
	@RequestMapping("/findAllRole")
	public String findAllRole(HttpServletRequest request, HttpServletResponse response) {
		// 权限检查
		String curPageNO = request.getParameter("curPageNO");
		String myaction;
		String search = request.getParameter("search");
		String action = "findAllRole";
		if (search == null) {
			search = "";
			myaction = action;
		} else {
			myaction = action + "?search=" + search;
		}

		PageSupport<Role> ps = roleService.findAllRole(search,myaction, curPageNO);
		PageSupportHelper.savePage(request, ps);
		
		request.setAttribute("search", search);
		return PathResolver.getPath(SecurityBackPage.ROLE_LIST);
	}

	/**
	 * Find role by function.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param functionId
	 *            the function id
	 * @return the string
	 */
	@RequestMapping("/findRoleByFunction")
	public String findRoleByFunction(HttpServletRequest request, HttpServletResponse response, String functionId) {

		Function function = functionService.getFunctionsById(functionId);
		List<Role> list = roleService.findRoleByFunction(functionId);

		request.setAttribute("list", list);
		request.setAttribute("function", function);

		return PathResolver.getPath(SecurityBackPage.FIND_ROLE_BY_FUNCTION);
	}

	/**
	 * Save role.
	 * 保存角色
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param roleForm
	 *            the role form
	 * @return the string
	 */
	@SystemControllerLog(description="保存角色")
	@RequestMapping("/saveRole")
	public String saveRole(HttpServletRequest request, HttpServletResponse response, Role role) {
		
		roleService.saveRole(role);

		return PathResolver.getPath(SecurityFowardPage.FIND_ALL_ROLE);
	}

	/**
	 * Update role by id.
	 * 更新角色
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param roleForm
	 *            the role form
	 * @return the string
	 */
	@SystemControllerLog(description="更新角色")
	@RequestMapping("/updateRoleById")
	public String updateRoleById(HttpServletRequest request, HttpServletResponse response, Role role) {
		
		roleService.updateRole(role);

		return PathResolver.getPath(SecurityFowardPage.FIND_ALL_ROLE);
	}
	
	@SystemControllerLog(description="删除权限")
	@RequestMapping(value = "/delete/{id}",  method = RequestMethod.POST )
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable String id) {
		functionService.deleteFunctionById(id);
		return PathResolver.getPath(SecurityFowardPage.FUNCTION_LIST_QUERY);
	}

	@RequestMapping("/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(SecurityBackPage.UPDATE_FUNCTION);
	}

	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Function function) {
		request.setAttribute("bean", function);
		List<Menu> menuList = menuManagerService.getMenu();
		List<MenuTree> nodeList = new ArrayList<MenuTree>(menuList.size());
		
		request.setAttribute("curPageNO", curPageNO);
		request.setAttribute("function", function);		
		for( Menu me : menuList ){
			nodeList.add(new MenuTree( me.getMenuId(), me.getParentId(), me.getName(),false));
		}
		
		request.setAttribute("menuId",function.getMenuId());
			
		request.setAttribute("menuList", JSONUtil.getJson(nodeList));

		return PathResolver.getPath(SecurityAdminPage.FUNCTION_LIST);
	}
	
	/**
	 * 查询权限内容
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param function
	 * @return
	 */
	@RequestMapping("/querycontent")
	public String queryright(HttpServletRequest request, HttpServletResponse response, String curPageNO, Function function) {
		int pageSize = systemParameterUtil.getFrontPageSize();
		PageSupport<Function> ps = functionService.findAllFunction(curPageNO,pageSize,function.getName(),function.getCategory(),function.getAppNo(),function.getMenuId());
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("bean", function);
		return PathResolver.getPath(SecurityBackPage.FUNCTION_LIST_CONTENT);
	}
	
	@SystemControllerLog(description="保存权限")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, Function function) {
		String id = function.getId();
		if (StringUtil.isEmpty(id)) {
			id = functionService.saveFunction(function);
		} else {
			functionService.updateFunction(function);
		}
		//发布更新菜单指令
		updateMenuProcessor.process(null);
		return PathResolver.getPath(SecurityFowardPage.FUNCTION_LIST_QUERY);
	}
	
	@SystemControllerLog(description="更新权限")
	@RequestMapping(value = "/update/{id}")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable String id) {
		Function function = functionService.getFunctionsById(id);
		request.setAttribute("bean", function);
		if(function.getMenuId() != null){
			Menu menu  =menuService.getMenu(function.getMenuId());
			request.setAttribute("menu", menu);
		}

		return PathResolver.getPath(SecurityBackPage.UPDATE_FUNCTION);
	}

	@RequestMapping(value = "/roles/{functionId}")
	public String roles(HttpServletRequest request, HttpServletResponse response, @PathVariable String functionId) {

		Function function = functionService.getFunctionsById(functionId);
		List<Role> list = roleService.findRoleByFunction(functionId);

		request.setAttribute("list", list);
		request.setAttribute("bean", function);

		return PathResolver.getPath(SecurityAdminPage.FIND_ROLE_BY_FUNCTION);
	}
	
	
	/**
	 * 加载菜单树
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/loadFunctionMenu")
	public String loadFunctionMenu(HttpServletRequest request, HttpServletResponse response, String menuId) {
		
		List<Menu> menuList = menuManagerService.getMenu();
		
		request.setAttribute("menuList", JSONUtil.getJson(constructMenuTree(menuList, menuId)));
		
		String path = PathResolver.getPath(SecurityBackPage.MENU_LOADFUNCMENU_PAGE);
		return path;
	}
	
	/**
	 * 用Ajax返回结果
	 * @param request
	 * @param response
	 * @param menuId
	 * @return
	 */
	@RequestMapping(value = "/loadAjaxFunctionMenu")
	public @ResponseBody List<MenuTree> loadAjaxFunctionMenu(HttpServletRequest request, HttpServletResponse response, String menuId) {
		List<Menu> menuList = menuManagerService.getMenu();
		return constructMenuTree(menuList, menuId);
	}
	
	/**
	 * 查询该权限拥有的用户
	 */
	@RequestMapping(value = "/loadUserByFunc")
	public String loadUserByFunc(HttpServletRequest request, HttpServletResponse response, String curPageNO,String userName,String funcId){
		if (AppUtils.isBlank(funcId)) {
			return null;
		}
		
		Function function = functionService.getFunctionsById(funcId);
		if(function == null){
			return null;
		}else{
			request.setAttribute("func", function);
		}
		/////  分页查询 ////////
   		PageSupport<UserDto> ps = functionService.loadUserByFunction(curPageNO,userName,funcId );
   		PageSupportHelper.savePage(request, ps);
   		request.setAttribute("funcId", funcId);
   		request.setAttribute("userName", userName);
		return PathResolver.getPath(SecurityAdminPage.FUNCTION_USER_LIST_PAGE);
	}
	
	
	/**
	 * 组建树结构
	 * @param menuList 菜单列表
	 * @param menuId  当前菜单ID
	 * @return
	 */
	private List<MenuTree> constructMenuTree(List<Menu> menuList, String menuId ){
		List<MenuTree> treeList = new ArrayList<MenuTree>(menuList.size());
		
		//组建树结构
		for( Menu me : menuList ){
			treeList.add(new MenuTree( me.getMenuId(), me.getParentId(), me.getName()));
		}

		for( MenuTree tree : treeList ) {
			if( tree.getId().equals(menuId) ){
				tree.setChecked(true); //选中当前的菜单
				break;
			}
		}
		return treeList;
	}
	
	
}
