/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.permission.common;

import com.legendshop.base.compare.DataComparer;
import com.legendshop.model.entity.Permission;

public class PermissionComparator implements DataComparer<Permission, Permission>{

	/**
	 * 不需要更新
	 */
	@Override
	public boolean needUpdate(Permission dto, Permission dbObj, Object obj) {
		return false;
	}

	/**
	 * 是否存在
	 */
	@Override
	public boolean isExist(Permission dto, Permission dbObj) {
		return dto.getId().equals(dbObj.getId());
	}

	/**
	 * 也不需要copy，直接采用界面的值
	 */
	@Override
	public Permission copyProperties(Permission dtoj, Object obj) {
		return dtoj;
	}
	

	
}
