/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.permission.dao.impl;

import com.legendshop.base.cache.FunctionUpdate;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.framework.commond.JCFException;
import com.legendshop.model.constant.ApplicationEnum;
import com.legendshop.model.dto.UserDto;
import com.legendshop.model.entity.Function;
import com.legendshop.model.entity.Permission;
import com.legendshop.model.entity.PerssionId;
import com.legendshop.permission.dao.FunctionDao;
import com.legendshop.permission.dao.PermissionRowMapper;
import com.legendshop.util.AppUtils;
import com.legendshop.util.StringUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 权限Dao
 *
 */
@Repository
public class FunctionDaoImpl extends GenericDaoImpl<Function, String> implements FunctionDao, InitializingBean {

	/** The Constant logger. */
	private static final Log logger = LogFactory.getLog(FunctionDaoImpl.class);

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	//后台权限 format url: list of function
	private Map<String,List<Function>> functionMap = null;

	/** The find function by role id. */
	private String findFunctionByRoleId="select f.id, f.name, f.parent_name as parentName, f.url, f.protect_function as protectFunction, f.note from ls_role r,ls_perm p,ls_func f where r.id=p.role_id and f.id=p.function_id  and r.app_no = f.app_no and r.id = ?";

	/** The find other function by role id. */
	private String findOtherFunctionByRoleId="select id, name, parent_name as parentName, url, protect_function as protectFunction, note from ls_func where id not in (select f.id from ls_role r,ls_perm p,ls_func f where r.id=p.role_id and f.id=p.function_id and r.id = ?)";

	/** The find other function by role id hql. */
	private String findOtherFunctionByRoleIdHQL="select id, name, parent_name as parentName, url, protect_function as protectFunction, note from ls_func where id not in (select f.id from ls_role r,ls_perm p,ls_func f where r.id=p.role_id and f.id=p.function_id and r.id = ?) order by name";

	/** The find other function by role id hql count. */
	private String findOtherFunctionByRoleIdHQLCount="select count(*) from Function where id not in (select f.id from Role r,Permission p,Function f where r.id=p.id.roleId and f.id=p.id.functionId and r.id = ?)";

	//find by functions
	private String loadUserByFunctionAllSQL = "SELECT count(*) FROM (SELECT DISTINCT u.id, u.real_name as nickName,u.name, u.enabled,u.note FROM ls_usr_role ur ,ls_admin_user u WHERE  ur.user_id=u.id  AND ur.role_id IN (SELECT  p.role_id FROM ls_func f ,ls_perm  p  WHERE  f.id = p.function_id AND p.function_id =?)) t";

	private String loadUserByFunctionSQL = "SELECT DISTINCT u.id, u.real_name as nickName,u.name, u.enabled,u.note FROM ls_usr_role ur ,ls_admin_user u WHERE  ur.user_id=u.id  AND ur.role_id IN (SELECT  p.role_id FROM ls_func f ,ls_perm  p  WHERE  f.id = p.function_id AND p.function_id =?)";

	//find by roles
	private String loadUserByRoleAllSQL = "SELECT count(*) FROM ls_usr_role ur ,ls_user u, ls_usr_detail  ud " +
			"WHERE  ur.user_id=u.id AND u.id = ud.user_id  AND ur.role_id = ?";

	private String loadUserByRoleSQL = "SELECT ud.nick_name AS nickName, u.name, u.id, u.enabled,u.note FROM ls_usr_role ur ,ls_user u, ls_usr_detail ud " +
		"WHERE  ur.user_id=u.id AND u.id = ud.user_id  AND ur.role_id = ?";

	//find by roles Admin
	private String loadAdminByRoleAllSQL = "SELECT count(*) FROM ls_usr_role ur ,ls_admin_user du " +
			"WHERE  ur.user_id=du.id  AND ur.role_id = ?";

	private String loadAdminByRoleSQL = "SELECT du.real_name AS nickName, du.name, du.id, du.enabled,du.note FROM ls_usr_role ur,ls_admin_user du " +
			"WHERE  ur.user_id=du.id AND ur.role_id = ?";


	private String getFunctionByUserIdSQL = "select m.menu_id,m.name,f.protect_function,f.note,f.id,m.parent_id as parentName from ls_menu m,ls_func f where m.menu_id = f.menu_id and f.id in( select p.function_id from ls_perm p INNER JOIN ls_role r on p.role_id = r.id INNER JOIN ls_usr_role ur on ur.role_id = r.id where ur.user_id= ? ) order by m.parent_id ";

	private String getFunctionByUserIdAppNoSQL = "select m.menu_id,m.name,f.protect_function,f.note,f.id,m.parent_id as parentName from ls_menu m,ls_func f where m.menu_id = f.menu_id  and f.app_no = ? and f.id in( select p.function_id from ls_perm p INNER JOIN ls_role r on p.role_id = r.id INNER JOIN ls_usr_role ur on ur.role_id = r.id where ur.user_id= ? ) order by m.parent_id ";

	private String  getSysFunctionByUserIdSQL = "select * from ls_func f where f.category=1 and f.id in ( select p.function_id from ls_perm p INNER JOIN ls_role r on p.role_id = r.id INNER JOIN ls_usr_role ur on ur.role_id = r.id where ur.user_id= ? ) ";
	/* (non-Javadoc)
	 * @see com.legendshop.permission.dao.impl.FunctionDao#setFindFunctionByRoleId(java.lang.String)
	 */

	/**
	 * 初始化
	 */
	public void afterPropertiesSet(){
        super.afterPropertiesSet();
		functionMap = new HashMap<String, List<Function>>();
		//查找后台权限
		List<Function> allFunc = query("select * from ls_func where url is not null AND app_no = ?", Function.class, ApplicationEnum.BACK_END.value());
		for (Function function : allFunc) {
			String funcUrl = function.getUrl();
			if(AppUtils.isNotBlank(funcUrl)){
				String[] urls = funcUrl.split(",");
				for (String url : urls) {
					String id = url.trim();
					if(AppUtils.isNotBlank(id)){
						List<Function> funcList = functionMap.get(id);
						if(AppUtils.isNotBlank(funcList)){
							funcList.add(function);
						}else{
							List<Function> list = new ArrayList<Function>();
							list.add(function);
							functionMap.put(id, list);
						}

					}

				}
			}
		}
		logger.info("function list init success");
	}

	/**
	 * 根据URL地质获取权限
	 *
	 * @param url
	 * @return
	 */
	public List<Function> getFunctionsByUrl(String url) {
		List<Function> result = new ArrayList<Function>();
		PathMatcher matcher = new AntPathMatcher();
		for (String u : functionMap.keySet()) {
			if(matcher.match(u+"/**", url)){
			//if(url.startsWith(u)){
				result.addAll(functionMap.get(u));
			}
		}
		return result;
	}


	public void setFindFunctionByRoleId(String findFunctionByRoleId) {
		this.findFunctionByRoleId = findFunctionByRoleId;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.permission.dao.impl.FunctionDao#setFindOtherFunctionByRoleId(java.lang.String)
	 */
	public void setFindOtherFunctionByRoleId(String findOtherFunctionByRoleId) {
		this.findOtherFunctionByRoleId = findOtherFunctionByRoleId;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.permission.dao.impl.FunctionDao#setFindOtherFunctionByRoleIdHQL(java.lang.String)
	 */
	public void setFindOtherFunctionByRoleIdHQL(String findOtherFunctionByRoleIdHQL) {
		this.findOtherFunctionByRoleIdHQL = findOtherFunctionByRoleIdHQL;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.permission.dao.impl.FunctionDao#FindFunctionByRoleId(java.lang.String)
	 */
	public List<Function> FindFunctionByRoleId(String roleId) {
		logger.info(StringUtil.convert(findFunctionByRoleId, new String[] { roleId }));
		return query(findFunctionByRoleId, Function.class, roleId);
	}

	/* (non-Javadoc)
	 * @see com.legendshop.permission.dao.impl.FunctionDao#FindOtherFunctionByRoleId(java.lang.String)
	 */
	public List<Function> FindOtherFunctionByRoleId(String roleId) {
		logger.info(StringUtil.convert(findOtherFunctionByRoleId, new String[] { roleId }));
		return query(findOtherFunctionByRoleId, Function.class, roleId);
	}

	/* (non-Javadoc)
	 * @see com.legendshop.permission.dao.impl.FunctionDao#FindOtherFunctionByRoleId(com.legendshop.core.dao.support.HqlQuery, java.lang.String)
	 */
	public PageSupport FindOtherFunctionByRoleId(final SimpleSqlQuery query, String roleId) throws JCFException {
		logger.info(StringUtil.convert(findOtherFunctionByRoleIdHQL, new String[] { roleId }));
		query.setQueryString(findOtherFunctionByRoleIdHQL);
		query.setAllCountString(findOtherFunctionByRoleIdHQLCount);
		query.setParam(new Object[] { roleId });
		return querySimplePage(query);
	}

	/* (non-Javadoc)
	 * @see com.legendshop.permission.dao.impl.FunctionDao#savePermissions(java.util.List)
	 */
	@FunctionUpdate
	public void savePermissions(final List<Permission> permissions) throws Exception {
		if(AppUtils.isBlank(permissions)){
			return;
		}
		List<Object[]> batchArgs = new ArrayList<Object[]>();
		for (Permission permission : permissions) {
			Object[] param = new Object[2];
			param[0] = permission.getId().getRoleId();
			param[1] = permission.getId().getFunctionId();
			batchArgs.add(param);
		}
		getJdbcTemplate().batchUpdate("insert into ls_perm (role_id,function_id) values (?,?)", batchArgs);
	}

	@FunctionUpdate
	public PerssionId savePermission(Permission permission) {
		if(permission == null){
			return null;
		}
		 getJdbcTemplate().update("insert into ls_perm (role_id,function_id) values (?,?)", permission.getId().getRoleId(),permission.getId().getFunctionId());
		 return permission.getId();
	}

	/* (non-Javadoc)
	 * @see com.legendshop.permission.dao.impl.FunctionDao#DeletePermissionByFunctionId(java.util.List)
	 */
	@FunctionUpdate
	public void DeletePermissionByFunctionId(List<Permission> permissions) {
		logger.info("DeletePermissionByFunctionId with size " + permissions.size());
		//deleteAll(permissions);
		if(AppUtils.isBlank(permissions)){
			return;
		}
		List<Object[]> batchArgs = new ArrayList<Object[]>();
		for (Permission permission : permissions) {
			Object[] param = new Object[2];
			param[0] = permission.getId().getRoleId();
			param[1] = permission.getId().getFunctionId();
			batchArgs.add(param);
		}
		getJdbcTemplate().batchUpdate("delete from ls_perm where role_id=? and function_id = ?", batchArgs);
	}

	/* (non-Javadoc)
	 * @see com.legendshop.permission.dao.impl.FunctionDao#setFindOtherFunctionByRoleIdHQLCount(java.lang.String)
	 */
	public void setFindOtherFunctionByRoleIdHQLCount(String findOtherFunctionByRoleIdHQLCount) {
		this.findOtherFunctionByRoleIdHQLCount = findOtherFunctionByRoleIdHQLCount;
	}

	/* (non-Javadoc)
	 * @see com.legendshop.permission.dao.impl.FunctionDao#updateFunction(com.legendshop.model.entity.Function)
	 */
	@FunctionUpdate
	public void updateFunction(Function function) {
		update(function);
	}

	/* (non-Javadoc)
	 * @see com.legendshop.permission.dao.impl.FunctionDao#deleteFunction(com.legendshop.model.entity.Function)
	 */
	@FunctionUpdate
	public void deleteFunction(Function function) {
		delete(function);
	}

	/* (non-Javadoc)
	 * @see com.legendshop.permission.dao.impl.FunctionDao#deleteFunction(java.lang.String)
	 */
	@FunctionUpdate
	public boolean deleteFunction(String functionId) {
		//删除角色和权限对应的关系
		update("delete from ls_perm where function_id = ?", functionId);
		return deleteById(functionId) > 0;
	}

	@FunctionUpdate
	public String saveFunction(Function function) {
		return save(function);
	}


	public PageSupport queryFunction(CriteriaQuery criteriaQuery) {
		return queryPage(criteriaQuery);
	}


	public List<Permission> queryPermission(String functionId) {
		return query("select p.role_id, p.function_id from ls_perm p where p.function_id=?", new Object[]{functionId}, new PermissionRowMapper());
	}

	/**
	 * 获取权限ById
	 */
	public Function getFunctionById(String id) {
		return getById(id);
	}


	/**
	 * 保存权限和菜单的关联关系
	 *
	 * @param funcId
	 * @param menuId
	 * @return
	 */
	@Override
	@FunctionUpdate
	public boolean saveFuncMenu(String funcId, String menuId) {
		int result = getJdbcTemplate().update("update ls_func set menu_id  = ? where id = ? ", menuId, funcId);
		return result > 0;
	}

	/**
	 * 获得所有权限
	 *
	 * @return 权限列表
	 */
	@Override
	public List<Function> getAllFunction() {
		return query("select m.menu_id,m.name,f.protect_function,f.note,f.url,f.id,m.parent_id as parentName from ls_func f,ls_menu m where m.menu_id = f.menu_id order by m.parent_id ", Function.class);
	}

	@Override
	public List<Function> getAllFunction(String appNo) {
		return query(" SELECT m.menu_id,m.name,f.protect_function,f.note,f.id,f.url,m.parent_id AS parentName FROM ls_func f LEFT JOIN ls_menu m ON m.menu_id = f.menu_id  WHERE f.app_no = ? ORDER BY m.parent_id ", Function.class, appNo);
	}


	/**
	 * 根据菜单ID获取相关联的资源
	 *
	 * @param menuId
	 * @return
	 */
	public List<Function> getFunctionsByMenuId(Long menuId) {
		return query(" select * from ls_func where menu_id = ? ",
				Function.class, menuId);
	}


	/**
	 * 根据用户ID查询用户拥有的操作权限信息
	 * @param userId
	 * @return
	 */
	public List<Function> getFunctionByUserId(String userId) {
		return query(getFunctionByUserIdSQL,Function.class, userId);
	}

	@Override
	public List<Function> getFunctionByUserId(String userId, String appNo) {
		return query(getFunctionByUserIdAppNoSQL, Function.class, appNo, userId);
	}


	/**
	 * 根据用户ID查询用户拥有的系统权限信息
	 * @param userId
	 * @return
	 */
	public List<Function> getSysFunctionByUserId(String userId) {
		return query(getSysFunctionByUserIdSQL, Function.class, userId);
	}

	/**
	 * 根据权限ID获取菜单ID列表
	 */
	@Override
	public List<Long> getMenuIdSByFuncId(String funcId) {
		return query(" select menu_id from ls_func where id  = ?", Long.class, funcId);
	}

	/**
	 * 根据权限查找用户列表
	 */
	@Override
	public PageSupport loadUserByFunction(String curPageNO, String userName, String funcId) {
   		SimpleSqlQuery query = new SimpleSqlQuery(UserDto.class,systemParameterUtil.getPageSize(), curPageNO);

   		String queryAllSQL = loadUserByFunctionAllSQL;
   		String querySQL = loadUserByFunctionSQL;

   		QueryMap map = new QueryMap();
   		map.put("funcId",funcId);
   		if(AppUtils.isNotBlank(userName)){
   			map.like("nickName", userName);
   			queryAllSQL = queryAllSQL + " WHERE t.nickName LIKE  ?";
   			querySQL = querySQL + " AND u.real_name LIKE  ?";
   		}
   		 query.setAllCountString(queryAllSQL);
   		 query.setQueryString(querySQL);
   		 query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport loadUserByRole(String curPageNO, String userName, String roleId) {
   		SimpleSqlQuery query = new SimpleSqlQuery(UserDto.class,systemParameterUtil.getPageSize(), curPageNO);

   		String queryAllSQL = loadUserByRoleAllSQL;
   		String querySQL = loadUserByRoleSQL;

   		QueryMap map = new QueryMap();
   		map.put("roleId",roleId);
   		if(AppUtils.isNotBlank(userName)){
   			map.like("nickName", userName);
   			queryAllSQL = queryAllSQL + " AND ud.nick_name LIKE  ?";
   			querySQL = querySQL + " AND ud.nick_name LIKE  ?";
   		}
   		 query.setAllCountString(queryAllSQL);
   		 query.setQueryString(querySQL);
   		 query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public PageSupport loadAdminByRole(String curPageNO, String userName, String roleId) {
		SimpleSqlQuery query = new SimpleSqlQuery(UserDto.class,systemParameterUtil.getPageSize(), curPageNO);

		String queryAllSQL = loadAdminByRoleAllSQL;
		String querySQL = loadAdminByRoleSQL;

		QueryMap map = new QueryMap();
		map.put("roleId",roleId);
		if(AppUtils.isNotBlank(userName)){
			map.like("nickName", userName);
			queryAllSQL = queryAllSQL + " AND du.real_name LIKE  ?";
			querySQL = querySQL + " AND du.real_name LIKE  ?";
		}
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

	@Override
	public List<Permission> getPermissionByRoleId(final String roleId) {
		return query("SELECT * FROM ls_perm WHERE role_id = ?", new Object[]{ roleId }, new RowMapper<Permission>() {
			@Override
			public Permission mapRow(ResultSet rs, int rowNum) throws SQLException {
				Permission p = new Permission();
				PerssionId id = new PerssionId();
				id.setFunctionId(rs.getString("function_id"));
				id.setRoleId(roleId);
				p.setId(id);
				return p;
			}
		});
	}

	@Override
	public List<Function> getSysFunctionByUserId(String userId, String appNo) {
		return query(
				"SELECT * FROM ls_func f WHERE f.category=1  AND f.app_no =? AND f.id IN( SELECT p.function_id FROM ls_perm p INNER JOIN ls_role r ON p.role_id = r.id INNER JOIN ls_usr_role ur ON ur.role_id = r.id WHERE r.app_no =  ? AND ur.user_id= ? )",
				Function.class, appNo, appNo, userId);
	}

	@Override
	public List<Function> getSysFunctionByAppNo(String appNo) {
		return query("SELECT * FROM ls_func f WHERE f.category=1  AND f.app_no =?",Function.class, appNo);
	}

	@Override
	public PageSupport<Function> queryFunction(String curPageNO, int pageSize, String name, Integer category,String appNo, Long menuId) {
		// Qbc查找方式
		CriteriaQuery cq = new CriteriaQuery(Function.class, curPageNO);
		cq.setPageSize(pageSize);
		if (!AppUtils.isBlank(name)) {
			cq.like("name", "%" + name.trim() + "%");// 1
		}
		if (!AppUtils.isBlank(category)) {
			cq.eq("category", category);
		}
		if (!AppUtils.isBlank(appNo)) {
			cq.eq("appNo", appNo);
		}
		if (!AppUtils.isBlank(menuId)) {
			cq.eq("menuId", menuId);
		}
		cq.addOrder("desc", "name");
		return queryPage(cq);
	}

	@Override
	public PageSupport<Function> findOtherFunctionByRoleId(String curPageNO, String roleId, int pageSize) {

		if (AppUtils.isBlank(curPageNO)) {
			curPageNO = "1";
		}

		SimpleSqlQuery query = new SimpleSqlQuery(Function.class, curPageNO);
		query.setPageSize(pageSize);
		query.setQueryString(findOtherFunctionByRoleIdHQL);
		query.setAllCountString(findOtherFunctionByRoleIdHQLCount);
		query.setParam(new Object[] { roleId });
		return querySimplePage(query);

	}



}
