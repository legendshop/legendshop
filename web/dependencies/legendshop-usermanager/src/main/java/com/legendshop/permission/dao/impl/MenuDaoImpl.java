/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.permission.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Menu;
import com.legendshop.permission.dao.MenuDao;
import com.legendshop.util.AppUtils;

/**
 * 菜单Dao.
 */
@Repository
public class MenuDaoImpl extends GenericDaoImpl<Menu, Long> implements MenuDao {

	public List<Menu> getMenu(String userName) {
		return this.queryByProperties(new EntityCriterion().eq("userName", userName));
	}
	
	/**
	 * 是否有子菜单
	 */
	public boolean hasSubMenu(Long menuId) {
		return  getLongResult("select count(*) from ls_menu where parent_id = ?", menuId) > 0;
	}


	public Menu getMenu(Long id) {
		return getById(id);
	}

	public void deleteMenu(Menu menu) {
		delete(menu);
	}

	public Long saveMenu(Menu menu) {
		return save(menu);
	}

	public void updateMenu(Menu menu) {
		Menu origin = getMenu(menu.getMenuId());
		origin.setAction(menu.getAction());
		origin.setLabel(menu.getLabel());
		origin.setName(menu.getName());
		origin.setProvidedPlugin(menu.getProvidedPlugin());
		origin.setSeq(menu.getSeq());
		origin.setTitle(menu.getTitle());
		origin.setParentId(menu.getParentId());
		menu.setGrade(origin.getGrade());
		update(origin);
	}

	@Override
	public PageSupport<Menu> getMenu(int pageSize,String curPageNO, int grade, Menu menu, DataSortResult result) {
        CriteriaQuery cq = new CriteriaQuery(Menu.class, curPageNO);
        cq.setPageSize(pageSize);
        cq.eq("grade", grade);//只是查询一级菜单
        cq.eq("name", menu.getName());
    	if (!result.isSortExternal()) {// 非外部排序
    		cq.addOrder("asc", "seq");
		}else {
			cq.addOrder(result.getOrderIndicator(), result.getSortName());
		}
		return queryPage(cq);
	}
	
	
	public PageSupport<Menu> getMenu(String curPageNO,int pageSize,Long parentId,DataSortResult result) {
		CriteriaQuery cq = new CriteriaQuery(Menu.class, curPageNO);
        cq.setPageSize(pageSize);
        cq.eq("parentId", parentId);
		if (!result.isSortExternal()) {// 非外部排序
			cq.addOrder("asc", "seq");
		}
		return queryPage(cq);
	}

	
	@Override
	public PageSupport<Menu> getMenu(String curPageNO, int pageSize, Long parentId, DataSortResult result, Menu menu) {
		CriteriaQuery cq = new CriteriaQuery(Menu.class, curPageNO);
		String name = menu.getName();
        cq.setPageSize(pageSize);
        if(AppUtils.isNotBlank(name)){
        	cq.like("name","%" + name.trim() + "%");
        }
        if(AppUtils.isBlank(parentId)||parentId.equals(0L)){
        	cq.isNull("parentId");
        }else{
        	 cq.eq("parentId", parentId);
        }
		if (!result.isSortExternal()) {// 非外部排序
			cq.addOrder("asc", "seq");
		}
		return queryPage(cq);
	}

}
