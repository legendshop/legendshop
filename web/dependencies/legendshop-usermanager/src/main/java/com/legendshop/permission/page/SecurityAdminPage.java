/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.permission.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * The Enum SecurityAdminPage.
 */
public enum SecurityAdminPage implements PageDefinition {
	/** The VARIABLE. 可变路径 */
	VARIABLE(""),
	
	/**用户管理*/
	USER_LIST_PAGE("/member/user/userlist"), 
	
	/**修改用户密码*/
	UPDATE_USER_PASSWORD("/member/user/updateUserPassword"), 
	
	/**用户角色*/
	FIND_ROLE_BY_USER_PAGE("/member/user/findRoleByUser"), 
	
	/**用户权限*/
	FIND_FUNCTION_BY_USER("/member/user/findFunctionByUser"), 
	
	/**用户添加角色*/
	FIND_OTHER_ROLE_BY_USER("/member/user/findOtherRoleByUser"),
	
	/**发送信息*/
	SENDMESSAGE("/member/user/sendMessage"),
	
	
	
	UPDATE_FUNCTION("/member/right/updateFunction"),

	ROLE_FUNCTION("/member/right/findFunctionByRole"),

	FIND_OTHER_FUNCTION_LIST("/member/right/findOtherfunctionList"),

	FUNCTION_LIST("/member/right/functionList"),

	ROLE_LIST("/member/right/roleList"),

	FIND_ROLE_BY_FUNCTION("/member/right/findRoleByFunction"),

	UPDATE_USER_STATUS("/member/user/updateUserStatus"),

	MODIFY_USER("/member/user/saveUser"),

	SAVE_ROLE("/member/right/saveRole"),
	
	MENU_LIST_PAGE("/menu/menuList"), 
	
	MENU_EDIT_PAGE("/menu/menu"),
	
	MENU_APPENDMENUROLE_PAGE("/menu/appendMenuRole"),

	/** 权限选择菜单 **/
	MENU_LOADFUNCMENU_PAGE("/menu/loadFuncMenu"),
	
	/** 拥有这个权限的用户 **/
	FUNCTION_USER_LIST_PAGE("/member/right/functionUserList"), 
	
	/** 拥有这个角色的用户 **/
	ROLE_USER_LIST_PAGE("/member/right/roleUserList"), 
	
	FUNCTION_LIST_CONTENT("/member/right/functionListContent"),
		
	/** 部门管理 **/
	DEPARTMENT_LIST_PAGE("/dept/departmentList"),

	;

	/** The value. */
	private final String value;

	private SecurityAdminPage(String value) {
		this.value = value;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("usermanager", path);
	}

	public String getNativeValue() {
		return value;
	}

}
