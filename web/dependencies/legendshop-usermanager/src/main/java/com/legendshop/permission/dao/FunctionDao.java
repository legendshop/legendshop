/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.permission.dao;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.framework.commond.JCFException;
import com.legendshop.model.entity.Function;
import com.legendshop.model.entity.Permission;
import com.legendshop.model.entity.PerssionId;

import java.util.List;

/**
 * The Interface FunctionDao.
 */
public interface FunctionDao extends Dao<Function, String>{

	/**
	 * Find function by role id.
	 * 
	 * @param roleId
	 *            the role id
	 * @return the list
	 */
	public abstract List<Function> FindFunctionByRoleId(String roleId);

	/**
	 * Find other function by role id.
	 * 
	 * @param roleId
	 *            the role id
	 * @return the list
	 */
	public abstract List<Function> FindOtherFunctionByRoleId(String roleId);

	/**
	 * Find other function by role id.
	 * 
	 * @param hqlQuery
	 *            the hql query
	 * @param roleId
	 *            the role id
	 * @return the page support
	 * @throws JCFException
	 *             the jCF exception
	 */
	public abstract PageSupport FindOtherFunctionByRoleId(final SimpleSqlQuery hqlQuery, String roleId) throws JCFException;

	/**
	 * Save permissions.
	 * 
	 * @param permissions
	 *            the permissions
	 * @throws Exception
	 *             the exception
	 */
	public abstract void savePermissions(final List<Permission> permissions) throws Exception;

	/**
	 * Delete permission by function id.
	 * 
	 * @param permissions
	 *            the permissions
	 */
	public abstract void DeletePermissionByFunctionId(List<Permission> permissions);


	/**
	 * Update function.
	 *
	 * @param function the function
	 */
	public abstract void updateFunction(Function function);

	/**
	 * Delete function.
	 *
	 * @param function the function
	 */
	public abstract void deleteFunction(Function function);

	/**
	 * Delete function.
	 *
	 * @param functionId the function id
	 * @return true, if successful
	 */
	public abstract boolean deleteFunction(String functionId);

	/**
	 * Save function.
	 *
	 * @param function the function
	 * @return the string
	 */
	public abstract String saveFunction(Function function);

	/**
	 * Save permission.
	 *
	 * @param permission the permission
	 * @return the perssion id
	 */
	public abstract PerssionId savePermission(Permission permission);

	/**
	 * Query function.
	 *
	 * @param criteriaQuery the criteria query
	 * @return the page support
	 */
	public abstract PageSupport queryFunction(CriteriaQuery criteriaQuery);

	/**
	 * Query permission.
	 *
	 * @param functionId the function id
	 * @return the list
	 */
	public abstract List<Permission> queryPermission(String functionId);
	
	/**
	 * 根据用户ID查询用户拥有的权限信息.
	 *
	 * @param userId the user id
	 * @return the sys function by user id
	 */
	public List<Function> getSysFunctionByUserId(String userId);

	/**
	 * Gets the function by id.
	 *
	 * @param id the id
	 * @return the function by id
	 */
	public abstract Function getFunctionById(String id);
	
	/**
	 * Gets the menu id s by func id.
	 *
	 * @param funcId ： 权限ID
	 * @return the menu id s by func id
	 */
	public List<Long> getMenuIdSByFuncId(String funcId);
	
	/**
	 * 保存权限和菜单的关联关系.
	 *
	 * @param funcId the func id
	 * @param menuId the menu id
	 * @return true, if successful
	 */
	public boolean saveFuncMenu(String funcId,String menuId);
	
	/**
	 * 获得所有权限.
	 *
	 * @return the all function
	 */
	public List<Function> getAllFunction();
	
	/**
	 * 根据菜单ID获取相关联的资源.
	 *
	 * @param menuId the menu id
	 * @return the functions by menu id
	 */
	public List<Function> getFunctionsByMenuId(Long menuId);
	
	/**
	 * 根据URL地质获取权限.
	 *
	 * @param url the url
	 * @return the functions by url
	 */
	public List<Function> getFunctionsByUrl(String url);
	
	/**
	 * 根据用户ID查询用户拥有的权限信息.
	 *
	 * @param userId the user id
	 * @return the function by user id
	 */
	public List<Function> getFunctionByUserId(String userId);


	public  PageSupport loadUserByFunction(String curPageNO, String userName, String funcId);

	/**
	 * 根据角色ID查找
	 * @param roleId
	 * @return
	 */
	public  List<Permission> getPermissionByRoleId(String roleId);

	public abstract PageSupport loadUserByRole(String curPageNO, String userName, String roleId);

	public abstract PageSupport loadAdminByRole(String curPageNO, String userName, String roleId);

	public abstract List<Function> getAllFunction(String appNo);

	public abstract List<Function> getFunctionByUserId(String userId, String appNo);

	public abstract List<Function> getSysFunctionByUserId(String userId, String appNo);

	public abstract List<Function> getSysFunctionByAppNo(String appNo);

	public abstract PageSupport<Function> queryFunction(String curPageNO, int pageSize, String name, Integer category,String appNo, Long menuId);

	public abstract PageSupport<Function> findOtherFunctionByRoleId(String curPageNO, String roleId, int pageSize);
	


}