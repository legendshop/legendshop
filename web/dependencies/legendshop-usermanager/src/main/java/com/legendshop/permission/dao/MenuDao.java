/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.permission.dao;
 
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Menu;

/**
 * 菜单Dao
 */

public interface MenuDao extends Dao<Menu, Long> {
     
    /**
     * Gets the menu.
     *
     * @param shopName the shop name
     * @return the menu
     */
    public abstract List<Menu> getMenu(String shopName);

	/**
	 * Gets the menu.
	 *
	 * @param id the id
	 * @return the menu
	 */
	public abstract Menu getMenu(Long id);
	
    /**
     * Delete menu.
     *
     * @param menu the menu
     */
    public abstract void deleteMenu(Menu menu);
	
	/**
	 * Save menu.
	 *
	 * @param menu the menu
	 * @return the long
	 */
	public abstract Long saveMenu(Menu menu);
	
	/**
	 * Update menu.
	 *
	 * @param menu the menu
	 */
	public abstract void updateMenu(Menu menu);
	

	/**
	 * Checks for sub menu.
	 *
	 * @param menuId the menu id
	 * @return true, if successful
	 */
	public abstract boolean hasSubMenu(Long menuId);

	public abstract PageSupport<Menu> getMenu(int pageSize,String curPageNO, int grade, Menu menu, DataSortResult result);

	public abstract PageSupport<Menu> getMenu(String curPageNO, int pageSize, Long parentId, DataSortResult result);

	//
	public abstract PageSupport<Menu> getMenu(String curPageNO, int pageSize, Long parentId, DataSortResult result, Menu menu);
	
 }
