package com.legendshop.permission.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.UserRoleDao;
import com.legendshop.model.entity.UserRole;
import com.legendshop.permission.service.UserRoleService;

/**
 * 
 * 用户角色服务
 *
 */
@Service("userRoleService")
public class UserRoleServiceImpl implements UserRoleService {
	
	@Autowired
	private UserRoleDao userRoleDao;

	@Override
	public void deleteUserRole(List<UserRole> userRoles) {
		userRoleDao.deleteUserRole(userRoles);
	}

	@Override
	public void saveUserRoleList(List<UserRole> userRoles) {
		userRoleDao.saveUserRoleList(userRoles);
	}

	@Override
	public void deleteByUserIdAndRoleId(String userId, String roldId) {
		userRoleDao.deleteByUserIdAndRoleId(userId, roldId);
	}

	@Override
	public Integer isExistUserRole(String userId, String roldId) {
		return userRoleDao.isExistUserRole(userId, roldId);
	}

}
