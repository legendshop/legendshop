/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.permission.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.UserDto;
import com.legendshop.model.entity.Function;
import com.legendshop.model.entity.Permission;

import java.util.List;

/**
 * 权限服务类.
 *
 */
public interface FunctionService {
	
	/**
	 * 根据用户ID查询用户拥有的权限信息.
	 *
	 * @param userId the user id
	 * @return the function by user id
	 */
	public List<Function> getFunctionByUserId(String userId);
	
	/**
	 * 保存权限和菜单的关联关系.
	 *
	 * @param funcId the func id
	 * @param menuId the menu id
	 * @return true, if successful
	 */
	public boolean saveFuncMenu(String funcId,String menuId);

	/**
	 * Gets the menu id s by func id.
	 *
	 * @param funcId ： 权限ID
	 * @return the menu id s by func id
	 */
	public List<Long> getMenuIdSByFuncId(String funcId);
	

	
	/**
	 * 获得所有权限.
	 *
	 * @return the all function
	 */
	public List<Function> getAllFunction();
	
	/**
	 * 根据菜单ID获取相关联的资源.
	 *
	 * @param menuId the menu id
	 * @return the functions by menu id
	 */
	public List<Function> getFunctionsByMenuId(Long menuId);
	

	
	/**
	 * 根据用户ID查询用户拥有的系统权限信息.
	 *
	 * @param userId the user id
	 * @return the sys function by user id
	 */
	public List<Function> getSysFunctionByUserId(String userId);
	
	/**
	 * 根据URL获取权限.
	 *
	 * @param url the url
	 * @return the functions by url
	 */
	public List<Function> getFunctionsByUrl(String url);
	
	/**
	 * 根据ID获取权限.
	 *
	 * @param url the url
	 * @return the functions by url
	 */
	public Function getFunctionsById(String funcId);

	/**
	 * 根据权限加载所属用户 分页
	 * @param curPageNO
	 * @param userName
	 * @param funcId
	 * @return
	 */
	public PageSupport<UserDto> loadUserByFunction(String curPageNO, String userName, String funcId);
	
	/**
	 * 根据角色加载所属用户 分页
	 * @param curPageNO
	 * @param userName
	 * @param funcId
	 * @return
	 */
	public PageSupport<UserDto> loadUserByRole(String curPageNO, String userName, String roleId);

	/**
	 * 根据角色加载所属管理员 分页
	 * @param curPageNO
	 * @param userName
	 * @param funcId
	 * @return
	 */
	public PageSupport<UserDto> loadAdminByRole(String curPageNO, String userName, String roleId);

	/**
	 * 根据角色ID查找
	 * @param roleId
	 * @return
	 */
	public List<Permission> getPermissionByRoleId(String roleId);

	/**
	 * 获取某个系统所有的权限
	 * @param appNo
	 * @return
	 */
	public List<Function> getAllFunction(String appNo);

	public List<Function> getFunctionByUserId(String userId, String appNo);

	public List<Function> getSysFunctionByUserId(String userId, String appNo);

	public List<Function> getSysFunctionByAppNo(String appNo);
	
	/**
	 * deleteFunctionById
	 */
	public boolean deleteFunctionById(String fundtionId);
	
	/**
	 * FindFunctionByRoleId
	 */
	public List<Function> findFunctionByRoleId(String roleId);

	public String saveFunction(Function function);

	public void saveFunctionsToRole(List<Permission> permissions) throws Exception;

	public void updateFunction(Function function);

	public void deleteFunctionsFromRole(List<Permission> permissions);

	public PageSupport<Function> findAllFunction(String curPageNO, int pageSize, String name, Integer category,String appNo, Long menuId);

	public PageSupport<Function> findOtherFunctionByHql(String curPageNO, String roleId, int pageSize);

}
