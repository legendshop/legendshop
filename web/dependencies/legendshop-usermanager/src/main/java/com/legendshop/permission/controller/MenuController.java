/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.permission.controller;

import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.helper.ResourceBundleHelper;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.CommonServiceWebUtil;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.constant.DataSortResult;
import com.legendshop.model.entity.Menu;
import com.legendshop.permission.page.SecurityAdminPage;
import com.legendshop.permission.page.SecurityRedirectPage;
import com.legendshop.permission.service.MenuService;
import com.legendshop.util.AppUtils;

/**
 * 资源管理
 *
 */
@Controller
@RequestMapping("/system/menu")
public class MenuController extends BaseController {
	
    @Autowired
    private MenuService menuService;
    
	@Resource(name="updateMenuProcessor")
	private EventProcessor<Object> updateMenuProcessor;

	@RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Menu menu) {
		int grade = 1;//只是查询一级菜单
		int pageSize = 30;//每页大小
		
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[]{"seq"});
		
		PageSupport<Menu> ps = menuService.getMenu(pageSize,curPageNO, grade, menu,result);
		PageSupportHelper.savePage(request, ps);
		
        request.setAttribute("menu", menu);
        request.setAttribute("menuLevel", 1);
        request.setAttribute("parentId", 0);
        String path = PathResolver.getPath(SecurityAdminPage.MENU_LIST_PAGE);
        return path;
    }
    
	@RequestMapping("/queryChildMenu/{parentId}")
    public String queryChildMenu(HttpServletRequest request, HttpServletResponse response, String curPageNO, @PathVariable Long parentId, Menu menu) {
    	Menu parentMenu= menuService.getMenu(parentId);
    	request.setAttribute("parentMenu", parentMenu);
		int pageSize = 30;
		DataSortResult result = CommonServiceWebUtil.isDataSortByExternal(request, new String[]{"seq"});
		PageSupport<Menu> ps = menuService.getMenu(curPageNO,pageSize,parentId,result,menu);
		PageSupportHelper.savePage(request, ps);
		Integer menuLevel=1;
		if(AppUtils.isNotBlank(parentMenu)){
			menuLevel= parentMenu.getGrade() + 1;
		}
        request.setAttribute("menuLevel",menuLevel);//得到下一级菜单
        List<KeyValueEntity> allParentMenu = menuService.getParentMenu(parentId);
        request.setAttribute("parentId",parentId);
        request.setAttribute("allParentMenu",allParentMenu);
        return PathResolver.getPath(SecurityAdminPage.MENU_LIST_PAGE);
    }
    
	/**
	 * 保存菜单
	 * @param request
	 * @param response
	 * @param menu
	 * @return
	 */
	@SystemControllerLog(description="保存菜单")
    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, Menu menu) {
        menuService.saveMenu(menu);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		//发布更新菜单指令
        updateMenuProcessor.process(null);
        return fowardToMenuList(request,response, menu);
    }
    
    /**
     * 根据目前的menu导航到不同level的菜单管理页面
     * @param request
     * @param response
     * @param menu
     * @return
     */
   private String fowardToMenuList(HttpServletRequest request, HttpServletResponse response,Menu menu){
       if(	1 == menu.getGrade()){//一级菜单
       	return PathResolver.getPath(SecurityRedirectPage.MENU_LIST_QUERY);
       }else{//二级和三级菜单
       	Menu parentMenu= menuService.getMenu(menu.getParentId());
       	Long parentId = parentMenu.getMenuId();
       	return PathResolver.getPath(SecurityRedirectPage.CHILD_MENU_LIST) + parentId;
       }
   }
   
   /**
    * 删除菜单
    * @param request
    * @param response
    * @param menuId
    * @return
    */
    @SystemControllerLog(description="删除菜单")
    @RequestMapping(value = "/delete/{menuId}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long menuId) {
        Menu menu = menuService.getMenu(menuId);
    	if(!menuService.hasSubMenu(menuId)){
    		menuService.deleteMenu(menu);
    		 saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
    	}else{
    		saveError(request, ResourceBundleHelper.getString("","请先删除子菜单，再删除父菜单"));
    	}
    	
		//发布更新菜单指令
    	updateMenuProcessor.process(null);
        return fowardToMenuList(request,response, menu);
    }

    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable
    Long id) {
        Menu menu = menuService.getMenu(id);
        request.setAttribute("menu", menu);
        if(menu != null && menu.getParentId() != null){
        	Menu parentMenu= menuService.getMenu(menu.getParentId());
        	request.setAttribute("parentMenu", parentMenu);
        }
         return PathResolver.getPath(SecurityAdminPage.MENU_EDIT_PAGE);
    }
    
	@RequestMapping(value = "/create/{parentId}")
	public String create(HttpServletRequest request, HttpServletResponse response,@PathVariable Long parentId) {
		request.setAttribute("parentMenu", menuService.getMenu(parentId));
        if(parentId != null){
            List<KeyValueEntity> allParentMenu = menuService.getParentMenu(parentId);
            request.setAttribute("allParentMenu",allParentMenu);
        }
		return PathResolver.getPath(SecurityAdminPage.MENU_EDIT_PAGE);
	}

    @RequestMapping(value = "/update")
    public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
        Menu menu = menuService.getMenu(id);
		request.setAttribute("menu", menu);
		return PathResolver.getPath(SecurityAdminPage.MENU_EDIT_PAGE);
    }
    
	@RequestMapping(value = "/appendMenuRole/{id}")
	public String appendMenuRole(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		request.setAttribute("menuId", id);
		return PathResolver.getPath(SecurityAdminPage.MENU_APPENDMENUROLE_PAGE);
	}
	
}
