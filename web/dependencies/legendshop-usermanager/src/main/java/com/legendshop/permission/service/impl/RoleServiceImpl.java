package com.legendshop.permission.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Function;
import com.legendshop.model.entity.Role;
import com.legendshop.permission.dao.FunctionDao;
import com.legendshop.permission.dao.RoleDao;
import com.legendshop.permission.service.RoleService;
import com.legendshop.util.AppUtils;

/**
 * 角色服务
 *
 */
@Service("roleService")
public class RoleServiceImpl  implements RoleService{
	
	@Autowired
	private RoleDao roleDao;
	
	@Autowired
	private FunctionDao functionDao;
	
	@Override
	public List<String> findRolesNameByUser(String userId, String appNo) {
		return roleDao.findRolesNameByUser(userId, appNo);
	}

	@Override
	public List<String> findFunctionsByUser(String userId, String appNo) {
		return roleDao.findFunctionsByUser(userId, appNo);
	}

	@Override
	public List<String> getPermByShopAccountRole(Long shopRoleId) {
		return roleDao.getPermByShopAccountRole(shopRoleId);
	}

	@Override
	public Role findRoleById(String roleId) {
		return roleDao.findRoleById(roleId);
	}

	@Override
	public void deleteRoleById(String roleId) {
		roleDao.deleteRoleById(roleId);
	}

	@Override
	public PageSupport<Role> findAllRole(String search, String myaction, String curPageNO) {
		
		return roleDao.findAllRole(search,myaction,curPageNO);
	}

	@Override
	public List<Role> findRoleByFunction(String functionId) {
		
		return roleDao.findRoleByFunction(functionId);
	}

	@Override
	public String saveRole(Role role) {
		return roleDao.saveRole(role);
	}

	@Override
	public void updateRole(Role role) {
		roleDao.updateRole(role);
	}

	@Override
	public PageSupport<Role> findAllRolePage(String curPageNO, int pageSize, String name, String enabled, String appNo,Integer category) {
		return roleDao.findAllRolePage( curPageNO,  pageSize,  name,  enabled,  appNo, category);
	}

	@Override
	public PageSupport<Role> findOtherRoleByUser(String curPageNO, String userId, String appNo) {
		return roleDao.FindOtherRoleByUser(curPageNO,userId,appNo);
	}

	@Override
	public List<Function> findFunctionByUser(String userId, String appNo) {
		List<Role> roles = roleDao.findRoleByUserId(userId,appNo);
		if (!AppUtils.isBlank(roles)) {
			return findFunctionByRole(roles);
		} else {
			return null;
		}
	}
	
	/**
	 * Find function by role.
	 * 
	 * @param roles
	 *            the roles
	 * @return the list
	 */
	public List<Function> findFunctionByRole(List roles) {
		Map functionMap = new HashMap();
		List<Function> functionList = new ArrayList();
		for (int i = 0; i < roles.size(); i++) {
			Role role = (Role) roles.get(i);
			if (!AppUtils.isBlank(role.getId())) {
				List<Function> fs = functionDao.FindFunctionByRoleId(role.getId());
				if (AppUtils.isNotBlank(fs))
					for (int j = 0; j < fs.size(); j++) {
						Function f = (Function) fs.get(j);
						if (!functionMap.containsKey(f.getId())) {
							functionMap.put(f.getId(), null);
							functionList.add(f);
						}

					}

			}

		}
		return functionList;
	}

	@Override
	public List<Role> findRolesByUser(String userId, String appNo) {
		return roleDao.findRoleByUserId(userId,appNo);
	}

	@Override
	public List<String> findRoleIdsByUser(String userId, String appNo) {
		return roleDao.findRoleIdsByUser(userId,appNo);
	}

}
