/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.permission.form;

import java.util.List;

import com.legendshop.model.entity.Function;

/**
 * 权限包装类.
 */
public class FunctionMenuDto {
	
	/** The menu id. */
	private Long menuId;

	/** The menu name. */
	private String menuName;
	
	/** The func list. */
	private List<Function> funcList;

	/**
	 * Gets the menu name.
	 *
	 * @return the menu name
	 */
	public String getMenuName() {
		return menuName;
	}

	/**
	 * Sets the menu name.
	 *
	 * @param menuName the new menu name
	 */
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	/**
	 * Gets the menu id.
	 *
	 * @return the menu id
	 */
	public Long getMenuId() {
		return menuId;
	}

	/**
	 * Sets the menu id.
	 *
	 * @param menuId the new menu id
	 */
	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}

	/**
	 * Gets the func list.
	 *
	 * @return the func list
	 */
	public List<Function> getFuncList() {
		return funcList;
	}

	/**
	 * Sets the func list.
	 *
	 * @param funcList the new func list
	 */
	public void setFuncList(List<Function> funcList) {
		this.funcList = funcList;
	}
	
	
}
