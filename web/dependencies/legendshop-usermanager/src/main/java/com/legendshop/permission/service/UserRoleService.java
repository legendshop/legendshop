package com.legendshop.permission.service;

import java.util.List;

import com.legendshop.model.entity.UserRole;

/**
 *  用户角色服务
 */
public interface UserRoleService {

	public abstract void deleteUserRole(List<UserRole> userRoles);

	public abstract void saveUserRoleList(List<UserRole> userRoles);
	
	/**
	 * 根据userId和roleId删除用户角色
	 * @param userId
	 * @param roldId
	 */
	public abstract void deleteByUserIdAndRoleId(String userId, String roldId);
	
	/**
	 * 判断用户是否拥有此角色
	 * @param userId
	 * @param roldId
	 * @return
	 */
	public abstract Integer isExistUserRole(String userId, String roldId);
	
}
