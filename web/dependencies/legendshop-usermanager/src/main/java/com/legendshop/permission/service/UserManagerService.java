package com.legendshop.permission.service;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.User;

public interface UserManagerService {

	public abstract  String saveUser(User user);

	public abstract  User getUser(String userId);

	public abstract void updateUser(User user);
	
	public void updateUserPassword(User user);

	public abstract PageSupport<User> queryUser(String curPageNO, String name, String enabled, Integer pageSize);
	
	public abstract PageSupport<User> queryUser(String curPageNO, String name, String enabled, String mobile, Integer pageSize);

}
