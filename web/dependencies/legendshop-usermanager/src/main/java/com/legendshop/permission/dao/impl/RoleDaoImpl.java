package com.legendshop.permission.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.GenericJdbcDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.framework.commond.JCFException;
import com.legendshop.model.constant.ApplicationEnum;
import com.legendshop.model.entity.Role;
import com.legendshop.permission.dao.RoleDao;
import com.legendshop.util.AppUtils;

/**
 * 角色Dao
 *
 */
@Repository
public class RoleDaoImpl extends GenericDaoImpl<Role, String> implements RoleDao {

	/** The generic jdbc dao. */
	@Autowired
	private GenericJdbcDao genericJdbcDao;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	private String sqlForFindRolesByUser = "SELECT DISTINCT r.role_type as name FROM ls_usr_role ur ,ls_role r WHERE r.enabled ='1'  AND ur.role_id=r.id AND r.app_no =? AND ur.user_id= ? ";

    private String sqlForFindFunctionsByUser = "select DISTINCT f.protect_function as name from ls_usr_role ur ,ls_role r,ls_perm p, ls_func f where r.enabled = '1'  and ur.role_id=r.id and r.id=p.role_id and p.function_id=f.id and r.app_no = ? and f.app_no = ? and ur.user_id= ?";

    private static String sqlQueryPermByShopAccountRole = "SELECT label FROM ls_shop_perm where role_id = ?";
    
	/** The find other role by user. */
	private String findOtherRoleByUser = "select id,name,role_type as roleType, enabled,note from ls_role where app_no = ? and id not in (select r.id from ls_role r,ls_usr_role ur, #USER_TABLE# u where r.id=ur.role_id and u.id=ur.user_id and u.id = ?  and r.app_no = ?)";

	/** The find other role by user count. */
	private String findOtherRoleByUserCount = "select count(*) from ls_role where  app_no = ? and id not in (select r.id from ls_role r,ls_usr_role ur, #USER_TABLE# u where r.id=ur.role_id and u.id=ur.user_id and u.id =  ? and r.app_no = ?)";
	
	/** The find role by user id. */
	private String findRoleByUserId = "select r.id,r.name,r.role_type as roleType, r.enabled, r.note from ls_role r,ls_usr_role ur,#USER_TABLE# u where r.id=ur.role_id and u.id=ur.user_id and u.id = ? and r.app_no = ?";
	
	@Override
	public List<String> findRolesNameByUser(String userId, String appNo) {
		return genericJdbcDao.query(sqlForFindRolesByUser, new Object[] { appNo, userId }, new RowMapper<String>() {
			public String mapRow(ResultSet rs, int index) throws SQLException {
				return new String(rs.getString("name"));
			}
		});
	}
	
	@Override
	public List<String> findFunctionsByUser(String userId, String appNo) {
		return genericJdbcDao.query(sqlForFindFunctionsByUser, new Object[]{appNo, appNo, userId}, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int index) throws SQLException {
                return new String(rs.getString("name"));
            }
        });
	}
	

	public void setGenericJdbcDao(GenericJdbcDao genericJdbcDao) {
		this.genericJdbcDao = genericJdbcDao;
	}

	@Override
	public List<String> getPermByShopAccountRole(Long shopRoleId) {
		return genericJdbcDao.query(sqlQueryPermByShopAccountRole, String.class, shopRoleId);
	}

	@Override
	public Role findRoleById(String roleId) {
		return getByProperties(new EntityCriterion().eq("id", roleId));
	}

	@Override
	public void deleteRoleById(String roleId) {
		this.deleteById(roleId);
		// 删除角色和权限对应的关系
		update("delete from ls_perm where role_id = ?", roleId);
	}

	@Override
	public PageSupport<Role> findAllRole(String search, String myaction, String curPageNO) {
		if (AppUtils.isBlank(curPageNO)) {
			curPageNO = "1";
		}
		CriteriaQuery cq = new CriteriaQuery(Role.class, curPageNO);
		cq.setPageSize(systemParameterUtil.getPageSize());
		if (!AppUtils.isBlank(search)) {
			cq.like("name", "%" + search + "%");
		}
		return queryPage(cq);
	}

	@Override
	public List<Role> findRoleByFunction(String functionId) {
		String sql = "select r.id,r.name,r.role_type as roleType, r.enabled, r.note from ls_role r,ls_perm p,ls_func f where r.id=p.role_id and f.id=p.function_id and f.id = ?";
		return query(sql, Role.class, functionId);
	}

	@Override
	public String saveRole(Role role) {
		return save(role);
	}

	@Override
	public void updateRole(Role role) {
		update(role);
	}

	@Override
	public PageSupport<Role> findAllRolePage(String curPageNO, int pageSize, String name, String enabled, String appNo,Integer category) {
		// Qbc查找方式
		CriteriaQuery cq = new CriteriaQuery(Role.class, curPageNO);
		cq.setPageSize(15);
		
		if (!AppUtils.isBlank(name)) {
			cq.like("name","%" + name.trim() + "%");
		}
		if (AppUtils.isNotBlank(enabled)) {
			cq.eq("enabled", enabled);
		}
		if (AppUtils.isNotBlank(appNo)) {
			cq.eq("appNo", appNo);
		}
		if (AppUtils.isNotBlank(category)) {
			cq.eq("category", category);
		}
		
		cq.addDescOrder("category");
		
		return queryPage(cq);
	}
	
	
	@Override
	public PageSupport<Role> FindOtherRoleByUser(String curPageNO, String userId, String appNo) {
		// HQL查找方式
		SimpleSqlQuery query = new SimpleSqlQuery(Role.class);
		query.setCurPage(curPageNO);

		//不作分页优化，现在分页有问题,且角色数量不会很多，分页不好使，默认读1000条记录，TODO： 取消分页
		query.setPageSize(1000);

		String userTable = getUserTableByAppNo(appNo);

		String sqlForFindOtherRoleByUser = findOtherRoleByUser.replace("#USER_TABLE#", userTable);
		String sqlForFindOtherRoleByUserCount = findOtherRoleByUserCount.replace("#USER_TABLE#", userTable);

		query.setQueryString(sqlForFindOtherRoleByUser);
		query.setAllCountString(sqlForFindOtherRoleByUserCount);
		query.setParam(new Object[] { appNo, userId, appNo });

		return querySimplePage(query);
	}
	
	
	/**
	 * 根据前后台动态切换表.
	 *
	 * @param userId the user id
	 * @param appNo the app no
	 * @return the list< role>
	 * @throws JCFException the JCF exception
	 */
	public List<Role> findRoleByUserId(String userId, String appNo) {
		String userTable = getUserTableByAppNo(appNo);
		String sql = findRoleByUserId.replace("#USER_TABLE#", userTable);
		return query(sql, Role.class, userId, appNo);
	}
	
	
	/**
	 * 根据类型得出表结构.
	 *
	 * @param appNo the app no
	 * @return the user table by app no
	 */
	private String getUserTableByAppNo(String appNo) {
		String userTable = null;
		if (ApplicationEnum.BACK_END.value().equals(appNo)) {// 后台管理员
			userTable = "ls_admin_user";
		} else if (ApplicationEnum.FRONT_END.value().equals(appNo)) {// 前台用户
			userTable = "ls_user";
		}
		return userTable;
	}

	
	@Override
	public List<String> findRoleIdsByUser(String userId, String appNo) {
		return genericJdbcDao.query(sqlForFindRolesByUser, new Object[] { appNo, userId }, new RowMapper<String>() {
			public String mapRow(ResultSet rs, int index) throws SQLException {
				return new String(rs.getString("name"));
			}
		});
	}

}
