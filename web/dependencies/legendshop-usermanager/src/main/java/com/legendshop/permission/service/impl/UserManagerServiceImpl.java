package com.legendshop.permission.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.UserManagerDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.User;
import com.legendshop.permission.service.UserManagerService;

@Service("userManagerService")
public class UserManagerServiceImpl  implements UserManagerService{
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UserManagerDao userManagerDao;

	@Override
	public String saveUser(User user) {
		if (user.getPassword() != null) {
			user.setPassword(passwordEncoder.encode(user.getPassword()));
		}
		return userManagerDao.saveUser(user);
	}

	@Override
	public User getUser(String userId) {
		return userManagerDao.getUser(userId);
	}

	@Override
	public void updateUser(User user) {
		userManagerDao.updateUser(user);
	}

	@Override
	public void updateUserPassword(User user) {
		userManagerDao.updateUserPassword(user);
	}

	@Override
	public PageSupport<User> queryUser(String curPageNO, String name, String enabled,Integer pageSize) {
		return userManagerDao.queryUser(curPageNO, name, enabled,pageSize);
	}

	@Override
	public PageSupport<User> queryUser(String curPageNO, String name, String enabled, String mobile, Integer pageSize) {
		return userManagerDao.queryUser(curPageNO, name, enabled, mobile, pageSize);
	}
	

}
