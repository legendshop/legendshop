/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.permission.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * The Enum SecurityRedirectPage.
 */
public enum SecurityRedirectPage implements PageDefinition {
	
	/** 用户权限管理列表 */
	USERS_LIST("/admin/member/user/query"),
	
	/** 用户信息管理列表 */
	USERINFO_LIST("/admin/system/userDetail/query"),
	
	/** 管理员列表 */
	ADMIN_USERS_LIST("/admin/adminUser/query"),
	
	/** 查找用户角色 */
	FIND_USER_ROLES("/admin/member/user/roles"),
	
	/** 角色管理列表 */
	ALL_ROLE("/admin/member/role/query"),
	
	/** 角色管理 增加权限 */
	FIND_FUNCTION_BY_ROLE("/admin/member/role/addFunctions"),
	
	/** 子菜单列表 */
	CHILD_MENU_LIST("/system/menu/queryChildMenu/"),
	
	/** 后台1级菜单定义 */
	MENU_LIST_QUERY("/system/menu/query"), 
	
	/** 部门管理列表 */
	DEPARTMENT_LIST_QUERY("/admin/department/query")
	
	;
	

	/** The value. */
	private final String value;

	private SecurityRedirectPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("redirect:", path);
	}

	/**
	 * Instantiates a new tiles page.
	 * 
	 * @param value
	 *            the value
	 */
	private SecurityRedirectPage(String value) {
		this.value = value;
	}

	public String getNativeValue() {
		return value;
	}

}
