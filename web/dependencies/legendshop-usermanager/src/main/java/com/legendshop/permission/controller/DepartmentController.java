/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.permission.controller;

import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.config.SystemParameterUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.Department;
import com.legendshop.permission.page.SecurityAdminPage;
import com.legendshop.permission.page.SecurityBackPage;
import com.legendshop.permission.page.SecurityRedirectPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.DepartmentService;
import com.legendshop.util.JSONUtil;

/**
 * 部门管理
 *
 */
@Controller
@RequestMapping("/admin/department")
public class DepartmentController extends BaseController{
    @Autowired
    private DepartmentService departmentService;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

    @RequestMapping("/query")
    public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Department department) {
	    List<Department> deptList = departmentService.getAllDepartment(); 
		String deptTree = JSONUtil.getJson(deptList);
		request.setAttribute("deptTree", deptTree);
		request.setAttribute("dept", new Department());

        return PathResolver.getPath(SecurityAdminPage.DEPARTMENT_LIST_PAGE);
    }
    
    @RequestMapping(value = "/query/{deptId}")
   	public String query(HttpServletRequest request, HttpServletResponse response, @PathVariable Long deptId) {
       	

       	Department dept = departmentService.getDepartment(deptId);
       	
       	request.setAttribute("dept", dept);

   		return PathResolver.getPath(SecurityBackPage.DEPARTMENT_EDIT_PAGE);
   	}
    
    /**
     *保存部门
     * @param request
     * @param response
     * @param department
     * @return
     */
    @SystemControllerLog(description="保存部门")
    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request, HttpServletResponse response, Department department) {
    	
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
    	department.setShopId(shopId);
    	
        departmentService.saveDepartment(department);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(SecurityRedirectPage.DEPARTMENT_LIST_QUERY);
    }
    
    /**
     * 删除部门
     * @param request
     * @param response
     * @param id
     * @return
     */
    @SystemControllerLog(description="删除部门")
    @RequestMapping(value = "/delete/{id}")
    public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
        Department department = departmentService.getDepartment(id);
		departmentService.deleteDepartment(department);
        saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
        return PathResolver.getPath(SecurityRedirectPage.DEPARTMENT_LIST_QUERY);
    }

    @RequestMapping(value = "/load/{id}")
    public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
        Department department = departmentService.getDepartment(id);
        request.setAttribute("department", department);
         return PathResolver.getPath(SecurityBackPage.DEPARTMENT_EDIT_PAGE);
    }
    
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		String path = PathResolver.getPath(SecurityBackPage.DEPARTMENT_EDIT_PAGE);
		return path;
	}
	
    @RequestMapping(value = "/update/{id}")
    public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {        
        Department department = departmentService.getDepartment(id);
		request.setAttribute("department", department);
		return PathResolver.getPath(SecurityRedirectPage.DEPARTMENT_LIST_QUERY);
    }
    
    /**
     * 更新部门
     * @param request
     * @param response
     * @param id
     * @param pid
     * @return
     */
    @SystemControllerLog(description="更新部门")
	@RequestMapping(value = "/updatePid")	
	public @ResponseBody String updatePid(HttpServletRequest request, HttpServletResponse response,  Long id, Long pid) {
	  	if( id == null | pid == null ) return "ID is null";
	  	Department department = departmentService.getDepartment(id);
	  	department.setParentId(pid);
	  	departmentService.updateDepartment(department);
	  	return Constants.SUCCESS;
	}
	
	/**
	 * 删除部门
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
    @SystemControllerLog(description="删除部门")
    @RequestMapping("/deleteDept")
    public @ResponseBody String deleteDept(HttpServletRequest request, HttpServletResponse response,  Long id) {
    	if( id == null ){
    		return "ID can not empty";
    	}
        Department department = departmentService.getDepartment(id);
		departmentService.deleteDepartment(department);
		return Constants.SUCCESS;
    }
    
    @RequestMapping("/addDept/{pid}")
    public String addDept(HttpServletRequest request, HttpServletResponse response, @PathVariable Long pid) {
    	
    	Department parentDept = ( pid == null ) ? new Department() :  departmentService.getDepartment(pid);
    	request.setAttribute("department",  "" );
    	request.setAttribute("parentDept", JSONUtil.getJson(parentDept));
    	
		return PathResolver.getPath(SecurityBackPage.DEPARTMENT_EDIT_PAGE);
    }
    
    @RequestMapping("/editDept/{id}")
    public String editDept(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

    	Department childDept = ( id == null ) ? new Department() : departmentService.getDepartment(id);
    	Department parentDept = ( childDept == null ) ? new Department() : departmentService.getDepartment(childDept.getParentId());
    	
    	request.setAttribute("department", JSONUtil.getJson(childDept));
    	request.setAttribute("parentDept", JSONUtil.getJson(parentDept));
    	
		return PathResolver.getPath(SecurityBackPage.DEPARTMENT_EDIT_PAGE);
    }
    
    /**
     * 更新部门
     * @param request
     * @param response
     * @param deptId
     * @param deptName
     * @param deptContact
     * @param deptMobile
     * @return
     */
    @SystemControllerLog(description="更新部门")
	@RequestMapping("/updateDept")
	public @ResponseBody String updateDept(HttpServletRequest request, HttpServletResponse response,
			Long deptId, String deptName, String deptContact, String deptMobile )  {
		
		Department department = departmentService.getDepartment(deptId);
		department.setName(deptName);
		department.setContact(deptContact);
		department.setMobile(deptMobile);
		departmentService.updateDepartment(department);
		return JSONUtil.getJson(department);
	}
	
	/**
	 * 保存部门
	 * @param request
	 * @param response
	 * @param parentId
	 * @param deptName
	 * @param deptContact
	 * @param deptMobile
	 * @return
	 */
    @SystemControllerLog(description="保存部门")
	@RequestMapping("/saveDept")
	public @ResponseBody String saveDept(HttpServletRequest request, HttpServletResponse response, Long parentId, String deptName, String deptContact, String deptMobile )  {
		
		Department department = new Department();
		department.setName(deptName);
		department.setContact(deptContact);
		department.setMobile(deptMobile);
		department.setParentId(parentId);
		department.setRecDate(new Date());
		department.setShopId(systemParameterUtil.getDefaultShopId());
		
		Long deptId = departmentService.saveDepartment(department);
		department.setId(deptId);

		return JSONUtil.getJson(department);
	}

    @RequestMapping("/queryDeptInfo")
    public @ResponseBody List<Department> queryDeptInfo(HttpServletRequest request, HttpServletResponse response) {
    	
    	List<Department> deptList = departmentService.getAllDepartment(); 
    	return deptList;
    }

}
