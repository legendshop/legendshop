/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.permission.form;


/**
 * 在权限中选择菜单，代表一个菜单
 */
public class MenuTree {
	/**
	 * 主键
	 */
	private String id;
	
	/**
	 * 是否父节点
	 */
	private boolean isParent=false;
	
	/**
	 * 是否选中当前的菜单
	 */
	private boolean checked = false;
	
	/**
	 * 名称
	 */
	private String name;
	
	/**
	 * 父节点ID
	 */
	private String  pId;
	
	/**
	 * 是否打开
	 */
	private boolean open = false;
	
	public MenuTree(Long id,Long pId,String name,boolean open){
		this.id = id.toString();
		this.setpId(pId == null ? null : pId.toString()) ;
		this.name = name;
		this.open = open;
	}
	
	
	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public MenuTree(){}
	
	public MenuTree(Long id,Long pId,String name){
		this.id = id.toString();
		this.setpId(pId == null ? null : pId.toString()) ;
		this.name = name;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public boolean isParent() {
		return isParent;
	}
	public void setParent(boolean isParent) {
		this.isParent = isParent;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public boolean getChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}


	public String getpId() {
		return pId;
	}


	public void setpId(String pId) {
		this.pId = pId;
	}


}
