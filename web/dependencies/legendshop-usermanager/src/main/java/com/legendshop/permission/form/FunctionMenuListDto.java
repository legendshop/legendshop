package com.legendshop.permission.form;

import java.util.List;

/**
 * 权限包装类
 *
 */
public class FunctionMenuListDto {
	
	private String menuId;

	private String menuName;
	
	private List<FunctionMenuDto> funcWrapperList;

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public List<FunctionMenuDto> getFuncWrapperList() {
		return funcWrapperList;
	}

	public void setFuncWrapperList(List<FunctionMenuDto> funcWrapperList) {
		this.funcWrapperList = funcWrapperList;
	}

	
	
}
