package com.legendshop.permission.dao;

import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Role;
/**
 *  角色Dao
 *
 */
public interface RoleDao  extends Dao<Role, String>{
	public List<String> findRolesNameByUser(String userId, String appNo) ;

	public List<String> findFunctionsByUser(String userId, String appNo);

	public List<String> getPermByShopAccountRole(Long shopRoleId);

	public Role findRoleById(String roleId);

	public void deleteRoleById(String roleId);

	public PageSupport<Role> findAllRole(String search, String myaction, String curPageNO);

	public List<Role> findRoleByFunction(String functionId);

	public String saveRole(Role role);

	public void updateRole(Role role);

	public PageSupport<Role> findAllRolePage(String curPageNO, int pageSize, String name, String enabled, String appNo,Integer category);
	
	public abstract  PageSupport<Role> FindOtherRoleByUser(String curPageNO, String userId, String appNo);
	
	public abstract List<Role> findRoleByUserId(String userId,  String appNo);

	public List<String> findRoleIdsByUser(String userId, String appNo);

}