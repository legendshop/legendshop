/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.tagext.TagSupport;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ShopDetailService;

/**
 * 
 * 
 * 我的商城 如果我是商家就显示对应的信息
 */
public class MyShopTag extends TagSupport {

	private static final long serialVersionUID = -4870270970591649109L;
	/** The shop service. */
	private static ShopDetailService shopDetailService;

	/**
	 * Instantiates a new shop domain tag.
	 */
	public MyShopTag() {
		shopDetailService = (ShopDetailService) ContextServiceLocator.getBean("shopDetailService");
	}

	public int doStartTag() {
		SecurityUserDetail user = UserManager.getUser((HttpServletRequest)pageContext.getRequest());
		if (user != null && user.getShopId() != null) {
			if (shopDetailService.isShopExist(user.getShopId())) {
				return TagSupport.EVAL_BODY_INCLUDE;
			}
		}
		return TagSupport.SKIP_BODY;
	}

}
