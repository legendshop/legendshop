package com.legendshop.core.tag.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.entity.SystemConfig;
import com.legendshop.model.vo.WechatCodeVO;
import com.legendshop.spi.service.SystemConfigService;
import com.legendshop.util.JSONUtil;

public class WechatCodeApi {
	private static SystemConfigService systemConfigService;

	private static final Logger log = LoggerFactory
			.getLogger(WechatCodeApi.class);

	static {
		systemConfigService = (SystemConfigService) ContextServiceLocator.getBean("systemConfigService");
	}

	/**
	 * 获取微信服务号的系统图片
	 */
	public static WechatCodeVO getWechatCodeVO() {
		try {
			SystemConfig systemConfig = systemConfigService.getSystemConfig();
			String wechatCode = systemConfig.getWechatCode();
			WechatCodeVO wechatCodeVO = JSONUtil.getObject(wechatCode,
					WechatCodeVO.class);
			return wechatCodeVO;
		} catch (Exception e) {
			log.error("calculate unread message error:{} with userName:{}",
					e.toString());
		}
		return null;
	}
}
