package com.legendshop.core.tag.api;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.entity.SystemConfig;
import com.legendshop.spi.service.SystemConfigService;

/**
 * 系统配置API
 *
 */
public class SystemConfigApi {
	
	private static SystemConfigService systemConfigService;
	
	private static SystemParameterUtil systemParameterUtil;
	
	static{
		systemConfigService = (SystemConfigService)ContextServiceLocator.getBean(SystemConfigService.class);
		systemParameterUtil = (SystemParameterUtil)ContextServiceLocator.getBean(SystemParameterUtil.class);
	}
	
	public static SystemConfig getSystemConfig(){
		try {
			return systemConfigService.getSystemConfig();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 是否启动qq登录
	 * 参考 QQ_LOGIN_ENABLE
	 */
	public static Boolean isQqLoginEnable(){
		Boolean result = systemParameterUtil.isQqLoginEnable();
		return result;
	}
	
	/**
	 * 是否启动微博登录
	 * 参考 WEIBO_LOGIN_ENABLE
	 */
	public static Boolean isWeiboLoginEnable(){
		Boolean result = systemParameterUtil.isWeiboLoginEnable();
		return result;
	}
	
	/**
	 * 是否启动微信登录
	 * 参考 WEIXIN_LOGIN_ENABLE
	 */
	public static Boolean isWeiXinLoginEnable(){
		Boolean result = systemParameterUtil.isWeixinLoginEnable();
		return result;
	}
	
	/**
	 * 是否启动qq登录
	 * 参考 QQ_LOGIN_ENABLE
	 */
	public static String getQqAppId(){
		String result = systemParameterUtil.getQqAppId();
		return result;
	}
	
	
}
