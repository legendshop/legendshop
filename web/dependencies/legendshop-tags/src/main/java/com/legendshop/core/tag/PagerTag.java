/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.tag;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.legendshop.core.pageprovider.MobilePageProviderImpl;
import com.legendshop.core.pageprovider.NoPageProviderImpl;
import com.legendshop.core.pageprovider.PageProviderEnum;
import com.legendshop.core.pageprovider.PageProviderImpl;
import com.legendshop.core.pageprovider.ProcessOrderPageProviderImpl;
import com.legendshop.core.pageprovider.SimplePageProviderImpl;
import com.legendshop.dao.support.ToolBarProvider;

/**
 * 分页工具条
 */
public class PagerTag extends TagSupport {

	/** The log. */
	private static Log log = LogFactory.getLog(PagerTag.class);
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -518291441012821104L;

	/** The cur page no. */
	private int curPageNO; // 当前页

	/** The page size. */
	private int pageSize; // 每页显示的记录数

	/** The rows count. */
	private long total; // 记录行数

	private String type;//类型

	private String actionUrl = "javascript:pager";

	public void setActionUrl(String actionUrl) {
		this.actionUrl = actionUrl;
	}

	public void setCurPageNO(int curPageNO) {
		this.curPageNO = curPageNO;
	}

	
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public void setType(String type) {
		this.type = type;
	}
	

	/**
	 * 处理工具条
	 */
	@Override
	public int doStartTag() {
		String toolBar = getBar(null, actionUrl);
		try {
			if(toolBar != null){
				pageContext.getOut().println(toolBar); //打印到页面
			}
				
		} catch (IOException e) {
			log.error("IOException: ", e);
		}
		return TagSupport.SKIP_BODY;
	}

	public String getBar(Locale locale, String url) {
		ToolBarProvider provider = null;
		if(type == null){
			 provider = new PageProviderImpl(total, curPageNO, pageSize, actionUrl);
		}else if (PageProviderEnum.SIMPLE_PAGE_PROVIDER.value().equals(type)){
			 provider = new SimplePageProviderImpl(total, curPageNO, pageSize, actionUrl);
		}else if (PageProviderEnum.PAGE_PROVIDER.value().equals(type)){
			 provider = new PageProviderImpl(total, curPageNO, pageSize, actionUrl);
		}else if (PageProviderEnum.PRODUCTLIST_SIMPLEPAGE_PROVIDER.value().equals(type)){
			 provider = new ProcessOrderPageProviderImpl(total, curPageNO, pageSize, actionUrl);
		}else if (PageProviderEnum.MOBILE_PRODUCTLIST_SIMPLEPAGE_PROVIDER.value().equals(type)){
			 provider = new MobilePageProviderImpl(total, curPageNO, pageSize, actionUrl);
		}else if (PageProviderEnum.PROCESS_ORDER_PAGEP_ROVIDER.value().equals(type)){
			 provider = new ProcessOrderPageProviderImpl(total, curPageNO, pageSize, actionUrl);
		}else if (PageProviderEnum.NO_PAGE_PROVIDER.value().equals(type)){
			 provider = new NoPageProviderImpl();
		}
		return provider.getToolBar();
	}
}