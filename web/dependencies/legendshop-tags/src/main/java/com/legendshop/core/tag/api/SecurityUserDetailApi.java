/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.tag.api;

import javax.servlet.http.HttpServletRequest;

import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.security.model.SimpleSecurityUserDetail;

/**
 * 获取当前已经登录用户信息
 */
public class SecurityUserDetailApi {
	
	public static SimpleSecurityUserDetail getSecurityUserDetail(HttpServletRequest request){
		SecurityUserDetail user = UserManager.getUser(request);
		SimpleSecurityUserDetail simpleUser = new SimpleSecurityUserDetail(user);//必须要有返回值,用于提高jstl的性能, 减少对非空值的判断
		return simpleUser;
	}
	
	

}
