package com.legendshop.core.tag.api;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.constant.CouponProviderEnum;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.spi.service.CouponService;
import com.legendshop.spi.service.OrderService;
import com.legendshop.spi.service.SubItemService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;

public class UserInfoApi {
	
	private static final Logger log = LoggerFactory.getLogger(UserInfoApi.class);
	
	private static OrderService orderService;
	
	private static SubItemService subItemService;
	
	private static UserDetailService userDetailService;

	private static CouponService couponService;
	
	
	static{
		orderService = (OrderService) ContextServiceLocator.getBean("orderService");
		subItemService = (SubItemService) ContextServiceLocator.getBean("subItemService");
		userDetailService = (UserDetailService) ContextServiceLocator.getBean("userDetailService");
		couponService = (CouponService)ContextServiceLocator.getBean("couponService");
	}
	
	/**
	 * 计算用户待付款,待收货,待评价 订单条数
	 */
	public static Map<String,Integer> calPendingOrderCount(String userId){
		Map<String,Integer> resultMap = new HashMap<String,Integer>();
		if(AppUtils.isNotBlank(userId)){
			try {
				Integer unPayOrderCount = orderService.calUnpayOrderCount(userId);
				Integer consmOrderCount = orderService.calConsignmentOrderCount(userId);
				Integer unCommCount = subItemService.getUnCommProdCount(userId);
				resultMap.put("unPayOrderCount", unPayOrderCount);
				resultMap.put("consmOrderCount", consmOrderCount);
				resultMap.put("unCommCount", unCommCount);
				
			} catch (Exception e) {
				log.error("calculate PendingOrder Count error:{} with userId:{}",e.toString(),userId);
			}
		}
		return resultMap;
	}
	
	/**
	 * 计算用户预存款
	 * @param userId
	 * @return
	 */
	public static Double calPredepositAmount(String userId){
		if(AppUtils.isBlank(userId)){
			return 0d;
		}
		Double result  = userDetailService.getAvailablePredeposit(userId);
		if(result == null){
			return 0d;
		}
		return result;
	}
	
	/**
	 * 计算用户积分
	 */
	public static Integer calIntergalAmount(String userId){
		if(AppUtils.isBlank(userId)){
			return 0;
		}
		try {
			UserDetail userDetail=userDetailService.getUserDetailById(userId);
			if(AppUtils.isNotBlank(userDetail) && userDetail.getScore()!=null){
				return  userDetail.getScore();
			}
		} catch (Exception e) {
			log.error("calIntergalAmount error:{} with userId:{}",e.toString(),userId);
		}
		return 0;
	}
	
	/**
	 * 计算用户可用优惠券数量
	 */
	public static Integer calUnuseCouponCount(String userId){
		if(AppUtils.isBlank(userId)){
			return 0;
		}
		try {

			return (int)couponService.queryPersonalCouponDto(
					userId, 1, "1", 1, CouponProviderEnum.SHOP.value()).getTotal();
		} catch (Exception e) {
			log.error("calUnuseCouponCount error:{} with userId:{}",e.toString(),userId);
		}
		return 0;
	}
	
	/**
	 * 计算用户可用红包数量
	 */
	public static Integer calRedPacketCount(String userId){
		if(AppUtils.isBlank(userId)){
			return 0;
		}
		try {
			return userDetailService.calUnuseCouponCount(userId,CouponProviderEnum.PLATFORM.value());
		} catch (Exception e) {
			log.error("calredPacketCount error:{} with userId:{}",e.toString(),userId);
		}
		return 0;
	}
	
	/**
	 * 计算 是否是推广员
	 * @param userId
	 * @return
	 */
	public static boolean isPromoter(String userId){
		if(AppUtils.isBlank(userId)){
			log.error("userId is null");
			return false;
		}
		
		try {
			return userDetailService.isPromoter(userId);
		} catch (Exception e) {
			log.error("isPromoter error:{} with userId:{}",e.toString(),userId);
		}
		return false;
	}

}
