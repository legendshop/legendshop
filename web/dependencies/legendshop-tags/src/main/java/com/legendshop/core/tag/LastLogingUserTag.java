/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.tag;

import java.io.IOException;
import java.net.URLDecoder;

import com.legendshop.model.constant.Constants;
import com.legendshop.web.util.CookieUtil;

/**
 * 最后登录用户.
 */
public class LastLogingUserTag extends LegendShopTag {

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
	 */
	@Override
	public void doTag() throws IOException {
		String userName = CookieUtil.getCookieValue(this.request(), Constants.LAST_USERNAME_KEY);
		if (userName != null) {
			userName=URLDecoder.decode(userName,"UTF-8");
			this.write(userName);
		}
	}

}
