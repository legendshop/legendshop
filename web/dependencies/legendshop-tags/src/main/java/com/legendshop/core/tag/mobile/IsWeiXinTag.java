/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.tag.mobile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.tagext.TagSupport;

import com.legendshop.web.util.WeiXinUtil;

/**
 * 是否微信登录.
 */
public class IsWeiXinTag extends TagSupport {

	private static final long serialVersionUID = -779804105023929973L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
	 */
	@Override
	public int doStartTag() {
		if (WeiXinUtil.isWeiXin((HttpServletRequest)pageContext.getRequest())) {
			return TagSupport.EVAL_BODY_INCLUDE;
		} else {
			return TagSupport.SKIP_BODY;
		}
	}

}
