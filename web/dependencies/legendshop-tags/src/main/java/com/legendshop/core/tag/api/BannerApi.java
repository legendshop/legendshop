/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.core.tag.api;

import java.util.List;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.entity.Indexjpg;
import com.legendshop.model.entity.Pub;
import com.legendshop.spi.service.IndexJpgService;
import com.legendshop.spi.service.PubService;

/**
 * 广告的Api.
 */
public class BannerApi {

	/** 首页背景图片的API. */
	private static IndexJpgService indexJpgService;

	/** The pub service. */
	private static PubService pubService;

	static {
		indexJpgService = (IndexJpgService) ContextServiceLocator.getBean("indexJpgService");
		pubService = (PubService) ContextServiceLocator.getBean("pubService");
	}

	/**
	 * 默认商城shopId为"0"的首页图片
	 */
	public static List<Indexjpg> findIndexjpg() {
		try {
			List<Indexjpg> indexJpgList = indexJpgService.getIndexJpegByShopId();
			return indexJpgList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the pub list.
	 *
	 * @return the pub list
	 */
	public static List<Pub> getPubList() {
		try {
			List<Pub> pubList = pubService.getPubByShopId();
			return pubList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
