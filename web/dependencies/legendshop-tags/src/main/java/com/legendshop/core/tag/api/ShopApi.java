/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.core.tag.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.model.security.ShopMenuGroup;
import com.legendshop.model.security.ShopUser;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;

/**
 * 商城相关的API.
 */
public class ShopApi {
	
	private final static Logger log = LoggerFactory.getLogger(ShopApi.class);
	
	/**
	 * 获取商城用户的菜单
	 */
	public static List<ShopMenuGroup> getShopMenuGroups(HttpServletRequest request){
		SecurityUserDetail user = UserManager.getUser(request);
		ShopUser shopUser = UserManager.getShopUser(user);
		if(shopUser == null){
			log.error("visit getShopMenuGroups in ShopApi but shop user does not logined");
			return null;
		}
		List<ShopMenuGroup> shopMenuGroups = shopUser.getShopMenuGroupList();
		return shopMenuGroups;
	}
}
