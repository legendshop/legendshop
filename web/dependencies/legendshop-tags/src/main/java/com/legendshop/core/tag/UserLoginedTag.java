/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.tag;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.tagext.TagSupport;

import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;

/**
 * 用户是否登录.
 */
public class UserLoginedTag extends TagSupport {

	private static final long serialVersionUID = -779804105023929973L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
	 */
	@Override
	public int doStartTag() {
		SecurityUserDetail user = UserManager.getUser((HttpServletRequest)pageContext.getRequest());
		if (UserManager.isUserLogined(user)) {
			return TagSupport.EVAL_BODY_INCLUDE;
		} else {
			return TagSupport.SKIP_BODY;
		}
	}

}
