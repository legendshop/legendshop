/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.tag;

import java.io.IOException;

import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;

/**
 * 当前用户标签.
 */
public class CurrentUserTag extends LegendShopTag {

	/**
	 *  开始
	 */
	@Override
	public void doTag() throws IOException {
		SecurityUserDetail user = UserManager.getUser(this.request());
		String userName = user.getUsername();
		if (userName != null) {
			this.write(userName);
		}
	}

}
