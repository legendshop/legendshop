package com.legendshop.core.tag.api;

import java.util.List;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.entity.Theme;
import com.legendshop.spi.service.ThemeService;


public class ThemeApi{
	private static ThemeService themeService;
	static{
		themeService =(ThemeService)ContextServiceLocator.getBean("themeService");
	}
	public static List<Theme> getThemeList(){
		List<Theme> themeList=themeService.getTheme();
		return themeList;
	}
}
