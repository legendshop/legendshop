package com.legendshop.core.tag;


import java.io.IOException;
import java.util.Set;

import javax.servlet.jsp.JspException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.LocaleResolver;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.constant.AttributeKeys;
import com.legendshop.model.dto.CategoryDto;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.util.AppUtils;
/**
 * 
 * @author tony
 *
 */
public class AllCategoryOptionTag extends LegendShopTag {
	/** The log. */
	private static Log log = LogFactory.getLog(AllCategoryOptionTag.class);

	/** The id. */
	protected String id;

	/** The default disp. */
	protected String defaultDisp = "-- 请选择 --";

	/** The exclude. */
	protected String exclude;

	/** The locale resolver. */
	protected LocaleResolver localeResolver;

	/** The required. */
	protected boolean required;

	/** The selected value. */
	protected String selectedValue;

	private Long shopId;
	
	/** The sort type. */
	protected String type;

	/** The on change. */
	protected String onChange;
	
	
	/** The sort service. */
	private CategoryService categoryService;

	/**
	 * Instantiates a new option tag.
	 */
	public AllCategoryOptionTag() {
		if (localeResolver == null) {
			localeResolver = (LocaleResolver) ContextServiceLocator.getBean(AttributeKeys.LOCALE_RESOLVER);
		}
		if (categoryService == null) {
			categoryService = (CategoryService) getBean("categoryService");
		}
	}

	/**
	 * Do tag.
	 * 
	 * @throws JspException
	 *             the jsp exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @see javax.servlet.jsp.tagext.SimpleTagSupport#doTag()
	 */
	@Override
	public void doTag() throws JspException, IOException {
		StringBuilder sb = new StringBuilder();
		// start
		if (AppUtils.isNotBlank(onChange)) {
			sb.append("<select id=\"").append(id).append("\" name=\"").append(id).append("\" ");
			sb.append("onChange=\"").append(onChange).append("\">");
		} else {
			sb.append("<select id=\"").append(id).append("\" name=\"").append(id).append("\">");
		}
		if (!required) {
			sb.append("<option value=\"\">").append(defaultDisp).append("</option> ");
		}

		String[] excludes = null;
		if (AppUtils.isNotBlank(exclude)) {
			excludes = exclude.split(",");
		}
		Set<CategoryDto> list = retrieveData(type);
		if (AppUtils.isNotBlank(list)) {
			for (CategoryDto category : list) {
				boolean isExclude = false;
				if (AppUtils.isNotBlank(excludes)) {
					for (String ex : excludes) {
						if (ex.equals(String.valueOf(category.getId()))) {
							isExclude = true;
							break;
						}
					}
				}
				if (!isExclude) {
					 this.addOption(sb, category, category.getCategoryName());
					//this.addSubOption(sb, category, category.getCategoryName(),0);
				}

			}
		}

		// end
		sb.append("</select>");
		try {
			this.pageContext().getOut().println(sb);
		} catch (IOException e) {
			log.error("IOException in SelectTag: ", e);
		}

	}

	/**
	 * Retrieve data.
	 * 
	 * @param type
	 *            the type
	 * @param userName
	 *            the user name
	 * @return the list
	 */
	public  Set<CategoryDto> retrieveData(String type){
		return categoryService.getAllCategoryByType(type);
	}

	/**
	 * Adds the option.
	 * 
	 * @param sb
	 *            the sb
	 * @param value
	 *            the value
	 * @param dispValue
	 *            the disp value
	 */
	private void addOption(StringBuilder sb, CategoryDto categoryDto, String dispValue) {
		Long value = categoryDto.getCategoryId();
		sb.append("<option value=\"").append("1-").append(value).append("\"");
		if (value.equals(selectedValue)) {
			sb.append(" selected ");
		}
		sb.append(">");
		sb.append(dispValue);
		sb.append("</option>");
		
		addSubOption(sb, categoryDto, dispValue,0);
	}
	
	private void addSubOption(StringBuilder sb, CategoryDto categoryDto, String dispValue,int num) {
		Set<CategoryDto> list = categoryDto.getChildrens();
		if(AppUtils.isNotBlank(list)){
			num++;
	        int m=0;
			for (CategoryDto nsort : list) {
				Long value =  nsort.getCategoryId();
				dispValue = nsort.getCategoryName();
				sb.append("<option value=\"").append(nsort.getLevel()).append("-").append(value).append("\"");
				if (value.equals(selectedValue)) {
					sb.append(" selected ");
				}
				sb.append(">");
				if(nsort.getLevel()==2){
					sb.append("&emsp;﹂");
				}else if(nsort.getLevel()==3){
					sb.append("&emsp;&emsp;&emsp;﹂");
				}
				/*for(int i=1;i<nsort.getLevel();i++){
                    sb.append("&emsp;﹂");
                }*/
				/*sb.append("&emsp;﹂");
				if(num !=1){
                    if(m<(list.size()-1)){ //判断是否是列表最后一个
                        sb.append("&nbsp;&nbsp;├&nbsp;");
                    }else{
                        sb.append("&nbsp;&nbsp;└&nbsp;");
                    }
                }else{
                    sb.append("&nbsp;&nbsp;");
                }*/
				sb.append(dispValue);
				sb.append("</option>");
				m++;  
				addSubOption(sb,nsort,dispValue,m);
			}
		}
	

	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the default disp.
	 * 
	 * @return the default disp
	 */
	public String getDefaultDisp() {
		return defaultDisp;
	}

	/**
	 * Sets the default disp.
	 * 
	 * @param defaultDisp
	 *            the new default disp
	 */
	public void setDefaultDisp(String defaultDisp) {
		this.defaultDisp = defaultDisp;
	}

	/**
	 * Gets the exclude.
	 * 
	 * @return the exclude
	 */
	public String getExclude() {
		return exclude;
	}

	/**
	 * Sets the exclude.
	 * 
	 * @param exclude
	 *            the new exclude
	 */
	public void setExclude(String exclude) {
		this.exclude = exclude;
	}

	/**
	 * Gets the locale resolver.
	 * 
	 * @return the locale resolver
	 */
	public LocaleResolver getLocaleResolver() {
		return localeResolver;
	}

	/**
	 * Sets the locale resolver.
	 * 
	 * @param localeResolver
	 *            the new locale resolver
	 */
	public void setLocaleResolver(LocaleResolver localeResolver) {
		this.localeResolver = localeResolver;
	}

	/**
	 * Checks if is required.
	 * 
	 * @return true, if is required
	 */
	public boolean isRequired() {
		return required;
	}

	/**
	 * Sets the required.
	 * 
	 * @param required
	 *            the new required
	 */
	public void setRequired(boolean required) {
		this.required = required;
	}

	/**
	 * Gets the selected value.
	 * 
	 * @return the selected value
	 */
	public String getSelectedValue() {
		return selectedValue;
	}

	/**
	 * Sets the selected value.
	 * 
	 * @param selectedValue
	 *            the new selected value
	 */
	public void setSelectedValue(String selectedValue) {
		this.selectedValue = selectedValue;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the on change.
	 * 
	 * @return the on change
	 */
	public String getOnChange() {
		return onChange;
	}

	/**
	 * Sets the on change.
	 * 
	 * @param onChange
	 *            the new on change
	 */
	public void setOnChange(String onChange) {
		this.onChange = onChange;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	
	
}
