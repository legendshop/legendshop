package com.legendshop.core.tag.api;

import java.util.Calendar;
import java.util.Date;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.dto.order.OrderSetMgDto;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 系统参数
 * @author tony
 *
 */
public class ConstTableApi {

	
	/** The product api service. */
	private static ConstTableService constTableService;
	
	static{
		constTableService = (ConstTableService) ContextServiceLocator.getBean("constTableService");
	}
	
	public static ConstTable getConstTable(String type,String keyValue){
		final ConstTable constTable=constTableService.getConstTablesBykey(type, keyValue);
		return constTable;
	}
	
	
	/**
	 * 获得订单的自动确认收货时间
	 * @return
	 */
	public static Date getOrderConfirmTime(Date date){
		String key="ORDER_SETTING";
		Integer expire=7;
		final ConstTable constTable=constTableService.getConstTablesBykey(key, key);
		if(AppUtils.isNotBlank(constTable) && AppUtils.isNotBlank(constTable.getValue())){
			OrderSetMgDto orderSetMgDto=JSONUtil.getObject(constTable.getValue(), OrderSetMgDto.class);
			if(AppUtils.isNotBlank(orderSetMgDto)){
				expire=Integer.valueOf(orderSetMgDto.getAuto_order_confirm());
			}
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, expire);
		return cal.getTime();
	}
	
}
