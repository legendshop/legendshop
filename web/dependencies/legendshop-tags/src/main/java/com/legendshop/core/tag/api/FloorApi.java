/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.tag.api;

import java.util.List;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.entity.FloorDto;
import com.legendshop.spi.service.IndexApiService;

/**
 * 首页楼层API.
 *
 */
public class FloorApi {
	
	private static IndexApiService indexApiService;
	
	static{
		indexApiService = (IndexApiService) ContextServiceLocator.getBean("indexApiService");
	}
	
	public static List<FloorDto> findFloor(){
		try {
			List<FloorDto> floorDtoList = indexApiService.loadFloor();
			return floorDtoList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
