package com.legendshop.core.tag;

/**
 * 下来框选项.
 */
public interface Selectable {
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName();
}
