package com.legendshop.core.tag.api;

import java.util.List;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.entity.Hotsearch;
import com.legendshop.spi.service.HotsearchService;

public class HotSearchApi {
	/**
	 * 首页背景图片的API
	 * @author liyuan
	 */
	private static HotsearchService hotsearchService;
	
	static{
		hotsearchService = (HotsearchService) ContextServiceLocator.getBean("hotsearchService");
		
	}
	
	public static List<Hotsearch> findHotSearch(){
		try {
			List<Hotsearch> hotList=hotsearchService.getHotsearch();
			return hotList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
