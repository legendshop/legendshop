/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.tag;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.taglibs.standard.lang.support.ExpressionEvaluatorManager;
import org.springframework.util.StringUtils;

import com.legendshop.model.constant.LoginUserType;
import com.legendshop.model.security.ShopMenuGroup;
import com.legendshop.model.security.ShopUser;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;

/**
 * 商家子账号权验证
 */
public class ShopUserAuthorizeActionTag extends TagSupport {
	

	private static final long serialVersionUID = 4535456389046052240L;

	/** The if all granted. */
	private String ifAllGranted = "";

	/** The if any granted. */
	private String ifAnyGranted = "";

	/** The if not granted. */
	private String ifNotGranted = "";

	public ShopUserAuthorizeActionTag() {

	}

	/**
	 * Sets the if all granted.
	 * 
	 * @param ifAllGranted
	 *            the new if all granted
	 * @throws JspException
	 *             the jsp exception
	 */
	public void setIfAllGranted(String ifAllGranted) throws JspException {
		this.ifAllGranted = ifAllGranted;
	}

	/**
	 * Gets the if all granted.
	 * 
	 * @return the if all granted
	 */
	public String getIfAllGranted() {
		return ifAllGranted;
	}

	/**
	 * Sets the if any granted.
	 * 
	 * @param ifAnyGranted
	 *            the new if any granted
	 * @throws JspException
	 *             the jsp exception
	 */
	public void setIfAnyGranted(String ifAnyGranted) throws JspException {
		this.ifAnyGranted = ifAnyGranted;
	}

	/**
	 * Gets the if any granted.
	 * 
	 * @return the if any granted
	 */
	public String getIfAnyGranted() {
		return ifAnyGranted;
	}

	/**
	 * Sets the if not granted.
	 * 
	 * @param ifNotGranted
	 *            the new if not granted
	 * @throws JspException
	 *             the jsp exception
	 */
	public void setIfNotGranted(String ifNotGranted) throws JspException {
		this.ifNotGranted = ifNotGranted;
	}

	/**
	 * Gets the if not granted.
	 * 
	 * @return the if not granted
	 */
	public String getIfNotGranted() {
		return ifNotGranted;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
	 */
	@Override
	public int doStartTag() throws JspException {
		if (((null == ifAllGranted) || "".equals(ifAllGranted)) && ((null == ifAnyGranted) || "".equals(ifAnyGranted))
				&& ((null == ifNotGranted) || "".equals(ifNotGranted))) {
			return Tag.SKIP_BODY;
		}

		//用户没有登陆则不显示元素
		SecurityUserDetail userDetail = UserManager.getUser((HttpServletRequest)pageContext.getRequest());
		if (userDetail == null) {
			return Tag.SKIP_BODY;
		}

		//不是商家则不显示元素
		ShopUser shopUser = userDetail.getShopUser();
		if (shopUser == null) {
			return Tag.SKIP_BODY;
		}
		
		//子账号登录才会限制
		if(!LoginUserType.EMPLOYEE_TYPE.equals(userDetail.getLoginUserType())){
			return Tag.EVAL_BODY_INCLUDE;
		}

		List<ShopMenuGroup> shopMenuGroupList = shopUser.getShopMenuGroupList();

		String evaledIfAnyGranted = (String) ExpressionEvaluatorManager.evaluate("ifAnyGranted", ifAnyGranted,
				String.class, pageContext);
		if ((null != evaledIfAnyGranted) && !"".equals(evaledIfAnyGranted)) {
			boolean incluedLabel = hitTheLabel(shopMenuGroupList, parseSecurityString(evaledIfAnyGranted));
			if (!incluedLabel) {
				return Tag.SKIP_BODY;
			}
		}

		String evaledIfNotGranted = (String) ExpressionEvaluatorManager.evaluate("ifNotGranted", ifNotGranted,
				String.class, pageContext);

		if ((null != evaledIfNotGranted) && !"".equals(evaledIfNotGranted)) {
			boolean incluedLabel = hitTheLabel(shopMenuGroupList, parseSecurityString(evaledIfNotGranted));
			if (incluedLabel) {
				return Tag.SKIP_BODY;
			}
		}

		String evaledIfAllGranted = (String) ExpressionEvaluatorManager.evaluate("ifAllGranted", ifAllGranted,
				String.class, pageContext);
		if ((null != evaledIfAllGranted) && !"".equals(evaledIfAllGranted)) {
			Set<String> granted = parseShopMenuLabel(shopMenuGroupList);
			if (!granted.containsAll(parseSecurityString(evaledIfAllGranted))) {
				return Tag.SKIP_BODY;
			}
		}

		return Tag.EVAL_BODY_INCLUDE;
	}

	/**
	 * 处理页面标志属性 ，用' ，'区分,返回Set型数据过滤重复项. 页面定义的需要的权限
	 * 
	 * @param functionsString
	 *            the functions string
	 * @return the sets the
	 */
	private Set<String> parseSecurityString(String shopPermLabel) {
		final Set<String> requiredLabels = new HashSet<String>();
		final String[] shopPermLabels = StringUtils.commaDelimitedListToStringArray(shopPermLabel);
		for (String label : shopPermLabels) {
			// Remove the role's whitespace characters without depending on JDK
			// 1.4+
			// Includes space, tab, new line, carriage return and form feed.
			String result = StringUtils.replace(label, " ", "");
			result = StringUtils.replace(result, "\t", "");
			result = StringUtils.replace(result, "\r", "");
			result = StringUtils.replace(result, "\n", "");
			result = StringUtils.replace(result, "\f", "");

			requiredLabels.add(result);
		}

		return requiredLabels;
	}

	/**
	 * 只要用户获得的权限和声明需要的权限有交集,就返回
	 */
	private boolean hitTheLabel(final List<ShopMenuGroup> shopMenuGroupList, final Set<String> required) {
		if (shopMenuGroupList == null) {// 没有权限则返回
			return false;
		}
		for (String requiredPermLabel : required) {
			for (ShopMenuGroup group : shopMenuGroupList) {
				if (AppUtils.isNotBlank(group.getShopMenuList())) {
//					for (ShopMenu shopMenu : group.getShopMenuList()) {
//						if (shopMenu.getLabel().equals(requiredPermLabel)) {
//							return true;
//						}
//					}
				}
			}
		}
		return false;
	}

	/**
	 * 计算用户有的权限
	 * @param shopMenuGroupList
	 * @return
	 */
	private Set<String> parseShopMenuLabel(final List<ShopMenuGroup> shopMenuGroupList) {
		if (shopMenuGroupList == null) {// 没有权限则返回
			return null;
		}
		Set<String> set = new HashSet<String>();
//		for (ShopMenuGroup group : shopMenuGroupList) {
//			if (AppUtils.isNotBlank(group.getShopMenuList())) {
//				for (ShopMenu shopMenu : group.getShopMenuList()) {
//					set.add(shopMenu.getLabel());
//				}
//			}
//		}
		return set;
	}

}
