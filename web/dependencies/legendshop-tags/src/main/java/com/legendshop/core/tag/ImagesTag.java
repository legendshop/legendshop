/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.tag;

import java.io.IOException;

import com.legendshop.base.util.PhotoPathDto;
import com.legendshop.base.util.PhotoPathResolver;

/**
 * The Class TemplateResourceTag.
 */
public class ImagesTag extends LegendShopTag {

	/** 用于缩略图. */
	private String item;
	
	/** 缩略图尺寸,default is 0. */
	private String scale;
	

	/**
	 * Instantiates a new images tag.
	 */
	public ImagesTag() {

	}

	/**
	 * 由PhotoPathResolver自动计算路径
	 */
	@Override
	public void doTag() throws IOException {
		PhotoPathDto result = PhotoPathResolver.getInstance().calculateImagePath(this.item, scale);
		this.write(result.getFilePath());
	}

	/**
	 * Sets the item.
	 * 
	 * @param item
	 *            the resource to set
	 */
	public void setItem(String item) {
		this.item = item;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}
}
