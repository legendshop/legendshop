/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.tag.api;

import java.util.HashMap;
import java.util.Map;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.promoter.model.DistSet;
import com.legendshop.spi.service.DistSetService;

/**
 * 分销设置Api
 */
public class DistSetApi{

	private static DistSetService distSetService;

	static {
		distSetService = (DistSetService) ContextServiceLocator.getBean("distSetService");
	}

	/**
	 * 获取平台推广设置.
	 *
	 * @return the dist set
	 */
	public static Map<String, String> getDistSet() {
		HashMap<String, String> map = new HashMap<String, String>();
		DistSet distSet = distSetService.getDistSet(1l);
		map.put("supportDist", distSet.getSupportDist().toString());
		map.put("firstLevelRate", distSet.getFirstLevelRate().toString());
		map.put("secondLevelRate", distSet.getSecondLevelRate().toString());
		map.put("thirdLevelRate", distSet.getThirdLevelRate().toString());
		return map;
	}

}
