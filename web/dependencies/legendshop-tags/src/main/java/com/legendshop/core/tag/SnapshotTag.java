/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.tag;

import java.io.IOException;

import com.legendshop.base.util.PhotoPathDto;
import com.legendshop.base.util.PhotoPathResolver;

/**
 * 快照图片.
 */
public class SnapshotTag extends LegendShopTag {

	/** 原来的大图片. */
	private String item;


	public SnapshotTag() {

	}

	/**
	 * Do tag.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @see javax.servlet.jsp.tagext.SimpleTagSupport#doTag()
	 */
	@Override
	public void doTag() throws IOException {
		PhotoPathDto result = PhotoPathResolver.getInstance().calculateFilePath(this.item);
		this.write(result.getFilePath());
	}

	/**
	 * Sets the item.
	 * 
	 * @param item
	 *            the resource to set
	 */
	public void setItem(String item) {
		this.item = item;
	}
}
