/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.tag.api;

import java.util.List;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ProductTypeEnum;
import com.legendshop.model.dto.TreeNode;
import com.legendshop.model.dto.api.NavigationCategory;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.Headmenu;
import com.legendshop.model.entity.Navigation;
import com.legendshop.spi.service.CategoryManagerService;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.spi.service.HeadmenuService;
import com.legendshop.spi.service.NavigationCategoryApiService;
import com.legendshop.spi.service.NavigationService;
import com.legendshop.util.AppUtils;
/**
 * 首页头部导航列表API
 * 
 */ 
public class TopSortApi {

	private static CategoryService categoryService;
	private static CategoryManagerService categoryManagerService;
	private static NavigationCategoryApiService navigationCategoryApiService;
	private static NavigationService navigationService;
	private static HeadmenuService headmenuService;

	static {
		categoryService = (CategoryService) ContextServiceLocator.getBean("categoryService");
		categoryManagerService = (CategoryManagerService) ContextServiceLocator.getBean("categoryManagerService");
		navigationCategoryApiService = (NavigationCategoryApiService) ContextServiceLocator.getBean("navigationCategoryApiService");
		navigationService = (NavigationService) ContextServiceLocator.getBean("navigationService");
		headmenuService = (HeadmenuService) ContextServiceLocator.getBean("headmenuService");
	}

	/**
	 * 获取头部导航
	 * 
	 * @return
	 */
	public static List<Category> findcategoryList() {
		try {
			List<Category> categoryList = categoryService.getCategory(ProductTypeEnum.PRODUCT.value(), 1, null, 1, 1);
			return categoryList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Deprecated
	public static List<NavigationCategory> findNavigationCategory() {
		try {
			List<NavigationCategory> naList = navigationCategoryApiService.findNavigationCategory();
			return naList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static TreeNode findTreeNode(Long treeId) {
		try {// TODO 需要检查
			if (AppUtils.isBlank(treeId)) {
				return null;
			}
			List<TreeNode> treeNodes = categoryManagerService.getTreeNodeNavigation(treeId, ",");
			if (AppUtils.isNotBlank(treeNodes)) {
				int size = treeNodes.size() - 1;
				final TreeNode currentNode = treeNodes.get(size);
				return currentNode;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取分类树根
	 * 
	 * @return
	 */
	public static TreeNode findRootTreeNode() {
		try {
			TreeNode rootTreeNo = categoryManagerService.getTreeNodeById(Constants.PRODUCT_CATEGORY_ROOT);
			return rootTreeNo;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取分类树根 面包屑
	 * 
	 * @return
	 */
	public static List<TreeNode> findTreeNodeNavigation(Long treeId) {
		try { // TODO need to check
			if (AppUtils.isBlank(treeId)) {
				return null;
			}
			List<TreeNode> treeNodes = categoryManagerService.getTreeNodeNavigation(treeId, ",");
			return treeNodes;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<Navigation> getNavigationList() {
		try {
			List<Navigation> navigationList = navigationService.getNavigationList();
			return navigationList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<Headmenu> getHeadmemnu(String type) {
		return headmenuService.getHeadmenuAll(type);
	}
}
