/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.tag;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.taglibs.standard.lang.support.ExpressionEvaluatorManager;
import org.springframework.util.StringUtils;

import com.legendshop.framework.model.GrantedFunction;
import com.legendshop.framework.model.GrantedFunctionImpl;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;

/**
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
@SuppressWarnings("serial")
public class AuthorizeActionTag extends TagSupport {

	/** The if all granted. */
	private String ifAllGranted = "";

	/** The if any granted. */
	private String ifAnyGranted = "";

	/** The if not granted. */
	private String ifNotGranted = "";

	public AuthorizeActionTag() {

	}

	/**
	 * Sets the if all granted.
	 * 
	 * @param ifAllGranted
	 *            the new if all granted
	 * @throws JspException
	 *             the jsp exception
	 */
	public void setIfAllGranted(String ifAllGranted) throws JspException {
		this.ifAllGranted = ifAllGranted;
	}

	/**
	 * Gets the if all granted.
	 * 
	 * @return the if all granted
	 */
	public String getIfAllGranted() {
		return ifAllGranted;
	}

	/**
	 * Sets the if any granted.
	 * 
	 * @param ifAnyGranted
	 *            the new if any granted
	 * @throws JspException
	 *             the jsp exception
	 */
	public void setIfAnyGranted(String ifAnyGranted) throws JspException {
		this.ifAnyGranted = ifAnyGranted;
	}

	/**
	 * Gets the if any granted.
	 * 
	 * @return the if any granted
	 */
	public String getIfAnyGranted() {
		return ifAnyGranted;
	}

	/**
	 * Sets the if not granted.
	 * 
	 * @param ifNotGranted
	 *            the new if not granted
	 * @throws JspException
	 *             the jsp exception
	 */
	public void setIfNotGranted(String ifNotGranted) throws JspException {
		this.ifNotGranted = ifNotGranted;
	}

	/**
	 * Gets the if not granted.
	 * 
	 * @return the if not granted
	 */
	public String getIfNotGranted() {
		return ifNotGranted;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
	 */
	@Override
	public int doStartTag() throws JspException {
		if (((null == ifAllGranted) || "".equals(ifAllGranted)) 
				&& ((null == ifAnyGranted) || "".equals(ifAnyGranted))
				&& ((null == ifNotGranted) || "".equals(ifNotGranted))) {
			return Tag.SKIP_BODY;
		}
		
		//查找用户拥有的权限
		SecurityUserDetail user = UserManager.getUser((HttpServletRequest)pageContext.getRequest());
		
		Collection<GrantedFunction> granted = UserManager.getFunctions(user);
		
		String evaledIfAnyGranted = (String) ExpressionEvaluatorManager.evaluate("ifAnyGranted", ifAnyGranted, String.class, pageContext);
		if ((null != evaledIfAnyGranted) && !"".equals(evaledIfAnyGranted)) {
			Set<GrantedFunction> grantedCopy = retainAll(granted, parseSecurityString(evaledIfAnyGranted));
			if (grantedCopy.isEmpty()) {
				return Tag.SKIP_BODY;
			}
		}

		String evaledIfNotGranted = (String) ExpressionEvaluatorManager.evaluate("ifNotGranted", ifNotGranted, String.class, pageContext);

		if ((null != evaledIfNotGranted) && !"".equals(evaledIfNotGranted)) {
			Set<GrantedFunction> grantedCopy = retainAll(granted, parseSecurityString(evaledIfNotGranted));
			if (!grantedCopy.isEmpty()) {
				return Tag.SKIP_BODY;
			}
		}

		String evaledIfAllGranted = (String) ExpressionEvaluatorManager.evaluate("ifAllGranted", ifAllGranted, String.class, pageContext);
		if ((null != evaledIfAllGranted) && !"".equals(evaledIfAllGranted)) {
			if (!granted.containsAll(parseSecurityString(evaledIfAllGranted))) {
				return Tag.SKIP_BODY;
			}
		}

		
		return Tag.EVAL_BODY_INCLUDE;
	}

	/**
	 * 得到用户功能（Function）的集合，并且验证是否合法，而且可以过滤重复项.
	 * 
	 * @param c
	 *            Collection 类型
	 * @return Set类型
	 */
	private Set<GrantedFunction> securityObjectToFunctions(Collection<GrantedFunction> c) {
		Set<GrantedFunction> target = new HashSet<GrantedFunction>();
		if(c == null){
			return target;
		}
		for (Iterator<GrantedFunction> iterator = c.iterator(); iterator.hasNext();) {
			GrantedFunction function = (GrantedFunction) iterator.next();
			if (null == function.getFunction()) {
				throw new IllegalArgumentException(
						"Cannot process GrantedFunction objects which return null from getFunction() - attempting to process "
								+ function.toString());
			}
			target.add(function);
		}

		return target;
	}

	/**
	 * 处理页面标志属性 ，用' ，'区分,返回Set型数据过滤重复项.
	 * 页面定义的需要的权限
	 * 
	 * @param functionsString
	 *            the functions string
	 * @return the sets the
	 */
	private Set<GrantedFunction> parseSecurityString(String functionsString) {
		final Set<GrantedFunction> requiredFunctions = new HashSet<GrantedFunction>();
		final String[] functions = StringUtils.commaDelimitedListToStringArray(functionsString);
		for (String authority : functions) {
			// Remove the role's whitespace characters without depending on JDK
			// 1.4+
			// Includes space, tab, new line, carriage return and form feed.
			String function = StringUtils.replace(authority, " ", "");
			function = StringUtils.replace(function, "\t", "");
			function = StringUtils.replace(function, "\r", "");
			function = StringUtils.replace(function, "\n", "");
			function = StringUtils.replace(function, "\f", "");

			requiredFunctions.add(new GrantedFunctionImpl(function));
		}

		return requiredFunctions;
	}

	/**
	 * 获得用户所拥有的Function 和 要求的 Function 的交集.
	 * 
	 * @param granted
	 *            用户已经获得的Function
	 * @param required
	 *            所需要的Function
	 * @return the sets the
	 */

	private Set<GrantedFunction> retainAll(final Collection<GrantedFunction> granted, final Set<GrantedFunction> required) {
		Set<GrantedFunction> grantedFunction = securityObjectToFunctions(granted);
		Set<GrantedFunction> requiredFunction = securityObjectToFunctions(required);
		// retailAll() 获得 grantedFunction 和 requiredFunction 的交集
		// 即删除 grantedFunction 中 除了 requiredFunction 的项
		grantedFunction.retainAll(requiredFunction);

		return rolesToAuthorities(grantedFunction, granted);
	}

	/**
	 * Roles to authorities.
	 * 
	 * @param grantedFunctions 已经被过滤过的Function
	 * @param granted 未被过滤过的，即用户所拥有的Function
	 * @return the sets the
	 */
	private Set<GrantedFunction> rolesToAuthorities(Set<GrantedFunction> grantedFunctions, Collection<GrantedFunction> granted) {
		Set<GrantedFunction> target = new HashSet<GrantedFunction>();
		for (Iterator<GrantedFunction> iterator = grantedFunctions.iterator(); iterator.hasNext();) {
			GrantedFunction function = iterator.next();
			for (Iterator<GrantedFunction> grantedIterator = granted.iterator(); grantedIterator.hasNext();) {
				GrantedFunction grantedFunction = (GrantedFunction) grantedIterator.next();
				if (grantedFunction.equals(function)) {
					target.add(grantedFunction);
					break;
				}
			}
		}

		return target;
	}
}
