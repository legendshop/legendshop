/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.tag.api;

import java.util.List;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.entity.ExternalLink;
import com.legendshop.spi.service.ExternalLinkService;

/**
 * 友情链接Api
 *
 */
public class ExternalLinkApi {
	
	private static ExternalLinkService externalLinkService;
	
	static{
		externalLinkService = (ExternalLinkService) ContextServiceLocator.getBean("externalLinkService");
	}
	
	/**
	 * 查找友情链接
	 * @return
	 */
	public static List<ExternalLink> findExternalLink(){
		try {
			return externalLinkService.getExternalLink();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
