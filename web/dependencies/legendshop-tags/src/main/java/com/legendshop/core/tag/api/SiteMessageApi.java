package com.legendshop.core.tag.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.spi.service.MessageService;
import com.legendshop.util.AppUtils;

public class SiteMessageApi {
	
	private static MessageService messageService;
	
	private static final Logger log = LoggerFactory.getLogger(SiteMessageApi.class);
	
	static{
		messageService = (MessageService) ContextServiceLocator.getBean("messageService");
	}
	
	/**
	 * 计算未读消息条数
	 */
	public static Integer calUnreadMsgCount(String userName){
		if(AppUtils.isBlank(userName)){
			return 0;
		}
		try {
			return  messageService.calUnreadMsgCount(userName);
		} catch (Exception e) {
			log.error("calculate unread message error:{} with userName:{}",e.toString(),userName);
		}
		return 0;
	}
}
