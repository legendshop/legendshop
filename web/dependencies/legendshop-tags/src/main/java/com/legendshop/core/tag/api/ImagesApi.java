/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.core.tag.api;

import com.legendshop.base.util.PhotoPathDto;
import com.legendshop.base.util.PhotoPathResolver;

/**
 * 图片相关的Api.
 */
public class ImagesApi {
	
	
	/**
	 * 获取大图的前缀,大图没有后缀
	 */
	public static String getPhotoPrefix(){
		PhotoPathDto result = PhotoPathResolver.getInstance().calculateContextPath();
		return result.getFilePath();
	}
	
	/**
	 * 获取缩略图的前缀
	 */
	public static String getImagesPrefix(String scale){
		if(scale == null) {
			scale = "0";
		}
		PhotoPathDto result = PhotoPathResolver.getInstance().calculateImagesPrefixPath(scale);
		return result.getFilePath();
	}
	
	/**
	 * 获取缩略图后缀
	 */
	public static String getImagesSuffix(String scale){
		if(scale == null) {
			scale = "0";
		}
		PhotoPathDto result = PhotoPathResolver.getInstance().calculateImageSuffixPath(scale);
		return result.getFilePath();
	}
	
}
