package com.legendshop.core.tag.api;

import java.util.List;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.Brand;
import com.legendshop.spi.service.BrandService;
import com.legendshop.util.AppUtils;

public class BrandApi {
	private static BrandService brandService;
	static{
		brandService = (BrandService) ContextServiceLocator.getBean("brandService");
	}
	public static List<Brand> findBrands(){
		try {
			List<Brand> brandList=brandService.getAllBrand();
			return brandList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Brand findBrand(Long brandId){
		try {
			if(AppUtils.isNotBlank(brandId)){
				Brand brand=brandService.getBrand(brandId);
				if(brand!=null && Constants.SUCCESS_STATUS.equals(brand.getStatus())){
					return brand;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
