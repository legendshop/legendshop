package com.legendshop.core.tag.api;

import javax.servlet.http.HttpServletRequest;

import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;

/**
 * 判断用户是否需要登录
 * @author liyuan
 *
 */
public class LoginedApi {
	
	/**
	 * 判断用户是否已经登录
	 * @param request
	 * @return
	 */
	public static boolean isUserLogined(HttpServletRequest request){
		SecurityUserDetail user = UserManager.getUser(request);
		return UserManager.isUserLogined(user);
	}
	
	/**
	 * 判断管理员是否已经登录
	 * @param request
	 * @return
	 */
	public static boolean isAdminLogined(HttpServletRequest request){
		SecurityUserDetail user = UserManager.getUser(request);
		return UserManager.isAdminLogined(user);
	}
}
