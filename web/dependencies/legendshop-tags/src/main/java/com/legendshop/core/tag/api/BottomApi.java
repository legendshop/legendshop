/*
 * 

 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.core.tag.api;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.constant.NavigationPositionEnum;
import com.legendshop.model.constant.NewsPositionEnum;
import com.legendshop.model.dto.NewsCategoryDto;
import com.legendshop.model.dto.NewsDto;
import com.legendshop.model.entity.NavigationItem;
import com.legendshop.model.entity.News;
import com.legendshop.spi.service.NavigationItemService;
import com.legendshop.spi.service.NewsPositionService;
import com.legendshop.spi.service.NewsService;

/**
 * 底部的文章
 *
 */
public class BottomApi {
	
	private static NewsService newsService;
	
	private static NewsPositionService newsPositionService;
	
	private static NavigationItemService navigationItemService;
	
	static{
		newsService = (NewsService)ContextServiceLocator.getBean("newsService");
		
		newsPositionService = (NewsPositionService)ContextServiceLocator.getBean("newsPositionService");
		
		navigationItemService=(NavigationItemService)ContextServiceLocator.getBean("navigationItemService");
	}

	public static Map<Long, NewsCategoryDto> getNewsMap(){
		try {
			List<News> list = newsService.getNews(NewsPositionEnum.NEWS_BOTTOM,200);
			Map<Long, NewsCategoryDto> newsCatMap = new LinkedHashMap<Long, NewsCategoryDto>();
			
			for (News news : list) {
				Long newsCategoryId = news.getNewsCategoryId();
				String newsCategoryName = news.getNewsCategoryName();
				NewsCategoryDto newsCategoryDto = newsCatMap.get(newsCategoryId);
				if(newsCategoryDto == null){
					newsCategoryDto = new NewsCategoryDto();
					newsCategoryDto.setNewsCategoryId(newsCategoryId);
					newsCategoryDto.setNewsCategoryName(newsCategoryName);
					newsCategoryDto.setNewsDtoList(new ArrayList<NewsDto>());
				}
				newsCategoryDto.getNewsDtoList().add(new NewsDto(news.getNewsId(),news.getNewsTitle()));
				newsCatMap.put(newsCategoryId, newsCategoryDto);
			}
			
			return newsCatMap;
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	/**
	 * Gets the news.
	 *
	 * @param size the size
	 * @return the news
	 */
	public static List<News> getNews(Integer size){
		try {
			List<News> list = newsService.getNews(NewsPositionEnum.NEWS_BOTTOM,size);
			return list;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}
	
	
	public static List<NavigationItem> queryNavigationItem(){
		try {
			List<NavigationItem> navigationItemList = navigationItemService.getNavigationItem(NavigationPositionEnum.BOTTOM);
			return navigationItemList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static List<NewsDto> getmostBottonNews(){
		try {
			List<NewsDto> newsList=newsPositionService.getNewsList();
			return newsList;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
