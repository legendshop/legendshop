/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.core.tag.api;

import com.legendshop.base.util.RedisImCacheUtil;
import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.framework.model.GrantedFunction;
import com.legendshop.model.constant.HistoryMessageTypeEnum;
import com.legendshop.model.constant.RoleEnum;
import com.legendshop.model.dto.AdminMessageDto;
import com.legendshop.model.dto.AdminMessageDtoList;
import com.legendshop.model.dto.AdminMessageEnum;
import com.legendshop.model.dto.im.ChatBody;
import com.legendshop.model.entity.Headmenu;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.HeadmenuService;
import com.legendshop.spi.service.MessageService;
import org.springframework.security.core.GrantedAuthority;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 获取头部菜单.
 */
public class HeadMenuApi {
	
	/** The headmenu service. */
	private static HeadmenuService headmenuService;
	
	/** The message service. */
	private static MessageService messageService;
	
	/**客服消息统计工具*/
	private static RedisImCacheUtil imCacheUtil;
	
	static{
		headmenuService = (HeadmenuService) ContextServiceLocator.getBean("headmenuService");
		messageService = (MessageService) ContextServiceLocator.getBean("messageService");
		imCacheUtil = (RedisImCacheUtil) ContextServiceLocator.getBean("redisImCacheUtil");
	}
	
	/**
	 * Gets the headmemnu.
	 *
	 * @param type the type
	 * @return the headmemnu
	 */
	public static List<Headmenu> getHeadmemnu(String type){
		return headmenuService.getHeadmenuAll(type);
	}
	
	/**
	 * Gets the message.
	 *
	 * @param request the request
	 * @return the message
	 */
	public static AdminMessageDtoList getMessage(HttpServletRequest request){
		// 判断是否登录
		SecurityUserDetail userDetail = UserManager.getUser(request);
		if(userDetail == null){
			return null;
		}
		Collection<GrantedAuthority> grantedAuthorities = userDetail.getAuthorities();
		boolean contain = containRoleSupervisor(grantedAuthorities);	
		
		Collection<GrantedFunction> userAuthentication = userDetail.getFunctions();
		
		List<AdminMessageDto> adminMessageList = new ArrayList<AdminMessageDto>();
		
		AdminMessageDtoList adminMessageDtoList=new AdminMessageDtoList();
		
		Integer totalNum =0;
		if(contain || containsAuthentication(userAuthentication,AdminMessageEnum.AUDITUING_SHOP.getLabel())){
			Integer auditShopNumber = messageService.getAuditShop();
			totalNum=totalNum+auditShopNumber;
			
			AdminMessageDto adminMessageDto = new AdminMessageDto();
			adminMessageDto.setName(AdminMessageEnum.AUDITUING_SHOP.value());
			adminMessageDto.setUrl(AdminMessageEnum.AUDITUING_SHOP.url());
			adminMessageDto.setMenuId(AdminMessageEnum.AUDITUING_SHOP.menuId());
			adminMessageDto.setNum(auditShopNumber);
			adminMessageList.add(adminMessageDto);
			
		}
		
        if(contain ||  containsAuthentication(userAuthentication,AdminMessageEnum.PROD_CONSULT.getLabel())){//待回复的咨询
        	Integer productConsultNum = messageService.getProductConsultCount();
        	totalNum=totalNum+productConsultNum;
        	
        	AdminMessageDto adminMessageDto2 = new AdminMessageDto();
    		adminMessageDto2.setName(AdminMessageEnum.PROD_CONSULT.value());
    		adminMessageDto2.setUrl(AdminMessageEnum.PROD_CONSULT.url());
    		adminMessageDto2.setMenuId(AdminMessageEnum.PROD_CONSULT.menuId());
    		adminMessageDto2.setNum(productConsultNum);
    		adminMessageList.add(adminMessageDto2);
		}
		
		if (contain ||  containsAuthentication(userAuthentication,AdminMessageEnum.AUDITUING_PROD.getLabel())) { //商品管理
			Integer auditProductNum = messageService.getAuditProductCount();
			totalNum=totalNum+auditProductNum;
			
			AdminMessageDto adminMessageDto3 = new AdminMessageDto();
			adminMessageDto3.setName(AdminMessageEnum.AUDITUING_PROD.value());
			adminMessageDto3.setUrl(AdminMessageEnum.AUDITUING_PROD.url());
			adminMessageDto3.setMenuId(AdminMessageEnum.AUDITUING_PROD.menuId());
			adminMessageDto3.setNum(auditProductNum);
			adminMessageList.add(adminMessageDto3);
			
		}

		if (contain ||  containsAuthentication(userAuthentication, AdminMessageEnum.AUDITUING_BRAND.getLabel())) { //品牌管理
			Integer auditBrandNum = messageService.getAuditBrandCount();
			totalNum=totalNum+auditBrandNum;
			
			AdminMessageDto adminMessageDto4 = new AdminMessageDto();
			adminMessageDto4.setName(AdminMessageEnum.AUDITUING_BRAND.value());
			adminMessageDto4.setUrl(AdminMessageEnum.AUDITUING_BRAND.url());
			adminMessageDto4.setMenuId(AdminMessageEnum.AUDITUING_BRAND.menuId());
			adminMessageDto4.setNum(auditBrandNum);
			adminMessageList.add(adminMessageDto4);
		}

		if (contain ||  containsAuthentication(userAuthentication, AdminMessageEnum.RETURNGOODS.getLabel())) { //退货退款管理
			Integer returnGoodsNum = messageService.getReturnGoodsCount();
			totalNum=totalNum+returnGoodsNum;
			
			AdminMessageDto adminMessageDto5 = new AdminMessageDto();
			adminMessageDto5.setName(AdminMessageEnum.RETURNGOODS.value());
			adminMessageDto5.setUrl(AdminMessageEnum.RETURNGOODS.url());
			adminMessageDto5.setMenuId(AdminMessageEnum.RETURNGOODS.menuId());
			adminMessageDto5.setNum(returnGoodsNum);
			adminMessageList.add(adminMessageDto5);
		}
		
		if (contain ||  containsAuthentication(userAuthentication, AdminMessageEnum.RETURNMONEY.getLabel() )) { //单退款管理
			Integer returnMoneyNum = messageService.getReturnMoneyCount();
			totalNum=totalNum+returnMoneyNum;
			
			AdminMessageDto adminMessageDto6 = new AdminMessageDto();
			adminMessageDto6.setName(AdminMessageEnum.RETURNMONEY.value());
			adminMessageDto6.setUrl(AdminMessageEnum.RETURNMONEY.url());
			adminMessageDto6.setMenuId(AdminMessageEnum.RETURNMONEY.menuId());
			adminMessageDto6.setNum(returnMoneyNum);
			adminMessageList.add(adminMessageDto6);
			
		}
		
		
		if (contain ||  containsAuthentication(userAuthentication, AdminMessageEnum.ACCUSATION.getLabel() )) { //举报管理
			Integer accusationNum = messageService.getAccusationCount();
			totalNum=totalNum+accusationNum;
			
			AdminMessageDto adminMessageDto7 = new AdminMessageDto();
			adminMessageDto7.setName(AdminMessageEnum.ACCUSATION.value());
			adminMessageDto7.setUrl(AdminMessageEnum.ACCUSATION.url());
			adminMessageDto7.setMenuId(AdminMessageEnum.ACCUSATION.menuId());
			adminMessageDto7.setNum(accusationNum);
			adminMessageList.add(adminMessageDto7);
		}
		
		if (contain ||  containsAuthentication(userAuthentication, "M_SHOP_BILL")) { //结算管理
			Integer shopBillNum = messageService.getShopBillCount();
			totalNum=totalNum+shopBillNum;
			
			AdminMessageDto adminMessageDto8 = new AdminMessageDto();
			adminMessageDto8.setName(AdminMessageEnum.SHOP_BILL.value());
			adminMessageDto8.setUrl(AdminMessageEnum.SHOP_BILL.url());
			adminMessageDto8.setMenuId(AdminMessageEnum.SHOP_BILL.menuId());
			adminMessageDto8.setNum(shopBillNum);
			adminMessageList.add(adminMessageDto8);
			
			
			Integer shopBillConfirmNum = messageService.getShopBillConfirmCount();
			totalNum=totalNum+shopBillConfirmNum;
			
			AdminMessageDto adminMessageDto9 = new AdminMessageDto();
			adminMessageDto9.setName(AdminMessageEnum.SHOP_BILL_CONFIRM.value());
			adminMessageDto9.setUrl(AdminMessageEnum.SHOP_BILL_CONFIRM.url());
			adminMessageDto9.setMenuId(AdminMessageEnum.SHOP_BILL_CONFIRM.menuId());
			adminMessageDto9.setNum(shopBillConfirmNum);
			adminMessageList.add(adminMessageDto9);
			
		}
		
		if (contain ||  containsAuthentication(userAuthentication, "M_AUCTION_LIST")) { //拍卖活动管理
			Integer auctionNum = messageService.getAuctionCount();
			totalNum=totalNum+auctionNum;
			
			AdminMessageDto adminMessageDto10 = new AdminMessageDto();
			adminMessageDto10.setName(AdminMessageEnum.AUCTION.value());
			adminMessageDto10.setUrl(AdminMessageEnum.AUCTION.url());
			adminMessageDto10.setMenuId(AdminMessageEnum.AUCTION.menuId());
			adminMessageDto10.setNum(auctionNum);
			adminMessageList.add(adminMessageDto10);
		}
		
		if (contain ||  containsAuthentication(userAuthentication, "M_SECKILL_LIST")) { //秒杀活动管理
			Integer seckillActivityNum = messageService.getSeckillActivityCount();
			totalNum=totalNum+seckillActivityNum;
			
			AdminMessageDto adminMessageDto11 = new AdminMessageDto();
			adminMessageDto11.setName(AdminMessageEnum.SECKILL_ACTIVITY.value());
			adminMessageDto11.setUrl(AdminMessageEnum.SECKILL_ACTIVITY.url());
			adminMessageDto11.setMenuId(AdminMessageEnum.SECKILL_ACTIVITY.menuId());
			adminMessageDto11.setNum(seckillActivityNum);
			adminMessageList.add(adminMessageDto11);
		}
		
		if (contain ||  containsAuthentication(userAuthentication, "M_GROUP_LIST")) { //团购活动管理
			Integer groupNum = messageService.getGroupCount();
			totalNum=totalNum+groupNum;
			
			AdminMessageDto adminMessageDto12 = new AdminMessageDto();
			adminMessageDto12.setName(AdminMessageEnum.GROUP.value());
			adminMessageDto12.setUrl(AdminMessageEnum.GROUP.url());
			adminMessageDto12.setMenuId(AdminMessageEnum.GROUP.menuId());
			adminMessageDto12.setNum(groupNum);
			adminMessageList.add(adminMessageDto12);
			
		}
		
		
		if (contain ||  containsAuthentication(userAuthentication, "M_PRESELL_MANAGER")) { //待审核的预售活动
			Integer presellProdNum = messageService.getPresellProdCount();
			totalNum=totalNum+presellProdNum;
			
			AdminMessageDto adminMessageDto13 = new AdminMessageDto();
			adminMessageDto13.setName(AdminMessageEnum.PRESELL_PROD.value());
			adminMessageDto13.setUrl(AdminMessageEnum.PRESELL_PROD.url());
			adminMessageDto13.setMenuId(AdminMessageEnum.PRESELL_PROD.menuId());
			adminMessageDto13.setNum(presellProdNum);
			adminMessageList.add(adminMessageDto13);
		}
	
		
		if (contain ||  containsAuthentication(userAuthentication,  AdminMessageEnum.USERCOMMIS.getLabel())) { //推广员列表
			Integer userCommisNum = messageService.getUserCommisCount();
			totalNum=totalNum+userCommisNum;
			
			AdminMessageDto adminMessageDto14 = new AdminMessageDto();
			adminMessageDto14.setName(AdminMessageEnum.USERCOMMIS.value());
			adminMessageDto14.setUrl(AdminMessageEnum.USERCOMMIS.url());
			adminMessageDto14.setMenuId(AdminMessageEnum.USERCOMMIS.menuId());
			adminMessageDto14.setNum(userCommisNum);
			adminMessageList.add(adminMessageDto14);
		}
		
		if (contain ||  containsAuthentication(userAuthentication, AdminMessageEnum.USER_FEEDBACK.getLabel())) { //反馈意见
			Integer userFeedBackNum = messageService.getUserFeedBackCount();
			totalNum=totalNum+userFeedBackNum;
			
			AdminMessageDto adminMessageDto15 = new AdminMessageDto();
			adminMessageDto15.setName(AdminMessageEnum.USER_FEEDBACK.value());
			adminMessageDto15.setUrl(AdminMessageEnum.USER_FEEDBACK.url());
			adminMessageDto15.setMenuId(AdminMessageEnum.USER_FEEDBACK.menuId());
			adminMessageDto15.setNum(userFeedBackNum);
			adminMessageList.add(adminMessageDto15);
		}
		
		if (contain ||  containsAuthentication(userAuthentication, AdminMessageEnum.PRODUCT_COMMENT.getLabel())) { //待审核的评论
			Integer productcommentNum = messageService.getProductcommentCount();
			totalNum=totalNum+productcommentNum;
			
			AdminMessageDto adminMessageDto16 = new AdminMessageDto();
			adminMessageDto16.setName(AdminMessageEnum.PRODUCT_COMMENT.value());
			adminMessageDto16.setUrl(AdminMessageEnum.PRODUCT_COMMENT.url());
			adminMessageDto16.setMenuId(AdminMessageEnum.PRODUCT_COMMENT.menuId());
			adminMessageDto16.setNum(productcommentNum);
			adminMessageList.add(adminMessageDto16);
		}
		
		if (contain ||  containsAuthentication(userAuthentication, AdminMessageEnum.IM.getLabel())) { //待回复的客服消息
			//获取发给平台的未读客服消息数
			int unReadUserMessage = 0;
			Set<Object> keys=imCacheUtil.offPlatformMessageKeys(HistoryMessageTypeEnum.USER.value());
			for (Object key : keys) {
				Set<Object> messages = imCacheUtil.getOffMessage(key);
				for (Object object : messages) {
					ChatBody chatBody = (ChatBody)object;
					if(chatBody.getMsgType() != 7) {
						unReadUserMessage ++;
					}
				}
			}
			totalNum=totalNum+unReadUserMessage;
			
			AdminMessageDto adminMessageDto17 = new AdminMessageDto();
			adminMessageDto17.setName(AdminMessageEnum.IM.value());
			adminMessageDto17.setUrl(AdminMessageEnum.IM.url());
			adminMessageDto17.setMenuId(AdminMessageEnum.IM.menuId());
			adminMessageDto17.setNum(unReadUserMessage);
			adminMessageList.add(adminMessageDto17);
		}
		
		adminMessageDtoList.setList(adminMessageList);
		adminMessageDtoList.setTotalMessage(totalNum);
		return adminMessageDtoList;
	}
	
	
	/**
	 * 判断是否包含该资源的权限信息.
	 *
	 * @param userAuthentication the user authentication
	 * @param func the func
	 * @return true, if successful
	 */
	private static boolean containsAuthentication(Collection<GrantedFunction> userAuthentication, String func) {
		boolean result=false;
		for (Iterator<GrantedFunction> iterator = userAuthentication.iterator(); iterator.hasNext();) {
			GrantedFunction grantedFunction = (GrantedFunction) iterator.next();
			String function=grantedFunction.getFunction();
			if(function.equals(func)){
				result=true;
				break;
			}
		}
		return result;
	}
	
	
	/**
	 * 判断是否超级管理员帐号.
	 *
	 * @param rolesCollection the roles collection
	 * @return true, if successful
	 */
	private static boolean containRoleSupervisor(Collection<GrantedAuthority> rolesCollection) {
		boolean contain = false;
		for (Iterator<GrantedAuthority> iterator = rolesCollection.iterator(); iterator.hasNext();) {
			GrantedAuthority grantedAuthority = (GrantedAuthority) iterator.next();
			if (grantedAuthority.getAuthority().equals(String.valueOf(RoleEnum.ROLE_SUPERVISOR))) {
				contain = true;
				break;
			}
		}
		return contain;
	}
	
}
