/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.tag.api;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.core.handler.ShopCartHandler;
import com.legendshop.framework.handler.impl.ContextServiceLocator;
import com.legendshop.model.dto.ParamGroupDto;
import com.legendshop.model.entity.AfterSale;
import com.legendshop.model.entity.Product;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.AfterSaleService;
import com.legendshop.spi.service.BasketService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.util.AppUtils;

/**
 * 商品服务API.
 *
 * @author newway
 */
public class ProductApi {
	
	/** The Constant log. */
	private final static Logger log=LoggerFactory.getLogger(ProductApi.class);
	
	/** The product service. */
	private static ProductService productService;
	
	
	private static ShopCartHandler shopCartHandler;
	
	
	private static BasketService basketService;
	
	private static AfterSaleService afterSaleService;
	
	static{
		productService = (ProductService) ContextServiceLocator.getBean("productService");
		basketService = (BasketService) ContextServiceLocator.getBean("basketService");
		shopCartHandler = (ShopCartHandler) ContextServiceLocator.getBean("shopCartHandler");
		afterSaleService = (AfterSaleService) ContextServiceLocator.getBean("afterSaleService");
	}
	
	/**
	 * Instantiates a new product api.
	 */
	private  ProductApi() {
		
	}
	
	/**
	 * 根据商品id 获取 参数信息
	 * @param prodId
	 * @return
	 */
	public static List<ParamGroupDto> getProdParameter(Long prodId){
		if (AppUtils.isBlank(prodId)) {
			return null;
		}
		return productService.getParamGroups(prodId);
	}
	
	/**
	 * 获取猜你喜欢 的商品列表
	 * @return
	 */
	public static List<Product> guessYouLike(){
		return productService.getNewProds(12);
	}
	
	/**
	 * 获取最近收藏的商品
	 * @param amount 数量
	 * @return
	 */
	public static List<Product> getRencFavorProd(HttpServletRequest request,Integer amount){
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		return productService.getRencFavorProd(amount,userId);
	}
	
	/**
	 * 获取购物车数量
	 * @return
	 */
	public static Integer getShopCartCounts(HttpServletRequest request){
		int allCount = 0;
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(user)) {
			allCount = shopCartHandler.getShopCartAllCount(request);
		}else{
			allCount = basketService.getBasketCountByUserId(user.getUserId());
		}
		return allCount;
	}
	
	
	/**
	 * 获取分类的热销产品
	 * @return
	 */
	public static List<Product> getHotsaleProdByCategoryId(Long categoryId,Integer total){
		List<Product> hotsaleProducts=null;
		if(AppUtils.isNotBlank(categoryId) && categoryId>0){
			hotsaleProducts=productService.getHotsaleProdByCategoryId(categoryId,total);
		}
		return hotsaleProducts;
	}
	
	/**
	 * 获取售后说明
	 * @param afterSaleId
	 * @return
	 */
	public static AfterSale getProdAfterSale(Long afterSaleId){
		return afterSaleService.getAfterSale(afterSaleId);
	}

}
