/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.core.tag.mobile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.tagext.TagSupport;

import com.legendshop.web.util.WeiXinUtil;

/**
 * 是否手机端和微信
 *
 */
public class IsMobileAndWeiXinTag extends TagSupport{
	
	private static final long serialVersionUID = -1741565206606951360L;

	@Override
	public int doStartTag() {
		if (WeiXinUtil.isWeiXin((HttpServletRequest)pageContext.getRequest())
				&&WeiXinUtil.isMoblie((HttpServletRequest)pageContext.getRequest())) {
			return TagSupport.EVAL_BODY_INCLUDE;
		} else {
			return TagSupport.SKIP_BODY;
		}
	}
	
	
}
