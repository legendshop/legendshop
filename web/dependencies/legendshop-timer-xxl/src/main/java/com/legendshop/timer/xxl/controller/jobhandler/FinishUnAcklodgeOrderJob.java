/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.model.dto.order.OrderSetMgDto;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.Sub;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.spi.service.OrderScheduledService;
import com.legendshop.spi.service.SubService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 * 订单7天自动确认.
 */
@JobHandler(value="finishUnAcklodgeOrderJob")
@Component
public class FinishUnAcklodgeOrderJob extends IJobHandler {
	
	@Autowired
	private ConstTableService constTableService;
	
	@Autowired
	private SubService subService;

	@Autowired
	private OrderScheduledService orderScheduledService;
	
	
	//////////////////////////////////////////////////

	private Integer commitInteval = 100;

	private final static String type = "ORDER_SETTING";

	private final static String keyValue = "ORDER_SETTING";
	
	/**
	 * 执行方法体
	 */
	@Override
	public ReturnT<String> execute(String param) throws Exception {
		ConstTable constTable = constTableService.getConstTablesBykey(type, keyValue);
		Integer expire = 7;
		if (AppUtils.isNotBlank(constTable) && AppUtils.isNotBlank(constTable.getValue())) {
			OrderSetMgDto orderSet = JSONUtil.getObject(constTable.getValue(), OrderSetMgDto.class);
			if (AppUtils.isNotBlank(orderSet) && AppUtils.isNotBlank(orderSet.getAuto_order_confirm())) {
				expire = Integer.valueOf(orderSet.getAuto_order_confirm());
			}
		}

		// 默认7天前
		Date date = getfinishUnAcklodgeDate(expire);
		XxlJobLogger.log("<!---执行订单7天自动确认任务 -->");
		long processNum = 0;  //执行的条数
		boolean haveValue = true;
		while (haveValue) {
			List<Sub> list = subService.getUnAcklodgeSub(commitInteval, date);
			if (AppUtils.isBlank(list)) {
				haveValue = false;
			} else {
				processNum = processNum + list.size();
				orderScheduledService.finishUnAcklodge(list);
			}
		}
		return new ReturnT<>(200, "处理订单条数： " + processNum);
	}
	
	/**
	 * 得到跟现在若干天时间，如果为负数则向前推.
	 * 
	 * @param days
	 *            the days
	 * @return the date
	 */
	private Date getfinishUnAcklodgeDate(int days) {
		Date myDate = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(myDate);
		cal.add(Calendar.DATE, -days);
		return cal.getTime();
	}

}
