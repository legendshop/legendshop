package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.legendshop.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.model.constant.SeckillStatusEnum;
import com.legendshop.model.dto.order.OrderSetMgDto;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.SeckillActivity;
import com.legendshop.model.entity.Sub;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.spi.service.SeckillActivityService;
import com.legendshop.spi.service.SeckillService;
import com.legendshop.spi.service.SubService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;
import com.legendshop.util.JSONUtil;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 *定时处理过期的秒杀活动
 */
@JobHandler(value = "seckillActivityExpiredJob")
@Component
public class SeckillActivityExpiredJob extends IJobHandler {

	@Autowired
	private SeckillActivityService seckillActivityService;
	
	@Autowired
	private ConstTableService constTableService;
	
	@Autowired
	private SeckillService seckillService;
	
	@Autowired
	private SubService subService;
	
    @Override
    public ReturnT<String> execute(String param) throws Exception {
        XxlJobLogger.log("开始处理过期秒杀活动[第三步]");
        
        //获取已完成的秒杀活动
        List<SeckillActivity> SeckillActivitys = seckillActivityService.querySeckillActivityByStatus(SeckillStatusEnum.TRANSORDER.value());
        if(AppUtils.isNotBlank(SeckillActivitys)){
        	
        	XxlJobLogger.log("共有 {} 个过期秒杀活动需要处理!", SeckillActivitys.size());
        	
        	// 获取订单设置配置
        	ConstTable constTable = constTableService.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
        	String setting = constTable.getValue();
        	OrderSetMgDto orderSetting = JSONUtil.getObject(setting, OrderSetMgDto.class);
        	
        	Date now = new Date();
        	for (SeckillActivity seckillActivity : SeckillActivitys) {
        		
        		// 标记当前秒杀活动是否可回滚
        		boolean rolbackable = true;
        		
        		// 查询当前秒杀活动最后一个已提交但未支付的订单
        		Sub sub = subService.getLastOneSubmitAndUnPaySeckillSub(seckillActivity.getId());
        		if(null != sub){
        			
        			// 取这个订单的提交时间 加上 订单的自动取消时间, 如果过了这个时间就可以回滚库存了
        			Date rollbackTime = DateUtil.add(sub.getSubDate(), Calendar.MINUTE, Long.parseLong(orderSetting.getAuto_order_cancel()));
        			if(now.getTime() < rollbackTime.getTime()){// 说明没有过这个时间, 代表还有已提交未支付的订单, 暂不能回滚库存
						XxlJobLogger.log("还有已提交未支付的订单，暂不能回滚库存");
						rolbackable = false;
        			}
        		}
        		
        		if(rolbackable){
					XxlJobLogger.log("开始处理未支付订单库存回滚");
					// 未支付订单库存回滚
        			seckillActivityService.stockRollback(seckillActivity.getId());
        		}
        	}
        }else{
        	XxlJobLogger.log("没有已过期的秒杀活动要处理~");
        }
        
        XxlJobLogger.log("结束处理过期秒杀活动[第三步]");
        return SUCCESS;
    }
    
}
