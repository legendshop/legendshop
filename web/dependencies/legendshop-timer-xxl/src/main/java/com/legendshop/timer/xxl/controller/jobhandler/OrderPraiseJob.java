package com.legendshop.timer.xxl.controller.jobhandler;

import com.legendshop.model.dto.order.OrderSetMgDto;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.*;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@JobHandler(value = "orderPraiseJob")
@Component
public class OrderPraiseJob extends IJobHandler {

    @Autowired
    private SubItemService subItemService;

    @Autowired
    private ConstTableService constTableService;

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductCommentService productCommentService;

    @Autowired
    private UserDetailService userDetailService;

    @Override
    public ReturnT<String> execute(String s) throws Exception {

        ConstTable constTable = constTableService.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
        OrderSetMgDto orderSetting = null;
        if (AppUtils.isNotBlank(constTable)) {
            String setting = constTable.getValue();
            orderSetting = JSONUtil.getObject(setting, OrderSetMgDto.class);
        }

        String comment = orderSetting.getAuto_order_comment();
        //设置默认好评的时间
		Integer integer = Integer.valueOf(comment);
//		Long time =  integer* 3600 * 1000 * 24L;
		BigDecimal second=new BigDecimal(3600);
		BigDecimal min=new BigDecimal(1000);
		BigDecimal hour=new BigDecimal(24);
		BigDecimal bigTime=new BigDecimal(integer).multiply(second).multiply(min).multiply(hour);
//        测试默认值 30
//        Long time = 30 * 3600 * 1000 * 24L;
        Long nowTime = new Date().getTime();
		BigDecimal bigNewTime=new BigDecimal(nowTime);
		BigDecimal commTime =bigNewTime.subtract(bigTime);
        Date commDate = new Date(bigTime.intValue());

        //查询所有已完成，未评价，且订单完成时间  finally_date 加默认好评时间  time  大于等于现在时间 now
        List<SubItem> subItemList = subItemService.getNoCommonSubItemList(commDate);
        //遍历订单项，创建评论
        if (subItemList != null && subItemList.size() > 0) {
            for (SubItem subItem : subItemList) {
                ProductComment productComment = new ProductComment();
                productComment.setProdId(subItem.getProdId());
                //缺少店铺名称
                ProductDetail prodDetail = productService.getProdDetail(subItem.getProdId());
                if (prodDetail == null) {
                    //商品不存在，跳过评论！
                    subItem.setCommSts(1);
                    subItemService.updateSubItem(subItem);
                    continue;
                }
                productComment.setOwnerName(prodDetail.getUserName());
                productComment.setUserId(subItem.getUserId());
                UserDetail userDetail = userDetailService.getUserDetailById(subItem.getUserId());
                //缺少用户名称
                productComment.setUserName(userDetail.getUserName());
                productComment.setContent("评价方未及时做出评价,系统默认好评！");
                productComment.setPostip("127.0.0.1");
                productComment.setScore(5);
                productComment.setShopScore(5);
                productComment.setLogisticsScore(5);
                productComment.setUsefulCounts(0);
                productComment.setReplayCounts(0);
                productComment.setSubItemId(subItem.getSubItemId());
                productComment.setPhotos(null);
                productComment.setIsAnonymous(1);
                productComment.setIsAddComm(false);
                productComment.setStatus(1);
                productComment.setAddtime(new Date());
                productComment.setIsReply(false);
                productComment.setShopReplyContent(null);
                productComment.setShopReplyTime(null);
                productCommentService.save(productComment);
                subItem.setCommSts(1);
                subItemService.updateSubItem(subItem);

				//更新评论统计信息, 包括评论统计, 店铺评分统计, 物流评分统计, 以及商品的评论数和分数
				productCommentService.updateCommentStatInfo(productComment);
            }

        }

        //遍历所有查出订单项，通过订单项生成商品评价

        return SUCCESS;
    }
}
