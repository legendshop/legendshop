/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.service.impl;

import com.legendshop.model.entity.BiddersWin;
import com.legendshop.timer.xxl.dao.TimerBiddersWinDao;
import com.legendshop.timer.xxl.service.TimerBiddersWinService;
import com.legendshop.util.AppUtils;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 中标服务实现类
 */
@Service("timerBiddersWinService")
public class TimerBiddersWinServiceImpl implements TimerBiddersWinService {
	
	@Autowired
	private TimerBiddersWinDao timerBiddersWinDao;

	/**
	 * 获取中标用户 
	 */
	public List<BiddersWin> getBiddersWin(String userId) {
		return timerBiddersWinDao.getBiddersWin(userId);
	}

	/**
	 * 获取中标用户 
	 */
	public BiddersWin getBiddersWin(Long id) {
		return timerBiddersWinDao.getBiddersWin(id);
	}

	/**
	 * 删除中标用户 
	 */
	public void deleteBiddersWin(BiddersWin biddersWin) {
		timerBiddersWinDao.deleteBiddersWin(biddersWin);
	}

	/**
	 * 保存中标用户 
	 */
	public Long saveBiddersWin(BiddersWin biddersWin) {
		if (!AppUtils.isBlank(biddersWin.getId())) {
			updateBiddersWin(biddersWin);
			return biddersWin.getId();
		}
		return timerBiddersWinDao.saveBiddersWin(biddersWin);
	}

	/**
	 * 更新中标用户 
	 */
	public void updateBiddersWin(BiddersWin biddersWin) {
		timerBiddersWinDao.updateBiddersWin(biddersWin);
	}

	/**
	 * 获取中标用户 
	 */
	@Override
	public BiddersWin getBiddersWin(String userId, Long id) {
		return timerBiddersWinDao.getBiddersWin(userId, id);
	}

	/**
	 * 更新中标用户 
	 */
	@Override
	public void updateBiddersWin(Long id, String subNember, Long subId, int value) {
		timerBiddersWinDao.updateBiddersWin(id, subNember, subId, value);
	}

	/**
	 * 获取中标用户 
	 */
	@Override
	public List<BiddersWin> getBiddersWin(Long firstId, Long toId, int commitInteval, Integer status) {
		return timerBiddersWinDao.getBiddersWin(firstId, toId, commitInteval, status);
	}

	/**
	 * 获取中标用户列表 
	 */
	@Override
	public List<BiddersWin> getBiddersWinList(Long id) {
		return timerBiddersWinDao.getBiddersWinList(id);
	}

	/**
	 * 获取最小id 
	 */
	@Override
	public Long getMinId(Integer status, Date nowDate) {
		return timerBiddersWinDao.getMinId(status, nowDate);
	}

	/**
	 * 获取最大id 
	 */
	@Override
	public Long getMaxId(Integer status, Date nowDate) {
		return timerBiddersWinDao.getMaxId(status, nowDate);
	}

}
