/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.model.constant.AuctionsStatusEnum;
import com.legendshop.model.constant.BiddersWinStatusEnum;
import com.legendshop.model.entity.Auctions;
import com.legendshop.model.entity.Bidders;
import com.legendshop.model.entity.BiddersWin;
import com.legendshop.spi.service.BiddersService;
import com.legendshop.timer.xxl.service.TimerAuctionsService;
import com.legendshop.timer.xxl.service.TimerBiddersWinService;
import com.legendshop.util.AppUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 * 竞拍结束后没有完成拍卖定时器.
 */
@JobHandler(value="acutionFinishBidJob")
@Component
public class AcutionFinishBidJob extends IJobHandler {
	
	@Autowired
	private TimerAuctionsService timerAuctionsService;
	
	@Autowired
	private BiddersService biddersService;
	
	@Autowired
	private TimerBiddersWinService timerBiddersWinService;
	
	@Override
	public ReturnT<String> execute(String param) throws Exception {
			XxlJobLogger.log("<!--- auctionBiddersWin is today, generate auctionBiddersWin begin");
			
			int processOrder = 0;
			
			//查询所有在线并且过期的活动
			Date now = new Date();
			List<Auctions> auctionList = timerAuctionsService.getOvertimeAuctionsBystatus(AuctionsStatusEnum.ONLINE.value(), now);
			
			if(AppUtils.isNotBlank(auctionList)){
				for (Auctions auctions : auctionList) {
					
					boolean isDelay = false;
					double curPrice = 1d;
					
					// 获取最新出价记录
					List<Bidders> bidders = biddersService.getBiddersByAuctionId(auctions.getId(), 0, 1);
					
					Bidders bid = null;
					if (AppUtils.isNotBlank(bidders)) {
						bid = bidders.get(0);
						if (bid != null) {
							isDelay = isDelay(auctions, bid);
							curPrice = bid.getPrice().doubleValue();
						}

						if (!isDelay) { // 不需要延迟处理
							int result = timerAuctionsService.finishAuction(auctions.getId());
							if (result > 0 && bid != null) {
								BiddersWin biddersWin = new BiddersWin();
								biddersWin.setAId(auctions.getId());
								biddersWin.setBidId(bid.getId());
								biddersWin.setProdId(auctions.getProdId());
								biddersWin.setSkuId(auctions.getSkuId());
								biddersWin.setUserId(bid.getUserId());
								biddersWin.setBitTime(new Date());
								biddersWin.setPrice(bid.getPrice());
								biddersWin.setStatus(BiddersWinStatusEnum.NOORDER.value());
								Long bidId = timerBiddersWinService.saveBiddersWin(biddersWin);
								XxlJobLogger.log("<!--- biddersWin id = {}", bidId);
								processOrder++;
							}
							
							int crowdWatch = biddersService.getCrowdWatchAuctions(auctions.getId());
							long bidCount = biddersService.findBidsCount(auctions.getId());
							// 更新拍卖活动数据
							timerAuctionsService.updateAuctions(crowdWatch, bidCount, curPrice, auctions.getEndTime(), auctions.getId());
						}
					}else { //没有出价记录
						timerAuctionsService.finishAuction(auctions.getId());
					}
				}
			}
			
			XxlJobLogger.log(" generate biddersWin end -->");
			return new ReturnT<>(200, "已经处理的订单数: " + processOrder);
	}
	
	
	/**
	 * 判断是否要延迟处理.
	 *
	 * @param auctions
	 *            the auctions
	 * @param bid
	 *            the bid
	 * @return true, if checks if is delay
	 */
	private boolean isDelay(Auctions auctions, Bidders bid) {
		if (auctions.getIsSecurity().intValue() == 1 && auctions.getDelayTime().intValue() > 0) { // 需要保证金
			if(AppUtils.isNotBlank(auctions.getFixedPrice())){
				if(bid.getPrice().doubleValue()>=auctions.getFixedPrice().doubleValue()){
					return false;
				}
			}
			Date _endTime = auctions.getEndTime();
			Date bitTime = bid.getBitTime();
			long[] diff = getDistanceTimes(_endTime, bitTime);
			long day = diff[0];
			long hour = diff[1];
			long min = diff[2];
			long sec = diff[3];
			if (day == 0 && hour == 0) {
				if ((min > 0 && min <= 2) || (min == 0 && sec > 0)) { // 说明是竞拍结束的前两分钟出价或者
																		// 结束的前几秒进行出价
					// 需要延迟处理
					return true;
				}
			}
		}
		return false;
	}
	
	
	/**
	 * 两个时间相差距离多少天多少小时多少分多少秒.
	 *
	 * @param one
	 *            活动过期时间
	 * @param two
	 *            中标时间
	 * @return long[] 返回值为：{天, 时, 分, 秒}
	 */
	public static long[] getDistanceTimes(Date one, Date two) {
		long day = 0;
		long hour = 0;
		long min = 0;
		long sec = 0;
		long time1 = one.getTime();
		long time2 = two.getTime();
		long diff;
		/*
		 * if(time1<time2) { diff = time2 - time1; } else { diff = time1 -
		 * time2; }
		 */
		diff = time1 - time2;
		day = diff / (24 * 60 * 60 * 1000);
		hour = (diff / (60 * 60 * 1000) - day * 24);
		min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
		sec = (diff / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
		long[] times = { day, hour, min, sec };
		return times;
	}
	
}
