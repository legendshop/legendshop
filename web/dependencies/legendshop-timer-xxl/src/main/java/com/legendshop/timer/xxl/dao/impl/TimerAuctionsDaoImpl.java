/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.constant.AuctionsStatusEnum;
import com.legendshop.model.entity.Auctions;
import com.legendshop.timer.xxl.dao.TimerAuctionsDao;

/**
 * 拍卖活动Dao实现类
 */
@Repository
public class TimerAuctionsDaoImpl extends GenericDaoImpl<Auctions, Long> implements TimerAuctionsDao {

	/**
	 * 查找活动
	 */
	public List<Auctions> getAuctionsBystatus(Long firstId, Long lastId, Long status) {
		return queryByProperties(new EntityCriterion().ge("id", firstId).lt("id", lastId).eq("status", status).eq("isSecurity", 1l));
	}

	/**
	 * 获取拍卖活动
	 */
	public Auctions getAuctions(Long id) {
		return getById(id);
	}

	/**
	 * 删除拍卖活动 
	 */
	public int deleteAuctions(Auctions auctions) {
		return delete(auctions);
	}

	/**
	 * 保存拍卖活动 
	 */
	public Long saveAuctions(Auctions auctions) {
		return save(auctions);
	}

	/**
	 * 更新拍卖活动 
	 */
	public int updateAuctions(Auctions auctions) {
		return update(auctions);
	}

	/**
	 * 完成拍卖 
	 */
	@Override
	public int finishAuction(Long id) {
		return update("update ls_auctions set status=2 where id=? and status=1 ", id);
	}

	/**
	 * 查询所有在线并且过期的活动
	 */
	@Override
	public List<Auctions> getonlineList() {
		return this.queryByProperties(new EntityCriterion().eq("status", AuctionsStatusEnum.ONLINE.value()).gt("endTime", new Date()));
	}

	/**
	 * 获取最小id 
	 */
	@Override
	public Long getMinId() {
		return this.get("select min(id) from ls_auctions where status=? and flag_status=0", Long.class, AuctionsStatusEnum.FINISH.value());
	}

	/**
	 * 获取最大id 
	 */
	@Override
	public Long getMaxId() {
		return this.get("select max(id) from ls_auctions where status=? and flag_status=0", Long.class, AuctionsStatusEnum.FINISH.value());
	}
	
	/**
	 * 查询所有在线并且过期的活动 
	 */
	@Override
	public List<Auctions> getOvertimeAuctionsBystatus(Long status, Date now) {
		return this.queryByProperties(new EntityCriterion().eq("status", status).lt("endTime", now));
	}
	
}
