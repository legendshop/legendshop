/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.model.entity.AuctionDepositRec;
import com.legendshop.timer.xxl.dao.TimerAuctionDepositRecDao;
import com.legendshop.timer.xxl.service.TimerAuctionDepositRecService;
import com.legendshop.util.AppUtils;

/**
 * 保证金服务实现类
 * 
 */
@Service("timerAuctionDepositRecService")
public class TimerAuctionDepositRecServiceImpl  implements TimerAuctionDepositRecService{
    
	@Autowired
	private TimerAuctionDepositRecDao timerAuctionDepositRecDao;

	/**
     * 获取保证金 
     */
    public AuctionDepositRec getAuctionDepositRec(Long id) {
        return timerAuctionDepositRecDao.getAuctionDepositRec(id);
    }

    /**
     * 删除保证金 
     */
    public void deleteAuctionDepositRec(AuctionDepositRec auctionDepositRec) {
    	timerAuctionDepositRecDao.deleteAuctionDepositRec(auctionDepositRec);
    }

    /**
     * 保存保证金 
     */
    public Long saveAuctionDepositRec(AuctionDepositRec auctionDepositRec) {
        if (!AppUtils.isBlank(auctionDepositRec.getId())) {
            updateAuctionDepositRec(auctionDepositRec);
            return auctionDepositRec.getId();
        }
        return timerAuctionDepositRecDao.saveAuctionDepositRec(auctionDepositRec);
    }

    /**
     * 更新保证金 
     */
    public void updateAuctionDepositRec(AuctionDepositRec auctionDepositRec) {
    	timerAuctionDepositRecDao.updateAuctionDepositRec(auctionDepositRec);
    }

    /**
     * 判断支付环境是否安全 
     */
	@Override
	public boolean isPaySecurity(String userId, Long paimaiId) {
		return timerAuctionDepositRecDao.isPaySecurity(userId, paimaiId);
	}

	/**
	 * 查询报名人数 
	 */
	@Override
	public Long  queryAccess(Long paimaiId) {
		return timerAuctionDepositRecDao.queryAccess(paimaiId);
	}

	/**
	 * 根据id获取保证金 
	 */
	@Override
	public List<AuctionDepositRec> getAuctionDepositRecByAid(Long aId) {
		return timerAuctionDepositRecDao.getAuctionDepositRecByAid(aId);
	}

	/**
	 * 查询那些中标并且支付过订单的 没有处理过退款操作的用户 保证金记录 
	 */
	@Override
	public List<AuctionDepositRec> getBidWinsNoFlag() {
		return timerAuctionDepositRecDao.getBidWinsNoFlag();
	}
}
