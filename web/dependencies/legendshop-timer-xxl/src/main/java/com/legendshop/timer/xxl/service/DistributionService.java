/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.service;

import com.legendshop.model.entity.SubItem;

/**
 * 分佣金服务
 */
public interface DistributionService {
	
	/** 订单佣金结算 */
	public  void settleDistribution(SubItem subItem);

}
