package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.model.constant.ScheduleTypeEnum;
import com.legendshop.model.entity.Group;
import com.legendshop.model.entity.ScheduleList;
import com.legendshop.spi.service.GroupService;
import com.legendshop.spi.service.ScheduleListService;
import com.legendshop.util.AppUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 * 过期团购未成团原路退款定时计划任务
 */
@JobHandler(value="groupFinishRefundDepositJob")
@Component
public class GroupFinishRefundDepositJob extends IJobHandler {
	
	@Autowired
	private ScheduleListService scheduleListService;
	
	@Autowired
	private GroupService groupService;
	
	@Override
	public ReturnT<String> execute(String param) throws Exception {
		
		XxlJobLogger.log(" ######################### 自动执行过期团购未成团原路退款任务定时任务  ###########################");
		//查询
		Date today = new Date();
		
		/** 查询今天能执行的 过期团购未成团原路退款 定时计划任务列表 */
		List<ScheduleList> scheduleLists = scheduleListService.getNonexecutionScheduleList(today,ScheduleTypeEnum.TUTO_RETURN_GROUP.value());
		
		if(AppUtils.isBlank(scheduleLists)){
			return new ReturnT<>(200,"没有任何需要处理的活动信息");	
		}
		
		XxlJobLogger.log("######### 取消未成团成功的团购活动  ########");
		
		for(ScheduleList schedule:scheduleLists ){
			
			Group group  = groupService.getGroup(Long.valueOf(schedule.getScheduleSn()));
			if(AppUtils.isBlank(group)){
				XxlJobLogger.log(" ######################### scheduleSn={},shopId={} 未找到团购记录 ###########################",schedule.getScheduleSn(),schedule.getShopId());
				scheduleListService.deleteScheduleList(schedule);
				continue;
			}
			groupService.offlineGroup(group);
		}
		
		return new ReturnT<>(200,"处理取消未成团成功的团购活动成功");
	}

}
