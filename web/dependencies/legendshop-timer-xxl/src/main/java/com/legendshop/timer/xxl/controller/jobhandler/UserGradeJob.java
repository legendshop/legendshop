/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.base.log.TimerLog;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.UserGrade;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.spi.service.UserGradeService;
import com.legendshop.util.AppUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 * 用户等级处理,根据用户的消费记录来计算用户等级
 */
@JobHandler(value="userGradeJob")
@Component
public class UserGradeJob extends IJobHandler{
	
	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private UserGradeService userGradeService;

	/**
	 * 根据消费记录 计算用户等级.
	 */
	@Override
	public ReturnT<String> execute(String param) throws Exception {
		// 获得用户总数
		long totalCount = userDetailService.getUserTotalCount();
		TimerLog.info("Calculate User Grade, totalCount= {}", totalCount);
		// 每页条数
		int pageSize = 100;
		// 总页数
		long totalPage = (totalCount + pageSize - 1) / pageSize;
		// 当前页码
		int currPage = 1;

		// 获取等级表的配置
		List<UserGrade> userGrades = userGradeService.queryUserGrades();

		int code = 200;
		while (currPage <= totalPage) {
			List<UserDetail> userDetails = userDetailService.getUserDetailsByPage(currPage, pageSize);
			if (AppUtils.isNotBlank(userDetails)) {
				for (UserDetail userDetail : userDetails) {
					try {
						// 计算每个用户的 等级
						userDetailService.calculateUserGrade(userDetail.getUserId(), userDetail.getGradeId(), userGrades);
					} catch (Exception e) {
						code = 500;
						XxlJobLogger.log("======== calculate User Grade error:{} width userId:{} ========", e, userDetail.getUserId());
					}
				}
			}
			currPage++;
		}

		XxlJobLogger.log("======== calculate User Grade end ========");
		return new ReturnT<>(code, "处理用户数： " + totalCount);
	}
	
}
