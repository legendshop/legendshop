package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.model.constant.RefundReturnStatusEnum;
import com.legendshop.model.constant.ScheduleStatusEnum;
import com.legendshop.model.constant.ScheduleTypeEnum;
import com.legendshop.model.entity.ScheduleList;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.spi.service.ScheduleListService;
import com.legendshop.spi.service.SubRefundReturnService;
import com.legendshop.util.AppUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 * 买家已经退货，等待商家收货，时限为自买家填写物流单后起7天，超时系统将自动执行商家确认收货。
 * 退款单更新为卖家已收货。
 */
@JobHandler(value = "autoConfirmReceiptJob")
@Component
public class AutoConfirmReceiptJob extends IJobHandler {

	@Autowired
	private ScheduleListService scheduleListService;
	
	@Autowired
	private SubRefundReturnService subRefundReturnService;
	
    @Override
    public ReturnT<String> execute(String param) throws Exception {
    	
        XxlJobLogger.log(" ######################### 自动执行商家确认收货  ###########################");
		//查询
		Date today = new Date();
		
		List<ScheduleList> scheduleLists = scheduleListService.getNonexecutionScheduleList(today,ScheduleTypeEnum.AUTO_7_GOODS_REFUND.value());
		if (AppUtils.isBlank(scheduleLists)) {
			XxlJobLogger.log("######################### 没有要同意申请的任务 #########################");
			return SUCCESS;
		}
		
		XxlJobLogger.log(" ######################### 执行商家确认收货任务列表 ###########################");
		for (ScheduleList schedule : scheduleLists) {
			XxlJobLogger.log(" ######################### schedule={} ###########################", schedule.getId());
			String scheduleSn = schedule.getScheduleSn();
			Long shopId = schedule.getShopId(); 
			XxlJobLogger.log(" ######################### scheduleSn={},shopId={} ###########################",scheduleSn,shopId);
		    /**
		     * 1.查询该订单,若不存在,则删除该定时记录
		     */
			SubRefundReturn subRefun= subRefundReturnService.getSubRefundReturnByRefundSn(scheduleSn);
			if(AppUtils.isBlank(subRefun)){
				XxlJobLogger.log(" ######################### scheduleSn={},shopId={} 未找到订单记录 ###########################",scheduleSn,shopId);
				scheduleListService.deleteScheduleList(schedule);
				continue;
			}
			/**
		     * 2.查询该订单,是否处于待收货状态,若不是则删除该定时记录
		     */
			if(RefundReturnStatusEnum.SELLER_AGREE.value()!=subRefun.getSellerState() && RefundReturnStatusEnum.LOGISTICS_WAIT_RECEIVE.value()!=subRefun.getGoodsState()){
				XxlJobLogger.log(" ######################### scheduleSn={},shopId={} 订单已经审核  ###########################",scheduleSn,shopId);
				scheduleListService.deleteScheduleList(schedule);
				continue;
			}
			
			
			XxlJobLogger.log(" ######################### 执行商家确认收货   ###########################");
			subRefun.setReceiveMessage("超时未确认系统自动同意");
			subRefun.setReceiveTime(today);
			subRefun.setGoodsState(RefundReturnStatusEnum.LOGISTICS_RECEIVED.value());
			subRefun.setApplyState(RefundReturnStatusEnum.APPLY_WAIT_ADMIN.value());// 待管理员审核
			subRefundReturnService.updateSubRefundReturn(subRefun);
		
			scheduleListService.updateScheduleListStatus(shopId, ScheduleStatusEnum.EXECUTED.value(), schedule.getId());
		}
		XxlJobLogger.log(" ######################### 执行商家确认收货任务列表  ###########################");
        return SUCCESS;
    }
}
