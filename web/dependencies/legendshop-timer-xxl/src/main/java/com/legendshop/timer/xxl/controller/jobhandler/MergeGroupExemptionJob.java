package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.spi.service.MergeGroupAddService;
import com.legendshop.spi.service.MergeGroupService;
import com.legendshop.util.AppUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 * 拼团活动-团长免单
 * @author Tony
 *
 */
@JobHandler(value="mergeGroupExemptionJob")
@Component
public class MergeGroupExemptionJob extends IJobHandler {
	
	@Autowired
	private MergeGroupService mergeGroupService;
	
	@Autowired
	private MergeGroupAddService mergeGroupAddService;

	@Override
	public ReturnT<String> execute(String param) throws Exception {
		XxlJobLogger.log(" ######################### 自动执行拼团活动团长免单操作定时任务  ###########################");
		
		/**
		 *  查询已经拼团成功并且是团长免单的拼团记录; 
		 */
		List<Long> ids =mergeGroupAddService.findGroupExemption();
		if(AppUtils.isBlank(ids)){
			XxlJobLogger.log("没有任何需要处理的团长免单信息");
			return new ReturnT<>(200, "没有任何需要处理的团长免单信息");	
		}
		
		mergeGroupService.mergeGroupExemption(ids);
		
		return new ReturnT<>(200, "需要处理的团长免单成功");
	}

}
