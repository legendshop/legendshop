/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.Auctions;

import java.util.Date;
import java.util.List;

/**
 * 拍卖活动Dao
 */
public interface TimerAuctionsDao extends GenericDao<Auctions, Long> {

	/** 根据状态获取拍卖活动列表 */
	public abstract List<Auctions> getAuctionsBystatus(Long firstId, Long lastId, Long status);
	
	/** 查询所有在线并且过期的活动 */
	public abstract List<Auctions> getOvertimeAuctionsBystatus(Long status, Date now);

	/** 获取拍卖活动 */
	public abstract Auctions getAuctions(Long id);

	/** 删除拍卖活动 */
	public abstract int deleteAuctions(Auctions auctions);

	/** 保存拍卖活动 */
	public abstract Long saveAuctions(Auctions auctions);

	/** 更新拍卖活动 */
	public abstract int updateAuctions(Auctions auctions);

	/** 完成拍卖 */
	public abstract int finishAuction(Long id);

	/** 查询所有在线并且不过期的活动 */
	public abstract List<Auctions> getonlineList();

	/** 获取最小id */
	public abstract Long getMinId();

	/** 获取最大id */
	public abstract Long getMaxId();

}
