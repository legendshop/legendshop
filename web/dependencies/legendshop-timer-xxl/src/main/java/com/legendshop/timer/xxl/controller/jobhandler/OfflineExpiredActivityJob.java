package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.model.entity.Marketing;
import com.legendshop.spi.service.MarketingProdsService;
import com.legendshop.spi.service.MarketingService;
import com.legendshop.util.AppUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 *定时处理过期的促销活动
 */
@JobHandler(value = "offlineExpiredActivityJob")
@Component
public class OfflineExpiredActivityJob extends IJobHandler {

	@Autowired
	private MarketingService marketingService;
	
	@Autowired
	private MarketingProdsService marketingProdsService;
	
	private  Integer commitInteval = 100;//取100条
	
	
    @Override
    public ReturnT<String> execute(String param) throws Exception {
        XxlJobLogger.log("=========== 处理过期促销活动   ===============");
        Date now = new Date();//当前时间
        long processNum = 0;  //执行的条数
        
		boolean haveValue = true;
		while (haveValue) {
			//获取已过期的促销活动
	        List<Marketing> expiredList = marketingService.getExpiredMarketing(commitInteval,now);
	        XxlJobLogger.log("待处理过期促销活动个数： " + expiredList.size());
	        if (AppUtils.isBlank(expiredList)) {
				haveValue = false;
				XxlJobLogger.log("暂时没有要处理的促销活动");
				return SUCCESS;
			} else {
				processNum = processNum + expiredList.size(); 
				//处理逻辑：把过期活动标记，下线，释放商品	
				for (Marketing marketing : expiredList) {
					XxlJobLogger.log("=========== 处理过期促销活动   marketingId = {} ===============", marketing.getId());
					//标记，下线
					marketing.setState(0);
					marketing.setIsExpired(1);
					marketingService.updateMarketing(marketing);
					//释放商品
					Integer result = marketingProdsService.deleteByMarketId(marketing.getId());
					XxlJobLogger.log("=========== 处理过期促销活动 result = {} ===============", result > 0);
				}
			}
		}
		XxlJobLogger.log("处理的促销活动个数： " + processNum);
        return SUCCESS;
    }
}
