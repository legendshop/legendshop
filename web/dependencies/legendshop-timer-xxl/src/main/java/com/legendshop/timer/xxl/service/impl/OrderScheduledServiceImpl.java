/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.service.impl;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.log.TimerLog;
import com.legendshop.util.DateUtils;
import com.legendshop.business.dao.ProductDao;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.SubHistoryEnum;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubHistory;
import com.legendshop.spi.service.OrderScheduledService;
import com.legendshop.spi.service.OrderService;
import com.legendshop.spi.service.SubHistoryService;
import com.legendshop.spi.service.SubService;
import com.legendshop.util.AppUtils;

/**
 * 订单定时器服务实现类
 */
@Service("orderScheduledService")
public class OrderScheduledServiceImpl implements OrderScheduledService {
	
	@Autowired
	private SubService subService;
	
	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private SubHistoryService subHistoryService;
	
	
	/**
	 * 结束超时不付费的订单.
	 *
	 * @param list the list
	 */
	@Override
	public void finishUnPay(List<Sub> list) {
		
		TimerLog.info("<! ---- finishUnPay Order starting ");
		//当前时间
		Date now = new Date();
		StringBuilder idStr=new StringBuilder();
		for (Iterator<Sub> iterator = list.iterator(); iterator.hasNext();) {
			Sub sub = (Sub) iterator.next();
			if(AppUtils.isNotBlank(sub) ){

				String timeStr = DateUtils.format(now, DateUtils.PATTERN_CLASSICAL);
				String historyReason = "于" + timeStr + "系统自动取消订单";
				String subReason = "系统自动取消订单";

				sub.setStatus(OrderStatusEnum.CLOSE.value());
				sub.setCancelReason(subReason);
				boolean result=subService.cancleOrder(sub);
				if(result){ //如果该订单取消成功
					/*
					 * 记录订单历史
					 */
					SubHistory subHistory=new SubHistory();
					subHistory.setRecDate(now);
					subHistory.setSubId(sub.getSubId());
					subHistory.setStatus(SubHistoryEnum.ORDER_OVER_TIME.value());
					subHistory.setUserName("系统自动");
					subHistory.setReason(historyReason);
					idStr.append(sub.getSubId()).append(",");
					subHistoryService.saveSubHistory(subHistory);
					//清除缓存
					orderService.cleanOrderCache(sub.getUserId(), sub.getShopId());
				}
				
			}
		}
		TimerLog.info("finishUnPay sub id by {} ",idStr.toString());
		TimerLog.info("finishUnPay Order success --->");

	}


	/**
	 * 自动确认收货.
	 *
	 * @param list the list
	 */
	@Override
	public void finishUnAcklodge(List<Sub> list) {
		TimerLog.info("<!--- finishUnAcklodge starting");
		String sql=" update ls_sub set status=? , update_date = ?,finally_date=?  WHERE sub_id=? and status=3 "; //status = 3的含义是：CONSIGNMENT 发货
		StringBuilder idStr=new StringBuilder();
		Date now = new Date();
		for (Iterator<Sub> iterator = list.iterator(); iterator.hasNext();) {
			Sub sub = (Sub) iterator.next();
			if(AppUtils.isNotBlank(sub)){
				int count= productDao.update(sql, new Object[]{OrderStatusEnum.SUCCESS.value(),now,now,sub.getSubId()});
				if(count>0){
					/*
					 * 记录订单历史
					 */
					SubHistory subHistory=new SubHistory();
					String time = subHistory.DateToString(now);
					subHistory.setRecDate(now);
					subHistory.setSubId(sub.getSubId());
					subHistory.setStatus(SubHistoryEnum.TAKE_DELIVER_TIME.value());
					StringBuilder sb=new StringBuilder();
					sb.append("于").append(time).append("系统自动确认收货");
					subHistory.setUserName("系统自动");
					subHistory.setReason(sb.toString());
					
					idStr.append(sub.getSubId()).append(",");
					
					subHistoryService.saveSubHistory(subHistory);
					
					//清除缓存
					orderService.cleanOrderCache(sub.getUserId(), sub.getShopId());
				}
			}
		}
		TimerLog.info("finishUnAcklodge sub id by {} ",idStr.toString());
		TimerLog.info("finishUnAcklodge success ----!> ");
	}

	/* (non-Javadoc)
	 * @see com.legendshop.spi.service.OrderScheduledService#finishUnAcklodgeNotice()
	 */
	@Override
	public void finishUnAcklodgeNotice() {
	}
}
