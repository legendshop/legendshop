/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.model;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import com.legendshop.business.dao.SubItemDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.model.entity.SubItem;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 统计---- 所有的分销单
 * 
 */
public class GenerateWorkerDistSubItemThread implements Callable<Boolean> {

	// 一个同步辅助类，它允许一组线程互相等待，直到到达某个公共屏障点 (common barrier point)
	// private CyclicBarrier barrier;

	/** The shop bill count. */
	private ShopBillCount shopBillCount;

	private SubItemDao subItemDao;

	private UserDetailDao userDetailDao;

	private Long shopId;

	private Date endDate;

	@Override
	public Boolean call() throws Exception {
		try {
			/**
			 * 查询当前分期的所有的分销订单
			 */
			List<SubItem> distSubItems = subItemDao.getShopDistSubItemList(shopId, endDate);
			if (AppUtils.isBlank(distSubItems)) {
				return true;
			}

			// 分销佣金总额
			double distCommisTotals = 0;
			for (SubItem subItem : distSubItems) {
				boolean needDist = true;
				// 0:没发起过退款,1:在处理,2:处理完成,-1:不同意
				long refund_state = subItem.getRefundState();
				if (refund_state == 1 || refund_state == 2) {
					needDist = false;
				}
				// 没有退货，或者退货不成功，则商城需要支付分销佣金
				if (needDist) {
					// 一级用户分佣
					if (AppUtils.isNotBlank(subItem.getDistUserName()) && AppUtils.isNotBlank(subItem.getDistUserCommis())) {
						UserDetail userDetail = userDetailDao.getUserDetailByName(subItem.getDistUserName());
						if (AppUtils.isNotBlank(userDetail)) {
							// 没有退货，或者退货不成功，则商城需要支付分销佣金
							distCommisTotals = Arith.add(distCommisTotals, subItem.getDistUserCommis());
						}
					}

					// 二级用户分佣
					if (AppUtils.isNotBlank(subItem.getDistSecondName()) && AppUtils.isNotBlank(subItem.getDistSecondCommis())) {
						UserDetail userDetail = userDetailDao.getUserDetailByName(subItem.getDistSecondName());
						if (AppUtils.isNotBlank(userDetail)) {
							// 没有退货，或者退货不成功，则商城需要支付分销佣金
							distCommisTotals = Arith.add(distCommisTotals, subItem.getDistSecondCommis());
						}
					}

					// 三级用户分佣
					if (AppUtils.isNotBlank(subItem.getDistThirdName()) && AppUtils.isNotBlank(subItem.getDistThirdCommis())) {
						UserDetail userDetail = userDetailDao.getUserDetailByName(subItem.getDistThirdName());
						if (AppUtils.isNotBlank(userDetail)) {
							// 没有退货，或者退货不成功，则商城需要支付分销佣金
							distCommisTotals = Arith.add(distCommisTotals, subItem.getDistThirdCommis());
						}
					}
				} else {
					// 正在维权过程中的退款单记录起来;
				}
				subItem = null;
			}
			shopBillCount.setDistCommisTotals(distCommisTotals);
			distSubItems = null;
			// barrier.await(2,TimeUnit.SECONDS); //两分钟之内
			// 阻塞一直等待执行完成拿到结果，如果在超时时间内，没有拿到则抛出异常
			return true;
		} catch (Exception e) {
			throw e;

		}
	}

	public GenerateWorkerDistSubItemThread(ShopBillCount shopBillCount, SubItemDao subItemDao, UserDetailDao userDetailDao, Long shopId, Date endDate) {
		super();
		// this.barrier = barrier;
		this.shopBillCount = shopBillCount;
		this.subItemDao = subItemDao;
		this.userDetailDao = userDetailDao;
		this.shopId = shopId;
		this.endDate = endDate;
	}

}
