/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.timer.xxl.model;

import com.legendshop.util.constant.StringEnum;

/**
 *  商家结算周期.
 */
public enum BillPeriodEnum implements StringEnum{
      
      /** 月结. */
	MONTH("MONTH"),
	
	/** 按周结算. */
	WEEK("WEEK"),
	
	/** 按天结算， T+n, n在使用端定义. */
	DAY("DAY");
	
	private final String value;

	private BillPeriodEnum(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

	/**
	 * 根据字符串获取对应的实例
	 * @return
	 */
	public static BillPeriodEnum instance(String value) {
		BillPeriodEnum[] enums = values();
		for (BillPeriodEnum enum1 : enums) {
			if (enum1.value().equals(value)) {
				return enum1;
			}
		}
		return null;
	}
	
}
