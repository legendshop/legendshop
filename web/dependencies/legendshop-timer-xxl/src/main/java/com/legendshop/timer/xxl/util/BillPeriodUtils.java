package com.legendshop.timer.xxl.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.util.DateUtils;
import com.legendshop.timer.xxl.model.BillPeriodEnum;
import org.springframework.stereotype.Component;

/**
 * 结算周期工具类
 * @author 开发很忙
 */
@Component("billPeriodUtils")
public class BillPeriodUtils {
	
	
	@Autowired
	private  SystemParameterUtil systemParameterUtil;
	
	/**
	 * 判断是否合适的时间去执行定时器，按月，周，日3种情况进行判断
	 * @return
	 */
	public boolean isCanBeSettle() {
		
		//获取当前结算的周期类型
		String shopBillPeriodType = systemParameterUtil.getShopBillPeriodType();
			
		BillPeriodEnum type = BillPeriodEnum.instance(shopBillPeriodType);
		
		if (BillPeriodEnum.DAY.equals(type)) {// 日结, 无法判断什么时候开始执行，所以只要定时器一执行就生效
			return true;
		} else if (BillPeriodEnum.WEEK.equals(type)) {// 周结
			
			// 按周计算的话以周一为开始，周一时执行定时器才能继续往下执行，在settlementAnyDay
			Calendar c = Calendar.getInstance();
			int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
			return Calendar.MONDAY == dayOfWeek; 
			
		} else {// 月结 找出结算月的第一天作为上期结算的最后一天，以自然月来计算
			Date now = new Date();
			DateFormat format = new java.text.SimpleDateFormat("dd");
			Integer shopBillDay = systemParameterUtil.getShopBillDay();
			
			if (shopBillDay == null) {
				shopBillDay = 7; // 默认是第7天进行月结
			}
			Integer targetDay = Integer.parseInt(format.format(now));
			return targetDay.equals(shopBillDay);
		}
	}
	
	/**
	 * 获取结算的结束时间
	 * @return上期结算的终止日期
	 */
	public Date getEndDate() {
		Date endDate = null;
		
		//获取当前结算的周期类型
		BillPeriodEnum type = BillPeriodEnum.instance(systemParameterUtil.getShopBillPeriodType());
		int settleInDays = 1;//用于决定日结是每多少天结算一次, 默认是一天
		
		if (BillPeriodEnum.DAY.equals(type)) {// 日结
			Date myDate = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(myDate);
			cal.add(Calendar.DATE, -settleInDays);
			// 将时分秒,毫秒域清零
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			endDate = cal.getTime();
		} else if (BillPeriodEnum.WEEK.equals(type)) {// 周结
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // 设置时间格式
			Calendar cal = Calendar.getInstance();
			cal.setFirstDayOfWeek(Calendar.MONDAY);// 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
			int day = cal.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天
			int intevalDays = cal.getFirstDayOfWeek() - day;
			if (intevalDays > 0) {
				intevalDays = intevalDays - 7; // 确保是上一周
			}
			cal.add(Calendar.DATE, intevalDays);// 根据日历的规则，给当前日期减去星期几与一个星期第一天的差值
			try {
				endDate = sdf.parse(sdf.format(cal.getTime()));
			} catch (ParseException e) {
				throw new BusinessException("date format exception");
			}
		} else {// 月结 找出结算月的第一天作为上期结算的最后一天，以自然月来计算
			DateFormat df = new SimpleDateFormat("yyyy-MM");
			try {
				endDate = df.parse(df.format(new Date()));
			} catch (ParseException e) {
				throw new BusinessException("date format exception");
			}
		}
		
		endDate = DateUtils.getIntegralEndTime(endDate);
		
		return endDate;
	}

	/**
	 * 获取结算的开始时间
	 */
	public Date getStartDate(Date endDate) {
		
		Date startDate = null;
		
		//获取当前结算的周期类型
		BillPeriodEnum type = BillPeriodEnum.instance(systemParameterUtil.getShopBillPeriodType());
		int settleInDays = 1;//用于决定日结是每多少天结算一次, 默认是一天
				
		if (BillPeriodEnum.DAY.equals(type)) {// 日结
			// 往前推n天
			Calendar c = Calendar.getInstance();
			c.setTime(endDate);
			c.add(Calendar.DATE, -settleInDays);
			startDate = c.getTime();
		} else if (BillPeriodEnum.WEEK.equals(type)) {// 周结
			// 往前推一周
			Calendar c = Calendar.getInstance();
			c.setTime(endDate);
			c.add(Calendar.WEEK_OF_YEAR, -1);
			startDate = c.getTime();

		} else {// 月结
			// 往前推一个月
			Calendar c = Calendar.getInstance();
			c.setTime(endDate);
			c.add(Calendar.MONTH, -1);
			return c.getTime();
		}
		
		startDate = DateUtils.getIntegralStartTime(endDate);
		
		return startDate;
	}
}
