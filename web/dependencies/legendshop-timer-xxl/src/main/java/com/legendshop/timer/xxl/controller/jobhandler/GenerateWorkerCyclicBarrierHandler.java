/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.controller.jobhandler;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.business.dao.auction.AuctionDepositRecDao;
import com.legendshop.business.dao.presell.PresellSubDao;
import com.legendshop.timer.xxl.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.dao.SubDao;
import com.legendshop.business.dao.SubItemDao;
import com.legendshop.business.dao.SubRefundReturnDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.constant.ShopOrderBillStatusEnum;
import com.legendshop.model.dto.ShopDetailTimerDto;
import com.legendshop.model.entity.ShopBillPeriod;
import com.legendshop.model.entity.ShopOrderBill;
import com.legendshop.spi.service.ShopOrderBillService;
import com.legendshop.spi.service.SubService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 利用屏障多任务计算最后合并处理结果.
 *
 * @author
 */
@Service("generateWorkerCyclicBarrierHandler")
public class GenerateWorkerCyclicBarrierHandler {

	@Autowired
	private ShopOrderBillService shopOrderBillService;
	
	@Autowired
	private SubService subService;

	@Autowired
	private SubRefundReturnDao subRefundReturnDao;

	@Autowired
	private SubDao subDao;

	@Autowired
	private UserDetailDao userDetailDao;

	@Autowired
	private SubItemDao subItemDao;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Autowired
	private PresellSubDao presellSubDao;

	@Autowired
	private AuctionDepositRecDao auctionDepositRecDao;
	
	/**
	 * 生成结算单，结算的时间 为上一个自然月 例如当前为 12月份，则结算时间为 大于等于11月1日 0点 小于 12月1日 0 点.
	 *
	 * @param shopDetail
	 * @param shopBillPeriod
	 */
	public void generateShopBill(ShopDetailTimerDto shopDetail, ShopBillPeriod shopBillPeriod, Date startDate, Date endDate) {
		Long shopId = shopDetail.getShopId();
		if (AppUtils.isBlank(shopId)) {
			return;
		}
		// 用于计算统计
		ShopBillCount shopBillCount = new ShopBillCount();
		int N = 5;
		ExecutorService exec = Executors.newFixedThreadPool(N);
		CompletionService<Boolean> execcomp = new ExecutorCompletionService<Boolean>(exec);
		try {
			/* 统计计算 所有已完成的订单 并且没有进行退换货的订单 */
			execcomp.submit(new GenerateWorkerSubThread(shopBillCount, subDao, shopId, endDate));
			/* 统计计算 所有已完成的退货单 */
			execcomp.submit(new GenerateWorkerProdReturnsThread(shopBillCount,subService, subRefundReturnDao, shopId, endDate));
			/* 统计计算 当前分期的所有的分销订单 */
			execcomp.submit(new GenerateWorkerDistSubItemThread(shopBillCount, subItemDao, userDetailDao, shopId, endDate));
			/* 统计计算 当前分期的所有的预售订单（结算预售定金） */
			execcomp.submit(new GenerateWorkerPresellSubThread(shopBillCount, presellSubDao, shopId, endDate));
			/* 统计计算 当前分期的所有的拍卖保证金（结算拍卖保证金） */
			execcomp.submit(new GenerateWorkerAuctionDepositrecThread(shopBillCount, auctionDepositRecDao, shopId));


		} catch (Throwable e) {
			exec.shutdown(); // 平滑关闭线程服务
		} finally {
			exec.shutdown(); // 平滑关闭线程服务
		}

		/*
		 * 获取线程结果是否处理成功 如果没有成功则不处理
		 */
		boolean config = true;
		for (int i = 0; i < N; i++) {
			// 检索并移除表示下一个已完成任务的 Future，如果目前不存在这样的任务，则等待。
			Future<Boolean> future = null;
			try {
				future = execcomp.take();
				// 说明有一个任务失败了,从而取消其他任务执行
				if (!config) {
					future.cancel(true);
				}else {

					// 两分钟之内，阻塞一直等待执行完成拿到结果，如果在超时时间内，没有拿到则抛出异常
					boolean result = future.get(2, TimeUnit.MINUTES);
					if (!result) {
						config = false;
						continue;
					}
				}
			} catch (Exception e) {
				config = false;
				e.printStackTrace();
				if (future != null) {
					future.cancel(true);
				}
			}
		}

		if (!config){
			return;
		}
		/*
		 * 处理统计信息
		 */
		// 订单金额(含运费)
		double orderAmount = shopBillCount.getOrderAmount();
		// 使用的红包总额
		double redpackOffPrice = shopBillCount.getRedpackOffPrice();
		// 运费总额
		double shippingTotals = shopBillCount.getShippingTotals();
		// 退单金额
		double orderReturnTotals = shopBillCount.getOrderReturnTotals();
		// 退单红包金额
		double orderReturnRedpackOffPriceTotals = shopBillCount.getOrderReturnRedpackOffPriceTotals();
		// 预售定金总额
		double preDepositPriceTotal = shopBillCount.getPreDepositPriceTotal();
		// 分销佣金总额
		double distCommisTotals = shopBillCount.getDistCommisTotals();
		// 拍卖保证金总额
		double auctionDepositTotal = shopBillCount.getAuctionDepositTotal();

		// 应结金额 :订单金额(含运费) - 退款金额 - 平台佣金 - 分销佣金 + 红包金额 - 退款红包金额 + 预售定金 + 拍卖保证金
		double resultTotalAmount = Arith.sub(orderAmount, orderReturnTotals);
		//平台佣金  取商家的独立 若没有则取平台的公共设置 为0-100  需要除以100
		double commisrate = 0.0;
		if(AppUtils.isBlank(shopDetail.getCommissionRate())){
			commisrate = systemParameterUtil.getShopCommisRate();
		}else{
			commisrate = shopDetail.getCommissionRate();
		}
		double commisTotals = Arith.mul(resultTotalAmount, Arith.mul(commisrate, 0.01));
		
		resultTotalAmount = Arith.sub(resultTotalAmount, commisTotals);
		resultTotalAmount = Arith.sub(resultTotalAmount, distCommisTotals);
		resultTotalAmount = Arith.add(resultTotalAmount, redpackOffPrice);
		resultTotalAmount = Arith.sub(resultTotalAmount,orderReturnRedpackOffPriceTotals);
		
		//应得金额结算预售定金
		resultTotalAmount = Arith.add(resultTotalAmount, preDepositPriceTotal);

		//应得金额结算拍卖保证金
		resultTotalAmount = Arith.add(resultTotalAmount, auctionDepositTotal);

		// 计算档期记录 的总值
		shopBillPeriod.setOrderAmount(Arith.add(shopBillPeriod.getOrderAmount(), orderAmount));
		shopBillPeriod.setOrderReturnTotals(Arith.add(shopBillPeriod.getOrderReturnTotals(), orderReturnTotals));
		shopBillPeriod.setCommisTotals(Arith.add(shopBillPeriod.getCommisTotals(), commisTotals));
		shopBillPeriod.setDistCommisTotals(Arith.add(shopBillPeriod.getDistCommisTotals(), distCommisTotals));
		shopBillPeriod.setResultTotalAmount(Arith.add(shopBillPeriod.getResultTotalAmount(), resultTotalAmount));
		shopBillPeriod.setRedpackTotals(Arith.add(shopBillPeriod.getRedpackTotals(), redpackOffPrice));

		// 保存结算单
		ShopOrderBill shopOrderBill = new ShopOrderBill();
		shopOrderBill.setFlag(shopBillPeriod.getFlag());
		shopOrderBill.setSn(generateShopBillSN(shopId));
		shopOrderBill.setStartDate(startDate);
		shopOrderBill.setEndDate(endDate);
		shopOrderBill.setOrderAmount(orderAmount);
		shopOrderBill.setShippingTotals(shippingTotals);
		shopOrderBill.setOrderReturnTotals(orderReturnTotals);
		shopOrderBill.setCommisTotals(commisTotals);
		shopOrderBill.setDistCommisTotals(distCommisTotals);
		shopOrderBill.setCommisReturnTotals(0d);
		shopOrderBill.setResultTotalAmount(resultTotalAmount);
		shopOrderBill.setRedpackTotals(redpackOffPrice);
		shopOrderBill.setCreateDate(new Date());
		shopOrderBill.setMonth(getShopBillMonth());
		shopOrderBill.setStatus(ShopOrderBillStatusEnum.INIT.value());
		shopOrderBill.setShopId(shopId);
		shopOrderBill.setShopName(shopDetail.getSiteName());
		shopOrderBill.setCommisRate(commisrate);
		shopOrderBill.setOrderReturnRedpackTotals(orderReturnRedpackOffPriceTotals);
		shopOrderBill.setPreDepositPriceTotal(preDepositPriceTotal);
		shopOrderBill.setAuctionDepositTotal(auctionDepositTotal);

		// 修改订单是否结算标识;
		List<Long> billSubIds = shopBillCount.getBillSubIds();
		// 处理过的退款单信息
		List<Long> billRefundId = shopBillCount.getBillRefundId();
		//处理过的预售单ID集合
		List<Long> billPresellSubIds = shopBillCount.getBillPresellSubIds();
		// 处理过的拍卖保证金ID集合
		List<Long> billAuctionDepositRecIds = shopBillCount.getBillAuctionDepositRecIds();

		shopOrderBillService.generateShopBill(shopOrderBill, billSubIds, billRefundId,billPresellSubIds,billAuctionDepositRecIds);
		
	}

	/**
	 * 生成结算单号.
	 *
	 * @param shopId
	 * @return the string
	 */
	private synchronized String generateShopBillSN(Long shopId) {
		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyMMddHHmmssSSS");
		String billSN = simpledateformat.format(new Date());
		Random r = new Random();
		String shopIds = shopId.toString();
		int length = shopIds.length();
		if(length>2){
			shopIds = shopIds.substring(length - 2, length);
		}
		billSN = billSN + shopIds + CommonServiceUtil.randomNumeric(r, 3);
		return billSN;
	}

	/**
	 * 获取当前期结算的月份.
	 *
	 * @return the shop bill month
	 */
	public String getShopBillMonth() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, -1);
		return sdf.format(c.getTime());
	}

	public void setShopOrderBillService(ShopOrderBillService shopOrderBillService) {
		this.shopOrderBillService = shopOrderBillService;
	}

	public void setSubDao(SubDao subDao) {
		this.subDao = subDao;
	}

	public void setUserDetailDao(UserDetailDao userDetailDao) {
		this.userDetailDao = userDetailDao;
	}

	public void setSubItemDao(SubItemDao subItemDao) {
		this.subItemDao = subItemDao;
	}

	public void setSubRefundReturnDao(SubRefundReturnDao subRefundReturnDao) {
		this.subRefundReturnDao = subRefundReturnDao;
	}

	public void setSubService(SubService subService) {
		this.subService = subService;
	}

}
