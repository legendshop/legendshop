/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.service;

import com.legendshop.model.entity.AuctionDepositRec;
import com.legendshop.model.entity.BiddersWin;

/**
 * 定期拍卖服务
 */
public interface AcutionScheduledService {

	/** 竞拍失败的用户退款操作 回滚用户的保证金 */
	public boolean bidUserRefund(AuctionDepositRec auctionDepositRec);

	/** 结束24小时之内未转订单的操作 */
	public void finishUnAuctionOrder(BiddersWin biddersWin);

	/** 结束72小时未支付竞拍订单的操作 */
	public void finishUnAucitonPay(BiddersWin biddersWin);

	/** 取消订单 */
	public void cancelAuctionOrder(BiddersWin biddersWin);

}
