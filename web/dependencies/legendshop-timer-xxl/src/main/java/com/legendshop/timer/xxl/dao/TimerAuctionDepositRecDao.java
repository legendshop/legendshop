/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.dao;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.AuctionDepositRec;

import java.util.List;

/**
 * 保证金Dao
 */
public interface TimerAuctionDepositRecDao extends GenericDao<AuctionDepositRec, Long> {

	/** 获取保证金 */
	public abstract AuctionDepositRec getAuctionDepositRec(Long id);

	/** 删除保证金 */
	public abstract int deleteAuctionDepositRec(AuctionDepositRec auctionDepositRec);

	/** 保存保证金 */
	public abstract Long saveAuctionDepositRec(AuctionDepositRec auctionDepositRec);

	/** 更新保证金 */
	public abstract int updateAuctionDepositRec(AuctionDepositRec auctionDepositRec);

	/** 判断支付环境是否安全 */
	public abstract boolean isPaySecurity(String userId, Long paimaiId);

	/** 查询报名人数 */
	public abstract Long queryAccess(Long paimaiId);

	/** 根据id获取保证金 */
	public abstract List<AuctionDepositRec> getAuctionDepositRecByAid(Long aId);

	/** 获取保证金 */
	public abstract AuctionDepositRec getAucitonRec(Long aId, String userId);

	/** 查询那些中标并且支付过订单的 没有处理过退款操作的用户 保证金记录 */
	public abstract List<AuctionDepositRec> getBidWinsNoFlag();

}
