/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.model.constant.BiddersWinStatusEnum;
import com.legendshop.model.entity.BiddersWin;
import com.legendshop.timer.xxl.service.AcutionScheduledService;
import com.legendshop.timer.xxl.service.TimerBiddersWinService;
import com.legendshop.util.AppUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 * 拍卖定时处理 24小时内未成功提交拍卖订单
 * 
 */
@JobHandler(value="acutionFinishUnAuctionOrderJob")
@Component
public class AcutionFinishUnAuctionOrderJob extends IJobHandler {
	
	@Autowired
	private TimerBiddersWinService timerBiddersWinService;
	
	@Autowired
	private AcutionScheduledService auctionScheduledService;
	
	private static Integer expireDate = 24; // 默认是24小时

	private static Integer commitInteval = 10000;// 默认取100条

	/**
	 * 结束24小时之内未转订单的操作
	 */
	@Override
	public ReturnT<String> execute(String param) throws Exception {
		XxlJobLogger.log("<!--- finishUnAuctionOrder begin ");
		Date nowDate = getHour(expireDate);
		/**
		 * 拍卖成功24小时未转订单的记录
		 */

		// 获得起始id
		Long firstId  = timerBiddersWinService.getMinId(BiddersWinStatusEnum.NOORDER.value(), nowDate);
		// 获得结束id
		Long lastId   = timerBiddersWinService.getMaxId(BiddersWinStatusEnum.NOORDER.value(), nowDate);

		int processOrder = 0;

		// 循环取100条数据
		while (firstId <= lastId) {
			Long toId = firstId + commitInteval>lastId?lastId:(firstId + commitInteval);
			List<BiddersWin> biddersWinsList = timerBiddersWinService.getBiddersWin(firstId, toId, commitInteval, BiddersWinStatusEnum.NOORDER.value());
			if (AppUtils.isNotBlank(biddersWinsList)) {

				// 交保证金的中标用户集合
				List<BiddersWin> getCashBiddersWinList = new ArrayList<BiddersWin>();

				// 未交保证金的中标用户集合
				List<BiddersWin> notGetCashBiddersWinList = new ArrayList<BiddersWin>();

				for (BiddersWin biddersWin : biddersWinsList) {

					//是否交了保证金， 防止旧数据引起失败
					if(biddersWin.getIsSecurity() != null) {
						if (biddersWin.getIsSecurity().intValue() == 1) { 
							getCashBiddersWinList.add(biddersWin);
						} else if (biddersWin.getIsSecurity().intValue() == 0 || AppUtils.isBlank(biddersWin.getIsSecurity())) {
							notGetCashBiddersWinList.add(biddersWin);
						}
					}
				}
				// 处理已交保证金的未转订单
				if (AppUtils.isNotBlank(getCashBiddersWinList)) {
					for (Iterator<BiddersWin> iterator = getCashBiddersWinList.iterator(); iterator.hasNext();) {
						BiddersWin biddersWin = (BiddersWin) iterator.next();
						if (AppUtils.isBlank(biddersWin)) {
							continue;
						}
						auctionScheduledService.finishUnAuctionOrder(biddersWin);
					}
				}
				// 处理未交保证金的订单
				if (AppUtils.isNotBlank(notGetCashBiddersWinList)) {
					for (Iterator<BiddersWin> iterator = notGetCashBiddersWinList.iterator(); iterator.hasNext();) {
						BiddersWin biddersWin = (BiddersWin) iterator.next();
						if (AppUtils.isBlank(biddersWin)) {
							continue;
						}
						auctionScheduledService.cancelAuctionOrder(biddersWin);
					}
				}
				
				processOrder = processOrder + biddersWinsList.size();
			}
			firstId = toId+1;
		}

		XxlJobLogger.log(" finishUnAuctionOrder refund end -->");
		return new ReturnT<>(200,"处理订单数：" + processOrder);
	}	

	/**
	 * 得到跟现在若干小时时间，如果为负数则向前推.
	 * 
	 * @param hour
	 * @return the hour
	 */
	private Date getHour(int hour) {
		Date myDate = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(myDate);
		cal.add(Calendar.HOUR_OF_DAY, -hour);
		return cal.getTime();
	}

}
