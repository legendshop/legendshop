/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.model;

import com.legendshop.business.dao.presell.PresellSubDao;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.entity.PresellSub;
import com.legendshop.model.entity.Sub;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * 统计计算 所有已支付定金未支付尾款但尾款支付时间已过且已关闭的订单.
 */
public class GenerateWorkerPresellSubThread implements Callable<Boolean> {

	// 一个同步辅助类，它允许一组线程互相等待，直到到达某个公共屏障点 (common barrier point)
	/** 用于计算统计 */
	private ShopBillCount shopBillCount;

	/** The presellsub dao */
	private PresellSubDao presellSubDao;

	/** 店铺id */
	private Long shopId;

	/** 结束时间 */
	private Date endDate;

	/**
	 * The Constructor.
	 *
	 * @param shopBillCount
	 * @param presellSubDao
	 * @param shopId
	 * @param endDate
	 */
	public GenerateWorkerPresellSubThread(ShopBillCount shopBillCount, PresellSubDao presellSubDao, Long shopId, Date endDate) {
		this.shopBillCount = shopBillCount;
		this.presellSubDao = presellSubDao;
		this.shopId = shopId;
		this.endDate = endDate;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public Boolean call() throws Exception {
		// 查询当前期 所有已支付定金未支付尾款但尾款支付时间已过且已关闭的订单.
		try {

			List<PresellSub> presellSubList = presellSubDao.getBillUnPayFinalPresellOrder(shopId,endDate,OrderStatusEnum.CLOSE.value());

			if (AppUtils.isBlank(presellSubList)) {
				return true;
			}

			// 定金金额
			double preDepositPriceTotal = 0;

			List<Long> billPresellSubIds = new ArrayList<Long>();
			for (PresellSub presellSub : presellSubList) {
				preDepositPriceTotal = Arith.add(preDepositPriceTotal,presellSub.getPreDepositPrice().doubleValue());

				billPresellSubIds.add(presellSub.getSubId());
				presellSub = null;
			}

			shopBillCount.setPreDepositPriceTotal(preDepositPriceTotal);

			shopBillCount.setBillPresellSubIds(billPresellSubIds);
			presellSubList = null;
			// barrier.await(2,TimeUnit.SECONDS); //两分钟之内
			// 阻塞一直等待执行完成拿到结果，如果在超时时间内，没有拿到则抛出异常
			return true;
		} catch (Exception e) {
			throw e;
		}
	}


}
