package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.model.entity.PresellProd;
import com.legendshop.spi.service.PresellProdService;
import com.legendshop.util.AppUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 *定时处理未审核或审核拒绝后导致未能正常上线过期的预售活动
 */
@JobHandler(value = "presellActivityAuditExpiredJob")
@Component
public class PresellActivityAuditExpiredJob extends IJobHandler {

	@Autowired
	private PresellProdService presellProdService;
	
    @Override
    public ReturnT<String> execute(String param) throws Exception {
        XxlJobLogger.log("开始处理未审核或审核拒绝后导致未能正常上线过期的预售活动");
        Date now = new Date();//当前时间
        long processNum = 0;  //执行的条数
        
        //获取未审核或审核拒绝后导致未能正常上线过期的预售活动
		List<PresellProd> presellProds = presellProdService.getAuditExpiredPresellProd(now);
		if(AppUtils.isBlank(presellProds)) {
			 XxlJobLogger.log("没有未审核或审核拒绝后导致未能正常上线过期的预售活动要处理");
			 return SUCCESS;
		}
		
		for (PresellProd presellProd : presellProds) {
			 XxlJobLogger.log("预售活动  presellId = {},开始处理", presellProd.getId());
			 
			 presellProdService.releasePresellProd(presellProd);
			 
			 XxlJobLogger.log("预售活动  presellId = {}, 处理完成", presellProd.getId());
			 processNum++;
		}
		XxlJobLogger.log("处理未审核或审核拒绝后导致未能正常上线过期的预售活动个数： " + processNum);
        return SUCCESS;
    }
}
