/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.model.dto.order.OrderSetMgDto;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.Sub;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.spi.service.SubService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;
/**
 * 结束超时 尚未支付的订单（包括已支付定金但是未支付尾款且支付尾款时间结束的订单）
 * @author linzh
 * @date 2018年12月26日
 */
@JobHandler(value = "finishUnPayOrderJob")
@Component
public class FinishUnPayOrderJob extends IJobHandler {

    @Autowired
	private SubService subService;
	 
    @Autowired
	private ConstTableService constTableService;
	
	private  Integer commitInteval = 100;

	private static Integer expireDate = 60; // 默认是60分钟
	
	private final static String type = "ORDER_SETTING";

	private final static String keyValue = "ORDER_SETTING";
	
	@Override
	public ReturnT<String> execute(String param) throws Exception {
		 XxlJobLogger.log("调用订单服务的处理器");
		 Integer expire = getOrderCancelExpireDate();
			// 获取60分钟后前的时间
			Date date = getMinute(expire);
			XxlJobLogger.log("<!---结束超时尚未支付的订单 -->");
			long processNum = 0;  //执行的条数
			
			boolean haveValue = true;
			while (haveValue) {
				List<Sub> list = subService.getFinishUnPay(commitInteval, date);
				if (AppUtils.isBlank(list)) {
					haveValue = false;
					XxlJobLogger.log("暂时没有要处理的订单");
				} else {
					processNum = processNum + list.size(); 
					subService.finishUnPay(list);
				}
			}
			XxlJobLogger.log("处理订单条数： " + processNum);
		return SUCCESS;
	}
	
	/**
	 * 获取自动取消超时的时间
	 * 
	 * @return
	 */
	private Integer getOrderCancelExpireDate() {
		ConstTable constTable = constTableService.getConstTablesBykey(type, keyValue);
		Integer expire = expireDate;
		if (AppUtils.isNotBlank(constTable) && AppUtils.isNotBlank(constTable.getValue())) {
			OrderSetMgDto orderSet = JSONUtil.getObject(constTable.getValue(), OrderSetMgDto.class);
			if (AppUtils.isNotBlank(orderSet) && AppUtils.isNotBlank(orderSet.getAuto_order_cancel())) {
				expire = Integer.valueOf(orderSet.getAuto_order_cancel());
			}
		}
		return expire;
	}
	
	/**
	 * 得到跟现在若干天时间，如果为负数则向前推.
	 * 
	 * @param days
	 *            the days
	 * @return the date
	 */
	private Date getMinute(int minutes) {
		Date myDate = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(myDate);
		cal.add(Calendar.MINUTE, -minutes);
		return cal.getTime();
	}
}
	
