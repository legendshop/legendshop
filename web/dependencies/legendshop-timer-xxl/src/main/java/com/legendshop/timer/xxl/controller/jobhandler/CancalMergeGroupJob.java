package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.model.entity.MergeGroup;
import com.legendshop.spi.service.MergeGroupService;
import com.legendshop.util.AppUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 * 处理过期拼团活动，更改活动状态并且释放sku
 * @author Tony
 *
 */
@JobHandler(value="cancalMergeGroupJob")
@Component
public class CancalMergeGroupJob extends IJobHandler{
	
	@Autowired
	private MergeGroupService mergeGroupService;

	@Override
	public ReturnT<String> execute(String param) throws Exception {
		XxlJobLogger.log(" ######################### 处理过期拼团活动，更改活动状态定时任务  ###########################");

		// 所有上线的拼团活动
		List<MergeGroup> groups = mergeGroupService.queryMergeGroupListByStatus();
		if(AppUtils.isBlank(groups)){
			return new ReturnT<>(200, "没有过期拼团活动要处理");
		}
		
		int processOrder = mergeGroupService.updateOffineMergeGroup(groups);
		
		return new ReturnT<>(200, "成功取消过期拼团活动数量:"+processOrder);
	}
	
}
