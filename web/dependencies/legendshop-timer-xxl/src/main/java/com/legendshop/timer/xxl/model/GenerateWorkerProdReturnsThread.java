/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

import com.legendshop.business.dao.SubRefundReturnDao;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.spi.service.SubService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 统计计算 所有已完成的退货单
 */
public class GenerateWorkerProdReturnsThread implements Callable<Boolean> {

	// 一个同步辅助类，它允许一组线程互相等待，直到到达某个公共屏障点 (common barrier point)
	// private CyclicBarrier barrier;

	/** 用于计算统计 */
	private ShopBillCount shopBillCount;

	private SubService subService;
	
	private SubRefundReturnDao subRefundReturnDao;

	/** The shop id. */
	private Long shopId;

	/** 开始时间 */
	private Date startDate;

	/** 结束时间 */
	private Date endDate;

	/**
	 * 
	 */
	@Override
	public Boolean call() throws Exception {
		try {
			// 查询当前期 所有已完成的退货单
			List<SubRefundReturn> prodReturns = subRefundReturnDao.getSubRefundSuccess(shopId, endDate);
			if (AppUtils.isBlank(prodReturns)) {
				return true;
			}

			// 退单金额
			double orderReturnTotals = 0;
			//退单红包金额
			double orderReturnRedpackOffPriceTotals = 0;
			List<Long> billRefundId = new ArrayList<Long>();
			Set<String> subNumbers = new HashSet<String>();
			for (SubRefundReturn prodReturn : prodReturns) {
				orderReturnTotals = Arith.add(orderReturnTotals, prodReturn.getRefundAmount().doubleValue());
				billRefundId.add(prodReturn.getRefundId());
				
				subNumbers.add(prodReturn.getSubNumber());
				
				prodReturn = null;
			}
			//查询退款的总红包金额
			Double count = subService.getReturnRedpackOffPrice(subNumbers);
			orderReturnRedpackOffPriceTotals += count;
			
			prodReturns = null;
			
			shopBillCount.setOrderReturnTotals(orderReturnTotals);
			shopBillCount.setBillRefundId(billRefundId);
			shopBillCount.setOrderReturnRedpackOffPriceTotals(orderReturnRedpackOffPriceTotals);
			// barrier.await(2,TimeUnit.SECONDS); //两分钟之内
			// 阻塞一直等待执行完成拿到结果，如果在超时时间内，没有拿到则抛出异常
			return true;
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * The Constructor.
	 *
	 * @param shopBillCount
	 * @param subRefundReturnDao
	 * @param shopId
	 * @param endDate
	 */
	public GenerateWorkerProdReturnsThread(ShopBillCount shopBillCount,SubService subService, SubRefundReturnDao subRefundReturnDao, Long shopId, Date endDate) {
		super();
		this.shopBillCount = shopBillCount;
		this.subService = subService;
		this.subRefundReturnDao = subRefundReturnDao;
		this.shopId = shopId;
		this.endDate = endDate;
	}

}
