/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.controller.jobhandler;

import com.legendshop.base.log.TimerLog;
import com.legendshop.model.dto.ShopDetailTimerDto;
import com.legendshop.model.entity.ShopBillPeriod;
import com.legendshop.spi.service.ShopBillPeriodService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.ShopOrderBillService;
import com.legendshop.timer.xxl.util.BillPeriodUtils;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtil;
import com.legendshop.util.DateUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 商家结算中心,每天凌晨1点跑定时服务 0 0 1 * * ?
 *
 * @author legendshop
 */
@JobHandler(value="shopOrderBillJob")
@Component
public class ShopOrderBillJob extends IJobHandler {
	
	@Autowired
	private ShopOrderBillService shopOrderBillService;
	
	@Autowired
	private ShopDetailService shopDetailService;
	
	@Autowired
	private ShopBillPeriodService shopBillPeriodService;

	@Autowired
	private GenerateWorkerCyclicBarrierHandler generateWorkerCyclicBarrierHandler;

	@Autowired
	private BillPeriodUtils billPeriodUtils;

	/**
	 * 每次查询的条数
	 */
	private Integer commitInteval = 5;

	/**
	 * 不限制结算日结算，只要定时器运行就开始结算
	 */
	private boolean settlementAnyDay = true;

	/**
	 * 生成结算单
	 */
	@Override
	public ReturnT<String> execute(String param) throws Exception {
		
		// 马上执行结算动作 或者 指定的日期执行才有效
		boolean settleNow = settlementAnyDay || billPeriodUtils.isCanBeSettle();
		
		//获取结算周期的时间
		Date endDate = billPeriodUtils.getEndDate();
		Date startDate = billPeriodUtils.getStartDate(endDate);

		// 每周一结算 上周的订单
		if (settleNow) {

			// 获取商城商家
			Long firstShopId = shopDetailService.getMinShopId();
			Long lastShopId = shopDetailService.getMaxShopId();
			if (firstShopId == 0 && firstShopId.equals(lastShopId)) {

				return new ReturnT<>(200, "没有商家需要结算");
			}
			int code = 200;
			// 线程池
			int number = 0;
			AtomicInteger atomicInteger = new AtomicInteger(0);

			// 最多有20线程 ,超出在线程队列等待.
			ExecutorService exec = Executors.newFixedThreadPool(20);

			// 只能5个线程同时处理
			final Semaphore semaphore = new Semaphore(5);
			// 新建档期记录
			ShopBillPeriod shopBillPeriod = new ShopBillPeriod();
			try {
				Long shopBillPeriodId = shopBillPeriodService.getShopBillPeriodId();
				shopBillPeriod.setId(shopBillPeriodId);
				// 档期标示，例如 201512-ID
				String flag = shopOrderBillService.getShopBillMonth() + "-" + shopBillPeriodId;
				shopBillPeriod.setFlag(flag);
				shopBillPeriod.setOrderAmount(0d);
				shopBillPeriod.setOrderReturnTotals(0d);
				shopBillPeriod.setCommisTotals(0d);
				shopBillPeriod.setDistCommisTotals(0d);
				shopBillPeriod.setResultTotalAmount(0d);
				shopBillPeriod.setRecDate(new Date());
				

				/** 每次获取 commitInteval个商家 **/
				List<ShopDetailTimerDto> shopDetails = null;
				do {
					// 传入commitInteval， 确保每次都能拿到足够的数据来运算，避免因为id间隔过大而空转
					shopDetails = shopDetailService.getShopDetailList(firstShopId, commitInteval);
					if (AppUtils.isNotBlank(shopDetails)) {
						int size = shopDetails.size();
						for (int i = 0; i < size; i++) {
							ShopDetailTimerDto shopDetail = shopDetails.get(i);
							if (AppUtils.isBlank(shopDetail)) {
								continue;
							}
							Long shopId = shopDetail.getShopId();
							if (AppUtils.isBlank(shopId)) {
								continue;
							}
							number++;
							GenerateWorker worker = new GenerateWorker(atomicInteger, shopDetail, semaphore, shopBillPeriod, startDate, endDate);
							exec.execute(worker);
						}
						// 计算下一个shopId
						firstShopId = shopDetails.get(shopDetails.size() - 1).getShopId() + 1;
					}
				} while (AppUtils.isNotBlank(shopDetails));

			} catch (Exception e) {
				// 有异常发生
				code = 500;
				XxlJobLogger.log("", e);
			} finally {
				// 退出线程池
				exec.shutdown();
				// 循环等待线程结束
				for (;;) {
					if (number != atomicInteger.intValue()) {
						try {
							Thread.sleep(300);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					} else {
						break;
					}
				}

				XxlJobLogger.log("执行完毕: " + shopBillPeriod);
				
				// 保存档期记录
				if (number > 0) {
					shopBillPeriodService.saveShopBillPeriodWithId(shopBillPeriod, shopBillPeriod.getId());
				}
			}
			return new ReturnT<>(code, shopBillPeriod.toString());
		} else {
			return new ReturnT<>(500, "不在结算日不执行：" + DateUtil.DateToString(new Date(), "yyyy-MM-dd"));
		}

	}
	
	/**
	 * 多线程执行
	 * 
	 */
	class GenerateWorker implements Runnable {

		/**
		 * 用于标识是否执行成功
		 */
		private volatile AtomicInteger atomicInteger;

		private ShopDetailTimerDto shopDetail;

		private Semaphore semaphore;

		private ShopBillPeriod shopBillPeriod;

		private Date startDate;

		private Date endDate;

		/**
		 * 构造函数
		 * 
		 * @param atomicInteger
		 * @param shopDetail
		 * @param semaphore
		 * @param shopBillPeriod
		 * @param startDate
		 * @param endDate
		 */
		public GenerateWorker(AtomicInteger atomicInteger, ShopDetailTimerDto shopDetail, Semaphore semaphore, ShopBillPeriod shopBillPeriod, Date startDate,
				Date endDate) {
			this.atomicInteger = atomicInteger;
			this.shopDetail = shopDetail;
			this.semaphore = semaphore;
			this.shopBillPeriod = shopBillPeriod;
			this.startDate = startDate;
			this.endDate = endDate;
		}

		@Override
		public void run() {
			try {
				semaphore.acquire();
				TimerLog.info(" *****  Worker " + shopDetail.getShopId() + " 占用一个机器在生产...");
				generateWorkerCyclicBarrierHandler.generateShopBill(shopDetail, shopBillPeriod, parseShopStartDate(shopDetail, startDate, endDate),
						parseShopEndDate(shopDetail, endDate));
				TimerLog.info(" *** Worker " + shopDetail.getShopId() + "释放出机器...");
			} catch (InterruptedException e) {
				TimerLog.error("", e);
			} finally {
				atomicInteger.incrementAndGet(); // 标识处理任务
				semaphore.release();
			}
		}

		/**
		 * 如果每个商家的结算时间不一样，则在shopDetail中设置 TODO，如果有需求变更则在此变更
		 * 
		 * @param shopDetail
		 * @param startDate
		 * @param endDate
		 * @return
		 */
		private Date parseShopStartDate(ShopDetailTimerDto shopDetail, Date startDate, Date endDate) {
			return startDate;
		}

		/**
		 * 如果每个商家的结算时间不一样，则在shopDetail中设置 TODO，如果有需求变更则在此变更
		 * 
		 * @param shopDetail
		 * @param endDate
		 * @return
		 */
		private Date parseShopEndDate(ShopDetailTimerDto shopDetail, Date endDate) {
			return endDate;
		}

		public ShopDetailTimerDto getShopDetail() {
			return shopDetail;
		}

		public void setShopDetail(ShopDetailTimerDto shopDetail) {
			this.shopDetail = shopDetail;
		}

		public Semaphore getSemaphore() {
			return semaphore;
		}

		public void setSemaphore(Semaphore semaphore) {
			this.semaphore = semaphore;
		}

		public ShopBillPeriod getShopBillPeriod() {
			return shopBillPeriod;
		}

		public void setShopBillPeriod(ShopBillPeriod shopBillPeriod) {
			this.shopBillPeriod = shopBillPeriod;
		}

		public AtomicInteger getAtomicInteger() {
			return atomicInteger;
		}

		public void setAtomicInteger(AtomicInteger atomicInteger) {
			this.atomicInteger = atomicInteger;
		}

		public Date getStartDate() {
			return startDate;
		}

		public void setStartDate(Date startDate) {
			this.startDate = startDate;
		}

		public Date getEndDate() {
			return endDate;
		}

		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}

	}

	public void setSettlementAnyDay(boolean settlementAnyDay) {
		this.settlementAnyDay = settlementAnyDay;
	}

	public boolean isSettlementAnyDay() {
		return settlementAnyDay;
	}
}
