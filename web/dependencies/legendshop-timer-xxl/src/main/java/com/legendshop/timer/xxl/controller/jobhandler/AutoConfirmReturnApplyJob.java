package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.model.constant.RefundReturnStatusEnum;
import com.legendshop.model.constant.RefundReturnTypeEnum;
import com.legendshop.model.constant.ScheduleStatusEnum;
import com.legendshop.model.constant.ScheduleTypeEnum;
import com.legendshop.model.entity.ScheduleList;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.spi.service.ScheduleListService;
import com.legendshop.spi.service.SubRefundReturnService;
import com.legendshop.util.AppUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 * 买家申请退款，等待商家处理，时限为7天，超时系统将自动同意。
 *
 * 1、仅退款：  商家已经同意退款  ，商品库存恢复 ,退款记录 更新为商家已同意，等待平台退款。
 * 2、退货退款： 商家已经同意退款  ，等待卖家发货。
 */
@JobHandler(value = "autoConfirmReturnApplyJob")
@Component
public class AutoConfirmReturnApplyJob extends IJobHandler {

	@Autowired
	private ScheduleListService scheduleListService;
	
	@Autowired
	private SubRefundReturnService subRefundReturnService;
	
    @Override
    public ReturnT<String> execute(String param) throws Exception {
    	
        XxlJobLogger.log(" ######################### 自动执行商家同意买家退款申请 ###########################");
		//查询
		Date today = new Date();
		
		/** 查询今天能执行的商家同意买家退款 定时计划任务列表 */
		List<ScheduleList> scheduleLists = scheduleListService.getNonexecutionScheduleList(today, ScheduleTypeEnum.AUTO_7_REFUND.value());
		
		if (AppUtils.isBlank(scheduleLists)) {
			 XxlJobLogger.log(" ######################### 没有要同意申请的任务 ###########################");
			return SUCCESS;
		}
		
		XxlJobLogger.log(" ######################### 执行商家同意买家退款申请任务列表 ###########################");
		for (ScheduleList schedule : scheduleLists) {
			XxlJobLogger.log(" ######################### schedule={} ###########################", schedule.getId());
			String scheduleSn = schedule.getScheduleSn();
			Long shopId = schedule.getShopId(); 
			XxlJobLogger.log(" ######################### scheduleSn={},shopId={} ###########################",scheduleSn,shopId);
		    /**
		     * 1.查询该订单,若不存在,则删除该定时记录
		     */
			SubRefundReturn subRefun= subRefundReturnService.getSubRefundReturnByRefundSn(scheduleSn);
			if(AppUtils.isBlank(subRefun)){
				XxlJobLogger.log(" ######################### scheduleSn={},shopId={} 未找到订单记录 ###########################",scheduleSn,shopId);
				scheduleListService.deleteScheduleList(schedule);
				continue;
			}
			/**
		     * 2.查询该订单,是否经过审核,如果已经审核,则删除该定时记录
		     */
			if(RefundReturnStatusEnum.SELLER_WAIT_AUDIT.value()!=subRefun.getSellerState()){
				XxlJobLogger.log(" ######################### scheduleSn={},shopId={} 订单已经审核,或用户撤销申请  ###########################",scheduleSn,shopId);
				scheduleListService.deleteScheduleList(schedule);
				continue;
			}
			
			XxlJobLogger.log(" ######################### 执行商家同意买家退款申请   ###########################");
			
			if (RefundReturnTypeEnum.REFUND.value()==subRefun.getApplyType()) { //仅退款
				subRefundReturnService.shopAuditRefund(subRefun, true, "超时未审核系统自动同意退款",false);
			}else if(RefundReturnTypeEnum.REFUND_RETURN.value()==subRefun.getApplyType()){ //退款退货
				subRefundReturnService.shopAuditReturn(subRefun,true,false,"超时未审核系统自动同意,等待买家发货",false);
			}
			
			scheduleListService.updateScheduleListStatus(shopId, ScheduleStatusEnum.EXECUTED.value(), schedule.getId());
		}
		XxlJobLogger.log(" ######################### 执行商家同意买家退款申请 任务列表 ###########################");
        return SUCCESS;
    }
}
