/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import com.legendshop.business.dao.SubDao;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.entity.Sub;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 统计计算 所有已完成的订单.
 */
public class GenerateWorkerSubThread implements Callable<Boolean> {

	// 一个同步辅助类，它允许一组线程互相等待，直到到达某个公共屏障点 (common barrier point)
	/** 用于计算统计 */
	private ShopBillCount shopBillCount;

	/** The sub dao */
	private SubDao subDao;

	/** 店铺id */
	private Long shopId;

	/** 结束时间 */
	private Date endDate;
	
	/**
	 * The Constructor.
	 *
	 * @param shopBillCount
	 * @param subDao
	 * @param shopId
	 * @param endDate
	 */
	public GenerateWorkerSubThread(ShopBillCount shopBillCount, SubDao subDao, Long shopId, Date endDate) {
		this.shopBillCount = shopBillCount;
		this.subDao = subDao;
		this.shopId = shopId;
		this.endDate = endDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public Boolean call() throws Exception {
		// 查询当前期 所有已完成的订单
		try {
			List<Sub> subList = subDao.getBillFinishOrders(shopId, endDate, OrderStatusEnum.SUCCESS.value());

			if (AppUtils.isBlank(subList)) {
				return true;
			}

			double orderAmount = 0;// 订单金额(含运费)
			double redpackOffPrice = 0;// 红包总额
			double shippingTotals = 0;// 运费总额
			List<Long> billSubIds = new ArrayList<Long>();
			for (Sub sub : subList) {
				orderAmount = Arith.add(orderAmount, sub.getActualTotal());
				redpackOffPrice = Arith.add(redpackOffPrice, sub.getRedpackOffPrice());
				shippingTotals = Arith.add(shippingTotals, sub.getFreightAmount());
				billSubIds.add(sub.getSubId());
				sub = null;
			}

			shopBillCount.setOrderAmount(orderAmount);
			shopBillCount.setRedpackOffPrice(redpackOffPrice);
			shopBillCount.setShippingTotals(shippingTotals);
			shopBillCount.setBillSubIds(billSubIds);
			subList = null;
			// barrier.await(2,TimeUnit.SECONDS); //两分钟之内
			// 阻塞一直等待执行完成拿到结果，如果在超时时间内，没有拿到则抛出异常
			return true;
		} catch (Exception e) {
			throw e;
		}
	}


}
