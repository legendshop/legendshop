/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.base.log.TimerLog;
import com.legendshop.model.entity.AuctionDepositRec;
import com.legendshop.timer.xxl.service.AcutionScheduledService;
import com.legendshop.timer.xxl.service.TimerAuctionDepositRecService;
import com.legendshop.util.AppUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 * 拍卖订单完成后,退还完成订单的用户订金
 * 
 */
@JobHandler(value="acutionFinishRefundDepositJob")
@Component
public class AcutionFinishRefundDepositJob extends IJobHandler {
	
	@Autowired
	private TimerAuctionDepositRecService timerAuctionDepositRecService;
	
	@Autowired
	private AcutionScheduledService auctionScheduledService;

	/*private static Integer expireDatePay = 72; // 默认是72小时

	private static Integer commitInteval = 100;// 默认取100条
	 */
	
	/**
	 * 回滚中标的用户并且已经下单的用户 释放冻结金额
	 */
	@Override
	public ReturnT<String> execute(String param) throws Exception {
		XxlJobLogger.log("<!--- auctionRefund rollBack  begin");
		int processOrder = 0;
		// 获得此次活动的竞拍用户集合
		List<AuctionDepositRec> auctionDepositRecList = timerAuctionDepositRecService.getBidWinsNoFlag();
		if (AppUtils.isNotBlank(auctionDepositRecList)) {
			for (Iterator<AuctionDepositRec> iterator2 = auctionDepositRecList.iterator(); iterator2.hasNext();) {
				AuctionDepositRec depositRec = iterator2.next();
				// 进行退款操作
				boolean result = auctionScheduledService.bidUserRefund(depositRec);
				TimerLog.info(" rollBack refund money result =  {} ", result);
				if (result) { // 如果成功 形成退款标识动作
					processOrder++;
					depositRec.setFlagStatus(1L);
					// update FlagStatus
					timerAuctionDepositRecService.updateAuctionDepositRec(depositRec);
					XxlJobLogger.log(" rollBack refund money result success by userId={} money ={} ", depositRec.getUserId(), depositRec.getPayMoney());
				}
			}
		}
		XxlJobLogger.log("auctionRefund rollBack  end ---->");
		return new ReturnT<>(200, "处理退换保证金的拍卖订单数： "+processOrder);
	}

}
