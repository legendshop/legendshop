package com.legendshop.timer.xxl.controller.jobhandler;

import org.springframework.stereotype.Component;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;


@JobHandler(value = "orderJobHandler")
@Component
public class OrderJobHandler extends IJobHandler {

	@Override
	public ReturnT<String> execute(String param) throws Exception {
		 XxlJobLogger.log("调用订单服务的处理器");
		return SUCCESS;
	}
}
