package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.model.constant.RefundReturnStatusEnum;
import com.legendshop.model.constant.ScheduleStatusEnum;
import com.legendshop.model.constant.ScheduleTypeEnum;
import com.legendshop.model.entity.ScheduleList;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.spi.service.ScheduleListService;
import com.legendshop.spi.service.SubRefundReturnService;
import com.legendshop.util.AppUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 * 商家同意买家退款申请，提醒买家发货，时限为7天，买家超时未填写物流单号，系统将自动关闭退款申请
 */
@JobHandler(value = "autoCloseReturnApplyJob")
@Component
public class AutoCloseReturnApplyJob extends IJobHandler {

	@Autowired
	private ScheduleListService scheduleListService;
	
	@Autowired
	private SubRefundReturnService subRefundReturnService;
	
    @Override
    public ReturnT<String> execute(String param) throws Exception {
    	
        XxlJobLogger.log(" ######################### 自动执行退款订单关闭 ###########################");
		//查询
		Date today = new Date();
		
		List<ScheduleList> scheduleLists = scheduleListService.getNonexecutionScheduleList(today,ScheduleTypeEnum.AUTO_7_NOT_WRITE_CLOSE_REFUND.value());
		if (AppUtils.isBlank(scheduleLists)) {
			 XxlJobLogger.log("######################### 没有要关闭申请的任务 #########################");
			return SUCCESS;
		}
		
		XxlJobLogger.log(" ######################### 执行退款订单关闭任务列表 ###########################");
		for (ScheduleList schedule : scheduleLists) {
			
			XxlJobLogger.log(" ######################### schedule={} ###########################",schedule.getId());
			String scheduleSn = schedule.getScheduleSn();
			Long shopId = schedule.getShopId(); 
			XxlJobLogger.log(" ######################### scheduleSn={},shopId={} ###########################",scheduleSn,shopId);
		    /**
		     * 1.查询该订单,若不存在,则删除该定时记录
		     */
			SubRefundReturn subRefun= subRefundReturnService.getSubRefundReturnByRefundSn(scheduleSn);
			if(AppUtils.isBlank(subRefun)){
				XxlJobLogger.log(" ######################### scheduleSn={},shopId={} 未找到订单记录 ###########################",scheduleSn,shopId);
				scheduleListService.deleteScheduleList(schedule);
				continue;
			}
			/**
		     * 2.查询该订单,是否处于待发货状态,若不是则删除该定时记录
		     */
			if(RefundReturnStatusEnum.SELLER_AGREE.value()!=subRefun.getSellerState() && RefundReturnStatusEnum.LOGISTICS_WAIT_DELIVER.value()!=subRefun.getGoodsState()){
				XxlJobLogger.log(" ######################### scheduleSn={},shopId={} 订单已经发货或关闭  ###########################",scheduleSn,shopId);
				scheduleListService.deleteScheduleList(schedule);
				continue;
			}
			
			XxlJobLogger.log(" ######################### 执行退款订单关闭操作   ###########################");
			//关闭订单
			subRefundReturnService.closeSubRefundReturnByReturn(subRefun,false);
			
			scheduleListService.updateScheduleListStatus(shopId, ScheduleStatusEnum.EXECUTED.value(), schedule.getId());
			
		}
		XxlJobLogger.log(" ######################### 执行退款订单关闭任务列表  ###########################");
        return SUCCESS;
    }
}
