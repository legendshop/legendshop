/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.model;

import com.legendshop.business.dao.auction.AuctionDepositRecDao;
import com.legendshop.business.dao.presell.PresellSubDao;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.entity.AuctionDepositRec;
import com.legendshop.model.entity.PresellSub;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * 统计计算 结算拍卖活动扣除的拍卖保证金
 */
public class GenerateWorkerAuctionDepositrecThread implements Callable<Boolean> {

	// 一个同步辅助类，它允许一组线程互相等待，直到到达某个公共屏障点 (common barrier point)
	/** 用于计算统计 */
	private ShopBillCount shopBillCount;

	/** The presellsub dao */
	private PresellSubDao presellSubDao;

	private AuctionDepositRecDao auctionDepositRecDao;

	/** 店铺id */
	private Long shopId;


	/**
	 * The Constructor.
	 *
	 * @param shopBillCount
	 * @param auctionDepositRecDao
	 * @param shopId
	 */
	public GenerateWorkerAuctionDepositrecThread(ShopBillCount shopBillCount, AuctionDepositRecDao auctionDepositRecDao, Long shopId) {
		this.shopBillCount = shopBillCount;
		this.auctionDepositRecDao = auctionDepositRecDao;
		this.shopId = shopId;
	}

	/**
	 * (non-Javadoc)
	 *
	 * @see Callable#call()
	 */
	@Override
	public Boolean call() throws Exception {

		try {

			// 查询有扣除保证金标识并且未结算的拍卖定金列表
			List<AuctionDepositRec> auctionDepositRecList = auctionDepositRecDao.getBillAuctionDepositRecList(shopId);

			if (AppUtils.isBlank(auctionDepositRecList)) {
				return true;
			}

			// 扣除拍卖保证金总额
			double auctionDepositTotal = 0;

			List<Long> billAuctionDepositRecIds = new ArrayList<Long>();
			for (AuctionDepositRec auctionDepositRec : auctionDepositRecList) {
				auctionDepositTotal = Arith.add(auctionDepositTotal,auctionDepositRec.getPayMoney().doubleValue());

				billAuctionDepositRecIds.add(auctionDepositRec.getId());
				auctionDepositRec = null;
			}

			shopBillCount.setAuctionDepositTotal(auctionDepositTotal);

			shopBillCount.setBillAuctionDepositRecIds(billAuctionDepositRecIds);
			auctionDepositRecList = null;
			// barrier.await(2,TimeUnit.SECONDS); //两分钟之内
			// 阻塞一直等待执行完成拿到结果，如果在超时时间内，没有拿到则抛出异常
			return true;
		} catch (Exception e) {
			throw e;
		}
	}


}
