/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.service.impl;

import java.util.Date;
import java.util.Map;

import com.legendshop.base.log.RefundLog;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.*;
import com.legendshop.model.form.SysSubReturnForm;
import com.legendshop.spi.service.PaymentRefundService;
import com.legendshop.spi.service.SubSettlementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.log.TimerLog;
import com.legendshop.business.dao.ExpensesRecordDao;
import com.legendshop.business.dao.PdCashLogDao;
import com.legendshop.business.dao.SubHistoryDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.PdCashLogEnum;
import com.legendshop.model.constant.SubHistoryEnum;
import com.legendshop.timer.xxl.dao.TimerAuctionDepositRecDao;
import com.legendshop.timer.xxl.dao.TimerAuctionsDao;
import com.legendshop.timer.xxl.service.AcutionScheduledService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 定期拍卖服务实现类
 * 
 */
@SuppressWarnings("all")
@Service("acutionScheduledService")
public class AcutionScheduledServiceImpl implements AcutionScheduledService {
	
	@Autowired
	private TimerAuctionDepositRecDao timerAuctionDepositRecDao;

	@Autowired
	private UserDetailDao userDetailDao;

	@Autowired
	private TimerAuctionsDao timerAuctionsDao;

	@Autowired
	private PdCashLogDao pdCashLogDao;

	@Autowired
	private PaymentRefundService paymentRefundService;

	@Autowired
	private ExpensesRecordDao expensesRecordDao;

	@Autowired
	private SubHistoryDao subHistoryDao;

	@Autowired
	private SubSettlementService subSettlementService;

	/**
	 * 竞拍失败的用户退款操作 回滚用户的保证金 
	 */
	@Override
	public boolean bidUserRefund(AuctionDepositRec auctionDepositRec) {

		String userId = auctionDepositRec.getUserId();
		String subNumbers = auctionDepositRec.getSubNumber();
		double money = auctionDepositRec.getPayMoney().doubleValue();
		String outRefundNo = CommonServiceUtil.getRandomSn();
		if (!"FULL_PAY".equals(auctionDepositRec.getPayType())){
			SubSettlement subSettlement = subSettlementService.getSubSettlementBySn(auctionDepositRec.getSubSettlementSn());
			SysSubReturnForm returnForm=new SysSubReturnForm();
			returnForm.setOutRequestNo(outRefundNo);
			returnForm.setFlowTradeNo(subSettlement.getFlowTradeNo());
			returnForm.setSubSettlementSn(subSettlement.getSubSettlementSn());
			returnForm.setOrderDateTime(subSettlement.getCreateTime());
			returnForm.setReturnAmount(subSettlement.getCashAmount());
			returnForm.setRefundTypeId(subSettlement.getPayTypeId());
			returnForm.setPayAmount(subSettlement.getCashAmount());

			/**
			 * 第三方支付退款处理--->
			 */
			Map<String, Object> refundMap = paymentRefundService.refund(returnForm);
			return true;
		}

		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(userId);
		if (AppUtils.isBlank(userDetail)) {
			return false;
		}
		Integer count = timerAuctionDepositRecDao.update(
				"update ls_pd_cash_holding set status= ?,release_time=?  where sn=?  and user_id=?  and status=0 and type=?",
				new Object[] { 1, new Date(), subNumbers, userId, "auctions_freeze" }); // 更新
																						// hoding
																						// 如果更新成功才执行下面的动作
		if (count == 0) {
			return false;
		}
		Double availablePredeposit = userDetail.getAvailablePredeposit(); // 可用金额
		Double freezePredeposit = userDetail.getFreezePredeposit(); // 冻结金额
		Double updatePredeposit = Arith.add(availablePredeposit, money); // avaialbe_amout
																			// +
																			// 保证金
		Double updatefreePredeposit = Arith.sub(freezePredeposit, money); // hold_amount
																			// -
																			// 保证金
		count = userDetailDao.updatePredeposit(updatePredeposit, availablePredeposit, updatefreePredeposit, freezePredeposit, userId);
		
		
		if (count == 0) {
			TimerLog.error("回滚用户的拍卖保证金 失败 by userId ={} refundMoney={}", userId, money);
			throw new BusinessException("回滚用户的拍卖保证金 失败");
		}else {
			/*
			 * insert PdCashLog  退还用户保证金
			 */
			PdCashLog pdCashLog = new PdCashLog();
			pdCashLog.setUserId(userId);
			pdCashLog.setUserName(userDetailDao.getUserNameByUserId(userId));
			pdCashLog.setLogType(PdCashLogEnum.REFUND.value());
			pdCashLog.setAmount(money);
			pdCashLog.setAddTime(new Date());
			pdCashLog.setLogDesc("退还保证金:" + money);
			pdCashLog.setSn(subNumbers);
			pdCashLogDao.savePdCashLog(pdCashLog);
		}
			
		return true;

	}

	/**
	 * 撤销用户保证金 
	 * 
	 * @param userId
	 *            用户Id
	 * @param subNumbers
	 *            拍卖保证金支付单据号
	 * @param money
	 *            保证金金额
	 * @return
	 */
	public boolean undoUserBond(String userId, String subNumbers, double money) {
		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(userId);
		if (AppUtils.isBlank(userDetail)) {
			return false;
		}
		Integer count = timerAuctionDepositRecDao.update(
				"update ls_pd_cash_holding set status= ?,release_time=?  where sn=?  and user_id=?  and status=0 and type=?",
				new Object[] { 1, new Date(), subNumbers, userId, "auctions_freeze" }); // 更新
																						// hoding
																						// 如果更新成功才执行下面的动作
		if (count == 0) {
			return false;
		}
		Double freezePredeposit = userDetail.getFreezePredeposit(); // 冻结金额
		Double updatefreePredeposit = Arith.sub(freezePredeposit, money); // hold_amount
																			// -
																			// 保证金
		count = userDetailDao.updateFreezePredeposit(updatefreePredeposit, freezePredeposit, userId);
		if (count == 0) {
			TimerLog.error("回滚用户的拍卖保证金 失败 by userId ={} refundMoney={}", userId, money);
			throw new BusinessException("回滚用户的拍卖保证金 失败");
		}

		/*
		 * insert PdCashLog  违约拍卖用户现金流水
		 */
		PdCashLog pdCashLog = new PdCashLog();
		pdCashLog.setUserId(userId);
		pdCashLog.setUserName(userDetailDao.getUserNameByUserId(userId));
		pdCashLog.setLogType(PdCashLogEnum.AUCTION_DEDUCT.value());
		pdCashLog.setAmount(-money);
		pdCashLog.setAddTime(new Date());
		pdCashLog.setLogDesc("违约赔付保证金:" + money);
		pdCashLog.setSn(subNumbers);
		pdCashLogDao.savePdCashLog(pdCashLog);

		/*
		 * insert expensersRecored 违约拍卖用户流水
		 */
		ExpensesRecord expensesRecord = new ExpensesRecord();
		expensesRecord.setRecordDate(new Date());
		expensesRecord.setRecordMoney(-money);
		expensesRecord.setRecordRemark("违约赔付保证金:" + money);
		expensesRecord.setUserId(userId);
		expensesRecordDao.saveExpensesRecord(expensesRecord);
		
		return true;
	}

	/**
	 * 退款赔付保证金
	 * @param userId
	 * @param userName
	 * @param amount
	 * @param sn
	 * @param message
	 * @return
	 */
	public boolean refundShopAmount(String userId, String userName, Double amount, String sn) {
		/*
		 * 查询用户帐号 判断是否存在
		 */
		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(userId);
		if (AppUtils.isBlank(userDetail)) {
			return false;
		}

		Double available_predeposit = Arith.add(userDetail.getAvailablePredeposit(), amount);
		int count = userDetailDao.updateAvailablePredeposit(available_predeposit, userDetail.getAvailablePredeposit(), userId);
		if (count == 0) {
			return false;
		}

		/*
		 * insert PdCashLog
		 */
		PdCashLog pdCashLog = new PdCashLog();
		pdCashLog.setUserId(userId);
		pdCashLog.setUserName(userName);
		pdCashLog.setLogType(PdCashLogEnum.REFUND.value());
		pdCashLog.setAmount(amount);
		pdCashLog.setAddTime(new Date());
		pdCashLog.setLogDesc("退款赔付保证金:" + amount);
		pdCashLog.setSn(sn);
		pdCashLogDao.savePdCashLog(pdCashLog);

		/*
		 * insert expensersRecored
		 */
		ExpensesRecord expensesRecord = new ExpensesRecord();
		expensesRecord.setRecordDate(new Date());
		expensesRecord.setRecordMoney(amount);
		expensesRecord.setRecordRemark("退款赔付保证金:" + amount);
		expensesRecord.setUserId(userId);
		expensesRecordDao.saveExpensesRecord(expensesRecord);
		return true;

	}

	/**
	 * 判断用户可用预存款余额足够充值
	 * @param userId
	 * @param userName
	 * @param amount
	 * @param sn
	 * @param message
	 * @return
	 */
	public boolean recharge(String userId, String userName, Double amount, String sn, String message) {
		/*
		 * 查询用户帐号 判断是否存在
		 */
		UserDetail userDetail = userDetailDao.getUserDetailByidForUpdate(userId);
		if (AppUtils.isBlank(userDetail)) {
			return false;
		}
		Double available_predeposit = Arith.add(userDetail.getAvailablePredeposit(), amount);
		int count = userDetailDao.updateAvailablePredeposit(available_predeposit, userDetail.getAvailablePredeposit(), userId);
		if (count == 0) {
			return false;
		}

		/*
		 * insert PdCashLog
		 */
		PdCashLog pdCashLog = new PdCashLog();
		pdCashLog.setUserId(userId);
		pdCashLog.setUserName(userName);
		pdCashLog.setLogType(PdCashLogEnum.REFUND.value());
		pdCashLog.setAmount(amount);
		pdCashLog.setAddTime(new Date());
		pdCashLog.setLogDesc(message);
		pdCashLog.setSn(sn);
		pdCashLogDao.savePdCashLog(pdCashLog);
		/*
		 * insert expensersRecored
		 */
		ExpensesRecord expensesRecord = new ExpensesRecord();
		expensesRecord.setRecordDate(new Date());
		expensesRecord.setRecordMoney(amount);
		expensesRecord.setRecordRemark(message);
		expensesRecord.setUserId(userId);
		expensesRecordDao.saveExpensesRecord(expensesRecord);
		return true;
	}

	/**
	 * 24小时未转订单
	 */
	@Override
	public void finishUnAuctionOrder(BiddersWin biddersWin) {
		/**
		 * 退还用户的保证金 1、获取缴纳保证金的记录 2、获取该活动是属于哪个商家用户的、 3、退还至商家用户 4、处理状态，找出来的orderStatus = 1的记录
		 */
		AuctionDepositRec auctionDepositRec = timerAuctionDepositRecDao.getAucitonRec(biddersWin.getAId(), biddersWin.getUserId());

		if (AppUtils.isBlank(auctionDepositRec) || auctionDepositRec.getFlagStatus().intValue() == 1) {//如果已经处理过的就不再处理
			return;
		}
		/**
		 * 扣除拍卖用户的保证金金额
		 */
		double money = auctionDepositRec.getPayMoney().doubleValue();

		boolean config = Boolean.TRUE;
		if (PayTypeEnum.FULL_PAY.value().equals(auctionDepositRec.getPayType())){
			config = undoUserBond(auctionDepositRec.getUserId(), auctionDepositRec.getSubNumber(), money);
		}
		if (config) {

			// TODO BY JoeLin 注释保证金退还给商家预存款操作，改为结算方式与商家结算

			/*// 保证金：退还至商家
			boolean flag = refundShopAmount(biddersWin.getShopUserId(), userDetailDao.getUserNameByUserId(biddersWin.getShopUserId()),
					auctionDepositRec.getPayMoney().doubleValue(), auctionDepositRec.getSubNumber());
			if (!flag) {
				// TODO 通过auctionDepositRec 标识处理
				TimerLog.error("违约拍卖用户退付商家失败 userId ={},shopUserId={},refundMoney={}", auctionDepositRec.getUserId(), biddersWin.getShopUserId(), money);
			}*/
			
			
			// 更新中标记录状态
			timerAuctionsDao.update(" update ls_bidders_win set status=-2 where a_id=? ", biddersWin.getAId());
			// 更新处理标识状态
			timerAuctionsDao.update(" update ls_auction_depositrec set flag_status=1 where id=? ", auctionDepositRec.getId());
			// 更新扣除保证金标识
			timerAuctionsDao.update(" update ls_auction_depositrec set detain_flag=1 where id=? ",auctionDepositRec.getId());

		}

	}

	/**
	 * 72已经转订单 但是未支付的
	 */
	@Override
	public void finishUnAucitonPay(BiddersWin biddersWin) {
		/**
		 * 退还用户的保证金 1、获取缴纳保证金的记录 2、获取该活动是属于哪个商家用户的、 3、退还至商家用户 4、处理状态
		 */
		AuctionDepositRec auctionDepositRec = timerAuctionDepositRecDao.getAucitonRec(biddersWin.getAId(), biddersWin.getUserId());
		if (AppUtils.isBlank(auctionDepositRec) || auctionDepositRec.getOrderStatus().intValue() == 0 || auctionDepositRec.getFlagStatus().intValue() == 1) {
			return;
		}
		/*
		 * 扣除拍卖用户的保证金金额
		 */
		double money = auctionDepositRec.getPayMoney().doubleValue();
		boolean config = undoUserBond(auctionDepositRec.getUserId(), auctionDepositRec.getSubNumber(), money);
		if (config) {

			// TODO BY JoeLin 注释保证金退还给商家预存款操作，改为结算方式与商家结算
			/*// 保证金：退还至商家
			boolean flag = refundShopAmount(biddersWin.getShopUserId(), userDetailDao.getUserNameByUserId(biddersWin.getShopUserId()),
					auctionDepositRec.getPayMoney().doubleValue(), auctionDepositRec.getSubNumber());
			if (!flag) {
				TimerLog.error("违约拍卖用户退付商家失败 userId ={},shopUserId={},refundMoney={}", auctionDepositRec.getUserId(), biddersWin.getShopUserId(), money);
			}*/

			// 更新中标记录状态
			timerAuctionsDao.update(" update ls_bidders_win set status=-2 where a_id=? ", biddersWin.getAId());
			// 更新处理标识状态
			timerAuctionsDao.update(" update ls_auction_depositrec set flag_status=1 where id=? ", auctionDepositRec.getId());
			// 更新扣除保证金标识
			timerAuctionsDao.update(" update ls_auction_depositrec set detain_flag=1 where id=? ",auctionDepositRec.getId());
			
			// 取消订单
			Date now = new Date();
			int number = timerAuctionsDao.update(" update ls_sub set status=?,update_date=? where sub_id=? ", OrderStatusEnum.CLOSE.value(), new Date(),
					biddersWin.getSubId());

			if (number > 0) { // 如果该订单取消成功
				/*
				 * 记录订单历史
				 */
				SubHistory subHistory = new SubHistory();
				String time = subHistory.DateToString(now);
				subHistory.setRecDate(now);
				subHistory.setSubId(biddersWin.getSubId());
				subHistory.setStatus(SubHistoryEnum.ORDER_CANCEL.value());
				StringBuilder sb = new StringBuilder();
				sb.append("于").append(time).append("系统自动取消订单");
				subHistory.setUserName("系统自动");
				subHistoryDao.saveSubHistory(subHistory);
			}
		}
	}

	/**
	 * 取消订单
	 */
	@Override
	public void cancelAuctionOrder(BiddersWin biddersWin) {
		Date now = new Date();
		// 更新中标记录状态
		timerAuctionsDao.update(" update ls_bidders_win set status=-2 where a_id=? ", biddersWin.getAId());

		int number = timerAuctionsDao.update(" update ls_sub set status=?,update_date=? where sub_id=? ", OrderStatusEnum.CLOSE.value(), new Date(),
				biddersWin.getSubId());

		if (number > 0) { // 如果该订单取消成功
			/*
			 * 记录订单历史
			 */
			SubHistory subHistory = new SubHistory();
			String time = subHistory.DateToString(now);
			subHistory.setRecDate(now);
			subHistory.setSubId(biddersWin.getSubId());
			subHistory.setStatus(SubHistoryEnum.ORDER_CANCEL.value());
			StringBuilder sb = new StringBuilder();
			sb.append("于").append(time).append("系统自动关闭订单");
			subHistory.setUserName("系统自动");
			subHistoryDao.saveSubHistory(subHistory);
		}
	}
}
