/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.business.dao.SubItemDao;
import com.legendshop.model.dto.order.OrderSetMgDto;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.SubItem;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;

/**
 * 分佣计算 每天都进行结算
 */
@JobHandler(value="distributionJob")
@Component
public class DistributionJob extends IJobHandler {
	
	@Autowired
	private ConstTableService constTableService;
	
	@Autowired
	private SubItemDao subItemDao;
	
	@Resource(name="orderCommisSettleProcessor")
	private EventProcessor<List<SubItem>> OrderCommisSettleProcessor;

	private final static Integer commitInteval = 100;
	
	/**
	 * 结算分销佣金
	 */
	@Override
	public ReturnT<String> execute(String param) throws Exception {
		Date date = new Date();
		
		//获取平台退换货有效时间
		ConstTable constTable = constTableService.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
		OrderSetMgDto orderSetting = null;
		if (AppUtils.isNotBlank(constTable)) {
			String setting = constTable.getValue();
			orderSetting = JSONUtil.getObject(setting, OrderSetMgDto.class);
		}
		String autoProductReturn = orderSetting.getAuto_product_return();
		
		// 获取第一条订单详情
		Long firstSubItemId = subItemDao.getFirstDistSubItemId(date);
		// 获取最后一条订单详情
		Long lastSubItemId = subItemDao.getLastDistSubItemId(date);

		if (firstSubItemId == 0 && firstSubItemId == lastSubItemId) {
			return new ReturnT<>(200, "没有订单需要分佣");
		}
		long processOrder = 0;
		try {

			// 循环取100条数据
			while (firstSubItemId <= lastSubItemId) {
				Long toSubItemId = firstSubItemId + commitInteval;
				
				//修改为获取超过退货时间的订单项
				List<SubItem> subItems = subItemDao.getDistSubAfterReturnItemList(firstSubItemId, toSubItemId, autoProductReturn);
				
				if (AppUtils.isNotBlank(subItems)) {
					processOrder = processOrder + subItems.size();
					// 订单分佣金 发事件让promoter插件处理
					OrderCommisSettleProcessor.process(subItems);
				}
				firstSubItemId = toSubItemId;
			}
		} catch (Exception e) {
			return new ReturnT<>(500,"error:"+ e);
		}
		return new ReturnT<>(200,"处理的订单数： " + processOrder);
	}
	
	public synchronized void settleDistribution() {

	}

	/**
	 * 获取退货时常
	 * 
	 * @return
	 */
	private Integer getAutoProductReturn() {
		ConstTable constTable = constTableService.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
		OrderSetMgDto orderSetting = null;
		if (AppUtils.isNotBlank(constTable)) {
			String setting = constTable.getValue();
			orderSetting = JSONUtil.getObject(setting, OrderSetMgDto.class);
			String result = orderSetting.getAuto_product_return();
			return Integer.parseInt(result);
		}
		return null;
	}

	public void setSubItemDao(SubItemDao subItemDao) {
		this.subItemDao = subItemDao;
	}

}
