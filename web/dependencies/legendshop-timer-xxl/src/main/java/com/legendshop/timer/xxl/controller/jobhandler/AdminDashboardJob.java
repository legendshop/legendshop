/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.controller.jobhandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.model.constant.DashboardTypeEnum;
import com.legendshop.spi.service.AdminDashboardService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;

/**
 * 计算平台首页的统计信息 每30分钟运行一次
 * 
 */
@JobHandler(value="adminDashboardJob")
@Component
public class AdminDashboardJob extends IJobHandler {
	
	@Autowired
	private AdminDashboardService adminDashboardService;

	/**
	 * 计算dashboard
	 */
	@Override
	public ReturnT<String> execute(String param) throws Exception {
		DashboardTypeEnum[] dashboardTypeEnum = DashboardTypeEnum.values();
		for (DashboardTypeEnum typeEnum : dashboardTypeEnum) {
			adminDashboardService.calAdminDashboard(typeEnum);
		}
		return new ReturnT<>(200, "处理统计数目： " + dashboardTypeEnum.length);
	}

}
