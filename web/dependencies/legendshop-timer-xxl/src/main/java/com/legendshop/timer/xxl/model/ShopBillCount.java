/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.model;

import java.util.List;

/**
 * 用于计算统计.
 * @author linzh review
 */
public class ShopBillCount {
	
	/** 订单金额(含运费) */
	protected volatile double orderAmount = 0;
	
	/** 运费总额 */
	protected volatile double shippingTotals = 0;
	
	/** 退单金额 */
	protected volatile double orderReturnTotals = 0;
	
	/** 退单红包金额*/
	protected volatile double orderReturnRedpackOffPriceTotals = 0;
	
	/** 分销佣金总额 */
	protected volatile double distCommisTotals = 0;
	
	/** 红包总额 */
	protected volatile double redpackOffPrice = 0;
	
	/** 处理的订单ID */
	private List<Long> billSubIds;
	
	/** 处理过的退款单信息 */
	private List<Long> billRefundId;

	/** 处理过的预售订单信息  */
	private List<Long> billPresellSubIds;

	/** 处理过的拍卖保证金信息 */
	private List<Long> billAuctionDepositRecIds;

	/** 预售定金总额 */
	private volatile double preDepositPriceTotal = 0;

	private volatile double auctionDepositTotal = 0;

	
	public double getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(double orderAmount) {
		this.orderAmount = orderAmount;
	}

	public double getShippingTotals() {
		return shippingTotals;
	}

	public void setShippingTotals(double shippingTotals) {
		this.shippingTotals = shippingTotals;
	}

	public double getOrderReturnTotals() {
		return orderReturnTotals;
	}

	public void setOrderReturnTotals(double orderReturnTotals) {
		this.orderReturnTotals = orderReturnTotals;
	}

	public double getDistCommisTotals() {
		return distCommisTotals;
	}

	public void setDistCommisTotals(double distCommisTotals) {
		this.distCommisTotals = distCommisTotals;
	}

	public List<Long> getBillSubIds() {
		return billSubIds;
	}

	public void setBillSubIds(List<Long> billSubIds) {
		this.billSubIds = billSubIds;
	}

	public List<Long> getBillRefundId() {
		return billRefundId;
	}

	public void setBillRefundId(List<Long> billRefundId) {
		this.billRefundId = billRefundId;
	}

	public double getRedpackOffPrice() {
		return redpackOffPrice;
	}

	public void setRedpackOffPrice(double redpackOffPrice) {
		this.redpackOffPrice = redpackOffPrice;
	}

	public double getOrderReturnRedpackOffPriceTotals() {
		return orderReturnRedpackOffPriceTotals;
	}

	public void setOrderReturnRedpackOffPriceTotals(double orderReturnRedpackOffPriceTotals) {
		this.orderReturnRedpackOffPriceTotals = orderReturnRedpackOffPriceTotals;
	}

	public List<Long> getBillPresellSubIds() {
		return billPresellSubIds;
	}

	public void setBillPresellSubIds(List<Long> billPresellSubIds) {
		this.billPresellSubIds = billPresellSubIds;
	}

	public double getPreDepositPriceTotal() {
		return preDepositPriceTotal;
	}

	public void setPreDepositPriceTotal(double preDepositPriceTotal) {
		this.preDepositPriceTotal = preDepositPriceTotal;
	}

	public List<Long> getBillAuctionDepositRecIds() {
		return billAuctionDepositRecIds;
	}

	public void setBillAuctionDepositRecIds(List<Long> billAuctionDepositRecIds) {
		this.billAuctionDepositRecIds = billAuctionDepositRecIds;
	}

	public double getAuctionDepositTotal() {
		return auctionDepositTotal;
	}

	public void setAuctionDepositTotal(double auctionDepositTotal) {
		this.auctionDepositTotal = auctionDepositTotal;
	}
}
