package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.legendshop.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.model.constant.SeckillStatusEnum;
import com.legendshop.model.dto.order.OrderSetMgDto;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.SeckillActivity;
import com.legendshop.model.entity.SeckillSuccess;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.spi.service.SeckillActivityService;
import com.legendshop.spi.service.SeckillService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;
import com.legendshop.util.JSONUtil;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 *定时取消未转订单的秒杀资格
 */
@JobHandler(value = "seckillCancleOrderJob")
@Component
public class SeckillCancleOrderJob extends IJobHandler {

	@Autowired
	private SeckillActivityService seckillActivityService;
	
	@Autowired
	private ConstTableService constTableService;
	
	@Autowired
	private SeckillService seckillService;
	
    @Override
    public ReturnT<String> execute(String param) throws Exception {
        XxlJobLogger.log("开始处理过期秒杀活动[第二步]");
        
        // 获取已完成的秒杀活动
        List<SeckillActivity> SeckillActivitys = seckillActivityService.querySeckillActivityByStatus(SeckillStatusEnum.FINISH.value());
        if(AppUtils.isNotBlank(SeckillActivitys)){
			XxlJobLogger.log("已经完成的活动数 " + SeckillActivitys.size());
        	//获取订单设置配置
        	ConstTable constTable = constTableService.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
        	String setting = constTable.getValue();
        	OrderSetMgDto orderSetting = JSONUtil.getObject(setting, OrderSetMgDto.class);
        	
        	Date now = new Date();
        	for (SeckillActivity seckillActivity : SeckillActivitys) {
        		
        		//根据id获取秒杀成功记录
        		List<SeckillSuccess> list = seckillActivityService.getSeckillSucessBySeckillId(seckillActivity.getId());
        		if (AppUtils.isBlank(list)) {//如果秒杀成功记录为空,则直接更新秒杀活动改状态为“转换订单结束”，不需要等待自动取消秒杀未转订单时间过后再执行

					XxlJobLogger.log("活动秒杀成功但未支付记录数 " + list.size());
        			//取消未转订单的秒杀资格
        			seckillService.cancleSeckillOrder(seckillActivity.getId(),list);
				}else{
					XxlJobLogger.log("没有活动秒杀成功但未支付记录, 秒杀活动Id: {}", seckillActivity.getId());
					//获取当前秒杀活动的取消时间
	        		Date cancleTime = DateUtil.add(seckillActivity.getEndTime(), Calendar.MINUTE, Long.parseLong(orderSetting.getAuto_seckill_cancel()));
	        		if(now.after(cancleTime)){
	        			//取消未转订单的秒杀资格
	        			seckillService.cancleSeckillOrder(seckillActivity.getId(),list);
	        		}
				}
			}
        }
        XxlJobLogger.log("处理过期秒杀活动结束[第二步]");

        return SUCCESS;
    }
    
}
