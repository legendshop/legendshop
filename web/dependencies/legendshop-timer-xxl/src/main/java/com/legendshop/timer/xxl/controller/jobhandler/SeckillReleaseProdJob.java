package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.model.entity.SeckillActivity;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.SeckillActivityService;
import com.legendshop.util.AppUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 *定时处理过期的秒杀活动[第一步]
 * 把所有未完成的秒杀活动状态（未审核，未通过，进行中，失效）转换成已完成
 */
@JobHandler(value = "seckillReleaseProdJob")
@Component
public class SeckillReleaseProdJob extends IJobHandler {

	@Autowired
	private SeckillActivityService seckillActivityService;
	
	@Autowired
	private ProductService productService;
	
    @Override
    public ReturnT<String> execute(String param) throws Exception {
        XxlJobLogger.log("开始处理过期秒杀活动[第一步]");
        
        //判断是否存在已过期的秒杀活动，但状态没有变更
        List<SeckillActivity> seckillActivitys = seckillActivityService.queryUnfinishSeckillActivity();
        if(AppUtils.isNotBlank(seckillActivitys)){
			XxlJobLogger.log("已经过期的活动数 " + seckillActivitys.size());
        	for (SeckillActivity seckillActivity : seckillActivitys) {
        		productService.releaseSeckill(seckillActivity);
			}
        }else{
        	XxlJobLogger.log("没有过期的秒杀活动[第一步]");
        }
        
        XxlJobLogger.log("处理过期秒杀活动结束");

        return SUCCESS;
    }
    
}
