/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.base.log.TimerLog;
import com.legendshop.model.constant.BiddersWinStatusEnum;
import com.legendshop.model.entity.AuctionDepositRec;
import com.legendshop.model.entity.BiddersWin;
import com.legendshop.timer.xxl.service.AcutionScheduledService;
import com.legendshop.timer.xxl.service.TimerAuctionDepositRecService;
import com.legendshop.timer.xxl.service.TimerBiddersWinService;
import com.legendshop.util.AppUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 * 拍卖定时处理 72小时内为成功支付拍卖订单
 * 
 */
@JobHandler(value="acutionFinishUnAucitonPayJob")
@Component
public class AcutionFinishUnAucitonPayJob extends IJobHandler {
	
	@Autowired
	private TimerBiddersWinService timerBiddersWinService;
		
	@Autowired
	private TimerAuctionDepositRecService timerAuctionDepositRecService;
	
	@Autowired
	private AcutionScheduledService auctionScheduledService;
	
	private static Integer expireDatePay = 72; // 默认是72小时

	private static Integer commitInteval = 100;// 默认取100条
	
	/**
	 * 结束72小时未支付竞拍订单的操作
	 */
	@Override
	public ReturnT<String> execute(String param) throws Exception {
		XxlJobLogger.log("<!--- finishUnAucitonPay begin ");
		Date nowDate = getHour(expireDatePay);
		
		/**
		 * 拍卖成功72小时下单未支付的拍卖订单的记录
		 */
		// 获得起始id
		Long firstId = timerBiddersWinService.getMinId(BiddersWinStatusEnum.WAITPAY.value(), nowDate);

		// 获得结束id
		Long lastId = timerBiddersWinService.getMaxId(BiddersWinStatusEnum.WAITPAY.value(), nowDate);
		int processOrder = 0;
		while (firstId <= lastId) {
			Long toId = firstId + commitInteval;
			List<BiddersWin> biddersWinsList = timerBiddersWinService.getBiddersWin(firstId, toId, commitInteval, BiddersWinStatusEnum.WAITPAY.value());
			if (AppUtils.isNotBlank(biddersWinsList)) {

				// 交保证金的中标用户集合
				List<BiddersWin> getCashBiddersWinList = new ArrayList<BiddersWin>();

				// 未交保证金的中标用户集合
				List<BiddersWin> notGetCashBiddersWinList = new ArrayList<BiddersWin>();
				for (BiddersWin biddersWin : biddersWinsList) {

					//要处理空值的问题,因为有部分拍卖可能会被删除
					if(biddersWin.getIsSecurity() != null ) {
						if (biddersWin.getIsSecurity().intValue() == 1) {
							getCashBiddersWinList.add(biddersWin);
						} else if (biddersWin.getIsSecurity().intValue() == 0) {
							notGetCashBiddersWinList.add(biddersWin);
						}
					}
		
				}
				// 处理已交保证金的未转订单
				if (AppUtils.isNotBlank(getCashBiddersWinList)) {
					for (Iterator<BiddersWin> iterator = getCashBiddersWinList.iterator(); iterator.hasNext();) {
						BiddersWin biddersWin = (BiddersWin) iterator.next();
						if (AppUtils.isBlank(biddersWin)) {
							continue;
						}
						auctionScheduledService.finishUnAucitonPay(biddersWin);
					}
				}

				if (AppUtils.isNotBlank(notGetCashBiddersWinList)) {
					for (Iterator<BiddersWin> iterator = notGetCashBiddersWinList.iterator(); iterator.hasNext();) {
						BiddersWin biddersWin = (BiddersWin) iterator.next();
						if (AppUtils.isBlank(biddersWin)) {
							continue;
						}
						auctionScheduledService.cancelAuctionOrder(biddersWin);
					}
				}
				processOrder = processOrder + biddersWinsList.size();
			}
			firstId = toId;
		}
		XxlJobLogger.log(" finishUnAuctionOrder refund end -->");
		return new ReturnT<>(200, "处理拍卖订单数： " + processOrder);
	}
	
	/**
	 * 回滚中标的用户并且已经下单的用户 释放冻结金额
	 */
	private void rollBack() {
		TimerLog.info("<!--- auctionRefund rollBack  begin");
		// 获得此次活动的竞拍用户集合
		List<AuctionDepositRec> auctionDepositRecList = timerAuctionDepositRecService.getBidWinsNoFlag();
		if (AppUtils.isNotBlank(auctionDepositRecList)) {
			for (Iterator<AuctionDepositRec> iterator2 = auctionDepositRecList.iterator(); iterator2.hasNext();) {
				AuctionDepositRec depositRec = iterator2.next();
				// 进行退款操作
				boolean result = auctionScheduledService.bidUserRefund(depositRec);
				TimerLog.info(" rollBack refund money result =  {} ", result);
				if (result) { // 如果成功 形成退款标识动作
					depositRec.setFlagStatus(1L);
					// update FlagStatus
					timerAuctionDepositRecService.updateAuctionDepositRec(depositRec);
					TimerLog.info(" rollBack refund money result success by userId={} money ={} ", depositRec.getUserId(), depositRec.getPayMoney());
				}
			}
		}
		TimerLog.info(" auctionRefund rollBack  end ----> ");
	}

	/**
	 * 得到跟现在若干小时时间，如果为负数则向前推.
	 * 
	 * @param hour
	 * @return the hour
	 */
	private Date getHour(int hour) {
		Date myDate = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(myDate);
		cal.add(Calendar.HOUR_OF_DAY, -hour);
		return cal.getTime();
	}

}
