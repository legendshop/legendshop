/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.model.constant.AuctionsStatusEnum;
import com.legendshop.model.entity.AuctionDepositRec;
import com.legendshop.model.entity.Auctions;
import com.legendshop.model.entity.BiddersWin;
import com.legendshop.timer.xxl.service.AcutionScheduledService;
import com.legendshop.timer.xxl.service.TimerAuctionDepositRecService;
import com.legendshop.timer.xxl.service.TimerAuctionsService;
import com.legendshop.timer.xxl.service.TimerBiddersWinService;
import com.legendshop.util.AppUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 * 竞拍失败的用户退款定时器.
 */
@JobHandler(value="acutionBidUserRefundJob")
@Service
public class AcutionBidUserRefundJob extends IJobHandler {
	
	@Autowired
	private TimerAuctionsService timerAuctionsService;
	
	@Autowired
	private TimerBiddersWinService timerBiddersWinService;
	
	@Autowired
	private TimerAuctionDepositRecService timerAuctionDepositRecService;
	
	@Autowired
	private AcutionScheduledService auctionScheduledService;
	
	/** The commit inteval. */
	private static Integer commitInteval = 100;// 默认取100条
	
	/** 
	 * 退还竞拍失败的用户退款操作.
	 */
	@Override
	public ReturnT<String> execute(String param) throws Exception {
			XxlJobLogger.log("<!--- auctionRefund is today, generate auctionRefund begin");
			
			Long firstId = timerAuctionsService.getMinId(); // 获得起始id
			Long lastId = timerAuctionsService.getMaxId(); // 获得结束id
			int processOrder = 0;
			// 循环取100条数据
			while (firstId <= lastId) {
				Long toId = firstId + commitInteval+1;
				/*
				 * 获取所有已经完成的拍卖列表
				 */
				List<Auctions> auctionList = timerAuctionsService.getAuctionsBystatus(firstId, toId, AuctionsStatusEnum.FINISH.value());
				for (Iterator<Auctions> iterator = auctionList.iterator(); iterator.hasNext();) {
					Long aucitonId = iterator.next().getId();
					// 获得中标的用户
					List<BiddersWin> biddersWins = timerBiddersWinService.getBiddersWinList(aucitonId);
					/*if (AppUtils.isBlank(biddersWins)) {// 如果中标用户为空，则说明此次拍卖活动商品已经流拍
						continue;
					}*/
					// 获得此次活动的竞拍用户集合
					List<AuctionDepositRec> auctionDepositRecList = timerAuctionDepositRecService.getAuctionDepositRecByAid(aucitonId);

					// 排除中标用户
					auctionDepositRecList = exclude(auctionDepositRecList, biddersWins);
					if (auctionDepositRecList == null) {
						continue;
					}
					
					for (Iterator<AuctionDepositRec> iterator2 = auctionDepositRecList.iterator(); iterator2.hasNext();) {
						AuctionDepositRec depositRec = iterator2.next();
						// 进行退款操作
						boolean result = auctionScheduledService.bidUserRefund(depositRec);
						if (result) { // 如果成功 形成退款标识动作
							depositRec.setFlagStatus(1L);
							// update FlagStatus
							timerAuctionDepositRecService.updateAuctionDepositRec(depositRec);
						}
					}
					processOrder ++;
					// 处理完成这个活动
					timerAuctionsService.updateAuctionsFlagStatus(aucitonId, 1); // 0为已处理，1未处理
				}
				firstId = toId;
			}
			XxlJobLogger.log(" generate refund end -->");
			return new ReturnT<>(200, "已经处理的订单数: " + processOrder);
	}
	
	/**
	 * 排除交了保证金的中标用户.
	 *
	 * @param auctionDepositRecList the auction deposit rec list
	 * @param biddersWins the bidders wins
	 * @return the list< auction deposit rec>
	 */
	private List<AuctionDepositRec> exclude(List<AuctionDepositRec> auctionDepositRecList, List<BiddersWin> biddersWins) {
		if (AppUtils.isBlank(auctionDepositRecList)) {
			return null;
		}
		Iterator<AuctionDepositRec> iterator = auctionDepositRecList.iterator();
		while (iterator.hasNext()) {
			AuctionDepositRec depositRec = iterator.next();
			for (BiddersWin biddersWin : biddersWins) {
				if (biddersWin.getUserId().equals(depositRec.getUserId())) {
					iterator.remove();
				}
			}
		}
		return auctionDepositRecList;
	}

}
