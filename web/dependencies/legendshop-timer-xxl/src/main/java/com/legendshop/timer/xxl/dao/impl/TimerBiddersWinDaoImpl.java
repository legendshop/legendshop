/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.BiddersWin;
import com.legendshop.timer.xxl.dao.TimerBiddersWinDao;

/**
 * 中标Dao实现类
 */
@Repository
public class TimerBiddersWinDaoImpl extends GenericDaoImpl<BiddersWin, Long> implements TimerBiddersWinDao {

	/**
	 * 获取中标数据 
	 */
	public List<BiddersWin> getBiddersWin(String userId) {
		return this.queryByProperties(new EntityCriterion().eq("userId", userId).addAscOrder("status").addAscOrder("bitTime"));
	}

	/**
	 * 获取中标数据 
	 */
	public BiddersWin getBiddersWin(Long id) {
		return getById(id);
	}

	/**
	 * 删除中标数据 
	 */
	public int deleteBiddersWin(BiddersWin biddersWin) {
		return delete(biddersWin);
	}

	/**
	 * 保存中标数据 
	 */
	public Long saveBiddersWin(BiddersWin biddersWin) {
		return save(biddersWin);
	}

	/**
	 * 更新中标数据 
	 */
	public int updateBiddersWin(BiddersWin biddersWin) {
		return update(biddersWin);
	}

	/**
	 * 获取中标数据 
	 */
	@Override
	public BiddersWin getBiddersWin(String userId, Long id) {
		return get(
				"select id as id,a_id as aId,prod_id as prodId,sku_id as skuId,price as price,status as status from ls_bidders_win where user_id=? and id=? ",
				BiddersWin.class, new Object[] { userId, id });
	}

	/**
	 * 更新中标数据 
	 */
	@Override
	public void updateBiddersWin(Long id, String subNember, Long subId, int value) {
		update("update ls_bidders_win set sub_number=? ,sub_id=?,status=?,order_time=? where id=? and status=-1 ",
				new Object[] { subNember, subId, value, new Date(), id });
	}

	/**
	 * 获取中标数据 
	 */
	@Override
	public List<BiddersWin> getBiddersWin(Long firstId, Long toId, int commitInteval, Integer status) {
		String sql = "SELECT win.a_id AS aId,win.user_id AS userId,ls_auctions.user_id AS shopUserId,win.sub_id AS subId,ls_auctions.is_security as isSecurity FROM ls_bidders_win win LEFT JOIN ls_auctions ON ls_auctions.id=win.a_id WHERE   win.status=? AND win.id>=? AND win.id<=? ";
		return queryLimit(sql, BiddersWin.class, 0, commitInteval, new Object[] { status, firstId, toId });
	}

	/**
	 * 获取中标数据列表
	 */
	@Override
	public List<BiddersWin> getBiddersWinList(Long id) {
		return this.queryByProperties(new EntityCriterion().eq("aId", id));
	}

	/**
	 * 获取最小id 
	 */
	@Override
	public Long getMinId(Integer status, Date nowDate) {
		return this.get("select min(win.id) FROM ls_bidders_win win LEFT JOIN ls_auctions ON ls_auctions.id=win.a_id WHERE  win.status=? AND bit_time< ?",
				Long.class, status, nowDate);
	}

	/**
	 * 获取最大id 
	 */
	@Override
	public Long getMaxId(Integer status, Date nowDate) {
		return this.get("select max(win.id) FROM ls_bidders_win win LEFT JOIN ls_auctions ON ls_auctions.id=win.a_id WHERE  win.status=? AND bit_time< ?",
				Long.class, status, nowDate);
	}
}
