package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.model.constant.PresellProdStatusEnum;
import com.legendshop.model.entity.PresellProd;
import com.legendshop.spi.service.PresellProdService;
import com.legendshop.util.AppUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 *定时处理过期的预售活动
 */
@JobHandler(value = "presellActivityExpiredJob")
@Component
public class PresellActivityExpiredJob extends IJobHandler {

	@Autowired
	private PresellProdService presellProdService;
	
    @Override
    public ReturnT<String> execute(String param) throws Exception {
        XxlJobLogger.log("开始处理过期预售活动");
        Date now = new Date();//当前时间
        long processNum = 0;  //执行的条数
        
        //获取已过期，未完成的预售活动
		List<PresellProd> presellProds = presellProdService.getPresellProdByTime(now);
		if(AppUtils.isBlank(presellProds)) {
			 XxlJobLogger.log("没有过期未完成的预售活动要处理");
			 return SUCCESS;
		}
		
		for (PresellProd presellProd : presellProds) {
			 XxlJobLogger.log("预售活动  presellId = {},开始处理", presellProd.getId());
			 if(presellProd.getStatus().equals(PresellProdStatusEnum.FINISH.value()) 
					 || presellProd.getStatus().equals(PresellProdStatusEnum.STOP.value())) {
				 XxlJobLogger.log("判断该预售无需处理");
			 }else {
				 presellProd.setStatus(PresellProdStatusEnum.FINISH.value());
				 presellProdService.updatePresellProd(presellProd);
			 }
			 XxlJobLogger.log("预售活动  presellId = {}, 处理完成", presellProd.getId());
			 processNum++;
		}
		XxlJobLogger.log("处理过期预售活动个数： " + processNum);
        return SUCCESS;
    }
}
