/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import com.legendshop.business.dao.SkuDao;
import com.legendshop.model.constant.SkuActiveTypeEnum;
import com.legendshop.model.entity.Auctions;
import com.legendshop.timer.xxl.dao.TimerAuctionsDao;
import com.legendshop.timer.xxl.service.TimerAuctionsService;
import com.legendshop.util.AppUtils;

/**
 * 拍卖服务实现类
 */
@Service("timerAuctionsService")
public class TimerAuctionsServiceImpl  implements TimerAuctionsService{
    
	@Autowired
	private TimerAuctionsDao timerAuctionsDao;
	
	@Autowired
	private SkuDao skuDao;

	/**
     * 根据状态获取拍卖活动列表
     */
	public List<Auctions> getAuctionsBystatus(Long firstId,Long lastId,Long status) {
        return timerAuctionsDao.getAuctionsBystatus(firstId,lastId,status);
    }
	
	/**
	 * 查询所有在线并且过期的活动
	 */
	@Override
	public List<Auctions> getOvertimeAuctionsBystatus(Long status, Date now) {
		return timerAuctionsDao.getOvertimeAuctionsBystatus(status, now);
	}

	/**
     * 获取拍卖活动
     */
    public Auctions getAuctions(Long id) {
        return timerAuctionsDao.getAuctions(id);
    }

	/**
	 * 获取当前商品的价格 
	 */
	@Override
	public String currentQueryPrice(Long prodId, Long skuId) {
		if(AppUtils.isNotBlank(skuId) && skuId!=0){
			return timerAuctionsDao.get("select price from ls_sku where sku_id=? ", String.class, skuId);
		}else{
			return timerAuctionsDao.get("select cash from ls_prod where prod_id=? ", String.class, prodId);
		}
	}

	/**
	 * 完成拍卖 
	 */
	@Override
	public int finishAuction(Long id) {
		return timerAuctionsDao.finishAuction(id);	
	}

	/**
	 * 更新拍卖活动
	 */
	@Override
	public void updateAuctions(int crowdWatch, long bidCount, double curPrice,Long id) {
		timerAuctionsDao.update("update ls_auctions set crowd_watch=? , bidding_number=? , cur_price=? where id=? ", new Object[]{crowdWatch,bidCount,curPrice,id});
	}

	/**
	 * 查询所有在线并且不过期的活动 
	 */
	@Override
	public List<Auctions> getonlineList() {
		return timerAuctionsDao.getonlineList();
	}

	/**
	 * 获取最小id 
	 */
	@Override
	public Long getMinId() {
		return timerAuctionsDao.getMinId();
	}

	/**
	 * 获取最大id 
	 */
	@Override
	public Long getMaxId() {
		return timerAuctionsDao.getMaxId();
	}

	/**
	 * 更新拍卖状态 
	 */
	@Override
	public void updateAuctionsFlagStatus(Long aucitonId, Integer flagStatus) {
		timerAuctionsDao.update("update ls_auctions set flag_status=?  where id=?",flagStatus,aucitonId);
	}

	/**
	 * 更新中标记录,定时器使用
	 */
	@Override
	@Caching(evict = { @CacheEvict(value = "AuctionsDetailDto", key = "#id"), @CacheEvict(value = "AuctionsDto", key = "#id") })
	public void updateAuctions(int crowdWatch, long bidCount, double curPrice, Date date, Long id) {
		
		timerAuctionsDao.update("update ls_auctions set crowd_watch=? , bidding_number=? , cur_price=?,end_time=?  where id=? ",
				new Object[] { crowdWatch, bidCount, curPrice, date, id });
		
		//重置SKU的营销标记
		Auctions auctions = timerAuctionsDao.getAuctions(id);
		if(auctions != null){
			skuDao.updateSkuTypeById(auctions.getSkuId(), SkuActiveTypeEnum.PRODUCT.value(),SkuActiveTypeEnum.AUCTION.value());
		}
	}

}
