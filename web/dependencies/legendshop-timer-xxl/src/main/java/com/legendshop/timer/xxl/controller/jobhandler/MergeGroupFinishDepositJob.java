package com.legendshop.timer.xxl.controller.jobhandler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.legendshop.base.constants.SysParameterEnum;
import com.legendshop.base.log.TimerLog;
import com.legendshop.model.entity.MergeGroupOperate;
import com.legendshop.spi.service.MergeGroupOperateService;
import com.legendshop.spi.service.MergeGroupService;
import com.legendshop.spi.service.SystemParameterService;
import com.legendshop.util.AppUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import com.xxl.job.core.log.XxlJobLogger;

/**
 * 自动执行过期拼团未成团原路退款任务定时任务
 * @author Tony
 *
 */
@JobHandler(value="mergeGroupFinishDepositJob")
@Component
public class MergeGroupFinishDepositJob extends IJobHandler {
	
	@Autowired
	private MergeGroupService mergeGroupService;
	
	@Autowired
	private MergeGroupOperateService  mergeGroupOperateService;
	
	@Autowired
	private SystemParameterService systemParameterService;
	
	@Override
	public ReturnT<String> execute(String param) throws Exception {
		
		XxlJobLogger.log(" ######################### 自动执行过期拼团未成团原路退款任务定时任务  ###########################");
		
		/**
		 * 查询所有 未成团成功的参与拼团信息;
		 */
		
		List<MergeGroupOperate>  mergeGroupOperates= mergeGroupOperateService.findHaveInMergeGroupOperate();
		if(AppUtils.isBlank(mergeGroupOperates)){
			return new ReturnT<>(200, "没有任何需要处理的活动信息");	
		}
		
		//获取成团限制时间
		Integer groupSurvivalTime = Integer.valueOf(systemParameterService.getSystemParameter(SysParameterEnum.GROUP_SURVIVAL_TIME.name()).getValue().toString());
		
		if (AppUtils.isBlank(groupSurvivalTime)) { // 默认 设置 24 小时
			groupSurvivalTime = 24;
		}
		
		Long  currentTimeMillis=System.currentTimeMillis();
		TimerLog.info("######### 取消未成团成功的拼团活动  ########");
		
		mergeGroupService.finishDepositJob(mergeGroupOperates,groupSurvivalTime,currentTimeMillis);
		
		return new ReturnT<>(200, "处理取消未成团成功的拼团活动成功");
	}
	
}
