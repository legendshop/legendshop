/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.dao.impl;
 
import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.AuctionDepositRec;
import com.legendshop.timer.xxl.dao.TimerAuctionDepositRecDao;

/**
 * 保证金Dao实现类
 */
@Repository
public class TimerAuctionDepositRecDaoImpl extends GenericDaoImpl<AuctionDepositRec, Long> implements TimerAuctionDepositRecDao  {

    /**
     * 获取保证金 
     */
	public AuctionDepositRec getAuctionDepositRec(Long id){
		return getById(id);
	}
	
	  /**
     * 删除保证金 
     */
    public int deleteAuctionDepositRec(AuctionDepositRec auctionDepositRec){
    	return delete(auctionDepositRec);
    }
	
    /**
     * 保存保证金 
     */
	public Long saveAuctionDepositRec(AuctionDepositRec auctionDepositRec){
		return save(auctionDepositRec);
	}
	
	 /**
     * 更新保证金 
     */
	public int updateAuctionDepositRec(AuctionDepositRec auctionDepositRec){
		return update(auctionDepositRec);
	}
	
	 /**
     * 判断支付环境是否安全 
     */
	@Override
	public boolean isPaySecurity(String userId, Long paimaiId) {
		long number= getLongResult("select count(id) from ls_auction_depositrec where a_id=? and user_id=? ", paimaiId,userId) ;
		return number>0;
	}

	/**
	 * 查询报名人数 
	 */
	@Override
	public Long queryAccess(Long paimaiId) {
		return getLongResult("select count(id) from ls_auction_depositrec where a_id=? and order_status=1 ", paimaiId) ;
	}

	/**
	 * 根据id获取保证金 
	 */
	@Override
	public List<AuctionDepositRec> getAuctionDepositRecByAid(Long aId) {
	 return this.queryByProperties(new EntityCriterion().eq("aId", aId).eq("flagStatus", 0l).eq("orderStatus", 1));
	}

	/**
     * 获取保证金 
     */
	@Override
	public AuctionDepositRec getAucitonRec(Long aId, String userId) {
		return this.getByProperties(new EntityCriterion().eq("aId", aId).eq("userId", userId).eq("orderStatus", 1));
	}

	/**
	 * 查询那些中标并且支付过订单的 没有处理过退款操作的用户 保证金记录 
	 */
	@Override
	public List<AuctionDepositRec> getBidWinsNoFlag() {
		return query("SELECT ls_auction_depositrec.* FROM ls_bidders_win LEFT JOIN ls_auction_depositrec ON  ls_auction_depositrec.a_id=ls_bidders_win.a_id WHERE ls_bidders_win.status=1  AND ls_auction_depositrec.user_id=ls_bidders_win.user_id AND ls_auction_depositrec.flag_status=0 ", AuctionDepositRec.class);
	}
	
	
 }
