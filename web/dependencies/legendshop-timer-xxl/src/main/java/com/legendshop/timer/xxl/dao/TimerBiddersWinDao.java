/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.dao;
 
import java.util.Date;
import java.util.List;

import com.legendshop.dao.Dao;
import com.legendshop.model.entity.BiddersWin;

/**
 * 中标Dao
 */
public interface TimerBiddersWinDao extends Dao<BiddersWin, Long> {
     
	/** 获取中标数据 */
    public abstract List<BiddersWin> getBiddersWin(String userId);

    /** 获取中标数据 */
	public abstract BiddersWin getBiddersWin(Long id);
	
	/** 删除中标数据 */
    public abstract int deleteBiddersWin(BiddersWin biddersWin);
	
    /** 保存中标数据 */
	public abstract Long saveBiddersWin(BiddersWin biddersWin);
	
	/** 更新中标数据 */
	public abstract int updateBiddersWin(BiddersWin biddersWin);

	/** 获取中标数据 */
	public abstract BiddersWin getBiddersWin(String userId, Long id);

	/** 更新中标数据 */
	public abstract void updateBiddersWin(Long id, String subNember,Long subId, int value);

	/** 获取中标数据 */
	public abstract List<BiddersWin> getBiddersWin(Long  firstId,Long toId, int commitInteval,Integer status);
	
	/** 获取中标数据列表 */
	public abstract List<BiddersWin> getBiddersWinList(Long id);

	/** 获取最小id */
	public abstract Long getMinId(Integer status,Date nowDate);

	/** 获取最大id */
	public abstract Long getMaxId(Integer status,Date nowDate);
	
 }
