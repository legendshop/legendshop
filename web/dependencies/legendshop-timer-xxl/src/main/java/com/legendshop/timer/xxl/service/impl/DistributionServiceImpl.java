/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.log.TimerLog;
import com.legendshop.model.constant.RefundReturnStatusEnum;
import com.legendshop.model.entity.SubItem;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.spi.service.PreDepositTimerService;
import com.legendshop.spi.service.SubItemService;
import com.legendshop.spi.service.SubRefundReturnService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.timer.xxl.service.DistributionService;
import com.legendshop.util.AppUtils;

/**
 * 分佣金服务实现类
 * 
 */
@Service("distributionService")
public class DistributionServiceImpl implements DistributionService {

	@Autowired
	private SubItemService subItemService;
	
	@Autowired
	private PreDepositTimerService preDepositTimerService;
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private SubRefundReturnService subRefundReturnService;
	
	/**
	 * 结算分销佣金.
	 *
	 * @param subItem the tle distribution
	 */
	public  void settleDistribution(SubItem subItem){
		/**
		 * 查询该订单是否有退款/退货,并且判断退款/退货是否成功
		 * 
		 * 1.整个订单退款
		 * 2.单个订单项退款或退货
		 */
		boolean needDist = true;
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturnBySubNumber(subItem.getSubItemNumber());
		if(null == subRefundReturn 
				|| RefundReturnStatusEnum.APPLY_FINISH.value().equals(subRefundReturn.getApplyState())){//订单退款/退货成功
			if(subRefundReturn.getSubItemId().equals(subItem.getSubItemId())){//如果是当前订单项的退款/退货
				needDist = false;
			}
		}
		
		//没有退货，或者退货不成功，则商城需要支付分销佣金
		if(needDist){
			//一级用户分佣金
			rechargeForDistCommis(subItem, subItem.getDistUserName(), subItem.getDistCommisCash());
			//二级用户分佣金
			rechargeForDistCommis(subItem, subItem.getDistSecondName(), subItem.getDistSecondCommis());
			//三级用户分佣金
			rechargeForDistCommis(subItem, subItem.getDistThirdName(), subItem.getDistThirdCommis());
		}
		//更新子订单状态
		subItem.setCommisSettleSts(1);//佣金结算状态: 0:待结算, 1:已结算
		subItemService.updateSubItem(subItem);
	}
	
	/**
	 * 三级用户分佣金.
	 *
	 * @param subItem 订单
	 * @param distName 分佣金的人
	 * @param distCommis 分的钱数
	 */
	private void rechargeForDistCommis(SubItem subItem, String distName, Double distCommis){
		if(AppUtils.isNotBlank(distName) && AppUtils.isNotBlank(distName)){
			try {
				if(AppUtils.isNotBlank(distName) && AppUtils.isNotBlank(distCommis)){
					UserDetail userDetail = userDetailService.getUserDetail(distName);
					if(AppUtils.isNotBlank(userDetail)){
							//分销佣金充值到预存款
							preDepositTimerService.rechargeForDistCommis(userDetail.getUserId(), subItem.getDistSecondCommis(), subItem.getSubItemNumber());
					}
				}
			}catch (Exception e) {
				TimerLog.error("分销佣金充值失败  by userName ={} , distCommisCash = {} , subItemNumber ={}  ", distName,distCommis,subItem.getSubItemNumber());
			}
		}
	}
}
