/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.service;

import com.legendshop.model.entity.Auctions;

import java.util.Date;
import java.util.List;

/**
 * 拍卖服务
 */
public interface TimerAuctionsService  {

	public List<Auctions> getAuctionsBystatus(Long firstId,Long lastId,Long status);
	
	/** 查询所有在线并且过期的活动 */
	public List<Auctions> getOvertimeAuctionsBystatus(Long status, Date now);

    public Auctions getAuctions(Long id);
    
    /** 完成拍卖 */
    public int finishAuction(Long id);

	/** 获取当前商品的价格 */
	public String currentQueryPrice(Long prodId, Long skuId);
	
	 /** 更新拍卖活动 */
	public void updateAuctions(int crowdWatch, long bidCount, double curPrice,Long id);
    
	/** 查询所有在线并且不过期的活动 */
	public List<Auctions> getonlineList();

	/** 获取最小id */
	public Long getMinId();

	/** 获取最大id */
	public Long getMaxId();

	/** 更新拍卖状态 */
	public void updateAuctionsFlagStatus(Long aucitonId, Integer i);
	
	/** 更新拍卖活动数据 */
	public void updateAuctions(int crowdWatch, long bidCount, double curPrice,Date date, Long id);
	
}
