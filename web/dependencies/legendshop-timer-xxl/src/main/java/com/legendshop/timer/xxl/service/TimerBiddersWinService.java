/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.timer.xxl.service;

import java.util.Date;
import java.util.List;

import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.BiddersWin;

/**
 * 中标服务
 */
public interface TimerBiddersWinService {

	/** 获取中标用户 */
	public BiddersWin getBiddersWin(String userId, Long id);

	/** 获取中标用户 */
	public List<BiddersWin> getBiddersWin(String userId);

	/** 获取中标用户 */
	public List<BiddersWin> getBiddersWin(Long firstId, Long toId, int commitInteval, Integer status);

	/** 获取中标用户 */
	public BiddersWin getBiddersWin(Long id);

	/** 获取中标用户列表 */
	public List<BiddersWin> getBiddersWinList(Long id);

	/** 删除中标用户 */
	public void deleteBiddersWin(BiddersWin biddersWin);

	/** 保存中标用户 */
	public Long saveBiddersWin(BiddersWin biddersWin);

	/** 更新中标用户 */
	public void updateBiddersWin(BiddersWin biddersWin);

	/** 更新中标用户 */
	public void updateBiddersWin(Long id, String subNember, Long subId, int value);

	/** 获取最小id */
	public Long getMinId(Integer status, Date nowDate);

	/** 获取最大id */
	public Long getMaxId(Integer status, Date nowDate);

}
