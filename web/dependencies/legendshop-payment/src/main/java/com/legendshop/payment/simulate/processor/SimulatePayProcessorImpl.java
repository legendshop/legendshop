/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.payment.simulate.processor;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.base.util.MerchantApiUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PaySourceEnum;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.model.form.SysSubReturnForm;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PaymentProcessor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;


/**
 * 模拟支付
 *
 */
@Service("simulatePayProcessor")
public class SimulatePayProcessorImpl implements PaymentProcessor {
	
	@Autowired
	private PayTypeService payTypeService;

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(SimulatePayProcessorImpl.class);
	
	@Override
	public Map<String,Object> payto(SysPaymentForm paymentForm) {
		
		Map<String,Object>  map=new HashMap<String, Object>();
		map.put("result", false);
		
		PayType payType = payTypeService.getPayTypeById(PayTypeEnum.SIMULATE_PAY.value());
		PaymentLog.log("############# 平台模拟支付模式  {} ########## ",JSONUtil.getJson(payType));
		
		if (payType == null || AppUtils.isBlank(payType.getPaymentConfig())) {
			map.put("message", "支付方式不存在,请设置支付方式");
			return map;
		} else if (payType.getIsEnable() == Constants.OFFLINE) {
			map.put("message", "该支付方式没有启用!");
			return map;
		}
		
		paymentForm.setPayTypeId(PayTypeEnum.SIMULATE_PAY.value());
		paymentForm.setPayTypeName(PayTypeEnum.SIMULATE_PAY.desc());
		
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		
		String key =  jsonObject.getString("key");
		String token =  jsonObject.getString("token");
		String out_trade_no = paymentForm.getSubSettlementSn();
		
		StringBuffer buffer = new StringBuffer();
	    buffer.append("订单编号：").append(out_trade_no);
	     
	    Map<String , Object> paramMap = new HashMap<String, Object>();
	
		//-----------------------------
		//设置支付参数
		//-----------------------------
	    paramMap.put("token", token);		        //商户号
	    paramMap.put("out_trade_no", out_trade_no);
	    paramMap.put("total_fee", paymentForm.getTotalAmount());
	    paramMap.put("trade_no", CommonServiceUtil.getRandomSn());
	    paramMap.put("subject", buffer.toString());
	    paramMap.put("payType", PayTypeEnum.SIMULATE_PAY.name());
        String sign = MerchantApiUtil.getSign(paramMap, key);
        PaymentLog.info(" 签名及生成密钥信息  sign "+sign);
        paramMap.put("sign",sign);
        
        String returnUrl = null;
        String result = null;
        if(PaySourceEnum.isAPP(paymentForm.getSource()) || PaySourceEnum.isWxMp(paymentForm.getSource())){
        	returnUrl = paymentForm.getDomainName()  + "/payNotify/notify/"+ PayTypeEnum.SIMULATE_PAY.value();
        	result = MerchantApiUtil.buildRequestForApp(paramMap, returnUrl);
        }else {
        	returnUrl = paymentForm.getDomainName()  + "/payNotify/response/"+ PayTypeEnum.SIMULATE_PAY.value();
        	result = MerchantApiUtil.buildRequest(paramMap, returnUrl ,"GET", "确定" );
        }
    	
   		PaymentLog.info("send to simulatePay gateway " + result);
   		
   		map.put("message", result);
		map.put("result", true);
		return map;
	}

	public String triggerReturn(SysSubReturnForm returnForm) {
		return null;
	}

	private boolean isHttps(String url){
		if(url.startsWith("https://")){
			return true;
		}
		return false;
	}
}
