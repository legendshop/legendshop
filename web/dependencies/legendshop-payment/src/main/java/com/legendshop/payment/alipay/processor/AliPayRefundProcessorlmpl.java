package com.legendshop.payment.alipay.processor;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeRefundModel;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipayTradeRefundResponse;
import com.legendshop.base.log.RefundLog;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.form.SysSubReturnForm;
import com.legendshop.payment.alipay.util.AlipayConfig;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PaymentRefundProcessor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 支付宝退款处理
 *
 */
@Service("aliPayRefundProcessor")
public class AliPayRefundProcessorlmpl implements PaymentRefundProcessor {
	
	@Autowired
	private PayTypeService payTypeService;
	
	@Override
	public Map<String, Object> refund(SysSubReturnForm returnForm) {
		
		RefundLog.info("############# 支付宝退款中心 ########## " );
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", false);
		PayType payType = payTypeService.getPayTypeById(PayTypeEnum.ALI_PAY.value());
		RefundLog.info("############# 退款支付参数  {} ########## ",JSONUtil.getJson(payType));
		if (payType == null || AppUtils.isBlank(payType.getPaymentConfig())) {
			map.put("message", "支付方式不存在,请设置支付方式");
			return map;
		}
		
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		
		String out_trade_no=returnForm.getSubSettlementSn() ;// 支付时传入的商户订单号，与trade_no必填一个
		String trade_no= returnForm.getFlowTradeNo();	//支付时返回的支付宝交易号，与out_trade_no必填一个
		String out_request_no=returnForm.getOutRequestNo() ;	//本次退款请求流水号，部分退款时必传
		Double refund_amount=returnForm.getReturnAmount();	// 本次退款金额
		
       
		String appId = jsonObject.getString(AlipayConfig.APPID);
		String rsa_private = jsonObject.getString(AlipayConfig.RSA_PRIVATE);
		String rsa_publish = jsonObject.getString(AlipayConfig.RSA_PUBLIC);
		
		
		AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.serverUrl,appId,rsa_private,"json",AlipayConfig.input_charset, rsa_publish, AlipayConfig.sign_type);
		
		
		AlipayTradeRefundModel refundModel=new AlipayTradeRefundModel();
		refundModel.setOutTradeNo(out_trade_no);;
		refundModel.setTradeNo(trade_no);
		refundModel.setRefundAmount(refund_amount+"");
		refundModel.setRefundReason("正常退款");
		refundModel.setOutRequestNo(out_request_no);
		
		AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
		request.setBizModel(refundModel);
		
		AlipayTradeRefundResponse response = null;
		try {
			response = alipayClient.execute(request);
			String code=response.getCode(); //网关返回码,
			System.out.print(response.getBody());
			System.out.println(response.toString());
			if("10000".equals(code)){
				RefundLog.info(" 支付宝退款相应结果 {}  ",JSONUtil.getJson(response));
				if (response.isSuccess()) {
					System.out.println("调用成功");
					map.put("result", true);
				} else {
					System.out.println("调用失败");
				}
			}else{
				if(AppUtils.isNotBlank(response.getSubMsg())){
					map.put("message", response.getSubMsg());
				}else{
					map.put("message", response.getMsg());
				}
				RefundLog.warn(" AlipayTradeRefund result {}",JSONUtil.getJson(response));	
			}
		} catch (AlipayApiException e) {
			e.printStackTrace();
			map.put("message","退款失败！");
			map.put("result", false);
		}
		return map;
	}
	
	
}
