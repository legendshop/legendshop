package com.legendshop.payment.weixin.processor;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PaymentProcessor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.MD5Util;
import com.legendshop.util.RandomStringUtils;

/**
 * 微信内置浏览器公众号支付 JSAPI方式
 */
@Service("weXinWapPayProcessor")
public class WeXinWapPayProcessorImpl implements PaymentProcessor {

	@Autowired
	private PayTypeService payTypeService;
	
	@Autowired
	private PropertiesUtil propertiesUtil;
	
	@Override
	public Map<String, Object> payto(SysPaymentForm paymentForm) {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", false);
		
		PayType payType = payTypeService.getPayTypeById(PayTypeEnum.WX_PAY.value()); //这种方式是卖家中心调用支付服务
		if (payType == null || AppUtils.isBlank(payType.getPaymentConfig())) {
			map.put("message", "支付方式不存在,请设置支付方式---WeXinWapPayProcessorImpl");
			return map;
		} else if (payType.getIsEnable() == Constants.OFFLINE) {
			map.put("message", "该支付方式没有启用!");
			return map;
		}
		paymentForm.setPayTypeId(PayTypeEnum.WX_PAY.value());
		paymentForm.setPayTypeName(PayTypeEnum.WX_PAY.desc());
		
		String userId = paymentForm.getUserId();
		String outTradeNo = paymentForm.getSubSettlementSn();
		
		// 待请求参数数组
		StringBuffer sbHtml = new StringBuffer(300);
		String token = RandomStringUtils.randomNumeric(4);
		

		Long validateTime = System.currentTimeMillis(); // 增加链接校验时间
		
		SortedMap<String,Object> params=new TreeMap<String,Object>();
		params.put("token", token);
		params.put("outTradeNo", outTradeNo);
		params.put("userId", userId);
		params.put("openId", paymentForm.getOpenId());
		params.put("validateTime", validateTime);
		params.put("subjects", paymentForm.getSubject());
		params.put("key",propertiesUtil.getWeiXinKey());
		params.put("showUrl",paymentForm.getShowUrl());
		
		System.out.println("-----微信支付请求加密参数:"+JSONUtil.getJson(params));
		
		String secret =MD5Util.createSign(params);
		
		String subject = paymentForm.getSubject();
		try {
			subject = URLEncoder.encode(paymentForm.getSubject(), "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String weixinGateway=propertiesUtil.getWeiXinDomainName()+"/wxpay/weixinJSBridge/WX_PAY";
		sbHtml.append("<form id='payForm' action= '").append(weixinGateway).append("' method='post'> ");
		sbHtml.append(" <input type='hidden' name='token' value='").append(token).append("' >");
		sbHtml.append(" <input type='hidden' name='secret' value='").append(secret).append("' >");
		sbHtml.append(" <input type='hidden' name='validateTime' value='").append(validateTime).append("' >");
		sbHtml.append(" <input type='hidden' name='outTradeNo' value='").append(outTradeNo).append("' >");
		sbHtml.append(" <input type='hidden' name='userId' value='").append(userId).append("' >");
		sbHtml.append(" <input type='hidden' name='openId' value='").append(paymentForm.getOpenId()).append("' >");
		sbHtml.append(" <input type='hidden' name='subjects' value='").append(subject).append("' >");
		sbHtml.append(" <input type='hidden' name='showUrl' value='").append(paymentForm.getShowUrl()).append("' >");
		sbHtml.append("</form>");
		sbHtml.append("<script>document.forms['payForm'].submit();</script>");
		 
		map.put("result", true);
		map.put("message", sbHtml.toString());
		
		return map;
	}
	


}
