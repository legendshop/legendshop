package com.legendshop.payment.weixin.enums;


/**
 * @功能说明: 微信交易状态枚举类
 * @创建者: Tony
 * @版本:V1.0
 */
public enum WeixinTradeStateEnum {

	SUCCESS("成功"), FAIL("失败");

	/** 描述 */
	private String desc;

	private WeixinTradeStateEnum(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public static WeixinTradeStateEnum getEnum(String name) {
		WeixinTradeStateEnum[] arry = WeixinTradeStateEnum.values();
		for (int i = 0; i < arry.length; i++) {
			if (arry[i].name().equalsIgnoreCase(name)) {
				return arry[i];
			}
		}
		return null;
	}
	
}
