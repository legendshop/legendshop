package com.legendshop.payment.unionPay.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;


/**
 * 
 * @author Tony
 *
 */
public class AcpSdkUtils  {
	
	public AcpSdkUtils(){}
	
	private static Map<String,String> map=Collections.synchronizedMap(new HashMap<String, String>());
	static{
		try {
			Properties props = new Properties();
			props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("weibo-config.properties"));
			Set<Map.Entry<Object,Object>> entrys= props.entrySet();
			for (Entry<Object, Object> entry : entrys) {
				String key=(String) entry.getKey();
				String value=(String) entry.getValue();
				map.put(key, value);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String getValue(String key){
		return map.get(key);
	}
	
}
