package com.legendshop.payment.weixin.util;

import java.security.MessageDigest;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;

import com.legendshop.payment.tenpay.util.TenpayMD5Util;

/*
'============================================================================
'api说明：
'createSHA1Sign创建签名SHA1
'getSha1()Sha1签名
'============================================================================
'*/
public class Sha1Util {
	
	public static String getNonceStr() {
		Random random = new Random();
		return TenpayMD5Util.MD5Encode(String.valueOf(random.nextInt(10000)), "UTF-8");
	}
	public static String getTimeStamp() {
		return String.valueOf(System.currentTimeMillis() / 1000);
	}
	   //创建签名SHA1
		public static String createSHA1Sign(SortedMap<String, String> signParams) throws Exception {
			StringBuffer sb = new StringBuffer();
			Set es = signParams.entrySet();
			Iterator it = es.iterator();
			while (it.hasNext()) {
				Map.Entry entry = (Map.Entry) it.next();
				String k = (String) entry.getKey();
				String v = (String) entry.getValue();
				sb.append(k + "=" + v + "&");
				//要采用URLENCODER的原始值！
			}
			String params = sb.substring(0, sb.lastIndexOf("&"));
			System.out.println("sha1 sb:" + params);
			
			return getSha1(params);
		}
	
		//Sha1签名
		public static String getSha1(String str) {
			if (str == null || str.length() == 0) {
				return null;
			}
			char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
					'a', 'b', 'c', 'd', 'e', 'f' };

			try {
				MessageDigest mdTemp = MessageDigest.getInstance("SHA1");
				mdTemp.update(str.getBytes("GBK"));

				byte[] md = mdTemp.digest();
				int j = md.length;
				char buf[] = new char[j * 2];
				int k = 0;
				for (int i = 0; i < j; i++) {
					byte byte0 = md[i];
					buf[k++] = hexDigits[byte0 >>> 4 & 0xf];
					buf[k++] = hexDigits[byte0 & 0xf];
				}
				return new String(buf);
			} catch (Exception e) {
				return null;
			}
		}
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println(DigestUtils.sha1Hex("appid=wxb51f5dc25bda937c&appkey=o757Ysf2UH3sLki48XhZdFjhK2oxo2kCdcy8dt7OwAbedUfj4iMqSWSGKhmmhwqBO7Tq5MiUMLNT79qJnPm2eu0f9pyoSZMV4bBAnyi8He4I4gAhHEKJWiyB7KOaqWUo&noncestr=a787f02ed34fd886eb6d49e60d9c9120&package=bank_type=WX&body=1&fee_type=1&input_charset=GBK&notify_url=http%3A%2F%2Fjyxc.cnftjc.com%2Fwechat%2Fnotify%2F&out_trade_no=1345131923&partner=1219989201&spbill_create_ip=127.0.0.1&total_fee=1&sign=72FE5CD0C608EC8C53F15A92C08C1212&timestamp=1406699113")); 
	}
}
