package com.legendshop.payment.util;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.base.util.MerchantApiUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.util.MD5Util;

/**
 * 钱包支付的验证回调
 * 
 * @author Tony哥
 * 
 */
public class FullPayVerifyStrategyImpl implements PayVerifyStrategy {

	@Override
	public boolean verifyNotify(JSONObject jsonObject, Map<String, String> notifyMap) {

		// 先进行校验，是否是微信服务器返回的信息
		String signStr=notifyMap.get("sign");
		String token=notifyMap.get("token");

		// 用于验签
		Map<String , Object>  parameters = new HashMap<String, Object>();
		for (String keyValue : notifyMap.keySet()) {
			/** 输出返回的订单支付信息 */
			if (!"sign".equals(keyValue)) {
				parameters.put(keyValue, notifyMap.get(keyValue));
			}
		}
		
		String paySecret=MD5Util.toMD5(Constants._MD5 + token);
		boolean verify_result= MerchantApiUtil.isRightSign(parameters, paySecret, signStr);
		PaymentLog.info("钱包全额支付的验证回调数据校验 verify_result:{}" ,verify_result);
		return verify_result;
	}
	

}
