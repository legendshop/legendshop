package com.legendshop.payment.weixin.util;

public class WeiXinPayConfig {

	/** 微信公众号APPID */
	public static String APPID = "APPID";

	/** 微信公众号绑定的商户号 */
	public static String MCH_ID = "MCH_ID";

	public static String APPSECRET = "APPSCECRET";
	
	public static String PARTNERKEY = "PARTNERKEY";

	public static String TOKEN = "TOKEN";

	// 字符编码格式 目前支持 gbk 或 utf-8
	/** The input_charset. */
	public static String input_charset = "utf-8";
	
	public static final  String unifiedorder_url = "https://api.mch.weixin.qq.com/pay/unifiedorder" ;
	
	/** 申请退款 */
	public final static String refundURL = "https://api.mch.weixin.qq.com/secapi/pay/refund";
	
}
