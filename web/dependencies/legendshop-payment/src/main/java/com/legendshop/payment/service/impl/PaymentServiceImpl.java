/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.payment.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import com.legendshop.model.constant.PaymentProcessorEnum;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.spi.service.PaymentProcessor;
import com.legendshop.spi.service.PaymentService;

@Service
public class PaymentServiceImpl implements PaymentService {

  /** The log. */
  private static Logger log = LoggerFactory.getLogger(PaymentServiceImpl.class);

  @Override
  public Map<String, Object> payto(PaymentProcessorEnum processorEnum, SysPaymentForm paymentForm) {
    PaymentProcessor processor = processorMap.get(processorEnum.value());
    if (processor == null) {
      log.warn("PaymentProcessor processor is null by processorTypeId = {}  ", processorEnum.value());
      return null;
    }
    return processor.payto(paymentForm);
  }

  public void setProcessorMap(Map<String, PaymentProcessor> processorMap) {
    this.processorMap = processorMap;
  }

  @Resource(name = "aliPayProcessor")
  PaymentProcessor aliPayProcessor;

  @Resource(name = "tenDirectPayProcessor")
  PaymentProcessor tenDirectPayProcessor;

  @Resource(name = "unionPayProcessor")
  PaymentProcessor unionPayProcessor;

  @Resource(name = "tongLianPayProcessor")
  PaymentProcessor tongLianPayProcessor;

  @Resource(name = "tlShouYinBaoPayProcessor")
  PaymentProcessor tlShouYinBaoPayProcessor;

  @Resource(name = "weXinSacnCodePayProcessor")
  PaymentProcessor weXinSacnCodePayProcessor;

  @Resource(name = "weXinWapPayProcessor")
  PaymentProcessor weXinWapPayProcessor;
  
  @Resource(name = "weXinH5PayProcessor")
  PaymentProcessor weXinH5PayProcessor;
  
  @Resource(name = "weXinAppPayProcessor")
  PaymentProcessor weXinAppPayProcessor;

  @Resource(name = "weXinMiniProgramPayProcessor")
  PaymentProcessor weXinMiniProgramPayProcessor;

  @Resource(name = "weXinBlendPayProcessor")
  PaymentProcessor weXinBlendPayProcessor;

  @Resource(name = "simulatePayProcessor")
  PaymentProcessor simulatePayProcessor;

  @Resource(name = "fullPayProcessor")
  PaymentProcessor fullPayProcessor;

  @Bean(name = "paymentProcessors")
  Map<String, PaymentProcessor> paymentProcessors() {
    Map<String, PaymentProcessor> map = new HashMap<String, PaymentProcessor>();
    map.put(PaymentProcessorEnum.ALP.value(), aliPayProcessor);
    map.put(PaymentProcessorEnum.TDP.value(), tenDirectPayProcessor);
    map.put(PaymentProcessorEnum.UNION_PAY.value(), unionPayProcessor);
    map.put(PaymentProcessorEnum.TONGLIAN_PAY.value(), tongLianPayProcessor);
    map.put(PaymentProcessorEnum.TONGLIAN_WAP_PAY.value(), tlShouYinBaoPayProcessor);
    map.put(PaymentProcessorEnum.WX_SACN_PAY.value(), weXinSacnCodePayProcessor);
    map.put(PaymentProcessorEnum.WX_PAY.value(), weXinWapPayProcessor);
    map.put(PaymentProcessorEnum.WX_H5_PAY.value(), weXinH5PayProcessor);
    map.put(PaymentProcessorEnum.WX_APP_PAY.value(), weXinAppPayProcessor);
    map.put(PaymentProcessorEnum.WX_MP_PAY.value(), weXinMiniProgramPayProcessor);
    map.put(PaymentProcessorEnum.WX_BLEND_PAY.value(), weXinBlendPayProcessor);
    map.put(PaymentProcessorEnum.SIMULATE_PAY.value(), simulatePayProcessor);
    map.put(PaymentProcessorEnum.FULL_PAY.value(), fullPayProcessor);
    return map;
  }

  /** 支付实现类 */
  @Resource(name = "paymentProcessors")
  private Map<String, PaymentProcessor> processorMap;
  
}
