package com.legendshop.payment.util;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.payment.tenpay.util.TenpayMD5Util;
import com.legendshop.payment.tenpay.util.TenPayConfig;

/**
 * 腾讯支付的验证回调
 * 
 * @author Tony
 * 
 */
public class TenPayVerifyStrategyImpl implements PayVerifyStrategy {

	@Override
	public boolean verifyNotify(JSONObject jsonObject, Map<String, String> notifyMap) {

		String key = jsonObject.getString(TenPayConfig.KEY);
		
		// 用于验签
		SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();
		for (Object keyValue : notifyMap.keySet()) {
			parameters.put(keyValue, notifyMap.get(keyValue));
		}
		return isTenpaySign(key, parameters);
	}
	
	private boolean isTenpaySign(String key,SortedMap<Object, Object> parameters) {
		StringBuilder sb = new StringBuilder();
		String sign=parameters.get("sign").toString();
		Set<Entry<Object, Object>> es =parameters.entrySet();
		Iterator<Entry<Object, Object>> it = es.iterator();
		while(it.hasNext()) {
			Map.Entry<Object, Object> entry = (Map.Entry<Object, Object>)it.next();
			String k = (String)entry.getKey();
			String v = (String)entry.getValue();
			if(!"sign".equals(k) && null != v && !"".equals(v)) {
//				if(!k.equals("PcacheTime")){
					sb.append(k + "=" + v + "&");
//				}
			}
		}
		sb.append("key=" +key);
		//算出摘要
		String _sign = TenpayMD5Util.MD5Encode(sb.toString(), TenPayConfig.input_charset).toLowerCase();
		String tenpaySign = sign.toLowerCase();
		boolean verify_result=tenpaySign.equals(_sign);
		PaymentLog.info("腾讯支付的验证回调数据校验 verify_result:{}" ,verify_result);
		return verify_result;
	}

}
