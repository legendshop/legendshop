package com.legendshop.payment.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import com.legendshop.model.dto.BackRefundResponseDto;
import com.legendshop.spi.service.BackRefundProcessService;
import com.legendshop.spi.service.BackRefundProcessor;

/**
 * 原路退款策略上下文
 *
 */
@Service("backRefundProcessService")
public class BackRefundProcessServiceImpl implements BackRefundProcessService {

  /**
   * 调用对应支付平台组装支付请求报文
   * 
   * @param payType
   * @param params
   * @return
   */
  @Override
  public BackRefundResponseDto backRefund(String subSettlementType, String backRefund) {
    BackRefundProcessor backRefundProcessor = processorMap.get(subSettlementType);
    return backRefundProcessor.backRefund(backRefund);
  }

  @Resource(name = "commonGroupBackRefundProcessor")
  BackRefundProcessor commonGroupBackRefundProcessor;

  @Resource(name = "mergeGroupBackRefundProcessor")
  BackRefundProcessor mergeGroupBackRefundProcessor;

  @Resource(name = "groupExemptionBackRefundProcessor")
  BackRefundProcessor groupExemptionBackRefundProcessor;

  @Bean(name = "backRefundProcessor")
  Map<String, BackRefundProcessor> processorMap() {
    Map<String, BackRefundProcessor> map = new HashMap<String, BackRefundProcessor>();
    map.put("COMMON_GROUP", commonGroupBackRefundProcessor);
    map.put("MERGE_GROUP", mergeGroupBackRefundProcessor);
    map.put("GROUP_EXEMPTION", groupExemptionBackRefundProcessor);
    return map;
  }
  
  @Resource(name = "backRefundProcessor")
  private Map<String, BackRefundProcessor> processorMap;

}
