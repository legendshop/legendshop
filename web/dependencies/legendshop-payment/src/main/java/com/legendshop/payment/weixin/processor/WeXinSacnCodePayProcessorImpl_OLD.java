package com.legendshop.payment.weixin.processor;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.model.form.SysSubReturnForm;
import com.legendshop.payment.weixin.enums.WeiXinTradeTypeEnum;
import com.legendshop.payment.weixin.util.RequestHandler;
import com.legendshop.payment.weixin.util.WeiXinPayConfig;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PaymentProcessor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.RandomStringUtils;
import com.legendshop.web.util.WeiXinUtil;

/**
 * 
 * @author Tony哥 ----------请勿删除
 *
 */
@Deprecated
public class WeXinSacnCodePayProcessorImpl_OLD implements PaymentProcessor {
	
	
	private PayTypeService payTypeService;

	@Override
	public Map<String,Object> payto(SysPaymentForm paymentForm) {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", false);
		
		PayType payType = payTypeService.getPayTypeById(PayTypeEnum.WX_PAY.value()); //这种方式是卖家中心调用支付服务
		if (payType == null || AppUtils.isBlank(payType.getPaymentConfig())) {
			map.put("message", "支付方式不存在,请设置支付方式");
			return map;
		} else if (payType.getIsEnable() == Constants.OFFLINE) {
			map.put("message", "该支付方式没有启用!");
			return map;
		}
		paymentForm.setPayTypeId(PayTypeEnum.WX_PAY.value());
		paymentForm.setPayTypeName(PayTypeEnum.WX_PAY.desc());
		
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		
		/** 微信公众号APPID */
		 String appid = jsonObject.getString(WeiXinPayConfig.APPID);
		 String mch_id = jsonObject.getString(WeiXinPayConfig.MCH_ID);
		 String app_secret = jsonObject.getString(WeiXinPayConfig.APPSECRET);
		 String partnerkey = jsonObject.getString(WeiXinPayConfig.PARTNERKEY);
		 String token = jsonObject.getString(WeiXinPayConfig.TOKEN);
		
		if(AppUtils.isBlank(appid) || AppUtils.isBlank(mch_id) || AppUtils.isBlank(app_secret) || AppUtils.isBlank(partnerkey)
				|| AppUtils.isBlank(token)
				){
			map.put("message", "请设置支付的密钥信息!");
			return map;
		}
		paymentForm.setIp("119.131.117.242");
		
		/** The notify_url 交易过程中服务器通知的页面 要用 http://格式的完整路径，不允许加?id=123这类自定义参数. */
		String notifyUrl = paymentForm.getDomainName() + "/payNotify/notify/"+ PayTypeEnum.WX_PAY.value();

		// 总金额以分为单位，不带小数点
		String totalFee = String.valueOf(new BigDecimal(paymentForm.getTotalAmount())
		.setScale(2,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).intValue());
				
		
        String trade_type = WeiXinTradeTypeEnum.NATIVE.name();
		// 随机字符串
		String nonce_str = RandomStringUtils.randomNumeric(8);
		// 商品描述根据情况修改
		String body =paymentForm.getSubject();
		// 商户订单号
		String out_trade_no = paymentForm.getSubSettlementSn();
		
		SortedMap<String, String> packageParams = new TreeMap<String, String>();
		packageParams.put("appid", appid);
		packageParams.put("mch_id",mch_id);
		packageParams.put("nonce_str", nonce_str);
		packageParams.put("body", body);
		packageParams.put("out_trade_no", out_trade_no);
		// 这里写的金额为1 分到时修改
		packageParams.put("total_fee", totalFee);
		packageParams.put("spbill_create_ip", paymentForm.getIp());
		packageParams.put("notify_url", notifyUrl);
		packageParams.put("trade_type", trade_type);
		
		RequestHandler reqHandler = new RequestHandler();
	    reqHandler.init(token, appid, app_secret, partnerkey);
	    
	    String sign = reqHandler.createSign(packageParams);
	    
	    StringBuilder builder=new StringBuilder();
		builder.append("<?xml version='1.0' encoding='UTF-8' standalone='yes' ?><xml>");  
		builder.append("<appid>").append(appid).append("</appid>");
		builder.append("<mch_id>").append(mch_id).append("</mch_id>");
		builder.append("<nonce_str>").append(nonce_str).append("</nonce_str>");
		builder.append("<sign>").append(sign).append("</sign>");
		builder.append("<body>").append("<![CDATA[").append(body).append("]]>").append("</body>");
		builder.append("<out_trade_no>").append(out_trade_no).append("</out_trade_no>");
		builder.append("<total_fee>").append(totalFee).append("</total_fee>");
		builder.append("<spbill_create_ip>").append(paymentForm.getIp()).append("</spbill_create_ip>");
		builder.append("<notify_url>").append(notifyUrl).append("</notify_url>");
		builder.append("<trade_type>").append(trade_type).append("</trade_type>");
		builder.append("</xml>");
		
		String code_url = "";
		Map<String, String> form = null;
		try {
			form = WeiXinUtil.httpPostXML(WeiXinPayConfig.unifiedorder_url,
					new String(builder.toString().getBytes("UTF-8"), "ISO-8859-1"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		if (map != null) {
			
			String return_code = form.get("return_code"); // 返回状态码
			String result_code= form.get("result_code"); 
			if("FAIL".equals(return_code)){
				map.put("message", form.get("return_msg"));
				return map;
			}
			
			if("FAIL".equals(result_code)){
				map.put("message", form.get("err_code_des"));
				return map;
			}
			
			code_url = form.get("code_url");
		}
		map.put("code_url", code_url);
		map.put("result", true);
		return map;
	}
	
	
	public String triggerReturn(SysSubReturnForm returnForm) {
		return null;
	}


	public void setPayTypeService(PayTypeService payTypeService) {
		this.payTypeService = payTypeService;
	}
	
}
