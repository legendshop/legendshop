package com.legendshop.payment.controller;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.constant.*;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.SubSettlement;
import com.legendshop.model.form.BankCallbackForm;
import com.legendshop.payment.alipay.enums.TradeStatusEnum;
import com.legendshop.payment.util.PayVerifyStrategyContext;
import com.legendshop.spi.manager.PaymentResolverManager;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.SubService;
import com.legendshop.spi.service.SubSettlementService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.web.helper.IPHelper;
import com.legendshop.web.util.WeiXinUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 支付网关回调中心
 * 特别提醒：商户系统对于支付结果通知的内容一定要做签名验证,并校验返回的订单金额是否与商户侧的订单金额一致，防止数据泄漏导致出现“假通知”，造成资金损失。
 * @author Tony
 */
@Controller
@RequestMapping(value = "/payNotify")
public class PaymentNotifyController {

	@Autowired
	private PayTypeService payTypeService;

	@Autowired
	private SubSettlementService subSettlementService;

	@Autowired
	private PaymentResolverManager paymentResolverManager;

	@Autowired
	private SubService subService;

	@Autowired
	private PropertiesUtil propertiesUtil;

	/**
	 * 支付平台异步通知.
	 *
	 * @param payWayCode
	 * @param httpServletRequest
	 * @param httpServletResponse
	 * @throws Exception
	 */
	@RequestMapping(value="/notify/{payWayCode}")
	public void notify(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, @PathVariable("payWayCode") String payWayCode)
			throws Exception {

		//构造Dto
		String ip = IPHelper.getIpAddr(httpServletRequest);

		PaymentLog.info("##################### 支付网关回调 notify #######################");

		PaymentLog.info("notify,payWayCode:{} ,前台接收报文返回开始  IP = {}" , payWayCode,ip);

		String printSuccessStr = "";
		String printFailStr = "";
		Map<String, String> notifyMap = new HashMap<String, String>();
		BankCallbackForm bankCallbackForm = new BankCallbackForm();
		bankCallbackForm.setUpdateStatus(OrderStatusEnum.PADYED);//期望下单之后的状态为，预售订单的订金除外
		bankCallbackForm.setValidateStatus(OrderStatusEnum.UNPAY);//下单前现在的状态位
		String totalFee="0.00"; //订单总金额，单位为分
		/*  微信支付 */
		if (PayTypeEnum.isWeiXin(payWayCode)) {
			/** 支付成功后，微信回调返回的信息 */
			notifyMap = WeiXinUtil.parseXml(httpServletRequest);

			PaymentLog.info("微信支付成功后，微信回调返回的信息 notifyMap:{}" ,JSONUtil.getJson(notifyMap) );

			printSuccessStr = "<xml>\n" + "  <return_code><![CDATA[SUCCESS]]></return_code>\n" + "  <return_msg><![CDATA[OK]]></return_msg>\n"
					+ "</xml>";

			printFailStr = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>" + "<return_msg><![CDATA[支付失败]]></return_msg>" + "</xml> ";

			PaymentLog.log(" notify 前台接收报文返回开始  IP = {} ", ip);

			bankCallbackForm.setOutOrderNo(notifyMap.get("out_trade_no"));
			bankCallbackForm.setFlowTradeNo(notifyMap.get("transaction_id"));
			totalFee=notifyMap.get("total_fee");  //订单总金额，单位为分

			String return_code = notifyMap.get("return_code"); // 返回状态码
			PaymentLog.info("微信支付成功后，微信回调返回的信息 return_code:{}" ,return_code);
			if (!"SUCCESS".equals(return_code)) {
				httpServletResponse.getWriter().print(printFailStr);
				return;
			}
			String result_code = notifyMap.get("result_code"); // 返回状态码
			PaymentLog.info("微信支付成功后，微信回调返回的信息 result_code:{}" ,result_code);
			if (!"SUCCESS".equals(result_code)) {
				PaymentLog.info("微信支付成功后，微信回调返回的信息 err_code:{}" ,notifyMap.get("err_code"));
				PaymentLog.info("微信支付成功后，微信回调返回的信息 err_code_des:{}" ,notifyMap.get("err_code_des"));
				httpServletResponse.getWriter().print(printFailStr);
				return;
			}

			/*  支付宝支付 */
		} else if (PayTypeEnum.isAliPay(payWayCode)) {
			Map<String, String[]> requestParams = httpServletRequest.getParameterMap();
			notifyMap = parseNotifyMsg(requestParams);
			printSuccessStr = "success";
			printFailStr = "fail";

			PaymentLog.info("支付宝支付成功后，微信回调返回的信息 notifyMap:{}" ,JSONUtil.getJson(notifyMap));

			String trade_status = notifyMap.get("trade_status");
			if (!TradeStatusEnum.TRADE_SUCCESS.getDesc().equals(trade_status) && !TradeStatusEnum.TRADE_FINISHED.getDesc().equals(trade_status)) {
				PaymentLog.info("pay notify  trade_status={}",trade_status);
				httpServletResponse.getWriter().print(printFailStr);
				return;
			}
			bankCallbackForm.setOutOrderNo(notifyMap.get("out_trade_no"));
			bankCallbackForm.setFlowTradeNo(notifyMap.get("trade_no"));

			totalFee = String.valueOf(new BigDecimal(notifyMap.get("total_amount")).setScale(2, BigDecimal.ROUND_HALF_UP)
					.multiply(new BigDecimal(100)).intValue());
			/*  财付通支付 */
		} else if (PayTypeEnum.TENPAY.value().equals(payWayCode)) {
			Map<String, String[]> requestParams = httpServletRequest.getParameterMap();
			notifyMap = parseNotifyMsg(requestParams);
			// 支付结果
			PaymentLog.info("腾讯支付成功后，微信回调返回的信息 notifyMap:{}" ,JSONUtil.getJson(notifyMap));

			String trade_state = notifyMap.get("trade_state");
			if (!"0".equals(trade_state)) {
				PaymentLog.info("pay notify  trade_state={}",trade_state);
				httpServletResponse.getWriter().print(printFailStr);
				return;
			}
			bankCallbackForm.setOutOrderNo(notifyMap.get("out_trade_no"));
			bankCallbackForm.setFlowTradeNo(notifyMap.get("transaction_id"));
			totalFee = notifyMap.get("total_fee"); //订单总金额，单位为分

			printSuccessStr = "success";
			printFailStr = "fail";
			/*  银联支付 */
		}else if(PayTypeEnum.UNIONPAY.value().equals(payWayCode)){ //银联支付
			Map<String, String[]> requestParams = httpServletRequest.getParameterMap();
			notifyMap = parseNotifyMsg(requestParams);
			// 支付结果
			PaymentLog.info("UNIONPAY 支付成功后，回调返回的信息 notifyMap:{}" ,JSONUtil.getJson(notifyMap));

			String orderId=notifyMap.get("orderId");
			String queryId=notifyMap.get("queryId");

			bankCallbackForm.setOutOrderNo(orderId);
			bankCallbackForm.setFlowTradeNo(queryId);
			totalFee=notifyMap.get("totalFee"); //订单总金额，单位为分

			printSuccessStr = "ok";
			printFailStr = "fail";
		}
		/*  通联支付*/
		else if(PayTypeEnum.TONGLIAN_PAY.value().equals(payWayCode)
				|| PayTypeEnum.TONGLIAN_CROSSBORDER_PAY.value().equals(payWayCode)){ //通联支付
			Map<String, String[]> requestParams = httpServletRequest.getParameterMap();
			notifyMap = parseNotifyMsg(requestParams);
			// 支付结果
			PaymentLog.info(" TONGLIAN_PAY 支付成功后，回调返回的信息 notifyMap:{}" ,JSONUtil.getJson(notifyMap));
			String paymentOrderId=notifyMap.get("paymentOrderId");
			String orderNo=notifyMap.get("orderNo");
			String orderAmount=notifyMap.get("orderAmount");
			System.out.println(orderAmount);
			totalFee=notifyMap.get("payAmount");

			bankCallbackForm.setOutOrderNo(orderNo);
			bankCallbackForm.setFlowTradeNo(paymentOrderId);

			printSuccessStr = "ok";
			printFailStr = "fail";
		}
		/*  快钱支付*/
		else if(PayTypeEnum.KQ_PAY.value().equals(payWayCode)){ // 快钱支付
			Map<String, String[]> requestParams = httpServletRequest.getParameterMap();
			notifyMap = parseNotifyMsg(requestParams);
			// 支付结果
			PaymentLog.info(" KQ_PAY 支付成功后，回调返回的信息 notifyMap:{}" ,JSONUtil.getJson(notifyMap));
			//获取商户订单号
			String orderId=notifyMap.get("orderId");
			//获取快钱交易号
			///获取该交易在快钱的交易号
			String dealId=notifyMap.get("dealId");
			//获取原始订单金额
			///订单提交到快钱时的金额，单位为分。
			///比方2 ，代表0.02元
			totalFee=notifyMap.get("orderAmount");

			bankCallbackForm.setOutOrderNo(orderId);
			bankCallbackForm.setFlowTradeNo(dealId);

			printSuccessStr = "ok";
			printFailStr = "fail";

		}else if (PayTypeEnum.SIMULATE_PAY.value().equals(payWayCode)) {//模拟支付

			Map<String, String[]> requestParams = httpServletRequest.getParameterMap();
			notifyMap = parseNotifyMsg(requestParams);

			bankCallbackForm.setOutOrderNo(notifyMap.get("out_trade_no"));
			bankCallbackForm.setFlowTradeNo(notifyMap.get("trade_no"));
			totalFee = String.valueOf(new BigDecimal(notifyMap.get("total_fee")).setScale(2, BigDecimal.ROUND_HALF_UP)
					.multiply(new BigDecimal(100)).intValue());

			printSuccessStr = "ok";
			printFailStr = "fail";

		}else if (PayTypeEnum.FULL_PAY.value().equals(payWayCode)) {//预存款支付

			Map<String, String[]> requestParams = httpServletRequest.getParameterMap();
			notifyMap = parseNotifyMsg(requestParams);

			bankCallbackForm.setOutOrderNo(notifyMap.get("out_trade_no"));
			bankCallbackForm.setFlowTradeNo(notifyMap.get("trade_no"));
			totalFee = String.valueOf(new BigDecimal(notifyMap.get("total_fee")).setScale(2, BigDecimal.ROUND_HALF_UP)
					.multiply(new BigDecimal(100)).intValue());

			printSuccessStr = "ok";
			printFailStr = "fail";
		}

		if (AppUtils.isBlank(bankCallbackForm.getOutOrderNo())) {
			PaymentLog.error("pay notify error OutOrderNo is null ");
			httpServletResponse.getWriter().print(printFailStr);
			return;
		}
		bankCallbackForm.setTotalFee(totalFee);

		SubSettlement subSettlement = subSettlementService.getSubSettlementBySn(bankCallbackForm.getOutOrderNo());
		if (AppUtils.isBlank(subSettlement)) {
			PaymentLog.error("pay notify error, subSettlement is null by SubSettlementBySn={}",bankCallbackForm.getOutOrderNo());
			httpServletResponse.getWriter().print(printFailStr);
			return;
		} else if (subSettlement.getIsClearing()) {
			PaymentLog.info("pay notify subSettlement is IsClearing=true ");
			httpServletResponse.getWriter().print(printSuccessStr);
			return;
		}

		/**
		 * 并校验返回的订单金额是否与商户侧的订单金额一致，
		 */
		String _totalFee = String.valueOf(new BigDecimal(subSettlement.getCashAmount()).setScale(2, BigDecimal.ROUND_HALF_UP)
				.multiply(new BigDecimal(100)).intValue());

		if(!_totalFee.equals(totalFee)){
			PaymentLog.error("支付失败,返回的订单金额与商户侧的订单金额不一致 _totalFee={},totalFee={}",_totalFee,totalFee);
			httpServletResponse.getWriter().print(printFailStr);
			return;
		}

		JSONObject jsonObject=null;
		if(!PayTypeEnum.FULL_PAY.value().equals(payWayCode)){
			PayType payType = payTypeService.getPayTypeById(payWayCode);
			if (payType == null) {
				PaymentLog.error("pay notify error, payType is null by payWayCode={} ",payWayCode);
				httpServletResponse.getWriter().print(printFailStr);
				return;
			} else if (payType.getIsEnable() == Constants.OFFLINE) {
				PaymentLog.error("pay notify error, payType OFFLINE by payWayCode={} ",payWayCode);
				httpServletResponse.getWriter().print(printFailStr);
				return;
			}

			jsonObject= JSONObject.parseObject(payType.getPaymentConfig());
		}

		/**
		 * 支付回调签名验证
		 */
		PaymentLog.info(" ############### 支付网关业务处理 ################### ");
		PaymentLog.info(" ############### 支付网关业务处理  subSettlement={} ################### ",JSONUtil.getJson(subSettlement));
		PaymentLog.info(" ############### 支付网关业务处理  bankCallbackForm={} ################### ",JSONUtil.getJson(bankCallbackForm));

		PayVerifyStrategyContext context = new PayVerifyStrategyContext();
		boolean verifyNotify = context.verifyNotify(payWayCode, jsonObject, notifyMap);
		if (!verifyNotify) {
			PaymentLog.error("pay notify error, verifyNotify={},by payWayCode={} ",verifyNotify,payWayCode);
			httpServletResponse.getWriter().print(printFailStr);
			return;
		}

		// TODO 这里可以组装参数 通过MQ 发送

		// 处理业务逻辑
		try {
			paymentResolverManager.doBankCallback(subSettlement, bankCallbackForm);
			PaymentLog.info(" ############### 支付网关业务处理 ################### ");
			httpServletResponse.getWriter().print(printSuccessStr);
			return;
		} catch (Exception e) {
			PaymentLog.error("pay notify , error {} ",e);
			httpServletResponse.getWriter().print(printFailStr);
			return;
		}finally{
			PaymentLog.info(" into notify,payWayCode:{} ----> " , payWayCode);
		}

	}

	/**
	 * 页面同步回调.
	 *
	 * @param payWayCode
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/response/{payWayCode}",method=RequestMethod.GET)
	public String response(HttpServletRequest request,
			@PathVariable("payWayCode") String payWayCode, Model model) throws Exception {

		Map<String, String> notifyMap = parseNotifyMsg(request.getParameterMap());

        PaymentLog.info("##################### 支付网关回调 response #######################");

        //构造Dto
      	String ip = IPHelper.getIpAddr(request);
		PaymentLog.info("notify,payWayCode:{} ,前台接收报文返回开始  IP = {}" , payWayCode,ip);
		PaymentLog.info("支付网关回调   response 返回的信息 notifyMap:{}" ,JSONUtil.getJson(notifyMap));

		if(AppUtils.isBlank(notifyMap)){
			PaymentLog.error(" 支付网关业务处理 response error notifyMap is null by payWayCode={} ",payWayCode);

			return "redirect:/p/payment/payError?message="+URLEncoder.encode("数据不规范,非法操作", "utf-8"); //TODO 这种方式 是多模版的导致的
		}

		BankCallbackForm bankCallbackForm = new BankCallbackForm();
		bankCallbackForm.setUpdateStatus(OrderStatusEnum.PADYED);//期望下单之后的状态为，预售订单的订金除外
		bankCallbackForm.setValidateStatus(OrderStatusEnum.UNPAY);//下单前现在的状态位

		String totalFee="0.00"; //订单总金额，单位为分

		if (PayTypeEnum.isAliPay(payWayCode)) {
			bankCallbackForm.setOutOrderNo(notifyMap.get("out_trade_no"));
			bankCallbackForm.setFlowTradeNo(notifyMap.get("trade_no"));
			bankCallbackForm.setTotalFee(notifyMap.get("total_amount"));

			totalFee = String.valueOf(new BigDecimal(notifyMap.get("total_amount")).setScale(2, BigDecimal.ROUND_HALF_UP)
					.multiply(new BigDecimal(100)).intValue());


		} else if (PayTypeEnum.TENPAY.value().equals(payWayCode)) {
			// 支付结果
			bankCallbackForm.setOutOrderNo(notifyMap.get("out_trade_no"));
			bankCallbackForm.setFlowTradeNo(notifyMap.get("transaction_id"));

			totalFee=notifyMap.get("total_fee");//订单总金额，单位为分

		}else if(PayTypeEnum.UNIONPAY.value().equals(payWayCode)){ //银联支付
			String orderId=notifyMap.get("orderId");
			String queryId=notifyMap.get("queryId");
			bankCallbackForm.setOutOrderNo(orderId);
			bankCallbackForm.setFlowTradeNo(queryId);
			totalFee=notifyMap.get("totalFee"); //订单总金额，单位为分
		}
		/*  通联支付*/
		else if(PayTypeEnum.TONGLIAN_PAY.value().equals(payWayCode)
				|| PayTypeEnum.TONGLIAN_CROSSBORDER_PAY.value().equals(payWayCode)){ //通联支付
			// 支付结果
			String paymentOrderId=notifyMap.get("paymentOrderId");
			String orderNo=notifyMap.get("orderNo");
			String orderAmount=notifyMap.get("orderAmount"); //订单金额
			totalFee=notifyMap.get("payAmount");//支付金额
			bankCallbackForm.setOutOrderNo(orderNo);
			bankCallbackForm.setFlowTradeNo(paymentOrderId);
		}
		/*  快钱支付*/
		else if(PayTypeEnum.KQ_PAY.value().equals(payWayCode)){ // 快钱支付
			// 支付结果
			//获取商户订单号
			String orderId=notifyMap.get("orderId");
			//获取快钱交易号
			///获取该交易在快钱的交易号
			String dealId=notifyMap.get("dealId");
			//获取原始订单金额
			///订单提交到快钱时的金额，单位为分。
			///比方2 ，代表0.02元
			totalFee=notifyMap.get("orderAmount");

			bankCallbackForm.setOutOrderNo(orderId);
			bankCallbackForm.setFlowTradeNo(dealId);
		}
		else if (PayTypeEnum.SIMULATE_PAY.value().equals(payWayCode)) {//模拟支付
			bankCallbackForm.setOutOrderNo(notifyMap.get("out_trade_no"));
			bankCallbackForm.setFlowTradeNo(notifyMap.get("trade_no"));
			totalFee = String.valueOf(new BigDecimal(notifyMap.get("total_fee")).setScale(2, BigDecimal.ROUND_HALF_UP)
					.multiply(new BigDecimal(100)).intValue());

		}
		else if (PayTypeEnum.FULL_PAY.value().equals(payWayCode)) {//预存款支付
			bankCallbackForm.setOutOrderNo(notifyMap.get("out_trade_no"));
			bankCallbackForm.setFlowTradeNo(notifyMap.get("trade_no"));
			totalFee = String.valueOf(new BigDecimal(notifyMap.get("total_fee")).setScale(2, BigDecimal.ROUND_HALF_UP)
					.multiply(new BigDecimal(100)).intValue());
		}
		bankCallbackForm.setTotalFee(totalFee);

		SubSettlement subSettlement = subSettlementService.getSubSettlementBySn(bankCallbackForm.getOutOrderNo());
		if (AppUtils.isBlank(subSettlement)) {
			PaymentLog.error("支付网关业务处理 response error,  subSettlement is null by SubSettlementBySn={} ",bankCallbackForm.getOutOrderNo());
			return "redirect:/p/payment/payError?message="+URLEncoder.encode("支付失败,未找到支付单据信息", "utf-8");
		} else if (subSettlement.getIsClearing()) {
			PaymentLog.info("支付网关业务处理 response  ,subSettlement IsClearing=true SubSettlementBySn={} ",bankCallbackForm.getOutOrderNo());
			return goPayResultPage(subSettlement.getSettlementFrom(), subSettlement.getSubSettlementSn(), subSettlement.getType(), null);
		}

		if (AppUtils.isBlank(bankCallbackForm.getOutOrderNo())) {
			PaymentLog.error("支付网关业务处理 response error,  OutOrderNo is null");
			return goPayResultPage(subSettlement.getSettlementFrom(), null, null, "支付失败,参数有误");
		}

		/**
		 * 并校验返回的订单金额是否与商户侧的订单金额一致，
		 */
		String _totalFee = String.valueOf(new BigDecimal(subSettlement.getCashAmount()).setScale(2, BigDecimal.ROUND_HALF_UP)
				.multiply(new BigDecimal(100)).intValue());

		if(!_totalFee.equals(totalFee)){
			PaymentLog.error("支付网关业务处理 response 支付失败,返回的订单金额与商户侧的订单金额不一致 _totalFee={},totalFee={}",_totalFee,totalFee);
			return goPayResultPage(subSettlement.getSettlementFrom(), null, null, "支付失败,返回的订单金额与商户侧的订单金额不一致");
		}

		JSONObject jsonObject=null;
		if(!PayTypeEnum.FULL_PAY.value().equals(payWayCode)){
			PayType payType  = payTypeService.getPayTypeById(payWayCode);
			if ( payType == null ) {
				PaymentLog.error("pay notify error, payType is null by payWayCode={}",payWayCode);
				return goPayResultPage(subSettlement.getSettlementFrom(), null, null, "提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
			} else if ( payType.getIsEnable() == Constants.OFFLINE) {
				PaymentLog.error("pay notify error, payType OFFLINE by payWayCode={} ",payWayCode);
				return goPayResultPage(subSettlement.getSettlementFrom(), null, null, "提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
			}
			jsonObject= JSONObject.parseObject(payType.getPaymentConfig());
		}

		/**
		 * 支付回调签名验证
		 */
		PaymentLog.info(" ############### 支付网关业务处理 response  ################### ");
		PaymentLog.info(" ############### 支付网关业务处理  response subSettlement={} ################### ",JSONUtil.getJson(subSettlement));
		PaymentLog.info(" ############### 支付网关业务处理  response bankCallbackForm={} ################### ",JSONUtil.getJson(bankCallbackForm));

		PayVerifyStrategyContext context = new PayVerifyStrategyContext();
		boolean verifyNotify = context.verifyNotify(payWayCode, jsonObject, notifyMap);
		if (!verifyNotify) {
			PaymentLog.error("pay response , verifyNotify={} ",verifyNotify);
			return goPayResultPage(subSettlement.getSettlementFrom(), null, null, "提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
		}

		// 处理业务逻辑
		try {
			paymentResolverManager.doBankCallback(subSettlement, bankCallbackForm);
			PaymentLog.info(" ############### 支付网关业务处理 ################### ");
			PaymentLog.info(" ############### subSettlement ################### ",subSettlement.getPayPctType());
			return goPayResultPage(subSettlement.getSettlementFrom(), subSettlement.getSubSettlementSn(), subSettlement.getType(), null);
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
			PaymentLog.error("pay response , error {} ",e);
			return goPayResultPage(subSettlement.getSettlementFrom(), null, null, "提示信息： 银行交易错误,请记录您的支付订单号联系商家确认订单状态");
		}
	}

	/**
	 * 跳转支付结果, TODO 这段代码需要优化, 不能这么写
	 * @param paySource 支付来源
	 * @param subSettlementSn 交易单号
	 * @param subSettlementType 交易单据类型
	 * @param errorMsg 错误消息, 如果有则代表错误
	 * @return
	 */
	private String goPayResultPage(String paySource, String subSettlementSn, String subSettlementType, String errorMsg){

		String page = "redirect:";
		if(PaySourceEnum.isPC(paySource) ||PaySourceEnum.isWAPJSP(paySource)){//如果是PC端 或 h5 jsp
			if(AppUtils.isNotBlank(errorMsg)){//代表跳错误页面
				try {
					page = "redirect:/p/payment/payError?message="+URLEncoder.encode(errorMsg, "utf-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					return "redirect:/";
				}
			}else{//代表跳成功页面
				page = "redirect:/p/payment/returnPay?subSettlementSn=" + subSettlementSn + "&subSettlementType=" + subSettlementType;
			}
		}else{//说明是移动端
			String vueDomain = propertiesUtil.getVueDomainName();
			if(AppUtils.isNotBlank(errorMsg)){//代表跳错误页面
				try {
					page = "redirect:" + vueDomain + MobilePayResultEnum.COMMON_ORDER_RESULT.getAddr() + "?message=" + URLEncoder.encode(errorMsg, "utf-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					page = "redirect:" + vueDomain;
				}
			}else{
				if(SubSettlementTypeEnum.USER_ORDER_PAY.value().equals(subSettlementType)||SubSettlementTypeEnum.PRESELL_ORDER_PAY.value().equals(subSettlementType)){
					List<Sub> subs = subService.getSubBySubSettlementSn(subSettlementSn);
					if(AppUtils.isNotBlank(subs) && subs.size() == 1){
						Sub sub = subs.get(0);
						if(SubTypeEnum.MERGE_GROUP.value().equals(sub.getSubType())){//如果是拼团订单
							page = "redirect:" + vueDomain + MobilePayResultEnum.MARKETING_ORDER_RESULT.getAddr() + "?sn=" + sub.getSubNumber() + "&source=SUB";
						}else{
							page = "redirect:" + vueDomain + MobilePayResultEnum.COMMON_ORDER_RESULT.getAddr() + "?subSettlementSn=" + subSettlementSn + "&subSettlementType=" + subSettlementType;
						}
					}else{
						page = "redirect:" + vueDomain + MobilePayResultEnum.COMMON_ORDER_RESULT.getAddr() + "?subSettlementSn=" + subSettlementSn + "&subSettlementType=" + subSettlementType;
					}
				}else if(SubSettlementTypeEnum.USER_PREPAID_RECHARGE.value().equals(subSettlementType)){
					page = "redirect:" + vueDomain + MobilePayResultEnum.PRE_DEPOSIT_RESULT.getAddr() + "?subSettlementSn="+subSettlementSn+"&subSettlementType="+subSettlementType;
				}else {
					page = "redirect:" + vueDomain;
				}
			}
		}

		return page;
	}

	public Map<String, String> parseNotifyMsg(Map<String, String[]> requestParams) {
		Map<String, String> params = new HashMap<>();
		for (String name : requestParams.keySet()) {
			String[] values = requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
			}
			params.put(name, valueStr);
		}
		return params;
	}


}
