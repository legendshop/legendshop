package com.legendshop.payment.tl.precessor;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.allinpay.ets.client.RequestOrder;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.payment.tl.util.TLPayConfig;
import com.legendshop.payment.tl.util.TlFormUtil;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PaymentProcessor;
import com.legendshop.util.AppUtils;

/**
 * 通联支付
 *
 */
@Service("tongLianPayProcessor")
public class TongLianPayProcessorImpl implements PaymentProcessor {
	
	@Autowired
	private PayTypeService payTypeService;

	/**
	 * 去支付
	 */
	@Override
	public Map<String,Object> payto(SysPaymentForm paymentForm) {
		
		Map<String,Object>  resultMap=new HashMap<String, Object>();
		resultMap.put("result", false);
		
		PayType  payType = payTypeService.getPayTypeById(PayTypeEnum.TONGLIAN_PAY.value());
		if (payType == null || AppUtils.isBlank(payType.getPaymentConfig())) {
			resultMap.put("message", "支付方式不存在,请设置支付方式");
			return resultMap;
		} else if (payType.getIsEnable() == Constants.OFFLINE) {
			resultMap.put("message", "该支付方式没有启用!");
			return resultMap;
		}
		paymentForm.setPayTypeId(PayTypeEnum.TONGLIAN_PAY.value());
		paymentForm.setPayTypeName(PayTypeEnum.TONGLIAN_PAY.desc());
		
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		
		String showUrl = paymentForm.getShowUrl();
		String notifyUrl = paymentForm.getDomainName() + "/payNotify/notify/"+ PayTypeEnum.TONGLIAN_PAY.value();
		String returnUrl = paymentForm.getDomainName() + "/payNotify/response/"+ PayTypeEnum.TONGLIAN_PAY.value();
		
		
		String out_trade_no = paymentForm.getSubSettlementSn();
		String total_fee = String.valueOf(paymentForm.getTotalAmount());
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyyMMddHHmmss");
		String orderDatetime= dateFormat.format(paymentForm.getOrderDatetime());
		String myParas=paymentForm.getUserId();//自己定义的参数
		String proName=paymentForm.getSubject();
		
		
		RequestOrder requestOrder = new RequestOrder();
		requestOrder.setInputCharset(1); // UTF-8
		requestOrder.setPickupUrl(returnUrl);
		requestOrder.setReceiveUrl(notifyUrl);
		requestOrder.setVersion("v1.0");
		requestOrder.setLanguage(1);
		requestOrder.setSignType(1);
		requestOrder.setMerchantId(jsonObject.getString(TLPayConfig.MERCHANTID));
		requestOrder.setOrderNo(out_trade_no);
		requestOrder.setOrderAmount(new BigDecimal(total_fee)
				.setScale(2, BigDecimal.ROUND_HALF_UP)
				.multiply(new BigDecimal(100)).longValue());
		requestOrder.setOrderCurrency("0");
		requestOrder.setOrderDatetime(orderDatetime);
		requestOrder.setOrderExpireDatetime("120");
		requestOrder.setExt1(myParas);
		requestOrder.setPayType(0);
		requestOrder.setKey(jsonObject.getString(TLPayConfig.key));
		requestOrder.setProductName(proName.trim()); // 商品名称
		requestOrder.setPayerName(paymentForm.getUserId()); // 付款方
		String strSignMsg = requestOrder.doSign();

		Map<String, String> map = new HashMap<String, String>();
		map.put("inputCharset", requestOrder.getInputCharset() + "");
		map.put("pickupUrl", requestOrder.getPickupUrl());
		map.put("receiveUrl", requestOrder.getReceiveUrl());
		map.put("version", requestOrder.getVersion());
		map.put("language", requestOrder.getLanguage() + "");
		map.put("signType", requestOrder.getSignType() + "");
		map.put("merchantId", requestOrder.getMerchantId());
		map.put("orderNo", requestOrder.getOrderNo());
		map.put("orderAmount", requestOrder.getOrderAmount() + "");
		map.put("orderCurrency", requestOrder.getOrderCurrency());
		map.put("orderDatetime", requestOrder.getOrderDatetime());
		map.put("orderExpireDatetime", requestOrder.getOrderExpireDatetime());
		map.put("ext1", requestOrder.getExt1());
		map.put("payType", requestOrder.getPayType() + "");
		map.put("productName", requestOrder.getProductName());
		map.put("payerName", requestOrder.getPayerName());
		map.put("signMsg", strSignMsg);
		
		String result = TlFormUtil.buildHtmlForm(map,TLPayConfig.serverUrl, "post");
		PaymentLog.info("send to tonglianPay gateway " + result);
		resultMap.put("result", true);
		resultMap.put("message", result);
		return resultMap;
	}

}
