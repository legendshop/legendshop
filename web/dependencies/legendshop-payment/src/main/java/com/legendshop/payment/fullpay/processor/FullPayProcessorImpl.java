/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.payment.fullpay.processor;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.legendshop.base.log.PaymentLog;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.base.util.MerchantApiUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PaySourceEnum;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.model.form.SysSubReturnForm;
import com.legendshop.spi.service.PaymentProcessor;
import com.legendshop.util.MD5Util;


/**
 * 钱包全额支付
 *
 */
@Service("fullPayProcessor")
public class FullPayProcessorImpl implements PaymentProcessor {

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(FullPayProcessorImpl.class);
	
	@Override
	public Map<String,Object> payto(SysPaymentForm paymentForm) {
		
		Map<String,Object>  map=new HashMap<String, Object>();
		map.put("result", false);
		
		PaymentLog.log("############# 钱包全额支付模式 ########## ");
		
		paymentForm.setPayTypeId(PayTypeEnum.FULL_PAY.name()); //使用全额支付
		paymentForm.setPayTypeName(PayTypeEnum.FULL_PAY.desc());
		
		String out_trade_no = paymentForm.getSubSettlementSn();
		
		StringBuffer buffer = new StringBuffer();
	    buffer.append("订单编号：").append(out_trade_no);
		
	    Map<String , Object> paramMap = new HashMap<String, Object>();
	
	    Integer token=CommonServiceUtil.generateRandom();
		//-----------------------------
		//设置支付参数
		//-----------------------------
	    paramMap.put("token", token);		        //商户号
	    paramMap.put("out_trade_no", out_trade_no);
	    paramMap.put("total_fee", paymentForm.getTotalAmount());
	    paramMap.put("trade_no", CommonServiceUtil.getRandomSn());
	    paramMap.put("subject", buffer.toString());
	    paramMap.put("payType", PayTypeEnum.FULL_PAY.name());
        String sign = MerchantApiUtil.getSign(paramMap,MD5Util.toMD5(Constants._MD5 +  token));
        PaymentLog.info(" 签名及生成密钥信息  sign "+sign);
        paramMap.put("sign",sign);
    	
        String returnUrl = null;
        String result = null;
        if(PaySourceEnum.isAPP(paymentForm.getSource()) || PaySourceEnum.isWxMp(paymentForm.getSource())){
        	returnUrl = paymentForm.getDomainName()  + "/payNotify/notify/"+ PayTypeEnum.FULL_PAY.value();
        	result = MerchantApiUtil.buildRequestForApp(paramMap, returnUrl);
        }else {
        	returnUrl = paymentForm.getDomainName()  + "/payNotify/response/"+ PayTypeEnum.FULL_PAY.value();
        	result = MerchantApiUtil.buildRequest(paramMap, returnUrl ,"GET", "确定" );
        }
		
    	PaymentLog.info("send to Full Pay gateway " + result);
		
		map.put("result", true);
		map.put("message", result);
		
		return map;
	}
	
	public String triggerReturn(SysSubReturnForm returnForm) {
		return null;
	}

	private boolean isHttps(String url){
		if(url.startsWith("https://")){
			return true;
		}
		return false;
	}

}
