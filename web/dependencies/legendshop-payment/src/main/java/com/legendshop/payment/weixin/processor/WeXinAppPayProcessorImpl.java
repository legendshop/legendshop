package com.legendshop.payment.weixin.processor;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.payment.weixin.enums.WeiXinTradeTypeEnum;
import com.legendshop.payment.weixin.util.RequestHandler;
import com.legendshop.payment.weixin.util.WeiXinPayConfig;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PaymentProcessor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.RandomStringUtils;

@Service("weXinAppPayProcessor")
public class WeXinAppPayProcessorImpl implements PaymentProcessor {

	@Autowired
	private PayTypeService payTypeService;

	@Override
	public Map<String, Object> payto(SysPaymentForm paymentForm) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", false);
		
		PayType payType = payTypeService.getPayTypeById(PayTypeEnum.WX_APP_PAY.value()); //这种方式是卖家中心调用支付服务
		if (payType == null || AppUtils.isBlank(payType.getPaymentConfig())) {
			map.put("message", "支付方式不存在,请设置支付方式");
			return map;
		} else if (payType.getIsEnable() == Constants.OFFLINE) {
			map.put("message", "该支付方式没有启用!");
			return map;
		}
		paymentForm.setPayTypeId(PayTypeEnum.WX_APP_PAY.value());
		paymentForm.setPayTypeName(PayTypeEnum.WX_APP_PAY.desc());
		
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());

		/** 微信公众号APPID */
		String appid = jsonObject.getString(WeiXinPayConfig.APPID);
		/** 微信公众号绑定的商户号 */
		String mch_id = jsonObject.getString(WeiXinPayConfig.MCH_ID);
		String appsecret = jsonObject.getString(WeiXinPayConfig.APPSECRET);
		String partnerkey = jsonObject.getString(WeiXinPayConfig.PARTNERKEY);

		if (AppUtils.isBlank(appid) || AppUtils.isBlank(mch_id) || AppUtils.isBlank(appsecret)) {
			map.put("message", "请设置支付的密钥信息!");
			return map;
		}

		String subject = paymentForm.getSubject().replace(" ", "");// 注意标题一定去空格
		String totalPrice = String.valueOf(new BigDecimal(paymentForm.getTotalAmount()).setScale(2, BigDecimal.ROUND_HALF_UP)
				.multiply(new BigDecimal(100)).intValue());

		String notifyUrl = paymentForm.getDomainName() + "/payNotify/notify/"+ PayTypeEnum.WX_APP_PAY.value();
		
		
		SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();
		parameters.put("appid", appid);
		parameters.put("mch_id", mch_id);
		parameters.put("nonce_str", RandomStringUtils.randomNumeric(8));
		parameters.put("body", subject);
		parameters.put("out_trade_no", paymentForm.getSubSettlementSn());  
		parameters.put("fee_type", "CNY");  
	    /** 订单金额以分为单位，只能为整数 */
		parameters.put("total_fee", totalPrice);
		/** 客户端本地ip */
		parameters.put("spbill_create_ip", paymentForm.getIp());
		parameters.put("notify_url", notifyUrl);
		/** 支付方式为APP支付 */
		parameters.put("trade_type", WeiXinTradeTypeEnum.APP.name());

		RequestHandler reqHandler = new RequestHandler();
		reqHandler.init(null, appid, appsecret, partnerkey);

		// 计算签名
		String sign = reqHandler.createSign(parameters);
		parameters.put("sign", sign);

		Map<String, String> reqHandlerMap = reqHandler.getPrepayId(parameters);
		if (AppUtils.isBlank(reqHandlerMap)) {
			map.put("message", "微信统一下单集成失败！");
			return map;
		}

		String return_code = reqHandlerMap.get("return_code"); // 返回状态码
		
		System.out.println("-----微信支付请求加密参数:"+JSONUtil.getJson(reqHandlerMap));
		
		String result= "";													// SUCCESS/FAIL
		if ("SUCCESS".equals(return_code)) {

			// 参数
			String timeStamp = System.currentTimeMillis() / 1000 + "";

			// 参数
			SortedMap<Object, Object> paraMap = new TreeMap<Object, Object>();
			paraMap.put("noncestr", reqHandlerMap.get("nonce_str"));
			paraMap.put("appid", reqHandlerMap.get("appid"));
			paraMap.put("partnerid", reqHandlerMap.get("mch_id"));
			paraMap.put("prepayid", reqHandlerMap.get("prepay_id"));
			paraMap.put("package", "Sign=WXPay");
			paraMap.put("timestamp", timeStamp);

			// 要签名
			String paySign = reqHandler.createSign(paraMap);

			// 参数
			JSONObject payResult = new JSONObject();
			payResult.put("appid", reqHandlerMap.get("appid")); // 调用接口提交的应用ID
			payResult.put("partnerid", reqHandlerMap.get("mch_id")); // 调用接口提交的商户号
			payResult.put("noncestr", reqHandlerMap.get("nonce_str")); // 微信返回的随机字符串
			payResult.put("sign", paySign); // 微信返回的签名
			payResult.put("prepayid", reqHandlerMap.get("prepay_id")); // 微信生成的预支付回话标识，用于后续接口调用中使用，该值有效期为2小时
			payResult.put("package", "Sign=WXPay");
			payResult.put("timestamp", timeStamp);
			/*payResult.put("trade_type", "APP");*/ //因为要按照Hbuilder要求, 不能要这个

			map.put("result", true);
			result= payResult.toJSONString();
			
		}else{
			 result= reqHandlerMap.get("return_msg");
		}
		PaymentLog.info("send to WeiXin APP PAY gateway " + result);
		map.put("message", result);
		return map;
	}

}
