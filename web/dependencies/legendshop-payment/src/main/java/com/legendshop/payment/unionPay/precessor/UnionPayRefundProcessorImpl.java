package com.legendshop.payment.unionPay.precessor;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.model.form.SysSubReturnForm;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PaymentRefundProcessor;


/**
 * 在线银联退款
 *
 */
@Service("unionPayRefundProcessor")
public class UnionPayRefundProcessorImpl implements PaymentRefundProcessor {
	
	@Autowired
	private  PayTypeService payTypeService;
	
	@Override
	public Map<String, Object> refund(SysSubReturnForm returnForm) {
		
		String out_trade_no=returnForm.getSubSettlementSn() ;// 支付时传入的商户订单号，与trade_no必填一个
		String trade_no= returnForm.getFlowTradeNo();	//支付时返回的支付宝交易号，与out_trade_no必填一个
		String out_request_no=returnForm.getOutRequestNo() ;	//本次退款请求流水号，部分退款时必传
		Double refund_amount=returnForm.getReturnAmount();	// 本次退款金额
		
		
		
		return null;
	}
	
}
