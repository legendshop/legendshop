package com.legendshop.payment.tenpay.processor;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.RefundLog;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.form.SysSubReturnForm;
import com.legendshop.payment.tenpay.client.ClientResponseHandler;
import com.legendshop.payment.tenpay.client.TenpayHttpClient;
import com.legendshop.payment.tenpay.util.TenpayMD5Util;
import com.legendshop.payment.tenpay.util.RequestHandler;
import com.legendshop.payment.tenpay.util.TenPayConfig;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PaymentRefundProcessor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 
 * 腾讯直接退款操作
 *
 */
@Service("tenDirectPayRefundProcessor")
public class TenDirectPayRefundProcessorImpl implements PaymentRefundProcessor {

	@Autowired
	private  PayTypeService payTypeService;
	
	@Override
	public Map<String, Object> refund(SysSubReturnForm returnForm) {
		
        RefundLog.info("############# 腾讯支付退款中心 ########## " );
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", false);
		PayType payType = payTypeService.getPayTypeById(PayTypeEnum.TENPAY.value());
		RefundLog.info("############# 退款支付参数  {} ########## ",JSONUtil.getJson(payType));
		if (payType == null || AppUtils.isBlank(payType.getPaymentConfig())) {
			map.put("message", "支付方式不存在,请设置支付方式");
			return map;
		}
		
		String out_trade_no=returnForm.getSubSettlementSn() ;// 支付时传入的商户订单号，与trade_no必填一个
		String trade_no= returnForm.getFlowTradeNo();	//支付时返回的支付宝交易号，与out_trade_no必填一个
		String out_request_no=returnForm.getOutRequestNo() ;	//本次退款请求流水号，部分退款时必传
		Double refund_amount=returnForm.getReturnAmount();	// 本次退款金额
		
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		
		String bargainor_id 		= jsonObject.getString(TenPayConfig.BARGAINOR_ID);
		String key 			=  jsonObject.getString(TenPayConfig.KEY);
		
		//创建查询请求对象
	    RequestHandler reqHandler = new RequestHandler(null, null);
	    //通信对象
	    TenpayHttpClient httpClient = new TenpayHttpClient();
	    //应答对象
	    ClientResponseHandler resHandler = new ClientResponseHandler();
		
	    //-----------------------------
	    //设置请求参数
	    //-----------------------------
	    reqHandler.setKey(key);
	    reqHandler.setGateUrl("https://mch.tenpay.com/refundapi/gateway/refund.xml");
	    
	    
	    //-----------------------------
	    //设置接口参数
	    //-----------------------------
	    reqHandler.setParameter("service_version", "1.1");
	    reqHandler.setParameter("partner", bargainor_id);	
	    reqHandler.setParameter("out_trade_no",out_trade_no);	
	    reqHandler.setParameter("transaction_id",trade_no);
	    reqHandler.setParameter("out_refund_no",out_request_no);	
	   // reqHandler.setParameter("total_fee", String.valueOf(new BigDecimal(returnForm.getSubAmout()).setScale(2,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).intValue()));	
	    reqHandler.setParameter("refund_fee", String.valueOf(new BigDecimal(refund_amount).setScale(2,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).intValue()));
	    reqHandler.setParameter("op_user_id", bargainor_id);	
	    //操作员密码,MD5处理
	    reqHandler.setParameter("op_user_passwd", TenpayMD5Util.MD5Encode("111111","UTF-8"));	
	    reqHandler.setParameter("recv_user_id", "");	
	    reqHandler.setParameter("reccv_user_name", "");
	    
	  //-----------------------------
	    //设置通信参数
	    //-----------------------------
	    //设置请求返回的等待时间
	    httpClient.setTimeOut(5);	
	    
	    //设置发送类型POST
	    httpClient.setMethod("POST");     
	    
	    //设置请求内容
	    String requestUrl = null;
		try {
			requestUrl = reqHandler.getRequestURL();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	    httpClient.setReqContent(requestUrl);
	    String rescontent = "null";
	    //后台调用
	    if(httpClient.call()) {
	    	
	    	//设置结果参数
	    	rescontent = httpClient.getResContent();
	    	try {
				resHandler.setContent(rescontent);
			} catch (Exception e) {
				e.printStackTrace();
			}
	    	resHandler.setKey(key);
	    	
	    	//获取返回参数
	    	String retcode = resHandler.getParameter("retcode");
	    	boolean isTenpaySign= resHandler.isTenpaySign();
	    	RefundLog.log("判断签名及结果 isTenpaySign={}  ,retcode={} ",isTenpaySign,isTenpaySign);
	    	//判断签名及结果
	    	if(isTenpaySign && "0".equals(retcode)) {
	    		/*退款状态	refund_status	
				  4，10：退款成功。
				  3，5，6：退款失败。
				  8，9，11:退款处理中。
				  1，2: 未确定，需要商户原退款单号重新发起。
				  7：转入代发，退款到银行发现用户的卡作废或者冻结了，导致原路退款银行卡失败，资金回流到商户的现金帐号，需要商户人工干预，通过线下或者财付通转账的方式进行退款。
				*/
	    		String refund_status=resHandler.getParameter("refund_status");
	        	String out_refund_no=resHandler.getParameter("out_refund_no");
	        	String refund_id=resHandler.getParameter("refund_id");
	        	RefundLog.log("商户退款单号 {} 退款状态{} 财付通退款单号 {} ",out_refund_no,refund_status,refund_id);
	        	if("4".equals(refund_status) || "10".equals(refund_status)){
	        		map.put("result", true);
	    			return map;
	        	}else if("8".equals(refund_status) || "9".equals(refund_status) || "11".equals(refund_status) ){
	        		
	        		map.put("message", "退款处理中,请勿重复操作");
	    			return map;
	        		
	        	}else{
	        		map.put("message", "退款失败");
	    			return map;
	        	}
		    } else {
	    		//错误时，返回结果未签名，记录retcode、retmsg看失败详情。
	    		System.out.println("验证签名失败或业务错误");
	    		System.out.println("retcode:" + resHandler.getParameter("retcode")+
	    	    	                    " retmsg:" + resHandler.getParameter("retmsg"));
	    		System.out.println("retcode:" + resHandler.getParameter("retcode")+
	    	    	                    " retmsg:" + resHandler.getParameter("retmsg"));
	    		map.put("message","错误码:"+ resHandler.getParameter("retcode") +" 错误信息:"+ resHandler.getParameter("retmsg"));
    			return map;
	    	}
	    }else {
	    	System.out.println("后台调用通信失败");   	
	    	System.out.println(httpClient.getResponseCode());
	    	System.out.println(httpClient.getErrInfo());
	    	//有可能因为网络原因，请求已经处理，但未收到应答。
	    	map.put("message","后台调用通信失败");
			return map;
	    }
	}
	
}
