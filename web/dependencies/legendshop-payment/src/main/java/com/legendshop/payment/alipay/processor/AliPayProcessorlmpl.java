/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.payment.alipay.processor;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.domain.AlipayTradePayModel;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PaySourceEnum;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.payment.alipay.util.AlipayConfig;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PaymentProcessor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;


/**
 * 支付宝支付接口
 *
 */
@Service("aliPayProcessor")
public class AliPayProcessorlmpl implements PaymentProcessor {

	@Autowired
	private PayTypeService payTypeService;

	@Override
	public Map<String, Object> payto(SysPaymentForm paymentForm) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", false);

		PayType payType = payTypeService.getPayTypeById(PayTypeEnum.ALI_PAY.value());
		PaymentLog.log("############# 支付参数  {} ########## ",JSONUtil.getJson(payType));

		if (payType == null || AppUtils.isBlank(payType.getPaymentConfig())) {
			map.put("message", "支付方式不存在,请设置支付方式");
			return map;
		} else if (Constants.OFFLINE.equals(payType.getIsEnable())) {
			map.put("message", "该支付方式没有启用!");
			return map;
		}

		paymentForm.setPayTypeId(PayTypeEnum.ALI_PAY.value());
		paymentForm.setPayTypeName(PayTypeEnum.ALI_PAY.desc());
		paymentForm.setSubject(paymentForm.getSubject().replace('&', ' '));

		String showUrl = paymentForm.getQuitUrl();
		String notifyUrl = paymentForm.getDomainName() + "/payNotify/notify/"+ PayTypeEnum.ALI_PAY.value();
		String returnUrl = paymentForm.getDomainName() + "/payNotify/response/"+ PayTypeEnum.ALI_PAY.value();


		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());

		String appId = jsonObject.getString(AlipayConfig.APPID);
		String rsa_private = jsonObject.getString(AlipayConfig.RSA_PRIVATE);
		String rsa_publish = jsonObject.getString(AlipayConfig.RSA_PUBLIC);
		String totalPrice = String.valueOf(paymentForm.getTotalAmount());

		String subject=paymentForm.getSubject();
		if(AppUtils.isNotBlank(subject) && subject.length()>127){
			subject=subject.substring(0,127);
		}

		if(PaySourceEnum.isPC(paymentForm.getSource())){//PC端
			AliPayTradePay( map,paymentForm.getSubSettlementSn(), subject, notifyUrl, returnUrl, appId, rsa_private, rsa_publish, totalPrice);
		}else if(PaySourceEnum.isWAP(paymentForm.getSource())){//WAP端
			AlipayTradeWapPay( map,paymentForm.getSubSettlementSn(),subject, notifyUrl, returnUrl, showUrl ,appId, rsa_private, rsa_publish, totalPrice);
		}else if(PaySourceEnum.isWEIXIN(paymentForm.getSource())) {//微信端
			AlipayTradeWeixinPay( map,paymentForm.getSubSettlementSn(),subject, notifyUrl, returnUrl, showUrl ,appId, rsa_private, rsa_publish, totalPrice);
		}else if(PaySourceEnum.isAPP(paymentForm.getSource())){//原生APP
			AlipayTradeAppPay( map,paymentForm.getSubSettlementSn(),subject, notifyUrl, returnUrl, appId, rsa_private, rsa_publish, totalPrice);
		}

		return map;
	}



	private void AlipayTradeAppPay( Map<String, Object> map,String subSettlementSn,String subject, String notifyUrl, String returnUrl,  String appId,
			String rsa_private, String rsa_publish, String totalPrice) {

	    /**
		 * https://docs.open.alipay.com/api_1/alipay.trade.precreate
		 */
		AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.serverUrl, appId, rsa_private, "json",
				AlipayConfig.input_charset, rsa_publish, AlipayConfig.sign_type);

		//实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
		AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
		//SDK已经封装掉了公共参数，这里只需要传入业务参数。以下方法为sdk的model入参方式(model和biz_content同时存在的情况下取biz_content)。
		AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
		model.setBody(subject);
		model.setSubject(subject);
		model.setOutTradeNo(subSettlementSn);
		model.setTimeoutExpress("30m");
		model.setTotalAmount(totalPrice);
		model.setProductCode("QUICK_MSECURITY_PAY");

		request.setBizModel(model);
		request.setNotifyUrl(notifyUrl);
		request.setReturnUrl(returnUrl);

		  // form表单生产
	    String form = "";
		AlipayTradeAppPayResponse response =null;
		try {
		        //这里和普通的接口调用不同，使用的是sdkExecute
		        response = alipayClient.sdkExecute(request);
		        System.out.println(response.getBody());//就是orderString 可以直接给客户端请求，无需再做处理。
		        form = response.getBody();
		    } catch (AlipayApiException e) {
		        e.printStackTrace();
		}
		map.put("result", true);
		map.put("message", form);
	}

	private void AlipayTradeWapPay( Map<String, Object> map,String subSettlementSn,String subject, String notifyUrl, String returnUrl,String quitUrl,  String appId,
			String rsa_private, String rsa_publish, String totalPrice) {
		 AlipayTradeWapPayRequest alipay_request=new AlipayTradeWapPayRequest();
	    // 封装请求支付信息
	    AlipayTradeWapPayModel model=new AlipayTradeWapPayModel();
	    model.setOutTradeNo(subSettlementSn);
	    model.setSubject(subject);
	    model.setTotalAmount(totalPrice);
	    model.setBody(subject);
	    model.setProductCode("QUICK_WAP_PAY");
	    model.setQuitUrl(quitUrl);
	    model.setTimeoutExpress("30m");

	    alipay_request.setBizModel(model);
	    // 设置异步通知地址
	    alipay_request.setNotifyUrl(notifyUrl);
	    // 设置同步地址
	    alipay_request.setReturnUrl(returnUrl);

	    /**
		 * https://docs.open.alipay.com/api_1/alipay.trade.precreate
		 */
		AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.serverUrl, appId, rsa_private, "json",
				AlipayConfig.input_charset, rsa_publish, AlipayConfig.sign_type);

	    // form表单生产
	    String form = "";
		try {
			// 调用SDK生成表单
			form = alipayClient.pageExecute(alipay_request).getBody();
		} catch (AlipayApiException e) {
			e.printStackTrace();
		}
		map.put("result", true);
		map.put("message", form);
	}

	private void AlipayTradeWeixinPay( Map<String, Object> map,String subSettlementSn,String subject, String notifyUrl, String returnUrl,String quitUrl,  String appId,
			String rsa_private, String rsa_publish, String totalPrice) {
		 AlipayTradeWapPayRequest alipay_request=new AlipayTradeWapPayRequest();
	    // 封装请求支付信息
	    AlipayTradeWapPayModel model=new AlipayTradeWapPayModel();
	    model.setOutTradeNo(subSettlementSn);
	    model.setSubject(subject);
	    model.setTotalAmount(totalPrice);
	    model.setBody(subject);
	    model.setProductCode("QUICK_WAP_PAY");
	    model.setQuitUrl(quitUrl);
	    model.setTimeoutExpress("30m");

	    alipay_request.setBizModel(model);
	    // 设置异步通知地址
	    alipay_request.setNotifyUrl(notifyUrl);
	    // 设置同步地址
	    alipay_request.setReturnUrl(returnUrl);

	    /**
		 * https://docs.open.alipay.com/api_1/alipay.trade.precreate
		 */
		AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.serverUrl, appId, rsa_private, "json",
				AlipayConfig.input_charset, rsa_publish, AlipayConfig.sign_type);

	    // form表单生产
	    String form = "";
		try {
			// 调用SDK生成表单
			form = alipayClient.pageExecute(alipay_request,"GET").getBody();
		} catch (AlipayApiException e) {
			e.printStackTrace();
		}
		map.put("result", true);
		map.put("message", form);
	}

	private void AliPayTradePay( Map<String, Object> map,String subSettlementSn,String subject, String notifyUrl, String returnUrl, String appId,
			String rsa_private, String rsa_publish, String totalPrice) {
		AlipayTradePayModel alipayTradePayModel = new AlipayTradePayModel();
		alipayTradePayModel.setOutTradeNo(subSettlementSn);
		alipayTradePayModel.setTotalAmount(totalPrice);
		alipayTradePayModel.setSubject(subject);
		alipayTradePayModel.setBody(subject);
		alipayTradePayModel.setProductCode("FAST_INSTANT_TRADE_PAY");
		alipayTradePayModel.setTimeoutExpress("30m");

		// 使用SDK，构建群发请求模型
		AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
		request.setBizModel(alipayTradePayModel);
		request.setNotifyUrl(notifyUrl);
		request.setReturnUrl(returnUrl);

		/**
		 * https://docs.open.alipay.com/api_1/alipay.trade.precreate
		 */
		AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.serverUrl, appId, rsa_private, "json",
				AlipayConfig.input_charset, rsa_publish, AlipayConfig.sign_type);

		String form = "";
		try {
			form = alipayClient.pageExecute(request).getBody(); // 调用SDK生成表单
		} catch (AlipayApiException e) {
			e.printStackTrace();
		}
		map.put("result", true);
		map.put("message", form);
	}


}
