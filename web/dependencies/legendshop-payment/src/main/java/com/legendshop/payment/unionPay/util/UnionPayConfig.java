package com.legendshop.payment.unionPay.util;

import com.legendshop.config.PropertiesUtil;
import com.legendshop.config.PropertiesUtilManager;
import com.legendshop.util.SystemUtil;

public class UnionPayConfig {

	
	public static String MER_ID="merId";
	
	static PropertiesUtil propertiesUtil;
	  
	static { 
	  propertiesUtil= PropertiesUtilManager.getPropertiesUtil();
	}
	
	/** The notify_url. */
	// 如需使用自动对帐，商户需将相应自动对帐文件（例如：/notify ）地址通知网银在线客服部，网银在线客服电话：010-62698008
	public static String NOTIFY_URL = propertiesUtil.getPcDomainName() + SystemUtil.getContextPath() + "/pay/unionPay/notify";

	/** The return_url. */
	public  static String RETURN_URL = propertiesUtil.getPcDomainName() + SystemUtil.getContextPath() + "/pay/unionPay/response";
	
	//全渠道固定值
	public static String version = "5.0.0";
	
	
}
