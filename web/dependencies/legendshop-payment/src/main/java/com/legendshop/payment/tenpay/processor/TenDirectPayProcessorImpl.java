/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.payment.tenpay.processor;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.model.form.SysSubReturnForm;
import com.legendshop.payment.tenpay.util.TenPayConfig;
import com.legendshop.payment.tenpay.util.TenPaySubmit;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PaymentProcessor;
import com.legendshop.util.AppUtils;


/**
 * 财付通即时支付
 * 
 * 官方网站：http://www.legendesign.net
 */
@Service("tenDirectPayProcessor")
public class TenDirectPayProcessorImpl implements PaymentProcessor {
	
	@Autowired
	private PayTypeService payTypeService;

	
	@Override
	public Map<String,Object> payto(SysPaymentForm paymentForm) {
		
		Map<String,Object>  map=new HashMap<String, Object>();
		map.put("result", false);
		
		PayType  payType = payTypeService.getPayTypeById(PayTypeEnum.TENPAY.value());
		if (payType == null || AppUtils.isBlank(payType.getPaymentConfig())) {
			map.put("message", "支付方式不存在,请设置支付方式");
			return map;
		} else if (payType.getIsEnable() == Constants.OFFLINE) {
			map.put("message", "该支付方式没有启用!");
			return map;
		}
		paymentForm.setPayTypeId(PayTypeEnum.TENPAY.value());
		paymentForm.setPayTypeName(PayTypeEnum.TENPAY.desc());
		
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		
		String showUrl = paymentForm.getShowUrl();
		String notifyUrl = paymentForm.getDomainName() + "/payNotify/notify/"+ PayTypeEnum.TENPAY.value();
		String returnUrl = paymentForm.getDomainName() + "/payNotify/response/"+ PayTypeEnum.TENPAY.value();
		
		
		String bargainor_id 		= jsonObject.getString(TenPayConfig.BARGAINOR_ID);
		String key 			=  jsonObject.getString(TenPayConfig.KEY);
		String out_trade_no = paymentForm.getSubSettlementSn();
		
		StringBuffer buffer = new StringBuffer();
	    buffer.append("订单编号：").append(out_trade_no);
	    
	    String subject=paymentForm.getSubject();
		if(AppUtils.isNotBlank(subject) && subject.length()>127){
			subject=subject.substring(0,127);
		}
	     
		String ip = paymentForm.getIp();
		
		
		Map<String, String> sParaTemp = new HashMap<String, String>();

		//当前时间 yyyyMMddHHmmss
		SimpleDateFormat outFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String currTime = outFormat.format(paymentForm.getOrderDatetime());

//		//8位日期
//		String strDate = currTime.substring(0, 8);

//		//6位时间
//		String strTime = currTime.substring(8, currTime.length());

//		//四位随机数
//		String strRandom = TenpayUtil.buildRandom(4) + "";
		
//		//10位序列号,可以自行调整。
//		String strReq = strTime + strRandom;
		

		//财付通交易单号，规则为：10位商户号+8位时间（YYYYmmdd)+10位流水号
//		String transaction_id = seller_email + strDate + strReq;
		
		//商家订单号,长度若超过32位，取前32位。财付通只记录商家订单号，不保证唯一。
//		String sp_billno = strReq;
		
	
		//-----------------------------
		//设置支付参数
		//-----------------------------
		//sParaTemp.put("key", key);
		sParaTemp.put("partner", bargainor_id);		        //商户号
		sParaTemp.put("out_trade_no", out_trade_no);		//商家订单号 //strReq?
		
		String totalPrice = String.valueOf(new BigDecimal(paymentForm.getTotalAmount()).setScale(2, BigDecimal.ROUND_HALF_UP)
				.multiply(new BigDecimal(100)).intValue());
		
		sParaTemp.put("total_fee", totalPrice);			        //商品金额,以分为单位
		sParaTemp.put("return_url", returnUrl);		    //交易完成后跳转的URL
		sParaTemp.put("notify_url", notifyUrl);		    //接收财付通通知的URL
		sParaTemp.put("subject", subject);              //商品名称(中介交易时必填)
		sParaTemp.put("body", buffer.toString());	                    //商品描述
		sParaTemp.put("bank_type", "DEFAULT");		    //银行类型(中介担保时此参数无效)
		sParaTemp.put("spbill_create_ip",ip);   //用户的公网ip，不是商户服务器IP
		sParaTemp.put("fee_type", "1");                    //币种，1人民币

		//系统可选参数
		sParaTemp.put("sign_type", "MD5");                //签名类型,默认：MD5
		sParaTemp.put("service_version", "1.0");			//版本号，默认为1.0
		sParaTemp.put("input_charset", "utf-8");            //字符编码
		sParaTemp.put("sign_key_index", "1");             //密钥序号


		//业务可选参数
		sParaTemp.put("attach", "");                      //附加数据，原样返回
		sParaTemp.put("product_fee", "");                 //商品费用，必须保证transport_fee + product_fee=total_fee
		sParaTemp.put("transport_fee", "0");               //物流费用，必须保证transport_fee + product_fee=total_fee
		sParaTemp.put("time_start", currTime);            //订单生成时间，格式为yyyymmddhhmmss
		sParaTemp.put("time_expire", "");                 //订单失效时间，格式为yyyymmddhhmmss
		sParaTemp.put("buyer_id", "");                    //买方财付通账号
		sParaTemp.put("goods_tag", "");                   //商品标记
		sParaTemp.put("trade_mode", "1");  //交易模式，1即时到账(默认)，2中介担保，3后台选择（买家进支付中心列表选择）
		sParaTemp.put("transport_desc", "");              //物流说明
		sParaTemp.put("trans_type", "1");                  //交易类型，1实物交易，2虚拟交易
		sParaTemp.put("agentid", "");                     //平台ID
		sParaTemp.put("agent_type", "");                  //代理模式，0无代理(默认)，1表示卡易售模式，2表示网店模式
		sParaTemp.put("seller_id", "");                   //卖家商户号，为空则等同于partner
		
		String result =TenPaySubmit.buildForm(sParaTemp, TenPayConfig.TENPAY_GATEWAY, "post", "确定", key);
		PaymentLog.info("send to tencent gateway " + result);
		map.put("result", true);
		map.put("message", result);
		return map;
	}
	

	public String triggerReturn(SysSubReturnForm returnForm) {
		return null;
	}

}
