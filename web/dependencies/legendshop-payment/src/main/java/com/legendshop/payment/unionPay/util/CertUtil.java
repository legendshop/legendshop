package com.legendshop.payment.unionPay.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.RSAPublicKeySpec;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.util.AppUtils;
import com.legendshop.util.StringUtil;

/**
 * 
 * @author Tony
 *
 */
public class CertUtil {
	
	private final static Logger LOGGER=LoggerFactory.getLogger(CertUtil.class);

	/** 证书容器. */
	private static KeyStore keyStore = null;
	/** 密码加密证书 */
	private static X509Certificate encryptCert = null;
	/** 磁道加密证书 */
	private static X509Certificate encryptTrackCert = null;
	
	/** 验证签名证书. */
	private static X509Certificate validateCert = null;
	/** 验签证书存储Map. */
	private static Map<String, X509Certificate> certMap = new HashMap<String, X509Certificate>();
	
	/** 基于Map存储多商户RSA私钥 */
	private final static Map<String, KeyStore> certKeyStoreMap = new ConcurrentHashMap<String, KeyStore>();

	
	/**
	 * 初始化所有证书.
	 */
	public static void init() {
		if (SDKConstants.TRUE_STRING.equals(SDKConfig.getConfig().getSingleMode())) {
			// 单证书模式,初始化配置文件中的签名证书
			initSignCert();
		}
		initEncryptCert();// 初始化加密公钥证书
		
		
		
		initValidateCertFromDir();// 初始化所有的验签证书
	}

	/**
	 * 加载签名证书
	 */
	private static void initSignCert() {
		if (null != keyStore) {
			keyStore = null;
		}
		try {
			if(AppUtils.isBlank(SDKConfig.getConfig().getSignCertPath())){
				return;
			}
			keyStore = getKeyInfo(SDKConfig.getConfig().getSignCertPath(),
					SDKConfig.getConfig().getSignCertPwd(), SDKConfig
							.getConfig().getSignCertType());
			String CertId=getSignCertId() ;
			if(CertId==null){
				LOGGER.error(" InitSignCert Successful. CertId is null ");
			}else{
				LOGGER.info("InitSignCert Successful. CertId=["
						+ getSignCertId() + "]");
			}
		} catch (IOException e) {
			LOGGER.error("InitSignCert Error", e);
			
		}
	}



	/**
	 * 加载RSA签名证书
	 * 
	 * @param certFilePath
	 * @param certPwd
	 */
	public static void loadRsaCert(String certFilePath, String certPwd) {
		KeyStore keyStore = null;
		try {
			keyStore = getKeyInfo(certFilePath, certPwd, "PKCS12");
			certKeyStoreMap.put(certFilePath, keyStore);
			LOGGER.info("LoadRsaCert Successful");
		} catch (IOException e) {
			LOGGER.error("LoadRsaCert Error", e);
		}
	}

	/**
	 * 加载密码加密证书 目前支持有两种加密
	 */
	private static void initEncryptCert() {
		LOGGER.info("加载敏感信息加密证书==>"+SDKConfig.getConfig().getEncryptCertPath());
		if (! StringUtil.isBlank(SDKConfig.getConfig().getEncryptCertPath())) {
			encryptCert = initCert(SDKConfig.getConfig().getEncryptCertPath());
			LOGGER.info("LoadEncryptCert Successful");
		} else {
			LOGGER.info("WARN: acpsdk.encryptCert.path is empty");
		}
		if (!StringUtil.isBlank(SDKConfig.getConfig().getEncryptTrackCertPath())) {
			encryptTrackCert = initCert(SDKConfig.getConfig()
					.getEncryptTrackCertPath());
			LOGGER.info("LoadEncryptTrackCert Successful");
		} else {
			LOGGER.info("WARN: acpsdk.encryptTrackCert.path is empty");
		}
	}
	/**
	 * 
	 * @param path
	 * @return
	 */
	private static X509Certificate initCert(String path) {
		X509Certificate encryptCertTemp = null;
		CertificateFactory cf = null;
		FileInputStream in = null;
		try {
			cf = CertificateFactory.getInstance("X.509");
			in = new FileInputStream(path);
			encryptCertTemp = (X509Certificate) cf.generateCertificate(in);
			// 打印证书加载信息,供测试阶段调试
			LOGGER.info("[" + path + "][CertId="
					+ encryptCertTemp.getSerialNumber().toString() + "]");
		} catch (CertificateException e) {
			LOGGER.error("InitCert Error", e);
		} catch (FileNotFoundException e) {
			LOGGER.error("InitCert Error File Not Found", e);
		} finally {
			if (null != in) {
				try {
					in.close();
				} catch (IOException e) {
					LOGGER.error(e.toString());
				}
			}
		}
		return encryptCertTemp;
	}
	

	/**
	 * 从指定目录下加载验证签名证书
	 * 
	 */
	private static void initValidateCertFromDir() {
		certMap.clear();
		
		//TODO  jar 形式不行
		URL url=Thread.currentThread().getContextClassLoader().getResource("/certs/unionPay/");
		if(AppUtils.isBlank(url)){
			return ;
		}
		String dir=url.getPath();
		LOGGER.info("加载验证签名证书目录==>" + dir);
		if (StringUtil.isBlank(dir)) {
			LOGGER.info("ERROR: acpsdk.validateCert.dir is empty");
			return;
		}
		CertificateFactory cf = null;
		FileInputStream in = null;
		try {
			cf = CertificateFactory.getInstance("X.509");
			File fileDir = new File(dir);
			File[] files = fileDir.listFiles(new CerFilter());
			if(AppUtils.isNotBlank(files)){
				for (int i = 0; i < files.length; i++) {
					File file = files[i];
					in = new FileInputStream(file.getAbsolutePath());
					validateCert = (X509Certificate) cf.generateCertificate(in);
					certMap.put(validateCert.getSerialNumber().toString(),
							validateCert);
					// 打印证书加载信息,供测试阶段调试
					LOGGER.info("[" + file.getAbsolutePath() + "][CertId="
							+ validateCert.getSerialNumber().toString() + "]");
				}
			}
			
			LOGGER.info("LoadVerifyCert Successful");
		} catch (CertificateException e) {
			LOGGER.error("LoadVerifyCert Error", e);
		} catch (FileNotFoundException e) {
			LOGGER.error("LoadVerifyCert Error File Not Found", e);
		} finally {
			if (null != in) {
				try {
					in.close();
				} catch (IOException e) {
					LOGGER.error(e.toString());
				}
			}
		}
	}


	/**
	 * 获取签名证书私钥（单证书模式）
	 * 
	 * @return
	 */
	public static PrivateKey getSignCertPrivateKey() {
		try {
			Enumeration<String> aliasenum = keyStore.aliases();
			String keyAlias = null;
			if (aliasenum.hasMoreElements()) {
				keyAlias = aliasenum.nextElement();
			}
			PrivateKey privateKey = (PrivateKey) keyStore.getKey(keyAlias,
					SDKConfig.getConfig().getSignCertPwd().toCharArray());
			return privateKey;
		} catch (KeyStoreException e) {
			LOGGER.error("getSignCertPrivateKey Error", e);
			return null;
		} catch (UnrecoverableKeyException e) {
			LOGGER.error("getSignCertPrivateKey Error", e);
			return null;
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error("getSignCertPrivateKey Error", e);
			return null;
		}
	}

	public static PrivateKey getSignCertPrivateKeyByStoreMap(String certPath,
			String certPwd) {
		if (!certKeyStoreMap.containsKey(certPath)) {
			loadRsaCert(certPath, certPwd);
		}
		try {
			Enumeration<String> aliasenum = certKeyStoreMap.get(certPath)
					.aliases();
			String keyAlias = null;
			if (aliasenum.hasMoreElements()) {
				keyAlias = aliasenum.nextElement();
			}
			PrivateKey privateKey = (PrivateKey) certKeyStoreMap.get(certPath)
					.getKey(keyAlias, certPwd.toCharArray());
			return privateKey;
		} catch (KeyStoreException e) {
			LOGGER.error("getSignCertPrivateKeyByStoreMap Error", e);
			return null;
		} catch (UnrecoverableKeyException e) {
			LOGGER.error("getSignCertPrivateKeyByStoreMap Error", e);
			return null;
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error("getSignCertPrivateKeyByStoreMap Error", e);
			return null;
		}
	}

	/**
	 * 获取加密证书公钥.密码加密时需要
	 * 
	 * @return
	 */
	public static PublicKey getEncryptCertPublicKey() {
		if (null == encryptCert) {
			String path = SDKConfig.getConfig().getEncryptCertPath();
			if (!StringUtil.isBlank(path)) {
				encryptCert = initCert(path);
				return encryptCert.getPublicKey();
			} else {
				LOGGER.info("ERROR: acpsdk.encryptCert.path is empty");
				return null;
			}
		} else {
			return encryptCert.getPublicKey();
		}
	}
	
	/**
	 * 获取加密证书公钥.密码加密时需要
	 * 加密磁道信息证书
	 * 
	 * @return
	 */
	public static PublicKey getEncryptTrackCertPublicKey() {
		if (null == encryptTrackCert) {
			String path = SDKConfig.getConfig().getEncryptTrackCertPath();
			if (!StringUtil.isBlank(path)) {
				encryptTrackCert = initCert(path);
				return encryptTrackCert.getPublicKey();
			} else {
				LOGGER.info("ERROR: acpsdk.encryptTrackCert.path is empty");
				return null;
			}
		} else {
			return encryptTrackCert.getPublicKey();
		}
	}

	/**
	 * 验证签名证书
	 * 
	 * @return 验证签名证书的公钥
	 */
	public static PublicKey getValidateKey() {
		if (null == validateCert) {
			return null;
		}
		return validateCert.getPublicKey();
	}

	/**
	 * 通过certId获取证书Map中对应证书的公钥
	 * 
	 * @param certId
	 *            证书物理序号
	 * @return 通过证书编号获取到的公钥
	 */
	public static PublicKey getValidateKey(String certId) {
		X509Certificate cf = null;
		if (certMap.containsKey(certId)) {
			// 存在certId对应的证书对象
			cf = certMap.get(certId);
			return cf.getPublicKey();
		} else {
			// 不存在则重新Load证书文件目录
			initValidateCertFromDir();
			if (certMap.containsKey(certId)) {
				// 存在certId对应的证书对象
				cf = certMap.get(certId);
				return cf.getPublicKey();
			} else {
				LOGGER.error("缺少certId=[" + certId + "]对应的验签证书.");
				return null;
			}
		}
	}

	

	/**
	 * 获取签名证书中的证书序列号（单证书）
	 * 
	 * @return 证书的物理编号
	 */
	public static String getSignCertId() {
		try {
			if(keyStore!=null){
				Enumeration<String> aliasenum = keyStore.aliases();
				String keyAlias = null;
				if (aliasenum.hasMoreElements()) {
					keyAlias = aliasenum.nextElement();
				}
				X509Certificate cert = (X509Certificate) keyStore
						.getCertificate(keyAlias);
				return cert.getSerialNumber().toString();
			}
			
		} catch (Exception e) {
			LOGGER.error("getSignCertId Error", e);
			return null;
		}
		return null;
	}

	/**
	 * 获取加密证书的证书序列号
	 * 
	 * @return
	 */
	public static String getEncryptCertId() {
		if (null == encryptCert) {
			String path = SDKConfig.getConfig().getEncryptCertPath();
			if (!StringUtil.isBlank(path)) {
				encryptCert = initCert(path);
				return encryptCert.getSerialNumber().toString();
			} else {
				LOGGER.info("ERROR: acpsdk.encryptCert.path is empty");
				return null;
			}
		} else {
			return encryptCert.getSerialNumber().toString();
		}
	}
	
	/**
	 * 获取磁道加密证书的证书序列号
	 * 
	 * @return
	 */
	public static String getEncryptTrackCertId() {
		if (null == encryptTrackCert) {
			String path = SDKConfig.getConfig().getEncryptTrackCertPath();
			if (!StringUtil.isBlank(path)) {
				encryptTrackCert = initCert(path);
				return encryptTrackCert.getSerialNumber().toString();
			} else {
				LOGGER.info("ERROR: acpsdk.encryptTrackCert.path is empty");
				return null;
			}
		} else {
			return encryptTrackCert.getSerialNumber().toString();
		}
	}

	/**
	 * 获取签名证书公钥对象
	 * 
	 * @return
	 */
	public static PublicKey getSignPublicKey() {
		try {
			Enumeration<String> aliasenum = keyStore.aliases();
			String keyAlias = null;
			if (aliasenum.hasMoreElements()) // we are readin just one
			// certificate.
			{
				keyAlias = (String) aliasenum.nextElement();
			}
			Certificate cert = keyStore.getCertificate(keyAlias);
			PublicKey pubkey = cert.getPublicKey();
			return pubkey;
		} catch (Exception e) {
			LOGGER.error(e.toString());
			return null;
		}
	}
	
	
	/**
	 * 将证书文件读取为证书存储对象
	 * 
	 * @param pfxkeyfile
	 *            证书文件名
	 * @param keypwd
	 *            证书密码
	 * @param type
	 *            证书类型
	 * @return 证书对象
	 * @throws IOException 
	 */
	public static KeyStore getKeyInfo(String pfxkeyfile, String keypwd,
			String type) throws IOException {
		LOGGER.info("加载签名证书==>" + pfxkeyfile);
		InputStream fis = null;
		try {
			KeyStore ks = null;
			if ("JKS".equals(type)) {
				ks = KeyStore.getInstance(type);
			} else if ("PKCS12".equals(type)) {
				String jdkVendor = System.getProperty("java.vm.vendor");
				String javaVersion = System.getProperty("java.version");
				LOGGER.info("java.vm.vendor=[" + jdkVendor + "]");
				LOGGER.info("java.version=[" + javaVersion + "]");
//				Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
				if (null != jdkVendor && jdkVendor.startsWith("IBM")) {
					// 如果使用IBMJDK,则强制设置BouncyCastleProvider的指定位置,解决使用IBMJDK时兼容性问题
					Security.insertProviderAt(
							new org.bouncycastle.jce.provider.BouncyCastleProvider(),
							1);
					printSysInfo();
				}else{
					Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
				}
//				ks = KeyStore.getInstance(type, "BC");
				ks = KeyStore.getInstance(type);
			}
			LOGGER.info("Load RSA CertPath=[" + pfxkeyfile + "],Pwd=["
					+ keypwd + "]");
			if(pfxkeyfile.indexOf(".jar!")>0){ //就是以JAR形式嵌入
				 fis=CertFileUtil.getJarInputStream(pfxkeyfile);
			}else{
				fis = new FileInputStream(pfxkeyfile);
			}
			char[] nPassword = null;
			nPassword = null == keypwd || "".equals(keypwd.trim()) ? null
					: keypwd.toCharArray();
			if (null != ks) {
				ks.load(fis, nPassword);
			}
			return ks;
		} catch (Exception e) {
			if (Security.getProvider("BC") == null) {
				LOGGER.info("BC Provider not installed.");
			}
			LOGGER.error("getKeyInfo Error", e);
			if ((e instanceof KeyStoreException) && "PKCS12".equals(type)) {
				Security.removeProvider("BC");
			}
			return null;
		}finally{
			if(null!=fis)
				fis.close();
		}
	}

	// 打印系统环境信息
	public static void printSysInfo() {
		LOGGER.info("================= SYS INFO begin====================");
		LOGGER.info("os_name:" + System.getProperty("os.name"));
		LOGGER.info("os_arch:" + System.getProperty("os.arch"));
		LOGGER.info("os_version:" + System.getProperty("os.version"));
		LOGGER.info("java_vm_specification_version:"
				+ System.getProperty("java.vm.specification.version"));
		LOGGER.info("java_vm_specification_vendor:"
				+ System.getProperty("java.vm.specification.vendor"));
		LOGGER.info("java_vm_specification_name:"
				+ System.getProperty("java.vm.specification.name"));
		LOGGER.info("java_vm_version:"
				+ System.getProperty("java.vm.version"));
		LOGGER.info("java_vm_name:" + System.getProperty("java.vm.name"));
		LOGGER.info("java.version:" + System.getProperty("java.version"));
		printProviders();
		LOGGER.info("================= SYS INFO end=====================");
	}
	
	public static void printProviders() {
		LOGGER.info("Providers List:");
		Provider[] providers = Security.getProviders();
		for (int i = 0; i < providers.length; i++) {
			LOGGER.info(i + 1 + "." + providers[i].getName());
		}
	}

	/**
	 * 证书文件过滤器
	 * 
	 */
	static class CerFilter implements FilenameFilter {
		public boolean isCer(String name) {
			if (name.toLowerCase().endsWith(".cer")) {
				return true;
			} else {
				return false;
			}
		}
		public boolean accept(File dir, String name) {
			return isCer(name);
		}
	}
	
	
	
	public static String getCertIdByKeyStoreMap(String certPath, String certPwd) {
		if (!certKeyStoreMap.containsKey(certPath)) {
			// 缓存中未查询到,则加载RSA证书
			loadRsaCert(certPath, certPwd);
		}
		return getCertIdIdByStore(certKeyStoreMap.get(certPath));
	}

	private static String getCertIdIdByStore(KeyStore keyStore) {
		Enumeration<String> aliasenum = null;
		try {
			aliasenum = keyStore.aliases();
			String keyAlias = null;
			if (aliasenum.hasMoreElements()) {
				keyAlias = aliasenum.nextElement();
			}
			X509Certificate cert = (X509Certificate) keyStore
					.getCertificate(keyAlias);
			return cert.getSerialNumber().toString();
		} catch (KeyStoreException e) {
			LOGGER.error("getCertIdIdByStore Error", e);
			return null;
		}
	}
	

	/**
	 * 获取证书容器
	 * 
	 * @return
	 */
	public static Map<String, X509Certificate> getCertMap() {
		return certMap;
	}

	/**
	 * 设置证书容器
	 * 
	 * @param certMap
	 */
	public static void setCertMap(Map<String, X509Certificate> certMap) {
		CertUtil.certMap = certMap;
	}
	
	/**
	 * 使用模和指数生成RSA公钥 注意：此代码用了默认补位方式，为RSA/None/PKCS1Padding，不同JDK默认的补位方式可能不同
	 * 
	 * @param modulus
	 *            模
	 * @param exponent
	 *            指数
	 * @return
	 */
	public static PublicKey getPublicKey(String modulus, String exponent) {
		try {
			BigInteger b1 = new BigInteger(modulus);
			BigInteger b2 = new BigInteger(exponent);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			RSAPublicKeySpec keySpec = new RSAPublicKeySpec(b1, b2);
			return keyFactory.generatePublic(keySpec);
		} catch (Exception e) {
			LOGGER.error("构造RSA公钥失败：" + e);
			return null;
		}
	}
	
	/**
	 * 使用模和指数的方式获取公钥对象
	 * 
	 * @return
	 */
	public static PublicKey getEncryptTrackCertPublicKey(String modulus,
			String exponent) {
		if (StringUtil.isBlank(modulus) ||StringUtil.isBlank(exponent)) {
			LOGGER.error("[modulus] OR [exponent] invalid");
			return null;
		}
		return getPublicKey(modulus, exponent);
	}
	
	
}
