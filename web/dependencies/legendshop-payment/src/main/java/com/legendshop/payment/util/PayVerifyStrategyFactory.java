package com.legendshop.payment.util;

import com.legendshop.model.constant.PayTypeEnum;


/**
 * 支付验证策略工厂
 * @author Tony哥
 *
 */
public class PayVerifyStrategyFactory {

    private static PayVerifyStrategy[] strategyArray = new PayVerifyStrategy[11];

    static {
    	//支付宝
    	strategyArray[0]=new AliPayVerifyStrategyImpl();
    	//微信APP
    	strategyArray[1]=new WeiXinAppPayVerifyStrategyImpl();
    	//微信
    	strategyArray[2]=new WeiXinPayVerifyStrategyImpl();
    	//腾讯支付
    	strategyArray[3]=new TenPayVerifyStrategyImpl();
    	//通联支付
    	strategyArray[4]=new TongLianPayVerifyStrategyImpl();
    	//银联支付
    	strategyArray[5]=new UnionPayVerifyStrategyImpl();
    	//模拟支付
    	strategyArray[6]=new SimulatePayVerifyStrategyImpl();
    	//钱包全额支付
    	strategyArray[7]=new FullPayVerifyStrategyImpl();
    	//京东支付
    	strategyArray[8]=new JDPayVerifyStrategyImpl();
    	//快钱支付
    	strategyArray[9]=new KQPayVerifyStrategyImpl();
    	//微信小程序支付
    	strategyArray[10]=new WeiXinMpPayVerifyStrategyImpl();
    }

    private PayVerifyStrategyFactory() {
    }

    private static class InstanceHolder {
        public static PayVerifyStrategyFactory instance = new PayVerifyStrategyFactory();
    }

    public static PayVerifyStrategyFactory getInstance() {
        return InstanceHolder.instance;
    }

	public PayVerifyStrategy creator(String payWayCode) {
		
		 if(PayTypeEnum.isAliPay(payWayCode)){
			return strategyArray[0];
		}
		else if(PayTypeEnum.WX_APP_PAY.value().equals(payWayCode)){
			return strategyArray[1];
		}
		else if(PayTypeEnum.WX_MP_PAY.value().equals(payWayCode)){
			return strategyArray[10];
		}
		else if(PayTypeEnum.isWeiXin(payWayCode)){
			return strategyArray[2];
		}
		else if(PayTypeEnum.TENPAY.value().equals(payWayCode)){
			return strategyArray[3];
		}
		/*  通联支付*/
		else if(PayTypeEnum.TONGLIAN_PAY.value().equals(payWayCode) 
				|| PayTypeEnum.TONGLIAN_CROSSBORDER_PAY.value().equals(payWayCode)){ //通联支付
			return strategyArray[4];
		}
		else if(PayTypeEnum.UNIONPAY.value().equals(payWayCode)){
			return strategyArray[5];
		}
		else if(PayTypeEnum.SIMULATE_PAY.value().equals(payWayCode)){
			return strategyArray[6];
		}
		else if(PayTypeEnum.FULL_PAY.value().equals(payWayCode)){
			return strategyArray[7];
		}
		else if(PayTypeEnum.JD_PAY.value().equals(payWayCode)){
			return strategyArray[8];
		}
		else if(PayTypeEnum.KQ_PAY.value().equals(payWayCode)){
			return strategyArray[9];
		}
		return null;
	}

}
