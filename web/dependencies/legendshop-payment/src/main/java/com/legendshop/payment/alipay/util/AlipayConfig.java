/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.payment.alipay.util;

import com.legendshop.config.PropertiesUtil;
import com.legendshop.config.PropertiesUtilManager;

/**
 * 支付宝配置
 * LegendShop 版权所有,并保留所有权利。
 * 
 * 
 * 官方网站：http://www.legendesign.net
 */
public class AlipayConfig {
    static PropertiesUtil propertiesUtil;
    
    static {
      propertiesUtil= PropertiesUtilManager.getPropertiesUtil();
    }
  
    public static final String PC_DOMAIN_NAME = propertiesUtil.getPcDomainName();
	// 如何获取安全校验码和合作身份者ID
	// 1.访问支付宝商户服务中心(b.alipay.com)，然后用您的签约支付宝账号登陆.
	// 2.访问“技术服务”→“下载技术集成文档”（https://b.alipay.com/support/helperApply.htm?action=selfIntegration）
	// 3.在“自助集成帮助”中，点击“合作者身份(Partner ID)查询”、“安全校验码(Key)查询”
	
	/** The notify_url 交易过程中服务器通知的页面 要用 http://格式的完整路径，不允许加?id=123这类自定义参数. */
	public static String notify_url = PC_DOMAIN_NAME + "/pay/alipay/notify";


	/** The return_url.  付完款后跳转的页面 要用 http://格式的完整路径，不允许加?id=123这类自定义参数*/
	public static String return_url = PC_DOMAIN_NAME  + "/pay/alipay/response";


	public static String return_bank_url = PC_DOMAIN_NAME + "/pay/alipayBank/notify";;


	public static String notify_bank_url = PC_DOMAIN_NAME + "/pay/alipayBank/response";

	
	public static  String  PAY_RETURN_URL=PC_DOMAIN_NAME + "/pay/alipay/retrun/notify";

	
	
	/**
	 * 支付参数配置(支付宝)
	 */
    public static final String PARTNER = "partner";
    
    public static final String KEY = "key";
    
    public static final String SELLER_EMAIL = "seller_email";
    
	public static final String RSA_PRIVATE = "rsa_private";
	
	public static final String RSA_PUBLIC = "rsa_publish";
	
	/**
	 * 支付宝当面付参数
	 */
    public static final String APPID = "appId";
    
    
    
	/** The input_charset. */
	public static String input_charset = "UTF-8";
	
	/** The transport. 
	 * 访问模式,根据自己的服务器是否支持ssl访问，若支持请选择https；若不支持请选择http
	 * */
	public static String transport = "http";
	
	/** The sign_type. 
	 * 签名方式 不需修改
	 */
	public static String sign_type = "RSA2";
	
	//支付服务地址
	public static String serverUrl = "https://openapi.alipay.com/gateway.do";
	
	
}
