/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.payment.jd.util;

/**
 * 支付宝配置 LegendShop 版权所有,并保留所有权利。
 * 
 * 
 * 官方网站：http://www.legendesign.net
 */
public class JDConfig {

	public static final String MID = "mid";
	
	public static final String KEY = "key";

}
