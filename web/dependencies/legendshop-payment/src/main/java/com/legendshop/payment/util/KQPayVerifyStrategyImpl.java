package com.legendshop.payment.util;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.payment.kq.util.KQConfig;
import com.legendshop.payment.kq.util.KQMD5Util;

/**
 * 块钱支付的验证回调
 * 
 * @author Tony
 * 
 */
public class KQPayVerifyStrategyImpl implements PayVerifyStrategy {

	@Override
	public boolean verifyNotify(JSONObject jsonObject, Map<String, String> notifyMap) {

		String key = jsonObject.getString(KQConfig.KEY);
		// 获取加密签名串
		String signMsg = notifyMap.get("signMsg");

		// 生成加密串。必须保持如下顺序。
		String merchantSignMsgVal = "";
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "merchantAcctId", notifyMap.get("merchantAcctId"));
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "version", notifyMap.get("version"));
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "language", notifyMap.get("language"));
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "signType", notifyMap.get("signType"));
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "payType", notifyMap.get("payType"));
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "bankId", notifyMap.get("bankId"));
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "orderId", notifyMap.get("orderId"));
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "orderTime", notifyMap.get("orderTime"));
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "orderAmount", notifyMap.get("orderAmount"));
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "dealId", notifyMap.get("dealId"));
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "bankDealId", notifyMap.get("bankDealId"));
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "dealTime", notifyMap.get("dealTime"));
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "payAmount", notifyMap.get("payAmount"));
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "fee", notifyMap.get("fee"));
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "ext1", notifyMap.get("ext1"));
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "ext2", notifyMap.get("ext2"));
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "payResult", notifyMap.get("payResult"));
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "errCode", notifyMap.get("errCode"));
		merchantSignMsgVal = appendParam(merchantSignMsgVal, "key", key);

		String merchantSignMsg = "";
		try {
			merchantSignMsg = KQMD5Util.md5Hex(merchantSignMsgVal.getBytes("gb2312")).toUpperCase();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		boolean verify_result = signMsg.toUpperCase().equals(merchantSignMsg.toUpperCase());

		PaymentLog.info("快钱支付的验证回调数据校验 verify_result:{}", verify_result);

		if (verify_result) {
			return true;
		}
		return false;
	}

	private String appendParam(String returnStr, String paramId, String paramValue) {
		if (!returnStr.equals("")) {
			if (!paramValue.equals("")) {
				returnStr = returnStr + "&" + paramId + "=" + paramValue;
			}
		} else {
			if (!paramValue.equals("")) {
				returnStr = paramId + "=" + paramValue;
			}
		}
		return returnStr;
	}

}
