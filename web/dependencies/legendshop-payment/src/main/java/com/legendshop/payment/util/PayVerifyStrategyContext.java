package com.legendshop.payment.util;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;

/**
 *  支付回调验证策略上下文
 * @author Tony
 *
 */
public class PayVerifyStrategyContext {

    private PayVerifyStrategy payStrategy;
    
    /**
     * 调用对应支付平台组装支付请求报文
     * @param payType
     * @param params
     * @return
     */
    public boolean verifyNotify(String payWayCode,JSONObject jsonObject,Map<String, String> notifyMap ){
        payStrategy = PayVerifyStrategyFactory.getInstance().creator(payWayCode);
        if(payStrategy==null){
        	return false;
        }
        return payStrategy.verifyNotify(jsonObject, notifyMap);
    }
    

}
