package com.legendshop.payment.weixin.processor;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import com.legendshop.uploader.AttachmentManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.RefundLog;
import com.legendshop.base.util.PhotoPathResolver;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.form.SysSubReturnForm;
import com.legendshop.payment.weixin.util.ClientCustomSSL;
import com.legendshop.payment.weixin.util.RequestHandler;
import com.legendshop.payment.weixin.util.WeiXinPayConfig;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PaymentRefundProcessor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.RandomStringUtils;
import com.legendshop.web.util.WeiXinUtil;

/**
 * https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_4&index=6
 * 微信APP支付退款
 * @author Tony
 *
 */
@Service("weXinAppPayRefundProcessor")
public class WeXinAppPayRefundProcessorImpl implements PaymentRefundProcessor {

	@Autowired
	private PayTypeService payTypeService;

	@Autowired
	AttachmentManager attachmentManager;

	@Override
	public Map<String, Object> refund(SysSubReturnForm returnForm) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", false);

		PayType payType = payTypeService.getPayTypeById(PayTypeEnum.WX_APP_PAY.value()); // 这种方式是卖家中心调用支付服务
		if (payType == null || AppUtils.isBlank(payType.getPaymentConfig())) {
			map.put("message", "支付方式不存在,请设置支付方式");
			return map;
		}
		JSONObject jsonObject = JSONObject.parseObject(payType.getPaymentConfig());

		Integer isRefund = jsonObject.getInteger("isRefund");
		if (isRefund != 1) {
			map.put("message", "请开启微信支付退款支持");
			return map;
		}
		String refundFile = jsonObject.getString("refundFile");
		if (AppUtils.isBlank(refundFile)) {
			map.put("message", "请上传微信的商户证书");
			return map;
		}
		

		/** 微信公众号APPID */
		String appid = jsonObject.getString(WeiXinPayConfig.APPID);
		/** 微信公众号绑定的商户号 */
		String mch_id = jsonObject.getString(WeiXinPayConfig.MCH_ID);
		String appsecret = jsonObject.getString(WeiXinPayConfig.APPSECRET);
		String partnerkey = jsonObject.getString(WeiXinPayConfig.PARTNERKEY);

		String out_trade_no = returnForm.getSubSettlementSn();// 支付时传入的商户订单号，与trade_no必填一个
		String trade_no = returnForm.getFlowTradeNo(); // 支付时返回的支付宝交易号，与out_trade_no必填一个
		String out_request_no = returnForm.getOutRequestNo(); // 本次退款请求流水号，部分退款时必传
		Double refund_amount = returnForm.getReturnAmount(); // 本次退款金额

		String refund_fee = String.valueOf(new BigDecimal(refund_amount).setScale(2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).intValue());

		String total_fee = String
				.valueOf(new BigDecimal(returnForm.getPayAmount()).setScale(2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).intValue());

		// 随机字符串
		String nonce_str = RandomStringUtils.randomAlphabetic(20); // 随机字符串，不长于32位
		SortedMap<String, String> packageParams = new TreeMap<String, String>();
		packageParams.put("appid", appid);
		packageParams.put("mch_id", mch_id);
		packageParams.put("nonce_str", nonce_str);
		packageParams.put("transaction_id", trade_no);
		packageParams.put("out_refund_no", out_request_no);
		packageParams.put("total_fee", total_fee);
		/** start weibf修改了微信退款金额 */
		// 仅针对老资金流商户使用
		// REFUND_SOURCE_UNSETTLED_FUNDS---未结算资金退款（默认使用未结算资金退款）
		// REFUND_SOURCE_RECHARGE_FUNDS---可用余额退款
		packageParams.put("refund_account", "REFUND_SOURCE_RECHARGE_FUNDS");
		packageParams.put("refund_fee", refund_fee);
		/** weibf修改了微信退款金额 end */
		packageParams.put("op_user_id", mch_id);
		RequestHandler reqHandler = new RequestHandler();
		reqHandler.init(null, appid, appsecret, partnerkey);

		String sign = reqHandler.createSign(packageParams);

		StringBuilder builder = new StringBuilder("<xml>");
		builder.append("<appid>").append(appid).append("</appid>");
		builder.append("<mch_id>").append(mch_id).append("</mch_id>");
		builder.append("<nonce_str>").append(nonce_str).append("</nonce_str>");
		builder.append("<op_user_id>").append(mch_id).append("</op_user_id>");
		builder.append("<out_refund_no>").append(out_request_no).append("</out_refund_no>");
		/** start weibf修改了微信退款金额 */
		builder.append("<refund_account>REFUND_SOURCE_RECHARGE_FUNDS</refund_account>");
		builder.append("<refund_fee>").append(refund_fee).append("</refund_fee>");
		/** weibf修改了微信退款金额 end */
		builder.append("<total_fee>").append(total_fee).append("</total_fee>");
		builder.append("<transaction_id>").append(trade_no).append("</transaction_id>");
		builder.append("<sign>").append(sign).append("</sign>");
		builder.append("</xml>");

		String payReturnResult = null;
		try {
			RefundLog.log("微信退款发送请求 ={} ", builder.toString());
//			String refundPath = PhotoPathResolver.getInstance().calculateFilePath(refundFile).getFilePath();
			String refundPath = attachmentManager.getPrivateObjectUrl(refundFile);
			RefundLog.log("微信 refundPath ={} ", refundPath);

			payReturnResult = ClientCustomSSL.doRefund(WeiXinPayConfig.refundURL, builder.toString(), mch_id, refundPath);
			RefundLog.log("微信 ClientCustomSSL  doRefund payReturnResult ={} ", payReturnResult);
			if (Constants.FAIL.equals(payReturnResult)) {
				map.put("message", payReturnResult);
				return map;
			}
			Map<String, String> payReturnMap = WeiXinUtil.parseXml(payReturnResult);
			
			RefundLog.info(" 微信退款 处理结果 payReturnMap={} ", JSONUtil.getJson(payReturnMap));
			String return_code = payReturnMap.get("return_code"); // 返回状态码
			if (!"SUCCESS".equals(return_code)) {
				String return_msg = payReturnMap.get("return_msg");
				RefundLog.info(" 微信退款失败 return_msg={} ",return_msg);
				map.put("message", return_msg);
				return map;
			} 
			
			String result_code=payReturnMap.get("result_code");
			if (!"SUCCESS".equals(result_code)) {
				String err_code = payReturnMap.get("err_code");
				String err_code_des = payReturnMap.get("err_code_des");
				RefundLog.info(" 微信退款失败 err_code={},err_code_des={} ",err_code,err_code_des);
				map.put("message", err_code_des);
				return map;
			} 
			map.put("result", true);
			map.put("message", "微信退款成功");
			return map;
		} catch (Exception e) {
			RefundLog.error(" 微信 ClientCustomSSL doRefund payReturnResult ={}   ", e);
			map.put("message", "微信退款失败");
			return map;
		}
	}

}
