/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.payment.jd.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.model.form.SysSubReturnForm;
import com.legendshop.payment.jd.util.JDConfig;
import com.legendshop.payment.jd.util.JDMD5;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PaymentProcessor;
import com.legendshop.util.AppUtils;




/**
 * 京东网关支付
 * @author tony
 *
 */
public class JdChinaBankPayProcessorImpl implements PaymentProcessor {
	
	private PayTypeService payTypeService;
	
	

	/* (non-Javadoc)
	 * @see com.legendshop.spi.processor.PaymentProcessor#payto(com.legendshop.spi.form.SysPaymentForm)
	 */
	@Override
	public  Map<String,Object> payto(SysPaymentForm paymentForm) {
		String v_oid = paymentForm.getSubSettlementSn();
		String v_amount = String.valueOf(paymentForm.getTotalAmount());
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", false);

		PayType payType = payTypeService.getPayTypeById(PayTypeEnum.UNIONPAY.value());
		if (payType == null || AppUtils.isBlank(payType.getPaymentConfig())) {
			map.put("message", "支付方式不存在,请设置支付方式");
			return map;
		} else if (payType.getIsEnable() == Constants.OFFLINE) {
			map.put("message", "该支付方式没有启用!");
			return map;
		}
		paymentForm.setPayTypeId(PayTypeEnum.UNIONPAY.value());
		paymentForm.setPayTypeName(PayTypeEnum.UNIONPAY.desc());
		
		String showUrl = paymentForm.getShowUrl();
		String notifyUrl = paymentForm.getDomainName() + "/payNotify/notify/" + PayTypeEnum.UNIONPAY.value();
		String returnUrl = paymentForm.getDomainName() + "/payNotify/response/" + PayTypeEnum.UNIONPAY.value();
		
		JSONObject jsonObject = JSONObject.parseObject(payType.getPaymentConfig());
		String v_mid = jsonObject.getString(JDConfig.MID); //商户号，这里为测试商户号1001，替换为自己的商户号(老版商户号为4位或5位,新版为8位)即可
		String key = jsonObject.getString(JDConfig.KEY);
		String v_moneytype="CNY";
		
		
		StringBuilder text=new StringBuilder();
		text.append(v_amount).append(v_moneytype).append(v_oid).append(v_mid).append(notifyUrl).append(key);
		
		JDMD5 jdmd5=new JDMD5();
		String v_md5info=jdmd5.getMD5ofStr(text.toString());  
		
		// 把请求参数打包成数组
		Map<String, String> sParaTemp = new HashMap<String, String>();
		sParaTemp.put("v_md5info", v_md5info);
		sParaTemp.put("v_mid", v_mid);
		sParaTemp.put("v_oid", v_oid);
		sParaTemp.put("v_amount", v_amount);
		sParaTemp.put("v_moneytype", v_moneytype);
		sParaTemp.put("v_url",returnUrl);
		
		/**
		 * 以下几项项为网上支付完成后，随支付反馈信息一同传给信息接收页
		 */
		sParaTemp.put("remark1","");
		sParaTemp.put("remark2",notifyUrl);
		
		/**
		 * 以下几项只是用来记录客户信息，可以不用，不影响支付
		 */
		sParaTemp.put("v_rcvname", "");
		sParaTemp.put("v_rcvaddr", "");
		sParaTemp.put("v_rcvtel", "");
		sParaTemp.put("v_rcvpost", "");
		sParaTemp.put("v_rcvemail","");
		sParaTemp.put("v_rcvmobile","");
		sParaTemp.put("v_ordername","");
		sParaTemp.put("v_orderaddr","");
		sParaTemp.put("v_ordertel","");
		sParaTemp.put("v_orderpost","");
		sParaTemp.put("v_orderemail","");
		sParaTemp.put("v_ordermobile","");
		
		String result=buildForm(sParaTemp);
		PaymentLog.info("send to JD PAY gateway " + result);
		map.put("result", true);
		map.put("message", result);
		return map;
	}
	
	/**
	 * 构造提交表单HTML数据 直接支付接口.
	 * 
	 * @param sParaTemp
	 *            请求参数数组
	 * @param gateway
	 *            网关地址
	 * @param strMethod
	 *            提交方式。两个值可选：post、get
	 * @param strButtonName
	 *            确认按钮显示文字
	 * @param validateKey
	 *            the validate key
	 * @return 提交表单HTML文本
	 */
	public String buildForm(Map<String, String> sPara) {
		List<String> keys = new ArrayList<String>(sPara.keySet());
		StringBuffer sbHtml = new StringBuffer();
		sbHtml.append("<form id=\"jdpaysubmit\" name=\"jdpaysubmit\"  method=\"POST\" action=\"https://pay3.chinabank.com.cn/PayGate\">");
		for (int i = 0; i < keys.size(); i++) {
			String name = keys.get(i);
			String value = sPara.get(name);
			sbHtml.append("<input type=\"hidden\" name=\"" + name
					+ "\" value=\"" + value + "\"/>");
		}
		String strButtonName = "确认";
		// submit按钮控件请不要含有name属性
		sbHtml.append("<input type=\"submit\" value=\"" + strButtonName
				+ "\" style=\"display:none;\"></form>");
		sbHtml.append("<script>document.forms['jdpaysubmit'].submit();</script>");
		return sbHtml.toString();

	}

	public String triggerReturn(SysSubReturnForm returnForm) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setPayTypeService(PayTypeService payTypeService) {
		this.payTypeService = payTypeService;
	}


}
