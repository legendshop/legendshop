package com.legendshop.payment.unionPay.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.legendshop.model.constant.CertFileEnum;
import com.legendshop.util.AppUtils;

public class SDKConfig {

	/** 前台请求URL. */
	private String frontRequestUrl;
	/** 后台请求URL. */
	private String backRequestUrl;
	/** 单笔查询 */
	private String singleQueryUrl;
	/** 批量查询 */
	private String batchQueryUrl;
	/** 批量交易 */
	private String batchTransUrl;
	/** 文件传输 */
	private String fileTransUrl;
	/** 签名证书路径. */
	private String signCertPath;
	/** 签名证书密码. */
	private String signCertPwd;
	/** 签名证书类型. */
	private String signCertType;
	/** 加密公钥证书路径. */
	private String encryptCertPath;
	/** 验证签名公钥证书目录. */
	private String validateCertDir;
	/** 按照商户代码读取指定签名证书目录. */
	private String signCertDir;
	/** 磁道加密证书路径. */
	private String encryptTrackCertPath;
	/** 有卡交易. */
	private String cardRequestUrl;
	/** app交易 */
	private String appRequestUrl;
	/** 证书使用模式(单证书/多证书) */
	private String singleMode;

	/*缴费相关地址*/
	private String jfFrontRequestUrl;
	private String jfBackRequestUrl;
	private String jfSingleQueryUrl;
	private String jfCardRequestUrl;
	private String jfAppRequestUrl;
	
	
	/** 配置文件中的前台URL常量. */
	private  final String SDK_FRONT_URL = "acpsdk.frontTransUrl";
	/** 配置文件中的后台URL常量. */
	private  final String SDK_BACK_URL = "acpsdk.backTransUrl";
	/** 配置文件中的单笔交易查询URL常量. */
	private  final String SDK_SIGNQ_URL = "acpsdk.singleQueryUrl";
	/** 配置文件中的批量交易查询URL常量. */
	private  final String SDK_BATQ_URL = "acpsdk.batchQueryUrl";
	/** 配置文件中的批量交易URL常量. */
	private  final String SDK_BATTRANS_URL = "acpsdk.batchTransUrl";
	/** 配置文件中的文件类交易URL常量. */
	private  final String SDK_FILETRANS_URL = "acpsdk.fileTransUrl";
	/** 配置文件中的有卡交易URL常量. */
	private  final String SDK_CARD_URL = "acpsdk.cardTransUrl";
	/** 配置文件中的app交易URL常量. */
	private  final String SDK_APP_URL = "acpsdk.appTransUrl";


	
	/** 以下缴费产品使用，其余产品用不到，无视即可 */
	// 前台请求地址
	private  final String JF_SDK_FRONT_TRANS_URL= "acpsdk.jfFrontTransUrl";
	// 后台请求地址
	private  final String JF_SDK_BACK_TRANS_URL="acpsdk.jfBackTransUrl";
	// 单笔查询请求地址
	private  final String JF_SDK_SINGLE_QUERY_URL="acpsdk.jfSingleQueryUrl";
	// 有卡交易址
	private  final String JF_SDK_CARD_TRANS_URL="acpsdk.jfCardTransUrl";
	// App交易地址
	private  final String JF_SDK_APP_TRANS_URL="acpsdk.jfAppTransUrl";
	
	
	
	
	/** 配置文件中签名证书路径常量. */
	/*private  final String SDK_SIGNCERT_PATH = "acpsdk.signCert.path";*/
	/** 配置文件中签名证书密码常量. */
	private  final String SDK_SIGNCERT_PWD = "acpsdk.signCert.pwd";
	/** 配置文件中签名证书类型常量. */
	private  final String SDK_SIGNCERT_TYPE = "acpsdk.signCert.type";
	/** 配置文件中密码加密证书路径常量. */
	private  final String SDK_ENCRYPTCERT_PATH = "acpsdk.encryptCert.path";
	/** 配置文件中磁道加密证书路径常量. */
	private  final String SDK_ENCRYPTTRACKCERT_PATH = "acpsdk.encryptTrackCert.path";
	/** 配置文件中验证签名证书目录常量. */
/*	private  final String SDK_VALIDATECERT_DIR = "acpsdk.validateCert.dir";*/

	/** 配置文件中是否加密cvn2常量. */
	private  final String SDK_CVN_ENC = "acpsdk.cvn2.enc";
	/** 配置文件中是否加密cvn2有效期常量. */
	private  final String SDK_DATE_ENC = "acpsdk.date.enc";
	/** 配置文件中是否加密卡号常量. */
	private  final String SDK_PAN_ENC = "acpsdk.pan.enc";
	/** 配置文件中证书使用模式 */
	private  final String SDK_SINGLEMODE = "acpsdk.singleMode";
	
	private static SDKConfig sdkConfig=null;
	
	private final static Logger LOGGER=LoggerFactory.getLogger(SDKConfig.class);
	
	private SDKConfig(){
		/** 属性文件对象. */
	    Properties properties=null;
	    InputStream in = null;
		try {
			in =Thread.currentThread().getContextClassLoader().getResourceAsStream("acp_sdk.properties");
			if (null != in) {
				BufferedReader bf = new BufferedReader(new InputStreamReader(in, "utf-8"));
				properties = new Properties();
				try {
					properties.load(bf);
				} catch (IOException e) {
					throw e;
				}
			} else {
				return;
			}
			loadProperties(properties);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (null != in) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			properties=null;
			in=null;
		}
		
	}
	


	/**
	 * 根据传入的 {@link #load(java.util.Properties)}对象设置配置参数
	 * 
	 * @param pro
	 */
	public void loadProperties(Properties pro) {
		LOGGER.info("开始从属性文件中加载配置项");
		String value = null;
		value = pro.getProperty(SDK_SINGLEMODE);
		if (SDKUtil.isEmpty(value) || SDKConstants.TRUE_STRING.equals(value)) {
			this.singleMode = SDKConstants.TRUE_STRING;
			LOGGER.info("单证书模式，使用配置文件配置的私钥签名证书，SingleCertMode:[" + this.singleMode + "]");
			// 单证书模式
			URL url=Thread.currentThread().getContextClassLoader().getResource(CertFileEnum.UNIONPAY_ACP_TEST_SIGN.value());
			if(AppUtils.isBlank(url)){
				return ;
			}
			String path=url.getPath();
			if (!SDKUtil.isEmpty(path)) {
				this.signCertPath = path;
				LOGGER.info("配置项：私钥签名证书路径==>"+path +" 已加载");
			}
			value = pro.getProperty(SDK_SIGNCERT_PWD);
			if (!SDKUtil.isEmpty(value)) {
				this.signCertPwd = value.trim();
				LOGGER.info("配置项：私钥签名证书密码==>"+SDK_SIGNCERT_PWD +" 已加载");
			}
			value = pro.getProperty(SDK_SIGNCERT_TYPE);
			if (!SDKUtil.isEmpty(value)) {
				this.signCertType = value.trim();
				LOGGER.info("配置项：私钥签名证书类型==>"+SDK_SIGNCERT_TYPE +"==>"+ value+" 已加载");
			}
		} else {
			// 多证书模式
			this.singleMode = SDKConstants.FALSE_STRING;
			LOGGER.info("多证书模式，不需要加载配置文件中配置的私钥签名证书，SingleMode:[" + this.singleMode + "]");
		}
		value = pro.getProperty(SDK_ENCRYPTCERT_PATH);
		if (!SDKUtil.isEmpty(value)) {
			this.encryptCertPath = value.trim();
			LOGGER.info("配置项：敏感信息加密证书==>"+SDK_ENCRYPTCERT_PATH +"==>"+ value+" 已加载");
		}
		
	/*	String VALIDATECERT_DIR=Thread.currentThread().getContextClassLoader().getResource("certs/acp_test_sign.pfx").getPath();
		
		value = pro.getProperty(VALIDATECERT_DIR);
		if (!SDKUtil.isEmpty(value)) {
			this.validateCertDir = value.trim();
			LOGGER.info("配置项：验证签名证书路径(这里配置的是目录，不要指定到公钥文件)==>"+VALIDATECERT_DIR +"==>"+ value+" 已加载");
		}*/
		value = pro.getProperty(SDK_FRONT_URL);
		if (!SDKUtil.isEmpty(value)) {
			this.frontRequestUrl = value.trim();
		}
		value = pro.getProperty(SDK_BACK_URL);
		if (!SDKUtil.isEmpty(value)) {
			this.backRequestUrl = value.trim();
		}
		value = pro.getProperty(SDK_BATQ_URL);
		if (!SDKUtil.isEmpty(value)) {
			this.batchQueryUrl = value.trim();
		}
		value = pro.getProperty(SDK_BATTRANS_URL);
		if (!SDKUtil.isEmpty(value)) {
			this.batchTransUrl = value.trim();
		}
		value = pro.getProperty(SDK_FILETRANS_URL);
		if (!SDKUtil.isEmpty(value)) {
			this.fileTransUrl = value.trim();
		}
		value = pro.getProperty(SDK_SIGNQ_URL);
		if (!SDKUtil.isEmpty(value)) {
			this.singleQueryUrl = value.trim();
		}
		value = pro.getProperty(SDK_CARD_URL);
		if (!SDKUtil.isEmpty(value)) {
			this.cardRequestUrl = value.trim();
		}
		value = pro.getProperty(SDK_APP_URL);
		if (!SDKUtil.isEmpty(value)) {
			this.appRequestUrl = value.trim();
		}
		value = pro.getProperty(SDK_ENCRYPTTRACKCERT_PATH);
		if (!SDKUtil.isEmpty(value)) {
			this.encryptTrackCertPath = value.trim();
		}
		
		/**缴费部分**/
		value = pro.getProperty(JF_SDK_FRONT_TRANS_URL);
		if (!SDKUtil.isEmpty(value)) {
			this.jfFrontRequestUrl = value.trim();
		}

		value = pro.getProperty(JF_SDK_BACK_TRANS_URL);
		if (!SDKUtil.isEmpty(value)) {
			this.jfBackRequestUrl = value.trim();
		}
		
		value = pro.getProperty(JF_SDK_SINGLE_QUERY_URL);
		if (!SDKUtil.isEmpty(value)) {
			this.jfSingleQueryUrl = value.trim();
		}
		
		value = pro.getProperty(JF_SDK_CARD_TRANS_URL);
		if (!SDKUtil.isEmpty(value)) {
			this.jfCardRequestUrl = value.trim();
		}
		
		value = pro.getProperty(JF_SDK_APP_TRANS_URL);
		if (!SDKUtil.isEmpty(value)) {
			this.jfAppRequestUrl = value.trim();
		}
		
	}
	
	
	
	public static SDKConfig getConfig(){
		if(sdkConfig==null){
			synchronized (SDKConfig.class) {
				if(sdkConfig==null){
					 sdkConfig= new SDKConfig();
				}
			}
		}
		return sdkConfig;
	}


	public String getFrontRequestUrl() {
		return frontRequestUrl;
	}


	public String getBackRequestUrl() {
		return backRequestUrl;
	}


	public String getSingleQueryUrl() {
		return singleQueryUrl;
	}


	public String getBatchQueryUrl() {
		return batchQueryUrl;
	}


	public String getBatchTransUrl() {
		return batchTransUrl;
	}


	public String getFileTransUrl() {
		return fileTransUrl;
	}


	public String getSignCertPath() {
		return signCertPath;
	}


	public String getSignCertPwd() {
		return signCertPwd;
	}


	public String getSignCertType() {
		return signCertType;
	}


	public String getEncryptCertPath() {
		return encryptCertPath;
	}


	public String getValidateCertDir() {
		return validateCertDir;
	}


	public String getSignCertDir() {
		return signCertDir;
	}


	public String getEncryptTrackCertPath() {
		return encryptTrackCertPath;
	}


	public String getCardRequestUrl() {
		return cardRequestUrl;
	}


	public String getAppRequestUrl() {
		return appRequestUrl;
	}


	public String getSingleMode() {
		return singleMode;
	}


	public String getJfFrontRequestUrl() {
		return jfFrontRequestUrl;
	}


	public String getJfBackRequestUrl() {
		return jfBackRequestUrl;
	}


	public String getJfSingleQueryUrl() {
		return jfSingleQueryUrl;
	}


	public String getJfCardRequestUrl() {
		return jfCardRequestUrl;
	}


	public String getJfAppRequestUrl() {
		return jfAppRequestUrl;
	}
	
	
	
	

}
