package com.legendshop.payment.util;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.payment.alipay.util.AlipayConfig;


/**
 * 支付宝的验证回调
 * @author Tony
 *
 */
public class AliPayVerifyStrategyImpl implements PayVerifyStrategy {

	@Override
	public boolean verifyNotify(JSONObject jsonObject, Map<String, String> notifyMap) {
		/*String partner = paymentConfig.get(AlipayConfig.PARTNER);
		String key = paymentConfig.get(AlipayConfig.KEY);
		String rsa_publish = paymentConfig.get(AlipayConfig.RSA_PUBLIC);
		String rsa_private = paymentConfig.get(AlipayConfig.RSA_PRIVATE);
		return AlipayNotify.verify(partner, rsa_publish,notifyMap);*/
		
		String partner = jsonObject.getString(AlipayConfig.PARTNER);
		String key = jsonObject.getString(AlipayConfig.KEY);
		String rsa_publish = jsonObject.getString(AlipayConfig.RSA_PUBLIC);
		String rsa_private = jsonObject.getString(AlipayConfig.RSA_PRIVATE);
		
		try {
			boolean verify_result = AlipaySignature.rsaCheckV1(notifyMap, rsa_publish, AlipayConfig.input_charset, AlipayConfig.sign_type);
			PaymentLog.info("支付宝的验证回调数据校验 verify_result:{}" ,verify_result);
			return verify_result;
		} catch (AlipayApiException e) {
			e.printStackTrace();
			return false;
		}
		
	}

}
