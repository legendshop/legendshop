package com.legendshop.payment.weixin.enums;


/**
 * @功能说明: 微信支付,交易类型枚举类
 * @创建者: Tony
 * @版本:V1.0
 */
public enum WeiXinTradeTypeEnum {

	/**
	 * JSAPI--公众号支付、NATIVE--原生扫码支付、APP--app支付，统一下单接口trade_type的传参可参考这里
	 * MICROPAY--刷卡支付，刷卡支付有单独的支付接口，不调用统一下单接口
	 **/
	JSAPI("公众号支付"), NATIVE("原生扫码支付"), APP("app支付"), MICROPAY("刷卡支付"),MWEB("H5支付");

	/** 描述 */
	private String desc;

	private WeiXinTradeTypeEnum(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public static WeiXinTradeTypeEnum getEnum(String name) {
		WeiXinTradeTypeEnum[] arry = WeiXinTradeTypeEnum.values();
		for (int i = 0; i < arry.length; i++) {
			if (arry[i].name().equalsIgnoreCase(name)) {
				return arry[i];
			}
		}
		return null;
	}

	
}
