package com.legendshop.payment.unionPay.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * 获取JAR的文件流数据
 * 
 * @author Tony
 * 
 */
public class CertFileUtil {

	/**
	 * 从ClassPath中的Jar包读取某文件夹下的所有文件
	 * jar:file:/c:/xxx/my.jar!/com/mycompany/MyClass.class
	 */
	public static InputStream getJarInputStream(String dirPath,String fileName) throws IOException {
		int jarIndex=dirPath.indexOf("!/");
		if(jarIndex>0){
			dirPath = dirPath.substring(0, jarIndex+2);
			dirPath = "jar:"+ dirPath;
		}
		System.out.println(dirPath);
		URL jarURL = new URL(dirPath); 
		JarURLConnection jarCon = (JarURLConnection) jarURL.openConnection(); 
	    JarFile jarFile = jarCon.getJarFile(); 
		JarEntry jarEntry = jarFile.getJarEntry(fileName); 
	    InputStream inputStream = jarFile.getInputStream(jarEntry);  
	    return inputStream;
	}
	
	/**
	 * 从ClassPath中的Jar包读取某文件夹下的所有文件
	 * jar:file:/c:/almanac/my.jar!/com/mycompany/MyClass.class
	 */
	public static InputStream getJarInputStream(String dirPath) throws IOException {
		int jarIndex=dirPath.indexOf("!/");
		if(jarIndex>0){
			dirPath = "jar:"+ dirPath;
		}
		System.out.println(dirPath);
		URL jarURL = new URL(dirPath); 
		JarURLConnection jarCon = (JarURLConnection) jarURL.openConnection(); 
	    JarFile jarFile = jarCon.getJarFile(); 
	    JarEntry jarEntry = jarCon.getJarEntry();
	    InputStream inputStream = jarFile.getInputStream(jarEntry);  
	    return inputStream;
	}
	
	
}
