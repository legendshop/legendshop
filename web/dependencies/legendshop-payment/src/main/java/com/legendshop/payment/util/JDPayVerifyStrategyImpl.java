package com.legendshop.payment.util;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.payment.jd.util.JDConfig;
import com.legendshop.payment.jd.util.JDMD5;

/**
 * 京东支付的验证回调
 * 
 * @author Tony
 * 
 */
public class JDPayVerifyStrategyImpl implements PayVerifyStrategy {

	@Override
	public boolean verifyNotify(JSONObject jsonObject, Map<String, String> notifyMap) {

		String v_oid = notifyMap.get("v_oid"); // 订单号
		String v_pstatus = notifyMap.get("v_pstatus"); // 支付结果，20支付完成；30支付失败；
		String v_amount = notifyMap.get("v_amount"); // 订单实际支付金额
		String v_moneytype = notifyMap.get("v_moneytype"); // 币种
		String v_md5str = notifyMap.get("v_md5str"); // MD5校验码
		
		StringBuilder text=new StringBuilder();
		text.append(v_oid).append(v_pstatus).append(v_amount).append(v_moneytype).append(jsonObject.get(JDConfig.KEY));
		
		JDMD5 jdmd5=new JDMD5();
		String v_md5text = jdmd5.getMD5ofStr(text.toString()).toUpperCase();
		PaymentLog.log("v_md5text {}   ----  v_md5str {}  ", v_md5text,v_md5str);
		
		boolean verify_result=v_md5str.equals(v_md5text);
		
		PaymentLog.info("JD 支付的验证回调数据校验 verify_result:{}" ,verify_result);
		return verify_result;
		
	}
	
	

}
