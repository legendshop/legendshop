package com.legendshop.payment.util;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.payment.weixin.util.RequestHandler;
import com.legendshop.payment.weixin.util.WeiXinPayConfig;
import com.legendshop.util.JSONUtil;

/**
 * 微信支付的验证回调
 * 
 * @author Tony
 * 
 */
public class WeiXinPayVerifyStrategyImpl implements PayVerifyStrategy {

	@Override
	public boolean verifyNotify(JSONObject jsonObject, Map<String, String> notifyMap) {

		// 先进行校验，是否是微信服务器返回的信息
		/** 微信公众号APPID */
		String appid = jsonObject.getString(WeiXinPayConfig.APPID);
		/** 微信公众号绑定的商户号 */
		String app_secret = jsonObject.getString(WeiXinPayConfig.APPSECRET);
		String partnerkey = jsonObject.getString(WeiXinPayConfig.PARTNERKEY);
		String token = jsonObject.getString(WeiXinPayConfig.TOKEN);

		// 用于验签
		SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();
		for (Object keyValue : notifyMap.keySet()) {
			/** 输出返回的订单支付信息 */
			if (!"sign".equals(keyValue)) {
				parameters.put(keyValue, notifyMap.get(keyValue));
			}
		}

		RequestHandler reqHandler = new RequestHandler();
		reqHandler.init(token, appid, app_secret, partnerkey);
		String checkSign = reqHandler.createSign(parameters);
		
		PaymentLog.info(" createSign parameters={}" ,JSONUtil.getJson(parameters));
		PaymentLog.info(" createSign {}" ,checkSign);
		PaymentLog.info(" notifyMap sign {}" ,notifyMap.get("sign"));
		
		boolean verify_result=checkSign.equals(notifyMap.get("sign"));
		PaymentLog.info("微信支付的验证回调数据校验 verify_result:{}" ,verify_result);
		return verify_result;
	}
	
	

}
