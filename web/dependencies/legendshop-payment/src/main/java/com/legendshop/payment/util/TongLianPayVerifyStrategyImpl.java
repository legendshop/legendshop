package com.legendshop.payment.util;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.base.util.PhotoPathDto;
import com.legendshop.base.util.PhotoPathResolver;
import com.legendshop.payment.tl.util.TLPayConfig;
import com.legendshop.util.JSONUtil;

/**
 * 通联支付的验证回调
 * 
 * @author Tony
 * 
 */
public class TongLianPayVerifyStrategyImpl implements PayVerifyStrategy {

	@Override
	public boolean verifyNotify(JSONObject jsonObject, Map<String, String> respParam) {

		String merchantId = respParam.get("merchantId");
		String version = respParam.get("version");
		String language = respParam.get("language");
		String signType = respParam.get("signType");
		String payType = respParam.get("payType");
		String issuerId = respParam.get("issuerId");
		String paymentOrderId = respParam.get("paymentOrderId");
		String orderNo = respParam.get("orderNo");
		String orderDatetime = respParam.get("orderDatetime");
		String orderAmount = respParam.get("orderAmount");
		String payDatetime = respParam.get("payDatetime");
		String payAmount = respParam.get("payAmount");
		String ext1 = respParam.get("ext1");
		String ext2 = respParam.get("ext2");
		String payResult = respParam.get("payResult");
		String errorCode = respParam.get("errorCode");
		String returnDatetime = respParam.get("returnDatetime");
		String signMsg = respParam.get("signMsg");

		PaymentLog.log(" -------------- tonglian pay notify 接收报文返回  orderNo = {} ", orderNo);
		PaymentLog.log(" -------------- respParam = {} ", JSONUtil.getJson(respParam));
		PaymentLog.log(" -------------- tonglian pay notify   signType = {} ", signType);

		// 验签是商户为了验证接收到的报文数据确实是支付网关发送的。
		// 构造订单结果对象，验证签名。
		com.allinpay.ets.client.PaymentResult paymentResult = new com.allinpay.ets.client.PaymentResult();
		paymentResult.setMerchantId(merchantId);
		paymentResult.setVersion(version);
		paymentResult.setLanguage(language);
		paymentResult.setSignType(signType);
		paymentResult.setPayType(payType);
		paymentResult.setIssuerId(issuerId);
		paymentResult.setPaymentOrderId(paymentOrderId);
		paymentResult.setOrderNo(orderNo);
		paymentResult.setOrderDatetime(orderDatetime);
		paymentResult.setOrderAmount(orderAmount);
		paymentResult.setPayDatetime(payDatetime);
		paymentResult.setPayAmount(payAmount);
		paymentResult.setExt1(ext1);
		paymentResult.setPayResult(payResult);
		paymentResult.setErrorCode(errorCode);
		paymentResult.setReturnDatetime(returnDatetime);

		// signMsg为服务器端返回的签名值。
		paymentResult.setSignMsg(signMsg);

		// signType为"1"时，必须设置证书路径。
		if ("1".equals(signType)) {
			String tlcert_path = jsonObject.getString(TLPayConfig.TLCERT_PATH);
			PhotoPathDto result = PhotoPathResolver.getInstance().calculateFilePath(tlcert_path);
			PaymentLog.log(" tonglian TLCert 证书位置 = {} ", result.getFilePath());
			paymentResult.setCertPath(result.getFilePath());
		}

		// 验证签名：返回true代表验签成功；否则验签失败。
		boolean verifyResult = paymentResult.verify();
		// 验签成功，还需要判断订单状态，为"1"表示支付成功。
		boolean paySuccess = verifyResult && paymentResult.getPayResult().equals("1");
		PaymentLog.log("tonglian pay verifyResult = {} ", verifyResult);
		PaymentLog.log("tonglian pay paySuccess = {} ", paySuccess);
		if (paySuccess) {
			return true;
		}
		return false;
	}

}
