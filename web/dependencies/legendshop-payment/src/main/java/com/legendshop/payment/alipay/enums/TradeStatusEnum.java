package com.legendshop.payment.alipay.enums;


public enum TradeStatusEnum {
	
	/**
	 * 交易完成
	 */
	TRADE_FINISHED("TRADE_FINISHED"),
	
	/**
	 * 交易成功
	 */
	TRADE_SUCCESS("TRADE_SUCCESS"),
	;

	/** 描述 */
	private String desc;

	private TradeStatusEnum(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * Instance.
	 * 
	 * @param name
	 *            the name
	 * @return true, if successful
	 */
	public static boolean instance(String name) {
		TradeStatusEnum[] licenseEnums = values();
		for (TradeStatusEnum licenseEnum : licenseEnums) {
			if (licenseEnum.getDesc().equals(name)) {
				return true;
			}
		}
		return false;
	}
	
}
