package com.legendshop.payment.weixin.processor;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import com.legendshop.uploader.AttachmentManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.RefundLog;
import com.legendshop.base.util.PhotoPathResolver;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.form.SysSubReturnForm;
import com.legendshop.payment.weixin.util.ClientCustomSSL;
import com.legendshop.payment.weixin.util.RequestHandler;
import com.legendshop.payment.weixin.util.WeiXinPayConfig;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PaymentRefundProcessor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.RandomStringUtils;
import com.legendshop.web.util.WeiXinUtil;

/**
 * 微信小程序退款
 */
@Service("weXinMiniProgramPayRefundProcessor")
public class WeXinMiniProgramRefundProcessorImpl implements PaymentRefundProcessor {

	/** The log. */
	private final static String refundURL = "https://api.mch.weixin.qq.com/secapi/pay/refund";

	@Autowired
	private PayTypeService payTypeService;

	@Autowired
	AttachmentManager attachmentManager;
	/*
	 *  微信退款是有状态机的概念;
	 *   /pay/refund——————》 只是退款申请成功;
	 *   必须通过退款通知来知道退款的状态是否成功,或者通过定期轮询refund query机制
	 *  
	 */
	@Override
	public Map<String, Object> refund(SysSubReturnForm returnForm) {
		
		RefundLog.info("############# 微信退款中心 ########## " );

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", false);
		
		PayType payType = payTypeService.getPayTypeById(PayTypeEnum.WX_MP_PAY.value());
		if (payType == null) {
			map.put("message", "支付方式不存在,请设置支付方式");
			return map;
		}
		if (payType.getIsEnable() == Constants.OFFLINE) {
			map.put("message", "该支付方式没有启用!");
			return map;
		}
		
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		
		Integer isRefund = jsonObject.getInteger("isRefund");
		if (isRefund != 1) {
			map.put("message", "请开启微信支付退款支持");
			return map;
		}
		String refundFile = jsonObject.getString("refundFile");
		if (AppUtils.isBlank(refundFile)) {
			map.put("message", "请上传微信的商户证书");
			return map;
		}
		
		String appid = jsonObject.getString(WeiXinPayConfig.APPID);
		String mch_id = jsonObject.getString(WeiXinPayConfig.MCH_ID);
		String appsecret = jsonObject.getString(WeiXinPayConfig.APPSECRET);
		String token = jsonObject.getString(WeiXinPayConfig.TOKEN);
		String partnerkey = jsonObject.getString(WeiXinPayConfig.PARTNERKEY);
		
		//String out_trade_no = returnForm.getSubSettlementSn();// 支付时传入的商户订单号，与trade_no必填一个
		String trade_no = returnForm.getFlowTradeNo(); // 支付时返回的支付宝交易号，与out_trade_no必填一个
		String out_request_no = returnForm.getOutRequestNo(); // 本次退款请求流水号，部分退款时必传
		
		String refund_fee = String.valueOf(new BigDecimal(returnForm.getReturnAmount()).setScale(2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).intValue());
		String total_fee = String
				.valueOf(new BigDecimal(returnForm.getPayAmount()).setScale(2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).intValue());

		
		// 随机字符串
		String nonce_str = RandomStringUtils.randomNumeric(3, 20); // 随机字符串，不长于32位
		SortedMap<String, String> packageParams = new TreeMap<String, String>();
		packageParams.put("appid", appid);
		packageParams.put("mch_id", mch_id);
		packageParams.put("nonce_str", nonce_str);
		packageParams.put("transaction_id", trade_no);
		packageParams.put("out_refund_no", out_request_no);
		packageParams.put("total_fee", total_fee);
		/** start weibf修改了微信退款金额 */
		packageParams.put("refund_fee", refund_fee);
		/** weibf修改了微信退款金额 end */
		packageParams.put("op_user_id", mch_id);
		RequestHandler reqHandler = new RequestHandler();
		reqHandler.init(token, appid, appsecret, partnerkey);
		
		String sign = reqHandler.createSign(packageParams);
		
		StringBuilder builder = new StringBuilder("<xml>");
		builder.append("<appid>").append(appid).append("</appid>");
		builder.append("<mch_id>").append(mch_id).append("</mch_id>");
		builder.append("<nonce_str>").append(nonce_str).append("</nonce_str>");
		builder.append("<op_user_id>").append(mch_id).append("</op_user_id>");
		builder.append("<out_refund_no>").append(out_request_no).append("</out_refund_no>");
		/** start weibf修改了微信退款金额 */
		builder.append("<refund_fee>").append(refund_fee).append("</refund_fee>");
		/** weibf修改了微信退款金额 end */
		builder.append("<total_fee>").append(total_fee).append("</total_fee>");
		builder.append("<transaction_id>").append(trade_no).append("</transaction_id>");
		builder.append("<sign>").append(sign).append("</sign>");
		builder.append("</xml>");
		
		String payReturnResult = null;
		
		try {
			RefundLog.info("微信退款发送请求 ={} ", builder.toString());
//			String refundPath = PhotoPathResolver.getInstance().calculateFilePath(refundFile).getFilePath();
			String refundPath = attachmentManager.getPrivateObjectUrl(refundFile);
			RefundLog.info("微信 refundPath ={} ", refundPath);

			payReturnResult = ClientCustomSSL.doRefund(refundURL, builder.toString(), mch_id, refundPath);
			RefundLog.info("微信 ClientCustomSSL  doRefund payReturnResult ={} ", payReturnResult);
			if (Constants.FAIL.equals(payReturnResult)) {
				map.put("message", payReturnResult);
				return map;
			}
			Map<String, String> payReturnMap = WeiXinUtil.parseXml(payReturnResult);
			String return_code = payReturnMap.get("return_code"); // 返回状态码
			RefundLog.info(" 微信退款 处理结果 payReturnMap={} ", JSONUtil.getJson(payReturnMap));
			if (!"SUCCESS".equals(return_code)) {
				String return_msg = payReturnMap.get("return_msg");
				RefundLog.info(" 微信退款失败 return_msg={} ",return_msg);
				map.put("message", return_msg);
				return map;
			} 
			
			String result_code=payReturnMap.get("result_code");
			if (!"SUCCESS".equals(result_code)) {
				String err_code = payReturnMap.get("err_code");
				String err_code_des = payReturnMap.get("err_code_des");
				RefundLog.info(" 微信退款失败 err_code={},err_code_des={} ",err_code,err_code_des);
				map.put("message", err_code_des);
				return map;
			} 
			
			map.put("result", true);
			map.put("message", "微信退款成功");
			
			return map;
		} catch (Exception e) {
			RefundLog.error(" 微信 ClientCustomSSL doRefund payReturnResult ={}   ", e);
			map.put("message", "微信退款失败");
			return map;
		}
	}

	/**
	 * 查询订单, 暂时不实现
	 * https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=9_2
	 */
/*	@Override
	public Map<String, Object> refundQuery(SysSubReturnForm returnForm) {
		
		
		RefundLog.info("############# 微信退款中心 ########## " );

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", -1);
		
		PayType payType = null;
		//如果卖家中心来源 或者是开启则使用平台代理模式
		if( returnForm.getIsAgentPay() ){
			 payType = payTypeDao.getPayTypeById(PayTypeEnum.ALI_PAY.value(), 0l); //这种方式是卖家中心调用支付服务
			 RefundLog.info("############# 平台代理支付模式  {} ########## ",JSONUtil.getJson(payType));
		}
		else{
			payType = payTypeDao.getPayTypeById(PayTypeEnum.ALI_PAY.value(), returnForm.getShopId());
			RefundLog.info("############# 商家支付模式  {} ########## ",JSONUtil.getJson(payType));
		}
		
		if (payType == null) {
			map.put("message", "支付方式不存在,请设置支付方式");
			return map;
		} else if (payType.getIsEnable() == Constants.OFFLINE) {
			map.put("message", "该支付方式没有启用!");
			return map;
		}
		
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		String appid = jsonObject.getString(WeiXinPayConfig.APPID);
		String mch_id = jsonObject.getString(WeiXinPayConfig.MCH_ID);
		String appsecret = jsonObject.getString(WeiXinPayConfig.APPSECRET);
		String token = jsonObject.getString(WeiXinPayConfig.TOKEN);
		String partnerkey = jsonObject.getString(WeiXinPayConfig.PARTNERKEY);
		
		String out_trade_no = returnForm.getSubSettlementSn();// 支付时传入的商户订单号，与trade_no必填一个
		String trade_no = returnForm.getFlowTradeNo(); // 支付时返回的支付宝交易号，与out_trade_no必填一个
		String out_request_no = returnForm.getOutRequestNo(); // 本次退款请求流水号，部分退款时必传
		
		// 随机字符串
		String nonce_str = RandomStringUtils.randomNumeric(3, 20); // 随机字符串，不长于32位
		SortedMap<String, String> parameters = new TreeMap<String, String>();
		parameters.put("appid", appid);
		parameters.put("mch_id", mch_id);
		parameters.put("nonce_str", nonce_str);
		// 下列四个单号同时存在优先级refund_id>out_refund_no>transaction_id>out_trade_no
		parameters.put("transaction_id", trade_no);// 微信交易单号
		parameters.put("out_trade_no", out_trade_no);// 商户交易单号
		parameters.put("out_refund_no", out_request_no);// 商户退款单号
	    
		RequestHandler reqHandler = new RequestHandler();
		reqHandler.init(token, appid, appsecret, partnerkey);
		String sign = reqHandler.createSign(parameters);
		
		StringBuilder builder = new StringBuilder("<xml>");
		builder.append("<appid>").append(appid).append("</appid>");
		builder.append("<mch_id>").append(mch_id).append("</mch_id>");
		builder.append("<nonce_str>").append(nonce_str).append("</nonce_str>");
		builder.append("<out_refund_no>").append(out_request_no).append("</out_refund_no>");
		builder.append("<out_trade_no>").append(out_trade_no).append("</out_trade_no>");
		builder.append("<refund_id></refund_id>");
		builder.append("<transaction_id>").append(trade_no).append("</transaction_id>");
		builder.append("<sign>").append(sign).append("</sign>");
		builder.append("</xml>");
		
		Map<String, String> form  = WeiXinUtil.httpPostXML(WeiXinPayConfig.REFUND_QUERY_API,builder.toString() );
		String return_code = form.get("return_code"); // 返回状态码
		String result_code= form.get("result_code"); 
		if("FAIL".equals(return_code)){
			map.put("message", form.get("return_msg"));
			return map;
		}
		if("FAIL".equals(result_code)){
			map.put("message", form.get("err_code_des"));
			return map;
		}
		
		RefundLog.info("############# 退款查询参数返回 params= {}  ########## ",JSONUtil.getJson(form));
		RefundLog.info("############# 退款渠道 {}  ########## ",form.get("refund_channel_$n"));
		RefundLog.info("############# 申请退款金额 {}  ########## ",form.get("refund_fee_$n"));
		RefundLog.info("############# 退款金额 {}  ########## ",form.get("settlement_refund_fee_$n"));
		RefundLog.info("############# 退款状态 {}  ########## ",form.get("refund_status_$n"));
		RefundLog.info("############# 退款资金来源 {}  ########## ",form.get("refund_account_$n"));
		RefundLog.info("############# 退款入账账户 {}  ########## ",form.get("refund_recv_accout_$n"));
		RefundLog.info("############# 退款成功时间 {}  ########## ",form.get("refund_success_time$n"));
		
		String refund_status_=form.get("refund_status_$n");
		if("SUCCESS".equals(refund_status_)){
			map.put("result", 1);
			map.put("message", "Success");
			return map;
		}else if("REFUNDCLOSE".equals(refund_status_)){
			RefundLog.info("#############  REFUNDCLOSE—退款关闭   ########## "); 
			map.put("result", -2);
			map.put("message", "REFUNDCLOSE—退款关闭");
		}else if("PROCESSING".equals(refund_status_)){
			map.put("result", 0);
			map.put("message", "PROCESSING—退款处理中");
			RefundLog.info("############# PROCESSING—退款处理中  ########## "); 
		}else if("CHANGE".equals(refund_status_)){
			map.put("message", "CHANGE—退款异常");
			RefundLog.info("############# 退款异常，退款到银行发现用户的卡作废或者冻结了，导致原路退款银行卡失败，可前往商户平台（pay.weixin.qq.com）-交易中心，手动处理此笔退款。$n为下标，从0开始编号。  ########## " );
			map.put("result", -2);
		}
		return map;
	}*/

}
