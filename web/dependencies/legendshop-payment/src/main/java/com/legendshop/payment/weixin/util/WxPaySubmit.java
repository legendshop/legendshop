/*
 * 
 * 
 * 
 *  
 * 
 */
package com.legendshop.payment.weixin.util;

/**
 * The Class WxPaySubmit.
 */
public class WxPaySubmit {

	public static String buildForm(String requestUrl) {
		// 待请求参数数组
		StringBuffer sbHtml = new StringBuffer();
		sbHtml.append("<script type='text/javascript'>");
		sbHtml.append("window.location.href='" + requestUrl+"'");
		sbHtml.append("</script>");
		return sbHtml.toString();
	}

}
