package com.legendshop.payment.util;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.payment.unionPay.util.AcpService;

/**
 * 在线银联支付的验证回调
 * @author Tony哥
 * 
 */
public class UnionPayVerifyStrategyImpl implements PayVerifyStrategy {

	@Override
	public boolean verifyNotify(JSONObject jsonObject, Map<String, String> notifyMap) {

		Map<String, String> valideData = null;
		if (null != notifyMap && !notifyMap.isEmpty()) {
			Iterator<Entry<String, String>> it = notifyMap.entrySet().iterator();
			valideData = new HashMap<String, String>(notifyMap.size());
			while (it.hasNext()) {
				Entry<String, String> e = it.next();
				String key = (String) e.getKey();
				String value = (String) e.getValue();
				try {
					value = new String(value.getBytes("UTF-8"),"UTF-8");
				} catch (UnsupportedEncodingException e1) {
					e1.printStackTrace();
				}
				valideData.put(key, value);
			}
		}
		
		boolean verify_result=AcpService.validate(valideData, "UTF-8");
		PaymentLog.info("在线银联支付的验证回调数据校验 verify_result:{}" ,verify_result);
		return verify_result;
	}
	
	
}
