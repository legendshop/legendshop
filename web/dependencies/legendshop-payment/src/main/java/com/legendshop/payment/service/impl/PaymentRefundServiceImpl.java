package com.legendshop.payment.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.legendshop.model.form.SysSubReturnForm;
import com.legendshop.spi.service.PaymentRefundProcessor;
import com.legendshop.spi.service.PaymentRefundService;

/**
 * 
 * 支付退单
 *
 */
@Service("paymentRefundService")
public class PaymentRefundServiceImpl implements PaymentRefundService{

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(PaymentServiceImpl.class);

	/**支付实现类 */
	@Resource(name="paymentRefundProcessor")
	private Map<String, PaymentRefundProcessor> processorMap;
	
	@Override
	public Map<String, Object> refund(SysSubReturnForm returnForm) {
		PaymentRefundProcessor processor=processorMap.get(returnForm.getRefundTypeId());
		if(processor==null){
			log.warn("PaymentProcessor processor is null by RefundTypeId = {}  ", returnForm.getRefundTypeId() );
			return null;
		}
		return processor.refund(returnForm);
	}
	
	public void setProcessorMap(Map<String, PaymentRefundProcessor> processorMap) {
		this.processorMap = processorMap;
	}

}
