package com.legendshop.payment.util;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;

/**
 * 
 * @author Tony
 *
 */
public interface PayVerifyStrategy {

	/**
	 * 回调通知验证签名
	 * @param paymentConfig 支付配置参数
	 * @param notifyMap 支付回调的信息
	 * @return
	 */
    public boolean verifyNotify(JSONObject jsonObject ,Map<String, String> notifyMap);
    
	
}
