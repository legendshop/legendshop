package com.legendshop.payment.tl.precessor;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.util.HttpURLConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.allinpay.ets.client.util.MySecureProtocolSocketFactory;
import com.legendshop.base.log.RefundLog;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.form.SysSubReturnForm;
import com.legendshop.payment.tl.util.TLPayConfig;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PaymentRefundProcessor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;


/**
 * 通联退款 
 * @author Tony
 *
 */
@Service("tongLianPayRefundProcessor")
public class TongLianPayRefundProcessorImpl implements PaymentRefundProcessor {
	
	@Autowired
	private PayTypeService payTypeService;
	
	@Override
	public Map<String, Object> refund(SysSubReturnForm returnForm) {
		
	    RefundLog.info("############# 通联退款中心 ########## " );
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", false);
		PayType payType = payTypeService.getPayTypeById(PayTypeEnum.TONGLIAN_PAY.value());
		RefundLog.info("############# 退款支付参数  {} ########## ",JSONUtil.getJson(payType));
		if (payType == null || AppUtils.isBlank(payType.getPaymentConfig())) {
			map.put("message", "支付方式不存在,请设置支付方式");
			return map;
		}
		
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());
		
		String out_trade_no=returnForm.getSubSettlementSn() ;// 传入的商户订单号
		String trade_no= returnForm.getFlowTradeNo();	//   交易号，与out_trade_no
		String out_request_no=returnForm.getOutRequestNo() ;	//本次退款请求流水号，部分退款时必传
		Double refund_amount=returnForm.getReturnAmount();	// 本次退款金额
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String orderDatetime = dateFormat.format(returnForm.getOrderDateTime());
		
		com.allinpay.ets.client.RequestOrder requestOrder = new com.allinpay.ets.client.RequestOrder();
		requestOrder.setVersion("v1.3");
		requestOrder.setSignType(0);
		requestOrder.setMerchantId(jsonObject.getString(TLPayConfig.MERCHANTID));
		requestOrder.setOrderNo(out_trade_no);
		requestOrder.setOrderDatetime(orderDatetime);
		requestOrder.setRefundAmount(new BigDecimal(returnForm.getReturnAmount()).setScale(2,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).longValue());
		requestOrder.setKey(jsonObject.getString(TLPayConfig.key));
		String strSrcMsg = requestOrder.getSrc(); // 此方法用于debug，测试通过后可注释。
		String strSignMsg = requestOrder.doSign(); // 签名，设为signMsg字段值。
		
		RefundLog.info(" 通联支付 strSrcMsg={} ", strSrcMsg);
		System.out.println(strSignMsg);
		
		PostMethod postMethod = null;
		Map Namemap = new HashMap();
		
		try {
			Protocol myhttps = new Protocol("https",new MySecureProtocolSocketFactory(), 443);
			Protocol.registerProtocol("https", myhttps);
			HttpClient httpclient = new HttpClient();
			postMethod = new PostMethod(TLPayConfig.serverUrl);
 			NameValuePair[] datas = {
					new NameValuePair("merchantId",requestOrder.getMerchantId()),
					new NameValuePair("version", requestOrder.getVersion()),
					new NameValuePair("signType", requestOrder.getSignType()+""),
					new NameValuePair("orderNo", requestOrder.getOrderNo()),
					new NameValuePair("mchtRefundOrderNo", requestOrder.getMchtRefundOrderNo()),
					new NameValuePair("orderDatetime", requestOrder.getOrderDatetime()),
					new NameValuePair("refundAmount",requestOrder.getRefundAmount()+""),
					new NameValuePair("signMsg",strSignMsg) };
			postMethod.setRequestBody(datas);
			httpclient.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");
			int responseCode = httpclient.executeMethod(postMethod);
			// 取得查询交易结果
			if (responseCode == HttpURLConnection.HTTP_OK) {
				String trxXML = postMethod.getResponseBodyAsString();
				String[] msg = trxXML.split("&");
				String[] paramPair = null;
				for (int i = 0; i < msg.length; i++) {
					paramPair = msg[i].split("=");
					Namemap.put(paramPair[0], paramPair[1]);
					System.out.println(paramPair[0] + ":" +Namemap.get(paramPair[0]) );
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
			map.put("message", "退款失败");
			return map;
		}
		
		// 验签是商户为了验证接收到的报文数据确实是支付网关发送的。
	    // 构造订单结果对象，验证签名。
		
		RefundLog.info(" 退款结果{} ",JSONUtil.getJson(Namemap));
		com.allinpay.ets.client.PaymentResult paymentResult = new com.allinpay.ets.client.PaymentResult();
		if ("".equals(Namemap.get("ERRORCODE")) || null == Namemap.get("ERRORCODE")) {
			
			// 如果errorCode为空，说明返回正确退款报文信息，接下来对报文进行解析验签
			paymentResult.setMerchantId(Namemap.get("merchantId").toString());
			paymentResult.setVersion(Namemap.get("version").toString());
			paymentResult.setSignType(Namemap.get("signType").toString());
			paymentResult.setOrderNo(Namemap.get("orderNo").toString());
			paymentResult.setOrderDatetime(Namemap.get("orderDatetime").toString());
			paymentResult.setOrderAmount(Namemap.get("orderAmount").toString());
			paymentResult.setErrorCode(null == Namemap.get("errorCode") ? "" : map.get("errorCode").toString());
			paymentResult.setRefundAmount(Namemap.get("refundAmount").toString());
			paymentResult.setRefundDatetime(Namemap.get("refundDatetime").toString());
			paymentResult.setRefundResult(Namemap.get("refundResult").toString());
			paymentResult.setReturnDatetime(Namemap.get("returnDatetime").toString());
			// signMsg为服务器端返回的签名值。
			paymentResult.setSignMsg(Namemap.get("signMsg").toString());
			paymentResult.setKey(TLPayConfig.key);
			// 验证签名：返回true代表验签成功；否则验签失败。
			boolean verifyResult = paymentResult.verify();
			System.out.println(Namemap);
			if (verifyResult) {
				map.put("result", true);
				return map;
			}
		}
		RefundLog.info(" 退款失败：错误代码 {} , ERRORMSG {}  ", Namemap.get("ERRORCODE"),Namemap.get("ERRORMSG"));
		return map;
	}

}
