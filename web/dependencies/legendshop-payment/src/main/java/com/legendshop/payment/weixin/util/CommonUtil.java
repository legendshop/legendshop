package com.legendshop.payment.weixin.util;

import java.net.URLEncoder;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;

public class CommonUtil {

	public static String formatQueryParaMap(SortedMap<String, String> packageParams, boolean urlEncode) throws Exception {
		StringBuffer sb = new StringBuffer();
		Set<Entry<String, String>> set = packageParams.entrySet();
		for (Map.Entry<String, String> mItem : set) {
			String k = (String) mItem.getKey();
			String v = (String) mItem.getValue();
			if ((null != v) && (!"".equals(v)) && (!"sign".equals(k)) && (!"key".equals(k))) {
				if (urlEncode) {
					sb.append(k + "=" + URLEncoder.encode(v, "").replace("+", "%20") + "&");
				} else {
					sb.append(k + "=" + v + "&");
				}
			}

		}
		System.out.println("sb :" + sb.toString() + "/r/t");
		String sbString = sb.toString();
		return sbString.substring(0, sbString.length());
	}

}
