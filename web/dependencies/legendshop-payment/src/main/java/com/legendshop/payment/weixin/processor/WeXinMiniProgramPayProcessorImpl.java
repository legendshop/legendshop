package com.legendshop.payment.weixin.processor;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PassportSourceEnum;
import com.legendshop.model.constant.PassportTypeEnum;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.dto.PassportFull;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.payment.weixin.enums.WeiXinTradeTypeEnum;
import com.legendshop.payment.weixin.util.RequestHandler;
import com.legendshop.payment.weixin.util.WeiXinPayConfig;
import com.legendshop.spi.service.PassportService;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PaymentProcessor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.RandomStringUtils;

/**
 * 微信小程序支付
 * https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=7_3&index=1
 */
@Service("weXinMiniProgramPayProcessor")
public class WeXinMiniProgramPayProcessorImpl implements PaymentProcessor {

	/** The log. */
	private static Logger log = LoggerFactory.getLogger(WeXinMiniProgramPayProcessorImpl.class);

	@Autowired
	private PayTypeService payTypeService;
	
	@Autowired
	private PassportService passportService;

	@Override
	public Map<String, Object> payto(SysPaymentForm paymentForm) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", false);

		PayType payType = payTypeService.getPayTypeById(PayTypeEnum.WX_MP_PAY.value());

		if (payType == null) {
			map.put("message", "支付方式不存在,请设置支付方式");
			return map;
		}
		
		if (payType.getIsEnable() == Constants.OFFLINE) {
			map.put("message", "该支付方式没有启用!");
			return map;
		}

		paymentForm.setPayTypeId(PayTypeEnum.WX_MP_PAY.value());
		paymentForm.setPayTypeName(PayTypeEnum.WX_MP_PAY.desc());

		JSONObject jsonObject = JSONObject.parseObject(payType.getPaymentConfig());

		/** 微信分配的小程序ID */
		String appid = jsonObject.getString(WeiXinPayConfig.APPID);

		/** 微信公众号绑定的商户号 */
		String mch_id = jsonObject.getString(WeiXinPayConfig.MCH_ID);

		String appSecret = jsonObject.getString(WeiXinPayConfig.APPSECRET);

		String partnerkey = jsonObject.getString(WeiXinPayConfig.PARTNERKEY);

		if (AppUtils.isBlank(appid) || AppUtils.isBlank(mch_id) || AppUtils.isBlank(partnerkey)) {
			map.put("message", "请设置支付的密钥信息!");
			return map;
		}

         String subject = paymentForm.getSubject().replaceAll(" ", "").replaceAll("/", "");// 注意标题一定去空格
		
		if(subject.length()>20){
			subject=subject.substring(0, 20);
			subject+="...";
		}

		String totalPrice = String.valueOf(new BigDecimal(paymentForm.getTotalAmount())
				.setScale(2, BigDecimal.ROUND_HALF_UP)
				.multiply(new BigDecimal(100)).intValue());

		SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();

		/** 公众号APPID */
		parameters.put("appid", appid);
		/** 商户号 */
		parameters.put("mch_id", mch_id);
		/** 随机字符串 */
		parameters.put("nonce_str", RandomStringUtils.randomNumeric(8));// 随机字符串
		/** 商品名称 */
		parameters.put("body", subject);
		/** 订单号 */
		parameters.put("out_trade_no", paymentForm.getSubSettlementSn());
		/** 订单金额以分为单位，只能为整数 */
		parameters.put("total_fee", totalPrice);
		/** 客户端本地ip */
		parameters.put("spbill_create_ip", paymentForm.getIp());
		
		/** The notify_url 交易过程中服务器通知的页面 要用 http://格式的完整路径，不允许加?id=123这类自定义参数. */
		String notifyUrl = paymentForm.getDomainName()+ "/payNotify/notify/"+ PayTypeEnum.WX_MP_PAY.value();

		parameters.put("notify_url", notifyUrl);

		/** 支付方式为APP支付 */
		parameters.put("trade_type", WeiXinTradeTypeEnum.JSAPI.name());

		/** 用户微信的openid，当trade_type为JSAPI的时候，该属性字段必须设置 */
		parameters.put("openid", paymentForm.getOpenId());

		RequestHandler reqHandler = new RequestHandler();
		reqHandler.init(null, appid, appSecret, partnerkey);

		// 计算签名
		String sign = reqHandler.createSign(parameters);
		parameters.put("sign", sign);

		Map<String, String> reqHandlerMap = reqHandler.getPrepayId(parameters);
		if (AppUtils.isBlank(reqHandlerMap)) {
			map.put("message", "微信统一下单集成失败！");
			return map;
		}

		String return_code = reqHandlerMap.get("return_code"); // 返回状态码
		String result_code = reqHandlerMap.get("result_code");
		if ("FAIL".equals(return_code)) {
			map.put("message", reqHandlerMap.get("return_msg"));
			return map;
		}

		if ("FAIL".equals(result_code)) {
			map.put("message", reqHandlerMap.get("err_code_des"));
			return map;
		}

		// 参数
		SortedMap<Object, Object> paraMap = new TreeMap<Object, Object>();
		// 参数
		String timeStamp = System.currentTimeMillis() + "";

		paraMap.put("appId", appid);
		paraMap.put("timeStamp", timeStamp);
		paraMap.put("nonceStr", RandomStringUtils.randomNumeric(8));
		/**
		 * 获取预支付单号prepay_id后，需要将它参与签名。 微信支付最新接口中，要求package的值的固定格式为prepay_id=...
		 */
		String packageValue = "prepay_id=" + reqHandlerMap.get("prepay_id");
		paraMap.put("package", packageValue);

		/** 微信支付新版本签名算法使用MD5，不是SHA1 */
		paraMap.put("signType", "MD5");

		/**
		 * 获取预支付prepay_id之后，需要再次进行签名，参与签名的参数有：appId、timeStamp、nonceStr、package、
		 * signType. 主意上面参数名称的大小写. 该签名用于前端js中WeixinJSBridge.invoke中的paySign的参数值
		 */
		// 要签名
		String paySign = reqHandler.createSign(paraMap);

		// 参数
		SortedMap<Object, Object> sortedMap = new TreeMap<Object, Object>();
		sortedMap.put("timeStamp", paraMap.get("timeStamp")); //时间戳从1970年1月1日00:00:00至今的秒数,即当前的时间
		sortedMap.put("nonceStr", paraMap.get("nonceStr")); // 微信返回的随机字符串
		sortedMap.put("package",  paraMap.get("package"));  //统一下单接口返回的 prepay_id 参数值，提交格式如：prepay_id=*
		sortedMap.put("signType", paraMap.get("signType")); // 签名类型，默认为MD5，
		sortedMap.put("paySign", paySign); // 微信返回的签名

		map.put("result", true);
		map.put("message", JSONUtil.getJson(sortedMap));
		return map;
	}
}
