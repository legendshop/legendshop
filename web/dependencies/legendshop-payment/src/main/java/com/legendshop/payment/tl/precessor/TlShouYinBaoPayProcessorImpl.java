package com.legendshop.payment.tl.precessor;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.base.log.PaymentLog;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.payment.tl.util.HttpConnectionUtil;
import com.legendshop.payment.tl.util.SybUtil;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PaymentProcessor;
import com.legendshop.util.JSONUtil;

/**
 * 网上收银统一下单接口报文规范V1.1
 *  系统提供商户通过系统对接的方式发起移动支付交易，包括微信,支付宝,手机QQ的NATIVE支付，JSAPI支付和刷卡支付。
 */

@Service("tlShouYinBaoPayProcessor")
public class TlShouYinBaoPayProcessorImpl implements PaymentProcessor {
	
	@Autowired
	private PayTypeService payTypeService;

	// public static final String serverUrl =
	// "https://vsp.allinpay.com/apiweb/unitorder/pay";//生产环境

	private static final String serverUrl = "http://113.108.182.3:10080/apiweb/unitorder/pay"; // 测试

	@Override
	public Map<String, Object> payto(SysPaymentForm paymentForm) {

		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("result", false);

		/*PayType payType = payTypeService.getPayTypeById(PayTypeEnum.TONGLIAN_WAP_PAY.value());
		if (payType == null || AppUtils.isBlank(payType.getPaymentConfig())) {
			resultMap.put("message", "支付方式不存在,请设置支付方式");
			return resultMap;
		} else if (payType.getIsEnable() == Constants.OFFLINE) {
			resultMap.put("message", "该支付方式没有启用!");
			return resultMap;
		}
		paymentForm.setPayTypeId(PayTypeEnum.TONGLIAN_WAP_PAY.value());
		paymentForm.setPayTypeName(PayTypeEnum.TONGLIAN_WAP_PAY.desc());*/

		//JSONObject jsonObject = JSONObject.parseObject(payType.getPaymentConfig());
		String showUrl = paymentForm.getShowUrl();
		String notifyUrl = paymentForm.getDomainName() + "/payNotify/notify/" + PayTypeEnum.TONGLIAN_WAP_PAY.value();
		String returnUrl = paymentForm.getDomainName() + "/payNotify/response/" + PayTypeEnum.TONGLIAN_WAP_PAY.value();
		
		String out_trade_no = paymentForm.getSubSettlementSn();
		String total_fee = String.valueOf(paymentForm.getTotalAmount());
		
		String trxamt=String.valueOf(new BigDecimal(total_fee).setScale(2,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).intValue());
		HttpConnectionUtil http = new HttpConnectionUtil(serverUrl);
		try {
			http.init();
		} catch (Exception e) {
			e.printStackTrace();
		}
		TreeMap<String,String> params = new TreeMap<String,String>();
		params.put("cusid","990440153996000"); //商户号
		params.put("appid", "00000000"); //应用ID
		params.put("version", "11");
		params.put("trxamt", String.valueOf(trxamt));
		params.put("reqsn", out_trade_no); //商户交易单号
		params.put("paytype", "A01"); 
		params.put("randomstr", SybUtil.getValidatecode(8));
		params.put("body", paymentForm.getSubject()); //订单商品名称，为空则以商户名作为商品名称
		params.put("remark", ""); //备注信息
		params.put("notify_url", notifyUrl);
		
		try {
			String sign = SybUtil.sign(params,"43df939f1e7f5c6909b3f4b63f893994");
			params.put("sign",sign);
			byte[] bys = http.postParams(params, true);
			String result = new String(bys,"UTF-8");
			//响应参数：
			PaymentLog.info(" TlShouYinBaoPayProcessorImpl 响应回调参数 result ={} ",result);
			
			@SuppressWarnings("unchecked")
			Map<String,String> map =JSONUtil.getMap(result);
			
			if("SUCCESS".equals(map.get("retcode"))){
				TreeMap<String,String> tmap = new TreeMap<String,String>();
				tmap.putAll(map);
				
				String _sign = tmap.remove("sign").toString();
				String sign1 = SybUtil.sign(tmap,"43df939f1e7f5c6909b3f4b63f893994");
				
				if(_sign.toLowerCase().equals(sign1.toLowerCase())){
					System.out.println("chnltrxid==>"+map.get("chnltrxid"));
					System.out.println("fintime==>"+map.get("fintime"));
					System.out.println("payinfo==>"+map.get("payinfo"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

}
