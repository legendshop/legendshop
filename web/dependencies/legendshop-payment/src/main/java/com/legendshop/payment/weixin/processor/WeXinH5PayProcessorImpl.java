package com.legendshop.payment.weixin.processor;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.payment.weixin.enums.WeiXinTradeTypeEnum;
import com.legendshop.payment.weixin.util.RequestHandler;
import com.legendshop.payment.weixin.util.WeiXinPayConfig;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PaymentProcessor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * 微信H5支付
 * https://pay.weixin.qq.com/wiki/doc/api/H5.php?chapter=15_4
 * 
 *
 */
@Service("weXinH5PayProcessor")
public class WeXinH5PayProcessorImpl implements PaymentProcessor {

	@Autowired
	private PayTypeService payTypeService;

	@Override
	public Map<String, Object> payto(SysPaymentForm paymentForm) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", false);
		
		PayType payType = payTypeService.getPayTypeById(PayTypeEnum.WX_PAY.value()); //这种方式是卖家中心调用支付服务
		if (payType == null || AppUtils.isBlank(payType.getPaymentConfig())) {
			map.put("message", "支付方式不存在,请设置支付方式");
			return map;
		} else if (payType.getIsEnable() == Constants.OFFLINE) {
			map.put("message", "该支付方式没有启用!");
			return map;
		}
		paymentForm.setPayTypeId(PayTypeEnum.WX_PAY.value());
		paymentForm.setPayTypeName(PayTypeEnum.WX_PAY.desc());
		
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());

		/** 微信公众号APPID */
		String appid = jsonObject.getString(WeiXinPayConfig.APPID);
		
		/** 微信公众号APPID */
		String token = jsonObject.getString(WeiXinPayConfig.TOKEN);
		/** 微信公众号绑定的商户号 */
		String mch_id = jsonObject.getString(WeiXinPayConfig.MCH_ID);
		
		String appSecret=jsonObject.getString(WeiXinPayConfig.APPSECRET);
		
		String partnerkey = jsonObject.getString(WeiXinPayConfig.PARTNERKEY);

		if (AppUtils.isBlank(appid) || AppUtils.isBlank(mch_id) || AppUtils.isBlank(partnerkey)) {
			map.put("message", "请设置支付的密钥信息!");
			return map;
		}

		String subject = paymentForm.getSubject().replace(" ", "");// 注意标题一定去空格
		String totalPrice = String.valueOf(BigDecimal.valueOf(paymentForm.getTotalAmount()).setScale(2, BigDecimal.ROUND_HALF_UP)
				.multiply(new BigDecimal(100)).intValue());

		SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();

		/** 公众号APPID */
		parameters.put("appid", appid);
		/** 商户号 */
		parameters.put("mch_id", mch_id);
		/** 随机字符串 */
		parameters.put("nonce_str", RandomStringUtils.randomNumeric(8));// 随机字符串
		/** 商品名称 */
		parameters.put("body", subject);
		/** 订单号 */
		parameters.put("out_trade_no", paymentForm.getSubSettlementSn());
		/** 订单金额以分为单位，只能为整数 */
		parameters.put("total_fee", totalPrice);
		/** 客户端本地ip */
		parameters.put("spbill_create_ip", paymentForm.getIp());

		String notifyUrl = paymentForm.getDomainName()  + "/payNotify/notify/"+ PayTypeEnum.WX_PAY.value();

		parameters.put("notify_url", notifyUrl);

		/** 支付方式为APP支付 */
		parameters.put("trade_type", WeiXinTradeTypeEnum.MWEB.name());

		/** 用户微信的openid，当trade_type为JSAPI的时候，该属性字段必须设置 */
		//parameters.put("openid", paymentForm.getOpenId());
		
		
		StringBuilder scene_info = new StringBuilder();
		scene_info.append("{\"h5_info\": {\"type\":\"Wap\",\"wap_url\": \""+ paymentForm.getWapDomain() +"\",\"wap_name\": \"订单支付\"}}");
		
		parameters.put("scene_info", scene_info.toString());
		
		RequestHandler reqHandler = new RequestHandler();
		reqHandler.init(token, appid, appSecret, partnerkey);

		// 计算签名
		String sign = reqHandler.createSign(parameters);
		parameters.put("sign", sign);
		
		System.out.println(JSONUtil.getJson(parameters));

		Map<String, String> reqHandlerMap = reqHandler.getPrepayId(parameters);
		if (AppUtils.isBlank(reqHandlerMap)) {
			map.put("message", "微信统一下单集成失败！");
			return map;
		}

		String return_code = reqHandlerMap.get("return_code"); // 返回状态码
		String result_code= reqHandlerMap.get("result_code"); 
		if("FAIL".equals(return_code)){
			map.put("message", reqHandlerMap.get("return_msg"));
			return map;
		}
		
		if("FAIL".equals(result_code)){
			map.put("message", reqHandlerMap.get("err_code_des"));
			return map;
		}
		
		System.out.println(JSONUtil.getJson(reqHandlerMap));
		String mweb_url= reqHandlerMap.get("mweb_url");
		
		if(AppUtils.isNotBlank(paymentForm.getRedirectUrl())){
			try {
				mweb_url=mweb_url+"&redirect_url="+URLEncoder.encode(paymentForm.getRedirectUrl() ,"utf-8") ;
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		
		StringBuffer sbHtml = new StringBuffer();
		sbHtml.append("<form id='payForm' action= '").append(mweb_url).append("' method='POST'> ");
		sbHtml.append("</form>");
		sbHtml.append("<script>document.forms['payForm'].submit();</script>");
		/*sbHtml.append("<script>window.location.href='"+mweb_url+"';</script>");*///以前是这样的, 现在变为表单
		
		System.out.println(sbHtml.toString());
		map.put("message",sbHtml.toString());
		map.put("result", true);
		return map;
	}
	

}
