package com.legendshop.payment.weixin.processor;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.payment.weixin.enums.WeiXinTradeTypeEnum;
import com.legendshop.payment.weixin.util.RequestHandler;
import com.legendshop.payment.weixin.util.WeiXinPayConfig;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PaymentProcessor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.RandomStringUtils;

/**
 * 微信内置浏览器公众号支付 JSAPI方式  ----------请勿删除
 * @author Tony
 *
 */
@Deprecated
public class WeXinWapPayProcessorImpl_OLD implements PaymentProcessor {

	private PayTypeService payTypeService;

	@Override
	public Map<String, Object> payto(SysPaymentForm paymentForm) {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", false);
		
		PayType payType = payTypeService.getPayTypeById(PayTypeEnum.WX_APP_PAY.value()); //这种方式是卖家中心调用支付服务
		if (payType == null || AppUtils.isBlank(payType.getPaymentConfig())) {
			map.put("message", "支付方式不存在,请设置支付方式");
			return map;
		} else if (payType.getIsEnable() == Constants.OFFLINE) {
			map.put("message", "该支付方式没有启用!");
			return map;
		}
		paymentForm.setPayTypeId(PayTypeEnum.WX_PAY.value());
		paymentForm.setPayTypeName(PayTypeEnum.WX_PAY.desc());
		
		JSONObject jsonObject=JSONObject.parseObject(payType.getPaymentConfig());

		/** 微信公众号APPID */
		String appid = jsonObject.getString(WeiXinPayConfig.APPID);
		
		/** 微信公众号APPID */
		String token = jsonObject.getString(WeiXinPayConfig.TOKEN);
		/** 微信公众号绑定的商户号 */
		String mch_id = jsonObject.getString(WeiXinPayConfig.MCH_ID);
		
		String appSecret=jsonObject.getString(WeiXinPayConfig.APPSECRET);
		
		String partnerkey = jsonObject.getString(WeiXinPayConfig.PARTNERKEY);

		if (AppUtils.isBlank(appid) || AppUtils.isBlank(mch_id) || AppUtils.isBlank(partnerkey)) {
			map.put("message", "请设置支付的密钥信息!");
			return map;
		}

		String subject = paymentForm.getSubject().replace(" ", "");// 注意标题一定去空格
		String totalPrice = String.valueOf(new BigDecimal(paymentForm.getTotalAmount()).setScale(2, BigDecimal.ROUND_HALF_UP)
				.multiply(new BigDecimal(100)).intValue());

		SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();

		/** 公众号APPID */
		parameters.put("appid", appid);
		/** 商户号 */
		parameters.put("mch_id", mch_id);
		/** 随机字符串 */
		parameters.put("nonce_str", RandomStringUtils.randomNumeric(8));// 随机字符串
		/** 商品名称 */
		parameters.put("body", subject);
		/** 订单号 */
		parameters.put("out_trade_no", paymentForm.getSubSettlementSn());
		/** 订单金额以分为单位，只能为整数 */
		parameters.put("total_fee", totalPrice);
		/** 客户端本地ip */
		parameters.put("spbill_create_ip", paymentForm.getIp());

		String notifyUrl = paymentForm.getDomainName() + "/payNotify/notify/"+ PayTypeEnum.WX_PAY.value();

		parameters.put("notify_url", notifyUrl);

		/** 支付方式为APP支付 */
		parameters.put("trade_type", WeiXinTradeTypeEnum.JSAPI.name());

		/** 用户微信的openid，当trade_type为JSAPI的时候，该属性字段必须设置 */
		parameters.put("openid", paymentForm.getOpenId());
		parameters.put("attach", paymentForm.getSubSettlementSn());
		
		RequestHandler reqHandler = new RequestHandler();
		reqHandler.init(token, appid, appSecret, partnerkey);

		// 计算签名
		String sign = reqHandler.createSign(parameters);
		parameters.put("sign", sign);

		Map<String, String> reqHandlerMap = reqHandler.getPrepayId(parameters);
		PaymentLog.info("微信统一下单集成返回结果！ reqHandlerMap={} ",JSONArray.toJSONString(reqHandlerMap));
		if (AppUtils.isBlank(reqHandlerMap)) {
			map.put("message", "微信统一下单集成失败！");
			return map;
		}

		String return_code = reqHandlerMap.get("return_code"); // 返回状态码
																// SUCCESS/FAIL
		if ("SUCCESS".equals(return_code)) {
			// 参数
			SortedMap<Object, Object> paraMap = new TreeMap<Object, Object>();
			// 参数
			String timeStamp = System.currentTimeMillis() + "";

			paraMap.put("appId",appid);
			paraMap.put("timeStamp", timeStamp);
			paraMap.put("nonceStr", RandomStringUtils.randomNumeric(8));
			/**
			 * 获取预支付单号prepay_id后，需要将它参与签名。 微信支付最新接口中，要求package的值的固定格式为prepay_id=...
			 */
			String packageValue="prepay_id=" + reqHandlerMap.get("prepay_id");
			paraMap.put("package",packageValue);
			
			/** 微信支付新版本签名算法使用MD5，不是SHA1 */
			paraMap.put("signType", "MD5");
			
			/**
			 * 获取预支付prepay_id之后，需要再次进行签名，参与签名的参数有：appId、timeStamp、nonceStr、package、
			 * signType. 主意上面参数名称的大小写. 该签名用于前端js中WeixinJSBridge.invoke中的paySign的参数值
			 */
			// 要签名
			String paySign = reqHandler.createSign(paraMap);
			paraMap.put("paySign", paySign);
			paraMap.put("packageValue", packageValue);
	
			System.out.println(JSONUtil.getJson(paraMap));
			
			map.put("result", true);
			map.put("message", JSONUtil.getJson(paraMap));
			return map;
		}else{
			map.put("message", reqHandlerMap.get("return_msg"));
		}
		return map;
	}
	
	

	public void setPayTypeService(PayTypeService payTypeService) {
		this.payTypeService = payTypeService;
	}

}
