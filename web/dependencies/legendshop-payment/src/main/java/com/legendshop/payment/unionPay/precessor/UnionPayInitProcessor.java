package com.legendshop.payment.unionPay.precessor;

import javax.servlet.ServletContextEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.legendshop.base.event.EventProcessor;
import com.legendshop.model.entity.PayType;
import com.legendshop.payment.unionPay.util.CertUtil;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.util.AppUtils;

/**
 * 网银在线的初始化
 *  暂时没用上 TODO
 */
@Service("unionPayInitProcessor")
public class UnionPayInitProcessor implements EventProcessor<ServletContextEvent>{
	private static Logger log = LoggerFactory.getLogger(UnionPayInitProcessor.class);
	
	private PayTypeService payTypeService;
	
	private final static String PAY_TYPE_ID="UNION_PAY";
	
	@Override
	public void process(ServletContextEvent task) {
		PayType payType=payTypeService.getPayTypeByPayTypeId(PAY_TYPE_ID);
		if(AppUtils.isNotBlank(payType) && payType.getIsEnable().intValue()==1 ){
			log.info(" UnionPayInitProcessor start .....");
			CertUtil.init();
			log.info(" UnionPayInitProcessor end .....");
		}
	}

	public void setPayTypeService(PayTypeService payTypeService) {
		this.payTypeService = payTypeService;
	}
	
	

}
