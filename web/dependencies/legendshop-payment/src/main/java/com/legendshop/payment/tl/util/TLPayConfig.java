package com.legendshop.payment.tl.util;

/**
 * 通联支付配置文件
 * @author Tony
 *
 */
public class TLPayConfig {

	public static final String serverUrl="https://service.allinpay.com/gateway/index.do?"; //通联支付正式地址
	
	//public static final String serverUrl="http://ceshi.allinpay.com/gateway/index.do?"; //通联支付测试地址
	
	public static final  String MERCHANTID="merchantId"; //数字串，商户在通联申请开户的商户号     (正式)109020201605045
	
	public static final  String key="key"; //数字串，商户在通联申请开户的商户号     (正式)109020201605045
	
	public static final  String TLCERT_PATH="tlcert_path"; //通联数字证书
	
	
	public static final  String cusid="cusid"; //商户号
	public static final  String appid="appid"; //应用ID
	
	
	
}
