/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.seckill.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.util.SeckillConstant;
import com.legendshop.base.util.SeckillStringUtils;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.page.CommonFrontPage;
import com.legendshop.framework.cache.client.RedisCacheClient;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.dto.seckill.SeckillDto;
import com.legendshop.model.dto.seckill.SeckillExposer;
import com.legendshop.model.dto.seckill.SeckillJsonDto;
import com.legendshop.model.dto.seckill.SeckillResult;
import com.legendshop.model.dto.seckill.SeckillSkuDto;
import com.legendshop.model.dto.seckill.SeckillStateEnum;
import com.legendshop.model.entity.SeckillSuccess;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.seckill.frontendPage.MobileSeckillFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.SeckillService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.util.SeckillCachedhandle;
import com.legendshop.util.AppUtils;
import com.legendshop.util.MD5Util;
import com.legendshop.web.util.ValidateCodeUtil;

/**
 * （手机端）秒杀控制器
 */
@Controller
@RequestMapping("/seckillMobile")
public class SeckillMobileController {
	
	private Logger logger=LoggerFactory.getLogger(SeckillController.class);

	@Autowired
	private SeckillService seckillService;

	/** 秒杀缓存处理 */
	@Autowired
	private SeckillCachedhandle seckillCachedhandle;
	
	@Autowired
	private RedisCacheClient redisCacheClient;
	
	@Autowired
	private ShopDetailService shopDetailService;

	/**
	 * 秒杀商品详情.
	 *
	 * @param request
	 * @param response
	 * @param seckillId
	 * @return the string
	 */
	@RequestMapping("/views/{seckillId}")
	public String seckillDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable Long seckillId) {
		// use cache
		SeckillDto seckill = null;
		if (AppUtils.isBlank(seckill)) {
			seckill = seckillService.getSeckillDetail(seckillId);
			if (seckill == null || seckill.getSecStatus().intValue() == 0 || seckill.getSecStatus().intValue() == -2) { // 下线或者审核中
				return PathResolver.getPath(CommonFrontPage.PROD_NON_EXISTENT);
			}
			seckillCachedhandle.addSeckill(seckill); // 存取缓存
		}
		if (seckill == null || seckill.getSecStatus().intValue() == 0 || seckill.getSecStatus().intValue() == -2) { // 下线或者审核中
			return PathResolver.getPath(CommonFrontPage.PROD_NON_EXISTENT);
		}
		ShopDetail shopDetail = shopDetailService.getShopDetailByIdNoCache(seckill.getShopId());
		if(AppUtils.isBlank(shopDetail) || shopDetail.getStatus() != ShopStatusEnum.NORMAL.value()){
			return PathResolver.getPath(CommonFrontPage.PROD_NON_EXISTENT);
		}
		
		request.setAttribute("seckill", seckill);
		
		SecurityUserDetail user = UserManager.getUser(request);
		request.setAttribute("loginUserName", user.getUsername());
		return PathResolver.getPath(MobileSeckillFrontPage.SECKILL_VIEW);
	}

	/**
	 * 获取时间
	 *
	 * @return the time
	 */
	@RequestMapping(value = "/time/now", method = RequestMethod.GET, produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public SeckillJsonDto<Date> getTime() {
		return new SeckillJsonDto<Date>(true, new Date());
	}

	/**
	 * 秒杀开关
	 *
	 * @param seckillId
	 * @param prodId
	 * @param skuId
	 * @return the json dto< seckill exposer>
	 */
	@RequestMapping(value = "/{seckillId}/exposeUrl")
	@ResponseBody
	// 既然是暴露接口，这个接口是由人去触发的，还是前台自动触发.返回的是秒杀商品列表
	public SeckillJsonDto<SeckillExposer> SckillExpose(@PathVariable("seckillId") long seckillId, @RequestParam long prodId, @RequestParam long skuId) {
		return seckillService.exportSeckillUrl(seckillId, prodId, skuId);
	}

	/**
	 * 执行秒杀
	 *
	 * @param request
	 * @param seckillId
	 * @param md5
	 * @param prodId
	 * @param skuId
	 * @return the json dto< seckill result>
	 */
	@RequestMapping(value = "/{seckillId}/{md5}/execute")
	@ResponseBody
	public SeckillJsonDto<SeckillResult> sckillExecute(HttpServletRequest request, @PathVariable("seckillId") Long seckillId, 
			@PathVariable("md5") String md5, @RequestParam long prodId, @RequestParam long skuId) {

		SeckillJsonDto<SeckillResult> seckillJsonDto = new SeckillJsonDto<SeckillResult>();
		if (AppUtils.isBlank(seckillId) || AppUtils.isBlank(prodId) || AppUtils.isBlank(md5)) {
			seckillJsonDto.setIsSuccess(false);
			seckillJsonDto.setStatus(SeckillStateEnum.URLCHANGED.getState());
			seckillJsonDto.setError(SeckillStateEnum.URLCHANGED.getStateinfo());
			return seckillJsonDto;
		}

		String builderMd5 = SeckillStringUtils.joint(seckillId, prodId, skuId);
		String _md5 = MD5Util.toMD5(builderMd5 + Constants._MD5);
		if (AppUtils.isBlank(md5) || !md5.equals(_md5)) {
			seckillJsonDto.setIsSuccess(false);
			seckillJsonDto.setStatus(SeckillStateEnum.URLCHANGED.getState());
			seckillJsonDto.setError(SeckillStateEnum.URLCHANGED.getStateinfo());
			return seckillJsonDto; // 返回一个关闭的接口
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		if (AppUtils.isBlank(user)) {
			seckillJsonDto.setIsSuccess(false);
			seckillJsonDto.setStatus(SeckillStateEnum.NOLOGIN.getState());
			seckillJsonDto.setError(SeckillStateEnum.NOLOGIN.getStateinfo());
			return seckillJsonDto;
		}
		
		String userId = user.getUserId();
		
		SeckillDto seckill = seckillService.getSeckillDetail(seckillId);
		// 判断从Redis 获取的秒杀商品是否存在
		if (AppUtils.isBlank(seckill)) {
			// 如果该秒杀商品不存在则关闭这个接口
			seckillJsonDto.setIsSuccess(false);
			seckillJsonDto.setStatus(SeckillStateEnum.NOT_FIND.getState());
			seckillJsonDto.setError(SeckillStateEnum.NOT_FIND.getStateinfo());
			return seckillJsonDto; // 返回一个关闭的接口
		}
		
		if(userId.equals(seckill.getUserId())){
			// 如果该秒杀商品不存在则关闭这个接口
			seckillJsonDto.setIsSuccess(false);
			seckillJsonDto.setStatus(SeckillStateEnum.SELF_SECKILL.getState());
			seckillJsonDto.setError(SeckillStateEnum.SELF_SECKILL.getStateinfo());
			return seckillJsonDto; // 返回一个关闭的接口
		}
		
		Date startTime = seckill.getStartTime();
		Date endTime = seckill.getEndTime();
		Date now = new Date();
		if (now.getTime() >= endTime.getTime()) { // 秒杀结束
			seckillJsonDto.setIsSuccess(false);
			seckillJsonDto.setStatus(SeckillStateEnum.TIME_OUT.getState());
			return seckillJsonDto;
		} else if (now.getTime() < startTime.getTime()) { // 距离活动开始时间
			seckillJsonDto.setIsSuccess(false);
			seckillJsonDto.setStatus(SeckillStateEnum.READY.getState());
			return seckillJsonDto;
		}
		/**
		 * 2 限制统一帐号，同一动作，同2秒钟并发次数，超过次不做做动作，返回操作失败
		 */
		String mutex = SeckillStringUtils.cacheKeyJoint(SeckillConstant.MUTEX_KEY_PREFIX, seckillId, userId);
		String mutexresult=redisCacheClient.get(mutex);
		if(AppUtils.isNotBlank(mutexresult)){
			seckillJsonDto.setIsSuccess(false);
			seckillJsonDto.setError("操作频繁");
			return seckillJsonDto;
		}
		redisCacheClient.put(mutex, 2, "true");
		
		int flag = seckillCachedhandle.seckillCheck(seckill.getSid(), prodId, skuId, userId); // redis
		if (flag == 2) {
			seckillJsonDto.setIsSuccess(false);
			seckillJsonDto.setStatus(SeckillStateEnum.REPEATSECKILL.getState());
			seckillJsonDto.setError(SeckillStateEnum.REPEATSECKILL.getStateinfo());
			return seckillJsonDto;
		} else if (flag == -3) {
			seckillJsonDto.setIsSuccess(false);
			seckillJsonDto.setStatus(SeckillStateEnum.SELL_OUT.getState());
			seckillJsonDto.setError(SeckillStateEnum.SELL_OUT.getStateinfo());
			return seckillJsonDto;
		} else if (flag == 5) {
			logger.debug("##### 秒杀检查结果成功，结果已经秒杀成功过  ######");
			seckillJsonDto.setIsSuccess(false);
			seckillJsonDto.setStatus(SeckillStateEnum.SUCCESS2.getState());
			seckillJsonDto.setError(SeckillStateEnum.SUCCESS2.getStateinfo());
			return seckillJsonDto;
		}
		
		int result = 0;
		try {
			
			SeckillSkuDto skuDto=getSelect(seckill.getProdDto().getSkuDtoList(),skuId);
			
			//set的是秒杀价
			result = seckillService.executeSeckill(seckillId, prodId, skuId, userId, skuDto.getSeckPrice(), 1);
			
			logger.debug("##### 执行数据库结果 ={}  ######",result);
			
			if(result==-2){ //重复秒杀
				seckillJsonDto.setIsSuccess(false);
				seckillJsonDto.setStatus(SeckillStateEnum.REPEATSECKILL.getState());
				seckillJsonDto.setError(SeckillStateEnum.REPEATSECKILL.getStateinfo());
				return seckillJsonDto;
			}
			
			if(result==0){ //秒杀失败
				logger.debug("##### 执行数据库结果成功,秒杀成功入库失败  ######");
				seckillJsonDto = new SeckillJsonDto<SeckillResult>(false, SeckillStateEnum.NORESULT.getStateinfo());
				return seckillJsonDto;
			}
			
			logger.debug("##### 执行数据库过程结果成功,秒杀成功入库,设置用户秒杀结果凭证信息  ######");
			SeckillSuccess seckillSuccess=new SeckillSuccess();
			seckillSuccess.setProdId(prodId);
			seckillSuccess.setResult(result);
			seckillSuccess.setSeckillId(seckillId);
			seckillSuccess.setSeckillNum(1);
			//set的是秒杀价
			seckillSuccess.setSeckillPrice(skuDto.getSeckPrice());
			seckillSuccess.setSeckillTime(new java.sql.Timestamp(System.currentTimeMillis()));
			seckillSuccess.setSkuId(skuId);
			seckillSuccess.setStatus(0);
			seckillSuccess.setUserId(userId);
			seckillCachedhandle.seckillSuccess(seckillSuccess);
			SeckillResult seckillResult = new SeckillResult(seckillId, SeckillStateEnum.SUCCESS);
			seckillJsonDto = new SeckillJsonDto<SeckillResult>(true, seckillResult);
			return seckillJsonDto;
		} catch (Exception e) {
			// 删除缓存标识
			seckillCachedhandle.rollbackSeckill(seckillId, prodId, skuId, userId);
			e.printStackTrace();
			seckillJsonDto = new SeckillJsonDto<SeckillResult>(false, "系统繁忙,请稍候！");
			return seckillJsonDto;
		} finally{
			if(result!=1){
				// 删除缓存标识
				String user_key = SeckillStringUtils.cacheKeyJoint(SeckillConstant.SECKILL_BUY_USERS_, seckillId, userId, prodId, skuId);
				redisCacheClient.delete(user_key);
				seckillCachedhandle.rollbackSeckill(seckillId, prodId, skuId, userId);
			}
		}
	}
	
	private SeckillSkuDto getSelect(List<SeckillSkuDto> skuDtoList, long skuId) {
		for(SeckillSkuDto seckillSkuDto:skuDtoList){
			if(seckillSkuDto.getSkuId().equals(skuId)){
				return seckillSkuDto;
			}
		}
		return null;
	}

	/**
	 * 验证验证码
	 *
	 * @param request
	 * @param response
	 * @param randNum
	 * @return true, if validate rand img
	 */
	@RequestMapping("/validateRandImg")
	public @ResponseBody boolean validateRandImg(HttpServletRequest request, HttpServletResponse response, String randNum) {
		return ValidateCodeUtil.validateCodeAndKeep(randNum, request.getSession());
	}
}
