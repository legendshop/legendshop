/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.seckill.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.SeckillActivity;
import com.legendshop.model.entity.SeckillProd;
import com.legendshop.seckill.adminPage.SeckillAdminPage;
import com.legendshop.seckill.frontendPage.SeckillBackPage;
import com.legendshop.seckill.frontendPage.SeckillFrontPage;
import com.legendshop.spi.service.SeckillActivityService;
import com.legendshop.spi.service.SeckillProdService;
import com.legendshop.util.AppUtils;

/**
 * 秒杀控制器
 *
 */
@Controller
@RequestMapping("/seckillActivity")
public class SeckillActivityFrontController {
	
	@Autowired
	private SeckillActivityService seckillActivityService;

	@Autowired
	private SeckillProdService seckillProdService;

	/**
	 * 前台秒杀活动列表
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String list(HttpServletRequest request, HttpServletResponse response, String curPageNO, String startTime) {
		PageSupport<SeckillActivity> ps = seckillActivityService.querySeckillActivityList(curPageNO, startTime);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(SeckillFrontPage.SECKILL_LIST); // simple,没找到
	}

	/**
	 * 前台秒杀商品列表（异步load）
	 * 
	 * @param request
	 * @param response
	 * @param startTime
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping(value = "/querySeckillList", method = RequestMethod.GET)
	public String querySeckillProd(HttpServletRequest request, HttpServletResponse response, String curPageNO, String startTime) {
		request.setAttribute("startTime", startTime);
		PageSupport<SeckillActivity> ps = seckillActivityService.queryFrontSeckillList(curPageNO, startTime);
		getSeckillProdDetail(ps);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(SeckillBackPage.SECKILL_PROD_LIST); // simple
	}

	/**
	 * 查看秒杀商品详情
	 * @param ps
	 */
	private void getSeckillProdDetail(PageSupport ps) {
		List<Long> ids = new ArrayList<Long>();
		List<SeckillActivity> list = (List<SeckillActivity>) ps.getResultList();
		for (SeckillActivity seckillActivity : list) {
			if (AppUtils.isNotBlank(seckillActivity)) {
				ids.add(seckillActivity.getId());
			}
		}
		if (ids.size() > 0) {
			List<SeckillProd> prodList = this.seckillProdService.querySeckillProdList(ids);
			if (prodList.size() > 0) {
				for (SeckillActivity seckillActivity : list) {
					for (SeckillProd seckillProd : prodList) {
						if (seckillProd.getSId().equals(seckillActivity.getId())) {
							Long stock = seckillActivity.getStockTotal() + seckillProd.getSeckillStock();
							seckillActivity.setStockTotal(stock);

						}
					}
				}
			}
		}
	}

	/**
	 * 获得活动所在时间段
	 * 
	 * @param cur_hour
	 * @return
	 */
	private Integer getPeriodTime(int cur_hour) {
		if (cur_hour < 9) {
			return 0;
		} else if (9 <= cur_hour && cur_hour < 12) {
			return 9;
		} else if (12 <= cur_hour && cur_hour < 15) {
			return 12;
		} else if (15 <= cur_hour && cur_hour < 18) {
			return 15;
		} else if (18 <= cur_hour && cur_hour < 21) {
			return 18;
		} else if (21 <= cur_hour && cur_hour < 24) {
			return 21;
		} else {
			return 0;
		}

	}

	/**
	 *   进入编辑页面
	 */
	@RequestMapping("/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SeckillActivity seckillActivity = seckillActivityService.getSeckillActivity(id);
		if (AppUtils.isBlank(seckillActivity)) {
			throw new NullPointerException("seckillActivity is null");
		}
		request.setAttribute("seckillActivity", seckillActivity);
		return PathResolver.getPath(SeckillAdminPage.SECKILL_ACTIVITY_PAGE);
	}

	/**
	 *  查看活动
	 */
	@RequestMapping(value = "/seckView/{id}", method = RequestMethod.GET)
	public String seckView(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SeckillActivity seckillActivity = seckillActivityService.getSeckillActivity(id);
		if (AppUtils.isBlank(seckillActivity)) {
			throw new NullPointerException("seckillActivity is null");
		}
		request.setAttribute("seckillActivity", seckillActivity);
		return PathResolver.getPath(SeckillFrontPage.SECKILL_VIEW);
	}

	/**
	 *   审核
	 */
	@RequestMapping(value = "/audit", method = RequestMethod.POST)
	public @ResponseBody String audit(HttpServletRequest request, HttpServletResponse response, SeckillActivity seckillActivity) {
		if (AppUtils.isBlank(seckillActivity) || AppUtils.isBlank(seckillActivity.getAuditOpinion()) || AppUtils.isBlank(seckillActivity.getStatus())) {
			return Constants.FAIL;
		}
		SeckillActivity seckillPo = seckillActivityService.getSeckillActivity(seckillActivity.getId());
		if (AppUtils.isBlank(seckillPo)) {
			return Constants.FAIL;
		}
		seckillPo.setStatus(seckillActivity.getStatus());
		seckillPo.setAuditTime(new Date());
		seckillPo.setAuditOpinion(seckillActivity.getAuditOpinion());
		seckillActivityService.updateSeckillActivity(seckillPo);
		return Constants.SUCCESS;
	}

	/**
	 *  上下线
	 */
	@RequestMapping(value = "/updateStatus/{id}/{status}", method = RequestMethod.GET)
	public @ResponseBody String updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Long status) {
		SeckillActivity seckillPo = seckillActivityService.getSeckillActivity(id);
		if (AppUtils.isBlank(seckillPo) || AppUtils.isBlank(status)) {
			return Constants.FAIL;
		}
		if (!status.equals(seckillPo.getStatus())) {
			seckillPo.setStatus(status);
			seckillActivityService.updateSeckillActivity(seckillPo);
		}
		return Constants.SUCCESS;
	}

}
