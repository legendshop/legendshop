/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.seckill.frontendPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 秒杀重定向页面定义
 */
public enum SeckillRedirectPage implements PageDefinition {
	
	/** 查询秒杀活动 */
	SECKILL_QUERY("/s/seckillActivity/query");

	/** The value. */
	private final String value;

	private SeckillRedirectPage(String value, String... template) {
		this.value = value;
	}

	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("redirect:", path);
	}

	private SeckillRedirectPage(String value) {
		this.value = value;
	}

	public String getNativeValue() {
		return value;
	}

}
