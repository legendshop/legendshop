/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.seckill.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.entity.*;
import com.legendshop.seckill.frontendPage.SeckillFrontPage;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.spi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.SeckillStatusEnum;
import com.legendshop.model.dto.seckill.SeckillActivityDto;
import com.legendshop.seckill.adminPage.SeckillAdminBackPage;
import com.legendshop.seckill.adminPage.SeckillAdminFrontPage;
import com.legendshop.seckill.adminPage.SeckillAdminPage;
import com.legendshop.seckill.adminPage.SeckillAdminRedirectPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.util.SeckillCachedhandle;
import com.legendshop.util.AppUtils;

/**
 * 秒杀活动管理控制器
 *
 */
@Controller
@RequestMapping("/admin/seckillActivity")
public class AdminSeckillActivityController {
	
	@Autowired
	private SeckillActivityService seckillActivityService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private ProductService productService;

	@Autowired
	private SkuService skuService;

	@Autowired
	private SubService subService;

	@Autowired
	private SeckillProdService seckillProdService;

	@Autowired
	private SeckillCachedhandle seckillCachedService;

	/**
	 * 查找秒杀活动
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, SeckillActivity seckillActivity) {
		request.setAttribute("seckillActivity", seckillActivity);
		return PathResolver.getPath(SeckillAdminPage.SECKILL_ACTIVITY_LIST);
	}
	
	/**
	 * 查找秒杀活动列表
	 */
	@RequestMapping("/queryContent")
	public String queryContent(HttpServletRequest request, HttpServletResponse response, String curPageNO, SeckillActivity seckillActivity) {
		int pageSize = 20;
		if(AppUtils.isNotBlank(seckillActivity.getStatus())){
			seckillActivity.setOverdate(new Date());
		}
		PageSupport<SeckillActivity> ps = seckillActivityService.getSeckillActivity(curPageNO, pageSize, seckillActivity);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("seckillActivity", seckillActivity);
		return PathResolver.getPath(SeckillAdminPage.SECKILL_ACTIVITY_CONTENT_LIST);
	}

	/**
	 * 进入编辑页面
	 */
	@RequestMapping("/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SeckillActivity seckillActivity = seckillActivityService.getSeckillActivity(id);
		if (AppUtils.isBlank(seckillActivity)) {
			throw new NullPointerException("seckillActivity is null");
		}
		request.setAttribute("seckillActivity", seckillActivity);
		return PathResolver.getPath(SeckillAdminPage.SECKILL_ACTIVITY_PAGE);
	}

	@RequestMapping("/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(SeckillAdminPage.SECKILL_ACTIVITY_PAGE);
	}

	/**
	 * 挑选商品弹层
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param product
	 * @return
	 */
	@RequestMapping("/seckillProdLayout")
	public String SeckillProdLayout(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		int pageSize = 18;
		PageSupport<Product> ps = productService.getProductList(curPageNO, pageSize, shopId, product);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prod", product);
		String result = PathResolver.getPath(SeckillAdminBackPage.SECKILL_PROD_LAYOUT);
		return result;
	}

	/**
	 * 
	 * 秒杀商品sku
	 * @param request
	 * @param response
	 * @param prodId
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/seckillProdSku/{prodId}")
	public String seckillProdSku(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId, String curPageNO) {
		int status = 1;
		int stocks = 1;
		PageSupport<Sku> ps = skuService.getSku(curPageNO, prodId, status, stocks);
		PageSupportHelper.savePage(request, ps);
		List<Sku> skuList =  ps.getResultList();
		if (AppUtils.isNotBlank(skuList)) {
			for (int i = 0; i < skuList.size(); i++) {
				Sku sku = skuList.get(i);
				String property = skuService.getSkuByProd(sku);
				if (AppUtils.isNotBlank(property)) {
					sku.setProperty(property);
				}
			}
			request.setAttribute("skuList", skuList);
		} else {
			ProductDetail prod = productService.getProdDetail(prodId);
			request.setAttribute("prod", prod);
		}
		String result = PathResolver.getPath(SeckillAdminFrontPage.SECKILL_PROD_SKU_LIST);
		return result;
	}

	/**
	 * 保存秒杀活动
	 * @param request
	 * @param response
	 * @param seckillActivity
	 * @return
	 */
	@RequestMapping("/save")
	public String save(HttpServletRequest request, HttpServletResponse response, SeckillActivity seckillActivity) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		Long shopId = user.getShopId();
		
		if (AppUtils.isBlank(seckillActivity.getId())) {
			// save
			String shopName = shopDetailService.getShopName(shopId);
			if (AppUtils.isNotBlank(shopId) && AppUtils.isNotBlank(shopName) && AppUtils.isNotBlank(userId) && AppUtils.isNotBlank(userName)) {
				boolean result = seckillActivityService.saveSeckillActivityAndProd(seckillActivity, userId, shopId, shopName, userName,
						SeckillStatusEnum.OFFLINE.value());
				if (!result) {
					throw new NullPointerException("save fail");
				}
			}
		} else {
			// update
			boolean result = seckillActivityService.updateSeckillActivityAndProd(seckillActivity, shopId, userName);
			if (!result) {
				throw new NullPointerException("update fail");
			}
		}
		return PathResolver.getPath(SeckillAdminRedirectPage.SECKILL_QUERY);

	}

	/**
	 * 查看活动
	 */
	@RequestMapping(value = "/seckView/{id}", method = RequestMethod.GET)
	public String seckView(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SeckillActivity seckillActivity = seckillActivityService.getSeckillActivity(id);
		if (AppUtils.isBlank(seckillActivity)) {
			throw new NullPointerException("seckillActivity is null");
		}
		List<SeckillProd> seckillProds = seckillProdService.getSeckillProds(seckillActivity.getId());
		List<SeckillActivityDto> activityDtos = seckillProdService.findSkuProd(seckillProds);
		
		if(AppUtils.isNotBlank(seckillProds)) {
			request.setAttribute("prodId", seckillProds.get(seckillProds.size()-1).getProdId());
		}
		
		request.setAttribute("activityDtos", activityDtos);
		request.setAttribute("seckillActivity", seckillActivity);
		
		return PathResolver.getPath(SeckillAdminPage.SECKILL_ACTIVITY_VIEW);
	}

	/**
	 *  审核秒杀活动
	 */
	@SystemControllerLog(description="审核秒杀活动")
	@RequestMapping(value = "/audit", method = RequestMethod.POST)
	public @ResponseBody String audit(HttpServletRequest request, HttpServletResponse response, SeckillActivity seckillActivity) {
		if (AppUtils.isBlank(seckillActivity) || AppUtils.isBlank(seckillActivity.getAuditOpinion()) || AppUtils.isBlank(seckillActivity.getStatus())) {
			return Constants.FAIL;
		}
		SeckillActivity seckillPo = seckillActivityService.getSeckillActivity(seckillActivity.getId());
		if (AppUtils.isBlank(seckillPo)) {
			return Constants.FAIL;
		}
		seckillPo.setStatus(seckillActivity.getStatus());
		seckillPo.setAuditTime(new Date());
		seckillPo.setAuditOpinion(seckillActivity.getAuditOpinion());
		seckillActivityService.updateSeckillActivity(seckillPo);
		if (!SeckillStatusEnum.FAILED.value().equals(seckillActivity.getStatus())){
			seckillActivityService.updateSeckillActivity(seckillPo.getId(),SeckillStatusEnum.ONLINE.value());
		}
		return Constants.SUCCESS;
	}

	/**
	 *  终止秒杀活动
	 */
	@SystemControllerLog(description="终止秒杀活动")
	@RequestMapping(value = "/offlineSeckillActivity/{id}")
	public @ResponseBody String offlineSeckillActivity(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

		String result = seckillActivityService.updateSeckillActivity(id,SeckillStatusEnum.OFFLINE.value());
		return result;
	}

	/**
	 *  编辑
	 */
	@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SeckillActivity seckillActivity = seckillActivityService.getSeckillActivity(id);
		if (AppUtils.isBlank(seckillActivity)) {
			throw new NullPointerException("seckillActivity is null");
		}
		List<SeckillProd> seckillProds = seckillProdService.getSeckillProds(seckillActivity.getId());
		List<SeckillActivityDto> activityDtos = seckillProdService.findSkuProd(seckillProds);
		
		if(AppUtils.isNotBlank(seckillProds)) {
			request.setAttribute("prodId", seckillProds.get(seckillProds.size()).getProdId());
		}
		
		request.setAttribute("activityDtos", activityDtos);
		request.setAttribute("seckillActivity", seckillActivity);
		// 已经上线但未开始时 点击编辑 需要清除缓存
		if (seckillActivity.getStatus() == 1) {
			seckillCachedService.delSeckill(id);
		}
		return PathResolver.getPath(SeckillAdminPage.SECKILL_ACTIVITY_PAGE);
	}

	/**
	 * 删除
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="删除秒杀活动")
	@RequestMapping("/delete/{id}")
	public String deleteSeckillActivity(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		if (AppUtils.isBlank(id)) {
			throw new NullPointerException("SeckillActivity id is null");
		}
		SeckillActivity seckillActivity = seckillActivityService.getSeckillActivity(id);
		if (AppUtils.isBlank(seckillActivity)) {
			throw new NullPointerException("error");
		}
		seckillActivityService.deleteSeckillActivity(seckillActivity);
		seckillCachedService.delSeckill(id);
		return PathResolver.getPath(SeckillAdminRedirectPage.SECKILL_QUERY);
	}

	/**
	 *   运营活动
	 */
	@RequestMapping(value = "/operatorSeckillActivity/{seckillId}", method = RequestMethod.GET)
	public String seckView(HttpServletRequest request, HttpServletResponse response, @PathVariable Long seckillId, String curPageNO) {
		SeckillActivity seckillActivity = seckillActivityService.getSeckillActivity(seckillId);
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(seckillActivity)) {
			throw new NullPointerException("seckillActivity is null");
		}
		// 获取秒杀活动的订单
		if(AppUtils.isBlank(curPageNO) || "0".equals(curPageNO)){
			curPageNO = "1";
		}
		PageSupport<Sub> ps = subService.getSeckillOperationList(curPageNO, 6, seckillActivity.getId(),user.getShopId());
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("seckillId",seckillId);
		return PathResolver.getPath(SeckillAdminPage.SECKILL_OPERATOR_VIEW);
	}
}
