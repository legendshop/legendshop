/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.seckill.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.seckill.SeckillSuccessRecord;
import com.legendshop.seckill.frontendPage.MobileSeckillFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.SeckillService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.MD5Util;

/**
 * （手机端）用户秒杀记录控制器
 */
@Controller
@RequestMapping("/p/wap/seckill/record")
public class MobileUserSeckillRecController {

	@Autowired
	private SeckillService seckillService;

	/**
	 * 查找秒杀记录列表
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return the string
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		PageSupport<SeckillSuccessRecord> ps = seckillService.querySeckillRecord(curPageNO, userId);

		List<SeckillSuccessRecord> successRecords = (List<SeckillSuccessRecord>) ps.getResultList();
		if (AppUtils.isNotBlank(successRecords)) {
			for (SeckillSuccessRecord successRecord : successRecords) {
				StringBuilder builder = new StringBuilder(10);
				builder.append(successRecord.getSeckillId()).append(successRecord.getProdId()).append(successRecord.getSkuId());
				String secretKey = MD5Util.toMD5(builder.toString() + Constants._MD5);
				successRecord.setSecretKey(secretKey);
			}
			ps.setResultList(successRecords);
		}
		PageSupportHelper.savePage(request, ps);
		String path = PathResolver.getPath("/mobile/seckillRec", MobileSeckillFrontPage.VARIABLE);
		return path;
	}

	/**
	 * 删除记录
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return the string
	 */
	@RequestMapping(value = "/deleteRecord/{id}", method = RequestMethod.GET)
	public String deleteRecord(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		seckillService.deleteRecord(id, userId);
		return PathResolver.getRedirectPath("/p/wap/seckill/record");
	}

}
