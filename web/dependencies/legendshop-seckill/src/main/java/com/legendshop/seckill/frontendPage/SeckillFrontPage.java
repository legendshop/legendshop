/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.seckill.frontendPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 秒杀前台页面定义
 */
public enum SeckillFrontPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 秒杀活动列表 */
	SECKILL_LIST("/seckillList"),

	/** 商品详情 */
	SECKILL_VIEW("/seckillDetail"),

	/** 秒杀记录 */
	SECKILL_RECORD("/seckillRec"),
	
	/** 秒杀记录列表 */
	USER_SECKILL_REC_LIST("/userSeckillRecList"),

	/** 秒杀活动管理 */
	SECKILL_ACTIVITY_LIST_PAGE("/shop/seckillActivityList"),

	/** 添加秒杀活动 */
	SECKILL_ACTIVITY_PAGE("/shop/seckillActivity"),

	/** 查看秒杀活动 */
	SECKILL_ACTIVITY__DEATAIL_PAGE("/shop/seckillActivityDetail"),

	/** 秒杀活动详情 */
	SECKILL_ACTIVITY_VIEW("/shop/seckillActivityView"),;

	/** The value. */
	private final String value;

	private SeckillFrontPage(String value) {
		this.value = value;
	}

	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath("seckill", path);
	}

	public String getNativeValue() {
		return value;
	}

}
