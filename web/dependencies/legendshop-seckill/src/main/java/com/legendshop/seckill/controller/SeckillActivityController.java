/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.seckill.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.constant.ActiveBannerTypeEnum;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.business.service.impl.SkuServiceImpl;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.SeckillStatusEnum;
import com.legendshop.model.constant.SkuActiveTypeEnum;
import com.legendshop.model.dto.seckill.SeckillActivityDto;
import com.legendshop.seckill.adminPage.SeckillAdminFrontPage;
import com.legendshop.seckill.frontendPage.SeckillBackPage;
import com.legendshop.seckill.frontendPage.SeckillFrontPage;
import com.legendshop.seckill.frontendPage.SeckillRedirectPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.util.SeckillCachedhandle;
import com.legendshop.util.AppUtils;
import com.legendshop.util.StringUtil;

/**
 * 秒杀活动控制器
 *
 */
@Controller
@RequestMapping("/s/seckillActivity")
public class SeckillActivityController {
	
	@Autowired
	private SeckillActivityService seckillActivityService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private SkuService skuService;

	@Autowired
	private SubService subService;
	
	@Autowired
	private SeckillProdService seckillProdService;

	@Autowired
	private SeckillService seckillService;

	@Autowired
	private SeckillCachedhandle seckillCachedService;
	
	@Autowired
	private SystemParameterService systemParameterService;

	/**
	 * 查找秒杀活动
	 */
	@RequestMapping("/query")
	@Authorize(authorityTypes = AuthorityType.SECKILL_MANAGE)
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, SeckillActivity seckillActivity) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		int pageSize = 10;
		PageSupport<SeckillActivity> ps = seckillActivityService.getSeckillActivity(curPageNO, shopId, pageSize, seckillActivity);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("seckillActivity", seckillActivity);
		String result = PathResolver.getPath(SeckillFrontPage.SECKILL_ACTIVITY_LIST_PAGE);
		return result;
	}

	/**
	 *  上下线
	 */
	@RequestMapping(value = "/updateStatus/{id}/{status}", method = RequestMethod.GET)
	@Authorize(authorityTypes = AuthorityType.SECKILL_MANAGE)
	public String updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Long status) {
		SeckillActivity seckillPo = seckillActivityService.getSeckillActivity(id);
		if (AppUtils.isBlank(seckillPo) || AppUtils.isBlank(status)) {
			return PathResolver.getPath(SeckillFrontPage.SECKILL_ACTIVITY_LIST_PAGE);
		}


		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		if (seckillPo.getShopId().intValue() != shopId.intValue()) {
			return PathResolver.getPath(SeckillFrontPage.SECKILL_ACTIVITY_LIST_PAGE);
		}
		String result = seckillActivityService.shopUpdateSeckillActivity(seckillPo,id,status);
		if(StringUtil.isNotBlank(result)){
			request.setAttribute("message", result);
			return PathResolver.getPath("/seckillError", SeckillFrontPage.VARIABLE);
		}
		return PathResolver.getPath(SeckillRedirectPage.SECKILL_QUERY);
	}

	/**
	 * 增加秒杀活动
	 */
	@RequestMapping("/addSeckillActivity")
	@Authorize(authorityTypes = AuthorityType.SECKILL_MANAGE)
	public String addSeckillActivity(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(SeckillFrontPage.SECKILL_ACTIVITY_PAGE);
	}

	/**
	 *   保存
	 */
	@RequestMapping("/save")
	@Authorize(authorityTypes = AuthorityType.SECKILL_MANAGE)
	public String save(HttpServletRequest request, HttpServletResponse response, SeckillActivity seckillActivity) {
		SecurityUserDetail userDetail = UserManager.getUser(request);
		if (userDetail == null) {
			request.setAttribute("message", "非法访问");
			return PathResolver.getPath("/seckillError", SeckillFrontPage.VARIABLE);
		}
		if (AppUtils.isBlank(seckillActivity) || !validateSeckill(seckillActivity)) {
			request.setAttribute("message", "请完善秒杀信息");
			return PathResolver.getPath("/seckillError", SeckillFrontPage.VARIABLE);
		}
		if(seckillActivity.getStartTime().after(seckillActivity.getEndTime()) || seckillActivity.getStartTime().equals(seckillActivity.getEndTime())){
			request.setAttribute("message", "秒杀活动开始时间必须小于结束时间");
			return PathResolver.getPath("/seckillError", SeckillFrontPage.VARIABLE);
		}
		String skus = seckillActivity.getSkus();
		if (AppUtils.isBlank(skus)) {
			request.setAttribute("message", "请添加商品信息");
			return PathResolver.getPath("/seckillError", SeckillFrontPage.VARIABLE);
		}
		SystemParameter systemParameter=systemParameterService.getSystemParameter("SPIKE_ACTIVITY_AUDIT");
		
		Long spikeStatus;
		if(AppUtils.isNotBlank(systemParameter)&&systemParameter.getValue().equals("true")){//确定秒杀活动是否需要审核
			spikeStatus=SeckillStatusEnum.VALIDATING.value();
		}else{
			spikeStatus=SeckillStatusEnum.ONLINE.value();
		}
		
		if (AppUtils.isBlank(seckillActivity.getId())) {
			boolean result = seckillActivityService.saveSeckillActivityAndProd(seckillActivity, userDetail.getUserId(), userDetail.getShopUser().getShopId(), userDetail.getShopUser().getShopName(), userDetail.getUsername(),spikeStatus);
			if (!result) {
				request.setAttribute("message", "保存失败");
				return PathResolver.getPath("/seckillError", SeckillFrontPage.VARIABLE);
			}
		} else {
			seckillActivity.setStatus(spikeStatus);
			boolean result = seckillActivityService.updateSeckillActivityAndProd(seckillActivity, userDetail.getShopUser().getShopId(),
					userDetail.getUsername());
			if (!result) {
				throw new NullPointerException("update fail");
			}
		}
		return PathResolver.getPath(SeckillRedirectPage.SECKILL_QUERY);
	}

	/**
	 * 校验活动数据
	 * 
	 * @param seckillActivity
	 * @return
	 */
	private boolean validateSeckill(SeckillActivity seckillActivity) {
		if (AppUtils.isBlank(seckillActivity.getSeckillTitle())) {
			return false;
		}
		if (AppUtils.isBlank(seckillActivity.getStartTime())) {
			return false;
		}
		if (AppUtils.isBlank(seckillActivity.getEndTime())) {
			if(AppUtils.isNotBlank(seckillActivity.getId())){
				return true;
			}
			return false;
		}
		return true;
	}

	/**
	 *   运营活动
	 */
	@RequestMapping(value = "/operatorSeckillActivity/{id}", method = RequestMethod.GET)
	@Authorize(authorityTypes = AuthorityType.SECKILL_MANAGE)
	public String seckView(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, String curPageNO) {
		SeckillActivity seckillActivity = seckillActivityService.getSeckillActivity(id);
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(seckillActivity)) {
			throw new NullPointerException("seckillActivity is null");
		}
		// 获取秒杀活动的订单
		if(AppUtils.isBlank(curPageNO) || "0".equals(curPageNO)){
			curPageNO = "1";
		}
		PageSupport<Sub> ps = subService.getSeckillOperationList(curPageNO, 6, seckillActivity.getId(),user.getShopId());
		PageSupportHelper.savePage(request, ps);
		OperateStatisticsDTO operateStatistics = seckillService.getOperateStatistics(id);
		operateStatistics.setPeopleCount(seckillService.getPersonCount(id));
		request.setAttribute("seckillId",seckillActivity.getId());
		request.setAttribute("operateStatistics",operateStatistics);
		return PathResolver.getPath(SeckillFrontPage.SECKILL_ACTIVITY_VIEW);
	}

	/**
	 *   去编辑页面
	 */
	@RequestMapping(value = "/editSeckillActivity/{id}", method = RequestMethod.GET)
	@Authorize(authorityTypes = AuthorityType.SECKILL_MANAGE)
	public String editSeckillActivity(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, String curPageNO) {
		SeckillActivity seckillActivity = seckillActivityService.getSeckillActivity(id);
		if (AppUtils.isBlank(seckillActivity)) {
			throw new NullPointerException("seckillActivity is null");
		}
		List<SeckillProd> seckillProds = seckillProdService.getSeckillProds(seckillActivity.getId());
		List<SeckillActivityDto> activityDtos = seckillProdService.findSkuProd(seckillProds);

		request.setAttribute("activityDtos", activityDtos);
		request.setAttribute("seckillActivity", seckillActivity);
		return PathResolver.getPath(SeckillFrontPage.SECKILL_ACTIVITY_PAGE);
	}



	/**
	 *   查看活动
	 */
	@RequestMapping(value = "/seckillActivityDetail/{id}", method = RequestMethod.GET)
	@Authorize(authorityTypes = AuthorityType.SECKILL_MANAGE)
	public String seckillActivityDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, String curPageNO) {
		SeckillActivity seckillActivity = seckillActivityService.getSeckillActivity(id);
		if (AppUtils.isBlank(seckillActivity)) {
			throw new NullPointerException("seckillActivity is null");
		}
		List<SeckillProd> seckillProds = seckillProdService.getSeckillProds(seckillActivity.getId());
		List<SeckillActivityDto> activityDtos = seckillProdService.findSkuProd(seckillProds);

		request.setAttribute("activityDtos", activityDtos);
		request.setAttribute("seckillActivity", seckillActivity);
		return PathResolver.getPath(SeckillFrontPage.SECKILL_ACTIVITY__DEATAIL_PAGE);
	}

	/**
	 *   删除
	 */
	@RequestMapping("/delete/{id}")
	@Authorize(authorityTypes = AuthorityType.SECKILL_MANAGE)
	public String deleteSeckillActivity(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		if (AppUtils.isBlank(id)) {
			throw new NullPointerException("SeckillActivity id is null");
		}
		SeckillActivity seckillActivity = seckillActivityService.getSeckillActivity(id);

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		if (!shopId.equals(seckillActivity.getShopId())) {
			request.setAttribute("message", "没有操作权限");
			return PathResolver.getPath("/seckillError", SeckillFrontPage.VARIABLE);
		}
		seckillActivityService.deleteSeckillActivity(seckillActivity);
		seckillCachedService.delSeckill(seckillActivity.getId());
		return PathResolver.getPath(SeckillRedirectPage.SECKILL_QUERY);
	}

	/**
	 * 秒杀挑选商品弹层
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param product
	 * @return
	 */
	@RequestMapping("/seckillProdLayout")
	@Authorize(authorityTypes = AuthorityType.SECKILL_MANAGE)
	public String SeckillProdLayout(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		int pageSize = 5;
		PageSupport<Product> ps = productService.getSeckillLayer(curPageNO, shopId, pageSize, product);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prod", product);
		String result = PathResolver.getPath(SeckillBackPage.SECKILL_PROD_LAYOUT); // product
		return result;
	}

	/**
	 * 检查秒杀商品是否可以参加秒杀活动，检查库存
	 * 
	 * @param request
	 * @param response
	 * @param prodId
	 * @return
	 */
	@RequestMapping("/checkSeckillProdSku/{prodId}")
	@ResponseBody
	public boolean checkSeckillProdSku(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {
		return seckillProdService.checkSeckillProdSku(prodId);
	}

	@RequestMapping("/seckillProdSku/{skus}")
	@Authorize(authorityTypes = AuthorityType.SECKILL_MANAGE)
	public String seckillProdSku(HttpServletRequest request, HttpServletResponse response, @PathVariable Long[] skus) {
		List<Sku> skuList = skuService.getSku(skus);
		request.setAttribute("skuList", skuList);
		String result = PathResolver.getPath(SeckillAdminFrontPage.SECKILL_PROD_SKU_LIST); // 没找到
		return result;
	}

	/**
	 *  编辑
	 */
	@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	@Authorize(authorityTypes = AuthorityType.SECKILL_MANAGE)
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SeckillActivity seckillActivity = seckillActivityService.getSeckillActivity(id);
		if (AppUtils.isBlank(seckillActivity)) {
			throw new NullPointerException("seckillActivity is null");
		}
		List<SeckillProd> seckillProds = seckillProdService.getSeckillProds(seckillActivity.getId());
		List<SeckillActivityDto> activityDtos = seckillProdService.findSkuProd(seckillProds);
		
		if(AppUtils.isNotBlank(seckillProds)) {
			request.setAttribute("prodId", seckillProds.get(seckillProds.size()-1).getProdId());
		}
		
		request.setAttribute("activityDtos", activityDtos);
		request.setAttribute("seckillActivity", seckillActivity);
		if (seckillActivity.getStatus() == 1) {// 已经上线但未开始时 点击编辑 需要清除缓存
			seckillCachedService.delSeckill(id);
		}
		return PathResolver.getPath(SeckillFrontPage.SECKILL_ACTIVITY_PAGE);
	}	
	
	/**
	 * 是否所有的sku都参加了其他营销活动
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/isAttendActivity/{prodId}", method = RequestMethod.GET)
	@ResponseBody
	public boolean isAttendActivity(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId) {
		List<Sku> skus = skuService.getSkuByProd(prodId);
		for (Sku sku : skus) {
			if(sku.getSkuType().equals(SkuActiveTypeEnum.PRODUCT.value())) {
				return true;
			}
		}
		return false;
	}
}
