/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.seckill.frontendPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 手机端秒杀.
 */
public enum MobileSeckillFrontPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 手机端秒杀 **/
	MOBILE_SECKILL_LIST("/mobile/seckillActList"),

	/** 商品列表 **/
	MOBILE_SECKILLMOBILE_LIST("/mobile/seckillProdList"),

	/** 秒杀商品详细 */
	SECKILL_VIEW("/mobile/seckillDetail"),;

	/** The value. */
	private final String value;

	private MobileSeckillFrontPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculateMobilePluginFronendPath("seckill", path);
	}

	public String getNativeValue() {
		return value;
	}

}
