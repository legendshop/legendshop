/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.seckill.adminPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 秒杀管理前台页面定义
 */
public enum SeckillAdminFrontPage implements PageDefinition {
	
	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 商品sku */
	SECKILL_PROD_SKU_LIST("/shop/seckillProdSku"),
	
	;

	/** The value. */
	private final String value;

	private SeckillAdminFrontPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("seckill", path);
	}

	public String getNativeValue() {
		return value;
	}

}
