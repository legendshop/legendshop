/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.seckill.adminPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 秒杀管理重定向页面定义
 */
public enum SeckillAdminRedirectPage implements PageDefinition {
	
	/** The VARIABLE. 可变路径 */
	VARIABLE(""),
	
	/** 查找秒杀活动 */
	SECKILL_QUERY("/admin/seckillActivity/query"),
	
	/** 秒杀广告图列表 */
	SECKILL_BANNNER_LIST("/admin/seckillAdvertise/index"),
	
	;
	
	/** The value. */
	private final String value;

	private SeckillAdminRedirectPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("redirect:", path);
	}

	/**
	 * Instantiates a new tiles page.
	 * 
	 * @param value
	 *            the value
	 */
	private SeckillAdminRedirectPage(String value) {
		this.value = value;
	}

	public String getNativeValue() {
		return value;
	}

}
