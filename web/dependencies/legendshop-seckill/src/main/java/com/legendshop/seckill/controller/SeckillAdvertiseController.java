/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.seckill.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.ActiveBannerTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.ActiveBanner;
import com.legendshop.seckill.adminPage.SeckillAdminPage;
import com.legendshop.seckill.adminPage.SeckillAdminRedirectPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ActiveBannerService;
import com.legendshop.util.AppUtils;

/**
 * 
 * 秒杀广告控制器
 *
 */
@Controller
@RequestMapping("/admin/seckillAdvertise")
public class SeckillAdvertiseController {

	@Autowired
	private ActiveBannerService activeBannerService;

	@RequestMapping(value = "/index")
	public String ad(HttpServletRequest request, HttpServletResponse response) {
		List<ActiveBanner> bannerList = activeBannerService.getBannerList(ActiveBannerTypeEnum.SECKILL_BANNER.value());
		request.setAttribute("bannerList", bannerList);
		return PathResolver.getPath(SeckillAdminPage.SECKILL_ADV);
	}

	/**
	 * 保存秒杀广告
	 */
	@SystemControllerLog(description="保存秒杀广告")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, ActiveBanner groupBanner,MultipartFile file) throws IOException{

		SecurityUserDetail user = UserManager.getUser(request);
		
		activeBannerService.saveBanner(groupBanner,user.getUsername(), user.getUserId(), user.getShopId(),file);
		
		return PathResolver.getPath(SeckillAdminRedirectPage.SECKILL_BANNNER_LIST);
	}

	/**
	 * 删除秒杀广告
	 */
	@SystemControllerLog(description="删除秒杀广告")
	@RequestMapping(value = "/delete/{id}")
	@ResponseBody
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable
			Long id) {
		ActiveBanner groupBanner = activeBannerService.getBanner(id);
		if (AppUtils.isBlank(groupBanner)) {
			return "删除失败，请刷新重试";
		}
		activeBannerService.deleteBanner(groupBanner);
		return Constants.SUCCESS;
	}


	/**
	 * 更新编辑页面
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response,Long id) {        
		if(AppUtils.isNotBlank(id)){
			ActiveBanner activeBanner = activeBannerService.getBanner(id);
			request.setAttribute("activeBanner", activeBanner);
		}
		return PathResolver.getPath(SeckillAdminPage.SECKILL_ADV_ADD);
	}

}
