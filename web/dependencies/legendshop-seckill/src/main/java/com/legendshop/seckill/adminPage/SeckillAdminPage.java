/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.seckill.adminPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 秒杀管理Tiles页面定义
 *
 */
public enum SeckillAdminPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),
	
	/** 秒杀活动管理 */
	SECKILL_ACTIVITY_LIST("/shop/seckillActivityList"),
	
	/** 秒杀活动列表 */
	SECKILL_ACTIVITY_CONTENT_LIST("/shop/seckillActivityContentList"),
	
	/** 编辑秒杀活动 */
	SECKILL_ACTIVITY_PAGE("/shop/seckillActivity"),
	
	/** 查看秒杀活动 */
	SECKILL_ACTIVITY_VIEW("/shop/seckillActivityView"),

	/** 运营秒杀活动 */
	SECKILL_OPERATOR_VIEW("/shop/seckillActivityOperation"),

	/** 秒杀活动广告图列表 */
	SECKILL_ADV("/ad/ad"),
	
	/**秒杀活动广告图编辑页面 */
	SECKILL_ADV_ADD("/ad/addAdverties"),
	
	;
	
	
	/** The value. */
	private final String value;

	private SeckillAdminPage(String value) {
		this.value = value;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("seckill", path);
	}

	public String getNativeValue() {
		return value;
	}
	
	
}
