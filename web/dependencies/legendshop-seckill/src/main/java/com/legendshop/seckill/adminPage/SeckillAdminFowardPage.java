/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.seckill.adminPage;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;
import com.legendshop.util.AppUtils;

/**
 * 秒杀管理跳转页面定义
 */
public enum SeckillAdminFowardPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE("");

	private final String value;

	private List<String> templates;

	private SeckillAdminFowardPage(String value, String... template) {
		this.value = value;
		if (AppUtils.isNotBlank(template)) {
			this.templates = new ArrayList<String>();
			for (String temp : template) {
				templates.add(temp);
			}
		}

	}

	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("forward:", path);
	}

	public String getNativeValue() {
		return value;
	}

	public List<String> getTemplates() {
		return this.templates;
	}

}
