/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.seckill.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.SeckillActivity;
import com.legendshop.seckill.frontendPage.MobileSeckillFrontPage;
import com.legendshop.spi.service.SeckillActivityService;

/**
 * （手机端）秒杀控制器
 */
@Controller
@RequestMapping("/wap/seckill")
public class MobileSeckillController {

	@Autowired
	private SeckillActivityService seckillActivityService;

	/**
	 * 手机端秒杀活动列表
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param startTime
	 * @return the string
	 */
	@RequestMapping("/list")
	public String seckillActList(HttpServletRequest request, HttpServletResponse response, String curPageNO, String startTime) {
		return PathResolver.getPath(MobileSeckillFrontPage.MOBILE_SECKILL_LIST);
	}

	/**
	 * 前台秒杀商品列表（异步load）.
	 *
	 * @param request
	 * @param response
	 * @param startTime
	 * @return the string
	 */
	@RequestMapping(value = "/querySeckillList", method = RequestMethod.GET)
	public String querySeckillProd(HttpServletRequest request, HttpServletResponse response, String startTime) {
		/*
		 * SimpleSqlQuery query = new SimpleSqlQuery(SeckillActivity.class);
		 * QueryMap map = new QueryMap(); Calendar periodStart =
		 * Calendar.getInstance(); periodStart.setTime(new Date());
		 * periodStart.set(Calendar.MILLISECOND, 0);
		 * periodStart.set(Calendar.SECOND,0);
		 * periodStart.set(Calendar.MINUTE,0); Calendar periodEnd =
		 * Calendar.getInstance(); periodEnd.setTime(new Date());
		 * periodEnd.set(Calendar.MILLISECOND, 0);
		 * periodEnd.set(Calendar.SECOND,0); periodEnd.set(Calendar.MINUTE,0);
		 * if(startTime!=null){ periodStart.set(Calendar.HOUR_OF_DAY,
		 * Integer.parseInt(startTime));//当前点击的时间段 起始时间点 map.put("periodStart",
		 * periodStart.getTime()); int foo =Integer.parseInt(startTime) + 3;
		 * periodEnd.set(Calendar.HOUR_OF_DAY, foo); //当前点击的时间段 结束时间点
		 * map.put("periodEnd", periodEnd.getTime()); }else{
		 * periodStart.set(Calendar.HOUR_OF_DAY,getPeriodTime(periodStart.get(
		 * Calendar.HOUR_OF_DAY)));//当前时间所在时间段 起始时间点 map.put("periodStart",
		 * periodStart.getTime()); int foo
		 * =periodStart.get(Calendar.HOUR_OF_DAY) + 3;
		 * periodEnd.set(Calendar.HOUR_OF_DAY,getPeriodTime(foo));//当前时间所在时间段
		 * 结束时间点 map.put("periodEnd", periodEnd.getTime()); } //获得当前选择的时间段
		 * //活动开始时间和结束时间在该时段之内 String querySQL =
		 * ConfigCode.getInstance().getCode("seckill.getFrontSeckillList", map);
		 * String queryAllSQL =
		 * ConfigCode.getInstance().getCode("seckill.getFrontSeckillListCount",
		 * map); query.setAllCountString(queryAllSQL);
		 * query.setQueryString(querySQL); query.setParam(map.toArray());
		 */
		request.setAttribute("startTime", startTime);
		PageSupport<SeckillActivity> ps = seckillActivityService.querySeckillActivityList(startTime);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(MobileSeckillFrontPage.MOBILE_SECKILLMOBILE_LIST); //
	}

	/**
	 * 获得活动所在时间段.
	 *
	 * @param cur_hour
	 * @return the period time
	 */
	private Integer getPeriodTime(int cur_hour) {
		if (cur_hour < 9) {
			return 0;
		} else if (9 <= cur_hour && cur_hour < 12) {
			return 9;
		} else if (12 <= cur_hour && cur_hour < 15) {
			return 12;
		} else if (15 <= cur_hour && cur_hour < 18) {
			return 15;
		} else if (18 <= cur_hour && cur_hour < 21) {
			return 18;
		} else if (21 <= cur_hour && cur_hour < 24) {
			return 21;
		} else {
			return 0;
		}

	}

}
