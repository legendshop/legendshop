/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.seckill.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.base.util.SeckillConstant;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.SubTypeEnum;
import com.legendshop.model.constant.SubmitOrderStatusEnum;
import com.legendshop.model.dto.OrderAddParamDto;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.order.AddOrderMessage;
import com.legendshop.model.dto.seckill.SuccessProdDto;
import com.legendshop.model.entity.Invoice;
import com.legendshop.model.entity.SeckillSuccess;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.processor.ProdSnapshotProcessor;
import com.legendshop.seckill.frontendPage.MobileSeckillFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.InvoiceService;
import com.legendshop.spi.service.OrderCartResolverManager;
import com.legendshop.spi.service.OrderUtil;
import com.legendshop.spi.service.SeckillService;
import com.legendshop.spi.service.SubService;
import com.legendshop.spi.service.UserAddressService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.MD5Util;

/**
 * （手机）秒杀订单下单中心.
 *
 */
@Controller
@RequestMapping("/p/seckillMobile/order")
public class SeckillMobileOrderController {

	@Autowired
	private SeckillService seckillService;

	@Autowired
	private UserAddressService userAddressService;

	@Autowired
	private SubService subService;

	@Autowired
	private InvoiceService invoiceService;

	// 订单常用处理工具类
	@Autowired
	private OrderUtil orderUtil;

	// 订单处理类
	@Autowired
	private OrderCartResolverManager orderCartResolverManager;
	
	/** 订单快照 **/
	@Autowired
	private ProdSnapshotProcessor prodSnapshotProcessor;

	/**
	 * 秒杀成功
	 *
	 * @param request
	 * @param response
	 * @param seckillId
	 * @param md5
	 * @param prodId
	 * @param skuId
	 * @return the string
	 */
	@RequestMapping(value = "/{seckillId}/{md5}/{prodId}/{skuId}/success")
	public String success(HttpServletRequest request, HttpServletResponse response, @PathVariable("seckillId") Long seckillId, @PathVariable("md5") String md5,
			@PathVariable long prodId, @PathVariable long skuId) {
		if (AppUtils.isBlank(seckillId) || AppUtils.isBlank(prodId) || AppUtils.isBlank(prodId)) {
			request.setAttribute("message", "访问失败,参数有误");
			return PathResolver.getPath("/seckillError", MobileSeckillFrontPage.VARIABLE);
		}
		if (AppUtils.isBlank(skuId))
			skuId = 0l;
		StringBuilder builder = new StringBuilder();
		builder.append(seckillId).append(prodId).append(skuId);
		if (md5 == null || !md5.equals(MD5Util.toMD5(builder.toString() + Constants._MD5))) {
			request.setAttribute("message", "非法访问");
			return PathResolver.getPath("/seckillError", MobileSeckillFrontPage.VARIABLE);
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		
		SeckillSuccess seckillSuccess = seckillService.getSeckillSucess(user.getUserId(), seckillId, prodId, skuId);
		if (AppUtils.isBlank(seckillSuccess)) {
			request.setAttribute("message", "访问失败,没有秒杀成功或者过期取消资格");
			return PathResolver.getPath("/seckillError", MobileSeckillFrontPage.VARIABLE);
		}
		if (seckillSuccess.getStatus().intValue() == 1) {
			return PathResolver.getRedirectPath("/p/userOrder");
		}
		SuccessProdDto successProdDto = seckillService.findSuccessProdDto(seckillSuccess);
		request.setAttribute("successProd", successProdDto);
		// 设置令牌,防止URL伪造
		String _md5 = MD5Util.toMD5(builder.toString() + Constants._MD5 + "ORDER");
		request.setAttribute("_md5", _md5);
		return PathResolver.getPath("/seckillSuccess", MobileSeckillFrontPage.VARIABLE);
	}

	/**
	 * 跳转秒杀订单
	 *
	 * @param request
	 * @param response
	 * @param seckillId
	 * @param md5
	 * @param prodId
	 * @param skuId
	 * @return the string
	 */
	@RequestMapping(value = "/{seckillId}/{md5}/{prodId}/{skuId}/goOrder")
	public String goOrder(HttpServletRequest request, HttpServletResponse response, @PathVariable("seckillId") Long seckillId, @PathVariable("md5") String md5,
			@PathVariable long prodId, @PathVariable long skuId) {
		StringBuilder builder = new StringBuilder();
		builder.append(seckillId).append(prodId).append(skuId);
		if (md5 == null || !md5.equals(MD5Util.toMD5(builder.toString() + Constants._MD5 + "ORDER"))) {
			request.setAttribute("message", "非法访问");
			return PathResolver.getPath("/seckillError", MobileSeckillFrontPage.VARIABLE);
		}
		com.legendshop.security.model.SecurityUserDetail userDetail = UserManager.getUser(request);
		String userId = userDetail.getUserId();
		SeckillSuccess seckillSuccess = seckillService.getSeckillSucess(userId, seckillId, prodId, skuId);
		if (AppUtils.isBlank(seckillSuccess)) {
			request.setAttribute("message", "访问失败,没有秒杀成功或者过期取消资格");
			return PathResolver.getPath("/seckillError", MobileSeckillFrontPage.VARIABLE);
		}
		if (seckillSuccess.getStatus().intValue() == 1) {
			return PathResolver.getRedirectPath("/p/userOrder");
		}
		/* 根据类型查询订单列表 */
		UserShopCartList shopCartList = queryOrder(seckillSuccess, userDetail.getUsername());
		if (AppUtils.isBlank(shopCartList)) {
			request.setAttribute("message", "访问失败,没有秒杀成功或者过期取消资格");
			return PathResolver.getPath("/seckillError", MobileSeckillFrontPage.VARIABLE);
		}

		// 查出买家的默认发票内容
		Invoice userdefaultInvoice = invoiceService.getDefaultInvoice(userId);
		request.setAttribute("userdefaultInvoice", userdefaultInvoice);
		request.setAttribute("userShopCartList", shopCartList);
		// 设置订单提交令牌,防止重复提交
		Integer token = CommonServiceUtil.generateRandom();
		request.getSession().setAttribute(SeckillConstant.SECKILL_SESSION_TOKEN, token);
		request.setAttribute("seckillId", seckillId);
		request.setAttribute("md5", md5);
		request.setAttribute("prodId", prodId);
		request.setAttribute("skuId", skuId);
		String path = PathResolver.getPath("/seckillOrderDetails", MobileSeckillFrontPage.VARIABLE);
		return path;
	}

	/**
	 * 提交订单
	 *
	 * @param request
	 * @param response
	 * @param seckillId
	 * @param md5
	 * @param prodId
	 * @param skuId
	 * @param paramDto
	 * @return the adds the order message
	 */
	@RequestMapping(value = "/submit/{seckillId}/{md5}")
	public @ResponseBody AddOrderMessage submitOrder(HttpServletRequest request, HttpServletResponse response, @PathVariable("seckillId") Long seckillId,
			@PathVariable("md5") String md5, @RequestParam long prodId, @RequestParam long skuId, OrderAddParamDto paramDto) {

		AddOrderMessage message = new AddOrderMessage();
		if (AppUtils.isBlank(skuId))
			skuId = 0l;
		StringBuilder builder = new StringBuilder();
		builder.append(seckillId).append(prodId).append(skuId);
		if (md5 == null || !md5.equals(MD5Util.toMD5(builder.toString() + Constants._MD5 + "ORDER"))) {
			message.setMessage("非法访问");
			return message;
		}
		SecurityUserDetail userDetail = UserManager.getUser(request);
		String userId = userDetail.getUserId();
		String userName = userDetail.getUsername();

		SeckillSuccess seckillSuccess = seckillService.getSeckillSucess(userId, seckillId, prodId, skuId);
		if (AppUtils.isBlank(seckillSuccess)) {
			message.setMessage("访问失败,没有秒杀成功或者过期取消资格");
			return message;
		}
		if (seckillSuccess.getStatus().intValue() == 1) {
			message.setMessage("访问失败,已下单成功");
			return message;
		}

		// 判断是否重复提交
		Integer sessionToken = (Integer) request.getSession().getAttribute(SeckillConstant.SECKILL_SESSION_TOKEN); // 取session中保存的token
		Integer userToken = Integer.parseInt(paramDto.getToken()); // 取用户提交过来的token进行对比
		if (!userToken.equals(sessionToken)) {
			message.setCode(SubmitOrderStatusEnum.INVALID_TOKEN.value());
			return message;
		}

		/* 根据类型查询订单列表 */
		UserShopCartList userShopCartList = queryOrder(seckillSuccess, userDetail.getUsername());
		if (AppUtils.isBlank(userShopCartList)) {
			message.setCode(SubmitOrderStatusEnum.NOT_PRODUCTS.value());
			return message;
		}

		// 检查金额： 如果促销导致订单金额为负数
		if (userShopCartList.getOrderActualTotal() <= 0) {
			message.setMessage("订单金额为负数,下单失败!");
			return message;
		}

		/** 根据id获取收货地址信息 **/
		UserAddress userAddress = userAddressService.getDefaultAddress(userId);
		if (AppUtils.isBlank(userAddress)) {
			message.setCode(SubmitOrderStatusEnum.NO_ADDRESS.value());
			return message;
		}
		userShopCartList.setUserAddress(userAddress);
		// 处理运费模板
		String result = orderUtil.handlerDelivery(paramDto.getDelivery(), userShopCartList);
		if (!Constants.SUCCESS.equals(result)) {
			message.setCode(result);
			return message;
		}

		// 验证成功，清除令牌
		request.getSession().removeAttribute(SeckillConstant.SECKILL_SESSION_TOKEN);
		userShopCartList.setUserId(userId);
		userShopCartList.setUserName(userName);
		userShopCartList.setInvoiceId(paramDto.getInvoiceId());
		userShopCartList.setUserAddress(userAddress);
		userShopCartList.setPayManner(2);
		ShopCarts shopCarts = userShopCartList.getShopCarts().get(0);
		shopCarts.setRemark(paramDto.getRemarkText());

		/**
		 * 处理订单
		 */
		List<String> subIds = null;
		try {
			subIds = orderCartResolverManager.addOrder(CartTypeEnum.SECKILL, userShopCartList);
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof BusinessException) {
				message.setCode(SubmitOrderStatusEnum.ERR.value());
				message.setMessage(e.getMessage());
				return message;
			}
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("下单失败,请联系商城管理员或商城客服");
			return message;
		}
		if (AppUtils.isBlank(subIds)) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("下单失败,请联系商城管理员或商城客服");
			return message;
		} else {
			// 发送网站快照备份
			prodSnapshotProcessor.process(subIds);
		}

		// 标记回收
		userShopCartList = null;
		// 更新session购物车数量
		subService.evictUserShopCartCache(CartTypeEnum.SECKILL.toCode(), userId);
		// 更新session购物车数量
		StringBuffer url = new StringBuffer();
		url.append("/p/orderPay?subNums=");
		for (int i = 0; i < subIds.size(); i++) {
			url.append(subIds.get(i)).append(",");
		}
		url.append("&subType=" + SubTypeEnum.SECKILL.value());
		String path = url.substring(0, url.length() - 1);
		message.setResult(true);
		message.setUrl(path);
		return message;
	}

	/**
	 * 查找订单
	 *
	 * @param seckillSuccess the seckill success
	 * @param userName the user name
	 * @return the user shop cart list
	 */
	private UserShopCartList queryOrder(SeckillSuccess seckillSuccess,String userName){
		UserShopCartList userShopCartList = subService.findUserShopCartList(CartTypeEnum.SECKILL.toCode(), seckillSuccess.getUserId());
		if(AppUtils.isBlank(userShopCartList)){
			/*  根据类型查询订单列表  */
			Map<String,Object> params=new HashMap<String,Object>();
			params.put("userId", seckillSuccess.getUserId());
			params.put("userName", userName);
			params.put("seckillId", seckillSuccess.getId());
			params.put("prodId", seckillSuccess.getProdId());
			params.put("skuId", seckillSuccess.getSkuId());
			UserShopCartList shopCartList=orderCartResolverManager.queryOrders(CartTypeEnum.SECKILL,params);
			if(AppUtils.isNotBlank(shopCartList)){
				subService.putUserShopCartList(CartTypeEnum.SECKILL.toCode(), seckillSuccess.getUserId(), shopCartList);
			}
			return shopCartList;
		}
		return userShopCartList;
	}
}
