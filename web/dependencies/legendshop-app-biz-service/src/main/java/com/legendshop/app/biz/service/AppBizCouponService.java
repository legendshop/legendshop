package com.legendshop.app.biz.service;

import com.legendshop.biz.model.dto.AppBizCouponDto;
import com.legendshop.biz.model.dto.AppBizShowCouponDto;
import com.legendshop.biz.model.dto.AppBizUserCouponDto;
import com.legendshop.model.dto.app.AppPageSupport;

/**
 * @author 27158
 */
public interface AppBizCouponService {

	/**
	 * app商家获取优惠卷列表
	 * @return
	 */
	AppPageSupport<AppBizShowCouponDto> queryCoupon(String curPageNO, Integer status, Long shopId, String couponName);

	/***
	 *  app商家添加优惠卷
	 * @param appBizCouponDto
	 * @param shopId
	 * @return
	 */
	int saveByCouponAndShopId(AppBizCouponDto appBizCouponDto, String userName, Long shopId, String userId);

	/**
	 * 查看优惠卷详情
	 * @param couponId
	 * @return
	 */
	AppBizShowCouponDto getCoupon(Long couponId);

	/**
	 * app商家删除优惠卷列表
	 * @param couponId
	 */
	void shopDeleteCoupon(Long couponId);

	/**
	 * 获取用户优惠券列表
	 * @param curPageNO 当前页码
	 * @param pageSize 获取条数
	 * @param couponId 优惠券ID
	 * @return
	 */
	AppPageSupport<AppBizUserCouponDto> queryUserCouponList(String curPageNO,Integer pageSize, Long couponId);

	/**
	 * 获取用户优惠券详情
	 * @param userCouponId
	 * @return
	 */
	AppBizUserCouponDto getUserCouponById(Long userCouponId);
}
