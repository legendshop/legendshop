/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.biz.controller;

import com.legendshop.app.biz.dto.LoginUserTimes;
import com.legendshop.app.biz.service.AppBizTokenService;
import com.legendshop.app.biz.service.impl.AppBizLoginUtil;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.base.util.ValidationUtil;
import com.legendshop.framework.cache.LegendCacheManager;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.dto.app.AppTokenDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.AppToken;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.User;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.SMSLogService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtil;
import com.legendshop.util.DateUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.support.SimpleValueWrapper;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.Date;

/**
 * 商家登录相关接口
 */
@RestController
@Api(tags="商家登录",value="商家登录相关接口")
public class AppBizTokenController {
	
	private static Logger log = LoggerFactory.getLogger(AppBizTokenController.class);
	
	@Autowired
	private AppBizTokenService appBizTokenService;

	@Autowired
	private UserDetailService userDetailService;
	
	/** The legend cache manager. */
	@Autowired
	private LegendCacheManager legendCacheManager;
	
	private Cache INVALID_BIZ_LOGIN_USER_CACHE = null;

	@Autowired
	private AppBizLoginUtil appBizLoginUtil;
	
    @Autowired
    private PasswordEncoder passwordEncoder;
    
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private SMSLogService smsLogService;


	/**
	 * 商家账号密码登录
	 * @param loginName 登录名字： 可以是手机，昵称或者邮件
	 * @param password 密码
	 * @param platform 登录平台类型
	 * @return
	 */
	@ApiOperation(value = "商家账号密码登录", httpMethod = "POST", notes = "用户账号密码登录, 如果登录失败则返回错误消息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "loginName", value = "登录账号, 可以是手机号, 用户名, 邮箱", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "password", value = "登录密码", required = true, dataType = "String"),
		@ApiImplicitParam(paramType="query", name = "platform", value = "登录平台类型, H5: 手机网页, ANDROID: 安卓APP, IOS: 苹果APP, MP: 小程序", required = false, dataType = "string"),

	})
	@PostMapping(value="/login")
	public ResultDto<AppTokenDto> login(
			@RequestParam(required = true) String loginName, 
			@RequestParam(required = true)  String password,
			@RequestParam String platform) {
			try{
				

				boolean canuserLogin = isUserCanLogin(loginName);
				if(!canuserLogin){
					return ResultDtoManager.fail(-1, "登录错误次数过多, 请一小时后再重试!");
				}
				
				String name = userDetailService.convertUserLoginName(loginName);
				if(AppUtils.isBlank(name)){

					//记录用户的登录出错次数
					addUserTimes(loginName);
					return ResultDtoManager.fail(-1, "用户不存在");
				}
			
				User user = userDetailService.getUserByName(name);
				if(null == user || "0".equals(user.getEnabled())){

					//记录用户的登录出错次数
					addUserTimes(loginName);
					return ResultDtoManager.fail(-1, "用户不存在或已被冻结");
				}

				if (!passwordEncoder.matches(password, user.getPassword())) {

					//记录用户的登录出错次数
					addUserTimes(loginName);
					return ResultDtoManager.fail(-1, "密码不正确");
				}

				if (AppUtils.isBlank(user.getShopId())){

					//记录用户的登录出错次数
					addUserTimes(loginName);
					return ResultDtoManager.fail(-1, "该用户未开店，请登录商家账号");
				}

				ShopDetail shopDetail = shopDetailService.getShopDetailById(user.getShopId());
				if (AppUtils.isBlank(shopDetail)){

					//记录用户的登录出错次数
					addUserTimes(loginName);
					return ResultDtoManager.fail(-1, "该店铺不存在或已被系统删除，请联系平台客服");
				}

				if(ShopStatusEnum.OFFLINE.value().equals(shopDetail.getStatus())){

					//记录用户的登录出错次数
					addUserTimes(loginName);
					return ResultDtoManager.fail(-1, "店铺已下线");
				}
				if(ShopStatusEnum.AUDITING.value().equals(shopDetail.getStatus())){

					//记录用户的登录出错次数
					addUserTimes(loginName);
					return ResultDtoManager.fail(-1, "该店铺待审核中，请联系平台客服");
				}
				if(ShopStatusEnum.DENY.value().equals(shopDetail.getStatus())){

					//记录用户的登录出错次数
					addUserTimes(loginName);
					return ResultDtoManager.fail(-1, "该店铺开店申请被拒绝，请联系平台客服");
				}
				if(ShopStatusEnum.CLOSED.value().equals(shopDetail.getStatus())){

					//记录用户的登录出错次数
					addUserTimes(loginName);
					return ResultDtoManager.fail(-1, "该店铺已被平台关闭，请联系平台客服");
				}

				AppToken token= appBizLoginUtil.loginToken(user.getId(), user.getUserName(), null, platform, "1.0",user.getShopId());
				if(AppUtils.isBlank(token)){
					return ResultDtoManager.fail(-1, "登录失败, 请联系管理人员");
				}
				
				return ResultDtoManager.success(token.toDto());
			}catch (Exception e) { 
				log.error("用户登录异常, 登录参数: loginName: {}; ---->> exception: {}", loginName, e);
				return ResultDtoManager.fail(-1, "登录失败, 请联系管理人员");
			}
	}
	
	/**
	 * 手机短信验证码登录
	 * @param mobile 手机号码
	 * @param mobileCode 手机短信验证码
	 * @param deviceId 
	 * @param platform
	 * @return
	 */
	@ApiOperation(value = "手机短信验证码登录", httpMethod = "POST", notes = "手机短信验证码登录",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "mobile", value = "手机号码", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "mobileCode", value = "验证码", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "deviceId", value = "手机的唯一设备", required = false, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "platform", value = "设备平台类型, H5: 手机网页, ANDROID: 安卓APP, IOS: 苹果APP, MP: 小程序", required = true, dataType = "string"),

	})
	@PostMapping(value="/smsLogin")
	public ResultDto<AppTokenDto> smsLogin(
			@RequestParam String mobile,
		    @RequestParam  String mobileCode,
		    @RequestParam String deviceId,
		    @RequestParam String platform) {
		
		try{

			// 检查手机号码的格式
			boolean isMobile = ValidationUtil.checkPhone(mobile);
			if(!isMobile){
				return ResultDtoManager.fail(-1, "请输入正确的手机号码");
			}
			
			// 检查登录次数是够满足
			boolean canuserLogin = isUserCanLogin(mobile);
			if(!canuserLogin){
				return ResultDtoManager.fail(-1, "登录错误次数过多, 请一小时后再重试!");
			}

			// 检查验证码是否正确
			boolean mobileResult=userDetailService.verifyMobileCode(mobile, mobileCode);
			if(!mobileResult){

				// 记录用户的登录出错次数
				addUserTimes(mobile);
				return ResultDtoManager.fail(-1, "验证码不正确");
			}
			
			User user =userDetailService.getUserByMobile(mobile);

			if (AppUtils.isBlank(user)){
				return ResultDtoManager.fail(-1, "此手机号未注册");
			}

			if("0".equals(user.getEnabled())){
				return ResultDtoManager.fail(-1, "用户已被冻结,请联系平台客服");
			}

			if (AppUtils.isBlank(user.getShopId())){

				// 记录用户的登录出错次数
				addUserTimes(mobile);
				return ResultDtoManager.fail(-1, "该用户未开店，请登录商家账号");
			}

			ShopDetail shopDetail = shopDetailService.getShopDetailById(user.getShopId());
			if (AppUtils.isBlank(shopDetail)){

				//记录用户的登录出错次数
				addUserTimes(mobile);
				return ResultDtoManager.fail(-1, "该店铺不存在或已被系统删除，请联系平台客服");
			}

			if(ShopStatusEnum.OFFLINE.value().equals(shopDetail.getStatus())){

				//记录用户的登录出错次数
				addUserTimes(mobile);
				return ResultDtoManager.fail(-1, "店铺已下线");
			}
			if(ShopStatusEnum.AUDITING.value().equals(shopDetail.getStatus())){

				//记录用户的登录出错次数
				addUserTimes(mobile);
				return ResultDtoManager.fail(-1, "该店铺待审核中，请联系平台客服");
			}
			if(ShopStatusEnum.DENY.value().equals(shopDetail.getStatus())){

				//记录用户的登录出错次数
				addUserTimes(mobile);
				return ResultDtoManager.fail(-1, "该店铺开店申请被拒绝，请联系平台客服");
			}
			if(ShopStatusEnum.CLOSED.value().equals(shopDetail.getStatus())){

				//记录用户的登录出错次数
				addUserTimes(mobile);
				return ResultDtoManager.fail(-1, "该店铺已被平台关闭，请联系平台客服");
			}

			//登录, 如果用户不存在,则新建token, 否则修改token
			AppToken token= appBizLoginUtil.loginToken(user.getId(), user.getUserName(), null, platform, "1.0",user.getShopId());
			if(AppUtils.isBlank(token)){
				return ResultDtoManager.fail(-1, "登录失败, 请联系平台客服");
			}
			
			return ResultDtoManager.success(token.toDto());
		}catch(Exception e){
			log.error("用户登录异常, 登录参数: mobile: {},mobileCode:{}, deviceId: {}, platform: {}; ---->> exception: {}", mobile,mobileCode,deviceId, platform);
			return ResultDtoManager.fail(-1, "登录失败, 请联系平台客服");
		}
	}


	
	/**
	 * 退出登录
	 * @return
	 */
	@ApiOperation(value = "退出登录", httpMethod = "POST", notes = "退出,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/s/logout")
	public ResultDto<String> delete(AppTokenDto dto) {
		LoginedUserInfo appToken = loginedUserService.getUser();
		if (appToken != null) {
			boolean result = appBizTokenService.deleteAppTokeByshopId(appToken.getUserId(), appToken.getShopId());
			if (result) {
				return ResultDtoManager.success(Constants.SUCCESS);
			}
		}
		return ResultDtoManager.success(Constants.FAIL);
	}

	/**
	 * 刷新token，增强有效时间
	 * @param dto
	 * @return
	 */
	@ApiOperation(value = "刷新token", httpMethod = "POST", notes = "刷新token，增强有效时间",produces = MediaType.APPLICATION_JSON_UTF8_VALUE, response=AppTokenDto.class)
	@PostMapping(value="/s/refresh")
	public ResultDto<AppTokenDto> refresh(AppTokenDto dto) {
		LoginedUserInfo token = loginedUserService.getUser();
		if (token != null) {
			AppToken appToken = appBizTokenService.getAppToken(token.getUserId(),token.getShopId(),dto.getPlatform());
			if (appToken != null) {
				// 获得随机码
				String securiyCode = CommonServiceUtil.getRandomString(8);
				appToken.setSecuriyCode(securiyCode);
				appToken.setStartDate(new Date());
				appBizTokenService.updateAppToken(appToken);
				return ResultDtoManager.success(appToken.toDto());
			}
		}
		return ResultDtoManager.fail(0,"用户没有登录");
	}


	/**
	 * 找回密码
	 * @param code
	 * @param newPwd
	 * @param mobile
	 * @return
	 */
	@ApiOperation(value = "找回密码", httpMethod = "POST", notes = "找回密码,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "code", value = "手机验证码", required = true, dataType = "string"),
			@ApiImplicitParam(paramType="query", name = "newPwd", value = "新密码", required = false, dataType = "string"),
			@ApiImplicitParam(paramType="query", name = "mobile", value = "手机号码(1:PC 2:android 3:wap 4:IOS)", required = true, dataType = "string")
	})
	@PostMapping(value="/forgetPwd")
	public  ResultDto<String> forgetPwd(@RequestParam String code, @RequestParam String newPwd, @RequestParam String mobile){

		if(!smsLogService.verifyCodeAndClear(mobile, code)){
			return ResultDtoManager.fail(0, "验证码错误");
		}
		User user = userDetailService.getUserByMobile(mobile);
		if(user==null){
			return ResultDtoManager.fail(0, "手机号不存在");
		}else{
			user.setPassword(passwordEncoder.encode(newPwd));
			userDetailService.updateUser(user);
			return ResultDtoManager.success(Constants.SUCCESS);
		}
	}
	

	/**
	 * 检查用户是否重复输入错误的密码,如果输入错误10次以上要锁定密码一个小时
	 * @return
	 */
	private boolean isUserCanLogin(String loginName){
		SimpleValueWrapper valueWrapper = (SimpleValueWrapper) getCache().get(loginName);
        if(valueWrapper==null){
        	return true;
        }
        LoginUserTimes times = (LoginUserTimes)valueWrapper.get();
        if(times != null){

			// 错误10次以上
        	if(times.getErrorNum() >= 10){
        		Date oneHourLater = DateUtil.add(times.getLoginTime(), Calendar.HOUR, 1);

				// 还没有超过一个小时不能登录
        		if(oneHourLater.after(new Date())){
        			return false;
        		}
        	}
        }
		return true;
	}
	
	private void addUserTimes(String loginName){
		SimpleValueWrapper valueWrapper = (SimpleValueWrapper) getCache().get(loginName);
        if(valueWrapper==null){
        	// 第一次加入
        	LoginUserTimes times = new LoginUserTimes(new Date(),1);    
        	getCache().put(loginName, times);
        }else{
        	LoginUserTimes times = (LoginUserTimes)valueWrapper.get();
        	times.setErrorNum(times.getErrorNum() + 1);
        	times.setLoginTime(new Date());

			// 设置回去缓存
			INVALID_BIZ_LOGIN_USER_CACHE.put(loginName, times);
        }
	}
	
	/**
	 * 得到缓存
	 */
	private Cache getCache(){
		if(INVALID_BIZ_LOGIN_USER_CACHE == null){
			INVALID_BIZ_LOGIN_USER_CACHE = legendCacheManager.getCache("InvalidBizLoginUsers");
		}
		return INVALID_BIZ_LOGIN_USER_CACHE;
	}
	
	
}
