/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.biz.config;


import com.legendshop.biz.model.dto.ShopTokenContext;
import com.legendshop.model.dto.app.AppTokenDto;

import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.spi.service.LoginedUserService;

/**
 *  获取 默认的登录用户信息
 *  @author linzh
 */
public class DefaultBizLoginedUserServiceImpl implements LoginedUserService {

	/**
	 * 获取登录用户的信息
	 */
	@Override
	public LoginedUserInfo getUser() {
		AppTokenDto appTokenDto = ShopTokenContext.getToken();
		if(appTokenDto == null) {
			return null;
		}
		LoginedUserInfo user = new LoginedUserInfo(appTokenDto.getUserId(), appTokenDto.getUserName(), appTokenDto.getShopId(),appTokenDto.getAccessToken());
		return user;
	}

}