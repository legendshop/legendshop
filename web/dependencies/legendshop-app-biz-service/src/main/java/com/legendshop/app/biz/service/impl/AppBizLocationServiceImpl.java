package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.service.AppBizLocationService;
import com.legendshop.biz.model.dto.KeyValueDto;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.spi.service.LocationService;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
/**
 * @author 27158
 */
@Service("appBizLocationService")
public class AppBizLocationServiceImpl implements AppBizLocationService {

	@Autowired
	private LocationService locationService;

	@Override
	public List<KeyValueDto> loadProvinces() {
		List<KeyValueEntity> provinces = locationService.loadProvinces();
		return convertKeyValueDto(provinces);
	}

	@Override
	public List<KeyValueDto> loadCities(Integer provinceid) {
		List<KeyValueEntity> cities = locationService.loadCities(provinceid);
		return convertKeyValueDto(cities);
	}

	@Override
	public List<KeyValueDto> loadAreas(Integer cityid) {
		List<KeyValueEntity> areas = locationService.loadAreas(cityid);
		return convertKeyValueDto(areas);
	}

	private List<KeyValueDto> convertKeyValueDto(List<KeyValueEntity> keyValueEntityList){
		if(AppUtils.isNotBlank(keyValueEntityList)){
			List<KeyValueDto> keyValueDtoList = new ArrayList<KeyValueDto>();
			for (KeyValueEntity keyValueEntity : keyValueEntityList) {
				KeyValueDto keyValueDto = new KeyValueDto();
				keyValueDto.setValue(keyValueEntity.getValue());
				keyValueDto.setKey(keyValueEntity.getKey());
				keyValueDtoList.add(keyValueDto);
			}
			return keyValueDtoList;
		}

		return null;
	}
}
