package com.legendshop.app.biz.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 分类DTO
 */
@ApiModel("分类DTO")
public class AppbizDecorateShopCategoryDto implements Serializable  {
	
	/**  */
	private static final long serialVersionUID = 8273611093405866310L;

	/** 分类ID */
	@ApiModelProperty("分类ID")
	private Long id;
	
	/** 分类名称  */
	@ApiModelProperty("分类名称")
	private String name;
	
	/** 父类ID */
	@ApiModelProperty("父类ID")
	private Long parentId;
	
	/** 分类层级  */
	@ApiModelProperty("分类层级")
	private Integer grade;
	
	/** 子分类*. */
	@ApiModelProperty("子分类")
	private List<AppbizDecorateShopCategoryDto> subCatList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}


	public void addSubCatList(AppbizDecorateShopCategoryDto categoryDto) {
		if(subCatList == null){
			subCatList = new ArrayList<AppbizDecorateShopCategoryDto>();
		}
		subCatList.add(categoryDto);
	}

	public List<AppbizDecorateShopCategoryDto> getSubCatList() {
		return subCatList;
	}

	public void setSubCatList(List<AppbizDecorateShopCategoryDto> subCatList) {
		this.subCatList = subCatList;
	}
}