package com.legendshop.app.biz.service;

import com.legendshop.biz.model.dto.AppBizProdArrivalInformDto;
import com.legendshop.model.dto.app.AppPageSupport;

/**
 * 商家端 到货通知服务接口
 * @author linzh
 */
public interface AppBizProdArrivalService {


	/**
	 * 获取到货通知列表
	 * @param curPageNO
	 * @param shopId
	 * @param productName
	 * @return
	 */
	AppPageSupport<AppBizProdArrivalInformDto> queryProdArrivalList(String curPageNO, Long shopId, String productName);

	/**
	 * 获取通知用户列表
	 * @param curPageNO
	 * @param shopId
	 * @param skuId
	 * @return
	 */
	AppPageSupport<AppBizProdArrivalInformDto> queryProdArrivaUserList(String curPageNO, Long shopId, String skuId);
}
