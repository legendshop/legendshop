package com.legendshop.app.biz.controller;

import com.legendshop.app.biz.service.AppBizProductService;
import com.legendshop.app.biz.service.AppBizShippingActiveService;
import com.legendshop.biz.model.dto.AppBizMarketingProdDto;
import com.legendshop.biz.model.dto.AppBizShippingActiveDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.entity.ShippingActive;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.ShippingActiveService;
import com.legendshop.util.AppUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * app包邮活动控制器
 * @author 27158
 */
@RestController
@Api(tags="包邮活动控制器",value="活动查看、添加、编辑")
public class AppBizShippingActiveController {
	/**
	 * The log.
	 */
	private final Logger LOGGER = LoggerFactory.getLogger(AppBizShippingActiveController.class);
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private AppBizShippingActiveService appBizShippingActiveService;
	@Autowired
	private ShippingActiveService shippingActiveService;
	@Autowired
	private AppBizProductService appBizProductService;
	/**
	 * 获取包邮活动列表
	 *
	 * @return
	 */
	@ApiOperation(value = "获取包邮活动列表", httpMethod = "POST", notes = "获取包邮活动列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query",name = "curPageNO",value="分页",dataType = "string",required = false),
			@ApiImplicitParam(paramType = "query",name = "name",value="活动名称",dataType = "string",required = false),
			@ApiImplicitParam(paramType = "query", name = "searchType", value = "类型，所有：ALL  未开始：NOT_STARTED 进行中:ONLINE" +
					"已结束:FINISHED 已撤销:EXPIRED ", dataType = "string", required = true),
	})
	@PostMapping("/s/ShopShipping")
	public ResultDto<AppPageSupport<AppBizShippingActiveDto>> ShopShipping(@RequestParam(required = false) String curPageNO,@RequestParam (required = false) String name,@RequestParam (required = false) String searchType) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		AppPageSupport<AppBizShippingActiveDto> ps=appBizShippingActiveService.queryShippingActiveList(curPageNO,shopId,name,searchType);

		return ResultDtoManager.success(ps);
	}
	/**
	 * 获取包邮的商品列表
	 */
	@ApiOperation(value = "获取包邮的商品列表", httpMethod = "POST", notes = "获取包邮的商品列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query",name = "curPageNO",value="分页",dataType = "string"),
			@ApiImplicitParam(paramType = "query",name = "name",value="商品名称",dataType = "string"),
			@ApiImplicitParam(paramType = "query", name = "shopCatId", value = "店铺分类ID  ", dataType = "long"),
			@ApiImplicitParam(paramType = "query", name = "shopCatType", value = "店铺分类级别 1 ， 2 ，3 ", dataType = "integer")
	})
	@PostMapping("/s/getShopShippingProds")
	public ResultDto<AppPageSupport<AppBizMarketingProdDto>> getShopShippingProds(@RequestParam(required = false) String curPageNO,@RequestParam(required = false) String name,
																				  @RequestParam(required = false) Long shopCatId ,
																				  @RequestParam(required = false) Integer shopCatType ) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		AppPageSupport<AppBizMarketingProdDto>  ps = appBizProductService.getShopShippingProds(curPageNO, shopId,name,shopCatId,shopCatType);
		return ResultDtoManager.success(ps);
	}

	/**
	 * 添加包邮活动
	 *
	 * @return
	 */
	@ApiOperation(value = "添加包邮活动", httpMethod = "POST", notes = "添加包邮活动", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/s/saveShopShipping")
	public ResultDto<String> saveShopShipping(@RequestBody AppBizShippingActiveDto appBizShippingActiveDto) {
		Long shopId  = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		if (AppUtils.isBlank(appBizShippingActiveDto)) {
			return ResultDtoManager.fail(-1, "包邮活动参数不足");
		}
		boolean b = validateMarketingDto(appBizShippingActiveDto);
		if(!b){
			return ResultDtoManager.fail(-1, "包邮活动参数不足");
		}
		appBizShippingActiveDto.setShopId(shopId);
		appBizShippingActiveService.saveShippingActive(appBizShippingActiveDto);
		return ResultDtoManager.success();
	}

	/**
	 * 删除包邮活动
	 *
	 * @return
	 */
	@ApiOperation(value = "删除包邮活动", httpMethod = "POST", notes = "删除包邮活动", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query",name = "id",value="包邮活动ID",dataType = "long",required = true)
	})
	@PostMapping("/s/deleteShopShipping")
	public ResultDto<String> deleteShopShipping(@RequestParam Long id) {
		Long shopId  = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		ShippingActive shippingActive = shippingActiveService.getShippingActiveById(id, shopId);
		if (AppUtils.isBlank(shippingActive)){
			return ResultDtoManager.fail(-1,"没有该活动");
		}
		if (shippingActive.getStatus().equals(1)){
			return ResultDtoManager.fail(-1,"该活动正在进行中不能删除");
		}
		shippingActiveService.deleteShippingActive(shippingActive);
		return ResultDtoManager.success();
	}
	/**
	 * 撤销包邮活动
	 *
	 * @return
	 */
	@ApiOperation(value = "撤销包邮活动", httpMethod = "POST", notes = "撤销包邮活动", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query",name = "id",value="包邮活动ID",dataType = "long",required = true)
	})
	@PostMapping("/s/undoShopShipping")
	public ResultDto<String> undoShopShipping(@RequestParam Long id) {
		Long shopId  = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		ShippingActive shippingActive = shippingActiveService.getShippingActiveById(id, shopId);
		if (AppUtils.isBlank(shippingActive)){
			return ResultDtoManager.fail(-1,"没有该活动");
		}
		shippingActive.setStatus(false);
		shippingActiveService.updateShippingActive(shippingActive);
		return ResultDtoManager.success();
	}

	/**
	 * 查看包邮活动详情
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "查看包邮活动详情", httpMethod = "POST", notes = "查看包邮活动详情", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "query",name = "id",value = "包邮活动ID",dataType = "long",required = true)
	@PostMapping("/s/showShopShipping")
	public ResultDto<AppBizShippingActiveDto> showShopShipping(@RequestParam Long id) {

		Long shopId  = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		AppBizShippingActiveDto appBizShippingActiveDto=appBizShippingActiveService.getShippingActiveById(id);
		if (AppUtils.isBlank(appBizShippingActiveDto)){
			return  ResultDtoManager.fail(-1, "活动异常");
		}

		return ResultDtoManager.success(appBizShippingActiveDto);
	}


	/**
	 * 编辑包邮活动
	 * @param id
	 * @param name
	 * @param prodIds
	 * @return
	 */
	@ApiOperation(value = "编辑包邮活动", httpMethod = "POST", notes = "编辑包邮活动", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query",name = "id",value = "包邮活动ID",dataType = "long",required = true),
			@ApiImplicitParam(paramType = "query",name = "name",value = "包邮活动名称",dataType = "string",required = true),
			@ApiImplicitParam(paramType = "query",name = "prodIds",value = "包邮活动商品id集合",dataType = "long", allowMultiple = true)
	})
	@PostMapping("/s/updateShopShipping")
	public ResultDto<String> updateShopShipping( @RequestParam Long id,@RequestParam String name,@RequestParam(required = false)  Long [] prodIds) {

		Long shopId  = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		if (AppUtils.isBlank(id) || AppUtils.isBlank(name)) {
			return ResultDtoManager.fail(-1, "包邮活动参数不足");
		}
		int i = shippingActiveService.appUpdateShipping(id, name,prodIds);
		if (i==1){
			return ResultDtoManager.success();
		}
		return ResultDtoManager.fail(-1,"包邮活动修改失败");
	}


	/**
	 * 参数校验
	 * @param dto
	 * @return
	 */
	private boolean validateMarketingDto(AppBizShippingActiveDto dto) {
		if(AppUtils.isBlank(dto.getName())){
			return false;
		}
		if (AppUtils.isBlank(dto.getFullType())){
			return false;
		}
		if(AppUtils.isBlank(dto.getReduceType())){
			return false;
		}
		if(AppUtils.isBlank(dto.getFullValue())){
			return false;
		}
		if (AppUtils.isBlank(dto.getStartDate())){
			return false;
		}
		if(AppUtils.isBlank(dto.getEndDate())){
			return false;
		}
		if (dto.getStartDate().getTime()>=dto.getEndDate().getTime()){
			LOGGER.info("[活动开始时间不能大于结束时间]");
			return false;
		}
		if(dto.getFullType().equals(1)){
//			部分商品
			if (AppUtils.isBlank(dto.getProdIds())){
				LOGGER.info("[商品ID 不能为 null]");
				return false;
			}
		}
		return true;
	}

}
