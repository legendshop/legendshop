package com.legendshop.app.biz.service;

import com.legendshop.biz.model.dto.AppBizUserDetailDto;

import java.util.List;

/**
 * @author 27158
 */
public interface AppBizUserDetailService {
	/**
	 * 获取用户信息详情
	 * @param userId the user id
	 * @return the user detail
	 */
	AppBizUserDetailDto getUserDetail(String userId);

	/**
	 * 修改商家用户呢称
	 * @param userId
	 * @param nickName
	 * @return
	 */
	int updateNickName(String userId, String nickName);

	/**
	 * 修改商家用户头像
	 * @param userId
	 * @param portraitPic
	 * @return
	 */
	int updatePortrait(String userId, String portraitPic);

	/**
	 * 根据手机号匹配用户手机号
	 * @param userPhone
	 * 	 @param userId
	 */
	List<AppBizUserDetailDto> getUserDetailByPhone(String userPhone, String userId);
}
