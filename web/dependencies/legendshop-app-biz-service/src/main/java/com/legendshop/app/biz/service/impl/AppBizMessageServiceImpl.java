package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.service.AppBizMessageService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.biz.model.dto.msg.AppBizSiteInformationDto;
import com.legendshop.business.dao.MessageDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.MsgStatusEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.siteInformation.SiteInformation;
import com.legendshop.spi.service.MessageService;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 商家端消息箱相关服务实现
 * @author linzh
 */
@Service("appBizMessageService")
public class AppBizMessageServiceImpl implements AppBizMessageService {

	@Autowired
	private MessageService messageService;

	@Autowired
	private MessageDao messageDao;

	@Autowired
	private UserDetailDao userDetailDao;


	/**
	 * 获取用户普通消息详情
	 * @param msgId 消息Id
	 * @param userName 当前登录用户名
	 * @return
	 */
	@Override
	public AppBizSiteInformationDto getMsgDetail(Long msgId, String userName) {

		SiteInformation siteInformation = messageService.getMsgText(userName, msgId, true);

		if(null == siteInformation){
			return null;
		}

		siteInformation.setMsgId(msgId);

		String sendNickName = userDetailDao.getNickNameByUserName(siteInformation.getSendName());

		if(AppUtils.isNotBlank(sendNickName)){
			siteInformation.setSendName(sendNickName);
		}

		AppBizSiteInformationDto msgDetail = convertToAppSiteInformationDto(siteInformation);

		return msgDetail;
	}

	/**
	 * 获取用户系统通知消息详情
	 * @param msgId 消息Id
	 * @param userName 当前登录用户名
	 * @return
	 */
	@Override
	public AppBizSiteInformationDto getSystemMsgDetail(Long msgId, String userName) {

		SiteInformation siteInformation = messageService.getMsgText(userName, msgId, false);
		if(null == siteInformation){
			return null;
		}

		String sendNickName = userDetailDao.getNickNameByUserName(siteInformation.getSendName());
		if(AppUtils.isNotBlank(sendNickName)){
			siteInformation.setSendName(sendNickName);
		}

		siteInformation.setMsgId(msgId);

		AppBizSiteInformationDto msgDetail = convertToAppSiteInformationDto(siteInformation);

		//更新系统通知为已读状态
		messageService.updateSystemMsgStatus(userName, msgId, MsgStatusEnum.READED.value());

		return msgDetail;
	}

	/**
	 * 获取系统消息列表
	 * @param curPageNO 当前页码
	 * @param gradeId 等级ID
	 * @param userName 用户名
	 * @param userDetail 用户详情
	 * @return
	 */
	@Override
	public AppPageSupport<AppBizSiteInformationDto> querySystemMessages(String curPageNO, Integer gradeId, String userName, UserDetail userDetail) {

		PageSupport<SiteInformation> ps = messageDao.querySimplePage(curPageNO,gradeId,userName,userDetail);
		List<SiteInformation> siteInformationList = ps.getResultList();
		List<SiteInformation> newList = new ArrayList<SiteInformation>();

		for(SiteInformation siteInformation : siteInformationList){
			String htmlStr = siteInformation.getText();

			//定义script的正则表达式
			String regEx_script="<script[^>]*?>[\\s\\S]*?<\\/script>";

			//定义style的正则表达式
			String regEx_style="<style[^>]*?>[\\s\\S]*?<\\/style>";

			//定义HTML标签的正则表达式
			String regEx_html="<[^>]+>";

			Pattern p_script= Pattern.compile(regEx_script,Pattern.CASE_INSENSITIVE);
			Matcher m_script=p_script.matcher(htmlStr);

			//过滤script标签
			htmlStr=m_script.replaceAll("");

			Pattern p_style=Pattern.compile(regEx_style,Pattern.CASE_INSENSITIVE);
			Matcher m_style=p_style.matcher(htmlStr);

			//过滤style标签
			htmlStr=m_style.replaceAll("");

			Pattern p_html=Pattern.compile(regEx_html,Pattern.CASE_INSENSITIVE);
			Matcher m_html=p_html.matcher(htmlStr);

			//过滤html标签
			htmlStr = m_html.replaceAll("");
			htmlStr = htmlStr.replaceAll("<img[^>]*/>", " ");
			siteInformation.setText(htmlStr);
			newList.add(siteInformation);
		}
		ps.setResultList(newList);

		AppPageSupport<AppBizSiteInformationDto> ps2 = convertToAppPageSupport(ps);

		return ps2;
	}

	/**
	 * 获取站内信列表
	 * @param curPageNO 当前页码
	 * @param userName  用户名
	 * @return
	 */
	@Override
	public AppPageSupport<AppBizSiteInformationDto> queryInboxMsg(String curPageNO, String userName) {
		PageSupport<SiteInformation> ps = messageDao.querySimplePage(curPageNO,userName);
		AppPageSupport<AppBizSiteInformationDto> ps2 = convertToAppPageSupport(ps);
		return ps2;
	}

	private  AppPageSupport<AppBizSiteInformationDto> convertToAppPageSupport(PageSupport<SiteInformation> ps) {

		return PageUtils.convertPageSupport(ps, new PageUtils.ResultListConvertor<SiteInformation, AppBizSiteInformationDto>() {

			@Override
			public List<AppBizSiteInformationDto> convert(List<SiteInformation> form) {

				List<AppBizSiteInformationDto> list = new ArrayList<AppBizSiteInformationDto>();
				for(SiteInformation siteInformation : form){
					AppBizSiteInformationDto appSiteInformationDto  = convertToAppSiteInformationDto(siteInformation);
					list.add(appSiteInformationDto);
				}

				return list;
			}

		});
	}

	/**
	 * 转换DTO
	 * @param siteInformation
	 * @return
	 */
	private AppBizSiteInformationDto convertToAppSiteInformationDto(SiteInformation siteInformation){
		AppBizSiteInformationDto appSiteInformationDto  = new AppBizSiteInformationDto();
		appSiteInformationDto.setMsgId(siteInformation.getMsgId());
		appSiteInformationDto.setTitle(siteInformation.getTitle());
		appSiteInformationDto.setText(siteInformation.getText());
		appSiteInformationDto.setSendName(siteInformation.getSendName());
		appSiteInformationDto.setRecDate(siteInformation.getRecDate());
		appSiteInformationDto.setStatus(siteInformation.getStatus());
		return appSiteInformationDto;
	}
}
