package com.legendshop.app.biz.dao;

import com.legendshop.biz.model.dto.AppBizDetailDto;
import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.ShopDetail;

/**
 * @author 27158
 */
public interface AppBizShopDetailDao extends GenericDao<ShopDetail,Long> {

	/**
	 * 获取商家信息
	 * @param shopId
	 * @return
	 */
	AppBizDetailDto getShopDetail(Long shopId);
}
