package com.legendshop.app.biz.controller;

import com.legendshop.app.biz.service.AppBizCouponService;
import com.legendshop.app.biz.service.AppBizUserDetailService;
import com.legendshop.biz.model.dto.AppBizCouponDto;
import com.legendshop.biz.model.dto.AppBizShowCouponDto;
import com.legendshop.biz.model.dto.AppBizUserCouponDto;
import com.legendshop.biz.model.dto.AppBizUserDetailDto;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.spi.service.CouponService;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.util.AppUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * app店铺优惠卷控制器
 * @author 27158
 */
@Api(tags="优惠卷管理",value="优惠卷查看、添加、编辑")
@RestController
public class AppBizCouponController {
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(AppBizCouponController.class);
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private AppBizCouponService appBizCouponService;

	@Autowired
	private CouponService couponService;
	@Autowired
	private AppBizUserDetailService appBizUserDetailService;
	/**
	 * 优惠卷列表
	 *
	 * @param curPageNO
	 * @param status
	 * @return
	 */
	@ApiOperation(value = "优惠卷列表", httpMethod = "POST", notes = "优惠卷列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", name = "curPageNO", value = "分页", dataType = "string"),
			@ApiImplicitParam(paramType = "query", name = "status", value = "状态  有效:1 失效:0 过期 -1 所有 不填" , dataType = "integer"),
			@ApiImplicitParam(paramType = "query", name = "couponName", value = "优惠卷名称", dataType = "string")
	})
	@PostMapping("/s/shopCoupon")
	public ResultDto<AppPageSupport<AppBizShowCouponDto>> shopCoupon(@RequestParam(required = false) String curPageNO, @RequestParam(required = false) Integer status ,@RequestParam(required = false) String couponName) {

		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		AppPageSupport <AppBizShowCouponDto> ps=appBizCouponService.queryCoupon(curPageNO,status,shopId,couponName);
		return ResultDtoManager.success(ps);

	}
	/**
	 * 添加优惠券
	 * @return
	 */
	@ApiOperation(value = "添加优惠券", httpMethod = "POST", notes = "保存优惠券", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/s/saveShopCoupon")
	public ResultDto<String> saveShopCoupon(@RequestBody AppBizCouponDto appBizCouponDto) {

		LoginedUserInfo user = loginedUserService.getUser();
		Long shopId = user.getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		if (AppUtils.isBlank(appBizCouponDto)){
			return ResultDtoManager.fail(-1, "保存优惠券参数异常");
		}
		Boolean aBoolean = validationCoupon(appBizCouponDto);
		if (!aBoolean){
			return ResultDtoManager.fail(-1, "保存优惠卷参数错误");
		}
		int re=appBizCouponService.saveByCouponAndShopId(appBizCouponDto,user.getUserName(),shopId,user.getUserId());
		if (re==1){
			return ResultDtoManager.success();
		}
		return ResultDtoManager.fail(-1, "添加优惠卷失败");
	}

	/**
	 * 查看优惠卷
	 * @param couponId
	 * @return
	 */
	@ApiOperation(value = "查看优惠卷", httpMethod = "POST", notes = "查看优惠卷", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "query", name = "couponId", value = "优惠卷ID", dataType = "long",required = true)
	@PostMapping("/s/showShopCoupon")
	public ResultDto<AppBizShowCouponDto> showShopCoupon(@RequestParam Long couponId) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		AppBizShowCouponDto appBizShowCouponDto = appBizCouponService.getCoupon(couponId);
		return ResultDtoManager.success(appBizShowCouponDto);
	}


	/**
	 * 获取用户优惠券列表
	 * @param couponId
	 * @return
	 */
	@ApiOperation(value = "查看优惠卷", httpMethod = "POST", notes = "获取用户优惠券列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "curPageNO", value = "当前页码", dataType = "string"),
		@ApiImplicitParam(paramType = "query", name = "pageSize", value = "获取条数", dataType = "int"),
		@ApiImplicitParam(paramType = "query", name = "couponId", value = "优惠卷ID", dataType = "long",required = true)
	})
	@PostMapping("/s/userCouponList")
	public ResultDto<AppPageSupport<AppBizUserCouponDto>> userCouponList(String curPageNO,Integer pageSize,@RequestParam Long couponId) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		AppPageSupport<AppBizUserCouponDto> ps = appBizCouponService.queryUserCouponList(curPageNO,pageSize,couponId);
		return ResultDtoManager.success(ps);
	}


	/**
	 * 获取用户优惠券详情
	 * @param userCouponId
	 * @return
	 */
	@ApiOperation(value = "查看用户优惠券详情", httpMethod = "POST", notes = "查看用户优惠券详情", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "query", name = "userCouponId", value = "用户优惠券ID", dataType = "long",required = true)
	@PostMapping("/s/userCoupon")
	public ResultDto<AppBizUserCouponDto> userCoupon(@RequestParam Long userCouponId) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		AppBizUserCouponDto appBizUserCouponDto = appBizCouponService.getUserCouponById(userCouponId);
		return ResultDtoManager.success(appBizUserCouponDto);
	}

	/**
	 * 上线、下线优惠卷
	 * @param couponId
	 * @return
	 */
	@ApiOperation(value = "上线、下线优惠卷", httpMethod = "POST", notes = "上线、下线优惠卷", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", name = "couponId", value = "优惠卷ID", dataType = "long",required = true),
			@ApiImplicitParam(paramType = "query", name = "status", value = "状态：1 上线 0 下线", dataType = "integer",required = true),
	})
	@PostMapping("/s/updateShopCoupon")
	public ResultDto<String> updateShopCoupon(@RequestParam Long couponId,@RequestParam Integer status) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		Coupon coupon = couponService.getCoupon(couponId);
		if (AppUtils.isBlank(coupon)) {
			return ResultDtoManager.fail(-1, "没有该优惠卷");
		}

		if (!shopId.equals(coupon.getShopId())){
			return ResultDtoManager.fail(-1, "商家信息异常");
		}
		if (status.equals(1)) {
//		上线
			Date date =new Date();
			if (coupon.getEndDate().getTime() <= date.getTime()){
				return ResultDtoManager.fail(-1, "该活动已过期");
			}

		}
		coupon.setStatus(status);
		couponService.updateCoupon(coupon);
		return ResultDtoManager.success();
	}


	/**
	 * 删除优惠卷
	 * @param couponId
	 * @return
	 */
	@ApiOperation(value = "删除优惠卷", httpMethod = "POST", notes = "删除优惠卷", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", name = "couponId", value = "优惠卷ID", dataType = "long",required = true)
	})
	@PostMapping("/s/deleteShopCoupon")
	public ResultDto<String> deleteShopCoupon(@RequestParam Long couponId) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		Coupon coupon = couponService.getCoupon(couponId);
		if (AppUtils.isBlank(coupon)) {
			return ResultDtoManager.fail(-1, "没有该优惠卷");
		}
		if(!coupon.getShopId().equals(shopId)){
			return ResultDtoManager.fail(-1, "商家信息异常");
		}
		Date date = new Date();
		if(coupon.getStatus().equals(1) && coupon.getEndDate().getTime() >date.getTime()){
			return ResultDtoManager.fail(-1, "该优惠卷处于上线中不能删除");
		}
		if (coupon.getBindCouponNumber()>0){
			return ResultDtoManager.fail(-1, "优惠券已被使用，不能删除！");
		}
		appBizCouponService.shopDeleteCoupon(couponId);
		return ResultDtoManager.success();
	}
	/**
	 * 获取指定用户信息
	 * @param userPhone
	 * @return
	 */
	@ApiOperation(value = "获取指定用户信息", httpMethod = "POST", notes = "获取指定用户信息", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", name = "userPhone", value = "用户手机号", dataType = "string",required = true)
	})
	@PostMapping("/s/getShopCouponUser")
	public ResultDto<List<AppBizUserDetailDto>> getShopCouponUser(@RequestParam String  userPhone) {
		LoginedUserInfo user = loginedUserService.getUser();
		Long shopId = user.getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		List<AppBizUserDetailDto> userDetailByPhone = appBizUserDetailService.getUserDetailByPhone(userPhone, user.getUserId());
		return 	ResultDtoManager.success(userDetailByPhone);
	}

	/**
	 * 校验优惠券参数
	 * @param coupon
	 */
	private Boolean validationCoupon(AppBizCouponDto coupon) {
		if(AppUtils.isBlank(coupon.getCouponName())){
			return false;
		}
		if(AppUtils.isBlank(coupon.getFullPrice())){
			return false;
		}
		if(AppUtils.isBlank(coupon.getOffPrice())){
			return false;
		}
		if(coupon.getOffPrice() > coupon.getFullPrice()){
			log.info("面额不能大于消费金额");
			return false;
		}
		if(AppUtils.isBlank(coupon.getStartDate())){
			return false;
		}
		if(AppUtils.isBlank(coupon.getEndDate())){
			return false;
		}
		if(AppUtils.isBlank(coupon.getGetType())){
			return false;
		}
		if (coupon.getStartDate().getTime() > coupon.getEndDate().getTime()){
			log.info("开始时间不能大于结束时间");
			return false;
		}
		String getType = coupon.getGetType();
		if ("points".equals(getType)){
//			积分兑换
			if(AppUtils.isBlank(coupon.getNeedPoints())){
				return false;
			}
		}
		if(AppUtils.isBlank(coupon.getCouponType())){
			return false;
		}
		if("product".equals(coupon.getCouponType())){
			if (AppUtils.isBlank(coupon.getProdidList())){
				return false;
			}
		}
		if(AppUtils.isBlank(coupon.getIsDsignatedUser())){
			return false;
		}
		if(coupon.getIsDsignatedUser().equals(1)){
//			指定用户
			if(AppUtils.isBlank(coupon.getUserIdLists())){
				return false;
			}
			if(! "free".equals(coupon.getGetType())){
				return false;
			}
		}
		if (coupon.getIsDsignatedUser().equals(0)){
//			不指定用户领卷数不能为null
			if(AppUtils.isBlank(coupon.getGetLimit())){
				return false;
			}
//			不指定初始化券数量不能为null
			if(AppUtils.isBlank(coupon.getCouponNumber())){
				return false;
			}
		}
		return true;
	}
}
