package com.legendshop.app.biz.constants;

public class StatusCodes {
	
	private StatusCodes(){};
	
	/** 失败 */
	public final static int FAIL = -500;

	/** 未登录 */
	public final static int NOT_LOGIN = -1;
	
	/** 非法操作 */
	public final static int ILLEGAL_OPERATION = -20;
	
	/** 重复操作 */
	public final static int REPETITIVE = -50;
	
	/** 参数错误 */
	public final static int PARAMS_ERROR= -110;
	
	/** 未知错误 */
	public final static int UNKNOWN_ERROR= -999;

	/** 找不到数据 */
	public static final int NOT_FOUND = 0;
	
	/** 电话系统的老用户 */
	public static final int OLD_USER = -2;

	/** 密码错误 */
	public static final int PASSWORD_ERROR = -5;

	/** 验证码错误 */
	public static final int VERIFICATION_CODE_ERROR = -10;
	
	/** 数据已过期 */
	public static final int OVERTIME = -250;
}
