package com.legendshop.app.biz.controller;


import com.legendshop.app.biz.service.AppBizAccusationService;
import com.legendshop.biz.model.dto.AppBizAccusationDto;
import com.legendshop.biz.model.dto.productConsult.AppBizProductConsultDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.Accusation;
import com.legendshop.spi.service.AccusationService;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.util.AppUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 举报管理相关接口
 * @author linzh
 */
@Api(tags="举报管理",value="举报管理相关接口")
@RestController
public class AppBizAccusationController {

	private final Logger LOGGER = LoggerFactory.getLogger(AppBizAccusationController.class);

	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private AppBizAccusationService appBizAccusationService;

	@Autowired
	private AccusationService accusationService;



	/**
	 * 被举报列表
	 */
	@ApiOperation(value = "获取被举报列表", httpMethod = "POST", notes = "获取被举报列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = false, dataType = "String")
	@PostMapping(value="/s/accusationList")
	public ResultDto<AppPageSupport<AppBizAccusationDto>> query(String curPageNO) {

		try {

			LoginedUserInfo user = loginedUserService.getUser();
			if (AppUtils.isBlank(user)){

				return ResultDtoManager.fail(-1, "请先登录");
			}

			AppPageSupport<AppBizAccusationDto> ps = appBizAccusationService.queryAccusationList(curPageNO,user.getShopId());
			return ResultDtoManager.success(ps);
		} catch (Exception e) {
			LOGGER.error("获取被举报列表异常：", e.getMessage());
			return ResultDtoManager.fail();
		}
	}



	/**
	 * 举报详情
	 */
	@ApiOperation(value = "获取举报详情", httpMethod = "POST", notes = "获取举报详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "accusationId", value = "举报Id", required = true, dataType = "Long"),
	})
	@PostMapping(value="/accusationDetail")
	public ResultDto<AppBizAccusationDto> getAccusationDetail(Long accusationId) {

		try {

			Accusation accusation = accusationService.getAccusation(accusationId);
			if (AppUtils.isBlank(accusation)){

				return ResultDtoManager.fail(-1, "该举报信息不存在或已被删除！");
			}

			AppBizAccusationDto dto = appBizAccusationService.getAccusationDetail(accusationId);
			return ResultDtoManager.success(dto);
		} catch (Exception e) {
			LOGGER.error("获取举报详情异常：", e.getMessage());
			return ResultDtoManager.fail();
		}
	}

}
