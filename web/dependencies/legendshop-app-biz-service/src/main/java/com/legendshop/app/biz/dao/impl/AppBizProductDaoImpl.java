package com.legendshop.app.biz.dao.impl;

import com.legendshop.app.biz.dao.AppBizProductDao;
import com.legendshop.base.util.PageUtils;
import com.legendshop.biz.model.dto.AppProdCommDto;
import com.legendshop.biz.model.dto.comment.AppBizProductCommentsParamsDto;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.Product;
import org.springframework.stereotype.Repository;


/**
 * 商家端App商品DAO实现
 * @author linzh
 */
@Repository
public class AppBizProductDaoImpl extends GenericDaoImpl<Product, Long> implements AppBizProductDao {


	/**
	 * 获取商品评论列表
	 * @param params
	 * @return
	 */
	@Override
	public AppPageSupport<AppProdCommDto> queryProductComments(AppBizProductCommentsParamsDto params) {


		String curPageNO = String.valueOf(params.getCurPageNO());
		SimpleSqlQuery query = new SimpleSqlQuery(AppProdCommDto.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		map.put("prodId", params.getProdId());

		String condition = params.getCondition();

		// 如果不是全部评论
		if(!"all".equals(condition)){
			if("good".equals(condition)){
				map.put("scoreOperater", " AND c.score >= 4");
			}else if("medium".equals(condition)){
				map.put("scoreOperater", " AND c.score >= 3 AND c.score < 4");
			}else if("poor".equals(condition)){
				map.put("scoreOperater", " AND c.score < 3");
			}else if("photo".equals(condition)){
				map.put("isHasPhoto", " AND (c.photos IS NOT NULL OR ac.photos IS NOT NULL)");
			}else if("append".equals(condition)){
				map.put("isAddComm", 1);
			}else if("waitReply".equals(condition)){
				map.put("waitReply", " AND (c.is_reply = 0 OR (c.is_add_comm = 1 AND ac.is_reply = 0))");
			}
		}

		String queryAllSQL = ConfigCode.getInstance().getCode("app.biz.queryCommentsCount", map);
		String querySQL = ConfigCode.getInstance().getCode("app.biz.queryComments", map);

		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		map.remove("scoreOperater");
		map.remove("isHasPhoto");
		map.remove("waitReply");

		query.setParam(map.toArray());

		PageSupport<AppProdCommDto> ps = querySimplePage(query);
		AppPageSupport<AppProdCommDto> appProdCommList = PageUtils.convertPageSupport(ps);
		return appProdCommList;

	}
}
