package com.legendshop.app.biz.service;

import com.legendshop.app.biz.dto.AppBizShopProdCategoryDto;
import com.legendshop.app.biz.dto.AppbizDecorateShopCategoryDto;
import com.legendshop.biz.model.dto.AppBizCategoryDto;

import java.util.List;

/**
 * @author 27158
 */
public interface AppBizShopCategoryService {
	/**
	 * app 商家端获取 获取店铺分类列表
	 * @param shopId
	 * @return
	 */
	List<AppBizCategoryDto> queryShopCategory(Long shopId, Integer grade, Long parentId);

	/**
	 * 获取店铺商品类目
	 * @param shopId
	 * @return
	 */
	List<AppBizShopProdCategoryDto> getFirstShopCategory(Long shopId);

	/**
	 *获取店铺全部商品类目
	 * @param shopId
	 * @return
	 */
	List<AppbizDecorateShopCategoryDto> getShopCategoryDtoList(Long shopId);
}
