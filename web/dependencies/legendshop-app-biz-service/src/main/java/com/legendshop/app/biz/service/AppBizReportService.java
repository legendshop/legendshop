package com.legendshop.app.biz.service;

import com.legendshop.biz.model.dto.AppBizIndexDto;

/**
 * @author 27158
 */
public interface AppBizReportService {
	/**
	 * 获取商家端信息
	 * @param shopId
	 * @return
	 */
	AppBizIndexDto getShopSales(Long shopId);
}
