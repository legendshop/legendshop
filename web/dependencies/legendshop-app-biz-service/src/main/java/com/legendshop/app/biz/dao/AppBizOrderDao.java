package com.legendshop.app.biz.dao;

import com.legendshop.biz.model.dto.order.AppBizOrderDetailDto;
import com.legendshop.biz.model.dto.order.AppBizOrderDto;
import com.legendshop.biz.model.dto.order.AppBizOrderPriceDto;
import com.legendshop.biz.model.dto.order.AppBizOrderRemarkDto;
import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.order.OrderSearchParamDto;
import com.legendshop.model.entity.Sub;

public interface AppBizOrderDao extends GenericDao<Sub, Long> {
	/**
	 * 获取用户订单
	 * @param paramDto
	 * @param condition
	 * @return
	 */
	PageSupport<AppBizOrderDto> getBizShopOrderDtos(OrderSearchParamDto paramDto, String condition);

	/**
	 *获取订单详情
	 * @param subNumber
	 * @return
	 */
	AppBizOrderDetailDto getBizShopOrderDetail(String subNumber, Long shopId);

	/**
	 * 获取订单价格（原总价/现在总价/运费）
	 * @param subNumber
	 * @return
	 */
	AppBizOrderPriceDto getBizOrderPriceDto(String subNumber);

	/**
	 * 获取订单备注
	 * @param subNumber
	 * @return
	 */
	AppBizOrderRemarkDto getOrderRemarkInfo(String subNumber);
}
