/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.biz.dao;
 
import com.legendshop.dao.Dao;
import com.legendshop.model.entity.AppToken;

/**
 * The Class AppBizTokenDao.
 */

public interface AppBizTokenDao extends Dao<AppToken, Long> {

	public abstract AppToken getAppToken(Long id);

	public abstract int deleteAppToken(AppToken appToken);

	public abstract Long saveAppToken(AppToken appToken);

	public abstract int updateAppToken(AppToken appToken);

	public abstract boolean deleteAppToken(String userId, String accessToken);

	public abstract AppToken getAppTokenByUserId(String userId);

	public abstract boolean deleteAppToken(String userId);


	/**
	 * 根据userId、shopId和平台类型获取token
	 * @param userId 用户Id
	 * @param shopId 商家Id
	 * @param platform 平台类型
	 * @return
	 */
    AppToken getAppTokenByUserId(String userId, Long shopId, String platform);

	/**
	 * 删除商家端token
	 * @param userId 用户ID
	 * @param shopId 商家ID
	 * @return
	 */
	boolean deleteAppTokeByshopId(String userId, Long shopId);
}
