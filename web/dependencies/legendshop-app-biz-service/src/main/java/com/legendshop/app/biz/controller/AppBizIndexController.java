/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.biz.controller;

import com.legendshop.app.biz.service.AppBizReportService;
import com.legendshop.biz.model.dto.AppBizIndexDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.util.AppUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商家端首页相关接口
 * @author linzh
 */
@RestController
@Api(tags="商家端首页",value="商家端首页相关接口")
public class AppBizIndexController {
	
	private static Logger log = LoggerFactory.getLogger(AppBizIndexController.class);


	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private AppBizReportService appBizReportService;


	@ApiOperation(value = "商家端首页", httpMethod = "GET", notes = "获取商家端首页（商家工作台）",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@GetMapping("/s/shopIndex")
	public ResultDto<AppBizIndexDto> shopIndex() {


		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){

			return ResultDtoManager.fail(-1, "请先登录商家账号");
		}

		Long shopId = user.getShopId();
		if (AppUtils.isBlank(shopId)){
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		AppBizIndexDto shopIndexDto =appBizReportService.getShopSales(shopId);

		return ResultDtoManager.success(shopIndexDto);
	}

}
