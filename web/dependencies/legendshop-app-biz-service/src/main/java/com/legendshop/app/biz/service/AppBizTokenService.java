/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.biz.service;
import com.legendshop.model.entity.AppToken;

/**
 * The Class AppBizTokenService.
 */
public interface AppBizTokenService {

    public AppToken getAppTokenByUserId(String userId);

    public AppToken getAppToken(Long id);

    public void deleteAppToken(AppToken appToken);

    public Long saveAppToken(AppToken appToken);

    public void updateAppToken(AppToken appToken);

    public boolean deleteAppToken(String userId, String accessToken);

    public boolean deleteAppToken(String userId);


    /**
     * 根据userId、shopId和平台类型获取token
     * @param userId 用户Id
     * @param shopId 商家Id
     * @param platform 平台类型
     * @return
     */
    AppToken getAppToken(String userId, Long shopId, String platform);

    /**
     * 删除商家端token
     * @param userId 用户ID
     * @param shopId 商家ID
     * @return
     */
    boolean deleteAppTokeByshopId(String userId, Long shopId);
}
