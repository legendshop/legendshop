package com.legendshop.app.biz.controller;

import com.legendshop.app.biz.service.AppBizShopDetailService;
import com.legendshop.biz.model.dto.AppBizDetailDto;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * app店铺设置
 * @author 27158
 */
@RestController
@Api(tags="店铺设置",value="店鋪资料查看、修改、编辑")
public class AppBizSettingController {

	/** The log. */
	private final Logger LOGGER = LoggerFactory.getLogger(AppBizSettingController.class);
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private AppBizShopDetailService appShopDetailService;
	@Autowired
	private com.legendshop.spi.service.ShopDetailService ShopDetailService;

	@Autowired
	private AttachmentManager attachmentManager;

	@ApiOperation(value = "店鋪信息", httpMethod = "POST", notes = "店鋪信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/s/ShopSetting")
	public ResultDto<AppBizDetailDto> ShopSetting() {
		try {
			LoginedUserInfo user = loginedUserService.getUser();
			if(AppUtils.isBlank(user)){
				return ResultDtoManager.fail(-1, "对不起,您要查看的账号信息不存在或已被删除!");
			}
			Long shopId = user.getShopId();
			if (AppUtils.isBlank(shopId)){
				return ResultDtoManager.fail(-1, "该用户不是商家");
			}
			AppBizDetailDto shopDetailDto=appShopDetailService.getShopDetail(shopId);
			return ResultDtoManager.success(shopDetailDto);
		} catch (Exception e) {
			LOGGER.error("获取商家信息异常!", e);
			return ResultDtoManager.fail();
		}
	}

	/**
	 *  更新店铺logo图
	 * @return
	 */
	@ApiOperation(value = "更新店铺logo图", httpMethod = "POST", notes = "更新头像,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "logoPic", value = "新头像图片路径", required = true, dataType = "string")
	})
	@PostMapping(value="/s/updateShoplogoPic")
	public ResultDto<String> updateShoplogoPic(@RequestParam String logoPic) {

		try {
			Long shopId = loginedUserService.getUser().getShopId();
			ShopDetail shopDetail = ShopDetailService.getShopDetailByIdNoCache(shopId);
			if (AppUtils.isBlank(shopDetail)){
				return ResultDtoManager.fail();
			}
			String logoPic1 = shopDetail.getShopPic2();
			if ( AppUtils.isNotBlank(logoPic1)){
//				删除旧logo
				attachmentManager.delAttachmentByFilePath(logoPic1);
				attachmentManager.deleteTruely(logoPic1);
			}
			int flag =appShopDetailService.updateShoplogoPic(shopId,logoPic);
			if(flag == 1){
				return ResultDtoManager.success();
			}else{
				return ResultDtoManager.fail(-1, "商家不存在");
			}
		} catch (Exception e) {
			LOGGER.error("更新商家logo失败异常!", e);
			return ResultDtoManager.fail();
		}
	}
	/**
	 * 更新联系人
	 * @return
	 */
	@ApiOperation(value = "更新联系人", httpMethod = "POST", notes = "更新联系人,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "contactName", value = "新姓名", required = true, dataType = "string")
	@PostMapping(value="/s/updateContactName")
	public ResultDto<Object> updateContactName(@RequestParam String contactName) {

		try {
			Long shopId = loginedUserService.getUser().getShopId();
			if (AppUtils.isBlank(shopId)){
				return ResultDtoManager.fail();
			}
			int result =  appShopDetailService.updateContactName(shopId,contactName);
			if(result == 1){
				return ResultDtoManager.success(result);
			}else{
				return ResultDtoManager.fail(-1, "商家不存在");
			}
		} catch (Exception e) {
			LOGGER.error("更新商家联系人失败异常!", e);
			return ResultDtoManager.fail();
		}
	}
	/**
	 * 更新店铺名称
	 * @return
	 */
	@ApiOperation(value = "更新店铺名称", httpMethod = "POST", notes = "更新店铺名称,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "siteName", value = "新店铺名称", required = true, dataType = "string")
	@PostMapping(value="/s/updateSiteName")
	public ResultDto<Object> updateSiteName(@RequestParam String siteName) {

		try {
			Long shopId = loginedUserService.getUser().getShopId();
			if (AppUtils.isBlank(shopId)){
				return ResultDtoManager.fail();
			}
			int result =  appShopDetailService.updateSiteName(shopId,siteName);
			if(result == 1){
				return ResultDtoManager.success(result);
			}else{
				return ResultDtoManager.fail(-1, "商家不存在");
			}
		} catch (Exception e) {
			LOGGER.error("更新商家名称失败异常!", e);
			return ResultDtoManager.fail();
		}
	}

	/**
	 * 更新手机号
	 * @return
	 */
	@ApiOperation(value = "更新手机号", httpMethod = "POST", notes = "更新姓名信息,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "contactMobile", value = "手机号", required = true, dataType = "string")
	@PostMapping(value="/s/updateContactMobile")
	public ResultDto<Object> updateContactMobile(@RequestParam String contactMobile) {
		try {
			Long shopId = loginedUserService.getUser().getShopId();
			if (AppUtils.isBlank(shopId)){
				return ResultDtoManager.fail();
			}
			int result =  appShopDetailService.updateContactMobile(shopId,contactMobile);
			if(result == 1){
				return ResultDtoManager.success(result);
			}else{
				return ResultDtoManager.fail(-1, "商家不存在");
			}
		} catch (Exception e) {
			LOGGER.error("更新店铺手机号失败异常!", e);
			return ResultDtoManager.fail();
		}
	}

	/**
	 * 保存店铺地址
	 * @param shopDetailDto
	 * @return
	 */
	@ApiOperation(value = "保存或更新店铺地址", httpMethod = "POST", notes = "保存或更新店铺地址，以地址id为标识",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/s/saveAddress")
	public ResultDto<AppBizDetailDto> saveAddress(AppBizDetailDto shopDetailDto) {
		LoginedUserInfo token = loginedUserService.getUser();
		Long shopId = token.getShopId();
		if (AppUtils.isBlank(shopId)){
			return ResultDtoManager.fail(-1,"该用户不是商家");
		}
		if(!shopId.equals(shopDetailDto.getShopId())){
			return ResultDtoManager.fail(-1,"商家信息异常");
		}
		if(!checkUserAddress(shopDetailDto)){
			//检查非空字段
			return ResultDtoManager.fail(0, Constants.FAIL);
		}
		int result = appShopDetailService.updateDetail(shopDetailDto);
		if(result ==1){
			AppBizDetailDto shopDetail = appShopDetailService.getShopDetail(shopId);
			return ResultDtoManager.success(shopDetail);
		}else{
			return ResultDtoManager.fail(-1, "商家不存在");
		}
	}
	/**
	 * 检查店铺地址.
	 *
	 * @param shopDetailDto the user address
	 * @return true, if check user address
	 */
	private boolean checkUserAddress(AppBizDetailDto shopDetailDto) {
		if(
			AppUtils.isBlank(shopDetailDto.getProvinceId()) ||
			AppUtils.isBlank(shopDetailDto.getCityId()) ||
			AppUtils.isBlank(shopDetailDto.getAreaId()) ||
			AppUtils.isBlank(shopDetailDto.getShopAddr())
		){
			return false;
		}
		return true;
	}
}
