package com.legendshop.app.biz.service;


import com.legendshop.app.biz.dto.AppBizProductDetailDto;
import com.legendshop.biz.model.dto.AppBizMarketingProdDto;
import com.legendshop.biz.model.dto.AppProdCommDto;
import com.legendshop.biz.model.dto.comment.AppBizProductCommentsParamsDto;
import com.legendshop.biz.model.dto.prod.AppBizProdCategoryDto;
import com.legendshop.biz.model.dto.prod.AppBizProdDto;
import com.legendshop.model.dto.app.AppPageSupport;

import java.util.List;

/**
 * @author 27158
 */
public interface AppBizProductService {

	/**
	 * 查找商品列表
	 * @param curPageNO
	 * @param shopId
	 * @param status
	 * @param supportDist
	 * @return
	 */
	AppPageSupport<AppBizProdDto> queryProductList(String curPageNO, Long shopId, Integer status, Integer supportDist, String prodName);


	/**
	 * 修改商品状态
	 * @param shopId
	 * @param productIds
	 * @param status
	 * @return
	 */
	String updateProdStatus(Long shopId, List<Long> productIds, Integer status);

	/**
	 * 商家端推出分销
	 * @param shopId
	 * @param productIds
	 * @param supportDist
	 * @return
	 */
	String quiteDistribution(Long shopId, List<Long> productIds, Integer supportDist);

	/**
	 * 商家修改商品店铺分类
	 * @param shopId
	 * @param productIds
	 * @param firstCatId
	 * @param secondCatId
	 * @param thirdCatId
	 * @return
	 */
	String updateShopCategory(Long shopId, List<Long> productIds, Long firstCatId, Long secondCatId, Long thirdCatId);

	/**
	 * APP获取商品信息
	 * @param prodId
	 * @return
	 */
	AppBizProductDetailDto getProductDetailDto(Long prodId);

	/**
	 * 获取商品的评论列表
	 * @param params
	 * @return
	 */
	AppPageSupport<AppProdCommDto> queryProdComments(AppBizProductCommentsParamsDto params);


	/**
	 * 根据商家ID获取活动商品列表
	 * @param curPageNO
	 * @param shopId
	 * @return
	 */
	AppPageSupport<AppBizMarketingProdDto> getProductByShopId(String curPageNO, Long shopId, String name, Long shopFirstCatId, Integer shopCatType, String activityType);

	/**
	 * 获取下一级类目
	 * @param categoryId
	 * @return
	 */
	List<AppBizProdCategoryDto> nextCategory(Long categoryId);

	/**
	 * 发布商品，获取平台等级分类
	 * @param grade
	 * @param parentId
	 * @return the string
	 */
	List<AppBizProdCategoryDto> platformGradeCategory(Integer grade, Long parentId);

	/**
	 * app 商家端获取包邮活动商品列表
	 * @param curPageNO
	 * @param shopId
	 * @return
	 */
	AppPageSupport<AppBizMarketingProdDto> getShopShippingProds(String curPageNO, Long shopId, String name, Long shopCatId, Integer shopCatType);

	/**
	 * app 商家端获取秒杀活动商品列表
	 * @param curPageNO
	 * @param name
	 * @param shopId
	 * @return
	 */
	AppPageSupport<AppBizMarketingProdDto> getSeckillActivityPords(String curPageNO, String name, Long shopId);
}
