package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.dao.AppBizProductCommentDao;
import com.legendshop.app.biz.service.AppBizProductCommentService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.biz.model.dto.comment.AppBizProductCommentDto;
import com.legendshop.business.dao.ProductCommentDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.ProductCommentDto;
import com.legendshop.model.dto.ShopCommProdDto;
import com.legendshop.model.dto.app.AppPageSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 商家端商品评论服务
 * @author linzh
 */
@Service("appBizProductCommentService")
public class AppBizProductCommentServiceImpl implements AppBizProductCommentService {

	@Autowired
	private AppBizProductCommentDao appBizProductCommentDao;

	@Autowired
	private ProductCommentDao productCommentDao;


	/**
	 * 获取商品评论列表
	 * @param shopId 商家ID
	 * @param curPageNO 当前页码
	 * @param prodName 商品名称
	 * @param orders 排序
	 * @return
	 */
	@Override
	public AppPageSupport<AppBizProductCommentDto> queryProductCommentList(Long shopId, String curPageNO, String prodName, String orders) {


		// 获取商品评论列表
		PageSupport<AppBizProductCommentDto> ps  = appBizProductCommentDao.queryProductCommentList(shopId,curPageNO,prodName,orders);

		// 获取商品待回复评论数量
		for (AppBizProductCommentDto appBizProductCommentDto : ps.getResultList()) {
			Integer count = productCommentDao.getWaitReplyShopComm(appBizProductCommentDto.getProdId());
			appBizProductCommentDto.setWaitReply(count);
		}

		return PageUtils.convertPageSupport(ps);
	}

	/**
	 * 获取商品评论详情
	 * @param prodComId
	 * @return
	 */
	@Override
	public ProductCommentDto getProductCommentDetail(Long prodComId) {

		ProductCommentDto prodComm = productCommentDao.getProductCommentDetail(prodComId);
		//如果追评不为空，则获取多少天后追评的
		prodComm.setAppendDays(prodComm.getAppendDays());
		return prodComm;

	}

}
