package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.service.AppBizUserDetailService;
import com.legendshop.biz.model.dto.AppBizUserDetailDto;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 27158
 */
@Service("appBizUserDetailService")
public class AppBizUserDetailServiceImpl implements AppBizUserDetailService {
	@Autowired
	private UserDetailService userDetailService;
	@Autowired
	private UserDetailDao userDetailDao;
	@Override
	public AppBizUserDetailDto getUserDetail(String userId) {
		if (AppUtils.isNotBlank(userId)) {
			UserDetail userDetail = userDetailService.getUserDetailById(userId);
			if(userDetail == null){
				//防止用户被删除，App访问失效
				return null;
			}
			return convertUserDetailDto(userDetail);
		}
		return null;
	}

	@Override
	public int updateNickName(String userId, String nickName) {
		if (userDetailService.isNickNameExist(nickName)) {
			return 0;
		} else {
			UserDetail userDetail = userDetailService.getUserDetailById(userId);
			if (userDetail != null) {
				userDetail.setNickName(nickName);
				userDetailService.updateUserDetail(userDetail);
				return 1;
			} else {
				return -1;
			}
		}
	}

	@Override
	public int updatePortrait(String userId, String portraitPic) {
		UserDetail userDetail = userDetailService.getUserDetailById(userId);
		if(userDetail != null){
			userDetail.setPortraitPic(portraitPic);
			userDetailService.updateUserDetail(userDetail);
			return 1;
		}else{
			return -1;
		}
	}

	private AppBizUserDetailDto convertUserDetailDto(UserDetail userDetail){
		if(AppUtils.isNotBlank(userDetail)){
			AppBizUserDetailDto userDetailDto = new AppBizUserDetailDto();
			userDetailDto.setPortraitPic(userDetail.getPortraitPic());
			userDetailDto.setSex(userDetail.getSex());
			userDetailDto.setNickName(userDetail.getNickName());
			userDetailDto.setUserId(userDetail.getUserId());
			userDetailDto.setUserName(userDetail.getUserName());
			userDetailDto.setMobile(userDetail.getUserMobile());
			userDetailDto.setRealName(userDetail.getRealName());
			return userDetailDto;
		}
		return null;
	}


	@Override
	public List <AppBizUserDetailDto> getUserDetailByPhone(String userPhone, String userId) {
		List<UserDetail>  list =userDetailService.getUserDetailByPhone(userPhone,userId);
		if(AppUtils.isNotBlank(list)){
			List<AppBizUserDetailDto> appBizUserDetailDtos=new ArrayList<>();
			list.stream().forEach(userDetail -> {
				AppBizUserDetailDto appBizUserDetailDto = new AppBizUserDetailDto();
				appBizUserDetailDto.setUserId(userDetail.getUserId());
				appBizUserDetailDto.setMobile(userDetail.getUserMobile());
				appBizUserDetailDtos.add(appBizUserDetailDto);
			});
			return appBizUserDetailDtos;
		}
		return null;
	}

}
