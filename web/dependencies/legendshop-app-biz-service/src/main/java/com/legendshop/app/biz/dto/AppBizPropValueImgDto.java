/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.biz.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 属性图片Dto.
 */

@ApiModel("属性图片Dto")
public class AppBizPropValueImgDto implements Serializable{
	
	private static final long serialVersionUID = 8649282116100050143L;

	/**
	 * 属性值id
	 */
	@ApiModelProperty(value = "属性值id")
	private Long valueId;

	/**
	 * 图片路径 集合
	 */
	@ApiModelProperty(value = "图片路径 集合")
	private List<String> imgList;
	
	/**
	 * 属性值名称
	 */
	@ApiModelProperty(value = "属性值名称")
	private String valueName;
	
	/**
	 * 属性ID
	 */
	@ApiModelProperty(value = "属性ID")
	private Long propId;
	
	/**
	 * 属性值图片
	 */
	@ApiModelProperty(value = "属性值图片")
	private String valueImage;
	
	/**
	 * 排序
	 */
	@ApiModelProperty(value = "排序")
	private Long seq;

	public Long getValueId() {
		return valueId;
	}

	public void setValueId(Long valueId) {
		this.valueId = valueId;
	}

	public List<String> getImgList() {
		return imgList;
	}

	public void setImgList(List<String> imgList) {
		this.imgList = imgList;
	}

	public String getValueName() {
		return valueName;
	}

	public void setValueName(String valueName) {
		this.valueName = valueName;
	}

	public Long getPropId() {
		return propId;
	}

	public void setPropId(Long propId) {
		this.propId = propId;
	}

	public String getValueImage() {
		return valueImage;
	}

	public void setValueImage(String valueImage) {
		this.valueImage = valueImage;
	}

	public Long getSeq() {
		return seq;
	}

	public void setSeq(Long seq) {
		this.seq = seq;
	}
	
}
