package com.legendshop.app.biz.controller;


import com.legendshop.app.biz.service.AppBizAccusationService;
import com.legendshop.app.biz.service.AppBizProdArrivalService;
import com.legendshop.biz.model.dto.AppBizAccusationDto;
import com.legendshop.biz.model.dto.AppBizProdArrivalInformDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.Accusation;
import com.legendshop.spi.service.AccusationService;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.ProductArrivalInformService;
import com.legendshop.util.AppUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 到货通知相关接口
 * @author linzh
 */
@Api(tags="到货通知",value="到货通知相关接口")
@RestController
public class AppBizProdArrivalController {

	private final Logger LOGGER = LoggerFactory.getLogger(AppBizProdArrivalController.class);

	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private AppBizProdArrivalService appBizProdArrivalService;


	/**
	 * 到货通知列表
	 */
	@ApiOperation(value = "获取到货通知列表", httpMethod = "POST", notes = "获取到货通知列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = false, dataType = "String"),
			@ApiImplicitParam(paramType="query", name = "productName", value = "商品名称 （保留字段）", required = false, dataType = "String")
	})
	@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = false, dataType = "String")
	@PostMapping(value="/s/prodArrivalList")
	public ResultDto<AppPageSupport<AppBizProdArrivalInformDto>> queryProdArrivalList(String curPageNO,String productName) {

		try {

			LoginedUserInfo user = loginedUserService.getUser();
			if (AppUtils.isBlank(user)){

				return ResultDtoManager.fail(-1, "请先登录");
			}

			AppPageSupport<AppBizProdArrivalInformDto> ps = appBizProdArrivalService.queryProdArrivalList(curPageNO,user.getShopId(),productName);
			return ResultDtoManager.success(ps);
		} catch (Exception e) {
			LOGGER.error("获取到货通知列表异常：", e.getMessage());
			return ResultDtoManager.fail();
		}
	}


	/**
	 * 获取通知用户列表
	 */
	@ApiOperation(value = "获取通知用户列表", httpMethod = "POST", notes = "获取通知用户列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = false, dataType = "String"),
		@ApiImplicitParam(paramType="query", name = "skuId", value = "skuId", required = true, dataType = "String")
	})
	@PostMapping(value="/s/prodArrivaUserList")
	public ResultDto<AppPageSupport<AppBizProdArrivalInformDto>> queryProdArrivaUserList(String curPageNO, String skuId) {

		try {

			LoginedUserInfo user = loginedUserService.getUser();
			if (AppUtils.isBlank(user)){

				return ResultDtoManager.fail(-1, "请先登录");
			}

			AppPageSupport<AppBizProdArrivalInformDto> ps = appBizProdArrivalService.queryProdArrivaUserList(curPageNO,user.getShopId(),skuId);
			return ResultDtoManager.success(ps);
		} catch (Exception e) {
			LOGGER.error("获取通知用户列表异常：", e.getMessage());
			return ResultDtoManager.fail();
		}
	}



}
