package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.service.AppBizProdArrivalService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.biz.model.dto.AppBizAccusationDto;
import com.legendshop.biz.model.dto.AppBizProdArrivalInformDto;
import com.legendshop.business.dao.ProductArrivalInformDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.stock.ProdArrivalInformDto;
import com.legendshop.model.entity.Accusation;
import com.legendshop.util.AppUtils;
import com.legendshop.util.MethodUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 商家端 到货通知服务接口实现
 * @author linzh
 */
@Service("appBizProdArrivalService")
public class AppBizProdArrivalServiceImpl implements AppBizProdArrivalService {

	@Autowired
	private ProductArrivalInformDao productArrivalInformDao;


	/**
	 * 获取到货通知列表
	 * @param curPageNO
	 * @param shopId
	 * @param productName
	 * @return
	 */
	@Override
	public AppPageSupport<AppBizProdArrivalInformDto> queryProdArrivalList(String curPageNO, Long shopId, String productName) {

		PageSupport<ProdArrivalInformDto> ps = productArrivalInformDao.getProdArrival(shopId, productName, curPageNO);

		AppPageSupport<AppBizProdArrivalInformDto> converPs =  PageUtils.convertPageSupport(ps, new PageUtils.ResultListConvertor<ProdArrivalInformDto, AppBizProdArrivalInformDto>() {


			@Override
			public List<AppBizProdArrivalInformDto> convert(List<ProdArrivalInformDto> form) {

				List<AppBizProdArrivalInformDto> toList = new ArrayList<>();

				for (ProdArrivalInformDto prodArrivalInformDto : form) {
					if (AppUtils.isNotBlank(prodArrivalInformDto)) {

						AppBizProdArrivalInformDto dto = convertToAppBizProdArrivalInformDto(prodArrivalInformDto);
						toList.add(dto);
					}
				}
				return toList;
			}

			private AppBizProdArrivalInformDto convertToAppBizProdArrivalInformDto(ProdArrivalInformDto prodArrivalInformDto){
				AppBizProdArrivalInformDto appBizProdArrivalInformDto = new AppBizProdArrivalInformDto();

				appBizProdArrivalInformDto.setProdArrivalInformId(prodArrivalInformDto.getProdArrivalInformId());
				appBizProdArrivalInformDto.setPrice(prodArrivalInformDto.getPrice());
				appBizProdArrivalInformDto.setCnProperties(prodArrivalInformDto.getCnProperties());
				appBizProdArrivalInformDto.setProdId(prodArrivalInformDto.getProdId());
				appBizProdArrivalInformDto.setPicture(prodArrivalInformDto.getPicture());
				appBizProdArrivalInformDto.setProductName(prodArrivalInformDto.getProductName());
				appBizProdArrivalInformDto.setSkuId(prodArrivalInformDto.getSkuId());
				return appBizProdArrivalInformDto;
			}
		});
		return converPs;
	}

	/**
	 *
	 * @param curPageNO
	 * @param shopId
	 * @param skuId
	 * @return
	 */
	@Override
	public AppPageSupport<AppBizProdArrivalInformDto> queryProdArrivaUserList(String curPageNO, Long shopId, String skuId) {

		PageSupport<ProdArrivalInformDto> ps = productArrivalInformDao.getSelectArrival(curPageNO, skuId, shopId);

		AppPageSupport<AppBizProdArrivalInformDto> converPs =  PageUtils.convertPageSupport(ps, new PageUtils.ResultListConvertor<ProdArrivalInformDto, AppBizProdArrivalInformDto>() {


			@Override
			public List<AppBizProdArrivalInformDto> convert(List<ProdArrivalInformDto> form) {

				List<AppBizProdArrivalInformDto> dtoList = new ArrayList<>();

				for (ProdArrivalInformDto prodArrivalInformDto : form) {
					if (AppUtils.isNotBlank(prodArrivalInformDto)) {

						AppBizProdArrivalInformDto dto = convertToAppBizProdArrivalInformDto(prodArrivalInformDto);
						dtoList.add(dto);
					}
				}
				return dtoList;
			}

			private AppBizProdArrivalInformDto convertToAppBizProdArrivalInformDto(ProdArrivalInformDto prodArrivalInformDto){
				AppBizProdArrivalInformDto appBizProdArrivalInformDto = new AppBizProdArrivalInformDto();
				appBizProdArrivalInformDto.setMobilePhone(prodArrivalInformDto.getMobilePhone());
				appBizProdArrivalInformDto.setProdArrivalInformId(prodArrivalInformDto.getProdArrivalInformId());
				appBizProdArrivalInformDto.setCreateTime(prodArrivalInformDto.getCreateTime());
				appBizProdArrivalInformDto.setPrice(prodArrivalInformDto.getPrice());
				appBizProdArrivalInformDto.setCnProperties(prodArrivalInformDto.getCnProperties());
				appBizProdArrivalInformDto.setProdId(prodArrivalInformDto.getProdId());
				appBizProdArrivalInformDto.setUserName(prodArrivalInformDto.getUserName());
				appBizProdArrivalInformDto.setPicture(prodArrivalInformDto.getPicture());
				appBizProdArrivalInformDto.setProductName(prodArrivalInformDto.getProductName());
				appBizProdArrivalInformDto.setEmail(prodArrivalInformDto.getEmail());
				return appBizProdArrivalInformDto;
			}
		});
		return converPs;
	}

}
