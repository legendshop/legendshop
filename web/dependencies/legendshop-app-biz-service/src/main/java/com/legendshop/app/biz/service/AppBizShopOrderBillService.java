package com.legendshop.app.biz.service;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.ShopOrderBill;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.ShopOrderBillService;
import com.legendshop.util.AppUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


public interface AppBizShopOrderBillService {


	/**
	 * 获取结算列表
	 * @param shopId
	 * @param curPageNO
	 * @param sn
	 * @param status
	 * @return
	 */
	AppPageSupport<ShopOrderBill> getShopOrderBillPage(Long shopId, String curPageNO, String sn, Integer status);
}
