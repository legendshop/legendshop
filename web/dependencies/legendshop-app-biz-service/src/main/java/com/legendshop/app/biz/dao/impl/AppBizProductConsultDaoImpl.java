package com.legendshop.app.biz.dao.impl;


import com.legendshop.app.biz.dao.AppBizProductConsultDao;
import com.legendshop.biz.model.dto.productConsult.AppBizProductConsultListDto;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.model.entity.ProductConsult;
import org.springframework.stereotype.Repository;

/**
 * 商家端app 商品咨询DAO
 * @author linzh
 */
@Repository
public class AppBizProductConsultDaoImpl extends GenericDaoImpl<ProductConsult, Long> implements AppBizProductConsultDao {


	/**
	 * 获取商品咨询列表
	 * @param shopId
	 * @param curPageNO
	 * @param keywords
	 * @param pointType
	 * @return
	 */
	@Override
	public PageSupport<AppBizProductConsultListDto> queryProductConsultList(Long shopId, String curPageNO, String keywords, Integer pointType) {

		Integer pageSize = 10;
		SimpleSqlQuery query = new SimpleSqlQuery(AppBizProductConsultListDto.class, pageSize, curPageNO);

		QueryMap map = new QueryMap();
		map.put("shopId",shopId);
		map.like("userName", keywords);
		map.like("nickName", keywords);
		map.like("prodName", keywords);
		map.put("pointType", pointType);

		String queryAllSQL = ConfigCode.getInstance().getCode("app.biz.queryProductConsultListCount", map);
		String querySQL = ConfigCode.getInstance().getCode("app.biz.queryProductConsultList", map);

		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		query.setParam(map.toArray());

		return querySimplePage(query);

	}
}
