package com.legendshop.app.biz.util;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * Knife4j 配置中心
 * @author Joe
 */
//@Configuration
//@EnableSwagger2
//@EnableKnife4j
//@Import(BeanValidatorPluginsConfiguration.class)
public class RestApiConfig {
	
	/**
	 * 商家端API文档信息
	 * @return
	 */
    private ApiInfo userApiInfo(){
   	 return new ApiInfoBuilder()
        .title("Dev商家端API文档 ")
        .description("LegendShop DEV Platform's REST API, all the applications could access the Object model data via JSON.")
        .version("5.5")
        .termsOfServiceUrl("NO terms of service")
        .license("The Apache License, Version 2.0")
        .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
        .build();
   }
   
    /**
     * 创建商家端API文档
     * @return
     */
    @Bean
    public Docket userDocket() {
    	Predicate<RequestHandler> selector1 = RequestHandlerSelectors.basePackage("com.legendshop.app.biz.controller");
    	return new Docket(DocumentationType.SWAGGER_2)
        .select()  // 选择那些路径和api会生成document
        .apis(Predicates.or(selector1))
        .build()
        .securitySchemes(userSecuritySchemes())
        .securityContexts(userSecurityContexts())
        .groupName("商家端API 5.5")
        .apiInfo(userApiInfo())
        ;
    }
    
	
	
    
    private List<ApiKey> userSecuritySchemes() {
    	List<ApiKey> list = new ArrayList<ApiKey>();
    	
    	//用户鉴权相关header头
    	list.add(new ApiKey("userId", "userId", "header"));
    	list.add(new ApiKey("accessToken", "accessToken", "header"));
    	list.add(new ApiKey("deviceId", "deviceId", "header"));
    	list.add(new ApiKey("securiyCode", "securiyCode", "header"));
    	list.add(new ApiKey("sign", "sign", "header"));
    	list.add(new ApiKey("verId", "verId", "header"));
    	list.add(new ApiKey("time", "time", "header"));
    	
        return list;
    }

    private List<SecurityContext> userSecurityContexts() {
    	
    	List<SecurityContext> list = new ArrayList<SecurityContext>();
    	
    	SecurityContext securityContext = SecurityContext.builder()
        .securityReferences(userAuth())
        .forPaths(PathSelectors.regex("/s/.*"))
        .build();
    	
    	list.add(securityContext);
    	
        return list;
    }

    List<SecurityReference> userAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        
        List<SecurityReference> list = new ArrayList<SecurityReference>();
        
    	list.add(new SecurityReference("userId", authorizationScopes));
    	list.add(new SecurityReference("accessToken", authorizationScopes));
    	list.add(new SecurityReference("deviceId", authorizationScopes));
    	list.add(new SecurityReference("securiyCode", authorizationScopes));
    	list.add(new SecurityReference("sign", authorizationScopes));
    	list.add(new SecurityReference("verId", authorizationScopes));
    	list.add(new SecurityReference("time", authorizationScopes));
        
        return list;
    }
    
}