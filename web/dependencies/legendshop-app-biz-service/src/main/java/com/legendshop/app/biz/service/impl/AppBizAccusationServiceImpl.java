package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.service.AppBizAccusationService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.biz.model.dto.AppBizAccusationDto;
import com.legendshop.business.dao.AccusationDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.Accusation;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.spi.service.AccusationService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.MethodUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 商家端 举报服务接口实现
 * @author linzh
 */
@Service("appBizAccusationService")
public class AppBizAccusationServiceImpl implements AppBizAccusationService {

	@Autowired
	private AccusationService accusationService;

	@Autowired
	private AccusationDao accusationDao;



	/**
	 * 获取被举报商品列表
	 * @param curPageNO
	 * @param shopId
	 * @return
	 */
	@Override
	public AppPageSupport<AppBizAccusationDto> queryAccusationList(String curPageNO, Long shopId) {


		PageSupport<Accusation> ps = accusationService.queryReportForbit(curPageNO,shopId);

		AppPageSupport<AppBizAccusationDto> convertPs =  PageUtils.convertPageSupport(ps, new PageUtils.ResultListConvertor<Accusation, AppBizAccusationDto>() {
			@Override
			public List<AppBizAccusationDto> convert(List<Accusation> form) {


				List<AppBizAccusationDto> toList = new ArrayList<AppBizAccusationDto>();
				for (Accusation accusation : form) {
					if (AppUtils.isNotBlank(accusation)) {

						AppBizAccusationDto dto =  convertToAppBizAccusationDto(accusation);
						toList.add(dto);
					}
				}
				return toList;

			}

			// 转换DTO
			private AppBizAccusationDto convertToAppBizAccusationDto(Accusation accusation){
				AppBizAccusationDto appBizAccusationDto = new AppBizAccusationDto();

				appBizAccusationDto.setId(accusation.getId());
				appBizAccusationDto.setAccuType(accusation.getAccuType());
				appBizAccusationDto.setResult(accusation.getResult());
				appBizAccusationDto.setStatus(accusation.getStatus());
				appBizAccusationDto.setProdId(accusation.getProdId());
				appBizAccusationDto.setProdName(accusation.getProdName());
				appBizAccusationDto.setProdPic(accusation.getProdPic());

				return appBizAccusationDto;
			}
		});
		return convertPs;
	}

	/**
	 * 获取举报内容详情
	 * @param accusationId
	 * @return
	 */
	@Override
	public AppBizAccusationDto getAccusationDetail(Long accusationId) {

		Accusation accusation = accusationDao.getDetailedAccusation(accusationId);
		if (AppUtils.isNotBlank(accusation)){

			AppBizAccusationDto appBizAccusationDto = convertToAppBizAccusationDto(accusation);
			return appBizAccusationDto;
		}
		return null;
	}

	/**
	 * 转换DTO
	 * @param accusation
	 * @return
	 */
	private AppBizAccusationDto convertToAppBizAccusationDto(Accusation accusation){
		AppBizAccusationDto appBizAccusationDto = new AppBizAccusationDto();
		appBizAccusationDto.setHandleTime(accusation.getHandleTime());
		appBizAccusationDto.setHandleInfo(accusation.getHandleInfo());
		appBizAccusationDto.setIllegalOff(accusation.getIllegalOff());
		appBizAccusationDto.setProdId(accusation.getProdId());
		appBizAccusationDto.setTitle(accusation.getTitle());
		appBizAccusationDto.setAccuType(accusation.getAccuType());
		appBizAccusationDto.setRecDate(accusation.getRecDate());
		appBizAccusationDto.setContent(accusation.getContent());
		appBizAccusationDto.setResult(accusation.getResult());
		appBizAccusationDto.setPic1(accusation.getPic1());
		appBizAccusationDto.setProdName(accusation.getProdName());
		appBizAccusationDto.setId(accusation.getId());
		appBizAccusationDto.setPic2(accusation.getPic2());
		appBizAccusationDto.setPic3(accusation.getPic3());
		appBizAccusationDto.setProdPic(accusation.getProdPic());
		appBizAccusationDto.setStatus(accusation.getStatus());
		appBizAccusationDto.setUserDelStatus(accusation.getUserDelStatus());
		return appBizAccusationDto;
	}
}
