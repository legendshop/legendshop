package com.legendshop.app.biz.service;

import com.legendshop.biz.model.dto.AppBizAccusationDto;
import com.legendshop.model.dto.app.AppPageSupport;

/**
 * 商家端 举报服务接口
 * @author linzh
 */
public interface AppBizAccusationService {


	/**
	 * 获取被举报商品列表
	 * @param curPageNO
	 * @param shopId
	 * @return
	 */
	AppPageSupport<AppBizAccusationDto> queryAccusationList(String curPageNO, Long shopId);

	/**
	 * 获取举报内容详情
	 * @param accusationId
	 * @return
	 */
	AppBizAccusationDto getAccusationDetail(Long accusationId);
}
