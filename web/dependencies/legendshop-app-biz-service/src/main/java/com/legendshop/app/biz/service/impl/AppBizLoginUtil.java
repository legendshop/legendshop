package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.service.AppBizTokenService;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.model.constant.AppTokenTypeEnum;
import com.legendshop.model.constant.LoginUserTypeEnum;
import com.legendshop.model.constant.VisitSourceEnum;
import com.legendshop.model.dto.LoginSuccess;
import com.legendshop.model.entity.AppToken;
import com.legendshop.processor.LoginHistoryProcessor;
import com.legendshop.processor.LoginSendIntegralProcessor;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 商家端APP登录工具类
 * @author joeLin
 */
@Service("appBizLoginUtil")
public class AppBizLoginUtil {

	@Autowired
    private AppBizTokenService appBizTokenService;


    /**
     * loginToken 登录
     *
     * @param userName
     * @return
     */

    /**
     *
     * @param userId
     * @param userName
     * @param deviceId
     * @param platform
     * @param verId
     * @param shopId
     * @return
     */
    public AppToken loginToken(String userId, String userName, String deviceId, String platform, String verId,Long shopId) {

        // 获取token
        AppToken appToken = appBizTokenService.getAppToken(userId,shopId,platform);

        if (appToken != null) {

            //更新登录时间
        	appToken.setStartDate(new Date());

        	appToken.setVerId(verId);
        	appToken.setDeviceId(deviceId);
        	appToken.setPlatform(platform.trim());


            appBizTokenService.updateAppToken(appToken);
        // 第一次登陆，保存并返回token
        } else {
            Date now = new Date();
            appToken = new AppToken();

            // 获得随机码
            String accessToken = CommonServiceUtil.getRandomString(30);
            appToken.setAccessToken(accessToken);
            appToken.setDeviceId(deviceId);
            appToken.setPlatform(platform);
            appToken.setRecDate(now);

            // 获得随机码
            String securiyCode = CommonServiceUtil.getRandomString(8);
            appToken.setSecuriyCode(securiyCode);
            appToken.setStartDate(now);
            appToken.setUserId(userId);
            appToken.setUserName(userName);
            appToken.setShopId(shopId);

            // 保持登陆2周
            appToken.setValidateTime(1209600L);
            appToken.setVerId(verId);
            appToken.setType(AppTokenTypeEnum.SHOP.value());

            appBizTokenService.saveAppToken(appToken);
        }

		return appToken;
    }

}
