package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.dto.AppBizShopProdCategoryDto;
import com.legendshop.app.biz.dto.AppbizDecorateShopCategoryDto;
import com.legendshop.app.biz.service.AppBizShopCategoryService;
import com.legendshop.biz.model.dto.AppBizCategoryDto;
import com.legendshop.business.dao.ShopCategoryDao;
import com.legendshop.model.dto.appdecorate.DecorateShopCategoryDto;
import com.legendshop.model.entity.ShopCategory;
import com.legendshop.spi.service.ShopCategoryService;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 27158
 */
@Service("appBizShopCategoryService")
public class AppBizShopCategoryServiceImpl implements AppBizShopCategoryService {

	@Autowired
	private ShopCategoryService shopCategoryService;

	@Autowired
	private ShopCategoryDao shopCategoryDao;

	@Override
	public List<AppBizCategoryDto>  queryShopCategory( Long shopId, Integer grade, Long parentId) {
		List<ShopCategory> shopCategoryList = shopCategoryDao.appQueryShopCategory(shopId, grade, parentId);
		if (AppUtils.isNotBlank(shopCategoryList)) {
			List<AppBizCategoryDto> dtoList = new ArrayList<>();
			shopCategoryList.stream().forEach(shopCategory -> {
				AppBizCategoryDto appBizCategoryDto = new AppBizCategoryDto();
				List<ShopCategory> shopCategories = shopCategoryDao.queryByParentId(shopCategory.getId());
				if (shopCategories.size() > 0) {
					appBizCategoryDto.setIsNext(1);
				} else {
					appBizCategoryDto.setIsNext(-1);
				}
				appBizCategoryDto.setId(shopCategory.getId());
				appBizCategoryDto.setName(shopCategory.getName());
				appBizCategoryDto.setParentId(shopCategory.getParentId());
				appBizCategoryDto.setPic(shopCategory.getPic());
				appBizCategoryDto.setRecDate(shopCategory.getRecDate());
				appBizCategoryDto.setStatus(shopCategory.getStatus());
				appBizCategoryDto.setSeq(shopCategory.getSeq());
				appBizCategoryDto.setGrade(shopCategory.getGrade());
				dtoList.add(appBizCategoryDto);
			});
			return dtoList;
		}

		return null;
	}

	@Override
	public List<AppBizShopProdCategoryDto> getFirstShopCategory(Long shopId) {
		List<ShopCategory> shopCategoryList = shopCategoryService.getFirstShopCategory(shopId);
		return convert(shopCategoryList);
	}

	@Override
	public List<AppbizDecorateShopCategoryDto> getShopCategoryDtoList(Long shopId) {
		List<DecorateShopCategoryDto> shopCategoryDtoList = shopCategoryDao.getShopCategoryDtoList(shopId);
		return convertAppbizDecorateShopCategoryDto(shopCategoryDtoList);
	}

	private List<AppbizDecorateShopCategoryDto> convertAppbizDecorateShopCategoryDto(List<DecorateShopCategoryDto> shopCategoryDtoList) {
		List<AppbizDecorateShopCategoryDto> toList = new ArrayList<AppbizDecorateShopCategoryDto>();
		for (DecorateShopCategoryDto decorateShopCategoryDto : shopCategoryDtoList) {
			if (AppUtils.isNotBlank(decorateShopCategoryDto)) {
				AppbizDecorateShopCategoryDto appbizDecorateShopCategoryDto = new AppbizDecorateShopCategoryDto();
				appbizDecorateShopCategoryDto.setId(decorateShopCategoryDto.getId());
				appbizDecorateShopCategoryDto.setName(decorateShopCategoryDto.getName());
				appbizDecorateShopCategoryDto.setParentId(decorateShopCategoryDto.getParentId());
				appbizDecorateShopCategoryDto.setGrade(decorateShopCategoryDto.getGrade());
				toList.add(appbizDecorateShopCategoryDto);
			}
		}
		return toList;
	}

	private List<AppBizShopProdCategoryDto> convert(List<ShopCategory> from) {
		List<AppBizShopProdCategoryDto> toList = new ArrayList<AppBizShopProdCategoryDto>();
		for (ShopCategory shopCategory : from) {
			if (AppUtils.isNotBlank(shopCategory)) {
				AppBizShopProdCategoryDto appBizShopProdCategoryDto = convertToAppBizShopProdCategoryDto(shopCategory);
				toList.add(appBizShopProdCategoryDto);
			}
		}
		return toList;
	}

	private AppBizShopProdCategoryDto convertToAppBizShopProdCategoryDto(ShopCategory shopCategory) {
		AppBizShopProdCategoryDto appBizShopProdCategoryDto = new AppBizShopProdCategoryDto();
		appBizShopProdCategoryDto.setSubCatList(shopCategory.getSubCatList());
		appBizShopProdCategoryDto.setNextCatId(shopCategory.getNextCatId());
		appBizShopProdCategoryDto.setName(shopCategory.getName());
		appBizShopProdCategoryDto.setSubCatId(shopCategory.getSubCatId());
		appBizShopProdCategoryDto.setId(shopCategory.getId());
		appBizShopProdCategoryDto.setPic(shopCategory.getPic());
		appBizShopProdCategoryDto.setShopId(shopCategory.getShopId());
		appBizShopProdCategoryDto.setParentId(shopCategory.getParentId());
		appBizShopProdCategoryDto.setSeq(shopCategory.getSeq());
		appBizShopProdCategoryDto.setStatus(shopCategory.getStatus());
		return appBizShopProdCategoryDto;
	}

}
