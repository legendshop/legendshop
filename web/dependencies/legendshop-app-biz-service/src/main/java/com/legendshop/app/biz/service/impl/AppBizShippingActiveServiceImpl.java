package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.service.AppBizShippingActiveService;
import com.legendshop.biz.model.dto.AppBizMarketingProdDto;
import com.legendshop.biz.model.dto.AppBizShippingActiveDto;
import com.legendshop.business.dao.ShippingActiveDao;
import com.legendshop.business.dao.ShippingProdDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.ShippingTypeEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.ShippingActive;
import com.legendshop.model.entity.ShippingProd;
import com.legendshop.model.prod.FullActiveProdDto;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author 27158
 */
@Service("appBizShippingActiveService")
public class AppBizShippingActiveServiceImpl implements AppBizShippingActiveService {
	@Autowired
	private ShippingActiveDao shippingActiveDao;
	@Autowired
	private ShippingProdDao shippingProdDao;
	@Override
	public void saveShippingActive(AppBizShippingActiveDto appBizShippingActiveDto) {
//		封装ShippingActive 实体类
		ShippingActive shippingActive=new ShippingActive();
		shippingActive.setName(appBizShippingActiveDto.getName());
		shippingActive.setShopId(appBizShippingActiveDto.getShopId());
		shippingActive.setStartDate(appBizShippingActiveDto.getStartDate());
		shippingActive.setEndDate(appBizShippingActiveDto.getEndDate());
		shippingActive.setCreateDate(new Date());
		shippingActive.setStatus(true);
		shippingActive.setFullValue(appBizShippingActiveDto.getFullValue());
		shippingActive.setReduceType(appBizShippingActiveDto.getReduceType());
		if (appBizShippingActiveDto.getReduceType().equals(1)) {
//			元
			shippingActive.setDescription("满"+appBizShippingActiveDto.getFullValue()+"元，包邮");
		}else{
//			件
			shippingActive.setDescription("满"+appBizShippingActiveDto.getFullValue()+"件，包邮");
		}
		shippingActive.setFullType(appBizShippingActiveDto.getFullType());
		shippingActive.setSource("APP");
		Long id = shippingActiveDao.saveShippingActive(shippingActive);
		if (appBizShippingActiveDto.getFullType().equals(ShippingTypeEnum.PROD.value()) && AppUtils.isNotBlank(id)){
			Long[] prodIds = appBizShippingActiveDto.getProdIds();
			//	保存商品信息
			List<ShippingProd> lists = new ArrayList<ShippingProd>();
			for(int i=0;i<prodIds.length;i++){
				ShippingProd activeProd = new ShippingProd();
				activeProd.setShopId(appBizShippingActiveDto.getShopId());
				activeProd.setProdId(prodIds[i]);
				activeProd.setShippingId(id);
				lists.add(activeProd);
			}
			shippingProdDao.batchShippingProd(lists);
		}

	}

	@Override
	public AppPageSupport<AppBizShippingActiveDto> queryShippingActiveList(String curPageNO, Long shopId, String name, String searchType) {
		if (AppUtils.isBlank(curPageNO)){
			curPageNO="1";
		}
		ShippingActive shippingActive = new ShippingActive();
		shippingActive.setName(name);
		shippingActive.setSearchType(searchType);
		PageSupport<ShippingActive> ps = shippingActiveDao.queryShippingActiveList(curPageNO, shopId,shippingActive);
		return convertPageSupportDto(ps);
	}

	private AppPageSupport<AppBizShippingActiveDto> convertPageSupportDto(PageSupport<ShippingActive>  pageSupport){
		if(AppUtils.isNotBlank(pageSupport)){
			AppPageSupport<AppBizShippingActiveDto> pageSupportDto = new AppPageSupport<>();
			pageSupportDto.setTotal(pageSupport.getTotal());
			pageSupportDto.setCurrPage(pageSupport.getCurPageNO());
			pageSupportDto.setPageSize(pageSupport.getPageSize());
			List<AppBizShippingActiveDto> appBizShippingActiveDtos= convertAddressDtoList(pageSupport.getResultList());
			pageSupportDto.setResultList(appBizShippingActiveDtos);
			pageSupportDto.setPageCount(pageSupport.getPageCount());
			return pageSupportDto;
		}
		return null;
	}
	private List<AppBizShippingActiveDto> convertAddressDtoList(List<ShippingActive> shippingActiveList){
		if(AppUtils.isNotBlank(shippingActiveList)){
			List<AppBizShippingActiveDto> list = new ArrayList<>();
			shippingActiveList.stream().forEach(shippingActive -> {
				AppBizShippingActiveDto appBizShippingActiveDto = new AppBizShippingActiveDto();
				appBizShippingActiveDto.setId(shippingActive.getId());
				appBizShippingActiveDto.setName(shippingActive.getName());
				appBizShippingActiveDto.setShopId(shippingActive.getShopId());
				appBizShippingActiveDto.setStartDate(shippingActive.getStartDate());
				appBizShippingActiveDto.setEndDate(shippingActive.getEndDate());
				appBizShippingActiveDto.setFullType(shippingActive.getFullType());
				appBizShippingActiveDto.setFullValue(shippingActive.getFullValue());
				appBizShippingActiveDto.setStatus(shippingActive.getStatus());
				appBizShippingActiveDto.setReduceType(shippingActive.getReduceType());
				list.add(appBizShippingActiveDto);
			});
			return list;
		}
		return null;
	}

	@Override
	public AppBizShippingActiveDto getShippingActiveById(Long id) {
		ShippingActive shippingActive = shippingActiveDao.getShippingActive(id);
		if (AppUtils.isBlank(shippingActive)){
			return null;
		}
		AppBizShippingActiveDto appBizShippingActiveDto=new AppBizShippingActiveDto();
		appBizShippingActiveDto.setId(shippingActive.getId());
		appBizShippingActiveDto.setName(shippingActive.getName());
		appBizShippingActiveDto.setShopId(shippingActive.getShopId());
		appBizShippingActiveDto.setStatus(shippingActive.getStatus());
		appBizShippingActiveDto.setStartDate(shippingActive.getStartDate());
		appBizShippingActiveDto.setEndDate(shippingActive.getEndDate());
		appBizShippingActiveDto.setReduceType(shippingActive.getReduceType());
		appBizShippingActiveDto.setFullValue(shippingActive.getFullValue());
		appBizShippingActiveDto.setFullType(shippingActive.getFullType());
		if (shippingActive.getFullType().equals(1)){
//			部分商品
			List<FullActiveProdDto> fullActiveProdDtoList = shippingProdDao.getFullActiveProdDtoByActiveId( shippingActive.getShopId(),shippingActive.getId());
			List<AppBizMarketingProdDto> list=new ArrayList<>();
			Long [] prodIds = null;
			if(AppUtils.isNotBlank(fullActiveProdDtoList)){
				int num = 0;
				prodIds = new Long[fullActiveProdDtoList.size()];
				for (FullActiveProdDto fullActiveProdDto : fullActiveProdDtoList) {
					AppBizMarketingProdDto appBizMarketingProdDto = new AppBizMarketingProdDto();
					appBizMarketingProdDto.setProdId(fullActiveProdDto.getProdId());
					appBizMarketingProdDto.setProdName(fullActiveProdDto.getProdName());
					appBizMarketingProdDto.setPic(fullActiveProdDto.getPic());
					appBizMarketingProdDto.setCash(fullActiveProdDto.getPrice());
					appBizMarketingProdDto.setStocks(fullActiveProdDto.getActualStocks());
					list.add(appBizMarketingProdDto);
					prodIds[num]=fullActiveProdDto.getProdId();
					num++;
				}
			}
			if (AppUtils.isNotBlank(prodIds)){
				appBizShippingActiveDto.setProdIds(prodIds);
			}
			appBizShippingActiveDto.setMarketingProdDtos(list);
		}

		return appBizShippingActiveDto;
	}
}
