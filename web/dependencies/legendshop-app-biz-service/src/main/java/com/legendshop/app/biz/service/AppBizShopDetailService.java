package com.legendshop.app.biz.service;

import com.legendshop.biz.model.dto.AppBizDetailDto;

/**
 * @author 27158
 */
public interface AppBizShopDetailService {

	/**
	 *  获取商家信息
	 * @param shopId
	 * @return
	 */
	AppBizDetailDto getShopDetail(Long shopId);

	/**
	 * 修改商家logo
	 * @param shopId
	 * @param logoPic
	 * @return
	 */
	int updateShoplogoPic(Long shopId, String logoPic);

	/**
	 * 修改店铺联系人
	 * @param shopId
	 * @param contactName
	 * @return
	 */
	int updateContactName(Long shopId, String contactName);

	/**
	 * 修改店铺联系人手机号
	 * @param shopId
	 * @param contactMobile
	 * @return
	 */
	int updateContactMobile(Long shopId, String contactMobile);

	/**
	 * 修改店铺信息
	 * @param shopDetailDto
	 * return
	 */
	int updateDetail(AppBizDetailDto shopDetailDto);

	/**
	 * 更新店铺名称
	 * @param shopId
	 * @param siteName
	 * @return
	 */
	int updateSiteName(Long shopId, String siteName);
}
