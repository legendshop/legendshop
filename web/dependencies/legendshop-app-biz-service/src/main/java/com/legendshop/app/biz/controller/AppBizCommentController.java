package com.legendshop.app.biz.controller;


import com.legendshop.app.biz.service.AppBizProductCommentService;
import com.legendshop.app.biz.service.AppBizProductService;
import com.legendshop.biz.model.dto.AppProdCommDto;
import com.legendshop.biz.model.dto.comment.AppBizProductCommentDto;
import com.legendshop.biz.model.dto.comment.AppBizProductCommentsParamsDto;
import com.legendshop.model.constant.ProdCommStatusEnum;
import com.legendshop.model.dto.ProductCommentDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.ProdAddComm;
import com.legendshop.model.entity.ProductComment;
import com.legendshop.model.entity.ProductCommentCategory;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.ProdAddCommService;
import com.legendshop.spi.service.ProductCommentService;
import com.legendshop.spi.service.SensitiveWordService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SafeHtml;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Set;

/**
 * 评价管理相关接口
 * @author linzh
 */
@Api(tags="评价管理",value="评价管理相关接口")
@RestController
public class AppBizCommentController {

	private final Logger LOGGER = LoggerFactory.getLogger(AppBizCommentController.class);

	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private AppBizProductCommentService appBizProductCommentService;

	@Autowired
	private AppBizProductService appBizProductService;

	@Autowired
	private ProductCommentService productCommentService;

	@Autowired
	private SensitiveWordService sensitiveWordService;

	@Autowired
	private ProdAddCommService prodAddCommService;



	/**
	 * 已评论商品列表
	 */
	@ApiOperation(value = "已评论商品列表", httpMethod = "POST", notes = "已评论商品列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = false, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "prodName", value = "商品名称", required = false, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "orders", value = "排序:默认为最新评价时间addTime,平均分为avgScore,评价数为comments,格式如avgScore,desc，avgScore,asc 降序升序", required = false, dataType = "string")
	})
	@PostMapping(value="/s/comments/query")
	public ResultDto<AppPageSupport<AppBizProductCommentDto>> query(String curPageNO,String prodName,String orders) {


		try {

			LoginedUserInfo user = loginedUserService.getUser();
			if (AppUtils.isBlank(user)){

				return ResultDtoManager.fail(-1, "请先登录");
			}
			Long shopId = user.getShopId();

			AppPageSupport<AppBizProductCommentDto> ps = appBizProductCommentService.queryProductCommentList(shopId,curPageNO,prodName,orders);

			return ResultDtoManager.success(ps);
		} catch (Exception e) {
			LOGGER.error("获取已评论商品列表异常：", e.getMessage());
			return ResultDtoManager.fail();
		}
	}



	/**
	 * 获取商品评论列表
	 * @return
	 */
	@ApiOperation(value = "获取商品评论列表", httpMethod = "POST", notes = "获取商品评论列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/productComments")
	public ResultDto<AppPageSupport<AppProdCommDto>> queryProductComments(AppBizProductCommentsParamsDto params){

		try {

			AppPageSupport<AppProdCommDto> result = appBizProductService.queryProdComments(params);
			return ResultDtoManager.success(result);

		} catch (Exception e) {
			LOGGER.error("获取商品评论列表异常：", e.getMessage());
			return ResultDtoManager.fail();
		}


	}



	/**
	 * 获取商品评论数量
	 * @param prodId
	 */
	@ApiOperation(value = "获取商品评论数量", httpMethod = "POST", notes = "获取VUE商品评论数量,各个评价等级的数据统计",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/productCommentNumbers/{prodId}")
	public ResultDto<ProductCommentCategory> productCommentNumbers(@PathVariable Long prodId){

		try {

			//计算当前商品的评论情况
			ProductCommentCategory pcc = productCommentService.initProductCommentCategory(prodId);
			return ResultDtoManager.success(pcc);

		} catch (Exception e) {
			LOGGER.error("获取商品评论数量异常：", e.getMessage());
			return ResultDtoManager.fail();
		}
	}



	/**
	 *  获取商品评论详情
	 */
	@ApiOperation(value = "获取商品评论详情", httpMethod = "POST", notes = "获取商品评论详情，返回AppProdCommDto",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "query", dataType = "Long", name = "prodComId", value = "评论Id", required = true)
	@PostMapping(value="/prodCommentDetail")
	public ResultDto<ProductCommentDto> prodCommentDetail(Long prodComId){

		try {

			ProductCommentDto prodCommentDetail = appBizProductCommentService.getProductCommentDetail(prodComId);
			return ResultDtoManager.success(prodCommentDetail);

		} catch (Exception e) {
			LOGGER.error("获取获取商品评论详情异常：", e.getMessage());
			return ResultDtoManager.fail();
		}

	}



	/**
	 *  商家回复评论
	 */
	@ApiOperation(value = "商家回复评论", httpMethod = "POST", notes = "商家回复评论 成功返回ok,失败返回fail",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", name = "prodComentId", value = "评论Id",dataType = "Long", required = true),
			@ApiImplicitParam(paramType = "query", name = "replyType", value = "回复类型 first ：回复初评  append ：回复追评",dataType = "String", required = true),
			@ApiImplicitParam(paramType = "query", name = "replyContent", value = "回复内容",dataType = "String", required = true)
	})
	@PostMapping(value="/s/replyComment")
	public ResultDto<Object> shopReplyComment(Long prodComentId,String replyType, String replyContent){

		try {

			LoginedUserInfo user = loginedUserService.getUser();
			if (AppUtils.isBlank(user)){

				return ResultDtoManager.fail(-1, "请先登录");
			}

			String userName = user.getUserName();

			ProductComment productComment = productCommentService.getProductCommentById(prodComentId);
			if (null == productComment) {
				return ResultDtoManager.fail(-1, "对不起, 您要回复的评论不存在!");
			}

			if (!productComment.getOwnerName().equals(userName)) {

				return ResultDtoManager.fail(-1, "对不起, 您无权回复此评论!");
			}

			//敏感字过滤
			Set<String> findWords = sensitiveWordService.checkSensitiveWords(replyContent);
			if(AppUtils.isNotBlank(findWords)){
				return ResultDtoManager.fail(-1,"您回复的内容含有敏感词 " + findWords + ", 请更正再提交！");
			}

			// 回复初评
			if("first".equals(replyType)){

				if (!ProdCommStatusEnum.SUCCESS.value().equals(productComment.getStatus())) {

					return ResultDtoManager.fail(-1, "对不起, 该评论暂不可以回复!");
				}

				if (productComment.getIsReply()) {

					return ResultDtoManager.fail(-1, "对不起, 您已经回复过该评论了!");
				}

				SafeHtml safeHtml = new SafeHtml();
				String safeReplyContent = safeHtml.makeSafe(replyContent.trim());

				productComment.setIsReply(true);
				productComment.setShopReplyContent(safeReplyContent);
				productComment.setShopReplyTime(new Date());

				productCommentService.update(productComment);

			// 回复追评
			}else{

				if (!ProdCommStatusEnum.SUCCESS.value().equals(productComment.getStatus()) || !productComment.getIsAddComm()) {
					return ResultDtoManager.fail(-1, "对不起, 该评论暂不可以回复!");
				}

				ProdAddComm prodAddComm = prodAddCommService.getProdAddCommByCommId(prodComentId);

				if (!ProdCommStatusEnum.SUCCESS.value().equals(prodAddComm.getStatus())) {
					return ResultDtoManager.fail(-1, "对不起, 该评论暂不可以回复!");
				}

				if (prodAddComm.getIsReply()) {
					return ResultDtoManager.fail(-1, "对不起, 该评论你已经回复过了!");
				}

				prodAddComm.setShopReplyContent(replyContent);
				prodAddComm.setShopReplyTime(new Date());
				prodAddComm.setIsReply(true);

				prodAddCommService.updateProdAddComm(prodAddComm);
			}

			return ResultDtoManager.success();

		} catch (Exception e) {
			LOGGER.error("回复商品评论异常：", e.getMessage());
			return ResultDtoManager.fail();
		}

	}

}
