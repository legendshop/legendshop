package com.legendshop.app.biz.service;

import com.legendshop.biz.model.dto.KeyValueDto;

import java.util.List;


/**
 * @author 27158
 */
public interface AppBizLocationService {
	/**
	 * 加載所有省
	 * @return
	 */
	public List<KeyValueDto> loadProvinces();

	/**
	 * 加載所有市
	 * @param provinceid
	 * @return
	 */
	public List<KeyValueDto> loadCities(Integer provinceid);

	/**
	 * 加載所有區、县
	 * @param cityid
	 * @return
	 */
	public List<KeyValueDto> loadAreas(Integer cityid);
}
