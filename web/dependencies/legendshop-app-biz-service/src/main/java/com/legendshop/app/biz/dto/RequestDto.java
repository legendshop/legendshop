package com.legendshop.app.biz.dto;

import java.io.Serializable;
/**
 * 返回值
 *
 */
public class RequestDto implements Serializable{

	private static final long serialVersionUID = -4437856374554789917L;
	
	private final Object result;
	
	public RequestDto(Object result){
		this.result = result;
	}

	public Object getResult() {
		return result;
	}

}
