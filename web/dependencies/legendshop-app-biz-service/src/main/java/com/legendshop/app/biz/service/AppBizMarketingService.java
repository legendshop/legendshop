package com.legendshop.app.biz.service;

import com.legendshop.biz.model.dto.AppBizMarketingDto;
import com.legendshop.biz.model.dto.AppBizShowMarketingDto;
import com.legendshop.model.dto.app.AppPageSupport;

/**
 * @author 27158
 */
public interface AppBizMarketingService {
	/**
	 * 获取满减满折列表
	 * @param curPageNO
	 * @param shopId
	 * @return
	 */
	AppPageSupport<AppBizShowMarketingDto> getMjMarketingPage(String curPageNO, String searchType, Long shopId, String marketName);

	/**
	 * 获取限时折扣列表
	 */
	AppPageSupport<AppBizShowMarketingDto> getZkMarketingPage(String curPageNO, String searchType, Long shopId, String marketName);
	/**
	 * 查看活动详情
	 * @param id
	 * @param shopId
	 * @return
	 */
	AppBizShowMarketingDto getMarketingByIdAndShopId(Long id, Long shopId);

	/**
	 * 添加促销活动
	 * @param appBizMarketingDto
	 * @return
	 */
	int saveShopMarketing(AppBizMarketingDto appBizMarketingDto);

	/**
	 * 修改促销活动
	 * @param appBizMarketingDto
	 * @return
	 */
	int updateMarketing(AppBizMarketingDto appBizMarketingDto);
}
