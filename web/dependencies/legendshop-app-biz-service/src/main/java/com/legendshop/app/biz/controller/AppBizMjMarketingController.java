package com.legendshop.app.biz.controller;

import com.legendshop.app.biz.service.AppBizMarketingService;
import com.legendshop.app.biz.service.AppBizProductService;
import com.legendshop.biz.model.dto.AppBizMarketingDto;
import com.legendshop.biz.model.dto.AppBizMarketingProdDto;
import com.legendshop.biz.model.dto.AppBizShowMarketingDto;
import com.legendshop.model.constant.ActivityTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.Marketing;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.MarketingProdsService;
import com.legendshop.spi.service.MarketingService;
import com.legendshop.util.AppUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * @author 27158
 */
@RestController
@Api(tags="滿減满折管理",value="活动查看、添加、编辑")
public class AppBizMjMarketingController {

	/**
	 * The log.
	 */
	private final Logger LOGGER = LoggerFactory.getLogger(AppBizMjMarketingController.class);

	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private AppBizMarketingService appBizMarketingService;

	@Autowired
	private AppBizProductService appBizProductService;


	@Autowired
	private MarketingService marketingService;

	@Autowired
	private MarketingProdsService marketingProdsService;
	/**
	 * 滿減满折活动列表
	 *
	 * @param curPageNO
	 * @param searchType
	 * @return
	 */

	@ApiOperation(value = "滿減满折活动列表", httpMethod = "POST", notes = "滿減满折活动列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", name = "curPageNO", value = "分页", dataType = "string"),
			@ApiImplicitParam(paramType = "query", name = "searchType", value = "类型，所有：ALL 未发布：NO_PUBLISH 未开始：NOT_STARTED 进行中:ONLINE" +
					"已暂停:PAUSE 已结束:FINISHED 已失效:EXPIRED ", dataType = "string",required = true),
			@ApiImplicitParam(paramType = "query",name="marketName",value = "活动名称",dataType = "string")
	})
	@PostMapping("/s/shopMarketing")
	public ResultDto<AppPageSupport<AppBizShowMarketingDto>> shopMarketing(@RequestParam(required = false) String curPageNO, @RequestParam String searchType,@RequestParam(required = false) String marketName) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		AppPageSupport<AppBizShowMarketingDto> ps = appBizMarketingService.getMjMarketingPage(curPageNO, searchType, shopId,marketName);
		return ResultDtoManager.success(ps);
	}

	/**
	 * 查看满减满折详情
	 *
	 * @return
	 */
	@ApiOperation(value = "查看满减满折详情", httpMethod = "POST", notes = "查看满减满折详情", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", name = "id", value = "活动ID", dataType = "long",required = true)
	})
	@PostMapping("/s/showShopMarketing")
	public ResultDto<AppBizShowMarketingDto> showShopMarketing(@RequestParam Long id) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		AppBizShowMarketingDto appBizMarketingDto = appBizMarketingService.getMarketingByIdAndShopId(id, shopId);
		if (AppUtils.isBlank(appBizMarketingDto)) {
			return ResultDtoManager.fail(-1, "没有该满减满折活动");
		}
		return ResultDtoManager.success(appBizMarketingDto);
	}

	/**
	 * 获取店铺商品列表
	 *
	 * @return
	 */
	@ApiOperation(value = "获取店铺商品列表", httpMethod = "POST", notes = "获取店铺商品列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", name = "curPageNO", value = "分页", dataType = "string"),
			@ApiImplicitParam(paramType = "query", name = "name", value = "商品名称", dataType = "string"),
			@ApiImplicitParam(paramType = "query", name = "shopCatId", value = "店铺分类ID  ", dataType = "long"),
			@ApiImplicitParam(paramType = "query", name = "shopCatType", value = "店铺分类级别 1 ， 2 ，3 ", dataType = "integer"),
			@ApiImplicitParam(paramType = "query", name = "activityType", value = "活动类型", dataType = "string")
	})
	@PostMapping("/s/getShopMarketingProds")
	public ResultDto<AppPageSupport<AppBizMarketingProdDto>> getShopMarketingProds(@RequestParam(required = false) String curPageNO,@RequestParam (required = false) String name,
																				   @RequestParam(required = false) Long shopCatId ,
																				   @RequestParam(required = false) Integer shopCatType,
																				   @RequestParam(required = false) String activityType) {
		Long shopId = loginedUserService.getUser().getShopId();
		//查看商品
		AppPageSupport<AppBizMarketingProdDto> ps = appBizProductService.getProductByShopId(curPageNO, shopId,name,shopCatId ,shopCatType, activityType);
		return ResultDtoManager.success(ps);
	}


	/**
	 * 添加滿減满折活动
	 *
	 * @return
	 */
	@ApiOperation(value = "添加滿減满折活动", httpMethod = "POST", notes = "添加滿減满折活动", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/s/saveShopMarketing")
	public ResultDto<String> saveShopMarketing(@RequestBody  AppBizMarketingDto appBizMarketingDto) {
		LoginedUserInfo user = loginedUserService.getUser();
		Long shopId =user.getShopId();
		String userId = user.getUserId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		if(AppUtils.isBlank(appBizMarketingDto)){
			return ResultDtoManager.fail(-1, "促销活动参数不足");
		}
		boolean b = validateMarketingDto(appBizMarketingDto);
		if(!b){
			LOGGER.info("促销活动参数错误");
			return ResultDtoManager.fail(-1, "促销活动参数错误");
		}
		Integer isAllProds = appBizMarketingDto.getIsAllProds();
//		活动重复时间校验判断
		if (isAllProds.equals(1)){
			// 全部商品
			List<Marketing> oldMarketings = marketingService.findOngoingMarketing(shopId, appBizMarketingDto.getIsAllProds(), appBizMarketingDto.getStartTime(), appBizMarketingDto.getEndTime());
			if(AppUtils.isNotBlank(oldMarketings)){
				LOGGER.info("该时间段内已存在全场满折促销活动,请重新选择时间");
				ResultDtoManager.fail(-1, Constants.FAIL);
			}
		}
		appBizMarketingDto.setShopId(shopId);
		appBizMarketingDto.setUserId(userId);
		int result	=appBizMarketingService.saveShopMarketing(appBizMarketingDto);
		if (result ==1){
			return ResultDtoManager.success();
		}
		return ResultDtoManager.fail(-1, Constants.FAIL);
	}


	/**
	 * 发布促销活动
	 *
	 * @return
	 */

	@ApiOperation(value = "发布、恢复 促销活动", httpMethod = "POST", notes = "发布、恢复 促销活动", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", name = "id", value = "促销活动ID", dataType = "long",required = true)
	})
	@PostMapping ("/s/releaseMarketing")
	public ResultDto<String> releaseMarketing(@RequestParam Long id ) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		Marketing marketing = marketingService.getMarketing(shopId, id);
		if (AppUtils.isBlank(marketing)) {
			return ResultDtoManager.fail(-1,"找不到该活动");
		}
		if (new Date().after(marketing.getEndTime())) {
			// 在 当前时间之前
			return ResultDtoManager.fail(-1,"该活动已过期");
		}

		if(marketing.getIsAllProds().equals(1)){
			//同一时间段只能发布一个全场满折活动
			int count = marketingService.getAllProdsMarketing(shopId, marketing.getType(), marketing.getStartTime(), marketing.getEndTime());
			if(count != 0){
				return ResultDtoManager.fail(-1,"已有其他全场促销活动上线，请先下线其他全场促销活动！");
			}
		}
		if (marketing.getIsAllProds().equals(0)) {
			// 部分商品,是否添加了商品信息
			Long count = marketingService.isExistProds(marketing.getId(), marketing.getShopId());
			if (count == 0) {
				return ResultDtoManager.fail(-1,"请设置活动商品！");
			}
		}
		/**
		 * 修改活动状态
		 */
		if (marketing.getState().equals(0)|| marketing.getState().equals(2)) {
			marketing.setState(1);
			marketingService.updateMarketing(marketing);
			if (marketing.getIsAllProds().equals(1)) {
				// 针对于全场商品
				marketingProdsService.clearGlobalMarketCache(shopId);
			} else {
				marketingProdsService.clearProdMarketCache();
			}

			return ResultDtoManager.success();
		}
		return ResultDtoManager.fail(-1,"活动状态异常");

	}


	/**
	 * 暂停促销活动
	 *
	 * @return
	 */

	@ApiOperation(value = "暂停促销活动", httpMethod = "POST", notes = "暂停促销活动", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", name = "id", value = "促销活动ID", dataType = "long",required = true)
	})
	@PostMapping ("/s/suspendMarketing")
	public ResultDto<String> suspendMarketing(@RequestParam Long id) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		Marketing marketing = marketingService.getMarketing(shopId, id);
		if (AppUtils.isBlank(marketing)) {
			return ResultDtoManager.fail(-1,"找不到该活动");
		}
		if (marketing.getState().equals(1)) {
			marketing.setState(2);
			marketingService.updateMarketing(marketing);
			return ResultDtoManager.success();
		}
		if (marketing.getIsAllProds().intValue() == 1) { // 针对于全场商品
			marketingProdsService.clearGlobalMarketCache(shopId);
		} else {
			marketingProdsService.clearProdMarketCache();
		}

		return ResultDtoManager.fail(-1,"活动状态异常");
	}
	/**
	 * 删除促销活动
	 *
	 * @return
	 */

	@ApiOperation(value = "删除促销活动", httpMethod = "POST", notes = "删除促销活动", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "id", value = "促销活动ID", dataType = "long",required = true)
	})
	@PostMapping ("/s/deleteMarketing")
	public ResultDto<String> deleteMarketing(@RequestParam Long id) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		Marketing marketing = marketingService.getMarketing(shopId, id);
		if (AppUtils.isBlank(marketing)) {
			return ResultDtoManager.fail(-1,"找不到该活动");
		}
		Date date = new Date();
		if (marketing.getState().equals(1) && marketing.getEndTime().getTime() >= date.getTime()) {
			return ResultDtoManager.fail(-1,"该活动在进行,不能删除");
		}
		if(marketing.getState().equals(2)){
			return ResultDtoManager.fail(-1,"该活动在进行,不能删除");
		}
		marketingService.deleteMarketing(marketing);
		if (marketing.getIsAllProds().intValue() == 1) {
			// 针对于全场商品
			marketingProdsService.clearGlobalMarketCache(shopId);
		} else {
			marketingProdsService.clearProdMarketCache();
		}
		return ResultDtoManager.success();
	}

	/**
	 * 下线促销活动
	 *
	 * @return
	 */
	@ApiOperation(value = "下线促销活动", httpMethod = "POST", notes = "下线促销活动", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "id", value = "促销活动ID", dataType = "long",required = true)
	})
	@PostMapping ("/s/offlineMarketing")
	public ResultDto<String> offlineMarketing(@RequestParam Long id) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		Marketing marketing = marketingService.getMarketing(shopId, id);
		if (AppUtils.isBlank(marketing)) {
			return ResultDtoManager.fail(-1,"找不到该活动");
		}

		if (marketing.getState().equals(1) || marketing.getState().equals(2)) {
			marketing.setState(3);
			marketingService.updateMarketing(marketing);
			if (marketing.getIsAllProds().intValue() == 1) {
				// 针对于全场商品
				marketingProdsService.clearGlobalMarketCache(shopId);
			} else {
				marketingProdsService.clearProdMarketCache();
			}
			return ResultDtoManager.success();
		}
		return ResultDtoManager.fail(-1,"活动状态异常");
	}

	/**
	 * 编辑满减满折详情
	 *
	 * @return
	 */
	@ApiOperation(value = "编辑满减满折详情", httpMethod = "POST", notes = "编辑满减满折详情", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/s/updateShopMarketing")
	public ResultDto<String> updateShopMarketing(@RequestBody  AppBizMarketingDto appBizMarketingDto) {
		LoginedUserInfo user = loginedUserService.getUser();
		Long shopId =user.getShopId();
		String userId = user.getUserId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		if(AppUtils.isBlank(appBizMarketingDto) ||AppUtils.isBlank(appBizMarketingDto.getId())){
			return ResultDtoManager.fail(-1, "促销活动参数不足");
		}
		boolean b = validateMarketingDto(appBizMarketingDto);
		if(!b){
			LOGGER.info("促销活动参数不足");
			return ResultDtoManager.fail(-1, "促销活动参数不足");
		}
		Integer isAllProds = appBizMarketingDto.getIsAllProds();
//		活动重复时间校验判断
		if (isAllProds.equals(1)){
			// 全部商品
			List<Marketing> oldMarketings = marketingService.findOngoingMarketing(shopId, appBizMarketingDto.getIsAllProds(), appBizMarketingDto.getStartTime(), appBizMarketingDto.getEndTime());
			if(AppUtils.isNotBlank(oldMarketings)){
				LOGGER.info("该时间段内已存在全场满折促销活动,请重新选择时间");
				ResultDtoManager.fail(-1, "该时间段内已存在全场满折促销活动,请重新选择时间");
			}
		}
		int result=	appBizMarketingService.updateMarketing(appBizMarketingDto);
		if (result ==1){
			return ResultDtoManager.success();
		}
		return ResultDtoManager.fail(-1, "保存活动失败");

	}

	/**
	 * 验证促销活动
	 *
	 * @param appBizMarketingDto
	 * @return
	 */
	private boolean validateMarketingDto(AppBizMarketingDto appBizMarketingDto) {
		if (AppUtils.isBlank(appBizMarketingDto.getMarketName())) {
			return false;
		}
		if (AppUtils.isBlank(appBizMarketingDto.getStartTime())) {
			return false;
		}
		
		if (AppUtils.isBlank(appBizMarketingDto.getEndTime())) {
			return false;
		}
		if (appBizMarketingDto.getStartTime().getTime() > appBizMarketingDto.getEndTime().getTime()) {
			LOGGER.info("活动开始时间要小于结束时间");
			return false;
		}
		if (AppUtils.isBlank(appBizMarketingDto.getIsAllProds())) {
			return false;
		}
		if (appBizMarketingDto.getIsAllProds().equals(0)) {
//			判断选择部分商品 sku id 集合
			if (AppUtils.isBlank(appBizMarketingDto.getSkuIds())){
				LOGGER.info("sku集合为 null");
				return false;
			}
		}
		if (AppUtils.isBlank(appBizMarketingDto.getType())) {
			return false;
		}
		if (AppUtils.isBlank(appBizMarketingDto.getManjianRule()) && AppUtils.isBlank(appBizMarketingDto.getManzeRule())) {
			return false;
		}
		if(appBizMarketingDto.getType().equals(0)){
//			折扣类型 0:满减 1:满折 2:限时折扣 3.直降',
			if(AppUtils.isBlank(appBizMarketingDto.getManjianRule())){
				LOGGER.info("满减规则异常");
				return false;
			}
			for ( String s : appBizMarketingDto.getManjianRule() ) {
				// 解决用户过长时间不操作导致前端清空参数缓存，然后将undefined转过来
				if (s.contains("undefined")) {
					LOGGER.info("满减规则参数异常， {}", s);
					return false;
				}
				String[] split = s.split(",");
				String price=split[0];
				String Mjprice=split[1];
				if(Double.valueOf(Mjprice)>Double.valueOf(price)){
					LOGGER.info("满减规则异常");
					return false;
				}
			}

		}else if(appBizMarketingDto.getType().equals(1)){
			if(AppUtils.isBlank(appBizMarketingDto.getManzeRule())){
				LOGGER.info("满折规则异常");
				return false;
			}
			for (String s : appBizMarketingDto.getManzeRule()) {
				if (s.contains("undefined")) {
					LOGGER.info("满折规则参数异常， {}", s);
					return false;
				}
			}
		}
		return true;
	}

}