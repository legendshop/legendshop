/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.dao.AppBizTokenDao;
import com.legendshop.app.biz.service.AppBizTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.model.entity.AppToken;
import com.legendshop.util.AppUtils;

/**
 * The Class AppBizTokenServiceImpl.
 */
@Service("appBizTokenService")
public class AppBizTokenServiceImpl implements AppBizTokenService {

    @Autowired
    private AppBizTokenDao appBizTokenDao;

    @Override
    public AppToken getAppTokenByUserId(String userId) {
        if(userId == null) {
            return null;
        }
        return appBizTokenDao.getAppTokenByUserId(userId);
    }

    @Override
    public AppToken getAppToken(Long id) {
        return appBizTokenDao.getAppToken(id);
    }

    @Override
    public void deleteAppToken(AppToken appToken) {
        appBizTokenDao.deleteAppToken(appToken);
    }

    @Override
    public Long saveAppToken(AppToken appToken) {
        if (!AppUtils.isBlank(appToken.getId())) {
            updateAppToken(appToken);
            return appToken.getId();
        }
        return appBizTokenDao.saveAppToken(appToken);
    }

    @Override
    public void updateAppToken(AppToken appToken) {
        appBizTokenDao.updateAppToken(appToken);
    }

    @Override
    public boolean deleteAppToken(String userId, String accessToken) {
        return appBizTokenDao.deleteAppToken(userId, accessToken);
    }

    @Override
    public boolean deleteAppToken(String userId) {
        return appBizTokenDao.deleteAppToken(userId);
    }


    /**
     * 根据userId、shopId和平台类型获取token
     * @param userId 用户Id
     * @param shopId 商家Id
     * @param platform 平台类型
     * @return
     */
    @Override
    public AppToken getAppToken(String userId, Long shopId, String platform) {

        if(userId == null) {
            return null;
        }
        return appBizTokenDao.getAppTokenByUserId(userId,shopId,platform);
    }

    /**
     * 删除商家端token
     * @param userId 用户ID
     * @param shopId 商家ID
     * @return
     */
    @Override
    public boolean deleteAppTokeByshopId(String userId, Long shopId) {

        return appBizTokenDao.deleteAppTokeByshopId(userId, shopId);
    }


}
