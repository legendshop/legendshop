package com.legendshop.app.biz.service;

import com.legendshop.biz.model.dto.order.AppBizStoreDto;

public interface AppBizStoreService {

	/**
	 * 获取门店信息
	 * @param storeId 门店Id
	 * @return
	 */
	public AppBizStoreDto getStore(Long storeId);

}
