package com.legendshop.app.biz.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.legendshop.app.biz.service.AppBizTransportService;
import com.legendshop.biz.model.dto.AppBizTransportDto;
import com.legendshop.biz.model.dto.AppBizTransportInfoDto;
import com.legendshop.business.dao.TransportDao;
import com.legendshop.model.entity.Transport;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 27158
 */
@Service
public class AppBizTransportServiceImpl implements AppBizTransportService {
	@Autowired
	private TransportDao transportDao;
	/**
	 * app 商家获取运费模板
	 *
	 * @param shopId
	 * @return
	 */
	@Override
	public List<AppBizTransportDto> getTransports(Long shopId) {
		List<Transport> transports = transportDao.getTransports(shopId);
		if(AppUtils.isNotBlank(transports)){
			List<AppBizTransportDto> list=new ArrayList<>();
			for (Transport transport : transports) {
				AppBizTransportDto appBizTransportDto = new AppBizTransportDto();
				BeanUtil.copyProperties(transport,appBizTransportDto);
				list.add(appBizTransportDto);
			}
			return list;
		}
		return null;
	}

	@Override
	public AppBizTransportInfoDto getDetailedTransport(Long shopId, Long id)
	{
		AppBizTransportInfoDto appBizPublishProductDto = new AppBizTransportInfoDto();
		Transport detailedTransport = transportDao.getDetailedTransport(shopId, id);
		BeanUtil.copyProperties(detailedTransport,appBizPublishProductDto);
		return appBizPublishProductDto;
	}

}
