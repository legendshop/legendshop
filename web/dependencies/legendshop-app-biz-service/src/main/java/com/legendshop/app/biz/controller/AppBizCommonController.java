package com.legendshop.app.biz.controller;
import com.legendshop.app.biz.constants.SendSMSCodeSourceEnum;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSMSEvent;
import com.legendshop.base.event.ShortMessageInfo;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.model.constant.AttachmentTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.User;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 公共服务接口，上传图片，删除图片等
 */
@Api(tags="公共服务",value="一些公共的服务接口")
@RestController
@RequestMapping
public class AppBizCommonController{

	/** The log. */
	private final Logger log = LoggerFactory.getLogger(AppBizCommonController.class);

	@Autowired
	private AttachmentManager attachmentManager;

	@Autowired
	private UserDetailService userDetailService;

	/**
	 * 短信发送者
	 */
	@Resource(name = "sendSMSProcessor")
	private EventProcessor<ShortMessageInfo> sendSMSProcessor;

	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	/**
	 * 发送短信验证码
	 * @param mobile 手机号
	 * @param userName 用户名, 当source是注册时, 才需要传
	 * @param source 发送来源, reg: 注册, forget: 忘记密码, updatePhone: 修改手机号,updatePasswd：修改登录密码 withdraw: 提现, other: 其他
	 * @return
	 */
	@ApiOperation(value = "发送短信验证码", httpMethod = "POST", notes = "发送短信验证码",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "mobile", value = "手机号码, 当来源是提现, 修改密码, 修改手机号, 自己时不需要传", required = false, dataType = "string"),
			@ApiImplicitParam(paramType="query", name = "userName", value = "用户名, 当source是注册时, 才需要传", required = false, dataType = "string"),
			@ApiImplicitParam(paramType="query", name = "passportIdKey", value = "用户名, 当source是绑定手机号时, 才需要传", required = false, dataType = "long"),
			@ApiImplicitParam(paramType="query", name = "source", value = "发送来源, reg: 注册, smsLogin: 手机号登录, forget: 忘记密码, updatePhone: 修改手机号, withdraw: 提现, updatePasswd: 修改登录密码, updatePayPasswd, bindPhone: 绑定手机号, self: 自己, other: 其他", required = true, dataType = "string")
	})
	@PostMapping("/sendSMSCode")
	public  ResultDto<Object> sendSMSCode(String mobile, String userName, String passportIdKey,
										  @RequestParam(defaultValue = "other") String source){

		if(!SendSMSCodeSourceEnum.inSendSMSCodeSourceEnum(source)){
			return ResultDtoManager.fail(-1, "非法参数");
		}

		if(SendSMSCodeSourceEnum.WITHDRAW.value().equals(source)
				|| SendSMSCodeSourceEnum.UPDATE_PASSWD.value().equals(source)
				|| SendSMSCodeSourceEnum.UPDATE_PAY_PASSWD.value().equals(source)
				|| SendSMSCodeSourceEnum.SELF.value().equals(source)
				|| SendSMSCodeSourceEnum.UPDATE_PHONE.value().equals(source)
		){//如果是提现,修改密码等操作, 则取用户登录的手机号码发送

			if(AppUtils.isNotBlank(mobile)){
				return ResultDtoManager.fail(-1, "非法参数");
			}

			//获取用户登录信息
			LoginedUserInfo token = loginedUserService.getUser();
			if(null == token){
				return ResultDtoManager.fail(-1, "您还未登录或登录已失效,请重新登录");
			}

			String userId = token.getUserId();
			mobile = userDetailService.getUserMobile(userId);
			if(AppUtils.isBlank(mobile)){
				return ResultDtoManager.fail(-1, "您登录的用户不存在");
			}

		}else{

			if(AppUtils.isBlank(mobile)){
				return ResultDtoManager.fail(-1, "请输入手机号");
			}

			boolean existed = userDetailService.isPhoneExist(mobile);

			if(SendSMSCodeSourceEnum.REG.value().equals(source)){
				// 检查是否有已存在的手机号码, 因为是注册, 所以不允许存在
				if(existed){
					return ResultDtoManager.fail(-1, "您要注册的手机号已存在");
				}

				if(AppUtils.isBlank(userName)){
					return ResultDtoManager.fail(-1, "请输入用户名");
				}

				if(userDetailService.isUserExist(userName)){
					return ResultDtoManager.fail(-2, "用户名已存在");
				}

			}else if(SendSMSCodeSourceEnum.SMS_LOGIN.value().equals(source)){

				// 检查是否不存在, 因为是短信登录,所以一定要存在
				if(!existed){
					return ResultDtoManager.fail(-1, "此手机号未注册");
				}
				// 判断是不是商家
				User user = userDetailService.getUserByMobile(mobile);
				if (AppUtils.isBlank(user.getShopId())){
					return ResultDtoManager.fail(-1, "该用户未开店，请登录商家账号");
				}

			}else if(SendSMSCodeSourceEnum.FORGET.value().equals(source)){
				// 检查是否不存在, 因为是忘记密码,所以一定要存在
				if(!existed){
					return ResultDtoManager.fail(-1, "此手机号未注册");
				}
			}else if(SendSMSCodeSourceEnum.UPDATE_PHONE.value().equals(source)){
				if(existed){
					return ResultDtoManager.fail(-1, "您要修改的手机号已存在");
				}
			}else if(SendSMSCodeSourceEnum.BIND_PHONE.value().equals(source)){
				//绑定手机号

			}else if(SendSMSCodeSourceEnum.OTHER.value().equals(source)){

			}
		}

		if(AppUtils.isBlank(userName)){
			userName = mobile;
		}

		//发送短信
		String mobileCode = CommonServiceUtil.getRandomSMSCode();
		sendSMSProcessor.process(new SendSMSEvent(mobile,mobileCode,userName).getSource());

		return  ResultDtoManager.success(Constants.SUCCESS);
	}


	/**
	 * 上传图片
	 * @param files
	 * @return
	 */
	@ApiOperation(value = "上传图片", httpMethod = "POST", notes = "上传图片, 上传成功后, 返回图片的路径集合")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "files", paramType = "query", value = "上传的图片文件, 支持多张一起上传,返回图片路径集合", required = true, dataType = "file"),
	})
	@PostMapping("/s/upload")
	public ResultDto<List<String>> upload(MultipartFile[] files) {

		if(AppUtils.isBlank(files)){
			return ResultDtoManager.fail(0,"未选择任何图片");
		}

		try {
			log.info("Uploading file");
			LoginedUserInfo user = loginedUserService.getUser();

			List<String> pics = new ArrayList<String>();
			for(int i=0;i<files.length;i++){
				if(AppUtils.isNotBlank(files[i])){
					if(!files[i].isEmpty()){
						log.info("Uploading file name is {}", files[i].getName());
						String path = attachmentManager.upload(files[i]);
						attachmentManager.saveImageAttachment(user.getUserName(), user.getUserId(), user.getShopId(), path, files[i], AttachmentTypeEnum.IMGFILE);
						pics.add(path);
					}
				}else{
					return ResultDtoManager.fail(0,"未选择任何图片");
				}
			}
			if(AppUtils.isBlank(pics)){
				log.info("Uploading file failed");
				return ResultDtoManager.fail(0,"上传失败");
			}
			return ResultDtoManager.success(pics);
		} catch (Exception e) {
			log.error("", e);
			return ResultDtoManager.fail(-1,"系统异常, 上传图片失败~");
		}
	}


	/**
	 * 删除图片
	 * @param picPath
	 * @return
	 */
	@ApiOperation(value = "删除图片", httpMethod = "POST", notes = "删除图片,返回'OK'表示成功，返回'fail'表示失败")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "picPath",required=true, value = "图片路径的集合",paramType="query", dataType = "String[]"),
	})
	@PostMapping("/s/deletePic")
	public ResultDto<Object> deletePic(@RequestParam String[] picPath) {

		try {
			for(int i=0;i<picPath.length;i++){
				attachmentManager.delAttachmentByFilePath(picPath[i]);
			}
			return ResultDtoManager.success();
		} catch (Exception e) {
			log.error("", e);
			return ResultDtoManager.fail(-1,"系统异常, 删除图片失败~");
		}
	}

}
