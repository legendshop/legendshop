package com.legendshop.app.biz.service;

import com.legendshop.biz.model.dto.productAttribute.*;

import java.util.List;

/**
 * app 商家端商品属性服务
 * @author 27158
 */

public interface AppBizProductAttributeService {

	/**
	 *	app 商家端获取平台商品属性
	 * @param categoryId
	 */
	AppBizPublishProductDto getAdminProdParameter(Long categoryId);

	/**
	 *  app 商家端获取平台商品规格属性
	 * @param categoryId
	 * @return
	 */
	AppBizPublishProductDto getAdminSpecification(Long categoryId);

	/**
	 * 取商家自定义规格属性列表
	 * @param customAttribute
	 * @return
	 */
	List<AppBizAttdefDto> applyProperty(String originUserProperties, String customAttribute, String userName);

	/**
	 * 获取商家定义的参数
	 * @param shopId
	 * @return
	 */
	List<AppBizProdShopParamDto> getShopProdParameter(Long shopId);

	/**
	 * 根据参数id获取平台参数的值
	 * @param parmaId
	 * @return
	 */
	List<AppBizAttdefPropertyValueDto> getProdParameterValueList(Long parmaId);

	/**
	 * 获取商家定义的规格属性
	 * @param shopId
	 * @return
	 */
	List<AppBizProdShopAttributeDto> getShopProdAttribute(Long shopId);

	/**
	 * 根据参数id获取商家参数的值
	 * @param parmaId
	 * @return
	 */
	List<AppBizProdShopParamDto> getShopParaValueList(Long parmaId, String name);

	/**
	 * 根据规格id获取商家规格的值
	 * @param attrId
	 * @return
	 */
	List<AppBizProdShopAttributeDto> getShopAttrValueList(Long attrId, String name);
}
