package com.legendshop.app.biz.controller;

import com.legendshop.app.biz.dto.AppBizProductDetailDto;
import com.legendshop.app.biz.service.AppBizProductService;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.util.PageUtils;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.SendArrivalInform;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.constant.VisitSourceEnum;
import com.legendshop.model.constant.VisitTypeEnum;
import com.legendshop.model.dto.ParamGroupDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.*;
import com.legendshop.processor.SendArrivalInformProcessor;
import com.legendshop.processor.VisitLogProcessor;
import com.legendshop.spi.service.*;
import com.legendshop.util.AppUtils;
import com.legendshop.web.helper.IPHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 商品操作
 *
 */
@Api(tags="商品",value="商品操作")
@RestController
@SuppressWarnings("all")
public class AppBizProductController {
	
	private static Logger LOGGER = LoggerFactory.getLogger(AppBizProductController.class);

	@Autowired
	private AppBizProductService appBizProductService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private ShippingActiveService shippingActiveService;

	@Autowired
	private ProductCommentStatService productCommentStatService;

	@Autowired
	private ShopCommStatService shopCommStatService;

	@Autowired
	private DvyTypeCommStatService dvyTypeCommStatService;

	@Autowired
	private CouponService couponService;

	@Autowired
	private UserCouponService userCouponService;

	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Autowired
	private VisitLogProcessor visitLogProcessor;

	@Autowired
	private ProductService productService;

	@Autowired
	private PropertiesUtil propertiesUtil;

	@Autowired
	private MyfavoriteService myfavoriteService;

	@Autowired
	private SkuService skuService;

	@Autowired
	private SendArrivalInformProcessor sendArrivalInformProcessor;

	@Autowired
	private StockLogService stockLogService;


	/**
	 * 获取 商品信息
	 * @param prodId
	 * @return
	 */
	@ApiOperation(value = "获取商品详情", httpMethod = "POST", notes = "获取商品的详细信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="path", name = "prodId", value = "商品Id", required = true, dataType = "Long")
	@PostMapping(value="/productDetail/{prodId}")
	public ResultDto<AppBizProductDetailDto> getProdDetail(HttpServletRequest request, @PathVariable Long prodId){
		Float shopscore=0f;
		Float prodscore=0f;
		Float logisticsScore=0f;
		AppBizProductDetailDto productDetailDto = appBizProductService.getProductDetailDto(prodId);

		ResultDto<AppBizProductDetailDto> dto = ResultDtoManager.success();
		if(productDetailDto == null){
			dto.setMsg("Prod not found by id " + prodId);
		}else{
			//判断 生效时间
			if(ProductStatusEnum.PROD_ONLINE.value().equals(productDetailDto.getStatus())){
				Date startDate = productDetailDto.getStartDate();
				Date now = new Date();
				//还没生效
				if(startDate!=null && startDate.after(now)){
					request.setAttribute("invalid", true);
				}
			}
			
			//查询店铺的详情信息
			ShopDetail shopDetail = null;
			if(productDetailDto.getShopId() != null){
				 shopDetail = shopDetailService.getShopDetailById(productDetailDto.getShopId());
			}
			
			if(shopDetail!=null){
				
				// 保存商家信息
				productDetailDto.setShopId(shopDetail.getShopId());
				productDetailDto.setShopName(shopDetail.getSiteName());
				productDetailDto.setShopLogo(shopDetail.getShopPic());
				productDetailDto.setShopType(shopDetail.getShopType());

				String mailfeeStr = shippingActiveService.queryShopMailfeeStr(shopDetail.getShopId(),prodId);

				if(mailfeeStr != null){
					productDetailDto.setMailfee(mailfeeStr);
				}
				//计算该店铺所有已评分商品的平均分
				ProductCommentStat prodCommentStat = productCommentStatService.getProductCommentStatByShopId(shopDetail.getShopId());
				if(prodCommentStat!=null&&prodCommentStat.getScore()!=null&&prodCommentStat.getComments()!=null){
					prodscore=(float)prodCommentStat.getScore()/prodCommentStat.getComments();
					productDetailDto.setProdscore(new DecimalFormat("0.0").format(prodscore));
				}

				//计算该店铺所有已评分店铺的平均分
				ShopCommStat shopCommentStat = shopCommStatService.getShopCommStatByShopId(shopDetail.getShopId());
				if(shopCommentStat!=null&&shopCommentStat.getScore()!=null&&shopCommentStat.getCount()!=null){
					shopscore=(float)shopCommentStat.getScore()/shopCommentStat.getCount();
					productDetailDto.setShopScore(new DecimalFormat("0.0").format(shopscore));
				}
				//计算该店铺所有已评分物流的平均分
				DvyTypeCommStat  dvyTypeCommStat = dvyTypeCommStatService.getDvyTypeCommStatByShopId(shopDetail.getShopId());
				if(dvyTypeCommStat!=null&&dvyTypeCommStat.getScore()!=null&&dvyTypeCommStat.getCount()!=null){
					logisticsScore=(float)dvyTypeCommStat.getScore()/dvyTypeCommStat.getCount();
					productDetailDto.setLogisticsScore(new DecimalFormat("0.0").format(logisticsScore));
				}
				Float sum=0f;
				if (AppUtils.isNotBlank(shopscore)&&AppUtils.isNotBlank(prodscore)&&AppUtils.isNotBlank(logisticsScore)) {
					sum=(float)Math.floor((shopscore+prodscore+logisticsScore)/3);
				}else if(AppUtils.isNotBlank(shopscore)&&AppUtils.isNotBlank(logisticsScore)){
					sum=(float)Math.floor((shopscore+logisticsScore)/2);
				}
				productDetailDto.setSum(new DecimalFormat("0.0").format(sum));
			}else{
				dto.setMsg("ShopDetail not found by id " + prodId);
			}
			
			//获取商品的分享url
			String shareUrl = this.getProdShareUrl(prodId);
			productDetailDto.setShareUrl(shareUrl);

			LoginedUserInfo user = loginedUserService.getUser();
			String userId = null;
			if (AppUtils.isNotBlank(user)) {
				userId = user.getUserId();
			}
			
			/*2018-11-12 改造， 查找可用的的商品优惠券
			 * 获取商品相关优惠券
			 */
			List<Coupon> couponList = couponService.getShopAndProdsCoupons(productDetailDto.getShopId(), prodId);
			List<Coupon> resultList = new ArrayList<Coupon>();
			List<Coupon> userResultList = new ArrayList<Coupon>();
			if (AppUtils.isNotBlank(userId)) {
				for (Coupon coupon : couponList) {
					UserCoupon usercoupon = userCouponService.getUsercoupon(coupon.getCouponId(), userId);
					if (AppUtils.isNotBlank(usercoupon)) {
						userResultList.add(coupon);
					} else {
						resultList.add(coupon);
					}
				}
			} else {
				// 未登录默认将所有可用优惠券放入可领列表中
				resultList.addAll(couponList);
			}

			productDetailDto.setCouponList(resultList);
			productDetailDto.setUserCouponList(userResultList);
			
			dto.setResult(productDetailDto);
			
			// 更新查看次数 TODO 不知道需不需要加
			/*if (PropertiesUtil.getObject(SysParameterEnum.VISIT_HW_LOG_ENABLE, Boolean.class)) {
				productService.updateProdViews(prodId);
			}*/

			LoginedUserInfo token = loginedUserService.getUser();
			
			// 多线程记录访问历史
			if (systemParameterUtil.isVisitLogEnable()) {
				if(null != token){
					String userName = token.getUserName();
					VisitLog visitLog = new VisitLog(IPHelper.getIpAddr(request), userName, shopDetail.getShopId(), shopDetail.getSiteName(), 
							prodId, productDetailDto.getName(), productDetailDto.getPic(), productDetailDto.getCash(), VisitTypeEnum.PROD.value(), VisitSourceEnum.APP.value(), new Date());
					visitLogProcessor.process(visitLog);
				}
			}
		}
		return dto;
	}
	
	/**
	 * 获取商品 参数
	 * @param request
	 * @param prodId
	 * @return
	 */
	@ApiOperation(value = "获取商品参数", httpMethod = "GET", notes = "获取商品参数,无需异步请求，Responses无响应",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "path", dataType = "Long", name = "prodId", value = "商品Id", required = true)
	@GetMapping(value="/getProdParams/{prodId}")
	public ResultDto<List<ParamGroupDto>> getProdParams(HttpServletRequest request, @PathVariable Long prodId) {
		List<ParamGroupDto> prodParams = productService.getParamGroups(prodId);
		return ResultDtoManager.success(prodParams);
	}
	
	private String getProdShareUrl(Long prodId){
		String domain = propertiesUtil.getMobileDomainName();
		String shareUrl = new StringBuilder(domain).append("/views/").append(prodId).toString();
		return shareUrl;
	}
	
	/**
	 * 是否已收藏该商品
	 * @param prodId 
	 * @return
	 */
	@ApiOperation(value = "是否已收藏商品", httpMethod = "POST", notes = "是否已收藏商品,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "prodId", value = "商品id", required = true, dataType = "Long")
	@PostMapping("/isExistsFavorite")
	public ResultDto<Boolean> isExistsFavorite(HttpServletRequest request, @RequestParam Long prodId){
		
		LoginedUserInfo token = loginedUserService.getUser();
		if(null == token){
			return ResultDtoManager.success(false);
		}
		boolean result = myfavoriteService.isExistsFavorite(prodId, token.getUserName());
		return ResultDtoManager.success(result);
	}

	/**
	 * 获取商品库存列表
	 * @param prodId
	 * @return
	 */
	@ApiOperation(value = "获取商品库存列表", httpMethod = "POST", notes = "",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "prodId", value = "商品id", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = true, dataType = "String")
	})
	@PostMapping("/loadSkuStockList")
	public ResultDto<AppPageSupport<Sku>> loadSkuStockList(HttpServletRequest request, @RequestParam Long prodId, @RequestParam String curPageNO){

		LoginedUserInfo token = loginedUserService.getUser();
		if(null == token){
			return ResultDtoManager.fail();
		}
		String prodChange = skuService.isProdChange(prodId);
		if(!Constants.SUCCESS.equals(prodChange)){
			return ResultDtoManager.fail(prodChange);
		}

		PageSupport<Sku> ps = skuService.loadSkuListPage(curPageNO, prodId, null);
		AppPageSupport<Sku> result = PageUtils.convertPageSupport(ps);
		return ResultDtoManager.success(result);
	}

	@ApiOperation(value = "更新商品sku库存", httpMethod = "POST", notes = "修改成功返回实际库存，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "skuId", value = "商品skuId", required = true, dataType = "Long"),
			@ApiImplicitParam(paramType="query", name = "stock", value = "库存数", required = true, dataType = "Long")
	})
	@PostMapping("/editUpdateStocks")
	public ResultDto<Long> editUpdateStocks(Long skuId, Long stock) {

		// 检查用户是否登录
		LoginedUserInfo user = loginedUserService.getUser();
		Long shopId = user.getShopId();

		String shopName = shopDetailService.getShopName(shopId);

		Long beforeStock = 0L;
		Long beforeActualStocks = 0L;
		Long actualStocks = 0L ;
		Sku originSku = skuService.getSku(skuId);
		if (!stock.equals(originSku.getStocks())) {
			Long diff = stock - originSku.getStocks();
			beforeStock = originSku.getStocks();
			originSku.setStocks(stock);
			originSku.setBeforeStock(originSku.getStocks());
			originSku.setBeforeActualStock(originSku.getActualStocks());
			if (originSku.getActualStocks() == null) {
				originSku.setActualStocks(0L);
			}
			beforeActualStocks = originSku.getActualStocks();
			originSku.setActualStocks(originSku.getActualStocks() + diff);//实际库存按照虚拟库存的差距来设置
			actualStocks=originSku.getActualStocks();
			skuService.updateSku(originSku);
		}
		//更新库存记录
		StockLog stockLog = new StockLog();
		stockLog.setProdId(originSku.getProdId());
		stockLog.setSkuId(originSku.getSkuId());
		stockLog.setName(originSku.getName());
		if (beforeStock!=0){
			stockLog.setBeforeStock(beforeStock);
		}else{
			stockLog.setBeforeStock(originSku.getStocks());
		}
		if (beforeActualStocks != 0) {
			stockLog.setBeforeActualStock(beforeActualStocks);
		} else {
			stockLog.setBeforeActualStock(originSku.getActualStocks());
		}
		stockLog.setAfterStock(stock);
		stockLog.setUpdateTime(new Date());
		stockLog.setUpdateRemark("修改库存'"+originSku.getName()+"'，商品销售库存为:'" + stock + "'，实际库存为:'" + originSku.getActualStocks() + "'");
		stockLogService.saveStockLog(stockLog);

		// 到货通知事件
		sendArrivalInformProcessor.process(new SendArrivalInform(skuId,stock,shopName));
		updateProdStock(originSku.getProdId());
		return ResultDtoManager.success(actualStocks);
	}
	private void updateProdStock(Long prodId){
		Product product = productService.getProductById(prodId);
		if (AppUtils.isNotBlank(product)){
			Long stocks = 0L;
			Long actualStocks = 0L;
			List<Sku> skuList = skuService.getSkuByProd(product.getProdId());
			if (AppUtils.isNotBlank(skuList)){
				for (Sku sku : skuList) {
					stocks+=sku.getStocks();
					actualStocks+=sku.getActualStocks();
				}
			}
			product.setStocks(Integer.parseInt(stocks+""));
			product.setActualStocks(Integer.parseInt(actualStocks+""));
			productService.updateProd(product);
		}

	}

}
