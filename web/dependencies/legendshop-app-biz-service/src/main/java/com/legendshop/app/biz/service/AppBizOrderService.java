package com.legendshop.app.biz.service;

import com.legendshop.biz.model.dto.order.*;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.order.OrderSearchParamDto;

public interface AppBizOrderService {
	/**
	 * 获取商家订单
	 * @param paramDto
	 * @param condition
	 * @param choose
	 * @return
	 */
	AppPageSupport<AppBizOrderDto> queryShopOrderList(OrderSearchParamDto paramDto, String condition);

	/**
	 * 获取订单详情
	 * @param subNumber
	 * @return
	 */
	AppBizOrderDetailDto queryShopOrderDetail(String subNumber, Long shopId);

	/**
	 * 获取订单价格（原总价/现在总价/运费）
	 * @param subNumber
	 * @return
	 */
	AppBizOrderPriceDto getBizOrderPriceDto(String subNumber);

	/**
	 * 获取订单备注
	 * @param subNumber
	 * @return
	 */
	AppBizOrderRemarkDto getOrderRemarkInfo(String subNumber);

	/**
	 * 获取订单发货信息
	 * @param subNumber
	 * @return
	 */
	AppBizOrderSendCargoDto getOrderSendCargoInfo(String subNumber, Long shopId);
}
