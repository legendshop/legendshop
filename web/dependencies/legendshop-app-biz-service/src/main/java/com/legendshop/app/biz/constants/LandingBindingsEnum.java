package com.legendshop.app.biz.constants;

import com.legendshop.util.constant.StringEnum;

public enum LandingBindingsEnum implements StringEnum{

	/**存在绑定关系*/
	EXISTENT_BINDINGS("existentBindings"),
	
	/**不存在绑定关系*/
	NON_EXISTENT_BINDINGS("nonExistentBindings"),
	
	/**存在用户*/
	EXISTENT_USER("existentUser"),
	
	/**不存在用户*/
	NON_EXISTENT_USER("nonExistentUser"),
	
	/**绑定成功*/
	BINDINGS_SUCCESS("bindingsSuccess"),
	
	/**绑定失败*/
	BINDINGS_FAIL("bindingsFail"),
	
	/**手机号码格式错误*/
	FORMAT_ERROR_MOBILE("formatErrorMobile"),
	
	/**验证码错误*/
	VALIDATE_CODE_ERROR("validateCodeError"),
	
	/**注册失败*/
	LOGIN_HAS_FAILED("loginHasFailed");
	
	
	private final String value;
	
	private LandingBindingsEnum(String value) {
		this.value = value;
	}

	@Override
	public String value() {
		return this.value;
	}
}
