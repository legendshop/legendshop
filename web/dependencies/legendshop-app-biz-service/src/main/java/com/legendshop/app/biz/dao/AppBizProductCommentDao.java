package com.legendshop.app.biz.dao;

import com.legendshop.biz.model.dto.comment.AppBizProductCommentDto;
import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Product;

/**
 * 商家端商品评论DAO
 * @author linzh
 */
public interface AppBizProductCommentDao extends GenericDao<Product, Long> {

	/**
	 * 获取商品评论列表
	 * @param shopId 商家ID
	 * @param curPageNO 当前页码
	 * @param prodName 商品名称
	 * @param orders 排序
	 * @return
	 */
	PageSupport<AppBizProductCommentDto> queryProductCommentList(Long shopId, String curPageNO, String prodName, String orders);
}
