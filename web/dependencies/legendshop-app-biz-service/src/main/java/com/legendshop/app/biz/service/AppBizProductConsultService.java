package com.legendshop.app.biz.service;

import com.legendshop.biz.model.dto.productConsult.AppBizProductConsultDto;
import com.legendshop.biz.model.dto.productConsult.AppBizProductConsultListDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.ProductConsult;

/**
 * 商家端 咨询服务接口
 * @author linzh
 */
public interface AppBizProductConsultService {


	/**
	 * 获取咨询列表
	 * @param shopId
	 * @param curPageNO
	 * @param keywords
	 * @param pointType
	 * @return
	 */
	AppPageSupport<AppBizProductConsultListDto> queryProductConsultList(Long shopId, String curPageNO, String keywords, Integer pointType);

	/**
	 * 获取商品咨询详情
	 * @param consId
	 * @return
	 */
	AppBizProductConsultDto getProductConsultDetail(Long consId);

	/**
	 * 回复商品咨询
	 * @param productConsult
	 */
	void replyProductConsult(ProductConsult productConsult);
}
