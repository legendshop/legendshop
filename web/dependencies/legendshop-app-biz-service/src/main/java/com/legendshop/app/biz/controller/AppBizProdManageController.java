package com.legendshop.app.biz.controller;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.ValueFilter;
import com.legendshop.app.biz.dto.AppBizProdPosterDataDto;
import com.legendshop.app.biz.dto.AppbizDecorateShopCategoryDto;
import com.legendshop.app.biz.service.*;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.util.XssFilterUtil;
import com.legendshop.biz.model.dto.AppBizCategoryDto;
import com.legendshop.biz.model.dto.AppBizTransportDto;
import com.legendshop.biz.model.dto.AppBizTransportInfoDto;
import com.legendshop.biz.model.dto.DistSetDto;
import com.legendshop.biz.model.dto.prod.AppBizProdCategoryDto;
import com.legendshop.biz.model.dto.prod.AppBizProdDto;
import com.legendshop.biz.model.dto.prod.AppBizProductPublishDto;
import com.legendshop.biz.model.dto.prod.AppBizSkusDto;
import com.legendshop.biz.model.dto.productAttribute.*;
import com.legendshop.business.service.weixin.WeixinMiniProgramApi;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.SendArrivalInform;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.SupportDistEnum;
import com.legendshop.model.dto.ProductDto;
import com.legendshop.model.dto.ProductIdDto;
import com.legendshop.model.dto.PublishProductDto;
import com.legendshop.model.dto.TreeNode;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.dto.weixin.WXACodeResultDto;
import com.legendshop.model.entity.*;
import com.legendshop.processor.SendArrivalInformProcessor;
import com.legendshop.promoter.model.DistSet;
import com.legendshop.spi.service.*;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.uploader.model.FeignMultipartFile;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.SafeHtml;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * app商家产品管理控制器
 *
 * @author 27158
 */
@RestController
@Api(tags = "商品管理", value = "商品发布、店铺分类")
public class AppBizProdManageController {
	/**
	 * The log.
	 */
	private final Logger LOGGER = LoggerFactory.getLogger(AppBizProdManageController.class);
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private AppBizShopCategoryService appBizShopCategoryService;

	@Autowired
	private ShopCategoryService shopCategoryService;

	@Autowired
	private AppBizProductService appBizProductService;

	@Autowired
	private ProductService productService;

	@Autowired
	private WeixinMpTokenService weixinMpTokenService;

	@Autowired
	private AttachmentManager attachmentManager;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Resource(name = "sendArrivalInformProcessor")
	private SendArrivalInformProcessor sendArrivalInformProcessor;

	@Resource(name = "productIndexUpdateProcessor")
	private EventProcessor<ProductIdDto> productIndexUpdateProcessor;

	@Resource(name = "productIndexSaveProcessor")
	private EventProcessor<ProductIdDto> productIndexSaveProcessor;

	@Autowired
	private AppBizProductAttributeService appBizProductAttributeService;

	@Autowired
	private AppBizTransportService appBizTransportService;

	@Autowired
	private ProductPropertyService productPropertyService;

	@Autowired
	private CategoryManagerService categoryManagerService;

	@Autowired
	private TransportService transportService;


	@Autowired
	private DistSetService distSetService;

	@Autowired
	private AppBizSkuService appBizSkuService;

	/**
	 * 店铺一级分类列表.
	 *
	 * @return the string
	 */
	@ApiOperation(value = "店铺分类列表", httpMethod = "POST", notes = "店铺分类列表，默认进入一级类目", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", name = "grade", value = "分类层次,一级：1 二级：2 三级：3", required = true, dataType = "integer"),
			@ApiImplicitParam(name = "parentId", paramType = "query", value = "父级ID，当grade为一级时，parentId 不传", required = true, dataType = "Long")
	})
	@PostMapping(value = "/s/shopCat")
	public ResultDto<List<AppBizCategoryDto>> OneShopCat(@RequestParam Integer grade,@RequestParam Long parentId) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		if (AppUtils.isBlank(parentId)&&grade!=1) {
			return ResultDtoManager.success();
		}
		List<AppBizCategoryDto> ps = appBizShopCategoryService.queryShopCategory(shopId, grade, parentId);
		return ResultDtoManager.success(ps);
	}



	/**
	 * 店铺下一级分类列表.
	 *
	 * @param curPageNO the cur page no
	 * @return the string
	 */
	@ApiOperation(value = "店铺下一级分类列表", httpMethod = "POST", notes = "店铺下一级分类列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", name = "parentId", value = "父节点", required = true, dataType = "long")
	})
	@PostMapping(value = "/s/nextShopCat")
	public ResultDto<List<AppBizCategoryDto>> nextShopCat(@RequestParam(required = false) String curPageNO, @RequestParam Long parentId) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		List<AppBizCategoryDto> ps = appBizShopCategoryService.queryShopCategory(shopId, null, parentId);

		return ResultDtoManager.success(ps);
	}

	/**
	 * 保存 店铺分类类目
	 *
	 * @return the string
	 */
	@ApiOperation(value = "保存 店铺分类类目", httpMethod = "POST", notes = "保存 店铺分类类目", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", name = "parentId", value = "父节点,一级分类父节点为空", required = false, dataType = "long"),
			@ApiImplicitParam(paramType = "query", name = "grade", value = "分类层次,一级：1 二级：2 三级：3", required = true, dataType = "integer"),
			@ApiImplicitParam(paramType = "query", name = "name", value = "分类名称", required = true, dataType = "string")
	})
	@PostMapping("/s/saveShopCate")
	public ResultDto<String> saveShopCate(@RequestParam(required = false) Long parentId, @RequestParam Integer grade, @RequestParam String name) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		if (AppUtils.isBlank(parentId)) {
//			增加店铺一级分类
			shopCategoryService.saveShopCategory(grade, shopId, name, parentId);
		} else {
			shopCategoryService.saveShopCategory(grade, shopId, name, null);
		}
		return ResultDtoManager.success();
	}


	/**
	 * 商品列表
	 */
	@ApiOperation(value = "商品列表", httpMethod = "POST", notes = "商品列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", name = "curPageNO", value = "当前页面", required = true, dataType = "String"),
			@ApiImplicitParam(paramType = "query", name = "prodName", value = "商品名称", required = false, dataType = "String"),
			@ApiImplicitParam(paramType = "query", name = "status", value = "商品状态：0 仓库中的商品、1 出售中的商品、2 违规商品、3 待审核的商品、 4 审核失败的商品、5 是否分销商品、 -1 回收站的商品、-2 商家永久删除的商品", required = true, dataType = "Integer")
	})
	@PostMapping("/s/productList")
	public ResultDto<AppPageSupport<AppBizProdDto>> productList(@RequestParam String curPageNO, @RequestParam Integer status,String prodName) {
		Long shopId = loginedUserService.getUser().getShopId();
		Integer supportDist = null;
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		// 判断分销
		if (status==5){
			supportDist= SupportDistEnum.SUPPORT.value();
			// 分销商品，并且商品是上线状态才显示
			status = 1;
		}
		AppPageSupport<AppBizProdDto> ps = appBizProductService.queryProductList(curPageNO, shopId, status, supportDist,prodName);
		return ResultDtoManager.success(ps);
	}

	/**
	 * 商品操作
	 *
	 * @param productIds
	 * @param status
	 * @return
	 */
	@ApiOperation(value = "商品操作（单个或批量）", httpMethod = "POST", notes = "商品操作", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", name = "productIds", value = "商品IDS，例：[1,2,3,...,n]", required = true, dataType = "Long[]"),
			@ApiImplicitParam(paramType = "query", name = "status", value = "商品状态：0 下线商品（仓库中的商品）、1 上线商品（出售中的商品）、-1 删除商品（回收站的商品）", required = true, dataType = "Integer"),
	})
	@PostMapping(value = "/s/updateProdStatus")
	public ResultDto<String> updateProdStatus(Long[] productIds,Integer status) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(productIds)) {
			return ResultDtoManager.fail("商品信息为空，请刷新页面重新操作");
		}

		List<Long> productIdList = Arrays.asList(productIds);
		String result = appBizProductService.updateProdStatus(shopId, productIdList, status);
		if (Constants.FAIL.equals(result)) {
			return ResultDtoManager.fail("操作失败，请刷新页面重新操作");
		}
		return ResultDtoManager.success("操作成功");
	}

	@ApiOperation(value = "退出分销（单个或批量）", httpMethod = "POST", notes = "退出分销", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", name = "productIds", value = "商品IDS，例：[1,2,3,...,n]", required = true, dataType = "Long[] "),
			@ApiImplicitParam(paramType = "query", name = "supportDist", value = "分销状态：0 退出分销，1 分销中", required = true, dataType = "Integer"),
	})
	@PostMapping(value = "/s/quiteDistribution")
	public ResultDto<String> quiteDistribution(Long[] productIds,Integer supportDist) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(productIds) || AppUtils.isBlank(supportDist)) {
			return ResultDtoManager.fail("商品参数为空，请刷新页面重新操作");
		}
		List<Long> productIdList = Arrays.asList(productIds);
		String result = appBizProductService.quiteDistribution(shopId, productIdList, supportDist);
		if (Constants.FAIL.equals(result)) {
			return ResultDtoManager.fail("退出分销失败，请刷新页面重试");
		}
		return ResultDtoManager.success("退出分销成功");
	}

	@ApiOperation(value = "删除商品（放入回收站）", httpMethod = "POST", notes = "删除商品（放入回收站）", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "query", name = "prodId", value = "商品ID", required = true, dataType = "Long")
	@PostMapping(value = "/s/toDustbin")
	public ResultDto<String> toDustbin(Long prodId) {

		if (AppUtils.isNotBlank(prodId)) {
			Product product = productService.getProductById(prodId);

			Long shopId = loginedUserService.getUser().getShopId();

			if (product.getShopId().equals(shopId)) {
				productService.updateStatus(Constants.STOPUSE, prodId, null);
				return ResultDtoManager.success("商品已加入回收站");
			}
		}
		return ResultDtoManager.fail("删除商品失败，请刷新后重试");
	}


	@ApiOperation(value = "修改商品店铺分类（单个或批量）", httpMethod = "POST", notes = "修改商品店铺分类", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", name = "productIds", value = "商品IDS，例：[1,2,3,...,n]", required = true, dataType = "Long[]"),
			@ApiImplicitParam(paramType = "query", name = "firstCatId", value = "一级分类ID", required = false, dataType = "Long"),
			@ApiImplicitParam(paramType = "query", name = "secondCatId", value = "二级分类ID", required = false, dataType = "Long"),
			@ApiImplicitParam(paramType = "query", name = "thirdCatId", value = "三级分类ID", required = false, dataType = "Long"),
	})
	@PostMapping(value = "/s/updateShopCategory")
	public ResultDto<String> updateShopCategory(Long[] productIds,  Long firstCatId,  Long secondCatId,  Long thirdCatId) {
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(productIds)) {
			return ResultDtoManager.fail("商品参数为空，请刷新页面重新操作");
		}
		List<Long> productIdList = Arrays.asList(productIds);
		String result = appBizProductService.updateShopCategory(shopId, productIdList, firstCatId, secondCatId, thirdCatId);
		if (Constants.FAIL.equals(result)) {
			return ResultDtoManager.fail("修改分类失败，请刷新页面重试");
		}
		return ResultDtoManager.success("修改分类成功");
	}


	/**
	 * 获取生成普通商品海报相关的数据
	 *
	 * @param prodId
	 * @return
	 */
	@ApiOperation(value = "获取生成普通商品海报相关的数据", notes = "获取生成普通商品海报相关的数据", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "prodId", paramType = "query", value = "商品Id", required = true, dataType = "Long"),
/*		@ApiImplicitParam(name = "userName", paramType = "query", value = "商品Id", required = true, dataType = "Long"),
		@ApiImplicitParam(name = "sign", paramType = "query", value = "商品Id", required = true, dataType = "Long"),*/
	})
	@PostMapping("/getProdPosterData")
	public ResultDto<AppBizProdPosterDataDto> getProdPosterData(@RequestParam(required = true) Long prodId) {
		try {
			/** 获取小程序码 */
			Product product = productService.getProductById(prodId);
			if (null == product) {
				return ResultDtoManager.fail("商品不存在!");
			}
			String shopWxCode = product.getShopWxCode();
			if (AppUtils.isBlank(shopWxCode)) {
				String accessToken = weixinMpTokenService.getAccessToken();
				String scene = "prodId=" + prodId;
				String page = "commonModules/goodsDetail/goodsDetail";
				WXACodeResultDto wxACodeResult = WeixinMiniProgramApi.getWXACodeUnlimit(accessToken, scene, page, null, null, null, null);
				if (!wxACodeResult.isSuccess()) {//说明失败

					WXACodeResultDto.Errormsg error = wxACodeResult.getErrormsg();
					if (null == error) {
						return ResultDtoManager.fail("获取二维码错误, 未知错误!");
					}

					return ResultDtoManager.fail(error.getErrmsg());
				}

				//说明成功返回二维码图片
				// TODO 这里不应该这么去实现, 会太多图片上传到图片服务器
				ByteArrayOutputStream output = wxACodeResult.getOutputStream(); //ByteArrayOutputStream 无需close
				shopWxCode = attachmentManager.upload(new FeignMultipartFile("SHOP_WX_CODE_PROD" + prodId + ".jpg", output.toByteArray()));

				if (AppUtils.isBlank(shopWxCode)) {
					return ResultDtoManager.fail("获取二维码错误, 未知错误!");
				}

				/** 更新到商品 */
				product.setShopWxCode(shopWxCode);
				productService.updateProd(product);
			}
			AppBizProdPosterDataDto prodPosterData = new AppBizProdPosterDataDto();

			/** 获取用户头像,昵称 */
			LoginedUserInfo token = loginedUserService.getUser();
			if (null != token) {
				UserDetail userDetail = userDetailService.getUserDetailById(token.getUserId());
				prodPosterData.setHeadPortrait(userDetail.getPortraitPic());
				prodPosterData.setNickName(userDetail.getNickName());
			}
			prodPosterData.setShopWxCode(shopWxCode);
			return ResultDtoManager.success(prodPosterData);

		} catch (Exception e) {
			LOGGER.error("获取生成商品海报相关的数据错误: ", e);
			return ResultDtoManager.fail("未知错误!");
		}
	}

	/**
	 * 店铺商品分类
	 *
	 * @param shopId
	 * @return
	 */
	@ApiOperation(value = "获取店铺商品分类", httpMethod = "POST", notes = "店铺商品分类", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "path", name = "shopId", value = "店铺id", required = true, dataType = "Long")
	@PostMapping(value = "/shop/prodCategory/{shopId}")
	public ResultDto<List<AppbizDecorateShopCategoryDto>> shopCategory(@PathVariable Long shopId) {
		try {
			List<AppbizDecorateShopCategoryDto> shopCategoryList = appBizShopCategoryService.getShopCategoryDtoList(shopId);

			//组装树结构
			List<AppbizDecorateShopCategoryDto> tree = getTree(0L,shopCategoryList);

			if (AppUtils.isBlank(tree)) {
				return ResultDtoManager.fail(-1, "该店铺暂没有商品分类信息");
			}
			return ResultDtoManager.success(tree);
		} catch (Exception e) {
			LOGGER.error("获取店铺商品分类异常!", e);
			return ResultDtoManager.fail("获取店铺商品分类异常");
		}
	}

	/**
	 * 发布商品，获取平台等级分类
	 *
	 * @param grade
	 * @param parentId
	 * @return the string
	 */
	@ApiOperation(value = "发布商品，获取平台等级分类", notes = "发布商品，获取平台等级分类", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "grade", paramType = "query", value = "分类等级：1 一级、2 二级、3 三级", required = true, dataType = "Integer"),
			@ApiImplicitParam(name = "parentId", paramType = "query", value = "父级ID，当grade为一级时，parentId = null", required = true, dataType = "Long"),
	})
	@PostMapping(value = "/s/platformGradeCategory")
	public ResultDto<List<AppBizProdCategoryDto>> platformGradeCategory(@RequestParam Integer grade, @RequestParam Long parentId) {
		if (AppUtils.isBlank(parentId)) {
			parentId= 0L;
		}
		List<AppBizProdCategoryDto> categoryDtoList = appBizProductService.platformGradeCategory(grade, parentId);
		if (AppUtils.isBlank(categoryDtoList)) {
			return ResultDtoManager.success(null);
		}
		return ResultDtoManager.success(categoryDtoList);
	}

	/**
	 * 获取下一级商品分类.
	 *
	 * @param categoryId
	 * @return the string
	 */
	@ApiOperation(value = "获取下一级商品分类", notes = "获取下一级商品分类", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "categoryId", paramType = "query", value = "分类ID", required = true, dataType = "Long"),
	})
	@PostMapping(value = "/s/nextCategory/{categoryId}")
	public ResultDto<List<AppBizProdCategoryDto>> nextCategory(@PathVariable Long categoryId) {
		List<AppBizProdCategoryDto> categoryDtoList = appBizProductService.nextCategory(categoryId);
		if (AppUtils.isBlank(categoryDtoList)) {
			return ResultDtoManager.fail("查找不到下一级分类");
		}
		return ResultDtoManager.success(categoryDtoList);
	}

	/**
	 * 保存商品 1、检查用户是否登录 2、检查商品是否有图片 3、对商品sku的保存.
	 *
	 * @param productPublishDto  商品
	 * @return 返回页面
	 */
	@ApiOperation(value = "发布商品", notes = "发布商品", httpMethod = "POST",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value = "/s/saveProd")
	public ResultDto<String> saveProd(@RequestBody AppBizProductPublishDto productPublishDto) {
		// 检查用户是否登录
		LoginedUserInfo userInfo = loginedUserService.getUser();
		if(AppUtils.isBlank(userInfo)){
			return ResultDtoManager.fail(-1,"请先登入");
		}
		if (AppUtils.isBlank(userInfo.getShopId())) {
			return ResultDtoManager.fail(-1,"该用户不是商家");
		}
		String userId = userInfo.getUserId();
		String userName = userInfo.getUserName();
		Long shopId = userInfo.getShopId();
		ShopDetail shopDetail = shopDetailService.getShopDetailByIdNoCache(shopId);
		if (AppUtils.isBlank(shopDetail)) {
			return ResultDtoManager.fail("发布商品失败，店铺信息异常");
		}
//		String userProperties = productPublishDto.getUserProperties();
//		if(AppUtils.isNotBlank(userProperties)){
//			String replace = userProperties.replace("appBizProductPropertyDto", "productProperty");
//			productPublishDto.setUserProperties(replace);
//		}
		//copy 参数
		Product product = new Product();
		// 封装 Product 实体类
		BeanUtil.copyProperties(productPublishDto, product);

		// 由于调dao的save方法会set进去prodId。所以提前标志
		boolean hasProdId = AppUtils.isNotBlank(product.getProdId());

		if (!hasProdId){
			// 设置商品信息 默认参数
			product.setViews(0);
			product.setVolume(0D);
			product.setWeight(0D);
			product.setReviewScores(0);
			product.setIsGroup(0);
			product.setHasInvoice(0);
			product.setHasGuarantee(0);
			product.setComments(0);
			product.setBuys(0);
			product.setAvgScores(0);
			product.setActualStocks(0);
			product.setIsSupportCod(0);

			// 构造默认sku
			Sku sku = new Sku();
			sku.setName(product.getName());
			sku.setPrice(product.getCash());
			sku.setStocks(product.getStocks().longValue());
			sku.setActualStocks(product.getStocks().longValue());
			sku.setPic(product.getPic());
			sku.setStatus(1);

			List<Sku> skus = new ArrayList<>();
			skus.add(sku);
			product.setSkus(JSONUtil.getJson(skus));
		}

		
		// 封装sku 商品 ;
//		if (AppUtils.isNotBlank(productPublishDto.getSkuDtoList())){
//			List<AppBizSkusDto> skuList = productPublishDto.getSkuDtoList();
//			// 空字段 转为 ""
//			String skus = JSON.toJSONString(skuList, new ValueFilter() {
//				@Override
//				public Object process(Object obj, String s, Object v) {
//					if (v == null){
//						return "";
//					}
//					return v;
//				}
//			});
//			product.setSkus(skus);
//		}
		//过滤html代码
		product.setName(XssFilterUtil.cleanXSS(product.getName()));

		// 发布商品
		Long prodId = productService.releaseOfProduct(shopId, userId, userName, product);
		//到货通知需要重新设计

		//发送到货通知消息
		List<Sku> list = product.getUpdateSkuList();
		if (list != null) {
			for (Sku sku : list) {
				long skuId = sku.getSkuId();
				long stocks = sku.getStocks();
				sendArrivalInformProcessor.process(new SendArrivalInform(skuId, stocks, shopDetail.getSiteName()));
			}
		}
// 		判断发布商品还是编辑商品
		if (hasProdId) {
			//如果商品ID不为空，更新商品索引
			productIndexUpdateProcessor.process(new ProductIdDto(product.getProdId()));
		} else {
			product.setId(prodId);
			//创建商品后 ，索引数据
			productIndexSaveProcessor.process(new ProductIdDto(product.getProdId()));
		}

		return ResultDtoManager.success("操作成功！！！");
	}


	/**
	 * 获取平台商品参数列表
	 */
	@ApiOperation(value = "获取平台商品参数列表", httpMethod = "POST", notes = "获取平台商品参数列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query",name = "categoryId",value = "平台商品分类ID",dataType = "long",required = true)
	})
	@PostMapping("/s/getAdminProdParameter")
	public ResultDto<AppBizPublishProductDto> getAdminProdParameter(@RequestParam Long categoryId){
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		AppBizPublishProductDto appBizPublishProductDto = appBizProductAttributeService.getAdminProdParameter(categoryId);
		return ResultDtoManager.success(appBizPublishProductDto);
	}



	/**
	 * 获取平台商品规格属性列表
	 */
	@ApiOperation(value = "获取平台商品规格属性列表", httpMethod = "POST", notes = "获取平台商品规格属性列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query",name = "categoryId",value = "平台商品分类ID",dataType = "long",required = true)
	})
	@PostMapping("/s/getAdminSpecification")
	public ResultDto<AppBizPublishProductDto> getAdminSpecification(@RequestParam Long categoryId){
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}

		AppBizPublishProductDto appBizPublishProductDto = appBizProductAttributeService.getAdminSpecification(categoryId);
		return ResultDtoManager.success(appBizPublishProductDto);
	}


	/**
	 * 获取运费模板
	 */
	@ApiOperation(value = "获取运费模板", httpMethod = "GET", notes = "获取运费模板", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@GetMapping ("/s/getCarriageTemplates")
	public ResultDto<List<AppBizTransportDto>> getCarriageTemplates (){
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		List<AppBizTransportDto> transports=appBizTransportService.getTransports(shopId);
		return ResultDtoManager.success(transports);
	}



	/**
	 * 获取商家商品参数列表
	 */
	@ApiOperation(value = "获取商家商品参数列表", httpMethod = "POST", notes = "获取商家商品参数列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/s/shopProdParameterList")
	public ResultDto<List<AppBizProdShopParamDto>> getShopProdParameter(){
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail(-1,"请先登入");
		}
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		List<AppBizProdShopParamDto> appBizProdShopParams = appBizProductAttributeService.getShopProdParameter(shopId);
		return ResultDtoManager.success(appBizProdShopParams);
	}



	/**
	 * 获取参数的值
	 */
	@ApiOperation(value = "获取平台参数的值", httpMethod = "POST", notes = "获取平台参数的值", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query",name = "parmaId",value = "参数id",dataType = "long",required = true)
	})
	@PostMapping("/s/prodParameterValueList")
	public ResultDto<List<AppBizAttdefPropertyValueDto>> prodParameterValueList(@RequestParam Long parmaId){
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail(-1,"请先登入");
		}
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		List<AppBizAttdefPropertyValueDto> appBizProdShopParams = appBizProductAttributeService.getProdParameterValueList(parmaId);
		return ResultDtoManager.success(appBizProdShopParams);
	}



	/**
	 * 获取参数的值
	 */
	@ApiOperation(value = "获取商家参数的值", httpMethod = "POST", notes = "获取商家参数的值", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query",name = "parmaId",value = "参数id",dataType = "long",required = true),
			@ApiImplicitParam(paramType = "query",name = "name",value = "参数名",dataType = "String",required = true)
	})
	@PostMapping("/s/prodShopParaValueList")
	public ResultDto<List<AppBizProdShopParamDto>> prodShopParaValueList(@RequestParam Long parmaId,@RequestParam String name){
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail(-1,"请先登入");
		}
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		List<AppBizProdShopParamDto> appBizProdShopParams = appBizProductAttributeService.getShopParaValueList(parmaId,name);
		return ResultDtoManager.success(appBizProdShopParams);
	}



	@ApiOperation(value = "保存商家自定义参数模板", httpMethod = "POST", notes = "保存商家自定义参数", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query",name = "propId",value = "参数id(新增的时候不用传)",dataType = "Long",required = false),
			@ApiImplicitParam(paramType = "query",name = "content",value = "json字符串[{\"key\":\"\",\"value\":\"\"}]",dataType = "String",required = true)
	})
	@PostMapping("/s/saveShopParam")
	public ResultDto<Object> saveShopParam(String content,Long propId) {
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail(-1,"请先登入");
		}
		String userId = user.getUserId();
		Long shopId = loginedUserService.getUser().getShopId();
		if (AppUtils.isBlank(shopId)) {
			return ResultDtoManager.fail(-1, "该用户不是商家");
		}
		List<KeyValueEntity> contentList = JSONUtil.getArray(content, KeyValueEntity.class);
		if (AppUtils.isBlank(contentList)) {
			return ResultDtoManager.fail("json字符串有误！！！");
		}

		boolean haveData = false;
		for (KeyValueEntity entity : contentList) {
			if (AppUtils.isNotBlank(entity.getKey()) || AppUtils.isNotBlank(entity.getValue())) {
				haveData = true;
				break;
			}
		}

		if (!haveData) {
			return ResultDtoManager.fail("json字符串有误！！！");
		}
		if(AppUtils.isNotBlank(propId)){
			ProdUserParam prodUserParam = productService.getProdUserParam(propId);
			if (AppUtils.isBlank(prodUserParam)){
				return ResultDtoManager.fail("参数不存在参数已被修改或者删除！！！");
			}
			prodUserParam.setContent(content);
			productService.updateProdUserParam(prodUserParam);
		}else{
			ProdUserParam prodUserParam = new ProdUserParam();
			prodUserParam.setContent(content);
			prodUserParam.setName("手机端发布参数保存模板");
			prodUserParam.setRecDate(new Date());
			prodUserParam.setUserId(userId);
			prodUserParam.setShopId(shopId);
			productService.saveProdUserParam(prodUserParam);
		}
		return ResultDtoManager.success("操作成功！！！");
	}



	@ApiOperation(value = "保存商家自定义规格属性列表", httpMethod = "POST", notes = "保存商家自定义规格属性列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query",name = "originUserProperties",value = "用户之前自定义属性集合",dataType = "String",required = true),
			@ApiImplicitParam(paramType = "query",name = "customAttribute",value = "json字符串[{\"key\":\"XXX\",\"value\":[\"XXX\",\"XXX\"]}]",dataType = "String",required = true)
	})
	@PostMapping("/s/applyProperty")
	public ResultDto<List<AppBizAttdefDto>> applyProperty(String originUserProperties, String customAttribute) {

		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail(-1,"请先登入");
		}
		String userName = user.getUserName();
		if (AppUtils.isBlank(userName)) {
			return ResultDtoManager.fail(-1, "商家信息错误！！！");
		}
		List<AppBizAttdefDto> resultCustomPropertyDtoList = appBizProductAttributeService.applyProperty(originUserProperties, customAttribute, user.getUserName());
		// 返回结果
		return ResultDtoManager.success(resultCustomPropertyDtoList);
	}



	@ApiOperation(value = "获取修改商品数据", httpMethod = "POST", notes = "获取修改商品数据", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "path",name = "prodId",value = "商品id",dataType = "Long",required = true)
	@PostMapping(value = "/s/updateProd/{prodId}")
	public ResultDto<AppBizUpdateProdDto> updateProd(@PathVariable Long prodId) {
		AppBizUpdateProdDto appBizUpdateProdDto = new AppBizUpdateProdDto();
		// 商品
		Product product = productService.getProductById(prodId);
		if (product == null) {
			return ResultDtoManager.fail("prodId 不能为空");
		}
		// 当前用户的shopId
		LoginedUserInfo user = loginedUserService.getUser();
		Long shopId = user.getShopId();


		if (!shopId.equals(product.getShopId())) {
			return ResultDtoManager.fail("这不是您的商品");
		}

		Long publishCategoryId = null;
		if (AppUtils.isBlank(publishCategoryId)) {
			publishCategoryId = product.getCategoryId();
		}

		boolean isSimilarImport = false;// 如果非空则是类似商品导入

		//1. 获取商品分类下的标准信息
		PublishProductDto publishProductDto = productPropertyService.queryProductProperty(publishCategoryId);

		//2. 计算该商品的属性
		ProductDto productDto = productService.getProductDto(publishProductDto, product, prodId, isSimilarImport);
		List<AppBizTreeNode> appBizTreeNodes = new ArrayList<>();
		List<TreeNode> treeNodes = categoryManagerService.getTreeNodeNavigation(publishCategoryId, ",");
		//获取平台分类
		for (TreeNode treeNode : treeNodes) {
			AppBizTreeNode appBizTreeNode = new AppBizTreeNode();
			appBizTreeNode.setId(treeNode.getSelfId());
			appBizTreeNode.setParentId(treeNode.getParentId());
			appBizTreeNode.setNodeName(treeNode.getNodeName());
			appBizTreeNodes.add(appBizTreeNode);
		}
		//获取店铺分类
		if (AppUtils.isNotBlank(productDto)){
			if (AppUtils.isNotBlank(productDto.getShopFirstCatId())){
				String shopCategoryName = shopCategoryService.getShopCategoryName(productDto.getShopFirstCatId());
				productDto.setShopFirstCatName(shopCategoryName);
				if (AppUtils.isNotBlank(productDto.getShopSecondCatId())){
					String categoryName = shopCategoryService.getShopCategoryName(productDto.getShopSecondCatId());
					productDto.setShopSecondCatName(categoryName);
					if (AppUtils.isNotBlank(productDto.getShopThirdCatId())){
						String thirdCatName = shopCategoryService.getShopCategoryName(productDto.getShopThirdCatId());
						productDto.setShopThirdCatName(thirdCatName);
					}
				}
			}
			if (AppUtils.isNotBlank(productDto.getTransportId())){
				Transport transport = transportService.getTransport(productDto.getTransportId());
				if (AppUtils.isNotBlank(transport)){
					productDto.setTransportName(transport.getTransName());
				}
			}
		}

		appBizUpdateProdDto.setProductDto(productDto);
		appBizUpdateProdDto.setPublishCategoryId(publishCategoryId);
		appBizUpdateProdDto.setTreeNodes(appBizTreeNodes);
		AppBizPublishProductDto appBizPublishProductDto = new AppBizPublishProductDto();
		BeanUtil.copyProperties(publishProductDto,appBizPublishProductDto);
		appBizUpdateProdDto.setAppBizPublishProductDto(appBizPublishProductDto);
		String jsonString = JSON.toJSONString(appBizUpdateProdDto, SerializerFeature.DisableCircularReferenceDetect);
		AppBizUpdateProdDto updateProdDto = JSONUtil.getObject(jsonString, AppBizUpdateProdDto.class);
		return ResultDtoManager.success(updateProdDto);
	}

	@ApiOperation(value = "获取运费模板详情", httpMethod = "POST", notes = "获取运费模板详情", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "path",name = "id",value = "运费模板",dataType = "Long",required = true)
	@PostMapping(value = "/transportInfo/{id}")
	public ResultDto<AppBizTransportInfoDto> transportLoad(@PathVariable Long id) {
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail(-1,"请先登入");
		}
		Long shopId = user.getShopId();

		AppBizTransportInfoDto transport = appBizTransportService.getDetailedTransport(shopId,id);

		return ResultDtoManager.success(transport);
	}


	@ApiOperation(value = "获取平台分销推广设置", httpMethod = "POST", notes = "获取平台分销推广设置", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value = "/distSet")
	public ResultDto<DistSetDto> distSet() {

		DistSet distSet = distSetService.getDistSet(1L);

		DistSetDto distSetDto = new DistSetDto();
		if(AppUtils.isNotBlank(distSet)){

			distSetDto.setSupportDist(distSet.getSupportDist());
			distSetDto.setFirstLevelRate(distSet.getFirstLevelRate());
			distSetDto.setSecondLevelRate(distSet.getSecondLevelRate());
			distSetDto.setThirdLevelRate(distSet.getThirdLevelRate());
		}

		return ResultDtoManager.success(distSetDto);
	}

	@GetMapping(value = "/s/isProdChange")
	@ApiOperation(value = "判断商品是否存在营销活动", httpMethod = "GET", notes = "判断商品是否存在营销活动", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResultDto<String> isProdChange(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "prodId") Long prodId) {
		String prodChange = appBizSkuService.isProdChange(prodId);
		if (Constants.SUCCESS.equals(prodChange)) {
			return ResultDtoManager.success();
		}
		return ResultDtoManager.fail(prodChange);
	}


	//递归遍历树
	private List<AppbizDecorateShopCategoryDto> getTree(Long pid ,List<AppbizDecorateShopCategoryDto> list){

		List<AppbizDecorateShopCategoryDto> tree = new ArrayList<AppbizDecorateShopCategoryDto>();

		for (AppbizDecorateShopCategoryDto shopCategory : list) {

			Long parentId = shopCategory.getParentId();
			if (shopCategory.getParentId() == null) {
				parentId = 0L;
			}
			if (pid.equals(parentId)) {
				shopCategory.setSubCatList(getTree(shopCategory.getId(), list));
				tree.add(shopCategory);
			}
		}
		return tree;
	}
}
