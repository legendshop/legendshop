package com.legendshop.app.biz.service;

import com.legendshop.biz.model.dto.msg.AppBizSiteInformationDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.UserDetail;

/**
 * 商家端消息箱相关服务
 * @author linzh
 */
public interface AppBizMessageService {


	/**
	 * 获取系统消息列表
	 * @param curPageNO 当前页码
	 * @param gradeId 等级ID
	 * @param userName 用户名
	 * @param userDetail 用户详情
	 * @return
	 */
	AppPageSupport<AppBizSiteInformationDto> querySystemMessages(String curPageNO, Integer gradeId, String userName, UserDetail userDetail);

	/**
	 * 获取站内信列表
	 * @param curPageNO 当前页码
	 * @param userName  用户名
	 * @return
	 */
	AppPageSupport<AppBizSiteInformationDto> queryInboxMsg(String curPageNO, String userName);

	/**
	 * 获取用户普通消息详情
	 * @param msgId 消息Id
	 * @param userName 当前登录用户名
	 * @return
	 */
	AppBizSiteInformationDto getMsgDetail(Long msgId, String userName);

	/**
	 * 获取用户系统通知消息详情
	 * @param msgId 消息Id
	 * @param userName 当前登录用户名
	 * @return
	 */
	AppBizSiteInformationDto getSystemMsgDetail(Long msgId, String userName);
	
}
