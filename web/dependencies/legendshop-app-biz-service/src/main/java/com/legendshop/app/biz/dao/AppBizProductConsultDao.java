package com.legendshop.app.biz.dao;

import com.legendshop.biz.model.dto.productConsult.AppBizProductConsultListDto;
import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.ProductConsult;

/**
 * 商家端app 商品咨询DAO
 * @author linzh
 */
public interface AppBizProductConsultDao extends GenericDao<ProductConsult, Long> {


	/**
	 * 获取商品咨询列表
	 * @param shopId
	 * @param curPageNO
	 * @param keywords
	 * @param pointType
	 * @return
	 */
	PageSupport<AppBizProductConsultListDto> queryProductConsultList(Long shopId, String curPageNO, String keywords, Integer pointType);
}
