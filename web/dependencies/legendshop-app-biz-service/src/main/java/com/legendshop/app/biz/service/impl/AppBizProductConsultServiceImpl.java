package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.dao.AppBizProductConsultDao;
import com.legendshop.app.biz.service.AppBizProductConsultService;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.base.util.PageUtils;
import com.legendshop.base.util.XssFilterUtil;
import com.legendshop.biz.model.dto.productConsult.AppBizProductConsultDto;
import com.legendshop.biz.model.dto.productConsult.AppBizProductConsultListDto;
import com.legendshop.business.dao.ProductConsultDao;
import com.legendshop.business.dao.ProductDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductConsult;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SafeHtml;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 商家端 咨询服务接口实现
 * @author linzh
 */
@Service("appBizProductConsultService")
public class AppBizProductConsultServiceImpl implements AppBizProductConsultService {

	/**
	 * 发送站内信
	 */
	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;

	@Autowired
	private AppBizProductConsultDao AppBizProductConsultDao;

	@Autowired
	private ProductConsultDao productConsultDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private UserDetailDao userDetailDao;



	/**
	 * 获取咨询列表
	 * @param shopId
	 * @param curPageNO
	 * @param keywords
	 * @param pointType
	 * @return
	 */
	@Override
	public AppPageSupport<AppBizProductConsultListDto> queryProductConsultList(Long shopId, String curPageNO, String keywords, Integer pointType) {


		PageSupport<AppBizProductConsultListDto> ps = AppBizProductConsultDao.queryProductConsultList(shopId,curPageNO,keywords,pointType);
		return PageUtils.convertPageSupport(ps);
	}

	/**
	 * 获取商品咨询详情
	 * @param consId
	 * @return
	 */
	@Override
	public AppBizProductConsultDto getProductConsultDetail(Long consId) {

		ProductConsult productConsult = productConsultDao.getProductConsult(consId);

		if (productConsult == null) {
			return null;
		}

		// 转换DTO
		AppBizProductConsultDto consultDto = convertToAppBizProductConsultDto(productConsult);

		// 获取商品详情
		Product product = productDao.getProductById(productConsult.getProdId());
		if (AppUtils.isNotBlank(product)){

			consultDto.setProdName(product.getName());
			consultDto.setProdPic(product.getPic());
		}

		// 获取咨询用户详情
		UserDetail userDetail = userDetailDao.getUserDetailByName(productConsult.getAskUserName());
		if (AppUtils.isNotBlank(userDetail)){

			consultDto.setNickName(userDetail.getNickName());
			consultDto.setPortraitPic(userDetail.getPortraitPic());
		}
		return consultDto;
	}

	/**
	 * 回复商品咨询
	 * @param productConsult
	 */
	@Override
	public void replyProductConsult(ProductConsult productConsult) {

		productConsult.setAnswer(XssFilterUtil.cleanXSS(productConsult.getAnswer()));

		productConsult.setAnswertime(new Date());
		productConsult.setReplySts(1);
		productConsultDao.updateProductConsult(productConsult);
		// 发送通知站内信
		sendSiteMessageProcessor.process(new SendSiteMsgEvent("系统", productConsult.getAskUserName(),
				"咨询回复" , "亲，您的咨询商家已回复，回复内容为【"+ productConsult.getAnswer() +"】").getSource());

	}


	/**
	 * 转换Dto
	 * @param productConsult
	 * @return
	 */
	private AppBizProductConsultDto convertToAppBizProductConsultDto(ProductConsult productConsult){
		AppBizProductConsultDto appBizProductConsultDto = new AppBizProductConsultDto();
		appBizProductConsultDto.setConsId(productConsult.getConsId());
		appBizProductConsultDto.setAnswer(productConsult.getAnswer());
		appBizProductConsultDto.setAnswertime(productConsult.getAnswertime());
		appBizProductConsultDto.setNickName(productConsult.getNickName());
		appBizProductConsultDto.setProdName(productConsult.getProdName());
		appBizProductConsultDto.setAskUserName(productConsult.getAskUserName());
		appBizProductConsultDto.setPointType(productConsult.getPointType());
		appBizProductConsultDto.setReplySts(productConsult.getReplySts());
		appBizProductConsultDto.setRecDate(productConsult.getRecDate());
		appBizProductConsultDto.setPortraitPic(productConsult.getPortraitPic());
		appBizProductConsultDto.setContent(productConsult.getContent());
		return appBizProductConsultDto;
	}



}
