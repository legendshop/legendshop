package com.legendshop.app.biz.dto;

import java.io.Serializable;
/**
 * 返回值
 *
 */
public class ResponseDto implements Serializable{

	private static final long serialVersionUID = -4437856374554789917L;
	
	private String errorCode = "0";
	
	private final Object result;
	
	public ResponseDto(Object result){
		this.result = result;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public Object getResult() {
		return result;
	}

}
