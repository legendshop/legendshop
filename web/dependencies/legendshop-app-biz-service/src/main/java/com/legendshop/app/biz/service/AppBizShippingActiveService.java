package com.legendshop.app.biz.service;

import com.legendshop.biz.model.dto.AppBizShippingActiveDto;
import com.legendshop.model.dto.app.AppPageSupport;

/**
 *	app 商家端包邮活动service
 * @author 27158
 */
public interface AppBizShippingActiveService {
	/**
	 * 添加包邮活动
	 * @param appBizShippingActiveDto
	 */
	void saveShippingActive(AppBizShippingActiveDto appBizShippingActiveDto);

	/**
	 * 获取包邮活动列表
	 * @param shopId
	 * @param searchType
	 * @return
	 */
	AppPageSupport<AppBizShippingActiveDto> queryShippingActiveList(String curPageNO, Long shopId, String name, String searchType);

	/**
	 * 根据Id 获取包邮活动详情
	 * @param id
	 * @return
	 */
	AppBizShippingActiveDto getShippingActiveById(Long id);
}
