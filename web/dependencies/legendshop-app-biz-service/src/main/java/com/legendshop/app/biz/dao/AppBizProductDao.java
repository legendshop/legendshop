package com.legendshop.app.biz.dao;

import com.legendshop.biz.model.dto.AppProdCommDto;
import com.legendshop.biz.model.dto.comment.AppBizProductCommentsParamsDto;
import com.legendshop.dao.GenericDao;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.Product;

/**
 * 商家端商品DAO
 * @author linzh
 */
public interface AppBizProductDao extends GenericDao<Product, Long> {

	/**
	 * 获取商品评论列表
	 * @param params
	 * @return
	 */
	AppPageSupport<AppProdCommDto> queryProductComments(AppBizProductCommentsParamsDto params);
}
