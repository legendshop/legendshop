package com.legendshop.app.biz.service;

import com.legendshop.biz.model.dto.AppBizMarketingProdDto;

import java.util.List;

/**
 * @author 27158
 */
public interface AppBizSkuService {

	/**
	 * 判断商品是否可修改，正在参加营销活动不能修改
	 * @param prodId
	 * @return
	 */
	public String isProdChange(Long prodId);

}
