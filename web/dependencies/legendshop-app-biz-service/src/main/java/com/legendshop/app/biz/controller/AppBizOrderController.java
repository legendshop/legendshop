package com.legendshop.app.biz.controller;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSONObject;
import com.legendshop.app.biz.service.AppBizOrderService;
import com.legendshop.app.biz.service.AppBizStoreService;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.base.util.ExpressMD5;
import com.legendshop.biz.model.dto.order.*;
import com.legendshop.model.constant.*;
import com.legendshop.model.dto.ExpressDeliverDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.order.ApplyRefundReturnDto;
import com.legendshop.model.dto.order.KuaiDiLogs;
import com.legendshop.model.dto.order.MySubDto;
import com.legendshop.model.dto.order.OrderSearchParamDto;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.*;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.model.entity.presell.PresellSubEntity;
import com.legendshop.model.entity.store.StoreOrder;
import com.legendshop.processor.helper.DelayCancelHelper;
import com.legendshop.sms.event.processor.SendSiteMessageProcessor;
import com.legendshop.spi.service.*;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;
import com.legendshop.util.HttpUtil;
import com.legendshop.util.JSONUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 订单管理
 */
@RestController
@Api(tags="订单管理",value="订单管理")
public class AppBizOrderController {


	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private AppBizOrderService appBizOrderService;

	@Autowired
	private StoreOrderService storeOrderService;

	@Autowired
	private SubService subService;

	@Autowired
	private AppBizStoreService appBizStoreService;

	@Autowired
	private MergeGroupOperateService mergeGroupOperateService;

	@Autowired
	private DeliveryTypeService deliveryTypeService;

	@Autowired
	private SendSiteMessageProcessor sendSiteMessageProcessor;

	@Autowired
	private SubRefundReturnService subRefundReturnService;

	@Autowired
	private SubHistoryService subHistoryService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private DelayCancelHelper delayCancelHelper;

	@Autowired
	private DeliveryCorpService deliveryCorpService;

	@Autowired
	private SystemParameterService systemParameterService;

	@Autowired
	private KuaiDiService kuaiDiService;

	@Autowired
	private SubItemService subItemService;

	@Autowired
	private AttachmentManager attachmentManager;

	@Autowired
	private UserDetailService userDetailService;


	@ApiOperation(value = "获取用户订单列表", httpMethod = "POST", notes = "获取用户订单",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "state_type", value = "订单状态[待付款: 1 ，待发货：2 ，已发货：3 ，已完成：4 ， 已关闭：5 ，待成团 6 ，带付尾款 7 ，待提货 8]", required = false, dataType = "Integer"),
			@ApiImplicitParam(paramType="query", name = "sub_type", value = "订单类型[普通订单: NORMAL ，团购订单：GROUP ，拍卖订单：AUCTIONS ，秒杀订单：SECKILL ， 拼团订单：MERGE_GROUP ，预售订单 PRE_SELL ，门店订单 SHOP_STORE]", required = false, dataType = "String"),
			@ApiImplicitParam(paramType="query", name = "refund_state", value = "维权状态[退款处理中 1 ，退款完成 2]", required = false, dataType = "Integer"),
			@ApiImplicitParam(paramType="query", name = "is_payed", value = "支付状态[未支付 0 ，已支付 1]", required = false, dataType = "Integer"),
			@ApiImplicitParam(paramType="query", name = "condition", value = "搜索条件(订单号/收货人姓名/收货人号码)", required = false, dataType = "String"),
			@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = false, dataType = "String"),
			@ApiImplicitParam(paramType="query", name = "pageSize", value = "每页多少条", required = false, dataType = "Integer")
	})
	@PostMapping(value = "/s/ordersList")
	public ResultDto<AppPageSupport<AppBizOrderDto>> ordersList(Integer state_type,String curPageNO,String condition,String sub_type,Integer refund_state,Integer is_payed,Integer pageSize) {
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail("你还未登陆，请登录后再操作");
		}
		if (AppUtils.isBlank(user.getShopId())){
			return ResultDtoManager.fail("您还不是商家，请成为商家后再试");
		}
		OrderSearchParamDto paramDto=new OrderSearchParamDto();
		if(AppUtils.isBlank(pageSize)){
			pageSize = 10;
		}
		paramDto.setPageSize(pageSize);
		paramDto.setCurPageNO(curPageNO);
		paramDto.setShopId(user.getShopId());
		paramDto.setStatus(state_type);
		paramDto.setSubType(sub_type);
		paramDto.setRefundStatus(refund_state);
		paramDto.setIsPayed(is_payed);
		paramDto.setDeleteStatus(OrderDeleteStatusEnum.NORMAL.value());
		AppPageSupport<AppBizOrderDto> appPageSupport = appBizOrderService.queryShopOrderList(paramDto,condition);
		return ResultDtoManager.success(appPageSupport);
	}



	@ApiOperation(value = "获取用户订单详情", httpMethod = "POST", notes = "获取用户订单详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "subNumber", value = "订单号", required = true, dataType = "String")
	@PostMapping("/s/orderDetail")
	public ResultDto<AppBizOrderDetailDto> orderDetail(String subNumber) {
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail("你还未登陆，请登录后再操作！！！");
		}
		Long shopId = user.getShopId();
		if (AppUtils.isBlank(shopId)){
			return ResultDtoManager.fail("您好你还不是商家，请成为商家后再试！！！");
		}
		AppBizOrderDetailDto appBizOrderDto = appBizOrderService.queryShopOrderDetail(subNumber, shopId);

		if(AppUtils.isBlank(appBizOrderDto)){
			return ResultDtoManager.fail(-1, "该订单已不存在或被删除，请重新加载！");
		}

		//如果是门店订单，获取门店信息
		if(SubTypeEnum.SHOP_STORE.value().equals(appBizOrderDto.getSubType())){
			StoreOrder storeOrder = storeOrderService.getDeliveryOrderBySubNumber(appBizOrderDto.getSubNumber());
			if(AppUtils.isNotBlank(storeOrder)){

				//获取门店信息
				AppBizStoreDto store = appBizStoreService.getStore(storeOrder.getStoreId());
				//获取提货码
				String dlyoPickupCode = storeOrder.getDlyoPickupCode();
				if (AppUtils.isNotBlank(dlyoPickupCode)) {
					appBizOrderDto.setDlyoPickupCode(dlyoPickupCode);
				}
				appBizOrderDto.setStore(store);
			}
		}

		Date countDownTime = null;
		if(OrderStatusEnum.UNPAY.value().equals(appBizOrderDto.getStatus())){

			//计算订单自动取消的截止时间
			Integer cancelMins = subService.getOrderCancelExpireDate();//获取订单自动取消的时间
			countDownTime = DateUtils.rollMinute(appBizOrderDto.getSubDate(), cancelMins);
		}else if(OrderStatusEnum.PADYED.value().equals(appBizOrderDto.getStatus())){
			if(SubTypeEnum.MERGE_GROUP.value().equals(appBizOrderDto.getSubType())){
				MergeGroupOperate mergeGroupOperate = mergeGroupOperateService.getMergeGroupOperateByAddNumber(appBizOrderDto.getAddNumber());
				if(null != mergeGroupOperate){
					countDownTime = mergeGroupOperate.getEndTime();
					appBizOrderDto.setMergerGroupPeopleNum(mergeGroupOperate.getPeopleNumber());
					appBizOrderDto.setMergerGroupAlreadyAddNum(mergeGroupOperate.getNumber());
				}
			}
		}else if(OrderStatusEnum.CONSIGNMENT.value().equals(appBizOrderDto.getStatus())){

			//计算订单自动确认收货的截止时间
			Integer confirmDay = subService.getOrderConfirmExpireDate();//获取订单自动确认的时间
			countDownTime = DateUtils.rollDay(appBizOrderDto.getDvyDate(), confirmDay);
		}

		appBizOrderDto.setCountDownTime(countDownTime);
		return ResultDtoManager.success(appBizOrderDto);
	}



	@ApiOperation(value = "获取配送方式", httpMethod = "GET", notes = "获取配送方式",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@GetMapping("/s/deliveryList")
	public ResultDto<List<DeliveryType>>deliveryList(){
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail("你还未登陆，请登录后再操作！！！");
		}
		if (AppUtils.isBlank(user.getShopId())){
			return ResultDtoManager.fail("您好你还不是商家，请成为商家后再试！！！");
		}
		Long shopId = user.getShopId();
		List<DeliveryType> deliveryTypes = deliveryTypeService.getDeliveryTypeByShopId(shopId);
		return ResultDtoManager.success(deliveryTypes);
	}



	@ApiOperation(value = "商品发货", httpMethod = "POST", notes = "商品发货",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "subNumber", value = "订单号", required = true, dataType = "String"),
			@ApiImplicitParam(paramType="query", name = "dvyTypeId", value = "配送方式ID", required = true, dataType = "Long"),
			@ApiImplicitParam(paramType="query", name = "dvyFlowId", value = "物流单号", required = true, dataType = "String"),
	})
	@PostMapping(value = "/s/sendCargo")
	public ResultDto<String> sendCargo(String subNumber, Long dvyTypeId, String dvyFlowId) {
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail("你还未登陆，请登录后再操作！！！");
		}
		if (AppUtils.isBlank(user.getShopId())){
			return ResultDtoManager.fail("您好你还不是商家，请成为商家后再试！！！");
		}
		Long shopId = user.getShopId();

		if (subNumber == null || dvyTypeId == null || dvyFlowId == null) {
			return ResultDtoManager.fail("发货参数不足");
		}
		String result;
		try {
			// 发货
			result = subService.fahuo(subNumber, dvyTypeId, dvyFlowId, null, shopId);
		} catch (Exception e) {
			return ResultDtoManager.fail("发货失败");
		}
		return ResultDtoManager.success("发货成功！！！");
	}



	@ApiOperation(value = "商品批量发货", httpMethod = "POST", notes = "商品批量发货",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "subNumbers", value = "订单号集合", required = true, dataType = "String[]"),
			@ApiImplicitParam(paramType="query", name = "dvyTypeId", value = "配送方式ID", required = true, dataType = "Long"),
			@ApiImplicitParam(paramType="query", name = "dvyFlowId", value = "物流单号", required = true, dataType = "String"),
	})
	@PostMapping(value = "/s/batchSendCargo")
	public ResultDto<String> batchSendCargo(String[] subNumbers, Long dvyTypeId, String dvyFlowId) {
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail("你还未登陆，请登录后再操作！！！");
		}
		if (AppUtils.isBlank(user.getShopId())){
			return ResultDtoManager.fail("您好你还不是商家，请成为商家后再试！！！");
		}
		Long shopId = user.getShopId();

		if (subNumbers == null || dvyTypeId == null || dvyFlowId == null) {
			return ResultDtoManager.fail("发货参数不足");
		}
		String result;
		try {
			// 发货
			for (String subNumber : subNumbers) {
				result = subService.fahuo(subNumber, dvyTypeId, dvyFlowId, null, shopId);
			}
		} catch (Exception e) {
			return ResultDtoManager.fail("发货失败");
		}
		return ResultDtoManager.success("发货成功！！！");
	}



	@ApiOperation(value = "查看物流信息", httpMethod = "POST", notes = "查看物流信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "subNumber", value = "订单号", required = true, dataType = "string")
	@PostMapping("/s/orderExpress")
	public ResultDto<AppBizDeliveryDetailDto> orderExpress(String subNumber) {
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail("你还未登陆，请登录后再操作！！！");
		}
		if (AppUtils.isBlank(user.getShopId())){
			return ResultDtoManager.fail("您好你还不是商家，请成为商家后再试！！！");
		}
		Long shopId = user.getShopId();
		MySubDto myorder = subService.findOrderDetail(subNumber,shopId);
		if(AppUtils.isBlank(myorder)){
			return ResultDtoManager.fail(-1, "该订单不存在！");
		}

		//物流查询参数
		Long dvyId = myorder.getDvyTypeId();
		String dvyFlowId = myorder.getDvyFlowId();
		String reciverMobile = myorder.getUserAddressSub().getMobile();

		if (AppUtils.isBlank(dvyId) || AppUtils.isBlank(dvyFlowId) || AppUtils.isBlank(reciverMobile)) {
			return ResultDtoManager.fail(-1, "查询物流信息参数有误，请联系客服！");
		}

		DeliveryCorp deliveryCorp = deliveryCorpService.getDeliveryCorpByDvyId(dvyId);

		AppBizDeliveryDetailDto appDeliveryDto = new AppBizDeliveryDetailDto();
		if (AppUtils.isNotBlank(deliveryCorp)) {

			SystemParameter systemParameter = systemParameterService.getSystemParameter("EXPRESS_DELIVER");
			if (AppUtils.isBlank(systemParameter) || AppUtils.isBlank(systemParameter.getValue())) { //查询到没配置密钥则 是免费的

				String url = deliveryCorp.getQueryUrl();
				if (AppUtils.isNotBlank(url)) {
					url = url.replaceAll("\\{dvyFlowId\\}", dvyFlowId);
				}
				KuaiDiLogs logs = kuaiDiService.query(url);
				if (AppUtils.isNotBlank(logs) && AppUtils.isNotBlank(logs.getEntries()) && logs.getEntries().size()>0) {

					String entriesLog = JSONUtil.getJson(logs.getEntries());

					//封装返回参数
					appDeliveryDto.setDvyDetail(entriesLog);
					appDeliveryDto.setDvyName(deliveryCorp.getName());
					appDeliveryDto.setDvyNumber(dvyFlowId);
					/*appDeliveryDto.setDvyPic(dvyPic);暂时没有图片返回*/
					return ResultDtoManager.success(appDeliveryDto);
				}

			}else{  //快递100

				if(AppUtils.isBlank(deliveryCorp.getCompanyCode())){
					return ResultDtoManager.fail(-1, "查询该订单的物流信息有误，如需更新物流单号请联系客服或自行到PC端修改！");
				}

				ExpressDeliverDto express = JSONUtil.getObject(systemParameter.getValue(), ExpressDeliverDto.class);

				//组装参数密钥
				String param ="{\"com\":\""+deliveryCorp.getCompanyCode()+"\",\"num\":\""+dvyFlowId+"\",\"mobiletelephone\":\""+reciverMobile+"\"}";
				String sign = ExpressMD5.encode(param+express.getKey()+express.getCustomer());
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("param",param);
				params.put("sign",sign);
				params.put("customer",express.getCustomer());
				//请求第三方接口
				String text = HttpUtil.httpPost(ExpressMD5.API_EXPRESS_URL, params);
				if (AppUtils.isBlank(text)) {
					return ResultDtoManager.fail(-1, "查询该订单的物流信息有误，如需更新物流单号请联系客服或自行到PC端修改！");
				}

				JSONObject jsonObject = JSONObject.parseObject(text);
				String result = jsonObject.getString("status");
				if(AppUtils.isBlank(result) || !"200".equals(result)){
					return ResultDtoManager.fail(-1, "查询该订单的物流信息有误，如需更新物流单号请联系客服或自行到PC端修改！");
				}

				//封装返回参数
				appDeliveryDto.setDvyName(deliveryCorp.getName());
				appDeliveryDto.setDvyNumber(dvyFlowId);
				/*appDeliveryDto.setDvyPic(dvyPic);暂时没有图片返回*/
				appDeliveryDto.setDvyDetail(jsonObject.getString("data"));

				return ResultDtoManager.success(appDeliveryDto);
			}
		}
		return ResultDtoManager.fail(-2, "暂无物流信息！");
	}



	@ApiOperation(value = "获取订单价格信息", httpMethod = "GET", notes = "获取订单价格信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="path", name = "subNumber", value = "订单号", required = true, dataType = "String")
	@GetMapping("/s/changePreOrderFee/{subNumber}")
	public ResultDto<AppBizOrderPriceDto> changePreOrderFee(@PathVariable String subNumber) {
		AppBizOrderPriceDto appBizOrderPriceDto = appBizOrderService.getBizOrderPriceDto(subNumber);
		return ResultDtoManager.success(appBizOrderPriceDto);
	}



	@ApiOperation(value = "修改订单价格", httpMethod = "POST", notes = "修改订单价格",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "subNumber", value = "订单号", required = true, dataType = "String"),
			@ApiImplicitParam(paramType="query", name = "freight", value = "运费", required = true, dataType = "Double"),
			@ApiImplicitParam(paramType="query", name = "orderAmount", value = "商品总价", required = true, dataType = "Double"),
	})
	@PostMapping(value = "/s/changeOrderFee")
	public ResultDto<String> changeOrderFee(String subNumber, Double freight, Double orderAmount) {
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail("你还未登陆，请登录后再操作！！！");
		}
		String userName = user.getUserName();

		if (userName == null || subNumber == null || AppUtils.isBlank(orderAmount) || orderAmount == 0) {
			return ResultDtoManager.fail("参数有误！！！");
		}
		if (orderAmount == null || orderAmount < 0 || orderAmount > 9999999999999D) {
			return ResultDtoManager.fail("参数有误！！！");
		}
		if (AppUtils.isBlank(freight)) {
			freight = 0.00;
		}
		boolean result = false;
		try {
			Sub subBySubNumber = subService.getSubBySubNumber(subNumber);

			//检查订单归属
			if(subBySubNumber == null) {
				return ResultDtoManager.fail("订单已不存在，请刷新页面或联系管理员！！！");
			}

			if(!subBySubNumber.getShopId().equals(user.getShopId())) {
				return ResultDtoManager.fail("修改失败，不是当前商家的订单！！！");
			}

			result = subService.updateSubPrice(subNumber, freight, orderAmount, userName);
			if (!subBySubNumber.getTotal().equals(orderAmount) || !subBySubNumber.getFreightAmount().equals(freight)) {
				// 发送站内信 通知
				sendSiteMessageProcessor.process(new SendSiteMsgEvent(subBySubNumber.getUserName(), "价格变更通知", "亲，您的待支付订单[" + subNumber + "]价格已变更").getSource());
			}
		} catch (Exception e) {
			return ResultDtoManager.fail("调整失败！！！");
		}
		if (result) {
			return ResultDtoManager.success("调整成功！！！");
		} else {
			return ResultDtoManager.fail("调整失败！！！");
		}
	}



	@ApiOperation(value = "商家添加备注", httpMethod = "POST", notes = "商家添加备注",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "subNumber", value = "订单号", required = true, dataType = "String"),
			@ApiImplicitParam(paramType="query", name = "shopRemark", value = "备注", required = true, dataType = "String")
	})
	@PostMapping("/s/remarkShopOrder")
	public  ResultDto<String> remarkShopOrder( String subNumber, String shopRemark) {
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail("你还未登陆，请登录后再操作！！！");
		}
		Long shopId = user.getShopId();

		Map<String, Object> result = new HashMap<>();
		Sub sub = subService.getSubBySubNumber(subNumber);

		if (AppUtils.isBlank(sub)) {
			return ResultDtoManager.fail("订单已不存在，请刷新页面或联系管理员！！！");
		}

		if (!shopId.equals(sub.getShopId())) {
			return ResultDtoManager.fail("备注失败，不是当前商家的订单！！！");
		}

		sub.setShopRemark(shopRemark);
		sub.setShopRemarkDate(new Date());
		sub.setIsShopRemarked(true);
		subService.updateSub(sub);

		return ResultDtoManager.success("备注成功！！！");
	}



	@ApiOperation(value = "查看退款详情", httpMethod = "GET", notes = "查看退款详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="path", name = "refundId", value = "退款编号", required = true, dataType = "Long")
	@GetMapping("/s/refundDetail/{refundId}")
	public ResultDto<SubRefundReturn> refundDetail(@PathVariable Long refundId) {
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
		//七天的时间
		/*int sTime = 3600*24*7*1000;*/
		//剩余时间 = 七天时间-（当前时间-申请时间）；
		/*Long endTime = sTime - (new Date().getTime()-subRefundReturn.getApplyTime().getTime());*/
		if (AppUtils.isBlank(subRefundReturn)) {
			return ResultDtoManager.fail("对不起,您要查看的退款记录不存在货已被删除！！！");
		}
		if (AppUtils.isNotBlank(subRefundReturn.getApplyTime())) {
			Long endTime = cn.hutool.core.date.DateUtil.offsetDay(subRefundReturn.getApplyTime(), 7).getTime();
			subRefundReturn.setEndTime(endTime);
		}
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail("你还未登陆，请登录后再操作！！！");
		}
		if (AppUtils.isBlank(user.getShopId())){
			return ResultDtoManager.fail("您好你还不是商家，请成为商家后再试！！！");
		}
		Long shopId = user.getShopId();

		if (!shopId.equals(subRefundReturn.getShopId())) {
			return ResultDtoManager.fail("对不起,您没有权限！！！");
		}
		List<SubItem> subItemList = subItemService.getSubItemByReturnId(subRefundReturn.getRefundId());
		if (AppUtils.isNotBlank(subItemList)){
			subRefundReturn.setSubItemList(subItemList);
		}
		return ResultDtoManager.success(subRefundReturn);
	}



	@ApiOperation(value = "查看退货详情", httpMethod = "GET", notes = "查看退货详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="path", name = "refundId", value = "退货编号", required = true, dataType = "Long")
	@GetMapping("/s/returnDetail/{refundId}")
	public ResultDto<SubRefundReturn> returnDetail(@PathVariable Long refundId) {
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
		if (AppUtils.isBlank(subRefundReturn)) {
			return ResultDtoManager.fail("对不起,您要查看的退款记录不存在或已被删除！！！");
		}
		if (AppUtils.isNotBlank(subRefundReturn.getApplyTime())){
			Long endTime = cn.hutool.core.date.DateUtil.offsetDay(subRefundReturn.getApplyTime(),7).getTime();
			subRefundReturn.setEndTime(endTime);
		}
		if (AppUtils.isNotBlank(subRefundReturn.getShipTime())){
			Long shipTimeNumber = cn.hutool.core.date.DateUtil.offsetDay(subRefundReturn.getShipTime(),7).getTime();
			subRefundReturn.setShipTimeNumber(shipTimeNumber);
		}
		if (AppUtils.isNotBlank(subRefundReturn.getSellerTime())){
			Long sellerTime = cn.hutool.core.date.DateUtil.offsetDay(subRefundReturn.getSellerTime(),7).getTime();
			subRefundReturn.setSellerTimeNumber(sellerTime);
		}
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail("你还未登陆，请登录后再操作！！！");
		}
		if (AppUtils.isBlank(user.getShopId())){
			return ResultDtoManager.fail("您好你还不是商家，请成为商家后再试！！！");
		}
		Long shopId = user.getShopId();

		if (!shopId.equals(subRefundReturn.getShopId())) {
			return ResultDtoManager.fail("对不起,您没有权限！！！");
		}
		List<SubItem> subItemList = subItemService.getSubItemByReturnId(subRefundReturn.getRefundId());
		if (AppUtils.isNotBlank(subItemList)){
			subRefundReturn.setSubItemList(subItemList);
		}
		return ResultDtoManager.success(subRefundReturn);
	}



	@ApiOperation(value = "审核用户申请订单退款", httpMethod = "POST", notes = "审核用户申请订单退款",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "refundId", value = "退款,退货ID", required = true, dataType = "String"),
			@ApiImplicitParam(paramType="query", name = "isAgree", value = "是否同意", required = true, dataType = "String"),
			@ApiImplicitParam(paramType="query", name = "sellerMessage", value = "备注", required = true, dataType = "String"),
	})
	@PostMapping("/s/auditRefund")
	public ResultDto<String> auditRefund(Long refundId, Boolean isAgree, String sellerMessage) {
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
		if (AppUtils.isBlank(subRefundReturn)) {
			return ResultDtoManager.fail("对不起,您要查看的退款记录不存在或已被删除！！！");
		}

		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail("你还未登陆，请登录后再操作！！！");
		}
		if (AppUtils.isBlank(user.getShopId())){
			return ResultDtoManager.fail("您好你还不是商家，请成为商家后再试！！！");
		}
		Long shopId = user.getShopId();

		if (!shopId.equals(subRefundReturn.getShopId())
				|| RefundReturnTypeEnum.REFUND.value() != subRefundReturn.getApplyType()) {
			return ResultDtoManager.fail("对不起,您没有权限！！！");
		}
		saveOrderHistoryMoney(subRefundReturn,isAgree);
		subRefundReturnService.shopAuditRefund(subRefundReturn, isAgree, sellerMessage, true);
		return ResultDtoManager.success("操作成功！！！");
	}



	@ApiOperation(value = "查看订单备注", httpMethod = "GET", notes = "查看订单备注",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="path", name = "subNumber", value = "订单id", required = true, dataType = "String")
	@GetMapping("/s/remarkInfo/{subNumber}")
	public ResultDto<AppBizOrderRemarkDto> remarkInfo(@PathVariable String subNumber) {
		AppBizOrderRemarkDto appBizOrderRemarkDto = appBizOrderService.getOrderRemarkInfo(subNumber);
		return ResultDtoManager.success(appBizOrderRemarkDto);
	}



	@ApiOperation(value = "取消订单", httpMethod = "POST", notes = "取消订单",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "subNumber", value = "订单id", required = true, dataType = "String")
	@PostMapping(value = "/s/cancleOrder")
	public ResultDto<Object> cancleOrder(String subNumber) {
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail("你还未登陆，请登录后再操作！！！");
		}
		if (AppUtils.isBlank(user.getShopId())){
			return ResultDtoManager.fail("你还不是商家，请成为商家在重新操作！！！");
		}
		String userName = user.getUserName();
		Long shopId = user.getShopId();
		try {
			if (AppUtils.isNotBlank(subNumber)) {
				Sub sub = subService.getSubBySubNumberByShopId(subNumber, shopId);
				if (AppUtils.isNotBlank(sub) && OrderStatusEnum.UNPAY.value().equals(sub.getStatus())) {
					StringBuilder sb = new StringBuilder();
					String time = DateUtil.format(new Date(), DateUtils.PATTERN_CLASSICAL);
					sb.append("商家").append(userName).append("于").append(time).append("取消订单 ");
					Date date = new Date();
					sub.setUpdateDate(date);
					sub.setStatus(OrderStatusEnum.CLOSE.value());
					sub.setCancelReason(sb.toString());
					boolean result = subService.cancleOrder(sub);
					if (result) {
						//写入订单历史
						SubHistory subHistory = new SubHistory();
						subHistory.setRecDate(date);
						subHistory.setSubId(sub.getSubId());
						subHistory.setStatus(SubHistoryEnum.ORDER_CANCEL.value());

						subHistory.setUserName(userName);
						subHistory.setReason(sb.toString());

						subHistoryService.saveSubHistory(subHistory);

						orderService.cleanOrderCache(sub.getUserId(), sub.getShopId());

						// remove delay queue ordre
						delayCancelHelper.remove(sub.getSubNumber());
						return ResultDtoManager.success("取消成功！！！");
					}
				} else {
					return ResultDtoManager.fail("取消失败！！！");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResultDtoManager.fail("取消失败！！！");
	}



	@ApiOperation(value = "发货详情", httpMethod = "GET", notes = "发货详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="path", name = "subNumber", value = "订单id", required = true, dataType = "String")
	@GetMapping("/s/sendCargoInfo/{subNumber}")
	public ResultDto<AppBizOrderSendCargoDto> sendCargoInfo(@PathVariable String subNumber) {
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail("你还未登陆，请登录后再操作！！！");
		}
		if (AppUtils.isBlank(user.getShopId())){
			return ResultDtoManager.fail("您好你还不是商家，请成为商家后再试！！！");
		}
		Long shopId = user.getShopId();
		if (AppUtils.isBlank(shopId)){
			return ResultDtoManager.fail("您好你还不是商家，请成为商家后再试！！！");
		}
		AppBizOrderSendCargoDto apBizOrderSendCargoDto = appBizOrderService.getOrderSendCargoInfo(subNumber,shopId);
		return ResultDtoManager.success(apBizOrderSendCargoDto);
	}



	@ApiOperation(value = "调整预售订单费用", httpMethod = "POST", notes = "调整预售订单费用",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "subNumber", value = "订单号", required = true, dataType = "String"),
			@ApiImplicitParam(paramType="query", name = "preDepositPrice", value = "定金价格", required = true, dataType = "BigDecimal"),
			@ApiImplicitParam(paramType="query", name = "finalPrice", value = "尾款价格", required = true, dataType = "BigDecimal"),
	})
	@PostMapping(value = "/s/changePreOrderFee")
	public ResultDto<Object> changePreOrderFee(String subNumber,BigDecimal preDepositPrice,BigDecimal finalPrice) {

		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail("你还未登陆，请登录后再操作！！！");
		}
		String userName = user.getUserName();

		PresellSubEntity presellSub = new PresellSubEntity();
		presellSub.setSubNumber(subNumber);
		presellSub.setPreDepositPrice(preDepositPrice);
		presellSub.setFinalPrice(finalPrice);

		PresellSubEntity oldPresellsub = subService.getPreSubBySubNo(presellSub.getSubNumber());
		if (AppUtils.isBlank(presellSub.getSubNumber()) || AppUtils.isBlank(presellSub.getPreDepositPrice())  || AppUtils.isBlank(presellSub.getFinalPrice())) {
			return ResultDtoManager.fail("参数不全！！！");
		}
		if (presellSub.getPreDepositPrice().doubleValue() <= 0 || presellSub.getPreDepositPrice().doubleValue() > 9999999999999D) {
			return ResultDtoManager.fail("定金价格取值范围有误（1-9999999999999）！！！");
		}
		if (oldPresellsub.getPayPctType().equals(1) && (presellSub.getFinalPrice().doubleValue() <= 0 || presellSub.getFinalPrice().doubleValue() > 9999999999999D)) {
			return ResultDtoManager.fail("尾款价格取值范围有误（1-9999999999999）！！！");
		}
		boolean rs = subService.updateSubPrice(presellSub, userName);
		return rs ? ResultDtoManager.success("修改成功！！！") : ResultDtoManager.fail("修改失败！！！");
	}



	@ApiOperation(value = "保存发货信息", httpMethod = "POST", notes = "保存发货信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "subNumber", value = "订单号", required = true, dataType = "String"),
			@ApiImplicitParam(paramType="query", name = "deliv", value = "配送方式ID", required = true, dataType = "Long"),
			@ApiImplicitParam(paramType="query", name = "dvyFlowId", value = "物流单号", required = true, dataType = "String")
	})
	@PostMapping(value = "/s/order/changeDeliverGoods")
	public ResultDto<Object> changeDeliverGoods(String subNumber, Long deliv, String dvyFlowId) {
		if (subNumber == null || deliv == null || dvyFlowId == null) {
			return ResultDtoManager.fail("参数不全！！！");
		}
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail("你还未登陆，请登录后再操作！！！");
		}
		String userName = user.getUserName();

		boolean result = false;
		try {
			result = subService.changeDeliverGoods(subNumber, deliv, dvyFlowId, null, userName);
		} catch (Exception e) {
			throw new BusinessException("changeDeliverGoods failed");
		}
		if (result) {
			return ResultDtoManager.success("保存成功！！！");
		} else {
			return ResultDtoManager.fail("保存失败！！！");
		}
	}



	@ApiOperation(value = "审核用户申请订单退货退款", httpMethod = "POST", notes = "审核用户申请订单退货退款",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "refundId", value = "退款,退货ID", required = true, dataType = "String"),
			@ApiImplicitParam(paramType="query", name = "isAgree", value = "是否同意", required = true, dataType = "String"),
			@ApiImplicitParam(paramType="query", name = "isJettison", value = "是否弃货", required = true, dataType = "Boolean"),
			@ApiImplicitParam(paramType="query", name = "sellerMessage", value = "备注", required = true, dataType = "String")
	})
	@PostMapping("/s/auditItemReturn")
	public ResultDto<Object> auditItemReturn(Long refundId, Boolean isAgree, Boolean isJettison,
								  String sellerMessage) {
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
		if (AppUtils.isBlank(subRefundReturn)) {
			return ResultDtoManager.fail("对不起,您要操作的记录不存在!");
		}

		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail("你还未登陆，请登录后再操作！！！");
		}
		if (AppUtils.isBlank(user.getShopId())){
			return ResultDtoManager.fail("您好你还不是商家，请成为商家后再试！！！");
		}
		Long shopId = user.getShopId();

		if (!shopId.equals(subRefundReturn.getShopId())|| RefundReturnTypeEnum.REFUND_RETURN.value() != subRefundReturn.getApplyType()) {
			return ResultDtoManager.fail("非法操作!");
		}
		Sub sub = new Sub();
		sub.setSubId(subRefundReturn.getSubId());
		saveOrderHistory(sub,isAgree);
		subRefundReturnService.shopAuditReturn(subRefundReturn, isAgree, isJettison, sellerMessage, true);
		return ResultDtoManager.success("操作成功！");
	}



	@ApiOperation(value = "商家确认收货", httpMethod = "POST", notes = "商家确认收货",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "refundId", value = "退款,退货ID", required = true, dataType = "String"),
			@ApiImplicitParam(paramType="query", name = "goodsState", value = "收货状态（未收到:3,已收货:4）", required = true, dataType = "Integer")
	})
	@PostMapping("/s/receiveGoods")
	public ResultDto<Object> receiveGoods(Long refundId, Integer goodsState) {
		if (!RefundReturnStatusEnum.LOGISTICS_UNRECEIVED.value().equals(goodsState) && !RefundReturnStatusEnum.LOGISTICS_RECEIVED.value().equals(goodsState)) {
			return ResultDtoManager.fail("非法操作！！！");
		}
		SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
		if (AppUtils.isBlank(subRefundReturn)) {
			return ResultDtoManager.fail("对不起,您要操作的记录不存在！！！");
		}
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)){
			return ResultDtoManager.fail("你还未登陆，请登录后再操作！！！");
		}
		if (AppUtils.isBlank(user.getShopId())){
			return ResultDtoManager.fail("您好你还不是商家，请成为商家后再试！！！");
		}
		Long shopId = user.getShopId();

		if (!shopId.equals(subRefundReturn.getShopId())) {
			return ResultDtoManager.fail("非法操作！！！");
		}
		if (subRefundReturn.getGoodsState() == goodsState) {
			return ResultDtoManager.fail("非法操作！！！");
		}
		if (RefundReturnStatusEnum.LOGISTICS_UNRECEIVED.value().equals(goodsState)) {
			if (!com.legendshop.util.DateUtil.getDay(subRefundReturn.getShipTime(), 5).before(new Date())) {
				return ResultDtoManager.fail("非法操作！！！");
			} else {
				subRefundReturn.setGoodsState(RefundReturnStatusEnum.LOGISTICS_UNRECEIVED.value());
				subRefundReturnService.closeSubRefundReturnByReturn(subRefundReturn, true);
			}
		} else {
			subRefundReturn.setReceiveTime(new Date());
			subRefundReturn.setGoodsState(RefundReturnStatusEnum.LOGISTICS_RECEIVED.value());
			subRefundReturn.setApplyState(RefundReturnStatusEnum.APPLY_WAIT_ADMIN.value());// 待管理员审核
		}
		subRefundReturn.setReceiveMessage("");
		subRefundReturnService.confirmSubRefundReturn(subRefundReturn);
		Sub sub=new Sub();
		sub.setSubId(subRefundReturn.getSubId());
		sub.setUserName(subRefundReturn.getUserName());
		saveOrderHistoryProduct(sub);
		return ResultDtoManager.success("操作成功！！！");
	}


	/**
	 * 商家处理退款订单日志
	 * @param sub
	 */
	private void saveOrderHistoryMoney(SubRefundReturn sub,Boolean rs) {
		SubHistory subHistory = new SubHistory();
		Date date = new Date();
		String time = subHistory.DateToString(date);
		subHistory.setRecDate(date);
		subHistory.setSubId(sub.getSubId());
		subHistory.setStatus(SubHistoryEnum.AGREED_RETURNMONEY.value());
		StringBuilder sb = new StringBuilder();
		sb.append("[退货/退款]商家").append("于").append(time).append("商家"+(rs==true?"同意":"不同意")+"退款 ");
		subHistory.setUserName("商家");
		subHistory.setReason(sb.toString());
		subHistoryService.saveSubHistory(subHistory);
	}

	/**
	 * 商家同意退货退款订单日志
	 * @param sub
	 */
	private void saveOrderHistory(Sub sub,Boolean rs) {
		SubHistory subHistory = new SubHistory();
		Date date = new Date();
		String time = subHistory.DateToString(date);
		subHistory.setRecDate(date);
		subHistory.setSubId(sub.getSubId());
		subHistory.setStatus(SubHistoryEnum.AGREED_RETURNGOOD.value());
		StringBuilder sb = new StringBuilder();
		sb.append("[退货/退款]商家").append("于").append(time).append("商家"+(rs==true?"同意":"不同意")+"退货退款");
		subHistory.setUserName("商家");
		subHistory.setReason(sb.toString());
		subHistoryService.saveSubHistory(subHistory);
	}

	/**
	 * 商家收到退货订单日志
	 * @param sub
	 */
	private void saveOrderHistoryProduct(Sub sub) {
		SubHistory subHistory = new SubHistory();
		Date date = new Date();
		String time = subHistory.DateToString(date);
		subHistory.setRecDate(date);
		subHistory.setSubId(sub.getSubId());
		subHistory.setStatus(SubHistoryEnum.AGREED_RETURNGOOD.value());
		StringBuilder sb = new StringBuilder();
		sb.append("[退货/退款]商家").append("于").append(time).append("商家收到退货 ");
		subHistory.setUserName("商家");
		subHistory.setReason(sb.toString());
		subHistoryService.saveSubHistory(subHistory);
	}




	@RequestMapping("/s/shopRefund/apply/{userId}/{orderId}")
	@ApiOperation(value = "商家前往退款申请页面", httpMethod = "GET", notes = "商家前往退款申请页面",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="path", name = "userId", value = "订单所属用户ID", required = true, dataType = "String"),
			@ApiImplicitParam(paramType="path", name = "orderId", value = "订单ID", required = true, dataType = "String")
	})
	@GetMapping("/s/shopRefund/apply/{userId}/{orderId}")
	public ResultDto<ApplyRefundReturnDto> shopApplyRefund(@PathVariable String userId, @PathVariable Long orderId) {
		ApplyRefundReturnDto refund = subRefundReturnService.getApplyRefundReturnDto(orderId, RefundReturnTypeEnum.OP_ORDER, userId);
		if (AppUtils.isBlank(refund)) {
			return ResultDtoManager.fail("该订单为空");
		}
		//检查订单是否允许退款操作
		if (!isAllowRefund(refund, RefundSouceEnum.SHOP.value())) {//不允许
			return ResultDtoManager.fail("该订单不允许退款操作");
		}

		if (Double.doubleToRawLongBits(refund.getSubMoney()) == Double.doubleToLongBits(0.00)) {
			return ResultDtoManager.fail("该订单金额为0，不支持退款");
		}
		refund.setUserId(userId);
		return ResultDtoManager.success(refund);
	}



	@ApiOperation(value = "保存商家主动退还订金申请", httpMethod = "POST", notes = "保存商家主动退还订金申请",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "userId", value = "订单所属用户ID", required = true, dataType = "String"),
	})
		@PostMapping("/s/shopRefund/save")
	public ResultDto<Object> saveShopRefund(String userId,AppBizApplyRefundReturnBase form) {


		ApplyRefundReturnDto refund = subRefundReturnService.getApplyRefundReturnDto(form.getOrderId(), RefundReturnTypeEnum.OP_ORDER, userId);

		if (AppUtils.isBlank(refund)) {
			return ResultDtoManager.fail("对不起,您操作的订单不存在或已被删除!");
		}

		//检查订单是否允许退款
		if (!isAllowRefund(refund, RefundSouceEnum.SHOP.value())) {
			return ResultDtoManager.fail("对不起,您操作的订单不允许退款!");
		}

		//获取退款用户信息
		UserDetail userDetail = userDetailService.getUserDetailByIdNoCache(userId);

		if (AppUtils.isBlank(userDetail)) {
			return ResultDtoManager.fail("对不起,你操作的订单所属用户不存在!");
		}
		if (refund.getSubType().equals(CartTypeEnum.PRESELL.toCode()) && refund.getPayPctType() == 1) {
			double percent = refund.getDepositRefundAmount() / refund.getProdCash();
			Double prodCash = refund.getProdCash();
			double preVal = prodCash * percent;
			double finalPayVal = prodCash - preVal;
			refund.setDepositRefundAmount(preVal);
			refund.setFinalRefundAmount(finalPayVal);
		}
		SubRefundReturn subRefundReturn = new SubRefundReturn();

		subRefundReturn.setSubId(form.getOrderId());
		subRefundReturn.setSubNumber(refund.getSubNumber());
		subRefundReturn.setSubMoney(BigDecimal.valueOf(refund.getSubMoney()));//设置订单总金额
		subRefundReturn.setIsHandleSuccess(0);

		subRefundReturn.setRefundAmount(BigDecimal.valueOf(refund.getFinalRefundAmount()));
		subRefundReturn.setFlowTradeNo(refund.getFinalFlowTradeNo());
		subRefundReturn.setSubSettlementSn(refund.getFinalSettlementSn());
		subRefundReturn.setOrderDatetime(refund.getFinalOrderDateTime());
		subRefundReturn.setPayTypeId(refund.getFinalPayTypeId());
		subRefundReturn.setPayTypeName(refund.getPayTypeName());

		subRefundReturn.setShopId(refund.getShopId());
		subRefundReturn.setShopName(refund.getShopName());
		subRefundReturn.setRefundSn(CommonServiceUtil.getRandomSn(new Date(), "yyyyMMdd"));
		subRefundReturn.setUserId(userId);
		subRefundReturn.setUserName(userDetail.getUserName());
		subRefundReturn.setSellerState(RefundReturnStatusEnum.SELLER_AGREE.value());
		subRefundReturn.setApplyState(RefundReturnStatusEnum.APPLY_WAIT_ADMIN.value());
		subRefundReturn.setApplyTime(new Date());
		subRefundReturn.setBuyerMessage(form.getBuyerMessage());
		subRefundReturn.setReasonInfo("商家主动发起退款");
		subRefundReturn.setSellerTime(new Date());
		subRefundReturn.setSellerMessage("预售订单，商家主动发起退款");

		//保存退款及退货凭证图片
		if (AppUtils.isNotBlank(form.getPhotoFile1())) {
			subRefundReturn.setPhotoFile1(form.getPhotoFile1());
		}
		if (AppUtils.isNotBlank(form.getPhotoFile2())) {
			subRefundReturn.setPhotoFile2(form.getPhotoFile2());
		}
		if (AppUtils.isNotBlank(form.getPhotoFile3())) {
			subRefundReturn.setPhotoFile3(form.getPhotoFile3());
		}

		//订单全部退款,这些设置为0
		subRefundReturn.setSubItemId(0L);
		subRefundReturn.setProductImage(refund.getProdPic());
		subRefundReturn.setProductId(0L);
		subRefundReturn.setSkuId(0L);
		subRefundReturn.setProductName("预售订单退款");
		subRefundReturn.setApplyType(RefundReturnTypeEnum.REFUND.value());
		subRefundReturn.setRefundAmount(BigDecimal.valueOf(form.getRefundAmount()));//尾款金额

		if (SubTypeEnum.PRESELL.value().equals(refund.getSubType())) {//预售订单
			subRefundReturn.setIsPresell(true);
			if (refund.getPayPctType() == 1) {
				subRefundReturn.setRefundAmount(BigDecimal.valueOf(refund.getFinalRefundAmount()));
				subRefundReturn.setPayTypeId(refund.getPayTypeId());
				subRefundReturn.setFlowTradeNo(refund.getFlowTradeNo());
				subRefundReturn.setSubSettlementSn(refund.getSubSettlementSn());
				subRefundReturn.setOrderDatetime(refund.getOrderDateTime());
			}
		} else {
			subRefundReturn.setIsPresell(false);
		}

		subRefundReturn.setIsRefundDeposit(form.getIsRefundDeposit());
		subRefundReturn.setIsDepositHandleSucces(RefundReturnStatusEnum.DEPOSIT_REFUND_PROCESSING.value());

		subRefundReturn.setDepositRefund(BigDecimal.valueOf(refund.getDepositRefundAmount()));
		subRefundReturn.setRefundSouce(RefundSouceEnum.SHOP.value());

		subRefundReturn.setDepositFlowTradeNo(refund.getFlowTradeNo());
		subRefundReturn.setDepositSubSettlementSn(refund.getSubSettlementSn());
		subRefundReturn.setDepositOrderDatetime(refund.getOrderDateTime());
		subRefundReturn.setDepositPayTypeId(refund.getPayTypeId());
		subRefundReturn.setDepositPayTypeName(refund.getPayTypeName());

		subRefundReturnService.saveApplyOrderRefund(subRefundReturn);
		//商家发起退款，发送给用户的站内信通知
		sendSiteMessageProcessor.process(new SendSiteMsgEvent("系统", userDetail.getUserName(), "退款通知", "亲，您的订单[" + subRefundReturn.getSubNumber() + "],商家发起订金退款").getSource());
		return ResultDtoManager.success("提交成功！！！");
	}

	/**
	 * 检查订单是否允许退订金
	 *
	 * @param refund
	 * @param source 申请来源
	 * @return
	 */
	private boolean isAllowRefund(ApplyRefundReturnDto refund, Integer source) {

		//用户不能申请 退订金
		if (!(source == RefundSouceEnum.SHOP.value() || source == RefundSouceEnum.PLATFORM.value())) {
			return false;
		}
		//不是预售订单，没有退订金的说话
		if (!refund.getSubType().equals(SubTypeEnum.PRESELL.value())) {
			return false;
		}
		//已申请退订金，或已经退订金
		if (refund.getRefundStatus() != RefundReturnStatusEnum.ORDER_NO_REFUND.value()) {
			return false;
		}
		//如果是货到付款，不允许退款
		if (PayMannerEnum.DELIVERY_ON_PAY.value().equals(refund.getPayManner())) {
			return false;
		}
		//预售订金支付，订单状态还是未支付状态
		if (!(OrderStatusEnum.UNPAY.value().equals(refund.getOrderStatus()) || OrderStatusEnum.PADYED.value().equals(refund.getOrderStatus()))) {
			return false;
		}
		//定不是已支付状态，尾款不是未支付状态
		if ((refund.getIsPayDeposit() != 1 && refund.getIsPayFinal() != 0) || (refund.getIsPayDeposit() != 1 && refund.getIsPayFinal() != 1)) {
			return false;
		}
		return true;
	}

}
