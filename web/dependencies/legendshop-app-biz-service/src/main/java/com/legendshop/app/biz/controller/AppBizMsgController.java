/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.app.biz.controller;

import com.legendshop.app.biz.service.AppBizMessageService;
import com.legendshop.biz.model.dto.msg.AppBizSiteInformationDto;
import com.legendshop.model.constant.MsgStatusEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.MessageService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 消息管理
 * @author linzh
 */
@RestController
@Api(tags="消息管理",value="消息管理")
@RequestMapping("/s")
public class AppBizMsgController {
	
	private final Logger LOGGER = LoggerFactory.getLogger(AppBizMsgController.class);
	

	@Autowired
	private MessageService messageService;
	
	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private AppBizMessageService appBizMessageService;

	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;
	

	
	/**
	 * 获取系统通知列表
	 * @param curPageNO
	 * @return
	 */
	@ApiOperation(value = "获取系统通知列表", httpMethod = "POST", notes = "系统通知列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页", dataType = "string", required = false)
	@PostMapping("/msg/system/list")
	public  ResultDto<AppPageSupport<AppBizSiteInformationDto>> getSystemMessages(@RequestParam(defaultValue = "1") String curPageNO) {
		try {

			String userName = loginedUserService.getUser().getUserName();
			Integer gradeId = messageService.getGradeId(userName);
			
			UserDetail userDetail = userDetailService.getUserDetail(userName);
			
			AppPageSupport<AppBizSiteInformationDto> ps = appBizMessageService.querySystemMessages(curPageNO, gradeId, userName, userDetail);
			
			return ResultDtoManager.success(ps);
		} catch (Exception e) {
			LOGGER.error("获取系统通知列表异常：", e.getMessage());
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 获取普通消息列表
	 * @param curPageNO
	 * @return
	 */
	@ApiOperation(value = "获取普通消息列表", httpMethod = "POST", notes = "获取消息列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页", dataType = "string")
	@PostMapping("/msg/list")
	public  ResultDto<AppPageSupport<AppBizSiteInformationDto>> getInbox(@RequestParam(defaultValue = "1") String curPageNO) {
		try {
			String userName = loginedUserService.getUser().getUserName();
			AppPageSupport<AppBizSiteInformationDto> ps = appBizMessageService.queryInboxMsg(curPageNO, userName);
			return ResultDtoManager.success(ps);
		} catch (Exception e) {
			LOGGER.error("获取普通消息列表异常：", e.getMessage());
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 获取普通消息详情
	 * @param msgId
	 * @return
	 */
	@ApiOperation(value = "获取普通消息详情", httpMethod = "POST", notes = "获取普通消息详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "msgId", value = "消息ID", dataType = "string", required = true)
	@PostMapping("/msg/load")
	public  ResultDto<AppBizSiteInformationDto> loadMsg(@RequestParam(required = true) Long msgId) {
		try {
			
			String userName = loginedUserService.getUser().getUserName();

			AppBizSiteInformationDto msgDetail = appBizMessageService.getMsgDetail(msgId, userName);
			
			return ResultDtoManager.success(msgDetail);
		} catch (Exception e) {
			LOGGER.error("获取获取普通消息详情异常：", e.getMessage());
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 获取系统通详情
	 * @param msgId
	 * @return
	 */
	@ApiOperation(value = "获取系统通知详情", httpMethod = "POST", notes = "获取系统通知详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "msgId", value = "系统消息ID", required = true, dataType = "string")
	@PostMapping("/msg/system/load")
	public  ResultDto<AppBizSiteInformationDto> loadSystemMsg(@RequestParam(required = true) Long msgId) {
		try {
			String userName = loginedUserService.getUser().getUserName();

			AppBizSiteInformationDto systemMsgDetail = appBizMessageService.getSystemMsgDetail(msgId, userName);
			
			return ResultDtoManager.success(systemMsgDetail);
		} catch (Exception e) {
			LOGGER.error("获取系统通知详情异常：", e.getMessage());
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 删除普通消息
	 * @param ids 
	 * @return
	 */
	@ApiOperation(value = "删除普通消息", httpMethod = "POST", notes = "删除普通消息, 可多项删除，删除成功返回OK",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "ids", value = "收件箱邮件ids，删除多个用逗号,隔开", required = true, dataType = "string")
	@PostMapping("/msg/delete")
    public ResultDto<Object>  deleteInboxMessage(@RequestParam(required = true) String ids){
		try {
			String userName = loginedUserService.getUser().getUserName();
			
			String[] idsArray = ids.split(",");
			messageService.deleteInboxByMsgIds(idsArray, userName);
			
			return ResultDtoManager.success();
		} catch (Exception e) {
            return ResultDtoManager.fail();
		}
    }
	
	/**
	 * 删除 系统通知
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "删除系统通知", httpMethod = "POST", notes = "删除系统消息, 可多项删除，删除成功返回OK",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "ids", value = "系统消息ids，删除多个用逗号,隔开", dataType = "string", required = true)
	@PostMapping("/msg/system/delete")
	public  ResultDto<String>  deleteMessages(@RequestParam(required = true) String ids){
		
		try{
			String userName = loginedUserService.getUser().getUserName();
			
			String[] idsArray = ids.split(",");
			if(AppUtils.isNotBlank(idsArray)){
	    		for (String msgId : idsArray) {
	    			messageService.updateSystemMsgStatus(userName, Long.valueOf(msgId), MsgStatusEnum.DELETED.value());
				}
	    	}
			return ResultDtoManager.success();
		} catch(Exception e){
			e.printStackTrace();
			return ResultDtoManager.fail();
		}
    }
	

}
