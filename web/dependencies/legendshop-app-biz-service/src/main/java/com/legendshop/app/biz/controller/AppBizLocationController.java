package com.legendshop.app.biz.controller;

import com.legendshop.app.biz.service.AppBizLocationService;
import com.legendshop.biz.model.dto.KeyValueDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.util.JSONUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * 位置地区
 * @author 27158
 */
@Api(tags="位置地区",value="位置地区信息管理")
@RestController
public class AppBizLocationController {
	
	@Autowired
	private AppBizLocationService appBizLocationService;
	
	/**
	 *加载所有的省份
	 * @return the list
	 */
	@ApiOperation(value = "加载所有的省份", httpMethod = "POST", notes = "加载所有的省份",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/common/loadProvinces")
	public ResultDto<List<KeyValueDto>> loadProvinces() {
		
		System.out.println(JSONUtil.getJson(appBizLocationService.loadProvinces()));
		return ResultDtoManager.success(appBizLocationService.loadProvinces());
	}

	/**
	 * 加载省份的所有城市
	 *
	 * @param request the request
	 * @param response the response
	 * @param provinceId the provinceId
	 * @return the list
	 */
	@ApiOperation(value = "加载省份的所有城市", httpMethod = "POST", notes = "加载省份的所有城市",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/common/loadCities/{provinceId}")
	public ResultDto<List<KeyValueDto>> loadCities(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer provinceId) {
		return ResultDtoManager.success(appBizLocationService.loadCities(provinceId));
	}

	/**
	 * 加载城市的所有区域
	 *
	 * @param request the request
	 * @param response the response
	 * @param cityId the cityId
	 * @return the list
	 */
	@ApiOperation(value = "加载城市的所有区域", httpMethod = "POST", notes = "加载城市的所有区域",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/common/loadAreas/{cityId}")
	public  ResultDto<List<KeyValueDto>> loadAreas(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer cityId) {
		return ResultDtoManager.success(appBizLocationService.loadAreas(cityId));
	}
	
}
