package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.dao.AppBizReportDao;
import com.legendshop.app.biz.service.AppBizReportService;
import com.legendshop.biz.model.dto.AppBizIndexDto;
import com.legendshop.business.dao.ShopDetailDao;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 27158
 */
@Service("appBizReportService")
public class AppBizReportServiceImpl implements AppBizReportService {

	@Autowired
	private AppBizReportDao appBizReportDao;
	@Autowired
	private ShopDetailDao shopDetailDao;

	/**
	 * 获取商家端信息
	 * @param shopId
	 * @return
	 */
	@Override
	public AppBizIndexDto getShopSales(Long shopId) {

		ShopDetail shopDetailByShopId = shopDetailDao.getShopDetailNoCacheByShopId(shopId);

		String logoPic = shopDetailByShopId.getShopPic2();
		String siteName = shopDetailByShopId.getSiteName();
		String contactMobile = shopDetailByShopId.getContactMobile();
		AppBizIndexDto shopSales = appBizReportDao.getShopSales(shopId);
		if(AppUtils.isNotBlank(shopSales)){
//			计算出客单价=总销售额/订购用户数
			if (shopSales.getUserNum() > 0 && shopSales.getActualTotal() > 0){
				shopSales.setPerCapitaNumber(shopSales.getActualTotal()/shopSales.getUserNum());
			}else{
				shopSales.setPerCapitaNumber(0);
			}
		}
		shopSales.setShopId(shopDetailByShopId.getShopId());
		shopSales.setLogoPic(logoPic);
		shopSales.setShopName(siteName);
		shopSales.setContactMobile(contactMobile);
		return shopSales;
	}
}
