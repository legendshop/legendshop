package com.legendshop.app.biz.service.impl;


import com.legendshop.app.biz.service.AppBizShopOrderBillService;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.util.PageUtils;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.ShopOrderBill;
import com.legendshop.spi.service.ShopOrderBillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("appBizShopOrderBillService")
public class AppBizShopOrderBillServiceImpl implements AppBizShopOrderBillService {

	@Autowired
	private ShopOrderBillService shopOrderBillService;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	@Override
	public AppPageSupport<ShopOrderBill> getShopOrderBillPage(Long shopId, String curPageNO, String sn, Integer status) {
		ShopOrderBill shopOrderBill = new ShopOrderBill();
		shopOrderBill.setStatus(status);
		shopOrderBill.setSn(sn);
		shopOrderBill.setShopId(shopId);
		PageSupport<ShopOrderBill> ps = shopOrderBillService.getShopOrderBillPage(shopId, curPageNO, shopOrderBill);
		int shopBillDay = systemParameterUtil.getShopBillDay();
		for (ShopOrderBill orderBill : ps.getResultList()) {
			orderBill.setShopBillDay(shopBillDay);
		}
		AppPageSupport<ShopOrderBill> shopOrderBillAppPageSupport = PageUtils.convertPageSupport(ps);
		return shopOrderBillAppPageSupport;
	}
}
