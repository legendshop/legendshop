package com.legendshop.app.biz.controller;

import com.legendshop.app.biz.service.AppBizUserDetailService;
import com.legendshop.biz.model.dto.AppBizUserDetailDto;
import com.legendshop.model.constant.SMSTypeEnum;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.User;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.SMSLogService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * app商家信息管理控制器
 * @author 27158
 */
@RestController
@Api(tags="商家管理",value="商家资料查看、修改、编辑")
public class AppBizManageController {
	/** The log. */
	private final Logger LOGGER = LoggerFactory.getLogger(AppBizManageController.class);

	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private	AttachmentManager attachmentManager;
	@Autowired
	private SMSLogService smsLogService;

	@Autowired
	private AppBizUserDetailService appBizUserDetailService;
	/**
	 * 商家个人信息
	 * @return
	 */
	@ApiOperation(value = "商家个人信息", httpMethod = "POST", notes = "商家个人基本信息页面",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/s/shopInfo")
	public ResultDto<AppBizUserDetailDto> shopInfo() {
		try {
			String userId = loginedUserService.getUser().getUserId();
			if(AppUtils.isBlank(userId)){
				return ResultDtoManager.fail(-1, "对不起,您要查看的账号信息不存在或已被删除!");
			}
			AppBizUserDetailDto userDetailDto=appBizUserDetailService.getUserDetail(userId);
			return ResultDtoManager.success(userDetailDto);
		} catch (Exception e) {
			LOGGER.error("获取商家个人信息异常!", e);
			return ResultDtoManager.fail();
		}
	}

	/**
	 *  更新昵称
	 * @return
	 */
	@ApiOperation(value = "更新昵称", httpMethod = "POST", notes = "更新昵称信息,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "nickName", value = "新昵称", required = true, dataType = "string")
	@PostMapping(value="/s/updateNickName")
	public ResultDto<Object> updateNickName(@RequestParam String nickName) {

		try {
			String userId = loginedUserService.getUser().getUserId();
			int result = 	appBizUserDetailService.updateNickName(userId,nickName);
			if(result == 1){
				return ResultDtoManager.success(result);
			}else if(result == 0){
				return ResultDtoManager.fail(0, "昵称已经存在");
			}else{
				return ResultDtoManager.fail(-1, "用户不存在");
			}
		} catch (Exception e) {
			LOGGER.error("更新昵称失败异常!", e);
			return ResultDtoManager.fail();
		}
	}

	/**
	 *  更新头像
	 * @return
	 * @throws IOException
	 */
	@ApiOperation(value = "更新头像", httpMethod = "POST", notes = "更新头像,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "portraitPic", value = "新头像图片路径", required = true, dataType = "string")
	})
	@PostMapping(value="/s/updatePortrait")
	public ResultDto<String> updatePortrait1( @RequestParam String portraitPic) {

		try {
			String userId = loginedUserService.getUser().getUserId();
			UserDetail userDetail =  userDetailService.getUserDetailById(userId);
			String originPic = userDetail.getPortraitPic();
			//先删除旧头像
			if (AppUtils.isNotBlank(originPic)) {
				attachmentManager.delAttachmentByFilePath(originPic);
				attachmentManager.deleteTruely(originPic);
			}
			int flag = appBizUserDetailService.updatePortrait(userId, portraitPic);
			if(flag == 1){
				return ResultDtoManager.success();
			}else{
				return ResultDtoManager.fail(-1, "用户不存在");
			}
		} catch (Exception e) {
			LOGGER.error("更新头像失败异常!", e);
			return ResultDtoManager.fail();
		}
	}

	/**
	 * 校验原手机号码
	 * @return
	 */
	@ApiOperation(value = "校验原手机号码", httpMethod = "POST", notes = "校验原手机号码,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "mobile", value = "原手机号码", required = true, dataType = "string"),
			@ApiImplicitParam(paramType="query", name = "mobileCode", value = "验证码", required = true, dataType = "string")
	})
	@PostMapping(value="/s/validateUserMobile")
	public ResultDto<String> validateUserMobile(String mobile,String mobileCode) {

		try {
			String userId = loginedUserService.getUser().getUserId();
			UserDetail userDetail = userDetailService.getUserDetailById(userId);
			if (AppUtils.isBlank(userDetail)) {
				return ResultDtoManager.fail(-1, "该用户不存在或信息已被删除!");
			}

			if(AppUtils.isBlank(mobile)){
				return ResultDtoManager.fail(-1, "原手机号码不能为空!");
			}

			if (!userDetail.getUserMobile().equals(mobile)) {
				return ResultDtoManager.fail(-1, "原手机号码不正确，请重新输入!");
			}

			if(AppUtils.isBlank(mobileCode) || !checkSMSCode(mobile,mobileCode)){
				return ResultDtoManager.fail(-1, "请输入正确的验证码!");
			}
			return ResultDtoManager.success();
		} catch (Exception e) {
			LOGGER.error("校验手机号码异常!", e);
			return ResultDtoManager.fail();
		}
	}

	/**
	 * 更新手机号码
	 * @return
	 */
	@ApiOperation(value = "更新手机号码", httpMethod = "POST", notes = "更新手机号码,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "mobile", value = "新手机号码", required = true, dataType = "string"),
			@ApiImplicitParam(paramType="query", name = "mobileCode", value = "短信验证码", required = true, dataType = "string")
	})
	@PostMapping(value="/s/updateMobile")
	public ResultDto<String> updateMobile(String mobile,String mobileCode) {

		try {

			String userName = loginedUserService.getUser().getUserName();
			if(AppUtils.isBlank(userName)){
				return ResultDtoManager.fail(-1, "该用户不存在!");
			}

			if(AppUtils.isBlank(mobile)){
				return ResultDtoManager.fail(-1, "手机号码不能为空!");
			}

			boolean isExist = userDetailService.isPhoneExist(mobile);
			if (isExist) {
				return ResultDtoManager.fail(-1, "该手机号码已被注册，请重新输入!");
			}

			if(AppUtils.isBlank(mobileCode) || !checkSMSCode(mobile,mobileCode)){
				return ResultDtoManager.fail(-1, "请输入正确的验证码!");
			}

			userDetailService.updateUserMobile(mobile,userName);
			return ResultDtoManager.success();
		} catch (Exception e) {
			LOGGER.error("校验手机号码异常!", e);
			return ResultDtoManager.fail();
		}
	}
	/**
	 * 修改密码
	 * @param newPwd
	 * @return
	 */
	@ApiOperation(value = "修改密码", httpMethod = "POST", notes = "修改密码,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "mobileCode", value = "短信验证码", required = true, dataType = "string"),
			@ApiImplicitParam(paramType="query", name = "newPwd", value = "新密码", required = true, dataType = "string")

	})
	@PostMapping(value="/s/changePwd")
	public ResultDto<String> changePwd(String mobileCode, String newPwd){

		try {

			String userName = loginedUserService.getUser().getUserName();
			if(AppUtils.isBlank(userName)){
				return ResultDtoManager.fail(-1, "未登录!");
			}

			UserDetail userDetail = userDetailService.getUserDetail(userName);

			if(AppUtils.isBlank(userDetail)){
				return ResultDtoManager.fail(-1, "该用户不存在或信息已被删除");
			}

			if(!smsLogService.verifyCodeAndClear(userDetail.getUserMobile(), mobileCode) ){
				return ResultDtoManager.fail(-1, "短信验证码错误！");
			}

			User user = userDetailService.getUserByName(userName);
			if (AppUtils.isBlank(user)){
				return ResultDtoManager.fail(-1, "该用户不存");
			}

			user.setPassword(passwordEncoder.encode(newPwd));
			userDetailService.updateUser(user);
			return ResultDtoManager.success();

		} catch (Exception e) {
			LOGGER.error("更新用户密码异常!", e);
			return ResultDtoManager.fail();
		}
	}
	/**
	 * 判断手机验证码
	 * @param mobile
	 * @param code
	 * @return
	 */
	private boolean checkSMSCode(String mobile,String code){
		return smsLogService.verifyCodeAndClear(mobile,code, SMSTypeEnum.VAL);
	}

}
