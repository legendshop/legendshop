package com.legendshop.app.biz.dao.impl;

import com.legendshop.app.biz.dao.AppBizShopDetailDao;
import com.legendshop.biz.model.dto.AppBizDetailDto;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.model.entity.ShopDetail;
import org.springframework.stereotype.Repository;

/**
 * @author 27158
 */
@Repository
public class AppBizShopDetailDaoImpl extends GenericDaoImpl<ShopDetail,Long> implements AppBizShopDetailDao {

	private final static String GETSHOPDETAILBYSHOPID="SELECT " +
			"sh.shop_id as shopId,sh.user_id as userId,sh.user_name as userName,sh.site_name as siteName,sh.shop_pic2 as logoPic,"+
			"sh.contact_name as contactName,sh.contact_mobile as contactMobile,sh.provinceId as provinceid,sh.cityid as cityid,"+
			"sh.areaid as areaid,p.province as province,c.city as city,a.area as area,sh.shop_addr as shopAddr " +
			"FROM ls_shop_detail sh ,ls_provinces p ,ls_cities c ,ls_areas a  WHERE sh.provinceid=p.id and sh.cityid=c.id and sh.areaid=a.id  and sh.shop_id=?";

	@Override
	public AppBizDetailDto getShopDetail(Long shopId) {

		return 	this.get(GETSHOPDETAILBYSHOPID, AppBizDetailDto.class, shopId);

	}
}
