package com.legendshop.app.biz.dao.impl;

import com.legendshop.app.biz.dao.AppBizReportDao;
import com.legendshop.biz.model.dto.AppBizIndexDto;
import com.legendshop.dao.dialect.Dialect;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @author 27158
 */
@Repository
public class AppBizReportDaoImpl extends GenericDaoImpl<ShopDetail,Long> implements AppBizReportDao {

	/** The dialect. */
	@Autowired
	private Dialect dialect;

	@Override
	public AppBizIndexDto getShopSales(Long shopId) {
		Date startMonment = DateUtils.getStartMonment(new Date());
		Date endMonment = DateUtils.getEndMonment(new Date());
		String sql = ConfigCode.getInstance().getCode(dialect.getDialectType() + ".reportGetShopSales");
		return this.get(sql, AppBizIndexDto.class, shopId, startMonment, endMonment);
	}
}
