package com.legendshop.app.biz.dao.impl;

import com.legendshop.app.biz.dao.AppBizOrderDao;
import com.legendshop.biz.model.dto.order.*;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.dao.support.DefaultPagerProvider;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.dto.order.OrderDetailDto;
import com.legendshop.model.dto.order.OrderSearchParamDto;
import com.legendshop.model.entity.Sub;
import com.legendshop.util.AppUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class AppBizOrderDaoImpl extends GenericDaoImpl<Sub, Long> implements AppBizOrderDao {
	@Override
	public PageSupport<AppBizOrderDto> getBizShopOrderDtos(OrderSearchParamDto paramDto, String condition) {
		List<Object> args = new ArrayList<>();
		int curPageNO = 1;
		try {
			curPageNO=Integer.valueOf(paramDto.getCurPageNO());
		} catch (Exception e) {
			curPageNO = 1;
		}
		if(curPageNO <=0){
			//防止溢出
			curPageNO = 1;
		}
		int pageSize=paramDto.getPageSize();
		int offset=(curPageNO-1)*paramDto.getPageSize();
		QueryMap map = new QueryMap();
		if (AppUtils.isNotBlank(paramDto)){
			//商家id
			map.put("shopId",paramDto.getShopId());
			args.add(paramDto.getShopId());
			//订单状态
			if (AppUtils.isNotBlank(paramDto.getStatus())){
				//（1-5）订单状态
				if (paramDto.getStatus()<6){
					map.put("status", paramDto.getStatus());
					args.add(paramDto.getStatus());
				}else if (paramDto.getStatus()==6){//待成团
					map.put("groupType", CartTypeEnum.GROUP.toCode());
					args.add(CartTypeEnum.GROUP.toCode());
					map.put("mergeType", CartTypeEnum.MERGE_GROUP.toCode());
					args.add(CartTypeEnum.MERGE_GROUP.toCode());
					map.put("groupStatus", 0);
					args.add(0);
					map.put("mergeStatus", 0);
					args.add(0);
				}else if (paramDto.getStatus()==7){//待支付尾款
					map.put("isPayFinal", 0);
					args.add(0);
					map.put("isPayDeposit", 1);
					args.add(1);
					map.put("subType", CartTypeEnum.PRESELL.toCode());
					args.add(CartTypeEnum.PRESELL.toCode());
					map.put("status", OrderStatusEnum.PADYED.value());
					args.add(OrderStatusEnum.PADYED.value());
				}else if (paramDto.getStatus()==8){//代提货
					map.put("status", OrderStatusEnum.PADYED.value());
					args.add(OrderStatusEnum.PADYED.value());
					map.put("subType", CartTypeEnum.SHOP_STORE.toCode());
					args.add(CartTypeEnum.SHOP_STORE.toCode());
				}
			}
			//订单类型
			if(AppUtils.isNotBlank(paramDto.getSubType())){

				Object subType = map.get("subType");
				if(AppUtils.isBlank(subType)){
					map.put("subType", paramDto.getSubType());
					args.add(paramDto.getSubType());
				}
			}
			//维权状态
			if (AppUtils.isNotBlank(paramDto.getRefundStatus())) {
				map.put("refundState", paramDto.getRefundStatus());
				args.add(paramDto.getRefundStatus());
			}
			//是否支付
			if(AppUtils.isNotBlank(paramDto.getIsPayed())){
				map.put("isPayed", paramDto.getIsPayed());
				args.add(paramDto.getIsPayed());
			}
			//订单删除状态
			if(AppUtils.isNotBlank(paramDto.getDeleteStatus()) && !paramDto.isIncludeDeleted()){
				map.put("deleteStatus", paramDto.getDeleteStatus());
				args.add(paramDto.getDeleteStatus());
			}
		}
		if (AppUtils.isNotBlank(condition)){
			map.put("mobile",condition);
			args.add(condition);
			map.put("subNumber",condition);
			args.add(condition);
			map.put("receiver",condition);
			args.add(condition);
		}
		map.put("offset", offset);
		map.put("pageSize", pageSize);
		String queryAllSQL = ConfigCode.getInstance().getCode("app.biz.queryUserOrdersCount", map);
		String querySQL = ConfigCode.getInstance().getCode("app.biz.queryUserOrders", map);

//		query.setAllCountString(queryAllSQL);
//		query.setQueryString(querySQL);
//		query.setParam(map.toArray());
		Long total = getLongResult(queryAllSQL, args.toArray());
		List<AppBizOrderDto> mySubDtos = getMySubDtos(querySQL, args);
		PageSupport<AppBizOrderDto> pageSupport = initPageProvider(total, Integer.parseInt(paramDto.getCurPageNO()), paramDto.getPageSize(), mySubDtos);

		return pageSupport;
	}

	private PageSupport<AppBizOrderDto> initPageProvider(long total, Integer curPageNO, Integer pageSize, List<AppBizOrderDto> resultList) {
		DefaultPagerProvider pageProvider = new DefaultPagerProvider(total, curPageNO, pageSize);
		PageSupport<AppBizOrderDto> ps = new PageSupport<AppBizOrderDto>(resultList, pageProvider);
		return ps;
	}

	@Override
	public AppBizOrderDetailDto getBizShopOrderDetail(String subNumber, Long shopId) {
		QueryMap queryMap = new QueryMap();
		queryMap.put("shopId",shopId);
		String sql = ConfigCode.getInstance().getCode("app.biz.queryOrderDetail", queryMap);
		return get(sql,AppBizOrderDetailDto.class,subNumber,shopId);
	}

	@Override
	public AppBizOrderPriceDto getBizOrderPriceDto(String subNumber)
	{
		String sql="SELECT o.sub_id AS subId,o.sub_number AS subNumber,o.user_name AS userName,o.total AS total,o.actual_total AS  actualTotal,o.sub_type AS subType,o.freight_amount AS freightAmount,pre_deposit_price AS preDepositPrice,pay_pct_type AS payPctType,final_price AS finalPrice" +
				"  FROM ls_sub AS o" +
				"  LEFT JOIN ls_presell_sub AS psub" +
				"  ON o.sub_id=psub.sub_id" +
				"  WHERE o.sub_number=?";
		return get(sql,AppBizOrderPriceDto.class,subNumber);
	}

	@Override
	public AppBizOrderRemarkDto getOrderRemarkInfo(String subNumber) {
		String sql="SELECT" +
				"  sub_id AS subId," +
				"  sub_number AS subNumber," +
				"  shop_remark AS shopRemark," +
				"  shop_remark_date AS shopRemarkDate" +
				"  FROM" +
				"  ls_sub" +
				"  WHERE sub_number = ?";
		return get(sql,AppBizOrderRemarkDto.class,subNumber);
	}

	private List<AppBizOrderDto> getMySubDtos(String sql, List<Object> args) {
		Map<String, AppBizOrderDto> map = new HashMap<String, AppBizOrderDto>();
		super.query(sql, args.toArray(), new MySubDtoListRowMapper(map));
		if (AppUtils.isBlank(map)) {
			return null;
		}
		List<AppBizOrderDto> subDtos = new ArrayList<AppBizOrderDto>();
		for (Map.Entry<String, AppBizOrderDto> entry : map.entrySet()) {
			AppBizOrderDto mySubDto = entry.getValue();
			subDtos.add(mySubDto);
		}
		Collections.sort(subDtos, new Comparator<AppBizOrderDto>() {
			@Override
			public int compare(AppBizOrderDto o1, AppBizOrderDto o2) {
				int flag = o2.getSubDate().compareTo(o1.getSubDate());
				return flag;
			}
		});
		return subDtos;
	}


	class MySubDtoListRowMapper implements RowMapper<OrderDetailDto> {

		private Map<String, AppBizOrderDto> map = null;

		public MySubDtoListRowMapper(Map<String, AppBizOrderDto> map) {
			this.map = map;
		}

		@Override
		public OrderDetailDto mapRow(ResultSet rs, int rowNum) throws SQLException {
			String subNumber = rs.getString("subNumber");
			if (map.containsKey(subNumber)) { // 存在
				AppBizOrderDto mySubDto = map.get(subNumber);
				AppBizOrderItemDto mySubItemDto = buildSubItem(rs);
				mySubDto.addItem(mySubItemDto);
			} else {
				AppBizOrderDto dto = buildSub(rs);
				AppBizOrderItemDto mySubItemDto = buildSubItem(rs);

				dto.addItem(mySubItemDto);

				map.put(subNumber, dto);
			}
			return null;
		}

		public AppBizOrderDto buildSub(ResultSet rs) throws SQLException {
			AppBizOrderDto dto = new AppBizOrderDto();
			dto.setSubId(rs.getLong("subId"));
			dto.setSubNumber(rs.getString("subNumber"));
			dto.setUserId(rs.getString("userId"));
			dto.setUserName(rs.getString("userName"));
			dto.setSubType(rs.getString("subType"));
			dto.setActualTotal(rs.getDouble("actualTotal"));
			dto.setPayManner(rs.getInt("payManner"));
			dto.setShopId(rs.getLong("shopId"));
			dto.setStatus(rs.getInt("status"));
			dto.setFreightAmount(rs.getDouble("freightAmount"));
			dto.setSubDate(rs.getTimestamp("subDate"));
			dto.setRefundState(rs.getLong("refundState"));
			dto.setDvyTypeId(rs.getLong("dvyTypeId"));
			//备注
			dto.setIsShopRemarked(rs.getInt("isShopRemarked") == 1 ? true : false);
			dto.setActiveId(rs.getLong("activeId"));
			dto.setGroupStatus(rs.getInt("groupStatus"));
			dto.setMergeGroupStatus((Integer) rs.getObject("mergeGroupStatus"));
			dto.setAddNumber(rs.getString("addNumber"));
			dto.setIsNeedInvoice(rs.getInt("isNeedInvoice"));
			dto.setProductNums(rs.getInt("productNums"));
			dto.setPayDate(rs.getTimestamp("payDate"));

			//2018-12-14  新增 预售订单的信息
			dto.setPreDepositPrice(rs.getBigDecimal("preDepositPrice"));
			dto.setIsPayDeposit(rs.getInt("isPayDeposit"));
			dto.setFinalPrice(rs.getBigDecimal("finalPrice"));
			dto.setPayPctType(rs.getInt("payPctType"));
			dto.setFinalMStart(rs.getTimestamp("finalMStart"));
			dto.setFinalMEnd(rs.getTimestamp("finalMEnd"));
			dto.setIsPayFinal(rs.getInt("isPayFinal"));
			dto.setFinallyDate(rs.getTimestamp("finallyDate"));
			dto.setPortraitPic(rs.getString("portraitPic"));
			dto.setIsPayed(rs.getInt("isPayed"));
			return dto;
		}

		private AppBizOrderItemDto buildSubItem(ResultSet rs) throws SQLException {
			AppBizOrderItemDto subOrderItemDto = new AppBizOrderItemDto();
			subOrderItemDto.setSubItemId(rs.getLong("subItemId"));
			subOrderItemDto.setProdId(rs.getLong("prodId"));
			subOrderItemDto.setBasketCount(rs.getInt("basketCount"));
			subOrderItemDto.setProdName(rs.getString("itemProdName"));
			subOrderItemDto.setAttribute(rs.getString("attribute"));
			subOrderItemDto.setPic(rs.getString("pic"));
			subOrderItemDto.setCash(rs.getDouble("cash"));
			subOrderItemDto.setProductTotalAmout(rs.getDouble("productTotalAmout"));
			subOrderItemDto.setCommSts(rs.getInt("commSts"));
			subOrderItemDto.setRefundId(rs.getLong("refundId"));
			subOrderItemDto.setRefundType(rs.getLong("refundType"));
			subOrderItemDto.setRefundState(rs.getLong("itemRefundState"));
			subOrderItemDto.setRefundAmount(rs.getDouble("refundAmount"));
			return subOrderItemDto;
		}
	}
}
