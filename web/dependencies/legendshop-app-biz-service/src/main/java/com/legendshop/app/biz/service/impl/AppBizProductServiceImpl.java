package com.legendshop.app.biz.service.impl;

import com.legendshop.advanced.search.processor.ProductIndexDelProcessor;
import com.legendshop.advanced.search.processor.ProductIndexUpdateProcessor;
import com.legendshop.app.biz.dao.AppBizProductDao;
import com.legendshop.app.biz.dao.AppBizShopDetailDao;
import com.legendshop.app.biz.dto.AppBizProductDetailDto;
import com.legendshop.app.biz.dto.AppBizPropValueImgDto;
import com.legendshop.app.biz.dto.AppBizSkuDto;
import com.legendshop.app.biz.service.AppBizProductService;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.biz.model.dto.AppBizMarketingProdDto;
import com.legendshop.biz.model.dto.AppBizMarketingProdSkuDto;
import com.legendshop.biz.model.dto.AppProdCommDto;
import com.legendshop.biz.model.dto.comment.AppBizProductCommentsParamsDto;
import com.legendshop.biz.model.dto.prod.AppBizProdCategoryDto;
import com.legendshop.biz.model.dto.prod.AppBizProdDto;
import com.legendshop.biz.model.dto.prod.AppBizProductPropertyDto;
import com.legendshop.biz.model.dto.prod.AppBizProductPropertyValueDto;
import com.legendshop.business.dao.*;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.dao.support.DefaultPagerProvider;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.constant.*;
import com.legendshop.model.dto.*;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.buy.CartMarketRules;
import com.legendshop.model.dto.marketing.RuleResolver;
import com.legendshop.model.entity.*;
import com.legendshop.model.prod.FullActiveProdDto;
import com.legendshop.spi.service.ActiveRuleResolverManager;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author 27158
 */
@Service("appBizProductService")
public class AppBizProductServiceImpl implements AppBizProductService {

	@Autowired
	private AppBizShopDetailDao appBizShopDetailDao;

	@Autowired
	private ShopDetailDao shopDetailDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private SkuDao skuDao;

	@Autowired
	private MarketingProdsDao marketingProdsDao;

	@Autowired
	private MarketingDao marketingDao;

	@Autowired
	private ProductIndexDelProcessor productIndexDelProcessor;

	@Autowired
	private ProductIndexUpdateProcessor productIndexUpdateProcessor;

	@Autowired
	private BasketDao basketDao;

	@Autowired
	private AppBizProductDao appBizProductDao;

	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;

	@Autowired
	private ProductService productService;

	@Autowired
	private ActiveRuleResolverManager activeRuleResolverManager;

	@Autowired
	private CategoryService categoryService;

	@Override
	public AppPageSupport<AppBizProdDto> queryProductList(String curPageNO, Long shopId, Integer status, Integer supportDist,String prodName) {
		PageSupport<Product> ps = productDao.queryProductList(curPageNO, shopId, status, supportDist,prodName);
		List<AppBizProdDto> appBizProdDtoList = new ArrayList<>();
		if (AppUtils.isNotBlank(ps.getResultList())) {
			for (Product product : ps.getResultList()) {
				AppBizProdDto appBizProdDto = new AppBizProdDto();
				appBizProdDto.setBuys(product.getBuys());
				appBizProdDto.setName(product.getName());
				appBizProdDto.setPic(product.getPic());
				appBizProdDto.setProdId(product.getProdId());
				appBizProdDto.setPrice(product.getCash());
				appBizProdDto.setStatus(product.getStatus());
				appBizProdDto.setStocks(product.getStocks());
				appBizProdDto.setSupportDist(product.getSupportDist());
				appBizProdDto.setStocksArm(product.getStocksArm());
				appBizProdDtoList.add(appBizProdDto);
			}
		}
		AppPageSupport appPs = new AppPageSupport();
		appPs.setResultList(appBizProdDtoList);
		appPs.setPageCount(ps.getPageCount());
		appPs.setCurrPage(ps.getCurPageNO());
		appPs.setPageSize(ps.getPageSize());
		appPs.setTotal(ps.getTotal());
		return appPs;
	}


	@Override
	public AppPageSupport<AppBizMarketingProdDto> getProductByShopId(String curPageNO, Long shopId, String name, Long shopCatId, Integer shopCatType, String activityType) {
		ActivityTypeEnum typeEnum = ActivityTypeEnum.matchType(activityType);
		PageSupport<Product> ps = null;
		switch (typeEnum) {
			// 2020-08-19 目前只有包邮自己与自己冲突，另做处理
			case FREE_SHIPPING:
				ps = productDao.getFreeShippingProductByShopId(curPageNO, name, shopId, shopCatId, shopCatType);
				return convertPageSupportDto(ps);
			// PC一致
			case FULL_REDUCTION:
			case DISCOUNT:
			case COUPON:
			// 2020-08-19 目前商家移动端没有以下功能，防止前端还有其它页面也调这个接口，做兼容
			case GROUP:
			case MERGE_GROUP:
			case SECKILL:
			case AUCTION:
			case PRESELL:
			default:
				ps = productDao.getProdductByShopId(curPageNO, name, shopId,shopCatId,shopCatType);
				return convertPageSupportDto(ps);
		}
	}

	private AppPageSupport<AppBizMarketingProdDto> convertPageSupportDto(PageSupport<Product>  pageSupport){
		if(AppUtils.isNotBlank(pageSupport)){
			AppPageSupport<AppBizMarketingProdDto> pageSupportDto = new AppPageSupport<>();
			pageSupportDto.setTotal(pageSupport.getTotal());
			pageSupportDto.setCurrPage(pageSupport.getCurPageNO());
			pageSupportDto.setPageSize(pageSupport.getPageSize());
			pageSupportDto.setPageCount(pageSupport.getPageCount());

			List<AppBizMarketingProdDto> appBizMarketingProdDtos = convertMarketingProdDtoList(pageSupport.getResultList());
			pageSupportDto.setResultList(appBizMarketingProdDtos);
			return pageSupportDto;
		}
		return null;
	}

	private List<AppBizMarketingProdDto> convertMarketingProdDtoList(List<Product> productList){
		if(AppUtils.isNotBlank(productList)){
			List<AppBizMarketingProdDto> dtoList = new ArrayList<>();
			productList.stream().forEach(product -> {
				AppBizMarketingProdDto appBizMarketingProdDto =new AppBizMarketingProdDto();
				appBizMarketingProdDto.setProdId(product.getProdId());
				appBizMarketingProdDto.setProdName(product.getName());
				appBizMarketingProdDto.setStocks(product.getStocks());
				appBizMarketingProdDto.setCash(product.getCash());
				appBizMarketingProdDto.setPic(product.getPic());

//				根据prodID 获取商品sku列表
				List<Sku> skuByProd = skuDao.getSkuByProd(product.getProdId());
				if(AppUtils.isNotBlank(skuByProd)){
					List<AppBizMarketingProdSkuDto> marketingProdSkuDtoList=new ArrayList<>();
					skuByProd.forEach(sku -> {
						AppBizMarketingProdSkuDto appBizMarketingProdSkuDto = new AppBizMarketingProdSkuDto();
						appBizMarketingProdSkuDto.setSkuId(sku.getSkuId());
						appBizMarketingProdSkuDto.setProdId(sku.getProdId());
						appBizMarketingProdSkuDto.setName(sku.getName());
						appBizMarketingProdSkuDto.setPic(sku.getPic());
						appBizMarketingProdSkuDto.setPrice(sku.getPrice());
						appBizMarketingProdSkuDto.setCnProperties(sku.getCnProperties());
						appBizMarketingProdSkuDto.setStocks(sku.getStocks().intValue());
						marketingProdSkuDtoList.add(appBizMarketingProdSkuDto);
					});
					appBizMarketingProdDto.setMarketingProdSkuDtos(marketingProdSkuDtoList);
				}
				dtoList.add(appBizMarketingProdDto);
			});
			return dtoList;
		}
		return null;
	}

	/**
	 * 修改商品状态
	 *
	 * @param shopId
	 * @param productIds
	 * @param status
	 * @return
	 */
	@Override
	public String updateProdStatus(Long shopId, List<Long> productIds, Integer status) {
		ShopDetail shopDetail = shopDetailDao.getShopDetailNoCacheByShopId(shopId);
		if (AppUtils.isBlank(shopDetail) || !ShopStatusEnum.NORMAL.value().equals(shopDetail.getStatus())) {
			return Constants.FAIL;
		}
		//查找所有商品
		List<Product> selProdictList = productDao.getProductListByIds(productIds);
		if (selProdictList.size() != productIds.size()) {
			return Constants.FAIL;
		}
		if (ProductStatusEnum.PROD_ONLINE.value().equals(status)) {
			//上线商品
			for (Product product : selProdictList) {
				if (!ProductStatusEnum.PROD_OFFLINE.value().equals(product.getStatus())) {
					return Constants.FAIL;
				}
				product.setStatus(ProductStatusEnum.PROD_ONLINE.value());
			}
			productDao.batchUpdateProduct(selProdictList);
			//更新商品出售数量
			shopDetailDao.updateShopDetailProdNum(shopId);
			productIndexUpdateProcessor.process(new ProductIdDto(productIds));
		} else if (ProductStatusEnum.PROD_OFFLINE.value().equals(status)) {
			//下线商品、仓库中的商品
			for (Product product : selProdictList) {
				if (!ProductStatusEnum.PROD_ONLINE.value().equals(product.getStatus())) {
					return Constants.FAIL;
				}
				product.setStatus(ProductStatusEnum.PROD_OFFLINE.value());
				//下线，如果有营销活动，更改营销活动
				List<MarketingProds> prods = marketingProdsDao.getmarketingProdByProdId(product.getProdId());
				if (AppUtils.isNotBlank(prods)) {
					List<Long> ids = new ArrayList<Long>();
					for (MarketingProds marketingProds : prods) {
						ids.add(marketingProds.getMarketId());
					}
					if (ids.size() > 0) {
						marketingDao.updateMarketStatus(ids);
					}
				}
				//清除缓存
				productDao.cleanProductCache(product);
			}
			productDao.batchUpdateProduct(selProdictList);

			//更新商品出售数量
			shopDetailDao.updateShopDetailProdNum(shopId);
			//更改用户购物车的商品为失效
			basketDao.batchUpdateIsFailureByIds(true, productIds);
			productIndexDelProcessor.process(new ProductIdDto(productIds));
		} else if (ProductStatusEnum.PROD_DELETE.value().equals(status)) {
			for (Product product : selProdictList) {
				if (ProductStatusEnum.PROD_ONLINE.value().equals(product.getStatus())
						|| ProductStatusEnum.PROD_DELETE.value().equals(product.getStatus())
						|| ProductStatusEnum.PROD_SHOP_DELETE.value().equals(product.getStatus())) {
					return Constants.FAIL;
				}
				product.setStatus(ProductStatusEnum.PROD_DELETE.value());
			}
			productDao.batchUpdateProduct(selProdictList);
			productIndexDelProcessor.process(new ProductIdDto(productIds));
		}
		return Constants.SUCCESS;
	}

	@Override
	public String quiteDistribution(Long shopId, List<Long> productIds, Integer supportDist) {
		ShopDetail shopDetail = shopDetailDao.getShopDetailNoCacheByShopId(shopId);
		if (AppUtils.isBlank(shopDetail) || !ShopStatusEnum.NORMAL.value().equals(shopDetail.getStatus())) {
			return Constants.FAIL;
		}

		//查找所有商品
		List<Product> selProdictList = productDao.getProductListByIds(productIds);
		if (selProdictList.size() != productIds.size()) {
			return Constants.FAIL;
		}
		for (Product product : selProdictList) {
			if (product.getSupportDist() == 0) {
				return Constants.FAIL;
			}
			product.setSupportDist(SupportDistEnum.NOT_SUPPORT.value());
			product.setDistCommisRate(0d);
			product.setModifyDate(new Date());
			//清除缓存
			productDao.cleanProductCache(product);
		}
		productDao.batchUpdateProduct(selProdictList);
		productIndexUpdateProcessor.process(new ProductIdDto(productIds));
		return Constants.SUCCESS;
	}

	@Override
	public String updateShopCategory(Long shopId, List<Long> productIds, Long firstCatId, Long secondCatId, Long thirdCatId) {
		ShopDetail shopDetail = shopDetailDao.getShopDetailNoCacheByShopId(shopId);
		if (AppUtils.isBlank(shopDetail) || !ShopStatusEnum.NORMAL.value().equals(shopDetail.getStatus())) {
			return Constants.FAIL;
		}
		//查找所有商品
		List<Product> selProdictList = productDao.getProductListByIds(productIds);
		if (selProdictList.size() != productIds.size()) {
			return Constants.FAIL;
		}
		for (Product product : selProdictList) {
			product.setShopFirstCatId(firstCatId);
			product.setShopSecondCatId(secondCatId);
			product.setShopThirdCatId(thirdCatId);
			//清除缓存
			productDao.cleanProductCache(product);
		}
		productDao.batchUpdateProduct(selProdictList);
		productIndexUpdateProcessor.process(new ProductIdDto(productIds));
		return Constants.SUCCESS;
	}

	@Override
	public AppBizProductDetailDto getProductDetailDto(Long prodId) {
		ProductDto prod = productService.getProductDto(prodId);
		if (prod == null) {
			return null;
		}

		//计算 营销活动
		calculateMarketing(prod);

		return convertProductDetailDto(prod);
	}

	private void calculateMarketing(ProductDto prod) {
		List<SkuDto> list = prod.getSkuDtoList();
		if (AppUtils.isBlank(list)) {
			return;
		}
		if (!ProductTypeEnum.PRODUCT.value().equals(prod.getProdType())) {
			return;
		}
		if (prod.getIsGroup() != null && prod.getIsGroup().intValue() == 1) { //如果是团购,不参与计算营销
			return;
		}
		for (SkuDto skuDto : list) {
			RuleResolver resolver = new RuleResolver();
			resolver.setPrice(skuDto.getPrice());
			resolver.setPromotionPrice(skuDto.getPrice());
			resolver.setBasketCount(1);
			resolver.setShopId(prod.getShopId());
			resolver.setProdId(prod.getProdId());
			resolver.setSkuId(skuDto.getSkuId());
			/**
			 * 查询该商品的参与的营销规则-->计算价格
			 */
			activeRuleResolverManager.executorMarketing(resolver);

			List<CartMarketRules> marketRules = resolver.getCartMarketRules();
			skuDto.setPromotionPrice(resolver.getPromotionPrice()); //设置计算营销后的价格
			skuDto.setRuleList(marketRules);
			resolver = null;
		}
	}

	/**
	 * @Description: 将productDto转ProductDetailDto
	 * @date 2016-7-22
	 */
	private AppBizProductDetailDto convertProductDetailDto(ProductDto productDto) {
		AppBizProductDetailDto productDetailDto = new AppBizProductDetailDto();
		productDetailDto.setName(productDto.getName());
		productDetailDto.setStatus(productDto.getStatus());
		productDetailDto.setProdId(productDto.getProdId());
		productDetailDto.setSkuList(convertSkuDto(productDto.getSkuDtoList()));  //sku集合
		productDetailDto.setPic(productDto.getPic());
		productDetailDto.setShopId(productDto.getShopId());
		productDetailDto.setPrice(productDto.getPrice());
		productDetailDto.setCash(productDto.getCash());
		productDetailDto.setContentM(productDto.getContentM());//手机端图文详情
		productDetailDto.setProdPropList(convertProductPropertyDto(productDto.getProdPropDtoList()));
		productDetailDto.setProdPics(convertProductPicDto(productDto.getProdPics(), productDto.getPropValueImgList()));
		productDetailDto.setBrief(productDto.getBrief());
		productDetailDto.setBrandId(productDto.getBrandId());
		productDetailDto.setBrandName(productDto.getBrandName());
		productDetailDto.setPropValueImgList(convertPropValueImgList(productDto.getPropValueImgList()));//商品属性图片集合
		productDetailDto.setStartDate(productDto.getStartDate());
		productDetailDto.setProVideoUrl(productDto.getProVideoUrl());
		return productDetailDto;
	}

	/**
	 * 将model sku转换app SkuDto
	 */
	private List<AppBizSkuDto> convertSkuDto(List<SkuDto> skuDtoList) {
		if (AppUtils.isNotBlank(skuDtoList)) {
			List<AppBizSkuDto> appSkuDtoList = new ArrayList<AppBizSkuDto>();
			for (SkuDto skuDto : skuDtoList) {
				AppBizSkuDto appSkuDto = new AppBizSkuDto();
				appSkuDto.setSkuId(skuDto.getSkuId());
				appSkuDto.setName(skuDto.getName());
				appSkuDto.setStatus(skuDto.getStatus());
				appSkuDto.setStocks(skuDto.getStocks());
				appSkuDto.setCash(skuDto.getCash());
				appSkuDto.setPromotionPrice(skuDto.getPromotionPrice());
				appSkuDto.setProperties(skuDto.getProperties());
				appSkuDto.setRuleList(skuDto.getRuleList());
				appSkuDto.setModelId(skuDto.getModelId());
				appSkuDto.setPrice(skuDto.getPrice());
				appSkuDto.setSkuId(skuDto.getSkuId());
				appSkuDtoList.add(appSkuDto);
			}
			return appSkuDtoList;
		}

		return null;
	}

	private List<AppBizProductPropertyDto> convertProductPropertyDto(List<ProductPropertyDto> productPropertyDtoList) {
		if (AppUtils.isNotBlank(productPropertyDtoList)) {
			List<AppBizProductPropertyDto> appPropertyDtoList = new ArrayList<AppBizProductPropertyDto>();
			for (ProductPropertyDto productPropertyDto : productPropertyDtoList) {
				AppBizProductPropertyDto appProductPropertyDto = new AppBizProductPropertyDto();
				appProductPropertyDto.setPropId(productPropertyDto.getPropId());
				appProductPropertyDto.setPropName(productPropertyDto.getPropName());
				appProductPropertyDto.setProdPropValList(convertProductPropertyValueDto(productPropertyDto.getProductPropertyValueList()));
				appPropertyDtoList.add(appProductPropertyDto);
			}
			return appPropertyDtoList;
		}

		return null;
	}

	private List<String> convertProductPicDto(List<ProdPicDto> prodPicDtoList, List<PropValueImgDto> propValImgList) {
		List<String> productPics = new ArrayList<String>();
		if (AppUtils.isNotBlank(propValImgList)) {
			for (PropValueImgDto propValImg : propValImgList) {
				productPics.addAll(propValImg.getImgList());
			}
		} else {
			if (AppUtils.isNotBlank(prodPicDtoList)) {
				for (ProdPicDto prodPicDto : prodPicDtoList) {
					productPics.add(prodPicDto.getFilePath());
				}
			}
		}
		return productPics;
	}

	/**
	 * 转换商品属性图片
	 */
	private List<AppBizPropValueImgDto> convertPropValueImgList(List<PropValueImgDto> proValueImgs) {
		if (AppUtils.isBlank(proValueImgs)) {
			return null;
		}
		List<AppBizPropValueImgDto> appProValueImgs = new ArrayList<AppBizPropValueImgDto>();
		for (PropValueImgDto imgDto : proValueImgs) {
			AppBizPropValueImgDto appImgDto = new AppBizPropValueImgDto();
			appImgDto.setValueId(imgDto.getValueId());
			appImgDto.setValueName(imgDto.getValueName());
			appImgDto.setPropId(imgDto.getPropId());
			appImgDto.setValueImage(imgDto.getValueImage());
			appImgDto.setSeq(imgDto.getSeq());
			appImgDto.setImgList(imgDto.getImgList());
			appProValueImgs.add(appImgDto);
		}
		return appProValueImgs;
	}

	private List<AppBizProductPropertyValueDto> convertProductPropertyValueDto(List<ProductPropertyValueDto> productPropertyValueDtoList) {
		if (AppUtils.isNotBlank(productPropertyValueDtoList)) {
			List<AppBizProductPropertyValueDto> appPropertyValueDtoList = new ArrayList<AppBizProductPropertyValueDto>();
			for (ProductPropertyValueDto productPropertyValueDto : productPropertyValueDtoList) {
				AppBizProductPropertyValueDto appProductPropertyValueDto = new AppBizProductPropertyValueDto();
				appProductPropertyValueDto.setIsSelected(productPropertyValueDto.getIsSelected());
				appProductPropertyValueDto.setPropId(productPropertyValueDto.getPropId());
				appProductPropertyValueDto.setName(productPropertyValueDto.getName());
				appProductPropertyValueDto.setValueId(productPropertyValueDto.getValueId());
				appPropertyValueDtoList.add(appProductPropertyValueDto);
			}
			return appPropertyValueDtoList;
		}
		return null;
	}

	/**
	 * 获取商品评论
	 * @param params
	 * @return
	 */
	@Override
	public AppPageSupport<AppProdCommDto> queryProdComments(AppBizProductCommentsParamsDto params) {

		AppPageSupport<AppProdCommDto> aps = appBizProductDao.queryProductComments(params);
		//处理图片路径
		List<AppProdCommDto> resultList = aps.getResultList();
		if(AppUtils.isNotBlank(resultList)){
			for (AppProdCommDto appProdCommDto : resultList) {
				//处理评论图片路径
				String photos = appProdCommDto.getPhotos();
				if(AppUtils.isNotBlank(photos)){
					String[] photoArray = photos.split(",");
					List<String> photoList = Arrays.asList(photoArray);
					appProdCommDto.setPhotoList(photoList);
				}

				//处理追加评论图片路径
				String addPhotos = appProdCommDto.getAddPhotos();
				if(AppUtils.isNotBlank(addPhotos)){
					String[] AddPhotoArray = addPhotos.split(",");
					List<String> addPhotoList = Arrays.asList(AddPhotoArray);
					appProdCommDto.setAddPhotoList(addPhotoList);
				}

				//如果追评不为空，则获取多少天后追评的
				appProdCommDto.setAppendDays(appProdCommDto.getAppendDays());
			}
		}
		return aps;
	}

	/**
	 * 获取下级分类
	 * @param categoryId
	 * @return
	 */
	@Override
	public List<AppBizProdCategoryDto> nextCategory(Long categoryId) {
		List<AppBizProdCategoryDto> categoryDtoList = new ArrayList<>();
		List<Category> categoryList = categoryService.findCategory(categoryId);
		for (Category category : categoryList) {
			AppBizProdCategoryDto appBizProdCategoryDto = new AppBizProdCategoryDto();
			appBizProdCategoryDto.setId(category.getId());
			appBizProdCategoryDto.setGrade(category.getGrade());
			appBizProdCategoryDto.setIcon(category.getIcon());
			appBizProdCategoryDto.setName(category.getName());
			appBizProdCategoryDto.setParentId(category.getParentId());
			appBizProdCategoryDto.setPic(category.getPic());
			appBizProdCategoryDto.setSeq(category.getSeq());
			appBizProdCategoryDto.setType(category.getType());
			appBizProdCategoryDto.setTypeName(category.getTypeName());
			appBizProdCategoryDto.setStatus(category.getStatus());
			List<Category> nextCategoryList = categoryService.findCategory(category.getId());
			appBizProdCategoryDto.setIsNext(AppUtils.isNotBlank(nextCategoryList)?1:0);
			categoryDtoList.add(appBizProdCategoryDto);
		}
		return categoryDtoList;
	}

	@Override
	public List<AppBizProdCategoryDto> platformGradeCategory(Integer grade, Long parentId) {
		List<AppBizProdCategoryDto> categoryDtoList = new ArrayList<>();
		List<Category> categoryList = categoryService.findCategoryByGradeAndParentId(grade, parentId);
		for (Category category : categoryList) {
			AppBizProdCategoryDto appBizProdCategoryDto = new AppBizProdCategoryDto();
			appBizProdCategoryDto.setId(category.getId());
			appBizProdCategoryDto.setGrade(category.getGrade());
			appBizProdCategoryDto.setIcon(category.getIcon());
			appBizProdCategoryDto.setName(category.getName());
			appBizProdCategoryDto.setParentId(category.getParentId());
			appBizProdCategoryDto.setPic(category.getPic());
			appBizProdCategoryDto.setSeq(category.getSeq());
			appBizProdCategoryDto.setType(category.getType());
			appBizProdCategoryDto.setTypeName(category.getTypeName());
			appBizProdCategoryDto.setStatus(category.getStatus());
			List<Category> nextCategoryList = categoryService.findCategory(category.getId());
			appBizProdCategoryDto.setIsNext(AppUtils.isNotBlank(nextCategoryList)?1:0);
			categoryDtoList.add(appBizProdCategoryDto);
		}
		return categoryDtoList;
	}


	@Override
	public AppPageSupport<AppBizMarketingProdDto> getShopShippingProds(String curPageNO, Long shopId,String name,Long shopCatId ,Integer shopCatType) {
		PageSupport<FullActiveProdDto> ps =productDao.getAppFullActiveProdDto(shopId, curPageNO,name,shopCatId,shopCatType);
		if(AppUtils.isNotBlank(ps)){
			AppPageSupport<AppBizMarketingProdDto> pageSupportDto = new AppPageSupport<>();
			pageSupportDto.setTotal(ps.getTotal());
			pageSupportDto.setCurrPage(ps.getCurPageNO());
			pageSupportDto.setPageSize(ps.getPageSize());
			pageSupportDto.setPageCount(ps.getPageCount());
			if(AppUtils.isNotBlank(ps.getResultList())){
				List<AppBizMarketingProdDto> marketingProdDtos=new ArrayList<>();
				ps.getResultList().stream().filter(fullActiveProdDto -> {
					//包邮活动表的ID，如果为空，证明没有参加包邮活动
					if (AppUtils.isNotBlank(fullActiveProdDto.getActiveId())){
						return false;
					}
					return true;
				}).forEach(fullActiveProdDto -> {
					AppBizMarketingProdDto appBizMarketingProdDto=new AppBizMarketingProdDto();
					appBizMarketingProdDto.setProdId(fullActiveProdDto.getProdId());
					appBizMarketingProdDto.setProdName(fullActiveProdDto.getProdName());
					appBizMarketingProdDto.setPic(fullActiveProdDto.getPic());
					appBizMarketingProdDto.setStocks(fullActiveProdDto.getActualStocks());
					appBizMarketingProdDto.setCash(fullActiveProdDto.getPrice());
					marketingProdDtos.add(appBizMarketingProdDto);
				});
				pageSupportDto.setResultList(marketingProdDtos);

				return pageSupportDto;
			}
		}
		return null;
	}

	/**
	 * app 商家端获取秒杀活动商品列表
	 * @param curPageNO
	 * @param name
	 * @param shopId
	 * @return
	 */
	@Override
	public AppPageSupport<AppBizMarketingProdDto> getSeckillActivityPords(String curPageNO, String name, Long shopId) {

		QueryMap queryMap = new QueryMap();
		queryMap.put("shopId",shopId);
		List<Object> list=new ArrayList<>();
		if (AppUtils.isBlank(curPageNO)){
			curPageNO="1";
		}
		if(AppUtils.isNotBlank(name)){
			queryMap.put("name",name);
			list.add("%" + name.trim() + "%");
		}

//		计算分页开始数
		int offset=(Integer.valueOf(curPageNO)-1)*5;
		queryMap.put("offset", offset);
		queryMap.put("pageSize", 5);

		String sql = ConfigCode.getInstance().getCode("prod.getSeckillActivityPordDto", queryMap);
		String sqlCount = ConfigCode.getInstance().getCode("prod.getSeckillActivityPordDtoCount", queryMap);

		List<AppBizMarketingProdDto> prodDtoList=new ArrayList<>();

		Long total = productDao.getLongResult(sqlCount, list.toArray());
		List<AppBizMarketingProdSkuDto> prodSkulist= productDao.query(sql,AppBizMarketingProdSkuDto.class,list.toArray());


		if (AppUtils.isNotBlank(prodSkulist)){
			prodSkulist.stream()
					.collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getProdId()))), ArrayList::new))
					.forEach(appBizMarketingProdSkuDto -> {
						Long prodId = appBizMarketingProdSkuDto.getProdId();
						Product prod = productDao.getProd(prodId, shopId);
						AppBizMarketingProdDto appBizMarketingProdDto=new AppBizMarketingProdDto();
						appBizMarketingProdDto.setProdId(prod.getProdId());
						appBizMarketingProdDto.setProdName(prod.getName());
						appBizMarketingProdDto.setPic(prod.getPic());
						appBizMarketingProdDto.setCash(prod.getCash());
						appBizMarketingProdDto.setStocks(prod.getStocks());
						List<AppBizMarketingProdSkuDto> collect = prodSkulist.stream().filter(appBizMarketingProdSkuDto1 -> appBizMarketingProdSkuDto1.getProdId().equals(prodId)).collect(Collectors.toList());
						appBizMarketingProdDto.setMarketingProdSkuDtos(collect);
						prodDtoList.add(appBizMarketingProdDto);

					});

		}
		DefaultPagerProvider pageProvider = new DefaultPagerProvider(total, curPageNO, 5);
		AppPageSupport<AppBizMarketingProdDto> ps = new AppPageSupport<>(prodDtoList, pageProvider);
		return ps;
	}
	public static void main(String[] args) {
		List<AppBizMarketingProdDto> infoList = new ArrayList<>();
		AppBizMarketingProdDto appBizMarketingProdDto = new AppBizMarketingProdDto();
		appBizMarketingProdDto.setProdId(1L);
		infoList.add(appBizMarketingProdDto);
		infoList.add(appBizMarketingProdDto);
		infoList.add(appBizMarketingProdDto);
		infoList.add(appBizMarketingProdDto);
		ArrayList<AppBizMarketingProdDto> collect = infoList.stream()
				.collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(f -> f.getProdId()))), ArrayList::new));
	}

}

