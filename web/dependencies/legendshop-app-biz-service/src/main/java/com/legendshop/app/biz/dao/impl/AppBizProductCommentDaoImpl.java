package com.legendshop.app.biz.dao.impl;

import com.legendshop.app.biz.dao.AppBizProductCommentDao;
import com.legendshop.biz.model.dto.comment.AppBizProductCommentDto;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.model.entity.Product;
import com.legendshop.util.AppUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

/**
 * 商家端商品评论DAO实现
 * @author linzh
 */

@Repository
public class AppBizProductCommentDaoImpl extends GenericDaoImpl<Product, Long> implements AppBizProductCommentDao {


	/**
	 * 获取商品评论列表
	 * @param shopId 商家ID
	 * @param curPageNO 当前页码
	 * @param prodName 商品名称
	 * @param orders 排序
	 * @return
	 */
	@Override
	public PageSupport<AppBizProductCommentDto> queryProductCommentList(Long shopId, String curPageNO, String prodName, String orders) {

		Integer pageSize = 10;
		SimpleSqlQuery query = new SimpleSqlQuery(AppBizProductCommentDto.class, pageSize, curPageNO);
		QueryMap map = new QueryMap();

		//搜索条件
		map.put("shopId", shopId);
		map.like("prodName", prodName);
		if (AppUtils.isBlank(orders)){

			map.put("order", "c.addtime " + "desc");
		}else{

			//处理排序
			String[] order = StringUtils.split(orders, ",");
			map.put("order",order[0]+" "+ order[1]);
		}

		String querySQL = ConfigCode.getInstance().getCode("app.biz.queryProductCommentList", map);
		String queryCountSQL = ConfigCode.getInstance().getCode("app.biz.queryProductCommentListCount", map);
		query.setQueryString(querySQL);
		query.setAllCountString(queryCountSQL);
		map.remove("order");
		query.setParam(map.toArray());

		return querySimplePage(query);
	}
}
