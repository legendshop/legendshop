package com.legendshop.app.biz.controller;


import com.legendshop.app.biz.service.AppBizProductConsultService;
import com.legendshop.biz.model.dto.comment.AppBizProductCommentDto;
import com.legendshop.biz.model.dto.productConsult.AppBizProductConsultDto;
import com.legendshop.biz.model.dto.productConsult.AppBizProductConsultListDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.ProductConsult;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.ProductConsultService;
import com.legendshop.spi.service.SensitiveWordService;
import com.legendshop.util.AppUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

/**
 * 商品咨询管理相关接口
 * @author linzh
 */
@Api(tags="商品咨询管理",value="商品咨询管理相关接口")
@RestController
public class AppBizConsultController {

	private final Logger LOGGER = LoggerFactory.getLogger(AppBizConsultController.class);

	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private AppBizProductConsultService appBizProductConsultService;

	@Autowired
	private SensitiveWordService sensitiveWordService;

	@Autowired
	private ProductConsultService productConsultService;



	/**
	 * 商品咨询列表
	 */
	@ApiOperation(value = "获取商品咨询列表", httpMethod = "POST", notes = "获取商品咨询列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = false, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "keywords", value = "搜索关键字", required = false, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "pointType", value = "咨询类型,1: 商品咨询, 2:库存配送, 3:售后咨询", required = false, dataType = "int")
	})
	@PostMapping(value="/s/productConsultList")
	public ResultDto<AppPageSupport<AppBizProductConsultListDto>> query(String curPageNO, String keywords, Integer pointType) {


		try {

			LoginedUserInfo user = loginedUserService.getUser();
			if (AppUtils.isBlank(user)){

				return ResultDtoManager.fail(-1, "请先登录");
			}
			AppPageSupport<AppBizProductConsultListDto> ps = appBizProductConsultService.queryProductConsultList(user.getShopId(),curPageNO,keywords,pointType);

			return ResultDtoManager.success(ps);
		} catch (Exception e) {
			LOGGER.error("获取咨询列表异常：", e.getMessage());
			return ResultDtoManager.fail();
		}
	}



	/**
	 * 商品咨询详情
	 */
	@ApiOperation(value = "获取商品咨询详情", httpMethod = "POST", notes = "获取商品咨询详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "consId", value = "商品咨询Id", required = true, dataType = "Long"),
	})
	@PostMapping(value="/productConsult")
	public ResultDto<AppBizProductConsultDto> getProductConsultDetail(Long consId) {

		try {

			AppBizProductConsultDto consult = appBizProductConsultService.getProductConsultDetail(consId);
			return ResultDtoManager.success(consult);
		} catch (Exception e) {
			LOGGER.error("获取商品咨询详情异常：", e.getMessage());
			return ResultDtoManager.fail();
		}
	}



	/**
	 * 回复商品咨询
	 */
	@ApiOperation(value = "回复商品咨询", httpMethod = "POST", notes = "复商品咨询",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "consId", value = "商品咨询Id", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="query", name = "content", value = "回复内容 非空", required = true, dataType = "Long")
	})
	@PostMapping(value="/s/replyProductConsult")
	public ResultDto<Object> replyProductConsult(Long consId,String content) {

		try {

			LoginedUserInfo user = loginedUserService.getUser();
			if (AppUtils.isBlank(user)){

				return ResultDtoManager.fail(-1, "请先登录");
			}

			if (AppUtils.isBlank(content)){

				return ResultDtoManager.fail(-1,"回复内容不能为空！");
			}

			// 敏感字过滤
			Set<String> findwords = sensitiveWordService.checkSensitiveWords(content);
			if(AppUtils.isNotBlank(findwords)){
				return ResultDtoManager.fail(-1,"亲, 您回复的内容含有敏感词 " + findwords + ", 请更正再提交！");
			}

			ProductConsult productConsult = productConsultService.getProductConsult(consId);
			if (AppUtils.isBlank(productConsult)){
				return ResultDtoManager.fail(-1,"您回复的咨询不存在或已被删除！");
			}

			// 回复人用户名
			productConsult.setAnsUserName(user.getUserName());
			// 回复内容
			productConsult.setAnswer(content);
			appBizProductConsultService.replyProductConsult(productConsult);

			return ResultDtoManager.success();
		} catch (Exception e) {
			LOGGER.error("回复商品咨询异常：", e.getMessage());
			return ResultDtoManager.fail();
		}
	}

}
