package com.legendshop.app.biz.service;

import com.legendshop.biz.model.dto.comment.AppBizProductCommentDto;
import com.legendshop.model.dto.ProductCommentDto;
import com.legendshop.model.dto.app.AppPageSupport;

/**
 * 商家端商品评论服务
 * @author linzh
 */
public interface AppBizProductCommentService {

	/**
	 * 获取商品评论列表
	 * @param shopId 商家ID
	 * @param curPageNO 当前页码
	 * @param prodName 商品名称
	 * @param orders 排序
	 * @return
	 */
	AppPageSupport<AppBizProductCommentDto> queryProductCommentList(Long shopId, String curPageNO, String prodName, String orders);

	/**
	 * 获取商品评论详情
	 * @param prodComId
	 * @return
	 */
	ProductCommentDto getProductCommentDetail(Long prodComId);
}
