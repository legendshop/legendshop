package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.service.AppBizStoreService;
import com.legendshop.biz.model.dto.order.AppBizStoreDto;
import com.legendshop.model.entity.store.Store;
import com.legendshop.spi.service.StoreService;
import com.legendshop.util.MethodUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;

@Service("appBizStoreService")
public class AppBizStoreServiceImpl implements AppBizStoreService {

	@Autowired
	private StoreService storeService;

	@Override
	public AppBizStoreDto getStore(Long storeId) {
		Store store = storeService.getStore(storeId);

		return convertToAppBizStoreDto(store);
	}

	public AppBizStoreDto convertToAppBizStoreDto(Store store){
		AppBizStoreDto appBizStoreDto = new AppBizStoreDto();
		appBizStoreDto.setArea(store.getArea());
		appBizStoreDto.setAddress(store.getAddress());
		appBizStoreDto.setLng(store.getLng());
		appBizStoreDto.setProvince(store.getProvince());
		appBizStoreDto.setCity(store.getCity());
		appBizStoreDto.setName(store.getName());
		appBizStoreDto.setBusinessHours(store.getBusinessHours());
		appBizStoreDto.setLat(store.getLat());
		return appBizStoreDto;
	}
}
