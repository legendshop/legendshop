package com.legendshop.app.biz.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用于生成商品海报数据的DTO
 */
@ApiModel(value = "用于生成商品海报数据")
public class AppBizProdPosterDataDto {
	
	/**
	 * 用户头像
	 */
	@ApiModelProperty("用户头像url")
	private String headPortrait;
	
	/**
	 * 用户昵称
	 */
	@ApiModelProperty("用户昵称")
	private String nickName;
	
	/**
	 * 小程序码
	 */
	@ApiModelProperty("小程序码url")
	private String shopWxCode;
	
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getHeadPortrait() {
		return headPortrait;
	}

	public void setHeadPortrait(String headPortrait) {
		this.headPortrait = headPortrait;
	}

	public String getShopWxCode() {
		return shopWxCode;
	}

	public void setShopWxCode(String shopWxCode) {
		this.shopWxCode = shopWxCode;
	}
}
