/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.biz.util;

import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.entity.AppToken;
import com.legendshop.util.AppUtils;
import com.legendshop.util.MD5Util;

import java.util.Date;

/**
 * 校验参数和Token等
 * @author Joe Lin
 */
public class AppTokenUtil {
	/**
	 *  验证签名和AppToken的合法性
	 * 
	 */
	public static ResultDto<Object> checkAppBizToken(AppToken appToken, String userId, String verId, String sign, String accessToken, String securiyCode,String shopId) {

		ResultDto<Object> result = new ResultDto<Object>();
		result.setVerId(verId);
		if (AppUtils.isBlank(appToken) || AppUtils.isBlank(userId) || AppUtils.isBlank(accessToken) || AppUtils.isBlank(securiyCode) || AppUtils.isBlank(verId) || AppUtils.isBlank(shopId)) {
			result.setStatus(401);
			result.setMsg("您还未登入 请先登入!");
			return result;
		}

		StringBuffer content = new StringBuffer().append(userId).append(accessToken).append(verId).append(securiyCode);
		String _sign = MD5Util.toMD5(content.toString());

		if (!_sign.equals(sign)) {
			result.setStatus(401);
			result.setMsg("Invalid signature!");
			return result;
		}

		if (!appToken.getAccessToken().equals(accessToken) || !appToken.getSecuriyCode().equals(securiyCode)) {
			result.setStatus(401);
			result.setMsg("User authentication failed, please log in again!");
			return result;
		}

		Date curDate = new Date();
		Date startDate = appToken.getStartDate();

		// 获取token从开始到现在相隔的时间, 单位秒
		long diffTime = (curDate.getTime() - startDate.getTime()) / 1000;

		// 失效时间是两周
		if (diffTime > appToken.getValidateTime()) {
			result.setStatus(401);
			result.setMsg("User login information has expired, please log in again!");
			return result;
		}

		result.setStatus(1);
		result.setMsg("Verification success!");

		return result;
	}
}
