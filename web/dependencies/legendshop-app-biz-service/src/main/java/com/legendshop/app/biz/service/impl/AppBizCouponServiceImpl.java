package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.service.AppBizCouponService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.biz.model.dto.AppBizCouponDto;
import com.legendshop.biz.model.dto.AppBizMarketingProdDto;
import com.legendshop.biz.model.dto.AppBizShowCouponDto;
import com.legendshop.biz.model.dto.AppBizUserCouponDto;
import com.legendshop.business.dao.CouponDao;
import com.legendshop.business.dao.UserCouponDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.CouponProviderEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.CouponProd;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.spi.service.CouponProdService;
import com.legendshop.spi.service.CouponService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author 27158
 */
@Service("appBizCouponService")
public class AppBizCouponServiceImpl implements AppBizCouponService {
	private final static String QUERY_COUPON ="SELECT co.*,sd.site_name AS shopName FROM ls_coupon AS co,ls_shop_detail AS sd  WHERE co.shop_id=sd.shop_id AND co.shop_del=0 AND co.coupon_provider=? AND sd.shop_id=? ";
	private final static String QUERY_COUPON_Count="SELECT COUNT(*) FROM ls_coupon AS co,ls_shop_detail AS sd WHERE co.shop_id=sd.shop_id AND co.shop_del=0 AND co.coupon_provider=?  AND sd.shop_id=?";
	@Autowired
	private CouponService couponService;

	@Autowired
	private UserCouponDao userCouponDao;
	@Autowired
	private CouponDao couponDao;
	@Autowired
	private CouponProdService couponProdService;
	@Autowired
	private ProductService productService;
	@Override
	public AppPageSupport<AppBizShowCouponDto> queryCoupon(String curPageNO, Integer status,Long shopId,String couponName) {
//		查询sql 语句
		StringBuffer sql = new StringBuffer();
		sql.append(QUERY_COUPON);
//		优惠卷总数sql 语句
		StringBuffer sqlCount = new StringBuffer();
		sqlCount.append(QUERY_COUPON_Count);
//		条件参数
		List<Object> objects = new ArrayList<>();
		//		礼券提供方
		objects.add(CouponProviderEnum.SHOP.value());
		objects.add(shopId);
		if(AppUtils.isNotBlank(status)){
			if (status ==-1) {
//			过期
				Date date = new Date();
				sql.append(" AND end_Date < ? ");
				sqlCount.append(" AND end_Date < ?");
				objects.add(date);
			} else if (status == 1) {
//			有效
				Date date = new Date();
				sql.append(" AND end_Date > ?");
				sqlCount.append(" AND end_Date > ?");
				sql.append(" AND co.status=?");
				sqlCount.append(" AND co.status=?");
				objects.add(date);
				objects.add(status);
			} else if(status ==0) {
//			失效
				Date date = new Date();
				sql.append(" AND co.status = ? ");
				sqlCount.append(" AND co.status = ? ");
				sql.append(" AND end_Date > ? ");
				sqlCount.append(" AND end_Date > ?");
				objects.add(status);
				objects.add(date);
			}
		}
		if (AppUtils.isNotBlank(couponName)) {
			sql.append(" AND coupon_name like ?");
			sqlCount.append(" AND coupon_name like ?");
			objects.add("%" + couponName + "%");
		}
		sql.append(" order by co.create_date desc");
		PageSupport<Coupon> ps = couponService.queryCoupon(sql, curPageNO, sqlCount, objects);
		return convertPageSupportDto(ps);
	}

	private AppPageSupport<AppBizShowCouponDto> convertPageSupportDto(PageSupport<Coupon>  pageSupport){
		if(AppUtils.isNotBlank(pageSupport)){
			AppPageSupport<AppBizShowCouponDto> pageSupportDto = new AppPageSupport<>();
			pageSupportDto.setTotal(pageSupport.getTotal());
			pageSupportDto.setCurrPage(pageSupport.getCurPageNO());
			pageSupportDto.setPageSize(pageSupport.getPageSize());
			List<AppBizShowCouponDto> appBizCouponDtos = convertAppBizCouponList(pageSupport.getResultList());
			pageSupportDto.setResultList(appBizCouponDtos);
			pageSupportDto.setPageCount(pageSupport.getPageCount());
			return pageSupportDto;
		}
		return null;
	}
	private List<AppBizShowCouponDto> convertAppBizCouponList(List<Coupon> CouponList){
		if(AppUtils.isNotBlank(CouponList)){
			List<AppBizShowCouponDto> dtoList = new ArrayList<>();
			CouponList.stream().forEach(coupon -> {
				AppBizShowCouponDto appBizShowCouponDto=new AppBizShowCouponDto();
				appBizShowCouponDto.setCouponId(coupon.getCouponId());
				appBizShowCouponDto.setCouponName(coupon.getCouponName());
				if (new Date().after(coupon.getEndDate())) {
					appBizShowCouponDto.setStatus(-1);
				} else {
					appBizShowCouponDto.setStatus(coupon.getStatus());
				}
				appBizShowCouponDto.setFullPrice(coupon.getFullPrice());
				appBizShowCouponDto.setOffPrice(coupon.getOffPrice());
				appBizShowCouponDto.setStartDate(coupon.getStartDate());
				appBizShowCouponDto.setEndDate(coupon.getEndDate());
				appBizShowCouponDto.setCouponProvider(coupon.getCouponProvider());
				appBizShowCouponDto.setGetType(coupon.getGetType());
				appBizShowCouponDto.setCouponPic(coupon.getCouponPic());
				appBizShowCouponDto.setCouponType(coupon.getCouponType());
				appBizShowCouponDto.setCouponNumber(coupon.getCouponNumber());
				appBizShowCouponDto.setBindCouponNumber(coupon.getBindCouponNumber());
				appBizShowCouponDto.setUseCouponNumber(coupon.getUseCouponNumber());
				appBizShowCouponDto.setShopName(coupon.getShopName());
//				领取人数
				int userCouponCount = userCouponDao.getUserCouponCount(coupon.getCouponId());
				appBizShowCouponDto.setDrawUserNum(userCouponCount);

				dtoList.add(appBizShowCouponDto);
			});

			return dtoList;
		}
		return null;
	}

	@Override
	public int saveByCouponAndShopId(AppBizCouponDto appBizCouponDto,String userName, Long shopId,String userId) {
//		封装Coupon 实体类
		Coupon coupon=getCoupon(appBizCouponDto,shopId);
		try {
			couponService.saveByCouponAndShopId(coupon, userName,userId, shopId, appBizCouponDto.getProdidList());
		} catch (Exception e){
			return -1;
		}
		return 1;
	}

	/**
	 * 封装Coupon 实体类
	 * @param appBizCouponDto
	 * @return
	 */
	private Coupon getCoupon(AppBizCouponDto appBizCouponDto,Long shopId) {
		Coupon coupon=new Coupon();
		coupon.setCouponNumber(appBizCouponDto.getCouponNumber());
		coupon.setCouponName(appBizCouponDto.getCouponName());
		coupon.setStatus(1);
		coupon.setFullPrice(appBizCouponDto.getFullPrice());
		coupon.setOffPrice(appBizCouponDto.getOffPrice());
		coupon.setStartDate(appBizCouponDto.getStartDate());
		coupon.setEndDate(appBizCouponDto.getEndDate());
		coupon.setCouponProvider(CouponProviderEnum.SHOP.value());
		coupon.setGetType(appBizCouponDto.getGetType());
		coupon.setCouponPic(appBizCouponDto.getCouponPic());
		coupon.setGetLimit(appBizCouponDto.getGetLimit());
		coupon.setCreateDate(new Date());
		coupon.setDescription(appBizCouponDto.getDescription());
		coupon.setProdidList(appBizCouponDto.getProdidList());
		coupon.setIsDsignatedUser(appBizCouponDto.getIsDsignatedUser());
		coupon.setUserIdLists(appBizCouponDto.getUserIdLists());
		coupon.setCouponType(appBizCouponDto.getCouponType());
		coupon.setNeedPoints(appBizCouponDto.getNeedPoints());
		coupon.setShopId(shopId);
		return coupon;
	}

	@Override
	public AppBizShowCouponDto getCoupon(Long couponId) {
		Coupon coupon = couponDao.getCoupon(couponId);
		if(AppUtils.isBlank(coupon)){
			return null;
		}
		AppBizShowCouponDto appBizShowCouponDto=new AppBizShowCouponDto();
		appBizShowCouponDto.setCouponId(couponId);
		appBizShowCouponDto.setCouponName(coupon.getCouponName());
		appBizShowCouponDto.setStatus(coupon.getStatus());
		appBizShowCouponDto.setFullPrice(coupon.getFullPrice());
		appBizShowCouponDto.setOffPrice(coupon.getOffPrice());
		appBizShowCouponDto.setStartDate(coupon.getStartDate());
		appBizShowCouponDto.setEndDate(coupon.getEndDate());
		appBizShowCouponDto.setGetType(coupon.getGetType());
		appBizShowCouponDto.setCouponPic(coupon.getCouponPic());
		String couponType = coupon.getCouponType();
		if("product".equals(couponType)){
// 如果是商品券,取出该商品券中所有的商品名称
			List<CouponProd> couponProd = couponProdService.getCouponProdByCouponId(couponId);
			if (AppUtils.isNotBlank(couponProd)){
				List<AppBizMarketingProdDto> list=new ArrayList<>();
				couponProd.stream().forEach(couponProd1 -> {
					Product productById = productService.getProductById(couponProd1.getProdId());
					AppBizMarketingProdDto appBizMarketingProdDto=new AppBizMarketingProdDto();
					appBizMarketingProdDto.setProdId(productById.getProdId());
					appBizMarketingProdDto.setProdName(productById.getName());
					appBizMarketingProdDto.setPic(productById.getPic());
					appBizMarketingProdDto.setStocks(productById.getStocks());
					appBizMarketingProdDto.setCash(productById.getCash());
					list.add(appBizMarketingProdDto);
				});
//				添加关联商品
				appBizShowCouponDto.setMarketingProd(list);
			}
		}
		appBizShowCouponDto.setCouponType(couponType);
		appBizShowCouponDto.setGetLimit(coupon.getGetLimit());
		appBizShowCouponDto.setCouponNumber(coupon.getCouponNumber());
		appBizShowCouponDto.setDescription(coupon.getDescription());
		appBizShowCouponDto.setIsDsignatedUser(coupon.getIsDsignatedUser());
		return appBizShowCouponDto;
	}

	@Override
	public void shopDeleteCoupon(Long couponId) {
		 couponDao.shopDeleteCoupon(couponId);
	}

	@Override
	public AppPageSupport<AppBizUserCouponDto> queryUserCouponList(String curPageNO,Integer pageSize, Long couponId) {

		PageSupport<UserCoupon> result = userCouponDao.getUserCouponListPage(curPageNO,pageSize, couponId);
		return PageUtils.convertPageSupport(result, new PageUtils.ResultListConvertor<UserCoupon, AppBizUserCouponDto>() {
			@Override
			public List<AppBizUserCouponDto> convert(List<UserCoupon> form) {

				List<AppBizUserCouponDto> toList = new ArrayList<>();
				for (UserCoupon userCoupon : form) {

					AppBizUserCouponDto appBizUserCouponDto = convert2AppBizUserCouponDto(userCoupon);
					toList.add(appBizUserCouponDto);
				}
				return toList;
			}
		});
	}

	@Override
	public AppBizUserCouponDto getUserCouponById(Long userCouponId) {

		UserCoupon userCoupon = userCouponDao.getUserCoupon(userCouponId);
		return convert2AppBizUserCouponDto(userCoupon);
	}

	private AppBizUserCouponDto convert2AppBizUserCouponDto(UserCoupon userCoupon) {

		AppBizUserCouponDto appBizUserCouponDto = new AppBizUserCouponDto();
		appBizUserCouponDto.setUserCouponId(userCoupon.getUserCouponId());
		appBizUserCouponDto.setCouponName(userCoupon.getCouponName());
		appBizUserCouponDto.setUserName(userCoupon.getUserName());
		appBizUserCouponDto.setCouponSn(userCoupon.getCouponSn());
		appBizUserCouponDto.setCouponPwd(userCoupon.getCouponPwd());
		appBizUserCouponDto.setGetTime(userCoupon.getGetTime());
		appBizUserCouponDto.setUseTime(userCoupon.getUseTime());
		appBizUserCouponDto.setUseStatus(userCoupon.getUseStatus());

		return appBizUserCouponDto;
	}


}
