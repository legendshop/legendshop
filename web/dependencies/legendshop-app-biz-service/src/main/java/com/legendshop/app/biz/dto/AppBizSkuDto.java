/**
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.biz.dto;

import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.dto.buy.CartMarketRules;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/** 商品sku DTO **/
@ApiModel("商品规格")
public class AppBizSkuDto implements Serializable  {

	private static final long serialVersionUID = 6830009574780972733L;
	
	/** sku id **/
	@ApiModelProperty(value = "sku id")
	private Long skuId ; 
		
	/** sku的销售属性组合字符串（颜色，大小，等等）,格式是p1:v1;p2:v2 **/
	@ApiModelProperty(value = "sku的销售属性组合字符串（颜色，大小，等等）,格式是p1:v1;p2:v2")
	private String properties ; 
	
	/** 价格 **/
	@ApiModelProperty(value = "价格 ")
	private Double price ;     
	
	/** 现价 */
	@ApiModelProperty(value = "现价")
	private Double cash;
	
	/**拼团价格*/
	@ApiModelProperty(value="拼团价格, 拼团活动时取这个价格")
	private Double mergePrice;
	
	/** 促销价格 如果没有促销 则等于现价 */
	@ApiModelProperty(value = "促销价格 如果没有促销 则等于现价")
	private Double promotionPrice; 
	  
    /** 营销规则 */
	@ApiModelProperty(value = "营销规则列表")
    private List<CartMarketRules> ruleList;
    
	/** sku名称 */
	@ApiModelProperty(value = "sku名称")
	private String name ; 
	
	/** SKU标题 */
	@ApiModelProperty(value = "SKU标题列表")
	private List<KeyValueEntity> propertiesNameList;
	
	/** sku状态。 1:正常     0:删除 **/
	@ApiModelProperty(value = "sku状态。 1:正常     0:删除 ")
	private Integer status ;
  
    /** sku库存 */
	@ApiModelProperty(value = "sku库存")
	private Long stocks ; 
	
	/** 商家编码 */
	@ApiModelProperty(value = "商家编码")
	private String partyCode;
		
	/** 商品条形码 */
	@ApiModelProperty(value = "商品条形码 ")
	private String modelId;
	
	/** 对应属性值ID */
	@ApiModelProperty(value = "对应属性值ID")
	private String propValueIds;

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}
	
	public Double getMergePrice() {
		return mergePrice;
	}

	public void setMergePrice(Double mergePrice) {
		this.mergePrice = mergePrice;
	}

	public Double getPromotionPrice() {
		return promotionPrice;
	}

	public void setPromotionPrice(Double promotionPrice) {
		this.promotionPrice = promotionPrice;
	}

	public List<CartMarketRules> getRuleList() {
		return ruleList;
	}

	public void setRuleList(List<CartMarketRules> ruleList) {
		this.ruleList = ruleList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<KeyValueEntity> getPropertiesNameList() {
		return propertiesNameList;
	}

	public void setPropertiesNameList(List<KeyValueEntity> propertiesNameList) {
		this.propertiesNameList = propertiesNameList;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getStocks() {
		return stocks;
	}

	public void setStocks(Long stocks) {
		this.stocks = stocks;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getPropValueIds() {
		return propValueIds;
	}

	public void setPropValueIds(String propValueIds) {
		this.propValueIds = propValueIds;
	}
}
