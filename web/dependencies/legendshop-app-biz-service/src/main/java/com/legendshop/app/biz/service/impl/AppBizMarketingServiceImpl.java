package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.service.AppBizMarketingService;
import com.legendshop.biz.model.dto.AppBizMarketingDto;
import com.legendshop.biz.model.dto.AppBizMarketingProdSkuDto;
import com.legendshop.biz.model.dto.AppBizMarketingRuleDto;
import com.legendshop.biz.model.dto.AppBizShowMarketingDto;
import com.legendshop.business.dao.MarketingDao;
import com.legendshop.business.dao.MarketingRuleDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.MarketingTypeEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.marketing.MarketingProdDto;
import com.legendshop.model.entity.Marketing;
import com.legendshop.model.entity.Sku;
import com.legendshop.spi.service.MarketingProdsService;
import com.legendshop.spi.service.MarketingService;
import com.legendshop.spi.service.SkuService;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author 27158
 */

 @Service("appBizMarketingService")
public class AppBizMarketingServiceImpl implements AppBizMarketingService {
	@Autowired
	private MarketingDao marketingDao;
	@Autowired
	private MarketingProdsService marketingProdsService;
	@Autowired
	private MarketingRuleDao marketingRuleDao;
	@Autowired
	private MarketingService marketingService;
	@Autowired
	private SkuService skuService;
	/**
	 * 获取活动规则sql
	 */
	private final static String QUERY_MJ_MARKETING_RULE ="select full_price as fullPrice,off_discount as offDiscount,off_amount as offAmount,cal_type as calType from ls_marketing_rule where market_id=? and shop_id=?  order by full_price asc,off_amount asc ";
	@Override
	public AppPageSupport<AppBizShowMarketingDto> getMjMarketingPage(String curPageNO, String searchType, Long shopId,String marketName) {
		Marketing marketing = new Marketing();
		marketing.setSearchType(searchType);
		marketing.setShopId(shopId);
		marketing.setMarketName(marketName);
		PageSupport<Marketing> ps =marketingDao.getMarketingPage(curPageNO,marketing);
		return convertPageSupportDto(ps);
	}

	@Override
	public AppPageSupport<AppBizShowMarketingDto> getZkMarketingPage(String curPageNO, String searchType, Long shopId,String marketName) {
		Marketing marketing = new Marketing();
		marketing.setSearchType(searchType);
		marketing.setMarketName(marketName);
		PageSupport<Marketing> ps = marketingDao.getMarketingPage(curPageNO, shopId, marketing);
		return convertPageSupportDto(ps);
	}

	private AppPageSupport<AppBizShowMarketingDto> convertPageSupportDto(PageSupport<Marketing>  pageSupport){
		if(AppUtils.isNotBlank(pageSupport)){
			AppPageSupport<AppBizShowMarketingDto> pageSupportDto = new AppPageSupport<>();
			pageSupportDto.setTotal(pageSupport.getTotal());
			pageSupportDto.setCurrPage(pageSupport.getCurPageNO());
			pageSupportDto.setPageSize(pageSupport.getPageSize());
			List<AppBizShowMarketingDto> shopMarketingDtoList= convertAddressDtoList(pageSupport.getResultList());
			pageSupportDto.setResultList(shopMarketingDtoList);
			pageSupportDto.setPageCount(pageSupport.getPageCount());
			return pageSupportDto;
		}
		return null;
	}

	private List<AppBizShowMarketingDto> convertAddressDtoList(List<Marketing> marketingList){
		if(AppUtils.isNotBlank(marketingList)){
			List<AppBizShowMarketingDto> dtoList = new ArrayList<>();
			marketingList.stream().forEach(marketing -> {
				AppBizShowMarketingDto appBizShowMarketingDto=new AppBizShowMarketingDto();
				appBizShowMarketingDto.setId(marketing.getId());
				appBizShowMarketingDto.setMarketName(marketing.getMarketName());
				appBizShowMarketingDto.setStartTime(marketing.getStartTime());
				appBizShowMarketingDto.setEndTime(marketing.getEndTime());
				appBizShowMarketingDto.setShopId(marketing.getShopId());
				appBizShowMarketingDto.setType(marketing.getType());
				appBizShowMarketingDto.setIsAllProds(marketing.getIsAllProds());
				appBizShowMarketingDto.setState(marketing.getState());
				appBizShowMarketingDto.setDiscount(marketing.getDiscount());
//				获取活动规则
				List<AppBizMarketingRuleDto> appBizMarketingRuleDtos = marketingDao.query(QUERY_MJ_MARKETING_RULE, AppBizMarketingRuleDto.class, marketing.getId(), marketing.getShopId());
				appBizShowMarketingDto.setMarketingRuleDto(appBizMarketingRuleDtos);
				dtoList.add(appBizShowMarketingDto);
			});
			return dtoList;
		}
		return null;
	}

	@Override
	public AppBizShowMarketingDto getMarketingByIdAndShopId(Long id, Long shopId) {
		Marketing marketing = marketingDao.getMarketing(shopId, id);
		if(AppUtils.isNotBlank(marketing)){
			AppBizShowMarketingDto appBizShowMarketingDto=new AppBizShowMarketingDto();
			appBizShowMarketingDto.setId(marketing.getId());
			appBizShowMarketingDto.setMarketName(marketing.getMarketName());
			appBizShowMarketingDto.setStartTime(marketing.getStartTime());
			appBizShowMarketingDto.setEndTime(marketing.getEndTime());
			appBizShowMarketingDto.setShopId(marketing.getShopId());
			appBizShowMarketingDto.setType(marketing.getType());
			appBizShowMarketingDto.setIsAllProds(marketing.getIsAllProds());
			appBizShowMarketingDto.setState(marketing.getState());
			appBizShowMarketingDto.setRemark(marketing.getRemark());
//			获取活动规则 折扣类型 0:满减 1:满折 2:限时折扣 3.直降',
			if (MarketingTypeEnum.XIANSHI_ZE.value().equals(marketing.getType())){
//				限时折扣
				appBizShowMarketingDto.setDiscount(marketing.getDiscount());
			}else if(MarketingTypeEnum.MAN_JIAN.value().equals(marketing.getType()) || MarketingTypeEnum.MAN_ZE.value().equals(marketing.getType())){
//				0:满减 1:满折
				List<AppBizMarketingRuleDto> query = marketingDao.query(QUERY_MJ_MARKETING_RULE, AppBizMarketingRuleDto.class, marketing.getId(), marketing.getShopId());
				appBizShowMarketingDto.setMarketingRuleDto(query);
			}
			if (marketing.getIsAllProds().equals(0)) {
				//部分商品
				List<AppBizMarketingProdSkuDto> list=new ArrayList();
				List<MarketingProdDto> marketingProdsDtoByMarketId = marketingProdsService.getMarketingProdsDtoByMarketId(marketing.getId());
//				拷贝sku商品信息
				marketingProdsDtoByMarketId.stream().forEach(marketingProdDto -> {
					AppBizMarketingProdSkuDto  appBizMarketingProdSkuDto=new AppBizMarketingProdSkuDto();
					appBizMarketingProdSkuDto.setSkuId(marketingProdDto.getSkuId());
					appBizMarketingProdSkuDto.setProdId(marketingProdDto.getProdId());
					appBizMarketingProdSkuDto.setName(marketingProdDto.getProdName());
					appBizMarketingProdSkuDto.setPic(marketingProdDto.getPic());
					appBizMarketingProdSkuDto.setCnProperties(marketingProdDto.getCnProperties());
					appBizMarketingProdSkuDto.setPrice(marketingProdDto.getCash());
					appBizMarketingProdSkuDto.setStocks(marketingProdDto.getStock());
					list.add(appBizMarketingProdSkuDto);
				});
				appBizShowMarketingDto.setMarketingProdSkus(list);
			}
			return appBizShowMarketingDto;
		}
		return null;
	}

	@Override
	public int saveShopMarketing(AppBizMarketingDto appBizMarketingDto) {
//		封装Marketing实体类
		Marketing marketing = getMarketing(appBizMarketingDto);
//		添加促销活动
		String s = marketingService.saveMarketing(marketing);
		String s1=null;
		if(AppUtils.isNotBlank(marketing.getSkuIds()) && marketing.getIsAllProds().equals(0)){
			for (Long skuId : marketing.getSkuIds()){
//			获取sku 商品列表
				Sku sku = skuService.getSku(skuId);
				//		添加参数活动商品
				s1=marketingProdsService.addMarketingRuleProds(marketing.getId(), marketing.getType(), marketing.getShopId(), sku.getProdId(), sku.getSkuId(), sku.getPrice());
			}
		}else if (marketing.getIsAllProds().equals(1)){
			s1=Constants.SUCCESS;
		}
		if(Constants.SUCCESS.equals(s) && Constants.SUCCESS .equals(s1)){
			return 1;
		}
		return -1;
	}

	@Override
	public int updateMarketing(AppBizMarketingDto appBizMarketingDto) {
		Marketing marketing = marketingDao.getMarketing(appBizMarketingDto.getId());
		if (AppUtils.isBlank(marketing)){
			return -1;
		}
		Date date = new Date();
		if(marketing.getState().equals(1) && marketing.getStartTime().getTime() <= date.getTime()){
			//进行中不能修改
			return -1;
		}
		if(marketing.getState().equals(3)){
//			终止不能修改
			return -1;
		}
		marketing.setMarketName(appBizMarketingDto.getMarketName());
		marketing.setStartTime(appBizMarketingDto.getStartTime());
		marketing.setEndTime(appBizMarketingDto.getEndTime());
		marketing.setIsAllProds(appBizMarketingDto.getIsAllProds());
		marketing.setType(appBizMarketingDto.getType());
		marketing.setSkuIds(appBizMarketingDto.getSkuIds());
		marketing.setManze_rule(appBizMarketingDto.getManzeRule());
		marketing.setManjian_rule(appBizMarketingDto.getManjianRule());
		marketing.setDiscount(appBizMarketingDto.getDiscount());
		marketing.setRemark(appBizMarketingDto.getRemark());

//		修改
		marketingService.updateMarketing(marketing);
//		修改活动规则,删除原的
		marketingRuleDao.deleteMarketingRuleByMarketing(marketing.getId());
		marketingService.saveMarketingRule(marketing);
//		修改活动商品，删除原的
		marketingProdsService.deleteByMarketId(marketing.getId());
		if(AppUtils.isNotBlank(marketing.getSkuIds()) && marketing.getIsAllProds().equals(0)){
			for (Long skuId : marketing.getSkuIds()){
//			获取sku 商品列表
				Sku sku = skuService.getSku(skuId);
//			添加参数活动商品
				marketingProdsService.addMarketingRuleProds(marketing.getId(), marketing.getType(), marketing.getShopId(), sku.getProdId(), sku.getSkuId(), sku.getPrice());
			}
		}
		return 1;
	}

	/**
	 * 封装 Marketing实体类
	 * 	 * @param appBizMarketingDto
	 */
	private Marketing getMarketing(AppBizMarketingDto appBizMarketingDto) {
		Marketing marketing = new Marketing();
		marketing.setMarketName(appBizMarketingDto.getMarketName());
		marketing.setStartTime(appBizMarketingDto.getStartTime());
		marketing.setEndTime(appBizMarketingDto.getEndTime());
		marketing.setUserId(appBizMarketingDto.getUserId());
		marketing.setShopId(appBizMarketingDto.getShopId());
		marketing.setType(appBizMarketingDto.getType());
		marketing.setIsAllProds(appBizMarketingDto.getIsAllProds());
		marketing.setSkuIds(appBizMarketingDto.getSkuIds());
		marketing.setManjian_rule(appBizMarketingDto.getManjianRule());
		marketing.setManze_rule(appBizMarketingDto.getManzeRule());
		marketing.setDiscount(appBizMarketingDto.getDiscount());
		marketing.setRemark(appBizMarketingDto.getRemark());
		return marketing;
	}


}



