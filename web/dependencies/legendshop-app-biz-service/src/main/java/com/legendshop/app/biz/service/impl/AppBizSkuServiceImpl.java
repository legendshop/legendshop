package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.service.AppBizSkuService;
import com.legendshop.biz.model.dto.AppBizMarketingProdDto;
import com.legendshop.business.dao.SkuDao;
import com.legendshop.model.entity.Sku;
import com.legendshop.spi.service.SkuService;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
/**
 * @author 27158
 */
@Service("appBizSkuService")
public class AppBizSkuServiceImpl implements AppBizSkuService {
	@Autowired
	private SkuDao skuDao;

	@Autowired
	private SkuService skuService;

	@Override
	public String isProdChange(Long prodId) {
		return skuService.isProdChange(prodId);
	}
}
