package com.legendshop.app.biz.controller;

import com.legendshop.app.biz.service.AppBizShopOrderBillService;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ShopOrderBillStatusEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.ShopOrderBill;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.ShopOrderBillService;
import com.legendshop.util.AppUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@Api(tags = "商家结算",value = "商家结算接口")
public class AppBizShopOrderBillController {

	@Autowired
	private  LoginedUserService loginedUserService;

	@Autowired
	private AppBizShopOrderBillService appBizShopOrderBillService;

	@Autowired
	private ShopOrderBillService shopOrderBillService;

	/**
	 * 获取商家结算列表
	 * @param curPageNO
	 * @param sn
	 * @param status
	 * @return
	 */
	@ApiOperation(value = "获取商家结算列表", httpMethod = "POST", notes = "获取商家结算列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
				@ApiImplicitParam(paramType="query", name = "sn", value = "订单号", required = false, dataType = "String"),
			@ApiImplicitParam(paramType="query", name = "status", value = "账单状态:，1已出账 2商家已确认  3平台已审核  4结算完成", required = false, dataType = "Integer"),
			@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = false, dataType = "String"),
	})
	@PostMapping("/s/shopOrderBillList")
	public ResultDto<AppPageSupport<ShopOrderBill>> query(String curPageNO,String sn,Integer status) {
		LoginedUserInfo user = loginedUserService.getUser();
		Long shopId = user.getShopId();
		if (AppUtils.isBlank(user.getShopId())){
			return ResultDtoManager.fail("您好你还不是商家，请成为商家后再试！！！");
		}
		if (AppUtils.isBlank(curPageNO)){
			curPageNO="1";
		}
		AppPageSupport<ShopOrderBill> ps = appBizShopOrderBillService.getShopOrderBillPage(shopId, curPageNO,sn,status);
		return ResultDtoManager.success(ps);
	}



	@ApiOperation(value = "获取商家结算详情", httpMethod = "POST", notes = "获取商家结算详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "id", value = "结算号", required = true, dataType = "Long")
	@PostMapping(value = "/s/shopOrderBillDetail")
	public ResultDto<ShopOrderBill> load(Long id) {
		ShopOrderBill shopOrderBill = shopOrderBillService.getShopOrderBill(id);
		return  ResultDtoManager.success(shopOrderBill);
	}


	/**
	 * 商家确认
	 *
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "确认结算", httpMethod = "POST", notes = "确认结算",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "id", value = "结算号", required = true, dataType = "Long")
	@PostMapping(value = "/s/shopOrderBillConfirm")
	public ResultDto<Object> confirm(Long id) {
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user.getShopId())){
			return ResultDtoManager.fail("您好你还不是商家，请成为商家后再试！！！");
		}
		Long shopId = user.getShopId();

		ShopOrderBill shopOrderBill = shopOrderBillService.getShopOrderBill(id);
		if (shopOrderBill != null && shopOrderBill.getShopId().equals(shopId)) {
			shopOrderBill.setStatus(ShopOrderBillStatusEnum.SHOP_CONFIRM.value());
			shopOrderBillService.updateShopOrderBill(shopOrderBill);
			return ResultDtoManager.success("结算成功！！！");
		}
		return ResultDtoManager.fail("结算失败！！！");
	}
}
