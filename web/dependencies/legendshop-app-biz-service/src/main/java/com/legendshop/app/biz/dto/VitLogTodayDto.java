package com.legendshop.app.biz.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * 今日访客
 * @author Tony
 *
 */
@ApiModel(value="UserAddress地址")
public class VitLogTodayDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	/** 访问的用户名 */
	@ApiModelProperty(value="访问的用户名")
	private String userName;
	
	/** 用户头像, 为空显示默认头像  */
	@ApiModelProperty(value="用户头像, 为空显示默认头像")
	private String portrait;
	
	/** 访问次数  */
	@ApiModelProperty(value="访问次数")
	private Integer visitNum;
	
	/** 最后一次访问内容 */
	@ApiModelProperty(value="最后一次访问内容")
	private String lastContent;
	
	/** 最后一次访问时间 */
	@ApiModelProperty(value="最后一次访问时间")
	private Date lastTime;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPortrait() {
		return portrait;
	}

	public void setPortrait(String portrait) {
		this.portrait = portrait;
	}

	public Integer getVisitNum() {
		return visitNum;
	}

	public void setVisitNum(Integer visitNum) {
		this.visitNum = visitNum;
	}

	public String getLastContent() {
		return lastContent;
	}

	public void setLastContent(String lastContent) {
		this.lastContent = lastContent;
	}

	public Date getLastTime() {
		return lastTime;
	}

	public void setLastTime(Date lastTime) {
		this.lastTime = lastTime;
	}
}
