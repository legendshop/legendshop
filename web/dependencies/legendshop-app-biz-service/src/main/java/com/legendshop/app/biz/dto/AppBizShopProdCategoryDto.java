package com.legendshop.app.biz.dto;

import com.legendshop.model.entity.ShopCategory;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;


/**
 * 店铺首页 商品分类DTO
 * @author linzh
 */
@ApiModel(value="店铺商品分类Dto")
public class AppBizShopProdCategoryDto implements Serializable{

	/**  */
	private static final long serialVersionUID = 7536585172554342091L;


	/** 产品类目ID */
	@ApiModelProperty(value="产品类目ID")
	private Long id;

	/** 父节点 */
	@ApiModelProperty(value="父节点")
	private Long parentId;

	/** 产品类目名称 */
	@ApiModelProperty(value="产品类目名称")
	private String name;

	/** 类目的显示图片 */
	@ApiModelProperty(value="类目的显示图片")
	private String pic;

	/** 排序 */
	@ApiModelProperty(value="排序")
	private Integer seq;

	/** 默认是1，表示正常状态,0为下线状态 */
	@ApiModelProperty(value="默认是1，表示正常状态,0为下线状态")
	private Integer status;

	/** 店铺ID */
	@ApiModelProperty(value=" 店铺ID")
	private Long shopId;

	/** 下一级分类 list **/
	@ApiModelProperty(value="下一级分类 list")
	private List<ShopCategory> subCatList;

	/** 下一级 分类id **/
	@ApiModelProperty(value="下一级 分类id")
	private Long nextCatId;

	/** 最后一级分类id **/
	@ApiModelProperty(value="最后一级分类id")
	private Long subCatId;

	public AppBizShopProdCategoryDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public List<ShopCategory> getSubCatList() {
		return subCatList;
	}

	public void setSubCatList(List<ShopCategory> subCatList) {
		this.subCatList = subCatList;
	}

	public Long getNextCatId() {
		return nextCatId;
	}

	public void setNextCatId(Long nextCatId) {
		this.nextCatId = nextCatId;
	}

	public Long getSubCatId() {
		return subCatId;
	}

	public void setSubCatId(Long subCatId) {
		this.subCatId = subCatId;
	}
}
