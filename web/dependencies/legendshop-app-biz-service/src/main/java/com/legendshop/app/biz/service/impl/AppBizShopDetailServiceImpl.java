package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.dao.AppBizShopDetailDao;
import com.legendshop.app.biz.service.AppBizShopDetailService;
import com.legendshop.biz.model.dto.AppBizDetailDto;

import com.legendshop.business.dao.ShopDetailDao;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 27158
 */
@Service("appBizShopDetailService")
public class AppBizShopDetailServiceImpl implements AppBizShopDetailService {

	@Autowired
	private AppBizShopDetailDao appBizShopDetailDao;

	@Autowired
	private ShopDetailDao shopDetailDao;

	@Override
	public AppBizDetailDto getShopDetail(Long shopId) {

		AppBizDetailDto appBizDetailDto = appBizShopDetailDao.getShopDetail(shopId);
		return appBizDetailDto;
	}

	@Override
	public int updateShoplogoPic(Long shopId, String logoPic) {
		ShopDetail shopDetail = shopDetailDao.getById(shopId);
		if (AppUtils.isNotBlank(shopDetail)){

			shopDetail.setShopPic2(logoPic);
			shopDetailDao.update(shopDetail);
			return 1;
		}
		return -1;
	}

	@Override
	public int updateContactName(Long shopId, String contactName) {
		ShopDetail shopDetail = shopDetailDao.getById(shopId);
		if (AppUtils.isNotBlank(shopDetail)){
			shopDetail.setContactName(contactName);
			shopDetailDao.update(shopDetail);
			return 1;
		}
		return 0;
	}

	@Override
	public int updateContactMobile(Long shopId, String contactMobile) {
		ShopDetail shopDetail = shopDetailDao.getById(shopId);
		if (AppUtils.isNotBlank(shopDetail)){
			shopDetail.setContactMobile(contactMobile);
			shopDetailDao.update(shopDetail);
			return 1;
		}
		return 0;
	}

	@Override
	public int updateDetail(AppBizDetailDto shopDetailDto) {
		Long shopId = shopDetailDto.getShopId();
		ShopDetail shopDetail = shopDetailDao.getShopDetailByShopId(shopId);
		if (AppUtils.isNotBlank(shopDetail)){
			shopDetail.setProvinceid(shopDetailDto.getProvinceId());
			shopDetail.setCityid(shopDetailDto.getCityId());
			shopDetail.setAreaid(shopDetailDto.getAreaId());
			shopDetail.setShopAddr(shopDetailDto.getShopAddr());
			shopDetailDao.update(shopDetail);
			return 1;
		}
		return -1;
	}

	/**
	 * 更新店铺名称
	 * @param shopId
	 * @param siteName
	 * @return
	 */
	@Override
	public int updateSiteName(Long shopId, String siteName) {

		ShopDetail shopDetail = shopDetailDao.getById(shopId);
		if (AppUtils.isNotBlank(shopDetail)){
			shopDetail.setSiteName(siteName);
			shopDetailDao.update(shopDetail);
			return 1;
		}
		return 0;
	}
}
