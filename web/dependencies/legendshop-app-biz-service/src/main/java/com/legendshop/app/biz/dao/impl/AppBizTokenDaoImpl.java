/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.biz.dao.impl;

import com.legendshop.app.biz.dao.AppBizTokenDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.AppToken;
import org.springframework.stereotype.Repository;

/**
 * The Class AppBizTokenDaoImpl.
 */
@Repository
public class AppBizTokenDaoImpl extends GenericDaoImpl<AppToken, Long> implements AppBizTokenDao {

	@Override
	/** @Cacheable(value="App_Token", key="#userId")*/
	public AppToken getAppTokenByUserId(String userId) {
		return this.getByProperties(new EntityCriterion().eq("userId", userId));
	}

	@Override
	public AppToken getAppToken(Long id){
		return getById(id);
	}

	@Override
	/** @CacheEvict(value = "App_Token", key = "#appToken.userId")*/
	public int deleteAppToken(AppToken appToken){
		return delete(appToken);
	}

	@Override
	public Long saveAppToken(AppToken appToken){
		return save(appToken);
	}

	@Override
	/** @CacheEvict(value = "App_Token", key = "#appToken.userId")*/
	public int updateAppToken(AppToken appToken){
		return update(appToken);
	}

	/**
	 * 删除token
	 */
	@Override
	/** @CacheEvict(value = "App_Token", key = "#userId")*/
	public boolean deleteAppToken(String userId, String accessToken) {
		return update("delete from ls_app_token where user_id = ? and access_token = ?",userId, accessToken) > 0;
	}

	@Override
	public boolean deleteAppToken(String userId) {
		return update("delete from ls_app_token where user_id = ?",userId) > 0;
	}


	/**
	 * 根据userId、shopId和平台类型获取token
	 * @param userId 用户Id
	 * @param shopId 商家Id
	 * @param platform 平台类型
	 * @return
	 */
    @Override
    public AppToken getAppTokenByUserId(String userId, Long shopId, String platform) {

		return this.getByProperties(new EntityCriterion().eq("userId", userId).eq("shopId",shopId).eq("platform", platform));
    }

	/**
	 * 删除商家端token
	 * @param userId 用户ID
	 * @param shopId 商家ID
	 * @return
	 */
	@Override
	public boolean deleteAppTokeByshopId(String userId, Long shopId) {

		return update("delete from ls_app_token where user_id = ? and shop_id = ?",userId, shopId) > 0;
	}

}
