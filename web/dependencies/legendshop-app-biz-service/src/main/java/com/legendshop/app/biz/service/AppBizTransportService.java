package com.legendshop.app.biz.service;

import com.legendshop.biz.model.dto.AppBizTransportDto;
import com.legendshop.biz.model.dto.AppBizTransportInfoDto;

import java.util.List;

/**
 * app 运费模板服务
 * @author 27158
 */
public interface AppBizTransportService {
	/**
	 * app 商家获取运费模板
	 * @param shopId
	 * @return
	 */
	List<AppBizTransportDto> getTransports(Long shopId);

	/**
	 * 获取运费模板详情
	 * @param shopId
	 * @param id
	 * @return
	 */
	AppBizTransportInfoDto getDetailedTransport(Long shopId, Long id);
}
