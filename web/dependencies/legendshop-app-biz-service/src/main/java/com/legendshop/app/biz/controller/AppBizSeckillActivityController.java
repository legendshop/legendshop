package com.legendshop.app.biz.controller;

import com.legendshop.app.biz.service.AppBizProductService;
import com.legendshop.biz.model.dto.AppBizMarketingProdDto;
import com.legendshop.biz.model.dto.AppBizSeckillActivityDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.util.AppUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * app商家端秒杀活动
 * @author 27158
 */
@Api(tags = "秒杀活动管理",value = "活动查看、编辑、发布")
@RestController
public class AppBizSeckillActivityController {

	@Autowired
	private LoginedUserService loginedUserService;
	@Autowired
	private AppBizProductService appBizProductService;

	/**
	 * 添加秒杀活动
	 */
	@ApiOperation(value = "添加秒杀活动",httpMethod = "POST",notes = "添加秒杀活动",produces  = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/s/saveSeckillActivity")
	public ResultDto<String> saveSeckillActivity(AppBizSeckillActivityDto appBizSeckillActivityDto) {
		Long shopId = loginedUserService.getUser().getShopId();
		if(AppUtils.isBlank(shopId)){
			return ResultDtoManager.fail(-1,"该用户不是商家");
		}

		return null;
	}
	/**
	 * 获取秒杀活动商品列表
	 */
	@ApiOperation(value = "获取秒杀活动商品列表",httpMethod = "POST",notes = "获取秒杀活动商品列表",produces  = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query",name = "curPageNO",value = "分页",dataType = "string"),
			@ApiImplicitParam(paramType = "query",name = "name",value = "商品名称",dataType = "string")
	})
	@PostMapping("/s/getSeckillActivityPords")
	public ResultDto<AppPageSupport<AppBizMarketingProdDto>> getSeckillActivityPords(@RequestParam (required = false) String curPageNO,@RequestParam(required = false) String name) {
		Long shopId = loginedUserService.getUser().getShopId();
		if(AppUtils.isBlank(shopId)){
			return ResultDtoManager.fail(-1,"该用户不是商家");
		}
		AppPageSupport<AppBizMarketingProdDto> ps =appBizProductService.getSeckillActivityPords(curPageNO,name,shopId);
		return ResultDtoManager.success(ps);
	}
}
