package com.legendshop.app.biz.dao;

import com.legendshop.biz.model.dto.AppBizIndexDto;
import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.ShopDetail;

/**
 * @author 27158
 */
public interface AppBizReportDao extends GenericDao<ShopDetail,Long> {

	/**
	 * 获取商家端信息
	 * @param shopId
	 * @return
	 */
	 AppBizIndexDto getShopSales(Long shopId);
}
