package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.dao.AppBizOrderDao;
import com.legendshop.app.biz.service.AppBizOrderService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.biz.model.dto.order.*;
import com.legendshop.business.dao.DeliveryCorpDao;
import com.legendshop.business.dao.SubItemDao;
import com.legendshop.business.dao.UserAddressDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.SubTypeEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.order.MySubDto;
import com.legendshop.model.dto.order.OrderSearchParamDto;
import com.legendshop.model.dto.order.SubOrderItemDto;
import com.legendshop.model.entity.MergeGroupOperate;
import com.legendshop.model.entity.SubItem;
import com.legendshop.model.entity.UserAddressSub;
import com.legendshop.spi.service.MergeGroupOperateService;
import com.legendshop.spi.service.OrderService;
import com.legendshop.spi.service.SubService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("appBizOrderService")
public class AppBizOrderServiceImpl implements AppBizOrderService {

	@Autowired
	private OrderService orderService;

	@Autowired
	private AppBizOrderDao appBizOrderDao;

	@Autowired
	private SubItemDao subItemDao;

	@Autowired
	private UserAddressDao userAddressDao;

	@Autowired
	private DeliveryCorpDao deliveryCorpDao;

	@Autowired
	private SubService subService;

	@Autowired
	private MergeGroupOperateService mergeGroupOperateService;


	@Override
	public AppPageSupport<AppBizOrderDto> queryShopOrderList(OrderSearchParamDto paramDto, String condition) {
		PageSupport<AppBizOrderDto> ps = appBizOrderDao.getBizShopOrderDtos(paramDto,condition);
		if(AppUtils.isNotBlank(ps)&&AppUtils.isNotBlank(ps.getResultList())){
			for (AppBizOrderDto appBizOrderDto : ps.getResultList()) {
				List<SubItem> subItemBySubId = subItemDao.getSubItemBySubId(appBizOrderDto.getSubId());
				appBizOrderDto.setSubOrderItemDtos(convertOrderItemDto(subItemBySubId,appBizOrderDto));
			}
			int expireDate = subService.getOrderCancelExpireDate();//获取订单自动取消的时间
			for (AppBizOrderDto appBizOrderDto : ps.getResultList()) {
				Date countDownTime = null;
				if(OrderStatusEnum.UNPAY.value().equals(appBizOrderDto.getStatus())){

					//计算订单自动取消的截止时间
					countDownTime = DateUtils.rollMinute(appBizOrderDto.getSubDate(), expireDate);
				}else if(OrderStatusEnum.PADYED.value().equals(appBizOrderDto.getStatus())){
					if(SubTypeEnum.MERGE_GROUP.value().equals(appBizOrderDto.getSubType())){
						MergeGroupOperate mergeGroupOperate = mergeGroupOperateService.getMergeGroupOperateByAddNumber(appBizOrderDto.getAddNumber());
						if(null != mergeGroupOperate){
							countDownTime = mergeGroupOperate.getEndTime();
//						appBizOrderDto.setMergerGroupPeopleNum(mergeGroupOperate.getPeopleNumber());
//						appBizOrderDto.setMergerGroupAlreadyAddNum(mergeGroupOperate.getNumber());
						}
					}
				}else if(OrderStatusEnum.CONSIGNMENT.value().equals(appBizOrderDto.getStatus())){
					countDownTime = DateUtils.rollDay(appBizOrderDto.getDvyDate(), expireDate);
				}
				appBizOrderDto.setCountDownTime(countDownTime);

				//剩余支付时间
				int passTime = DateUtils.getOffsetMinutes(appBizOrderDto.getSubDate(), new Date());
				int orderCancelMinute = expireDate - passTime;
				appBizOrderDto.setOrderCancelMinute(Math.max(orderCancelMinute, 0));
			}
		}

		return PageUtils.convertPageSupport(ps);
	}

	@Override
	public AppBizOrderPriceDto getBizOrderPriceDto(String subNumber) {
		return appBizOrderDao.getBizOrderPriceDto(subNumber);
	}

	@Override
	public AppBizOrderRemarkDto getOrderRemarkInfo(String subNumber) {
		return appBizOrderDao.getOrderRemarkInfo(subNumber);
	}

	@Override
	public AppBizOrderSendCargoDto getOrderSendCargoInfo(String subNumber,Long shopId) {
		MySubDto mySubDto = subService.findOrderDetail(subNumber, shopId);
		if(null == mySubDto){
			return null;
		}
		return convertToAppBizOrderSendCargoDto(mySubDto);
	}

	public List<AppBizOrderItemDto> convertOrderItemDto(List<SubItem> subOrderItemDtoList,AppBizOrderDto appBizOrderDto){
		if(AppUtils.isNotBlank(subOrderItemDtoList)){
			Long returnId = -1L;
			List<AppBizOrderItemDto> dtoList = new ArrayList<AppBizOrderItemDto>();
			for (SubItem subOrderItemDto : subOrderItemDtoList) {
				AppBizOrderItemDto orderItemDto = new AppBizOrderItemDto();
				orderItemDto.setBasketCount(subOrderItemDto.getBasketCount());
				orderItemDto.setAttribute(subOrderItemDto.getAttribute());
				orderItemDto.setProdId(subOrderItemDto.getProdId());
				orderItemDto.setProductTotalAmout(subOrderItemDto.getProductTotalAmout());
				orderItemDto.setPic(subOrderItemDto.getPic());
				orderItemDto.setProdName(subOrderItemDto.getProdName());
				orderItemDto.setSkuId(subOrderItemDto.getSkuId());
				orderItemDto.setCash(subOrderItemDto.getCash());
				orderItemDto.setSubItemId(subOrderItemDto.getSubItemId());
				orderItemDto.setCommSts(subOrderItemDto.getCommSts());
				orderItemDto.setRefundId(subOrderItemDto.getRefundId());
				orderItemDto.setRefundState(subOrderItemDto.getRefundState());
				if (AppUtils.isNotBlank(subOrderItemDto.getRefundAmount())){
					orderItemDto.setRefundAmount(Double.parseDouble(subOrderItemDto.getRefundAmount()+""));
				}
				if (AppUtils.isNotBlank(subOrderItemDto.getRefundType())){
					orderItemDto.setRefundType(Long.parseLong(subOrderItemDto.getRefundType()+""));
					// 2020-08-17 jzh: 没搞明白为什么只有仅退款才能看退款详情 （前端不应该以订单的退款状态和退款id来判断，而应该以每个订单项来判断，所以下面这个if注不注释对前端毫无影响）
					if (subOrderItemDto.getRefundType()==1){
						returnId = subOrderItemDto.getRefundId();
					}
				}
				orderItemDto.setRefundCount(subOrderItemDto.getRefundCount());
				dtoList.add(orderItemDto);
			}
			appBizOrderDto.setRefundId(returnId);
			return dtoList;
		}
		return null;
	}


	@Override
	public AppBizOrderDetailDto queryShopOrderDetail(String subNumber, Long shopId) {
		MySubDto mySubDto = subService.findOrderDetail(subNumber, shopId);
		if(null == mySubDto){
			return null;
		}

		return convertToAppOrderDto(mySubDto);
	}

	private AppBizOrderDetailDto convertToAppOrderDto(MySubDto mySubDto){
		AppBizOrderDetailDto orderDto = new AppBizOrderDetailDto();
		orderDto.setActualTotal(mySubDto.getActualTotal());
		orderDto.setTotal(mySubDto.getTotal());
		orderDto.setPayManner(mySubDto.getPayManner());
		orderDto.setDvyFlowId(mySubDto.getDvyFlowId());
		orderDto.setOrderRemark(mySubDto.getOrderRemark());
		orderDto.setProdName(mySubDto.getProdName());
		orderDto.setShopId(mySubDto.getShopId());
		orderDto.setUserId(mySubDto.getUserId());
		orderDto.setDvyType(mySubDto.getDvyType());
		orderDto.setSubType(mySubDto.getSubType());
		orderDto.setUserName(mySubDto.getUserName());
		orderDto.setProductNums(mySubDto.getProductNums());
		orderDto.setFreightAmount(mySubDto.getFreightAmount());
		orderDto.setPayDate(mySubDto.getPayDate());
		orderDto.setPayTypeName(mySubDto.getPayTypeName());
		orderDto.setFinallyDate(mySubDto.getFinallyDate());
		orderDto.setSubId(mySubDto.getSubId());
		orderDto.setStatus(mySubDto.getStatus());
		orderDto.setDvyTypeId(mySubDto.getDvyTypeId());
		orderDto.setDvyDate(mySubDto.getDvyDate());
		orderDto.setUpdateDate(mySubDto.getUpdateDate());
		orderDto.setShopName(mySubDto.getShopName());
		orderDto.setSubDate(mySubDto.getSubDate());
		orderDto.setSubNumber(mySubDto.getSubNum());
		orderDto.setPayTypeId(mySubDto.getPayTypeId());
		orderDto.setInvoiceSubId(mySubDto.getInvoiceSubId());
		orderDto.setRemindDelivery(mySubDto.isRemindDelivery());
		orderDto.setRemindDeliveryTime(mySubDto.getRemindDeliveryTime());
		orderDto.setRefundState(mySubDto.getRefundState());
		orderDto.setDiscountPrice(mySubDto.getDiscountPrice());
		orderDto.setRedpackOffPrice(mySubDto.getRedpackOffPrice());
		orderDto.setCouponOffPrice(mySubDto.getCouponOffPrice());
		orderDto.setChangedPrice(mySubDto.getChangedPrice());
		orderDto.setIsShopRemarked(mySubDto.getIsShopRemarked());
		orderDto.setIsPayed(mySubDto.isIspay()?1:0);
		orderDto.setGroupStatus(mySubDto.getGroupStatus());
		orderDto.setMergeGroupStatus(mySubDto.getMergeGroupStatus());
		orderDto.setAddNumber(mySubDto.getAddNumber());

		List<AppBizOrderItemDto> orderItemDtoList =convertOrderItemDtos(mySubDto.getSubOrderItemDtos(),orderDto);
		orderDto.setSubOrderItemDtos(orderItemDtoList);

		AppBizAddressDto addressDto = convertAddressDto(mySubDto.getUserAddressSub());
		orderDto.setAddressDto(addressDto);

		orderDto.setDeliveryDto(convertDeliveryDto(mySubDto.getDelivery()));
		orderDto.setInvoiceSub(mySubDto.getInvoiceSub());

		//预售订单字段
		orderDto.setPayPctType(mySubDto.getPayPctType());
		orderDto.setPreDepositPrice(mySubDto.getPreDepositPrice());
		orderDto.setFinalPrice(mySubDto.getFinalPrice());
		orderDto.setFinalMStart(mySubDto.getFinalMStart());
		orderDto.setFinalMEnd(mySubDto.getFinalMEnd());
		orderDto.setIsPayDeposit(mySubDto.getIsPayDeposit());
		orderDto.setIsPayFinal(mySubDto.getIsPayFinal());

		Integer order_cancel = subService.getOrderCancelExpireDate(); //获取订单自动取消的时间
		//剩余支付时间
		int passTime = DateUtils.getOffsetMinutes(mySubDto.getSubDate(), new Date());
		orderDto.setOrderCancelMinute(order_cancel-passTime>0?order_cancel-passTime:0);

		orderDto.setRefundId(mySubDto.getRefundId());

		return orderDto;
	}

	public List<AppBizOrderItemDto> convertOrderItemDtos(List<SubOrderItemDto> subOrderItemDtoList,AppBizOrderDetailDto appBizOrderDetailDto){
		if(AppUtils.isNotBlank(subOrderItemDtoList)){
			List<AppBizOrderItemDto> dtoList = new ArrayList<AppBizOrderItemDto>();
			Long returnId = -1L;
			for (SubOrderItemDto subOrderItemDto : subOrderItemDtoList) {
				AppBizOrderItemDto orderItemDto = new AppBizOrderItemDto();
				orderItemDto.setBasketCount(subOrderItemDto.getBasketCount());
				orderItemDto.setProdId(subOrderItemDto.getProdId());
				orderItemDto.setAttribute(subOrderItemDto.getAttribute());
				orderItemDto.setProductTotalAmout(subOrderItemDto.getProductTotalAmout());
				orderItemDto.setPic(subOrderItemDto.getPic());
				orderItemDto.setProdName(subOrderItemDto.getProdName());
				orderItemDto.setSkuId(subOrderItemDto.getSkuId());
				orderItemDto.setCash(subOrderItemDto.getCash());
				orderItemDto.setSubItemId(subOrderItemDto.getSubItemId());
				orderItemDto.setCommSts(subOrderItemDto.getCommSts());
				orderItemDto.setRefundId(subOrderItemDto.getRefundId());
				orderItemDto.setRefundState(subOrderItemDto.getRefundState());
				orderItemDto.setRefundAmount(subOrderItemDto.getRefundAmount());
				orderItemDto.setRefundType(subOrderItemDto.getRefundType());
				if (subOrderItemDto.getRefundType()==1){
					returnId = subOrderItemDto.getRefundId();
				}
				orderItemDto.setRefundCount(subOrderItemDto.getRefundCount());
				dtoList.add(orderItemDto);
			}
			if (AppUtils.isNotBlank(appBizOrderDetailDto)){
				appBizOrderDetailDto.setRefundId(returnId);
			}
			return dtoList;
		}
		return null;
	}


	private AppBizAddressDto convertAddressDto(UserAddressSub usrAddrSubDto){
		if(AppUtils.isNotBlank(usrAddrSubDto)){
			AppBizAddressDto addressDto = new AppBizAddressDto();
			addressDto.setReceiver(usrAddrSubDto.getReceiver());
			addressDto.setSubPost(usrAddrSubDto.getSubPost());
			addressDto.setTelphone(usrAddrSubDto.getTelphone());
			addressDto.setDetailAddress(usrAddrSubDto.getDetailAddress());
			addressDto.setCityId(usrAddrSubDto.getCityId());
			addressDto.setProvinceId(usrAddrSubDto.getProvinceId());
			addressDto.setAreaId(usrAddrSubDto.getAreaId());
			addressDto.setEmail(usrAddrSubDto.getEmail());
			addressDto.setSubAdds(usrAddrSubDto.getSubAdds());
			addressDto.setMobile(usrAddrSubDto.getMobile());
			return addressDto;
		}
		return null;
	}

	public AppBizDeliveryDto convertDeliveryDto(com.legendshop.model.dto.order.DeliveryDto delivery){
		if(AppUtils.isNotBlank(delivery)){
			AppBizDeliveryDto deliveryDto = new AppBizDeliveryDto();
			deliveryDto.setQueryUrl(delivery.getQueryUrl());
			deliveryDto.setDvyFlowId(delivery.getDvyFlowId());
			deliveryDto.setDvyTypeId(delivery.getDvyTypeId());
			deliveryDto.setDelUrl(delivery.getDelUrl());
			deliveryDto.setDelName(delivery.getDelName());
			return deliveryDto;
		}
		return null;
	}


	private AppBizOrderSendCargoDto convertToAppBizOrderSendCargoDto(MySubDto mySubDto){
		AppBizOrderSendCargoDto orderDto = new AppBizOrderSendCargoDto();
		orderDto.setActualTotal(mySubDto.getActualTotal());
		orderDto.setTotal(mySubDto.getTotal());
		orderDto.setDvyFlowId(mySubDto.getDvyFlowId());
		orderDto.setShopId(mySubDto.getShopId());
		orderDto.setUserId(mySubDto.getUserId());
		orderDto.setDvyType(mySubDto.getDvyType());
		orderDto.setProductNums(mySubDto.getProductNums());
		orderDto.setFreightAmount(mySubDto.getFreightAmount());
		orderDto.setPayDate(mySubDto.getPayDate());
		orderDto.setSubId(mySubDto.getSubId());
		orderDto.setDvyTypeId(mySubDto.getDvyTypeId());
		orderDto.setDvyDate(mySubDto.getDvyDate());
		orderDto.setSubDate(mySubDto.getSubDate());
		orderDto.setSubNumber(mySubDto.getSubNum());
		orderDto.setChangedPrice(mySubDto.getChangedPrice());
		orderDto.setIsPayed(mySubDto.isIspay()?1:0);

		List<AppBizOrderItemDto> orderItemDtoList =convertOrderItemDtos(mySubDto.getSubOrderItemDtos(),null);
		orderDto.setSubOrderItemDtos(orderItemDtoList);

		AppBizAddressDto addressDto = convertAddressDto(mySubDto.getUserAddressSub());
		orderDto.setAddressDto(addressDto);

		orderDto.setDeliveryDto(convertDeliveryDto(mySubDto.getDelivery()));

		//预售订单字段
		orderDto.setPayPctType(mySubDto.getPayPctType());
		orderDto.setPreDepositPrice(mySubDto.getPreDepositPrice());
		orderDto.setFinalPrice(mySubDto.getFinalPrice());
		orderDto.setFinalMStart(mySubDto.getFinalMStart());
		orderDto.setFinalMEnd(mySubDto.getFinalMEnd());
		orderDto.setIsPayDeposit(mySubDto.getIsPayDeposit());
		orderDto.setIsPayFinal(mySubDto.getIsPayFinal());

		return orderDto;
	}


}
