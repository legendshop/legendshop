package com.legendshop.app.biz.constants;

public class MessageConstant {
	
	private MessageConstant(){};
	
	/** 未登录 */
	public final static String NOT_LOGIN = "对不起,请先登录!";
	
	/** 验证码错误 */
	public final static String VERIFICATION_CODE_ERROR = "对不起,验证码错误!";
	
	/** 密码错误 */
	public final static String PASSWORD_ERROR = "对不起,密码错误!";
	
	/** 参数错误 */
	public final static String PARAMS_ERROR= "对不起,请求参数错误!";
	
	/** 未知错误 */
	public final static String UNKNOWN_ERROR= "未知错误!";
	
}
