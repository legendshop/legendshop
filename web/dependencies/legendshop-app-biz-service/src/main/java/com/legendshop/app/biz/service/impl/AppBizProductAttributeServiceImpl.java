package com.legendshop.app.biz.service.impl;

import com.legendshop.app.biz.service.AppBizProductAttributeService;
import com.legendshop.biz.model.dto.productAttribute.*;
import com.legendshop.business.dao.*;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.CustomPropertyDto;
import com.legendshop.model.ResultCustomPropertyDto;
import com.legendshop.model.dto.ProductPropertyDto;
import com.legendshop.model.dto.ProductPropertyValueDto;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.ProductPropertyValueService;
import com.legendshop.spi.util.CategoryManagerUtil;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * app 商家端商品属性服务
 * @author 27158
 */
@Service
public class AppBizProductAttributeServiceImpl implements AppBizProductAttributeService {
	private  final  String  querybrandListByTypeId = "SELECT b.brand_id AS brandId, b.brand_name AS brandName  FROM ls_brand b INNER JOIN ls_prod_type_brand ptb on ptb.brand_id=b.brand_id where b.status=1  AND  ptb.type_id=? ";

	@Autowired
	private CategoryManagerUtil categoryManagerUtil;

	@Autowired
	private ProdTypeDao prodTypeDao;

	@Autowired
	private ProductPropertyDao productPropertyDao;

	@Autowired
 	private ProductPropertyValueService productPropertyValueService;

	@Autowired
	private ProdUserParamDao prodUserParamDao;

	@Autowired
	private ProdUserAttributeDao prodUserAttributeDao;

	@Autowired
	private ProductPropertyValueDao productPropertyValueDao;
	/**
	 * app 商家端获取平台商品参数
	 *
	 * @param categoryId
	 */
	@Override
	public AppBizPublishProductDto getAdminProdParameter(Long categoryId) {
		Integer typeId = categoryManagerUtil.getRelationTypeId(categoryId);
		if(AppUtils.isNotBlank(typeId)){
			AppBizPublishProductDto appBizPublishProductDto =new AppBizPublishProductDto();
			ProdType prodType = prodTypeDao.getProdType(Long.parseLong(typeId.toString()));
			if (AppUtils.isNotBlank(prodType)) {
				appBizPublishProductDto.setParamEditable(prodType.getParamEditable());
			}
			//1. 装载参数属性 // ProductPropertyTypeEnum.PARAM_ATTR.value()) = 2
			List<ProductPropertyDto> paramDtoList=  productPropertyDao.query(ConfigCode.getInstance().getCode("prod.queryProdParamByTypeId"),ProductPropertyDto.class,typeId);
			Map<Long, ProductPropertyDto> productPropertyMap= loadProdPropertiesValue(paramDtoList);
			ArrayList<ProductPropertyDto> productPropertyDtos = new ArrayList<>(productPropertyMap.values());
			List<AppBizAttdefPropertyDto> dtos=new ArrayList<>();
			productPropertyDtos
			.forEach(productPropertyDto -> {
				AppBizAttdefPropertyDto appBizAttdefPropertyDto=new AppBizAttdefPropertyDto();

				appBizAttdefPropertyDto.setPropId(productPropertyDto.getPropId());
				appBizAttdefPropertyDto.setPropName(productPropertyDto.getPropName());
				appBizAttdefPropertyDto.setRequired(productPropertyDto.isRequired());
				appBizAttdefPropertyDto.setIsMulti(productPropertyDto.isMulti());
				appBizAttdefPropertyDto.setSequence(productPropertyDto.getSequence());
				appBizAttdefPropertyDto.setType(productPropertyDto.getType());
				appBizAttdefPropertyDto.setIsRuleAttributes(productPropertyDto.getIsRuleAttributes());
				appBizAttdefPropertyDto.setIsForSearch(productPropertyDto.isForSearch());
				appBizAttdefPropertyDto.setInputProp(productPropertyDto.isInputProp());
				appBizAttdefPropertyDto.setName(productPropertyDto.getName());
				//1： 用户自定义   0：系统自带
				if (productPropertyDto.getIsCustom()==true){
					appBizAttdefPropertyDto.setIsCustom(1);
				}else{
					appBizAttdefPropertyDto.setIsCustom(0);
				}
				List<ProductPropertyValueDto> productPropertyValueList = productPropertyDto.getProductPropertyValueList();
				List<AppBizAttdefPropertyValueDto> valueDtos=new ArrayList<>();
//				拷贝
				valueDtos = convertToAppBizAttdefPropertyValueDto(productPropertyValueList);
				appBizAttdefPropertyDto.setPropertyValueList(valueDtos);
				dtos.add(appBizAttdefPropertyDto);
			});

			//2. 装载品牌
			List<AppBizBrandDto> brandList = productPropertyDao.query(querybrandListByTypeId, AppBizBrandDto.class,typeId);
			appBizPublishProductDto.setPropertyDtoList(dtos);
			appBizPublishProductDto.setBrandList(brandList);
			return appBizPublishProductDto;
		}
		return null;
	}

	/**
	 * app 商家端获取平台商品规格属性
	 *
	 * @param categoryId
	 * @return
	 */
	@Override
	public AppBizPublishProductDto getAdminSpecification(Long categoryId) {
		Integer typeId = categoryManagerUtil.getRelationTypeId(categoryId);
		if(AppUtils.isNotBlank(typeId)) {
			AppBizPublishProductDto appBizPublishProductDto =new AppBizPublishProductDto();
			ProdType prodType = prodTypeDao.getProdType(Long.parseLong(typeId.toString()));
			if (AppUtils.isNotBlank(prodType)) {
				appBizPublishProductDto.setAttrEditable(prodType.getAttrEditable());
			}
			//1. 装载规格属性  ProductPropertyTypeEnum.SALE_ATTR.value()) = 1
			List<ProductPropertyDto> propDtoList = productPropertyDao.query(ConfigCode.getInstance().getCode("prod.queryProdPropByTypeId"), ProductPropertyDto.class, typeId);
			Map<Long, ProductPropertyDto> specDtoMap = loadProdPropertiesValue(propDtoList);
			ArrayList<ProductPropertyDto> productPropertyDtos = new ArrayList<>(specDtoMap.values());

			List<AppBizAttdefPropertyDto> dtos=new ArrayList<>();
			productPropertyDtos
					.forEach(productPropertyDto -> {
						AppBizAttdefPropertyDto appBizAttdefPropertyDto=new AppBizAttdefPropertyDto();
						appBizAttdefPropertyDto.setPropId(productPropertyDto.getPropId());
						appBizAttdefPropertyDto.setPropName(productPropertyDto.getPropName());
						appBizAttdefPropertyDto.setRequired(productPropertyDto.isRequired());
						appBizAttdefPropertyDto.setIsMulti(productPropertyDto.isMulti());
						appBizAttdefPropertyDto.setSequence(productPropertyDto.getSequence());
						appBizAttdefPropertyDto.setType(productPropertyDto.getType());
						appBizAttdefPropertyDto.setIsRuleAttributes(productPropertyDto.getIsRuleAttributes());
						appBizAttdefPropertyDto.setIsForSearch(productPropertyDto.isForSearch());
						appBizAttdefPropertyDto.setInputProp(productPropertyDto.isInputProp());
						appBizAttdefPropertyDto.setName(productPropertyDto.getName());
						if(productPropertyDto.getIsCustom()==true){
							appBizAttdefPropertyDto.setIsCustom(1);
						}else{
							appBizAttdefPropertyDto.setIsCustom(0);
						}
						List<ProductPropertyValueDto> productPropertyValueList = productPropertyDto.getProductPropertyValueList();
						List<AppBizAttdefPropertyValueDto> valueDtos=new ArrayList<>();
//						拷贝
						valueDtos = convertToAppBizAttdefPropertyValueDto(productPropertyValueList);
						appBizAttdefPropertyDto.setPropertyValueList(valueDtos);
						dtos.add(appBizAttdefPropertyDto);
					});
			appBizPublishProductDto.setSpecDtoList(dtos);
			return appBizPublishProductDto;
		}
		return null;
	}

	/**
	 * 加载属性下的值
	 * @param propertyDtoList
	 * @return
	 */
	private Map<Long, ProductPropertyDto> loadProdPropertiesValue(List<ProductPropertyDto> propertyDtoList){
		Map<Long, ProductPropertyDto> productPropertyMap= new LinkedHashMap<Long, ProductPropertyDto>();
		for (ProductPropertyDto productPropertyDto : propertyDtoList) {
			ProductPropertyDto ppd = productPropertyMap.get(productPropertyDto.getPropId());
			if(ppd == null){
				ppd = productPropertyDto;
				productPropertyMap.put(productPropertyDto.getPropId(), ppd);
			}
			ppd.addProductPropertyValueDto(productPropertyDto.getPropId(),productPropertyDto.getName(),productPropertyDto.getValueId(),productPropertyDto.getValuePic());
		}

		return productPropertyMap;
	}


	@Override
	public List<AppBizAttdefDto> applyProperty(String originUserProperties,String customAttribute,String userName) {
		//1. 要返回的属性和属性值结果
		List<ResultCustomPropertyDto> resultCustomPropertyDtoList = new ArrayList<ResultCustomPropertyDto>();

		//1.1. 如果该商品已有 用户自定义属性了，也需取出并加入
		if (AppUtils.isNotBlank(originUserProperties)) {
			List<ResultCustomPropertyDto> originPropList = JSONUtil.getArray(originUserProperties, ResultCustomPropertyDto.class);
			if (AppUtils.isNotBlank(originPropList)) {
				resultCustomPropertyDtoList.addAll(originPropList);
			}
		}

		//2. 客户新加的自定义属性
		List<CustomPropertyDto> customPropertyDtoList = JSONUtil.getArray(customAttribute, CustomPropertyDto.class);
		if (AppUtils.isNotBlank(customPropertyDtoList)) {
			for (int i = 0; i < customPropertyDtoList.size(); i++) {
				String propertyName = customPropertyDtoList.get(i).getKey();
				List<String> propValue = customPropertyDtoList.get(i).getValue();

				if (AppUtils.isNotBlank(propertyName) && AppUtils.isNotBlank(propValue)) {

					//2.2 获取新的属性的ID值并返回
					ProductProperty customProperty = this.saveCustomAttribute(propertyName, userName, i);
					List<ProductPropertyValue> newValueList = productPropertyValueService.saveCustomAttributeValues(customProperty.getId(), propValue);

					ResultCustomPropertyDto resultCustomPropertyDto = new ResultCustomPropertyDto();
					resultCustomPropertyDto.setProductProperty(customProperty);
					resultCustomPropertyDto.setPropertyValueList(newValueList);
					resultCustomPropertyDtoList.add(resultCustomPropertyDto);
				}
			}

		}
		return convertToAppBizProductPropertyDto(resultCustomPropertyDtoList);
	}


	public List<AppBizAttdefDto> convertToAppBizProductPropertyDto(List<ResultCustomPropertyDto> resultCustomPropertyDtoList){
		List<AppBizAttdefDto> list = new ArrayList<>();
		for (ResultCustomPropertyDto resultCustomPropertyDto : resultCustomPropertyDtoList) {
			ProductProperty productProperty = resultCustomPropertyDto.getProductProperty();
			AppBizAttdefDto appBizAttdefDto = new AppBizAttdefDto();
			AppBizProductPropertyDto appBizProductPropertyDto = new AppBizProductPropertyDto();
			appBizProductPropertyDto.setIsRuleAttributes(productProperty.getIsRuleAttributes());
			appBizProductPropertyDto.setModifyDate(productProperty.getModifyDate());
			appBizProductPropertyDto.setPropId(productProperty.getPropId());
			appBizProductPropertyDto.setMemo(productProperty.getMemo());
			appBizProductPropertyDto.setType(Short.parseShort(productProperty.getType()+""));
			appBizProductPropertyDto.setUserName(productProperty.getUserName());
			appBizProductPropertyDto.setRecDate(productProperty.getRecDate());
			appBizProductPropertyDto.setPropName(productProperty.getPropName());
			appBizProductPropertyDto.setIsForSearch(productProperty.getIsForSearch());
			appBizProductPropertyDto.setIsMulti(productProperty.getIsMulti());
			appBizProductPropertyDto.setSequence(productProperty.getSequence());
			appBizProductPropertyDto.setIsCustom(productProperty.getIsCustom());
			appBizProductPropertyDto.setStatus(productProperty.getStatus());
			appBizAttdefDto.setAppBizProductPropertyDto(appBizProductPropertyDto);
			appBizAttdefDto.setPropertyValueList(convertToAppBizProductPropertyValueDto(resultCustomPropertyDto.getPropertyValueList()));
			list.add(appBizAttdefDto);
		}
		return list;
	}

	public List<AppBizProductPropertyValueDto> convertToAppBizProductPropertyValueDto(List<ProductPropertyValue> productPropertyValue){
		List<AppBizProductPropertyValueDto> list = new ArrayList<>();
		for (ProductPropertyValue propertyValue : productPropertyValue) {
			AppBizProductPropertyValueDto appBizProductPropertyValueDto = new AppBizProductPropertyValueDto();
			appBizProductPropertyValueDto.setValueId(propertyValue.getValueId());
			appBizProductPropertyValueDto.setModifyDate(propertyValue.getModifyDate());
			appBizProductPropertyValueDto.setPropId(propertyValue.getPropId());
			appBizProductPropertyValueDto.setName(propertyValue.getName());
			appBizProductPropertyValueDto.setAlias(propertyValue.getAlias());
			appBizProductPropertyValueDto.setPic(propertyValue.getPic());
			appBizProductPropertyValueDto.setRecDate(propertyValue.getRecDate());
			appBizProductPropertyValueDto.setStatus(propertyValue.getStatus());
			list.add(appBizProductPropertyValueDto);
		}
		return list;
	}

	public List<AppBizAttdefPropertyValueDto> convertToAppBizAttdefPropertyValue(List<ProductPropertyValue> productPropertyValueList){
		List<AppBizAttdefPropertyValueDto> appBizAttdefPropertyValueDtos = new ArrayList<>();
		for (ProductPropertyValue propertyValue : productPropertyValueList) {
			AppBizAttdefPropertyValueDto appBizAttdefPropertyValueDto = new AppBizAttdefPropertyValueDto();
			appBizAttdefPropertyValueDto.setValueId(propertyValue.getValueId());
			appBizAttdefPropertyValueDto.setPropId(propertyValue.getPropId());
			appBizAttdefPropertyValueDto.setName(propertyValue.getName());
			appBizAttdefPropertyValueDto.setAlias(propertyValue.getAlias());
			appBizAttdefPropertyValueDto.setPic(propertyValue.getPic());
			appBizAttdefPropertyValueDtos.add(appBizAttdefPropertyValueDto);
		}
		return appBizAttdefPropertyValueDtos;
	}

	@Override
	public List<AppBizProdShopParamDto> getShopProdParameter(Long shopId) {
		List<ProdUserParam> shopProdUserParam = prodUserParamDao.getShopProdUserParam(shopId);
		return convertToAppBizProdShopParam(shopProdUserParam);
	}

	@Override
	public List<AppBizAttdefPropertyValueDto> getProdParameterValueList(Long parmaId) {
		List<ProductPropertyValueDto> valueListByPropList = productPropertyValueDao.getValueListByPropId(parmaId);
		return convertToAppBizAttdefPropertyValueDto(valueListByPropList);
	}

	@Override
	public List<AppBizProdShopAttributeDto> getShopProdAttribute(Long shopId) {
		List<ProdUserAttribute> shopProdAttribute = prodUserAttributeDao.getShopProdAttribute(shopId);
		return convertToAppBizProdShopAttribute(shopProdAttribute);
	}

	@Override
	public List<AppBizProdShopParamDto> getShopParaValueList(Long parmaId, String name) {
		ProdUserParam shopProdUserParam = prodUserParamDao.getProdUserParam(parmaId);
		return convertToAppBizProdShopParam(shopProdUserParam,name);
	}

	@Override
	public List<AppBizProdShopAttributeDto> getShopAttrValueList(Long attrId, String name) {
		ProdUserAttribute shopProdAttribute = prodUserAttributeDao.getProdUserAttribute(attrId);
		return convertToAppBizProdShopAttr(shopProdAttribute,name);
	}

	private List<AppBizProdShopAttributeDto> convertToAppBizProdShopAttr(ProdUserAttribute shopProdAttribute, String name) {
		List<AppBizProdShopAttributeDto> appBizProdShopAttributeDtos = new ArrayList<>();
		List<AppBizKeyValueDto> array = JSONUtil.getArray(shopProdAttribute.getContent(), AppBizKeyValueDto.class);
		for (AppBizKeyValueDto appBizKeyValueDto : array) {
			if(AppUtils.isNotBlank(appBizKeyValueDto.getKey())&&!"".equals(appBizKeyValueDto.getKey())){
				if (appBizKeyValueDto.getKey().equals(name)){
					String[] valueList = appBizKeyValueDto.getValue().split(",");
					for (String s : valueList) {
						AppBizProdShopAttributeDto appBizProdShopAttributeDto = new AppBizProdShopAttributeDto();
						appBizProdShopAttributeDto.setName(s);
						appBizProdShopAttributeDtos.add(appBizProdShopAttributeDto);
					}
				}

			}
		}
		return appBizProdShopAttributeDtos;
	}

	private List<AppBizProdShopParamDto> convertToAppBizProdShopParam(ProdUserParam shopProdUserParam, String name) {
		List<AppBizProdShopParamDto> appBizProdShopParams = new ArrayList<>();
			List<AppBizKeyValueDto> array = JSONUtil.getArray(shopProdUserParam.getContent(), AppBizKeyValueDto.class);
			for (AppBizKeyValueDto appBizKeyValueDto : array) {
				if(AppUtils.isNotBlank(appBizKeyValueDto.getKey())&&!"".equals(appBizKeyValueDto.getKey())){
					if (appBizKeyValueDto.getKey().equals(name)){
						String[] valueList = appBizKeyValueDto.getValue().split(",");
						for (String s : valueList) {
							AppBizProdShopParamDto appBizProdShopParamDto = new AppBizProdShopParamDto();
							appBizProdShopParamDto.setName(s);
							appBizProdShopParams.add(appBizProdShopParamDto);
						}
					}

				}
			}
		return appBizProdShopParams;
	}

	public List<AppBizAttdefPropertyValueDto> convertToAppBizAttdefPropertyValueDto(List<ProductPropertyValueDto> productPropertyValueDtos){
		List<AppBizAttdefPropertyValueDto> appBizAttdefPropertyValueDtos = new ArrayList<>();
		for (ProductPropertyValueDto productPropertyValueDto : productPropertyValueDtos) {
			AppBizAttdefPropertyValueDto appBizAttdefPropertyValueDto = new AppBizAttdefPropertyValueDto();
			appBizAttdefPropertyValueDto.setValueId(productPropertyValueDto.getValueId());
			appBizAttdefPropertyValueDto.setPropId(productPropertyValueDto.getPropId());
			appBizAttdefPropertyValueDto.setName(productPropertyValueDto.getName());
			appBizAttdefPropertyValueDto.setAlias(productPropertyValueDto.getAlias());
			appBizAttdefPropertyValueDto.setPic(productPropertyValueDto.getPic());
			appBizAttdefPropertyValueDtos.add(appBizAttdefPropertyValueDto);
		}
		return appBizAttdefPropertyValueDtos;
	}

	public List<AppBizProdShopParamDto> convertToAppBizProdShopParam(List<ProdUserParam> prodUserParams){
		List<AppBizProdShopParamDto> appBizProdShopParams = new ArrayList<>();
		for (ProdUserParam userParam : prodUserParams) {
			List<AppBizKeyValueDto> array = JSONUtil.getArray(userParam.getContent(), AppBizKeyValueDto.class);
			for (AppBizKeyValueDto appBizKeyValueDto : array) {
				if(AppUtils.isNotBlank(appBizKeyValueDto.getKey())&&!"".equals(appBizKeyValueDto.getKey())){
					AppBizProdShopParamDto appBizProdShopParamDto = new AppBizProdShopParamDto();
					appBizProdShopParamDto.setId(userParam.getId());
					appBizProdShopParamDto.setName(appBizKeyValueDto.getKey());
					appBizProdShopParams.add(appBizProdShopParamDto);
				}
			}
		}
		return appBizProdShopParams;
	}

	public List<AppBizProdShopAttributeDto> convertToAppBizProdShopAttribute(List<ProdUserAttribute> prodUserAttributes){
		List<AppBizProdShopAttributeDto> appBizProdShopAttributes = new ArrayList<>();
		for (ProdUserAttribute prodUserAttribute : prodUserAttributes) {
			List<AppBizKeyValueDto> array = JSONUtil.getArray(prodUserAttribute.getContent(), AppBizKeyValueDto.class);
			for (AppBizKeyValueDto appBizKeyValueDto : array) {
				if(AppUtils.isNotBlank(appBizKeyValueDto.getKey())&&!"".equals(appBizKeyValueDto.getKey())){
					AppBizProdShopAttributeDto appBizProdShopAttributeDto = new AppBizProdShopAttributeDto();
					appBizProdShopAttributeDto.setId(prodUserAttribute.getId());
					appBizProdShopAttributeDto.setName(appBizKeyValueDto.getKey());
					appBizProdShopAttributes.add(appBizProdShopAttributeDto);
				}
			}
		}
		return appBizProdShopAttributes;
	}

	public ProductProperty saveCustomAttribute(String propertyName, String userName, int sequence) {

		ProductProperty newProperty = new ProductProperty();
		newProperty.setPropName(propertyName);//属性名
		newProperty.setMemo(propertyName);//别名
		newProperty.setIsMulti(false);//是否多选
		newProperty.setIsRequired(false);//是否必须
		newProperty.setSequence((long) sequence);//顺序
		newProperty.setStatus((short) 1);//状态
		newProperty.setType(0);//类型：文字；
		Date date = new Date();
		newProperty.setRecDate(date);
		newProperty.setModifyDate(date);
		newProperty.setIsRuleAttributes(1);//销售属性
		newProperty.setIsForSearch(false);//是否可以搜索
		newProperty.setIsInputProp(true);//是否输入属性
		newProperty.setUserName(userName);
		newProperty.setProdId(null);//商品ID
		newProperty.setIsCustom(1);//用户自定义
		Long propId = productPropertyDao.saveCustomProperty(newProperty);
		newProperty.setId(propId);
		return newProperty;
	}

}
