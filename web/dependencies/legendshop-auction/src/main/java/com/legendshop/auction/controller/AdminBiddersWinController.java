/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.auction.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.BiddersWin;
import com.legendshop.spi.service.BiddersWinService;

/**
 * 中标记录控制器
 *
 */
@Controller
@RequestMapping("/admin/biddersWin")
public class AdminBiddersWinController extends BaseController{

	@Autowired
	private BiddersWinService biddersWinService;

	/**
	 * 查询中标记录列表
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, BiddersWin biddersWin) {
		PageSupport<BiddersWin> ps = biddersWinService.getBiddersWinPage(curPageNO, biddersWin);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("biddersWin", biddersWin);

		return "/biddersWin/biddersWinList";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.BIDDERSWIN_LIST_PAGE);
	}

	/**
	 * 保存
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, BiddersWin biddersWin) {
		biddersWinService.saveBiddersWin(biddersWin);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return "forward:/admin/biddersWin/query.htm";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// FowardPage.BIDDERSWIN_LIST_QUERY);
	}

	/**
	 * 删除
	 */
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		BiddersWin biddersWin = biddersWinService.getBiddersWin(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// biddersWin.getUserName());
		// if(result!=null){
		// return result;
		// }
		biddersWinService.deleteBiddersWin(biddersWin);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return "forward:/admin/biddersWin/query.htm";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// FowardPage.BIDDERSWIN_LIST_QUERY);
	}

	/**
	 * 查看中标记录详情
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		BiddersWin biddersWin = biddersWinService.getBiddersWin(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// biddersWin.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("biddersWin", biddersWin);
		return "/biddersWin/biddersWin";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.BIDDERSWIN_EDIT_PAGE);
	}

	/**
	 * 跳转中标编辑页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return "/biddersWin/biddersWin";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.BIDDERSWIN_EDIT_PAGE);
	}

	/**
	 * 更新
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		BiddersWin biddersWin = biddersWinService.getBiddersWin(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// biddersWin.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("biddersWin", biddersWin);
		return "forward:/admin/biddersWin/query.htm";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.BIDDERSWIN_EDIT_PAGE);
	}

}
