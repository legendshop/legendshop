/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.auction.adminPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 用户中心前端页面定义.
 */
public enum AuctionAdminFrontPage implements PageDefinition {

	/** 商品信息 */
	AUCTIONS_PROD_SKU_LIST("/shop/auctionsProdSku"),
	
	/** 出价记录 */
	BIDDERS_LIST("/shop/biddersList"),
	
	/** The VARIABLE. 可变路径 */
	VARIABLE("");

	/** The value. */
	private final String value;

	private AuctionAdminFrontPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("auction", path);
	}

	public String getNativeValue() {
		return value;
	}

}
