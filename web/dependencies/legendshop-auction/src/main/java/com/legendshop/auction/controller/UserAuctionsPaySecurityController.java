/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.auction.controller;

import java.math.BigDecimal;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.spi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.auction.frontendPage.AuctionFrontPage;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.page.CommonFrontPage;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.entity.AuctionDepositRec;
import com.legendshop.model.entity.Auctions;
import com.legendshop.model.entity.SystemConfig;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;

/**
 * The Class AuctionsPaySecurityController.
 */
@Controller
@RequestMapping("/p/paimai/paySecurity")
public class UserAuctionsPaySecurityController {

	@Autowired
	private SystemConfigService systemConfigService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private AuctionsService auctionsService;

	@Autowired
	private AuctionDepositRecService auctionDepositRecService;

	@Autowired
	private PayTypeService payTypeService;
	/**
	 * 交保证金.
	 *
	 * @param request
	 * @param response
	 * @param paimaiId
	 * @return the string
	 */
	@RequestMapping(value = "/agreement/{paimaiId}")
	public String agreement(HttpServletRequest request, HttpServletResponse response, @PathVariable Long paimaiId) {
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		request.setAttribute("systemConfig", systemConfig);
		request.setAttribute("paimaiId", paimaiId);
		String result = PathResolver.getPath(AuctionFrontPage.AUCTION_AGREEMENT);
		return result;
	}

	/**
	 * 支付保证金.
	 *
	 * @param request
	 * @param response
	 * @param paimaiId
	 * @return the string
	 */
	@RequestMapping(value = "/pay/{paimaiId}")
	public String pay(HttpServletRequest request, HttpServletResponse response, @PathVariable Long paimaiId) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		Auctions auction = auctionsService.getAuctions(paimaiId);
		// 下线或者审核中
		if (auction == null || auction.getStatus().intValue() == 0 || auction.getStatus().intValue() == -2) {
			return PathResolver.getPath(CommonFrontPage.PROD_NON_EXISTENT);
		}
		if (auction.getIsSecurity().intValue() == 0 && auction.getSecurityPrice().doubleValue() > 0) {
			return PathResolver.getPath(CommonFrontPage.PROD_NON_EXISTENT);
		}
		
		String sn=CommonServiceUtil.getRandomSn();
		AuctionDepositRec depositRec = new AuctionDepositRec();
		depositRec.setAId(paimaiId);
		depositRec.setAuctionsTitle(auction.getAuctionsTitle());
		depositRec.setSubNumber(sn);
		depositRec.setPayMoney(new BigDecimal(auction.getSecurityPrice().doubleValue()));
		depositRec.setUserId(userId);
		depositRec.setOrderStatus(0);
		depositRec.setFlagStatus(0L);
		depositRec.setShopId(auction.getShopId());
		auctionDepositRecService.saveAuctionDepositRec(depositRec);
		String result = PathResolver.getRedirectPath("/p/paimai/paySecurity/pay/"+paimaiId+"/"+sn);
		return result;
	}
	
	
	/**
	 * 支付保证金.
	 *
	 * @param request
	 * @param response
	 * @param paimaiId
	 * @return the string
	 */
	@RequestMapping(value = "/pay/{paimaiId}/{paySn}")
	public String pay(HttpServletRequest request, HttpServletResponse response, @PathVariable Long paimaiId, @PathVariable String paySn) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		Auctions auction = auctionsService.getAuctions(paimaiId);
		if (auction == null || auction.getStatus().intValue() == 0 || auction.getStatus().intValue() == -2) { // 下线或者审核中
			return PathResolver.getPath(CommonFrontPage.PROD_NON_EXISTENT);
		}
		if (auction.getIsSecurity().intValue() == 0 && auction.getSecurityPrice().doubleValue() > 0) {
			return PathResolver.getPath(CommonFrontPage.PROD_NON_EXISTENT);
		}
		
		AuctionDepositRec depositRec=auctionDepositRecService.getAuctionDepositRec(userId,paySn);
		if (AppUtils.isBlank(depositRec)) {
			return PathResolver.getPath(CommonFrontPage.PROD_NON_EXISTENT);
		}
		if (depositRec.getOrderStatus()==1) {
			return PathResolver.getPath(CommonFrontPage.PROD_NON_EXISTENT);
		}
		
		UserDetail userDetail = userDetailService.getUserDetailById(userId);
		// 获取 支付方式
		Map<String, Integer> payTypesMap = payTypeService.getPayTypeMap();

		request.setAttribute("payTypesMap", payTypesMap);
		request.setAttribute("userDetail", userDetail);
		request.setAttribute("subNumber", depositRec.getSubNumber());
		request.setAttribute("amount", auction.getSecurityPrice().doubleValue());
		String result = PathResolver.getPath(AuctionFrontPage.PAY_SECURITY);
		return result;
	}
	

	/**
	 * 支付拍卖活动保证金.
	 *
	 * @param request
	 * @param response
	 * @param paimaiId
	 * @return the string
	 */
	@RequestMapping(value = "/payAuction/{paimaiId}")
	@ResponseBody
	public String payAuction(HttpServletRequest request, HttpServletResponse response, @PathVariable Long paimaiId) {
		Auctions auction = auctionsService.getAuctions(paimaiId);

		// 下线或者审核中
		if (auction == null || auction.getStatus().intValue() == 0 || auction.getStatus().intValue() == -2) {
			return "该活动已删除或者不存在";
		}
		if (auction.getIsSecurity().intValue() == 0 && auction.getSecurityPrice().doubleValue() > 0) {
			return "该活动不需要支付保证金";
		}
		if (!"true".equals(request.getSession().getAttribute(Constants.PASSWORD_CALLBACK))) {
			return "请输入正确的支付密码";
		}
		request.getSession().removeAttribute(Constants.PASSWORD_CALLBACK);
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		if (auctionDepositRecService.isPaySecurity(userId, paimaiId)) {
			return "您已支付活动保证金，请勿重复支付";
		}
		AuctionDepositRec auctionDepositRec = auctionDepositRecService.getAuctionDepositRecByUserIdAndaId(userId, paimaiId);
		if (AppUtils.isNotBlank(auctionDepositRec)) {
			if (auctionDepositRec.getFlagStatus() == 1) {
				return "卖家已处理你的活动保证金，你已不能参加该活动";
			}
		}
		double amount = auction.getSecurityPrice().doubleValue();
		// 去冻结预付款
		String result = auctionsService.payAuction(paimaiId, amount, userId,auction.getShopId(),auction.getAuctionsTitle());
		return result;
	}

}
