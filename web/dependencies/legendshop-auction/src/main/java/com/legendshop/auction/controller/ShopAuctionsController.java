/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.auction.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.auction.frontendPage.AuctionBackPage;
import com.legendshop.auction.frontendPage.AuctionFrontPage;
import com.legendshop.auction.frontendPage.AuctionRedirectPage;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AuctionsStatusEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.auctions.AuctionsDto;
import com.legendshop.model.entity.AuctionDepositRec;
import com.legendshop.model.entity.Auctions;
import com.legendshop.model.entity.Bidders;
import com.legendshop.model.entity.BiddersWin;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.Sku;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.AuctionDepositRecService;
import com.legendshop.spi.service.AuctionsService;
import com.legendshop.spi.service.BiddersService;
import com.legendshop.spi.service.BiddersWinService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.SkuService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;

/**
 * 拍卖控制器
 * 
 */
@SuppressWarnings("all")
@Controller
@RequestMapping("/s/auction")
public class ShopAuctionsController {

	private final Logger log = LoggerFactory.getLogger(ShopAuctionsController.class);

	@Autowired
	private AuctionsService auctionsService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private BiddersService biddersService;

	@Autowired
	private BiddersWinService biddersWinService;

	@Autowired
	private ProductService productService;

	@Autowired
	private SkuService skuService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private AuctionDepositRecService auctionDepositRecService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * 查看拍卖活动列表
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param auctions
	 * @return
	 */
	@RequestMapping("/query")
	@Authorize(authorityTypes = AuthorityType.AUCTIONS_MANAGE)
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Auctions auctions) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<Auctions> ps = auctionsService.queryAuctionListPage(curPageNO, shopId, auctions);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("bean", auctions);
		return PathResolver.getPath(AuctionFrontPage.AUCTIONS_LIST_PAGE);
	}

	/**
	 * 添加拍卖活动
	 * 
	 * @param request
	 * @param response
	 * @param auctions
	 * @return
	 */
	@RequestMapping("/addShopAuction")
	@Authorize(authorityTypes = AuthorityType.AUCTIONS_MANAGE)
	public String addAuctionActivity(HttpServletRequest request, HttpServletResponse response, Auctions auctions) {
		return PathResolver.getPath(AuctionFrontPage.AUCTIONS_PAGE);
	}

	/**
	 * 删除拍卖活动
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	@Authorize(authorityTypes = AuthorityType.AUCTIONS_MANAGE)
	public @ResponseBody String deleteAuction(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Auctions auctions = auctionsService.getAuctions(id);
		if (AppUtils.isBlank(auctions)) {
			return "操作失败,数据发送改变 ,请刷新后重试!";
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		if (!auctions.getShopId().equals(shopId)) {
			return "操作失败,无权操作!";
		}
		Date nowDate = new Date();
		if (AuctionsStatusEnum.ONLINE.value().equals(auctions.getId())) {
			if (auctions.getStartTime().before(nowDate) && auctions.getEndTime().after(nowDate)) {
				return "活动进行中,不能删除!";
			}
		}
		auctionsService.deleteAuctions(auctions);
		return Constants.SUCCESS;
	}

	/**
	 * 排除预错商品
	 * 
	 * @param request
	 * @param response
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	@RequestMapping(value = "/excludePresell", method = RequestMethod.POST)
	public @ResponseBody String excludePresell(HttpServletRequest request, HttpServletResponse response, @RequestParam Long prodId, @RequestParam Long skuId) {
		boolean result = auctionsService.excludePresell(prodId, skuId);
		if (result) {
			return "该sku已经参加了其他活动";
		}
		return Constants.SUCCESS;
	}

	/**
	 * 修改拍卖活动
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/updateAuction/{id}", method = RequestMethod.GET)
	@Authorize(authorityTypes = AuthorityType.AUCTIONS_MANAGE)
	public String updateAuction(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		if (AppUtils.isBlank(id)) {
			throw new NullPointerException("id is null");
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		Long shopId = user.getShopId();
		
		Auctions auction = auctionsService.getAuctions(id);
		AuctionsDto dto = productService.getAuctionProd(auction.getProdId(), auction.getSkuId());

		if (AppUtils.isBlank(auction) ||!auction.getShopId().equals(shopId)) {
			throw new NullPointerException("error");
		}
		
		if (AppUtils.isNotBlank(dto)) {
			auction.setProdName(dto.getProdName());
			auction.setProdPic(dto.getProdPic());
			auction.setProdPrice(dto.getProdPrice());
			auction.setCnProperties(dto.getCnProperties());
			request.setAttribute("dto", dto);
		}
		
		String shopName = shopDetailService.getShopName(shopId);
		
		if (AppUtils.isBlank(shopId) || AppUtils.isBlank(shopName)) {
			throw new NullPointerException("shopId is null or shopName is null");
		}
		
		auction.setStatus(AuctionsStatusEnum.VALIDATING.value());
		auction.setShopId(shopId);
		auction.setShopName(shopName);
		auction.setCrowdWatch(0L);
		auction.setEnrolNumber(0L);
		auction.setBiddingNumber(0L);
		auction.setCurPrice(new java.math.BigDecimal(1));
		auction.setUserId(userId);
		request.setAttribute("auctions", auction);
		return PathResolver.getPath(AuctionFrontPage.AUCTIONS_PAGE);
	}

	/**
	 * 保存（更新）拍卖活动
	 * 
	 * @param request
	 * @param response
	 * @param auctions
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@Authorize(authorityTypes = AuthorityType.AUCTIONS_MANAGE)
	public String save(HttpServletRequest request, HttpServletResponse response, Auctions auctions) {
		if (!validateAucitons(auctions)) {
			throw new NullPointerException("auctions is null");
		}
		if (auctions.getId() == null) {// 新建活动
			auctions.setStatus(AuctionsStatusEnum.VALIDATING.value());
		}
		if (AuctionsStatusEnum.FAILED.value().equals(auctions.getStatus())) {
			auctions.setStatus(AuctionsStatusEnum.VALIDATING.value());
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		Long shopId = user.getShopId();
		
		String shopName = shopDetailService.getShopName(shopId);
		if (AppUtils.isBlank(shopId) || AppUtils.isBlank(shopName)) {
			throw new NullPointerException("shopId is null or shopName is null");
		}

		if (auctions.getId()!=null){
			Auctions auction = auctionsService.getAuctions(auctions.getId());
			auction.setAuctionsTitle(auctions.getAuctionsTitle());
			auction.setStartTime(auctions.getStartTime());
			auction.setEndTime(auctions.getEndTime());
			auction.setProdId(auctions.getProdId());
			auction.setSkuId(auctions.getSkuId());
			auction.setFloorPrice(auctions.getFloorPrice());
			auction.setIsSecurity(auctions.getIsSecurity());
			auction.setIsCeiling(auctions.getIsCeiling());
			auction.setHideFloorPrice(auctions.getHideFloorPrice());
			auction.setMinMarkupRange(auctions.getMinMarkupRange());
			auction.setMaxMarkupRange(auctions.getMaxMarkupRange());
			if (AppUtils.isNotBlank(auctions.getFixedPrice())){
				auction.setFixedPrice(auctions.getFixedPrice());
			}
			if (AppUtils.isNotBlank(auctions.getSecurityPrice())){
				auction.setSecurityPrice(auctions.getSecurityPrice());
			}
			auction.setDelayTime(auctions.getDelayTime());
			auctionsService.saveAuctions(auction);
		}else {
			if (auctions.getIsSecurity().intValue() == 0) {
				auctions.setDelayTime(0L);
			}
			if (auctions.getSkuId() == null || auctions.getSkuId() ==0) {
				throw new NullPointerException("skuId is null or Zero");
			}
			auctions.setShopId(shopId);
			auctions.setShopName(shopName);
			auctions.setCrowdWatch(0L);
			auctions.setEnrolNumber(0L);
			auctions.setBiddingNumber(0L);
			auctions.setCurPrice(auctions.getFloorPrice());
			auctions.setUserId(userId);
			auctions.setFlagStatus(0);

			Boolean isAudit= systemParameterUtil.isAuctionRequereAudit();//后台拍卖业务设置的是否需要审核
			if(!isAudit){
				auctions.setStatus(AuctionsStatusEnum.ONLINE.value());
			}
			auctionsService.saveAuctions(auctions);
		}


		return PathResolver.getPath(AuctionRedirectPage.AUCTIONS_LIST_PAGE);
	}

	/**
	 * 运营
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/shopAuctionOperation/{id}", method = RequestMethod.GET)
	@Authorize(authorityTypes = AuthorityType.AUCTIONS_MANAGE)
	public String shopAuctionOperation(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		if (AppUtils.isBlank(id)) {
			throw new NullPointerException("id is null");
		}
		Auctions auction = auctionsService.getAuctions(id);
		if (auction.getStatus().equals(AuctionsStatusEnum.FINISH.value())) {
			BiddersWin biddersWin = biddersWinService.getBiddersWinByAid(auction.getId());
			if (AppUtils.isNotBlank(biddersWin)) {

				UserDetail user = userDetailService.getUserDetailById(biddersWin.getUserId());
				String userName = user.getNickName();
				if (AppUtils.isBlank(userName)) {
					userName = user.getUserName();
				}
				request.setAttribute("userName", userName);
			}
		}

		AuctionsDto dto = productService.getAuctionProd(auction.getProdId(), auction.getSkuId());
		if (AppUtils.isNotBlank(dto)) {
			auction.setProdName(dto.getProdName());
			auction.setProdPic(dto.getProdPic());
			auction.setProdPrice(dto.getProdPrice());
			auction.setCnProperties(dto.getCnProperties());
			auction.setProdStatus(dto.getProdStatus());
			auction.setSkuStatus(dto.getSkuStatus());
		}
		request.setAttribute("auction", auction);

		return PathResolver.getPath(AuctionFrontPage.AUCTIONS_OPERATION_PAGE);
	}

	/**
	 * 查看活动
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/shopAuctionDetail/{id}", method = RequestMethod.GET)
	@Authorize(authorityTypes = AuthorityType.AUCTIONS_MANAGE)
	public String shopAuctionDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		if (AppUtils.isBlank(id)) {
			throw new NullPointerException("id is null");
		}
		Auctions auction = auctionsService.getAuctions(id);
		AuctionsDto dto = productService.getAuctionProd(auction.getProdId(), auction.getSkuId());
		if (AppUtils.isNotBlank(dto)) {
			auction.setProdName(dto.getProdName());
			auction.setProdPic(dto.getProdPic());
			auction.setProdPrice(dto.getProdPrice());
			auction.setCnProperties(dto.getCnProperties());
			auction.setProdStatus(dto.getProdStatus());
			auction.setSkuStatus(dto.getSkuStatus());
			request.setAttribute("dto",dto);
		}
		request.setAttribute("auctions", auction);

		return PathResolver.getPath(AuctionFrontPage.AUCTIONS_DETAIL_PAGE);
	}

	/**
	 * 竞价详情
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/bidList/{id}")
	@Authorize(authorityTypes = AuthorityType.AUCTIONS_MANAGE)
	public String bidList(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, String curPageNO) {
		PageSupport<Bidders> ps = biddersService.queryBiddListPage(curPageNO, id);
		PageSupportHelper.savePage(request, ps);
		String result = PathResolver.getPath(AuctionFrontPage.BID_LIST);
		return result;
	}

	/**
	 * 交保证金的详情
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/depositRecList/{id}")
	public String depositRecList(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, String curPageNO) {
		PageSupport<AuctionDepositRec> ps = auctionDepositRecService.queryDepositRecList(curPageNO, id);
		PageSupportHelper.savePage(request, ps);
		String result = PathResolver.getPath(AuctionFrontPage.DEPOSIREC_LIST);
		return result;
	}

	/**
	 * 保证金记录
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/depositRec", method = RequestMethod.GET)
	public String depositRec(HttpServletRequest request, HttpServletResponse response, Long depId) {
		AuctionDepositRec auctionDepositRec = auctionDepositRecService.getAuctionDepositRec(depId);
		request.setAttribute("auctionDepositRec", auctionDepositRec);
		String result = PathResolver.getPath(AuctionBackPage.DEPOSIREC);
		return result;
	}

	/**
	 * 修改为已处理
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/updateFlagStatus", method = RequestMethod.POST)
	public @ResponseBody String updateFlagStatus(HttpServletRequest request, HttpServletResponse response, @RequestParam Long id) {
		AuctionDepositRec auctionDepositRec = auctionDepositRecService.getAuctionDepositRec(id);
		if (AppUtils.isBlank(auctionDepositRec)) {
			return Constants.FAIL;
		}

		auctionDepositRec.setFlagStatus(1l);
		auctionDepositRec.setOrderStatus(1);
		auctionDepositRecService.updateAuctionDepositRec(auctionDepositRec);
		return Constants.SUCCESS;
	}

	/**
	 * 拍卖商品显示
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param product
	 * @return
	 */
	@RequestMapping("/AuctionProdLayout")
	@Authorize(authorityTypes = AuthorityType.AUCTIONS_MANAGE)
	public String AuctionProdLayout(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {


		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<Product> ps = productService.getProductListPage(curPageNO, shopId, product);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prod", product);
		
		//新页面
		String result = PathResolver.getPath(AuctionBackPage.ADD_AUCTION_PROD_SKU_LAYOUT);
		return result;
	}
	
	/**
	 * 挑选商品弹层
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param product
	 * @return
	 */
	@RequestMapping(value = "/addProdSkuLayout", method = RequestMethod.GET)
	public String addProdSkuLayout(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<Product> ps = productService.getAddProdSkuLayout(curPageNO, shopId, product);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prod", product);
		return PathResolver.getPath(AuctionBackPage.ADD_AUCTION_PROD_SKU_LAYOUT);
	}

	/**
	 * 拍卖活动商品信息
	 * @param request
	 * @param response
	 * @param prodId
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/AuctionProdSku/{prodId}")
	@Authorize(authorityTypes = AuthorityType.AUCTIONS_MANAGE)
	public String AuctionProdSku(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId, String curPageNO) {
		PageSupport<Sku> ps = skuService.getAuctionProdSku(curPageNO, prodId);
		PageSupportHelper.savePage(request, ps);
		List<Sku> skuList = (List<Sku>) ps.getResultList();
		if (AppUtils.isNotBlank(skuList)) {
			for (int i = 0; i < skuList.size(); i++) {
				Sku sku = skuList.get(i);
				String property = skuService.getSkuByProd(sku);
				if (AppUtils.isNotBlank(property)) {
					sku.setProperty(property);
				}
			}
			request.setAttribute("skuList", skuList);
		}
		return PathResolver.getPath(AuctionFrontPage.AUCTIONS_PROD_SKU_LIST);
	}

	/**
	 * 验证拍卖活动
	 * 
	 * @param auctions
	 * @return
	 */
	private boolean validateAucitons(Auctions auctions) {
		if (AppUtils.isBlank(auctions.getAuctionsTitle())) {
			return false;
		}
		if (AppUtils.isBlank(auctions.getStartTime())) {
			return false;
		}
		if (AppUtils.isBlank(auctions.getEndTime())) {
			return false;
		}
		if (AppUtils.isBlank(auctions.getFloorPrice())) {
			return false;
		}
		if (AppUtils.isBlank(auctions.getMinMarkupRange())) {
			return false;
		}
		if (AppUtils.isBlank(auctions.getMaxMarkupRange())) {
			return false;
		}
		if (AppUtils.isBlank(auctions.getIsCeiling())) {
			return false;
		}
		if (AppUtils.isBlank(auctions.getIsSecurity())) {
			return false;
		}
		if (AppUtils.isBlank(auctions.getHideFloorPrice())) {
			return false;
		}
		if (AppUtils.isBlank(auctions.getProdId())) {
			return false;
		}
		return true;
	}

	/**
	 * 更新状态
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param status
	 * @return
	 */
	@RequestMapping(value = "/updatestatus/{id}/{status}", method = RequestMethod.POST)
	@Authorize(authorityTypes = AuthorityType.AUCTIONS_MANAGE)
	public @ResponseBody String updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Long status) {
		Auctions auctions = auctionsService.getAuctions(id);
		if (AppUtils.isBlank(auctions)) {
			return "操作失败,数据发送改变 ,请刷新后重试!";
		}
		
		Date nowDate = new Date();
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		if (!auctions.getShopId().equals(shopId)) {
			return "操作失败,无权操作!";
		}
		/*
		 * if(status.equals(auctions.getStatus())){ return
		 * "操作失败,数据发送改变 ,请刷新后重试!"; }
		 */

		if (status == AuctionsStatusEnum.OFFLINE.value()) {
			/* 下线：未开始才能下线 */
			if (auctions.getStartTime().before(nowDate) && auctions.getEndTime().after(nowDate)) {
				return "活动进行中,不能下线!";
			}
			auctions.setStatus(status);
			auctionsService.updateAuctions(auctions);
		} else if (status == AuctionsStatusEnum.ONLINE.value()) {
			/* 上线：未过期才能上线(发布) */
			if (auctions.getEndTime().before(nowDate)) {
				return "活动已过期,不能发布!";
			}
			auctions.setStatus(status);
			auctionsService.updateAuctions(auctions);
		}
		return Constants.SUCCESS;
	}

	@RequestMapping("/querySkuInfo")
	public String querySkuInfo(HttpServletRequest request,Long skuId){
		Sku sku = skuService.getSku(skuId);
		request.setAttribute("sku",sku);
		return PathResolver.getPath(AuctionFrontPage.AUCTIONS_PROD_SKU);
	}

}
