/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.auction.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Bidders;
import com.legendshop.spi.service.BiddersService;

/**
 * 出价记录控制器
 *
 */
@Controller
@RequestMapping("/admin/bidders")
public class AdminBiddersController extends BaseController{

	@Autowired
	private BiddersService biddersService;

	/**
	 * 查询出价记录列表
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Bidders bidders) {
		PageSupport<Bidders> ps = biddersService.getBiddersPage(curPageNO, bidders);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("bidders", bidders);

		return "/bidders/biddersList";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.BIDDERS_LIST_PAGE);
	}

	/**
	 * 保存
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, Bidders bidders) {
		biddersService.saveBidders(bidders);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return "forward:/admin/bidders/query.htm";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// FowardPage.BIDDERS_LIST_QUERY);
	}

	/**
	 * 删除
	 */
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Bidders bidders = biddersService.getBidders(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// bidders.getUserName());
		// if(result!=null){
		// return result;
		// }
		biddersService.deleteBidders(bidders);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return "forward:/admin/bidders/query.htm";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// FowardPage.BIDDERS_LIST_QUERY);
	}

	/**
	 * 查看出价记录详情
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Bidders bidders = biddersService.getBidders(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// bidders.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("bidders", bidders);
		return "/bidders/bidders";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.BIDDERS_EDIT_PAGE);
	}

	/**
	 * 跳转出价纪录编辑页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return "/bidders/bidders";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.BIDDERS_EDIT_PAGE);
	}

	/**
	 * 更新
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		Bidders bidders = biddersService.getBidders(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// bidders.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("bidders", bidders);
		return "forward:/admin/bidders/query.htm";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.BIDDERS_EDIT_PAGE);
	}

}
