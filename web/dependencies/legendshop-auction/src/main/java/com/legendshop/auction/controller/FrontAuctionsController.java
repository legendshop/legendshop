/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.auction.controller;

import com.legendshop.auction.frontendPage.AuctionBackPage;
import com.legendshop.auction.frontendPage.AuctionFrontPage;
import com.legendshop.base.util.XssFilterUtil;
import com.legendshop.base.xssfilter.XssDtoFilter;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.page.CommonFowardPage;
import com.legendshop.core.page.CommonFrontPage;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AuctionsStatusEnum;
import com.legendshop.model.constant.AuctionsTimeStatusEnum;
import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.dto.auctions.AuctionsResponse;
import com.legendshop.model.dto.auctions.BidDto;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.model.entity.*;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.*;
import com.legendshop.util.AppUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 前台拍卖活动控制器
 */
@Controller
@RequestMapping("/auction")
public class FrontAuctionsController {

	@Autowired
	private AuctionsService auctionsService;

	@Autowired
	private BiddersService biddersService;

	@Autowired
	private BiddersWinService biddersWinService;

	@Autowired
	private AuctionDepositRecService auctionDepositRecService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	RedissonClient redissonClient;

	@Autowired
	private ProductCommentStatService productCommentStatService;

	@Autowired
	private ShopCommStatService shopCommStatService;

	@Autowired
	private DvyTypeCommStatService dvyTypeCommStatService;

	@Autowired
	private UserDetailService userDetailService;

	/**
	 * 前台拍卖列表列表
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping(value = "/list")
	public String list(HttpServletRequest request, HttpServletResponse response, String curPageNO, ProductSearchParms parms) {
		parms = XssDtoFilter.safeProductSearchParms(parms);
		PageSupport<Auctions> ps = auctionsService.queryAuctionListPage(curPageNO, parms);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(AuctionFrontPage.AUCTIONS_LIST);
	}

	/**
	 * 前台拍卖列表列表排序
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping(value = "/autionList")
	public String autionList(HttpServletRequest request, HttpServletResponse response, String curPageNO, ProductSearchParms parms) {
		//xss过滤
		parms = XssDtoFilter.safeProductSearchParms(parms);
		PageSupport<Auctions> ps = auctionsService.queryPageAuctionList(curPageNO, parms);
		request.setAttribute("searchParams", parms);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(AuctionFrontPage.AUCTIONS_LIST);
	}

	/**
	 * 拍卖详情
	 *
	 * @param request
	 * @param response
	 * @param paimaiId
	 * @return
	 */
	@RequestMapping("/views/{paimaiId}")
	public String views(HttpServletRequest request, HttpServletResponse response, @PathVariable Long paimaiId) {
		SecurityUserDetail userDetail = UserManager.getUser(request);
		String buyUserId = null;
		//不登录也可以查看，需做判断
		if (AppUtils.isNotBlank(userDetail)) {
			buyUserId = userDetail.getUserId();
		}
		if (paimaiId == null) {
			return PathResolver.getPath(CommonFowardPage.INDEX_QUERY);
		}
		// use cache 加载拍卖活动详情和图片
		Auctions auction = auctionsService.getAuctionsDetails(paimaiId);
		// 下线或者审核中
		if (auction == null || AuctionsStatusEnum.OFFLINE.value().equals(auction.getStatus()) || AuctionsStatusEnum.FAILED.value().equals(auction.getStatus())) {
			return PathResolver.getPath(CommonFrontPage.PROD_NON_EXISTENT);
		}

		ShopDetail shopDetail = shopDetailService.getShopDetailByIdNoCache(auction.getShopId());
		if (AppUtils.isBlank(shopDetail) || !ShopStatusEnum.NORMAL.value().equals(shopDetail.getStatus())) {
			return PathResolver.getPath(CommonFrontPage.PROD_NON_EXISTENT);
		}
		request.setAttribute("shopDetail", shopDetail);

		//计算该店铺所有已评分商品的平均分
		Float shopscore = 0f;
		Float prodscore = 0f;
		Float logisticsScore = 0f;
		DecimalFormat decimalFormat = new DecimalFormat("0.0");
		decimalFormat.setRoundingMode(RoundingMode.FLOOR);

		ProductCommentStat prodCommentStat = productCommentStatService.getProductCommentStatByShopId(auction.getShopId());
		if (prodCommentStat != null && prodCommentStat.getScore() != null && prodCommentStat.getComments() != null) {
			prodscore = (float) prodCommentStat.getScore() / prodCommentStat.getComments();
		}
		request.setAttribute("prodAvg", decimalFormat.format(prodscore));

		//计算该店铺所有已评分店铺的平均分
		ShopCommStat shopCommentStat = shopCommStatService.getShopCommStatByShopId(auction.getShopId());
		if (shopCommentStat != null && shopCommentStat.getScore() != null && shopCommentStat.getCount() != null) {
			shopscore = (float) shopCommentStat.getScore() / shopCommentStat.getCount();
		}
		request.setAttribute("shopAvg", decimalFormat.format(shopscore));
		//计算该店铺所有已评分物流的平均分
		DvyTypeCommStat dvyTypeCommStat = dvyTypeCommStatService.getDvyTypeCommStatByShopId(auction.getShopId());
		if (dvyTypeCommStat != null && dvyTypeCommStat.getScore() != null && dvyTypeCommStat.getCount() != null) {
			logisticsScore = (float) dvyTypeCommStat.getScore() / dvyTypeCommStat.getCount();
		}
		request.setAttribute("dvyAvg", decimalFormat.format(logisticsScore));
		Float sum = 0f;
		if (shopscore != 0 && prodscore != 0 && logisticsScore != 0) {
			sum = (float) Math.floor((shopscore + prodscore + logisticsScore) / 3);
		} else if (shopscore != 0 && logisticsScore != 0) {
			sum = (float) Math.floor((shopscore + logisticsScore) / 2);
		}
		request.setAttribute("sum", decimalFormat.format(sum));

		// 围观数量
		Integer crowdWatch = auction.getCrowdWatch().intValue();
		if (crowdWatch == 0) {
			crowdWatch = biddersService.getCrowdWatchAuctions(paimaiId);
			if (crowdWatch == null || crowdWatch == 0) {
				crowdWatch = (int) (Math.random() * 1000);
				biddersService.crowdWatchAuctions(paimaiId, crowdWatch);
			}
		}
		// 设置围观数到缓存
		biddersService.crowdWatchAuctions(paimaiId, ++crowdWatch);

		// 当前登录用户
		String loginUserName = null;
		if (AppUtils.isNotBlank(userDetail)) {
			loginUserName = userDetail.getUsername();
		}

		request.setAttribute("loginUserName", loginUserName);
		request.setAttribute("auction", auction);

		Date nowDate = new Date();
		Long timeStatus = -1L;
		Long runTime = 0L;
		//尚未开始
		if (nowDate.getTime() < auction.getStartTimeStr()) {
			runTime = auction.getStartTimeStr() - nowDate.getTime();
			timeStatus = AuctionsTimeStatusEnum.UNSTART.value();
			request.setAttribute("time", auction.getEndTimeStr() - auction.getStartTimeStr());
		} else if ((nowDate.getTime() > auction.getStartTimeStr()) && (nowDate.getTime() < auction.getEndTimeStr())) {
			//进行中的拍卖
			runTime = auction.getEndTimeStr() - nowDate.getTime();
			timeStatus = AuctionsTimeStatusEnum.PROCESSING.value();
		} else if ((nowDate.getTime() > auction.getEndTimeStr())) {
			//过期的拍卖
			timeStatus = AuctionsTimeStatusEnum.OVER_TIME.value();
		}
		request.setAttribute("timeStatus", timeStatus);
		request.setAttribute("runTime", runTime);
		request.setAttribute("buyUserId", buyUserId);

		if (AppUtils.isNotBlank(userDetail)) {
			UserDetail user = userDetailService.getUserDetailById(userDetail.getUserId());
			if (AppUtils.isNotBlank(user)) {
				String password = user.getUserName() + "123456";
				request.setAttribute("user_name", user.getUserName());
				request.setAttribute("user_pass", password);
			}
		}

		// 跳转没有保证金的页面
		if (auction.getIsSecurity().intValue() == 0) {
			return PathResolver.getPath(AuctionFrontPage.AUCTIONS_VIEW);

			// 跳转保证金页面
		} else {
			return PathResolver.getPath(AuctionFrontPage.AUCTIONS_VIEW_BAOZHENG);
		}
	}

	/**
	 * 出价记录列表
	 *
	 * @param request
	 * @param response
	 * @param aId
	 * @param prodId
	 * @param skuId
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping(value = "/queryBidList")
	public String bidList(HttpServletRequest request, HttpServletResponse response, Long aId, Long prodId, Long skuId, String curPageNO) {
		PageSupport<Bidders> ps = biddersService.queryBiddersListPage(curPageNO, aId, prodId, skuId);
		PageSupportHelper.savePage(request, ps);
		String result = PathResolver.getPath(AuctionFrontPage.BIDDERS_LIST_VIEW);
		return result;
	}

	/**
	 * 竞拍协议
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/payDialog")
	public String payDialog(HttpServletRequest request, HttpServletResponse response) {
		String result = PathResolver.getPath(AuctionBackPage.PAY_DIALOG);
		return result;
	}

	/**
	 * 出价，并更新出价记录.
	 *
	 * @param request  the request
	 * @param response the response
	 * @param paimaiId the paimai id
	 * @param price    the price
	 * @param prodId   the prod id
	 * @param skuId    the sku id
	 * @return the string
	 */
	@RequestMapping(value = "/bid/{paimaiId}", method = RequestMethod.POST)
	@ResponseBody
	public String bid(HttpServletRequest request, HttpServletResponse response, @PathVariable Long paimaiId, Double price, Long prodId, Long skuId) {

		RLock fairLock = redissonClient.getFairLock("AuctionService_Bidder");
		try {
			fairLock.lock(60, TimeUnit.SECONDS);

			SecurityUserDetail user = UserManager.getUser(request);

			boolean config = UserManager.isUserLogined(user);
			if (!config || AppUtils.isBlank(user)) {
				return "login";
			}

			String userId = user.getUserId();
			Long shopId = user.getShopId();

			// 检查频率 可以使用redis 现在暂时性检查
			// 写入中标记录;
			int logo = biddersService.getDisposeBids(paimaiId);
			if (logo == 1) {
				return "出价失败,请稍候";
			}
			Auctions auction = auctionsService.getAuctions(paimaiId);
			Long id = auction.getId();
			if (shopId != null && shopId.equals(auction.getShopId())) {
				return "不能自己拍卖自己的商品";
			}
			String result = checkPaiMai(auction);
			if (!"success".equals(result)) {
				return result;
			}
			result = checkPrice(price, auction);
			if (!"success".equals(result)) {
				return result;
			}

			if (AuctionsStatusEnum.FINISH.value().equals(auction.getStatus())) {
				//出价失败，竞拍已完成
				return "31071";
			}

			List<Bidders> bidders = biddersService.getBiddersByAuctionId(paimaiId, 0, 6); // bidderService更新？
			// 可以使用redis
			Bidders topBidders = null;
			if (AppUtils.isNotBlank(bidders)) {
				topBidders = bidders.get(0);
				result = checkBid(price, userId, auction, topBidders);
				if (!"success".equals(result)) {
					return result;
				}
			}
			Set<String> biddersUserSet = bidders.stream().map(Bidders::getUserId).collect(Collectors.toSet());
			// 设置参加人数
//			auction.setEnrolNumber((long) biddersUserSet.size() + 1);
			biddersUserSet.add(userId);
			auction.setEnrolNumber((long) biddersUserSet.size());
			Bidders bid = new Bidders();
			bid.setBitTime(new Date());
			bid.setAId(paimaiId);
			bid.setProdId(prodId);
			bid.setSkuId(skuId);
			bid.setPrice(new BigDecimal(price));
			bid.setUserId(userId);

			String result2 = biddersService.bidAuctions(auction, bid, id, prodId, skuId);

			if (result2 != null) {
				return result2;
			}


		} catch (Exception ex) {

		} finally {
			fairLock.unlock();
		}

		return "573";
	}

	/**
	 * 获取当前商品的价格
	 *
	 * @param request
	 * @param response
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	@RequestMapping(value = "/current/queryPrice", method = RequestMethod.GET)
	@ResponseBody
	public String currentQueryPrice(HttpServletRequest request, HttpServletResponse response, Long prodId, Long skuId) {
		return auctionsService.currentQueryPrice(prodId, skuId);
	}

	/**
	 * @param request
	 * @param response
	 * @param paimaiId
	 * @return
	 */
	@RequestMapping(value = "/current/queryAccess/{paimaiId}", method = RequestMethod.GET)
	@ResponseBody
	public Long queryAccess(HttpServletRequest request, HttpServletResponse response, @PathVariable Long paimaiId) {
		return auctionDepositRecService.queryAccess(paimaiId);
	}

	/**
	 * 最新中标记录
	 *
	 * @param request
	 * @param response
	 * @param paimaiId 拍卖活动Id
	 * @param prodId   商品Id
	 * @param skuId    单品Id
	 * @return
	 */
	@RequestMapping(value = "/current/bidders/{paimaiId}", method = RequestMethod.GET)
	@ResponseBody
	public AuctionsResponse currentBidders(HttpServletRequest request, HttpServletResponse response, @PathVariable Long paimaiId, Long prodId, Long skuId,
										   int start, int end) {
		AuctionsResponse response2 = new AuctionsResponse();
		Auctions auction = auctionsService.getAuctions(paimaiId);
		if (auction == null) {
			return response2;
		}

		SecurityUserDetail user = UserManager.getUser(request);

		//围观数
		int crowdWatch = auction.getCrowdWatch().intValue();
		if (crowdWatch == 0) { // 查询缓存
			crowdWatch = biddersService.getCrowdWatchAuctions(paimaiId);
		}
		response2.setCrowdWatch(crowdWatch);
		Date now = new Date();
		if (auction.getStatus().intValue() == 1) {
			response2.setAuctionStatus(1);
			// 是否过期
			if (now.before(auction.getStartTime())) { // 当前时间 < 开始时间
				response2.setRemainTime(-1); // 未开始
			}
			// Date endTime=getDate(auction.getEndTime(),5);
			// //提前5秒结束,目的好让系统分析中标结果
			if (now.after(auction.getEndTime())) { // 当前时间 > 结束时间
				response2.setRemainTime(1); // 过期
			}
		} else if (auction.getStatus().intValue() == 2) { // 判断该活动是否上线
			response2.setAuctionStatus(2);
			// 已经完成 判断是否中标
			if (AppUtils.isNotBlank(user)) {
				boolean config = biddersWinService.isWin(user.getUserId(), prodId, skuId, paimaiId);
				if (config) {
					response2.setOrderStatus(1);
				}
			}
		}
		/* auctionDepositRecService.isPaySecurity(userId, paimaiId); */
		if (auction.getIsSecurity().intValue() == 1 && AppUtils.isNotBlank(user)) { // 该活动需要缴纳保证金
			int qualification = 0;
			boolean isPay = checkIsPaySecurity(user.getUserId(), paimaiId);
			if (isPay) {
				qualification = 1;
			}
			response2.setQualification(qualification);
		}
		double currentPrice = 0d;
		if (auction.getCurPrice() != null)
			currentPrice = auction.getCurPrice().doubleValue();
		// 获取出价记录
		List<Bidders> bidders = biddersService.getBiddersByAuctionId(paimaiId, start, end); // 可以使用redis
		Bidders topBidders = null;
		if (AppUtils.isNotBlank(bidders)) {
			topBidders = bidders.get(0);
			if (topBidders != null) {
				currentPrice = topBidders.getPrice().doubleValue();
			}
			SimpleDateFormat sdfLogin = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			int size = bidders.size();
			List<BidDto> bidList = new ArrayList<BidDto>(size);
			for (Iterator<Bidders> iterator = bidders.iterator(); iterator.hasNext(); ) {
				Bidders b = (Bidders) iterator.next();
				BidDto bidDto = new BidDto();
				String str = "";
				if (AppUtils.isNotBlank(b.getNickName())) {
					if (b.getNickName().length() >= 3) {
						str = b.getNickName().substring(b.getNickName().length() - 2, b.getNickName().length());
					} else {
						str = b.getNickName();
					}
				} else {
					if (b.getUserName().length() >= 3) {
						str = b.getUserName().substring(b.getUserName().length() - 2, b.getUserName().length());
					} else {
						str = b.getUserName();
					}
				}
				bidDto.setUserId("****" + str + "");
				String bidTime = sdfLogin.format(b.getBitTime());
				bidDto.setBidTime(bidTime);
				bidDto.setPriceStr(b.getPrice().toString());
				bidList.add(bidDto);
			}
			response2.setBidList(bidList);
			response2.setBidCount(size);
			if (size >= end) {
				long bidCount = biddersService.findBidsCount(paimaiId);
				response2.setBidCount(bidCount);
			}
		}
		response2.setCurrentPrice(currentPrice);

		return response2;
	}


	/**
	 * 检查用户选择的SKU是否参加拍卖
	 *
	 * @param request
	 * @param response
	 * @param prodId
	 * @return
	 */
	@RequestMapping(value = "/isAuction", method = RequestMethod.GET)
	@ResponseBody
	public Auctions isAuction(HttpServletRequest request, HttpServletResponse response, Long prodId, Long skuId) {
		Auctions auctions = auctionsService.isAuction(prodId, skuId);
		return auctions;
	}

	/**
	 * 检查支付是否安全
	 *
	 * @param userId
	 * @param paimaiId
	 * @return
	 */
	private boolean checkIsPaySecurity(String userId, Long paimaiId) {
		AuctionDepositRec auctionDepositRec = auctionDepositRecService.getAuctionDepositRecByUserIdAndaId(userId, paimaiId);
		if (AppUtils.isBlank(auctionDepositRec)) {
			return false;
		}
		return true;
		/*if (auctionDepositRec.getFlagStatus() == 1) {
			return true;
		} else {
			return false;
		}*/
	}

	/**
	 * 检查是否处理
	 *
	 * @param userId
	 * @param paimaiId
	 * @return
	 */
	private Integer checkIsHandle(String userId, Long paimaiId) {
		AuctionDepositRec auctionDepositRec = auctionDepositRecService.getAuctionDepositRecByUserIdAndaId(userId, paimaiId);
		if (AppUtils.isNotBlank(auctionDepositRec)) {
			if (auctionDepositRec.getOrderStatus() == 0) {
				return 1;
			} else {
				return 2;
			}
		}
		return 0;
	}

	/**
	 * 检查支付消息
	 *
	 * @param auction
	 * @return
	 */
	private String checkPaiMai(Auctions auction) {
		if (auction.getStatus().intValue() != 1) { // 判断该活动是否上线
			return "000"; // 下架
		}
		// 是否过期
		Date now = new Date();
		if (now.before(auction.getStartTime())) { // 当前时间 < 开始时间
			return "001";// 未开始
		}
		if (now.after(auction.getEndTime())) { // 当前时间 > 结束时间
			return "002";// 过期
		}
		return "success";
	}

	/**
	 * 检验价格
	 *
	 * @param price
	 * @return
	 */
	private String checkPrice(double price, Auctions auction) {
		if (AppUtils.isBlank(price) || price == 0) {
			return "请输入出价价格";
		}
		if (auction.getIsCeiling().intValue() == 1) {
			double maxPrice = auction.getFixedPrice().doubleValue();//
			if (maxPrice > 0 && price > maxPrice) {
				return "出价不能超过本次竞拍封顶价（￥" + maxPrice + "）";
			}
		}

		// add 起拍价过滤
		BigDecimal bg = new BigDecimal(price);
		if (auction.getFloorPrice().compareTo(bg) == 1) {
			return "出价不能低于起拍价（￥" + auction.getFloorPrice() + "）";
		}

		if (price <= auction.getCurPrice().doubleValue()) {
			return "出价不能低于当前价（￥" + auction.getCurPrice().doubleValue() + "）";
		}
		return "success";
	}

	/**
	 * 检查中标规则
	 *
	 * @param price
	 * @param userId
	 * @param auction
	 * @param topBidders
	 * @return
	 */
	private String checkBid(double price, String userId, Auctions auction, Bidders topBidders) {
		if (topBidders == null) {
			return "success";
		}

		if (userId.equals(topBidders.getUserId())) {
			// 将其按照以下格式转换成字符串
			SimpleDateFormat sdfLogin = new SimpleDateFormat("yyyy-MM-dd");
			// 按照格式转换两个数据
			String lastTime = sdfLogin.format(topBidders.getBitTime());
			String nowTime = sdfLogin.format(new Date());
			// 判断不是同一天则加上积分，否则就不加
			if (nowTime.equals(lastTime)) {
				return "600";
			}
		}

		/** 封顶 **/
		if (auction.getIsCeiling() == 1 && new BigDecimal(price).compareTo(auction.getFixedPrice()) == 0) {
			return "success";
		}

		double currentPrice = topBidders.getPrice().doubleValue();
		if (price <= currentPrice) {
			return "出价不能低于当前价（￥" + currentPrice + "）";
		}
		double priceLowerOffset = auction.getMinMarkupRange().doubleValue();
		if (price < currentPrice + priceLowerOffset) {
			return "加价幅度不能低于最低加价幅度（￥" + priceLowerOffset + "）";
		}
		double priceHigherOffset = auction.getMaxMarkupRange().doubleValue();
		if (price > currentPrice + priceHigherOffset) {
			return "加价幅度不能高于最高加价幅度（￥" + priceHigherOffset + "）";
		}

		return "success";
	}



}
