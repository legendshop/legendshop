/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.auction.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.auction.adminPage.AuctionAdminBackPage;
import com.legendshop.auction.adminPage.AuctionAdminFrontPage;
import com.legendshop.auction.adminPage.AuctionAdminPage;
import com.legendshop.auction.adminPage.AuctionAdminRedirectPage;
import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.AuctionsStatusEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.auctions.AuctionsDto;
import com.legendshop.model.entity.Auctions;
import com.legendshop.model.entity.Bidders;
import com.legendshop.model.entity.BiddersWin;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.model.entity.Sku;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.AuctionsService;
import com.legendshop.spi.service.BiddersService;
import com.legendshop.spi.service.BiddersWinService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.SkuService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;

/**
 * 拍卖活动管理控制器
 *
 */
@Controller
@RequestMapping("/admin/auction")
public class AdminAuctionsController {

	private final Logger log = LoggerFactory.getLogger(AdminAuctionsController.class);

	@Autowired
	private AuctionsService auctionsService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private ProductService productService;

	@Autowired
	private SkuService skuService;

	@Autowired
	private BiddersService biddersService;

	@Autowired
	private BiddersWinService biddersWinService;

	@Autowired
	private UserDetailService userDetailService;

	/**
	 * 拍卖活动页面
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param auctions
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, Auctions auctions) {
		if (AppUtils.isNotBlank(auctions.getEndTime())) {
			request.setAttribute("flag", 1);
		}
		request.setAttribute("bean", auctions);
		return PathResolver.getPath(AuctionAdminPage.AUCTIONS_LIST_PAGE);
	}
	
	/**
	 * 查询拍卖活动列表
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param auctions
	 * @return
	 */
	@RequestMapping("/queryContent")
	public String queryContent(HttpServletRequest request, HttpServletResponse response, String curPageNO, Auctions auctions) {
		if (AppUtils.isNotBlank(auctions.getEndTime())) {
			request.setAttribute("flag", 1);
		}
		PageSupport<Auctions> ps = auctionsService.queryAuctionListPage(curPageNO, auctions);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("bean", auctions);
		return PathResolver.getPath(AuctionAdminPage.AUCTIONS_CONTENT_LIST_PAGE);
	}

	/**
	 * 修改活动信息
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/revise/{id}", method = RequestMethod.GET)
	public String revise(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		if (AppUtils.isBlank(id)) {
			throw new NullPointerException("id is null");
		}
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		Long shopId = user.getShopId();
		
		Auctions auction = auctionsService.getAuctions(id);
		AuctionsDto dto = productService.getAuctionProd(auction.getProdId(), auction.getSkuId());
		if (AppUtils.isBlank(auction) || auction.getShopId() != shopId) {
			throw new NullPointerException("error");
		}
		if (AppUtils.isNotBlank(dto)) {
			auction.setProdName(dto.getProdName());
			auction.setProdPic(dto.getProdPic());
			auction.setProdPrice(dto.getProdPrice());
			auction.setCnProperties(dto.getCnProperties());
		}
		String shopName = shopDetailService.getShopName(shopId);
		
		if (AppUtils.isBlank(shopId) || AppUtils.isBlank(shopName)) {
			throw new NullPointerException("shopId is null or shopName is null");
		}
		auction.setStatus(-1l);
		auction.setShopId(shopId);
		auction.setShopName(shopName);
		auction.setCrowdWatch(0l);
		auction.setEnrolNumber(0l);
		auction.setBiddingNumber(0l);
		auction.setCurPrice(new java.math.BigDecimal(1));
		auction.setUserId(userId);
		request.setAttribute("auctions", auction);
		return PathResolver.getPath(AuctionAdminPage.AUCTIONS_REVISE_PAGE);
	}

	/**
	 * 保存活动信息
	 * 
	 * @param request
	 * @param response
	 * @param auctions
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(HttpServletRequest request, HttpServletResponse response, Auctions auctions) {
		if (!validateAucitons(auctions)) {
			throw new NullPointerException("auctions is null");
		}
		if (auctions.getId() == null) {
			// 新建活动进入审核中的状态
			auctions.setStatus(AuctionsStatusEnum.VALIDATING.value());
		}
		if (AuctionsStatusEnum.FAILED.value() == auctions.getStatus()) {
			// 对审核不通过的活动，修改后回到审核中的状态
			auctions.setStatus(AuctionsStatusEnum.VALIDATING.value());
		}
		auctions.setStatus(AuctionsStatusEnum.OFFLINE.value());
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		Long shopId = user.getShopId();
		
		String shopName = shopDetailService.getShopName(shopId);
		if (AppUtils.isBlank(shopId) || AppUtils.isBlank(shopName)) {
			throw new NullPointerException("shopId is null or shopName is null");
		}
		if (auctions.getIsSecurity().intValue() == 0) {
			auctions.setDelayTime(0l);
		}
		if (auctions.getSkuId() == null) {
			auctions.setSkuId(0l);// 无sku，则skuId默认为0
		}
		auctions.setShopId(shopId);
		auctions.setUserId(userId);
		auctions.setShopName(shopName);
		auctions.setCrowdWatch(0l);
		auctions.setEnrolNumber(0l);
		auctions.setBiddingNumber(0l);
		auctions.setCurPrice(new java.math.BigDecimal(1));
		auctions.setFlagStatus(0);
		auctionsService.saveAuctions(auctions);
		return PathResolver.getPath(AuctionAdminRedirectPage.AUCTION_LIST_QUERY);
	}

	@RequestMapping(value = "/excludePresell", method = RequestMethod.POST)
	public @ResponseBody String excludePresell(HttpServletRequest request, HttpServletResponse response, @RequestParam Long prodId, @RequestParam Long skuId) {
		boolean result = auctionsService.excludePresell(prodId, skuId);
		if (result) {
			return "该sku已经参加了其他活动";
		}
		return Constants.SUCCESS;
	}

	@RequestMapping("/load")
	public String load(HttpServletRequest request, HttpServletResponse response, Auctions auctions) {
		return PathResolver.getPath(AuctionAdminPage.AUCTIONS_PAGE);
	}

	/**
	 * 审核拍卖活动页面
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/audit/{id}", method = RequestMethod.GET)
	public String audit(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		if (AppUtils.isBlank(id)) {
			throw new NullPointerException("id is null");
		}
		Auctions auction = auctionsService.getAuctions(id);
		if (AppUtils.isNotBlank(auction)) {
			request.setAttribute("bean", auction);
		}
		AuctionsDto dto = productService.getAuctionProd(auction.getProdId(), auction.getSkuId());
		if (AppUtils.isNotBlank(dto)) {
			request.setAttribute("prod", dto);
		}
		return PathResolver.getPath(AuctionAdminPage.AUCTIONS_AUDIT_PAGE);
	}

	/**
	 * 删除拍卖活动
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="删除拍卖活动")
	@RequestMapping("/delete/{id}")
	public String deleteAuction(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		if (AppUtils.isBlank(id)) {
			throw new NullPointerException("auction id is null");
		}
		Auctions auctions = auctionsService.getAuctions(id);
		log.info("{}, delete  Picture {}", auctions.getId(), auctions.getAuctionsTitle());
		if (AppUtils.isBlank(auctions)) {
			throw new NullPointerException("error");
		}
		auctionsService.deleteAuctions(auctions);
		return PathResolver.getPath(AuctionAdminRedirectPage.AUCTION_LIST_QUERY);
	}

	/**
	 * 挑选商品弹层
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param product
	 * @return
	 */
	@RequestMapping("/AuctionProdLayout")
	public String AuctionProdLayout(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<Product> ps = productService.getAuctionProdLayout(curPageNO, shopId, product);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prod", product);
		String result = PathResolver.getPath(AuctionAdminBackPage.AUCTIONS_PROD_LAYOUT);
		return result;
	}

	@RequestMapping("/AuctionProdSku/{prodId}")
	public String AuctionProdSku(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId, String curPageNO) {
		PageSupport<Sku> ps = skuService.getAuctionProdSku(curPageNO, prodId);
		PageSupportHelper.savePage(request, ps);
		List<Sku> skuList = (List<Sku>) ps.getResultList();
		if (AppUtils.isNotBlank(skuList)) {
			for (int i = 0; i < skuList.size(); i++) {
				Sku sku = skuList.get(i);
				String property = skuService.getSkuByProd(sku);
				if (AppUtils.isNotBlank(property)) {
					sku.setProperty(property);
				}
			}
			request.setAttribute("skuList", skuList);
		} else {
			ProductDetail prod = productService.getProdDetail(prodId);
			request.setAttribute("prod", prod);
		}
		String result = PathResolver.getPath(AuctionAdminFrontPage.AUCTIONS_PROD_SKU_LIST);
		return result;
	}

	/**
	 * 校验数据
	 * 
	 * @param auctions
	 * @return
	 */
	private boolean validateAucitons(Auctions auctions) {
		if (AppUtils.isBlank(auctions.getAuctionsTitle())) {
			return false;
		}
		if (AppUtils.isBlank(auctions.getStartTime())) {
			return false;
		}
		if (AppUtils.isBlank(auctions.getEndTime())) {
			return false;
		}
		if (AppUtils.isBlank(auctions.getFloorPrice())) {
			return false;
		}
		if (AppUtils.isBlank(auctions.getMinMarkupRange())) {
			return false;
		}
		if (AppUtils.isBlank(auctions.getMaxMarkupRange())) {
			return false;
		}
		if (AppUtils.isBlank(auctions.getIsCeiling())) {
			return false;
		}
		if (AppUtils.isBlank(auctions.getIsSecurity())) {
			return false;
		}
		if (AppUtils.isBlank(auctions.getHideFloorPrice())) {
			return false;
		}
		if (AppUtils.isBlank(auctions.getProdId())) {
			return false;
		}
		return true;
	}

	/**
	 * 更新拍卖活动状态
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param status
	 * @return
	 */
	@SystemControllerLog(description="更新拍卖活动状态")
	@RequestMapping(value = "/updatestatus/{id}/{status}", method = RequestMethod.GET)
	public @ResponseBody String updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Long status) {
		Auctions auctions = auctionsService.getAuctions(id);
		if (AppUtils.isBlank(auctions) || AppUtils.isBlank(status))
			if (auctions == null) {
				throw new NullPointerException("null");
			}
		if (!status.equals(auctions.getStatus())) {
			if (AuctionsStatusEnum.OFFLINE.value().equals((Long) status) || AuctionsStatusEnum.ONLINE.value().equals((Long) status)) {
				auctions.setStatus(status);
				auctionsService.updateAuctions(auctions);
			}
		}
		return Constants.SUCCESS;
	}

	/**
	 * 终止拍卖活动
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="终止拍卖活动")
	@RequestMapping(value = "/offlineAuctions/{id}", method = RequestMethod.POST)
	public @ResponseBody String offlineAuctions(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {

		Auctions auctions = auctionsService.getAuctions(id);
		if(AppUtils.isBlank(auctions)){
			return "该活动不存在或已被删除，请刷新后重试";
		}

		// 终止拍卖活动,退还拍卖保证金以及释放sku
		String result = auctionsService.offlineAuctions(id);
		biddersWinService.cleanAuctions(id);
		biddersWinService.cleanAuctionsDetail(id);
		return result;
	}

	/**
	 * 审核拍卖活动
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param auditText
	 * @param flag
	 * @return
	 */
	@SystemControllerLog(description="审核拍卖活动")
	@RequestMapping(value = "/audit", method = RequestMethod.POST)
	public @ResponseBody String audit(HttpServletRequest request, HttpServletResponse response, Long id, String auditText, String flag) {
		Auctions auctions = auctionsService.getAuctions(id);
		if (AppUtils.isBlank(auctions) || AppUtils.isBlank(auditText) || AppUtils.isBlank(flag)) {
			return Constants.FAIL;
		}
		if (flag.equals("ok")) {
			auctions.setStatus(AuctionsStatusEnum.ONLINE.value());
		} else if (flag.equals("no")) {
			auctions.setStatus(AuctionsStatusEnum.FAILED.value());
		} else {
			return Constants.FAIL;
		}
		auctions.setAuditTime(new Date());
		auctions.setAuditOpinion(auditText);
		auctionsService.updateAuctions(auctions);
		return Constants.SUCCESS;
	}

	/**
	 * 拍卖详情
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/check/{id}", method = RequestMethod.GET)
	public String check(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		if (AppUtils.isBlank(id)) {
			throw new NullPointerException("id is null");
		}
		Auctions auction = auctionsService.getAuctions(id);
		if (auction.getStatus() == AuctionsStatusEnum.FINISH.value()) {
			BiddersWin biddersWin = biddersWinService.getBiddersWinByAid(auction.getId());
			if (AppUtils.isNotBlank(biddersWin)) {

				UserDetail user = userDetailService.getUserDetailById(biddersWin.getUserId());
				String userName = user.getNickName();
				if (AppUtils.isBlank(userName)) {
					userName = user.getUserName();
				}
				request.setAttribute("userName", userName);
			}
		}
		AuctionsDto dto = productService.getAuctionProd(auction.getProdId(), auction.getSkuId());
		if (AppUtils.isNotBlank(dto)) {
			auction.setProdName(dto.getProdName());
			auction.setProdPic(dto.getProdPic());
			auction.setProdPrice(dto.getProdPrice());
		}
		request.setAttribute("auction", auction);

		String result = PathResolver.getPath(AuctionAdminPage.CHECK_AUCITON_PAGE);
		return result;
	}

	/**
	 * 拍卖详情
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/bidList/{id}")
	public String bidList(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, String curPageNO) {
		PageSupport<Bidders> ps = biddersService.queryBiddersListPage(curPageNO, id);
		PageSupportHelper.savePage(request, ps);
		String result = PathResolver.getPath(AuctionAdminFrontPage.BIDDERS_LIST);
		return result;
	}

	/**
	 * 运营详情
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/operation/{auctionId}")
	public String operation(HttpServletRequest request, HttpServletResponse response, @PathVariable Long auctionId, String curPageNO) {
		PageSupport<Bidders> ps = biddersService.queryBiddersListPage(curPageNO, auctionId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("auctionId",auctionId);
		String result = PathResolver.getPath(AuctionAdminPage.AUCTIONS_OPERATION_PAGE);
		return result;
	}

}
