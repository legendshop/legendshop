/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.auction.adminPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 拍卖活动页面定义.
 */
public enum AuctionAdminPage implements PageDefinition {

	
	/** 拍卖活动管理 */
	AUCTIONS_LIST_PAGE("/shop/auctionsList"),
	
	/** 拍卖活动列表 */
	AUCTIONS_CONTENT_LIST_PAGE("/shop/auctionsContentList"),
	
	/** 拍卖活动详情 */
	CHECK_AUCITON_PAGE("/shop/checkAuctions"),
	
	/** 添加拍卖活动 */
	AUCTIONS_PAGE("/shop/auctions"),
	
	/** 审核拍卖活动 */
	AUCTIONS_AUDIT_PAGE("/shop/auctionsAudit"),
	
	/** 修改拍卖活动 */
	AUCTIONS_REVISE_PAGE("/shop/reviseAuction"),

	/** 拍卖运营管理 */
	AUCTIONS_OPERATION_PAGE("/shop/auctionsOperation");
	
	/** The value. */
	private final String value;

	/**
	 * The Constructor.
	 *
	 * @param value the value
	 */
	private AuctionAdminPage(String value) {
		this.value = value;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/* (non-Javadoc)
	 * @see com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.String, com.legendshop.core.constant.PageDefinition)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("auction", path);
	}

	/* (non-Javadoc)
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}
	
	
}
