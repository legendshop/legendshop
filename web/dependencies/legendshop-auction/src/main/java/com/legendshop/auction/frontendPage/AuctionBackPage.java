/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.auction.frontendPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 后台页面定义.
 */
public enum AuctionBackPage implements PageDefinition {

	/** 商品列表 */
	AUCTIONS_PROD_LAYOUT("/shop/auctionsProdLayout"),
	
	/** 新商品列表 */
	ADD_AUCTION_PROD_SKU_LAYOUT("/shop/addAuctionProdSkuLayout"),
	
	/** 支付提示信息 */
	PAY_DIALOG("/payDialog"),
	
	/** 竞拍人记录 */
	DEPOSIREC("/shop/depositrec"),
	
	
	AUCTION_PROD("/RuctionProd"),
	
	/** The variable. */
	VARIABLE("");

	/** The value. */
	private final String value;

	/**
	 * 实例化一个新的后台页面.
	 *
	 * @param value the value
	 */
	private AuctionBackPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath("auction", path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
