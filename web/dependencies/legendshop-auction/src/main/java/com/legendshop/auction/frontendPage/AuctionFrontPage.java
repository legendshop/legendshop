/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.auction.frontendPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 用户中心前端页面定义.
 */
public enum AuctionFrontPage implements PageDefinition {

	/** 拍卖活动列表 */
	AUCTIONS_LIST_PAGE("/shop/shopAuctionList"),

	/** 拍卖活动管理 */
	AUCTIONS_PAGE("/shop/addShopAuction"),

	/** 拍卖活动详情 */
	AUCTIONS_DETAIL_PAGE("/shop/showShopAuction"),

	/** 运营拍卖活动 */
	AUCTIONS_OPERATION_PAGE("/shop/shopAuctionDetail"),

	/** 商品信息 */
	AUCTIONS_PROD_SKU_LIST("/shop/auctionsProdSku"),

	/** 商品信息 */
	AUCTIONS_PROD_SKU("/auctionProd"),

	/** 出价记录列表 */
	BIDDERS_LIST("/shop/biddersList"),

	/** 出价记录 */
	BID_LIST("/shop/bidList"),

	/** 竞拍人记录列表 */
	DEPOSIREC_LIST("/shop/depositrecList"),

	/** 保证金页面 */
	PAY_SECURITY("/paySecurity"),

	/** 拍卖活动详情页面 */
	AUCTIONS_VIEW("/auctionsView"),

	/** 保证金拍卖活动 */
	AUCTIONS_VIEW_BAOZHENG("/auctionsViewB"),

	/** 拍卖活动列表 */
	AUCTIONS_LIST("/acutionsList"),

	/** 出价记录列表 */
	BIDDERS_LIST_VIEW("/biddersList"),

	/** 竞拍协议 */
	AUCTION_AGREEMENT("/auctionAgreement"),

	/** 拍卖中标记录 */
	AUCTIONBIDREC("/auctionBidRec"),
	
	/**参与拍卖 */
	USER_BIDDER_LIST("/userBidderList"),

	/** 修改页面 */
	AUCTIONS_UPDATE_PAGE("/shop/updateShopAuction"),

	/** The VARIABLE. 可变路径 */
	VARIABLE("");

	/** The value. */
	private final String value;

	private AuctionFrontPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath("auction", path);
	}

	public String getNativeValue() {
		return value;
	}

}
