/*
 * 
 * LegendShop 版权所有 2009-2011,并保留所有权利。
 * 
 * 官方网站：http://www.legendesign.net
 */
package com.legendshop.auction.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.auction.frontendPage.AuctionFrontPage;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.Bidders;
import com.legendshop.model.entity.BiddersWin;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.BiddersService;
import com.legendshop.spi.service.BiddersWinService;
import com.legendshop.util.AppUtils;

/**
 * 用户拍卖控制器
 *
 */
@Controller
@RequestMapping("/p")
public class UserAuctionsController extends BaseController {

	@Autowired
	private BiddersWinService biddersWinService;

	@Autowired
	private BiddersService biddersService;
	/**
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/auctionBidRec")
	public String auctionBidRec(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		PageSupport<BiddersWin> ps = biddersWinService.queryBiddersWinListPage(curPageNO, userId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("biddersWinList", request.getAttribute("list"));
		String result = PathResolver.getPath(AuctionFrontPage.AUCTIONBIDREC);
		return result;
	}
	

	/**
	 * 用户拍卖中心
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping("/userPartAution")
	public String userPartAution(HttpServletRequest request, HttpServletResponse response, String curPageNO) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		PageSupport<BiddersWin> pageSupport = biddersWinService.queryUserPartAution(curPageNO, userId);
		//判断是否领先
		List<BiddersWin> list = pageSupport.getResultList();
		if(AppUtils.isNotBlank(list)){
			for (BiddersWin bidders : list) {
				List<Bidders> bidders2 = biddersService.getBidders(bidders.getAId(), bidders.getProdId(), bidders.getSkuId());
				if(bidders2.get(0).getUserId().equals(bidders.getUserId())){
					bidders.setFirst(1);//领先
				}else
					bidders.setFirst(0);//出局
			}
		}
		PageSupportHelper.savePage(request, pageSupport);
		return PathResolver.getPath(AuctionFrontPage.USER_BIDDER_LIST);
	}
}