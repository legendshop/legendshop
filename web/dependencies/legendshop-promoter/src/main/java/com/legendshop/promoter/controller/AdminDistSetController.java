/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.promoter.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.promoter.model.DistSet;
import com.legendshop.promoter.page.PromoterAdminPage;
import com.legendshop.promoter.page.PromoterRedirectPage;
import com.legendshop.spi.service.DistSetService;
import com.legendshop.util.AppUtils;

/**
 * 后台 推广设置 控制类
 *
 */
@Controller
@RequestMapping("/admin/distSet")
public class AdminDistSetController extends BaseController {

	@Autowired
	private DistSetService distSetService;

	/**
	 * 保存 推广设置
	 * 
	 * @param request
	 * @param response
	 * @param distSet
	 * @param type
	 * @return
	 */
	@SystemControllerLog(description="保存 推广设置")
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, DistSet distSet, String type) {
		DistSet originDistSet = distSetService.getDistSet(1L);
		if (AppUtils.isBlank(originDistSet)) {
			originDistSet = new DistSet();
		}

		if ("rule".equals(type)) {// 分佣规则设置
			originDistSet.setRegCommis(distSet.getRegCommis());
			originDistSet.setSupportDist(distSet.getSupportDist());
			originDistSet.setFirstLevelRate(distSet.getFirstLevelRate());
			originDistSet.setSecondLevelRate(distSet.getSecondLevelRate());
			originDistSet.setThirdLevelRate(distSet.getThirdLevelRate());
		} else if ("promo".equals(type)) {// 推广员申请设置
			originDistSet.setRegAsPromoter(distSet.getRegAsPromoter());
			originDistSet.setChkRealName(distSet.getChkRealName());
			originDistSet.setChkMobile(distSet.getChkMobile());
			originDistSet.setChkMail(distSet.getChkMail());
			originDistSet.setChkAddr(distSet.getChkAddr());
		}

		distSetService.saveDistSet(originDistSet);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(PromoterRedirectPage.DISTSET_PAGE) + "?type=" + type;
	}

	/**
	 * 加载 推广设置
	 * 
	 * @param request
	 * @param response
	 * @param type
	 * @return
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response, String type) {
		DistSet distSet = distSetService.getDistSet(1L);
		request.setAttribute("distSet", distSet);
		request.setAttribute("type", type);
		return PathResolver.getPath(PromoterAdminPage.DISTSET_PAGE);
	}

}
