/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.promoter.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 跳转页面定义
 */
public enum PromoterFowardPage implements PageDefinition {
	
	/** The VARIABLE. 可变路径 */
	VARIABLE(""),
	
	;

	/** The value. */
	private final String value;

	/**
	 * 实例化一个新的tiles页面
	 * 
	 * @param value
	 */
	private PromoterFowardPage(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("forward:", path);
	}

	public String getNativeValue() {
		return value;
	}

}
