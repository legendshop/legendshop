/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.promoter.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PdCashLogEnum;
import com.legendshop.model.dto.promotor.SubUserCountDto;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.promoter.model.DistSet;
import com.legendshop.promoter.model.UserCommis;
import com.legendshop.promoter.page.PromoterMobileFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.DistSetService;
import com.legendshop.spi.service.PdCashLogService;
import com.legendshop.spi.service.UserCommisService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;

/**
 * 分销推广前台控制器
 */
@Controller
public class PromoterMobileController extends BaseController {

	@Autowired
	private UserCommisService userCommisService;

	@Autowired
	private DistSetService distSetService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private PdCashLogService pdCashLogService;
	
	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;
	
    @Autowired
    private PasswordEncoder passwordEncoder;

	/**
	 * 申请推广员
	 * 
	 * @param request
	 * @param response
	 * @param type
	 * @return
	 */
	@RequestMapping(value = "/p/wap/applyPromoter")
	public String applyPromoter(HttpServletRequest request, HttpServletResponse response, Integer reset) {
		DistSet originDistSet = distSetService.getDistSet(1L);

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		UserDetail userDetail = userDetailService.getUserDetailById(userId);
		UserCommis userCommis = userCommisService.getUserCommis(userId);

		if (AppUtils.isNotBlank(userCommis) && userCommis.getPromoterSts() == 1) { // 审核通过
			request.setAttribute("domainName", propertiesUtil.getMobileDomainName());
			request.setAttribute("userName", userDetail.getUserName());
			request.setAttribute("originDistSet", originDistSet);
			return PathResolver.getPath(PromoterMobileFrontPage.SHARE_REG_LINK);
		}

		if (AppUtils.isBlank(userCommis) || userCommis.getPromoterSts().intValue() == 0 || AppUtils.isNotBlank(reset)) {
			request.setAttribute("userCommis", userCommis);
			request.setAttribute("originDistSet", originDistSet);
			request.setAttribute("userDetail", userDetail);
			return PathResolver.getPath(PromoterMobileFrontPage.APPLYP_ROMOTE);
		} else {
			request.setAttribute("userCommis", userCommis); // 审批:拒绝 -1 等待审批 -2
			return PathResolver.getPath(PromoterMobileFrontPage.APPLY_STATUS);
		}

	}

	/**
	 * 提交申请
	 * 
	 * @param mobile
	 * @return
	 */
	@RequestMapping(value = "/p/wap/promoter/submitPromoter", method = RequestMethod.POST)
	public @ResponseBody String submitPromoter(HttpServletRequest request, HttpServletResponse response,UserDetail userDetail) {
		SecurityUserDetail user = UserManager.getUser(request);
		String result = userCommisService.mobileSubmitPromoter(userDetail,user.getUserId(),user.getUsername());
		return result;
		
	}

	/**
	 * 我的分销
	 * 
	 * @param mobile
	 * @return
	 */
	@RequestMapping(value = "/p/wap/myPromote", method = RequestMethod.GET)
	public String myPromote(HttpServletRequest request, HttpServletResponse response) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();

		// 查询当前用户佣金明细
		UserCommis userCommis = userCommisService.getUserCommis(userId);

		// 查询各级用户数量
		SubUserCountDto subUserCount = userCommisService.getSubUserCount(userName);

		request.setAttribute("userCommis", userCommis);
		request.setAttribute("subUserCount", subUserCount);
		return PathResolver.getPath(PromoterMobileFrontPage.MY_PROMOTE);
	}

	/**
	 * go to 申请提现页面
	 * 
	 * @param mobile
	 * @return
	 */
	@RequestMapping(value = "/p/wap/toApplyCash", method = RequestMethod.GET)
	public String toApplyCash(HttpServletRequest request, HttpServletResponse response, String curPageNO) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		UserCommis userCommis = userCommisService.getUserCommis(userId);

		request.setAttribute("userCommis", userCommis);
		return PathResolver.getPath(PromoterMobileFrontPage.APPLY_CASH);
	}

	/**
	 * 处理申请提现
	 * 
	 * @param money
	 *            提现金额
	 * @param passwd
	 *            用户密码
	 * @return
	 */
	@RequestMapping(value = "/p/wap/applyCash", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> applyCash(HttpServletRequest request, @RequestParam Double money, @RequestParam String passwd) {
		Map<String, Object> result = new HashMap<String, Object>();
		SecurityUserDetail userDetail = UserManager.getUser(request);
		// 校验密码是否正确
		UserDetail user = userDetailService.getUserDetailById(userDetail.getUserId());
		if (!passwordEncoder.matches(passwd, user.getPayPassword())) {
			result.put("status", Constants.FAIL);
			result.put("msg", "对不起,您输入的密码有误,请重新输入!");
			return result;
		}

		// 处理佣金提现
		result = userCommisService.processWithDraw(userDetail.getUsername(), money);

		return result;
	}

	/**
	 * go to 提现记录页面
	 * 
	 * @param mobile
	 * @return
	 */
	@RequestMapping(value = "/p/wap/withdrawList/page", method = RequestMethod.GET)
	public String withdrawListPage(HttpServletRequest request, HttpServletResponse response, String curPageNO) {

		int pageSize = 15;
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		String logType = PdCashLogEnum.PROMOTER.value();
		PageSupport<PdCashLog> ps = pdCashLogService.getPdCashLog(curPageNO, userId, logType, pageSize);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(PromoterMobileFrontPage.WITHDRAW_LIST_PAGE);
	}

	/**
	 * 提现记录列表
	 * 
	 * @param mobile
	 * @return
	 */
	@RequestMapping(value = "/p/wap/withdrawList/content", method = RequestMethod.GET)
	public String withdrawList(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		int pageSize = 10;
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		String logType = PdCashLogEnum.PROMOTER.value();
		PageSupport<PdCashLog> ps = pdCashLogService.getPresentRecord(curPageNO, pageSize, userId, logType);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(PromoterMobileFrontPage.WITHDRAW_LIST_CONTENT);
	}

}
