/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.promoter.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;


/**
 * 前台页面定义
 */
public enum PromoterFrontPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),
	
	/**提交申请**/
	SUBMIT_APPLY_PROMOTER("/applyPromoter"),
	
	/**申请状态**/
	APPLY_STATUS("/applyStatus"),
	
	/**分享注册链接**/
	SHARE_REG_LINK("/shareRegLink"),
	
	/** 我的佣金页面 */
	MY_COMMIS("/myCommis"),
	
	/** 奖励记录列表 */
	AWARD_LIST("/awardList"),
	
	/**我推广的会员**/
	MY_PROMOTER_USER("/myPromoterUser"),
	
	/** 申请提现 */
	APPLY_CASH("/applyCash"),
	
	/** 提现记录列表 */
	WITH_DRAWLIST("/withdrawList"),
	;

	/** The value. */
	private final String value;

	private PromoterFrontPage(String value) {
		this.value = value;
	}

	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath("promoter", path);
	}

	public String getNativeValue() {
		return value;
	}

}
