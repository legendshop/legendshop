/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.promoter.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 推销商管理Tiles页面定义
 * 
 */
public enum PromoterAdminPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 推广设置 页面. */
	DISTSET_PAGE("/distSet/distSet"),

	/** 推广员列表*. */
	USERCOMMIS_LIST("/userCommis/userCommisList"),

	/** 分销转账历史记录*. */
	CASHLOG_LIST("/commisChangeLog/pdCashLogList"),

	/** The usercommis page. */
	USERCOMMIS_PAGE("/userCommis/userCommis"),


	COMMIS_CHANGELOG_LIST("/commisChangeLog/commisChangeLogList");

	/** The value. */
	private final String value;

	/**
	 * 构造器
	 *
	 * @param value
	 */
	private PromoterAdminPage(String value) {
		this.value = value;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.String, com.legendshop.core.constant.PageDefinition)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("promoter", path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
