/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.promoter.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * （手机端）前台页面定义
 */
public enum PromoterMobileFrontPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),
	
	/** 申请推广员结果 */
	APPLY_STATUS("/applyStatus"),
	
	/** 申请推广员 */
	APPLYP_ROMOTE("/applyPromote"),
	
	/** 我的分销 */
	MY_PROMOTE("/myPromote"),
	
	/** 提现记录 */
	WITHDRAW_LIST_PAGE("/withdrawList"), 
	
	/** 提现记录详情 */
	WITHDRAW_LIST_CONTENT("/withdrawListContent"), 
		
	/**分享注册链接**/
	SHARE_REG_LINK("/shareRegLink"),
	
	/** 申请提现页面 */
	APPLY_CASH("/applyCash"),
	
	;

	/** The value. */
	private final String value;

	private PromoterMobileFrontPage(String value) {
		this.value = value;
	}

	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateMobilePluginFronendPath("promoter", path);
	}

	public String getNativeValue() {
		return value;
	}

}
