/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.promoter.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.promotor.SubLevelUserDto;
import com.legendshop.model.dto.promotor.SubUserCountDto;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.promoter.model.UserCommis;
import com.legendshop.promoter.page.PromoterAdminPage;
import com.legendshop.promoter.page.PromoterBackPage;
import com.legendshop.security.menu.AdminMenuUtil;
import com.legendshop.spi.service.UserCommisService;
import com.legendshop.spi.service.UserDetailService;

/**
 * 推广员
 */
@Controller
@RequestMapping("/admin/userCommis")
public class UserCommisController extends BaseController {
	
	@Autowired
	private UserCommisService userCommisService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private AdminMenuUtil adminMenuUtil;

	/**
	 * 批量关闭推广员分佣提成
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="批量关闭推广员分佣提成")
	@RequestMapping(value="/batchCloseCommis/{userIdStr}",method=RequestMethod.PUT)
	@ResponseBody
	public String batchCloseCommis(HttpServletRequest request, HttpServletResponse response, @PathVariable String userIdStr) {
		try{
			userCommisService.batchCloseCommis(userIdStr);
			return Constants.SUCCESS;
		}catch(Exception e){
			e.printStackTrace();
			return "批量关闭分佣失败。";
		}
	}

	/**
	 * 获取推广员列表数据
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param userCommis
	 * @return
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, UserCommis userCommis) {
		PageSupport<UserCommis> ps = userCommisService.getUserCommis(curPageNO, userCommis);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("userCommis", userCommis);
		adminMenuUtil.parseMenu(request, 8); //固定死选择某用户菜单，如果菜单有调整则需要调整参数，用于后台首页连接自动跳转到页面
		return PathResolver.getPath(PromoterAdminPage.USERCOMMIS_LIST); // default
	}
	
	/**
	 * 获取审核信息
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("/loadAuditInfo/{id}")
	public String loadAuditInfo(HttpServletRequest request, HttpServletResponse response, @PathVariable String id) {
		UserDetail userDetail = userDetailService.getUserDetailById(id);
		UserCommis userCommis = userCommisService.getUserCommis(id);
		request.setAttribute("userDetail", userDetail);
		request.setAttribute("userCommis", userCommis);
		return PathResolver.getPath(PromoterBackPage.USERCOMMIS_AUDIT);
	}

	/**
	 * 关闭推广员分佣提成
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="关闭推广员分佣提成")
	@RequestMapping("/closeCommis/{userId}")
	@ResponseBody
	public String closeCommis(HttpServletRequest request, HttpServletResponse response, @PathVariable String userId) {
		UserCommis userCommis = userCommisService.getUserCommis(userId);
		userCommis.setCommisSts(0);
		userCommisService.saveUserCommis(userCommis);
		return Constants.SUCCESS;
	}

	/**
	 * 开启分佣提成
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="开启分佣提成")
	@RequestMapping("/openCommis/{userId}")
	@ResponseBody
	public String openCommis(HttpServletRequest request, HttpServletResponse response, @PathVariable String userId) {
		UserCommis userCommis = userCommisService.getUserCommis(userId);
		userCommis.setCommisSts(1);
		userCommisService.saveUserCommis(userCommis);
		return Constants.SUCCESS;
	}

	/**
	 * 推广员详情
	 * 
	 * @param request
	 * @param response
	 * @param userId
	 * @return
	 */
	@RequestMapping("/load/{userId}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable String userId) {
		UserCommis userCommis = userCommisService.getUserCommis(userId);
		UserDetail userDetail = userDetailService.getUserDetailById(userId);

		SubUserCountDto subUserCount = userCommisService.getSubUserCount(userDetail.getUserName());

		request.setAttribute("subUserCount", subUserCount);
		request.setAttribute("userCommis", userCommis);
		request.setAttribute("userDetail", userDetail);
		return PathResolver.getPath(PromoterAdminPage.USERCOMMIS_PAGE);
	}

	/**
	 * 加载下级用户列表
	 */
	@RequestMapping("/loadSubLevel/{userName}")
	public String loadSubLevel(HttpServletRequest request, HttpServletResponse response, @PathVariable String userName, String curPageNO, String level,
			String nickName) {
		PageSupport<SubLevelUserDto> ps = userCommisService.getUserCommis(curPageNO, level, userName, nickName);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(PromoterBackPage.SUB_LEVEL_USER_PAGE); // default
	}

	/**
	 * 审核推广员
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param status
	 * @param promoterAuditComm
	 * @return
	 */
	@SystemControllerLog(description="审核推广员")
	@RequestMapping(value = "/updateStatus", method = RequestMethod.POST)
	public @ResponseBody String updateStatus(HttpServletRequest request, HttpServletResponse response, @RequestParam String id, @RequestParam Long status,
			@RequestParam String promoterAuditComm) {
		UserCommis userCommis = userCommisService.getUserCommis(id);
		if (userCommis == null) {
			return Constants.FAIL;
		}
		UserDetail  userDetail =  userDetailService.getUserDetailById(userCommis.getUserId());

		if (1 == status) {
			userCommis.setPromoterTime(new Date()); // 成为推广员时间
		}
		userCommis.setPromoterAuditComm(promoterAuditComm);// 审核意见
		userCommis.setPromoterSts(status);// 状态
		userCommisService.updateUserCommis(userCommis);
		
		if (status == -1) {//如果审核为拒绝，删除申请时候的保存到userdetail表里的用户邮箱，不然重新申请的时候，用同一个邮箱会报邮箱已存在
			userDetail.setUserMail(null);
			userDetailService.updateUserDetail(userDetail);
		}
		
		return Constants.SUCCESS;
	}

}
