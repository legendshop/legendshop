/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.promoter.controller;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.promoter.model.CommisChangeLog;
import com.legendshop.promoter.page.PromoterAdminPage;
import com.legendshop.spi.service.CommisChangeLogService;
import com.legendshop.spi.service.PdCashLogService;

/**
 * 佣金变更历史控制器
 */
@Controller
@RequestMapping("/admin/commisChangeLog")
public class AdminCommisChangeLogController extends BaseController{

	@Autowired
	private CommisChangeLogService commisChangeLogService;

	@Autowired
	private PdCashLogService cashLogService; // 转账的历史记录

	/**
	 * 查询转账的历史记录
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param pdCashLog
	 * @return
	 */
	@RequestMapping("/pdCashLogQuery")
	public String pdCashLogQuery(HttpServletRequest request, HttpServletResponse response, String curPageNO, PdCashLog pdCashLog) {
		int pageSize = 10;
		PageSupport<PdCashLog> ps = cashLogService.getPdCashLog(curPageNO, pdCashLog, pageSize);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("pdCashLog", pdCashLog);
		return PathResolver.getPath(PromoterAdminPage.CASHLOG_LIST);
	}

	/**
	 * 查询佣金变更历史列表
	 */
	@RequestMapping("/query")
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, CommisChangeLog commisChangeLog) {
		// CriteriaQuery cq = new CriteriaQuery(CommisChangeLog.class,
		// curPageNO);
		// cq.setPageSize(PropertiesUtil.getObject(SysParameterEnum.PAGE_SIZE,
		// Integer.class));
		// DataFunctionUtil.hasAllDataFunction(cq, request,
		// StringUtils.trim(cash.getUserName()));
		/*
		 * //TODO add your condition
		 */
		PageSupport<CommisChangeLog> ps = commisChangeLogService.getCommisChangeLogByCq(curPageNO);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("commisChangeLog", commisChangeLog);
		return PathResolver.getPath(PromoterAdminPage.COMMIS_CHANGELOG_LIST);
	}

	/**
	 * 保存佣金变更历史
	 */
	@RequestMapping(value = "/save")
	public String save(HttpServletRequest request, HttpServletResponse response, CommisChangeLog commisChangeLog) {
		commisChangeLogService.saveCommisChangeLog(commisChangeLog);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return "forward:/admin/commisChangeLog/query.htm";
	}

	/**
	 * 删除佣金变更历史表
	 */
	@RequestMapping(value = "/delete/{id}")
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		CommisChangeLog commisChangeLog = commisChangeLogService.getCommisChangeLog(id);
		commisChangeLogService.deleteCommisChangeLog(commisChangeLog);
		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("entity.deleted"));
		return "forward:/admin/commisChangeLog/query.htm";
	}

	/**
	 * 查看佣金变更历史表详情
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}")
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		CommisChangeLog commisChangeLog = commisChangeLogService.getCommisChangeLog(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// commisChangeLog.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("commisChangeLog", commisChangeLog);
		return "/commisChangeLog/commisChangeLog";
		// TODO, replace by next line, need to predefined FowardPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.COMMISCHANGELOG_EDIT_PAGE);
	}

	/**
	 * 加载创建佣金变更历史表页面
	 */
	@RequestMapping(value = "/load")
	public String load(HttpServletRequest request, HttpServletResponse response) {
		return "/commisChangeLog/commisChangeLog";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.COMMISCHANGELOG_EDIT_PAGE);
	}

	/**
	 * 更新佣金变更历史
	 */
	@RequestMapping(value = "/update")
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		CommisChangeLog commisChangeLog = commisChangeLogService.getCommisChangeLog(id);
		// String result = checkPrivilege(request,
		// UserManager.getUsername(request.getSession()),
		// commisChangeLog.getUserName());
		// if(result!=null){
		// return result;
		// }
		request.setAttribute("commisChangeLog", commisChangeLog);
		return "forward:/admin/commisChangeLog/query.htm";
		// TODO, replace by next line, need to predefined BackPage parameter
		// return PathResolver.getPath(request, response,
		// BackPage.COMMISCHANGELOG_EDIT_PAGE);
	}

}
