/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.promoter.page;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 后台页面定义
 */
public enum PromoterBackPage implements PageDefinition {

	/** The variable. */
	VARIABLE(""),

	/** 推广员订单列表 */
	SUB_LEVEL_USER_PAGE("/userCommis/subUserList"),

	/** 推广员审核页面 **/
	USERCOMMIS_AUDIT("/userCommis/userCommisAudit"),;

	/** The value. */
	private final String value;

	/**
	 * 实例化一个新的后台页面
	 *
	 * @param value
	 */
	private PromoterBackPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("promoter", path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
