/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.promoter.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.util.DateUtils;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PdCashLogEnum;
import com.legendshop.model.dto.promotor.AwardSearchParamDto;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.promoter.model.CommisChangeLog;
import com.legendshop.promoter.model.DistSet;
import com.legendshop.promoter.model.UserCommis;
import com.legendshop.promoter.page.PromoterFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.CommisChangeLogService;
import com.legendshop.spi.service.DistSetService;
import com.legendshop.spi.service.PdCashLogService;
import com.legendshop.spi.service.UserCommisService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;

/**
 * 分销推广前台控制器
 */
@Controller
@RequestMapping("/p/promoter")
public class PromoterFrontController extends BaseController {
	
	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;

	@Autowired
	private UserCommisService userCommisService;

	@Autowired
	private CommisChangeLogService commisChangeLogService;

	@Autowired
	private DistSetService distSetService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private PdCashLogService pdCashLogService;
	
    @Autowired
    private PasswordEncoder passwordEncoder;

	/**
	 * 申请推广员
	 * 
	 * @param request
	 * @param response
	 * @param reset
	 *            重新开始填的标识
	 * @return
	 */
	@RequestMapping(value = "/applyPromoter")
	public String applyPromoter(HttpServletRequest request, HttpServletResponse response, Integer reset) {
		DistSet originDistSet = distSetService.getDistSet(1L);

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		UserDetail userDetail = userDetailService.getUserDetailById(userId);
		UserCommis userCommis = userCommisService.getUserCommis(userId);

		if (AppUtils.isNotBlank(userCommis) && userCommis.getPromoterSts() == 1) { // 审核通过
			request.setAttribute("domainName", propertiesUtil.getPcDomainName());
			request.setAttribute("userName", userDetail.getUserName());
			request.setAttribute("originDistSet", originDistSet);
			return PathResolver.getPath(PromoterFrontPage.SHARE_REG_LINK);
		}

		if (AppUtils.isBlank(userCommis) || userCommis.getPromoterSts().intValue() == 0 || AppUtils.isNotBlank(reset)) {// 提交申请
			request.setAttribute("userCommis", userCommis);
			request.setAttribute("originDistSet", originDistSet);
			request.setAttribute("userDetail", userDetail);
			return PathResolver.getPath(PromoterFrontPage.SUBMIT_APPLY_PROMOTER);
		} else {// 申请状态
			request.setAttribute("userCommis", userCommis); // 审批:拒绝 -1 等待审批 -2
			return PathResolver.getPath(PromoterFrontPage.APPLY_STATUS);
		}

	}

	/**
	 * 提交申请
	 *
	 * @return
	 */
	@RequestMapping(value = "/submitPromoter", method = RequestMethod.POST)
	public @ResponseBody String submitPromoter(HttpServletRequest request, HttpServletResponse response,UserDetail userDetail) {
		SecurityUserDetail user = UserManager.getUser(request);
		String result = userCommisService.submitPromoter(userDetail,user.getUserId(),user.getUsername());
		return result;

	}

	/**
	 * 我的佣金页面
	 *
	 * @return
	 */
	@RequestMapping(value = "/myCommis", method = RequestMethod.GET)
	public String myCommis(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		UserCommis userCommis = userCommisService.getUserCommis(userId);
		// 查询奖励记录
		int pageSize = 10;
		PageSupport<CommisChangeLog> ps = commisChangeLogService.getCommisChangeLog(pageSize, userId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("userCommis", userCommis);
		return PathResolver.getPath(PromoterFrontPage.MY_COMMIS);
	}

	/**
	 * 搜索奖励记录
	 *
	 * @return
	 */
	@RequestMapping(value = "/searchAward", method = RequestMethod.GET)
	public String searchAwardList(HttpServletRequest request, HttpServletResponse response, String curPageNO, AwardSearchParamDto paramDto) {
		int pageSize = 10;

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		PageSupport<CommisChangeLog> ps = commisChangeLogService.getCommisChangeLog(curPageNO, pageSize, userId, paramDto);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("curPageNO", curPageNO);
		return PathResolver.getPath(PromoterFrontPage.AWARD_LIST);
	}

	/**
	 * go to 申请提现
	 *
	 * @return
	 */
	@RequestMapping(value = "/toApplyCash", method = RequestMethod.GET)
	public String toApplyCash(HttpServletRequest request, HttpServletResponse response) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		UserCommis userCommis = userCommisService.getUserCommis(userId);

		request.setAttribute("userCommis", userCommis);
		return PathResolver.getPath(PromoterFrontPage.APPLY_CASH);
	}

	/**
	 * 处理申请提现
	 *
	 * @return
	 */
	@RequestMapping(value = "/applyCash", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> applyCash(HttpServletRequest request, HttpServletResponse response, @RequestParam Double money, @RequestParam String passwd) {
		Map<String, Object> result = new HashMap<String, Object>();

		SecurityUserDetail securityUserDetail = UserManager.getUser(request);
		
		// 校验密码是否正确
		UserDetail user = userDetailService.getUserDetailById(securityUserDetail.getUserId());
		if (!passwordEncoder.matches(passwd, user.getPayPassword())) {
			result.put("status", Constants.FAIL);
			result.put("msg", "对不起,您输入的密码有误,请重新输入!");
			return result;
		}

		// 处理佣金提现
		result = userCommisService.processWithDraw(securityUserDetail.getUsername(), money);

		return result;
	}

	/**
	 * 查询提现记录
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/withdrawList", method = RequestMethod.GET)
	public String getWithdrawList(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		int pageSize = 10;

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		String logType = PdCashLogEnum.PROMOTER.value();
		PageSupport<PdCashLog> ps = pdCashLogService.getPdCashLog(curPageNO, pageSize, userId, logType);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(PromoterFrontPage.WITH_DRAWLIST); 
	}

	/**
	 * 检查手机号码唯一性
	 * 
	 * @param Phone
	 * @return
	 */
	@RequestMapping("/isPhoneExist")
	public @ResponseBody Boolean isPhoneExist(HttpServletRequest request, HttpServletResponse response, String Phone) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		UserDetail userPo = userDetailService.getUserDetailById(userId);
		boolean flag = userDetailService.isPhoneExist(Phone);
		if (Phone.equals(userPo.getUserMobile()) && flag) {
			return false;
		}
		return flag;
	}
	
	@RequestMapping("/isUserNameExist")
	public @ResponseBody Boolean isUserNameexist(String userName){
		return userDetailService.isUserExist(userName);
	}

	/**
	 * 我推广的会员
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping(value = "/myPromoterUser")
	public String myPromoterUser(HttpServletRequest request, HttpServletResponse response, String curPageNO, UserCommis userCommis) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		
		int pageSize = 10;
		PageSupport<UserCommis> ps = userCommisService.getUserCommis(curPageNO, userName, userCommis, pageSize);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("userCommis", userCommis);
		// 总记录
		request.setAttribute("totalUser", ps.getTotal());
		Date startTime = DateUtils.getMonthStartTime();// 获得开始时间
		Date endTime = new Date();
		// 查询本月发展会员数
		Long monthTotal = userCommisService.getMonthUser(startTime, endTime, userName);
		request.setAttribute("monthTotal", monthTotal);

		UserCommis originUserCommis = userCommisService.getUserCommis(userId);
		request.setAttribute("originUserCommis", originUserCommis);
		return PathResolver.getPath(PromoterFrontPage.MY_PROMOTER_USER); // simple
	}
	
}
