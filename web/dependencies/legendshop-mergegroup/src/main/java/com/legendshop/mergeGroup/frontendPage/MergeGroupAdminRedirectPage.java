/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.mergeGroup.frontendPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 重定向页面定义.
 */
public enum MergeGroupAdminRedirectPage implements PageDefinition {
	
	//拼团轮播图
	MERGER_BANNNER_LIST("/admin/mergeGroupAdvertise/index");
	
	/** The value. */
	private final String value;

	private MergeGroupAdminRedirectPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("redirect:", path);
	}

	/**
	 * 实例化一个新的tiles页面
	 * 
	 * @param value
	 */
	private MergeGroupAdminRedirectPage(String value) {
		this.value = value;
	}

	public String getNativeValue() {
		return value;
	}

}
