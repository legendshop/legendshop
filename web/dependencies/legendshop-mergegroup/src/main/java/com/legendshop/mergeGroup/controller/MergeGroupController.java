/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.mergeGroup.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.helper.VisitHistoryHelper;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.mergeGroup.frontendPage.FrontPage;
import com.legendshop.model.constant.ActiveBannerTypeEnum;
import com.legendshop.model.constant.MergeGroupAddStatusEnum;
import com.legendshop.model.constant.MergeGroupStatusEnum;
import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.constant.VisitSourceEnum;
import com.legendshop.model.constant.VisitTypeEnum;
import com.legendshop.model.dto.MergeGroupingDto;
import com.legendshop.model.dto.ProductDto;
import com.legendshop.model.entity.ActiveBanner;
import com.legendshop.model.entity.MergeGroup;
import com.legendshop.model.entity.MergeGroupOperate;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.VisitLog;
import com.legendshop.processor.VisitLogProcessor;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ActiveBannerService;
import com.legendshop.spi.service.MergeGroupAddService;
import com.legendshop.spi.service.MergeGroupOperateService;
import com.legendshop.spi.service.MergeGroupService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.web.helper.IPHelper;

/**
 * 
 * @Description:拼团
 *
 */
@Controller
public class MergeGroupController {
	
	@Autowired
	private MergeGroupService mergeGroupService;
	
	@Autowired
	private MergeGroupAddService mergeGroupAddService;
	
	@Autowired
	private MergeGroupOperateService mergeGroupOperateService;

	@Autowired
	private ProductService productService;
	
	@Autowired
	private ShopDetailService shopDetailService;
	
	@Autowired
	private ActiveBannerService activeBannerService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	/**
	 * 日志查看
	 */
	@Autowired
	private VisitLogProcessor visitLogProcessor;

	@RequestMapping(value = "/mergeGroup", method = RequestMethod.GET)
	public String mergeGroup(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		List<ActiveBanner> bannerList = activeBannerService.getBannerList(ActiveBannerTypeEnum.MERGER_BANNER.value());
		request.setAttribute("bannerList", bannerList);
		curPageNO = AppUtils.isNotBlank(curPageNO) ? curPageNO : "1";
		PageSupport<MergeGroup> ps = mergeGroupService.getAvailable(curPageNO);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(FrontPage.MERGE_GROUP);
	}

	@RequestMapping(value = "/mergeGroup/list", method = RequestMethod.POST)
	public String mergeGroupList(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		PageSupport<MergeGroup> ps = mergeGroupService.getAvailable(curPageNO);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(FrontPage.MERGE_GROUP_LIST);
	}

	@RequestMapping(value = "/mergeGroup/detail/{mergeGroupId}", method = RequestMethod.GET)
	public String mergeGroupDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable Long mergeGroupId,String addNumber) {
		
		MergeGroup mergeGroup = mergeGroupService.getMergeGroup(mergeGroupId);
		if (AppUtils.isBlank(mergeGroup)) {
			request.setAttribute("message", "活动不存在或已下线");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}
		if (mergeGroup.getStatus() != MergeGroupStatusEnum.ONLINE.value()) {
			request.setAttribute("message", "活动不存在或已下线");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}
		Date nowDate = new Date();
		if(mergeGroup.getEndTime().getTime()<=nowDate.getTime()){
			request.setAttribute("message", "该活动已过期");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}
		
		if(AppUtils.isNotBlank(addNumber)){
			MergeGroupOperate mergeGroupOperate = mergeGroupOperateService.getMergeGroupOperateByAddNumber(addNumber);
			if(AppUtils.isBlank(mergeGroupOperate)){
				request.setAttribute("message", "活动不存在或已下线");
				return PathResolver.getPath(FrontPage.OPERATION_ERROR);
			}
			if(MergeGroupAddStatusEnum.SUCCESS.value()==mergeGroupOperate.getStatus()){
				request.setAttribute("message", "该团已经拼团成功，请选择参加其他团");
				return PathResolver.getPath(FrontPage.OPERATION_ERROR);
			}
			if(MergeGroupAddStatusEnum.FAIL.value()==mergeGroupOperate.getStatus()){
				request.setAttribute("message", "超过拼团有效期，拼团失败，请选择参加其他团");
				return PathResolver.getPath(FrontPage.OPERATION_ERROR);
			}
			
			Calendar c = Calendar.getInstance();
			c.setTime(mergeGroupOperate.getCreateTime());
			c.add(Calendar.DATE, 1);
			if(new Date().getTime()>c.getTime().getTime()){
				request.setAttribute("message", "超过拼团有效期，拼团失败，请选择参加其他团");
				return PathResolver.getPath(FrontPage.OPERATION_ERROR);
			}
			request.setAttribute("addNumber", addNumber);
		}
		
		
		ProductDto prod = productService.getProductDtoForMergrGroup(mergeGroup.getProdId(),mergeGroupId);
		
		if (AppUtils.isBlank(prod)) {
			request.setAttribute("message", "拼团商品已被删除或已被下线");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}
		
		//不能设置空,否则在spring boot环境下js报错
		if(AppUtils.isNotBlank(prod.getSkuDtoList())) {
			String skuDtoList = JSON.toJSONString(prod.getSkuDtoList(),SerializerFeature.DisableCircularReferenceDetect);
			prod.setSkuDtoListJson(skuDtoList);
		}
		
		request.setAttribute("prod", prod);
		
		// 查询店铺的详情信息
		Long shopId = prod.getShopId();
		ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
		
		if (AppUtils.isBlank(shopDetail)) {
			request.setAttribute("message", "该活动所属的店铺已不存在");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}
		
		if (!shopDetail.getStatus().equals(ShopStatusEnum.NORMAL.value())) {
			request.setAttribute("message", "该活动所属的店铺已经被下线或关闭");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}
		
		request.setAttribute("shopDetail", shopDetail);
		// 包邮状态
		Integer mailfeeSts = shopDetail.getMailfeeSts();
		if (mailfeeSts != null && mailfeeSts == 1) {
			// 包邮类型
			Integer mailfeeType = shopDetail.getMailfeeType();
			Double mailfeeCon = shopDetail.getMailfeeCon();
			String mailfeeStr = null;
			if (mailfeeType == 1) {// 满金额包邮
				mailfeeStr = "满" + mailfeeCon + "元包邮";
			} else if (mailfeeType == 2 && AppUtils.isNotBlank(mailfeeCon)) {
				mailfeeStr = "满" + mailfeeCon.intValue() + "件包邮";
			}
			request.setAttribute("mailfeeStr", mailfeeStr);
		}
		
		
		
		// 记录商品访问历史
		VisitHistoryHelper.visit(mergeGroup.getProdId(), request, response);
		// 多线程记录访问历史
		if (systemParameterUtil.isVisitLogEnable()) {
			SecurityUserDetail user = UserManager.getUser(request);
			String userName = user == null ? null : user.getUsername();
			VisitLog visitLog = new VisitLog(IPHelper.getIpAddr(request), userName, shopDetail.getShopId(), shopDetail.getSiteName(), 
					mergeGroup.getProdId(), prod.getName(), prod.getPic(), prod.getCash(), 
					VisitTypeEnum.PROD.value(), VisitSourceEnum.PC.value(), new Date());
			visitLogProcessor.process(visitLog);
			
		}
		
		//获取活动参团列表 前两个
		List<MergeGroupingDto> groupingList = mergeGroupAddService.getMergeGroupingByMergeId(mergeGroup.getId(), 2);
		
		request.setAttribute("groupingList", groupingList);
		request.setAttribute("mergeGroup", mergeGroup);
		request.setAttribute("limitedEndtime", mergeGroup.getEndTime().getTime());
		
		System.out.println(PathResolver.getPath(FrontPage.MERGE_GROUP_DETAIL));
		
		return PathResolver.getPath(FrontPage.MERGE_GROUP_DETAIL);
	}
	
	@RequestMapping(value = "/mergeGroup/addUser/list", method = RequestMethod.GET)
	public String addUserList(HttpServletRequest request, HttpServletResponse response,Long mergeId) {
		
		MergeGroup mergeGroup = mergeGroupService.getMergeGroup(mergeId);
		if (AppUtils.isBlank(mergeGroup)) {
			return "活动不存在，已经下线";
		}
		if (mergeGroup.getStatus() != MergeGroupStatusEnum.ONLINE.value()) {
			return "活动不存在，已经下线";
		}
		Date nowDate = new Date();
		if(mergeGroup.getEndTime().getTime()<=nowDate.getTime()){
			return "该活动已过期";
		}
		request.setAttribute("mergeGroup", mergeGroup);
		
		List<MergeGroupingDto> list = mergeGroupAddService.getMergeGroupingByMergeId(mergeGroup.getId(), null);
		request.setAttribute("list", list);
		request.setAttribute("number", mergeGroup.getPeopleNumber());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		request.setAttribute("enddate",sdf.format(mergeGroup.getEndTime()));
		
		return PathResolver.getPath(FrontPage.MERGE_GROUP_ING_LIST);
	}
	
}
