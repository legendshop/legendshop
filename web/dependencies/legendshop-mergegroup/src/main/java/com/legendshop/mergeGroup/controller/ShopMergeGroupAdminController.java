/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.mergeGroup.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.exception.ErrorCodes;
import com.legendshop.base.exception.NotFoundException;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.mergeGroup.frontendPage.MergeGroupAdminPage;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.MergeGroupDto;
import com.legendshop.model.dto.MergeGroupOperationDto;
import com.legendshop.model.dto.MergeGroupSelProdDto;
import com.legendshop.model.entity.MergeGroup;
import com.legendshop.security.menu.AdminMenuUtil;
import com.legendshop.spi.service.MergeGroupService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

/**
 * 拼团后台管理
 */

@Controller
@RequestMapping("/admin/mergeGroup")
public class ShopMergeGroupAdminController {

	@Autowired
	private MergeGroupService mergeGroupService;
	
	@Autowired
	private ProductService productService;

	@Autowired
	private AdminMenuUtil adminMenuUtil;

	/***
	 * 跳转修改成团订单数界面
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/mergeGroupCount/{id}/{count}")
	public String updateMergeGroupCount(HttpServletRequest request, HttpServletResponse response,@PathVariable Long id,@PathVariable Long count) {
		return PathResolver.getPath(MergeGroupAdminPage.UPDATE_MERGEGROUP_COUNT);
	}
	/***
	 * 修改成团订单数
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="修改成团订单数")
	@RequestMapping(value = "/updateCount")
	@ResponseBody
	public Map<String, Object> updateCount(HttpServletRequest request, HttpServletResponse response,Long id,Long count) {
		Map<String, Object> reslut = new HashMap<String, Object>();
		if(AppUtils.isBlank(id)) {
			reslut.put("status", Constants.FAIL);
			reslut.put("msg", "拼团活动不存在");
			return reslut;
		}
		if(AppUtils.isBlank(count)) {
			reslut.put("status", Constants.FAIL);
			reslut.put("msg", "成团订单数不能为空");
			return reslut;
		}
		MergeGroup mergeGroup=mergeGroupService.getMergeGroup(id);
		mergeGroup.setCount(count);
		mergeGroupService.updateMergeGroup(mergeGroup);
		reslut.put("status", Constants.SUCCESS);
		reslut.put("msg", "修改成功");
		return reslut;
	}
	/**
	 * 拼团活动页面
	 */
	@RequestMapping(value="/query")
	public String mergeGroupList(HttpServletRequest request,HttpServletResponse response,String curPageNO,MergeGroupDto mergeGroup){
		request.setAttribute("mergeGroup", mergeGroup);
		request.setAttribute("date", new Date());
		return PathResolver.getPath(MergeGroupAdminPage.MERGEGROUP_LIST_PAGE);
	}
	
	/**
	 * 拼团活动列表
	 */
	@RequestMapping(value="/queryContent")
	public String mergeGroupContentList(HttpServletRequest request,HttpServletResponse response,String curPageNO,MergeGroupDto mergeGroup){
		Integer pageSize = 10;
		PageSupport<MergeGroupDto> ps = mergeGroupService.getMergeGroupList(curPageNO,pageSize,mergeGroup);
		PageSupportHelper.savePage(request, ps);
		adminMenuUtil.parseMenu(request, 8);
		request.setAttribute("date", new Date());
		return PathResolver.getPath(MergeGroupAdminPage.MERGEGROUP_CONTENT_LIST_PAGE);
	}
	
	/**
     * 更新拼团活动状态
     */
	@SystemControllerLog(description="更新拼团活动状态")
    @RequestMapping(value="/updateStatus/{id}/{status}",method = RequestMethod.GET)
    @ResponseBody
    public String updateStatus(HttpServletRequest request, HttpServletResponse response,@PathVariable Long id, @PathVariable Integer status){
    	if(AppUtils.isBlank(id)){
    		 throw new NotFoundException("stage id is non nullable", ErrorCodes.NON_NULLABLE);
    	}
    	
    	mergeGroupService.updateMergeGroupStatus(id,status);
    	return Constants.SUCCESS;
    }
	
	
	
	
	/**
	 * 活动运营列表
	 */
	@RequestMapping("/operation/{mergeId}")
	public String queryOperationList(HttpServletRequest request, HttpServletResponse response, String curPageNO,@PathVariable Long mergeId){
		Integer pageSize = 10;
        PageSupport<MergeGroupOperationDto> ps = mergeGroupService.getMergeGroupOperationList(curPageNO,pageSize,mergeId,null);
        PageSupportHelper.savePage(request, ps);
        request.setAttribute("mergeId", mergeId);
		return PathResolver.getPath(MergeGroupAdminPage.MERGEGROYP_OPERATION_LIST_PAGE);
	}
	
	/**
	 * 查看活动详情，不允许修改
	 */
	@RequestMapping("/mergeGroupDetail/{id}")
	public String mergeGroupDetail(HttpServletRequest request, HttpServletResponse response,@PathVariable Long id) {
		MergeGroup mergeGroup = mergeGroupService.getMergeGroupByshopId(id);
		if(AppUtils.isBlank(mergeGroup)){
			throw new BusinessException("对不起,该活动不存在，请返回重试");
		}
		request.setAttribute("mergeGroup", mergeGroup);
		List<MergeGroupSelProdDto> prodLists = productService.queryMergeGroupProductByMergeId(mergeGroup.getId());
		request.setAttribute("prodLists", prodLists);
		System.out.println(JSONUtil.getJson(prodLists));
		return PathResolver.getPath(MergeGroupAdminPage.MERGE_GROUP_DETAIL);
	}
}
