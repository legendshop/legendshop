/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.mergeGroup.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.spi.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.mergeGroup.frontendPage.FrontPage;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.MergeGroupAddStatusEnum;
import com.legendshop.model.constant.MergeGroupStatusEnum;
import com.legendshop.model.constant.SubmitOrderStatusEnum;
import com.legendshop.model.dto.MergeGroupJoinDto;
import com.legendshop.model.dto.OrderAddParamDto;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.order.AddOrderMessage;
import com.legendshop.model.dto.order.OrderDetailsParamsDto;
import com.legendshop.model.entity.Invoice;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.processor.DelayCancelOfferProcessor;
import com.legendshop.processor.ProdSnapshotProcessor;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;

/**
 * 
 * for手机端 拼团订单
 *
 */
@Controller
public class MergeGroupOrderController {
	
	private final Logger log = LoggerFactory.getLogger(MergeGroupOrderController.class);
	
	/** 订单处理器封装器 */
	@Autowired
	private OrderCartResolverManager orderCartResolverManager;
	
	@Autowired
	private MergeGroupOperateService mergeGroupOperateService;
	
	@Autowired
	private SubService subService;

	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private InvoiceService invoiceService;
	
	// 订单常用处理工具类
	@Autowired
	private OrderUtil orderUtil;
	
	@Autowired
	private UserAddressService userAddressService;
	
	@Autowired
	private ShopDetailService shopDetailService;
	
	/** 订单延迟取消队列 **/
	@Autowired
	private DelayCancelOfferProcessor delayCancelOfferProcessor;
	
	/** 订单快照 **/
	@Autowired
	private ProdSnapshotProcessor prodSnapshotProcessor;
	 
	
	@RequestMapping(value = "/p/mergeGroup/orderDetails", method = RequestMethod.GET)
	public String orderDetails(HttpServletRequest request, HttpServletResponse response,OrderDetailsParamsDto reqParams) {
		String type = reqParams.getType();
		if (!CartTypeEnum.MERGE_GROUP.toCode().equals(type)) {
			request.setAttribute("message", "操作失败，请刷新重试");
			return PathResolver.getPath(FrontPage.OPERATION_ERROR);
		}
		
		CartTypeEnum typeEnum = CartTypeEnum.fromCode(type);
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		
		// 参团订单需要判断，校检是否成团人数是否满足
		if (AppUtils.isNotBlank(reqParams.getAddNumber())) {
			MergeGroupJoinDto mergeGroupOperate = mergeGroupOperateService.getMergeGroupOperateByAddNumber(reqParams.getActiveId(), reqParams.getAddNumber());
			if (AppUtils.isBlank(mergeGroupOperate)) {
				request.setAttribute("message", "该拼团活动不存在，请返回重试");
				return PathResolver.getPath(FrontPage.OPERATION_ERROR);
			}
			if (mergeGroupOperate.getMergeStatus() != MergeGroupStatusEnum.ONLINE.value() || mergeGroupOperate.getEndTime().getTime() < new Date().getTime()) {
				request.setAttribute("message", "该拼团活动下线或已过期，请返回重试");
				return PathResolver.getPath(FrontPage.OPERATION_ERROR);
			}
			if (mergeGroupOperate.getOperateStatus() != MergeGroupAddStatusEnum.IN.value() || mergeGroupOperate.getPeopleNumber() <= mergeGroupOperate.getNumber()) {// 超过成团数量，不允许下单
				request.setAttribute("message", "该团已经拼团成功，请返回开团或重新参团");
				return PathResolver.getPath(FrontPage.OPERATION_ERROR);
			}
		}
		
		/* 根据类型查询订单列表 */
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("mergeGroupId", reqParams.getActiveId());
		params.put("productId", reqParams.getProdId());
		params.put("skuId", reqParams.getSkuId());
		params.put("number", reqParams.getCount());
		params.put("userId", userId);
		params.put("userName", userName);

		UserShopCartList shopCartList = orderCartResolverManager.queryOrders(typeEnum, params);
		if (AppUtils.isBlank(shopCartList)) {
			return PathResolver.getRedirectPath("/mergeGroup/detail/" + reqParams.getActiveId());
		}
		if (shopCartList == null || AppUtils.isBlank(shopCartList.getShopCarts())) {
			return PathResolver.getRedirectPath("/mergeGroup/detail/" + reqParams.getActiveId());
		}
		shopCartList.setUserId(userId);
		shopCartList.setUserName(userName);
		
		// 拼团订单需要将拼团活动编号添加addNumber
		shopCartList.setAddNumber(reqParams.getAddNumber());

		// 查出买家的默认发票内容
		Invoice userdefaultInvoice = invoiceService.getDefaultInvoice(userId);
		shopCartList.setDefaultInvoice(userdefaultInvoice);
		
		request.setAttribute("userShopCartList", shopCartList);
		request.setAttribute("orderDetailsParam", reqParams);
		
		request.setAttribute("userdefaultInvoice", userdefaultInvoice);
		
		//获取卖家是否开启发票功能
		Long shopId = shopCartList.getShopCarts().get(0).getShopId();
	    ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
	    
	    if (AppUtils.isBlank(shopDetail.getSwitchInvoice())) {
	    	Integer switchInvoice = 0;
	    	request.setAttribute("switchInvoice",switchInvoice);
		}
	    request.setAttribute("switchInvoice", shopDetail.getSwitchInvoice());
	    request.setAttribute("shopId", shopId);
		
		// 设置订单提交令牌,防止重复提交
		Integer token = CommonServiceUtil.generateRandom();
		request.getSession().setAttribute(Constants.TOKEN, token);
		subService.putUserShopCartList(typeEnum.toCode(), userId, shopCartList);
		
		return PathResolver.getPath(FrontPage.MERGE_GROUP_ORDER);

	}
	
	
	@RequestMapping(value="/p/mergeGroup/submitOrder",method=RequestMethod.POST)
	public @ResponseBody AddOrderMessage submitOrder(HttpServletRequest request, HttpServletResponse response,OrderAddParamDto paramDto) {
		
		//参数检查
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();

		//判断是否重复提交
		Integer sessionToken = (Integer) request.getSession().getAttribute(Constants.TOKEN); // 取session中保存的token
		
		String type=(paramDto.getType()==null?"MERGE_GROUP":paramDto.getType());
		
		CartTypeEnum typeEnum=CartTypeEnum.fromCode(type);
		AddOrderMessage message= orderUtil.checkSubmitParam(userId, sessionToken, typeEnum, paramDto);//检查输入参数
		if(message.hasError()){//有错误就返回
			return message;
		}

		// TODO 校验用户参数，SpringSession后台过期机制有问题
		if (!userDetailService.isUserExist(userName)) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("用户不存在！");
			return message;
		}
		
		//get cache 从上个页面获取设置进入缓存的订单
		UserShopCartList userShopCartList=subService.findUserShopCartList(type,userId);
		if(AppUtils.isBlank(userShopCartList)){
			message.setCode(SubmitOrderStatusEnum.NOT_PRODUCTS.value());
			return message;
		}
		//判断用户是否符合拼团资格，如活动限购
		/*boolean status = mergeGroupAddService.joinMergeGroup(userShopCartList.getActiveId(),userId);
		if (!status) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("您的开团次数已超出该活动限制，请参加其他活动");
			return message;
		}*/
		if (AppUtils.isNotBlank(userShopCartList.getAddNumber())) {// 参团订单需要判断，校检是否成团人数是否满足
			
			// 校检用户是否参加了该团
			Long count = mergeGroupOperateService.getMergeGroupAddByUserId(userId, userShopCartList.getAddNumber());
			if (count > 0) {
				message.setCode(SubmitOrderStatusEnum.ERR.value());
				message.setMessage("您已参加该团，请选择参加其它拼团活动");
				return message;
			}
			MergeGroupJoinDto mergeGroupOperate = mergeGroupOperateService.getMergeGroupOperateByAddNumber(userShopCartList.getActiveId(), userShopCartList.getAddNumber());
			if (AppUtils.isBlank(mergeGroupOperate)) {
				message.setCode(SubmitOrderStatusEnum.ERR.value());
				message.setMessage("该拼团活动不存在，请返回重试");
				return message;
			}
			if (mergeGroupOperate.getMergeStatus() != MergeGroupStatusEnum.ONLINE.value() || mergeGroupOperate.getEndTime().getTime() < new Date().getTime()) {
				message.setCode(SubmitOrderStatusEnum.ERR.value());
				message.setMessage("该拼团活动下线或已过期，请返回重试");
				return message;
			}
			if (mergeGroupOperate.getOperateStatus() != MergeGroupAddStatusEnum.IN.value() || mergeGroupOperate.getPeopleNumber() <= mergeGroupOperate.getNumber()) {// 超过成团数量，不允许下单
				message.setCode(SubmitOrderStatusEnum.ERR.value());
				message.setMessage("该团已经拼团成功，请返回开团或重新参团");
				return message;
			}
		}
		
		
		
		//检查金额： 如果促销导致订单金额为负数
	    if(userShopCartList.getOrderActualTotal()< 0){
	    	message.setCode(SubmitOrderStatusEnum.ERR.value());
	    	message.setMessage("订单金额为负数,下单失败!");
			return message;
	    }
	    
	  //处理订单的卖家留言备注
		try {
			orderUtil.handlerRemarkText(paramDto.getRemarkText(), userShopCartList);
		} catch (Exception e) {
			log.error("",e);
			message.setCode(SubmitOrderStatusEnum.PARAM_ERR.value());
			message.setMessage("下单失败，请联系商城管理员或商城客服");
			return message;
		}

		/** 根据id获取收货地址信息 **/
		UserAddress userAddress = userShopCartList.getUserAddress();
		if (AppUtils.isBlank(userAddress)) {
			userAddress = userAddressService.getDefaultAddress(userId);
			if (AppUtils.isBlank(userAddress)) {
				message.setCode(SubmitOrderStatusEnum.NO_ADDRESS.value());
				return message;
			}
			userShopCartList.setUserAddress(userAddress);
		}
			
		// 处理运费模板
		String result = orderUtil.handlerDelivery(paramDto.getDelivery(), userShopCartList);
		if (!Constants.SUCCESS.equals(result)) {
			message.setCode(result);
			return message;
		}
			
		// 处理优惠券
		orderUtil.handleCouponStr(userShopCartList);

		// 验证成功，清除令牌
		request.getSession().removeAttribute(Constants.TOKEN);
		userShopCartList.setUserId(userId);
		userShopCartList.setUserName(userName);
		userShopCartList.setInvoiceId(paramDto.getInvoiceId());
		userShopCartList.setUserAddress(userAddress);
		userShopCartList.setPayManner(paramDto.getPayManner());
		userShopCartList.setType(typeEnum.toCode());
		userShopCartList.setSwitchInvoice(paramDto.getSwitchInvoice());//当前订单所属商家是否开启发票

		/**
		 * 处理订单
		 */
		List<String> subIds = null;
		try {
			subIds = orderCartResolverManager.addOrder(typeEnum,userShopCartList);
		} catch (Exception e) {
			log.error(e.getMessage());
			if (e instanceof BusinessException) {
				message.setCode(SubmitOrderStatusEnum.ERR.value());
				message.setMessage(e.getMessage());
				return message;
			}
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage(e.getMessage());
			return message;
		}
		if (AppUtils.isBlank(subIds)) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("下单失败,请联系商城管理员或商城客服");
			return message;
		}else{
			
	        //offer 延时取消队列
	  		delayCancelOfferProcessor.process(subIds);
	        
			//发送网站快照备份
			prodSnapshotProcessor.process(subIds);
			
		}

		// 标记回收
		userShopCartList = null;
		// 更新session购物车数量
		subService.evictUserShopCartCache(type, userId);
		StringBuffer url = new StringBuffer();
		url.append("/p/orderPay?subNums=");
		for (int i = 0; i < subIds.size(); i++) {
			url.append(subIds.get(i)).append(",");
		}
		String path = url.substring(0, url.length() - 1);
		message.setResult(true);
		message.setUrl(path);
		return message;
	}
	

}
