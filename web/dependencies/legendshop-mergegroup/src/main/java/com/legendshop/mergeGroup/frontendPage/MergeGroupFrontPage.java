package com.legendshop.mergeGroup.frontendPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

public enum MergeGroupFrontPage implements PageDefinition {

	CONSUMER_INFO("/consumerInfo"),

	CONSUMER_INFO_LIST("/consumerInfoList"),

	CONSUMER_ORDER_LIST("/consumeOrders"),

	MERGE_GROUP_DETAIL("/mergeGroupDetail"),

	MERGE_GROUP_SHOW("/mergeShowGroup"),

	MERGE_GROUP_EDIT("/mergeGroupEdit"),

	MERGE_GROUP_EDIT_PROD("/mergeGroupProd"),

	MERGE_GROUP_EDIT_PROD_LIST("/mergeGroupProdList"),

	MERGE_GROUP_LIST("/mergeGroupList"),

	MERGE_GROUP_OPERATION("/mergeGroupOperation"),

	MERGE_GROUP_SKU("/mergeGroupSku"),

	MERGE_GROUP_SKULIST("/mergeGroupSkuList"),

	/* 拼团选择商品窗口页面 */
	MERGE_GROUP_PROD_LAYOUT("/mergeGroupProdLayout"),

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	;

	/** The value. */
	private final String value;


	private MergeGroupFrontPage(String value) {
		this.value = value;
	}

	public String getValue() {
		return getValue(value);
	}

	public String getNativeValue() {
		return value;
	}

	public String getValue(String path) {
		return PagePathCalculator.calculatePluginFronendPath("mergeGroup", path);
	}

}
