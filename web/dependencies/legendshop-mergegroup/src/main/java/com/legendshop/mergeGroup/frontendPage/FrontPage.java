/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.mergeGroup.frontendPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * The Enum MobileFrontPage.
 */
public enum FrontPage implements PageDefinition {
	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	MERGE_GROUP("/mergeGroup/mergeGroup"),

	MERGE_GROUP_LIST("/mergeGroup/mergeGroupList"),

	MERGE_GROUP_DETAIL("/mergeGroup/mergeGroupDetail"),
	
	MERGE_GROUP_ING_LIST("/mergeGroup/mergeIngList"),
	
	MERGE_GROUP_ORDER("/mergeGroup/mergeOrderDetail"),
	
	/** 操作错误页面 */
	OPERATION_ERROR("/operationError"),
	
	;

	/** The value. */
	private final String value;

	/**
	 * Instantiates a new front page.
	 * 
	 * @param value
	 *            the value
	 * @param template
	 *            the template
	 */
	private FrontPage(String value, String... template) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	@Override
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.String, java.util.List)
	 */
	@Override
	public String getValue(String path) {
		return PagePathCalculator.calculateMobileFronendPath(path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	@Override
	public String getNativeValue() {
		return value;
	}

}
