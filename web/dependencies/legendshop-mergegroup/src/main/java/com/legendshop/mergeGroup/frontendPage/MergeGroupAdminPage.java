package com.legendshop.mergeGroup.frontendPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

public enum MergeGroupAdminPage implements PageDefinition {
	
	/** 拼团活动 **/
	MERGEGROUP_LIST_PAGE("/mergeGroup/mergeGroupList"),
	
	/** 拼团活动列表 **/
	MERGEGROUP_CONTENT_LIST_PAGE("/mergeGroup/mergeGroupContentList"),
	
	/**修改成团订单数**/
	UPDATE_MERGEGROUP_COUNT("/mergeGroup/updatemergeGroupCount"),
	
	/** 拼团运营列表 **/
	MERGEGROYP_OPERATION_LIST_PAGE("/mergeGroup/mergeGroupOperation"),
	
	/** 拼团详情 **/
	MERGE_GROUP_DETAIL("/mergeGroup/mergeGroupDetail"),
	
	/** 拼团广告管理 */
	MERGE_GROUP_ADV("/mergeGroupAd/ad"),
	
	/** 新增拼团广告 */
	MERGE_GROUP_ADV_ADD("/mergeGroupAd/addAdverties"), 
	
	;

	
	private final String value;

	private MergeGroupAdminPage(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return getValue(value);
	}

	public String getNativeValue() {
		return value;
	}

	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("mergeGroup", path);
	}

}
