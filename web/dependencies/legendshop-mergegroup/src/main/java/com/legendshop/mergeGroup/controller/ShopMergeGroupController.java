/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.mergeGroup.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.mergeGroup.frontendPage.MergeGroupFrontPage;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.MergeGroupStatusEnum;
import com.legendshop.model.constant.SkuActiveTypeEnum;
import com.legendshop.model.dto.MergeGroupDto;
import com.legendshop.model.dto.MergeGroupOperationDto;
import com.legendshop.model.dto.MergeGroupSelProdDto;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.entity.MergeGroup;
import com.legendshop.model.entity.MergeProduct;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.Sku;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.MergeGroupService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.SkuService;
import com.legendshop.spi.service.SubService;
import com.legendshop.util.AppUtils;

/**
 * 拼团活动控制器
 *
 */
@Controller
@RequestMapping("/s/mergeGroupActivity")
public class ShopMergeGroupController {

	@Autowired
	private MergeGroupService mergeGroupService;

	@Autowired
	private ProductService productService;
	
	@Autowired
	private SkuService skuService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;
	

	/**
	 * 拼团活动列表
	 */
	@RequestMapping("/query")
	@Authorize(authorityTypes = AuthorityType.MERGE_GROUP_MANAGE)
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, MergeGroupDto mergeGroup) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		mergeGroup.setShopId(shopId);
		PageSupport<MergeGroupDto> ps = mergeGroupService.getMergeGroupList(curPageNO, 10, mergeGroup);

		PageSupportHelper.savePage(request, ps);
		request.setAttribute("mergeGroup", mergeGroup);
		request.setAttribute("date", new Date());

		return PathResolver.getPath(MergeGroupFrontPage.MERGE_GROUP_LIST);
	}

	/**
	 * 添加拼团活动
	 */
	@RequestMapping("/save")
	@Authorize(authorityTypes = AuthorityType.MERGE_GROUP_MANAGE)
	public String save(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(MergeGroupFrontPage.MERGE_GROUP_EDIT);
}


	/**
	 * 编辑 TODO
	 */
	@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	@Authorize(authorityTypes = AuthorityType.MERGE_GROUP_MANAGE)
	public String update(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		MergeGroup mergeGroup = mergeGroupService.getMergeGroupByshopId(id, shopId);
		if (AppUtils.isBlank(mergeGroup)) {
			throw new BusinessException("对不起,该活动不存在，请返回重试");
		}
		request.setAttribute("mergeGroup", mergeGroup);
		List<MergeGroupSelProdDto> prodLists = productService.queryMergeGroupProductByMergeId(mergeGroup.getId());
		request.setAttribute("prodLists", prodLists);

		return PathResolver.getPath(MergeGroupFrontPage.MERGE_GROUP_EDIT);
	}

	/**
	 * 保存拼团活动
	 * 
	 * @throws IOException
	 */
	@RequestMapping("/saveCommit")
	@ResponseBody
	public String saveCommit(HttpServletRequest request, HttpServletResponse response, MergeGroup mergeGroup,@RequestParam(required = false) MultipartFile file) throws IOException {
		SecurityUserDetail user = UserManager.getUser(request);

		if (AppUtils.isBlank(mergeGroup)) {
			return "参数错误";
		}
		List<MergeProduct> mergeProductList = mergeGroup.getMergeProductList();
		if (AppUtils.isBlank(mergeProductList) || mergeProductList.size() == 0) {
			return "活动商品不允许为空";
		}
		if (AppUtils.isNotBlank(mergeGroup.getId())){
            MergeGroup merge = mergeGroupService.getMergeGroup(mergeGroup.getId());
            merge.setMergeName(mergeGroup.getMergeName());
            merge.setStartTime(mergeGroup.getStartTime());
            merge.setEndTime(mergeGroup.getEndTime());
            merge.setPeopleNumber(mergeGroup.getPeopleNumber());
            merge.setIsHeadFree(merge.getIsHeadFree());
            merge.setMergeProductList(mergeGroup.getMergeProductList());
			merge.setStatus(MergeGroupStatusEnum.NEED_AUDITED.value());
			mergeGroupService.updateMergeGroupAndProd(merge,mergeProductList,file,user.getUsername(), user.getUserId(), user.getShopId());
			return "修改成功！";
        }else {
			String mergeNumber = CommonServiceUtil.getRandomSn();
			mergeGroup.setProdId(mergeProductList.get(0).getProdId());
			mergeGroup.setMergeNumber(mergeNumber);
			mergeGroup.setStatus(MergeGroupStatusEnum.NEED_AUDITED.value());
			mergeGroup.setShopId(user.getShopId());
			mergeGroup.setCreateTime(new Date());
			mergeGroup.setCount(0l);
			mergeGroup.setDeleteStatus(0);
			//默认不限售
			if (AppUtils.isBlank(mergeGroup.getIsLimit())) {
				mergeGroup.setIsLimit(false);
			}
			if (AppUtils.isBlank(mergeGroup.getLimitNumber())) {
				mergeGroup.setLimitNumber(0l);
			}

			//成团倒计时长
			Integer countdown = systemParameterUtil.getGroupSurvivalTime();
			if(null == countdown){
				countdown = 24;
			}
			mergeGroup.setCountdown(countdown);

			mergeGroupService.saveMergeGroupAndProd(mergeGroup, mergeProductList,file,user.getUsername(), user.getUserId(), user.getShopId());
			return "添加成功！";
        }
	}

	/**
	 * 选择商品
	 *//*
	@RequestMapping("/selProd")
	public String selProd(HttpServletRequest request, HttpServletResponse response) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<Product> ps = productService.queryMergeGroupProduct(shopId);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(MergeGroupFrontPage.MERGE_GROUP_EDIT_PROD);
	}
*/


	/**
	 * 异步选择商品
	 *//*
	@RequestMapping("/selProdList")
	public String selProdList(HttpServletRequest request, HttpServletResponse response, String curPageNO, String prodName) {
		curPageNO = AppUtils.isNotBlank(curPageNO) ? curPageNO : "1";

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<Product> ps = productService.queryMergeGroupProductList(shopId, curPageNO, prodName);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(MergeGroupFrontPage.MERGE_GROUP_EDIT_PROD_LIST);
	}*/

	/**
	 * 新选择商品弹框
	 */
	@RequestMapping("/mergeGroupProdLayout")
	public String selProdList(HttpServletRequest request, HttpServletResponse response, String curPageNO, String prodName) {
		curPageNO = AppUtils.isNotBlank(curPageNO) ? curPageNO : "1";

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();

		PageSupport<Product> ps = productService.queryMergeGroupProductList(shopId, curPageNO, prodName);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(MergeGroupFrontPage.MERGE_GROUP_PROD_LAYOUT);
	}


	@RequestMapping("/getProdToSku")
	public String getProdToSku(HttpServletRequest request, HttpServletResponse response, @RequestBody Long[] prodIds) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		List<MergeGroupSelProdDto> prodLists = productService.queryMergeGroupProductToSku(shopId, prodIds);
		request.setAttribute("prodLists", prodLists);
		return PathResolver.getPath(MergeGroupFrontPage.MERGE_GROUP_SKU);
	}

	@RequestMapping("/getProdToSkuList")
	@ResponseBody
	public List<MergeGroupSelProdDto> getProdToSkuList(HttpServletRequest request, HttpServletResponse response, @RequestBody Long[] prodIds) {

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		List<MergeGroupSelProdDto> prodLists = productService.queryMergeGroupProductToSku(shopId, prodIds);
		return prodLists;
	}

	/**
	 * 查看活动详情，不允许修改
	 */
	@RequestMapping("/mergeGroupDetail/{id}")
	public String mergeGroupDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		MergeGroup mergeGroup = mergeGroupService.getMergeGroupByshopId(id, shopId);
		if (AppUtils.isBlank(mergeGroup)) {
			throw new BusinessException("对不起,该活动不存在，请返回重试");
		}
		request.setAttribute("mergeGroup", mergeGroup);
		List<MergeGroupSelProdDto> prodLists = productService.queryMergeGroupProductByMergeId(mergeGroup.getId());
		request.setAttribute("prodLists", prodLists);
		return PathResolver.getPath(MergeGroupFrontPage.MERGE_GROUP_SHOW);
	}

	/**
	 * 终止拼团活动
	 */
	@RequestMapping(value = "/updateStatus/{id}/{status}", method = RequestMethod.POST)
	@ResponseBody
	public String updateStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @PathVariable Integer status) {
		
		MergeGroup mergeGroup = mergeGroupService.getMergeGroup(id);
		if (AppUtils.isBlank(mergeGroup)) {
			
			return "终止失败，该活动不存在或已被删除";
		}
		mergeGroupService.updateMergeGroupStatus(id, status);
		return Constants.SUCCESS;

	}

	/**
	 * 活动运营列表
	 */
	@RequestMapping("/operation/{mergeId}")
	public String queryOperationList(HttpServletRequest request, HttpServletResponse response, String curPageNO, @PathVariable Long mergeId) {
		Integer pageSize = 10;

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		// 获取运营结果统计数据
		OperateStatisticsDTO operateStatistics = mergeGroupService.getOperateStatistics(mergeId,shopId);
		
		PageSupport<MergeGroupOperationDto> ps = mergeGroupService.getMergeGroupOperationList(curPageNO, pageSize, mergeId, shopId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("mergeId", mergeId);
		request.setAttribute("date", new Date());
		request.setAttribute("operateStatistics",operateStatistics);
		return PathResolver.getPath(MergeGroupFrontPage.MERGE_GROUP_OPERATION);
	}

	/**
	 * 删除拼团活动列表
	 */
	@RequestMapping("/delete/{id}")
	@ResponseBody
	@Authorize(authorityTypes = AuthorityType.MERGE_GROUP_MANAGE)
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		
		MergeGroup mergeGroup = mergeGroupService.getMergeGroupByshopId(id);
		
		if (AppUtils.isBlank(mergeGroup)) {
			return "该活动不存在或已被删除";
		}
		
		//查询待删除的拼团活动是否有：待成团的拼团
		Integer i = mergeGroupService.findHaveInMergeGroupOperate(id);
		if( i > 0 ){
			return "该活动存在拼团记录，不能删除";
		}else{

			SecurityUserDetail user = UserManager.getUser(request);
			Long shopId = user.getShopId();
			
			mergeGroupService.deleteMergeGroupByShopId(shopId, id);
			return Constants.SUCCESS;
		}
		
	}
	
	
	/**
	 * 更新拼团活动删除状态（逻辑删除 ）
	 */
	@RequestMapping("/updateDeleteStatus/{id}")
	@ResponseBody
	@Authorize(authorityTypes = AuthorityType.MERGE_GROUP_MANAGE)
	public String updateDeleteStatus(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		
		MergeGroup mergeGroup = mergeGroupService.getMergeGroupByshopId(id);
		
		if (AppUtils.isBlank(mergeGroup)) {
			return "该活动不存在或已被删除";
		}
			
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		Integer deleteStatus = 1;
		mergeGroupService.updateDeleteStatus(shopId, id,deleteStatus);
		return Constants.SUCCESS;
		
	}


	
	/**
	 * 是否参加其他活动
	 * @return 返回'false' 表示该商品没有参加营销活动
	 */
	@RequestMapping("/isAttendActivity/{productId}")
	@ResponseBody
	public String join(HttpServletRequest request, HttpServletResponse response, @PathVariable Long productId) {
		//拼团时所有sku都会参加拼团活动的，所以只要商品有一个sku参加了其他活动，都不能参加拼团活动
		List<Sku> skus = skuService.getSkuListByProdId(productId);
		for (Sku sku : skus) {
			if(!sku.getSkuType().equals(SkuActiveTypeEnum.PRODUCT.value())) {
				return SkuActiveTypeEnum.getSkuTypeMes(sku.getSkuType());
			}
		}
		return "false";
	}

/**
 * 选择商品
 */
	@RequestMapping("/selProd")
	public String selProd(HttpServletRequest request, HttpServletResponse response,Long prodId) {
		List<Sku> skuList = skuService.getSkuByProd(prodId);
		request.setAttribute("skuList",skuList);
		return PathResolver.getPath(MergeGroupFrontPage.MERGE_GROUP_SKULIST);
	}
}
