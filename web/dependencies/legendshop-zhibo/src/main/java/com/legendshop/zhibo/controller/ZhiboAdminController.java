/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.zhibo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.zhibo.ZhiboRoomMemberDto;
import com.legendshop.model.dto.zhibo.ZhiboRoomProdDto;
import com.legendshop.model.entity.ZhiboNewLiveRecord;
import com.legendshop.spi.service.ZhiboInteractAvRoomService;
import com.legendshop.spi.service.ZhiboNewLiveRecordService;
import com.legendshop.spi.service.ZhiboProdService;
import com.legendshop.zhibo.adminPage.ZhiboAdminPage;

/**
 * 直播控制器.
 */
@Controller
@RequestMapping("/admin/zhibo")
public class ZhiboAdminController {

	@Autowired
	private ZhiboNewLiveRecordService zhiboNewLiveRecordService;

	@Autowired
	private ZhiboInteractAvRoomService zhiboInteractAvRoomService;

	@Autowired
	private ZhiboProdService zhiboProdService;

	/**
	 * 获取直播房间列表
	 *
	 * @param request the request
	 * @param response the response
	 * @param curPageNO the cur page no
	 * @param avRoomId the av room id
	 * @return the string
	 */
	@RequestMapping(value = "/zhiboRoomList", method = RequestMethod.GET)
	public String zhiboRoomList(HttpServletRequest request, HttpServletResponse response, String curPageNO, String avRoomId) {
		PageSupport<ZhiboNewLiveRecord> ps = zhiboNewLiveRecordService.getRoom(curPageNO, avRoomId);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("avRoomId", avRoomId);
		return PathResolver.getPath(ZhiboAdminPage.ZHIBO_ROOM); 
	}

	/**
	 * 获取直播房间商品
	 *
	 * @param request the request
	 * @param response the response
	 * @param curPageNO the cur page no
	 * @param prodName the prod name
	 * @param avRoomId the av room id
	 * @return the string
	 */
	@RequestMapping(value = "/zhiboRoomProd", method = RequestMethod.GET)
	public String zhiboRoomProd(HttpServletRequest request, HttpServletResponse response, String curPageNO, String prodName, String avRoomId) {
		PageSupport<ZhiboRoomProdDto> ps = zhiboProdService.queryZhiboRoomProd(curPageNO, avRoomId, prodName);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prodName", prodName);
		request.setAttribute("avRoomId", avRoomId);
		return PathResolver.getPath(ZhiboAdminPage.ZHIBO_ROOM_PROD); 
	}

	/**
	 * 直播房间下线.
	 *
	 * @param request the request
	 * @param response the response
	 * @param avRoomId the av room id
	 * @param hostUid the host uid
	 * @return the string
	 */
	@RequestMapping(value = "/zhiboRoomOutline/{avRoomId}/{hostUid}", method = RequestMethod.GET)
	public String outLineRoom(HttpServletRequest request, HttpServletResponse response, @PathVariable String avRoomId, @PathVariable String hostUid) {
		if (avRoomId.isEmpty() || "".equals(avRoomId) || hostUid.isEmpty() || "".equals(hostUid)) {
			return Constants.FAIL;
		}
		// 将用户hostUid的直播记录删除。一个用户同一时间只能开启一个直播,成功则返回1
		zhiboNewLiveRecordService.deleteUid(hostUid, Integer.valueOf(avRoomId));

		// 清空房间成员,用于直播结束清空房间成员；成功则返回1
		zhiboNewLiveRecordService.clearRoom(Integer.valueOf(avRoomId));
		return Constants.SUCCESS;
	}

	/**
	 * 获取直播房间成员
	 *
	 * @param request the request
	 * @param response the response
	 * @param curPageNO the cur page no
	 * @param userName the user name
	 * @param avRoomId the av room id
	 * @return the string
	 */
	@RequestMapping(value = "/zhiboRoomMember", method = RequestMethod.GET)
	public String zhiboRoomMember(HttpServletRequest request, HttpServletResponse response, String curPageNO, String userName, String avRoomId) {

		PageSupport<ZhiboRoomMemberDto> ps = zhiboProdService.queryZhiboRoomProdq(curPageNO, avRoomId, userName);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("avRoomId", avRoomId);
		request.setAttribute("userName", userName);
		return PathResolver.getPath(ZhiboAdminPage.ZHIBO_ROOM_MEMBER); 
	}

	/**
	 * 踢人.
	 *
	 * @param request the request
	 * @param response the response
	 * @param avRoomId the av room id
	 * @param uid the uid
	 * @return the string
	 */
	@RequestMapping(value = "/zhiboRoomMemberKicking/{avRoomId}/{uid}", method = RequestMethod.GET)
	public String zhiboRoomMemberKicking(HttpServletRequest request, HttpServletResponse response, @PathVariable String avRoomId, @PathVariable String uid) {

		if (avRoomId.isEmpty() || "".equals(avRoomId) || uid.isEmpty() || "".equals(uid)) {
			return Constants.FAIL;
		}
		// 将用户hostUid从房间移除
		zhiboInteractAvRoomService.kickingMemberByUid(uid, Integer.valueOf(avRoomId));

		return Constants.SUCCESS;
	}
}
