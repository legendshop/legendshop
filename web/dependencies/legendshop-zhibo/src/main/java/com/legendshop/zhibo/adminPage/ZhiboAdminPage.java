/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.zhibo.adminPage;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * The Enum BackPage.
 */
public enum ZhiboAdminPage implements PageDefinition {

	/** The variable. */
	VARIABLE(""),

	/** 直播房间列表 */
	ZHIBO_ROOM("/zhiboRoom"),

	/** 直播房间商品 */
	ZHIBO_ROOM_PROD("/zhiboRoomProd"),

	/** 直播房间成员 */
	ZHIBO_ROOM_MEMBER("/zhiboRoomMember"),

	;

	/** The value. */
	private final String value;

	/**
	 * 实例化一个新的后台页面
	 *
	 * @param value
	 *            the value
	 */
	private ZhiboAdminPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("zhibo", path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
