/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.presell.page.front;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 用户中心前台页面
 */
public enum MobileFrontPage implements PageDefinition {

	/** The VARIABLE. 可变路径 */
	VARIABLE(""),

	/** 商家预售商品管理列表 */
	S_PRESELLPROD_LIST_PAGE("/shop/presellProd/shopPresellProdList"),

	/** 预售商品管理列表内容 */
	S_PRESELLPROD_LIST_CONTENT("/shop/presellProd/listContent"),

	/** 商家预售商品编辑页面(添加,修改) */
	S_PRESELLPROD_EDIT_PAGE("/shop/presellProd/shopPresellProdEdit"),

	/** 商家查看预售商品详情页面 */
	S_PRESELLPROD_DETAIL_PAGE("/shop/presellProd/shopPresellProdDetail"),

	/** 商家预售商品添加商品SKU弹层 */
	S_PRESELLPROD_PROD_SKU_LAYOUT("/shop/presellProd/addProdSkuLayout"),

	/** 商家预售商品加载当前商品SKU列表 */
	S_PRESELLPROD_LOAD_PROD_SKU_LIST("/shop/presellProd/loadProdSkuList"),

	/** 预售商品详情页面 */
	U_PRESELLPROD_DETAIL("/user/presellProd/presellProdDetail"),

	/** 生成预售商品订单页面 */
	U_PRESELL_GENERATE_ORDER("/user/presellSub/presellGenerateOrder"),

	/** 预售商品订单详情 */
	ORDERDETAILS("/user/presellOrderDetail"),
	

	/** 支付预售定金 */
	PRESELL_PAY_DEPOSIT("/user/presellPayDeposit"),
	
	/** 商家预售商品管理列表 */
	PRESELL_PROD_LIST("/presell/presellProdList"),
	
	PRESELL_PROD("/presell/presellProd"),
	
	PRESELLPROD_DETAIL("/presell/presellDetail"),
	
	/**mobile预售商品订单提交页面 */
	PRESELLSUBMITORDERS("/presell/presellSubmitOrders"),
	
	/** 操作错误页面 */
	OPERATION_ERROR("/operationError"),
	
	/** 订单成功 */
	ORDER_PAY("/orderPay"),

	;

	/** The value. */
	private final String value;

	private MobileFrontPage(String value) {
		this.value = value;
	}

	/**
	 * 获取当前页面定义的全路径
	 * 
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http.HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/**
	 * 根据页面定义计算页面的全路径
	 * 
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http.HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculateMobileFronendPath(path);
	}

	public String getNativeValue() {
		return value;
	}

}
