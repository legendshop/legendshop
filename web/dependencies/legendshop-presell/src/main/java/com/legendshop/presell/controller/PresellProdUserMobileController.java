/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.presell.controller;

import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.xssfilter.XssDtoFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.ActiveBannerTypeEnum;
import com.legendshop.model.constant.PresellProdStatusEnum;
import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.dto.TreeNode;
import com.legendshop.model.dto.presell.PresellProdDetailDto;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.model.entity.ActiveBanner;
import com.legendshop.model.entity.Area;
import com.legendshop.model.entity.City;
import com.legendshop.model.entity.PresellProd;
import com.legendshop.model.entity.ProductCommentStat;
import com.legendshop.model.entity.Province;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.presell.page.front.MobileFrontPage;
import com.legendshop.presell.page.front.PresellFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.ActiveBannerService;
import com.legendshop.spi.service.CategoryManagerService;
import com.legendshop.spi.service.LocationService;
import com.legendshop.spi.service.PresellProdService;
import com.legendshop.spi.service.ProductCommentStatService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.util.AppUtils;

/**
 * 预售商品MobileController
 */
@Controller
@RequestMapping("/presell/mobile")
public class PresellProdUserMobileController extends BaseController {

	@Autowired
	private PresellProdService presellProdService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private CategoryManagerService categoryManagerService;

	@Autowired
	private LocationService locationService;
	
	@Autowired
	private ProductCommentStatService productCommentStatService;
	
	@Autowired
	private ActiveBannerService activeBannerService;
	
	
	/**
	 * 跳转预售活动列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/presellProd")
	public String presellProd(HttpServletRequest request, HttpServletResponse response) {
		List<ActiveBanner> bannerList = activeBannerService.getBannerList(ActiveBannerTypeEnum.PRESELL_BANNER.value());
		request.setAttribute("bannerList", bannerList);
		return PathResolver.getPath(MobileFrontPage.PRESELL_PROD);
	}
	
	/**
	 * 异步获取预售活动列表
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param parms
	 * @return
	 */
	@RequestMapping(value = "/presellProdList")
	public String presellProdList(HttpServletRequest request, HttpServletResponse response, String curPageNO, ProductSearchParms parms) {

		parms = XssDtoFilter.safeProductSearchParms(parms);

		PageSupport<PresellProd> ps = presellProdService.queryPresellProdListPage(curPageNO,parms);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(MobileFrontPage.PRESELL_PROD_LIST);
	}
	
	
	/**
	 * 跳转到预售商品详情页面
	 * 
	 * @param request
	 * @param response
	 * @param id
	 *            预售活动ID
	 * @return
	 */
	@RequestMapping(value = "/views/{presellId}", method = RequestMethod.GET)
	public String toPreProdDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable Long presellId) {
		if (AppUtils.isBlank(presellId)) {
			request.setAttribute("message", "该活动所属的店铺已不存在");
			return PathResolver.getPath(MobileFrontPage.OPERATION_ERROR);
		}
		PresellProdDetailDto presellProdDetail = presellProdService.getPresellProdDetailDto(presellId);
		if (AppUtils.isBlank(presellProdDetail)) {
			request.setAttribute("message", "该活动已结束或已下线");
			return PathResolver.getPath(MobileFrontPage.OPERATION_ERROR);
		}
		
		if(presellProdDetail.getStatus() != null && presellProdDetail.getStatus() != 0) {//不等于0表示已经下线
			//返回商品详情
			return PathResolver.getRedirectPath("/views/" + presellProdDetail.getProdId());
		}
		
		// 查询该商品所属分类
		// 查询该分类
		if (AppUtils.isNotBlank(presellProdDetail.getCategoryId())) {
			List<TreeNode> treeNodes = categoryManagerService.getTreeNodeNavigation(presellProdDetail.getCategoryId(), ",");
			request.setAttribute("treeNodes", treeNodes);
		}
		Long shopId = presellProdDetail.getShopId();
		// 查询店铺详情
		ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
		
		// 查询店铺的详情信息
				
		if (AppUtils.isBlank(shopDetail)) {
			request.setAttribute("message", "该活动所属的店铺已不存在");
			return PathResolver.getPath(MobileFrontPage.OPERATION_ERROR);
		}
		
		if (!shopDetail.getStatus().equals(ShopStatusEnum.NORMAL.value())) {
			request.setAttribute("message", "该活动所属的店铺已经被下线或关闭");
			return PathResolver.getPath(MobileFrontPage.OPERATION_ERROR);
		}
		
		// 包邮状态
		Integer mailfeeSts = shopDetail.getMailfeeSts();
		if (mailfeeSts != null && mailfeeSts.intValue() == 1) {
			// 包邮类型
			Integer mailfeeType = shopDetail.getMailfeeType();
			Double mailfeeCon = shopDetail.getMailfeeCon();
			String mailfeeStr = null;
			if (mailfeeType.intValue() == 1) {// 满金额包邮
				mailfeeStr = "满" + mailfeeCon + "元包邮";
			} else if (mailfeeType.intValue() == 2 && AppUtils.isNotBlank(mailfeeCon)) {
				mailfeeStr = "满" + mailfeeCon.intValue() + "件包邮";
			}
			request.setAttribute("mailfeeStr", mailfeeStr);
		}
				
		//计算该店铺所有已评分商品的平均分
		ProductCommentStat shopCommentStat = productCommentStatService.getProductCommentStatByShopId(shopId);
		if(shopCommentStat!=null&&shopCommentStat.getScore()!=null&&shopCommentStat.getComments()!=null){
			float sum=(float)shopCommentStat.getScore()/shopCommentStat.getComments();
			request.setAttribute("sum",new DecimalFormat("0.0").format(sum));
		}
		// 配送至地址 默认值
		List<Province> provinces = locationService.getAllProvince();
		List<City> citys = locationService.getCity(5);
		List<Area> areas = locationService.getArea(64);

		request.setAttribute("provinces", provinces);
		request.setAttribute("citys", citys);
		request.setAttribute("areas", areas);
		request.setAttribute("presellProd", presellProdDetail);
		request.setAttribute("shopDetail", shopDetail);
		
		SecurityUserDetail user = UserManager.getUser(request);
		if (UserManager.isUserLogined(user)) {
			request.setAttribute("loginUser", user.getUsername());
		}
		String path = PathResolver.getPath(MobileFrontPage.PRESELLPROD_DETAIL);
		return path;
	}

	/**
	 * 跳转到生成订单页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toGenerateOrder", method = RequestMethod.GET)
	public String toGenerateOrder(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(PresellFrontPage.U_PRESELL_GENERATE_ORDER);
	}

	/**
	 * 检查用户选择的SKU是否参加预售
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/isPresellProd", method = RequestMethod.GET)
	@ResponseBody
	public PresellProd isPresellProd(HttpServletRequest request, HttpServletResponse response, Long prodId, Long skuId) {
		if (AppUtils.isNotBlank(prodId) || AppUtils.isNotBlank(skuId)) {
			PresellProd presellProd = presellProdService.getPresellProd(prodId, skuId);
			if(presellProd != null && PresellProdStatusEnum.ONLINE.value().equals(presellProd.getStatus())){
				return presellProd;
			}
		}
		return null;
	}
}
