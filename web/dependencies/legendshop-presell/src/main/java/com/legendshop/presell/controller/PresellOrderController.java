/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.presell.controller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.util.NumberUtil;
import com.legendshop.model.dto.order.MySubDto;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.exception.NotStocksException;
import com.legendshop.base.model.UserMessages;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.util.DateUtils;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.page.CommonFrontPage;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayPctTypeEnum;
import com.legendshop.model.constant.PresellProdStatusEnum;
import com.legendshop.model.constant.SubmitOrderStatusEnum;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.order.AddOrderMessage;
import com.legendshop.model.dto.presell.PreSellExposerDto;
import com.legendshop.model.dto.presell.PreSellShopCartItem;
import com.legendshop.presell.page.front.PresellFrontPage;
import com.legendshop.processor.ProdSnapshotProcessor;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;
import com.legendshop.util.MD5Util;

/**
 * 预售订单控制器
 *
 */
@Controller
@RequestMapping("/p/presell/order")
public class PresellOrderController {

	@Autowired
	private OrderCartResolverManager orderCartResolverManager;

	@Autowired
	private InvoiceService invoiceService;

	@Autowired
	private SubService subService;

	@Autowired
	private PresellProdService presellProdService;

	@Autowired
	private StockService stockService;

	@Autowired
	private UserAddressService userAddressService;

	// 订单常用处理工具类
	@Autowired
	private OrderUtil orderUtil;

	@Autowired
	private PresellSubService presellSubService;

	@Autowired
	private PresellSkuService presellSkuService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private PayTypeService payTypeService;

	@Autowired
	private ShopDetailService shopDetailService;

	/** 订单快照 **/
	@Autowired
	private ProdSnapshotProcessor prodSnapshotProcessor;

	/**
	 * 预售订单详情
	 * 
	 * @param request
	 * @param response
	 * @param pctType
	 * @return
	 */
	@RequestMapping("/payDeposit/{pctType}/{subNumber}/{presellId}")
	public String orderSuccess(HttpServletRequest request, HttpServletResponse response, @PathVariable int pctType, @PathVariable String subNumber,@PathVariable Long presellId) {
		if (pctType != 1 && pctType != 0 && pctType != 2) {
			request.setAttribute("message", "非法链接访问");
			return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
		}

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		PresellSub presellSub = presellSubService.getPresellSub(userId, subNumber);
		if (presellSub == null) {
			request.setAttribute("message", "找不到预售订单");
			return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
		}
		PresellProd presellProd = presellProdService.getPresellProd(presellId);
		if (AppUtils.isBlank(presellProd)){
			request.setAttribute("message", "该活动不存在或已下线！！！");
			return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
		}
		Date now = new Date();
		// 获取订单自动取消的时间
		int cancelMins = subService.getOrderCancelExpireDate();

		if (pctType == PayPctTypeEnum.FULL_PAY.value()) {// 全额支付
			// 判断订单是否已经取消
			Sub sub = subService.getSubBySubNumber(subNumber);
			if (AppUtils.isBlank(sub)) {
				request.setAttribute("message", "数据异常，查找不到订单");
				return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
			}

			int offsetMin = DateUtils.getOffsetMinutes(sub.getSubDate(), now);

			if (offsetMin >= cancelMins) {
				request.setAttribute("message", "超过" + cancelMins + "分未付款，订单已取消");
				return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
			}

			// 判断是否在预售期间
			if (presellSub.getPreSaleStart().after(now)) {
				request.setAttribute("message", "预售活动未开始");
				return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
			}
			if (presellSub.getPreSaleEnd().before(now)) {
				request.setAttribute("message", "预售活动已过期");
				return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
			}

			if (presellSub.getIsPayDeposit().intValue() == 1) {
				return PathResolver.getRedirectPath("/p/presellOrder");
			}

			request.setAttribute("totalAmount", presellSub.getTotal().doubleValue());
			request.setAttribute("preDepositPrice", presellSub.getPreDepositPrice().doubleValue());
		} else if (pctType == PayPctTypeEnum.FRONT_MONEY.value()) {// 定金支付
			// 判断订单是否已经取消
			Sub sub = subService.getSubBySubNumber(subNumber);
			if (AppUtils.isBlank(sub)) {
				request.setAttribute("message", "数据异常，查找不到订单");
				return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
			}

			int offsetMin = DateUtils.getOffsetMinutes(sub.getSubDate(), now);

			if (offsetMin >= cancelMins) {
				request.setAttribute("message", "超过" + cancelMins + "分未付款，订单已取消");
				return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
			}

			// 判断是否在预售期间
			if (presellSub.getPreSaleStart().after(now)) {
				request.setAttribute("message", "预售活动未开始");
				return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
			}
			if (presellSub.getPreSaleEnd().before(now)) {
				request.setAttribute("message", "预售活动已过期");
				return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
			}

			if (presellSub.getIsPayDeposit().intValue() == 1) {
				return PathResolver.getRedirectPath("/p/presellOrder");
			}

			request.setAttribute("totalAmount", presellSub.getPreDepositPrice());
			request.setAttribute("preDepositPrice", presellSub.getPreDepositPrice());

		} else if (pctType == PayPctTypeEnum.FINAL_MONEY.value()) {// 尾款支付

			if (presellSub.getIsPayDeposit().intValue() == 0) {
				request.setAttribute("message", "请先支付定金");
				return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
			}
			if (presellSub.getIsPayFinal().intValue() == 1) {
				return PathResolver.getRedirectPath("/p/presellOrder");
			}

			if (presellSub.getFinalMStart().after(now)) {
				request.setAttribute("message", "尾款支付时间未到");
				return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
			}
			if (presellSub.getFinalMEnd().before(now)) {
				request.setAttribute("message", "尾款支付过期");
				return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
			}
//			request.setAttribute("totalAmount",presellSub.getFinalPrice());
			BigDecimal total = presellSub.getTotal();
			BigDecimal preDepositPrice = presellSub.getPreDepositPrice();
			BigDecimal finalShouldPay = NumberUtil.sub(total, preDepositPrice);
			request.setAttribute("totalAmount",finalShouldPay);
			MySubDto subDto = subService.getMySubDtoBySubNumber(subNumber);
			request.setAttribute("freightAmount",subDto.getFreightAmount());
		}

		Double availablePredeposit = userDetailService.getAvailablePredeposit(userId);
		// 获取 支付方式
		Map<String, Integer> payTypesMap = payTypeService.getPayTypeMap();

		request.setAttribute("payTypesMap", payTypesMap);
		request.setAttribute("cancelMins", cancelMins);
		request.setAttribute("subNums", presellSub.getSubNumber());
		request.setAttribute("availablePredeposit", availablePredeposit == null ? 0 : availablePredeposit);
		request.setAttribute("payPctType", pctType);
		request.getSession().removeAttribute(Constants.PASSWORD_CALLBACK);
		return PathResolver.getPath(PresellFrontPage.PRESELL_PAY_DEPOSIT);
	}

	/**
	 * 预售订单支付
	 * 
	 * @param request
	 * @param response
	 * @param pctType           预售支付:尾款/定金
	 * @param payTypeId
	 * @param bankCode
	 * @param password_callback
	 * @return
	 */
	@RequestMapping("/payDeposit/payto/{subNumber}")
	public String payment(HttpServletRequest request, HttpServletResponse response, @PathVariable String subNumber, @RequestParam int pctType, String payTypeId, String bankCode,
			String password_callback, String prePayTypeVal) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();

		PresellSub presellSub = presellSubService.getPresellSub(userId, subNumber);

		/*
		 * if (pctType != 1 && pctType != 2) { request.setAttribute("message",
		 * "非法链接访问"); return PathResolver.getPath(request, response,
		 * CommonFrontPage.OPERATION_ERROR); } PresellSub presellSub =
		 * presellSubService.getPresellSub(subId); if (presellSub == null) {
		 * request.setAttribute("message", "找不到预售订单"); return
		 * PathResolver.getPath(request, response, CommonFrontPage.OPERATION_ERROR); }
		 * double totalAmount = 0; if (pctType == 1) { // 定金支付 if
		 * (presellSub.getIsPayDeposit().intValue() == 1) {
		 * request.setAttribute("message", "请勿重复支付"); return
		 * PathResolver.getPath(request, response, CommonFrontPage.OPERATION_ERROR); }
		 * totalAmount = presellSub.getPreDepositPrice().doubleValue(); } else {// 尾款支付
		 * if (presellSub.getIsPayDeposit().intValue() == 0) {
		 * request.setAttribute("message", "请先支付定金"); return
		 * PathResolver.getPath(request, response, CommonFrontPage.OPERATION_ERROR); }
		 * if (presellSub.getIsPayFinal().intValue() == 1) {
		 * request.setAttribute("message", "请勿重复支付"); return
		 * PathResolver.getPath(request, response, CommonFrontPage.OPERATION_ERROR); }
		 * Date now = new Date(); if (presellSub.getFinalMStart().after(now)) {
		 * request.setAttribute("message", "尾款支付时间未到"); return
		 * PathResolver.getPath(request, response, CommonFrontPage.OPERATION_ERROR); }
		 * if (presellSub.getFinalMEnd().before(now)) { request.setAttribute("message",
		 * "尾款支付过期"); return PathResolver.getPath(request, response,
		 * CommonFrontPage.OPERATION_ERROR); } totalAmount =
		 * presellSub.getFinalPrice().doubleValue(); } Double accountAmount = 0d; if
		 * ("1".equals(password_callback) &&
		 * "true".equals(request.getSession().getAttribute(Constants.PASSWORD_CALLBACK))
		 * ) { // 预付款支付 UserDetail userDetail =
		 * userDetailService.getUserDetailById(userId); Double predeposit =
		 * userDetail.getAvailablePredeposit(); if (predeposit >= totalAmount) { //
		 * 预付款金额大于订单支付金额 cashiersControlCenterService.predepositPayPreSell(userId,
		 * totalAmount, String.valueOf(subId), pctType, prePayTypeVal); return
		 * PathResolver.getPath(request, response, "/p/presellOrder",
		 * PresellRedirectPage.VARIABLE); } accountAmount = predeposit; } else if
		 * (totalAmount < 0.01) { request.setAttribute("message", "支付金额错误，不能支付!");
		 * return PathResolver.getPath(request, response,
		 * CommonFrontPage.OPERATION_ERROR); }
		 * 
		 * if (payTypeId == null) { request.setAttribute("message", "请选择支付方式!"); return
		 * PathResolver.getPath(request, response, CommonFrontPage.OPERATION_ERROR); }
		 * PayType payType = payTypeService.getPayTypeByPayTypeId(payTypeId); if
		 * (payType == null) { request.setAttribute("message", "支付方式不存在!"); return
		 * PathResolver.getPath(request, response, CommonFrontPage.OPERATION_ERROR); }
		 * else if (payType.getIsEnable() == Constants.OFFLINE) {
		 * request.setAttribute("message", "该支付方式没有启用!"); return
		 * PathResolver.getPath(request, response, CommonFrontPage.OPERATION_ERROR); }
		 * 
		 * String ip = IPHelper.getIpAddr(request); SysPaymentForm paymentForm = new
		 * SysPaymentForm(totalAmount, accountAmount, payType.getPayId(), userId,
		 * String.valueOf(subId), payType.getInterfaceType(), payTypeId,
		 * payType.getPayTypeName(), "预售订单支付", ip);
		 * 
		 *//**
			 * 修改一次支付 都会往支付结算单据中心插入一条记录
			 *//*
				 * List<SubSettlement> settlement =
				 * subSettlementService.getSubSettlements(paymentForm.getTradingNumbers(),
				 * paymentForm.getUserId(), pctType); SubSettlement selectSettlement = null; //
				 * 当前选中的支付方式单据 if (AppUtils.isNotBlank(settlement)) { for
				 * (Iterator<SubSettlement> iterator = settlement.iterator();
				 * iterator.hasNext();) { SubSettlement subSettlement = (SubSettlement)
				 * iterator.next(); if (subSettlement.getIsClearing().intValue() == 1) { throw
				 * new BusinessException("请勿重复下单操作", ErrorCodes.BUSINESS_ERROR); } //
				 * 首先判断该支付方式是否提交过 and not change Money if
				 * (subSettlement.getPayTypeId().equals(paymentForm.getPayTypeId()) &&
				 * !isChage(subSettlement.getCashAmount(), paymentForm.getTotalAmount()) &&
				 * !isChage(subSettlement.getPdAmount(), paymentForm.getAccountAmount())) {
				 * selectSettlement = subSettlement; break; } } } selectSettlement =
				 * buildSubSettlement(selectSettlement, paymentForm);
				 * selectSettlement.setPayPctType(pctType); selectSettlement.setPresell(true);
				 * paymentForm.setSubSettlementSn(selectSettlement.getSubSettlementSn());
				 * paymentForm.setTradingNumbers(selectSettlement.getSettlementNumbers());
				 * paymentForm.setBankCode(bankCode); paymentForm.setShowUrl(show_url); String
				 * payment_result =
				 * cashiersControlCenterService.CashiersControlCenter(selectSettlement,
				 * paymentForm); request.setAttribute("payment_result", payment_result); boolean
				 * isTestPay = PropertiesUtil.getBooleanPay(); // 是否模拟支付 if (isTestPay) { return
				 * PathResolver.getPath(request, response, PresellRedirectPage.RETURNPAY_TEST) +
				 * "/" + selectSettlement.getSubSettlementSn(); } else { return
				 * PathResolver.getPath(request, response, BackPage.PAY_PAGE); }
				 */
		return userId;
	}

	/**
	 * 是否有改变
	 */
	private boolean isChage(Object a, Object b) {
		if (AppUtils.isNotBlank(a) && AppUtils.isNotBlank(b)) {
			if (!a.equals(b)) {
				return true;
			} else {
				return false;
			}
		} else if (AppUtils.isBlank(a) && AppUtils.isBlank(b)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 预售检查
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param number
	 * @return
	 */
	@RequestMapping(value = "/checkPresell/{id}", method = RequestMethod.POST)
	@ResponseBody
	public PreSellExposerDto checkPresell(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, @RequestParam Integer number,Long skuId) {

		PreSellExposerDto exposerDto = new PreSellExposerDto();
		if (number <= 0) {
			exposerDto.setExpose(false);
			exposerDto.setMsg("请输入正确的购买数量");
			return exposerDto;
		}

		SecurityUserDetail user = UserManager.getUser(request);
		
		PresellProd presellProd = presellProdService.getPresellProd(id);
		if (presellProd == null) {
			exposerDto.setExpose(false);
			exposerDto.setMsg("找不到预售信息");
			return exposerDto;
		}

		if (user != null && presellProd.getShopId().equals(user.getShopId())) {
			exposerDto.setExpose(false);
			exposerDto.setMsg("自己的商品,不能购买!");
			return exposerDto;
		}

		if (PresellProdStatusEnum.ONLINE.value().intValue() != presellProd.getStatus()) {
			exposerDto.setExpose(false);
			exposerDto.setMsg("预售未上线");
			return exposerDto;
		}
		Date startTime = presellProd.getPreSaleStart();
		Date endTime = presellProd.getPreSaleEnd();
		Date now = new Date();
		if (now.getTime() >= endTime.getTime()) { // 活动已经过期
			exposerDto.setExpose(false);
			exposerDto.setMsg("预售活动已经过期");
			return exposerDto;
		} else if (now.getTime() < startTime.getTime()) { // 距离活动开始时间
			exposerDto.setExpose(false);
			exposerDto.setMsg("预售活动未开始");
			return exposerDto;
		}
		/*
		 * 1.付款减库存 简单的检查商品库存够不够
		 */
		Integer stocks = stockService.getStocksByMode(presellProd.getProdId(), skuId);
		if (stocks - number < 0) {
			exposerDto.setExpose(false);
			exposerDto.setMsg("NOT_STOCK");
			return exposerDto;
		}

		// 检查是否自己的商品

		StringBuffer buffer = new StringBuffer().append(user.getUserId()).append("_").append(id);
		String _md5 = MD5Util.toMD5(buffer.toString() + Constants._MD5);
		exposerDto.setToken(_md5);
		exposerDto.setExpose(true);
		exposerDto.setMsg("检测通过");
		exposerDto.setId(id);
		request.getSession().setAttribute("PRESELL_TOKEN", _md5);
		request.getSession().setAttribute("PRESELL_NUMBER", number);
		return exposerDto;
	}

	/**
	 * 去往订单详情页面 ----下单
	 * 
	 * @param request
	 * @param response
	 * @param presellId
	 * @return
	 */
	@RequestMapping(value = "/details/{presellId}/{payType}/{skuId}", method = RequestMethod.POST)
	public String details(HttpServletRequest request, HttpServletResponse response, @PathVariable Long presellId, @PathVariable String payType,Long invoiceId,@PathVariable Long skuId) {
		com.legendshop.security.model.SecurityUserDetail userDetail = UserManager.getUser(request);
		String userId = userDetail.getUserId();
		StringBuffer buffer = new StringBuffer().append(userId).append("_").append(presellId);
		String _md5 = MD5Util.toMD5(buffer.toString() + Constants._MD5);
		String PRESELL_TOKEN = (String) request.getSession().getAttribute("PRESELL_TOKEN");
		if (!_md5.equals(PRESELL_TOKEN)) {
			request.setAttribute("ERROR_MESSAGE", "非法访问");
			UserMessages uem = new UserMessages();
			uem.setTitle("非法访问");
			request.setAttribute(UserMessages.MESSAGE_KEY, uem);
			return PathResolver.getPath(CommonFrontPage.ERROR);
		}
		Integer number = (Integer) request.getSession().getAttribute("PRESELL_NUMBER");
		if (number == null || number <= 0) {
			return PathResolver.getRedirectPath("/presell/views/" + presellId);
		}
		/* 根据类型查询订单列表 */
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", userDetail.getUserId());
		params.put("userName", userDetail.getUsername());
		params.put("presellId", presellId);
		params.put("number", number);
		params.put("skuId", skuId);
		UserShopCartList shopCartList = orderCartResolverManager.queryOrders(CartTypeEnum.PRESELL, params);
		if (AppUtils.isBlank(shopCartList)) {
			return PathResolver.getRedirectPath("/presell/views/" + presellId);
		}

		// 获取卖家是否开启发票功能
		Long shopId = shopCartList.getShopCarts().get(0).getShopId();
		ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
		Invoice userInvoice = null;
		if (AppUtils.isNotBlank(shopDetail.getSwitchInvoice()) && shopDetail.getSwitchInvoice().equals(1)) {

			if (AppUtils.isBlank(invoiceId)){
				// 查出买家的默认发票内容
				userInvoice = invoiceService.getDefaultInvoice(userDetail.getUserId());
			}else{
				// 查出传递过来的发票
				userInvoice = invoiceService.getInvoice(invoiceId,userDetail.getUserId());
			}

		}
		request.setAttribute("userdefaultInvoice", userInvoice);
		request.setAttribute("userShopCartList", shopCartList);
		request.setAttribute("presellId", presellId);
		request.setAttribute("payType", payType);
		request.setAttribute("skuId", skuId);
		// 设置订单提交令牌,防止重复提交
		Integer token = CommonServiceUtil.generateRandom();
		request.getSession().setAttribute(Constants.TOKEN, token);
		subService.putUserShopCartList(CartTypeEnum.PRESELL.toCode(), userId, shopCartList);

		if (PayPctTypeEnum.FRONT_MONEY.value().equals(Integer.parseInt(payType))) {// 定金支付
			// 获取预售订单定金金额
			PreSellShopCartItem preSellShopCartItem = (PreSellShopCartItem) shopCartList.getShopCarts().get(0).getCartItems().get(0);
			Double preDepositPriceAmount = preSellShopCartItem.getPreDepositPrice() * preSellShopCartItem.getBasketCount();
			request.setAttribute("preDepositPriceAmount", preDepositPriceAmount);
		}
		return PathResolver.getPath(PresellFrontPage.ORDERDETAILS);
	}

	/**
	 * 提交订单
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/submitOrder/{presellId}", method = RequestMethod.POST)
	public @ResponseBody AddOrderMessage submitOrder(HttpServletRequest request, HttpServletResponse response, @PathVariable Long presellId, @RequestParam String remarkText,
			@RequestParam Long invoiceId, @RequestParam String delivery, @RequestParam String token, @RequestParam String payType,@RequestParam Long skuId) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		
		StringBuffer buffer = new StringBuffer().append(userId).append("_").append(presellId);
		String _md5 = MD5Util.toMD5(buffer.toString() + Constants._MD5);
		String PRESELL_TOKEN = (String) request.getSession().getAttribute("PRESELL_TOKEN");
		AddOrderMessage message = new AddOrderMessage();
		message.setResult(false);
		if (!_md5.equals(PRESELL_TOKEN)) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("非法访问");
			return message;
		}

		// TODO 校验用户参数，SpringSession后台过期机制有问题
		if (!userDetailService.isUserExist(userName)) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("用户不存在！");
			return message;
		}

		Integer number = (Integer) request.getSession().getAttribute("PRESELL_NUMBER");
		if (number == null || number <= 0) {
			message.setCode(SubmitOrderStatusEnum.NOT_PRODUCTS.value());
			return message;
		}
		Integer sessionToken = (Integer) request.getSession().getAttribute(Constants.TOKEN); // 取session中保存的token
		Integer userToken = Integer.parseInt(token); // 取用户提交过来的token进行对比
		if (AppUtils.isBlank(sessionToken)) {
			message.setCode(SubmitOrderStatusEnum.NULL_TOKEN.value());
			return message;
		}
		if (!userToken.equals(sessionToken)) {
			message.setCode(SubmitOrderStatusEnum.INVALID_TOKEN.value());
			return message;
		}
		// get cache
		UserShopCartList userShopCartList = subService.findUserShopCartList(CartTypeEnum.PRESELL.toCode(), userId);
		if (AppUtils.isBlank(userShopCartList)) {
			message.setCode(SubmitOrderStatusEnum.NOT_PRODUCTS.value());
			return message;
		}
		// 检查金额： 如果促销导致订单金额为负数
		if (userShopCartList.getOrderActualTotal() < 0) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("订单金额为负数,下单失败!");
			return message;
		}
		ShopCarts shopCarts = userShopCartList.getShopCarts().get(0);
		shopCarts.setRemark(remarkText);

		Long shopId = userShopCartList.getShopCarts().get(0).getShopId();
		ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
		request.setAttribute("switchInvoice", shopDetail.getSwitchInvoice());
		if (AppUtils.isNotBlank(shopDetail.getSwitchInvoice()) && shopDetail.getSwitchInvoice().equals(1)) {
			// 查出买家的默认发票内容
			Invoice userdefaultInvoice = invoiceService.getDefaultInvoice(userId);
			request.setAttribute("userdefaultInvoice", userdefaultInvoice);
		}

		/** 根据id获取收货地址信息 **/
		UserAddress userAddress = userAddressService.getDefaultAddress(userId);
		if (AppUtils.isBlank(userAddress)) {
			message.setCode(SubmitOrderStatusEnum.NO_ADDRESS.value());
			return message;
		}
		userShopCartList.setUserAddress(userAddress);

		// 处理运费模板
		String result = orderUtil.handlerDelivery(delivery, userShopCartList);
		if (!Constants.SUCCESS.equals(result)) {
			message.setCode(result);
			return message;
		}

		// 验证成功，清除令牌
		request.getSession().removeAttribute(Constants.TOKEN);
		userShopCartList.setUserId(userId);
		userShopCartList.setUserName(userName);
		userShopCartList.setInvoiceId(invoiceId);
		userShopCartList.setUserAddress(userAddress);
		userShopCartList.setPayManner(2);
		userShopCartList.setType(CartTypeEnum.PRESELL.toCode());
		userShopCartList.setActiveId(presellId);

		/**
		 * 处理订单
		 */
		List<String> subIds = null;
		try {
			subIds = orderCartResolverManager.addOrder(CartTypeEnum.PRESELL, userShopCartList);
		} catch (NotStocksException e) {
			message.setCode(SubmitOrderStatusEnum.UNDERSTOCK.value());
			message.setMessage(e.getMessage());
			return message;
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof BusinessException) {
				message.setCode(SubmitOrderStatusEnum.ERR.value());
				message.setMessage(e.getMessage());
				return message;
			}
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("下单失败,请联系商城管理员或商城客服");
			return message;
		}

		if (AppUtils.isBlank(subIds)) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("下单失败,请联系商城管理员或商城客服");
			return message;
		} else {
			// 发送网站快照备份
			prodSnapshotProcessor.process(subIds);
		}
		subService.evictUserShopCartCache(CartTypeEnum.PRESELL.toCode(), userId);
		request.getSession().removeAttribute("PRESELL_NUMBER");
		request.getSession().removeAttribute("PRESELL_TOKEN");
		StringBuffer url = new StringBuffer();
		url.append("/p/presell/order/payDeposit/" + payType + "/" + subIds.get(0) + "/"+ presellId);
		message.setResult(true);
		message.setUrl(url.toString());
		return message;
	}

}
