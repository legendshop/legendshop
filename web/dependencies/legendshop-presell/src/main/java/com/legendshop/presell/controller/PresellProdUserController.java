/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.presell.controller;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.util.XssFilterUtil;
import com.legendshop.base.xssfilter.XssDtoFilter;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.page.CommonFowardPage;
import com.legendshop.core.page.CommonFrontPage;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.PresellProdStatusEnum;
import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.dto.TreeNode;
import com.legendshop.model.dto.presell.PresellProdDetailDto;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.presell.page.front.PresellFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;

/**
 * 预售商品Controller
 *
 */
@Controller
@RequestMapping("/presell")
public class PresellProdUserController extends BaseController {

	@Autowired
	private PresellProdService presellProdService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private CategoryManagerService categoryManagerService;

	@Autowired
	private LocationService locationService;

	@Autowired
	private SkuService skuService;
	
	@Autowired
	private ProductCommentStatService productCommentStatService;

	@Autowired
	private ProductService productService;

	@Autowired
	private ShopCommStatService shopCommStatService;

	@Autowired
	private DvyTypeCommStatService dvyTypeCommStatService;

	@Autowired
	private UserDetailService userDetailService;

	/**
	 * 跳转到预售商品详情页面
	 * 
	 * @param request
	 * @param response
	 * @param id
	 *            预售活动ID
	 * @return
	 */
	@RequestMapping(value = "/views/{presellId}", method = RequestMethod.GET)
	public String toPreProdDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable Long presellId) {
		if (AppUtils.isBlank(presellId)) {
			return PathResolver.getPath(CommonFowardPage.INDEX_QUERY);
		}
		
		PresellProdDetailDto presellProdDetail = presellProdService.getPresellProdDetailDto(presellId);
		if (AppUtils.isBlank(presellProdDetail)) {
			return PathResolver.getPath(CommonFrontPage.PROD_NON_EXISTENT);
		}
		//不等于0表示已经下线
		if(presellProdDetail.getStatus() != null && presellProdDetail.getStatus() != 0) {
			//返回商品详情
			return PathResolver.getRedirectPath("/views/" + presellProdDetail.getProdId());
		}

		Float shopscore=0f;
		Float prodscore=0f;
		Float logisticsScore=0f;
		DecimalFormat decimalFormat = new DecimalFormat("0.0");
		decimalFormat.setRoundingMode(RoundingMode.FLOOR);
		// 查询该商品所属分类
		// 查询该分类
		if (AppUtils.isNotBlank(presellProdDetail.getCategoryId())) {
			List<TreeNode> treeNodes = categoryManagerService.getTreeNodeNavigation(presellProdDetail.getCategoryId(), ",");
			request.setAttribute("treeNodes", treeNodes);
		}
		Long shopId = presellProdDetail.getShopId();
		// 查询店铺详情
		ShopDetail shopDetail = shopDetailService.getShopDetailByIdNoCache(shopId);
		if(AppUtils.isBlank(shopDetail) || shopDetail.getStatus() != ShopStatusEnum.NORMAL.value()){
			return PathResolver.getPath(CommonFrontPage.PROD_NON_EXISTENT);
		}
		//计算该店铺所有已评分商品的平均分
		ProductCommentStat prodCommentStat = productCommentStatService.getProductCommentStatByShopId(shopId);
		if(prodCommentStat!=null&&prodCommentStat.getScore()!=null&&prodCommentStat.getComments()!=null){
			prodscore=(float)prodCommentStat.getScore()/prodCommentStat.getComments();
		}
		request.setAttribute("prodAvg",decimalFormat.format(prodscore));

		//计算该店铺所有已评分店铺的平均分
		ShopCommStat shopCommentStat = shopCommStatService.getShopCommStatByShopId(shopId);
		if(shopCommentStat!=null&&shopCommentStat.getScore()!=null&&shopCommentStat.getCount()!=null){
			shopscore=(float)shopCommentStat.getScore()/shopCommentStat.getCount();
		}
		request.setAttribute("shopAvg",decimalFormat.format(shopscore));
		//计算该店铺所有已评分物流的平均分
		DvyTypeCommStat  dvyTypeCommStat = dvyTypeCommStatService.getDvyTypeCommStatByShopId(shopId);
		if(dvyTypeCommStat!=null&&dvyTypeCommStat.getScore()!=null&&dvyTypeCommStat.getCount()!=null){
			logisticsScore=(float)dvyTypeCommStat.getScore()/dvyTypeCommStat.getCount();
		}
		request.setAttribute("dvyAvg",decimalFormat.format(logisticsScore));
		Float sum=0f;
		if (shopscore!=0&&prodscore!=0&&logisticsScore!=0) {
			sum=(float)Math.floor((shopscore+prodscore+logisticsScore)/3);
		}else if(shopscore!=0&&logisticsScore!=0){
			sum=(float)Math.floor((shopscore+logisticsScore)/2);
		}
		request.setAttribute("sum",decimalFormat.format(sum));
		// 配送至地址 默认值
		List<Province> provinces = locationService.getAllProvince();
		List<City> citys = locationService.getCity(5);
		List<Area> areas = locationService.getArea(64);
		List<Sku> skuByProd = skuService.getSkuByProd(presellProdDetail.getProdId());
		presellProdDetail.setSkuList(skuByProd);
		request.setAttribute("provinces", provinces);
		request.setAttribute("citys", citys);
		request.setAttribute("areas", areas);
		request.setAttribute("presellProd", presellProdDetail);
		request.setAttribute("shopDetail", shopDetail);
		
		SecurityUserDetail user = UserManager.getUser(request);
		if (UserManager.isUserLogined(user)) {
			request.setAttribute("loginUser", user.getUsername());
		}

		if(AppUtils.isNotBlank(user)){
			UserDetail userInfo = userDetailService.getUserDetailById(user.getUserId());
			if(AppUtils.isNotBlank(userInfo)){
				String password = userInfo.getUserName()+"123456";
				request.setAttribute("user_name",userInfo.getUserName());
				request.setAttribute("user_pass", password);
			}
		}
		return PathResolver.getPath(PresellFrontPage.U_PRESELLPROD_DETAIL);
	}

	/**
	 * 跳转到生成订单页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toGenerateOrder", method = RequestMethod.GET)
	public String toGenerateOrder(HttpServletRequest request, HttpServletResponse response) {
		return PathResolver.getPath(PresellFrontPage.U_PRESELL_GENERATE_ORDER);
	}

	/**
	 * 检查用户选择的SKU是否参加预售
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/isPresellProd", method = RequestMethod.GET)
	@ResponseBody
	public PresellProd isPresellProd(HttpServletRequest request, HttpServletResponse response, Long prodId, Long skuId) {
		if (AppUtils.isNotBlank(prodId) || AppUtils.isNotBlank(skuId)) {
			//查询预售
			PresellProd presell=productService.getPresellProdPrice(prodId,skuId);
			if (presell!=null) {
				Date nowDate = new Date();
				if(nowDate.getTime()<presell.getPreSaleEnd().getTime()){//如果当前时间已经大于活动的结束时间就把商品的状态回显
					return presell;
				}
			}
		}


		return null;
	}
	
	@RequestMapping(value = "/presellProd/list")
	public String list(HttpServletRequest request, HttpServletResponse response, String curPageNO, ProductSearchParms parms) {

		//xss过滤
		parms = XssDtoFilter.safeProductSearchParms(parms);

		PageSupport<PresellProd> ps = presellProdService.queryPresellProdListPage(curPageNO,parms);
		request.setAttribute("searchParams", parms);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(PresellFrontPage.PRESELL_PROD_LIST);
	}



}
