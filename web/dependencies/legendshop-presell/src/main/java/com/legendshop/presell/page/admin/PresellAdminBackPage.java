/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.presell.page.admin;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 后台页面定义
 */
public enum PresellAdminBackPage implements PageDefinition {

	/** The variable. */
	VARIABLE(""),
	
	/** 添加商品SKU弹层 */
	ADD_PROD_SKU_LAYOUT("/presellProd/addProdSkuLayout"),
	
	/** 加载商品SKU列表 */
	LOAD_PROD_SKU_LIST("/presellProd/loadProdSkuList"),

	;
	
	/** The value. */
	private final String value;

	/**
	 * 实例化一个新的后台页面
	 *
	 * @param value the value
	 */
	private PresellAdminBackPage(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http
	 * .HttpServletRequest, java.lang.String)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("presell", path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
