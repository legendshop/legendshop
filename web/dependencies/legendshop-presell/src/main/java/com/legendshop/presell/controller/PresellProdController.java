/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.presell.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.xssfilter.XssDtoFilter;
import com.legendshop.model.constant.*;
import com.legendshop.model.dto.MergeGroupSelProdDto;
import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.dto.PresellSkuDto;
import com.legendshop.model.dto.PresellSubDTO;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.page.CommonFrontPage;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.presell.PresellProdDto;
import com.legendshop.model.dto.presell.PresellSearchParamDto;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.presell.page.front.PresellFrontPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 预售商品Controller
 *
 */
@Controller
@RequestMapping("/s/presellProd")
public class PresellProdController extends BaseController {

	@Autowired
	private PresellProdService presellProdService;

	@Autowired
	private ProductService productService;

	@Autowired
	private SkuService skuService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private SubService subService;

	@Autowired
	private PresellSubService presellSubService;

	@Autowired
	private PresellSkuService presellSkuService;
	
	/**
	 * 初始化
	 * 
	 * @param binder
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

	@RequestMapping(value = "/list")
	public String list(HttpServletRequest request, HttpServletResponse response, String curPageNO, ProductSearchParms parms) {

		parms = XssDtoFilter.safeProductSearchParms(parms);

		PageSupport<PresellProd> ps = presellProdService.queryPresellProdListPage(curPageNO,parms);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(PresellFrontPage.PRESELL_PROD_LIST);
	}

	/**
	 * 跳转到预售管理页面
	 * 
	 * @param request
	 * @param response
	 * @param searchType
	 *            搜索类型
	 * @return
	 */
	@RequestMapping(value = "/query/{searchType}", method = RequestMethod.GET)
	@Authorize(authorityTypes = AuthorityType.PRESELL_MANAGE)
	public String query(HttpServletRequest request, HttpServletResponse response, @PathVariable String searchType) {
		request.setAttribute("searchType", searchType);
		return PathResolver.getPath(PresellFrontPage.S_PRESELLPROD_LIST_PAGE);
	}

	/**
	 * 获取预售活动列表内容
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 *            当前页
	 * @param paramDto
	 *            搜索参数
	 * @return
	 */
	@RequestMapping(value = "/listContent", method = RequestMethod.GET)
	public String getListContent(HttpServletRequest request, HttpServletResponse response, String curPageNO, PresellSearchParamDto paramDto) {
		SecurityUserDetail user = UserManager.getUser(request);
		PageSupport<PresellProdDto> ps = presellProdService.queryPresellProdListPage(curPageNO, user.getShopId(), paramDto);
		PageSupportHelper.savePage(request, ps);

		request.setAttribute("paramDto", paramDto);

		return PathResolver.getPath(PresellFrontPage.S_PRESELLPROD_LIST_CONTENT);
	}

	/**
	 * 查看预售商品详情页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/toPresellProdDetail/{id}", method = RequestMethod.GET)
	public String toPresellProdDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PresellProdDto presellProd = presellProdService.getPresellProdDto(id);
		if (AppUtils.isBlank(presellProd)) {
			return PathResolver.getPath(CommonFrontPage.PROD_NON_EXISTENT);
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		if (!presellProd.getShopId().equals(shopId)) {
			return PathResolver.getPath(CommonFrontPage.INSUFFICIENT_AUTHORITY);
		}
		request.setAttribute("presellProd", presellProd);
		return PathResolver.getPath(PresellFrontPage.S_PRESELLPROD_DETAIL_PAGE);
	}

	/**
	 * 挑选商品弹层
	 * 
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param product
	 * @return
	 */
	@RequestMapping(value = "/addProdSkuLayout", method = RequestMethod.GET)
	public String addProdSkuLayout(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {
		
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		PageSupport<Product> ps = productService.getAddProdSkuLayout(curPageNO, shopId, product);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prod", product);
		return PathResolver.getPath(PresellFrontPage.S_PRESELLPROD_PROD_SKU_LAYOUT);
	}

	/**
	 *  拿到prodId对应的商品
	 * @param request
	 * @param response
	 * @param prodId
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping(value = "/loadProdSku/{prodId}", method = RequestMethod.GET)
	public String loadProdSku(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId, String curPageNO) {
		List<Sku> skuList = skuService.getSkuByProd(prodId);
		if (AppUtils.isNotBlank(skuList)) {
			skuList.removeIf(sku -> sku.getStatus() == 0);
		}
		request.setAttribute("skuList", skuList);
		return PathResolver.getPath(PresellFrontPage.S_PRESELLPROD_LOAD_PROD_SKU_LIST);
	}

	/**
	 * 加载商家预售商品编辑页面
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/load", method = RequestMethod.GET)
	public String load(HttpServletRequest request, HttpServletResponse response) {

		return PathResolver.getPath(PresellFrontPage.S_PRESELLPROD_EDIT_PAGE);
	}

	/**
	 * 编辑预售
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/edit/{activeId}", method = RequestMethod.GET)
	public String edit(HttpServletRequest request, HttpServletResponse response,@PathVariable Long activeId) {
		PresellProd presellProd = presellProdService.getPresellProd(activeId);
		if (AppUtils.isBlank(presellProd)) {
			throw new BusinessException("对不起,该活动不存在，请返回重试");
		}
		request.setAttribute("presellProd", presellProd);
		List<PresellSkuDto> prodLists = productService.queryPresellProductByPresellId(presellProd.getId());
		request.setAttribute("prodLists", prodLists);
		return PathResolver.getPath(PresellFrontPage.S_PRESELLPROD_EDIT_PAGE);
	}

	/**
	 * 查看
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/details/{activeId}", method = RequestMethod.GET)
	public String details(HttpServletRequest request, HttpServletResponse response,@PathVariable Long activeId) {
		PresellProd presellProd = presellProdService.getPresellProd(activeId);
		if (AppUtils.isBlank(presellProd)) {
			throw new BusinessException("对不起,该活动不存在，请返回重试");
		}
		request.setAttribute("presellProd", presellProd);
		List<PresellSkuDto> prodLists = productService.queryPresellProductByPresellId(presellProd.getId());
		request.setAttribute("prodLists", prodLists);
		return PathResolver.getPath(PresellFrontPage.S_PRESELLPROD_DETAILS_PAGE);
	}
	

	/**
	 * 加载商家预售商品详情
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}", method = RequestMethod.GET)
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PresellProdDto presellProdDto = presellProdService.getPresellProdDto(id);
		if (AppUtils.isBlank(presellProdDto)) {
			return PathResolver.getPath(CommonFrontPage.ERROR);
		}
		SecurityUserDetail user = UserManager.getUser(request);
		if (!presellProdDto.getShopId().equals(user.getShopId())) {
			return PathResolver.getPath(CommonFrontPage.INSUFFICIENT_AUTHORITY);
		}
		request.setAttribute("presellProd", presellProdDto);
		return PathResolver.getPath(PresellFrontPage.S_PRESELLPROD_EDIT_PAGE);
	}

	/**
	 * 保存预售活动
	 * 
	 * @param request
	 * @param response
	 * @param presellProd
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(HttpServletRequest request, HttpServletResponse response, PresellProd presellProd) {
		if (AppUtils.isBlank(presellProd)) {
			request.setAttribute("message", "保存失败，参数有误");
			return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
		}

		if (AppUtils.isBlank(presellProd.getSkus())) {
			request.setAttribute("message", "保存失败，活动商品不能为空");
			return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
		}
		String searchType = ActivitySearchTypeEnum.WAIT_AUDIT.value();
		
		// 发货日期转换,因为其日期格式没有时分
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			presellProd.setPreDeliveryTime(dateFormat.parse(presellProd.getPreDeliveryTimeStr()));
		} catch (ParseException e) {
			e.printStackTrace();
			request.setAttribute("message", "发货时间日期格式错误!");
			return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
		}
		
		if(1 == presellProd.getPayPctType()){//定金模式
			presellProd.setPayPct(Arith.div(presellProd.getPreDepositPrice().doubleValue(), presellProd.getPrePrice().doubleValue(),2));
		}

		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		if (AppUtils.isBlank(presellProd.getId())) {// 添加
			String shopName = shopDetailService.getShopName(shopId);
			presellProd.setShopId(shopId);
			presellProd.setShopName(shopName);
			presellProd.setCreateTime(new Date());
			presellProd.setStatus(PresellProdStatusEnum.WAIT_AUDIT.value());
			presellProdService.savePresellProd(presellProd);
		} else {// 更新
			PresellProd oldPresellProd = presellProdService.getPresellProd(presellProd.getId());
			if (!oldPresellProd.getShopId().equals(shopId)) {
				request.setAttribute("message", "没有权限操作");
				return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
			}
			/*// 当前预售活动不处于待提审且也不处于拒绝状态,不允许修改
			if (!PresellProdStatusEnum.WAIT_AUDIT.value().equals(oldPresellProd.getStatus())
					&& !PresellProdStatusEnum.DENY.value().equals(oldPresellProd.getStatus())) {
				request.setAttribute("message", "没有权限操作");
				return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
			}
			// 已过期,不允许修改
			if (oldPresellProd.getPreSaleStart().before(new Date())) {
				request.setAttribute("message", "已过期,不允许修改");
				return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
			}*/
			/*if (PresellProdStatusEnum.DENY.value().equals(oldPresellProd.getStatus())) {// 如果是审核不通过
																						// 则修改后变为待审核
				oldPresellProd.setStatus(PresellProdStatusEnum.WAIT_AUDIT.value());
				searchType = PresellSearchParamDto.WAIT_AUDIT;
			}*/
			if (presellProd.getPayPctType().equals(1L)) {// 支付类型为全额支付
				oldPresellProd.setPreDepositPrice(presellProd.getPreDepositPrice());
				oldPresellProd.setFinalMStart(presellProd.getFinalMStart());
				oldPresellProd.setFinalMEnd(presellProd.getFinalMEnd());
				oldPresellProd.setPayPct(presellProd.getPayPct());
			} else if (presellProd.getPayPctType().equals(0L)) {// 定金支付
				oldPresellProd.setPreDepositPrice(null);
				oldPresellProd.setFinalMStart(null);
				oldPresellProd.setFinalMEnd(null);
				oldPresellProd.setPayPct(null);
			} else {
				request.setAttribute("message", "参数有误");
				return PathResolver.getPath(CommonFrontPage.OPERATION_ERROR);
			}
			oldPresellProd.setSchemeName(presellProd.getSchemeName());
			oldPresellProd.setProdId(presellProd.getProdId());
			oldPresellProd.setPrePrice(presellProd.getPrePrice());
			oldPresellProd.setPreSaleStart(presellProd.getPreSaleStart());
			oldPresellProd.setPreSaleEnd(presellProd.getPreSaleEnd());
			oldPresellProd.setPayPctType(presellProd.getPayPctType());
			oldPresellProd.setPreDeliveryTime(presellProd.getPreDeliveryTime());
			oldPresellProd.setStatus(PresellProdStatusEnum.WAIT_AUDIT.value());
			searchType = ActivitySearchTypeEnum.WAIT_AUDIT.value();

			presellProdService.updatePresellProd(oldPresellProd);
			presellSkuService.deletePresellSkuByPid(oldPresellProd.getId());
		}

		//新增修改预售商品集合
		List<PresellSku> skus = presellProd.getSkus();
		for (PresellSku presellSku : skus) {
			presellSku.setPId(presellProd.getId());
			presellSkuService.savePresellSku(presellSku);
		}
		return PathResolver.getRedirectPath("/s/presellProd/query/" + searchType);
	}

	/**
	 * 提审预售活动
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/submit/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String submit(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PresellProd presellProd = presellProdService.getPresellProd(id);
		if (AppUtils.isBlank(presellProd)) {
			return "对不起,当前预售活动不存在或已被删除!";
		}
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		if (!presellProd.getShopId().equals(shopId)) {
			return "对不起,您没有权限!";
		}

		// 已过期
		if (presellProd.getPreSaleStart().before(new Date())) {
			return "对不起，您操作的数据已过期!";
		}

		// 不是待提审状态
		if (!PresellProdStatusEnum.NOT_PUT_AUDIT.value().equals(presellProd.getStatus())) {
			return "对不起,请不要非法操作!";
		}

		presellProd.setStatus(PresellProdStatusEnum.WAIT_AUDIT.value());
		presellProdService.updatePresellProd(presellProd);

		return Constants.SUCCESS;
	}

	/**
	 * 删除预售活动
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PresellProd presellProd = presellProdService.getPresellProd(id);
		if (AppUtils.isBlank(presellProd)) {
			return "对不起,当前预售活动不存在,或已被删除!";
		}
		SecurityUserDetail user = UserManager.getUser(request);
		Long shopId = user.getShopId();
		
		if (!presellProd.getShopId().equals(shopId)) {
			return "对不起,您没有权限!";
		}
		if (PresellProdStatusEnum.ONLINE.value().equals(presellProd.getStatus()) && presellProd.getPreSaleStart().before(new Date())
				&& presellProd.getPreSaleEnd().after(new Date())) {
			return "对不起,请不要非法操作!";
		}
		presellProdService.deletePresellProd(presellProd);
		return Constants.SUCCESS;
	}

	/**
	 * 检查schemeName是否已存在
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/isSchemeNameExist", method = RequestMethod.GET)
	@ResponseBody
	public Boolean isSchemeNameExist(HttpServletRequest request, HttpServletResponse response, String schemeName) {
		if (AppUtils.isNotBlank(schemeName)) {
			
			SecurityUserDetail user = UserManager.getUser(request);
			Long shopId = user.getShopId();
			
			PresellProd presellProd = presellProdService.getPresellProd(schemeName, shopId);
			if (presellProd == null) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断预售商品是否已存在
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/isProdSkuExist", method = RequestMethod.GET)
	@ResponseBody
	public Boolean isProdSkuExist(HttpServletRequest request, HttpServletResponse response, Long presellId, Long prodId, Long skuId) {
		return isProdSkuExist(presellId, prodId, skuId);
	}

	/**
	 * 判断当前sku是否已参与其他营销活动
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/isAttendActivity/{prodId}/{skuId}", method = RequestMethod.GET)
	@ResponseBody
	public Boolean isAttendActivity(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable Long prodId, @PathVariable Long skuId) {
		return presellProdService.isAttendActivity(null, prodId, skuId);
	}

	/**
	 * 判断商品Sku是否参加其他营销活动（包括预售）
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	private Boolean isProdSkuExist(Long presellId, Long prodId, Long skuId) {
		Sku sku = skuService.getSkuByProd(prodId, skuId);
		if(!SkuActiveTypeEnum.PRODUCT.value().equals(sku.getSkuType())) {
			return true;
		} 
		return false;
	}


	/**
	 *   运营活动
	 */
	@RequestMapping(value = "/operatorPresellActivity/{id}", method = RequestMethod.GET)
	@Authorize(authorityTypes = AuthorityType.SECKILL_MANAGE)
	public String presellActivity(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id, String curPageNO) {
		PresellProd presellProd = presellProdService.getPresellProd(id);
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(presellProd)) {
			throw new NullPointerException("presellProd is null");
		}
		// 获取秒杀活动的订单
		if(AppUtils.isBlank(curPageNO) || "0".equals(curPageNO)){
			curPageNO = "1";
		}
		OperateStatisticsDTO operateStatistics = presellProdService.getOperateStatistics(id);
		PageSupport<Sub> ps = subService.getPresellOperationList(curPageNO, 6, presellProd.getId(),user.getShopId());
		for (Sub sub : ps.getResultList()) {
			PresellSub presellSub = presellSubService.getPresellSubByPresell(sub.getSubNumber());
			sub.setIsPayFinal(presellSub.getIsPayFinal());
			sub.setIsPayDeposit(presellSub.getIsPayDeposit());
			sub.setPayPctType(presellSub.getPayPctType());
		}
		PageSupportHelper.savePage(request, ps);
		if (AppUtils.isNotBlank(ps.getResultList())){
			operateStatistics.setPeopleCount(Integer.parseInt(ps.getTotal()+""));
		}
		request.setAttribute("presellId",presellProd.getId());
		request.setAttribute("operateStatistics",operateStatistics);
		request.setAttribute("presellProd",presellProd);
		return PathResolver.getPath(PresellFrontPage.S_PRESELLPROD_OPRTATOR_PAGE);
	}
}
