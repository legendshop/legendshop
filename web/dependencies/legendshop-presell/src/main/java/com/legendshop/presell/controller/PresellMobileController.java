/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.presell.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.dto.app.ResultDtoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.exception.NotStocksException;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.util.DateUtils;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayPctTypeEnum;
import com.legendshop.model.constant.PresellOrderPayStatusEnum;
import com.legendshop.model.constant.PresellProdStatusEnum;
import com.legendshop.model.constant.SubSettlementTypeEnum;
import com.legendshop.model.constant.SubmitOrderStatusEnum;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.order.AddOrderMessage;
import com.legendshop.model.dto.presell.PreSellExposerDto;
import com.legendshop.model.dto.presell.PreSellShopCartItem;
import com.legendshop.model.entity.Invoice;
import com.legendshop.model.entity.PresellProd;
import com.legendshop.model.entity.PresellSub;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.presell.page.front.MobileFrontPage;
import com.legendshop.processor.ProdSnapshotProcessor;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.spi.service.InvoiceService;
import com.legendshop.spi.service.OrderCartResolverManager;
import com.legendshop.spi.service.OrderUtil;
import com.legendshop.spi.service.PayTypeService;
import com.legendshop.spi.service.PresellProdService;
import com.legendshop.spi.service.PresellSubService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.StockService;
import com.legendshop.spi.service.SubService;
import com.legendshop.spi.service.UserAddressService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.MD5Util;

/**
 * 预售手机端页面
 */
@Controller
@RequestMapping("/p/mobile/presellProd")
public class PresellMobileController extends BaseController {

	@Autowired
	private PresellProdService presellProdService;
	
	@Autowired
	private InvoiceService invoiceService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private StockService stockService;
	
	@Autowired
	private OrderCartResolverManager orderCartResolverManager;
	
	@Autowired
	private SubService subService;

	@Autowired
	private UserAddressService userAddressService;
	
	@Autowired
	private OrderUtil orderUtil;
	
	@Autowired
	private PresellSubService presellSubService;
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private PayTypeService payTypeService;
	
	/** 订单快照 **/
	@Autowired
	private ProdSnapshotProcessor prodSnapshotProcessor;

	/**
	 * 初始化
	 * 
	 * @param binder
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

	/**
	 * 预售检查
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @param number
	 * @return
	 */
	@RequestMapping(value = "/checkPresell/{presellId}", method = RequestMethod.POST)
	@ResponseBody
	public PreSellExposerDto checkPresell(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable Long presellId, @RequestParam Integer number) {

		PreSellExposerDto exposerDto = new PreSellExposerDto();
		if (number <= 0) {
			exposerDto.setExpose(false);
			exposerDto.setMsg("请输入正确的购买数量");
			return exposerDto;
		}
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		Long shopId = user.getShopId();

		PresellProd presellProd = presellProdService.getPresellProd(presellId);
		if (AppUtils.isBlank(presellProd)) {
			exposerDto.setExpose(false);
			exposerDto.setMsg("找不到预售信息");
			return exposerDto;
		}

		if (UserManager.isUserLogined(user)) {
			if (presellProd.getShopId().equals(shopId)) {
				exposerDto.setExpose(false);
				exposerDto.setMsg("自己的商品,不能购买!");
				return exposerDto;
			}
		}

		if (PresellProdStatusEnum.ONLINE.value().intValue() != presellProd.getStatus()) {
			exposerDto.setExpose(false);
			exposerDto.setMsg("预售未上线");
			return exposerDto;
		}

		Date startTime = presellProd.getPreSaleStart();
		Date endTime = presellProd.getPreSaleEnd();

		Date now = new Date();
		if (now.getTime() >= endTime.getTime()) { // 活动已经过期
			exposerDto.setExpose(false);
			exposerDto.setMsg("预售活动已经过期");
			return exposerDto;
		} else if (now.getTime() < startTime.getTime()) { // 距离活动开始时间
			exposerDto.setExpose(false);
			exposerDto.setMsg("预售活动未开始");
			return exposerDto;
		}
		/*
		 * 1.付款减库存 简单的检查商品库存够不够
		 */
		Integer stocks = stockService.getStocksByMode(presellProd.getProdId(), presellProd.getSkuId());
		if (stocks - number < 0) {
			exposerDto.setExpose(false);
			exposerDto.setMsg("预售商品库存不足,请选择其他预售商品");
			return exposerDto;
		}

		StringBuffer buffer = new StringBuffer().append(userId).append("_").append(presellId);
		String _md5 = MD5Util.toMD5(buffer.toString() + Constants._MD5);
		exposerDto.setToken(_md5);
		exposerDto.setExpose(true);
		exposerDto.setMsg("检测通过");
		exposerDto.setId(presellId);
		request.getSession().setAttribute("PRESELL_TOKEN", _md5);
		request.getSession().setAttribute("PRESELL_NUMBER", number);
		return exposerDto;
	}

	/**
	 * 去往订单详情页面 ----下单
	 * 
	 * @param request
	 * @param response
	 * @param presellId
	 * @return
	 */
	@RequestMapping(value = "/details/{presellId}/{payType}")
	public String details(HttpServletRequest request, HttpServletResponse response, @PathVariable Long presellId, @PathVariable String payType) {
		com.legendshop.security.model.SecurityUserDetail userDetail = UserManager.getUser(request);
		String userId = userDetail.getUserId();
		StringBuffer buffer = new StringBuffer().append(userId).append("_").append(presellId);
		String _md5 = MD5Util.toMD5(buffer.toString() + Constants._MD5);
		String PRESELL_TOKEN = (String) request.getSession().getAttribute("PRESELL_TOKEN");
		if (!_md5.equals(PRESELL_TOKEN)) {
			request.setAttribute("message", "非法访问");
			return PathResolver.getPath(MobileFrontPage.OPERATION_ERROR);
		}
		Integer number = (Integer) request.getSession().getAttribute("PRESELL_NUMBER");
		if (number == null || number <= 0) {
			request.setAttribute("message", "购买数量不合法");
			return PathResolver.getPath(MobileFrontPage.OPERATION_ERROR);
		}
		/* 根据类型查询订单列表 */
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userId", userDetail.getUserId());
		params.put("userName", userDetail.getUsername());
		params.put("presellId", presellId);
		params.put("number", number);
		UserShopCartList shopCartList = orderCartResolverManager.queryOrders(CartTypeEnum.PRESELL, params);
		if (AppUtils.isBlank(shopCartList)) {
			request.setAttribute("message", "数据异常，请重新下单");
			return PathResolver.getPath(MobileFrontPage.OPERATION_ERROR);
		}
		// 查出买家的默认发票内容
		Invoice userdefaultInvoice = invoiceService.getDefaultInvoice(userId);
		request.setAttribute("userdefaultInvoice", userdefaultInvoice);
		request.setAttribute("userShopCartList", shopCartList);
		request.setAttribute("presellId", presellId);
		request.setAttribute("payType", payType);
		// 设置订单提交令牌,防止重复提交
		Integer token = CommonServiceUtil.generateRandom();
		request.getSession().setAttribute(Constants.TOKEN, token);
		subService.putUserShopCartList(CartTypeEnum.PRESELL.toCode(), userId, shopCartList);
		
		if(PayPctTypeEnum.FRONT_MONEY.value().equals(Integer.parseInt(payType))) {//定金支付
			//获取预售订单定金金额
			PreSellShopCartItem preSellShopCartItem = (PreSellShopCartItem) shopCartList.getShopCarts().get(0).getCartItems().get(0);
			Double preDepositPriceAmount = preSellShopCartItem.getPreDepositPrice() * preSellShopCartItem.getBasketCount();
			request.setAttribute("preDepositPriceAmount", preDepositPriceAmount);
		}
		
		return PathResolver.getPath(MobileFrontPage.PRESELLSUBMITORDERS);
	}

	/**
	 * 提交订单
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/submitOrder/{presellId}", method = RequestMethod.POST)
	public @ResponseBody AddOrderMessage submitOrder(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable Long presellId, @RequestParam String remarkText, 
			@RequestParam Long invoiceId, @RequestParam String delivery, 
			@RequestParam String token, @RequestParam String payType) {

		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		String userName = user.getUsername();
		
		StringBuffer buffer = new StringBuffer().append(userId).append("_").append(presellId);
		String _md5 = MD5Util.toMD5(buffer.toString() + Constants._MD5);
		String PRESELL_TOKEN = (String) request.getSession().getAttribute("PRESELL_TOKEN");
		AddOrderMessage message = new AddOrderMessage();
		message.setResult(false);
		if (!_md5.equals(PRESELL_TOKEN)) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("非法访问");
			return message;
		}

		// TODO 校验用户参数，SpringSession后台过期机制有问题
		if (!userDetailService.isUserExist(userName)) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("用户不存在！");
			return message;
		}

		Integer number = (Integer) request.getSession().getAttribute("PRESELL_NUMBER");
		if (number == null || number <= 0) {
			message.setCode(SubmitOrderStatusEnum.NOT_PRODUCTS.value());
			return message;
		}
		Integer sessionToken = (Integer) request.getSession().getAttribute(Constants.TOKEN); // 取session中保存的token
		Integer userToken = Integer.parseInt(token); // 取用户提交过来的token进行对比
		if (AppUtils.isBlank(sessionToken)) {
			message.setCode(SubmitOrderStatusEnum.NULL_TOKEN.value());
			return message;
		}
		if (!userToken.equals(sessionToken)) {
			message.setCode(SubmitOrderStatusEnum.INVALID_TOKEN.value());
			return message;
		}
		// get cache
		UserShopCartList userShopCartList = subService.findUserShopCartList(CartTypeEnum.PRESELL.toCode(), userId);
		if (AppUtils.isBlank(userShopCartList)) {
			message.setCode(SubmitOrderStatusEnum.NOT_PRODUCTS.value());
			return message;
		}
		// 检查金额： 如果促销导致订单金额为负数
		if (userShopCartList.getOrderActualTotal() < 0) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("订单金额为负数,下单失败!");
			return message;
		}
		
		ShopCarts shopCarts = userShopCartList.getShopCarts().get(0);
		shopCarts.setRemark(remarkText);

		Long shopId = userShopCartList.getShopCarts().get(0).getShopId();
		ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
		request.setAttribute("switchInvoice", shopDetail.getSwitchInvoice());
		if (AppUtils.isNotBlank(shopDetail.getSwitchInvoice()) && shopDetail.getSwitchInvoice().equals(1)) {
			// 查出买家的默认发票内容
			Invoice userdefaultInvoice = invoiceService.getDefaultInvoice(userId);
			request.setAttribute("userdefaultInvoice", userdefaultInvoice);
		}

		/** 根据id获取收货地址信息 **/
		UserAddress userAddress = userAddressService.getDefaultAddress(userId);
		if (AppUtils.isBlank(userAddress)) {
			message.setCode(SubmitOrderStatusEnum.NO_ADDRESS.value());
			return message;
		}
		
		userShopCartList.setUserAddress(userAddress);

		// 处理运费模板
		String result = orderUtil.handlerDelivery(delivery, userShopCartList);
		if (!Constants.SUCCESS.equals(result)) {
			message.setCode(result);
			return message;
		}

		// 验证成功，清除令牌
		request.getSession().removeAttribute(Constants.TOKEN);

		userShopCartList.setUserId(userId);
		userShopCartList.setUserName(userName);
		userShopCartList.setInvoiceId(invoiceId);
		userShopCartList.setUserAddress(userAddress);
		userShopCartList.setPayManner(2);
		userShopCartList.setType(CartTypeEnum.PRESELL.toCode());
		userShopCartList.setActiveId(presellId);

		/**
		 * 处理订单
		 */
		List<String> subIds = null;
		try {
			subIds = orderCartResolverManager.addOrder(CartTypeEnum.PRESELL, userShopCartList);
		} catch (NotStocksException e) {
			message.setCode(SubmitOrderStatusEnum.UNDERSTOCK.value());
			message.setMessage(e.getMessage());
			return message;
		} catch (Exception e) {
			e.printStackTrace();
			if (e instanceof BusinessException) {
				message.setCode(SubmitOrderStatusEnum.ERR.value());
				message.setMessage(e.getMessage());
				return message;
			}
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("下单失败,请联系商城管理员或商城客服");
			return message;
		}

		if (AppUtils.isBlank(subIds)) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("下单失败,请联系商城管理员或商城客服");
			return message;
		} else {
			// 发送网站快照备份
			prodSnapshotProcessor.process(subIds);
		}
		subService.evictUserShopCartCache(CartTypeEnum.PRESELL.toCode(), userId);
		request.getSession().removeAttribute("PRESELL_NUMBER");
		request.getSession().removeAttribute("PRESELL_TOKEN");
		StringBuffer url = new StringBuffer();
		url.append("/p/mobile/presellProd/payDeposit/" + payType + "/" + subIds.get(0));
		message.setResult(true);
		message.setUrl(url.toString());
		return message;
	}
	
	
	/**
	 * 预售前往支付
	 * @param request
	 * @param response
	 * @param pctType  0  全额支付, 1 定金支付的定金  2 定金支付尾款
	 * @param subNumber  订单号
	 * @return
	 */
	@RequestMapping("/payDeposit/{pctType}/{subNumber}")
	public String orderSuccess(HttpServletRequest request, HttpServletResponse response, 
			@PathVariable int pctType, @PathVariable String subNumber) {
		if (!PayPctTypeEnum.FRONT_MONEY.value().equals(pctType) && !PayPctTypeEnum.FINAL_MONEY.value().equals(pctType) && 
				!PayPctTypeEnum.FULL_PAY.value().equals(pctType)) {
			request.setAttribute("message", "非法链接访问");
			return PathResolver.getPath(MobileFrontPage.OPERATION_ERROR);
		}
		
		SecurityUserDetail user = UserManager.getUser(request);
		String userId = user.getUserId();
		
		PresellSub presellSub = presellSubService.getPresellSub(userId,subNumber);
		if (AppUtils.isBlank(presellSub)) {
			request.setAttribute("message", "找不到预售订单");
			return PathResolver.getPath(MobileFrontPage.OPERATION_ERROR);
		}
		
		Date now = new Date();
		//获取订单自动取消的时间
		int cancelMins = subService.getOrderCancelExpireDate();
		if (PayPctTypeEnum.FRONT_MONEY.value().equals(pctType) || PayPctTypeEnum.FULL_PAY.value().equals(pctType)) { // 定金支付或者全额支付
			
			//判断订单是否已经取消
			Sub sub = subService.getSubBySubNumber(subNumber);
			if(AppUtils.isBlank(sub)) {
				request.setAttribute("message", "数据异常，查找不到订单");
				return PathResolver.getPath(MobileFrontPage.OPERATION_ERROR);
			}
			
			int offsetMin = DateUtils.getOffsetMinutes(sub.getSubDate(), now);
			
			if(offsetMin >= cancelMins) {
				request.setAttribute("message", "超过" + cancelMins + "分未付款，订单已取消");
				return PathResolver.getPath(MobileFrontPage.OPERATION_ERROR);
			}
			
			//判断是否在预售期间
			if (presellSub.getPreSaleStart().after(now)) {
				request.setAttribute("message", "预售活动未开始");
				return PathResolver.getPath(MobileFrontPage.OPERATION_ERROR);
			}
			if (presellSub.getPreSaleEnd().before(now)) {
				request.setAttribute("message", "预售活动已过期");
				return PathResolver.getPath(MobileFrontPage.OPERATION_ERROR);
			}		
			
			if (PresellOrderPayStatusEnum.PAYED_DEPOSIT.value().equals(presellSub.getIsPayDeposit())) {//是否支付定金，1代表已经支付
				return PathResolver.getRedirectPath("/p/presellOrder");
			}
			request.setAttribute("totalAmount", presellSub.getTotal().doubleValue());
			request.setAttribute("preDepositPrice", presellSub.getPreDepositPrice().doubleValue());
		} else if(PayPctTypeEnum.FINAL_MONEY.value().equals(pctType)){// 尾款支付
			if (PresellOrderPayStatusEnum.UPAY_DEPOSOIT.value().equals(presellSub.getIsPayDeposit())) {
				request.setAttribute("message", "请先支付定金");
				return PathResolver.getPath(MobileFrontPage.OPERATION_ERROR);
			}
			
			if (PresellOrderPayStatusEnum.PAYED_FINAL.value().equals(presellSub.getIsPayFinal())) {//如果已经支付尾款则不需要重新支付
				return PathResolver.getRedirectPath("/p/presellOrder");
			}
			if (presellSub.getFinalMStart().after(now)) {
				request.setAttribute("message", "尾款支付时间未到");
				return PathResolver.getPath(MobileFrontPage.OPERATION_ERROR);
			}
			if (presellSub.getFinalMEnd().before(now)) {
				request.setAttribute("message", "尾款支付过期");
				return PathResolver.getPath(MobileFrontPage.OPERATION_ERROR);
			}
			request.setAttribute("totalAmount", presellSub.getFinalPrice().doubleValue());
		}
		
		UserDetail userDetail = userDetailService.getUserDetailById(userId);
		
		
		
		//获取 支付方式
		Map<String,Integer> payTypesMap =  payTypeService.getPayTypeMap();
		
		request.setAttribute("payTypesMap", payTypesMap);
		request.setAttribute("cancelMins", cancelMins);
		request.setAttribute("subNums", presellSub.getSubNumber());
		request.setAttribute("userDetail", userDetail);
		request.setAttribute("payPctType", pctType);
		request.setAttribute("subSettlementType", SubSettlementTypeEnum.PRESELL_ORDER_PAY.value());
		request.getSession().removeAttribute(Constants.PASSWORD_CALLBACK);
		return PathResolver.getPath(MobileFrontPage.ORDER_PAY);
	}

}
