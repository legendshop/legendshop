/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.presell.page.front;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 重定向页面定义
 */
public enum PresellRedirectPage implements PageDefinition {
	
	/** 查询预售商品 */
	S_PRESELLPROD_LIST_PAGE("/s/presellProd/query"), 
	
	
	/** 查询商品详情 */
	PROD_VIEWS_PAGE("/views"), 

	;
	/** The value. */
	private final String value;

	
	private PresellRedirectPage(String value, String... template) {
		this.value = value;
	}

	/**
	 * 获取当前页面定义的全路径
	 */
	public String getValue() {
		return getValue(value);
	}

	/**
	 * 根据页面定义计算页面的全路径
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculateActionPath("redirect:", path);
	}

	/**
	 * 实例化一个新的tiles页面
	 * 
	 * @param value
	 */
	private PresellRedirectPage(String value) {
		this.value = value;
	}

	public String getNativeValue() {
		return value;
	}

}
