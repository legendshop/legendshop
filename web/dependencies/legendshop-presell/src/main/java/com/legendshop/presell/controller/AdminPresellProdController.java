/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.presell.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.dto.OperateStatisticsDTO;
import com.legendshop.model.dto.PresellSkuDto;
import com.legendshop.model.entity.*;
import com.legendshop.presell.page.front.PresellFrontPage;
import com.legendshop.security.authorize.AuthorityType;
import com.legendshop.security.authorize.Authorize;
import com.legendshop.spi.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.aop.SystemControllerLog;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.core.base.BaseController;
import com.legendshop.core.constant.PathResolver;
import com.legendshop.core.utils.PageSupportHelper;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PresellProdStatusEnum;
import com.legendshop.model.dto.presell.PresellProdDto;
import com.legendshop.presell.page.admin.PresellAdminBackPage;
import com.legendshop.presell.page.admin.PresellAdminPage;
import com.legendshop.presell.page.admin.PresellAdminRedirectPage;
import com.legendshop.security.UserManager;
import com.legendshop.security.model.SecurityUserDetail;
import com.legendshop.util.AppUtils;

/**
 * 预售活动
 *
 */
@Controller
@RequestMapping("/admin/presellProd")
public class AdminPresellProdController extends BaseController {

	@Autowired
	private PresellProdService presellProdService;

	@Autowired
	private ProductService productService;

	@Autowired
	private PresellSubService presellSubService;

	@Autowired
	private PresellSkuService presellSkuService;

	@Autowired
	private SubService subService;

	@Autowired
	private SkuService skuService;

	@Autowired
	private ShopDetailService shopDetailService;

	/**
	 * 删除预售活动
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/batchDelete/{ids}",method=RequestMethod.DELETE)
	@ResponseBody
	public String batchDelete(HttpServletRequest request, HttpServletResponse response, @PathVariable String ids) {
		try{
			presellProdService.batchDelete(ids);
			return Constants.SUCCESS;
		}catch(Exception e){
			e.printStackTrace();
			return e.getMessage();
		}
	}

	/**
	 * 批量下线预售活动
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="批量下线预售活动")
	@RequestMapping(value = "/batchStop/{ids}",method=RequestMethod.PUT)
	@ResponseBody
	public String batchStop(HttpServletRequest request, HttpServletResponse response, @PathVariable String ids) {
		try{
			presellProdService.batchStop(ids);
			return Constants.SUCCESS;
		}catch(Exception e){
			e.printStackTrace();
			return e.getMessage();
		}
	}


	/**
	 * 查询预售活动
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param isExpires
	 * @param presellProd
	 * @return
	 */
	@RequestMapping(value = "/query", method = RequestMethod.GET)
	public String query(HttpServletRequest request, HttpServletResponse response, String curPageNO, boolean isExpires, PresellProdDto presellProd) {
		request.setAttribute("isExpires", isExpires);
		request.setAttribute("presellProd", presellProd);
		return PathResolver.getPath(PresellAdminPage.PERSELLPROD_LIST_PAGE);
	}

	/**
	 * 查询预售活动列表
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param isExpires
	 * @param presellProd
	 * @return
	 */
	@RequestMapping(value = "/queryContent", method = RequestMethod.GET)
	public String queryContent(HttpServletRequest request, HttpServletResponse response, String curPageNO, boolean isExpires, PresellProdDto presellProd) {
		PageSupport<PresellProdDto> ps = presellProdService.queryPresellProdListPage(curPageNO, isExpires, presellProd);
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("isExpires", isExpires);
		request.setAttribute("presellProd", presellProd);

		return PathResolver.getPath(PresellAdminPage.PERSELLPROD_CONTENT_LIST_PAGE);
	}

	/**
	 * go to 预售活动详情页面
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/toPresellProdDetail/{id}", method = RequestMethod.GET)
	public String toPresellProdDetail(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PresellProdDto presellProd = presellProdService.getPresellProdDto(id);
//		List<Long> skuids = presellSkuService.getSkuIdByPresellId(id);
//		List<Sku> skuList = skuService.getSku(skuids.toArray(new Long[skuids.size()]));
//		presellProd.setSkuList(skuList);
		List<PresellSkuDto> prodLists = productService.queryPresellProductByPresellId(presellProd.getId());
		request.setAttribute("prodLists",prodLists);
		request.setAttribute("presellProd", presellProd);
		return PathResolver.getPath(PresellAdminPage.PERSELLPROD_DETAIL_PAGE);
	}

	/**
	 * 挑选商品弹层
	 *
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param product
	 * @return
	 */
	@RequestMapping(value = "/addProdSkuLayout", method = RequestMethod.GET)
	public String addProdSkuLayout(HttpServletRequest request, HttpServletResponse response, String curPageNO, Product product) {

		SecurityUserDetail user = UserManager.getUser(request);

		PageSupport<Product> ps = productService.getAddProdSkuLayoutPage(curPageNO, product, user.getShopId());
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("prod", product);
		return PathResolver.getPath(PresellAdminBackPage.ADD_PROD_SKU_LAYOUT);
	}

	/**
	 * load 当前选中的商品的sku
	 *
	 * @param request
	 * @param response
	 * @param prodId
	 * @param curPageNO
	 * @return
	 */
	@RequestMapping(value = "/loadProdSku/{prodId}", method = RequestMethod.GET)
	public String loadProdSku(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId, String curPageNO) {
		PageSupport<Sku> ps = skuService.getloadProdSkuPage(curPageNO, prodId);
		PageSupportHelper.savePage(request, ps);
		return PathResolver.getPath(PresellAdminBackPage.LOAD_PROD_SKU_LIST);
	}


	/**
	 * 加载编辑预售活动页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/load", method = RequestMethod.GET)
	public String load(HttpServletRequest request, HttpServletResponse response) {

		return PathResolver.getPath(PresellAdminPage.PRESELLPROD_EDIT_PAGE);
	}


	/**
	 * 加载编辑预售活动页面
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/load/{id}", method = RequestMethod.GET)
	public String load(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PresellProdDto presellProdDto = presellProdService.getPresellProdDto(id);
		request.setAttribute("presellProd", presellProdDto);
		return PathResolver.getPath(PresellAdminPage.PRESELLPROD_EDIT_PAGE);
	}


	/**
	 * 保存预售活动
	 * @param request
	 * @param response
	 * @param presellProd
	 * @return
	 */
	@SystemControllerLog(description="保存预售活动")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(HttpServletRequest request, HttpServletResponse response, PresellProd presellProd) {
		if (AppUtils.isBlank(presellProd)) {
			throw new NullPointerException("presellProd is Null");
		}

		// 校验sku是否重复参与预售
		if (isProdSkuExist(presellProd.getId(), presellProd.getProdId(), presellProd.getSkuId())) {
			throw new BusinessException("presellProd is Exisit");
		}

		// 发货日期转换
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			presellProd.setPreDeliveryTime(dateFormat.parse(presellProd.getPreDeliveryTimeStr()));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (AppUtils.isBlank(presellProd.getId())) {// 添加
			SecurityUserDetail user = UserManager.getUser(request);
			Long shopId = user.getShopId();
			String shopName = shopDetailService.getShopName(shopId);
			presellProd.setShopId(shopId);
			presellProd.setShopName(shopName);
			presellProd.setCreateTime(new Date());
			presellProd.setStatus(PresellProdStatusEnum.ONLINE.value());
			presellProdService.savePresellProd(presellProd);
		} else {// 更新
			PresellProd oldPresellProd = presellProdService.getPresellProd(presellProd.getId());
			if (PresellProdStatusEnum.ONLINE.value().equals(oldPresellProd.getStatus()) && oldPresellProd.getPreSaleStart().before(new Date())) {
				// 当前预售活动已上线并且已开始,不允许修改
				throw new BusinessException("deny update PresellProd");
			}
			if (PresellProdStatusEnum.FINISH.value().equals(oldPresellProd.getStatus())
					|| PresellProdStatusEnum.STOP.value().equals(oldPresellProd.getStatus())) {
				// 当前预售活动已完成或已终止,不允许修改
				throw new BusinessException("deny update PresellProd");
			}
			// 如果是拒绝状态,则修改过后状态变为待审核
			if (PresellProdStatusEnum.DENY.value().equals(oldPresellProd.getStatus())) {
				oldPresellProd.setStatus(PresellProdStatusEnum.WAIT_AUDIT.value());
			}
			if (presellProd.getPayPctType().equals(1L)) {// 支付类型为全额支付
				oldPresellProd.setPreDepositPrice(presellProd.getPreDepositPrice());
				oldPresellProd.setFinalMStart(presellProd.getFinalMStart());
				oldPresellProd.setFinalMEnd(presellProd.getFinalMEnd());
				oldPresellProd.setPayPct(presellProd.getPayPct());
			} else if (presellProd.getPayPctType().equals(0L)) {// 定金支付
				oldPresellProd.setPreDepositPrice(null);
				oldPresellProd.setFinalMStart(null);
				oldPresellProd.setFinalMEnd(null);
				oldPresellProd.setPayPct(null);
			} else {
				throw new BusinessException("PayPctType value is Invalid");
			}
			oldPresellProd.setSchemeName(presellProd.getSchemeName());
			oldPresellProd.setProdId(presellProd.getProdId());
			oldPresellProd.setSkuId(presellProd.getSkuId());
			oldPresellProd.setPrePrice(presellProd.getPrePrice());
			oldPresellProd.setPreSaleStart(presellProd.getPreSaleStart());
			oldPresellProd.setPreSaleEnd(presellProd.getPreSaleEnd());
			oldPresellProd.setPayPctType(presellProd.getPayPctType());
			oldPresellProd.setPreDeliveryTime(presellProd.getPreDeliveryTime());

			presellProdService.updatePresellProd(oldPresellProd);
		}

		saveMessage(request, ResourceBundle.getBundle("i18n/ApplicationResources").getString("operation.successful"));
		return PathResolver.getPath(PresellAdminRedirectPage.PRESELL_PROD_LIST_QUERY);
	}

	/**
	 * go to 审核页面
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/toAudit/{id}", method = RequestMethod.GET)
	public String toAudit(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PresellProdDto presellProd = presellProdService.getPresellProdDto(id);
//		List<Long> skuids = presellSkuService.getSkuIdByPresellId(id);
//		List<Sku> skuList = skuService.getSku(skuids.toArray(new Long[skuids.size()]));
//		presellProd.setSkuList(skuList);
		List<PresellSkuDto> prodLists = productService.queryPresellProductByPresellId(presellProd.getId());
		request.setAttribute("prodLists",prodLists);

		request.setAttribute("presellProd", presellProd);
		return PathResolver.getPath(PresellAdminPage.PRESELLPROD_AUDIT_PAGE);
	}

	/**
	 * 审核预售活动
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="审核预售活动")
	@RequestMapping(value = "/audit", method = RequestMethod.GET)
	@ResponseBody
	public String audit(HttpServletRequest request, HttpServletResponse response, Long id, String auditOpinion, Boolean isAgree) {
		if (AppUtils.isBlank(id) || AppUtils.isBlank(auditOpinion) || AppUtils.isBlank(isAgree)) {
			return "对不起,参数为空或有误!";
		}
		PresellProd presellProd = presellProdService.getPresellProd(id);
		if (AppUtils.isBlank(presellProd)) {
			return "对不起,当前审核的预售活动不存在或已被删除!";
		}
		// 不是待审核状态,或已过期
		if (PresellProdStatusEnum.EXPIRED.value().equals(presellProd.getStatus())){
			return "对不起,该活动已过期!";
		}

		// 不是待审核状态,或已过期
		if (!PresellProdStatusEnum.WAIT_AUDIT.value().equals(presellProd.getStatus())) {
			return "对不起,请不要非法操作!";
		}

		if (isAgree) {// 同意
			// 审核通过设为上线状态
			presellProd.setStatus(PresellProdStatusEnum.ONLINE.value());
		} else {// 不同意
			presellProd.setStatus(PresellProdStatusEnum.DENY.value());
		}
		presellProd.setAuditOpinion(auditOpinion);
		presellProd.setAuditTime(new Date());
		presellProdService.updatePresellProd(presellProd);

		return Constants.SUCCESS;
	}


	/**
	 * 删除预售活动
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String delete(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PresellProd presellProd = presellProdService.getPresellProd(id);

		if (AppUtils.isBlank(presellProd)) {
			return "对不起,当前预售活动不存在,或已被删除!";
		}
		if (presellProd.getStatus().intValue() > PresellProdStatusEnum.ONLINE.value().intValue()
				|| (PresellProdStatusEnum.ONLINE.value().equals(presellProd.getStatus()) && presellProd.getPreSaleStart().before(new Date()))
				|| (presellProd.getStatus().intValue() > PresellProdStatusEnum.ONLINE.value().intValue()
						&& !PresellProdStatusEnum.ONLINE.value().equals(presellProd.getStatus()))) {
			return "对不起,请不要非法操作!";
		}
		presellProdService.deletePresellProd(presellProd);
		return Constants.SUCCESS;
	}

	/**
	 * 终止预售活动
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@SystemControllerLog(description="终止预售活动")
	@RequestMapping(value = "/stop/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String stop(HttpServletRequest request, HttpServletResponse response, @PathVariable Long id) {
		PresellProd presellProd = presellProdService.getPresellProd(id);
		if (AppUtils.isBlank(presellProd)) {
			return "对不起,当前预售活动不存在或已被删除!";
		}
		// 如果当前活动不是正在进行状态
		//|| presellProd.getPreSaleStart().after(new Date())
		if (!PresellProdStatusEnum.ONLINE.value().equals(presellProd.getStatus())) {
			return "对不起,请不要非法操作!";
		}
		presellProd.setStatus(PresellProdStatusEnum.STOP.value());
		presellProdService.updatePresellProd(presellProd);
		return Constants.SUCCESS;
	}

	/**
	 * 检查schemeName是否已存在
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/isSchemeNameExist", method = RequestMethod.GET)
	@ResponseBody
	public Boolean isSchemeNameExist(HttpServletRequest request, HttpServletResponse response, String schemeName) {
		if (AppUtils.isNotBlank(schemeName)) {
			SecurityUserDetail user = UserManager.getUser(request);
			Long shopId = user.getShopId();
			PresellProd presellProd = presellProdService.getPresellProd(schemeName, shopId);
			if (presellProd == null) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断预售商品是否已存在
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/isProdSkuExist", method = RequestMethod.GET)
	@ResponseBody
	public Boolean isProdSkuExist(HttpServletRequest request, HttpServletResponse response, Long presellId, Long prodId, Long skuId) {

		return isProdSkuExist(presellId, prodId, skuId);
	}

	/**
	 * 判断当前sku是否已参与其他营销活动,目前该方法其实只判断有没有参加拍卖活动
	 *
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/isAttendActivity/{prodId}/{skuId}", method = RequestMethod.GET)
	@ResponseBody
	public Boolean isAttendActivity(HttpServletRequest request, HttpServletResponse response, @PathVariable Long prodId, @PathVariable Long skuId) {
		return presellProdService.isAttendActivity(null, prodId, skuId);
	}

	/**
	 * 判断预售商品是否已存在
	 *
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	private Boolean isProdSkuExist(Long presellId, Long prodId, Long skuId) {
		PresellProd presellProd = presellProdService.getPresellProd(prodId, skuId);
		if (AppUtils.isNotBlank(presellId)) {
			return AppUtils.isNotBlank(presellProd) && !presellProd.getId().equals(presellId);
		} else {
			return AppUtils.isNotBlank(presellProd);
		}
	}

	/**
	 *   运营活动
	 */
	@RequestMapping(value = "/operatorPresellActivity/{presellId}", method = RequestMethod.GET)
	public String presellActivity(HttpServletRequest request, HttpServletResponse response, @PathVariable Long presellId, String curPageNO) {
		PresellProd presellProd = presellProdService.getPresellProd(presellId);
		SecurityUserDetail user = UserManager.getUser(request);
		if (AppUtils.isBlank(presellProd)) {
			throw new NullPointerException("presellProd is null");
		}
		// 获取秒杀活动的订单
		if(AppUtils.isBlank(curPageNO) || "0".equals(curPageNO)){
			curPageNO = "1";
		}
		PageSupport<Sub> ps = subService.getPresellOperationList(curPageNO, 6, presellProd.getId(),user.getShopId());
		for (Sub sub : ps.getResultList()) {
			PresellSub presellSub = presellSubService.getPresellSubByPresell(sub.getSubNumber());
			sub.setIsPayFinal(presellSub.getIsPayFinal());
			sub.setIsPayDeposit(presellSub.getIsPayDeposit());
			sub.setPayPctType(presellSub.getPayPctType());
		}
		PageSupportHelper.savePage(request, ps);
		request.setAttribute("presellId",presellId);
		request.setAttribute("presellProd",presellProd);
		return PathResolver.getPath(PresellAdminPage.PRESELLPROD_OPERACTION_PAGE);
	}
}
