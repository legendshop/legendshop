/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.presell.page.admin;

import com.legendshop.core.constant.PageDefinition;
import com.legendshop.core.constant.PagePathCalculator;

/**
 * 预售管理Tiles页面定义
 */
public enum PresellAdminPage implements PageDefinition {

	/** 预售活动管理 */
	PERSELLPROD_LIST_PAGE("/presellProd/presellProdList"),
	
	/** 预售活动列表 */
	PERSELLPROD_CONTENT_LIST_PAGE("/presellProd/presellProdContentList"),

	/** 查看预售活动详情 */
	PERSELLPROD_DETAIL_PAGE("/presellProd/presellProdDetail"),

	/** 编辑竞价活动 */
	PRESELLPROD_EDIT_PAGE("/presellProd/presellProdEdit"),

	/** 审核预售活动 */
	PRESELLPROD_AUDIT_PAGE("/presellProd/presellProdAudit"),

	/** 运营预售活动 */
	PRESELLPROD_OPERACTION_PAGE("/presellProd/presellProdOperation"),
	
	/**预售广告管理 */
	PRESELL_ADV("/presellAdvertise/ad"),
	
	/** 新增预售广告 */
	PRESELL_ADV_ADD("/presellAdvertise/addAdverties"),
	
	
	/** The variable. */
	VARIABLE(""),;

	/** The value. */
	private final String value;

	/**
	 * The Constructor.
	 *
	 * @param value
	 */
	private PresellAdminPage(String value) {
		this.value = value;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public String getValue() {
		return getValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.legendshop.core.constant.PageDefinition#getValue(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse,
	 * java.lang.String, com.legendshop.core.constant.PageDefinition)
	 */
	public String getValue(String path) {
		return PagePathCalculator.calculatePluginBackendPath("presell", path);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.legendshop.core.constant.PageDefinition#getNativeValue()
	 */
	public String getNativeValue() {
		return value;
	}

}
