package com.legendshop.app.test;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;

public class ImageTest {

	public static void main(String[] args) {
		// System.out.println(getImageBinary());

		base64StringToImage(getImageBinary());

		byte[] bytes1 = Base64.decodeBase64(getImageBinary());
		String savePath = "C:/Users/newway/Desktop";
		String saveName = "1112.jpg";
		int flag = saveToImgByStr(bytes1, savePath, saveName);
		System.out.println(flag);
	}

	static String getImageBinary() {
		// File f = new File("C://templates//ttt.jpg");
		File f = new File("C:/Users/newway/Desktop/1111.jpg");

		BufferedImage bi;
		try {
			bi = ImageIO.read(f);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(bi, "jpg", baos);
			byte[] bytes = baos.toByteArray();

			return Base64.encodeBase64String(bytes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	static void base64StringToImage(String base64String) {
		try {
			byte[] bytes1 = Base64.decodeBase64(base64String);

			ByteArrayInputStream bais = new ByteArrayInputStream(bytes1);
			BufferedImage bi1 = ImageIO.read(bais);
			File w2 = new File("C://templates//QQ.bmp");// 可以是jpg,png,gif格式
			ImageIO.write(bi1, "jpg", w2);// 不管输出什么格式图片，此处不需改动
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 将接收的二进制流转换成图片保存
	 * 
	 * @param imgByte
	 *            二进制流
	 * @param imgPath
	 *            图片的保存路径
	 * @param imgName
	 *            图片的名称
	 * @return 1：保存正常 0：保存失败
	 */
	public static int saveToImgByStr(byte[] imgByte, String imgPath, String imgName) {
		int stateInt = 1;
		if (imgByte.length > 0) {
			try {
				File validateCodeFolder = new File(imgPath);
				if (!validateCodeFolder.exists()) {
					validateCodeFolder.mkdirs();
				}
				// 将字符串转换成二进制，用于显示图片
				// 将上面生成的图片格式字符串 imgStr，还原成图片显示
				InputStream in = new ByteArrayInputStream(imgByte);
				File file = new File(imgPath, imgName);// 可以是任何图片格式.jpg,.png等
				FileOutputStream fos = new FileOutputStream(file);
				byte[] b = new byte[1024];
				int nRead = 0;
				while ((nRead = in.read(b)) != -1) {
					fos.write(b, 0, nRead);
				}
				fos.flush();
				fos.close();
				in.close();

			} catch (Exception e) {
				stateInt = 0;
				e.printStackTrace();
			} finally {
			}
		}
		return stateInt;
	}

}