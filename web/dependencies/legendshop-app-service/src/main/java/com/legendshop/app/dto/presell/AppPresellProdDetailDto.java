/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto.presell;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.legendshop.model.dto.app.AppPresellProdDto;
import com.legendshop.model.dto.presell.ProdPropDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app预售商品详情Dto
 */
@ApiModel(value="app预售商品详情Dto")
public class AppPresellProdDetailDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 预售ID */
	@ApiModelProperty(value="预售ID")
	private Long id;

	/** 商品id */
	@ApiModelProperty(value="商品id")
	private Long prodId;

	/** skuId */
	@ApiModelProperty(value="skuId")
	private Long skuId;

	/** 预售价格 */
	@ApiModelProperty(value="预售价格")
	private Double prePrice;

	/** 定金金额 */
	@ApiModelProperty(value="定金百分比")
	private Double preDepositPrice;

	/** 尾款 */
	@ApiModelProperty(value="尾款")
	private Double finalPayment;

	/** 预售开始时间 */
	@ApiModelProperty(value="预售开始时间")
	private Date preSaleStart;

	/** 预售结束时间 */
	@ApiModelProperty(value="预售结束时间")
	private Date preSaleEnd;

	/** 预售支付方式 ,0:全额,1:定金 */
	@ApiModelProperty(value="预售支付方式 ,0:全额,1:定金")
	private Integer payPctType;

	/** 定金占百分比 */
	@ApiModelProperty(value="定金占百分比")
	private Double payPct;

	/** 预售状态 */
	@ApiModelProperty(value="预售状态, -2:未提审,-1:已提审,0:审核通过(上线),1:拒绝,2:已完成,-3:已过期")
	private Long status;
	
	/** 尾款支付开始时间 */
	@ApiModelProperty(value="尾款支付开始时间")
	private Date finalMStart;
	
	/** 尾款支付结束时间 */
	@ApiModelProperty(value="尾款支付结束时间")
	private Date finalMEnd;
	
	/** 预售发货时间 */
	@ApiModelProperty(value="预售发货时间")
	private Date preDeliveryTime;

	// ----------- 商品信息 ------------//
	/** 商品编号 */
	@ApiModelProperty(value="商品编号")
	private String modelId;

	/** 商品名称 */
	@ApiModelProperty(value="商品名称")
	private String prodName;

	/** 商品所属全局分类 */
	@ApiModelProperty(value="商品所属全局分类")
	private Long categoryId;

	/** 商品拥有者 */
	@ApiModelProperty(value="商品拥有者")
	private String userName;

	/** 关键字 */
	@ApiModelProperty(value="关键字")
	private String keyWord;

	/** 简要描述,卖点等 */
	@ApiModelProperty(value="简要描述,卖点等")
	private String brief;

	/** 商品SEOTitle */
	@ApiModelProperty(value="商品SEOTitle")
	private String metaTitle;

	/** 商品原价 */
	@ApiModelProperty(value="商品原价")
	private Double prodPrice;

	/** 商品现价 */
	@ApiModelProperty(value="商品现价")
	private Double prodCash;

	/** 商品主图 */
	@ApiModelProperty(value="商品主图")
	private String prodMainPic;

	/** 是否已收藏 */
	@ApiModelProperty(value="是否已收藏")
	private Boolean isCollect;

	/** 商品数量 */
	@ApiModelProperty(value="商品数量")
	private Integer prodStocks;

	/** 商品条形码 */
	@ApiModelProperty(value="商品条形码")
	private String partyCode;

	/** pc商品详情描述 */
	@ApiModelProperty(value="pc商品详情描述")
	private String content;
	
	/***mobile商品详情描述***/
	@ApiModelProperty(value="mobile商品详情描述")
	private String contentM;

	/** 商品状态 */

	/** 售后服务ID */
	@ApiModelProperty(value="售后服务ID")
	private Long afterSaleId;

	/** 商品所有图片 */
	@ApiModelProperty(value="商品所有图片")
	private List<String> prodPics;

	// ---------- SKU 信息 -----------//

	/** sku名称 */
	@ApiModelProperty(value="sku名称")
	private String skuName;

	/** sku价格 */
	@ApiModelProperty(value="sku价格")
	private Double skuPrice;

	/** sku主图 */
	@ApiModelProperty(value="sku主图")
	private String skuPic;

	/** sku属性 */
	@ApiModelProperty(value="sku属性")
	private String properties;

	/** 属性键值对 */
	@ApiModelProperty(value="属性键值对")
	private Map<String, Long> propKeyValues;

	/** 属性id */
	@ApiModelProperty(value="属性id")
	private List<Long> propIds;

	/** 属性值id */
	@ApiModelProperty(value="属性值id")
	private List<Long> propValueIds;

	/** sku状态 */
	@ApiModelProperty(value="sku状态")
	private Integer skuStatus;

	/** 购买状态：1:进行中,-1:已结束,0:未开始,2:已售完 */
	@ApiModelProperty(value="1:进行中,-1:已结束,0:未开始")
	private Integer buyStatus;

	/** sku 库存 */
	@ApiModelProperty(value="sku 库存")
	private Long skuStocks;

	/** sku实际库存 */
	@ApiModelProperty(value="sku实际库存")
	private Long skuActualStocks;

	/** sku属性集合 */
	@ApiModelProperty(value="sku属性集合")
	private List<ProdPropDto> prodPropDtos;

	/** 活动商品详情 */
	@ApiModelProperty(value="活动商品详情")
	private AppPresellProdDto prodDto;

	/** sku属性值的所有图片URL */
	@ApiModelProperty(value="sku属性值的所有图片URL")
	private List<String> propValImageUrls = new ArrayList<String>();

	// ----------- 运费 -----------------//
	/** 运费模板ID **/
	@ApiModelProperty(value="运费模板ID")
	protected Long transportId;

	/** 商家是否承担运费 0:买家承担,1:卖家承担 */
	@ApiModelProperty(value="商家是否承担运费 0:买家承担,1:卖家承担")
	private Integer supportTransportFree;

	/** 是否固定运费 0:使用运费模板,1:固定运费 */
	@ApiModelProperty(value="是否固定运费 0:使用运费模板,1:固定运费")
	private Integer transportType;

	/** ems运费 */
	@ApiModelProperty(value="ems运费")
	private Double emsTransFee;

	/** express运费 */
	@ApiModelProperty(value="express运费")
	private Double expressTransFee;

	/** 包邮运费 */
	@ApiModelProperty(value="包邮运费")
	private Double mailTransFee;

	/** 物流体积 */
	@ApiModelProperty(value="物流体积")
	private Double volume;

	/** 物流重量 */
	@ApiModelProperty(value="物流重量")
	private Double weight;
	
	// ----------- 商家信息 -----------------//
	
	/** 商家id */
	@ApiModelProperty(value="商家id")
	private Long shopId;
	
	 /** 商城图片. */
    @ApiModelProperty(value = "商城图片")
	private String shopPic;
    
    /** 商城名字. */
	@ApiModelProperty(value = "商城名字")
    protected String siteName;
    
	/**店铺类型 0专营,1旗舰,2自营**/
	@ApiModelProperty(value = "店铺类型 0专营,1旗舰,2自营")
	protected Integer shopType;
	
	/** 店铺评分 */
	@ApiModelProperty(value="店铺评分")
	private String shopScore;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Double getPrePrice() {
		return prePrice;
	}

	public void setPrePrice(Double prePrice) {
		this.prePrice = prePrice;
	}

	public Double getPreDepositPrice() {
		return preDepositPrice;
	}

	public void setPreDepositPrice(Double preDepositPrice) {
		this.preDepositPrice = preDepositPrice;
	}

	public Double getFinalPayment() {
		return finalPayment;
	}

	public void setFinalPayment(Double finalPayment) {
		this.finalPayment = finalPayment;
	}

	public Date getPreSaleStart() {
		return preSaleStart;
	}

	public void setPreSaleStart(Date preSaleStart) {
		this.preSaleStart = preSaleStart;
	}

	public Date getPreSaleEnd() {
		return preSaleEnd;
	}

	public void setPreSaleEnd(Date preSaleEnd) {
		this.preSaleEnd = preSaleEnd;
	}

	public Integer getPayPctType() {
		return payPctType;
	}

	public void setPayPctType(Integer payPctType) {
		this.payPctType = payPctType;
	}

	public Double getPayPct() {
		return payPct;
	}

	public void setPayPct(Double payPct) {
		this.payPct = payPct;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public String getMetaTitle() {
		return metaTitle;
	}

	public void setMetaTitle(String metaTitle) {
		this.metaTitle = metaTitle;
	}

	public Double getProdPrice() {
		return prodPrice;
	}

	public void setProdPrice(Double prodPrice) {
		this.prodPrice = prodPrice;
	}

	public Double getProdCash() {
		return prodCash;
	}

	public void setProdCash(Double prodCash) {
		this.prodCash = prodCash;
	}

	public String getProdMainPic() {
		return prodMainPic;
	}

	public void setProdMainPic(String prodMainPic) {
		this.prodMainPic = prodMainPic;
	}

	public Integer getProdStocks() {
		return prodStocks;
	}

	public void setProdStocks(Integer prodStocks) {
		this.prodStocks = prodStocks;
	}

	public String getPartyCode() {
		return partyCode;
	}

	public void setPartyCode(String partyCode) {
		this.partyCode = partyCode;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContentM() {
		return contentM;
	}

	public void setContentM(String contentM) {
		this.contentM = contentM;
	}

	public Long getAfterSaleId() {
		return afterSaleId;
	}

	public void setAfterSaleId(Long afterSaleId) {
		this.afterSaleId = afterSaleId;
	}

	public List<String> getProdPics() {
		return prodPics;
	}

	public void setProdPics(List<String> prodPics) {
		this.prodPics = prodPics;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public Double getSkuPrice() {
		return skuPrice;
	}

	public void setSkuPrice(Double skuPrice) {
		this.skuPrice = skuPrice;
	}

	public String getSkuPic() {
		return skuPic;
	}

	public void setSkuPic(String skuPic) {
		this.skuPic = skuPic;
	}

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}

	public Map<String, Long> getPropKeyValues() {
		return propKeyValues;
	}

	public void setPropKeyValues(Map<String, Long> propKeyValues) {
		this.propKeyValues = propKeyValues;
	}

	public List<Long> getPropIds() {
		return propIds;
	}

	public void setPropIds(List<Long> propIds) {
		this.propIds = propIds;
	}

	public List<Long> getPropValueIds() {
		return propValueIds;
	}

	public void setPropValueIds(List<Long> propValueIds) {
		this.propValueIds = propValueIds;
	}

	public Integer getSkuStatus() {
		return skuStatus;
	}

	public void setSkuStatus(Integer skuStatus) {
		this.skuStatus = skuStatus;
	}

	public Long getSkuStocks() {
		return skuStocks;
	}

	public void setSkuStocks(Long skuStocks) {
		this.skuStocks = skuStocks;
	}

	public Long getSkuActualStocks() {
		return skuActualStocks;
	}

	public void setSkuActualStocks(Long skuActualStocks) {
		this.skuActualStocks = skuActualStocks;
	}

	public List<ProdPropDto> getProdPropDtos() {
		return prodPropDtos;
	}

	public void setProdPropDtos(List<ProdPropDto> prodPropDtos) {
		this.prodPropDtos = prodPropDtos;
	}

	public List<String> getPropValImageUrls() {
		return propValImageUrls;
	}

	public void setPropValImageUrls(List<String> propValImageUrls) {
		this.propValImageUrls = propValImageUrls;
	}

	public Long getTransportId() {
		return transportId;
	}

	public void setTransportId(Long transportId) {
		this.transportId = transportId;
	}

	public Integer getSupportTransportFree() {
		return supportTransportFree;
	}

	public void setSupportTransportFree(Integer supportTransportFree) {
		this.supportTransportFree = supportTransportFree;
	}

	public Integer getTransportType() {
		return transportType;
	}

	public void setTransportType(Integer transportType) {
		this.transportType = transportType;
	}

	public Double getEmsTransFee() {
		return emsTransFee;
	}

	public void setEmsTransFee(Double emsTransFee) {
		this.emsTransFee = emsTransFee;
	}

	public Double getExpressTransFee() {
		return expressTransFee;
	}

	public void setExpressTransFee(Double expressTransFee) {
		this.expressTransFee = expressTransFee;
	}

	public Double getMailTransFee() {
		return mailTransFee;
	}

	public void setMailTransFee(Double mailTransFee) {
		this.mailTransFee = mailTransFee;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getShopPic() {
		return shopPic;
	}

	public void setShopPic(String shopPic) {
		this.shopPic = shopPic;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public Integer getShopType() {
		return shopType;
	}

	public void setShopType(Integer shopType) {
		this.shopType = shopType;
	}

	public String getShopScore() {
		return shopScore;
	}

	public void setShopScore(String shopScore) {
		this.shopScore = shopScore;
	}

	public Date getFinalMStart() {
		return finalMStart;
	}

	public void setFinalMStart(Date finalMStart) {
		this.finalMStart = finalMStart;
	}

	public Date getFinalMEnd() {
		return finalMEnd;
	}

	public void setFinalMEnd(Date finalMEnd) {
		this.finalMEnd = finalMEnd;
	}

	public Date getPreDeliveryTime() {
		return preDeliveryTime;
	}

	public void setPreDeliveryTime(Date preDeliveryTime) {
		this.preDeliveryTime = preDeliveryTime;
	}

	public AppPresellProdDto getProdDto() {
		return prodDto;
	}

	public void setProdDto(AppPresellProdDto prodDto) {
		this.prodDto = prodDto;
	}

	public Integer getBuyStatus() {
		return buyStatus;
	}

	public void setBuyStatus(Integer buyStatus) {
		this.buyStatus = buyStatus;
	}

	public Boolean getIsCollect() {
		return isCollect;
	}

	public void setIsCollect(Boolean collect) {
		isCollect = collect;
	}
}
