package com.legendshop.app.controller;

/**
 * 普通登录和短信登录
 *
 */

import com.legendshop.app.dto.AppUserRegDto;
import com.legendshop.base.util.HttpUtil;
import com.legendshop.base.util.ValidationUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.entity.SystemConfig;
import com.legendshop.model.entity.User;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.form.UserMobileForm;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.SMSLogService;
import com.legendshop.spi.service.SystemConfigService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.web.helper.IPHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@RestController
@Api(tags="用户注册",value="用户注册相关接口")
public class AppRegisterController {

	private static Logger logger = LoggerFactory.getLogger(AppRegisterController.class);
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private SMSLogService smsLogService;
	
	@Autowired
	private SystemConfigService systemConfigService;
	
    @Autowired
    private PasswordEncoder passwordEncoder;
    
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	//环信注册URL
	private static String IMREG_URL = "https\\://a1.easemob.com/legendshop/legendshop/users" ;

	
	/**
	 * 用户注册
	 * @param request
	 * @param response
	 * @return
	 */
	@ApiOperation(value = "用户注册", httpMethod = "POST", notes = "用户注册,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/userReg")
	public ResultDto<String> userReg(HttpServletRequest request, HttpServletResponse response, 
			AppUserRegDto userRegDto) {
		try {
			
			if(AppUtils.isBlank(userRegDto.getMobile())){
				return ResultDtoManager.fail(-4, "手机号不能为空");
			}
			
			if(AppUtils.isBlank(userRegDto.getMobileCode())){
				return ResultDtoManager.fail(-4, "手机验证码不能为空");
			}
			
			if(AppUtils.isBlank(userRegDto.getPassword())){
				return ResultDtoManager.fail(-4, "密码不能为空");
			}
			
			if(AppUtils.isNotBlank(userRegDto.getUserName())){
				if(!ValidationUtil.checkUserName(userRegDto.getUserName())){
					return ResultDtoManager.fail(-4, "用户名可以是4-16位的汉字、数字、字母和_，不能以 数字和_开头");
				}
			}
			
			//0: 检查验证码
			if(!smsLogService.verifyCodeAndClear(userRegDto.getMobile(), userRegDto.getMobileCode())){
				return ResultDtoManager.fail(0, "手机验证码错误");
			}
			
			if(AppUtils.isNotBlank(userRegDto.getNickName())){
				// -2 检查昵称是否重名，如果有昵称的话
				boolean existedNickName = userDetailService.isNickNameExist(userRegDto.getNickName());
				if(existedNickName){
					return ResultDtoManager.fail(-2, "用户名已存在");
				}
			}
			
			// -1：检查是否重名
			boolean existedMobile = userDetailService.isPhoneExist(userRegDto.getMobile());
			if(existedMobile){
				return ResultDtoManager.fail(-1, "手机号已注册");
			}
			
			//保存用户
			UserMobileForm userMobileForm = new UserMobileForm();
			userMobileForm.setUserName(userRegDto.getUserName());
			userMobileForm.setNickName(userRegDto.getNickName());
			userMobileForm.setPassword(userRegDto.getPassword());
			userMobileForm.setMobile(userRegDto.getMobile());
			userMobileForm.setMobileCode(userRegDto.getMobile());
			userMobileForm.setParentUserName(userRegDto.getParentUserName());
			
			User user = userDetailService.saveUserReg(IPHelper.getIpAddr(request), userMobileForm,passwordEncoder.encode(userMobileForm.getPassword()));
			
			if(user==null){
				//注册失败
				return ResultDtoManager.fail(-3, "注册失败，请重新注册");
			}
			
			//手机注册的用户，用户安全等级+1,手机验证变为1
			userDetailService.updatePhoneVerifn(user.getName());
			
			return ResultDtoManager.success("success");//注册成功
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDtoManager.fail(-3, "注册失败，请重新注册");
		}
	}
	
	/**
	 * 判断用户名是否存在
	 * @param userName
	 * @return
	 */
	@ApiOperation(value = "判断用户名是否存在", httpMethod = "POST", notes = "判断用户名是否存在，返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="path", name = "userName", value = "用户名", required = true, dataType = "String")
	@PostMapping(value="/isUserNameExist")
	public  ResultDto<Object> isNickNameExist(@RequestParam String userName) { 
		
		boolean result = userDetailService.isUserExist(userName);
		return  ResultDtoManager.success(result);
	}
	
	/**
	 * 判断手机号码是否存在
	 * @param phone
	 * @return
	 */
	@ApiOperation(value = "判断手机号码是否存在", httpMethod = "POST", notes = "判断手机号码是否存,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "phone", value = "手机号码", required = true, dataType = "String")
	@PostMapping(value="/isPhoneExist")
	public  ResultDto<Object> isPhoneExist(@RequestParam String phone) {
		
		boolean result=userDetailService.isPhoneExist(phone);
		return  ResultDtoManager.success(result);
	}
	
	/**
	 * 找回密码
	 * @param request
	 * @param response
	 * @param code
	 * @param newPwd
	 * @param mobile
	 * @return
	 */
	 @ApiOperation(value = "找回密码", httpMethod = "POST", notes = "找回密码,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
     @ApiImplicitParams({
    	@ApiImplicitParam(paramType="query", name = "code", value = "手机验证码", required = true, dataType = "string"),
    	@ApiImplicitParam(paramType="query", name = "newPwd", value = "新密码", required = false, dataType = "string"),
    	@ApiImplicitParam(paramType="query", name = "mobile", value = "手机号码(1:PC 2:android 3:wap 4:IOS)", required = true, dataType = "string")
     })
	 @PostMapping(value="/forgetPwd")
	public  ResultDto<String> forgetPwd(HttpServletRequest request, HttpServletResponse response,@RequestParam String code,@RequestParam String newPwd,@RequestParam String mobile){
		if(!smsLogService.verifyCodeAndClear(mobile, code)){
			return ResultDtoManager.fail(0, "验证码错误");
		}
		User user = userDetailService.getUserByMobile(mobile);
		if(user==null){
			return ResultDtoManager.fail(0, "手机号不存在");
		}else{
			user.setPassword(passwordEncoder.encode(newPwd));
			userDetailService.updateUser(user);
			return ResultDtoManager.success(Constants.SUCCESS);
		}
	}

	/**
	 * 注册协议
	 * @param request
	 * @param response
	 * @return
	 */
	@ApiOperation(value = "注册协议", httpMethod = "POST", notes = "注册协议,返回注册协议文本",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/registerAgreement")
	public ResultDto<String> registerAgreement(HttpServletRequest request, HttpServletResponse response) {
		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		String content = systemConfig.getRegProtocolTemplate();
		return ResultDtoManager.success(content);
	}
	
	/**
	 * 获取商家的IM，如果没有，就注册
	 * @param shopId
	 * @return
	 */
	@ApiIgnore
	@ApiOperation(value = "获取商家的IM", httpMethod = "POST", notes = "获取商家的IM，如果没有，就注册",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="path", name = "shopId", value = "手机号码", required = true, dataType = "Long")
	@PostMapping(value="/p/getShopIMId/{shopId}")
	public ResultDto<String> getShopIMId(@PathVariable Long shopId){
		String userId = loginedUserService.getUser().getUserId();
		UserDetail userDetail=userDetailService.getUserDetailById(userId);//当前用户
		if(AppUtils.isBlank(userId)){
			return ResultDtoManager.fail(-1, "请登录用户");
		}
		if(AppUtils.isBlank(userDetail)){
			return ResultDtoManager.fail(-1, "开启失败,用户不存在");
		}
		
		UserDetail shopUserDetail=userDetailService.getUserDetailByShopId(shopId);//商家用户
		if(AppUtils.isBlank(shopUserDetail)){
			return ResultDtoManager.fail(-1, "开启失败,该商家不存在");
		}
		
		if(AppUtils.isBlank(shopUserDetail.getIsRegIM())){
			return ResultDtoManager.fail(-1, "环信失败");
		}
		int userIsRegIM=0;
		int shopIsRegIM=0;
		if(AppUtils.isNotBlank(shopUserDetail.getIsRegIM())){
			shopIsRegIM=shopUserDetail.getIsRegIM();
		}
		if(AppUtils.isNotBlank(userDetail.getIsRegIM())){
			userIsRegIM=userDetail.getIsRegIM();
		}
		
		if(shopIsRegIM==1&&userIsRegIM==1
				){ //商家、当前用户已注册环信ID
			return ResultDtoManager.success(shopUserDetail.getUserName());
		}
		Map<String, Object> paramMap = new HashMap<String, Object>();
		if(shopIsRegIM==0){ //店家
			logger.info("/p/getShopIMId-----------shop no reg----------");
			paramMap.put("username", shopUserDetail.getUserName());
			paramMap.put("password",shopUserDetail.getUserName()+"123456");
		}
		
		if(userIsRegIM==0){ //当前用户
			logger.info("/p/getShopIMId-----------currreUser no reg----------");
			paramMap.put("username", userDetail.getUserName());
			paramMap.put("password",userDetail.getUserName()+"123456");
		}
		
		
		String result = HttpUtil.httpPostJSON(IMREG_URL, paramMap);
		logger.info("/p/getShopIMId   request IM result="+result);
		if(AppUtils.isBlank(result)){
			return ResultDtoManager.fail(-1, "环信失败");
		}
		
		if(userIsRegIM==0){
			userDetail.setIsRegIM(1);//当前用户标记注册环信账号
			userDetailService.updateUserDetail(userDetail);
		}
		
		if(shopIsRegIM==0){
			UserDetail shopUser=userDetailService.getUserDetailById(shopUserDetail.getUserId());
			shopUser.setIsRegIM(1);//商家标记注册环信账号
			userDetailService.updateUserDetail(shopUser);
		}
		
		return ResultDtoManager.success(shopUserDetail.getUserName());
	}
	
}
