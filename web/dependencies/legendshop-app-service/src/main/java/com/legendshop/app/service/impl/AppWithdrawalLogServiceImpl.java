/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.AppWithdrawalLogDto;
import com.legendshop.app.service.AppWithdrawalLogService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.business.dao.PdCashLogDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.PdCashLogEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.util.AppUtils;

/**
 * 预存款日志服务实现类
 */
@Service("appWithdrawalLogService")
public class AppWithdrawalLogServiceImpl implements AppWithdrawalLogService {

	@Autowired
	private PdCashLogDao pdCashLogDao;

	/**
	 * 查询分销的历史转账.
	 *
	 * @param curPageNO
	 * @param pageSize
	 * @return the pd cash log
	 */
	@Override
	public AppPageSupport<AppWithdrawalLogDto> getPdCashLog(String curPageNO, int pageSize,String userId) {
		PageSupport<PdCashLog> ps = pdCashLogDao.getPdCashLog(curPageNO, pageSize, userId, PdCashLogEnum.PROMOTER.value());
		AppPageSupport<AppWithdrawalLogDto> convertPageSupport = PageUtils.convertPageSupport(ps,new PageUtils.ResultListConvertor<PdCashLog, AppWithdrawalLogDto>() {
			@Override
			public List<AppWithdrawalLogDto> convert(List<PdCashLog> form) {
				List<AppWithdrawalLogDto> toList = new ArrayList<AppWithdrawalLogDto>();
				for (PdCashLog pdCashLog : form) {
					if (AppUtils.isNotBlank(pdCashLog)) {
						AppWithdrawalLogDto logDto =  convertToAppWithdrawalLogDto(pdCashLog);
						toList.add(logDto);
					}
				}
				return toList;
			}
			
			private AppWithdrawalLogDto convertToAppWithdrawalLogDto(PdCashLog pdCashLog){
				  AppWithdrawalLogDto appWithdrawalLogDto = new AppWithdrawalLogDto();
				  appWithdrawalLogDto.setAddTime(pdCashLog.getAddTime());
				  appWithdrawalLogDto.setAmount(pdCashLog.getAmount());
				  appWithdrawalLogDto.setId(pdCashLog.getId());
				  appWithdrawalLogDto.setLogDesc(pdCashLog.getLogDesc());
				  appWithdrawalLogDto.setLogType(pdCashLog.getLogType());
				  appWithdrawalLogDto.setLogTypeName(PdCashLogEnum.getDesc(pdCashLog.getLogType()));
				  return appWithdrawalLogDto;
				}
		});
		return convertPageSupport;
	}
	
}
