/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.util;

import java.util.Date;

import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.entity.AppToken;
import com.legendshop.util.AppUtils;
import com.legendshop.util.MD5Util;

/**
 * 校验参数和Token等
 *
 */
public class AppTokenUtil {
	/**
	 *  验证签名和AppToken的合法性
	 * 
	 */
	public static ResultDto<Object> checkAppToken(AppToken appToken, String userId, String verId, String sign, String accessToken, String securiyCode) {

		ResultDto<Object> result = new ResultDto<Object>();
		result.setVerId(verId);
		if (AppUtils.isBlank(appToken) || AppUtils.isBlank(userId) || AppUtils.isBlank(accessToken) || AppUtils.isBlank(securiyCode) || AppUtils.isBlank(verId)) {
			result.setStatus(401);
			result.setMsg("您的请求被拒绝 请登入后重试!");
			return result;
		}

		StringBuffer content = new StringBuffer().append(userId).append(accessToken).append(verId).append(securiyCode);
		String _sign = MD5Util.toMD5(content.toString());

		if (!_sign.equals(sign)) {
			result.setStatus(401);
			result.setMsg("签名无效!");
			return result;
		}

		if (!appToken.getAccessToken().equals(accessToken) || !appToken.getSecuriyCode().equals(securiyCode)) {
			result.setStatus(401);
			result.setMsg("用户认证失败，请重新登录!");
			return result;
		}

		Date curDate = new Date();
		Date startDate = appToken.getStartDate();

		// 获取token从开始到现在相隔的时间, 单位秒
		long diffTime = (curDate.getTime() - startDate.getTime()) / 1000;
		if (diffTime > appToken.getValidateTime()) {// 失效时间是两周
			result.setStatus(401);
			result.setMsg("用户登录信息已过期，请重新登录!");
			return result;
		}

		result.setStatus(1);
		result.setMsg("验证成功!");

		return result;
	}
}
