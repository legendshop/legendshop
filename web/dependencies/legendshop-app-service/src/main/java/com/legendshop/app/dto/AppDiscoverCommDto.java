package com.legendshop.app.dto;

import java.util.Date;
import java.util.List;

import com.legendshop.model.entity.DiscoverComme;
import com.legendshop.model.entity.DiscoverProd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("发现文章评论Dto")
public class AppDiscoverCommDto {
	
	@ApiModelProperty(value="评论id")
	private Long id;
	
	@ApiModelProperty(value="评论内容")
	private String content;
	
	@ApiModelProperty(value="评论时间")
	private Date createTime;
	
	@ApiModelProperty(value="文章id")
	private Long disId	;
	
	@ApiModelProperty(value="用户id")
	private String userId;
	
	@ApiModelProperty(value="用户头像")
	private String userImage;
	
	@ApiModelProperty(value="用户昵称")
	private String userName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getDisId() {
		return disId;
	}

	public void setDisId(Long disId) {
		this.disId = disId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserImage() {
		return userImage;
	}

	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
