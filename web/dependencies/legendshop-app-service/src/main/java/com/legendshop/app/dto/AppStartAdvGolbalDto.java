package com.legendshop.app.dto;

import java.util.List;

import com.legendshop.model.dto.app.AppStartAdvDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * APP启动广告图, 暂时已经没在用了
 */
@Deprecated
@ApiModel(value="AppStartAdvGolbalDto") 
public class AppStartAdvGolbalDto {
	
	/** 广告版本号 */
	@ApiModelProperty(value="广告版本号")  
	private String version;
	
	/** 是否已改变 */
	@ApiModelProperty(value="是否已改变")  
	private Boolean isChange;
	
	/** app启动广告列表 */
	@ApiModelProperty(value="app启动广告列表")  
	private List<AppStartAdvDto> appStartAdvList;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Boolean getIsChange() {
		return isChange;
	}

	public void setIsChange(Boolean isChange) {
		this.isChange = isChange;
	}

	public List<AppStartAdvDto> getAppStartAdvList() {
		return appStartAdvList;
	}

	public void setAppStartAdvList(List<AppStartAdvDto> appStartAdvList) {
		this.appStartAdvList = appStartAdvList;
	}

}
