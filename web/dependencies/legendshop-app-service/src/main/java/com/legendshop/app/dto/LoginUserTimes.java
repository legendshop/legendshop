package com.legendshop.app.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 在缓存中记录登录错误的次数
 * @author Administrator
 *
 */
public class LoginUserTimes implements Serializable{

	/**
	 * 登录时间
	 */
	private Date loginTime;

	/**
	 * 登录错误次数
	 */
	private Integer errorNum;
	
	public LoginUserTimes(){
		
	}
	
	public LoginUserTimes(Date loginTime, Integer errorNum){
		this.loginTime= loginTime;
		this.errorNum = errorNum;
	}
	
	public Date getLoginTime() {
		return loginTime;
	}
	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}
	public Integer getErrorNum() {
		return errorNum;
	}
	public void setErrorNum(Integer errorNum) {
		this.errorNum = errorNum;
	}
	
}