package com.legendshop.app.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.service.AppFavoriteShopService;
import com.legendshop.app.service.MyFavoriteService;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.app.AppFavoriteShopDto;
import com.legendshop.model.dto.app.AppMyFavoriteDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.util.AppUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
/**
 * app我的收藏
 */
@RestController
@RequestMapping("/p")
@Api(tags="收藏管理",value="我的收藏相关，商品收藏，店铺收藏")
public class AppMyFavoriteController{
	
	private static Logger LOGGER = LoggerFactory.getLogger(AppMyFavoriteController.class);
	
	@Autowired
	private MyFavoriteService myfavoriteService;
	
	@Autowired
	private AppFavoriteShopService appFavoriteShopService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	/**
	 * 收藏的商品列表
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@ApiOperation(value = "收藏的商品列表", httpMethod = "POST", notes = "获取我收藏的商品",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = false, dataType = "string")
	@PostMapping("/favorite/query")
	public ResultDto<AppPageSupport<AppMyFavoriteDto>> queryMyFavorite(String curPageNO) {
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			
			AppPageSupport<AppMyFavoriteDto>  result = myfavoriteService.getAppFavoriteList(curPageNO,systemParameterUtil.getPageSize(), userId);
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("获取收藏商品列表异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 收藏的店铺列表
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @param userId
	 * @return
	 */
	@ApiOperation(value = "收藏的店铺列表", httpMethod = "POST", notes = "获取收藏的店铺列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = false, dataType = "string")
	@PostMapping("/favoriteshop/query")
	public ResultDto<AppPageSupport<AppFavoriteShopDto>> queryFavoriteShop(String curPageNO) {
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			
			AppPageSupport<AppFavoriteShopDto> result = appFavoriteShopService.queryFavoriteShop(curPageNO, null, userId);
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("获取收藏店铺列表异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	
	/**
	 * 收藏或取消收藏商品
	 * @param prodId
	 * @return
	 */
	@ApiOperation(value = "收藏或取消收藏商品", httpMethod = "POST", notes = "收藏或取消收藏商品,成功返回'收藏成功'或'取消成功'，返回'收藏失败'或'取消失败'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "prodId", value = "商品id", required = true, dataType = "Long")
	@PostMapping("/favorite/saveOrCancel")
	public ResultDto<String> save(@RequestParam Long prodId){
		
		Boolean result = false;
		
		String userId=loginedUserService.getUser().getUserId();
		String userName=loginedUserService.getUser().getUserName();
		
		if(AppUtils.isBlank(userId)||AppUtils.isBlank(userName)){
			return ResultDtoManager.fail(0,Constants.FAIL);
		}
		
		boolean isExistsFavorite = myfavoriteService.isExistsFavorite(prodId,userName);
		if (isExistsFavorite) {//已收藏
			
			result = myfavoriteService.cancelFavorite(prodId, userId);//取消收藏
			if(result){
				return ResultDtoManager.success("取消成功");
			}else{
				return ResultDtoManager.fail(-1,"取消失败");
			}
		}
			
		result = myfavoriteService.collectProduct(prodId,userName,userId);//添加收藏
		if(result){
			return ResultDtoManager.success("收藏成功");
		}else{
			return ResultDtoManager.fail(-1,"收藏失败");
		}
			
	}
	
	/**
	 * @Description 取消收藏商品/批量取消收藏商品
	 * @param selectedFavs
	 * @return
	 */
	@ApiOperation(value = "取消收藏商品/批量取消收藏商品", httpMethod = "POST", notes = "取消收藏商品/批量取消收藏商品,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "selectedFavs", value = "选中的收藏id,多个已‘;’分割传递到后台", required = true, dataType = "String")
	@PostMapping("/favorite/delete")
	public ResultDto<Object> delete(@RequestParam String selectedFavs) {
		String userId=loginedUserService.getUser().getUserId();
		if(AppUtils.isBlank(userId)){
			return ResultDtoManager.fail(0, "userId error");
		}
		myfavoriteService.deleteFavs(userId, selectedFavs);
		return ResultDtoManager.success(true);
	}
	
	
	/**
	 * @Description 取消收藏店铺/批量取消收藏店铺
	 * @param request
	 * @param response
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "取消收藏店铺/批量取消收藏店铺", httpMethod = "POST", notes = "取消收藏店铺/批量取消收藏店铺,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "fsIds", value = "选中的收藏id,多个已‘;’分割传递到后台", required = true, dataType = "String")
	@PostMapping("/favoriteshop/deleteBatch")
	public ResultDto<Object> deleteBatch(HttpServletRequest request, HttpServletResponse response, String fsIds) {
		String userId=loginedUserService.getUser().getUserId();
		if(AppUtils.isBlank(userId)){
			return ResultDtoManager.fail(-1, "未登录");
		}
		appFavoriteShopService.deleteFavoriteShops(fsIds, userId);
		return ResultDtoManager.success(true);
	}
	
	/**
	 * 批量添加收藏
	 * @param prodId
	 * @return
	 */
	@ApiOperation(value = "批量添加收藏商品", httpMethod = "POST", notes = "批量添加收藏商品，返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "selectedProdId", value = "选中的prodId,多个以‘;’分割传递到后台", required = true, dataType = "List")
	@PostMapping("/favorite/saves")
	public ResultDto<Object> saves(String selectedProdId){
		
		try {
			String userId=loginedUserService.getUser().getUserId();
			String userName=loginedUserService.getUser().getUserName();
			
			if(AppUtils.isBlank(userId)||AppUtils.isBlank(userName)){
				return ResultDtoManager.fail(-1,"请先登录");
			}
			boolean result = myfavoriteService.collectProducts(selectedProdId,userName,userId);
			if(result)
				return ResultDtoManager.success();
			else
				return ResultDtoManager.fail();
		} catch (Exception e) {
			LOGGER.error("批量添加收藏异常异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 删除我的店铺收藏
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	/*@ApiOperation(value = "删除收藏店铺", httpMethod = "POST", notes = "删除收藏店铺,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "id", value = "id", required = true, dataType = "Long")
	@RequestMapping(value = "/favoriteshop/delete", method = RequestMethod.POST)
	
	public ResultDto<Object> delete(HttpServletRequest request, HttpServletResponse response, Long id) {
		String userId=loginedUserService.getUser().getUserId();
		if(AppUtils.isBlank(userId)){
			return ResultDtoManager.fail(-1, "未登录");
		}
		if(appFavoriteShopService.deleteFavoriteShop(id, userId)){
			return ResultDtoManager.success();
		}
		return ResultDtoManager.fail(-1, "删除收藏的店铺失败");
	}*/

}
