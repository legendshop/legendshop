/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.AppCheckGroupDto;
import com.legendshop.app.dto.AppUserShopCartOrderDto;
import com.legendshop.app.dto.GroupOrderDetailsParamsDto;
import com.legendshop.app.service.AppCartOrderService;
import com.legendshop.app.service.AppCategoryService;
import com.legendshop.app.service.AppGroupService;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.framework.cache.client.CacheClient;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.GroupStatusEnum;
import com.legendshop.model.constant.ProductTypeEnum;
import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.group.AppGroupCategoryDto;
import com.legendshop.model.dto.group.AppGroupDetailDto;
import com.legendshop.model.dto.group.AppGroupDto;
import com.legendshop.model.dto.group.GroupDto;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.Group;
import com.legendshop.model.entity.Invoice;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.spi.service.GroupService;
import com.legendshop.spi.service.InvoiceService;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.OrderCartResolverManager;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.StockService;
import com.legendshop.spi.service.SubService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.MD5Util;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 团购 APP控制器
 */
@RestController
@Api(tags="团购活动",value="团购活动相关接口")
@RequestMapping
public class AppGroupController {

	private final Logger log = LoggerFactory.getLogger(AppGroupController.class);

	@Autowired
	private AppGroupService appGroupService;

	@Autowired
	private GroupService groupService;

	@Autowired
	private StockService stockService;

	@Autowired
	private SubService subService;

	@Autowired
	private InvoiceService invoiceService;

	@Autowired
	private OrderCartResolverManager orderCartResolverManager;
	
	@Autowired
	private ShopDetailService shopDetailService;
	
	@Autowired
	private AppCartOrderService appCartOrderService;

	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private AppCategoryService appCategoryService;
	
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;
	
	@Autowired
	private CacheClient cacheClient;
	
	/**
	 * 团购活动列表, 暂时不做排序, 所以注释掉
	 * @param curPageNO
	 * @param 排序规则 销量: buyer_count,asc(desc), 价格: group_price, asc(desc)
	 * @return
	 */
	@ApiOperation(value = "团购活动列表", httpMethod = "POST", notes = "团购活动列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "curPageNO", value = "分页", required = false, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "groupCategoryId", value = "团购分类ID", required = false, dataType = "Long")
	})
	@PostMapping("/app/group/list")
	public ResultDto<AppPageSupport<AppGroupDto>> groupList(String curPageNO,Long groupCategoryId) {
		try {
			AppPageSupport<AppGroupDto> ps = appGroupService.queryGroupPage(curPageNO, "buyer_count,asc",groupCategoryId);
			return ResultDtoManager.success(ps);
		} catch (Exception e) {
			log.error("查看团购列表异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 团购活动详情
	 * @param id 团购活动ID
	 * @return
	 */
	@ApiOperation(value = "团购活动详情", httpMethod = "POST", notes = "团购活动详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "id", value = "团购活动id", required = true, dataType = "Long")
	@PostMapping("/app/group/views")
	public ResultDto<AppGroupDetailDto> view(@RequestParam(required = true) Long id) {
		
		try {
			GroupDto groupdto = groupService.getGroupDto(id);
			if (AppUtils.isBlank(groupdto)) {
				return ResultDtoManager.fail(-1, "团购商品不存在！");
			}
			ShopDetail shopDetail = shopDetailService.getShopDetailByIdNoCache(groupdto.getShopId());
			if(AppUtils.isBlank(shopDetail) || shopDetail.getStatus() != ShopStatusEnum.NORMAL.value()){
				return ResultDtoManager.fail(-1, "团购商品不存在！");
			}

			AppGroupDetailDto appGroupDto = appGroupService.convertGroupProductDto(groupdto);
			return ResultDtoManager.success(appGroupDto);
		} catch (Exception e) {
			log.error("查看团购详情异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 团购分类
	 */
	@ApiOperation(value = "团购分类", httpMethod = "POST", notes = "团购分类列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	
	@PostMapping("/app/group/categoryList")
	public ResultDto<List<AppGroupCategoryDto>> categoryList() {
		
		try {
			List<AppGroupCategoryDto> catList = appCategoryService.getGroupCategoryList(ProductTypeEnum.GROUP.value());
			return ResultDtoManager.success(catList);
			
		} catch (Exception e) {
			log.error("获取团购分类异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	
	
	/**
	 * 检查团购下单
	 * @param groupId
	 * @param productId
	 * @param skuId
	 * @param number
	 * @return
	 */
	@ApiOperation(value = "检查团购下单", httpMethod = "POST", notes = "检查团购下单，成功返回ok",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "groupId", value = "团购id", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="query", name = "productId", value = "商品id", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="query", name = "skuId", value = "skuId", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="query", name = "number", value = "数量", required = true, dataType = "Integer")
	})
	@PostMapping("/p/app/group/checkGroupOrder")
	public ResultDto<AppCheckGroupDto> checkGroupOrder(HttpServletRequest request, HttpServletResponse response, Long groupId, Long productId,
			Long skuId, Integer number) {
		LoginedUserInfo token = loginedUserService.getUser();
		String userId = token.getUserId();
//		Long shopId = token.getShopId();
		UserDetail userDetail = userDetailService.getUserDetailById(userId);
		Long shopId = userDetail.getShopId();
		AppCheckGroupDto checkDto = new AppCheckGroupDto();
		Group group = groupService.getGroup(groupId);
		if (group == null) {
			return ResultDtoManager.fail(-1,"找不到团购信息");
		}
		if (shopId != null && group.getShopId().equals(shopId)) {
			return ResultDtoManager.fail(-1,"不能购买自己的商品");
		}
		if (GroupStatusEnum.ONLINE.value().intValue() != group.getStatus()) {
			return ResultDtoManager.fail(-1,"团购未上线");
		}
		Date startTime = group.getStartTime();
		Date endTime = group.getEndTime();
		Date now = new Date();
		if (now.getTime() >= endTime.getTime()) { // 活动已经过期
			return ResultDtoManager.fail(-1,"团购活动已经过期");
		} else if (now.getTime() < startTime.getTime()) { // 距离活动开始时间
			return ResultDtoManager.fail(-1,"团购活动未开始");
		}

		/*
		 * 1.付款减库存 简单的检查商品库存够不够
		 */
		Integer stocks = stockService.getStocksByMode(productId, skuId);
		if (stocks - number < 0) {
			return ResultDtoManager.fail(-1,"团购商品库存不足,请选择其他团购商品");
		}
		if (group.getBuyQuantity() != null && group.getBuyQuantity() > 0) {
			if (number > group.getBuyQuantity()) {
				return ResultDtoManager.fail(-1,"超出活动限购数");
			}
			// 检查这个用户是否超出限购数量
			Integer count = subService.findUserOrderGroup(userId, productId, skuId, groupId);
			if ((count + number) > group.getBuyQuantity()) {
				return ResultDtoManager.fail(-1,"超出活动限购数");
			}
		}
		StringBuffer buffer = new StringBuffer().append(groupId).append("_").append(productId).append("_").append(skuId);
		String _md5 = MD5Util.toMD5(buffer.toString() + Constants._MD5);
		checkDto.setToken(_md5);
		checkDto.setGroupId(groupId);
		request.getSession().setAttribute("GROP_TOKEN", _md5);
		request.getSession().setAttribute("GROP_NUMBER", number);
		return ResultDtoManager.success(checkDto);
	}
	
	/**
	 * 团购提交订单的详情
	 * @param reqParams
	 * @return
	 */
	@ApiOperation(value = "团购提交订单的详情", httpMethod = "POST", notes = "团购提交订单的详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/p/app/group/orderDetails")
	public ResultDto<AppUserShopCartOrderDto> orderDetails(HttpServletRequest request,GroupOrderDetailsParamsDto reqParams) {
		LoginedUserInfo userToken = loginedUserService.getUser();
		String userId = userToken.getUserId();
		String userName = userToken.getUserName();
		String type = reqParams.getType();
		type=(type==null?"GROUP":type); 
		
		//根据您的下单类型
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("userId",userId);
		params.put("userName",userName);
		params.put("adderessId", reqParams.getAdderessId());
		params.put("groupId", reqParams.getGroupId());
		params.put("number", reqParams.getCount());
		params.put("productId", reqParams.getProdId());
		params.put("skuId", reqParams.getSkuId());		
		
		CartTypeEnum typeEnum = CartTypeEnum.fromCode(type);
		UserShopCartList shopCartList = orderCartResolverManager.queryOrders(typeEnum, params);
		if(AppUtils.isBlank(shopCartList)){
			return ResultDtoManager.fail(-2, "数据已经发生改变,请重新刷新后操作!");
	    }
		
		//查出买家的默认发票内容 check need or not? TODO 
		Invoice userDefaultInvoice = null;
		if (AppUtils.isBlank(reqParams.getInvoiceId())) {
			userDefaultInvoice = invoiceService.getDefaultInvoice(userId);
		}else{
			userDefaultInvoice = invoiceService.getInvoice(reqParams.getInvoiceId(), userId);
		}
		shopCartList.setDefaultInvoice(userDefaultInvoice);
		
		//获取卖家是否开启发票功能
	    Long shopId = shopCartList.getShopCarts().get(0).getShopId();
	    ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
	    
	    if (AppUtils.isBlank(shopDetail.getSwitchInvoice())) {
	    	shopCartList.setSwitchInvoice(0);
		}
	    shopCartList.setSwitchInvoice(shopDetail.getSwitchInvoice());
		
		AppUserShopCartOrderDto shopCartOrderDto = appCartOrderService.getShoppingCartOrderDto(shopCartList);
		if(AppUtils.isBlank(shopCartOrderDto)){
			return ResultDtoManager.fail(-2, "数据已经发送改变,请重新刷新后操作!");
	    }
		//设置订单提交令牌,防止重复提交
		Integer token=CommonServiceUtil.generateRandom();
		shopCartOrderDto.setToken(token+"");
		cacheClient.put(Constants.APP_FROM_SESSION_TOKEN + ":" + typeEnum.toCode() + ":" + userId, Constants.APP_FROM_SESSION_TOKEN_EXPIRE, token);
		subService.putUserShopCartList(typeEnum.toCode(),userId,shopCartList);
		
		return ResultDtoManager.success(shopCartOrderDto);
	}
	
}
