/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.app.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.base.xssfilter.XssDtoFilter;
import com.legendshop.spi.service.SensitiveWordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.advanced.search.service.SearchService;
import com.legendshop.app.dto.AppHotsearchDto;
import com.legendshop.app.dto.AppProdDto;
import com.legendshop.app.dto.AppShopDto;
import com.legendshop.app.service.AppSearchService;
import com.legendshop.dao.support.DefaultPagerProvider;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.search.AppProdSearchParms;
import com.legendshop.model.dto.search.AppShopSearchParams;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.model.entity.ProductDetail;
import com.legendshop.util.AppUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

/**
 * App端搜索.
 */
@RestController
@Api(tags="搜索接口",value="商品搜索,店铺搜索相关接口")
@RequestMapping("/appSearch")
public class AppSearchController{

	/** The search service locator. */
	@Autowired
	private  AppSearchService  appSearchService;
	
	@Autowired
	private SearchService searchService;

	@Autowired
	private SensitiveWordService sensitiveWordService;
	
	/**
	 * 搜索商品列表 旧.
	 *
	 */
	//@RequestMapping(value="/prodList", method = RequestMethod.POST)
	public ResultDto<AppPageSupport<AppProdDto>> searchProdlist(AppProdSearchParms parms) {
		
		AppPageSupport<AppProdDto> appProdDtoList = appSearchService.appProdList(parms); 
		
		return ResultDtoManager.success(appProdDtoList);
	}
	
	/**
	 * 搜索商品列表 2019-2-16.
	 */
	@ApiOperation(value = "搜索商品列表", httpMethod = "POST", notes = "搜索商品列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "query", dataType = "Integer", name = "curPageNO", value = "当前页")
	@PostMapping(value="/prodlist")
	public ResultDto<AppPageSupport<AppProdDto>> prodlist(HttpServletRequest request, HttpServletResponse response,Integer curPageNO, ProductSearchParms parms) {

		parms = XssDtoFilter.safeProductSearchParms(parms);

		// 敏感字过滤
		Set<String> findwords = sensitiveWordService.checkSensitiveWords(parms.getKeyword());
		if (AppUtils.isNotBlank(findwords)) {
			return ResultDtoManager.fail("您查询的内容含有敏感词 " + findwords + ",根据本地法律无法显示，请更正再重试");
		}
		
		curPageNO = AppUtils.isNotBlank(curPageNO) ?curPageNO : 1;
		Integer pageSize=20;
		JSONObject json=searchService.prodlist(curPageNO,pageSize,parms);
		if(json==null){
			return ResultDtoManager.success();
		}
		@SuppressWarnings("unchecked")
		List<ProductDetail> details=(List<ProductDetail>) json.get("result");
		
		List<AppProdDto> ProdDetailDtoList = convertToAppProdDto(details);
		
		Long numFound=json.getLong("numFound");
		AppPageSupport<AppProdDto> result = initPageProvider(request,numFound,curPageNO,pageSize,ProdDetailDtoList);
		return ResultDtoManager.success(result);
	}
	
	/**
	 * 搜索商家列表.
	 *
	 * @param parms the parms
	 * @return the result dto
	 */
	@ApiOperation(value = "搜索商家列表", httpMethod = "POST", notes = "搜索商家列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/shopList")
	public ResultDto<AppPageSupport<AppShopDto>> searchShoplist(AppShopSearchParams parms) {
		AppPageSupport<AppShopDto>  appShopListDto = appSearchService.appSearchShopList(parms);
		return ResultDtoManager.success(appShopListDto);
	}
	
	/**
	 * 热搜词.
	 *
	 * @return the result dto
	 */
	@ApiOperation(value = "热搜词", httpMethod = "POST", notes = "热搜词",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/hotSearchList")
	public ResultDto<List<AppHotsearchDto>> hotSearchList() {
		
		List<AppHotsearchDto> hotList = appSearchService.getHotsearch();
		
		return ResultDtoManager.success(hotList);
	}
	
	/**
	 * 转换成List<AppProdDto>输出
	 */
	private List<AppProdDto> convertToAppProdDto(List<ProductDetail> resultList) {
		List<AppProdDto> appProdDtoList = new ArrayList<AppProdDto>();
		for (ProductDetail productDetail : resultList) {
			AppProdDto appProdDto = new AppProdDto();
			appProdDto.setCash(productDetail.getCash());
			appProdDto.setName(productDetail.getName());
			appProdDto.setPic(productDetail.getPic());
			appProdDto.setPrice(productDetail.getPrice());
			appProdDto.setProdId(productDetail.getProdId());
			appProdDto.setStatus(productDetail.getStatus());
			appProdDto.setComments(productDetail.getComments());
			appProdDto.setProdType(productDetail.getProdType());
			appProdDto.setShopType(productDetail.getShopType());
			appProdDtoList.add(appProdDto);
		}
		return appProdDtoList;
	}
	
	private AppPageSupport<AppProdDto> initPageProvider(HttpServletRequest request, long total, Integer curPageNO, Integer pageSize, List<AppProdDto> resultList) {
		DefaultPagerProvider pageProvider = new DefaultPagerProvider(total, curPageNO, pageSize);
		AppPageSupport<AppProdDto> ps = new AppPageSupport<AppProdDto>(resultList, pageProvider);
		return ps;
	}
}
