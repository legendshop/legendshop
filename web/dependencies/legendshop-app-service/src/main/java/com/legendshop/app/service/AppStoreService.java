package com.legendshop.app.service;

import java.util.List;

import com.legendshop.app.dto.AppUserShopCartOrderDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.AppStoreDto;
import com.legendshop.model.dto.app.AppStoreSearchParamsDTO;
import com.legendshop.model.dto.app.store.AppUserShopStoreCartOrderDto;
import com.legendshop.model.dto.buy.UserShopCartList;

public interface AppStoreService {

	/** 按商品来查找地区的门店 */
	public List<AppStoreDto> findStoreCitys(Long poductId, Integer provinceId, Integer cityId, Integer areaId);

	/** 查找商城在城市里的门店 */
	public List<AppStoreDto> findStoreShopCitys(long shopId, int provinceid, int cityid, int areaid);

	/**
	 * 根据传入参数获取相关门店
	 * @param searchParams 搜索参数
	 * @return
	 */
	public AppPageSupport<AppStoreDto> queryStore(AppStoreSearchParamsDTO searchParams);

	/**
	 * 获取门店信息
	 * @param storeId 门店Id
	 * @return
	 */
	public AppStoreDto getStore(Long storeId);
	
	/**
	 * 根据经纬度获取距离最近的门店
	 * @param prodId 商品id
	 * @param skuId 
	 * @param lng 经度
	 * @param lat 纬度
	 * @return
	 */
	public AppStoreDto getNearStore(Long prodId, Long skuId, Double lng, Double lat);

	/**
	 * 转换门店购物车DTO
	 * @param shopCartList
	 * @return
	 */
	AppUserShopStoreCartOrderDto getShoppingCartOrderDto(UserShopCartList shopCartList);
}
