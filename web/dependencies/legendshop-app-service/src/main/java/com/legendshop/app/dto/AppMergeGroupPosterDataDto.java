package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用于生成团购商品海报数据的DTO
 */
@ApiModel(value = "用于生成商品海报数据")
public class AppMergeGroupPosterDataDto {
	
	/**
	 * 用户头像
	 */
	@ApiModelProperty("用户头像url")
	private String headPortrait;
	
	/**
	 * 用户昵称
	 */
	@ApiModelProperty("用户昵称url")
	private String nickName;
	
	/**
	 * 小程序码
	 */
	@ApiModelProperty("小程序码url")
	private String wxACode;

	public String getHeadPortrait() {
		return headPortrait;
	}

	public void setHeadPortrait(String headPortrait) {
		this.headPortrait = headPortrait;
	}
	
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getWxACode() {
		return wxACode;
	}

	public void setWxACode(String wxACode) {
		this.wxACode = wxACode;
	}
}
