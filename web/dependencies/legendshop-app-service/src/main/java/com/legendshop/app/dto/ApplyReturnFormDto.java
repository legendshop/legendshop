package com.legendshop.app.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app 申请退货退款参数封装Dto
 */
@ApiModel(value="申请退货退款Dto")
public class ApplyReturnFormDto extends ApplyRefundReturnBaseDto {

	/**  */
	private static final long serialVersionUID = -7304221763402704260L;

	/** 订单项 ID */
	@ApiModelProperty(value="订单项id")
	@NotNull(message = "订单项id不能为空")
	private Long orderItemId;
	
	/** 退货数量 */
	@ApiModelProperty(value="退货数量")
	@NotNull(message = "退货数量")
	@Min(value=1,message = "退货数量至少为1")
	private Long goodsNum;

	public Long getOrderItemId() {
		return orderItemId;
	}

	public void setOrderItemId(Long orderItemId) {
		this.orderItemId = orderItemId;
	}

	public Long getGoodsNum() {
		return goodsNum;
	}

	public void setGoodsNum(Long goodsNum) {
		this.goodsNum = goodsNum;
	}
}
