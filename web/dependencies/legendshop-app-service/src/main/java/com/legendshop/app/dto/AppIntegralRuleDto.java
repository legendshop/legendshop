package com.legendshop.app.dto ;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * app积分规则DTO
 */
@ApiModel("app积分规则DTO")
public class AppIntegralRuleDto  implements Serializable {
	
	private static final long serialVersionUID = 6512889041062842310L;

	/** 注册 */
	@ApiModelProperty(value = "注册可得积分")
	private Integer reg;
	
	/** 登录 */
	@ApiModelProperty(value = "登录可得积分")
	private Integer login;
	
	/** 验证邮箱 */
	@ApiModelProperty(value = "验证邮箱可得积分")
	private Integer verifyEmail;
	
	/** 验证手机 */
	@ApiModelProperty(value = "验证手机可得积分")
	private Integer verifyMobile;
	
	/** 商品评论 */
	@ApiModelProperty(value = "商品评论可得积分")
	private Integer productReview;
	
	/** 状态1为开启，其他为不开启 */
	@ApiModelProperty(value = "状态1为开启，其他为不开启")
	private Integer status;
	
	public Integer getReg() {
		return reg;
	}
	public void setReg(Integer reg) {
		this.reg = reg;
	}
	public Integer getLogin() {
		return login;
	}
	public void setLogin(Integer login) {
		this.login = login;
	}
	public Integer getProductReview() {
		return productReview;
	}
	public void setProductReview(Integer productReview) {
		this.productReview = productReview;
	}
	public Integer getVerifyEmail() {
		return verifyEmail;
	}
	public void setVerifyEmail(Integer verifyEmail) {
		this.verifyEmail = verifyEmail;
	}
	public Integer getVerifyMobile() {
		return verifyMobile;
	}
	public void setVerifyMobile(Integer verifyMobile) {
		this.verifyMobile = verifyMobile;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
