/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.app.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 分类Dto.
 */
@ApiModel(value="分类Dto") 
public class CategoryDto implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2051766267006855254L;

	/** 产品类目ID. */
	@ApiModelProperty(value="产品类目ID")  
	private Long id; 
	
	/** ID以外的区别符*. */
	@ApiModelProperty(value="ID以外的区别符")  
	private Long identfy;
	
	/** 产品类目名称. */
	@ApiModelProperty(value="产品类目名称 ")  
	private String name; 
		
	/** 类目的显示图片. */
	@ApiModelProperty(value="类目的显示图片")  
	private String pic; 
		
	/** 分类层级. */
	@ApiModelProperty(value="分类层级")  
	private Integer level;
	
	/** 是否为空. */
	@ApiModelProperty(value = "是否为空")  
	private Boolean isBlank;

	/** 子分类*. */
	@ApiModelProperty(value="子分类")  
	private List<CategoryDto> childrenList;
	
	/** 父级目录名称*. */
	@ApiModelProperty(value="父级目录名称")  
	private String parent;
	
	
	
	public Boolean getIsBlank() {
		return isBlank;
	}

	public void setIsBlank(Boolean isBlank) {
		this.isBlank = isBlank;
	}
	
	
	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public String getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the parent
	 */
	public void setParent(String parent) {
		this.parent = parent;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Gets the identfy.
	 *
	 * @return the identfy
	 */
	public Long getIdentfy() {
		return identfy;
	}

	/**
	 * Sets the identfy.
	 *
	 * @param identfy the identfy
	 */
	public void setIdentfy(Long identfy) {
		this.identfy = identfy;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the pic.
	 *
	 * @return the pic
	 */
	public String getPic() {
		return pic;
	}

	/**
	 * Sets the pic.
	 *
	 * @param pic the pic
	 */
	public void setPic(String pic) {
		this.pic = pic;
	}

	/**
	 * Gets the level.
	 *
	 * @return the level
	 */
	public Integer getLevel() {
		return level;
	}

	/**
	 * Sets the level.
	 *
	 * @param level the level
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}

	/**
	 * Gets the children list.
	 *
	 * @return the children list
	 */
	public List<CategoryDto> getChildrenList() {
		return childrenList;
	}

	/**
	 * Sets the children list.
	 *
	 * @param childrenList the children list
	 */
	public void setChildrenList(List<CategoryDto> childrenList) {
		this.childrenList = childrenList;
	}
	
	/**
	 * Adds the children.
	 *
	 * @param categoryDto the category dto
	 */
	public void addChildren(CategoryDto categoryDto) {
		if(childrenList == null){
			childrenList = new ArrayList<CategoryDto>();
		}
		childrenList.add(categoryDto);
	}
}
