package com.legendshop.app.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.AppVisitHistoryDto;
import com.legendshop.app.service.AppVisitHistoryService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.business.dao.VisitLogDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.VisitLog;

@Service("appVisitHistoryService")
public class AppVisitHistoryServiceImpl implements AppVisitHistoryService {

	@Autowired
	private VisitLogDao visitLogDao;

	@Override
	public AppPageSupport<AppVisitHistoryDto> queryVisitHistory(String curPageNO, String userName) {
		PageSupport<VisitLog> ps = visitLogDao.getVisitLogListPageForApp(curPageNO, userName);
		return PageUtils.convertPageSupport(ps, form -> {
			List<AppVisitHistoryDto> appVisitHistoryList = new ArrayList<>();
			for (VisitLog visitLog : form) {
				AppVisitHistoryDto appVisitHistoryDto = new AppVisitHistoryDto();
				appVisitHistoryDto.setVisitId(visitLog.getVisitId());
				appVisitHistoryDto.setProdId(visitLog.getProductId());
				appVisitHistoryDto.setPic(visitLog.getPic());
				appVisitHistoryDto.setProdName(visitLog.getProductName());
				appVisitHistoryDto.setPrice(visitLog.getPrice());
				appVisitHistoryDto.setVisitNum(visitLog.getVisitNum());
				appVisitHistoryDto.setRecDate(visitLog.getDate());
				appVisitHistoryDto.setSource(visitLog.getSource());
				appVisitHistoryList.add(appVisitHistoryDto);
			}
			// 商品Id去重
//			appVisitHistoryList = appVisitHistoryList.stream()
//					// 过滤商品Id为空的对象
//					.filter(e -> e.getProdId() != null)
//					// 通过ProdId排序配合TreeSet来进行Id去重
//					.collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(AppVisitHistoryDto::getProdId))), ArrayList::new));
//			// 通过记录时间排序
//			appVisitHistoryList.sort((o1, o2) -> o1.getRecDate().getTime() <= o2.getRecDate().getTime() ? 0 : -1);
			return appVisitHistoryList;
		});
	}

	@Override
	public boolean clearVisitHistory(String userName) {

		int result = visitLogDao.updateIsUserDelBy(userName, true);

		return result > 0;
	}
}
