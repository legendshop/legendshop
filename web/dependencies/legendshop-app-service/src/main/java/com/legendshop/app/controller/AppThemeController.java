/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.AppThemeDetailDto;
import com.legendshop.app.dto.AppThemeListDto;
import com.legendshop.app.service.AppThemeService;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.util.AppUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

/**
 * 专题活动
 */
@RestController
@Api(tags="专题活动",value="专题活动相关接口")
public class AppThemeController {
	
	/** The log. */
	private final Logger LOGGER = LoggerFactory.getLogger(AppThemeController.class);
	
	@Autowired
	private AppThemeService appThemeService;
	
	/**
	 * 专题活动类表
	 * @param curPageNO 当前页
	 * @return
	 */
	@ApiOperation(value = "专题活动列表", httpMethod = "POST", notes = "专题活动列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = false, dataType = "string")
	@PostMapping(value="/theme/list")
	public ResultDto<AppPageSupport<AppThemeListDto>> allThemes(String curPageNO){
		
		try {
			AppPageSupport<AppThemeListDto> result = appThemeService.getThemePage(curPageNO);
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("专题活动列表异常!", e);
			return ResultDtoManager.fail();
		}
	}
	/**
	 * 专题活动详情
	 * @param curPageNO 当前页
	 * @return
	 */
	@ApiOperation(value = "专题活动详情", httpMethod = "POST", notes = "专题活动详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "themeId", value = "专题活动id", required = true, dataType = "Long")
	@PostMapping(value="/theme/detail")
	public ResultDto<AppThemeDetailDto> themeDetail(Long themeId){
		
		try {
			//获取主题活动详情
			AppThemeDetailDto appThemeDetailDto = appThemeService.getThemeDetail(themeId);
			if (AppUtils.isBlank(appThemeDetailDto)) {
				return ResultDtoManager.fail(-1, "该专题活动已过期或已被删除!");
			}

			return ResultDtoManager.success(appThemeDetailDto);
		} catch (Exception e) {
			LOGGER.error("获取专题活动详情异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	
}
