/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 提交订单返回给App pay的参数
 */
@ApiModel(value="订单支付参数Dto") 
public class AppOrderPayParamDto {

   /** 订单流水号 **/
	@ApiModelProperty(value="订单流水号")  
   private String subNumber;
   
   /** 订单支付金额 **/
	@ApiModelProperty(value="订单支付金额")  
   private Double totalAmount;
   
   /** 商品描述 **/
	@ApiModelProperty(value="商品描述")  
   private String subjects;
   
   /** 支付宝密钥 **/
	@ApiModelProperty(value="支付宝密钥",hidden = true)  
   private String payCode;
	
	/** 支付单据类型 **/
	@ApiModelProperty(value="支付单据类型")  
	private String subSettlementType;
	
	public String getPayCode() {
	return payCode;
	}
	
	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}

	
	public Double getTotalAmount() {
		return totalAmount;
	}
	
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	public String getSubjects() {
		return subjects;
	}

	public void setSubjects(String subjects) {
		this.subjects = subjects;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public String getSubSettlementType() {
		return subSettlementType;
	}

	public void setSubSettlementType(String subSettlementType) {
		this.subSettlementType = subSettlementType;
	}

}
