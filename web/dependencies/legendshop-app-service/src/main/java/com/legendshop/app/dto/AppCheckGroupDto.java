/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app检测团购返回数据Dto
 *
 */
@ApiModel(value="app检测团购返回数据Dto")
public class AppCheckGroupDto {

	/** 团购活动id */
	@ApiModelProperty(value="团购活动id")
	private Long groupId;

	/** 团购加密Token. */
	@ApiModelProperty(value="团购加密Token")
	private String token;

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}
