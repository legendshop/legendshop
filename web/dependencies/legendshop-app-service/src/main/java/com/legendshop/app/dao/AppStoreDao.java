package com.legendshop.app.dao;

import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppStoreDto;
import com.legendshop.model.dto.app.AppStoreSearchParamsDTO;
import com.legendshop.model.entity.store.Store;

public interface AppStoreDao extends GenericDao<Store, Long> {

	/** 获取门店所在城市 */
	public abstract List<AppStoreDto> findStoreCitys(Long poductId, Integer provinceId, Integer cityId, Integer areaId);

	/** 查找商城在城市里的门店 */
	public abstract List<AppStoreDto> findStoreShopCitys(long shopId, int provinceid, int cityid, int areaid);

	/**
	 * 根据传入参数获取相关门店
	 * @param searchParams 搜索参数
	 * @return
	 */
	public abstract PageSupport<AppStoreDto> queryStore(AppStoreSearchParamsDTO searchParams);
}
