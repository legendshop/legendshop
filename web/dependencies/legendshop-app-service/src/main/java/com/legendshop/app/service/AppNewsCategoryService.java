package com.legendshop.app.service;

import com.legendshop.app.dto.AppNewsCategoryDto;
import com.legendshop.model.dto.app.AppPageSupport;

/**
 * app 新闻栏目service
 */
public interface AppNewsCategoryService {

	/**
	 * 获取新闻栏目列表
	 * @param curPageNO
	 * @return
	 */
	public AppPageSupport<AppNewsCategoryDto> getNewsCategoryListPage(String curPageNO);

}
