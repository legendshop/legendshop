package com.legendshop.app.service;

import java.util.List;

import com.legendshop.model.dto.app.IndexjpgDto;

/**
 * 
 * @author Administrator
 *
 */
public interface AppIndexService {
	
	/**
	 * 首页统一返回一个Dto
	 * @param shopId
	 * @return
	 */
	public List<IndexjpgDto> getIndexJpegByShopId(Long shopId);
	
	
}
