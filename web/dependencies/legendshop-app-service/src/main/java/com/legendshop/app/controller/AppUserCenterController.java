/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.service.AppSubService;
import com.legendshop.app.service.AppVisitLogService;
import com.legendshop.app.service.MyFavoriteService;
import com.legendshop.model.dto.app.AppSubCountsDto;
import com.legendshop.model.dto.app.AppUserCenterDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.UserGrade;
import com.legendshop.spi.service.FavoriteShopService;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.MessageService;
import com.legendshop.spi.service.SubService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.spi.service.UserGradeService;
import com.legendshop.util.AppUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 个人中心控制器
 */
@RestController
@RequestMapping(value = "/p")
@Api(tags="个人中心",value="个人中心相关接口")
public class AppUserCenterController {
	
	private static Logger LOGGER = LoggerFactory.getLogger(AppUserCenterController.class);

	@Autowired
	private MyFavoriteService myFavoriteService;
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private SubService subService;
	
	@Autowired
	private FavoriteShopService favoriteShopService ;
	
	@Autowired
	private AppSubService appSubService;
	
	@Autowired
	private UserGradeService userGradeService;
	
	@Autowired
	private AppVisitLogService appVisitLogService;
	
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;
	
	/**
	 * 个人中心首页
	 *
	 * @return UserCenterDto
	 */
	@ApiOperation(value = "个人中心首页", httpMethod = "POST", notes = "查取个人中心首页相关数据",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/userCenter")
	public ResultDto<AppUserCenterDto> userCenter() {
		try {
			LoginedUserInfo token = loginedUserService.getUser();
			
			if (AppUtils.isBlank(token)) {
				return ResultDtoManager.fail(-1, "请先登录!");
			}
			
			String userName = token.getUserName();
	    	String userId = token.getUserId();
	    	
	    	UserDetail userDetail = userDetailService.getUserDetailById(userId);
	    	if(AppUtils.isNotBlank(userDetail) && AppUtils.isNotBlank(userDetail.getBirthDate())){//从生日中提取 年月日 
				 Calendar cal = Calendar.getInstance();  
				 cal.setTime(userDetail.getBirthDate());
				 userDetail.setYear(cal.get(Calendar.YEAR));
				 userDetail.setMonth(cal.get(Calendar.MONTH)+1);
				 userDetail.setDay(cal.get(Calendar.DATE));
			}
	    	
	    	//获取用户会员等级信息
	    	UserGrade userGrade = userGradeService.getUserGrade(userDetail.getGradeId());
	    	
			//商品收藏数
			Long favoriteLength =  myFavoriteService.getfavoriteLength(userId);
			//店铺收藏数
			Long favoriteShopLength = favoriteShopService.getfavoriteShopLength(userId);
			
			//总收藏数量
			Long totalFavoriteCount = favoriteLength + favoriteShopLength;
			
			// 未读信息
			Date userRegtime = userDetail.getUserRegtime();
			long countMessage = messageService.getCountById(userName);
			long countSystemMessage = messageService.getCalUnreadSystemMessagesCount(userName, userDetail.getGradeId(), userRegtime);
			countMessage += countSystemMessage;
			
			//获取我的足迹统计数量
			Long visitLogCount = appVisitLogService.getVisitLogCountByUserName(userName);
			
			//获取订单个数
			List<AppSubCountsDto> subCountsList = appSubService.querySubCounts(userId);
			
			//已完成的订单 取未评价的 订单个数
			Integer unCommCount = appSubService.getUnCommentOrderCount(userId);
			
			//构建返回dto
			AppUserCenterDto appUserCenterDto = new AppUserCenterDto();
			userName = userDetail.getNickName() == null ? userDetail.getUserName(): userDetail.getNickName();
			appUserCenterDto.setUserId(userId);
			appUserCenterDto.setUserName(userName);
			appUserCenterDto.setPortraitPic(userDetail.getPortraitPic());
			appUserCenterDto.setFavoriteLength(favoriteLength);
			appUserCenterDto.setFavoriteShopLength(favoriteShopLength);
			appUserCenterDto.setSubCountsDtoList(subCountsList);
			appUserCenterDto.setMessageCount(countMessage);
			appUserCenterDto.setUnCommCount(unCommCount);
			appUserCenterDto.setScore(userDetail.getScore());
			appUserCenterDto.setUserGradeName(userGrade.getName());
			appUserCenterDto.setTotalFavoriteCount(totalFavoriteCount);
			appUserCenterDto.setVisitLogCount(visitLogCount);
			
			return ResultDtoManager.success(appUserCenterDto);
		} catch (Exception e) {
			LOGGER.error("获取个人中心信息异常!", e);
			return ResultDtoManager.fail();
		}
	}
}
