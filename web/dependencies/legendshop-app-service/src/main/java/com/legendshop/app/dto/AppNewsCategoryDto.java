package com.legendshop.app.dto;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app 文章栏目dto
 */
@ApiModel(value="文章栏目dto")
public class AppNewsCategoryDto {

	/** 新闻栏目ID. */
	@ApiModelProperty(value="新闻栏目ID")  
	private Long newsCategoryId;

	/** 新闻栏目名称. */
	@ApiModelProperty(value="新闻栏目名称")  
	private String newsCategoryName;

	/** 状态. */
	@ApiModelProperty(value="状态")  
	private Short status;

	/** 顺序. */
	@ApiModelProperty(value="顺序")  
	private Integer seq;

	/** 发表时间. */
	@ApiModelProperty(value="发表时间")  
	private Date newsCategoryDate;

	/** 用户id. */
	@ApiModelProperty(value="用户id")  
	private String userId;

	/** 用户名. */
	@ApiModelProperty(value="用户名")  
	private String userName;

	/** 图片 */
	@ApiModelProperty(value="图片")  
	private String pic;

	public Long getNewsCategoryId() {
		return newsCategoryId;
	}

	public void setNewsCategoryId(Long newsCategoryId) {
		this.newsCategoryId = newsCategoryId;
	}

	public String getNewsCategoryName() {
		return newsCategoryName;
	}

	public void setNewsCategoryName(String newsCategoryName) {
		this.newsCategoryName = newsCategoryName;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Date getNewsCategoryDate() {
		return newsCategoryDate;
	}

	public void setNewsCategoryDate(Date newsCategoryDate) {
		this.newsCategoryDate = newsCategoryDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}
}
