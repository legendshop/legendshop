/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.service.AppUserDetailService;
import com.legendshop.app.service.MyFavoriteService;
import com.legendshop.model.constant.SMSTypeEnum;
import com.legendshop.model.dto.app.AppUserDetailDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.entity.User;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.SMSLogService;
import com.legendshop.spi.service.UserAddressService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 用户信息管理控制器
 */
@RestController
@Api(tags="账户管理",value="个人资料查看、修改、编辑")
public class AppUserManageController {
	
	/** The log. */
	private final Logger LOGGER = LoggerFactory.getLogger(AppUserManageController.class);
	
	@Autowired
	private AppUserDetailService appUserDetailService;
	
	@Autowired 
	AttachmentManager attachmentManager;
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private SMSLogService smsLogService;
	
	@Autowired
	private MyFavoriteService myfavoriteService;
	
	@Autowired
	private UserAddressService userAddressService;
	
    @Autowired
    private PasswordEncoder passwordEncoder;
    
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;
	
	/**
	 * 个人信息 
	 * @param request
	 * @param response
	 * @return
	 */
	@ApiOperation(value = "个人基本信息", httpMethod = "POST", notes = "个人基本信息页面",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/p/userInfo")
	public ResultDto<AppUserDetailDto> userInfo() {
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			if(AppUtils.isBlank(userId)){
				return ResultDtoManager.fail(-1, "对不起,您要查看的账号信息不存在或已被删除!");
			}
			AppUserDetailDto userDetailDto = appUserDetailService.getUserDetail(userId);
			//查询默认地址
			UserAddress defaultAddress = userAddressService.getDefaultAddress(userId);
			if (AppUtils.isNotBlank(defaultAddress)) {
				userDetailDto.setCommonAddr(defaultAddress.getProvince()+defaultAddress.getCity()+defaultAddress.getArea()+defaultAddress.getSubAdds());
			}
			return ResultDtoManager.success(userDetailDto);
		} catch (Exception e) {
			LOGGER.error("获取个人信息异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 *  更新昵称
	 * @return
	 */
	@ApiOperation(value = "更新昵称", httpMethod = "POST", notes = "更新昵称信息,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "nickName", value = "新昵称", required = true, dataType = "string")
	@PostMapping(value="/p/updateNickName")
	public ResultDto<Object> updateNickName(@RequestParam String nickName) {
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			
			int result = appUserDetailService.updateNickName(userId,nickName);
			if(result == 1){
				 return ResultDtoManager.success(result);
			}else if(result == 0){
				 return ResultDtoManager.fail(0, "昵称已经存在");
			}else{
				 return ResultDtoManager.fail(-1, "用户不存在");
			}
		} catch (Exception e) {
			LOGGER.error("更新昵称失败异常!", e);
			return ResultDtoManager.fail();
		}
	}

	/**
	 *  更新昵称
	 * @return
	 */
	@ApiOperation(value = "更新签名", httpMethod = "POST", notes = "更新签名信息,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "userSignature", value = "新签名", required = true, dataType = "string")
	@PostMapping(value="/p/updateSignature")
	public ResultDto<Object> updateSignature(@RequestParam String userSignature) {

		try {
			String userId = loginedUserService.getUser().getUserId();

			int result = appUserDetailService.updateSignature(userId,userSignature);
			if(result == 1){
				return ResultDtoManager.success(result);
			}else{
				return ResultDtoManager.fail(-1, "用户不存在");
			}
		} catch (Exception e) {
			LOGGER.error("更新签名失败异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 更新姓名
	 * @return
	 */
	@ApiOperation(value = "更新姓名", httpMethod = "POST", notes = "更新姓名信息,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "realName", value = "新姓名", required = true, dataType = "string")
	@PostMapping(value="/p/updateRealName")
	public ResultDto<Object> updateRealName(@RequestParam String realName) {
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			
			int result = appUserDetailService.updateRealName(userId, realName);
			if(result == 1){
				return ResultDtoManager.success(result);
			}else{
				return ResultDtoManager.fail(-1, "用户不存在");
			}
		} catch (Exception e) {
			LOGGER.error("更新昵称失败异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 *  更新性别
	 *  sex说明 M:男; F: 女
	 * @return
	 */
	@ApiOperation(value = "更新性别", httpMethod = "POST", notes = "更新昵称信息,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "sex", value = "性别  sex说明 M:男; F: 女", required = true, dataType = "string")
	@PostMapping(value="/p/updateSex")
	public ResultDto<Object> updateSex(@RequestParam String sex) {
		
		try {
			if("M".equals(sex) || "F".equals(sex) || "".equals(sex) ){
				int result = appUserDetailService.updateSex(loginedUserService.getUser().getUserId(), sex);
				if(result == 1){
					 return ResultDtoManager.success(result);
				}else{
					 return ResultDtoManager.fail(-1, "用户不存在");
				}
			}else{
				 return ResultDtoManager.fail(0, "参数错误");
			}
		} catch (Exception e) {
			LOGGER.error("更新性别失败异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 *  更新头像
	 * @return
	 * @throws IOException 
	 */
	@ApiOperation(value = "更新头像", httpMethod = "POST", notes = "更新头像,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "portraitPic", value = "新头像图片路径", required = true, dataType = "string")
	})
	@PostMapping(value="/p/updatePortrait")
	public ResultDto<String> updatePortrait1(HttpServletRequest req, HttpServletResponse response,@RequestParam String portraitPic) {

		try {
			String userId = loginedUserService.getUser().getUserId();
			UserDetail userDetail =  userDetailService.getUserDetailById(userId);
			
			String originPic = userDetail.getPortraitPic();
			
			//先删除旧头像
			if (AppUtils.isNotBlank(originPic)) {
				attachmentManager.delAttachmentByFilePath(originPic);
				attachmentManager.deleteTruely(originPic);
			}
			
			int flag = appUserDetailService.updatePortrait(loginedUserService.getUser().getUserId(), portraitPic);
			if(flag == 1){
				 return ResultDtoManager.success();
			}else{
				 return ResultDtoManager.fail(-1, "用户不存在");
			}
		} catch (Exception e) {
			LOGGER.error("更新头像失败异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 更新生日
	 * @return
	 */
	@ApiOperation(value = "更新生日", httpMethod = "POST", notes = "更新生日,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "birthDate", value = "生日", required = true, dataType = "Date")
	@PostMapping(value="/p/updateBirthDate")
	public ResultDto<Object> updateBirthDate(Date birthDate) {
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			
			int result = appUserDetailService.updateBirthDate(userId, birthDate);
			if(result == 1){
				return ResultDtoManager.success(result);
			}else{
				return ResultDtoManager.fail(-1, "用户不存在");
			}
		} catch (Exception e) {
			LOGGER.error("更新昵称失败异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 校验原手机号码
	 * @return
	 */
	@ApiOperation(value = "校验原手机号码", httpMethod = "POST", notes = "校验原手机号码,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "mobile", value = "原手机号码", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "mobileCode", value = "验证码", required = true, dataType = "string")
	})
	@PostMapping(value="/p/validateUserMobile")
	public ResultDto<String> validateUserMobile(String mobile,String mobileCode) {
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			UserDetail userDetail = userDetailService.getUserDetailById(userId);
			if (AppUtils.isBlank(userDetail)) {
				return ResultDtoManager.fail(-1, "该用户不存在或信息已被删除!");
			}
			
			if(AppUtils.isBlank(mobile)){
				return ResultDtoManager.fail(-1, "原手机号码不能为空!");
			}
			
			if (!userDetail.getUserMobile().equals(mobile)) {
				return ResultDtoManager.fail(-1, "原手机号码不正确，请重新输入!");
			}
			
			if(AppUtils.isBlank(mobileCode) || !checkSMSCode(mobile,mobileCode)){
				return ResultDtoManager.fail(-1, "请输入正确的验证码!");
			}
			return ResultDtoManager.success();
		} catch (Exception e) {
			LOGGER.error("校验手机号码异常!", e);
			return ResultDtoManager.fail();
		}
	}
	/**
	 * 更新手机号码
	 * @return
	 */
	@ApiOperation(value = "更新手机号码", httpMethod = "POST", notes = "更新手机号码,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "mobile", value = "新手机号码", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "mobileCode", value = "短信验证码", required = true, dataType = "string")
	})
	@PostMapping(value="/p/updateMobile")
	public ResultDto<String> updateMobile(String mobile,String mobileCode) {
		
		try {
			
			String userName = loginedUserService.getUser().getUserName();
			if(AppUtils.isBlank(userName)){
				return ResultDtoManager.fail(-1, "该用户不存在!");
			}
			
			if(AppUtils.isBlank(mobile)){
				return ResultDtoManager.fail(-1, "手机号码不能为空!");
			}
			
			boolean isExist = userDetailService.isPhoneExist(mobile);
			if (isExist) {
				return ResultDtoManager.fail(-1, "该手机号码已被注册，请重新输入!");
			}
			
			if(AppUtils.isBlank(mobileCode) || !checkSMSCode(mobile,mobileCode)){
				return ResultDtoManager.fail(-1, "请输入正确的验证码!");
			}
			
			userDetailService.updateUserMobile(mobile,userName);
			return ResultDtoManager.success();
		} catch (Exception e) {
			LOGGER.error("校验手机号码异常!", e);
			return ResultDtoManager.fail();
		}
	}

	
	/**
	 * 修改密码
	 * @param request
	 * @param response
	 * @param code
	 * @param newPwd
	 * @param mobile
	 * @return
	 */
	 @ApiOperation(value = "修改密码", httpMethod = "POST", notes = "修改密码,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
     @ApiImplicitParams({
    	@ApiImplicitParam(paramType="query", name = "mobileCode", value = "短信验证码", required = true, dataType = "string"),
    	@ApiImplicitParam(paramType="query", name = "newPwd", value = "新密码", required = true, dataType = "string"),
    	@ApiImplicitParam(paramType="query", name = "mobile", value = "手机号码", required = true, dataType = "string")
     })
	@PostMapping(value="/p/changePwd")
	public ResultDto<String> changePwd(String mobileCode, String newPwd,String mobile){
		 
		try {
			
			if(!smsLogService.verifyCodeAndClear(mobile, mobileCode) ){
				return ResultDtoManager.fail(-1, "短信验证码错误！");
			}
			
			User user = userDetailService.getUserByMobile(mobile);
			if(user==null){
				return ResultDtoManager.fail(-1, "手机号不存在或信息已被删除");
			}else{
				user.setPassword(passwordEncoder.encode(newPwd));
				userDetailService.updateUser(user);
				return ResultDtoManager.success();
			}
		} catch (Exception e) {
			LOGGER.error("更新用户密码异常!", e);
			return ResultDtoManager.fail();
		} 
	}
	
	
	/**
	 * 更改支付密码
	 */
	@ApiOperation(value = "更改支付密码", httpMethod = "POST", notes = "更改支付密码，返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "paymentPassword", value = "新支付密码", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "mobileCode", value = "短信验证码", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "mobile", value = "手机号码", required = true, dataType = "string")
	})
	@PostMapping(value="/p/updatePaymentPassword")
	public ResultDto<String> updatePaymentPassword(String paymentPassword,String mobileCode,String mobile){
		try {
			
			User user = userDetailService.getUserByMobile(mobile);
			if(user==null){
				return ResultDtoManager.fail(-1, "手机号不存在或信息已被删除");
			}
			if(AppUtils.isBlank(paymentPassword)){
				return ResultDtoManager.fail(-1, "新密码不能为空!");
			}
			
			if(!checkSMSCode(mobile,mobileCode)){
				return  ResultDtoManager.fail(-1, "短信验证码错误！");
			}
			
			myfavoriteService.updatePaymentPassword(user.getId(),user.getName(), passwordEncoder.encode(paymentPassword));
			Integer paypassVerifn = userDetailService.getPaypassVerifn(user.getUserName());	
			if(paypassVerifn==0){
				//TODO 该用户安全等级+1,支付密码验证变为1
				userDetailService.updatePaypassVerifn(user.getUserName());
				
			}
			return  ResultDtoManager.success();
		} catch (Exception e) {
			LOGGER.error("更改支付密码异常!", e);
			return  ResultDtoManager.fail(-1, "操作支付密码失败，请联系商城管理员");
		}
	}
	
	
	/**
	 * 获取用户的手机号码
	 */
	@ApiOperation(value = "获取手机号码", httpMethod = "POST", notes = "获取手机号码",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/p/getUserMobile")
	public ResultDto<String> getUserMobile() {
		
		try {
			String userId=loginedUserService.getUser().getUserId();
			
			String userMobile = appUserDetailService.getUserMobile(userId);
			return ResultDtoManager.success(userMobile);
		} catch (Exception e) {
			LOGGER.error("获取用户手机号码异常!", e);
			return  ResultDtoManager.fail();
		}
	}
	
	
	/**检查验证码时间
	 * 检查手机验证码和数据库中的是否一致 
	 * @param userName
	 * @param securityCode
	 * @return
	 */
	private boolean checkSMSCode(String mobile,String code){

		return smsLogService.verifyCodeAndClear(mobile,code, SMSTypeEnum.VAL);
	}
}
