package com.legendshop.app.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.base.util.ResourcePathUtil;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.util.SystemUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags="图片路径",value="图片路径")
public class AppPhotoPathController {
  
	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;
	
	/**
	 * 获取大图前缀
	*/
	@ApiOperation(value = "获取大图前缀", httpMethod = "GET", notes = "获取大图前缀，返回图片路径",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@RequestMapping(value = "/getPhotoPath",method=RequestMethod.GET)
	@GetMapping(value="/getPhotoPath")
	public ResultDto<String> getPhotoPath(HttpServletRequest request, HttpServletResponse response) {
		String photoServer = propertiesUtil.getPhotoServer();
		if(photoServer.equals(SystemUtil.getContextPath())){
			photoServer = propertiesUtil.getPcDomainName();
		}
		String imagePathPrefix = photoServer +ResourcePathUtil.getPhotoPathPrefix();
		String path = new StringBuilder(64).append(imagePathPrefix).toString();
		return ResultDtoManager.success(path);
	}
	
	/**
	 * 获取小图前缀
	*/
	@ApiOperation(value = "获取小图前缀", httpMethod = "POST", notes = "获取小图前缀，返回图片路径",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "scale", value = "规格", required = false, dataType = "String")
	@PostMapping(value="/getImagePath")
	public ResultDto<String> getImagePath(HttpServletRequest request, HttpServletResponse response,@RequestParam String scale) {
		String photoServer = propertiesUtil.getPhotoServer();
		if(photoServer.equals(SystemUtil.getContextPath())){
			photoServer = propertiesUtil.getPcDomainName();
		}
		String imagePathPrefix = photoServer +ResourcePathUtil.getSmallImagePathPrefix();
		String path = new StringBuilder(64).append(imagePathPrefix).append(scale).append("/").toString();
		return ResultDtoManager.success(path);
	}
}
