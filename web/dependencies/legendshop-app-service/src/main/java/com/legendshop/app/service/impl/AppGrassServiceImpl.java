package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.legendshop.business.dao.*;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.UserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.AppGrassArticleInfoDto;
import com.legendshop.app.dto.AppGrassArticleListDto;
import com.legendshop.app.dto.AppGrassCommDto;
import com.legendshop.app.dto.AppGrassConcernListDto;
import com.legendshop.app.dto.AppGrassLabelListDto;
import com.legendshop.app.dto.AppGrassProdDto;
import com.legendshop.app.dto.AppGrassWriteArticleListDto;
import com.legendshop.app.dto.AppGrassWriterInfoDto;
import com.legendshop.app.service.AppGrassService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.spi.service.GrassConcernService;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.util.AppUtils;
@SuppressWarnings("all")
@Service("appGrassService")
public class AppGrassServiceImpl implements AppGrassService {

	@Autowired
	private GrassLabelDao grassLabelDao;

	@Autowired
	private GrassArticleDao grassArticleDao;

	@Autowired
	private GrassCommDao grassCommeDao;

	@Autowired
	private GrassProdDao grassProdDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private GrassThumbDao grassThumbDao;

	@Autowired
	private GrassConcernDao grassConcernDao;

	@Autowired
	private ProductCommentDao productCommentDao;

	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private UserDetailDao UserDetailDao;

	@Autowired
	private SubItemDao subItemDao;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private GrassArticleLableDao grassArticleLableDao;

	@Override
	public AppPageSupport<AppGrassLabelListDto> queryLabel(String pageNo, String name, Integer size) {
		PageSupport<GrassLabel> ps = grassLabelDao.queryGrassLabelPage(pageNo, name, size);
		AppPageSupport<AppGrassLabelListDto> convertPageSupport = PageUtils.convertPageSupport(ps,
				new PageUtils.ResultListConvertor<GrassLabel, AppGrassLabelListDto>() {
					@Override
					public List<AppGrassLabelListDto> convert(List<GrassLabel> grassLabels) {
						List<AppGrassLabelListDto> toList = new ArrayList<AppGrassLabelListDto>();
						for (GrassLabel grassLabel : grassLabels) {
							if (AppUtils.isNotBlank(grassLabel)) {
								AppGrassLabelListDto disDto = convertToAppGrassLabelListDto(grassLabel);
								toList.add(disDto);
							}
						}
						return toList;
					}

					private AppGrassLabelListDto convertToAppGrassLabelListDto(GrassLabel grassLabel) {
						AppGrassLabelListDto appGrassLabelListDto = new AppGrassLabelListDto();
						appGrassLabelListDto.setId(grassLabel.getId());
						appGrassLabelListDto.setCreateTime(grassLabel.getCreateTime());
						appGrassLabelListDto.setIsrecommend(grassLabel.getIsrecommend());
						appGrassLabelListDto.setName(grassLabel.getName());
						appGrassLabelListDto.setRefcount(grassLabel.getRefcount());
						return appGrassLabelListDto;
					}
				});
		return convertPageSupport;
	}

	@Override
	public AppPageSupport<AppGrassArticleListDto> queryArticle(String pageNo, Integer size,String condition,String order) {
		PageSupport<GrassArticle> ps = grassArticleDao.queryGrassArticlePage(pageNo,size,condition,order);
		AppPageSupport<AppGrassArticleListDto> convertPageSupport = PageUtils.convertPageSupport(ps,
				new PageUtils.ResultListConvertor<GrassArticle, AppGrassArticleListDto>() {
					@Override
					public List<AppGrassArticleListDto> convert(List<GrassArticle> grassArticles) {
						List<AppGrassArticleListDto> toList = new ArrayList<AppGrassArticleListDto>();
						for (GrassArticle grassArticle : grassArticles) {
							if (AppUtils.isNotBlank(grassArticle)) {
								AppGrassArticleListDto disDto = convertToAppGrassArticleListDto(grassArticle);
								toList.add(disDto);
							}
						}
						return toList;
					}

					private AppGrassArticleListDto convertToAppGrassArticleListDto(GrassArticle grassArticle) {
						AppGrassArticleListDto appGrassArticleListDto = new AppGrassArticleListDto();
						appGrassArticleListDto.setId(grassArticle.getId());
						appGrassArticleListDto.setTitle(grassArticle.getTitle());
						if (AppUtils.isNotBlank(grassArticle.getImage())){
							List<String> articleImgs = new ArrayList<String>();
							String[] images = grassArticle.getImage().split(",");
							for (String image : images) {
								appGrassArticleListDto.setImage(image);
								break;
							}
						}
						appGrassArticleListDto.setCreateTime(grassArticle.getPublishTime());
						appGrassArticleListDto.setIntro(grassArticle.getIntro());
						appGrassArticleListDto.setThumbNum(grassArticle.getThumbNum());
						UserDetail user = UserDetailDao.getById(grassArticle.getUserId());
						if (AppUtils.isNotBlank(user)) {
							appGrassArticleListDto.setUid(user.getUserId());
							appGrassArticleListDto.setUserName(user.getUserName());
							appGrassArticleListDto.setUserImage(user.getPortraitPic());
						}
						return appGrassArticleListDto;
					}
				});
		return convertPageSupport;
	}

	@Override
	public AppGrassArticleInfoDto queryByDisId(Long grassId) {
		List<AppGrassProdDto> prods = new ArrayList<AppGrassProdDto>();
		List<AppGrassLabelListDto> labels = new ArrayList<AppGrassLabelListDto>();
		GrassArticle grassArticle = grassArticleDao.getGrassArticle(grassId);
		if (AppUtils.isNotBlank(grassArticle)) {
			AppGrassArticleInfoDto appGrassArticleInfoDto = new AppGrassArticleInfoDto();
			appGrassArticleInfoDto.setId(grassArticle.getId());
			appGrassArticleInfoDto.setTitle(grassArticle.getTitle());
			appGrassArticleInfoDto.setThumbNum(grassArticle.getThumbNum());
			appGrassArticleInfoDto.setContent(grassArticle.getContent());
			appGrassArticleInfoDto.setComcount(grassArticle.getCommentsNum());
			appGrassArticleInfoDto.setPublishTime(grassArticle.getPublishTime());
			if (AppUtils.isNotBlank(grassArticle.getImage())) {
				List<String> articleImgs = new ArrayList<String>();
				String[] images = grassArticle.getImage().split(",");
				for (String image : images) {
					articleImgs.add(image);
				}
				appGrassArticleInfoDto.setImages(articleImgs);
			}
			LoginedUserInfo user = loginedUserService.getUser();
			UserDetail writer = UserDetailDao.getById(grassArticle.getUserId());
			if (AppUtils.isNotBlank(writer)) {
				appGrassArticleInfoDto.setUid(writer.getUserId());
				appGrassArticleInfoDto.setUserName(writer.getUserName());
				appGrassArticleInfoDto.setUserImage(writer.getPortraitPic());
				appGrassArticleInfoDto.setUserSignature(writer.getUserSignature());
				if (AppUtils.isNotBlank(user)){
					appGrassArticleInfoDto.setRsThumb(grassThumbDao.rsThumb(appGrassArticleInfoDto.getId(), user.getUserId()) > 0);
					appGrassArticleInfoDto.setRsConcern(grassConcernDao.rsConcern(appGrassArticleInfoDto.getUid(), user.getUserId()) > 0);
				}
			}
			List<GrassProd> grassProds = grassProdDao.getGrassProdByGrassId(grassArticle.getId());
			if (AppUtils.isNotBlank(grassProds)) {
				for (GrassProd grassProd : grassProds) {
					Product product = productDao.getById(grassProd.getProdId());
					if (AppUtils.isNotBlank(product)) {
						AppGrassProdDto appGrassProdDto = new AppGrassProdDto();
						appGrassProdDto.setProdId(product.getId());
						appGrassProdDto.setGoodCommentsPercent(product.getReviewScores());
						appGrassProdDto.setName(product.getName());
						appGrassProdDto.setPic(product.getPic());
						appGrassProdDto.setPrice(product.getCash());
						appGrassProdDto.setComments(productCommentDao.getComments(product.getId()));
						prods.add(appGrassProdDto);
					}
				}
				}
				appGrassArticleInfoDto.setProds(prods);
				appGrassArticleInfoDto.setProcount(Long.parseLong(prods.size()+""));

				List<GrassLabel> grassLabels = grassLabelDao.getGrassLabelByGrassId(grassArticle.getId());
				if (AppUtils.isNotBlank(grassLabels)) {
					for (GrassLabel grassLabel : grassLabels) {
						AppGrassLabelListDto labelListDto = new AppGrassLabelListDto();
						labelListDto.setId(grassLabel.getId());
						labelListDto.setName(grassLabel.getName());
						labelListDto.setIsrecommend(grassLabel.getIsrecommend());
						labelListDto.setRefcount(grassLabel.getRefcount());
						labelListDto.setCreateTime(grassLabel.getCreateTime());
						labels.add(labelListDto);
					}
					appGrassArticleInfoDto.setLabels(labels);
				}
				return appGrassArticleInfoDto;
			}
			return null;
		}

	@Override
	public AppGrassWriterInfoDto queryWriterInfo(String grassUid) {
		AppGrassWriterInfoDto appGrassWriterInfoDto = new AppGrassWriterInfoDto();
		UserDetail user = UserDetailDao.getById(grassUid);
		if (AppUtils.isNotBlank(user)) {
			appGrassWriterInfoDto.setUserId(user.getUserId());
			appGrassWriterInfoDto.setUserName(user.getUserName());

			appGrassWriterInfoDto.setUserImage(user.getPortraitPic());
			appGrassWriterInfoDto.setUserSignature(user.getUserSignature());
		}
		appGrassWriterInfoDto.setConcernCount(grassConcernDao.getConcernCountByGrassUid(grassUid));
		appGrassWriterInfoDto.setPursuerCount(grassConcernDao.getPursuerCountByUid(grassUid));
		appGrassWriterInfoDto.setThumbCount(grassArticleDao.getThumbCountByUid(grassUid));
		if (AppUtils.isNotBlank(loginedUserService.getUser())) {
			appGrassWriterInfoDto
					.setRsConcern(grassConcernDao.rsConcern(grassUid, loginedUserService.getUser().getUserId())>0);
		}
		return appGrassWriterInfoDto;
	}

	@Override
	public AppPageSupport<AppGrassCommDto> queryGrassCommByGrassId(Long grassId,String currPage) {
		PageSupport<GrassComm> ps = grassCommeDao.queryGrassCommByGrassId(currPage,20,grassId);
		AppPageSupport<AppGrassCommDto> convertPageSupport = PageUtils.convertPageSupport(ps,
				new PageUtils.ResultListConvertor<GrassComm, AppGrassCommDto>() {
					@Override
					public List<AppGrassCommDto> convert(List<GrassComm> gassComms) {
						List<AppGrassCommDto> toList = new ArrayList<AppGrassCommDto>();
						for (GrassComm grassComm : gassComms) {
							if (AppUtils.isNotBlank(grassComm)) {
								AppGrassCommDto comDto = convertToAppGrassCommDtoListDto(grassComm);
								toList.add(comDto);
							}
						}
						return toList;
					}

					private AppGrassCommDto convertToAppGrassCommDtoListDto(GrassComm grassComm) {
						AppGrassCommDto appGrassCommDto = new AppGrassCommDto();
						appGrassCommDto.setId(grassComm.getId());
						appGrassCommDto.setArtiId(grassComm.getGraId());
						appGrassCommDto.setContent(grassComm.getContent());
						appGrassCommDto.setCreateTime(grassComm.getCreateTime());
						appGrassCommDto.setUserId(grassComm.getUserId());
						UserDetail user = userDetailService.getUserDetailById(grassComm.getUserId());
						if (AppUtils.isNotBlank(user)){
							appGrassCommDto.setUserName(user.getUserName());
							appGrassCommDto.setUserImage(user.getPortraitPic());
						}
						return appGrassCommDto;
					}
				});
		return convertPageSupport;
	}

	@Override
	public AppPageSupport<AppGrassProdDto> queryUserProd(String currPage, int size) {
		List<Long> prodIds = subItemDao.getProdIdByUser(loginedUserService.getUser().getUserId());
		PageSupport<Product> ps = productDao.getProdByUser(currPage,size,prodIds);
		AppPageSupport<AppGrassProdDto> convertPageSupport = PageUtils.convertPageSupport(ps,
				new PageUtils.ResultListConvertor<Product, AppGrassProdDto>() {
					@Override
					public List<AppGrassProdDto> convert(List<Product> products) {
						List<AppGrassProdDto> toList = new ArrayList<AppGrassProdDto>();
						for (Product product : products) {
							if (AppUtils.isNotBlank(product)) {
								AppGrassProdDto comDto = convertToAppGrassProdDtoListDto(product);
								toList.add(comDto);
							}
						}
						return toList;
					}

					private AppGrassProdDto convertToAppGrassProdDtoListDto(Product product) {
						AppGrassProdDto appGrassProdDto = new AppGrassProdDto();
						appGrassProdDto.setProdId(product.getId());
						appGrassProdDto.setGoodCommentsPercent(product.getReviewScores());
						appGrassProdDto.setName(product.getName());
						appGrassProdDto.setPic(product.getPic());
						appGrassProdDto.setPrice(product.getCash());
						appGrassProdDto.setComments(productCommentDao.getComments(product.getId()));
						return appGrassProdDto;
					}
				});
		return convertPageSupport;
	}

	@Override
	public AppPageSupport<AppGrassArticleListDto> queryArticleByLabel(String pageNo, int size, Long[] labels) {
		Set<Long> ids = new HashSet<Long>();
		for (Long label : labels) {
			ids.addAll(grassArticleLableDao.getArticleLableByLid(label));
		}
		PageSupport<GrassArticle> ps = grassArticleDao.queryGrassArticleByLabels(pageNo,size,ids);
		AppPageSupport<AppGrassArticleListDto> convertPageSupport = PageUtils.convertPageSupport(ps,
				new PageUtils.ResultListConvertor<GrassArticle, AppGrassArticleListDto>() {
					@Override
					public List<AppGrassArticleListDto> convert(List<GrassArticle> grassArticles) {
						List<AppGrassArticleListDto> toList = new ArrayList<AppGrassArticleListDto>();
						for (GrassArticle grassArticle : grassArticles) {
							if (AppUtils.isNotBlank(grassArticle)) {
								AppGrassArticleListDto disDto = convertToAppGrassArticleListDto(grassArticle);
								toList.add(disDto);
							}
						}
						return toList;
					}

					private AppGrassArticleListDto convertToAppGrassArticleListDto(GrassArticle grassArticle) {
						AppGrassArticleListDto appGrassArticleListDto = new AppGrassArticleListDto();
						appGrassArticleListDto.setId(grassArticle.getId());
						appGrassArticleListDto.setTitle(grassArticle.getTitle());
						if (AppUtils.isNotBlank(grassArticle.getImage())){
							List<String> articleImgs = new ArrayList<String>();
							String[] images = grassArticle.getImage().split(",");
							for (String image : images) {
								appGrassArticleListDto.setImage(image);
								break;
							}
						}
						appGrassArticleListDto.setCreateTime(grassArticle.getPublishTime());
						appGrassArticleListDto.setIntro(grassArticle.getIntro());
						appGrassArticleListDto.setThumbNum(grassArticle.getThumbNum());
						UserDetail user = UserDetailDao.getById(grassArticle.getUserId());
						if (AppUtils.isNotBlank(user)) {
							appGrassArticleListDto.setUid(user.getUserId());
							appGrassArticleListDto.setUserName(user.getUserName());
							appGrassArticleListDto.setUserImage(user.getPortraitPic());
						}
						return appGrassArticleListDto;
					}
				});
		return convertPageSupport;
	}

	@Override
	public AppPageSupport<AppGrassWriteArticleListDto> queryWriter(String pageNo, String writerId,
			Integer size) {
		PageSupport<GrassArticle> ps = grassArticleDao.queryArticleBywriterId(pageNo, writerId, size);
		AppPageSupport<AppGrassWriteArticleListDto> convertPageSupport = PageUtils.convertPageSupport(ps,
				new PageUtils.ResultListConvertor<GrassArticle, AppGrassWriteArticleListDto>() {
					@Override
					public List<AppGrassWriteArticleListDto> convert(List<GrassArticle> grassArticles) {
						List<AppGrassWriteArticleListDto> toList = new ArrayList<AppGrassWriteArticleListDto>();
						for (GrassArticle grassArticle : grassArticles) {
							if (AppUtils.isNotBlank(grassArticle)) {
								AppGrassWriteArticleListDto disDto = convertToAppGrassWriteArticleListDto(grassArticle);
								toList.add(disDto);
							}
						}
						return toList;
					}

					private AppGrassWriteArticleListDto convertToAppGrassWriteArticleListDto(
							GrassArticle grassArticle) {
						AppGrassWriteArticleListDto appGrassWriteArticleListDto = new AppGrassWriteArticleListDto();
						List<AppGrassProdDto> prods = new ArrayList<AppGrassProdDto>();
						appGrassWriteArticleListDto.setId(grassArticle.getId());
						appGrassWriteArticleListDto.setTitle(grassArticle.getTitle());
						if (AppUtils.isNotBlank(grassArticle.getImage())) {
							List<String> articleImgs = new ArrayList<String>();
							String[] images = grassArticle.getImage().split(",");
							for (String image : images) {
								articleImgs.add(image);
							}
							appGrassWriteArticleListDto.setImages(articleImgs);
						}
						appGrassWriteArticleListDto.setContent(grassArticle.getContent());
						appGrassWriteArticleListDto.setCreateTime(grassArticle.getCreateTime());
						appGrassWriteArticleListDto.setIntro(grassArticle.getIntro());
						appGrassWriteArticleListDto.setThumbNum(grassArticle.getThumbNum());
						appGrassWriteArticleListDto.setComcount(grassArticle.getCommentsNum());
						UserDetail user = UserDetailDao.getById(grassArticle.getUserId());
						if (AppUtils.isNotBlank(user)) {
							appGrassWriteArticleListDto.setUid(user.getUserId());
							appGrassWriteArticleListDto.setUserName(user.getUserName());
							appGrassWriteArticleListDto.setUserImage(user.getPortraitPic());
							appGrassWriteArticleListDto.setRsThumb(grassThumbDao.rsThumb(grassArticle.getId(),user.getUserId())>0);
						}
						List<GrassProd> grassProds = grassProdDao.getGrassProdByGrassId(grassArticle.getId());
						if (AppUtils.isNotBlank(grassProds)) {
							for (GrassProd grassProd : grassProds) {
								Product product = productDao.getById(grassProd.getProdId());
								AppGrassProdDto appGrassProdDto = new AppGrassProdDto();

								if(AppUtils.isNotBlank(product)){

									appGrassProdDto.setProdId(product.getId());
									appGrassProdDto.setName(product.getName());
									appGrassProdDto.setGoodCommentsPercent(product.getReviewScores());

									appGrassProdDto.setPic(product.getPic());
									appGrassProdDto.setPrice(product.getCash());
									appGrassProdDto.setComments(productCommentDao.getComments(product.getId()));

									prods.add(appGrassProdDto);
								}
							}
						}
						appGrassWriteArticleListDto.setProds(prods);
						return appGrassWriteArticleListDto;
					}
				});
		return convertPageSupport;
	}

	@Override
	public AppPageSupport<AppGrassConcernListDto> queryConcernByFlag(String pageNo,String id,boolean flag, Integer size) {
		PageSupport<GrassConcern> ps =null;
		if (flag) {
			ps= grassConcernDao.getPursuerByUid(pageNo, size, id);
		}else{
			ps= grassConcernDao.getConcernByGrassUid(pageNo, size, id);
		}
		LoginedUserInfo user = loginedUserService.getUser();
		AppPageSupport<AppGrassConcernListDto> convertPageSupport = PageUtils.convertPageSupport(ps,
				new PageUtils.ResultListConvertor<GrassConcern, AppGrassConcernListDto>() {
					@Override
					public List<AppGrassConcernListDto> convert(List<GrassConcern> grassConcerns) {
						List<AppGrassConcernListDto> toList = new ArrayList<AppGrassConcernListDto>();
						for (GrassConcern grassConcern : grassConcerns) {
							if (AppUtils.isNotBlank(grassConcern)) {
								AppGrassConcernListDto disDto = convertToAppGrassConcernListDto(grassConcern);
								toList.add(disDto);
							}
						}
						return toList;
					}

					private AppGrassConcernListDto convertToAppGrassConcernListDto(GrassConcern grassConcern) {
						AppGrassConcernListDto appGrassConcernListDto = new AppGrassConcernListDto();
						UserDetail user=null;
						if (flag) {
							 user = UserDetailDao.getById(grassConcern.getUserId());
						}else{
							 user = UserDetailDao.getById(grassConcern.getGrauserId());
						}
						if (AppUtils.isNotBlank(user)) {
							appGrassConcernListDto.setUid(user.getUserId());
							appGrassConcernListDto.setName(user.getUserName());
							appGrassConcernListDto.setImage(user.getPortraitPic());
							if (AppUtils.isNotBlank(user)) {
									appGrassConcernListDto.setRsConcern(grassConcernDao.rsConcern(user.getUserId(),loginedUserService.getUser().getUserId()) > 0);
							}
						}
						return appGrassConcernListDto;
					}
				});
		return convertPageSupport;
	}
}
