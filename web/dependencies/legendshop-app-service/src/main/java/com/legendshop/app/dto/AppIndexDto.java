package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("首页相关对象")
public class AppIndexDto {
	
	/** 图片路径 */
	@ApiModelProperty(value = "图片路径")
	private String photopath; 
	
		
	
}
