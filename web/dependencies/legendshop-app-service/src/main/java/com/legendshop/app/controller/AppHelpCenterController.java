package com.legendshop.app.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.AppNewsCategoryDto;
import com.legendshop.app.dto.AppNewsDto;
import com.legendshop.app.service.AppNewsCategoryService;
import com.legendshop.app.service.AppNewsService;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.util.AppUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 常见问题（帮助中心）
 */
@RestController
@Api(tags="常见问题",value="常见问题相关操作")
public class AppHelpCenterController {
	
	private final Logger LOGGER = LoggerFactory.getLogger(AppHelpCenterController.class);
	
    @Autowired
    private AppNewsCategoryService appNewsCategoryService;
    
    @Autowired
    private AppNewsService appNewsService;


    /**
	 * 常见问题
	 */
    @ApiOperation(value = "常见问题", httpMethod = "GET", notes = "常见问题,获取常见问题分类列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = false, dataType = "string")
    @GetMapping("/help/newsCategory")
	public ResultDto<AppPageSupport<AppNewsCategoryDto>> newsCategory(String curPageNO) {
		
    	try {
    		AppPageSupport<AppNewsCategoryDto> result = appNewsCategoryService.getNewsCategoryListPage(curPageNO);
    		
    		return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("获取常见问题列表异常!", e);
			return ResultDtoManager.fail();
		}
	}
    
    
    /**
     * 获取常见问题文章列表
     */
    @ApiOperation(value = "获取常见问题文章列表", httpMethod = "GET", notes = "获取常见问题文章列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "newsCategoryId", value = "栏目分类id", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = false, dataType = "string")
	})
    @GetMapping("/help/news")
    public ResultDto<AppPageSupport<AppNewsDto>> news(Long newsCategoryId,String curPageNO) {
    	
    	try {
    		AppPageSupport<AppNewsDto> result = appNewsService.getNewsListPage(newsCategoryId, curPageNO);
    		
    		return ResultDtoManager.success(result);
    	} catch (Exception e) {
    		LOGGER.error("获取常见问题文章列表异常!", e);
    		return ResultDtoManager.fail();
    	}
    }
    /**
     * 获取常见问题文章详情
     */
    @ApiOperation(value = "获取常见问题文章详情", httpMethod = "GET", notes = "获取常见问题文章详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "newsId", value = "文章id", required = true, dataType = "Long")
    @GetMapping("/help/newsDetail")
    public ResultDto<AppNewsDto> newsDetail(@RequestParam(required = true) Long newsId) {
    	
    	try {
    		
    		AppNewsDto result = appNewsService.getNews(newsId);
    		
    		if (AppUtils.isBlank(result)) {
    			return ResultDtoManager.fail(-1, "您查看的文章不存在或已被删除!");
			}
    		
    		return ResultDtoManager.success(result);
    		
    	} catch (Exception e) {
    		LOGGER.error("获取常见问题文章列表异常!", e);
    		return ResultDtoManager.fail();
    	}
    }
}
