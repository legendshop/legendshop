package com.legendshop.app.service;

import java.util.List;

import com.legendshop.app.dto.BrandDto;


public interface AppBrandService {

	public abstract List<BrandDto> queryBrandDto(String brandName);

}
