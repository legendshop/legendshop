package com.legendshop.app.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value="AppShopDto商家") 
public class AppShopDto implements Serializable{
	private static final long serialVersionUID = 5049919089101634020L;

	/** 店铺唯一ID */
    @ApiModelProperty(value="店铺唯一ID")  
	private Long shopId;
	
	/** 店铺名称 */
    @ApiModelProperty(value="店铺名称")  
	private String siteName;
	
	/** 店铺图片 */
    @ApiModelProperty(value="店铺图片")  
	private String logo;
	
	/** 店铺描述 */
    @ApiModelProperty(value="店铺描述")  
	private String briefDesc;
	
	/** 店铺省份 */
    @ApiModelProperty(value="店铺省份")  
	private String province;
	
	/** 店铺评分 */
    @ApiModelProperty(value="店铺评分")  
	private Double score;
	
	/** 月销量 */
    @ApiModelProperty(value="月销量")  
	private Integer sales;
	
	/**
	 * 宝贝数量
	 */
    @ApiModelProperty(value="宝贝数量")  
	private Long prodCount;
	
	/**
	 * 评论数量
	 */
    @ApiModelProperty(value="评论数量")  
	private Long commCount;

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getBriefDesc() {
		return briefDesc;
	}

	public void setBriefDesc(String briefDesc) {
		this.briefDesc = briefDesc;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Integer getSales() {
		return sales;
	}

	public void setSales(Integer sales) {
		this.sales = sales;
	}
	
	public Long getProdCount() {
		return prodCount;
	}

	public void setProdCount(Long prodCount) {
		this.prodCount = prodCount;
	}

	public Long getCommCount() {
		return commCount;
	}

	public void setCommCount(Long commCount) {
		this.commCount = commCount;
	}
	
}

