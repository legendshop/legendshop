package com.legendshop.app.util;

/**
 * 检查输入字段的合法性
 * @author Administrator
 *
 */
public class InputCheckingUtil {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		System.out.println("result = " + checkNickName("对对对1213"));
		System.out.println("result = " + checkNickName("dddd"));
		System.out.println("result = " + checkNickName("123123"));
		System.out.println("result = " + checkNickName("1df123基督教@@！￥"));
	}
	
	/**
	 * 昵称可以是数字，英文或者中文，不能是全数字
	 * @param nickName
	 * @return
	 */
	public static boolean checkNickName(String nickName){
		boolean a=nickName.matches("^[a-zA-Z0-9\u4E00-\u9FA5]+$"); //是否只有数字，英文或者中文
		boolean b=nickName.matches("[0-9]+"); //是否全部数字
		return a && !b;
	}
	
	/**
	 * 检查是否是手机
	 * @param nickName
	 * @return
	 */
	public static boolean checkMobile(String nickName){
		boolean a=nickName.matches("^[a-zA-Z0-9\u4E00-\u9FA5]+$"); //是否只有数字，英文或者中文
		boolean b=nickName.matches("[0-9]+"); //是否全部数字
		return a && !b;
	}
	
}
