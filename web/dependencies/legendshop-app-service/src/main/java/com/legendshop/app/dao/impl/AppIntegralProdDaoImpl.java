package com.legendshop.app.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.legendshop.app.dao.AppIntegralProdDao;
import com.legendshop.app.dto.AppIntegralProdListDto;
import com.legendshop.base.util.PageUtils;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.ProductTypeEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.integral.IntegralProd;
import com.legendshop.util.AppUtils;

/**
 * 积分商品
 */
@Repository 
public class AppIntegralProdDaoImpl extends GenericDaoImpl<IntegralProd, Long> implements AppIntegralProdDao {

	/* 
	 * 根据ID获取数据
	 */
	@Override
	public IntegralProd getIntegralProd(Long id) {
		return getById(id);
	}

	/* 
	 * 更改库存
	 */
	@Override
	@CacheEvict(value="IntegralProdDto",key="#id")
	public int updateStock(Integer count, Long id) {
		return update("update ls_integral_prod set prod_stock=prod_stock-?,sale_num=sale_num+? where id=? and (prod_stock-?)>=0 and prod_stock>0 and status=1",
				new Object[]{count,count,id,count});
	}

	private static String sqlGetCategory = "select n.id as id, n.parent_id as parentId, n.name as name ,n.pic as pic ,n.status  as status, n.type_id as typeId,n.keyword as keyword,n.cat_desc as catDesc,n.title as title,n.grade as grade,n.seq as seq from ls_category n  where  n.status = 1 and  n.type= ? order by n.grade,n.seq";
	/* 
	 * 获取所有分类
	 */
	@Override
	@Cacheable(value="AllIntegralCategoryDtoList")
	public List<Category> findAllIntegralCategory() {
		List<Category> categories=query(sqlGetCategory, Category.class,  ProductTypeEnum.INTEGRAL.value());
		if(AppUtils.isBlank(categories)){
			return null;
		}
		return categories;
	}

	/* 
	 * 查询分页数据
	 */
	@Override
	public PageSupport<IntegralProd> getIntegralProd(String curPageNO, String integralSort, String saleNumSort) {
		CriteriaQuery cq = new CriteriaQuery(IntegralProd.class, curPageNO);
		cq.setPageSize(8);
		cq.eq("status", 1);
		if(AppUtils.isNotBlank(integralSort)&&integralSort.equals("asc")){
			cq.addAscOrder("exchangeIntegral");
		}else if(AppUtils.isNotBlank(integralSort)&&integralSort.equals("desc")){
			cq.addDescOrder("exchangeIntegral"); //兑换的积分
		}
		
		if(AppUtils.isNotBlank(saleNumSort)&&saleNumSort.equals("asc")){
			cq.addAscOrder("saleNum"); //销售数量
		}else if(AppUtils.isNotBlank(saleNumSort)&&saleNumSort.equals("desc")){
			cq.addDescOrder("saleNum");
		}
		PageSupport<IntegralProd> ps = queryPage(cq);
		return ps;
	}

	/* 
	 * 查询分页数据
	 */
	@Override
	public AppPageSupport<AppIntegralProdListDto> getIntegralProdList(String curPageNO, Integer pageSize,Boolean isCommend, String order) {
		CriteriaQuery cq = new CriteriaQuery(IntegralProd.class, curPageNO);
		cq.setPageSize(pageSize);
		cq.eq("status", 1);
		if(isCommend){
			cq.eq("isCommend", 1);
		}
		if(AppUtils.isNotBlank(order)&&order.equals("saleNum")){
			cq.addDescOrder("saleNum"); //销售数量
		}
		PageSupport<IntegralProd> ps = queryPage(cq);
		AppPageSupport<AppIntegralProdListDto> convertPageSupport = PageUtils.convertPageSupport(ps,new PageUtils.ResultListConvertor<IntegralProd, AppIntegralProdListDto>() {
			@Override
			public List<AppIntegralProdListDto> convert(List<IntegralProd> form) {
				List<AppIntegralProdListDto> toList = new ArrayList<AppIntegralProdListDto>();
				for (IntegralProd integralProd : form) {
					if (AppUtils.isNotBlank(integralProd)) {
						AppIntegralProdListDto prodDto = convertToAppIntegralProdListDto(integralProd);
						toList.add(prodDto);
					}
				}
				return toList;
			}
			private AppIntegralProdListDto convertToAppIntegralProdListDto(IntegralProd integralProd){
				AppIntegralProdListDto prodDto = new AppIntegralProdListDto();
				prodDto.setId(integralProd.getId());
				prodDto.setExchangeIntegral(integralProd.getExchangeIntegral());
				prodDto.setIsCommend(integralProd.getIsCommend());
				prodDto.setName(integralProd.getName());
				prodDto.setProdImage(integralProd.getProdImage());
				prodDto.setProdStock(integralProd.getProdStock());
				prodDto.setSaleNum(integralProd.getSaleNum());
				prodDto.setStatus(integralProd.getStatus());
				return prodDto;
			}
		});
		return convertPageSupport;
	}

}
