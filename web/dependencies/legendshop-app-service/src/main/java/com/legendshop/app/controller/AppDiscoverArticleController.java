package com.legendshop.app.controller;

import java.util.Date;
import java.util.Set;

import com.legendshop.business.service.weixin.WeixinMiniProgramApi;
import com.legendshop.model.dto.weixin.WeiXinMsg;
import com.legendshop.spi.service.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.AppDiscoverArticleInfoDto;
import com.legendshop.app.dto.AppDiscoverArticleListDto;
import com.legendshop.app.dto.AppDiscoverCommDto;
import com.legendshop.app.service.AppDiscoverArticleService;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.AppArticleThumb;
import com.legendshop.model.entity.DiscoverArticle;
import com.legendshop.model.entity.DiscoverComme;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.util.AppUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@Api(tags = "发现文章", value = "发现文章相关接口")
@RestController()
public class AppDiscoverArticleController {

	@Autowired
	private AppDiscoverArticleService appDiscoverArticleService;

	@Autowired
	private DiscoverCommeService discoverCommeService;

	@Autowired
	private DiscoverArticleService discoverArticleService;

	@Autowired
	private WeixinMpTokenService weixinMpTokenService;

	@Autowired
	private LoginedUserService loginedUserService;
	
	@Autowired
	private AppArticleThumbService appArticleThumbService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private SensitiveWordService sensitiveWordService;

	@ApiOperation(value = "获取文章列表", httpMethod = "POST", notes = "获取文章列表数据", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "curPageNO", value = "当前页", dataType = "String", required = false),
		@ApiImplicitParam(paramType = "query", name = "condition", value = "作者名 或者 标题", dataType = "String", required = false),
		@ApiImplicitParam(paramType = "query", name = "order", value = "排序条件  综合synthesize 最热hot 最新new", dataType = "String", required = true)
	})
	@PostMapping("/discoverList")
	public ResultDto<AppPageSupport<AppDiscoverArticleListDto>> discoverList(String curPageNO,String condition,String order) {
		String index = AppUtils.isNotBlank(curPageNO) ? curPageNO : "1";
		AppPageSupport<AppDiscoverArticleListDto> disSupport = appDiscoverArticleService.queryPage(index, 10,condition,order);
		return ResultDtoManager.success(disSupport);
	}



	@ApiOperation(value = "获取文章详情", httpMethod = "POST", notes = "获取文章详情数据", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "query", name = "discoverId", value = "文章id", dataType = "Long", required = true)
	@PostMapping("/discoverInfo")
	public ResultDto<AppDiscoverArticleInfoDto> discoverInfo(Long discoverId) {
		AppDiscoverArticleInfoDto discoverArticleInfoDto = appDiscoverArticleService.queryByDisId(discoverId);
		if (discoverArticleInfoDto == null) {
			return ResultDtoManager.fail("文章不存在，或已下架");


		}
		return ResultDtoManager.success(discoverArticleInfoDto);
	}


	@ApiOperation(value="获取文章评论列表",httpMethod = "POST",notes = "获取文章评论列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query",name="curPageNO",value = "当前页",dataType = "String",required = false),
			@ApiImplicitParam(paramType = "query",name="disId",value = "发现文章ID",dataType = "Long",required = true)
	})
	@PostMapping("/discoverCommentList")
	public ResultDto<AppPageSupport<AppDiscoverCommDto>> discoverCommentList(String curPageNO, Long disId){
		String index="".equals(curPageNO)?"1":curPageNO;
		AppPageSupport<AppDiscoverCommDto> appDiscoverCommDto = appDiscoverArticleService.queryDisCommByGrassId(disId,index);
		return ResultDtoManager.success(appDiscoverCommDto);
	}



	@ApiOperation(value = "点赞", httpMethod = "POST", notes = "true点赞 false取消点赞",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "discoverId", value = "文章id", dataType = "Long", required = true),
		@ApiImplicitParam(paramType = "query", name = "rsThumb", value = "标识", dataType = "boolean", required = true)
	})
	@PostMapping("/p/discoverThum")
		public ResultDto<String> discoverThum(Long discoverId,boolean rsThumb){
			try {

				// 获取文章
				DiscoverArticle discoverArticle= discoverArticleService.getDiscoverArticle(discoverId);
				if (AppUtils.isBlank(discoverArticle)) {

					return ResultDtoManager.fail(-1,"您要点赞的文章不存在或已被删除~~");
				}

				LoginedUserInfo token = loginedUserService.getUser();

				// 获取我的点赞
				Integer myThumb = appArticleThumbService.rsThumb(discoverId, token.getUserId());
				Long num=0l;

				// 点赞
				if (rsThumb) {
					if (myThumb>0){
						return ResultDtoManager.fail("您已经点过赞了哦~~");
					}
					AppArticleThumb appArticleThumb = new AppArticleThumb();
					appArticleThumb.setArtiId(discoverArticle.getId());
					appArticleThumb.setUserId(token.getUserId());
					appArticleThumbService.saveAppArticleThumb(appArticleThumb);
					num = discoverArticle.getThumbNum()+1;
				}else {

					num = discoverArticle.getThumbNum()-1;
					appArticleThumbService.deleteAppArticleThumbByDiscoverId(discoverArticle.getId(),token.getUserId());

				}
				discoverArticle.setThumbNum(num);
				discoverArticleService.updateDiscoverArticle(discoverArticle);


				return ResultDtoManager.success(rsThumb?"点赞成功！":"取消成功！");
			} catch (Exception e) {
				return ResultDtoManager.fail("点赞失败");
			}
		}



	@ApiOperation(value = "发表评论", httpMethod = "POST", notes = "发表评论", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", name = "content", value = "评论内容", dataType = "String", required = false),
			@ApiImplicitParam(paramType = "query", name = "disId", value = "文章id", dataType = "Long", required = true)
	})
	@PostMapping("/p/saveDiscoverComme")
	public ResultDto<Object> saveDiscoverComme(String content,Long disId) {
		if (content.length()>500){
			return ResultDtoManager.fail("字数过长（500以内）");
		}
		// 敏感字过滤
		//Set<String> findwords = sensitiveWordService.checkSensitiveWords(content);
		//if (AppUtils.isNotBlank(findwords)) {
		//	return ResultDtoManager.fail("亲, 您提交的内容含有敏感词 " + findwords + ", 请更正再提交！");
		//}
		String  accessToken = weixinMpTokenService.getAccessToken();
		if(StringUtils.isNotBlank(accessToken) && StringUtils.isNotBlank(content)){
			WeiXinMsg checkTitResult = WeixinMiniProgramApi.checkMsg(accessToken, content);
			if(checkTitResult == null || !"ok".equals(checkTitResult.getErrmsg())){
				return ResultDtoManager.fail("亲, 您提交的内容含有敏感词 , 请更正再提交！");
			}
		}
		DiscoverArticle discoverArticle = discoverArticleService.getDiscoverArticle(disId);
		discoverArticle.setCommentsNum(discoverArticle.getCommentsNum() + 1);
		discoverArticleService.updateDiscoverArticle(discoverArticle);
		UserDetail user = userDetailService.getUserDetailByIdNoCache(loginedUserService.getUser().getUserId());
		DiscoverComme discoverComme=new DiscoverComme();
		discoverComme.setDisId(disId);
		discoverComme.setContent(content);
		discoverComme.setUserId(user.getId());
		discoverComme.setUserName(user.getUserName());
		discoverComme.setUserImage(user.getPortraitPic());
		discoverComme.setCreateTime(new Date());
		Long rs= discoverCommeService.saveDiscoverComme(discoverComme);
		if (rs > 0) {
			return ResultDtoManager.success("评论成功");
		} else {
			return ResultDtoManager.fail("评论失败");
		}
	}

}
