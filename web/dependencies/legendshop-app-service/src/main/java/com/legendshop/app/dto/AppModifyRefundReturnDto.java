/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app 退款及退货回显数据DTO
 */
@ApiModel(value="退货退款Dto")
public class AppModifyRefundReturnDto {
	
	/** 订单ID */
	@ApiModelProperty(value="订单ID")
	private Long orderId;

	/** 订单类型 */
	@ApiModelProperty(value="订单类型 ")
	private String subType;
	
	/** 订单项Id */
	@ApiModelProperty(value="订单项Id")
	private Long orderItemId;
	
	/** 订单号 */
	@ApiModelProperty(value="订单号")
	private String subNumber;
	
	/** 订单总金额 */
	@ApiModelProperty(value="订单总金额")
	private Double subMoney;
		
	/** 商品名称 或 订单编号 */
	@ApiModelProperty(value="商品名称 或 订单编号")
	private String name; 
	
	/** 商品属性 */
	@ApiModelProperty(value="商品属性")
	private String attribute;
	
	/** 商品购买数量 */
	@ApiModelProperty(value="商品购买数量")
	private Long basketCount;
		
	/** 退款金额 */
	@ApiModelProperty(value="退款金额")
	private Double refundAmount;
	
	/** 退货数量 */
	@ApiModelProperty(value="退货数量 ")
	private Integer goodsCount;
	
	/** shop Id */
	@ApiModelProperty(value="店铺id")
	private Long shopId;
	
	/** 店铺名称  */
	@ApiModelProperty(value="店铺名称")
	private String shopName;

	/** 订单商品ID */
	@ApiModelProperty(value="订单商品ID")
	private Long prodId; 
		
	/** 订单SKU ID */
	@ApiModelProperty(value="订单SKU ID")
	private Long skuId; 
	
	/** 商品名称 */
	@ApiModelProperty(value="商品名称")
	private String prodName;
	
	/** 商品图片 */
	@ApiModelProperty(value="商品图片")
	private String prodPic;
	
	/** 预售支付方式  0  全额支付, 1 订金支付的订金  2 订金支付尾款   PayPctTypeEnum */
	@ApiModelProperty(value="如果是预售订单，预售支付方式  0  全额支付, 1 订金支付的订金  2 订金支付尾款  ")
	private Integer payPctType;
	
	/**预售订金退款金额 */
	@ApiModelProperty(value="预售订金退款金额")
	private Double depositRefundAmount;
	
	/**是否支付订金 */
	@ApiModelProperty(value="是否支付订金")
	private Integer isPayDeposit;
	
	/**预售尾款退款金额 */
	@ApiModelProperty(value="预售尾款退款金额")
	private Double finalRefundAmount;
	
	/**是否支付尾款 */
	@ApiModelProperty(value="是否支付尾款")
	private Integer isPayFinal;
	
	/**预售尾款支付类型Id */
	@ApiModelProperty(value="预售尾款支付类型Id")
	private String finalPayTypeId;
	
	/** 预售尾款支付类型名称 */
	@ApiModelProperty(value="预售尾款支付类型名称")
	private String finalPayTypeName;
	
	/** 预售尾款支付流水号 */
	@ApiModelProperty(value="预售尾款支付流水号")
	private String finalFlowTradeNo;
	
	/**  ls_sub_settlement 预售尾款 清算单据号 */
	@ApiModelProperty(value="预售尾款 清算单据号")
	private String finalSettlementSn;
	
	/**预售尾款  ls_sub_settlement 创建时间*/
	@ApiModelProperty(value="预售尾款 创建时间")
	private Date finalOrderDateTime;
	
	/** 退款记录ID */
	@ApiModelProperty(value="退款记录ID")
	private Long refundId; 
	
	/** 申请类型:1,仅退款,2退款退货,默认为1 */
	@ApiModelProperty(value="申请类型:1,仅退款,2退款退货,默认为1")
	private int applyType;
	
	/** 文件凭证1 */
	@ApiModelProperty(value="文件凭证1")
	private  String photoFile1; 
		
	/** 文件凭证2 */
	@ApiModelProperty(value="文件凭证2")
	private   String photoFile2; 
		
	/** 文件凭证3 */
	@ApiModelProperty(value="文件凭证3")
	private   String photoFile3; 
	
	/** 退款说明 */
	@ApiModelProperty(value="退款说明")
	private String reasonInfo; 
	
	/** 申请原因 */
	@ApiModelProperty(value="申请原因")
	private String buyerMessage; 
		
		

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public Long getOrderItemId() {
		return orderItemId;
	}

	public void setOrderItemId(Long orderItemId) {
		this.orderItemId = orderItemId;
	}

	public Double getSubMoney() {
		return subMoney;
	}

	public void setSubMoney(Double subMoney) {
		this.subMoney = subMoney;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public Long getBasketCount() {
		return basketCount;
	}

	public void setBasketCount(Long basketCount) {
		this.basketCount = basketCount;
	}

	public Double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}

	public Integer getGoodsCount() {
		return goodsCount;
	}

	public void setGoodsCount(Integer goodsCount) {
		this.goodsCount = goodsCount;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getProdPic() {
		return prodPic;
	}

	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}

	public Integer getPayPctType() {
		return payPctType;
	}

	public void setPayPctType(Integer payPctType) {
		this.payPctType = payPctType;
	}

	public Double getDepositRefundAmount() {
		return depositRefundAmount;
	}

	public void setDepositRefundAmount(Double depositRefundAmount) {
		this.depositRefundAmount = depositRefundAmount;
	}

	public Integer getIsPayDeposit() {
		return isPayDeposit;
	}

	public void setIsPayDeposit(Integer isPayDeposit) {
		this.isPayDeposit = isPayDeposit;
	}

	public Double getFinalRefundAmount() {
		return finalRefundAmount;
	}

	public void setFinalRefundAmount(Double finalRefundAmount) {
		this.finalRefundAmount = finalRefundAmount;
	}

	public Integer getIsPayFinal() {
		return isPayFinal;
	}

	public void setIsPayFinal(Integer isPayFinal) {
		this.isPayFinal = isPayFinal;
	}

	public String getFinalPayTypeId() {
		return finalPayTypeId;
	}

	public void setFinalPayTypeId(String finalPayTypeId) {
		this.finalPayTypeId = finalPayTypeId;
	}

	public String getFinalPayTypeName() {
		return finalPayTypeName;
	}

	public void setFinalPayTypeName(String finalPayTypeName) {
		this.finalPayTypeName = finalPayTypeName;
	}

	public String getFinalFlowTradeNo() {
		return finalFlowTradeNo;
	}

	public void setFinalFlowTradeNo(String finalFlowTradeNo) {
		this.finalFlowTradeNo = finalFlowTradeNo;
	}

	public String getFinalSettlementSn() {
		return finalSettlementSn;
	}

	public void setFinalSettlementSn(String finalSettlementSn) {
		this.finalSettlementSn = finalSettlementSn;
	}

	public Date getFinalOrderDateTime() {
		return finalOrderDateTime;
	}

	public void setFinalOrderDateTime(Date finalOrderDateTime) {
		this.finalOrderDateTime = finalOrderDateTime;
	}

	public Long getRefundId() {
		return refundId;
	}

	public void setRefundId(Long refundId) {
		this.refundId = refundId;
	}

	public int getApplyType() {
		return applyType;
	}

	public void setApplyType(int applyType) {
		this.applyType = applyType;
	}

	public String getPhotoFile1() {
		return photoFile1;
	}

	public void setPhotoFile1(String photoFile1) {
		this.photoFile1 = photoFile1;
	}

	public String getPhotoFile2() {
		return photoFile2;
	}

	public void setPhotoFile2(String photoFile2) {
		this.photoFile2 = photoFile2;
	}

	public String getPhotoFile3() {
		return photoFile3;
	}

	public void setPhotoFile3(String photoFile3) {
		this.photoFile3 = photoFile3;
	}

	public String getReasonInfo() {
		return reasonInfo;
	}

	public void setReasonInfo(String reasonInfo) {
		this.reasonInfo = reasonInfo;
	}

	public String getBuyerMessage() {
		return buyerMessage;
	}

	public void setBuyerMessage(String buyerMessage) {
		this.buyerMessage = buyerMessage;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}	
	
	
	
}
