package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app 相关专题Dto
 */
@ApiModel(value="相关专题Dto")
public class AppThemeRelatedDto {

	/** 相关专题id */

	@ApiModelProperty(value="相关专题id")
	private Long relatedId; 
		
	/** 专题id */
	@ApiModelProperty(value="专题id")
	private Long themeId; 
		
	/** 相关专题图片 */
	@ApiModelProperty(value="相关专题图片")
	private String img; 
		
	/** 手机端相关专题链接 */
	@ApiModelProperty(value="手机端相关专题链接")
	private String mLink;

	/** 手机端相关专题ID*/
	@ApiModelProperty(value="手机端相关专题ID")
	private Long mRelatedThemeId;

	public Long getmRelatedThemeId() {
		return mRelatedThemeId;
	}

	public void setmRelatedThemeId(Long mRelatedThemeId) {
		this.mRelatedThemeId = mRelatedThemeId;
	}

	public Long getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(Long relatedId) {
		this.relatedId = relatedId;
	}

	public Long getThemeId() {
		return themeId;
	}

	public void setThemeId(Long themeId) {
		this.themeId = themeId;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getmLink() {
		return mLink;
	}

	public void setmLink(String mLink) {
		this.mLink = mLink;
	}
	
	
}
