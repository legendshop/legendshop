package com.legendshop.app.controller;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.AppActiveBanner;
import com.legendshop.app.dto.AppCheckGroupDto;
import com.legendshop.app.dto.AppMergeGroupPosterDataDto;
import com.legendshop.app.dto.mergeGroup.AppMergeGroupDetailDto;
import com.legendshop.app.dto.mergeGroup.AppMergeGroupDto;
import com.legendshop.app.dto.mergeGroup.AppMergeGroupGroupDetailDto;
import com.legendshop.app.dto.mergeGroup.AppMergeGroupingDto;
import com.legendshop.app.service.AppMergeGroupService;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.business.service.weixin.WeixinMiniProgramApi;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.GroupStatusEnum;
import com.legendshop.model.constant.VisitSourceEnum;
import com.legendshop.model.constant.VisitTypeEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.dto.weixin.WXACodeResultDto;
import com.legendshop.model.entity.MergeGroup;
import com.legendshop.model.entity.MergeGroupOperate;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.VisitLog;
import com.legendshop.processor.VisitLogProcessor;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.MergeGroupOperateService;
import com.legendshop.spi.service.MergeGroupService;
import com.legendshop.spi.service.MyfavoriteService;
import com.legendshop.spi.service.StockService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.spi.service.WeixinMpTokenService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.uploader.model.FeignMultipartFile;
import com.legendshop.util.AppUtils;
import com.legendshop.util.MD5Util;
import com.legendshop.web.helper.IPHelper;
import com.legendshop.web.util.WeiXinUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@Slf4j
@Api(tags = "拼团活动", value = "拼团活动相关接口")
@RestController
public class AppMergeGroupController {

	private final static Logger LOGGER = LoggerFactory.getLogger(AppMergeGroupController.class);
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private MyfavoriteService myFavoriteService;
	
	@Autowired
	private AppMergeGroupService appMergeGroupService;
	
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private MergeGroupService mergeGroupService;

	@Autowired
	private StockService stockService;
	
	@Autowired
	private MergeGroupOperateService mergeGroupOperateService;
	
	/**
	 * 日志查看
	 */
	@Autowired
	private VisitLogProcessor visitLogProcessor;
	
	@Autowired
	private AttachmentManager attachmentManager;
	
	@Autowired
	private WeixinMpTokenService weixinMpTokenService;

	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	/**
	 * 检查拼团下单
	 * @param mergeGroupId
	 * @param productId
	 * @param skuId
	 * @param number
	 * @return
	 */
	@ApiOperation(value = "检查拼团下单", httpMethod = "POST", notes = "检查拼团下单，成功返回ok",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "mergeGroupId", value = "拼团id", required = true, dataType = "Long"),
			@ApiImplicitParam(paramType="query", name = "productId", value = "商品id", required = true, dataType = "Long"),
			@ApiImplicitParam(paramType="query", name = "skuId", value = "skuId", required = true, dataType = "Long"),
			@ApiImplicitParam(paramType="query", name = "number", value = "数量", required = true, dataType = "Integer")
	})
	@PostMapping("/p/app/mergeGroup/checkGroupOrder")
	public ResultDto<AppCheckGroupDto> checkGroupOrder(HttpServletRequest request, HttpServletResponse response, Long mergeGroupId, Long productId,
													   Long skuId, Integer number) {

		LoginedUserInfo token = loginedUserService.getUser();
		String userId = token.getUserId();
//		Long shopId = token.getShopId();
		UserDetail userDetail = userDetailService.getUserDetailById(userId);
		Long shopId = userDetail.getShopId();
		AppCheckGroupDto checkDto = new AppCheckGroupDto();
		MergeGroup mergeGroup = mergeGroupService.getMergeGroup(mergeGroupId);
		if (mergeGroup == null) {
			return ResultDtoManager.fail(-1,"找不到拼团信息");
		}
		if (shopId != null && mergeGroup.getShopId().equals(shopId)) {
			return ResultDtoManager.fail(-1,"不能购买自己的商品");
		}
		if (GroupStatusEnum.ONLINE.value().intValue() != mergeGroup.getStatus()) {
			return ResultDtoManager.fail(-1,"团购未上线");
		}
		Date startTime = mergeGroup.getStartTime();
		Date endTime = mergeGroup.getEndTime();
		Date now = new Date();
		if (now.getTime() >= endTime.getTime()) { // 活动已经过期
			return ResultDtoManager.fail(-1,"拼团活动已经过期");
		} else if (now.getTime() < startTime.getTime()) { // 距离活动开始时间
			return ResultDtoManager.fail(-1,"拼团活动未开始");
		}

		/*
		 * 1.付款减库存 简单的检查商品库存够不够
		 */
		Integer stocks = stockService.getStocksByMode(productId, skuId);
		if (stocks - number < 0) {
			return ResultDtoManager.fail(-1,"团购商品库存不足,请选择其他团购商品");
		}
		if (mergeGroup.getLimitNumber() != null && mergeGroup.getLimitNumber() > 0) {
			if (number > mergeGroup.getLimitNumber()) {
				return ResultDtoManager.fail(-1,"超出活动限购数");
			}
		}
		StringBuffer buffer = new StringBuffer().append(mergeGroupId).append("_").append(productId).append("_").append(skuId);
		String _md5 = MD5Util.toMD5(buffer.toString() + Constants._MD5);
		checkDto.setToken(_md5);
		checkDto.setGroupId(mergeGroupId);
		return ResultDtoManager.success(checkDto);
	}

	
	@ApiOperation(value = "获取拼团专区的banner图列表", notes = "获取拼团专区的banner图列表", httpMethod="POST")
	@PostMapping("/app/mergeGroup/banners")
	public ResultDto<List<AppActiveBanner>> getActiveBanners() {
		try {
			List<AppActiveBanner> result = appMergeGroupService.getActiveBanners();
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("获取拼团专区的banner图列表失败!", e);
			return ResultDtoManager.fail();
		}
	}
	
	@ApiOperation(value = "获取拼团列表", notes = "获取拼团列表", httpMethod="POST")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "curPageNO", paramType="query",value = "页码", required = true, dataType = "String")
	})
	@PostMapping("/app/mergeGroup/list")
	public ResultDto<AppPageSupport<AppMergeGroupDto>> mergeGroupList(String curPageNO) {
		try {
			AppPageSupport<AppMergeGroupDto> ps = appMergeGroupService.getAvailableMergeGroups(curPageNO);

			return ResultDtoManager.success(ps);
		} catch (Exception e) {
			LOGGER.error("获取拼团列表失败!", e);
			return ResultDtoManager.fail();
		}
	}

	@ApiOperation(value = "获取拼团活动详情", notes = "获取拼团活动详情", httpMethod="POST")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "mergeGroupId", paramType="query",value = "活动Id", required = true, dataType = "Long"),
		@ApiImplicitParam(name = "addNumber", paramType="query",value = "已开团的团编号(当用户是被邀请过来的, 会带上邀请参团的团编号)", required = false, dataType = "String"),
	})
	@PostMapping("/app/mergeGroup/detail")
	public ResultDto<AppMergeGroupDetailDto> mergeGroupDetail(HttpServletRequest request, 
			@RequestParam(required = true) Long mergeGroupId, String addNumber) {
		
		try{
			
			LoginedUserInfo appToken = loginedUserService.getUser();
			String userId = null;
			if(appToken != null) {
				userId = appToken.getUserId();
			}
			
			Map<String, Object> result = appMergeGroupService.getMergeGroupDetail(mergeGroupId, addNumber, userId);
			String status = (String) result.get("status");
			if(!Constants.SUCCESS.equals(status)){
				String errorMsg = (String) result.get("error");
				return ResultDtoManager.fail(-1, errorMsg);
			}
			
			AppMergeGroupDetailDto mergeGroupDetail = (AppMergeGroupDetailDto) result.get("mergeGroupDetail");

			Boolean isCollect = false;//用户是否已收藏
			if (null != appToken) {//如果用户已登录
				UserDetail userDetail = userDetailService.getUserDetailById(appToken.getUserId());
				if (AppUtils.isNotBlank(userDetail)) {
					//查询当前用户是否已收藏此商品
					isCollect = myFavoriteService.isExistsFavorite(mergeGroupDetail.getProdId(), appToken.getUserName());
				}
				
				// 多线程记录访问历史
				if (systemParameterUtil.isVisitLogEnable()) {
					String visitSource = null;
					if (WeiXinUtil.isWeiXin(request)) {// 微信访问
						visitSource = VisitSourceEnum.WEIXIN.value();
					} else {
						visitSource = VisitSourceEnum.APP.value();
					}
					
					VisitLog visitLog = new VisitLog(IPHelper.getIpAddr(request), appToken.getUserName(), mergeGroupDetail.getShopId(), mergeGroupDetail.getShopName(), 
							mergeGroupDetail.getProdId(), mergeGroupDetail.getProdName(), mergeGroupDetail.getPic(), mergeGroupDetail.getCash(), 
							VisitTypeEnum.PROD.value(), visitSource, new Date());
					visitLogProcessor.process(visitLog);
				}
			}
			
			mergeGroupDetail.setIsCollect(isCollect);
			return ResultDtoManager.success(mergeGroupDetail);
		} catch (Exception e) {
			LOGGER.error("系统异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	@ApiOperation(value = "获取已开团详情", notes = "获取已开团详情", httpMethod="POST")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "sn", paramType="query",value = "编号, 当来源是SUB时, sn传订单号(subNumber);当来源是GROUP, sn传团编号(addNumber或groupNumber)", required = true, dataType = "String"),
		@ApiImplicitParam(name = "source", paramType="query",value = "来源, 订单: SUB, 团编号: GROUP", required = true, dataType = "String"),
	})
	@PostMapping(value="/app/mergeGroup/groupDetail")
	public  ResultDto<AppMergeGroupGroupDetailDto> groupDetail(HttpServletRequest request, String sn, 
			@RequestParam(defaultValue = "GROUP") String source){
		
		try{
			log.info("============================ 获取开团详情 ============================");
			log.info("============================ sn: {}, source: {} ============================", sn, source);
			
			if(AppUtils.isBlank(sn)){
				return ResultDtoManager.fail(-1,"对不起, 请不要非法访问!");
			}
			
			if(!"SUB".equals(source) && !"GROUP".equals(source)){
				return ResultDtoManager.fail(-1,"对不起, 请不要非法访问!");
			}
			
			LoginedUserInfo appToken = loginedUserService.getUser();
			
			String userId = null;
			if(appToken != null) {
				userId = appToken.getUserId();
			}
			
			Map<String, Object> result = appMergeGroupService.getMergeGroupGroupDetail(sn, source, userId);
			String status = (String) result.get("status");
			if(!Constants.SUCCESS.equals(status)){
				String errorMsg = (String) result.get("error");
				return ResultDtoManager.fail(-1, errorMsg);
			}
			
			AppMergeGroupGroupDetailDto mergeGroupGroupDetail = (AppMergeGroupGroupDetailDto) result.get("mergeGroupGroupDetail");
			
			return ResultDtoManager.success(mergeGroupGroupDetail);
		} catch (Exception e){
			e.printStackTrace();
			LOGGER.error("系统异常!", e);
			return ResultDtoManager.fail();
		} 
	}
	
	@ApiOperation(value = "获取正在进行的团列表", notes = "获取正在进行的团列表", httpMethod="POST")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "mergeGroupId", paramType="query",value = "拼团活动Id", required = true, dataType = "Long")
	})
	@PostMapping(value = "/app/mergeGroup/addUser/list")
	public ResultDto<List<AppMergeGroupingDto>> addUserList(Long mergeGroupId) {
		try{
			
			List<AppMergeGroupingDto> resultLilst = appMergeGroupService.getMergeGroupingDto(mergeGroupId, null);
			
			return ResultDtoManager.success(resultLilst);
		} catch (Exception e) {
			LOGGER.error("系统异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 获取邀请好友拼团海报相关的数据
	 * @param addNumber
	 * @return
	 */
	@ApiOperation(value = "获取邀请好友拼团海报相关的数据", notes = "获取邀请好友拼团海报相关的数据", httpMethod="POST")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "addNumber", paramType = "query", value = "要参加的团编号", required = true, dataType = "String"),
	})
	@PostMapping("/getInviteJoinGroupPosterData")
	public ResultDto<AppMergeGroupPosterDataDto> getInviteJoinGroupPosterData(@RequestParam(required = true) String addNumber){
		
		try {
			
			/** 获取小程序码 */
			MergeGroupOperate mergeGroupOperate = mergeGroupOperateService.getMergeGroupOperateByAddNumber(addNumber);
			if(null == mergeGroupOperate){
				return ResultDtoManager.fail("团购活动不存在或已下线!");
			}
			
			String wxACode = mergeGroupOperate.getWxACode();
			if(AppUtils.isBlank(wxACode)){
				
				String  accessToken = weixinMpTokenService.getAccessToken();
				String scene = "sn=" + addNumber;
				String page = "marketingModules/fight/fightGroupDetail";
				WXACodeResultDto wxACodeResult = WeixinMiniProgramApi.getWXACodeUnlimit(accessToken, scene, page, null, null, null, null);
				if(!wxACodeResult.isSuccess()){//说明失败
					
					WXACodeResultDto.Errormsg error = wxACodeResult.getErrormsg();
					if(null == error){
						return ResultDtoManager.fail("获取二维码错误, 未知错误!");
					}
					
					return ResultDtoManager.fail(error.getErrmsg());
				}
					
				//说明成功返回二维码图片
				// TODO 这里不应该这么去实现, 会太多图片上传到图片服务器
				ByteArrayOutputStream output = wxACodeResult.getOutputStream(); //ByteArrayOutputStream 无需close,因为这个方法是空的,参见ByteArrayOutputStream的代码
				FeignMultipartFile file = new FeignMultipartFile("WX_A_CODE_MERGEGROUP_GROUP_"+addNumber+".jpg",output.toByteArray());
				wxACode = attachmentManager.upload(file);
				
				if(AppUtils.isBlank(wxACode)){
					return ResultDtoManager.fail("获取二维码错误, 未知错误!");
				}
				
				/** 更新到团购活动 */
				mergeGroupOperate.setWxACode(wxACode);
				mergeGroupOperateService.saveMergeGroupOperate(mergeGroupOperate);
			}
			AppMergeGroupPosterDataDto mergeGroupPosterData = new AppMergeGroupPosterDataDto();
			
			LoginedUserInfo appToken = loginedUserService.getUser();
			if(null != appToken){
				/** 获取用户头像,昵称 */
				UserDetail userDetail = userDetailService.getUserDetailById(appToken.getUserId());
				
				mergeGroupPosterData.setHeadPortrait(userDetail.getPortraitPic());
				mergeGroupPosterData.setNickName(userDetail.getNickName());
			}
			
			mergeGroupPosterData.setWxACode(wxACode);
			
			return ResultDtoManager.success(mergeGroupPosterData);
			
		} catch (Exception e) {
			LOGGER.error("获取生成商品海报相关的数据错误: ", e);
			return ResultDtoManager.fail("未知错误!");
		}
	}
}
