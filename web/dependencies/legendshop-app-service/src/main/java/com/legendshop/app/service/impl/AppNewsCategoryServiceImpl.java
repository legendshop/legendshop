package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.AppNewsCategoryDto;
import com.legendshop.app.service.AppNewsCategoryService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.NewsCategory;
import com.legendshop.spi.service.NewsCategoryService;
import com.legendshop.util.AppUtils;

/**
 * app 新闻栏目service实现
 */
@Service("appNewsCategoryService")
public class AppNewsCategoryServiceImpl implements AppNewsCategoryService {

	@Autowired
	private NewsCategoryService newsCategoryService;

	@Override
	public AppPageSupport<AppNewsCategoryDto> getNewsCategoryListPage(String curPageNO) {
		
		PageSupport<NewsCategory> ps = newsCategoryService.getNewsCategoryListPage(curPageNO);
		
		//转Dto
		AppPageSupport<AppNewsCategoryDto> pageSupportDto =
		PageUtils.convertPageSupport(ps, new PageUtils.ResultListConvertor<NewsCategory, AppNewsCategoryDto>() {

			@Override
			public List<AppNewsCategoryDto> convert(List<NewsCategory> form) {

				if (AppUtils.isBlank(form)) {
					return null;
				}
				List<AppNewsCategoryDto> toList = new ArrayList<AppNewsCategoryDto>();
				
				for (NewsCategory newsCategory : form) {
					AppNewsCategoryDto appNewsCategoryDto = convertToAppNewsCategoryDto(newsCategory);
					toList.add(appNewsCategoryDto);
				}
				return toList;
			}
		});
		return pageSupportDto;
	}
	
	private AppNewsCategoryDto convertToAppNewsCategoryDto(NewsCategory newsCategory){
		  AppNewsCategoryDto appNewsCategoryDto = new AppNewsCategoryDto();
		  appNewsCategoryDto.setNewsCategoryDate(newsCategory.getNewsCategoryDate());
		  appNewsCategoryDto.setPic(newsCategory.getPic());
		  appNewsCategoryDto.setNewsCategoryId(newsCategory.getNewsCategoryId());
		  appNewsCategoryDto.setUserName(newsCategory.getUserName());
		  appNewsCategoryDto.setUserId(newsCategory.getUserId());
		  appNewsCategoryDto.setSeq(newsCategory.getSeq());
		  appNewsCategoryDto.setStatus(newsCategory.getStatus());
		  appNewsCategoryDto.setNewsCategoryName(newsCategory.getNewsCategoryName());
		  return appNewsCategoryDto;
		}
}
