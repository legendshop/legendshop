package com.legendshop.app.dto;

import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("种草社区作者详情Dto")
public class AppGrassWriterInfoDto {
	
	@ApiModelProperty(value="作者id")
	private String userId;
	
	@ApiModelProperty(value="作者名")
	private String userName;

	@ApiModelProperty(value="作者头像")
	private String userImage;

	@ApiModelProperty(value="个性签名")
	private String userSignature;
	
	@ApiModelProperty(value="作者被赞数")
	private Long thumbCount;
	
	@ApiModelProperty(value="作者粉丝数")
	private Long concernCount;
	
	@ApiModelProperty(value="作者关注数")
	private Long pursuerCount;
	
	@ApiModelProperty(value="是否关注")
	private boolean rsConcern;

	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getUserImage() {
		return userImage;
	}


	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}


	public Long getThumbCount() {
		return thumbCount;
	}


	public void setThumbCount(Long thumbCount) {
		this.thumbCount = thumbCount;
	}


	public Long getConcernCount() {
		return concernCount;
	}


	public void setConcernCount(Long concernCount) {
		this.concernCount = concernCount;
	}


	public Long getPursuerCount() {
		return pursuerCount;
	}


	public void setPursuerCount(Long pursuerCount) {
		this.pursuerCount = pursuerCount;
	}

	public boolean isRsConcern() {
		return rsConcern;
	}


	public void setRsConcern(boolean rsConcern) {
		this.rsConcern = rsConcern;
	}

	public String getUserSignature() {
		return userSignature;
	}

	public void setUserSignature(String userSignature) {
		this.userSignature = userSignature;
	}
}
