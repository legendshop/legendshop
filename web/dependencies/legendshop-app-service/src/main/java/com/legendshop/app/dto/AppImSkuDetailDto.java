package com.legendshop.app.dto;

import com.legendshop.model.entity.ProductDetail;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;


/**
 * 用于appIM系统会话展示信息对象
 */
@ApiModel(value="用户咨询的商品规格信息, 当consultType=prod时有")
public class AppImSkuDetailDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3654397100114605649L;

	/** skuId */
	@ApiModelProperty(value="skuId")
	private Long skuId;
	
	/** 商品Id */
	@ApiModelProperty(value="商品Id")
	private Long prodId;
	
	/** skuId */
	@ApiModelProperty(value="sku价格")
	private Double skuPrice;
	
	/** 销售属性组合（中文） */
	@ApiModelProperty(value="销售属性组合（中文）")
	private String cnProperties;
	
	/** sku商家编码 */
	@ApiModelProperty(value="sku商家编码")
	private String skuPartyCode;
	
	/** sku图片 */
	@ApiModelProperty(value="sku图片")
	private String skuPic;
	
	/** sku名称 */
	@ApiModelProperty(value="sku名称")
	private String skuName;
	
	/** 商品图片 */
	@ApiModelProperty(value="商品图片")
	private String prodPic;
	
	/** 商品名称 */
	@ApiModelProperty(value="商品名称")
	private String prodName;
	
	/** 商家ID */
	@ApiModelProperty(value="商家ID")
	private Long shopId;
	
	/** 商品商家编码 */
	@ApiModelProperty(value="商品商家编码")
	private String prodPartyCode;
	
	/** 浏览商品记录 */
	@ApiModelProperty(value="浏览商品记录")
	List<ProductDetail> products;

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}
	
	public Double getSkuPrice() {
		return skuPrice;
	}

	public void setSkuPrice(Double skuPrice) {
		this.skuPrice = skuPrice;
	}

	public String getCnProperties() {
		return cnProperties;
	}

	public void setCnProperties(String cnProperties) {
		this.cnProperties = cnProperties;
	}

	public String getSkuPartyCode() {
		return skuPartyCode;
	}

	public void setSkuPartyCode(String skuPartyCode) {
		this.skuPartyCode = skuPartyCode;
	}

	public String getSkuPic() {
		return skuPic;
	}

	public void setSkuPic(String skuPic) {
		this.skuPic = skuPic;
	}

	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

	public String getProdPic() {
		return prodPic;
	}

	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getProdPartyCode() {
		return prodPartyCode;
	}

	public void setProdPartyCode(String prodPartyCode) {
		this.prodPartyCode = prodPartyCode;
	}

	public List<ProductDetail> getProducts() {
		return products;
	}

	public void setProducts(List<ProductDetail> products) {
		this.products = products;
	}

}
