package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 移動APP支付結果DTO
 */
@ApiModel(value="支付返回结果") 
public class AppPaymentResultDto {

	@ApiModelProperty(value="支付信息")  
	private PayInfo payInfo;
	
	@ApiModelProperty(value="预支付结果, 前端通过预支付结果调起第三方支付收银台完成支付")  
	private String prepayResult;

	public PayInfo getPayInfo() {
		return payInfo;
	}

	public void setPayInfo(PayInfo payInfo) {
		this.payInfo = payInfo;
	}

	public String getPrepayResult() {
		return prepayResult;
	}

	public void setPrepayResult(String prepayResult) {
		this.prepayResult = prepayResult;
	}
	
	@ApiModel(value="支付信息") 
	public static class PayInfo{
		
		@ApiModelProperty(value="交易单据号, 一次支付一个单据号, 与订单号是一对多的关系.")  
		private String subSettlementSn;
		
		@ApiModelProperty(value="交易单据类型, USER_ORDER: 订单支付, USER_PRED_RECHARGE: 余额充值, AUCTION_DEPOSIT: 拍卖保证金支付, PRESELL_ORDER_PAY: 预售支付")  
		private String subSettlementType;
		
		@ApiModelProperty(value="支付类型ID")  
		private String payTypeId;
		
		@ApiModelProperty(value="支付金额")  
		private Double totalAmount;
		
		@ApiModelProperty("订单类型, 如: MERGE_GROUP 代表拼团订单, 订单支付成功后可以根据这个字段来跳拼团详情页面; 为空或其他值时暂不需特殊处理")
		private String subType;
		
		@ApiModelProperty("订单号, 用于跳转团详情页面")
		private String subNumber;

		public String getSubSettlementSn() {
			return subSettlementSn;
		}

		public void setSubSettlementSn(String subSettlementSn) {
			this.subSettlementSn = subSettlementSn;
		}

		public String getSubSettlementType() {
			return subSettlementType;
		}

		public void setSubSettlementType(String subSettlementType) {
			this.subSettlementType = subSettlementType;
		}

		public String getPayTypeId() {
			return payTypeId;
		}

		public void setPayTypeId(String payTypeId) {
			this.payTypeId = payTypeId;
		}

		public Double getTotalAmount() {
			return totalAmount;
		}

		public void setTotalAmount(Double totalAmount) {
			this.totalAmount = totalAmount;
		}

		public String getSubType() {
			return subType;
		}

		public void setSubType(String subType) {
			this.subType = subType;
		}

		public String getSubNumber() {
			return subNumber;
		}

		public void setSubNumber(String subNumber) {
			this.subNumber = subNumber;
		}
	}
}
