package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *app积分商品详情Dto
 */
@ApiModel(value="积分商品Dto") 
public class AppIntegralProdDto {

	/** 主键id */
	@ApiModelProperty(value="主键id") 
	private Long id;

	/** 积分商品名称 */
	@ApiModelProperty(value="积分商品名称") 
	private String name;

	/** 市场价格 */
	@ApiModelProperty(value="商品价格") 
	private Double price;

	/** 兑换积分 */
	@ApiModelProperty(value="兑换积分") 
	private Integer exchangeIntegral;

	/** 商品库存 */
	@ApiModelProperty(value="商品库存") 
	private Long prodStock;

	/** 商品图片 */
	@ApiModelProperty(value="商品图片") 
	private String prodImage;

	/** 是否推荐商品 */
	@ApiModelProperty(value="是否推荐商品")  
	private Integer isCommend;

	/** 销售数量 */
	@ApiModelProperty(value="销售数量")  
	private Integer saleNum;

	/** 是否限制购买数量 */
	@ApiModelProperty(value="是否限制购买数量")  
	private Integer isLimit;

	/** 限制数量 */
	@ApiModelProperty(value="限制数量")  
	private Integer limitNum;

	/** 商品介绍 */
	@ApiModelProperty(value="商品描述")  
	private String prodDesc;
	
	public AppIntegralProdDto() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getExchangeIntegral() {
		return exchangeIntegral;
	}

	public void setExchangeIntegral(Integer exchangeIntegral) {
		this.exchangeIntegral = exchangeIntegral;
	}

	public Long getProdStock() {
		return prodStock;
	}

	public void setProdStock(Long prodStock) {
		this.prodStock = prodStock;
	}

	public String getProdImage() {
		return prodImage;
	}

	public void setProdImage(String prodImage) {
		this.prodImage = prodImage;
	}

	public Integer getIsCommend() {
		return isCommend;
	}

	public void setIsCommend(Integer isCommend) {
		this.isCommend = isCommend;
	}

	public Integer getSaleNum() {
		return saleNum;
	}

	public void setSaleNum(Integer saleNum) {
		this.saleNum = saleNum;
	}

	public Integer getIsLimit() {
		return isLimit;
	}

	public void setIsLimit(Integer isLimit) {
		this.isLimit = isLimit;
	}

	public Integer getLimitNum() {
		return limitNum;
	}

	public void setLimitNum(Integer limitNum) {
		this.limitNum = limitNum;
	}

	public String getProdDesc() {
		return prodDesc;
	}

	public void setProdDesc(String prodDesc) {
		this.prodDesc = prodDesc;
	}

}
