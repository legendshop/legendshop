/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.util;

import com.legendshop.util.AppUtils;
import org.apache.commons.codec.binary.Base64;

import java.io.IOException;

/**
 * 处理文件格式.
 */
public class FileUtil {
	
	public static byte[] decodeBase64(String base64) throws IOException{
		byte[] file = null;
		if (AppUtils.isNotBlank(base64)) {
			file = Base64.decodeBase64(base64);
			if(AppUtils.isNotBlank(base64)){
				for (int i = 0; i < file.length; ++i) {
					if (file[i] < 0) {
						// 调整异常数据
						file[i] += 256;
					}
				}
			}
		}
		return file;
	}

}
