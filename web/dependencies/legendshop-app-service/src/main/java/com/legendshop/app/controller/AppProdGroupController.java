package com.legendshop.app.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.AppProdDto;
import com.legendshop.app.service.AppProductService;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.appdecorate.ProdGroupConditionalDto;
import com.legendshop.model.entity.ProdGroup;
import com.legendshop.spi.service.ProdGroupService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 商品分组相关接口
 *
 */
@Api(tags="商品分组",value="商品分组相关操作")
@RestController
public class AppProdGroupController {
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(AppProdGroupController.class);

	@Autowired
	private ProdGroupService prodGroupService;
	
	@Autowired
	private AppProductService appProductService;
	
	/**
	 * 获取所有分类
	 * @param request
	 * @param response
	 * @return
	 */
    @ApiOperation(value = "获取分组内的商品", httpMethod = "POST", notes = "获取分组内的商品",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
    	 @ApiImplicitParam(paramType="query", name = "prodGroupId", value = "商品分组ID", dataType = "Long",required = true),
    	 @ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", dataType = "Integer")
    })
    @ApiImplicitParam(paramType="query", name = "prodGroupId", value = "商品分组ID", dataType = "Long",required = true)
	@PostMapping("/groupProdList")
	public ResultDto<AppPageSupport<AppProdDto>> groupProdList(Long prodGroupId,Integer curPageNO) {
		try {
	    	
			ProdGroup prodGroup = prodGroupService.getProdGroup(prodGroupId);
			if (AppUtils.isBlank(prodGroup)) {
				return ResultDtoManager.fail(-1, "该分组不存在或已被删除");
			}
			
			AppPageSupport<AppProdDto> prodList = null;
			
			if (prodGroup.getType() == 0) { //系统定义
				
				//获取系统定义分组的排序条件
				String conditional = prodGroup.getConditional();
				ProdGroupConditionalDto prodGroupConditionalDto = JSONUtil.getObject(conditional, ProdGroupConditionalDto.class);
				String groupConditional = prodGroupConditionalDto.getType();
				
				prodList = appProductService.queryProductByConditional(groupConditional,curPageNO);
				return ResultDtoManager.success(prodList);
				
			}
			//获取自定义分组内的排序
			String sort = prodGroup.getSort();
			
			prodList = appProductService.queryProductByProdGroupId(prodGroup.getId(),sort,curPageNO);
			return ResultDtoManager.success(prodList);
			
		} catch (Exception e) {
			log.error("获取商品分组异常",e);
		}
		return ResultDtoManager.fail();
	}
	

	



	
}
