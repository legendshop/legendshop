package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 系统最新通知DTO
 */
@ApiModel(value="系统最新通知") 
public class AppNewSystemMsgDto {
	
	@ApiModelProperty(value="最新的一条系统通知数据, 如果没有则为null")
	private AppSiteInformationDto msg;
	
	@ApiModelProperty(value="未读系统通知数")
	private Integer unreadCount = 0;

	public AppSiteInformationDto getMsg() {
		return msg;
	}

	public void setMsg(AppSiteInformationDto msg) {
		this.msg = msg;
	}

	public Integer getUnreadCount() {
		return unreadCount;
	}

	public void setUnreadCount(Integer unreadCount) {
		this.unreadCount = unreadCount;
	}
	
}
