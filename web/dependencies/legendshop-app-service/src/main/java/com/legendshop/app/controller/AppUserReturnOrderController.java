package com.legendshop.app.controller;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.entity.*;
import com.legendshop.spi.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.AppApplyRefundReturnDto;
import com.legendshop.app.dto.AppModifyRefundReturnDto;
import com.legendshop.app.dto.AppSubRefundReturnDto;
import com.legendshop.app.dto.ApplyRefundFormDto;
import com.legendshop.app.dto.ApplyRefundReturnBaseDto;
import com.legendshop.app.dto.ApplyReturnFormDto;
import com.legendshop.app.service.AppSubRefundReturnService;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.util.DateUtils;
import com.legendshop.framework.event.EventHome;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.PayMannerEnum;
import com.legendshop.model.constant.PayPctTypeEnum;
import com.legendshop.model.constant.RefundReturnStatusEnum;
import com.legendshop.model.constant.RefundReturnTypeEnum;
import com.legendshop.model.constant.RefundSouceEnum;
import com.legendshop.model.constant.SubTypeEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.order.ApplyRefundReturnDto;
import com.legendshop.model.dto.order.OrderSetMgDto;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import com.legendshop.util.JSONUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
/**
 * 退款退货相关接口
 *
 */
@RestController
@Api(tags="退款退货",value="订单单退款、订单退款退货、订单项退款退货相关接口")
public class AppUserReturnOrderController {

	private final Logger LOGGER = LoggerFactory.getLogger(AppUserReturnOrderController.class);

	@Autowired
	private SubRefundReturnService subRefundReturnService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ConstTableService constTableService;
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private AppSubRefundReturnService appSubRefundReturnService;
	
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private SubService subService;
	
	/**
	 * 申请退款（整个订单退款）
	 * @param request
	 * @param response
	 * @param orderId 订单ID
	 * @return
	 */
	@ApiOperation(value = "申请退款", httpMethod = "POST", notes = "跳转到 '整个订单申请退款'页面",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "subId", value = "订单id", required = true, dataType = "Long")
	@PostMapping(value="/p/refund/apply")
	public ResultDto<AppApplyRefundReturnDto> applyRefund(Long subId) {
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			ApplyRefundReturnDto refund = subRefundReturnService.getApplyRefundReturnDto(subId,RefundReturnTypeEnum.OP_ORDER,userId);
			
			if(null == refund){
				return ResultDtoManager.fail(-1, "对不起, 数据发生异常, 请刷新页面!");
			}
			//检查订单是否允许退款操作
			if(!isAllowRefund(refund)){//不允许
				return ResultDtoManager.fail(-1, "对不起, 该订单不允许退款操作或订单状态已发生改变，请返回");
			}
			
			//转Dto
			AppApplyRefundReturnDto refundReturnDto = convertToAppApplyRefundReturnDto(refund);
			
			return ResultDtoManager.success(refundReturnDto);
		} catch (Exception e) {
			LOGGER.error("订单退款异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	
	/**
	 * 提交申请退款 --- 当订单处于待发货状态 只能整个订单退款处理
	 */
	@ApiOperation(value = "提交退款申请", httpMethod = "POST", notes = "提交退款申请,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/p/refund/save")
	public ResultDto<String> saveOrderRefund(HttpServletRequest request,@Validated ApplyRefundFormDto form,BindingResult error) {
		
		try {

			if(error.hasErrors()){
				return ResultDtoManager.fail(-1,error.getFieldError().getDefaultMessage());
			}
			
			String userId = loginedUserService.getUser().getUserId();
			ApplyRefundReturnDto refund = subRefundReturnService.getApplyRefundReturnDto(form.getOrderId(),RefundReturnTypeEnum.OP_ORDER,userId);
			if(AppUtils.isBlank(refund)){
				return ResultDtoManager.fail(-1, "对不起,您操作的订单不存在或已被删除!");
			}
			//检查订单是否允许退款
			if(!isAllowRefund(refund)){
				return ResultDtoManager.fail(-1, "对不起,您操作的订单不允许退款!");
			}
			//给SubRefundReturn属性设值,获取已设值的SubRefundReturn对象
			SubRefundReturn subRefundReturn = setCommonProperty(form,refund,request);
			
			//订单全部退款,这些设置为0
			subRefundReturn.setSubItemId(0L);
			subRefundReturn.setProductImage(refund.getProdPic());
			subRefundReturn.setProductId(0L);
			subRefundReturn.setSkuId(0L);
			subRefundReturn.setProductName("订单商品全部退款");
			subRefundReturn.setApplyType(RefundReturnTypeEnum.REFUND.value());
			subRefundReturn.setRefundAmount(new BigDecimal(refund.getRefundAmount()));//退款金额等于订单金额

			subRefundReturnService.returnProcess(subRefundReturn.getSubId(), subRefundReturn.getSubItemId(), userId);

			subRefundReturnService.saveApplyOrderRefund(subRefundReturn);
			UserDetail shopUser = userDetailService.getUserDetailByShopId(refund.getShopId());
			EventHome.publishEvent(new SendSiteMsgEvent("系统", shopUser.getUserName(), "退款通知" , "亲，您的订单["+subRefundReturn.getSubNumber()+"],买家申请退款！"));
			
			return ResultDtoManager.success();
		} catch (Exception e) {
			LOGGER.error("提交退款申请异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	
	/**
	 * 订单项申请退款
	 */
	@ApiOperation(value = "订单项申请退款", httpMethod = "POST", notes = "跳转到 '订单项申请退款' 页面",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "orderId", value = "订单id", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="query", name = "orderItemId", value = "订单项id", required = true, dataType = "Long")
	})
	@PostMapping(value="/p/itemRefund/apply")
	public ResultDto<AppApplyRefundReturnDto> applyRefund(Long orderId,Long orderItemId) {
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			ApplyRefundReturnDto refund = subRefundReturnService.getApplyRefundReturnDto(orderItemId,RefundReturnTypeEnum.OP_ORDER_ITEM,userId);
			
			if(null == refund){
				return ResultDtoManager.fail(-1, "对不起,您操作的订单不存在或已被删除!");
			}
			
			//检查订单是否允许退款退货操作
			refund.setApplyType(RefundReturnTypeEnum.REFUND.value().longValue());
			
			if(!isAllowItemReturn(refund)){//不允许
				return ResultDtoManager.fail(-1, "该订单不允许退款操作");
			}
			if(OrderStatusEnum.SUCCESS.value().equals(refund.getOrderStatus()) && !isInReturnValidPeriod(refund)){
				return ResultDtoManager.fail(-1, "该订单退换货时间已到期");
			}
			
			if(SubTypeEnum.PRESELL.value().equals(refund.getSubType()) && refund.getPayPctType() == PayPctTypeEnum.FRONT_MONEY.value()) {//预售订金支付
				Double shouldRefund = Arith.sub(refund.getRefundAmount(), refund.getDepositRefundAmount());
				refund.setRefundAmount(shouldRefund);
			}else {
				//已退金额
				Double subRefundAmount = calculateAlreadyRefundAmount(refund.getSubSettlementSn());

				Double shouldRefund = Arith.sub(refund.getSubMoney(), subRefundAmount);
				//检查可以退款的金额,可退金额不能超过应退金额
				if(shouldRefund < refund.getRefundAmount()){
					refund.setRefundAmount(shouldRefund);
				}
			}
			//转Dto
			AppApplyRefundReturnDto refundReturnDto = convertToAppApplyRefundReturnDto(refund);
			
			return ResultDtoManager.success(refundReturnDto);
		} catch (Exception e) {
			LOGGER.error("订单项申请退款异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	
	/**
	 * 提交订单项退款申请
	 */
	@ApiOperation(value = "提交订单项退款申请", httpMethod = "POST", notes = "提交订单项退款申请,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/p/itemRefund/save")
	public ResultDto<String> saveItemRefund(HttpServletRequest request,@Validated ApplyRefundFormDto form,BindingResult error) {
		
		try {
			if(error.hasErrors()){
				return ResultDtoManager.fail(-1,error.getFieldError().getDefaultMessage());
			}
			
			String userId = loginedUserService.getUser().getUserId();
			ApplyRefundReturnDto refund = subRefundReturnService.getApplyRefundReturnDto(form.getOrderItemId(),RefundReturnTypeEnum.OP_ORDER_ITEM,userId);
			
			if(AppUtils.isBlank(refund)){
				return ResultDtoManager.fail(-1,"对不起,您操作的订单不存在或已被删除!");
			}
			Double shouldRefund =refund.getRefundAmount();
			
			if(SubTypeEnum.PRESELL.value().equals(refund.getSubType()) && refund.getPayPctType() == PayPctTypeEnum.FRONT_MONEY.value()) {//预售订金支付
				shouldRefund = Arith.sub(refund.getRefundAmount(), refund.getDepositRefundAmount());
			}
			
			//校验退款金额
			if(form.getRefundAmount().doubleValue() <= 0L 
					|| form.getRefundAmount().doubleValue() > shouldRefund){
				return ResultDtoManager.fail(-1,"对不起, 退款金额必须要大于0小于或等于" + refund.getRefundAmount() + "!");
			}
			
			//检查订单项是否允许退款
			refund.setApplyType(RefundReturnTypeEnum.REFUND.value().longValue());
			if(!isAllowItemReturn(refund)){
				return ResultDtoManager.fail(-1,"对不起,您操作的订单不允许退款!");
			}
			
			SubRefundReturn subRefundReturn = new SubRefundReturn();
			
			if(SubTypeEnum.PRESELL.value().equals(refund.getSubType())) { //预售订单
				//给SubRefundReturn属性设值
				 subRefundReturn = setPresellCommonProperty(form,refund,request);//设置各种申请的公共属性
			} else {//其他订单
				//给SubRefundReturn属性设值
				 subRefundReturn = setCommonProperty(form,refund,request);//设置各种申请的公共属性
			}
			
			subRefundReturn.setSubItemId(form.getOrderItemId());
			subRefundReturn.setProductId(refund.getProdId());
			subRefundReturn.setSkuId(refund.getSkuId());
			subRefundReturn.setProductName(refund.getProdName());
			subRefundReturn.setProductImage(refund.getProdPic());
			subRefundReturn.setProductNum(refund.getBasketCount());
			subRefundReturn.setProductAttribute(refund.getAttribute());
			subRefundReturn.setRefundAmount(new BigDecimal(form.getRefundAmount()));
			
			if(SubTypeEnum.PRESELL.value().equals(refund.getSubType())) {
				subRefundReturn.setIsRefundDeposit(form.getIsRefundDeposit());
				subRefundReturn.setDepositRefund(new BigDecimal(form.getDepositRefundAmount()));
			}
			
			subRefundReturn.setGoodsNum(Long.valueOf(refund.getGoodsCount()));
			subRefundReturn.setApplyType(RefundReturnTypeEnum.REFUND.value());

			subRefundReturnService.returnProcess(subRefundReturn.getSubId(), subRefundReturn.getSubItemId(), userId);
			
			subRefundReturnService.saveApplyItemRefund(subRefundReturn);
			UserDetail shopUser = userDetailService.getUserDetailByShopId(refund.getShopId());
			EventHome.publishEvent(new SendSiteMsgEvent("系统", shopUser.getUserName(), "退款通知" , "亲，您的订单["+subRefundReturn.getSubNumber()+"],买家申请退款！"));
			
			return ResultDtoManager.success();
		} catch (Exception e) {
			LOGGER.error("提交订单项退款申请异常!", e);
			return ResultDtoManager.fail();
		}
	}

	/**
	 * 申请退货退款
	 */
	@ApiOperation(value = "申请退货退款", httpMethod = "POST", notes = "跳转到 '退货退款'页面",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "orderId", value = "订单id", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="query", name = "orderItemId", value = "订单项id", required = true, dataType = "Long")
	})
	@PostMapping(value="/p/return/apply")
	public ResultDto<AppApplyRefundReturnDto> applyReturn(Long orderId,Long orderItemId) {
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			ApplyRefundReturnDto refund = 
					subRefundReturnService.getApplyRefundReturnDto(orderItemId,RefundReturnTypeEnum.OP_ORDER_ITEM,userId);
			if(null == refund){
				return ResultDtoManager.fail(-1, "对不起,您操作的订单不存在或已被删除!");
			}

			//检查订单是否允许退款退货操作
			refund.setApplyType(RefundReturnTypeEnum.REFUND_RETURN.value().longValue());
			if(!isAllowItemReturn(refund)){//不允许
				return ResultDtoManager.fail(-1, "该订单不允许退货操作");
			}
			if(OrderStatusEnum.SUCCESS.value().equals(refund.getOrderStatus()) && !isInReturnValidPeriod(refund)){
				return ResultDtoManager.fail(-1, "该订单退换货时间已到期");
			}
			
			if(SubTypeEnum.PRESELL.value().equals(refund.getSubType()) && refund.getPayPctType() == PayPctTypeEnum.FRONT_MONEY.value()) {//预售订金支付
				Double shouldRefund = Arith.sub(refund.getRefundAmount(), refund.getDepositRefundAmount());
				refund.setRefundAmount(shouldRefund);
			}else {
				//已退金额
				Double subRefundAmount = calculateAlreadyRefundAmount(refund.getSubSettlementSn());

				Double shouldRefund = Arith.sub(refund.getSubMoney(), subRefundAmount);
				//检查可以退款的金额,可退金额不能超过应退金额
				if(shouldRefund < refund.getRefundAmount()){
					refund.setRefundAmount(shouldRefund);
				}
			}
			
			//转Dto
			AppApplyRefundReturnDto refundReturnDto = convertToAppApplyRefundReturnDto(refund);
			
			return ResultDtoManager.success(refundReturnDto);
		} catch (Exception e) {
			LOGGER.error("申请退货退款异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	
	/**
	 * 提交退货退款申请
	 */
	@ApiOperation(value = "提交退货退款申请", httpMethod = "POST", notes = "提交退货退款申请，返回'OK'表示成功，返回'fail'表示失败 （注：至少要上传一张退货凭证）",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/p/itemReturn/save")
	public ResultDto<String> saveReturnItem(HttpServletRequest request,@Validated ApplyReturnFormDto form, BindingResult error){
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			ApplyRefundReturnDto refund = subRefundReturnService.getApplyRefundReturnDto(form.getOrderItemId(),RefundReturnTypeEnum.OP_ORDER_ITEM,userId);

			//异常判断
			if(error.hasErrors()){
				return ResultDtoManager.fail(-1, error.getFieldError().getDefaultMessage());
			}
			if (AppUtils.isBlank(form.getPhoto1())&&AppUtils.isBlank(form.getPhoto2())&&AppUtils.isBlank(form.getPhoto3())) {
				return ResultDtoManager.fail(-1, "至少上传一张凭证图片");
			}
			if(AppUtils.isBlank(refund)){
				return ResultDtoManager.fail(-1, "对不起,您操作的的订单不存在或已被删除!");
			}


			Double shouldRefund = 0d;
			if(SubTypeEnum.PRESELL.value().equals(refund.getSubType()) 
					&& refund.getPayPctType() == PayPctTypeEnum.FRONT_MONEY.value()) { //预售订金支付
				shouldRefund = Arith.sub(refund.getRefundAmount(), refund.getDepositRefundAmount());
			}else {
				//已退金额
				Double subRefundAmount = calculateAlreadyRefundAmount(refund.getSubSettlementSn());
				//检查可以退款的金额,可退金额不能超过应退金额			
				shouldRefund = Arith.sub(refund.getSubMoney(), subRefundAmount);
			}
			
			//检查订单是否允许退款退货
			refund.setApplyType(RefundReturnTypeEnum.REFUND_RETURN.value().longValue());
			if(!isAllowItemReturn(refund)){
				return ResultDtoManager.fail(-1, "对不起,您操作的订单不允许退款!");
			}
			
			//校验退款金额
			if(form.getRefundAmount().doubleValue() <= 0L 
					|| form.getRefundAmount().doubleValue() > shouldRefund){
				return ResultDtoManager.fail(-1, "对不起, 退款金额必须要大于0小于或等于" + refund.getRefundAmount() + "!");
			}
			
			//校验退货数量
			if(form.getGoodsNum().intValue() <=0 || form.getGoodsNum() >refund.getGoodsCount()){
				return ResultDtoManager.fail(-1, "对不起, 退货数量不能小于0或大于" + refund.getGoodsCount() +"!");
			}
			
			SubRefundReturn subRefundReturn = new SubRefundReturn();
			
			if(SubTypeEnum.PRESELL.value().equals(refund.getSubType())) { //预售订单
				//给SubRefundReturn属性设值
				 subRefundReturn = setPresellCommonProperty(form,refund,request);//设置各种申请的公共属性
			} else {//其他订单
				//给SubRefundReturn属性设值
				 subRefundReturn = setCommonProperty(form,refund,request);//设置各种申请的公共属性
			}		
			
			subRefundReturn.setSubItemId(form.getOrderItemId());
			subRefundReturn.setProductId(refund.getProdId());
			subRefundReturn.setSkuId(refund.getSkuId());
			subRefundReturn.setProductName(refund.getProdName());
			subRefundReturn.setProductImage(refund.getProdPic());
			subRefundReturn.setProductNum(refund.getBasketCount());
			subRefundReturn.setProductAttribute(refund.getAttribute());
			subRefundReturn.setRefundAmount(new BigDecimal(form.getRefundAmount()));
			
			if(SubTypeEnum.PRESELL.value().equals(refund.getSubType())) {
				subRefundReturn.setIsRefundDeposit(form.getIsRefundDeposit());
				subRefundReturn.setDepositRefund(new BigDecimal(form.getDepositRefundAmount()));
			}
			
			subRefundReturn.setGoodsNum(form.getGoodsNum());
			subRefundReturn.setApplyType(RefundReturnTypeEnum.REFUND_RETURN.value());

			subRefundReturnService.returnProcess(subRefundReturn.getSubId(), subRefundReturn.getSubItemId(), userId);
			subRefundReturnService.saveApplyItemReturn(subRefundReturn);

			UserDetail shopUser = userDetailService.getUserDetailByShopId(refund.getShopId());
			EventHome.publishEvent(new SendSiteMsgEvent("系统", shopUser.getUserName(), "退款通知" , "亲，您的订单["+subRefundReturn.getSubNumber()+"],买家申请退款且退货！"));
			
			return ResultDtoManager.success();
		} catch (Exception e) {
			LOGGER.error("提交退货退款申请异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 撤销申请
	 */
	@ApiOperation(value = "撤销申请", httpMethod = "POST", notes = "撤销申请(退款，退货退款共用)，返回'OK'表示成功，返回'fail'表示失败 ",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "refundSn", value = "退款单号", required = true, dataType = "string")
	@PostMapping(value="/p/refund/candelAction")
	public ResultDto<String> candelAction(String refundSn) {
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturnByRefundSn(refundSn);
			if (null == subRefundReturn) {
				return ResultDtoManager.fail(-1, "对不起,您操作的记录不存在或已被删除!");
			}
			if (!userId.equals(subRefundReturn.getUserId())) {
				return ResultDtoManager.fail(-1, "请不要非法操作!");
			}
			// 仅在卖家未处理该申请的时候才允许撤销操作
			if (RefundReturnStatusEnum.SELLER_WAIT_AUDIT.value() != subRefundReturn.getSellerState()) {
				return ResultDtoManager.fail(-1, "该订单已在审核流程，无法撤销，请联系商家");
			}
			subRefundReturnService.undoApplySubRefundReturn(subRefundReturn);
			return ResultDtoManager.success();
		} catch (Exception e) {
			LOGGER.error("撤销申请异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 修改申请 TODO
	 */
	@ApiOperation(value = "修改申请", httpMethod = "POST", notes = "修改申请(退款、退货退款被拒绝后显示)，返回'OK'表示成功，返回'fail'表示失败 ",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "refundId", value = "退款记录id", required = true, dataType = "Long")
	@PostMapping(value="/p/refund/editAction")
	public ResultDto<AppModifyRefundReturnDto> editAction(Long refundId) {
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			AppSubRefundReturnDto refund = appSubRefundReturnService.getSubRefundReturn(refundId);
			
			if (null == refund) {
				return ResultDtoManager.fail(-1, "对不起,您操作的记录不存在或已被删除!");
			}
			if (!userId.equals(refund.getUserId())) {
				return ResultDtoManager.fail(-1, "请不要非法操作!");
			}
			// 仅在卖家拒绝该申请的时候才允许修改操作
			if (RefundReturnStatusEnum.SELLER_DISAGREE.value() != refund.getSellerState()) {
				return ResultDtoManager.fail(-1, "该订单已在审核流程，无法撤销，请联系商家");
			}
			
			//构建返回参数
			AppModifyRefundReturnDto modifyDto = new AppModifyRefundReturnDto();
			modifyDto.setApplyType(refund.getApplyType());
			modifyDto.setRefundId(refund.getRefundId());
			modifyDto.setProdId(refund.getProductId());
			modifyDto.setProdName(refund.getProductName());
			modifyDto.setProdPic(refund.getProductImage());
			modifyDto.setAttribute(refund.getProductAttribute());
			modifyDto.setBasketCount(refund.getProductNum());
			modifyDto.setSubMoney(refund.getSubMoney().doubleValue());
			modifyDto.setOrderItemId(refund.getSubItemId());
			modifyDto.setRefundAmount(refund.getRefundAmount().doubleValue());
			if (AppUtils.isNotBlank(refund.getGoodsNum())){
				modifyDto.setGoodsCount(refund.getGoodsNum().intValue());
			}
			modifyDto.setBuyerMessage(refund.getBuyerMessage());
			modifyDto.setReasonInfo(refund.getReasonInfo());
			modifyDto.setPhotoFile1(refund.getPhotoFile1());
			modifyDto.setPhotoFile2(refund.getPhotoFile2());
			modifyDto.setPhotoFile3(refund.getPhotoFile3());
			modifyDto.setSubNumber(refund.getSubNumber());
			
			return ResultDtoManager.success(modifyDto);
		} catch (Exception e) {
			LOGGER.error("获取修改申请数据异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	
	@ApiOperation(value = "保存修改申请", notes = "保存修改申请", httpMethod = "POST")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "refundId", paramType = "query", value = "退款单Id", required = true, dataType = "Long"),
		@ApiImplicitParam(name = "refundAmount", paramType = "query", value = "退款金额", required = true, dataType = "BigDecimal"),
		@ApiImplicitParam(name = "reasonInfo", paramType = "query", value = "退款原因", required = true, dataType = "String"),
		@ApiImplicitParam(name = "buyerMessage", paramType = "query", value = "退款说明", required = true, dataType = "String"),
		@ApiImplicitParam(name = "photoFile1", paramType = "query", value = "退款凭证1", required = false, dataType = "String"),
		@ApiImplicitParam(name = "photoFile2", paramType = "query", value = "退款凭证2", required = false, dataType = "String"),
		@ApiImplicitParam(name = "photoFile3", paramType = "query", value = "退款凭证3", required = false, dataType = "String")
	})
	@PostMapping(value="/p/refund/submitAgainApply")
	public ResultDto<String> submitAgainApply(Long refundId,BigDecimal refundAmount,String buyerMessage,String reasonInfo,String photoFile1,String photoFile2,String photoFile3) {
		try{
			
			LoginedUserInfo appToken = loginedUserService.getUser();
			if (appToken == null) {// 用户还没有登录
				return ResultDtoManager.fail(-1, "您还未登录, 请先登录用户!");
			}
			SubRefundReturn refund = subRefundReturnService.getSubRefundReturn(refundId);
			if(AppUtils.isNotBlank(refund) && refund.getApplyState() == RefundReturnStatusEnum.SELLER_DISAGREE.value()){
				refund.setBuyerMessage(buyerMessage);
				refund.setRefundAmount(refundAmount);
				refund.setReasonInfo(reasonInfo);
				refund.setPhotoFile1(photoFile1);
				refund.setPhotoFile2(photoFile2);
				refund.setPhotoFile3(photoFile3);
				subRefundReturnService.submitAgainApplySubRefundReturn(refund);
				return ResultDtoManager.success();
			}
			return ResultDtoManager.fail(-1,"操作失败，请检查退款单状态");
		} catch (Exception e) {
			LOGGER.error("系统异常, 再次提交申请失败~", e);
			return ResultDtoManager.fail();
		}
	}
	
	
	/**
	 * 用户退货给商家
	 * @param refundId 退货记录ID
	 * @param expressName 物流公司
	 * @param expressNo 物流单号
	 * @return
	 */
	@ApiOperation(value = "用户退货给商家", httpMethod = "POST", notes = "用户退货给商家,返回'OK'表示成功，返回'fail'表示失败 ",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "refundId", value = "退货记录ID", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="query", name = "expressName", value = "物流公司", required = true, dataType = "String"),
		@ApiImplicitParam(paramType="query", name = "expressNo", value = " 物流单号", required = true, dataType = "string")
		})
	@PostMapping(value="/p/returnGoods")
	public ResultDto<String> returnGoods(Long refundId,String expressName,String expressNo) {
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			if(null == refundId || AppUtils.isBlank(expressName) || AppUtils.isBlank(expressNo)){
				return ResultDtoManager.fail(-1, "对不起,请输入物流信息再提交!");
				
			}
			SubRefundReturn subRefundReturn = subRefundReturnService.getSubRefundReturn(refundId);
			if(null == subRefundReturn){
				return ResultDtoManager.fail(-1, "对不起,您操作的退货记录不存在或已被删除!");
			}
			
			if (!userId.equals(subRefundReturn.getUserId())) {
				return ResultDtoManager.fail(-1, "请不要非法操作!");
			}
			
			if(RefundReturnStatusEnum.SELLER_AGREE.value().longValue() != subRefundReturn.getSellerState()
					|| RefundReturnTypeEnum.NEED_GOODS.value().longValue() != subRefundReturn.getReturnType()
					|| RefundReturnStatusEnum.LOGISTICS_WAIT_DELIVER.value().longValue() != subRefundReturn.getGoodsState()){
				return ResultDtoManager.fail(-1, "请不要非法操作!");
			}
			
			subRefundReturn.setExpressName(expressName);
			subRefundReturn.setExpressNo(expressNo);
			subRefundReturn.setShipTime(new Date());
			subRefundReturn.setGoodsState(RefundReturnStatusEnum.LOGISTICS_WAIT_RECEIVE.value());//物流状态,待收货
			subRefundReturnService.shipSubRefundReturn(subRefundReturn);
			
			return ResultDtoManager.success();
		} catch (Exception e) {
			LOGGER.error("用户退货异常!", e);
			return ResultDtoManager.fail();
		}
	}
	

	/**
	 * 申请记录
	 */
	@ApiOperation(value = "申请记录", httpMethod = "POST", notes = "退款，退货退款申请记录",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "curPageNO", value = "分页", required = false, dataType = "String"),
		@ApiImplicitParam(paramType="query", name = "status", value = "处理退款状态:[全部:null,退款处理中:1]", required = false, dataType = "Integer")
	})
	@PostMapping(value="/p/subRefundReturnList")
	public ResultDto<AppPageSupport<AppSubRefundReturnDto>> subRefundReturnList(String curPageNO,Integer status) {
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			
			if (AppUtils.isBlank(userId)) {
				return ResultDtoManager.fail(-1, "请先登录");
			}
			AppPageSupport<AppSubRefundReturnDto> result = appSubRefundReturnService.querySubRefundReturnList(userId,curPageNO,status);
			
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("获取用户退款记录列表异常!", e);
			return ResultDtoManager.fail();
		}
	}

	/**
	 * 查看退款详情
	 * 
	 * @param request
	 * @param response
	 * @param refundId
	 * @return
	 */
	@ApiOperation(value = "查看退款详情", httpMethod = "POST", notes = "查看退款详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "refundId", value = "退款记录id", required = true, dataType = "Long")
	@PostMapping(value="/p/subRefundReturnDetail")
	public ResultDto<AppSubRefundReturnDto> refundDetail(Long refundId) {
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			AppSubRefundReturnDto subRefundReturnDto = appSubRefundReturnService.getSubRefundReturn(refundId);
			
			if(AppUtils.isBlank(subRefundReturnDto)){
				return ResultDtoManager.fail(-1, "该申请退款单不存在或已被删除");
			}
			if(!userId.equals(subRefundReturnDto.getUserId())){
				return ResultDtoManager.fail(-1, "没有操作权限，请勿非法操作"); 
			}
			
			// 返回待商家审核的倒计时
			Integer number = findAudoReview(0);
			Date sellerAuditCountDownTime = DateUtils.rollDay(subRefundReturnDto.getApplyTime(), number);
			subRefundReturnDto.setSellerAuditCountDownTime(sellerAuditCountDownTime);
			if (subRefundReturnDto.getIsPresell()){
				Sub sub = subService.getSubById(subRefundReturnDto.getSubId());
				subRefundReturnDto.setReturnFreightAmount(BigDecimal.ZERO);
				if (subRefundReturnDto.getRefundAmount() !=null && subRefundReturnDto.getRefundAmount().compareTo(BigDecimal.ZERO) == 1){
					subRefundReturnDto.setReturnFreightAmount(new BigDecimal(Double.toString(sub.getFreightAmount())));
				}
			}
			return ResultDtoManager.success(subRefundReturnDto);
		} catch (Exception e) {
			LOGGER.error("获取用户退款记录详情异常!", e);
			return ResultDtoManager.fail();
		}
	}


	/**
	 * 删除退换货信息
	 * @param request
	 * @param response
	 * @param refundId
	 * @return
	 */
	@ApiOperation(value = "删除退换货信息", httpMethod = "POST", notes = "删除退换货信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "refundId", value = "退款id", required = true, dataType = "Long")
	@PostMapping(value="/p/refund/delete/{refundId}")
	public ResultDto<String> deleteRefundReturn(HttpServletRequest request,
			HttpServletResponse response, @PathVariable Long refundId) {
		String userId = loginedUserService.getUser().getUserId();
		SubRefundReturn subRefundReturn = subRefundReturnService
				.getSubRefundReturn(refundId);
		if (null == subRefundReturn) {
			return ResultDtoManager.fail(0, "对不起,您操作的记录不存在或已被删除!");
		}
		if (!userId.equals(subRefundReturn.getUserId())) {
			return ResultDtoManager.fail(0, "请不要非法操作!");
		}
		if (RefundReturnStatusEnum.SELLER_DISAGREE.value() != subRefundReturn
				.getSellerState()) {
			return ResultDtoManager.fail(0, "请不要非法操作!");
		}
		subRefundReturnService.deleteRefundReturn(subRefundReturn);
		return ResultDtoManager.success();
	}
	
	
	
	/**
	 * 设置仅退款和退款及退货申请的公共属性
	 * @param from
	 * @return 返回设置完成的 SubRefundReturn 对象
	 */
	private SubRefundReturn setCommonProperty(ApplyRefundReturnBaseDto form, ApplyRefundReturnDto refund, HttpServletRequest request){
		String userId = loginedUserService.getUser().getUserId();
		String userName = loginedUserService.getUser().getUserName(); 
		
		SubRefundReturn subRefundReturn = new SubRefundReturn();
		subRefundReturn.setSubId(form.getOrderId());
		subRefundReturn.setSubNumber(refund.getSubNumber());
		subRefundReturn.setSubMoney(new BigDecimal(refund.getSubMoney()));//设置订单总金额
		subRefundReturn.setSubItemMoney(refund.getProdCash());//订单项的金额
		subRefundReturn.setFlowTradeNo(refund.getFlowTradeNo());
		subRefundReturn.setSubSettlementSn(refund.getSubSettlementSn());
		subRefundReturn.setOrderDatetime(refund.getOrderDateTime());
		subRefundReturn.setPayTypeId(refund.getPayTypeId());
		subRefundReturn.setPayTypeName(refund.getPayTypeName());
		subRefundReturn.setShopId(refund.getShopId());
		subRefundReturn.setShopName(refund.getShopName());
		subRefundReturn.setRefundSn(CommonServiceUtil.getRandomSn(new Date(),"yyyyMMdd"));
		subRefundReturn.setUserId(userId);
		subRefundReturn.setUserName(userName);
		subRefundReturn.setSellerState(RefundReturnStatusEnum.SELLER_WAIT_AUDIT.value());
		subRefundReturn.setApplyState(RefundReturnStatusEnum.APPLY_WAIT_SELLER.value());
		subRefundReturn.setApplyTime(new Date());
		subRefundReturn.setBuyerMessage(form.getBuyerMessage());
		subRefundReturn.setReasonInfo(form.getReasonInfo());
		
		subRefundReturn.setRefundSouce(RefundSouceEnum.USER.value());
		subRefundReturn.setIsPresell(false);
		subRefundReturn.setIsDepositHandleSucces(RefundReturnStatusEnum.DEPOSIT_NO_REFUND.value());
		
		//保存退款及退货凭证图片
		subRefundReturn.setPhotoFile1(form.getPhoto1());
		subRefundReturn.setPhotoFile2(form.getPhoto2());
		subRefundReturn.setPhotoFile3(form.getPhoto3());
		
		return subRefundReturn;
	}
	
	/**
	 * 设置预售订单仅退款和退款及退货申请的公共属性
	 * @param from
	 * @return 返回设置完成的 SubRefundReturn 对象
	 */
	private SubRefundReturn setPresellCommonProperty(ApplyRefundReturnBaseDto form, ApplyRefundReturnDto refund, HttpServletRequest request){
		SubRefundReturn subRefundReturn = new SubRefundReturn();
		String userId = loginedUserService.getUser().getUserId();
		String userName = loginedUserService.getUser().getUserName(); 
		
		subRefundReturn.setSubId(form.getOrderId());
		subRefundReturn.setSubNumber(refund.getSubNumber());
		subRefundReturn.setSubMoney(new BigDecimal(refund.getSubMoney()));//设置订单总金额
		subRefundReturn.setSubItemMoney(refund.getProdCash());//订单项的金额
		subRefundReturn.setShopId(refund.getShopId());
		subRefundReturn.setShopName(refund.getShopName());
		subRefundReturn.setRefundSn(CommonServiceUtil.getRandomSn(new Date(),"yyyyMMdd"));
		subRefundReturn.setUserId(userId);
		subRefundReturn.setUserName(userName);
		
		subRefundReturn.setSellerState(RefundReturnStatusEnum.SELLER_WAIT_AUDIT.value());
		subRefundReturn.setApplyState(RefundReturnStatusEnum.APPLY_WAIT_SELLER.value());
		subRefundReturn.setApplyTime(new Date());
		subRefundReturn.setBuyerMessage(form.getBuyerMessage());
		subRefundReturn.setReasonInfo(form.getReasonInfo());
		
		subRefundReturn.setRefundSouce(RefundSouceEnum.USER.value());
		subRefundReturn.setIsPresell(true);
		
		
		if(PayPctTypeEnum.FULL_PAY.value() == refund.getPayPctType()) {//全额支付，不需要处理订金
			subRefundReturn.setFlowTradeNo(refund.getFlowTradeNo());
			subRefundReturn.setSubSettlementSn(refund.getSubSettlementSn());
			subRefundReturn.setOrderDatetime(refund.getOrderDateTime());
			subRefundReturn.setPayTypeId(refund.getPayTypeId());
			subRefundReturn.setPayTypeName(refund.getPayTypeName());
			subRefundReturn.setIsDepositHandleSucces(RefundReturnStatusEnum.DEPOSIT_NO_REFUND.value());
			
		}else {
			subRefundReturn.setFlowTradeNo(refund.getFinalFlowTradeNo());
			subRefundReturn.setSubSettlementSn(refund.getFinalSettlementSn());
			subRefundReturn.setOrderDatetime(refund.getFinalOrderDateTime());
			subRefundReturn.setPayTypeId(refund.getFinalPayTypeId());
			subRefundReturn.setPayTypeName(refund.getFinalPayTypeName());
			
			subRefundReturn.setDepositFlowTradeNo(refund.getFlowTradeNo());
			subRefundReturn.setDepositSubSettlementSn(refund.getSubSettlementSn());
			subRefundReturn.setDepositOrderDatetime(refund.getOrderDateTime());
			subRefundReturn.setDepositPayTypeId(refund.getPayTypeId());
			subRefundReturn.setDepositPayTypeName(refund.getPayTypeName());
			
			if(form.getIsRefundDeposit()) { //用户选择退订金
				subRefundReturn.setIsDepositHandleSucces(RefundReturnStatusEnum.DEPOSIT_REFUND_PROCESSING.value());
			}else {
				subRefundReturn.setIsDepositHandleSucces(RefundReturnStatusEnum.DEPOSIT_NO_REFUND.value());
			}
		}
	
		//保存退款及退货凭证图片
		subRefundReturn.setPhotoFile1(form.getPhoto1());
		subRefundReturn.setPhotoFile2(form.getPhoto2());
		subRefundReturn.setPhotoFile3(form.getPhoto3());
		
		return subRefundReturn;
	}
	
	/**
	 * 计算已经退款的金额
	 * @return
	 */
	private Double calculateAlreadyRefundAmount(String subSettlementSn){
		//根据settlementSn获得所有已经退款成功的订单项
		List<SubRefundReturn> sunRefundlist = subRefundReturnService.getSubRefundsSuccessBySettleSn(subSettlementSn);
		Double alreadyAmount = 0d;  //已经成功退款的总金额
		for(SubRefundReturn refund:sunRefundlist){
			Double successRefund = refund.getRefundAmount().doubleValue();  //已经成功退款的金额
			alreadyAmount = Arith.add(alreadyAmount,successRefund);  //累加订单中已经成功退款的总金额
		}
		
		return alreadyAmount;
	}
	
	/**
	 * 检查订单是否允许退款操作
	 * @param refund
	 * @return
	 */
	private boolean isAllowRefund(ApplyRefundReturnDto refund) {
		//如果是货到付款 并且订单已经完结
		if(PayMannerEnum.DELIVERY_ON_PAY.value().equals(refund.getPayManner()) && OrderStatusEnum.SUCCESS.value().equals(refund.getOrderStatus())){
			return true;
		}
		if(!PayMannerEnum.DELIVERY_ON_PAY.value().equals(refund.getPayManner())  &&
				OrderStatusEnum.PADYED.value().equals(refund.getOrderStatus()) && 
				RefundReturnStatusEnum.ORDER_NO_REFUND.value().equals(refund.getRefundStatus())){
			//订单处于待发货 并且没有处理过退款单
			return true;
		}
		return false;
	}
	
	
	/**
	 * 检查订单项是否允许退款,退货 操作
	 * @param refund
	 * @return
	 */
	private boolean isAllowItemReturn(ApplyRefundReturnDto refund) {
		//如果这个订单项处于退款操作中或已经完成 则不允许
		if(RefundReturnStatusEnum.ITEM_REFUND_PROCESSING.value().equals(refund.getItemRefundStatus())
				|| RefundReturnStatusEnum.ITEM_REFUND_FINISH.value().equals(refund.getItemRefundStatus())){
			return false;
		}
		if(RefundReturnTypeEnum.REFUND.value().equals(refund.getApplyType().intValue())){
			if(!OrderStatusEnum.CONSIGNMENT.value().equals(refund.getOrderStatus()) && !OrderStatusEnum.SUCCESS.value().equals(
					refund.getOrderStatus())&&!OrderStatusEnum.PADYED.value().equals(refund.getOrderStatus())){
				return false;
			}
		}else{
			if(!OrderStatusEnum.CONSIGNMENT.value().equals(refund.getOrderStatus()) && !OrderStatusEnum.SUCCESS.value().equals(
					refund.getOrderStatus())){
				return false;
			}
		}
		//如果是货到付款 并且订单已经完结
		if (PayMannerEnum.DELIVERY_ON_PAY.value().equals(refund.getPayManner())
				&& ! OrderStatusEnum.SUCCESS.value().equals(
						refund.getOrderStatus())) {
			return false;
		}
		return true;
	}

	
	/**
	 * 检查订单项是否在退换货有效时间内
	 * @return
	 */
	private boolean isInReturnValidPeriod(ApplyRefundReturnDto refund){
		Long prodId = refund.getProdId();
		ProductDetail prod = productService.getProdDetail(prodId);
		Integer days = 0;
		if (AppUtils.isNotBlank(prod)){
			Long categoryId = prod.getCategoryId();
			Category category = categoryService.getCategory(categoryId);
			days = category.getReturnValidPeriod();
		}
		
		//当分类退换货时间设置为空，获取系统默认设置
		if(days == null || days == 0){
			ConstTable constTable = constTableService.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
			OrderSetMgDto orderSetting=null;
			if(AppUtils.isNotBlank(constTable)){
				String setting=constTable.getValue();
			   orderSetting=JSONUtil.getObject(setting, OrderSetMgDto.class);
			}
			days = Integer.parseInt(orderSetting.getAuto_product_return());
		}
		Date finallyDate = refund.getFinallyDate();
		if(null == finallyDate){//说明订单还未完成，默认有效
			return true;
		}
		
		//获得订单完成日期加上退款有效期天后的日期
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(finallyDate);
		calendar.add(Calendar.DATE, days);
		Date returnFinalDate = calendar.getTime();
		return returnFinalDate.after(new Date());
	}
	
	
	/**
	 * 获取退换货自动审核时间或者退换货有效时间
	 * 
	 * @param type
	 *            0 获取退换货自动审核时间 1 获取退换货有效时间
	 * @return
	 */
	private Integer findAudoReview(Integer type) {
		Integer number = 7;
		ConstTable constTable = constTableService.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
		OrderSetMgDto orderSetting = null;
		if (AppUtils.isNotBlank(constTable)) {
			String setting = constTable.getValue();
			orderSetting = JSONUtil.getObject(setting, OrderSetMgDto.class);
			if (1 == type && AppUtils.isNotBlank(orderSetting.getAuto_product_return())) {
				number = Integer.parseInt(orderSetting.getAuto_product_return());
			}
			if (0 == type && AppUtils.isNotBlank(orderSetting.getAuto_return_review())) {
				number = Integer.parseInt(orderSetting.getAuto_return_review());
			}
		}
		return number;
	}
	
	//转退货退款回显参数dto
	private AppApplyRefundReturnDto convertToAppApplyRefundReturnDto(ApplyRefundReturnDto applyRefundReturnDto){
		AppApplyRefundReturnDto appApplyRefundReturnDto = new AppApplyRefundReturnDto();
		appApplyRefundReturnDto.setOrderId(applyRefundReturnDto.getOrderId());
		appApplyRefundReturnDto.setFinalOrderDateTime(applyRefundReturnDto.getFinalOrderDateTime());
		appApplyRefundReturnDto.setIsPayFinal(applyRefundReturnDto.getIsPayFinal());
		appApplyRefundReturnDto.setShopName(applyRefundReturnDto.getShopName());
		appApplyRefundReturnDto.setProdId(applyRefundReturnDto.getProdId());
		appApplyRefundReturnDto.setBasketCount(applyRefundReturnDto.getBasketCount());
		appApplyRefundReturnDto.setFinalRefundAmount(applyRefundReturnDto.getFinalRefundAmount());
		appApplyRefundReturnDto.setFinalFlowTradeNo(applyRefundReturnDto.getFinalFlowTradeNo());
		appApplyRefundReturnDto.setProdName(applyRefundReturnDto.getProdName());
		appApplyRefundReturnDto.setPayPctType(applyRefundReturnDto.getPayPctType());
		appApplyRefundReturnDto.setShopId(applyRefundReturnDto.getShopId());
		appApplyRefundReturnDto.setAttribute(applyRefundReturnDto.getAttribute());
		appApplyRefundReturnDto.setSkuId(applyRefundReturnDto.getSkuId());
		appApplyRefundReturnDto.setRefundAmount(applyRefundReturnDto.getRefundAmount());
		appApplyRefundReturnDto.setDepositRefundAmount(applyRefundReturnDto.getDepositRefundAmount());
		appApplyRefundReturnDto.setOrderItemId(applyRefundReturnDto.getOrderItemId());
		appApplyRefundReturnDto.setFinalPayTypeId(applyRefundReturnDto.getFinalPayTypeId());
		appApplyRefundReturnDto.setIsPayDeposit(applyRefundReturnDto.getIsPayDeposit());
		appApplyRefundReturnDto.setSubMoney(applyRefundReturnDto.getSubMoney());
		appApplyRefundReturnDto.setGoodsCount(applyRefundReturnDto.getGoodsCount());
		appApplyRefundReturnDto.setFinalSettlementSn(applyRefundReturnDto.getFinalSettlementSn());
		appApplyRefundReturnDto.setName(applyRefundReturnDto.getName());
		appApplyRefundReturnDto.setSubType(applyRefundReturnDto.getSubType());
		appApplyRefundReturnDto.setFinalPayTypeName(applyRefundReturnDto.getFinalPayTypeName());
		appApplyRefundReturnDto.setProdPic(applyRefundReturnDto.getProdPic());
		appApplyRefundReturnDto.setProdCash(applyRefundReturnDto.getProdCash());
		appApplyRefundReturnDto.setSubNumber(applyRefundReturnDto.getSubNumber());
		return appApplyRefundReturnDto;
	}
	
	
}
