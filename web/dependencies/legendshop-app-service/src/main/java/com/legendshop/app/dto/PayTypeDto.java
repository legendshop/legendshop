/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;


import com.legendshop.dao.persistence.Transient;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 支付类型Dto
 */
@ApiModel(value="支付类型Dto")
public class PayTypeDto {

	/** The pay id. */
	@ApiModelProperty(value="表里的id")
	private Long payId;
	
	/** 支付方式ID. */
	@ApiModelProperty(value="支付方式标识ID")
	private String payTypeId;
	
	/** 支付方式名称. */
	@ApiModelProperty(value="支付方式名称")
	private String payTypeName;
	
	/** 备注. */
	@ApiModelProperty(value="备注")
	private String memo;
	
	/** 接口类型 */
	@ApiModelProperty(value="接口类型")
	private Integer interfaceType;
	
	/** 是否启用 */
	@ApiModelProperty(value="是否启用")
	private Integer isEnable;
	
	/** 支付接口配置信息 */
	@ApiModelProperty(value="支付接口配置信息")
	private String paymentConfig;

	/**
	 * 默认构造函数.
	 */
	public PayTypeDto() {
	}

	/**
	 * 构造函数.
	 *
	 */
	public PayTypeDto(Long payId, String payTypeId) {
		this.payId = payId;
		this.payTypeId = payTypeId;
	}

	/**
	 * 构造函数
	 *
	 */
	public PayTypeDto(Long payId, String payTypeId, String payTypeName,  String memo) {
		this.payId = payId;
		this.payTypeId = payTypeId;
		this.payTypeName = payTypeName;
		this.memo = memo;
	}
	
	public PayTypeDto(Long payId, String payTypeId, String payTypeName, String memo,Integer isEnable) {
		this.payId = payId;
		this.payTypeId = payTypeId;
		this.payTypeName = payTypeName;
		this.memo = memo;
		this.isEnable = isEnable;
	}

	public Long getPayId() {
		return this.payId;
	}

	public void setPayId(Long payId) {
		this.payId = payId;
	}

	public String getPayTypeId() {
		return this.payTypeId;
	}

	public void setPayTypeId(String payTypeId) {
		this.payTypeId = payTypeId;
	}

	public String getPayTypeName() {
		return this.payTypeName;
	}

	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	@Transient
	public Long getId() {
		return payId;
	}
	
	public void setId(Long id) {
		this.payId = id;
	}

	public Integer getInterfaceType() {
		return interfaceType;
	}

	public void setInterfaceType(Integer interfaceType) {
		this.interfaceType = interfaceType;
	}

	public Integer getIsEnable() {
		return isEnable;
	}

	public void setIsEnable(Integer isEnable) {
		this.isEnable = isEnable;
	}

	public String getPaymentConfig() {
		return paymentConfig;
	}

	public void setPaymentConfig(String paymentConfig) {
		this.paymentConfig = paymentConfig;
	}
	

	
	

}