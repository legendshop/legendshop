/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.biz.model.dto.AppProdCommDto;

import io.swagger.annotations.Api;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 用户咨询.
 */
@ApiIgnore
@Api(tags="用户咨询",value="用户咨询")
@RestController
@RequestMapping(value = "/p")
public class AppUserColsultController{
	/**
	 * Query.
	 *
	 * @param request the request
	 * @param response the response
	 * @param curPageNO the cur page no
	 * @param orderStatus the order status
	 * @return the list
	 */
	@PostMapping(value="/consult/query")
	public List<AppProdCommDto> query(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Load.
	 *
	 * @param request the request
	 * @param response the response
	 * @param curPageNO the cur page no
	 * @param id the id
	 * @return the order dto
	 */
	@PostMapping(value="/consult/save")
	public String save(HttpServletRequest request, HttpServletResponse response, AppProdCommDto userCommentsDto) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Load.
	 *
	 * @param request the request
	 * @param response the response
	 * @param curPageNO the cur page no
	 * @param id the id
	 * @return the order dto
	 */
	@PostMapping(value="/consult/load")
	public AppProdCommDto load(HttpServletRequest request, HttpServletResponse response, String curPageNO, Long consId) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Delete.
	 *
	 * @param request the request
	 * @param response the response
	 * @param id the id
	 * @return the string
	 */
	@PostMapping(value="/consult/delete")
	public String delete(HttpServletRequest request, HttpServletResponse response, Long consId) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
