package com.legendshop.app.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app用于IM会话展示订单数据对象
 */
@ApiModel(value="我的订单信息")
public class AppImOrderDto  implements Serializable{

	private static final long serialVersionUID = -9051781632290284395L;

	/** 商家ID */
	@ApiModelProperty(value="商家ID")
	private Long shopId;
	
	/** 订单Id */
	@ApiModelProperty(value="订单Id")
	private Long subId;
	
	/** 订单编号 */
	@ApiModelProperty(value="订单编号")
	private String subNumber;
	
	/** 订单实际支付总额 */
	@ApiModelProperty(value="订单实际支付总额")
	private Double actualTotal;
	
	/** 订单类型 */
	@ApiModelProperty(value="订单类型")
	private String subType;
	
	/** 订单购买时间 */
	@ApiModelProperty(value="下单时间")
	private Date subDate;
	
	/** 配送类型 参见DvyTypeEnum */
	@ApiModelProperty(value="配送类型")
	private String dvyType;
	
	/** 拼团商品状态  [0:拼团中 -1:拼团失败 2：拼团成功] */
	@ApiModelProperty(value="拼团商品状态  [0:拼团中 -1:拼团失败 2：拼团成功]")
	private Integer mergeGroupStatus;
	
	/** 订单状态 */
	@ApiModelProperty(value="订单状态")
	private Integer status;
	
	/** 退换货状态：0:默认,1:在处理,2:处理完成 */
	private long refundState; 
	
	/** 订单项信息 */
	private List<OrderItem> orderItems;
	
	/** 订单项 */
	@ApiModel(value="订单项信息")
	public static class OrderItem {
		
		@ApiModelProperty(value="订单项ID")
		private Long subItemId;
		
		@ApiModelProperty(value="商品Id")
		private Long prodId;
		
		@ApiModelProperty(value="商品SKU_ID")
		private Long skuId;
		
		@ApiModelProperty(value="商品图片")
		private String pic;
		
		@ApiModelProperty(value="商品sku名称")
		private String skuName;
		
		@ApiModelProperty(value="商品属性")
		private String attribute;
		
		@ApiModelProperty(value="商品原价")
		private Double price;

		@ApiModelProperty(value="商品销售价")
		private Double cash;
		
		@ApiModelProperty(value="购买数量")
		private Integer basketCount;

		public Long getSubItemId() {
			return subItemId;
		}

		public void setSubItemId(Long subItemId) {
			this.subItemId = subItemId;
		}

		public Long getProdId() {
			return prodId;
		}

		public void setProdId(Long prodId) {
			this.prodId = prodId;
		}

		public Long getSkuId() {
			return skuId;
		}

		public void setSkuId(Long skuId) {
			this.skuId = skuId;
		}

		public String getPic() {
			return pic;
		}

		public void setPic(String pic) {
			this.pic = pic;
		}

		public String getSkuName() {
			return skuName;
		}

		public void setSkuName(String skuName) {
			this.skuName = skuName;
		}

		public String getAttribute() {
			return attribute;
		}

		public void setAttribute(String attribute) {
			this.attribute = attribute;
		}
		
		public Double getPrice() {
			return price;
		}

		public void setPrice(Double price) {
			this.price = price;
		}

		public Double getCash() {
			return cash;
		}

		public void setCash(Double cash) {
			this.cash = cash;
		}

		public Integer getBasketCount() {
			return basketCount;
		}

		public void setBasketCount(Integer basketCount) {
			this.basketCount = basketCount;
		}
	}
	
	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public Date getSubDate() {
		return subDate;
	}

	public void setSubDate(Date subDate) {
		this.subDate = subDate;
	}
	
	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public Double getActualTotal() {
		return actualTotal;
	}

	public void setActualTotal(Double actualTotal) {
		this.actualTotal = actualTotal;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getMergeGroupStatus() {
		return mergeGroupStatus;
	}

	public void setMergeGroupStatus(Integer mergeGroupStatus) {
		this.mergeGroupStatus = mergeGroupStatus;
	}

	public String getDvyType() {
		return dvyType;
	}

	public void setDvyType(String dvyType) {
		this.dvyType = dvyType;
	}

	public long getRefundState() {
		return refundState;
	}

	public void setRefundState(long refundState) {
		this.refundState = refundState;
	}

	public List<OrderItem> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(List<OrderItem> orderItems) {
		this.orderItems = orderItems;
	}
}
