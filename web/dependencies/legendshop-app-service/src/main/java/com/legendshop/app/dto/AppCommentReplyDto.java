package com.legendshop.app.dto;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value="评论回复Dto") 
public class AppCommentReplyDto {
	
	/** 回复ID */
	@ApiModelProperty(value="回复ID")  
	private Long id;
	
	/** 用户头像 */
	@ApiModelProperty(value="用户头像 ")
	private String portrait;
	
	/** 用户名 */
	@ApiModelProperty(value="用户名")
	private String replyUserName;
	
	/** 用户等级 */
	@ApiModelProperty(value="用户等级")
	private String gradeName;
	
	/** 回复的父回复Id */
	@ApiModelProperty(value="回复的父回复Id")
	private Long parentReplyId;
	
	/** 回复的回复的用户名 */
	@ApiModelProperty(value="回复的回复的用户名")
	private String parentUserId;
	
	/** 回复的回复的用户名 */
	@ApiModelProperty(value="回复的回复的用户名")
	private String parentUserName;
	
	/** 回复内容 */
	@ApiModelProperty(value="回复内容 ")
	private String replyContent;
	
	/** 回复时间 */
	@ApiModelProperty(value="回复时间")
	/*private String replyTime;*/
	private Date replyTime;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPortrait() {
		return portrait;
	}

	public void setPortrait(String portrait) {
		this.portrait = portrait;
	}

	public String getReplyUserName() {
		return replyUserName;
	}

	public void setReplyUserName(String replyUserName) {
		this.replyUserName = replyUserName;
	}

	public String getGradeName() {
		return gradeName;
	}

	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}

	public Long getParentReplyId() {
		return parentReplyId;
	}

	public void setParentReplyId(Long parentReplyId) {
		this.parentReplyId = parentReplyId;
	}

	public String getParentUserId() {
		return parentUserId;
	}

	public void setParentUserId(String parentUserId) {
		this.parentUserId = parentUserId;
	}

	public String getParentUserName() {
		return parentUserName;
	}

	public void setParentUserName(String parentUserName) {
		this.parentUserName = parentUserName;
	}

	public String getReplyContent() {
		return replyContent;
	}

	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}

	public Date getReplyTime() {
		return replyTime;
	}

	public void setReplyTime(Date replyTime) {
		this.replyTime = replyTime;
	}

	/*public String getReplyTime() {
		return replyTime;
	}

	public void setReplyTime(String replyTime) {
		this.replyTime = replyTime;
	}*/
	
	
}
