package com.legendshop.app.dto.mergeGroup;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.legendshop.app.dto.AppProductPropertyDto;
import com.legendshop.app.dto.AppPropValueImgDto;
import com.legendshop.app.dto.AppSkuDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="拼团活动的已开团详情DTO")
public class AppMergeGroupGroupDetailDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty("拼团活动Id")
	private Long mergeId; 
	
	@ApiModelProperty("商品ID")
	private Long prodId;
	
	@ApiModelProperty("商品名称")
	private String prodName;
	
	@ApiModelProperty("商品图片")
	private String prodPic;
	
	@ApiModelProperty("最低拼团价格")
	private Double minPrice; 
	
	@ApiModelProperty("最低商品价格")
	private Double minProdPrice; 
	
	@ApiModelProperty("拼团人数, 用于显示几人团")
	private Integer peopleNumber;
	
	@ApiModelProperty("是否团长免单")
	private Boolean isHeadFree;
	
	@ApiModelProperty("是否限购,默认否")
	private Boolean isLimit; 
	
	@ApiModelProperty("限购数量, 如果isLimit = true才有")
	private Long limitNumber; 
	
	@ApiModelProperty("拼团活动创建时间")
	private Date activeCreateTime;
	
	@ApiModelProperty("拼团活动开始时间")
	private Date activeStartTime;
	
	@ApiModelProperty("拼团活动结束时间")
	private Date activeEndTime;
	
	@ApiModelProperty("状态[-2:已过期、-1下线、1上线]")
	private Integer activeStatus;
	
	@ApiModelProperty("团ID")
	private Long groupId;
	
	/** 对应addNumber */
	@ApiModelProperty("团编号, 也叫addNumber")
	private String groupNumber;
	
	@ApiModelProperty("已参团人数")
	private Integer participantNumber;
	
	@ApiModelProperty("开团时间")
	private Date openGroupTime;
	
	@ApiModelProperty("成团截止时间, 使用这个时间来做倒计时")
	private Date groupEndTime;
	
	@ApiModelProperty("团状态, 1:进行中,2:成功,3:失败")
	private Integer groupStatus;
	
	@ApiModelProperty("是否自己的团")
	private Boolean isSelf = false;
	
	@ApiModelProperty("是否参加过该团")
	private Boolean isParticipate = false;
	
	@ApiModelProperty("已参团用户信息列表")
	List<AddUserList> addUserList;
	
	@ApiModelProperty("当前用户开团或参团的订单编号, 如果当前用户看的是别人开的团且自己为参与, 则此字段为空")
	private String subNumber;
	
	/** 商品sku集合 */
	@ApiModelProperty("商品sku集合")
	private List<AppSkuDto> skuList;
	
	/** 商品属性集合 **/
	@ApiModelProperty(value="商品属性集合")  
	private List<AppProductPropertyDto> prodPropList;
	
    /**商品属性图片集合**/
	@ApiModelProperty(value="商品属性图片集合")  
    List<AppPropValueImgDto> propValueImgList;
	
	public static class AddUserList {
		
		@ApiModelProperty("用户ID")
		private String userId;
		
		@ApiModelProperty("用户名")
		private String userName;
		
		@ApiModelProperty("用户昵称")
		private String nickName;
		
		@ApiModelProperty("用户头像")
		private String portraitPic;
		
		@ApiModelProperty("是否是团长")
		private Boolean isHead;

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getNickName() {
			return nickName;
		}

		public void setNickName(String nickName) {
			this.nickName = nickName;
		}

		public String getPortraitPic() {
			return portraitPic;
		}

		public void setPortraitPic(String portraitPic) {
			this.portraitPic = portraitPic;
		}

		public Boolean getIsHead() {
			return isHead;
		}

		public void setIsHead(Boolean isHead) {
			this.isHead = isHead;
		}
		
	}

	public Long getMergeId() {
		return mergeId;
	}

	public void setMergeId(Long mergeId) {
		this.mergeId = mergeId;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getProdPic() {
		return prodPic;
	}

	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}

	public Double getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}

	public Double getMinProdPrice() {
		return minProdPrice;
	}

	public void setMinProdPrice(Double minProdPrice) {
		this.minProdPrice = minProdPrice;
	}

	public Integer getPeopleNumber() {
		return peopleNumber;
	}

	public void setPeopleNumber(Integer peopleNumber) {
		this.peopleNumber = peopleNumber;
	}

	public Boolean getIsHeadFree() {
		return isHeadFree;
	}

	public void setIsHeadFree(Boolean isHeadFree) {
		this.isHeadFree = isHeadFree;
	}

	public Boolean getIsLimit() {
		return isLimit;
	}

	public void setIsLimit(Boolean isLimit) {
		this.isLimit = isLimit;
	}

	public Long getLimitNumber() {
		return limitNumber;
	}

	public void setLimitNumber(Long limitNumber) {
		this.limitNumber = limitNumber;
	}

	public Date getActiveCreateTime() {
		return activeCreateTime;
	}

	public void setActiveCreateTime(Date activeCreateTime) {
		this.activeCreateTime = activeCreateTime;
	}

	public Date getActiveStartTime() {
		return activeStartTime;
	}

	public void setActiveStartTime(Date activeStartTime) {
		this.activeStartTime = activeStartTime;
	}

	public Date getActiveEndTime() {
		return activeEndTime;
	}

	public void setActiveEndTime(Date activeEndTime) {
		this.activeEndTime = activeEndTime;
	}

	public Integer getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(Integer activeStatus) {
		this.activeStatus = activeStatus;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getGroupNumber() {
		return groupNumber;
	}

	public void setGroupNumber(String groupNumber) {
		this.groupNumber = groupNumber;
	}

	public Integer getParticipantNumber() {
		return participantNumber;
	}

	public void setParticipantNumber(Integer participantNumber) {
		this.participantNumber = participantNumber;
	}

	public Date getOpenGroupTime() {
		return openGroupTime;
	}

	public void setOpenGroupTime(Date openGroupTime) {
		this.openGroupTime = openGroupTime;
	}

	public Date getGroupEndTime() {
		return groupEndTime;
	}

	public void setGroupEndTime(Date groupEndTime) {
		this.groupEndTime = groupEndTime;
	}

	public Integer getGroupStatus() {
		return groupStatus;
	}

	public void setGroupStatus(Integer groupStatus) {
		this.groupStatus = groupStatus;
	}

	public Boolean getIsSelf() {
		return isSelf;
	}

	public void setIsSelf(Boolean isSelf) {
		this.isSelf = isSelf;
	}

	public Boolean getIsParticipate() {
		return isParticipate;
	}

	public void setIsParticipate(Boolean isParticipate) {
		this.isParticipate = isParticipate;
	}

	public List<AddUserList> getAddUserList() {
		return addUserList;
	}

	public void setAddUserList(List<AddUserList> addUserList) {
		this.addUserList = addUserList;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public List<AppSkuDto> getSkuList() {
		return skuList;
	}

	public void setSkuList(List<AppSkuDto> skuList) {
		this.skuList = skuList;
	}

	public List<AppProductPropertyDto> getProdPropList() {
		return prodPropList;
	}

	public void setProdPropList(List<AppProductPropertyDto> prodPropList) {
		this.prodPropList = prodPropList;
	}

	public List<AppPropValueImgDto> getPropValueImgList() {
		return propValueImgList;
	}

	public void setPropValueImgList(List<AppPropValueImgDto> propValueImgList) {
		this.propValueImgList = propValueImgList;
	}

}
