/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dao;
 
import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.AppToken;

/**
 * The Class AppTokenDao.
 */

public interface AppTokenDao extends GenericDao<AppToken, Long> {
     
	public abstract AppToken getAppToken(Long id);
	
    public abstract int deleteAppToken(AppToken appToken);
	
	public abstract Long saveAppToken(AppToken appToken);
	
	public abstract int updateAppToken(AppToken appToken);
	
	public abstract boolean deleteAppToken(String userId, String accessToken);

	public abstract AppToken getAppTokenByUserId(String userId);

	public abstract boolean deleteAppToken(String userId);

	/**
	 * 根据用户id和平台类型获取token
	 * @param userId    用户id
	 * @param platform  设备平台类型, H5: 手机网页, ANDROID: 安卓APP, IOS: 苹果APP, MP: 小程序
	 * @return
	 */
	public abstract AppToken getAppTokenByUserId(String userId, String platform,String type);

 }
