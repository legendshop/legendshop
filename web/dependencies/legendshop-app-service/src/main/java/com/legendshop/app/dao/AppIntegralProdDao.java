/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dao;
 

import java.util.List;

import com.legendshop.app.dto.AppIntegralProdListDto;
import com.legendshop.dao.GenericDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.integral.IntegralProd;


/**
 * @author quanzc
 * 积分商品
 */
public interface AppIntegralProdDao extends GenericDao<IntegralProd, Long> {
     
	/**
	 * 根据ID获取积分商品
	 * @param id
	 * @return
	 */
	public abstract IntegralProd getIntegralProd(Long id);

	/**
	 * 获取积分商品所有分类
	 * @return
	 */
	public abstract List<Category> findAllIntegralCategory();
	
	/**
	 * 更新商品库存
	 * @param count
	 * @param id
	 * @return
	 */
	public abstract int updateStock(Integer count, Long id);


	
	/**
	 * 查询积分商品分页数据
	 * @param cq
	 * @return
	 */
	public abstract PageSupport<IntegralProd> getIntegralProd(String curPageNO, String integralSort, String saleNumSort);

	/**
	 * 可以根据是否推荐,排序规则查询积分商品列表
	 * @return
	 */
	public abstract AppPageSupport<AppIntegralProdListDto> getIntegralProdList(String curPageNO, Integer pageSize,Boolean isCommend, String order);

	
	
 }
