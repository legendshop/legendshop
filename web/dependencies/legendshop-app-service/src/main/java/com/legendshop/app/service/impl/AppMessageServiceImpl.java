package com.legendshop.app.service.impl;

import com.legendshop.app.dto.AppNewSystemMsgDto;
import com.legendshop.app.dto.AppSiteInformationDto;
import com.legendshop.app.service.AppMessageService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.business.dao.MessageDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.MsgStatusEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.siteInformation.SiteInformation;
import com.legendshop.spi.service.MessageService;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service("appMessageService")
public class AppMessageServiceImpl implements AppMessageService {

	@Autowired
	private MessageService messageService;

	@Autowired
	private MessageDao messageDao;

	@Autowired
	private UserDetailDao userDetailDao;

	@Override
	public AppNewSystemMsgDto getNewSystemMessages(String userName, Integer gradeId) {

		AppNewSystemMsgDto result = new AppNewSystemMsgDto();

		UserDetail userDetail = userDetailDao.getUserDetailByName(userName);
		Date userRegtime = null;
		if(userDetail != null && userDetail.getUserRegtime() != null){
			userRegtime = userDetail.getUserRegtime();
		}

		SiteInformation siteInformation = messageDao.getNewSystemMessages(userName, gradeId, userRegtime);
		if(null != siteInformation){
			String htmlStr = siteInformation.getText();
	
			String regEx_script="<script[^>]*?>[\\s\\S]*?<\\/script>"; //定义script的正则表达式
	        String regEx_style="<style[^>]*?>[\\s\\S]*?<\\/style>"; //定义style的正则表达式
	        String regEx_html="<[^>]+>"; //定义HTML标签的正则表达式
	
			Pattern p_script= Pattern.compile(regEx_script,Pattern.CASE_INSENSITIVE);
	        Matcher m_script=p_script.matcher(htmlStr);
	        htmlStr=m_script.replaceAll(""); //过滤script标签
	
	        Pattern p_style=Pattern.compile(regEx_style,Pattern.CASE_INSENSITIVE);
	        Matcher m_style=p_style.matcher(htmlStr);
	        htmlStr=m_style.replaceAll(""); //过滤style标签
	
	        Pattern p_html=Pattern.compile(regEx_html,Pattern.CASE_INSENSITIVE);
	        Matcher m_html=p_html.matcher(htmlStr);
	        htmlStr = m_html.replaceAll(""); //过滤html标签
			htmlStr = htmlStr.replaceAll("<img[^>]*/>", " ");
			siteInformation.setText(htmlStr);

			result.setMsg(convertToAppSiteInformationDto(siteInformation));
		}

		int unreadCount = messageDao.getCalUnreadSystemMessagesCount(gradeId, userName, userRegtime);
		result.setUnreadCount(unreadCount);

		return result;
	}

	@Override
	public AppSiteInformationDto getMsgDetail(Long msgId, String userName) {

		SiteInformation siteInformation = messageService.getMsgText(userName, msgId, true);

		if(null == siteInformation){
			return null;
		}

		siteInformation.setMsgId(msgId);

		String sendNickName = userDetailDao.getNickNameByUserName(siteInformation.getSendName());

		if(AppUtils.isNotBlank(sendNickName)){
			siteInformation.setSendName(sendNickName);
		}

		AppSiteInformationDto msgDetail = convertToAppSiteInformationDto(siteInformation);

		return msgDetail;
	}

	@Override
	public AppSiteInformationDto getSystemMsgDetail(Long msgId, String userName) {

		SiteInformation siteInformation = messageService.getMsgText(userName, msgId, false);
		if(null == siteInformation){
			return null;
		}

		String sendNickName = userDetailDao.getNickNameByUserName(siteInformation.getSendName());
		if(AppUtils.isNotBlank(sendNickName)){
			siteInformation.setSendName(sendNickName);
		}

		siteInformation.setMsgId(msgId);

		AppSiteInformationDto msgDetail = convertToAppSiteInformationDto(siteInformation);

		//更新系统通知为已读状态
		messageService.updateSystemMsgStatus(userName, msgId, MsgStatusEnum.READED.value());

		return msgDetail;
	}

	@Override
	public AppPageSupport<AppSiteInformationDto> querySystemMessages(String curPageNO, Integer gradeId, String userName, UserDetail userDetail) {

		PageSupport<SiteInformation> ps = messageDao.querySimplePage(curPageNO,gradeId,userName,userDetail);
		List<SiteInformation> siteInformationList = ps.getResultList();
		List<SiteInformation> newList = new ArrayList<SiteInformation>();

		for(SiteInformation siteInformation : siteInformationList){
			String htmlStr = siteInformation.getText();

			String regEx_script="<script[^>]*?>[\\s\\S]*?<\\/script>"; //定义script的正则表达式
			String regEx_style="<style[^>]*?>[\\s\\S]*?<\\/style>"; //定义style的正则表达式
			String regEx_html="<[^>]+>"; //定义HTML标签的正则表达式

			Pattern p_script= Pattern.compile(regEx_script,Pattern.CASE_INSENSITIVE);
			Matcher m_script=p_script.matcher(htmlStr);
			htmlStr=m_script.replaceAll(""); //过滤script标签

			Pattern p_style=Pattern.compile(regEx_style,Pattern.CASE_INSENSITIVE);
			Matcher m_style=p_style.matcher(htmlStr);
			htmlStr=m_style.replaceAll(""); //过滤style标签

			Pattern p_html=Pattern.compile(regEx_html,Pattern.CASE_INSENSITIVE);
			Matcher m_html=p_html.matcher(htmlStr);
			htmlStr = m_html.replaceAll(""); //过滤html标签
			htmlStr = htmlStr.replaceAll("<img[^>]*/>", " ");
			siteInformation.setText(htmlStr);
			newList.add(siteInformation);
		}
		ps.setResultList(newList);

		AppPageSupport<AppSiteInformationDto> ps2 = convertToAppPageSupport(ps);

		return ps2;
	}

	@Override
	public AppPageSupport<AppSiteInformationDto> queryInboxMsg(String curPageNO, String userName) {
		PageSupport<SiteInformation> ps = messageDao.querySimplePage(curPageNO,userName);
		AppPageSupport<AppSiteInformationDto> ps2 = convertToAppPageSupport(ps);
		return ps2;
	}

	private  AppPageSupport<AppSiteInformationDto> convertToAppPageSupport(PageSupport<SiteInformation> ps) {

		return PageUtils.convertPageSupport(ps, new PageUtils.ResultListConvertor<SiteInformation, AppSiteInformationDto>() {

			@Override
			public List<AppSiteInformationDto> convert(List<SiteInformation> form) {

				List<AppSiteInformationDto> list = new ArrayList<AppSiteInformationDto>();
				for(SiteInformation siteInformation : form){
					AppSiteInformationDto appSiteInformationDto  = convertToAppSiteInformationDto(siteInformation);
					list.add(appSiteInformationDto);
				}

				return list;
			}

		});
	}

	private AppSiteInformationDto convertToAppSiteInformationDto(SiteInformation siteInformation){
		AppSiteInformationDto appSiteInformationDto  = new AppSiteInformationDto();
		appSiteInformationDto.setMsgId(siteInformation.getMsgId());
		appSiteInformationDto.setTitle(siteInformation.getTitle());
		appSiteInformationDto.setText(siteInformation.getText());
		appSiteInformationDto.setSendName(siteInformation.getSendName());
		appSiteInformationDto.setRecDate(siteInformation.getRecDate());
		appSiteInformationDto.setStatus(siteInformation.getStatus());
		return appSiteInformationDto;
	}
}
