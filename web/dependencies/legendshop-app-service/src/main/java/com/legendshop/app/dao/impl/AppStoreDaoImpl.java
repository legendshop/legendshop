package com.legendshop.app.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.legendshop.app.dao.AppStoreDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.dto.app.AppStoreDto;
import com.legendshop.model.dto.app.AppStoreSearchParamsDTO;
import com.legendshop.model.entity.store.Store;
import com.legendshop.util.AppUtils;

@Repository
public class AppStoreDaoImpl extends GenericDaoImpl<Store, Long> implements AppStoreDao {

	@Override
	public List<AppStoreDto> findStoreCitys(Long poductId, Integer provinceId, Integer cityId, Integer areaId) {


		QueryMap map = new QueryMap();
		map.put("s_province", provinceId);
		map.put("s_city", cityId);
		map.put("s_areaid", areaId);
		map.put("prod_id", poductId);
		String sql = ConfigCode.getInstance().getCode("store.findStoreCitys", map);
		List<AppStoreDto> appStoreDto = query(sql, map.toArray(), new RowMapper<AppStoreDto>() {
			@Override
			public AppStoreDto mapRow(ResultSet rs, int rowNum) throws SQLException {
				Long storeId = rs.getLong("id");
				String sName = rs.getString("name");
				String sAddress = rs.getString("address");
				String province = rs.getString("province");
				String city = rs.getString("city");
				String ara = rs.getString("ara");
				AppStoreDto appStoreDto = new AppStoreDto();
				/*appStoreDto.setProvinceId(provinceId);
				appStoreDto.setCityId(cityId);
				appStoreDto.setAreaId(areaId);*/
				appStoreDto.setStoreId(storeId);
				appStoreDto.setName(sName);
				appStoreDto.setProvince(province);
				appStoreDto.setCity(city);
				appStoreDto.setArea(ara);
				StringBuffer buffer = new StringBuffer(province);
				buffer.append(" ").append(city).append(" ").append(ara).append(" ").append(sAddress);
				appStoreDto.setAddress(buffer.toString());
				return appStoreDto;
			}
		});
		return appStoreDto;
	}

	@Override
	public List<AppStoreDto> findStoreShopCitys(long shopId, int provinceid, int cityid, int areaid) {
		
		QueryMap map = new QueryMap();
		map.put("s_province", provinceid);
		map.put("s_city", cityid);
		map.put("s_areaid", areaid);
		map.put("shopId", shopId);
		String sql = ConfigCode.getInstance().getCode("store.findStoreShopCitys", map);
		List<AppStoreDto> appStoreDto = query(sql, map.toArray(), new RowMapper<AppStoreDto>() {
			@Override
			public AppStoreDto mapRow(ResultSet rs, int rowNum) throws SQLException {
				Long storeId = rs.getLong("id");
				String sName = rs.getString("name");
				String sAddress = rs.getString("address");
				String province = rs.getString("province");
				String city = rs.getString("city");
				String ara = rs.getString("ara");
				AppStoreDto appStoreDto = new AppStoreDto();
				appStoreDto.setStoreId(storeId);
				appStoreDto.setName(sName);
				StringBuffer buffer = new StringBuffer(province);
				buffer.append(" ").append(city).append(" ").append(ara).append(" ").append(sAddress);
				appStoreDto.setAddress(buffer.toString());
				return appStoreDto;
			}
		});
		return appStoreDto;
		
	}

	/**
	 * 根据传入参数获取相关门店
	 */
	@Override
	public PageSupport<AppStoreDto> queryStore(AppStoreSearchParamsDTO searchParams) {
		
		if (AppUtils.isBlank(searchParams.getPageSize())) {
			searchParams.setPageSize(10);
		}
		if (AppUtils.isBlank(searchParams.getCurPageNO())) {//默认搜索第一页
			searchParams.setCurPageNO("1");
		}
		
		SimpleSqlQuery query = new SimpleSqlQuery(AppStoreDto.class,searchParams.getPageSize(), searchParams.getCurPageNO());
		
		QueryMap map = new QueryMap();
		map.put("prodId", searchParams.getProdId());
		map.put("skuId", searchParams.getSkuId());
		map.put("shopId",searchParams.getShopId());
		map.put("province", searchParams.getProvince());
		map.put("city", searchParams.getCity());
		map.put("area", searchParams.getArea());
		map.put("lng", searchParams.getLng());
		map.put("lat", searchParams.getLat());
		if (AppUtils.isNotBlank(searchParams.getStoreName())) {
			map.put("storeName", "%"+  searchParams.getStoreName() + "%");
		}
		
		String querySQL = ConfigCode.getInstance().getCode("store.queryStores", map);
		String queryAllSQL =  ConfigCode.getInstance().getCode("store.queryStoresCount", map);

		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		
		map.remove("lng", searchParams.getLng());
		map.remove("lat", searchParams.getLat());
		query.setParam(map.toArray());
		return querySimplePage(query);
	}

}
