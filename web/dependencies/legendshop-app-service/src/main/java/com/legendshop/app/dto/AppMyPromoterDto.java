/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用户分销中心数据对象
 *
 */
@ApiModel(value="用户分销中心数据对象")
public class AppMyPromoterDto{

	/** 累计获得佣金. */
	@ApiModelProperty(value="累计获得佣金")
	private Double totalDistCommis; 
		
	/** 已结算佣金. */
	@ApiModelProperty(value="已结算奖励,也是可提现佣金")
	private Double settledDistCommis; 
		
	/** 未结算佣金. */
	@ApiModelProperty(value="未结算奖励")
	private Double unsettleDistCommis; 
		
	/** 推广员状态1是0否 -1拒绝. */
	@ApiModelProperty(value="推广员状态:1是,0否,-1拒绝.")
	private Long promoterSts; 
		
	/** 分佣状态  0 关闭  1 正常. */
	@ApiModelProperty(value="分佣状态 :0 关闭  1 正常.")
	private Integer commisSts;
	
	public Double getTotalDistCommis() {
		return totalDistCommis;
	}

	public Double getSettledDistCommis() {
		return settledDistCommis;
	}

	public void setSettledDistCommis(Double settledDistCommis) {
		this.settledDistCommis = settledDistCommis;
	}

	public Double getUnsettleDistCommis() {
		return unsettleDistCommis;
	}

	public void setUnsettleDistCommis(Double unsettleDistCommis) {
		this.unsettleDistCommis = unsettleDistCommis;
	}

	public Long getPromoterSts() {
		return promoterSts;
	}

	public void setPromoterSts(Long promoterSts) {
		this.promoterSts = promoterSts;
	}

	public Integer getCommisSts() {
		return commisSts;
	}

	public void setCommisSts(Integer commisSts) {
		this.commisSts = commisSts;
	}

	public void setTotalDistCommis(Double totalDistCommis) {
		this.totalDistCommis = totalDistCommis;
	}
	
}
