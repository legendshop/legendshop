package com.legendshop.app.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class AppRefundDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/** 记录ID */
	private Long refundId; 
		
	/** ID */
	private Long subId; 
		
	/** 订单编号 */
	private String subNumber; 
	
	/** 订单总金额 */
	private BigDecimal subMoney;
		
	/** 订单项ID */
	private Long subItemId; 
		
	/** 申请编号 */
	private String refundSn; 
		
	/** 流水号(订单支付流水号) */
	private String flowTradeNo; 
	
	/** ls_sub_settlement 订单支付-清算单据号 去第三方支付的订单单据号 */
	private String subSettlementSn;
	
	/** 第三方退款单号(微信退款单号) */
	private String outRefundNo;
		
	/** 订单支付Id */
	private String payTypeId; 
	
	/** 订单支付Id */
	private String payTypeName; 
		
	/** 店铺ID */
	private Long shopId; 
		
	/** 店铺名称 */
	private String shopName; 
		
	/** 订单商品ID,全部退款是0,默认0 */
	private long productId; 
		
	/** 订单SKU ID,全部退款是0,默认0 */
	private long skuId; 
		
	/** 商品名称 */
	private String productName; 
		
	/** 商品数量 */
	private Long goodsNum; 
		
	/** 退款金额 */
	private java.math.BigDecimal refundAmount; 
		
	/** 商品图片 */
	private String productImage; 
		
	/** 申请类型:1,仅退款,2退款退货,默认为1 */
	private long applyType; 
		
	/** 卖家处理状态:1为待审核,2为同意,3为不同意 */
	private long sellerState; 
		
	/** 申请状态:1为处理中,2为待管理员处理,3为已完成*/
	private long applyState; 
		
	/** 退货类型:1为不用退货,2为需要退货,默认为1 */
	private long returnType; 
		
	/** 物流状态:1为待发货,2为待收货,3为未收到,4为已收货,默认为1 */
	private long goodsState; 
	
	/** 平台处理退款 是否成功*/
	private int isHandleSuccess;
	
	/** 处理退款的方式 : 线下处理 预存款处理 等*/
	private String handleType; 
	
	/** 申请时间 */
	private Date applyTime; 
		
	/** 卖家处理时间 */
	private Date sellerTime; 
		
	/** 管理员处理时间 */
	private Date adminTime; 
	
	/** 申请原因 */
	private String buyerMessage; 
		
	/** 原因内容 */
	private String reasonInfo; 
		
	/** 卖家备注 */
	private String sellerMessage; 
		
	/** 管理员备注 */
	private String adminMessage; 
		
	/** 物流公司名称 */
	private String expressName; 
		
	/** 物流单号 */
	private String expressNo; 
		
	/** 发货时间 */
	private Date shipTime; 
		
	/** 收货时间 */
	private Date receiveTime; 
		
	/** 收货备注 */
	private String receiveMessage; 
	
	/** 第三方已退款金额 */
	private BigDecimal thirdPartyRefund;

	public Long getRefundId() {
		return refundId;
	}

	public void setRefundId(Long refundId) {
		this.refundId = refundId;
	}

	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public BigDecimal getSubMoney() {
		return subMoney;
	}

	public void setSubMoney(BigDecimal subMoney) {
		this.subMoney = subMoney;
	}

	public Long getSubItemId() {
		return subItemId;
	}

	public void setSubItemId(Long subItemId) {
		this.subItemId = subItemId;
	}

	public String getRefundSn() {
		return refundSn;
	}

	public void setRefundSn(String refundSn) {
		this.refundSn = refundSn;
	}

	public String getFlowTradeNo() {
		return flowTradeNo;
	}

	public void setFlowTradeNo(String flowTradeNo) {
		this.flowTradeNo = flowTradeNo;
	}

	public String getSubSettlementSn() {
		return subSettlementSn;
	}

	public void setSubSettlementSn(String subSettlementSn) {
		this.subSettlementSn = subSettlementSn;
	}

	public String getOutRefundNo() {
		return outRefundNo;
	}

	public void setOutRefundNo(String outRefundNo) {
		this.outRefundNo = outRefundNo;
	}

	public String getPayTypeId() {
		return payTypeId;
	}

	public void setPayTypeId(String payTypeId) {
		this.payTypeId = payTypeId;
	}

	public String getPayTypeName() {
		return payTypeName;
	}

	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getSkuId() {
		return skuId;
	}

	public void setSkuId(long skuId) {
		this.skuId = skuId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Long getGoodsNum() {
		return goodsNum;
	}

	public void setGoodsNum(Long goodsNum) {
		this.goodsNum = goodsNum;
	}

	public java.math.BigDecimal getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(java.math.BigDecimal refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	public long getApplyType() {
		return applyType;
	}

	public void setApplyType(long applyType) {
		this.applyType = applyType;
	}

	public long getSellerState() {
		return sellerState;
	}

	public void setSellerState(long sellerState) {
		this.sellerState = sellerState;
	}

	public long getApplyState() {
		return applyState;
	}

	public void setApplyState(long applyState) {
		this.applyState = applyState;
	}

	public long getReturnType() {
		return returnType;
	}

	public void setReturnType(long returnType) {
		this.returnType = returnType;
	}

	public long getGoodsState() {
		return goodsState;
	}

	public void setGoodsState(long goodsState) {
		this.goodsState = goodsState;
	}

	public int getIsHandleSuccess() {
		return isHandleSuccess;
	}

	public void setIsHandleSuccess(int isHandleSuccess) {
		this.isHandleSuccess = isHandleSuccess;
	}

	public String getHandleType() {
		return handleType;
	}

	public void setHandleType(String handleType) {
		this.handleType = handleType;
	}

	public Date getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(Date applyTime) {
		this.applyTime = applyTime;
	}

	public Date getSellerTime() {
		return sellerTime;
	}

	public void setSellerTime(Date sellerTime) {
		this.sellerTime = sellerTime;
	}

	public Date getAdminTime() {
		return adminTime;
	}

	public void setAdminTime(Date adminTime) {
		this.adminTime = adminTime;
	}

	public String getBuyerMessage() {
		return buyerMessage;
	}

	public void setBuyerMessage(String buyerMessage) {
		this.buyerMessage = buyerMessage;
	}

	public String getReasonInfo() {
		return reasonInfo;
	}

	public void setReasonInfo(String reasonInfo) {
		this.reasonInfo = reasonInfo;
	}

	public String getSellerMessage() {
		return sellerMessage;
	}

	public void setSellerMessage(String sellerMessage) {
		this.sellerMessage = sellerMessage;
	}

	public String getAdminMessage() {
		return adminMessage;
	}

	public void setAdminMessage(String adminMessage) {
		this.adminMessage = adminMessage;
	}

	public String getExpressName() {
		return expressName;
	}

	public void setExpressName(String expressName) {
		this.expressName = expressName;
	}

	public String getExpressNo() {
		return expressNo;
	}

	public void setExpressNo(String expressNo) {
		this.expressNo = expressNo;
	}

	public Date getShipTime() {
		return shipTime;
	}

	public void setShipTime(Date shipTime) {
		this.shipTime = shipTime;
	}

	public Date getReceiveTime() {
		return receiveTime;
	}

	public void setReceiveTime(Date receiveTime) {
		this.receiveTime = receiveTime;
	}

	public String getReceiveMessage() {
		return receiveMessage;
	}

	public void setReceiveMessage(String receiveMessage) {
		this.receiveMessage = receiveMessage;
	}

	public BigDecimal getThirdPartyRefund() {
		return thirdPartyRefund;
	}

	public void setThirdPartyRefund(BigDecimal thirdPartyRefund) {
		this.thirdPartyRefund = thirdPartyRefund;
	}

}
