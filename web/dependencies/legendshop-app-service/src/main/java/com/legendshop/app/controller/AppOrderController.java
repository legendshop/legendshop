package com.legendshop.app.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import cn.hutool.core.util.NumberUtil;
import com.legendshop.model.constant.*;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.*;
import com.legendshop.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.app.constants.StatusCodes;
import com.legendshop.app.dto.AppDeliveryDto;
import com.legendshop.app.service.AppOrderService;
import com.legendshop.app.service.AppStoreService;
import com.legendshop.app.service.AppSubItemService;
import com.legendshop.app.service.AppSubService;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSiteMsgEvent;
import com.legendshop.base.util.ExpressMD5;
import com.legendshop.model.SiteMessageInfo;
import com.legendshop.model.dto.ExpressDeliverDto;
import com.legendshop.model.dto.app.AppOrderDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.AppStoreDto;
import com.legendshop.model.dto.app.AppSubCountsDto;
import com.legendshop.model.dto.app.AppSuccessOrderDto;
import com.legendshop.model.dto.app.OrderItemDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.order.KuaiDiLogs;
import com.legendshop.model.dto.order.MySubDto;
import com.legendshop.model.dto.order.OrderSearchParamDto;
import com.legendshop.model.entity.store.StoreOrder;
import com.legendshop.processor.helper.DelayCancelHelper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 订单
 * @author admin
 *
 */
@RestController
@RequestMapping(value = "/p")
@Api(tags="订单管理",value="订单管理，订单列表，订单详情，取消订单，删除订单，查看物流，提醒发货")
public class AppOrderController{

	/** The log. */
	private final Logger Logger = LoggerFactory.getLogger(AppOrderController.class);

	@Autowired
	private SubService subService;

	@Autowired
	private KuaiDiService kuaiDiService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private DelayCancelHelper delayCancelHelper;

	@Autowired
	private OrderService orderService;

	@Autowired
	private AppOrderService appOrderService;

	@Autowired
	private AppSubService appSubService;

	@Autowired
	private DeliveryCorpService deliveryCorpService;

	@Autowired
	private SystemParameterService systemParameterService;

	@Autowired
	private AppSubItemService appSubItemService;

	@Autowired
	private SubHistoryService subHistoryService;

	@Autowired
	private SubItemService subItemService;

	@Autowired
	private BasketService basketService;

	@Autowired
	private SkuService skuService;
	
	@Autowired
	private StoreOrderService storeOrderService;
	
	@Autowired
	private AppStoreService appStoreService;

	@Autowired
	private PresellSubService presellSubService;

	@Autowired
	private MergeGroupAddService mergeGroupAddService;

	/**
	 * 发送站内信
	 */
	@Resource(name = "sendSiteMessageProcessor")
	private EventProcessor<SiteMessageInfo> sendSiteMessageProcessor;

	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private MergeGroupOperateService mergeGroupOperateService;


	/**
	 * 我的订单列表
	 */
	@ApiOperation(value = "我的订单列表", httpMethod = "POST", notes = "我的订单列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "state_type", value = "订单状态[全部：null， 待付款: 1 ，待发货：2 ，待收货：3 ，已完成：4 ， 已取消：5]", required = false, dataType = "Integer"),
		@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = false, dataType = "String"),
	})
	@PostMapping("/myOrder")
	public ResultDto<AppPageSupport<AppOrderDto>> query(Integer state_type,String curPageNO){

		try {
			String userId=loginedUserService.getUser().getUserId();
			if(AppUtils.isBlank(userId)){
				return ResultDtoManager.fail(-1,"请先登录");
			}
			int pageSize = 5;// 默认一页显示条数

			//构建搜索参数
			OrderSearchParamDto paramDto=new OrderSearchParamDto();
			paramDto.setPageSize(pageSize);
			paramDto.setCurPageNO(curPageNO);
			paramDto.setUserId(userId);
			if(AppUtils.isNotBlank(state_type)){
				paramDto.setStatus(state_type);
			}
			paramDto.setDeleteStatus(OrderDeleteStatusEnum.NORMAL.value());
			
			AppPageSupport<AppOrderDto> result  = appOrderService.getMyOrderDtos(paramDto);
			List<AppOrderDto> list = result.getResultList();
			// 修复空指针问题
			if (AppUtils.isNotBlank(list)) {
				list.forEach(s -> s.setFinalPrice(NumberUtil.add(s.getFinalPrice(), s.getFreightAmount())));
			}
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			Logger.error("获取订单列表失败异常!", e);
			return ResultDtoManager.fail();
		}
	}

	/**
	 * 通过订单号获取订单总金额
	 * */
	@ApiOperation(value = "订单总金额", httpMethod = "POST", notes = "订单总金额",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "subNumber", value = "订单号", required = true, dataType = "String")
	@PostMapping("/getOrderActualTotal")
	public ResultDto<Map<String,Object>> getOrderActualTotal(String subNumber){
		Logger.info("*****************************进入通过订单号获取订单总金额***************");
		Map<String,Object> map = new HashMap<String,Object>();
		Double actualTotal = 0.0d;
		String subType = null;
		Integer payPctType = 0;

		String userId = loginedUserService.getUser().getUserId();

		String[] strs = subNumber.split(",");
		for(String str : strs){
			AppOrderDto appOrderDto = appOrderService.findOrderDetail(str, userId);
			if(CartTypeEnum.PRESELL.toCode().equals(appOrderDto.getSubType())){ //定金支付的预售订单
				if(appOrderDto.getPayPctType()==1){
					if(appOrderDto.getIsPayDeposit()==0){ //未支付定金时，计算定金
						actualTotal += appOrderDto.getPreDepositPrice().doubleValue();
						payPctType = appOrderDto.getPayPctType();
					}else { //已支付定金，计算尾款
						// 包含运费
						Double finalPrice = Arith.add(appOrderDto.getFinalPrice().doubleValue(), appOrderDto.getFreightAmount());
						actualTotal += finalPrice;
						payPctType = 2;
					}
				}else{
					actualTotal += appOrderDto.getActualTotal();
				}

				subType = appOrderDto.getSubType();
				
			} else { //其他订单
				actualTotal += appOrderDto.getActualTotal();
			}
		}
		Logger.info("*****************actualTotal: "+actualTotal+"***************************",actualTotal);
		Logger.info("*****************subType: "+subType+"***************************",subType);
		Logger.info("*****************payPctType: "+payPctType+"***************************",payPctType);
		map.put("actualTotal",actualTotal);
		map.put("subType",subType);
		map.put("payPctType", payPctType);
		return ResultDtoManager.success(map);
	}
	
	/**
	 * 我的订单详情
	 */
	@ApiOperation(value = "我的订单详情", httpMethod = "POST", notes = "我的订单详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "subNumber", value = "订单号", required = true, dataType = "String")
	@PostMapping("/myOrderDetail")
	public ResultDto<AppOrderDto> myOrderDetail(String subNumber){
		try {
			String userId = loginedUserService.getUser().getUserId();
			if(AppUtils.isBlank(userId)){
				return ResultDtoManager.fail(-1,"请先登录");
			}
			AppOrderDto appOrderDto = appOrderService.findOrderDetail(subNumber,userId);

			if(AppUtils.isBlank(appOrderDto)){
				return ResultDtoManager.fail(-1, "该订单已不存在或被删除，请重新加载！");
			}
			
			//如果是门店订单，获取门店信息
			if(SubTypeEnum.SHOP_STORE.value().equals(appOrderDto.getSubType())){
				StoreOrder storeOrder = storeOrderService.getDeliveryOrderBySubNumber(appOrderDto.getSubNumber());
				if(AppUtils.isNotBlank(storeOrder)){
					
					//获取门店信息
					AppStoreDto store = appStoreService.getStore(storeOrder.getStoreId());
					//获取提货码
					String dlyoPickupCode = storeOrder.getDlyoPickupCode();
					if (AppUtils.isNotBlank(dlyoPickupCode)) {
						appOrderDto.setDlyoPickupCode(dlyoPickupCode);
					}
					appOrderDto.setStore(store);
				}
			}

			Date countDownTime = null;
			if(OrderStatusEnum.UNPAY.value().equals(appOrderDto.getStatus())){

				//计算订单自动取消的截止时间
				Integer cancelMins = subService.getOrderCancelExpireDate();//获取订单自动取消的时间
				countDownTime = DateUtils.rollMinute(appOrderDto.getSubDate(), cancelMins);
			}else if(OrderStatusEnum.PADYED.value().equals(appOrderDto.getStatus())){


				if(SubTypeEnum.MERGE_GROUP.value().equals(appOrderDto.getSubType())){
					MergeGroupOperate mergeGroupOperate = mergeGroupOperateService.getMergeGroupOperateByAddNumber(appOrderDto.getAddNumber());
					if(null != mergeGroupOperate){
						countDownTime = mergeGroupOperate.getEndTime();
						appOrderDto.setMergerGroupPeopleNum(mergeGroupOperate.getPeopleNumber());
						appOrderDto.setMergerGroupAlreadyAddNum(mergeGroupOperate.getNumber());
					}
				}
			}else if(OrderStatusEnum.CONSIGNMENT.value().equals(appOrderDto.getStatus())){

				//计算订单自动确认收货的截止时间
				Integer confirmDay = subService.getOrderConfirmExpireDate();//获取订单自动确认的时间
				countDownTime = DateUtils.rollDay(appOrderDto.getDvyDate(), confirmDay);
			}

			appOrderDto.setCountDownTime(countDownTime);

			return ResultDtoManager.success(appOrderDto);
		} catch (Exception e) {
			Logger.error("获取订单详情异常!", e);
			return ResultDtoManager.fail();
		}
	}

	/**
	 * 普通订单成功订单详情
	 */
	@ApiOperation(value = "普通订单成功订单详情", httpMethod = "POST", notes = "普通订单成功订单详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "subSettlementSn", value = "清算单据号", required = true, dataType = "String"),
		@ApiImplicitParam(paramType="query", name = "subSettlementType", value = "单据类型", required = true, dataType = "String")
	})
	@PostMapping(value="/successOrderDetail")
	public ResultDto<AppSuccessOrderDto> successOrderDetail(String subSettlementSn,String subSettlementType){
		try {
			String userId = loginedUserService.getUser().getUserId();
			if(AppUtils.isBlank(userId)){
				return ResultDtoManager.fail(-1,"请先登录");
			}
			AppSuccessOrderDto appOrderDto = new AppSuccessOrderDto();
			appOrderDto = appOrderService.findOrderDetailBySubSettlementSn(subSettlementSn,userId);
			if(AppUtils.isBlank(appOrderDto)){
				return ResultDtoManager.fail(-1, "订单还未完成支付！");
			}
			appOrderDto.setSubSettlementType(subSettlementType);
			if (appOrderDto.getSubType().equals("PRE_SELL")){
				PresellSub presellSub = presellSubService.getPresellSub(appOrderDto.getSubNumber());
				if (AppUtils.isNotBlank(presellSub)){
					if (presellSub.getPayPctType()==1){
						// 优先尾款判断
						if (presellSub.getIsPayFinal()==1){
							appOrderDto.setActualTotal(Double.parseDouble(presellSub.getFinalPrice().toString()));
						}else if (presellSub.getIsPayDeposit()==1){
							appOrderDto.setActualTotal(Double.parseDouble(presellSub.getPreDepositPrice().toString()));
						}
					}
				}
			}
			return ResultDtoManager.success(appOrderDto);
		} catch (Exception e) {
			Logger.error("获取提交成功的订单详情异常!", e);
			return ResultDtoManager.fail();
		}
	}

	/**
	 * 我的订单项详情
	 */
	@ApiOperation(value = "我的订单项详情", httpMethod = "POST", notes = "我的订单项详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "subItemId", value = "订单项id", required = true, dataType = "Long")
	@PostMapping(value="/myOrderItemDetail")
	public ResultDto<OrderItemDto> myOrderItemDetail(Long subItemId){

		try {
			String userId = loginedUserService.getUser().getUserId();
			if(AppUtils.isBlank(userId)){
				return ResultDtoManager.fail(-1,"请先登录");
			}
			OrderItemDto itemDto  = appSubItemService.getSubItem(subItemId);
			if(AppUtils.isBlank(itemDto)){
				ResultDtoManager.fail(-1, "该订单项已不存在或被删除，请重新加载！");
			}

			return ResultDtoManager.success(itemDto);
		} catch (Exception e) {
			Logger.error("获取订单项详情异常!", e);
			return ResultDtoManager.fail();
		}
	}


	/**
	 * 获取订单类型角标数量
	 */
	@ApiOperation(value = "获取订单类型角标数量", httpMethod = "GET", notes = "获取订单类型角标数量",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@RequestMapping(value = "/getSubCounts", method = RequestMethod.GET)
	@GetMapping(value="/getSubCounts")
	public ResultDto<List<AppSubCountsDto>> getSubCounts(){

		try {
			String userId=loginedUserService.getUser().getUserId();
			if(AppUtils.isBlank(userId)){
				return ResultDtoManager.fail(-1,"请先登录");
			}

			//获取订单个数
			List<AppSubCountsDto> subCountsList = appSubService.querySubCounts(userId);
			return ResultDtoManager.success(subCountsList);
		} catch (Exception e) {
			Logger.error("获取订单类型角标数量失败异常!", e);
			return ResultDtoManager.fail();
		}
	}

	/**
	 * 取消订单
	 * @param subNumber
	 * @return
	 */
	@ApiOperation(value = "取消订单", httpMethod = "POST", notes = "取消订单,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "subNumber", value = "订单号", required = true, dataType = "String"),
		@ApiImplicitParam(paramType="query", name = "cancelReason", value = "取消订单的原因", required = true, dataType = "String")
	})
	@PostMapping(value="/cancleOrder")
	public ResultDto<String> cancleOrder(String subNumber,String cancelReason){

		try {
			String userId=loginedUserService.getUser().getUserId();
			Sub sub = subService.getSubBySubNumberByUserId(subNumber, userId);
			if(AppUtils.isBlank(sub)){
				return ResultDtoManager.fail(-1, "该订单不存在，请重新加载！");
			}
			if(AppUtils.isBlank(cancelReason)){
				return ResultDtoManager.fail(-1, "请选择取消订单的原因！");
			}
			if(!OrderStatusEnum.UNPAY.value().equals(sub.getStatus())){  //订单是未支付的状态
				return ResultDtoManager.fail(-1, "数据已发生改变，请刷新后再操作!");
			}
			Date date = new Date();
			sub.setUpdateDate(date);
			sub.setStatus(OrderStatusEnum.CLOSE.value());
			sub.setCancelReason(cancelReason);

			//取消订单
			boolean result= subService.cancleOrder(sub);
			if(result){
				SubHistory subHistory = new SubHistory();
				String time = subHistory.DateToString(date);
				subHistory.setRecDate(date);
				subHistory.setSubId(sub.getSubId());
				subHistory.setStatus(SubHistoryEnum.ORDER_CANCEL.value());
				StringBuilder sb=new StringBuilder();
				sb.append(sub.getUserName()).append("于").append(time).append("因为：("+cancelReason+")取消订单 .");
				subHistory.setUserName(sub.getUserName());
				subHistory.setReason(sb.toString());
				subHistoryService.saveSubHistory(subHistory);

				// remove delay queue ordre
				delayCancelHelper.remove(sub.getSubNumber());
			}else {
				return ResultDtoManager.fail();
			}
			return ResultDtoManager.success();
		} catch (Exception e) {
			Logger.error("取消订单异常! ---- >>> 订单号: {subNumber}", e, subNumber);
			return ResultDtoManager.fail();
		}
	}

	/**
	 * 删除订单（将订单放入回收站）
	 * @param subNumber
	 * @return
	 */
	@ApiOperation(value = "删除订单", httpMethod = "POST", notes = "删除订单，返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "subNumber", value = "订单号码", required = true, dataType = "string")
	@PostMapping(value="/removeOrder")
	public ResultDto<String> removeOrder(String subNumber) {
		try{
			String userId = loginedUserService.getUser().getUserId();
			Sub sub = subService.getSubBySubNumberByUserId(subNumber, userId);

			if(AppUtils.isBlank(sub)){
				return ResultDtoManager.fail(-1, "对不起, 您要删除的订单不存在或已被删除!");
			}

			if (OrderStatusEnum.CLOSE.value().equals(sub.getStatus()) || OrderStatusEnum.SUCCESS.value().equals(sub.getStatus())
					&& sub.getRefundState() != 1) {//只有成功或关闭的订单才可以删除
				updateOrderDeleteStatus(sub,OrderDeleteStatusEnum.DELETED.value());
				return ResultDtoManager.success();
			}
			return ResultDtoManager.fail(-1, "对不起, 该订单不允许删除, 请不要非法操作!");
		}catch (Exception e) {
			Logger.error("删除订单异常! ---- >>> 订单号: {subNumber}", e, subNumber);
			return ResultDtoManager.fail(-1, "对不起, 删除失败, 请联系客服!");
		}
	}

	/**
	 * 更改订单删除状态
	 * @param sub
	 * @param deleteStatus
	 */
	private void updateOrderDeleteStatus(Sub sub, Integer deleteStatus) {
		Date date = new Date();
		sub.setUpdateDate(date);
		sub.setDeleteStatus(deleteStatus);
		subService.updateSub(sub);
		orderService.cleanOrderCache(sub.getUserId(), sub.getShopId());
	}



	/**
	 * 确认收货
	 */
	@ApiOperation(value = "确认收货", httpMethod = "POST", notes = "确认收货，返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "subNumber", value = "订单号", required = true, dataType = "string")
	@PostMapping(value="/orderReceive")
	public ResultDto<String> orderReceive(String subNumber) {

		try {
			String userId = loginedUserService.getUser().getUserId();
			if (AppUtils.isBlank(userId)) {
				return ResultDtoManager.fail(0, "未登录！");
			}
			boolean result = subService.orderReceive(subNumber, userId);
			if (result) {
				return ResultDtoManager.success();
			} else {
				return ResultDtoManager.fail(-1, "确认收货失败，请联系客服!");
			}
		} catch (Exception e) {
			Logger.error("确认收货异常! ---- >>> 订单号: {subNumber}", e, subNumber);
			return ResultDtoManager.fail(-1, "对不起, 删除失败, 请联系客服!");
		}
	}

	/**
	 * 提醒发货
	 */
	@ApiOperation(value = "提醒发货", httpMethod = "POST", notes = "提醒发货,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "subNumber", value = "订单号", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "shopId", value = "商家id", required = true, dataType = "Long"),
	})
	@PostMapping(value="/sendSiteMsg")
	public ResultDto<String> sendSiteMsg(String subNumber,Long shopId){
		try {
			String userId = loginedUserService.getUser().getUserId();

			Sub sub = subService.getSubBySubNumber(subNumber);
			if(AppUtils.isBlank(sub)){
				return ResultDtoManager.fail(-1,"对不起, 您操作的订单不存在或已被删除!");
			}

			ShopDetail shopDetail = shopDetailService.getShopDetailById(sub.getShopId());
			if(AppUtils.isBlank(shopDetail)){
				return ResultDtoManager.fail(-1,"对不起, 该订单的所属店铺不存在!");
			}

			if(AppUtils.isNotBlank(sub.getRemindDelivery())){
				if(sub.getRemindDelivery()==1){
					return ResultDtoManager.fail(-1, "您已经提醒过商家了哦, 请耐心等候吧!");
				}
			}else if(!(0==sub.getRefundState())){
				return ResultDtoManager.fail(-1, "正在申请退款中,请耐心等候!");
			}
			subService.remindDeliveryBySN(sub.getSubNumber(), userId);

			//发送站内信 通知
			sendSiteMessageProcessor.process(new SendSiteMsgEvent(shopDetail.getUserName(),"发货提醒","亲，订单【"+sub.getSubNumber()+"】该发货啦").getSource());

		} catch (Exception e) {
			Logger.error("提醒发货信息发送失败----------->" + e.getMessage());
			return ResultDtoManager.fail(StatusCodes.UNKNOWN_ERROR, "对不起, 提醒发货失败, 请联系客服!");
		}
		return ResultDtoManager.success();
	}



	/**
	 * 订单申请售后前 判断该订单是否是拼团的团长免单订单
	 */
	@ApiOperation(value = "订单申请售后前 判断该订单是否是拼团的团长免单订单", httpMethod = "POST", notes = "订单申请售后前 判断该订单是否是拼团的团长免单订单,返回'OK'表示不是团长免单订单，返回'fail'表示是团长免单订单，弹出提示‘该拼团订单为团长免单订单哦,不能发起退货退款申请，您支付的款项拼团成功过后会自动退还’",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "subId", value = "订单Id", required = true, dataType = "Long")
	@PostMapping(value="/isMergeGroupHeadFreeSub")
	public ResultDto<String> isMergeGroupHeadFreeSub(Long subId){


		Sub sub = subService.getSubById(subId);
		if (SubTypeEnum.MERGE_GROUP.value().equals(sub.getSubType())){

			Boolean result = mergeGroupAddService.isMergeGroupHeadFreeSub(subId);
			if (result){
				return ResultDtoManager.fail(-1,"该拼团订单为团长免单订单哦,不能发起退货退款申请，您支付的款项拼团成功过后会自动退还");
			}
		}
		return ResultDtoManager.success();
	}


	/**
	 * 查看物流
	 */
	@ApiOperation(value = "查看物流", httpMethod = "POST", notes = "查看订单物流信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "subNumber", value = "订单号", required = true, dataType = "string"),
	})
	@PostMapping(value="/orderLogistics")
	public ResultDto<AppDeliveryDto> orderLogistics(@RequestParam String subNumber){
		String userId = loginedUserService.getUser().getUserId();
		if (AppUtils.isBlank(userId)) {
			return ResultDtoManager.fail(0, "未登录！");
		}
		MySubDto myorder = subService.findOrderDetail(subNumber,userId);
		if(AppUtils.isBlank(myorder)){
			return ResultDtoManager.fail(-1, "该订单不存在！");
		}

		//物流查询参数
		Long dvyId = myorder.getDvyTypeId();
		String dvyFlowId = myorder.getDvyFlowId();
		String reciverMobile = myorder.getUserAddressSub().getMobile();

		if (AppUtils.isBlank(dvyId) || AppUtils.isBlank(dvyFlowId) || AppUtils.isBlank(reciverMobile)) {
			return ResultDtoManager.fail(-1, "查询物流信息参数有误，请联系客服！");
		}

		DeliveryCorp deliveryCorp = deliveryCorpService.getDeliveryCorpByDvyId(dvyId);

		AppDeliveryDto appDeliveryDto = new AppDeliveryDto();
		if (AppUtils.isNotBlank(deliveryCorp)) {

			SystemParameter systemParameter = systemParameterService.getSystemParameter("EXPRESS_DELIVER");
			if (AppUtils.isBlank(systemParameter) || AppUtils.isBlank(systemParameter.getValue())) { //查询到没配置密钥则 是免费的

				String url = deliveryCorp.getQueryUrl();
				if (AppUtils.isNotBlank(url)) {
					url = url.replaceAll("\\{dvyFlowId\\}", dvyFlowId);
				}
				KuaiDiLogs logs = kuaiDiService.query(url);
				if (AppUtils.isNotBlank(logs) && AppUtils.isNotBlank(logs.getEntries()) && logs.getEntries().size()>0) {

					String entriesLog = JSONUtil.getJson(logs.getEntries());

					//封装返回参数
					appDeliveryDto.setDvyDetail(entriesLog);
					appDeliveryDto.setDvyName(deliveryCorp.getName());
					appDeliveryDto.setDvyNumber(dvyFlowId);
					/*appDeliveryDto.setDvyPic(dvyPic);暂时没有图片返回*/
					return ResultDtoManager.success(appDeliveryDto);
				}

			}else{  //快递100

				if(AppUtils.isBlank(deliveryCorp.getCompanyCode())){
					return ResultDtoManager.fail(-1, "查询该订单的物流信息有误，请联系客服！");
				}

				ExpressDeliverDto express = JSONUtil.getObject(systemParameter.getValue(), ExpressDeliverDto.class);

				//组装参数密钥
				String param ="{\"com\":\""+deliveryCorp.getCompanyCode()+"\",\"num\":\""+dvyFlowId+"\",\"mobiletelephone\":\""+reciverMobile+"\"}";
				String sign = ExpressMD5.encode(param+express.getKey()+express.getCustomer());
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("param",param);
				params.put("sign",sign);
				params.put("customer",express.getCustomer());
				//请求第三方接口
				String text = HttpUtil.httpPost(ExpressMD5.API_EXPRESS_URL, params);
				if (AppUtils.isBlank(text)) {
					return ResultDtoManager.fail(-1, "查询该订单的物流信息有误，请联系客服！");
				}

				JSONObject jsonObject = JSONObject.parseObject(text);
				String result = jsonObject.getString("status");
				if(AppUtils.isBlank(result) || !"200".equals(result)){
					Logger.warn("Express inquiry failed message = {},returnCode = {}", jsonObject.getString("message"),jsonObject.getString("returnCode"));
					return ResultDtoManager.fail(-1, "查询该订单的物流信息有误，请联系客服！");
				}

				//封装返回参数
				appDeliveryDto.setDvyName(deliveryCorp.getName());
				appDeliveryDto.setDvyNumber(dvyFlowId);
				/*appDeliveryDto.setDvyPic(dvyPic);暂时没有图片返回*/
				appDeliveryDto.setDvyDetail(jsonObject.getString("data"));

				return ResultDtoManager.success(appDeliveryDto);
			}
		}
		return ResultDtoManager.fail(-2, "暂无物流信息！");
	}

	/**
	 * 再来一单，直接进入下单界面
	 * @param subNumber
	 * @return
	 */
	@ApiOperation(value = "再次购买", httpMethod = "POST", notes = "再次购买,  返回shopCartItemId数组, 前端可以通过shopCartItemId数组路由到提交订单页面",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "subNumber", value = "订单号", required = true, dataType = "string"),
	})
	@PostMapping(value="/buyAgain")
	public ResultDto<List<Long>> buyAgain(@RequestParam(required = true) String subNumber) {

		try {
			String userId = loginedUserService.getUser().getUserId();

			List<Long> cartList = new ArrayList<>();

			Sub sub = subService.getSubBySubNumber(subNumber);
			if(null == sub){
				return ResultDtoManager.fail();
			}

			List<SubItem> subItemList = subItemService.getSubItem(subNumber);
			if(AppUtils.isNotBlank(subItemList)){
				for(SubItem subItem: subItemList){
					//添加了再次下单时，对于sku商品的库存校验
					Sku sku = skuService.getSku(subItem.getSkuId());
					if(AppUtils.isNotBlank(sku) && sku.getStocks() >= subItem.getBasketCount()){
						Long basketId = basketService.saveToCart(userId, subItem.getProdId(),(int) subItem.getBasketCount(), subItem.getSkuId(),sub.getShopId(), subItem.getDistUserName(), null);
						cartList.add(basketId);
					}else{
						//商品库存不足返回错误信息
						return ResultDtoManager.fail(-1,"部分商品库存不足");
					}
				}
			}

			if(cartList.size() == 0){
				return ResultDtoManager.fail(-1,"没有可购买的商品");
			}
			return ResultDtoManager.success(cartList);
		} catch(Exception e){
			e.printStackTrace();
			return ResultDtoManager.fail();
		}
	}
}
