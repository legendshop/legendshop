package com.legendshop.app.dto ;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 *	积分优惠券列表数据Dto
 */
@ApiModel(value="积分优惠券列表数据Dto") 
public class AppIntegralCouponListDto {

	/** 优惠券 ID */
	@ApiModelProperty(value="优惠券 ID")  
	private Long couponId; 
		
	/** 优惠券 名称 */
	@ApiModelProperty(value="优惠券 名称")  
	private String couponName; 
	
	/** 兑换所需积分（领取方式为"积分兑换"时生效） **/
	@ApiModelProperty(value="兑换所需积分")  
	private Integer needPoints;
		
	/** 优惠券状态 */
	@ApiModelProperty(value="优惠券状态：1为有效，0为失效")  
	private Integer status; 
		
	/** 优惠券图片 **/
	@ApiModelProperty(value="优惠券图片")  
	private String couponPic;
		
	/** 绑定优惠券数量 */
	@ApiModelProperty(value="已兑换数量")  
	private Long bindCouponNumber;
		
	public AppIntegralCouponListDto() {
    }

	public Long getCouponId() {
		return couponId;
	}

	public void setCouponId(Long couponId) {
		this.couponId = couponId;
	}

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCouponPic() {
		return couponPic;
	}

	public void setCouponPic(String couponPic) {
		this.couponPic = couponPic;
	}

	public Long getBindCouponNumber() {
		return bindCouponNumber;
	}

	public void setBindCouponNumber(Long bindCouponNumber) {
		this.bindCouponNumber = bindCouponNumber;
	}

	public Integer getNeedPoints() {
		return needPoints;
	}

	public void setNeedPoints(Integer needPoints) {
		this.needPoints = needPoints;
	}
		
} 
