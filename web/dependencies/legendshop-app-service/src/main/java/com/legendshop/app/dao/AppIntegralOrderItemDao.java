/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dao;
 
import com.legendshop.dao.GenericDao;
import com.legendshop.model.entity.integral.IntegralOrderItem;


/**
 * @author quanzc
 * 积分订单子项
 */
public interface AppIntegralOrderItemDao extends GenericDao<IntegralOrderItem, Long> {
     
	public abstract IntegralOrderItem getIntegralOrderItem(Long id);
	
	public abstract Long saveIntegralOrderItem(IntegralOrderItem integralOrderItem);
	
	public abstract void deleteByOrderId(Long id);
	
 }
