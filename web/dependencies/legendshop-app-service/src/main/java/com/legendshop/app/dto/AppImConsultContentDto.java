package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * 客服咨询内容DTO
 */
@ApiModel(value="客服咨询内容")
public class AppImConsultContentDto {

	/** 咨询的商品sku详情 */
	@ApiModelProperty(value="咨询的商品sku详情")
	private AppImSkuDetailDto skuDetail;
	
	/** 咨询的商品退款单详情 */
	@ApiModelProperty(value="咨询的商品退款单详情")
	private AppSubRefundReturnDto subRefundReturn;

	/** 咨询的商品退款单详情 */
	@ApiModelProperty(value="咨询的订单详情")
	private List<AppImOrderDto> appImOrderDtoList;

	public List<AppImOrderDto> getAppImOrderDtoList() {
		return appImOrderDtoList;
	}

	public void setAppImOrderDtoList(List<AppImOrderDto> appImOrderDtoList) {
		this.appImOrderDtoList = appImOrderDtoList;
	}

	public AppImSkuDetailDto getSkuDetail() {
		return skuDetail;
	}

	public void setSkuDetail(AppImSkuDetailDto skuDetail) {
		this.skuDetail = skuDetail;
	}

	public AppSubRefundReturnDto getSubRefundReturn() {
		return subRefundReturn;
	}

	public void setSubRefundReturn(AppSubRefundReturnDto subRefundReturn) {
		this.subRefundReturn = subRefundReturn;
	}
	
}
