package com.legendshop.app.service;

import com.legendshop.app.dto.*;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.GrassArticle;
import com.legendshop.model.entity.GrassLabel;

import java.util.List;

public interface AppGrassService {

	/**
	 * 分页查询标签
	 * @param pageNo 当前页
	 * @param name 标题
	 * @param size 分页数
	 * @return
	 */
	public abstract AppPageSupport<AppGrassLabelListDto> queryLabel(String pageNo,String name,Integer size);

	/**
	 * 根据标识分页查询粉丝或关注
	 * @param pageNo
	 * @param grassUid 文章用户id
	 * @param flag 标识
	 * @param size
	 * @return
	 */
	public abstract AppPageSupport<AppGrassConcernListDto> queryConcernByFlag(String pageNo,String grassUid,boolean flag,Integer size);


	/**
	 * 分页查询文章
	 * @param pageNo
	 * @param size
	 * @param condition 查询条件
	 * @param order 排序条件
	 * @return
	 */
	public abstract AppPageSupport<AppGrassArticleListDto> queryArticle(String pageNo,Integer size,String condition,String order);

	/**
	 * 分页查询作者文章
	 * @param pageNo
	 * @param writerId 作者id
	 * @param size
	 * @return
	 */
	public abstract AppPageSupport<AppGrassWriteArticleListDto> queryWriter(String pageNo,String writerId,Integer size);

	/**
	 * 文章详情查询
	 * @param grassId 种草文章id
	 * @return
	 */
	public abstract AppGrassArticleInfoDto queryByDisId(Long grassId);

	/**
	 * 查询作者详情
	 * @param grassUid 作者id
	 * @return
	 */
	public abstract AppGrassWriterInfoDto queryWriterInfo(String grassUid);

	/**
	 * 根据种草文章id分页查询文章评论
	 * @param grassId 种草文章id
	 * @return
	 */
	public abstract AppPageSupport<AppGrassCommDto> queryGrassCommByGrassId(Long grassId,String currPage);

	/**
	 * 查询用户购买过的商品列表
	 * @param index
	 * @param size
	 * @return
	 */
	public abstract AppPageSupport<AppGrassProdDto> queryUserProd(String index, int size);

    AppPageSupport<AppGrassArticleListDto> queryArticleByLabel(String pageNo, int size, Long[] labels);
}
