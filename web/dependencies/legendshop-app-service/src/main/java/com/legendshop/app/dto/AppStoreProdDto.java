package com.legendshop.app.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 店铺首页 商品DTO
 * @author 开发很忙
 */
@ApiModel(value="店铺商品Dto") 
public class AppStoreProdDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9116795460218080281L;
	
	/** 商品唯一ID */
	@ApiModelProperty(value="商品唯一ID")  
	private Long prodId;
	
	/** 商品名称 */
	@ApiModelProperty(value="商品名称")  
	private String name;
	
	/** 商品图片 */
	@ApiModelProperty(value="商品图片")  
	private String pic;
	
	/** 商品现价 */
	@ApiModelProperty(value="商品现价")  
	private Double cash;
	
	/** 商品价格 */
	@ApiModelProperty(value="商品价格")  
	private Double price;
	
	/** 已售商品数量 */
	@ApiModelProperty(value="已售商品数量")  
	private Long buys;
	
	/** 商品评论数 */
	@ApiModelProperty(value="商品评论数")  
	private Long comments;
	
	/** 好评数量 **/
	@ApiModelProperty(value="好评数量")  
	private String goodCommentsPercent;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Long getBuys() {
		return buys;
	}

	public void setBuys(Long buys) {
		this.buys = buys;
	}

	public Long getComments() {
		return comments;
	}

	public void setComments(Long comments) {
		this.comments = comments;
	}

	public String getGoodCommentsPercent() {
		return goodCommentsPercent;
	}

	public void setGoodCommentsPercent(String goodCommentsPercent) {
		this.goodCommentsPercent = goodCommentsPercent;
	}

	@Override
	public String toString() {
		return "AppStoreProdDto [prodId=" + prodId + ", name=" + name + ", pic=" + pic + ", cash=" + cash + ", price="
				+ price + ", buys=" + buys + ", comments=" + comments + ", goodCommentsPercent=" + goodCommentsPercent
				+ "]";
	}
}
