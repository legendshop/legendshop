package com.legendshop.app.dto;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("种草社区文章列表Dto")
public class AppGrassArticleListDto {
	
	@ApiModelProperty(value="文章id")
	private Long id;
	
	@ApiModelProperty(value="文章标题")
	private String title;

	@ApiModelProperty(value="创建时间")
	private Date createTime;
	
	@ApiModelProperty(value="文章简介")
	private String intro;
	
	@ApiModelProperty(value="文章图片")
	private String image;
	
	@ApiModelProperty(value="点赞数")
	private Long thumbNum;
	
	@ApiModelProperty(value="作者昵称")
	private String userName;
	
	@ApiModelProperty(value="作者id")
	private String uid;
	
	@ApiModelProperty(value="用户头像")
	private String userImage;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Long getThumbNum() {
		return thumbNum;
	}

	public void setThumbNum(Long thumbNum) {
		this.thumbNum = thumbNum;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getUserImage() {
		return userImage;
	}

	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}


	
	
	
	
}
