/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.constants.StatusCodes;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

/**
 * APP分享的Controller
 */
@ApiIgnore
@Api(tags="分享相关接口",value="处理分享扫码请求")
@RestController
public class AppShareController {
	
	private static Logger logger = LoggerFactory.getLogger(AppShareController.class);
	
	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;
	
	/**
	 * 统一处理扫码请求的Controller
	 * @return
	 */
	@ApiOperation(value = "处理扫码请求", httpMethod = "POST", notes = "处理扫码请求，返回一个url的map集合",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/app/share")
	public ResultDto<Map<String, Object>> appShare() {
		String appDownloadURL = null;
		try {
			appDownloadURL = propertiesUtil.getMobileDomainName() + "/apps/download";
			
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("url", appDownloadURL);
			
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			
			logger.debug("APP分享时获取APP下载地址出错!", e);
			return ResultDtoManager.fail(StatusCodes.UNKNOWN_ERROR, "获取APP下载地址异常, 未知错误!");
		}
	}
}
