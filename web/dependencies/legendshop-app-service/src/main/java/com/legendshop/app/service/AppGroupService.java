/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service;

import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.group.AppGroupDetailDto;
import com.legendshop.model.dto.group.AppGroupDto;
import com.legendshop.model.dto.group.GroupDto;

/**
 * 团购app服务接口 The Interface GroupAppService.
 *
 * @author quanzc 团购 app
 */
public interface AppGroupService {

	/**
	 * 查询团购数据.
	 * @param curPageNO 当前页码
	 * @param sortOrder 排序规则 buyer_count,asc(desc) group_price,asc(desc)
	 * @param groupCategoryId 团购分类ID
	 */
	public abstract AppPageSupport<AppGroupDto> queryGroupPage(String curPageNO, String sortOrder,Long groupCategoryId);

	/**
	 * 转换APP团购DTO(减少不必要的参数).O
	 *
	 * @param groupDto
	 *            the group dto
	 * @return the app group dto
	 */
	public abstract AppGroupDetailDto convertGroupProductDto(GroupDto groupDto);


}
