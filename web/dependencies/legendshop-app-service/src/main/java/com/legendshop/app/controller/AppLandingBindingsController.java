package com.legendshop.app.controller;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.constants.LandingBindingsEnum;
import com.legendshop.app.dto.LandingBindingsDto;
import com.legendshop.app.service.impl.AppLoginUtil;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.SendSMSEvent;
import com.legendshop.base.event.ShortMessageInfo;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.model.constant.SMSTypeEnum;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.entity.AppToken;
import com.legendshop.model.entity.Passport;
import com.legendshop.model.entity.User;
import com.legendshop.model.form.UserMobileForm;
import com.legendshop.spi.service.PassportService;
import com.legendshop.spi.service.SMSLogService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.web.helper.IPHelper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@RestController
@Api(tags="第三方登录",value="第三方登录相关操作")
public class AppLandingBindingsController {


	/** The log. */
	private final Logger log = LoggerFactory.getLogger(AppLandingBindingsController.class);
	
	@Autowired
	private AppLoginUtil appLoginUtil;
	
	@Autowired
	private SMSLogService smsLogService;

	@Autowired
	private PassportService passportService;
	
	@Autowired
	private UserDetailService userDetailService;
	
	/**
	* 短信发送者
	*/
	@Resource(name = "sendSMSProcessor")
    private EventProcessor<ShortMessageInfo> sendSMSProcessor;	
	
    @Autowired
    private PasswordEncoder passwordEncoder;
	
	/**
	 * 第三方登陆
	 * @param dto
	 * @return
	 */
	@ApiOperation(value = "第三方登录", httpMethod = "POST", notes = "第三方登录",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/app/landing")
	public ResultDto<Object> landing(HttpServletRequest request, LandingBindingsDto dto) {
		/**绑定类型*/
		String type = dto.getType();
		/**版本id*/
		String verId = dto.getVerId();
		/**openId*/
		String openId = dto.getOpenId();
		/**昵称*/
		
		@SuppressWarnings("unused")
		String nickName = dto.getNickName();
		
		/**设备id*/
		String deviceId = dto.getDeviceId();
		/**设备平台类型, H5: 手机网页, ANDROID: 安卓APP, IOS: 苹果APP, MP: 小程序*/
		String platform = dto.getPlatform();
		
		Passport passport = passportService.getUserIdByOpenId(type, openId);
		if(AppUtils.isBlank(passport)) {
			return ResultDtoManager.success(LandingBindingsEnum.NON_EXISTENT_BINDINGS.value());
		}
		String userId = passport.getUserId();
		User user = userDetailService.getUser(userId);
		if(AppUtils.isBlank(user)) {
			return ResultDtoManager.success(LandingBindingsEnum.NON_EXISTENT_USER.value());
		}
		
		/**登陆*/
		String ip = IPHelper.getIpAddr(request);
		AppToken appToken= appLoginUtil.loginToken(ip, user.getId(), user.getUserName(), deviceId, platform, verId, null);
		System.out.println(appToken.toDto());
		return ResultDtoManager.success(appToken.toDto());
	}
	
	
	// 获取验证码
	@ApiOperation(value = "获取验证码", httpMethod = "POST", notes = "获取 验证码,成功就返回验证码",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "mobile", value = "手机号码", required = true, dataType = "string")
	@PostMapping("/app/getValidationCode")
	public ResultDto<Object> getValidationCode(HttpServletRequest request,String mobile) {
		String result; // 返回参数
		if(!checkMobile(mobile)) {
			result = LandingBindingsEnum.FORMAT_ERROR_MOBILE.value();
			return ResultDtoManager.fail(-1,result);
		}
		// 发送手机验证码
		String mobileCode = CommonServiceUtil.getRandomSMSCode();

		String ip = "";
		try {
			ip = IPHelper.getIpAddr(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("AppLandingBindingsController getValidationCode 发送短信验证码 mobile：{}，ip：{}", mobile, ip);
		sendSMSProcessor.process(new SendSMSEvent(mobile, mobileCode, mobile, "/app/getValidationCode", ip, new Date()).getSource());
		userDetailService.updateUserSecurity(mobile, mobileCode);
		
		User user = userDetailService.getUserByMobile(mobile);
		result = AppUtils.isBlank(user) 
				? LandingBindingsEnum.NON_EXISTENT_USER.value()
				: LandingBindingsEnum.EXISTENT_USER.value();
				
		return ResultDtoManager.success(result);
	}
	
	/**
	 * 第三方登陆绑定 回调
	 * @param request
	 * @param dto
	 * @return
	 */
	@ApiOperation(value = "第三方登陆绑定回调", httpMethod = "POST", notes = "第三方登陆绑定 回调",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/app/landingBindings")
	public ResultDto<Object> landingBindings(HttpServletRequest request, LandingBindingsDto dto) {
		
		/**绑定类型*/
		String type = dto.getType();
		/**版本id*/
		String verId = dto.getVerId();
		/**openId*/
		String openId = dto.getOpenId();
		/**版本id*/
		String nickName = dto.getNickName();
		/**设备id*/
		String deviceId = dto.getDeviceId();
		/**设备平台类型, H5: 手机网页, ANDROID: 安卓APP, IOS: 苹果APP, MP: 小程序*/
		String platform = dto.getPlatform();
		
		
		String result;
		if(!checkMobile(dto.getMobile())) {
			result = LandingBindingsEnum.FORMAT_ERROR_MOBILE.value();
			return ResultDtoManager.success(result);
		}
		if(!checkValidateCode(dto.getMobile(), dto.getValidateCode())) {
			result = LandingBindingsEnum.VALIDATE_CODE_ERROR.value();
			return ResultDtoManager.success(result);
		}
		User user = userDetailService.getUserByMobile(dto.getMobile());
		if(AppUtils.isBlank(user)) {
			// 注册
			UserMobileForm userMobileForm = new UserMobileForm();
			userMobileForm.setNickName(nickName);
			userMobileForm.setMobile(dto.getMobile());
			userMobileForm.setPassword(dto.getPassword());
			userMobileForm.setMobileCode(dto.getValidateCode());
			user = userDetailService.saveUserReg(IPHelper.getIpAddr(request), userMobileForm, passwordEncoder.encode(userMobileForm.getPassword()));
			if(AppUtils.isBlank(user)) {
				// 注册失败
				result = LandingBindingsEnum.LOGIN_HAS_FAILED.value();
				return ResultDtoManager.success(result);
			}
			// 手机注册的用户，用户安全等级+1,手机验证变为1
			userDetailService.updatePhoneVerifn(user.getName());
			/* 注册环信 去掉 TODO, 有需求再打开
			Map<String, String> paramMap=new HashMap<String, String>();
			paramMap.put("username", user.getUserName());
			paramMap.put("password",user.getUserName()+"123456");
			String param=HttpUtil.httpPost(IMREGURL, paramMap);
			if(AppUtils.isNotBlank(param)) {
				userDetailService.upIsRegIM(user.getId(), 1);
			}
			*/
		} 
		// 绑定第三方登陆
		Passport passport = new Passport();
		passport.setType(type);
		passport.setOpenId(openId);
		passport.setUserId(user.getId());
		passport.setCreateTime(new Date());
		Long id = passportService.savePassport(passport);
		if(AppUtils.isBlank(id)) {
			result = LandingBindingsEnum.BINDINGS_FAIL.value();
			return ResultDtoManager.success(result);
		}
		
		/**登陆*/
		String ip = IPHelper.getIpAddr(request);
		AppToken appToken= appLoginUtil.loginToken(ip, user.getId(), user.getUserName(), deviceId, platform, verId, null);
		return ResultDtoManager.success(appToken.toDto());
	}
	
	
	
	/**校验手机号码格式*/
	private boolean checkMobile(String mobile) {
		if(AppUtils.isBlank(mobile)) {
			return false;
		}
		String regExp = "^(1[3-8])\\d{9}$";  
		Pattern p = Pattern.compile(regExp);  
		Matcher m = p.matcher(mobile);  
		return m.find();
	}
	
	/**检验手机验证码*/
	private boolean checkValidateCode(String mobile, String validateCode) {
		
//		if(AppUtils.isBlank(validateCode)) {
//			return false;
//		}
//		SMSLog smsLog  = smsLogService.getValidSMSCode(mobile, SMSTypeEnum.VAL);
//		if(AppUtils.isBlank(smsLog)) {
//			return false;
//		}
//		return smsLog.getMobileCode().equals(validateCode);
		
		return smsLogService.verifyCodeAndClear(mobile,validateCode,SMSTypeEnum.VAL);
	}
	
}
