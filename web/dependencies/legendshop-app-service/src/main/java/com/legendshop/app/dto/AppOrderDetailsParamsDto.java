package com.legendshop.app.dto;

import java.util.List;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 订单详情参数Dto
 */
@ApiModel
public class AppOrderDetailsParamsDto {
	
	/** 购物车id集合 */
	@ApiModelProperty(value = "购物车id集合")
	private List<Long> shopCartItems;
	
	/** 订单类型:NORMAL */
	@ApiModelProperty(value = "订单类型, 普通订单: NORMAL, 秒杀订单: SECKILL, 门店订单: SHOP_STORE, 预售订单: PRE_SELL, 团购订单: GROUP, 拼团活动: MERGE_GROUP, AUCTIONS: 拍卖订单")
	private String type;
	
	/** 是否使用优惠券,yes or no */
	@ApiModelProperty(value = "是否使用优惠券,yes or no")
	private String couponBack;
	
	/** 地址id */
	@ApiModelProperty(value = "地址id")
	private Long adderessId;

	/** 是否立即购买 false or true */
	@ApiModelProperty(value = "是否立即购买 false or true")
	private Boolean buyNow;
	
	/** 商品id */
	@ApiModelProperty(value = "商品id")
	private Long prodId;
	
	/** skuId */
	@ApiModelProperty(value = "skuId ")
	private Long skuId;
	
	/** 数量 */
	@ApiModelProperty(value = "数量")
	private Integer count;
	
	/** 活动ID */
	@ApiModelProperty(value = "活动ID, 比如拼团订单: MERGE_GROUP, 那么就是拼团活动ID")
	private Long activeId;
	
	/** 团编号 */
	@ApiModelProperty(value = "团编号, 拼团活动才有")
	private String addNumber;

	/** 发票id */
	@ApiModelProperty(value = "发票id")
	private Long invoiceId;
	
	public List<Long> getShopCartItems() {
		return shopCartItems;
	}

	public void setShopCartItems(List<Long> shopCartItems) {
		this.shopCartItems = shopCartItems;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCouponBack() {
		return couponBack;
	}

	public void setCouponBack(String couponBack) {
		this.couponBack = couponBack;
	}

	public Long getAdderessId() {
		return adderessId;
	}

	public void setAdderessId(Long adderessId) {
		this.adderessId = adderessId;
	}

	public Boolean getBuyNow() {
		if(null == buyNow){
			return false;
		}
		return buyNow;
	}

	public void setBuyNow(Boolean buyNow) {
		this.buyNow = buyNow;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}

	public String getAddNumber() {
		return addNumber;
	}

	public void setAddNumber(String addNumber) {
		this.addNumber = addNumber;
	}

	public Long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}
}
