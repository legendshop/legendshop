package com.legendshop.app.service;

import com.legendshop.app.dto.AppHeadlineDto;
import com.legendshop.model.dto.app.AppPageSupport;

/**
 * app头条新闻 service
 * @author linzh
 */
public interface AppHeadlineService {

	/**
	 * 获取头条新闻列表
	 * @param curPageNO 当前页码
	 * @return
	 */
	public abstract AppPageSupport<AppHeadlineDto> query(String curPageNO);

	/**
	 * 获取头条新闻详情
	 * @param id 
	 * @return
	 */
	public abstract AppHeadlineDto getHeadline(Long id);

}
