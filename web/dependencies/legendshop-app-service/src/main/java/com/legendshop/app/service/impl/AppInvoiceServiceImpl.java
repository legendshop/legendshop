package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.util.MethodUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.AppInvoiceService;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.InvoiceDto;
import com.legendshop.model.entity.Invoice;
import com.legendshop.spi.service.InvoiceService;
import com.legendshop.util.AppUtils;

@Service("appInvoiceService")
public class AppInvoiceServiceImpl implements AppInvoiceService{

	@Autowired
	private InvoiceService invoiceService;
	
	@Override
	public AppPageSupport<InvoiceDto> getInvoice(String curPageNO, String userName) {
        PageSupport<Invoice> ps = invoiceService.getInvoicePage(userName, curPageNO,10);
		return convertPageSupportDto(ps);
	}

	private AppPageSupport<InvoiceDto> convertPageSupportDto(PageSupport<Invoice> pageSupport){
		if(AppUtils.isNotBlank(pageSupport)){
			AppPageSupport<InvoiceDto> pageSupportDto = new AppPageSupport<InvoiceDto>();
			pageSupportDto.setTotal(pageSupport.getTotal());
			pageSupportDto.setPageCount(pageSupport.getPageCount());
			pageSupportDto.setCurrPage(pageSupport.getCurPageNO());
			pageSupportDto.setPageSize(pageSupport.getPageSize());
			
			List<InvoiceDto> invoiceDtoList = convertInvoiceDto((List<Invoice>)pageSupport.getResultList());
			pageSupportDto.setResultList(invoiceDtoList);
			pageSupportDto.setPageCount(pageSupport.getPageCount());
			return pageSupportDto;
		}
		return null;
	}
	
	public List<InvoiceDto> convertInvoiceDto(List<Invoice> invoiceList){
		if(AppUtils.isNotBlank(invoiceList)){
			List<InvoiceDto> dtoList = new ArrayList<InvoiceDto>();
			for (Invoice invoice : invoiceList) {

				InvoiceDto invoiceDto = convertToInvoiceDto(invoice);
				dtoList.add(invoiceDto);
			}
			
			return dtoList;
		}
		
		return null;
	}

	private InvoiceDto convertToInvoiceDto(Invoice invoice){
		InvoiceDto invoiceDto = new InvoiceDto();
		invoiceDto.setDepositBank(invoice.getDepositBank());
		invoiceDto.setRegisterPhone(invoice.getRegisterPhone());
		invoiceDto.setInvoiceHumNumber(invoice.getInvoiceHumNumber());
		invoiceDto.setType(invoice.getType());
		invoiceDto.setUserName(invoice.getUserName());
		invoiceDto.setTitle(invoice.getTitle());
		invoiceDto.setUserId(invoice.getUserId());
		invoiceDto.setContent(invoice.getContent());
		invoiceDto.setCommonInvoice(invoice.getCommonInvoice());
		invoiceDto.setRegisterAddr(invoice.getRegisterAddr());
		invoiceDto.setCompany(invoice.getCompany());
		invoiceDto.setId(invoice.getId());
		invoiceDto.setBankAccountNum(invoice.getBankAccountNum());
		return invoiceDto;
	}


	@Override
	public void delById(Long id) {
		invoiceService.delById(id);
	}

    /**
     * 根据发票类型获取发票列表
     * @param userName 用户名
     * @param invoiceType 发票类型
     * @param curPageNo 当前页码
     * @return
     */
    @Override
    public AppPageSupport<InvoiceDto> queryInvoice(String userName, Integer invoiceType, String curPageNo) {

        PageSupport<Invoice> ps  = invoiceService.queryInvoice(userName,invoiceType,curPageNo);
        return convertPageSupportDto(ps);
    }

	/**
	 * 获取发票信息
	 * @param userId 用户ID
	 * @param invoiceId 发票ID
	 * @return
	 */
	@Override
	public InvoiceDto getInvoiceDto(String userId, Long invoiceId) {

		Invoice invoice = invoiceService.getInvoice(invoiceId, userId);
		return convertToInvoiceDto(invoice);
	}

}
