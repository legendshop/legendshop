package com.legendshop.app.dto;

import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app 专题详情返回参数Dto
 */
@ApiModel(value="专题详情返回参数Dto")
public class AppThemeDetailDto {
	
	/** 专题id */
	@ApiModelProperty(value="专题id, 用于跳转专题详情")
	private Long themeId; 
		
	/** 开始时间 */
	@ApiModelProperty(value="开始时间")
	private Date startTime; 
		
	/** 结束时间 */
	@ApiModelProperty(value="结束时间")
	private Date endTime; 
		
	/** 标题 */
	@ApiModelProperty(value="标题")
	private String title; 
	
	/** 标题颜色 */
	@ApiModelProperty(value="标题颜色")
	private String titleColor; 
		
	/** 标题是否在横幅区显示 */
	@ApiModelProperty(value="标题是否在横幅区显示")
	private Long isTitleShow; 
		
	/** 简介(小编导语) */
	@ApiModelProperty(value="简介(小编导语)")
	private String intro; 
		
	/** 简介字体颜色 */
	@ApiModelProperty(value="简介字体颜色")
	private String introColor; 
		
	/** 简介是否在横幅区显示 */
	@ApiModelProperty(value="简介是否在横幅区显示")
	private Long isIntroShow; 
		
	/** 专题描述 */
	@ApiModelProperty(value="专题描述")
	private String themeDesc; 
		
	/** 专题banner图片 */
	@ApiModelProperty(value="专题banner图片")
	private String bannerImg; 
		
	/** 横幅区(banner区)背景色 */
	@ApiModelProperty(value="横幅区(banner区)背景色")
	private String bannerImgColor; 
		
	/** 页面背景图*/
	@ApiModelProperty(value="页面背景图")
	private String backgroundImg; 
	
	/**专题活动图(列表显示图)*/
	@ApiModelProperty(value="专题活动图(列表显示图)")
	private String themeImg;
		
	/** 页面背景色 */
	@ApiModelProperty(value="页面背景色")
	private String backgroundColor; 
		
	/**自定义模块*/
	@ApiModelProperty(value="自定义模块")
	private String customContent;
	
	/** 模板类型 */
	@ApiModelProperty(value="模板类型, 1:商品专题, 精选专题")
	private Integer templateType;
	
	/** 状态 */
	@ApiModelProperty(value="状态 ")
	private Integer status; 
	
	/** 板块与板块商品列表*/
	@ApiModelProperty(value="板块与板块商品列表")
	private List<AppThemeModuleDto>  themeModuleList;
	
	/** 相关专题列表 */
	@ApiModelProperty(value="相关专题列表")
	private List<AppThemeRelatedDto> themeRelatedList;
	
	
	public AppThemeDetailDto() {
		
	}
	
	public Long getThemeId() {
		return themeId;
	}

	public void setThemeId(Long themeId) {
		this.themeId = themeId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getTitleColor() {
		return titleColor;
	}

	public void setTitleColor(String titleColor) {
		this.titleColor = titleColor;
	}

	public Long getIsTitleShow() {
		return isTitleShow;
	}

	public void setIsTitleShow(Long isTitleShow) {
		this.isTitleShow = isTitleShow;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public String getIntroColor() {
		return introColor;
	}

	public void setIntroColor(String introColor) {
		this.introColor = introColor;
	}

	public Long getIsIntroShow() {
		return isIntroShow;
	}

	public void setIsIntroShow(Long isIntroShow) {
		this.isIntroShow = isIntroShow;
	}

	public String getBannerImg() {
		return bannerImg;
	}

	public void setBannerImg(String bannerImg) {
		this.bannerImg = bannerImg;
	}

	public String getThemeImg() {
		return themeImg;
	}

	public void setThemeImg(String themeImg) {
		this.themeImg = themeImg;
	}

	public String getThemeDesc() {
		return themeDesc;
	}

	public void setThemeDesc(String themeDesc) {
		this.themeDesc = themeDesc;
	}

	public String getBannerImgColor() {
		return bannerImgColor;
	}

	public void setBannerImgColor(String bannerImgColor) {
		this.bannerImgColor = bannerImgColor;
	}

	public String getBackgroundImg() {
		return backgroundImg;
	}

	public void setBackgroundImg(String backgroundImg) {
		this.backgroundImg = backgroundImg;
	}

	public String getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getCustomContent() {
		return customContent;
	}

	public void setCustomContent(String customContent) {
		this.customContent = customContent;
	}

	public Integer getTemplateType() {
		return templateType;
	}

	public void setTemplateType(Integer templateType) {
		this.templateType = templateType;
	}

	public List<AppThemeModuleDto> getThemeModuleList() {
		return themeModuleList;
	}

	public void setThemeModuleList(List<AppThemeModuleDto> themeModuleList) {
		this.themeModuleList = themeModuleList;
	}

	public List<AppThemeRelatedDto> getThemeRelatedList() {
		return themeRelatedList;
	}

	public void setThemeRelatedList(List<AppThemeRelatedDto> themeRelatedList) {
		this.themeRelatedList = themeRelatedList;
	}
}
