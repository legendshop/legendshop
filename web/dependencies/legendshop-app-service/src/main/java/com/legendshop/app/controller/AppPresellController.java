/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.app.controller;

import java.util.List;

import com.legendshop.app.service.MyFavoriteService;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.spi.service.LoginedUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.AppActiveBanner;
import com.legendshop.app.dto.presell.AppPresellActivityDto;
import com.legendshop.app.dto.presell.AppPresellProdDetailDto;
import com.legendshop.app.service.AppPresellActivityService;
import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.util.AppUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * App端预售.
 */
@RestController
@Api(tags="预售活动接口",value="预售活动接口")
public class AppPresellController{
	
	private final static Logger LOGGER = LoggerFactory.getLogger(AppPresellController.class);
	
	@Autowired
	private AppPresellActivityService appPresellActivityService;
	
	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private MyFavoriteService myFavoriteService;
	
	@ApiOperation(value = "预售商品列表", httpMethod = "POST", notes = "预售商品列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", dataType = "String", name = "curPageNO", value = "当前页码"),
		@ApiImplicitParam(paramType = "query", dataType = "String", name = "status", value = "状态，0：未开始，1：进行中，2：已结束")
	})
	@PostMapping("/app/presell/list")
	public ResultDto<AppPageSupport<AppPresellActivityDto>> querySeckillProd(@RequestParam String curPageNO, @RequestParam String status) {
		try {
			AppPageSupport<AppPresellActivityDto> ps = appPresellActivityService.queryPresellActivityList(curPageNO, status);

			return ResultDtoManager.success(ps);
		} catch (Exception e) {
			LOGGER.error("获取预售列表失败!", e);
			return ResultDtoManager.fail("获取预售列表失败!");
		}
	}
	
	
	@ApiOperation(value = "预售商品详情", httpMethod = "POST", notes = "预售商品详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "path", dataType = "Long", name = "presellId", value = "预售活动id")
	@PostMapping("/app/presell/views/{presellId}")
	public ResultDto<AppPresellProdDetailDto> queryPresellProdDetail(@PathVariable Long presellId) {
		try {
			if (AppUtils.isBlank(presellId)) {
				return ResultDtoManager.fail("该活动已下线"); // 返回错误状态码
			}
			
			//活动商品详情
			AppPresellProdDetailDto appPresellProdDetailDto = appPresellActivityService.getPresellProdDetailDto(presellId);
			if (AppUtils.isBlank(appPresellProdDetailDto)) {
				return ResultDtoManager.fail("该活动已下线"); 
			}
			if(appPresellProdDetailDto.getStatus() != null && appPresellProdDetailDto.getStatus() != 0) {//不等于0表示已经下线
				return ResultDtoManager.fail("该活动已下线"); 
			}
			
			// 查询店铺详情
			Long shopId = appPresellProdDetailDto.getShopId();
			ShopDetail shopDetail = shopDetailService.getShopDetailByIdNoCache(shopId);
			if(AppUtils.isBlank(shopDetail) || shopDetail.getStatus() != ShopStatusEnum.NORMAL.value()){
				return ResultDtoManager.fail("该店铺已下线"); 
			}
			appPresellProdDetailDto.setSiteName(shopDetail.getSiteName());
			appPresellProdDetailDto.setShopPic(shopDetail.getShopPic());
			appPresellProdDetailDto.setShopType(shopDetail.getShopType());
			
			//获取店铺评分
			String shopScore = shopDetailService.getshopScore(shopDetail.getShopId());
			appPresellProdDetailDto.setShopScore(shopScore);

			//获取登录用户名
			Boolean isCollect = false;
			LoginedUserInfo userInfo = loginedUserService.getUser();
			if(AppUtils.isNotBlank(userInfo)){
				//用户是否已收藏
				isCollect = myFavoriteService.isExistsFavorite(appPresellProdDetailDto.getProdDto().getProdId(), userInfo.getUserName());
			}
			appPresellProdDetailDto.setIsCollect(isCollect);
			
			return ResultDtoManager.success(appPresellProdDetailDto);
		} catch (Exception e) {
			LOGGER.error("获取预售商品详情失败!", e);
			return ResultDtoManager.fail("获取预售商品详情失败!");
		}
	}
	
	
	/**
	 * 获取预售专区的banner图列表
	 * @return
	 */
	@ApiOperation(value = "获取预售专区的banner图列表", notes = "获取预售专区的banner图列表", httpMethod="POST")
	@PostMapping("/app/presell/banners")
	public ResultDto<List<AppActiveBanner>> getActiveBanners() {
		try {
			List<AppActiveBanner> result = appPresellActivityService.getActiveBanners();
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("获取预售专区的banner图列表失败!", e);
			return ResultDtoManager.fail("获取预售专区的banner图列表失败!");
		}
	}
	
}
