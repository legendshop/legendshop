package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *	IntegralProd积分商品
 */
@ApiModel(value="IntegralProd积分商品列表数据对象") 
public class AppIntegralProdListDto  {

	/** 积分商品Id*/
	@ApiModelProperty(value="积分商品Id") 
	private Long id;

	/** 积分商品名称 */
	@ApiModelProperty(value="积分商品名称") 
	private String name;

	/** 兑换积分 */
	@ApiModelProperty(value="兑换积分") 
	private Integer exchangeIntegral;

	/** 商品库存 */
	@ApiModelProperty(value="商品库存") 
	private Long prodStock;

	/** 商品图片 */
	@ApiModelProperty(value="商品图片") 
	private String prodImage;

	/** 状态 */
	@ApiModelProperty(value="状态") 
	private Integer status;

	/** 是否推荐商品 */
	@ApiModelProperty(value="是否推荐商品")  
	private Integer isCommend;

	/** 销售数量 */
	@ApiModelProperty(value="销售兑换数量")  
	private Integer saleNum;

	public AppIntegralProdListDto() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getExchangeIntegral() {
		return exchangeIntegral;
	}

	public void setExchangeIntegral(Integer exchangeIntegral) {
		this.exchangeIntegral = exchangeIntegral;
	}

	public Long getProdStock() {
		return prodStock;
	}

	public void setProdStock(Long prodStock) {
		this.prodStock = prodStock;
	}

	public String getProdImage() {
		return prodImage;
	}

	public void setProdImage(String prodImage) {
		this.prodImage = prodImage;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getIsCommend() {
		return isCommend;
	}

	public void setIsCommend(Integer isCommend) {
		this.isCommend = isCommend;
	}

	public Integer getSaleNum() {
		return saleNum;
	}

	public void setSaleNum(Integer saleNum) {
		this.saleNum = saleNum;
	}
	
}
