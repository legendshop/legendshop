/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app佣金变更历史Dto.
 */
@ApiModel(value="app佣金明细Dto")
public class AppCommisChangeLogDto{

	/** ID. */
	@ApiModelProperty(value="记录id")
	private Long id; 
		
	/** 流水号. */
	@ApiModelProperty(value="流水订单号")
	private String sn; 
		
	/** 变更金额. */
	@ApiModelProperty(value="金额")
	private Double amount; 
		
	/** 添加时间. */
	@ApiModelProperty(value="添加记录时间")
	private Date addTime; 
	
	/** 佣金类型. */
	@ApiModelProperty(value="佣金奖励类型：FIRST_COMMIS为直接下级奖励,SECOND_COMMIS为下二级奖励,THIRD_COMMIS为下三级奖励,REG_COMMIS为推广注册奖励,REDUCE_COMMIS为商品退款，回退奖励")
	private String commisType;

	/** 佣金类型名称. CommisTypeEnum */
	@ApiModelProperty(value="佣金奖励类型名称")
	private String commisTypeName;
		
	/** 下级用户. */
	@ApiModelProperty(value="下级用户名")
	private String subUserName;
	
	/** 结算时间. */
	@ApiModelProperty(value="结算时间")
	private Date settleTime;
	
	/** 结算状态. */
	@ApiModelProperty(value="结算状态")
	private Integer settleSts;
		
	/** 描述. */
	@ApiModelProperty(value="描述")
	private String logDesc; 
	
	/**
	 * The Constructor.
	 */
	public AppCommisChangeLogDto() {
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public String getCommisType() {
		return commisType;
	}

	public void setCommisType(String commisType) {
		this.commisType = commisType;
	}

	public String getSubUserName() {
		return subUserName;
	}

	public void setSubUserName(String subUserName) {
		this.subUserName = subUserName;
	}

	public Date getSettleTime() {
		return settleTime;
	}

	public void setSettleTime(Date settleTime) {
		this.settleTime = settleTime;
	}

	public Integer getSettleSts() {
		return settleSts;
	}

	public void setSettleSts(Integer settleSts) {
		this.settleSts = settleSts;
	}

	public String getLogDesc() {
		return logDesc;
	}

	public void setLogDesc(String logDesc) {
		this.logDesc = logDesc;
	}

	public String getCommisTypeName() {
		return commisTypeName;
	}

	public void setCommisTypeName(String commisTypeName) {
		this.commisTypeName = commisTypeName;
	}
}
