package com.legendshop.app.controller;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.app.dto.AppOrderPayParamDto;
import com.legendshop.app.dto.AppStoreOrderAddParamDto;
import com.legendshop.app.dto.AppStoreOrderDetailsParamsDTO;
import com.legendshop.app.dto.AppUserShopCartOrderDto;
import com.legendshop.app.service.AppCartOrderService;
import com.legendshop.app.service.AppStoreService;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.OrderCommis;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.framework.cache.client.CacheClient;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.*;
import com.legendshop.model.dto.app.*;
import com.legendshop.model.dto.app.store.AppUserShopStoreCartOrderDto;
import com.legendshop.model.dto.buy.*;
import com.legendshop.model.dto.coupon.UserCouponListDto;
import com.legendshop.model.dto.order.AddOrderMessage;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.*;
import com.legendshop.model.entity.store.Store;
import com.legendshop.model.entity.store.StoreSku;
import com.legendshop.processor.DelayCancelOfferProcessor;
import com.legendshop.processor.ProdSnapshotProcessor;
import com.legendshop.processor.coupon.CounponsUtils;
import com.legendshop.spi.service.*;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import com.legendshop.util.JSONUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@Api(tags="门店自提",value="门店自提相关功能")
public class AppStoreCotroller {
	
	/** The log. */
	private final Logger Logger = LoggerFactory.getLogger(AppStoreCotroller.class);

	/**
	 * 订单常用处理工具类
	 */
	@Autowired
	private OrderUtil orderUtil;
	
	@Autowired
	private AppStoreService appStoreService;
	
	@Autowired
	private StoreProdService storeProdService;
	
	@Autowired
	private UserAddressService userAddressService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private SkuService skuService;
	
	@Autowired
	private StockService stockService;
	
	@Autowired
	private OrderCartResolverManager orderCartResolverManager;
	
	@Autowired
	private InvoiceService invoiceService;
	
	@Autowired
	private ShopDetailService shopDetailService;
	
	@Autowired
	private AppCartOrderService appCartOrderService;
	
	@Autowired
	private CacheClient cacheClient;
	
	@Autowired
	private SubService subService;
	
	@Autowired
	private TransportManagerService transportManagerService;
	
	@Autowired
	private CounponsUtils counponsUtils;
	
	@Autowired
	private LoginedUserService loginedUserService;
	
	@Autowired
	private BasketService basketService;
	
	/** 订单快照 **/
	@Autowired
	private ProdSnapshotProcessor prodSnapshotProcessor;
	
	/** 订单延迟取消队列 **/
	@Autowired
	private DelayCancelOfferProcessor delayCancelOfferProcessor;
	
	/** 订单三级分佣 **/
	@Autowired(required=false)
	@Qualifier(value="orderCommisProcessor")
	private EventProcessor<OrderCommis> orderCommisProcessor;

	@Autowired
	private StoreService storeService;

	@Autowired
	private StoreSkuService storeSkuService;
	
	
	/**
	 * 是否支持门店自提
	 * @param poductId  商品Id
	 * @param skuId skuId
	 * @return
	 */
	@ApiOperation(value = "是否支持门店自提", httpMethod = "POST", notes = "根据商品Id和skuId,查询该商品是否支持门店自提,没有skuId则传空,返回true为支持,false为不支持",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "poductId", value = "商品Id", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="query", name = "skuId", value = "skuId", required = false, dataType = "Long")
	})
	@PostMapping(value="/isSupportStore")
	public ResultDto<Boolean> storeProductCitys1(Long poductId, Long skuId) {
	
		Boolean result = false;
		if (AppUtils.isBlank(skuId)) {
			skuId = 0L;
		}
		long number = storeProdService.getStoreProdCount(poductId, skuId);
		if (number > 0) {
			result = true;
		}
		return ResultDtoManager.success(result);
	}
	
	
	
	@ApiOperation(value = "获取门店信息", httpMethod = "POST", notes = "获取门店信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "storeId", value = "门店Id", required = true, dataType = "Long")
	@PostMapping(value="/getStore")
	public ResultDto<AppStoreDto> getStore(Long storeId) {


		AppStoreDto store = appStoreService.getStore(storeId);
		if (AppUtils.isBlank(store)){

			return ResultDtoManager.fail(0, "门店不存在或已被下架");
		}
		return ResultDtoManager.success(store);
	}


	@ApiOperation(value = "获取门店列表", httpMethod = "POST", notes = "根据传入的参数获取相关门店列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/queryStore")
	public ResultDto<AppPageSupport<AppStoreDto>> queryStore(AppStoreSearchParamsDTO searchParams) {

		if (AppUtils.isBlank(searchParams.getProdId()) || AppUtils.isBlank(searchParams.getSkuId())) {
			return ResultDtoManager.fail(0, "该商品已被删除或下架，请重新选择");
		}
		AppPageSupport<AppStoreDto> result = appStoreService.queryStore(searchParams);
		return ResultDtoManager.success(result);
	}

	
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "poductId", value = "商品Id", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="query", name = "skuId", value = "skuId", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="query", name = "lng", value = "经度", required = true, dataType = "Double"),
		@ApiImplicitParam(paramType="query", name = "lat", value = "纬度", required = true, dataType = "Double")
	})
	@ApiOperation(value = "获取距离最近的门店", httpMethod = "POST", notes = "获取距离当前定位最近的门店",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/getNearStore")
	public ResultDto<AppStoreDto> getNearStore(Long poductId,Long skuId,Double lng,Double lat) {
	
		if (AppUtils.isBlank(poductId) || AppUtils.isBlank(skuId)) {
			return ResultDtoManager.fail(0, "该商品已被删除或下架，请重新选择");
		}
		
		AppStoreDto appStoreDto = appStoreService.getNearStore(poductId, skuId,lng,lat);
		return ResultDtoManager.success(appStoreDto);
	}
	
	
	/**
	 * 去往门店订单详情页面 ----下单.
	 * @param reqParams
	 * @return
	 */
	@ApiOperation(value = "确认并核对门店订单信息", httpMethod = "POST", notes = "确认并核对门店订单信息页面",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/p/storeOrderDetails")
	public ResultDto<AppUserShopStoreCartOrderDto> storeOrderDetails(AppStoreOrderDetailsParamsDTO reqParams) {
		
		String userId = loginedUserService.getUser().getUserId();
		String userName = loginedUserService.getUser().getUserName();
		List<Long> shopCartItems = reqParams.getShopCartItems();
		Boolean isBuyNow = reqParams.getBuyNow();
		
		Long storeId = reqParams.getStoreId();
		
		/* 根据类型查询订单列表 */
		Map<String, Object> params = new HashMap<String, Object>();
		if (isBuyNow) {
			
			JSONObject jsonObject = buyNow(isBuyNow, reqParams.getProdId(), reqParams.getSkuId(), reqParams.getCount(),storeId);
			if(!jsonObject.getBooleanValue("result")){
				return ResultDtoManager.fail(0, jsonObject.getString("data"));
			}
			ShopCartItem shopCartItem = jsonObject.getObject("data", ShopCartItem.class);
			params.put("buyNow", true);
			params.put("shopCartItem", shopCartItem);
			
		}
		
		params.put("userId", userId);
		params.put("userName", userName);
		params.put("shopId", reqParams.getShopId());
		params.put("ids", shopCartItems);

		UserShopCartList shopCartList = orderCartResolverManager.queryOrders(CartTypeEnum.SHOP_STORE, params);
		if(AppUtils.isBlank(shopCartList)){
			return ResultDtoManager.fail(-2, "数据已经发生改变,请重新刷新后操作!");
	    }

		//处理购物车，按门店分组
		handlerUserShopCartList(shopCartList);

		// 转换DTO
		AppUserShopStoreCartOrderDto shopCartOrderDto = appStoreService.getShoppingCartOrderDto(shopCartList);

		if(AppUtils.isBlank(shopCartOrderDto)){
			return ResultDtoManager.fail(-2, "数据已经发送改变,请重新刷新后操作!");
	    }

		//设置订单提交令牌,防止重复提交
		Integer token=CommonServiceUtil.generateRandom();
		shopCartOrderDto.setToken(token+"");
		cacheClient.put(Constants.APP_FROM_SESSION_TOKEN + ":" + CartTypeEnum.SHOP_STORE.toCode() + ":" + userId, Constants.APP_FROM_SESSION_TOKEN_EXPIRE, token);

		subService.putUserShopCartList(CartTypeEnum.SHOP_STORE.toCode(),userId,shopCartList);
		return ResultDtoManager.success(shopCartOrderDto);
	}
	
	
	/**
	 * 提交门店订单
	 * @return
	 */
	@ApiOperation(value = "提交门店订单", httpMethod = "POST", notes = "提交门店订单",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/p/submitStoreOrder")
	public  ResultDto<AppOrderPayParamDto> submitStoreOrder(HttpServletRequest request,AppStoreOrderAddParamDto paramDto) {
		
		//参数检查
		LoginedUserInfo appToken = loginedUserService.getUser();
		String userId = appToken.getUserId();
		String userName = appToken.getUserName();
		
		//判断是否重复提交
		Integer sessionToken = cacheClient.get(Constants.APP_FROM_SESSION_TOKEN + ":" + CartTypeEnum.SHOP_STORE.toCode() + ":" + userId);
		AddOrderMessage message = checkSubmitParam(userId, sessionToken, CartTypeEnum.SHOP_STORE, paramDto);//检查输入参数
		if(message.hasError()){//有错误就返回
			return ResultDtoManager.fail(0, message.getMessage());
		}
		
		if(AppUtils.isBlank(userId)){
			return ResultDtoManager.fail(0, "没有登录");
		}
		
		//get cache 从上个页面获取设置进入缓存的订单
		UserShopCartList userShopCartList = subService.findUserShopCartList(CartTypeEnum.SHOP_STORE.toCode(), userId);
		if(AppUtils.isBlank(userShopCartList)){
			return ResultDtoManager.fail(-1,"没有购物清单");
		}
				
		//检查金额： 如果促销导致订单金额为负数
	    if(userShopCartList.getOrderTotalCash() < 0){
	    	return ResultDtoManager.fail(-1,"订单金额为负数,下单失败!");
	    }

		// 处理门店提货人名称
		try {
			orderUtil.handlerStoreBuyerName(paramDto.getBuyerName(),userShopCartList);
		} catch (Exception e) {
			Logger.error("处理门店提货人名称出错----->"+e.getMessage());
			return ResultDtoManager.fail(-1,"处理门店提货人名称出错!");
		}

		// 处理门店提货人号码
		try {
			orderUtil.handlerStoreTelPhone(paramDto.getTelPhone(),userShopCartList);
		} catch (Exception e) {
			Logger.error("处理门店提货人号码出错----->"+e.getMessage());
			return ResultDtoManager.fail(-1,"处理门店提货人号码出错!");
		}

		//处理门店买家留言
		try {
			orderUtil.handlerStoreRemarkText(paramDto.getRemarkText(), userShopCartList);
		} catch (Exception e) {
			Logger.error("处理门店买家留言出错----->"+e.getMessage());
			return ResultDtoManager.fail(-1,"处理门店买家留言出错!");
		}

		//检查库存
		String result = orderUtil.verifySafetyStock(userShopCartList);
		if (!Constants.SUCCESS.equals(result)) {
			return ResultDtoManager.fail(-1,result);
		}
		
		userShopCartList.setUserId(userId);
		userShopCartList.setUserName(userName);
		userShopCartList.setInvoiceId(paramDto.getInvoiceId());
		userShopCartList.setPayManner(2);//线上支付
		userShopCartList.setType(CartTypeEnum.SHOP_STORE.toCode());

		
		/**
		 * 处理订单
		 */
		List<String> subIdList= null;
		try {
			subIdList=orderCartResolverManager.addOrder(CartTypeEnum.SHOP_STORE, userShopCartList);
		} 
		catch (Exception e) {
			Logger.error(e.getMessage());
			if(e instanceof BusinessException){
				Logger.error("处理订单出错---->"+e.getMessage());
				return ResultDtoManager.fail(-1,e.getMessage());
			}
			Logger.error("处理订单出错---->"+e.getMessage());
			return ResultDtoManager.fail(-1,"下单失败,请联系商城管理员或商城客服");
		}
		subService.evictUserShopCartCache(CartTypeEnum.SHOP_STORE.toCode(), userId);//evict cache
		if (AppUtils.isBlank(subIdList)) {
			return ResultDtoManager.fail(-1,"下单失败,请联系商城管理员或商城客服");
		}else{
			//offer 延时取消队列
	  		delayCancelOfferProcessor.process(subIdList);
	        
			//发送网站快照备份
			prodSnapshotProcessor.process(subIdList);
			
			//订单分佣情况
			orderCommisProcessor.process(new OrderCommis(subIdList, userId));
		}
		
		final String [] ids=new String[subIdList.size()];
		for (int i = 0; i < subIdList.size(); i++) {
			ids[i]=subIdList.get(i);
		}
		//根据订单IDs获取订单集合
		List<Sub> subs=subService.getSubBySubNums(ids, userId, OrderStatusEnum.UNPAY.value());
		Double totalAmount=0.0;//总金额
		String subject=""; //商品描述
		StringBuffer sub_number=new StringBuffer(""); //订单流水号
		for (Sub sub : subs) {
			totalAmount= Arith.add(totalAmount,sub.getActualTotal());
			sub_number.append(sub.getSubNumber()).append(",");
			subject += subject+sub.getProdName()+",";
		}
		
		String subNumber=sub_number.substring(0, sub_number.length()-1);
		
		AppOrderPayParamDto orderPay = new AppOrderPayParamDto();
		orderPay.setTotalAmount(totalAmount);
		orderPay.setSubNumber(subNumber);
		orderPay.setSubjects(subject);
		orderPay.setSubSettlementType(SubSettlementTypeEnum.USER_ORDER_PAY.value());
		
		request.getSession().removeAttribute(Constants.PASSWORD_CALLBACK);
		// 更新session购物车数量
		subService.evictUserShopCartCache(CartTypeEnum.SHOP_STORE.toCode(), userId);
		// 验证成功，清除令牌
		cacheClient.delete(Constants.APP_FROM_SESSION_TOKEN + ":" +CartTypeEnum.SHOP_STORE.toCode() + ":" + userId);		
		
		return ResultDtoManager.success(orderPay);
	}

	
	
	/**
	 * 门店商品立即购买参数检查和构建
	 * @param buyNow
	 * @param prodId
	 * @param skuId
	 * @param count
	 * @return
	 */
	private JSONObject buyNow(Boolean buyNow, Long prodId, Long skuId, Integer count,Long storeId){
		JSONObject jsonObject=new JSONObject();
		jsonObject.put("result", false);
		
		Product product = productService.getProductById(prodId);
		ResultDto<T> resultDto = null;

		if(product == null){
			resultDto= ResultDtoManager.fail(-1, "对不起,您要购买的商品不存在或已下架!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			return jsonObject;
		}

		Store store  = storeService.getStore(storeId);
		if (AppUtils.isBlank(store) || !store.getIsDisable()) {

			resultDto= ResultDtoManager.fail(-1, "该门店不存在或刚被下线!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			return jsonObject;
		}

		if (count == null || count == 0) {//默认要为1
			count = 1;
		}

		if(AppUtils.isBlank(product.getShopId()) || product.getShopId()==0){
			resultDto=  ResultDtoManager.fail(-1, "对不起, 您购买的商品信息异常, 不能购买!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			return jsonObject;
		}

		if(Constants.OFFLINE.equals(product.getStatus())){
			resultDto = ResultDtoManager.fail(-1, "对不起, 您要购买的商品已下架!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			return jsonObject;
		}

		if(!stockService.canOrder(product, count)){
			//数量检查
			resultDto = ResultDtoManager.fail(-1, "对不起,您购买的商品库存不足!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			return jsonObject;
		}

		//SKU数量检查
		Sku sku = skuService.getSku(skuId);
		if(!stockService.canOrder(sku, count)){
			resultDto = ResultDtoManager.fail(-1, "对不起, 您要购买的数量已超出商品的库存量!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			return jsonObject;
		}

		StoreSku storeSku = storeSkuService.getStoreSkuBySkuId(storeId, skuId);
		//检查门店的sku是否存在
		if (AppUtils.isBlank(storeSku)) {
			resultDto = ResultDtoManager.fail(-1, "亲, 该门店商品不存在或已被删除了哦, 看看其他的吧！");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			return jsonObject;
		}
		//检查门店的sku库存
		if(storeSku.getStock() - count < 0 ){
			resultDto = ResultDtoManager.fail(-1, "亲, 该门店库存不足哦, 选择其他门店看看吧!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			return jsonObject;
		}

		ShopCartItem shopCartItem=new ShopCartItem();
		shopCartItem.setProdId(prodId);
		shopCartItem.setShopId(product.getShopId());
		shopCartItem.setSkuId(skuId);
		shopCartItem.setPrice(sku.getPrice());
		if(AppUtils.isNotBlank(sku.getPic())){
			shopCartItem.setPic(sku.getPic());
		}else{
			shopCartItem.setPic(product.getPic());
		}
		if(AppUtils.isNotBlank(sku.getName())){
			shopCartItem.setProdName(sku.getName());
		}else{
			shopCartItem.setProdName(product.getName());
		}
		shopCartItem.setPromotionPrice(sku.getPrice());
		shopCartItem.setBasketCount(count);
		shopCartItem.setStatus(1);
		shopCartItem.setStocks(sku.getStocks().intValue());
		shopCartItem.setActualStocks(sku.getActualStocks().intValue());
		shopCartItem.setStockCounting(product.getStockCounting());
		shopCartItem.setWeight(sku.getWeight());
		shopCartItem.setVolume(sku.getVolume());
		shopCartItem.setTransportId(product.getTransportId());
		shopCartItem.setTransportFree(product.getSupportTransportFree());
		shopCartItem.setTransportType(product.getTransportType());
		shopCartItem.setEmsTransFee(0F);
		shopCartItem.setExpressTransFee(product.getExpressTransFee().floatValue());
		shopCartItem.setMailTransFee(0F);
		shopCartItem.setSupportCod(product.getIsSupportCod()==0?false:true);
		shopCartItem.setIsGroup(product.getIsGroup());
		shopCartItem.setProperties(sku.getProperties());
		shopCartItem.setCnProperties(sku.getCnProperties());
		shopCartItem.setSupportStore(true);
		shopCartItem.setCheckSts(1);//立即购买默认是选中
		shopCartItem.setStoreId(storeId);
		shopCartItem.setStoreName(store.getName());
		shopCartItem.setStoreAddr(store.getShopAddr());

		jsonObject.put("result", true);
		jsonObject.put("data", shopCartItem);

		return jsonObject;
	}
	

	
	
	/**
	 * 检查门店订单参数
	 * @return
	 */
	private AddOrderMessage checkSubmitParam(String userId, Integer sessionToken, CartTypeEnum typeEnum, AppStoreOrderAddParamDto paramDto){
		
		AddOrderMessage message=new AddOrderMessage();
		message.setResult(false);
		if(AppUtils.isBlank(userId)){
			message.setCode(SubmitOrderStatusEnum.NOT_LOGIN.value());
			return message;
		}
		//根据您的下单
		if(typeEnum==null){
			message.setCode(SubmitOrderStatusEnum.PARAM_ERR.value());
			message.setMessage("参数有误");
			return message;
		}
		// 取用户提交过来的token进行对比
		Integer userToken = Integer.parseInt(paramDto.getToken());
		if(AppUtils.isBlank(sessionToken)){
			message.setCode(SubmitOrderStatusEnum.NULL_TOKEN.value());
			message.setMessage("购物信息已失效, 请刷新页面重试!");
			return message;
		}
		if (!userToken.equals(sessionToken)){
			message.setCode(SubmitOrderStatusEnum.INVALID_TOKEN.value());
			message.setMessage("购物信息已失效, 请刷新页面重试!");
			return message;
		}
		return  message;
	}

	/**
	 * 处理购物车，按门店分组 TODO
	 * @param shopCartList
	 */
	private void handlerUserShopCartList(UserShopCartList shopCartList) {

		ShopCarts shopCarts  = shopCartList.getShopCarts().get(0);
		List<ShopCartItem> shopCartItem = shopCarts.getCartItems();

		// 获取用户默认地址
		UserAddress userAddress = shopCartList.getUserAddress();

		if(AppUtils.isNotBlank(shopCartItem)){

			Set<Long> storeCartsSet = new HashSet<Long>();//购物车的storeId集合
			int size = shopCartItem.size();
			for (int i = 0; i < size; i++) {
				storeCartsSet.add(shopCartItem.get(i).getStoreId());
			}
			List <StoreCarts> storeCartList = new ArrayList<StoreCarts>();//按storeId来分

			for (Long storeId : storeCartsSet) {//按门店来分单

				//查询该门店的信息
				Store store = storeService.getStore(storeId);
				if(store == null){
					continue;
				}

				StoreCarts storeCart = new StoreCarts();

				// 组装门店信息
				storeCart.setStoreStatus(store.getIsDisable());
				storeCart.setStoreId(storeId);
				storeCart.setStoreName(store.getName());
				storeCart.setStoreAddr(store.getShopAddr());

				// 提货人信息取默认收货地址的信息
				if(AppUtils.isNotBlank(userAddress)){

					storeCart.setBuyerName(userAddress.getReceiver());
					storeCart.setTelphone(userAddress.getMobile());
				}

				List<ShopCartItem> cartItems = new ArrayList<ShopCartItem>();

				for (ShopCartItem cartItem : shopCartItem) {

					if (cartItem.getStoreId().equals(storeId)) {

						cartItems.add(cartItem);
					}
				}
				storeCart.setCartItems(cartItems);
				storeCartList.add(storeCart);
			}
			shopCarts.setStoreCarts(storeCartList);
		}
	}

}
