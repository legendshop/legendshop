package com.legendshop.app.dto;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

/**
 * app 文章Dto
 */
public class AppNewsDto {
	
	/** 文章ID */
	@ApiModelProperty(value="文章ID")  
	private Long newsId;

    /** 文章分类id */
	@ApiModelProperty(value="文章分类id")  
    private Long newsCategoryId;
    
    /** 文章分类名称 */
	@ApiModelProperty(value="文章分类名称")  
    private String newsCategoryName;
    
    /**文章栏目图片*/
	@ApiModelProperty(value="文章栏目图片")  
    private String newsCategoryPic;
    
    /** 文章标题 */
	@ApiModelProperty(value="文章标题 ")  
    private String newsTitle;

    /** 文章内容 */
	@ApiModelProperty(value="文章内容")  
    private String newsContent;

    /** 发表时间 */
	@ApiModelProperty(value="发表时间")  
    private Date newsDate;
    
    /** 1:上线 0：下线. */
	@ApiModelProperty(value="1:上线 0：下线")  
    private Integer status;
    
    /** 位置 *. */
	@ApiModelProperty(value="位置")  
    private Integer position;
    
	/** 是否高亮,1:yes,0:no */
	@ApiModelProperty(value="是否高亮,1:yes,0:no")  
    private Integer highLine;

    /** 用户ID */
	@ApiModelProperty(value="用户ID")  
    private String userId;

    /** 所属用户名称 */
	@ApiModelProperty(value="所属用户名称")  
    private String userName;
    
	/** 文章提要 */
	@ApiModelProperty(value="文章提要")  
    private String newsBrief;
    
	/** 顺序 */
	@ApiModelProperty(value="顺序")  
    private Long seq;
    
	/** 新闻编号 */
	@ApiModelProperty(value="新闻编号")  
    private Long positionId;
    
	/** 文章标签 */
	@ApiModelProperty(value="文章标签")  
    private String newsTags;
    
	/** 新闻标签 */
	@ApiModelProperty(value="新闻标签")  
    private String positionTags;

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public Long getNewsCategoryId() {
		return newsCategoryId;
	}

	public void setNewsCategoryId(Long newsCategoryId) {
		this.newsCategoryId = newsCategoryId;
	}

	public String getNewsCategoryName() {
		return newsCategoryName;
	}

	public void setNewsCategoryName(String newsCategoryName) {
		this.newsCategoryName = newsCategoryName;
	}

	public String getNewsCategoryPic() {
		return newsCategoryPic;
	}

	public void setNewsCategoryPic(String newsCategoryPic) {
		this.newsCategoryPic = newsCategoryPic;
	}

	public String getNewsTitle() {
		return newsTitle;
	}

	public void setNewsTitle(String newsTitle) {
		this.newsTitle = newsTitle;
	}

	public String getNewsContent() {
		return newsContent;
	}

	public void setNewsContent(String newsContent) {
		this.newsContent = newsContent;
	}

	public Date getNewsDate() {
		return newsDate;
	}

	public void setNewsDate(Date newsDate) {
		this.newsDate = newsDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public Integer getHighLine() {
		return highLine;
	}

	public void setHighLine(Integer highLine) {
		this.highLine = highLine;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNewsBrief() {
		return newsBrief;
	}

	public void setNewsBrief(String newsBrief) {
		this.newsBrief = newsBrief;
	}

	public Long getSeq() {
		return seq;
	}

	public void setSeq(Long seq) {
		this.seq = seq;
	}

	public Long getPositionId() {
		return positionId;
	}

	public void setPositionId(Long positionId) {
		this.positionId = positionId;
	}

	public String getNewsTags() {
		return newsTags;
	}

	public void setNewsTags(String newsTags) {
		this.newsTags = newsTags;
	}

	public String getPositionTags() {
		return positionTags;
	}

	public void setPositionTags(String positionTags) {
		this.positionTags = positionTags;
	}
	
}
