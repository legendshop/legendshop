package com.legendshop.app.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value="AppProdDto商品") 
public class AppProdDto implements Serializable{
	private static final long serialVersionUID = 5049919089101634020L;

	/** 商品唯一ID */
	@ApiModelProperty(value=" 商品唯一ID")  
	private Long prodId;
	
	/** 商品名称 */
	@ApiModelProperty(value="商品名称")  
	private String name;
	
	/** 商品图片 */
	@ApiModelProperty(value="商品图片")  
	private String pic;
	
	/** 商品现价 */
	@ApiModelProperty(value="商品现价")  
	private Double cash;
	
	/** 商品价格 */
	@ApiModelProperty(value="商品价格")  
	private Double price;
	
	/** 已售商品数量 */
	@ApiModelProperty(value="已售商品数量")  
	private Long buys;
	
	/** 商品评论数 */
	@ApiModelProperty(value="商品评论数")  
	private Long comments;
	
	/** 商品状态   1 正常   其他均为下线**/
	@ApiModelProperty(value="商品状态   1 正常   其他均为下线")  
	private Integer status;	
	
	/** 商品类型，P.普通商品，G:团购商品，S:二手商品，D:打折商品. */
	@ApiModelProperty(value="商品类型，P.普通商品，G:团购商品，S:二手商品，D:打折商品")  
	private String prodType;
	
	/** 店铺类型0.专营店1.旗舰店2.自营店**/
	@ApiModelProperty(value="店铺类型0.专营店1.旗舰店2.自营店")  
	private Integer shopType;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
	public Long getBuys() {
		return buys;
	}

	public void setBuys(Long buys) {
		this.buys = buys;
	}

	public Long getComments() {
		return comments;
	}

	public void setComments(Long comments) {
		this.comments = comments;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getProdType() {
		return prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public Integer getShopType() {
		return shopType;
	}

	public void setShopType(Integer shopType) {
		this.shopType = shopType;
	}

	@Override
	public String toString() {
		return "AppProdDto [prodId=" + prodId + ", name=" + name + ", pic=" + pic + ", cash=" + cash + ", price="
				+ price + ", buys=" + buys + ", comments=" + comments + ", status=" + status + ", prodType=" + prodType
				+ ", shopType=" + shopType + "]";
	}
	
}
