package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.AppUserAddressService;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AddressDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.spi.service.UserAddressService;
import com.legendshop.util.AppUtils;

/**
 * 用户地址服务
 *
 */
@Service("appUserAddressService")
public class AppUserAddressServiceImpl implements AppUserAddressService{

	@Autowired
	private UserAddressService userAddressService;

	/**
	 * 分页查询地址
	 */
	@Override
	public AppPageSupport<AddressDto> queryUserAddress(String curPageNO, String userId) {
		PageSupport<UserAddress> ps = userAddressService.queryUserAddress(userId, curPageNO, 7);
		return convertPageSupportDto(ps);
	}
	
	private AppPageSupport<AddressDto> convertPageSupportDto(PageSupport<UserAddress> pageSupport){
		if(AppUtils.isNotBlank(pageSupport)){
			AppPageSupport<AddressDto> pageSupportDto = new AppPageSupport<AddressDto>();
			pageSupportDto.setTotal(pageSupport.getTotal());
			pageSupportDto.setCurrPage(pageSupport.getCurPageNO());
			pageSupportDto.setPageSize(pageSupport.getPageSize());
			List<AddressDto> addressDtoList = convertAddressDtoList((List<UserAddress>) pageSupport.getResultList());
			pageSupportDto.setResultList(addressDtoList);
			pageSupportDto.setPageCount(pageSupport.getPageCount());
			return pageSupportDto;
		}
		return null;
	}

	private List<AddressDto> convertAddressDtoList(List<UserAddress> userAddressList){
		if(AppUtils.isNotBlank(userAddressList)){
			List<AddressDto> dtoList = new ArrayList<AddressDto>();
			for (UserAddress userAddress : userAddressList) {
				AddressDto addressDto = new AddressDto();
				addressDto.setAddrId(userAddress.getAddrId());
				addressDto.setReceiver(userAddress.getReceiver());
				addressDto.setSubPost(userAddress.getSubPost());
				addressDto.setAliasAddr(userAddress.getAliasAddr());
				addressDto.setTelphone(userAddress.getTelphone());
				addressDto.setCommonAddr(userAddress.getCommonAddr());
				addressDto.setEmail(userAddress.getEmail());
				addressDto.setUserId(userAddress.getUserId());
				addressDto.setUserName(userAddress.getUserName());
				addressDto.setProvinceId(userAddress.getProvinceId());
				addressDto.setCityId(userAddress.getCityId());
				addressDto.setAreaId(userAddress.getAreaId());
				addressDto.setProvince(userAddress.getProvince());
				addressDto.setCity(userAddress.getCity());
				addressDto.setArea(userAddress.getArea());
				addressDto.setMobile(userAddress.getMobile());
				addressDto.setSubAdds(userAddress.getSubAdds());
				dtoList.add(addressDto);
			}
			return dtoList;
		}
		return null;
	}

	@Override
	public AddressDto getUserAddressDto(Long addrId) {
			UserAddress userAddress = userAddressService.getUserAddress(addrId);
			if(userAddress != null){
				return convertAddressDto(userAddress);
			}
		return null;
	}
	
	public AddressDto convertAddressDto(UserAddress userAddress){
		if(AppUtils.isNotBlank(userAddress)){
			AddressDto addressDto = new AddressDto();
			addressDto.setAddrId(userAddress.getAddrId());
			addressDto.setReceiver(userAddress.getReceiver());
			addressDto.setSubPost(userAddress.getSubPost());
			addressDto.setAliasAddr(userAddress.getAliasAddr());
			addressDto.setTelphone(userAddress.getTelphone());
			addressDto.setCity(userAddress.getCity());
			addressDto.setArea(userAddress.getArea());
			addressDto.setProvinceId(userAddress.getProvinceId());
			addressDto.setCityId(userAddress.getCityId());
			addressDto.setAreaId(userAddress.getAreaId());
			addressDto.setCommonAddr(userAddress.getCommonAddr());
			addressDto.setEmail(userAddress.getEmail());
			addressDto.setUserId(userAddress.getUserId());
			addressDto.setProvince(userAddress.getProvince());
			addressDto.setUserName(userAddress.getUserName());
			addressDto.setMobile(userAddress.getMobile());
			addressDto.setSubAdds(userAddress.getSubAdds());
			return addressDto;
		}
		
		return null;
	}
}
