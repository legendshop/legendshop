/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service;

import com.legendshop.app.dto.AppCommisChangeLogDto;
import com.legendshop.model.dto.app.AppPageSupport;

/**
 * 佣金变更历史Service.
 */
public interface AppCommisChangeLogService  {

	/** 
	 * 获取佣金变更历史 
	 */
	public AppPageSupport<AppCommisChangeLogDto> getCommisChangeLog(int pageSize, String userId, String curPageNO);
	
}
