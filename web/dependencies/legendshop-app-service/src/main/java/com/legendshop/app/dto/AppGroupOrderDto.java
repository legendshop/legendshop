package com.legendshop.app.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.legendshop.model.dto.app.cartorder.ShopCartOrderDto;
import com.legendshop.model.entity.Invoice;
import com.legendshop.model.entity.UserAddress;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * APP 用户的购物车订单信息
 * @author Tony
 */
@ApiModel(value="UserShopCartOrderDto用户的购物车订单信息") 
public class AppGroupOrderDto implements Serializable{

	@ApiModelProperty(value="团购Id") 
	private Long groupId; 
	
	@ApiModelProperty(value="默认发票") 
	private Invoice defaultInvoice; 
    
	@ApiModelProperty(value="用户地址") 
	private UserAddress userAddress;  
	
	/** token， 防止重复提交. */
	@ApiModelProperty(value = "token， 防止重复提交")
	private String token;
	
	public Invoice getDefaultInvoice() {
		return defaultInvoice;
	}
	
	public AppGroupOrderDto() {
	}

	public void setDefaultInvoice(Invoice defaultInvoice) {
		this.defaultInvoice = defaultInvoice;
	}

	public UserAddress getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(UserAddress userAddress) {
		this.userAddress = userAddress;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	
}
