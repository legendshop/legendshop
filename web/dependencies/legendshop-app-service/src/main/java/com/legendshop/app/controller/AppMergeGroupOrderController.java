/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.constant.*;
import com.legendshop.spi.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.AppOrderDetailsParamsDto;
import com.legendshop.app.dto.AppOrderPayParamDto;
import com.legendshop.app.dto.AppUserShopCartOrderDto;
import com.legendshop.app.service.AppCartOrderService;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.framework.cache.client.CacheClient;
import com.legendshop.model.dto.OrderAddParamDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.order.AddOrderMessage;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.MergeGroup;
import com.legendshop.model.entity.MergeGroupOperate;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.processor.DelayCancelOfferProcessor;
import com.legendshop.processor.ProdSnapshotProcessor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 拼团订单
 */
@Api(tags="拼团下单",value="拼团下单接口, 参团或开团")
@RestController
@RequestMapping
public class AppMergeGroupOrderController {
	
	private final Logger log = LoggerFactory.getLogger(AppMergeGroupOrderController.class);
	
	/** 防止表单重复提交的Session token 失效时间 */
	private static final int SESSION_TOKEN_EXPIRE = 60 * 10;
	
	/** 订单处理器封装器 */
	@Autowired
	private OrderCartResolverManager orderCartResolverManager;
	
	@Autowired
	private MergeGroupOperateService mergeGroupOperateService;
	
	@Autowired
	private SubService subService;
	
	// 订单常用处理工具类
	@Autowired
	private OrderUtil orderUtil;
	
	@Autowired
	private UserAddressService userAddressService;
	
	/** 订单延迟取消队列 **/
	@Autowired
	private DelayCancelOfferProcessor delayCancelOfferProcessor;
	
	/** 订单快照 **/
	@Autowired
	private ProdSnapshotProcessor prodSnapshotProcessor;
	
	@Autowired
	private MergeGroupService mergeGroupService;
	
	@Autowired
	private AppCartOrderService appCartOrderService;
	
	@Autowired
	private CacheClient cacheClient;

	@Autowired
	private UserDetailService userDetailService;
	
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;
	
	/**
	 * 提交订单的详情
	 * @param reqParams
	 * @return
	 */
	@ApiOperation(value = "提交订单的详情", httpMethod = "POST", notes = "提交订单的详情" )
	@PostMapping(value = "/p/app/mergeGroup/orderDetails")
	public ResultDto<AppUserShopCartOrderDto> mergeGroupOrderDetails(AppOrderDetailsParamsDto reqParams) {
		
		try{
			LoginedUserInfo appToken = loginedUserService.getUser();
			
			String userId = appToken.getUserId();
			String userName = appToken.getUserName();
			
			// 参团订单需要判断，校检是否成团人数是否满足
			String checkResult = this.checkMergeGroupParams(reqParams, userId);
			if(!Constants.SUCCESS.equals(checkResult)){
				return ResultDtoManager.fail(0, checkResult);
			}
			
			/* 根据类型查询订单列表 */
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("mergeGroupId", reqParams.getActiveId());
			params.put("productId", reqParams.getProdId());
			params.put("skuId", reqParams.getSkuId());
			params.put("number", reqParams.getCount());
			params.put("userId", userId);
			params.put("userName", userName);
			
			CartTypeEnum typeEnum = CartTypeEnum.MERGE_GROUP;

			UserShopCartList shopCartList = orderCartResolverManager.queryOrders(typeEnum, params);
			if (AppUtils.isBlank(shopCartList)) {
				return ResultDtoManager.fail(0, "找不到您要购买的商品! ");
			}
			
			if (shopCartList == null || AppUtils.isBlank(shopCartList.getShopCarts())) {
				return ResultDtoManager.fail(0, "找不到您要购买的商品! ");
			}
			
			AppUserShopCartOrderDto shopCartOrderDto = appCartOrderService.getMergeGroupShoppingCartOrderDto(shopCartList, reqParams, userId);
			if(null == shopCartOrderDto){
				return ResultDtoManager.fail(0, "找不到您要购买的商品! ");
			}
			
			// 设置订单提交令牌,防止重复提交
			Integer token = CommonServiceUtil.generateRandom();
			shopCartOrderDto.setToken(token+"");
			cacheClient.put(Constants.APP_FROM_SESSION_TOKEN + ":" + typeEnum.toCode() + ":" + userId, SESSION_TOKEN_EXPIRE, token);
			
			subService.putUserShopCartList(CartTypeEnum.MERGE_GROUP.toCode(), userId, shopCartList);
			
			return ResultDtoManager.success(shopCartOrderDto);
		}catch(Exception e){
			e.printStackTrace();
			return ResultDtoManager.fail(0, "系统错误, 下单失败, 请联系平台客服! ");
		}
	}
	
	@ApiOperation(value = "提交拼团订单", httpMethod = "POST", notes = "提交订单的详情" )
	@PostMapping(value="/p/app/mergeGroup/submitOrder")
	public ResultDto<AppOrderPayParamDto> submitOrder(HttpServletRequest request, HttpServletResponse response,OrderAddParamDto paramDto) {
		
		LoginedUserInfo appToken = loginedUserService.getUser();
		String userId = appToken.getUserId();
		String userName = appToken.getUserName();
		
		CartTypeEnum typeEnum = CartTypeEnum.MERGE_GROUP;
		
		Integer sessionToken = cacheClient.get(Constants.APP_FROM_SESSION_TOKEN + ":" + typeEnum.toCode() + ":" + userId);
		AddOrderMessage message = orderUtil.checkSubmitParam(userId, sessionToken, typeEnum, paramDto);//检查输入参数
		if(message.hasError()){//有错误就返回
			return ResultDtoManager.fail(0, message.getMessage());
		}

		// TODO 校验用户参数，SpringSession后台过期机制有问题
		if (!userDetailService.isUserExist(userName)) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("用户不存在！");
			return ResultDtoManager.fail(0, message.getMessage());
		}
		
		//get cache 从上个页面获取设置进入缓存的订单
		UserShopCartList userShopCartList = subService.findUserShopCartList(typeEnum.toCode(), userId);
		if(AppUtils.isBlank(userShopCartList)){
			return ResultDtoManager.fail(0,"没有购物清单");
		}
		
		//检查金额： 如果促销导致订单金额为负数
	    if(userShopCartList.getOrderActualTotal()< 0){
	    	return ResultDtoManager.fail(0,"订单金额为负数,下单失败!");
	    }
	    
	  //处理订单的卖家留言备注
		try {
			orderUtil.handlerRemarkText(paramDto.getRemarkText(), userShopCartList);
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResultDtoManager.fail(0,"下单失败，请联系商城管理员或商城客服");
		}

		/** 根据id获取收货地址信息 **/
		UserAddress userAddress = userShopCartList.getUserAddress();
		if (AppUtils.isBlank(userAddress)) {
			userAddress = userAddressService.getDefaultAddress(userId);
			if (AppUtils.isBlank(userAddress)) {
				return ResultDtoManager.fail(0,"请选择或添加您的收货地址！");
			}
			userShopCartList.setUserAddress(userAddress);
		}
			
		// 处理运费模板
		String result = orderUtil.handlerDelivery(paramDto.getDelivery(), userShopCartList);
		if (!Constants.SUCCESS.equals(result)) {
			return ResultDtoManager.fail(0,"运费处理失败, 请联系商城管理员或商城客服！");
		}
			
		// 处理优惠券
		orderUtil.handleCouponStr(userShopCartList);

		// 验证成功，清除令牌
		request.getSession().removeAttribute(Constants.TOKEN);
		userShopCartList.setUserId(userId);
		userShopCartList.setUserName(userName);
		userShopCartList.setInvoiceId(paramDto.getInvoiceId());
		userShopCartList.setUserAddress(userAddress);
		userShopCartList.setPayManner(paramDto.getPayManner());
		userShopCartList.setType(typeEnum.toCode());
		userShopCartList.setSwitchInvoice(paramDto.getSwitchInvoice());//当前订单所属商家是否开启发票

		/**
		 * 处理订单
		 */
		List<String> subNumberList = null;
		try {
			subNumberList = orderCartResolverManager.addOrder(typeEnum,userShopCartList);
			
			if (AppUtils.isBlank(subNumberList)) {
				return ResultDtoManager.fail(0,"下单失败，请联系商城管理员或商城客服");
			}
			
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResultDtoManager.fail(0,"下单失败，请联系商城管理员或商城客服");
		}
			
        //offer 延时取消队列
  		delayCancelOfferProcessor.process(subNumberList);
        
		//发送网站快照备份
		prodSnapshotProcessor.process(subNumberList);

		// 标记回收
		userShopCartList = null;
		// 清空缓存
		subService.evictUserShopCartCache(typeEnum.toCode(), userId);
		// 验证成功，清除令牌
		cacheClient.delete(Constants.APP_FROM_SESSION_TOKEN + ":" + typeEnum.toCode() + ":" + userId);
		
		final String [] subNumbers=new String[subNumberList.size()];
		for (int i = 0; i < subNumberList.size(); i++) {
			subNumbers[i] = subNumberList.get(i);
		}
		
		//根据订单IDs获取订单集合
		List<Sub> subs = subService.getSubBySubNums(subNumbers, userId, OrderStatusEnum.UNPAY.value());
		Double totalAmount = 0.0;//总金额
		String subject = ""; //商品描述
		StringBuffer sub_number = new StringBuffer(""); //订单流水号
		for (Sub sub : subs) {
			totalAmount= Arith.add(totalAmount,sub.getActualTotal());
			sub_number.append(sub.getSubNumber()).append(",");
			subject += subject+sub.getProdName()+",";
		}
		String subNumber = sub_number.substring(0, sub_number.length() - 1);
		
		AppOrderPayParamDto orderPay = new AppOrderPayParamDto();
		orderPay.setTotalAmount(totalAmount);
		orderPay.setSubNumber(subNumber);
		orderPay.setSubjects(subject);
		orderPay.setSubSettlementType(SubSettlementTypeEnum.USER_ORDER_PAY.value());
		
		return ResultDtoManager.success(orderPay);
	}
	

	/**
	 * 检查拼团活动, 是否可以参团或开团
	 * @param activeId 拼团活动ID
	 * @param addNumber 团编号, 参团的时候才有
	 * @param shopId 商城ID
	 * @param userId 当前登录用户ID
	 * @return
	 */
	private String checkMergeGroupParams(AppOrderDetailsParamsDto reqParams, String userId) {
		
		if(AppUtils.isBlank(reqParams.getType()) 
				|| AppUtils.isBlank(reqParams.getActiveId())
				|| AppUtils.isBlank(reqParams.getProdId())
				|| AppUtils.isBlank(reqParams.getSkuId())
				|| AppUtils.isBlank(reqParams.getCount())){
			return "非法参数";
		}
		
		if (!CartTypeEnum.MERGE_GROUP.toCode().equals(reqParams.getType())) {
			return "非法参数! ";
		}
		
		if(reqParams.getCount() <= 0 ){
			return "购买数量不能小于或等于0! ";
		}
		
		MergeGroup mergeGroup = mergeGroupService.getMergeGroup(reqParams.getActiveId());
		if (null == mergeGroup) {
			return "该拼团活动不存在, 返回换个团试试吧!";
		}
		
		long currentTime = System.currentTimeMillis();
		
		if (AppUtils.isNotBlank(reqParams.getAddNumber())) {// 说明是参团
			
			MergeGroupOperate mergeGroupOperate = mergeGroupOperateService.getMergeGroupOperateByAddNumber(reqParams.getAddNumber());
			if (AppUtils.isBlank(mergeGroupOperate)) {
				return "您要参与的团不存在, 返回换个团试试吧!";
			}
			
			if(MergeGroupOperateStatusEnum.FAIL.value().equals(mergeGroupOperate.getStatus())){
				return "您要参与的团已过期, 返回换个团试试吧!";
			}
			
			if(mergeGroupOperate.getEndTime().getTime() <= currentTime){
				return "您要参与的团已过期, 返回换个团试试吧!";
			}
			
			// 校检用户是否参加了该团
			Long count = mergeGroupOperateService.getMergeGroupAddByUserId(userId, reqParams.getAddNumber());
			if (count > 0) {
				return "您已参加该团, 返回换个团试试吧!";
			}
			
			if(MergeGroupOperateStatusEnum.SUCCESS.value().equals(mergeGroupOperate.getStatus()) 
					|| mergeGroup.getPeopleNumber() == mergeGroupOperate.getNumber()){
				return "您要参与的团已满人, 返回换个团试试吧!";
			}
			
		}else{//说明是开团
			
			if(!MergeGroupStatusEnum.ONLINE.value().equals(mergeGroup.getStatus())){//如果不是正常上线状态
				return "该拼团活动未上线或已过期, 返回换个团试试吧!";
			}
			
			if(mergeGroup.getEndTime().getTime() <= currentTime ){
				return "该拼团活动已过期, 返回换个团试试吧!";
			}
		}
		
		return Constants.SUCCESS;
	}
}
