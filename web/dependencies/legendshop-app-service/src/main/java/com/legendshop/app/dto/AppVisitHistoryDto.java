package com.legendshop.app.dto;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 我的足迹
 */
@ApiModel(value="我的足迹数据") 
public class AppVisitHistoryDto {

	@ApiModelProperty(value = "记录ID, 唯一标识")
	private Long visitId;
	
	@ApiModelProperty(value = "商品ID, 用于跳转商品详情页面")
	private Long prodId;
	
	@ApiModelProperty(value = "商品图片")
	private String pic;

	@ApiModelProperty(value = "商品名称")
	private String prodName;
	
	@ApiModelProperty(value = "商品价格")
	private Double price;

	@ApiModelProperty(value = "访问时间")
	private Date recDate;

	@ApiModelProperty(value = "访问次数", hidden=true)
	private Integer visitNum;

	@ApiModelProperty(value = "访问来源", hidden=true)
	private String source;

	public Long getVisitId() {
		return visitId;
	}

	public void setVisitId(Long visitId) {
		this.visitId = visitId;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}
	
	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	public Integer getVisitNum() {
		return visitNum;
	}

	public void setVisitNum(Integer visitNum) {
		this.visitNum = visitNum;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
}
