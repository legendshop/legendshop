package com.legendshop.app.service;

import java.util.List;

import com.legendshop.model.dto.app.KeyValueDto;

public interface AppLocationService {

	public List<KeyValueDto> loadProvinces();
	
	public List<KeyValueDto> loadCities(Integer provinceid);
	 
	public List<KeyValueDto> loadAreas(Integer cityid);
}
