/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.app.controller;

import java.util.Calendar;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.constant.AppTokenTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.support.SimpleValueWrapper;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.LoginUserTimes;
import com.legendshop.app.service.AppTokenService;
import com.legendshop.app.service.impl.AppLoginUtil;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.ShortMessageInfo;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.base.util.ValidationUtil;
import com.legendshop.framework.cache.LegendCacheManager;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.PassportFull;
import com.legendshop.model.dto.app.AppTokenDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.AppToken;
import com.legendshop.model.entity.User;
import com.legendshop.model.form.UserMobileForm;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.ThirdLoginService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.DateUtil;
import com.legendshop.web.helper.IPHelper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 用户登录相关接口
 */
@RestController
@Api(tags="用户登录",value="用户登录相关接口")
public class AppTokenController {

	private static Logger log = LoggerFactory.getLogger(AppTokenController.class);

	@Autowired
	private AppTokenService appTokenService;

	@Autowired
	private UserDetailService userDetailService;

	/** The legend cache manager. */
	@Autowired
	private LegendCacheManager legendCacheManager;

	private Cache INVALID_LOGIN_USER_CACHE = null;

	@Autowired
	private AppLoginUtil appLoginUtil;

	/**
	* 短信发送者
	*/
	@Resource(name = "sendSMSProcessor")
    private EventProcessor<ShortMessageInfo> sendSMSProcessor;

    @Autowired
    private PasswordEncoder passwordEncoder;

	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private ThirdLoginService thridLoginService;

	/**
	 * 用户账号密码登录
	 * @param loginName 登录名字： 可以是手机，昵称或者邮件
	 * @param password 密码
	 * @param deviceId 手机的唯一设备
	 * @param platform Vue
	 * @param verId 版本号
	 * @return ResultDto 返回结果
	 */
	@ApiOperation(value = "用户账号密码登录", httpMethod = "POST", notes = "用户账号密码登录, 如果登录失败则返回错误消息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "loginName", value = "登录账号, 可以是手机号, 用户名, 邮箱", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "password", value = "登录密码", required = true, dataType = "String"),
		@ApiImplicitParam(paramType="query", name = "deviceId", value = "手机的唯一设备, APP端才需要传", required = false, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "platform", value = "设备平台类型, H5: 手机网页, ANDROID: 安卓APP, IOS: 苹果APP, MP: 小程序", required = false, dataType = "string"),
		@ApiImplicitParam(paramType="passportIdKey", name = "verId", value = "通行证密钥", required = false, dataType = "String")
	})
	@PostMapping(value="/login")
	public ResultDto<AppTokenDto> login(HttpServletRequest request,
			@RequestParam(required = true) String loginName,
			@RequestParam(required = true)  String password,
			String passportIdKey,
			String platform) {
			try{

				//如果用户有传 passportKey
				PassportFull passport = null;
				if(AppUtils.isNotBlank(passportIdKey)){
					//检查是否有通行证
					passport = thridLoginService.checkPassportIdKey(passportIdKey);
					if(null == passport){
						return ResultDtoManager.fail(-2, "对不起, 您的授权凭证无效, 请重新授权!");
					}
				}

				boolean canuserLogin = isUserCanLogin(loginName);
				if(!canuserLogin){
					return ResultDtoManager.fail(-1, "登录错误次数过多, 请一小时后再重试!");
				}

				String name = userDetailService.convertUserLoginName(loginName);
				if(AppUtils.isBlank(name)){
					addUserTimes(loginName);//记录用户的登录出错次数
					return ResultDtoManager.fail(-1, "用户不存在!");
				}

				User user = userDetailService.getUserByName(name);
				if(null == user || "0".equals(user.getEnabled())){
					addUserTimes(loginName);//记录用户的登录出错次数
					return ResultDtoManager.fail(-1, "用户不存在或已被冻结!");
				}

				if (!passwordEncoder.matches(password, user.getPassword())) {
					addUserTimes(loginName);//记录用户的登录出错次数
					return ResultDtoManager.fail(-1, "密码不正确!");
				}

				String openId = null;
				if(null != passport){
					openId = passport.getOpenId();
					thridLoginService.bindAccount(user, passport);
				}

				//清除缓存中的PassportID
				if(AppUtils.isNotBlank(passportIdKey)){
					thridLoginService.removePassportIDFromCache(passportIdKey);
				}

				AppToken token= appLoginUtil.loginToken(user.getId(), user.getUserName(), null, platform, "1.0", IPHelper.getIpAddr(request), openId);
				if(AppUtils.isBlank(token)){
					return ResultDtoManager.fail(-1, "登录失败, 请联系管理人员!");
				}

				return ResultDtoManager.success(token.toDto());
			}catch (Exception e) {
				log.error("用户登录异常, 登录参数: loginName: {}; ---->> exception: {}", loginName, e);
				return ResultDtoManager.fail(-1, "登录失败, 请联系管理人员!");
			}
	}

	/**
	 * 手机短信验证码登录
	 * @param request
	 * @param mobile 手机号码
	 * @param mobileCode 手机短信验证码
	 * @param deviceId
	 * @param platform
	 * @param verId
	 * @return
	 */
	@ApiOperation(value = "手机短信验证码登录", httpMethod = "POST", notes = "手机短信验证码登录",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "mobile", value = "手机号码", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "mobileCode", value = "验证码", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "deviceId", value = "手机的唯一设备", required = false, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "platform", value = "设备平台类型, H5: 手机网页, ANDROID: 安卓APP, IOS: 苹果APP, MP: 小程序", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "passportIdKey", value = "通行证密钥", required = false, dataType = "string")
	})
	@PostMapping(value="/smsLogin")
	public ResultDto<AppTokenDto> smsLogin(HttpServletRequest request,
			@RequestParam String mobile, @RequestParam  String mobileCode,
			String deviceId, String platform, String passportIdKey) {

		try{

			//如果用户有传 passportKey
			PassportFull passport = null;
			if(AppUtils.isNotBlank(passportIdKey)){
				//检查是否有通行证
				passport = thridLoginService.checkPassportIdKey(passportIdKey);
				if(null == passport){
					return ResultDtoManager.fail(-2, "对不起, 您的授权凭证无效, 请重新授权!");
				}
			}

			//检查手机号码的格式
			boolean isMobile = ValidationUtil.checkPhone(mobile);
			if(!isMobile){
				return ResultDtoManager.fail(-1, "请输入正确的手机号码");
			}

			//检查登录次数是够满足
			boolean canuserLogin = isUserCanLogin(mobile);
			if(!canuserLogin){
				return ResultDtoManager.fail(-1, "登录错误次数过多, 请一小时后再重试!");
			}

			//检查验证码是否正确
			boolean mobileResult=userDetailService.verifyMobileCode(mobile, mobileCode);
			if(!mobileResult){
				//记录用户的登录出错次数
				addUserTimes(mobile);
				return ResultDtoManager.fail(-1, "验证码不正确");
			}

			User user =userDetailService.getUserByMobile(mobile);
			//短信注册,如果用户不存在则直接新增一个用户
			if(null == user){
				UserMobileForm userForm = new UserMobileForm();
				userForm.setMobile(mobile);
				String nickName = "手机尾号" + mobile.substring(7) + "用户";
				userForm.setNickName(nickName);
				userForm.setActivated(true);
				user = userDetailService.saveUserReg(IPHelper.getIpAddr(request), userForm, "");

//				if(null == user){
//					userDetailService.updatePaypassVerifn(user.getUserName());
//				}
			}

			if("0".equals(user.getEnabled())){
				return ResultDtoManager.fail(-1, "用户已被冻结,请联系平台客服!");
			}

			String openId = null;
			if(null != passport){
				openId = passport.getOpenId();
				thridLoginService.bindAccount(user, passport);
			}

			//清除缓存中的PassportID
			if(AppUtils.isNotBlank(passportIdKey)){
				thridLoginService.removePassportIDFromCache(passportIdKey);
			}

			//登录, 如果用户不存在,则新建token, 否则修改token
			AppToken token= appLoginUtil.loginToken(user.getId(), user.getUserName(), null, platform, "1.0", IPHelper.getIpAddr(request), openId);
			if(AppUtils.isBlank(token)){
				return ResultDtoManager.fail(-1, "登录失败, 请联系平台客服!");
			}

			//登入成功把验证码过期
			userDetailService.expiredVerificationCode(mobile, mobileCode);

			return ResultDtoManager.success(token.toDto());
		}catch(Exception e){
			log.error("用户登录异常, 登录参数: mobile: {},mobileCode:{}, deviceId: {}, platform: {}; ---->> exception: {}", mobile,mobileCode,deviceId, platform);
			return ResultDtoManager.fail(-1, "登录失败, 请联系平台客服!");
		}
	}



	/**
	 * 绑定手机号
	 * @param code
	 * @param encryptedData
	 * @param iv
	 * @return
	 */
	@ApiOperation(value = "绑定手机号", notes = "绑定手机号, 绑定成功后会返回token信息前端完成登录", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "passportIdKey", paramType = "query", value = "通行证密钥, 在微信授权登录后可以获得.", required = true, dataType = "long"),
			@ApiImplicitParam(name = "phone", paramType = "query", value = "手机号码", required = true, dataType = "String"),
			@ApiImplicitParam(name = "code", paramType = "query", value = "短信验证码", required = true, dataType = "String"),
			@ApiImplicitParam(name = "platform", paramType = "query", value = "设备平台类型, H5: 手机网页, ANDROID: 安卓APP, IOS: 苹果APP, MP: 小程序", required = true, dataType = "String")
	})
	@PostMapping(value = "/bindPhone")
	public ResultDto<AppTokenDto> bindPhone(HttpServletRequest request, @RequestParam(required = true) String passportIdKey,
			@RequestParam(required = true) String phone, @RequestParam(required = true) String code,@RequestParam(required = true) String platform) {

		try {

			// 如果用户有传 passportKey
			PassportFull passport = thridLoginService.checkPassportIdKey(passportIdKey);
			if(null == passport){
				return ResultDtoManager.fail(-2, "对不起, 您的授权凭证无效, 请重新授权!");
			}

			// 说明通行证已经绑定用户了
			if(AppUtils.isNotBlank(passport.getUserId())){
				return ResultDtoManager.fail(-1, "对不起, 这个微信已被绑定了!");
			}

			// 检查手机号码的格式
			boolean isMobile=ValidationUtil.checkPhone(phone);
			if(!isMobile){
				return ResultDtoManager.fail(-1, "请输入正确的手机号码");
			}

			// 检查验证码是否正确
			boolean mobileResult=userDetailService.verifyMobileCode(phone, code);
			if(!mobileResult){
				addUserTimes(phone);//记录用户的登录出错次数
				return ResultDtoManager.fail(-1, "验证码不正确");
			}


			User user =userDetailService.getUserByMobile(phone);
			if(null == user){//短信注册,如果用户不存在则直接新增一个用户
				UserMobileForm userForm = new UserMobileForm();
				userForm.setMobile(phone);
				String nickName = "手机尾号" + phone.substring(7) + "用户";
				userForm.setNickName(nickName);
				userForm.setActivated(true);
				user = userDetailService.saveUserReg(IPHelper.getIpAddr(request), userForm, "");

				if(null == user){
					return ResultDtoManager.fail(-1, "绑定失败, 请联系平台客服!");
				}

//				userDetailService.updatePaypassVerifn(user.getUserName());
			}

			if("0".equals(user.getEnabled())){
				return ResultDtoManager.fail(-1, "用户已被冻结,请联系平台客服!");
			}

			String bindResult  = thridLoginService.bindAccount(user, passport);
			if(!Constants.SUCCESS.equals(bindResult)){
				return ResultDtoManager.fail(-1, "绑定失败, 请联系平台客服!");
			}

			AppToken token= appLoginUtil.loginToken(user.getId(), user.getUserName(), null, platform, "1.0", IPHelper.getIpAddr(request), passport.getOpenId());
			if(AppUtils.isBlank(token)){
				return ResultDtoManager.fail(-1, "绑定失败, 请联系平台客服!");
			}

			//清除缓存中的PassportID
			if(AppUtils.isNotBlank(passportIdKey)){
				thridLoginService.removePassportIDFromCache(passportIdKey);
			}

			return ResultDtoManager.success(token.toDto());
		} catch(Exception e){
			log.error("用户绑定手机号异常!", e);
			return ResultDtoManager.fail(-1, "绑定失败, 请联系平台客服!");
		}
	}

	/**
	 * 退出
	 * @return ResultDto
	 */
	@ApiOperation(value = "退出登录", httpMethod = "POST", notes = "退出,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/p/logout")
	public ResultDto<String> delete(HttpServletRequest request, HttpServletResponse response, AppTokenDto dto) {
		LoginedUserInfo appToken = loginedUserService.getUser();
		if (appToken != null) {
			boolean result = appTokenService.deleteAppToken(appToken.getUserId(), appToken.getAccessToken());
			if (result) {
				return ResultDtoManager.success(Constants.SUCCESS);
			}
		}
		return ResultDtoManager.success(Constants.FAIL);
	}

	/**
	 * 刷新token，增强有效时间
	 *
	 * @param request the request
	 * @param response the response
	 * @return ResultDto
	 */
	@ApiOperation(value = "刷新token", httpMethod = "POST", notes = "刷新token，增强有效时间",produces = MediaType.APPLICATION_JSON_UTF8_VALUE, response=AppTokenDto.class)
	@PostMapping(value="/p/refresh")
	public ResultDto<AppTokenDto> refresh(HttpServletRequest request, HttpServletResponse response, AppTokenDto dto) {
		LoginedUserInfo token = loginedUserService.getUser();
		if (token != null) {
			AppToken appToken = appTokenService.getAppToken(token.getUserId(),dto.getPlatform(), AppTokenTypeEnum.USER.value());
			if (appToken != null) {
				// 获得随机码
				String securiyCode = CommonServiceUtil.getRandomString(8);
				appToken.setSecuriyCode(securiyCode);
				appToken.setStartDate(new Date());
				appTokenService.updateAppToken(appToken);
				return ResultDtoManager.success(appToken.toDto());
			}
		}
		return ResultDtoManager.fail(0,"用户没有登录");
	}


	/**
	 * 检查用户是否重复输入错误的密码,如果输入错误10次以上要锁定密码一个小时
	 * @return
	 */
	private boolean isUserCanLogin(String loginName){
		SimpleValueWrapper valueWrapper = (SimpleValueWrapper) getCache().get(loginName);
        if(valueWrapper==null){
        	return true;
        }
        LoginUserTimes times = (LoginUserTimes)valueWrapper.get();
        if(times != null){
        	if(times.getErrorNum() >= 10){//错误10次以上
        		Date oneHourLater = DateUtil.add(times.getLoginTime(), Calendar.HOUR, 1);
        		if(oneHourLater.after(new Date())){//还没有超过一个小时不能登录
        			return false;
        		}
        	}
        }
		return true;
	}

	private void addUserTimes(String loginName){
		SimpleValueWrapper valueWrapper = (SimpleValueWrapper) getCache().get(loginName);
        if(valueWrapper==null){
        	//第一次加入
        	LoginUserTimes times = new LoginUserTimes(new Date(),1);
        	getCache().put(loginName, times);
        }else{
        	LoginUserTimes times = (LoginUserTimes)valueWrapper.get();
        	times.setErrorNum(times.getErrorNum() + 1);
        	times.setLoginTime(new Date());
        	INVALID_LOGIN_USER_CACHE.put(loginName, times);//设置回去缓存
        }
	}

	/**
	 * 得到缓存
	 * @return
	 */
	private Cache getCache(){
		if(INVALID_LOGIN_USER_CACHE == null){
			INVALID_LOGIN_USER_CACHE = legendCacheManager.getCache("InvalidLoginUsers");
		}
		return INVALID_LOGIN_USER_CACHE;
	}


}
