/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;

import com.legendshop.model.dto.group.GroupDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app下单详情返回数据Dto
 *
 */
@ApiModel(value="app下单详情返回数据Dto")
public class GroupSubmitDetailDto {

	/** 团购id */
	@ApiModelProperty(value="团购id")
	private Long groupId;

	/** 团购加密Token. */
	@ApiModelProperty(value="团购加密Token")
	private String token;
	
	
	
	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}
