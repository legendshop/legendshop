package com.legendshop.app.service;

import java.util.List;

import com.legendshop.model.dto.app.shopcart.ShopCartItemDto;
import com.legendshop.model.dto.app.shopcart.UserShopCartListDto;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.UserShopCartList;

public interface AppCartService {
	
	/**
	 * APP 获取 用户 购物车信息
	 * @param cartItems
	 * @return
	 */
	public abstract UserShopCartListDto getShoppingCartsDto(List<ShopCartItem> cartItems);

	/**
	 * 转换成UserShopCartListDto对象
	 */
	public abstract UserShopCartListDto convertUserShopCartListDto(UserShopCartList userShopCartList);

	/**
	 * 获取失效商品集合
	 */
	public abstract List<ShopCartItemDto> InvalidListDto(List<ShopCartItem> cartItems);

}
