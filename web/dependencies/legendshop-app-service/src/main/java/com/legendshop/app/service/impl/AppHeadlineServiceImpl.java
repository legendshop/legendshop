package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.AppHeadlineDto;
import com.legendshop.app.service.AppHeadlineService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.Headline;
import com.legendshop.spi.service.HeadlineService;
import com.legendshop.util.AppUtils;

/**
 *  app头条新闻 service实现
 */

@Service("appHeadlineService")
public class AppHeadlineServiceImpl implements AppHeadlineService {

	@Autowired
	private HeadlineService headlineService;

	/**
	 * 获取头条新闻列表
	 */
	@Override
	public AppPageSupport<AppHeadlineDto> query(String curPageNO) {
		
		PageSupport<Headline> ps =  headlineService.query(curPageNO);
		
		//转成Dto
		AppPageSupport<AppHeadlineDto> pageSupportDto =  PageUtils.convertPageSupport(ps,
				new PageUtils.ResultListConvertor<Headline, AppHeadlineDto>() {

			@Override
			public List<AppHeadlineDto> convert(List<Headline> form) {
				
				List<AppHeadlineDto> toList = new ArrayList<AppHeadlineDto>();
				
				for (Headline headline : form) {
					if (AppUtils.isNotBlank(headline)) {
						
						AppHeadlineDto appHeadlineDto =  convertToAppHeadlineDto(headline);
						toList.add(appHeadlineDto);
					}
				}
				return toList;
			}
		});
		return pageSupportDto;
	}


	/**
	 * 获取头条新闻详情
	 */
	@Override
	public AppHeadlineDto getHeadline(Long id) {

		Headline headline = headlineService.getHeadline(id);
		if (AppUtils.isBlank(headline)) {
			return null;
		}
		//转成Dto
		return convertToAppHeadlineDto(headline);
	}
	
	//转换Dto方法
	private AppHeadlineDto convertToAppHeadlineDto(Headline headline){
		  AppHeadlineDto appHeadlineDto = new AppHeadlineDto();
		  appHeadlineDto.setAddTime(headline.getAddTime());
		  appHeadlineDto.setId(headline.getId());
		  appHeadlineDto.setDetail(headline.getDetail());
		  appHeadlineDto.setPic(headline.getPic());
		  appHeadlineDto.setTitle(headline.getTitle());
		  appHeadlineDto.setSeq(headline.getSeq());
		  return appHeadlineDto;
		}
}
