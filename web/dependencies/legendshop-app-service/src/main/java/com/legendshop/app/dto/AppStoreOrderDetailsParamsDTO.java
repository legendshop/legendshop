package com.legendshop.app.dto;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * 门店自提下单参数DTO
 */
@ApiModel("app门店自提下单详情参数")
public class AppStoreOrderDetailsParamsDTO {

	@ApiModelProperty(value = "购物车Id集合(商品详情下单不需要必填，购物车下单为必填)",required = true)
	private List<Long> shopCartItems;

	@ApiModelProperty(value = "是否立即下单（商品详情下单为true，购物车下单为false）",required = true)
	private Boolean buyNow;
	
	@ApiModelProperty(value = "商品Id",required = true)
	private Long prodId;
	
	@ApiModelProperty(value = "skuId",required = true)
	private Long skuId;
	
	@ApiModelProperty(value = "商品数量",required = true)
	private Integer count;
	
	@ApiModelProperty(value = "店铺Id",required = true)
	private Long shopId;
	
	/** 是否使用优惠券,yes or no */
	@ApiModelProperty(value = "是否使用优惠券,yes or no")
	private String couponBack;
	
	@ApiModelProperty(value = "经度",required = true)
	private Double lng;
	
	@ApiModelProperty(value = "纬度",required = true)
	private Double lat;
	
	@ApiModelProperty(value = "门店Id")
	private Long storeId;

	public List<Long> getShopCartItems() {
		return shopCartItems;
	}

	public void setShopCartItems(List<Long> shopCartItems) {
		this.shopCartItems = shopCartItems;
	}

	public Boolean getBuyNow() {
		return buyNow;
	}

	public void setBuyNow(Boolean buyNow) {
		this.buyNow = buyNow;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getCouponBack() {
		return couponBack;
	}

	public void setCouponBack(String couponBack) {
		this.couponBack = couponBack;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	
	
}
