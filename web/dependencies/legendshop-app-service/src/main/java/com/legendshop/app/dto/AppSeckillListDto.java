package com.legendshop.app.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="秒杀活动列表")
public class AppSeckillListDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/*@ApiModelProperty(value="秒杀活动分页")
	private AppPageSupport<AppSeckillActivityListDto> appPageResult;*/
	
	@ApiModelProperty(value="秒杀活动开始时间")
	private Long seckillStartTime;
	
	@ApiModelProperty(value="秒杀活动结束时间")
	private Long seckillEndTime;
	
	@ApiModelProperty(value="当前状态：活动结束为-1,进行中为1,未开始为0")
	private Integer status;

	public Long getSeckillStartTime() {
		return seckillStartTime;
	}

	public void setSeckillStartTime(Long seckillStartTime) {
		this.seckillStartTime = seckillStartTime;
	}

	public Long getSeckillEndTime() {
		return seckillEndTime;
	}

	public void setSeckillEndTime(Long seckillEndTime) {
		this.seckillEndTime = seckillEndTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
