package com.legendshop.app.controller;

import com.legendshop.app.dto.*;
import com.legendshop.app.service.AppGrassService;
import com.legendshop.business.service.weixin.WeixinMiniProgramApi;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.dto.weixin.WeiXinMsg;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.*;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SafeHtml;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Set;

@Api(tags = "种草社区", value = "种草社区相关接口")
@RestController()
public class AppGrassController {

    @Autowired
    private AppGrassService appGrassService;

    @Autowired
    private GrassArticleService grassArticleService;

    @Autowired
    private AttachmentManager attachmentManager;

    @Autowired
    private GrassProdService grassProdService;

    @Autowired
    private GrassArticleLableService grassArticleLableService;

    @Autowired
    private SensitiveWordService sensitiveWordService;

    @Autowired
    private UserDetailService userDetailService;

    @Autowired
    private LoginedUserService loginedUserService;

    @Autowired
    private GrassThumbService grassThumbService;

    @Autowired
    private GrassConcernService grassConcernService;

    @Autowired
    private GrassCommService grassCommService;

    @Autowired
    private GrassLabelService grassLabelService;

	@Autowired
    private WeixinMpTokenService weixinMpTokenService;


    @ApiOperation(value = "获取标签列表", httpMethod = "POST", notes = "获取标签列表数据", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "curPageNO", value = "当前页", dataType = "String", required = false),
            @ApiImplicitParam(paramType = "query", name = "title", value = "标签名", dataType = "String", required = false)})
    @PostMapping("/grassLabelList")
    public ResultDto<AppPageSupport<AppGrassLabelListDto>> queryLabel(String curPageNO, String title) {
        String index = AppUtils.isNotBlank(curPageNO) ? curPageNO : "1";
        AppPageSupport<AppGrassLabelListDto> appGrassLabels = appGrassService.queryLabel(index, title, 10);
        return ResultDtoManager.success(appGrassLabels);
    }



    @ApiOperation(value = "获取文章列表", httpMethod = "POST", notes = "获取文章列表数据按照点赞数排序", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "curPageNO", value = "当前页", dataType = "String", required = false),
            @ApiImplicitParam(paramType = "query", name = "condition", value = "作者名 或者 标题", dataType = "String", required = false),
            @ApiImplicitParam(paramType = "query", name = "order", value = "排序条件  综合synthesize 最热hot 最新new", dataType = "String", required = true)
    })
    @PostMapping("/grassList")
    public ResultDto<AppPageSupport<AppGrassArticleListDto>> queryArticleList(String curPageNO, String condition, String order) {
        String index = AppUtils.isNotBlank(curPageNO) ? curPageNO : "1";
        AppPageSupport<AppGrassArticleListDto> graSupport = appGrassService.queryArticle(index, 20, condition, order);
        return ResultDtoManager.success(graSupport);
    }



    @ApiOperation(value = "获取相关文章列表", httpMethod = "POST", notes = "获取文章列表数据按照点赞数排序", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "curPageNO", value = "当前页", dataType = "String", required = false),
            @ApiImplicitParam(paramType = "query", name = "labels", value = "标签集合", dataType = "Long[]", required = true),
    })
    @PostMapping("/grassRelatedList")
    public ResultDto<AppPageSupport<AppGrassArticleListDto>> queryRelatedList(String curPageNO, Long[] labels) {
        String index = AppUtils.isNotBlank(curPageNO) ? curPageNO : "1";
        AppPageSupport<AppGrassArticleListDto> graSupport = appGrassService.queryArticleByLabel(index, 20, labels);
        return ResultDtoManager.success(graSupport);
    }



    @ApiOperation(value = "获取用户购买商品列表", httpMethod = "POST", notes = "获取用户购买的商品", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "curPageNO", value = "当前页", dataType = "String", required = false),
    })
    @PostMapping("/grassUserProdList")
    public ResultDto<AppPageSupport<AppGrassProdDto>> grassUserProdList(String curPageNO) {
        String index = AppUtils.isNotBlank(curPageNO) ? curPageNO : "1";
        AppPageSupport<AppGrassProdDto> prodDtoAppPageSupport = appGrassService.queryUserProd(index, 20);
        return ResultDtoManager.success(prodDtoAppPageSupport);
    }


    @ApiOperation(value = "获取文章详情", httpMethod = "POST", notes = "获取文章详情数据", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParam(paramType = "query", name = "grassId", value = "文章id", dataType = "Long", required = true)
    @PostMapping("/grassInfo")
    public ResultDto<AppGrassArticleInfoDto> queryArticleInfo(Long grassId) {
        AppGrassArticleInfoDto appGrassArticleInfoDto = appGrassService.queryByDisId(grassId);
        if (appGrassArticleInfoDto == null) {
            return ResultDtoManager.fail("文章不存在，或已下架");
        }
        return ResultDtoManager.success(appGrassArticleInfoDto);
    }


    @ApiOperation(value = "获取文章评论列表", httpMethod = "POST", notes = "获取文章评论列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "curPageNO", value = "当前页", dataType = "String", required = false),
            @ApiImplicitParam(paramType = "query", name = "grassId", value = "种草文章ID", dataType = "Long", required = true)
    })
    @PostMapping("/grassCommentList")
    public ResultDto<AppPageSupport<AppGrassCommDto>> grassCommentList(String curPageNO, Long grassId) {
        String index = "".equals(curPageNO) ? "1" : curPageNO;
        AppPageSupport<AppGrassCommDto> appGrassCommDto = appGrassService.queryGrassCommByGrassId(grassId, index);
        return ResultDtoManager.success(appGrassCommDto);
    }


    @ApiOperation(value = "新增或修改文章", httpMethod = "POST", notes = "新增或修改文章", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("/p/saveGrassArticle")
    public ResultDto<String> save(AppGrassArticleSaveDto appGrassArticleSaveDto) {
       /* Set<String> findwords = sensitiveWordService.checkSensitiveWords(appGrassArticleSaveDto.getContent());
        if (AppUtils.isNotBlank(findwords)) {
            return ResultDtoManager.fail("亲, 您提交的内容含有敏感词 " + findwords + ", 请更正再提交！");
        }*/

        // 获取当前登录用户
		String  accessToken = weixinMpTokenService.getAccessToken();

		// 检查标题信息
		WeiXinMsg checkTitResult = WeixinMiniProgramApi.checkMsg(accessToken, appGrassArticleSaveDto.getTitle());
		if(!"ok".equals(checkTitResult.getErrmsg())){
			return ResultDtoManager.fail("亲, 您提交的种草标题含有敏感词, 请更正再提交！");
		}

		// 检查内容信息
		WeiXinMsg checkConResult = WeixinMiniProgramApi.checkMsg(accessToken, appGrassArticleSaveDto.getContent());
		if(!"ok".equals(checkConResult.getErrmsg())){
			return ResultDtoManager.fail("亲, 您提交的种草内容含有敏感词, 请更正再提交！");
		}

		try {
            String pic = "";
            if (AppUtils.isNotBlank(appGrassArticleSaveDto.getPicFile())) {
                for (String image : appGrassArticleSaveDto.getPicFile()) {
                    pic += image + ",";
                }
            }
            LoginedUserInfo user = loginedUserService.getUser();

			SafeHtml safe = new SafeHtml();

            if (appGrassArticleSaveDto.getId() != null) {// update
                GrassArticle entity = grassArticleService.getGrassArticle(appGrassArticleSaveDto.getId());
                if (entity != null) {
                    grassProdService.deleteByGrassId(entity.getId());
                    grassArticleLableService.deleteByGrassId(entity.getId());
                    entity.setTitle(safe.makeSafe(appGrassArticleSaveDto.getTitle()));
                    if (AppUtils.isNotBlank(user)) {
                        entity.setUserId(user.getUserId());
                        entity.setWriterName(user.getUserName());
                    }
                    entity.setIntro(appGrassArticleSaveDto.getIntro());
                    entity.setImage(pic);
                    entity.setContent(safe.makeSafe(appGrassArticleSaveDto.getContent()));
                    entity.setPublishTime(new Date());
                    grassArticleService.updateGrassArticle(entity);
                }
            } else {// save
                GrassArticle grassArticle = new GrassArticle();
                grassArticle.setContent(safe.makeSafe(appGrassArticleSaveDto.getContent()));
                grassArticle.setIntro(appGrassArticleSaveDto.getIntro());
                grassArticle.setTitle(safe.makeSafe(appGrassArticleSaveDto.getTitle()));
                grassArticle.setImage(pic);
                grassArticle.setCreateTime(new Date());
                grassArticle.setPublishTime(new Date());
                grassArticle.setThumbNum(0l);
                grassArticle.setCommentsNum(0l);
                if (AppUtils.isNotBlank(user)) {
                    grassArticle.setUserId(user.getUserId());
                    grassArticle.setWriterName(user.getUserName());
                }
                grassArticleService.saveGrassArticle(grassArticle);
                appGrassArticleSaveDto.setId(grassArticle.getId());
            }

            if (AppUtils.isNotBlank(appGrassArticleSaveDto.getProdIds())) {
                for (Long pid : appGrassArticleSaveDto.getProdIds()) {
                    GrassProd grassProd = new GrassProd();
                    grassProd.setGrarticleId(appGrassArticleSaveDto.getId());
                    grassProd.setProdId(pid);
                    grassProdService.saveGrassProd(grassProd);
                }
            }

            if (AppUtils.isNotBlank(appGrassArticleSaveDto.getLabelIds())) {
                for (Long lid : appGrassArticleSaveDto.getLabelIds()) {
                    grassLabelService.addRefNum(lid);
                    GrassArticleLable grassArticleLable = new GrassArticleLable();
                    grassArticleLable.setGrarticleId(appGrassArticleSaveDto.getId());
                    grassArticleLable.setGrlabelId(lid);
                    grassArticleLableService.saveGrassArticleLable(grassArticleLable);
                }
            }
        } catch (Exception e) {
            return ResultDtoManager.fail("操作失败");
        }
        return ResultDtoManager.success("操作成功");

    }


    @ApiOperation(value = "点赞", httpMethod = "POST", notes = "true 点赞  false取消点赞", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "grassId", value = "文章id", dataType = "Long", required = true),
            @ApiImplicitParam(paramType = "query", name = "rsThumb", value = "关注标识", dataType = "boolean", required = true)
    })
    @PostMapping("/p/grassThum")
    public ResultDto<String> thumNum(Long grassId, boolean rsThumb) {
        try {
            GrassArticle grassArticle = grassArticleService.getGrassArticle(grassId);
            LoginedUserInfo token = loginedUserService.getUser();
            Long num = 0l;
            int thumbNum = grassThumbService.rsThumb(grassId, token.getUserId());
            if (rsThumb) {
                if (thumbNum>0){
                    return ResultDtoManager.fail("你已经点赞过了，切勿重复点赞！");
                }
                GrassThumb grassThumb = new GrassThumb();
                grassThumb.setGraId(grassArticle.getId());
                grassThumb.setUserId(token.getUserId());
                grassThumbService.saveGrassThumb(grassThumb);
                num = grassArticle.getThumbNum() + 1;
            } else {
                if (thumbNum>0){
                    num = grassArticle.getThumbNum() - 1;
                    grassThumbService.deleteGrassThumbByDiscoverId(grassArticle.getId(), token.getUserId());
                }
            }
            if (AppUtils.isNotBlank(grassArticle)) {
                if (rsThumb || thumbNum>0) {
                    grassArticle.setThumbNum(num);
                    grassArticleService.updateGrassArticle(grassArticle);
                }
            }
            return ResultDtoManager.success(rsThumb ? "点赞成功！" : "取消成功！");
        } catch (Exception e) {
            return ResultDtoManager.fail("操作失败");
        }
    }


    @ApiOperation(value = "关注", httpMethod = "POST", notes = "true 关注  false取关", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "grassUserId", value = "文章作者id", dataType = "String", required = true),
            @ApiImplicitParam(paramType = "query", name = "rsConcern", value = "关注标识", dataType = "boolean", required = true)
    })
    @PostMapping("/p/grassConcern")
    public ResultDto<String> concern(String grassUserId, boolean rsConcern) {
        try {
            LoginedUserInfo token = loginedUserService.getUser();
            if (grassUserId.equals(token.getUserId())){
                return ResultDtoManager.fail("自己不能关注自己！");
            }
            if (rsConcern) {
                boolean concernCount = grassConcernService.rsConcern(grassUserId,token.getUserId())>0;
                if (concernCount){
                    return ResultDtoManager.fail("你已经关注过作者，切勿重复关注！");
                }
                GrassConcern grassConcern = new GrassConcern();
                grassConcern.setGrauserId(grassUserId);
                grassConcern.setUserId(token.getUserId());
                grassConcern.setCreateTime(new Date());
                grassConcernService.saveGrassConcern(grassConcern);
            } else {
                grassConcernService.deleteGrassConcernByDiscoverId(grassUserId, token.getUserId());
            }
            return ResultDtoManager.success(rsConcern ? "关注成功！" : "取关成功！");
        } catch (Exception e) {
            return ResultDtoManager.fail("操作失败");
        }
    }


    @ApiOperation(value = "发表评论", httpMethod = "POST", notes = "发表评论", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "content", value = "评论内容", dataType = "String", required = false),
            @ApiImplicitParam(paramType = "query", name = "grassId", value = "文章id", dataType = "Long", required = true)
    })
    @PostMapping("/p/saveGrassComme")
    public ResultDto<Object> saveGrassComme(String content, Long grassId) {
        if (content.length() > 500) {
            return ResultDtoManager.fail("字数过长（500以内）");
        }
        // 敏感字过滤
        //Set<String> findwords = sensitiveWordService.checkSensitiveWords(content);
        //if (AppUtils.isNotBlank(findwords)) {
        //    return ResultDtoManager.fail("亲, 您提交的内容含有敏感词 " + findwords + ", 请更正再提交！");
        //}
		String  accessToken = weixinMpTokenService.getAccessToken();
		WeiXinMsg checkTitResult = WeixinMiniProgramApi.checkMsg(accessToken, content);
		if(null == checkTitResult || !"ok".equals(checkTitResult.getErrmsg())){
			return ResultDtoManager.fail("亲, 您提交的内容含有敏感词 , 请更正再提交！");
		}

        GrassArticle grassArticle = grassArticleService.getGrassArticle(grassId);
        if (grassArticle.getCommentsNum()!=null){
            grassArticle.setCommentsNum(grassArticle.getCommentsNum() + 1);
        }else {
            grassArticle.setCommentsNum(1L);
        }
        grassArticleService.saveGrassArticle(grassArticle);
        GrassComm grassComm = new GrassComm();
        UserDetail user = userDetailService.getUserDetailByIdNoCache(loginedUserService.getUser().getUserId());
        grassComm.setContent(content);
        grassComm.setCreateTime(new Date());
        grassComm.setGraId(grassId);
        grassComm.setUserId(user.getUserId());
        grassComm.setUserImage(user.getPortraitPic());
        grassComm.setUserName(user.getUserName());
        Long rs = grassCommService.saveGrassComm(grassComm);
        if (rs > 0) {
            return ResultDtoManager.success("评论成功");
        } else {
            return ResultDtoManager.fail("评论失败");
        }
    }


    @ApiOperation(value = "获取个人详情", httpMethod = "POST", notes = "获取个人详情数据", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "grassUid", value = "作者id", dataType = "String", required = true)
    })
    @PostMapping("/grassWriterInfo")
    public ResultDto<AppGrassWriterInfoDto> queryWriterInfo(String grassUid) {
        AppGrassWriterInfoDto appPageSupport = appGrassService.queryWriterInfo(grassUid);
        return ResultDtoManager.success(appPageSupport);
    }


    @ApiOperation(value = "获取作者相关文章列表", httpMethod = "POST", notes = "获取作者相关文章列表点赞数排序", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "curPageNO", value = "当前页", dataType = "String", required = false),
            @ApiImplicitParam(paramType = "query", name = "writerId", value = "作者id", dataType = "String", required = false)
    })
    @PostMapping("/grassWriterList")
    public ResultDto<AppPageSupport<AppGrassWriteArticleListDto>> queryWriter(String curPageNO, String writerId) {
        String index = AppUtils.isNotBlank(curPageNO) ? curPageNO : "1";
        AppPageSupport<AppGrassWriteArticleListDto> disSupport = appGrassService.queryWriter(index, writerId, 6);
        return ResultDtoManager.success(disSupport);
    }


    @ApiOperation(value = "获取作者关注或粉丝列表", httpMethod = "POST", notes = "获取作者关注或粉丝列表数据", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "curPageNO", value = "当前页", dataType = "String", required = false),
            @ApiImplicitParam(paramType = "query", name = "flag", value = "关注或粉丝标识 true 粉丝 false 关注", dataType = "boolean", required = true),
            @ApiImplicitParam(paramType = "query", name = "grassUid", value = "作者id", dataType = "String", required = true)
    })
    @PostMapping("/grassConcernList")
    public ResultDto<AppPageSupport<AppGrassConcernListDto>> concernList(String curPageNO, boolean flag, String grassUid) {
        String index = AppUtils.isNotBlank(curPageNO) ? curPageNO : "1";
        AppPageSupport<AppGrassConcernListDto> disSupport = appGrassService.queryConcernByFlag(index, grassUid, flag, 6);
        return ResultDtoManager.success(disSupport);
    }


    @ApiOperation(value = "删除文章", httpMethod = "POST", notes = "删除文章", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParam(paramType = "query", name = "grassArticleId", value = "文章id", dataType = "Long", required = true)
    @PostMapping("/deleteGrassArticle")
    public ResultDto<Object> deleteArticle(Long grassArticleId) {
        int num = grassArticleService.deleteGrassArticle(grassArticleId);
        if (num > 0) {
            return ResultDtoManager.success("删除成功");
        } else {
            return ResultDtoManager.fail("删除失败");
        }
    }

}
