package com.legendshop.app.controller;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.app.dto.*;
import com.legendshop.app.service.AppCartOrderService;
import com.legendshop.app.service.AppCouponService;
import com.legendshop.app.service.AppUserDetailService;
import com.legendshop.base.event.EventProcessor;
import com.legendshop.base.event.OrderCommis;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.framework.cache.client.CacheClient;
import com.legendshop.model.constant.*;
import com.legendshop.model.dto.OrderAddParamDto;
import com.legendshop.model.dto.app.AppUserDetailDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.coupon.UserCouponListDto;
import com.legendshop.model.dto.order.AddOrderMessage;
import com.legendshop.model.dto.order.OrderSetMgDto;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.*;
import com.legendshop.processor.DelayCancelOfferProcessor;
import com.legendshop.processor.ProdSnapshotProcessor;
import com.legendshop.processor.coupon.CounponsUtils;
import com.legendshop.spi.service.*;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import com.legendshop.util.JSONUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 下单流程控制器
 */
@RestController
@RequestMapping(value = "/p/app")
@Api(tags="购物下单",value="下单流程:提交订单")
public class AppSubmitOrderController{

	/** The log. */
	private final Logger Logger = LoggerFactory.getLogger(AppSubmitOrderController.class);

	@Autowired
	private AppCartOrderService appCartOrderService;

	@Autowired
	private SubService subService;

	@Autowired
	private UserAddressService userAddressService;

	@Autowired
	private InvoiceService invoiceService;

	@Autowired
	private OrderCartResolverManager orderCartResolverManager;

	//订单常用处理工具类
	@Autowired
	private OrderUtil orderUtil;

	@Autowired
	private AppUserDetailService appUserDetailService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private StockService stockService;

	@Autowired
	private ProductService productService;

	@Autowired
	private SkuService skuService;

	@Autowired
    private TransportManagerService transportManagerService;

	@Autowired
	private CounponsUtils counponsUtils;

	@Autowired
	private ConstTableService constTableService;

	@Autowired
	private PayTypeService payTypeService;

	@Autowired
	private CounponsUtils counponsUtil;

	@Autowired
	private AppCouponService appCouponService;

	@Autowired
	private UserDetailService userDetailService;

	/** 订单快照 **/
	@Autowired
	private ProdSnapshotProcessor prodSnapshotProcessor;

	/** 订单延迟取消队列 **/
	@Autowired
	private DelayCancelOfferProcessor delayCancelOfferProcessor;

	/** 订单三级分佣 **/
	@Autowired(required=false)
	@Qualifier(value="orderCommisProcessor")
	private EventProcessor<OrderCommis> orderCommisProcessor;

	@Autowired
	private CacheClient cacheClient;

	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	/**
	 * 立即购买参数检查和构建
	 * @param buyNow
	 * @param prodId
	 * @param skuId
	 * @param count
	 * @return
	 */
	private JSONObject buyNow(Boolean buyNow, Long prodId, Long skuId, Integer count){
		JSONObject jsonObject=new JSONObject();
		jsonObject.put("result", false);

		Product product = productService.getProductById(prodId);
		ResultDto<T> resultDto = null;

		//SKU数量检查
		Sku sku = skuService.getSku(skuId);
		if(!stockService.canOrder(sku, count)){
			resultDto = ResultDtoManager.fail(-1, "对不起, 您要购买的数量已超出商品的库存量!");
			jsonObject.put("data", JSONUtil.getJson(resultDto));
			return jsonObject;
		}

		ShopCartItem shopCartItem=new ShopCartItem();
		shopCartItem.setProdId(prodId);
		shopCartItem.setShopId(product.getShopId());
		shopCartItem.setSkuId(skuId);
		shopCartItem.setPrice(sku.getPrice());
		if(AppUtils.isNotBlank(sku.getPic())){
			shopCartItem.setPic(sku.getPic());
		}else{
			shopCartItem.setPic(product.getPic());
		}
		if(AppUtils.isNotBlank(sku.getName())){
			shopCartItem.setProdName(sku.getName());
		}else{
			shopCartItem.setProdName(product.getName());
		}
		shopCartItem.setPromotionPrice(sku.getPrice());
		shopCartItem.setBasketCount(count);
		shopCartItem.setStatus(1);
		shopCartItem.setStocks(sku.getStocks().intValue());
		shopCartItem.setActualStocks(sku.getActualStocks().intValue());
		shopCartItem.setStockCounting(product.getStockCounting());
		shopCartItem.setWeight(sku.getWeight());
		shopCartItem.setVolume(sku.getVolume());
		shopCartItem.setTransportId(product.getTransportId());
		shopCartItem.setTransportFree(product.getSupportTransportFree());
		shopCartItem.setTransportType(product.getTransportType());
		//shopCartItem.setEmsTransFee(product.getEmsTransFee().floatValue());
		shopCartItem.setEmsTransFee(0F);
		shopCartItem.setExpressTransFee(product.getExpressTransFee().floatValue());
		//shopCartItem.setMailTransFee(product.getMailTransFee().floatValue());
		shopCartItem.setMailTransFee(0F);
		shopCartItem.setSupportCod(product.getIsSupportCod()==0?false:true);
		shopCartItem.setIsGroup(product.getIsGroup());
		shopCartItem.setProperties(sku.getProperties());
		shopCartItem.setCnProperties(sku.getCnProperties());
		shopCartItem.setSupportStore(false);
		shopCartItem.setCheckSts(1);//立即购买默认是选中
		jsonObject.put("result", true);
		jsonObject.put("data", shopCartItem);

		return jsonObject;
	}

	/**
	 * 立即购买参数检查
	 * @param prodId
	 * @param skuId
	 * @param count
	 */
	@ApiOperation(value = "立即购买参数检查", httpMethod = "POST", notes = "立即购买参数检查,成功返回Ok,失败返回-1并提示错误信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "prodId", value = "商品Id", dataType = "Long",required=true),
		@ApiImplicitParam(paramType="query", name = "skuId", value = "skuId", dataType = "Long",required=true),
		@ApiImplicitParam(paramType="query", name = "count", value = "购买数量", dataType = "Integer",required=true)
	})
	@PostMapping(value="/checkBuyNow")
	public ResultDto<String> checkBuyNow(Long prodId, Long skuId, Integer count){
		String userId = loginedUserService.getUser().getUserId();
		Product product = productService.getProductById(prodId);



		//不能购买自己商品
		UserDetail userDetail = userDetailService.getUserDetailById(userId);
		if(AppUtils.isNotBlank(userDetail.getShopId()) ){
			if(userDetail.getShopId().equals(product.getShopId())){
				return ResultDtoManager.fail(-1, "对不起,不能购买自己的商品哦!");
			}
		}

		if(product == null){
			return ResultDtoManager.fail(-1, "对不起,您要购买的商品不存在或已下架!");
		}

		if (count == null || count == 0) {//默认要为1
			Logger.debug("加入购物车时, 购物车商品的数量为0!");
			count = 1;
		}

		if(AppUtils.isBlank(product.getShopId()) || product.getShopId()==0){
			return ResultDtoManager.fail(-1, "对不起, 您购买的商品信息异常, 不能购买!");
		}

		if(Constants.OFFLINE.equals(product.getStatus())){
			return ResultDtoManager.fail(-1, "对不起, 您要购买的商品已下架!");
		}

		if(!stockService.canOrder(product, count)){
			//数量检查
			return ResultDtoManager.fail(-1, "对不起,您购买的商品库存不足!");
		}

		//SKU数量检查
		Sku sku = skuService.getSku(skuId);
		if(!stockService.canOrder(sku, count)){
			return ResultDtoManager.fail(-1, "对不起, 您要购买的数量已超出商品的库存量!");
		}
		return ResultDtoManager.success();
	}

	/**
	 * 提交订单时 显示的订单详情
	 * @param reqParams
	 * @return
	 */
	@ApiOperation(value = "提交订单的详情", httpMethod = "POST", notes = "提交订单的详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/orderDetails")
	public ResultDto<AppUserShopCartOrderDto> orderDetails(HttpServletRequest request,AppOrderDetailsParamsDto reqParams) {

		String userId = loginedUserService.getUser().getUserId();
		List<Long> shopCartItems = reqParams.getShopCartItems();
		String type = reqParams.getType();
		type=(type==null?"NORMAL":type);
		Boolean isBuyNow = reqParams.getBuyNow();

		if(AppUtils.isBlank(shopCartItems) && (AppUtils.isBlank(isBuyNow) || !isBuyNow)){
			Logger.info("app orderDetails params is null;params======={}" + shopCartItems + "----" + type);
			return ResultDtoManager.fail(0, " 数据已经发送改变,请重新刷新后操作! ");
		}

		//根据您的下单类型
		Map<String,Object> params=new HashMap<String,Object>();
		if(isBuyNow){
			JSONObject jsonObject = buyNow(isBuyNow, reqParams.getProdId(), reqParams.getSkuId(), reqParams.getCount());
			if(!jsonObject.getBooleanValue("result")){
				return ResultDtoManager.fail(0, jsonObject.getString("data"));
			}
			ShopCartItem shopCartItem = jsonObject.getObject("data", ShopCartItem.class);
			params.put("buyNow", true);
			params.put("shopCartItem", shopCartItem);
		}
		params.put("userId",userId);
		params.put("adderessId", reqParams.getAdderessId());
		params.put("ids", shopCartItems);

		UserShopCartList shopCartList = null;
		CartTypeEnum typeEnum = CartTypeEnum.fromCode(type);
		String couponBack = reqParams.getCouponBack();
		if(couponBack!=null && "yes".equals(couponBack)){//如果是从选择优惠券页面返回, 则从缓存中获取
			shopCartList = subService.findUserShopCartList(typeEnum.toCode(), userId);
			transportManagerService.clacCartDeleivery(shopCartList,true);
		}else{
			shopCartList = orderCartResolverManager.queryOrders(typeEnum, params);
			//========================================================
			//默认选中优惠券,如果不需要自动选择优惠券的情况， 则注释下面的方法即可。 Newway
			if(type.equals("NORMAL") || type.equals("VIRTUAL") || type.equals("AUCTIONS")){//普通、虚拟、拍卖订单可自动选择优惠券
				if(AppUtils.isNotBlank(shopCartList)) {

					orderCouponsPage(shopCartList, userId, "add", type);

				}
			}
			//========================================================
		}
		if(AppUtils.isBlank(shopCartList)){
			return ResultDtoManager.fail(-2, "数据已经发生改变,请重新刷新后操作!");
	    }

		//查出买家的默认发票内容,发票ID不为空，则获取选择的发票
		Invoice userDefaultInvoice = null;
		if (AppUtils.isBlank(reqParams.getInvoiceId())){

			userDefaultInvoice = invoiceService.getDefaultInvoice(userId);
		}else{
			userDefaultInvoice = invoiceService.getInvoice(reqParams.getInvoiceId(),userId);
		}
		shopCartList.setDefaultInvoice(userDefaultInvoice);

		//获取卖家是否开启发票功能
	    Long shopId = shopCartList.getShopCarts().get(0).getShopId();
	    ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);

	    if (AppUtils.isBlank(shopDetail.getSwitchInvoice())) {
	    	shopCartList.setSwitchInvoice(0);
		}
	    shopCartList.setSwitchInvoice(shopDetail.getSwitchInvoice());

		AppUserShopCartOrderDto shopCartOrderDto = appCartOrderService.getShoppingCartOrderDto(shopCartList);
		if(AppUtils.isBlank(shopCartOrderDto)){
			return ResultDtoManager.fail(-2, "数据已经发送改变,请重新刷新后操作!");
	    }

		//设置订单提交令牌,防止重复提交
		Integer token=CommonServiceUtil.generateRandom();
		shopCartOrderDto.setToken(token+"");
		cacheClient.put(Constants.APP_FROM_SESSION_TOKEN + ":" + typeEnum.toCode() + ":" + userId, Constants.APP_FROM_SESSION_TOKEN_EXPIRE, token);

		subService.putUserShopCartList(typeEnum.toCode(),userId,shopCartList);

		return ResultDtoManager.success(shopCartOrderDto);
	}

	/**
	 * 提交订单
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "提交订单", httpMethod = "POST", notes = "提交订单",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/submitOrder")
	public  ResultDto<AppOrderPayParamDto> submitOrder(HttpServletRequest request,OrderAddParamDto paramDto) {

		//参数检查
		LoginedUserInfo appToken = loginedUserService.getUser();
		String userId = appToken.getUserId();
		String userName = appToken.getUserName();

		String type=(paramDto.getType()==null?"NORMAL":paramDto.getType());
		CartTypeEnum typeEnum = CartTypeEnum.fromCode(type);

		//判断是否重复提交
		Integer sessionToken = cacheClient.get(Constants.APP_FROM_SESSION_TOKEN + ":" + typeEnum.toCode() + ":" + userId);
		AddOrderMessage message= orderUtil.checkSubmitParam(userId, sessionToken, typeEnum, paramDto);//检查输入参数
		if(message.hasError()){//有错误就返回
			return ResultDtoManager.fail(0, message.getMessage());
		}

		if(AppUtils.isBlank(userId)){
			return ResultDtoManager.fail(0, "没有登录");
		}

		// TODO 校验用户参数，SpringSession后台过期机制有问题
		if (!userDetailService.isUserExist(userName)) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("用户不存在！");
			return ResultDtoManager.fail(0, message.getMessage());
		}

		//get cache 从上个页面获取设置进入缓存的订单
		UserShopCartList userShopCartList = subService.findUserShopCartList(type, userId);
		if(AppUtils.isBlank(userShopCartList)){
			return ResultDtoManager.fail(-1,"没有购物清单");
		}

		//检查金额： 如果促销导致订单金额为负数
	    if(userShopCartList.getOrderTotalCash() < 0){
	    	return ResultDtoManager.fail(-1,"订单金额为负数,下单失败!");
	    }

		//处理订单的卖家留言备注
		try {
			orderUtil.handlerRemarkText(paramDto.getRemarkText(), userShopCartList);
		} catch (Exception e) {
			Logger.error("处理订单卖家留言出错----->"+e.getMessage());
			return ResultDtoManager.fail(-1,"处理订单卖家留言出错!");
		}

		/** 根据id获取收货地址信息 **/
		UserAddress userAddress = userShopCartList.getUserAddress();
		if (AppUtils.isBlank(userAddress)) {
			userAddress = userAddressService.getDefaultAddress(userId);
			if (AppUtils.isBlank(userAddress)) {
				return ResultDtoManager.fail(-1,"无收货地址！");
			}
			userShopCartList.setUserAddress(userAddress);
		}

		//处理运费模板
		String result=orderUtil.handlerDelivery(paramDto.getDelivery(),userShopCartList);
		if (!Constants.SUCCESS.equals(result)) {
			return ResultDtoManager.fail(-1,"处理运费模板有误！");
		}

		//处理优惠券
		orderUtil.handleCouponStr(userShopCartList);

		//检查库存
		result = orderUtil.verifySafetyStock(userShopCartList);
		if (!Constants.SUCCESS.equals(result)) {
			return ResultDtoManager.fail(-1,result);
		}

		userShopCartList.setUserId(userId);
		userShopCartList.setUserName(userName);
		userShopCartList.setInvoiceId(paramDto.getInvoiceId());
		userShopCartList.setUserAddress(userAddress);
		userShopCartList.setPayManner(paramDto.getPayManner());
		userShopCartList.setType(typeEnum.toCode());
		userShopCartList.setSwitchInvoice(paramDto.getSwitchInvoice());
		if(!"NORMAL".equals(type)){
			userShopCartList.setActiveId(paramDto.getActiveId());
		}
		/**
		 * 处理订单
		 */
		List<String> subIdList= null;
		try {
			subIdList=orderCartResolverManager.addOrder(typeEnum, userShopCartList);
		}
		catch (Exception e) {
			Logger.error(e.getMessage());
			if(e instanceof BusinessException){
				Logger.error("处理订单出错---->"+e.getMessage());
				return ResultDtoManager.fail(-1,e.getMessage());
			}
			Logger.error("处理订单出错---->"+e.getMessage());
			return ResultDtoManager.fail(-1,"下单失败!"+ e.getMessage());
		}
		subService.evictUserShopCartCache(type, userId);//evict cache
		if (AppUtils.isBlank(subIdList)) {
			return ResultDtoManager.fail(-1,"下单失败,请联系商城管理员或商城客服");
		}else{
			//offer 延时取消队列
	  		delayCancelOfferProcessor.process(subIdList);

			//发送网站快照备份
			prodSnapshotProcessor.process(subIdList);

//			//订单分佣情况
//			orderCommisProcessor.process(new OrderCommis(subIdList, userId));
		}

		final String [] ids=new String[subIdList.size()];
		for (int i = 0; i < subIdList.size(); i++) {
			ids[i]=subIdList.get(i);
		}
		//根据订单IDs获取订单集合
		List<Sub> subs=subService.getSubBySubNums(ids, userId, OrderStatusEnum.UNPAY.value());
		Double totalAmount=0.0;//总金额
		String subject=""; //商品描述
		StringBuffer sub_number=new StringBuffer(""); //订单流水号
		for (Sub sub : subs) {
			totalAmount= Arith.add(totalAmount,sub.getActualTotal());
			sub_number.append(sub.getSubNumber()).append(",");
			subject += subject+sub.getProdName()+",";
		}

		String subNumber=sub_number.substring(0, sub_number.length()-1);

		AppOrderPayParamDto orderPay = new AppOrderPayParamDto();
		orderPay.setTotalAmount(totalAmount);
		orderPay.setSubNumber(subNumber);
		orderPay.setSubjects(subject);
		orderPay.setSubSettlementType(SubSettlementTypeEnum.USER_ORDER_PAY.value());
	/*	String payCode=ConfigCode.getInstance().getCode("payCode.aliPayCode");//获取支付宝密钥
		orderPay.setPayCode(payCode);*/

		request.getSession().removeAttribute(Constants.PASSWORD_CALLBACK);
		// 更新session购物车数量
		subService.evictUserShopCartCache(type, userId);
		// 验证成功，清除令牌
		cacheClient.delete(Constants.APP_FROM_SESSION_TOKEN + ":" + typeEnum.toCode() + ":" + userId);

		return ResultDtoManager.success(orderPay);
	}

	private List<PayTypeDto> convertToPayTypeDtoList(List<PayType> payTypeList){
		if(AppUtils.isNotBlank(payTypeList)){
			List<PayTypeDto> payTypeDtoList = new ArrayList<PayTypeDto>();
			for (PayType payType : payTypeList) {
				PayTypeDto payTypeDto = new PayTypeDto(payType.getPayId(), payType.getPayTypeId(), payType.getPayTypeName(), payType.getMemo(),payType.getIsEnable());
				payTypeDtoList.add(payTypeDto);
			}
			return payTypeDtoList;
		}
		return null;
	}

	/**
	 * 获取订单自动取消的时间
	 * @return
	 */
	private Integer getOrderCancelExpireDate(){
		ConstTable constTable= constTableService.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
		Integer expire=60;//默认是60分钟
		if(AppUtils.isNotBlank(constTable) && AppUtils.isNotBlank(constTable.getValue())){
			OrderSetMgDto orderSet=JSONUtil.getObject(constTable.getValue(), OrderSetMgDto.class);
			if(AppUtils.isNotBlank(orderSet) && AppUtils.isNotBlank(orderSet.getAuto_order_cancel())){
				expire=Integer.valueOf(orderSet.getAuto_order_cancel());
			}
		}
		return expire;
	}


	//默认选择优惠券
	private  UserCouponListDto orderCouponsPage(UserShopCartList userShopCartList, String userId, String opType, String orderType) {
		// get cache 从上个页面获取进入缓存的订单
		orderType = (orderType == null ? "NORMAL" : orderType);

		//获取默认选中的优惠券或者红包，选择最优惠的优惠券。  Newway
		//List<Long> userCouponIdList = userCouponService.getSelectedUserCoupons(userId);

		UserCouponListDto couponListDto=counponsUtils.analysisCoupons(userId, userShopCartList, null);//1. 第一次刚从页面进来，查出有效的优惠券

		List<Long> userCouponIdList = null;
		if(AppUtils.isNotBlank(couponListDto) && AppUtils.isNotBlank(couponListDto.getUsableUserCouponList())) {
			userCouponIdList = new ArrayList<Long>();
			Long  platformRedId = 0l; 	 //券id
			Double platformRedCash = 0d; //券金额
			Double platformRedFull = 0d; //券满减金额
			Long shopCouponId = 0l;
			Double shopCouponCash = 0d;
			Double shopCouponFull = 0d;
			Long prodCouponId = 0l;
			Double prodCouponCash = 0d;
			Double prodCouponFull = 0d;
			for (UserCoupon userCoupon : couponListDto.getUsableUserCouponList()) {
				if(userCoupon.getCouponProvider().equals(CouponProviderEnum.PLATFORM.value())) {//红包
					if(Double.compare(userCoupon.getOffPrice(), platformRedCash) >= 0 ) {
						platformRedCash = userCoupon.getOffPrice();
						platformRedId = userCoupon.getUserCouponId();
						platformRedFull = userCoupon.getFullPrice();
					}
				}else if(userCoupon.getCouponProvider().equals(CouponProviderEnum.SHOP.value()) && userCoupon.getCouponType().equals("common")) {//店铺券
					if(Double.compare(userCoupon.getOffPrice(), shopCouponCash) >= 0 ) {
						shopCouponCash = userCoupon.getOffPrice();
						shopCouponId = userCoupon.getUserCouponId();
						shopCouponFull = userCoupon.getFullPrice();
					}
				}else{//商品券
					if(Double.compare(userCoupon.getOffPrice(), prodCouponCash) >= 0 ) {
						prodCouponCash = userCoupon.getOffPrice();
						prodCouponId = userCoupon.getUserCouponId();
						prodCouponFull = userCoupon.getFullPrice();
					}
				}
			}

			///促销价=商品总价-促销优惠
			Double cash = userShopCartList.getOrderTotalCash() - userShopCartList.getAllDiscount();

			//1.如果只有一个红包
			if(platformRedId != 0l && shopCouponId == 0l  && prodCouponId == 0l ){
				userCouponIdList.add(platformRedId);
			}
			//2.如果只有一个店铺券
			if(shopCouponId != 0l && platformRedId == 0l && prodCouponId == 0l ){
				userCouponIdList.add(shopCouponId);
			}
			//3.如果只有一个商品券
			if(prodCouponId != 0l && platformRedId == 0l && shopCouponId == 0l ){
				userCouponIdList.add(prodCouponId);
			}

			//4.如果有一个红包和一个店铺券
			if(platformRedId != 0l && shopCouponId != 0l && prodCouponId == 0l ){
				Double firstOff = 0d;//使用一个优惠券/红包之后的价格
				if(Double.compare(platformRedCash, shopCouponCash) >= 0){ //红包大于或等于店铺券
					firstOff = cash-platformRedCash;//优惠价=促销价-红包金额
					//如果店铺券满减金额大于firstOff 不满足店铺券的使用
					if(Double.compare(shopCouponFull, firstOff) > 0){
						userCouponIdList.add(platformRedId);
					}else{
						userCouponIdList.add(platformRedId);
						userCouponIdList.add(shopCouponId);
					}
				}else{//店铺券大于等于红包
					firstOff = Arith.sub(cash, shopCouponCash);//优惠价=促销价-店铺券
					//如果红包券满减金额大于firstOff 不满足红包的使用
					if(Double.compare(platformRedFull, firstOff) > 0){
						userCouponIdList.add(shopCouponId);
					}else{
						userCouponIdList.add(shopCouponId);
						userCouponIdList.add(platformRedId);
					}
				}
			}

			//5.如果有一个红包和一个商品券
			if(platformRedId != 0l && prodCouponId != 0l && shopCouponId == 0l ){
				Double firstOff = 0d;//使用一个优惠券/红包之后的价格
				if(Double.compare(platformRedCash, prodCouponCash)>=0){ //红包大于或等于商品券
					firstOff = Arith.sub(cash, platformRedCash);//优惠价=促销价-红包金额
					//如果商品券满减金额大于firstOff 不满足商品券的使用
					if(Double.compare(prodCouponFull, firstOff)>0){
						userCouponIdList.add(platformRedId);
					}else{
						userCouponIdList.add(platformRedId);
						userCouponIdList.add(prodCouponId);
					}
				}else{//商品券大于等于红包
					firstOff = Arith.sub(cash, prodCouponCash);//优惠价=促销价-商品券
					//如果红包券满减金额大于firstOff 不满足红包的使用
					if(Double.compare(platformRedFull, firstOff)>0){
						userCouponIdList.add(prodCouponId);
					}else{
						userCouponIdList.add(prodCouponId);
						userCouponIdList.add(platformRedId);
					}
				}
			}
			//6.如果有一个店铺券和一个商品券
			if(platformRedId == 0l && shopCouponId != 0l && prodCouponId != 0l){
				Double firstOff = 0d;//使用一个优惠券之后的价格
				if(Double.compare(shopCouponCash, prodCouponCash)>=0){//店铺券金额大于商品券金额
					firstOff = Arith.sub(cash, shopCouponCash);//优惠价=促销价-优惠券
					if(Double.compare(prodCouponFull, firstOff)>0){
						userCouponIdList.add(shopCouponId);
					}else{
						userCouponIdList.add(shopCouponId);
						userCouponIdList.add(prodCouponId);
					}
				}else{
					firstOff = Arith.sub(cash, prodCouponCash);//优惠价=促销价-优惠券
					if(Double.compare(shopCouponFull, firstOff)>0){
						userCouponIdList.add(prodCouponId);
					}else{
						userCouponIdList.add(prodCouponId);
						userCouponIdList.add(shopCouponId);
					}
				}
			}

			//7.如果有一个红包和一个店铺券和一个商品券
			if(platformRedId != 0l && shopCouponId != 0l && prodCouponId != 0l){
				Double firstOff = 0d;//使用一个优惠之后的价格
				Double secondOff = 0d ;//使用两个优惠之后的价格
				if(Double.compare(platformRedCash, shopCouponCash)>=0 && Double.compare(platformRedCash, prodCouponCash)>=0){//红包金额最大
					firstOff = Arith.sub(cash, platformRedCash);
					if(Double.compare(shopCouponCash, prodCouponCash)>=0){//店铺券大于商品券
						if(Double.compare(firstOff, shopCouponFull)>=0){//店铺券满足使用金额
							secondOff = Arith.sub(firstOff, shopCouponCash);
							if(Double.compare(secondOff, prodCouponFull)>=0){//商品券满足使用条件
								userCouponIdList.add(platformRedId);
								userCouponIdList.add(shopCouponId);
								userCouponIdList.add(prodCouponId);
							}else{
								userCouponIdList.add(platformRedId);
								userCouponIdList.add(shopCouponId);
							}
						}else if(Double.compare(firstOff, shopCouponFull)>=0){//商品券满足使用金额
							userCouponIdList.add(platformRedId);
							userCouponIdList.add(prodCouponId);
						}else{//店铺券和商品都不满足使用金额
							userCouponIdList.add(platformRedId);
						}
					}else{//商品券大于店铺券
						if(Double.compare(firstOff, prodCouponFull)>=0){//商品券满足使用金额
							secondOff = Arith.sub(firstOff, prodCouponCash);
							if(Double.compare(secondOff, shopCouponFull)>=0){//店铺券满足使用条件
								userCouponIdList.add(platformRedId);
								userCouponIdList.add(prodCouponId);
								userCouponIdList.add(shopCouponId);

							}else{
								userCouponIdList.add(platformRedId);
								userCouponIdList.add(prodCouponId);
							}
						}else if(Double.compare(firstOff, shopCouponFull)>=0){//店铺券满足使用金额
							userCouponIdList.add(platformRedId);
							userCouponIdList.add(shopCouponId);
						}else{//店铺券和商品都不满足使用金额
							userCouponIdList.add(platformRedId);
						}
					}
				}
				if(Double.compare(shopCouponCash, platformRedCash)>=0 && Double.compare(shopCouponCash, prodCouponCash)>=0){//店铺券金额最大
					firstOff = Arith.sub(cash, shopCouponCash);
					if((platformRedCash-prodCouponCash)>=0){//红包大于商品券
						if(Double.compare(firstOff, platformRedFull)>=0){//红包满足使用金额
							secondOff = Arith.sub(firstOff, platformRedCash);
							if(Double.compare(secondOff, prodCouponFull)>=0){//商品券满足使用条件
								userCouponIdList.add(shopCouponId);
								userCouponIdList.add(platformRedId);
								userCouponIdList.add(prodCouponId);
							}else{
								userCouponIdList.add(shopCouponId);
								userCouponIdList.add(platformRedId);
							}
						}else if(Double.compare(firstOff, prodCouponFull)>=0){//商品券满足使用金额
							userCouponIdList.add(platformRedId);
							userCouponIdList.add(prodCouponId);
						}else{
							userCouponIdList.add(shopCouponId);
						}
					}else{//商品券大于红包
						if(Double.compare(firstOff, prodCouponFull)>=0){//商品券满足使用金额
							secondOff = Arith.sub(firstOff, prodCouponFull);
							if(Double.compare(secondOff, platformRedFull)>=0){//红包满足使用条件
								userCouponIdList.add(shopCouponId);
								userCouponIdList.add(prodCouponId);
								userCouponIdList.add(platformRedId);
							}else{
								userCouponIdList.add(shopCouponId);
								userCouponIdList.add(prodCouponId);
							}
						}else if(Double.compare(firstOff, platformRedFull)>=0){//红包满足使用金额
							userCouponIdList.add(shopCouponId);
							userCouponIdList.add(platformRedId);
						}else{
							userCouponIdList.add(shopCouponId);
						}
					}
				}
				if(Double.compare(prodCouponCash, platformRedCash)>=0 && Double.compare(prodCouponCash, shopCouponCash)>=0){//商品券金额最大
					firstOff = Arith.sub(cash, prodCouponCash);
					if(Double.compare(platformRedCash, shopCouponCash)>=0){//红包大于店铺券
						if(Double.compare(firstOff, platformRedFull)>=0){//红包满足使用条件
							secondOff = Arith.sub(firstOff, platformRedCash);
							if(Double.compare(secondOff, shopCouponFull)>=0){//店铺券满足使用条件
								userCouponIdList.add(prodCouponId);
								userCouponIdList.add(platformRedId);
								userCouponIdList.add(shopCouponId);
							}else{
								userCouponIdList.add(prodCouponId);
								userCouponIdList.add(platformRedId);
							}
						}else if(Double.compare(firstOff, shopCouponFull)>=0){//店铺券满足使用金额
							userCouponIdList.add(prodCouponId);
							userCouponIdList.add(shopCouponId);
						}else{
							userCouponIdList.add(prodCouponId);
						}
					}else{//店铺券大于红包
						if(Double.compare(firstOff, shopCouponFull)>=0){//店铺券满足使用条件
							secondOff = Arith.sub(firstOff, shopCouponCash);
							if(Double.compare(secondOff, platformRedFull)>=0){//红包满足使用条件
								userCouponIdList.add(prodCouponId);
								userCouponIdList.add(shopCouponId);
								userCouponIdList.add(platformRedId);
							}else{
								userCouponIdList.add(prodCouponId);
								userCouponIdList.add(shopCouponId);
							}
						}else if(Double.compare(firstOff, platformRedFull)>=0){//红包满足使用金额
							userCouponIdList.add(prodCouponId);
							userCouponIdList.add(platformRedId);
						}else{
							userCouponIdList.add(prodCouponId);
						}
					}
				}

			}

		}
		if(AppUtils.isNotBlank(userShopCartList)){

			List<ShopCarts> shopCarts=userShopCartList.getShopCarts();
			for(ShopCarts carts:shopCarts){
				for(ShopCartItem shopCartItem:carts.getCartItems()){
					shopCartItem.setRedpackOffPrice(0d);
					shopCartItem.setCouponOffPrice(0d);
				}
				carts.setRedpackOffPrice(0d);
				carts.setCouponOffPrice(0d);
			}
			userShopCartList.setCouponOffPrice(0d);

			List<Long> userCouponIds = userShopCartList.getUserCouponIds();
			if(userCouponIdList!=null){

				for (Long userCouponId : userCouponIdList.stream().distinct().collect(Collectors.toList())) {
					if("add".equals(opType)){
						userCouponIds.add(userCouponId);
					}else if("remove".equals(opType)){
						userCouponIds.remove(userCouponId);
					}
				}
			}
			//分析处理 红包和优惠券
			couponListDto=counponsUtils.analysisCoupons(userId, userShopCartList, userCouponIds);//2. 传入最优的优惠券，计算出优惠值

			userShopCartList.setUserCouponIds(userCouponIds);
			subService.putUserShopCartList(orderType,userId,userShopCartList);
			return couponListDto;
		}

		return null;
	}

		/**
		 * (app端)（新）
		 * 用户订单页面 查询 可用和不可用的 所有优惠券
		 * @return
		 */
		@ApiOperation(value = "用户订单页面 查询 可用和不可用的 所有优惠券", httpMethod = "POST", notes = "用户订单页面 查询 可用和不可用的 所有优惠券",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
		@ApiImplicitParam(paramType = "query", dataType = "String", name = "userCouponIdsStr", value = "当前页面选择的用户优惠券Id，以，分割",required=false)
		@PostMapping(value="/coupon/orderCouponsPageForApp")
		public  ResultDto<AppUserCouponListDto> orderCouponsPageForApp(String userCouponIdsStr) {
			//获取登录用户的userId
			String userId = loginedUserService.getUser().getUserId();
			//get cache 从上个页面获取进入缓存的订单
			UserShopCartList userShopCartList=subService.findUserShopCartList(CartTypeEnum.NORMAL.toCode(),userId);
			AppUserCouponListDto appCouponListDto = new AppUserCouponListDto();
			if(AppUtils.isNotBlank(userShopCartList)){

				// 分割 已选中的 用户优惠券ID
				List<Long> userCouponIds = new ArrayList<Long>();

				// 如果传递的优惠券ID为空，则取默认优惠券
				if (AppUtils.isBlank(userCouponIdsStr)) {
					if(userShopCartList.getLoadDefaultCoupon()){
						userCouponIds = userShopCartList.getUserCouponIds();
						userShopCartList.setLoadDefaultCoupon(false);
					}
				}else{

					String[] userCouponIdStrs = userCouponIdsStr.split(",");
					for (int i = 0; i < userCouponIdStrs.length; i++) {
						try {
							Long userCouponId = Long.valueOf(userCouponIdStrs[i]);
							userCouponIds.add(userCouponId);
						} catch (NumberFormatException e) {
							Logger.error("userCouponId:{} NumberFormatException", userCouponIdStrs[i]);
						}
					}
				}

				//将所有购物车项的优惠券金额重置为0
				for (ShopCarts shopCarts : userShopCartList.getShopCarts()) {
					for(ShopCartItem shopCartItem:shopCarts.getCartItems()){
						shopCartItem.setCouponOffPrice(0.0);
						shopCartItem.setRedpackOffPrice(0.0);
					}
				}

				//分析处理 红包和优惠券
				UserCouponListDto couponListDto = counponsUtil.analysisCoupons(userId, userShopCartList, userCouponIds);
				appCouponListDto = appCouponService.convertToAppUserCouponListDto(couponListDto);
				userShopCartList.setUserCouponIds(userCouponIds);
				subService.putUserShopCartList(CartTypeEnum.NORMAL.toCode(),userId,userShopCartList);
			}
			return ResultDtoManager.success(appCouponListDto);
		}

		/**
		 * (app端)（旧）
		 * 用户订单页面 查询 可用和不可用的 所有优惠券
		 * @param request
		 * @param response
		 * @return
		 */
		/*@ApiOperation(value = "用户订单页面 查询 可用和不可用的 所有优惠券", httpMethod = "POST", notes = "用户订单页面 查询 可用和不可用的 所有优惠券",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
		@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", dataType = "Long", name = "userCouponId", value = "用户优惠券Id",required=false),
			@ApiImplicitParam(paramType = "query", dataType = "String", name = "opType", value = "优惠券选中状态:add为优惠券选中使用状态，remove为取消选中使用",required=false)
		})
		@PostMapping(value="/coupon/orderCouponsPageForApp")
		public  ResultDto<AppUserCouponListDto> orderCouponsPageForApp(HttpServletRequest request, HttpServletResponse response,Long userCouponId,String opType) {
			//获取登录用户的userId
			String userId = loginedUserService.getUser().getUserId();
			//get cache 从上个页面获取进入缓存的订单
			UserShopCartList userShopCartList=subService.findUserShopCartList(CartTypeEnum.NORMAL.toCode(),userId);
			AppUserCouponListDto appCouponListDto = new AppUserCouponListDto();
			if(AppUtils.isNotBlank(userShopCartList)){
				List<Long> userCouponIds = userShopCartList.getUserCouponIds();
				if(userCouponId!=null){
					if("add".equals(opType)){
						userCouponIds.add(userCouponId);
					}else if("remove".equals(opType)){
						userCouponIds.remove(userCouponId);
					}
				}

				//将所有购物车项的优惠券金额重置为0
				for (ShopCarts shopCarts : userShopCartList.getShopCarts()) {
					for(ShopCartItem shopCartItem:shopCarts.getCartItems()){
						shopCartItem.setCouponOffPrice(0.0);
						shopCartItem.setRedpackOffPrice(0.0);
					}
				}

				//分析处理 红包和优惠券
				UserCouponListDto couponListDto = counponsUtil.analysisCoupons(userId, userShopCartList, userCouponIds);
				appCouponListDto = appCouponService.convertToAppUserCouponListDto(couponListDto);
				userShopCartList.setUserCouponIds(userCouponIds);
				subService.putUserShopCartList(CartTypeEnum.NORMAL.toCode(),userId,userShopCartList);
			}
			return ResultDtoManager.success(appCouponListDto);
		}*/

		/**
		 * (app端)付款页面获取数据
		 */
		@ApiOperation(value = "付款页面获取数据", httpMethod = "POST", notes = "付款页面获取数据",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
		@ApiImplicitParam(paramType = "query", dataType = "Long", name = "userCouponId", value = "优惠券Id",required=true)
		@PostMapping(value="/payData")
		public  ResultDto<AppPayPageDateDto> payData() {
			//获取登录用户的userId
			String userId = loginedUserService.getUser().getUserId();
			// 获取订单自动取消的时间
			int cancelMins = getOrderCancelExpireDate();
			// 获取 支付方式
			List<PayType> payTypeList = payTypeService.getPayTypeList();
			List<PayTypeDto> payTypeDtoList = convertToPayTypeDtoList(payTypeList);
			AppUserDetailDto userDetail = appUserDetailService.getUserDetail(userId);
			Double availablePredeposit = userDetail.getAvailablePredeposit();
			AppPayPageDateDto appPayPageDateDto = new AppPayPageDateDto();
			appPayPageDateDto.setPayTypeDtoList(payTypeDtoList);
			appPayPageDateDto.setCancelMins(cancelMins);
			appPayPageDateDto.setAvailablePredeposit(availablePredeposit);
			return ResultDtoManager.success(appPayPageDateDto);
		}

		/**
		 * 订单支付页面
		 * @param request
		 * @param response
		 * @param subNums //订单Id
		 *@date 2016-7-5
		 */
		@ApiOperation(value = "订单支付页面", httpMethod = "POST", notes = "订单支付页面",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
		@PostMapping(value="/orderPay")
		public  ResultDto<AppOrderPayParamDto> orderPay(HttpServletRequest request, HttpServletResponse response, String[] subNums) {
			String userId = loginedUserService.getUser().getUserId();
			if (AppUtils.isBlank(userId)) {
				return ResultDtoManager.fail(-1,"请先登录!");
			} else if (AppUtils.isBlank(subNums)) {
				return ResultDtoManager.fail(-1,"下单失败!"); // 没有订单号跳到用户订单列表页面
			}

			double totalAmount = 0d; // 支付金额
			String subIds = "";
			String subject = ""; // 商品名

			List<Sub> subs = subService.getSubBySubNums(subNums, userId, OrderStatusEnum.UNPAY.value());
			if (AppUtils.isBlank(subs)) {
				return ResultDtoManager.fail(-1,"下单失败!");
			}
			for (Sub sub : subs) {
				totalAmount = Arith.add(totalAmount, sub.getActualTotal());
				subIds += sub.getSubNumber() + ",";
				subject += subject + sub.getProdName() + ",";
			}
			subIds = subIds.substring(0, subIds.length() - 1);
			AppOrderPayParamDto orderPay = new AppOrderPayParamDto();
			orderPay.setSubjects(subject);
			orderPay.setTotalAmount(totalAmount);
			orderPay.setSubNumber(subIds);// 方便设置支付密码回跳
			orderPay.setSubSettlementType(SubSettlementTypeEnum.USER_ORDER_PAY.value());
			request.getSession().removeAttribute(Constants.PASSWORD_CALLBACK);
			return ResultDtoManager.success(orderPay);
		}
}
