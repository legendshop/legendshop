/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.app.service.impl;

import com.legendshop.business.dao.PassportDao;
import com.legendshop.business.dao.PassportSubDao;
import com.legendshop.model.constant.PassportTypeEnum;
import com.legendshop.model.entity.Passport;
import com.legendshop.model.entity.PassportDto;
import com.legendshop.model.entity.PassportSub;
import com.legendshop.spi.service.PassportService;
import com.legendshop.spi.service.PassportSubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dao.AppTokenDao;
import com.legendshop.app.service.AppTokenService;
import com.legendshop.model.entity.AppToken;
import com.legendshop.util.AppUtils;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Token服务.
 */
@Service("appTokenService")
public class AppTokenServiceImpl implements AppTokenService {

	@Autowired
	private AppTokenDao appTokenDao;

	@Autowired
	private PassportDao passportDao;

	@Autowired
	private PassportSubDao passportSubDao;

	@Override
	public AppToken getAppTokenByUserId(String userId) {
		if (userId == null) {
			return null;
		}
		return appTokenDao.getAppTokenByUserId(userId);
	}

	public AppToken getAppToken(Long id) {
		return appTokenDao.getAppToken(id);
	}

	public void deleteAppToken(AppToken appToken) {
		appTokenDao.deleteAppToken(appToken);
	}

	public Long saveAppToken(AppToken appToken) {
		if (!AppUtils.isBlank(appToken.getId())) {
			updateAppToken(appToken);
			return appToken.getId();
		}
		return appTokenDao.saveAppToken(appToken);
	}

	public void updateAppToken(AppToken appToken) {
		appTokenDao.updateAppToken(appToken);
	}

	@Override
	@Transactional
	public boolean deleteAppToken(String userId, String accessToken) {
		List<Passport> passportList = passportDao.getPassport(userId, PassportTypeEnum.WEIXIN.value());
		passportSubDao.delByPassportId(passportList.stream().map(Passport::getId).collect(Collectors.toList()));
		passportDao.delete(passportList);
		return appTokenDao.deleteAppToken(userId, accessToken);
	}

	@Override
	public boolean deleteAppToken(String userId) {
		return appTokenDao.deleteAppToken(userId);
	}

	/**
	 * 根据用户id和平台类型获取token
	 */
	@Override
	public AppToken getAppToken(String userId, String platform, String type) {

		if (userId == null) {
			return null;
		}
		return appTokenDao.getAppTokenByUserId(userId, platform, type);
	}

}
