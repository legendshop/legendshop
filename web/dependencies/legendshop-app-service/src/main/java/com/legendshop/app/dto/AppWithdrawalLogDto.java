package com.legendshop.app.dto ;
import java.util.Date;

import com.legendshop.dao.persistence.Column;
import com.legendshop.dao.persistence.Entity;
import com.legendshop.dao.persistence.GeneratedValue;
import com.legendshop.dao.persistence.GenerationType;
import com.legendshop.dao.persistence.Id;
import com.legendshop.dao.persistence.Table;
import com.legendshop.dao.persistence.TableGenerator;
import com.legendshop.dao.persistence.Transient;
import com.legendshop.dao.support.GenericEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 提现记录Dto
 */
@ApiModel("提现记录Dto")
public class AppWithdrawalLogDto  {

	/** 记录id */
	@ApiModelProperty(value = "记录id")
	private Long id; 
		
	/** 金额变更 */
	@ApiModelProperty(value = "金额变更 ")
	private Double amount; 
		
	/** 描述 */
	@ApiModelProperty(value = "描述")
	private String logDesc; 
		
	/** 添加时间 */
	@ApiModelProperty(value = "提现时间 ")
	private Date addTime; 
	
	/** 日志类型 */
	@ApiModelProperty(value = "日志类型:order_finally下单，auctions_freeze冻结，recharge充值，promoter分销提现，commission佣金收入，refund退款，auctions_deduct扣除保证金")
	private String logType;

	/** 日志类型 PdCashLogEnum */
	@ApiModelProperty(value = "日志类型名称")
	private String logTypeName;
	
	public AppWithdrawalLogDto() {
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getLogDesc() {
		return logDesc;
	}

	public void setLogDesc(String logDesc) {
		this.logDesc = logDesc;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}

	public String getLogTypeName() {
		return logTypeName;
	}

	public void setLogTypeName(String logTypeName) {
		this.logTypeName = logTypeName;
	}
}
