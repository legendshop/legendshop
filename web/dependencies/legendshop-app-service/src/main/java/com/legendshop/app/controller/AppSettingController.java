/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.appdecorate.AppSettingDto;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 装修--基础设置Controller
 */
@RestController
@Api(tags="装修基础设置",value="移动端装修基础设置")
public class AppSettingController{
	
	private final Logger LOGGER = LoggerFactory.getLogger(AppSettingController.class);

	@Autowired
	private ConstTableService constTableService;

	
	
	@ApiOperation(value = "获取基础设置", httpMethod = "GET", notes = "获取基础设置",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@GetMapping("/appSetting")
	public ResultDto<AppSettingDto> appSetting() {
		
		try {
			
			ConstTable constTable = constTableService.getConstTablesBykey("APP_SETTING", "APP_SETTING");
			AppSettingDto appSettingDto = null;
			if (AppUtils.isNotBlank(constTable)) {
				
				String setting = constTable.getValue();
				
				appSettingDto = JSONUtil.getObject(setting, AppSettingDto.class);
				
				String  categorySetting = JSONUtil.getObject(appSettingDto.getCategorySetting(), String.class);
				appSettingDto.setCategorySetting(categorySetting);
			}
			
			return ResultDtoManager.success(appSettingDto);
		} catch (Exception e) {
			LOGGER.error("获取基础设置异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	
}
