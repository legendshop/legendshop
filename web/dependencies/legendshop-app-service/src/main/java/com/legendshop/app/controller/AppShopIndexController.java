package com.legendshop.app.controller;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.dto.BusinessMessageDTO;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.legendshop.app.constants.StatusCodes;
import com.legendshop.app.dto.AppShopProdCategoryDto;
import com.legendshop.app.dto.AppStoreProdDto;
import com.legendshop.app.service.AppProductService;
import com.legendshop.app.service.AppShopCategoryService;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.model.constant.VisitSourceEnum;
import com.legendshop.model.constant.VisitTypeEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.app.store.AppShopIndexDto;
import com.legendshop.model.dto.app.store.AppShopProdSearchParams;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.processor.VisitLogProcessor;
import com.legendshop.util.AppUtils;
import com.legendshop.web.helper.IPHelper;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;


/**
 * 店铺主页相关
 */
@Api(tags="店铺首页",value="店铺首页相关")
@RestController
public class AppShopIndexController {
	
	private static Logger LOGGER = LoggerFactory.getLogger(AppShopIndexController.class);
	
	@Autowired
	private ShopDetailService shopDetailService;
	
	@Autowired
	private FavoriteShopService favoriteShopService;
	
	@Autowired
	private AppProductService appProductService;
	
	@Autowired
	private AppShopCategoryService appShopCategoryService;
	
	@Autowired
	private ShopAppDecorateService shopAppDecorateService;
	
	/**
	 * 日志查看
	 */
	@Autowired
	private VisitLogProcessor visitLogProcessor;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private ProductCommentStatService productCommentStatService;

	@Autowired
	private ShopCommStatService shopCommStatService;

	@Autowired
	private DvyTypeCommStatService dvyTypeCommStatService;

	@Autowired
	private ShopService shopService;
	
	/**
	 * 店铺主页
	 * @param request
	 * @param response
	 * @param shopId  店铺ID
	 * @return 
	 */
	@ApiOperation(value = "店铺首页", httpMethod = "POST", notes = "店铺首页",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="path", name = "shopId", value = "店铺id", required = true, dataType = "Long"),
	})
	@PostMapping(value="/shop/index/{shopId}")
	public ResultDto<AppShopIndexDto> storeIndex(HttpServletRequest request, @PathVariable Long shopId){
		Float shopscore=0f;
		Float prodscore=0f;
		Float logisticsScore=0f;
		DecimalFormat decimalFormat = new DecimalFormat("0.0");
		decimalFormat.setRoundingMode(RoundingMode.FLOOR);
		try {
			LoginedUserInfo token = loginedUserService.getUser();

			AppShopIndexDto shopIndexDto = new AppShopIndexDto();
			
			//获取店铺信息详情
			ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
			
			if (AppUtils.isBlank(shopDetail)) {
				return ResultDtoManager.fail(-1, "对不起,您要查看的店铺信息不存在!");
			}
			if (AppUtils.isNotBlank(shopDetail.getStatus())
					&& shopDetail.getStatus() != 1) {
				return ResultDtoManager.fail(-1, "对不起, 该店铺未正常营业!");
			}
			
			if (AppUtils.isNotBlank(token)) {
				
				//查看该店铺是否已被该用户收藏
				boolean existsFavoriteShop = favoriteShopService.isExistsFavoriteShop(token.getUserId(),shopId);
				if (existsFavoriteShop) {
					shopIndexDto.setIsCollection(1);
				}else{
					shopIndexDto.setIsCollection(0);
				}
			}else{
				shopIndexDto.setIsCollection(0);
			}

			//计算该店铺所有已评分商品的平均分
			ProductCommentStat prodCommentStat = productCommentStatService.getProductCommentStatByShopId(shopDetail.getShopId());
			if(prodCommentStat!=null&&prodCommentStat.getScore()!=null&&prodCommentStat.getComments()!=null){
				prodscore=(float)prodCommentStat.getScore()/prodCommentStat.getComments();
			}
			shopIndexDto.setProdscore(decimalFormat.format(prodscore));

			//计算该店铺所有已评分店铺的平均分
			ShopCommStat shopCommentStat = shopCommStatService.getShopCommStatByShopId(shopDetail.getShopId());
			if(shopCommentStat!=null&&shopCommentStat.getScore()!=null&&shopCommentStat.getCount()!=null){
				shopscore=(float)shopCommentStat.getScore()/shopCommentStat.getCount();
			}
			shopIndexDto.setShopScore(decimalFormat.format(shopscore));
			//计算该店铺所有已评分物流的平均分
			DvyTypeCommStat  dvyTypeCommStat = dvyTypeCommStatService.getDvyTypeCommStatByShopId(shopDetail.getShopId());
			if(dvyTypeCommStat!=null&&dvyTypeCommStat.getScore()!=null&&dvyTypeCommStat.getCount()!=null){
				logisticsScore=(float)dvyTypeCommStat.getScore()/dvyTypeCommStat.getCount();
			}
			shopIndexDto.setLogisticsScore(decimalFormat.format(logisticsScore));
			Float sum=0f;
			if (shopscore!=0&&prodscore!=0&&logisticsScore!=0) {
				sum=(float)Math.floor((shopscore+prodscore+logisticsScore)/3);
			}else if(shopscore!=0&&logisticsScore!=0){
				sum=(float)Math.floor((shopscore+logisticsScore)/2);
			}
			shopIndexDto.setScore(decimalFormat.format(sum));
			
			//构建返回Dto参数
			shopIndexDto.setSiteName(shopDetail.getSiteName());
			shopIndexDto.setShopPic(shopDetail.getShopPic());

			// 多线程记录访问历史
			if (systemParameterUtil.isVisitLogEnable()) {
				if(AppUtils.isNotBlank(token)){
					
					VisitLog visitLog = new VisitLog(IPHelper.getIpAddr(request), token.getUserName(), 
							shopDetail.getShopId(), shopDetail.getSiteName(), 
							VisitTypeEnum.INDEX.value(), VisitSourceEnum.APP.value(), new Date());
					visitLogProcessor.process(visitLog);
				}
			}
			return ResultDtoManager.success(shopIndexDto);
			
		} catch (Exception e) {
			LOGGER.error("获取店铺首页信息异常!", e);
			return ResultDtoManager.fail();
		}
		
	}
	
	/**
	 * 店铺首页装修
	 * @param shopId
	 * @return
	 */
	@ApiOperation(value = "店铺首页装修", httpMethod = "POST", notes = "返回店铺首页装修数据",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="path", name = "shopId", value = "店铺id", required = true, dataType = "Long")
	@PostMapping(value="/shop/decorate/{shopId}")
	public ResultDto<JSONObject> decorate(@PathVariable Long shopId){

		try {
			
			String releaseDecorate = shopAppDecorateService.getReleaseDecorateDataByShopId(shopId);
			if (AppUtils.isBlank(releaseDecorate)) {
				return ResultDtoManager.fail(-1, "该店铺暂无装修");
			}
			Integer status = shopService.getShopStatus(shopId);
			if (status.equals(ShopStatusEnum.OFFLINE.value())){
				return ResultDtoManager.fail(-1, "该店铺已不营业");
			}
			JSONObject appDecorateDate = JSONUtil.parseObj(releaseDecorate);
			return ResultDtoManager.success(appDecorateDate);
		} catch (Exception e) {
			LOGGER.error("获取店铺首页装修数据异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 店铺商品分类
	 * @param shopId
	 * @return
	 */
	@ApiOperation(value = "店铺商品分类", httpMethod = "POST", notes = "店铺商品分类",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="path", name = "shopId", value = "店铺id", required = true, dataType = "Long")
	@PostMapping(value="/shop/prodCategory/{shopId}")
	public ResultDto<List<AppShopProdCategoryDto>> shopCategory(@PathVariable Long shopId){

		try {
			
			//获取商家的一级类目
			List<AppShopProdCategoryDto> result = appShopCategoryService.getFirstShopCategory(shopId);
			
			if (AppUtils.isBlank(result)) {
				return ResultDtoManager.fail(-1, "该店铺暂没有商品分类信息");
			}
			
			return ResultDtoManager.success(result);
			
		} catch (Exception e) {
			LOGGER.error("获取店铺商品分类异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 店铺精选好货
	 * @param shopId
	 * @return
	 */
	@ApiOperation(value = "店铺精选好货", httpMethod = "POST", notes = "店铺精选好货",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="path", name = "shopId", value = "店铺id", required = true, dataType = "Long")
	@PostMapping(value="/shop/hotProd/{shopId}")
	public ResultDto<List<AppStoreProdDto>> hotProd(@PathVariable Long shopId){

		try {

			//获取店铺信息详情
			ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);

			if (AppUtils.isBlank(shopDetail)) {
				return ResultDtoManager.fail(-1, "对不起,您要查看的店铺信息不存在!");
			}
			if (AppUtils.isNotBlank(shopDetail.getStatus())
					&& shopDetail.getStatus() == 0) {
				return ResultDtoManager.fail(-1, "对不起, 该店铺已不营业!");
			}
			List<AppStoreProdDto> result = appProductService.getHotProd(shopId, 6);
			if (AppUtils.isBlank(result)) {
				return ResultDtoManager.fail(-1, "该店铺暂没有精选商品提供");
			}
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("获取店铺精选商品异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	
	/**
	 * 店铺商品查询
	 * @param params
	 * @return
	 */
	@ApiOperation(value = "店铺全部商品", httpMethod = "POST", notes = "店铺商品查询",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/shop/prodList")
	public ResultDto<AppPageSupport<AppStoreProdDto>> storeProdctList(AppShopProdSearchParams params){
		
		try {

			//获取店铺信息详情
			ShopDetail shopDetail = shopDetailService.getShopDetailById(params.getShopId());
			if (AppUtils.isBlank(shopDetail)) {
				return ResultDtoManager.fail(-1, "对不起,您要查看的店铺信息不存在!");
			}
			if (AppUtils.isNotBlank(shopDetail.getStatus())
					&& shopDetail.getStatus() == 0) {
				return ResultDtoManager.fail(-1, "对不起, 该店铺已不营业!");
			}

			if(AppUtils.isBlank(params.getShopId()) || params.getShopId() == 0){
				return ResultDtoManager.fail(StatusCodes.PARAMS_ERROR, "对不起,请求参数错误!");
			}
			
			AppPageSupport<AppStoreProdDto> result = appProductService.storeProdctList(params);
			
			if (AppUtils.isBlank(result.getResultList())) {
				return ResultDtoManager.fail(-1, "暂无商品");
			}
			
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("获取店铺全部商品异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	
	/**
	 * 店铺最新产品(上新)
	 * @param shopId
	 * @return
	 */
	@ApiOperation(value = "店铺上新", httpMethod = "POST", notes = "店铺最新产品(上新)",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="path", name = "shopId", value = "店铺id", required = true, dataType = "Long")
	@PostMapping(value="/shop/newProdList/{shopId}")
	public ResultDto<List<AppStoreProdDto>> storeNewProdctList(@PathVariable Long shopId){
		
		try {

			//获取店铺信息详情
			ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);

			if (AppUtils.isBlank(shopDetail)) {
				return ResultDtoManager.fail(-1, "对不起,您要查看的店铺信息不存在!");
			}
			if (AppUtils.isNotBlank(shopDetail.getStatus())
					&& shopDetail.getStatus() == 0) {
				return ResultDtoManager.fail(-1, "对不起, 该店铺已不营业!");
			}

			List<AppStoreProdDto> result = appProductService.getStoreNewProds(shopId, 9);
			
			if (AppUtils.isBlank(result)) {
				return ResultDtoManager.fail(-1, "该店铺暂无商品");
			}
			
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("获取店铺最新商品异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 收藏店铺
	 * @param shopId
	 * @return
	 */
	@ApiOperation(value = "收藏店铺", httpMethod = "POST", notes = "收藏店铺,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="path", name = "shopId", value = "店铺id", required = true, dataType = "Long")
	@PostMapping(value="/p/shop/addFavoriteShop/{shopId}")
	public ResultDto<String> addFavoriteShop(@PathVariable Long shopId){
		
		try{
			LoginedUserInfo appToken = loginedUserService.getUser();
			String userId = appToken.getUserId();
			String userName = appToken.getUserName();
			
			if(favoriteShopService.isExistsFavoriteShop(userId,shopId)){
				 return ResultDtoManager.fail(-1, "您已收藏该店铺，无须重复收藏");
			}
			
			FavoriteShop favoriteShop = new FavoriteShop();
			favoriteShop.setShopId(shopId);
			favoriteShop.setUserId(userId);
			favoriteShop.setUserName(userName);
			favoriteShop.setRecDate(new Date());
			favoriteShopService.savefavoriteShop(favoriteShop);
			return ResultDtoManager.success();
			
		}catch (Exception e) {
			LOGGER.error("收藏店铺失败", e);
			return ResultDtoManager.fail(StatusCodes.UNKNOWN_ERROR, "对不起, 未知错误, 请重试或联系客服!");
		}
	}
	
	/**
	 * 取消收藏店铺
	 * @param shopId
	 * @return
	 */
	@ApiOperation(value = "取消收藏店铺", httpMethod = "POST", notes = "取消收藏店铺,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="path", name = "shopId", value = "店铺id", required = true, dataType = "Long")
	@PostMapping(value="/p/shop/cancelFavoriteShop/{shopId}")
	public ResultDto<String> cancelFavoriteShop(@PathVariable Long shopId){
		
		try{
			LoginedUserInfo appToken = loginedUserService.getUser();
			
			String userId = appToken.getUserId();
			
			if(favoriteShopService.deletefavoriteShopByShopIdAndUserId(shopId, userId)){
				return ResultDtoManager.success();
			}else{
				return ResultDtoManager.fail(StatusCodes.UNKNOWN_ERROR, "对不起, 取消收藏失败, 未知错误!");
			}
		}catch (Exception e) {
			LOGGER.debug("店铺首页取消收藏异常!", e);
			return ResultDtoManager.fail(StatusCodes.UNKNOWN_ERROR, "对不起, 取消收藏失败, 未知错误!");
		}
	}

	/**
	 * 获取营业执照信息
	 */
	@ApiOperation(value = "根据店铺id获取营业执照信息", produces = MediaType.APPLICATION_JSON_VALUE)
	@GetMapping(value = "/shop/business/message")
	@ApiImplicitParam(name = "shopId", paramType = "query", value = "店铺id", required = true, dataType = "Long")
	public ResultDto<BusinessMessageDTO> getBusinessMessage(Long shopId) {
		return ResultDtoManager.success(shopDetailService.getBusinessMessage(shopId));
	}
}
