package com.legendshop.app.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.util.PhotoPathResolver;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;

import springfox.documentation.annotations.ApiIgnore;

/**
 * 图片配置
 *
 */
@RestController
@RequestMapping("/images/config")
@ApiIgnore
public class AppImagesConfigController {

	@Resource(name="scaleList")
	private Map<String, List<Integer>> scaleList;

	/**
	 * 参数初始化
	 * 
	 * @return
	 */
	@PostMapping
	public ResultDto<Object> config() {
		JSONObject config = new JSONObject();
		// 获取原图
		String photoPrefix = PhotoPathResolver.getInstance().calculateContextPath().getFilePath();
		config.put("photo", photoPrefix);

		// 获取略缩图
		Set<Map.Entry<String, List<Integer>>> entry = scaleList.entrySet();
		Iterator<Map.Entry<String, List<Integer>>> iterator = entry.iterator();

		JSONObject images = new JSONObject();
		
		while (iterator.hasNext()) {
			Map<String, Object> map = new HashMap<String, Object>();
			Map.Entry<String, List<Integer>> keyVal = iterator.next();
			String scale = keyVal.getKey();
			String imagePrefix = PhotoPathResolver.getInstance().calculateImagesPrefixPath(scale).getFilePath();
			String params = PhotoPathResolver.getInstance().calculateImageSuffixPath(scale).getFilePath();
			map.put("prefix", imagePrefix);
			map.put("params", params);
			images.put(scale, map);
		}

		config.put("images", images);
		return ResultDtoManager.success(config);
	}

}
