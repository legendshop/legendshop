package com.legendshop.app.service;

import java.util.List;

import com.legendshop.app.dto.AppShopProdCategoryDto;

/**
 * app店铺商品分类service
 * @author linzh
 */
public interface AppShopCategoryService {

	/**
	 * 获取店铺商品分类
	 * @param shopId
	 * @return
	 */
	public abstract List<AppShopProdCategoryDto> getFirstShopCategory(Long shopId);

}
