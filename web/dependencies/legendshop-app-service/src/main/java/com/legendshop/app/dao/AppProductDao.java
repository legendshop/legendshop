/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dao;

import java.util.List;

import com.legendshop.app.dto.AppCommentReplyDto;
import com.legendshop.app.dto.AppProdDto;
import com.legendshop.app.dto.AppStoreProdDto;
import com.legendshop.biz.model.dto.AppProdCommDto;
import com.legendshop.dao.GenericDao;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.AppQueryProductCommentsParams;
import com.legendshop.model.dto.app.store.AppStoreSearchParams;
import com.legendshop.model.entity.Product;

/**
 * App商品Dao.
 */
public interface AppProductDao extends GenericDao<Product, Long>{

	public AppPageSupport<AppStoreProdDto> getStoreProds(AppStoreSearchParams params);

	public AppPageSupport<AppCommentReplyDto> queryCommentReplyList(Long prodComId, Integer curPageNO);

	public AppPageSupport<AppProdCommDto> queryProductComments(AppQueryProductCommentsParams params);
	
	public List<AppStoreProdDto> getAppHotProd(Long shopId, int maxNum);

	public List<AppStoreProdDto> getAppNewProds(Long shopId, int maxNum);

	/**
	 *  获取指定排序规则的商品
	 * @param groupConditional 排序条件
	 * @param pageCount 获取数量
	 * @return
	 */
	public List<AppProdDto> queryProductByOrder(String groupConditional, Integer pageCount);

	/**
	 * 获取自定义商品分组内的商品
	 * @param prodGroupId 商品分组ID
	 * @param sort 组内排序
	 * @param pageCount 获取数量
	 * @return
	 */
	public List<AppProdDto> queryProductByProdGroup(Long prodGroupId, String sort, Integer pageCount);

	/**
	 * 根据分类ID获取商品
	 * @param categoryId 分类ID
	 * @param pageCount 获取数量
	 * @return
	 */
	public List<AppProdDto> queryProductByCategory(Long categoryId, Integer pageCount);

	
	/**
	 * 获取指定排序规则的所有商品
	 * @param groupConditional 排序规则
	 * @return
	 */
	public AppPageSupport<AppProdDto> queryProductByConditional(String groupConditional,Integer curPageNO);

	/**
	 * 获取分组内的所有商品
	 * @param prodGroupId 分组ID
	 * @param sort 组内排序
	 * @param curPageNO 当前页码
	 * @return
	 */
	public AppPageSupport<AppProdDto> queryProductByProdGroupId(Long prodGroupId, String sort, Integer curPageNO);

	/**
	 * 根据店铺分类ID获取商品
	 * @param shopCategoryId 店铺分类ID
	 * @param pageCount 获取数量
	 * @param grade 分类等级
	 * @return
	 */
	public List<AppProdDto> queryProductByShopCategory(Long shopCategoryId,Integer grade, Integer pageCount);

}
