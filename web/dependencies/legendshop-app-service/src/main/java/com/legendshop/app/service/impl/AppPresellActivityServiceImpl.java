package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.legendshop.model.dto.ProdPicDto;
import com.legendshop.model.dto.PropValueImgDto;
import com.legendshop.model.dto.app.AppPresellProdDto;
import com.legendshop.model.dto.presell.PresellProdDto;
import com.legendshop.model.dto.presell.PresellProdNewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.AppActiveBanner;
import com.legendshop.app.dto.presell.AppPresellActivityDto;
import com.legendshop.app.dto.presell.AppPresellProdDetailDto;
import com.legendshop.app.service.AppPresellActivityService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.ActiveBannerTypeEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.presell.PresellProdDetailDto;
import com.legendshop.model.dto.search.ProductSearchParms;
import com.legendshop.model.entity.ActiveBanner;
import com.legendshop.model.entity.PresellProd;
import com.legendshop.spi.service.ActiveBannerService;
import com.legendshop.spi.service.PresellProdService;
import com.legendshop.util.AppUtils;

@Service("appPresellActivityService")
public class AppPresellActivityServiceImpl implements AppPresellActivityService{

	@Autowired
	private PresellProdService presellProdService;
	
	@Autowired
	private ActiveBannerService activeBannerService;

	@Override
	public AppPageSupport<AppPresellActivityDto> queryPresellActivityList(String curPageNO, String status) {
		ProductSearchParms parms  = new ProductSearchParms();
		parms.setTag(status);
		PageSupport<PresellProd> ps = presellProdService.queryPresellProdListPage(curPageNO,parms);
		
		return PageUtils.convertPageSupport(ps, new PageUtils.ResultListConvertor<PresellProd, AppPresellActivityDto>() {
			@Override
			public List<AppPresellActivityDto> convert(List<PresellProd> form) {
				List<AppPresellActivityDto> resultList = new ArrayList<AppPresellActivityDto>();
				for (PresellProd presellProd : form) {
					AppPresellActivityDto dto = new AppPresellActivityDto();
					dto.setId(presellProd.getId());
					dto.setProdId(presellProd.getProdId());
					dto.setSkuId(presellProd.getSkuId());
					dto.setShopId(presellProd.getShopId());
					dto.setSchemeName(presellProd.getSchemeName());
					dto.setPrePrice(presellProd.getPrePrice());
					dto.setPreSaleStart(presellProd.getPreSaleStart());
					dto.setPreSaleEnd(presellProd.getPreSaleEnd());
					dto.setStatus(presellProd.getStatus());
					dto.setProdName(presellProd.getProdName());
					dto.setSkuPic(presellProd.getSkuPic());
					dto.setProdPic(presellProd.getProdPic());
					resultList.add(dto);
				}
				return resultList;
			}
		});
	}

	@Override
	public AppPresellProdDetailDto getPresellProdDetailDto(Long presellId) {
		PresellProdDetailDto presellProdDetail = presellProdService.getPresellProdDetailDto(presellId);
		if(AppUtils.isBlank(presellProdDetail)){
			return null;
		}
		AppPresellProdDetailDto dto = new  AppPresellProdDetailDto();
		//转换为app预售商品详情Dto
		dto = convertToAppPresellProdDetailDto(dto, presellProdDetail);
		return dto;
	}
	
	private AppPresellProdDetailDto convertToAppPresellProdDetailDto(AppPresellProdDetailDto dto, PresellProdDetailDto presellProdDetail){
		//活动信息
		dto.setId(presellProdDetail.getId());
		dto.setProdId(presellProdDetail.getProdId());
		dto.setSkuId(presellProdDetail.getSkuId());
		dto.setPrePrice(presellProdDetail.getPrePrice());
		dto.setPreDepositPrice(presellProdDetail.getPreDepositPrice());
		dto.setFinalPayment(presellProdDetail.getFinalPayment());
		dto.setPreSaleStart(presellProdDetail.getPreSaleStart());
		dto.setPreSaleEnd(presellProdDetail.getPreSaleEnd());
		dto.setPayPctType(presellProdDetail.getPayPctType());
		dto.setPayPct(presellProdDetail.getPayPct());
		dto.setStatus(presellProdDetail.getStatus());
		dto.setFinalMStart(presellProdDetail.getFinalMStart());
		dto.setFinalMEnd(presellProdDetail.getFinalMEnd());
		dto.setPreDeliveryTime(presellProdDetail.getPreDeliveryTime());
		//商品信息
		dto.setModelId(presellProdDetail.getModelId());
		dto.setProdName(presellProdDetail.getProdName());
		dto.setCategoryId(presellProdDetail.getCategoryId());
		dto.setUserName(presellProdDetail.getUserName());
		dto.setShopId(presellProdDetail.getShopId());
		dto.setKeyWord(presellProdDetail.getKeyWord());
		dto.setBrief(presellProdDetail.getBrief());
		dto.setProdPrice(presellProdDetail.getProdPrice());
		dto.setProdCash(presellProdDetail.getProdCash());
		dto.setProdMainPic(presellProdDetail.getProdMainPic());
		dto.setProdStocks(presellProdDetail.getProdStocks());
		dto.setContentM(presellProdDetail.getContentM());
		dto.setProdPics(presellProdDetail.getProdPics());
		//SKU 信息
		dto.setSkuName(presellProdDetail.getSkuName());
		dto.setSkuPrice(presellProdDetail.getSkuPrice());
		dto.setSkuPic(presellProdDetail.getSkuPic());
		dto.setProperties(presellProdDetail.getProperties());
		dto.setPropKeyValues(presellProdDetail.getPropKeyValues());
		dto.setPropIds(presellProdDetail.getPropIds());
		dto.setPropValueIds(presellProdDetail.getPropValueIds());
		dto.setSkuStatus(presellProdDetail.getSkuStatus());
		dto.setSkuStocks(presellProdDetail.getSkuStocks());
		dto.setSkuActualStocks(presellProdDetail.getSkuActualStocks());
		dto.setProdPropDtos(presellProdDetail.getProdPropDtos());
		dto.setPropValImageUrls(presellProdDetail.getPropValImageUrls());

		AppPresellProdDto prodDto = new AppPresellProdDto();
		PresellProdNewDto prodNewDto = presellProdDetail.getProdDto();
		copyProdDto(prodNewDto,prodDto);

		dto.setProdDto(prodDto);
		long nowTime = Calendar.getInstance().getTimeInMillis();
		//根据时间获取状态
		if(nowTime < dto.getPreSaleStart().getTime()){ //未开始
			dto.setBuyStatus(0);
		}else if(nowTime >= dto.getPreSaleEnd().getTime()){ //已结束
			dto.setBuyStatus(-1);
		}else {
			dto.setBuyStatus(1); //进行中
		}
		return dto;
	}

	private void copyProdDto(PresellProdNewDto prodNewDto, AppPresellProdDto prodDto) {
		prodDto.setTransportId(prodNewDto.getTransportId());
		prodDto.setModelId(prodNewDto.getModelId());
		prodDto.setImgFileList(prodNewDto.getImgFileList());
		prodDto.setPic(prodNewDto.getPic());
		prodDto.setProdId(prodNewDto.getProdId());
		prodDto.setContent(prodNewDto.getContent());
		prodDto.setStocks(prodNewDto.getStocks());
		prodDto.setProdPropDtoList(prodNewDto.getProdPropDtoList());
		prodDto.setExpressTransFee(prodNewDto.getExpressTransFee());
		prodDto.setEmsTransFee(prodNewDto.getEmsTransFee());
		prodDto.setTransportType(prodNewDto.getTransportType());
		prodDto.setSkuDtoList(prodNewDto.getSkuDtoList());
		prodDto.setContentM(prodNewDto.getContentM());
		prodDto.setSkuDtoListJson(prodNewDto.getSkuDtoListJson());
		prodDto.setPropImageDtoList(prodNewDto.getPropImageDtoList());
		prodDto.setCash(prodNewDto.getCash());
		prodDto.setSkuId(prodNewDto.getSkuId());
		prodDto.setBrief(prodNewDto.getBrief());
		prodDto.setPrePrice(prodNewDto.getPrePrice());
		prodDto.setPropValueImgListJson(prodNewDto.getPropValueImgListJson());
		prodDto.setProVideoUrl(prodNewDto.getProVideoUrl());
		prodDto.setMailTransFee(prodNewDto.getMailTransFee());
		prodDto.setName(prodNewDto.getName());
		prodDto.setPropValueImgList(prodNewDto.getPropValueImgList());
		prodDto.setProdPics(convertProductPicDto(prodNewDto.getProdPics(), prodNewDto.getPropValueImgList()));
		prodDto.setSupportTransportFree(prodNewDto.getSupportTransportFree());
		prodDto.setCategoryId(prodNewDto.getCategoryId());
		prodDto.setPropertyImageDtoMap(prodNewDto.getPropertyImageDtoMap());
		prodDto.setStatus(prodNewDto.getStatus());
	}

	@Override
	public List<AppActiveBanner> getActiveBanners() {
		List<ActiveBanner> bannerList = activeBannerService.getBannerList(ActiveBannerTypeEnum.PRESELL_BANNER.value());
		
		List<AppActiveBanner> resultList = new ArrayList<AppActiveBanner>();
		for(ActiveBanner activeBanner : bannerList){
			AppActiveBanner appActiveBanner = new AppActiveBanner();
			appActiveBanner.setId(activeBanner.getId());
			appActiveBanner.setPic(activeBanner.getImageFile());
			appActiveBanner.setUrl(activeBanner.getUrl());
			
			resultList.add(appActiveBanner);
		}
		return resultList;
	}

	private String processName(String name) {
		if (name == null) {
			return null;
		}
		name = name.replace("\"", "〞");
		name = name.replace("'", "’");
		return name;
	}

	private List<String> convertProductPicDto(List<ProdPicDto> prodPicDtoList, List<PropValueImgDto> propValImgList){
		List<String> productPics = new ArrayList<String>();
		if(AppUtils.isNotBlank(propValImgList)){
			for (PropValueImgDto propValImg:propValImgList) {
				productPics.addAll(propValImg.getImgList());
			}
		}else{
			if(AppUtils.isNotBlank(prodPicDtoList)){
				for (ProdPicDto prodPicDto : prodPicDtoList) {
					productPics.add(prodPicDto.getFilePath());
				}
			}
		}
		return productPics;
	}
	
}
