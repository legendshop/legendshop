package com.legendshop.app.service.impl;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.legendshop.model.entity.UserGrade;
import com.legendshop.spi.service.UserGradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.AppUserDetailService;
import com.legendshop.model.dto.app.AppUserDetailDto;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.UserSecurity;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;

/**
 * app 用户信息service实现
 */
@Service("appUserDetailService")
public class AppUserDetailServiceImpl implements AppUserDetailService {

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private UserGradeService userGradeService;
	
	@Override
	public AppUserDetailDto getUserDetail(String userId) {
		
		if (AppUtils.isNotBlank(userId)) {
			UserDetail userDetail = userDetailService.getUserDetailById(userId);
			
			if(userDetail == null){//防止用户被删除，App访问失效
				return null;
			}
			
			if(AppUtils.isNotBlank(userDetail.getBirthDate())){//从生日中提取 年月日 
				try {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-d");  
					 Calendar cal = Calendar.getInstance();  
					 //Date date = sdf.parse(userDetail.getBirthDate());
					 cal.setTime(userDetail.getBirthDate());
					 userDetail.setYear(cal.get(Calendar.YEAR));
					 userDetail.setMonth(cal.get(Calendar.MONTH)+1);
					 userDetail.setDay(cal.get(Calendar.DATE));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return convertUserDetailDto(userDetail);
		}
		return null;
	}
	
	private AppUserDetailDto convertUserDetailDto(UserDetail userDetail){
		if(AppUtils.isNotBlank(userDetail)){
			//获取用户会员等级信息
			UserGrade userGrade = userGradeService.getUserGrade(userDetail.getGradeId());

			AppUserDetailDto userDetailDto = new AppUserDetailDto();
			userDetailDto.setPortraitPic(userDetail.getPortraitPic());
			userDetailDto.setSex(userDetail.getSex());
			userDetailDto.setNickName(userDetail.getNickName());
			userDetailDto.setUserId(userDetail.getUserId());
			userDetailDto.setUserName(userDetail.getUserName());
			userDetailDto.setBirthDate(userDetail.getBirthDate());
			userDetailDto.setAvailablePredeposit(userDetail.getAvailablePredeposit());
			userDetailDto.setPayPassword(userDetail.getPayPassword());
			userDetailDto.setMobile(userDetail.getUserMobile());
			userDetailDto.setRealName(userDetail.getRealName());
			userDetailDto.setUserSignature(userDetail.getUserSignature());
			userDetailDto.setUserGradeName(userGrade.getName());
			return userDetailDto;
		}
		return null;
	}

	/**
	 * 更新用户昵称
	 * 1:  成功
	 * 0: 昵称已经存在
	 * -1: 用户不存在
	 */
	@Override
	public int updateNickName(String userId, String nickName) {
		if(userDetailService.isNickNameExist(nickName)){
			return 0;
		}else{
			UserDetail userDetail = userDetailService.getUserDetailById(userId);
			if(userDetail != null){
				userDetail.setNickName(nickName);
				userDetailService.updateUserDetail(userDetail);
				return 1;
			}else{
				return -1;
			}
		}

	}


	/**
	 * 更新用户签名
	 * 1:  成功
	 * -1: 用户不存在
	 */
	@Override
	public int updateSignature(String userId, String userSignature) {
			UserDetail userDetail = userDetailService.getUserDetailById(userId);
			if(userDetail != null){
				userDetail.setUserSignature(userSignature);
				userDetailService.updateUserDetail(userDetail);
				return 1;
			}else{
				return -1;
			}

	}
	
	/**
	 * 更新用户姓名
	 */
	@Override
	public int updateRealName(String userId, String realName) {

		UserDetail userDetail = userDetailService.getUserDetailById(userId);
		if(userDetail != null){
			userDetail.setRealName(realName);
			userDetailService.updateUserDetail(userDetail);
			return 1;
		}else{
			return -1;
		}
	}

	/**
	 * 更新性别
	 */
	@Override
	public int updateSex(String userId, String sex) {
		UserDetail userDetail = userDetailService.getUserDetailById(userId);
		if(userDetail != null){
			userDetail.setSex(sex);
			userDetailService.updateUserDetail(userDetail);
			return 1;
		}else{
			return -1;
		}
	}

	/**
	 * 更新头像
	 */
	@Override
	public int updatePortrait(String userId, String portraitPic) {
		UserDetail userDetail = userDetailService.getUserDetailById(userId);
		if(userDetail != null){
			userDetail.setPortraitPic(portraitPic);
			userDetailService.updateUserDetail(userDetail);
			return 1;
		}else{
			return -1;
		}
	}

	/**
	 * @Description: 获取用户安全数据
	 * @param @param userName
	 * @param @return   
	 * @date 2016-7-14
	 */
	@Override
	public UserSecurity getUserSecurity(String userName) {
		return userDetailService.getUserSecurity(userName);
	}

	@Override
	public String getUserMobile(String userId) {
		return userDetailService.getUserMobile(userId);
	}

	@Override
	public int updateBirthDate(String userId, Date birthDate) {
		
		UserDetail userDetail = userDetailService.getUserDetailById(userId);
		if(userDetail != null){
			userDetail.setBirthDate(birthDate);
			userDetailService.updateUserDetail(userDetail);
			return 1;
		}else{
			return -1;
		}
	}
}
