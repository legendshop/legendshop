package com.legendshop.app.dto ;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app客服会话服务Dto
 */
@ApiModel(value="app客服会话服务")
public class AppDialogueDto  {

	/** 会话记录id */
	@ApiModelProperty(value="会话记录id, 唯一标识")
	private Long id; 
		
	/** 会话服务编号 */
	@ApiModelProperty(value="会话服务编号, 唯一标识")
	private String serviceNo; 
	
	/** 商家ID */
	@ApiModelProperty(value="咨询店铺的ID, 为0代表平台客服")
	private Long shopId; 
	
	/**店铺名称*/
	@ApiModelProperty(value="咨询店铺的名称")
	private String shopName;
	
	/** 店铺Logo */
	@ApiModelProperty(value="咨询店铺的Logo")
	private String shopPic;
		
	/** 客服ID */
	@ApiModelProperty(value="客服ID")
	private Long customId; 
	
	/**客服名称**/
	@ApiModelProperty(value="接待的客服名称")
	private String customName;
	
	/** 离线消息数量 */
	@ApiModelProperty(value="离线消息数量")
	private Long offLineCount;
	
	/**最新离线消息时间**/
	@ApiModelProperty(value="最新离线消息时间")
	private Date offLineDate;
	
	/** 最新离线消息内容 */
	@ApiModelProperty(value="最新离线消息内容")
	private String offLineContent;
	
	/**咨询类型, 咨询商品: prod, 咨询退款单: order **/
	@ApiModelProperty(value="咨询类型, 咨询商品: prod, 咨询退款单: order")
	private String consultType;
	
	/** 咨询来源渠道, PC: pc端, H5: h5端, APP: APP端 */
	@ApiModelProperty(value="咨询来源渠道, PC: pc端, H5: h5端, APP: APP端")
	private String source; 
		
	/** 状态, 0:会话中, 1:已结束 */
	@ApiModelProperty(value="状态, 0:会话中, 1:已结束")
	private Integer status; 
	
	/** 会话创建时间 */
	@ApiModelProperty(value="会话创建时间")
	private Date createTime; 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getServiceNo() {
		return serviceNo;
	}

	public void setServiceNo(String serviceNo) {
		this.serviceNo = serviceNo;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public Long getCustomId() {
		return customId;
	}

	public void setCustomId(Long customId) {
		this.customId = customId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getOffLineCount() {
		return offLineCount;
	}

	public void setOffLineCount(Long offLineCount) {
		this.offLineCount = offLineCount;
	}

	public Date getOffLineDate() {
		return offLineDate;
	}

	public void setOffLineDate(Date offLineDate) {
		this.offLineDate = offLineDate;
	}

	public String getOffLineContent() {
		return offLineContent;
	}

	public void setOffLineContent(String offLineContent) {
		this.offLineContent = offLineContent;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopPic() {
		return shopPic;
	}

	public void setShopPic(String shopPic) {
		this.shopPic = shopPic;
	}

	public String getConsultType() {
		return consultType;
	}

	public void setConsultType(String consultType) {
		this.consultType = consultType;
	}

	public String getCustomName() {
		return customName;
	}

	public void setCustomName(String customName) {
		this.customName = customName;
	}
} 
