package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("种草文章商品Dto")
public class AppGrassProdDto {
	@ApiModelProperty(value="商品id")
	private Long prodId;
	
	@ApiModelProperty(value="商品名字")
	private String name;
	
	@ApiModelProperty(value="商品价格")
	private Double price;
	
	@ApiModelProperty(value="评论数")
	private Long comments;
	
	@ApiModelProperty(value="评分")
	private Integer goodCommentsPercent;
	
	@ApiModelProperty(value="图片")
	private String pic;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Long getComments() {
		return comments;
	}

	public void setComments(Long comments) {
		this.comments = comments;
	}

	public Integer getGoodCommentsPercent() {
		return goodCommentsPercent;
	}

	public void setGoodCommentsPercent(Integer goodCommentsPercent) {
		this.goodCommentsPercent = goodCommentsPercent;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}
	

	
	
	
	
}
