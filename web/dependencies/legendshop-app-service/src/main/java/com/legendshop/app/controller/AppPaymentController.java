package com.legendshop.app.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.entity.Passport;
import com.legendshop.spi.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.app.dto.AppPaymentResultDto;
import com.legendshop.base.log.PaymentLog;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.service.impl.PaymentUtil;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.framework.cache.client.CacheClient;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.PaySourceEnum;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.constant.SubSettlementTypeEnum;
import com.legendshop.model.dto.app.AppPaytoParamsDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.payment.InitPayFromDto;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.UserSecurity;
import com.legendshop.model.form.SysPaymentForm;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import com.legendshop.web.helper.IPHelper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

/**
 * APP支付中心
 */
@Controller
@Api(tags="支付中心",value="支付中心")
public class AppPaymentController {

	/** The log. */
	private final static Logger log = LoggerFactory.getLogger(AppPaymentController.class);

	@Autowired
	private PaymentUtil paymentUtil;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private PayTypeService payTypeService;

	@Autowired
	private UserDetailService  userDetailService;

	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;

    @Autowired
    private PasswordEncoder passwordEncoder;

	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private CacheClient cacheClient;

	@Autowired
	private SubService subService;

	@Autowired
	private PdRechargeService pdRechargeService;

	/** Session token 失效时间 */
	private static final int SESSION_TOKEN_EXPIRE = 60 * 10;

	/**
	 * 获取支付方式
	 * @return
	 */
	@ApiOperation(value = "获取支付方式", httpMethod = "POST", notes = "获取支付方式",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/p/payment/payTypes")
	@ResponseBody
	public ResultDto<JSONObject>  payTypes() {
		// 获取 支付方式
		Map<String, Integer> payTypesMap = payTypeService.getPayTypeMap();
		List<String> payTypes = new ArrayList<String>();
		if (AppUtils.isNotBlank(payTypesMap)) {
			for (Entry<String, Integer> iterator : payTypesMap.entrySet()) {
				if (iterator.getValue() == 1) {
					payTypes.add(iterator.getKey());
				}
			}
		}
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("payTypes", payTypes);
		return ResultDtoManager.success(jsonObject);
	}

	/**
	 * 使用预付款或金币
	 * @param request
	 * @param password
	 * @param payType 1：预付款  2：金币
	 * @return
	 */
	@ApiOperation(value = "使用预付款或金币", httpMethod = "POST", notes = "使用预付款或金币1：预付款  2：金币,但是目前我们只有预付款没有金币,成功返回map对象,错误则显示提示信息;返回-1为普通错误信息，-2为未开启支付密码",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "path", dataType = "Integer", name = "payType", value = "支付类型", required = true),
	    @ApiImplicitParam(paramType = "query", dataType = "String", name = "password", value = "支付密码", required = true)
	})
	@PostMapping(value="/p/payment/userPredeposit/{payType}")
	@ResponseBody
	public  ResultDto<Object> userPredeposit(HttpServletRequest request,
			@PathVariable Integer payType, @RequestParam String password) {

	    Map<String,Object> map=new HashMap<String, Object>();
	    map.put("status", false);
	    if(AppUtils.isBlank(password)){
		   return ResultDtoManager.fail(-1,"请输入支付密码");
	    }

	    LoginedUserInfo token = loginedUserService.getUser();

	    String userName = token.getUserName();
	    UserSecurity security=userDetailService.getUserSecurity(userName);
	    if(security.getPaypassVerifn().intValue()==0){ //是否通过支付验证
		   return ResultDtoManager.fail(-2,"请开启安全支付密码");
	    }
	    String userId = token.getUserId();
	    com.legendshop.model.entity.UserDetail detail=userDetailService.getUserDetailByIdNoCache(userId);
	    if(detail.getAvailablePredeposit() <=0 && payType==1){
		   return ResultDtoManager.fail(-1,"账户余额不足,请充值");
	    }else if(detail.getUserCoin()<=0 && payType==2){
		   return ResultDtoManager.fail(-1,"金币不足,请充值");
	    }
	    if(!passwordEncoder.matches(password, detail.getPayPassword())){
	       log.warn("支付密码错误请重新输入！ userId = {}" ,  userId);
		   return ResultDtoManager.fail(-1,"支付密码错误请重新输入，如忘记支付密码，可以我的设置中重置！");
	    }

	    cacheClient.put(Constants.PASSWORD_CALLBACK + ":" + PayTypeEnum.FULL_PAY.desc() + ":" + userId, SESSION_TOKEN_EXPIRE, true);

		map.put("status", true);
		map.put(Constants.PASSWORD_CALLBACK, "true");
		return ResultDtoManager.success(map);
	}

	/**
	 * 调用支付
	 */
	@ApiOperation(value = "调用支付", httpMethod = "POST", notes = "调用支付,成功返回map对象,错误则显示提示信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "path", dataType = "String", name = "subSettlementType", value = "支付单据类型", required = true),
	})
	@PostMapping(value="/p/payment/payto/{subSettlementType}")
	@ResponseBody
	public  ResultDto<AppPaymentResultDto> payment(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String subSettlementType, AppPaytoParamsDto paytoParam) {

		PaymentLog.info("*****************调用支付***************************");
		PaymentLog.info(" subSettlementType： "+subSettlementType+"",subSettlementType);
		PaymentLog.info(" PayTypeId： "+paytoParam.getPayTypeId()+"",paytoParam.getPayTypeId());

		String [] subNumbers = paytoParam.getSubNumbers();
		if(AppUtils.isBlank(subNumbers)){
			return ResultDtoManager.fail(-1,"数据格式有误");
		}

		LoginedUserInfo token = null;
		String userId = null;
		if(!PayTypeEnum.ALI_PAY.value().equals(paytoParam.getPayTypeId())){
			token = loginedUserService.getUser();
			userId = token.getUserId();
		}else{
			if(SubSettlementTypeEnum.USER_PREPAID_RECHARGE.value().equals(subSettlementType)){
				userId = pdRechargeService.findRechargePay(subNumbers[0]).getUserId();
			}
			else{
				userId = subService.getSubBySubNumber(subNumbers[0]).getUserId();
			}

		}

		if(AppUtils.isBlank(paytoParam.getPaySource())){//不传默认为wap端
			paytoParam.setPaySource(PaySourceEnum.WAP.getDesc());
		}

		//判断是否启用全额预付款支付
		boolean walletPay = false; // 钱包支付
		if ("1".equals(paytoParam.getPasswordCallback())) {

			Object PASSWORD_CALLBACK = cacheClient.get(Constants.PASSWORD_CALLBACK + ":" + PayTypeEnum.FULL_PAY.desc()  + ":" + userId);
			if(AppUtils.isBlank(PASSWORD_CALLBACK)){
				return ResultDtoManager.fail(-1,"余额支付密码错误！");
			}

			if (AppUtils.isBlank(paytoParam.getPrePayTypeVal())) {
				return ResultDtoManager.fail(-1,"参数有误,余额支付方式不符合规则！");
			}
			walletPay = true;
		}
		PaymentLog.info(" userId={},paytoParam={},walletPay={} ",userId,JSONUtil.getJson(paytoParam),walletPay);

		/*
		 * 首先先查询这个支付单据有没有已经支付成功的单据信息
		 */
		if (paymentUtil.checkRepeatPay(subNumbers,userId, subSettlementType)) {
			PaymentLog.error("===>请勿重复支付");
			return ResultDtoManager.fail(-1,"请勿重复支付！");
		}

		String domainURL = propertiesUtil.getUserAppDomainName();
		String wapDomainURL = propertiesUtil.getVueDomainName();
		String ip = IPHelper.getIpAddr(request);
		PaymentLog.info(" ip={}, domainURL={}", ip, domainURL);

		String subSettlementSn = CommonServiceUtil.getRandomSn();

		InitPayFromDto initPayFromDto = new InitPayFromDto(subSettlementSn, subNumbers, walletPay, paytoParam.getPrePayTypeVal(), paytoParam.getPayTypeId(), userId,
				token == null ? null : token.getOpenId(), domainURL, wapDomainURL, subSettlementType, paytoParam.getPaySource(), ip, paytoParam.getPayPctType(), paytoParam.isPresell());

		Map<String, Object> payment_result = paymentUtil.initPay(initPayFromDto);
		if (!Boolean.parseBoolean(payment_result.get("result").toString())) {
			PaymentLog.info(" 支付请求参数:{}",payment_result.get("message"));
			return ResultDtoManager.fail(-1,payment_result.get("message").toString());
		}

		SysPaymentForm paymentForm=(SysPaymentForm) payment_result.get("message");
		PaymentLog.info(" paymentForm {} ##########",JSONUtil.getJson(paymentForm));

		payment_result = paymentService.payto(paymentForm.getProcessorEnum(), paymentForm);
		PaymentLog.log(" buildRequest={} ",payment_result.get("message"));
		if (!Boolean.parseBoolean(payment_result.get("result").toString())) {
			PaymentLog.error(payment_result.get("message").toString());
			return ResultDtoManager.fail(-1,payment_result.get("message").toString());
		}

		/**
		 * 初始化单据信息
		 */
		paymentUtil.saveSubSettlement(paymentForm);

		AppPaymentResultDto paymentResult = new AppPaymentResultDto();
		AppPaymentResultDto.PayInfo payInfo = new AppPaymentResultDto.PayInfo();

		if(SubSettlementTypeEnum.USER_ORDER_PAY.value().equals(subSettlementType)){
			List<Sub> subs = subService.getSubBySubNums(paytoParam.getSubNumbers(), userId, OrderStatusEnum.UNPAY.value());
			if(AppUtils.isNotBlank(subs) && subs.size() == 1){
				Sub sub = subs.get(0);
				payInfo.setSubType(sub.getSubType());
			}
		}

		payInfo.setSubSettlementSn(subSettlementSn);
		payInfo.setSubSettlementType(subSettlementType);
		payInfo.setPayTypeId(paytoParam.getPayTypeId());
		payInfo.setTotalAmount(paymentForm.getTotalAmount());

		paymentResult.setPayInfo(payInfo);
		paymentResult.setPrepayResult(payment_result.get("message").toString());

		return ResultDtoManager.success(paymentResult);
	}

    /**
     * 支付失败回调
     * @param message
     * @return
     */
	@ApiIgnore
	@GetMapping(value="/payment/payError")
	public String payError(HttpServletRequest request, HttpServletResponse response, String message) {

		PaymentLog.log("################# APP 支付错误回调 {} ######################",message);
		String vueDomainName = propertiesUtil.getVueDomainName();
		try {
			return "redirect:"+vueDomainName+"/orderPayResult" + URLEncoder.encode(message, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return "redirect:"+vueDomainName;
		}
	}
}
