/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.app.dto.AppProdDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.AppCouponDto;
import com.legendshop.app.dto.AppCouponProdDto;
import com.legendshop.app.service.AppCouponService;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.CouponProviderEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.buy.PersonalCouponDto;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.spi.service.CouponService;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.UserCouponService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.util.AppUtils;
import com.legendshop.web.util.ValidateCodeUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 优惠券红包积分相关接口
 */
@RestController
@Api(tags="优惠券红包",value="优惠券红包相关接口")
public class AppCouponController {
	
//	private final Logger log = LoggerFactory.getLogger(AppCouponController.class);
	
	@Autowired 
	private AttachmentManager attachmentManager;
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private AppCouponService appCouponService;
	
	@Autowired
	private UserCouponService userCouponService;
	
	@Autowired
	private CouponService couponService;
	
	
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;
	
	/**
	 * 领券中心
	 */
	@ApiOperation(value = "领券中心", httpMethod = "POST", notes = "领券中心",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "couponType", value = "优惠券类型:全场通用allPlatform;店铺商品券shopProd;店铺券shop", required = true, dataType = "String"),
		@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", dataType = "String")
	})
	@PostMapping("/app/couponCenter")
	public ResultDto<AppPageSupport<AppCouponDto>> couponCenter(HttpServletRequest request,String couponType,String curPageNO){
		curPageNO = AppUtils.isBlank(curPageNO) ? "1":curPageNO;
		AppPageSupport<AppCouponDto> ps = appCouponService.getCouponByCouponType(curPageNO,couponType);
		return ResultDtoManager.success(ps);
	}

	/**
	 * 领取优惠卷(新)
	 * 2018-12-4 日改造，旧的方法也在用，逻辑没有改，只是改变了返回类型
	 * @param request
	 * @param response
	 * @param couponId
	 * @return
	 */
	@ApiOperation(value = "领取优惠券", httpMethod = "POST",
		notes = "领取优惠券,返回'OK'表示成功处理并返回map对象，对象的status为状态:只有'OK'为领取成功其余都是失败（显示失败原因），对象的msg为提示信息，如果领取成功还会返回券号（通过对象的couponSn获取），返回'fail'表示出现未知失败",
		produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "couponId", value = "优惠券id", required = true, dataType = "string")
	@PostMapping("/p/app/coupon/newReceive/{couponId}")
	public ResultDto<Object> newReceive(HttpServletRequest request, HttpServletResponse response, @PathVariable Long couponId){
		LoginedUserInfo token = loginedUserService.getUser();
		String userName = token.getUserName();
		String userId = token.getUserId();
		Map<String, Object> result = new HashMap<String, Object>();
 		if (AppUtils.isBlank(userName)) {
 			result.put("status", "NOLOGIN");
 			result.put("msg", "您还没登录");
			return ResultDtoManager.success(result);
		} else {
			Date date = new Date();
			Coupon coupon = couponService.getCoupon(couponId);
			UserDetail userDetail = userDetailService.getUserDetailById(token.getUserId());
			if ( AppUtils.isNotBlank(userDetail.getShopId()) && userDetail.getShopId().equals(coupon.getShopId())){
				result.put("status", Constants.FAIL);
				result.put("msg", "您不能领取自己店铺的优惠券~~");
				return ResultDtoManager.success(result);
			}
			if (coupon == null || coupon.getStatus() == Constants.OFFLINE.intValue()) {
				result.put("status", Constants.FAIL);
	 			result.put("msg", "当前券已失效，快寻找新目标吧~");
	 			return ResultDtoManager.success(result);
			} else if (date.getTime() > coupon.getEndDate().getTime()) {
				result.put("status", Constants.FAIL);
				result.put("msg", "当前券已失效，快寻找新目标吧~");
				return ResultDtoManager.success(result);
			} else {
				if (coupon.getGetLimit() != null) {
					List<UserCoupon> userCouponList = userCouponService.queryUserCouponList(userName,couponId);
					if (userCouponList != null && userCouponList.size() >= coupon.getGetLimit()) {
						result.put("status", Constants.FAIL);
						result.put("msg", "领取失败，您该张券的领取量已超限额~");
						return ResultDtoManager.success(result);
					}
				}
				if (coupon.getBindCouponNumber() >= coupon.getCouponNumber()) {
					result.put("status", Constants.FAIL);
					result.put("msg", "领取失败，当前券已领完，快寻找其他目标~");
					return ResultDtoManager.success(result);
				}
				result = userCouponService.newAppCouponReceive(coupon, userDetail);
				return ResultDtoManager.success(result);
			}
		}
	}

	/**
	 * 我的优惠券
	 */
	@ApiOperation(value = "我的优惠券", httpMethod = "POST", notes = "我的优惠券",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "curPageNO", value = "分页", required = false, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "useStatus", value = "使用状态,1代表未使用，2代表已使用,不传值就是已过期的", required = false, dataType = "string")
	})
	@PostMapping("/p/myCoupon")
	public ResultDto<AppPageSupport<PersonalCouponDto>> myCoupon(String curPageNO, Integer useStatus){

		String userId = loginedUserService.getUser().getUserId();
		AppPageSupport<PersonalCouponDto> pageDto = appCouponService.queryCoupon(userId, curPageNO, useStatus);

		return ResultDtoManager.success(pageDto);
	}

	/**
	 * 我的红包
	 */
	@ApiOperation(value = "我的红包", httpMethod = "POST", notes = "我的红包",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "curPageNO", value = "分页", required = false, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "useStatus", value = "使用状态,1代表未使用，2代表已使用,不传值就是已过期的", required = false, dataType = "string")
	})
	@PostMapping("/p/myRedPack")
	public ResultDto<AppPageSupport<PersonalCouponDto>> myRedPack(String curPageNO, Integer useStatus){

		String userId = loginedUserService.getUser().getUserId();
		AppPageSupport<PersonalCouponDto> pageDto = appCouponService.queryRedPack(userId, curPageNO, useStatus);

		return ResultDtoManager.success(pageDto);
	}

	/**
	 * 激活优惠券(领取优惠券), TODO 该方法暂不能使用
	 * @param couponPwd 卡密
	 * @param verifyCode 验证码
	 */
	@PostMapping("/p/getCoupon")
	@ApiIgnore
	public ResultDto<Object> getCoupon(HttpServletRequest request,String verifyCode, String couponPwd){

		String userId = loginedUserService.getUser().getUserId();
		String userName = loginedUserService.getUser().getUserName();

		if(AppUtils.isBlank(verifyCode) || !ValidateCodeUtil.validateCodeFromCache(verifyCode)){
    		return ResultDtoManager.fail(-1, "亲, 您输入的图形验证码不正确!");
    	}

		//判断激活码是否为空
		if(AppUtils.isBlank(couponPwd)){
			return ResultDtoManager.fail(-1, "亲, 您还没有输入卡密呢!");
		}

		UserCoupon userCoupon = userCouponService.getCouponByCouponPwd(couponPwd);
		if(userCoupon == null){
			return ResultDtoManager.fail(-1, "亲, 您输入的卡密不正确哦!");
		}

		if(AppUtils.isNotBlank(userCoupon.getUserId())) {
			return ResultDtoManager.fail(-1, "亲, 该优惠券已被领取了哦, 换一个试试吧!");
		}

		long couponId = userCoupon.getCouponId();
		Coupon coupon = couponService.getCoupon(couponId);
		//判断输入的礼券激活码是否是对应的礼券，比如红包激活码不能激活优惠券激活码
		if(!(CouponProviderEnum.SHOP.value().equals(coupon.getCouponProvider()))){
			return ResultDtoManager.fail(-1, "亲, 您输入的卡密不正确哦!");
		}

		//判断礼券是否已经下线
		int status = coupon.getStatus();
		if(status == 0){
			return ResultDtoManager.fail(-1, "亲, 该优惠券已下线不能领取哦!");
		}

		//查询用户已经领取了几张优惠券
		int number = userCouponService.getUserCouponCount(userId, couponId);

		//判断是否已经达到领取上限
		long limit = coupon.getGetLimit();
		if(number >= limit){
			return ResultDtoManager.fail(-1, "亲, 您领取的优惠券以达到上限了哦!");
		}

		//判断时候已经超过活动结束时间
		Date endDate = coupon.getEndDate();
		Date nowDate = new Date();
		if(nowDate.getTime() > endDate.getTime()){
			return ResultDtoManager.fail(-1, "亲, 活动已结束了哦, 下次再来吧!");
		}

		//初始化优惠券数量
		long initNumber = coupon.getCouponNumber();
		//已绑定优惠券数量
		long bindNumber = coupon.getBindCouponNumber();
		if(bindNumber >= initNumber){
			return ResultDtoManager.fail(-1, "亲, 优惠券已经领完了哦, 下次再来吧!");
		}

		couponService.activateCoupon(userId, userName, coupon, userCoupon);
		return ResultDtoManager.success();
	}


	/**
	 * 激活优惠券或红包
	 */
	@ApiOperation(value = "激活优惠券或红包", httpMethod = "POST", notes = "激活优惠券或红包",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "couPro", value = "优惠券提供方：平台: platform，店铺:shop", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "verifyCode", value = "验证码", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "couponPwd", value = "卡密", required = true, dataType = "string")
	})
	@PostMapping("/p/app/activateCoupon")
	public ResultDto<Object> activateCoupon(String couPro,String verifyCode, String couponPwd){
		//验证码验证
    	if(AppUtils.isBlank(verifyCode) || !ValidateCodeUtil.validateCodeFromCache(verifyCode)){
    		return ResultDtoManager.fail(-1, "请输入正确的图形验证码");
    	}
		//判断激活码是否为空
		if(AppUtils.isBlank(couponPwd)){
			return ResultDtoManager.fail(-1, "请输入卡密号");
		}else{
			//输入激活码，查出coupon，判断是否存在和正确
			UserCoupon userCoupon = userCouponService.getCouponByCouponPwd(couponPwd);
			if(userCoupon == null){
				return ResultDtoManager.fail(-1, "请输入正确卡密号");
			}else if(userCoupon.getUserId() == null){
					String userId = loginedUserService.getUser().getUserId();
					long couponId = userCoupon.getCouponId();
					Coupon coupon = couponService.getCoupon(couponId);
					//判断输入的礼券激活码是否是对应的礼券，比如红包激活码不能激活优惠券激活码
					if(!(couPro.equals(coupon.getCouponProvider()))){
						return ResultDtoManager.fail(-1, "请输入正确卡密号");
					}
					//判断礼券是否已经下线
					int status = coupon.getStatus();
					if(status == 0){
						return ResultDtoManager.fail(-1, "礼券已失效");
					}
					//查询用户已经领取了几张优惠券
					int number = userCouponService.getUserCouponCount(userId, couponId);
					//判断是否已经达到领取上限
					long limit = coupon.getGetLimit();
					if(number>=limit){
						return ResultDtoManager.fail(-1, "领取优惠券已达到上限");
					}else{
						//判断时候已经超过活动结束时间
						Date endDate = coupon.getEndDate();
						Date nowDate = new Date();
						if(nowDate.getTime() > endDate.getTime()){
							return ResultDtoManager.fail(-1, "优惠券已过期");
						}else{
							//初始化优惠券数量
							long initNumber = coupon.getCouponNumber();
							//已绑定优惠券数量
							long bindNumber = coupon.getBindCouponNumber();
							if(bindNumber >= initNumber){
								return ResultDtoManager.fail(-1, "优惠券已领完");
							}else{
								//可以正常激活
								String userName = loginedUserService.getUser().getUserName();
								couponService.activateCoupon(userId, userName, coupon, userCoupon);
								return ResultDtoManager.success();
							}
						}
					}
			}else {
				//已有人领取
				return ResultDtoManager.fail(-1, "该券已被领取了, 换个卡密吧!");
			}
		}
	}


	/**
	 * 优惠券分享
	 */
	@ApiOperation(value = "优惠券分享", httpMethod = "POST", notes = "优惠券分享",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="path", name = "couponId", value = "优惠券id", required = true, dataType = "Long")
	@PostMapping(value = "/couponDetail/{couponId}")
	public ResultDto<AppCouponDto> couponShare(@PathVariable Long couponId) {

		AppCouponDto appCouponDto = appCouponService.getCouponDto(couponId);

		if(AppUtils.isBlank(appCouponDto)) {
			return ResultDtoManager.fail(-1, "该优惠券已失效!");
		}
		return ResultDtoManager.success(appCouponDto);
	}

	@ApiOperation(value="获取优惠劵对应的商品",httpMethod="POST",notes="获取优惠劵对应的商品",produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="path",name="curPageNo",value="当前页",required=false,dataType="Long"),
		@ApiImplicitParam(paramType="path",name="couponId",value="优惠劵id",required=true,dataType="Long"),
		@ApiImplicitParam(paramType="path",name="orders",value="排序条件（属性名/排序方式）",required=true,dataType="String"),
	})
	@PostMapping(value = "/couponProdList")
	public ResultDto<AppPageSupport<AppProdDto>> couponProdList(HttpServletRequest request, HttpServletResponse response,
			Long couponId, String curPageNO, String orders) {
		AppPageSupport<AppProdDto> ps=appCouponService.queryCouponProdList(couponId,curPageNO,orders);
		return ResultDtoManager.success(ps);
	}

	@ApiOperation(value="获取红包对应的商品",httpMethod="POST",notes="获取红包对应的商品",produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="path",name="curPageNo",value="当前页",required=false,dataType="Long"),
			@ApiImplicitParam(paramType="path",name="couponId",value="优惠劵id",required=true,dataType="Long"),
			@ApiImplicitParam(paramType="path",name="orders",value="排序条件（属性名/排序方式）",required=true,dataType="String"),
	})
	@PostMapping(value = "/redpackProdList")
	public ResultDto<AppPageSupport<AppProdDto>> redpackProdList(HttpServletRequest request, HttpServletResponse response,
																 Long couponId, String curPageNO, String orders) {
		AppPageSupport<AppProdDto> ps=appCouponService.queryRedpackProdList(couponId,curPageNO,orders);
		return ResultDtoManager.success(ps);
	}

}
