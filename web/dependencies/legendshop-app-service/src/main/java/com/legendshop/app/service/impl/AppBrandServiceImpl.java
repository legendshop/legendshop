package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.BrandDto;
import com.legendshop.app.service.AppBrandService;
import com.legendshop.model.entity.Brand;
import com.legendshop.spi.service.BrandService;
import com.legendshop.util.AppUtils;

@Service("appBrandService")
public class AppBrandServiceImpl implements AppBrandService{

	@Autowired
	private BrandService brandService;
	
	@Override
	public List<BrandDto> queryBrandDto(String brandName) {
		List<Brand> BrandList = brandService.getBrandsByName(brandName);
		return convert(BrandList);
	}

	private List<BrandDto> convert(List<Brand> BrandList){
		if(AppUtils.isNotBlank(BrandList)){
			List<BrandDto> dtoList = new ArrayList<BrandDto>();
			for(Brand brand : BrandList){				
				BrandDto dto = new BrandDto();
				dto.setBrandId(brand.getBrandId());
				dto.setBrandName(brand.getBrandName());
				dto.setStatus(brand.getStatus());
				dto.setSeq(brand.getSeq());
				dto.setBrandPicMobile(brand.getBrandPicMobile());
				dtoList.add(dto);
			}
			return dtoList;
		}
		return null;
	}
}
