/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dao;
 
import java.util.List;

import com.legendshop.dao.GenericDao;
import com.legendshop.model.dto.integral.IntegralOrderDto;
import com.legendshop.model.entity.integral.IntegralOrder;

/**
 * @author quanzc
 * 积分订单
 */
public interface AppIntegralOrderDao extends GenericDao<IntegralOrder, Long> {
     

	/**
	 * 根据id获取积分订单
	 * @param id
	 * @return
	 */
	public abstract IntegralOrder getIntegralOrder(Long id);
	
	/**
	 * 保存
	 * @param integralOrder
	 * @return
	 */
	public abstract Long saveIntegralOrder(IntegralOrder integralOrder);
	
	/**
	 * 根据sql和参数获取积分订单集合
	 * @param subSQl
	 * @param args
	 * @return
	 */
	public abstract List<IntegralOrderDto> getIntegralOrderDtos(String subSQl,List<Object> args);

	/**
	 * 取消订单
	 * @param orderSn
	 * @return
	 */
	public abstract int orderCancel(String orderSn);
	
	/**
	 * 根据orderSn获取订单
	 * @param orderSn
	 * @return
	 */
	public abstract IntegralOrder getIntegralOrderByOrderSn(String orderSn);

	/**
	 * 根据orderSn获取订单详细
	 * @param ordeSn
	 * @return
	 */
	public abstract IntegralOrderDto findIntegralOrderDetail(String ordeSn);

	/**
	 * 
	 * @param id
	 * @param userId
	 * @return
	 */
	public abstract int getIntegralBuyCount(Long id, String userId);
	
 }
