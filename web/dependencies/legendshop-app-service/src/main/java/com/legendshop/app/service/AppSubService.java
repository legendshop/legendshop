package com.legendshop.app.service;

import java.util.List;

import com.legendshop.model.dto.app.AppSubCountsDto;

/**
 * app 订单服务
 */
public interface AppSubService {

	/**
	 * 获取各个状态的订单 统计数量
	 * @param userId
	 * @return
	 */
	public abstract List<AppSubCountsDto> querySubCounts(String userId);

	/**
	 * 已完成的订单未评价的 订单个数
	 * @param userId
	 * @return
	 */
	public Integer getUnCommentOrderCount(String userId);
	
}
