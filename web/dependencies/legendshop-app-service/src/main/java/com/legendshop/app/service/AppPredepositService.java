/**
 * 
 */
package com.legendshop.app.service;

import com.legendshop.model.dto.app.UserPredepositDto;


/**
 * @author quanzc
 * 预付款
 */
public interface AppPredepositService {
	
	/**
	 * @Description: 余额记录
	 * @date 2016-7-15 
	 */
	public UserPredepositDto findUserPredepositLog(String userId, String curPageNO);

	/**
	 * @Description: 余额充值记录
	 * @date 2016-7-15 
	 */
	public UserPredepositDto findUserPredepositRecharge(String userId, String curPageNO);
	
	/**
	 * 消费记录
	 * */
	public UserPredepositDto findUserExpensesRecord(String userId, String curPageNO);
}
