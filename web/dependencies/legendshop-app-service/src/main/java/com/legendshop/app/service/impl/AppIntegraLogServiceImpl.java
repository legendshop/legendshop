/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.model.constant.IntegralLogTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.AppIntegraLogDto;
import com.legendshop.app.service.AppIntegraLogService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.business.dao.IntegraLogDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.integral.IntegraLog;

/**
 * 会员积分日志明细服务.
 */
@Service("appIntegraLogService")
public class AppIntegraLogServiceImpl  implements AppIntegraLogService{

	@Autowired
    private IntegraLogDao integraLogDao;


    @Override
	public AppIntegraLogDto getIntegraLog(Long id) {

		IntegraLog integraLog = integraLogDao.getIntegraLog(id);
		return convertToAppIntegraLogDto(integraLog);
    }

    public void deleteIntegraLog(IntegraLog integraLog) {
        integraLogDao.deleteIntegraLog(integraLog);
    }


	/**
	 * 获取积分列表
	 */
	@Override
	public AppPageSupport<AppIntegraLogDto> queryUserIntegral(String curPageNO, String userId, Integer type) {
		
		PageSupport<IntegraLog> ps= integraLogDao.getIntegraLogByIsUsed(userId, curPageNO, type, null);
		
		AppPageSupport<AppIntegraLogDto> pageSupportDto = PageUtils.convertPageSupport(ps, new PageUtils.ResultListConvertor<IntegraLog, AppIntegraLogDto>() {
			
			@Override
			public List<AppIntegraLogDto> convert(List<IntegraLog> form) {
				
				List<AppIntegraLogDto> toList = new ArrayList<AppIntegraLogDto>();
				
				for(IntegraLog integraLog: form){
					
					AppIntegraLogDto appIntegraLogDto = convertToAppIntegraLogDto(integraLog);
					toList.add(appIntegraLogDto);
				}
				
				return toList;
			}
			

		});
		
		return pageSupportDto;
	}

	public AppIntegraLogDto convertToAppIntegraLogDto(IntegraLog integraLog){
		AppIntegraLogDto appIntegraLogDto = new AppIntegraLogDto();
		appIntegraLogDto.setLogType(integraLog.getLogType());
		appIntegraLogDto.setLogTypeName(IntegralLogTypeEnum.getDesc(integraLog.getLogType()));
		appIntegraLogDto.setIntegralNum(integraLog.getIntegralNum());
		appIntegraLogDto.setLogDesc(integraLog.getLogDesc());
		appIntegraLogDto.setAddTime(integraLog.getAddTime());
		appIntegraLogDto.setUpdateUserId(integraLog.getUpdateUserId());
		appIntegraLogDto.setNickName(integraLog.getNickName());
		appIntegraLogDto.setUserMobile(integraLog.getUserMobile());
		appIntegraLogDto.setId(integraLog.getId());
		appIntegraLogDto.setUserId(integraLog.getUserId());
		return appIntegraLogDto;
	}

}
