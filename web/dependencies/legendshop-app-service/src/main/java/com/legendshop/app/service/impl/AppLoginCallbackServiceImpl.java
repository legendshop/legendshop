package com.legendshop.app.service.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.AppLoginCallbackService;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.model.dto.app.AppPassportDto;
import com.legendshop.model.entity.Passport;
import com.legendshop.model.entity.User;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.form.UserForm;
import com.legendshop.spi.service.PassportService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.RandomStringUtils;
import com.legendshop.util.SafeHtml;

/**
 * 第三方回调
 */
@Service("appLoginCallbackService")
public class AppLoginCallbackServiceImpl implements AppLoginCallbackService {
	private final static Logger LOGGER=LoggerFactory.getLogger(AppLoginCallbackServiceImpl.class);
	
	@Autowired
	private PassportService passportService;
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private  UserDetailDao userDetailDao;
	
    @Autowired
    private PasswordEncoder passwordEncoder;
	
	/**
	 * 绑定注册
	 */
	@Override
	public Passport autoRegBind(AppPassportDto passportDto) {
		try {
			//存在商城用户
			Passport passport=passportService.getUserIdByOpenIdForUpdate(passportDto.getCallbackType(), passportDto.getOpenId());
			LOGGER.info("------LOGIN TYPE={}",passportDto.getCallbackType());
			if(AppUtils.isNotBlank(passport)){
				LOGGER.info("LOGINCALLBACK existing passport by CallbackType = {} , openid={} ",passportDto.getCallbackType(),passportDto.getOpenId());
				return passport;
			}
			LOGGER.info("not Existing passport by openid={}",passportDto.getOpenId());
		    passport=new Passport();
		    String nickName=passportDto.getNickName()+RandomStringUtils.randomAlphanumeric(4);
			passport.setNickName(nickName);
			passport.setOpenId(passportDto.getOpenId());
			passport.setType(passportDto.getCallbackType()); //类型
			passport.setAccessToken(passportDto.getAccessToken());
			passport.setProps(passportDto.getProps());
			
			UserForm userForm =new UserForm();
			String userName="";
			if(passportDto.getCallbackType().equals("qq")){
				userName="QQ_"+userDetailService.generateUserName();
			}else if(passportDto.getCallbackType().equals("weixin")){
				userName="WX_"+userDetailService.generateUserName();
			}else if(passportDto.getCallbackType().equals("weibo")){
				userName="WB_"+userDetailService.generateUserName();
			}
			String password=RandomStringUtils.randomAlphanumeric(6);
			userForm.setNickName(nickName);
			userForm.setName(userName);
			userForm.setPassword(passwordEncoder.encode(password)); //密码
			userForm.setUserRegip(passportDto.getRegIP());
			if(AppUtils.isNotBlank(passportDto.getSex())){
				if("男".equals(passportDto.getSex().trim())){
					userForm.setSex("M");	
				}else if("女".equals(passportDto.getSex().trim())){
					userForm.setSex("F");
				}
			}
			//注册并保持通行证
			UserDetail userDetail=parseUserDeatil(userForm);
			userDetail.setPortraitPic(passportDto.getHeadimgurl());
			User u=parseUser(userForm);
			User user=userDetailDao.saveUser(u,userDetail);
			if(user == null){
				throw new BusinessException("auto register user error");
			}
			
			passport.setUserId(user.getId());//保存绑定关系
			passport.setUserName(user.getName());
			passportService.savePassport(passport);
			return passport;
		} catch (Exception e) {
			LOGGER.error("LOGINCALLBACK autoRegBind fail------->",e.getMessage());
			return null;
		}
	
	}
	
	
	/**
	 * @Description: 过滤特殊字符
	 * @date 2016-7-20 
	 */
	public UserForm safeHtml(UserForm form){
		// 过滤特殊字符
		SafeHtml safeHtml = new SafeHtml();
		form.setNickName(safeHtml.makeSafe(form.getNickName()));
		form.setUserName(safeHtml.makeSafe(form.getUserName()));
		form.setUserMemo(safeHtml.makeSafe(form.getUserMemo()));
		form.setUserMobile(safeHtml.makeSafe(form.getUserMobile()));
		form.setUserPostcode(safeHtml.makeSafe(form.getUserPostcode()));
		form.setUserTel(safeHtml.makeSafe(form.getUserTel()));
		form.setUserMail(safeHtml.makeSafe(form.getUserMail()));
		form.setUserAdds(safeHtml.makeSafe(form.getUserAdds()));
		form.setMsn(safeHtml.makeSafe(form.getMsn()));
		form.setNote(safeHtml.makeSafe(form.getNote()));
		form.setQq(safeHtml.makeSafe(form.getQq()));
		return form;
	}
	
	/**
	 * @Description: 封装User
	 * @date 2016-7-20 
	 */
	public User parseUser(UserForm form){
		form=safeHtml(form);
		User user = new User();
		user.setName(form.getName());
		user.setNote(form.getNote());
		user.setEnabled(form.getEnabled());
		user.setPassword(form.getPassword());
		return user;
	}
	
	/**
	 * @Description: 封装UserDetail
	 * @date 2016-7-20 
	 */
	public UserDetail parseUserDeatil(UserForm form){
		form=safeHtml(form);
		UserDetail userDetail = new UserDetail();
		userDetail.setUserName(form.getUserName());
		userDetail.setUserMemo(form.getUserMemo());
		userDetail.setUserMobile(form.getUserMobile());
		userDetail.setUserPostcode(form.getUserPostcode());
		userDetail.setUserTel(form.getUserTel());
		userDetail.setUserMail(form.getUserMail());
		userDetail.setUserAdds(form.getUserAdds());
		userDetail.setNickName(form.getNickName());
		userDetail.setBirthDate(form.getBirthDate());
		userDetail.setEnabled(form.getEnabled());
		userDetail.setFax(form.getFax());
		userDetail.setQq(form.getQq());
		userDetail.setMsn(form.getMsn());
	    userDetail.setGradeId(1);// 注册用户
		Date date = new Date();
	    userDetail.setUserRegtime(date);
		userDetail.setModifyTime(date);
		userDetail.setUserRegip(form.getUserRegip());
		userDetail.setTotalCash(0d);
		userDetail.setTotalConsume(0d);
		userDetail.setSex(form.getSex());
		return userDetail;
	}

	/**
	 * @Description: 获取ID
	 * @param @param userId
	 * @param @return   
	 * @date 2016-7-20
	 */
	@Override
	public User getUser(String userId) {
		return userDetailDao.getUser(userId);
	}

}
