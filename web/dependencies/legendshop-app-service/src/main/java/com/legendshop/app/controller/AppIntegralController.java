package com.legendshop.app.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.constant.SubmitOrderStatusEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.app.dto.AppDeliveryDto;
import com.legendshop.app.dto.AppIntegraLogDto;
import com.legendshop.app.dto.AppIntegralCouponListDto;
import com.legendshop.app.dto.AppIntegralIndexDto;
import com.legendshop.app.dto.AppIntegralOrderDto;
import com.legendshop.app.dto.AppIntegralProdDto;
import com.legendshop.app.dto.AppIntegralProdListDto;
import com.legendshop.app.dto.AppIntegralReturnDto;
import com.legendshop.app.dto.AppIntegralRuleDto;
import com.legendshop.app.dto.AppSubmitIntegralProdDto;
import com.legendshop.app.service.AppCouponService;
import com.legendshop.app.service.AppIntegraLogService;
import com.legendshop.app.service.AppIntegralService;
import com.legendshop.base.util.ExpressMD5;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.ExpressDeliverDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.integral.IntegralOrderDto;
import com.legendshop.model.dto.order.KuaiDiLogs;
import com.legendshop.model.dto.order.OrderSearchParamDto;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.ConstTable;
import com.legendshop.model.entity.DeliveryCorp;
import com.legendshop.model.entity.SystemParameter;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.model.entity.UserAddressSub;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.integral.IntegralOrder;
import com.legendshop.model.entity.integral.IntegralProd;
import com.legendshop.spi.service.ConstTableService;
import com.legendshop.spi.service.DeliveryCorpService;
import com.legendshop.spi.service.IntegralOrderService;
import com.legendshop.spi.service.KuaiDiService;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.SystemParameterService;
import com.legendshop.spi.service.UserAddressService;
import com.legendshop.spi.service.UserAddressSubService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import com.legendshop.util.HttpUtil;
import com.legendshop.util.JSONUtil;
import com.legendshop.util.MD5Util;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;


/**
 * 积分商城
 */	
@RestController
@RequestMapping
@Api(tags="积分商城",value="积分商城")
public class AppIntegralController {
	
	private final Logger LOGGER = LoggerFactory.getLogger(AppIntegralController.class);
	
	@Autowired
	private AppIntegralService appIntegralService;
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private UserAddressService userAddressService;
	
	@Autowired
	private AppCouponService appCouponService;
	
    @Autowired
    private AppIntegraLogService appIntegraLogService;
    
    @Autowired
    private ConstTableService constTableService;
    
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;
	
	@Autowired
	private IntegralOrderService integralOrderService;
	
	@Autowired
	private DeliveryCorpService deliveryCorpService;
	
	@Autowired
	private UserAddressSubService userAddressSubService;
	
	@Autowired
	private SystemParameterService systemParameterService;
	
	@Autowired
	private KuaiDiService kuaiDiService;
    
    /**
	 * 我的积分中心.
	 *
	 * @return the string
	 */
    @ApiOperation(value = "获取我的积分中心数据", httpMethod = "GET", notes = "获取我的积分中心数据,成功返回OK，可获取用户积分余额",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("p/app/integral/myIntegral")
    public  ResultDto<Object> myIntegral() {
		try {
			// 获取该用户的积分值
			String userId = loginedUserService.getUser().getUserId();
			Integer score = userDetailService.getScore(userId);
			return ResultDtoManager.success(score);
		} catch (Exception e) {
			LOGGER.error("获取积分中心数据失败",e.getMessage());
			return ResultDtoManager.fail();
		}
	}
    
    /**
	 * 获取积分规则相关数据
	 * @param request
	 * @param response
	 * @return
	 */
	@ApiOperation(value = "获取积分规则相关数据", httpMethod = "POST", notes = "获取积分规则相关数据",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("p/app/integral/getRule")
	public  ResultDto<AppIntegralRuleDto> getRule(HttpServletRequest request, HttpServletResponse response) {
		try {
			ConstTable constTable = constTableService.getConstTablesBykey("INTEGRAL_RELU_SETTING", "INTEGRAL_RELU_SETTING");
			AppIntegralRuleDto releSetting = null;
			if (AppUtils.isNotBlank(constTable)) {
				String setting = constTable.getValue();
				releSetting = JSONUtil.getObject(setting, AppIntegralRuleDto.class);
			}
			return ResultDtoManager.success(releSetting);
		} catch (Exception e) {
			LOGGER.error("获取积分规则相关数据出错",e.getMessage());
			return ResultDtoManager.fail();
		} 
	}
	
	/**
	 * 积分明细.
	 *
	 * @return the string
	 */
    @ApiOperation(value = "获取积分明细列表", httpMethod = "GET", notes = "获取积分明细列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
    	@ApiImplicitParam(paramType="query", name = "curPageNO", value = "分页", required = false, dataType = "string"),
    	@ApiImplicitParam(paramType="query", name = "type", value = "类型:获取传1，使用传-1", required = false, dataType = "Integer")
    })
	@PostMapping("/p/app/integral/integralDetail")
    public  ResultDto<AppPageSupport<AppIntegraLogDto>> integralDetail(String curPageNO,Integer type) {
		try {
			// 获取该用户的积分值
			String userId = loginedUserService.getUser().getUserId();
			AppPageSupport<AppIntegraLogDto> ps = appIntegraLogService.queryUserIntegral(curPageNO, userId,type);
			return ResultDtoManager.success(ps);
		} catch (Exception e) {
			LOGGER.error("获取积分中心数据失败",e.getMessage());
			return ResultDtoManager.fail();
		}
	}


	/**
	 * 积分明细详情.
	 */
    @ApiOperation(value = "获取积分明细详情", httpMethod = "GET", notes = "获取积分明细详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "integralLogId", value = "积分明细ID", required = true, dataType = "long")
	@PostMapping("/p/app/integral/integralInfo")
    public  ResultDto<AppIntegraLogDto> integralInfo(Long integralLogId) {
		try {
			AppIntegraLogDto integralLog = appIntegraLogService.getIntegraLog(integralLogId);
			return ResultDtoManager.success(integralLog);
		} catch (Exception e) {
			LOGGER.error("获取积分明细详情失败",e.getMessage());
			return ResultDtoManager.fail();
		}
	}

    @ApiOperation(value = "获取积分商品首页", httpMethod = "POST", notes = "获取积分商品首页",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("/app/integral/integralIndex")
    public  ResultDto<AppIntegralIndexDto> integralIndex(HttpServletRequest request) {
    	try {
    		AppIntegralIndexDto appIntegralIndexDto = new AppIntegralIndexDto();
    		
    		LoginedUserInfo appToken = loginedUserService.getUser();
    		if(null != appToken){//如果用户已登录, 才获取积分
    			String userId = appToken.getUserId();
    			//获取用户积分
    			Integer score = userDetailService.getScore(userId);
    			appIntegralIndexDto.setScore(score);
    		}
			
			//获取积分优惠券列表
			AppPageSupport<AppIntegralCouponListDto> integralCoupons = appCouponService.getIntegralCoupons(6,"1");
			List<AppIntegralCouponListDto> resultList = integralCoupons.getResultList();
			appIntegralIndexDto.setIntegralCouponList(resultList);
			
			//获取积分商品列表
			AppPageSupport<AppIntegralProdListDto> integralProdList = appIntegralService.getIntegralProdList("1", 6, false,"saleNum");
			List<AppIntegralProdListDto> resultList2 = integralProdList.getResultList();
			appIntegralIndexDto.setIntegralProdList(resultList2);
			
			return ResultDtoManager.success(appIntegralIndexDto);
		} catch (Exception e) {
			LOGGER.error("获取积分首页失败:{}",e.getMessage());
			return ResultDtoManager.fail();
		}
	}
    
    /**
	 * 获取积分优惠券列表
	 * @param curPageNO
	 */
	@ApiOperation(value = "获取积分优惠券列表", httpMethod = "POST", notes = "获取积分优惠券列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "curPageNO", value = "分页", required = false, dataType = "string")
	@PostMapping("/app/integral/integralCouponList")
	public  ResultDto<AppPageSupport<AppIntegralCouponListDto>> integralCouponList(String curPageNO) {
	  curPageNO = AppUtils.isNotBlank(curPageNO) ? "1" : curPageNO;
	  AppPageSupport<AppIntegralCouponListDto> integralCoupons = appCouponService.getIntegralCoupons(10,"1");
	  return ResultDtoManager.success(integralCoupons);
	}
	
    /**
	 * 获取热门兑换列表
	 * @param curPageNO
	 */
	@ApiOperation(value = "获取热门兑换列表", httpMethod = "POST", notes = "获取热门兑换列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "curPageNO", value = "分页", required = false, dataType = "string")
	@PostMapping("/app/integral/hotIntegralProdList")
	public  ResultDto<AppPageSupport<AppIntegralProdListDto>> hotIntegralProdList(String curPageNO) {
	  curPageNO = AppUtils.isBlank(curPageNO) ? "1" : curPageNO;
	  AppPageSupport<AppIntegralProdListDto> integralProdList = appIntegralService.getIntegralProdList(curPageNO, 10, false,"saleNum");
		return ResultDtoManager.success(integralProdList);
	}
	
	/**
	 * 获取精品优选列表
	 * @param curPageNO
	 */
	@ApiOperation(value = "获取精品优选列表", httpMethod = "POST", notes = "获取精品优选列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "curPageNO", value = "分页", required = false, dataType = "string")
	@PostMapping("/app/integral/commendIntegralProdList")
	public  ResultDto<AppPageSupport<AppIntegralProdListDto>> commendIntegralProdList(String curPageNO) {
	  curPageNO = AppUtils.isNotBlank(curPageNO) ? "1" : curPageNO;
	  AppPageSupport<AppIntegralProdListDto> integralProdList = appIntegralService.getIntegralProdList(curPageNO, 10, true,"saleNum");
		return ResultDtoManager.success(integralProdList);
	}
    
	/**
	 * 获取积分商品详情
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "获取积分商品详情", httpMethod = "POST", notes = "获取积分商品详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "id", value = "积分商品id", required = true, dataType = "Long")
	@PostMapping("/app/integral/view")
	public ResultDto<AppIntegralProdDto> view(Long id) {
		
		try {
			AppIntegralProdDto result = appIntegralService.getIntegralProdDtoById(id);
			
			return ResultDtoManager.success(result);
		
		} catch (Exception e) {
			LOGGER.error("获取积分商品详情异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	
	/**
	 * 去兑换积分商品
	 * @param id
	 * @param count
	 * @return
	 */
	@ApiOperation(value = "去兑换积分商品", httpMethod = "POST", notes = "去兑换积分商品，返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "id", value = "积分商品id", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="query", name = "count", value = "数量", required = false, dataType = "Integer"),
	})
	@PostMapping("/p/app/integral/goExchange")
	public  ResultDto<AppIntegralReturnDto> goExchange(Long id,Integer count) {
		LoginedUserInfo appToken = loginedUserService.getUser();
		String userName = appToken.getUserName();
		String userId = appToken.getUserId();
		
		IntegralProd integralProd = appIntegralService.getIntegralProdById(id);
		//校验通过
		StringBuilder builder = new StringBuilder();
		builder.append(Constants._MD5);
		builder.append("&").append(userName).append("&").append(id).append("&").append(count);
		
		String token = MD5Util.toMD5(builder.toString());
		UserAddress defaultAddress = userAddressService.getDefaultAddress(userId);//查出买家的默认地址
		
		AppSubmitIntegralProdDto IntegralProdDto = appIntegralService.convertToAppSubmitIntegralProdDto(integralProd);
		
		AppIntegralReturnDto returnDto = new AppIntegralReturnDto();
		returnDto.setIntegralProdDto(IntegralProdDto);
		returnDto.setDefaultAddress(defaultAddress);
		returnDto.setToken(token);
		returnDto.setCount(count);
		returnDto.setTotalExchangeIntegral(Arith.mul(count, integralProd.getExchangeIntegral()));
		
		return ResultDtoManager.success(returnDto);
	}
	 
    /**
     * 校验兑换参数是否正确
     * @param loginUserName
     * @param userId
     * @param integralProd
     * @return
     */
    private String validateIntegral(String loginUserName,String userId,IntegralProd integralProd,Integer count){
    	if(AppUtils.isBlank(integralProd) || Constants.OFFLINE.equals(integralProd.getStatus()) ){
    		return "商品已下架或者不存在";
    	}
    	
    	if(AppUtils.isBlank(count)||count == 0){
    		return "请输入兑换数量";
    	}
    	
    	if(Constants.ONLINE.equals(integralProd.getIsLimit())){ //如果启用购买限制
    		if(count > integralProd.getLimitNum()){
        		return "超过购买限制！";
    		}
    		
    		int number = appIntegralService.getIntegralBuyCount(integralProd.getId(),userId);
        	if((number+count) > integralProd.getLimitNum()){
        		return "您购买的数量已经超过限购数";
        	}
    	}
    	
    	if(count > integralProd.getProdStock()){
        	return "商品库存不足,请重新选择数量！";
    	}
    	
		UserDetail userDetail = userDetailService.getUserDetailByIdNoCache(userId);
    	Double score = Arith.mul(integralProd.getExchangeIntegral(), count);
    
    	if(AppUtils.isBlank(userDetail.getScore()) || userDetail.getScore()<=0 || userDetail.getScore()<score ){
        	return "对不起, 您的积分不足!!";
    	}
    	
		return Constants.SUCCESS;
    }
    
    
    /**
     * 提交积分兑换订单
     * @param id
     * @param count 数量
     * @return
     */
    @ApiOperation(value = "提交积分兑换订单", httpMethod = "POST", notes = "提交积分兑换订单，成功返回OK",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "id", value = "积分商品id", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="query", name = "count", value = "数量", required = true, dataType = "Integer"),
		@ApiImplicitParam(paramType="query", name = "token", value = "token令牌,校验订单用的", required = true, dataType = "String")
	})
    @PostMapping("/p/app/integral/submitOrder")
    public  ResultDto<String>  submitOrder(Long id,Integer count,String token) {
		
    	String userName = loginedUserService.getUser().getUserName();
		String userId = loginedUserService.getUser().getUserId();
		
		//校验通过
		StringBuilder builder = new StringBuilder();
		builder.append(Constants._MD5);
		builder.append("&").append(userName).append("&").append(id).append("&").append(count);
		String buildToken = MD5Util.toMD5(builder.toString());
				
		if(!buildToken.equals(token)){
			LOGGER.info("提交兑换订单-------非法访问rsa="+buildToken);
    		return ResultDtoManager.fail(-1,"非法访问");
    	}

		// TODO 校验用户参数，SpringSession后台过期机制有问题
		if (!userDetailService.isUserExist(userName)) {
			return ResultDtoManager.fail(-1,"用户不存在");
		}

		IntegralProd integralProd = appIntegralService.getIntegralProdById(id);
		String result = validateIntegral(userName,userId,integralProd,count);
		if(AppUtils.isNotBlank(result)&&!result.equals(Constants.SUCCESS)){
			LOGGER.info("提交兑换订单-------error="+result);
			return ResultDtoManager.fail(-1, result);
		}
		//查出买家的默认地址
		final UserAddress userAddress = userAddressService.getDefaultAddress(userId);
		if(userAddress == null) {
			LOGGER.info("提交兑换订单-------找不到用户地址");
			return ResultDtoManager.fail(-1,"找不到用户地址！");
		}
		UserDetail userDetail = userDetailService.getUserDetailById(userId);
		if(userDetail == null) {
			LOGGER.info("提交兑换订单-------找不到用户");
			return ResultDtoManager.fail(-1,"找不到用户！");
		}
		try {
			result=appIntegralService.shopBuyIntegral(userId,userAddress,integralProd,count,"");
			if(!Constants.SUCCESS.equals(result)){
        		return ResultDtoManager.fail(-1, result);
        	}
		} catch (Exception e) {
				return ResultDtoManager.fail(-1, e.getMessage());
		}
		return ResultDtoManager.success();
	}
    
    /**
     * 兑换记录列表
     * @param curPageNO
     * @return
     */
    @ApiOperation(value = "兑换记录列表", httpMethod = "POST", notes = "兑换记录列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页", required = false, dataType = "string")
    @PostMapping("/p/app/integral/orderList")
    public  ResultDto<AppPageSupport<AppIntegralOrderDto>> orderList(String curPageNO) {
    	
   		String userId = loginedUserService.getUser().getUserId();
   		OrderSearchParamDto paramDto = new OrderSearchParamDto();
		paramDto.setPageSize(10);
		
		if (AppUtils.isNotBlank(curPageNO)) {
			paramDto.setCurPageNO(curPageNO);
		}
		paramDto.setUserId(userId);
		AppPageSupport<AppIntegralOrderDto> integralOrderDtoList = appIntegralService.getIntegralOrderDtoList(paramDto);
   		return ResultDtoManager.success(integralOrderDtoList);
   	}
    
    
    /**
     * 积分订单详情
     * @param orderSn
     * @return
     */
    @ApiOperation(value = "积分订单详情", httpMethod = "POST", notes = "积分订单详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParam(paramType="query", name = "orderSn", value = "订单号", required = true, dataType = "string")
	@PostMapping("/p/app/integral/orderDetail/{orderSn}")
	public  ResultDto<IntegralOrderDto> orderDetail(@PathVariable String orderSn) {
    	
		IntegralOrderDto order = appIntegralService.findIntegralOrderDetail(orderSn);
		return  ResultDtoManager.success(order);
	}
    
    /**
     * 根据流水号取消订单
     * @return
     */
    @ApiOperation(value = "根据流水号取消订单", httpMethod = "POST", notes = "根据流水号取消订单,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParam(paramType="path", name = "orderSn", value = "订单号", required = true, dataType = "string")
    @PostMapping("/p/app/integral/orderCancel/{orderSn}")
    public  ResultDto<String> orderCancel(@PathVariable String orderSn) {
    	
		String updateUserId = loginedUserService.getUser().getUserId();
		String result = appIntegralService.orderCancel(orderSn,null,updateUserId,"用户取消订单,该订单流水号是:"+orderSn);
		
		if(AppUtils.isNotBlank(result)&&!Constants.SUCCESS.equals(result)){
			return ResultDtoManager.fail(-1, result);
		}
		return ResultDtoManager.success();
	}
    
    /**
     * 根据流水号删除订单
     * @return
     */
    @ApiOperation(value = "根据流水号删除订单", httpMethod = "POST", notes = "根据流水号删除订单,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParam(paramType="path", name = "orderSn", value = "订单号", required = true, dataType = "string")
    @PostMapping("/p/app/integral/orderDel/{orderSn}")
    public  ResultDto<Object> orderDel(@PathVariable String orderSn) {
    	
		String updateUserId = loginedUserService.getUser().getUserId();
		String result = appIntegralService.orderDel(orderSn,null,updateUserId,"用户删除订单,该订单流水号是:"+orderSn);
		
		if(AppUtils.isNotBlank(result)&&!Constants.SUCCESS.equals(result)){
			return ResultDtoManager.fail(-1, result);
		}
		return ResultDtoManager.success();
	}

  	/**
  	 * 根据流水号确认收货
  	 * @return
  	 */
    @ApiOperation(value = "根据流水号确认收货", httpMethod = "POST", notes = "根据流水号确认收货,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParam(paramType="path", name = "orderSn", value = "订单号", required = true, dataType = "string")
    @PostMapping("/p/app/integral/integralOrderReceive/{orderSn}")
    public  ResultDto<Object> integralOrderReceive(@PathVariable String orderSn) {
    	
  		String result = appIntegralService.shouhuo(orderSn);
  		
  		if(AppUtils.isNotBlank(result)&&!Constants.SUCCESS.equals(result)){
  			return ResultDtoManager.fail(-1, result);
  		}
  		return ResultDtoManager.success();
  	}
    
    
	/**
	 * 查看物流
	 */
	@ApiOperation(value = "查看物流", httpMethod = "POST", notes = "查看积分订单物流信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "orderSn", value = "积分订单号", required = true, dataType = "string"),
	})
	@PostMapping(value="/p/app/integral/order/logistics")
	public ResultDto<AppDeliveryDto> orderLogistics(@RequestParam(required = true) String orderSn){
		
		String userId = loginedUserService.getUser().getUserId();
		if (AppUtils.isBlank(userId)) {
			return ResultDtoManager.fail(0, "未登录！");
		}
		
		IntegralOrder integralOrder = integralOrderService.getIntegralOrder(orderSn, userId);
		if(AppUtils.isBlank(integralOrder)){
			return ResultDtoManager.fail(-1, "该订单不存在！");
		}
		
		//物流查询参数
		Long dvyId = integralOrder.getDvyTypeId();
		String dvyFlowId = integralOrder.getDvyFlowId();
		
		if (AppUtils.isBlank(dvyId) || AppUtils.isBlank(dvyFlowId)) {
			return ResultDtoManager.fail(-1, "查询物流信息参数有误，请联系客服！");
		}
		
		DeliveryCorp deliveryCorp = deliveryCorpService.getDeliveryCorpByDvyId(dvyId);
		
		AppDeliveryDto appDeliveryDto = new AppDeliveryDto();
		if (AppUtils.isNotBlank(deliveryCorp)) {
			
			SystemParameter systemParameter = systemParameterService.getSystemParameter("EXPRESS_DELIVER");
			if (AppUtils.isBlank(systemParameter) || AppUtils.isBlank(systemParameter.getValue())) { //查询到没配置密钥则 是免费的  
				
				String url = deliveryCorp.getQueryUrl();
				if (AppUtils.isNotBlank(url)) {
					url = url.replaceAll("\\{dvyFlowId\\}", dvyFlowId);
				}
				KuaiDiLogs logs = kuaiDiService.query(url);
				if (AppUtils.isNotBlank(logs) && AppUtils.isNotBlank(logs.getEntries()) && logs.getEntries().size()>0) {
					
					String entriesLog = JSONUtil.getJson(logs.getEntries());
					
					//封装返回参数
					appDeliveryDto.setDvyDetail(entriesLog);
					appDeliveryDto.setDvyName(deliveryCorp.getName());
					appDeliveryDto.setDvyNumber(dvyFlowId);
					/*appDeliveryDto.setDvyPic(dvyPic);暂时没有图片返回*/				
					return ResultDtoManager.success(appDeliveryDto);
				}
				
			}else{  //快递100
				
				UserAddressSub userAddressSub = userAddressSubService.getUserAddressSub(integralOrder.getAddrOrderId());
				if(null == userAddressSub){
					return ResultDtoManager.fail(-1, "查询物流信息参数有误，请联系客服！");
				}
				
				String reciverMobile = userAddressSub.getMobile();
				
				if(AppUtils.isBlank(deliveryCorp.getCompanyCode())){
					return ResultDtoManager.fail(-1, "查询该订单的物流信息有误，请联系客服！");
				}
				
				ExpressDeliverDto express = JSONUtil.getObject(systemParameter.getValue(), ExpressDeliverDto.class);
				
				//组装参数密钥
				String param ="{\"com\":\""+deliveryCorp.getCompanyCode()+"\",\"num\":\""+dvyFlowId+"\",\"mobiletelephone\":\""+reciverMobile+"\"}";
				String sign = ExpressMD5.encode(param+express.getKey()+express.getCustomer());
				HashMap<String, String> params = new HashMap<String, String>();
				params.put("param",param);
				params.put("sign",sign);
				params.put("customer",express.getCustomer());
				//请求第三方接口
				String text = HttpUtil.httpPost(ExpressMD5.API_EXPRESS_URL, params);
				if (AppUtils.isBlank(text)) {
					return ResultDtoManager.fail(-1, "查询该订单的物流信息有误，请联系客服！");
				}
				
				JSONObject jsonObject = JSONObject.parseObject(text);
				String result = jsonObject.getString("status");
				if(AppUtils.isBlank(result) || !"200".equals(result)){ 
					LOGGER.warn("Express inquiry failed message = {},returnCode = {}", jsonObject.getString("message"),jsonObject.getString("returnCode"));
					return ResultDtoManager.fail(-1, "查询该订单的物流信息有误，请联系客服！");
				}
				
				//封装返回参数
				appDeliveryDto.setDvyName(deliveryCorp.getName());
				appDeliveryDto.setDvyNumber(dvyFlowId);
				/*appDeliveryDto.setDvyPic(dvyPic);暂时没有图片返回*/				
				appDeliveryDto.setDvyDetail(jsonObject.getString("data"));
				
				return ResultDtoManager.success(appDeliveryDto);
			}
		}
		return ResultDtoManager.fail(-2, "暂无物流信息！");
	}

	/**
	 * 去兑换积分商品
	 * @param id
	 * @param count
	 * @return
	 */
	@ApiOperation(value = "检查兑换积分商品", httpMethod = "POST", notes = "检查兑换积分商品，返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "id", value = "积分商品id", required = true, dataType = "Long"),
			@ApiImplicitParam(paramType="query", name = "count", value = "数量", required = false, dataType = "Integer"),
	})
	@PostMapping("/p/app/integral/checkExchange")
	public  ResultDto<Object> checkExchange(Long id,Integer count) {
		LoginedUserInfo appToken = loginedUserService.getUser();
		String userName = appToken.getUserName();
		String userId = appToken.getUserId();

		IntegralProd integralProd = appIntegralService.getIntegralProdById(id);
		/*String result= validateIntegral(userName,userId,integralProd,count,password);

		if(AppUtils.isNotBlank(result)&&!result.equals(Constants.SUCCESS)){
			return ResultDtoManager.fail(-1, result);
		}*/

		if (AppUtils.isBlank(integralProd) || Constants.OFFLINE.equals(integralProd.getStatus())) {
			return ResultDtoManager.fail(-1, "商品已下架或者不存在");
		}

		if (AppUtils.isBlank(count) || count == 0) {
			return ResultDtoManager.fail(-1, "请输入兑换数量");
		}

		if (Constants.ONLINE.equals(integralProd.getIsLimit())) { //如果启用购买限制
			if (count > integralProd.getLimitNum()) {
				return ResultDtoManager.fail(-1, "超过购买限制！");
			}

			int number = appIntegralService.getIntegralBuyCount(integralProd.getId(), userId);
			if ((number + count) > integralProd.getLimitNum()) {
				return ResultDtoManager.fail(-1, "您购买的数量已经超过限购数");
			}
		}

		if (count > integralProd.getProdStock()) {
			return ResultDtoManager.fail(-1, "商品库存不足,请重新选择数量！");
		}

		UserDetail userDetail = userDetailService.getUserDetailByIdNoCache(userId);

		Double score = Arith.mul(integralProd.getExchangeIntegral(), count);
		if (AppUtils.isBlank(userDetail.getScore()) || userDetail.getScore() <= 0 || userDetail.getScore() < score) {
			return ResultDtoManager.fail(-1, "对不起, 您的积分不足!");
		}
		return ResultDtoManager.success("OK");
	}
}
