package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.AppCartService;
import com.legendshop.model.dto.app.shopcart.ShopCartItemDto;
import com.legendshop.model.dto.app.shopcart.ShopCartsDto;
import com.legendshop.model.dto.app.shopcart.ShopGroupCartItemDto;
import com.legendshop.model.dto.app.shopcart.UserShopCartListDto;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.marketing.MarketingDto;
import com.legendshop.spi.service.BasketService;
import com.legendshop.util.AppUtils;

@Service("appCartService")
public class AppCartServiceImpl implements AppCartService{
	
	@Autowired
	private BasketService basketService;
	
	@Override
	public UserShopCartListDto getShoppingCartsDto(List<ShopCartItem> cartItems) {
		UserShopCartList result = basketService.getShoppingCarts(cartItems);
		return convertUserShopCartListDto(result);
	}
	
	/**
	 * 构建APP 购物车模型
	 * @param userShopCartList
	 * @return
	 */
	@Override
	public UserShopCartListDto convertUserShopCartListDto(UserShopCartList userShopCartList){
		if(AppUtils.isNotBlank(userShopCartList)){
			UserShopCartListDto userShopCartListDto = new UserShopCartListDto();
			userShopCartListDto.setUserId(userShopCartList.getUserId());
			userShopCartListDto.setUserName(userShopCartList.getUserName());
			userShopCartListDto.setType(userShopCartList.getType());
			userShopCartListDto.setOrderActualTotal(userShopCartList.getOrderActualTotal());
			userShopCartListDto.setOrderTotalCash(userShopCartList.getOrderTotalCash());
			userShopCartListDto.setOrderTotalQuanlity(userShopCartList.getOrderTotalQuanlity());
			userShopCartListDto.setOrderTotalVolume(userShopCartList.getOrderTotalVolume());
			userShopCartListDto.setOrderTotalWeight(userShopCartList.getOrderTotalWeight());
			userShopCartListDto.setAllDiscount(userShopCartList.getAllDiscount());

			List<ShopCarts> shopCarts = userShopCartList.getShopCarts();
			// 判断该店铺分组购物车是否存在失效商品，如果有失效商品则剔除，如果剔除完该失效商品后，分组购物的的项为空，则把整个店铺分组剔除
			Iterator<ShopCarts> ShopCartsIt = shopCarts.iterator();

			while(ShopCartsIt.hasNext()){

				ShopCarts shopCart = ShopCartsIt.next();

				// 找出失效的商品项 执行剔除
				List<ShopCartItem> cartItems = shopCart.getCartItems();
				Iterator cartItemsIt = cartItems.iterator();
				while(cartItemsIt.hasNext()){

					ShopCartItem cartItem = (ShopCartItem)cartItemsIt.next();
					if(cartItem.getIsFailure()){
						cartItemsIt.remove();
					}
				}

				// 如果分组购物的的项为空，则把整个店铺分组剔除
				if (shopCart.getCartItems().size() <= 0 ){
					ShopCartsIt.remove();
				}

				// 判断促销活动列表的商品项商品是否失效
				List<MarketingDto> marketingDtoList = shopCart.getMarketingDtoList();
				Iterator<MarketingDto> marketingDtoIt = marketingDtoList.iterator();
				while(marketingDtoIt.hasNext()){

					MarketingDto marketingDto = (MarketingDto)marketingDtoIt.next();

					List<ShopCartItem> hitCartItems = marketingDto.getHitCartItems();
					if(AppUtils.isNotBlank(hitCartItems)){

						Iterator<ShopCartItem> hitCartItemsIt = hitCartItems.iterator();
						while(hitCartItemsIt.hasNext()){

							ShopCartItem hitCartItem = (ShopCartItem)hitCartItemsIt.next();
							if(hitCartItem.getIsFailure()){
								hitCartItemsIt.remove();
							}
						}
					}
				}

			}

			List<ShopCartsDto> shopCartsDto = convertShopCartsDto(shopCarts);
			
			userShopCartListDto.setShopCarts(shopCartsDto);
			return userShopCartListDto;
		}
		return null;
	}
	

	/**
	 * 构建APP 购物车失效商品列表Dto
	 */
	@Override
	public List<ShopCartItemDto> InvalidListDto(List<ShopCartItem> cartItems){
		if(AppUtils.isNotBlank(cartItems)){
			List<ShopCartItemDto> shopCartItemDto = convertShopCartItemDto(cartItems);
			List<ShopCartItemDto> invalidCartItems = new ArrayList<ShopCartItemDto>();
			for (ShopCartItemDto cartItemDto : shopCartItemDto) {
				if(cartItemDto.getIsFailure()){
					invalidCartItems.add(cartItemDto);
				}
			}
			return invalidCartItems;
		}
		return null;
	}
	

	/**
	 * 构建商家订单购物车
	 * @param shopCartList
	 * @return
	 */
	private List<ShopCartsDto> convertShopCartsDto(List<ShopCarts> shopCartList){
		if(AppUtils.isNotBlank(shopCartList)){
			List<ShopCartsDto> dtoList = new ArrayList<ShopCartsDto>();
			for (ShopCarts shopCarts : shopCartList) {
				
				ShopCartsDto shopCartsDto = new ShopCartsDto();
				shopCartsDto.setShopId(shopCarts.getShopId());
				shopCartsDto.setShopName(shopCarts.getShopName());
				shopCartsDto.setShopStatus(shopCarts.getShopStatus());
				shopCartsDto.setShopTotalCash(shopCarts.getShopTotalCash());
				shopCartsDto.setShopActualTotalCash(shopCarts.getShopActualTotalCash());
				shopCartsDto.setShopTotalWeight(shopCarts.getShopTotalWeight());
				shopCartsDto.setShopTotalVolume(shopCarts.getShopTotalVolume());
				shopCartsDto.setTotalcount(shopCarts.getTotalcount());
				shopCartsDto.setDiscountPrice(shopCarts.getDiscountPrice());
				shopCartsDto.setMailfeeStr(shopCarts.getMailfeeStr());
				shopCartsDto.setShopCouponCount(shopCarts.getShopCouponCount());
				shopCartsDto.setSupportStore(shopCarts.isSupportStore());
				
				/* conver ShopGroupCartItemDto */
				List<MarketingDto> marketingDtoList =  shopCarts.getMarketingDtoList();
				List<ShopGroupCartItemDto> groupCartItems = new ArrayList<ShopGroupCartItemDto>();
				if(AppUtils.isNotBlank(marketingDtoList)){
					for( MarketingDto marketingDto: marketingDtoList){
						ShopGroupCartItemDto cartItemDto = new ShopGroupCartItemDto();
						if(AppUtils.isNotBlank(marketingDto.getType())){
							switch(marketingDto.getType().intValue()){
							   case 0:
								   cartItemDto.setType("满减");
								   break;
							   case 1:
								   cartItemDto.setType("满折");
								   break;
							   case 2:
								   cartItemDto.setType("限时");
								   break;
							    default:
							       cartItemDto.setType("none");
							       break;
							}
						}else{
							 cartItemDto.setType("none");
						}
						cartItemDto.setPromotionInfo(marketingDto.getPromotionInfo());
						List<ShopCartItemDto> shopCartItemDtoList = convertShopCartItemDto(marketingDto.getHitCartItems());
						cartItemDto.setCartItems(shopCartItemDtoList);
						groupCartItems.add(cartItemDto);
					}
				}
				
				shopCartsDto.setGroupCartItems(groupCartItems);
				dtoList.add(shopCartsDto);
				
			}

			return dtoList;
		}
		return null;
	}
	

	private List<ShopCartItemDto> convertShopCartItemDto(List<ShopCartItem> shopCartItemList){
		if(AppUtils.isNotBlank(shopCartItemList)){
			List<ShopCartItemDto> dtoList = new ArrayList<ShopCartItemDto>();
			for (ShopCartItem shopCartItem : shopCartItemList) {
				ShopCartItemDto shopCartItemDto = new ShopCartItemDto();
				/** 购物车ID */
				shopCartItemDto.setBasketId(shopCartItem.getBasketId());
			    /**选中状态 */
				shopCartItemDto.setCheckSts(shopCartItem.getCheckSts());
			    /** 商品ID */
				shopCartItemDto.setProdId(shopCartItem.getProdId());
			    /** shop ID  */
				shopCartItemDto.setShopId(shopCartItem.getShopId());
			    /** skuId */
				shopCartItemDto.setSkuId(shopCartItem.getSkuId());
				/** 商品原价*/
				shopCartItemDto.setPrice(shopCartItem.getPrice());
				/** 促销价格  */
				shopCartItemDto.setPromotionPrice(shopCartItem.getPromotionPrice());
				/** sku图片  */
				shopCartItemDto.setPic(shopCartItem.getPic());
			    /** 购物车商品  */
				shopCartItemDto.setBasketCount(shopCartItem.getBasketCount());
			    /** 商品名称  */
				shopCartItemDto.setProdName(shopCartItem.getProdName());
			    /** sku名称  */
				shopCartItemDto.setSkuName(shopCartItem.getSkuName());
				/** 商品状态 */
				shopCartItemDto.setStatus(shopCartItem.getStatus());
			    /** 库存*/
				shopCartItemDto.setStocks(shopCartItem.getStocks());
			    /** 实际库存*/
				shopCartItemDto.setActualStocks(shopCartItem.getActualStocks());
			    /** 物流重量(千克)  */
				shopCartItemDto.setWeight(shopCartItem.getWeight());
			    /** 物流体积(立方米)  */
			    shopCartItemDto.setVolume(shopCartItem.getVolume());
				 /** 货到付款; 0:普通商品 , 1:货到付款商品   */
			    shopCartItemDto.setIsGroup(shopCartItem.getIsGroup());
			    shopCartItemDto.setIsSupportCod(shopCartItem.isSupportCod());
			    shopCartItemDto.setCnProperties(shopCartItem.getCnProperties());
			    /** 销售属性组合（中文）  */
			    /** 属性Id组合 */
			    shopCartItemDto.setProperties(shopCartItem.getProperties());
			    /** 商品总费用  商品总价格   单价*数量 */
			    shopCartItemDto.setTotal(shopCartItem.getTotal());
			    /** 商品实际总费用  商品总价格  + 运费  - 促销费用*/
			    shopCartItemDto.setTotalMoeny(shopCartItem.getTotalMoeny());
				/** 总重量 */
			    shopCartItemDto.setTotalWeight(shopCartItem.getTotalWeight());
			    shopCartItemDto.setTotalVolume(shopCartItem.getTotalVolume());
			    shopCartItemDto.setIsFailure(shopCartItem.getIsFailure());
				/** -------------------------营销活动信息----------------------  */
				/** 优惠价价格 */
			    shopCartItemDto.setDiscountPrice(shopCartItem.getDiscountPrice());
			    shopCartItemDto.setDiscount(shopCartItem.getDiscount());
			    shopCartItemDto.setSupportStore(shopCartItem.isSupportStore());

			    /** 门店信息 */
			    shopCartItemDto.setStoreId(shopCartItem.getStoreId());
			    shopCartItemDto.setStoreName(shopCartItem.getStoreName());
			    shopCartItemDto.setStoreAddr(shopCartItem.getStoreAddr());

				dtoList.add(shopCartItemDto);
			}
			return dtoList;
		}
		return null;
	}

}
