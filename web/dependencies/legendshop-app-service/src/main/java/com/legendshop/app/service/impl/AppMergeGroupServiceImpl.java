package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.AppActiveBanner;
import com.legendshop.app.dto.AppCouponDto2;
import com.legendshop.app.dto.AppProductPropertyDto;
import com.legendshop.app.dto.AppProductPropertyValueDto;
import com.legendshop.app.dto.AppPropValueImgDto;
import com.legendshop.app.dto.AppSkuDto;
import com.legendshop.app.dto.mergeGroup.AppMergeGroupDetailDto;
import com.legendshop.app.dto.mergeGroup.AppMergeGroupDto;
import com.legendshop.app.dto.mergeGroup.AppMergeGroupGroupDetailDto;
import com.legendshop.app.dto.mergeGroup.AppMergeGroupingDto;
import com.legendshop.app.service.AppMergeGroupService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.ActiveBannerTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.MergeGroupStatusEnum;
import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.dto.MergeGroupingDto;
import com.legendshop.model.dto.ParamGroupDto;
import com.legendshop.model.dto.ProdPicDto;
import com.legendshop.model.dto.ProductDto;
import com.legendshop.model.dto.ProductPropertyDto;
import com.legendshop.model.dto.PropValueImgDto;
import com.legendshop.model.dto.SkuDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.ActiveBanner;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.MergeGroup;
import com.legendshop.model.entity.MergeGroupAdd;
import com.legendshop.model.entity.MergeGroupOperate;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.Sub;
import com.legendshop.spi.service.ActiveBannerService;
import com.legendshop.spi.service.CouponService;
import com.legendshop.spi.service.MergeGroupAddService;
import com.legendshop.spi.service.MergeGroupOperateService;
import com.legendshop.spi.service.MergeGroupService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.SubService;
import com.legendshop.spi.service.UserCouponService;
import com.legendshop.util.AppUtils;

@Slf4j
@Service("appMergeGroupService")
public class AppMergeGroupServiceImpl implements AppMergeGroupService {
	
	@Autowired
	private MergeGroupService mergeGroupService;
	
	@Autowired
	private ActiveBannerService activeBannerService;
	
	@Autowired
	private MergeGroupOperateService mergeGroupOperateService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ShopDetailService shopDetailService;
	
	@Autowired
	private CouponService couponService;
	
	@Autowired
	private UserCouponService userCouponService;
	
	@Autowired
	private MergeGroupAddService mergeGroupAddService;
	
	@Autowired
	private SubService subService;
	
	@Override
	public List<AppActiveBanner> getActiveBanners() {
		
		List<ActiveBanner> bannerList = activeBannerService.getBannerList(ActiveBannerTypeEnum.MERGER_BANNER.value());
		
		List<AppActiveBanner> resultList = new ArrayList<AppActiveBanner>();
		for(ActiveBanner activeBanner : bannerList){
			AppActiveBanner appActiveBanner = new AppActiveBanner();
			appActiveBanner.setId(activeBanner.getId());
			appActiveBanner.setPic(activeBanner.getImageFile());
			appActiveBanner.setUrl(activeBanner.getUrl());
			
			resultList.add(appActiveBanner);
		}
		
		return resultList;
	}

	@Override
	public AppPageSupport<AppMergeGroupDto> getAvailableMergeGroups(String curPageNO) {
		
		PageSupport<MergeGroup> ps = mergeGroupService.getAvailable(curPageNO);
		
		return PageUtils.convertPageSupport(ps, new PageUtils.ResultListConvertor<MergeGroup, AppMergeGroupDto>() {

			@Override
			public List<AppMergeGroupDto> convert(List<MergeGroup> form) {
				
				List<AppMergeGroupDto> resultList = new ArrayList<AppMergeGroupDto>();
				
				for(MergeGroup mergeGroup : form){
					AppMergeGroupDto appMergeGroupDto = new AppMergeGroupDto();
					appMergeGroupDto.setId(mergeGroup.getId());
					appMergeGroupDto.setMergeName(mergeGroup.getMergeName());
					appMergeGroupDto.setPic(mergeGroup.getPic());
					appMergeGroupDto.setMinPrice(mergeGroup.getMinPrice());
					appMergeGroupDto.setMinProdPrice(mergeGroup.getMinProdPrice());
					appMergeGroupDto.setPeopleNumber(mergeGroup.getPeopleNumber());
					appMergeGroupDto.setCount(mergeGroup.getCount());
					
					resultList.add(appMergeGroupDto);
				}
				
				return resultList;
			}
		});
		
	}

	@Override
	public Map<String, Object> getMergeGroupDetail(Long mergeGroupId, String addNumber, String userId) {
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		MergeGroup mergeGroup = mergeGroupService.getMergeGroup(mergeGroupId);
		if (AppUtils.isBlank(mergeGroup)) {
			result.put("status", Constants.FAIL);
			result.put("error", "活动不存在或已下线");
			return result;
		}
		
		if (mergeGroup.getStatus() != MergeGroupStatusEnum.ONLINE.value()) {
			result.put("status", Constants.FAIL);
			result.put("error", "活动不存在或已下线");
			return result;
		}
		
		Date nowDate = new Date();
		if(mergeGroup.getEndTime().getTime()<=nowDate.getTime()){
			result.put("status", Constants.FAIL);
			result.put("error", "该活动已过期");
			return result;
		}
		
//		if(AppUtils.isNotBlank(addNumber)){ //说明是分享进来的
//		MergeGroupOperate mergeGroupOperate = mergeGroupOperateService.getMergeGroupOperateByAddNumber(addNumber);
//		if(AppUtils.isBlank(mergeGroupOperate)){
//			return ResultDtoManager.fail(-1,"活动不存在或已下线");
//		}
//		if(MergeGroupAddStatusEnum.SUCCESS.value()==mergeGroupOperate.getStatus()){
//			return ResultDtoManager.fail(-1,"该团已经拼团成功，请选择参加其他团");
//		}
//		if(MergeGroupAddStatusEnum.FAIL.value()==mergeGroupOperate.getStatus()){
//			return ResultDtoManager.fail(-1,"超过拼团有效期，拼团失败，请选择参加其他团");
//		}
//
//		if(nowDate.getTime() > mergeGroupOperate.getEndTime()){
//			return ResultDtoManager.fail(-1,"超过拼团有效期，拼团失败，请选择参加其他团");
//		}
//	}
		
		/** 获取活动已开团列表, 前五个 */
		List<AppMergeGroupingDto> groupingList = this.getMergeGroupingDto(mergeGroup.getId(), 5);
		
		/** 查询店铺的详情信息 */
		Long shopId = mergeGroup.getShopId();
		ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
		
		if (AppUtils.isBlank(shopDetail)) {
			result.put("status", Constants.FAIL);
			result.put("error", "该活动所属的店铺已不存在!");
			return result;
		}
		
		if (!shopDetail.getStatus().equals(ShopStatusEnum.NORMAL.value())) {
			result.put("status", Constants.FAIL);
			result.put("error", "该活动所属的店铺已经被下线或关闭!");
			return result;
		}
		
		//包邮描述
		String mailfeeStr = null; 
		Integer mailfeeSts = shopDetail.getMailfeeSts();// 包邮状态
		if (mailfeeSts != null && mailfeeSts == 1) {
			// 包邮类型
			Integer mailfeeType = shopDetail.getMailfeeType();
			Double mailfeeCon = shopDetail.getMailfeeCon();
			
			if (mailfeeType == 1) {// 满金额包邮
				mailfeeStr = "满" + mailfeeCon + "元包邮";
			} else if (mailfeeType == 2 && AppUtils.isNotBlank(mailfeeCon)) {
				mailfeeStr = "满" + mailfeeCon.intValue() + "件包邮";
			}
		}
		
		ProductDto prod = productService.getProductDtoForMergrGroup(mergeGroup.getProdId(),mergeGroupId);
		if (AppUtils.isBlank(prod)) {
			result.put("status", Constants.FAIL);
			result.put("error", "拼团商品已被删除或已被下线");
			return result;
		}
		
		/**
		 * 获取平台, 店铺, 商品相关优惠券
		 */
		// TODO 拼团活动没有优惠券
/*		List<Coupon> allCouponList = couponService.getShopAndProdsCoupons(prod.getShopId(), prod.getProdId());
		List<AppCouponDto2> couponList = new ArrayList<AppCouponDto2>();
		List<AppCouponDto2> userCouponList = new ArrayList<AppCouponDto2>();
		for (Coupon coupon : allCouponList) {
			
			List<UserCoupon> usercoupons = null;
			if(null != userId){
				usercoupons = userCouponService.getUserCoupons(coupon.getCouponId(), userId);
			}
			
			AppCouponDto2 appCoupon = convertToAppCouponDto(coupon);
			
			if(AppUtils.isNotBlank(usercoupons)){
				userCouponList.add(appCoupon);
			}else {
				couponList.add(appCoupon);
			}
		}*/
		
		/** 组装 AppMergeGroupDetailDto */
		AppMergeGroupDetailDto mergeGroupDetail = new AppMergeGroupDetailDto();
		
		//组装拼团活动信息
		mergeGroupDetail.setId(mergeGroup.getId());
		mergeGroupDetail.setPeopleNumber(mergeGroup.getPeopleNumber());
		mergeGroupDetail.setIsHeadFree(mergeGroup.getIsHeadFree());
		mergeGroupDetail.setMinPrice(mergeGroup.getMinPrice());
		mergeGroupDetail.setMinProdPrice(mergeGroup.getMinProdPrice());
		mergeGroupDetail.setIsLimit(mergeGroup.getIsLimit());
		mergeGroupDetail.setLimitNumber(mergeGroup.getLimitNumber());
		mergeGroupDetail.setLimitedEndtime(mergeGroup.getEndTime().getTime());
		mergeGroupDetail.setLimitedStarttime(mergeGroup.getStartTime().getTime());
		mergeGroupDetail.setAddNumber(addNumber);
		
		mergeGroupDetail.setGroupingList(groupingList);
		
		//组装拼团商品信息
		mergeGroupDetail.setProdName(prod.getName());
		mergeGroupDetail.setProdId(prod.getProdId());
		mergeGroupDetail.setSkuList(convertSkuDto(prod.getSkuDtoList()));  //sku集合
		mergeGroupDetail.setPic(prod.getPic());
		mergeGroupDetail.setPrice(prod.getPrice());
		mergeGroupDetail.setCash(prod.getCash());
		mergeGroupDetail.setProdPropList(convertProductPropertyDto(prod.getProdPropDtoList()));
		mergeGroupDetail.setProdPics(convertProductPicDto(prod.getProdPics(), prod.getPropValueImgList()));
		mergeGroupDetail.setBrief(prod.getBrief());
		mergeGroupDetail.setProVideoUrl(prod.getProVideoUrl());
		mergeGroupDetail.setPropValueImgList(convertPropValueImgList(prod.getPropValueImgList()));//商品属性图片集合
		if (AppUtils.isNotBlank(prod.getParameter())) {
			List<ParamGroupDto> paramList = productService.buildParamGroups(prod.getProdId(), prod.getParameter(), prod.getUserParameter());
			mergeGroupDetail.setParameters(paramList);
		}
		mergeGroupDetail.setContent(prod.getContentM());//手机端图文详情
		
		//组装店铺信息
		mergeGroupDetail.setShopId(shopDetail.getShopId());
		mergeGroupDetail.setShopName(shopDetail.getSiteName());
		mergeGroupDetail.setShopLogo(shopDetail.getShopPic());
		mergeGroupDetail.setShopType(shopDetail.getShopType());
		String shopScore = shopDetailService.getshopScore(shopDetail.getShopId());
		mergeGroupDetail.setShopScore(shopScore);
		mergeGroupDetail.setMailfee(mailfeeStr);
		
		/*mergeGroupDetail.setCouponList(couponList);
		mergeGroupDetail.setUserCouponList(userCouponList);*/
		
		result.put("status", Constants.SUCCESS);
		result.put("mergeGroupDetail", mergeGroupDetail);
		
		return result;
	}
	
	@Override
	public List<AppMergeGroupingDto> getMergeGroupingDto(Long mergeGroupId, Integer count) {
		
		List<MergeGroupingDto> groupingList = mergeGroupAddService.getMergeGroupingByMergeId(mergeGroupId, count);
		
		return convertToAppMergeGroupingDtoList(groupingList);
	}
	
	@Override
	public Map<String, Object> getMergeGroupGroupDetail(String sn, String source, String userId) {
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		String addNumber = sn;
		if(source.equals("SUB")){
			
			//这里暂不校验用户
/*			if(null == userId){
				result.put("status", Constants.FAIL);
				result.put("error", "对不起, 请不要非法访问!");
				return result;
			}*/
			
			Sub sub = subService.getSubBySubNumber(sn);
			if(null == sub || OrderStatusEnum.CLOSE.value().equals(sub.getStatus()) 
					|| OrderStatusEnum.UNPAY.value().equals(sub.getStatus())){
				log.info("============================ sub不存在 ============================");
				result.put("status", Constants.FAIL);
				result.put("error", "对不起, 您要查看的团不存在!");
				return result;
				
			}
			addNumber = sub.getAddNumber();
		}
		
		if(AppUtils.isBlank(addNumber)){
			log.info("============================ addNumber不存在 ============================");
			result.put("status", Constants.FAIL);
			result.put("error", "对不起, 您要查看的团不存在!");
			return result;
		}
		
		//获取已开团的团数据
		MergeGroupOperate mergeGroupOperate = mergeGroupOperateService.getMergeGroupOperateByAddNumber(addNumber);
		if(null == mergeGroupOperate){
			log.info("============================ 已开团的团数据不存在 ============================");
			result.put("status", Constants.FAIL);
			result.put("error", "对不起, 您要查看的团不存在!");
			return result;
		}
		
		//获取已参加该团的参团信息, 团长要排第一位
		List<MergeGroupAdd> mergeGroupAddList = mergeGroupAddService.queryMergeGroupAddList(mergeGroupOperate.getId(), 5);
		if(AppUtils.isBlank(mergeGroupAddList)){
			log.info("============================ 已参团信息不存在 ============================");
			result.put("status", Constants.FAIL);
			result.put("error", "对不起, 您要查看的团错误!");
			return result;
		}
		
		//获取团对应的拼团活动
		MergeGroup mergeGroup = mergeGroupService.getMergeGroup(mergeGroupOperate.getMergeId());
		if(null == mergeGroup){
			log.info("============================ 拼团活动不存在 ============================");
			result.put("status", Constants.FAIL);
			result.put("error", "对不起, 您要查看拼团活动不存在!");
			return result;
		}
		
		boolean isSelf = false;//是否自己的团
		boolean isParticipate = false;//是否已参加该团
		String subNumber = null;
		
		if(null != userId){//如果用户已登录
			
			//判断当前团是否是自己开的团
			MergeGroupAdd groupHeader = mergeGroupAddList.get(0);//第一个是团长
			if(userId.equals(groupHeader.getUserId())){//判断团长是否是当前登录用户, 如果是
				isSelf = true;
				subNumber = groupHeader.getSubNumber();
			}
			
			isParticipate = isSelf;//因为开团者肯定是参与者
			
			if(!isParticipate){
				//判断当前用户是否已参与该团
				for(MergeGroupAdd mergeGroupAdd : mergeGroupAddList){
					if(userId.equals(mergeGroupAdd.getUserId())){//如果是
						isParticipate = true;
						subNumber = mergeGroupAdd.getSubNumber();
						break;
					}
				}
				
				//因为上面不是所有参团信息, 如果还不能确定是否已参加该团, 那么到数据库在查询一次
				if(!isParticipate){
					MergeGroupAdd groupParticipates = mergeGroupAddService.getMergeGroupAddByOperateIdAndUserId(mergeGroupOperate.getId(), userId);
					if(null != groupParticipates){
						isParticipate = true;
						subNumber = groupParticipates.getSubNumber();
					}
				}
			}
		}
		
		ProductDto prod = productService.getProductDtoForMergrGroup(mergeGroup.getProdId(),mergeGroup.getId());
		if(null == prod){
			log.info("============================ 拼团商品不存在 ============================");
			result.put("status", Constants.FAIL);
			result.put("error", "对不起, 您要查看拼团活动不存在!");
			return result;
		}
		
		AppMergeGroupGroupDetailDto appMergeGroupGroupDetailDto = new AppMergeGroupGroupDetailDto();
		
		//拼团活动部分属性
		appMergeGroupGroupDetailDto.setMergeId(mergeGroup.getId());
		appMergeGroupGroupDetailDto.setProdId(mergeGroup.getProdId());
		appMergeGroupGroupDetailDto.setProdName(prod.getName());
		appMergeGroupGroupDetailDto.setProdPic(prod.getPic());
		appMergeGroupGroupDetailDto.setMinPrice(mergeGroup.getMinPrice());
		appMergeGroupGroupDetailDto.setMinProdPrice(mergeGroup.getMinProdPrice());
		appMergeGroupGroupDetailDto.setPeopleNumber(mergeGroup.getPeopleNumber());
		appMergeGroupGroupDetailDto.setIsHeadFree(mergeGroup.getIsHeadFree());
		appMergeGroupGroupDetailDto.setIsLimit(mergeGroup.getIsLimit());
		appMergeGroupGroupDetailDto.setLimitNumber(mergeGroup.getLimitNumber());
		appMergeGroupGroupDetailDto.setActiveCreateTime(mergeGroup.getCreateTime());
		appMergeGroupGroupDetailDto.setActiveStartTime(mergeGroup.getStartTime());
		appMergeGroupGroupDetailDto.setActiveEndTime(mergeGroup.getEndTime());
		appMergeGroupGroupDetailDto.setActiveStatus(mergeGroup.getStatus());
		
		//团部分属性
		appMergeGroupGroupDetailDto.setGroupId(mergeGroupOperate.getId());
		appMergeGroupGroupDetailDto.setGroupNumber(mergeGroupOperate.getAddNumber());
		appMergeGroupGroupDetailDto.setParticipantNumber(mergeGroupOperate.getNumber());
		appMergeGroupGroupDetailDto.setOpenGroupTime(mergeGroupOperate.getCreateTime());
		appMergeGroupGroupDetailDto.setGroupEndTime(mergeGroupOperate.getEndTime());
		appMergeGroupGroupDetailDto.setGroupStatus(mergeGroupOperate.getStatus());
		
		appMergeGroupGroupDetailDto.setIsSelf(isSelf);
		appMergeGroupGroupDetailDto.setIsParticipate(isParticipate);
		
		List<AppMergeGroupGroupDetailDto.AddUserList> addUserList = new ArrayList<AppMergeGroupGroupDetailDto.AddUserList>();
		for(MergeGroupAdd mergeGroupAdd : mergeGroupAddList){
			
			AppMergeGroupGroupDetailDto.AddUserList addUser = new AppMergeGroupGroupDetailDto.AddUserList();
			addUser.setUserId(mergeGroupAdd.getUserId());
			addUser.setUserName(mergeGroupAdd.getUserName());
			addUser.setNickName(mergeGroupAdd.getNickName());
			addUser.setPortraitPic(mergeGroupAdd.getPortraitPic());
			addUser.setIsHead(mergeGroupAdd.getIsHead());
			
			addUserList.add(addUser);
		}
		appMergeGroupGroupDetailDto.setAddUserList(addUserList);
		
		appMergeGroupGroupDetailDto.setSubNumber(subNumber);
		
		appMergeGroupGroupDetailDto.setSkuList(convertSkuDto(prod.getSkuDtoList()));
		appMergeGroupGroupDetailDto.setProdPropList(convertProductPropertyDto(prod.getProdPropDtoList()));
		appMergeGroupGroupDetailDto.setPropValueImgList(convertPropValueImgList(prod.getPropValueImgList()));
		
		result.put("status", Constants.SUCCESS);
		result.put("mergeGroupGroupDetail", appMergeGroupGroupDetailDto);
		return result;
	}

	/**
	 * 将model SkuDto转换AppSkuDto
	 */
	private List<AppSkuDto> convertSkuDto(List<SkuDto> skuDtoList){
		if(AppUtils.isNotBlank(skuDtoList)){
			List<AppSkuDto> appSkuDtoList = new ArrayList<AppSkuDto>();
			for (SkuDto skuDto : skuDtoList) {
				AppSkuDto appSkuDto = new AppSkuDto();
				appSkuDto.setSkuId(skuDto.getSkuId());
				appSkuDto.setName(skuDto.getName());
				appSkuDto.setStatus(skuDto.getStatus());
				appSkuDto.setStocks(skuDto.getStocks());
				appSkuDto.setPrice(skuDto.getPrice());
				appSkuDto.setCash(skuDto.getCash());
				appSkuDto.setPromotionPrice(skuDto.getPromotionPrice());
				appSkuDto.setMergePrice(skuDto.getMergePrice());
				appSkuDto.setProperties(skuDto.getProperties());
				appSkuDto.setRuleList(skuDto.getRuleList());
				appSkuDto.setModelId(skuDto.getModelId());
				appSkuDto.setSkuId(skuDto.getSkuId());
				appSkuDtoList.add(appSkuDto);
			}
			return appSkuDtoList;
		}
		
		return null;
	}
	
	private List<AppProductPropertyDto> convertProductPropertyDto(List<ProductPropertyDto> productPropertyDtoList){
		if(AppUtils.isNotBlank(productPropertyDtoList)){
			List<AppProductPropertyDto> appPropertyDtoList = new ArrayList<AppProductPropertyDto>();
			for (ProductPropertyDto productPropertyDto : productPropertyDtoList) {
				AppProductPropertyDto appProductPropertyDto = new AppProductPropertyDto();
				appProductPropertyDto.setPropId(productPropertyDto.getPropId());
				appProductPropertyDto.setPropName(productPropertyDto.getPropName());
				appProductPropertyDto.setProdPropValList(convertProductPropertyValueDto(productPropertyDto.getProductPropertyValueList()));
				appPropertyDtoList.add(appProductPropertyDto);
			}
			return appPropertyDtoList;
		}
		return null;
	}
	
	private List<AppProductPropertyValueDto> convertProductPropertyValueDto(List<com.legendshop.model.dto.ProductPropertyValueDto> productPropertyValueDtoList){
		if(AppUtils.isNotBlank(productPropertyValueDtoList)){
			List<AppProductPropertyValueDto> appPropertyValueDtoList = new ArrayList<AppProductPropertyValueDto>();
			for (com.legendshop.model.dto.ProductPropertyValueDto productPropertyValueDto : productPropertyValueDtoList) {
				AppProductPropertyValueDto appProductPropertyValueDto = new AppProductPropertyValueDto();
				appProductPropertyValueDto.setIsSelected(productPropertyValueDto.getIsSelected());
				appProductPropertyValueDto.setPropId(productPropertyValueDto.getPropId());
				appProductPropertyValueDto.setName(productPropertyValueDto.getName());
				appProductPropertyValueDto.setValueId(productPropertyValueDto.getValueId());
				appPropertyValueDtoList.add(appProductPropertyValueDto);
			}
			return appPropertyValueDtoList;
		}
		
		return null;
	}
	
	private List<String> convertProductPicDto(List<ProdPicDto> prodPicDtoList, List<PropValueImgDto> propValImgList){
		List<String> productPics = new ArrayList<>();
		if(AppUtils.isNotBlank(propValImgList)){
			for (PropValueImgDto propValImg:propValImgList) {
				productPics.addAll(propValImg.getImgList());
			}
		}else{
			if(AppUtils.isNotBlank(prodPicDtoList)){
				for (ProdPicDto prodPicDto : prodPicDtoList) {
					productPics.add(prodPicDto.getFilePath());
				}
			}
		}
		return productPics;
	}
	
	/**
	 * 转换商品属性图片
	 */
	private List<AppPropValueImgDto> convertPropValueImgList(List<PropValueImgDto> proValueImgs){
		if(AppUtils.isBlank(proValueImgs)){
			return null;
		}
		List<AppPropValueImgDto> appProValueImgs=new ArrayList<AppPropValueImgDto>();
		for(PropValueImgDto imgDto : proValueImgs){
			AppPropValueImgDto appImgDto=new AppPropValueImgDto();
			appImgDto.setValueId(imgDto.getValueId());
			appImgDto.setValueName(imgDto.getValueName());
			appImgDto.setPropId(imgDto.getPropId());
			appImgDto.setValueImage(imgDto.getValueImage());
			appImgDto.setSeq(imgDto.getSeq());
			appImgDto.setImgList(imgDto.getImgList());
			appProValueImgs.add(appImgDto);
		}
		return appProValueImgs;
	}
	
	private AppCouponDto2 convertToAppCouponDto(Coupon coupon){
		
		AppCouponDto2 appCoupon = new AppCouponDto2();
		appCoupon.setCouponId(coupon.getCouponId());
		appCoupon.setCouponName(coupon.getCouponName());
		appCoupon.setDescription(coupon.getDescription());
		appCoupon.setFullPrice(coupon.getFullPrice());
		appCoupon.setOffPrice(coupon.getOffPrice());
		appCoupon.setStartDate(coupon.getStartDate());
		appCoupon.setEndDate(coupon.getEndDate());
		appCoupon.setCouponProvider(coupon.getCouponProvider());
		appCoupon.setCouponType(coupon.getCouponType());
		appCoupon.setStatus(coupon.getStatus());
		appCoupon.setCouponPic(coupon.getCouponPic());
		appCoupon.setGetLimit(coupon.getGetLimit());
		appCoupon.setCreateDate(coupon.getCreateDate());
		
		return appCoupon;
	}
	
	private List<AppMergeGroupingDto> convertToAppMergeGroupingDtoList(List<MergeGroupingDto> list){
		
		List<AppMergeGroupingDto> resultList = new ArrayList<AppMergeGroupingDto>();
		for(MergeGroupingDto mergeGroupingDto : list){
			AppMergeGroupingDto appMergeGroupingDto = new AppMergeGroupingDto();
			appMergeGroupingDto.setAddNumber(mergeGroupingDto.getAddNumber());
			appMergeGroupingDto.setPortraitPic(mergeGroupingDto.getPortraitPic());
			appMergeGroupingDto.setPropleCount(mergeGroupingDto.getPropleCount());
			appMergeGroupingDto.setCreateTime(mergeGroupingDto.getCreateTime());
			appMergeGroupingDto.setEndTime(mergeGroupingDto.getEndTime());
			
			resultList.add(appMergeGroupingDto);
		}
		
		return resultList;
	}

	@Override
	public List<AppActiveBanner> getSeckillActiveBanners() {
		List<ActiveBanner> bannerList = activeBannerService.getBannerList(ActiveBannerTypeEnum.SECKILL_BANNER.value());
		
		List<AppActiveBanner> resultList = new ArrayList<AppActiveBanner>();
		for(ActiveBanner activeBanner : bannerList){
			AppActiveBanner appActiveBanner = new AppActiveBanner();
			appActiveBanner.setId(activeBanner.getId());
			appActiveBanner.setPic(activeBanner.getImageFile());
			appActiveBanner.setUrl(activeBanner.getUrl());
			
			resultList.add(appActiveBanner);
		}
		
		return resultList;
	}

}
