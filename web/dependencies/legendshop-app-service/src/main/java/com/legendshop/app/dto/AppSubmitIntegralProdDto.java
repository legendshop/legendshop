package com.legendshop.app.dto;



import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 提交积分商品订单页面数据Dto
 */
@ApiModel(value="提交积分商品订单页面数据Dto") 
public class AppSubmitIntegralProdDto {

	/** 积分商品Id*/
	@ApiModelProperty(value="积分商品Id") 
	private Long id;

	/** 积分商品名称 */
	@ApiModelProperty(value="积分商品名称") 
	private String name;

	/** 兑换积分 */
	@ApiModelProperty(value="兑换积分") 
	private Integer exchangeIntegral;

	/** 商品图片 */
	@ApiModelProperty(value="商品图片") 
	private String prodImage;

	/** 状态 */
	@ApiModelProperty(value="状态") 
	private Integer status;

	public AppSubmitIntegralProdDto() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getExchangeIntegral() {
		return exchangeIntegral;
	}

	public void setExchangeIntegral(Integer exchangeIntegral) {
		this.exchangeIntegral = exchangeIntegral;
	}

	public String getProdImage() {
		return prodImage;
	}

	public void setProdImage(String prodImage) {
		this.prodImage = prodImage;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
