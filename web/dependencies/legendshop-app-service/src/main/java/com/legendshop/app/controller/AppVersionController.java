/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.constants.StatusCodes;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.dto.app.AppVersion;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.spi.service.AppVersionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

/**
 * app系统版本
 */
@ApiIgnore
@Api(tags="系统版本",value="获取系统的版本号")
@RestController
public class AppVersionController {
	
	private static Logger LOGGER = LoggerFactory.getLogger(AppVersionController.class);
	
	@Autowired
    private AppVersionService appVersionService;
	
    /** 系统配置. */
    @Autowired
    private PropertiesUtil propertiesUtil;

	/**
	 * 获取系统的版本号和获取APK的下载地址
	 * @param request
	 * @param response
	 * @param platform
	 * @return
	 */
	@ApiOperation(value = "获取系统的版本号", httpMethod = "POST", notes = "获取系统的版本号和获取APK的下载地址，返回一个map集合",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/version")
	public ResultDto<Map<String, Object>> version(HttpServletRequest request, HttpServletResponse response, String platform) {
		try{
			Map<String, Object> result = new HashMap<String, Object>();
			AppVersion appVersion = appVersionService.getAppVersion();
			 
			if(null == appVersion){
				result.put("version", null);
				result.put("url", null);
				return ResultDtoManager.success(result);
			}
			
			String prefix = propertiesUtil.getMobileDomainName() + "/" + propertiesUtil.getAppFileSubPath();
			
			result.put("version", appVersion.getAndroidUserVersion());
			result.put("url", prefix + "/" + appVersion.getAndroidUserApk());
			
			return ResultDtoManager.success(result);
		}catch (Exception e) {
			LOGGER.debug("获取版本号异常!", e);
			return ResultDtoManager.fail(StatusCodes.UNKNOWN_ERROR, "获取版本号异常,未知错误!");
		}
	}
}
