package com.legendshop.app.controller;

import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.seckill.SeckillSuccessRecord;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.SeckillService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.MD5Util;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/p/secKill")
@Api(tags = "我的秒杀", value = "我的秒杀")
public class AppUserSecKillRecController {

	@Autowired
	private SeckillService seckillService;

	@Autowired
	private LoginedUserService loginedUserService;

	/**
	 * 查询秒杀记录
	 *
	 * @param curPageNO the cur page no
	 * @return the string
	 */
	@GetMapping("/query")
	@ApiOperation(value = "查询秒杀记录", httpMethod = "GET", notes = "查询秒杀记录", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "curPageNO", name = "curPageNO", value = "页码", required = true, dataType = "string")
	})
	public ResultDto<AppPageSupport<SeckillSuccessRecord>> query(String curPageNO) {

		LoginedUserInfo user = loginedUserService.getUser();
		String userId = user.getUserId();

		PageSupport<SeckillSuccessRecord> ps = seckillService.queryUserSeckillRecord(curPageNO, userId);
		AppPageSupport<SeckillSuccessRecord> result = new AppPageSupport<>();
		result.setTotal(ps.getTotal());
		result.setCurrPage(ps.getCurPageNO());
		result.setPageSize(ps.getPageSize());
		result.setResultList(ps.getResultList());
		result.setPageCount(ps.getPageCount());
		List<SeckillSuccessRecord> successRecords = result.getResultList();
		if (AppUtils.isNotBlank(successRecords)) {
			for (SeckillSuccessRecord successRecord : successRecords) {
				String secretKey = MD5Util.toMD5(String.valueOf(successRecord.getSeckillId()) + successRecord.getProdId() + successRecord.getSkuId() + Constants._MD5);
				successRecord.setSecretKey(secretKey);
			}
			result.setResultList(successRecords);
		}

		return ResultDtoManager.success(result);
	}

	/**
	 * 删除记录
	 */
	@GetMapping(value = "/deleteRecord/{id}")
	@ApiOperation(value = "删除秒杀记录", httpMethod = "GET", notes = "删除秒杀记录", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "id", name = "id", value = "秒杀记录Id", required = true, dataType = "Long")
	})
	public ResultDto<Void> deleteRecord(@PathVariable Long id) {

		LoginedUserInfo user = loginedUserService.getUser();
		String userId = user.getUserId();

		seckillService.deleteRecord(id, userId);
		return ResultDtoManager.success();
	}

}
