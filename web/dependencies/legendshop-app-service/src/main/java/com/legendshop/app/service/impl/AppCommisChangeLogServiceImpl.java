/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.model.dto.promotor.CommisTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.AppCommisChangeLogDto;
import com.legendshop.app.dto.AppWithdrawalLogDto;
import com.legendshop.app.service.AppCommisChangeLogService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.business.dao.promoter.CommisChangeLogDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.promotor.AwardSearchParamDto;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.promoter.model.CommisChangeLog;
import com.legendshop.spi.service.CommisChangeLogService;
import com.legendshop.util.AppUtils;

/**
 * app佣金变更历史Service实现类
 */
@Service("appCommisChangeLogService")
public class AppCommisChangeLogServiceImpl implements AppCommisChangeLogService {

	@Autowired
	private CommisChangeLogDao commisChangeLogDao;

	/**
	 * 获取佣金变更历史 
	 */
	@Override
	public AppPageSupport<AppCommisChangeLogDto> getCommisChangeLog(int pageSize, String userId,String curPageNO) {
		PageSupport<CommisChangeLog> ps = commisChangeLogDao.getCommisChangeLog(curPageNO, pageSize, userId);
		AppPageSupport<AppCommisChangeLogDto> convertPageSupport = PageUtils.convertPageSupport(ps,new PageUtils.ResultListConvertor<CommisChangeLog, AppCommisChangeLogDto>() {
			@Override
			public List<AppCommisChangeLogDto> convert(List<CommisChangeLog> form) {
				List<AppCommisChangeLogDto> toList = new ArrayList<AppCommisChangeLogDto>();
				for (CommisChangeLog commisChangeLog : form) {
					if (AppUtils.isNotBlank(commisChangeLog)) {
						AppCommisChangeLogDto logDto =  convertToAppCommisChangeLogDto(commisChangeLog);
						toList.add(logDto);
					}
				}
				return toList;
			}
			
			private AppCommisChangeLogDto convertToAppCommisChangeLogDto(CommisChangeLog commisChangeLog){
				AppCommisChangeLogDto appCommisChangeLogDto = new AppCommisChangeLogDto();
				appCommisChangeLogDto.setAddTime(commisChangeLog.getAddTime());
				appCommisChangeLogDto.setAmount(commisChangeLog.getAmount());
				appCommisChangeLogDto.setCommisType(commisChangeLog.getCommisType());
				appCommisChangeLogDto.setCommisTypeName(CommisTypeEnum.getDesc(commisChangeLog.getCommisType()));
				appCommisChangeLogDto.setId(commisChangeLog.getId());
				appCommisChangeLogDto.setLogDesc(commisChangeLog.getLogDesc());
				appCommisChangeLogDto.setSettleSts(commisChangeLog.getSettleSts());
				appCommisChangeLogDto.setSettleTime(commisChangeLog.getSettleTime());
				appCommisChangeLogDto.setSn(commisChangeLog.getSn());
				appCommisChangeLogDto.setSubUserName(commisChangeLog.getSubUserName());
				return appCommisChangeLogDto;
			}
		});
		return convertPageSupport;
	}
	
}
