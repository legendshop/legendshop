/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service;

import com.legendshop.model.dto.app.AppMyFavoriteDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.Myfavorite;

/**
 * 我的商品关注服务.
 */
public interface MyFavoriteService{

	void deleteFavs(String userId, String selectedFavs);
	
	void deleteAllFavs(String userId);
	
	void saveFavorite(Myfavorite myfavorite);
	
	Boolean isExistsFavorite(Long prodId,String userName);
	
	Long getfavoriteLength(String userId);
	
	/**
	 * 根据用户名获取优惠券数量
	 * @param userName
	 * @return
	 */
	public Long getCouponLength(String userName);

	public abstract Boolean collectProduct(Long prodId, String userName, String userId);
	
	/**
	 * @Description: 更改支付密码
	 * @date 2016-7-13 
	 */
	public void updatePaymentPassword(String userId,String userName, String paymentPassword);

	public boolean cancelFavorite(Long prodId, String userId);

	/**
	 * 批量收藏
	 */
	public boolean collectProducts(String selectedProdId, String userName, String userId);

	/**
	 * 获取我收藏的商品列表
	 * @param curPageNO 当前页码
	 * @param userId 用户id
	 * @return
	 */
	public AppPageSupport<AppMyFavoriteDto> getAppFavoriteList(String curPageNO, Integer pageSize, String userId);

}
