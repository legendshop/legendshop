package com.legendshop.app.service;

import com.legendshop.model.dto.app.NewsDto;
import com.legendshop.app.dto.AppNewsDto;
import com.legendshop.model.dto.app.AppPageSupport;

/**
 * app 文章service
 */
public interface AppNewsService {

	public AppPageSupport<NewsDto> getNewsList(String curPageNO,Long shopId);

	/**
	 * 根据栏目分类id获取文章列表
	 * @param newsCategoryId 栏目分类id
	 * @param curPageNO 当前页码
	 * @return
	 */
	public AppPageSupport<AppNewsDto> getNewsListPage(Long newsCategoryId, String curPageNO);

	
	/**
	 * 获取文章详情
	 * @param newsId 文章id
	 * @return
	 */
	public AppNewsDto getNews(Long newsId);
	
	
}
