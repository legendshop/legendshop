package com.legendshop.app.service;


/**
 * app 我的足迹service
 */
public interface AppVisitLogService {

	/**
	 * 获取我的足迹统计数量
	 * @param userName 用户名
	 * @return
	 */
	public abstract Long getVisitLogCountByUserName(String userName);

}
