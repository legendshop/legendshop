package com.legendshop.app.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 支付回调携带参数Dto
 */
@ApiModel(value="支付回调携带参数Dto")
public class AppReturnPayDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="订单编号")
	private String subSettlementSn;
	
	
	@ApiModelProperty(value="单据类型")
	private String subSettlementType;

	@ApiModelProperty(value="状态，成功为1")
	private Integer status;

	public String getSubSettlementSn() {
		return subSettlementSn;
	}

	public void setSubSettlementSn(String subSettlementSn) {
		this.subSettlementSn = subSettlementSn;
	}

	public String getSubSettlementType() {
		return subSettlementType;
	}

	public void setSubSettlementType(String subSettlementType) {
		this.subSettlementType = subSettlementType;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
