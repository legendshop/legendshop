package com.legendshop.app.service;

import com.legendshop.app.dto.AppVisitHistoryDto;
import com.legendshop.model.dto.app.AppPageSupport;

/**
 * 我的足迹app service
 */
public interface AppVisitHistoryService {

	/**
	 * 查询用户的访问足迹列表(仅仅是查询商品部分哦)
	 * @param userId
	 * @return
	 */
	AppPageSupport<AppVisitHistoryDto> queryVisitHistory(String curPageNO, String userName);
	
	/**
	 * 清空浏览历史
	 * @param userName
	 * @return
	 */
	boolean clearVisitHistory(String userName);
	
}
