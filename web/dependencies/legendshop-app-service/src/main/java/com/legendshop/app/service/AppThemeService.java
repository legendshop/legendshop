package com.legendshop.app.service;

import com.legendshop.app.dto.AppThemeDetailDto;
import com.legendshop.app.dto.AppThemeListDto;
import com.legendshop.model.dto.app.AppPageSupport;

/**
 * app 专题活动service
 */
public interface AppThemeService {

	/**
	 * 获取专题活动列表
	 * @param curPageNO 当前页码
	 * @return
	 */
	public AppPageSupport<AppThemeListDto> getThemePage(String curPageNO);

	/**
	 * 获取专题活动详情
	 * @param themeId 专题活动id
	 * @return
	 */
	public AppThemeDetailDto getThemeDetail(Long themeId);

}
