package com.legendshop.app.dto;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("会员积分")
public class AppIntegraLogDto {
	
	/** 唯一标识id */
	@ApiModelProperty(value = "唯一标识id")
	private Long id; 
		
	/** 用户ID */
	@ApiModelProperty(value = "用户ID",hidden=true)
	private String userId; 
		
	/** 后台用户操作ID */
	@ApiModelProperty(value = "后台用户操作ID",hidden=true)
	private String updateUserId; 
		
	/** 积分数量，- 为负数 */
	@ApiModelProperty(value = "积分数量，- 为负数")
	private Integer integralNum; 
		
	/** 日志类型[1:注册 2：登录 3:充值] IntegralLogTypeEnum */
	@ApiModelProperty(value = "日志类型[1:注册 2:登录 3:充值 4:积分订单兑换 5:完善用户资料 6:订单评论 7:推荐用户送积分 8:晒单送积分 9:订单消费送积分]")
	private Integer logType;

	/** 日志类型名称 IntegralLogTypeEnum */
	@ApiModelProperty(value = "日志类型名称")
	private String logTypeName;
		
	/** 日志描述 */
	@ApiModelProperty(value = "日志描述")
	private String logDesc; 
		
	/** 添加时间 */
	@ApiModelProperty(value = "添加时间")
	private Date addTime; 
	
	/** 昵称 */
	@ApiModelProperty(value = "昵称",hidden=true)
	private String nickName;
	
	/** 手机号码 */
	@ApiModelProperty(value = "手机号码",hidden=true)
	private String userMobile;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Integer getIntegralNum() {
		return integralNum;
	}

	public void setIntegralNum(Integer integralNum) {
		this.integralNum = integralNum;
	}

	public Integer getLogType() {
		return logType;
	}

	public void setLogType(Integer logType) {
		this.logType = logType;
	}

	public String getLogDesc() {
		return logDesc;
	}

	public void setLogDesc(String logDesc) {
		this.logDesc = logDesc;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public String getLogTypeName() {
		return logTypeName;
	}

	public void setLogTypeName(String logTypeName) {
		this.logTypeName = logTypeName;
	}
}
