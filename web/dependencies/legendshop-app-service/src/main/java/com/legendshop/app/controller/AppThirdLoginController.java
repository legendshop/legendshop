/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.app.controller;

import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.app.dto.AppThirdUserInfo;
import com.legendshop.app.service.impl.AppLoginUtil;
import com.legendshop.business.manager.impl.ThirdUserAuthorizeManagerFactory;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.constant.PassportSourceEnum;
import com.legendshop.model.dto.ThirdUserAuthorizeResult;
import com.legendshop.model.dto.app.AppTokenDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.entity.AppToken;
import com.legendshop.model.entity.User;
import com.legendshop.spi.manager.ThirdUserAuthorizeManager;
import com.legendshop.util.AppUtils;
import com.legendshop.web.helper.IPHelper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 用户第三方登录相关接口
 */
@Controller
@Api(tags="第三方登录",value="用户第三方登录相关接口")
@RequestMapping("/thirdlogin")
public class AppThirdLoginController {

	private static Logger log = LoggerFactory.getLogger(AppThirdLoginController.class);

	@Autowired
	private AppLoginUtil appLoginUtil;

	@Autowired
	private PropertiesUtil propertiesUtil;

	/**
	 * H5网页前往认证授权
	 * @param request
	 * @param response
	 * @param type 类型
	 * @param source 来源
	 * @return
	 */
	@ApiIgnore
	@GetMapping("/{type}/{source}/authorize")
	public String authorize(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String type, @PathVariable String source) {

		String authorizeURL = "";
		try {
			ThirdUserAuthorizeManager thirdUserAuthorizeManager = ThirdUserAuthorizeManagerFactory.getIntance(type);
			authorizeURL = thirdUserAuthorizeManager.constructAuthorizeUrl(source);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info(authorizeURL);
		return "redirect:" + authorizeURL;
	}

	/**
	 * H5网页授权成功回调
	 * @param request
	 * @param response
	 * @param type 类型
	 * @return
	 */
	@ApiIgnore
	@GetMapping("/{type}/h5/callback")
	public String h5AuthorizeCallback(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String type, String code, String state) {
		try {
			ThirdUserAuthorizeManager thirdUserAuthorizeManager = ThirdUserAuthorizeManagerFactory.getIntance(type);

			ThirdUserAuthorizeResult authThridUserResult = thirdUserAuthorizeManager.authorizeCallback(code, state, PassportSourceEnum.H5.value());
			if(authThridUserResult.isSuccess()){// 认证通过, 拿到用户

				User user = authThridUserResult.getUser();
				if("0".equals(user.getEnabled())){
					return goThirdLoginResultPage(0, null, null, "用户已被冻结,请联系平台客服!");
				}

				AppToken token= appLoginUtil.loginToken(user.getId(), user.getUserName(), null, "H5", "1.0", IPHelper.getIpAddr(request), authThridUserResult.getPassport().getOpenId());
				if(AppUtils.isBlank(token)){
					return goThirdLoginResultPage(0, null, null, "登录失败,请联系平台客服!");
				}

				AppTokenDto tokenDto = token.toDto();
				return goThirdLoginResultPage(1, tokenDto, null, null);
			}

			if(authThridUserResult.isNeedBindAccount()){// 认证通过, 需要绑定账号
				return goThirdLoginResultPage(2, null, authThridUserResult.getPassportIdKey(), "需要绑定账号!");
			}

			return goThirdLoginResultPage(0, null, null, "登录失败,请联系平台客服!");
		} catch (Exception e) {
			e.printStackTrace();
			return goThirdLoginResultPage(0, null, null, "登录失败,请联系平台客服!");
		}
	}

	private String goThirdLoginResultPage(int status, AppTokenDto tokenDto, String passportIdKey, String msg){

		String vueDomainName = propertiesUtil.getVueDomainName();
		try{
			StringBuilder sb = new StringBuilder("redirect:" + vueDomainName + "/accountModules/thirdLoginResult/thirdLoginResult");
			sb.append("?status=").append(status);
			if(null != tokenDto){
				sb.append("&token=").append(URLEncoder.encode(JSONObject.toJSONString(tokenDto), "utf-8"));
			}

			if(null != passportIdKey){
				sb.append("&passportIdKey=").append(passportIdKey);
			}

			if(null != msg){
				sb.append("&msg=").append(URLEncoder.encode(msg, "utf-8"));
			}
			log.info("********* goThirdLoginResultPage {}",sb.toString());
			return sb.toString();
		} catch(Exception e){
			e.printStackTrace();
			StringBuilder sb = new StringBuilder("redirect:" + vueDomainName + "/accountModules/thirdLoginResult/thirdLoginResult");
			sb.append("?status=").append(0);
			log.info("********* goThirdLoginResultPage {}",sb.toString());
			return sb.toString();
		}

	}

	/**
	 * app授权成功回调
	 * @param request
	 * @param response
	 * @param type 类型
	 * @return
	 */
	@ApiOperation(value = "第三方用户授权成功回调, 如果可以直接登录成功则返回的status为1, result为token信息; 如果需要绑定手机号, 则返回status为2, result为passportId, 然后带上passportId跳转到登录页面.", httpMethod = "POST", notes = "第三方用户授权成功回调, 用于原生APP的第三方登录实现",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="path", name = "type", value = "第三方登录类型, weixin: 微信, qq: QQ, sinaweibo: 新浪微博", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "platform", value = "设备平台类型, H5: 手机网页, ANDROID: 安卓APP, IOS: 苹果APP, MP: 小程序", required = true, dataType = "string")
	})
	@PostMapping("/{type}/app/callback")
	@ResponseBody
	public ResultDto<Object> appAuthorizeCallback(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String type, AppThirdUserInfo thirdUserInfo ,
			@RequestParam(required = true) String platform) {

		try {

			ThirdUserAuthorizeManager thirdUserAuthorizeManager = ThirdUserAuthorizeManagerFactory.getIntance(type);

			ThirdUserAuthorizeResult authThridUserResult = thirdUserAuthorizeManager.appAuthorizeCallback(thirdUserInfo.convert2ThirdUserInfo());
			if(authThridUserResult.isSuccess()){//成功返回用户信息

				User user = authThridUserResult.getUser();
				if("0".equals(user.getEnabled())){
					return ResultDtoManager.fail(-1, "登录用户已被冻结,");
				}

				AppToken token= appLoginUtil.loginToken(user.getId(), user.getUserName(), null, platform, "1.0", IPHelper.getIpAddr(request), authThridUserResult.getPassport().getOpenId());
				if(AppUtils.isBlank(token)){
					return ResultDtoManager.fail(-1, "登录失败, 请联系平台客服!");
				}
				return ResultDtoManager.success(token.toDto());
			}

			if(authThridUserResult.isNeedBindAccount()){//只有passport, 需要用户绑定手机号
				return ResultDtoManager.fail(authThridUserResult.getPassportIdKey(), 2, "需要绑定账号");
			}

			return ResultDtoManager.fail(authThridUserResult.getMsg());
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDtoManager.fail("登录失败, 请联系平台客服!");
		}
	}

	/**
	 * 小程序登录
	 * @param code
	 * @param encryptedData
	 * @param iv
	 * @return
	 */
	@ApiOperation(value = "小程序登录", notes = "小程序登录, 如果可以直接登录成功则返回的status为1, result为token信息; 如果需要绑定手机号, 则返回status为2, result为passportId, 然后带上passportId跳转到登录页面.", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "code", paramType = "query", value = "用户登录凭证（有效期五分钟）小程序端调用login时获取的 code", required = true, dataType = "String"),
			@ApiImplicitParam(name = "encryptedData", paramType = "query", value = "包括敏感数据在内的完整用户信息的加密数据, 调用getUserInfo获得", required = false, dataType = "String"),
			@ApiImplicitParam(name = "iv", paramType = "query", value = "加密算法的初始向量, 调用getUserInfo获得", required = false, dataType = "String"),
			@ApiImplicitParam(name = "platform", paramType = "query", value = "设备平台类型, H5: 手机网页, ANDROID: 安卓APP, IOS: 苹果APP, MP: 小程序", required = true, dataType = "String")
	})
	@PostMapping(value = "/{type}/mp/callback")
	@ResponseBody
	public ResultDto<Object> mpAuthorizeCallback(HttpServletRequest request, @PathVariable String type,
			@RequestParam(required = true) String code, String encryptedData, String iv,String platform) {

		try {
			ThirdUserAuthorizeManager thirdUserAuthorizeManager = ThirdUserAuthorizeManagerFactory.getIntance(type);

			ThirdUserAuthorizeResult authThridUserResult = thirdUserAuthorizeManager.mpAuthorizeCallback(code, encryptedData, iv);
			if(authThridUserResult.isSuccess()){//成功返回用户信息

				User user = authThridUserResult.getUser();
				if("0".equals(user.getEnabled())){
					return ResultDtoManager.fail(-1, "登录用户已被冻结,");
				}

				AppToken token= appLoginUtil.loginToken(user.getId(), user.getUserName(), null, platform, "1.0", IPHelper.getIpAddr(request), authThridUserResult.getPassport().getOpenId());
				if(AppUtils.isBlank(token)){
					return ResultDtoManager.fail(-1, "登录失败, 请联系平台客服!");
				}
				return ResultDtoManager.success(token.toDto());
			}

			if(authThridUserResult.isNeedBindAccount()){//只有passport, 需要用户绑定手机号
				return ResultDtoManager.fail(authThridUserResult.getPassportIdKey(), 2, "需要绑定账号");
			}

			return ResultDtoManager.fail(-1, "登录失败, 请联系平台客服!");
		} catch (Exception e) {
			log.error("用户登录异常!", e);
			return ResultDtoManager.fail(-1, "登录失败, 请联系平台客服!");
		}
	}
}
