package com.legendshop.app.service;

import com.legendshop.model.dto.app.InvoiceDto;
import com.legendshop.model.dto.app.AppPageSupport;

public interface AppInvoiceService {

	AppPageSupport<InvoiceDto> getInvoice(String curPageNO,String userName);

	void delById(Long id);

	/**
	 * 根据发票类型获取发票列表
	 * @param userName 用户名
	 * @param invoiceType 发票类型
	 * @param curPageNo 当前页码
	 * @return
	 */
    AppPageSupport<InvoiceDto> queryInvoice(String userName, Integer invoiceType, String curPageNo);

	/**
	 * 获取发票信息
	 * @param userId 用户ID
	 * @param invoiceId 发票ID
	 * @return
	 */
    InvoiceDto getInvoiceDto(String userId, Long invoiceId);
}
