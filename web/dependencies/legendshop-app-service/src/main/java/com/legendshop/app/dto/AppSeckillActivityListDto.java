package com.legendshop.app.dto ;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app秒杀列表数据Dto
 */
@ApiModel(value="app秒杀列表数据Dto")
public class AppSeckillActivityListDto {

	/** 秒杀活动Id */
	@ApiModelProperty(value="秒杀活动Id")
	private Long id; 
	
	/** 秒杀店铺用户Id */
	@ApiModelProperty(value="秒杀店铺用户Id")
	private String userId; 
	
	/** 秒杀店铺Id */
	@ApiModelProperty(value="秒杀店铺Id")
	private Long shopId; 
		
	/** 秒杀活动店铺名称 */
	@ApiModelProperty(value="秒杀活动店铺名称")
	private String shopName; 
		
	/** 活动名称 */
	@ApiModelProperty(value="活动名称")
	private String seckillTitle; 
	
	/** 开始时间 */
	@ApiModelProperty(value="开始时间")
	private Date startTime; 
		
	/** 结束时间 */
	@ApiModelProperty(value="结束时间")
	private Date endTime; 
		
	/** 秒杀活动图片 */
	@ApiModelProperty(value="秒杀活动图片")
	private String seckillPic; 
	
	/** 秒杀限时价 */
	@ApiModelProperty(value="秒杀限时价")
	private java.math.BigDecimal seckillLowPrice; 
		
	/** 秒杀总库存 */
	@ApiModelProperty(value="秒杀总库存")
	private Long stockTotal;
	
	/** 商品原价 */
	@ApiModelProperty(value="商品原价")
	private double price;
	
	/** 活动状态 */
	@ApiModelProperty(value="活动状态：未通过为-2, 审核中为-1, 上线为1, 下线为0, 秒杀结束为2")
	private Long status;
	
	/** 购买状态：1:进行中,-1:已结束,0:未开始,2:已售完 */
	@ApiModelProperty(value="购买状态：1:进行中, -1:已结束, 0:未开始, 2:已售完 4已终止")
	private Integer buyStatus;
	
	/** 秒杀活动描述 */
	@ApiModelProperty(value="秒杀活动描述")
	private String seckillDesc; 
	
	/** 秒杀活动摘要 */
	@ApiModelProperty(value="秒杀活动摘要")
	private String seckillBrief; 
	
	/** 所有剩余的秒杀sku库存 */
	@ApiModelProperty(value="所有秒杀的sku库存")
	private Long allSeckillStock;
	
	/** 所有参与秒杀活动的sku库存 */
	@ApiModelProperty(value="所有参与秒杀活动的sku库存")
	private Long allActivityStock;
	
	/** 秒杀库存百分比 */
	@ApiModelProperty(value="秒杀库存百分比")
	private Double seckillRate;
	
	public AppSeckillActivityListDto() {
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getSeckillTitle() {
		return seckillTitle;
	}

	public void setSeckillTitle(String seckillTitle) {
		this.seckillTitle = seckillTitle;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getSeckillPic() {
		return seckillPic;
	}

	public void setSeckillPic(String seckillPic) {
		this.seckillPic = seckillPic;
	}

	public java.math.BigDecimal getSeckillLowPrice() {
		return seckillLowPrice;
	}

	public void setSeckillLowPrice(java.math.BigDecimal seckillLowPrice) {
		this.seckillLowPrice = seckillLowPrice;
	}

	public Long getStockTotal() {
		return stockTotal;
	}

	public void setStockTotal(Long stockTotal) {
		this.stockTotal = stockTotal;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getSeckillDesc() {
		return seckillDesc;
	}

	public void setSeckillDesc(String seckillDesc) {
		this.seckillDesc = seckillDesc;
	}

	public String getSeckillBrief() {
		return seckillBrief;
	}

	public void setSeckillBrief(String seckillBrief) {
		this.seckillBrief = seckillBrief;
	}

	public Long getAllSeckillStock() {
		return allSeckillStock;
	}

	public void setAllSeckillStock(Long allSeckillStock) {
		this.allSeckillStock = allSeckillStock;
	}

	public Long getAllActivityStock() {
		return allActivityStock;
	}

	public void setAllActivityStock(Long allActivityStock) {
		this.allActivityStock = allActivityStock;
	}

	public Double getSeckillRate() {
		return seckillRate;
	}

	public void setSeckillRate(Double seckillRate) {
		this.seckillRate = seckillRate;
	}

	public Integer getBuyStatus() {
		return buyStatus;
	}

	public void setBuyStatus(Integer buyStatus) {
		this.buyStatus = buyStatus;
	}
	
} 
