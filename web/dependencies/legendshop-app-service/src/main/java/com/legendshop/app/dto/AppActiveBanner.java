package com.legendshop.app.dto ;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 广告 ActiveBanner
 */
@ApiModel(value="拼团活动Banner图")
public class AppActiveBanner  implements Serializable{

	private static final long serialVersionUID = 1L;

	/** 主键 */
	@ApiModelProperty("图片ID, 唯一标识")
	private Long id; 

	/** 图片路径 */
	@ApiModelProperty("图片路径")
	private String pic; 

	/** 图片链接 */
	@ApiModelProperty("跳转链接, http或https开头以a标签跳转, #代表正在开发, 其余的以路由的方式跳转")
	private String url; 

	public AppActiveBanner() {
	}

	public Long  getId(){
		return id;
	} 

	public void setId(Long id){
		this.id = id;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String  getUrl(){
		return url;
	} 

	public void setUrl(String url){
		this.url = url;
	}
} 
