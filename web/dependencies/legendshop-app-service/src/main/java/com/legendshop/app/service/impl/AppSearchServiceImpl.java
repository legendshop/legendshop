package com.legendshop.app.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.AppHotsearchDto;
import com.legendshop.app.dto.AppProdDto;
import com.legendshop.app.dto.AppShopDto;
import com.legendshop.app.service.AppSearchService;
import com.legendshop.util.DateUtils;
import com.legendshop.business.dao.ProductDao;
import com.legendshop.business.dao.ShopDetailDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.ProdOrderWayEnum;
import com.legendshop.model.constant.ShopOrderWayEnum;
import com.legendshop.model.constant.SortTypeEnum;
import com.legendshop.model.dto.TreeNode;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.search.AppProdSearchParms;
import com.legendshop.model.dto.search.AppShopSearchParams;
import com.legendshop.model.entity.Hotsearch;
import com.legendshop.spi.service.CategoryManagerService;
import com.legendshop.spi.service.HotsearchService;
import com.legendshop.util.AppUtils;

@Service("appSearchService")
public class AppSearchServiceImpl implements AppSearchService {
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(AppSearchServiceImpl.class);

	/** The product dao. */
	@Autowired
	protected ProductDao productDao;
	
	@Autowired
	private CategoryManagerService  categoryManagerService;
	
	@Autowired
	protected ShopDetailDao shopDetailDao;
	
	@Autowired
	private HotsearchService hotsearchService;
	
	@Override
	public AppPageSupport<AppProdDto> appProdList(AppProdSearchParms params) {
		
		String curPageNO = String.valueOf(params.getCurPageNO());
		
		SimpleSqlQuery query = new SimpleSqlQuery(AppProdDto.class, params.getPageSize(), curPageNO);
		QueryMap map = new QueryMap();
		
		if(AppUtils.isNotBlank(params.getCategoryId())){
			TreeNode treeNode = categoryManagerService.getTreeNodeById(params.getCategoryId());
			
			if(AppUtils.isBlank(treeNode)){
				map.put("categoryId", params.getCategoryId());
			}else{
				List<TreeNode> nodes = treeNode.getJuniors();
				if(AppUtils.isNotBlank(nodes)){
					StringBuffer ca=new StringBuffer();
					ca.append(params.getCategoryId() + ",");
					for (int i = 0; i < nodes.size(); i++) {
						ca.append(nodes.get(i).getSelfId()).append(",");
					}
					ca.setLength(ca.length() - 1);
					map.put("categoryId", ca.toString());
			    }else{
					map.put("categoryId", params.getCategoryId());
			    }
		    }
		}
		
		//商品所属店铺类型:0专营店，1旗舰店，2自营店
		map.like("shopType", params.getShopType());
		
		//关键字搜索条件
		String keyword = params.getKeyword();
    	map.like("prodName", keyword);
    	map.like("keyword", keyword);
    	map.like("brief", keyword);
		
    	//其他条件
		if (AppUtils.isNotBlank(params.getIsHasProd()) && params.getIsHasProd()) {
			map.put("hasProd", 0); // prod.actualStocks > $hasProd$ hasProd = 0
		}
		map.put("startPrice", params.getStartPrice());
		map.put("endPrice", params.getEndPrice());
		map.put("brandId", params.getBrandId());
		
		//处理排序
		String orderWay = params.getOrderWay();
		String orderType = params.getOrderType();
		if(!SortTypeEnum.instance(orderType)){
			orderType = SortTypeEnum.DESC.value();
		}
		if(ProdOrderWayEnum.recDate.value().equals(orderWay)){//综合
			map.put("order", "prod.buys " + orderType + ", prod.views " + orderType + ", prod.comments " + orderType);
		}else if(ProdOrderWayEnum.BUYS.value().equals(orderWay)){//销量
			map.put("order", "prod.buys " + orderType);
		}else if(ProdOrderWayEnum.CASH.value().equals(orderWay)){//价格
			map.put("order", "prod.cash " + orderType);
		}else if(ProdOrderWayEnum.Comments.value().equals(orderWay)){//好评
			map.put("order", "prod.comments " + orderType);
		}else{
			map.put("order", "prod.buys " + orderType + ", prod.views " + orderType + ", prod.cash " + orderType + ", prod.comments " + orderType);
		}

		String querySQL = ConfigCode.getInstance().getCode("app.search.getAppSearchProd", map);
		String queryCountSQL = ConfigCode.getInstance().getCode("app.search.getAppSearchProdCount", map);
		query.setQueryString(querySQL);
		query.setAllCountString(queryCountSQL);
		map.remove("order");
		map.remove("categoryId");
		query.setParam(map.toArray());
		
		PageSupport ps = productDao.querySimplePage(query);
		/*AppPageSupport<AppProdDto> dto = PageUtils.convertPageSupport(ps, AppProdDto.class);
		return dto;*/
		AppPageSupport<AppProdDto> pageSupportDto = convertToAppPageSupport(ps);
		return pageSupportDto;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public AppPageSupport<AppShopDto> appSearchShopList(AppShopSearchParams params) {
		log.debug("search by keyword {}", params.getKeyword());
		
		Integer pageSize = params.getPageSize();
		String curPageNo = String.valueOf(params.getCurPageNO());
		SimpleSqlQuery query = new SimpleSqlQuery(AppShopDto.class, pageSize, curPageNo);
		QueryMap map = new QueryMap();
		
		Date endTime = DateUtils.getIntegralEndTime(new Date());
		Date startTime = DateUtils.getIntegralStartTime(DateUtils.rollDay(endTime, -30));
		SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		map.put("endDate", "'" + sd.format(endTime) + "'");
		map.put("startDate", "'" + sd.format(startTime) + "'");
		
		//关键字搜索条件
		String keyword = params.getKeyword();
    	map.like("siteName", keyword);
    	map.like("briefDesc", keyword);
		
		//处理排序
		String orderWay = params.getOrderWay();
		String orderType = params.getOrderType();
		if(!SortTypeEnum.instance(orderType)){
			orderType = SortTypeEnum.DESC.value();
		}
		if(ShopOrderWayEnum.SYNTHESIZE.value().equals(orderWay)){//综合
			map.put("order", "sales " + orderType + ", sd.comm_num " + orderType);
		}else if(ShopOrderWayEnum.SALES.value().equals(orderWay)){//月销量
			map.put("order", "sales " + orderType);
		}else if(ShopOrderWayEnum.COMMENTS.value().equals(orderWay)){//好评数
			map.put("order", "sd.comm_num " + orderType);
		}else{
			map.put("order", "sales " + orderType + ", sd.comm_num " + orderType);
		}

		String querySQL = ConfigCode.getInstance().getCode("app.search.getAppSearchShop", map);
		String queryCountSQL = ConfigCode.getInstance().getCode("app.search.getAppSearchShopCount", map);
		query.setQueryString(querySQL);
		query.setAllCountString(queryCountSQL);
		
		map.remove("endDate");
		map.remove("startDate");
		map.remove("order");
		query.setParam(map.toArray());
		
		PageSupport ps = shopDetailDao.querySimplePage(query);
		/*AppPageSupport<AppShopDto> result = PageUtils.convertPageSupport(ps, AppShopDto.class);
		return result;*/
		AppPageSupport<AppShopDto> pageSupportDto = convertToAppPageSupport(ps);
		return pageSupportDto; 
	}

	@Override
	public List<AppHotsearchDto> getHotsearch() {
		
		List<Hotsearch> hotsearchList = hotsearchService.getHotsearch();
		
		List<AppHotsearchDto> appHotsearchs = new ArrayList<AppHotsearchDto>();
		
		if(AppUtils.isNotBlank(hotsearchList)){
			for(Hotsearch hotsearch : hotsearchList){
				AppHotsearchDto appHotsearch = this.convertToAppHotsearch(hotsearch);
				appHotsearchs.add(appHotsearch);
			}
		}
		
		return appHotsearchs;
	}
	
	private AppHotsearchDto convertToAppHotsearch(Hotsearch hotsearch){
	  AppHotsearchDto appHotsearch = new AppHotsearchDto();
	  appHotsearch.setDate(hotsearch.getDate());
	  appHotsearch.setMsg(hotsearch.getMsg());
	  appHotsearch.setId(hotsearch.getId());
	  appHotsearch.setTitle(hotsearch.getTitle());
	  appHotsearch.setCategoryName(hotsearch.getCategoryName());
	  appHotsearch.setSeq(hotsearch.getSeq());
	  appHotsearch.setStatus(hotsearch.getStatus());
	  return appHotsearch;
	}
	
	/**
	 * 转换成AppPageSupport输出
	 */
	@SuppressWarnings("unused")
	private <T> AppPageSupport<T> convertToAppPageSupport(PageSupport<T> ps) {
		AppPageSupport<T> result = new AppPageSupport<T>();
		result.setCurrPage(ps.getCurPageNO());
		result.setPageCount(ps.getPageCount());
		result.setPageSize(ps.getPageSize());
		result.setResultList(ps.getResultList());
		result.setTotal(ps.getTotal());
		return result;
	}
}
