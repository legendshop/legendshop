/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.app.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The Class BrandDto.
 */
@ApiModel(value="品牌Dto") 
public class BrandDto implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7261593755649051584L;

	/** 品牌ID *. */
	@ApiModelProperty(value="品牌ID")  
	private Long brandId;
	
	/** 品牌名称  *. */
	@ApiModelProperty(value="品牌名称")  
	private String brandName;
	
	/** 品牌状态 *. */
	@ApiModelProperty(value="品牌状态")  
	protected Integer status;
	
	/** 品牌排序 *. */
	@ApiModelProperty(value="品牌排序 ")  
	private Integer seq;
	
	/** 移动端品牌图片 *. */
	@ApiModelProperty(value="移动端品牌图片")
	private String brandPicMobile;

	/**
	 * Gets the brand id.
	 *
	 * @return the brand id
	 */
	public Long getBrandId() {
		return brandId;
	}

	/**
	 * Sets the brand id.
	 *
	 * @param brandId the brand id
	 */
	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	/**
	 * Gets the brand name.
	 *
	 * @return the brand name
	 */
	public String getBrandName() {
		return brandName;
	}

	/**
	 * Sets the brand name.
	 *
	 * @param brandName the brand name
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * Gets the seq.
	 *
	 * @return the seq
	 */
	public Integer getSeq() {
		return seq;
	}

	/**
	 * Sets the seq.
	 *
	 * @param seq the seq
	 */
	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public String getBrandPicMobile() {
		return brandPicMobile;
	}

	public void setBrandPicMobile(String brandPicMobile) {
		this.brandPicMobile = brandPicMobile;
	}
}
