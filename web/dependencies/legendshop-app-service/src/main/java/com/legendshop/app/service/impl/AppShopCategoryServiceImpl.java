package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.AppShopProdCategoryDto;
import com.legendshop.app.service.AppShopCategoryService;
import com.legendshop.model.entity.ShopCategory;
import com.legendshop.spi.service.ShopCategoryService;
import com.legendshop.util.AppUtils;

/**
 * app店铺商品分类service实现
 */
@Service("appShopCategoryService")
public class AppShopCategoryServiceImpl implements AppShopCategoryService {

	@Autowired
	private ShopCategoryService shopCategoryService;

	/**
	 * 获取店铺商品分类
	 */
	@Override
	public List<AppShopProdCategoryDto> getFirstShopCategory(Long shopId) {
		
		List<ShopCategory> shopCategoryList =  shopCategoryService.getFirstShopCategory(shopId);
		return convert(shopCategoryList);
	}
	
	private List<AppShopProdCategoryDto> convert(List<ShopCategory> from){
		
		List<AppShopProdCategoryDto> toList = new ArrayList<AppShopProdCategoryDto>();
		
		for (ShopCategory shopCategory : from) {
			
			if (AppUtils.isNotBlank(shopCategory)) {
				
				AppShopProdCategoryDto appShopProdCategoryDto = convertToAppShopProdCategoryDto(shopCategory);
				toList.add(appShopProdCategoryDto);
			}
		}
		return toList;
	}
	

	private AppShopProdCategoryDto convertToAppShopProdCategoryDto(ShopCategory shopCategory){
	  AppShopProdCategoryDto appShopProdCategoryDto = new AppShopProdCategoryDto();
	  appShopProdCategoryDto.setSubCatList(shopCategory.getSubCatList());
	  appShopProdCategoryDto.setNextCatId(shopCategory.getNextCatId());
	  appShopProdCategoryDto.setName(shopCategory.getName());
	  appShopProdCategoryDto.setSubCatId(shopCategory.getSubCatId());
	  appShopProdCategoryDto.setId(shopCategory.getId());
	  appShopProdCategoryDto.setPic(shopCategory.getPic());
	  appShopProdCategoryDto.setShopId(shopCategory.getShopId());
	  appShopProdCategoryDto.setParentId(shopCategory.getParentId());
	  appShopProdCategoryDto.setSeq(shopCategory.getSeq());
	  appShopProdCategoryDto.setStatus(shopCategory.getStatus());
	  return appShopProdCategoryDto;
	}

}
