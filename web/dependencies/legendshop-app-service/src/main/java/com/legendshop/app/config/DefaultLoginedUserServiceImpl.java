/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.config;

import com.legendshop.model.dto.app.AppTokenDto;
import com.legendshop.model.dto.app.UserTokenContext;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.spi.service.LoginedUserService;

/**
 * 获取 默认的登录用户信息
 *
 */
public class DefaultLoginedUserServiceImpl implements LoginedUserService {

	/**
	 * 获取登录用户的信息
	 */
	@Override
	public LoginedUserInfo getUser() {
		
		AppTokenDto appTokenDto = UserTokenContext.getToken();
	
		if (appTokenDto == null) {
			return null;
		}
		
		LoginedUserInfo user = new LoginedUserInfo(appTokenDto.getUserId(), appTokenDto.getUserName(), 
				appTokenDto.getShopId(), appTokenDto.getAccessToken(), appTokenDto.getOpenId());
		
		return user;
	}

}