/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.app.service.impl;



import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.legendshop.model.dto.AppSecKillActivityQueryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.AppActiveBanner;
import com.legendshop.app.dto.AppSeckillActivityListDto;
import com.legendshop.app.dto.AppSeckillDto;
import com.legendshop.app.dto.AppSeckillSuccessDto;
import com.legendshop.app.service.AppSeckillActivityService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.business.dao.SeckillActivityDao;
import com.legendshop.business.dao.SeckillProdDao;
import com.legendshop.business.dao.SkuDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.ActiveBannerTypeEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.seckill.SeckillDto;
import com.legendshop.model.dto.seckill.SeckillSuccessRecord;
import com.legendshop.model.dto.seckill.SuccessProdDto;
import com.legendshop.model.entity.ActiveBanner;
import com.legendshop.model.entity.SeckillActivity;
import com.legendshop.model.entity.SeckillProd;
import com.legendshop.model.entity.SeckillSuccess;
import com.legendshop.model.entity.Sku;
import com.legendshop.spi.service.ActiveBannerService;
import com.legendshop.spi.service.SeckillService;
import com.legendshop.spi.util.SeckillCachedhandle;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * app秒杀活动ServiceImpl.
 */
@Service("appSeckillActivityService")
public class AppSeckillActivityServiceImpl implements AppSeckillActivityService {
	
	@Autowired
	private SeckillService seckillService;

	@Autowired
    private SeckillActivityDao seckillActivityDao;

	@Autowired
    private SeckillProdDao seckillProdDao;
	
	/** 秒杀缓存处理 */
	@Autowired
	private SeckillCachedhandle seckillCachedhandle;
	
	@Autowired
	private ActiveBannerService activeBannerService;
	
	@Autowired
	private SkuDao skuDao;
	
    /**
     * 查找秒杀活动列表
     */
    @Override
    public AppPageSupport<AppSeckillActivityListDto> querySeckillActivityList(AppSecKillActivityQueryDTO queryDTO) {
    	PageSupport<SeckillActivity> ps = seckillActivityDao.queryAuctionList(queryDTO);
    	
    	return PageUtils.convertPageSupport(ps, form -> {

			List<AppSeckillActivityListDto> resultList = new ArrayList<>();

			//当前时间戳
			long nowTime = Calendar.getInstance().getTimeInMillis();
			for (SeckillActivity seckillActivity : form) {
				AppSeckillActivityListDto appSeckillActivityListDto = new AppSeckillActivityListDto();
				appSeckillActivityListDto.setId(seckillActivity.getId());
				appSeckillActivityListDto.setUserId(seckillActivity.getUserId());
				appSeckillActivityListDto.setShopId(seckillActivity.getShopId());
				appSeckillActivityListDto.setShopName(seckillActivity.getShopName());
				appSeckillActivityListDto.setStartTime(seckillActivity.getStartTime());
				appSeckillActivityListDto.setEndTime(seckillActivity.getEndTime());
				appSeckillActivityListDto.setSeckillPic(seckillActivity.getSeckillPic());
				appSeckillActivityListDto.setSeckillLowPrice(seckillActivity.getSeckillLowPrice());
				appSeckillActivityListDto.setStockTotal(seckillActivity.getStockTotal());
				appSeckillActivityListDto.setPrice(seckillActivity.getPrice());
				appSeckillActivityListDto.setStatus(seckillActivity.getStatus());
				appSeckillActivityListDto.setSeckillTitle(seckillActivity.getSeckillTitle());
				appSeckillActivityListDto.setSeckillBrief(seckillActivity.getSeckillBrief());

				//获取每个秒杀活动的秒杀比例
				List<SeckillProd> seckillProdList = seckillProdDao.getSeckillProdBySId(seckillActivity.getId());
				if(AppUtils.isNotBlank(seckillProdList)){
					Sku sku = skuDao.getSkuById(seckillProdList.get(0).getSkuId());
					// 找不到sku，没有价格就直接跳过
					if (AppUtils.isBlank(sku)) {
						continue;
					}
					appSeckillActivityListDto.setPrice(sku.getPrice());
				}

				long allSeckillStock = 0L;
				long allActivityStock = 0L;
				for (SeckillProd seckillProd : seckillProdList) {
					allSeckillStock = allSeckillStock + seckillProd.getSeckillStock();
					allActivityStock = allActivityStock + seckillProd.getActivityStock();
				}
				long count =  allActivityStock - allSeckillStock;
				double seckillRate = Arith.div(count, allActivityStock);
				appSeckillActivityListDto.setAllSeckillStock(allSeckillStock);
				appSeckillActivityListDto.setAllActivityStock(allActivityStock);
				DecimalFormat df = new DecimalFormat("0.00");
				String format = df.format(seckillRate);
				appSeckillActivityListDto.setSeckillRate(Double.parseDouble(format)*100);

				//根据时间获取状态
				if(nowTime < seckillActivity.getStartTime().getTime()){ //未开始
					appSeckillActivityListDto.setBuyStatus(0);
				}else if(nowTime >= seckillActivity.getEndTime().getTime()){ //已结束
					appSeckillActivityListDto.setBuyStatus(-1);
				}else {
					if(allSeckillStock == 0){
						appSeckillActivityListDto.setBuyStatus(2); //已抢完
					}else if(seckillActivity.getStatus() == 4){

						appSeckillActivityListDto.setBuyStatus(4); //已终止
					}else {
						appSeckillActivityListDto.setBuyStatus(1); //进行中
					}
				}

				resultList.add(appSeckillActivityListDto);
			}
			return resultList;
		});
    }
    
    /**
	 * 查看秒杀商品详情
	 * @param ps
	 */
	private void getSeckillProdDetail(PageSupport ps) {
		List<Long> ids = new ArrayList<Long>();
		List<SeckillActivity> list = (List<SeckillActivity>) ps.getResultList();
		for (SeckillActivity seckillActivity : list) {
			if (AppUtils.isNotBlank(seckillActivity)) {
				ids.add(seckillActivity.getId());
			}
		}
		if (ids.size() > 0) {
			List<SeckillProd> prodList = seckillProdDao.querySeckillProdList(ids);
			if (prodList.size() > 0) {
				for (SeckillActivity seckillActivity : list) {
					for (SeckillProd seckillProd : prodList) {
						if (seckillProd.getSId().equals(seckillActivity.getId())) {
							Long stock = seckillActivity.getStockTotal() + seckillProd.getSeckillStock();
							seckillActivity.setStockTotal(stock);
						}
					}
				}
			}
		}
	}
	
    
	/**
	 * 查看秒杀商品详情
	 */
	@Override
	public AppSeckillDto getSeckillDetail(Long seckillId) {
		SeckillDto seckill = seckillService.getSeckillDetail(seckillId);
		if (seckill == null || seckill.getSecStatus().intValue() == 0 || seckill.getSecStatus().intValue() == -2) { // 下线或者审核中
			return null;
		}
		seckillCachedhandle.addSeckill(seckill); // 存取缓存
		
		//转换app秒杀列表数据Dto
		AppSeckillDto appSeckillDto = convertToAppSeckillDto(seckill);
		return appSeckillDto;
	}
	
	
	/**
	 * 转换app秒杀列表数据Dto
	 * @param seckill
	 * @return
	 */
	private AppSeckillDto convertToAppSeckillDto(SeckillDto seckill){
		AppSeckillDto appSeckillActivityListDto = new AppSeckillDto();
		appSeckillActivityListDto.setId(seckill.getSid());
		appSeckillActivityListDto.setUserId(seckill.getUserId());
		appSeckillActivityListDto.setShopId(seckill.getShopId());
		appSeckillActivityListDto.setShopName(seckill.getShopName());
		appSeckillActivityListDto.setStartTime(seckill.getStartTime());
		appSeckillActivityListDto.setEndTime(seckill.getEndTime());
		appSeckillActivityListDto.setSeckillTitle(seckill.getSeckillTitle());
		appSeckillActivityListDto.setSeckillDesc(seckill.getSecDesc());
		appSeckillActivityListDto.setSeckillBrief(seckill.getSecBrief());
		appSeckillActivityListDto.setStatus(seckill.getSecStatus());
		appSeckillActivityListDto.setProdDto(seckill.getProdDto());
		//当前时间戳
		long nowTime = Calendar.getInstance().getTimeInMillis();
		//根据时间获取状态
		if(nowTime < seckill.getStartTime().getTime()){ //未开始
			appSeckillActivityListDto.setBuyStatus(0);
		}else if(nowTime >= seckill.getEndTime().getTime()){ //已结束
			appSeckillActivityListDto.setBuyStatus(-1);
		}else {
			appSeckillActivityListDto.setBuyStatus(1); //进行中
		}
		return appSeckillActivityListDto;
	}
    

	@Override
	public AppSeckillSuccessDto getSeckillSuccessInfo(SeckillSuccess seckillSuccess) {
		SuccessProdDto successProdDto=seckillService.findSuccessProdDto(seckillSuccess);
		AppSeckillSuccessDto appSeckillSuccessDto = new AppSeckillSuccessDto();
		appSeckillSuccessDto.setId(successProdDto.getSucessId());
		appSeckillSuccessDto.setSeckillId(successProdDto.getSeckillId());
		appSeckillSuccessDto.setSkuId(successProdDto.getSkuId());
		appSeckillSuccessDto.setProdId(successProdDto.getProdId());
		appSeckillSuccessDto.setSeckillNum(successProdDto.getSeckillNum());
		appSeckillSuccessDto.setSeckillPrice(successProdDto.getSeckillPrice());
		appSeckillSuccessDto.setStatus(successProdDto.getStatus());
		appSeckillSuccessDto.setPrice(successProdDto.getPrice());
		appSeckillSuccessDto.setPic(successProdDto.getPic());
		appSeckillSuccessDto.setProdName(successProdDto.getName());
		//计算已经秒杀百分比
		double res = Arith.sub(successProdDto.getActivityStock(), successProdDto.getSeckillStock());
		double seckillRate = Arith.div(res, successProdDto.getActivityStock(), 2);
		appSeckillSuccessDto.setSeckillRate(seckillRate);
		return appSeckillSuccessDto;
	}
	
	
	@Override
	public AppSeckillSuccessDto getSeckillSuccessInfo(String userId, Long seckillId, Long skuId) {
		SuccessProdDto successProdDto = seckillProdDao.getSeckillSuccessInfo(userId, seckillId, skuId);
		AppSeckillSuccessDto appSeckillSuccessDto = new AppSeckillSuccessDto();
		appSeckillSuccessDto.setId(successProdDto.getSucessId());
		appSeckillSuccessDto.setSeckillId(successProdDto.getSeckillId());
		appSeckillSuccessDto.setSkuId(successProdDto.getSkuId());
		appSeckillSuccessDto.setProdId(successProdDto.getProdId());
		appSeckillSuccessDto.setSeckillNum(successProdDto.getSeckillNum());
		appSeckillSuccessDto.setSeckillPrice(successProdDto.getSeckillPrice());
		appSeckillSuccessDto.setStatus(successProdDto.getStatus());
		appSeckillSuccessDto.setPrice(successProdDto.getPrice());
		appSeckillSuccessDto.setPic(successProdDto.getPic());
		appSeckillSuccessDto.setProdName(successProdDto.getName());
		//计算已经秒杀百分比
		double res = Arith.sub(successProdDto.getActivityStock(), successProdDto.getSeckillStock());
		double seckillRate = Arith.div(res, successProdDto.getActivityStock(), 2);
		appSeckillSuccessDto.setSeckillRate(seckillRate);
		return appSeckillSuccessDto;
	}

	
	@Override
	public AppPageSupport<SeckillSuccessRecord> queryUserSeckillRecord(String curPageNO, String userId) {
		PageSupport<SeckillSuccessRecord> result = seckillService.queryUserSeckillRecord(curPageNO, userId);
		return convertToAppPageSupport(result);
	}
	
	
	private <T> AppPageSupport<T> convertToAppPageSupport(PageSupport<T> ps) {
		AppPageSupport<T> result = new AppPageSupport<T>();
		result.setCurrPage(ps.getCurPageNO());
		result.setPageCount(ps.getPageCount());
		result.setPageSize(ps.getPageSize());
		result.setResultList(ps.getResultList());
		result.setTotal(ps.getTotal());
		return result;
	}

	/**
	 * 获取秒杀banner图
	 */
	@Override
	public List<AppActiveBanner> getActiveBanners() {
		
		List<ActiveBanner> bannerList = activeBannerService.getBannerList(ActiveBannerTypeEnum.SECKILL_BANNER.value());
		
		List<AppActiveBanner> resultList = new ArrayList<AppActiveBanner>();
		for(ActiveBanner activeBanner : bannerList){
			AppActiveBanner appActiveBanner = new AppActiveBanner();
			appActiveBanner.setId(activeBanner.getId());
			appActiveBanner.setPic(activeBanner.getImageFile());
			appActiveBanner.setUrl(activeBanner.getUrl());
			
			resultList.add(appActiveBanner);
		}
		return resultList;
	}
}
