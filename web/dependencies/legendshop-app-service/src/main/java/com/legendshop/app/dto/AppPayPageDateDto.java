/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;

import java.util.List;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 付款页面数据对象
 */
@ApiModel(value="付款页面数据对象") 
public class AppPayPageDateDto {

   /** 可用余额 **/
	@ApiModelProperty(value="可用余额")  
   private Double availablePredeposit;
   
	/**订单自动取消的时间 **/
	@ApiModelProperty(value="订单自动取消的时间")  
	private int cancelMins;
	
	/** 支付方式列表 **/
	@ApiModelProperty(value="支付方式列表")  
	private List<PayTypeDto> PayTypeDtoList;

	public Double getAvailablePredeposit() {
		return availablePredeposit;
	}

	public void setAvailablePredeposit(Double availablePredeposit) {
		this.availablePredeposit = availablePredeposit;
	}

	public int getCancelMins() {
		return cancelMins;
	}

	public void setCancelMins(int cancelMins) {
		this.cancelMins = cancelMins;
	}

	public List<PayTypeDto> getPayTypeDtoList() {
		return PayTypeDtoList;
	}

	public void setPayTypeDtoList(List<PayTypeDto> payTypeDtoList) {
		PayTypeDtoList = payTypeDtoList;
	}
   
}
