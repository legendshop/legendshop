package com.legendshop.app.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.model.constant.Constants;

import io.swagger.annotations.Api;
import springfox.documentation.annotations.ApiIgnore;


@RestController
@Api(tags="测试",value="测试")
@ApiIgnore
public class AppTestController{
	
	@PostMapping(value = "/test")
	public String test() {
		return Constants.SUCCESS;
	}
	
	@PostMapping(value = "/test1")
	public String test1() {
		String url = "http://192.168.0.93:8080/submitSuccess?subSettlementSn=123456";
		return "redirect:" + url;
	}
}
