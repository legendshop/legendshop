/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;

import java.io.Serializable;
import java.util.Date;

import com.legendshop.dao.persistence.Transient;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="AppHotsearch热词搜索") 
public class AppHotsearchDto implements Serializable {


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1349792154724940069L;

	/** 热搜词唯一ID  */
	@ApiModelProperty(value="热搜词唯一ID")
	private Long id;

	/** 标题  */
	@ApiModelProperty(value="标题（该字段暂时没用）",hidden = true)
	private String title;
	
	/** 产品分类  */
	@ApiModelProperty(value="产品分类（该字段暂时没用",hidden = true)
	private Long sort;
	
	/** 分类名称  */
	@ApiModelProperty(value="分类名称（该字段暂时没用）",hidden = true)
	private String categoryName;

	/** 热搜内容  */
	@ApiModelProperty(value="热搜内容")
	private String msg;

	/** 录入时间  */
	@ApiModelProperty(value="录入时间（该字段暂时没用）",hidden = true)
	private Date date;

	/** 默认是1，表示正常状态,0为下线状态  */
	@ApiModelProperty(value="默认是1，表示正常状态,0为下线状态（该字段暂时没用）",hidden = true)
	protected Integer status;

	/** 顺序  */
	@ApiModelProperty(value="顺序（该字段暂时没用）",hidden = true)
	private Integer seq;
	

	/**
	 * default constructor.
	 */
	public AppHotsearchDto() {
	}

	public AppHotsearchDto(Long id, String title, String msg, Date date) {
		this.id = id;
		this.title = title;
		this.msg = msg;
		this.date = date;
	}

	public AppHotsearchDto(Long id, String title, String msg, Date date,
			String userId, String userName) {
		this.id = id;
		this.title = title;
		this.msg = msg;
		this.date = date;

	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMsg() {
		return this.msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Long getSort() {
		return sort;
	}

	public void setSort(Long sort) {
		this.sort = sort;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	@Transient
	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}