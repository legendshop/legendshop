package com.legendshop.app.service.impl;

import java.util.Date;

import com.legendshop.model.constant.AppTokenTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.AppTokenService;
import com.legendshop.base.event.LoginEvent;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.framework.event.EventHome;
import com.legendshop.model.constant.LoginUserTypeEnum;
import com.legendshop.model.constant.VisitSourceEnum;
import com.legendshop.model.dto.LoginSuccess;
import com.legendshop.model.entity.AppToken;
import com.legendshop.processor.LoginHistoryProcessor;
import com.legendshop.processor.LoginSendIntegralProcessor;
import com.legendshop.util.AppUtils;
/**
 * Applogin工具类
 *
 */
@Service("appLoginUtil")
public class AppLoginUtil {

	@Autowired
    private AppTokenService appTokenService;
	
	@Autowired
    private LoginHistoryProcessor loginHistoryProcessor;
	
	@Autowired
	private LoginSendIntegralProcessor loginSendIntegralProcessor;

    /**
     * loginToken 登录
     *
     * @param userName
     * @return
     */
    public AppToken loginToken(String userId, String userName, String deviceId, String platform, String verId, String ip, String openId) {
    	
        AppToken appToken = appTokenService.getAppToken(userId,platform,AppTokenTypeEnum.USER.value());
        if (appToken != null) {
        	appToken.setStartDate(new Date());//更新登录时间	

        	appToken.setVerId(verId);
        	appToken.setDeviceId(deviceId);
        	appToken.setPlatform(platform);
        	if(AppUtils.isNotBlank(openId)){
        		appToken.setOpenId(openId);
        	}
        	
            appTokenService.updateAppToken(appToken);
           
        } else {// 第一次登陆，保存并返回token
            Date now = new Date();
            appToken = new AppToken();

            // 获得随机码
            String accessToken = CommonServiceUtil.getRandomString(30);
            appToken.setAccessToken(accessToken);
            appToken.setDeviceId(deviceId);
            appToken.setPlatform(platform);
            appToken.setRecDate(now);

            // 获得随机码
            String securiyCode = CommonServiceUtil.getRandomString(8);
            appToken.setSecuriyCode(securiyCode);
            appToken.setStartDate(now);
            appToken.setUserId(userId);
            appToken.setUserName(userName);

            appToken.setValidateTime(1209600l);// 保持登陆2周
            appToken.setVerId(verId);
            appToken.setType(AppTokenTypeEnum.USER.value());
            appToken.setOpenId(openId);

            appTokenService.saveAppToken(appToken);
        }
		
        // 发布登录事件  Event事件发布不成功，注释掉，换成代理模式
       /* EventHome.publishEvent(new LoginEvent(new LoginSuccess(appToken.getUserId(), appToken.getUserName(), ip, LoginUserTypeEnum.USER.value(), VisitSourceEnum.APP.value())));*/
        
        //登录历史事件
        loginHistoryProcessor.process(new LoginSuccess(appToken.getUserId(), appToken.getUserName(), ip, LoginUserTypeEnum.USER.value(), VisitSourceEnum.APP.value()));
        //登录总积分事件
        loginSendIntegralProcessor.process(new LoginSuccess(appToken.getUserId(), appToken.getUserName(), ip, LoginUserTypeEnum.USER.value(), VisitSourceEnum.APP.value()));
		
		return appToken;
    }

}
