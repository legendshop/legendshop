package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.AppSubRefundReturnDto;
import com.legendshop.app.service.AppSubRefundReturnService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.orderreturn.SubRefundReturn;
import com.legendshop.spi.service.SubRefundReturnService;
import com.legendshop.util.AppUtils;

/**
 * app 退款，退货退款service实现
 */
@Service("appSubRefundReturnService")
public class AppSubRefundReturnServiceImpl implements AppSubRefundReturnService {

	@Autowired
	private SubRefundReturnService subRefundReturnService;

	/**
	 * 根据id 获取退款退货记录
	 */
	@Override
	public AppSubRefundReturnDto getSubRefundReturn(Long refundId) {
		
		SubRefundReturn subRefundReturn =  subRefundReturnService.getSubRefundReturn(refundId);
		if (AppUtils.isBlank(subRefundReturn)) {
			return null;
		}
		//转dto
		AppSubRefundReturnDto subRefundReturnDto = convertToAppSubRefundReturnDto(subRefundReturn);
		return subRefundReturnDto;
	}
	
	/**
	 * 获取用户退款退货记录列表
	 */
	@Override
	public AppPageSupport<AppSubRefundReturnDto> querySubRefundReturnList(String userId, String curPageNO,Integer status) {
		
		PageSupport<SubRefundReturn> ps =  subRefundReturnService.querySubRefundReturnList(userId,curPageNO,status);
		
		//转Dto
		return PageUtils.convertPageSupport(ps,
				form -> {
					if (AppUtils.isBlank(form)) {
						return null;
					}
					List<AppSubRefundReturnDto> toList = new ArrayList<>();
					for (SubRefundReturn subRefundReturn : form) {
						AppSubRefundReturnDto appSubRefundReturnDto = convertToAppSubRefundReturnDto(subRefundReturn);
						toList.add(appSubRefundReturnDto);
					}
					return toList;
				});
	}
	
	
	//转dto方法
	private AppSubRefundReturnDto convertToAppSubRefundReturnDto(SubRefundReturn subRefundReturn){
		  AppSubRefundReturnDto appSubRefundReturnDto = new AppSubRefundReturnDto();
		  appSubRefundReturnDto.setSubItemId(subRefundReturn.getSubItemId());
		  appSubRefundReturnDto.setApplyType(subRefundReturn.getApplyType());
		  appSubRefundReturnDto.setShipTime(subRefundReturn.getShipTime());
		  appSubRefundReturnDto.setSellerTime(subRefundReturn.getSellerTime());
		  appSubRefundReturnDto.setSubSettlementSn(subRefundReturn.getSubSettlementSn());
		  appSubRefundReturnDto.setProductName(subRefundReturn.getProductName());
		  appSubRefundReturnDto.setOrderDatetime(subRefundReturn.getOrderDatetime());
		  appSubRefundReturnDto.setIsPresell(subRefundReturn.getIsPresell());
		  appSubRefundReturnDto.setProductImage(subRefundReturn.getProductImage());
		  appSubRefundReturnDto.setGoodsState(subRefundReturn.getGoodsState());
		  appSubRefundReturnDto.setShopId(subRefundReturn.getShopId());
		  appSubRefundReturnDto.setApplyTime(subRefundReturn.getApplyTime());
		  appSubRefundReturnDto.setSkuId(subRefundReturn.getSkuId());
		  appSubRefundReturnDto.setReasonInfo(subRefundReturn.getReasonInfo());
		  appSubRefundReturnDto.setReceiveMessage(subRefundReturn.getReceiveMessage());
		  appSubRefundReturnDto.setOutRefundNo(subRefundReturn.getOutRefundNo());
		  appSubRefundReturnDto.setProductId(subRefundReturn.getProductId());
		  appSubRefundReturnDto.setDepositSubSettlementSn(subRefundReturn.getDepositSubSettlementSn());
		  appSubRefundReturnDto.setThirdPartyRefund(subRefundReturn.getThirdPartyRefund());
		  appSubRefundReturnDto.setSubNumber(subRefundReturn.getSubNumber());
		  appSubRefundReturnDto.setFlowTradeNo(subRefundReturn.getFlowTradeNo());
		  appSubRefundReturnDto.setReturnType(subRefundReturn.getReturnType());
		  appSubRefundReturnDto.setRefundSn(subRefundReturn.getRefundSn());
		  appSubRefundReturnDto.setDepositPayTypeId(subRefundReturn.getDepositPayTypeId());
		  appSubRefundReturnDto.setExpressNo(subRefundReturn.getExpressNo());
		  appSubRefundReturnDto.setShopName(subRefundReturn.getShopName());
		  appSubRefundReturnDto.setDepositPayTypeName(subRefundReturn.getDepositPayTypeName());
		  appSubRefundReturnDto.setIsHandleSuccess(subRefundReturn.getIsHandleSuccess());
		  appSubRefundReturnDto.setIsDepositHandleSucces(subRefundReturn.getIsDepositHandleSucces());
		  appSubRefundReturnDto.setAdminMessage(subRefundReturn.getAdminMessage());
		  appSubRefundReturnDto.setSellerMessage(subRefundReturn.getSellerMessage());
		  appSubRefundReturnDto.setGoodsNum(subRefundReturn.getGoodsNum());
		  appSubRefundReturnDto.setRefundAmount(subRefundReturn.getRefundAmount());
		  appSubRefundReturnDto.setPlatformRefund(subRefundReturn.getPlatformRefund());
		  appSubRefundReturnDto.setIsRefundDeposit(subRefundReturn.getIsRefundDeposit());
		  appSubRefundReturnDto.setDepositOutRefundNo(subRefundReturn.getDepositOutRefundNo());
		  appSubRefundReturnDto.setApplyState(subRefundReturn.getApplyState());
		  appSubRefundReturnDto.setSellerState(subRefundReturn.getSellerState());
		  appSubRefundReturnDto.setSubMoney(subRefundReturn.getSubMoney());
		  appSubRefundReturnDto.setUserName(subRefundReturn.getUserName());
		  appSubRefundReturnDto.setDepositOrderDatetime(subRefundReturn.getDepositOrderDatetime());
		  appSubRefundReturnDto.setUserId(subRefundReturn.getUserId());
		  appSubRefundReturnDto.setPayTypeName(subRefundReturn.getPayTypeName());
		  appSubRefundReturnDto.setDepositFlowTradeNo(subRefundReturn.getDepositFlowTradeNo());
		  appSubRefundReturnDto.setIsBill(subRefundReturn.getIsBill());
		  appSubRefundReturnDto.setReceiveTime(subRefundReturn.getReceiveTime());
		  appSubRefundReturnDto.setSubId(subRefundReturn.getSubId());
		  appSubRefundReturnDto.setAdminTime(subRefundReturn.getAdminTime());
		  appSubRefundReturnDto.setExpressName(subRefundReturn.getExpressName());
		  appSubRefundReturnDto.setDepositHandleType(subRefundReturn.getDepositHandleType());
		  appSubRefundReturnDto.setBuyerMessage(subRefundReturn.getBuyerMessage());
		  appSubRefundReturnDto.setPayTypeId(subRefundReturn.getPayTypeId());
		  appSubRefundReturnDto.setHandleType(subRefundReturn.getHandleType());
		  appSubRefundReturnDto.setDepositRefund(subRefundReturn.getDepositRefund());
		  appSubRefundReturnDto.setRefundSouce(subRefundReturn.getRefundSouce());
		  appSubRefundReturnDto.setBillSn(subRefundReturn.getBillSn());
		  appSubRefundReturnDto.setPhotoFile1(subRefundReturn.getPhotoFile1());
		  appSubRefundReturnDto.setPhotoFile3(subRefundReturn.getPhotoFile3());
		  appSubRefundReturnDto.setPhotoFile2(subRefundReturn.getPhotoFile2());
		  appSubRefundReturnDto.setRefundId(subRefundReturn.getRefundId());
		  appSubRefundReturnDto.setProductNum(subRefundReturn.getProductNum());
		  appSubRefundReturnDto.setProductAttribute(subRefundReturn.getProductAttribute());
		  appSubRefundReturnDto.setSubItemMoney(subRefundReturn.getSubItemMoney());
		  appSubRefundReturnDto.setReturnDetailAddress(subRefundReturn.getReturnDetailAddress());
		  appSubRefundReturnDto.setReturnContact(subRefundReturn.getReturnContact());
		  appSubRefundReturnDto.setReturnPhone(subRefundReturn.getReturnPhone());
		  return appSubRefundReturnDto;
		}
	
}
