package com.legendshop.app.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.model.constant.AppConfigPropertiesEnum;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.util.EnvironmentConfig;

import springfox.documentation.annotations.ApiIgnore;

/**
 * 第三方登录
 * 过时原因：第三登录配置信息已由前端管理 by:linzh
 */
@ApiIgnore
@RestController
@Deprecated
@RequestMapping("/third/config")
public class AppThirdConfigController {
	
	/**
	 * 参数初始化
	 * @return
	 */
	@PostMapping
	public ResultDto<JSONObject> config() {
		
		JSONObject jsonObject=new JSONObject(3);
		String third_weixin_appid= EnvironmentConfig.getInstance().getPropertyValue(AppConfigPropertiesEnum.THIRD_WEIXIN_APPID);
		String third_weixin_secret= EnvironmentConfig.getInstance().getPropertyValue(AppConfigPropertiesEnum.THIRD_WEIXIN_SECRET);
		JSONObject object=new JSONObject();
		object.put("third_weixin_appid", third_weixin_appid);
		object.put("third_weixin_secret", third_weixin_secret);
		jsonObject.put("weixin", object);
		
		String third_qq_appid= EnvironmentConfig.getInstance().getPropertyValue(AppConfigPropertiesEnum.THIRD_QQ_APPID);
		String third_qq_secret= EnvironmentConfig.getInstance().getPropertyValue(AppConfigPropertiesEnum.THIRD_QQ_SECRET);
		
		object=new JSONObject();
		object.put("third_qq_appid", third_qq_appid);
		object.put("third_qq_secret", third_qq_secret);
		jsonObject.put("qq", object);
		
		String third_weibo_appid= EnvironmentConfig.getInstance().getPropertyValue( AppConfigPropertiesEnum.THIRD_WEIBO_APPID);
		String third_weibo_secret= EnvironmentConfig.getInstance().getPropertyValue(AppConfigPropertiesEnum.THIRD_WEIBO_SECRET);
		
		object=new JSONObject();
		object.put("third_weibo_appid", third_weibo_appid);
		object.put("third_weibo_secret", third_weibo_secret);
		jsonObject.put("weibo", object);
		
		return ResultDtoManager.success(jsonObject);
	}
	
	
}
