/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.app.dto;

import java.util.Date;
import java.util.List;

import com.legendshop.model.dto.integral.IntegralOrderItemDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * app积分订单Dto
 */
@ApiModel(value="app积分订单Dto") 
public class AppIntegralOrderDto  {
	
	/** 积分订单id. */
	@ApiModelProperty(value="积分订单id") 
	private Long orderId;
	
	/** 订单流水号. */
	@ApiModelProperty(value="订单流水号") 
	private String orderSn;
	
	/** 订单商品总数. */
	@ApiModelProperty(value="订单商品总数") 
	private Integer productNums;
	
	/** 积分总数. */
	@ApiModelProperty(value="积分总数") 
	private Integer integralTotal;
	
	/** 0(默认):已兑换;1:已发货;2:已收货;3已完成;5已取消. */
	@ApiModelProperty(value="0(默认):待发货;1:待收货;2:已完成;3:已取消 ") 
	private Integer orderStatus;
	
	/** 添加时间. */
	@ApiModelProperty(value="添加时间") 
	private Date addTime;
	
	/** 积分订单项列表. */
	@ApiModelProperty(value="积分订单项列表")
	private List<IntegralOrderItemDto> orderItemDtos;
	
	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getOrderSn() {
		return orderSn;
	}

	public void setOrderSn(String orderSn) {
		this.orderSn = orderSn;
	}

	public Integer getProductNums() {
		return productNums;
	}

	public void setProductNums(Integer productNums) {
		this.productNums = productNums;
	}

	public Integer getIntegralTotal() {
		return integralTotal;
	}

	public void setIntegralTotal(Integer integralTotal) {
		this.integralTotal = integralTotal;
	}

	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public List<IntegralOrderItemDto> getOrderItemDtos() {
		return orderItemDtos;
	}

	public void setOrderItemDtos(List<IntegralOrderItemDto> orderItemDtos) {
		this.orderItemDtos = orderItemDtos;
	}
	
	
}
