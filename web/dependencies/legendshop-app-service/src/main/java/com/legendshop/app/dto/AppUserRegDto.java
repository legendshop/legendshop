/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;

import java.io.Serializable;

import com.legendshop.util.SafeHtml;

import io.swagger.annotations.ApiModelProperty;

/**
 * 用户注册参数
 */
public class AppUserRegDto{

	/** 用户昵称, 可以是4-16位的汉字、数字、字母和_，不能以 数字和_开头 */
	@ApiModelProperty(value = "用户昵称, 可以是4-16位的汉字、数字、字母和_，不能以 数字和_开头", hidden = true)
	private String nickName;
	
	/** 用户名, 可以是4-16位的汉字、数字、字母和_，不能以 数字和_开头 */
	@ApiModelProperty(value = "用户名, 可以是4-16位的汉字、数字、字母和_，不能以 数字和_开头")
	private String userName;

	/** 密码. */
	@ApiModelProperty(value = "用户密码, 密码由6-20位字母、数字或符号(除空格)的两种及以上组成")
	private String password;
	
	/** 手机号码 */
	@ApiModelProperty(value = "手机号码, 需要正确格式的手机号码")
	private String mobile;
	
	/** 验证码 **/
	@ApiModelProperty(value = "短信验证码")
	private String mobileCode;
	
	/** 分销上级用户*/
	@ApiModelProperty(value="分销上级用户,分销用户链接过来才要传")
	private String parentUserName;
	
	@ApiModelProperty(value="通行证密钥")
	private String passportKey;
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMobileCode() {
		return mobileCode;
	}

	public void setMobileCode(String mobileCode) {
		this.mobileCode = mobileCode;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	public String getParentUserName() {
		return parentUserName;
	}

	public void setParentUserName(String parentUserName) {
		this.parentUserName = parentUserName;
	}
	
	public String getPassportKey() {
		return passportKey;
	}

	public void setPassportKey(String passportKey) {
		this.passportKey = passportKey;
	}

	public void encoding(){
		// 过滤特殊字符
		SafeHtml safeHtml = new SafeHtml();
		this.nickName = safeHtml.makeSafe(nickName);
		this.userName = safeHtml.makeSafe(userName);
		this.mobile = safeHtml.makeSafe(mobile);
		this.mobileCode = safeHtml.makeSafe(mobileCode);
	}

}