/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *秒杀成功Dto.
 *
 */
@ApiModel(value="秒杀成功Dto")
public class AppSeckillSuccessDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 秒杀成功Id. */
	@ApiModelProperty(value="秒杀成功Id")
	private Long id;

	/** 秒杀活动Id. */
	@ApiModelProperty(value="秒杀活动Id")
	private Long seckillId; 
	
	/** skuId. */
	@ApiModelProperty(value="skuId")
	private Long skuId; 
	
	/** prodId. */
	@ApiModelProperty(value="prodId")
	private Long prodId;
	
	/** 秒杀数量. */
	@ApiModelProperty(value="秒杀数量")
	private Integer seckillNum; 
	
	/** 秒杀价格. */
	@ApiModelProperty(value="秒杀价格")
	private Double seckillPrice; 
	
	/** 用户的状态默认0 0成功状态 1:已下单. */
	@ApiModelProperty(value="用户的状态默认0 0成功状态 1:已下单")
	private Integer status; 
	
	/** 秒杀商品名称. */
	@ApiModelProperty(value="秒杀商品名称")
	private String prodName;
	
	/** 商品原价. */
	@ApiModelProperty(value="商品原价")
	private Double price;
	
	/** 商品sku图片. */
	@ApiModelProperty(value="商品sku图片")
	private String pic;
	
	/** 秒杀商品剩余百分比 */
	@ApiModelProperty(value="秒杀商品剩余百分比")
	private Double seckillRate ;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSeckillId() {
		return seckillId;
	}

	public void setSeckillId(Long seckillId) {
		this.seckillId = seckillId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}
	
	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Integer getSeckillNum() {
		return seckillNum;
	}

	public void setSeckillNum(Integer seckillNum) {
		this.seckillNum = seckillNum;
	}

	public Double getSeckillPrice() {
		return seckillPrice;
	}

	public void setSeckillPrice(Double seckillPrice) {
		this.seckillPrice = seckillPrice;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Double getSeckillRate() {
		return seckillRate;
	}

	public void setSeckillRate(Double seckillRate) {
		this.seckillRate = seckillRate;
	}
	
}
