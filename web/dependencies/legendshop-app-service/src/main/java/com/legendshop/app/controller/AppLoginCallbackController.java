package com.legendshop.app.controller;

import java.util.Date;

import com.legendshop.model.constant.AppTokenTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.service.AppLoginCallbackService;
import com.legendshop.app.service.AppTokenService;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.app.AppPassportDto;
import com.legendshop.model.dto.app.AppTokenDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.entity.AppToken;
import com.legendshop.model.entity.Passport;
import com.legendshop.model.entity.User;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 第三方登录回调
 */
@ApiIgnore
@Api(tags="第三方登录回调",value="第三方登录回调")
@RestController
public class AppLoginCallbackController{
	
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(AppLoginCallbackController.class);
	
	@Autowired
	private AppLoginCallbackService appLoginCallbackService;
	
	@Autowired
	private AppTokenService appTokenService;
	
	
	/**
	 * @Description: 第三方登录回调
	 * @date 2016-7-20 
	 */
	@ApiOperation(value = "第三方登录回调", httpMethod = "POST", notes = "第三方登录回调",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/loginCallback")
	public  ResultDto<AppTokenDto> loginCallback(AppPassportDto passportDto){
		log.info("第三方登录回调------------------------------------->");
		if(checkQQCallbackParam(passportDto)){
			log.info("param error==============={}"+JSONUtil.getJson(passportDto));
			return ResultDtoManager.fail(-1, "参数有误！");
		}
		
		if(!passportDto.getCallbackType().equals("qq")&&!passportDto.getCallbackType().equals("weixin")
				&&!passportDto.getCallbackType().equals("weibo")){
			return ResultDtoManager.fail(-1, "回调类型有误！");
		}
		
		Passport passport=appLoginCallbackService.autoRegBind(passportDto);
		if(passport==null){
			return ResultDtoManager.fail(0, "login failed");
		}
		
		User user = appLoginCallbackService.getUser(passport.getUserId());
		if(AppUtils.isBlank(user)){
			log.info("User==============={}"+JSONUtil.getJson(user));
			//addUserTimes(user.getName());//记录用户的登录出错次数
			return ResultDtoManager.fail(0, "login failed");
		}
		return returnAppToken(user,passportDto);
	}
	
	
	
	
	/**
	 * @Description: 返回token
	 * @date 2016-7-21 
	 */
	public ResultDto<AppTokenDto> returnAppToken(User user,AppPassportDto passportDto){
		AppToken token = appTokenService.getAppToken(user.getId(),passportDto.getPlatform(), AppTokenTypeEnum.USER.value());
		if (token != null) {
			token.setStartDate(new Date());//更新登录时间
			appTokenService.updateAppToken(token);
			return ResultDtoManager.success(token.toDto());
		} else {// 第一次登陆，保存并返回token
			Date now = new Date();
			AppToken persistToken = new AppToken();
			// 获得随机码
			String accessToken = CommonServiceUtil.getRandomString(30);
			persistToken.setAccessToken(accessToken);
			persistToken.setDeviceId(passportDto.getDeviceId());
			persistToken.setPlatform(passportDto.getPlatform());
			persistToken.setRecDate(now);
			// 获得随机码
			String securiyCode = CommonServiceUtil.getRandomString(8);
			persistToken.setSecuriyCode(securiyCode);
			persistToken.setStartDate(now);
			persistToken.setUserId(user.getId());
			persistToken.setUserName(user.getName());
			persistToken.setValidateTime(1209600L);// 保持登陆2周
			persistToken.setVerId(Constants.APP_VERSION);
			appTokenService.saveAppToken(persistToken);
			return ResultDtoManager.success(persistToken.toDto());
		}
	}
	
	
	/**
	 * @Description: 判断登录回调参数
	 * @date 2016-7-20 
	 */
	public boolean checkQQCallbackParam(AppPassportDto passport){
		if(AppUtils.isBlank(passport)||AppUtils.isBlank(passport.getOpenId())
		||AppUtils.isBlank(passport.getNickName())||AppUtils.isBlank(passport.getRegIP())||AppUtils.isBlank(passport.getCallbackType())||
		AppUtils.isBlank(passport.getDeviceId())||AppUtils.isBlank(passport.getPlatform())||AppUtils.isBlank(passport.getVerId())
				){
			return true;
		}
		return false;
	}

	
}
