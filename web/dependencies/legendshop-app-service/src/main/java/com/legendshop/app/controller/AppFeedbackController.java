package com.legendshop.app.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.model.constant.UserFeelbackStatusEnum;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.UserFeedBack;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.UserFeedBackService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SafeHtml;
import com.legendshop.web.helper.IPHelper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 意见反馈
 *
 */
@RestController
@RequestMapping(value = "/p")
@Api(tags="意见反馈",value="意见反馈相关操作")
public class AppFeedbackController {
	
	private final Logger LOGGER = LoggerFactory.getLogger(AppFeedbackController.class);
    @Autowired
    private UserFeedBackService userFeedBackService;
    
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;


	/**
	 * 提交反馈意见
	 * @param content
	 * @param mobile
	 * @param reIp
	 * @param feedBackSource
	 * @return
	 */
    @ApiOperation(value = "提交反馈意见", httpMethod = "POST", notes = "提交反馈意见,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
    	@ApiImplicitParam(paramType="query", name = "mobile", value = "手机号码", required = true, dataType = "string"),
    	@ApiImplicitParam(paramType="query", name = "content", value = "反馈内容", required = true, dataType = "string"),
    })
    @PostMapping("/submitFeedback")
	public ResultDto<Object> submitFeedback(HttpServletRequest request,String content, String mobile) {
		try {
			LoginedUserInfo token = loginedUserService.getUser();
			if (AppUtils.isBlank(token)) {
				return ResultDtoManager.fail(-1, "请先登录!");
			}
			
			String userId = token.getUserId();
			String userName = token.getUserName();
			
			if (AppUtils.isBlank(mobile)) {
				return ResultDtoManager.fail(-1, "手机号码不能为空!");
			}
	    	if(content.isEmpty()){
	    		return ResultDtoManager.fail(-1, "反馈内容不能为空!");
	    	}
	    	
	    	//构建反馈意见参数
	    	UserFeedBack userFeedBack=new UserFeedBack();
    		userFeedBack.setUserId(userId);
    		userFeedBack.setName(userName);
	    	
			// 处理评论内容
			SafeHtml safeHtml = new SafeHtml();
			String html = safeHtml.makeSafe(content.trim());
			StringBuilder nicksb = new StringBuilder();
			int l = html.length();
			for (int i = 0; i < l; i++) {
				char _s = html.charAt(i);
				if (isEmojiCharacter(_s)) {
					nicksb.append(_s);
				}
			}
	    	
	     	userFeedBack.setContactInformation(mobile);
	    	userFeedBack.setRecDate(new Date());
	    	userFeedBack.setContent(nicksb.toString());
	    	userFeedBack.setStatus(UserFeelbackStatusEnum.UNREAD.value());
	    	userFeedBack.setIp(IPHelper.getIpAddr(request));
	    	userFeedBack.setFeedBackSource(UserFeelbackStatusEnum.WAP.value());//add 反馈来源 2016.07.11
	        userFeedBackService.saveUserFeedBack(userFeedBack);
			
			return ResultDtoManager.success();
		} catch (Exception e){
			LOGGER.error("提交意见反馈异常!", e);
			return ResultDtoManager.fail();
		}
	}
    
    
    /**
	 * 是否是表情符号
	 * @param codePoint
	 * @return
	 */
	private boolean isEmojiCharacter(char codePoint) {
	    return (codePoint == 0x0) || (codePoint == 0x9) 
	    	|| (codePoint == 0xA) || (codePoint == 0xD) 
	    	|| ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) 
	        || ((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) 
	        || ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF));
	}
	
}
