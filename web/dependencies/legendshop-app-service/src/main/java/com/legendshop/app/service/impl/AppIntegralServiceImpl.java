package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.legendshop.app.dao.AppIntegralOrderDao;
import com.legendshop.app.dao.AppIntegralOrderItemDao;
import com.legendshop.app.dao.AppIntegralProdDao;
import com.legendshop.app.dto.AppIntegralOrderDto;
import com.legendshop.app.dto.AppIntegralProdDto;
import com.legendshop.app.dto.AppIntegralProdListDto;
import com.legendshop.app.dto.AppSubmitIntegralProdDto;
import com.legendshop.app.service.AppIntegralService;
import com.legendshop.base.exception.BusinessException;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.business.dao.IntegraLogDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.dao.support.PageProvider;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.IntegralLogTypeEnum;
import com.legendshop.model.constant.IntegralOrderStatusEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.integral.IntegralOrderDto;
import com.legendshop.model.dto.order.DeliveryDto;
import com.legendshop.model.dto.order.OrderSearchParamDto;
import com.legendshop.model.entity.Category;
import com.legendshop.model.entity.DeliveryCorp;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.model.entity.UserAddressSub;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.integral.IntegraLog;
import com.legendshop.model.entity.integral.IntegralOrder;
import com.legendshop.model.entity.integral.IntegralOrderItem;
import com.legendshop.model.entity.integral.IntegralProd;
import com.legendshop.spi.service.DeliveryCorpService;
import com.legendshop.spi.service.UserAddressSubService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

/**
 * 积分商城
 */
@Service("appIntegralService")
public class AppIntegralServiceImpl implements AppIntegralService {
	
	@Autowired
	private AppIntegralProdDao appIntegralProdDao;

	@Autowired
	private UserDetailDao userDetailDao;
	
	@Autowired
    private UserAddressSubService userAddressSubService;
    
	@Autowired
    private AppIntegralOrderDao appIntegralOrderDao;
    
	@Autowired
    private AppIntegralOrderItemDao appIntegralOrderItemDao;
    
	@Autowired
    private IntegraLogDao integraLogDao;
    
	@Autowired
    private  DeliveryCorpService deliveryCorpService;
	/* 
	 * 获取积分商品列表
	 */
	@Override
	public AppPageSupport<IntegralProd> queryIntegralProd(String curPageNO,String integralSort, String saleNumSort) {
		PageSupport<IntegralProd> pageSupport = appIntegralProdDao.getIntegralProd(curPageNO, integralSort, saleNumSort);
		return convertProPageSupportDto(pageSupport);
	}

	/**
	 * 转换积分商品page
	 * @param pageSupport
	 * @return
	 */
	private AppPageSupport<IntegralProd> convertProPageSupportDto(PageSupport<IntegralProd> pageSupport){
		if(AppUtils.isBlank(pageSupport)){
			return null;
		}
		AppPageSupport<IntegralProd> pageSupportDto = new AppPageSupport<IntegralProd>();
		pageSupportDto.setTotal(pageSupport.getTotal());
		pageSupportDto.setCurrPage(pageSupport.getCurPageNO());
		pageSupportDto.setPageSize(pageSupport.getPageSize());
		List<IntegralProd> integralProds = pageSupport.getResultList();
		pageSupportDto.setResultList(integralProds);
		pageSupportDto.setPageCount(pageSupport.getPageCount());
		return pageSupportDto;
	}
	
	


	/**
	 * 根据ID获取积分详情Dto
	 */
	@Override
	@Cacheable(value="IntegralProdDto",key="#id")
	public AppIntegralProdDto getIntegralProdDtoById(Long id) {
		
		IntegralProd integralProd = appIntegralProdDao.getIntegralProd(id);
		
		if (AppUtils.isBlank(integralProd)) {
			return null;
		}
		return convertToAppIntegralProdDto(integralProd);
	}
	
	/**
	 * 
	 */
	@Override
	public IntegralProd getIntegralProdById(Long id) {
		
		IntegralProd integralProd = appIntegralProdDao.getIntegralProd(id);
		if (integralProd != null) {
			if (AppUtils.isNotBlank(integralProd.getFirstCid())) {
				Category category = findCategory(integralProd.getFirstCid());
				if (category != null) {
					integralProd.setCategoryName(category.getName());
				}
			}
			if (AppUtils.isNotBlank(integralProd.getTwoCid())) {
				Category category = findCategory(integralProd.getTwoCid());
				if (category != null) {
					integralProd.setTwoCategoryName(category.getName());
				}
			}
			if (AppUtils.isNotBlank(integralProd.getThirdCid())) {
				Category category = findCategory(integralProd.getThirdCid());
				if (category != null) {
					integralProd.setThirdCategoryName(category.getName());
				}
			}
		}
		return integralProd;
	}
	
	  /**
	   * 积分商品分类
	 * @param categoryId
	 * @return
	 */
	public Category findCategory(Long categoryId){
	    	List<Category> categories=appIntegralProdDao.findAllIntegralCategory();
	    	if(AppUtils.isBlank(categories)){
	    		return null;
	    	}
	    	for (Category category : categories) {
	    		if(category.getId().equals(categoryId)){
	    			return category;
	    		}
			}
	    	return null;
	    }
	

	/* 
	 * 用积分兑换商品
	 */
	@Override
	public String shopBuyIntegral(String userId, UserAddress userAddress,IntegralProd integralProd, Integer count, String orderDesc) {
		UserDetail userDetail=userDetailDao.getUserDetailByidForUpdate(userId);//查询userDetail 并锁定
		Double score=Arith.mul(integralProd.getExchangeIntegral(), count);
		/**更新用户的积分信息**/
	    int result=userDetailDao.updateScore(score.intValue(), userDetail.getScore(), userId);
		if(result==0){
			return "对不起, 您的积分不足!";
		}
	    String orderSn = CommonServiceUtil.getSubNember(userDetail.getUserName());
		
		/**保存订单用户地址信息 **/
		UserAddressSub userAddressSub=saveUserAddressSub(userAddress);
		/**保存订单 **/
		IntegralOrder order=new IntegralOrder();
		order.setOrderSn(orderSn);
		order.setUserId(userId);
		order.setUserName(userDetail.getUserName());
		order.setProductNums(count);
		order.setIntegralTotal(score.intValue());
        order.setOrderStatus(IntegralOrderStatusEnum.SUBMIT.value());
        order.setAddrOrderId(userAddressSub.getAddrOrderId());
        order.setAddTime(new Date());
        order.setOrderDesc(orderDesc);
        Long orderId=appIntegralOrderDao.saveIntegralOrder(order);
		
        /**保存订单项**/
        IntegralOrderItem item=new IntegralOrderItem();
		item.setOrderId(orderId);
		item.setUserId(userId);
		item.setProdId(integralProd.getId());
		item.setProdName(integralProd.getName());
		item.setProdPic(integralProd.getProdImage());
		item.setBasketCount(count);
		item.setExchangeIntegral(integralProd.getExchangeIntegral());
		item.setPrice(integralProd.getPrice());
		appIntegralOrderItemDao.saveIntegralOrderItem(item);
		
		/**保存积分消费记录**/
		IntegraLog integraLog=new IntegraLog();
		integraLog.setUserId(userId);
		integraLog.setIntegralNum(-score.intValue());
		integraLog.setLogType(IntegralLogTypeEnum.LOG_ORDER.value());
		integraLog.setLogDesc("订单消费,积分订单流水号:"+orderSn);
		integraLog.setAddTime(new Date());
		integraLogDao.save(integraLog);
		
		/**更新商品的库存 **/
		result=appIntegralProdDao.updateStock(count,integralProd.getId());
		if(result<=0){
			throw new BusinessException("兑换订单失败,该商品库存不足或者已经被下线！");
		}
		return Constants.SUCCESS;
	}
	
	
	public UserAddressSub saveUserAddressSub(UserAddress userAddress) {
		UserAddressSub userAddressSub = new UserAddressSub();
		userAddressSub.setUserId(userAddress.getUserId());
		userAddressSub.setReceiver(userAddress.getReceiver());
		userAddressSub.setSubPost(userAddress.getSubPost());
		userAddressSub.setProvinceId(userAddress.getProvinceId());
		userAddressSub.setCityId(userAddress.getCityId());
		userAddressSub.setAreaId(userAddress.getAreaId());
		userAddressSub.setSubAdds(userAddress.getSubAdds());
		userAddressSub.setAliasAddr(userAddress.getAliasAddr());
		StringBuffer buffer=new StringBuffer();
		buffer.append(userAddress.getProvince()).append(" ").append(userAddress.getCity())
		.append(" ").append(userAddress.getArea()).append(" ").append(userAddress.getSubAdds());
		userAddressSub.setDetailAddress(buffer.toString());
		userAddressSub.setEmail(userAddress.getEmail());
		userAddressSub.setMobile(userAddress.getMobile());
		userAddressSub.setTelphone(userAddress.getTelphone());
		Long addrOrderId = userAddressSubService.saveUserAddressSub(userAddressSub);
		userAddressSub.setAddrOrderId(addrOrderId);
		return userAddressSub;
	}
	
	private static String ADMIN_COUNT_ORDER_SQL="SELECT COUNT(o.id) FROM ls_integral_order o WHERE 1=1 ";
	
	private static String ORDER_SQL="SELECT o.id AS orderId,o.order_sn AS orderSn,o.user_id AS userId,o.user_name AS userName ,o.product_nums AS productNums,o.integral_total AS integralTotal,o.dvy_type_id AS dvyTypeId,o.dvy_flow_id AS dvyFlowId,o.order_status AS orderStatus,o.addr_order_id AS addrOrderId,o.add_time AS addTime," +
			"o.dvy_time AS dvyTime,o.finally_time AS finallyTime,o.order_desc AS orderDesc" +
			",item.id AS orderItemId,item.prod_id AS prodId,item.prod_name AS prodName,item.prod_pic AS prodPic,item.basket_count AS basketCount," +
			" item.exchange_integral AS exchangeIntegral,item.price AS price,addr.receiver  AS receiver,addr.detail_address AS subAdds,addr.sub_post AS subPost,addr.province_id AS province,addr.city_id AS city," +
			" addr.area_id AS AREA,addr.mobile AS mobile, addr.telphone AS telphone,addr.email AS email ,addr.sub_post AS subPost from  FROM_SUB_SQL " +
			" LEFT JOIN ls_usr_addr_sub addr ON addr.addr_order_id=o.addr_order_id LEFT JOIN ls_integral_order_item item ON item.order_id=o.id order by o.add_time desc ";
	
	/* 
	 * 查询积分订单
	 */
	@Override
	public AppPageSupport<IntegralOrderDto> getIntegralOrderDtos(OrderSearchParamDto paramDto) {
		
		int curPageNO=Integer.valueOf(paramDto.getCurPageNO());
		int pageSize=paramDto.getPageSize();
		int offset=(curPageNO-1)*paramDto.getPageSize();
		
		StringBuilder orderSQl=new StringBuilder();
		//不显示已删除的订单
		List<Object> args=new ArrayList<Object>();
		if(AppUtils.isNotBlank(paramDto.getUserName())){
			args.add(paramDto.getUserName());
			orderSQl.append(" and o.user_name =?  ");
		}
		if(AppUtils.isNotBlank(paramDto.getUserId())){
			args.add(paramDto.getUserId());
			orderSQl.append(" and o.user_id =?  ");
		}
		
		if(AppUtils.isNotBlank(paramDto.getSubNumber())){
			args.add( paramDto.getSubNumber());
			orderSQl.append(" and o.order_sn =?  ");
		}
		if(AppUtils.isNotBlank(paramDto.getStatus())){
			args.add( paramDto.getStatus());
			orderSQl.append(" and o.order_status =?  ");
		}
		
		if (AppUtils.isNotBlank(paramDto.getStartDate())) {
			 args.add(paramDto.getStartDate());
			 orderSQl.append(" and o.add_time >= ?  ");
		}
		if(AppUtils.isNotBlank(paramDto.getEndDate())){
			 args.add(paramDto.getEndDate());
			 orderSQl.append(" and o.add_time < ?  ");
		}
		
		StringBuilder countSql=new StringBuilder();
		countSql.append(ADMIN_COUNT_ORDER_SQL);
		countSql.append(orderSQl.toString());
		
		StringBuilder sb=new StringBuilder();
		sb.append("SELECT o.* FROM ls_integral_order o WHERE 1=1 ");
		sb.append(orderSQl.toString());
		sb.append("ORDER BY o.add_time DESC,o.order_status ASC");
		
		StringBuilder limitSql=new StringBuilder("(");
		appIntegralOrderDao.getDialect().getLimitString(limitSql, sb.toString(), offset, pageSize); //分页函数，跟数据库相关
		limitSql.append(") o ");
		
		String subSQl=ORDER_SQL.replace("FROM_SUB_SQL", limitSql.toString());
		
		Long total=appIntegralOrderDao.getLongResult(countSql.toString(), args.toArray());
		List<IntegralOrderDto> integralOrderDtos=appIntegralOrderDao.getIntegralOrderDtos(subSQl,args);
		
		AppPageSupport<IntegralOrderDto> pageSupport=initPageProvider(total, curPageNO, pageSize, integralOrderDtos,paramDto.getPageProvider());
		return pageSupport;
	}
	
	/**
	 * 处理订单集合PageProvider
	 * @param total
	 * @param curPageNO
	 * @param pageSize
	 * @param resultList
	 * @param pageProvider
	 * @return
	 */
	private AppPageSupport<IntegralOrderDto> initPageProvider(long total, Integer curPageNO, Integer pageSize, List<IntegralOrderDto> resultList,PageProvider pageProvider) {
		AppPageSupport<IntegralOrderDto> pageSupportDto=new AppPageSupport<IntegralOrderDto>();
		pageSupportDto.setTotal(total);
		long pageCount = (total + pageSize - 1) / pageSize;
		pageSupportDto.setPageCount(pageCount);
		pageSupportDto.setCurrPage(curPageNO);
		pageSupportDto.setPageSize(pageSize);
		pageSupportDto.setResultList(resultList);
	    return pageSupportDto;
	}
	

	
	/* 
	 * 根据流水号查询订单详情
	 */
	@Override
	public IntegralOrderDto findIntegralOrderDetail(String orderSn) {
		IntegralOrderDto integralOrderDto=appIntegralOrderDao.findIntegralOrderDetail(orderSn);
		if(integralOrderDto!=null){
				DeliveryCorp corp=deliveryCorpService.getDeliveryCorpByDvyId(integralOrderDto.getDvyTypeId());
				if(AppUtils.isNotBlank(corp)){
					integralOrderDto.setDvyTime(integralOrderDto.getDvyTime());
					DeliveryDto delivery=new DeliveryDto();
					delivery.setDvyTypeId(integralOrderDto.getDvyTypeId());
					delivery.setDvyFlowId(integralOrderDto.getDvyFlowId());
					delivery.setDelName(corp.getName());
					delivery.setDelUrl(corp.getCompanyHomeUrl());
					delivery.setQueryUrl(corp.getQueryUrl());
					integralOrderDto.setDelivery(delivery);
				}
		}
		return integralOrderDto;
	}
	
	
	
	/* 
	 * 取消订单
	 */
	@Override
	public String orderCancel(String orderSn, String userId,String updateUserId, String logDesc) {
		//取消订单操作
		IntegralOrder integralOrder=appIntegralOrderDao.getIntegralOrderByOrderSn(orderSn);
		if(AppUtils.isBlank(integralOrder)){
			return "更新失败,没有找到该订单信息";
		}
		if(!IntegralOrderStatusEnum.SUBMIT.value().equals(integralOrder.getOrderStatus())){
			return "取消失败,该订单状态不是提交状态";
		}
		if(AppUtils.isNotBlank(userId) &&  userId.equals(integralOrder.getUserId())){
			return "您没有权限操作该订单！";
		}
		
		int result=appIntegralOrderDao.orderCancel(orderSn);
		if(result>0){
			//回滚订单积分
			userDetailDao.updateScore(integralOrder.getIntegralTotal(), integralOrder.getUserId());
			//记录积分明细
			IntegraLog integraLog=new IntegraLog();
			integraLog.setIntegralNum(integralOrder.getIntegralTotal());
			integraLog.setLogType(IntegralLogTypeEnum.LOG_DEFAULT.value());
			integraLog.setAddTime(new Date());
			integraLog.setLogDesc(logDesc);
			integraLog.setUpdateUserId(updateUserId);
			integraLog.setUserId(integralOrder.getUserId());
			integraLogDao.saveIntegraLog(integraLog);
		}
		return Constants.SUCCESS;
	}
	
	/* 
	 * 删除订单
	 */
	@Override
	public String orderDel(String orderSn, String userId,String updateUserId, String logDesc) {
		
		//删除订单操作
		IntegralOrder integralOrder=appIntegralOrderDao.getIntegralOrderByOrderSn(orderSn);
		if(AppUtils.isBlank(integralOrder)){
			return "删除失败,没有找到该订单信息";
		}
		
		if(!IntegralOrderStatusEnum.CANCEL.value().equals(integralOrder.getOrderStatus())){
			return "删除失败,该订单状态不是取消状态";
		}
		
		if(AppUtils.isNotBlank(userId) &&  userId.equals(integralOrder.getUserId())){
			return "您没有权限操作该订单！";
		}
		if(IntegralOrderStatusEnum.CANCEL.value().equals(integralOrder.getOrderStatus())){
			appIntegralOrderItemDao.deleteByOrderId(integralOrder.getId());
			appIntegralOrderDao.delete(integralOrder);
		}
		
		return Constants.SUCCESS;
	}
	
	
	/* 
	 * 确认收货
	 */
	@Override
	public String shouhuo(String orderSn) {
		if(AppUtils.isBlank(orderSn)){
			return Constants.FAIL;
		}
		IntegralOrder order=appIntegralOrderDao.getIntegralOrderByOrderSn(orderSn);
		if(AppUtils.isBlank(order)){
			return Constants.FAIL;
		}
		order.setOrderStatus(IntegralOrderStatusEnum.SUCCESS.value());
		order.setFinallyTime(new Date());
		appIntegralOrderDao.update(order);
		return Constants.SUCCESS;
	}


	@Override
	public int getIntegralBuyCount(Long id, String userId) {
		return appIntegralOrderDao.getIntegralBuyCount(id,userId);
	}

	//积分商品详情转Dto方法
	private AppIntegralProdDto convertToAppIntegralProdDto(IntegralProd integralProd){
		  AppIntegralProdDto appIntegralProdDto = new AppIntegralProdDto();
		  appIntegralProdDto.setLimitNum(integralProd.getLimitNum());
		  appIntegralProdDto.setProdImage(integralProd.getProdImage());
		  appIntegralProdDto.setIsCommend(integralProd.getIsCommend());
		  appIntegralProdDto.setExchangeIntegral(integralProd.getExchangeIntegral());
		  appIntegralProdDto.setPrice(integralProd.getPrice());
		  appIntegralProdDto.setName(integralProd.getName());
		  appIntegralProdDto.setProdStock(integralProd.getProdStock());
		  appIntegralProdDto.setIsLimit(integralProd.getIsLimit());
		  appIntegralProdDto.setId(integralProd.getId());
		  appIntegralProdDto.setSaleNum(integralProd.getSaleNum());
		  appIntegralProdDto.setProdDesc(integralProd.getProdDesc());
		  return appIntegralProdDto;
		}
	
	@Override
	public AppPageSupport<AppIntegralProdListDto> getIntegralProdList(String curPageNO, Integer pageSize,
			Boolean isCommend, String order) {
		AppPageSupport<AppIntegralProdListDto> ps = appIntegralProdDao.getIntegralProdList(curPageNO, pageSize, isCommend, order);
		return ps;
	}
	
	@Override
	public AppSubmitIntegralProdDto convertToAppSubmitIntegralProdDto(IntegralProd integralProd) {
		AppSubmitIntegralProdDto dto = new AppSubmitIntegralProdDto();
		dto.setId(integralProd.getId());
		dto.setExchangeIntegral(integralProd.getExchangeIntegral());
		dto.setName(integralProd.getName());
		dto.setProdImage(integralProd.getProdImage());
		return dto;
	}

	@Override
	public AppPageSupport<AppIntegralOrderDto> getIntegralOrderDtoList(OrderSearchParamDto paramDto) {
		AppPageSupport<IntegralOrderDto> integralOrderDtos = getIntegralOrderDtos(paramDto);
		List<IntegralOrderDto> resultList = integralOrderDtos.getResultList();
		AppPageSupport<AppIntegralOrderDto> appPageSupport = new AppPageSupport<AppIntegralOrderDto>();
		if(AppUtils.isNotBlank(resultList)){
			List<AppIntegralOrderDto> arrayList = new ArrayList<AppIntegralOrderDto>();
			for (IntegralOrderDto integralOrderDto : resultList) {
				AppIntegralOrderDto appIntegralOrderDto = new AppIntegralOrderDto();
				appIntegralOrderDto.setAddTime(integralOrderDto.getAddTime());
				appIntegralOrderDto.setIntegralTotal(integralOrderDto.getIntegralTotal());
				appIntegralOrderDto.setOrderId(integralOrderDto.getOrderId());
				appIntegralOrderDto.setOrderSn(integralOrderDto.getOrderSn());
				appIntegralOrderDto.setOrderStatus(integralOrderDto.getOrderStatus());
				appIntegralOrderDto.setProductNums(integralOrderDto.getProductNums());
				appIntegralOrderDto.setOrderItemDtos(integralOrderDto.getOrderItemDtos());
				
				arrayList.add(appIntegralOrderDto);
			}
			appPageSupport.setResultList(arrayList);
			appPageSupport.setCurrPage(integralOrderDtos.getCurrPage());
			appPageSupport.setPageCount(integralOrderDtos.getPageCount());
			appPageSupport.setPageSize(integralOrderDtos.getPageSize());
			appPageSupport.setTotal(integralOrderDtos.getTotal());
		}
		return appPageSupport;
	}

}
