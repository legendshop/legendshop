package com.legendshop.app.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import com.legendshop.app.dto.AppOrderDetailsParamsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.AppUserShopCartOrderDto;
import com.legendshop.app.service.AppCartOrderService;
import com.legendshop.model.constant.PayPctTypeEnum;
import com.legendshop.model.dto.TransfeeDto;
import com.legendshop.model.dto.app.cartorder.ShopCartOrderDto;
import com.legendshop.model.dto.app.cartorder.ShopGroupCartOrderItemDto;
import com.legendshop.model.dto.app.shopcart.ShopCartItemDto;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.marketing.MarketingDto;
import com.legendshop.model.dto.presell.PreSellShopCartItem;
import com.legendshop.model.entity.Invoice;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.spi.service.InvoiceService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

@Service("appCartOrderService")
public class AppCartOrderServiceImpl implements AppCartOrderService{
	
	@Autowired
	private InvoiceService invoiceService;
	
	@Autowired
	private ShopDetailService shopDetailService;
	
	@Override
	public AppUserShopCartOrderDto getShoppingCartOrderDto(UserShopCartList userShopCartList) {
		
		if(AppUtils.isBlank(userShopCartList.getShopCarts())){
			return null;
		}
		
		return convertToAppUserShopCartOrderDto(userShopCartList);
	}
	
	@Override
	public AppUserShopCartOrderDto getMergeGroupShoppingCartOrderDto(UserShopCartList shopCartList, AppOrderDetailsParamsDto reqParams, String userId) {
		
		// 拼团订单需要将拼团活动编号添加addNumber
		shopCartList.setAddNumber(reqParams.getAddNumber());

		// 查出买家的默认发票内容 发票ID不为空的时候，获取选择的发票
		Invoice userdefaultInvoice = null;
		if (AppUtils.isBlank(reqParams.getInvoiceId())) {
			userdefaultInvoice = invoiceService.getDefaultInvoice(userId);
		}else{
			userdefaultInvoice = invoiceService.getInvoice(reqParams.getInvoiceId(), userId);
		}
		shopCartList.setDefaultInvoice(userdefaultInvoice);
		
		//获取卖家是否开启发票功能
		Long shopId = shopCartList.getShopCarts().get(0).getShopId();
	    ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
	    
	    Integer switchInvoice = 0;
	    if (null != shopDetail && AppUtils.isNotBlank(shopDetail.getSwitchInvoice())) {
	    	switchInvoice = shopDetail.getSwitchInvoice();

		}
	    shopCartList.setSwitchInvoice(switchInvoice);
	    
	    AppUserShopCartOrderDto appUserShopCartOrderDto = convertToAppUserShopCartOrderDto(shopCartList);
	    
		return appUserShopCartOrderDto;
	}
	
	@Override
	public AppUserShopCartOrderDto getSeckillShoppingCartOrderDto(UserShopCartList shopCartList,AppOrderDetailsParamsDto reqParams, String userId) {

		// 查出买家的默认发票内容 发票ID不为空的时候，获取选择的发票
		Invoice userdefaultInvoice = null;
		if (AppUtils.isBlank(reqParams.getInvoiceId())) {
			userdefaultInvoice = invoiceService.getDefaultInvoice(userId);
		}else{
			userdefaultInvoice = invoiceService.getInvoice(reqParams.getInvoiceId(), userId);
		}
		shopCartList.setDefaultInvoice(userdefaultInvoice);
		
		//获取卖家是否开启发票功能
		Long shopId = shopCartList.getShopCarts().get(0).getShopId();
	    ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
	    
	    Integer switchInvoice = 0;
	    if (null != shopDetail && AppUtils.isNotBlank(shopDetail.getSwitchInvoice())) {
	    	switchInvoice = shopDetail.getSwitchInvoice();

		}
	    shopCartList.setSwitchInvoice(switchInvoice);
	    
	    AppUserShopCartOrderDto appUserShopCartOrderDto = convertToAppUserShopCartOrderDto(shopCartList);
	    
		return appUserShopCartOrderDto;
	}
	
	@Override
	public AppUserShopCartOrderDto getPresellShoppingCartOrderDto(UserShopCartList shopCartList, String userId, String payType,Long invoiceId) {

		// 查出买家的默认发票内容 发票ID不为空的时候，获取选择的发票
		Invoice userdefaultInvoice = null;
		if (AppUtils.isBlank(invoiceId)){
			userdefaultInvoice = invoiceService.getDefaultInvoice(userId);
		}else{
			userdefaultInvoice = invoiceService.getInvoice(invoiceId, userId);
		}
		shopCartList.setDefaultInvoice(userdefaultInvoice);
		
		//获取卖家是否开启发票功能
		Long shopId = shopCartList.getShopCarts().get(0).getShopId();
	    ShopDetail shopDetail = shopDetailService.getShopDetailById(shopId);
	    
	    Integer switchInvoice = 0;
	    if (null != shopDetail && AppUtils.isNotBlank(shopDetail.getSwitchInvoice())) {
	    	switchInvoice = shopDetail.getSwitchInvoice();

		}
	    shopCartList.setSwitchInvoice(switchInvoice);
	    
	    AppUserShopCartOrderDto appUserShopCartOrderDto = convertToAppUserShopCartOrderDto(shopCartList);
	    
		if (PayPctTypeEnum.FRONT_MONEY.value().equals(Integer.parseInt(payType))) { //定金支付
			//获取预售订单定金金额
			PreSellShopCartItem preSellShopCartItem = (PreSellShopCartItem) shopCartList.getShopCarts().get(0).getCartItems().get(0);
			Double prePrice = preSellShopCartItem.getPrePrice();
			Double percent = preSellShopCartItem.getPreDepositPrice()/100;
			Double depositPrice = prePrice*percent;
			Double preDepositPriceAmount = depositPrice * preSellShopCartItem.getBasketCount();
			BigDecimal bigDecimal = new BigDecimal(preDepositPriceAmount).setScale(2, RoundingMode.HALF_UP);
			appUserShopCartOrderDto.setPdAmount(bigDecimal.doubleValue());
			//预售商品单价
//			double preProdCash =  Arith.div(appUserShopCartOrderDto.getPdAmount(), (double)shopCartList.getOrderTotalQuanlity(), 2);
			bigDecimal = new BigDecimal(depositPrice).setScale(2, RoundingMode.HALF_UP);
			appUserShopCartOrderDto.setPreProdCash(bigDecimal.doubleValue());
		} else { //全额支付
			//预售商品单价
			double preProdCash =  Arith.div(appUserShopCartOrderDto.getOrderTotalCash(), (double)shopCartList.getOrderTotalQuanlity(), 2);
			appUserShopCartOrderDto.setPreProdCash(preProdCash);
		}
		appUserShopCartOrderDto.setPayPctType(Long.valueOf(payType)); //预售支付方式

		return appUserShopCartOrderDto;
	}
	
	private AppUserShopCartOrderDto convertToAppUserShopCartOrderDto(UserShopCartList userShopCartList){
		
		AppUserShopCartOrderDto userShopCartListDto = new AppUserShopCartOrderDto();
		userShopCartListDto.setUserId(userShopCartList.getUserId());
		userShopCartListDto.setUserName(userShopCartList.getUserName());
		userShopCartListDto.setType(userShopCartList.getType());
		userShopCartListDto.setInvoiceId(userShopCartList.getInvoiceId());
		userShopCartListDto.setDefaultInvoice(userShopCartList.getDefaultInvoice());
		userShopCartListDto.setPayManner(userShopCartList.getPayManner());
		userShopCartListDto.setPdAmount(userShopCartList.getPdAmount());
		userShopCartListDto.setOrderActualTotal(userShopCartList.getOrderActualTotal());
		userShopCartListDto.setOrderTotalCash(userShopCartList.getOrderTotalCash());
		userShopCartListDto.setOrderTotalQuanlity(userShopCartList.getOrderTotalQuanlity());
		userShopCartListDto.setOrderTotalVolume(userShopCartList.getOrderTotalVolume());
		userShopCartListDto.setOrderTotalWeight(userShopCartList.getOrderTotalWeight());
		userShopCartListDto.setOrderFreightAmount(userShopCartList.getOrderCurrentFreightAmount());
		userShopCartListDto.setAllDiscount(userShopCartList.getAllDiscount());

		userShopCartListDto.setIsCod(userShopCartList.isCod());
		userShopCartListDto.setUserAddress(userShopCartList.getUserAddress());
		userShopCartListDto.setOrderToken(userShopCartList.getOrderToken());
		userShopCartListDto.setActiveId(userShopCartList.getActiveId());
		userShopCartListDto.setAddNumber(userShopCartList.getAddNumber());
		userShopCartListDto.setDefaultInvoice(userShopCartList.getDefaultInvoice());
		userShopCartListDto.setSwitchInvoice(userShopCartList.getSwitchInvoice());
		List<ShopCartOrderDto> shopCartsDto = convertShopCartOrderDto(userShopCartList.getShopCarts()); //订单是按店铺区分， 对应的店铺的购物车信息。
		userShopCartListDto.setShopCarts(shopCartsDto);
/*
		Double couponOffPrice = shopCartsDto.stream().mapToDouble(s -> s.getCouponOffPrice()).sum();
		Double redpackOffPrice = shopCartsDto.stream().mapToDouble(s -> s.getRedpackOffPrice()).sum();
		userShopCartListDto.setCouponOffPrice(Arith.add(couponOffPrice,redpackOffPrice));*/
		userShopCartListDto.setCouponOffPrice(userShopCartList.getCouponOffPrice());
		
		return userShopCartListDto;
	}

	private List<ShopCartOrderDto> convertShopCartOrderDto(List<ShopCarts> shopCartList) {
		if(AppUtils.isNotBlank(shopCartList)){
			List<ShopCartOrderDto> dtoList = new ArrayList<ShopCartOrderDto>(shopCartList.size());
			for (ShopCarts shopCarts : shopCartList) {
				ShopCartOrderDto shopCartsDto = new ShopCartOrderDto();
				shopCartsDto.setShopId(shopCarts.getShopId());
				shopCartsDto.setShopName(shopCarts.getShopName());
				shopCartsDto.setShopStatus(shopCarts.getShopStatus());
				shopCartsDto.setShopTotalCash(shopCarts.getShopTotalCash());
				shopCartsDto.setShopActualTotalCash(shopCarts.getShopActualTotalCash());
				shopCartsDto.setShopTotalWeight(shopCarts.getShopTotalWeight());
				shopCartsDto.setShopTotalVolume(shopCarts.getShopTotalVolume());
				shopCartsDto.setTotalcount(shopCarts.getTotalcount());
				shopCartsDto.setDiscountPrice(shopCarts.getDiscountPrice());
				shopCartsDto.setFreightAmount(shopCarts.getFreightAmount());
				shopCartsDto.setIsFreePostage(shopCarts.isFreePostage());
				shopCartsDto.setMailfeeStr(shopCarts.getMailfeeStr());
				shopCartsDto.setTotalIntegral(shopCarts.getTotalIntegral());
				if(AppUtils.isNotBlank(shopCarts.getTransfeeDtos())){
					List<TransfeeDto>  transfeeDtos=new ArrayList<TransfeeDto>(shopCarts.getTransfeeDtos().size());
					for(TransfeeDto dto:shopCarts.getTransfeeDtos()){
						TransfeeDto transfeeDto=new TransfeeDto();
						transfeeDto.setDeliveryAmount(dto.getDeliveryAmount());
						transfeeDto.setDesc(dto.getDesc());
						transfeeDto.setFreightMode(dto.getFreightMode());
						transfeeDtos.add(transfeeDto);
					}
					shopCartsDto.setTransfeeDtos(transfeeDtos);
				}
			
				shopCartsDto.setCurrentSelectTransfee(shopCarts.getCurrentSelectTransfee());
				shopCartsDto.setRemark(shopCarts.getRemark());
				shopCartsDto.setSupportStore(shopCarts.isSupportStore());
				shopCartsDto.setMailfeeStr(shopCarts.getMailfeeStr());
				shopCartsDto.setCouponOffPrice(shopCarts.getCouponOffPrice());
				shopCartsDto.setRedpackOffPrice(shopCarts.getRedpackOffPrice());
				
				
				/* conver ShopGroupCartItemDto */
				List<MarketingDto> marketingDtoList =  shopCarts.getMarketingDtoList();
				List<ShopGroupCartOrderItemDto> groupCartItems=new ArrayList<ShopGroupCartOrderItemDto>();
				if(AppUtils.isNotBlank(marketingDtoList)){
					for( MarketingDto marketingDto: marketingDtoList){
						ShopGroupCartOrderItemDto cartItemDto=new ShopGroupCartOrderItemDto();
						if(AppUtils.isNotBlank(marketingDto.getType())){
							switch(marketingDto.getType().intValue()){
							   case 0:
								   cartItemDto.setType("满减");
								   break;
							   case 1:
								   cartItemDto.setType("满折");
								   break;
							   case 2:
								   cartItemDto.setType("限时");
								   break;
							    default:
							       cartItemDto.setType("none");
							       break;
							}
						}else{
							 cartItemDto.setType("none");
						}
						cartItemDto.setPromotionInfo(marketingDto.getPromotionInfo());
						List<ShopCartItemDto> shopCartItemDtoList = convertShopCartItemDto(marketingDto.getHitCartItems());
						cartItemDto.setCartItems(shopCartItemDtoList);
						groupCartItems.add(cartItemDto);
					}
				}
				shopCartsDto.setGroupCartItems(groupCartItems);
				
				
				
				dtoList.add(shopCartsDto);
			}
			return dtoList;
		}
		return null;
	}


	private List<ShopCartItemDto> convertShopCartItemDto(List<ShopCartItem> shopCartItemList){
		if(AppUtils.isNotBlank(shopCartItemList)){
			List<ShopCartItemDto> dtoList = new ArrayList<ShopCartItemDto>();
			for (ShopCartItem shopCartItem : shopCartItemList) {
				ShopCartItemDto shopCartItemDto = new ShopCartItemDto();
				/** 购物车ID */
				shopCartItemDto.setBasketId(shopCartItem.getBasketId());
			    /**选中状态 */
				shopCartItemDto.setCheckSts(shopCartItem.getCheckSts());
			    /** 商品ID */
				shopCartItemDto.setProdId(shopCartItem.getProdId());
			    /** shop ID  */
				shopCartItemDto.setShopId(shopCartItem.getShopId());
			    /** skuId */
				shopCartItemDto.setSkuId(shopCartItem.getSkuId());
				/** 商品原价*/
				shopCartItemDto.setPrice(shopCartItem.getPrice());
				/** 促销价格  */
				shopCartItemDto.setPromotionPrice(shopCartItem.getPromotionPrice());
				/** sku图片  */
				shopCartItemDto.setPic(shopCartItem.getPic());
			    /** 购物车商品  */
				shopCartItemDto.setBasketCount(shopCartItem.getBasketCount());
			    /** 商品名称  */
				shopCartItemDto.setProdName(shopCartItem.getProdName());
			    /** sku名称  */
				shopCartItemDto.setSkuName(shopCartItem.getSkuName());
				/** 商品状态 */
				shopCartItemDto.setStatus(shopCartItem.getStatus());
			    /** 库存*/
				shopCartItemDto.setStocks(shopCartItem.getStocks());
			    /** 实际库存*/
				shopCartItemDto.setActualStocks(shopCartItem.getActualStocks());
			    /** 物流重量(千克)  */
				shopCartItemDto.setWeight(shopCartItem.getWeight());
			    /** 物流体积(立方米)  */
			    shopCartItemDto.setVolume(shopCartItem.getVolume());
				 /** 货到付款; 0:普通商品 , 1:货到付款商品   */
			    shopCartItemDto.setIsGroup(shopCartItem.getIsGroup());
			    shopCartItemDto.setIsSupportCod(shopCartItem.isSupportCod());
			    shopCartItemDto.setCnProperties(shopCartItem.getCnProperties());
			    /** 销售属性组合（中文）  */
			    /** 属性Id组合 */
			    shopCartItemDto.setProperties(shopCartItem.getProperties());
			    /** 商品总费用  商品总价格   单价*数量 */
			    shopCartItemDto.setTotal(shopCartItem.getTotal());
			    /** 商品实际总费用  商品总价格  + 运费  - 促销费用*/
			    shopCartItemDto.setTotalMoeny(shopCartItem.getTotalMoeny());
				/** 总重量 */
			    shopCartItemDto.setTotalWeight(shopCartItem.getTotalWeight());
			    shopCartItemDto.setTotalVolume(shopCartItem.getTotalVolume());
				/** -------------------------营销活动信息----------------------  */
				/** 优惠价价格 */
			    shopCartItemDto.setDiscountPrice(shopCartItem.getDiscountPrice());
			    shopCartItemDto.setDiscount(shopCartItem.getDiscount());
			    shopCartItemDto.setStoreIsExist(shopCartItem.getStoreIsExist());//门店自提添加的标识 所选门店是否存在该商品（false为不存在 ）
			    
				dtoList.add(shopCartItemDto);
			}
			return dtoList;
		}
		return null;
	}

}
