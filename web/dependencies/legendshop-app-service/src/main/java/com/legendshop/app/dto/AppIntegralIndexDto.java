package com.legendshop.app.dto;


import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 积分商城首页数据对象
 */
@ApiModel(value="积分商城首页数据对象") 
public class AppIntegralIndexDto {

	/** 用户积分*/
	@ApiModelProperty(value="用户积分") 
	private Integer score;
	
	/** 积分优惠券列表*/
	@ApiModelProperty(value="积分优惠券列表")
	List<AppIntegralCouponListDto> integralCouponList;
	
	/** 积分商品列表*/
	@ApiModelProperty(value="积分商品列表")
	List<AppIntegralProdListDto> integralProdList;

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public List<AppIntegralCouponListDto> getIntegralCouponList() {
		return integralCouponList;
	}

	public void setIntegralCouponList(List<AppIntegralCouponListDto> integralCouponList) {
		this.integralCouponList = integralCouponList;
	}

	public List<AppIntegralProdListDto> getIntegralProdList() {
		return integralProdList;
	}

	public void setIntegralProdList(List<AppIntegralProdListDto> integralProdList) {
		this.integralProdList = integralProdList;
	}
	
}
