package com.legendshop.app.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.legendshop.model.dto.AppSecKillActivityQueryDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.AppActiveBanner;
import com.legendshop.app.dto.AppSeckillActivityListDto;
import com.legendshop.app.dto.AppSeckillDto;
import com.legendshop.app.dto.AppSeckillListDto;
import com.legendshop.app.dto.AppSeckillResultDto;
import com.legendshop.app.service.AppSeckillActivityService;
import com.legendshop.base.util.SeckillConstant;
import com.legendshop.base.util.SeckillStringUtils;
import com.legendshop.framework.cache.client.RedisCacheClient;
import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.seckill.SeckillDto;
import com.legendshop.model.dto.seckill.SeckillExposer;
import com.legendshop.model.dto.seckill.SeckillJsonDto;
import com.legendshop.model.dto.seckill.SeckillSkuDto;
import com.legendshop.model.dto.seckill.SeckillStateEnum;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.SeckillSuccess;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.MyfavoriteService;
import com.legendshop.spi.service.SeckillService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.util.SeckillCachedhandle;
import com.legendshop.util.AppUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * app端秒杀
 */
@Api(tags="秒杀相关接口",value="秒杀相关接口")
@RestController
public class AppSeckillController {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(AppSeckillController.class);
	
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private AppSeckillActivityService appSeckillActivityService;
	
	@Autowired
	private SeckillService seckillService;
	
	@Autowired
	private ShopDetailService shopDetailService;
	
	@Autowired
	private MyfavoriteService myFavoriteService;
	
	@Autowired
	private SeckillCachedhandle seckillCachedhandle;
	
	@Autowired
	private RedisCacheClient redisCacheClient;

	/**
	 * 秒杀商品列表
	 * @param queryDTO
	 * @return the string
	 */
	@ApiOperation(value = "秒杀商品列表", httpMethod = "POST", notes = "秒杀商品列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", dataTypeClass = AppSecKillActivityQueryDTO.class, value = "秒杀活动查询DTO")
	})
	@PostMapping(value="/app/seckillList")
	public ResultDto<AppPageSupport<AppSeckillActivityListDto>> querySeckillProd(AppSecKillActivityQueryDTO queryDTO) {
		try {
			
			AppPageSupport<AppSeckillActivityListDto> ps = appSeckillActivityService.querySeckillActivityList(queryDTO);
			return ResultDtoManager.success(ps);
			
		} catch (Exception e) {
			LOGGER.error("获取秒杀列表失败!", e);
			return ResultDtoManager.fail("获取秒杀列表失败!");
		}
	}
	
	@ApiOperation(value = "秒杀列表时间、状态信息", httpMethod = "POST", notes = "秒杀列表时间、状态信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "query", dataType = "String", name = "startTime", value = "开始时间")
	@PostMapping(value="/app/seckillList/statusInfo")
	public ResultDto<AppSeckillListDto> getSeckillTimeAndStatus(String startTime) {
		try {
			AppSeckillListDto appSeckillListDto = new AppSeckillListDto();
			if(AppUtils.isNotBlank(startTime)){
				//开始、结束时间
				int hour =  Integer.parseInt(startTime);
				appSeckillListDto.setSeckillStartTime(getSeckillStartTime(hour));
				appSeckillListDto.setSeckillEndTime(getSeckillEndTime(hour));
				//状态
				int  nowHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
				if(nowHour < hour){
					appSeckillListDto.setStatus(0);
				}else if(hour <= nowHour && nowHour < hour+3){
					appSeckillListDto.setStatus(1);
				}else {
					appSeckillListDto.setStatus(-1);
				}
			} else {
				//开始、结束时间
				int  nowHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
				appSeckillListDto.setSeckillStartTime(getSeckillStartTime(nowHour));
				appSeckillListDto.setSeckillEndTime(getSeckillEndTime(nowHour));
				//状态
				if(nowHour >= 9 && nowHour < 24){
					appSeckillListDto.setStatus(1);
				}else {
					appSeckillListDto.setStatus(0);
				}
			}
			return ResultDtoManager.success(appSeckillListDto);
		} catch (Exception e) {
			LOGGER.error("获取秒杀列时间、状态信息失败!", e);
			return ResultDtoManager.fail("获取秒杀列时间、状态信息失败!");
		}
	}
	
	
	/**
	 * 获取开始时间
	 * @param hour
	 * @return
	 */
	private Long getSeckillStartTime(int hour){
		Calendar periodStart = Calendar.getInstance();
		periodStart.setTime(new Date());
		periodStart.set(Calendar.SECOND, 0);
		periodStart.set(Calendar.MINUTE, 0);
		periodStart.set(Calendar.MILLISECOND, 0);
		
		if (hour < 9) {
			return null;
		} else if (9 <= hour && hour < 12) {
			periodStart.set(Calendar.HOUR_OF_DAY, 9);
		} else if (12 <= hour && hour < 15) {
			periodStart.set(Calendar.HOUR_OF_DAY, 12);
		} else if (15 <= hour && hour < 18) {
			periodStart.set(Calendar.HOUR_OF_DAY, 15);
		} else if (18 <= hour && hour < 21) {
			periodStart.set(Calendar.HOUR_OF_DAY, 18);
		} else if (21 <= hour && hour < 24) {
			periodStart.set(Calendar.HOUR_OF_DAY, 21);
		} else {
			return null;
		}
		
		return periodStart.getTimeInMillis();
	}
	
	/**
	 * 获取结束时间
	 * @param hour
	 * @return
	 */
	private Long getSeckillEndTime(int hour){
		Calendar periodStart = Calendar.getInstance();
		periodStart.setTime(new Date());
		periodStart.set(Calendar.SECOND, 59);
		periodStart.set(Calendar.MINUTE, 59);
		periodStart.set(Calendar.MILLISECOND, 999);
		
		if (hour < 9) {
			return null;
		} else if (9 <= hour && hour < 12) {
			periodStart.set(Calendar.HOUR_OF_DAY, 11);
		} else if (12 <= hour && hour < 15) {
			periodStart.set(Calendar.HOUR_OF_DAY, 14);
		} else if (15 <= hour && hour < 18) {
			periodStart.set(Calendar.HOUR_OF_DAY, 17);
		} else if (18 <= hour && hour < 21) {
			periodStart.set(Calendar.HOUR_OF_DAY, 20);
		} else if (21 <= hour && hour < 24) {
			periodStart.set(Calendar.HOUR_OF_DAY, 23);
		} else {
			return null;
		}
		
		return periodStart.getTimeInMillis();
	}
	
	/**
	 * @Description 秒杀商品详情
	 * @param seckillId
	 * @return
	 */
	@ApiOperation(value = "秒杀商品详情", httpMethod = "POST", notes = "秒杀商品详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "path", dataType = "Long", name = "seckillId", value = "秒杀活动id")
	@PostMapping(value="/app/seckills/{seckillId}")
	public ResultDto<AppSeckillDto> querySeckillProdDetail(@PathVariable Long seckillId) {
		try {
			//获取秒杀信息
			AppSeckillDto seckill = appSeckillActivityService.getSeckillDetail(seckillId);
			if (AppUtils.isBlank(seckill)) {
				return ResultDtoManager.fail("该秒杀商品已下架");
			}
			
			//获取店铺信息
			ShopDetail shopDetail = shopDetailService.getShopDetailByIdNoCache(seckill.getShopId());
			if(AppUtils.isBlank(shopDetail) || shopDetail.getStatus() != ShopStatusEnum.NORMAL.value()){
				return ResultDtoManager.fail("该秒杀商品已下架");
			}
			seckill.setShopPic(shopDetail.getShopPic());
			seckill.setShopType(shopDetail.getShopType());
			
			//获取店铺评分
			String shopScore = shopDetailService.getshopScore(shopDetail.getShopId());
			seckill.setShopScore(shopScore);
			
			//获取登录用户名
			Boolean isCollect = false;
			LoginedUserInfo userInfo = loginedUserService.getUser();
			if(AppUtils.isNotBlank(userInfo)){
				seckill.setLoginUserName(userInfo.getUserName());
				//用户是否已收藏
				isCollect = myFavoriteService.isExistsFavorite(seckill.getProdDto().getProdId(), userInfo.getUserName());
			}
			seckill.setIsCollect(isCollect);
			
			return ResultDtoManager.success(seckill);
		} catch (Exception e) {
			LOGGER.error("获取秒杀商品详情失败!", e);
			return ResultDtoManager.fail("获取秒杀商品详情失败!");
		}
	}
	
	
	@ApiOperation(value = "执行秒杀", httpMethod = "POST", notes = "执行秒杀",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)	
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "path", dataType = "Long", name = "seckillId", value = "秒杀活动id"),
		@ApiImplicitParam(paramType = "query", dataType = "Long", name = "prodId", value = "商品id"),
		@ApiImplicitParam(paramType = "query", dataType = "Long", name = "skuId", value = "skuId")
	})
	@PostMapping(value = "/p/app/{seckillId}/execute")
	public ResultDto<AppSeckillResultDto> sckillExecute(@PathVariable("seckillId") Long seckillId, @RequestParam Long prodId, @RequestParam Long skuId) {
		
		AppSeckillResultDto appSeckillResultDto = new AppSeckillResultDto();
		
		LoginedUserInfo user = loginedUserService.getUser();
		if (AppUtils.isBlank(user)) {
			return ResultDtoManager.fail(-1, "未登录"); // 返回错误状态码
		}
		
		String userId = user.getUserId();
		
		SeckillDto seckill = seckillService.getSeckillDetail(seckillId);
		// 判断从Redis 获取的秒杀商品是否存在
		if (AppUtils.isBlank(seckill)) {
			return ResultDtoManager.fail("该活动已下线"); // 返回错误状态码
		}
		
		if(userId.equals(seckill.getUserId())){
			// 自己的商品，不能购买
			return ResultDtoManager.fail("不能参与自己的秒杀!"); // 返回错误状态码
		}
		
		Date startTime = seckill.getStartTime();
		Date endTime = seckill.getEndTime();
		Date now = new Date();
		if (now.getTime() >= endTime.getTime()) { // 秒杀结束
			return ResultDtoManager.fail("活动已过期"); // 返回错误状态码
		} else if (now.getTime() < startTime.getTime()) { // 距离活动开始时间
			return ResultDtoManager.fail("未开始"); // 返回错误状态码
		}
		
		/**
		 * 2 限制统一帐号，同一动作，同2秒钟并发次数，超过次不做做动作，返回操作失败
		 */
		String mutex = SeckillStringUtils.cacheKeyJoint(SeckillConstant.MUTEX_KEY_PREFIX, seckillId, userId);
		String mutexresult=redisCacheClient.get(mutex);
		if(AppUtils.isNotBlank(mutexresult)){
			return ResultDtoManager.fail("操作频繁"); // 返回错误状态码
		}
		redisCacheClient.put(mutex, 2, "true");
		
		int flag = seckillCachedhandle.seckillCheck(seckill.getSid(), prodId, skuId, userId); // redis
		if (flag == 2) {
			return ResultDtoManager.fail("重复秒杀"); // 返回错误状态码
		} else if (flag == -3) {
			return ResultDtoManager.fail(SeckillStateEnum.SELL_OUT.getStateinfo()+""); // 返回错误状态码
		} else if (flag == 5) {
			LOGGER.debug("##### 秒杀检查结果成功，结果已经秒杀成功过  ######");
			return ResultDtoManager.fail("已参与过该活动"); // 返回错误状态码
		}
		
		int result = 0;
		try {
			
			SeckillSkuDto skuDto=getSelect(seckill.getProdDto().getSkuDtoList(),skuId);
			
			result = seckillService.executeSeckill(seckillId, prodId, skuId, userId, skuDto.getSeckPrice(), 1);
			
			LOGGER.debug("##### 执行数据库结果 ={}  ######",result);
			
			if(result==-2){ //重复秒杀
				return ResultDtoManager.fail("重复秒杀"); // 返回错误状态码
			}
			
			if(result==0){ //秒杀失败
				LOGGER.debug("##### 执行数据库结果成功,秒杀成功入库失败  ######");
				return ResultDtoManager.fail("系统繁忙,请稍候！"); // 返回错误状态码
			}
			
			LOGGER.debug("##### 执行数据库过程结果成功,秒杀成功入库,设置用户秒杀结果凭证信息  ######");
			SeckillSuccess seckillSuccess=new SeckillSuccess();
			seckillSuccess.setProdId(prodId);
			seckillSuccess.setResult(result);
			seckillSuccess.setSeckillId(seckillId);
			seckillSuccess.setSeckillNum(1);
			//set的应该是秒杀价
			/*seckillSuccess.setSeckillPrice(skuDto.getPrice());*/
			seckillSuccess.setSeckillPrice(skuDto.getSeckPrice());
			seckillSuccess.setSeckillTime(new java.sql.Timestamp(System.currentTimeMillis()));
			seckillSuccess.setSkuId(skuId);
			seckillSuccess.setStatus(0);
			seckillSuccess.setUserId(userId);
			seckillCachedhandle.seckillSuccess(seckillSuccess);
			
			//秒杀成功
			appSeckillResultDto.setSeckillId(seckillId);
			appSeckillResultDto.setStatue(SeckillStateEnum.SUCCESS.getState());
			appSeckillResultDto.setStateInfo(SeckillStateEnum.SUCCESS.getStateinfo());
			return ResultDtoManager.success(appSeckillResultDto);
			
		} catch (Exception e) {
			// 删除缓存标识
			seckillCachedhandle.rollbackSeckill(seckillId, prodId, skuId, userId);
			e.printStackTrace();
			return ResultDtoManager.fail("系统繁忙,请稍候！"); // 返回错误状态码
		} finally {
			if(result!=1){
				// 删除缓存标识
				String user_key = SeckillStringUtils.cacheKeyJoint(SeckillConstant.SECKILL_BUY_USERS_, seckillId, userId, prodId, skuId);
				redisCacheClient.delete(user_key);
				seckillCachedhandle.rollbackSeckill(seckillId, prodId, skuId, userId);
			}
		}
		
	}
	
	private SeckillSkuDto getSelect(List<SeckillSkuDto> skuDtoList, long skuId) {
		for(SeckillSkuDto seckillSkuDto:skuDtoList){
			if(seckillSkuDto.getSkuId().equals(skuId)){
				return seckillSkuDto;
			}
		}
		return null;
	}

	
	/**
	 * @Description 秒杀开关
	 * @param seckillId
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	@ApiOperation(value = "秒杀开关", httpMethod = "POST", notes = "秒杀开关",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)	
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "path", dataType = "String", name = "seckillId", value = "秒杀活动id"),
		@ApiImplicitParam(paramType = "query", dataType = "String", name = "prodId", value = "商品id"),
		@ApiImplicitParam(paramType = "query", dataType = "String", name = "skuId", value = "skuId")
	})
	@PostMapping(value="/app/seckills/{seckillId}/exposeUrl")
	public SeckillJsonDto<SeckillExposer> SckillExpose(@PathVariable("seckillId") long seckillId, @RequestParam long prodId, @RequestParam long skuId) {
		return seckillService.exportSeckillUrl(seckillId, prodId, skuId);
	}
	
	
	/**
	 * 获取秒杀专区的banner图列表
	 * @return
	 */
	@ApiOperation(value = "获取秒杀专区的banner图列表", notes = "获取秒杀专区的banner图列表", httpMethod="POST")
	@PostMapping("/app/seckills/banners")
	public ResultDto<List<AppActiveBanner>> getActiveBanners() {
		try {
			List<AppActiveBanner> result = appSeckillActivityService.getActiveBanners();
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("获取秒杀专区的banner图列表失败!", e);
			return ResultDtoManager.fail("获取秒杀专区的banner图列表失败!");
		}
	}
	
}
