package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("优惠劵商品Dto")
public class AppCouponProdDto {
	@ApiModelProperty(value="商品id")
	private Long prodId;
	
	@ApiModelProperty(value="商品名字")
	private String name;
	
	@ApiModelProperty(value="商品价格")
	private Double price;
	
	@ApiModelProperty(value="评论数")
	private Integer comcount;
	
	@ApiModelProperty(value="图片")
	private String pic;

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getComcount() {
		return comcount;
	}

	public void setComcount(Integer comcount) {
		this.comcount = comcount;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}
	

	
	
	
	
}
