/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

/**
 * 这个类是用于将秒杀的结果进行封装.
 *
 */
public class AppSeckillResultDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/** The seckill id. */
	@ApiModelProperty(value="秒杀活动Id")
	private long seckillId;

	/** 秒杀的的状态. */
	@ApiModelProperty(value="秒杀的的状态")
	private int statue;

	/** 秒杀的输出的信息. */
	@ApiModelProperty(value="秒杀的输出的信息")
	private String stateInfo;

	public long getSeckillId() {
		return seckillId;
	}

	public void setSeckillId(long seckillId) {
		this.seckillId = seckillId;
	}

	public int getStatue() {
		return statue;
	}

	public void setStatue(int statue) {
		this.statue = statue;
	}

	public String getStateInfo() {
		return stateInfo;
	}

	public void setStateInfo(String stateInfo) {
		this.stateInfo = stateInfo;
	}

}
