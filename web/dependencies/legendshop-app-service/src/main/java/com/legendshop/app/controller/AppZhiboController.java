/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.app.dto.AppProdDto;
import com.legendshop.app.dto.ZhiboConfig;
import com.legendshop.model.constant.ErrorNoEnum;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.app.ZhiboResultDto;
import com.legendshop.model.entity.ZhiboAccount;
import com.legendshop.model.entity.ZhiboInteractAvRoom;
import com.legendshop.model.entity.ZhiboNewLiveRecord;
import com.legendshop.model.prod.AppProdListDto;
import com.legendshop.model.prod.AppProdSearchParms;
import com.legendshop.spi.service.AppShopProdManageService;
import com.legendshop.spi.service.ZhiboAccountService;
import com.legendshop.spi.service.ZhiboNewLiveRecordService;
import com.legendshop.util.AppUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 直播注册.
 */
@ApiIgnore
@Api(tags="直播",value="直播相关操作")
@RestController
@Deprecated
public class AppZhiboController{
	
	/** The logger. */
	private final Logger logger = LoggerFactory.getLogger(AppZhiboController.class);
	
	/** T直播账号服务. */
	@Autowired
	private ZhiboAccountService zhiboAccountService;
	
	/** 直播记录服务. */
	@Autowired
	private ZhiboNewLiveRecordService zhiboNewLiveRecordService;
	
	/** 直播商品管理. */
	@Autowired
	private AppShopProdManageService appShopProdManageService;
	
    @Autowired
    private PasswordEncoder passwordEncoder;

	/**
	 * 注册.
	 *
	 * @param request the request
	 * @param response the response
	 * @param id the id
	 * @param pwd the pwd
	 * @return the zhibo result dto
	 */
	@ApiOperation(value = "注册", httpMethod = "POST", notes = "注册",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "id", value = "id", required = false, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "pwd", value = "密码", required = false, dataType = "string")
	})
	@PostMapping(value="/account/regist")
	public ZhiboResultDto regist(HttpServletRequest request,HttpServletResponse response,String id,String pwd) {
		
		ZhiboResultDto zhiboResultDto = new ZhiboResultDto();
		
		if(AppUtils.isBlank(id) || AppUtils.isBlank(pwd)){
			logger.warn("regist parameter can not empty");
	        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_REGISTER_USER_EXIST.value());
	        zhiboResultDto.setErrorInfo("参数不能为空");
			return zhiboResultDto;
		}
		ZhiboAccount zhiboAccount = zhiboAccountService.getZhiboAccountByType(id,ZhiboConfig.TYPE);
        

		if(AppUtils.isNotBlank(zhiboAccount)){
			logger.warn("regist user had exists. id ={}", id);
	        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_REGISTER_USER_EXIST.value());
	        zhiboResultDto.setErrorInfo("注册用户已存在");
			return zhiboResultDto;
		}else{
			Date registerTime = new Date();
			zhiboAccount = new ZhiboAccount();
			zhiboAccount.setId(id);
			zhiboAccount.setState(true);
			zhiboAccount.setUid(id);
			zhiboAccount.setPwd(passwordEncoder.encode(pwd));
			zhiboAccount.setRegisterTime(registerTime);

			int result = zhiboAccountService.insertZhiboAccount(zhiboAccount);
			if(result<0){
		        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_SUCCESS.value());
		        zhiboResultDto.setErrorInfo("");
				return zhiboResultDto;
			}
		}
        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_SUCCESS.value());
        zhiboResultDto.setErrorInfo("");
		return zhiboResultDto;
	}
	
/*	*//**
	 * 登录.
	 *
	 * @param request the request
	 * @param response the response
	 * @param id the id
	 * @param pwd the pwd
	 * @return the zhibo result dto
	 *//*
	@ApiOperation(value = "登录", httpMethod = "POST", notes = "登录",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "id", value = "id", required = false, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "pwd", value = "密码", required = false, dataType = "string")
	})
	@PostMapping(value="/account/login")
	public ZhiboResultDto login(HttpServletRequest request,HttpServletResponse response,String id,String pwd){
		 ZhiboResultDto zhiboResultDto = new ZhiboResultDto();
		if(AppUtils.isBlank(id) || AppUtils.isBlank(pwd)){
			logger.warn("login without parameter. id ={}", id);
	        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_REGISTER_USER_EXIST.value());
	        zhiboResultDto.setErrorInfo("参数不能为空");
			return zhiboResultDto;
		}
		try{
		ZhiboAccount zhiboAccount = zhiboAccountService.getZhiboAccountByType(id, ZhiboConfig.TYPE);
		
		if(AppUtils.isNotBlank(zhiboAccount)){
	        //校验密码
	        if(passwordEncoder.matches(pwd, zhiboAccount.getPwd())){
	        GenTLSSignatureResult result = tls_sigature.GenTLSSignatureEx(ZhiboConfig.sdkAppId, id, ZhiboConfig.privateKey);
	        
	        Date LoginDate = new Date();
	        String token = id; // + LoginDate.getTime(); 去掉时间 id = user_name
	        String compressToken = BASE64Encoder.encode(token.getBytes());
	        
	        logger.info("generate token: {}, compressToken: {}, sig: {}",id, compressToken,  result.urlSig);
	        
	        JSONObject data = new JSONObject();
	        data.put("userSig", result.urlSig);
	        data.put("token", compressToken);
	        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_SUCCESS.value());
	        zhiboResultDto.setData(data);
	        
	        zhiboAccount.setLastRequestTime(LoginDate);
	        zhiboAccount.setLoginTime(LoginDate);
	        zhiboAccount.setToken(compressToken);
	        zhiboAccount.setUserSig(result.urlSig);
	        zhiboAccountService.loginUpdate(zhiboAccount);
	        
	        return zhiboResultDto;
	        }else{
	        	logger.info("User password dismatch, Id = {}", id);
	        	zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_PASSWORD.value());
	        	zhiboResultDto.setErrorInfo("密码不匹配");
		        return zhiboResultDto;
	        }
		}else{
			logger.info("User does not exist, Id = {}", id);
        	zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_USER_NOT_EXIST.value());
        	zhiboResultDto.setErrorInfo("用户不存在");
	        return zhiboResultDto;
		}
		}catch (Exception e){
			logger.error("",e);
	        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_SERVER.value());
	        zhiboResultDto.setErrorInfo("服务器错误");
			return zhiboResultDto;
		}
	}*/
	
	/**
	 * 查询房间列表.
	 *
	 * @param token the token
	 * @param type the type
	 * @param index the index
	 * @param size the size
	 * @return the zhibo result dto
	 */
	@ApiOperation(value = "查询房间列表", httpMethod = "POST", notes = "查询房间列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "token", value = "token", required = false, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "type", value = "类型", required = false, dataType = "Integer"),
		@ApiImplicitParam(paramType="query", name = "index", value = "index", required = false, dataType = "Integer"),
		@ApiImplicitParam(paramType="query", name = "size", value = "size", required = false, dataType = "Integer")
	})
	@PostMapping(value="/live/roomlist")
	public ZhiboResultDto roomlist (String token, Integer type, Integer index, Integer size	) {
		ZhiboResultDto zhiboResultDto = new ZhiboResultDto();
		if(AppUtils.isBlank(token) || AppUtils.isBlank(index) || AppUtils.isBlank(size)){
			logger.warn("roomlist without parameter. token ={}", token);
	        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_REGISTER_USER_EXIST.value());
	        zhiboResultDto.setErrorInfo("参数不能为空");
			return zhiboResultDto;
		}
		ZhiboAccount zhiboAccount = zhiboAccountService.getAccountByToken(token,ZhiboConfig.TYPE);
		if(AppUtils.isNotBlank(zhiboAccount)){
			// 获得房间列表的集合
			List<ZhiboNewLiveRecord> list = zhiboNewLiveRecordService.getRoomList(index, size);
			
			if (AppUtils.isNotBlank(list)) {
				JSONObject data = new JSONObject();
				data.put("total", list.size());
				data.put("rooms", list);

		        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_SUCCESS.value());
				zhiboResultDto.setErrorInfo("");
				zhiboResultDto.setData(data);
				return zhiboResultDto;
			}else{
		        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_SUCCESS.value());
				zhiboResultDto.setErrorInfo("");
				return zhiboResultDto;
			}
		}else{
			logger.warn("roomlist can not find room by token ={}", token);
	        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_INVALID_REQ.value());
	        zhiboResultDto.setErrorInfo("找不到直播帐号");
			return zhiboResultDto;
		}

	}
	
	
	/**
	 * 通过房间id查找房间成员.
	 *
	 * @param roomnum the roomnum
	 * @param currPageNo the curr page no
	 * @return the result dto
	 */
	@ApiOperation(value = "查找房间成员", httpMethod = "POST", notes = "通过房间id查找房间成员,返回AppProdDto礼品的list集合",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "roomnum", value = "房间号", required = false, dataType = "Integer"),
		@ApiImplicitParam(paramType="query", name = "currPageNo", value = "分页", required = false, dataType = "Integer")
	})
	@PostMapping(value="/zhibo/shopProdListByRoomnum")
	public ResultDto<AppProdListDto> shopProdListByRoomnum(Integer roomnum, Integer currPageNo) {
		
		AppProdSearchParms searchParms = new AppProdSearchParms();
		searchParms.setCurPageNO(currPageNo);
		searchParms.setPageSize(10);
		
		AppProdListDto appProdListDto = appShopProdManageService.getAppProdListByRoomnum(searchParms, roomnum, currPageNo.toString());
		return ResultDtoManager.success(appProdListDto);
	}
	
	/**
	 * 用户进入、退出房间.
	 *
	 * @param token the token
	 * @param roomnum the roomnum
	 * @param role the role
	 * @param operate the operate
	 * @return the zhibo result dto
	 * @throws ParseException the parse exception
	 */
	@ApiOperation(value = "用户进入退出房间", httpMethod = "POST", notes = "用户进入、退出房间",produces = MediaType.APPLICATION_JSON_UTF8_VALUE,response=AppProdDto.class)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "token", value = "token", required = false, dataType = "String"),
		@ApiImplicitParam(paramType="query", name = "roomnum", value = "房间号", required = false, dataType = "Integer"),
		@ApiImplicitParam(paramType="query", name = "role", value = "角色", required = false, dataType = "Integer"),
		@ApiImplicitParam(paramType="query", name = "operate", value = "操作", required = false, dataType = "Integer")
	})
	@PostMapping(value="/live/reportmemid")
	public ZhiboResultDto reportmemid(HttpServletRequest request,HttpServletResponse response,String token, Integer roomnum, Integer role, Integer operate) throws ParseException {
		logger.error("reportmemid is calling token: {}, roomnum {}, role:{}, operate: {}, remote address {}" , token, roomnum,role, operate,request.getRemoteAddr());
		ZhiboResultDto zhiboResultDto = new ZhiboResultDto();
		ZhiboAccount zhiboAccount = zhiboAccountService.getAccountByToken(token,ZhiboConfig.TYPE);
		if (AppUtils.isNotBlank(zhiboAccount)) {
			// 先判断有无此房间（通过roomnum查找）
			ZhiboNewLiveRecord room  = zhiboNewLiveRecordService.getRoomById(roomnum);
			if (room == null) {
		        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_AV_ROOM_NOT_EXIST.value());
		        zhiboResultDto.setErrorInfo("房间号错误");
				return zhiboResultDto;
			} else {
				// 成员进入房间
				if (operate == 0) {
					// 判断该成员是否已经在房间内了
					if (!zhiboNewLiveRecordService.getInfoByUid(zhiboAccount.getUid()).isEmpty()) {
						zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_REGISTER_USER_EXIST.value());
						zhiboResultDto.setErrorInfo("成员已存在该房间，请重试！！！");
						return zhiboResultDto;
					} else {
						// 通过operate判断成员是进入房间还是退出房间（0：进入 1：退出）
						// 获得当前时间
						Date date = new Date();
						SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String nowDate = dateFormat.format(date);
						// 如果返回1，则成功进入房间
						if (zhiboNewLiveRecordService.enterRoom(zhiboAccount.getUid(), roomnum, "off", nowDate, role) == 1) {
							zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_SUCCESS.value());
							zhiboResultDto.setErrorInfo("成功进入");
							return zhiboResultDto;
						} else {
							zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_SERVER.value());
							zhiboResultDto.setErrorInfo("服务器错误");
							return zhiboResultDto;
						}
					}
				// 成员退出房间
				} else if (operate == 1) {
					// 判断该成员是否在房间内
					if (zhiboNewLiveRecordService.getInfoByUid(zhiboAccount.getUid()).isEmpty()) {
						zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_REGISTER_USER_EXIST.value());
						zhiboResultDto.setErrorInfo("成员已不在该房间，请重试！！！");
						return zhiboResultDto;
					} else {
						int ret = zhiboNewLiveRecordService.exitRoom(zhiboAccount.getUid());
						if ( ret == 1) {
							zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_SUCCESS.value());
							zhiboResultDto.setErrorInfo("成功退出");
							return zhiboResultDto;
						} else {
							zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_SERVER.value());
							zhiboResultDto.setErrorInfo("服务器错误");
							return zhiboResultDto;
						}
					}
				// 当operate参数输入不是0、1时返回：参数输入错误
				} else {
					zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_INVALID_REQ.value());
					zhiboResultDto.setErrorInfo("参数operate输入错误，只能输入0、1");
					return zhiboResultDto;
				}
			}
		}
        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_INVALID_REQ.value());
        zhiboResultDto.setErrorInfo("reportmemid 找不到直播账号 " + token);
		return zhiboResultDto;
	}
	
	/**
	 * 查询房间成员列表.
	 *
	 * @param request the request
	 * @param response the response
	 * @param token the token
	 * @param roomnum the roomnum
	 * @param index the index
	 * @param size the size
	 * @return the zhibo result dto
	 */
	@ApiOperation(value = "查询房间成员列表", httpMethod = "POST", notes = "查询房间成员列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "token", value = "token", required = false, dataType = "String"),
		@ApiImplicitParam(paramType="query", name = "roomnum", value = "房间号", required = false, dataType = "Integer"),
		@ApiImplicitParam(paramType="query", name = "index", value = "index", required = false, dataType = "Integer"),
		@ApiImplicitParam(paramType="query", name = "size", value = "size", required = false, dataType = "Integer")
	})
	@PostMapping(value="/live/roomidlist")
	public ZhiboResultDto roomidlist(HttpServletRequest request,HttpServletResponse response, String token, Integer roomnum, Integer index, Integer size){
		if(AppUtils.isBlank(token) || AppUtils.isBlank(roomnum) || AppUtils.isBlank(index) || AppUtils.isBlank(size)){
	        ZhiboResultDto zhiboResultDto = new ZhiboResultDto();
	        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_REGISTER_USER_EXIST.value());
	        zhiboResultDto.setErrorInfo("参数不能为空");
			return zhiboResultDto;
		}
		ZhiboAccount zhiboAccount = zhiboAccountService.getAccountByToken(token,"user");
		ZhiboResultDto zhiboResultDto = new ZhiboResultDto();
		if(AppUtils.isNotBlank(zhiboAccount)){
			// 先判断有无此房间（通过roomnum查找）
			ZhiboNewLiveRecord room = zhiboNewLiveRecordService.getRoomById(roomnum);
			if (room == null) {
		        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_AV_ROOM_NOT_EXIST.value());
		        zhiboResultDto.setErrorInfo("房间号错误");
				return zhiboResultDto;
			} else {
				// 获取房间内的成员
				List<ZhiboInteractAvRoom> memberList = zhiboNewLiveRecordService.getRoomidList(index, size, roomnum);
				if (memberList.isEmpty() || memberList.size() == 0) {
			        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_USER_NOT_EXIST.value());
			        zhiboResultDto.setErrorInfo("此房间暂无用户观看");
					return zhiboResultDto;
				} else {
					JSONObject data = new JSONObject();
					data.put("total", memberList.size());
					data.put("idlist", memberList);

					zhiboResultDto.setData(data);
					zhiboResultDto.setErrorInfo("查询成功！！！");
					zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_SUCCESS.value());
					return zhiboResultDto;
				}
			}
		}
        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_INVALID_REQ.value());
        zhiboResultDto.setErrorInfo("roomidlist 找不到直播账号 " + token);
		return zhiboResultDto;
	}
	
	/**
	 * 退出正在直播的房间.
	 *
	 * @param request the request
	 * @param response the response
	 * @param token the token
	 * @param roomnum the roomnum
	 * @param type the type
	 * @return the zhibo result dto
	 */
	@ApiOperation(value = "退出正在直播的房间", httpMethod = "POST", notes = "退出正在直播的房间",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "token", value = "token", required = false, dataType = "String"),
		@ApiImplicitParam(paramType="query", name = "roomnum", value = "房间号", required = false, dataType = "Integer"),
		@ApiImplicitParam(paramType="query", name = "type", value = "类型", required = false, dataType = "Integer")
	})
	@PostMapping(value="/live/exitroom")
	public ZhiboResultDto exitroom(HttpServletRequest request,HttpServletResponse response,String token, Integer roomnum, Integer type){
		if(AppUtils.isBlank(token) || AppUtils.isBlank(roomnum) || AppUtils.isBlank(type)){
	        ZhiboResultDto zhiboResultDto = new ZhiboResultDto();
	        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_REGISTER_USER_EXIST.value());
	        zhiboResultDto.setErrorInfo("参数不能为空");
			return zhiboResultDto;
		}
		
		ZhiboAccount zhiboAccount = zhiboAccountService.getZhiboAccountByType(token, ZhiboConfig.TYPE);
		if(AppUtils.isNotBlank(zhiboAccount)){
			try {
				
			} catch (Exception e) {
				ZhiboResultDto zhiboResultDto = new ZhiboResultDto();
		        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_SERVER.value());
		        zhiboResultDto.setErrorInfo("服务器错误");
				return zhiboResultDto;
			}
			
		}
		ZhiboResultDto zhiboResultDto = new ZhiboResultDto();
        zhiboResultDto.setErrorCode(ErrorNoEnum.ERR_SUCCESS.value());
        zhiboResultDto.setErrorInfo("");
		return zhiboResultDto;
	}

}
