package com.legendshop.app.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/** 商品属性 **/
@ApiModel("商品属性")
public class AppProductPropertyDto  implements Serializable{

	private static final long serialVersionUID = -6991917665604921238L;
	
	/** 属性ID **/
	@ApiModelProperty(value = "属性ID")
	private Long propId;

	/** 属性名称 **/
	@ApiModelProperty(value = "属性名称")
	private String propName;

	/** 商品属性值 **/
	@ApiModelProperty(value = "商品属性值列表")
	private List<AppProductPropertyValueDto> prodPropValList;


	public Long getPropId() {
		return propId;
	}

	public void setPropId(Long propId) {
		this.propId = propId;
	}

	public String getPropName() {
		return propName;
	}

	public void setPropName(String propName) {
		this.propName = propName;
	}

	public List<AppProductPropertyValueDto> getProdPropValList() {
		return prodPropValList;
	}

	public void setProdPropValList(List<AppProductPropertyValueDto> prodPropValList) {
		this.prodPropValList = prodPropValList;
	}



}
