package com.legendshop.app.controller;

import com.legendshop.app.service.AppInvoiceService;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.InvoiceDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.Invoice;
import com.legendshop.spi.service.InvoiceService;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.MethodUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * app发票管理控制器
 * @author  linzh
 */
@RestController
@RequestMapping(value = "/p/app")
@Api(tags="发票管理",value="发票管理")
public class AppInvoiceController {


    /** The log. */
    private final org.slf4j.Logger Logger = LoggerFactory.getLogger(AppInvoiceController.class);


    @Autowired
    private AppInvoiceService appInvoiceService;

    @Autowired
    private InvoiceService invoiceService;

    /**
     * 获取已经登录的用户信息
     */
    @Autowired
    private LoginedUserService loginedUserService;


    /**
     * 发票列表
     * @param invoiceType 发票类型
     * @param curPageNo 当前页码
     */
    @ApiOperation(value = "发票列表", httpMethod = "POST", notes = "发票列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)

    @ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "invoiceType", value = "发票类型", required = true, dataType = "Integer"),
        @ApiImplicitParam(paramType="query", name = "curPageNo", value = "当前页码", required = false, dataType = "String")
    })
    @PostMapping(value="/invoicePage")
    public ResultDto<AppPageSupport<InvoiceDto>> invoicePage(Integer invoiceType, String curPageNo) {

        String userName=loginedUserService.getUser().getUserName();
        if(AppUtils.isBlank(userName)){
            return ResultDtoManager.fail(0, "userName is null");
        }

        AppPageSupport<InvoiceDto> ps = appInvoiceService.queryInvoice(userName,invoiceType,curPageNo);
        return ResultDtoManager.success(ps);
    }


    @ApiOperation(value = "删除发票", httpMethod = "POST", notes = "删除发票",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParam(paramType="query", name = "invoiceId", value = "发票id", required = true, dataType = "Long")
    @PostMapping(value="/invoiceDelete")
    public ResultDto<Object> invoiceDelete(Long invoiceId) {

        Invoice invoice = invoiceService.getInvoice(invoiceId);
        if (AppUtils.isBlank(invoice)){

            return ResultDtoManager.fail(-1, "该发票不存在或已被删除！");
        }

        appInvoiceService.delById(invoiceId);
        return ResultDtoManager.success();
    }

    /**
     * 设置发票为默认
     */
    @ApiOperation(value = "设置发票为默认", httpMethod = "POST", notes = "设置发票为默认",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParam(paramType="query", name = "invoiceId", value = "发票id", required = true, dataType = "Long")
    @PostMapping(value="/setInvoiceCommon")
    public ResultDto<Object> setInvoiceCommon(Long invoiceId){

        Invoice invoice = invoiceService.getInvoice(invoiceId);
        if(null == invoice) {

            return ResultDtoManager.fail(-1, "该发票不存在或已被删除！");
        }

        String userName=loginedUserService.getUser().getUserName();
        if(AppUtils.isBlank(userName)){
            return ResultDtoManager.fail(0, "userName is null");
        }

        invoiceService.updateDefaultInvoice(invoiceId,userName);
        return ResultDtoManager.success();
    }


    /**
     * 加载发票信息
     */
    @ApiOperation(value = "加载发票信息", httpMethod = "POST", notes = "加载发票信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParam(paramType="query", name = "invoiceId", value = "发票ID", required = true, dataType = "Long")
    @PostMapping("/loadInvoice")
    public ResultDto<InvoiceDto> loadInvoice(Long invoiceId){
        if(invoiceId == null){
            return ResultDtoManager.fail(0, "invoiceId is null");
        }

        String userId = loginedUserService.getUser().getUserId();
        if(AppUtils.isBlank(userId)){
            return ResultDtoManager.fail(0, "userId is null");
        }

        InvoiceDto result = appInvoiceService.getInvoiceDto(userId,invoiceId);
        return ResultDtoManager.success(result);
    }




    /**
     * 保存或更新发票信息
     * @param invoiceDto
     * @return
     */
    @ApiOperation(value = "保存或更新发票信息", httpMethod = "POST", notes = "保存或更新发票信息，以发票id为标识",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping(value="/invoice/save")
    public ResultDto<InvoiceDto> saveInvoice(InvoiceDto invoiceDto){
        LoginedUserInfo appToken = loginedUserService.getUser();
        String userName = appToken.getUserName();
        String userId = appToken.getUserId();

        if(AppUtils.isBlank(invoiceDto)){
            return ResultDtoManager.fail(-1, "对不起, 请求参数有误!");
        }

        try {

            Invoice invoice = convertToInvoice(invoiceDto);
            invoice.setUserId(userId);
            invoice.setUserName(userName);
            invoice.setCreateTime(new Date());

            // 保存或更新发票
            Long invoiceId = invoiceService.saveInvoice(invoice);

            //检查当前保存的发票是否选择设置为默认
            if(invoice.getCommonInvoice().equals(1)){

                invoiceService.updateDefaultInvoice(invoiceId,userName);
            }
            return ResultDtoManager.success(invoiceDto);
        } catch (Exception e) {
            Logger.error("发票保存更新失败:"+e.getMessage());
            return ResultDtoManager.fail(-1, "发票保存更新失败");
        }
    }


    /**
     * 转换类方法
     * @param invoiceDto
     * @return
     */
    private Invoice convertToInvoice(InvoiceDto invoiceDto){
        Invoice invoice = new Invoice();
        invoice.setDepositBank(invoiceDto.getDepositBank());
        invoice.setRegisterPhone(invoiceDto.getRegisterPhone());
        invoice.setInvoiceHumNumber(invoiceDto.getInvoiceHumNumber());
        invoice.setType(invoiceDto.getType());
        invoice.setUserName(invoiceDto.getUserName());
        invoice.setTitle(invoiceDto.getTitle());
        invoice.setUserId(invoiceDto.getUserId());
        invoice.setContent(invoiceDto.getContent());
        invoice.setCommonInvoice(invoiceDto.getCommonInvoice());
        invoice.setRegisterAddr(invoiceDto.getRegisterAddr());
        invoice.setCompany(invoiceDto.getCompany());
        invoice.setId(invoiceDto.getId());
        invoice.setBankAccountNum(invoiceDto.getBankAccountNum());
        return invoice;
    }

}
