package com.legendshop.app.service;

import com.legendshop.app.dto.AppOrderDetailsParamsDto;
import com.legendshop.app.dto.AppUserShopCartOrderDto;
import com.legendshop.model.dto.buy.UserShopCartList;

public interface AppCartOrderService {
	
	/**
	 * 获取用户的购物车下单信息
	 */
	public abstract AppUserShopCartOrderDto getShoppingCartOrderDto(UserShopCartList shopCartList);

	/**
	 * 获取用户拼团下单信息
	 * @param shopCartList 下单参数
	 * @param reqParams 请求参数
	 * @param userId TODO
	 * @return
	 */
	public abstract AppUserShopCartOrderDto getMergeGroupShoppingCartOrderDto(UserShopCartList shopCartList, AppOrderDetailsParamsDto reqParams, String userId);
	
	/**
	 * 获取用户秒杀下单信息
	 * @param shopCartList
	 * @param userId
	 * @return
	 */
	public abstract AppUserShopCartOrderDto getSeckillShoppingCartOrderDto(UserShopCartList shopCartList,AppOrderDetailsParamsDto reqParams, String userId);
	
	/**
	 * 获取用户预售下单信息
	 * @param shopCartList
	 * @param userId
	 * @return
	 */
	public abstract AppUserShopCartOrderDto getPresellShoppingCartOrderDto(UserShopCartList shopCartList, String userId, String payType,Long invoiceId);
	
}
