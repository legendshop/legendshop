package com.legendshop.app.service;

import com.legendshop.model.dto.app.InvoiceSubDto;
import com.legendshop.model.dto.app.AppOrderDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.AppSuccessOrderDto;
import com.legendshop.model.dto.order.OrderSearchParamDto;

/**
 * app 订单service
 */
public interface AppOrderService {

	/**
	 * 获取我的订单列表
	 * @param paramDto
	 * @return
	 */
	public AppPageSupport<AppOrderDto> getMyOrderDtos(OrderSearchParamDto paramDto);

	/**
	 * 获取订单详情
	 * @param subNumber
	 * @return
	 */
	public AppOrderDto findOrder(String subNumber);

	/**
	 * 获取发票信息
	 * @param invoiceSubId
	 * @return
	 */
	public InvoiceSubDto loadInvoice(Long invoiceSubId);

	/**
	 * 获取我的订单详情
	 * @param subNumber
	 * @param userId
	 * @return
	 */
	public AppOrderDto findOrderDetail(String subNumber, String userId);

	/**
	 * 通过清算单号获取订单详情
	 */
	public AppSuccessOrderDto findOrderDetailBySubSettlementSn(String subSettlementSn, String userId);
	
	
}
