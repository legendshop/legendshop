/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app团购下单提交参数
 *
 */
@ApiModel(value="app团购下单提交参数")
public class SubmitGroupParam {

	/** 团购id */
	@ApiModelProperty(value="团购id",required=true)
	private Long groupId;

	/** 团购加密Token. */
	@ApiModelProperty(value="团购加密Token",required=true)
	private String token;
	
	/** 商品id */
	@ApiModelProperty(value="商品id",required=true)
	private Long productId;
	
	/** skuId */
	@ApiModelProperty(value="skuId",required=true)
	private Long skuId;
	
	/** 给卖家留言 */
	@ApiModelProperty(value="给卖家留言")
	private String remarkText;
	
	/** 发票Id */
	@ApiModelProperty(value="发票Id")
	private Long invoiceId;
	
	/** 运费模板 */
	@ApiModelProperty(value="运费模板",required=true)
	private String delivery;
	
	/** 发票开关 */
	@ApiModelProperty(value="发票开关,1为开启,0为不开启",required=true)
	private Integer switchInvoice;
	
	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public String getRemarkText() {
		return remarkText;
	}

	public void setRemarkText(String remarkText) {
		this.remarkText = remarkText;
	}

	public Long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getDelivery() {
		return delivery;
	}

	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}

	public Integer getSwitchInvoice() {
		return switchInvoice;
	}

	public void setSwitchInvoice(Integer switchInvoice) {
		this.switchInvoice = switchInvoice;
	}
	
}
