/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.util;

import java.net.URLEncoder;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.base.util.WxSignUtil;
import com.legendshop.business.dao.PayTypeDao;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.PayTypeEnum;
import com.legendshop.model.entity.PayType;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SystemUtil;

/**
 * 微信登录助手
 */
@Component
public class WeiXinOAuth2Helper {
	
    private final static Logger log = LoggerFactory.getLogger(WeiXinOAuth2Helper.class);
	
	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;
	
    @Autowired
    private PayTypeDao payTypeDao;

    /** 微信外部的网页授权链接 */
    private String WEIXIN_OPEN_OAUTO_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=APPID";

    /**
     * 进行微信oauth2.0认证
     * 认证流程: 重定向到微信授权认证页面 > 用户授权认证通过后 > 微信服务器callback到我们的微信服务/weixin/oauth这个Controller > 我们微信服务再callback到我们的应用服务器(比如app服务)
     * @param refrenceURL 认证来源URL, 用于认证的最终完成之后需要重定向到这个地址.
     * @return
     */
    public JSONObject oauth2(String refrenceURL) {

        log.info("请求微信服务, 请求前记录来源地址：{}", refrenceURL);
        
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status", false);
        
        //获取微信公众号支付(JS API方式)的 配置, 从中获取APPID
        PayType payType = payTypeDao.getPayTypeById(PayTypeEnum.WX_PAY.value());
        if (payType == null) {
            log.error("微信支付方式不存在,请设置支付方式 payTypeId = {} ", PayTypeEnum.WX_PAY.value());
            return null;
        }
        
        if (payType.getIsEnable() == Constants.OFFLINE) {
            log.error("微信支付方式没有启用 payTypeId = {} ", PayTypeEnum.WX_PAY.value());
        }
        
        JSONObject _jsonObject = JSONObject.parseObject(payType.getPaymentConfig());
        String appId = _jsonObject.getString("APPID");

        String open_oauto_url = WEIXIN_OPEN_OAUTO_URL.replaceAll("APPID", appId);
        StringBuilder oautoURL = new StringBuilder();

        String redirect_uri = constructRedirectUrl(refrenceURL);
        try {
            log.info("--------------前往网页授权  --------- redirect_uri = {} ", redirect_uri);
            redirect_uri = URLEncoder.encode(redirect_uri, "UTF-8");
            oautoURL.append(open_oauto_url);                  
            oautoURL.append("&redirect_uri=").append(redirect_uri).append("&response_type=code&scope=snsapi_base&state=patient#wechat_redirect");
            
            log.info("--------------前往网页授权  --------- oautoURL = {} ", oautoURL);

            jsonObject.put("status", true);
            jsonObject.put("oauth2_authorize_url", oautoURL.toString());
            jsonObject.put("message", "用户网页校验成功,需要跳转授权!");
            return jsonObject;

        } catch (Exception e) {
            e.printStackTrace();
            jsonObject.put("message", "用户网页授权失败!");
            return jsonObject;
        }
    }

    /**
     * 构建重定向URL, 
     * @param refrenceURL  认证来源URL, 用于认证的最终完成之后需要重定向到这个地址.
     * @return
     */
    private String constructRedirectUrl(String refrenceURL) {
      
         String oauth2url = propertiesUtil.getUserAppDomainName() + SystemUtil.getContextPath() + "/weixin/oauth2/callback";
         /*
          * 通过内部weixinServce 通过对网页授权验证
          */
         String weixinServerOautoUrl = propertiesUtil.getWeiXinDomainName() + "/weixin/oauth";
        long currentTime = new Date().getTime();
        String validateCode = WxSignUtil.makeSignature(oauth2url, String.valueOf(currentTime), propertiesUtil.getWeiXinKey());
        StringBuilder sb = new StringBuilder();
        sb.append(weixinServerOautoUrl).append("?oauth2url=").append(oauth2url)
                .append("&time=").append(currentTime)
                .append("&validateCode=").append(validateCode);

        if(AppUtils.isNotBlank(refrenceURL))
            sb.append("&refrenceURL=").append(refrenceURL);
        
        return sb.toString();
    }

}
