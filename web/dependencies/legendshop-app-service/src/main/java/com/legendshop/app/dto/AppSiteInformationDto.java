/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 站内信消息详情
 */
@ApiModel(value="消息详情") 
public class AppSiteInformationDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7789163001081755625L;

	@ApiModelProperty(value="消息Id")
	private Long msgId;

	@ApiModelProperty(value="消息标题")
	private String title;
	
	@ApiModelProperty(value="发送人")
	private String sendName;

	@ApiModelProperty(value="内容")
	private String text;

	@ApiModelProperty(value="发送时间")
	private Date recDate;
	
	@ApiModelProperty(value="状态, 0: 未读, 1:已读")
	private Integer status;
	
	public Long getMsgId() {
		return msgId;
	}

	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}

	public String getSendName() {
		return sendName;
	}

	public void setSendName(String sendName) {
		this.sendName = sendName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getRecDate() {
		return recDate;
	}

	public void setRecDate(Date recDate) {
		this.recDate = recDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
