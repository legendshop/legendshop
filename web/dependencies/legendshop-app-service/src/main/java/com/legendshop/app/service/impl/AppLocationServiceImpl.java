package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.AppLocationService;
import com.legendshop.model.KeyValueEntity;
import com.legendshop.model.dto.app.KeyValueDto;
import com.legendshop.spi.service.LocationService;
import com.legendshop.util.AppUtils;

@Service("appLocationService")
public class AppLocationServiceImpl implements AppLocationService {
	
	@Autowired
	private LocationService locationService;

	@Override
	public List<KeyValueDto> loadProvinces() {
		List<KeyValueEntity> provinces = locationService.loadProvinces();
		return convertKeyValueDto(provinces);
	}

	@Override
	public List<KeyValueDto> loadCities(Integer provinceid) {
		List<KeyValueEntity> cities = locationService.loadCities(provinceid);
		return convertKeyValueDto(cities);
	}

	@Override
	public List<KeyValueDto> loadAreas(Integer cityid) {
		List<KeyValueEntity> areas = locationService.loadAreas(cityid);
		return convertKeyValueDto(areas);
	}

	private List<KeyValueDto> convertKeyValueDto(List<KeyValueEntity> keyValueEntityList){
		if(AppUtils.isNotBlank(keyValueEntityList)){
			List<KeyValueDto> keyValueDtoList = new ArrayList<KeyValueDto>();
			for (KeyValueEntity keyValueEntity : keyValueEntityList) {
				KeyValueDto keyValueDto = new KeyValueDto();
				keyValueDto.setValue(keyValueEntity.getValue());
				keyValueDto.setKey(keyValueEntity.getKey());
				keyValueDtoList.add(keyValueDto);
			}
			
			return keyValueDtoList;
		}
		
		return null;
	}
}
