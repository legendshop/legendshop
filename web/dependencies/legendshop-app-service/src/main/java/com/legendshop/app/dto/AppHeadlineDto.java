package com.legendshop.app.dto;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 头条新闻Dto
 */
@ApiModel(value="头条新闻Dto") 
public class AppHeadlineDto implements Serializable{

	/**  */
	private static final long serialVersionUID = -7055925877430698650L;

	/** 主键 */
	@ApiModelProperty(value="主键id") 
	private Long id; 
		
	/** 标题 */
	@ApiModelProperty(value=" 新闻标题") 
	private String title; 
		
	/** 详情 */
	@ApiModelProperty(value="新闻内容详情") 
	private String detail; 
		
	/** 图片 */
	@ApiModelProperty(value="图片") 
	private String pic; 
		
	/** 添加时间 */
	@ApiModelProperty(value="添加时间") 
	private Date addTime; 
		
	/** 顺序 */
	@ApiModelProperty(value="顺序") 
	private Long seq;

	@Override
	public String toString() {
		return "AppHeadlineDto [id=" + id + ", title=" + title + ", detail=" + detail + ", pic=" + pic + ", addTime="
				+ addTime + ", seq=" + seq + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public Long getSeq() {
		return seq;
	}

	public void setSeq(Long seq) {
		this.seq = seq;
	} 
}
