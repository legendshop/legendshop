package com.legendshop.app.dto;

import java.io.Serializable;

import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.integral.IntegraLog;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="AppUserIntegralListDto") 
public class AppUserIntegralListDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="IntegraLog")  
	private AppPageSupport<AppIntegraLogDto> result;
	
	@ApiModelProperty(value="客户参数")  
	private Integer customParam;

	public AppPageSupport<AppIntegraLogDto> getResult() {
		return result;
	}

	public void setResult(AppPageSupport<AppIntegraLogDto> result) {
		this.result = result;
	}

	public Integer getCustomParam() {
		return customParam;
	}

	public void setCustomParam(Integer customParam) {
		this.customParam = customParam;
	}

	public AppUserIntegralListDto(AppPageSupport<AppIntegraLogDto> result,
			Integer customParam) {
		super();
		this.result = result;
		this.customParam = customParam;
	}
	
}
