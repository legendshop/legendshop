package com.legendshop.app.service;

import java.util.List;

import com.legendshop.app.dto.AppHotsearchDto;
import com.legendshop.app.dto.AppProdDto;
import com.legendshop.app.dto.AppShopDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.search.AppProdSearchParms;
import com.legendshop.model.dto.search.AppShopSearchParams;

public interface AppSearchService {

	/**
	 * 商品搜索
	 * @param parms
	 */
	public abstract AppPageSupport<AppProdDto> appProdList(AppProdSearchParms parms);

	/**
	 * 店铺搜索
	 * @param parms
	 * @return
	 */
	public abstract AppPageSupport<AppShopDto> appSearchShopList(AppShopSearchParams parms);

	/**
	 * 获取热门搜索词
	 * @return
	 */
	public abstract List<AppHotsearchDto> getHotsearch();
}
