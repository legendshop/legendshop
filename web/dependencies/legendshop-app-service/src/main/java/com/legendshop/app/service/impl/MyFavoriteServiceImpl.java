/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.MyFavoriteService;
import com.legendshop.business.dao.MyfavoriteDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppMyFavoriteDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.Myfavorite;
import com.legendshop.util.AppUtils;

/**
 * 我的关注服务实现类
 */
@Service("myFavoriteService")
public class MyFavoriteServiceImpl  implements MyFavoriteService{
	
	/** 我的商品收藏Dao. */
	@Autowired
	private MyfavoriteDao myfavoriteDao;

	public void deleteFavs(String userId, String selectedFavs) {
		String[] favIds = selectedFavs.split(";");
		if(AppUtils.isNotBlank(favIds)){
			myfavoriteDao.deleteFavorite(userId, favIds);
		}
	}

	public void deleteAllFavs(String userId) {
		myfavoriteDao.deleteFavoriteByUserId(userId);
	}

	public void saveFavorite(Myfavorite myfavorite) {
		myfavoriteDao.saveFavorite(myfavorite);
	}

	public Boolean isExistsFavorite(Long prodId,String userName) {
		return myfavoriteDao.isExistsFavorite(prodId,userName);
	}


	public Long getfavoriteLength(String userId) {
		return myfavoriteDao.getfavoriteLength(userId);
	}

	@Override
	public Boolean collectProduct(Long prodId, String userName, String userId) {
		try {
			Myfavorite myfavorite = new Myfavorite();
			myfavorite.setAddtime(new Date());
			myfavorite.setUserId(userId);
			myfavorite.setUserName(userName);
			myfavorite.setProdId(prodId);
			myfavoriteDao.saveFavorite(myfavorite);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
		
	}

	/**
	 * @Description: 更改支付密码
	 * @param @param userName
	 * @param @param paymentPassword   
	 * @date 2016-7-13
	 */
	@Override
	public void updatePaymentPassword(String userId,String userName, String paymentPassword) {
		myfavoriteDao.updatePaymentPassword(userName, paymentPassword);
		myfavoriteDao.flushUserDetail(userId);
	}

	/* 
	 * 获取优惠券可用数量
	 */
	@Override
	public Long getCouponLength(String userName) {
		return myfavoriteDao.getCouponLength(userName);
	}
	
	@Override
	public boolean cancelFavorite(Long prodId, String userId) {
		int result = myfavoriteDao.deleteFavoriteByProdIdAndUserId(prodId, userId);
		return result > 0;
	}

	/**
	 * 批量收藏商品
	 */
	@Override
	public boolean collectProducts(String selectedProdId, String userName, String userId) {
		
		try {
			    String[] prodIds = selectedProdId.split(",");
				Myfavorite myfavorite = new Myfavorite();
				myfavorite.setAddtime(new Date());
				myfavorite.setUserId(userId);
				myfavorite.setUserName(userName);
				for(String prodId : prodIds){
					
					//判断该商品是否已经被收藏
					Boolean existsFavorite = myfavoriteDao.isExistsFavorite(Long.parseLong(prodId),userName);
					myfavorite.setProdId(Long.parseLong(prodId));
					if(!existsFavorite){
					myfavoriteDao.saveFavorite(myfavorite);
					}
				}
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 获取我收藏的商品列表
	 */
	@Override
	public AppPageSupport<AppMyFavoriteDto> getAppFavoriteList(String curPageNO,Integer pageSize, String userId) {
		PageSupport<Myfavorite> ps = myfavoriteDao.collect(curPageNO, pageSize, userId);
		AppPageSupport<AppMyFavoriteDto> pageSupportDto = convertToAppPageSupport(ps);
		return pageSupportDto;
	}
	
	//转换dto方法
	private AppPageSupport<AppMyFavoriteDto> convertToAppPageSupport(PageSupport<Myfavorite> pageSupport){
		  AppPageSupport<AppMyFavoriteDto> appPageSupport = new AppPageSupport<AppMyFavoriteDto>();
		  appPageSupport.setCurrPage(pageSupport.getCurPageNO());
		  appPageSupport.setTotal(pageSupport.getTotal());
		  appPageSupport.setPageCount(pageSupport.getPageCount());
		  appPageSupport.setPageSize(pageSupport.getPageSize());
		  if(AppUtils.isNotBlank(pageSupport.getResultList())) {
			  List<AppMyFavoriteDto> list = new ArrayList<AppMyFavoriteDto>();
			  for (Myfavorite myfavorite : pageSupport.getResultList()) {
				  AppMyFavoriteDto appMyFavoriteDto = new AppMyFavoriteDto();
				  appMyFavoriteDto.setBuys(myfavorite.getBuys());
				  appMyFavoriteDto.setComments(myfavorite.getComments());
				  appMyFavoriteDto.setPrice(myfavorite.getPrice());
				  appMyFavoriteDto.setProdName(myfavorite.getProdName());
				  appMyFavoriteDto.setId(myfavorite.getId());
				  appMyFavoriteDto.setProdId(myfavorite.getProdId());
				  appMyFavoriteDto.setPic(myfavorite.getPic());
				  appMyFavoriteDto.setCash(myfavorite.getCash());
				  appMyFavoriteDto.setStocks(myfavorite.getStocks());
				  appMyFavoriteDto.setFavoriteCount(myfavorite.getFavoriteCount());
				  list.add(appMyFavoriteDto);
			}
			  appPageSupport.setResultList(list);
		  }
		  return appPageSupport;
		}
}
