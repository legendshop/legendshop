package com.legendshop.app.dao.impl;


import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.app.dao.AppIntegralOrderItemDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.integral.IntegralOrderItem;

/**
 * 积分订单项Dao
 *
 */
@Repository 
public class AppIntegralOrderItemDaoImpl extends GenericDaoImpl<IntegralOrderItem, Long> implements AppIntegralOrderItemDao {

	@Override
	public IntegralOrderItem getIntegralOrderItem(Long id) {
		return getById(id);
	}

	@Override
	public Long saveIntegralOrderItem(IntegralOrderItem integralOrderItem) {
		return save(integralOrderItem);
	}
	
	@Override
	public void deleteByOrderId(Long orderId) {
		List<IntegralOrderItem> list = queryByProperties(new EntityCriterion().eq("orderId", orderId));
		delete(list);
	}
	
}
