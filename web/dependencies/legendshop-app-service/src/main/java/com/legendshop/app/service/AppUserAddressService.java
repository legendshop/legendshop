package com.legendshop.app.service;

import com.legendshop.model.dto.app.AddressDto;
import com.legendshop.model.dto.app.AppPageSupport;

public interface AppUserAddressService {

	public AppPageSupport<AddressDto> queryUserAddress(String curPageNO ,String userId);
	
	public AddressDto getUserAddressDto(Long addrId);
}
