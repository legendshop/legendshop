package com.legendshop.app.service;

import com.legendshop.app.dto.AppCommentReplyDto;
import com.legendshop.app.dto.AppProdDto;
import com.legendshop.app.dto.AppStoreProdDto;
import com.legendshop.app.dto.AppProductDetailDto;
import com.legendshop.biz.model.dto.AppProdCommDto;
import com.legendshop.model.dto.app.*;
import com.legendshop.model.dto.app.store.AppShopProdSearchParams;
import com.legendshop.model.dto.app.store.AppStoreSearchParams;

import java.util.List;


public interface AppProductService {

	/** APP 获取 商品 信息  **/
	public abstract AppProductDetailDto getProductDetailDto(Long prodId);

	public abstract List<AppParamGroupDto> getParamGroups(Long prodId);
    
	/**
	 * 店内商品搜索
	 * @param params
	 * @return
	 */
	public abstract AppPageSupport<AppStoreProdDto> getStoreProds(AppStoreSearchParams params);
	
	/**
	 * 店铺热卖商品
	 * @param shopId
	 * @return
	 */
	public  abstract List<AppStoreProdDto> getHotProd(Long shopId, int maxNum);
	
	/**
	 * 店铺上新
	 * @param shopId
	 * @param i
	 * @return
	 */
	public abstract List<AppStoreProdDto> getStoreNewProds(Long shopId, int maxNum);
	
	/**
	 * 查询商品评论列表
	 * @param params
	 * @param userId 
	 * @return
	 */
	public abstract AppPageSupport<AppProdCommDto> queryProductComments(AppQueryProductCommentsParams params, String userId);

	/**
	 * 查询评论回复列表
	 * @param prodComId
	 * @param curPageNO
	 * @return
	 */
	public abstract AppPageSupport<AppCommentReplyDto> queryCommentReplyList(Long prodComId, Integer curPageNO);

	/**
	 * 店铺全部商品
	 * @param params 搜索参数
	 * @return
	 */
	public abstract AppPageSupport<AppStoreProdDto> storeProdctList(AppShopProdSearchParams params);

	/**
	 * 获取指定排序规则的商品
	 * @param groupConditional 排序规则
	 * @param pageCount  获取数量
	 * @return
	 */
	public abstract List<AppProdDto> queryProductByOrder(String groupConditional, Integer pageCount);

	
	/**
	 *  获取自定义商品分组内的商品
	 * @param prodGroupId 分组ID
	 * @param sort 组内排序规则
	 * @param pageCount  获取数量
	 * @return
	 */
	public abstract List<AppProdDto> queryProductByProdGroup(Long prodGroupId, String sort, Integer pageCount);

	
	/**
	 * 根据分类ID获取商品
	 * @param categoryId 分类ID
	 * @param pageCount 获取数量
	 * @return
	 */
	public abstract List<AppProdDto> queryProductByCategory(Long categoryId, Integer pageCount);

	/**
	 * 获取指定排序规则的所有商品
	 * @param groupConditional排序规则
	 * @return
	 */
	public abstract AppPageSupport<AppProdDto> queryProductByConditional(String groupConditional,Integer curPageNO);

	/**
	 * 获取分组内的所有商品
	 * @param id 分组ID 
	 * @param sort 组内排序条件
	 * @param curPageNO 当前页码
	 * @return
	 */
	public abstract AppPageSupport<AppProdDto> queryProductByProdGroupId(Long prodGroupId, String sort, Integer curPageNO);

	/**
	 * 根据店铺分类ID获取商品
	 * @param shopCategoryId 店铺分类ID
	 * @param pageCount 获取数量
	 * @param grade 分类等级
	 * @return
	 */
	public abstract List<AppProdDto> queryProductByShopCategory(Long shopCategoryId,Integer grade, Integer pageCount);

}
