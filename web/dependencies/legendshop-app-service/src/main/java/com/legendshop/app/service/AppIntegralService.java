package com.legendshop.app.service;

import com.legendshop.app.dto.AppIntegralOrderDto;
import com.legendshop.app.dto.AppIntegralProdDto;
import com.legendshop.app.dto.AppIntegralProdListDto;
import com.legendshop.app.dto.AppSubmitIntegralProdDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.integral.IntegralOrderDto;
import com.legendshop.model.dto.order.OrderSearchParamDto;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.model.entity.integral.IntegralProd;

/**
 * 积分商城
 */
public interface AppIntegralService {
	
	/**
	 * 获取积分商品列表
	 * @param curPageNO 当前页
	 * @param integralSort  兑换积分排序 asc desc
	 * @param saleNumSort  销售数量排序 asc desc
	 * @return
	 */
	public abstract AppPageSupport<IntegralProd> queryIntegralProd(String curPageNO,String integralSort,String saleNumSort);
	
	/**
	 * 根据ID获取积分商品详情Dto
	 * @param id
	 * @return
	 */
	public abstract AppIntegralProdDto getIntegralProdDtoById(Long id);
	
	/**
	 * 根据ID获取积分商品详情
	 * @param id
	 * @return
	 */
	public abstract IntegralProd getIntegralProdById(Long id);
	
	/**
	 * 用积分兑换商品
	 * @param userId
	 * @param userAddress
	 * @param integralProd
	 * @param count
	 * @param orderDesc
	 * @return
	 */
	public abstract String shopBuyIntegral(String userId, UserAddress userAddress ,IntegralProd integralProd,
			Integer count,String orderDesc);
	
	/**
	 * 查询积分订单
	 * @param paramDto
	 * @return
	 */
	public abstract AppPageSupport<IntegralOrderDto> getIntegralOrderDtos(OrderSearchParamDto paramDto);
	
	/**
	 * 查询积分订单列表
	 * @param paramDto
	 * @return
	 */
	public abstract AppPageSupport<AppIntegralOrderDto> getIntegralOrderDtoList(OrderSearchParamDto paramDto);
	
	/**
	 * 根据订单流水号查看订单详情
	 * @param orderSn
	 * @return
	 */
	public abstract IntegralOrderDto findIntegralOrderDetail(String orderSn);
	
	
	/**
	 * 取消积分订单
	 * @param orderSn 订单编号
	 * @param updateUserId 后台更新用户ID
	 * @param logDesc 积分操作日记
	 * @return
	 */
	public abstract String orderCancel(String orderSn, String userId,String updateUserId, String logDesc);
	
	/**
	 * 删除积分订单
	 * @param orderSn 订单编号
	 * @param updateUserId 后台更新用户ID
	 * @param logDesc 积分操作日记
	 * @return
	 */
	public abstract String orderDel(String orderSn, String userId,String updateUserId, String logDesc);
	
	/**
	 * 根据流水号确认收货
	 * @param orderSn
	 * @return
	 */
	public abstract String shouhuo(String orderSn);

	/**
	 * 获取用户商品购买数量
	 * @param id
	 * @param userId
	 * @return
	 */
	public abstract int getIntegralBuyCount(Long id, String userId);
	
	/**
	 * 可以根据是否推荐,排序规则查询积分商品列表
	 * @return
	 */
	public abstract AppPageSupport<AppIntegralProdListDto> getIntegralProdList(String curPageNO, Integer pageSize,Boolean isCommend, String order);
	
	/**
	 * 转换成积分商品提交订单详情Dto
	 * @param id
	 * @return
	 */
	public abstract AppSubmitIntegralProdDto convertToAppSubmitIntegralProdDto(IntegralProd integralProd);
	
}
