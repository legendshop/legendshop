/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import java.util.Date;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.model.constant.SMSTypeEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.AppPdCashLogDto;
import com.legendshop.model.dto.app.AppPdRechargeDto;
import com.legendshop.model.dto.app.AppPdRechargeReturnDto;
import com.legendshop.model.dto.app.AppPdWithdrawCashDto;
import com.legendshop.model.dto.app.AppPredepositDto;
import com.legendshop.model.dto.app.AppRechargeDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.PdRecharge;
import com.legendshop.model.entity.PdWithdrawCash;
import com.legendshop.model.entity.SMSLog;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.spi.service.AppPdCashLogService;
import com.legendshop.spi.service.AppPdRechargeService;
import com.legendshop.spi.service.AppPdWithdrawCashService;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.PdRechargeService;
import com.legendshop.spi.service.PreDepositService;
import com.legendshop.spi.service.SMSLogService;
import com.legendshop.spi.service.SubSettlementService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * （App）预存款控制器
 */
@Api(tags="预存款",value="预存款主页、充值、提现、账户明细、提现记录相关接口")
@Controller
@RequestMapping("/p/app/predeposit")
public class AppPredepositController {
	
	private static Logger LOGGER = LoggerFactory.getLogger(AppPredepositController.class);

	@Autowired
	private PdRechargeService pdRechargeService;

	@Autowired
	private PreDepositService preDepositService;
	
	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private SMSLogService smsLogService;
	
	@Autowired
	private AppPdRechargeService appPdRechargeService;
	
	@Autowired
	private AppPdCashLogService appPdCashLogService;
	
	@Autowired
	private AppPdWithdrawCashService appPdWithdrawCashService;
	
	@Autowired
	private SubSettlementService subSettlementService;

	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;
	
	/**
	 * 预存款主页
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ApiOperation(value = "预存款主页", httpMethod = "POST", notes = "预存款主页信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping
	@ResponseBody
	public ResultDto<AppPredepositDto> preDeposit() {
		try {
			String userId = loginedUserService.getUser().getUserId();
			UserDetail userDetail = userDetailService.getUserDetailByIdNoCache(userId);
			
			if (AppUtils.isBlank(userDetail)) {
				return ResultDtoManager.fail(-1, "用户信息不存在或已被删除!");
			}
			
			Double freezePredeposit = userDetail.getFreezePredeposit();
			Double availablePredeposit = userDetail.getAvailablePredeposit();
			Double totalPredeposit = Arith.add(freezePredeposit, availablePredeposit);
			
			//构建返回参数
			AppPredepositDto appPredepositDto = new AppPredepositDto();
			appPredepositDto.setAvailablePredeposit(availablePredeposit);
			appPredepositDto.setFreezePredeposit(freezePredeposit);
			appPredepositDto.setTotalPredeposit(totalPredeposit);

			return ResultDtoManager.success(appPredepositDto);
		} catch (Exception e) {
			LOGGER.error("获取预存款信息异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	
	/**
	 * 预存款充值
	 *
	 */
	@ApiOperation(value = "充值", httpMethod = "POST", notes = "预存款充值",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "amount", value = "充值金额", required = true, dataType = "double")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "amount", value = "充值金额", required = true, dataType = "double"),
		@ApiImplicitParam(paramType="query", name = "pdrSn", value = "充值订单号", required = false, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "paytypeId", value = "支付方式", required = true, dataType = "string")
	})
	@PostMapping(value="/recharge")
	@ResponseBody
	public ResultDto<AppPdRechargeReturnDto> recharge(Double amount,String pdrSn, String  paytypeId) {
		
		try {
			
			LoginedUserInfo token = loginedUserService.getUser();
			AppPdRechargeReturnDto appPdRechargeReturnDto = new AppPdRechargeReturnDto();
			
			if (AppUtils.isBlank(token)) {
				return ResultDtoManager.fail(-1, "请先登录!");
			}
			if (AppUtils.isBlank(amount) || amount <= 0) {
				return ResultDtoManager.fail(-1, "金额不能小于0");
	        }
			
			if (AppUtils.isNotBlank(pdrSn)) {
				 PdRecharge pdRecharge = pdRechargeService.findRechargePay(token.getUserId(),pdrSn, 0);
				 if (AppUtils.isBlank(pdRecharge)) {
						return ResultDtoManager.fail(-1, "该充值单据无效或者已经支付完成！");
					}
				 appPdRechargeReturnDto.setPdrSn(pdRecharge.getSn());
			}else{
				
				//构建充值订单
				String pdrSub = CommonServiceUtil.getRandomSn();
	            PdRecharge pdRecharge = new PdRecharge();
	            pdRecharge.setSn(pdrSub);
	            pdRecharge.setUserId(token.getUserId());
	            pdRecharge.setUserName(token.getUserName());
	            pdRecharge.setAmount(amount);
	            pdRecharge.setAddTime(new Date());
	            pdRecharge.setPaymentState(0);
	            
	            //保存充值订单，未支付
				Long result = pdRechargeService.savePdRecharge(pdRecharge);
				if (result == 0) {
					return ResultDtoManager.fail(-1, "充值订单创建失败！");
				}
				//订单号
				appPdRechargeReturnDto.setPdrSn(pdrSub);
			}
			 appPdRechargeReturnDto.setAmount(amount);
			 appPdRechargeReturnDto.setPaymentId(paytypeId);
			 appPdRechargeReturnDto.setSubSettlementType("USER_PRED_RECHARGE");//预存款充值
			 
			 return ResultDtoManager.success(appPdRechargeReturnDto);
		} catch (Exception e) {
			LOGGER.error("预存款充值订单创建异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 去充值（ 已生成充值, 但是未完成支付）
	 */
	@ApiOperation(value = "去充值", httpMethod = "POST", notes = "预存款充值",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="path", name = "pdrSn", value = "充值订单号", required = true, dataType = "string")
	@PostMapping(value="/toRecharge/{pdrSn}")
	@ResponseBody
	public ResultDto<AppPdRechargeReturnDto> rechargeForm(HttpServletRequest request, HttpServletResponse response, @PathVariable String pdrSn) {
		
		String userId = loginedUserService.getUser().getUserId();
		PdRecharge pdRecharge = pdRechargeService.findRechargePay(userId, pdrSn, 0);
		
		if (AppUtils.isBlank(pdRecharge)) {
			return ResultDtoManager.fail(-1,"该充值单据无效或者被删除！");
		}
		if (pdRecharge.getPaymentState().intValue() == 1) {
			return ResultDtoManager.fail(-1,"该充值单据无效已经支付完成！");
		}

		//构建返回数据
		AppPdRechargeReturnDto appPdRechargeReturnDto = new AppPdRechargeReturnDto();
		appPdRechargeReturnDto.setPdrSn(pdrSn);
		appPdRechargeReturnDto.setAmount(pdRecharge.getAmount());
		appPdRechargeReturnDto.setPaymentId(pdRecharge.getPaymentCode());
		appPdRechargeReturnDto.setSubSettlementType("USER_PRED_RECHARGE");
		return ResultDtoManager.success(appPdRechargeReturnDto);
	}
	
	
	/**
	 * 充值记录
	 *
	 */
	@ApiOperation(value = "充值记录", httpMethod = "POST", notes = "充值记录",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "state", value = "支付状态[null: 全部  ,1:已支付 ,0:未支付]", required = false, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = true, dataType = "string")
	})
	@PostMapping(value="/rechargeList")
	@ResponseBody
	public ResultDto<AppPageSupport<AppPdRechargeDto>> rechargeList(String state, String curPageNO) {
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			AppPageSupport<AppPdRechargeDto> result = appPdRechargeService.getRechargeDetailedPage(curPageNO, userId, state);
			
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("获取预存款充值记录异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 充值记录详情
	 *
	 */
	@ApiOperation(value = "充值记录详情", httpMethod = "POST", notes = "充值记录详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "id", value = "充值记录id", required = true, dataType = "Long")
	@PostMapping(value="/rechargeDetail")
	@ResponseBody
	public ResultDto<AppPdRechargeDto> rechargeDetail(Long id) {
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			AppPdRechargeDto appPdRechargeDto = appPdRechargeService.getPdRecharge(id,userId);
			
			if (AppUtils.isBlank(appPdRechargeDto)) {
				return ResultDtoManager.fail(-1, "该记录不存在或已被删除!");
			}
			return ResultDtoManager.success(appPdRechargeDto);
		} catch (Exception e) {
			LOGGER.error("获取预存款充值记录详情异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 账户明细 
	 *
	 */
	@ApiOperation(value = "账户明细", httpMethod = "POST", notes = "账户明细",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "state", value = "明细类型[null: 全部  ,1: 收入 ,0: 支出]", required = false, dataType = "Long"),
		@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = true, dataType = "string")
	})
	@PostMapping(value="/pdCashLogList")
	@ResponseBody
	public ResultDto<AppPageSupport<AppPdCashLogDto>> pdCashLogList(Integer state,String curPageNO) {
		
		try {
			String userId = loginedUserService.getUser().getUserId();
			AppPageSupport<AppPdCashLogDto> result = appPdCashLogService.getPdCashLogPageByType(userId,state,curPageNO);
			
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("获取预存款账户明细异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 账户明细详情
	 *
	 */
	@ApiOperation(value = "账户明细详情", httpMethod = "POST", notes = "账户明细",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "id", value = "账户明细id", required = true, dataType = "Long")
	@PostMapping(value="/pdCashLogDetail")
	@ResponseBody
	public ResultDto<AppPdCashLogDto> pdCashLogDetail(Long id) {
		
		try {
			AppPdCashLogDto result = appPdCashLogService.getPdCashLog(id);
			
			if (AppUtils.isBlank(result)) {
				return ResultDtoManager.fail(-1, "对不起,您要查看明细信息不存在或已被删除!");
			}
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("获取预存款账户明细详情异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 获取可提现金额
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@ApiOperation(value = "获取可提现金额", httpMethod = "GET", notes = "返回可提现金额 ，类型为double",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@GetMapping(value="/getAvailablePredeposit")
	@ResponseBody
	public ResultDto<Double> getAvailablePredeposit() {
		try {
			String userId =  loginedUserService.getUser().getUserId();
			Double availablePredeposit = userDetailService.getAvailablePredeposit(userId);
			if (AppUtils.isBlank(availablePredeposit)) {
				availablePredeposit = 0d;
			}
			return ResultDtoManager.success(availablePredeposit);
		} catch (Exception e) {
			LOGGER.error("获取预存款可提现金额异常!", e);
			return ResultDtoManager.fail();
		}
	}

	
	/**
	 * 提现提交
	 *
	 */
	@ApiOperation(value = "提现", httpMethod = "POST", notes = "提交提现，返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "bankName", value = "提现方式", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "bankNo", value = "提现账号", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "bankUser", value = "开户姓名", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "bankOfDeposit", value = "开户银行, 当选择的是银行卡的时候才需要填写这个", required = false, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "amount", value = "提现金额", required = true, dataType = "double"),
		@ApiImplicitParam(paramType="query", name = "mobileCode", value = "短信验证码", required = true, dataType = "string")
	})
	@PostMapping(value="/withdrawalsSave")
	@ResponseBody
	public ResultDto<String> withdrawalsSave(String bankName,  String bankNo,String bankUser, String bankOfDeposit, 
			Double amount, String mobileCode) {
		
		try {
	        if (AppUtils.isBlank(bankName)) {
	            return ResultDtoManager.fail(-1, "请选择提现方式!");
	        }
	        if (AppUtils.isBlank(bankNo)) {
	            return ResultDtoManager.fail(-1, "请输入提款帐号!");
	        }
	        if (AppUtils.isBlank(bankUser)) {
	            return ResultDtoManager.fail(-1, "请填写您的真实姓名!");
	        }
	        if (AppUtils.isBlank(amount) || amount <= 0) {
	            return ResultDtoManager.fail(-1, "输入正确提现金额格式,提现金额为大于或者等于0.01的数字!");
	        }
	        Pattern moneyPattern = Pattern.compile("^\\d+(\\.\\d{1,2})?$");
	        if (!moneyPattern.matcher(amount.toString()).matches()) {
	            return ResultDtoManager.fail(-1, "输入正确提现金额格式,提现金额为大于或者等于0.01的数字!");
	        }
	        if (AppUtils.isBlank(mobileCode)) {
	            return ResultDtoManager.fail(-1, "请输入手机验证码!");
	        }
	        
	        String userId =  loginedUserService.getUser().getUserId();
	        String userName = loginedUserService.getUser().getUserName();
	        
	        UserDetail userDetail = userDetailService.getUserDetailById(userId);
	        Double availablePredeposit = userDetail.getAvailablePredeposit();
			
			if (availablePredeposit <= 0 || availablePredeposit < amount) { 
				return ResultDtoManager.fail(-1, "您的余额不足!");
			}
	        
	        SMSLog smsLog = smsLogService.getValidSMSCode(userDetail.getUserMobile(), SMSTypeEnum.VAL);
	        if (AppUtils.isBlank(smsLog)) {
	            return ResultDtoManager.fail(-1, "请先获取手机验证码!");
	        }
	        if (!mobileCode.equals(smsLog.getMobileCode())) {
	            return ResultDtoManager.fail(-1, "请输入正确的手机验证码!");
	        }
	        // 检查短信验证码是否正确
	        if (!smsLogService.verifyCodeAndClear(userDetail.getUserMobile(), mobileCode, SMSTypeEnum.VAL)) {
	            return ResultDtoManager.fail(-1, "您的验证码已经过期,请重新获取验证码信息!");
	        }

	        String pdcSn = CommonServiceUtil.getRandomSn();
	        PdWithdrawCash orgin = new PdWithdrawCash();
	        orgin.setAddTime(new Date());
	        orgin.setPdcSn(pdcSn);
	        orgin.setUserId(userId);
	        orgin.setUserName(userName);
	        orgin.setAmount(amount);
	        orgin.setBankName(bankName);
	        orgin.setBankNo(bankNo);
	        orgin.setBankUser(bankUser);
	        orgin.setBankOfDeposit(bankOfDeposit);
	        orgin.setPaymentState(0);
	        String result = preDepositService.withdrawalApply(orgin);
			
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("获取预存款提现异常!", e);
			return ResultDtoManager.fail();
		}
	}

	/**
	 * 提现记录列表
	 *
	 */
	@ApiOperation(value = "提现记录", httpMethod = "POST", notes = "提现记录",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "state", value = "提现状态，[null: 全部  ,1: 已成功 ,0: 待审核, -1:已拒绝]", required = true, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = true, dataType = "string"),
	})
	@PostMapping(value="/withdrawalsList")
	@ResponseBody
	public ResultDto<AppPageSupport<AppPdWithdrawCashDto>> withdrawalsList(String state, String curPageNO) {
		 
		try {
			String userId =  loginedUserService.getUser().getUserId();
			
			AppPageSupport<AppPdWithdrawCashDto> result = appPdWithdrawCashService.getPdWithdrawCashPage(curPageNO, userId, state);
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("获取预存款提现明细列表异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	
	/**
	 * 提现记录详情
	 *
	 */
	@ApiOperation(value = "提现记录详情", httpMethod = "POST", notes = "提现记录详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "id", value = "提现记录id", required = true, dataType = "Long")
	@PostMapping(value="/withdrawalsDetail")
	@ResponseBody
	public ResultDto<AppPdWithdrawCashDto> withdrawalsDetail(Long id) {
		
		try {
			String userId =  loginedUserService.getUser().getUserId();
			AppPdWithdrawCashDto result = appPdWithdrawCashService.getPdWithdrawCash(id,userId);
			if (AppUtils.isBlank(result)) {
				return ResultDtoManager.fail(-1, "对不起,您要查看提现记录不存在或已被删除!");
			}
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("获取预存款提现记录详情异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 普通订单成功订单详情
	 */
	@ApiOperation(value = "充值订单成功订单详情", httpMethod = "POST", notes = "普通订单成功订单详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "subSettlementSn", value = "清算单据号", required = true, dataType = "String"),
		@ApiImplicitParam(paramType="query", name = "subSettlementType", value = "单据类型", required = true, dataType = "String")
	})
	@PostMapping(value="/successOrderDetail")
	@ResponseBody 
	public ResultDto<AppRechargeDto> successOrderDetail(String subSettlementSn,String subSettlementType){
		try {
			String userId = loginedUserService.getUser().getUserId();
			if(AppUtils.isBlank(userId)){
				return ResultDtoManager.fail(-1,"请先登录");
			}
			AppRechargeDto appRechargeDto = subSettlementService.getSubSettlementToAppOrderDto(subSettlementSn, userId);
			if(AppUtils.isBlank(appRechargeDto)){
				ResultDtoManager.fail(-1, "该订单已不存在或被删除，请重新加载！");
			}
			appRechargeDto.setSubSettlementType(subSettlementType);
			return ResultDtoManager.success(appRechargeDto);
		} catch (Exception e) {
			LOGGER.error("获取提交成功的订单详情异常!", e.getMessage());
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 提现身份认证
	 *
	 */
	//身份校验已经更改，这里暂时注释掉
	/*@RequestMapping(value = "/sendSmsCodeForApp") 
	@ResponseBody
	public ResultDto sendSmsCode(String authType) {
		String userId = loginedUserService.getUser().getUserId();
		String userName = loginedUserService.getUser().getUserName();
		UserDetail usDetailer = userDetailService.getUserDetailById(userId);
		if ("email".equals(authType)) {
			UserSecurity security = userDetailService.getUserSecurity(userName);
			if (security.getMailVerifn().intValue() == 0) {
				return ResultDtoManager.fail(-1, "该邮箱没有通过身份绑定");
			}
			MailEntity mailEntity = mailEntityService.getMailEntity(usDetailer.getUserMail(), MailCategoryEnum.TIX);
			if (AppUtils.isNotBlank(mailEntity)) {
				// 检查是否超时
				Date recDate = mailEntity.getRecTime();
				// 控制3分钟内不能重复发送
				if (new Date().getTime() < (recDate.getTime() + 60000 * 3)) {
					return ResultDtoManager.fail(-1, "请不要频繁获取安全认证码,请稍候重试");
				}
			}
			// 获得验证码
			String code = RandomStringUtils.randomNumeric(3, 6);
			// 发送邮件
			mailManager.withdrawApply(usDetailer, usDetailer.getUserMail(), code);
			return ResultDtoManager.success();
		} else if ("phone".equals(authType)) {
			UserSecurity security = userDetailService.getUserSecurity(userName);
			if (security.getPhoneVerifn().intValue() == 0) {
				return ResultDtoManager.fail(-1, "该手机没有通过身份绑定");
			}
			SMSLog smsLog = smsLogService.getValidSMSCode(usDetailer.getUserMobile(), SMSTypeEnum.TIX);
			if (AppUtils.isNotBlank(smsLog)) {
				// 检查是否超时
				Date recDate = smsLog.getRecDate();
				// 控制3分钟内不能重复发送
				if (new Date().getTime() < (recDate.getTime() + 60000 * 3)) {
					return ResultDtoManager.fail(-1, "请不要频繁获取安全认证码,请稍候重试");
				}
			}
			// 获得验证码
			String code = RandomStringUtils.randomNumeric(3, 6);
			// 发送短信
			EventHome.publishEvent(new SendSMSEvent(usDetailer.getUserMobile(), code, userName));
			return ResultDtoManager.success();
		}
		return ResultDtoManager.fail(-1, "请求参数错误");
	}*/

}
