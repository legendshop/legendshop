package com.legendshop.app.service;

import com.legendshop.app.dto.AppIntegraLogDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.integral.IntegraLog;

public interface AppIntegraLogService {

	/**
	 * @param type 
	 * @Description: 我的积分
	 * @date 2016-7-13 
	 */
	public AppPageSupport<AppIntegraLogDto> queryUserIntegral(String curPageNO ,String userId, Integer type);

	/**
	 * 获取积分明细详情
	 * @param id
	 * @return
	 */
	AppIntegraLogDto getIntegraLog(Long id);
}
