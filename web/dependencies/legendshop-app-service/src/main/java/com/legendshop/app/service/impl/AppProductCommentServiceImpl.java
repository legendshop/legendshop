/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.AppProductCommentService;
import com.legendshop.business.dao.ProductCommentDao;
import com.legendshop.model.entity.ProductCommentCategory;

/**
 * app产品评论服务.
 */
@Service("appProductCommentService")
public class AppProductCommentServiceImpl implements AppProductCommentService {

	@Autowired
	private ProductCommentDao productCommentDao;
	
	@Override
	public ProductCommentCategory initProductCommentCategory(Long prodId) {
		return productCommentDao.initProductCommentCategory(prodId);
	}

}
