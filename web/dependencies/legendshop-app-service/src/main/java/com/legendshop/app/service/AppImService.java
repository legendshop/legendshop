/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service;

import java.util.List;

import com.legendshop.app.dto.AppDialogueDto;
import com.legendshop.app.dto.AppImConsultContentDto;
import com.legendshop.app.dto.AppImOrderDto;

/**
 * app IM服务接口
 */
public interface AppImService {
	
	/**
	 * 获取客服会话列表
	 * @param userId
	 * @return
	 */
	public List<AppDialogueDto> getImDialogues(String userId);

	/**
	 * 获取用户在店铺下的商品
	 * @param userId 用户ID
	 * @param shopId 商家ID
	 * @return
	 */
	public List<AppImOrderDto> getOrders(String userId, Long shopId);

	/**
	 * 获取用户当前咨询的内容
	 * @param consultId
	 * @param consultType
	 * @param userId
	 * @return
	 */
	public AppImConsultContentDto getImConsulting(String consultId, String consultType, String userId);

}
