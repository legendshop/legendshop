package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.AppThemeDetailDto;
import com.legendshop.app.dto.AppThemeListDto;
import com.legendshop.app.dto.AppThemeModuleDto;
import com.legendshop.app.dto.AppThemeModuleProdDto;
import com.legendshop.app.dto.AppThemeRelatedDto;
import com.legendshop.app.service.AppThemeService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.Theme;
import com.legendshop.model.entity.ThemeModule;
import com.legendshop.model.entity.ThemeModuleProd;
import com.legendshop.model.entity.ThemeRelated;
import com.legendshop.spi.service.ThemeModuleService;
import com.legendshop.spi.service.ThemeRelatedService;
import com.legendshop.spi.service.ThemeService;
import com.legendshop.util.AppUtils;

/**
 * app 专题活动service实现
 */
@Service("appThemeService")
public class AppThemeServiceImpl  implements AppThemeService{

	@Autowired
	private ThemeService themeService;
	
	@Autowired
	private ThemeModuleService themeModuleService;
	
	@Autowired
	private ThemeRelatedService themeRelatedService;

	/**
	 * 获取专题活动列表
	 */
	@Override
	public AppPageSupport<AppThemeListDto> getThemePage(String curPageNO) {

		PageSupport<Theme> ps = themeService.allThemes(curPageNO);
		
		//转Dto
		AppPageSupport<AppThemeListDto> pageSupportDto = PageUtils.convertPageSupport(ps,
				new PageUtils.ResultListConvertor<Theme, AppThemeListDto>() {

			@Override
			public List<AppThemeListDto> convert(List<Theme> form) {

				if (AppUtils.isBlank(form)) {
					return null;
				}
				List<AppThemeListDto> toList = new ArrayList<AppThemeListDto>();
				for (Theme theme : form) {
					AppThemeListDto themeListDto = convertToAppThemeListDto(theme);
					toList.add(themeListDto);
				}
				return toList;
			}
		});
		return pageSupportDto;
	}
	
	/**
	 * 获取专题活动详情
	 */
	@Override
	public AppThemeDetailDto getThemeDetail(Long themeId) {
		
		//查询专题活动
		Theme theme =  themeService.getTheme(themeId);
		if (AppUtils.isBlank(theme)) {
			return null;
		}
		
		//查询专题模块
		Map<ThemeModule, List<ThemeModuleProd>> themeModuleMap  =themeModuleService.getThemeModuleAndProdByThemeId(themeId);
		
		//查询相关专题
		List<ThemeRelated> themeRelateds = themeRelatedService.getThemeRelatedByTheme(themeId);

		
		return convertToAppThemeDetailDto(theme, themeModuleMap, themeRelateds);
	}
	
	
	//转Dto方法
	private AppThemeDetailDto convertToAppThemeDetailDto(Theme theme, 
			Map<ThemeModule, List<ThemeModuleProd>> themeModuleMap, List<ThemeRelated> themeRelateds){
		
	  AppThemeDetailDto  appThemeDetailDto  = new AppThemeDetailDto();
	
	  //转换专题活动信息
	  appThemeDetailDto.setThemeId(theme.getThemeId());
	  appThemeDetailDto.setTemplateType(theme.getTemplateType());
	  appThemeDetailDto.setBackgroundColor(theme.getBackgroundColor());
	  appThemeDetailDto.setBackgroundImg(theme.getBackgroundMobileImg());
	  appThemeDetailDto.setTitle(theme.getTitle());
	  appThemeDetailDto.setIsTitleShow(theme.getIsTitleShow());
	  appThemeDetailDto.setIsIntroShow(theme.getIsIntroMShow());
	  appThemeDetailDto.setIntro(theme.getIntroMobile());
	  appThemeDetailDto.setIntroColor(theme.getIntroMColor());
	  appThemeDetailDto.setThemeImg(theme.getThemeMobileImg());
	  appThemeDetailDto.setTitleColor(theme.getTitleColor());
	  appThemeDetailDto.setBannerImgColor(theme.getBannerImgColor());
	  appThemeDetailDto.setBannerImg(theme.getBannerMobileImg());
	  appThemeDetailDto.setCustomContent(theme.getCustomContent());
	  appThemeDetailDto.setStartTime(theme.getStartTime());
	  appThemeDetailDto.setEndTime(theme.getEndTime());
	  appThemeDetailDto.setThemeDesc(theme.getThemeDesc());
	  appThemeDetailDto.setStatus(theme.getStatus());
	  
	 if(null != themeModuleMap && !themeModuleMap.isEmpty()){
		 //转换专题板块
		 List<AppThemeModuleDto> themeModuleList = new ArrayList<AppThemeModuleDto>();
		 
		 //增强型for循环遍历Map集合
	     for(Entry<ThemeModule, List<ThemeModuleProd>> themeModeuleMapItem : themeModuleMap.entrySet()) {
	    	//遍历出的数据，转换后set入AppThemeModuleDetailDto
	    	AppThemeModuleDto appThemeModuleDto = convertToAppThemeModuleDto(themeModeuleMapItem);
	    	themeModuleList.add(appThemeModuleDto);
	     } 
	     appThemeDetailDto.setThemeModuleList(themeModuleList);
	 }
	 
	 //转换相关专题
	if (AppUtils.isNotBlank(themeRelateds)) {
		List<AppThemeRelatedDto> appThemeRelatedDtoLis = new ArrayList<AppThemeRelatedDto>();
		for (ThemeRelated themeRelated : themeRelateds) {
			AppThemeRelatedDto appThemeRelatedDto = convertToAppThemeRelatedDto(themeRelated);
			appThemeRelatedDtoLis.add(appThemeRelatedDto);
		}
		appThemeDetailDto.setThemeRelatedList(appThemeRelatedDtoLis);
	}

	  return appThemeDetailDto;
	}
	
	
	private AppThemeListDto convertToAppThemeListDto(Theme theme){
		  AppThemeListDto appThemeListDto = new AppThemeListDto();
		  appThemeListDto.setThemeId(theme.getThemeId());
		  appThemeListDto.setThemeMobileImg(theme.getThemeMobileImg());
		  appThemeListDto.setEndTime(theme.getEndTime());
		  appThemeListDto.setTitle(theme.getTitle());
		  appThemeListDto.setTitleColor(theme.getTitleColor());
		  appThemeListDto.setIntroMobile(theme.getIntroMobile());
		  appThemeListDto.setIntroMColor(theme.getIntroMColor());
		  appThemeListDto.setIsIntroMShow(theme.getIsIntroMShow());
		  return appThemeListDto;
		}
	
	//专题板块转Dto方法
	private AppThemeModuleDto convertToAppThemeModuleDto(Entry<ThemeModule, List<ThemeModuleProd>> themeModeuleMap){
		  
		  ThemeModule themeModule =  themeModeuleMap.getKey();
	      List<ThemeModuleProd> prodList = themeModeuleMap.getValue();
	      
		  if (null == themeModule) {
			  return null;
		  }
		  
		  AppThemeModuleDto appThemeModuleDto = new AppThemeModuleDto();
		  appThemeModuleDto.setTitleColor(themeModule.getTitleColor());
		  appThemeModuleDto.setIsTitleShow(themeModule.getIsTitleShow());
		  appThemeModuleDto.setTitleBgImg(themeModule.getTitleBgMobileimg());
		  appThemeModuleDto.setAdMobileUrl(themeModule.getAdMobileUrl());
		  appThemeModuleDto.setTitle(themeModule.getTitle());
		  appThemeModuleDto.setMoreUrl(themeModule.getMoreUrl());
		  appThemeModuleDto.setTitleBgColor(themeModule.getTitleBgColor());
		  appThemeModuleDto.setThemeModuleId(themeModule.getThemeModuleId());
		  appThemeModuleDto.setProdList(convertToAppThemeModuleProdDtoList(prodList));
	    	
		  return appThemeModuleDto;
		}
	
	//板块商品列表转Dto列表方法
	private List<AppThemeModuleProdDto> convertToAppThemeModuleProdDtoList(List<ThemeModuleProd> prodList){
		if (AppUtils.isBlank(prodList)) {
			  return null;
		  }
		
		List<AppThemeModuleProdDto> toList = new ArrayList<AppThemeModuleProdDto>();
		for (ThemeModuleProd themeModuleProd : prodList) {
			AppThemeModuleProdDto appThemeModuleProdDto = convertToAppThemeModuleProdDto(themeModuleProd);
			toList.add(appThemeModuleProdDto);
		}
		return toList;
	}
	
	//板块商品转Dto方法
	private AppThemeModuleProdDto convertToAppThemeModuleProdDto(ThemeModuleProd themeModuleProd){
		 
		if (AppUtils.isBlank(themeModuleProd)) {
			  return null;
		  }
		AppThemeModuleProdDto appThemeModuleProdDto = new AppThemeModuleProdDto();
		  appThemeModuleProdDto.setImg(themeModuleProd.getImg());
		  appThemeModuleProdDto.setAngleIcon(themeModuleProd.getAngleIcon());
		  appThemeModuleProdDto.setModulePrdId(themeModuleProd.getModulePrdId());
		  appThemeModuleProdDto.setProdName(themeModuleProd.getProdName());
		  appThemeModuleProdDto.setIsSoldOut(themeModuleProd.getIsSoldOut());
		  appThemeModuleProdDto.setProdId(themeModuleProd.getProdId());
		  appThemeModuleProdDto.setCash(themeModuleProd.getCash());
		  appThemeModuleProdDto.setThemeModuleId(themeModuleProd.getThemeModuleId());
		  return appThemeModuleProdDto;
		}
	
	//相关专题转Dto方法
	private AppThemeRelatedDto convertToAppThemeRelatedDto(ThemeRelated themeRelated){
		  AppThemeRelatedDto appThemeRelatedDto = new AppThemeRelatedDto();
		  appThemeRelatedDto.setImg(themeRelated.getImg());
		  appThemeRelatedDto.setmLink(themeRelated.getmLink());
		  appThemeRelatedDto.setThemeId(themeRelated.getThemeId());
		  appThemeRelatedDto.setRelatedId(themeRelated.getRelatedId());
		  appThemeRelatedDto.setmRelatedThemeId(themeRelated.getmRelatedThemeId());
		  return appThemeRelatedDto;
		}
}
