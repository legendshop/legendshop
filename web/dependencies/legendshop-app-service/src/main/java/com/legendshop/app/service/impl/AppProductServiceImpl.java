package com.legendshop.app.service.impl;

import com.legendshop.app.dao.AppProductDao;
import com.legendshop.app.dto.*;
import com.legendshop.app.service.AppProductService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.biz.model.dto.AppProdCommDto;
import com.legendshop.business.dao.ProductCommentDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.constant.ProductTypeEnum;
import com.legendshop.model.dto.*;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.AppParamGroupDto;
import com.legendshop.model.dto.app.AppProdParamDto;
import com.legendshop.model.dto.app.AppQueryProductCommentsParams;
import com.legendshop.model.dto.app.store.AppShopProdSearchParams;
import com.legendshop.model.dto.app.store.AppStoreSearchParams;
import com.legendshop.model.dto.buy.CartMarketRules;
import com.legendshop.model.dto.marketing.RuleResolver;
import com.legendshop.model.entity.Product;
import com.legendshop.spi.service.ActiveRuleResolverManager;
import com.legendshop.spi.service.ProductCommentService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.util.AppUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("appProductService")
public class AppProductServiceImpl  implements AppProductService {

	@Autowired
	private ProductService productService;
	
	@Autowired
	private ActiveRuleResolverManager activeRuleResolverManager;
	
	@Autowired
	private AppProductDao appProductDao;
	
	@Autowired
	private ProductCommentService productCommentService;
	
	@Autowired
	private ProductCommentDao productCommentDao;
	
	/**
	 * 获取店铺全部商品
	 */
	@Override
	public AppPageSupport<AppStoreProdDto> storeProdctList(AppShopProdSearchParams params) {
		
		PageSupport<Product> ps =  productService.storeProdctList(params.getShopId(), params.getCurPageNO(),
				params.getFireCat(),params.getSecondCat(), params.getThirdCat(), params.getKeyword(), params.getOrder());
		
		List<Product> productList = (List<Product>) ps.getResultList();
		productCommentService.setGoodComments(productList);
		
		//转Dto
		AppPageSupport<AppStoreProdDto> pageSupportDto =  PageUtils.convertPageSupport(ps,new PageUtils.ResultListConvertor<Product, AppStoreProdDto>() {

			@Override
			public List<AppStoreProdDto> convert(List<Product> form) {
				
				List<AppStoreProdDto> toList = new ArrayList<AppStoreProdDto>();
				
				for (Product product : form) {
					
					if (AppUtils.isNotBlank(product)) {
						
						AppStoreProdDto appStoreProdDto =  convertToAppStoreProdDto(product);
						toList.add(appStoreProdDto);
					}
				}
				return toList;
			}
			
			private AppStoreProdDto convertToAppStoreProdDto(Product product){
				  AppStoreProdDto appStoreProdDto = new AppStoreProdDto();
				  appStoreProdDto.setPrice(product.getPrice());
				  appStoreProdDto.setName(product.getName());
				  appStoreProdDto.setProdId(product.getProdId());
				  appStoreProdDto.setPic(product.getPic());
				  appStoreProdDto.setGoodCommentsPercent(product.getGoodCommentsPercent());
				  appStoreProdDto.setCash(product.getCash());
				  appStoreProdDto.setBuys(product.getBuys());
				  appStoreProdDto.setComments(product.getComments());
				  return appStoreProdDto;
				}
		});
		return pageSupportDto;
	}
	
	@Override
	public AppProductDetailDto getProductDetailDto(Long prodId) {
		
		ProductDto prod = productService.getProductDto(prodId);
		if(prod  == null){
			return null;
		}
		
		//计算 营销活动
		calculateMarketing(prod);
		
		return convertProductDetailDto(prod);
	}
	
	private void calculateMarketing(ProductDto prod){
		List<SkuDto> list=prod.getSkuDtoList();
		if(AppUtils.isBlank(list)){
			return ;
		}
		if(!ProductTypeEnum.PRODUCT.value().equals(prod.getProdType())){
			return ;
		}
		if( prod.getIsGroup()!=null && prod.getIsGroup().intValue()==1){ //如果是团购,不参与计算营销
			return ;
		}
		for(SkuDto skuDto:list){
			RuleResolver resolver=new RuleResolver();
			resolver.setPrice(skuDto.getPrice());
			resolver.setPromotionPrice(skuDto.getPrice());
			resolver.setBasketCount(1);
			resolver.setShopId(prod.getShopId());
			resolver.setProdId(prod.getProdId());
			resolver.setSkuId(skuDto.getSkuId());
			/**
			 * 查询该商品的参与的营销规则-->计算价格
			 */
			activeRuleResolverManager.executorMarketing(resolver);
			
			List<CartMarketRules> marketRules=resolver.getCartMarketRules();
			skuDto.setPromotionPrice(resolver.getPromotionPrice()); //设置计算营销后的价格
			skuDto.setRuleList(marketRules);
			resolver=null;
		}
	}
	
	@Override
	public AppPageSupport<AppProdCommDto> queryProductComments(AppQueryProductCommentsParams params,String userId) {
		AppPageSupport<AppProdCommDto> aps = appProductDao.queryProductComments(params);
		//处理图片路径
		List<AppProdCommDto> resultList = aps.getResultList();
		if(AppUtils.isNotBlank(resultList)){
			for (AppProdCommDto appProdCommDto : resultList) {
				//处理评论图片路径
				String photos = appProdCommDto.getPhotos();
				if(AppUtils.isNotBlank(photos)){
					String[] photoArray = photos.split(",");
					List<String> photoList = Arrays.asList(photoArray);
					appProdCommDto.setPhotoList(photoList);
				}
				
				//处理追加评论图片路径
				String addPhotos = appProdCommDto.getAddPhotos();
				if(AppUtils.isNotBlank(addPhotos)){
					String[] AddPhotoArray = addPhotos.split(",");
					List<String> addPhotoList = Arrays.asList(AddPhotoArray);
					appProdCommDto.setAddPhotoList(addPhotoList);
				}
				
				//如果用户登录了，查询用户是否给该评论点赞
				if(AppUtils.isNotBlank(userId) && appProdCommDto.getUsefulCounts() > 0){
					boolean isAlreadyUseful = productCommentService.isAlreadyUseful(appProdCommDto.getId(), userId);
					appProdCommDto.setIsAlreadyUseful(isAlreadyUseful);
				}
				
				//如果追评不为空，则获取多少天后追评的
				appProdCommDto.setAppendDays(appProdCommDto.getAppendDays());
			}
		}
		return aps;
	}
	
	/**
	 * @Description: 将productDto转ProductDetailDto
	 * @date 2016-7-22 
	 */
	private AppProductDetailDto convertProductDetailDto(ProductDto productDto){
		AppProductDetailDto productDetailDto = new AppProductDetailDto();
		productDetailDto.setName(productDto.getName());
		productDetailDto.setStatus(productDto.getStatus());
		productDetailDto.setProdId(productDto.getProdId());
		productDetailDto.setSkuList(convertSkuDto(productDto.getSkuDtoList()));  //sku集合
		productDetailDto.setPic(productDto.getPic());
		productDetailDto.setShopId(productDto.getShopId());
		productDetailDto.setPrice(productDto.getPrice());
		productDetailDto.setCash(productDto.getCash());
		productDetailDto.setContent(productDto.getContentM());//手机端图文详情
		productDetailDto.setProdPropList(convertProductPropertyDto(productDto.getProdPropDtoList()));
		productDetailDto.setProdPics(convertProductPicDto(productDto.getProdPics(), productDto.getPropValueImgList()));
		productDetailDto.setBrief(productDto.getBrief());
		productDetailDto.setBrandId(productDto.getBrandId());
		productDetailDto.setBrandName(productDto.getBrandName());
		productDetailDto.setPropValueImgList(convertPropValueImgList(productDto.getPropValueImgList()));//商品属性图片集合
		productDetailDto.setStartDate(productDto.getStartDate());
		productDetailDto.setProVideoUrl(productDto.getProVideoUrl());
		return productDetailDto;
	}

	/**
	 * 将model sku转换app SkuDto
	 */
	private List<AppSkuDto> convertSkuDto(List<SkuDto> skuDtoList){
		if(AppUtils.isNotBlank(skuDtoList)){
			List<AppSkuDto> appSkuDtoList = new ArrayList<AppSkuDto>();
			for (SkuDto skuDto : skuDtoList) {
				AppSkuDto appSkuDto = new AppSkuDto();
				appSkuDto.setSkuId(skuDto.getSkuId());
				appSkuDto.setName(skuDto.getName());
				appSkuDto.setStatus(skuDto.getStatus());
				appSkuDto.setStocks(skuDto.getStocks());
				appSkuDto.setCash(skuDto.getCash());
				appSkuDto.setPromotionPrice(skuDto.getPromotionPrice());
				appSkuDto.setProperties(skuDto.getProperties());
				appSkuDto.setRuleList(skuDto.getRuleList());
				appSkuDto.setModelId(skuDto.getModelId());
				appSkuDto.setPrice(skuDto.getPrice());
				appSkuDto.setSkuId(skuDto.getSkuId());
				appSkuDtoList.add(appSkuDto);
			}
			return appSkuDtoList;
		}
		
		return null;
	}
	
	
	/**
	 * 转换商品属性图片
	 */
	private List<AppPropValueImgDto> convertPropValueImgList(List<PropValueImgDto> proValueImgs){
		if(AppUtils.isBlank(proValueImgs)){
			return null;
		}
		List<AppPropValueImgDto> appProValueImgs=new ArrayList<AppPropValueImgDto>();
		for(PropValueImgDto imgDto : proValueImgs){
			AppPropValueImgDto appImgDto=new AppPropValueImgDto();
			appImgDto.setValueId(imgDto.getValueId());
			appImgDto.setValueName(imgDto.getValueName());
			appImgDto.setPropId(imgDto.getPropId());
			appImgDto.setValueImage(imgDto.getValueImage());
			appImgDto.setSeq(imgDto.getSeq());
			appImgDto.setImgList(imgDto.getImgList());
			appProValueImgs.add(appImgDto);
		}
		return appProValueImgs;
	}
	
	
	/**
	 * @Description: 将model MarketingDtos  转换为  app AppMarketingDto
	 * @date 2016-7-22 
	 */
/*	private List<CartMarketRules> convertMarketingDto(List<CartMarketRules>  cartMarketRules){
		if(AppUtils.isBlank(cartMarketRules)){
			return null;
		}
		
		List<AppMarketingDto> appMarks=new ArrayList<AppMarketingDto>();
		for(MarketingDto  marks:MarketingDtos){
			AppMarketingDto appMark=new AppMarketingDto();
			appMark.setMarketId(marks.getMarketId());
			appMark.setMarketName(marks.getMarketName());
			appMark.setStartTime(marks.getStartTime());
			appMark.setEndTime(marks.getEndTime());
			appMark.setShopId(marks.getShopId());
			appMark.setProdId(marks.getProdId());
			appMark.setSkuId(marks.getSkuId());
			appMark.setCreateTime(marks.getCreateTime());
			appMark.setType(marks.getType());
			appMark.setIsAllProds(marks.getIsAllProds());
			appMark.setMarketingRuleDtos(convertMarketingRuleDto(marks.getMarketingRuleDtos()));//转化促销规则
			appMark.setMarketingProdDtos(convertMarketingProdDto(marks.getMarketingProdDtos()));//转化产品规则
			appMarks.add(appMark);
		}
		return appMarks;
	}
	*/
	
	/**
	 * @Description: 转化促销规则
	 * @date 2016-7-22 
	 */
/*	private List<AppMarketingRuleDto> convertMarketingRuleDto(List<MarketingRuleDto>  markRuleDtos){
		if(AppUtils.isBlank(markRuleDtos)){
			return null;
		}
		List<AppMarketingRuleDto>  appMarksRules=new ArrayList<AppMarketingRuleDto>();
		for(MarketingRuleDto markRuleDto:markRuleDtos){
			AppMarketingRuleDto appMarkRuleDto=new AppMarketingRuleDto();
			appMarkRuleDto.setMarketId(markRuleDto.getMarketId());
			appMarkRuleDto.setShopId(markRuleDto.getShopId());
			appMarkRuleDto.setRuleId(markRuleDto.getRuleId());
			appMarkRuleDto.setMarketRuleNm(markRuleDto.getMarketRuleNm());
			appMarkRuleDto.setOffAmount(markRuleDto.getOffAmount());
			appMarkRuleDto.setFullPrice(markRuleDto.getOffAmount());
			appMarkRuleDto.setOffDiscount(markRuleDto.getOffDiscount());
			appMarkRuleDto.setCutAmount(markRuleDto.getCutAmount());
			appMarkRuleDto.setType(markRuleDto.getType());
			appMarksRules.add(appMarkRuleDto);
		}
		return appMarksRules;
	}
	*/
	
	/**
	 * @Description: 转化产品规则
	 * @date 2016-7-22 
	 */
/*	private List<AppMarketingProdDto> convertMarketingProdDto(List<MarketingProdDto> marketingProdDtos){
		if(AppUtils.isBlank(marketingProdDtos)){
			return null;
		}
		List<AppMarketingProdDto> appMarketingProds=new ArrayList<AppMarketingProdDto>();
		for(MarketingProdDto marketingProdDto:marketingProdDtos){
			AppMarketingProdDto appMarketingProdDto=new AppMarketingProdDto();
			appMarketingProdDto.setMarketId(marketingProdDto.getMarketId());
			appMarketingProdDto.setId(marketingProdDto.getId());
			appMarketingProdDto.setShopId(marketingProdDto.getShopId());
			appMarketingProdDto.setProdId(marketingProdDto.getShopId());
			appMarketingProdDto.setProdName(marketingProdDto.getProdName());
			//appMarketingProdDto.setSkuName(marketingProdDto.getSkuName());
			appMarketingProdDto.setCash(marketingProdDto.getCash());
			//appMarketingProdDto.setSkuCash(marketingProdDto.getSkuCash());
			appMarketingProdDto.setStock(marketingProdDto.getStock());
			//appMarketingProdDto.setSkuStock(marketingProdDto.getSkuStock());
			appMarketingProdDto.setSkuId(marketingProdDto.getSkuId());
			appMarketingProdDto.setPic(marketingProdDto.getPic());
			appMarketingProdDto.setDiscount(marketingProdDto.getDiscount());
			appMarketingProdDto.setDiscountPrice(marketingProdDto.getDiscountPrice());
			appMarketingProds.add(appMarketingProdDto);
		}
		return appMarketingProds;
	}*/
	
	/**
	 * @Description: 价格明细 转换
	 * @date 2016-7-22 
	 */
	/*private AppAmountDetailDto convertAmountDetailDto(AmountDetailDto amountDetailDto){
		if(AppUtils.isBlank(amountDetailDto)){
			return null;
		}
		AppAmountDetailDto appAmountDetailDto=new AppAmountDetailDto();
		appAmountDetailDto.setPromotionPrice(amountDetailDto.getPromotionPrice());
		appAmountDetailDto.setPrice(amountDetailDto.getPrice());
		appAmountDetailDto.setCash(amountDetailDto.getCash());
		appAmountDetailDto.setNum(amountDetailDto.getNum());
		appAmountDetailDto.setTotalAmount(amountDetailDto.getTotalAmount());
		appAmountDetailDto.setAmountNm(amountDetailDto.getAmountNm());
		appAmountDetailDto.setType(amountDetailDto.getType());
		return appAmountDetailDto;
	}*/
	

	private List<String> convertProductPicDto(List<ProdPicDto> prodPicDtoList, List<PropValueImgDto> propValImgList){
		List<String> productPics = new ArrayList<String>();
		if(AppUtils.isNotBlank(propValImgList)){
			for (PropValueImgDto propValImg:propValImgList) {
				productPics.addAll(propValImg.getImgList());
			}
		}else{
			if(AppUtils.isNotBlank(prodPicDtoList)){
				for (ProdPicDto prodPicDto : prodPicDtoList) {
					productPics.add(prodPicDto.getFilePath());
				}
			}
		}
		return productPics;
	}

	private List<AppProductPropertyDto> convertProductPropertyDto(List<ProductPropertyDto> productPropertyDtoList){
		if(AppUtils.isNotBlank(productPropertyDtoList)){
			List<AppProductPropertyDto> appPropertyDtoList = new ArrayList<AppProductPropertyDto>();
			for (ProductPropertyDto productPropertyDto : productPropertyDtoList) {
				AppProductPropertyDto appProductPropertyDto = new AppProductPropertyDto();
				appProductPropertyDto.setPropId(productPropertyDto.getPropId());
				appProductPropertyDto.setPropName(productPropertyDto.getPropName());
				appProductPropertyDto.setProdPropValList(convertProductPropertyValueDto(productPropertyDto.getProductPropertyValueList()));
				appPropertyDtoList.add(appProductPropertyDto);
			}
			return appPropertyDtoList;
		}
		
		return null;
	}

	private List<AppProductPropertyValueDto> convertProductPropertyValueDto(List<com.legendshop.model.dto.ProductPropertyValueDto> productPropertyValueDtoList){
		if(AppUtils.isNotBlank(productPropertyValueDtoList)){
			List<AppProductPropertyValueDto> appPropertyValueDtoList = new ArrayList<AppProductPropertyValueDto>();
			for (com.legendshop.model.dto.ProductPropertyValueDto productPropertyValueDto : productPropertyValueDtoList) {
				AppProductPropertyValueDto appProductPropertyValueDto = new AppProductPropertyValueDto();
				appProductPropertyValueDto.setIsSelected(productPropertyValueDto.getIsSelected());
				appProductPropertyValueDto.setPropId(productPropertyValueDto.getPropId());
				appProductPropertyValueDto.setName(productPropertyValueDto.getName());
				appProductPropertyValueDto.setValueId(productPropertyValueDto.getValueId());
				appPropertyValueDtoList.add(appProductPropertyValueDto);
			}
			return appPropertyValueDtoList;
		}
		
		return null;
	}

	@Override
	public List<AppParamGroupDto> getParamGroups(Long prodId) {
		List<ParamGroupDto> paramGroupDtos = productService.getParamGroups(prodId);
		List<AppParamGroupDto> paramGroups = new ArrayList<AppParamGroupDto>();
		if(AppUtils.isNotBlank(paramGroupDtos)){
			for (ParamGroupDto paramGroupDto : paramGroupDtos) {
				AppParamGroupDto paramGroup = new AppParamGroupDto();
				paramGroup.setGroupName(paramGroupDto.getGroupName());
				List<ProdParamDto> prodParamDtos = paramGroupDto.getParams();
				List<AppProdParamDto> params = new ArrayList<AppProdParamDto>();
				for (ProdParamDto prodParamDto : prodParamDtos) {
					AppProdParamDto param = new AppProdParamDto();
					param.setParamName(prodParamDto.getParamName());
					param.setParamValueName(prodParamDto.getParamValueName());
					params.add(param);
				}
				paramGroup.setParams(params);
				paramGroups.add(paramGroup);
			}
		}
		return paramGroups;
	}


	@Override
	public List<AppStoreProdDto> getHotProd(Long shopId, int maxNum) {
		
		List<AppStoreProdDto> prodList = appProductDao.getAppHotProd(shopId, maxNum);
		
		NumberFormat nt = NumberFormat.getPercentInstance();
		//计算商品评论数
		for (AppStoreProdDto appStoreProdDto : prodList) {
			
			if(AppUtils.isNotBlank(appStoreProdDto.getComments()) && appStoreProdDto.getComments() != 0){
				//通过商品ID找到改商品好评(score>3)的数目
				Long num = productCommentDao.setGoodComments(appStoreProdDto.getProdId());
				double percent = (double)num / (double)appStoreProdDto.getComments();
				if (percent > 1d) {
					percent = 1d;
				}
				appStoreProdDto.setGoodCommentsPercent(nt.format(percent));
			}
		}
		return prodList;
	}
	
	/**
	 * 获取门店的商品
	 */
	@Override
	public AppPageSupport<AppStoreProdDto> getStoreProds(AppStoreSearchParams params) {
		return appProductDao.getStoreProds(params);
	}
	
	/**
	 * 获取商品评论列表
	 */
	@Override
	public AppPageSupport<AppCommentReplyDto> queryCommentReplyList(Long prodComId, Integer curPageNO) {
		return appProductDao.queryCommentReplyList(prodComId, curPageNO);
	}
	
	@Override
	public List<AppStoreProdDto> getStoreNewProds(Long shopId, int maxNum) {
		
		List<AppStoreProdDto> list =  appProductDao.getAppNewProds(shopId, maxNum);
		
		NumberFormat nt = NumberFormat.getPercentInstance();
		//计算商品评论数
		for (AppStoreProdDto appStoreProdDto : list) {
			
			if(AppUtils.isNotBlank(appStoreProdDto.getComments()) && appStoreProdDto.getComments() != 0){
				//通过商品ID找到改商品好评(score>3)的数目
				Long num = productCommentDao.setGoodComments(appStoreProdDto.getProdId());
				double percent = (double)num / (double)appStoreProdDto.getComments();
				if (percent > 1d) {
					percent = 1d;
				}
				appStoreProdDto.setGoodCommentsPercent(nt.format(percent));
			}
		}
		return list;
	}

	/**
	 * 获取指定排序规则的商品
	 */
	@Override
	public List<AppProdDto> queryProductByOrder(String groupConditional, Integer pageCount) {
		
		return appProductDao.queryProductByOrder(groupConditional,pageCount);
	}

	/**
	 * 获取自定义商品分组内的商品
	 */
	@Override
	public List<AppProdDto> queryProductByProdGroup(Long prodGroupId, String sort, Integer pageCount) {
		
		return appProductDao.queryProductByProdGroup(prodGroupId,sort,pageCount);
	}

	/**
	 * 根据分类ID获取商品
	 */
	@Override
	public List<AppProdDto> queryProductByCategory(Long categoryId, Integer pageCount) {
		
		return appProductDao.queryProductByCategory(categoryId,pageCount);
	}

	/**
	 * 获取指定排序规则的所有商品
	 */
	@Override
	public AppPageSupport<AppProdDto> queryProductByConditional(String groupConditional,Integer curPageNO) {
		
		return appProductDao.queryProductByConditional(groupConditional,curPageNO);
	}

	/**
	 * 获取分组内的所有商品
	 */
	@Override
	public AppPageSupport<AppProdDto> queryProductByProdGroupId(Long prodGroupId, String sort, Integer curPageNO) {
		
		return appProductDao.queryProductByProdGroupId(prodGroupId,sort,curPageNO);
	}

	/**
	 * 根据店铺分类ID获取商品
	 */
	@Override
	public List<AppProdDto> queryProductByShopCategory(Long shopCategoryId,Integer grade,Integer pageCount) {
		
		return appProductDao.queryProductByShopCategory(shopCategoryId,grade,pageCount);
	}

	/**
	 *  封装促销信息
	 */
/*	private MarketingProdRuleEngineDto buildingEngineDto(ProductDto prod) {
		MarketingProdRuleEngineDto engineDto =new MarketingProdRuleEngineDto();
		engineDto.setProdId(prod.getProdId());
		engineDto.setPrice(prod.getPrice());
		engineDto.setCash(prod.getCash());
		engineDto.setShopId(prod.getShopId());
		if(AppUtils.isNotBlank(prod.getSkuDtoList())){
			List<MarketingSkuRuleEngineDto> engineParams=new ArrayList<MarketingSkuRuleEngineDto>();			
			for (Iterator<SkuDto> iterator = prod.getSkuDtoList().iterator(); iterator.hasNext();) {
				SkuDto type = (SkuDto) iterator.next();
				MarketingSkuRuleEngineDto skuRuleEngineDto=new MarketingSkuRuleEngineDto();
				skuRuleEngineDto.setPrice(type.getPrice());
				skuRuleEngineDto.setSkuId(type.getSkuId());
				engineParams.add(skuRuleEngineDto);
			}
			engineDto.setEngineParams(engineParams);
		}
		return engineDto;
	}*/

}
