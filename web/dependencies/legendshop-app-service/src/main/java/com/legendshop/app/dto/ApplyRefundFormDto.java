package com.legendshop.app.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app 申请退款参数封装Dto
 * @author linzh
 */
@ApiModel(value="申请退款Dto")
public class ApplyRefundFormDto extends ApplyRefundReturnBaseDto implements Serializable {

	/**  */
	private static final long serialVersionUID = -9121464533762894163L;

	/** 订单项id */
	@ApiModelProperty(value="订单项id[订单项退款才需要传的]",required=false)
	private Long orderItemId;
	

	public Long getOrderItemId() {
		return orderItemId;
	}

	public void setOrderItemId(Long orderItemId) {
		this.orderItemId = orderItemId;
	}
	
}
