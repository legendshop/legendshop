/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.AppMyPromoterUserDto;
import com.legendshop.app.service.AppUserCommisService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.business.dao.PdCashLogDao;
import com.legendshop.business.dao.UserDetailDao;
import com.legendshop.business.dao.promoter.DistSetDao;
import com.legendshop.business.dao.promoter.UserCommisDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.promoter.model.UserCommis;
import com.legendshop.spi.service.SMSLogService;
import com.legendshop.util.AppUtils;

/**
 * 用户佣金ServiceImpl
 */
@Service("appUserCommisService")
public class AppUserCommisServiceImpl implements AppUserCommisService {

	@Autowired
	private UserCommisDao userCommisDao;

	@Autowired
	private UserDetailDao userDetailDao;

	@Autowired
	private PdCashLogDao pdCashLogDao;
	
	@Autowired
	private SMSLogService smsLogService;
	
	@Autowired
	private DistSetDao distSetDao;

	/**
	 * 加载下级用户列表 
	 */
	@Override
	public AppPageSupport<AppMyPromoterUserDto> getUserCommis(String curPageNO, String userName, UserCommis userCommis, int pageSize) {
		PageSupport<UserCommis> ps = userCommisDao.getUserCommisByName(curPageNO, userName, userCommis, pageSize);
		AppPageSupport<AppMyPromoterUserDto> convertPageSupport = PageUtils.convertPageSupport(ps,new PageUtils.ResultListConvertor<UserCommis, AppMyPromoterUserDto>() {
			@Override
			public List<AppMyPromoterUserDto> convert(List<UserCommis> form) {
				List<AppMyPromoterUserDto> toList = new ArrayList<AppMyPromoterUserDto>();
				for (UserCommis userCommis : form) {
					if (AppUtils.isNotBlank(userCommis)) {
						AppMyPromoterUserDto dto =  convertToAppMyPromoterUserDto(userCommis);
						toList.add(dto);
					}
				}
				return toList;
			}
			
			private AppMyPromoterUserDto convertToAppMyPromoterUserDto(UserCommis userCommis){
				AppMyPromoterUserDto appMyPromoterUserDto = new AppMyPromoterUserDto();
				appMyPromoterUserDto.setParentBindingTime(userCommis.getParentBindingTime());
				appMyPromoterUserDto.setTotalDistCommis(userCommis.getTotalDistCommis());
				appMyPromoterUserDto.setUserMobile(userCommis.getUserMobile());
				appMyPromoterUserDto.setUserName(userCommis.getUserName());
				return appMyPromoterUserDto;
			}
		});
		return convertPageSupport;
	}

}
