/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service;

import java.sql.Date;

import com.legendshop.model.dto.app.AppUserDetailDto;
import com.legendshop.model.entity.UserSecurity;

/**
 * app 用户信息service
 */
public interface AppUserDetailService {

	/**
	 * 获取用户信息详情
	 *
	 * @param userId the user id
	 * @return the user detail
	 */
	public AppUserDetailDto getUserDetail(String userId);

	/**
	 * 更新昵称
	 *
	 * @param userId the user id
	 * @param nickName the nick name
	 * @return the int
	 */
	public  int updateNickName(String userId, String nickName);
	
	/**
	 * 更新姓名
	 *
	 * @param userId the user id
	 * @param nickName the nick name
	 * @return the int
	 */
	public  int updateRealName(String userId, String realName);

	/**
	 * 更新性别
	 *
	 * @param userId the user id
	 * @param sex the sex
	 * @return the int
	 */
	public int updateSex(String userId, String sex);

	/**
	 * 更新头像
	 * @param userId
	 * @param portraitPic 上传的头像地址
	 * @return
	 */
	public int updatePortrait(String userId, String portraitPic);
	
	/**
	 * @Description: 获取用户安全信息
	 * @date 2016-7-14 
	 */
	public UserSecurity getUserSecurity(String userName);

	/**
	 * 获取用户手机号码
	 * @param userId
	 * @return
	 */
	public String getUserMobile(String userId);

	/***
	 * 更新生日
	 * @param userId
	 * @param birthDate
	 * @return
	 */
	public int updateBirthDate(String userId,Date birthDate);

	/**
	 * 更新用户签名
	 * @param userId
	 * @param userSignature
	 * @return
	 */
    int updateSignature(String userId, String userSignature);
}
