/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;

import java.io.Serializable;

import com.legendshop.model.dto.order.KuaiDiLogs.Status;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 申请推广员返回给app端的参数
 *
 */
@ApiModel(value="申请推广员返回给app端的参数")
public class AppApplyRParamDto {

	/** 是否需要真实姓名 */
	@ApiModelProperty(value="是否需要真实姓名")
	private Integer chkRealName;

	/** 是否需要邮箱 */
	@ApiModelProperty(value="是否需要邮箱")
	private Integer chkMail;

	/** 是否需要手机 */
	@ApiModelProperty(value="是否需要手机")
	private Integer chkMobile;

	/** 是否需要住址 */
	@ApiModelProperty(value="是否需要住址")
	private Integer chkAddr;

	/** 真实姓名 */
	@ApiModelProperty(value="真实姓名")
	private String realName;

	/** 用户名 */
	@ApiModelProperty(value="用户名")
	private String userName;
	
	/** 用户电话 */
	@ApiModelProperty(value="用户电话")
	private String userMobile;
	
	/** 用户邮箱 */
	@ApiModelProperty(value="用户邮箱")
	private String userMail;
	
	/** 用户地址 */
	@ApiModelProperty(value="用户地址")
	private String userAdds;
	
	/** 省份 */
	@ApiModelProperty(value="省份Id")
	protected Integer provinceid;
	
	/** 省份 */
	@ApiModelProperty(value="省份名称")
	private String province;
	
	/** 城市 */
	@ApiModelProperty(value="城市Id")
	protected Integer cityid;
	
	/** 省份 */
	@ApiModelProperty(value="城市名称")
	private String city;
	
	/** 地区 */
	@ApiModelProperty(value="地区Id")
	protected Integer areaid;
	
	/** 省份 */
	@ApiModelProperty(value="地区名称")
	private String area;
	
	/** 推广员状态 */
	@ApiModelProperty(value="推广员状态:-2为等待审批;-1为被拒绝;0为到填写申请;1为显示推广链接")
	private Integer status;

	/** 前端域名*/
	@ApiModelProperty(value="前端域名")
	private String vueDomain;
	
	/**
	 * Gets the chk real name.
	 *
	 * @return the chk real name
	 */
	public Integer getChkRealName() {
		return chkRealName;
	}

	/**
	 * Sets the chk real name.
	 *
	 * @param chkRealName
	 *            the chk real name
	 */
	public void setChkRealName(Integer chkRealName) {
		this.chkRealName = chkRealName;
	}

	/**
	 * Gets the chk mail.
	 *
	 * @return the chk mail
	 */
	public Integer getChkMail() {
		return chkMail;
	}

	/**
	 * Sets the chk mail.
	 *
	 * @param chkMail
	 *            the chk mail
	 */
	public void setChkMail(Integer chkMail) {
		this.chkMail = chkMail;
	}

	/**
	 * Gets the chk mobile.
	 *
	 * @return the chk mobile
	 */
	public Integer getChkMobile() {
		return chkMobile;
	}

	/**
	 * Sets the chk mobile.
	 *
	 * @param chkMobile
	 *            the chk mobile
	 */
	public void setChkMobile(Integer chkMobile) {
		this.chkMobile = chkMobile;
	}

	/**
	 * Gets the chk addr.
	 *
	 * @return the chk addr
	 */
	public Integer getChkAddr() {
		return chkAddr;
	}

	/**
	 * Sets the chk addr.
	 *
	 * @param chkAddr
	 *            the chk addr
	 */
	public void setChkAddr(Integer chkAddr) {
		this.chkAddr = chkAddr;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the user mobile.
	 *
	 * @return the user mobile
	 */
	public String getUserMobile() {
		return userMobile;
	}

	/**
	 * Sets the user mobile.
	 *
	 * @param userMobile
	 *            the user mobile
	 */
	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	/**
	 * Gets the user mail.
	 *
	 * @return the user mail
	 */
	public String getUserMail() {
		return userMail;
	}

	/**
	 * Sets the user mail.
	 *
	 * @param userMail
	 *            the user mail
	 */
	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	/**
	 * Gets the user adds.
	 *
	 * @return the user adds
	 */
	public String getUserAdds() {
		return userAdds;
	}

	/**
	 * Sets the user adds.
	 *
	 * @param userAdds
	 *            the user adds
	 */
	public void setUserAdds(String userAdds) {
		this.userAdds = userAdds;
	}

	/**
	 * Gets the provinceid.
	 *
	 * @return the provinceid
	 */
	public Integer getProvinceid() {
		return provinceid;
	}

	/**
	 * Sets the provinceid.
	 *
	 * @param provinceid
	 *            the provinceid
	 */
	public void setProvinceid(Integer provinceid) {
		this.provinceid = provinceid;
	}

	/**
	 * Gets the cityid.
	 *
	 * @return the cityid
	 */
	public Integer getCityid() {
		return cityid;
	}

	/**
	 * Sets the cityid.
	 *
	 * @param cityid
	 *            the cityid
	 */
	public void setCityid(Integer cityid) {
		this.cityid = cityid;
	}

	/**
	 * Gets the areaid.
	 *
	 * @return the areaid
	 */
	public Integer getAreaid() {
		return areaid;
	}

	/**
	 * Sets the areaid.
	 *
	 * @param areaid
	 *            the areaid
	 */
	public void setAreaid(Integer areaid) {
		this.areaid = areaid;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getVueDomain() {
		return vueDomain;
	}

	public void setVueDomain(String vueDomain) {
		this.vueDomain = vueDomain;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}
}
