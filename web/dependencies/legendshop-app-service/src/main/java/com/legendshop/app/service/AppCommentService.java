package com.legendshop.app.service;

import com.legendshop.app.dto.UserCommentDetailDto;
import com.legendshop.model.dto.ProductCommentDto;
import com.legendshop.model.dto.UserAddCommentDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.UserCommentProdDto;

public interface AppCommentService {

	public AppPageSupport<UserCommentProdDto> queryComments(String curPageNO,String userName);

	public UserCommentDetailDto getCommDetail(Long commId, String userId);

	public AppPageSupport<ProductCommentDto> prodCommentState(String curPageNO, String state, String userName);

	/**
	 * 获取用户评论相关内容
	 */
	public UserAddCommentDto getUserAddCommentDto(Long prodCommId, String userId);
	
}
