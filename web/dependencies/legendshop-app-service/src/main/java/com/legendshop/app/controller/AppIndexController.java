/*
 *
 * LegendShop 多用户商城系统
 *
 *  版权所有,并保留所有权利。
 *
 */
package com.legendshop.app.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.AppProdDto;
import com.legendshop.app.service.AppProductService;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.appdecorate.ProdGroupConditionalDto;
import com.legendshop.model.entity.ProdGroup;
import com.legendshop.model.entity.appDecorate.AppPageManage;
import com.legendshop.spi.service.AppPageManageService;
import com.legendshop.spi.service.ProdGroupService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;

import cn.hutool.json.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 首页Controller
 */
@RestController
@Api(tags="首页",value="首页展示相关操作")
public class AppIndexController{

	@Autowired
	private AppPageManageService appPageManageService;

	@Autowired
	private ProdGroupService prodGroupService;

	@Autowired
	private AppProductService appProductService;



	@ApiOperation(value = "新首页展示", httpMethod = "GET", notes = "获取首页页面装修数据进行展示",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@GetMapping("/index")
	public ResultDto<JSONObject> index() {

		//使用中的页面
		Integer isUse = 1;
		AppPageManage appPageManage = appPageManageService.getUseAppPageByUse(isUse);
		if (AppUtils.isBlank(appPageManage) || AppUtils.isBlank(appPageManage.getReleaseData())) {

			return ResultDtoManager.fail(-1,"暂无装修数据");
		}
		String releaseData = appPageManage.getReleaseData();
		JSONObject appDecorateDate = cn.hutool.json.JSONUtil.parseObj(releaseData);

		return ResultDtoManager.success(appDecorateDate);
	}



	@ApiOperation(value = "获取商品分组商品", httpMethod = "POST", notes = "获取商品分组下的商品",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "prodGroupId", value = "商品分组ID", dataType = "Long", required = true),
		@ApiImplicitParam(paramType="query", name = "pageCount", value = "获取数量", dataType = "int")
	})
	@PostMapping("/groupProd")
	public ResultDto<List<AppProdDto>> getProdByGroup(Long prodGroupId,Integer pageCount) {

		ProdGroup prodGroup = prodGroupService.getProdGroup(prodGroupId);
		if (AppUtils.isBlank(prodGroup)) {
			return ResultDtoManager.fail(-1, "该分组不存在或已被删除");
		}

		List<AppProdDto> prodList = new ArrayList<AppProdDto>();

		if (prodGroup.getType() == 0) { //系统定义

			//获取系统定义分组的排序条件
			String conditional = prodGroup.getConditional();
			ProdGroupConditionalDto prodGroupConditionalDto = JSONUtil.getObject(conditional, ProdGroupConditionalDto.class);
			String groupConditional = prodGroupConditionalDto.getType();

			prodList = appProductService.queryProductByOrder(groupConditional,pageCount);
			return ResultDtoManager.success(prodList);

		}
		//获取自定义分组内的排序
		String sort = prodGroup.getSort();

		prodList = appProductService.queryProductByProdGroup(prodGroup.getId(),sort,pageCount);
		return ResultDtoManager.success(prodList);
	}


	@ApiOperation(value = "获取分类下的商品", httpMethod = "POST", notes = "获取分类下的商品",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "categoryId", value = "分类ID", dataType = "Long",required = true),
		@ApiImplicitParam(paramType="query", name = "pageCount", value = "获取数量", dataType = "int",required = true)
	})
	@PostMapping("/categoryProd")
	public ResultDto<List<AppProdDto>> categoryProd(Long categoryId,Integer pageCount) {

		if (AppUtils.isBlank(categoryId)) {

			return ResultDtoManager.fail(-1, "该分类不存在或已被删除");
		}
		List<AppProdDto> prodList = appProductService.queryProductByCategory(categoryId,pageCount);
		return ResultDtoManager.success(prodList);
	}


	@ApiOperation(value = "获取店铺分类下的商品", httpMethod = "POST", notes = "获取店铺分类下的商品",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "shopCategoryId", value = "店铺分类ID", dataType = "Long",required = true),
		@ApiImplicitParam(paramType="query", name = "grade", value = "分类等级", dataType = "int",required = true),
		@ApiImplicitParam(paramType="query", name = "pageCount", value = "获取数量", dataType = "int",required = true)
	})
	@PostMapping("/shopCategoryProd")
	public ResultDto<List<AppProdDto>> shopCategoryProd(Long shopCategoryId,Integer grade,Integer pageCount) {

		if (AppUtils.isBlank(shopCategoryId)) {

			return ResultDtoManager.fail(-1, "该分类不存在或已被删除");
		}
		List<AppProdDto> prodList = appProductService.queryProductByShopCategory(shopCategoryId,grade,pageCount);

		return ResultDtoManager.success(prodList);
	}


	@ApiOperation(value = "获取海报页内容", httpMethod = "POST", notes = "获取海报页内容",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "pageId", value = "页面ID", dataType = "Long",required = true),

	})
	@PostMapping("/showPosterPage")
	public ResultDto<JSONObject> showPosterPage(Long pageId) {


		AppPageManage appPageManage = appPageManageService.getAppPageManage(pageId);

		if (AppUtils.isBlank(appPageManage) || AppUtils.isBlank(appPageManage.getReleaseData())) {

			return ResultDtoManager.fail(-1,"暂无数据");
		}
		String releaseData = appPageManage.getReleaseData();
		JSONObject appDecorateDate = cn.hutool.json.JSONUtil.parseObj(releaseData);

		return ResultDtoManager.success(appDecorateDate);
	}

}
