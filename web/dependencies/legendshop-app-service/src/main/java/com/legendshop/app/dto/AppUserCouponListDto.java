/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用户优惠卷集合Dto
 *
 */
@ApiModel(value="用户优惠卷集合Dto")
public class AppUserCouponListDto {

	/** 可用的优惠券和红包 */
	@ApiModelProperty(value="可用的优惠券和红包")
	private List<AppUserCouponDto> usableUserCouponList ;
	
	/** 不可用的优惠券和红包 */
	@ApiModelProperty(value="不可用的优惠券和红包")
	private List<AppUserCouponDto> disableUserCouponList;

	public List<AppUserCouponDto> getUsableUserCouponList() {
		return usableUserCouponList;
	}

	public void setUsableUserCouponList(List<AppUserCouponDto> usableUserCouponList) {
		this.usableUserCouponList = usableUserCouponList;
	}

	public List<AppUserCouponDto> getDisableUserCouponList() {
		return disableUserCouponList;
	}

	public void setDisableUserCouponList(List<AppUserCouponDto> disableUserCouponList) {
		this.disableUserCouponList = disableUserCouponList;
	}
	
	
	
}
