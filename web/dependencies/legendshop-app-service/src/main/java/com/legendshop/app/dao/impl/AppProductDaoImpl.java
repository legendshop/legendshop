/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.legendshop.app.dao.AppProductDao;
import com.legendshop.app.dto.AppCommentReplyDto;
import com.legendshop.app.dto.AppProdDto;
import com.legendshop.app.dto.AppStoreProdDto;
import com.legendshop.biz.model.dto.AppProdCommDto;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.ProdOrderWayEnum;
import com.legendshop.model.constant.SortTypeEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.AppQueryProductCommentsParams;
import com.legendshop.model.dto.app.store.AppStoreSearchParams;
import com.legendshop.model.entity.Product;

/**
 * App商品Dao.
 */
@Repository
public class AppProductDaoImpl extends GenericDaoImpl<Product, Long> implements AppProductDao {

	@Override
	public AppPageSupport<AppStoreProdDto> getStoreProds(AppStoreSearchParams params) {
		Integer pageSize = params.getPageSize();
		String curPageNo = String.valueOf(params.getCurPageNO());
		SimpleSqlQuery query = new SimpleSqlQuery(AppStoreProdDto.class, pageSize, curPageNo);
		QueryMap map = new QueryMap();
		
		map.put("shopId", params.getShopId());
		
		//关键字搜索条件
		String keyword = params.getKeyword();
    	map.like("prodName", keyword);
    	map.like("keyword", keyword);
    	map.like("brief", keyword);
		
		//处理排序
		String orderWay = params.getOrderWay();
		String orderType = params.getOrderType();
		if(!SortTypeEnum.instance(orderType)){
			orderType = SortTypeEnum.DESC.value();
		}
		if(ProdOrderWayEnum.recDate.value().equals(orderWay)){//综合
			map.put("order", "prod.buys " + orderType + ", prod.views " + orderType + ", prod.comments " + orderType + ", cash " + orderType);
		}else if(ProdOrderWayEnum.BUYS.value().equals(orderWay)){//销量
			map.put("order", "prod.buys " + orderType);
		}else if(ProdOrderWayEnum.Comments.value().equals(orderWay)){//好评
			map.put("order", "prod.comments " + orderType);
		}else if(ProdOrderWayEnum.CASH.value().equals(orderWay)){//价格
			map.put("order", "prod.cash " + orderType);
		}else{
			map.put("order", "prod.buys " + orderType + ", prod.views " + orderType + ", prod.comments " + orderType + ", cash " + orderType);
		}

		String querySQL = ConfigCode.getInstance().getCode("app.store.getAppSearchProd", map);
		String queryCountSQL = ConfigCode.getInstance().getCode("app.store.getAppSearchProdCount", map);
		query.setQueryString(querySQL);
		query.setAllCountString(queryCountSQL);
		map.remove("order");
		query.setParam(map.toArray());
		
		PageSupport<AppStoreProdDto> ps = querySimplePage(query);
		/*return PageUtils.convertPageSupport(ps, AppStoreProdDto.class);*/
		return null;
	}

	@Override
	public AppPageSupport<AppCommentReplyDto> queryCommentReplyList(Long prodComId, Integer curPageNO) {

		SimpleSqlQuery query = new SimpleSqlQuery(AppCommentReplyDto.class, 20, String.valueOf(curPageNO));
		QueryMap map = new QueryMap();
		
		map.put("prodCommId", prodComId);
		
		String querySQL = ConfigCode.getInstance().getCode("app.prod.queryCommentReplys", map);
		String queryCountSQL = ConfigCode.getInstance().getCode("app.prod.queryCommentReplysCount", map);
		query.setQueryString(querySQL);
		query.setAllCountString(queryCountSQL);
		query.setParam(map.toArray());
		
		PageSupport<AppCommentReplyDto> ps = querySimplePage(query);
		/*return PageUtils.convertPageSupport(ps, AppCommentReplyDto.class);*/
		AppPageSupport<AppCommentReplyDto> appProdCommList = convertToAppPageSupport(ps);
		return appProdCommList;
	}

	/**
	 * 获取商家的评论
	 */
	@Override
	public AppPageSupport<AppProdCommDto> queryProductComments(AppQueryProductCommentsParams params) {

		String curPageNO = String.valueOf(params.getCurPageNO());
		SimpleSqlQuery query = new SimpleSqlQuery(AppProdCommDto.class, 10, curPageNO);
	   	QueryMap map = new QueryMap();
	    map.put("prodId", params.getProdId());
	    
	    String condition = params.getCondition();
	    
	    if(!"all".equals(condition)){//如果不是全部评论
	    	if("good".equals(condition)){
	    		map.put("scoreOperater", " AND c.score >= 4"); 
	    	}else if("medium".equals(condition)){
	    		map.put("scoreOperater", " AND c.score >= 3 AND c.score < 4"); 
	    	}else if("poor".equals(condition)){
	    		map.put("scoreOperater", " AND c.score < 3");
	    	}else if("photo".equals(condition)){
	    		map.put("isHasPhoto", " AND (c.photos IS NOT NULL OR ac.photos IS NOT NULL)");
	    	}else if("append".equals(condition)){
	    		map.put("isAddComm", 1);
	    	}
	    }
	     
	  	String queryAllSQL = ConfigCode.getInstance().getCode("app.prod.queryProductCommentCount", map);
		String querySQL = ConfigCode.getInstance().getCode("app.prod.queryProductComment", map);
		
		query.setAllCountString(queryAllSQL);
		query.setQueryString(querySQL);
		map.remove("scoreOperater");
		map.remove("isHasPhoto");
		
		query.setParam(map.toArray());
		 
		PageSupport<AppProdCommDto> ps = querySimplePage(query);
		/*return PageUtils.convertPageSupport(ps, AppProdCommDto.class);*/
		AppPageSupport<AppProdCommDto> appProdCommList = convertToAppPageSupport(ps);
		return appProdCommList;
	}
	
	@Override
	public List<AppStoreProdDto> getAppHotProd(Long shopId, int maxNum) {
		String sql = ConfigCode.getInstance().getCode("app.store.getHotProd");
		return this.queryLimit(sql, AppStoreProdDto.class, 0, maxNum, shopId);
	}

	@Override
	public List<AppStoreProdDto> getAppNewProds(Long shopId, int maxNum) {
		String sql = ConfigCode.getInstance().getCode("app.store.getNewProds");
		return this.queryLimit(sql, AppStoreProdDto.class, 0, maxNum, shopId);
	}
	
	private <T> AppPageSupport<T> convertToAppPageSupport(PageSupport<T> ps) {
		AppPageSupport<T> result = new AppPageSupport<T>();
		result.setCurrPage(ps.getCurPageNO());
		result.setPageCount(ps.getPageCount());
		result.setPageSize(ps.getPageSize());
		result.setResultList(ps.getResultList());
		result.setTotal(ps.getTotal());
		return result;
	}

	/**
	 * 获取指定排序规则的商品
	 */
	@Override
	public List<AppProdDto> queryProductByOrder(String groupConditional, Integer pageCount) {
		
		String sql = "SELECT *,p.prod_id AS prodId FROM ls_prod p WHERE p.status = 1 ORDER BY "+ groupConditional;
		return queryLimit(sql, AppProdDto.class, 0, pageCount);
	}

	/**
	 * 获取自定义商品分组内的商品
	 */
	@Override
	public List<AppProdDto> queryProductByProdGroup(Long prodGroupId, String sort, Integer pageCount) {
		
		String sql = "SELECT *,lp.prod_id AS prodId FROM ls_prod lp LEFT JOIN ls_prod_group_relevance lg ON lp.prod_id = lg.prod_id WHERE lp.status = 1 AND lg.group_id = ? ORDER BY " + sort +" DESC";
		return queryLimit(sql, AppProdDto.class, 0, pageCount,prodGroupId);
	}

	/**
	 * 根据分类ID获取商品
	 */
	@Override
	public List<AppProdDto> queryProductByCategory(Long categoryId, Integer pageCount) {
		
		String sql = "SELECT *,lp.prod_id AS prodId FROM ls_prod lp  WHERE lp.status = 1 AND lp.category_id = ? ORDER BY lp.buys";
		return queryLimit(sql, AppProdDto.class, 0, pageCount,categoryId);
	}

	
	/**
	 * 获取指定排序规则的所有商品
	 */
	@Override
	public AppPageSupport<AppProdDto> queryProductByConditional(String groupConditional,Integer curPageNO) {
		
		curPageNO = curPageNO == null ? 1 : curPageNO;
		
		SimpleSqlQuery query = new SimpleSqlQuery(AppProdDto.class, 10, String.valueOf(curPageNO));
		QueryMap map = new QueryMap();
		
		map.put("groupConditional",groupConditional);
		
		String querySQL = ConfigCode.getInstance().getCode("app.prod.queryProductByConditional", map);
		String queryCountSQL = ConfigCode.getInstance().getCode("app.prod.queryProductByConditionalCount", map);
		query.setQueryString(querySQL);
		query.setAllCountString(queryCountSQL);
		
		map.remove("groupConditional");
		query.setParam(map.toArray());
		
		PageSupport<AppProdDto> ps = querySimplePage(query);
		return convertToAppPageSupport(ps);
	}

	/**
	 * 获取分组内的所有商品
	 */
	@Override
	public AppPageSupport<AppProdDto> queryProductByProdGroupId(Long prodGroupId, String sort, Integer curPageNO) {
		
		curPageNO = curPageNO == null ? 1 : curPageNO;
		
		SimpleSqlQuery query = new SimpleSqlQuery(AppProdDto.class, 10, String.valueOf(curPageNO));
		QueryMap map = new QueryMap();
		
		map.put("prodGroupId",prodGroupId);
		map.put("sort", sort);
		
		String querySQL = ConfigCode.getInstance().getCode("app.prod.queryProductByProdGroup", map);
		String queryCountSQL = ConfigCode.getInstance().getCode("app.prod.queryProductByProdGroupCount", map);
		query.setQueryString(querySQL);
		query.setAllCountString(queryCountSQL);
		
		map.remove("sort");
		query.setParam(map.toArray());
		
		PageSupport<AppProdDto> ps = querySimplePage(query);
		return convertToAppPageSupport(ps);
		
	}

	/**
	 * 根据店铺分类ID获取商品
	 */
	@Override
	public List<AppProdDto> queryProductByShopCategory(Long shopCategoryId,Integer grade, Integer pageCount) {
		
		String sql = "";
		if (grade == 1) { //店铺一级分类ID
			
			sql = "SELECT *,lp.prod_id AS prodId FROM ls_prod lp  WHERE lp.shop_first_cat_id = ? AND lp.status = 1 ORDER BY lp.buys";
		}else if(grade == 2){ // 店铺二级分类ID
			
			sql = "SELECT *,lp.prod_id AS prodId FROM ls_prod lp  WHERE lp.shop_second_cat_id = ? AND lp.status = 1 ORDER BY lp.buys";
		}else{ // 店铺三级分类ID
			
			sql = "SELECT *,lp.prod_id AS prodId FROM ls_prod lp  WHERE  lp.shop_third_cat_id = ? AND lp.status = 1 ORDER BY lp.buys";
		}
		return queryLimit(sql, AppProdDto.class, 0, pageCount,shopCategoryId);
	}

}
