package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.app.dto.*;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.UserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.AppDiscoverArticleService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.business.dao.AppArticleThumbDao;
import com.legendshop.business.dao.DiscoverArticleDao;
import com.legendshop.business.dao.DiscoverCommeDao;
import com.legendshop.business.dao.DiscoverProdDao;
import com.legendshop.business.dao.ProductCommentDao;
import com.legendshop.business.dao.ProductDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.util.AppUtils;

import net.bytebuddy.asm.Advice.Return;

@Service("appDiscoverArticleService")
public class AppDiscoverArticleServiceImpl implements AppDiscoverArticleService {

    @Autowired
    private DiscoverArticleDao discoverArticleDao;

    @Autowired
    private DiscoverCommeDao discoverCommeDao;

    @Autowired
    private DiscoverProdDao discoverProdDao;

    @Autowired
    private ProductDao productDao;

    @Autowired
    private AppArticleThumbDao appArticleThumbDao;

    @Autowired
    private ProductCommentDao productCommentDao;

    @Autowired
    private LoginedUserService loginedUserService;

    @Autowired
	private UserDetailService userDetailService;

    @Override
    public AppPageSupport<AppDiscoverArticleListDto> queryPage(String pageNo, Integer size, String condition, String order) {
        PageSupport<DiscoverArticle> ps = discoverArticleDao.queryDiscoverArticle(pageNo, size, condition, order);
        AppPageSupport<AppDiscoverArticleListDto> convertPageSupport = PageUtils.convertPageSupport(ps, new PageUtils.ResultListConvertor<DiscoverArticle, AppDiscoverArticleListDto>() {
            @Override
            public List<AppDiscoverArticleListDto> convert(List<DiscoverArticle> discoverArticles) {
                List<AppDiscoverArticleListDto> toList = new ArrayList<AppDiscoverArticleListDto>();
                for (DiscoverArticle discoverArticle : discoverArticles) {
                    if (AppUtils.isNotBlank(discoverArticle)) {
                        AppDiscoverArticleListDto disDto = convertToAppDiscoverArticleListDto(discoverArticle);
                        toList.add(disDto);
                    }
                }
                return toList;
            }

            private AppDiscoverArticleListDto convertToAppDiscoverArticleListDto(DiscoverArticle discoverArticle) {
                AppDiscoverArticleListDto appDiscoverArticleListDto = new AppDiscoverArticleListDto();
                appDiscoverArticleListDto.setId(discoverArticle.getId());
                appDiscoverArticleListDto.setName(discoverArticle.getName());
                appDiscoverArticleListDto.setImage(discoverArticle.getImage());
                appDiscoverArticleListDto.setCreateTime(discoverArticle.getPublishTime());
                appDiscoverArticleListDto.setIntro(discoverArticle.getIntro());
                appDiscoverArticleListDto.setThumbNum(discoverArticle.getThumbNum());
                appDiscoverArticleListDto.setPageView(discoverArticle.getPageView());
                appDiscoverArticleListDto.setWriterName(discoverArticle.getWriterName());
                return appDiscoverArticleListDto;
            }
        });
        return convertPageSupport;
    }

    @Override
    public AppDiscoverArticleInfoDto queryByDisId(Long discoverId) {
        List<AppDiscoverProdDto> prods = new ArrayList<AppDiscoverProdDto>();
        DiscoverArticle discoverArticle = discoverArticleDao.getDiscoverArticle(discoverId);
        if (AppUtils.isNotBlank(discoverArticle)) {
            discoverArticle.setPageView(discoverArticle.getPageView() + 1);
            discoverArticleDao.updateDiscoverArticle(discoverArticle);
            AppDiscoverArticleInfoDto appDiscoverArticleInfoDto = new AppDiscoverArticleInfoDto();
            appDiscoverArticleInfoDto.setId(discoverArticle.getId());
            appDiscoverArticleInfoDto.setName(discoverArticle.getName());
            appDiscoverArticleInfoDto.setThumbNum(discoverArticle.getThumbNum());
            appDiscoverArticleInfoDto.setContent(discoverArticle.getContent());
            appDiscoverArticleInfoDto.setIntro(discoverArticle.getIntro());
            appDiscoverArticleInfoDto.setPageView(discoverArticle.getPageView());
            appDiscoverArticleInfoDto.setPublishTime(discoverArticle.getPublishTime());
            appDiscoverArticleInfoDto.setWriterName(discoverArticle.getWriterName());
            LoginedUserInfo user = loginedUserService.getUser();
            if (AppUtils.isNotBlank(user)) {
                appDiscoverArticleInfoDto.setRsThumb(appArticleThumbDao.rsThumb(appDiscoverArticleInfoDto.getId(), user.getUserId())>0);
            }
            appDiscoverArticleInfoDto.setComcount(discoverCommeDao.getDiscoverCommeByDisId(discoverArticle.getId()).size()+"");

            List<DiscoverProd> discoverProds = discoverProdDao.getDiscoverProdByDisId(discoverArticle.getId());
            if (AppUtils.isNotBlank(discoverProds)) {
                for (DiscoverProd discoverProd : discoverProds) {
                    Product product = productDao.getById(discoverProd.getProdId());
                    if (AppUtils.isNotBlank(product)) {
                        AppDiscoverProdDto appDiscoverProdDto = new AppDiscoverProdDto();
                        appDiscoverProdDto.setProdId(product.getId());
                        appDiscoverProdDto.setGoodCommentsPercent(product.getReviewScores());
                        appDiscoverProdDto.setName(product.getName());
                        appDiscoverProdDto.setPic(product.getPic());
                        appDiscoverProdDto.setPrice(product.getCash());
                        appDiscoverProdDto.setComments(productCommentDao.getComments(product.getId()));
                        prods.add(appDiscoverProdDto);
                    }
                }
                appDiscoverArticleInfoDto.setProds(prods);
                appDiscoverArticleInfoDto.setProcount(prods.size() + "");
            }
            return appDiscoverArticleInfoDto;
        }

        return null;
    }

    @Override
    public AppPageSupport<AppDiscoverCommDto> queryDisCommByGrassId(Long disId, String currPage) {
        PageSupport<DiscoverComme> ps = discoverCommeDao.queryDiscoverCommeByGrassId(currPage,20,disId);
        AppPageSupport<AppDiscoverCommDto> convertPageSupport = PageUtils.convertPageSupport(ps,
                new PageUtils.ResultListConvertor<DiscoverComme, AppDiscoverCommDto>() {
                    @Override
                    public List<AppDiscoverCommDto> convert(List<DiscoverComme> discoverCommes) {
                        List<AppDiscoverCommDto> toList = new ArrayList<AppDiscoverCommDto>();
                        for (DiscoverComme discoverComme : discoverCommes) {
                            if (AppUtils.isNotBlank(discoverComme)) {
                                AppDiscoverCommDto comDto = convertToAppDiscoverCommDtoListDto(discoverComme);
                                toList.add(comDto);
                            }
                        }
                        return toList;
                    }

                    private AppDiscoverCommDto convertToAppDiscoverCommDtoListDto(DiscoverComme discoverComme) {
                        AppDiscoverCommDto appDiscoverCommDto = new AppDiscoverCommDto();
                        appDiscoverCommDto.setId(discoverComme.getId());
                        appDiscoverCommDto.setDisId(discoverComme.getDisId());
                        appDiscoverCommDto.setContent(discoverComme.getContent());
                        appDiscoverCommDto.setCreateTime(discoverComme.getCreateTime());
                        appDiscoverCommDto.setUserId(discoverComme.getUserId());
						UserDetail user = userDetailService.getUserDetailById(discoverComme.getUserId());
						if (AppUtils.isNotBlank(user)){
							appDiscoverCommDto.setUserName(user.getUserName());
							appDiscoverCommDto.setUserImage(user.getPortraitPic());
						}
                        return appDiscoverCommDto;
                    }
                });
        return convertPageSupport;
    }

}
