package com.legendshop.app.dto.mergeGroup;

import java.io.Serializable;
import java.util.List;

import com.legendshop.app.dto.AppCouponDto2;
import com.legendshop.app.dto.AppProductPropertyDto;
import com.legendshop.app.dto.AppPropValueImgDto;
import com.legendshop.app.dto.AppSkuDto;
import com.legendshop.model.dto.ParamGroupDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="拼团商品详情")
public class AppMergeGroupDetailDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty("拼团活动Id")
	private Long id; 
	
	@ApiModelProperty("活动截止时间")
	private Long limitedEndtime;

	@ApiModelProperty("活动开始时间")
	private Long limitedStarttime;


	@ApiModelProperty("拼团人数")
	private Integer peopleNumber; 
		
	@ApiModelProperty("是否团长免单,默认否")
	private Boolean isHeadFree; 
	
	@ApiModelProperty("最低拼团价格")
	private Double minPrice; 
	
	@ApiModelProperty("最低商品价格")
	private Double minProdPrice; 
	
	@ApiModelProperty("是否限购,默认否")
	private Boolean isLimit; 
	
	@ApiModelProperty("限购数量 ")
	private Long limitNumber; 
	
	@ApiModelProperty("参团编号")
	private String addNumber;
	
	@ApiModelProperty("进行中的团购")
	private List<AppMergeGroupingDto> groupingList;
	
	/** 商品ID */
	@ApiModelProperty("商品ID")
	private Long prodId;
	
	/** 商品名称 */
	@ApiModelProperty("商品名称")
	private String prodName;

	/** 简要描述,卖点等 */
	@ApiModelProperty("简要描述,卖点等")
	private String brief;

	/** 商品价格 */
	@ApiModelProperty("商品原价")
	private Double price;
	
	/** 商品现价 */
	@ApiModelProperty("商品现价, 销售价")
	private Double cash;

	/** 商品主图 */
	@ApiModelProperty("商品主图")
	private String pic;
	
	/** 商品图片列表 */
	@ApiModelProperty("商品图片列表")
	private List<String> prodPics;
	
	/** 商品详情内容 */
	@ApiModelProperty("商品详情内容")
	private String content;

	/** 商品动态参数 */
	@ApiModelProperty("商品动态参数")
	private List<ParamGroupDto> parameters;
	
	/** 商品sku集合 */
	@ApiModelProperty("商品sku集合")
	private List<AppSkuDto> skuList;
	
	/** 商品属性集合 **/
	@ApiModelProperty(value="商品属性集合")  
	private List<AppProductPropertyDto> prodPropList;
	
    /**商品属性图片集合**/
	@ApiModelProperty(value="商品属性图片集合")  
    List<AppPropValueImgDto> propValueImgList;
	
    /** 商家id */
	@ApiModelProperty(value="商家id")  
	private Long shopId;
	
	/** 商家LOGO */
	@ApiModelProperty(value="商家logo")  
	private String shopLogo;
	
    /** 商家名称 */
	@ApiModelProperty(value="商家名称")  
	private String shopName;
	
	/**店铺类型 0专营,1旗舰,2自营**/
	@ApiModelProperty(value = "店铺类型 0专营,1旗舰,2自营")
	protected Integer shopType;
	
	/** 店铺综合评分 */
	@ApiModelProperty(value="商家综合评分")  
	private String shopScore;
	
	/** 包邮信息 */
	@ApiModelProperty(value="包邮信息")  
	private String mailfee;
	
	// TODO 拼团活动没有优惠券
/*	@ApiModelProperty("可领取优惠券列表")
	private List<AppCouponDto2> couponList;
	
	@ApiModelProperty("已领取优惠券列表")
	private List<AppCouponDto2> userCouponList;*/
	
	/** 是否已收藏 */
	@ApiModelProperty("是否已收藏")
	private Boolean isCollect;


	/** 商品视频 **/
	@ApiModelProperty("商品视频")
	private String proVideoUrl;

	public String getAddNumber() {
		return addNumber;
	}

	public void setAddNumber(String addNumber) {
		this.addNumber = addNumber;
	}

	public List<AppMergeGroupingDto> getGroupingList() {
		return groupingList;
	}

	public void setGroupingList(List<AppMergeGroupingDto> groupingList) {
		this.groupingList = groupingList;
	}

	public Long getLimitedEndtime() {
		return limitedEndtime;
	}

	public void setLimitedEndtime(Long limitedEndtime) {
		this.limitedEndtime = limitedEndtime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPeopleNumber() {
		return peopleNumber;
	}

	public void setPeopleNumber(Integer peopleNumber) {
		this.peopleNumber = peopleNumber;
	}

	public Boolean getIsHeadFree() {
		return isHeadFree;
	}

	public void setIsHeadFree(Boolean isHeadFree) {
		this.isHeadFree = isHeadFree;
	}

	public Double getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}

	public Double getMinProdPrice() {
		return minProdPrice;
	}

	public void setMinProdPrice(Double minProdPrice) {
		this.minProdPrice = minProdPrice;
	}

	public Boolean getIsLimit() {
		return isLimit;
	}

	public void setIsLimit(Boolean isLimit) {
		this.isLimit = isLimit;
	}

	public Long getLimitNumber() {
		return limitNumber;
	}

	public void setLimitNumber(Long limitNumber) {
		this.limitNumber = limitNumber;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<ParamGroupDto> getParameters() {
		return parameters;
	}

	public void setParameters(List<ParamGroupDto> parameters) {
		this.parameters = parameters;
	}

	public List<AppSkuDto> getSkuList() {
		return skuList;
	}

	public void setSkuList(List<AppSkuDto> skuList) {
		this.skuList = skuList;
	}
	
	public List<AppProductPropertyDto> getProdPropList() {
		return prodPropList;
	}

	public void setProdPropList(List<AppProductPropertyDto> prodPropList) {
		this.prodPropList = prodPropList;
	}
	
	public List<AppPropValueImgDto> getPropValueImgList() {
		return propValueImgList;
	}

	public void setPropValueImgList(List<AppPropValueImgDto> propValueImgList) {
		this.propValueImgList = propValueImgList;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopLogo() {
		return shopLogo;
	}

	public void setShopLogo(String shopLogo) {
		this.shopLogo = shopLogo;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public Integer getShopType() {
		return shopType;
	}

	public void setShopType(Integer shopType) {
		this.shopType = shopType;
	}

	public String getShopScore() {
		return shopScore;
	}

	public void setShopScore(String shopScore) {
		this.shopScore = shopScore;
	}

	public String getMailfee() {
		return mailfee;
	}

	public void setMailfee(String mailfee) {
		this.mailfee = mailfee;
	}

/*	public List<AppCouponDto2> getCouponList() {
		return couponList;
	}

	public void setCouponList(List<AppCouponDto2> couponList) {
		this.couponList = couponList;
	}
	
	public List<AppCouponDto2> getUserCouponList() {
		return userCouponList;
	}

	public void setUserCouponList(List<AppCouponDto2> userCouponList) {
		this.userCouponList = userCouponList;
	}*/

	public List<String> getProdPics() {
		return prodPics;
	}

	public void setProdPics(List<String> prodPics) {
		this.prodPics = prodPics;
	}

	public Boolean getIsCollect() {
		return isCollect;
	}

	public void setIsCollect(Boolean isCollect) {
		this.isCollect = isCollect;
	}

	public String getProVideoUrl() {
		return proVideoUrl;
	}

	public void setProVideoUrl(String proVideoUrl) {
		this.proVideoUrl = proVideoUrl;
	}

	public Long getLimitedStarttime() {
		return limitedStarttime;
	}

	public void setLimitedStarttime(Long limitedStarttime) {
		this.limitedStarttime = limitedStarttime;
	}
}
