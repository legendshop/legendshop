package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app 专题板块商品Dto
 */

@ApiModel(value="专题板块商品Dto")
public class AppThemeModuleProdDto {

	/** 专题板块商品id */
	@ApiModelProperty(value="专题板块商品id")
	private Long modulePrdId; 
		
	/** 板块id */
	@ApiModelProperty(value="板块id")
	private Long themeModuleId; 
		
	/** 商品id */
	@ApiModelProperty(value="商品id")
	private Long prodId; 
		
	/** 商品名称 */
	@ApiModelProperty(value="商品名称")
	private String prodName; 
		
	/** 商品图片 */
	@ApiModelProperty(value="商品图片")
	private String img; 
		
	/** 角标图片 */
	@ApiModelProperty(value="角标图片 ")
	private String angleIcon; 
		
	/** 是否售罄 */
	@ApiModelProperty(value="是否售罄")
	private Long isSoldOut; 
	
	/** 商品现价 */
	@ApiModelProperty(value="商品现价")
	private Double cash;

	public Long getModulePrdId() {
		return modulePrdId;
	}

	public void setModulePrdId(Long modulePrdId) {
		this.modulePrdId = modulePrdId;
	}

	public Long getThemeModuleId() {
		return themeModuleId;
	}

	public void setThemeModuleId(Long themeModuleId) {
		this.themeModuleId = themeModuleId;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getAngleIcon() {
		return angleIcon;
	}

	public void setAngleIcon(String angleIcon) {
		this.angleIcon = angleIcon;
	}

	public Long getIsSoldOut() {
		return isSoldOut;
	}

	public void setIsSoldOut(Long isSoldOut) {
		this.isSoldOut = isSoldOut;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}
	
	
}
