package com.legendshop.app.dto;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 发票Dto
 */
@ApiModel(value="发票Dto") 
public class AppInvoiceDto {


	/** ID */
	@ApiModelProperty(value="发票id") 
	private Long id;
	
	/** 发票类型 */
	@ApiModelProperty(value="发票类型，1为普通发票") 
	private Integer typeId;
	
	/** 发票标题，1为个人，2为单位 */
	@ApiModelProperty(value="发票标题，1为个人，2为单位") 
	private Integer titleId;
	
	/** 发票抬头 */
	@ApiModelProperty(value="发票抬头") 
	private String company;
	
	/** 发票内容 */
	@ApiModelProperty(value="发票内容,1为不开发票，2为明细") 
	private Integer contentId;
	
	/** 用户id */
	@ApiModelProperty(value="用户id") 
	private String userId;
	
	/** 用户名 */
	@ApiModelProperty(value="用户名") 
	private String userName;
	
	/** 创建时间 */
	@ApiModelProperty(value="创建时间") 
	private Date createTime;
	
	/** 默认使用 */
	@ApiModelProperty(value="默认使用,1为默认") 
	private Integer commonInvoice;
	
	/** 纳税人号 */
	@ApiModelProperty(value="纳税人号") 
	private String invoiceHumNumber;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public Integer getTitleId() {
		return titleId;
	}

	public void setTitleId(Integer titleId) {
		this.titleId = titleId;
	}

	public Integer getContentId() {
		return contentId;
	}

	public void setContentId(Integer contentId) {
		this.contentId = contentId;
	}

	public Integer getCommonInvoice() {
		return commonInvoice;
	}

	public void setCommonInvoice(Integer commonInvoice) {
		this.commonInvoice = commonInvoice;
	}

	public String getInvoiceHumNumber() {
		return invoiceHumNumber;
	}

	public void setInvoiceHumNumber(String invoiceHumNumber) {
		this.invoiceHumNumber = invoiceHumNumber;
	}
	
	
}
