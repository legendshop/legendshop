package com.legendshop.app.dto ;
import java.util.Date;

import com.legendshop.dao.persistence.Transient;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 *优惠券与红包
 */
@ApiModel(value="优惠券与红包") 
public class AppCouponDto2 {
	
	/** 优惠券 ID */
	@ApiModelProperty(value="优惠券ID")  
	private Long couponId; 
		
	/** 优惠券 名称 */
	@ApiModelProperty(value="优惠券名称")  
	private String couponName; 
	
	/** 描述 */
	@ApiModelProperty(value="优惠券描述")  
	private String description; 
	
	/** 劵值满多少金额 */
	@ApiModelProperty(value="劵值满多少金额")  
	private Double fullPrice; 
		
	/** 劵值减多少金额 */
	@ApiModelProperty(value="劵值减多少金额")  
	private Double offPrice; 
	
	/** 活动开始时间 */
	@ApiModelProperty(value="活动开始时间")  
	private Date startDate; 
		
	/** 活动结束时间 */
	@ApiModelProperty(value="活动结束时间")  
	private Date endDate; 
	
	/** 优惠券提供方：平台: platform，店铺:shop */
	@ApiModelProperty(value="优惠券提供方：平台: platform，店铺:shop, ")  
	private String couponProvider;
	
	/** 优惠券的类型 */
	@ApiModelProperty(value="优惠券的类型, 通用券:common，品类券:category，指定商品券:product，指定店铺：shop")  
	private String couponType; 
		
	/** 优惠券状态 */
	@ApiModelProperty(value="优惠券状态")  
	private Integer status; 
		
	/** 优惠券图片 **/
	@ApiModelProperty(value="优惠券图片", hidden = true)  
	private String couponPic;
		
	/** 领取上限 */
	@ApiModelProperty(value="领取上限", hidden = true)  
	private Long getLimit; 
		
	/** 创建时间 */
	@ApiModelProperty(value="创建时间", hidden = true)  
	private Date createDate; 
		
	public AppCouponDto2() {
    }
		
	public Long  getCouponId(){
		return couponId;
	} 
		
	public void setCouponId(Long couponId){
			this.couponId = couponId;
		}
		
	public String  getCouponName(){
		return couponName;
	} 
		
	public void setCouponName(String couponName){
			this.couponName = couponName;
		}
		
	public Integer  getStatus(){
		return status;
	} 
		
	public void setStatus(Integer status){
			this.status = status;
		}
		
	public Double  getFullPrice(){
		return fullPrice;
	} 
		
	public void setFullPrice(Double fullPrice){
			this.fullPrice = fullPrice;
		}
		
	public Double  getOffPrice(){
		return offPrice;
	} 
		
	public void setOffPrice(Double offPrice){
			this.offPrice = offPrice;
		}
		
	public Date  getStartDate(){
		return startDate;
	} 
		
	public void setStartDate(Date startDate){
			this.startDate = startDate;
		}
		
	public Date  getEndDate(){
		return endDate;
	} 
		
	public void setEndDate(Date endDate){
			this.endDate = endDate;
		}
		
	public String  getCouponType(){
		return couponType;
	} 
		
	public void setCouponType(String couponType){
			this.couponType = couponType;
		}
		
	public Long  getGetLimit(){
		return getLimit;
	} 
		
	public void setGetLimit(Long getLimit){
			this.getLimit = getLimit;
		}
		
	public Date  getCreateDate(){
		return createDate;
	} 
		
	public void setCreateDate(Date createDate){
			this.createDate = createDate;
		}
		
	public String  getDescription(){
		return description;
	} 
		
	public void setDescription(String description){
			this.description = description;
		}
	
	public String getCouponProvider() {
		return couponProvider;
	}

	public void setCouponProvider(String couponProvider) {
		this.couponProvider = couponProvider;
	}

	public String getCouponPic() {
		return couponPic;
	}

	public void setCouponPic(String couponPic) {
		this.couponPic = couponPic;
	}
	
	
} 
