package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.AppIndexService;
import com.legendshop.business.dao.IndexJpgDao;
import com.legendshop.model.dto.app.IndexjpgDto;
import com.legendshop.model.entity.Indexjpg;
import com.legendshop.util.AppUtils;

/**
 * 首页服务
 *
 */
@Service("appIndexService")
public class AppIndexServiceImpl implements AppIndexService {
	
	@Autowired
	private IndexJpgDao indexJpgDao;
	
	/**
	 * 获取首页的轮播图
	 */
	@Override
	public List<IndexjpgDto> getIndexJpegByShopId(Long shopId) {
		List<Indexjpg> result = indexJpgDao.queryMobileIndexJpeg(shopId);
		if(AppUtils.isBlank(result)){
			return null;
		}
		List<IndexjpgDto> resultDtoList = new ArrayList<IndexjpgDto>(); 
		for (Indexjpg indexjpg : result) {
			resultDtoList.add(convertIndexjpgDto(indexjpg));
		}
		return resultDtoList;
	}

	public IndexjpgDto convertIndexjpgDto(Indexjpg indexjpg){
		IndexjpgDto indexjpgDto = new IndexjpgDto();
		indexjpgDto.setTitle(indexjpg.getTitle());
		indexjpgDto.setLink(indexjpg.getLink());
		indexjpgDto.setImg(indexjpg.getImg());
		return indexjpgDto;
	}
	
}
