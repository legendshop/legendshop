package com.legendshop.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.AppVisitLogService;
import com.legendshop.spi.service.VisitLogService;

/**
 * app 我的足迹service实现
 */
@Service("appVisitLogService")
public class AppVisitLogServiceImpl implements AppVisitLogService {

	@Autowired
	private VisitLogService visitLogService;
	
	public void setVisitLogService(VisitLogService visitLogService) {
		this.visitLogService = visitLogService;
	}



	@Override
	public Long getVisitLogCountByUserName(String userName) {
		
		return visitLogService.getVisitLogCountByUserName(userName);
	}

}
