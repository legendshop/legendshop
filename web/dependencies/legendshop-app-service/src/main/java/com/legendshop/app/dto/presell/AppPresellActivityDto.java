package com.legendshop.app.dto.presell;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app预售活动数据Dto
 * @author 森
 *
 */
@ApiModel(value="app预售活动数据Dto")
public class AppPresellActivityDto {
	
	/** 主键 */
	@ApiModelProperty(value="主键")
	private Long id; 
		
	/** 产品ID */
	@ApiModelProperty(value="产品ID")
	private Long prodId; 
		
	/** 产品SKU ID */
	@ApiModelProperty(value="产品SKU ID")
	private Long skuId; 
		
	/** 商家ID */
	@ApiModelProperty(value="商家ID")
	private Long shopId; 
	
	/** 商家名称 */
	@ApiModelProperty(value="商家名称")
	private String shopName;
	
	/** 方案名称 */
	@ApiModelProperty(value="方案名称")
	private String schemeName;
		
	/** 支付方式,0:全额,1:定金 */
	@ApiModelProperty(value="支付方式,0:全额,1:定金")
	private Long payPctType; 
		
	/** 预售价格 */
	@ApiModelProperty(value="预售价格")
	private java.math.BigDecimal prePrice; 
	
	/** 定金金额 */
	@ApiModelProperty(value="定金金额")
	private java.math.BigDecimal preDepositPrice; 
		
	/** 预售开始时间 */
	@ApiModelProperty(value="预售开始时间")
	private Date preSaleStart; 
		
	/** 预售结束时间 */
	@ApiModelProperty(value="预售结束时间")
	private Date preSaleEnd; 
		
	/** 预售发货时间 */
	@ApiModelProperty(value="预售发货时间")
	private Date preDeliveryTime; 
	
	/** 预售发货时间 */
	@ApiModelProperty(value="预售发货时间")
	private String preDeliveryTimeStr;
		
	/** 预售支付百分比 */
	@ApiModelProperty(value="预售支付百分比")
	private Double payPct; 
		
	/** 尾款支付开始时间 */
	@ApiModelProperty(value="尾款支付开始时间")
	private Date finalMStart; 
		
	/** 尾款支付结束时间 */
	@ApiModelProperty(value="尾款支付结束时间")
	private Date finalMEnd; 
		
	/** 状态,-3:未提审,-2:已提审,-1:审核通过(上线),0:审核不通过,1:已完成,2:已终止 */
	@ApiModelProperty(value="状态,-3:未提审,-2:已提审,-1:审核通过(上线),0:审核不通过,1:已完成,2:已终止")
	private Long status; 
	
	/** 创建时间 */
	@ApiModelProperty(value="创建时间")
	private Date createTime;
		
	/** 审核意见 */
	@ApiModelProperty(value="审核意见")
	private String auditOpinion; 
		
	/** 审核时间 */
	@ApiModelProperty(value="审核时间")
	private Date auditTime; 
	
	/**商品名称**/
	@ApiModelProperty(value="商品名称")
	private String prodName;
	
	/**sku图片名称**/
	@ApiModelProperty(value="sku图片")
	private String skuPic;
	
	/**商品图片**/
	@ApiModelProperty(value="商品图片")
	private String prodPic;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getSchemeName() {
		return schemeName;
	}

	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}

	public Long getPayPctType() {
		return payPctType;
	}

	public void setPayPctType(Long payPctType) {
		this.payPctType = payPctType;
	}

	public java.math.BigDecimal getPrePrice() {
		return prePrice;
	}

	public void setPrePrice(java.math.BigDecimal prePrice) {
		this.prePrice = prePrice;
	}

	public java.math.BigDecimal getPreDepositPrice() {
		return preDepositPrice;
	}

	public void setPreDepositPrice(java.math.BigDecimal preDepositPrice) {
		this.preDepositPrice = preDepositPrice;
	}

	public Date getPreSaleStart() {
		return preSaleStart;
	}

	public void setPreSaleStart(Date preSaleStart) {
		this.preSaleStart = preSaleStart;
	}

	public Date getPreSaleEnd() {
		return preSaleEnd;
	}

	public void setPreSaleEnd(Date preSaleEnd) {
		this.preSaleEnd = preSaleEnd;
	}

	public Date getPreDeliveryTime() {
		return preDeliveryTime;
	}

	public void setPreDeliveryTime(Date preDeliveryTime) {
		this.preDeliveryTime = preDeliveryTime;
	}

	public String getPreDeliveryTimeStr() {
		return preDeliveryTimeStr;
	}

	public void setPreDeliveryTimeStr(String preDeliveryTimeStr) {
		this.preDeliveryTimeStr = preDeliveryTimeStr;
	}

	public Double getPayPct() {
		return payPct;
	}

	public void setPayPct(Double payPct) {
		this.payPct = payPct;
	}

	public Date getFinalMStart() {
		return finalMStart;
	}

	public void setFinalMStart(Date finalMStart) {
		this.finalMStart = finalMStart;
	}

	public Date getFinalMEnd() {
		return finalMEnd;
	}

	public void setFinalMEnd(Date finalMEnd) {
		this.finalMEnd = finalMEnd;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAuditOpinion() {
		return auditOpinion;
	}

	public void setAuditOpinion(String auditOpinion) {
		this.auditOpinion = auditOpinion;
	}

	public Date getAuditTime() {
		return auditTime;
	}

	public void setAuditTime(Date auditTime) {
		this.auditTime = auditTime;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getSkuPic() {
		return skuPic;
	}

	public void setSkuPic(String skuPic) {
		this.skuPic = skuPic;
	}

	public String getProdPic() {
		return prodPic;
	}

	public void setProdPic(String prodPic) {
		this.prodPic = prodPic;
	}
	
}
