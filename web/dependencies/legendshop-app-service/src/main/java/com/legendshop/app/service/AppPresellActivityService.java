/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service;


import java.util.List;

import com.legendshop.app.dto.AppActiveBanner;
import com.legendshop.app.dto.presell.AppPresellActivityDto;
import com.legendshop.app.dto.presell.AppPresellProdDetailDto;
import com.legendshop.model.dto.app.AppPageSupport;

/**
 * app预售活动Service.
 */
public interface AppPresellActivityService  {

	/** 根据状态获取预售活动列表 */
	public AppPageSupport<AppPresellActivityDto> queryPresellActivityList(String curPageNO, String status);
	
	/** 获取预售商品详情 */
	public AppPresellProdDetailDto getPresellProdDetailDto(Long presellId);
	
	/** 获取预售banner图 */
	public List<AppActiveBanner> getActiveBanners();
	
}
