package com.legendshop.app.service;

import com.legendshop.app.dto.AppSubRefundReturnDto;
import com.legendshop.model.dto.app.AppPageSupport;

/**
 * app 退款，退货退款service
 */
public interface AppSubRefundReturnService {

	/**
	 * 根据id 获取退款退货记录
	 * @param refundId  退款退货记录id
	 * @return
	 */
	public AppSubRefundReturnDto getSubRefundReturn(Long refundId);

	
	/**
	 * 获取用户退款退货记录列表
	 * @param userId 用户id
	 * @param curPageNO 当前页码
	 * @param status 处理状态
	 * @return
	 */
	public AppPageSupport<AppSubRefundReturnDto> querySubRefundReturnList(String userId, String curPageNO,Integer status);

}
