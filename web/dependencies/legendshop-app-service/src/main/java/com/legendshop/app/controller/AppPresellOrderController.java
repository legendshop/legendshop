/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.app.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.legendshop.model.constant.*;
import com.legendshop.model.dto.app.cartorder.ShopCartOrderDto;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.spi.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.AppOrderPayParamDto;
import com.legendshop.app.dto.AppUserShopCartOrderDto;
import com.legendshop.app.service.AppCartOrderService;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.framework.cache.client.CacheClient;
import com.legendshop.model.dto.OrderAddParamDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.order.AddOrderMessage;
import com.legendshop.model.dto.presell.PreSellExposerDto;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.PresellProd;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.processor.DelayCancelOfferProcessor;
import com.legendshop.processor.ProdSnapshotProcessor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import com.legendshop.util.MD5Util;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * App端预售订单.
 */
@RestController
@Api(tags="预售订单接口",value="预售订单接口")
public class AppPresellOrderController{
	
	private final static Logger LOGGER = LoggerFactory.getLogger(AppPresellOrderController.class);
	
	/** 防止表单重复提交的Session token 失效时间 */
	private static final int SESSION_TOKEN_EXPIRE = 60 * 10;
	
	@Autowired
	private LoginedUserService loginedUserService;
	
	@Autowired
	private OrderCartResolverManager orderCartResolverManager;
	
	@Autowired
	private AppCartOrderService appCartOrderService;
	
	@Autowired
	private SubService subService;
	
	@Autowired
	private PresellProdService presellProdService;
	
	@Autowired
	private UserAddressService userAddressService;
	
	@Autowired
	private StockService stockService;
	
	@Autowired
	private CacheClient cacheClient;

	@Autowired
	private UserDetailService userDetailService;
	
	// 订单常用处理工具类
	@Autowired
	private OrderUtil orderUtil;
	
	/** 订单延迟取消队列 **/
	@Autowired
	private DelayCancelOfferProcessor delayCancelOfferProcessor;
	
	/** 订单快照 **/
	@Autowired
	private ProdSnapshotProcessor prodSnapshotProcessor;
	
	@ApiOperation(value = "预售检查下单", httpMethod = "POST", notes = "预售检查",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", dataType = "Long", name = "presellId", value = "预售活动Id"),
		@ApiImplicitParam(paramType = "query", dataType = "Integer", name = "number", value = "购买数量"),
		@ApiImplicitParam(paramType = "query", dataType = "Long", name = "skuId", value = "skuId")
	})
	@PostMapping("/p/app/presell/checkPresell")
	private ResultDto<PreSellExposerDto> checkPresell(@RequestParam Long presellId, @RequestParam Integer number,@RequestParam Long skuId){
		PreSellExposerDto exposerDto = new PreSellExposerDto();
		if (number <= 0) {
			return ResultDtoManager.fail("请输入正确的购买数量");
		}
		
		LoginedUserInfo userInfo = loginedUserService.getUser();

		PresellProd presellProd = presellProdService.getPresellProd(presellId);
		if (presellProd == null) {
			return ResultDtoManager.fail("找不到预售信息");
		}
		if (userInfo != null ) {
			UserDetail userDetail = userDetailService.getUserDetailById(userInfo.getUserId());
			if (presellProd.getShopId().equals(userDetail.getShopId())){
				return ResultDtoManager.fail("自己的商品,不能购买!");
			}
		}
		
		if (PresellProdStatusEnum.ONLINE.value().intValue() != presellProd.getStatus()) {
			return ResultDtoManager.fail("预售未上线");
		}
		
		Date startTime = presellProd.getPreSaleStart();
		Date endTime = presellProd.getPreSaleEnd();
		Date now = new Date();
		if (now.getTime() >= endTime.getTime()) { // 活动已经过期
			return ResultDtoManager.fail("预售活动已经过期");
		} else if (now.getTime() < startTime.getTime()) { // 距离活动开始时间
			return ResultDtoManager.fail("预售活动未开始");
		}
		
		/*
		 * 1.付款减库存 简单的检查商品库存够不够
		 */
		Integer stocks = stockService.getStocksByMode(presellProd.getProdId(), skuId);
		if (stocks - number < 0) {
			return ResultDtoManager.fail("商品库存不足");
		}
		
		StringBuffer buffer = new StringBuffer().append(userInfo.getUserId()).append("_").append(presellId);
		String _md5 = MD5Util.toMD5(buffer.toString() + Constants._MD5);
		exposerDto.setToken(_md5);
		exposerDto.setExpose(true);
		exposerDto.setMsg("检测通过");
		exposerDto.setId(presellId);
		return ResultDtoManager.success(exposerDto);
	}
	
	
	@ApiOperation(value = "提交预售订单详情", httpMethod = "POST", notes = "提交预售订单详情" )
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", dataType = "Long", name = "presellId", value = "预售活动id"),
		@ApiImplicitParam(paramType = "query", dataType = "String", name = "payType", value = "预售付款方式: 0 全额支付, 1 定金支付的定金"),
		@ApiImplicitParam(paramType = "query", dataType = "Integer", name = "number", value = "购买数量"),
		@ApiImplicitParam(paramType = "query", dataType = "Long", name = "skuId", value = "skuId")
	})
	@PostMapping(value = "/p/app/presell/orderDetails")
	public ResultDto<AppUserShopCartOrderDto> mergeGroupOrderDetails(@RequestParam Long presellId, @RequestParam String payType, @RequestParam Integer number,@RequestParam Long skuId) {
		try {
			LoginedUserInfo appToken = loginedUserService.getUser();
			String userId = appToken.getUserId();
			
			if (AppUtils.isBlank(number) || number <= 0) {
				return ResultDtoManager.fail("请正确输入购买数量 ");
			}
			
			/* 根据类型查询订单列表 */
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("userId", userId);
			params.put("userName", appToken.getUserName());
			params.put("presellId", presellId);
			params.put("number", number);
			params.put("skuId", skuId);

			CartTypeEnum typeEnum = CartTypeEnum.PRESELL;
			UserShopCartList shopCartList = orderCartResolverManager.queryOrders(typeEnum, params);
			if (AppUtils.isBlank(shopCartList)) {
				return ResultDtoManager.fail("系统错误，下单失败，请重新下单");
			}
			
			shopCartList.setActiveId(presellId);
			AppUserShopCartOrderDto shopCartOrderDto = appCartOrderService.getPresellShoppingCartOrderDto(shopCartList, userId, payType,null);
			if(AppUtils.isBlank(shopCartOrderDto)){
				return ResultDtoManager.fail("找不到您要购买的商品! ");
			}
			
			// 设置订单提交令牌,防止重复提交
			Integer token = CommonServiceUtil.generateRandom();
			shopCartOrderDto.setToken(token+"");
			cacheClient.put(Constants.APP_FROM_SESSION_TOKEN + ":" + typeEnum.toCode() + ":" + userId, SESSION_TOKEN_EXPIRE, token);
			
			subService.putUserShopCartList(CartTypeEnum.PRESELL.toCode(), userId, shopCartList);

			for (ShopCartOrderDto shopCart : shopCartOrderDto.getShopCarts()) {
				shopCart.setPresellSkuId(skuId);
			}
			return ResultDtoManager.success(shopCartOrderDto);
			
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDtoManager.fail("系统错误, 下单失败, 请联系平台客服! ");
		}
	}
	
	
	@ApiOperation(value = "提交预售订单", httpMethod = "POST", notes = "提交预售订单" )
	@PostMapping(value="/p/app/presell/submitOrder")
	public ResultDto<AppOrderPayParamDto> submitOrder(HttpServletRequest request, OrderAddParamDto paramDto) {
		
		LoginedUserInfo appToken = loginedUserService.getUser();
		String userId = appToken.getUserId();
		String userName = appToken.getUserName();
		
		CartTypeEnum typeEnum = CartTypeEnum.PRESELL;
		
		Integer sessionToken = cacheClient.get(Constants.APP_FROM_SESSION_TOKEN + ":" + typeEnum.toCode() + ":" + userId);
		AddOrderMessage message = orderUtil.checkSubmitParam(userId, sessionToken, typeEnum, paramDto);//检查输入参数
		if(message.hasError()){//有错误就返回
			return ResultDtoManager.fail(message.getMessage());
		}

		// TODO 校验用户参数，SpringSession后台过期机制有问题
		if (!userDetailService.isUserExist(userName)) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("用户不存在！");
			return ResultDtoManager.fail(0, message.getMessage());
		}
		
		//get cache 从上个页面获取设置进入缓存的订单
		UserShopCartList userShopCartList = subService.findUserShopCartList(typeEnum.toCode(), userId);
		if(AppUtils.isBlank(userShopCartList)){
			return ResultDtoManager.fail("没有购物清单");
		}
		
		//检查金额： 如果促销导致订单金额为负数
	    if(userShopCartList.getOrderActualTotal()< 0){
	    	return ResultDtoManager.fail("订单金额为负数,下单失败!");
	    }
	    
	    //处理订单的卖家留言备注
	    try {
			orderUtil.handlerRemarkText(paramDto.getRemarkText(), userShopCartList);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return ResultDtoManager.fail("下单失败，请联系商城管理员或商城客服");
		}
		
	    /** 根据id获取收货地址信息 **/
		UserAddress userAddress = userShopCartList.getUserAddress();
		if (AppUtils.isBlank(userAddress)) {
			userAddress = userAddressService.getDefaultAddress(userId);
			if (AppUtils.isBlank(userAddress)) {
				return ResultDtoManager.fail("请选择或添加您的收货地址！");
			}
			userShopCartList.setUserAddress(userAddress);
		}
		
		// 处理运费模板
		String result = orderUtil.handlerDelivery(paramDto.getDelivery(), userShopCartList);
		if (!Constants.SUCCESS.equals(result)) {
			return ResultDtoManager.fail("运费处理失败, 请联系商城管理员或商城客服！");
		}
		
		// 处理优惠券
		orderUtil.handleCouponStr(userShopCartList);
		
		// 验证成功，清除令牌
		request.getSession().removeAttribute(Constants.TOKEN);
		userShopCartList.setUserId(userId);
		userShopCartList.setUserName(userName);
		userShopCartList.setInvoiceId(paramDto.getInvoiceId());
		userShopCartList.setUserAddress(userAddress);
		userShopCartList.setPayManner(paramDto.getPayManner());
		userShopCartList.setType(typeEnum.toCode());
		userShopCartList.setSwitchInvoice(paramDto.getSwitchInvoice());//当前订单所属商家是否开启发票
		
		/**
		 * 处理订单
		 */
		List<String> subNumberList = null;
		try {
			subNumberList = orderCartResolverManager.addOrder(typeEnum,userShopCartList);
			
			if (AppUtils.isBlank(subNumberList)) {
				return ResultDtoManager.fail("下单失败，请联系商城管理员或商城客服");
			}
			
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return ResultDtoManager.fail("下单失败，请联系商城管理员或商城客服");
		}
		
		//offer 延时取消队列
  		delayCancelOfferProcessor.process(subNumberList);
        
		//发送网站快照备份
		prodSnapshotProcessor.process(subNumberList);

		// 标记回收
		userShopCartList = null;
		// 清空缓存
		subService.evictUserShopCartCache(typeEnum.toCode(), userId);
		// 验证成功，清除令牌
		cacheClient.delete(Constants.APP_FROM_SESSION_TOKEN + ":" + typeEnum.toCode() + ":" + userId);
		
		final String [] subNumbers=new String[subNumberList.size()];
		for (int i = 0; i < subNumberList.size(); i++) {
			subNumbers[i] = subNumberList.get(i);
		}
		
		//根据订单IDs获取订单集合
		List<Sub> subs = subService.getSubBySubNums(subNumbers, userId, OrderStatusEnum.UNPAY.value());
		Double totalAmount = 0.0;//总金额
		String subject = ""; //商品描述
		StringBuffer sub_number = new StringBuffer(""); //订单流水号
		for (Sub sub : subs) {
			totalAmount= Arith.add(totalAmount,sub.getActualTotal());
			sub_number.append(sub.getSubNumber()).append(",");
			subject += subject+sub.getProdName()+",";
		}
		String subNumber = sub_number.substring(0, sub_number.length() - 1);
		
		AppOrderPayParamDto orderPay = new AppOrderPayParamDto();
		orderPay.setTotalAmount(totalAmount);
		orderPay.setSubNumber(subNumber);
		orderPay.setSubjects(subject);
		orderPay.setSubSettlementType(SubSettlementTypeEnum.USER_ORDER_PAY.value());
		
		return ResultDtoManager.success(orderPay);
	}
	
}
