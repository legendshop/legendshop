package com.legendshop.app.dto.mergeGroup;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 正在进行拼团的小组列表
 */
@ApiModel(value="正在进行拼团的小组")
public class AppMergeGroupingDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty("参团编号")
	private String addNumber;

	@ApiModelProperty("现有人数, 已参团人数.")
	private Integer propleCount;
	
	@ApiModelProperty("团长头像")
	private String portraitPic;
	
	@ApiModelProperty("开团时间")
	private Date createTime;
	
	@ApiModelProperty("成团截止时间, 用这个作为倒计时")
	private Date endTime;

	public String getAddNumber() {
		return addNumber;
	}

	public void setAddNumber(String addNumber) {
		this.addNumber = addNumber;
	}

	public Integer getPropleCount() {
		return propleCount;
	}

	public void setPropleCount(Integer propleCount) {
		this.propleCount = propleCount;
	}

	public String getPortraitPic() {
		return portraitPic;
	}

	public void setPortraitPic(String portraitPic) {
		this.portraitPic = portraitPic;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
}
