package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 查看物流返回信息封装
 */
@ApiModel(value="查看物流信息Dto")
public class AppDeliveryDto {

	/** 承运来源 */
	@ApiModelProperty(value="承运来源")
	private String dvyName;
	
	/** 运单号 */
	@ApiModelProperty(value="运单号")
	private String dvyNumber;
	
	/** 物流公司图片 */
	@ApiModelProperty(value="物流公司图片(暂时没用)")
	private String dvyPic;
	
	/** 物流追踪信息 */
	@ApiModelProperty(value="物流追踪信息")
	private String dvyDetail;

	public String getDvyName() {
		return dvyName;
	}

	public void setDvyName(String dvyName) {
		this.dvyName = dvyName;
	}

	public String getDvyNumber() {
		return dvyNumber;
	}

	public void setDvyNumber(String dvyNumber) {
		this.dvyNumber = dvyNumber;
	}

	public String getDvyPic() {
		return dvyPic;
	}

	public void setDvyPic(String dvyPic) {
		this.dvyPic = dvyPic;
	}

	public String getDvyDetail() {
		return dvyDetail;
	}

	public void setDvyDetail(String dvyDetail) {
		this.dvyDetail = dvyDetail;
	}
}
