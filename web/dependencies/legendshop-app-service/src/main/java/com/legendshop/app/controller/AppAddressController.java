/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.app.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.service.AppUserAddressService;
import com.legendshop.base.util.ValidationUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.app.AddressDto;
import com.legendshop.model.dto.app.AddressRequestDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.UserAddressService;
import com.legendshop.util.AppUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

/**
 * 用户地址管理相关接口
 */
@RestController
@Api(tags="收货地址",value="收货地址管理相关接口")
@RequestMapping("/p")
public class AppAddressController {
	
	@Autowired
	private AppUserAddressService appUserAddressService;
	
	@Autowired
	private UserAddressService userAddressService;
	
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;
	
	/**
	 * 获取收货地址列表.
	 */
	@ApiOperation(value = "获取收货地址列表", httpMethod = "POST", notes = "获取收货地址列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "curPageNO", value = "分页", required = false, dataType = "string")
	@PostMapping("/addressList")
	public ResultDto<AppPageSupport<AddressDto>> addressList(String curPageNO) {
		String userId = loginedUserService.getUser().getUserId();
		AppPageSupport<AddressDto> result = appUserAddressService.queryUserAddress(curPageNO,userId);
		return ResultDtoManager.success(result); 
	}
	 
	
	/**
	 * 保存收货地址.
	 *
	 * @param address the address
	 * @return the result dto< user address>
	 */
	@ApiOperation(value = "保存或更新收货地址", httpMethod = "POST", notes = "保存或更新收货地址,以地址id为标识",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/saveAddress")
    public ResultDto<UserAddress> saveAddress(AddressRequestDto address) {
		LoginedUserInfo token = loginedUserService.getUser();
		String userName = token.getUserName();
		String userId = token.getUserId();
		
		if(!checkUserAddress(address)){//检查非空字段
			return ResultDtoManager.fail(0, Constants.FAIL);
			
		}else if(AppUtils.isBlank(address.getAddrId())){
			
			if(checkMaxNumber(userName)){
				return ResultDtoManager.fail(-1, "保存地址最多为12个");
			}
		}
		UserAddress userAddress = convertUserAddress(address,userName,userId);
		userAddress.setStatus(Constants.YES);
		Long addrId = userAddressService.saveUserAddress(userAddress,userId);
		
		//检查当前保存的地址是否选择设置为默认
		if(userAddress.getCommonAddr(). equals("1")){
			userAddressService.updateOtherDefault(addrId,userId,"0");
		}
				
		return ResultDtoManager.success(userAddress);
	}
	

	/**
	 * 加载收货地址.
	 *
	 * @param request the request
	 * @param response the response
	 * @param addrId the addr id
	 * @return the result dto< address dto>
	 */
	@ApiOperation(value = "加载收货地址", httpMethod = "POST", notes = "加载收货地址",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "addrId", value = "地址id", required = false, dataType = "Long")
	@PostMapping("/loadAddress")
	public ResultDto<AddressDto> loadAddress(HttpServletRequest request, HttpServletResponse response,Long addrId){
		if(addrId == null){
			return ResultDtoManager.fail(0, "addrId is null");
		}
		AddressDto result = appUserAddressService.getUserAddressDto(addrId);
		return ResultDtoManager.success(result);
	}
	
	
	/**
	 * 删除地址.
	 *
	 * @param request the request
	 * @param response the response
	 * @param addrId the addr id
	 * @return the result dto< string>
	 */
	@ApiOperation(value = "删除收货地址", httpMethod = "POST", notes = "msg返回OK，则表示删除成功， 返回 fail 则表示删除失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "addrId", value = "地址id", required = true, dataType = "long")
	@PostMapping("/deladdress")
	public  ResultDto<String> deladdress(HttpServletRequest request, HttpServletResponse response,@RequestParam Long addrId) {
		
		String userName=loginedUserService.getUser().getUserName();
		UserAddress userAddress = userAddressService.getUserAddress(addrId);
		if(userAddress != null){
			if(AppUtils.isNotBlank(userName) && userName.equals(userAddress.getUserName())){
				int result = userAddressService.deleteUserAddress(userAddress);
				if(result > 0){
					return ResultDtoManager.success(Constants.SUCCESS);
				}
			}
		}
		return ResultDtoManager.fail(0, Constants.FAIL);
	}
	
	/**
	 * 设置默认收货地址.
	 *
	 * @param addrId the addr id
	 * @return the result dto< string>
	 */
	@ApiOperation(value = "设置默认收货地址", httpMethod = "POST", notes = "msg返回OK，则表示设置成功， 返回 fail 则表示设置失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "addrId", value = "地址id", required = false, dataType = "Long")
	@PostMapping("/defaultaddress")
	public  ResultDto<String> defaultaddress(@RequestParam Long addrId) {
		String userId = loginedUserService.getUser().getUserId();
		UserAddress userAddress = userAddressService.getUserAddress(addrId);
		if(userAddress != null && userId.equals(userAddress.getUserId())){
			userAddressService.updateDefaultUserAddress(addrId,userId);
			return ResultDtoManager.success(Constants.SUCCESS);
		}else{
			return ResultDtoManager.fail(0, Constants.FAIL);
		}
	}
	
	
	/**
	 * 检查用户地址.
	 *
	 * @param userAddress the user address
	 * @return true, if check user address
	 */
	private boolean checkUserAddress(AddressRequestDto userAddress) {
		if(AppUtils.isBlank(userAddress.getReceiver()) || 
				AppUtils.isBlank(userAddress.getSubAdds()) || 
				AppUtils.isBlank(userAddress.getProvinceId()) || 
				AppUtils.isBlank(userAddress.getCityId()) || 
				AppUtils.isBlank(userAddress.getAreaId()) || 
				!ValidationUtil.checkPhone(userAddress.getMobile())
				){
			return false;
		}
		return true;
	}
	
	/**
	 * Check max number.
	 *
	 * @param userName the user name
	 * @return true, if check max number
	 */
	private boolean checkMaxNumber(String userName) {
		Long MaxNumber = userAddressService.getMaxNumber(userName);
		if(MaxNumber >= 20){
			return true;
		}else{
			return false;
		}
	}
	
	
	/**
	 * Convert user address.
	 *
	 * @param address the address
	 * @param userName the user name
	 * @param userId the user id
	 * @return the user address
	 * @Description: 封装地址
	 * @date 2016-8-13
	 */
	private UserAddress convertUserAddress(AddressRequestDto address,String userName,String userId){
		UserAddress userAddress = new UserAddress();
		userAddress.setAddrId(address.getAddrId());
		userAddress.setUserName(userName);
		userAddress.setUserId(userId);
		userAddress.setCreateTime(new Date());
		userAddress.setVersion(1l);
		userAddress.setModifyTime(new Date());
		userAddress.setMobile(address.getMobile());
		userAddress.setProvinceId(address.getProvinceId());
		userAddress.setCityId(address.getCityId());
		userAddress.setAreaId(address.getAreaId());
		userAddress.setSubAdds(address.getSubAdds());
		userAddress.setReceiver(address.getReceiver());
		userAddress.setCommonAddr(AppUtils.isNotBlank(address.getCommonAddr())?address.getCommonAddr():"0");
		return userAddress;
	}
}
