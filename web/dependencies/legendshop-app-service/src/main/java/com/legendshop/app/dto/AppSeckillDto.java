/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;

import java.io.Serializable;
import java.util.Date;

import com.legendshop.model.dto.seckill.SeckillProdDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *秒杀详情Dto.
 *
 */
@ApiModel(value="秒杀详情Dto")
public class AppSeckillDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/** The sid. */
	@ApiModelProperty(value="秒杀活动Id")
	private Long id;

	/** 用户编号. */
	@ApiModelProperty(value="秒杀店铺用户Id")
	private String userId;

	/** 商家编号. */
	@ApiModelProperty(value="秒杀店铺Id")
	private Long shopId;

	/** 店铺名称. */
	@ApiModelProperty(value="店铺名称")
	private String shopName;
	
	/** 店铺图片. */
	@ApiModelProperty(value="店铺图片")
	private String shopPic;
	
	/**店铺类型 0专营,1旗舰,2自营**/
	@ApiModelProperty(value = "店铺类型 0专营,1旗舰,2自营")
	protected Integer shopType;

	/** 活动名称. */
	@ApiModelProperty(value="活动名称")
	private String seckillTitle;

	/** 开始时间. */
	@ApiModelProperty(value="开始时间")
	private Date startTime;

	/** 结束时间. */
	@ApiModelProperty(value="结束时间")
	private Date endTime;

	/** 秒杀活动描述 */
	@ApiModelProperty(value="秒杀活动描述")
	private String seckillDesc;
	
	/** 秒杀活动摘要 */
	@ApiModelProperty(value="秒杀活动摘要")
	private String seckillBrief;
	
	/** 秒杀价格 */
	@ApiModelProperty(value="秒杀价格")
	private Double  seckillPrice;

	/** 活动状态. */
	@ApiModelProperty(value="活动状态：未通过为-2, 审核中为-1, 上线为1, 下线为0, 秒杀结束为2")
	private Long status;

	/** 秒杀商品Dto. */
	@ApiModelProperty(value="秒杀商品Dto")
	private SeckillProdDto prodDto;
	
	/** 购买用户名. */
	@ApiModelProperty(value="购买用户名")
	private String loginUserName;
	
	/** 购买状态：1:进行中,-1:已结束,0:未开始,2:已售完 */
	@ApiModelProperty(value="购买状态：1:进行中, -1:已结束, 0:未开始")
	private Integer buyStatus;
	
	/** 商家综合评分 */
	@ApiModelProperty(value="商家综合评分")
	private String shopScore;
	
	/** 是否已收藏 */
	@ApiModelProperty(value="是否已收藏")
	private Boolean isCollect;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getSeckillTitle() {
		return seckillTitle;
	}

	public void setSeckillTitle(String seckillTitle) {
		this.seckillTitle = seckillTitle;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getSeckillDesc() {
		return seckillDesc;
	}

	public void setSeckillDesc(String seckillDesc) {
		this.seckillDesc = seckillDesc;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public SeckillProdDto getProdDto() {
		return prodDto;
	}

	public void setProdDto(SeckillProdDto prodDto) {
		this.prodDto = prodDto;
	}

	public String getLoginUserName() {
		return loginUserName;
	}

	public void setLoginUserName(String loginUserName) {
		this.loginUserName = loginUserName;
	}

	public Integer getBuyStatus() {
		return buyStatus;
	}

	public void setBuyStatus(Integer buyStatus) {
		this.buyStatus = buyStatus;
	}

	public String getShopScore() {
		return shopScore;
	}

	public void setShopScore(String shopScore) {
		this.shopScore = shopScore;
	}

	public Boolean getIsCollect() {
		return isCollect;
	}

	public void setIsCollect(Boolean isCollect) {
		this.isCollect = isCollect;
	}

	public String getSeckillBrief() {
		return seckillBrief;
	}

	public void setSeckillBrief(String seckillBrief) {
		this.seckillBrief = seckillBrief;
	}

	public String getShopPic() {
		return shopPic;
	}

	public void setShopPic(String shopPic) {
		this.shopPic = shopPic;
	}

	public Integer getShopType() {
		return shopType;
	}

	public void setShopType(Integer shopType) {
		this.shopType = shopType;
	}

	public Double getSeckillPrice() {
		return seckillPrice;
	}

	public void setSeckillPrice(Double seckillPrice) {
		this.seckillPrice = seckillPrice;
	}
	
	
}
