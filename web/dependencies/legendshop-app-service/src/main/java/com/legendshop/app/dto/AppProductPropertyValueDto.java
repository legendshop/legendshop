package com.legendshop.app.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("商品属性值Dto")
public class AppProductPropertyValueDto implements Serializable{

	private static final long serialVersionUID = 6598280148952180408L;

		/** 属性值ID **/
		@ApiModelProperty(value = "属性值ID")
		private Long valueId;

		/** 属性ID **/ 
		@ApiModelProperty(value = "属性ID")
		private Long propId;

		/** 属性值名称 **/
		@ApiModelProperty(value = "属性值名称")
		private String name;
		
		/** 是否被sku选中 **/
		@ApiModelProperty(value = "是否被sku选中")
		private boolean isSelected = false;

		public Long getValueId() {
			return valueId;
		}

		public void setValueId(Long valueId) {
			this.valueId = valueId;
		}

		public Long getPropId() {
			return propId;
		}

		public void setPropId(Long propId) {
			this.propId = propId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public boolean getIsSelected() {
			return isSelected;
		}

		public void setIsSelected(boolean isSelected) {
			this.isSelected = isSelected;
		}
		
}
