/**
 *
 */
package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.legendshop.app.dto.*;
import com.legendshop.model.entity.CouponShop;
import com.legendshop.spi.service.CouponShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.AppCouponService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.business.dao.CouponDao;
import com.legendshop.business.dao.UserCouponDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.dao.support.QueryMap;
import com.legendshop.dao.support.SimpleSqlQuery;
import com.legendshop.dao.sql.ConfigCode;
import com.legendshop.model.constant.AppCouponTypeEnum;
import com.legendshop.model.constant.CouponGetTypeEnum;
import com.legendshop.model.constant.CouponProviderEnum;
import com.legendshop.model.constant.CouponTypeEnum;
import com.legendshop.model.constant.UserCouponEnum;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.buy.PersonalCouponDto;
import com.legendshop.model.dto.coupon.UserCouponListDto;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.UserCoupon;
import com.legendshop.spi.service.CouponProdService;
import com.legendshop.spi.service.ProductService;
import com.legendshop.util.AppUtils;

/**
 * 优惠卷
 */
@Service("appCouponService")
public class AppCouponServiceImpl implements AppCouponService {

	@Autowired
	private CouponDao couponDao;

	@Autowired
	private UserCouponDao userCouponDao;
	
	@Autowired
	private CouponProdService couponProdService;

	@Autowired
	private CouponShopService couponShopService;
	
	@Autowired 
	private ProductService productService;

	/**
	 * @param @param  userName
	 * @param @param  curPageNO
	 * @param @param  useStatus
	 * @param @return
	 * @Description: 根据状态获取优惠卷信息
	 * @date 2016-7-19
	 */
	@Override
	public AppPageSupport<PersonalCouponDto> queryCoupon(String userId, String curPageNO, Integer useStatus) {
		SimpleSqlQuery query = new SimpleSqlQuery(PersonalCouponDto.class, 10, curPageNO);
		QueryMap map = new QueryMap();

		map.put("userId", userId);
		map.put("couponProvider", CouponProviderEnum.SHOP.value());
		query.setParam(map.toArray());

		if (UserCouponEnum.UNUSED.value().equals(useStatus)) {
			//查询未使用未过期的优惠券
			query.setAllCountString(ConfigCode.getInstance().getCode("app.coupon.queryUnuseCouponCount", map));
			query.setQueryString(ConfigCode.getInstance().getCode("app.coupon.queryUnuseCoupon", map));
		} else if (UserCouponEnum.USED.value().equals(useStatus)) {

			//查询已使用的优惠券
			query.setAllCountString(ConfigCode.getInstance().getCode("app.coupon.queryAlreadyUseCouponCount", map));
			query.setQueryString(ConfigCode.getInstance().getCode("app.coupon.queryAlreadyUseCoupon", map));
		} else {
			//查询未使用已过期的优惠券
			query.setAllCountString(ConfigCode.getInstance().getCode("app.coupon.queryUnuseOutTimeCouponCount", map));
			query.setQueryString(ConfigCode.getInstance().getCode("app.coupon.queryUnuseOutTimeCoupon", map));
		}

		PageSupport<PersonalCouponDto> ps = couponDao.query(query);
		AppPageSupport<PersonalCouponDto> appPageSupport = convertToAppPageSupport(ps);
		return appPageSupport;
	}

	@Override
	public AppPageSupport<PersonalCouponDto> queryRedPack(String userId, String curPageNO,
														  Integer useStatus) {
		SimpleSqlQuery query = new SimpleSqlQuery(PersonalCouponDto.class, 10, curPageNO);
		QueryMap map = new QueryMap();

		map.put("userId", userId);
		map.put("couponProvider", CouponProviderEnum.PLATFORM.value());
		query.setParam(map.toArray());

		//查询未使用未过期的红包
		if (UserCouponEnum.UNUSED.value().equals(useStatus)) {
			query.setAllCountString(ConfigCode.getInstance().getCode("coupon.queryCountUnuseRedPacket", map));
			query.setQueryString(ConfigCode.getInstance().getCode("coupon.queryUnuseRedPacket", map));
		} else if (UserCouponEnum.USED.value().equals(useStatus)) {
			//查询已使用的红包
			query.setAllCountString(ConfigCode.getInstance().getCode("coupon.queryAlreadyUseRedPacketCount", map));
			query.setQueryString(ConfigCode.getInstance().getCode("coupon.queryAlreadyUseRedPacket", map));
		} else {
			//查询未使用已过期的红包
			query.setAllCountString(ConfigCode.getInstance().getCode("coupon.queryUnuseOutTimeRedPacketCount", map));
			query.setQueryString(ConfigCode.getInstance().getCode("coupon.queryUnuseOutTimeRedPacket", map));
		}

		PageSupport<PersonalCouponDto> ps = couponDao.query(query);
		AppPageSupport<PersonalCouponDto> appPageSupport = convertToAppPageSupport(ps);
		/*return PageUtils.convertPageSupport(ps, PersonalCouponDto.class);*/
		return appPageSupport;
	}


	/**
	 * @param @param  id
	 * @param @return
	 * @Description: 根据ID获取优惠卷
	 * @date 2016-7-19
	 */
	@Override
	public Coupon getCoupon(Long id) {
		return couponDao.getCoupon(id);
	}


	/**
	 * @param @param userCoupon
	 * @param @param userDetail
	 * @param @param cou
	 * @Description: 正常绑定 优惠券到用户
	 * @date 2016-7-19
	 */
	@Override
	public void updateUsercouponProperty(UserCoupon userCoupon, String userName, String userId, Coupon cou) {
		/**
		 * 1 修改usercoupon 绑定状态
		 * 2 修改coupon 绑定数量
		 */
		if (userCoupon != null && userCoupon.getCouponId() > 0) {
			userCoupon.setUserName(userName);
			userCoupon.setUserId(userId);
			userCoupon.setGetTime(new Date());
			userCouponDao.updateUserCoupon(userCoupon);
			if (cou != null) {
				cou.setBindCouponNumber(cou.getBindCouponNumber() + 1);
				couponDao.updateCoupon(cou);
			}
		}

	}
	
	private <T> AppPageSupport<T> convertToAppPageSupport(PageSupport<T> ps) {
		AppPageSupport<T> result = new AppPageSupport<T>();
		result.setCurrPage(ps.getCurPageNO());
		result.setPageCount(ps.getPageCount());
		result.setPageSize(ps.getPageSize());
		result.setResultList(ps.getResultList());
		result.setTotal(ps.getTotal());
		return result;
	}

	@Override
	public AppUserCouponListDto convertToAppUserCouponListDto(UserCouponListDto couponListDto) {
		List<UserCoupon> usableUserCouponList = couponListDto.getUsableUserCouponList();
		List<UserCoupon> disableUserCouponList = couponListDto.getDisableUserCouponList();
		List<AppUserCouponDto> usableUserCouponDtoList = new ArrayList<AppUserCouponDto>();
		List<AppUserCouponDto> disableUserCouponDtoList = new ArrayList<AppUserCouponDto>();
		AppUserCouponListDto appUserCouponListDto = new AppUserCouponListDto();
		//可用的转换
		if(AppUtils.isNotBlank(usableUserCouponList)){
			for (UserCoupon userCoupon : usableUserCouponList) {
				AppUserCouponDto usableUserCoupon = convertToAppUserCouponDto(userCoupon);
				usableUserCouponDtoList.add(usableUserCoupon);
			}
		}
		//不可用的转换
		if(AppUtils.isNotBlank(disableUserCouponList)){
			for (UserCoupon userCoupon1 : disableUserCouponList) {
				AppUserCouponDto disableUserCoupon = convertToAppUserCouponDto(userCoupon1);
				disableUserCouponDtoList.add(disableUserCoupon);
			}
		}
		appUserCouponListDto.setUsableUserCouponList(usableUserCouponDtoList);
		appUserCouponListDto.setDisableUserCouponList(disableUserCouponDtoList);
		
		return appUserCouponListDto;
	}
	
	public AppUserCouponDto convertToAppUserCouponDto(UserCoupon userCoupon) {
		AppUserCouponDto appUserCouponDto = new AppUserCouponDto(userCoupon);
		return appUserCouponDto;
	}

	@Override
	public AppPageSupport<AppCouponDto> getCouponByCouponType(String curPageNO, String couponType) {
		SimpleSqlQuery q = new SimpleSqlQuery(Coupon.class, 10, curPageNO);
		QueryMap map = new QueryMap();
		Date nowDate = new Date();
		String querySQL;
		String queryAllSQL;
		map.put("getType", CouponGetTypeEnum.FREE.value());
		map.put("endDate", nowDate);
		if (couponType.equals(AppCouponTypeEnum.ALL_PLATFORM.value())) {
			//全场通用
			map.put("couponProvider", CouponProviderEnum.PLATFORM.value());
			map.put("couponType", CouponTypeEnum.COMMON.value());
			querySQL = ConfigCode.getInstance().getCode("coupon.queryCouponListByAllPlatform", map);
			queryAllSQL = ConfigCode.getInstance().getCode("coupon.queryCouponListByAllPlatformCount", map);
		} else if (couponType.equals(AppCouponTypeEnum.SHOP_PROD.value())) {
			//店铺商品
			map.put("couponProvider", CouponProviderEnum.SHOP.value());
			map.put("couponType", CouponTypeEnum.PRODUCT.value());
			querySQL = ConfigCode.getInstance().getCode("coupon.queryCouponListByShop", map);
			queryAllSQL = ConfigCode.getInstance().getCode("coupon.queryCouponListByShopCount", map);
		}else{
			//店铺券
			map.put("couponProvider2", CouponProviderEnum.SHOP.value());
			map.put("couponType2", CouponTypeEnum.COMMON.value());
			map.put("couponProvider3", CouponProviderEnum.PLATFORM.value());
			map.put("couponType3", CouponTypeEnum.SHOP.value());
			querySQL = ConfigCode.getInstance().getCode("coupon.queryCouponListByShop", map);
			queryAllSQL = ConfigCode.getInstance().getCode("coupon.queryCouponListByShopCount", map);
		}
		q.setAllCountString(queryAllSQL);
		q.setQueryString(querySQL);
		q.setParam(map.toArray());
		PageSupport<Coupon> ps = couponDao.queryCoupon(q);
		AppPageSupport<AppCouponDto> pageSupportDto = PageUtils.convertPageSupport(ps,new PageUtils.ResultListConvertor<Coupon, AppCouponDto>() {
			@Override
			public List<AppCouponDto> convert(List<Coupon> form) {
				List<AppCouponDto> toList = new ArrayList<AppCouponDto>();
				for (Coupon coupon : form) {
					if (AppUtils.isNotBlank(coupon)) {
						AppCouponDto appStoreProdDto = convertToAppCouponDto(coupon);
						toList.add(appStoreProdDto);
					}
				}
				return toList;
			}
			
		});
		return pageSupportDto;
	}

	@Override
	public AppPageSupport<AppIntegralCouponListDto> getIntegralCoupons(Integer pageSize,String curPageNO) {
		SimpleSqlQuery q = new SimpleSqlQuery(Coupon.class, pageSize, curPageNO);
		QueryMap map = new QueryMap();
		Date nowDate = new Date();
		map.put("getType", CouponGetTypeEnum.POINTS.value());
		map.put("endDate", nowDate);
		String querySQL = ConfigCode.getInstance().getCode("coupon.queryIntegralCouponList", map);
		String queryAllSQL = ConfigCode.getInstance().getCode("coupon.queryIntegralCouponListCount", map);
		q.setAllCountString(queryAllSQL);
		q.setQueryString(querySQL);
		q.setParam(map.toArray());
		PageSupport<Coupon> ps = couponDao.queryCoupon(q);
		AppPageSupport<AppIntegralCouponListDto> convertPageSupport = PageUtils.convertPageSupport(ps,new PageUtils.ResultListConvertor<Coupon, AppIntegralCouponListDto>() {
			@Override
			public List<AppIntegralCouponListDto> convert(List<Coupon> form) {
				List<AppIntegralCouponListDto> toList = new ArrayList<AppIntegralCouponListDto>();
				for (Coupon coupon : form) {
					if (AppUtils.isNotBlank(coupon)) {
						AppIntegralCouponListDto appStoreProdDto = convertToAppIntegralCouponDto(coupon);
						toList.add(appStoreProdDto);
					}
				}
				return toList;
			}
			private AppIntegralCouponListDto convertToAppIntegralCouponDto(Coupon coupon){
				AppIntegralCouponListDto appCouponDto = new AppIntegralCouponListDto();
				appCouponDto.setCouponId(coupon.getCouponId());
				appCouponDto.setCouponName(coupon.getCouponName());
				appCouponDto.setStatus(coupon.getStatus());
				appCouponDto.setCouponPic(coupon.getCouponPic());
				appCouponDto.setBindCouponNumber(coupon.getBindCouponNumber());
				appCouponDto.setNeedPoints(coupon.getNeedPoints());
				return appCouponDto;
			}
		});
		return convertPageSupport;
	}

	/**
	 * 获取优惠券详情
	 */
	@Override
	public AppCouponDto getCouponDto(Long couponId) {
		
		Coupon coupon = couponDao.getCouponByCouponId(couponId);
		if (AppUtils.isBlank(coupon)) {
			return null;
		}
		return convertToAppCouponDto(coupon);
	}
	
	private AppCouponDto convertToAppCouponDto(Coupon coupon){
		AppCouponDto appCouponDto = new AppCouponDto();
		appCouponDto.setCouponId(coupon.getCouponId());
		appCouponDto.setCouponName(coupon.getCouponName());
		appCouponDto.setCouponNumber(coupon.getCouponNumber());
		appCouponDto.setCouponPic(coupon.getCouponPic());
		appCouponDto.setShopId(coupon.getShopId());
		appCouponDto.setShopName(coupon.getShopName());
		appCouponDto.setCouponSn(coupon.getCouponSn());
		appCouponDto.setCouponPrice(coupon.getCouponPrice());
		appCouponDto.setCouponProvider(coupon.getCouponProvider());
		appCouponDto.setCouponType(coupon.getCouponType());
		appCouponDto.setDescription(coupon.getDescription());
		appCouponDto.setStartDate(coupon.getStartDate());
		appCouponDto.setEndDate(coupon.getEndDate());
		appCouponDto.setStatus(coupon.getStatus());
		appCouponDto.setCategoryId(coupon.getCategoryId());
		appCouponDto.setFullPrice(coupon.getFullPrice());
		appCouponDto.setOffPrice(coupon.getOffPrice());
		appCouponDto.setGetType(coupon.getGetType());
		appCouponDto.setSiteName(coupon.getSiteName());
		appCouponDto.setBindCouponNumber(coupon.getBindCouponNumber());
		return appCouponDto;
	}

	@Override
	public AppPageSupport<AppProdDto> queryCouponProdList(Long couponid, String curPageNO, String orders) {
		PageSupport<Product> ps = null;
		Coupon coupon = couponDao.getCoupon(couponid);
		if(CouponProviderEnum.SHOP.value().equals(coupon.getCouponProvider())) {//店铺优惠券
			if (CouponTypeEnum.PRODUCT.value().equals(coupon.getCouponType())) {//商品卷
				List<Long> prodIds = couponProdService.getCouponProdIdByCouponId(coupon.getCouponId());
				if(AppUtils.isBlank(prodIds)) {
					return null;
				}
				ps = productService.getCouponProds(null, prodIds, curPageNO, orders, null);
			}else if(CouponTypeEnum.COMMON.value().equals(coupon.getCouponType())){
				ps = productService.getCouponProds(coupon.getShopId(), null, curPageNO, orders, null);
			}
		}
		AppPageSupport<AppProdDto> aps=PageUtils.convertPageSupport(ps,new PageUtils.ResultListConvertor<Product, AppProdDto>() {

			@Override
			public List<AppProdDto> convert(List<Product> form) {
				List<AppProdDto> AppProdDtos=new ArrayList<AppProdDto>();
				for (Product pro : form) {
					AppProdDto AppProdDto=convertToAppProdDto(pro);
					AppProdDtos.add(AppProdDto);
				}
				return AppProdDtos;
			}

			private AppProdDto convertToAppProdDto(Product pro) {
				AppProdDto appProdDto = new AppProdDto();
				appProdDto.setProdId(pro.getProdId());
				appProdDto.setCash(pro.getCash());
				appProdDto.setName(pro.getName());
				appProdDto.setPic(pro.getPic());
				appProdDto.setPrice(pro.getPrice());
				appProdDto.setProdId(pro.getProdId());
				appProdDto.setStatus(pro.getStatus());
				appProdDto.setComments(pro.getComments());
				appProdDto.setProdType(pro.getProdType());
				return appProdDto;
			}
		});
		return aps;
	}

	@Override
	public AppPageSupport<AppProdDto> queryRedpackProdList(Long couponId, String curPageNO, String orders) {
		PageSupport<Product> ps = null;
		Coupon coupon = couponDao.getCoupon(couponId);
		if (CouponProviderEnum.PLATFORM.value().equals(coupon.getCouponProvider()) && CouponTypeEnum.SHOP.value().equals(coupon.getCouponType())) {
			List<CouponShop> couponShops = couponShopService.getCouponShopByCouponId(couponId);
			if (AppUtils.isNotBlank(couponShops)) {
				List<Long> shopIds = couponShops.stream().map(CouponShop::getShopId).collect(Collectors.toList());
				if (AppUtils.isNotBlank(shopIds)) {
					ps = productService.queryRedpackProd(shopIds, curPageNO, orders);
				}
			}
		}
		AppPageSupport<AppProdDto> aps = PageUtils.convertPageSupport(ps,new PageUtils.ResultListConvertor<Product, AppProdDto>() {

			@Override
			public List<AppProdDto> convert(List<Product> form) {
				List<AppProdDto> AppProdDtos=new ArrayList<AppProdDto>();
				for (Product pro : form) {
					AppProdDto AppProdDto=convertToAppProdDto(pro);
					AppProdDtos.add(AppProdDto);
				}
				return AppProdDtos;
			}

			private AppProdDto convertToAppProdDto(Product pro) {
				AppProdDto appProdDto = new AppProdDto();
				appProdDto.setProdId(pro.getProdId());
				appProdDto.setCash(pro.getCash());
				appProdDto.setName(pro.getName());
				appProdDto.setPic(pro.getPic());
				appProdDto.setPrice(pro.getPrice());
				appProdDto.setProdId(pro.getProdId());
				appProdDto.setStatus(pro.getStatus());
				appProdDto.setComments(pro.getComments());
				appProdDto.setProdType(pro.getProdType());
				return appProdDto;
			}
		});
		return aps;
	}
}
