package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.CategoryDto;
import com.legendshop.app.service.AppCategoryService;
import com.legendshop.model.constant.ProductTypeEnum;
import com.legendshop.model.dto.group.AppGroupCategoryDto;
import com.legendshop.model.entity.Category;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.util.AppUtils;

@Service("appCategoryService")
public class AppCategoryServiceimpl implements AppCategoryService {

	@Autowired
	private CategoryService categoryService;
	
	@Override
	public List<CategoryDto> queryCategoryDtoList() {
		Integer grade = 1;
		List<Category> categoryList = categoryService.queryCategoryList(ProductTypeEnum.PRODUCT.value(),grade);
		return convert(categoryList);
	}

	private List<CategoryDto> convert(List<Category> categoryList){
		if(AppUtils.isNotBlank(categoryList)){
			List<CategoryDto> dtoList = new ArrayList<CategoryDto>();
			for (Category category : categoryList) {
				CategoryDto dto = new CategoryDto();
				dto.setId(category.getId());
				dto.setName(category.getName());
				dto.setPic(category.getPic());
				dto.setLevel(category.getGrade());
				List<CategoryDto> childrenDtoList = findChildCategory(category.getId());
				if(AppUtils.isNotBlank(childrenDtoList)){
					dto.setChildrenList(childrenDtoList);
				}else{
					dto.setChildrenList(null);
				}
				dtoList.add(dto);
			}
			return dtoList;
		}
		return null;
	}

	@Override
	public List<CategoryDto> findChildCategory(Long parentId) {
		List<Category> categoryList = categoryService.findCategory(parentId);
		return convert(categoryList);
	}

	/**
	 * 获取团购分类
	 */
	@Override
	public List<AppGroupCategoryDto> getGroupCategoryList(String categoryType) {
		
		List<Category> catList = categoryService.getCategoryList(categoryType);
		return convertGroupCategoryList(catList);
	}
	
	// 转换团购分类列表
	private List<AppGroupCategoryDto> convertGroupCategoryList(List<Category> list){
		
		if(AppUtils.isNotBlank(list)){
			
			List<AppGroupCategoryDto> groupCatList = new ArrayList<AppGroupCategoryDto>();
			for (Category category : list) {
				
				if (category.getStatus() == 1) {
					
					AppGroupCategoryDto appGroupCategoryDto = new AppGroupCategoryDto();
					appGroupCategoryDto.setId(category.getId());
					appGroupCategoryDto.setName(category.getName());
					appGroupCategoryDto.setSeq(category.getSeq());
					appGroupCategoryDto.setStatus(category.getStatus());
					
					groupCatList.add(appGroupCategoryDto);
				}
			}
			return groupCatList;
		}
		
		return null;
	}
	
}
