package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 团购下单订单详情参数Dto
 */
@ApiModel
public class GroupOrderDetailsParamsDto {
	
	/** 团购Id */
	@ApiModelProperty(value = "团购Id")
	private Long groupId;
	
	/** 订单类型:GROUP */
	@ApiModelProperty(value = "订单类型:GROUP")
	private String type;
	
	/** 地址id */
	@ApiModelProperty(value = "地址id")
	private Long adderessId;
	
	/** 商品id */
	@ApiModelProperty(value = "商品id")
	private Long prodId;
	
	/** skuId */
	@ApiModelProperty(value = "skuId ")
	private Long skuId;
	
	/** 数量 */
	@ApiModelProperty(value = "数量")
	private Integer count;

	/** 发票id */
	@ApiModelProperty(value = "发票id")
	private Long invoiceId;
	
	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getAdderessId() {
		return adderessId;
	}

	public void setAdderessId(Long adderessId) {
		this.adderessId = adderessId;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}
}
