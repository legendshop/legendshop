/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.constant.*;
import com.legendshop.spi.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.AppOrderDetailsParamsDto;
import com.legendshop.app.dto.AppOrderPayParamDto;
import com.legendshop.app.dto.AppSeckillSuccessDto;
import com.legendshop.app.dto.AppUserShopCartOrderDto;
import com.legendshop.app.service.AppCartOrderService;
import com.legendshop.app.service.AppSeckillActivityService;
import com.legendshop.base.util.CommonServiceUtil;
import com.legendshop.framework.cache.client.CacheClient;
import com.legendshop.model.dto.OrderAddParamDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.order.AddOrderMessage;
import com.legendshop.model.dto.seckill.SeckillSuccessRecord;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.SeckillSuccess;
import com.legendshop.model.entity.Sub;
import com.legendshop.model.entity.UserAddress;
import com.legendshop.processor.DelayCancelOfferProcessor;
import com.legendshop.processor.ProdSnapshotProcessor;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;
import com.legendshop.util.MD5Util;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 秒杀订单
 */
@Api(tags="秒杀订单",value="秒杀订单下单接口")
@RestController
@RequestMapping
public class AppSeckillOrderController {
	
	private final Logger log = LoggerFactory.getLogger(AppSeckillOrderController.class);
	
	/** 防止表单重复提交的Session token 失效时间 */
	private static final int SESSION_TOKEN_EXPIRE = 60 * 10;
	
	@Autowired
	private SeckillService seckillService;
	
	@Autowired
	private AppSeckillActivityService appSeckillActivityService;
	
	/** 订单处理器封装器 */
	@Autowired
	private OrderCartResolverManager orderCartResolverManager;
	
	@Autowired
	private SubService subService;
	
	// 订单常用处理工具类
	@Autowired
	private OrderUtil orderUtil;
	
	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private UserAddressService userAddressService;
	
	/** 订单延迟取消队列 **/
	@Autowired
	private DelayCancelOfferProcessor delayCancelOfferProcessor;
	
	/** 订单快照 **/
	@Autowired
	private ProdSnapshotProcessor prodSnapshotProcessor;
	
	@Autowired
	private AppCartOrderService appCartOrderService;
	
	@Autowired
	private CacheClient cacheClient;
	
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;
	
	/**
	 * 获取用户的秒杀订单列表
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@ApiOperation(value = "获取用户的秒杀订单列表", httpMethod = "POST", notes = "获取用户的秒杀订单列表" )
	@PostMapping(value = "/p/app/seckill/query")
	public ResultDto<AppPageSupport<SeckillSuccessRecord>> mergeGroupOrderDetails(HttpServletRequest request, 
			HttpServletResponse response, String curPageNO) {
		LoginedUserInfo userInfo = loginedUserService.getUser();
		if(AppUtils.isBlank(userInfo)){
			log.debug("======NOT LOGIN======");
			return ResultDtoManager.fail(-1, "还没登录");
		}
		String userId = userInfo.getUserId();
		AppPageSupport<SeckillSuccessRecord> ps = appSeckillActivityService.queryUserSeckillRecord(curPageNO, userId);
		
		List<SeckillSuccessRecord> successRecords = (List<SeckillSuccessRecord>) ps.getResultList();
		if (AppUtils.isNotBlank(successRecords)) {
			for (SeckillSuccessRecord successRecord : successRecords) {
				StringBuilder builder = new StringBuilder(10);
				builder.append(successRecord.getSeckillId()).append(successRecord.getProdId()).append(successRecord.getSkuId());
				String secretKey = MD5Util.toMD5(builder.toString() + Constants._MD5);
				successRecord.setSecretKey(secretKey);
			}
			ps.setResultList(successRecords);
		}
		return ResultDtoManager.success(ps);
	}
	
	
	/**
	 * 获取秒杀成功信息
	 * @param request
	 * @param seckillId
	 * @param skuId
	 * @return
	 */
	@ApiOperation(value = "取秒杀成功信息", httpMethod = "POST", notes = "取秒杀成功信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)	
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "path", dataType = "Long", name = "seckillId", value = "秒杀活动id"),
		@ApiImplicitParam(paramType = "path", dataType = "Long", name = "prodId", value = "prodId"),
		@ApiImplicitParam(paramType = "path", dataType = "Long", name = "skuId", value = "skuId")
	})
	@PostMapping(value = "/p/app/{seckillId}/{prodId}/{skuId}/seckillSuccess")
	public ResultDto<AppSeckillSuccessDto> seckillSuccess(@PathVariable Long seckillId, @PathVariable long prodId, @PathVariable Long skuId){
		if(AppUtils.isBlank(seckillId) || AppUtils.isBlank(prodId) || AppUtils.isBlank(skuId)){
			return ResultDtoManager.fail("访问失败,参数有误");
		}
		String userId = loginedUserService.getUser().getUserId();
		SeckillSuccess seckillSuccess = seckillService.getSeckillSucess(userId, seckillId, prodId, skuId);
		if(AppUtils.isBlank(seckillSuccess)){
			return ResultDtoManager.fail("访问失败,没有秒杀成功或者过期取消资格");
		}
		if(seckillSuccess.getStatus().intValue()==1){
			return ResultDtoManager.fail("你已经参与过该秒杀活动，请不要重复秒杀");
		}
		
		AppSeckillSuccessDto dto = appSeckillActivityService.getSeckillSuccessInfo(userId, seckillId, skuId);
		
		if(AppUtils.isBlank(dto)){
			return ResultDtoManager.fail("秒杀成功信息获取失败");
		}
		return ResultDtoManager.success(dto);
	}
	
	
	/**
	 * 提交秒杀订单的详情
	 * @param reqParams
	 * @return
	 */
	@ApiOperation(value = "提交秒杀订单的详情", httpMethod = "POST", notes = "提交秒杀订单的详情" )
	@PostMapping(value = "/p/app/seckill/orderDetails")
	public ResultDto<AppUserShopCartOrderDto> mergeGroupOrderDetails(AppOrderDetailsParamsDto reqParams) {
		
		try{
			LoginedUserInfo appToken = loginedUserService.getUser();
			
			String userId = appToken.getUserId();
			String userName = appToken.getUserName();
			
			// 秒杀订单需要判断，校检是否成功秒杀
			SeckillSuccess seckillSuccess = seckillService.getSeckillSucess(userId, reqParams.getActiveId(), reqParams.getProdId(), reqParams.getSkuId());
			if(AppUtils.isBlank(seckillSuccess)){
				return ResultDtoManager.fail("访问失败，没有秒杀成功或者过期取消资格");
			}
			/*if(seckillSuccess.getStatus().intValue()==1){
				//TODO 需要跳转到我的秒杀接口
				return PathResolver.getRedirectPath("/p/myorder?uc=uc");	
			}*/
			
			/* 根据类型查询订单列表 */
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("seckillId", seckillSuccess.getId());
			params.put("prodId", seckillSuccess.getProdId());
			params.put("skuId", seckillSuccess.getSkuId());
			params.put("number", reqParams.getCount());
			params.put("userId", userId);
			params.put("userName", userName);
			
			CartTypeEnum typeEnum = CartTypeEnum.SECKILL;

			UserShopCartList shopCartList = orderCartResolverManager.queryOrders(typeEnum, params);

			if (AppUtils.isBlank(shopCartList)) {
				return ResultDtoManager.fail("找不到您要购买的商品! ");
			}

			if (AppUtils.isBlank(shopCartList.getShopCarts())) {
				return ResultDtoManager.fail("找不到您要购买的商品! ");
			}

			shopCartList.setActiveId(reqParams.getActiveId());
			AppUserShopCartOrderDto shopCartOrderDto = appCartOrderService.getSeckillShoppingCartOrderDto(shopCartList,reqParams, userId);

			if(AppUtils.isBlank(shopCartOrderDto)){
				return ResultDtoManager.fail("找不到您要购买的商品! ");
			}
			
			// 设置订单提交令牌,防止重复提交
			Integer token = CommonServiceUtil.generateRandom();
			shopCartOrderDto.setToken(token+"");
			cacheClient.put(Constants.APP_FROM_SESSION_TOKEN + ":" + typeEnum.toCode() + ":" + userId, SESSION_TOKEN_EXPIRE, token);
			
			subService.putUserShopCartList(CartTypeEnum.SECKILL.toCode(), userId, shopCartList);
			
			return ResultDtoManager.success(shopCartOrderDto);
		}catch(Exception e){
			e.printStackTrace();
			return ResultDtoManager.fail("系统错误, 下单失败, 请联系平台客服! ");
		}
	}
	
	@ApiOperation(value = "提交秒杀订单", httpMethod = "POST", notes = "提交秒杀订单" )
	@PostMapping(value="/p/app/seckill/submitOrder")
	public ResultDto<AppOrderPayParamDto> submitOrder(HttpServletRequest request, HttpServletResponse response, OrderAddParamDto paramDto) {
		
		LoginedUserInfo appToken = loginedUserService.getUser();
		String userId = appToken.getUserId();
		String userName = appToken.getUserName();
		
		CartTypeEnum typeEnum = CartTypeEnum.SECKILL;
		
		Integer sessionToken = cacheClient.get(Constants.APP_FROM_SESSION_TOKEN + ":" + typeEnum.toCode() + ":" + userId);
		AddOrderMessage message = orderUtil.checkSubmitParam(userId, sessionToken, typeEnum, paramDto);//检查输入参数
		if(message.hasError()){//有错误就返回
			return ResultDtoManager.fail(0, message.getMessage());
		}

		// TODO 校验用户参数，SpringSession后台过期机制有问题
		if (!userDetailService.isUserExist(userName)) {
			message.setCode(SubmitOrderStatusEnum.ERR.value());
			message.setMessage("用户不存在！");
			return ResultDtoManager.fail(0, message.getMessage());
		}

		//get cache 从上个页面获取设置进入缓存的订单
		UserShopCartList userShopCartList = subService.findUserShopCartList(typeEnum.toCode(), userId);
		if(AppUtils.isBlank(userShopCartList)){
			return ResultDtoManager.fail("没有购物清单");
		}
		
		//检查金额： 如果促销导致订单金额为负数
	    if(userShopCartList.getOrderActualTotal()< 0){
	    	return ResultDtoManager.fail("订单金额为负数,下单失败!");
	    }
	    
	    //处理订单的卖家留言备注
		try {
			orderUtil.handlerRemarkText(paramDto.getRemarkText(), userShopCartList);
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResultDtoManager.fail("下单失败，请联系商城管理员或商城客服");
		}

		/** 根据id获取收货地址信息 **/
		UserAddress userAddress = userShopCartList.getUserAddress();
		if (AppUtils.isBlank(userAddress)) {
			userAddress = userAddressService.getDefaultAddress(userId);
			if (AppUtils.isBlank(userAddress)) {
				return ResultDtoManager.fail("请选择或添加您的收货地址！");
			}
			userShopCartList.setUserAddress(userAddress);
		}
			
		// 处理运费模板
		String result = orderUtil.handlerDelivery(paramDto.getDelivery(), userShopCartList);
		if (!Constants.SUCCESS.equals(result)) {
			return ResultDtoManager.fail("运费处理失败, 请联系商城管理员或商城客服！");
		}
			
		// 处理优惠券
		orderUtil.handleCouponStr(userShopCartList);

		// 验证成功，清除令牌
		request.getSession().removeAttribute(Constants.TOKEN);
		userShopCartList.setUserId(userId);
		userShopCartList.setUserName(userName);
		userShopCartList.setInvoiceId(paramDto.getInvoiceId());
		userShopCartList.setUserAddress(userAddress);
		userShopCartList.setPayManner(paramDto.getPayManner());
		userShopCartList.setType(typeEnum.toCode());
		userShopCartList.setSwitchInvoice(paramDto.getSwitchInvoice());//当前订单所属商家是否开启发票
		//userShopCartList.setActiveId(paramDto.getActiveId());

		/**
		 * 处理订单
		 */
		List<String> subNumberList = null;
		try {
			subNumberList = orderCartResolverManager.addOrder(typeEnum,userShopCartList);
			
			if (AppUtils.isBlank(subNumberList)) {
				return ResultDtoManager.fail("下单失败，请联系商城管理员或商城客服");
			}
			
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResultDtoManager.fail("下单失败，请联系商城管理员或商城客服");
		}
			
        //offer 延时取消队列
  		delayCancelOfferProcessor.process(subNumberList);
        
		//发送网站快照备份
		prodSnapshotProcessor.process(subNumberList);

		// 标记回收
		userShopCartList = null;
		// 清空缓存
		subService.evictUserShopCartCache(typeEnum.toCode(), userId);
		// 验证成功，清除令牌
		cacheClient.delete(Constants.APP_FROM_SESSION_TOKEN + ":" + typeEnum.toCode() + ":" + userId);
		
		final String [] subNumbers=new String[subNumberList.size()];
		for (int i = 0; i < subNumberList.size(); i++) {
			subNumbers[i] = subNumberList.get(i);
		}
		
		//根据订单IDs获取订单集合
		List<Sub> subs = subService.getSubBySubNums(subNumbers, userId, OrderStatusEnum.UNPAY.value());
		Double totalAmount = 0.0;//总金额
		String subject = ""; //商品描述
		StringBuffer sub_number = new StringBuffer(""); //订单流水号
		for (Sub sub : subs) {
			totalAmount= Arith.add(totalAmount,sub.getActualTotal());
			sub_number.append(sub.getSubNumber()).append(",");
			subject += subject+sub.getProdName()+",";
		}
		String subNumber = sub_number.substring(0, sub_number.length() - 1);
		
		AppOrderPayParamDto orderPay = new AppOrderPayParamDto();
		orderPay.setTotalAmount(totalAmount);
		orderPay.setSubNumber(subNumber);
		orderPay.setSubjects(subject);
		orderPay.setSubSettlementType(SubSettlementTypeEnum.USER_ORDER_PAY.value());
		
		return ResultDtoManager.success(orderPay);
	}
	
}
