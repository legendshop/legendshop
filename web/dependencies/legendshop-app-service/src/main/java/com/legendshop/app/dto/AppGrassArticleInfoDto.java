package com.legendshop.app.dto;

import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.legendshop.dao.persistence.Transient;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("种草社区文章详情Dto")
public class AppGrassArticleInfoDto {
	
	@ApiModelProperty(value="文章id")
	private Long id;
	
	@ApiModelProperty(value="文章标题")
	private String title;
	
	@ApiModelProperty(value="文章内容")
	private String content;
	
	@ApiModelProperty(value="文章简介")
	private String intro;
	
	@ApiModelProperty(value="评论数量")
	private Long comcount;
	
	@ApiModelProperty(value="关联商品数量")
	private Long procount;

	@ApiModelProperty(value="文章图片集合")
	private List<String> images;
	
	@ApiModelProperty(value="点赞数")
	private Long thumbNum;
	
	@ApiModelProperty(value="是否点赞")
	private boolean rsThumb;
	
	@ApiModelProperty(value="是否关注")
	private boolean rsConcern;
	
	@ApiModelProperty(value="作者昵称")
	private String userName;
	
	@ApiModelProperty(value="作者id")
	private String uid;
	
	@ApiModelProperty(value="用户头像")
	private String userImage;

	@ApiModelProperty(value="用户签名")
	private String userSignature;

	@ApiModelProperty(value="发表时间")
	private Date publishTime;
	
	@ApiModelProperty(value="关联商品列表")
	private List<AppGrassProdDto> prods;
	
	@ApiModelProperty(value="关联标签列表")
	private List<AppGrassLabelListDto> labels;
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getUserImage() {
		return userImage;
	}
	

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getThumbNum() {
		return thumbNum;
	}

	public void setThumbNum(Long thumbNum) {
		this.thumbNum = thumbNum;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Long getComcount() {
		return comcount;
	}

	public void setComcount(Long comcount) {
		this.comcount = comcount;
	}

	public Long getProcount() {
		return procount;
	}

	public void setProcount(Long procount) {
		this.procount = procount;
	}

	public List<AppGrassProdDto> getProds() {
		return prods;
	}

	public void setProds(List<AppGrassProdDto> prods) {
		this.prods = prods;
	}

	public boolean isRsThumb() {
		return rsThumb;
	}

	public void setRsThumb(boolean rsThumb) {
		this.rsThumb = rsThumb;
	}

	public boolean isRsConcern() {
		return rsConcern;
	}

	public void setRsConcern(boolean rsConcern) {
		this.rsConcern = rsConcern;
	}

	public List<AppGrassLabelListDto> getLabels() {
		return labels;
	}

	public void setLabels(List<AppGrassLabelListDto> labels) {
		this.labels = labels;
	}
	
	public Date getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(Date publishTime) {
		this.publishTime = publishTime;
	}

	public String getUserSignature() {
		return userSignature;
	}

	public void setUserSignature(String userSignature) {
		this.userSignature = userSignature;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}
}
