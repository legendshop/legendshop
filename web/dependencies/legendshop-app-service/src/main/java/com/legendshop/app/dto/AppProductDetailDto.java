package com.legendshop.app.dto;

import com.legendshop.model.entity.Coupon;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
@ApiModel(value="ProductDetailDto商品详情") 
public class AppProductDetailDto implements Serializable{

	private static final long serialVersionUID = 8409692281714130123L;
	
	/** 商品id **/
	@ApiModelProperty(value="商品id ")  
	private Long prodId;	
	
	/** 商品名称 **/
	@ApiModelProperty(value="商品名称")  
	private String name;
	
	/** 商品原价 **/
	@ApiModelProperty(value="商品原价")  
	private Double price;	
	
	/** 商品现价 **/
	@ApiModelProperty(value="商品现价")  
	private Double cash;
	
	/**商品图文詳情**/
	@ApiModelProperty(value="商品图文詳情")  
	private String content;
	
	/** 商品主图 **/
	@ApiModelProperty(value="商品主图")  
	private String pic;	
	
	/** 简要描述,卖点等 **/
	@ApiModelProperty(value="简要描述,卖点等")  
	private String brief;
	
	/**
	 * 商品品牌ID
	 */
	@ApiModelProperty(value="商品品牌ID")
	private Long brandId;
	
	/** 品牌名称 */
	@ApiModelProperty(value="品牌名称")
	private String brandName;
	
	/** 商品状态   1 正常   其他均为下线**/
	@ApiModelProperty(value="商品状态   1 正常   其他均为下线")  
	private Integer status;	
	
	/** 商品图片集合 **/
	@ApiModelProperty(value="商品图片集合")  
	private List<String> prodPics;	
	
	/** 商品属性集合 **/
	@ApiModelProperty(value="商品属性集合")  
	private List<AppProductPropertyDto> prodPropList;
	
	/** 商品sku集合 **/
	@ApiModelProperty(value="商品sku集合")  
	private List<AppSkuDto> skuList;
	
    /**商品属性图片集合**/
	@ApiModelProperty(value="商品属性图片集合")  
    List<AppPropValueImgDto> propValueImgList;
    
    /** 商家id */
	@ApiModelProperty(value="商家id")  
	private Long shopId;
	
	/** 商家LOGO */
	@ApiModelProperty(value="商家logo")  
	private String shopLogo;
	
    /** 商家名称 */
	@ApiModelProperty(value="商家名称")  
	private String shopName;
	
	/**店铺类型 0专营,1旗舰,2自营**/
	@ApiModelProperty(value = "店铺类型 0专营,1旗舰,2自营")
	protected Integer shopType;
	
	/** 店铺综合评分 */
	@ApiModelProperty(value="商家综合评分")  
	private String sum;

	/** 商家商品综合评分 */
	@ApiModelProperty(value="商家描述评分")
	private String prodscore;

	/** 商家物流综合评分 */
	@ApiModelProperty(value="商家物流评分")
	private String logisticsScore;

	/** 商家店铺综合评分 */
	@ApiModelProperty(value="商家服务评分")
	private String shopScore;
	
	/** 包邮信息 */
	@ApiModelProperty(value="包邮信息")  
	private String mailfee;
	
	/** 商品的分享链接 */
	@ApiModelProperty(value="商品的分享链接 ")  
	private String shareUrl;
	
	/** 用户地址中所在的省 */
	@ApiModelProperty(value="用户地址中所在的省")  
	private String province;
	
	/** 用户地址中所在的市 */
	@ApiModelProperty(value="用户地址中所在的市")  
	private String city;
	
	/** 用户地址中所在的区 */
	@ApiModelProperty(value="用户地址中所在的区")  
	private String area;

	/** 商品开售时间 **/
	private Date StartDate;
    
	/**商品相关可用优惠券集合**/
	@ApiModelProperty(value="商品相关可用优惠券集合")  
    List<Coupon> couponList;
	
	/**用户已拥有的可用优惠券集合**/
	@ApiModelProperty(value="用户已拥有的可用优惠券集合")  
    List<Coupon> userCouponList;
	
	/** 商品视频 */
	@ApiModelProperty(value="商品视频")
    private String proVideoUrl;

	/** 是否可以购买*/
	@ApiModelProperty(value = "是否可以购买(0不可以/1可以)")
	private Integer isBuy;

	public Date getStartDate() {
		return StartDate;
	}

	public void setStartDate(Date startDate) {
		StartDate = startDate;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<AppPropValueImgDto> getPropValueImgList() {
		return propValueImgList;
	}

	public void setPropValueImgList(List<AppPropValueImgDto> propValueImgList) {
		this.propValueImgList = propValueImgList;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}
	
	public String getShopLogo() {
		return shopLogo;
	}

	public void setShopLogo(String shopLogo) {
		this.shopLogo = shopLogo;
	}

	public Integer getShopType() {
		return shopType;
	}

	public void setShopType(Integer shopType) {
		this.shopType = shopType;
	}

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}
	
	public Long getBrandId() {
		return brandId;
	}

	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getCash() {
		return cash;
	}

	public void setCash(Double cash) {
		this.cash = cash;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<String> getProdPics() {
		return prodPics;
	}

	public void setProdPics(List<String> prodPics) {
		this.prodPics = prodPics;
	}

	public List<AppProductPropertyDto> getProdPropList() {
		return prodPropList;
	}

	public void setProdPropList(List<AppProductPropertyDto> prodPropList) {
		this.prodPropList = prodPropList;
	}

	public List<AppSkuDto> getSkuList() {
		return skuList;
	}

	public void setSkuList(List<AppSkuDto> skuList) {
		this.skuList = skuList;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getMailfee() {
		return mailfee;
	}

	public void setMailfee(String mailfee) {
		this.mailfee = mailfee;
	}

	public String getShareUrl() {
		return shareUrl;
	}

	public void setShareUrl(String shareUrl) {
		this.shareUrl = shareUrl;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getShopScore() {
		return shopScore;
	}

	public void setShopScore(String shopScore) {
		this.shopScore = shopScore;
	}

	public List<Coupon> getCouponList() {
		return couponList;
	}

	public void setCouponList(List<Coupon> couponList) {
		this.couponList = couponList;
	}

	public List<Coupon> getUserCouponList() {
		return userCouponList;
	}

	public void setUserCouponList(List<Coupon> userCouponList) {
		this.userCouponList = userCouponList;
	}

	public String getProVideoUrl() {
		return proVideoUrl;
	}

	public void setProVideoUrl(String proVideoUrl) {
		this.proVideoUrl = proVideoUrl;
	}

	public String getSum() {
		return sum;
	}

	public void setSum(String sum) {
		this.sum = sum;
	}

	public String getProdscore() {
		return prodscore;
	}

	public void setProdscore(String prodscore) {
		this.prodscore = prodscore;
	}

	public String getLogisticsScore() {
		return logisticsScore;
	}

	public void setLogisticsScore(String logisticsScore) {
		this.logisticsScore = logisticsScore;
	}

	public Integer getIsBuy() {
		return isBuy;
	}

	public void setIsBuy(Integer isBuy) {
		this.isBuy = isBuy;
	}
}
