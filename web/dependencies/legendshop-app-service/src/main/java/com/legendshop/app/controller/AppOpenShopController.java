/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.convert.ShopDetailConverter;
import com.legendshop.model.R;
import com.legendshop.model.constant.ShopStatusEnum;
import com.legendshop.model.constant.ShopTypeEnum;
import com.legendshop.model.dto.app.OpenShopStatusDTO;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.shop.OpenShopInfoDTO;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.ShopAudit;
import com.legendshop.model.entity.ShopDetail;
import com.legendshop.model.entity.SystemConfig;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.ShopDetailService;
import com.legendshop.spi.service.SystemConfigService;
import com.legendshop.util.AppUtils;
import com.legendshop.web.helper.IPHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 用户移动端商家入驻
 * @author Joe
 */
@RestController
@Api(tags="商家入驻",value="商家入驻相关操作")
public class AppOpenShopController {

	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;

	@Autowired
	private SystemConfigService systemConfigService;

	@Autowired
	private ShopDetailConverter shopDetailConverter;



	@ApiOperation(value = "商家入驻", httpMethod = "POST", notes = "个人中心点击商家入驻请求的接口  返回当前商家入驻状态",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/p/openShop")
	public ResultDto<OpenShopStatusDTO> openShop() {

		String userId = loginedUserService.getUser().getUserId();
		ShopDetail shopDetail = shopDetailService.getShopDetailByUserId(userId);

		Boolean openShop = systemParameterUtil.isSupportOpenShop();

		// 未开店并且后台配置不支持支持开店
		if (AppUtils.isBlank(shopDetail) && !openShop){
			return ResultDtoManager.fail("暂未开放开店功能，详情请联系后台客服或管理员");
		}
		OpenShopStatusDTO openShopStatusDTO = new OpenShopStatusDTO();

		// 未开店，跳转到开店流程
		if (AppUtils.isBlank(shopDetail)){
			openShopStatusDTO.setStatus(ShopStatusEnum.NO_SHOP.value());
			return ResultDtoManager.success(openShopStatusDTO);
		}
		// 待审核
		if (ShopStatusEnum.AUDITING.value().equals(shopDetail.getStatus())) {
			openShopStatusDTO.setAuditType("请等待平台审核");
			openShopStatusDTO.setStatus(ShopStatusEnum.AUDITING.value());
			openShopStatusDTO.setAuditOpinion("我们将在7个工作日内完成审核，请耐心等待。");
			return ResultDtoManager.success(openShopStatusDTO);
		}

		// 获取入驻审核信息
		List<ShopAudit> shopAuditList = shopDetailService.getShopAuditInfo(shopDetail.getShopId());

		// 联系人信息失败
		if (ShopStatusEnum.CONTACT_INFO_FAIL.value().equals(shopDetail.getStatus())) {

			openShopStatusDTO.setShopId(shopDetail.getShopId());
			openShopStatusDTO.setStatus(ShopStatusEnum.CONTACT_INFO_FAIL.value());
			openShopStatusDTO.setAuditType("联系人信息有误");
			openShopStatusDTO.setAuditOpinion(shopAuditList.get(0).getAuditOpinion());
			return ResultDtoManager.success(openShopStatusDTO);
		}
		// 公司信息失败
		if (ShopStatusEnum.COMPANY_INFO_FAIL.value().equals(shopDetail.getStatus())) {
			openShopStatusDTO.setShopId(shopDetail.getShopId());
			openShopStatusDTO.setStatus(ShopStatusEnum.COMPANY_INFO_FAIL.value());
			openShopStatusDTO.setAuditType("公司信息有误");
			openShopStatusDTO.setAuditOpinion(shopAuditList.get(0).getAuditOpinion());
			return ResultDtoManager.success(openShopStatusDTO);
		}
		// 店铺信息失败
		if (ShopStatusEnum.SHOP_INFO_FAIL.value().equals(shopDetail.getStatus())) {
			openShopStatusDTO.setShopId(shopDetail.getShopId());
			openShopStatusDTO.setStatus(ShopStatusEnum.SHOP_INFO_FAIL.value());
			openShopStatusDTO.setAuditType("店铺信息有误");
			openShopStatusDTO.setAuditOpinion(shopAuditList.get(0).getAuditOpinion());
			return ResultDtoManager.success(openShopStatusDTO);
		}

		// 店铺下线
		if (ShopStatusEnum.OFFLINE.value().equals(shopDetail.getStatus())) {
			return ResultDtoManager.fail("您的店铺已被平台下线，详情请联系后台客服或管理员");
		}
		// 拒绝
		if (ShopStatusEnum.DENY.value().equals(shopDetail.getStatus())) {
			return ResultDtoManager.fail("您的开店申请已被平台拒绝，详情请联系后台客服或管理员");
		}
		// 关闭店铺
		if (ShopStatusEnum.CLOSED.value().equals(shopDetail.getStatus())) {
			return ResultDtoManager.fail("您的店铺已被平台关闭，详情请联系后台客服或管理员");
		}
		// 审核通过
		if (ShopStatusEnum.NORMAL.value().equals(shopDetail.getStatus())) {

			openShopStatusDTO.setShopId(shopDetail.getShopId());
			openShopStatusDTO.setStatus(ShopStatusEnum.NORMAL.value());
			openShopStatusDTO.setAuditType("审核通过");
			openShopStatusDTO.setOther(propertiesUtil.getPcDomainName());

			return ResultDtoManager.success(openShopStatusDTO);
		}
		return ResultDtoManager.success(openShopStatusDTO);
	}



	@ApiOperation(value = "获取商家入驻协议", httpMethod = "POST", notes = "获取商家入驻协议",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/p/shopAgreement")
	public ResultDto<String> shopAgreement() {

		SystemConfig systemConfig = systemConfigService.getSystemConfig();
		String openShopProtocolTemplate = systemConfig.getOpenShopProtocolTemplate();
		return ResultDtoManager.success(openShopProtocolTemplate);
	}



	@ApiOperation(value = "保存商家入驻信息", httpMethod = "POST", notes = "保存商家入驻信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/p/saveOpenShopInfo")
	public ResultDto<String> saveOpenShopInfo(HttpServletRequest request, OpenShopInfoDTO openShopInfoDTO) {

		LoginedUserInfo user = loginedUserService.getUser();
		ShopDetail shopDetail = shopDetailService.getShopDetailByUserId(user.getUserId());

		String ipAddr = IPHelper.getIpAddr(request);
		openShopInfoDTO.setIp(ipAddr);

		// 个人入驻
		if (ShopTypeEnum.PERSONAL.value().equals(openShopInfoDTO.getType())){

			R<String> result = shopDetailService.savePersonalOpenShopInfo(user,shopDetail,openShopInfoDTO);
			if (!result.getSuccess()){
				ResultDtoManager.fail(result.getMsg());
			}

		// 企业入驻 BUSINESS
		}else {

			R<String> result = shopDetailService.saveBusinessOpenShopInfo(user,shopDetail,openShopInfoDTO);
			if (!result.getSuccess()){
				ResultDtoManager.fail(result.getMsg());
			}
		}
		return ResultDtoManager.success("提交成功");
	}

	@ApiOperation(value = "获取商家入驻信息", httpMethod = "POST", notes = "保存商家入驻信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/p/getOpenShopInfo")
	public ResultDto<OpenShopInfoDTO> getOpenShopInfo() {

		LoginedUserInfo user = loginedUserService.getUser();
		ShopDetail shopDetail = shopDetailService.getShopDetailByUserId(user.getUserId());

		OpenShopInfoDTO openShopInfoDTO = shopDetailConverter.contervtToOpenShopInfoDTO(shopDetail);
		return ResultDtoManager.success(openShopInfoDTO);
	}

}
