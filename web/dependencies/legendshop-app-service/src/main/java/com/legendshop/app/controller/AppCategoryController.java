package com.legendshop.app.controller;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiImplicitParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.BrandDto;
import com.legendshop.app.dto.CategoryDto;
import com.legendshop.app.service.AppBrandService;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.TreeNode;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.spi.service.CategoryManagerService;
import com.legendshop.spi.service.CategoryService;
import com.legendshop.util.AppUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 分类品牌的控制类
 *
 */
@Api(tags="分类品牌",value="分类、分类品牌相关操作")
@RestController
public class AppCategoryController {
	/** The log. */
	private final Logger log = LoggerFactory.getLogger(AppCategoryController.class);

	@Autowired
	private AppBrandService appBrandService;

	@Autowired
	private CategoryManagerService categoryManagerService;
	
    @SuppressWarnings("unused")
	@Autowired
    private CategoryService categoryService;
    
	/**
	 * 获取所有分类
	 * @param request
	 * @param response
	 * @return
	 */
    @ApiOperation(value = "获取所有分类信息", httpMethod = "POST", notes = "获取所有的分类信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping("/category")
	public ResultDto<List<CategoryDto>> category() {
		try {
	    	TreeNode rootTreeNode=categoryManagerService.getTreeNodeById(Constants.PRODUCT_CATEGORY_ROOT);
	    	List<CategoryDto> catDtoList = convert(rootTreeNode);
	    	
			return ResultDtoManager.success(catDtoList);
			
		} catch (Exception e) {
			log.error("",e);
		}
		return ResultDtoManager.fail();
	}
	
    /**
	 * 获取所有品牌信息
	 * @return
	 */
	@ApiOperation(value = "获取所有品牌信息", httpMethod = "POST", notes = "获取所有品牌信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "query",name = "brandName",value = "品牌名",dataType = "Long",required = false)
	@PostMapping("/brandList")
	public ResultDto<List<BrandDto>> brandList(String brandName){
		return ResultDtoManager.success(appBrandService.queryBrandDto(brandName));
	}


	/**
	 * 分类数结构转换成分类Dto列表
	 * @param rootTreeNode
	 * @return
	 */
	private List<CategoryDto> convert(TreeNode rootTreeNode){
		if(AppUtils.isNotBlank(rootTreeNode)){
			List<CategoryDto> dtoList = new ArrayList<CategoryDto>();
			for (TreeNode node : rootTreeNode.getChildList()) {
				CategoryDto dto = convertDto(node);
				traverse(dto, node);
				dtoList.add(dto);
			}
			return dtoList;
		}
		return null;
	}
	
	/**
	 * 分类数结构转换成分类Dto
	 * @param category
	 * @return
	 */
	private CategoryDto convertDto(TreeNode category){
		CategoryDto dto = new CategoryDto();
		dto.setId(category.getSelfId());
		dto.setName(category.getNodeName());
		//System.out.println(category.getLevel() + " " + category.getNodeName());
		dto.setPic(category.getPic());
		dto.setLevel(category.getLevel());
		return dto;
	}
	
	/**
	 * 深度优先遍历树
	 * @param dto
	 * @param node
	 */
	public void traverse(CategoryDto dto, TreeNode node) {
		List<TreeNode> childList = node.getChildList();
		if (childList == null || childList.isEmpty()){
			return;
		}else{
			int childNumber = childList.size();
			for (int i = 0; i < childNumber; i++) {
				TreeNode child = childList.get(i);
				CategoryDto categoryDto = convertDto(child);
				dto.addChildren(categoryDto);
				 traverse(categoryDto, child);
			}
		}
			
	}
	
}
