package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.legendshop.model.constant.OrderStatusEnum;
import com.legendshop.model.dto.order.*;
import com.legendshop.model.entity.*;
import com.legendshop.spi.service.*;
import com.legendshop.util.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.AppOrderService;
import com.legendshop.util.DateUtils;
import com.legendshop.business.dao.InvoiceSubDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AddressDto;
import com.legendshop.model.dto.app.AppOrderDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.AppSuccessOrderDto;
import com.legendshop.model.dto.app.DeliveryDto;
import com.legendshop.model.dto.app.InvoiceSubDto;
import com.legendshop.model.dto.app.OrderItemDto;
import com.legendshop.util.AppUtils;

@Service("appOrderService")
public class AppOrderServiceImpl implements AppOrderService{
	
	@Autowired
	private OrderService orderService;

	@Autowired
	private InvoiceSubDao invoiceSubDao;	
	
	@Autowired
	private SubService subService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ConstTableService constTableService;

	@Autowired
	private ProductService productService;
	
	/**
	 * 获取我的订单列表
	 */
	@Override
	public AppPageSupport<AppOrderDto> getMyOrderDtos(OrderSearchParamDto paramDto) {
		
		PageSupport<MySubDto> ps = orderService.getMyorderDtos(paramDto);
		return convertPageSupportDto(ps);
	}
	
	/**
	 * 获取我的订单详情
	 */
	@Override
	public AppOrderDto findOrderDetail(String subNumber, String userId) {
		
		MySubDto mySubDto = subService.findOrderDetail(subNumber, userId);
		if(null == mySubDto){
			return null;
		}
		
		return convertToAppOrderDto(mySubDto);
	}

	/**
	 *获取订单详情
	 * */
	@Override
	public AppOrderDto findOrder(String subNumber) {

		MySubDto mySubDto = subService.getMySubDtoBySubNumber(subNumber);
		if(null == mySubDto){
			return null;
		}

		return convertToAppOrderDto(mySubDto);
	}
	
	
	@Override
	public InvoiceSubDto loadInvoice(Long invoiceSubId) {
		if (invoiceSubId!= null) {
			InvoiceSub invoice = invoiceSubDao.getInvoiceSub(invoiceSubId);
			if (invoice != null){
				String type=invoice.getType()== 1 ? "普通发票" : "增值税";
				String title=invoice.getTitle() == 1 ? "个人" : "单位";
				String content="明细";
				UsrInvoiceSubDto usrInvoiceSubDto = new UsrInvoiceSubDto(type, title, null, content);
				return convertInvoiceSubDto(usrInvoiceSubDto);
			} 
			return null;
		}
		return null;
	}
	
	private AppPageSupport<AppOrderDto> convertPageSupportDto(PageSupport<MySubDto> pageSupport){
		if(AppUtils.isNotBlank(pageSupport)){
			AppPageSupport<AppOrderDto> pageSupportDto = new AppPageSupport<AppOrderDto>();
			pageSupportDto.setTotal(pageSupport.getTotal());
			pageSupportDto.setCurrPage(pageSupport.getCurPageNO());
			pageSupportDto.setPageSize(pageSupport.getPageSize());
			List<MySubDto> mySubDtoList = (List<MySubDto>) pageSupport.getResultList();
			List<AppOrderDto> orderDtoList = convertOrderDtoList(mySubDtoList);
			pageSupportDto.setResultList(orderDtoList);
			pageSupportDto.setPageCount(pageSupport.getPageCount());
			return pageSupportDto;
		}
		return null;
	}

	private List<AppOrderDto> convertOrderDtoList(List<MySubDto> mySubDtoList){
		if(AppUtils.isNotBlank(mySubDtoList)){
			List<AppOrderDto> dtoList = new ArrayList<AppOrderDto>();
			
			//获取订单自动取消的时间
			Integer order_cancel = subService.getOrderCancelExpireDate();
			
			for (MySubDto mySubDto : mySubDtoList) {
				AppOrderDto orderDto = new AppOrderDto();
				orderDto.setActualTotal(mySubDto.getActualTotal());
				orderDto.setTotal(mySubDto.getTotal());
				orderDto.setPayManner(mySubDto.getPayManner());
				orderDto.setDvyFlowId(mySubDto.getDvyFlowId());
				orderDto.setOrderRemark(mySubDto.getOrderRemark());
				orderDto.setProdName(mySubDto.getProdName());
				orderDto.setShopId(mySubDto.getShopId());
				orderDto.setUserId(mySubDto.getUserId());
				orderDto.setDvyType(mySubDto.getDvyType());
				orderDto.setSubType(mySubDto.getSubType());
				orderDto.setUserName(mySubDto.getUserName());
				orderDto.setProductNums(mySubDto.getProductNums());
				orderDto.setFreightAmount(mySubDto.getFreightAmount());
				orderDto.setPayDate(mySubDto.getPayDate());
				orderDto.setPayTypeName(mySubDto.getPayTypeName());
				orderDto.setFinallyDate(mySubDto.getFinallyDate());
				orderDto.setSubId(mySubDto.getSubId());
				orderDto.setStatus(mySubDto.getStatus());
				orderDto.setDvyTypeId(mySubDto.getDvyTypeId());
				orderDto.setDvyDate(mySubDto.getDvyDate());
				orderDto.setUpdateDate(mySubDto.getUpdateDate());
				orderDto.setShopName(mySubDto.getShopName());
				orderDto.setSubDate(mySubDto.getSubDate());
				orderDto.setSubNumber(mySubDto.getSubNum());
				orderDto.setPayTypeId(mySubDto.getPayTypeId());
				orderDto.setInvoiceSubId(mySubDto.getInvoiceSubId());
				orderDto.setRemindDelivery(mySubDto.isRemindDelivery());
				orderDto.setRemindDeliveryTime(mySubDto.getRemindDeliveryTime());
				orderDto.setRefundState(mySubDto.getRefundState());
				orderDto.setDiscountPrice(mySubDto.getDiscountPrice());
				orderDto.setRedpackOffPrice(mySubDto.getRedpackOffPrice());
				orderDto.setCouponOffPrice(mySubDto.getCouponOffPrice());
				orderDto.setActiveId(mySubDto.getActiveId());
				orderDto.setGroupStatus(mySubDto.getGroupStatus());
				orderDto.setMergeGroupStatus(mySubDto.getMergeGroupStatus());
				orderDto.setAddNumber(mySubDto.getAddNumber());
				//剩余支付时间
				int passTime = DateUtils.getOffsetMinutes(mySubDto.getSubDate(), new Date());
				orderDto.setOrderCancelMinute(order_cancel-passTime>0?order_cancel-passTime:0);

				//预售订单
				orderDto.setPreDepositPrice(mySubDto.getPreDepositPrice());
				orderDto.setFinalPrice(mySubDto.getFinalPrice());
				orderDto.setPayPctType(mySubDto.getPayPctType());
				orderDto.setIsPayDeposit(mySubDto.getIsPayDeposit());
				orderDto.setIsPayFinal(mySubDto.getIsPayFinal());
				orderDto.setFinalMStart(mySubDto.getFinalMStart());
				orderDto.setFinalMEnd(mySubDto.getFinalMEnd());
				
				List<OrderItemDto> orderItemDtoList =convertOrderItemDto(mySubDto,mySubDto.getSubOrderItemDtos());
				orderDto.setSubItems(orderItemDtoList);
				
				AddressDto addressDto = convertAddressDto(mySubDto.getUserAddressSub());
				orderDto.setAddressDto(addressDto);
				
				orderDto.setDeliveryDto(convertDeliveryDto(mySubDto.getDelivery()));
				orderDto.setInvoiceSub(mySubDto.getInvoiceSub());

				dtoList.add(orderDto);
			}
			return dtoList;
		}
		return null;
	}


	private boolean isInReturnValidPeriod(Long prodId,MySubDto order){
		ProductDetail prod = productService.getProdDetail(prodId);
		Integer days = 0;
		if (AppUtils.isNotBlank(prod)){
			Long categoryId = prod.getCategoryId();
			Category category = categoryService.getCategory(categoryId);
			days = category.getReturnValidPeriod();
		}
		//当分类退换货时间设置为空，获取系统默认设置
		if(days == null || days == 0){
			ConstTable constTable =constTableService.getConstTablesBykey("ORDER_SETTING", "ORDER_SETTING");
			OrderSetMgDto orderSetting=null;
			if(AppUtils.isNotBlank(constTable)){
				String setting=constTable.getValue();
				orderSetting=JSONUtil.getObject(setting, OrderSetMgDto.class);
			}
			days = Integer.parseInt(orderSetting.getAuto_product_return());
		}
		Date finallyDate = order.getFinallyDate();
		if(null == finallyDate){//说明订单还未完成，默认有效
			return true;
		}
		return isInDates(finallyDate, days);
	}

	/**
	 * 代替 com.legendshop.util.DateUtils#isInDates(java.util.Date, int) 的方法，判断当前时间是否在范围之内
	 * @param date		时间
	 * @param days		偏移天数
	 * @return
	 */
	private static boolean isInDates(Date date, long days) {
		long ms = days * 24 * 3600 * 1000;
		long times = date.getTime() + ms;
		long now = (new Date()).getTime();
		return times > now;
	}
	
	public List<OrderItemDto> convertOrderItemDto(MySubDto mySubDto,List<SubOrderItemDto> subOrderItemDtoList){
		if(AppUtils.isNotBlank(subOrderItemDtoList)){
			List<OrderItemDto> dtoList = new ArrayList<OrderItemDto>();
			for (SubOrderItemDto subOrderItemDto : subOrderItemDtoList) {
				
				OrderItemDto orderItemDto = new OrderItemDto();
				orderItemDto.setBasketCount(subOrderItemDto.getBasketCount());
				orderItemDto.setSnapshotId(subOrderItemDto.getSnapshotId());
				orderItemDto.setProdId(subOrderItemDto.getProdId());
				orderItemDto.setAttribute(subOrderItemDto.getAttribute());
				orderItemDto.setProductTotalAmout(subOrderItemDto.getProductTotalAmout());
				orderItemDto.setPic(subOrderItemDto.getPic());
				orderItemDto.setProdName(subOrderItemDto.getProdName());
				orderItemDto.setSkuId(subOrderItemDto.getSkuId());
				orderItemDto.setCash(subOrderItemDto.getCash());
				orderItemDto.setSubItemId(subOrderItemDto.getSubItemId());
				orderItemDto.setCommSts(subOrderItemDto.getCommSts());
				orderItemDto.setRefundId(subOrderItemDto.getRefundId());
				orderItemDto.setRefundState(subOrderItemDto.getRefundState());
				orderItemDto.setRefundAmount(subOrderItemDto.getRefundAmount());
				orderItemDto.setRefundType(subOrderItemDto.getRefundType());
				orderItemDto.setRefundCount(subOrderItemDto.getRefundCount());
				orderItemDto.setIsInReturnValidPeriod(true);
				//判断是否过了可退款时间
				if(OrderStatusEnum.SUCCESS.value().equals(mySubDto.getStatus()) && !isInReturnValidPeriod(orderItemDto.getProdId(),mySubDto)){
					orderItemDto.setIsInReturnValidPeriod(false);
				}
				dtoList.add(orderItemDto);
			}
			return dtoList;
		}
		return null;
	}
	
	private AddressDto convertAddressDto(UserAddressSub usrAddrSubDto){
		if(AppUtils.isNotBlank(usrAddrSubDto)){
			AddressDto addressDto = new AddressDto();
			addressDto.setReceiver(usrAddrSubDto.getReceiver());
			addressDto.setSubPost(usrAddrSubDto.getSubPost());
			addressDto.setTelphone(usrAddrSubDto.getTelphone());
			addressDto.setDetailAddress(usrAddrSubDto.getDetailAddress());
			addressDto.setCityId(usrAddrSubDto.getCityId());
			addressDto.setProvinceId(usrAddrSubDto.getProvinceId());
			addressDto.setAreaId(usrAddrSubDto.getAreaId());
			/*addressDto.setCity(usrAddrSubDto.getCity());
			addressDto.setArea(usrAddrSubDto.getArea());*/
			addressDto.setEmail(usrAddrSubDto.getEmail());
		/*	addressDto.setProvince(usrAddrSubDto.getProvince());*/
			addressDto.setSubAdds(usrAddrSubDto.getSubAdds());
			addressDto.setMobile(usrAddrSubDto.getMobile());
			return addressDto;
		}
		return null;
	}
	
	public DeliveryDto convertDeliveryDto(com.legendshop.model.dto.order.DeliveryDto delivery){
		if(AppUtils.isNotBlank(delivery)){
			DeliveryDto deliveryDto = new DeliveryDto();
			deliveryDto.setQueryUrl(delivery.getQueryUrl());
			deliveryDto.setDvyFlowId(delivery.getDvyFlowId());
			deliveryDto.setDvyTypeId(delivery.getDvyTypeId());
			deliveryDto.setDelUrl(delivery.getDelUrl());
			deliveryDto.setDelName(delivery.getDelName());
			return deliveryDto;
		}
		return null;
	}

	private InvoiceSubDto convertInvoiceSubDto(UsrInvoiceSubDto usrInvoiceSubDto){
		if(AppUtils.isNotBlank(usrInvoiceSubDto)){
			InvoiceSubDto invoiceSubDto = new InvoiceSubDto();
			invoiceSubDto.setContent(usrInvoiceSubDto.getContent());
			invoiceSubDto.setTitle(usrInvoiceSubDto.getTitle());
			invoiceSubDto.setCompany(usrInvoiceSubDto.getCompany());
			invoiceSubDto.setType(usrInvoiceSubDto.getType());
			return invoiceSubDto;
		}
		return null;
	}
	
	private AppOrderDto convertToAppOrderDto(MySubDto mySubDto){
			AppOrderDto orderDto = new AppOrderDto();
			orderDto.setActualTotal(mySubDto.getActualTotal());
			orderDto.setTotal(mySubDto.getTotal());
			orderDto.setPayManner(mySubDto.getPayManner());
			orderDto.setDvyFlowId(mySubDto.getDvyFlowId());
			orderDto.setOrderRemark(mySubDto.getOrderRemark());
			orderDto.setProdName(mySubDto.getProdName());
			orderDto.setShopId(mySubDto.getShopId());
			orderDto.setUserId(mySubDto.getUserId());
			orderDto.setDvyType(mySubDto.getDvyType());
			orderDto.setSubType(mySubDto.getSubType());
			orderDto.setUserName(mySubDto.getUserName());
			orderDto.setProductNums(mySubDto.getProductNums());
			orderDto.setFreightAmount(mySubDto.getFreightAmount());
			orderDto.setPayDate(mySubDto.getPayDate());
			orderDto.setPayTypeName(mySubDto.getPayTypeName());
			orderDto.setFinallyDate(mySubDto.getFinallyDate());
			orderDto.setSubId(mySubDto.getSubId());
			orderDto.setStatus(mySubDto.getStatus());
			orderDto.setDvyTypeId(mySubDto.getDvyTypeId());
			orderDto.setDvyDate(mySubDto.getDvyDate());
			orderDto.setUpdateDate(mySubDto.getUpdateDate());
			orderDto.setShopName(mySubDto.getShopName());
			orderDto.setSubDate(mySubDto.getSubDate());
			orderDto.setSubNumber(mySubDto.getSubNum());
			orderDto.setPayTypeId(mySubDto.getPayTypeId());
			orderDto.setInvoiceSubId(mySubDto.getInvoiceSubId());
			orderDto.setRemindDelivery(mySubDto.isRemindDelivery());
			orderDto.setRemindDeliveryTime(mySubDto.getRemindDeliveryTime());
			orderDto.setRefundState(mySubDto.getRefundState());
			orderDto.setDiscountPrice(mySubDto.getDiscountPrice());
			orderDto.setRedpackOffPrice(mySubDto.getRedpackOffPrice());
			orderDto.setCouponOffPrice(mySubDto.getCouponOffPrice());
			orderDto.setChangedPrice(mySubDto.getChangedPrice());
			
			orderDto.setGroupStatus(mySubDto.getGroupStatus());
			orderDto.setMergeGroupStatus(mySubDto.getMergeGroupStatus());
			orderDto.setAddNumber(mySubDto.getAddNumber());
			
			List<OrderItemDto> orderItemDtoList =convertOrderItemDto(mySubDto,mySubDto.getSubOrderItemDtos());
			orderDto.setSubItems(orderItemDtoList);
			
			AddressDto addressDto = convertAddressDto(mySubDto.getUserAddressSub());
			orderDto.setAddressDto(addressDto);
			
			orderDto.setDeliveryDto(convertDeliveryDto(mySubDto.getDelivery()));
			orderDto.setInvoiceSub(mySubDto.getInvoiceSub());
			
			//预售订单字段
			orderDto.setPayPctType(mySubDto.getPayPctType());
			orderDto.setPreDepositPrice(mySubDto.getPreDepositPrice());
			orderDto.setFinalPrice(mySubDto.getFinalPrice());
			orderDto.setFinalMStart(mySubDto.getFinalMStart());
			orderDto.setFinalMEnd(mySubDto.getFinalMEnd());
			orderDto.setIsPayDeposit(mySubDto.getIsPayDeposit());
			orderDto.setIsPayFinal(mySubDto.getIsPayFinal());
			
			Integer order_cancel = subService.getOrderCancelExpireDate(); //获取订单自动取消的时间
			//剩余支付时间
			int passTime = DateUtils.getOffsetMinutes(mySubDto.getSubDate(), new Date());
			orderDto.setOrderCancelMinute(order_cancel-passTime>0?order_cancel-passTime:0);
			
			return orderDto;
		}

	@Override
	public AppSuccessOrderDto findOrderDetailBySubSettlementSn(String subSettlementSn, String userId) {
		MySubDto mySubDto = subService.findOrderDetailBySubSettlementSn(subSettlementSn, userId);
		return convertToAppSuccessOrderDto(mySubDto);
	}

	private AppSuccessOrderDto convertToAppSuccessOrderDto(MySubDto mySubDto) {
		
		if(null == mySubDto){
			return null;
		}
		
		AppSuccessOrderDto appSuccessOrderDto = new AppSuccessOrderDto();
		appSuccessOrderDto.setActualTotal(mySubDto.getActualTotal());
		appSuccessOrderDto.setSubNumber(mySubDto.getSubNum());
		appSuccessOrderDto.setUserName(mySubDto.getUserName());
		appSuccessOrderDto.setDetailAddress(mySubDto.getDetailAddress());
		appSuccessOrderDto.setMobile(mySubDto.getReciverMobile());
		appSuccessOrderDto.setSubType(mySubDto.getSubType());
		appSuccessOrderDto.setReceiver(mySubDto.getReciverName());
		
		return appSuccessOrderDto;
	}
}
