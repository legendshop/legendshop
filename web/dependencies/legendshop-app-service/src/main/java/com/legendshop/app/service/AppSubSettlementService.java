/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service;

import com.legendshop.model.dto.app.AppSuccessOrderDto;
import com.legendshop.model.entity.SubSettlement;

/**
 * app端订单结算票据中心
 *
 */
public interface AppSubSettlementService  {
	
	 /**
     * 根据结算号获取结算单.
     *
     * @return the sub settlement
     */
    public abstract SubSettlement getSubSettlementBySn(String settlementSn);
    
    public SubSettlement getSubSettlement(String settlementSn, String userId);

	public abstract AppSuccessOrderDto getSubSettlementToAppOrderDto(String subSettlementSn, String userId);
    
}
