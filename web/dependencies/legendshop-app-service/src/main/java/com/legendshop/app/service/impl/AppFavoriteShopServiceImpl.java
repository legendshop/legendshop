package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.AppFavoriteShopService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.business.dao.FavoriteShopDao;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppFavoriteShopDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.FavoriteShop;
import com.legendshop.model.entity.ProductCommentStat;
import com.legendshop.spi.service.ProductCommentStatService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.Arith;

@Service("appFavoriteShopService")
public class AppFavoriteShopServiceImpl implements AppFavoriteShopService{

	@Autowired
	private FavoriteShopDao favoriteShopDao;
	
	@Autowired
	private ProductCommentStatService productCommentStatService;

	@Override
	public AppPageSupport<AppFavoriteShopDto> queryFavoriteShop(String curPageNO, Integer pageSize, String userId) {
		
		PageSupport<FavoriteShop> ps = favoriteShopDao.queryFavoriteShop(curPageNO,pageSize,userId);
		
		//转换成dto
		AppPageSupport<AppFavoriteShopDto> pageSupportDto = PageUtils.convertPageSupport(ps,
				new PageUtils.ResultListConvertor<FavoriteShop, AppFavoriteShopDto>() {

			@Override
			public List<AppFavoriteShopDto> convert(List<FavoriteShop> form) {
				
				if (AppUtils.isBlank(form)) {
					return null;
				}
				List<AppFavoriteShopDto> toList = new ArrayList<AppFavoriteShopDto>();
				for (FavoriteShop favoriteShop : form) {
					AppFavoriteShopDto appFavoriteShopDto = convertToAppFavoriteShopDto(favoriteShop);
					
					//获取店铺信誉评分
					ProductCommentStat commentStat = productCommentStatService.getProductCommentStatByShopId(appFavoriteShopDto.getShopId());
					if(AppUtils.isNotBlank(commentStat) && commentStat.getScore()!=null && commentStat.getComments()!=null){
						Double credit = Arith.div(Double.valueOf(commentStat.getScore()), Double.valueOf(commentStat.getComments()), 1);
						if (AppUtils.isBlank(credit)) {
							credit=0d;
						}
						appFavoriteShopDto.setCredit(credit);
					}
					
					toList.add(appFavoriteShopDto);
				}
				return toList;
			}
			
			private AppFavoriteShopDto convertToAppFavoriteShopDto(FavoriteShop favoriteShop){
				  AppFavoriteShopDto appFavoriteShopDto = new AppFavoriteShopDto();
				  appFavoriteShopDto.setShopPic(favoriteShop.getShopPic());
				  appFavoriteShopDto.setGradeName(favoriteShop.getGradeName());
				  appFavoriteShopDto.setFsId(favoriteShop.getFsId());
				  appFavoriteShopDto.setSiteName(favoriteShop.getSiteName());
				  appFavoriteShopDto.setShopId(favoriteShop.getShopId());
				  appFavoriteShopDto.setShopType(favoriteShop.getShopType());
				  appFavoriteShopDto.setUserName(favoriteShop.getUserName());
				  appFavoriteShopDto.setRecDate(favoriteShop.getRecDate());
				  appFavoriteShopDto.setUserId(favoriteShop.getUserId());
				  appFavoriteShopDto.setFavoriteCount(favoriteShop.getFavoriteCount());
				  return appFavoriteShopDto;
				}
		});
		return pageSupportDto;
	}
	

	/**
	 * 删除店铺收藏
	 */
	@Override
	public boolean deleteFavoriteShop(Long id,String userId) {
		return favoriteShopDao.deletefavoriteShop(id, userId);
	}

	@Override
	public void deleteFavoriteShops(String Ids, String userId) {
		String[] fsIds = Ids.split(";");
		if(AppUtils.isNotBlank(fsIds)){
			favoriteShopDao.deletefavoriteShops(fsIds, userId);
		}
	}

	@Override
	public Long getfavoriteLength(String userId) {
		return favoriteShopDao.getfavoriteLength(userId);		
	}
	
}
