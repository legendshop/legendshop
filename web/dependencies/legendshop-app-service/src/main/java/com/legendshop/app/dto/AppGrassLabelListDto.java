package com.legendshop.app.dto;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("种草社区标签Dto")
public class AppGrassLabelListDto {
	
	@ApiModelProperty(value="标签id")
	private Long id;
	
	@ApiModelProperty(value="标签名")
	private String name;

	@ApiModelProperty(value="创建时间")
	private Date createTime;
	
	@ApiModelProperty(value="是否推荐")
	private Long isrecommend;
	
	@ApiModelProperty(value="引用次数")
	private Long refcount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getIsrecommend() {
		return isrecommend;
	}

	public void setIsrecommend(Long isrecommend) {
		this.isrecommend = isrecommend;
	}

	public Long getRefcount() {
		return refcount;
	}

	public void setRefcount(Long refcount) {
		this.refcount = refcount;
	}
	
}
