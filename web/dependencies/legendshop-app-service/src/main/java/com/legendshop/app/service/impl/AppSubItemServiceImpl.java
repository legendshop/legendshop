package com.legendshop.app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.AppSubItemService;
import com.legendshop.model.dto.app.OrderItemDto;
import com.legendshop.model.entity.SubItem;
import com.legendshop.spi.service.SubItemService;
import com.legendshop.util.AppUtils;

/**
 * app 订单项service实现
 */
@Service("appSubItemService")
public class AppSubItemServiceImpl implements AppSubItemService {

	@Autowired
	private SubItemService subItemService;

	/**
	 * 获取订单项详情
	 */
	@Override
	public OrderItemDto getSubItem(Long subItemId) {

		SubItem subItem = subItemService.getSubItem(subItemId);
		if (AppUtils.isBlank(subItem)) {
			return null;
		}
		return convertToOrderItemDto(subItem);
	}
	
	//转Dto方法
	private OrderItemDto convertToOrderItemDto(SubItem subItem){
		  OrderItemDto orderItemDto = new OrderItemDto();
		  orderItemDto.setSubItemId(subItem.getSubItemId());
		  orderItemDto.setSubItemDate(subItem.getSubItemDate());
		  orderItemDto.setRefundCount(subItem.getRefundCount());
		  orderItemDto.setSnapshotId(subItem.getSnapshotId());
		  orderItemDto.setCommSts(subItem.getCommSts());
		  orderItemDto.setObtainIntegral(subItem.getObtainIntegral());
		  orderItemDto.setWeight(subItem.getWeight());
		  orderItemDto.setProdId(subItem.getProdId());
		  orderItemDto.setPic(subItem.getPic());
		  orderItemDto.setUserId(subItem.getUserId());
		  orderItemDto.setSubNumber(subItem.getSubNumber());
		  orderItemDto.setBasketCount(subItem.getBasketCount());
		  orderItemDto.setVolume(subItem.getVolume());
		  orderItemDto.setProductTotalAmout(subItem.getProductTotalAmout());
		  orderItemDto.setSubItemNumber(subItem.getSubItemNumber());
		  orderItemDto.setPrice(subItem.getPrice());
		  orderItemDto.setProdName(subItem.getProdName());
		  orderItemDto.setAttribute(subItem.getAttribute());
		  orderItemDto.setCash(subItem.getCash());
		  orderItemDto.setSkuId(subItem.getSkuId());
		  orderItemDto.setRefundId(subItem.getRefundId());
		  return orderItemDto;
		}
	
}
