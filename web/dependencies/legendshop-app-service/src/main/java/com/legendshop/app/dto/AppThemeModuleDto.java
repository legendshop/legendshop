package com.legendshop.app.dto;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app 用于封装板块与板块商品Dto
 */
@ApiModel(value="板块与板块商品Dto")
public class AppThemeModuleDto {
	
	/** 板块id */
	@ApiModelProperty(value="板块id")
	private Long themeModuleId; 
		
	/** 板块标题 */
	@ApiModelProperty(value="板块标题")
	private String title; 
		
	/** 标题颜色 */
	@ApiModelProperty(value="标题颜色", hidden = true)
	private String titleColor; 
		
	/** 标题是否在横幅区显示 */
	@ApiModelProperty(value="标题是否在横幅区显示", hidden = true)
	private Long isTitleShow; 
		
	/** 板块标题背景色 */
	@ApiModelProperty(value="板块标题背景色", hidden = true)
	private String titleBgColor; 
			
	/** 广告图 */
	@ApiModelProperty(value="广告图", hidden = true)
	private String titleBgImg; 
		
	/** 更多跳转链接 */
	@ApiModelProperty(value="更多跳转链接")
	private String moreUrl;

	/** 手机端广告链接*/
	@ApiModelProperty(value="手机端广告链接")
	private String adMobileUrl;
	
	/** 专题板块商品 */
	@ApiModelProperty(value="专题板块商品")
	private List<AppThemeModuleProdDto> prodList;

	public Long getThemeModuleId() {
		return themeModuleId;
	}

	public void setThemeModuleId(Long themeModuleId) {
		this.themeModuleId = themeModuleId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleColor() {
		return titleColor;
	}

	public void setTitleColor(String titleColor) {
		this.titleColor = titleColor;
	}

	public Long getIsTitleShow() {
		return isTitleShow;
	}

	public void setIsTitleShow(Long isTitleShow) {
		this.isTitleShow = isTitleShow;
	}

	public String getTitleBgColor() {
		return titleBgColor;
	}

	public void setTitleBgColor(String titleBgColor) {
		this.titleBgColor = titleBgColor;
	}

	public String getTitleBgImg() {
		return titleBgImg;
	}

	public void setTitleBgImg(String titleBgImg) {
		this.titleBgImg = titleBgImg;
	}

	public String getMoreUrl() {
		return moreUrl;
	}

	public void setMoreUrl(String moreUrl) {
		this.moreUrl = moreUrl;
	}

	public List<AppThemeModuleProdDto> getProdList() {
		return prodList;
	}

	public void setProdList(List<AppThemeModuleProdDto> prodList) {
		this.prodList = prodList;
	}

	public String getAdMobileUrl() {
		return adMobileUrl;
	}

	public void setAdMobileUrl(String adMobileUrl) {
		this.adMobileUrl = adMobileUrl;
	}
}
