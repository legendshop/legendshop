package com.legendshop.app.service;

import java.util.List;

import com.legendshop.app.dto.CategoryDto;
import com.legendshop.model.dto.group.AppGroupCategoryDto;


public interface AppCategoryService {

	public abstract List<CategoryDto> queryCategoryDtoList();

	public abstract List<CategoryDto> findChildCategory(Long parentId);

	/**
	 * 获取团购分类
	 * @param categoryType 分类类型
	 * @return
	 */
	public abstract List<AppGroupCategoryDto> getGroupCategoryList(String categoryType);

}
