package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import java.util.List;

@ApiModel("种草社区作者相关文章列表Dto")
public class AppGrassWriteArticleListDto {
	
	@ApiModelProperty(value="文章id")
	private Long id;
	
	@ApiModelProperty(value="文章标题")
	private String title;

	@ApiModelProperty(value="创建时间")
	private Date createTime;
	
	@ApiModelProperty(value="文章简介")
	private String intro;

	@ApiModelProperty(value="文章评论数")
	private Long comcount;

	@ApiModelProperty(value="文章内容")
	private String content;

	@ApiModelProperty(value="文章图片集合")
	private List<String> images;
	
	@ApiModelProperty(value="点赞数")
	private Long thumbNum;
	
	@ApiModelProperty(value="作者昵称")
	private String userName;
	
	@ApiModelProperty(value="作者id")
	private String uid;
	
	@ApiModelProperty(value="用户头像")
	private String userImage;
	
	@ApiModelProperty(value="是否点赞")
	private boolean rsThumb;
	
	@ApiModelProperty(value="关联商品列表")
	private List<AppGrassProdDto> prods;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}

	public Long getThumbNum() {
		return thumbNum;
	}

	public void setThumbNum(Long thumbNum) {
		this.thumbNum = thumbNum;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getUserImage() {
		return userImage;
	}

	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}

	public boolean isRsThumb() {
		return rsThumb;
	}

	public void setRsThumb(boolean rsThumb) {
		this.rsThumb = rsThumb;
	}

	public void setProds(List<AppGrassProdDto> prods) {
		this.prods = prods;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<AppGrassProdDto> getProds() {
		return prods;
	}

	public Long getComcount() {
		return comcount;
	}

	public void setComcount(Long comcount) {
		this.comcount = comcount;
	}
}
