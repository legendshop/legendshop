package com.legendshop.app.dto;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 用户评论详情
 * @author 开发很忙
 *
 */
@ApiModel("用户评论详情Dto")
public class UserCommentDetailDto {
	
	/** 评论ID */
	@ApiModelProperty(value = "评论ID")
	private Long commId;
	
	/** 评论分数  */
	@ApiModelProperty(value = "评论分数")
	private Integer score;
	
	/** 评论内容 */
	@ApiModelProperty(value = "评论内容")
	private String content;
	
	/** 评论图片 */
	@ApiModelProperty(value = "评论图片")
	private String photos;
	
	/** 评论时间 */
	@ApiModelProperty(value = "评论时间")
	private Date addTime;
	
	/** 商家回复内容 */
	@ApiModelProperty(value = "商家回复内容")
	private String shopReplyContent;
	
	/** 商家回复时间 */
	@ApiModelProperty(value = "商家回复时间")
	private Date shopReplyTime;
	
	/** 追加主键ID */
	@ApiModelProperty(value = "追加主键ID")
	private Long addCommId; 
		
	/** 追加评论内容 */
	@ApiModelProperty(value = "追加评论内容")
	private String addContent; 
	
	/** 追加评论图片 */
	@ApiModelProperty(value = "追加评论图片")
	private String addPhotos;
		
	/** 追加创建时间 */
	@ApiModelProperty(value = "追加创建时间")
	private Date addAddTime;
		
	/** 追加商家回复 */
	@ApiModelProperty(value = "追加商家回复")
	private String addShopReplyContent;
		
	/** 追加商家回复时间 */
	@ApiModelProperty(value = "追加商家回复时间")
	private Date addShopReplyTime; 

	public Long getCommId() {
		return commId;
	}

	public void setCommId(Long commId) {
		this.commId = commId;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPhotos() {
		return photos;
	}

	public void setPhotos(String photos) {
		this.photos = photos;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public String getShopReplyContent() {
		return shopReplyContent;
	}

	public void setShopReplyContent(String shopReplyContent) {
		this.shopReplyContent = shopReplyContent;
	}

	public Date getShopReplyTime() {
		return shopReplyTime;
	}

	public void setShopReplyTime(Date shopReplyTime) {
		this.shopReplyTime = shopReplyTime;
	}

	public Long getAddCommId() {
		return addCommId;
	}

	public void setAddCommId(Long addCommId) {
		this.addCommId = addCommId;
	}

	public String getAddContent() {
		return addContent;
	}

	public void setAddContent(String addContent) {
		this.addContent = addContent;
	}

	public String getAddPhotos() {
		return addPhotos;
	}

	public void setAddPhotos(String addPhotos) {
		this.addPhotos = addPhotos;
	}

	public Date getAddAddTime() {
		return addAddTime;
	}

	public void setAddAddTime(Date addAddTime) {
		this.addAddTime = addAddTime;
	}

	public String getAddShopReplyContent() {
		return addShopReplyContent;
	}

	public void setAddShopReplyContent(String addShopReplyContent) {
		this.addShopReplyContent = addShopReplyContent;
	}

	public Date getAddShopReplyTime() {
		return addShopReplyTime;
	}

	public void setAddShopReplyTime(Date addShopReplyTime) {
		this.addShopReplyTime = addShopReplyTime;
	}
}
