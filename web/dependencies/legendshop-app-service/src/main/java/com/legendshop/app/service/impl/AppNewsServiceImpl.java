package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.AppNewsDto;
import com.legendshop.app.service.AppNewsService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.NewsDto;
import com.legendshop.model.entity.News;
import com.legendshop.spi.service.NewsService;
import com.legendshop.util.AppUtils;

@Service("appNewsService")
public class AppNewsServiceImpl implements AppNewsService{
	
	@Autowired
	private NewsService newsService;

	@Override
	public AppPageSupport<NewsDto> getNewsList(String curPageNO,Long shopId) {
		PageSupport<News> ps = newsService.getNewsList(curPageNO, shopId);
		return convertPageSupportDto(ps);
	}
	
	/**
	 * 根据栏目分类id获取文章列表
	 */
	@Override
	public AppPageSupport<AppNewsDto> getNewsListPage(Long newsCategoryId, String curPageNO) {

		PageSupport<News> ps = newsService.getNews(curPageNO, newsCategoryId);
		
		//转Dto
		AppPageSupport<AppNewsDto> pageSupportDto = PageUtils.convertPageSupport(ps, new PageUtils.ResultListConvertor<News, AppNewsDto>() {

			@Override
			public List<AppNewsDto> convert(List<News> form) {
				
				if (AppUtils.isBlank(form)) {
					return null;
				}
				List<AppNewsDto> toList = new ArrayList<AppNewsDto>();
				for (News news : form) {
					AppNewsDto appNewsDto =  convertToAppNewsDto(news);
					toList.add(appNewsDto);
				}
				return toList;
			}
		});
		return pageSupportDto;
	}
	
	/**
	 * 获取文章详情
	 */
	@Override
	public AppNewsDto getNews(Long newsId) {
		
		News news = newsService.getNewsById(newsId);
		if (AppUtils.isBlank(news)) {
			return null;
		}
		return convertToAppNewsDto(news);
	}
	
	//转Dto方法
	private AppNewsDto convertToAppNewsDto(News news){
		  AppNewsDto appNewsDto = new AppNewsDto();
		  appNewsDto.setNewsContent(news.getNewsContent());
		  appNewsDto.setPositionTags(news.getPositionTags());
		  appNewsDto.setNewsCategoryId(news.getNewsCategoryId());
		  appNewsDto.setUserName(news.getUserName());
		  appNewsDto.setUserId(news.getUserId());
		  appNewsDto.setNewsTags(news.getNewsTags());
		  appNewsDto.setNewsId(news.getNewsId());
		  appNewsDto.setPositionId(news.getPositionId());
		  appNewsDto.setNewsCategoryPic(news.getNewsCategoryPic());
		  appNewsDto.setNewsTitle(news.getNewsTitle());
		  appNewsDto.setPosition(news.getPosition());
		  appNewsDto.setNewsDate(news.getNewsDate());
		  appNewsDto.setSeq(news.getSeq());
		  appNewsDto.setNewsCategoryName(news.getNewsCategoryName());
		  appNewsDto.setStatus(news.getStatus());
		  appNewsDto.setHighLine(news.getHighLine());
		  appNewsDto.setNewsBrief(news.getNewsBrief());
		  return appNewsDto;
		}
	
	
	
	
	private AppPageSupport<NewsDto> convertPageSupportDto(PageSupport<News> pageSupport){
		if(AppUtils.isNotBlank(pageSupport)){
			AppPageSupport<NewsDto> pageSupportDto = new AppPageSupport<NewsDto>();
			pageSupportDto.setTotal(pageSupport.getTotal());
			pageSupportDto.setCurrPage(pageSupport.getCurPageNO());
			pageSupportDto.setPageSize(pageSupport.getPageSize());
			
			List<News> newsList = (List<News>) pageSupport.getResultList();
			List<NewsDto> newsDtoList = convertNewsDto(newsList);
			pageSupportDto.setResultList(newsDtoList);
			pageSupportDto.setPageCount(pageSupport.getPageCount());
			return pageSupportDto;
		}
		
		return null;
	}

	private List<NewsDto> convertNewsDto(List<News> newsList){
		if(AppUtils.isNotBlank(newsList)){
			List<NewsDto> newsDtoList = new ArrayList<NewsDto>();
			for (News news : newsList) {
				NewsDto newsDto = new NewsDto();
				newsDto.setNewsId(news.getNewsId());
				newsDto.setNewsTitle(news.getNewsTitle());
				newsDtoList.add(newsDto);
			}
			
			return newsDtoList;
		}
		
		return null;
	}
}
