/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */

package com.legendshop.app.dto;

public class ZhiboConfig {
	
	//腾讯云直播应用ID
	public static int sdkAppId = 1400114698;
	
	//私钥
	public static String privateKey = "-----BEGIN PRIVATE KEY-----\n"+
			"MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQg04g7Yzp4VNjm3MsM\n"+
			"al8UHRmXnGkCKyt6Wu2ZdN+VWgChRANCAAQdsv8xj6ZizqdxNEhoZp8RLUnP9zOl\n"+
			"Wyj1s6c6WC+mGtgAQ4+c5fL5vXby2Cv1aTNtOrYbiUAVflyd98ezmCy2\n"+
			"-----END PRIVATE KEY-----";

	
	public static String publicKey = "-----BEGIN PUBLIC KEY-----\n"+
			"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEHbL/MY+mYs6ncTRIaGafES1Jz/cz\n"+
			"pVso9bOnOlgvphrYAEOPnOXy+b128tgr9WkzbTq2G4lAFX5cnffHs5gstg==\n"+
			"-----END PUBLIC KEY-----";
	
	public static String URL = "liveplay.myqcloud.com/live/";
	
	public static String BIZID = "28391";

	//直播用户端类型为用户
	public static String TYPE = "user";

}
