/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service;

import com.legendshop.app.dto.AppWithdrawalLogDto;
import com.legendshop.model.dto.app.AppPageSupport;

/**
 * 预存款变更日志服务.
 */
public interface AppWithdrawalLogService  {

	/** 查询分销的历史转账 
	 * @param userId */
	public AppPageSupport<AppWithdrawalLogDto> getPdCashLog(String curPageNO, int pageSize, String userId);

}
