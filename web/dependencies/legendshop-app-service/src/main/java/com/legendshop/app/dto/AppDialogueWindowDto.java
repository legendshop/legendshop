package com.legendshop.app.dto ;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app客服会话窗口对象Dto
 */
@ApiModel(value="app客服会话窗口对象Dto")
public class AppDialogueWindowDto  {
	
	/** 用户Id */
	@ApiModelProperty(value="登录用户Id")
	private String userId;
	
	/** 用户名 */
	@ApiModelProperty(value="登录用户名")
	private String userName;
	
	/** 咨询相关Id,为配合前端，如果为Long类型统一转字符串类型 */
	@ApiModelProperty(value="咨询相关Id,根据咨询的问题不同会有不同，目前已知:skuId,refundSn")
	private String consultId;
	
	/** IM咨询类型 */
	@ApiModelProperty(value="IM咨询类型:prod为商品;order为订单")
	private String consultType;
	
	/** 商城Id */
	@ApiModelProperty(value="店铺Id")
	private Long shopId;
	
	/** 客服Id */
	@ApiModelProperty(value="接待的客服Id")
	private String customId;
	
	/** 客服会话来源 */
	@ApiModelProperty(value="客服会话来源有PC，H5，APP")
	private String consultSource;
	
	/** sign */
	@ApiModelProperty(value="sign签名")
	private String sign;

	public AppDialogueWindowDto() {
	}

	public AppDialogueWindowDto(String consultId, String consultType, Long shopId, String userId, String userName,
			String consultSource, String sign) {
		this.consultId = consultId;
		this.consultType = consultType;
		this.shopId = shopId;
		this.userId = userId;
		this.userName = userName;
		this.consultSource = consultSource;
		this.sign = sign;
	}

	public String getConsultId() {
		return consultId;
	}

	public void setConsultId(String consultId) {
		this.consultId = consultId;
	}

	public String getConsultType() {
		return consultType;
	}

	public void setConsultType(String consultType) {
		this.consultType = consultType;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getConsultSource() {
		return consultSource;
	}

	public void setConsultSource(String consultSource) {
		this.consultSource = consultSource;
	}

	public String getCustomId() {
		return customId;
	}

	public void setCustomId(String customId) {
		this.customId = customId;
	}
	
} 
