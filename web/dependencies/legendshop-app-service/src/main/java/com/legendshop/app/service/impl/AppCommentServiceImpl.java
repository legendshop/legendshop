package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dto.UserCommentDetailDto;
import com.legendshop.app.service.AppCommentService;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.ProductCommentDto;
import com.legendshop.model.dto.UserAddCommentDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.UserCommentProdDto;
import com.legendshop.model.entity.ProdAddComm;
import com.legendshop.model.entity.ProductComment;
import com.legendshop.spi.service.ProdAddCommService;
import com.legendshop.spi.service.ProductCommentService;
import com.legendshop.util.AppUtils;

@Service("appCommentService")
public class AppCommentServiceImpl implements AppCommentService{
	
	@Autowired
	private ProductCommentService productCommentService;
	
	@Autowired
	private ProdAddCommService prodAddCommService;

	@Override
	public AppPageSupport<UserCommentProdDto> queryComments(String curPageNO,String userName) {

		PageSupport<UserCommentProdDto> ps = productCommentService.queryComments(userName, curPageNO, 8);
		AppPageSupport<UserCommentProdDto> convertToAppPageSupport = convertToAppPageSupport(ps);
		return convertToAppPageSupport;
	}
	
	@Override
	public UserCommentDetailDto getCommDetail(Long commId, String userId) {
		
		ProductComment productComment = productCommentService.getProductComment(commId, userId);
		
		if(null != productComment){
			UserCommentDetailDto commentDetail = new UserCommentDetailDto();
			
			commentDetail.setCommId(productComment.getId());
			commentDetail.setScore(productComment.getScore());
			commentDetail.setContent(productComment.getContent());
			commentDetail.setPhotos(productComment.getPhotos());
			commentDetail.setAddTime(productComment.getAddtime());
			commentDetail.setShopReplyContent(commentDetail.getShopReplyContent());
			commentDetail.setShopReplyTime(commentDetail.getShopReplyTime());
			
			ProdAddComm prodAddComm = prodAddCommService.getProdAddCommByCommId(commId);
			
			if(null != prodAddComm){
				commentDetail.setAddCommId(prodAddComm.getProdCommId());
				commentDetail.setAddContent(prodAddComm.getContent());
				commentDetail.setAddPhotos(prodAddComm.getPhotos());
				commentDetail.setAddShopReplyContent(prodAddComm.getShopReplyContent());
				commentDetail.setAddShopReplyTime(prodAddComm.getShopReplyTime());
			}
			
			return commentDetail;
		}
		
		
		return null;
	}
	
	private <T> AppPageSupport<T> convertToAppPageSupport(PageSupport<T> ps) {
		AppPageSupport<T> result = new AppPageSupport<T>();
		result.setCurrPage(ps.getCurPageNO());
		result.setPageCount(ps.getPageCount());
		result.setPageSize(ps.getPageSize());
		result.setResultList(ps.getResultList());
		result.setTotal(ps.getTotal());
		return result;
	}

	@Override
	public AppPageSupport<ProductCommentDto> prodCommentState(String curPageNO, String state, String userName) {
		PageSupport<ProductCommentDto> ps = productCommentService.queryProdCommentByState(curPageNO, state, userName);
		AppPageSupport<ProductCommentDto> convertToAppPageSupport = convertToAppPageSupport(ps);
		return convertToAppPageSupport;
	}

	@Override
	public UserAddCommentDto getUserAddCommentDto(Long prodCommId, String userId) {
		UserAddCommentDto userAddCommentDto = productCommentService.getUserAddCommentDto(prodCommId, userId);
		String photos = userAddCommentDto.getPhotos();
		if(AppUtils.isNotBlank(photos)){
			String[] photoArray = photos.split(",");
			List<String> photoList = new ArrayList<String>();
			for (String photoPath : photoArray) {
				photoList.add(photoPath);
			}
			userAddCommentDto.setPhotoList(photoList);
		}
		return userAddCommentDto;
	}
}
