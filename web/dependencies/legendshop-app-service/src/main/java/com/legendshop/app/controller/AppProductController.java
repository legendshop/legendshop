package com.legendshop.app.controller;

import java.io.ByteArrayOutputStream;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.app.dto.*;
import com.legendshop.app.dto.mergeGroup.AppMergeGroupDto;
import com.legendshop.base.util.XssFilterUtil;
import com.legendshop.model.constant.*;
import com.legendshop.model.dto.weixin.WeiXinMsg;
import com.legendshop.model.entity.*;
import com.legendshop.model.entity.store.StoreSku;
import com.legendshop.spi.service.*;
import com.legendshop.util.SafeHtml;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.legendshop.app.constants.StatusCodes;
import com.legendshop.app.dto.presell.AppPresellActivityDto;
import com.legendshop.app.service.AppProductService;
import com.legendshop.app.service.MyFavoriteService;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.biz.model.dto.AppProdCommDto;
import com.legendshop.business.service.weixin.WeixinMiniProgramApi;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.dto.ParamGroupDto;
import com.legendshop.model.dto.ProductCommentDto;
import com.legendshop.model.dto.ProductDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.AppQueryProductCommentsParams;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.group.AppGroupDto;
import com.legendshop.model.dto.seckill.Seckill;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.dto.weixin.WXACodeResultDto;
import com.legendshop.processor.VisitLogProcessor;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.uploader.model.FeignMultipartFile;
import com.legendshop.util.AppUtils;
import com.legendshop.web.helper.IPHelper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import sun.misc.Request;

/**
 * 商品操作
 */
@Api(tags = "商品", value = "商品操作")
@RestController
@SuppressWarnings("all")
public class AppProductController {

	private static Logger LOGGER = LoggerFactory.getLogger(AppProductController.class);

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private ProductService productService;

	@Autowired
	private AppProductService appProductService;

	@Autowired
	private ShopDetailService shopDetailService;

	@Autowired
	private MyFavoriteService myfavoriteService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private ProductCommentService productCommentService;

	@Autowired
	private ProductReplyService productReplyService;

	@Autowired
	private ProductArrivalInformService productArrivalInformService;

	@Autowired
	private CouponService couponService;

	@Autowired
	private UserCouponService userCouponService;

	/**
	 * 日志查看
	 */
	@Autowired
	private VisitLogProcessor visitLogProcessor;

	@Autowired
	private SystemParameterUtil systemParameterUtil;

	/**
	 * 系统配置.
	 */
	@Autowired
	private PropertiesUtil propertiesUtil;

	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private ShippingActiveService shippingActiveService;

	@Autowired
	private WeixinMpTokenService weixinMpTokenService;

	@Autowired
	private AttachmentManager attachmentManager;

	@Autowired
	private SkuService skuService;

	@Autowired
	private ProductCommentStatService productCommentStatService;

	@Autowired
	private ShopCommStatService shopCommStatService;

	@Autowired
	private DvyTypeCommStatService dvyTypeCommStatService;

	@Autowired
	private MergeGroupService mergeGroupService;

	@Autowired
	private StoreSkuService storeSkuService;

	/**
	 * 获取 商品信息
	 *
	 * @param prodId
	 * @return
	 */
	@ApiOperation(value = "获取商品详情", httpMethod = "POST", notes = "获取商品的详细信息", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "path", name = "prodId", value = "商品Id", required = true, dataType = "Long")
	@PostMapping(value = "/productDetail/{prodId}")
	public ResultDto<AppProductDetailDto> getProdDetail(HttpServletRequest request, @PathVariable Long prodId,
														@RequestParam(value = "storeId", required = false) Long storeId) {
		Float shopscore = 0f;
		Float prodscore = 0f;
		Float logisticsScore = 0f;
		DecimalFormat decimalFormat = new DecimalFormat("0.0");
		decimalFormat.setRoundingMode(RoundingMode.FLOOR);
		AppProductDetailDto productDetailDto = appProductService.getProductDetailDto(prodId);

		if (AppUtils.isNotBlank(storeId)) {
			List<StoreSku> storeSkuList = storeSkuService.getStoreSku(storeId, prodId);

			// 筛选门店没有的商品skuId
			Set<Long> skuSet = productDetailDto.getSkuList().stream().map(AppSkuDto::getSkuId).collect(Collectors.toSet());
			Set<Long> storeSkuSet = storeSkuList.stream().map(StoreSku::getSkuId).collect(Collectors.toSet());
			skuSet.removeAll(storeSkuSet);
			for (AppSkuDto appSkuDto : productDetailDto.getSkuList()) {
				for (StoreSku storeSku : storeSkuList) {
					if (appSkuDto.getSkuId().equals(storeSku.getSkuId())) {
						appSkuDto.setStocks(storeSku.getStock());
						break;
					}

					// 如果门店没有，就置0
					if (skuSet.contains(appSkuDto.getSkuId())) {
						appSkuDto.setStocks(0L);
					}
				}
			}
		}

		ResultDto<AppProductDetailDto> dto = ResultDtoManager.success();
		if (productDetailDto == null) {
			dto.setMsg("Prod not found by id " + prodId);
			return dto;
		}
		//判断 生效时间
		if (ProductStatusEnum.PROD_ONLINE.value().equals(productDetailDto.getStatus())) {
			Date startDate = productDetailDto.getStartDate();
			Date now = new Date();
			//还没生效
			if (startDate != null && startDate.after(now)) {
				request.setAttribute("invalid", true);
			}
		}

		//查询店铺的详情信息
		ShopDetail shopDetail = null;
		shopDetail = shopDetailService.getShopDetailById(productDetailDto.getShopId());

		if (AppUtils.isBlank(shopDetail) || !shopDetail.getStatus().equals(ShopStatusEnum.NORMAL.value())) {
			return ResultDtoManager.fail("商品或店铺状态异常");
		}

		// 保存商家信息
		productDetailDto.setShopId(shopDetail.getShopId());
		productDetailDto.setShopName(shopDetail.getSiteName());
		productDetailDto.setShopLogo(shopDetail.getShopPic());
		productDetailDto.setShopType(shopDetail.getShopType());


		String mailfeeStr = shippingActiveService.queryShopMailfeeStr(shopDetail.getShopId(), prodId);
//				//包邮状态 (商家状态包邮为空)
//				Integer mailfeeSts = shopDetail.getMailfeeSts();
//				if(mailfeeSts!=null && mailfeeSts.intValue() == 1){
//					//包邮类型
//					Integer mailfeeType = shopDetail.getMailfeeType();
//					Double mailfeeCon = shopDetail.getMailfeeCon();
//					mailfeeStr = null;
//					if(mailfeeType.intValue()==1){//满金额包邮
//						mailfeeStr = "满" + mailfeeCon + "元包邮";
//					}else if(mailfeeType.intValue()==2 && AppUtils.isNotBlank(mailfeeCon)){
//						mailfeeStr = "满" + mailfeeCon.intValue() + "件包邮";
//					}
//
//				}
		if (mailfeeStr != null) {
			productDetailDto.setMailfee(mailfeeStr);
		}
		//计算该店铺所有已评分商品的平均分
		ProductCommentStat prodCommentStat = productCommentStatService.getProductCommentStatByShopId(shopDetail.getShopId());
		if (prodCommentStat != null && prodCommentStat.getScore() != null && prodCommentStat.getComments() != null) {
			prodscore = (float) prodCommentStat.getScore() / prodCommentStat.getComments();
		}
		productDetailDto.setProdscore(decimalFormat.format(prodscore));

		//计算该店铺所有已评分店铺的平均分
		ShopCommStat shopCommentStat = shopCommStatService.getShopCommStatByShopId(shopDetail.getShopId());
		if (shopCommentStat != null && shopCommentStat.getScore() != null && shopCommentStat.getCount() != null) {
			shopscore = (float) shopCommentStat.getScore() / shopCommentStat.getCount();
		}
		productDetailDto.setShopScore(decimalFormat.format(shopscore));
		//计算该店铺所有已评分物流的平均分
		DvyTypeCommStat dvyTypeCommStat = dvyTypeCommStatService.getDvyTypeCommStatByShopId(shopDetail.getShopId());
		if (dvyTypeCommStat != null && dvyTypeCommStat.getScore() != null && dvyTypeCommStat.getCount() != null) {
			logisticsScore = (float) dvyTypeCommStat.getScore() / dvyTypeCommStat.getCount();
		}
		productDetailDto.setLogisticsScore(decimalFormat.format(logisticsScore));
		Float sum = 0f;
		if (shopscore != 0 && prodscore != 0 && logisticsScore != 0) {
			sum = (float) Math.floor((shopscore + prodscore + logisticsScore) / 3);
		} else if (shopscore != 0 && logisticsScore != 0) {
			sum = (float) Math.floor((shopscore + logisticsScore) / 2);
		}
		productDetailDto.setSum(decimalFormat.format(sum));
		//检查是否为自己店铺的商品
		Integer isBuy = 1;
		LoginedUserInfo user = loginedUserService.getUser();
		String userId = null;
		if (AppUtils.isNotBlank(user)) {
			userId = user.getUserId();
			if (AppUtils.isNotBlank(user.getShopId()) && AppUtils.isNotBlank(productDetailDto.getShopId())) {
				isBuy = user.getShopId().equals(productDetailDto.getShopId()) ? 0 : 1;
			}
		}
		productDetailDto.setIsBuy(isBuy);
		//获取商品的分享url
		String shareUrl = this.getProdShareUrl(prodId);
		productDetailDto.setShareUrl(shareUrl);

		/*2018-11-12 改造， 查找可用的的商品优惠券
		 * 获取商品相关优惠券
		 */
		List<Coupon> couponList = couponService.getShopAndProdsCoupons(productDetailDto.getShopId(), prodId);
		List<Coupon> resultList = new ArrayList<Coupon>();
		List<Coupon> userResultList = new ArrayList<Coupon>();
		if (AppUtils.isNotBlank(userId)) {
			// 只有登录才有用户优惠券
			for (Coupon coupon : couponList) {
				UserCoupon usercoupon = userCouponService.getUsercoupon(coupon.getCouponId(), userId);
				if (AppUtils.isNotBlank(usercoupon)) {
					userResultList.add(coupon);
				} else {
					// 下线优惠券不显示
					if (coupon == null || coupon.getStatus() == Constants.OFFLINE.intValue()) {
						continue;
						// 过期优惠券不显示
					} else if (new Date().getTime() > coupon.getEndDate().getTime()) {
						continue;
					} else {
						// 领完优惠券不显示
						if (coupon.getBindCouponNumber() >= coupon.getCouponNumber()) {
							continue;
						}
					}
					resultList.add(coupon);
				}
			}
		} else {
			// 未登录默认将所有可用优惠券放入可领列表中
			resultList.addAll(couponList);
		}
		productDetailDto.setCouponList(resultList);
		productDetailDto.setUserCouponList(userResultList);

		dto.setResult(productDetailDto);

		// 更新查看次数 TODO 不知道需不需要加
			/*if (PropertiesUtil.getObject(SysParameterEnum.VISIT_HW_LOG_ENABLE, Boolean.class)) {
				productService.updateProdViews(prodId);
			}*/

		LoginedUserInfo token = loginedUserService.getUser();

		// 多线程记录访问历史
		if (systemParameterUtil.isVisitLogEnable()) {
			if (null != token) {
				String userName = token.getUserName();
				VisitLog visitLog = new VisitLog(IPHelper.getIpAddr(request), userName, shopDetail.getShopId(), shopDetail.getSiteName(),
						prodId, productDetailDto.getName(), productDetailDto.getPic(), productDetailDto.getCash(), VisitTypeEnum.PROD.value(), VisitSourceEnum.APP.value(), new Date());
				visitLogProcessor.process(visitLog);
			}
		}
		return ResultDtoManager.success(productDetailDto);
	}

	/**
	 * 获取商品 参数
	 *
	 * @param request
	 * @param prodId
	 * @return
	 */
	@ApiOperation(value = "获取商品参数", httpMethod = "GET", notes = "获取商品参数,无需异步请求，Responses无响应", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "path", dataType = "Long", name = "prodId", value = "商品Id", required = true)
	@GetMapping(value = "/getProdParams/{prodId}")
	public ResultDto<List<ParamGroupDto>> getProdParams(HttpServletRequest request, @PathVariable Long prodId) {
		List<ParamGroupDto> prodParams = productService.getParamGroups(prodId);
		return ResultDtoManager.success(prodParams);
	}

	private String getProdShareUrl(Long prodId) {
		String domain = propertiesUtil.getMobileDomainName();
		String shareUrl = new StringBuilder(domain).append("/views/").append(prodId).toString();
		return shareUrl;
	}

	/**
	 * 是否已收藏该商品
	 *
	 * @param prodId
	 * @return
	 */
	@ApiOperation(value = "是否已收藏商品", httpMethod = "POST", notes = "是否已收藏商品,返回'OK'表示成功，返回'fail'表示失败", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "query", name = "prodId", value = "商品id", required = true, dataType = "Long")
	@PostMapping("/isExistsFavorite")
	public ResultDto<Boolean> isExistsFavorite(HttpServletRequest request, @RequestParam Long prodId) {

		LoginedUserInfo token = loginedUserService.getUser();
		if (null == token) {
			return ResultDtoManager.success(false);
		}
		boolean result = myfavoriteService.isExistsFavorite(prodId, token.getUserName());
		return ResultDtoManager.success(result);
	}

	/**
	 * 获取商品评论列表
	 *
	 * @return
	 */
	@ApiOperation(value = "获取商品评论列表", httpMethod = "POST", notes = "获取商品评论列表，返回AppProdCommDto", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value = "/productComments")
	public ResultDto<AppPageSupport<AppProdCommDto>> queryProductComments(HttpServletRequest request, AppQueryProductCommentsParams params) {
		LoginedUserInfo token = loginedUserService.getUser();
		String userId = null;
		if (null != token) {
			userId = token.getUserId();
		}
		AppPageSupport<AppProdCommDto> result = appProductService.queryProductComments(params, userId);

		return ResultDtoManager.success(result);
	}

	/**
	 * 获取商品评论详情
	 *
	 * @return
	 */
	@ApiOperation(value = "获取商品评论详情", httpMethod = "POST", notes = "获取商品评论详情，返回AppProdCommDto", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "query", dataType = "Long", name = "prodComId", value = "评论Id", required = true)
	@PostMapping(value = "/prodCommentDetail")
	public ResultDto<ProductCommentDto> queryProductComment(HttpServletRequest request, Long prodComId) {
		LoginedUserInfo token = loginedUserService.getUser();
		String userId = null;
		if (null != token) {
			userId = token.getUserId();
		}
		ProductCommentDto prodCommentDetail = productCommentService.getProductCommentDetail(prodComId, userId);
		return ResultDtoManager.success(prodCommentDetail);
	}

	/**
	 * 获取用户是否给商品 评论点赞
	 *
	 * @return
	 */
	@ApiOperation(value = "获取用户是否给评论点赞", httpMethod = "POST", notes = "获取用户是否给评论点赞,true为点赞了,false为没点", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, response = Boolean.class)
	@ApiImplicitParam(paramType = "path", dataType = "Long", name = "prodComId", value = "评论Id", required = true)
	@PostMapping(value = "/p/isProdCommUseful")
	public ResultDto<Boolean> isProdCommUseful(Long prodComId) {
		String userId = loginedUserService.getUser().getUserId();
		boolean isAlreadyUseful = productCommentService.isAlreadyUseful(prodComId, userId);

		return ResultDtoManager.success(isAlreadyUseful);
	}

	/**
	 * 获取VUE商品评论数量
	 *
	 * @param prodId
	 * @return
	 */
	@ApiOperation(value = "获取VUE商品评论数量", httpMethod = "POST", notes = "获取VUE商品评论数量,各个评价等级的数据统计", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value = "/productCommentNumbers/{prodId}")
	public ResultDto<ProductCommentCategory> productCommentNumbers(@PathVariable Long prodId) {

		//计算当前商品的评论情况
		ProductCommentCategory pcc = productCommentService.initProductCommentCategory(prodId);

		return ResultDtoManager.success(pcc);
	}

	/**
	 * 给评论点赞
	 */
	@ApiOperation(value = "给评论点赞", httpMethod = "POST", notes = "给评论点赞,返回'OK'表示成功，返回'fail'表示失败", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "query", dataType = "Long", name = "prodComId", value = "评论Id", required = true)
	@PostMapping(value = "/p/updateUsefulCounts")
	public ResultDto<String> updateUsefulCounts(Long prodComId) {
		try {
			String userId = loginedUserService.getUser().getUserId();

			ProductComment productComment = productCommentService.getProductCommentById(prodComId);

			if (null == productComment) {
				return ResultDtoManager.fail(-1, "亲, 您要操作的评论不存在!");
			}

			if (productCommentService.isAlreadyUseful(prodComId, userId)) {
				return ResultDtoManager.fail(-1, "亲, 您已经点赞过了!");
			}

			productCommentService.updateUsefulCounts(prodComId, userId);

			return ResultDtoManager.success();
		} catch (Exception e) {
			LOGGER.error("更新评论有用的次数失败!", e);
			return ResultDtoManager.fail(StatusCodes.UNKNOWN_ERROR, "对不起, 点赞失败, 请联系客服!");
		}
	}

	/**
	 * 获取评论的回复列表
	 *
	 * @param prodComId 评论ID
	 * @return
	 */
	@ApiOperation(value = "获取评论的回复列表", httpMethod = "POST", notes = "获取评论的回复列表", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "query", dataType = "long", name = "prodComId", value = "评论Id", required = true),
			@ApiImplicitParam(paramType = "query", dataType = "integer", name = "curPageNO", value = "当前页码", required = false, defaultValue = "1")
	})
	@PostMapping(value = "/commentReplyList")
	public ResultDto<AppPageSupport<AppCommentReplyDto>> queryCommentReplyList(Long prodComId, @RequestParam(defaultValue = "1") Integer curPageNO) {
		AppPageSupport<AppCommentReplyDto> result = appProductService.queryCommentReplyList(prodComId, curPageNO);

		return ResultDtoManager.success(result);
	}

	/**
	 * 回复评价
	 *
	 * @param request
	 * @param prodComId
	 * @param replyText
	 * @return
	 */
	@ApiOperation(value = "回复评价", httpMethod = "POST", notes = "回复评价", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "path", dataType = "Long", name = "prodComId", value = "评论Id", required = true),
			@ApiImplicitParam(paramType = "path", dataType = "String", name = "replyText", value = "回复内容", required = false)
	})
	@PostMapping(value = "/p/replyComment")
	public ResultDto<String> replyComment(HttpServletRequest request, Long prodComId, String replyText) {
		try {

			ProductComment productComment = productCommentService.getProductCommentById(prodComId);

			if (null == productComment) {
				return ResultDtoManager.fail(-1, "亲, 您要回复评论不存在!");
			}

			if (StringUtils.isNotBlank(replyText)) {
				String accessToken = weixinMpTokenService.getAccessToken();
				WeiXinMsg checkTitResult = WeixinMiniProgramApi.checkMsg(accessToken, replyText);
				if (!"ok".equals(checkTitResult.getErrmsg())) {
					return ResultDtoManager.fail("亲, 您提交的内容含有敏感词 , 请更正再提交！");
				}
			}


			String userId = loginedUserService.getUser().getUserId();

			String userName = userDetailService.getNickNameByUserId(userId);
			if (AppUtils.isBlank(userName)) {
				userName = loginedUserService.getUser().getUserName();
			}
			ProductReply productReply = new ProductReply();
			productReply.setProdcommId(prodComId);
			productReply.setReplyContent(XssFilterUtil.cleanXSS(replyText));
			productReply.setReplyUserId(userId);
			productReply.setReplyUserName(userName);
			productReply.setStatus(1);
			productReply.setPostip(IPHelper.getIpAddr(request));
			productReply.setReplyTime(new Date());
			productReplyService.saveProductReply(productReply);

			return ResultDtoManager.success();
		} catch (Exception e) {
			LOGGER.error("回复评论异常!", e);
			return ResultDtoManager.fail(StatusCodes.UNKNOWN_ERROR, "亲, 回复失败哦, 请联系客服!");
		}

	}

	/**
	 * 回复其他回复人
	 *
	 * @param request
	 * @param parentReplyId
	 * @param replyText
	 * @return
	 */
	@ApiOperation(value = "回复其他回复人", httpMethod = "POST", notes = "回复其他回复人，返回回复结果信息内容", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType = "path", dataType = "Long", name = "parentReplyId", value = "回复Id", required = true),
			@ApiImplicitParam(paramType = "path", dataType = "String", name = "replyText", value = "回复内容", required = false)
	})
	@PostMapping(value = "/p/replyParent")
	public ResultDto<String> replyParent(HttpServletRequest request, Long parentReplyId, String replyText) {

		try {
			ProductReply parentReply = productReplyService.getProductReply(parentReplyId);

			if (null == parentReply) {
				return ResultDtoManager.fail(-1, "亲, 您要回复的内容不存在!");
			}

			String userId = loginedUserService.getUser().getUserId();

			if (parentReply.getReplyUserId().equals(userId)) {
				return ResultDtoManager.fail(-1, "亲, 自己不能回复自己哦!");
			}
			String userName = userDetailService.getNickNameByUserId(userId);
			if (AppUtils.isBlank(userName)) {
				userName = loginedUserService.getUser().getUserName();
			}
			ProductReply productReply = new ProductReply();
			productReply.setParentUserId(parentReply.getReplyUserId());
			productReply.setParentUserName(parentReply.getReplyUserName());
			productReply.setProdcommId(parentReply.getProdcommId());
			productReply.setReplyContent(XssFilterUtil.cleanXSS(replyText));
			productReply.setParentReplyId(parentReplyId);
			productReply.setReplyUserId(userId);
			productReply.setReplyUserName(userName);
			productReply.setPostip(IPHelper.getIpAddr(request));
			productReply.setStatus(1);
			productReply.setReplyTime(new Date());
			productReplyService.saveProductReply(productReply);

			return ResultDtoManager.success();
		} catch (Exception e) {
			LOGGER.error("回复其他人回复异常!", e);
			return ResultDtoManager.fail(StatusCodes.UNKNOWN_ERROR, "亲, 回复失败哦, 请联系客服!");
		}

	}

	/**
	 * 取消收藏店铺
	 * @Description: 根据sku、省份ID获取库存
	 * @date 2016-7-22 
	 */
	/*@RequestMapping(value="/calSkuStocksByProv",method=RequestMethod.POST)
	
	public Long calSkuStocksByProv(@RequestParam Long skuId,@RequestParam Integer provinceId){
		return stockService.calculateSkuStocksByProvince(skuId, provinceId);
	}*/

	/**
	 * @param prodId
	 * @return
	 */
	@ApiOperation(value = "取消收藏商品", httpMethod = "POST", notes = "取消收藏商品,返回'OK'表示成功，返回'fail'表示失败", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "path", dataType = "Long", name = "prodId", value = "商品Id", required = true)
	@PostMapping(value = "/p/favorite/cancel")
	public ResultDto<String> cancel(@RequestParam Long prodId) {
		try {
			String userId = loginedUserService.getUser().getUserId();
			boolean result = myfavoriteService.cancelFavorite(prodId, userId);
			if (result) {
				return ResultDtoManager.success(Constants.SUCCESS);
			} else {
				return ResultDtoManager.fail(0, Constants.FAIL);
			}
		} catch (Exception e) {
			LOGGER.debug("取消商品收藏异常!", e);
			return ResultDtoManager.fail(StatusCodes.UNKNOWN_ERROR, "对不起, 取消收藏失败, 未知错误!");
		}

	}


	@ApiOperation(value = "检查是否已提交到货通知", httpMethod = "POST", notes = "到货通知，返回'OK'表示成功", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "path", dataType = "Long", name = "skuId", value = "skuId", required = true)
	@PostMapping(value = "/p/checkProdArrInfo")
	public ResultDto<String> checkProdArrInfo(HttpServletRequest request, HttpServletResponse response, @PathVariable Long skuId) {
		//判断当前用户是否已经设置该商品为到货通知
		LoginedUserInfo token = loginedUserService.getUser();
		if (AppUtils.isBlank(token.getUserId())) {
			return ResultDtoManager.fail(-1, "请登录后再添加到货通知！");
		}
		ProdArrivalInform user = productArrivalInformService.getAlreadySaveUser(token.getUserId(), skuId, 0);
		if (AppUtils.isNotBlank(user)) {
			return ResultDtoManager.fail(0, Constants.FAIL);
		} else {
			return ResultDtoManager.success(Constants.SUCCESS);
		}
	}


	@ApiOperation(value = "提交到货通知", httpMethod = "POST", notes = "到货通知，返回'OK'表示成功", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "path", dataType = "ProdArrivalInform", name = "arrInform", value = "到货通知信息", required = true)
	@PostMapping(value = "/p/saveProdArrInfo")
	public ResultDto<String> saveProdArrInfo(HttpServletRequest request, HttpServletResponse response, ProdArrivalInform arrInform) {
		LoginedUserInfo token = loginedUserService.getUser();
		if (AppUtils.isBlank(token.getUserId())) {
			return ResultDtoManager.fail(-1, "请登录后再添加到货通知！");
		}
		//添加到货通知
		ProdArrivalInform prodArrInform = new ProdArrivalInform();
		prodArrInform.setUserId(token.getUserId());
		prodArrInform.setShopId(arrInform.getShopId());
		prodArrInform.setProdId(arrInform.getProdId());
		prodArrInform.setSkuId(arrInform.getSkuId());
		if (AppUtils.isNotBlank(arrInform.getMobilePhone())) {
			prodArrInform.setMobilePhone(arrInform.getMobilePhone());
		}
		if (AppUtils.isNotBlank(arrInform.getEmail())) {
			prodArrInform.setEmail(arrInform.getEmail());
		}
		String userNmae = token.getUserName();
		prodArrInform.setUserName(userNmae);
		productArrivalInformService.saveProdArriInfo(prodArrInform);
		return ResultDtoManager.success(Constants.SUCCESS);
	}

	/**
	 * 获取生成普通商品海报相关的数据
	 *
	 * @param prodId
	 * @return
	 */
	@ApiOperation(value = "获取生成普通商品海报相关的数据", notes = "获取生成普通商品海报相关的数据", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "prodId", paramType = "query", value = "商品Id", required = true, dataType = "Long"),
/*		@ApiImplicitParam(name = "userName", paramType = "query", value = "商品Id", required = true, dataType = "Long"),
		@ApiImplicitParam(name = "sign", paramType = "query", value = "商品Id", required = true, dataType = "Long"),*/
	})
	@PostMapping("/getProdPosterData")
	public ResultDto<AppProdPosterDataDto> getProdPosterData(@RequestParam(required = true) Long prodId) {
		try {
			AppProdPosterDataDto prodPosterData = new AppProdPosterDataDto();
			/** 获取用户头像,昵称 */
			LoginedUserInfo token = loginedUserService.getUser();
			if (null != token) {
				UserDetail userDetail = userDetailService.getUserDetailById(token.getUserId());
				prodPosterData.setHeadPortrait(userDetail.getPortraitPic());
				prodPosterData.setNickName(userDetail.getNickName());
			}
			String platform = request.getHeader("platform");
			if ("H5".equals(platform)) {
				return ResultDtoManager.success(prodPosterData);
			}
			/** 获取小程序码 */
			Product product = productService.getProductById(prodId);
			if (null == product) {
				return ResultDtoManager.fail("商品不存在!");
			}
			String wxACode = product.getWxACode();
			if (AppUtils.isBlank(wxACode)) {
				String accessToken = weixinMpTokenService.getAccessToken();
				String scene = "prodId=" + prodId;
				String page = "commonModules/goodsDetail/goodsDetail";
				WXACodeResultDto wxACodeResult = WeixinMiniProgramApi.getWXACodeUnlimit(accessToken, scene, page, null, null, null, null);
				if (!wxACodeResult.isSuccess()) {//说明失败

					WXACodeResultDto.Errormsg error = wxACodeResult.getErrormsg();
					if (null == error) {
						return ResultDtoManager.fail("获取二维码错误, 未知错误!");
					}

					return ResultDtoManager.fail(error.getErrmsg());
				}
				//说明成功返回二维码图片
				// TODO 这里不应该这么去实现, 会太多图片上传到图片服务器
				ByteArrayOutputStream output = wxACodeResult.getOutputStream();

				//ByteArrayOutputStream 无需close
				wxACode = attachmentManager.upload(new FeignMultipartFile("WX_A_CODE_PROD" + prodId + ".jpg", output.toByteArray()));

				if (AppUtils.isBlank(wxACode)) {
					return ResultDtoManager.fail("获取二维码错误, 未知错误!");
				}
				/** 更新到商品 */
				product.setWxACode(wxACode);
				productService.updateProd(product);
			}
			prodPosterData.setWxACode(wxACode);
			return ResultDtoManager.success(prodPosterData);

		} catch (Exception e) {
			LOGGER.error("获取生成商品海报相关的数据错误: ", e);
			return ResultDtoManager.fail("未知错误!");
		}
	}


	/**
	 * 根据sku返回参与了哪些活动
	 *
	 * @param prodId 商品Id
	 * @param skuId  skuId
	 * @return
	 */
	@ApiOperation(value = "根据sku返回参与了哪些活动", notes = "根据sku返回参与了哪些活动", httpMethod = "POST")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "prodId", paramType = "query", value = "商品id", required = true, dataType = "Long"),
			@ApiImplicitParam(name = "skuId", paramType = "query", value = "prodId", required = true, dataType = "Long"),
			@ApiImplicitParam(name = "skuId", paramType = "query", value = "skuId", required = true, dataType = "Long")
	})
	@PostMapping(value = "/bySkuActivity")
	public ResultDto<AppProdActivityInfoDto> bySkuActivity(Long prodId, Long skuId) {
		try {
			AppProdActivityInfoDto appProdActivityInfoDto = new AppProdActivityInfoDto();
			ProductDto prod = productService.getProductDto(prodId);
			Sku sku = skuService.getSku(skuId);
			prod.setSkuId(skuId);
			prod.setSkuType(sku.getSkuType());
			appProdActivityInfoDto.setName(sku.getSkuType());
			//查询团购
			if (prod.getIsGroup() == 1) {
				Group group = productService.getGroupPrice(prodId);
				if (AppUtils.isNotBlank(group)) {
					Date nowDate = new Date();
					if (nowDate.getTime() > group.getEndTime().getTime()) {//如果当前时间已经大于活动的结束时间就把商品的状态回显
						productService.updataIsGroup(prodId, 0);    //设置不是团购商品
						prod.setIsGroup(0);
					}
					appProdActivityInfoDto.setId(group.getId());
					appProdActivityInfoDto.setStatus(group.getStatus());
					appProdActivityInfoDto.setPrice(Double.parseDouble(group.getGroupPrice().toString()));
				}
			}

			//查询秒杀
			if ("SECKILL".equals(prod.getSkuType())) {
				Seckill seckill = productService.getSeckillPrice(prod.getActiveId(), prod.getProdId(), skuId);
				if (seckill != null) {
					Date nowDate = new Date();
					if (nowDate.getTime() < seckill.getEndTime().getTime()) {//如果当前时间已经大于活动的结束时间就把商品的状态回显
						appProdActivityInfoDto.setId(seckill.getSeckillId());
						appProdActivityInfoDto.setPrice(seckill.getSeckillPrice());
					}
				}
			}

			//查询预售
			if ("PRESELL".equals(prod.getSkuType())) {
				PresellProd presell = productService.getPresellProdPrice(prod.getProdId(), skuId);
				if (presell != null) {
					Date nowDate = new Date();
					if (nowDate.getTime() < presell.getPreSaleEnd().getTime()) {//如果当前时间已经大于活动的结束时间就把商品的状态回显
						appProdActivityInfoDto.setId(presell.getId());
						appProdActivityInfoDto.setPrice(Double.parseDouble(presell.getPrePrice().toString()));
					}
				}
			}

			//查询拼团
			if ("MERGE".equals(prod.getSkuType())) {
				MergeGroup mergeGroup = mergeGroupService.getMergeGroupPrice(prod.getProdId(), skuId);
				if (mergeGroup != null) {
					Date nowDate = new Date();
					if (nowDate.getTime() < mergeGroup.getEndTime().getTime()) {//如果当前时间已经大于活动的结束时间就把商品的状态回显
						appProdActivityInfoDto.setId(mergeGroup.getId());
						appProdActivityInfoDto.setPrice(mergeGroup.getMinPrice());
					}
				}
			}
			if (AppUtils.isBlank(appProdActivityInfoDto.getId())) {
				appProdActivityInfoDto.setStatus(-6);
			}
			return ResultDtoManager.success(appProdActivityInfoDto);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultDtoManager.fail("查询失败");
		}
	}

	/**
	 * 获取用户服务说明
	 */
	@ApiOperation(value = "根据商品id获取服务说明", produces = MediaType.APPLICATION_JSON_VALUE)
	@GetMapping("/serviceShow")
	@ApiImplicitParam(name = "prodId", paramType = "query", value = "商品id", required = true, dataType = "Long")
	public ResultDto<String> getServerShow(Long prodId) {
		String result = productService.getServerShowById(prodId);
		return ResultDtoManager.success(result);
	}

}
