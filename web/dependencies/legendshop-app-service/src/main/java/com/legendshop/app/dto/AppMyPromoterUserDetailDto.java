/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;
import java.util.Date;

import com.legendshop.model.dto.app.AppPageSupport;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app我的推广好友页面对象Dto
 */
@ApiModel(value="app我的推广好友页面对象Dto")
public class AppMyPromoterUserDetailDto{

	/** 本月发展会员数 */
	@ApiModelProperty(value="本月发展会员数")
	private Long monthTotal = 0L;
	
	/** 累计发展会员数. */
	@ApiModelProperty("累计发展会员数")
	private Long totalUser = 0L;
	
	/** 直接下级会员贡献佣金. */
	@ApiModelProperty(value="直接下级会员贡献佣金")
	private Double firstDistCommis; 
	
	/** 推广下级好友信息列表. */
	@ApiModelProperty(value="推广下级好友信息列表")
	AppPageSupport<AppMyPromoterUserDto> AppMyPromoterUserDtoList;
	
	/**
	 * The Constructor.
	 */
	public AppMyPromoterUserDetailDto() {
    }

	public Long getMonthTotal() {
		return monthTotal;
	}

	public void setMonthTotal(Long monthTotal) {
		this.monthTotal = monthTotal;
	}
	
	public Long getTotalUser() {
		return totalUser;
	}

	public void setTotalUser(Long totalUser) {
		this.totalUser = totalUser;
	}

	public Double getFirstDistCommis() {
		return firstDistCommis;
	}

	public void setFirstDistCommis(Double firstDistCommis) {
		this.firstDistCommis = firstDistCommis;
	}

	public AppPageSupport<AppMyPromoterUserDto> getAppMyPromoterUserDtoList() {
		return AppMyPromoterUserDtoList;
	}

	public void setAppMyPromoterUserDtoList(AppPageSupport<AppMyPromoterUserDto> appMyPromoterUserDtoList) {
		AppMyPromoterUserDtoList = appMyPromoterUserDtoList;
	}
	
} 
