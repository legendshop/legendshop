package com.legendshop.app.dto;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("发现文章列表Dto")
public class AppDiscoverArticleListDto {
	
	@ApiModelProperty(value="文章id")
	private Long id;
	
	@ApiModelProperty(value="文章标题")
	private String name;

	@ApiModelProperty(value="创建时间")
	private Date createTime;
	
	@ApiModelProperty(value="文章简介")
	private String intro;
	
	@ApiModelProperty(value="文章图片")
	private String image;
	
	@ApiModelProperty(value="点赞数")
	private Long thumbNum;
	
	@ApiModelProperty(value="访问量")
	private Long pageView;
	
	@ApiModelProperty(value="作者名")
	private String writerName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Long getThumbNum() {
		return thumbNum;
	}

	public void setThumbNum(Long thumbNum) {
		this.thumbNum = thumbNum;
	}

	public Long getPageView() {
		return pageView;
	}

	public void setPageView(Long pageView) {
		this.pageView = pageView;
	}

	public String getWriterName() {
		return writerName;
	}

	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}
	
	
	
	
}
