/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.AppGroupService;
import com.legendshop.base.util.PageUtils;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.ProductDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.group.AppGroupDetailDto;
import com.legendshop.model.dto.group.AppGroupDto;
import com.legendshop.model.dto.group.AppGroupProductDto;
import com.legendshop.model.dto.group.GroupDto;
import com.legendshop.model.dto.group.GroupProdSeachParam;
import com.legendshop.model.entity.Group;
import com.legendshop.spi.service.GroupService;
import com.legendshop.util.AppUtils;

/**
 * 团购app服务实现类
 */
@Service("appGroupService")
public class AppGroupServiceImpl implements AppGroupService {

	@Autowired
	private GroupService groupService;

	/**
	 * 查询团购数据.
	 *
	 * @param curPageNO
	 * @param sortOrder
	 * @return the page support dto< group>
	 */
	@Override
	public AppPageSupport<AppGroupDto> queryGroupPage(String curPageNO, String sortOrder,Long groupCategoryId) {
		
		GroupProdSeachParam searchParams = new GroupProdSeachParam();
		searchParams.setOrders(sortOrder);
		searchParams.setFirstCid(groupCategoryId);
		
		PageSupport<Group> ps = groupService.queryGouplist(curPageNO, searchParams);
		return convertToAppPageSupport(ps);
	}
	
	/**
	 * 转换PageSupport.为 AppPageSupport
	 */
	private AppPageSupport<AppGroupDto> convertToAppPageSupport(PageSupport<Group> ps) {
		
		
		return PageUtils.convertPageSupport(ps, new PageUtils.ResultListConvertor<Group, AppGroupDto>() {
			@Override
			public List<AppGroupDto> convert(List<Group> form) {
				
				ArrayList<AppGroupDto> resultList = new ArrayList<AppGroupDto>();
				
				for(Group group : form){
					AppGroupDto appGroupDto = new AppGroupDto();
					appGroupDto.setId(group.getId());
					appGroupDto.setGroupName(group.getGroupName());
					appGroupDto.setGroupImage(group.getGroupImage());
					appGroupDto.setStartTime(group.getStartTime());
					appGroupDto.setEndTime(group.getEndTime());
					appGroupDto.setGroupPrice(group.getGroupPrice());
					appGroupDto.setProductId(group.getProductId());
					appGroupDto.setShopId(group.getShopId());
					appGroupDto.setShopName(group.getShopName());
					appGroupDto.setPeopleCount(group.getPeopleCount());
					appGroupDto.setBuyerCount(group.getBuyerCount());
					appGroupDto.setBuyQuantity(group.getBuyQuantity());
					
					resultList.add(appGroupDto);
				}
				
				return resultList;
			}
		});
	}
	
	/**
	 * 转换成团购所需的AppGroupDto
	 */
	@Override
	public AppGroupDetailDto convertGroupProductDto(GroupDto groupDto) {
		if (AppUtils.isBlank(groupDto)) {
			return null;
		}
		AppGroupDetailDto appGroup = new AppGroupDetailDto();
		appGroup.setId(groupDto.getId());
		appGroup.setGroupName(groupDto.getGroupName());
		appGroup.setStartTime(groupDto.getStartTime());
		appGroup.setEndTime(groupDto.getEndTime());
		appGroup.setProductId(groupDto.getProductId());
		appGroup.setShopId(groupDto.getShopId());
		appGroup.setShopName(groupDto.getShopName());
		appGroup.setGroupPrice(groupDto.getGroupPrice());
		appGroup.setPeopleCount(groupDto.getPeopleCount());
		appGroup.setBuyerCount(groupDto.getBuyerCount());
		appGroup.setBuyQuantity(groupDto.getBuyQuantity());
		appGroup.setStatus(groupDto.getStatus());
		appGroup.setGroupImage(groupDto.getGroupImage());
		appGroup.setGroupDesc(groupDto.getGroupDesc());
		appGroup.setProductDto(convertGroupProductDto(groupDto.getProductDto()));
		return appGroup;
	}

	/**
	 * 转换成团购商品所需的DTO.
	 *
	 * @param productDto
	 * @return the app group product dto
	 */
	public AppGroupProductDto convertGroupProductDto(ProductDto productDto) {
		if (AppUtils.isNotBlank(productDto)) {
			AppGroupProductDto groupPro = new AppGroupProductDto();
			groupPro.setProdId(productDto.getProdId());
			groupPro.setName(productDto.getName());
			groupPro.setBrief(productDto.getBrief());
			groupPro.setPrice(productDto.getPrice());
			groupPro.setCash(productDto.getCash());
			groupPro.setPic(productDto.getPic());
			groupPro.setStatus(productDto.getStatus());
			groupPro.setStocks(productDto.getStocks());
			groupPro.setContent(productDto.getContent());
			groupPro.setProdPropList(productDto.getProdPropDtoList());
			groupPro.setProdPics(productDto.getProdPics());
			groupPro.setSkuDtoList(productDto.getSkuDtoList());
			groupPro.setPropValueImgList(productDto.getPropValueImgList());
			groupPro.setProVideoUrl(productDto.getProVideoUrl());
			return groupPro;
		}
		return null;
	}
}
