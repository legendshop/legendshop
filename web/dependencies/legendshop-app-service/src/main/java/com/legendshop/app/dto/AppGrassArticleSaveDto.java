package com.legendshop.app.dto;

import com.legendshop.dao.persistence.Transient;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@ApiModel("种草社区新增或修改文章Dto")
public class AppGrassArticleSaveDto {
	
	@ApiModelProperty(value="文章id")
	private Long id;
	
	@ApiModelProperty(value="文章标题")
	private String title;
	
	@ApiModelProperty(value="文章内容")
	private String content;
	
	@ApiModelProperty(value="文章简介")
	private String intro;

	@ApiModelProperty(value="图片")
	private List<String> picFile;

	@ApiModelProperty(value="商品id集合")
	private List<Long> prodIds;

	@ApiModelProperty(value="标签id集合")
	private List<Long> labelIds;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public List<String> getPicFile() {
		return picFile;
	}

	public void setPicFile(List<String> picFile) {
		this.picFile = picFile;
	}

	public List<Long> getProdIds() {
		return prodIds;
	}

	public void setProdIds(List<Long> prodIds) {
		this.prodIds = prodIds;
	}

	public List<Long> getLabelIds() {
		return labelIds;
	}

	public void setLabelIds(List<Long> labelIds) {
		this.labelIds = labelIds;
	}
}
