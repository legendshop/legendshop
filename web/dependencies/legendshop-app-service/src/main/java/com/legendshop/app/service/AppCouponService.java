/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service;

import com.legendshop.app.dto.*;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.buy.PersonalCouponDto;
import com.legendshop.model.dto.coupon.UserCouponListDto;
import com.legendshop.model.entity.Coupon;
import com.legendshop.model.entity.UserCoupon;

/**
 * The Class CouponService.
 * 优惠卷
 */
public interface AppCouponService  {
    
	/**
	 * 根据userName，状态，当前页查询优惠卷信息
	 */
	public abstract AppPageSupport<PersonalCouponDto> queryCoupon(String userId,String curPageNO, Integer useStatus);
	
	/**
	 * 根据userName，状态，当前页查询红包信息
	 * @date 2016-7-19 
	 */
	public abstract AppPageSupport<PersonalCouponDto> queryRedPack(String userId, String curPageNO, Integer useStatus);

	 /**
	 * @Description: 根据ID获取优惠卷
	 * @date 2016-7-19 
	 */
	public abstract Coupon getCoupon(Long id);
	
	/**
	 * @Description:  正常绑定 优惠券到用户
	 * @date 2016-7-19 
	 */
	void updateUsercouponProperty(UserCoupon userCoupon,String userName,String userId,Coupon cou);

	/**
	 * 转换成AppUserCouponListDto
	 */
	public abstract AppUserCouponListDto convertToAppUserCouponListDto(UserCouponListDto couponListDto);

	/**
	 * 根据app端券的类型获取礼券
	 */
	public abstract AppPageSupport<AppCouponDto> getCouponByCouponType(String curPageNO, String couponType);

	/**
	 * 获取可用的积分商品券
	 */
	public abstract AppPageSupport<AppIntegralCouponListDto> getIntegralCoupons(Integer pageSize,String curPageNO);

	/**
	 * 根据id获取优惠券详情
	 * @param couponId
	 * @return
	 */
	public abstract AppCouponDto getCouponDto(Long couponId);

	public abstract AppPageSupport<AppProdDto> queryCouponProdList(Long couponId, String curPageNO, String orders);

	AppPageSupport<AppProdDto> queryRedpackProdList(Long couponId, String curPageNO, String orders);
}
