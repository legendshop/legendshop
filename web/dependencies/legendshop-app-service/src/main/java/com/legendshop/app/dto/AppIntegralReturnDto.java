package com.legendshop.app.dto;

import java.io.Serializable;

import com.legendshop.model.entity.UserAddress;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author quanzc
 * 去往兑换商品页面数据DTO
 */
@ApiModel(value="兑换商品DTO") 
public class AppIntegralReturnDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6200606356326886366L;
	
	/** 积分商品兑换对象 */
	@ApiModelProperty(value="积分商品兑换对象")  
	private AppSubmitIntegralProdDto integralProdDto;
	
	/** 买家默认地址对象 */
	@ApiModelProperty(value="买家默认地址对象")  
	private UserAddress defaultAddress;
	
	/** token */
	@ApiModelProperty(value="token,防止重复提交用")  
	private String token;
	
	/** 兑换数量 */
	@ApiModelProperty(value="兑换数量")  
	private Integer count;
	
	/** 总的兑换积分, 商品购买数量 * 商品单价 */
	@ApiModelProperty(value="总的兑换积分")  
	private Double totalExchangeIntegral;

	public UserAddress getDefaultAddress() {
		return defaultAddress;
	}

	public void setDefaultAddress(UserAddress defaultAddress) {
		this.defaultAddress = defaultAddress;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public AppSubmitIntegralProdDto getIntegralProdDto() {
		return integralProdDto;
	}

	public void setIntegralProdDto(AppSubmitIntegralProdDto integralProdDto) {
		this.integralProdDto = integralProdDto;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Double getTotalExchangeIntegral() {
		return totalExchangeIntegral;
	}

	public void setTotalExchangeIntegral(Double totalExchangeIntegral) {
		this.totalExchangeIntegral = totalExchangeIntegral;
	}
}
