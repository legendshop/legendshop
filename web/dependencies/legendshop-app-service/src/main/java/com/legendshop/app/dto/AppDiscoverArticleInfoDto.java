package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import java.util.List;

@ApiModel("发现文章详情Dto")
public class AppDiscoverArticleInfoDto {
	
	@ApiModelProperty(value="文章id")
	private Long id;
	
	@ApiModelProperty(value="文章标题")
	private String name;
	
	@ApiModelProperty(value="文章内容")
	private String content;

	@ApiModelProperty(value="文章作者")
	private String writerName;

	@ApiModelProperty(value="文章简介")
	private String intro;

	@ApiModelProperty(value="评论数量")
	private String comcount;
	
	@ApiModelProperty(value="关联商品数量")
	private String procount;

	@ApiModelProperty(value="浏览量")
	private Long pageView;

	@ApiModelProperty(value="发布时间")
	private Date publishTime;

	@ApiModelProperty(value="点赞数")
	private Long thumbNum;
	
	@ApiModelProperty(value="是否点赞")
	private boolean rsThumb;
	
	@ApiModelProperty(value="关联商品列表")
	private List<AppDiscoverProdDto> prods;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getThumbNum() {
		return thumbNum;
	}

	public void setThumbNum(Long thumbNum) {
		this.thumbNum = thumbNum;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getComcount() {
		return comcount;
	}

	public void setComcount(String comcount) {
		this.comcount = comcount;
	}

	public String getProcount() {
		return procount;
	}

	public void setProcount(String procount) {
		this.procount = procount;
	}

	public List<AppDiscoverProdDto> getProds() {
		return prods;
	}

	public void setProds(List<AppDiscoverProdDto> prods) {
		this.prods = prods;
	}

	public boolean isRsThumb() {
		return rsThumb;
	}

	public void setRsThumb(boolean rsThumb) {
		this.rsThumb = rsThumb;
	}

	public String getWriterName() {
		return writerName;
	}

	public void setWriterName(String writerName) {
		this.writerName = writerName;
	}

	public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public Long getPageView() {
		return pageView;
	}

	public void setPageView(Long pageView) {
		this.pageView = pageView;
	}

	public Date getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(Date publishTime) {
		this.publishTime = publishTime;
	}
}
