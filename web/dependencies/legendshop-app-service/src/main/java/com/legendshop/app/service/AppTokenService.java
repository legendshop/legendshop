/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service;

import com.legendshop.model.entity.AppToken;

/**
 * 用户Token.
 */
public interface AppTokenService  {

    public AppToken getAppTokenByUserId(String userId);

    public AppToken getAppToken(Long id);
    
    public void deleteAppToken(AppToken appToken);
    
    public Long saveAppToken(AppToken appToken);

    public void updateAppToken(AppToken appToken);

	public boolean deleteAppToken(String userId, String accessToken);
	
	public boolean deleteAppToken(String userId);

	/**
	 * 根据用户id和平台类型获取token
	 * @param userId    用户id
	 * @param platform  设备平台类型, H5: 手机网页, ANDROID: 安卓APP, IOS: 苹果APP, MP: 小程序
	 * @return
	 */
	public AppToken getAppToken(String userId, String platform,String type);
	
}
