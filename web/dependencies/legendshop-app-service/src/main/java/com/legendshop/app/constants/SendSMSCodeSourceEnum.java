package com.legendshop.app.constants;

import com.legendshop.util.constant.StringEnum;

/**
 * 发送短信验证码来源枚举
 */
public enum SendSMSCodeSourceEnum implements StringEnum{

	/** reg: 注册 */
	REG("reg"),
	
	/** 短信登录 */
	SMS_LOGIN("smsLogin"),
	
	/** 忘记密码 */
	FORGET("forget"),
	
	/** 修改手机号码 */
	UPDATE_PHONE("updatePhone"),
	
	/** 提现 */
	WITHDRAW("withdraw"),
	
	/** 修改登录密码 */
	UPDATE_PASSWD("updatePasswd"),
	
	/** 修改支付密码 */
	UPDATE_PAY_PASSWD("updatePayPasswd"),
	
	/** 绑定手机号 */
	BIND_PHONE("bindPhone"),
	
	/** 自己 */
	SELF("self"),
	
	/** 其他 */
	OTHER("other"),
	
	;
	
	private final String value;
	
	private SendSMSCodeSourceEnum(String value) {
		this.value = value;
	}

	@Override
	public String value() {
		return this.value;
	}
	
    public static Boolean inSendSMSCodeSourceEnum(String value) {
        for (SendSMSCodeSourceEnum sendSMSCodeSourceEnum : SendSMSCodeSourceEnum.values()) {
            if (sendSMSCodeSourceEnum.value().equals(value)) {
                return true;
            }
        }
        return false;
    }
}
