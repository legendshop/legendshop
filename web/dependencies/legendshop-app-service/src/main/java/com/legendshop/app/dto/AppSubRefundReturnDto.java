package com.legendshop.app.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 退款记录Dto
 */
@ApiModel(value="退款记录Dto")
public class AppSubRefundReturnDto implements Serializable {

	/**  */
	private static final long serialVersionUID = -2671569068015616997L;

	/** 退款记录ID */
	@ApiModelProperty(value="退款记录ID")
	private Long refundId; 
		
	/** 订单ID */
	@ApiModelProperty(value="订单ID")
	private Long subId; 
		
	/** 订单编号 */
	@ApiModelProperty(value="订单编号")
	private String subNumber; 
	
	/** 订单支付时间*/
	@ApiModelProperty(value="订单支付时间")
	private Date orderDatetime;
	
	/** 订单总金额 */
	@ApiModelProperty(value="订单总金额")
	private BigDecimal subMoney;
	
	/** 订单项金额 */
	@ApiModelProperty(value="订单项金额")
	private Double subItemMoney;
		
	/** 订单项ID */
	@ApiModelProperty(value="订单项ID")
	private Long subItemId; 
		
	/** 申请编号 */
	@ApiModelProperty(value="申请编号")
	private String refundSn; 
		
	/** 流水号(订单支付流水号) */
	@ApiModelProperty(value="流水号(订单支付流水号)")
	private String flowTradeNo; 
	
	/** ls_sub_settlement 订单支付-清算单据号 去第三方支付的订单单据号 */
	@ApiModelProperty(value="订单支付-清算单据号 去第三方支付的订单单据号")
	private String subSettlementSn;
	
	/** 第三方退款单号(微信退款单号) */
	@ApiModelProperty(value="第三方退款单号(微信退款单号)")
	private String outRefundNo;
		
	/** 订单支付Id */
	@ApiModelProperty(value="订单支付Id")
	private String payTypeId; 
	
	/** 订单支付Id */
	@ApiModelProperty(value="订单支付Id")
	private String payTypeName; 
		
	/** 店铺ID */
	@ApiModelProperty(value="店铺ID")
	private Long shopId; 
		
	/** 店铺名称 */
	@ApiModelProperty(value="店铺名称")
	private String shopName; 
		
	/** 买家ID */
	@ApiModelProperty(value="买家ID")
	private String userId; 
		
	/** 买家会员 */
	@ApiModelProperty(value="买家会员")
	private String userName; 
		
	/** 订单商品ID,全部退款是0,默认0 */
	@ApiModelProperty(value="订单商品ID,全部退款是0,默认0")
	private long productId; 
		
	/** 订单SKU ID,全部退款是0,默认0 */
	@ApiModelProperty(value="订单SKU ID,全部退款是0,默认0")
	private long skuId; 
		
	/** 商品名称 */
	@ApiModelProperty(value="商品名称")
	private String productName; 
		
	/** 商品数量 */
	@ApiModelProperty(value="商品数量")
	private Long goodsNum; 
		
	/** 退款金额 */
	@ApiModelProperty(value="退款金额")
	private java.math.BigDecimal refundAmount; 
	
	/** 商品图片 */
	@ApiModelProperty(value=" 商品图片")
	private String productImage; 
		
	/** 申请类型:1,仅退款,2退款退货,默认为1 */
	@ApiModelProperty(value="申请类型:1,仅退款,2退款退货,默认为1")
	private int applyType;
		
	/** 卖家处理状态:1为待审核,2为同意,3为不同意 */
	@ApiModelProperty(value="卖家处理状态:1为待审核,2为同意,3为不同意")
	private long sellerState; 
		
	/** 申请状态:1为处理中,2为待管理员处理,3为已完成*/
	@ApiModelProperty(value="申请状态:1为处理中,2为待管理员处理,3为已完成")
	private long applyState; 
		
	/** 退货类型:1为不用退货,2为需要退货,默认为1 */
	@ApiModelProperty(value=" 退货类型:1为不用退货,2为需要退货,默认为1")
	private long returnType; 
		
	/** 物流状态:1为待发货,2为待收货,3为未收到,4为已收货,默认为0 */
	@ApiModelProperty(value="物流状态:1为待发货,2为待收货,3为未收到,4为已收货,默认为0")
	private long goodsState; 
	
	/** 处理退款状态:[ 0:退款处理中 1:退款成功 -1:退款失败]*/
	@ApiModelProperty(value="处理退款状态:[ 0:退款处理中 1:退款成功 -1:退款失败]")
	private int isHandleSuccess;
	
	/** 处理退款的方式 : 线下处理 预存款处理 等*/
	@ApiModelProperty(value="处理退款的方式 : 线下处理 预存款处理 等")
	private String handleType; 
	
	/** 是否结算  */
	@ApiModelProperty(value="是否结算")
	private transient Boolean isBill;
	
	/** 订单结算编号[用于结算档期统计]  */
	@ApiModelProperty(value="订单结算编号[用于结算档期统计]")
	private  transient String billSn;
	
	/** 申请时间 */
	@ApiModelProperty(value="申请时间 ")
	private Date applyTime; 
		
	/** 卖家处理时间 */
	@ApiModelProperty(value="卖家处理时间")
	private Date sellerTime; 
		
	/** 管理员处理时间 */
	@ApiModelProperty(value="管理员处理时间")
	private Date adminTime; 
	
	/** 申请原因 */
	@ApiModelProperty(value="申请原因")
	private String buyerMessage; 
		
	/** 退款说明 */
	@ApiModelProperty(value="退款说明")
	private String reasonInfo; 
		
	/** 文件凭证1 */
	@ApiModelProperty(value="文件凭证1")
	private  String photoFile1; 
		
	/** 文件凭证2 */
	@ApiModelProperty(value="文件凭证2")
	private   String photoFile2; 
		
	/** 文件凭证3 */
	@ApiModelProperty(value="文件凭证3")
	private   String photoFile3; 
		
	/** 卖家备注 */
	@ApiModelProperty(value="卖家备注")
	private String sellerMessage; 
		
	/** 管理员备注 */
	@ApiModelProperty(value="管理员备注")
	private String adminMessage; 
		
	/** 物流公司名称 */
	@ApiModelProperty(value="物流公司名称")
	private String expressName; 
		
	/** 物流单号 */
	@ApiModelProperty(value=" 物流单号")
	private String expressNo; 
		
	/** 发货时间 */
	@ApiModelProperty(value="发货时间 ")
	private Date shipTime; 
		
	/** 收货时间 */
	@ApiModelProperty(value="收货时间")
	private Date receiveTime; 
		
	/** 收货备注 */
	@ApiModelProperty(value="收货备注")
	private String receiveMessage; 
	
	/** 第三方已退款金额 */
	@ApiModelProperty(value="第三方已退款金额")
	private BigDecimal thirdPartyRefund;
	
	/** 平台已退款金额（金币或预存款） */
	@ApiModelProperty(value="平台已退款金额（金币或预存款）")
	private BigDecimal platformRefund;
	
	/**是否预售*/
	@ApiModelProperty(value="是否预售的退款单，默认为false")
	private Boolean isPresell;
	
	/**是否退订金*/
	@ApiModelProperty(value="是否退订金，默认为false")
	private Boolean isRefundDeposit;
	
	/**订金退款状态  RefundReturnStatusEnum*/
	@ApiModelProperty(value="订金退款状态，0为不用处理， 1为退款退货中， 2退款成功， -2为退款失败；需要退订金，默认为1，不需要退订金，默认为0")
	private int isDepositHandleSucces;
	
	/**订金处理退款方式*/
	@ApiModelProperty(value="订金处理退款方式")
	private String depositHandleType;
	
	/**订金退款金额*/
	@ApiModelProperty(value="订金退款金额，默认为0")
	private BigDecimal depositRefund;
	
	/**订金支付流水号*/
	@ApiModelProperty(value="订金支付流水号")
	private String depositFlowTradeNo;
	
	/**订金第三方退款单号(微信退款单号)*/
	@ApiModelProperty(value="订金第三方退款单号(微信退款单号)")
	private String depositOutRefundNo;
	
	/**订金第三方退款单号(微信退款单号)*/
	@ApiModelProperty(value="订金支付ID")
	private String depositPayTypeId;
	
	/**订金支付名称*/
	@ApiModelProperty(value="订金支付名称")
	private String depositPayTypeName;
	
	/** ls_sub_settlement 预售订单订金支付-清算单据号 去第三方支付的订单单据号 */
	@ApiModelProperty(value="预售订单订金支付-清算单据号 去第三方支付的订单单据号")
	private String depositSubSettlementSn; 
	
	/** 订金支付时间*/
	@ApiModelProperty(value="订金支付时间")
	private Date depositOrderDatetime;
	
	/**退款单创建来源  RefundSouceEnum */
	@ApiModelProperty(value="退款单创建来源，0为用户 ，1为商家，2为平台")
	private int refundSouce;
	
	/** 订单商品数量  */
	@ApiModelProperty(value="订单商品数量")
	private Long productNum;
	
	/** 订单商品规格属性  */
	@ApiModelProperty(value="订单商品规格属性 ")
	private String productAttribute;
	
	/** 待商家审核的倒计时  */
	@ApiModelProperty(value="待商家审核的倒计时 ")
	private Date sellerAuditCountDownTime;
	
	/**退换货有效时间倒计时*/
	@ApiModelProperty(value="退换货有效时间倒计时 ")
	private Date productReturnCountDownTime;

	/** 详细退货地址 **/
	@ApiModelProperty(value="详细退货地址")
	private String returnDetailAddress;

	/**
	 * 退货联系人
	 */
	@ApiModelProperty(value="退货联系人")
	private String returnContact;

	/**
	 * 退货联系电话
	 */
	@ApiModelProperty(value="退货联系电话")
	private String returnPhone;

	/**运费*/
	@ApiModelProperty(value="运费 ")
	private BigDecimal returnFreightAmount;



	public Long getRefundId() {
		return refundId;
	}

	public void setRefundId(Long refundId) {
		this.refundId = refundId;
	}

	public Long getSubId() {
		return subId;
	}

	public void setSubId(Long subId) {
		this.subId = subId;
	}

	public String getSubNumber() {
		return subNumber;
	}

	public void setSubNumber(String subNumber) {
		this.subNumber = subNumber;
	}

	public Date getOrderDatetime() {
		return orderDatetime;
	}

	public void setOrderDatetime(Date orderDatetime) {
		this.orderDatetime = orderDatetime;
	}

	public BigDecimal getSubMoney() {
		return subMoney;
	}

	public void setSubMoney(BigDecimal subMoney) {
		this.subMoney = subMoney;
	}

	public Long getSubItemId() {
		return subItemId;
	}

	public void setSubItemId(Long subItemId) {
		this.subItemId = subItemId;
	}

	public String getRefundSn() {
		return refundSn;
	}

	public void setRefundSn(String refundSn) {
		this.refundSn = refundSn;
	}

	public String getFlowTradeNo() {
		return flowTradeNo;
	}

	public void setFlowTradeNo(String flowTradeNo) {
		this.flowTradeNo = flowTradeNo;
	}

	public String getSubSettlementSn() {
		return subSettlementSn;
	}

	public void setSubSettlementSn(String subSettlementSn) {
		this.subSettlementSn = subSettlementSn;
	}

	public String getOutRefundNo() {
		return outRefundNo;
	}

	public void setOutRefundNo(String outRefundNo) {
		this.outRefundNo = outRefundNo;
	}

	public String getPayTypeId() {
		return payTypeId;
	}

	public void setPayTypeId(String payTypeId) {
		this.payTypeId = payTypeId;
	}

	public String getPayTypeName() {
		return payTypeName;
	}

	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getSkuId() {
		return skuId;
	}

	public void setSkuId(long skuId) {
		this.skuId = skuId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Long getGoodsNum() {
		return goodsNum;
	}

	public void setGoodsNum(Long goodsNum) {
		this.goodsNum = goodsNum;
	}

	public java.math.BigDecimal getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(java.math.BigDecimal refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	public int getApplyType() {
		return applyType;
	}

	public void setApplyType(int applyType) {
		this.applyType = applyType;
	}

	public long getSellerState() {
		return sellerState;
	}

	public void setSellerState(long sellerState) {
		this.sellerState = sellerState;
	}

	public long getApplyState() {
		return applyState;
	}

	public void setApplyState(long applyState) {
		this.applyState = applyState;
	}

	public long getReturnType() {
		return returnType;
	}

	public void setReturnType(long returnType) {
		this.returnType = returnType;
	}

	public long getGoodsState() {
		return goodsState;
	}

	public void setGoodsState(long goodsState) {
		this.goodsState = goodsState;
	}

	public int getIsHandleSuccess() {
		return isHandleSuccess;
	}

	public void setIsHandleSuccess(int isHandleSuccess) {
		this.isHandleSuccess = isHandleSuccess;
	}

	public String getHandleType() {
		return handleType;
	}

	public void setHandleType(String handleType) {
		this.handleType = handleType;
	}

	public Boolean getIsBill() {
		return isBill;
	}

	public void setIsBill(Boolean isBill) {
		this.isBill = isBill;
	}

	public String getBillSn() {
		return billSn;
	}

	public void setBillSn(String billSn) {
		this.billSn = billSn;
	}

	public Date getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(Date applyTime) {
		this.applyTime = applyTime;
	}

	public Date getSellerTime() {
		return sellerTime;
	}

	public void setSellerTime(Date sellerTime) {
		this.sellerTime = sellerTime;
	}

	public Date getAdminTime() {
		return adminTime;
	}

	public void setAdminTime(Date adminTime) {
		this.adminTime = adminTime;
	}

	public String getBuyerMessage() {
		return buyerMessage;
	}

	public void setBuyerMessage(String buyerMessage) {
		this.buyerMessage = buyerMessage;
	}

	public String getReasonInfo() {
		return reasonInfo;
	}

	public void setReasonInfo(String reasonInfo) {
		this.reasonInfo = reasonInfo;
	}

	public String getPhotoFile1() {
		return photoFile1;
	}

	public void setPhotoFile1(String photoFile1) {
		this.photoFile1 = photoFile1;
	}

	public String getPhotoFile2() {
		return photoFile2;
	}

	public void setPhotoFile2(String photoFile2) {
		this.photoFile2 = photoFile2;
	}

	public String getPhotoFile3() {
		return photoFile3;
	}

	public void setPhotoFile3(String photoFile3) {
		this.photoFile3 = photoFile3;
	}

	public String getSellerMessage() {
		return sellerMessage;
	}

	public void setSellerMessage(String sellerMessage) {
		this.sellerMessage = sellerMessage;
	}

	public String getAdminMessage() {
		return adminMessage;
	}

	public void setAdminMessage(String adminMessage) {
		this.adminMessage = adminMessage;
	}

	public String getExpressName() {
		return expressName;
	}

	public void setExpressName(String expressName) {
		this.expressName = expressName;
	}

	public String getExpressNo() {
		return expressNo;
	}

	public void setExpressNo(String expressNo) {
		this.expressNo = expressNo;
	}

	public Date getShipTime() {
		return shipTime;
	}

	public void setShipTime(Date shipTime) {
		this.shipTime = shipTime;
	}

	public Date getReceiveTime() {
		return receiveTime;
	}

	public void setReceiveTime(Date receiveTime) {
		this.receiveTime = receiveTime;
	}

	public String getReceiveMessage() {
		return receiveMessage;
	}

	public void setReceiveMessage(String receiveMessage) {
		this.receiveMessage = receiveMessage;
	}

	public BigDecimal getThirdPartyRefund() {
		return thirdPartyRefund;
	}

	public void setThirdPartyRefund(BigDecimal thirdPartyRefund) {
		this.thirdPartyRefund = thirdPartyRefund;
	}

	public BigDecimal getPlatformRefund() {
		return platformRefund;
	}

	public void setPlatformRefund(BigDecimal platformRefund) {
		this.platformRefund = platformRefund;
	}

	public Boolean getIsPresell() {
		return isPresell;
	}

	public void setIsPresell(Boolean isPresell) {
		this.isPresell = isPresell;
	}

	public Boolean getIsRefundDeposit() {
		return isRefundDeposit;
	}

	public void setIsRefundDeposit(Boolean isRefundDeposit) {
		this.isRefundDeposit = isRefundDeposit;
	}

	public int getIsDepositHandleSucces() {
		return isDepositHandleSucces;
	}

	public void setIsDepositHandleSucces(int isDepositHandleSucces) {
		this.isDepositHandleSucces = isDepositHandleSucces;
	}

	public String getDepositHandleType() {
		return depositHandleType;
	}

	public void setDepositHandleType(String depositHandleType) {
		this.depositHandleType = depositHandleType;
	}

	public BigDecimal getDepositRefund() {
		return depositRefund;
	}

	public void setDepositRefund(BigDecimal depositRefund) {
		this.depositRefund = depositRefund;
	}

	public String getDepositFlowTradeNo() {
		return depositFlowTradeNo;
	}

	public void setDepositFlowTradeNo(String depositFlowTradeNo) {
		this.depositFlowTradeNo = depositFlowTradeNo;
	}

	public String getDepositOutRefundNo() {
		return depositOutRefundNo;
	}

	public void setDepositOutRefundNo(String depositOutRefundNo) {
		this.depositOutRefundNo = depositOutRefundNo;
	}

	public String getDepositPayTypeId() {
		return depositPayTypeId;
	}

	public void setDepositPayTypeId(String depositPayTypeId) {
		this.depositPayTypeId = depositPayTypeId;
	}

	public String getDepositPayTypeName() {
		return depositPayTypeName;
	}

	public void setDepositPayTypeName(String depositPayTypeName) {
		this.depositPayTypeName = depositPayTypeName;
	}

	public String getDepositSubSettlementSn() {
		return depositSubSettlementSn;
	}

	public void setDepositSubSettlementSn(String depositSubSettlementSn) {
		this.depositSubSettlementSn = depositSubSettlementSn;
	}

	public Date getDepositOrderDatetime() {
		return depositOrderDatetime;
	}

	public void setDepositOrderDatetime(Date depositOrderDatetime) {
		this.depositOrderDatetime = depositOrderDatetime;
	}

	public int getRefundSouce() {
		return refundSouce;
	}

	public void setRefundSouce(int refundSouce) {
		this.refundSouce = refundSouce;
	}

	public Long getProductNum() {
		return productNum;
	}

	public void setProductNum(Long productNum) {
		this.productNum = productNum;
	}

	public String getProductAttribute() {
		return productAttribute;
	}

	public void setProductAttribute(String productAttribute) {
		this.productAttribute = productAttribute;
	}

	public Date getSellerAuditCountDownTime() {
		return sellerAuditCountDownTime;
	}

	public void setSellerAuditCountDownTime(Date sellerAuditCountDownTime) {
		this.sellerAuditCountDownTime = sellerAuditCountDownTime;
	}

	public Date getProductReturnCountDownTime() {
		return productReturnCountDownTime;
	}

	public void setProductReturnCountDownTime(Date productReturnCountDownTime) {
		this.productReturnCountDownTime = productReturnCountDownTime;
	}

	public Double getSubItemMoney() {
		return subItemMoney;
	}

	public void setSubItemMoney(Double subItemMoney) {
		this.subItemMoney = subItemMoney;
	}


	public String getReturnDetailAddress() {
		return returnDetailAddress;
	}

	public void setReturnDetailAddress(String returnDetailAddress) {
		this.returnDetailAddress = returnDetailAddress;
	}

	public String getReturnContact() {
		return returnContact;
	}

	public void setReturnContact(String returnContact) {
		this.returnContact = returnContact;
	}

	public String getReturnPhone() {
		return returnPhone;
	}

	public void setReturnPhone(String returnPhone) {
		this.returnPhone = returnPhone;
	}

	public BigDecimal getReturnFreightAmount() {
		return returnFreightAmount;
	}

	public void setReturnFreightAmount(BigDecimal returnFreightAmount) {
		this.returnFreightAmount = returnFreightAmount;
	}
}
