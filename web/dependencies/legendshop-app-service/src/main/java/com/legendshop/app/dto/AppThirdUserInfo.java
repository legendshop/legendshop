package com.legendshop.app.dto;

import com.legendshop.model.dto.ThirdUserInfo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 第三方用户信息, 包含oauth2.0的授权token信息, 用于原生APP的第三方登录
 * @author 开发很忙
 */
@ApiModel(value="第三方用户信息, 包含oauth2.0的授权token信息")
public class AppThirdUserInfo {
	
	/** 用户的第三方授权凭证 */
	@ApiModelProperty(value="用户的第三方授权凭证")
	private String accessToken;
	
	/** access_token接口调用凭证超时时间，单位（秒） */
	@ApiModelProperty(value="access_token接口调用凭证超时时间，单位（秒）")
	private int expiresIn;
	
	/** 用于刷新access_token */
	@ApiModelProperty(value="用于刷新access_token")
	private String refreshToken;
	
	/** 用户授权的作用域，使用逗号（,）分隔, 目前只有微信才有 */
	@ApiModelProperty(value="access_token接口调用凭证超时时间，单位（秒）")
	private String scope;
	
	/** 用户在第三方应用下的唯一用户标识, 其中微信和QQ, 对于用户在不同的APPID有不同的openID; 微博是uid, 多应用是唯一的 */
	@ApiModelProperty(value="户在第三方应用下的唯一用户标识, 其中微信和QQ, 对于用户在不同的APPID有不同的openID; 微博是uid, 多应用是唯一的 ", required = true)
	private String openId;
	
	/** 开放平台统一账号下的多个应用的统一标识, 目前微信和QQ有这个, 微博没有 */
	@ApiModelProperty(value="开放平台统一账号下的多个应用的统一标识, 目前微信和QQ有这个, 微博没有 ")
	private String unionid;
	
	/** 用户的昵称 */
	@ApiModelProperty(value="用户的昵称 ")
	private String nickName;
	
	/** 性别 */
	@ApiModelProperty(value="性别 ")
	private String gender;
	
	/** 用户所在城市 */
	@ApiModelProperty(value="用户所在城市")
	private String city;
	
	/** 用户所在国家 */
	@ApiModelProperty(value="用户所在国家")
	private String country;
	
	/** 用户所在省份 */
	@ApiModelProperty(value="用户所在省份")
	private String province;
	
	/** 用户的语言zh_CN */
	@ApiModelProperty(value="用户的语言zh_CN")
	private String language;
	
	/** 用户头像 */
	@ApiModelProperty(value="用户头像")
	private String avatarUrl;
	
	public AppThirdUserInfo() {
		super();
	}
	
	public int getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(int expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public ThirdUserInfo convert2ThirdUserInfo(){
		
		ThirdUserInfo userInfo = new ThirdUserInfo();
		userInfo.setAccessToken(this.getAccessToken());
		userInfo.setExpiresIn(this.getExpiresIn());
		userInfo.setRefreshToken(this.getRefreshToken());
		userInfo.setScope(this.getScope());
		
		userInfo.setOpenId(this.getOpenId());
		userInfo.setUnionid(this.getUnionid());
		userInfo.setNickName(this.getNickName());
		userInfo.setGender(this.getGender());
		userInfo.setCountry(this.getCountry());
		userInfo.setProvince(this.getProvince());
		userInfo.setCity(this.getCity());
		userInfo.setAvatarUrl(this.getAvatarUrl());
		
		return userInfo;
	}
}
