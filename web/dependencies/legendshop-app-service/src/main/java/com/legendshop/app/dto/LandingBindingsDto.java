package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 登陆绑定
 * */

@ApiModel("第三方登陆绑定Dto")
public class LandingBindingsDto {

	/** 平台类型 */
	@ApiModelProperty(value = "平台类型")
	private String type;
	
	/** 版本id */
	@ApiModelProperty(value = "版本id")
	private String verId;
	
	/** openid */
	@ApiModelProperty(value = "openid")
	private String openId;
	
	/** 手机号码 */
	@ApiModelProperty(value = "手机号码")
	private String mobile;
	
	/** 设备id */
	@ApiModelProperty(value = "设备id")
	private String deviceId;
	
	/** 设备平台类型, H5: 手机网页, ANDROID: 安卓APP, IOS: 苹果APP, MP: 小程序 */
	@ApiModelProperty(value = "设备平台类型, H5: 手机网页, ANDROID: 安卓APP, IOS: 苹果APP, MP: 小程序")
	private String platform;
	
	/** 昵称(qq、微信、微博名称) */
	@ApiModelProperty(value = "昵称(qq、微信、微博名称)")
	private String nickName;
	
	/** 密码 */
	@ApiModelProperty(value = "密码")
	private String password;
	
	/** 头像 */
	@ApiModelProperty(value = "头像")
	private String headimgurl;
	
	/** accessToken */
	@ApiModelProperty(value = "accessToken")
	private String accessToken;
	
	/** 验证码 */
	@ApiModelProperty(value = "验证码")
	private String validateCode;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getValidateCode() {
		return validateCode;
	}

	public void setValidateCode(String validateCode) {
		this.validateCode = validateCode;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getVerId() {
		return verId;
	}

	public void setVerId(String verId) {
		this.verId = verId;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getHeadimgurl() {
		return headimgurl;
	}

	public void setHeadimgurl(String headimgurl) {
		this.headimgurl = headimgurl;
	}
	
}
