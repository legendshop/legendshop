package com.legendshop.app.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.legendshop.model.dto.app.AppStoreDto;
import com.legendshop.model.dto.app.cartorder.ShopCartOrderDto;
import com.legendshop.model.entity.Invoice;
import com.legendshop.model.entity.UserAddress;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * APP 用户的购物车订单信息
 */
@ApiModel(value="UserShopCartOrderDto用户的购物车订单信息") 
public class AppUserShopCartOrderDto implements Serializable{

	private static final long serialVersionUID = 790433533026959806L;

	@ApiModelProperty(value="用户Id") 
	private  String userId;
	
	@ApiModelProperty(value="用户名") 
	private String userName;
	
	@ApiModelProperty(value="订单类型 [拍卖、普通、秒杀、门店订单]") 
	private String type; 
	
	@ApiModelProperty(value = "卖家是否开启发票功能, 0:代表关闭, 1: 代表关闭")
	private Integer switchInvoice;
	
	@ApiModelProperty(value="发票ID") 
	private Long invoiceId; 
	
	@ApiModelProperty(value="默认发票") 
	private Invoice defaultInvoice; 
	
	@ApiModelProperty(value="支付方式") 
	private Integer payManner;  
	
	@ApiModelProperty(value="预付款支付金额") 
	private Double pdAmount=0d; 
	
	@ApiModelProperty(value="订单商品总数量") 
    private int orderTotalQuanlity;
    
	@ApiModelProperty(value="订单商品总重量") 
    private double orderTotalWeight;
    
	@ApiModelProperty(value="订单商品总体积") 
    private double orderTotalVolume;
    
	@ApiModelProperty(value="商品总价") 
    private double orderTotalCash; 
    
	@ApiModelProperty(value="实际总值   商品总价+运费-优惠 -优惠券") 
    private double orderActualTotal; 
    
	@ApiModelProperty(value="订单运费") 
    private double orderFreightAmount;
    
	@ApiModelProperty(value="订单优惠总价") 
    private double allDiscount;
    
	@ApiModelProperty(value="是否支持货到付款") 
    private Boolean isCod; 
	
	@ApiModelProperty(value="用户地址") 
	private UserAddress userAddress;  
	
	@ApiModelProperty(value="订单是按店铺区分， 对应的店铺的购物车信息") 
    private List<ShopCartOrderDto> shopCarts = new ArrayList<ShopCartOrderDto>(); 
	
	@ApiModelProperty(value="总的 优惠券 优惠金额") 
	private Double couponOffPrice;
	
	@ApiModelProperty(value="orderToken") 
	private String orderToken;
	
	@ApiModelProperty(value="活动Id [秒杀活动ID/团购活动id]") 
	private Long activeId;
	
	@ApiModelProperty(value="团编号, 当是拼团订单时才有") 
	private String addNumber;
	
	/** token， 防止重复提交. */
	@ApiModelProperty(value = "token， 防止重复提交")
	private String token;
	
	@ApiModelProperty(value="门店信息 (改造已去掉)")
	private AppStoreDto appStoreDto;
	
	@ApiModelProperty(value="预售商品单价") 
	private Double preProdCash;
	
	@ApiModelProperty(value="预售订单支付方式,0:全额,1:定金") 
	private Long payPctType;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Integer getSwitchInvoice() {
		return switchInvoice;
	}

	public void setSwitchInvoice(Integer switchInvoice) {
		this.switchInvoice = switchInvoice;
	}

	public Long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Invoice getDefaultInvoice() {
		return defaultInvoice;
	}

	public void setDefaultInvoice(Invoice defaultInvoice) {
		this.defaultInvoice = defaultInvoice;
	}

	public Integer getPayManner() {
		return payManner;
	}

	public void setPayManner(Integer payManner) {
		this.payManner = payManner;
	}

	public Double getPdAmount() {
		return pdAmount;
	}

	public void setPdAmount(Double pdAmount) {
		this.pdAmount = pdAmount;
	}

	public int getOrderTotalQuanlity() {
		return orderTotalQuanlity;
	}

	public void setOrderTotalQuanlity(int orderTotalQuanlity) {
		this.orderTotalQuanlity = orderTotalQuanlity;
	}

	public double getOrderTotalWeight() {
		return orderTotalWeight;
	}

	public void setOrderTotalWeight(double orderTotalWeight) {
		this.orderTotalWeight = orderTotalWeight;
	}

	public double getOrderTotalVolume() {
		return orderTotalVolume;
	}

	public void setOrderTotalVolume(double orderTotalVolume) {
		this.orderTotalVolume = orderTotalVolume;
	}

	public double getOrderTotalCash() {
		return orderTotalCash;
	}

	public void setOrderTotalCash(double orderTotalCash) {
		this.orderTotalCash = orderTotalCash;
	}

	public double getOrderActualTotal() {
		return orderActualTotal;
	}

	public void setOrderActualTotal(double orderActualTotal) {
		this.orderActualTotal = orderActualTotal;
	}

	public double getOrderFreightAmount() {
		return orderFreightAmount;
	}

	public void setOrderFreightAmount(double orderFreightAmount) {
		this.orderFreightAmount = orderFreightAmount;
	}

	public double getAllDiscount() {
		return allDiscount;
	}

	public void setAllDiscount(double allDiscount) {
		this.allDiscount = allDiscount;
	}

	public Boolean getIsCod() {
		return isCod;
	}

	public void setIsCod(Boolean isCod) {
		this.isCod = isCod;
	}

	public UserAddress getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(UserAddress userAddress) {
		this.userAddress = userAddress;
	}

	public List<ShopCartOrderDto> getShopCarts() {
		return shopCarts;
	}

	public void setShopCarts(List<ShopCartOrderDto> shopCarts) {
		this.shopCarts = shopCarts;
	}

	public Double getCouponOffPrice() {
		return couponOffPrice;
	}

	public void setCouponOffPrice(Double couponOffPrice) {
		this.couponOffPrice = couponOffPrice;
	}

	public String getOrderToken() {
		return orderToken;
	}

	public void setOrderToken(String orderToken) {
		this.orderToken = orderToken;
	}

	public Long getActiveId() {
		return activeId;
	}

	public void setActiveId(Long activeId) {
		this.activeId = activeId;
	}
	
	public String getAddNumber() {
		return addNumber;
	}

	public void setAddNumber(String addNumber) {
		this.addNumber = addNumber;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public AppStoreDto getAppStoreDto() {
		return appStoreDto;
	}

	public void setAppStoreDto(AppStoreDto appStoreDto) {
		this.appStoreDto = appStoreDto;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Double getPreProdCash() {
		return preProdCash;
	}

	public void setPreProdCash(Double preProdCash) {
		this.preProdCash = preProdCash;
	}

	public Long getPayPctType() {
		return payPctType;
	}

	public void setPayPctType(Long payPctType) {
		this.payPctType = payPctType;
	}
	
}
