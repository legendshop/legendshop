package com.legendshop.app.dto;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

/**
 * app 专题列表Dto
 * @author linzh
 * @date 2019年3月1日
 */
public class AppThemeListDto {

	/** 专题id */
	@ApiModelProperty(value="专题id")
	private Long themeId; 
	
	/** 标题 */
	@ApiModelProperty(value="标题")
	private String title; 
	
	/** 标题颜色 */
	@ApiModelProperty(value="标题颜色")
	private String titleColor; 
	
	/** 结束时间 */
	@ApiModelProperty(value="结束时间")
	private Date endTime;
	
	/**专题手机活动图(列表显示图)*/
	@ApiModelProperty(value="专题手机活动图(列表显示图)")
	private String themeMobileImg;
	
	/** 手机端简介 */
	@ApiModelProperty(value="手机端简介")
	private String introMobile; 
	
	/** 手机简介字体颜色 */
	@ApiModelProperty(value="手机简介字体颜色")
	private String introMColor; 
		
	/** 手机简介是否在横幅区显示 */
	@ApiModelProperty(value="手机简介是否在横幅区显示 1为显示 0 为不显示")
	private Long isIntroMShow; 

	public Long getThemeId() {
		return themeId;
	}

	public void setThemeId(Long themeId) {
		this.themeId = themeId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getThemeMobileImg() {
		return themeMobileImg;
	}

	public void setThemeMobileImg(String themeMobileImg) {
		this.themeMobileImg = themeMobileImg;
	}

	public String getTitleColor() {
		return titleColor;
	}

	public void setTitleColor(String titleColor) {
		this.titleColor = titleColor;
	}

	public String getIntroMobile() {
		return introMobile;
	}

	public void setIntroMobile(String introMobile) {
		this.introMobile = introMobile;
	}

	public String getIntroMColor() {
		return introMColor;
	}

	public void setIntroMColor(String introMColor) {
		this.introMColor = introMColor;
	}

	public Long getIsIntroMShow() {
		return isIntroMShow;
	}

	public void setIsIntroMShow(Long isIntroMShow) {
		this.isIntroMShow = isIntroMShow;
	}
	
}


