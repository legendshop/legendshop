package com.legendshop.app.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.AppVisitHistoryDto;
import com.legendshop.app.service.AppVisitHistoryService;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.spi.service.LoginedUserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags="我的足迹",value="我的足迹相关接口")
public class AppVisitHistoryController {
	
	private final Logger LOGGER = LoggerFactory.getLogger(AppVisitHistoryController.class);
	
	@Autowired
	private AppVisitHistoryService appVisitHistoryService;
	
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;
	
	/**
	 * 我的足迹列表
	 * @param curPageNO
	 * @return
	 */
	@ApiOperation(value = "我的足迹列表", httpMethod = "POST", notes = "我的足迹列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = false, dataType = "string")
	@PostMapping(value="/p/my/visitHistory")
	public ResultDto<AppPageSupport<AppVisitHistoryDto>> visitHistory(String curPageNO){
		
		try {
			String userName = loginedUserService.getUser().getUserName();
			AppPageSupport<AppVisitHistoryDto> result = appVisitHistoryService.queryVisitHistory(curPageNO, userName);
			
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("查询我的足迹异常", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 清空足迹
	 * @return
	 */
	@ApiOperation(value = "清空足迹", httpMethod = "POST", notes = "清空足迹, result 返回true代表清空成功, false代表失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/p/my/clearVisitHistory")
	public ResultDto<Object> clearVisitHistory(){
		
		try {
			String userName = loginedUserService.getUser().getUserName();
			boolean result = appVisitHistoryService.clearVisitHistory(userName);
			return ResultDtoManager.success(result);
		} catch (Exception e) {
			LOGGER.error("清空足迹异常", e);
			return ResultDtoManager.fail();
		}
	}
}
