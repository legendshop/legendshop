/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.model.dto.app.AppStartAdvDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.spi.service.AppStartAdvService;
import com.legendshop.util.AppUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * app启动广告接口
 */
@Api(tags="启动广告",value="启动广告列表")
@RestController
public class AppStartAdvController {

    @Autowired
    private AppStartAdvService appStartAdvService;

    /**
     * 获取APP启动广告列表
     * @param version
     * @return
     */
	@ApiOperation(value = "获取启动广告列表", httpMethod = "POST", notes = "获取启动广告列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/appStartAdv")
    public ResultDto<AppStartAdvDto> query() {
		
		AppStartAdvDto result = null;

		try {
	        List<AppStartAdvDto> appStartAdvList = appStartAdvService.getOnlines();
	        if(AppUtils.isNotBlank(appStartAdvList)){
	            Random random = new Random();
	            int index = random.nextInt(appStartAdvList.size());
	            result = appStartAdvList.get(index);
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        return ResultDtoManager.success(result);
    }

}
