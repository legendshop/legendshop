package com.legendshop.app.service;

import com.legendshop.app.dto.AppNewSystemMsgDto;
import com.legendshop.app.dto.AppSiteInformationDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.entity.UserDetail;

public interface AppMessageService {

	/**
	 * 获取最新系统通知
	 * @param userName
	 * @param gradeId
	 * @return
	 */
	public AppNewSystemMsgDto getNewSystemMessages(String userName, Integer gradeId);

	/**
	 * 获取系统消息DTO
	 */
	public AppPageSupport<AppSiteInformationDto> querySystemMessages(String curPageNO, Integer gradeId, String userName, UserDetail userDetail);

	/**
	 * 获取站内信
	 */
	public AppPageSupport<AppSiteInformationDto> queryInboxMsg(String curPageNO, String userName);

	/**
	 * 获取用户普通消息详情
	 * @param msgId 消息Id
	 * @param userName 当前登录用户名
	 * @return
	 */
	public AppSiteInformationDto getMsgDetail(Long msgId, String userName);

	/**
	 * 获取用户系统通知消息详情
	 * @param msgId 消息Id
	 * @param userName 当前登录用户名
	 * @return
	 */
	public AppSiteInformationDto getSystemMsgDetail(Long msgId, String userName);
	
}
