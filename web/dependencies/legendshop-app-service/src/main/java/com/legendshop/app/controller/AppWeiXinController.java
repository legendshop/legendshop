/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.legendshop.app.util.WeiXinOAuth2Helper;
import com.legendshop.base.util.WxSignUtil;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.dto.weixin.JsApiInfo;
import com.legendshop.spi.service.PassportService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.HttpUtil;
import com.legendshop.util.JSONUtil;

import springfox.documentation.annotations.ApiIgnore;

/**
 * 微信网页授权回调, 已经写到AppThirdLoginController了
 */
@Deprecated
@Controller
@RequestMapping("/weixin")
public class AppWeiXinController {

	private final static Logger LOGGER = LoggerFactory.getLogger(AppWeiXinController.class);

	@Autowired
	protected PassportService passportService;

	@Autowired
	private PropertiesUtil propertiesUtil;
	
    @Autowired
    private WeiXinOAuth2Helper weiXinOAuth2Helper;
	
	/**
	 * 跳转微信oauth2.0认证的链接
	 * @param refrenceURL 认证来源URL, 用于认证的最终完成之后需要重定向到这个地址.
	 * @return
	 */
    @ApiIgnore
    @GetMapping("/oauth2")
	public void oauth2(HttpServletResponse response, @RequestParam String refrenceURL) {
		LOGGER.info("###################### refrenceURL  {} #######################", refrenceURL);
		
		//String vueDomainName = propertiesUtil.getVueDomainName();
	 			 
        //构建Oauth2.0url
        JSONObject jsonObject = weiXinOAuth2Helper.oauth2(refrenceURL);
        LOGGER.info("###################### {} #######################", jsonObject.toString());
        
       boolean status =  jsonObject.getBoolean("status");
       if(!status){
    	  return;
       }
       
       String oauth2_authorize_url =  jsonObject.getString("oauth2_authorize_url");
        
        try {
			response.sendRedirect(oauth2_authorize_url);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 在微信服务(weixin_server)完成认证之后接收callback, 然后重定向到vue页面, 并带上openId和unionid
	 * @param openId 认证完成之后拿到的微信用户ID
	 * @param unionid 认证完成之后拿到的微信用户unionid
	 * @param oauth2url 
	 * @param time 
	 * @param validateCode
	 * @param refrenceURL 认证来源URL, 用于认证的最终完成之后需要重定向到这个地址.
	 * @return
	 */
	@ApiIgnore
	@GetMapping("/oauth2/callback")
	public String callback(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam String openId, String unionid, 
			@RequestParam String oauth2url, 
			@RequestParam String time, @RequestParam String validateCode, 
			@RequestParam String refrenceURL) {

		LOGGER.info("=================微信网站网页授权回调========================");
		
		String vueDomainName = propertiesUtil.getVueDomainName();

		if (AppUtils.isBlank(openId)) {
			LOGGER.warn("parameter openId  missing");
			return "redirect:" + refrenceURL;
		}

		if (!checkSignature(oauth2url, time, validateCode)) {
			LOGGER.warn("=================微信网站网页授权回调失败,密钥校验不通过!========================");
			return "redirect:" + refrenceURL;
		}
		
		LOGGER.info("=================微信网站网页授权回调成功========================");
		
		return "redirect:" +  vueDomainName + "/weixinOAuth2?openId=" + openId + "&unionid=" + unionid + "&refrenceURL=" + refrenceURL;
	}

	/**
	 * 检查调用者的有效性
	 * @param oauth2url
	 * @param time
	 * @param validateCode
	 * @return
	 */
	private boolean checkSignature(String oauth2url, String time, String validateCode) {
		return WxSignUtil.checkSignature(oauth2url, validateCode, time, propertiesUtil.getWeiXinKey());
	}

	/**
	 * 用于调用微信的JSAPI的, 目前vue暂未用到
	 * @param request
	 * @param response
	 * @return
	 */
	@ApiIgnore
	@RequestMapping(value = "/weixinapi", method = RequestMethod.POST)
	@ResponseBody
	public  JsApiInfo weixinapi(HttpServletRequest request, HttpServletResponse response) {
		String WEIXIN_API_ADDR = propertiesUtil.getWeiXinDomainName() + "/getJsApiInfo";
		Map<String, String> paramMap = new HashMap<String, String>();
		String url = request.getParameter("url");
		paramMap.put("url", url);
		String result = HttpUtil.httpPost(WEIXIN_API_ADDR, paramMap);
		try {
			if (result != null) {
				JsApiInfo jsApiInfo = JSONUtil.getObject(result, JsApiInfo.class);
				return jsApiInfo;
			}
		} catch (Exception e) {
			LOGGER.error("Call URL {} with error {} ", url, e.getLocalizedMessage());
		}

		return null;
	}
}
