package com.legendshop.app.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.AppHeadlineDto;
import com.legendshop.app.service.AppHeadlineService;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.util.AppUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

/**
 * 头条文章相关接口
 */
@RestController
@Api(tags="头条新闻",value="头条新闻")
public class AppHeadlineController {

	private static Logger LOGGER = LoggerFactory.getLogger(AppHeadlineController.class);
	
	@Autowired
	private AppHeadlineService appHeadlineService;

	/**
	 * @Description 获取文章列表
	 * @param curPageNO
	 * @return
	 */
	@ApiOperation(value = "获取头条新闻列表", httpMethod = "POST", notes = "获取头条新闻列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = false, dataType = "string")
	@PostMapping("/headlines")
	public ResultDto<AppPageSupport<AppHeadlineDto> > headlines(String curPageNO) {
		
		try {
			AppPageSupport<AppHeadlineDto> ps = appHeadlineService.query(curPageNO);
			if (AppUtils.isBlank(ps.getResultList())) {
				return ResultDtoManager.fail(-1, "暂无新闻资讯");
			}
			
			return ResultDtoManager.success(ps);
		} catch (Exception e) {
			LOGGER.error("获取头条新闻列表异常!", e);
			return ResultDtoManager.fail();
		}
	}
	

	/**
	 * @Description 获取文章详情
	 * @param id
	 * @return
	 */
	
	@ApiOperation(value = "获取头条新闻详情", httpMethod = "POST", notes = "获取头条新闻详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="path", name = "id", value = "头条新闻id", required = true, dataType = "Long")
	@PostMapping("/headlines/{id}")
	public ResultDto<AppHeadlineDto> load(@PathVariable Long id) {
		
		try {
			AppHeadlineDto appHeadlineDto = appHeadlineService.getHeadline(id);
			if (AppUtils.isBlank(appHeadlineDto)) {
				return ResultDtoManager.fail(-1, "该新闻资讯不存在或已被删除");
			}
			
			return ResultDtoManager.success(appHeadlineDto);
		} catch (Exception e) {
			LOGGER.error("获取头条新闻详情异常!", e);
			return ResultDtoManager.fail();
		}
	}
}
