package com.legendshop.app.dto.mergeGroup;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="拼团列表")
public class AppMergeGroupDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty("拼团活动ID, 唯一标识")
	private Long id;
	
	@ApiModelProperty("活动名称")
	private String mergeName;
	
	@ApiModelProperty("拼团人数(几人团)")
	private Integer peopleNumber; 
	
	@ApiModelProperty("拼团图片")
	private String pic;
	
	@ApiModelProperty("最低拼团价格 ")
	private Double minPrice; 
	
	@ApiModelProperty("最低商品价格 ")
	private Double minProdPrice; 
	
	@ApiModelProperty("已拼人数")
	private Long count;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMergeName() {
		return mergeName;
	}

	public void setMergeName(String mergeName) {
		this.mergeName = mergeName;
	}

	public Integer getPeopleNumber() {
		return peopleNumber;
	}

	public void setPeopleNumber(Integer peopleNumber) {
		this.peopleNumber = peopleNumber;
	}

	public Double getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}

	public Double getMinProdPrice() {
		return minProdPrice;
	}

	public void setMinProdPrice(Double minProdPrice) {
		this.minProdPrice = minProdPrice;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

}
