package com.legendshop.app.service;

import java.util.List;
import java.util.Map;

import com.legendshop.app.dto.AppActiveBanner;
import com.legendshop.app.dto.mergeGroup.AppMergeGroupDto;
import com.legendshop.app.dto.mergeGroup.AppMergeGroupingDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.AppTokenDto;

/**
 * App拼团活动服务接口
 */
public interface AppMergeGroupService {

	/**
	 * 获取拼团活动专区banner图列表
	 * @return
	 */
	List<AppActiveBanner> getActiveBanners();
	
	/**
	 * 获取秒杀活动专区banner图列表
	 * @return
	 */
	List<AppActiveBanner> getSeckillActiveBanners();
	
	/**
	 * 获取上线中的团购活动列表
	 * @param curPageNO
	 * @return
	 */
	AppPageSupport<AppMergeGroupDto> getAvailableMergeGroups(String curPageNO);

	/**
	 * 获取团购活动详情
	 * @param mergeGroupId 团购活动Id
	 * @param userId TODO
	 * @return
	 */
	Map<String, Object> getMergeGroupDetail(Long mergeGroupId, String addNumber, String userId);

	/**
	 * 获取正在进行中的团列表
	 * @param mergeGroupId
	 * @param count TODO
	 * @return
	 */
	List<AppMergeGroupingDto> getMergeGroupingDto(Long mergeGroupId, Integer count);

	/**
	 * 获取已开团详情
	 * @param sn 编号, 当来源是SUB时, sn传订单号(subNumber);当来源是GROUP, sn传团编号(addNumber或groupNumber)
	 * @param source 来源, 订单: SUB, 团编号: GROUP
	 * @param appToken TODO
	 * @return
	 */
	Map<String, Object> getMergeGroupGroupDetail(String sn, String source, String userId);
}
