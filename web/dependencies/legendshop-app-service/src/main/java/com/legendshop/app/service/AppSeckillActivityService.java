/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service;


import java.util.List;

import com.legendshop.app.dto.AppActiveBanner;
import com.legendshop.app.dto.AppSeckillActivityListDto;
import com.legendshop.app.dto.AppSeckillDto;
import com.legendshop.app.dto.AppSeckillSuccessDto;
import com.legendshop.model.dto.AppSecKillActivityQueryDTO;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.seckill.SeckillSuccessRecord;
import com.legendshop.model.entity.SeckillSuccess;

/**
 * app秒杀活动Service.
 */
public interface AppSeckillActivityService  {

	/** 获取秒杀活动列表 */
	public AppPageSupport<AppSeckillActivityListDto> querySeckillActivityList(AppSecKillActivityQueryDTO queryDTO);
	
	/** 获取秒杀商品详情 */
	public AppSeckillDto getSeckillDetail(Long seckillId);
	
	/** 获取秒杀成功信息 */
	public AppSeckillSuccessDto getSeckillSuccessInfo(SeckillSuccess seckillSuccess);
	
	/** 获取秒杀成功信息 */
	public AppSeckillSuccessDto getSeckillSuccessInfo(String userId, Long seckillId, Long skuId);
	
	/** 获取用户秒杀订单列表 */
	public AppPageSupport<SeckillSuccessRecord> queryUserSeckillRecord(String curPageNO, String userId);

	/** 获取秒杀banner图 */
	public List<AppActiveBanner> getActiveBanners();

}
