/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dao.impl;
 
import org.springframework.stereotype.Repository;

import com.legendshop.app.dao.AppTokenDao;
import com.legendshop.dao.impl.GenericDaoImpl;
import com.legendshop.dao.support.EntityCriterion;
import com.legendshop.model.entity.AppToken;

/**
 * The Class AppTokenDaoImpl.
 */
@Repository
public class AppTokenDaoImpl extends GenericDaoImpl<AppToken, Long> implements AppTokenDao  {
    
	@Override
	/*@Cacheable(value="App_Token", key="#userId")*/
	public AppToken getAppTokenByUserId(String userId) {
		return this.getByProperties(new EntityCriterion().eq("userId", userId));
	}

	public AppToken getAppToken(Long id){
		return getById(id);
	}
	
	/*@CacheEvict(value = "App_Token", key = "#appToken.userId")*/
    public int deleteAppToken(AppToken appToken){
    	return delete(appToken);
    }
	
	public Long saveAppToken(AppToken appToken){
		return save(appToken);
	}
	
	/*@CacheEvict(value = "App_Token", key = "#appToken.userId")*/
	public int updateAppToken(AppToken appToken){
		return update(appToken);
	}
	
	/**
	 * 删除token
	 */
	@Override
	/*@CacheEvict(value = "App_Token", key = "#userId")*/
	public boolean deleteAppToken(String userId, String accessToken) {
		return update("delete from ls_app_token where user_id = ? and access_token = ?",userId, accessToken) > 0;
	}

	@Override
	public boolean deleteAppToken(String userId) {
		return update("delete from ls_app_token where user_id = ?",userId) > 0;
	}

	/**
	 * 根据用户id和平台类型获取token
	 */
	@Override
	public AppToken getAppTokenByUserId(String userId, String platform,String type) {

		return this.getByProperties(new EntityCriterion().eq("userId", userId).eq("platform", platform).eq("type", type));
	}

 }
