package com.legendshop.app.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.legendshop.app.dto.AppProductDetailDto;
import com.legendshop.app.service.AppCartService;
import com.legendshop.app.service.AppProductService;
import com.legendshop.model.constant.CartTypeEnum;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ProductStatusEnum;
import com.legendshop.model.dto.app.ChgBasketDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.app.shopcart.UserShopCartListDto;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.Basket;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.Sku;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.model.entity.store.StoreSku;
import com.legendshop.spi.service.*;
import com.legendshop.util.AppUtils;
import com.legendshop.util.JSONUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 购物车控制器
 *
 */
@Api(tags="购物车",value="查看购物车")
@RestController
public class AppShopCartController {
	
	private final Logger log = LoggerFactory.getLogger(AppShopCartController.class);
		
	@Autowired
	private BasketService basketService;
	
	@Autowired
	private AppCartService appBasketService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private SkuService skuService;
	
	@Autowired
	private SubService subService;
	
	@Autowired
	private OrderUtil orderUtil;
	
	@Autowired
	private AppProductService appProductService;
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private OrderService orderService;
	
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private StoreSkuService storeSkuService;
	
	/**
	 * 购物车商品数
	 * @param request
	 * @param response
	 * @return
	 */
	@ApiOperation(value = "购物车商品数", httpMethod = "POST", notes = "获取购物车商品数,返回数值",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/cart/count")
	public  ResultDto<Integer> count(HttpServletRequest request, HttpServletResponse response) {
		
		int basketCount = 0;
		
		LoginedUserInfo token = loginedUserService.getUser();
		if(null != token){
			basketCount = basketService.getBasketCountByUserId(token.getUserId());
		}
		
		return ResultDtoManager.success(basketCount);
	}
	
	/**
	 * 检验商品库存-商品详情
	 * @param request
	 * @param response
	 * @param shopId
	 * @param productId
	 * @param buyNum
	 * @return
	 */
	@ApiOperation(value = "检验商品库存", httpMethod = "POST", notes = "检验商品库存是否足够,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		  @ApiImplicitParam(paramType = "path", dataType = "Long", name = "productId", value = "商品Id", required = true),
		  @ApiImplicitParam(paramType = "path", dataType = "Long", name = "skuId", value = "skuId", required = true),
		  @ApiImplicitParam(paramType = "path", dataType = "Integer", name = "buyNum", value = "购买数量", required = true),
	})
	@PostMapping(value="/p/veryBuyNum/{productId}/{skuId}/{buyNum}")
	public ResultDto<String> veryBuyNum(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Long productId,@PathVariable Long skuId,@PathVariable Integer buyNum) {
		
		if(buyNum<=0){
			return ResultDtoManager.fail(-1,"请输入正确的购买数量");
		}
		
		Product item=productService.getProductById(productId);
		if(item==null || !ProductStatusEnum.PROD_ONLINE.value().equals(item.getStatus())){
			return ResultDtoManager.fail(-1,"数据已发生改变，请刷新后再操作!");
		}
        
        String userId = loginedUserService.getUser().getUserId();
        if(item.getUserId().equals(userId)){
        	return ResultDtoManager.fail(-1,"自己的商品,不能购买!");
        }
        
		Sku sku=skuService.getSku(skuId);
		if(sku==null || sku.getStatus().intValue()!=1){
			return ResultDtoManager.fail(-1,"数据已发生改变，请刷新后再操作!");
		}
		if(sku.getStocks()-buyNum>=0){
			return ResultDtoManager.success();
		}
		return ResultDtoManager.fail(-1,"您当前最多能购买数量为 "+sku.getStocks()+"！");
	}
	
	/**
	 * 购物车
	 * @param request
	 * @param response
	 * @param curPageNO
	 * @return
	 */
	@ApiOperation(value = "购物车列表", httpMethod = "POST", notes = "获取购物车的列表",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/p/shopCart")
	public ResultDto<UserShopCartListDto> shopCart(HttpServletRequest request, HttpServletResponse response) {
		LoginedUserInfo token = loginedUserService.getUser();
		List<ShopCartItem> cartItems  = basketService.getShopCartByUserId(token.getUserId());
		
		//TODO 这段代码会导致购物车商品全部失效的情况下,不展示失效商品，购物车为空，先注释掉
		//区分是否失效 
		/*List<ShopCartItem> validCartItems = basketService.differInvalidList(cartItems);
		
		if(AppUtils.isBlank(validCartItems)){
			return ResultDtoManager.success();
		}*/
		//购物车根据来源分组,按照商城来分组
		UserShopCartList userShopCartList= basketService.getShoppingCarts(cartItems);
		if(userShopCartList==null || AppUtils.isBlank(userShopCartList.getShopCarts())){
			return ResultDtoManager.success();
		}
		
		UserShopCartListDto userShopCartListDto = appBasketService.convertUserShopCartListDto(userShopCartList);
		userShopCartListDto.setInvalidCartItems(appBasketService.InvalidListDto(cartItems));
		
		//去除重复对象
		String jsonString = JSONArray.toJSONString(userShopCartListDto, SerializerFeature.DisableCircularReferenceDetect);
		UserShopCartListDto userShopCart = JSONUtil.getObject(jsonString, UserShopCartListDto.class);
		return ResultDtoManager.success(userShopCart);
	}
	
	/**
	 * 删除购物车数据
	 * @param request
	 * @param response
	 * @param basketId
	 * @param prodId
	 * @param skuId
	 * @return
	 */
	@ApiOperation(value = "删除购物车数据", httpMethod = "POST", notes = "删除购物车数据,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		  @ApiImplicitParam(paramType = "query", dataType = "Long", name = "basketId", value = "购物车Id", required = true),
		  @ApiImplicitParam(paramType = "query", dataType = "Long", name = "prodId", value = "商品Id", required = true),
		  @ApiImplicitParam(paramType = "query", dataType = "Long", name = "skuId", value = "skuId", required = true)
	})
	@PostMapping(value="/p/deleteShopCart")
	public  ResultDto<String> deleteShopCart(HttpServletRequest request, HttpServletResponse response,
			Long basketId,@RequestParam Long prodId,@RequestParam Long skuId) {
		
		LoginedUserInfo token = loginedUserService.getUser();
		String userId = token.getUserId();
		basketService.deleteBasketById(prodId,skuId, userId);
		
		//清除购物车缓存
		subService.evictUserShopCartCache(CartTypeEnum.NORMAL.toCode(),userId);
		return ResultDtoManager.success();
	}
	
	/**
	 * 批量删除购物车商品
	 * @param request
	 * @param response
	 * @param selectedSkuId 
	 * @return
	 */
	@ApiOperation(value = "批量删除购物车商品", httpMethod = "POST", notes = "批量删除购物车商品,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		  @ApiImplicitParam(paramType = "query", dataType = "string", name = "selectedSkuId", value = "选中的skuid,多个已‘;’分割传递到后台", required = true)
	})
	@PostMapping(value="/p/deleteShopCarts")
	public  ResultDto<String> deleteShopCarts(HttpServletRequest request, HttpServletResponse response, String selectedSkuId) {
		
		LoginedUserInfo appToken = loginedUserService.getUser();
		String userId = appToken.getUserId();
		if(AppUtils.isBlank(userId)){
			return ResultDtoManager.fail(0, "userId error");
		}
		basketService.deleteBasketByIds(selectedSkuId, userId);
		return ResultDtoManager.success();	
	}
	
	/**
	 * 加入购物车
	 * @param prodId
	 * @param count
	 * @return
	 */
	@ApiOperation(value = "加入购物车", httpMethod = "POST", notes = "将商品加入购物车中,返回'OK'表示成功，返回'-1'表示失败则显示提示信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		  @ApiImplicitParam(paramType = "query", dataType = "Long", name = "prodId", value = "商品Id", required = true),
		  @ApiImplicitParam(paramType = "query", dataType = "Long", name = "skuId", value = "skuId", required = true),
		  @ApiImplicitParam(paramType = "query", dataType = "int", name = "count", value = "购买数量", required = true),
		  @ApiImplicitParam(paramType = "query", dataType = "String", name = "distUserName", value = "分销用户", required = false),
		  @ApiImplicitParam(paramType = "query", dataType = "Long", name = "storeId", value = "门店ID (普通方式加入购物车可不传)", required = false),
	})
	@PostMapping(value="/p/addShopBuy")
	public  ResultDto<String>  addShopBuy(@RequestParam Long prodId, @RequestParam Long skuId ,@RequestParam Integer count,String distUserName,Long storeId) {
		String userId = loginedUserService.getUser().getUserId();
		Product product = productService.getProductById(prodId);
		
		//不能购买自己商品
		UserDetail userDetail = userDetailService.getUserDetailById(userId);
		if(AppUtils.isNotBlank(userDetail.getShopId()) ){
			if(userDetail.getShopId().equals(product.getShopId())){
				return ResultDtoManager.fail(-1,"自己的商品,不能购买!");
			}
		}

		String result = null;
		if (AppUtils.isBlank(storeId)) {

			result = orderUtil.checkCartParams(product,skuId,count,userId);
		}else{

			result = orderUtil.checkStoreCartParams(product, skuId, count, userId, storeId);
		}

		if(!Constants.SUCCESS.equals(result)){
			return  ResultDtoManager.fail(-1,result);
		}

		Long number = basketService.saveToCart(userId, prodId, count, skuId,product.getShopId(),distUserName,storeId);
		if(number<0){
			return ResultDtoManager.fail(-1,"购物车商品过多，请先处理！");
		}
		return  ResultDtoManager.success();
	}
	
	
	
	/**
	 * 更改购物车数量
	 * @param request
	 * @param response
	 * @return
	 */
	@ApiOperation(value = "更改购物车数量", httpMethod = "POST", notes = "将修改购物车里的商品数量,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		  @ApiImplicitParam(paramType = "query", dataType = "Long", name = "prodId", value = "商品Id", required = true),
		  @ApiImplicitParam(paramType = "query", dataType = "Long", name = "skuId", value = "skuId", required = true),
		  @ApiImplicitParam(paramType = "query", dataType = "Integer", name = "basketCount", value = "购物车数量", required = true),
	})
	@PostMapping(value="/p/changeShopCartNum")
	public  ResultDto<String> changeShopCartNum(HttpServletRequest request, HttpServletResponse response,
			@RequestParam Long prodId, 
			@RequestParam Long skuId, 
			@RequestParam Integer basketCount){
		LoginedUserInfo appToken = loginedUserService.getUser();
		if(basketCount<=0){
			return  ResultDtoManager.fail(0,"请输入正确的购买数量");
		}
		Product item=productService.getProductById(prodId);
		if(item==null ||  !Constants.ONLINE.equals(item.getStatus())){
			return  ResultDtoManager.fail(0,"数据已发生改变，请刷新后再操作!");
		}
		
		String userId=appToken.getUserId();
		if(userId.equals(item.getUserId())){
			return  ResultDtoManager.fail(0,"自己的商品,不能购买!");
		}
		Sku sku=skuService.getSku(skuId);
		if(sku==null || sku.getStatus().intValue()!=1){
			return  ResultDtoManager.fail(0,"数据已发生改变，请刷新后再操作!");
		}
		if(sku.getStocks()<basketCount){
			basketCount=sku.getStocks().intValue();
			return  ResultDtoManager.fail(0,"超过购买数量!");
		}
		basketService.updateBasket(prodId,skuId,basketCount,userId);
		return  ResultDtoManager.success();
	}
	
	
	/**
	 * 改变  购物车项 的选中状态 
	 */
	@ApiOperation(value = "更改购物车选中状态", httpMethod = "POST", notes = " 改变  购物车项 的选中状态,返回'OK'表示成功，返回'fail'表示失败 ",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		  @ApiImplicitParam(paramType = "path", dataType = "Long", name = "basketId", value = "购物车Id", required = true),
		  @ApiImplicitParam(paramType = "query", dataType = "int", name = "checkSts", value = "选中的状态", required = false),
	})
	@PostMapping(value="/p/changeBasketSts/{basketId}")
	public  ResultDto<String> changeBasketSts(HttpServletRequest request, HttpServletResponse response,@PathVariable Long basketId,
			int checkSts) {
		String userId= loginedUserService.getUser().getUserId();
		basketService.updateBasketChkSts(basketId, userId, checkSts);
		//清除购物车缓存
		subService.evictUserShopCartCache(CartTypeEnum.NORMAL.toCode(),userId);
		return  ResultDtoManager.success();
	}
	
	
	/**
	 * 批量改变  购物车项 的选中状态
	 * @param request
	 * @param response
	 * @param changeStr 拼接字符串  [{"basketId":1005,"checkSts":1},{"basketId":1005,"checkSts":1}]
	 * @return
	 */
	@ApiOperation(value = "批量更改购物车选中状态", httpMethod = "POST", notes = " 批量改变  购物车项 的选中状态,返回'OK'表示成功，返回'fail'表示失败 ",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType = "query", dataType = "String", name = "changeStr", value = "拼接字符串  [{\"basketId\":1005,\"checkSts\":1},{\"basketId\":1005,\"checkSts\":1}]", required = true)
	@PostMapping(value="/p/batchChgBasketSts")
	public  ResultDto<String> batchChgBasketSts(HttpServletRequest request, HttpServletResponse response,
			@RequestParam String changeStr) {
		List<ChgBasketDto> list = JSONUtil.getArray(changeStr, ChgBasketDto.class);
		if(AppUtils.isBlank(list)){
			return  ResultDtoManager.fail(-1, "参数数据有误!");
		}
		String userId= loginedUserService.getUser().getUserId();
		
		List<Basket> basketList = new ArrayList<Basket>(list.size());
		for(ChgBasketDto basketDto:list){
			Basket basket = new Basket();
			basket.setBasketId(basketDto.getBasketId());
			basket.setCheckSts(basketDto.getCheckSts());
			basket.setUserId(userId);
			basketList.add(basket);
		}
		basketService.batchUpdateBasketChkSts(userId, basketList);
		subService.evictUserShopCartCache(CartTypeEnum.NORMAL.toCode(),userId);
		return  ResultDtoManager.success();
	}
	
	/**
	 * 清空购物车失效商品
	 */
	@ApiOperation(value = "清空购物车失效商品", httpMethod = "GET", notes = "清空购物车失效商品,返回'OK'表示成功，返回'fail'表示失败",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@GetMapping(value="/p/clearInvalid")
	public  ResultDto<String> clearInvalid(HttpServletRequest request, HttpServletResponse response) {
		
		LoginedUserInfo appToken = loginedUserService.getUser();
		try {
			basketService.clearInvalidBaskets(appToken.getUserId());
			
			//清除购物车缓存
			subService.evictUserShopCartCache(CartTypeEnum.NORMAL.toCode(),appToken.getUserId());
			return ResultDtoManager.success();
		} catch (Exception e) {
			log.error("清空购物车失效商品数据错误:"+e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 查看获取商品所有sku选择列表
	 * @param prodId
	 * @return
	 */
	@ApiOperation(value = "查看获取商品所有sku选择", httpMethod = "POST", notes = "查看获取商品所有sku选择",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="path", name = "prodId", value = "商品Id", required = true, dataType = "Long")
	@PostMapping(value="/selectSkus/{prodId}")
	public ResultDto<AppProductDetailDto> selectSkus(HttpServletRequest request, @PathVariable Long prodId){
		
		AppProductDetailDto productDetailDto = appProductService.getProductDetailDto(prodId);
		
		ResultDto<AppProductDetailDto> dto = ResultDtoManager.success();
		if(productDetailDto == null){
			dto.setMsg("Prod not found by id " + prodId);
		}
		dto.setResult(productDetailDto);
		return dto;
	}
	
	/**
	 * 更改购物车中商品选中的sku
	 */
	@ApiOperation(value = "更改购物车中商品选中的sku", httpMethod = "POST", notes = "更改购物车中商品选中的sku,返回'OK'表示成功，返回'fail'表示失败,返回0则显示后台返回信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="path", name = "prodId", value = "商品Id", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="path", name = "oldSkuId", value = "之前的skuId", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="path", name = "skuId", value = "现在确认选中的skuId", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="path", name = "basketId", value = "购物车项Id", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="path", name = "basketCount", value = "加购数量", required = true, dataType = "Integer"),
	})
	@PostMapping(value="/p/changeSelectedSku")
	public ResultDto<String> changeSelectedSku(HttpServletRequest request,Long prodId,Long oldSkuId,Long skuId,Long basketId,Integer basketCount){
		LoginedUserInfo appToken = loginedUserService.getUser();
		try {
			if(basketCount<=0){
				return  ResultDtoManager.fail(0,"请输入正确的购买数量");
			}
			Product item=productService.getProductById(prodId);
			if(item==null ||  !Constants.ONLINE.equals(item.getStatus())){
				return  ResultDtoManager.fail(0,"数据已发生改变，请刷新后再操作!");
			}
			
			String userId=appToken.getUserId();
			if(userId.equals(item.getUserId())){
				return  ResultDtoManager.fail(0,"自己的商品,不能购买!");
			}
			Sku sku=skuService.getSku(skuId);
			if(sku==null || sku.getStatus().intValue()!=1){
				return  ResultDtoManager.fail(0,"数据已发生改变，请刷新后再操作!");
			}
			if(sku.getStocks() < basketCount){
				basketCount=sku.getStocks().intValue();
				return  ResultDtoManager.fail(0,"超过购买数量!");
			}
			//之前选中的购物车项
			Basket oldBasket = basketService.getBasketByUserId(prodId, appToken.getUserId(), oldSkuId);
			if(AppUtils.isBlank(oldBasket)){
				return  ResultDtoManager.fail(0,"数据已发生改变，请刷新后再操作!");
			}
			//即将更改的购物车项
			Basket basket = basketService.getBasketByUserId(prodId, appToken.getUserId(), skuId);
			if(AppUtils.isBlank(basket)){
				oldBasket.setSkuId(skuId);
				oldBasket.setBasketCount(basketCount);
				oldBasket.setBasketDate(new Date());
				basketService.updateBasket(oldBasket);
			}else{
				return  ResultDtoManager.fail(0,"购物车已存在该规格属性，请直接修改数量！");
			}
			//清除购物车缓存
			subService.evictUserShopCartCache(CartTypeEnum.NORMAL.toCode(),appToken.getUserId());
			return ResultDtoManager.success();
		} catch (Exception e) {
			log.error("更改购物车中商品选中的sku出错:"+e.getMessage());
			return ResultDtoManager.fail();
		}
	}


	/**
	 * 更换门店
	 */
	@ApiOperation(value = "更换门店", httpMethod = "POST", notes = "更换门店，成功返回ok,失败返回具体信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
			@ApiImplicitParam(paramType="query", name = "productId", value = "商品Id", required = true, dataType = "Long"),
			@ApiImplicitParam(paramType="query", name = "skuId", value = "skuId", required = true, dataType = "Long"),
			@ApiImplicitParam(paramType="query", name = "storeId", value = "门店Id", required = true, dataType = "Long"),
			@ApiImplicitParam(paramType="query", name = "basketCount", value = "购物车数量", required = true, dataType = "Integer"),
	})
	@PostMapping(value="/p/updateShopStore")
	public ResultDto<String> updateShopStore(Long productId,Long skuId,Long storeId,Integer basketCount){

		String userId = loginedUserService.getUser().getUserId();
		try {

			Product item=productService.getProductById(productId);
			if(item==null || !ProductStatusEnum.PROD_ONLINE.value().equals(item.getStatus())){

				return  ResultDtoManager.fail(0,"数据已发生改变，请刷新后再操作!");
			}

			Sku sku=skuService.getSku(skuId);
			if(sku==null || sku.getStatus().intValue()!=1){

				return  ResultDtoManager.fail(0,"数据已发生改变，请刷新后再操作!");
			}

			StoreSku storeSku = storeSkuService.getStoreSkuBySkuId(storeId, skuId);
			if (null == storeSku) {

				return  ResultDtoManager.fail(0,"数据已发生改变，请刷新后再操作!");
			}

			if(storeSku.getStock()<basketCount){

				return  ResultDtoManager.fail(0,"该门店库存不足，请重新选择!");
			}

			basketService.updateShopStore(productId,skuId,storeId, userId);

			return ResultDtoManager.success();
		} catch (Exception e) {
			log.error("更换门店异常:"+e.getMessage());
			return ResultDtoManager.fail();
		}
	}



	
}
