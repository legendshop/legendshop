package com.legendshop.app.service;

import com.legendshop.app.dto.AppDiscoverArticleInfoDto;
import com.legendshop.app.dto.AppDiscoverArticleListDto;
import com.legendshop.app.dto.AppDiscoverCommDto;
import com.legendshop.model.dto.app.AppPageSupport;

public interface AppDiscoverArticleService {

	/**
	 * 分页查询
	 * @param pageNo 当前页
	 * @param size 分页数
	 * @param condition 搜索条件
	 * @param order 排序条件
	 * @return
	 */
	public abstract AppPageSupport<AppDiscoverArticleListDto> queryPage(String pageNo,Integer size,String condition,String order);

	/**
	 * 文章详情查询
	 * @param discoverId 文章id
	 * @return
	 */
	public abstract AppDiscoverArticleInfoDto queryByDisId(Long discoverId);

	/**
	 * 根据发现文章id分页查询文章评论
	 * @param disId 发现文章id
	 * @param index
	 * @return
	 */
	public abstract AppPageSupport<AppDiscoverCommDto> queryDisCommByGrassId(Long disId, String index);
}
