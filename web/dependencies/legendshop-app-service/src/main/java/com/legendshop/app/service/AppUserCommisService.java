/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service;

import com.legendshop.app.dto.AppMyPromoterUserDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.promoter.model.UserCommis;

/**
 * 用户佣金Service
 */
public interface AppUserCommisService {

	/** 加载下级用户列表 */
	public AppPageSupport<AppMyPromoterUserDto> getUserCommis(String curPageNO, String userName, UserCommis userCommis, int pageSize);
	
}
