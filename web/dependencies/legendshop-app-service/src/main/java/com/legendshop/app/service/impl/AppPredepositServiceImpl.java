/**
 * 
 */
package com.legendshop.app.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.AppPredepositService;
import com.legendshop.business.dao.ExpensesRecordDao;
import com.legendshop.business.dao.PdCashLogDao;
import com.legendshop.business.dao.PdRechargeDao;
import com.legendshop.dao.support.CriteriaQuery;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.UserPredepositDto;
import com.legendshop.model.entity.ExpensesRecord;
import com.legendshop.model.entity.PdCashLog;
import com.legendshop.model.entity.PdRecharge;
import com.legendshop.model.entity.UserDetail;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.util.AppUtils;

/**
 * 我的余额
 */
@Service("appPredepositService")
public class AppPredepositServiceImpl implements AppPredepositService {

	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
    private PdRechargeDao pdRechargeDao;
    
	@Autowired
    private PdCashLogDao pdCashLogDao;
    
	@Autowired
    private ExpensesRecordDao expensesRecordDao;


	/**
	 * @Description: 余额日志
	 * @param @param userId
	 * @param @param curPageNO
	 * @param @return   
	 * @date 2016-7-15
	 */
	@Override
	public UserPredepositDto findUserPredepositLog(String userId,String curPageNO) {
		
		UserDetail userDetail = userDetailService.getUserDetailByIdNoCache(userId);
		UserPredepositDto  predeposit = new UserPredepositDto();
		if(AppUtils.isNotBlank(userDetail)){
			predeposit.setFreezePredeposit(userDetail.getFreezePredeposit());
			predeposit.setAvailablePredeposit(userDetail.getAvailablePredeposit());
		}
		
		PageSupport<PdCashLog> pdCashLogs = pdCashLogDao.getPdCashLogPage(userId,curPageNO);
		predeposit.setPdCashLogs(convertPageSupportDto(pdCashLogs,1));
		return predeposit;
	}
	
	
	
	/**
	 * 余额充值记录
	 */
	@Override
	public UserPredepositDto findUserPredepositRecharge(String userId,String curPageNO) {
		UserPredepositDto  predeposit=new UserPredepositDto();
		UserDetail userDetail=userDetailService.getUserDetailByIdNoCache(userId);
		if(AppUtils.isNotBlank(userDetail)){
			predeposit.setFreezePredeposit(userDetail.getFreezePredeposit());
			predeposit.setAvailablePredeposit(userDetail.getAvailablePredeposit());
		}
		PageSupport<PdRecharge> pdRecharges=pdRechargeDao.getPdRechargePage(userId, null, null, curPageNO, 10);
		predeposit.setRecharges(convertPageSupportDto(pdRecharges,2));
		return predeposit;
	}
	
	/**
	 * 消费记录
	 * */
	@Override
	public UserPredepositDto findUserExpensesRecord(String userId,String curPageNO) {
		UserDetail userDetail = userDetailService.getUserDetailByIdNoCache(userId);
		UserPredepositDto  predeposit=new UserPredepositDto();
		if(AppUtils.isNotBlank(userDetail)){
			predeposit.setFreezePredeposit(userDetail.getFreezePredeposit());
			predeposit.setAvailablePredeposit(userDetail.getAvailablePredeposit());
		}
		CriteriaQuery cq=new CriteriaQuery(ExpensesRecord.class, curPageNO);
		cq.setPageSize(10);
		cq.eq("userId", userId);
		cq.addDescOrder("recordDate");
		PageSupport<ExpensesRecord> ExpensesRecord = expensesRecordDao.getExpensesRecord(cq);
		predeposit.setRecharges(convertPageSupportDto(ExpensesRecord,3));
		return predeposit;
	}
	
	
	/**
	 * PageSupport转换为PageSupportDto
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private AppPageSupport convertPageSupportDto(PageSupport pageSupport,Integer flag){
		if(AppUtils.isNotBlank(pageSupport)){
			AppPageSupport pageSupportDto = new AppPageSupport();
			pageSupportDto.setTotal(pageSupport.getTotal());
			pageSupportDto.setCurrPage(pageSupport.getCurPageNO());
			pageSupportDto.setPageSize(pageSupport.getPageSize());
			switch (flag) {
			case 1:
				List<PdCashLog> integralLogs=(List<PdCashLog>) pageSupport.getResultList();
				pageSupportDto.setResultList(integralLogs);
				break;
           case 2:
        	    List<PdRecharge> PdRecharges=(List<PdRecharge>) pageSupport.getResultList();
				pageSupportDto.setResultList(PdRecharges);
				break;
           case 3:
        	   List<ExpensesRecord> ExpensesRecord = (List<ExpensesRecord>) pageSupport.getResultList();
        	   pageSupportDto.setResultList(ExpensesRecord);
			   break;
			default:
				break;
			}
			pageSupportDto.setPageCount(pageSupport.getPageCount());
			return pageSupportDto;
		}
		return null;
	}

}
