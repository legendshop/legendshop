package com.legendshop.app.dto;

import org.springframework.web.bind.annotation.RequestParam;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 门店下单参数DTO
 */
@ApiModel("门店下单参数DTO")
public class AppStoreOrderAddParamDto {

	/** 支付方式 暂只支持线上支付 */
	@ApiModelProperty(value = "支付方式 1:货到付款,2:在线支付 暂只支持线上支付 ")
	private  Integer payManner;

	/** 提货人名称    [storeId:提货人名称 ]*/
	@ApiModelProperty(value = "提货人名称    [storeId:提货人名称 ]")
	private String buyerName;

	/** 提货人手机号码    [storeId:提货人手机号码 ]*/
	@ApiModelProperty(value = "提货人手机号码    [storeId:提货人手机号码 ]")
	private String telPhone;

	/** 买家留言  [storeId:买家留言 ] */
	@ApiModelProperty(value = "买家留言  [storeId:买家留言 ]")
	private String remarkText;

	/** 防重复提交token */
	@ApiModelProperty(value = " 防重复提交token")
	private  String token;

	/** 发票Id 暂时没用 */
	@ApiModelProperty(value = "发票Id 暂时没用")
	private  Long invoiceId;

	/** 使用的优惠券  暂时没用 */
	@ApiModelProperty(value = " 使用的优惠券  暂时没用 ")
	private  String couponStr;

	public Integer getPayManner() {
		return payManner;
	}

	public void setPayManner(Integer payManner) {
		this.payManner = payManner;
	}

	public String getBuyerName() {
		return buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getTelPhone() {
		return telPhone;
	}

	public void setTelPhone(String telPhone) {
		this.telPhone = telPhone;
	}

	public String getRemarkText() {
		return remarkText;
	}

	public void setRemarkText(String remarkText) {
		this.remarkText = remarkText;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getCouponStr() {
		return couponStr;
	}

	public void setCouponStr(String couponStr) {
		this.couponStr = couponStr;
	}
}
