package com.legendshop.app.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("种草关注或粉丝Dto")
public class AppGrassConcernListDto {
	@ApiModelProperty(value="用户id")
	private String uid;
	
	@ApiModelProperty(value="用户名字")
	private String name;
	
	@ApiModelProperty(value="用户头像")
	private String image;
	
	@ApiModelProperty(value="是否关注")
	private boolean rsConcern;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public boolean isRsConcern() {
		return rsConcern;
	}

	public void setRsConcern(boolean rsConcern) {
		this.rsConcern = rsConcern;
	}

	
	
	
	
	
}
