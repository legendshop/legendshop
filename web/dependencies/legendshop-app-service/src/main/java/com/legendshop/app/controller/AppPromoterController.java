/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.model.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.dto.AppApplyRParamDto;
import com.legendshop.app.dto.AppCommisChangeLogDto;
import com.legendshop.app.dto.AppMyPromoterDto;
import com.legendshop.app.dto.AppMyPromoterUserDetailDto;
import com.legendshop.app.dto.AppMyPromoterUserDto;
import com.legendshop.app.dto.AppWithdrawalLogDto;
import com.legendshop.app.service.AppCommisChangeLogService;
import com.legendshop.app.service.AppUserCommisService;
import com.legendshop.app.service.AppWithdrawalLogService;
import com.legendshop.util.DateUtils;
import com.legendshop.business.service.weixin.WeixinMiniProgramApi;
import com.legendshop.config.PropertiesUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.dto.weixin.WXACodeResultDto;
import com.legendshop.promoter.model.DistSet;
import com.legendshop.promoter.model.UserCommis;
import com.legendshop.spi.service.DistSetService;
import com.legendshop.spi.service.LocationService;
import com.legendshop.spi.service.LoginedUserService;
import com.legendshop.spi.service.SMSLogService;
import com.legendshop.spi.service.UserCommisService;
import com.legendshop.spi.service.UserDetailService;
import com.legendshop.spi.service.WeixinMpTokenService;
import com.legendshop.uploader.AttachmentManager;
import com.legendshop.uploader.model.FeignMultipartFile;
import com.legendshop.util.AppUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * App分销推广控制器
 * @param <AppPromotePostersDto>
 */
@Api(tags="分销推广",value="分销推广相关接口")
@RestController
public class AppPromoterController<AppPromotePostersDto>{

	/** The log. */
	private final Logger LOGGER = LoggerFactory.getLogger(AppPromoterController.class);
	
	@Autowired
	private UserCommisService userCommisService;

	@Autowired
	private DistSetService distSetService;

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private SMSLogService smsLogService;

    @Autowired
    private PasswordEncoder passwordEncoder;
	
    @Autowired
	private LocationService locationService;
    
    /** 系统配置. */
	@Autowired
	private PropertiesUtil propertiesUtil;
    
	@Autowired
	private AppWithdrawalLogService appWithdrawalLogService;
	
	@Autowired
	private AppCommisChangeLogService appCommisChangeLogService;
	
	@Autowired
	private AppUserCommisService appUserCommisService;
	
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;
	
	@Autowired
	private WeixinMpTokenService weixinMpTokenService;
	
	@Autowired
	private AttachmentManager attachmentManager;
	/**
	 * 我的推广
	 * 
	 * @param reset
	 *  是否重填
	 */
    @ApiOperation(value = "我的推广", httpMethod = "POST", notes = "我的推广",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParam(paramType="query", name = "reset", value = "是否重填，申请时需要",  dataType = "Integer")
    @PostMapping(value = "/p/appPromoter/applyPromoter")
	public  ResultDto<AppApplyRParamDto> applyPromoter(Integer reset) {
		String userId = loginedUserService.getUser().getUserId();
		DistSet originDistSet = distSetService.getDistSet(1L);
		UserCommis userCommis = userCommisService.getUserCommis(userId);
		UserDetail userDetail = userDetailService.getUserDetailById(userId);
		
		AppApplyRParamDto appReturn = new AppApplyRParamDto();
		appReturn.setRealName(userDetail.getRealName());
		if (AppUtils.isNotBlank(userCommis) && userCommis.getPromoterSts() == 1) { // 审核通过返回用户名
			appReturn.setUserName(userDetail.getUserName());
			String vueDomainName = propertiesUtil.getVueDomainName();
			appReturn.setVueDomain(vueDomainName);
			appReturn.setStatus(1);
			return ResultDtoManager.success(appReturn);
		}

		if (AppUtils.isBlank(userCommis) || userCommis.getPromoterSts().intValue() == 0 || AppUtils.isNotBlank(reset)) {
			appReturn = convertAppApplyRParamDto(originDistSet, userDetail);
			appReturn.setStatus(0);
			return ResultDtoManager.success(appReturn);
		} else if(userCommis.getPromoterSts().intValue() == -1){
			// 审批:拒绝 -1  等待审批-2  0：初始(不是推广员)
			appReturn.setStatus(-1);
			return ResultDtoManager.success(appReturn);
		}else if(userCommis.getPromoterSts().intValue() == -2){
			appReturn.setStatus(-2);
			return ResultDtoManager.success(appReturn);
		}
		return ResultDtoManager.fail();
	}
	
	/**
	 * 封装去申请推广员数据
	 * 
	 * @param originDistSet
	 * @param userDetail
	 * @return
	 */
	private AppApplyRParamDto convertAppApplyRParamDto(DistSet originDistSet, UserDetail userDetail) {
		AppApplyRParamDto appReturn = new AppApplyRParamDto();
		appReturn.setChkRealName(originDistSet.getChkRealName());
		appReturn.setChkMobile(originDistSet.getChkMobile());
		appReturn.setChkMail(originDistSet.getChkMail());
		appReturn.setChkAddr(originDistSet.getChkAddr());
		appReturn.setRealName(userDetail.getRealName());
		appReturn.setUserName(userDetail.getUserName());
		appReturn.setUserMobile(userDetail.getUserMobile());
		appReturn.setUserMail(userDetail.getUserMail());
		appReturn.setUserAdds(userDetail.getUserAdds());
		appReturn.setProvinceid(userDetail.getProvinceid());
		appReturn.setCityid(userDetail.getCityid());
		appReturn.setAreaid(userDetail.getAreaid());
		if(AppUtils.isNotBlank(userDetail.getProvinceid())){
			Province province = locationService.getProvinceById(userDetail.getProvinceid());
			appReturn.setProvince(province.getProvince());
		}
		if(AppUtils.isNotBlank(userDetail.getCityid())){
			City city = locationService.getCityById(userDetail.getCityid());
			appReturn.setCity(city.getCity());
		}
		if(AppUtils.isNotBlank(userDetail.getAreaid())){
			Area area = locationService.getAreaById(userDetail.getAreaid());
			appReturn.setArea(area.getArea());
		}
		return appReturn;
	}

	/**
	 * 提交申请
	 * 
	 * @return
	 */
	@ApiOperation(value = "提交申请", httpMethod = "POST", notes = "提交申请，成功返回ok",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "name", value = "用户名", required = true, dataType = "String"),
		@ApiImplicitParam(paramType="query", name = "userMobile", value = "手机号", required = true, dataType = "String"),
		@ApiImplicitParam(paramType="query", name = "verifyCode", value = "手机验证码", required = true, dataType = "String"),
		@ApiImplicitParam(paramType="query", name = "provinceid", value = "省id", required = true, dataType = "Integer"),
		@ApiImplicitParam(paramType="query", name = "cityid", value = "市id", required = true, dataType = "Integer"),
		@ApiImplicitParam(paramType="query", name = "areaid", value = "区id", required = true, dataType = "Integer"),
		@ApiImplicitParam(paramType="query", name = "userAdds", value = "详细地址", required = true, dataType = "String")
	})
	@PostMapping(value = "/p/appPromoter/submitPromoter")
	public  ResultDto<String> submitPromoter(String name, String userMobile, String verifyCode, Integer provinceid, Integer cityid,
			Integer areaid, String userAdds) {
		LoginedUserInfo appToken = loginedUserService.getUser();
		String userId = appToken.getUserId();
		String userName = appToken.getUserName();
		DistSet originDistSet = distSetService.getDistSet(1L);
		UserDetail userPo = userDetailService.getUserDetailById(userId);
		if (AppUtils.isBlank(originDistSet) || AppUtils.isBlank(userPo)) {
			return ResultDtoManager.fail(-1, "数据有误！");
		}
		if (originDistSet.getChkRealName() == 1) {
			if(AppUtils.isBlank(name)){
				return ResultDtoManager.fail(-1, "用户名不能为空");
			}
			userPo.setRealName(name);
		}
		if (originDistSet.getChkMobile() == 1) {
			if (AppUtils.isBlank(userMobile) || AppUtils.isBlank(verifyCode)) {
				return ResultDtoManager.fail(-1, "手机号码或手机验证码不能为空");
			}
			boolean flag = smsLogService.verifyCodeAndClear(userMobile, verifyCode);
			if (!flag) { // 需要手机号码才验证
				return ResultDtoManager.fail(-1, "手机验证码错误");
			}
			userPo.setUserMobile(userMobile);
		}

		if (originDistSet.getChkAddr() == 1) {
			if (AppUtils.isBlank(provinceid) || AppUtils.isBlank(cityid) || AppUtils.isBlank(areaid) || AppUtils.isBlank(userAdds)) {
				return ResultDtoManager.fail(-1, "地址参数有误，请检查");
			}
			userPo.setProvinceid(provinceid);
			userPo.setCityid(cityid);
			userPo.setAreaid(areaid);
			userPo.setUserAdds(userAdds);
		}

		userDetailService.updateUserDetail(userPo); // 更新提交用户基础信息

		UserCommis userCommis = userCommisService.getUserCommis(userId);
		if (userCommis == null) {
			userCommis = new UserCommis();
			userCommis.setUserId(userId);
			userCommis.setUserName(userName);
			userCommis.setRealName(name);
			userCommis.setId(userId);
			userCommis.setTotalDistCommis(0d);
			userCommis.setFirstDistCommis(0d);
			userCommis.setSecondDistCommis(0d);
			userCommis.setThirdDistCommis(0d);
			userCommis.setSettledDistCommis(0d);
			userCommis.setUnsettleDistCommis(0d);
			userCommis.setPromoterSts(-2L);// 审核中
			userCommis.setPromoterApplyTime(new Date());// 申请时间
			userCommis.setCommisSts(1);//
			userCommisService.addSaveUserCommis(userCommis); // 新增用户佣金
		} else {
			userCommis.setPromoterSts(-2L);// 审核中
			userCommis.setPromoterApplyTime(new Date());// 申请时间
			userCommis.setCommisSts(1);//
			userCommisService.updateUserCommis(userCommis);
		}
		return ResultDtoManager.success();
	}

	/**
	 * 我的分销
	 * 
	 * @return
	 */
	@ApiOperation(value = "我的分销", httpMethod = "POST", notes = "我的分销，成功返回我的分销中心数据对象,返回失败-1则跳转到申请页面，返回失败-2则跳转到",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value = "/p/myPromoter")
	public  ResultDto<AppMyPromoterDto> myPromote() {
		try {
			LoginedUserInfo token = loginedUserService.getUser();
			String userId = token.getUserId();

			// 查询当前用户佣金明细
			UserCommis userCommis = userCommisService.getUserCommis(userId);
			if(AppUtils.isBlank(userCommis) || userCommis.getPromoterSts() != 1){
				return ResultDtoManager.fail(-1,"您还不是推广员");
			}
			
			AppMyPromoterDto appMyPromoter = getAppMyPromoter(userCommis);
			
			return ResultDtoManager.success(appMyPromoter);
		} catch (Exception e) {
			LOGGER.error("获取我的分销出错",e.getMessage());
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 加载 推广分销规则
	 *
	 * @return
	 */
	@ApiOperation(value = "分销规则", httpMethod = "GET", notes = "分销规则,成功返回ok并且结果为注册佣金,错误返回提示信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@GetMapping(value = "/loadRule")
	public  ResultDto<Double> loadRule() {
		try {
			DistSet distSet = distSetService.getDistSet(1L);
			if(AppUtils.isBlank(distSet)){
				return ResultDtoManager.fail(-1,"暂无分销规则！");
			}
			Double regCommis = distSet.getRegCommis();
			return ResultDtoManager.success(regCommis);
		} catch (Exception e) {
			LOGGER.error("获取分销规则出错",e.getMessage());
			return ResultDtoManager.fail();
		}
	}

	/**
	 * 封装返回我的分销App 数据
	 * 
	 * @param userCommis
	 * @return
	 */
	public AppMyPromoterDto getAppMyPromoter(UserCommis userCommis) {
		AppMyPromoterDto appMyPro = new AppMyPromoterDto();
		appMyPro.setTotalDistCommis(userCommis.getTotalDistCommis());
		appMyPro.setSettledDistCommis(userCommis.getSettledDistCommis());
		appMyPro.setUnsettleDistCommis(userCommis.getUnsettleDistCommis());
		appMyPro.setCommisSts(userCommis.getCommisSts());
		appMyPro.setPromoterSts(userCommis.getPromoterSts());
		return appMyPro;
	}

	/**
	 * go to 申请提现页面
	 * 
	 * @return
	 */
	@ApiOperation(value = "申请提现页面", httpMethod = "POST", notes = "申请提现页面,成功返回ok并且结果为可提现金额,错误返回提示信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value = "/p/toApplyCash")
	public ResultDto<Double> toApplyCash() {
		try {
			String userId = loginedUserService.getUser().getUserId();

			UserCommis userCommis = userCommisService.getUserCommis(userId);
			if(AppUtils.isBlank(userCommis)){
				return ResultDtoManager.fail(-1,"佣金提现状态异常，请联系商城客服！");
			}
			
			if(userCommis.getPromoterSts().intValue()==0){
				return ResultDtoManager.fail(-1,"对不起, 您不是分销员, 不能提现！");
			}
			
			Double settledDistCommis = userCommis.getSettledDistCommis();
			// 返回可提现金额
			return ResultDtoManager.success(settledDistCommis);
		} catch (Exception e) {
			LOGGER.error("获取申请提现页面出错",e.getMessage());
			return ResultDtoManager.fail();
		}
	}

	/**
	 * 处理申请提现
	 * 
	 * @param money
	 *            提现金额
	 * @param passwd
	 *            支付密码
	 * @return
	 */
	@ApiOperation(value = "处理申请提现", httpMethod = "POST", notes = "处理申请提现,成功返回ok并返回提现金额,错误返回提示信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "money", value = "提现金额", required = true, dataType = "Double"),
		@ApiImplicitParam(paramType="query", name = "passwd", value = "支付密码", required = true, dataType = "String")
	})
	@PostMapping(value = "/p/applyCash")
	public  ResultDto<Double> applyCash(Double money, String passwd) {
		String userName = loginedUserService.getUser().getUserName();
		Map<String, Object> result;
		UserSecurity security=userDetailService.getUserSecurity(userName);
		if(security.getPaypassVerifn().intValue()==0){ //是否通过支付验证
			return ResultDtoManager.fail(-2,"请开启安全支付密码");
		}
		// 校验密码是否正确
		UserDetail user = userDetailService.getUserDetailById(loginedUserService.getUser().getUserId());
		if (!passwordEncoder.matches(passwd, user.getPayPassword())) {
			return ResultDtoManager.fail(-1, "对不起,您输入的支付密码有误,请重新输入!");
		}
		// 处理佣金提现
		result = userCommisService.processWithDraw(userName, money);
		if (result.get("status").equals(Constants.SUCCESS)) {
			return ResultDtoManager.success(money);
		}
		return ResultDtoManager.fail(-1, result.get("msg").toString());
	}

	/**
	 * 提现记录
	 * @param curPageNO
	 * @return
	 */
	@ApiOperation(value = "提现记录", httpMethod = "POST", notes = "提现记录",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = true, dataType = "String")
	@PostMapping(value = "/p/withdrawList")
	public ResultDto<AppPageSupport<AppWithdrawalLogDto>> withdrawList(String curPageNO) {
		try {
			String userId = loginedUserService.getUser().getUserId();
			AppPageSupport<AppWithdrawalLogDto> ps = appWithdrawalLogService.getPdCashLog(curPageNO, 8,userId);
			return ResultDtoManager.success(ps);
		} catch (Exception e) {
			LOGGER.error("获取提现记录出错：{}",e.getMessage());
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 佣金明细
	 * 
	 */
	@ApiOperation(value = "佣金明细", httpMethod = "POST", notes = "佣金明细",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = true, dataType = "String")
	@PostMapping(value = "/p/myCommisDetailList")
	public ResultDto<AppPageSupport<AppCommisChangeLogDto>> myCommisDetailList(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		try {
			String userId = loginedUserService.getUser().getUserId();
			// 查询奖励记录
			int pageSize = 10;
			AppPageSupport<AppCommisChangeLogDto> ps = appCommisChangeLogService.getCommisChangeLog(pageSize, userId,curPageNO);
			return ResultDtoManager.success(ps);
		} catch (Exception e) {
			LOGGER.error("获取佣金明细记录出错：{}",e.getMessage());
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 我推广的好友
	 * 
	 * @return
	 */
	@ApiOperation(value = "我推广的好友", httpMethod = "POST", notes = "我推广的好友",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = true, dataType = "String")
	@PostMapping(value = "/p/myPromoterUser")
	public ResultDto<AppMyPromoterUserDetailDto> myPromoterUser(String curPageNO) {
		try {
			LoginedUserInfo appToken = loginedUserService.getUser();
			String userName = appToken.getUserName();
			String userId = appToken.getUserId();
			int pageSize = 20;
			
			UserCommis userCommis = new UserCommis();
			AppMyPromoterUserDetailDto myPromoterUserDto = new AppMyPromoterUserDetailDto();
			AppPageSupport<AppMyPromoterUserDto> ps = appUserCommisService.getUserCommis(curPageNO, userName, userCommis, pageSize);
			myPromoterUserDto.setAppMyPromoterUserDtoList(ps);
			// 总记录
			myPromoterUserDto.setTotalUser(ps.getTotal());
			// 查询本月发展会员数
			Date startTime = DateUtils.getMonthStartTime();// 获得本月开始时间
			Date endTime = new Date();
			Long monthTotal = userCommisService.getMonthUser(startTime, endTime, userName);
			myPromoterUserDto.setMonthTotal(monthTotal);

			UserCommis originUserCommis = userCommisService.getUserCommis(userId);
			myPromoterUserDto.setFirstDistCommis(originUserCommis.getFirstDistCommis());
			return ResultDtoManager.success(myPromoterUserDto);
		} catch (Exception e) {
			LOGGER.error("获取我推广的好友页面数据出错：{}",e.getMessage());
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 获取生成分销员推广海报相关的数据
	 * @return
	 */
	@ApiOperation(value = "获取生成分销员推广的小程序码", notes = "获取生成分销员推广的小程序码", httpMethod="POST")
	@PostMapping("/p/getPromoteWxACode")
	public ResultDto<String> getPromotePostersDto(){
		
		LoginedUserInfo appToken = loginedUserService.getUser();
		
		try {
			
			/** 获取用户头像,昵称信息 */
			UserCommis userCommis = userCommisService.getUserCommisByUserId(appToken.getUserId());
			
			/** 获取小程序码 */
			String wxACode = userCommis.getWxAcode();

			if(AppUtils.isBlank(wxACode)){
				String  accessToken = weixinMpTokenService.getAccessToken();
				String scene = "parentUserName=" + userCommis.getUserName();
				String page = "marketingModules/myExtension/applySuccess";
				WXACodeResultDto wxACodeResult = WeixinMiniProgramApi.getWXACodeUnlimit(accessToken, scene, page, null, null, null, null);
				if(!wxACodeResult.isSuccess()){//说明失败
					
					WXACodeResultDto.Errormsg error = wxACodeResult.getErrormsg();
					if(null == error){
						return ResultDtoManager.fail("获取二维码错误, 未知错误!");
					}
					
					return ResultDtoManager.fail(error.getErrmsg());
				}
					
				//说明成功返回二维码图片
				// TODO 这里不应该这么去实现, 会太多图片上传到图片服务器
				ByteArrayOutputStream output = wxACodeResult.getOutputStream();
				FeignMultipartFile file = new FeignMultipartFile( "WX_A_CODE_PROMOTER" + userCommis.getUserId()+".jpg",output.toByteArray());
				wxACode = attachmentManager.upload(file);
				
				if(AppUtils.isBlank(wxACode)){
					return ResultDtoManager.fail("获取二维码错误, 未知错误!");
				}
				
				/** 更新到用户分销员 */
				userCommis.setWxAcode(wxACode);
				userCommisService.updateUserCommis(userCommis);
			}
			
			return ResultDtoManager.success(wxACode);
			
		} catch (Exception e) {
			LOGGER.error("获取生成分销员推广的小程序码: ", e);
			return ResultDtoManager.fail("未知错误!");
		}
	}
	
}
