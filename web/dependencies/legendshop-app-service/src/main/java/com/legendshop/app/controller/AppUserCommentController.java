/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.legendshop.business.service.weixin.WeixinMiniProgramApi;
import com.legendshop.model.dto.weixin.WeiXinMsg;
import com.legendshop.spi.service.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.legendshop.app.constants.StatusCodes;
import com.legendshop.app.dto.UserCommentDetailDto;
import com.legendshop.app.service.AppCommentService;
import com.legendshop.base.config.SystemParameterUtil;
import com.legendshop.model.constant.Constants;
import com.legendshop.model.constant.ProdCommStatusEnum;
import com.legendshop.model.constant.SendIntegralRuleEnum;
import com.legendshop.model.dto.ProductCommentDto;
import com.legendshop.model.dto.UserAddCommParamsDto;
import com.legendshop.model.dto.UserAddCommentDto;
import com.legendshop.model.dto.UserCommParamsDto;
import com.legendshop.model.dto.UserCommentDto;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.AppUserAddCommParamsDto;
import com.legendshop.model.dto.app.AppUserCommParamsDto;
import com.legendshop.model.dto.app.ResultDto;
import com.legendshop.model.dto.app.ResultDtoManager;
import com.legendshop.model.dto.app.UserCommentProdDto;
import com.legendshop.model.dto.user.LoginedUserInfo;
import com.legendshop.model.entity.ProdAddComm;
import com.legendshop.model.entity.Product;
import com.legendshop.model.entity.ProductComment;
import com.legendshop.util.AppUtils;
import com.legendshop.util.SafeHtml;
import com.legendshop.web.helper.IPHelper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 用户评论. 
 */
@RestController
@RequestMapping(value = "/p")
@Api(tags="评论管理",value="我的评论管理")
public class AppUserCommentController{
	/** The log. */
	private final Logger LOGGER = LoggerFactory.getLogger(AppUserCommentController.class);
	
	@Autowired
	private AppCommentService appCommentService;
	
	@Autowired
	private ProductCommentService productCommentService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
    private SensitiveWordService sensitiveWordService; //敏感字过滤
	
	@Autowired
	private IntegralService integralService;
	
	@Autowired
	private SystemParameterUtil systemParameterUtil;
	
	/**
	 * 获取已经登录的用户信息
	 */
	@Autowired
	private LoginedUserService loginedUserService;

	@Autowired
	private WeixinMpTokenService weixinMpTokenService;

	/**
	 * 查询用户的评论
	 * @param curPageNO
	 * @param userName
	 * @return
	 */
	@ApiIgnore
	//@ApiOperation(value = "我的评论", httpMethod = "POST", notes = "查询我的评论",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = false, dataType = "string")
	@PostMapping(value="/comments/query")
	public ResultDto<AppPageSupport<UserCommentProdDto>> query(HttpServletRequest request, HttpServletResponse response, String curPageNO) {
		
		String userName = loginedUserService.getUser().getUserName();
		if(AppUtils.isBlank(userName)){
			ResultDtoManager.fail(-1,"请先登录");
		}
		AppPageSupport<UserCommentProdDto> result = appCommentService.queryComments(curPageNO, userName);
		return ResultDtoManager.success(result);
	}
	
	/** 
	 * 我的评论商品列表
	 * @param request
	 * @param response
	 * @return
	 */
	@ApiOperation(value = "我的评论", httpMethod = "POST", notes = "查询我的评论",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "curPageNO", value = "当前页码", required = false, dataType = "string"),
		@ApiImplicitParam(paramType="query", name = "state", value = "评论状态： 0 未评价 1 已评价", required = false, dataType = "string")
	})
	@PostMapping(value="/userComments")
	public ResultDto<AppPageSupport<ProductCommentDto>> prodComment(String curPageNO, String state) {
		String userName = loginedUserService.getUser().getUserName();
		if(AppUtils.isBlank(userName)){
			return ResultDtoManager.fail(-1,"请先登录");
		}
		AppPageSupport<ProductCommentDto> result = appCommentService.prodCommentState(curPageNO, state, userName);
		return ResultDtoManager.success(result);
	}
	
	/**
	 * 对商品进行评论
	 * @param prodId
	 * @param score
	 * @param content
	 * @return
	 */
	@ApiOperation(value = "发表评论", httpMethod = "POST", notes = "发表评论,发表成功需要审核返回SUCCESS,不需要审核返回OK;发表失败则返回-1，展示提示信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/app/comments/save")
    public ResultDto<String> saveComments(HttpServletRequest request, UserCommParamsDto params){
		try{
			LoginedUserInfo token = loginedUserService.getUser();
			String userId = token.getUserId();
			String userName = token.getUserName();
			
			//检查评分是否合法
			Integer score = params.getProdScore();
			Integer shopScore = params.getShopScore();
			Integer logisticsScore = params.getLogisticsScore();
			if(AppUtils.isBlank(score) || score <= 0){
				return ResultDtoManager.fail(-1,"对不起, 宝贝评分不能为空, 请选择评分!");
			}
			if(AppUtils.isBlank(score) || score <= 0 || score > 5){
				return ResultDtoManager.fail(-1,"对不起, 您的宝贝评分不合法, 请重新选择!");
			}
			if(AppUtils.isBlank(shopScore) || shopScore <= 0 ){
				return ResultDtoManager.fail(-1,"对不起, 卖家评分不能为空, 请选择评分!");
			}
			if(AppUtils.isBlank(shopScore) || shopScore <= 0 || shopScore > 5){
				return ResultDtoManager.fail(-1,"对不起, 您的卖家评分不合法, 请重新选择!");
			}
			if(AppUtils.isBlank(logisticsScore) || logisticsScore <= 0){
				return ResultDtoManager.fail(-1,"对不起, 物流评分不能为空, 请选择评分!");
			}
			if(AppUtils.isBlank(logisticsScore) || logisticsScore <= 0 || logisticsScore > 5){
				return ResultDtoManager.fail(-1,"对不起, 您的物流评分不合法, 请重新选择!");
			}
			
			//敏感字过滤
			//Set<String> findwords = sensitiveWordService.checkSensitiveWords(params.getContent());
			//if(AppUtils.isNotBlank(findwords)){
			//	return ResultDtoManager.fail(-1,"亲, 您提交的内容含有敏感词 " + findwords + ", 请更正再提交！");
			//}
			String  accessToken = weixinMpTokenService.getAccessToken();
			if (AppUtils.isNotBlank(accessToken) && StringUtils.isNotBlank(params.getContent())){
				WeiXinMsg checkTitResult = WeixinMiniProgramApi.checkMsg(accessToken, params.getContent());
				if(null == checkTitResult || !"ok".equals(checkTitResult.getErrmsg())){
					return ResultDtoManager.fail("亲, 您提交的内容含有敏感词 , 请更正再提交！");
				}
			}
			Long prodId = params.getProdId();
			
			Product prod = productService.getProductById(prodId);
			
			if(null == prod){
				return ResultDtoManager.fail(-1,"亲, 您评论的商品不存在哦!");
			}

			//判断是否评论
			Boolean rs = productCommentService.findByProdIdandUserIdandItemId(prodId, userId, params.getSubItemId());
			if(rs){
				return ResultDtoManager.fail(-1,"亲, 您已经评论过该商品了 请勿重复评论!");
			}

			//查询用户是否 可以评论此商品
			boolean canComm = productCommentService.canCommentThisProd(params.getProdId(), params.getSubItemId(), userId);
			if(!canComm){
				return ResultDtoManager.fail(-1,"亲, 您不能评价该商品哦!");
			}
			Long subItemId = params.getSubItemId();
			
			ProductComment productComment = new ProductComment();
			
			// 处理匿名评论
			Map<String, Object> handleResult = productCommentService.handleAnonymousComments(userName, params.getIsAnonymous());
			productComment.setIsAnonymous((Integer)handleResult.get("isAnonymous"));
			productComment.setUserName((String)handleResult.get("userName"));
			
			// 处理评论内容
			SafeHtml safeHtml = new SafeHtml();
			String html = safeHtml.makeSafe(params.getContent().trim());
			StringBuilder nicksb = new StringBuilder();
			int l = html.length();
			for (int i = 0; i < l; i++) {
				char _s = html.charAt(i);
				if (isEmojiCharacter(_s)) {
					nicksb.append(_s);
				}
			}
			productComment.setContent(nicksb.toString());   //评论内容
			
			List<String> photos = params.getPhotoList();
			if(AppUtils.isNotBlank(photos)){
				if(photos.size() > 4){
					return ResultDtoManager.fail(-1,"对不起, 评论图片不能超过4张!");
				}
				StringBuilder photosStr = new StringBuilder();
				for(int i = 0, len = photos.size(); i < len; i++){
					
					String photoPath = photos.get(i);
					
					//评论图片1
					if (AppUtils.isNotBlank(photoPath)) {
						if(i == (len - 1)){
							photosStr.append(photoPath); 
						}else{
							photosStr.append(photoPath).append(",");
						}
					}
				}
				productComment.setPhotos(photosStr.toString());
			}
			
			productComment.setUserId(userId);
			productComment.setSubItemId(subItemId);
			productComment.setProdId(prodId);
			productComment.setOwnerName(prod.getUserName());
			
			/** 是否需要审核 */
			Boolean needAudit = systemParameterUtil.isCommentNeedReview();
			String result = "";
			if(needAudit){
				productComment.setStatus(ProdCommStatusEnum.WAIT_AUDIT.value());
				result = "SUCCESS";
			}else{
				productComment.setStatus(ProdCommStatusEnum.SUCCESS.value());
				result = Constants.SUCCESS;
			}
			productComment.setScore(score);//宝贝评论得分
			productComment.setShopScore(shopScore);//卖家评分
			productComment.setLogisticsScore(logisticsScore);//物流评分
			productComment.setPostip(IPHelper.getIpAddr(request));
			productComment.setUsefulCounts(0);//有用的计数
			productComment.setReplayCounts(0);//回复的次数
			productComment.setIsReply(false);
			productComment.setAddtime(new Date());
			
			productCommentService.saveProductComment(productComment);
			
			//评论商品送积分
			integralService.addScore(userId,SendIntegralRuleEnum.ORDER_COMMENT);
			
			return ResultDtoManager.success(result);
		}catch (Exception e) {
			LOGGER.error("{}",e);
			LOGGER.error("用户发表评论异常!{}", e.getMessage());
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 追加评论 
	 * @param params
	 * @return
	 */
	@ApiOperation(value = "追加评论 ", httpMethod = "POST", notes = "追加评论 ,发表成功返回OK;发表失败则返回-1，展示提示信息",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/app/comments/addComm")
	public ResultDto<String> saveAddComm(HttpServletRequest request,UserAddCommParamsDto params) {
		try{
			String userId = loginedUserService.getUser().getUserId();
			
			if(AppUtils.isBlank(params.getProdCommId())){
				return ResultDtoManager.fail(-1,"对不起, 您提交的参数不合法!");
			}
			
			if(AppUtils.isBlank(params.getContent())){
				return ResultDtoManager.fail(-1,"亲, 评论内容不能为空哦!");
			}
			
			//敏感字过滤
			//Set<String> findwords = sensitiveWordService.checkSensitiveWords(params.getContent());
			//if(AppUtils.isNotBlank(findwords)){
			//	return ResultDtoManager.fail(-1,"亲, 您提交的内容含有敏感词 " + findwords + ", 请更正再提交！");
			//}

			String  accessToken = weixinMpTokenService.getAccessToken();
			if(StringUtils.isNotBlank(accessToken) && StringUtils.isNotBlank(params.getContent())) {
				WeiXinMsg checkTitResult = WeixinMiniProgramApi.checkMsg(accessToken, params.getContent());
				if(checkTitResult == null || !"ok".equals(checkTitResult.getErrmsg())){
					return ResultDtoManager.fail("亲, 您提交的内容含有敏感词 , 请更正再提交！");
				}
			}

			//查询用户是否可以追评
			String canAdd = productCommentService.isCanAddComment(params.getProdCommId(), userId);
			if(!Constants.SUCCESS.equals(canAdd)){
				return ResultDtoManager.fail(-1,canAdd);
			}
			
			ProdAddComm prodAddComm = new ProdAddComm();
			
			// 处理评论内容
			SafeHtml safeHtml = new SafeHtml();
			String html = safeHtml.makeSafe(params.getContent().trim());
			StringBuilder nicksb = new StringBuilder();
			int l = html.length();
			for (int i = 0; i < l; i++) {
				char _s = html.charAt(i);
				if (isEmojiCharacter(_s)) {
					nicksb.append(_s);
				}
			}
			prodAddComm.setContent(nicksb.toString());   //评论内容
			
			List<String> photos = params.getPhotoList();
			if(AppUtils.isNotBlank(photos)){
				if(photos.size() > 4){
					return ResultDtoManager.fail(-1,"对不起, 评论图片不能超过4张!");
				}
				StringBuilder photosStr = new StringBuilder();
				for(int i = 0, len = photos.size(); i < len; i++){
					
					String photoPath = photos.get(i);
					
					//评论图片1
					if (AppUtils.isNotBlank(photoPath)) {
						if(i == (len - 1)){
							photosStr.append(photoPath); 
						}else{
							photosStr.append(photoPath).append(",");
						}
					}
				}
				prodAddComm.setPhotos(photosStr.toString());
			}
			
			prodAddComm.setProdCommId(params.getProdCommId());
			
			/** 是否需要审核 */
			Boolean needAudit = systemParameterUtil.isCommentNeedReview();
			if(needAudit){
				prodAddComm.setStatus(ProdCommStatusEnum.WAIT_AUDIT.value());
			}else{
				prodAddComm.setStatus(ProdCommStatusEnum.SUCCESS.value());
			}
			prodAddComm.setPostip(IPHelper.getIpAddr(request));
			prodAddComm.setIsReply(false);
			prodAddComm.setCreateTime(new Date());
			
			productCommentService.addProdComm(prodAddComm);
			
			return ResultDtoManager.success();
		}catch (Exception e) {
			LOGGER.error("追加评论异常!", e);
			return ResultDtoManager.fail();
		}
	}
	
	/**
	 * 跳转到评价页面
	 */
	@ApiOperation(value = "获取评论页面数据", httpMethod = "POST", notes = "获取评论页面数据",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParams({
		@ApiImplicitParam(paramType="query", name = "prodId", value = "商品Id", required = true, dataType = "Long"),
		@ApiImplicitParam(paramType="query", name = "subItemId", value = "订单项Id", required = true, dataType = "Long")
	})
	@PostMapping(value="/comment/publish")
	public ResultDto<UserCommentDto> toPublishCommentPage(HttpServletRequest request, HttpServletResponse response, 
		 Long prodId, Long subItemId) {
		String userId = loginedUserService.getUser().getUserId();
		UserCommentDto userCommentDto = productCommentService.getUserCommentDto(prodId, subItemId, userId);
		
        return ResultDtoManager.success(userCommentDto);
	}
	
	/**
	 * 获取追加评论页面数据
	 */
	@ApiOperation(value = "获取追加评论页面数据", httpMethod = "POST", notes = "获取追加评论页面数据",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "prodCommId", value = "评论Id", required = true, dataType = "Long")
	@PostMapping(value="/comment/append/")
	public ResultDto<UserAddCommentDto> toAppendCommentPage(Long prodCommId) {
		String userId = loginedUserService.getUser().getUserId();
		UserAddCommentDto userAddCommentDto = appCommentService.getUserAddCommentDto(prodCommId, userId);
        return ResultDtoManager.success(userAddCommentDto);
	}
	
	/**
	 * 发表评论 TODO 通过base64编码方式上传图片需要改造为 文件流
	 * @param params
	 * @return
	 */
	@ApiIgnore
	//@ApiOperation(value = "发表评论", httpMethod = "POST", notes = "发表评论",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/comment/save")
	public ResultDto<String> save(HttpServletRequest request, AppUserCommParamsDto params) {
		try{
			LoginedUserInfo token = loginedUserService.getUser();
			String userId = token.getUserId();
			String userName = token.getUserName();
			
			String checkResult = checkCommentParam(params);
			if(!Constants.SUCCESS.equals(checkResult)){
				return ResultDtoManager.fail(-1, checkResult);
			}
			
			//敏感字过滤
			Set<String> findwords = sensitiveWordService.checkSensitiveWords(params.getContent());
			if(AppUtils.isNotBlank(findwords)){
				return ResultDtoManager.fail(-1, "亲, 您提交的内容含有敏感词 " + findwords + ", 请更正再提交！");
			}
			
			Long prodId = params.getProdId();
			
			Product prod = productService.getProductById(prodId);
			
			if(null == prod){
				return ResultDtoManager.fail(-1, "亲, 您评论的商品不存在哦!");
			}
			
			//查询用户是否 可以评论此商品
			boolean canComm = productCommentService.canCommentThisProd(params.getProdId(), params.getSubItemId(), userId);
			if(!canComm){
				return ResultDtoManager.fail(-1, "亲, 您不能评价该商品哦!");
			}
			
			Long subItemId = params.getSubItemId();
			
			ProductComment productComment = new ProductComment();
			
			// 处理匿名评论
			Map<String, Object> handleResult = productCommentService.handleAnonymousComments(userName, params.getIsAnonymous());
			productComment.setIsAnonymous((Integer)handleResult.get("isAnonymous"));
			productComment.setUserName((String)handleResult.get("userName"));
			
			// 处理评论内容
			SafeHtml safeHtml = new SafeHtml();
			String html = safeHtml.makeSafe(params.getContent().trim());
			StringBuilder nicksb = new StringBuilder();
			int l = html.length();
			for (int i = 0; i < l; i++) {
				char _s = html.charAt(i);
				if (isEmojiCharacter(_s)) {
					nicksb.append(_s);
				}
			}
			productComment.setContent(nicksb.toString());   //评论内容
			
			String photos = params.getPhotos();
			/*if(AppUtils.isNotBlank(photos)){
				List<AppImageFileUploadDto> imageFileUploads = JSONObject.parseArray(photos, AppImageFileUploadDto.class);
				if(AppUtils.isNotBlank(imageFileUploads)){
					if(imageFileUploads.size() > 9){
						return ResultDtoManager.fail(-1, "对不起, 评论图片不能超过9张!");
					}
					StringBuilder photosStr = new StringBuilder();
					for(int i = 0, len = imageFileUploads.size(); i < len; i++){
						// 对base64数据进行解码 生成 字节数组，不能直接用Base64.decode（）；进行解密
						AppImageFileUploadDto imageFileUpload = imageFileUploads.get(i);
						String fileName = imageFileUpload.getFileName();
						String base64Code = imageFileUpload.getBase64Code();
						byte[] image = FileUtil.decodeBase64(base64Code);
						
						String result = checkSubmitFile(image, fileName); // 图片凭证验证结果
						if (AppUtils.isNotBlank(result)) {
							return ResultDtoManager.fail(-1, result);
						}
						
						//评论图片1
						if (AppUtils.isNotBlank(image) && AppUtils.isNotBlank(fileName)) {
							String photoPath = attachmentManager.upload(new ByteArrayInputStream(image), fileName, image.length);
							if(i == (len - 1)){
								photosStr.append(photoPath);
							}else{
								photosStr.append(photoPath).append(",");
							}
						}
					}
					productComment.setPhotos(photosStr.toString());
				}
			}*/
			if(AppUtils.isNotBlank(photos)){
				productComment.setPhotos(photos.toString());
			}
			productComment.setUserId(userId);
			productComment.setSubItemId(subItemId);
			productComment.setProdId(prodId);
			productComment.setOwnerName(prod.getUserName());
			/** 是否需要审核 */
			Boolean needAudit =systemParameterUtil.isCommentNeedReview();
			if(needAudit){
				productComment.setStatus(ProdCommStatusEnum.WAIT_AUDIT.value());
			}else{
				productComment.setStatus(ProdCommStatusEnum.SUCCESS.value());
			}
			productComment.setScore(params.getProdScore());//评论得分
			productComment.setShopScore(params.getShopScore());//卖家评分
			productComment.setLogisticsScore(params.getLogisticsScore());//物流评分
			productComment.setPostip(IPHelper.getIpAddr(request));
			productComment.setUsefulCounts(0);//有用的计数
			productComment.setReplayCounts(0);//回复的次数
			productComment.setIsReply(false);
			productComment.setAddtime(new Date());
			
			productCommentService.saveProductComment(productComment);
			
			//评论商品送积分
			integralService.addScore(userId,SendIntegralRuleEnum.ORDER_COMMENT);
			
			return ResultDtoManager.success();
		}catch (Exception e) {
			LOGGER.error("发表评论异常!", e);
			return ResultDtoManager.fail(StatusCodes.UNKNOWN_ERROR, "对不起, 评论失败, 请联系客服!");
		}
		
	}
	
	@PostMapping(value="/vueComments/save")
	@ApiIgnore
	public ResultDto<String> vueComments(HttpServletRequest request, AppUserCommParamsDto params) {
		try{
			String userId = loginedUserService.getUser().getUserId();
			String userName = loginedUserService.getUser().getUserName();
			
			String checkResult = checkCommentParam(params);
			if(!Constants.SUCCESS.equals(checkResult)){
				return ResultDtoManager.fail(-1, checkResult);
			}
			
			//敏感字过滤
			Set<String> findwords = sensitiveWordService.checkSensitiveWords(params.getContent());
			if(AppUtils.isNotBlank(findwords)){
				return ResultDtoManager.fail(-1, "亲, 您提交的内容含有敏感词 " + findwords + ", 请更正再提交！");
			}
			
			Long prodId = params.getProdId();
			
			Product prod = productService.getProductById(prodId);
			
			if(null == prod){
				return ResultDtoManager.fail(-1, "亲, 您评论的商品不存在哦!");
			}
			
			//查询用户是否 可以评论此商品
			boolean canComm = productCommentService.canCommentThisProd(params.getProdId(), params.getSubItemId(), userId);
			if(!canComm){
				return ResultDtoManager.fail(-1, "亲, 您不能评价该商品哦!");
			}
			
			Long subItemId = params.getSubItemId();
			
			ProductComment productComment = new ProductComment();
			
			// 处理匿名评论
			Map<String, Object> handleResult = productCommentService.handleAnonymousComments(userName, params.getIsAnonymous());
			productComment.setIsAnonymous((Integer)handleResult.get("isAnonymous"));
			productComment.setUserName((String)handleResult.get("userName"));
			
			// 处理评论内容
			SafeHtml safeHtml = new SafeHtml();
			String html = safeHtml.makeSafe(params.getContent().trim());
			StringBuilder nicksb = new StringBuilder();
			int l = html.length();
			for (int i = 0; i < l; i++) {
				char _s = html.charAt(i);
				if (isEmojiCharacter(_s)) {
					nicksb.append(_s);
				}
			}
			productComment.setContent(nicksb.toString());   //评论内容
			
			String[] photos = params.getVuePhotos();
			if(AppUtils.isNotBlank(photos)){
				if(photos.length > 9){
					return ResultDtoManager.fail(-1, "对不起, 评论图片不能超过9张!");
				}
				StringBuilder photosStr = new StringBuilder();
				for(int i=0;i<photos.length;i++){
					if(i == (photos.length - 1)){
						photosStr.append(photos[i]);
					}else{
						photosStr.append(photos[i]).append(",");
					}
				}
				productComment.setPhotos(photosStr.toString());
			}
			
			productComment.setUserId(userId);
			productComment.setSubItemId(subItemId);
			productComment.setProdId(prodId);
			productComment.setOwnerName(prod.getUserName());
			/** 是否需要审核 */
			Boolean needAudit = systemParameterUtil.isCommentNeedReview();
			if(needAudit){
				productComment.setStatus(ProdCommStatusEnum.WAIT_AUDIT.value());
			}else{
				productComment.setStatus(ProdCommStatusEnum.SUCCESS.value());
			}
			productComment.setScore(params.getProdScore());//评论得分
			productComment.setShopScore(params.getShopScore());//卖家评分
			productComment.setLogisticsScore(params.getLogisticsScore());//物流评分
			productComment.setPostip(IPHelper.getIpAddr(request));
			productComment.setUsefulCounts(0);//有用的计数
			productComment.setReplayCounts(0);//回复的次数
			productComment.setIsReply(false);
			productComment.setAddtime(new Date());
			
			productCommentService.saveProductComment(productComment);
			
			//评论商品送积分
			integralService.addScore(userId,SendIntegralRuleEnum.ORDER_COMMENT);
			
			return ResultDtoManager.success();
		}catch (Exception e) {
			LOGGER.error("发表评论异常!", e);
			return ResultDtoManager.fail(StatusCodes.UNKNOWN_ERROR, "对不起, 评论失败, 请联系客服!");
		}
		
	}
	
	/**
	 * 
	 * @return
	 */
	@ApiIgnore
	//@ApiOperation(value = "评论详情", httpMethod = "POST", notes = "评论详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ApiImplicitParam(paramType="query", name = "commId", value = "评论id", required = false, dataType = "Long")
	@PostMapping(value="/comments/detail")
	public ResultDto<UserCommentDetailDto> getCommDetail(Long commId) {
		
		String userId = loginedUserService.getUser().getUserId();
		UserCommentDetailDto result = appCommentService.getCommDetail(commId, userId);
		
		return ResultDtoManager.success(result);
	}
	
	/**
	 * 追加评论 TODO 通过base64编码方式上传图片需要改造为 文件流
	 * @param params
	 * @return
	 */
	@ApiIgnore
	//@ApiOperation(value = "追加评论", httpMethod = "POST", notes = "评论详情",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@PostMapping(value="/comments/addComm")
	public ResultDto<String> saveAddComm(HttpServletRequest request, AppUserAddCommParamsDto params) {
		try{
			String userId = loginedUserService.getUser().getUserId();
			
			if(AppUtils.isBlank(params.getProdCommId())){
				return ResultDtoManager.fail(-1, "对不起, 您提交的参数不合法!");
			}
			
			if(AppUtils.isBlank(params.getContent())){
				return ResultDtoManager.fail(-1, "亲, 评论内容不能为空哦!");
			}
			
			//敏感字过滤
			Set<String> findwords = sensitiveWordService.checkSensitiveWords(params.getContent());
			if(AppUtils.isNotBlank(findwords)){
				return ResultDtoManager.fail(-1, "亲, 您提交的内容含有敏感词 " + findwords + ", 请更正再提交！");
			}
			
			//查询用户是否可以追评
			String canAdd = productCommentService.isCanAddComment(params.getProdCommId(), userId);
			if(!Constants.SUCCESS.equals(canAdd)){
				return ResultDtoManager.fail(-1, canAdd);
			}
			
			ProdAddComm prodAddComm = new ProdAddComm();
			
			// 处理评论内容
			SafeHtml safeHtml = new SafeHtml();
			String html = safeHtml.makeSafe(params.getContent().trim());
			StringBuilder nicksb = new StringBuilder();
			int l = html.length();
			for (int i = 0; i < l; i++) {
				char _s = html.charAt(i);
				if (isEmojiCharacter(_s)) {
					nicksb.append(_s);
				}
			}
			prodAddComm.setContent(nicksb.toString());   //评论内容
			
			String photos = params.getPhotos();
			/*if(AppUtils.isNotBlank(photos)){
				List<AppImageFileUploadDto> imageFileUploads = JSONObject.parseArray(photos, AppImageFileUploadDto.class);
				if(AppUtils.isNotBlank(imageFileUploads)){
					if(imageFileUploads.size() > 9){
						return ResultDtoManager.fail(-1, "对不起, 评论图片不能超过9张!");
					}
					StringBuilder photosStr = new StringBuilder();
					for(int i = 0, len = imageFileUploads.size(); i < len; i++){
						// 对base64数据进行解码 生成 字节数组，不能直接用Base64.decode（）；进行解密
						AppImageFileUploadDto imageFileUpload = imageFileUploads.get(i);
						String fileName = imageFileUpload.getFileName();
						String base64Code = imageFileUpload.getBase64Code();
						byte[] image = FileUtil.decodeBase64(base64Code);
						
						String result = checkSubmitFile(image, fileName); // 图片凭证验证结果
						if (AppUtils.isNotBlank(result)) {
							return ResultDtoManager.fail(-1, result);
						}
						
						//评论图片1
						if (AppUtils.isNotBlank(image) && AppUtils.isNotBlank(fileName)) {
							String photoPath = attachmentManager.upload(new ByteArrayInputStream(image), fileName, image.length);
							if(i == (len - 1)){
								photosStr.append(photoPath);
							}else{
								photosStr.append(photoPath).append(",");
							}
						}
					}
					prodAddComm.setPhotos(photosStr.toString());
				}
			}*/
			
			if(AppUtils.isNotBlank(photos)){
							
							prodAddComm.setPhotos(photos.toString());
						}
			
			
			prodAddComm.setProdCommId(params.getProdCommId());
			/** 是否需要审核 */
			Boolean needAudit = systemParameterUtil.isCommentNeedReview();
			if(needAudit){
				prodAddComm.setStatus(ProdCommStatusEnum.WAIT_AUDIT.value());
			}else{
				prodAddComm.setStatus(ProdCommStatusEnum.SUCCESS.value());
			}
			prodAddComm.setPostip(IPHelper.getIpAddr(request));
			prodAddComm.setIsReply(false);
			prodAddComm.setCreateTime(new Date());
			
			productCommentService.addProdComm(prodAddComm);
			
			return ResultDtoManager.success();
		}catch (Exception e) {
			LOGGER.error("追加评论异常!", e);
			return ResultDtoManager.fail(StatusCodes.UNKNOWN_ERROR, "对不起, 追加评论失败, 请联系客服!");
		}
		
	}
	
	@PostMapping(value="/comments/addVueComm")
	@ApiIgnore
	public ResultDto<String> saveAddVueComm(HttpServletRequest request, AppUserAddCommParamsDto params) {
		try{
			String userId = loginedUserService.getUser().getUserId();
			
			if(AppUtils.isBlank(params.getProdCommId())){
				return ResultDtoManager.fail(-1, "对不起, 您提交的参数不合法!");
			}
			
			if(AppUtils.isBlank(params.getContent())){
				return ResultDtoManager.fail(-1, "亲, 评论内容不能为空哦!");
			}
			
			//敏感字过滤
			Set<String> findwords = sensitiveWordService.checkSensitiveWords(params.getContent());
			if(AppUtils.isNotBlank(findwords)){
				return ResultDtoManager.fail(-1, "亲, 您提交的内容含有敏感词 " + findwords + ", 请更正再提交！");
			}
			
			//查询用户是否可以追评
			String canAdd = productCommentService.isCanAddComment(params.getProdCommId(), userId);
			if(!Constants.SUCCESS.equals(canAdd)){
				return ResultDtoManager.fail(-1, canAdd);
			}
			
			ProdAddComm prodAddComm = new ProdAddComm();
			
			// 处理评论内容
			SafeHtml safeHtml = new SafeHtml();
			String html = safeHtml.makeSafe(params.getContent().trim());
			StringBuilder nicksb = new StringBuilder();
			int l = html.length();
			for (int i = 0; i < l; i++) {
				char _s = html.charAt(i);
				if (isEmojiCharacter(_s)) {
					nicksb.append(_s);
				}
			}
			prodAddComm.setContent(nicksb.toString());   //评论内容
			
			String[] photos = params.getVuePhotos();
			if(AppUtils.isNotBlank(photos)){
				if(photos.length > 9){
					return ResultDtoManager.fail(-1, "对不起, 评论图片不能超过9张!");
				}
				StringBuilder photosStr = new StringBuilder();
				for(int i=0;i<photos.length;i++){
					if(i == (photos.length - 1)){
						photosStr.append(photos[i]);
					}else{
						photosStr.append(photos[i]).append(",");
					}
				}
				prodAddComm.setPhotos(photosStr.toString());
			}
			
			prodAddComm.setProdCommId(params.getProdCommId());
			/** 是否需要审核 */
			Boolean needAudit = systemParameterUtil.isCommentNeedReview();
			if(needAudit){
				prodAddComm.setStatus(ProdCommStatusEnum.WAIT_AUDIT.value());
			}else{
				prodAddComm.setStatus(ProdCommStatusEnum.SUCCESS.value());
			}
			prodAddComm.setPostip(IPHelper.getIpAddr(request));
			prodAddComm.setIsReply(false);
			prodAddComm.setCreateTime(new Date());
			
			productCommentService.addProdComm(prodAddComm);
			
			return ResultDtoManager.success();
		}catch (Exception e) {
			LOGGER.error("追加评论异常!", e);
			return ResultDtoManager.fail(StatusCodes.UNKNOWN_ERROR, "对不起, 追加评论失败, 请联系客服!");
		}
		
	}
	
	
	/**
	 * 删除评论
	 * @param id the id
	 * @return the string
	 */
	@ApiIgnore
	@PostMapping(value="/comments/delete")
	public ResultDto<T> delete(HttpServletRequest request, HttpServletResponse response, Long id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * 是否是表情符号
	 * @param codePoint
	 * @return
	 */
	private boolean isEmojiCharacter(char codePoint) {
        return (codePoint == 0x0) || (codePoint == 0x9) 
        		|| (codePoint == 0xA) || (codePoint == 0xD) 
        		|| ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) 
                || ((codePoint >= 0xE000) && (codePoint <= 0xFFFD)) 
                || ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF));
    }
	
	/**
	 * @Description: 检查评论参数
	 * @date 2016-7-29 
	 */
	public String checkCommentParam(AppUserCommParamsDto params){
		if(AppUtils.isBlank(params.getSubItemId()) || AppUtils.isBlank(params.getProdId())){
			return "对不起, 您提交的参数不合法!";
		}
		
		//检查评分是否合法
		Integer score = params.getProdScore();
		Integer shopScore = params.getShopScore();
		Integer logisticsScore = params.getLogisticsScore();
		if(AppUtils.isBlank(score) || score <= 0 || score > 5){
			return "亲, 您的宝贝评分不合法, 请重新选择!";
		}
		if(AppUtils.isBlank(shopScore) || shopScore <= 0 || shopScore > 5){
			return "亲, 您的卖家评分不合法, 请重新选择!";
		}
		if(AppUtils.isBlank(logisticsScore) || logisticsScore <= 0 || logisticsScore > 5){
			return "亲, 您的物流评分不合法, 请重新选择!";
		}
		
		if(AppUtils.isBlank(params.getContent())){
			return "亲, 评论内容不能为空哦!";
		}
		
		return Constants.SUCCESS;
	}
	

}
