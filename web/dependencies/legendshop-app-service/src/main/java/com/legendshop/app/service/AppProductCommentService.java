/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.service;

import com.legendshop.model.entity.ProductCommentCategory;

/**
 * 产品评论服务
 */
public interface AppProductCommentService {

	public abstract ProductCommentCategory initProductCommentCategory(Long prodId);
	
}