package com.legendshop.app.service;

import com.legendshop.model.dto.app.AppPassportDto;
import com.legendshop.model.entity.Passport;
import com.legendshop.model.entity.User;

/**
 * @author quanzc
 * 第三方登录回调
 */
public interface AppLoginCallbackService {
	
	/**
	 * @Description: 绑定并注册
	 * @date 2016-7-20 
	 */
	public Passport autoRegBind(AppPassportDto passport);
	
	/**
	 * @Description: 获取user
	 * @date 2016-7-20 
	 */
	public User getUser(String userId);

}
