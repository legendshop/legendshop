package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.service.AppSubService;
import com.legendshop.business.dao.SubDao;
import com.legendshop.model.dto.SubCountsDto;
import com.legendshop.model.dto.app.AppSubCountsDto;
import com.legendshop.spi.service.SubService;
import com.legendshop.util.AppUtils;

/**
 * app 订单服务
 */
@Service("appSubService")
public class AppSubServiceImpl implements AppSubService {

	@Autowired
	private SubService subService;
	
	@Autowired
	private SubDao subDao;
	
	/**
	 * 获取各个状态的订单 统计数量
	 */
	@Override
	public List<AppSubCountsDto> querySubCounts(String userId) {
		
		List<SubCountsDto> form =  subService.querySubCounts(userId);
		
		//转成dto
		List<AppSubCountsDto> toList = new ArrayList<>();
		for (SubCountsDto subCountsDto : form) {
			
			if (AppUtils.isBlank(subCountsDto)) {
				return null;
			}
			AppSubCountsDto appSubCountsDto = convertToAppSubCountsDto(subCountsDto);
			
			toList.add(appSubCountsDto);
		}
		return toList;
	}
	
	//转Dto方法
	private AppSubCountsDto convertToAppSubCountsDto(SubCountsDto subCountsDto){
		  AppSubCountsDto appSubCountsDto = new AppSubCountsDto();
		  appSubCountsDto.setSubCounts(subCountsDto.getSubCounts());
		  appSubCountsDto.setStatus(subCountsDto.getStatus());
		  return appSubCountsDto;
		}

	@Override
	public Integer getUnCommentOrderCount(String userId) {
		String sql = "SELECT count(*) FROM (SELECT s.sub_id,b.sub_item_id as subItemId FROM ls_prod p, ls_sub s, ls_sub_item b "
				+ "WHERE p.prod_id = b.prod_id AND s.sub_number = b.sub_number "
				+ "AND s.status = 4 AND b.comm_sts = 0 AND s.user_id = ? ) pp "
				+ "LEFT JOIN ls_prod_comm c ON pp.subItemId = c.sub_item_id";
		return subDao.get(sql,Integer.class, userId);
	}
}
