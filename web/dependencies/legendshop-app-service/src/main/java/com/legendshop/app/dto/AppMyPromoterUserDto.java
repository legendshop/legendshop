/*
 * 
 * LegendShop 多用户商城系统
 * 
 *  版权所有,并保留所有权利。
 * 
 */
package com.legendshop.app.dto;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * app我的推广好友信息Dto.
 */
@ApiModel(value="app我的推广好友信息列表数据对象Dto")
public class AppMyPromoterUserDto{

	/** 用户名 */
	@ApiModelProperty(value="会员好友用户名")
	private String userName;
		
	/** 绑定上级时间,即注册时间 */
	@ApiModelProperty(value="注册时间")
	private Date parentBindingTime; 
		
	/** 累积赚得佣金. */
	@ApiModelProperty(value="累积佣金")
	private Double totalDistCommis; 
		
	/** 会员手机号. */
	@ApiModelProperty(value="会员手机号")
	private String userMobile;
	
	/**
	 * The Constructor.
	 */
	public AppMyPromoterUserDto() {
    }

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getParentBindingTime() {
		return parentBindingTime;
	}

	public void setParentBindingTime(Date parentBindingTime) {
		this.parentBindingTime = parentBindingTime;
	}

	public Double getTotalDistCommis() {
		return totalDistCommis;
	}

	public void setTotalDistCommis(Double totalDistCommis) {
		this.totalDistCommis = totalDistCommis;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}
	
} 
