package com.legendshop.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.legendshop.app.dto.AppUserShopCartOrderDto;
import com.legendshop.model.dto.TransfeeDto;
import com.legendshop.model.dto.app.cartorder.ShopCartOrderDto;
import com.legendshop.model.dto.app.cartorder.ShopGroupCartOrderItemDto;
import com.legendshop.model.dto.app.shopcart.ShopCartItemDto;
import com.legendshop.model.dto.app.store.AppUserShopStoreCartOrderDto;
import com.legendshop.model.dto.app.store.ShopStoreCartOrderDto;
import com.legendshop.model.dto.app.store.StoreCartsDto;
import com.legendshop.model.dto.buy.ShopCartItem;
import com.legendshop.model.dto.buy.ShopCarts;
import com.legendshop.model.dto.buy.StoreCarts;
import com.legendshop.model.dto.buy.UserShopCartList;
import com.legendshop.model.dto.marketing.MarketingDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.legendshop.app.dao.AppStoreDao;
import com.legendshop.app.service.AppStoreService;
import com.legendshop.dao.support.PageSupport;
import com.legendshop.model.dto.app.AppPageSupport;
import com.legendshop.model.dto.app.AppStoreDto;
import com.legendshop.model.dto.app.AppStoreSearchParamsDTO;
import com.legendshop.model.entity.store.Store;
import com.legendshop.spi.service.StoreService;
import com.legendshop.util.AppUtils;
import com.legendshop.util.MethodUtils;

@Service("appStoreService")
public class AppStoreServiceImpl implements AppStoreService {
	
	@Autowired
	private AppStoreDao appStoreDao;
	
	@Autowired
	private StoreService storeService;
	
	@Override
	public List<AppStoreDto> findStoreCitys(Long poductId, Integer provinceId, Integer cityId, Integer areaId) {
		return appStoreDao.findStoreCitys(poductId, provinceId, cityId, areaId);
	}

	@Override
	public List<AppStoreDto> findStoreShopCitys(long shopId, int provinceid, int cityid, int areaid) {
		return appStoreDao.findStoreShopCitys(shopId, provinceid, cityid, areaid);
	}

	/**
	 * 根据传入参数获取相关门店
	 */
	@Override
	public AppPageSupport<AppStoreDto> queryStore(AppStoreSearchParamsDTO searchParams) {
		
		PageSupport<AppStoreDto> ps = appStoreDao.queryStore(searchParams);
		return convertToAppPageSupport(ps);
	}
	
	//转换PageSupport
	private <T> AppPageSupport<T> convertToAppPageSupport(PageSupport<T> ps) {
		AppPageSupport<T> result = new AppPageSupport<T>();
		result.setCurrPage(ps.getCurPageNO());
		result.setPageCount(ps.getPageCount());
		result.setPageSize(ps.getPageSize());
		result.setResultList(ps.getResultList());
		result.setTotal(ps.getTotal());
		return result;
	}

	
	/**
	 * 根据门店id获取详情
	 */
	@Override
	public AppStoreDto getStore(Long storeId) {
		
		Store store = storeService.getStore(storeId);
		return convertToAppStoreDto(store);
	}
	
	/**
	 * 根据经纬度获取距离最近的门店
	 */
	@Override
	public AppStoreDto getNearStore(Long prodId, Long skuId, Double lng, Double lat) {
		
		/** 组装搜索参数 **/
		AppStoreSearchParamsDTO searchParams = new AppStoreSearchParamsDTO();
		searchParams.setProdId(prodId);
		searchParams.setSkuId(skuId);
		searchParams.setLng(lng);
		searchParams.setLat(lat);
		searchParams.setPageSize(1);
		AppPageSupport<AppStoreDto> result = this.queryStore(searchParams);
		
		if (AppUtils.isBlank(result.getResultList())) {
			return null;
		}
		AppStoreDto appStoreDto = result.getResultList().get(0);
		return appStoreDto;
	}

	/**
	 * 转换门店购物车DTO
	 * @param userShopCartList
	 * @return
	 */
	@Override
	public AppUserShopStoreCartOrderDto getShoppingCartOrderDto(UserShopCartList userShopCartList) {

		if(AppUtils.isBlank(userShopCartList.getShopCarts())){
			return null;
		}
		return convertToAppUserShopCartOrderDto(userShopCartList);
	}


	private AppUserShopStoreCartOrderDto convertToAppUserShopCartOrderDto(UserShopCartList userShopCartList){

		AppUserShopStoreCartOrderDto userShopCartListDto = new AppUserShopStoreCartOrderDto();
		userShopCartListDto.setUserId(userShopCartList.getUserId());
		userShopCartListDto.setUserName(userShopCartList.getUserName());
		userShopCartListDto.setType(userShopCartList.getType());

		userShopCartListDto.setPayManner(userShopCartList.getPayManner());
		userShopCartListDto.setPdAmount(userShopCartList.getPdAmount());
		userShopCartListDto.setOrderActualTotal(userShopCartList.getOrderActualTotal());

		userShopCartListDto.setOrderTotalCash(userShopCartList.getOrderTotalCash());
		userShopCartListDto.setOrderTotalQuanlity(userShopCartList.getOrderTotalQuanlity());
		userShopCartListDto.setOrderTotalVolume(userShopCartList.getOrderTotalVolume());

		userShopCartListDto.setOrderTotalWeight(userShopCartList.getOrderTotalWeight());
		userShopCartListDto.setOrderFreightAmount(userShopCartList.getOrderCurrentFreightAmount());
		userShopCartListDto.setAllDiscount(userShopCartList.getAllDiscount());

		userShopCartListDto.setUserAddress(userShopCartList.getUserAddress());
		userShopCartListDto.setOrderToken(userShopCartList.getOrderToken());
		List<ShopStoreCartOrderDto> shopCartsDto = convertShopCartOrderDto(userShopCartList.getShopCarts()); //订单是按店铺区分， 对应的店铺的购物车信息。
		userShopCartListDto.setShopCarts(shopCartsDto);

		return userShopCartListDto;
	}


	private List<ShopStoreCartOrderDto> convertShopCartOrderDto(List<ShopCarts> shopCartList) {
		if(AppUtils.isNotBlank(shopCartList)){
			List<ShopStoreCartOrderDto> dtoList = new ArrayList<ShopStoreCartOrderDto>(shopCartList.size());
			for (ShopCarts shopCarts : shopCartList) {
				ShopStoreCartOrderDto shopCartsDto = new ShopStoreCartOrderDto();
				shopCartsDto.setShopId(shopCarts.getShopId());
				shopCartsDto.setShopName(shopCarts.getShopName());
				shopCartsDto.setShopStatus(shopCarts.getShopStatus());
				shopCartsDto.setShopTotalCash(shopCarts.getShopTotalCash());
				shopCartsDto.setShopActualTotalCash(shopCarts.getShopActualTotalCash());
				shopCartsDto.setShopTotalWeight(shopCarts.getShopTotalWeight());
				shopCartsDto.setShopTotalVolume(shopCarts.getShopTotalVolume());
				shopCartsDto.setTotalcount(shopCarts.getTotalcount());
				shopCartsDto.setDiscountPrice(shopCarts.getDiscountPrice());
				shopCartsDto.setTotalIntegral(shopCarts.getTotalIntegral());
				shopCartsDto.setSupportStore(shopCarts.isSupportStore());

				List<StoreCarts> storeCartsList = shopCarts.getStoreCarts();
				List<StoreCartsDto> storeCartDtoList = new ArrayList<StoreCartsDto>();
				if(AppUtils.isNotBlank(storeCartsList)){
					for( StoreCarts storeCarts: storeCartsList){
						StoreCartsDto storeCartsDto = new StoreCartsDto();

						storeCartsDto.setSubId(storeCarts.getSubId());
						storeCartsDto.setStoreAddr(storeCarts.getStoreAddr());
						storeCartsDto.setTelphone(storeCarts.getTelphone());
						storeCartsDto.setStoreName(storeCarts.getStoreName());
						storeCartsDto.setRemark(storeCarts.getRemark());
						storeCartsDto.setStoreId(storeCarts.getStoreId());
						storeCartsDto.setBuyerName(storeCarts.getBuyerName());
						storeCartsDto.setStoreStatus(storeCarts.getStoreStatus());
						storeCartsDto.setSubNumber(storeCarts.getSubNumber());

						List<ShopCartItemDto> shopCartItemDtoList = convertShopCartItemDto(storeCarts.getCartItems());
						storeCartsDto.setCartItems(shopCartItemDtoList);
						storeCartDtoList.add(storeCartsDto);
					}
				}
				shopCartsDto.setStoreCarts(storeCartDtoList);
				dtoList.add(shopCartsDto);
			}
			return dtoList;
		}
		return null;
	}

	private List<ShopCartItemDto> convertShopCartItemDto(List<ShopCartItem> shopCartItemList){
		if(AppUtils.isNotBlank(shopCartItemList)){
			List<ShopCartItemDto> dtoList = new ArrayList<ShopCartItemDto>();
			for (ShopCartItem shopCartItem : shopCartItemList) {
				ShopCartItemDto shopCartItemDto = new ShopCartItemDto();
				/** 购物车ID */
				shopCartItemDto.setBasketId(shopCartItem.getBasketId());
				/**选中状态 */
				shopCartItemDto.setCheckSts(shopCartItem.getCheckSts());
				/** 商品ID */
				shopCartItemDto.setProdId(shopCartItem.getProdId());
				/** shop ID  */
				shopCartItemDto.setShopId(shopCartItem.getShopId());
				/** skuId */
				shopCartItemDto.setSkuId(shopCartItem.getSkuId());
				/** 商品原价*/
				shopCartItemDto.setPrice(shopCartItem.getPrice());
				/** 促销价格  */
				shopCartItemDto.setPromotionPrice(shopCartItem.getPromotionPrice());
				/** sku图片  */
				shopCartItemDto.setPic(shopCartItem.getPic());
				/** 购物车商品  */
				shopCartItemDto.setBasketCount(shopCartItem.getBasketCount());
				/** 商品名称  */
				shopCartItemDto.setProdName(shopCartItem.getProdName());
				/** sku名称  */
				shopCartItemDto.setSkuName(shopCartItem.getSkuName());
				/** 商品状态 */
				shopCartItemDto.setStatus(shopCartItem.getStatus());
				/** 库存*/
				shopCartItemDto.setStocks(shopCartItem.getStocks());
				/** 实际库存*/
				shopCartItemDto.setActualStocks(shopCartItem.getActualStocks());
				/** 物流重量(千克)  */
				shopCartItemDto.setWeight(shopCartItem.getWeight());
				/** 物流体积(立方米)  */
				shopCartItemDto.setVolume(shopCartItem.getVolume());
				/** 货到付款; 0:普通商品 , 1:货到付款商品   */
				shopCartItemDto.setIsGroup(shopCartItem.getIsGroup());
				shopCartItemDto.setIsSupportCod(shopCartItem.isSupportCod());
				shopCartItemDto.setCnProperties(shopCartItem.getCnProperties());
				/** 销售属性组合（中文）  */
				/** 属性Id组合 */
				shopCartItemDto.setProperties(shopCartItem.getProperties());
				/** 商品总费用  商品总价格   单价*数量 */
				shopCartItemDto.setTotal(shopCartItem.getTotal());
				/** 商品实际总费用  商品总价格  + 运费  - 促销费用*/
				shopCartItemDto.setTotalMoeny(shopCartItem.getTotalMoeny());
				/** 总重量 */
				shopCartItemDto.setTotalWeight(shopCartItem.getTotalWeight());
				shopCartItemDto.setTotalVolume(shopCartItem.getTotalVolume());
				/** -------------------------营销活动信息----------------------  */
				/** 优惠价价格 */
				shopCartItemDto.setDiscountPrice(shopCartItem.getDiscountPrice());
				shopCartItemDto.setDiscount(shopCartItem.getDiscount());
				shopCartItemDto.setStoreIsExist(shopCartItem.getStoreIsExist());//门店自提添加的标识 所选门店是否存在该商品（false为不存在 ）

				dtoList.add(shopCartItemDto);
			}
			return dtoList;
		}
		return null;
	}




	//转换Dto
	private AppStoreDto convertToAppStoreDto(Store store){
		  AppStoreDto appStoreDto = new AppStoreDto();
		  
		  appStoreDto.setStoreId(store.getId());
		  appStoreDto.setArea(store.getArea());
		  appStoreDto.setAddress(store.getAddress());
		  appStoreDto.setProvince(store.getProvince());
		  appStoreDto.setLng(store.getLng());
		  appStoreDto.setCity(store.getCity());
		  appStoreDto.setName(store.getName());
		  appStoreDto.setBusinessHours(store.getBusinessHours());
		  appStoreDto.setLat(store.getLat());
		  return appStoreDto;
		}

}
