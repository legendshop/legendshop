package com.legendshop.app.dto;

import com.legendshop.app.dto.mergeGroup.AppMergeGroupDto;
import com.legendshop.app.dto.presell.AppPresellActivityDto;
import com.legendshop.model.dto.group.AppGroupDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

@ApiModel(value="商品参与的营销活动详情")
public class AppProdActivityInfoDto {
	
	@ApiModelProperty("活动id")
	private Long id;

	@ApiModelProperty("活动类型 （P:正常商品，PRESELL:预售活动，SECKILL:秒杀活动，GROUP:团购，MERGE:拼团，AUCTION:拍卖）")
	private String name;
	
	@ApiModelProperty("商品价格")
	private Double price;
	
	@ApiModelProperty("商品状态 （-6为正常商品）")
	private  Integer status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
