package com.legendshop.app.service;

import com.legendshop.model.dto.app.OrderItemDto;

/**
 * app订单项service
 */
public interface AppSubItemService {

	/**
	 * 获取订单项详情
	 * @param subItemId 订单项id
	 * @return
	 */
	public OrderItemDto getSubItem(Long subItemId);

}
