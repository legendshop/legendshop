package com.legendshop.app.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;

/**
 * app 退货退款公共参数Dto
 * @author linzh
 */
public class ApplyRefundReturnBaseDto implements Serializable {
	
	/**  */
	private static final long serialVersionUID = -3248781291208859740L;

	/** 订单ID **/
	@NotNull(message = "订单id不能为空")
	@ApiModelProperty(value="订单ID")
	private Long orderId;

	/** 退款原因 */
	@NotBlank(message = "退款原因不能为空")
	@Length(min=1,max=300)
	@ApiModelProperty(value="退款原因")
	private String buyerMessage;
	
	/** 退款金额 */
	@NotNull(message = "退款金额不能为空")
	@ApiModelProperty(value="退款金额")
	private Double refundAmount;
	
	/**预售订单，订金退款金额*/
	@ApiModelProperty(value="预售订单，订金退款金额")
	private Double depositRefundAmount;
	
	/**是否退订金*/
	@ApiModelProperty(value="是否退订金")
	private Boolean isRefundDeposit;
	
	/** 退款说明 */
	@Length(min=1,max=300,message="退款说明1~300个字符")
	@ApiModelProperty(value="退款说明")
	private String reasonInfo;
	
	/** 凭证图片1 */
	@ApiModelProperty(value="凭证图片1")
	private String photo1;
	
	/** 凭证图片2 */
	@ApiModelProperty(value="凭证图片2")
	private String photo2;
	
	/** 凭证图片3 */
	@ApiModelProperty(value="凭证图片3")
	private String photo3;

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getBuyerMessage() {
		return buyerMessage;
	}

	public void setBuyerMessage(String buyerMessage) {
		this.buyerMessage = buyerMessage;
	}

	public Double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}

	public Double getDepositRefundAmount() {
		return depositRefundAmount;
	}

	public void setDepositRefundAmount(Double depositRefundAmount) {
		this.depositRefundAmount = depositRefundAmount;
	}

	public Boolean getIsRefundDeposit() {
		return isRefundDeposit;
	}

	public void setIsRefundDeposit(Boolean isRefundDeposit) {
		this.isRefundDeposit = isRefundDeposit;
	}

	public String getReasonInfo() {
		return reasonInfo;
	}

	public void setReasonInfo(String reasonInfo) {
		this.reasonInfo = reasonInfo;
	}

	public String getPhoto1() {
		return photo1;
	}

	public void setPhoto1(String photo1) {
		this.photo1 = photo1;
	}

	public String getPhoto2() {
		return photo2;
	}

	public void setPhoto2(String photo2) {
		this.photo2 = photo2;
	}

	public String getPhoto3() {
		return photo3;
	}

	public void setPhoto3(String photo3) {
		this.photo3 = photo3;
	}
}
