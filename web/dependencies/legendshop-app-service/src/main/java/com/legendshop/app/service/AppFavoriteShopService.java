package com.legendshop.app.service;

import com.legendshop.model.dto.app.AppFavoriteShopDto;
import com.legendshop.model.dto.app.AppPageSupport;

public interface AppFavoriteShopService {

	/**
	 *  获取我收藏的店铺
	 * @param curPageNO
	 * @param pageSize
	 * @param userId
	 * @return
	 */
	public AppPageSupport<AppFavoriteShopDto> queryFavoriteShop(String curPageNO,Integer pageSize,String userId);
	
	/**
	 * 取消店铺收藏
	 * @param id
	 * @param userId
	 * @return
	 */
	public boolean deleteFavoriteShop(Long id,String userId);

	/**
	 * 取消/批量取消收藏店铺
	 * @param fsIds
	 * @param userId
	 */
	public void deleteFavoriteShops(String fsIds, String userId);

	//获取店铺收藏的数量
	public Long getfavoriteLength(String userId);
}
