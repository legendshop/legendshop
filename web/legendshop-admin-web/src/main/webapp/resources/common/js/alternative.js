function highlightTableRows(tableId) {
        var previousClass = null;  
        var table = document.getElementById(tableId);
        if(table == null) return;
        var tbody;
        if(table.getElementsByTagName("tbody") != null){
         tbody = table.getElementsByTagName("tbody")[0]; 
        }
        if (tbody == null) {  
            var rows = table.getElementsByTagName("tr");  
        } else {  
            var rows = tbody.getElementsByTagName("tr");  
        }  
        for (i=0; i < rows.length; i++) {
            rows[i].onmouseover = function() { previousClass=this.className;this.className='pointercolor' };  
            rows[i].onmouseout = function() { this.className=previousClass };  
			/*
            rows[i].onclick = function() {
                var cell = this.getElementsByTagName("td")[0];  
                var link = cell.getElementsByTagName("a")[0];  
                var theUrl=link.getAttribute("href");  
                if(theUrl != null && theUrl.indexOf("javascript")!=-1){
                    eval(theUrl); }else{
                    location.href=theUrl;  
                }
       		 this.style.backgroundColor="blue";  
            }  
            
            */
        }  
    } 
    
 function selAll(){
    chk = document.getElementById("checkbox");
	allSel = document.getElementsByName("strArray");
	
	if(!chk.checked)
		for(i=0;i<allSel.length;i++)
		{
			   allSel[i].checked=false;
		}
	else
		for(i=0;i<allSel.length;i++)
		{
			   allSel[i].checked=true;
		}
	}
	
 	function checkSelect(selAry){
		for(i=0;i<selAry.length;i++)
		{
			if(selAry[i].checked)
			{
				return true;
			}
		}
		return false;
	 }
 	
 	
 	//检查是否所有的checkbox都选中了
 	function isAllSel(allSel){
 		for(var i = 0; i < allSel.length; i++){
 			if(!allSel[i].checked){
 				return false;
 			}
 		}
 		return true;
 	}


    function pager(curPageNO){
        document.getElementById("curPageNO").value=curPageNO;
        document.getElementById("form1").submit();
    }
    
 /*	$(document).ready(function(){
 		var checkbox = document.getElementById("checkbox");
 		var allSel = document.getElementsByName("strArray");
 		for(var i = 0; i < allSel.length; i++){
 			allSel[i].onclick = function(event){
 				if(!this.checked){//当列表中有一个被取消选中,则让全选按钮取消选中
 					checkbox.checked = false;
 				}else{
 					if(isAllSel(allSel)){//当列表全部被选上了,则让全选按钮选中
 						checkbox.checked = true;
 					}
 				}
 			}
 		}
 });*/ 
 	
 	//全选
 	$(".selectAll").click(function(){
 		if($(this).attr("checked")=="checked"){
 			$(this).parent().parent().addClass("checkbox-wrapper-checked");
 	         $(".selectOne").each(function(){
 	        		 $(this).attr("checked",true);
 	        		 $(this).parent().parent().addClass("checkbox-wrapper-checked");
 	         });
 	     }else{
 	         $(".selectOne").each(function(){
 	             $(this).attr("checked",false);
 	             $(this).parent().parent().removeClass("checkbox-wrapper-checked");
 	         });
 	         $(this).parent().parent().removeClass("checkbox-wrapper-checked");
 	     }
 	 });
 	
 	function selectAll(obj){
 		if($(obj).attr("checked")=="checked"){
 			$(obj).parent().parent().addClass("checkbox-wrapper-checked");
 	         $(".selectOne").each(function(){
        	 	if(!this.disabled){
        	 		$(this).attr("checked",true);
        	 		$(this).parent().parent().addClass("checkbox-wrapper-checked");
        	 	}
 	         });
 	     }else{
 	         $(".selectOne").each(function(){
 	        	 if(!this.disabled){
 	        		 $(this).attr("checked",false);
 	        		 $(this).parent().parent().removeClass("checkbox-wrapper-checked");
 	        	 }
 	         });
 	         $(obj).parent().parent().removeClass("checkbox-wrapper-checked");
 	     }
 	 };

 	function selectOne(obj){
 		if(!this.disabled){
	 		if(!obj.checked){
	 			$(".selectAll").checked = obj.checked;
	 			$(obj).prop("checked",false);
	 			$(obj).parent().parent().removeClass("checkbox-wrapper-checked");
	 		}else{
	 			$(obj).prop("checked",true);
	 			$(obj).parent().parent().addClass("checkbox-wrapper-checked");
	 		}
	 		 var flag = true;
	 		  var arr = $(".selectOne");
	 		  for(var i=0;i<arr.length;i++){
	 		     if(!arr[i].checked){
	 		    	 flag=false; 
	 		    	 break;
	 		    	 }
	 		  }
	 		 if(flag){
	 			 $(".selectAll").prop("checked",true);
	 			 $(".selectAll").parent().parent().addClass("checkbox-wrapper-checked");
	 		 }else{
	 			 $(".selectAll").prop("checked",false);
	 			 $(".selectAll").parent().parent().removeClass("checkbox-wrapper-checked");
	 		 }
 		}
 	}
	 