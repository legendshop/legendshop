//图片空间
var setting = {
	view: {
		selectedMulti: false
	},
	edit: {
		enable: false,
		editNameSelectAll: false
	},
	async: {
		enable: true,
		url:webPath+"/imageAdmin/getChildTree",
		autoParam:["id=parentId"],
		dataFilter: null
	},
	data: {
		simpleData: {
			enable: true
		}
	},
	callback: {
		beforeDrag: beforeDrag,
		onClick: zTreeOnclick,
		onAsyncSuccess: zTreeOnAsyncSuccess
	}
};

var log, className = "dark";
function beforeDrag(treeId, treeNodes) {
	return false;
}

function showLog(str) {
	if (!log) log = $("#log");
	log.append("<li class='"+className+"'>"+str+"</li>");
	if(log.children("li").length > 8) {
		log.get(0).removeChild(log.children("li")[0]);
	}
}
function getTime() {
	var now= new Date(),
	h=now.getHours(),
	m=now.getMinutes(),
	s=now.getSeconds(),
	ms=now.getMilliseconds();
	return (h+":"+m+":"+s+ " " +ms);
}

function selectAll() {
	var zTree = $.fn.zTree.getZTreeObj("treeDemo");
	zTree.setting.edit.editNameSelectAll =  $("#selectAll").attr("checked");
}

function zTreeOnclick( event, treeId, treeNode){
	//alert(treeNode.id);
	ajaxpic(1,treeNode.id,"","");
	$("#currId").val(treeNode.id);
}

var init = true;
function zTreeOnAsyncSuccess(event, treeId, treeNode, msg) {
	if(init){
		var nodes = $.fn.zTree.getZTreeObj("treeDemo").getNodes();
		$.fn.zTree.getZTreeObj("treeDemo").selectNode(nodes[0]);
		init=false;
		 try { 
        	  var zTree = $.fn.zTree.getZTreeObj("treeDemo");
                 //调用默认展开第一个结点 
                 var selectedNode = zTree.getSelectedNodes(); 
                 zTree.expandNode(nodes[0], true); 
                 zTree.selectNode(nodes[0]);
           } catch (err) { 
             
           } 
	}
}
	
	
function pager(curPageNO){
    document.getElementById("curPageNO").value=curPageNO;
    ajaxpic(curPageNO,$("#currId").val(),$("#orderBy").val(),$("#searchName").val());
}

function ajaxpic(curPageNO,id,orderBy,searchName){
	$.ajax({
			url: webPath+"/imageAdmin/getPropChildImg", 
			data: {"curPageNO": curPageNO,"treeId":id,"orderBy":orderBy,"searchName":searchName},
			type:'post', 
			async : true, //默认为true 异步   
			dataType : 'json', 
			success:function(dataContainer){
				var data = dataContainer.pageData;
				var str = "";
				for(var i=0;i<data.resultList.length;i++){
					str+="<li title='";
					if(data.resultList[i].fileName!=null){
						str += data.resultList[i].fileName;
					}else{
						str += data.resultList[i].id;
					}
					str+="'>";
					str+="<span style='height: 100%;vertical-align: middle;display: inline-block;'></span>"
					str+="<img  photo=\""+photoPath+data.resultList[i].filePath;
					str+="\" filePath=\""+data.resultList[i].filePath;
					str+="\" class='img'  src=\"";
					str+=photoPath+data.resultList[i].filePath+suffixPath;
					str+="\"/>";
					str+="<h5>";
					if(data.resultList[i].fileName!=null){
						str += data.resultList[i].fileName;
					}else{
						str += data.resultList[i].id;
					}
					str+="</h5>";
					if(i==0){
						str+="<img src='"+webPath+"/resources/common/images/image_select.png' class='image_select'>";
					}
					str+="</li>";
				}
				$("#curPageNO").val(data.curPageNO);
				if(dataContainer.toolBar!=null){
					$("#toolBar").html(dataContainer.toolBar);
				}else{
					$("#toolBar").html("");
				}
				$("#main-div").html(str);
				var $img=$('<img src="'+webPath+'/resources/common/images/image_select.png" class="image_select">');
				$('.pictures_con ul li').click(function(){
					$(this).append($img).siblings().find('.image_select').remove();
				});
				$("#delPic").hide();
				$("#renamePic").hide();
				$(".pic").click(function(){
					$(".pic").removeClass("selected");
					$(this).addClass("selected");
					$("#delPic").show();
					$("#renamePic").show();
				});
			}   
		});
}

