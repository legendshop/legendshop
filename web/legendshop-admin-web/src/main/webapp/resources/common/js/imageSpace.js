var setting = {
			view: {
				addHoverDom: addHoverDom,
				removeHoverDom: removeHoverDom,
				selectedMulti: false
			},
			edit: {
				enable: true,
				editNameSelectAll: true,
				showRemoveBtn: showRemoveBtn,
				showRenameBtn: showRenameBtn
			},
			async: {
				enable: true,
				url:contextPath+"/imageAdmin/getChildTree",
				autoParam:["id=parentId"],
				dataFilter: null
			},
			data: {
				simpleData: {
					enable: true,
					idKey: "id",
					pIdKey: "parentId",
					rootPid:0
				},
				keep: {
					parent: true
				}
			},
			callback: {
				beforeDrag: beforeDrag,
				beforeEditName: beforeEditName,
				beforeRemove: beforeRemove,
				beforeRename: beforeRename,
				onRemove: onRemove,
				onRename: onRename,
				onClick: zTreeOnclick,
				onAsyncSuccess: zTreeOnAsyncSuccess
			}
		};

		var firstAsyncSuccessFlag = 0;
		function zTreeOnAsyncSuccess(event, treeId, msg) { 
			if (firstAsyncSuccessFlag == 0) { 
		          try { 
		        	  var zTree = $.fn.zTree.getZTreeObj("treeDemo");
		                 //调用默认展开第一个结点 
		                 var selectedNode = zTree.getSelectedNodes(); 
		                 var nodes = zTree.getNodes(); 
		                 zTree.expandNode(nodes[0], true); 
		                 zTree.selectNode(nodes[0]);
		                 firstAsyncSuccessFlag = 1; 
		           } catch (err) { 
		             
		           } 
		             
		     } 
		} 
		
		var log, className = "dark";
		function beforeDrag(treeId, treeNodes) {
			return false;
		}
		function beforeEditName(treeId, treeNode) {
			className = (className === "dark" ? "":"dark");
			showLog("[ "+getTime()+" beforeEditName ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name);
			var zTree = $.fn.zTree.getZTreeObj("treeDemo");
			zTree.selectNode(treeNode);
			ajaxpic(1,treeNode.id);
			$("#currId").val(treeNode.id);
			return true;
		}
		function beforeRemove(treeId, treeNode) {
			className = (className === "dark" ? "":"dark");
			showLog("[ "+getTime()+" beforeRemove ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name);
			var zTree = $.fn.zTree.getZTreeObj("treeDemo");
			layer.confirm('请确定是否删除选中文件夹及包含的所有图片? (不可恢复)',{
		  		 icon: 3
		  	     ,btn: ['确定','取消'] //按钮
		  	   }, function () {
		  		 $.ajax({
						url: contextPath+"/imageAdmin/delTreeAndPic", 
						data: {"treeId":treeNode.id},
						type:'post', 
						async : false, //默认为true 异步   
						dataType : 'json', 
						success:function(data){
							layer.msg('删除成功！', {icon: 1,time:700},function(){
		                		window.location.reload();
		                	});
						}   
					});
			})
		}
		function onRemove(e, treeId, treeNode) {
			//showLog("[ "+getTime()+" onRemove ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name);
		}
		function beforeRename(treeId, treeNode, newName, isCancel) {
			className = (className === "dark" ? "":"dark");
			showLog((isCancel ? "<span style='color:red'>":"") + "[ "+getTime()+" beforeRename ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name + (isCancel ? "</span>":""));
			if (newName.length == 0) {
				layer.alert('目录名称不能为空', {icon: 0});
				var zTree = $.fn.zTree.getZTreeObj("treeDemo");
				setTimeout(function(){zTree.editName(treeNode)}, 10);
				return false;
			}
			return true;
		}
		function onRename(e, treeId, treeNode, isCancel) {
			//showLog((isCancel ? "<span style='color:red'>":"") + "[ "+getTime()+" onRename ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name + (isCancel ? "</span>":""));
			renameNode(treeNode.id,treeNode.name);
		}
		function addNode(name,zTree,treeNode) {
		   // alert(treeNode.id + ", " + treeNode.name);
		   zTree.expandNode(treeNode, true);
		    $.ajax({
				url: contextPath+"/imageAdmin/addTree", 
				data: {"id":treeNode.id,"name": name},
				type:'post', 
				async : true, //默认为true 异步   
				dataType : 'json', 
				success:function(data){
					var nodes = zTree.addNodes(treeNode, {id:data, pId:treeNode.id, name:name,isParent:true});
					zTree.editName(nodes[0]);
					zTree.selectNode(nodes[0]);
					ajaxpic(1,nodes[0].id);
					$("#currId").val(nodes[0].id);
				}   
			});
		}
		function showRemoveBtn(treeId, treeNode) {
			return treeNode.level > 0;
		}
		function showRenameBtn(treeId, treeNode) {
			return treeNode.level > 0;
		}
		function showLog(str) {
			if (!log) log = $("#log");
			log.append("<li class='"+className+"'>"+str+"</li>");
			if(log.children("li").length > 8) {
				log.get(0).removeChild(log.children("li")[0]);
			}
		}
		function getTime() {
			var now= new Date(),
			h=now.getHours(),
			m=now.getMinutes(),
			s=now.getSeconds(),
			ms=now.getMilliseconds();
			return (h+":"+m+":"+s+ " " +ms);
		}

		var newCount = 1;
		function addHoverDom(treeId, treeNode) {
			var sObj = $("#" + treeNode.tId + "_span");
			if (treeNode.editNameFlag || $("#addBtn_"+treeNode.tId).length>0) return;
			var addStr = "<span class='button add' id='addBtn_" + treeNode.tId
				+ "' title='add node' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var btn = $("#addBtn_"+treeNode.tId);
			if (btn){
				btn.bind("click", function(){
					var zTree = $.fn.zTree.getZTreeObj("treeDemo");
					addNode("新目录" + (newCount++),zTree,treeNode);
					return false;
				});
			} 
		};
		function removeHoverDom(treeId, treeNode) {
			$("#addBtn_"+treeNode.tId).unbind().remove();
		};
		function selectAll() {
			var zTree = $.fn.zTree.getZTreeObj("treeDemo");
			zTree.setting.edit.editNameSelectAll =  $("#selectAll").attr("checked");
		}
		
		function zTreeOnclick( event, treeId, treeNode){
			//alert(treeNode.id);
			ajaxpic(1,treeNode.id);
			$("#currId").val(treeNode.id);
		}
		
	function pager(curPageNO){
        document.getElementById("curPageNO").value=curPageNO;
        ajaxpic(curPageNO,$("#currId").val());
    }
    
	//异步加载 某目录下的图片
    function ajaxpic(curPageNO,id){
    	var shopName = "";
    	if($("#shopName").get().length>0){
    		shopName = $("#shopName").val().trim();
    	}
    	if(id == "undefined"){
    		id = "0";
    	}
    	$.ajax({
				url: contextPath+"/imageAdmin/getChildImg", 
				data: {"curPageNO": curPageNO,"treeId":id,"searchName":$("#searchName").val().trim(),"shopName":shopName},
				type:'post', 
				async : true, //默认为true 异步   
				dataType : 'json', 
				success:function(dataContainer){
					var str = "";
					var data = dataContainer.pageData;
					for(var i=0;i<data.resultList.length;i++){
						str += "<div class='pic' nodeId='"+id+"' fileName='"+data.resultList[i].fileName+"' filePath='"+data.resultList[i].filePath +"' attachmentId='"+data.resultList[i].id+"'>";
						str += "<div class='pic_img'>";
						str += "<img  class='img'  src='" +imagesPrefix + data.resultList[i].filePath + imagesSuffix + "'/>";
						str += "</div><div title='"+data.resultList[i].fileName+"' class='name_div'>";
						str += data.resultList[i].fileName+"</div>";
						str += "<div class='name_input hidden'><input value="+data.resultList[i].fileName+" /></div></div>";
					}
					$("#curPageNO").val(data.curPageNO);
					if(dataContainer.toolBar!=null){
						$("#toolBar").html(dataContainer.toolBar);
					}else{
						$("#toolBar").html("");
					}
					$("#main-div").html(str);
					$("#delPic").hide();
					$("#renamePic").hide();
					$(".pic").click(function(){
						$(".pic").removeClass("selected");
						$(this).addClass("selected");
						$("#delPic").show();
						$("#renamePic").show();
					});
					//图片名称input 失去焦点事件  用于图片重命名
					$(".pic .name_input input").blur(function(){
						$(this).parent().hide();
						$(this).parent().parent().find(".name_div").show();
						rename($.trim($(this).val()),this);
					});
				}   
			});
    }
    
    //删除图片
    function delPic(){
			var delCon = layer.confirm("确定要删除'"+$(".pic:.selected").attr("fileName")+"'吗？", {
	     		 icon: 3
	      	     ,btn: ['确定','取消'] //按钮
	      	   },function () {
				$.ajax({
					url: contextPath+"/imageAdmin/delImg", 
					data: {"attachmentId": $(".pic:.selected").attr("attachmentId")},
					type:'post', 
					async : true, //默认为true 异步   
					dataType : 'json', 
					success:function(data){
						if(data=='OK'){
							var curPageNO = $("#curPageNO").val();
							var nodeId = $(".pic:.selected").attr("nodeId");
	        				ajaxpic(curPageNO,nodeId);
                            layer.msg("删除成功！！！",{icon:1,time:"2000"},function(){
                                window.location.reload();
                            });
						}else{
							layer.alert('删除文件,原因是' + data, {icon: 0});
						}
					}   
				});
			});
    	}
    
    function uploadPic(){
    	$("#uploadDiv").show();
    	$("#backDiv").show();
    }
    
    function renamePic(){
    	$(".pic:.selected .name_div").hide();
    	$(".pic:.selected .name_input").show();
    	$(".pic:.selected .name_input input").select();
    	$(".pic:.selected .name_input input").focus();
    }
    
    function closeBtn(){
    	$("#uploadDiv").hide();
    	$("#backDiv").hide();
    }
    
    //图片上传
    function upload(){
    	  $("#upload").attr("uploadStatus","1");
          $("#form1").ajaxForm().ajaxSubmit(function(data){
          	if(data=='"OK"'){
					$("#uploadDiv").hide();
					$("#backDiv").hide();
					$("#form1").resetForm();
					ajaxpic($("#curPageNO").val(),$("#currId").val());
			}
          	$("#upload").attr("uploadStatus","0");
          });
    }
    
    //图片重命名
    function rename(newName,obj){
    	var oldName = $(obj).parent().parent().find(".name_div").html();
    	if(newName =="" || oldName==newName){
    		
    	}else{
    		$.ajax({
				url: contextPath+"/imageAdmin/renameImg", 
				data: {"filePath": $(obj).parent().parent().attr("filePath"),"fileName":newName},
				type:'post', 
				async : true, //默认为true 异步   
				dataType : 'json', 
				success:function(data){
					if(data=='OK'){
						layer.msg('修改成功！', {icon: 1});
						$(obj).parent().parent().find(".name_div").html(newName);
					}
				}   
			});
    	}
    }
    
    //修改目录名称
    function renameNode(id,name){
    	$.ajax({
				url: contextPath+"/imageAdmin/renameTree", 
				data: {"id":id,"name": name},
				type:'post', 
				async : true, //默认为true 异步   
				dataType : 'json', 
				success:function(data){
					
				}   
			});
    }
    
    //搜索图片
    function searchPic(){
    	ajaxpic(1,0);
    }