if(typeof JSON == 'undefined'){$('head').append($("<script type='text/javascript' src='"+contextPath+"/resources/common/js/json.js'>"));}
function groupFloorLoad(curPageNO){
	var groupName = $("#groupName").val();
	$.ajax({
		url:contextPath+"/admin/floorItem/groupFloorLoad/",
		data:{"curPageNO":curPageNO,"groupName":groupName},
		type:'post', 
		async : true,  
		error: function(jqXHR, textStatus, errorThrown) {
		},
		success:function(result){
			jQuery("#floor_goods_list").empty().append(result);	
		}
	});
}

function selectGroupPager(curPageNO){
	groupFloorLoad(curPageNO);
}

jQuery(document).ready(function(){
	groupFloorLoad("");
});

function goods_list_set(img,productId,name){
	var count=jQuery(".floor_box_pls ul").length;
	var add=0; 
	if(count>= 1){
		layer.alert("最多只能添加1件团购商品！", {icon:2});
		add=1;
	}
	jQuery(".floor_box_pls .floor_pro_img img").each(function(){
		if(jQuery(this).attr("id")== productId){
			layer.alert("已经存在该商品", {icon:2});
			add=2;
		}
	});
	if(add==0){
		var s="<ul ondblclick='jQuery(this).remove();' class='floor_pro'><li class='floor_pro_img'><img id='"+productId+"' title='"+name+"' src='"+image+img+"' scale='2' />' /></li></ul>";
		jQuery(".floor_box_pls").append(s);
	}
}

function save_form(){
	var floorMap = [];
	jQuery(".floor_box_pls .floor_pro_img img").each(function(){
		var obj = new Object();
		obj.id = jQuery(this).attr("id");
		floorMap.push(obj);
	});
	var groupFloors = JSON.stringify(floorMap);
	$.ajax({
		url:contextPath+"/admin/floorItem/saveGroupFloor", 
		data: {"groupFloors":groupFloors,"flId":${flId}},
		type:'post', 
		dataType : 'json', 
		async : true, //默认为true 异步   
		error: function(jqXHR, textStatus, errorThrown) {
			layer.alert(textStatus, errorThrown);
		},
		success:function(result){
			layer.msg("保存成功", {icon:1, time:1000}, function(){
				parent.location.reload();
			});
		}
	});  
}