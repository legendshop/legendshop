jQuery(document).ready(function () {
	jQuery("#form1").validate({
		rules: {
			newsCategoryName: {
				required: true,
				minlength: 2
			},
			seq: {
				isNumber: true,
			},
			file:{
				required: "#imgName:blank",
				isImage:true
			},
		},
		messages: {
			newsCategoryName: {
				required: "请输入栏目名称",
				minlength: "栏目名称不能少于2个字"
			},
			seq: {
				isNumber: "请输入5位之内的正整数!",
			},
			file:{
				required:"请上传图片",
				isImage:"请选择正确的图片上传格式"
			},
		}
	});

	$("input[name=file]").change(function(){
		checkImgType(this);
		checkImgSize(this,512);
	});

	highlightTableRows("col1");
	//斑马条纹
	$("#col1 tr:nth-child(even)").addClass("even");
});

jQuery.validator.addMethod("isNumber", function(value, element) {
	return this.optional(element) || /^[1-9]\d{0,4}$/.test(value);
}, "必须整数");

jQuery.validator.addMethod("isImage", function(value, element) {
	if(value == ""){
		return true;
	}
	if(!/\.(JPEG|GIF|JPG|PNG|jpeg|gif|jpg|png)$/.test(value)){
		return false;
	}else{
		return true;
	}
}, "请上传图片"); 