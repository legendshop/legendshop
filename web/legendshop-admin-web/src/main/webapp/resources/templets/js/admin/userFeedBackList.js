function deleteById(id) {
	layer.confirm('确定删除 ?', {icon:3}, function(){
		window.location = contextPath + "/admin/userFeedBack/delete/" + id;
	});
}

function feedbackReply(id) {
	layer.open({
		title: '回复',
		id: 'reply',
		content: '回复内容(最多50个字)<font color="red">(*必填)</font>：<br/><textarea rows="10" cols="40" maxlength="50" id="text" placeholder="输入回复内容" style="height:100px;width: 100%;"></textarea>',
		btn: ['回复'],
		yes: function(index, layero){
			var text = $("#text").val();
			if (text == null || text == '' || text == undefined) {
				layer.msg("回复不能为空", {icon:2});
				return false;
			}else if(text.length>50){
				layer.msg("回复字数不能超过50个字！", {icon:2});
				return false;
			}
			$.ajax({
				url: contextPath + "/admin/userFeedBack/update",
				data: {"id": id, "reply": text},
				type: 'post',
				async: true, //默认为true 异步
				dataType: 'json',
				success: function (data) {
					if (data == "OK") {
						layer.msg("回复成功", {icon:1});
						window.location.reload(true);
						return true;
					} else {
						layer.msg(data, {icon:2});
						return false;
					}
				}
			});
		}
	});
}
function search() {
	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	$("#form1")[0].submit();
}

//显示反馈意见全部内容
function showContent(content) {
  layer.open({
    type: 1,
    closeBtn: false,
    shift: 2,
    shadeClose: true,
    area:["500px","200px"],
    content: "<div><p>"+content+"</p></div>"
  });
}
