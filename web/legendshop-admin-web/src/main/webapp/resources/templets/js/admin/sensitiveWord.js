$(document).ready(function() {
	jQuery("#form1").validate({
		rules : {
			words : "required",
			isGlobal : "required"
		},
		messages : {
			words : "请输入关键字",
			isGlobal : "请选择"
		}
	});

	//斑马条纹
	$("#col1 tr:nth-child(even)").addClass("even");
});

function change(v1) {
	var b1 = $("#one");
	if (v1 == 1) {
		b1.hide();
		$("#sortId").val("null");
		$("#nsortId").val("null");
		$("#subNsortId").val("null");
	}
	if (v1 == 0) {
		b1.show();
	}
}
function pager(curPageNO) {
	sendData(curPageNO);
}

jQuery(document).ready(function() {
	//三级联动
	change($("#isGlobal").val());
	$("select.combox").initSelect();
});