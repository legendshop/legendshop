$(document).ready(function () {
	jQuery("#form1").validate({
		rules: {
			url: "required",
			wordlink: "required",
			content: "required",
			bs: {
				number:true,
				digits:true
			}
		},
		messages: {
			url: {
				required: "请输入连接地址"
			},
			wordlink: {
				required: "请输入连接显示文字"
			},
			content: {
				required: "请输入描述"
			},
			bs: {
				number: "次序必须为数字",
				digits: "次序不能为负数"
			}
		}
	});

	$("#col1 tr").each(function (i) {
		if (i > 0) {
			if (i % 2 == 0) {
				$(this).addClass('even');
			} else {
				$(this).addClass('odd');
			}
		}
	});
	$("#col1 th").addClass('sortable');
});