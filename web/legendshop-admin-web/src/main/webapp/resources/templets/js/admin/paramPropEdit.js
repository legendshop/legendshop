$(document).ready(function() {
	$(".J_spu-property").divselect();
});
function searchParamGroup(obj){
	var value = {"groupName": $(obj).val() };
	$.ajax({
		url:contextPath+"/admin/prodType/getParamGroup", 
		type:'post', 
		data: value,
		async : true, //默认为true 异步   
		error: function(jqXHR, textStatus, errorThrown) {
			// alert(textStatus, errorThrown);
		},
		success:function(result){
			$(obj).parent().next().html(result);
		}
	});
}

function saveProdTypeParam(){
	var typeParamId = $("#typeParamId").val();
	var paramSeq = $("#paramSeq").val();
	var groupId = $("#groupId").val();
	$.ajax({
		url:contextPath+"/admin/prodType/saveProdTypeParam", 
		data: {"typeParamId":typeParamId,"paramSeq":paramSeq,"groupId":groupId},
		type:'post', 
		dataType : 'json', 
		async : true, //默认为true 异步   
		error: function(jqXHR, textStatus, errorThrown) {
			layer.alert(textStatus, errorThrown);
		},
		success:function(result){
			parent.location.reload();
		}
	});  
}