if (typeof JSON == 'undefined') {
	$('head').append($("<script type='text/javascript' src='"+contextPath+"/resources/common/js/json.js'>"));
}
function productFloorLoad(curPageNO) {
	var referId = $("#sort").val();
	var prodName = $("#prodName").val();
	var categoryId = referId.substring(2);
	var data = {
			"curPageNO": curPageNO,
			"categoryId": categoryId,
			"name": prodName
	};
	$.ajax({
		url: contextPath+"/admin/subFloorItem/productFloorLoad",
		data: data,
		type: 'post',
		async: true,
		success: function (result) {
			jQuery("#floor_goods_list").empty().append(result);
		}
	});
}

function selectProductPager(curPageNO) {
	productFloorLoad(curPageNO);
}

jQuery(document).ready(function () {
	jQuery("#floor_goods_info").sortable();
	jQuery("#floor_goods_info").disableSelection();
});

function save_form() {
	var floorMap = [];
	jQuery(".floor_box_pls .floor_pro_img img").each(function () {
		var obj = new Object();
		obj.id = jQuery(this).attr("id");
		obj.seq = jQuery(this).attr("seq");
		floorMap.push(obj);
	});
	if (floorMap.length == 0) {
		layer.alert("请选择商品信息", {icon:7});
		return;
	}
	var limit = "${limit}";
	if (limit != "") {
		if (floorMap.length > limit) {
			layer.alert("商品不能超过" + limit, {icon:2});
			return;
		}
	}
	$("#saveForm").attr("disabled", "disabled");
	var productFloors = JSON.stringify(floorMap);
	$.ajax({
		url: contextPath+"/admin/subFloorItem/saveProductFloor",
		data: {"productFloors": productFloors, "sfId": _sfId, "fId": _fId, "layout": layout},
		type: 'post',
		dataType: 'json',
		async: true, //默认为true 异步   
		error: function (jqXHR, textStatus, errorThrown) {
			$("#saveForm").removeAttr("disabled");
			layer.alert(textStatus, errorThrown);
		},
		success: function (result) {
			if (result == "OK") {
				layer.msg('成功！', {icon:1, time:1000}, function(){
					parent.location.reload();
				});
			} else {
				$("#saveForm").removeAttr("disabled");
				layer.alert(result, {icon:2});
				return;
			}
		}
	});
}

function convertResopnse(result) {
	var value = "保存成功";
	if ("NN" == value) {
		value = "数据不允许为空";
	} else if ("PN" == value) {
		value = "你不能编辑别人的楼层";
	}
	return value;
}