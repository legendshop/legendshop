$("#payForm").validate({
	errorElement: "div", //可以用其他标签，记住把样式也对应修改
	errorClass:"ant-form-explain",
	rules : {
		merchantId: {
			required: true
		},
		key: {
			required: true
		}
	},
	messages : {
		merchantId: {
			required: '请输入商户号'
		},
		key: {
			required: '请输入安全校验码'
		}
	},
	submitHandler : function(from) {

		$(from).ajaxSubmit({
			dataType:"json",
			success: function(result) {
				if(result == "OK") {
					layer.msg("保存成功",{icon:1},function(){
						parent.layer.closeAll();
					});
				}else {
					layer.msg(result,{icon:2});
				}          	
			}
		});



	}
});


$(".ant-radio-input").click(function() {
	$(".ant-radio-input").parent().removeClass("ant-radio-checked");
	$(this).parent().addClass("ant-radio-checked");
	$("#payForm input[name='isEnable']").val($(this).val());
});

$(".ant-checkbox-input").click(function() {
	var on=$(this).attr("switch");
	if(on == "on" ){ //关闭状态
		$(this).parent().addClass("ant-checkbox-checked");
		$(this).prop("checked", true);
		$(this).attr("switch","off");
	}else{
		$(this).parent().removeClass("ant-checkbox-checked");
		$(this).prop("checked", false);
		$(this).attr("switch","on");
	}
});