$(document).ready(function() {
	jQuery.validator.addMethod("checkBrandName", function(value,element){
		var returnMsg=true;
		jQuery.ajax({
			url: contextPath+"/admin/brand/checkBrandByName",
			data: {"brandName" : $("#brandName").val(),"brandId" : $("#brandId").val()},
			type: "post",
			async: false,
			success: function(retData){
				if(retData=='true'){
					returnMsg=false;
				}
			}
		});
		return returnMsg;
	}, "此品牌已被创建!");


	var isSubmitted = false;
	jQuery("#form1").validate({
		rules: {
			brandName: {
				required: true,
				checkBrandName: true //期望的是true,如果为false则展示提示信息
			},
			seq: {
				number: true
			},
      file: {
        required: "#brandPic:blank",
      },
      mobileLogoFile: {
        required: "#brandPicMobile:blank",
      },
			brief:{
				"required":true,
				maxlength: 100
			}
		},
		messages: {
			brandName: {
				required: "请输入品牌"
			},
      file: {
        required:"请选择PC端品牌LOGO"
      },
      mobileLogoFile: {
        required:"请选择移动端品牌LOGO"
      },
			seq: {
				number: "请输入数字"
			},
			brief:{
				required:"请填写品牌介绍",
				maxlength: "品牌介绍不能超过100个字符！"
			}
		},

		submitHandler : function(form){
			//防止表单重复提交
			if(!isSubmitted){
				form.submit();
				isSubmitted = true;
			}else{
				layer.alert('请不要重复提交表单!', {icon: 0});
			}
		}
	});

	highlightTableRows("col1");
	//斑马条纹
	$("#col1 tr:nth-child(even)").addClass("even");
	KindEditor.options.filterMode=false;
	KindEditor.create('textarea[name="content"]', {
		cssPath : contextPath+'/resources/plugins/kindeditor/plugins/code/prettify.css',
		uploadJson : contextPath+'/editor/uploadJson/upload;jsessionid='+UUID,
		fileManagerJson : contextPath+'/editor/uploadJson/fileManager',
		allowFileManager : true,
		afterBlur:function(){this.sync();},
		width : '100%',
		height:'400px',
		afterCreate : function() {
			var self = this;
			KindEditor.ctrl(document, 13, function() {
				self.sync();
				document.forms['example'].submit();
			});
			KindEditor.ctrl(self.edit.doc, 13, function() {
				self.sync();
				document.forms['example'].submit();
			});
		}
	});

	$("#file").change(function(){
		checkImgType(this);
		checkImgSize(this,5120);
	});
	$("#mobileLogoFile").change(function(){
		checkImgType(this);
		checkImgSize(this,5120);
	});

	$("#bigImagefile").change(function(){
		checkImgType(this);
		checkImgSize(this,5120);
	});

	$("#brandAuthorizefile").change(function(){
		checkImgType(this);
		checkImgSize(this,5120);
	});

});
