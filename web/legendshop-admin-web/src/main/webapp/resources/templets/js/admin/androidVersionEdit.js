$(function() {

	$("input[type='button'].delete-android-version").on("click", function(){
		var type = $(this).attr("data");
		layer.confirm("您确定删除此安卓版本?", {
			icon: 3
			,btn: ['确定','关闭'] //按钮
		}, function(){
			var url = $("#downloadPage").val();
			$.ajaxUtils.sendPost(urls.del, {"type": type});
		});
	});

	$("input[type='button'].upload-android-version").on("click", function(){

		var type = $(this).attr("data");

		var title = "更新Android用户版版本";
		if(type === "shop"){
			title = "更新Android商家版版本";
		}

		layer.open({
			title: title,
			type: 2, 
			content: [urls.uploadPage + "/" + type,'no'], 
			area: ['600px', '260px']
		}); 
	});
});