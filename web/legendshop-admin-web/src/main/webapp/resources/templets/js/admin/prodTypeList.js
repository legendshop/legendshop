function loadCategoryDialog(typeId) {
	layer.open({
		title : "关联分类",
		id : "RoleMenu",
		type: 2, 
		content: contextPath+"/admin/category/loadCategoryCheckbox/"+typeId,
		area: ['270px', '410px']
	});
}

function deleteById(id) {
	$.ajax({
		url : contextPath+"/admin/category/ifDeleteProdType",
		data : {"prodTypeId" : id},
		type : "post",
		async : true,
		dataType : "json",
		error : function() {
		},
		success : function(result) {
			if (result == true) {
				layer.alert("此类型正在被使用，不能删除！", {icon:2});
			} else {
				layer.confirm(" 确定删除 ?", {icon:3, title:'提示'}, function() {
					layer.msg('删除成功', {icon:1, time:700}, function(){
						window.location = contextPath+"/admin/prodType/delete/" + id;
					});
				});
			}
		}
	});
}

function pager(curPageNO) {
	document.getElementById("curPageNO").value = curPageNO;
	document.getElementById("form1").submit();
}

$("button[name='StatusImg']").click(function(event) {
	$this = $(this);
	initStatus($this.attr("itemId"), $this.attr("itemName"), $this.attr("status"),contextPath+"/admin/prodType/updatestatus/", $this,contextPath);
});

//加载分类用于搜索
function loadCategoryDialogForSrh() {
	layer.open({
		title: "选择分类",
		id: 'prodCat',
		type: 2,
		content: contextPath+"/admin/product/loadAllCategory/P",
		area: ['280px', '400px']
	});
}

function clearCategoryCallBack() {
	$("#prodCatId").val("");
	$("#prodCatName").val("");
}

function loadCategoryCallBack(id, catName) {
	$("#prodCatId").val(id);
	$("#prodCatName").val(catName);
	layer.closeAll();
}

function search() {
	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	$("#form1")[0].submit();
}