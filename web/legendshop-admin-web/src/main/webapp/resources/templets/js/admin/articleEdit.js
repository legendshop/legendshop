$(document).ready(function() {
	initKindEditor();
	validateFrom();

	laydate.render({
		type: 'datetime',
		format: 'yyyy-MM-dd HH:MM:ss',
		elem: '#issueTime',
		calendar: true,
		theme: 'grid',
		trigger: 'click',
		value: new Date()
	});
});
//对表单进行校验
function validateFrom() {
	//添加新的校验规则（图片）
	$.validator.addMethod("picFile", function(value) {
		var picValue = $("#pic").val();
		if (value != "" || picValue != "") {
			return true;
		} else {
			return false;
		}
	}, "请选择图片");

	jQuery("#form1").validate({
		rules : {
			name : {
				required : true,
				maxlength : 50,
			},
			author : {
				required : true,
				maxlength : 50,
			},
			intro : {
				required : true,
				maxlength : 300,
			},
			summary : {
				required : true,
				maxlength : 300,
			},
			type : {
				required : true,
			},
			picFile : "picFile",
			status : {
				required : true,
			},
			content : {
				required : true,
			},
			issueTime : {
				required : true,
			}
		},
		messages : {
			name : {
				required : "请输入文章名称",
				maxlength : "请输入文章名称长度小于50",
			},
			author : {
				required : "请输入作者",
				maxlength : "请输入作者长度小于50",
			},
			intro : {
				required : "请输入简介",
				maxlength : "请输入作者长度小于300",
			},
			summary : {
				required : "请输入概要",
				maxlength : "请输入作者长度小于300",
			},
			picFile : {
				required : "请选择图片",
			},
			issueTime : {
				required : "请选择发表时间",
			}
		}
	});
}


$("input[type=file]").change(function(){
	checkImgType(this);
	checkImgSize(this,1024);
});
//初始化富文本编辑器
function initKindEditor() {
	//斑马条纹
	$("#col1 tr:nth-child(even)").addClass("even");
	KindEditor.options.filterMode = false;
	KindEditor.create('textarea[name="content"]', {
		cssPath : contextPath+'/resources/plugins/kindeditor/plugins/code/prettify.css',
		uploadJson : contextPath+'/editor/uploadJson/upload;jsessionid='+UUID,
		fileManagerJson : contextPath+'/editor/uploadJson/fileManager',
		allowFileManager : true,
		afterBlur : function() {
			this.sync();
		},
		width : '100%',
		height : '300px',
		afterCreate : function() {
			var self = this;
			KindEditor.ctrl(document, 13, function() {
				self.sync();
				document.forms['example'].submit();
			});
			KindEditor.ctrl(self.edit.doc, 13, function() {
				self.sync();
				document.forms['example'].submit();
			});
		}
	});
	prettyPrint();
}