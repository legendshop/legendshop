$(document)
.ready(
		function() {
			jQuery("#form1")
			.validate(
					{
						rules : {
							name : {
								required : true,
							},
							dvyId : {
								required : true,
							},
							printtempId : {
								required : true,
							},
							isSystem : {
								required : true,
							}
						},
						messages : {
							name : {
								required : '<fmt:message key="name.required"/>',
							},
							dvyId : {
								required : '<fmt:message key="deliverytype.dvyid.required"/>',
							},
							// printtempId : {
							// 	required : "请设置物流配送打印模版！",
							// },
							isSystem : {
								required : "对不起, 请选择是否是系统默认模板!",
							}
						}
					});

			//斑马条纹
			$("#col1 tr:nth-child(even)").addClass("even");
			highlightTableRows("col1");
		});
