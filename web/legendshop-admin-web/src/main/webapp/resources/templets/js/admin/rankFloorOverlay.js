if(typeof JSON == 'undefined'){$('head').append($("<script type='text/javascript' src='"+contextPath+"/resources/common/js/json.js'>"));}
function productFloorLoad(curPageNO){
	var referId = $("#sort").val();
	var prodName = $("#prodName").val();
	var sortId ;
	var nsortId ;
	var data ;
	if(referId.substring(0,1)==1){
		sortId =referId.substring(2);
		data = {
				"curPageNO": curPageNO,
				"sortId": sortId,
				"name" :prodName
		};
	}else if(referId.substring(0,1)==2){
		nsortId = referId.substring(2);
		var data = {
				"curPageNO": curPageNO,
				"nsortId": nsortId,
				"name" :prodName
		};
	}else{
		var data = {
				"curPageNO": curPageNO
		};
	}
	$.ajax({
		url:contextPath+"/admin/subFloorItem/productFloorLoad/",
		data:data,
		type:'post', 
		async : true,  
		error: function(jqXHR, textStatus, errorThrown) {
		},
		success:function(result){
			jQuery("#floor_goods_list").empty().append(result);	
		}
	});
}

jQuery(document).ready(function(){
	jQuery("#floor_goods_info").sortable();
	jQuery("#floor_goods_info").disableSelection();
});

function save_form(){
	var floorMap = [];
	jQuery(".floor_box_pls .floor_pro_img img").each(function(){
		var obj = new Object();
		obj.id = jQuery(this).attr("id");
		floorMap.push(obj);
	});
	var rankFloors = JSON.stringify(floorMap);
	$.ajax({
		url:contextPath+"/admin/floorItem/saveRankFloor", 
		data: {"rankFloors":rankFloors,"flId":${flId}},
		type:'post', 
		dataType : 'json', 
		async : true, //默认为true 异步   
		error: function(jqXHR, textStatus, errorThrown) {
			layer.alert(textStatus, errorThrown);
		},
		success:function(result){
			layer.msg("保存成功", {icon:1, time:1000}, function(){
				parent.location.reload();
			});
		}
	});  
}
function selectProductPager(curPageNO){
	productFloorLoad(curPageNO);
}