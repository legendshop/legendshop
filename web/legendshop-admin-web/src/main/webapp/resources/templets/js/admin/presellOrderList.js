function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	sendData(curPageNO);
}

$(document).ready(function() {
	//当输入框的值改变时,改变订单金额
	$("#preDepositPrice,#finalPrice,#freightAmount").change(function(){
		changePreOrderFee();//改变订单金额
	});

	laydate.render({
		elem: '#startDate',
		calendar: true,
		theme: 'grid',
		trigger: 'click'
	});

	laydate.render({
		elem: '#endDate',
		calendar: true,
		theme: 'grid',
		trigger: 'click'
	});
	
	if(shopName == ''){
        makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'shopName');
    }else{
    	makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",shopName, 'shopName');
		$("#select2-chosen-1").html(shopName);
    }

	//页面加载模块
	$("b[mark^='name_']").each(function (){
		var _this=$(this);
		var addrOrderId=$(_this).attr("addrOrderId");
		if(addrOrderId!=null || addrOrderId!=undefined || addrOrderId!=""){
			$.ajax({
				url:contextPath+"/admin/order/findUsrAddrSub/"+addrOrderId, 
				type:'POST', 
				async : false, //默认为true 异步   
				dataType:'json',
				error: function(jqXHR, textStatus, errorThrown) {
				},
				success:function(result){
					var html="<div style='display:none;' class='xx'>";
					html += " <h6>买家信息</h6>";
					html += " <ul id='table_56'>";
					html += "<li><div> <span class='xx_sp'>姓 名：</span><span class='xx_date'>"+result.receiver+"</span></div></li>";
					html += "<li><div> <span class='xx_sp'>电 话：</span><span class='xx_date'>"+result.telphone+"</span></div></li>";
					html += "<li><div> <span class='xx_sp'>手 机：</span><span class='xx_date'>"+result.mobile+"</span></div></li>";
					html += "<li><div> <span class='xx_sp'>邮 编：</span><span class='xx_date'>"+result.subPost+"</span></div></li>";
					html += "<li><div> <span class='xx_sp'>地 址：</span><span class='xx_date'>"+result.detailAddress+"</span></div></li>";
					html += "</ul></div>";
					$(_this).append(html);
				}


			});
		}
	});
	sendData(1);
});

//改变订单金额
function changePreOrderFee(){
	var preDepositPrice = $("#preDepositPrice").val();
	var finalPrice = $("#finalPrice").val();
	var freightAmount = $("#freightAmount").val();

	var totalPrice = Number(preDepositPrice)+Number(finalPrice)+Number(freightAmount);
	$("#totalPrice").val(totalPrice);
	$("#totalPriceText").text("￥" + totalPrice);
}

//取消订单
function orderCancel(){
	var subNumber = $("#sub_id").val();
	if(isBlank(subNumber)){
		return;
	}
	var other=$("input:radio[name=state_info]:checked").val();
	if(isBlank(other)){
		layer.alert("请选择设置取消原因", {icon:2});
		return;
	}
	var info = ""; 
	if(other == "其他原因"){
		var otherInfo = $('#other_state_info').val();
		if(isBlank(otherInfo)){
			layer.alert("请填写备注信息", {icon:2});
			return;
		}
		info = otherInfo;
	}else{
		info = other;
		$('#other_state_info').val("");
	}
	$.ajax({
		url : contextPath+"/admin/order/orderCancel", 
		data : {"subNumber":subNumber,"status":5,"other":info},
		type : "POST", 
		async : false, //默认为true 异步   
		dataType : "json",
		error: function(jqXHR, textStatus, errorThrown) {
			$(".Popup_shade").css("display","none"); 
			$("#order_cancel").css("display","none"); 
			return;
		},
		success:function(result){
			if(result == "OK"){
				$(".Popup_shade").css("display","none"); 
				$("#order_cancel").css("display","none"); 
				layer.msg("取消订单成功！",  {icon:1, time:1000}, function(){
					window.location.reload(true);
				});
			}else if(result == "fail"){
				layer.alert("取消订单失败!", {icon:2});
				$(".Popup_shade").css("display","none"); 
				$("#order_cancel").css("display","none"); 
				return;
			}
		}
	});
}

//显示取消订单窗口
function orderCancelShow(_subnumber){
	if(_subnumber==null){
		return;
	}
	$("#sub_id").val(_subnumber);
	$("#sub_id").parent().next().html(_subnumber);
	$(".Popup_shade").css('display','block'); 
	$("#order_cancel").css('display','block'); 
}

//关闭取消订单窗口
function closeOrderCancel(){
	$("#sub_id").val();
	$(".Popup_shade").css('display','none'); 
	$("#order_cancel").css('display','none'); 
}
var payPctTypeTemp=0;
//显示调整订单费用窗口
function changeAmountShow(subNumber,userName,depositPrice,finalPrice,freightAmount,actualTotalPrice,payPctType){
	if(isBlank(actualTotalPrice) || isBlank(userName) || isBlank(depositPrice) || isBlank(finalPrice) || isBlank(freightAmount) || isBlank(actualTotalPrice||payPctType)){
		return;
	}
	$("#orderFeeIdText").text(subNumber);
	$("#orderFeeId").val(subNumber);
	$("#orderFeeUserNameText").text(userName);
	$("#orderFeeUserName").val(userName);
	$("#preDepositPrice").val(new Number(depositPrice));
	$("#finalPrice").val(new Number(finalPrice));
	$("#freightAmount").val(new Number(freightAmount));
	$("#totalPrice").val(new Number(actualTotalPrice));
	$("#totalPriceText").text(new Number(actualTotalPrice));
	$(".Popup_shade").css("display","block"); 
	$("#order_fee").css("display","block"); 

	if(payPctType == 1){
		$("#finalPrice").css("display","block"); 
	}else{
		$("#finalPrice").css("display","none"); 
	}
	payPctTypeTemp=payPctType;
}

//关闭调整费用窗口
function closeOrderFee(){
	$("#orderFeeIdText").text("");
	$("#orderFeeId").val("");
	$("#orderFeeUserNameText").text("");
	$("#orderFeeUserName").val("");
	$("#preDepositPrice").val("");
	$("#finalPrice").val("");
	$("#freightAmount").val("");
	$("#totalPrice").val("");
	$("#totalPriceText").text("");

	$(".Popup_shade").css("display","none"); 
	$("#order_fee").css("display","none"); 
}

//校验调整后的数据
function validate(subNumber,preDepositPrice,finalPrice,freightAmount){
	if(isBlank(subNumber)){
		layer.alert("非法操作!", {icon:2});
		return false;
	}
	if(isBlank(preDepositPrice)){
		layer.alert("请输入商品定金!", {icon:2});
		return false;
	}
	if(isBlank(finalPrice)){
		layer.alert("请输入商品定金!", {icon:2});
		return false;
	}
	if(!isMoney(preDepositPrice)){
		layer.alert("请输入正确的商品定金，金额最多只能有两位小数!", {icon:2});
		return false;
	}
	if(parseFloat(preDepositPrice)<=0){
		layer.alert("商品定金必须大于0!", {icon:2});
		return false;
	}
	if(!isMoney(finalPrice)){
		layer.alert("请输入正确的商品尾款，金额最多只能有两位小数!", {icon:2});
		return false;
	}
	if(payPctTypeTemp==1&&parseFloat(finalPrice)<=0){
		layer.alert("商品尾款必须大于0!", {icon:2});
		return false;
	}
	if(!isMoney(freightAmount)){
		layer.alert("请输入正确的配送运费!", {icon:2});
		return false;
	}
	return true;
}

//提交调整费用信息
function submitPreOrderFee(){
	var subNumber = $("#orderFeeId").val();
	var preDepositPrice = $("#preDepositPrice").val();
	var finalPrice  = $("#finalPrice").val();
	var freightAmount = $("#freightAmount").val();

	if(validate(subNumber,preDepositPrice,finalPrice,freightAmount)){
		$("#orderFee").attr("disabled","disabled");
		$.ajax({
			url : contextPath+"/admin/order/changePreOrderFee", 
			data : {"subNumber":subNumber,"preDepositPrice":preDepositPrice,"finalPrice":finalPrice,"freightAmount":freightAmount},
			type : "POST", 
			async : false, //默认为true 异步   
			dataType : "JSON",
			error: function(jqXHR, textStatus, errorThrown) {
				closeOrderFee();
				$("#orderFee").attr("disabled","");
				return;
			},
			success:function(result){
				if(result=="OK"){
					closeOrderFee();
					layer.msg("调整订单费用成功!",{icon:1, time:1000}, function(){
						window.location.reload(true);
					});
				}else if(result=="fail"){
					closeOrderFee();
					layer.alert("调整订单费用失败!", {icon:2});
					$("#orderFee").attr("disabled","");
					return;
				}
			}
		});
	}
}

//确认发货
function deliverGoods(_orderId){
	layer.open({
		title :  "确认发货",
		id : "deliverGoods",
		type: 2,
		content: contextPath+'/admin/order/deliverGoods/'+_orderId,
		area: ['430px', '300px']
	});
}

//导出Excel
function export_excel(){
	var list=$(".orderlist_one").get();
	if(list.length<=0){
		layer.msg("没有导出的订单数据",{icon:0});
		return;
	}
	var orders=new Array();
	for(var i = 0; i < list.length; i++){
		var _orderId=$(list[i]).attr("orderId");
		if(_orderId!=undefined  && _orderId!="" && _orderId!=null && _orderId!=0){
			orders.push(_orderId);
		}
	}
	if(orders.length<=0){
		layer.msg("没有导出的订单数据",{icon:0});
		return;
	}
	$(".editAdd_load").show();
	jQuery.ajax({
		url:contextPath+"/admin/order/exportExcel",
		data:{"items":orders},
		type:"post", 
		async : false, //默认为true 异步  
		dataType : 'json',  
		error:function(data){
			$(".editAdd_load").hide();
			layer.msg("导出数据失败",{icon:0});
			return ;
		},   
		success:function(data){
			if(data.result=="false"){
				$(".editAdd_load").hide();
				layer.msg(data.msg,{icon:1});
				return
			}
			else if(data.result=="true"){
				$(".editAdd_load").hide();
				var url=contextPath+"/servlet/downloadfileservlet"+data.msg;
				window.open(url);
				return;
			}
		}   
	});  
}

function switch_reason(obj){
	var ck = $(obj).attr("id");
	if(ck=="radio3"){
		$("#other_reason").show();
	}else{
		$("#other_reason").hide();  
	}
} 

//不知道这是什么=鬼?
$("b[mark^='name_']").hover(function(){
	$(".xx").hide();
	$(this).parent().parent().find("div.xx").show();
},function(){
	$(".xx").hide();
});

//是否是正确金额
function isMoney(obj) {
	if (!obj)
		return false;
	return (/^\d+(\.\d{1,2})?$/).test(obj);
}

//是否为空
function isBlank(value){
	return value == undefined || value == null || $.trim(value) == "";
}
function search(){
	$("#ListForm #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	sendData(1);
	
}

function sendData(curPageNO) {
    var data = {
        "shopId": $("#shopId").val(),
        "curPageNO": curPageNO,
        "status": $("#status").val(),
        "subNumber": $("#subNumber").val(),
        "userName": $("#userName").val(),
        "startDate": $("#startDate").val(),
        "endDate": $("#endDate").val(),
        "shopName":shopName
    };
    $.ajax({
        url: contextPath+"/admin/order/queryPresellOrdersContent",
        data: data,
        type: 'post',
        async: true, //默认为true 异步   
        success: function (result) {
            $("#presellOrderContentList").html(result);
        }
    });
}

//导出
function exportAll(){
	var status = $("#status").val();
	var shopId = $("#shopId").val();
	var subNumber = $("#subNumber").val();
	var userName = $("#userName").val();
	var startDate = $("#startDate").val();
	var endDate = $("#endDate").val();
	var array = [];
	array.push(status);
	array.push(shopId);
	array.push(subNumber);
	array.push(userName);
	array.push(startDate);
	array.push(endDate);
	var jsonData = encodeURI(JSON.stringify(array));
	window.location.href = contextPath + "/admin/order/exportPresellOrders?data=" + jsonData;
}

function makeSelect2(url,element,text,label,value,holder, textValue){
	$("#"+element).select2({
        placeholder: holder,
        allowClear: true,
        width:'200px',
        ajax: {  
    	  url : url,
    	  data: function (term,page) {
                return {
                	 q: term,
                	 pageSize: 10,    //一次性加载的数据条数
                 	 currPage: page, //要查询的页码
                };
            },
			type : "GET",
			dataType : "JSON",
            results: function (data, page, query) {
            	if(page * 5 < data.total){//判断是否还有数据加载
            		data.more = true;
            	}
                return data;
            },
            cache: true,
            quietMillis: 200//延时
      }, 
        formatInputTooShort: "请输入" + text,
        formatNoMatches: "没有匹配的" + text,
        formatSearching: "查询中...",
        formatResult: function(row,container) {//选中后select2显示的 内容
            return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
        },formatSelection: function(row) { //选择的时候，需要保存选中的id
        	$("#" + textValue).val(row.text);
            return row.text;//选择时需要显示的列表内容
        }, 
    });
}