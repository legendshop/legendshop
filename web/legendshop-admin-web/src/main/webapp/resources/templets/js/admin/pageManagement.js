$(document).ready(function() {

	//页面加载请求加载内容
	sendData(1);
	
	//点击显示修改页面名称的输入框
	$("body").on("click",".edit-name",function(){
		$(this).parent(".name-txt").hide();
		$(this).parent().next(".name-inp").show();
		$(this).parent().next(".name-inp").next().show();
	});
	
	//确认修改页面名称
	$("body").on("click",".confirm",function(){
		var name = $(this).prev().val();
		var pageId = $(this).data("id");
		if(!name){
			layer.msg('页面名称不能为空',{icon: 0});
			return;
		}
		if (!$.trim(name)){
            layer.msg('页面名称不能全部为空格',{icon: 0});
            return;
		}
		var e = this;
		$.ajax({
			url: contextPath+"/admin/app/pageManagement/changeName", 
			type:'post', 
			data:{"pageId":pageId,"name":name},
			async : false, //默认为true 异步   
			dataType:"json",
			success:function(data){
				if(data=="OK"){
					layer.msg('更新成功',{icon: 1});
					$(e).prev().prev().html(name+"<i class='edit-name'></i>");
				}else{
					layer.msg('更新失败',{icon: 2});
				}
				$(e).prev().hide();
				$(e).hide();
				$(e).prev().prev().show();
			}   
		});
	});
	
	
	
});

//搜索
function search(){
	$("#ListForm #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	sendData(1);
}


function pager(curPageNO){
    document.getElementById("curPageNO").value=curPageNO;
    sendData(curPageNO);
}

//异步加载内容
function sendData(curPageNO) {
    var data = {
        "curPageNO": curPageNO,
        "name": $("#name").val(),
        "status": $("#status").val(),
        "flag":"1",
        "category":category
    };
    $.ajax({
        url: contextPath+"/admin/app/pageManagement/query",
        data: data,
        type: 'post',
        async: true, //默认为true 异步   
        success: function (result) {
            $("#pageManagementContent").html(result);
        }
    });
}

//新建页面
function newPage(){
	window.location.href=contextPath+"/admin/app/pageManagement/createPage?category="+category;
}

//编辑页面
function edit(id){
	window.location.href=contextPath+"/admin/app/pageManagement/editPage/"+id;
}


//设为主页
function use(id){
	
	$.ajax({
		url: contextPath+"/admin/app/pageManagement/use/"+id, 
		type:'post', 
		async : false, //默认为true 异步   
		dataType:"json",
		success:function(data){
			if(data=="OK"){
				layer.msg('设为首页成功',{icon: 1});
				setTimeout(window.location.href=contextPath+"/admin/app/pageManagement/query?category="+category,6000);
			}else{
				layer.msg('设为首页失败',{icon: 2});
				setTimeout(window.location.href=contextPath+"/admin/app/pageManagement/query?category="+category,6000);
			}
		}   
	});
}

//复制页面
function clone(id){
	
	$.ajax({
		url: contextPath+"/admin/app/pageManagement/clone/"+id, 
		type:'post', 
		async : false, //默认为true 异步   
		dataType:"json",
		success:function(data){
			if(data=="OK"){
				layer.msg('复制成功',{icon: 1});
				setTimeout(window.location.href=contextPath+"/admin/app/pageManagement/query?category="+category,6000);
			}else{
				layer.msg('复制失败',{icon: 2});
				setTimeout(window.location.href=contextPath+"/admin/app/pageManagement/query?category="+category,6000);
			}
		}   
	});
}

//删除页面
function del(id){
	
	layer.confirm("确认删除该页面?", function () {
		$.ajax({
			url: contextPath+"/admin/app/pageManagement/delete/"+id, 
			type:'post', 
			async : false, //默认为true 异步   
			dataType:"json",
			success:function(data){
				if(data=="OK"){
					layer.msg('删除成功',{icon: 1});
					setTimeout(window.location.href=contextPath+"/admin/app/pageManagement/query?category="+category,6000);
				}else{
					layer.msg('删除失败',{icon: 2});
					setTimeout(window.location.href=contextPath+"/admin/app/pageManagement/query?category="+category,6000);
				}
			}   
		});
	}); 
}

//发布页面
function release(id){
	
	layer.confirm("确认发布该页面?", function () {
		$.ajax({
			url: contextPath+"/admin/app/pageManagement/release/"+id, 
			type:'post', 
			async : false, //默认为true 异步   
			dataType:"json",
			success:function(data){
				if(data=="OK"){
					layer.msg('发布成功',{icon: 1});
					setTimeout(window.location.href=contextPath+"/admin/app/pageManagement/query?category="+category,6000);
				}else{
					layer.msg(data,{icon: 2});
					setTimeout(window.location.href=contextPath+"/admin/app/pageManagement/query?category="+category,6000);
				}
			}   
		});
	}); 
}


//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}
