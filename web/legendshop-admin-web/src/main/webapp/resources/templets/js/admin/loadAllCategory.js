function selectCategory(){
	var zTree = $.fn.zTree.getZTreeObj("catTrees");
	var chkNodes = zTree.getCheckedNodes(true);
	if(chkNodes.length<1) {
		layer.msg("请选择类目", {icon:2});
		return;
	}
	var catId = chkNodes[0].id;
	var catName = chkNodes[0].name;
	var level= chkNodes[0].level;
	//保存分类信息
	window.parent.saveChoooseCat(catId,catName,sourceId);
	var index = parent.layer.getFrameIndex('loadAllCategory'); //先得到当前iframe层的索引
	parent.layer.close(index); //再执行关闭 
}

function clearCategory(){
	if(window.parent.document.getElementById('categoryName')!=null){
		window.parent.document.getElementById('categoryName').value='';
	}
	var index = parent.layer.getFrameIndex('loadAllCategory'); //先得到当前iframe层的索引
	parent.layer.close(index); //再执行关闭 
}

$(document).ready(function(){
	//获取节点
	var catTrees=JSON.parse(_catTrees);
	
	//zTree设置
	var setting = {
			check:{
				enable: true,
				chkboxType: {'Y':'ps','N':'ps'},
				chkStyle: 'radio',
				radioType: 'all'
			}, 
			data:{
				simpleData:{ enable: true }
			}, callback:{
				beforeCheck: beforeCheck,
				onCheck: onCheck
			}};
	$.fn.zTree.init($("#catTrees"), setting, catTrees);
});

function beforeCheck(treeId, treeNode, clickFlag) {
	var all="${all}";
	if(all!=undefined && all!=null && all!=""){
		return true;
	}
	if(treeNode.isParent){
		layer.alert("请选择最底层分类", {icon:2});
	}
	return !treeNode.isParent;//当是父节点 返回false 不让选取
}

function onCheck(){
}