$(document).ready(function() {
	//图片上传tab
	$(".multimage-wrapper .multimage-tabs .tab").click(function(){
		$(this).addClass("actived").siblings().removeClass("actived");
		if($(this).attr("type")=="remote-image"){
			//if($(".multimage-panels .remote-image").html().trim()==""){
			$(".multimage-panels .remote-image").html(
					"<iframe style='height:330px;width:810px;border:0px;' src='"+contextPath+"/imageAdmin/remoteImages'></iframe>"
			);
			//}
			$(".multimage-panels .local-panel").hide();
			$(".multimage-panels .remote-image").show();
		}else{
			$(".multimage-panels .local-panel").show();
			$(".multimage-panels .remote-image").hide();
		}
	});
});

function appendImg(img,url){
	console.log('------select image-----', url,photoPath);
	if(img=="" || url==""){
		layer.alert("图片加载失败，请检查url是否正确", {icon:2});
		return;
	}
	var _img=photoPath+url;
	console.log('------select _img-----', _img);
	var img = new Image();
	img.src = _img;
	img.onerror = function(){
		url = '';
		layer.alert("图片加载失败，请检查url是否正确", {icon:2});
	};
	if(img.complete){
		if(showsize!="" && showsize!=null && showsize!=undefined ){
			var size =showsize.split("*");
			console.log(img.width+" "+img.height +" showsize ="+showsize);
			if (img.width > size[0] || img.height > size[1]) {
				url = '';
				layer.alert("图片不得大于" + size[0] + "*" + size[1] + "px", {icon:2});
			}
		}
	}else{
		img.onload = function(){
			if(showsize!="" && showsize!=null && showsize!=undefined ){
				var size =showsize.split("*");
				console.log(img.width+" "+img.height +" showsize ="+showsize);
				if (img.width > size[0] || img.height > size[1]) {
					url = '';
					layer.alert("图片不得大于" + size[0] + "*" + size[1] + "px", {icon:2});
				}
			}
			img.onload=null;//避免重复加载
		}
	}
	setTimeout(
			function(){
				if(url!="" && url!=null){
					window.parent.saveChoooseImage(_img,url,sourceId);
					var index = parent.layer.getFrameIndex('loadImages'); //先得到当前iframe层的索引
					parent.layer.close(index); //再执行关闭  
				}else{
					return false;
				}
			},200);  
}

//宝贝图片处的文件上传，通过Ajax方式上传图片
function uploadFile(){

	var jsonData = new FormData();  
	var  files=$('#file').get(0).files;
	for (i = 0; i < files.length; i++) {
		var file= files[i];
		if(!checkImgType(file)){
			layer.alert("仅支持JPG、GIF、PNG、JPEG、BMP格式，大小不超过500k", {icon:2});
			return false;
		}else if(!checkImgSize(file,500)){
			layer.alert("仅支持JPG、GIF、PNG、JPEG、BMP格式，大小不超过500k", {icon:2});
			return false;
		}
		jsonData.append("files[]", file); 
	}
	//jsonData.append("files", files);  
	$.ajax({  
		type: "post",  
		url: contextPath + "/admin/app/decorate/prodpicture/save",     
		dataType: 'json',  
		data: jsonData,                          
		processData: false,  
		contentType: false, 
		success : function(response) {
			if(response =="OK"){
				layer.alert("上传成功,请用鼠标点击就可以选择图片信息", {icon:1});
				$(".multimage-wrapper .multimage-tabs .tab").eq(1).click();
			}else{
				layer.alert("仅支持JPG、GIF、PNG、JPEG、BMP格式，大小不超过500k", {icon:2});
			}
		}
	}); 
}

function checkImgType(file){
	if (!/\.(JPEG|BMP|GIF|JPG|PNG)$/.test(file.name.toUpperCase())) {
		return false;
	}
	return true;
}

function checkImgSize(file,limitSize){ 
	try {
		var maxsize = limitSize*1024;
		var msgSize = limitSize+"K";
		if(limitSize>=1024){
			msgSize = limitSize/1024+"M";
		}
		var errMsg = "上传的图片不能超过"+msgSize;
		if($(file).size>maxsize){
			return false;
		}
	} catch (e) {

	}
	return true;
}