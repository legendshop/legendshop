$(function() {

	if (isBlank(theme)) {
		theme = 'green';
	}
	
	$(".color-item").each(function(){
		if($(this).attr("theme") == theme){
			$(this).addClass("cur");
			var img = $(this).attr("data-img");
			$(".style-img img").attr("src",contextPath+img);
		}
	});
	
	$(".color-item").on("click",function(){
		$(".item-row").find(".cur").removeClass("cur");
		var img = $(this).attr("data-img");
		$(".style-img img").attr("src",contextPath+img);
		$(this).addClass("cur");
	});
	
});

/** 保存主题风格 */
function save(){
	var theme = $(".item-row").find(".cur").attr("theme");
	var color = $(".item-row").find(".cur").attr("color");
	var url = contextPath + "/admin/appSetting/saveThemeStyle";
	 $.ajax({
       url: url,
       type : "POST",
       data:{"theme":theme, "color": color}, 
	   dataType : "JSON", 
       async: false, //默认为true 异步   
       success: function (data) {
           if (data == "OK") {
           	layer.msg('保存成功',{icon:1});
           	setTimeout(function(){
           		parent.window.location.href = contextPath + "/admin/appSetting/themeStyle";}, 1000);  
           } else {   
        	   layer.msg(data,{icon:2});
           }
       } 
   });
}

//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}
