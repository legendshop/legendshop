$(document).ready(function() {
	jQuery("#form1").validate({
		rules : {
			icpInfo : {
				required : true,
				maxlength : 5000
			},
			logo : "required",
			shopName : {
				required : true,
				maxlength : 15
			},
			url : {
				required : true,
				maxlength : 100
			},
			supportMail : {
				required : true,
				checkEmail : true,
				maxlength : 100
			},
			internationalCode : {
				required : true,
				maxlength : 5
			},
			areaCode : {
				required : true,
				maxlength : 10
			},
			telephone : {
				required : true,
				maxlength : 20
			},
			wechatServiceCodeFile : {
				checkImage: true
			},
			wechatCodeFile : {
				checkImage: true
			},
			file : {
				checkImage: true
			}
		},
		messages : {
			icpInfo : "请输入icpInfo",
			logo : "请输入logo",
			shopName : {
        required: "请输入商城名称",
        maxlength: "长度不能超过15个字符"
      },
			url : "请输入url",
			supportMail : {
				required : "请输入supportMail",
				checkEmail : "您输入的多个邮箱中有邮箱格式不正确，请重新核对后再输入!"
			},
			internationalCode : "输入数字",
			areaCode : "输入数字",
			telephone : "输入数字"
		}
	});

	//斑马条纹
	$("#col1 tr:nth-child(even)").addClass("even");
});

jQuery.validator.addMethod("checkImage", function(value, element) {
	var obj_file = $(element).get(0).files;
    for(var i=0;i<obj_file.length;i++){
		if(obj_file[i].size>1024*1024){
			return false;
		}
	}
    return true;
}, "图片大小不得大于1M");

jQuery.validator.addMethod("checkEmail", function(value, element) {
	var reg = /^(\w)+(\.\w+)*@(\w)+((\.\w{2,3}){1,3})$/;
	var emails = value.split(",");
	if (emails.length > 0) {
		for (var i = 0; i < emails.length; i++) {
			var email = emails[i];
			if (isBlank(email)) {
				return false;
			} else if (!reg.test(email)) {
				return false;
			}
		}
		return true;
	} else {
		return reg.test(value);
	}
}, "您输入的多个邮箱中有邮箱格式不正确，请重新核对后再输入!");

function isBlank(value) {
	return value == undefined || value == null || $.trim(value) === "";
}
