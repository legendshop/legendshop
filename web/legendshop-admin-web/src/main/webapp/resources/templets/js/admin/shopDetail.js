$(document).ready(function() {
	jQuery("#form1").validate({
		rules: {
			siteName: {
				required: true,
				minlength: 2
			},
			contactName: "required",
			contactMobile: {
				isMobile: true,
				required: true
			},
			auditOpinion: {
				maxlength: 100
			},
		},
		messages: {
			contactName: {
				required: '<fmt:message key="errors.required"><fmt:param value=""/></fmt:message>'
			},
			contactMobile: {
				required: '<fmt:message key="errors.required"><fmt:param value=""/></fmt:message>'
			},
			siteName: {
				required: '<fmt:message key="errors.required"><fmt:param value=""/></fmt:message>',
				minlength: '<fmt:message key="errors.minlength"><fmt:param value=""/><fmt:param value="2"/></fmt:message>'
			},
			auditOpinion: {
				maxlength: '<fmt:message key="errors.maxlength"><fmt:param value=""/><fmt:param value="2"/></fmt:message>'
			}
		}
	});

	$("select.combox").initSelect();
	var type= '${shopDetail.type}';
	if(type==0){
		$("#status option[value='-5']").remove();
	}

});

function isMobile(str){
	var reg = /^1\d{10}$/;
	return reg.test(str);
}

jQuery.validator.addMethod("isMobile", function(value, element) {
	return isMobile(value);}, '请输入正确的手机号码');


function change(id){
	var text=$("#changeRequireAudit").val();
	var oldData;
	if(text=='修改'){
		oldData= $("#prodRequireAudit option:selected").val();
		$("#prodRequireAudit").attr("disabled",false);
		$("#prodRequireAudit").focus();
		$("#prodRequireAudit").css("border","1px solid red");
		$("#changeRequireAudit").val("保存");
	}else{
		var newData= $("#prodRequireAudit option:selected").val();
		if(oldData==newData){
			return false;
		}
		$.ajax({
			url: contextPath+"/admin/shopDetail/update", 
			data:{"shopId":id,"requireAudit":newData},
			type:'post', 
			async : false, //默认为true 异步   
			dataType:'json',
			success:function(result){
				if(result=="OK"){
					layer.msg("保存成功",{icon: 1},function(){
						window.location.reload(true);
					});
					return true;
				}else{
					layer.msg("保存失败",{icon: 2});
					return false;
				}
			}
		});
	}
}
function queryShopProd(){
	var url = encodeURI(contextPath+"/admin/product/query?siteName="+siteName);
	window.location.href = url;
}