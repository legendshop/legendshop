$(document).ready(function () {
	$(".pro-right-tit ul li").bind("click", function () {
		$(this).siblings().removeClass("arrow");
		$(this).addClass("arrow");
		var url = contextPath+"/admin/subFloorItem/sendFloorProduct/" + $(this).val();
		sendData(url);
	});

	$("div[class='recommend-classes']").hover(function () {
		$("#sortsEdit").show();
	}, function () {
		$("#sortsEdit").hide();
	});

	$("div[class='tabs-panel middle-goods-list']").hover(function () {
		$("#prodEdit").show();
	}, function () {
		$("#prodEdit").hide();
	});


	$("div[class='left-ads-con']").hover(function () {
		$("#floorImage").show();
	}, function () {
		$("#floorImage").hide();
	});

	//子楼层产品数据
	//$(".l1:eq(0)").addClass("focus");
  var floorItemId = $(".pro-right-tit ul li:eq(0)").val();

  if (!isBlank(floorItemId)){
    var url = contextPath+"/admin/subFloorItem/sendFloorProduct/" + floorItemId;
    sendData(url);
  }

});

function onclickTitle() {
	layer.open({
		title: '编辑分类',
		id: 'sortFloor',
		type: 2,
		content: contextPath+'/admin/floorItem/sortFloorOverlay/'+_flId+"?layout=wide_blend",
		area: ['650px', '450px'],
		cancel: function(index, layero){
			parent.location.reload();
		}
	});
}

function onclickProducts(id) {
	layer.open({
		title: '编辑产品',
		type: 2,
		content: contextPath+'/admin/subFloorItem/productFloorOverlay/' + id + "?layout=wide_blend&limit=10&flId="+_flId,
		area: ['700px', '560px'],
		cancel: function(index, layero){
			parent.location.reload();
		}
	});
}

function onclickRank(id) {
	layer.open({
		title: '编辑排行',
		type: 2,
		content: contextPath+'/admin/floorItem/rankFloorOverlay/' + id,
		area: ['650px', '540px'],
		cancel: function(index, layero){
			parent.location.reload();
		}
	});
}

function onclickBrand(id) {
	layer.open({
		title: '编辑品牌',
		type: 2,
		content: contextPath+'/admin/floorItem/brandFloorOverlay/' + id,
		area: ['650px', '400px'],
		cancel: function(index, layero){
			parent.location.reload();
		}
	});
}


function onclickGroup(id) {
	layer.open({
		title: '编辑团购',
		type: 2,
		content: contextPath+'/admin/floorItem/groupFloorOverlay/' + id,
		area: ['650px', '540px'],
		cancel: function(index, layero){
			parent.location.reload();
		}
	});
}

//楼层编辑中的 右下方广告列表
function onclickAdv(id, obj) {
	layer.open({
		title: '编辑广告',
		type: 2,
		content: contextPath+'/admin/floorItem/advListOverlay/' + id + "?limit=2&layout=wide_blend",
		area: ['650px', '400px'],
		cancel: function(index, layero){
			parent.location.reload();
		}
	});
}

//楼层编辑中的 左上方广告位
function onclickLeftAdv(id) {
	layer.open({
		title: '编辑广告',
		type: 2,
		content: contextPath+'/admin/floorItem/leftAdvOverlay/' + id + "?layout=wide_blend",
		area: ['650px', '400px'],
		cancel: function(index, layero){
			parent.location.reload();
		}
	});
}

//楼层编辑中的 底部广告位
function onclickBottomAdv(id) {
	layer.open({
		title: '编辑广告',
		type: 2,
		content: contextPath+'/admin/floorItem/bottomAdvOverlay/' + id,
		area: ['650px', '400px'],
		cancel: function(index, layero){
			parent.location.reload();
		}
	});
}

function onclickNews(id) {
	layer.open({
		title: '编辑文章',
		type: 2,
		content: contextPath+'/admin/floorItem/newsFloorOverlay/' + id,
		area: ['650px', '540px'],
		cancel: function(index, layero){
			parent.location.reload();
		}
	});
}

function sendData(url) {
	$.ajax({
		"url": url,
		type: 'post',
		async: true, //默认为true 异步   
		success: function (result) {
			$("div[class='tabs-panel middle-goods-list']").html(result);
		}
	});
}

function contentType(value) {
	var s;
	var url;
	if (value == "BD") {
		url = contextPath+"/admin/floor/brandContent/"+_flId+"?layout=wide_blend&limit=5";
		sendContent(url);
	}
}
function lowerContentType(value) {
	if (value == "NS") {
		var url = contextPath+"/admin/floor/newsContent/" +_flId;
		sendLowerContent(url);
	} else if (value == "RA") {
		var html = "<a href='javascript:onclickAdv("+_flId+")'>编辑</a> </b> <div><img src='"+contextPath+"/images/radv1.jpg' style='width:100%'></div> <div><img src='$"+contextPath+"/images/radv2.jpg' style='width:100%;margin-top:10px;'></div>";
		$("#UpperRight").after(html);
	}
}

function sendLowerContent(url) {
	$.ajax({
		"url": url,
		type: 'post',
		async: true, //默认为true 异步   
		success: function (result) {
			$("#UpperRight").after(result);
		}
	});
}


function sendContent(url) {
	$.ajax({
		"url": url,
		type: 'post',
		async: true, //默认为true 异步   
		success: function (result) {
			$("div[class='recommend-brand']").html(result);
		}
	});
}

//方法，判断是否为空
function isBlank(_value) {
	if (_value == null || _value == "" || _value == undefined) {
		return true;
	}
	return false;
}
