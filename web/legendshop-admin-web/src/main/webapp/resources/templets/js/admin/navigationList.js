function deleteById(id) {
	if(confirm(" 确定删除 ?")){
		window.location = contextPath+"/admin/system/navigation/delete/" + id";
	}
}

function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("form1").submit();
}

function deleteBychildId(id) {
	if(confirm(" 确定删除 ?")){
		window.location = contextPath+"/admin/system/navigationItem/delete/" + id";
	}
}

jQuery(document).ready(function(){
	$("button[name='parentStatusImg']").click(function(event){
		$this = $(this);
		initStatus($this.attr("itemId"), $this.attr("itemName"),  $this.attr("status"), contextPath+"/admin/system/navigation/updatestatus/", $this,contextPath);
	}
	);

	$("button[name='childStatusImg']").click(function(event){
		$this = $(this);
		initStatus($this.attr("itemId"), $this.attr("itemName"),  $this.attr("status"), contextPath+"/admin/system/navigationItem/updatestatus/", $this,contextPath);
	}
	);
	highlightTableRows("parent");  
});