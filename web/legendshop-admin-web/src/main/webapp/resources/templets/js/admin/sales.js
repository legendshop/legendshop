var nowYear = new Date().getFullYear();
var nowMonth = new Date().getMonth() + 1;
var commonYear = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];//平年月份的总天数
var LeapYear = [ 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];//闰年月份的总天数 

$(document).ready(function() {

	var queryTerms = $("#queryTerms").val();
	change(queryTerms);
	var shopName = $("#shopName").val();
	if(shopName == ''){
        makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'shopName');
    }else{
    	makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",shopName, 'shopName');
		$("#select2-chosen-1").html(shopName);
    }
});

function makeSelect2(url,element,text,label,value,holder, textValue){
	$("#"+element).select2({
        placeholder: holder,
        allowClear: false,
        width:'200px',
        ajax: {  
    	  url : url,
    	  data: function (term,page) {
                return {
                	 q: term,
                	 pageSize: 10,    //一次性加载的数据条数
                 	 currPage: page, //要查询的页码
                };
            },
			type : "GET",
			dataType : "JSON",
            results: function (data, page, query) {
            	if(page * 5 < data.total){//判断是否还有数据加载
            		data.more = true;
            	}
                return data;
            },
            cache: true,
            quietMillis: 200//延时
      }, 
        formatInputTooShort: "请输入" + text,
        formatNoMatches: "没有匹配的" + text,
        formatSearching: "查询中...",
        formatResult: function(row,container) {//选中后select2显示的 内容
            return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
        },formatSelection: function(row) { //选择的时候，需要保存选中的id
        	$("#" + textValue).val(row.text);
            return row.text;//选择时需要显示的列表内容
        }, 
    });
}

// 路径配置
require.config({
	paths : {
		echarts : contextPath + '/resources/plugins/ECharts/dist'
	}
});

// 使用
require([ 'echarts', 'echarts/chart/bar', 'echarts/chart/line' ], function(
		ec) {

	// 基于准备好的dom，初始化echarts图表
	var myChart = ec.init(document.getElementById('main'));

	var option = {
			tooltip : {
				trigger : 'axis'
			},
			legend : {
				data : [ '成交数量', '成交金额' ]
			},
			toolbox : {
				show : true,
				feature : {
					mark : {
						show : true
					},
					dataView : {
						show : true,
						readOnly : true
					},
					magicType : {
						show : true,
						type : [ 'line', 'bar' ]
					},
					restore : {
						show : true
					},
					saveAsImage : {
						show : true
					}
				}
			},
			calculable : true,
			dataZoom : {
				show : true,
				realtime : true,
				start : 0,
				end : 100
			},
			xAxis : [ {
				type : 'category',
				boundaryGap : true,
				data : getXAxisData()
			} ],
			yAxis : [ {
				type : 'value',
				name : '成交数量'
			}, {
				type : 'value',
				name : '成交金额'
			} ],
			series : [ {
				name : '成交数量',
				type : 'line',
				itemStyle : {
					normal : {
						color : '#b6a2de'
					}
				},
				data : getBarData()
			}, {
				name : '成交金额',
				type : 'bar',
				yAxisIndex : 1,
				itemStyle : {
					normal : {
						color : '#87cefa'
					}
				},
				data : getLineData()
			} ]
	};

	// 为echarts对象加载数据 
	myChart.setOption(option);

});

//方法，判断是否为空
function isBlank(_value) {
	if (_value == null || _value == "" || _value == undefined) {
		return true;
	}
	return false;
}

$("#submitOrder").click(function() {
	$("#form1 #curPageNO").val("1");
	$("#form1").submit();
});

function change(valueId) {

	if (valueId == 2) {//按照年份进行搜索
		var str = "<select name='selectedYear' id='selectedYear' class='criteria-select'>";
		if (!isBlank(selectedYear)) {
			for (var i = nowYear; i >= nowYear - 5; i--) {
				if (selectedYear == i) {
					str += "<option value='"+i+"' selected>" + i
					+ "</option>";
				} else {
					str += "<option value='"+i+"'>" + i + "</option>";
				}
			}
		} else {
			for (var i = nowYear; i >= nowYear - 5; i--) {
				str += "<option value='"+i+"'>" + i + "</option>";
			}
		}

		str += "</select>";
		$("#range").html(str);
	} else if (valueId == 1) {//按照月份进行搜索
		var str = "<select name='selectedYear' id='selectedYear' onchange='changeMonth()' class='criteria-select'>";
		var str1 = "<select name='selectedMonth' id='selectedMonth' style='margin-left: 5px;' class='criteria-select'>";

		if (!isBlank(selectedYear)) {
			for (var x = nowYear; x >= nowYear - 5; x--) {
				if (x == selectedYear) {
					str += "<option value='"+x+"' selected>" + x
					+ "年</option>";
				} else {
					str += "<option value='"+x+"'>" + x + "年</option>";
				}
			}
		} else {
			for (var x = nowYear; x >= nowYear - 5; x--) {
				str += "<option value='"+x+"'>" + x + "年</option>";
			}
		}
		str += "</select>";
		$("#range").html(str);

		if (!isBlank(selectedMonth)) {
			for (var y = 12; y >= 1; y--) {
				if (y == selectedMonth) {
					str1 += "<option value='"+y+"' selected>" + y
					+ "月</option>";
				} else {
					str1 += "<option value='"+y+"'>" + y + "月</option>";
				}
			}
		} else {
			for (var y = nowMonth; y >= 1; y--) {
				str1 += "<option value='"+y+"'>" + y + "月</option>";
			}
		}

		str1 += "</select>";
		$("#range").append(str1);
	} else if (valueId == 0) {//按照天数进行搜索
		var str = '<input readonly="readonly"  name="selectedDate" id="selectedDate" class="Wdate" type="text" onClick="WdatePicker()" value="'
			+ selectedDay + '" />';
		$("#range").html(str);
	} else if (valueId == 3) {//周
		var str = "<select name='selectedYear' id='selectedYear' onchange='changeMonth()' class='criteria-select sele'>";
		var str1 = "<select name='selectedMonth' id='selectedMonth' onchange='getWeek()' style='margin-left: 5px;' class='criteria-select sele'>";
		var str2 = "<select name='selectedWeek' id='selectedWeek'  style='margin-left: 5px;' class='criteria-select sele'><option value='-1'>请选择</option></select>";

		if (!isBlank(selectedYear)) {
			for (var x = nowYear; x >= nowYear - 5; x--) {
				if (x == selectedYear) {
					str += "<option value='"+x+"' selected>" + x
					+ "年</option>";
				} else {
					str += "<option value='"+x+"'>" + x + "年</option>";
				}
			}
		} else {
			for (var x = nowYear; x >= nowYear - 5; x--) {
				str += "<option value='"+x+"'>" + x + "年</option>";
			}
		}
		str += "</select>";
		$("#range").html(str);

		if (!isBlank(selectedMonth)) {
			for (var y = 12; y >= 1; y--) {
				if (y == selectedMonth) {
					str1 += "<option value='"+y+"' selected>" + y
					+ "月</option>";
				} else {
					str1 += "<option value='"+y+"'>" + y + "月</option>";
				}
			}
		} else {
			for (var y = nowMonth; y >= 1; y--) {
				str1 += "<option value='"+y+"'>" + y + "月</option>";
			}
		}

		str1 += "</select>";
		$("#range").append(str1);

		$("#range").append(str2);

		getWeek();
	}
}

/** 加载 月份 选项 **/
function changeMonth() {
	var nowYear = new Date().getFullYear();
	var selectedYear = $("#selectedYear").val();
	var nowMonth = new Date().getMonth() + 1;
	var str = "";

	if (selectedYear != nowYear) {
		for (var i = 1; i < 13; i++) {
			str += "<option value='"+i+"'>" + i + "月</option>"
		}
	} else {
		for (var n = 1; n <= nowMonth; n++) {
			str += "<option value='"+n+"'>" + n + "月</option>"
		}
	}

	$("#selectedMonth").html(str);
}

function IsPinYear(year) {//判断是否闰平年                      
	return (0 == year % 4 && (year % 100 != 0 || year % 400 == 0));
}

function getLastDay(year, month) {
	if (IsPinYear(year)) { //true : 润年，false :平年
		return LeapYear[month];
	} else {
		return commonYear[month];
	}
}

/**获取时间周期**/
function getWeek() {
	var year = $("#selectedYear").val();
	var month = $("#selectedMonth").val();
	$("#selectedWeek").html("");
	jQuery.ajax({
		type : 'POST',
		url : contextPath + '/admin/report/getWeek',
		data : {
			"year" : year,
			"month" : month
		},
		dataType : 'json',
		success : function(data) {
			var weekList = jQuery.parseJSON(data);
			// console.debug(weekList);
			var option;
			for (var i = 0; i < weekList.length; i++) {
				if (!isBlank(selectedWeek) && selectedWeek == weekList[i]) {
					option = '<option selected value="'+weekList[i]+'">'
					+ weekList[i] + '</option>';
				} else {
					option = '<option value="'+weekList[i]+'">'
					+ weekList[i] + '</option>';
				}

				$("#selectedWeek").append(option);
			}
		}
	});
}

//获取 销量 数据
function getBarData() {
	var list = [];
	if (!isBlank($("#salesResponseJSON").val())) {
		var reportList = jQuery.parseJSON($("#salesResponseJSON").val());
		var queryTerms = $("#queryTerms").val();
		if (queryTerms == 0) {
			for (var x = 0; x <= 24; x++) {//按天计算
				var flag = true;
				for (var q = 0; q < reportList.length; q++) {
					if (x == reportList[q].subDate) {
						list.push(reportList[q].numbers);
						flag = false;
						break;
					}
				}
				if (flag) {
					list.push(0);
				}
			}
		} else if (queryTerms == 1) {//按月计算
			var lastDay = getLastDay(selectedYear,
					(Number(selectedMonth) - 1));
			for (var y = 1; y <= lastDay; y++) {
				var flag = true;
				for (var q = 0; q < reportList.length; q++) {
					if (y == reportList[q].subDate) {
						list.push(reportList[q].numbers);
						flag = false;
						break;
					}
				}
				if (flag) {
					list.push(0);
				}
			}
		} else if (queryTerms == 2) {//按年计算
			for (var z = 1; z < 13; z++) {
				var flag = true;
				for (var q = 0; q < reportList.length; q++) {
					if (z == reportList[q].subDate) {
						list.push(reportList[q].numbers);
						flag = false;
						break;
					}
				}
				if (flag) {
					list.push(0);
				}
			}
		} else if (queryTerms == 3) {//按年计算
			for (var z = 1; z <= 7; z++) {
				var flag = true;
				for (var q = 0; q < reportList.length; q++) {
					if (z == reportList[q].subDate) {
						list.push(reportList[q].numbers);
						flag = false;
						break;
					}
				}
				if (flag) {
					list.push(0);
				}
			}

		}

	}
	return list;
}

//获取 金额 数据
function getLineData() {
	var list = [];
	if (!isBlank($("#salesResponseJSON").val())) {
		var reportList = jQuery.parseJSON($("#salesResponseJSON").val());
		var queryTerms = $("#queryTerms").val();
		if (queryTerms == 0) {
			for (var x = 0; x <= 24; x++) {//按天计算
				var flag = true;
				for (var q = 0; q < reportList.length; q++) {
					if (x == reportList[q].subDate) {
						list.push(reportList[q].totalCash);
						flag = false;
						break;
					}
				}
				if (flag) {
					list.push(0);
				}
			}
		} else if (queryTerms == 1) {//按月计算
			var lastDay = getLastDay(selectedYear,(Number(selectedMonth) - 1));
			for (var y = 1; y <= lastDay; y++) {
				var flag = true;
				for (var q = 0; q < reportList.length; q++) {
					if (y == reportList[q].subDate) {
						list.push(reportList[q].totalCash);
						flag = false;
						break;
					}
				}
				if (flag) {
					list.push(0);
				}
			}
		} else {//按周计算
			for (var z = 1; z <= 13; z++) {
				var flag = true;
				for (var q = 0; q < reportList.length; q++) {
					if (z == reportList[q].subDate) {
						list.push(reportList[q].totalCash);
						flag = false;
						break;
					}
				}
				if (flag) {
					list.push(0);
				}
			}
		}
	}
	return list;
}

//获取 x轴 数据
function getXAxisData() {
	var list = [];
	var str = "";

	var queryTerms = $("#queryTerms").val();
	if (queryTerms == 0) {
		for (var x = 0; x < 24; x++) {
			str = x + "小时";
			list.push(str);
		}
	} else if (queryTerms == 1) {
		var lastDay = getLastDay(selectedYear, (Number(selectedMonth) - 1));
		for (var y = 1; y <= lastDay; y++) {
			str = y + "日";
			list.push(str);
		}
	} else if (queryTerms == 2) {
		for (var z = 1; z < 13; z++) {
			str = z + "月份";
			list.push(str);
		}
	} else if (queryTerms == 3) {
		for (var z = 1; z <= 7; z++) {
			str = "星期" + z;
			list.push(str);
		}
	}

	return list;
}