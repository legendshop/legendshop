$(document).ready(function() {
	for ( var i = 0; i < userGradeArray.length; i++) {
		renderUserGrade(userGradeDiv, userGradeArray[i]);
	}		

	//验证
	jQuery("#form1").validate({

		errorPlacement: function(error, element) {
			element.parent().find("em").html("");
			error.appendTo(element.parent().find("em"));
		},

		rules : {
			title : "required",
			userGradeArray : "required",
			text: {
				required:true,
			},
		},
		messages : {
			title : "请输入标题",
			userGradeArray : "请选择用户等级",
			text: {
				required:"请输入内容",
			},
		}
	});
	//斑马条纹
	$("#col1 tr:nth-child(even)").addClass("even");
	var editor;
	KindEditor.ready(function(K) {
		editor = K.create('textarea[name="text"]', {
			resizeType : 1,
			width : 1280,
			allowPreviewEmoticons : false,
			allowImageUpload : false,
			allowFileManager : true,
			afterBlur:function(){this.sync();},
			items : [
			         'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
			         'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
			         'insertunorderedlist', '|', 'emoticons', 'link']
		});
	});
});

function renderUserGrade(target, entity) {
	var element;
	if (entity.status == 'Y') {
		element = " <label  class='text-overflow' style='margin-right: 10px;text-align: left;width:80px;'>"+
				  "<label class='checkbox-wrapper checkbox-wrapper-checked'>"+
					  "		<span class='checkbox-item'>"+
					  "			<input type='checkbox' checked='checked' name='userGradeArray' value='" + entity.key + "' id='" + entity.key + "' class='checkbox-input selectOne' onclick='selectOne(this);'/>"+
					  "			<span class='checkbox-inner' style='margin-left:10px;'></span>"+
					  "		</span>"+
					  " </label>"+ entity.value+
				  "</label>";
	} else {
		element = " <label  class='text-overflow' style='margin-right: 10px;text-align: left;width:80px;'>"+
				  "<label class='checkbox-wrapper'>"+
				  "		<span class='checkbox-item'>"+
				  "			<input type='checkbox' name='userGradeArray' value='" + entity.key + "' id='" + entity.key + "' class='checkbox-input selectOne' onclick='selectOne(this);'/>"+
				  "			<span class='checkbox-inner' style='margin-left:10px;'></span>"+
				  "		</span>"+
				  " </label>"+ entity.value+
				  "</label>";
	}
	target.append(element);
}
function selAll(){
         $(".selectOne").each(function(){
        	 $(this).attr("checked",true);
	 		 $(this).parent().parent().addClass("checkbox-wrapper-checked");
         });
};


