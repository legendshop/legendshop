function getCheckedFloor() {
		var floorList = $("input[type=checkbox][name=floorId]:checked");
		var floorIdArr = new Array();
		floorList.each(function(index, domEle) {
			var value = $(domEle).val();
			floorIdArr.push(value);
		});
		return floorIdArr.toString();
	}

	function batchOffline() {
		var floorIdStr = getCheckedFloor();
		jQuery.ajax({
					url : contextPath+"/admin/floor/batchOffline/"+floorIdStr,
					type : 'get',
					async : false, //默认为true 异步  
					dataType : 'json',
					error : function(data) {
					},
					success : function(data) {
						if (data == "OK") {
							layer.msg("楼层批量下线成功。", {icon:1, time:700}, function(){
								setInterval(function() {
									location.href = contextPath+"/admin/floor/batchOffline/query";
								}, 2000)
							});
						} else {
							layer.alert("楼层批量下线出错。", {icon:2});
						}
					}
				});
	}

	$("#checkAllbox").on("click", function() {
		if ($(this).is(":checked")) {
			$("input[type=checkbox][name=floorId]").prop("checked", "checked");
		} else {
			$("input[type=checkbox][name=floorId]").removeAttr("checked");
		}
	});

	function deleteById(id) {
		layer.confirm('删除楼层会同步删除楼层所有的数据，是否继续?', {icon:3, title:'提示'}, function() {
			window.location = contextPath+"/admin/floor/delete/" + id;
		});
	}
	
	function deleteBychildId(id) {
		layer.confirm('确定删除？', {icon:3, title:'提示'}, function(){
			window.location = contextPath+"/admin/subFloor/delete/" + id;
		});
	}
	
	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}
	
	$(document).ready(function() {
		$("button[name='statusImg']").click(function(event) {
			$this = $(this);
			initStatus($this.attr("itemId"),$this.attr("itemName"),$this.attr("status"),contextPath+"/admin/floor/updatestatus/",$this, contextPath);
		});
		$("button[name='childStatusImg']").click(function(event) {
			$this = $(this);
			initStatus($this.attr("itemId"),$this.attr("itemName"),$this.attr("status"),contextPath+"/admin/subFloor/updatestatus/",$this, contextPath);
		});
		highlightTableRows("item");
	});
	
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#form1")[0].submit();
	}