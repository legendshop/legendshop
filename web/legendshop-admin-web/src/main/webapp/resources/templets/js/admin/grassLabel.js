$(document).ready(function() {
	
	var isSubmitted = false; 
	jQuery("#form1").validate({
		rules: {
			name: {
				required: true,
				checkBrandName: true //期望的是true,如果为false则展示提示信息
			},
			commend: {
				required: true
			}
		},
		messages: {
			name: {
				required: "请输入标签名"
			},
			isrecommend: {
				required: "请选择是否推荐"
			}
		},

		submitHandler : function(form){
			//防止表单重复提交
			if(!isSubmitted){
				form.submit();
				isSubmitted = true;
			}else{
				layer.alert('请不要重复提交表单!', {icon: 0});
			}
		}
	});


});