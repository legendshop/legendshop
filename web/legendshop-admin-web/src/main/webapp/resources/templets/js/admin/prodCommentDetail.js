$("#audit-btn").on("click", function(){
	var $this = $(this);
	var commentId = $this.attr("commentId");
	var auditType = $this.attr("auditType");

	layer.open({
		title :"评论审核",
		type: 1, 
		content: $("#auditWin").html(),
		btn: ['确定','取消'],
		area: ['350px', '150px'],
		yes:function(index, layero){
			var auditVal = $("#auditForm input[name='audit']:checked").val();
			if(isBlank(auditVal)) {
				layer.msg("请选择“通过”还是“不通过”",{icon:0});
				return;
			}
			
			var url = contextPath + "/admin/productcomment/audit";
			if(auditType === "append"){
				url = contextPath + "/admin/productcomment/auditAdd";
			}

			var params = {
					"id": commentId,
					"audit": auditVal
			};
			layer.load(1);
			$.ajaxUtils.sendPost(url, params, function(result){
				if(result === "OK"){
					layer.msg("恭喜您, 审核成功!",{icon:1,time:500}, function(){
						parent.window.location.reload(true);
					});
				}else if(result === "fail"){
					layer.msg("对不起, 审核异常, 请联系客服!");
				}else{
					layer.msg(result);
				}
			});
		},
		cancel: function(){
			layer.close();
		}

	});
});

//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}