var endDate = '<fmt:formatDate value="${shopOrderBill.createDate}" pattern="yyyy-MM-dd"/>';

//发总资深代码 必须保留
/*    endDate = new Date(Date.parse(endDate.replace(/-/g, "/"))); 
   endDate.setDate(endDate.getDate() + 1);
   endDate = endDate.getFullYear() + "-" + (endDate.getMonth() + 1) + "-" + endDate.getDate(); */

jQuery(document).ready(function(){

  //订单金额tip
  $('#orderAmount').hover(function(){

    var that = this;
    //tips
    tips = layer.tips("<span style='color:#666;'>结算周期内产生确认收货，订单状态为已完成的订单，包含运费</span>",that, {
      tips: [1, '#e8f7fd'],
      time: 10000
    });
  },function(){
    layer.close(tips);
  });
  //退单金额tip
  $('#refundAmount').hover(function(){

    var that = this;
    //tips
    tips = layer.tips("<span style='color:#666;'>结算周期内平台确认退款，退款单状态为已完成的退款金额</span>",that, {
      tips: [1, '#e8f7fd'],
      time: 10000
    });
  },function(){
    layer.close(tips);
  });
  //平台佣金tip
  $('#commisTotals').hover(function(){

    var that = this;
    //tips
    tips = layer.tips("<span style='color:#666;'>平台佣金 = (订单金额 - 退单金额) * 平台结算佣金比例% </span>",that, {
      tips: [1, '#e8f7fd'],
      time: 10000
    });
  },function(){
    layer.close(tips);
  });
  //分销佣金tip
  $('#distCommisTotals').hover(function(){

    var that = this;
    //tips
    tips = layer.tips("<span style='color:#666;'>周期内，已完成且没有退货或者退货不成功的分销订单</span>",that, {
      tips: [1, '#e8f7fd'],
      time: 10000
    });
  },function(){
    layer.close(tips);
  });
  //红包金额tip
  $('#redpackTotals').hover(function(){

    var that = this;
    //tips
    tips = layer.tips("<span style='color:#666;'>订单使用的红包总额</span>",that, {
      tips: [1, '#e8f7fd'],
      time: 10000
    });
  },function(){
    layer.close(tips);
  });
  //退款红包金额tip
  $('#returnRedpackTotals').hover(function(){

    var that = this;
    //tips
    tips = layer.tips("<span style='color:#666;'>退款订单退的红包金额，订单产生全退款就不退还（部分商品的退货按比例补回）退款红包金额 = 订单项使用的红包金额 * 原价 / 订单总金额</span>",that, {
      tips: [1, '#e8f7fd'],
      time: 10000
    });
  },function(){
    layer.close(tips);
  });
  //预售定金tip
  $('#preDepositPriceTotal').hover(function(){

    var that = this;
    //tips
    tips = layer.tips("<span style='color:#666;'>平台收取的预售定金（已支付定金未支付尾款且尾款支付时间已结束的预售订单）</span>",that, {
      tips: [1, '#e8f7fd'],
      time: 10000
    });
  },function(){
    layer.close(tips);
  });
  //拍卖保证金tip
  $('#auctionDepositTotal').hover(function(){

    var that = this;
    //tips
    tips = layer.tips("<span style='color:#666;'>拍卖活动结束后，在用户中标并且24小时内未转订单或中标转成订单后但是72小时内未支付的情况下，按规则扣除保证金不予退还，平台按周期与商家结算收取的拍卖保证金</span>",that, {
      tips: [1, '#e8f7fd'],
      time: 10000
    });
  },function(){
    layer.close(tips);
  });

});

function pager(curPageNO){;
$("#curPageNO").val(curPageNO);
$("#form1").submit();
}

function adminConfirm(id){
	layer.confirm("审核后将无法撤销，进入下一步付款环节,确认审核吗?", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		jQuery.ajax({
			url:contextPath+"/admin/shopBill/confirm/"+id,
			type:'get', 
			async : false, //默认为true 异步  
			dataType : 'json',  
			success:function(data){
				if(data=="OK"){
					window.location = contextPath+"/admin/shopBill/load/"+id;
				}else{
					layer.alert("审核失败",{icon:2});
				}
			}   
		}); 
	});
}

function finishShopBill(id,sn){
	content="<table class='"+tableclass+"'>"+
	"<tr><td>结算单号</td><td>"+sn+"</td></tr>"+
	"<tr><td>付款日期</td><td><input style='width:200px;' readonly='readonly' id='payDate' class='Wdate' type='text' value=''></td></tr>"+
	"<tr><td>付款备注</td><td><textarea maxLength='200' placeHolder='请输入汇款单号、支付方式等付款凭证' id='payContent' rows='3' cols='50'></textarea> </td></tr></table>",
	layer.open({
		title :"付款完成",
		type: 1, 
		content: content, 
		area: ['900px', '320px'],
		btn: ['确定','关闭'],
		yes:function(){
			var payDate = $("#payDate").val();
			var payContent = $("#payContent").val().trim();
			if(payDate==""){
				layer.msg("请输入付款日期",{icon:0});
				return false;
			}else if(payContent==""){
				layer.msg("请输入付款备注",{icon:0});
				return false;
			}else{
				jQuery.ajax({
					url:contextPath+"/admin/shopBill/finishPay/"+id,
					type:'post',
					data:{"payDate":payDate,"payContent":payContent},
					async : false, //默认为true 异步  
					dataType : 'json',  
					success:function(data){
						if(data=="OK"){
							window.location = contextPath+"/admin/shopBill/load/"+id;
						}else{
							layer.msg("提交失败",{icon:2});
						}
					}   
				});  
			}
		}
	}); 
	
	
	laydate.render({
		   elem: '#payDate',
		   type:'datetime',
		   calendar: true,
		   theme: 'grid',
		   trigger: 'click'
		  });
}

function isblank(prop){
	if(prop==null||prop==""||prop==undefined){
		return true;
	}
	return false;
}

function exportOrder(){
	   if(isblank(type)){
		   type = 1;
	   }
	   var OrderBillType = type;
	   var array=[];
		array.push(OrderBillType);
		array.push(shopOrderBillId);
		array.push(subNumber);
		var jsondata=JSON.stringify(array);
		$("#exportParam").val(jsondata);
		$("#exportForm").submit();
}
