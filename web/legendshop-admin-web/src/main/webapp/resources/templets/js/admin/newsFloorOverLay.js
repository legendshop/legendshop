if(typeof JSON == 'undefined'){$('head').append($("<script type='text/javascript' src='"+contextPath+"/resources/common/js/json.js'>"));}
function newsFloorLoad(curPageNO){
	var name = $("#name").val();
	var newsCategoryId =  $("#newsCategoryId").val();
	$.ajax({
		url:contextPath+"/admin/floorItem/newsFloorLoad",
		data:{"curPageNO":curPageNO,"newsTitle":name, "newsCategoryId":newsCategoryId},
		type:'post', 
		async : true,  
		error: function(jqXHR, textStatus, errorThrown) {
		},
		success:function(result){
			jQuery("#floor_goods_list").empty().append(result);	
		}
	});
}

function selectNewsPager(curPageNO){
	newsFloorLoad(curPageNO);
}

function setNewsList(newsId,newsTitle){
	var count=jQuery(".floor_box_pls ul").length;
	var add=0; 
	if(count>6){
		layer.alert("最多只能添加7个文章！", {icon:2});
		add=1;
	}else{
		jQuery(".floor_box_pls .floor_pro").each(function(){
			if(jQuery(this).attr("id")== newsId){
				layer.alert("已经存在该文章", {icon:2});
				add=2;
			}
		});
		if(add==0){
			var s="<ul ondblclick='jQuery(this).remove();' class='floor_pro' id='" +newsId + "'><li href='"+contextPath+"/news/" + newsId +"'>" + newsTitle + "</li></ul>";
			jQuery(".floor_box_pls").append(s);
		}
	}

}

function saveForm(){
	var floorMap = [];
	jQuery(".floor_box_pls .floor_pro").each(function(){
		var obj = new Object();
		obj.id = jQuery(this).attr("id");
		floorMap.push(obj);
	});
	var productFloors = JSON.stringify(floorMap);
	$.ajax({
		url:contextPath+"/admin/floorItem/saveNewsFloor",
		data: {"productFloors":productFloors,"floorId":${flId}},
		type:'post',
		dataType : 'json',
		async : true, //默认为true 异步  
		error: function(jqXHR, textStatus, errorThrown) {
			layer.alert(textStatus, errorThrown);
		},
		success:function(result){
			layer.msg('保存成功', {icon:1, time:1000}, function(){
				parent.location.reload();
			});
		}
	}); 
}

jQuery(document).ready(function(){
	jQuery("#floor_goods_info").sortable();
	jQuery("#floor_goods_info").disableSelection();
});	