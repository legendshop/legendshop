$(document).ready(
		function() {
			//返回列表按钮单击事件
			$("input[type='button'][name='returnBtn']").on("click",
					function(event) {
				$.pageJumps.go(urls.list);
				return false;
			});

			$("#beanForm").validate(
					{
						//debug: true,
						rules : {
							name : {
								required : true,
								maxlength : 50,
								remote : {
									url : urls.checkNameIsExist,
									type : "GET",
									dataType : "JSON",
									data : {
										id : function() {
											return $("#id").val();
										},
										name : function() {
											return $("#name").val();
										}
									},
								}
							},
							url : {
								maxlength : 250
							},
							imgFile : {
								required : function() {
									if ($("#imgFile").attr("oldFile").trim()) {
										return false;
									}
									return true;
								},
							},
							status : {
								required : true,
							},
							description : {
								maxlength : 250
							}
						},
						messages : {
							name : {
								required : "请输入广告名称!",
								maxlength : "广告名称字符长度不能超过50!",
								remote : "已存在同名的广告名称,请换一个名称试试!",
							},
							url : {
								maxlength : "url长度不能超过250!"
							},
							imgFile : {
								required : "请选择广告图片!",
							},
							status : {
								required : "请选择广告状态!",
							},
							description : {
								maxlength : "广告描述不能超过250个字符!",
							}
						},
						submitHandler : function(form) {
							$.formUtils.ajaxSubmitFormData(form, function(result) {
								if (result === "OK") {
									if (!isEdit) {
										layer.confirm("恭喜您,创建成功!继续创建?", {icon:1}, function() {
											window.location.reload(true);
										}, function() {
											$.pageJumps.go(urls.list);
										});
									} else {
										layer.alert("恭喜您,保存成功!", {icon:1}, function() {
											$.pageJumps.go(urls.list);
										});
									}
								} else {
									layer.msg(result, {icon:2});
								}
							});
						}
					});

			$("#imgFile").change(function(){
				checkImgType(this);
				checkImgSize(this,5120);
			});

		});