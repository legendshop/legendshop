//校验手机号码
jQuery.validator.addMethod("checkMobile",function(value,element){
	var Reg = /^[1][0-9]{10}$/;

    if(!value){
		return true;
	}		

	if(!Reg.test(value)){
		return false;
	}

	return true;
},"手机号码格式不正确");

//校验身份证号
jQuery.validator.addMethod("checkIdCardNum",function(value,element){
	var v = $("#idCardNum").val();

	if(!value){
		return true;
	}
	return IdCardValidate(v);
},"身份证格式不正确");

//校验输入地址
jQuery.validator.addMethod("checkAddr",function(value,element){
	var Reg = /^[A-Za-z0-9\u4e00-\u9fa5]+$/;

	if(!value){
		return true;
	}		

	if(!Reg.test(value)){
		return false;
	}

	return true;
},"住址只能由中文、字母或数字组成");

//表单校验规则
$(document).ready(function() {
	jQuery("#form1").validate({
		rules : {
			name : {
				required : true,
				minlength : 3,
				stringCheck : true,
				checkName1 : true
			},
			password : "required",
			mobile : {
				checkMobile : true
			},
			idCardNum : {
				checkIdCardNum : true
			},
			addr : {
				checkAddr : true
			},
			note : {
				maxlength:50
			}
		},
		messages : {
			name : {
				required : "请输入管理员名称",
				minlength : "管理员名字必须大于3个字符"
			},
			password : {
				required : "请输入密码"
			}
		}
	});
	// 构造菜单
	createTree();
});

// 检查管理员是否已经存在
function checkAdminName() {
	var result = true;
	var nameValue = jQuery("#name").val();
	if (nameValue != null && nameValue != '') {
		if (nameValue.length >= 3 && nameValue.isAlpha()) {
			//call ajax action
			$.ajax({
				url : contextPath + "/admin/adminUser/isAdminUserExist",
				data : {
					"userName" : nameValue
				},
				type : 'post',
				async : false, //默认为true 异步   
				error : function(jqXHR, textStatus, errorThrown) {
					//console.log(textStatus, errorThrown);
				},
				success : function(retData) {
					if ('true' == retData) {
						result = false;
					}
				}
			});
		}
	}
	return result;
}

/*** 检查是否由数字字母和下划线组成 ***/
String.prototype.isAlpha = function() {
	return (this.replace(/\w/g, "").length == 0);
}

function showMenu(id) {
	var nameObj = $("#deptName");
	var nameOffset = nameObj.offset();
	$("#menuContent").css({
		left : nameOffset.left + "px",
		top : nameOffset.top + nameObj.outerHeight() + "px"
	}).slideDown("fast");
	$("body").bind("mousedown", blurMenu);
}

function hideMenu() {
	$("#menuContent").fadeOut("fast");
	$("body").unbind("mousedown", blurMenu);
}

function blurMenu(event) {
	var stillFocus = event.target.id == "menuBtn"
		|| event.target.id == "menuContent"
			|| $(event.target).parents("#menuContent").length > 0;
			if (!stillFocus) {
				hideMenu();
			}
}

function updateDept(deptId, deptName) {
	$("#deptName").val(' ' + deptName);
	$("#deptId").val(' ' + deptId);
}

function createTree() {
	var setting = {};
	setting.view = {
			selectedMulti : false
	};
	setting.edit = {
			enable : false
	};
	setting.callback = {
			onClick : onClick
	};
	setting.data = {
			simpleData : {
				enable : true,
				pIdKey : "parentId"
			}
	};

	var request = {};
	request.url = contextPath+'/admin/department/queryDeptInfo';
	request.type = 'post';
	request.dataType = 'json';
	request.ContentType = 'application/json; charset=utf-8';
	request.error = function(msg) {
		layer.alert("树状菜单创建失败",{icon:2});
	};
	request.success = function(data) {
		$.fn.zTree.init($("#deptMenu"), setting, data);
		$.fn.zTree.getZTreeObj("deptMenu").expandAll(true);
		initDept();
	};

	$.ajax(request);
}

function initDept() {
	var tree = $.fn.zTree.getZTreeObj("deptMenu");
	$("input[id^='deptName']").each(function() {

		var dept = $(this).attr("name");
		if (dept == "")
			return;

		var node = tree.getNodeByParam("id", dept);
		if (node != null)
			$(this).attr("value", "  " + node.name);

	});
}

function onClick(event, treeId, treeNode) {
	var zTree = $.fn.zTree.getZTreeObj("deptMenu");
	var dept = zTree.getSelectedNodes()[0];

	layer.confirm("确定要更改部门信息吗？",{
		icon: 3
		,btn: ['确定','取消'] //按钮
	},function(index) {
		updateDept(dept.id, dept.name);
		layer.close(index);
	});

	hideMenu();
}

laydate.render({
	elem: '#activeTime',
	type:'datetime',
	calendar: true,
	theme: 'grid',
	trigger: 'click'
});

laydate.render({
	elem: '#hireDate',
	calendar: true,
	theme: 'grid',
	trigger: 'click'
});