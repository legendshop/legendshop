$(document).ready(function() {
	$("#prodImg").mouseout(function(){
		if($("#prodImg").val()){
			$("#ImgPr").attr("style","display:block");
		}
	});
	
		$("#prodImg").uploadPreview({
			Img : "ImgPr",
			Width : 120,
			Height : 120,
			ImgType : [ "gif", "jpeg", "jpg", "bmp", "png" ],
			Callback : function() {
			}
		});
		jQuery("#form1").validate({
			rules : {
				name : {
					required : true,
					minlength : 2,
					maxlength : 10
				},
				url : {
					required : true,
	                minlength: 1
				},
				seq : {
					required : true,
					digits:true
				}
			},
			messages : {
				name : {
					required : "请输入标题",
					minlength : "请认真输入标题",
					maxlength : "标题最长长度为10个字符"
				},
				url : {
					required : "请输入链接",
					minlength : "请认真输入链接"
				},
				seq : {
					required : "请输入顺序",
					 digits:"输入非负整数"
				}
			}
		});
		
	}); 
	
	function imageCheck() {
		var editType = "${headmenu}";
		if (editType == null || editType == "") {
			var prodImg = $("#prodImg").val();
			if (prodImg != undefined) {
				if (prodImg <= 0) {
					layer.msg("请选择图片", {icon:2});
					return false;
				}
			}
		}
		return true;
	}

	function change() {
		$("#choose").hide();
		$("#previewImage").hide();
		$("#upload").show();
	}
	function hides() {
		$("#upload").hide();
		$("#choose").show();
		$("#prodImg").attr("value", "");
	}
	function changeImage() {
		$("#upload").hide();
		$("#previewImage").show();
	}

	jQuery.fn.extend({
		uploadPreview : function(opts) {
			var _self = this, _this = $(this);
			opts = jQuery.extend({
				Img : "ImgPr",
				Width : 100,
				Height : 100,
				ImgType : [ "gif", "jpeg", "jpg", "bmp", "png" ],
				Callback : function() {
				}
			}, opts || {});
			_self.getObjectURL = function(file) {
				var url = null;
				if (window.createObjectURL != undefined) {
					url = window.createObjectURL(file)
				} else if (window.URL != undefined) {
					url = window.URL.createObjectURL(file)
				} else if (window.webkitURL != undefined) {
					url = window.webkitURL.createObjectURL(file)
				}
				return url
			};
			_this.change(function() {

				if (this.value) {
					if (!RegExp("\.(" + opts.ImgType.join("|") + ")$", "i").test(this.value.toLowerCase())) {
						layer.alert("选择文件错误,图片类型必须是" + opts.ImgType.join("，") + "中的一种", {icon:2});
						this.value = "";
						$("#" + opts.Img).attr('src', "");
						return false
					}else if(!checkImgSize(this,1024)){
						this.value = "";
						$("#" + opts.Img).attr('src', "");
						return false;
					}else {
						$("#" + opts.Img).attr('src', _self.getObjectURL(this.files[0]))
					}
					opts.Callback()
				}
			})
		}
	});