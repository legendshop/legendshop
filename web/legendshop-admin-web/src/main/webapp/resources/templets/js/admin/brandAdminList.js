$(document).ready(function() {
	if(window.parent.brandIds.length > 0) {
		$("input[name='id']").each(function() {
			var brandId = $(this).val();
			if(window.parent.brandIds.indexOf(brandId) != -1){
				$(this).prop("checked", true);
			}
		});
	}
});

$("input[name='id']").click(function(){
	if($(this).is(":checked")){
		window.parent.brandIds.push($(this).val());
	}else {
		for(var i=0; i<window.parent.brandIds.length; i++) {
			if($(this).val() == window.parent.brandIds[i]) {
				window.parent.brandIds.splice(i, 1);
				break;
			}
		}
	}
});

function pager(curPageNO){
  document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("form1").submit();
}

//保存选定的品牌
function save_brand(){
	var brandMap = [];

	if(window.parent.brandIds.length==0){
		layer.alert("请先选择品牌。", {icon:2});
		return;
	}else {
		for(var i=0; i<window.parent.brandIds.length; i++) {
			var obj = new Object();
			obj.brandId = window.parent.brandIds[i];
			brandMap.push(obj);
		}
	}
	/*jQuery("input[type=checkbox]").each(function(){
		var obj = new Object();
		if(jQuery(this).attr("checked")=="checked"){
			obj.brandId = jQuery(this).attr("value");
			brandMap.push(obj);
		}
	});*/

	var brandIds = JSON.stringify(brandMap);
	$.ajax({
		url:contextPath+"/admin/prodType/saveBrandList",
		data: {"brandIds":brandIds,"id":proTypeId},
		type:'post',
		dataType : 'json',
		async : true, //默认为true 异步
		error: function(jqXHR, textStatus, errorThrown) {
			layer.alert(textStatus, errorThrown);
		},
		success:function(result){
			parent.location.reload();
		}
	});
}
