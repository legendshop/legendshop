$("#checkboxAll").on("click",function(){
	if($(this).is(":checked")){
		$("input[type=checkbox][name=pdId][can-del=true]").prop("checked",true);
	}else{
		$("input[type=checkbox][name=pdId][can-del=true]").removeAttr("checked");
	}
});

$("#batch-del").on("click",function(){
	var pds=$(".selectOne:checked");
	var pdArr=new Array();
	pds.each(function(){
		var pdId=$(this).val();
		pdArr.push(pdId);
	});
	var pdsIdStr=pdArr.toString();
	if(pdsIdStr==""){
		layer.alert("请选择要删除的记录",{icon: 0});
	}

	$.ajax({
		url: contextPath+"/admin/preDeposit/batchDelete/"+pdsIdStr,
		type:"DELETE",
		async : false, //默认为true 异步   
		dataType:"json",
		success:function(data){
			if(data=="OK"){
				layer.alert("批量删除成功。",{icon: 1},function(){
					location.reload();
				});
			}else{
				layer.alert("批量删除失败。",{icon: 2});
			}
		},

	});
});


function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("form1").submit();
}

function showPdRechargeInfo(id){
	layer.open({
		title :"充值信息",
		type: 2, 
		content: [contextPath+"/admin/preDeposit/pdRechargeQueryById?id="+id,"no"], 
		area: ['482px', 'auto'],
		success: function(layero, index) {
			  layer.iframeAuto(index);
			} 
	}); 
}

function deleteById(id) {
	layer.confirm("确定删除 ?", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		window.location = contextPath+"/admin/preDeposit/pdRechargeDelete/"+id;
	});
}
highlightTableRows("item");