$(document).ajaxSend(function(e, xhr, options) {
	var _csrf_header = '${_csrf.headerName}';
	var _csrf = '${_csrf.token}';
	if (_csrf != null && _csrf_header != null) {
		if (xhr.setRequestHeader) {
			xhr.setRequestHeader(_csrf_header, _csrf);
		}
	}
});
function appendCsrfInput() {
	var _csrf_header = '${_csrf.parameterName}';
	var _csrf = '${_csrf.token}';
	var hiddenField = document.createElement("input");
	if (_csrf != null && _csrf_header != null) {
		hiddenField.setAttribute("type", "hidden");
		hiddenField.setAttribute("name", _csrf_header);
		hiddenField.setAttribute("value", _csrf);
	}
	return hiddenField;
}
function uploadFile() {

	if(!fileV()){
		return false;
	}

	$.ajaxFileUpload({
		url : contextPath + '/admin/system/config/sendPic',
		secureuri : false,
		fileElementId : 'file',
		dataType : 'json',
		error : function(data, status, e) {
			layer.alert("上传图片异常！",{icon: 2});
		},
		success : function(data, status) {
			layer.msg("图片上传成功！",{icon: 1});
		}
	});
}

function fileV(){
	var file = $("#file").val();

	var filePath = file.length;
	if(filePath == null || filePath == "" || filePath == undefined){
		layer.msg("请选择正确的图片！",{icon: 2});
		return false;
	}

	var extStart = file.lastIndexOf('.');
	var ext = file.substring(extStart, file.length).toUpperCase();
	if (ext !== '.PNG' && ext !== '.JPG' && ext !== '.JPEG' && ext !== '.GIF') {
		layer.msg("请选择正确的图片！",{icon: 2});
		return false;
	}
	return true;

}

function telV(){
	var tel = $("#tel").val();
	if(tel != null && tel != "" && tel != undefined && tel.length == 11){
		return true;
	}
	layer.msg("请输入正确的手机号码！",{icon: 2});
	return false;

}