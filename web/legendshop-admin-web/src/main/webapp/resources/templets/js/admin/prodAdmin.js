jQuery(document).ready(function() {
	//三级联动
	$("select.combox").initSelect();
	//斑马条纹
	$("#col1 tr:nth-child(even)").addClass("even");
	highlightTableRows("col1");  
	var useSmallPic = '${prod.useSmallPic}';
	if('Y' == useSmallPic){
		var useSmallPic = document.getElementById("useSmallPic");
		useSmallPic.checked = "checked";
		showSmallPicDiv(useSmallPic);
	}
	KindEditor.options.filterMode=false;
	KindEditor.create('textarea[name="content"]', {
		cssPath : contextPath+'/resources/plugins/kindeditor/plugins/code/prettify.css',
		uploadJson : contextPath+'/editor/uploadJson/upload;jsessionid='+UUID,
		fileManagerJson : contextPath+'/editor/uploadJson/fileManager',
		allowFileManager : true,
		afterBlur:function(){this.sync();},
		width : '100%',
		height:'400px',
		afterCreate : function() {
			var self = this;
			KindEditor.ctrl(document, 13, function() {
				self.sync();
				document.forms['example'].submit();
			});
			KindEditor.ctrl(self.edit.doc, 13, function() {
				self.sync();
				document.forms['example'].submit();
			});
		}
	});
	prettyPrint();
});

function checkform() {
	var content = document.getElementById("content");
	var name = document.getElementById("name");
	if(name.value==null||name.value=='')
	{
		alert("请输入商品!");
		name.focus();
		return false;
	}

	if(name.length <=4)
	{
		alert("商品名称不能少于4位!");
		name.focus();
		return false;
	}

	var price = document.getElementById("price");
	if((price.value != null || price.value!='') && isNaN(price.value)){
		alert("商品原价必须为数字!");
		price.focus();
		return false;
	}
	var cash = document.getElementById("cash");
	if((cash.value != null || cash.value!='') && isNaN(cash.value)){
		alert("商品价格必须为数字!");
		cash.focus();
		return false;
	}
	var carriage = document.getElementById("carriage");
	if((carriage.value != null || carriage.value!='') && isNaN(carriage.value)){
		alert("商品运费必须为数字!");
		carriage.focus();
		return false;
	}
	var stocks = document.getElementById("stocks");
	if((stocks.value != null || stocks.value!='') && isNaN(stocks.value)){
		alert("库存量必须为数字!");
		cash.focus();
		return false;
	}	
	var file = document.getElementById("file");
	if(file.value==null||file.value=='')
	{
		var pic = document.getElementById("pic");
		if(pic.value==null||pic.value==''){
			alert("请上传商品图片!");
			file.focus();
			return false;
		}
	}			
	return true ;
}

function submitNext(){
	var result = checkSensitiveData();	 
	if(result != null && result != '' ){ 		
		art.dialog.confirm("您的输入含有敏感字： '"+result+"' 请更正后再提交",function(){
		}); 	
	}else{
		var saveProdForm = document.getElementById("saveProdForm");  
		if(checkform()){
			saveProdForm.submit();
		}
	}
}


function submitFinish(){
	var result = checkSensitiveData();	
	if(result != null && result != ''){ 		
		art.dialog.confirm("您的输入含有敏感字： '"+result+"' 请更正后再提交",function(){
		}); 	
	}else{
		var saveProdForm = document.getElementById("saveProdForm");
		document.getElementById("nextAction").value = "success";
		if(checkform()){
			saveProdForm.submit();
		}
	}			
}

function showSmallPicDiv(obj){
	var smallPicDiv = document.getElementById("smallPicDiv");
	if(obj.checked){
		smallPicDiv.style.display="block";
	}else{
		smallPicDiv.style.display="none";
	}		
}

function checkSensitiveData(){
	var sortId  = '${sort1.sortId}';
	var nsortId  = '${nsort2.nsortId}';
	var subNsortId  = '${nsort3.nsortId}';
	var content =  $("#content").val();	
	if(sortId == null || sortId == ''){
		sortId = -1;
	}		
	if(nsortId == null || nsortId == ''){
		nsortId = -1;
	}
	if(subNsortId == null || subNsortId == ''){
		subNsortId = -1;
	}	
	var url = contextPath+"/admin/sensitiveWord/filter/" + sortId + "/" + nsortId + "/" + subNsortId;
	$.ajax({
		"url":url, 
		data: {"src": content},
		type:'post', 
		dataType : 'json', 
		async : false, //默认为true 异步   
		error: function(jqXHR, textStatus, errorThrown) {
		},
		success:function(retData){
			result = retData;
		}
	});
	return result;
}

laydate.render({
	elem: '#startDate',
	calendar: true,
	theme: 'grid',
	trigger: 'click'
});

laydate.render({
	elem: '#endDate',
	calendar: true,
	theme: 'grid',
	trigger: 'click'
});