$(function() {
	if(!isBlank(errorMessage)){
		layer.alert(errorMessage, {icon:2});
	}

	$("input[name='advPicFile']").change(function(){
		checkImgType(this);
		checkImgSize(this,5120);
	});
});

function brdChkChange(ths) {
	if ($(ths).is(':checked')) {
		$(ths).parent().parent().addClass("checkbox-wrapper-checked");
		$(ths).parent().parent().parent().css("color", "#0e90d2");
	} else {
		$(ths).parent().parent().removeClass("checkbox-wrapper-checked");
		$(ths).parent().parent().parent().css("color", "#333");
	}
}

//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}