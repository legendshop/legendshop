function deleteById(id) {
	if (confirm("  确定删除 ?")) {
		window.location = contextPath+"/admin/advertisement/delete/" + id;
	}
}

$(document).ready(function() {
	$("button[name='statusImg']").click(function(event) {
		$this = $(this);
		initStatus(
				$this.attr("itemId"),
				$this.attr("itemName"),
				$this.attr("status"),
				contextPath+"/admin/advertisement/updatestatus/",
				event.target,
				contextPath);
	});
	highlightTableRows("item");
});