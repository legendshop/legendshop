$("#checkedAll").on("click", function() {
	if ($(this).is(":checked")) {
		$("input[type=checkbox][name=sid]").prop("checked", true);
	} else {
		$("input[type=checkbox][name=sid]").removeAttr("checked")
	}
});

$("#batch-del").on("click", function() {
	var checkedShops = $("input[type=checkbox][name=sid]:checked");
	var shopIdArr = new Array();
	checkedShops.each(function() {
		var shopId = $(this).val();
		shopIdArr.push(shopId);
	});
	var shopIdStr = shopIdArr.toString();
	if (shopIdStr == "") {
		layer.msg("请选择要删除的记录。",{icon: 0});
		return;
	}

	$.ajax({
		url : contextPath + "/admin/shopDetail/changeShopType",
		type : 'post',
		async : true, //默认为true 异步
		dataType : 'json',
		error : function(data) {
		},
		success : function(data) {
			if (data == "OK") {
				layer.msg("修改成功",{icon: 1});
			} else {
				layer.msg(data,{icon: 2});
			}
		}
	});
});
function showProduct(obj) {
	var shopId = $(obj).attr("shopId");
	var siteName = $(obj).attr("siteName");
	var url = encodeURI(contextPath + '/admin/product/query?shopId='
			+ shopId + '&siteName=' + siteName);
	window.location = url;
}
function deleteById(id) {

	layer.confirm("确定删除 ?", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		window.location = contextPath + "/admin/shopDetail/delete/" + id;
	});
}

function changeType(shopId,shopType) {
	if (isBlank(shopId)) {
		layer.msg("异常错误",{icon: 2});
		return false;
	}
	var content = ' <div style="padding: 25px 25px 0 25px;line-height: 26px;text-align: center;">选择店铺类型：<select style="width: 105px;" id="shopType" name="shopType">'
		+ '<option value="">--请选择--</option>'
		+ '<option value="0" zanwei1>专营店</option>'
		+ '<option value="1" zanwei2>旗舰店</option>'
		+ '<option value="2" zanwei3>自营店</option></select></div>';

	if (shopType==null){
    content=content.replace("zanwei1","")
    content=content.replace("zanwei2","")
    content=content.replace("zanwei3","")
  }else if (shopType==0){
    content=content.replace("zanwei1","selected")
    content=content.replace("zanwei2","")
    content=content.replace("zanwei3","")
  }else if (shopType==1){
    content=content.replace("zanwei1","")
    content=content.replace("zanwei2","selected")
    content=content.replace("zanwei3","")
  }else if (shopType==2){
    content=content.replace("zanwei1","")
    content=content.replace("zanwei2","")
    content=content.replace("zanwei3","selected")
  }

	layer.open({
		type: 1,
		title: '修改店铺类型',
		content: content,
		btn: ['确定'],
		area: ['330px', '165px'],
		yes: function(index, layero){
			var type = $(layero).find("#shopType").val();
			if (isBlank(type)) {
				layer.msg("请选择店铺类型",{icon: 0});
				return false;
			}
			$.ajax({
				url : contextPath + "/admin/shopDetail/changeShopType",
				data : {
					"shopId" : shopId,
					"shopType" : type
				},
				type : 'post',
				async : true, //默认为true 异步
				dataType : 'json',
				success : function(data) {
          if (data == "OK") {
            layer.msg("修改成功",{icon: 1},function () {
              window.location.reload();
            });
            layer.close(index);
          } else {
            layer.msg(data,{icon: 2});
						layer.close(index);
					}
				}
			});
			return true;
			layer.close(index); //如果设定了yes回调，需进行手工关闭
		}
	});
}

function pager(curPageNO) {
	document.getElementById("curPageNO").value = curPageNO;
	document.getElementById("form1").submit();
}

function sendRequest(userId, shopId, status) {
	var data = {
			"userId" : userId,
			"shopId" : shopId,
			"status" : status
	};
	jQuery.ajax({
		url : contextPath + "/admin/shopDetail/audit",
		type : 'post',
		data : data,
		async : true, //默认为true 异步
		dataType : 'html',
		success : function(retData) {
			if (retData == null || retData == '') {
				// alert('更改状态成功') ;
				window.location.reload();
			} else {
				//  alert('更改状态失败') ;
				window.location.reload();
			}
		}
	});
}

function isBlank(_value) {
	if (_value == null || _value == "" || _value == undefined) {
		return true;
	}
	return false;
}

function search() {
	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	$("#form1")[0].submit();
}

//编辑分佣比例
function editShopCommission(obj,commissionRate){
	var shopId = $(obj).attr("shopId");
	var commision = $(obj).attr("commision");
	if (isBlank(shopId)) {
		layer.msg("异常错误");
		return false;
	}
	var start = '<div style="padding-top:24px ;line-height:28px;text-align:center;">分佣比例<span style="color:red">*</span>：<input id="editCommision" style="resize:none;width:250px;height:30px;border:1px solid #e4e4e4;" maxlength="2" '
	var end=' placeholder="0-50之间" /></div>'
  var content =start+end;
  if (commissionRate!=null){
      commissionRate="value="+commissionRate;
      content = start+commissionRate+end;
  }
	layer.open({
		type: 1,
		title: '设置分佣比例',
		content: content,
		btn: ['确定'],
		area: ['400px', '160px'],
		yes: function(index, layero){
			var commision = $("#editCommision").val();
			if (isBlank(commision)) {
				layer.msg("请填写分佣比例");
				return false;
			}
			/*var reg = /^[1-9]\d*|0$/;*/
			var reg = /^\d$|^[0-4][0-9]$|50/ig;
			if(!reg.test(commision)){
				layer.msg("请输入正整数，且范围为0-50");
				return false;
			}
			if(commision<0 || commision>100){
				layer.msg("请输入正整数，且范围为0-50");
				return false;
			}

			$.ajax({
				url : contextPath + "/admin/shopDetail/changeShopCommision",
				data : {
					"shopId" : shopId,
					"commision" : commision
				},
				type : 'post',
				async : true, //默认为true 异步
				dataType : 'json',
				error : function(data) {
				},
				success : function(data) {
					if (data == "OK") {
						layer.close(index); //如果设定了yes回调，需进行手工关闭
						layer.msg("设置成功");
						setTimeout(pager($("#curPageNO").val()),1500);
					} else {
						layer.msg(data);
					}
					return true;
				}
			});

		}
	});
}

function deleteShopCommission(shopId) {
	if (isBlank(shopId)) {
		layer.msg("该店铺信息有误，或不存在");
		return false;
	}
	layer.confirm("确定清除店铺配置的分佣比例?清除后默认恢复使用平台设置的分佣比例", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		window.location = contextPath + "/admin/shopDetail/deleteShopCommision/"+shopId;
	});
}
