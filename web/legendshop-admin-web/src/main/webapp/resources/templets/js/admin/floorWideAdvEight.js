$(document).ready(function() {
	$("div[class='wide_floor_4']").hover(function () {  
		$("#floorImage").show();
	},function () {  
		$("#floorImage").hide();  
	});
});

function onclickAdv(id){
	layer.open({
		title: '编辑广告',
		id: 'advList',
		type: 2,
		content: contextPath+'/admin/floorItem/advListOverlay/'+ id+"?limit=8&layout=wide_adv_eight",
		area: ['750px', '500px'],
		cancel: function(index, layero){
			parent.location.reload();
		}
	});
}

//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}