$("#payForm").validate({
	errorElement : "div", //可以用其他标签，记住把样式也对应修改
	errorClass : "ant-form-explain",
	rules : {
		MCH_ID : {
			required : true
		},
		APPID : {
			required : true
		},
		APPSCECRET : {
			required : true
		}
		,
		PARTNERKEY : {
			required : true
		}
	},
	messages : {
		MCH_ID : {
			required : '请输入Mchid(商户号)'
		},
		APPID : {
			required : '请输入APPID(应用ID)'
		},
		APPSCECRET : {
			required : '请输入APPSCECRET'
		},
		PARTNERKEY : {
			required : '请输入PARTNERKEY(API密钥)'
		}
	},
	submitHandler : function(from) {

		$(from).ajaxSubmit({
			dataType:"json",
			success: function(data) {
				if(data == "OK") {
					layer.alert("保存成功",{icon:1},function(){
						parent.layer.closeAll();
					});
				}else {
					layer.alert(result,{icon:2});
				}          	
			}
		});

	}
});

$(".ant-radio-refund").click(function() {
	$(".ant-radio-refund").parent().removeClass("ant-radio-checked");
	$(this).parent().addClass("ant-radio-checked");
	var isRefund=$(this).val();
	if(isRefund==1){
		$("#payForm input[name='file']").rules("add", {required :true, messages: {required:"请上传原路退款证书"} });
	}else{
		$("#payForm input[name='file']").rules("remove");
	}
	$("#payForm input[name='isRefund']").val($(this).val());
});


$(".ant-radio-isEnable").click(function() {
	$(".ant-radio-isEnable").parent().removeClass("ant-radio-checked");
	$(this).parent().addClass("ant-radio-checked");
	$("#payForm input[name='isEnable']").val($(this).val());
});



$(".ant-checkbox-input").click(function() {
	var on = $(this).attr("switch");
	if (on == "on") { //关闭状态
		$(this).parent().addClass("ant-checkbox-checked");
		$(this).prop("checked", true);
		$(this).attr("switch", "off");
	} else {
		$(this).parent().removeClass("ant-checkbox-checked");
		$(this).prop("checked", false);
		$(this).attr("switch", "on");
	}
});