$(document).ready(function() {
	$("div[class='wide_floor']").hover(function () {  
		$("#prodEdit").show();
	},function () {  
		$("#prodEdit").hide();  
	});
});

function onclickAdvts(id){
	layer.open({
		title: '编辑广告',
		id: 'advList',
		type: 2,
		content: contextPath+'/admin/floorItem/advListOverlay/'+ id+"?limit=5&layout=wide_adv_five",
		area: ['750px', '550px'],
		cancel: function(index, layero){
			parent.location.reload();
		} 
	});
}

//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}