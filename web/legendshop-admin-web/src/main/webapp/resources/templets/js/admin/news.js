function checkFinish() {
	var result = checkSensitiveData();
	if (result != null && result != '') {
		layer.confirm("您的输入含有敏感字： '" + result + "' 请更正后再提交", {icon:2});
		return false;
	} else {
		return true;
	}
}

jQuery.validator.addMethod("checkSensitiveData1", function (value, element) {
	return checkSensitiveData(value);
}, '');

$(document).ready(function () {
	jQuery("#form1").validate({
		rules: {
			newsCategoryId: "required",
			newsTitle: {
				required: true,
				minlength: 3,
				maxlength: 50
			},
			newsContent: {
				maxlength:60000,
				required: true
			},
			seq: {
				number: true,
				digits:true
			}
		},
		messages: {
			newsCategoryId: {
				required: "请填写文章栏目"
			},
			newsTitle: {
				required: "请输入文章标题",
				minlength: "请认真填写，文章标题不能少于3个字符",
				maxlength: "超出长度限制50个字",
			},
			newsContent: {
				maxlength:"超出长度限制",
				required: "存在黑名单的关键字"
			},
			seq: {
				number: "请输入正确的数字!",
				digits: "不能输入负数"
			}
		}
	});
	//斑马条纹
	$("#col1 tr:nth-child(even)").addClass("even");
	KindEditor.options.filterMode = false;
	KindEditor.create('textarea[name="newsContent"]', {
		cssPath: contextPath+'/resources/plugins/kindeditor/plugins/code/prettify.css',
		uploadJson: contextPath+'/editor/uploadJson/upload;jsessionid='+UUID,
		fileManagerJson: contextPath+'/editor/uploadJson/fileManager',
		allowFileManager: true,
		imgMaxSize : 3*1024*1024,
		afterBlur: function () {
			this.sync();
		},
		width: '100%',
		height: '400px',
		afterCreate: function () {
			var self = this;
			KindEditor.ctrl(document, 13, function () {
				self.sync();
				document.forms['example'].submit();
			});
			KindEditor.ctrl(self.edit.doc, 13, function () {
				self.sync();
				document.forms['example'].submit();
			});
		}
	});
	prettyPrint();
});

function checkSensitiveData() {
	var content = $("#newsContent").val();
	var url = contextPath+"/admin/sensitiveWord/filter";
	$.ajax({
		url: url,
		data: {"src": content},
		type: 'post',
		dataType: 'json',
		async: false, //默认为true 异步   
		success: function (retData) {
			result = retData;
		}
	});
	return result;
}

jQuery.validator.addMethod("isNumber", function(value, element) {
	return this.optional(element) || /^[-\+]?\d+$/.test(value) || /^[-\+]?\d+(\d+)?$/.test(value);
}, "必须整数");

//文章标签
$("#newsTags").select2({
	data: tagData,
	multiple: true,
	initSelection: function (element, callback) {
		var selectData = [];
		$(element.val().split(",")).each(function () {
			var val = this;
			for (var i = 0; i < tagData.length; i++) {
				if (tagData[i].id == val) {
					selectData.push(tagData[i]);
					continue;
				}
			}
		});
		callback(selectData);
	}
});

$("#positionTags").select2({
	data: positiontagData,
	multiple: true,
	initSelection: function (element, callback) {
		var selectData = [];
		$(element.val().split(",")).each(function () {
			var val = this;
			for (var i = 0; i < positiontagData.length; i++) {
				if (positiontagData[i].id == val) {
					selectData.push(positiontagData[i]);
					continue;
				}
			}
		});
		callback(selectData);
	}
});
