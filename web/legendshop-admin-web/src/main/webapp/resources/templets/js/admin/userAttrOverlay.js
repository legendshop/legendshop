function pager(curPageNO){
	var thisObj = $("#J_CustomSKUList div[class=sku-optbar] a");
	selectAttribute(thisObj,curPageNO);
}

//删除自定义属性
function deleteParam(id){
	$.ajax({
		url:contextPath+'/admin/userAttribute/delete',
		data: {"id":id},
		type:'post', 
		async : true, //默认为true 异步   
		error: function(jqXHR, textStatus, errorThrown) {
			// alert(textStatus, errorThrown);
		},
		success:function(result){
			$("#pup" + id).remove();
		}
	});
}

//使用该模板
function useParam(id){
	$.ajax({
		url:contextPath+'/admin/userAttribute/use/'+id, 
		type:'post', 
		dataType : 'json',
		async : true, //默认为true 异步   
		error: function(jqXHR, textStatus, errorThrown) {
			// alert(textStatus, errorThrown);
		},
		success:function(result){


			if(result !=null){
				//输出 console.debug("result: "+result);
				var dataObj = eval("("+result+")");
				$("#J_CustomSKUList").find("div[class^=sku-group]").remove();

				$.each(dataObj,function(idx,item){
					//输出 console.debug(item.key+"，"+item.value+"，"+idx);

					var paramValues = '';
					for(var i=0; i<12; i++){
						if(item.value[i] !="" && item.value[i] != null && item.value[i] != undefined){
							paramValues =paramValues+'<li style="margin-right: 3px;"><input type="text" id="paramValue0" class="text" maxlength="20" value="'+item.value[i]+'" /></li>';
						}else{
							paramValues =paramValues+'<li style="margin-right: 3px;"><input type="text" id="paramValue0" class="text" maxlength="20" /></li>';
						}
					}

					var userParam ='<div class="sku-group sku-custom">'+
					'<label class="sku-label">'+
					'<div class="J_CustomCaption input-ph-wrap">'+
					'<label  class="label" style="display: none;">销售属性名称</label>'+
					'<input type="text" id="paramKey" class="sku-caption text" maxlength="4" value="'+item.key+'"/>：'+
					'</div>'+
					'</label>'+
					'<div class="sku-box">'+
					'<ul class="sku-list">'+
					paramValues +
					'</ul>'+
					'<div class="sku-custom-operations"><a title="删除" class="sku-delgroup J_SKUDelGroup" href="javascript:void(0);" onclick="delProperties(this);">删除</a></div>'+
					'</div>'+
					'</div>';

					if($("#J_CustomSKUList").find("div[class^=sku-group]").length<4){
						$("#J_CustomSKUList").append(userParam);
					}
				});
				$('.area_box').remove();
			}
		}
	});
}