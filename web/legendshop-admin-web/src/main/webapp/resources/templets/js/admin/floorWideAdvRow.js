$(document).ready(function() {
   		$("div[class='wide_floor_h']").hover(function () {
   				$("#floorImage").show();
   		    },function () {  
   		    	$("#floorImage").hide();  
   		    });
   	 });
   	 
     function onclickAdv(floorId,fiId){
        if(fiId==null || fiId=="" || fiId==undefined){
        	layer.open({
        		title: '编辑广告',
        		id: 'advFloor',
        		type: 2,
        		content: contextPath+'/admin/floorItem/advFloorOverlay/'+ floorId+"?layout=wide_adv_row",
        		area: ['650px', '400px'],
        		cancel: function(index, layero){
        			parent.location.reload();
        		}
        	});
        }else{
        	layer.open({
        		title: '编辑广告',
        		id: 'advFloor',
        		type: 2,
        		content: contextPath+'/admin/floorItem/advFloorOverlay/'+ floorId +'/'+ fiId+"?layout=wide_adv_row",
        		area: ['650px', '400px'],
        		cancel: function(index, layero){
        			parent.location.reload();
        		}
        	});
        }
    }
 
	 //方法，判断是否为空
	 function isBlank(_value){
	 	if(_value==null || _value=="" || _value==undefined){
	 		return true;
	 	}
	 	return false;
	 }