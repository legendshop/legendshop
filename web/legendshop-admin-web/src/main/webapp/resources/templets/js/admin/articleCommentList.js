function deleteById(id) {
	layer.confirm('确定删除 ?', {icon:3}, function(){
		window.location = contextPath+ "/admin/article/comment/delete/" + id;
	});
}

//更改审核状态
function exchangeStatus(articleCommentId, status) {
	$.ajax({
		url : contextPath + "/admin/article/comment/updateStatus",
		data : {
			"articleCommentId" : articleCommentId,
			"status" : status,
		},
		type : 'post',
		dataType : 'json',
		async : false, //默认为true 异步   
		error : function(jqXHR, textStatus, errorThrown) {
		},
		success : function(result) {
			if (result == "OK") {
				window.location.href = contextPath + "/admin/article/comment/query?curPageNO=" + curPageNO;
			} else {

			}

		}
	});
}