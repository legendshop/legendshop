$(document).ready(function() {
	$(document).delegate(".order_money_modify",clickType,function(){
		var freight=$(this).attr("freight");
		var freightCash;
		if(freight==null || freight==undefined || freight==""){
			freightCash=Number(0);
		}else{
			freightCash=freight;
		}
		var actualTotal=$(this).attr("actualTotal");
		var name=$(this).attr("name");
		var subNumber=$(this).attr("subNumber");
		var total=Number(actualTotal)-Number(freightCash);
		changeAmountShow(subNumber,name,total,freightCash,actualTotal);
	});

	$(document).delegate("#order_fee_amount","change",function(){
		var fee_amount=$(this).val();
		if(fee_amount==null || fee_amount==undefined || fee_amount==""){
			layer.alert("请正确输入商品总价", {icon:2});
			return false;
		}
		if(!isMoney(fee_amount) || fee_amount==0){
			layer.alert("请正确输入商品总价", {icon: 2});
			return false;
		}

		var _freight=$("#order_fee_ship_price").val();
		if(_freight==null || _freight==undefined || _freight==""){
			_freight=0;
		}
		if(!isMoney(_freight)){
			layer.alert("请正确输入配送金额", {icon: 2});
			return false;
		}
		$("#totalPrice").val(Number(fee_amount)+Number(_freight));
		$("#totalPrice").prev().html(Number(fee_amount)+Number(_freight));
	});

	$(document).delegate("#order_fee_ship_price","change",function(){
		var fee_amount=$("#order_fee_amount").val();
		if(fee_amount==null || fee_amount==undefined || fee_amount==""){
			layer.alert("请正确输入商品总价", {icon: 2});
			return false;
		}
		if(!isMoney(fee_amount) || fee_amount==0){
			layer.alert("请正确输入商品总价", {icon: 2});
			return false;
		}
		var _freight=$(this).val();
		if(_freight==null || _freight==undefined || _freight==""){
			_freight=0;
		}
		if(!isMoney(_freight)){
			layer.alert("请正确输入配送金额", {icon: 2});
			return false;
		}
		$("#totalPrice").val(Number(fee_amount)+Number(_freight));
		$("#totalPrice").prev().html(Number(fee_amount)+Number(_freight));
	});

	$("b[mark^='name_']").hover(function(){
		$(".xx").hide();
		$(this).parent().parent().find("div.xx").show();
	},function(){
		$(".xx").hide();
	});

	//页面加载模块
	$("b[mark^='name_']").each(function (){
		var _this=$(this);
		var addrOrderId=$(_this).attr("addrOrderId");
		if(addrOrderId!=null || addrOrderId!=undefined || addrOrderId!=""){
			$.ajax({
				url:contextPath+"/admin/order/findUsrAddrSub/"+addrOrderId,  
				type:'POST', 
				async : false, //默认为true 异步   
				dataType:'json',
				error: function(jqXHR, textStatus, errorThrown) {
				},
				success:function(result){
					var html="<div style='display:none;' class='xx'>";
					html += " <h6>买家信息</h6>";
					html += " <ul id='table_56'>";
					if(!isBlank(result.receiver)){
						html += "<li><div> <span class='xx_sp'>姓 名：</span><span class='xx_date'>"+result.receiver+"</span></div></li>";
					}
					if(!isBlank(result.telphone)){
						html += "<li><div> <span class='xx_sp'>电 话：</span><span class='xx_date'>"+result.telphone+"</span></div></li>";
					}
					if(!isBlank(result.mobile)){
						html += "<li><div> <span class='xx_sp'>手 机：</span><span class='xx_date'>"+result.mobile+"</span></div></li>";
					}
					if(!isBlank(result.subPost)){
						html += "<li><div> <span class='xx_sp'>邮 编：</span><span class='xx_date'>"+result.subPost+"</span></div></li>";
					}
					if(!isBlank(result.detailAddress)){
						html += "<li><div> <span class='xx_sp'>地 址：</span><span class='xx_date'>"+result.detailAddress+"</span></div></li>";
					}
					html += "</ul></div>";
					$(_this).append(html);
				}


			});
		}
	});

});

function switch_reason(){
	var ck=jQuery("#radio3").attr("checked");
	var i = $("input:radio[id='radio3']:checked").val();
	if(i) {
		$("#other_reason").show();
	} else {
		$("#other_reason").hide();  
	}
}

function isMoney(obj) {
	if (!obj)
		return false;
	return (/^\d+(\.\d{1,2})?$/).test(obj);
}

function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("ListForm").submit();
}

function changeOrderFee(){
	var _subnumber=$.trim($("#order_fee_id").html());
	if(_subnumber==null || _subnumber=="" || _subnumber==undefined){
		return;
	}
	var fee_amount=$.trim($("#order_fee_amount").val());
	if(fee_amount==null || fee_amount==undefined || fee_amount==""){
		layer.alert("请正确输入商品总价", {icon: 2});
		return false;
	}
	if(!isMoney(fee_amount) || fee_amount==0){
		layer.alert("请正确输入商品总价", {icon: 2});
		return false;
	}
	var _freight=$.trim($("#order_fee_ship_price").val());
	if(_freight==null || _freight==undefined || _freight==""){
		_freight=0;
	}
	if(!isMoney(_freight)){
		layer.alert("请正确输入配送金额", {icon: 2});
		return false;
	}
	$("#orderFee").attr("disabled","disabled");
	$.ajax({
		url:contextPath+"/admin/order/changeOrderFee", 
		data:{"subNumber":_subnumber,"freight":_freight,"orderAmount":fee_amount},
		type:'POST', 
		async : false, //默认为true 异步   
		dataType:'json',
		error: function(jqXHR, textStatus, errorThrown) {
			closeOrderFee();
			$("#orderFee").attr("disabled","");
			return;
		},
		success:function(result){
			if(result=="OK"){
				closeOrderFee();
				layer.msg("调整订单费用成功", {icon: 1, time: 700}, function(){
					window.location.reload(true);
				});
			}else if(result=="fail"){
				closeOrderFee();
				var msg = "调整订单费用失败";
				$("#orderFee").attr("disabled","");
				layer.alert(msg, {icon: 2});
				return;
			}
		}
	});
}

function orderCancel(){
	var _subnumber=$("#sub_id").val();
	if(_subnumber==null || _subnumber=="" || _subnumber==undefined){
		return;
	}
	var other=$('input:radio[name=state_info]:checked').val();
	if(other==null || other=="" || other==undefined){
		layer.alert("请选择设置取消原因", {icon: 2});
		return;
	}
	var info=""; 
	if(other=="其他原因"){
		var other_info=$('#other_state_info').val();
		if(other_info==null || other_info=="" || other_info==undefined){
			layer.alert("请填写备注信息", {icon: 2});
			return;
		}
		info=other_info;
	}else{
		info=other;
		$('#other_state_info').val("");
	}
	$.ajax({
		url:contextPath+"/admin/order/orderCancel", 
		data:{"subNumber":_subnumber,"status":5,"other":info},
		type:'POST', 
		async : false, //默认为true 异步   
		dataType:'json',
		error: function(jqXHR, textStatus, errorThrown) {
			$(".Popup_shade").css('display','none'); 
			$("#order_cancel").css('display','none'); 
			return;
		},
		success:function(result){
			if(result=="OK"){
				layer.msg("取消订单成功", {icon: 1, time: 700}, function(){
					window.location.reload(true);
				});
			}else if(result=="fail"){
				var msg = "取消订单失败";
				layer.alert(msg, {icon: 2});
				$(".Popup_shade").css('display','none'); 
				$("#order_cancel").css('display','none'); 
				return;
			}
		}
	});
}

function orderCancelShow(_subnumber){
	if(_subnumber==null){
		return;
	}
	$("#sub_id").val(_subnumber);
	$("#sub_id").parent().next().html(_subnumber);
	$(".Popup_shade").css('display','block'); 
	$("#order_cancel").css('display','block'); 
}


function closeOrderFee(){
	$("#order_fee_username").html("");
	$("#order_fee_id").html("");
	$("#order_fee_id").prev().val("");
	$("#order_fee_amount").val("");
	$("#order_fee_ship_price").val("");
	$("#totalPrice").val("");
	$("#totalPrice").prev().html("");
	$(".Popup_shade").css('display','none'); 
	$("#order_fee").css('display','none'); 
}

function closeOrderCancel(){
	$("#sub_id").val();
	$(".Popup_shade").css('display','none'); 
	$("#order_cancel").css('display','none'); 
}

function changeAmountShow(_subnumber,_name,_cash,_freight,_totalcash){
	if(_subnumber==null || _name==null ||_cash==null ||_freight==null ||_totalcash==null  ){
		return;
	}
	$("#order_fee_username").html(_name);
	$("#order_fee_id").html(_subnumber);
	$("#order_fee_id").prev().val(_subnumber);
	$("#order_fee_amount").val(new Number(_cash));
	$("#order_fee_ship_price").val(new Number(_freight));
	$("#totalPrice").val(new Number(_totalcash));
	$("#totalPrice").prev().html(new Number(_totalcash));
	$(".Popup_shade").css('display','block'); 
	$("#order_fee").css('display','block'); 
}

function deliverGoods(_orderId){
	layer.open({
		title: '确认发货',
		id:'deliverGoods',
		type: 2,
		content: contextPath+'/admin/order/deliverGoods/'+_orderId,
		area: ['430px', '250px']
	});
}

function export_excel(){
	var list=$(".orderlist_one").get();
	if(list.length<=0){
		layer.alert("没有导出的订单数据", {icon: 2});
		return;
	}
	var orders=new Array();
	for(var i = 0; i < list.length; i++){
		var _orderId=$(list[i]).attr("orderId");
		if(_orderId!=undefined  && _orderId!="" && _orderId!=null && _orderId!=0){
			orders.push(_orderId);
		}
	}
	if(orders.length<=0){
		layer.alert("没有导出的订单数据", {icon: 2});
		return;
	}
	$(".editAdd_load").show();
	jQuery.ajax({
		url:contextPath+"/admin/order/exportExcel",
		data:{"items":orders},
		type:"post", 
		async : false, //默认为true 异步  
		dataType : 'json',  
		error:function(data){
			$(".editAdd_load").hide();
			layer.alert("导出数据失败", {icon: 2});
			return ;
		},   
		success:function(data){
			if(data.result=="false"){
				$(".editAdd_load").hide();
				layer.alert(data.msg, {icon: 2});
				return
			}
			else if(data.result=="true"){
				$(".editAdd_load").hide();
				var url=contextPath+"/servlet/downloadfileservlet/"+data.msg;
				window.open(url);
				return;
			}
		}   
	});  
}

//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}

function exportOrder(){
	var status="${paramDto.status}";
	var shopName="${paramDto.shopName}";
	var orderSn="${paramDto.subNumber}";
	var userName="${paramDto.userName}";
	var startDate="${paramDto.startDate}";
	var endDate="${paramDto.endDate}";
	var subtype="${paramDto.subType}";
	var array=[];
	array.push(status);
	array.push(shopName);
	array.push(orderSn);
	array.push(userName);
	array.push(startDate);
	array.push(endDate);
	array.push(subtype);
	var jsondata=JSON.stringify(array);

	$("#exportParam").val(jsondata);
	$("#exportForm").submit();
}
function search(){
	$("#ListForm #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	$("#ListForm")[0].submit();
}