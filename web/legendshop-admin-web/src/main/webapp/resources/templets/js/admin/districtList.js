if(typeof JSON == 'undefined'){$('head').append($("<script type='text/javascript' src='"+contextPath+"/resources/common/js/json.js'>"));}
//默认一上来就初始三级联动和文本框加载省份
jQuery(document).ready(function(){
	//三级联动
	$("select.combox").initSelect();
	var url = contextPath+"/admin/system/district/queryProvince";
	if(areaid != ''){
		url = contextPath+"/admin/system/district/queryAreas/"+ cityid;
	}else if(cityid != ''){
		url = contextPath+"/admin/system/district/queryCities/"+ provinceid;
	}

	sendData(url);
});

//通过点击下拉框来把文本框的省份变成城市
function changeCities(provinceid){
	if(isBlank(provinceid)){
		return;
	}
	var url = contextPath+"/admin/system/district/queryCities/"+ provinceid;
	sendData(url);
}       

//通过点击下拉框来把文本框的城市变成地区
function changAreas(cityid){
	if(isBlank(cityid)){
		return;
	}
	var url = contextPath+"/admin/system/district/queryAreas/"+ cityid;
	sendData(url);
}

//通过点击下拉框来吧文本框的地区进行筛选
function filterAreas(areaid){
	if(isBlank(areaid)){
		return;
	}
	var url = contextPath+"/admin/system/district/filterAreas/"+ areaid;
	sendData(url);
}

function sendData(url){
	$.ajax({
		"url":url, 
		type:'post', 
		async : true, //默认为true 异步   
		error: function(jqXHR, textStatus, errorThrown) {
			layer.alert("系统 错误异常，请联系管理员",{icon:2});
		},
		success:function(result){
			$("#districtContentList").html(result);
		}
	});
}

function isBlank(_value) {
	if (_value == null || _value == "" || _value == undefined) {
		return true;
	}
	return false;
}

//页面的删除

function deleteAction(type){
	//获取选择的记录集合
	selAry = $(".selectOne");
	if(!checkSelect(selAry)){
		layer.alert('删除时至少选中一条记录！',{icon:0});
		return false;
	}

	layer.confirm("删除后不可恢复, 确定要删除吗？", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		var ids = []; 
		for(i=0;i<selAry.length;i++){
			if(selAry[i].checked){
				ids.push(selAry[i].value);
			}
		}
		var result = deleteDistrict(ids,type);
		if('OK' == result){
			layer.msg('删除地区成功',{icon:1,time:500},function(){
				window.location = contextPath+'/admin/system/district/query';
			});
		}else{
			layer.msg('删除地区失败',{icon:2});
		}
	})
	return true;
}

function deleteDistrict(ids,type) {
	var result;
	var idsJson = JSON.stringify(ids);
	var url = contextPath+"/admin/system/district/" + type;
	jQuery.ajax({
		"url":url , 
		data: {"ids": idsJson},
		type:'post', 
		async : false, //默认为true 异步   
		dataType : 'json', 
		error: function(jqXHR, textStatus, errorThrown) {
			layer.alert("系统 错误异常，请联系管理员",{icon:2});
		},
		success:function(retData){
			result = retData;
		}
	});

	return result;
}

//删除省份
function confirmDeletePrivonce(provinceid,province){
	layer.confirm("删除后不可恢复, 确定要删除'"+province+"'吗？", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		var url=contextPath+"/admin/system/district/deleteprovince/" + provinceid;
		var result = deleteLocation(url);
		if('OK' == result){
			layer.msg('删除成功!',{icon:1,time:500},function(){
				window.location = contextPath+'/admin/system/district/query';
			});
		}else{
			layer.msg("删除失败!",{icon:2});
		}

	});
}

//删除城市
function confirmDeleteCity(cityid,city){
	layer.confirm("删除后不可恢复, 确定要删除'"+city+"'吗？", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		var url=contextPath+"/admin/system/district/deletecity/" + cityid;
		var result = deleteLocation(url);
		if('OK' == result){
			layer.msg('删除成功!',{icon:1,time:500},function(){
				window.location = contextPath+'/admin/system/district/query';
			});
		}else{
			layer.msg("删除失败!",{icon:2});
		}
	});
}


//删除地区
function confirmDeleteArea(areaid,area){
	layer.confirm("删除后不可恢复, 确定要删除'"+area+"'吗？", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		var url=contextPath+"/admin/system/district/deletearea/" + areaid;
		var result = deleteLocation(url);
		if('OK' == result){
			layer.msg('删除成功!',{icon:1,time:500},function(){
				window.location = contextPath+'/admin/system/district/query';
			});
		}else{
			layer.msg("删除失败!",{icon:2});
		}
	});
}

function deleteLocation(url) {
	var result;
	jQuery.ajax({
		"url":url, 
		type:'post', 
		async : false, //默认为true 异步   
		dataType : 'json', 
		error: function(jqXHR, textStatus, errorThrown) {
			layer.alert("系统 错误异常，请联系管理员",{icon:2});
		},
		success:function(retData){
			result = retData;
		}
	});
	return result;
}

function addDisctrct(){
	var provinceid = $("#provinceid").val();
	var cityid = $("#cityid").val();
	if(provinceid == null || provinceid == '' ){
		window.location.href= contextPath+"/admin/system/district/addprovince";
	}else {
		if(cityid == null || cityid == ''){
			window.location.href= contextPath+"/admin/system/district/addcity/" + provinceid;
		}else{
			window.location.href= contextPath+"/admin/system/district/addarea/" + cityid;
		}
	}
}