$(document).ready(function () {
	
	var shopName = $("#siteName").val();

	if(shopName == ''){
        makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'shopName');
    }else{
    	makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",shopName, 'shopName');
		$("#select2-chosen-1").html(shopName);
    } 
	
	sendData(1);
	
});

function getCheckedIds(){
	var checkedBrand=$("input[type=checkbox][name=consultId]:checked");
	var consultIds=new Array();
	checkedBrand.each(function(index, domEle){
		var bandId=$(domEle).val();
		consultIds.push(bandId);
	});
	return consultIds.toString();
}

function batchDel(){
	var brandIdsStr=getCheckedIds();
	if(brandIdsStr==""){
		layer.msg("请选择要删除的咨询", {icon:0});
	}
	jQuery.ajax({
		url: contextPath+"/admin/productConsult/batchDelete/"+brandIdsStr,
		type: 'get',
		async: false, //默认为true 异步  
		data:{brandStatus:0},
		dataType: 'json',
		success: function (data) {
			if (data== "OK") {
				layer.msg("批量删除成功。", {icon:1, time:700}, function(){
					setInterval(function(){
						location.href=contextPath+"/admin/productConsult/query";
					},1000);
				});
			}else{
				layer.msg("批量删除失败。", {icon:2});
			}
		}
	});
};

function deleteById(id) {
	layer.confirm('确定删除？', {icon:3}, function(){
		window.location = contextPath+"/admin/productConsult/delete/" + id;
	});
}

highlightTableRows("item");
function search(){
	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	sendData(1);
}

laydate.render({
	elem: '#startTime',
	calendar: true,
	theme: 'grid',
	trigger: 'click'
}); 
laydate.render({
	elem: '#endTime',
	calendar: true,
	theme: 'grid',
	trigger: 'click'
});

function sendData(curPageNO) {
    var data = {
    	"curPageNO": curPageNO,
        "shopId": $("#shopId").val(),
        "siteName": $("#siteName").val(),
        "prodName": $("#prodName").val(),
        "nickName": $("#nickName").val(),
        "pointType": $("#pointType").val(),
        "replySts": $("#replySts").val(),
        "startTime": $("#startTime").val(),
        "endTime": $("#endTime").val(),
    };
    $.ajax({
        url: contextPath+"/admin/productConsult/queryContent",
        data: data,
        type: 'post',
        async: true, //默认为true 异步   
        success: function (result) {
            $("#productConsultList").html(result);
        }
    });
}

function makeSelect2(url,element,text,label,value,holder, textValue){
	$("#"+element).select2({
        placeholder: holder,
        allowClear: true,
        width:'200px',
        ajax: {  
    	  url : url,
    	  data: function (term,page) {
                return {
                	 q: term,
                	 pageSize: 10,    //一次性加载的数据条数
                 	 currPage: page, //要查询的页码
                };
            },
			type : "GET",
			dataType : "JSON",
            results: function (data, page, query) {
            	if(page * 5 < data.total){//判断是否还有数据加载
            		data.more = true;
            	}
                return data;
            },
            cache: true,
            quietMillis: 200//延时
      }, 
        formatInputTooShort: "请输入" + text,
        formatNoMatches: "没有匹配的" + text,
        formatSearching: "查询中...",
        formatResult: function(row,container) {//选中后select2显示的 内容
            return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
        },formatSelection: function(row) { //选择的时候，需要保存选中的id
        	$("#" + textValue).val(row.text);
            return row.text;//选择时需要显示的列表内容
        }, 
    });
}

function pager(curPageNO) {
    $("#curPageNO").val(curPageNO);
    sendData(curPageNO);
}