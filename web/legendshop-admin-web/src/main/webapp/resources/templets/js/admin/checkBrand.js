$("#returnBtn").on("click", function(){
	window.location.href = backURL;
	var backURL = contextPath+"/admin/brand/query";
	window.location.href = backURL;
});

function check(brandId,status){
	layer.prompt({
		formType: 2,
		title: '处理意见',
		area: ['368px', '110px'], //自定义文本域宽高
		maxlength: 100
	}, function(value, index, elem){
		if(value!=null && value!=''){
			$.ajax({
				
				url: contextPath+"/admin/brand/checkBrand/",
				data: {"brandId":brandId,"status":status,"opinion":value},
				type:'post', 
				async : true, //默认为true 异步   
				dataType:'json',
				success:function(data){
					if(data=="OK"){
						layer.alert("处理成功", {icon:1}, function(index){
							window.location.reload();
							lay.close(index);
						});
						return true;
					}else{
						layer.alert("处理失败", {icon:2});
						return false;
					}
				}
			});
		}else{
			layer.msg("处理意见不能为空",{icon:2});
			return false;
		}
	});
}