function selectCategory() {
	var zTree = $.fn.zTree.getZTreeObj("catTrees");
	var chkNodes = zTree.getCheckedNodes(true);
	if (chkNodes.length < 1) {
		layer.msg("请选择类目", {
			icon : 0
		});
		return;
	}
	var catId = chkNodes[0].id;
	var catName = chkNodes[0].name;

	var level = chkNodes[0].level;
	var first_cid = catId;
	var two_cid;
	var third_cid;
	if (level == 1) { //2级
		var parent = chkNodes[0].getParentNode();
		first_cid = parent.id;
		two_cid = catId;
	} else if (level == 2) { //3级
		var parent = chkNodes[0].getParentNode();
		var firstParent = parent.getParentNode();
		first_cid = firstParent.id;
		two_cid = parent.id;
		third_cid = catId;
	}
	if (type == "I" || type == "G") {
		window.parent.loadCategoryCallBack(first_cid, two_cid, third_cid, catName); //访问父页面方法 
		window.parent.layer.closeAll();
	} else {
		window.parent.loadCategoryCallBack(catId, catName); //访问父页面方法 
		window.parent.layer.closeAll();
	}
}

function clearCategory() {
	if(type=='I'){
		$(window.parent.document).find('#firstCid').val("");
		$(window.parent.document).find('#twoCid').val("");
		$(window.parent.document).find('#thirdCid').val("");
		$(window.parent.document).find('#prodCatName').val("");
	}else{
		$(window.parent.document).find('#prodCatName').val("");
		$(window.parent.document).find('#prodCatId').val("");
	}
	
	window.parent.layer.closeAll();
}

$(document).ready(function() {
	//zTree设置
	var catTrees = JSON.parse(data); 
	var setting = {
			check : {
				enable : true,
				chkboxType : {
					'Y' : 'ps',
					'N' : 'ps'
				},
				chkStyle : 'radio',
				radioType : 'all'
			},
			data : {
				simpleData : {
					enable : true
				}
			},
			callback : {
				beforeCheck : beforeCheck,
				onCheck : onCheck
			}
	};
	$.fn.zTree.init($("#catTrees"), setting, catTrees);
});

function beforeCheck(treeId, treeNode, clickFlag) {
	var all = "${all}";
	if (all != undefined && all != null && all != "") {
		return true;
	}
	if (treeNode.isParent) {
		layer.alert("请选择最底层分类", {
			icon : 7
		});
	}
	return !treeNode.isParent;//当是父节点 返回false 不让选取
}

function onCheck() {
}