$("#payForm").validate({
	errorElement: "div", //可以用其他标签，记住把样式也对应修改
	errorClass:"ant-form-explain",
	rules : {
		seller_email: {
			required: true
		},
		partner: {
			required: true
		},
		key: {
			required: true
		},
		appId: {
			required: true
		},
		rsa_private: {
			required: true
		},
		rsa_publish: {
			required: true
		}
	},
	messages : {
		seller_email: {
			required: '请输入支付宝企业账户'
		},
		partner: {
			required: '请输入合作者身份'
		},
		key: {
			required: '请输入安全校验码'
		},
		appId: {
			required: '请输入开放平台应用ID'
		},
		rsa_private: {
			required: '请输入商户应用私钥'
		},
		rsa_publish: {
			required: '请输入支付宝公钥'
		}
	},
	submitHandler : function() {
		var data = $("#payForm").serialize();
		console.log(data);
		var url = contextPath + "/admin/paytype/payEditAlipay";
		$.ajax({
			url: url,
			data: data,
			type: "post",
			async: false,
			dataType: "json",
			error: function() {
				layer.alert("网络延迟,请稍后再试",{icon:2});
			},
			success: function(result) {
				if(result == "OK") {
					layer.alert("保存成功",{icon:1},function(){
						parent.layer.closeAll();
					});
				}else {
					layer.alert(result,{icon:2});
				}
			},
		}); 

	}
});


$(".ant-radio-input").click(function() {
	$(".ant-radio-input").parent().removeClass("ant-radio-checked");
	$(this).parent().addClass("ant-radio-checked");
	$("#payForm input[name='isEnable']").val($(this).val());
});

$(".ant-checkbox-input").click(function() {
	var on=$(this).attr("switch");
	if(on == "on" ){ //关闭状态
		$(this).parent().addClass("ant-checkbox-checked");
		$(this).prop("checked", true);
		$(this).attr("switch","off");
	}else{
		$(this).parent().removeClass("ant-checkbox-checked");
		$(this).prop("checked", false);
		$(this).attr("switch","on");
	}
});