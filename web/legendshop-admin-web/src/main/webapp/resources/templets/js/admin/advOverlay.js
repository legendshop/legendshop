jQuery.validator.addMethod("checkFile", function(value, element) {
	return checkFile(value,element);}, '<fmt:message key="errors.required"><fmt:param value=""/></fmt:message>'); 

function checkFile(val,element){
	if(val=="" && $(element).attr("oldFile")==""){
		return false;
	}
	return true;
}

$(document).ready(function() {
	jQuery("#updateForm").validate({
		rules : {
			title : "required",
			linkUrl : {
				required:true,
				maxlength:200
			},
			"file": {
				checkFile: true
			}
		},
		messages : {
			title : "请输入标题名称！",

			linkUrl : {
				required:"请输入路径！",
				maxlength:"链接长度不能超过200个字符！"
			},
			file:{
				checkFile: "请上传文件！"
			}
		},
		submitHandler : function(form) {
			$("#updateAdvt").attr("disabled","disabled");
			var file=$("input[name=file]").val();
			if(file=="" || file==null || file==undefined){
				$("#picUrl").html("");
			}
			$("#updateForm").ajaxForm().ajaxSubmit({
				success:function(result) {
					var data=eval(result);
					if(data=="success"){
						parent.location.reload();
						return true;
					}else{
						layer.alert(file, {icon:2});
						if(file=="" || file==null || file==undefined){
							$("#picUrl").html("<input type='file'  name='file' id='file' oldFile='"+oldPicUrl+"' />");
						}
						$("#updateAdvt").removeAttr("disabled");
						var msg = "修改失败";
						layer.msg(msg, {icon:2, time:1000});
						return false; 
					}
				},
				error:function(XMLHttpRequest, textStatus,errorThrown) {
					if(file=="" || file==null || file==undefined){
						$("#picUrl").html("<input type='file'  name='file' id='file' oldFile='"+oldPicUrl+"' />");
					}
					$("#updateAdvt").removeAttr("disabled");
					layer.msg("失败", {icon:2, time:1000});
					return false;
				}	
			});
		} 
	});
	//斑马条纹
	$("#col1 tr:nth-child(even)").addClass("even");
});

$("input[name=file]").change(function(){
	checkImgType(this);
	checkImgSize(this,5120);
});
function subImage(url){
	if (url == "" || url == undefined) {
		layer.msg("请选择商品图片", {icon:7});
		return false;
	}
	var imath ='${requestScope.imagePrefix}';
	//var startPath = url.startWith(imath);
	var startPath=url.indexOf(imath);
	if(startPath>=0){
		var subPath = url.substring(startPath+imath.length, url.length);
		return subPath;
	}
	return null;
}

String.prototype.startWith = function(s) {
	if (s == null || s == "" || this.length == 0 || s.length > this.length)
		return false;
	if (this.substr(0, s.length) == s)
		return true;
	else
		return false;
	return true;
}

function appendImg(img){
	removeImg();
	$(".multimage-gallery ul li ").each(function(){
		var $this = jQuery(this);
		$this.find("div[class=preview]").html("");
		$this.find("div[class=preview]").append(img);
		$this.addClass("has-img");
	});
	function removeImg(){
		$(".multimage-gallery ul li ").each(function(){
			var $this = jQuery(this);
			/*  $this.find("div[class=preview]").next().remove(); */
			$this.find("div[class=preview]").html(""); 

		});
	}		
}
//取消按钮
function cancel(){
	var index=parent.layer.getFrameIndex(window.name);
	parent.layer.close(index);
}