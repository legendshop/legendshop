jQuery(document).ready(function() {
	jQuery(".order_follow_top li").click(function() {
		jQuery(".order_follow_box").hide();
		jQuery(".order_follow_top li").removeClass("this");
		jQuery(this).addClass("this");
		var index=jQuery(this).index();
		if(index==1){
			var info= $.trim($("#order_kuaidi tr:eq(1)").html());
			if( (info==null || info==undefined  || info=="") && dvyFlowId!=""){
				$.ajax({
					url:contextPath+"/admin/order/findKuaidiInfo", 
					data:{"dvyFlowId":dvyFlowId,"dvyId":dvyTypeId,"reciverMobile":reciverMobile},
					type:'POST', 
					async : false, //默认为true 异步   
					dataType:'json',
					error: function(jqXHR, textStatus, errorThrown) {
						return;
					},
					success:function(result){
						if(result=="fail" || result=="" ){
							$("#order_kuaidi tr:eq(0)").after("<tr><td>暂无快递信息</td></tr>");
						}else{
							var list = eval(result);
							var html="";
							for(var obj in list){ //第二层循环取list中的对象 
								html+="<tr><td>"+list[obj].ftime+"</td><td>"+list[obj].context+"</td> </tr>";
							} 
							$("#order_kuaidi tr:eq(0)").after(html); 
						}
					}
				});
			}
		}else if(index==2){
			var info= $.trim($("#order_ship tr:eq(1)").html());
			if(info==null || info==undefined  || info=="" ){
				$.ajax({
					url:contextPath+"/admin/order/findSubHisInfo", 
					data:{"subId":subId},
					type:'POST', 
					async : false, //默认为true 异步   
					dataType:'json',
					error: function(jqXHR, textStatus, errorThrown) {
						return;
					},
					success:function(result){
						if(result=="fail" || result=="" ){
							$("#order_ship tr:eq(0)").after("");
						}else{
							var list = eval("("+result+")"); 
							var html="";
							for(var obj in list){ //第二层循环取list中的对象 
								html+="<tr><td>"+list[obj].key+"</td><td>"+list[obj].value+"</td> </tr>";
							} 
							$("#order_ship tr:eq(0)").after(html);
						}
					}
				});

			}
		}
		jQuery(".order_follow_box").eq(jQuery(this).index()).show();
	});
});

function deliverGoods(_orderId){
	layer.open({
		title: '修改发货信息',
		id:'deliverGoods',
		type: 2,
		content: contextPath+'/admin/order/changeDeliverNum/'+_orderId,
		area: ['430px', '290px']
	});
}