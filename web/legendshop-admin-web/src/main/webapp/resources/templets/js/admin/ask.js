$(document).ready(function() {
	jQuery("#form1").validate({
		rules: {
			banner: {
				required: true,
				minlength: 5
			},
			url: "required"
		},
		messages: {
			banner: {
				required: "Please enter banner",
				minlength: "banner must consist of at least 5 characters"
			},
			url: {
				required: "Please provide a password"
			}
		}
	});

	$("#col1 tr").each(function(i){
		if(i>0){
			if(i%2 == 0){
				$(this).addClass('even');
			}else{    
				$(this).addClass('odd'); 
			}   
		}
	});   
	$("#col1 th").addClass('sortable'); 
});