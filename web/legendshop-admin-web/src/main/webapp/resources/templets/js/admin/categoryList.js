jQuery.validator.setDefaults({});


$(document).ready(function(){

	//表单数据校验
	jQuery("#form1").validate({
		rules: {
			newsCategoryName: {
				required: true,
				maxlength: 20
			},
			seq: {
				isNumber: true,
				maxlength: 5
			},
			file:{
				required: "#imgName:blank",
			},
		},
		messages: {
			newsCategoryName: {
				required: "请输入栏目名称",
				minlength: "栏目名称不能少于2个字"
			},
			seq: {
				isNumber: "请输入5位之内的正整数!",
			},
			smallCatFile2:{
				required:"请上传类目小图标图片",
				isImage:"请选择正确的图片上传格式"
			},
			catFile2:{
				required:"请上传类目图片",
				isImage:"请选择正确的图片上传格式"
			},
		}
	});

	$("input[name=catIconFile]").change(function(){
		checkImgType(this);
		checkImgSize(this,1024);
	});

	$("input[name=catPicFile]").change(function(){
		checkImgType(this);
		checkImgSize(this,1024);
	});


	var setting = { check:{}, data:{}, callback:{},view:{} };
	setting.check.enable = true;
	setting.check.chkboxType = {'Y':'ps','N':'ps'};
	setting.check.chkStyle = 'radio';
	setting.check.radioType = 'all';
	setting.check.enable = false;
	setting.view.dblClickExpand = false;
	setting.callback.onClick = clickNode;
	setting.data.simpleData =  { enable: true };

	$.fn.zTree.init($("#catTree"), setting, ${catTrees});

	//expandAll();
	$(".J_spu-property").divselect();

	var treeObj = $.fn.zTree.getZTreeObj("catTree");
	var node = treeObj.getNodeByTId('${category.id}');

	/* $("#headerMenu1").change(function(){
				    var headerMenu = $("#headerMenu1").val();
				    if(headerMenu=='true'){
				    	$("#headerMenu_span").css("display","block");
				    }else{
				    	$("#headerMenu_span").css("display","none");
				    }
				}); */

});

function deleteCategory() {
	var catname = $("#catname").val();
	var catid= $("#catid").val();
	layer.confirm("确定删除类目："+catname+" 吗？", {icon:3, title:'提示'}, function(){
		var zTree = $.fn.zTree.getZTreeObj("catTree");
		//var node = zTree.getNodeById(catid);
		var nodes = zTree.getSelectedNodes();
		if(nodes[0].isParent){
			layer.msg("请先删除子分类", {icon: 2});
		}else{
			// window.location = "<ls:url address='/admin/category/delete/" + catid + "'/>";
			var url=contextPath+"/admin/category/delete/"+catid;
			$.post(url,function(result){
				if(result=="ok"){
					layer.msg('删除成功', {icon: 1,time: 1500}, function(){
						window.location.reload(); 
					}); 
				}else{
					layer.msg("请先删除该分类下的商品", {icon: 2});
				}
			},"json");
		}
	});
}

function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("form1").submit();
}

function zhedie(){
	var treeObj = $.fn.zTree.getZTreeObj("catTree");
	treeObj.expandAll(false);
}

function expandAll() {
	var treeObj = $.fn.zTree.getZTreeObj("catTree");
	treeObj.expandAll(true);
}

function clickNode(event, treeId, treeNode) {
	var catId = treeNode.id;
	if(catId==0){
		$("#catDetail").hide();
		$("#newDetail").show();
	}else{
		$.ajax({
			url:contextPath+"/admin/category/load/"+catId, 
			type:'get', 
			dataType : 'json', 
			async : true, //默认为true 异步   
			error: function(jqXHR, textStatus, errorThrown) {
				layer.alert('err', {icon: 2});
			},
			success:function(retData){
				$("#catid").val(retData.id);
				$("#catname").val(retData.name);
				//$("#catpic").val(retData.pic);
				if(retData.pic=="" || retData.pic=="null" ||retData.pic==null){
					$("#imgspan").html("");
				}else{
					$("#imgspan").html("<img src='"+photos+retData.pic+"'/>"+"' width='100' height='100' style='margin-right:10px;'>");
				}
				if(retData.icon=="" || retData.icon=="null" ||retData.icon==null){
					$("#iconspan").html("");
				}else{
					$("#iconspan").html("<img src='"+photos+retData.icon+"'/>"+"' width='30' height='30' >");
				}
				$("#catparent").html(retData.parentName);
				$("#catseq").val(retData.seq);
				$("#catsts").val(retData.status);
				$("#cat").val(retData.name);
				$("#catheader").val(retData.headerMenu+"");
				$("#catnavi").val(retData.navigationMenu+"");
				$("#catkey").val(retData.keyword);
				$("#catdesc").val(retData.catDesc);
				$("#returnValidPeriod").val(retData.returnValidPeriod);
				$("#cattitle").val(retData.title);
				if(retData.typeId!=null){
					$(".kui-combobox-caption").val(retData.typeName);
					$("#typeId").html("<option selected='selected' value='"+retData.typeId+"'></option>");
				}else{
					$(".kui-combobox-caption").val("");
					$("#typeId").html("<option selected='selected' value=''></option>");
				}
				$("#catDetail").show();
				$("#newDetail").hide();
			}
		});
	}
}

function editCategory(){
	$(".kui-combobox-caption").val("");
	$("#newTypeId").html("<option selected='selected' value=''></option>");
	$("#catDetail").hide();
	$("#newDetail").show();
}

function cleanCategoryCache(){
	layer.confirm("确定清除前台缓存分类树？", {icon:3, title:'提示'}, function(index){
		$.ajax({
			url:contextPath+"/admin/category/cleanCategoryCache", 
			type:'post', 
			async : true, //默认为true 异步   
			error: function(jqXHR, textStatus, errorThrown) {
			},
			success:function(result){
				layer.msg("清除成功", {icon:1,time:1500});
			}
		});
		layer.close(index);
	});
}

//加载分类用于选择
function loadCategoryDialog(){
	layer.open({
		title :"选择父类",
		id:"RoleMenu",
		type: 2, 
		content: contextPath+"/admin/category/loadCategory",
		area: ['280px', '380px']
	});
}

function loadCategoryCallBack(id,catName){
	$("#parentId").val(id);
	$("#parentName").html("<b>" + catName + "</b>");
}

function checkNewCat(){
	$("#newDetail input[name=name]").val($.trim($("#newDetail input[name=name]").val()));
	var obj_file_1 = document.getElementById("smallCatFile2"); 
	var obj_file_2 = document.getElementById("catFile2");
	var returnValidPeriod = document.getElementById("returnValidPeriod");
	var flag = false;
	if($.trim($("#newDetail input[name=name]").val())==""){
		layer.msg("请输入类目名称", {icon:2});
	}else if($("#newDetail input[name=name]").val().length>10){
		layer.msg("类目名称不能超过10个字", {icon:2});
	}else if($("#parentId").val()==""){
		layer.msg("请选择父类", {icon:2});
	}else if($.trim($("#newDetail input[name=seq]").val())==""){
		layer.msg("请输入排序", {icon:2});
	}else if(obj_file_1.value==""){
		layer.msg("类目小图标不能为空", {icon:2});
	}else if(obj_file_2.value==""){
		layer.msg("类目图片不能为空", {icon:2});
	}else if(obj_file_1.files[0].size>1*1024*1024){
		layer.msg("类目小图标不能大于1M", {icon:2});
	}else if(obj_file_2.files[0].size>1*1024*1024){
		layer.msg("类目图片不能大于1M", {icon:2});
	}else{
		flag = false;
		var parentId = $("#parentId").val();
		$.ajax({
			url:contextPath+"/admin/category/ifCreate",
			data:{"parentId":parentId},
			type:'post',
			dataType:'json',
			async:false, //默认为true 异步   
			success:function(result){
				if(result=="OK"){
					layer.alert("创建成功", {icon:1});
					flag = true;
				}else{
					layer.msg(result, {icon:2});
				}
			}
		});
	}
	return flag;
}

function checkCatEdit(){
	$("#catname").val($.trim($("#catname").val()));
	var obj_file_1 = document.getElementById("smallCatFile1"); 
	var obj_file_2 = document.getElementById("catFile1");
	var returnValidPeriod = document.getElementById("returnValidPeriod");

	var flag = false;
	if($.trim($("#catname").val())==""){
		layer.msg("请输入类目名称", {icon:2});
	}else if($("#catname").val().length>10){
		layer.msg("类目名称不能超过10个字", {icon:2});
	}else if($.trim($("#catseq").val())==""){
		layer.msg("请输入排序", {icon:2});
	}else if(obj_file_1.files[0].size>1*1024*1024){
		layer.msg("类目小图标不能大于1M", {icon:2});
	}else if(obj_file_2.files[0].size>1*1024*1024){
		layer.msg("类目图片不能大于1M", {icon:2});
	}else{
		$("input[name=catIconFile]").change(function(){
			checkImgType(this);
			checkImgSize(this,512);

		});
		flag = true; 
	}
	return flag;
}

function searchType(obj){
	var value = {"typeName": $(obj).val() };
	$.ajax({
		url:contextPath+"/admin/category/getProdType", 
		type:'post', 
		data: value,
		async : true, //默认为true 异步   
		error: function(jqXHR, textStatus, errorThrown) {
			// layer.alert(textStatus, errorThrown);
		},
		success:function(result){
			$(obj).parent().next().html(result);
		}
	});
}

//查看类目下的商品集合链接
function lookLink(themeId){
	var catid=$("#catid").val();
	layer.open({
		anim: 2,
		shadeClose: true, //开启遮罩关闭
		skin: 'demo-class',
		content: "pc端链接："+pcDomainName+"/list?categoryId="+catid+"<br><br>手机端链接："+mobileDomainName+"/m_search/list?categoryId="+catid
	});
}
