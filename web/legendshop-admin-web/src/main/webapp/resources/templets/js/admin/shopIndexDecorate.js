$(document).ready(function() {

	//页面加载请求加载内容
	sendData(1);

	//初始化select2
	if(siteName == ''){
		makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'siteName');
	}else{
		makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",siteName, 'siteName');
		$("#select2-chosen-2").html(siteName);
	}

});

//搜索
function search(){
	$("#ListForm #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	sendData(1);
}

function pager(curPageNO){
    document.getElementById("curPageNO").value=curPageNO;
    sendData(curPageNO);
}

//异步加载内容
function sendData(curPageNO) {
    var data = {
        "curPageNO": curPageNO,
        "shopId": $("#shopId").val(),
        "flag":"1",
    };
    $.ajax({
        url: contextPath+"/admin/app/shopIndexDecorate/query",
        data: data,
        type: 'post',
        async: true, //默认为true 异步
        success: function (result) {
            $("#shopIndexDecorateContent").html(result);
        }
    });
}



//上下线
function update(id,status){
  var statusText = status==1?'上线':'下线';
  layer.confirm('确定'+statusText+'该装修管理？', {icon: 3, title:'提示'},function () {
    $.ajax({
      url: contextPath+"/admin/app/shopIndexDecorate/updateStatus",
      data: {"id":id,"status":status},
      type:'post',
      async : false, //默认为true 异步
      dataType:"json",
      success:function(data){
        if(data=="OK"){
          layer.msg('操作成功',{icon: 1});
          setTimeout(window.location.href=contextPath+"/admin/app/shopIndexDecorate/query",6000);
        }else{
          layer.msg(data,{icon: 2});
          setTimeout(window.location.href=contextPath+"/admin/app/shopIndexDecorate/query",6000);
        }
      }
    });
  }, function(index){
    layer.close(index);
  });
}


//删除
function del(id){

	layer.confirm("确认删除该店铺的首页装修?", function () {
		$.ajax({
			url: contextPath+"/admin/app/shopIndexDecorate/delete/"+id,
			type:'post',
			async : false, //默认为true 异步
			dataType:"json",
			success:function(data){
				if(data=="OK"){
					layer.msg('删除成功',{icon: 1});
					setTimeout(window.location.href=contextPath+"/admin/app/shopIndexDecorate/query",6000);
				}else{
					layer.msg(data,{icon: 2});
					setTimeout(window.location.href=contextPath+"/admin/app/shopIndexDecorate/query",6000);
				}
			}
		});
	});
}

/**
 * 初始化select2
 * @param id
 * @param clearFlag
 */
function makeSelect2(url,element,text,label,value,holder, textValue){
	$("#"+element).select2({
		placeholder: holder,
		allowClear: true,
		width:'200px',
		ajax: {
		  url : url,
		  data: function (term,page) {
				return {
					 q: term,
					 pageSize: 10,    //一次性加载的数据条数
					 currPage: page, //要查询的页码
				};
			},
			type : "GET",
			dataType : "JSON",
			results: function (data, page, query) {
				if(page * 5 < data.total){//判断是否还有数据加载
					data.more = true;
				}
				return data;
			},
			cache: true,
			quietMillis: 200//延时
	  },
		formatInputTooShort: "请输入" + text,
		formatNoMatches: "没有匹配的" + text,
		formatSearching: "查询中...",
		formatResult: function(row,container) {//选中后select2显示的 内容
			return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
		},formatSelection: function(row) { //选择的时候，需要保存选中的id
			$("#" + textValue).val(row.text);
			return row.text;//选择时需要显示的列表内容
		},
	});
}



//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}
