$(document).ready(function() {
	jQuery("#form1").validate({
		rules: {
			groupby: "required",
			province: "required",
			provinceid: {
				number:true,
				required:true
			},
			shortName:"required",
		},
		messages: {
			groupby:"请输入区域",
			province:"请输入省份",
			provinceid: {
				number:"请输入正确的邮编，必须是数字",
				required:"请输入邮编"
			},
			shortName:"请输入省份简称"
		},
		submitHandler:function(form){
			$.ajax({
				url:contextPath+"/admin/system/district/queryProviceId", 
				type:'post', 
				data:{"provinceId":$("#provinceid").val(),"id":$("#id").val()},
				async : true, //默认为true 异步   
				error: function(jqXHR, textStatus, errorThrown) {
					layer.alert("系统异常",{icon:2});
				},
				success:function(result){
					var _data=eval(result);
					if(_data=="fail"){
						layer.msg("该邮编已存在！",{icon:0});
						return false;
					}else{
						form.submit();
					}
				}
			});
		}
	});

});