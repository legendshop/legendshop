$(document).ready(function(){
	$("#tabs ul li").attr("class", "ui-state-default ui-corner-top");
	highlightTableRows("item");
	$("button[name='statusImg']").click(function(event){
		$this = $(this);
		initStatus($this.attr("itemId"), $this.attr("itemName"), $this.attr("status"), contextPath+"/admin/productProperty/updatestatus/", $this, contextPath);
	});
});

function deleteById(id, name,isRuleAttributes) {
  $.ajax({
    url : contextPath+"/admin/productProperty/ifDeleteProductProperty",
    data : {"propId" : id,"isRuleAttributes":isRuleAttributes},
    type : 'post',
    async : true, //默认为true 异步
    dataType : 'json',
    error : function() {
    },
    success : function(result) {
      if (result == 0) {
        layer.confirm('确定删除 '+name+' ?', {icon:3,title:'提示'}, function(){
          layer.msg('删除成功', {icon:1, time:700}, function(){
            window.location = contextPath+"/admin/productProperty/delete/" + id ;
          });
        });
      } else {
        layer.alert("此规格正在被使用，不能删除！", {icon:2});
      }
    }
  });
}

function pager(curPageNO) {
	$.post(contextPath+"/admin/productProperty/queryContent/"+isRuleAttributes,
			{"curPageNO" : curPageNO,"propName" : $("#propName").val(),"memo" : $("#memo").val()},
			function(data) {
				$("#proppertyContent").html(data);
			});
}

function searchContent() {
	$.post(contextPath+"/admin/productProperty/queryContent/"+isRuleAttributes,
			{"curPageNO" : 1,"propName" : $("#propName").val(),"memo" : $("#memo").val()},
			function(data) {
				$("#proppertyContent").html(data);
			});
}

function importSimilarProp() {
	layer.open({
		title : '选择要导入的属性',
		type: 2,
		content: contextPath+"/admin/productProperty/loadPropPage/"+isRuleAttributes,
		area: ['935px', '700px']
	});
}

function search() {
	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	$("#form1").submit();
}
