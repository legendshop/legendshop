
			/**
 * APP 装修服务中心
 * @author Zjy
 *
 */

/**
 *
 * @param {*} navigations  //原数组
 * @param {*} pageSize  //一卡多少个： column （一行多少个）* layout  （多少行分页）  比如一行4个， 2行分页 那么一卡就8个
 */

function page(navigations, pageSize) {
	var result = [];
	// var total = navigations.length;
	// var pageCount = total % pageSize == 0 ? total / pageSize : (Math.floor(total/pageSize) + 1);

	navigations.forEach((item, index) => {
		const page = Math.floor(index / pageSize)
		if(!result[page]) {
			result[page] = []
		}
		result[page].push(item)
	});
	return result
}

// text 要传文本
// digits 默认是2位小数点
function cash(text = 0, digits = 2) {
	if(!text) {
		text = 0
	}
	if(!(text instanceof Number)) {
		text = Number(text);
	}

	text = (text.toFixed(digits)).toString()
	if(text.indexOf(".") == -1) {
		text = text + '.00'
	}
	return text.indexOf(".") != -1 ? text.split(".") : text.join('');
}

const clickoutside = {

	// 初始化指令
	bind(el, binding, vnode) {

		function documentHandler(e) {

			// 这里判断点击的元素是否是本身，是本身，则返回
			if(el.contains(e.target)) {

				return false;

			}

			// 判断指令中是否绑定了函数

			if(binding.expression) {

				// 如果绑定了函数 则调用那个函数，此处binding.value就是handleClose方法

				binding.value(e);

			}

		}

		// 给当前元素绑定个私有变量，方便在unbind中可以解除事件监听

		el.vueClickOutside = documentHandler;

		document.addEventListener('click', documentHandler);

	},

	update() {},

	unbind(el, binding) {
		// 解除事件监听
		document.removeEventListener('click', el.vueClickOutside);
		delete el.vueClickOutside;
	},

};

const serviceUlr = 'http://47.107.172.77:7300/mock/'
Vue.use(VueDragging)
Vue.filter('dateformat', function(dataStr, pattern = 'YYYY-MM-DD HH:mm:ss') {
  return moment(dataStr).format(pattern)
})

var app = new Vue({
	el: '#app',
	data: {
		Money: cash,
		imageServer: photoPath, //图片服务器
		currentPath: contextPath, //本地图片
		action: null,
		navIndex: false,
		topPosi: 67,
		rightPost: 67,
		floorData: decotateData,
		pages: page,
		show: true,
		floorTitle: '', //楼层标题
		showSelectBox: true, // 默认显示组件化
		moveIndex: null, //移动的元素
		templateJson: {},
		noFloorData: false, //默认没数据显示 左边组件
		dialogVisible: false, //弹层的显示
		curTab: 'prod', //tab栏状态,默认选中商品
		selectValue: '',
		searchInput: '', //弹层搜索input
		diyInpnt: '', //自定义url的值input
		classifyInput: '', //分类关键词input
		searchGroupInput: '', //商品分组input
		prodlistArr: '', //商品弹层
		selectgoodsIndex: null, //商品弹层的打钩索引值
		prodGroupList: '', //商品分组弹层

		popTab: {
			curTab: 'prod',
			tab: null,
			index: null
		},

		dialogPicture: false, //图片弹窗

		treeData: [],
		defaultProps: {
			children: 'childrenList',
			label: 'name'
		},

		options: [{
			value: 'descDate',
			label: '按上传时间从晚到早'
		}, {
			value: 'ascDate',
			label: '按上传时间从早到晚'
		}, {
			value: 'descSize',
			label: '按图片从大到小'
		}, {
			value: 'descName',
			label: '按图片名降序'
		}, {
			value: 'ascName',
			label: '按图片名升序'
		}],
		selectValue: '按上传时间从晚到早',

		pictureParames: {
			treeId: '', //目录id
			curPageNO: 1,
			orderBy: '', //图片目录select状态
			searchName: '', //图片搜索
		},

		imagesArr: '', //图片空间
		currentPage1: 1,

		replaceImgIndex: null, //更换图片的当前index的替换
		prodPrames: '', //商品选择要·替换的全局信息

		firstClass: '', //一级分类选择。
		secondClass: '', //二级分类选择
		threeClass: '', //三级分类选择
		categoryName: '', // 选择的名字

		categoryAction: null,

		editor: null, //富文本实例化

		pageInput: '', //弹窗 tab-页面的搜索值
		pageArr: '', //弹窗tab-页面 接收的数据

		cubeIndex: 0, //魔方切换

		marketingArr: [{
				name: '拼团',
				type: 'fighGroup'
			},
			{
				name: '秒杀',
				type: 'seckill'
			},
			{
				name: '预售',
				type: 'presell'
			},
			{
				name: '团购',
				type: 'JoinGroup'
			}
        ], //营销的配置

    indicatorIndex: 0,  //轮播图的指示器索引
    clueIndex: null
	},
	created() {
		/* this.loadIndex()*/
		if(!this.floorData.components || !this.floorData.components.length) {
			this.noFloorData = true
		}
		console.log(this.floorData)
	},
	directives: {
		clickoutside
	},
	methods: {

        showClue(index,navigations) {
          navigations[index].flag = !navigations[index].flag
          this.$forceUpdate()
        },

        // =======编辑楼层-交互功能（比如复制楼层，删除楼层，添加楼层）=======

        //自定义点击空白处
		handleClose() {
			this.action = null
			this.navIndex = false
			this.noFloorData = false
        },

        //鼠标划入
		enter(index, event) {
			this.topPosi = this.$refs.box.children[1].children[1].children[index].offsetTop + 'px'
			this.action = index
			this.navIndex = true
			this.moveIndex = index
        },

        //点击当前选中的
		clickSelect(item, index) {
			KindEditor.remove('textarea[name="content"]')
			let title = ''
			this.rightPost = this.topPosi
			this.noFloorData = true
			this.showSelectBox = false

			switch(item.type) {
				case 'richText':
					title = '富文本'
					break;
				case 'prodList':
					title = '商品列表'
					break;
				case 'slider':
					title = '轮播图'
					break;
				case 'hotspot':
					title = '万能热区'
					break;
				case 'cube':
					title = '魔方'
					break;
				case 'navigation':
					title = '图文导航'
					break;
				case 'title':
					title = '标题'
					break;
				case 'title':
					title = '标题'
					break;
				case 'blank':
					title = '辅助空白'
					break;
				case 'subline':
					title = '辅助线'
					break;
			}


			if (item.type == "navigation")  {
			  for (var i= 0; i< item.navigations.length; i++) {
          item.navigations[i].flag = false;
        }
      }


			if(item.type == "richText") {
				this.$nextTick(() => {
					this.initKindEditor()
				})
			}
			this.floorTitle = title
			this.templateJson = item
        },

        //添加楼层
		addFloor() {
			this.rightPost = this.$refs.box.children[1].children[1].children[this.action].offsetTop + 'px'
			console.log(this.rightPost)
			this.noFloorData = true
			this.showSelectBox = true
		},

        //拷贝当前楼层
		copyOf() {
			let oldJson = this.floorData.components[this.moveIndex]
			let newJson = JSON.stringify(oldJson)
			this.floorData.components.splice(this.moveIndex + 1, 0, JSON.parse(newJson))
        },

        //删除当前楼层
		deleteFloor(index) {
			this.$confirm('是否删除此模块?', '提示', {
				confirmButtonText: '确定',
				cancelButtonText: '取消',
				type: 'warning'
			}).then(() => {
				this.floorData.components.splice(this.moveIndex, 1);
				this.noFloorData = false
				this.navIndex = false
				this.$message({
					type: 'success',
					message: '删除成功!',
					duration: '1000'
				});

				if(this.floorData.components.length == 0) {
					this.noFloorData = true
					this.showSelectBox = true
				}

			}).catch(() => {
				this.$message({
					type: 'info',
					message: '已取消删除',
					duration: '500'
				});
			})
        },

        //交换数组元素
		swapItems(direction) {
			let index = this.moveIndex
			let newIndex = index

			newIndex = direction == 'top' ? index - 1 : index + 1

			let tempOption = this.floorData.components[newIndex]

			Vue.set(this.floorData.components, newIndex, this.floorData.components[index])
			Vue.set(this.floorData.components, index, tempOption)
			this.navIndex = false
			this.noFloorData = false
			// this.templateJson = {}
        },

		//向上移动
		moveUp() {
			this.swapItems('top')
        },

		//向下移动
		moveDown() {
			this.swapItems('bottom')
        },

		//标题模式切换
		titlePattern(item) {
			console.log(item)
			if(item.layout == 'plentiful') {
				item.style = 'style1'
			}
		},


        // =======编辑楼层-交互功能（比如复制楼层，删除楼层，添加楼层）end=======



        // =======编辑楼层-左边初始化（比如添加轮播图，富文本）=======

        //模块添加对应
		addMould(templateIndex) {
			let JSON = null
			if(templateIndex == 0) {
				JSON = this.bannerJson()
			} else if(templateIndex == 1) {
				JSON = this.textNavigation()
			} else if(templateIndex == 2) {
				JSON = this.titleNavigation()
			} else if(templateIndex == 3) {
				JSON = this.assistCube()
			} else if(templateIndex == 4) {
				JSON = this.goodsList()
			} else if(templateIndex == 5) {
				JSON = this.richText()
			} else if(templateIndex == 7) {
				JSON = this.assistWhite()
			} else if(templateIndex == 8) {
				JSON = this.assistSubline()
			}
			this.noFloorData = false
			this.navIndex = null
			console.log(this.floorData)
			this.floorData.components.splice(this.action + 1, 0, JSON)
        },

        //轮播图
		bannerJson() {
			return {
				"type": "slider",
				"height": 168,
				"sizeType": 'default',
				"switchAnimation": "default",
				"photos": [],
				"markStyle": "none",
				"displayStyle": "default"
			}
        },

		//图文导航
		textNavigation() {
			return {
				"type": "navigation",
				"column": 3,
				"layout": 0,
				"backgroundType": "color",
				"background": "#FFFFFF",
				"fontColor": "#000000",
				"navigations": [{
						"icon": "",
						"title": "导航",
						"action": {}
					},
					{
						"icon": "",
						"title": "导航",
						"action": {}
					},
					{
						"icon": "",
						"title": "导航",
						"action": {}
					},
					{
						"icon": "",
						"title": "导航",
						"action": {}
					},
				],
			}
        },

		// 标题
		titleNavigation() {
			return {
				"type": "title",
				"photo": "",
				"title": "主标题",
				"subhead": "",
				"moreTitle": "查看更多",
				"titleColor": "#000",
				"subheadColor": "#000",
				"backgroundType": "color",
				"background": "#79bbf2",
				"lumpColor": "#000",
				"action": {},
				"layout": "simple",
				"style": "style1",
				"photoStyle": "hide"
			}
        },

		//商品列表
		goodsList() {
			return {
				"action": {
					"category": {},
					"prodGroup": {}
				},
				"type": "prodList",
				"displayContent": {
					"spuName": true,
					"skuName": true,
					"price": true,
					"cartStyle": true,
					"badgeStyle": true,
					"url": "",
					"cart": "style1",
					"badge": "new"
				},
				"prodSource": {
					"prods": [],
					"type": "category"
				},
				"prodCount": 4,
				"backgroundType": "color",
				"background": "#f7f7f7",
				"listStyle": "style1",
				"prodStyle": "style1"
			}
        },

		//富文本
		richText() {
			return {
				"type": "richText",
				"backgroundType": "color",
				"background": "#f6f6f6",
				"pagePadding": "10px",
				"content": `<p style="text-align: center; margin-bottom: 5px;">

                &nbsp; &nbsp;<span style="font-size: 20px;">文字标题</span>

               </p>
               <p>


               </p>
               <p style="text-align: center; margin-bottom: 10px;">

                &nbsp; &nbsp; <span style="font-size: 16px; color: rgb(165, 165, 165);">初始化富文</span><span style="font-size: 20px;"><br/></span>

               </p>
               <p>


               </p>
               <p style="margin-bottom: 5px; text-align: left;">
                   &nbsp; &nbsp; &nbsp;富文本
               </p>`
			}
        },

		// 辅助空白
		assistWhite() {
			return {
				"type": "blank",
				"height": 50,
				"backgroundType": "color",
				"background": "#f7f7f7"
			}
        },

		//辅助线
		assistSubline() {
			return {
				"type": "subline",
				"lineStyle": 1,
				"height": 50,
				"leftRightPadding": 10,
				"topBottomPadding": "10px",
				"lineColor": "#000",
				"backgroundType": "color",
				"background": "#f7f7f7",
				"style": "solid"
			}
        },

		//魔方
		assistCube() {
			return {
				"type": "cube",
				"layout": 0, // 模板, 可选值 0, 1, 2
				// 图片列表
				"photos": [{
					"path": "",
					"action": {}
				}, {
					"path": "",
					"action": {}
				}],
			}
		},

        // =======编辑楼层-左边初始化（比如添加轮播图，富文本） end=======


        // =======编辑楼层-请求的接口（比如添加轮播图，富文本） =======

        //商品tab的数据请求
		getProdList(curPageNO, prodName) {
			let that = this
			$.ajax({
				url: contextPath + "/admin/decorate/action/prodList",
				data: {
					"curPageNO": curPageNO,
					"prodName": prodName
				},
				type: 'POST',
				success: function(res) {
					if(res.status == 1) {
						if(res.result.resultList) {
							that.prodlistArr = res.result
						}
						console.log(that.prodlistArr)
					}
				}
			});
        },

        //商品分组tab的数据请求
		getProdGroupList(curPageNO, prodGroupName) {
			let that = this
			$.ajax({
				url: contextPath + "/admin/decorate/action/prodGroup",
				data: {
					"curPageNO": curPageNO,
					"name": prodGroupName
				},
        dataType:'JSON',
				type: 'POST',
				success: function(res) {
					if(res.status == 1) {
						if(res.result.resultList) {
							that.prodGroupList = res.result
						}
						console.log(that.prodGroupList)
					}
				}
			});
        },

        //分类商品弹层
		getCategory() {
			let that = this
			$.ajax({
				url: contextPath + "/admin/decorate/action/category",
				// data: { "curPageNO": curPageNO, "prodName": prodName },
				type: 'POST',
				success: function(result) {

				  if(result.msg == 'OK'){

            for(let index = 0; index < result.result.length; index++) {
              const element = result.result[index];
              element.check = false
            }

            that.firstClass = result.result
          }

				}
			});
        },

        //弹窗tab-页面接口请求
		getPageList(curPageNO, pageInput) {
			let that = this
			$.ajax({
				url: contextPath + "/admin/decorate/action/page",
				data: {
					"curPageNO": curPageNO,
					"name": pageInput
				},
				type: 'POST',
				success: function(res) {
					if(res.status == 1) {
						if(res.result.resultList) {
							that.pageArr = res.result
						}
						console.log(that.pageArr)
					}
				}
			});
		},

        //图片目录
		pictureTree() {
			let that = this
			$.ajax({
				url: contextPath + "/admin/decorate/action/imagesTree",
				data: {
					"parentId": 0
				},
				type: 'POST',
				success: function(res) {
					if(res.status == 1) {
						that.treeData = res.result
						that.pictureParames.treeId = res.result[0].parentId
						that.pictureSpace(that.pictureParames)
					}
				}
			});
        },

		//图片空间
		pictureSpace(params) {
			let that = this
			$.ajax({
				url: contextPath + "/admin/decorate/action/images",
				data: params,
				type: 'POST',
				success: function(res) {
					if(res.status == 1) {
						that.imagesArr = res.result
					}
				}
			});
		},

        // =======编辑楼层-请求的接口（比如添加轮播图，富文本） end=======



        // =======编辑楼层-左边的小功能方法 =======

        //轮播图的图片添加
		addImages() {
			console.log(this.floorData)
			if(this.templateJson.type == 'slider') {
				let parmes = {
					path: '',
					action: {
						"type": "",
						"target": '',
					}
				}
				this.templateJson.photos.push(parmes)
			}
			console.log(this.templateJson.photos)
        },

        //导航个数的添加
		addNavigation(arr) {
			let navigationPrames = {
				"icon": "",
				"title": "默认导航文字",
				"action": {}
			}
			let newJson = Object.assign({}, navigationPrames)
			arr.push(newJson)
        },

        //魔方模式切换
		switchCube(index) {
			console.log(index)
			this.cubeIndex = index
			if(this.templateJson.type = "cube") {
				// let params = {}
				if(!this.templateJson.photos[index]) {
					let params = {
						path: '',
						action: {}
					}
					this.templateJson.photos[index] = params
				}

			}
		},

		//轮播图的自定义高度
		changeImgH(e) {
			if (this.templateJson.type == 'slider') {
				if (e == 'default') {
					this.templateJson.height = 168
				} else if (e == 'big') {
					this.templateJson.height = 268
				} else if (e == 'small') {
					this.templateJson.height = 118
				} else {

					if (this.templateJson.height == this.templateJson.height) {
						this.templateJson.height
					} else {
						this.templateJson.height = 118
					}
					// if (this.templateJson.height == '400px') {

					// } else {
					// 	this.templateJson.height = this.templateJson.height
					// }
				}
			}
		},

        //修改魔方的数组长度。
        changecubeType(e) {

			if(this.templateJson.type = 'cube') {
				let len = ''
				switch(e) {
					case 0:
						len = 2
						break;
					case 1:
						len = 3
						break;
					case 2:
						len = 4
						break;
					case 3:
						len = 4
						break;
					case 4:
						len = 3
						break;
					case 5:
						len = 3
						break;
				}
				this.cubeIndex = 0
				this.templateJson.photos.length = len
				this.$forceUpdate()
			}
        },

        //富文本初始化
		initKindEditor() {
			let that = this
			KindEditor.options.filterMode = false;
			this.editor = KindEditor.create('textarea[name="content"]', {
				cssPath: contextPath+'/resources/plugins/kindeditor/plugins/code/prettify.css',
				uploadJson: contextPath+'/editor/uploadJson/upload;jsessionid='+jsessionid,
				fileManagerJson: contextPath+'/editor/uploadJson/fileManager',
				allowFileManager: true,
				resizeType: 0,
				afterBlur: function() {
					this.sync();
					if(that.templateJson.type = 'richText') {
						that.templateJson.content = this.html()
					}
				},
				width: '100%',
				height: 'auto',
				minHeight: '500px',
				afterChange: function() {
					this.sync();
					if(that.templateJson.type = 'richText') {
						that.templateJson.content = this.html()
					}
				}
			});
			prettyPrint();
		},

        // =======编辑楼层-左边的小功能方法  end=======



        //商品分组列表选择
        getRadioVal(value, $event) {
			// var radioVal = $event.target.value;
			this.prodPrames = value
        },


        // =======编辑楼层- 公用方法 =======

        //弹出层
		showPopup(index, params = {curTab: "prod",tab: null}) {
      // $(":input:text[placeholder=\"请输入商品名称\"]").val("");//清除搜索条件
			let that = this
			this.replaceImgIndex = index
			// 当外面没有写curTab 或 curTab 的值不在tab里面时，如{ curTab: "prod", tab: {category：true}} }
			if(params.tab) {
				var b = false;
				var count = 0;
				var firstTab = null;
				for(key in params.tab) {

					if(count == 0) {
						firstTab = key;
					}

					if(params.curTab == key) {
						b = true;
					}

					count++;
				}

				if(!b) {
					params.curTab = firstTab;
				}
			}

			console.log('tab',params);

			this.popTab = params;

			console.log('this.popTab.curTab', this.popTab.curTab)

			this.dialogVisible = true
			if(this.popTab.curTab == 'prod') {
				this.getProdList(curPageNO = 1, this.searchInput)
			}
			if(this.popTab.curTab == 'prodGroup') {
				this.getProdGroupList(curPageNO = 1, this.searchGroupInput)
			}
			if(this.popTab.curTab == 'category') {
				this.getCategory()
			}

			if (this.popTab.curTab == 'diy') {
			  console.log('123123',1431324124)
      }
	  },

        //删除当前数据
		deleteData(prodsArr, index) {
			this.$confirm('是否执行当前操作', '提示', {
				confirmButtonText: '确定',
				cancelButtonText: '取消',
				type: 'warning'
			}).then(() => {
				prodsArr.splice(index, 1)
				this.$message({
					type: 'success',
					message: '删除成功!',
					duration: '1000'
				});

			}).catch(() => {
				this.$message({
					type: 'info',
					message: '已取消删除',
					duration: '500'
				});
			})
        },


        //更换图片
        selectImg(filePath) {
			//轮播图图片替换
			if(this.templateJson.type == 'slider') {
				this.templateJson.photos[this.replaceImgIndex].path = filePath
			}
			if(this.templateJson.type == 'navigation') {
				this.templateJson.navigations[this.replaceImgIndex].icon = filePath
			}

			if(this.templateJson.type == 'title') {
				this.templateJson.photo = filePath
			}

			if(this.templateJson.type == 'prodList') {
				this.templateJson.displayContent.url = filePath
			}

			if(this.templateJson.type == 'cube') {
				this.templateJson.photos[this.cubeIndex].path = filePath
			}

			this.dialogPicture = false
			console.log(filePath)
		},

        // =======编辑楼层- 公用方法 end=======



        // =======编辑楼层- 弹出层功能 =======

        //选择商品的打钩
		selectGoods(prodIndex, prodItem) {
			this.selectgoodsIndex = prodIndex
			this.prodPrames = prodItem
        },

		//选择商品的搜索
		searchProd() {
			this.selectgoodsIndex = null
			this.getProdList(this.currentPage1 = 1, this.searchInput)
        },

		//搜索页面的搜索
		searchPage() {
			this.getPageList(this.currentPage1 = 1, this.pageInput)
        },

    //搜索商品分组
    searchGroup() {
      this.getProdGroupList(this.currentPage1 = 1, this.searchGroupInput)
    },

		//图片的Select改变
		changeSelect(event) {
			this.pictureParames.orderBy = event
			console.log(event)
			this.pictureSpace(this.pictureParames)
        },

		//图层input
		searchPictureFile() {
			this.pictureParames.curPageNO = 1
			this.pictureSpace(this.pictureParames)
        },

        //分类tab的选择
		changClass(item, index) {

			let classType = {

			}

			if(item.grade == '1') {
				this.secondClass = ''
				this.threeClass = ''
				if(item.childrenList) {
					for(let i = 0; i < item.childrenList.length; i++) {
						const element = item.childrenList[i];
						//给2级加check字段，用来加选中状态， 默认都是不选中
						element.check = false
					}
				}
				this.secondClass = item.childrenList

			} else if(item.grade == '2') {
				this.threeClass = ''
				if(item.childrenList) {
					for(let i = 0; i < item.childrenList.length; i++) {
						const element = item.childrenList[i];
						//给3级加check字段，用来加选中状态， 默认都是不选中
						element.check = false
					}
				}
				this.threeClass = item.childrenList
			} else if(item.grade == '3') {

			}

			if(index == 1) {
				for(let index = 0; index < this.firstClass.length; index++) {
					const element = this.firstClass[index];
					if(item.name == element.name) {
						element.check = true
						this.categoryName = element.name
						classType.grade = element.grade
						classType.id = element.id
						classType.categoryName = element.name
					} else {
						element.check = false
					}
				}
			} else if(index == 2) {
				console.log(2)
				for(let index = 0; index < this.secondClass.length; index++) {
					const element = this.secondClass[index];
					if(item.name == element.name) {
						element.check = true
						// secondName = element.name
						this.categoryName = this.categoryName.split('/')[0] + "/" + element.name
						classType.grade = element.grade
						classType.id = element.id
						classType.categoryName = element.name
					} else {
						element.check = false
					}
				}
			} else if(index == 3) {

				for(let index = 0; index < this.threeClass.length; index++) {
					const element = this.threeClass[index];
					if(item.name == element.name) {
						element.check = true
						this.categoryName = this.categoryName.split('/')[0] + "/" + this.categoryName.split('/')[1] + "/" + element.name
						classType.grade = element.grade
						classType.id = element.id
						classType.categoryName = element.name
					} else {
						element.check = false
					}
				}
			}

			this.prodPrames = classType

			// console.log(classType)

			// this.categoryName += firstName +  secondName + threeName

		},

        //图片弹出层
		showImgPopup(index) {
			this.replaceImgIndex = index
			this.dialogPicture = true
			this.pictureTree()
        },

        //弹出层tab切换
		switchTab(index) {

        this.popTab.curTab = index
        this.currentPage1 = 1
        this.selectgoodsIndex = null

        if(index == 'prodGroup') {
          console.log(this.popTab.curTab)
          this.getProdGroupList(this.currentPage1 = 1, this.searchGroupInput)
        } else if(index == 'prod') {
          this.getProdList(this.currentPage1 = 1, this.searchInput)
        } else if(index == 'category') {
          this.getCategory()
        } else if(index == 'page') {
          this.getPageList(this.currentPage1 = 1, this.pageInput)
        } else if (index == 'diy') {
           if (this.templateJson.type == 'navigation') {
               let  {name,subType,type} = this.templateJson.navigations[this.replaceImgIndex].action
                this.diyInpnt = name
           }
        }
     },

        //常用功能切换
		switchUsed(functionType) {
        this.selectgoodsIndex = functionType
        let action = {}
        if(this.popTab.curTab == 'method') {
          action.type = 'method'

        } else {
          action.type = 'marketing'
        }
        action.target = this.selectgoodsIndex
        this.prodPrames = action
     },

        //点击树目录获取当前的parentId
		handleNodeClick(prames) {
			this.pictureParames.treeId = prames.parentId
			this.pictureParames.curPageNO = 1
			this.pictureSpace(this.pictureParames)
		},


        //上传图片
		upload(files) {
			let that = this
			console.log(files)
			let formData = new FormData()

			let newFormDataArr = []
			newFormDataArr.push(files.file)

			for(let index = 0; index < newFormDataArr.length; index++) {
				const element = newFormDataArr[index];
				formData.append("files[]", element)
			}

			$.ajax({
				url: contextPath + "/admin/decorate/action/uploadPicture",
				type: 'POST',
				data: formData,
				processData: false, //必写
				contentType: false, //必写
				success: function(res) {
					if(res.status == 1) {
						if(res.msg = 'OK') {
							that.$message({
								message: '添加成功!',
								type: 'success'
							});
							that.pictureSpace(that.pictureParames)
						} else {
							that.$message({
								message: res.msg,
								type: 'error'
							});
						}
					} else {
						that.$message({
							message: res.msg,
							type: 'error'
						});
					}
				},
				error: function(resp) {
					that.$message({
						message: '网络异常!',
						type: 'error'
					});
				}
			});
			// return true;

		},

		handleExceed(file) {
			this.$message.warning(`当前限制选择 5 个文件，本次选择了 ${file.length} 个文件`);
		},

    clearFiles() {
          console.log(this.$refs)
      this.$refs['upload'].clearFiles();
    },

        //分页选择
		goPage(flag, event) {
			this.selectgoodsIndex = null
			if(flag == 'prod') {
				//商品
				this.getProdList(event, this.searchInput)
			} else if(flag == 'prodGroup') {
				//商品分组
				this.getProdGroupList(event, this.searchGroupInput)
			} else if(flag == 'pictureLayer') {
				//图片
				this.pictureParames.curPageNO = event
				this.pictureSpace(this.pictureParames)
			}
			console.log(flag, event)
        },

        //弹出层确定按钮
		confirm() {

      if(this.popTab.curTab == 'diy') {
				if(this.diyInpnt == '') {
					this.$message({
						message: '请麻烦输入输入框的信息',
						type: 'error',
						duration: 500
					});
					return
				}
			} else if (this.popTab.curTab == 'prod') {
        if (this.selectgoodsIndex != null) {
          this.searchInput = ''
        }
      }





			let index = this.replaceImgIndex
			if(this.popTab.curTab == 'prod') {

			  if (this.selectgoodsIndex == null) {
          this.$message({
            message: '请先选择商品',
            type: 'warning'
          });
			    return;
        }

				if(this.templateJson.type == 'slider') {
					this.templateJson.photos[index].action = {}
					this.templateJson.photos[index].action.type = 'prodDetail'
					this.templateJson.photos[index].action.target = this.prodPrames.prodId
					this.templateJson.photos[index].action.name = this.prodPrames.spuName
				} else if(this.templateJson.type == 'navigation') {
				  console.log('12312312',this.prodPrames)

					this.templateJson.navigations[index].action = {}
					this.templateJson.navigations[index].action.type = 'prodDetail'
					this.templateJson.navigations[index].action.target = this.prodPrames.prodId
          this.templateJson.navigations[index].action.name = this.prodPrames.spuName

				} else if(this.templateJson.type == 'title') {
					this.templateJson.action = {}
					this.templateJson.action.type = 'prodDetail'
					this.templateJson.action.target = this.prodPrames.prodId
				} else if(this.templateJson.type == 'prodList') {
					// this.templateJson.action = {}
					if(this.templateJson.prodSource.type == 'diy') {
						this.templateJson.prodSource.prods.push(this.prodPrames)
					}

					if(this.templateJson.prodSource.type == 'group') {
						this.templateJson.action.category = this.prodPrames
					}
				} else if(this.templateJson.type == 'cube') {
					if(this.templateJson.photos[this.cubeIndex].action) {
						this.templateJson.photos[this.cubeIndex].action = {}
						this.templateJson.photos[this.cubeIndex].action.type = 'prodDetail'
						this.templateJson.photos[this.cubeIndex].action.target = this.prodPrames.prodId
						this.templateJson.photos[this.cubeIndex].action.name = this.prodPrames.spuName
					}
				}
			} else if(this.popTab.curTab == 'prodGroup') {
			  if (this.$refs.radio!=null && this.$refs.radio.length != 0){
          this.$refs.radio.forEach((item,index)=>{
              if (item.checked == true) {
                item.checked = false
              }
          });
        }
				if(this.templateJson.type == 'slider') {
					this.templateJson.photos[index].action = {}
					this.templateJson.photos[index].action.type = 'prodGroup'
					this.templateJson.photos[index].action.target = this.prodPrames.id
					this.templateJson.photos[index].action.name = this.prodPrames.name
				} else if(this.templateJson.type == 'navigation') {
					this.templateJson.navigations[index].action.type = 'prodGroup'
					this.templateJson.navigations[index].action.target = this.prodPrames.id
          this.templateJson.navigations[index].action.name = this.prodPrames.name

        } else if(this.templateJson.type == 'title') {
					this.templateJson.action = {}
					this.templateJson.action.type = 'prodGroup'
					this.templateJson.action.target = this.prodPrames.id
				} else if(this.templateJson.type == 'prodList') {
					this.templateJson.action.prodGroup = this.prodPrames
				} else if(this.templateJson.type == 'cube') {
					if(this.templateJson.photos[this.cubeIndex].action) {
						this.templateJson.photos[this.cubeIndex].action = {}
						this.templateJson.photos[this.cubeIndex].action.type = 'prodGroup'
						this.templateJson.photos[this.cubeIndex].action.target = this.prodPrames.id
						this.templateJson.photos[this.cubeIndex].action.name = this.prodPrames.name
					}
				}
			} else if(this.popTab.curTab == 'category') {
				if(this.templateJson.type == 'slider') {
					this.templateJson.photos[index].action = {}
					this.templateJson.photos[index].action.type = 'prodList'
					this.templateJson.photos[index].action.subType = 'category'
					this.templateJson.photos[index].action.grade = this.prodPrames.grade //grade = 1,一级 2，二级 3，三级
					this.templateJson.photos[index].action.target = this.prodPrames.id //grade = 1,一级 2，二级 3，三级
					this.templateJson.photos[index].action.name = this.prodPrames.categoryName //grade = 1,一级 2，二级 3，三级
				} else if(this.templateJson.type == 'navigation') {
					this.templateJson.navigations[index].action = {}
					this.templateJson.navigations[index].action.type = 'prodList'
					this.templateJson.navigations[index].action.subType = 'category'
					this.templateJson.navigations[index].action.grade = this.prodPrames.grade //grade = 1,一级 2，二级 3，三级
					this.templateJson.navigations[index].action.target = this.prodPrames.id
          this.templateJson.navigations[index].action.name = this.prodPrames.categoryName
        } else if(this.templateJson.type == 'title') {
					this.templateJson.action = {}
					this.templateJson.action.type = 'prodList'
					this.templateJson.action.subType = 'category'
					this.templateJson.action.grade = this.prodPrames.grade //grade = 1,一级 2，二级 3，三级
					this.templateJson.action.target = this.prodPrames.id
				} else if(this.templateJson.type == 'prodList') {
					if(this.templateJson.prodSource.type == 'category') {
						this.templateJson.action.category = this.prodPrames
						console.log(this.templateJson.action)
					}
				} else if(this.templateJson.type == 'cube') {
					if(this.templateJson.photos[this.cubeIndex].action) {
						this.templateJson.photos[this.cubeIndex].action = {}
						this.templateJson.photos[this.cubeIndex].action.type = 'category'
						this.templateJson.photos[this.cubeIndex].action.grade = this.prodPrames.grade
						this.templateJson.photos[this.cubeIndex].action.target = this.prodPrames.id
						this.templateJson.photos[this.cubeIndex].action.name = this.prodPrames.categoryName
					}
				}
			} else if(this.popTab.curTab == 'page') {
        if (this.$refs.radio!=null && this.$refs.radio.length != 0){
          this.$refs.radio.forEach((item,index)=>{
            if (item.checked == true) {
              item.checked = false
            }
          });
        }
				if(this.templateJson.type == 'slider') {
					this.templateJson.photos[index].action = {}
					this.templateJson.photos[index].action.type = 'page'
					this.templateJson.photos[index].action.target = this.prodPrames.id
					this.templateJson.photos[index].action.name = this.prodPrames.name
				} else if(this.templateJson.type == 'navigation') {
					this.templateJson.navigations[index].action = {}
					this.templateJson.navigations[index].action.type = 'page'
					this.templateJson.navigations[index].action.target = this.prodPrames.id
          this.templateJson.navigations[index].action.name = this.prodPrames.name
				} else if(this.templateJson.type == 'title') {
					this.templateJson.action = {}
					this.templateJson.action.type = 'page'
					this.templateJson.action.target = this.prodPrames.id
				} else if(this.templateJson.type == 'cube') {
					if(this.templateJson.photos[this.cubeIndex].action) {
						this.templateJson.photos[this.cubeIndex].action = {}
						this.templateJson.photos[this.cubeIndex].action.type = 'page'
						this.templateJson.photos[this.cubeIndex].action.target = this.prodPrames.id
						this.templateJson.photos[this.cubeIndex].action.name = this.prodPrames.name
					}
				}
			} else if(this.popTab.curTab == 'diy') {
				if(this.templateJson.type == 'slider') {
					console.log(index)
					this.templateJson.photos[index].action = {}
					this.templateJson.photos[index].action.type = 'url'
					this.templateJson.photos[index].action.subType = 'url'
					this.templateJson.photos[index].action.target = this.diyInpnt
					this.templateJson.photos[index].action.name = this.diyInpnt
				} else if(this.templateJson.type == 'navigation') {
					this.templateJson.navigations[index].action = {}
					this.templateJson.navigations[index].action.type = 'url'
					this.templateJson.navigations[index].action.subType = 'url'
          this.templateJson.navigations[index].action.target = this.diyInpnt
          this.templateJson.navigations[index].action.name = this.diyInpnt
        } else if(this.templateJson.type == 'title') {
					this.templateJson.action = {}
					this.templateJson.action.type = 'url'
					this.templateJson.action.subType = 'url'
					this.templateJson.action.target = this.diyInpnt
				} else if(this.templateJson.type == 'cube') {
					if(this.templateJson.photos[this.cubeIndex].action) {
						this.templateJson.photos[this.cubeIndex].action = {}
						this.templateJson.photos[this.cubeIndex].action.type = 'url'
						this.templateJson.photos[this.cubeIndex].action.subType = 'url'
						this.templateJson.photos[this.cubeIndex].action.target = this.diyInpnt
						this.templateJson.photos[this.cubeIndex].action.name = this.diyInpnt
					}
				}
			} else if(this.popTab.curTab == 'method') {
        const target = (breed) =>({
          "home": "首页",
          "class": "分类",
          "car": "购物车",
          "my": "个人中心",
          "coupon": "我的优惠券",
          "myMsg": "我的信息",
          "keep": "我的收藏",
        })[breed];
				if(this.templateJson.type == 'slider') {
					this.templateJson.photos[index].action = {}
					this.templateJson.photos[index].action.type = this.prodPrames.type
					this.templateJson.photos[index].action.target = this.prodPrames.target


					this.templateJson.photos[index].action.name = target(this.templateJson.photos[index].action.target)

				} else if(this.templateJson.type == 'navigation') {
					this.templateJson.navigations[index].action = {}
					this.templateJson.navigations[index].action.type = this.prodPrames.type
          this.templateJson.navigations[index].action.target = this.prodPrames.target
          this.templateJson.navigations[index].action.name = target(this.templateJson.navigations[index].action.target)

        } else if(this.templateJson.type == 'title') {
					this.templateJson.action = {}
					this.templateJson.action.type = this.prodPrames.type
					this.templateJson.action.target = this.prodPrames.target
				} else if(this.templateJson.type == 'cube') {
					if(this.templateJson.photos[this.cubeIndex].action) {
						this.templateJson.photos[this.cubeIndex].action = {}
						this.templateJson.photos[this.cubeIndex].action.type = this.prodPrames.type
						this.templateJson.photos[this.cubeIndex].action.target = this.prodPrames.target
						this.templateJson.photos[this.cubeIndex].action.name = target(this.templateJson.photos[this.cubeIndex].action.target)
					}
				}
			} else if(this.popTab.curTab == 'activity') {
        const target = (breed) =>({
          "fighGroup": "拼团",
          "seckill": "秒杀",
          "presell": "预售",
          "JoinGroup": "团购"
        })[breed];
				if(this.templateJson.type == 'slider') {
					this.templateJson.photos[index].action = {}
					this.templateJson.photos[index].action.type = this.prodPrames.type
					this.templateJson.photos[index].action.target = this.prodPrames.target

					this.templateJson.photos[index].action.name = target(this.templateJson.photos[index].action.target)

				} else if(this.templateJson.type == 'navigation') {
					this.templateJson.navigations[index].action = {}
					this.templateJson.navigations[index].action.type = this.prodPrames.type
					this.templateJson.navigations[index].action.target = this.prodPrames.target
          this.templateJson.navigations[index].action.name = target(this.templateJson.navigations[index].action.target)

        } else if(this.templateJson.type == 'title') {
					this.templateJson.action = {}
					this.templateJson.action.type = this.prodPrames.type
					this.templateJson.action.target = this.prodPrames.target
				} else if(this.templateJson.type == 'cube') {
					if(this.templateJson.photos[this.cubeIndex].action) {
						this.templateJson.photos[this.cubeIndex].action = {}
						this.templateJson.photos[this.cubeIndex].action.type = this.prodPrames.type
						this.templateJson.photos[this.cubeIndex].action.target = this.prodPrames.target
            this.templateJson.photos[this.cubeIndex].action.name = target(this.templateJson.photos[this.cubeIndex].action.target)

            // this.templateJson.photos[this.cubeIndex].action.name = name
					}
				}
			}

			console.log(this.templateJson)

			this.diyInpnt = ''
			this.dialogVisible = false
			this.popTab.curTab = 'prod'
			this.selectgoodsIndex = null
			this.firstClass = ''
			this.secondClass = ''
			this.threeClass = ''
      this.searchGroupInput = ''
      this.pageInput = ''
      this.currentPage1 = 1
		},

		//弹出层取消按钮
		cancel() {
			if(this.popTab.curTab == 'page') {
        if (this.$refs.radio!=null && this.$refs.radio.length != 0){
          this.$refs.radio.forEach((item,index)=>{
            if (item.checked == true) {
              item.checked = false
            }
          });
        }
			} else if(this.popTab.curTab == 'prodGroup') {
        if (this.$refs.radio!=null && this.$refs.radio.length != 0){
          this.$refs.radio.forEach((item,index)=>{
            if (item.checked == true) {
              item.checked = false
            }
          });
        }

			}
			this.searchInput = ''
			this.diyInpnt = ''
			this.currentPage1 = 1
			this.dialogVisible = false
			this.dialogPicture = false
			this.popTab.curTab = 'prod'
			this.selectgoodsIndex = null
			this.firstClass = ''
			this.secondClass = ''
			this.threeClass = ''
      this.searchGroupInput = ''
      this.pageInput = ''
		},

        // =======编辑楼层- 弹出层功能 end=======




        // =======编辑楼层- 发布和保存 =======

        // 保存页面装修
		savePage() {
			var that = this
			if(!this.floorData || !this.floorData.components.length) {
				that.$message({
					message: '请添加一个楼层',
					type: 'warning'
				});
				return;
			}
			// 装修数据
			var dataStr = JSON.stringify(this.floorData);

			$.ajax({
				url: contextPath + "/admin/app/pageManagement/savePage",
				type: "post",
				data: {
					"decotateData": dataStr,
					"pageId": pageId,
					"category": category
				},
				dataType: "json",
				success: function(result) {
					if(result == 'OK') {

						that.$message({
							message: '保存成功',
							type: 'success',
              duration: 1000
						});

						// if (!pageId) {
							  setTimeout(() => {
						 	   window.location.href = contextPath + "/admin/app/pageManagement/query?category=" + category;
						 }, 2000)
						// }
					} else {

						that.$message({
							message: '保存失败，请稍后再试!',
							type: 'error',
              duration: 1000
						});
					}
				},
				error: function(resp) {
					that.$message({
						message: '网络异常!',
						type: 'error'
					});
				}
			});
		},

        // 发布页面装修
		releasePage() {
			var that = this
			if(!this.floorData || !this.floorData.components.length) {
				that.$message({
					message: '请添加一个楼层',
					type: 'warning'
				});
				return;
			}
			// 装修数据
			var dataStr = JSON.stringify(this.floorData);

			$.ajax({
				url: contextPath + "/admin/app/pageManagement/releasePage",
				type: "post",
				data: {
					"decotateData": dataStr,
					"pageId": pageId,
					"category": category
				},
				dataType: "json",
				success: function(result) {
					if(result == 'OK') {

						that.$message({
							message: '发布成功',
							type: 'success'
						});
						// if (!pageId) {
              setTimeout(() => {
               window.location.href = contextPath + "/admin/app/pageManagement/query?category=" + category;
           }, 2000)
						// }

					} else {

						that.$message({
							message: '发布失败，请稍后再试!',
							type: 'error'
						});
					}
				},
				error: function(resp) {
					that.$message({
						message: '网络异常!',
						type: 'error'
					});
				}
			});
        },


        switchBanner(e) {
            this.indicatorIndex = e
        },
        // =======编辑楼层- 发布和保存 end=======

	},
	components: {

	},
	wacth: {

	}
})
