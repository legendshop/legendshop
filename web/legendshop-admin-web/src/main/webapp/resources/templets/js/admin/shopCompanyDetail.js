$("select.combox").initSelect();
//上传图片检查
$("input[type=file]").change(function() {
	checkImgType(this);
	checkImgSize(this, 1024);
});
/**电话**/
function isTel(str) {
	var reg = /^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/;
	return reg.test(str);
}

jQuery.validator.addMethod("isTel", function(value, element) {
	if (value != null && value != '') {
		return isTel(value);
	} else {
		return true
	}
}, '固定电话格式为:010-XXXXXXXX');
$(document).ready(function(e) {
	$("#form1").validate({
		ignore : "",
		rules : {
			companyName : {
				required : true
			},
			licenseNumber : {
				required : true
				// number : true
			},
			licenseProvinceid : {
				required : true
			},
			licenseCityid : {
				required : true
			},
			licenseAreaid : {
				required : true
			},
			licenseAddr : {
				required : true
			},
			licenseEstablishDate : {
				required : true
			},
			licenseStartDate : {
				required : true
			},
			licenseEndDate : {
				required : true
			},
			licenseRegCapital : {
				required : true,
				digits : true
			},
			licenseBusinessScope : {
				required : true
			},
			companyProvinceid : {
				required : true
			},
			companyCityid : {
				required : true
			},
			companyAreaid : {
				required : true
			},
			companyAddress : {
				required : true
			},
			companyTelephone : {
				required : true,
				isTel : true
			},
			companyNum : {
				required : true
			},
			companyIndustry : {
				required : true
			},
			companyProperty : {
				required : true
			},
			taxpayerId : {
				required : true
			},
			taxRegistrationNum : {
				required : true
			},
			bankAccountName : {
				required : true
			},
			bankCard : {
				required : true
			},
			bankName : {
				required : true
			},
			bankLineNum : {
				required : true
			},
			bankProvinceid : {
				required : true
			},
			bankCityid : {
				required : true
			},
			bankAreaid : {
				required : true
			}
		},
		messages : {
			companyName : {
				required : "公司名不能为空"
			},
			licenseNumber : {
				required : "请填写营业执照号码"
				// number : "格式不正确(数字)"
			},
			licenseProvinceid : {
				required : "营业执照所在地区不能为空"
			},
			licenseCityid : {
				required : "营业执照所在地区不能为空"
			},
			licenseAreaid : {
				required : "营业执照所在地区不能为空"
			},
			licenseAddr : {
				required : "营业执照详情地址不能为空"
			},
			licenseEstablishDate : {
				required : "成立日期不能为空"
			},
			licenseStartDate : {
				required : "开始时间不能为空"
			},
			licenseEndDate : {
				required : "结束时间不能为空"
			},
			licenseRegCapital : {
				required : "注册资本不能为空",
				digits : "格式不正确(数值)"
			},
			licenseBusinessScope : {
				required : "经营范围不能为空"
			},
			companyProvinceid : {
				required : "公司所在地区不能为空"
			},
			companyCityid : {
				required : "公司所在地区不能为空"
			},
			companyAreaid : {
				required : "公司所在地区不能为空"
			},
			companyAddress : {
				required : "公司地址不能为空"
			},
			companyTelephone : {
				required : "公司电话不能为空"
			},
			companyNum : {
				required : "公司人数不能为空"
			},
			companyIndustry : {
				required : "公司行业不能为空"
			},
			companyProperty : {
				required : "公司性质不能为空"
			},
			taxpayerId : {
				required : "纳税人识别号不能为空"
			},
			taxRegistrationNum : {
				required : "税务登记证号不能为空"
			},
			bankAccountName : {
				required : "银行开户名不能为空"
			},
			bankCard : {
				required : "银行开户名不能为空"
			},
			bankName : {
				required : "开户银行支行名称不能为空"
			},
			bankLineNum : {
				required : "开户行支行联行号不能为空"
			},
			bankProvinceid : {
				required : "开户银行支行所在地不能为空"
			},
			bankCityid : {
				required : "开户银行支行所在地不能为空"
			},
			bankAreaid : {
				required : "开户银行支行所在地不能为空"
			}
		}
	});



	laydate.render({
		elem: '#licenseEstablishDate',
		calendar: true,
		theme: 'grid',
		trigger: 'click'
	});

	laydate.render({
		elem: '#licenseStartDate',
		calendar: true,
		theme: 'grid',
		trigger: 'click'
	});

	laydate.render({
		elem: '#licenseEndDate',
		calendar: true,
		theme: 'grid',
		trigger: 'click'
	});
});
