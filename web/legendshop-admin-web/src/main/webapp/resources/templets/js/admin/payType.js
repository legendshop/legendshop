$(document).ready(function() {
	var value=eval(${bean.paymentConfig});
	if(value!=null&&value!=undefined&&value!=""){
		var json = value;
		$("#col2 tbody").remove();
		var str="";
		for(var i=0;i<json.length;i++){
			if(i%2==0){
				str+="<tr class='even'>";
			}else{
				str+="<tr class='old'>";
			}
			if(i==0){
				str+="<td style='width:265px;border-top:0px;'>";
			}else{
				str+="<td style='width:265px;'>";
			}
			str+="<div align='center'>"+json[i].key+":</div>";
			str+="</td>";
			if(i==0){
				str+="<td style='border-top:0px;'>";
			}else{
				str+="<td >";
			}
			if(json[i].type!="" && json[i].type!=null && json[i].type!=undefined && json[i].type=="file"){
				str+="<input type='file' id='"+json[i].key+"' name='"+json[i].key+"' ></input>";
			}
			else{
				str+="<textarea rows='3' cols='80' id='"+json[i].key+"' name='"+json[i].key+"'>"+json[i].value+"</textarea>";
			}
			str+="<span style='margin-left:10px;'>"+json[i].title+"</span>";
			str+="</td>";
			str+="</tr>"; 
		}
		$("#col2 ").append(str);
		$("#col2").show();
	}


	jQuery("#form1").validate({
		submitHandler : function(form) {
			var size=$("#col2").find("textarea").length;
			if(size>0){
				var validateparam=false;
				var array = new Array();
				$("#col2").find("textarea").each(function () {
					var value=$(this).val();
					if(value==null||value==""||value==undefined){
						var title=$.trim($(this).next().html());
						alert("请输入"+title+"！");
						$(this).focus();
						validateparam=false;
						return false;
					}else{
						var obj = new Object();
						var name=$(this).attr("name");
						var title=$.trim($(this).next().html());
						obj.key=name;
						obj.value=value;
						obj.title=title;
						array.push(obj);
						validateparam=true; 
					}
				});
				$("#payParams").val(JSON.stringify(array));
				if(!validateparam){
					return;
				}
			}
			form.submit();
		}
	});
	$("#col2 tr").each(function(i){
		if(i>0){
			if(i%2 == 0){
				$(this).addClass('even');
			}else{    
				$(this).addClass('odd'); 
			}   
		}
	});   
	$("#col2 th").addClass('sortable'); 
});