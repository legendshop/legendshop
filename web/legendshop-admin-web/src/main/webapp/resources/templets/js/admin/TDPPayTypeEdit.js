$("#payForm").validate({
	errorElement : "div", //可以用其他标签，记住把样式也对应修改
	errorClass : "ant-form-explain",
	rules : {
		key : {
			required : true
		},
		bargainorId : {
			required : true
		}
	},
	messages : {
		key : {
			required : '请输入商户号密钥'
		},
		bargainorId : {
			required : '请输入商户号'
		}
	},
	submitHandler : function(from) {
		var data = $("#payForm").serialize();
		var url = contextPath+"/admin/paytype/payEditTDP";

		$.ajax({
			url : url,
			data : data,
			type : "post",
			async : false,
			dataType : "json",
			error : function() {
				layer.alert("网络延迟,请稍后再试",{icon:2});
			},
			success : function(result) {
				if (result == "OK") {
					layer.alert("保存成功",{icon:1},function(){
						parent.layer.closeAll();
					});
				} else {
					layer.alert(result,{icon:2});
				}
			},
		});
	}
});

$(".ant-radio-isEnable").click(function() {
	$(".ant-radio-isEnable").parent().removeClass("ant-radio-checked");
	$(this).parent().addClass("ant-radio-checked");
	$("#payForm input[name='isEnable']").val($(this).val());
});