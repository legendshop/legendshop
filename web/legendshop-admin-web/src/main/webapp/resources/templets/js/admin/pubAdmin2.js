var ue =  UE.getEditor('msg');
function GetEditor() {
	if (editor==null||editor==undefined) {
		editor = UE.getEditor('msg');
	}
	return editor;
}

$.validator.setDefaults({});
jQuery.validator.addMethod("compareDate", function (value, element, param) {
	var startDate = jQuery(param).val();
	startDate= startDate.replace(/-/g, '/');
	value = $("#endDate").val().replace(/-/g, '/');

	if (startDate == null || startDate == '' || value == null || value == '') {//只要有一个为空就不比较
		return true;
	}
	var date1 = new Date(startDate);
	var date2 = new Date(value);
	return date1.getTime() < date2.getTime();

}, '开始时间不能大于结束时间');

$(document).ready(function () {
	jQuery("#form1").validate({
		ignore: "",
		rules: {
			title: {
				required: true,
				minlength: 2
			},
			msg: {
				required: true
			},
			endDate: {
				compareDate: "#startDate"
			}
		},
		messages: {
			title: {
				required: "请输入标题",
				minlength: "请认真输入标题"
			},
			msg: {
				required: "请输入内容"
			},
			endDate: {
				compareDate: "结束日期必须大于开始日期!"
			}
		}
	});

	highlightTableRows("col1");
	//斑马条纹
	$("#col1 tr:nth-child(even)").addClass("even");
	laydate.render({
		elem: '#startDate',
		calendar: true,
		theme: 'grid',
		trigger: 'click'
	});

	laydate.render({
		elem: '#endDate',
		calendar: true,
		theme: 'grid',
		trigger: 'click'
	});

});

function checkFinish() {
	var result = checkSensitiveData();
	if (result != null && result != '') {
		layer.alert("您的输入含有敏感字： '"+result+"' 请更正后再提交", {icon:2});
		return false;
	} else {
		return true;
	}
}

function checkSensitiveData() {
	var content = $("#msg").val();
	var url = contextPath+"/admin/sensitiveWord/filter";
	var result;
	$.ajax({
		"url": url,
		data: {"src": content},
		type: 'post',
		dataType: 'json',
		async: false, //默认为true 异步   
		success: function (retData) {
			result = retData;
		}
	});
	return result;
}