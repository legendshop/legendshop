function checkFinish() {
	var result = checkSensitiveData();
	if (result != null && result != '') {
		layer.confirm("您的输入含有敏感字： '" + result + "' 请更正后再提交", {icon:2});
		return false;
	} else {
		return true;
	}
}

jQuery.validator.addMethod("checkSensitiveData1", function (value, element) {
	return checkSensitiveData(value);
}, '');

$(document).ready(function () {
	jQuery("#form1").validate({
		rules: {
			id: "required",
			name: {
				required: true,
				minlength: 3,
				maxlength: 50
			},
			content: {
				maxlength:60000,
				required: true
			},
      writerName:{
        required: true
      }
		},
		messages: {
			name: {
				required: "请输入文章标题",
				minlength: "请认真填写，文章标题不能少于3个字符",
				maxlength: "超出长度限制50个字",
			},
			content: {
				maxlength:"超出长度限制",
				required: "存在黑名单的关键字"
			},
      writerName:{
        required: "请输入文章作者"
      }
		}
	});
	$("#form1").submit(function () {
    if (($("#picFile").val()==null||$("#picFile").val()=="")&&($("#picFilePath").val()==null||$("#picFilePath").val()=="")){
      layer.msg("请先选择文章图片！！！",{icon:0})
      return false;
    }
  })
	//斑马条纹
	$("#col1 tr:nth-child(even)").addClass("even");
	KindEditor.options.filterMode = false;
	KindEditor.create('textarea[name="content"]', {
		cssPath: contextPath+'/resources/plugins/kindeditor/plugins/code/prettify.css',
		uploadJson : '/editor/uploadJson/upload;jsessionid='+UUID,
		fileManagerJson: contextPath+'/editor/uploadJson/fileManager',
		allowFileManager: true,
		imgMaxSize : 3*1024*1024,
		afterBlur: function () {
			this.sync();
		},
		width: '100%',
		height: '400px',
		afterCreate: function () {
			var self = this;
			KindEditor.ctrl(document, 13, function () {
				self.sync();
				document.forms['example'].submit();
			});
			KindEditor.ctrl(self.edit.doc, 13, function () {
				self.sync();
				document.forms['example'].submit();
			});
		}
	});
	prettyPrint();
});

function checkSensitiveData() {
	var content = $("#content").val();
	var url = contextPath+"/admin/sensitiveWord/filter";
	$.ajax({
		url: url,
		data: {"src": content},
		type: 'post',
		dataType: 'json',
		async: false, //默认为true 异步
		success: function (retData) {
			result = retData;
		}
	});
	return result;
}

jQuery.validator.addMethod("isNumber", function(value, element) {
	return this.optional(element) || /^[-\+]?\d+$/.test(value) || /^[-\+]?\d+(\d+)?$/.test(value);
}, "必须整数");
