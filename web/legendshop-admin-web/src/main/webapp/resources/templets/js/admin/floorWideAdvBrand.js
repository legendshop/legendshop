$(document).ready(function() {

	$("div[class='wide_brand_c_m_left']").hover(function () {  
		$("#advEdit").show();
	},function () {  
		$("#advEdit").hide();  
	});

	$("div[class='wide_brand_c_m_right']").hover(function () {  
		$("#brandEdit").show();
	},function () {  
		$("#brandEdit").hide();  
	});

	var  url = contextPath+"/admin/floor/brandContent/"+_flId+"?layout=wide_adv_brand&limit=40";
	sendContent(url);

});

function onclickAdv(floorId,fiId){
	if(fiId==null || fiId=="" || fiId==undefined){
		layer.open({
			title: '编辑广告',
			id: 'Floor',
			type: 2,
			content: contextPath+'/admin/floorItem/advFloorOverlay/'+ floorId+"?layout=wide_adv_brand",
			area: ['650px', '400px'],
			cancle: function(index, layero){
				parent.location.reload();
			}
		});
	}else{
		layer.open({
			title: '编辑广告',
			id: 'Floor',
			type: 2,
			content: contextPath+'/admin/floorItem/advFloorOverlay/'+ floorId +'/'+ fiId+"?layout=wide_adv_brand",
			area: ['650px', '400px'],
			cancle: function(index, layero){
				parent.location.reload();
			}
		});
	}
}

function onclickBrand(id){
	layer.open({
		title: '编辑品牌',
		type: 2,
		content: contextPath+'/admin/floorItem/brandFloorOverlay/'+ id+"?layout=wide_adv_brand&limit=40",
		area: ['700px', '600px'],
		cancle: function(index, layero){
			parent.location.reload();
		}
	});
}

function sendContent(url){
	$.ajax({
		"url":url, 
		type:'post', 
		async : true, //默认为true 异步   
		error: function(jqXHR, textStatus, errorThrown) {
		},
		success:function(result){
			$("div[class='recommend-brand']").html(result);
		}
	});
}

//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}