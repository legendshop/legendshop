function deleteById(id) {
	layer.confirm("确定删除 ?", {icon:3, title:'提示'}, function (index) {
		$.post(contextPath+"/admin/brand/delete/" + id, function (data) {
			if ("fail" == data) {
				layer.alert("删除失败,该品牌已上线或者其下有商品", {icon:2});
			} else if ("success" == data) {
				layer.alert("删除成功", {icon:1}, function(){
					window.location.reload(); 
				});
			}
		});
		layer.close(index);
	});
}

$(document).ready(function () {
	$("button[name='statusImg']").click(function (event) {
		$this = $(this);
		initStatus($this.attr("itemId"), $this.attr("itemName"), $this.attr("status"), contextPath+"/admin/brand/updatestatus/", $this, "${contextPath}");
	}
	);
	highlightTableRows("item");
	
	
	//导出
	$("#exportBrand").on("click", function(){
		var status = $("#status").val();
		var shopId = $("#shopId").val();
		var commend = $("#commend option:selected").val();
		var brandName = $("#brandName").val();
		var data = [];
	    data.push(status);
	    data.push(shopId);
	    data.push(commend);
	    data.push(brandName);
	    
	    var jsondata = encodeURI(JSON.stringify(data));
	    window.location.href = contextPath + "/admin/brand/export?data=" + jsondata;
	})

	if(shopName == ''){
        makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'shopName');
    }else{
    	makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",shopName, 'shopName');
		$("#select2-chosen-1").html(shopName);
    } 
	
	sendData(1);
	
});

function sendData(curPageNO) {
    var data = {
        "shopId": $("#shopId").val(),
        "curPageNO": curPageNO,
        "status": $("#status").val(),
        "brandName": $("#brandName").val(),
        "commend": $("#commend option:selected").val(),
    };
    $.ajax({
        url: contextPath+"/admin/brand/queryContent",
        data: data,
        type: 'post',
        async: true, //默认为true 异步   
        success: function (result) {
            $("#brandContentList").html(result);
        }
    });
}

function makeSelect2(url,element,text,label,value,holder, textValue){
	$("#"+element).select2({
        placeholder: holder,
        allowClear: true,
        width:'200px',
        ajax: {  
    	  url : url,
    	  data: function (term,page) {
                return {
                	 q: term,
                	 pageSize: 10,    //一次性加载的数据条数
                 	 currPage: page, //要查询的页码
                };
            },
			type : "GET",
			dataType : "JSON",
            results: function (data, page, query) {
            	if(page * 5 < data.total){//判断是否还有数据加载
            		data.more = true;
            	}
                return data;
            },
            cache: true,
            quietMillis: 200//延时
      }, 
        formatInputTooShort: "请输入" + text,
        formatNoMatches: "没有匹配的" + text,
        formatSearching: "查询中...",
        formatResult: function(row,container) {//选中后select2显示的 内容
            return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
        },formatSelection: function(row) { //选择的时候，需要保存选中的id
        	$("#" + textValue).val(row.text);
            return row.text;//选择时需要显示的列表内容
        }, 
    });
}

function search() {
	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	sendData(1);
}

function getCheckedIds(){
	var checkedBrand=$("input[type=checkbox][name=brandId]:checked");
	var brandIds=new Array();
	checkedBrand.each(function(index, domEle){
		var bandId=$(domEle).val();
		brandIds.push(bandId);
	});
	return brandIds.toString();
}

$("#batch-del").on("click",function(){
	var brandIdsStr=getCheckedIds();
	jQuery.ajax({
		url: contextPath+"/admin/brand/batchDel/"+brandIdsStr,
		type: 'put',
		async: false, //默认为true 异步  
		dataType: 'json',
		success: function (data) {
			if (data.successValue == "Y") {
				setInterval(function(){
					location.href=contextPath+"/admin/brand/query";
				},2000);
			}else{
				layer.msg(data.msg, {icon:2});
			}
		}
	});
});

$("#checkbox").on("click",function(){
	if(!$(this).hasClass("checked")){
		$("input[type=checkbox][name=brandId][status=1]").prop("checked","true");
		$(this).addClass("checked");
	}else{
		$("input[type=checkbox][name=brandId]").removeAttr("checked");
		$(this).removeClass("checked");
	}
});

function pager(curPageNO) {
    $("#curPageNO").val(curPageNO);
    sendData(curPageNO);
}