$(document).ready(function() {
	
	jQuery("#form1").validate({
		rules: {
			name: "required",
			sequence:{
				number : true,
				required: true
			}
		},
		messages: {
			name: {
				required: "请输入类型名称"
			},
			sequence:{
				number : "请输入数字",
				required: "请输入顺序"
			}
		}
	});

	//斑马条纹
	$("#col1 tr:nth-child(even)").addClass("even");
	$("#col2 tr:nth-child(even)").addClass("even");
	$("#col3 tr:nth-child(even)").addClass("even");
});

var brandIds = new Array;
function aaa(){
	alert("hahaha");
}

function selectProperty(id){
	layer.open({
		title: '选择规格',
		id: 'selectProperty',
		type: 2,
		content: contextPath+'/admin/prodType/propertyList/'+id,
		area: ['800px', '550px']
	});
}

function selectBrand(id){
	layer.open({
		title: '选择品牌',
		id: 'brandList',
		type: 2,
		content: contextPath+'/admin/prodType/brandList/'+id,
		area: ['800px', '570px'],
		end: function () {
			brandIds = []; //关闭则清空数组
		}
	});
}

function selectParameterProperty(id){
	layer.open({
		title: '选择参数属性',
		id: 'parameterProperty',
		type: 2,
		content: contextPath+'/admin/prodType/parameterProperty/'+id,
		area: ['800px', '550px']
	});
}

function deleteProdProp(propId,prodTypeId,obj) {
	$.ajax({
		url : contextPath + "/admin/prodType/ifDeleteProductProperty",
		data : {"propId" : propId, "prodTypeId" : prodTypeId},
		type : 'post',
		async : true, //默认为true 异步   
		dataType : 'json',
		error : function() {
		},
		success : function(result) {
			if (result == true) {
				layer.confirm('确定删除 ?', {icon:3,title:'提示'}, function(){
					$.ajax({
						url:contextPath+"/admin/prodType/deleteProdProp", 
						data: {"propId":propId,"prodTypeId":prodTypeId},
						type:'post', 
						dataType : 'json', 
						async : true, //默认为true 异步   
						error: function(jqXHR, textStatus, errorThrown) {
							layer.alert(textStatus, errorThrown);
						},
						success:function(result){
							if(result == 'OK'){
								layer.msg("删除成功！", {icon:1, time:700}, function(){
									window.location = contextPath + "/admin/prodType/load/" + prodTypeId;
								});
							}else{
								layer.alert("删除失败！", {icon:2});
							}
						}
					});  
				});
			} else {
				layer.alert("此规格正在被使用，不能删除！", {icon:2});
			}
		}
	});
}

function deleteBrand(brandId,prodTypeId,obj) {
	layer.confirm("确定删除 ?", {icon:3, title:'提示'}, function () {
		$.ajax({
			url:contextPath+"/admin/prodType/deleteBrand", 
			data: {"brandId":brandId,"prodTypeId":prodTypeId},
			type:'post', 
			dataType : 'json', 
			async : true, //默认为true 异步   
			error: function(jqXHR, textStatus, errorThrown) {
				layer.alert(textStatus, errorThrown);
			},
			success:function(result){
				if(result == 'OK'){
					layer.msg("删除成功！", {icon:1, time:700}, function(){
						window.location = contextPath+"/admin/prodType/load/" + prodTypeId;
					});
				}else{
					layer.alert("删除失败！", {icon:2});
				}
			}
		});  
	});
}

function deleteParameterProp(propId,prodTypeId,obj) {
	layer.confirm("确定删除 ?", {icon:3, title:'提示'}, function () {
		$.ajax({
			url:contextPath+"/admin/prodType/deleteParameterProp", 
			data: {"propId":propId,"prodTypeId":prodTypeId},
			type:'post', 
			dataType : 'json', 
			async : true, //默认为true 异步   
			error: function(jqXHR, textStatus, errorThrown) {
				layer.alert(textStatus, errorThrown);
			},
			success:function(result){
				if(result == 'OK'){
					layer.msg("删除成功！", {icon:1, time:700}, function(){
						window.location = contextPath+"/admin/prodType/load/" + prodTypeId ;
					});
				}else{
					layer.alert("删除失败！", {icon:2});
				}
			}
		});  
	});
}

function editParameterProp(propId,prodTypeId,obj) {
	layer.open({
		title: '编辑参数属性',
		id: 'editProp',
		type: 2,
		content: contextPath+'/admin/prodType/editParameterProp/'+propId+'/'+prodTypeId,
		area: ['610px', '350px']

	});
}

function editProdProp(propId,prodTypeId,obj) {
	var seq = $(obj).attr("seq");
	layer.open({
		title: '编辑顺序',
		type: 1,
		content: "<div style='text-align: center;margin-top:26px;line-height:26px;'>顺序：<input id='prodPropSeq' maxLength='5' onkeyup=\"if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\\D/g,'')}\" "+ 
		"onafterpaste=\"if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'0')}else{this.value=this.value.replace(/\\D/g,'')}\" type='text' value='"+seq+"'/></div>",
		area: ['310px', '160px'],
		btn: ['确定'],
		yes: function(index, layero){
			var propSeq = $("#prodPropSeq").val();
			$.ajax({
				url:contextPath+"/admin/prodType/saveProdTypeProp", 
				data: {"propId":propId,"typeId":prodTypeId,"propSeq":propSeq},
				type:'post', 
				dataType : 'json', 
				async : true, //默认为true 异步   
				error: function(jqXHR, textStatus, errorThrown) {
					layer.alert("网络异常,请稍后重试！",{icon:2});
				},
				success:function(result){
					parent.location.reload();
				}
			}); 
		}
	})
	$("#prodPropSeq").val("").focus().val(seq); 
}