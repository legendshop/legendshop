var urls = {
		edit: contextPath + "/admin/appStartAdv/edit",
		online: contextPath + "/admin/appStartAdv/online",
		offline: contextPath + "/admin/appStartAdv/offline",
		del: contextPath + "/admin/appStartAdv/delete",
};

$(function(){
	//点击创建
	$("#addBtn").on("click", function(e){
		window.location.href = urls.edit;
		return false;
	});

	//点击搜索
	$("#searchBtn").on("click", function(e){
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#form1")[0].submit();
		return false;
	});

	//点击上线
	$("button.online").on("click", function(e){
		var url = urls.online + "/" + $(this).attr("itemId");
		layer.confirm("您确定要上线该广告吗?", {icon:3, title:'提示'}, function(){
			$.ajax({
				url: url,
				type: 'post',
				dataType:'text',
				async : false, //默认为true 异步
				error: function(jqXHR, textStatus, errorThrown) {
				},
				success:function(result){
					if(result.substr(1,2)==="OK"){
						layer.msg('广告上线成功', {icon:1, time:1000}, function(){
							parent.location.reload();
						});
					}else {
						layer.msg(result, {icon:2});
					}
				}
			});
		});
		return false;
	});

	//点击下线
	$("button.offline").on("click", function(e){
		var url = urls.offline + "/" + $(this).attr("itemId");
		layer.confirm("您确定要下线该广告吗?", {icon:3, title:'提示'}, function(){
			$.ajax({
				url: url,
				type: 'post',
				dataType:'text',
				async : false, //默认为true 异步
				error: function(jqXHR, textStatus, errorThrown) {
				},
				success:function(result){
					if(result.substr(1,2)==="OK"){
						layer.msg('广告下线成功', {icon:1, time:1000}, function(){
							parent.location.reload();
						});
					}else {
						layer.msg(result, {icon:2});
					}
				}
			});
		});
		return false;
	});

	//点击删除
	$("button.delete").on("click", function(e){
	  alert("121111");
		var id = $(this).attr("itemId");
		layer.confirm("您确定要删除该广告?", {icon:3, title:'提示'}, function(){
			$.ajax({
				url: urls.del + "/" + id,
				type: 'post',
				dataType:'text',
				async : false, //默认为true 异步
				error: function(jqXHR, textStatus, errorThrown) {
				},
				success:function(result){
					if(result=="OK"){
						layer.msg('广告删除成功', {icon:1, time:1000}, function(){
							parent.location.reload();
						});
					}else {
						layer.msg(result, {icon:2});
					}
				}
			});
		});
		return false;
	});

	//点击编辑
	$("button.edit").on("click", function(e){
		var url = urls.edit + "/" + $(this).attr("itemId");
		$.pageJumps.go(url);
		return false;
	});
});
