$(document).ready(function() {
	jQuery("#form1").validate({
		rules : {
			title : {
				required : true,
				maxlength : 30
			},
			seq : {
				required : true,
				maxlength : 3
			},
			detail : {
				required : true,
				maxlength : 255
			}
		},
		messages : {
			banner : {
				required : "头条名称不能为空"
			},
			seq : {
				required : "头条顺序不能为空"
			},
			detail : {
				required : "头条描述不能为空"
			}
		}
	});

	//斑马条纹
	$("#col1 tr:nth-child(even)").addClass("even");
	
	$("input[id=prodImg]").change(function(){
		checkImgType(this);
		checkImgSize(this,1024);    			
	});
});

function check() {
	var name = $("#prodName").val();
	var pic = $("#prodImg").attr("value");
	var detail = $("#msg").val();
	var seq = $("#prodCash").val();
	var reg = new RegExp("^[0-9]*$");
	if($("#id")==null){
		if (pic == null ||pic.length==0||pic==undefined) {
			layer.msg("商品图片不能为空", {icon:2});
			return false;
		}
	}
	
	return true;
}

function change() {
	$("#choose").hide();
	$("#previewImage").hide();
	$("#upload").show();
}

function hides() {
	$("#upload").hide();
	$("#choose").show();
	$("#prodImg").attr("value", "");
}

function changeImage() {
	$("#upload").hide();
	$("#previewImage").show();
}

$(function() {
	KindEditor.options.filterMode = false;
	KindEditor.create('textarea[name="detail"]', 
			{
		cssPath : contextPath+'/resources/plugins/kindeditor/plugins/code/prettify.css',
		uploadJson : contextPath+'/editor/uploadJson/upload;jsessionid='+UUID,
		fileManagerJson : contextPath+'/editor/uploadJson/fileManager',
		allowFileManager : true,
		afterBlur : function() {
			this.sync();
		},
		width : '90%',
		height : '400px',
		afterCreate : function() {
			var self = this;
			KindEditor.ctrl(document, 13, function() {
				self.sync();
				document.forms['example'].submit();
			});
			KindEditor.ctrl(self.edit.doc, 13, function() {
				self.sync();
				document.forms['example'].submit();
			});
		}
			});

	$("#prodImg").uploadPreview({
		Img : "ImgPr",
		Width : 120,
		Height : 120,
		ImgType : [ "gif", "jpeg", "jpg", "bmp", "png" ],
		Callback : function() {
		}
	});
})

jQuery.fn.extend({
	uploadPreview : function(opts) {
		var _self = this, _this = $(this);
		opts = jQuery.extend({
			Img : "ImgPr",
			Width : 100,
			Height : 100,
			ImgType : [ "gif", "jpeg", "jpg", "bmp", "png" ],
			Callback : function() {
			}
		}, opts || {});
		_self.getObjectURL = function(file) {
			var url = null;
			if (window.createObjectURL != undefined) {
				url = window.createObjectURL(file)
			} else if (window.URL != undefined) {
				url = window.URL.createObjectURL(file)
			} else if (window.webkitURL != undefined) {
				url = window.webkitURL.createObjectURL(file)
			}
			return url
		};
		_this.change(function() {
			if (this.value) {
				if (!RegExp("\.(" + opts.ImgType.join("|") + ")$", "i").test(this.value.toLowerCase())) {
					layer.alert("选择文件错误,图片类型必须是"+opts.ImgType.join("，")+"中的一种", {icon:2});
					this.value = "";
					return false
				}

				if ($.browser.msie) {
					try {
						$("#" + opts.Img).attr('src', _self.getObjectURL(this.files[0]))
					} catch (e) {
						var src = "";
						var obj = $("#" + opts.Img);
						var div = obj.parent("div")[0];
						_self.select();
						if (top != self) {
							window.parent.document.body.focus()
						} else {
							_self.blur()
						}
						src = document.selection.createRange().text;
						document.selection.empty();
						obj.hide();
						obj.parent("div").css(
								{
									'filter' : 'progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale)',
									'width' : opts.Width + 'px',
									'height' : opts.Height + 'px'
								});
						div.filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = src
					}
				} else {
					$("#" + opts.Img).attr('src', _self.getObjectURL(this.files[0]))
				}
				opts.Callback()
			}
		})
	}
});