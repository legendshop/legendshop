jQuery.validator.addMethod("checkPassword", function(value, element) {
	var length = value.length;
	/* var regx = /(?=.*[a-zA-Z])(?=.*[\d])[\w\W]{6,16}/; */
	var regx1= /^[0-9A-z`~!@#$%^&*()_\-+=<>?:"{}|,.\/;'\\[\]·~！@#￥%……&*（）——\-+={}|《》？：“”【】、；‘’，。、]+$/g;
	var regx= /(?!^[0-9]+$)(?!^[A-z]+$)(?!^[~\\`!@#$%\\^&*\\(\\)-_+={}|\\[\\];':\\\",\\.\\\\\/\\?]+$)(?!^[^A-z0-9]+$)^.{6,20}$/;
	return this.optional(element) || (length >= 6 && length<=20 && regx1.test(value) && regx.test(value) && value.indexOf(" ") < 0);
}, '密码由6-20位字母、数字或符号(除空格)的两种及以上组成');

//校验手机号码，可以为空
jQuery.validator.addMethod("checkMobile",function(value,element){
	var Reg = /^[1][0-9]{10}$/;

	if(!value){
		return true;
	}		

	if(!Reg.test(value)){
		return false;
	}

	return true;
},"手机号码格式不正确");

//校验身份证号,可以为空
jQuery.validator.addMethod("checkIdCardNum",function(value,element){
	var v = $("#idCardNum").val();

	if(!value){
		return true;
	}
	return IdCardValidate(v);
},"身份证格式不正确");

//校验地址，可以为空
jQuery.validator.addMethod("checkAddr",function(value,element){

	var Reg = /^[A-Za-z0-9\u4e00-\u9fa5]+$/;

	if(!value){
		return true;
	}		

	if(!Reg.test(value)){
		return false;
	}

	return true;
},"住址只能由中文、字母或数字组成");

$(document).ready(function() {
	jQuery("#form1").validate({
		rules : {
			name : {
				required : true,
				minlength : 3,
				stringCheck : true,
				checkName1 : true
			},
			realName : "required",
			deptName : "required",
			password : {
				required : true,
				checkPassword : true
			},
			password2 : {
				equalTo : "#password"
			},
			mobile : {
				checkMobile : true
			},
			idCardNum : {
				checkIdCardNum : true
			},
			addr : {
				checkAddr : true
			}
		},
		messages : {
			name : {
				required : "请输入管理员名称",
				minlength : "管理员名字必须大于3个字符"
			},
			realName : {
				required : "请输入真实名字"
			},
			deptName : {
				required : "请输入部门"
			},
			password : {
				required : "请输入密码"
			},
			password2 : {
				equalTo : "密码必须要一致"
			}
		},
		submitHandler : function(form) {
			var role = $("input[name='roleId']:checked");
			if (role.length == 0) {
				layer.msg("请选择角色",{icon:0});
				return false;
			}
			form.submit();
		}
	});
	// 构造菜单
	createTree();
});

// 检查管理员是否已经存在
function checkAdminName() {
	var result = true;
	var nameValue = jQuery("#name").val();
	if (nameValue != null && nameValue != '') {
		if (nameValue.length >= 3 && nameValue.isAlpha()) {
			//call ajax action
			$.ajax({
				url : contextPath + "/admin/adminUser/isAdminUserExist",
				data : {
					"userName" : nameValue
				},
				type : 'post',
				async : false, //默认为true 异步   
				error : function(jqXHR, textStatus, errorThrown) {
					//console.log(textStatus, errorThrown);
				},
				success : function(retData) {
					if ('true' == retData) {
						result = false;
					}
				}
			});
		}
	}
	return result;
}

/*** 检查是否由数字字母和下划线组成 ***/
String.prototype.isAlpha = function() {
	return (this.replace(/\w/g, "").length == 0);
}

function showMenu(id) {
	var nameObj = $("#deptName");
	var nameOffset = nameObj.offset();
	$("#menuContent").css({
		left : nameOffset.left + "px",
		top : nameOffset.top + nameObj.outerHeight() + "px"
	}).slideDown("fast");
	$("body").bind("mousedown", blurMenu);
}

function hideMenu() {
	$("#menuContent").fadeOut("fast");
	$("body").unbind("mousedown", blurMenu);
}

function blurMenu(event) {
	var stillFocus = event.target.id == "menuBtn"
		|| event.target.id == "menuContent"
			|| $(event.target).parents("#menuContent").length > 0;
			if (!stillFocus) {
				hideMenu();
			}
}

function updateDept(deptId, deptName) {
	$("#deptName").val(' ' + deptName);
	$("#deptId").val(' ' + deptId);
}

function createTree() {
	var setting = {};
	setting.view = {
			selectedMulti : false
	};
	setting.edit = {
			enable : false
	};
	setting.callback = {
			onClick : onClick
	};
	setting.data = {
			simpleData : {
				enable : true,
				pIdKey : "parentId"
			}
	};

	var request = {};
	request.url = contextPath+'/admin/department/queryDeptInfo';
	request.type = 'post';
	request.dataType = 'json';
	request.ContentType = 'application/json; charset=utf-8';
	request.error = function(msg) {
		layer.alert("树状菜单创建失败",{icon:2});
	};
	request.success = function(data) {
		$.fn.zTree.init($("#deptMenu"), setting, data);
		$.fn.zTree.getZTreeObj("deptMenu").expandAll(true);
		initDept();
	};

	$.ajax(request);
}

function initDept() {
	var tree = $.fn.zTree.getZTreeObj("deptMenu");
	$("input[id^='deptName']").each(function() {

		var dept = $(this).attr("name");
		if (dept == "")
			return;

		var node = tree.getNodeByParam("id", dept);
		if (node != null)
			$(this).attr("value", "  " + node.name);

	});
}

function onClick(event, treeId, treeNode) {
	var zTree = $.fn.zTree.getZTreeObj("deptMenu");
	var dept = zTree.getSelectedNodes()[0];
	updateDept(dept.id, dept.name);
	hideMenu();
}

laydate.render({
	elem: '#activeTime',
	calendar: true,
	theme: 'grid',
	trigger: 'click'
});

laydate.render({
	elem: '#hireDate',
	calendar: true,
	theme: 'grid',
    trigger: 'click'
});