function updateAdv(floorId,floorItemId){
	var layout=$("input[name='layout']").val();
	layer.open({
		title: '编辑广告',
		id: 'Floor',
		type: 2,
		content: contextPath+'/admin/floorItem/advFloorOverlay/'+ floorId +'/'+ floorItemId+"?layout="+layout,
		area: ['650px', '400px']
	});
}

function deleteById(advtId,floorItemId){
	layer.confirm('是否删除该广告信息,删除后不能恢复?', {icon:3, title:'提示'}, function(index) {
		var url = contextPath+"/admin/floorItem/deleteAdvtItem/"+floorItemId+"/"+advtId;
		var tr = $("#adv_"+floorItemId);
		jQuery.ajax({
			type:'POST',
			"url": url,
			datatype: "json",
			success:function(result){
				var data=eval(result);
				if(data=="ok"){
					tr.remove();
				}else{
					layer.alert(data, {icon:2});
					return;
				}
			}
		});
		layer.close(index);
	});
}

function loadAdv(floorId){
	var limit=$("input[name='limit']").val();
	var layout=$("input[name='layout']").val();

	var _length=$("table[class='simple'] tbody").find("tr").length;
	if(limit!="" && limit!=null&& limit!=undefined){
		if(_length>=limit){
			layer.alert("广告不能超过"+limit, {icon:2});
			return;
		}
	}

	layer.open({
		title: '编辑广告',
		id: 'Floor',
		type: 2,
		content: contextPath+'/admin/floorItem/advFloorOverlay/'+floorId+"?layout="+layout,
		area: ['650px', '400px']
	})
}