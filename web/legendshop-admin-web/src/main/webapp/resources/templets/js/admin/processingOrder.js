$(document).ready(function() {

	//普通订单调整订单
	$(document).on("click",".orderModify", function(){
		var freight = $(this).attr("freight");

		if(isBlank(freight)){
			freight = Number(0);
		}

		var actualTotal = $(this).attr("actualTotal");
		var subtype = $(this).attr("subtype");
		var name = $(this).attr("name");
		var subNumber = $(this).attr("subNumber");
		var total = Number(actualTotal) - Number(freight);
		changeAmountShow(subNumber, name, total, freight, actualTotal,subtype);
	});

	//预售订单调整订单
	$(document).on("click", ".presellOrderModify", function(){
		var subNum = $(this).attr("subNum");
		var userName = $(this).attr("userName");
		var preDepositPrice = $(this).attr("preDepositPrice");
		var finalPrice = $(this).attr("finalPrice");

		var actualTotal = $(this).attr("actualTotal");
		var payPctType = $(this).attr("payPctType");
		changePresellAmountShow(subNum, userName, preDepositPrice, finalPrice,actualTotal,payPctType)
	})


	//监控普通订单金额改变
	$(document).on("change", "#order_fee_amount, #order_fee_ship_price", function(){
		var fee_amount = $("#order_fee_amount").val();
		if(isBlank(fee_amount) || !isMoney(fee_amount) || fee_amount==0){
			layer.msg("请正确输入商品总价",{icon:0});
			return false;
		}
		var _freight=$("#order_fee_ship_price").val();
		if(isBlank(_freight) || !isMoney(_freight) || _freight < 0){
			layer.msg("请正确输入配送金额",{icon:0});
			return false;
		}
		$("#totalPrice").val(Number(fee_amount)+Number(_freight));
		$("#totalPrice").prev().html(Number(fee_amount)+Number(_freight));
	});


	//预售订单监控 输入框的值改变时,改变订单金额
	$(document).on("change", "#preDepositPrice, #finalPrice", function(){
		var preDepositPrice = $("#preDepositPrice").val();
		var finalPrice = $("#finalPrice").val();

		if(isBlank(preDepositPrice) || !isMoney(preDepositPrice) || preDepositPrice == 0){
			layer.msg("请输入正确的定金金额",{icon:0});
			return false;
		}
		if(payPctTypeTemp == 1) {//定金支付才需要校验
			if(isBlank(finalPrice) || !isMoney(finalPrice) || finalPrice == 0){
				layer.msg("请正确的尾款金额",{icon:0});
				return false;
			}
		}

		var totalPrice = 0;
		if(payPctTypeTemp == 1) {//定金支付
			totalPrice = Number(preDepositPrice) + Number(finalPrice);
		}else {//全额支付
			totalPrice = Number(preDepositPrice)
		}

		$("#presellTotalPrice").val(totalPrice);
		$("#presellTotalPriceText").text("￥" + totalPrice);
	});


	$("b[mark^='name_']").hover(function(){
		$(".xx").hide();
		$(this).parent().parent().find("div.xx").show();
	},function(){
		$(".xx").hide();
	});

	//页面加载模块
	$("b[mark^='name_']").each(function (){
		var _this=$(this);
		var addrOrderId=$(_this).attr("addrOrderId");
		if(addrOrderId!=null || addrOrderId!=undefined || addrOrderId!=""){
			$.ajax({
				url:contextPath+"/admin/order/findUsrAddrSub/"+addrOrderId,
				type:'POST',
				async : false, //默认为true 异步
				dataType:'json',
				error: function(jqXHR, textStatus, errorThrown) {
				},
				success:function(result){
					var html="<div style='display:none;' class='xx'>";
					html += " <h6>买家信息</h6>";
					html += " <ul id='table_56'>";
					if(!isBlank(result.receiver)){
						html += "<li><div> <span class='xx_sp'>姓 名：</span><span class='xx_date'>"+result.receiver+"</span></div></li>";
					}
					if(!isBlank(result.telphone)){
						html += "<li><div> <span class='xx_sp'>电 话：</span><span class='xx_date'>"+result.telphone+"</span></div></li>";
					}
					if(!isBlank(result.mobile)){
						html += "<li><div> <span class='xx_sp'>手 机：</span><span class='xx_date'>"+result.mobile+"</span></div></li>";
					}
					if(!isBlank(result.subPost)){
						html += "<li><div> <span class='xx_sp'>邮 编：</span><span class='xx_date'>"+result.subPost+"</span></div></li>";
					}
					if(!isBlank(result.detailAddress)){
						html += "<li><div> <span class='xx_sp'>地 址：</span><span class='xx_date'>"+result.detailAddress+"</span></div></li>";
					}
					html += "</ul></div>";
					$(_this).append(html);
				}


			});
		}
	});

	laydate.render({
		elem: '#startDate',
		calendar: true,
		theme: 'grid',
		trigger: 'click'
	});

	laydate.render({
		elem: '#endDate',
		calendar: true,
		theme: 'grid',
		trigger: 'click'
	});

	if(shopName == ''){
        makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'shopName');
    }else{
    	makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",shopName, 'shopName');
		$("#select2-chosen-1").html(shopName);
    }

	sendData(1);
});

function sendData(curPageNO) {
    var data = {
        "shopId": $("#shopId").val(),
        "curPageNO": curPageNO,
        "status": $("#status").val(),
        "subNumber": $("#subNumber").val(),
        "userName": $("#userName").val(),
        "startDate": $("#startDate").val(),
        "endDate": $("#endDate").val(),
        "subType": $("#subType option:selected").val(),
        "shopName":shopName
    };
    $.ajax({
        url: contextPath+"/admin/order/processingContent",
        data: data,
        type: 'post',
        async: true, //默认为true 异步
        success: function (result) {
            $("#orderContentList").html(result);
        }
    });
}

function makeSelect2(url,element,text,label,value,holder, textValue){
	$("#"+element).select2({
        placeholder: holder,
        allowClear: true,
        width:'200px',
        ajax: {
    	  url : url,
    	  data: function (term,page) {
                return {
                	 q: term,
                	 pageSize: 10,    //一次性加载的数据条数
                 	 currPage: page, //要查询的页码
                };
            },
			type : "GET",
			dataType : "JSON",
            results: function (data, page, query) {
            	if(page * 5 < data.total){//判断是否还有数据加载
            		data.more = true;
            	}
                return data;
            },
            cache: true,
            quietMillis: 200//延时
      },
        formatInputTooShort: "请输入" + text,
        formatNoMatches: "没有匹配的" + text,
        formatSearching: "查询中...",
        formatResult: function(row,container) {//选中后select2显示的 内容
            return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
        },formatSelection: function(row) { //选择的时候，需要保存选中的id
        	$("#" + textValue).val(row.text);
            return row.text;//选择时需要显示的列表内容
        },
    });
}

//选择其他原因
function switch_reason(type){
	if(0==type){
		$("#orderCancel").find("#other_reason").hide();
	}else{
		$("#orderCancel").find("#other_reason").show();
	}
}

function isMoney(obj) {
	if (!obj)
		return false;
	return (/^\d+(\.\d{1,2})?$/).test(obj);
}

function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	sendData(curPageNO);
}

//调整普通订单费用
function changeOrderFee(){
	var _subnumber = $.trim($("#order_fee_id").html());
	if(isBlank(_subnumber)){
		layer.msg("找不到订单号",{icon:0});
		return;
	}
	var fee_amount = $("#order_fee_amount").val();
	if(isBlank(fee_amount)){
		layer.msg("商品总价不能为空",{icon:0});
		return false;
	}
	if(!isMoney(fee_amount) || fee_amount==0){
		layer.msg("请正确输入商品总价",{icon:0});
		return false;
	}
	var _freight=$("#order_fee_ship_price").val();
	if(isBlank(_freight)){
		_freight=0;
	}
	if(!isMoney(_freight)){
		layer.msg("请正确输入配送金额",{icon:0});
		return false;
	}

	$.ajax({
		url:contextPath+"/admin/order/changeOrderFee",
		data:{"subNumber":_subnumber,"freight":_freight,"orderAmount":fee_amount},
		type:'POST',
		async : false, //默认为true 异步
		dataType:'json',
		error: function(jqXHR, textStatus, errorThrown) {
			layer.alert("网络异常,请稍后重试！",{icon:2});
		},
		success:function(result){
			if(result=="OK"){
				layer.msg("调整订单费用成功",{icon:1,time:500},function(){
					layer.closeAll();
					window.location.reload(true);
				});
			}else if(result=="fail"){
				layer.msg("调整订单费用失败",{icon:2,time:500});
			}
		}
	});
}


//调整预售订单费用信息
function changePreOrderFee(){
	var subNumber = $("#orderFeeId").val();
	var preDepositPrice = $("#preDepositPrice").val();
	var finalPrice  = $("#finalPrice").val();

	if(validate(subNumber,preDepositPrice,finalPrice)){
		$("#orderFee").attr("disabled","disabled");
		$.ajax({
			url : contextPath+"/admin/order/changePreOrderFee",
			data : {"subNumber":subNumber, "preDepositPrice":preDepositPrice, "finalPrice":finalPrice},
			type : "POST",
			async : false, //默认为true 异步
			dataType : "JSON",
			error: function(jqXHR, textStatus, errorThrown) {
				$("#orderFee").attr("disabled","");
				return;
			},
			success:function(result){
				if(result=="OK"){
					layer.msg("调整订单费用成功!",{icon:1, time:1000}, function(){
						window.location.reload(true);
					});
				}else if(result=="fail"){
					layer.alert("调整订单费用失败!", {icon:2});
					$("#orderFee").attr("disabled","");
					return;
				}
			}
		});
	}
}


//取消订单
function orderCancel(){
	var _subnumber=$("#sub_id").val();
	if(_subnumber==null || _subnumber=="" || _subnumber==undefined){
		return;
	}
	var other=$('input:radio[name=state_info]:checked').val();
	if(other==null || other=="" || other==undefined){
		layer.msg("请选择设置取消原因",{icon:0});
		return;
	}
	var info="";
	if(other=="其他原因"){
		var other_info=$("#orderCancel").find('#other_state_info').val();
		if(other_info==null || other_info=="" || other_info==undefined){
			layer.msg("请填写备注信息",{icon:0});
			return;
		}
		info=other_info;
	}else{
		info=other;
		$('#other_state_info').val("");
	}
	$.ajax({
		url:contextPath+"/admin/order/orderCancel",
		data:{"subNumber":_subnumber,"status":5,"other":info},
		type:'POST',
		async : false, //默认为true 异步
		dataType:'json',
		error: function(jqXHR, textStatus, errorThrown) {
			layer.alert("网络异常,请稍后重试！",{icon:2});
		},
		success:function(result){
			if(result=="OK"){
				layer.msg("取消订单成功",{icon:1,time:500},function(){
					layer.closeAll();
					window.location.reload(true);
				});
			}else if(result=="fail"){
				layer.msg("取消订单失败",{icon:2},function(){
					layer.closeAll();
				});
				return;
			}
		}
	});

}

//取消订单
function orderCancelShow(_subnumber){
	if(_subnumber==null){
		return;
	}
	$("#sub_id").val(_subnumber);
	$("#sub_id").parent().next().html(_subnumber);

	layer.open({
		title :"取消订单",
		id:"orderCancel",
		type: 1,
		content: $("#order_cancel").html(), //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		area: ['450px','420px']
	});
}

//显示调整普通订单费用窗口
function changeAmountShow(_subnumber,_name,_cash,_freight,_totalcash,_subType){
	if(_subnumber==null || _name==null || _cash==null || _freight==null ||_totalcash==null || _subType ==null){
		return;
	}

	$("#order_fee_username").html(_name);
	$("#order_fee_id").html(_subnumber);
	$("#order_fee_id").prev().val(_subnumber);
	$("#order_fee_amount").val(new Number(_cash));
	$("#order_fee_ship_price").val(new Number(_freight));
	$("#totalPrice").val(new Number(_totalcash));
	$("#totalPriceText").html("￥"+new Number(_totalcash));

  if (_subType == "SHOP_STORE") {
    $("#order_fee_ship_div").hide();
  }

	layer.open({
		title :"调整费用",
		type: 1,
		content: $("#order_fee"),
		area: ['500px','420px']
	});
}

//显示调整预售订单费用窗口
function changePresellAmountShow(subNumber,userName,depositPrice,finalPrice, actualTotalPrice, payPctType){
	if(isBlank(actualTotalPrice) || isBlank(userName) || isBlank(depositPrice)
			|| isBlank(finalPrice) || isBlank(actualTotalPrice||payPctType)){
		layer.alert("数据异常，有空值", {icon:2});
		return;
	}

	$("#orderFeeIdText").text(subNumber);
	$("#orderFeeId").val(subNumber);
	$("#orderFeeUserNameText").text(userName);
	$("#orderFeeUserName").val(userName);
	$("#preDepositPrice").val(new Number(depositPrice));
	$("#finalPrice").val(new Number(finalPrice));
	$("#presellTotalPrice").val(new Number(actualTotalPrice));
	$("#presellTotalPriceText").text("￥"+new Number(actualTotalPrice));

	//判断是否显示尾款
	if(payPctType == 1) {//定金
		$("#preDepositPriceTr").hide();
	}else { //尾款
		$("#preDepositPriceTr").show();
	}

	//记录是全额支付，还是定金支付
	payPctTypeTemp = payPctType;

	layer.open({
		title :"调整费用",
		type: 1,
		content: $("#presell_order_fee"),
		area: ['415px','425px']
	});
}

//校验调整后的数据
function validate(subNumber,preDepositPrice,finalPrice,freightAmount){
	if(isBlank(subNumber)){
		layer.alert("非法操作!", {icon:2});
		return false;
	}
	if(isBlank(preDepositPrice)){
		layer.alert("请输入商品定金!", {icon:2});
		return false;
	}
	if(isBlank(finalPrice)){
		layer.alert("请输入商品定金!", {icon:2});
		return false;
	}
	if(!isMoney(preDepositPrice)){
		layer.alert("请输入正确的商品定金，金额最多只能有两位小数!", {icon:2});
		return false;
	}
	if(parseFloat(preDepositPrice)<=0){
		layer.alert("商品定金必须大于0!", {icon:2});
		return false;
	}
	if(!isMoney(finalPrice)){
		layer.alert("请输入正确的商品尾款，金额最多只能有两位小数!", {icon:2});
		return false;
	}
	if(payPctTypeTemp==1&&parseFloat(finalPrice)<=0){
		layer.alert("商品尾款必须大于0!", {icon:2});
		return false;
	}
	return true;
}

//发货
function deliverGoods(_orderId){
	layer.open({
		title :"发货",
		type: 2,
		content: [contextPath+'/admin/order/deliverGoods/'+_orderId,'no'],
		area: ['430px', '270px']
	});
}
function export_excel(){
	var list=$(".orderlist_one").get();
	if(list.length<=0){
		layer.msg("没有导出的订单数据",{icon:0});
		return;
	}
	var orders=new Array();
	for(var i = 0; i < list.length; i++){
		var _orderId=$(list[i]).attr("orderId");
		if(_orderId!=undefined  && _orderId!="" && _orderId!=null && _orderId!=0){
			orders.push(_orderId);
		}
	}
	if(orders.length<=0){
		layer.msg("没有导出的订单数据",{icon:0});
		return;
	}
	$(".editAdd_load").show();
	jQuery.ajax({
		url:contextPath+"/admin/order/exportExcel",
		data:{"items":orders},
		type:"post",
		async : false, //默认为true 异步
		dataType : 'json',
		error:function(data){
			$(".editAdd_load").hide();
			layer.msg("导出数据失败",{icon:0});
			return ;
		},
		success:function(data){
			if(data.result=="false"){
				$(".editAdd_load").hide();
				layer.msg(data.msg,{icon:0});
				return
			}
			else if(data.result=="true"){
				layer.load(1,{time:2000})
				var url=contextPath+"/servlet/downloadfileservlet/"+data.msg;
				window.open(url);
				return;
			}
		}
	});
}

//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}

//订单导出
function exportOrder(){
	$("#exportForm").submit();
}

function search(){
	$("#ListForm #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	sendData(1);
}
