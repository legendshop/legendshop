$(document).ready(function() {
	jQuery.validator.addMethod("checkBrandName", function(value,element){
		var returnMsg=true;
		jQuery.ajax({
			url: contextPath+"/admin/prodGroup/checkBrandByName",
			data: {"name" : $("#name").val(),"id" : $("#id").val()},
			type: "post",
			async: false,
			success: function(retData){
				if(retData=='true'){
					returnMsg=false;
				}
			}
		});
		return returnMsg;
	}, "此品牌已被创建!");


	var isSubmitted = false;
	jQuery("#form1").validate({
		rules: {
			name: {
				required: true,
				checkBrandName: true //期望的是true,如果为false则展示提示信息
			},
      description: {
			  required: true,
        checkBrandName: true
      }
		},
		messages: {
			name: {
				required: "请输入分组名"
			},
      description: {
			  required: "请输入分组描述"
      }
		},

		submitHandler : function(form){
			//防止表单重复提交
			if(!isSubmitted){
				form.submit();
				isSubmitted = true;
			}else{
				layer.alert('请不要重复提交表单!', {icon: 0});
			}
		}
	});



	$("#file").change(function(){
		checkImgType(this);
		checkImgSize(this,5120);
	});

	$("#bigImagefile").change(function(){
		checkImgType(this);
		checkImgSize(this,5120);
	});

	$("#brandAuthorizefile").change(function(){
		checkImgType(this);
		checkImgSize(this,5120);
	});

});
