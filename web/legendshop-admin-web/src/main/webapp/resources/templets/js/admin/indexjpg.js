$(document).ready(function () {
	$("#indexJpgForm").validate({
		rules: {
			alt: "required",
			title: "required",
			stitle: "required",
			link: "required",
			titleLink: "required",
			file: {
				required: "#imgName:blank",
				isImage:true
			},
			seq: {
				number: true
			}
		},
		messages: {
			alt: "说明文字不能少于5个字符",
			title: "请输入说明文字",
			stitle: "请输入说明文字",
			link: "请输入说明文字",
			titleLink: "请输入说明文字",
			file: {
				required:"上传文件不能为空",
				isImage:"请选择正确的图片上传格式"
			},
			seq: {
				number: "请输入数字"
			}
		}
	});
	$("#col1 tr").each(function (i) {
		if (i > 0) {
			if (i % 2 == 0) {
				$(this).addClass('even');
			} else {
				$(this).addClass('odd');
			}
		}
	});
	$("#col1 th").addClass('sortable');
});

$.validator.addMethod("isImage",function(value,element){
	if(value==""){
		return true;
	}
	if (!/\.(JPEG|GIF|JPG|PNG|jpeg|gif|jpg|png)$/.test(value)) {
		return false;
	}else{
		return true;
	}
},"请上传图片");


$("#file").change(function(){
	checkImgType(this);
	checkImgSize(this,2048);
});