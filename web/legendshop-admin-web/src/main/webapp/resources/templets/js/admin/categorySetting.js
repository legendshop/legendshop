$(function(){
	
	//没有数据的情况下给默认展示
	if (isBlank(category)) {
		category = 'first';
		$("#selectCategory").hide();
	}
	
	$(".cate-item").each(function(){
		if($(this).attr("data-type") == category){
			$(this).addClass("cur");
			if(schema == 'noImages'){
				var noImg = $(this).attr("data-noImg");
				$(".item-view img").attr("src",contextPath+noImg);
			}else{
				var img = $(this).attr("data-img");
				$(".item-view img").attr("src",contextPath+img);
			}
			
		}
	});
	$("#classify .cate-item").on("click",function(){
		if($(this).attr("data-type") == 'first'){
			$("#selectCategory").hide();
			var img = $(this).attr("data-img");
			$(".item-view img").attr("src",contextPath+img);
		}else{
			$("#selectCategory").show();
			if($(".cate-img input:radio[name='schema']:checked").val()=='hasImages'){
				var img = $(this).attr("data-img");
				$(".item-view img").attr("src",contextPath+img);
			}else{
				var noImg = $(this).attr("data-noImg");
				$(".item-view img").attr("src",contextPath+noImg);
			}
		}
		$("#classify").find(".cur").removeClass("cur");
		$(this).addClass("cur");
	});
	
	$(".cate-img input:radio[name='schema']").on("click",function(){
		if($(this).val()=='noImages'){
			var noImg = $("#classify").find(".cur").attr("data-noImg");
			$(".item-view img").attr("src",contextPath+noImg);
		}else{
			var img = $("#classify").find(".cur").attr("data-img");
			$(".item-view img").attr("src",contextPath+img);
		}
	});
});

function save(){
	var categorySetting = new Object();
	var category = $("#classify").find(".cur").attr("data-type");
	categorySetting.category = category;
	if(category=="first"){
		categorySetting.schema = "noImages";
	}else{
		var schema = $(".cate-img input:radio[name='schema']:checked").val();
		categorySetting.schema = schema;
	}
	var url = contextPath + "/admin/appSetting/saveCategorySetting";
	 $.ajax({
       url: url,
       type : "POST",
       data:JSON.stringify(categorySetting), 
       contentType:"application/json",     
       dataType:"json", 
       async: false, //默认为true 异步   
       success: function (data) {
           if (data == "OK") {
        	   layer.msg('保存成功',{icon:1});
           	setTimeout(function(){
           		parent.window.location.href = contextPath + "/admin/appSetting/categorySetting";}, 1000);  
           } else {   
        	   layer.msg(data,{icon:2});
           }
       } 
   });
}

//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}