function newTemplate(){
	var template='<div id="J_Tpl" class="J_Tpl template box" afterSaleId ="">'+
	'<div class="hd">'+
	'<h2>'+
	'<input maxlength="20" title="填写模板名称" id="J_NameEdit" value="" class="name">'+
	'<span id="J_NameShow"></span>'+
	'</h2>'+
	'</div>'+

	'<div class="bd">'+
	'<div id="J_ConShow" class="result"></div>'+
	'<textarea title="填写承诺内容" id="J_ConEdit" class="content J_ConEdit"></textarea>'+
	'<div class="apply">'+
	'<a class="J_Apply" href="javascript:void(0);" onclick="appTemplate(this);">'+
	'<span>应用该模板</span>'+
	'</a>'+
	'</div>'+
	'<div class="action">'+
	'<div class="letter-indicate">您还可以输入 <em id="J_LetterNum"></em> 字的内容</div>'+
	'<button id="J_Cancel" class="J_Cancel" type="button" onclick="cancelEdit(this);">取消</button>'+
	'<button class="J_Save" id="J_Save" type="button" onclick="saveEdit(this);">保存</button>'+
	'</div>'+
	'</div>'+

	'<div class="ft">'+
	'<span class="notice">卖家承诺所填写的内容，不得与国家相关法律，法规相关规定有冲突！</span>'+
	'<div class="action">'+
	'<a class="J_Edit" href="javascript:void(0);" onclick="editTemplate(this);">修改</a> - <a type="submit" class="J_Delete" href="javascript:void(0);" onclick="delTemplate(this);">删除</a>'+
	'</div>'+
	'</div>'+
	'</div>';

	$("#J_TplBox").prepend(template);
}

function editTemplate(obj){
	$(obj).parent().parent().parent().attr("class","J_Tpl template box");
}

function cancelEdit(obj){
	var conShow = $(obj).parent().parent().find("div[class=result]").text().length;
	var nameShow = $(obj).parent().parent().parent().find("span[id=J_NameShow]").text().length;
	if(conShow==0&&nameShow==0){
		$(obj).parent().parent().parent().remove();
	}else{
		$(obj).parent().parent().parent().attr("class","J_Tpl template box saved");
	}
}


//保存模板
function saveEdit(obj){
	var bdDiv = $(obj).parent().parent();
	var nameEdit = bdDiv.parent().find("input[id=J_NameEdit]").val();
	var conEdit = bdDiv.find("textarea[id=J_ConEdit]").val();
	var afterSaleId = bdDiv.parent().attr("afterSaleId");
	$.ajax({
		url:contextPath+'/admin/afterSale/save', 
		data:{"id":afterSaleId,"title":nameEdit,"content":conEdit},
		type:'post', 
		async : true, //默认为true 异步   
		error: function(jqXHR, textStatus, errorThrown) {
			// alert(textStatus, errorThrown);
		},
		success:function(result){
			bdDiv.parent().find("span[id=J_NameShow]").html(nameEdit);
			bdDiv.find("div[class=result]").html(conEdit);
			bdDiv.parent().attr("afterSaleId",result);
			bdDiv.parent().attr("afterSaleName",nameEdit);
			bdDiv.parent().attr("id","J_Tpl_"+result);
			bdDiv.parent().attr("class","J_Tpl template box saved");
		}
	});
}

function delTemplate(obj){
	var afterSaleId = $(obj).parent().parent().parent().attr("afterSaleId");
	$.ajax({
		url:contextPath+'/admin/afterSale/delete', 
		data:{"id":afterSaleId},
		type:'post', 
		async : true, //默认为true 异步   
		error: function(jqXHR, textStatus, errorThrown) {
			// alert(textStatus, errorThrown);
		},
		success:function(result){
			$(obj).parent().parent().parent().remove();
		}
	});
}

function pager(curPageNO){
	window.location.href = contextPath+"/admin/afterSale/overlay?curPageNO=" + curPageNO;
}

//应用该模板
function appTemplate(obj){
	var afterSaleId = $(obj).parent().parent().parent().attr("afterSaleId");
	var afterSaleName = $(obj).parent().parent().parent().attr("afterSaleName");
	var win = art.dialog.open.origin;
	win.setAfterSaleId(afterSaleId,afterSaleName);
	art.dialog.close();
}