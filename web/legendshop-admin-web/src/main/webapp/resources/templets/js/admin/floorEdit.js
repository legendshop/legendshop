$(document).ready(function() {
	var cssStyle = ${floor.cssStyle} ;
	if(cssStyle == 1){--%>
	$(".indexlp").addClass("blueback");
	$(".lpleft").addClass("smback");
	}else if(cssStyle == 2){
		$(".indexlp").addClass("greenback");
		$(".lpleft").addClass("spback");
	}else if(cssStyle == 3){
		$(".indexlp").addClass("redback");
		$(".lpleft").addClass("ssback");
	}

	$("#sorts").hover(function () {  
		$("#sortsEdit").show();
	},function () {  
		$("#sortsEdit").hide();  
	});

	$("#prods").hover(function () {  
		$("#prodEdit").show();
	},function () {  
		$("#prodEdit").hide();  
	});

	$("#brands").hover(function () {  
		$("#brandEdit").show();
	},function () {  
		$("#brandEdit").hide();  
	});

	$("#adv").hover(function () {  
		$("#advEdit").show();
	},function () {  
		$("#advEdit").hide();  
	});

	$("div[class='left-ads']").hover(function () {  
		$("#floorImage").show();
	},function () {  
		$("#floorImage").hide();  
	});

	$("#bottom").hover(function () {  
		$("#bottomAdvEdit").show();
	},function () {  
		$("#bottomAdvEdit").hide();  
	});

	//子楼层产品数据
	//$(".l1:eq(0)").addClass("focus");
	var url = contextPath+"/admin/subFloorItem/sendFloorProduct/"+$(".l1:eq(0)").val();
	sendData(url);

	$(".l1").bind("click", function() {
		$(".l1").removeClass("tabs-selected");
		var _this = $(this);
		_this.addClass("tabs-selected");
		var url = contextPath+"/admin/subFloorItem/sendFloorProduct/"+_this.val();
		sendData(url);
	});

	contentType(_contentType);
	lowerContentType(_contentType2);


	$.fn.jfocus = function(settings) {//首页焦点广告图切换
		var defaults = {
				time: 5000
		};
		var settings = $.extend(defaults, settings);
		return this.each(function(){
			var $this = $(this);
			var sWidth = $this.width();
			var len = $this.find("ul li").length;
			var index = 0;
			var picTimer;
			var btn = "<div class='pagination'>";
			for (var i = 0; i < len; i++) {
				btn += "<span></span>";
			}
			btn += "</div><div class='arrow pre'></div><div class='arrow next'></div>";
			$this.append(btn);
			$this.find(".pagination span").css("opacity", 0.4).mouseenter(function() {
				index = $this.find(".pagination span").index(this);
				showPics(index);
			}).eq(0).trigger("mouseenter");
			$this.find(".arrow").css("opacity", 0.0).hover(function() {
				$(this).stop(true, false).animate({
					"opacity": "0.5"
				},
				300);
			},
			function() {
				$(this).stop(true, false).animate({
					"opacity": "0"
				},
				300);
			});
			$this.find(".pre").click(function() {
				index -= 1;
				if (index == -1) {
					index = len - 1;
				}
				showPics(index);
			});
			$this.find(".next").click(function() {
				index += 1;
				if (index == len) {
					index = 0;
				}
				showPics(index);
			});
			$this.find("ul").css("width", sWidth * (len));
			$this.hover(function() {
				clearInterval(picTimer);
			},
			function() {
				picTimer = setInterval(function() {
					showPics(index);
					index++;
					if (index == len) {
						index = 0;
					}
				},
				settings.time);
			}).trigger("mouseleave");
			function showPics(index) {
				var nowLeft = -index * sWidth;
				$this.find("ul").stop(true, false).animate({
					"left": nowLeft
				},
				300);
				$this.find(".pagination span").stop(true, false).animate({
					"opacity": "0.4"
				},
				300).eq(index).stop(true, false).animate({
					"opacity": "1"
				},
				300);
			}
		});
	}

	$(".right-side-focus").jfocus();
});

function onclickTitle(){
	layer.open({
		title :"编辑分类",
		type: 2, 
		content: [contextPath+'/admin/floorItem/sortFloorOverlay/'+${floor.flId},'no'],
		area: ['650px', '450px']
	}); 
}

function onclickProducts(id){
	layer.open({
		title :"编辑产品",
		type: 2, 
		content: [contextPath+'/admin/subFloorItem/productFloorOverlay/'+ id,'no'],
		area: ['650px', '540px']
	}); 
}

function onclickRank(id){
	layer.open({
		title :"编辑排行",
		type: 2, 
		content: [contextPath+'/admin/floorItem/rankFloorOverlay/'+id,'no'],
		area: ['650px', '540px']
	}); 
}

function onclickBrand(id){
	layer.open({
		title :"编辑品牌",
		type: 2, 
		content: [contextPath+'/admin/floorItem/brandFloorOverlay/'+ id,'no'],
		area: ['650px', '540px']
	}); 
}

function onclickGroup(id){
	layer.open({
		title :"编辑团购",
		type: 2, 
		content: [contextPath+'/admin/floorItem/groupFloorOverlay/'+ id,'no'],
		area: ['650px', '540px']
	}); 
}

function onclickAdv(id,obj){
	layer.open({
		title :"编辑广告",
		type: 2, 
		content: [contextPath+'/admin/floorItem/advListOverlay/'+ id,'no'],
		area: ['650px', '400px']
	}); 
}

function onclickLeftAdv(id){
	layer.open({
		title :"编辑广告",
		type: 2, 
		content: [contextPath+'/admin/floorItem/leftAdvOverlay/'+ id,'no'],
		area: ['650px', '400px']
	}); 
}

function onclickBottomAdv(id){
	layer.open({
		title :"编辑广告",
		type: 2, 
		content: [contextPath+'/admin/floorItem/bottomAdvOverlay/'+ id,'no'],
		area: ['650px', '400px']
	}); 
}

function onclickNews(id){
	layer.open({
		title :"编辑文章",
		type: 2, 
		content: [contextPath+'/admin/floorItem/newsFloorOverlay/'+ id,'no'],
		area: ['650px', '540px']
	}); 
}

function sendData(url){
	$.ajax({
		"url":url, 
		type:'post', 
		async : true, //默认为true 异步   
		error: function(jqXHR, textStatus, errorThrown) {
		},
		success:function(result){
			$("div[class='tabs-panel middle-goods-list']").html(result);
		}
	});
}
function contentType(value){
	var s;
	var url;
	if(value == "BD"){
		//s = "品牌旗舰店&nbsp&nbsp<span><a href ='javascript:onclickBrand(${floor.flId})'>品牌编辑</a></span>";
		//$("#UpperRight h3").html(s);
		url = contextPath+"/admin/floor/brandContent/"+_flId;
		sendContent(url);
	}
	else if(value == "PT"){
		s = "商品排行&nbsp&nbsp<span><a href ='javascript:onclickRank("+_flId+")'>编辑排行</a></span>";
		$("#UpperRight h3").html(s);
		url = contextPath+"/admin/floor/rankContent/"+_flId;
		sendContent(url);
	}else if(value == "GP"){
		s= "今日团购&nbsp&nbsp<span><a href ='javascript:onclickGroup("+_flId+")'>团购编辑</a></span>";
		$("#UpperRight h3").html(s);
		url = contextPath+"/admin/floor/groupContent/"+_flId;
		sendContent(url);
	}
}
function lowerContentType(value){
	if(value == "NS"){
		var url = contextPath+"/admin/floor/newsContent/"+_flId;
		sendLowerContent(url);
	}else if(value == "RA"){
		var html = "<a href='javascript:onclickAdv("+_flId+")'>编辑</a> </b> <div><img src='"+contextPath"/images/radv1.jpg' style='width:100%'></div> <div><img src='"+contextPath+"/images/radv2.jpg' style='width:100%;margin-top:10px;'></div>";
		$("#UpperRight").after(html);
	}
}
function sendLowerContent(url){
	$.ajax({
		"url":url, 
		type:'post', 
		async : true, //默认为true 异步   
		error: function(jqXHR, textStatus, errorThrown) {
		},
		success:function(result){
			$("#UpperRight").after(result);
		}
	});
}
function sendContent(url){
	$.ajax({
		"url":url, 
		type:'post', 
		async : true, //默认为true 异步   
		error: function(jqXHR, textStatus, errorThrown) {
		},
		success:function(result){
			$("div[class='recommend-brand']").html(result);
		}
	});
}


//方法，判断是否为空
function isBlank(_value){
	if(_value==null || _value=="" || _value==undefined){
		return true;
	}
	return false;
}