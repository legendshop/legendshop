$(function() {
	$("#updateIosUserAppstore").on("click", function(){
		layer.confirm("您确定更改AppStore地址?", {
			icon: 3
			,btn: ['确定','关闭'] //按钮
		}, function(){
			var url = $("#iosUserAppstore").val();
			$.ajaxUtils.sendPost(urls.updateIOSAppstore, {"url": url, "type": "user"});
		});
		return false;
	});

	$("#updateIosShopAppstore").on("click", function(){
		layer.confirm("您确定更改AppStore地址?", {
			icon: 3
			,btn: ['确定','关闭'] //按钮
		}, function(){
			var url = $("#iosShopAppstore").val();
			$.ajaxUtils.sendPost(urls.updateIOSAppstore, {"url": url, "type": "shop"});
		});
		return false;
	});
});