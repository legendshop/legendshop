$(document).ready(function() {
	jQuery("#updatePwd").validate({
		rules: {
			oldPassword: {
				required: true
			},
			password: {
				required: true,
				passwd:true
			},
			password2:{
				equalTo: "#password"
			}
		},
		messages: {
			oldPassword: {
				required: '必填'
			},
			password: {
				required: '必填'
			},
			password2:{
				equalTo: "密码必须要一致"
			}
		}
	});


	// 附加验证方法
	jQuery.validator.addMethod("passwd", function(value, element) {
		var length = value.length;
		/* var pwd = /(?!.*[\u4E00-\u9FA5\s])(?!^[a-zA-Z]+$)(?!^[\d]+$)(?!^[^a-zA-Z\d]+$)^.{6,20}$/; */
		var regx1= /^[0-9A-z`~!@#$%^&*()_\-+=<>?:"{}|,.\/;'\\[\]·~！@#￥%……&*（）——\-+={}|《》？：“”【】、；‘’，。、]+$/g;
		var regx= /(?!^[0-9]+$)(?!^[A-z]+$)(?!^[~\\`!@#$%\\^&*\\(\\)-_+={}|\\[\\];':\\\",\\.\\\\\/\\?]+$)(?!^[^A-z0-9]+$)^.{6,20}$/;
		return this.optional(element) || (length >= 6 && length<=20 && regx1.test(value) && regx.test(value) && value.indexOf(" ") < 0);
	}, "<br>密码由6-20位字母、数字或符号(除空格)的两种及以上组成");//可以自定义默认提示信息


	//binding Submit
	$("#Submit").click(function(){
		if($("#updatePwd").valid()){
			var formData = $("#updatePwd").serialize();
			$.ajax({
				type: 'post', // 提交方式 get/post
				url:  contextPath+"/admin/adminUser/updatePwd", // 需要提交的 url
				data: formData,
				dataType : 'json', 
				error: function(jqXHR, textStatus, errorThrown) {
					layer.alert("保存失败" + textStatus);
				},
				success: function(data) { // data 保存提交后返回的数据，一般为 json 数据
					// 此处可对 data 作相关处理
					if("OK" == data){
						closeDialog();
						layer.alert("密码修改成功");
					}else if("NOCHANGE" == data){
						layer.alert("密码不能跟原密码一致");
					}else if("DISMATCH" == data){
						layer.alert("请输入正确的密码");
					}else{
						layer.alert("修改密码失败");
					}

				}
			});
			return false; // 阻止表单自动提交事件
		};
	});
});

function closeDialog(){
	var index = parent.layer.getFrameIndex(window.name); 
	parent.refreshMyProfile();
	parent.layer.close(index); //关闭弹出的子页面窗口  
}