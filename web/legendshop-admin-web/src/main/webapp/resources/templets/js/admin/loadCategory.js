function saveFunctionMenu(){
	var zTree = $.fn.zTree.getZTreeObj("catTrees");
	var chkNodes = zTree.getCheckedNodes(true);
	if(chkNodes.length<1) {
		layer.msg("请选择类目",{icon: 0});
		return;
	}
	var catId = chkNodes[0].id;
	var catName = chkNodes[0].name;
	parent.loadCategoryCallBack(catId,catName);
	parent.layer.closeAll('iframe'); //关闭所有的iframe层
}

$(document).ready(function(){
	//zTree设置
	var catTrees = JSON.parse(data); 
	var setting = {
			check:{
				enable: true,
				chkboxType: {'Y':'ps','N':'ps'},
				chkStyle: 'radio',
				radioType: 'all'
			}, 
			data:{
				simpleData:{ enable: true }
			}, callback:{
				beforeCheck: beforeCheck,
				onCheck: onCheck
			} };
	$.fn.zTree.init($("#catTrees"), setting, catTrees);
});

function beforeCheck(){
}

function onCheck(){
}