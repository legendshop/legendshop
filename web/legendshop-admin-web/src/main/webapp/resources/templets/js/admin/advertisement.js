$(document).ready(function() {
	jQuery("#form1").validate({
		rules : {
			linkUrl : {
				required : true,
				minlength : 3
			},
			file : {
				required : "#picUrlName:blank"
			}
		},
		messages : {
			linkUrl : {
				required : "请输入链接地址",
				minlength : "请认真填写，链接地址不能少于3个字符"
			},
			file : {
				required : "请上传广告图片"
			}
		}
	});

	$("#col1 tr").each(function(i) {
		if (i > 0) {
			if (i % 2 == 0) {
				$(this).addClass('even');
			} else {
				$(this).addClass('odd');
			}
		}
	});
	$("#col1 th").addClass('sortable');
});