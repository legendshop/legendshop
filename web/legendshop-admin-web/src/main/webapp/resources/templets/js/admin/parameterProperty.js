function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	document.getElementById("form1").submit();
}

//保存选定的规制
function save_form(){
	var propertyMap = [];
	jQuery("input[type=checkbox]").each(function(){
		var obj = new Object();
		if(jQuery(this).attr("checked")=="checked"){
			obj.propId = jQuery(this).attr("value");
			propertyMap.push(obj);
		}

	});
	var propertyIds = JSON.stringify(propertyMap);
	$.ajax({
		url:contextPath+"/admin/prodType/saveParameterProperty", 
		data: {"propertyIds":propertyIds,"id":proTypeId},
		type:'post', 
		dataType : 'json', 
		async : true, //默认为true 异步   
		error: function(jqXHR, textStatus, errorThrown) {
			layer.alert(textStatus, errorThrown);
		},
		success:function(result){
			parent.location.reload();
		}
	});  
}