function deleteAction() {
	//获取选择的记录集合
	selAry = $(".selectOne");
	if (!checkSelect(selAry)) {
		layer.alert('删除时至少选中一条记录！',{icon: 0});
		return false;
	}

	layer.confirm("确定要删除吗？", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		for (i = 0; i < selAry.length; i++) {
			if (selAry[i].checked) {
				var userId = selAry[i].value;
				var userName = selAry[i].getAttribute("arg");
				deleteUserDetail(userId, userName, true);
			}
		}

		window.location = contextPath + '/admin/system/userDetail/query';
		return true;
	});

}

function confirmDelete(userId, userName) {
	layer.confirm("删除后无法恢复数据， 确定要删除用户 " + userName + "？", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		deleteUserDetail(userId, userName, false);
	});
}

function deleteUserDetail(userId, userName, multiDel) {
	var data = {
			"userId" : userId,
			"userName" : userName
	};
	jQuery
	.ajax({
		url : contextPath + "/admin/system/userDetail/delete",
		data : data,
		type : 'post',
		async : false, //默认为true 异步   
		dataType : 'html',
		success : function(retData) {
			if (!multiDel) {
				if (retData == null || retData == '') {
					window.location = contextPath + '/admin/system/userDetail/query';
				} else {
					layer.msg(retData,{icon: 2});
				}
			}
		}
	});
}

function pager(curPageNO) {
	document.getElementById("curPageNO").value = curPageNO;
	document.getElementById("form1").submit();
}

function initVal(enabledValue, haveShopValue) {
	jQuery("#enabled").val(enabledValue);
	jQuery("#haveShop").val(haveShopValue);
}

jQuery(document).ready(function() {
	initVal(enabled, haveShop);
	highlightTableRows("item");
});

//金币充值页面
function coinPage(userId) {

	layer.open({
		title :"金币充值",
		type: 2, 
		content: contextPath + "/admin/coin/rechargePage?userId="+ userId, //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		area: ['420px', '300px']
	}); 
}

function exportUser() {
	$("#exportForm").submit();
}

function search() {
	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	$("#form1")[0].submit();
}
/*用户上线下线 */
function changeUserStatus(userId, status) {
	if (status == 0) {
		layer.confirm("您确定要下线该用户?<p style='margin-top:5px;font-size:13px;color:#333;'>PS:如果该用户有商城,那么用户的商城也会自动下线!</p>", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   sendReq(userId, status, "恭喜您,下线成功!");
		   });
	} else {
		layer.confirm("您确定要上线该用户?", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   sendReq(userId,status,"恭喜您,上线成功!<p style='margin-top:5px;font-size:13px;color:#333;'>如果该用户有商城,需要去店铺管理手动上线商城,否则商城不会上线!</p>");
		   });
	}
}
/*用户上线下线 */
function sendReq(userId, status, msg) {
	$.ajax({
		url : contextPath+"/admin/system/userDetail/changeStatus/"
				+ userId + "/" + status,
		type : "POST",
		dataType : "JSON",
		async : true,
		success : function(result, status, xhr) {
			if (result == "OK") {
				layer.alert(msg,{icon: 1}, function(index){
					window.location.reload(true);
			  });    
			} else {
				layer.alert(result,{icon: 2});
			}
		}
	});
}
/*批量用户下线*/
$("#batch-offline").on("click",function() {
	//获取选择的记录集合
	selAry = $(".selectOne");
	
	if (!checkSelect(selAry)) {
		layer.alert('请选择要下线的用户!',{icon: 0});
		return false;
	}
	
	layer.confirm("您确定要下线所选用户?<p style='margin-top:5px;font-size:13px;color:#333;'>PS:如果所选用户有商城,那么用户的商城也会自动下线!</p>", {
		 icon: 3
	     ,btn: ['确定','关闭'] //按钮
	   }, function(){
		  
		   var userIdList = new Array();
			for (i = 0; i < selAry.length; i++) {
				if (selAry[i].checked) {
					
					var userId = selAry[i].value;
					userIdList.push(userId);
				}
			}
			
			var userIdStr = userIdList.toString();
			$.ajax({
					url : contextPath+"/admin/system/userDetail/batchOfflineUser/"
							+ userIdStr,
					type : "PUT",
					dataType : "JSON",
					async : true,
					success : function(result, status, xhr) {
						if (result == "OK") {
							layer.alert('批量下线用户成功。',{icon: 1}, function(index){
								window.location.reload(true);
						  });     
						} else {
							layer.alert(result,{icon:2});
					}
				}
			});
		   
	   });
});


