function deleteById(shopId){

	layer.confirm("确定删除,一旦删除将无法恢复?", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		$.ajax({
			type: "GET",
			url: contextPath+"/admin/secDomainName/delete/"+shopId,
			dataType:"json",
			async : true, 
			success:function(data){
				if(data=="OK"){
					layer.msg(data,{icon:1},function(){
						window.location = contextPath+"/admin/secDomainName/query";
					});
				}else {
					layer.msg(data,{icon:0});
					return false;
				}
			}}
	});
}

//转到店铺首页
function toSite(secDomainName){
	var host = '${header.host}';
	var firstDomain = host.substring(host.indexOf(".", 0));
	window.location = "http://"+secDomainName+firstDomain;
}
function search(){
	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	$("#form1")[0].submit();
}