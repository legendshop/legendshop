function changeDeliverNum(){
	var deliv=$("#ecc_id").find("option:selected").val();

	if(deliv==undefined || deliv==null || deliv==""){
		layer.msg("请去设置配送方式", {icon:0});
		return ;
	}

	var dvyFlowId=$.trim($("#dvyFlowId").val());
	if(dvyFlowId==undefined || dvyFlowId==null || dvyFlowId==""){
		layer.msg("请输入物流单号", {icon:0});
		return ;
	}
	var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
 	var reg = /[\u4E00-\u9FA5\uF900-\uFA2D]/;
 	if(pattern.test(dvyFlowId)||reg.test(dvyFlowId)){
 		layer.msg("您的物流单号不合法！", {icon:2});
 		return ;
 	}
	var state_info="";
	var subNumber=$.trim($("#subNumber").val());
	if(subNumber==undefined || subNumber==null || subNumber==""){
		return;
	}

	$.post(contextPath+"/admin/order/changeDeliverNum" , 
			{"subNumber": subNumber, "deliv": deliv,"dvyFlowId":dvyFlowId,"info":state_info},
			function(retData) {
				if(retData == 'OK' ){
					parent.location.reload();
					var index = parent.layer.getFrameIndex('deliverGoods'); //先得到当前iframe层的索引
					parent.layer.close(index); //再执行关闭  
				}else if(retData == 'fail'){
					layer.msg('修改物流单号失败，订单号 ' + subNumber, {icon:2});
				}else{
					layer.msg(retData, {icon:2});
				}
			},'json');
}

function fahuo(){
	var deliv=$("#ecc_id").find("option:selected").val();

	if(deliv==undefined || deliv==null || deliv==""){
		layer.msg("请去设置配送方式", {icon:0});
		return ;
	}

	var dvyFlowId=$.trim($("#dvyFlowId").val());
	if(dvyFlowId==undefined || dvyFlowId==null || dvyFlowId==""){
		layer.msg("请输入物流单号", {icon:0});
		return ;
	}
	var state_info="";
	var subNumber=$.trim($("#subNumber").val());
	if(subNumber==undefined || subNumber==null || subNumber==""){
		return;
	}

	$.post(contextPath+"/admin/order/fahuo" , 
			{"subNumber": subNumber, "deliv": deliv,"dvyFlowId":dvyFlowId,"info":state_info},
			function(retData) {
				if(retData == 'OK' ){
					parent.location.reload();
					var index = parent.layer.getFrameIndex('deliverGoods'); //先得到当前iframe层的索引
					parent.layer.close(index); //再执行关闭  
				}else if(retData == 'fail'){
					layer.msg('发货失败，订单号 ' + subNumber, {icon:2});
				}else{
					layer.msg(retData, {icon:2});
				}
			},'json');
}

function changePrice(total,subId,subNumber) {
	var price = document.getElementById("price");
	if(price.value == null || price.value == "" || !isNumber(price.value) || price.value < 0){
		price.focus();
		//alert("请输入正确的价格");
		layer.msg("请输入正确的价格", {icon:2});
		return;
	}
	if(price.value!= null && price.value != "null" ){
		var subId = '${param.subId}';
		$.post(contextPath+"/admin/order/adminChangeSubPrice", {"subId": subId, "totalPrice": price.value},
				function(retData) {
			if(retData == 'OK' ){
				parent.location.reload();
				var index = parent.layer.getFrameIndex('deliverGoods'); //先得到当前iframe层的索引
				parent.layer.close(index); //再执行关闭  
			}else{
				//alert('更新价格失败，订单号 ' + subNumber) ;
				layer.msg('更新价格失败，订单号 ' + subNumber, {icon:2});
			}
		},'json');
	}
}