if(typeof JSON == 'undefined'){$('head').append($("<script type='text/javascript' src="+contextPath+"'/resources/common/js/json.js'>"));}
function brandFloorLoad(curPageNO){
	var name = $("#brandName").val();
	$.ajax({
		url:contextPath+"/admin/floorItem/brandFloorLoad/",
		data:{"curPageNO":curPageNO,"brandName":name},
		type:'post', 
		async : true,  
		error: function(jqXHR, textStatus, errorThrown) {
		},
		success:function(result){
			jQuery("#floor_goods_list").empty().append(result);	
		}
	});
}

jQuery(document).ready(function(){
	jQuery("#floor_goods_info").sortable();
	jQuery("#floor_goods_info").disableSelection();
	brandFloorLoad("");

	$('#brandName').bind('keyup', function(event) {
		if (event.keyCode == "13") {
			//回车执行查询
			$('#brandNameBtn').click();
		}
	});
});

function save_form(){
	var floorMap = [];
	jQuery(".floor_box_pls .floor_pro_name").each(function(){
		var obj = new Object();
		obj.id = jQuery(this).attr("id");
		floorMap.push(obj);
	});

	if(floorMap.length==0){
		layer.alert("请选择品牌信息", {icon:7});
		return;
	}
	var limit="${limit}";
	if(limit!=""){
		if(floorMap.length>limit){
			layer.alert("品牌不能超过"+limit, {icon:2});
			return;
		}
	}
	$("#saveForm").attr("disabled","disabled");
	var BrandFloors = JSON.stringify(floorMap);
	$.ajax({
		url:contextPath+"/admin/floorItem/saveBrandFloor", 
		data: {
			"BrandFloors":BrandFloors,
			"flId":flId,
			"layout":layout
			},
		type:'post', 
		dataType : 'json', 
		async : true, //默认为true 异步   
		error: function(jqXHR, textStatus, errorThrown) {
			$("#saveForm").removeAttr("disabled");
			layer.alert(textStatus, errorThrown);
		},
		success:function(result){
			if(result=="OK"){
				layer.msg('成功！', {icon:1, time:1000}, function(){
					parent.location.reload();
				});
			}else{
				$("#saveForm").removeAttr("disabled");
				layer.alert("添加失败");
				return;
			}
		}
	});  
}

function selectBrandPager(curPageNO){
	brandFloorLoad(curPageNO);
}

function convertResopnse(result){
	var value = "保存成功";
	if("NN" == value){
		value = "数据不允许为空";
	}else if("PN" == value){
		value = "你不能编辑别人的楼层";
	}
	return value;
}