$(function() {

	$("#apkFile").on("change", function(){
		var file = this.files[0];

		if (!/\.APK$/.test(file.name.toUpperCase())) {
			layer.msg("对不起, 请选择APK格式的文件!",{icon:0});
			this.value = null;
		}

		return false;
	});

	$("#beanForm").validate({
		//debug: true,
		errorPlacement: function(error, element) {
			var con = element.parent().find("em");
			if(!con.length){
				con = element.after(error);
			}else{
				error.appendTo(con);
			}
		},
		rules: {
			apkFile: {
				required: true,
			},
			version: {
				required: true,
			},
		},
		messages: {
			apkFile: {
				required: "请选择要上传的APK文件!",
			},
			version: {
				required: "请输入版本号!",
			},
		},
		submitHandler: function(form){
			$.formUtils.ajaxSubmitFormData(form, function(result){
				if(result === "OK"){
					layer.alert("恭喜您,更新成功!",{icon:1}, function(){
						window.parent.parent.location.reload(true);
					});
				}else{
					layer.msg(result,{icon:2});
				}
			});
		}
	}); 
});