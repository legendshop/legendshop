$("#checkedAll").on("click", function() {
	if ($(this).is(":checked")) {
		$("input[type=checkbox][name=messageId]").prop("checked", true);
	} else {
		$("input[type=checkbox][name=messageId]").removeAttr("checked");
	}
});

$("#batch-del").on("click",function() {
			var messageIds = $(".selectOne:checked");
			var messageIdList = new Array();
			messageIds.each(function() {
				var messageId = $(this).val();
				messageIdList.push(messageId);
			});
			var messageStr = messageIdList.toString();
			if (messageStr == "") {
				layer.msg("请选择要删除的系统通知记录。",{icon: 0});
				return;
			}
			jQuery
			.ajax({
				url : contextPath+"/admin/system/Message/batchDelete/"
					+ messageStr,
					type : 'DELETE',
					async : false, //默认为true 异步  
					dataType : 'json',
					error : function(data) {
					},
					success : function(data) {
						if (data == "OK") {
							layer.msg("批量删除成功。",{icon: 1});
							setInterval(
									function() {
										location.href = contextPath+"/admin/system/Message/query";
									}, 1000);
						} else {
							layer.msg("批量删除失败。",{icon: 2});
						}
					}
			});
		});

function deleteById(id) {
	layer.confirm("确定删除吗 ?", {
		icon: 3
		,btn: ['确定','关闭'] //按钮
	}, function(){
		window.location = contextPath+"/admin/system/Message/delete/" + id;
	});
}

function pager(curPageNO) {
	document.getElementById("curPageNO").value = curPageNO;
	document.getElementById("form1").submit();
}
function search() {
	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	$("#form1")[0].submit();
}