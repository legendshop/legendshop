function confirmUpload(){
	var path=$("#file").val();
	var filename=path.split(".")[1];
	var data='1';
	if(path.length>0){
		if(data=="1"){
			if(filename=="xls"||filename=="xlsx"){
				ajaxSubmitForm();
			}else{
				layer.msg("请选择excel文件",{icon:0});
			}
		}else{
			if(filename=="csv"){
				ajaxSubmitForm();
			}else{
				layer.msg("请选择CVS文件",{icon:0});
			}
		}
	}else{
		layer.msg("请选择文件",{icon:0});
	}
}

function ajaxSubmitForm(url){
	var data = $("#form1").serialize();
	var formData = new FormData($("#form1")[0]);
	var url = contextPath+"/admin/coupon/sendCoupon";
	$.ajax({
		url: url,
		data: formData,
		type: "post",
		dataType: "json",
		processData: false,
		contentType: false,
		error: function() {
			layer.alert("网络延迟，请稍后再试",{icon:2});
		},
		success: function(result) {
			if(result.b == "Y") {
				layer.msg("导入成功",{icon:1},function(){
					parent.location.reload();
				});
			} else {
				layer.msg(result.msg,{icon:2});
			}
		}
	});
}

function downloadTemplet(){
  window.location.href=contextPath+"/resources/templets/prodExecl/couponSend.xlsx"
}
