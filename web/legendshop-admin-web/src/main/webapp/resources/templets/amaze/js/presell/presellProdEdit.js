//记住当前表单是否已提交过了	
var isSubmitted = false;

	jQuery.validator.addMethod("isNumber", function(value, element) {       
	         return this.optional(element) || /^[-\+]?\d+$/.test(value) || /^[-\+]?\d+(\.\d+)?$/.test(value);       
	    }, "必须整数或小数");  
	
	jQuery.validator.addMethod("isValidMoney", function(value, element) { 
	         value = parseFloat(value);      
	         return this.optional(element) || value> 0;       
	    }, "金额必须要大于0元"); 
	    
	//校验方案名是否已存在
	$.validator.addMethod("checkVal",function(value,element){
			var config = true;
				if(value == schemeName){
					return config;
				}else{
					$.ajax({
						url: contextPath + "/admin/presellProd/isSchemeNameExist",
						async:false,
						data:{"schemeName":value},
						dataType: "json",
						success:function(data){
							if(!data)
								config=false;
						}
					});
					return config;
				}
		},"该方案名已被使用,请换一个!");
		
	    $(document).ready(function() {
	    	
	    	//支付设置更改时
	    	$("#form1 input[name='payPctType']").change(function(){
	    		if($(this).val() == "1"){
	    			if($("#preSaleEnd").val()){
	    				$("#depositPay").show("normal",function(){
		    				$("#payPct").removeAttr("disabled");
		    				$("#preDepositPrice").removeAttr("disabled");
		    				$("#finalMStart").removeAttr("disabled");
		    				$("#finalMEnd").removeAttr("disabled");
		    				//生成尾款支付时间
		    				generateFinalMTime();
		    			});
	    			}else{
	    				document.getElementById("payPctType-1").checked = false;
		    			document.getElementById("payPctType-0").checked = true;
	    				layer.msg("对不起,请先填写预售时间!", {icon:0});
	    			}
	    		}else{
	    			$("#depositPay").hide("normal",function(){
	    				$("#payPct").attr("disabled","disabled");
	    				$("#preDepositPrice").attr("disabled","disabled");
	    				$("#finalMStart").attr("disabled","disabled");
	    				$("#finalMEnd").attr("disabled","disabled");
	    			});
	    		}
	    		//重置发货时间
	    		if($(this).val() == oldPayPacType){
	    			$("#preDeliveryTimeStr").val(oldDeliveryTime);
	    		}else{
	    			$("#preDeliveryTimeStr").val("");
	    		}
	    	});
	    	
	    	//预售结束时间获取焦点时
	    	$("#form1 input[name='preSaleEnd']").focus(function(){
	    		if($("#preSaleStart").val()){
		    		WdatePicker({dateFmt:'yyyy-MM-dd HH:mm',minDate:'#F{$dp.$D(\'preSaleStart\',{d:1})}'});
	    		}else{
	    			layer.msg("对不起,请先填写预售开始时间!", {icon:0});
	    		}
	    	});
	    	
	    	//预售结束时间更改时
	    	$("#form1 input[name='preSaleEnd']").change(function(){
	    		//重置发货时间
	    		$("#preDeliveryTimeStr").val("");
	    		//生成尾款时间
	    		var payPctType = $("input[name='payPctType']:checked").val();
				if(preSaleEnd && payPctType == "1"){
	    			generateFinalMTime();
	    		}
	    	});
	    	
	    	//当发货时间获取焦点时
	    	$("#form1 input[name='preDeliveryTimeStr']").focus(function(){
	    		if($("#preSaleStart").val() && $("#preSaleEnd").val()){
		    		var minDate = minDeliveryDate();
		    		WdatePicker({dateFmt:"yyyy-MM-dd",minDate:minDate});
	    		}else{
	    			layer.msg("对不起,请先填写预售时间!", {icon:0});
	    		}
	    	});
	    	
	    	//校验表单数据
		    jQuery("#form1").validate({
		    		ignore: ".ignore",
		            rules: {
			            schemeName: {
			                required: true,
			                maxlength : 50,
			                checkVal:true
			            },
			            prePrice: {
			                required: true,
			                isNumber : true,
			                isValidMoney:true,
			                max : 9999999999
			            },
			            preSaleStart: {
			                required: true,
			            },
			            preSaleEnd: {
			                required: true,
			            },
			            payPctType: {
			                required: true,
			            },
			            preDepositPrice: {
			                required: true,
			                isNumber:true,
			                isValidMoney:true,
			                max:9999999999
			            },
			            finalMStart:{
			                required: true
			            },
			            finalMEnd:{
			            	required: true,
			            },
			            preDeliveryTimeStr:{
			            	required: true,
			            }
		        },
		        messages: {
		           		schemeName: {
			                required : "请输入方案名称!",
			                maxlength : "方案名长度不能大于50个字符!"
			            },
			            prePrice: {
			                required: "请输入预售价格!",
			                max : "金额大小不能超过9999999999!",
			                isNumber: "请输入正确的金额!",
			                isValidMoney: "金额必须要大于0元!"
			            },
			            preSaleStart: {
			                required: "请输入预售开始时间!",
			            },
			            preSaleEnd: {
			                required: "请输入预售结束时间!",
			            },
			            payPctType: {
			                required: "请选择支付类型!",
			            },
			            preDepositPrice: {
			                required: "请输入定金!",
			                max : "金额大小不能超过9999999999!",
			                isNumber: "请输入正确的金额!",
			                isValidMoney: "金额必须要大于0元!",
			            },
			            finalMStart:{
			                required: "请输入尾款支付开始时间!"
			            },
			            finalMEnd:{
			            	required: "请输入尾款支付结束时间!",
			            },
			            preDeliveryTimeStr:{
			            	required: "请输入发货时间!",
			            }
		        },
		        submitHandler : function(form){
		        	if(isBlank($("#prodId").val()) || isBlank($("#skuId").val())){
		        		layer.msg("请选择参与预售的商品后再提交!", {icon:0});
		        		return false;
		        	}
		        	
		        	//校验预售时间与发货时间
		        	if(!validationPreDate() || !validationDeliveryDate()){
		        		return false;
		        	}

		        	//校验用户选择的sku
		        	var presellId = $("#id").val();
		        	var currProdId = $("#prodId").val();
		        	var currSkuId = $("#skuId").val();
		      		if(isProdSkuExist(presellId,currProdId,currSkuId)){
		      			layer.alert("对不起,该SKU已参与预售了，不能重复添加!</br>如果是存在之前已完成、已过期、已终止的预售活动中,请删除后再添加!", {icon:0});
		      			return false;
		      		}
		        	
		        	if(!isSubmitted){
		        		form.submit();
		        		isSubmitted = true;
		        	}else{
		        		layer.alert("对不起,请不要重复提交表单!",  {icon:0});
		        	}
		        } 
		    });
	});
	    
	//计算定金占预售价格百分比
	function calculatePayPct(){
		var prePrice = parseFloat($("#prePrice").val());
		var preDepositPrice = parseFloat($("#preDepositPrice").val());
		if(prePrice && preDepositPrice){
			var payPct = preDepositPrice/prePrice;
			$("#payPct").val(payPct);
			$("#payPctSpan").text((payPct*100).toFixed(2));
		}
	}
	
	//生成尾款时间
	function generateFinalMTime(){
		var preSaleEnd = $("#preSaleEnd").val();
		if(preSaleEnd){
			$("#finalMStart").val(preSaleEnd);
			var finalMStart = $("#finalMStart").val();
			var finalMEnd = new Date(finalMStart.replace(/-/g,"/"));
			//尾款支付结束时间等于从尾款支付开始时间的3天后的第一个24点
			finalMEnd.setDate(finalMEnd.getDate() + 4);
			var formatFinalMEnd = finalMEnd.getFullYear() + "-" + (finalMEnd.getMonth()+1) + "-" + finalMEnd.getDate() + " " + "00" + ":" + "00";
			$("#finalMEnd").val(formatFinalMEnd);
		}
	}
	
	//最小发货日期
	function minDeliveryDate(){
		var payPctType = $("input[name='payPctType']:checked").val();
		var minDate = null;
		var date = null;
		if(payPctType == "0"){
			date = new Date($("#preSaleEnd").val().replace(/-/g,"/"));
		}else{
			date = new Date($("#finalMEnd").val().replace(/-/g,"/"));
		}
		date.setDate(date.getDate()+1);
		minDate = date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate() + " " + "00" + ":" + "00";
		return minDate;
	}
	
	//校验预售时间
	function validationPreDate(){
		var currTime = new Date();
		var startTime = new Date($("#preSaleStart").val().replace(/-/g,"/"));
		var endTime = new Date($("#preSaleEnd").val().replace(/-/g,"/"));
		
		//开始时间不能小于等于当前时间 
		if(startTime.getTime() <= currTime.getTime()){
			layer.alert("预售的开始时间不能小于当前时间!", {icon:0});
			return false;
		}
		
		//开始时间与结束时间必须相隔24小时,24小时*3600秒*1000=24小时的毫秒数
		if((endTime.getTime() - startTime.getTime())<=(24*3600*1000)){
			layer.alert("预售的开始时间与结束时间必须相隔24小时(不包含)以上!",  {icon:0});
			return false;
		}
		
		return true;
	}

	//校验发货时间
	function validationDeliveryDate(){
		var payPctType = $("input[name='payPctType']:checked").val();
		var preDeliveryTime = new Date($("#preDeliveryTimeStr").val().replace(/-/g,"/"));
		var preSaleEnd = new Date($("#preSaleEnd").val().replace(/-/g,"/"));
		var finalMEnd = new Date($("#finalMEnd").val().replace(/-/g,"/"));
		var firstTwentyFourPoint = null;
		var errorMsg = null;
		
		//如果是全额支付,发货时间要晚于预售结束时间后的第一个24点
		if(payPctType == "0"){
			preSaleEnd.setDate(preSaleEnd.getDate()+1);
			preSaleEnd.setHours(0);
			preSaleEnd.setMinutes(0);
			preSaleEnd.setSeconds(0);
			preSaleEnd.setMilliseconds(0);
			firstTwentyFourPoint = preSaleEnd;//预售结束时间后第一个24点的日期
			errorMsg = "发货时间要晚于预售结束时间24小时!";
		}else if(payPctType == "1"){//如果是全额支付,发货时间要晚于尾款结束时间后的第一个24点
			finalMEnd.setDate(finalMEnd.getDate()+1);
			finalMEnd.setHours(0);
			finalMEnd.setMinutes(0);
			finalMEnd.setSeconds(0);
			finalMEnd.setMilliseconds(0);
			firstTwentyFourPoint = finalMEnd;//尾款结束时间后第一个24点的日期
			errorMsg = "发货时间要晚于尾款结束时间后的第一个24点!";
		}else{
			layer.alert("对不起,请不要非法操作!", {icon:2});
			return false;
		}
		
		if((preDeliveryTime.getTime() - firstTwentyFourPoint.getTime()) < 0){
			layer.alert(errorMsg,  {icon:2});
			return false;
		}
		
		return true;
	}
	
	//判断用户当前选中的sku是否是已参与预售
    function isProdSkuExist(presellId,prodId,skuId){
    	var b = false;
    	$.ajax({
    		url : contextPath + "/admin/presellProd/isProdSkuExist",
    		data : {
    			"presellId" : presellId,
    			"prodId" : prodId,
    			"skuId" : skuId
    		},
    		async : false, 
    		dataType : "JSON",
    		success : function(result){
    			b = result;
    		}
    	});
    	return b;
    }
	
	//显示添加商品窗口
	function showProdlist(){
		layer.open({
			title: '选择商品',
			id: 'prodSkuLayout',
			type: 2,
			content: contextPath + "/admin/presellProd/addProdSkuLayout",
			area: ['590px', '420px']
		});
	}
	
	function addProdTo(prodId,skuId,skuName,skuPrice,cnProperties){
			$(".prod-table").css("display","");
			$("#prodId").val(prodId);
			$("#skuId").val(skuId);
			$("#skuName").text(skuName);
			$("#skuName").attr("href",contextPath + "/views/" + prodId);
			$("#skuPrice").text(skuPrice);
			if(isBlank(cnProperties)){
				$("#cnProperties").text("无");
			}else{
				$("#cnProperties").text(cnProperties);
			}
	}
	
	function isBlank(value){
		return value == undefined || value == null || $.trim(value) == "";
	}