$(function () {
  //初始化对象
  var chat = {};
  var currentUserId;//当前聊天的用户ID
  var MsgType = {TEXT: 0, IMAGE: 1, PROD: 5, ORDER: 6, TIME: 7, READ: 8}//消息类型;(如：0:text、1:image、2:voice、3:vedio、4:music、5:prod 6:order 7:时间消息)
  var ChatResp = {SUCCESS: 10000, OFFLINE: 10001};//发送响应状态,SUCCESS成功，OFFLINE离线
  var EMOJI_MAP = {
    "U1F401": '<img key="' + 'U1F401" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_1.png">',
    "U1F402": '<img key="' + 'U1F402" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_2.png">',
    "U1F403": '<img key="' + 'U1F403" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_3.png">',
    "U1F404": '<img key="' + 'U1F404" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_4.png">',
    "U1F405": '<img key="' + 'U1F405" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_5.png">',
    "U1F406": '<img key="' + 'U1F406" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_6.png">',
    "U1F407": '<img key="' + 'U1F407" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_7.png">',
    "U1F408": '<img key="' + 'U1F408" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_8.png">',
    "U1F409": '<img key="' + 'U1F409" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_9.png">',
    "U1F410": '<img key="' + 'U1F410" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_10.png">',
    "U1F411": '<img key="' + 'U1F411" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_11.png">',
    "U1F412": '<img key="' + 'U1F412" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_12.png">',
    "U1F413": '<img key="' + 'U1F413" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_13.png">',
    "U1F414": '<img key="' + 'U1F414" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_14.png">',
    "U1F415": '<img key="' + 'U1F415" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_15.png">',
    "U1F416": '<img key="' + 'U1F416" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_16.png">',
    "U1F417": '<img key="' + 'U1F417" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_17.png">',
    "U1F418": '<img key="' + 'U1F418" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_18.png">',
    "U1F419": '<img key="' + 'U1F419" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_19.png">',
    "U1F420": '<img key="' + 'U1F420" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_20.png">',
    "U1F421": '<img key="' + 'U1F421" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_21.png">',
    "U1F422": '<img key="' + 'U1F422" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_22.png">',
    "U1F423": '<img key="' + 'U1F423" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_23.png">',
    "U1F424": '<img key="' + 'U1F424" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_24.png">',
    "U1F425": '<img key="' + 'U1F425" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_25.png">',
    "U1F426": '<img key="' + 'U1F426" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_26.png">',
    "U1F427": '<img key="' + 'U1F427" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_27.png">',
    "U1F428": '<img key="' + 'U1F428" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_28.png">',
    "U1F429": '<img key="' + 'U1F429" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_29.png">',
    "U1F430": '<img key="' + 'U1F430" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_30.png">',
    "U1F431": '<img key="' + 'U1F431" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_31.png">',
    "U1F432": '<img key="' + 'U1F432" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_32.png">',
    "U1F433": '<img key="' + 'U1F433" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_33.png">',
    "U1F434": '<img key="' + 'U1F434" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_34.png">',
    "U1F435": '<img key="' + 'U1F435" ' + 'src="' + contextPath + '/resources/templets/amaze/images/faces/ee_35.png">'
  };

  //处理事件函数
  chat.toggle = function (ele, cleName, fn) {
    $(document).on(ele, cleName, function (e) {
      fn(e)
    })
  };

  //初始化webSocket
  var initWebSocket = function () {
    //判断当前浏览器是否支持WebSocket
    if ('WebSocket' in window) {
      websocket = new WebSocket(IM_BIND_ADDRESS);
    } else {
      layerMsg("当前浏览器 Not support websocket");
    }

    /*
     * websocket.readyState
   * readyState表示连接有四种状态：
   * CONNECTING (0)：表示还没建立连接
   * OPEN (1)： 已经建立连接，可以进行通讯
   * CLOSING (2)：通过关闭握手，正在关闭连接
   * CLOSED (3)：连接已经关闭或无法打开
   */
    //连接成功建立的回调方法
    websocket.onopen = function (event) {
      heartCheck.reset().start();//心跳检测重置
      console.log("WebSocket连接成功,状态：" + websocket.readyState + "  连接成功时间：" + new Date());
      chat.login(11, customId, name, shopId, sign);
    }

    //连接发生错误的回调方法
    websocket.onerror = function (event) {
      console.log("WebSocket连接发生错误,状态：" + websocket.readyState);
      reconnect();
      layerMsg("WebSocket连接发生错误");
    };

    //接收到消息的回调方法
    websocket.onmessage = function (event) {
      heartCheck.reset().start(); //拿到任何消息都说明当前连接是正常的
      var _object = $.parseJSON(event.data);
      console.log(event.data);
      //登录失败响应
      if (_object.command == "COMMAND_UNKNOW") {
        layerMsg(_object.data);//无法登录,密钥信息有误
        return;
      }

      //登录成功响应
      if (_object.command == "COMMAND_LOGIN_RESP") {
        if (!isBlank(_object.data.userDetail) && !isBlank(_object.data.userDetail.userId)) {//有新用户加入访问,组建联系人列表
          var respUserId = _object.data.userDetail.userId;//响应UserId
          var respNickName = _object.data.userDetail.nickName;//响应昵称
          var respUserMobile = _object.data.userDetail.userMobile;//响应电话
          if ($(".shop-list a.list-item[data-userId='" + respUserId + "']").length > 0) {//用户已经存在
            var respConsultId = _object.data.consultId;
            var respConsultType = _object.data.consultType;
            var _data = $(".right-sidebar").data("imConsult");
            //如果该用户的咨询商品改变了
            if (respUserId == currentUserId && (respConsultId != $(_data).find("div.list-item").attr("data-skuId")
              || respConsultType != $(_data).find("div.list-item").attr("data-type"))) {
              chat.getInitConsultProd(respUserId, respNickName, respUserMobile, 1000);
            }
            return;
          }
          console.log("COMMAND_LOGIN_RESP==>有新用户加入访问");
          var userNickName = isBlank(respNickName) ? respUserMobile : respNickName;
          var isCurrent = '<a href="javascript:void(0)" class="list-item item-stage" data-userId="' + respUserId + '" data-mobile="' + respUserMobile + '">';
          if ($(".shop-list").find("a.list-item").length == 0) {
            isCurrent = '<a href="javascript:void(0)" class="list-item item-stage current" data-userId="' + respUserId + '" data-mobile="' + respUserMobile + '">';
          }
          var _pic = '<img src="' + contextPath + '/resources/templets/amaze/images/head-default.jpg" class="left-img">';
          if (!isBlank(_object.data.userDetail.portraitPic)) {
            _pic = '<img src="' + imagesPrefix + _object.data.userDetail.portraitPic + '" class="left-img">';
          }
          var _shopList = isCurrent + '<span class="s-img">' +
            '<em class="shop-txt">用户</em>' + _pic +
            '</span>' +
            '<span class="s-info">' +
            '<span class="s-name">' + userNickName + '</span>' +
            '<span class="s-tip clear">' +
            '<span class="tip-txt"></span>' +
            '<span class="tip-time"></span>' +
            '</span>' +
            '</span>' +
            '</a>';
          if ($(".shop-list").find("a.list-item").length > 0) {
            $(".shop-list").find("a.list-item:eq(0)").before(_shopList);
          } else {//开始联系人列表为空
            $(".shop-list").append(_shopList);
            currentUserId = respUserId;//当前聊天的用户ID，切换聊天窗口值改变
            $(".currentName").html(isBlank(respNickName) ? respUserMobile : respNickName);
            chat.getInitConsultProd(respUserId, respNickName, respUserMobile, 1000);
          }
          var number = parseInt($(".shop-list").find(".list-tit em.num span").text()) + 1;
          $(".shop-list").find(".list-tit em.num span").text(number);
          return;
        }

        console.log("COMMAND_LOGIN_RESP==>登录成功");
        var _groups = _object.data.custom.users;//联系人
        var _shopList = "";
        for (var i = 0, len = _groups.length; i < len; i++) {
          var isCurrent = '<a href="javascript:void(0)" class="list-item item-stage" data-userId="' + _groups[i].userId + '" data-mobile="' + _groups[i].userMobile + '">';
          var userNickName = isBlank(_groups[i].nickName) ? _groups[i].userMobile : _groups[i].nickName;
          if (i == 0) {
            $(".currentName").html(userNickName);
            currentUserId = _groups[i].userId;//当前聊天的用户ID，切换聊天窗口值改变
            isCurrent = '<a href="javascript:void(0)" class="list-item item-stage current" data-userId="' + _groups[i].userId + '" data-mobile="' + _groups[i].userMobile + '">';
            chat.getChatRecord();//当前客服==》获取聊天记录
            chat.init(_groups[i]);//初始化==》正在咨询
          }
          //默认头像，须和前台保持一致
          var _pic = '<img src="' + contextPath + '/resources/templets/amaze/images/head-default.jpg" class="left-img">';
          if (!isBlank(_groups[i].userPic)) {
            _pic = '<img src="' + imagesPrefix + _groups[i].userPic + '" class="left-img">';
          }

          var _num = "";
          if (i != 0 && _groups[i].unReadMsg > 0) {//初始化未读消息数，第一条不显示
            _num = '<i class="num">' + _groups[i].unReadMsg + '</i>';
          }

          _shopList = _shopList + isCurrent + '<span class="s-img">' +
            '<em class="shop-txt">用户</em>' + _pic +
            '</span>' +
            '<span class="s-info">' +
            '<span class="s-name">' + userNickName + '</span>' +
            '<span class="s-tip clear">' +
            '<span class="tip-txt"></span>' +
            '<span class="tip-time"></span>' +
            '</span>' +
            '</span>' + _num +
            '</a>';
        }
        $(".shop-list").find(".list-tit em.num span").text(_groups.length);
        $(".shop-list").append(_shopList);
      }

      //接收到文本信息
      if (_object.command == "COMMAND_CHAT_REQ") {
        console.log(_object);
        var _received = _object.data;
        if (isBlank(_received) || _received.length == 0) {
          return;
        }
        if (isBlank(_received.content)) {
          return;
        }

        if (currentUserId == _received.from) {//当前聊天客服ID==发送信息的客服ID
          chat.receiveText(_received.msgType, _received.content);
        } else {//未读消息
          var num = $(".shop-list a.list-item[data-userId='" + _received.from + "']").find("i.num");
          if (num.length == 0) {
            $(".shop-list a.list-item[data-userId='" + _received.from + "']").append('<i class="num">1</i>');
          } else {
            var number = parseInt(num.text()) + 1;
            num.text(number);
          }
          if (_received.msgType == 0) {//文本消息才给出提示
            var sendTime = fmtDate(_received.createTime);
            $(".shop-list a.list-item[data-userId='" + _received.from + "']").find(".s-tip .tip-txt").html(parseEmoji(_received.content, EMOJI_MAP));
            $(".shop-list a.list-item[data-userId='" + _received.from + "']").find(".s-tip .tip-time").text(sendTime);
          }
          //放到最前面
          var $clonedCopy = $(".shop-list a.list-item[data-userId='" + _received.from + "']").clone();
          $(".shop-list a.list-item[data-userId='" + _received.from + "']").remove();
          $(".shop-list").find("a.list-item:eq(0)").before($clonedCopy);
        }
      }

      //发送信息反馈，用户不在线
      if (_object.command == "COMMAND_CHAT_RESP" && _object.code == ChatResp.OFFLINE) {
        debugger;
        // layerMsg("发送失败," + _object.msg);
        $(".chat-list").find("div.chat-txt:last").find("i.dia-arrow").after('<span class="un-read">未读</span>');
        return;
      }

    }

    //连接关闭的回调方法
    websocket.onclose = function () {
      console.log("WebSocket连接关闭,状态：" + websocket.readyState);
      console.log("连接关闭时间：" + new Date());
      //window.location.reload(true);
      reconnect();
    }

    //监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
    window.onbeforeunload = function () {
      console.log("WebSocket连接关闭,状态：" + websocket.readyState);
      websocket.close();
    }
  }

  //初始化webSocket
  initWebSocket();

  //init==>登录成功,初始化咨询数据
  chat.init = function (_userObj) {
    var _consultObj = $.parseJSON(_userObj.consultContent);
    var _nowList = '<div class="no-record">' +
      '<i class="rec-i"></i>' +
      '<p class="rec-p">暂无数据</p>' +
      '</div>';
    if (!isBlank(_consultObj)) {
      if (_consultObj.consultType == "prod") {
        if (isBlank(_consultObj.skuDetail)) {
          return;
        }
        var _prodPic = isBlank(_consultObj.skuDetail.skuPic) ? _consultObj.skuDetail.prodPic : _consultObj.skuDetail.skuPic;
        var _prodName = isBlank(_consultObj.skuDetail.skuName) ? _consultObj.skuDetail.prodName : _consultObj.skuDetail.skuName;
        var _cnProperties = isBlank(_consultObj.skuDetail.cnProperties) ? "" : _consultObj.skuDetail.cnProperties;
        _nowList = '<div class="list-item" data-type="' + _consultObj.consultType + '" data-skuId="' + _consultObj.skuDetail.skuId + '">' +
          '<div class="pro clear">' +
          '<div class="pro-img">' +
          '<a href="' + contextPath + '/views/' + _consultObj.skuDetail.prodId + '" style="cursor: pointer;" target="_blank">' +
          '<img src="' + imagesPrefix + _prodPic + '">' +
          '</a>' +
          '</div>' +
          '<div class="pro-txt">' +
          '<p class="name">' + _prodName + '</p>' + _cnProperties +
          '<p class="price">' + formatMoney(_consultObj.skuDetail.skuPrice) + '</p>' +
          '</div>' +
          '</div>' +
          '</div>';
      }
      if (_consultObj.consultType == "order") {
        if (isBlank(_consultObj.subRefundReturn)) {
          return;
        }
        var _prodPic = isBlank(_consultObj.subRefundReturn.productImage) ? contextPath + "/resources/templets/amaze/images/head-default.jpg" : imagesPrefix + _consultObj.subRefundReturn.productImage;
        var _prodName = _consultObj.subRefundReturn.productName;
        _nowList = '<div class="list-item" data-type="' + _consultObj.consultType + '">' +
          '<div class="pro clear">' +
          '<div class="pro-img">' +
          '<a href="javascript:void(0)" style="cursor: pointer;" target="_blank">' +
          '<img src="' + _prodPic + '">' +
          '</a>' +
          '</div>' +
          '<div class="pro-txt">' +
          '<p class="name" style="margin:0;">' + _prodName + '</p>' +
          '<p class="name" style="margin:3px 0 0;">退款编号:' + _consultObj.subRefundReturn.refundSn + '</p>' +
          '<p class="price" style="margin:3px 0 0;">' + formatMoney(_consultObj.subRefundReturn.refundAmount) + '</p>' +
          '</div>' +
          '</div>' +
          '</div>';
      }
    }

    var _userNickName = isBlank(_userObj.nickName) ? "暂未设置" : _userObj.nickName;
    var rightContext = '<div class="my-asking">' +
      '<div class="now-list">' + _nowList + '</div>' +
      '<div class="oth-list">' +
      '<div class="user-msg">' +
      '<h4 class="inf">用户信息</h4>' +
      '<p class="nam">用户昵称：<em class="nam-txt">' + _userNickName + '</em></p>' +
      '<p class="nam">用户电话：<em class="nam-txt">' + _userObj.userMobile + '</em></p>' +
      '</div>' +
      '</div>' +
      '</div>';
    $(".rightContext").html(rightContext);

    //页面进来的时候应当存好data,正在咨询放在缓存中data
    $(".right-sidebar").data("imConsult", $(".rightContext").html());
  }

  //异步初始化咨询商品列表
  chat.getInitConsultProd = function (userId, nickName, userMobile, time) {
    setTimeout(function () {//不能省略
      $.ajax({
        url: contextPath + "/admin/customer/getConsultProd",
        data: {"userId": userId},
        type: "post",
        dataType: "json",
        error: function () {
          console.log("网络延迟，请稍后再试");
        },
        success: function (result) {
          var _userObj = {
            consultContent: result,
            nickName: nickName,
            userMobile: userMobile
          }
          chat.init(_userObj);//初始化==》正在咨询
          $(".sid-tab .tab-item").removeClass("current");
          $(".sid-tab .tab-item:eq(0)").addClass("current");
        }
      });
    }, time);
  }

  //登录
  chat.login = function (cmd, customId, name, shopId, sign) {
    var options = {
      cmd: cmd,
      customId: customId,
      name: name,
      shopId: shopId,
      sign: sign,
    };
    websocket.send(JSON.stringify(options));
  }

  //滑动==》将信息滑动到底部
  chat.scrollTop = function () {
    var scrollHeight = $('.chat-list').prop("scrollHeight");
    $('.chat-list').animate({scrollTop: scrollHeight}, 600);
  }

  //发送消息
  chat.send = function (context, msgType) {
    var createTime = new Date().getTime();
    var msg = {
      cmd: 7,
      chatType: 2,
      msgType: msgType,
      content: context,
      createTime: createTime,
      from: customId,
      to: currentUserId, //当前聊天窗口的用户ID
      customChat: true, //是否客服发送
      shopId: shopId
    };

    // 已读未读
    if (msgType == MsgType.READ && websocket.readyState == WebSocket.OPEN) { //每次发送前应判断连接状态==》OPEN (1)： 已经建立连接，可以进行通讯
      websocket.send(JSON.stringify(msg));
      return;
    }

    var lastTime = $('.chat-list').find(".chat-txt").last().attr("date");
    if (createTime - lastTime >= 300000) { //300000时间戳,以五分钟一个跨度显示时间
      console.log("超过五分钟......");
      msg.msgType = MsgType.TIME;
      msg.content = createTime;
      if (websocket.readyState == WebSocket.OPEN) { //每次发送前应判断连接状态==》OPEN (1)： 已经建立连接，可以进行通讯
        websocket.send(JSON.stringify(msg));
      }
      $(".chat-list").append('<div class="chat-time" data="' + createTime + '"><p class="time">' + new Date(createTime).toLocaleString() + '</p></div>');
      msg.msgType = msgType;
      msg.content = context;
    }


    if (websocket.readyState == WebSocket.OPEN) { //每次发送前应判断连接状态==》OPEN (1)： 已经建立连接，可以进行通讯
      websocket.send(JSON.stringify(msg));
      return;
    }
    console.log("连接中断...暂不予处理...");
    if (msgType != MsgType.READ) {
      // $(".chat-list").children("div:last-child").find(".dia").append("<span class=\"dia-err\"></span>");
      // debugger;
      // $(".chat-list").children("div:last-child").find(".dia-arrow").after('<span class="dia-err"></span>')
      // layerMsg("连接中断,请求稍后重试!")

      setTimeout(function () {
        // $(".chat-list").children("div:last-child").find(".dia-arrow").after('<span class="dia-err"></span>')
        layerMsg("连接中断,请刷新后重试!")
      }, 500);


    }

  }

  //发送消息==》回显页面信息
  chat.setMessageInnerHTML = function (context) {
    $("#context").text("");//清空输入框信息
    var _sendText = getSendText(context, MsgStatus.SUCCESS, new Date().getTime(), EMOJI_MAP);
    $(".chat-list").append(_sendText);
    chat.scrollTop();
  }


  //接收消息==》回显文本信息==》根据消息类型决定消息展示形式
  chat.receiveText = function (msgType, valuestr) {
    var currentName = $(".currentName").text();
    var _receiveText = "";
    var createTime = new Date().getTime();
    switch (msgType) {
      case MsgType.TEXT: //文本消息
        _receiveText = getReceiveText(currentName, valuestr, createTime, EMOJI_MAP);
        break;
      case MsgType.IMAGE: //图片消息
        _receiveText = getReceivePicText(currentName, valuestr, createTime);
        break;
      case MsgType.PROD://咨询商品、浏览商品
        var _prodObj = $.parseJSON(valuestr);
        _receiveText = getReceiveProdText(currentName, _prodObj, createTime);
        break;
      case MsgType.ORDER://订单
        var _orderObj = $.parseJSON(valuestr);
        _receiveText = getReceiveOrderText(currentName, _orderObj, createTime);
        break;
      case MsgType.TIME://时间
        _receiveText = getReceiveTimeText(createTime);
        break;
      case MsgType.READ:// 对方已读
        // 更新已读消息 但是不显示
        debugger;
        // var arr = JSON.parse(valuestr);
        // $.each(arr, function (e, i, a) {
        //   debugger;
        //   $("#_chat_id_" + i).remove();
        // });
        // $(".dia-err").remove();
        // if (shopId == )
        $(".un-read").text("已读")
        // $(".un-read").remove();
        break;
    }
    $(".chat-list").append(_receiveText);
    chat.scrollTop();
  }

  //获取聊天记录==》参数==》fromUserId:发送用户id;userId:接收用户id;
  chat.getChatRecord = function () {
    offset = 0; //初始化分页偏移量
    count = 10; //初始化聊天记录展示记录条数
    $.ajax({
      url: contextPath + "/admin/customer/user/message",
      data: {"fromUserId": currentUserId, "userId": customId, "offset": offset, "count": count},
      type: "POST",
      dataType: "json",
      error: function () {
        console.log("网络延迟，请稍后再试");
      },
      success: function (result) {
        console.log("聊天记录");
        console.log(result);
        debugger;
        var _html = chat.getChatRecordHtml(result);
        $(".chat-list").append(_html);
        if ($(".chat-list").prop("scrollHeight") > $(".chat-list").prop("clientHeight")) {  //垂直滚动条
          $(".chat-list").find("div.chat-txt:eq(0)").before('<div class="chat-more"><a href="javascript:void(0)" class="more">点击加载更多</a></div>');
        }
        $('.chat-list').scrollTop($('.chat-list').prop("scrollHeight"), 200);
      }
    });
  }

  //获取聊天记录==》查询更多聊天信息
  chat.toggle("click", ".more", function (e) {// 如：0-5,6-11,12-17
    var temp = count - offset;
    offset = count ;
    count = count + temp + 1;
    $.ajax({
      url: contextPath + "/admin/customer/user/message",
      data: {"fromUserId": currentUserId, "userId": customId, "offset": offset, "count": count},
      type: "post",
      dataType: "json",
      error: function () {
        console.log("网络延迟，请稍后再试");
      },
      success: function (result) {
        console.log(result);
        if (isBlank(result)) {
          $('.chat-list').find(".chat-more").html('<span class="txt">没有更多了</span>');
          return;
        }
        var _html = chat.getChatRecordHtml(result);
        $(".chat-list").find("div.chat-txt:eq(0)").before(_html);
        if (result.length < (10)) {//返回条数不足
          $('.chat-list').find(".chat-more").html('<span class="txt">没有更多了</span>');
        }
      }
    });
  })

  //获取聊天记录HTML
  chat.getChatRecordHtml = function (result) {
    var _html = "";
    var _name = $("span.u-name").text();//昵称
    var currentName = $(".currentName").text();//用户名称
    var unReadIds = [];
    for (var i = 0, len = result.length; i < len; i++) {
      var _content = result[len - i - 1].content;//消息内容
      var _msgType = result[len - i - 1].msgType;//消息类型
      var status = result[len - i - 1].status;//消息状态
      var createTime = result[len - i - 1].createTime;
      var id = result[len - i - 1].id;
      if (result[len - i - 1].from == customId) {//发送人,右侧
        switch (_msgType) {
          case MsgType.TEXT: //文本消息
            _html = _html + getSendText(_content, status, createTime, EMOJI_MAP);
            break;
          case MsgType.IMAGE: //图片消息
            _html = _html + getSendPicText(_content, status, createTime,id,_name);
            break;
          // case MsgType.PROD://商品消息
          //   var _prodObj = $.parseJSON(_content);
          //   _html = _html + getSendProdText(_prodObj, status, createTime);
          //   break;
          // case MsgType.ORDER://订单消息
          //   var _orderObj = $.parseJSON(_content);
          //   _html = _html + getSendOrderText(_orderObj, status, createTime);
          //   break;
          case MsgType.TIME://时间消息
            _html = _html + getReceiveTimeText(createTime);
            break;
        }
      } else {//接收人,左侧
        if (status == MsgStatus.FAIL) {
          // 发送已读消息
          // 收集起来 然后批量发送
          unReadIds.push(result[len - i - 1].id);
        }
        switch (_msgType) {
          case MsgType.TEXT: //文本消息
            _html = _html + getReceiveText(currentName, _content, createTime, EMOJI_MAP);
            break;
          case MsgType.IMAGE: //图片消息
            _html = _html + getReceivePicText(currentName, _content, createTime);
            break;
          case MsgType.PROD: //商品消息
            var _prodObj = $.parseJSON(_content);
            _html = _html + getReceiveProdText(currentName, _prodObj, createTime);
            break;
          case MsgType.ORDER: //订单消息
            var _orderObj = $.parseJSON(_content);
            _html = _html + getReceiveOrderText(currentName, _orderObj, createTime);
            break;
          case MsgType.TIME://时间
            _html = _html + getReceiveTimeText(createTime);
            break;
        }
      }
    }
    if (unReadIds.length > 0) {
      chat.send(unReadIds, MsgType.READ);
      var num = $(".shop-list").find("i.num");
      var t = num.text() - (unReadIds.length + 1);
      if (t <= 0) {
        num.remove();
        return _html;
      }
      num.text(t);
    }
    return _html;
  }


  //发送==》发送信息
  chat.toggle("click", "#send", function (e) {
    var context = $.trim($("#context").html());
    if (isBlank(context)) {
      $("#context").text("");
      return;
    }
    if (isBlank(currentUserId)) {
      $("#context").text("");//清空输入框信息
      layerMsg("无法发送,当前无咨询用户");
      return;
    }

    var flag = true;//是否要发送图片
    var str = $("#context").html();
    $("#context").find("img").each(function (index, element) {
      let src = $(element)[0].src;
      let key = $(element).attr("key"); //表情图片会有key

      //如果发送的是复制的图片
      if (isBlank(key)) {
        let path = src.substring(imagesPrefix.length); //去掉前缀
        let _text = getSendPicText(path, MsgStatus.SUCCESS, new Date().getTime());
        $(".chat-list").append(_text);
        chat.scrollTop();
        chat.send(path, MsgType.IMAGE);//消息类型;(如：0:text、1:image、2:voice、3:vedio、4:music、5:prod 6:order)
        $("#context").text("");//清空输入框信息
        flag = false;
      } else {//将html替换表情图片
        str = str.replace($(element)[0].outerHTML, key);
      }

    });

    if (!flag) {
      return false;
    }
    chat.send(str, MsgType.TEXT);//发送消息,文本消息
    chat.setMessageInnerHTML(str);//将消息显示在网页上
  });

  //选择图片==>回显至输入框
  chat.toggle("change", "#sendImgFile", function (e) {
    var file = e.target.files[0];
    if (isBlank(sendImgFile)) {
      return;
    }
    var formData = new FormData();
    formData.append('sendImgFile', file);
    $.ajax({
      url: contextPath + "/admin/customer/upload",
      data: formData,
      type: "post",
      dataType: "json",
      contentType: false,
      processData: false,
      mimeType: "multipart/form-data",
      error: function () {
        console.log("网络延迟，请稍后再试");
      },
      success: function (result) {
        if (result.status == "OK") {
          var _text = getSendPicText(result.path, MsgStatus.SUCCESS, new Date().getTime());
          $(".chat-list").append(_text);
          chat.scrollTop();
          chat.send(result.path, MsgType.IMAGE);//消息类型;(如：0:text、1:image、2:voice、3:vedio、4:music、5:prod 6:order)
        } else {
          layer.msg(result.message, {icon: 2});
        }
      }
    });
  })

  //表情==》切换
  chat.toggle("click", ".rep-face .too-i,.edit-box", function (e) {
    $(".emoji").toggleClass("hide");
  })

  //表情==》点击文本框隐藏
  chat.toggle("click", ".edit-box", function (e) {
    $(".emoji").addClass("hide");
  })

  //表情==》单击选择表情
  chat.toggle("click", ".emoji li", function (e) {
    var _this = e.srcElement || e.target;
    var _html = $(_this).prop('outerHTML');
    $("#context").append(_html);
    $(".emoji").addClass("hide");
  })

  //切换==》正在咨询/本店订单/本店商品
  chat.toggle("click", ".sid-tab .tab-item", function (e) {
    var _this = e.srcElement || e.target;
    var _type = $(_this).attr("data-name");
    var e = $(".rightContext");

    $(".sid-tab .tab-item").removeClass("current");
    $(_this).parents(".tab-item").addClass("current");

    switch (_type) {
      case "imConsult": //正在咨询
        var _data = $(".right-sidebar").data("imConsult");
        if (isBlank(_data)) {
          var _context = '<div class="my-asking">' +
            '<div class="now-list">' +
            '<div class="no-record">' +
            '<i class="rec-i"></i>' +
            '<p class="rec-p">暂无数据</p>' +
            '</div>' +
            '</div>' +
            '</div>';
          e.html(_context);
          break;
        }
        e.html(_data);
        break;
      case "imOrder": //本店订单
        var url = contextPath + "/customerService/order";
        asyncload(url, currentUserId, e);
        break;
      case "imProd"://本店商品
        var url = contextPath + "/customerService/prod";
        asyncload(url, shopId, e);
        break;
    }
  })

  //切换聊天对话
  chat.toggle("click", ".shop-list a.list-item", function (e) {
    var _this = e.srcElement || e.target;
    var _item = $(_this).parents("a.list-item");
    var mobile = "暂无";
    if ($(_item).hasClass("current") || $(_this).hasClass("current")) {
      return false;
    }

    $(".shop-list a.list-item").removeClass("current");
    if (_item.length > 0) {
      _this = _item;
    }
    $(_this).addClass("current");
    currentUserId = $(_this).attr("data-userId");
    mobile = $(_this).attr("data-mobile");
    $(".currentName").html($(_this).find(".s-name").text());

    $(".shop-list a.list-item[data-userId='" + currentUserId + "']").find(".s-tip .tip-txt").text("");
    $(".shop-list a.list-item[data-userId='" + currentUserId + "']").find(".s-tip .tip-time").text("");
    var num = $(".shop-list a.list-item[data-userId='" + currentUserId + "']").find("i.num");
    if (num.length > 0) {
      num.remove();
    }

    //处理右侧
    $(".sid-tab .tab-item").removeClass("current");
    $(".sid-tab .tab-item:eq(0)").addClass("current");
    chat.getInitConsultProd(currentUserId, $(".currentName").html(), mobile, 0);

    $(".chat-list").html("");
    //回显与当前客服的聊天记录
    chat.getChatRecord();
  })

  //处理键盘回车事件
  document.onkeydown = function (event) {
    var e = event || window.event || arguments.callee.caller.arguments[0];
    if (e && e.keyCode == 13) {
      $("#send").click();
    }
  };

  //心跳检测
  var heartCheck = {
    timeout: 30000,        //30秒发一次心跳
    timeoutObj: null,
    serverTimeoutObj: null,
    reset: function () {
      clearTimeout(this.timeoutObj);
      clearTimeout(this.serverTimeoutObj);
      return this;
    },
    start: function () {
      var self = this;
      this.timeoutObj = setTimeout(function () {
        //这里发送一个心跳，后端收到后，返回一个心跳消息，
        //onmessage拿到返回的心跳就说明连接正常
        var options = {
          cmd: 9,
          userId: customId,
          type: 'pc'
        };
        websocket.send(JSON.stringify(options));
        console.log("心跳包 " + JSON.stringify(options) + "  " + new Date());
        self.serverTimeoutObj = setTimeout(function () {
          //如果超过一定时间还没重置，说明后端主动断开了
          //如果onclose会执行reconnect，我们执行ws.close()就行了.如果直接执行reconnect 会触发onclose导致重连两次
          websocket.close();
        }, self.timeout)
      }, this.timeout)
    }
  }

  //强制关闭浏览器  调用websocket.close（）,进行正常关闭
  window.onunload = function () {
    websocket.close();
  }

})

var MsgStatus = {SUCCESS: "success", FAIL: "fail"};//发送消息状态

//发送文本消息、聊天记录文本==》[右侧]
function getSendText(context, status, createTime, _emojiArrObj, id, _name) {
  var offline = "";
  // 用socket判断是发送失败还是成功
  if (websocket.readyState == WebSocket.OPEN) {
    offline = '<span class="un-read" id="_chat_id_"' + id + '>未读</span>';//红色感叹号
    if (status == MsgStatus.SUCCESS) {
      offline = "";
    }
  } else {
    offline = '<span class="dia-err"></span>';//红色感叹号
  }

  if (_name == null) {
    _name = name;
  }

  context = parseEmoji(context, _emojiArrObj);
  var _text = '<div class="chat-txt clear" date="' + createTime + '">' +
    '<div class="chat-area cus">' +
    '<p class="name" style="margin:0;">' + _name + '</p>' +
    '<div class="dialog">' +
    '<i class="dia-arrow"></i>' + offline +
    '<div class="dia-con"><p class="dia-con-p" style="margin:0;">' + context + '</p></div>' +
    '</div>' +
    '</div>' +
    '</div>';
  return _text;
}

//发送图片、聊天记录图片==》[右侧]
function getSendPicText(path, status, createTime,id,_name) {
  var offline = "";
  if (websocket.readyState == WebSocket.OPEN) {
    offline = '<span class="un-read" id="_chat_id_"' + id + '>未读</span>';//红色感叹号
    if (status == MsgStatus.SUCCESS) {
      offline = "";
    }
  } else {
    offline = '<span class="dia-err"></span>';//红色感叹号
  }

  if (_name == null) {
    _name = name;
  }

  var _text = '<div class="chat-txt clear" date="' + createTime + '">' +
    '<div class="chat-area cus">' +
    '<p class="name" style="margin:0;">' + _name + '</p>' +
    '<div class="dialog">' +
    '<i class="dia-arrow"></i>' + offline +
    '<div class="dia-con">' +
    '<a href="javascript:void(0)" class="dia-goods clear">' +
    '<span class="g-img" style="width:150px;margin-right:0px;height: inherit;"><img src="' + imagesPrefix + path + '"></span>' +
    '<span class="g-info" style="padding-left:0px"></span>' +
    '</a>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';
  return _text;
}

//接收文本消息、聊天记录文本==》[左侧]
function getReceiveText(currentName, valuestr, createTime, _emojiArrObj) {
  valuestr = parseEmoji(valuestr, _emojiArrObj);
  var _text = '<div class="chat-txt clear" date="' + createTime + '">' +
    '<div class="chat-area sho">' +
    '<p class="name" style="margin:0;">' + currentName + '</p>' +
    '<div class="dialog">' +
    '<i class="dia-arrow"></i>' +
    '<div class="dia-con">' +
    '<p class="dia-con-p" style="margin:0;">' + valuestr + '</p>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';
  return _text;
}

//接收商品消息、聊天记录商品==》[左侧]
function getReceiveProdText(currentName, _prodObj, createTime) {
  var refundSn = "";
  if (!isBlank(_prodObj.refundSn)) {
    refundSn = '<span class="g-num">退款编号：' + _prodObj.refundSn + '</span>';
  }
  if (_prodObj.pic.indexOf("logo") != -1) {
    _prodObj.pic = contextPath + "/resources/templets/amaze/images/head-default.jpg";
  }
  var _text = '<div class="chat-txt clear" date="' + createTime + '">' +
    '<div class="chat-area sho">' +
    '<p class="name" style="margin:0;">' + currentName + '</p>' +
    '<div class="dialog">' +
    '<i class="dia-arrow"></i>' +
    '<div class="dia-con">' +
    '<a href="javascript:void(0)" class="dia-goods clear" target="_blank">' +
    '<span class="g-img"><img src="' + _prodObj.pic + '"></span>' +
    '<span class="g-info">' +
    '<span class="g-nam">商品名称：' + _prodObj.prodName + '</span>' +
    refundSn +
    '<span class="g-num">商品价格：' + _prodObj.price + '</span>' +
    '</span>' +
    '</a>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';
  return _text;
}

//接收订单消息、聊天记录订单==》[左侧]
function getReceiveOrderText(currentName, _orderObj, createTime) {
  var _text = '<div class="chat-txt clear" date="' + createTime + '">' +
    '<div class="chat-area sho">' +
    '<p class="name" style="margin:0;">' + currentName + '</p>' +
    '<div class="dialog">' +
    '<i class="dia-arrow"></i>' +
    '<div class="dia-con">' +
    '<a href="javascript:void(0)" class="dia-goods clear" target="_blank">' +
    '<span class="g-img"><img src="' + _orderObj.pic + '"></span>' +
    '<span class="g-info">' +
    '<span class="g-num">订单编号：' + _orderObj.subNumber + '</span>' +
    '<span class="g-nam">订单金额：' + _orderObj.price + '</span>' +
    '<span class="g-nam">下单时间：' + _orderObj.orderDate + '</span>' +
    '</span>' +
    '</a>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';
  return _text;
}

//接收图片信息==》[左侧]
function getReceivePicText(currentName, path, createTime) {
  var _text = '<div class="chat-txt clear" date="' + createTime + '">' +
    '<div class="chat-area sho">' +
    '<p class="name" style="margin:0;">' + currentName + '</p>' +
    '<div class="dialog">' +
    '<i class="dia-arrow"></i>' +
    '<div class="dia-con">' +
    '<a href="javascript:void(0)" class="dia-goods clear">' +
    '<span class="g-img" style="width:150px;margin-right:0px;height: inherit;">' +
    '<img src="' + imagesPrefix + path + '">' +
    '</span>' +
    '<span class="g-info" style="padding-left:0px"></span>' +
    '</a>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>';
  return _text;
}

//接收时间信息==》[左侧]
function getReceiveTimeText(createTime) {
  var _text = '<div class="chat-txt chat-time" date="' + createTime + '"><p class="time">' + new Date(createTime).toLocaleString() + '</p></div>';
  return _text;
}

//异步替换数据
function asyncload(url, userId, classObject) {
  $.ajax({
    url: url,
    data: {"userId": userId},
    type: "post",
    dataType: "html",
    error: function () {
      console.log("网络延迟，请稍后再试");
    },
    success: function (result) {
      classObject.html(result);
    }
  });
}


//方法，判断是否为空
function isBlank(_value) {
  return _value == null || _value == "" || _value == undefined;
}

//layer提示
function layerMsg(context) {
  layer.msg(context, {
    time: 5000
  });
}

//格式化金额
function formatMoney(value) {
  if (isBlank(value)) {
    return "0.00";
  }
  var value = Math.round(parseFloat(value) * 100) / 100;
  var xsd = value.toString().split(".");
  if (xsd.length == 1) {
    value = value.toString() + ".00";
    return value;
  }
  if (xsd.length > 1) {
    if (xsd[1].length < 2) {
      value = value.toString() + "0";
    }
    return value;
  }
}

//时间戳==》格式化
function fmtDate(time) {
  var date = new Date(time);
  var HH = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
  var ss = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
  var mm = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
  return HH + ss + mm;
}

function reconnect() {
  setTimeout(function () {     //没连接上会一直重连，设置延迟避免请求过多
    websocket = new WebSocket(IM_BIND_ADDRESS);
    websocket.onclose = function () {
      reconnect();
    };
    websocket.onerror = function () {
      reconnect();
    };
  }, 2000);
}

//解析表情
function parseEmoji(valuestr, _emojiArrObj) {
  for (var emoji in _emojiArrObj) {
    valuestr = valuestr.replaceAll(emoji, _emojiArrObj[emoji]);
  }
  return valuestr;
}

//replaceAll 全局替换
String.prototype.replaceAll = function (FindText, RepText) {
  var regExp = new RegExp(FindText, "g");
  return this.replace(regExp, RepText);
}
