$.validator.setDefaults({

});

$(document).ready(function() {
	jQuery("#form1").validate({errorPlacement : function(error,element) {
				if (element.is(":checkbox")|| element.is(":radio")) {
					error.appendTo(element.parent());
				} else {
					error.insertAfter(element);
				}
			},
					rules : {
						name : {
							required : true
						},
						menuType : {
							required : true
						},
						menuUrl : {
							required : true
						},
						infoType : {
							required : true
						},
						content : {
							required : true
						},
						templateId : {
							required : true
						},
							menuKey : {
								required : true,
								remote : { // 异步验证是否存在
									type : "POST",
									url : contextPath
											+ "/admin/weixinMenu/isExitMenuKey",
									data : {
										oldmenukey : function() {
											return menuKey;
										},
										menukey : function() {
											return $.trim($(
													"#menuKey")
													.val());
										}
									}
								}
							},
							seq : {
								required : true,
								isNumber : true
							}
						},
						messages : {
							name : {
								required : "请输入名称"
							},
							menuType : {
								required : true
							},
							menuUrl : {
								required : "请输入跳转网页地址"
							},
							infoType : {
								required : "请选择消息类型！"
							},
							content : {
								required : "请输入消息内容！"
							},
							templateId : {
								required : "请选择消息模版！"
							},
							menuKey : {
								required : "请输入菜单标识",
								remote : "菜单标识已经存在！"
							},
							seq : {
								required : '请输入顺序',
								isNumber : '请输入5位之内的正整数'
							},
						}
					});

	if (templateId != null && templateId != ""
			&& templateId != undefined) {
		var templateObj = $("#templateId");
		templateObj.html("");
		$
				.ajax({
					url : contextPath
							+ "/admin/weixinMenu/getTemplates",
					data : {
						"msgType" : msgType
					},
					dataType : "JSON",
					type : "POST",
					success : function(result) {
						var msg = "";
						msg += "<option value=''>--------------------</option>";
						var data = eval(result);
						for (var i = 0; i < data.length; i++) {
							if (templateId == data[i].key) {
								msg += "<option value='"
										+ data[i].key
										+ "' selected='selected' >"
										+ data[i].value
										+ "</option>";
							} else {
								msg += "<option value='"
										+ data[i].key + "'  >"
										+ data[i].value
										+ "</option>";
							}
						}
						templateObj.html(msg);
					}
				});
	}

	if ("click" == menuType) {
		$("#text_content").removeAttr("style");
		$("#templateTr").attr("style", "display:none");
		$("#templateId").attr("disabled", "disabled");
		$("#trurl").hide();
		$("#xxtr").show();
		$("#menuKeyTr").removeAttr("style");
		if (infoType != null || infoType != undefined
				|| infoType != "") {
			if ("text" == infoType) {
				$("#text_content").removeAttr("style");
				$("#templateTr").attr("style", "display:none");
				$("#templateId").attr("disabled", "disabled");
			} else {
				$("#templateTr").removeAttr("style");
				$("#templateId").removeAttr("disabled");
				$("#text_content")
						.attr("style", "display:none");

			}
		}
	} else {
		$("#text_content").removeAttr("style");
		$("#templateTr").attr("style", "display:none");
		$("#templateId").attr("disabled", "disabled");
		$("#menuKeyTr").attr("style", "display:none");
		$("#text_content").attr("style", "display:none");
		$("#xxtr").hide();
		$("#trurl").show();
	}

	var menuId = $("#menuId").val();
	if (menuId == null || menuId == undefined || menuId == "") {
		$("#trurl").attr("style", "display:none");
		// $("#text_content").attr("style", "display:none");
		$("#text_content").removeAttr("style");
		$("#xxtr").show();
		$("#templateTr").show();
		$("#templateId").show();
		$("input[name='infoType'][type='radio']").prop(
				"checked", "checked");
	}

	$("input[name='infoType'][type='radio']").change(
			function() {
				var selectValue = $(this).val();
				if ("text" == selectValue) {
					$("#text_content").removeAttr("style");
					$("#templateTr").attr("style",
							"display:none");
					$("#templateId").attr("disabled",
							"disabled");
					$("#menuKeyTr").removeAttr("style");
				} else {
					$("#templateTr").removeAttr("style");
					$("#templateId").removeAttr("disabled");
					$("#text_content").attr("style",
							"display:none");
					$("#menuKeyTr").removeAttr("style");
					getTemplates(selectValue);
				}
			});

	$("#menuType")
			.change(
					function() {
						var selectValue = $(this).children(
								'option:selected').val();

						if ("click" == selectValue) {

							$("#url").attr("disabled",
									"disabled");
							$("#trurl").attr("style",
									"display:none");

							$("#xxtr").removeAttr("style");
							var inputAttr = $("input[name='infoType']");
							for (var i = 0; i < inputAttr.length; i++) {
								$(inputAttr[i]).removeAttr(
										"disabled");
							}
							$("#templateTr")
									.removeAttr("style");
							$("#templateId").removeAttr(
									"disabled");
							$("#menuKeyTr").removeAttr("style");
							var infoType = $(
									"input:radio[name='infoType']:checked")
									.val();
							if ("text" == infoType) {
								$("#text_content").removeAttr(
										"style");
								$("#templateTr").attr("style",
										"display:none");
								$("#templateId").attr(
										"disabled", "disabled");
							} else {
								$("#templateTr").removeAttr(
										"style");
								$("#templateId").removeAttr(
										"disabled");
								$("#text_content")
										.attr("style",
												"display:none");
							}

							// 设置消息类型和验证
						} else {

							$("#url").removeAttr("disabled");
							$("#trurl").removeAttr("style");
							$("#xxtr").attr("style",
									"display:none");
							$("#text_content").attr("style",
									"display:none");
							var inputAttr = $("input[name='infoType']");
							for (var i = 0; i < inputAttr.length; i++) {
								$(inputAttr[i]).attr(
										"disabled", "disabled");
							}
							$("#templateTr").attr("style",
									"display:none");
							$("#templateId").attr("disabled",
									"disabled");
							$("#menuKeyTr").attr("style",
									"display:none");
							// 取消验证。防止无法提交
						}
					});

	highlightTableRows("col1");
	// 斑马条纹
	$("#col1 tr:nth-child(even)").addClass("even");
});


jQuery.validator.addMethod("isNumber", function(value, element) {
    return this.optional(element) || /^[1-9]\d{0,4}$/.test(value);
}, "必须整数");

function getTemplates(msgType) {
	var templateObj = $("#templateId");
	templateObj.html("");
	$.ajax({
		url : contextPath + "/admin/weixinMenu/getTemplates",
		data : {
			"msgType" : msgType
		},
		dataType : "JSON",
		type : "POST",
		success : function(result) {
			var msg = "";
			msg += "<option value=''>--------------------</option>";
			var data = eval(result);
			for (var i = 0; i < data.length; i++) {
				msg += "<option value='" + data[i].key + "'>" + data[i].value
						+ "</option>";
			}
			templateObj.html(msg);
		}
	});
}
