$(document).ready(function() {
	//对所有的 文件选择 进行格式检查
	/*$(document).delegate("input[type='file']","change",function(e){
		checkImgType($(this));
		checkImgSize($(this),2048);
	});*/
	
	$(document).delegate('a',clickType, function(e){
		  var type = $(this).data("type");
		  var urlLink = this.href;
		  if(type!=undefined){
			  if(type=="menu"){
				  var menuId = $(this).data("menu");
				  document.cookie="_ls_menu_="+menuId+";path=/";
			  }else if(type=="index"){//清除已经选择的菜单
				  document.cookie="_ls_menu_= ;path=/";
			  }else if(type=="top"){//清除已经选择的菜单
				  document.cookie="_ls_menu_= ;path=/";
			  }else if(type=="main"){
		  			 e.preventDefault();
			  		sendAjax(urlLink, type); //调用ajax
			  }else if(type=="pager"){
		  		  	var pageNo = $(this).data("value");
		  			e.preventDefault();
		  			pager(pageNo); //调用javascript
			  }
			  
		  }
		});
	
	$("#message").on("click","a",function(){
		  var menuId = $(this).data("menu");
		  document.cookie="_ls_menu_="+menuId+";path=/";
	});
	
	selectMenu(getCookie());
	
	$('#shopIndex').mouseover(function () {
		$('#message').show();
	});
	$('#message').mouseover(function () {
		$('#message').show();
	});
	$('#message').mouseout(function () {
		$('#message').hide();
	});
		
	});
	
	function sendAjax(urlLink, type){
				$.ajax({
				"url":urlLink, 
				type:'get', 
				cache:false,
				dataType : 'html', 
				async : false, //默认为true 异步   
				error: function(jqXHR, textStatus, errorThrown) {
					if("Forbidden" ==errorThrown ){
						window.location = contextPath + "/adminlogin";
					}else{
						//错误页面
						$(document).find("html").html(jqXHR.responseText);
					}
				},
				success:function(result){
			  	     if("main" == type){
			  	    	 //暂时没有用了
			   			//	 $("#admin-content").html(result);//替换右边内容
			  		 }
				}
			});
		}
		
	function selectMenu(menuId){
		if(menuId!=undefined && menuId!=""){
			var menu = $("#menuId-" +menuId );
			if(menu != null){
				menu.addClass("third_menu_li_sel").siblings().removeClass("third_menu_li_sel");
				menu.parent().parent().siblings().find("ul").removeClass("am-in");
				menu.parent().addClass("am-in");
			}
		}else{//默认选择第一个
			var menu = $("#secondMenuUl li ul li").get()[0];
			$(menu).addClass("third_menu_li_sel").siblings().removeClass("third_menu_li_sel");
			$(menu).parent().parent().siblings().find("ul").removeClass("am-in");
			$(menu).parent().addClass("am-in");
		}
	}
	
//	String.prototype.startWith = function(compareStr){
//		return this.indexOf(compareStr) == 0;
//	}	
	
//	String.prototype.endWith=function(str){     
//	  var reg=new RegExp(str+"$");     
//	  return reg.test(this);        
//	}
	
	//取cookies函数  
	function getCookie(){
       var cookieArray=document.cookie.split("; ");    
	   for (var i=0;i<cookieArray.length;i++){   
	      var arr=cookieArray[i].split("=");        
	      if(arr[0]=="_ls_menu_"){
	    	return unescape(arr[1]); 
	      }
	   }
	   return null;
	}

	 //修改密码
	function changeAdminPwdDialog(obj){
		var url = contextPath + "/admin/adminUser/updatePwd";
    	layer.open({
  		  type: 2,
  		  title: "修改密码",
  		  btn: false,
  		  shadeClose: true,
  		  shade: 0.6,
  		  area: ['500px', '270px'],
  		  content: [url,'no'],
  		});
	}
		
	 //修改密码后把下拉列表隐藏
	function refreshMyProfile(){
		  $("#myProfile").click();
	}
	
	function showBackAbout(){
		$.ajax({
			url: contextPath+"/admin/system/about/query",
			type: "GET",
			dataType: "JSON",
			error: function(result){
			
			},
			success: function(result){
				$("#doc-modal-1 .am-modal-bd").html(result);
				$("#doc-modal-1").modal('open');
			}
		});
	}
	
	function checkmore(){
		$('#message li').each(function(index){
			$(this).show();
		});
		$('#check_more').hide();
	}

	function slip(){
		$('#message li').each(function(index){
			if(index>=4){
			$(this).hide();
			}
		});
		$('#check_more').show();
	}
	
