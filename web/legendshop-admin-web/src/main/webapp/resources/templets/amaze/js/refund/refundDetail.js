window.onload=function(){
	if(!isBlank(refundMSG)){
		layer.alert(refundMSG);
	}

	//同意退款
	$("#returnBtn").click(function(){
		var refundId = $("#refundId").val();
 	   	var adminMessage = $("#adminMessage").val();

 	   if(isBlank(refundId) || isBlank(adminMessage)){
 			layer.msg("请输入备注信息再提交！", {icon:2});
 			return;
 		}

	     layer.confirm('是否确定同意退款操作,在退款操作中请勿重复该操作,并且一旦提交将不可恢复？',  {icon:3}, function (index) {
		       $.ajax({
					type: "POST",
					dataType:"json",
				    data:{"adminMessage": adminMessage},
					url: contextPath+"/admin/payment/return/refund/"+refundId,
					async: false,
				   beforeSend: function(xhr){
					   loadingFlag= layer.msg('处理退款中，请稍候……', { icon: 16, shade: 0.01,shadeClose:false,time:60000 });

				   },
				   complete: function(xhr,status){
					   layer.close(loadingFlag);
				   },
				   error: function(rs){
					   layer.alert("强制退款失败,请勿非法操作");
				   },
					success: function(rs){
					  var result=eval(rs);
					  if(result.status){
						  parent.location.reload(true);
					  }else{
						  layer.alert(result.result);
					  }
				    }
				});
		       layer.close(index);
		    },
		    function () {
	  });
	});

	//退款查询
	$("#refundQuery").click(function(){
	    var ajaxurl = contextPath+"/admin/payment/return/refundQuery/"+refundId;
		$.ajax({
			type: "POST",
			dataType:"json",
			url: ajaxurl,
			async: false,
			success: function(rs){
	            layer.msg(rs);
		    }
		});
	});
}

//退货提交审核
function auditReturn(){
	var refundId = $("#refundId").val();
	var adminMessage = $("#adminMessage").val();
	if(isHandleSuccess!="1"){
	   layer.msg("必须要操作退款成功,才能提交处理", {icon:2});
	   return;
	}
	if(isBlank(refundId) || isBlank(adminMessage)){
		layer.msg("对不起,请输入备注信息再提交", {icon:2});
		return;
	}
	layer.confirm("您确定要提交审核吗,一旦提交将不可恢复?", {icon:3}, function(){
		$("#form1").submit();
	});
}
function isBlank(value){
	return value == undefined || value == null || $.trim(value) === "";
}

//退款提交审核
function auditRefund(){
	var refundId = $("#refundId").val();
	var adminMessage = $("#adminMessage").val();
	if(isHandleSuccess!="1"){
		layer.msg("必须要操作退款成功,才能提交处理", {icon:2});
		return;
	}
	if(isBlank(refundId) || isBlank(adminMessage)){
		layer.msg("请输入备注信息再提交！", {icon:2});
		return;
	}
	layer.confirm("您确定要提交审核吗,一旦提交将不可恢复?", {icon:3}, function(index){
		$("#form1").submit();
	});
}

function isBlank(value){
	return value == undefined || value == null || $.trim(value) === "";
}
