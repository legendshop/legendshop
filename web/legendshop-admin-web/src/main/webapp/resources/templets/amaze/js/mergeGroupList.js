$(document).ready(function(){
	highlightTableRows("item");
	var shopName = $("#shopName").val();
	if(shopName == ''){
        makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'shopName');
    }else{
    	makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",shopName, 'shopName');
		$("#select2-chosen-1").html(shopName);
    }
	sendData(1);
});
function search(){
	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
	sendData(1);
}

function searchType(doc,type){
  $("#searchType").val(type);
  $(".am-active").removeClass("am-active")
  $(doc).parent("li").addClass("am-active")
  sendData(1);
}

function sendData(curPageNO) {
    var data = {
		 "shopId": $("#shopId").val(),
		 "flag": "platform",
         "curPageNO": curPageNO,
         "mergeName": $("#mergeName").val(),
         "searchType": $("#searchType").val(),
    };
    $.ajax({
        url: contextPath+"/admin/mergeGroup/queryContent",
        data: data,
        type: 'post',
        async: true, //默认为true 异步
        success: function (result) {
            $("#mergeGroupContentList").html(result);
        }
    });
}

function makeSelect2(url,element,text,label,value,holder, textValue){
	$("#"+element).select2({
        placeholder: holder,
        allowClear: true,
        width:'200px',
        ajax: {
    	  url : url,
    	  data: function (term,page) {
                return {
                	 q: term,
                	 pageSize: 10,    //一次性加载的数据条数
                 	 currPage: page, //要查询的页码
                };
            },
			type : "GET",
			dataType : "JSON",
            results: function (data, page, query) {
            	if(page * 5 < data.total){//判断是否还有数据加载
            		data.more = true;
            	}
                return data;
            },
            cache: true,
            quietMillis: 200//延时
      },
        formatInputTooShort: "请输入" + text,
        formatNoMatches: "没有匹配的" + text,
        formatSearching: "查询中...",
        formatResult: function(row,container) {//选中后select2显示的 内容
            return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
        },formatSelection: function(row) { //选择的时候，需要保存选中的id
        	$("#" + textValue).val(row.text);
            return row.text;//选择时需要显示的列表内容
        },
    });
}

function initStatus(itemId, itemName, status, url, target,path,toStatus){
	var desc;
	if(status == 1){
		desc =" 是否确定进行下线操作,如果下线未成团的订单货款将全部原路返回 ";
	}else if(status == -1){
		desc = '确定要上线'+'【'+itemName+'】'+'吗?'
	}else if(status == 0 && toStatus == 1){
		desc = '确定要通过'+'【'+itemName+'】'+'吗?'
	}else if(status == -2){
		desc = '确定要通过'+'【'+itemName+'】'+'吗?'
	}else {
		desc = '确定要拒绝'+'【'+itemName+'】'+'吗?'
	}

	layer.confirm(desc, {icon:3}, function() {
		$.ajax({
			url : url + itemId + "/" + toStatus,
			type : 'get',
			async : false, //默认为true 异步
			dataType : "json",
			error : function(jqXHR, textStatus, errorThrown) {
			},
			success : function(data) {
				$target = $(target);
				if(data == 1){
					//$target.attr("src",path + "/resources/common/images/blue_down.png");
					$target.html("<span class='am-icon-arrow-down'></span>下线");
					$target.attr("style","color:#f37b1d;");
				}else if(data == 0){
					//$target.attr("src",path + "/resources/common/images/yellow_up.png");
					$target.html("<span class='am-icon-arrow-up'></span>上线");
					$target.attr("style","color:#0e90d2;");
				}
				$target.attr("status",data);
				window.location.reload();
			}


		});

	});

}

function highlightTableRows(tableId) {
	var previousClass = null;
	var table = document.getElementById(tableId);
	if(table == null) return;
	var tbody;
	if(table.getElementsByTagName("tbody") != null){
		tbody = table.getElementsByTagName("tbody")[0];
	}
	if (tbody == null) {
		var rows = table.getElementsByTagName("tr");
	} else {
		var rows = tbody.getElementsByTagName("tr");
	}
	for (i=0; i < rows.length; i++) {
		rows[i].onmouseover = function() { previousClass=this.className;this.className='pointercolor' };
		rows[i].onmouseout = function() { this.className=previousClass };
		/*
        rows[i].onclick = function() {
            var cell = this.getElementsByTagName("td")[0];
            var link = cell.getElementsByTagName("a")[0];
            var theUrl=link.getAttribute("href");
            if(theUrl != null && theUrl.indexOf("javascript")!=-1){
                eval(theUrl); }else{
                location.href=theUrl;
            }
   		 this.style.backgroundColor="blue";
        }

		 */
	}
}

function pager(curPageNO){
	document.getElementById("curPageNO").value=curPageNO;
	sendData(curPageNO);
}


