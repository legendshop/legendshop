$(function() {
	
	//因为ie兼容性问题 重写endWith 之前直接使用endsWith函数
	String.prototype.endWith=function(str){
	    if(str==null||str==""||this.length==0||str.length>this.length){
	        return false;
	    }
	    if(this.substring(this.length-str.length)==str){
	        return true;
	    }else{
	        return false;
	    }
	    return true;
	}
	
	// 点击保存配置,提交数据
	$("#saveBtn").click(function() {
		// 参数校验
		if (vm.sliders) {
			var hasErr = false;
			vm.sliders.forEach(function(e, i) {
				if (!e.img || e.img == "") {
					hasErr = true;
					return;
				}
			});
			if (hasErr) {
				layer.alert('顶部轮播图不允许存在为空的图片！', {
					icon : 2
				});
				return;
			}
		}
		if (vm.adverts) {
			var hasErr = false;
			vm.adverts.forEach(function(e, i) {
				if (!e.img || e.img == "") {
					hasErr = true;
					return;
				}
			});
			if (hasErr) {
				layer.alert('顶部广告区不允许存在为空的图片！', {
					icon : 2
				});
				return;
			}
		}
		if (vm.floors) {
			var hasErr = false;
			var errMsg = '';
			vm.floors.forEach(function(floor, idx) {
				var text = floor.text;
				if (floor.adverts) {
					floor.adverts.forEach(function(e, i) {
						if (!e.img || e.img == "") {
							hasErr = true;
							errMsg = '["' + text + '"]楼层广告区不允许存在为空的图片！';
							return;
						}
					});
				}
				if (hasErr) {
					return;
				}
				if (floor.banners) {
					floor.banners.forEach(function(e, i) {
						if (!e.img || e.img == "") {
							hasErr = true;
							errMsg = '["' + text + '"]楼层轮播区不允许存在为空的图片！';
							return;
						}
					});
				}
			});
			if (hasErr) {
				layer.alert(errMsg, {
					icon : 2
				});
				return;
			}
		}

		if (vm.floors && vm.floors.length < 1) {
			layer.alert('至少需要包含1个楼层', {
				icon : 2
			});
			return;
		}

		$("#saveBtn").prop('disabled', true);
		layer.confirm('保存后将影响app首页的展现，请确认是否保存？', {
			icon : 3
		}, function() {
			var dataStr = JSON.stringify(vm.$data);
			console.log('-----save ----', dataStr);
			$.ajax({
				url : contextPath + "/admin/app/decorate/saveAppIndex",
				context : document.body, // 传入上下文变量
				type : "post",
				data : {
					"decotateData" : dataStr
				},
				success : function(resp) {
					var result = eval(resp);
					console.log('---success-->', result);
					if (result == 'OK') {
						layer.alert('保存成功', {
							icon : 1
						});
						window.document.location = contextPath + "/admin/app/decorate/query";
					} else {
						layer.alert('保存失败，请稍后再试！', {
							icon : 2
						});
					}
					$("#saveBtn").prop('disabled', false);
				}
			});
		}, function() {
			$("#saveBtn").prop('disabled', false);
		});
	});
});

// 添加商品
function addGood(floorIndex) {
	productChooseWindow('floors[' + floorIndex + '].adverts', 10);
}

// 修改Action
function chooseAction(val, dataPath) {
	console.log('---chooseAction--->', val, dataPath);
	// action变化时，先清空actionParam
	vm.$set(dataPath + '.actionParam', {});
	if (val === 0) {
		vm.$set(dataPath + '.action', '');
	} else if (val === 1) { // 商品分类
		catChooseWindow(dataPath);
		vm.$set(dataPath + '.action', 'GoodsList');
	} else if (val === 2) { // 商品品牌
		brandChooseWindow(dataPath);
		vm.$set(dataPath + '.action', 'GoodsList');
	} else if (val === 3) { // 单个商品
		productChooseWindow(dataPath);
		vm.$set(dataPath + '.action', 'GoodsDetail');
	} else if (val === 4) { // 专题
		themesProductChooseWindow(dataPath);
		vm.$set(dataPath + '.action', 'Theme1');
	} else if (val === 5) { // URL
		URlChooseWindow(dataPath);
		vm.$set(dataPath + '.action', 'GoodsList');
	}
}

// 图片选择window
function imageChooseWindow(sourceId, showsize) {
	var  sourceId=escape(sourceId);
	layer.open({
		title : '选择图片',
		id : 'loadImages',
		type : 2,
		content : contextPath + "/admin/app/decorate/loadImages?sourceId=" + sourceId + "&showsize=" + showsize,
		area : [ '800px', '450px' ]
	});
}

// 商品选择window
function productChooseWindow(sourceId, maxChoose) {
	var  sourceId=escape(sourceId);
	maxChoose = maxChoose || 1;
	layer.open({
		title : '选择商品',
		id : 'loadProds',
		type : 2,
		content : contextPath + "/admin/app/decorate/loadProds?sourceId=" + sourceId + "&size=" + maxChoose,
		area : [ '760px', '645px' ]
	})
}

// 专题选择window
function themesProductChooseWindow(sourceId, maxChoose) {
	var  sourceId=escape(sourceId);
	maxChoose = maxChoose || 1;
	layer.open({
		title : '选择专题',
		id : 'loadProds',
		type : 2,
		content : contextPath + "/admin/app/decorate/loadThemes?sourceId=" + sourceId + "&size=" + maxChoose,
		area : [ '760px', '540px' ]
	})
}

// URL选择window
function URlChooseWindow(sourceId) {
	var content = 'URL：<input type="text" id="url" /><br />';
	layer.open({
		title : 'URL',
		id : 'URL',
		content : content,
		yes : function(index, layero) {
			var url = $("#url").val();
			if(url == null || url =="" || url == undefined){
				layer.tips('请先输入URL', '#url');
				return;
			}
			console.log('-----save url-----', url, sourceId);
			vm.$set(sourceId + '.actionParam', {
				"searchParam" : {
					"url" : {
						"url" : url
					}
				}
			});
			layer.close(index);
		}
	});
	/*
	 * var content='URL：<input type="text" id="url" /><br />' art.dialog({
	 * id: 'URL', title: 'URL', content: content, lock: true, fixed: true, ok:
	 * function () { var url=$("#url").val(); console.log('-----save url-----',
	 * url, sourceId); vm.$set(sourceId +
	 * '.actionParam',{"searchParam":{"url":{"url":url}}}); }, });
	 */
}

// 分类选择window
function catChooseWindow(sourceId) {
	var  sourceId=escape(sourceId);
	layer.open({
		title : '选择分类',
		id : 'loadAllCategory',
		type : 2,
		content : contextPath + '/admin/app/decorate/loadAllCategory?sourceId=' + sourceId,
		area : [ '400px', '415px' ]
	});
}

// 品牌选择window
function brandChooseWindow(sourceId) {
	var  sourceId=escape(sourceId);
	layer.open({
		title : '选择品牌',
		id : 'loadBrands',
		type : 2,
		content : contextPath + '/admin/app/decorate/loadBrands?sourceId=' + sourceId,
		area : [ '750px', '500px' ]
	});
}

// 保存选择图片
function saveChoooseImage(url, path, sourceId) {
	console.log('------save image-----', url, path, sourceId);
	vm.$set(sourceId + '.img', jQuery.isArray(path) ? url[0] : path);
}

// 保存选择的商品信息
function saveChoooseProduct(ids, imgs, names, prices, sourceId) {
	console.log('-----save product-----', ids, imgs, names, prices, sourceId);
	var floors = sourceId.match(/floors\[\d\]/ig) + "";
	var floors_index = floors.match(/\d/ig);
	var adverts = sourceId.match(/adverts\[\d\]/ig) + "";
	var adverts_index = adverts.match(/\d/ig);
	if (sourceId.endWith('adverts')) { // 添加商品
		var advertsData = JSON.parse(JSON.stringify(vm.$get(sourceId) || []));
		var goodsLength = vm.$get(sourceId).length;
		for (var i = 0; i < ids.length; i++) {
			var goodData = {
				"id" : ids[i],
				"text" : names[i],
				"img" : imgs[i],
				"action" : "GoodsDetail",
				"actionParam" : {
					"searchParam" : {
						"goods" : {
							"goodsInfoId" : ids[i],
							"goodsInfoname" : names[i]
						}
					}
				},
				"ordering" : goodsLength + 1 + i,
				"price" : prices[i]
			};
			advertsData.push(goodData);
		}
		vm.$set(sourceId, advertsData);
	} else if (sourceId.match(/adverts\[\d\]/ig) && adverts_index > 2 && vm.$get("floors[" + floors_index + "].template") == "5") {
		vm.$set(sourceId + '.id', ids[0]);
		vm.$set(sourceId + '.text', names[0]);
		vm.$set(sourceId + '.img', imgs[0]);
		vm.$set(sourceId + '.action', "GoodsDetail");
		vm.$set(sourceId + '.ordering', adverts_index[0]);
		vm.$set(sourceId + '.price', prices[0]);
		vm.$set(sourceId + '.actionParam', {
			"searchParam" : {
				"goods" : {
					"goodsInfoId" : ids[0],
					"goodsInfoname" : names[0]
				}
			}
		});
	} else if (sourceId.match(/adverts\[\d\]/ig) && vm.$get("floors[" + floors_index + "].template") == "7") {
		vm.$set(sourceId + '.id', ids[0]);
		vm.$set(sourceId + '.text', names[0]);
		vm.$set(sourceId + '.img', imgs[0]);
		vm.$set(sourceId + '.action', "GoodsDetail");
		vm.$set(sourceId + '.ordering', adverts_index[0]);
		vm.$set(sourceId + '.price', prices[0]);
		vm.$set(sourceId + '.actionParam', {
			"searchParam" : {
				"goods" : {
					"goodsInfoId" : ids[0],
					"goodsInfoname" : names[0]
				}
			}
		});
	} else {
		vm.$set(sourceId + '.actionParam', {
			"searchParam" : {
				"goods" : {
					"goodsInfoId" : ids[0],
					"goodsInfoname" : names[0]
				}
			}
		});
	}

}

// 保存选择的分类信息
function saveChoooseCat(catId, catName, sourceId) {
	console.log('-----save cat-----', catId, catName, sourceId);
	vm.$set(sourceId + '.actionParam', {
		"searchParam" : {
			"cates" : {
				"catId" : catId,
				"catName" : catName
			}
		}
	});
}

// 保存选择的品牌信息
function saveChoooseBrand(brandId, brandName, sourceId) {
	console.log('-----save brand-----', brandId, brandName, sourceId);
	vm.$set(sourceId + '.actionParam', {
		"searchParam" : {
			"brands" : {
				"brandId" : brandId,
				"brandName" : brandName
			}
		}
	});
}

// 保存专题
function saveChoooseTheme(themeId, themePcImg, title, sourceId) {
	console.log('-----save theme-----', themeId, title, themePcImg, sourceId);
	vm.$set(sourceId + '.actionParam', {
		"searchParam" : {
			"themes" : {
				"themeId" : themeId,
				"themeName" : title,
				"themePcImg" : themePcImg
			}
		}
	});
}
