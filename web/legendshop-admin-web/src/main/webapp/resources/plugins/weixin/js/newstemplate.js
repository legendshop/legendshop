var currEditItem;//当前编辑的图文对象
var delIds = "";//需要删除的图文id

jQuery(document).ready(
		function(){
			KindEditor.options.filterMode=false;
			//kindeditor 文本编辑框
			 KindEditor.create('textarea[name="content"]', {
					uploadJson : contextPath+'/editor/uploadJson/upload;jsessionid=${cookie.SESSION.value}',
					fileManagerJson : contextPath+'/editor/uploadJson/fileManager',
					allowFileManager : true,
					afterBlur:function(){
						this.sync();
						$(currEditItem).find("input[name='content']").val($("#contentEdit").val());
					},
					afterFocus:function(){
							$(".js_content_error").hide();
					},
					width : '100%',
					height:'400px',
					afterCreate : function() {
						var self = this;
						KindEditor.ctrl(document, 13, function() {
							self.sync();
							document.forms['example'].submit();
						});
						KindEditor.ctrl(self.edit.doc, 13, function() {
							self.sync();
							document.forms['example'].submit();
						});
					}
				});
				prettyPrint();
				
				$("#coverEditA").click();
				
				$("#titleEdit").keyup(function(){
					var len = $("#titleEdit").val().length;
					$("#titleEdit").next().html(len+"/64");
					var title = $.trim($("#titleEdit").val());
					if(title==""){
						$(currEditItem).find(".v_title").html("标题");
					}else{
						$(currEditItem).find(".v_title").html($("#titleEdit").val());
					}
					$(currEditItem).find("input[name='title']").val(title);
					
					$(".js_title_error").hide();
				});
				
				$("#imgForContentEdit").click(function(){
					if($("#imgForContentEdit").prop("checked")){
						$(currEditItem).find("input[name='imgForContent']").val(1);
					}else{
						$(currEditItem).find("input[name='imgForContent']").val(0);
					}
				});
				
				$("#authorEdit").keyup(function(){
					$(currEditItem).find("input[name='author']").val($("#authorEdit").val());
					var len = $("#authorEdit").val().length;
					$("#authorEdit").next().html(len+"/8");
				});
				
				$("#descEdit").keyup(function(){
					$(currEditItem).find("input[name='desc']").val($("#descEdit").val());
					var len = $("#descEdit").val().length;
					$("#descEdit").next().html(len+"/120");
				});
				
				$("#urlEdit").keyup(function(){
					$(currEditItem).find("input[name='url']").val($("#urlEdit").val());
				});
		}
);

//添加动作
function addItem(){
	$(".news_preview_content").append($("#addItemDiv").html());
	$(".news_preview_content:last .edit_tool_a").click();
}

//删除动作
function delItem(obj){
	$(obj).parents(".news_item").remove();
	$("#coverEditA").click();
	var id = $(obj).parents(".news_item").find("input[name='id']").val();
	if(id!=""){
		if(delIds==""){
			delIds = id;
		}else{
			delIds+=","+id;
		}
	}
}

//编辑动作
function editItem(obj){
	$(".frm_msg.fail").hide();
	currEditItem = $(obj).parents(".news_item");
	var title = $(obj).parents(".news_item").find("input[name='title']").val();
	var author = $(obj).parents(".news_item").find("input[name='author']").val();
	var desc = $(obj).parents(".news_item").find("input[name='desc']").val();
	var content = $(obj).parents(".news_item").find("input[name='content']").val();
	var imgForContent = $(obj).parents(".news_item").find("input[name='imgForContent']").val();
	var url = $(obj).parents(".news_item").find("input[name='url']").val();
	var img = $(obj).parents(".news_item").find("input[name='img']").val();
	$("#titleEdit").val(title);
	$("#authorEdit").val(author);
	$("#descEdit").val(desc);
	$("#urlEdit").val(url);
	$(".upload_preview img").attr("src",photoPath+img);
	if(img!=""){
		$(".upload_preview").show();
	}else{
		$(".upload_preview").hide();
	}
	if(imgForContent==0){
		$("#imgForContentEdit").prop("checked",false);
	}else{
		$("#imgForContentEdit").prop("checked",true);
	}
	KindEditor.html("#contentEdit",content);
	var p = $(obj).parents(".news_item").position().top;
	$(".item_editor").css("margin-top",p+"px");
	
	setEditorVal();
}

//编辑封面图文动作
function editCoverItem(obj){
	$(".frm_msg.fail").hide();
	currEditItem = $(obj).parents(".cover_msg_area");
	var title = $(obj).parents(".cover_msg_area").find("input[name='title']").val();
	var author = $(obj).parents(".cover_msg_area").find("input[name='author']").val();
	var desc = $(obj).parents(".cover_msg_area").find("input[name='desc']").val();
	var content = $(obj).parents(".cover_msg_area").find("input[name='content']").val();
	var imgForContent = $(obj).parents(".cover_msg_area").find("input[name='imgForContent']").val();
	var url = $(obj).parents(".cover_msg_area").find("input[name='url']").val();
	var img = $(obj).parents(".cover_msg_area").find("input[name='img']").val();
	$("#titleEdit").val(title);
	$("#authorEdit").val(author);
	$("#descEdit").val(desc);
	$("#urlEdit").val(url);
	$(".upload_preview img").attr("src",photoPath+img);
	if(img!=""){
		$(".upload_preview").show();
	}else{
		$(".upload_preview").hide();
	}
	if(imgForContent==0){
		$("#imgForContentEdit").prop("checked",false);
	}else{
		$("#imgForContentEdit").prop("checked",true);
	}
	KindEditor.html("#contentEdit",content);
	$(".item_editor").css("margin-top","0px");
	
	setEditorVal();
}

function setEditorVal(){
	var len1 = $("#titleEdit").val().length;
	$("#titleEdit").next().html(len1+"/64");
	
	var len2 = $("#authorEdit").val().length;
	$("#authorEdit").next().html(len2+"/8");
	
	var len3 = $("#descEdit").val().length;
	$("#descEdit").next().html(len3+"/120");
}

//删除图片动作
function delImg(obj){
	$(obj).parent().hide();
	$(obj).prev().attr("src","");
	$(currEditItem).find("input[name='img']").val("");
	$(currEditItem).find("img").hide();
	$(currEditItem).find("i").show();
}

//保存
function saveNewsItems(){
	var id = $(".cover_msg_area").find("input[name='id']").val();
	var title = $(".cover_msg_area").find("input[name='title']").val();
	var author = $(".cover_msg_area").find("input[name='author']").val();
	var desc = $(".cover_msg_area").find("input[name='desc']").val();
	var content = $(".cover_msg_area").find("input[name='content']").val();
	var imgForContent = $(".cover_msg_area").find("input[name='imgForContent']").val();
	var url = $(".cover_msg_area").find("input[name='url']").val();
	var img = $(".cover_msg_area").find("input[name='img']").val();
	var newsTemplateid = $(".cover_msg_area").find("input[name='newsTemplateid']").val();
	var thumbMediaId = $(".cover_msg_area").find("input[name='thumbMediaId']").val();
	var createDate = $(".cover_msg_area").find("input[name='createDate']").val();
	var createUserId = $(".cover_msg_area").find("input[name='createUserId']").val();
	
	var errCount = 0;
	var errMsg = "";
	if($.trim(title)=="" || $.trim(content)=="" || $.trim(img)==""){
		$("#coverEditA").click();
		if($.trim(title)==""){
			$(".js_title_error").show();
			errMsg = "标题不能为空";
			errCount++;
		}
		
		if($.trim(content)==""){
			if(errMsg==""){
				errMsg = "正文不能为空";
			}
			$(".js_content_error").show();
			errCount++;
		}
		
		if($.trim(img)==""){
			if(errMsg==""){
				errMsg = "必须插入一张图片";
			}
			$(".js_img_error").show();
			errCount++;
		}
	}
	
	if(errCount>0){
		layer.msg(errMsg,{icon:0});
		location.href="#news_edit_area";
		return;
	}
	
	if(imgForContent==""){
		imgForContent = 0;
	}
	var data={
		"id":id,
		"title":title,
		"author":author,
		"desction":desc,
		"content":content,
		"imagepathContent":imgForContent,
		"url":url,
		"imagepath":img,
		"newsTemplateid":newsTemplateid,
		"thumbMediaId":thumbMediaId,
		"createDate":createDate,
		"createUserId":createUserId
	};
	
	var itemsData = new Array();
	itemsData.push(data);
	
	var itemList = $(".news_preview_content .news_item").get();
	for(var i=0;i<itemList.length;i++){
		var id = $(itemList[i]).find("input[name='id']").val();
		var title = $(itemList[i]).find("input[name='title']").val();
		var author = $(itemList[i]).find("input[name='author']").val();
		var desc = $(itemList[i]).find("input[name='desc']").val();
		var content = $(itemList[i]).find("input[name='content']").val();
		var imgForContent = $(itemList[i]).find("input[name='imgForContent']").val();
		var url = $(itemList[i]).find("input[name='url']").val();
		var img = $(itemList[i]).find("input[name='img']").val();
		var newsTemplateid = $(itemList[i]).find("input[name='newsTemplateid']").val();
		var thumbMediaId = $(itemList[i]).find("input[name='thumbMediaId']").val();
		var createDate = $(itemList[i]).find("input[name='createDate']").val();
		var createUserId = $(itemList[i]).find("input[name='createUserId']").val();
		if(imgForContent==""){
			imgForContent = 0;
		}
		
		var errCount = 0;
		var errMsg = "";
		if($.trim(title)=="" || $.trim(content)=="" || $.trim(img)==""){
			$(itemList[i]).find(".item_edit_a").click();
			if($.trim(title)==""){
				$(".js_title_error").show();
				errMsg = "标题不能为空";
				errCount++;
			}
			if($.trim(content)==""){
				if(errMsg==""){
					errMsg = "正文不能为空";
				}
				$(".js_content_error").show();
				errCount++;
			}
			
			if($.trim(img)==""){
				if(errMsg==""){
					errMsg = "必须插入一张图片";
				}
				$(".js_img_error").show();
				errCount++;
			}
		}
		
		
		if(errCount>0){
			layer.msg(errMsg,{icon:0});
			location.href="#news_edit_area";
			return;
		}
		
		var data1={
				"id":id,
				"title":title,
				"author":author,
				"desction":desc,
				"content":content,
				"imagepathContent":imgForContent,
				"url":url,
				"imagepath":img,
				"newsTemplateid":newsTemplateid,
				"thumbMediaId":thumbMediaId,
				"createDate":createDate,
				"createUserId":createUserId
			};
		itemsData.push(data1);
	}
	
	$.ajax({
		url: contextPath+"/admin/weixinNewstemplate/saveNewsItems", 
		data: {"newsItemsJson":JSON.stringify(itemsData),"delIds":delIds},
		type:'post', 
		dataType:'json',
		async : true, //默认为true 异步   
		success:function(data){
			if(data=="OK"){
				layer.msg("保存成功!",{icon:1,time:1000},function(){
					window.location=contextPath+"/admin/weixinNewstemplate/query";
				});
			}else{
				layer.msg("保存失败",{icon:2});
			}
		}   
	});
}

function imageDialog(){
	layer.open({
		  title :"选择图片",
		  type: 2, 
		  content: [contextPath+"/admin/weixinNewstemplate/imageDialog",'no'], //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
		  area: ['950px', '760px'],
		  btn: ['确定','关闭'], //按钮
		  yes: function(index, layero){
			  var selDiv =  $("#layui-layer-iframe"+index).contents().find(".img_item_bd.selected").get();
	        	if(selDiv.length==0){
	        		return false;
	        	}else{
	        		$(".upload_preview").show();
	        		$(".upload_preview img").attr("src",photoPath+$(selDiv).data("path"));
	        		$(currEditItem).find("img").attr("src",photoPath+$(selDiv).data("path"));
	        		$(currEditItem).find("input[name='img']").val($(selDiv).data("path"));
	        		$(currEditItem).find("img").show();
	        		$(currEditItem).find("i").hide();
	        		$(".js_img_error").hide();
	        		layer.close(index);
	        	}
		  }
		}); 
}

function uploadOneFile(){
	$("#js_upload_btn").click();
	$('#js_upload_btn').change(function() {
		$('#js_appmsg_upload_cover').html('上传中...');
        $('#formFile').submit();
	});
}

function uploadSuccess(fileName) {
	$('#js_appmsg_upload_cover').html('本地上传');
	$(".upload_preview").show();
	$(".upload_preview img").attr("src",photoPath+fileName);
	$(currEditItem).find("img").attr("src",photoPath+fileName);
	$(currEditItem).find("input[name='img']").val(fileName);
	$(currEditItem).find("img").show();
	$(currEditItem).find("i").hide();
	$(".js_img_error").hide();
}