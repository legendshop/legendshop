<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/jqueryui/css/jquery.ui.all.css'/>">
     <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.core.js'/>"  type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.widget.js'/>"  type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.dialog.js'/>"  type="text/javascript"></script>

<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.draggable.js'/>"  type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.mouse.js'/>"  type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.position.js'/>"  type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.resizable.js'/>"  type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.sortable.js'/>"  type="text/javascript"></script>
<link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/jqueryui/css/demos.css'/>">
<html>
<head>
	<title>jQuery UI Dialog - Default functionality</title>

	<script>
	jQuery(document).ready(function(){	
			$( "#sortable" ).sortable();
			$( "#sortable" ).disableSelection();
	});
	</script>
</head>
<body>

<div class="demo">

<ul id="sortable">
	<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item 1</li>
	<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item 2</li>
	<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item 3</li>
	<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item 4</li>
	<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item 5</li>
	<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item 6</li>
	<li class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>Item 7</li>
</ul>

<div id="dialog" title="Basic dialog">
	<p>This is the default dialog which is useful for displaying information. The dialog window can be moved, resized and closed with the 'x' icon.</p>
</div>
</div><!-- End demo -->


</body>
</html>
