<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8" isErrorPage="true" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<script type="text/javascript">
			function refreshPage() {
				window.top.location.href = "${contextPath}/admin/index";
			}
			setTimeout('refreshPage()', 5000); //指定5秒刷新一次
		</script>
		<title>访问受限</title>
</head>
<body>
	<div class="myf_con clearfix">
		<h3>您没有权限访问当前页面，请联系管理员</h3>
	</div>
</body>
</html>