<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" isErrorPage="true"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>消息页面</title>
<style type="text/css">
	*{
		margin:0;
		padding:0;
	}
	#main{
		width:690px;
		height:auto;
		margin: 15% auto;
		overflow:hidden;
	}
	#left{
		float:left;
		padding:10px 10px;
	}
	#right{
		float:right;
		width:500px;
		margin-left:10px;
	}
	#right h1{
		padding:10px 0;
		color:red;
	}
	#right h3{
		padding-bottom:10px;
		color:#333;
		font-weight:none;
	}
	#right ul{
		list-style:none;
	}
	#right ul li{
		height:25px;
		line-height:25px;
	}
</style>
</head>
<body>
	<div id="main">
		<div id="left">
			<img alt="警告" src="<ls:templateResource item='/resources/common/images/warning.jpg'/>" width="140" height="120"/>
		</div>
		<div id="right">
			<h1>拒绝访问!</h1>
			<h3>原因: ${errorMessage }</h3>
			<ul>
				<li>1.进入<a href="${contextPath}">首页</a></li>
				<li>2.联系管理员</li>
			</ul>
		</div>
	</div>
</body>
</html>