<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/jquery.jsp" %>
<HTML>
<HEAD>
    <c:set var="systemConfig" value="${indexApi:getSystemConfig()}"></c:set>
    <c:set var="photoPrefix" value="${shopApi:getPhotoPrefix()}"></c:set>
	<c:set var="imagesPrefix" value="${shopApi:getImagesPrefix(1)}"></c:set>
	<c:set var="imagesSuffix" value="${shopApi:getImagesSuffix(1)}"></c:set>
    <title>图片空间-${systemConfig.shopName}</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/ztree/zTreeStyle.css'/>" type="text/css">
    <link rel="stylesheet" href="<ls:templateResource item='/resources/common/css/imageSpace.css'/>" type="text/css">
    <%@ include file="/WEB-INF/pages/common/jquery.jsp" %>
    <script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.js'/>"></script>
    <script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.exedit-3.5.js'/>"></script>
    <script type='text/javascript' src="<ls:templateResource item='/resources/common/js/ajaxfileupload.js'/>"></script>
    <script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>"></script>
    <link href="${contextPath}/resources/common/css/pager.css" rel="stylesheet" type="text/css"/>
    <SCRIPT type="text/javascript">
        var contextPath = '${contextPath}';
        var imagesPrefix = '${imagesPrefix}';
        var imagesSuffix = '${imagesSuffix}';
        $(document).ready(function () {
            $.fn.zTree.init($("#treeDemo"), setting);
            $("#selectAll").bind("click", selectAll);
            ajaxpic(1, 0);
            
            $("#upload").click(function () {
                if ($("#upload").attr("uploadStatus") == "1") {
                    layer.msg('正在上传图片，请稍后!', {icon: 0});
                    return false;
                }
                var files = document.getElementById("files").files;
                if (files.length == 0) {
                    layer.msg('请选择图片!', {icon: 0});
                    return false;
                }
                if (files.length > 30) {
                    layer.msg('一次最多上传30张!', {icon: 0});
                    return false;
                }
                for (var i = 0; i < files.length; i++) {
                    var file = files[i].name;
                    if (!/\.(JPEG|BMP|GIF|JPG|PNG)$/.test(file.toUpperCase())) {
                        
                        layer.alert('仅支持JPG、GIF、PNG、JPEG、BMP格式，请重新选择！', {icon: 0});
                        $("#files").val("");
                        return false;
                    }
                    //限制单张图片大小
                    if (files[i].size > 1 * 1024 * 1024) {
                        layer.alert('单张图片不能超过1MB！', {icon: 0});
                        $("#files").val("");
                        return false;
                    }
                }
                upload();

// 	    	status=0;
// 	    	art.dialog.alert("第二次--"+status);
            });
        });

    </SCRIPT>
    <script type='text/javascript' src="<ls:templateResource item='/resources/common/js/imageSpace.js'/>"></script>
</HEAD>

<body>

<div class="top_div">
    <h1>图片空间</h1>
</div>

<div class="content_wrap">


    <!-- 右侧图片区 -->
    <div class="right_div" style="width:1270px;">
        <!-- 右侧头部按钮区 -->
        <div class="pic_btn">图片名称：
            <input id="searchName" type="text" class="input_text" placeholder="图片名称..."/>
            <c:if test="${empty type}">&nbsp;商店名称：
            <input id="shopName" type="text" class="input_text" placeholder="商店名称..."/>
            </c:if>
            <input id="searchBtn" onclick="searchPic();" type="button" value="搜索"/>

            <%--<input id="uploadPic" onclick="uploadPic();" type="button" value="上传图片"/>--%>

            <input id="delPic" onclick="delPic();" type="button" value="删除图片" style="display:none"/>
            <input id="renamePic" onclick="renamePic();" type="button" value="重命名" style="display:none"/>

            <!-- 右侧中间 图片显示区 -->
            <div style="height:708px;margin-top:8px;background: #f0f0f0;">
                <div id="main-div"></div>
                <input type="hidden" id="curPageNO" name="curPageNO"/>
            </div>
            <!-- 右侧底部分页区 -->
            <div class="bottom_div">
                <div id="toolBar"></div>
            </div>
        </div>
    </div>

</body>
</HTML>