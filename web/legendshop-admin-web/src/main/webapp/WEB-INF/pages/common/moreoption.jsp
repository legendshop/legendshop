<%@ page language="java" isErrorPage="true" contentType="text/html;charset=UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<table cellSpacing="0" cellPadding="0" width="955px" align="center">
	<tr>
		<td>
			<BLOCKQUOTE>
				<OL>
					<UL type="square">
						<c:forEach items="${User_Messages.callBackList}" var="callback">	
							<li>
							<c:choose>
								<c:when test="${callback.callBackHref == null}">
									${callback.callBackTitle}
								</c:when>
								<c:otherwise><a href="${contextPath}/${callback.callBackHref}"><b>${callback.callBackTitle}</b></a></c:otherwise>
							</c:choose>
								 ${callback.callBackDesc}
							</li>
						</c:forEach>
						<br></br>

						<LI>
							<A href="${contextPath}"><b>首页</b></A> - 到当前店铺首页
						</LI>
						<LI>
							<A href="${contextPath}/reg"><b>注册</b></A> - 注册一个用户或者商家
						</LI>
						<LI>
							<A href="${contextPath}/login"><b>登录</b></A> - 使用用户名和密码登录系统
						</LI>
					</UL>
				</OL>
				<P></P>
			</BLOCKQUOTE>
		</td>
	</tr>
</table>