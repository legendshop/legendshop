<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%-- Error Messages --%>
<style>
.springMessages{
	background-color: #3bb4f2;
    color: white;
    height: 30px;
    padding-top: 5px;
    text-align: center;
    width: 99%;
}
</style>
<c:choose>
	<c:when test="${not empty springMessages}">
		<div class="springMessages">
        <c:forEach var="msg" items="${springMessages}" end="0">
				<span><i class="am-icon-info"></i>&nbsp;${msg}<br/></span>
      	  </c:forEach>
		</div>    
	
		<%
		session.removeAttribute("springMessages");
		 %>
	</c:when>
	<c:otherwise>
		<%-- Info Messages --%>
		<c:if test="${not empty springErrors}">
			<div class="error springMessages">
		        <c:forEach var="errorMsg" items="${springErrors}" end="0">
					<img src="${contextPath}/resources/common/images/iconWarning.gif" alt="Warning"/>${errorMsg}<br/>
		        </c:forEach>
			</div>    
			<%
				session.removeAttribute("springErrors");
			 %>
		</c:if>
	</c:otherwise>
</c:choose>





