 <%
   if(request.getAttribute("JqueryApplied") == null){
 %>
<script src="${contextPath}/resources/common/js/jquery-1.9.1.min.js" type="text/javascript"></script>
<script type="text/javascript">	
		$(document).ajaxSend(function(e, xhr, options) { var _csrf_header = '${_csrf.headerName}';  var _csrf = '${_csrf.token}';  if(_csrf != null && _csrf_header != null){ if(xhr.setRequestHeader) {xhr.setRequestHeader(_csrf_header, _csrf); } }});
		function appendCsrfInput(){var _csrf_header = '${_csrf.parameterName}'; var _csrf = '${_csrf.token}'; var hiddenField = document.createElement("input");if(_csrf != null && _csrf_header != null){hiddenField.setAttribute("type", "hidden");hiddenField.setAttribute("name", _csrf_header);hiddenField.setAttribute("value", _csrf);} return hiddenField;}
</script>
<%
	request.setAttribute("JqueryApplied", true);
}
 %>