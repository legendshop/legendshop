<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form"  prefix="form"%>  
<%@ taglib uri="http://www.legendesign.net/tags" prefix="ls"%>
<%@ taglib uri="http://www.legendesign.net/biz" prefix="lb"%>
<%@ taglib uri="http://www.legendesign.net/index-api"  prefix="indexApi" %>
<%@ taglib uri="http://www.legendesign.net/bottom-api"  prefix="bottomApi" %>
<%@ taglib uri="http://www.legendesign.net/product-api" prefix="prodApi"%>
<%@ taglib uri="http://www.legendesign.net/shop-api"  prefix="shopApi" %>

<%
  request.setAttribute("contextPath", request.getContextPath());
 // request.setAttribute("_style_", "-green");
  request.setAttribute("_style_", "");
%>
