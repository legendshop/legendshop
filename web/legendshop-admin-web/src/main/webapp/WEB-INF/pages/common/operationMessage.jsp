<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/plugins/main/included/top-top.jsp'%>
<style>
   .paySuccessBox { width: 500px;margin: 0 auto;padding: 150px;padding-left:50px;link-height: 200px;text-align: center;} 
   .paySuccessBox ul li{font-size: 14px; font-family: "微软雅黑"}
   .paySuccessBox .return{margin-top: 20px;}
</style>
<!--支付成功-->
<div class="box paySuccessBox">
    	<ul>
            <li>
            	<p>${message }</p>
                <div class="return">
                    <h4>您现在还可以：<a href='${contextPath}/'>返回首页</a>&nbsp;&nbsp;&nbsp;<a href="${contextPath}/p/myorder?uc=uc">订单中心</a></h4>
                </div>
            </li><div class="clear"></div>
        </ul>
</div>
<%@ include file="/WEB-INF/pages/plugins/templetRed/jsp/frontend/templetRed/included/bottom.jsp" %>