<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<HTML>
<HEAD>
<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
<title>视频空间-${systemConfig.shopName}</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	
	  <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/ztree/zTreeStyle.css'/>" type="text/css">
	  <link rel="stylesheet" href="<ls:templateResource item='/resources/common/css/imageSpace.css'/>" type="text/css">
	  <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
	  <script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.js'/>"></script>
	<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.exedit-3.5.js'/>"></script>
	<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/ajaxfileupload.js'/>"></script>
	<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>"></script>
	<link href="${contextPath}/resources/common/css/pager.css" rel="stylesheet" type="text/css" />
	<SCRIPT type="text/javascript">
	var contextPath = '${contextPath}';
	var imgPath1 = "<ls:images item='' scale='1'/>";
	var imagePrefix = "${imagePrefix}";
	var imageSuffix = "${imageSuffix}";
	console.log(imagePrefix+"===="+imageSuffix);
    $(document).ready(function(){
		$.fn.zTree.init($("#treeDemo"), setting);
		$("#selectAll").bind("click", selectAll);
		ajaxpic(1,0);
	});
    
	</SCRIPT>
	<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/imageSpace.js?v=1333'/>"></script>
</HEAD>

<body>

<div class="top_div">
	<h1>图片空间</h1>
</div>

<div class="content_wrap">
	<c:if test="${not isB2b2cAdmin}">
		<!-- 左侧分类树 -->
		<div class="zTreeDemoBackground left">
			<ul id="treeDemo" class="ztree"></ul>
			<div style="height:0px;clear:both;"></div>
		</div>
	</c:if>
	
	<!-- 右侧图片区 -->
	<div class="right_div" <c:if test="${isB2b2cAdmin}">style="width:1270px;"</c:if> >
		<!-- 右侧头部按钮区 -->
		<div class="pic_btn"> 
		 	<c:if test="${isB2b2cAdmin}">
		 		商家：<input id="shopName" type="text" class="input_text" placeholder="商家名称..."/>
		 	</c:if>
	 		图片名称：<input id="searchName" type="text" class="input_text" placeholder="图片名称..."/>
	 		<input id="searchBtn" onclick="searchPic();" type="button" value="搜索" />
	 		<c:if test="${not isB2b2cAdmin}">
				<input id="uploadPic" onclick="uploadPic();" type="button" value="上传图片" />
			</c:if>
		 	<input id="delPic" onclick="delPic();" type="button" value="删除图片" style="display:none"/>
		 	<input id="renamePic" onclick="renamePic();" type="button" value="重命名" style="display:none"/>
		 
		 <!-- 右侧中间 图片显示区 -->
		<div style="height:708px;margin-top:8px;background: #f0f0f0;">
			<div id="main-div"></div>
			<input type="hidden" id="curPageNO" name="curPageNO" />
        </div>
        <!-- 右侧底部分页区 -->
		<div class="bottom_div">
			<div id="toolBar"></div>
		</div>
	</div>
</div>

<!-- 图片上传弹窗 -->
<div id="uploadDiv">
	<div class="closeBtn " onclick="closeBtn();">×</div>
	<div class="aui_upload" >图片上传</div>
	<div style="margin-top: 30px;margin-left:20px;">
		  <form:form  action="${contextPath}/imageAdmin/uploadImg" method="post" id="form1" enctype="multipart/form-data">
		  	<input name="files" id="files" type="file"  multiple="true"">(按住ctrl多选)
			<input type="hidden" id="currId" name="treeId" value="0"/>
		  	<div align="center"><input  class="btn_upload"  type="button" id="upload" uploadStatus="0"  value="批量上传"></div>
		  </form:form>
	</div>
	
</div>

<div id="backDiv"></div>
<!-- <div class="black_overlay">正在加载图片</div> -->
<script type="text/javascript">
    /**图片格式验证**/
	$(document).ready(function(){
	    $("#upload").click(function(){
	    	if( $("#upload").attr("uploadStatus")=="1"){
		    	layer.msg('正在上传图片，请稍后!', {icon: 2});
		    	return false;
	    	}
	    	var files = document.getElementById("files").files;
	    	if(files.length == 0){
	    		layer.msg('请选择图片!', {icon: 2});
	    		return false;
	    	}
	    	if(files.length >30 ){
	    		layer.msg('一次最多上传30张!', {icon: 2});
	    		return false;
	    	}
	    	for(var i=0;i<files.length;i++){
	    		var file = files[i].name;
	    		if (!/\.(JPEG|BMP|GIF|JPG|PNG)$/.test(file.toUpperCase())) {
	    			layer.msg('仅支持JPG、GIF、PNG、JPEG、BMP格式，请重新选择！', {icon: 2});
					$("#files").val("");
					return false;
				}
				//限制单张图片大小
				if (files[i].size >1*1024*1024) {
	    			layer.msg('单张图片不能超过1MB！', {icon: 2});
					$("#files").val("");
					return false;
				}
	    	}
	    	upload();
	    	
// 	    	status=0;
// 	    	art.dialog.alert("第二次--"+status);
	    })
	})
</script>

</body>
</HTML>