<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
	<title>售后弹框页面</title>
</head>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<link type="text/css" href="<ls:templateResource item='/resources/templets/red/css/afterSaleOverlay.css'/>" rel="stylesheet"/>
<link href="${contextPath}/resources/templets/red/css/pager.css" rel="stylesheet" type="text/css" />
<body>
	<div id="content">
		<form:form  action="" id="form1">
	    <div class="c-t"></div>
		<div class="title">
		    <h1>售后说明:</h1>
		    <button id="J_NewTemplate" type="button" onclick="newTemplate();">新增说明模板</button>
	    </div>
	    <div id="J_TplBox" class="templates">
	    		<c:forEach items="${requestScope.list}" var="afterSale" varStatus="status">
			    			<div id="J_Tpl_${afterSale.id}" class="J_Tpl template box saved"  afterSaleId ="${afterSale.id}" afterSaleName ="${afterSale.title}">
				                <div class="hd">
				                    <h2>
				    					<input maxlength="20" title="${afterSale.title}" id="J_NameEdit" value="${afterSale.title}" class="name">
				    					<span id="J_NameShow">${afterSale.title}</span>
				    				</h2>
				                </div>
				
				                <div class="bd">
										<div id="J_ConShow" class="result">${afterSale.content}</div>
				                    	<textarea title="填写承诺内容" id="J_ConEdit" class="content J_ConEdit">${afterSale.content}</textarea>
					                    <div class="apply">
					    					<a class="J_Apply" href="javascript:void(0);" onclick="appTemplate(this);">
					    						<span>应用该模板</span>
					    					</a>
					    				</div>
					                    <div class="action">
					    					<div class="letter-indicate">您还可以输入 <em id="J_LetterNum"></em> 字的内容</div>
					    					<button id="J_Cancel" class="J_Cancel" type="button" onclick="cancelEdit(this);">取消</button>
					    					<button class="J_Save" id="J_Save" type="button" onclick="saveEdit(this);">保存</button>
					    				</div>
				                </div>
				
				                <div class="ft">
				                    <span class="notice">卖家承诺所填写的内容，不得与国家相关法律，法规相关规定有冲突！</span>
				                    <div class="action">
				                        <a class="J_Edit" href="javascript:void(0);" onclick="editTemplate(this);">修改</a> - <a type="submit" class="J_Delete" href="javascript:void(0);" onclick="delTemplate(this);">删除</a>
				                    </div>
				                </div>
			    		</div>
	    		</c:forEach>
	    		
	    </div>
	    <div class="c-b"></div>
	    	<div class="clear"></div>
				<c:if test="${toolBar!=null}">
						<p style="float: right;">
							<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
						</p>
				</c:if>    

	</div>

</body>

</html>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/afterSaleOverlay.js'/>"></script>
<script language="JavaScript" type="text/javascript">
var contextPath="${contextPath}";
</script>
