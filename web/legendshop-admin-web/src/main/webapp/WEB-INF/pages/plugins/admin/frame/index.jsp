<!doctype html>
<%@ page language="java" pageEncoding="UTF-8" %>
<html class="no-js fixed-layout">
<head>
  <%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
   <%@ include file="/WEB-INF/pages/common/jquery2.1.4.jsp"%>
  <% 
  	int port = request.getServerPort();
  	String domainPath = "";
  	if(port==80){
  		domainPath = request.getScheme()+"://"+request.getServerName();
  	}else{
  		domainPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort();
  	}
  	//记录当前页
  	Integer currentMenuId = (Integer)request.getSession().getAttribute("currentMenuId");
  	if(currentMenuId == null){
  		request.getSession().setAttribute("currentMenuId", 0);
  	}
	%>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/amazeui.css"/>
  <link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/admin.css">
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script src="<ls:templateResource item='/resources/common/js/alternative.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/cookieUtil.js'/>" type="text/javascript"></script>
 <script src="<ls:templateResource item='/resources/common/js/recordstatus.js'/>" type="text/javascript"></script>
 <script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
 <script type="text/javascript">
     var contextPath = '${contextPath}';
 	var domainPath = '<%=domainPath%>';
 	var ua = navigator.userAgent;
 	clickType = (ua.match(/iPad/i)) ? "touchstart" : "click";
 </script>
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，Amaze UI 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->

<jsp:include page="/admin/top" />
<div class="am-cf admin-main">
  <!-- sidebar start -->
  <div class="admin-sidebar am-offcanvas" id="admin-offcanvas">
  		<jsp:include page="/admin/menuContent/${currentMenuId}" />
  </div>
  <!-- sidebar end -->
	<!-- content start -->
	  <div class="admin-content" id="admin-content" style="overflow-x:auto;">
			<jsp:include page="../dashboard/indexCenter.jsp"></jsp:include>
	  </div>
  <!-- content end -->
</div>
<a href="#" class="am-icon-btn am-icon-th-list am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}"></a>
<script src="<ls:templateResource item='/resources/templets/amaze/js/legendshop.js'/>"></script>
</body>
</html>