<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
 <table class="${tableclass}" style="width: 100%">
 		<style>
 		</style>
	     <thead>
	    	<tr><th>
	    	<strong class="am-text-primary am-text-lg">拍卖活动</strong> / 拍卖活动详情
			</th>
	    	</tr>
	    </thead>
	    </table>
	    <div align="center" >
         <table class="prodInfo" style="border-collapse:separate; border-spacing:10px; font-size: 13px;" cellpadding="3px">
         	<tr >
         		<td rowspan="7"><a target="_blank" href="${contextPath}/views/${auction.prodId}"><img src="<ls:images item='${auction.prodPic}' scale='1'/>"></a></</td>
         		<td></td>
         	</tr>
         	<tr>
         		<td>商品名称:</td>
         		<td><a target="_blank" href="${contextPath}/views/${auction.prodId}">${auction.prodName}</a></td>
         	</tr>
         	<tr>
         		<td>商品价格:</td>
         		<td>￥${auction.prodPrice}</td>
         	</tr>
         	<tr>
         		<td>起止时间:</td>
         		<td><fmt:formatDate value="${auction.startTime}"  pattern="yyyy-MM-dd HH:mm:ss"/>&nbsp;—&nbsp;<fmt:formatDate value="${auction.endTime}"  pattern="yyyy-MM-dd HH:mm:ss"/></td>
         	</tr>
         	<tr>
         		<td>起拍价:</td>
         		<td>${auction.floorPrice}  　　　  加价幅度：${auction.minMarkupRange}&nbsp;—&nbsp;${auction.maxMarkupRange}</td>
         	</tr>
         	<tr>
         		<td>是否需要保证金:</td>
         		<td>
         			<c:choose>
         				<c:when test="${auction.isSecurity eq 0}">
         					否
         				</c:when>
         				<c:otherwise>是&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;保证金：${auction.securityPrice}元&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;延迟时间:${auction.delayTime}分钟
         				</c:otherwise>
         			</c:choose>        
         		</td>
         	</tr>
         	<tr>
         		<td><c:if test="${auction.isCeiling ==1 }">一口价:${auction.fixedPrice}</c:if></td>
         		<td>竞卖方:${auction.shopName }
         		</td>
         	</tr>
         </table>
          <div style="margin:20px 0;padding:10px 0;border: 1px solid #e4e4e4;">					
					<div> 拍卖状态：
							<c:choose>
								<c:when test="${auction.status==2 and not empty userName}"><b style="color:#e5004f;">中标用户：&nbsp;&nbsp;****${fn:substring(userName,fn:length(userName)-2,-1)}</b></c:when>
								<c:when test="${auction.status==2 and empty userName}">商品已流拍</c:when>
								<c:when test="${auction.status==1}">商品上线中</c:when> 
								<c:otherwise>商品未拍卖</c:otherwise>
							</c:choose>
					</div>
		</div>
         <div id="auctionDetail">
        	
         </div>
        
          <table  style="width: 100%" class="${tableclass}" id="col1">
         	<tr align="center">
         		<td colspan="4"><b>拍卖详情</b></td>
         	</tr>
         	<tr>
         		<td><center>${auction.auctionsDesc}</center> </td>
         	</tr>
         </table>
         
         </div>
<script language="javascript">
	$().ready(function(){
		var url="${contextPath}/admin/auction/bidList/"+${auction.id}+"?curPageNO=1";
				$("#auctionDetail").load(url);
	});
	
	
	 function pager(curPageNO){
	 	var url="${contextPath}/admin/auction/bidList/"+${auction.id}+"?curPageNO="+curPageNO;
				$("#auctionDetail").load(url);
		}
</script>
