<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>


<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${sessionScope.SPRING_SECURITY_LAST_USERNAME}- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/common/css/errorform.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/templets/amaze/css/viewBigImage.css" />
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<style>
/* .am-table > thead > tr > td, .am-table > tbody > tr > td, .am-table > tfoot > tr > td {
    height: 40px !important;
} */
</style>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">业务设置&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">基本配置</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/system/systemConfig/save"
				method="post" id="form1" enctype="multipart/form-data">
				<input id="id" name="id" value="${systemConfig.id}" type="hidden">
				<div align="center">
					<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
					<table border="0" align="center" class="${tableclass} no-border content-table" id="col1"
						style="width: 100%;margin-top: 20px;">
						<tr>
							<td >
								<div align="right">
									<font color="ff0000">*</font>商城名称：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" name="shopName" id="shopName"
								value="${systemConfig.shopName}" class="${inputclass}"
								style="width: 300px" /></td>
						</tr>
						<tr>
							<td >
								<div align="right">
									<font color="ff0000">*</font>商城域名：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" name="url" id="url"
								value="${systemConfig.url}" class="${inputclass}"
								style="width: 300px" /></td>
						</tr>
						<tr>
							<td >
								<div align="right">
									<font color="ff0000">*</font>支持邮箱： 
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" name="supportMail" id="supportMail"
								value="${systemConfig.supportMail}" class="${inputclass}"
								style="width: 300px" />&nbsp;&nbsp;&nbsp;<span style="color:#999;">多个请用英文逗号隔开</span></td>
						</tr>
						<tr>
							<td >
								<div align="right">
									<font color="ff0000"></font>联系方式： 
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><font>+</font> <input type="text"
								name="internationalCode"
								value="${systemConfig.internationalCode}" class="${inputclass}"
								style="width: 50px" maxlength="5" /> <input type="text"
								name="areaCode" value="${systemConfig.areaCode}"
								class="${inputclass}" style="width: 50px" maxlength="5" /> <input style="width:175px;"
								type="text" name="telephone" id="telephone"
								value="${systemConfig.telephone}" class="${inputclass}"
								maxlength="20" /> <font style="color:#999;">国际电话号码的正确写法：国家代码+地区代码(区号)+电话号码</font></td>
						</tr>
						<tr>
							<td >
								<div align="right">支持QQ号码：</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" name="qqNumber" id="qqNumber"
								value="${systemConfig.qqNumber}" class="${inputclass}"
								style="width: 300px" />&nbsp;&nbsp;&nbsp;<span style="color:#999;">多个请用英文逗号隔开</span></td>
						</tr>
						<tr>
							<td <c:if test="${systemConfig.adminLogoCode!=null}">valign="top"</c:if>>
								<div align="right">
									<font color="ff0000"></font>后台Logo：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<input type="file" name="adminLogoFile" id="adminLogoFile" />
								<c:if test="${systemConfig.adminLogoCode!=null}">
									<div class="zoombox">
										<span class="photoBox">
											<div class="loadingBox">
												<span class="loading"></span>
											</div> <img
												src="<ls:photo item='${systemConfig.adminLogoCode}'/>"
												class="zoom" onclick="zoom_image($(this).parent());"
												style="max-height: 100px; max-width: 150px;">
										</span>

										<div class="photoArea" style="display: none;">
											<p>
												<img src="about:blank" class="minifier"
													 onclick="zoom_image($(this).parent().parent());" />
											</p>
											<p class="toolBar gc">
													<span><a class="green" href="javascript:void(0)"
															 onclick="zoom_image($(this).parent().parent().parent());">收起</a></span>|<span
													class="view"><a class="green"
																	href="<ls:photo item='${systemConfig.adminLogoCode}'/>"
																	target="_blank">查看原图</a></span>
											</p>
										</div>
									</div>
								</c:if>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<div align="right">
									 <font color="ff0000"></font>微信订阅号二维码(大小600*600)：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<input type="file" name="wechatCodeFile" id="wechatCodeFile" />
								<c:if test="${wechatCodeVO.wechatOrderCode!=null}">
									<div class="zoombox">
										<span class="photoBox">
											<div class="loadingBox">
												<span class="loading"></span>
											</div> <img
											src="<ls:photo item='${wechatCodeVO.wechatOrderCode}'/>"
											class="zoom" onclick="zoom_image($(this).parent());"
											style="max-height: 100px; max-width: 150px;">
										</span>

										<div class="photoArea" style="display: none;">
											<p>
												<img src="about:blank" class="minifier"
													onclick="zoom_image($(this).parent().parent());" />
											</p>
											<p class="toolBar gc">
												<span><a class="green" href="javascript:void(0)"
													onclick="zoom_image($(this).parent().parent().parent());">收起</a></span>|<span
													class="view"><a class="green"
													href="<ls:photo item='${wechatCodeVO.wechatOrderCode}'/>"
													target="_blank">查看原图</a></span>
											</p>
										</div>
									</div>
								</c:if>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<div align="right" >
									<font color="ff0000"></font>微信服务号二维码(大小600*600)：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="file" name="wechatServiceCodeFile"
								id="wechatServiceCodeFile" /> <input type="hidden"
								name="wechatCode" id="wechatCode"
								value='${systemConfig.wechatCode}' />
								<c:if test="${wechatCodeVO.wechatServiceCode!=null}">
									<div class="zoombox">
										<span class="photoBox">
											<div class="loadingBox">
												<span class="loading"></span>
											</div> <img
											src="<ls:photo item='${wechatCodeVO.wechatServiceCode}'/>"
											class="zoom" onclick="zoom_image($(this).parent());"
											style="max-height: 100px; max-width: 150px;">
										</span>

										<div class="photoArea" style="display: none;">
											<p>
												<img src="about:blank" class="minifier"
													onclick="zoom_image($(this).parent().parent());" />
											</p>
											<p class="toolBar gc">
												<span><a class="green" href="javascript:void(0)"
													onclick="zoom_image($(this).parent().parent().parent());">收起</a></span>|<span
													class="view"><a class="green"
													href="<ls:photo item='${wechatCodeVO.wechatServiceCode}'/>"
													target="_blank">查看原图</a></span>
											</p>
										</div>
									</div>
								</c:if>
							</td>
						</tr>
						<!-- <tr>
							<td>
								<div align="center">
									微信头条 <font color="ff0000"></font>
								</div>
							</td>
							<td><input type="file" name="wechatTitlePicFile"
								id="wechatTitlePicFile" /> <input type="hidden"
								name="wechatTitlePic" id="wechatTitlePic"
								value="${systemConfig.wechatTitlePic}" /> <c:if
									test="${systemConfig.wechatTitlePic!=null}">
									<div class="zoombox">
										<span class="photoBox">
											<div class="loadingBox">
												<span class="loading"></span>
											</div> <img src="<ls:photo item='${systemConfig.wechatTitlePic}'/>"
											class="zoom" onclick="zoom_image($(this).parent());"
											style="max-height: 100px; max-width: 150px;">
										</span>

										<div class="photoArea" style="display: none;">
											<p>
												<img src="about:blank" class="minifier"
													onclick="zoom_image($(this).parent().parent());" />
											</p>
											<p class="toolBar gc">
												<span><a class="green" href="javascript:void(0)"
													onclick="zoom_image($(this).parent().parent().parent());">收起</a></span>|<span
													class="view"><a class="green"
													href="<ls:photo item='${systemConfig.wechatTitlePic}'/>"
													target="_blank">查看原图</a></span>
											</p>
										</div>
									</div>
								</c:if></td>
						</tr> -->
						<tr>
							<td valign="top">
								<div align="right" >
									<font color="ff0000">*</font>Logo标识(大小315*90)：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<input type="file" name="file" id="file" />
								<input type="hidden" name="logo" id="logo" value="${systemConfig.logo}" class="${inputclass}" style="width: 300px" />
								<c:if test="${systemConfig.logo!=null}">
									<div class="zoombox">
										<span class="photoBox">
											<div class="loadingBox">
												<span class="loading"></span>
											</div> <img src="<ls:photo item='${systemConfig.logo}'/>"
											class="zoom" onclick="zoom_image($(this).parent());"
											style="max-height: 100px; max-width: 150px;">
										</span>

										<div class="photoArea" style="display: none;">
											<p>
												<img src="about:blank" class="minifier"
													onclick="zoom_image($(this).parent().parent());" />
											</p>
											<p class="toolBar gc">
												<span><a class="green" href="javascript:void(0)"
													onclick="zoom_image($(this).parent().parent().parent());">收起</a></span>|<span
													class="view"><a class="green"
													href="<ls:photo item='${systemConfig.logo}'/>"
													target="_blank">查看原图</a></span>
											</p>
										</div>
									</div>
								</c:if>
							</td>
						</tr>
						<tr>
							<td width="250px;">
								<div align="right">
									<font color="ff0000">*</font>icp信息 ：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<textarea name="icpInfo" id="icpInfo" cols="150" rows="8" maxlength="5000" >${systemConfig.icpInfo}</textarea>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<div align="right">SEO标题：</div>
							</td>
							<td align="left" style="padding-left: 2px;"><textarea cols="77" rows="8" maxlength="200" style="width: 300px;"
									class="${inputclass}" name="title" class="text-input"
									id="title">${systemConfig.title}</textarea></td>
						</tr>


						<tr>
							<td valign="top">
								<div align="right">SEO关键字：</div>
							</td>
							<td align="left" style="padding-left: 2px;"><textarea cols="77" rows="8" maxlength="200" style="width: 300px;"
									class="${inputclass}" name="keywords" class="text-input"
									id="keywords">${systemConfig.keywords}</textarea></td>
						</tr>

						<tr>
							<td valign="top">
								<div align="right">SEO描述：</div>
							</td>
							<td align="left" style="padding-left: 2px;"><textarea cols="77" rows="8" maxlength="200" style="width: 300px;"
									class="${inputclass}" name="description" class="text-input"
									id="description">${systemConfig.description}</textarea></td>
						</tr>


						<tr>
							<td></td>
							<td align="left" style="padding-left: 2px;">
								<div style="margin-bottom: 60px;">
									<input type="submit" value="保存" class="${btnclass}" /> 
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="${contextPath}/resources/templets/amaze/js/viewBigImage.js"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/systemConfig.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	var contextPath = "${contextPath}";
</script>
</html>