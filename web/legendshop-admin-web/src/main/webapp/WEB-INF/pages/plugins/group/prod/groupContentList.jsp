<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}" pattern="yyyy-MM-dd HH:mm:ss"
	var="nowDate" />
<fmt:formatDate value="${nowTime}" pattern="yyyy-MM-dd" var="nowDay" />
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	<display:table name="list" requestURI="/admin/group/query" id="item" export="false" sort="external" class="${tableclass}" style="width:100%">
	
		<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
		<display:column title="店铺">
			<a href="${contextPath}/admin/shopDetail/load/${item.shopId}">${item.shopName}</a>
		</display:column>
		<display:column title="活动名称" style="width:20%">
			<a href="${PC_DOMAIN_NAME}/group/view/${item.id}" target="_blank">${item.groupName}</a>
		</display:column>
		<display:column title="开始时间" property="startTime" format="{0,date,yyyy-MM-dd HH:mm:ss}" sortable="true" sortName="startTime"></display:column>
		<display:column title="结束时间" property="endTime" format="{0,date,yyyy-MM-dd HH:mm:ss}" sortable="true" sortName="endTime"></display:column>
		<display:column title="活动状态">

			<c:choose>
				<c:when test="${item.endTime lt nowDate}">
					<span class="fight-sta">已结束</span><c:if test="${item.deleteStatus eq 1}"><font style="color:#e2393b;">(商家已删除)</font></c:if>
				</c:when>
				<c:when test="${empty item.prodStatus || item.prodStatus eq -1 || item.prodStatus eq -2}">
					<span class="fight-sta">商品已删除</span><c:if test="${item.deleteStatus eq 1}"><font style="color:#e2393b;">(商家已删除)</font></c:if>
				</c:when>
				<c:when test="${item.prodStatus eq 0}">
					<span class="fight-sta">商品已下线</span><c:if test="${item.deleteStatus eq 1}"><font style="color:#e2393b;">(商家已删除)</font></c:if>
				</c:when>
				<c:when test="${item.prodStatus eq 2}">
					<span class="fight-sta">商品违规下线</span><c:if test="${item.deleteStatus eq 1}"><font style="color:#e2393b;">(商家已删除)</font></c:if>
				</c:when>
				<c:when test="${item.prodStatus eq 3}">
					<span class="fight-sta">商品待审核</span><c:if test="${item.deleteStatus eq 1}"><font style="color:#e2393b;">(商家已删除)</font></c:if>
				</c:when>
				<c:when test="${item.prodStatus eq 4}">
					<span class="fight-sta">商品审核失败</span><c:if test="${item.deleteStatus eq 1}"><font style="color:#e2393b;">(商家已删除)</font></c:if>
				</c:when>
				<c:otherwise>
					<c:if test="${item.status==-2}">
						<span class="fight-sta">审核不通过</span>
					</c:if>
					<c:if test="${item.status==0}">
						<span class="fight-staed">已失效</span><c:if test="${item.deleteStatus eq 1}"><font style="color:#e2393b;">(商家已删除)</font></c:if>
					</c:if>
					<c:if test="${item.status==-1}">
						<span class="fight-sta">待审核</span>
					</c:if>
					<c:if test="${item.status==2}">
						<span class="fight-sta">已结束</span>
					</c:if>
					<c:if test="${item.status==1}">
						<c:choose>
							<c:when test="${item.startTime > nowDate}">
								<span class="fight-sta">未开始</span>
							</c:when>
							<c:otherwise>
								<span class="fight-staing">进行中</span>
							</c:otherwise>
						</c:choose>
					</c:if>
				</c:otherwise>
			</c:choose>
	
		</display:column>
	
		<display:column title="操作" media="html">
			<div class="table-btn-group">
				
				<button class="tab-btn" onclick="window.location='${contextPath}/admin/group/groupView/${item.id}'">
					查看
				</button>
				
				<c:if test="${item.status ne 1 || item.status ne 2}">
				<span class="btn-line">|</span>
				<button class="tab-btn" onclick="window.location='${contextPath}/admin/group/operation/${item.id}'">
					运营
				</button>
				</c:if>
				<c:choose>
					<%-- 上线状态  --%>
					<c:when test="${item.status eq 1}">
						<%--未开始 下线  --%>
						<c:if test="${item.startTime gt nowDate }">
							<span class="btn-line">|</span>
							<button class="tab-btn" id="statusImg" onclick="offlineGroup('${item.id}')">
								终止
							</button>
						</c:if>
						<c:if test="${nowDate > item.startTime && nowDate < item.endTime}">
							<span class="btn-line">|</span>
							<button class="tab-btn" id="statusImg" onclick="offlineGroup('${item.id}')">
								终止
							</button>
						</c:if>
					</c:when>
				</c:choose>
			</div>
		</display:column>
	</display:table>

	<div class="clearfix">
   		<div class="fr">
   			 <div cla ss="page">
    			 <div class="p-wrap">
        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			 	</div>
  			 </div>
  		</div>
   	</div>
