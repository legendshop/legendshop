<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%
    Integer offset = 1;
%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <style type="text/css">
  .order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
	}
  </style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
		<table class="${tableclass} title-border" style="width: 100%">
		    <tr>
		        <th class="title-border">系统配置&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">清空缓存</span></th>
		    </tr>
		</table>
        <div class="criteria-div">
	        <span class="item">
	            <input type="button" value="一键清除缓存" class="${btnclass}" onclick="javascript:clearCache()"/>
	            <span style="font-size: 12px;">点击可清空系统缓存，使数据重新从数据库中加载，可能会影响当前正登录用户，使其退出需要重新登录，但不影响系统正常运行。</span>
	        </span>
        </div>
		<div align="center" class="order-content-list">
		    <%@ include file="/WEB-INF/pages/common/messages.jsp" %>
		    <display:table name="cacheList" requestURI="/admin/system/cache/query" id="item"
		                   export="false" class="${tableclass}" style="width:100%">
		        <display:column title="顺序" class="orderwidth">&nbsp;<%=offset++%>&nbsp;&nbsp;&nbsp;</display:column>
		        <display:column title="缓存名称" property="cacheName"></display:column>
		        <display:column title="关键字" property="key"></display:column>
		        <display:column title="内容"><div class="order_img_name">${item.value}</div></display:column>
		    </display:table>
		</div>
	</div>
 </div>
</body>
<script type="text/javascript">
    function clearCache() {
        $.post("${contextPath}/admin/system/cache/removeall",
                function (retData) {
                    if (retData == null || retData == "") {
                        layer.msg("删除缓存成功！",{icon: 1,time: 2000},function(){
	                        window.location.reload();
                        });
                    } else {
                        layer.msg("删除缓存失败！",{icon: 2});
                    }
                });
    }
    highlightTableRows("item");
</script>
</html>

