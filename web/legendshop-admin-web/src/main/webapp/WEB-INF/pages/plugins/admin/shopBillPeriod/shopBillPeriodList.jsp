<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>结算档期 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">结算管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">结算档期</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/shopBillPeriod/query"
				id="form1" method="get">
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
					<span class="item">
						档期：<input type="test" class="${inputclass}" name="flag" value="${shopBillPeriod.flag}" placeholder="档期"/> 
						<input type="button" onclick="search()" value="搜索" class="${btnclass}"/>
					</span>
				</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/shopBillPeriod/query"
					id="item" export="false" sort="external" class="${tableclass}"
					style="width:100%">

					<display:column title="档期" property="flag"></display:column>
					<display:column title="应结总额">
						<fmt:formatNumber pattern="#.##" value="${item.resultTotalAmount}" />
					</display:column>
					<display:column title="订单总额">
						<fmt:formatNumber pattern="#.##" value="${item.orderAmount}" />
					</display:column>
					<display:column title="平台佣金总额">
						<fmt:formatNumber pattern="#.##" value="${item.commisTotals}" />
					</display:column>
					<display:column title="分销佣金总额" property="distCommisTotals"></display:column>
					<display:column title="退单总额" property="orderReturnTotals"></display:column>
					<display:column title="红包总额" property="redpackTotals"></display:column>
					<display:column title="创建时间" property="recDate"></display:column>

					<display:column title="操作" media="html">
						<div class="table-btn-group">
							<button class="tab-btn" onclick="window.location.href='${contextPath}/admin/shopBill/query?flag=${item.flag}'">
								查看
							</button>
						</div>
					</display:column>
				</display:table>
				<div class="clearfix">
			   		<div class="fr">
			   			 <div class="page">
			    			 <div class="p-wrap">
			        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			 			 	</div>
			   			 </div>
			   		</div>
			 	 </div>
				<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
					<tr>
						<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br>
						 	1. 当前商家结算日为:
						 	<c:choose>
							 	<c:when test="${shopBillPeriodType eq 'DAY'}">每天</c:when>
							 	<c:when test="${shopBillPeriodType eq 'WEEK'}">每周 (周一 )</c:when>
							 	<c:when test="${shopBillPeriodType eq 'MONTH'}">每月 (${shopBillDay}号)</c:when>
							</c:choose>
						 	,每次结算会形成一个结算档期<br> 
							2. 账单计算公式：<br>
							&nbsp;&nbsp;&nbsp;2.1、 订单金额(含运费) - 退单金额 - 平台佣金 - 分销佣金 + 红包总额 - 退单红包金额 + 预售定金 + 拍卖保证金<br>
							&nbsp;&nbsp;&nbsp;2.2、 结算价模式:（自营店铺，且结算模式为结算价模式 ） 不计算平台佣金，平台向店铺应付金额为：(按订单的商品数量-退单数量) * 商品结算价计算帐单<br>
							3. 账单处理流程为：系统出账 > 商家确认 > 平台审核 > 财务支付(完成结算)
							4个环节，其中平台审核和财务支付需要平台介入，请予以关注<br>
						</td>
					<tr>
				</table>
			</div>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}

	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#form1")[0].submit();
	}
</script>
</html>
