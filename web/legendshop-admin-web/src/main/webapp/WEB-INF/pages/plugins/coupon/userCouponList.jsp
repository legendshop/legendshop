<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<link rel="stylesheet" href="<ls:templateResource item='/resources/common/css/specProperty.css'/>">
<style type="text/css">
.editAdd_load {
	position: absolute;
	top: 50%;
	left: 50%;
	margin: -16px 0 0 -16px;
	z-index: 99;
	display: none;
}
</style>
<table class="${tableclass}" style="width: 100%">
	<thead>
		<tr>
			<th><strong class="am-text-primary am-text-lg">优惠券管理</strong> / 赠送礼券</th>
		</tr>
	</thead>
</table>
<div class="demo">

	<div class="ui-tabs ui-widget ui-widget-content ui-corner-all" id="tabs">
		<ul class="am-tabs-nav am-nav am-nav-tabs">
			<li id="ruleAttr1" filter="hyfilter" class="am-active"><a href="javascript:void(0);" onclick="toPage(this,1);">活跃度筛选</a>
			</li>
			<li id="ruleAttr2" filter="regfilter"><a href="javascript:void(0);" onclick="toPage(this,2);">注册时间筛选</a>
			</li>
			<li id="ruleAttr3" filter="commonfilter"><a href="javascript:void(0);" onclick="toPage(this,3);">普通搜索</a>
			</li>
		</ul>

		<div style="margin-top: 10px;">
			<input type="hidden" id="filMethod" name="filMethod" value="1"> <input type="hidden" id="couponId" name="couponId" value="${couponId}">
			<div style="padding: 3px;display: block;" id="hyfilter">
				&nbsp;登录次数 <select id="logNumberFilter" name="logNumberFilter" class="${selectclass}">
					<option value="">---请选择---</option>
					<option value="1">大于</option>
					<option value="2">小于</option>
					<option value="3">等于</option>
				</select> 
				<input type="text" name="logNumber" id="logNumber" maxlength="50" class="${inputclass}"/> 
				&nbsp;订单总金额 <select id="totalAmountFilter" name="totalAmountFilter" class="${selectclass}">
					<option value="">---请选择---</option>
					<option value="1">大于</option>
					<option value="2">小于</option>
					<option value="3">等于</option>
				</select> 
				<input type="text" name="totalAmount" id="totalAmount" maxlength="50" class="${inputclass}"/> 
				&nbsp;订单数量 <select id="orderNumberFilter" name="orderNumberFilter" class="${selectclass}">
					<option value="">---请选择---</option>
					<option value="1">大于</option>
					<option value="2">小于</option>
					<option value="3">等于</option>
				</select> 
				<input type="text" name="orderNumber" id="orderNumber" maxlength="50" class="${inputclass}"/> 
				<input type="button" value="搜索" class="${btnclass}" onclick="javascript:searchContent();" /> 
				<input type="button" value="返回" class="${btnclass}" onclick="window.location='<ls:url address="/admin/coupon/query"/>'" /> <br>
			</div>

			<div style="padding: 3px;display:none;" id="regfilter">
				&nbsp;注册时间 <input class="${inputclass}" readonly="readonly" name="regtime_start" id="regtime_start"  type="text" />
					至 <input class="${inputclass}" readonly="readonly" name="regtime_end" id="regtime_end" class="Wdate" type="text" /> 
					<input type="button" value="搜索" class="${btnclass}" onclick="javascript:searchContent();" /> 
					<input type="button" value="返回" class="${btnclass}" onclick="window.location='<ls:url address="/admin/coupon/query"/>'" /> <br>
			</div>

			<div style="padding: 3px;display:none;" id="commonfilter">
				&nbsp;查询属性 <select id="attribute" name="attribute" class="${selectclass}">
					<option value="">---请选择---</option>
					<option value="0">用户名</option>
					<option value="1">昵称</option>
					<option value="2">邮件</option>
					<option value="3">手机</option>

				</select> &nbsp;查询条件 <input class="${inputclass}" type="text" name="name" id="name" maxlength="50" /> 
				<input type="button" value="搜索" class="${btnclass}" onclick="javascript:searchContent();" /> 
				<input type="button" value="返回" class="${btnclass}" onclick="window.location='<ls:url address="/admin/coupon/query"/>'" /> <br>
			</div>
		</div>
		<div style="margin-top: 10px;" id="couponContent"></div>
	</div>
</div>

<img src="<ls:url address='/resources/common/images/indicator.gif'/>" class="editAdd_load" />
<script language="JavaScript" type="text/javascript">
	$(document).ready(function() {searchContent();});
	
 	function toPage(obj,isRuleAttributes){
 		$("#tabs ul li").attr("class","");
 		$(obj).parent().attr("class","am-active");
 		var filter=$(obj).parent().attr("filter");
 		if(filter=="hyfilter"){
 			$("#hyfilter").css('display','block');
 			$("#regfilter").css('display','none');
 			$("#commonfilter").css('display','none');
 			$("#filMethod").val("1");
 			//清理
 			 $("#name").val("");
  			$("#attribute").val("");
  		    $("#regtime_start").val("");
		    $("#regtime_end").val("");
		  
 		}else if(filter=="regfilter"){
			$("#regfilter").css('display','block');
 			$("#hyfilter").css('display','none');
 			$("#commonfilter").css('display','none');
 			$("#filMethod").val("2");
 			//
 			  $("#logNumberFilter").val("");
 			  $("#logNumber").val("");
 			  $("#totalAmountFilter").val("");
 			  $("#totalAmount").val("");
 			  $("#orderNumberFilter").val("");
 			  $("#orderNumber").val("");
 			  
 			 $("#name").val("");
 			$("#attribute").val("");
 		}else if(filter=="commonfilter"){
 			$("#commonfilter").css('display','block');
 			$("#hyfilter").css('display','none');
 			$("#regfilter").css('display','none');
 			$("#filMethod").val("3");
 			
 			 $("#logNumberFilter").val("");
			  $("#logNumber").val("");
			  $("#totalAmountFilter").val("");
			  $("#totalAmount").val("");
			  $("#orderNumberFilter").val("");
			  $("#orderNumber").val("");
			  
			  $("#regtime_start").val("");
			  $("#regtime_end").val("");
			
 		}
 	}
 	
 	jQuery(document).ready(function(){
 		highlightTableRows("item");  
 		
 		 laydate.render({
	   		 elem: '#regtime_start',
	   		 calendar: true,
	   		 theme: 'grid',
		 	 trigger: 'click'
	   	  });
	   	   
	   	  laydate.render({
	   	     elem: '#regtime_end',
	   	     calendar: true,
	   	     theme: 'grid',
		 	 trigger: 'click'
	      });
 	});
 	
 	
 	
 	function searchContent(){
 		//重新搜索
 		var curPageNO=$("#curPageNO").val("");
 		loaddata();
	}
 	
 	function loaddata(){
 		   var url="${contextPath}/admin/userCoupon/sendquery";
		   var attribute=$("#attribute").val()==undefined?"":$("#attribute").val();
		   var curPageNO=$("#curPageNO").val();
		   if(curPageNO==undefined||curPageNO==null||curPageNO==""){
			   curPageNO=1;
		   }
		   var name=$("#name").val();
		   if(name==undefined||name==null){
			   name="";
		   }else{
			   name=$.trim(name);
		   }
		   
		   var filMethod=$("#filMethod").val();
		   var couponId=$("#couponId").val();
		   if(couponId==null){
			   alert("couponId is null ");
			   return false;
		   }
		   var logNumberFilter=$("#logNumberFilter").val();
		   var logNumber=$.trim($("#logNumber").val());
		   if(logNumber!=null&&logNumber!=undefined&&logNumber!=""){
			   var re = /^[0-9\s]*$/;
			   if(logNumberFilter==""){
				   alert("请选择登录次数筛选条件!");
				   return false;
			   }else if (!re.test(logNumber)){
				   alert("请输入正确的登录次数格式!");
				   $("#logNumber").focus();
				   return false;
			   }
		   }else{
			   logNumber=0; 
		   }
		   
		   var totalAmountFilter=$("#totalAmountFilter").val();
		   var totalAmount=$.trim($("#totalAmount").val());
		   
		   if(totalAmount!=null&&totalAmount!=undefined&&totalAmount!=""){
			   if(totalAmountFilter==""){
				   alert("请选择订单金额筛选条件!");
				   return false;
			   }else if(!(/^\d+(\.\d{1,2})?$/.test(totalAmount))){
				   alert("请输入正确的订单金额格式!");
				   $("#totalAmount").focus();
				   return false;
			   }
		   }else{
			   totalAmount=0; 
		   }

		   var orderNumberFilter=$("#orderNumberFilter").val();
		   var orderNumber=$.trim($("#orderNumber").val());
		   
		   if(orderNumber!=null&&orderNumber!=undefined&&orderNumber!=""){
			   var re = /^[0-9\s]*$/;
			   if(orderNumberFilter==""){
				   alert("请选择订单数量筛选条件!");
				   return false;
			   }
			   else if (!re.test(orderNumber)){
				   alert("请输入正确的订单数量格式!");
				   $("#orderNumber").focus();
				   return false;
			   }
		   }else{
			   orderNumber=0; 
		   }
		   
		   var data = {
				"couponId":couponId,
				"filMethod":filMethod,
				"logNumberFilter":logNumberFilter,
				"logNumber":logNumber,
				"totalAmountFilter":totalAmountFilter,
				"totalAmount":totalAmount,
				"orderNumberFilter":orderNumberFilter,
				"orderNumber":orderNumber,
            	"curPageNO": curPageNO,
            	"name": name,
            	"attribute":attribute,
            	"regtime_start":$("#regtime_start").val(),
            	"regtime_end":$("#regtime_end").val()
            };
		   $(".editAdd_load").show();
			$.ajax({
				url:url, 
				data: data,
				type:"post", 
				async : false, 
				error: function(jqXHR, textStatus, errorThrown) {
					  $(".editAdd_load").hide();
			 		alert(textStatus, errorThrown);
				},
				success:function(result){
					 $(".editAdd_load").hide();
				   $("#couponContent").html(result);
				}
			});
 	}
 	

 	
 	function batckSendUser(){
 		   var url="${contextPath}/admin/userCoupon/batchSendUserCnps";
		   var attribute=$("#attribute").val()==undefined?"":$("#attribute").val();
		   var name=$("#name").val();
		   if(name==undefined||name==null){
			   name="";
		   }else{
			   name=$.trim(name);
		   }
		   
		   var filMethod=$("#filMethod").val();
		   var couponId=$("#couponId").val();
		   if(couponId==null){
			   alert("couponId is null ");
			   return false;
		   }
		   var logNumberFilter=$("#logNumberFilter").val();
		   var logNumber=$.trim($("#logNumber").val());
		   if(logNumber!=null&&logNumber!=undefined&&logNumber!=""){
			   var re = /^[0-9\s]*$/;
			   if(logNumberFilter==""){
				   alert("请选择登录次数筛选条件!");
				   return false;
			   }else if (!re.test(logNumber)){
				   alert("请输入正确的登录次数格式!");
				   $("#logNumber").focus();
				   return false;
			   }
		   }else{
			   logNumber=0; 
		   }
		   
		   var totalAmountFilter=$("#totalAmountFilter").val();
		   var totalAmount=$.trim($("#totalAmount").val());
		   
		   if(totalAmount!=null&&totalAmount!=undefined&&totalAmount!=""){
			   if(totalAmountFilter==""){
				   alert("请选择订单金额筛选条件!");
				   return false;
			   }else if(!(/^\d+(\.\d{1,2})?$/.test(totalAmount))){
				   alert("请输入正确的订单金额格式!");
				   $("#totalAmount").focus();
				   return false;
			   }
		   }else{
			   totalAmount=0; 
		   }

		   var orderNumberFilter=$("#orderNumberFilter").val();
		   var orderNumber=$.trim($("#orderNumber").val());
		   
		   if(orderNumber!=null&&orderNumber!=undefined&&orderNumber!=""){
			   var re = /^[0-9\s]*$/;
			   if(orderNumberFilter==""){
				   alert("请选择订单数量筛选条件!");
				   return false;
			   }
			   else if (!re.test(orderNumber)){
				   alert("请输入正确的订单数量格式!");
				   $("#orderNumber").focus();
				   return false;
			   }
		   }else{
			   orderNumber=0; 
		   }
		   
		   var data = {
				 "couponId":couponId,
				 "filMethod":filMethod,
				 "logNumberFilter":logNumberFilter,
				 "logNumber":logNumber,
				 "totalAmountFilter":totalAmountFilter,
				 "totalAmount":totalAmount,
				 "orderNumberFilter":orderNumberFilter,
				 "orderNumber":orderNumber,
         	     "name": name,
         	     "attribute":attribute,
         	     "regtime_start":$("#regtime_start").val(),
         	     "regtime_end":$("#regtime_end").val()
          };
		   
		   layer.confirm("确定要给筛选的用户赠送礼券吗？(批量赠送时间较慢,请赖心等候哦)", {
				 icon: 3
			     ,btn: ['确定','关闭'] //按钮
			   }, function(){
				   $(".editAdd_load").show();
					 $.ajax({
							url:url, 
							data: data,
							type:"post", 
							async : true, //默认为true 异步   
							error: function(jqXHR, textStatus, errorThrown) {
						 		alert(textStatus, errorThrown);
							},
							success:function(result){
								var re=eval(result);
								if('OK' == re){
						   			art.dialog.tips('赠送成功');
						   			//alert("赠送成功");
						   			//window.location.reload();
						   			  $(".editAdd_load").hide();
						   			loaddata();
						   		}else{
						   			//alert(result)
						   			 $(".editAdd_load").hide();
						   			art.dialog.tips(re);
						   		}
						    }
						 }); 
			   });
 	}
 	
 	
 	
 	
    
  function selAll(){
    chk = document.getElementById("checkbox");
	allSel = document.getElementsByName("strArray");
	
	if(!chk.checked)
		for(i=0;i<allSel.length;i++)
		{
			   allSel[i].checked=false;
		}
	else
		for(i=0;i<allSel.length;i++)
		{
			   allSel[i].checked=true;
		}
	}
	
 	function checkSelect(selAry){
		for(i=0;i<selAry.length;i++)
		{
			if(selAry[i].checked)
			{
				return true;
			}
		}
		return false;
	 }
 	
 	function pager(curPageNO){
 		$("#curPageNO").val(curPageNO);
 		loaddata();
    }
 	
 	function sendUser(){
    	var selArys = document.getElementsByName("strArray");
    	if(!checkSelect(selArys)){
    	  window.alert('至少选中一条记录！');
    	  return false;
    	}
    	 var couponId=$("#couponId").val();
   		 if(couponId==null){
			   alert("couponId is null ");
			   return false;
		 }
   		 
   		layer.confirm("确定要给这些用户赠送礼券吗？", {
   		 icon: 3
   	     ,btn: ['确定','关闭'] //按钮
   	   }, function(){
	   		var users=new Array();
	 		  for(i=0;i<selArys.length;i++){
				  if(selArys[i].checked){
					  var userId = selArys[i].value;
					  users.push(userId);
					  //var name = selAry[i].getAttribute("arg");
				  }
			 }
	 		 $(".editAdd_load").show();
	 		send(couponId,users);
   	   });
    }
 	
 	function send(couponId,users){
 		$.ajax({
			url:"${contextPath}/admin/userCoupon/sendUserCnps", 
			data: {"couponId":couponId,"userids":JSON.stringify(users)},
			type:"post", 
			async : true, //默认为true 异步   
			error: function(jqXHR, textStatus, errorThrown) {
				 $(".editAdd_load").hide();
				 layer.alert("网络异常,请稍后重试！",{icon:2});
			},
			success:function(result){
				var re=eval(result);
				if('OK' == re){
					 $(".editAdd_load").hide();
		   			layer.msg('赠送成功',{icon:1});
		   			searchContent();
		   		}else{
		   		   $(".editAdd_load").hide();
		   		   layer.msg(re,{icon:2});
		   		}
		    }
		 });
 	}

 	 function highlightTableRows(tableId) {
         var previousClass = null;  
         var table = document.getElementById(tableId);
         if(table == null) return;
         var tbody;
         if(table.getElementsByTagName("tbody") != null){
          tbody = table.getElementsByTagName("tbody")[0]; 
         }
         if (tbody == null) {  
             var rows = table.getElementsByTagName("tr");  
         } else {  
             var rows = tbody.getElementsByTagName("tr");  
         }  
         for (i=0; i < rows.length; i++) {
             rows[i].onmouseover = function() { previousClass=this.className;this.className='pointercolor' };  
             rows[i].onmouseout = function() { this.className=previousClass };  
         }  
     } 
	</script>