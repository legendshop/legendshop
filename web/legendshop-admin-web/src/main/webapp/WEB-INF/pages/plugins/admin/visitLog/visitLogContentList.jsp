<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	<display:table name="list" requestURI="/admin/visitLog/query"
		id="item" export="false" class="${tableclass}" style="width:100%"
		sort="external">
		<%-- <display:column title="顺序" style="min-width:50px;" class="orderwidth"><%=offset++%></display:column> --%>
		<display:column title="用户ID" property="userName" sortable="true"
			sortName="userName"></display:column>
		<display:column title="IP" property="ip" sortable="true"
			sortName="ip" style="min-width: 90px;"></display:column>
		<display:column title="国家" style="min-width:150px;" property="country" sortable="true"
			sortName="country"></display:column>
		<display:column title="地区" property="area" sortable="true"
			sortName="area" style="width:12%"></display:column>
		<display:column title="店铺"  style="min-width:100px;" property="shopName" sortable="true"
			sortName="shopName"></display:column>
		<display:column title="商品名" style="width:30%">
			<c:if test="${item.productName != null}">
				<a href="${PC_DOMAIN_NAME}/views/${item.productId}" target="_blank"><div class="order_img_name">${item.productName}</div></a>
			</c:if>
		</display:column>
		<display:column title="页面" style="min-width:50px;">
			<c:if test="${item.page == 0}">首页</c:if>
			<c:if test="${item.page == 1}">商品</c:if>
		</display:column>
		<display:column title="日期" style="min-width:130px;">
			<fmt:formatDate value="${item.date}" pattern="yyyy-MM-dd HH:mm" />
		</display:column>
		<display:column title="访问次数" style="min-width:70px;" property="visitNum">
		</display:column>
	</display:table>
	<div class="clearfix" style="margin-bottom: 60px;">
   		<div class="fr">
   			 <div class="page">
    			 <div class="p-wrap">
        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
 				 </div>
   			 </div>
   		</div>
   	</div>

