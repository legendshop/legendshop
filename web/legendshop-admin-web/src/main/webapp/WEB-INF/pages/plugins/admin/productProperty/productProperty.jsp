<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/jquery.jsp" %>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>属性规格- 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css"/>
  <style>
  	#tab th{
  		border:none;
  		border-bottom:1px solid #efefef;
  		background:#f9f9f9;
  	}
  	#tab th:first-child{
  		border-left:1px solid #efefef;
  	}
  	#tab th:last-child{
  		border-right:1px solid #efefef;
  	}
  </style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">

<form:form action="${contextPath}/admin/productProperty/save" method="post" id="form1" onsubmit="return checkForm();" enctype="multipart/form-data">
    <c:if test="${empty isSimilar}">
        <input id="propId" name="propId" value="${productProperty.propId}" type="hidden">
    </c:if>
    <input id="isRuleAttributes" name="isRuleAttributes" value="${isRuleAttributes}" type="hidden">
    <input id="status" name="status" value="1" type="hidden">
    <div align="center">
        <table style="width:100%" class="${tableclass} no-border">
            <thead>
            <tr>
                <th class="no-bg title-th">
                    <span class="title-span">属性管理  ＞
	                    <c:choose>
	                        <c:when test="${isRuleAttributes ==1}">
	                        	<a href="<ls:url address="/admin/productProperty/query/1"/>" style="color:#0e90d2;">规格属性</a> 
	                        	  ＞ <span style="color:#0e90d2;">创建规格属性</span>
	                        </c:when>
	                        <c:when test="${isRuleAttributes ==2 }">
	                        	<a style="color:#0e90d2;" href="<ls:url address="/admin/productProperty/query/2"/>">参数属性</a>
	                        	  ＞ <span style="color:#0e90d2;">创建参数属性</span>
	                        </c:when>
	                        <c:otherwise>创建关键属性</c:otherwise>
	                    </c:choose>
	                </span>
                </th>
            </tr>
            </thead>
        </table>
       	<table border="0" style="width:100%" align="left" class="${tableclass} no-border " id="col1">
            <tr>
                <td width="200px">
                    <div align="right"><font color="ff0000">*</font>名称：</div>
                </td>
                <td align="left" style="padding-left: 2px;">
                    <input style="margin-top:22px;width: 300px;" type="text" name="propName" id="propName" value="${productProperty.propName}" maxlength="100"/> <br/>
                    	<span style="color: #999;">用于关联类型并绑定到类目，建议名称为 "将要绑定的商品类目全路径名称 + 别名"</span>
                </td>
            </tr>
            <tr>
                <td>
                    <div align="right" style="height:40px;"><font color="ff0000">*</font>别名：</div>
                </td>
                <td align="left" style="padding-bottom: 0;padding-left: 2px;">
                    <input style="width: 300px;" type="text" name="memo" id="memo" value="${productProperty.memo}" maxlength="20"/><br/>
                    	<span style="color: #999;">前台显示的简要名字，最多20字</span>
                </td>
            </tr>
            <c:if test="${ isRuleAttributes == 1}">
                <tr>
                    <td>
                        <div align="right"><font color="ff0000">*</font>属性类型：</div>
                    </td>
                    <td align="left" style="padding-left: 2px;">
                        <select id="type" name="type" onchange="change(this.value)">
                            <ls:optionGroup type="select" required="true" cache="true" beanName="PROPERTY_TYPE" selectedValue="${productProperty.type}"/>
                        </select>
                    </td>
                </tr>
            </c:if>

            <%-- <c:if test="${ isRuleAttributes == 2}"> --%>
            <c:if test="${ isRuleAttributes != 1}">
                <tr>
                    <td>
                        <div align="right">是否：</div>
                    </td>
                    <td align="left" style="padding-left: 2px;">
                       <%--  <input type='checkbox' id="isRequired"
                               <c:if test="${productProperty.isRequired}">checked="checked"</c:if> name="isRequired"/> --%>
                        <!-- <span title="勾选该项目后，在发布商品时必须填写">必须</span> -->
                            <%-- sku不支持多选
                            <input type='checkbox'  id="isMulti"  <c:if test="${productProperty.isMulti}">checked="checked"</c:if>  name="isMulti" />多选
                             --%>
                        <c:if test="${ isRuleAttributes == 2}">
                        	<label class="checkbox-wrapper <c:if test="${productProperty.isForSearch}">checkbox-wrapper-checked </c:if>">
								<span class="checkbox-item">
									<input type="checkbox" class="checkbox-input selectOne" id="isForSearch" name="isForSearch"
									<c:if test="${productProperty.isForSearch}">checked="checked"</c:if> />
									<span class="checkbox-inner"></span>
								</span>
                        	<span title="勾选该项目后，在前台商品列表出现该选项">可以搜索</span>
						   </label>	
                        </c:if>

                        <label class="checkbox-wrapper <c:if test='${productProperty.isInputProp}'>checkbox-wrapper-checked </c:if>">
								<span class="checkbox-item">
									<input type="checkbox" class="checkbox-input selectOne" id="isInputProp" name="isInputProp"
									<c:if test="${productProperty.isInputProp}">checked="checked"</c:if> />
									<span class="checkbox-inner"></span>
								</span>
                        <span title="勾选该项目后，在发布商品时必须以文本方式输入">用户可自定义（不可搜索）</span>
						   </label>

                    </td>
                </tr>
            </c:if>
            <%-- </c:if> --%>

            <tr>
                <td>&nbsp;</td>
                <td valign="bottom" align="left" style="line-height: 30px;padding-left: 2px;"><input class="${btnclass}" type="button" id="addSpecBtn" value="增加属性值"/>（拖动可以改变顺序）</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="padding-left: 3px;">
                    <table id="tab" class="am-table am-table-striped" style="border-collapse: collapse;border:1px solid #efefef;width: 100%;box-sizing: border-box;width: 841px;" align="left">
                        <thead>
                        <tr style="padding: 8px;line-height: 22px;font-weight: 400;color: #333;">
                            <th style="text-align: center;border-left:1px solid #efefef;">名称</th>
                            <c:choose>
                                <c:when test="${isRuleAttributes == 1 and productProperty.type ==1}">
                                    <th id="img" style="text-align: center;">图片</th>
                                </c:when>
                                <c:otherwise>
                                    <th id="img" style="text-align: center;display:none;">图片</th>
                                </c:otherwise>
                            </c:choose>
                            <th style="text-align: center;border-right:1px solid #efefef;" >操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${valueList}" var="PropertyValue" varStatus="status">
                            <tr align="left" name="PropertyValue" id="${status.index}">
                                <td style="text-align: center;">
                                    <c:if test="${empty isSimilar}">
                                        <input type="hidden" name="valueList[${status.index}].valueId" value="${PropertyValue.valueId}">
                                    </c:if>
                                    <input type="text" maxlength="50" style="width: 88%;" size="50" value="${PropertyValue.name}" id="valueList[${status.index}].name" name="valueList[${status.index}].name"></td>
                                <c:choose>
                                    <c:when test="${isRuleAttributes == 1 and productProperty.type ==1}">
                                        <td id="img" style="width:250px;text-align: center;">
                                            <c:choose>
                                                <c:when test="${not empty PropertyValue.pic}">
                                                    <img src="<ls:images item="${PropertyValue.pic}" scale="3"/>" style="width: 22px; height: 22px; border: 1px solid #666666;margin-left: 30px;"/>
                                                    <input type="file" name="valueList[${status.index}].file" style="width: 153px;float: right;margin-right: 20px;" value="上传图片"/>
                                                </c:when>
                                                <c:otherwise><input type="file" name="valueList[${status.index}].file" style="width: 153px;float: right;margin-right: 20px;" value="上传图片"/></c:otherwise>
                                            </c:choose>
                                        </td>
                                    </c:when>
                                    <c:when test="${isRuleAttributes == 1}">
                                        <td id="img" style="width:250px;display:none;">
                                            <input type="file" name="valueList[${status.index}].file" style="width: 250px;" value="上传图片"/>
                                        </td>
                                    </c:when>
                                </c:choose>
                                <td width="123" style="text-align: center;"><input type="hidden" class="ppv_seq" value="${PropertyValue.sequence}" id="valueList[${status.index}].sequence" name="valueList[${status.index}].sequence" maxlength="5" style="width:40px">
                                    <input type="hidden" value="${PropertyValue.status}" id="valueList[${status.index}].status" name="valueList[${status.index}].status">
                                    <div class="am-btn-toolbar" style="text-align: center;float: unset;">
                                        <div style="text-align: center;float: unset;">
                                            <a style='cursor:pointer;'  onclick="deltr(this);" sequence="valueList[${status.index}].name">删除</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div align="center">
                        <input class="${btnclass}" type="submit" value="保存"/>
                        <input class="${btnclass}" type="button" value="返回" onclick="window.location='<ls:url address="/admin/productProperty/query/${isRuleAttributes}"/>'"/>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</form:form>
</div>
</div>
</body>

<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/checkImage.js'/>"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.core.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.widget.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.mouse.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.sortable.js'/>" type="text/javascript"></script>
<script language="javascript">
    $.validator.setDefaults({});

    $(document).ready(function () {

        jQuery("#form1").validate({
            rules: {
                propName: "required",
                memo: "required",
                sequence: {
                    number: true,
                    required: true
                }
            },
            messages: {
                propName: "请输入名称",
                memo: "请输入别名",
                sequence: {
                    required: "请输入排序",
                    number: "请输入合法的数字"
                }
            }
        });

        $("#col1 tr:nth-child(even)").addClass("even");
        $("#col1 th").addClass('sortable');

        //<tr/>居中
        $("#tab tr").attr("align", "left");

        var tableLength = $("#tab tbody tr").length;
        for (var i = 0; i < tableLength - 1; i++) {
            $("input[name='valueList[" + i + "].name']").rules("add", {required: true, messages: {required: "不能为空"}});
            $("input[name='valueList[" + i + "].sequence']").rules("add", {required: true, number: true, messages: {required: "不能为空", number: "请输数字"}});
        }
        
        var v = $("#type").val();
        if(v == 1){
        	$("input[name$='.file']").each(function(index,element){
        		
        		$(this).change(function(){
        			checkImgType(this);
                	checkImgSize(this,5120);
        		});
        	});
        }

        //增加<tr/>
        $("#addSpecBtn").click(function () {
            var _len = $("#tab tr").length;
            if (_len < 100) {
                var index = _len - 1;
                var type = $("#type").val();
                var propertyImg;
                if (type == 1) {
                    propertyImg = "<td id='img' style='text-align: center;'><input type='file' style='width: 153px;float: right;margin-right: 20px;' name='valueList[" + index + "].file' id='valueList[" + index + "].file' /></td>";
                } else {
                    propertyImg = "<td id='img' style='display: none;'><input type='file' style='width: 153px;float: right;margin-right: 20px;' name='valueList[" + index + "].file' id='valueList[" + index + "].file' /></td>";
                }

                var trcontent = "<tr id=" + _len + " align='left' name='PropertyValue'>"
                        + "<td style='text-align: center;'><input type='text' maxlength='50' style='width: 88%;'  size='50'  name='valueList[" + index + "].name' id='valueList[" + index + "].name' /></td>"
                        + propertyImg
                        + "<td width='123'><input class='ppv_seq' type='hidden' style='width:40px' maxlength='5' name='valueList[" + index + "].sequence' id='valueList[" + index + "].sequence' value='" + _len + "'/>"
                        + "<input type='hidden' name='valueList[" + index + "].status' id='valueList[" + index + "].status'  value='1'/>"
                        + "<div class='am-btn-toolbar' style='text-align: center;float: unset;'><div class='am-btn-group am-btn-group-xs' style='text-align: center;float: unset;'>"
                        + "<a style='cursor:pointer;' onclick=\'deltr(this)\'  sequence=\'valueList[" + index +"].name\' >删除</a></div></div></td>"
                        + "</tr>";

                $("#tab").append(trcontent);
                //动态添加验证 
                $("input[name='valueList[" + index + "].name']").rules("add", {required: true, messages: {required: "必填"}});
                $("input[name='valueList[" + index + "].sequence']").rules("add", {required: true, number: true, messages: {required: "必填", number: "请输数字"}});
                $("input[name='valueList[" + index + "].file']").change(function(){
                	checkImgType(this);
                	checkImgSize(this,5120);
                });
            } else {
                layer.msg("最多不能设置超过100个属性", {icon:2});
            }
        });

    });

	//可搜索和可输入只能二选一
    $("input[type=checkbox]").on("click", function(){

        var boxId = $(this).attr("id");

        if(!$(this).prop("checked")){
            $(this).prop("checked",false);
            $(this).parent().parent().removeClass("checkbox-wrapper-checked");

        }else {
            $(".selectOne").prop("checked",false);
            $(".selectOne").parent().parent().removeClass("checkbox-wrapper-checked");
            $(this).prop("checked",true);
            $(this).parent().parent().addClass("checkbox-wrapper-checked");
        }



    });
    
    //删除<tr/>
    var deltr = function (obj) {
        var propId = $("#propId").val();
        if(propId != null && propId != "" && propId != undefined){
            var propValue =	document.getElementsByName(obj.getAttribute("sequence"))[0].value
        	$.ajax({
            	url: "${contextPath}/admin/sku/ifDeleteProductPropertyValue",
                data: {"propId":propId,"propValue":propValue},
            	type: "POST",
            	async: false,
            	dataType: "json",
            	success: function (result) {
                	if (result == false) {
                    	$(obj).parent().parent().parent().parent().remove();//删除当前行
                	} else {
                    	layer.alert("此属性值正在被使用，不能删除！", {icon:2});
                	}
            	}
        	});
        } else {
        	$(obj).parent().parent().parent().parent().remove();//删除当前行
        }
    }

    //状态变更
    function changeStatus(obj) {
        $obj = $(obj);
        var statusValue = $obj.parent().parent().prev().val();
        if (statusValue == 1) {
            $obj.parent().parent().prev().val(0);
            //$obj.attr("src", "<ls:templateResource item='/resources/common/images/yellow_up.png'/>");
            $obj.html("<span class='am-icon-arrow-up'></span>上线");
            $obj.attr("style", "color:#0e90d2;");
        } else {
            $obj.parent().parent().prev().val(1);
            //$obj.attr("src", "<ls:templateResource item='/resources/common/images/blue_down.png'/>");
            $obj.html("<span class='am-icon-arrow-down'></span>下线");
            $obj.attr("style", "color:#f37b1d;");
        }
    }

    function change(v1) {
        if (v1 == 0) {
            $("#tab th[id='img']").hide();
            $("#tab td[id='img']").hide();
        } else if (v1 == 1) {
            $("#tab th[id='img']").show();
            $("#tab td[id='img']").show();
            $("#img input[type='file']").attr("style","width:153px;float:right;margin-right:20px");
            $("input[name$='.file']").each(function(index,element){
        		$(this).change(function(){
        			checkImgType(this);
                	checkImgSize(this,5120);
        		});
        	});
        }
    }

    var fixHelper = function (e, ui) {
        //console.log(ui)   
        ui.children().each(function () {
            $(this).width($(this).width());     //在拖动时，拖动行的cell（单元格）宽度会发生改变。在这里做了处理就没问题了   
        });
        return ui;
    };


    jQuery(function () {
        jQuery("#tab tbody").sortable({                //这里是talbe tbody，绑定 了sortable   
            helper: fixHelper,                  //调用fixHelper   
            axis: "y",
            start: function (e, ui) {
                ui.helper.css({"background": "#0e90d2"});    //拖动时的行，要用ui.helper   
                return ui;
            },
            stop: function (e, ui) {
                ui.item.css({"background": ""}); //释放鼠标时，要用ui.item才是释放的行   
                setNewSeq();//设置新的顺序值
                return ui;
            }
        });
    });

    function setNewSeq() {
        var seqList = $(".ppv_seq").get();
        for (var i = 0; i < seqList.length; i++) {
            $(seqList[i]).val(i + 1);
        }
    }

    function checkForm() {
        $("#propName").val($("#propName").val().trim());
        $("#memo").val($("#memo").val().trim());
        var isInputProp = $("#isInputProp").prop('checked');
        if (!isInputProp) {
            if ($("tr[name=PropertyValue]").length == 0) {
                layer.alert("非输入属性必须添加属性值", {icon:2});
                return false;
            }
        }
        return true;
    }
</script>
</html>