<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss" var="nowDate" />
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	<display:table name="list"
		requestURI="/admin/marketing/zhekou/query" id="item" export="false"
		sort="external" class="${tableclass}">
		<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
		<display:column title="活动名称" property="marketName"></display:column>
		<display:column title="店铺">
			<a
				href="<ls:url address="/admin/shopDetail/load/${item.shopId}"/>"
				target="_blank">${item.siteName}</a>
		</display:column>
		<display:column title="开始时间" sortable="true" sortName="start_time">
			<fmt:formatDate value="${item.startTime}"
				pattern="yyyy-MM-dd HH:mm:ss" />
			<c:set value="${item.startTime}" var="startTime"></c:set>
		</display:column>
		<display:column title="结束时间" sortable="true" sortName="end_time">
			<fmt:formatDate value="${item.endTime}"
				pattern="yyyy-MM-dd HH:mm:ss" />
			<c:set value="${item.endTime}" var="endTime"></c:set>
		</display:column>
		<display:column title="状态">
			<c:choose>
				<c:when test="${item.state eq 0}">
					<c:choose>
						<c:when test="${endTime lt nowDate}">已结束</c:when>
						<c:when test="${nowDate gt startTime && nowDate lt endTime}">未上线</c:when>
						<c:otherwise>
     					正常状态
     				</c:otherwise>
					</c:choose>
				</c:when>
				<c:when test="${item.state eq 1}">
					<c:choose>
						<c:when test="${endTime lt nowDate}">已结束</c:when>
						<c:when test="${nowDate gt startTime && nowDate lt endTime}">正在进行</c:when>
						<c:otherwise>
     					正常状态
     				</c:otherwise>
					</c:choose>
				</c:when>
				<c:when test="${item.state eq 2}">
					已暂停
     			</c:when>
				<c:when test="${item.state eq 3}">
					已失效
				</c:when>
			</c:choose>

		</display:column>
		<display:column title="操作" media="html" style="width:210px;">
				<div class="table-btn-group">
					<c:if test="${item.state eq 0}">
						<c:choose>
							<c:when test="${nowDate gt endTime}">
								<button class="tab-btn" onclick="deleteById(${item.id},${item.shopId})">
									删除
								</button>
							</c:when>
							<c:when test="${nowDate gt startTime && nowDate lt endTime}">
								<button class="tab-btn" onclick="window.location='${contextPath}/admin/marketing/zhekouQueryById/${item.id}/${item.shopId}'">
									查看
								</button>
								<span class="btn-line">|</span>
								<button class="tab-btn" onclick="deleteById(${item.id},${item.shopId})">
									删除
								</button>
							</c:when>
							<c:otherwise>
								<button class="tab-btn" onclick="deleteById(${item.id},${item.shopId})">
									删除
								</button>
							</c:otherwise>
						</c:choose>
					</c:if>

					<c:if test="${item.state eq 1}">
						<c:choose>
							<c:when test="${nowDate gt endTime}">
								<button onclick="javascript:offlineShopMarketing('${item.id}');" class="tab-btn">
									下线
								</button>
							</c:when>
							<c:when test="${nowDate gt startTime && nowDate lt endTime}">
								<button class="tab-btn" onclick="window.location='${contextPath}/admin/marketing/zhekouQueryById/${item.id}/${item.shopId}'">
									查看
								</button>
							</c:when>
							<c:otherwise>
								<button onclick="javascript:offlineShopMarketing('${item.id}');" class="tab-btn" >
									下线
								</button>
							</c:otherwise>
						</c:choose>
					</c:if>
				</div>
		</display:column>
	</display:table>
	<div class="clearfix">
   		<div class="fr">
   			 <div cla ss="page">
    			 <div class="p-wrap">
        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
			 	</div>
  			 </div>
  		</div>
   	</div>
