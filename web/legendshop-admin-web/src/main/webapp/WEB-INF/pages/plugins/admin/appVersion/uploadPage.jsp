<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.legendesign.net/tags" prefix="ls"%>
<%@ taglib uri="http://www.legendesign.net/biz" prefix="lb"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>  
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%
	request.setAttribute("contextPath", request.getContextPath());
%>
<html>
<head>
	<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
	<link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/amazeui.css"/>
	<link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/admin.css">
	<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
	<title>更新APP版本</title>
</head>
<body style="margin: 0;padding:0;height: 180px;">

	<form:form action="${contextPath}/admin/appVersion/uploadApk" method="POST" id="beanForm" enctype="multipart/form-data">
		<input type="hidden" id="type" name="type" value="${type }" />
		<table border="0" align="center" class="${tableclass} no-border" id="col1"  style="width: 100%">
	   		<tbody>
		        <tr>
		         	<td style="width:13%;min-width:185px;">
		         		<div align="right"><span class="required">*</span>APK文件 :</div>
		      		</td>
		         	<td align="left">
		         		<input type="file" id="apkFile" name="apkFile"/>
		         	</td>
		        </tr>
		        <tr>
		         	<td style="width:13%;min-width:185px;">
		         		<div align="right"><span class="required">*</span>版本号:</div>
		      		</td>
		         	<td align="left">
		         		<input type="text" id="version" name="version" class="${inputclass}" />
		         	</td>
		        </tr>
		        <tr>
		        	<td></td>
		            <td>
		             	<div align="left">
		                     <input class="${btnclass}" type="submit" value="确认保存" />
		                    <%--  <input class="${btnclass}" type="button" value="取消" onclick="art.dialog.close();"/> --%>
		                </div>
		             </td>
		         </tr>
	        </tbody>
         </table>
	</form:form>

<script type="text/javascript">
   var contextPath = "${contextPath}";
</script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/commonUtils.js'/>" /></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/uploadPage.js'/>"></script>
</body>
</html>