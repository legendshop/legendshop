<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>品牌编辑 - 后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
    <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
    <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.css'/>" />
</head>
<body>
       <jsp:include page="/admin/top" />
		<div class="am-cf admin-main">
		  <!-- sidebar start -->
		  	 <jsp:include page="../frame/left.jsp"></jsp:include>
		   <!-- sidebar end -->
		     <div class="admin-content" id="admin-content" style="overflow-x:auto;">
        <form:form  action="${contextPath}/admin/brand/save" method="post" id="form1" enctype="multipart/form-data">
        <table class="${tableclass}" style="width: 100%">
	     <thead>
	    	<tr>
		    	<th class="no-bg title-th">
			    	<span class="title-span">商品管理  ＞  <a href="<ls:url address="/admin/brand/query"/>">品牌管理</a>  ＞  
						<c:choose>
							<c:when test="${not empty bean.brandId }"><span style="color:#0e90d2;">品牌编辑</span></c:when>
							<c:otherwise><span style="color:#0e90d2;">创建品牌</span></c:otherwise>
						</c:choose>
					</span>
				</th>
	    	</tr>
	    </thead>
	    </table>
            <input id="brandId" name="brandId" value="${bean.brandId}" type="hidden">
            <div align="center">
            <table  align="center" class="${tableclass} no-border content-table" id="col1">
      <tr>
        <td width="200px">
          <div align="right" > <font color="ff0000">*</font>品牌：</div>
       </td>
        <td align="left" style="padding-left:2px;">
           <input type="text" style="width:150px;" name="brandName" id="brandName" value="${bean.brandName}" maxlength="30"/>
        </td>
      </tr>
      <tr>
	   <td>
          <div align="right" ><span><font color="ff0000">*</font>品牌LOGO(PC)：</span><br><span style="color:#999999;">(建议尺寸：138*46)&nbsp;</span></div>
       </td>
        <td align="left" style="padding-left:2px;">
            <input type="file" name="file" id="file"/>
			<input type="hidden" name="brandPic" id="brandPic" value="${bean.brandPic}" />  
        </td>
      </tr>
      <tr>
	   <td>
          <div align="right" ><span><font color="ff0000">*</font>品牌LOGO(手机)：</span><br><span style="color:#999999;">(建议尺寸：110*110)&nbsp;</span></div>
       </td>
        <td align="left" style="padding-left:2px;">
            <input type="file" name="mobileLogoFile" id="mobileLogoFile"/>
			<input type="hidden" name="brandPicMobile" id="brandPicMobile" value="${bean.brandPicMobile}" />
        </td>
      </tr>
      <tr>
        <td>
          <div align="right" ><span>品牌大图：</span><br><span style="color:#999999;">(建议尺寸：400*200)&nbsp;</span></div>
       </td>
        <td align="left" style="padding-left:2px;">
            <input type="file" name="bigImagefile" id="bigImagefile"/>
			<input type="hidden" name="bigImage" id="bigImage" value="${bean.bigImage}" />  
        </td>
      </tr>
      <tr>
        <td>
          <div align="right" >品牌授权书：</div>
       </td>
        <td align="left" style="padding-left:2px;">
            <input type="file" name="brandAuthorizefile" id="brandAuthorizefile"/>
			<input type="hidden" name="brandAuthorizefile" id="brandAuthorizefile" value="${bean.brandAuthorizefile}" />  
        </td>
      </tr>
        <tr>
        <td>
          <div align="right" >次序：</div>
       </td>
        <td align="left" style="padding-left:2px;">
           <input type="text" name="seq" id="seq" value="${bean.seq}" onkeyup="this.value=this.value.replace(/\D/g,'')"
					onafterpaste="this.value=this.value.replace(/\D/g,'')"  maxlength="5" size="5"/>
        </td>
      </tr>
      <tr>
	      <td>
	      <div align="right" >是否推荐品牌：</div>
	      </td>
	      <td align="left" style="padding-left:2px;">
	      	<select id="commend" name="commend">
	       			<ls:optionGroup type="select" required="true" cache="true"  beanName="YES_NO" selectedValue="${bean.commend}"/>
      		</select>
	      </td>      
      </tr>
      <tr>
        <td>
          <div align="right">备注：</div>
       </td>
        <td align="left" style="padding-left:2px;">
           <input type="text" name="memo" id="memo" value="${bean.memo}" style="width: 300px;" size="31" maxlength="50"/>
        </td>
      </tr>
      <tr>
        <td valign="top" >
          <div align="right" ><font color="ff0000">*</font>品牌介绍：</div>
       </td>
        <td align="left" style="padding-left:2px;">
           <%-- <input type="text" name="brief" id="brief" value="${bean.brief}" size="50" maxlength="50"/> --%>
           <textarea name="brief" id="brief" cols="60" style="width: 300px;height: 100px;">${bean.brief}</textarea>
        </td>
      </tr>
      	<c:if test="${not empty bean.brandPic}">
			<tr>
				<td align="right" >
					<div align="right">
						PC品牌LOGO：
					</div>
				</td>
				<td align="left" style="padding-left:2px;">
						<a href="<ls:photo item='${bean.brandPic}'/>" target="_blank"> <img src="<ls:photo item='${bean.brandPic}'/>" height="46"/></a>
				</td>
			</tr>
		</c:if>
      	<c:if test="${not empty bean.brandPicMobile}">
			<tr>
				<td align="right" >
					<div align="right">
						移动端品牌LOGO：
					</div>
				</td>
				<td align="left" style="padding-left:2px;">
						<a href="<ls:photo item='${bean.brandPicMobile}'/>" target="_blank"> <img src="<ls:photo item='${bean.brandPicMobile}'/>" height="110"/></a>
				</td>
			</tr>
		</c:if>

		<c:if test="${not empty bean.bigImage}">
			<tr>
				<td align="right" >
					<div align="center">
						品牌大图：
					</div>
				</td>
				<td align="left" style="padding-left:2px;">
						<a href="<ls:photo item='${bean.bigImage}'/>" target="_blank"> <img src="<ls:photo item='${bean.bigImage}'/>" height="130"/></a>
				</td>
			</tr>
		</c:if>
		<c:if test="${not empty bean.brandAuthorize}">
			<tr>
				<td align="right" >
					<div align="center">
						品牌授权书：
					</div>
				</td>
				<td align="left" style="padding-left:2px;">
						<a href="<ls:photo item='${bean.brandAuthorize}'/>" target="_blank"> <img src="<ls:photo item='${bean.brandAuthorize}'/>" height="130"/></a>
				</td>
			</tr>
		</c:if>
		<%-- <tr>
            <td colspan="2">
					<textarea name="content" id="content" cols="100" rows="8" style="width:700px;height:200px;visibility:hidden;">${bean.content}</textarea>
            </td>
        </tr> --%>
                <tr>
                	<td></td>
                    <td>
                        <div align="left">
					        <input class="${btnclass}" type="submit" value="保存"/>
                            
                            <%-- <input class="${btnclass}" type="button" value="返回"
                                onclick="window.location='${contextPath}/admin/brand/query?status=${bean.status}'" /> --%>
                                <input class="${btnclass}" type="button" value="返回"
                                onclick="javascript:history.go(-1)" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
     </div>
     </div>
     </body>
 		<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
		<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
		<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
		<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
        <script type="text/javascript" src='<ls:templateResource item="/resources/common/js/checkImage.js"/>'></script>
        <script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/brand.js'/>"></script>
		<script language="javascript">
			var contextPath = "${contextPath}";
			var UUID = "${cookie.SESSION.value}";
			$.validator.setDefaults({});
		</script>
</html>
