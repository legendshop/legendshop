<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
	<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	<display:table name="list" requestURI="/admin/productcomment/query"
		id="item" export="false" class="${tableclass}"
		style="width:100%; <%-- table-layout: fixed; --%>" sort="external">
		<%-- <display:column title="顺序" class="orderwidth"><%=offset++%></display:column> --%>
		<display:column title="店铺" property="siteName"
			style="text-overflow: ellipsis;white-space: nowrap;overflow: hidden;"></display:column>
		<display:column title="用户名" property="userName" style="width:100px"
			></display:column>
		<display:column title="是否匿名" style="width:50px">
			<c:if test="${item.isAnonymous eq 1}">是</c:if>
			<c:if test="${item.isAnonymous eq 0}">否</c:if>
		</display:column>
		<display:column title="订单号" style="min-width:200px;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;">
   					<a target="_blank" href="${contextPath}/admin/order/orderAdminDetail/${item.subNumber}">${item.subNumber}</a>
 				  </display:column>
		<display:column title="商品" sortName="name"
			style="max-width:150px; text-overflow: ellipsis;white-space: nowrap;overflow: hidden;">
			<a target="_blank" href="${PC_DOMAIN_NAME}/views/${item.prodId}">${item.prodName}</a>
		</display:column>
		<%-- <display:column title="评论内容"
			style="max-width:150px; text-overflow: ellipsis;white-space: nowrap;overflow: hidden;"
			property="content"></display:column> --%>
		<display:column title="评分" property="score" 
			sortable="true" sortName="score"></display:column>
		<display:column title="评论时间" 
			sortable="true" sortName="addtime" style="min-width: 90px;">
			<fmt:formatDate value="${item.addtime}" pattern="yyyy-MM-dd HH:mm" />
		</display:column>
		<c:choose>
			<c:when test="${commentNeedReview == true }">
				<display:column title="状态" sortable="ture" sortName="status"
					class="review" style="min-width:60px">
					<c:choose>
						<c:when test="${item.status == 1}">
							<span>通过</span>
						</c:when>
						<c:when test="${item.status == 0}">
							<span>待审核</span>
						</c:when>
						<c:when test="${item.status == -1 }">
							<span>不通过</span>
						</c:when>
					</c:choose>
				</display:column>
			</c:when>
		</c:choose>
		<%-- <display:column title="追加评论"
			style="max-width:150px; text-overflow: ellipsis;white-space: nowrap;overflow: hidden;">
			<c:choose>
				<c:when test="${not empty item.addContent}">${item.addContent}</c:when>
				<c:otherwise>------</c:otherwise>
			</c:choose>
		</display:column> --%>
		<display:column title="追加时间" style="min-width: 90px;">
			<c:choose>
				<c:when test="${not empty item.addAddTime}">
					<fmt:formatDate value="${item.addAddTime}"
						pattern="yyyy-MM-dd HH:mm" />
				</c:when>
				<c:otherwise>------</c:otherwise>
			</c:choose>
		</display:column>
		<c:choose>
			<c:when test="${commentNeedReview == true }">
				<display:column title="追评状态" class="review" style="min-width:60px">
					<c:choose>
						<c:when test="${item.addStatus == 1}">
							<span>通过</span>
						</c:when>
						<c:when test="${item.addStatus == 0}">
							<span>待审核</span>
						</c:when>
						<c:when test="${item.addStatus == -1 }">
							<span>不通过</span>
						</c:when>
						<c:otherwise>------</c:otherwise>
					</c:choose>
				</display:column>
			</c:when>
		</c:choose>
		<display:column title="操作" media="html" style="min-width:140px">
				<div class="table-btn-group">
					<c:choose>
						<c:when test="${item.status == 0}">
							<button onclick="window.location.href='${contextPath }/admin/productcomment/detail/${item.id}'" class="tab-btn">
								审核
							</button>
							<span class="btn-line">|</span>
						</c:when>
						<c:when
							test="${item.status eq 1 and item.isAddComm and item.addStatus eq 0}">
							<button onclick="window.location.href='${contextPath }/admin/productcomment/detail/${item.id}'" class="tab-btn">
								审核
							</button>
							<span class="btn-line">|</span>
						</c:when>
						<c:otherwise>
							<button class="tab-btn" onclick="window.location.href='${contextPath }/admin/productcomment/detail/${item.id}'">
								查看
							</button>
							<span class="btn-line">|</span>
						</c:otherwise>
					</c:choose>
					<button class="tab-btn" onclick="deleteById('${item.id}');">
						删除
					</button>
				</div>
		</display:column>
	</display:table>
	<div class="clearfix">
   		<div class="fr">
   			 <div cla ss="page">
    			 <div class="p-wrap">
        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			 	</div>
  			 </div>
  		</div>
   	</div>
