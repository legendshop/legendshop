<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<!-- 装载第二第三级商品分类 -->
<li class="box_edit_li">
  <h6><a id="${nsort.nsortId}"  level="2"  href="javascript:void(0);" ondblclick="javascript:jQuery(this.parentNode.parentNode).remove();">${nsort.nsortName}</a></h6>
  <span id="ui-nsort" class="ui-nsortable">
		  <c:forEach items="${nsort.subSort}" var="subSort">
		  		<a  id="${subSort.nsortId}"   pid="${nsort.nsortId}"  level="3"  href="javascript:void(0);" ondblclick="javascript:jQuery(this).remove();">${subSort.nsortName}</a>
		  </c:forEach>
  </span>
</li>
<script type="text/javascript">
jQuery(document).ready(function(){
	  jQuery(".ui-nsortable").sortable();
	  jQuery(".ui-nsortable").disableSelection();
});
</script>