<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<html>
<head>
	<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
    <script src="<ls:templateResource item='/common/default/js/alternative.js'/>" type="text/javascript"></script>
    <title>佣金变更历史表列表</title>
</head>
<body>
    <form:form action="${contextPath}/admin/commisChangeLog/query" id="form1" method="post">
        <table class="${tableclass}" style="width: 100%">
		    <thead>
		    	<tr>
			    	<th>
				    	<a href="<ls:url address='/admin/index'/>" target="_parent">首页</a> &raquo; 商城管理  &raquo; 
				    	<a href="<ls:url address='/admin/commisChangeLog/query'/>">佣金变更历史表</a>
			    	</th>
		    	</tr>
		    </thead>
		    <tbody><tr><td>
		    	    <div align="left" style="padding: 3px">
				       	    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
				        	<ls:auth ifAnyGranted="F_VIEW_ALL_DATA">
				            	&nbsp;商城名称
				            	<input type="text" name="userName" maxlength="50" value="${commisChangeLog.userName}" />
				            </ls:auth>
				            <input type="submit" value="搜索"/>
				            <input type="button" value="创建佣金变更历史表" onclick='window.location="<ls:url address='/admin/commisChangeLog/load'/>"'/>
				      </div>
		     </td></tr></tbody>
	    </table>
    </form:form>
    <div align="center">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		<display:table name="list" requestURI="/admin/commisChangeLog/query" id="item" export="false" sort="external" class="${tableclass}" style="width:100%">
	    
     		<display:column title="Id" property="id"></display:column>
     		<display:column title="Sn" property="sn"></display:column>
     		<display:column title="UserId" property="userId"></display:column>
     		<display:column title="UserName" property="userName"></display:column>
     		<display:column title="Amount" property="amount"></display:column>
     		<display:column title="AddTime" property="addTime"></display:column>
     		<display:column title="LogDesc" property="logDesc"></display:column>

	    <display:column title="Action" media="html">
		      <a href="<ls:url address='/admin/commisChangeLog/load/${item.id}'/>" title="修改">
		     		 <img alt="修改" src="<ls:templateResource item='/common/default/images/grid_edit.png'/>">
		      </a>
		      <a href='javascript:deleteById("${item.id}")' title="删除">
		      		<img alt="删除" src="<ls:templateResource item='/common/default/images/grid_delete.png'/>">
		      </a>
	      </display:column>
	    </display:table>
        <c:if test="${not empty toolBar}">
            <c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
        </c:if>
    </div>
        <script language="JavaScript" type="text/javascript">
			<!--
			  function deleteById(id) {
				  layer.confirm('确定删除 ?', {icon:3}, function(){
			            window.location = "<ls:url address='/admin/commisChangeLog/delete/" + id + "'/>";
				  });
			  }
			
			        function pager(curPageNO){
			            document.getElementById("curPageNO").value=curPageNO;
			            document.getElementById("form1").submit();
			        }
			//-->
		</script>
</body>
</html>

