<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>营销管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link type="text/css"
	href="<ls:templateResource item='/resources/templets/red/css/marketing.css'/>"
	rel="stylesheet" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} no-border" style="width: 100%;">
			    <thead>
			    	<tr>
			    		<th class="no-bg title-th">
			            	<span class="title-span">
								店铺促销  ＞
								<a href="<ls:url address="/admin/marketing/zhekou/query"/>">限时折扣</a>
								  ＞  <span style="color:#0e90d2;">查看详情</span>
							</span>
			            </th>
			    	</tr>
			    </thead>
			</table>
			<table style="width: 100%" class="${tableclass} no-border content-table" id="col1">
				<tr>
					<td style="width:200px;">
						<div align="right">
							活动名称：
						</div>
					</td>
					<td align="left" style="padding-left: 2px;"><span>${marketing.marketName}</span>
					</td>
				</tr>
				<tr>
					<td>
						<div align="right">
							开始时间：
						</div>
					</td>
					<td align="left" style="padding-left: 2px;"><span>${marketing.startTime}</span>
					</td>
				</tr>
				<tr>
					<td>
						<div align="right">
							结束时间：
						</div>
					</td>
					<td align="left" style="padding-left: 2px;"><span>${marketing.endTime}</span>
					</td>
				</tr>
				<tr>
					<td>
						<div align="right">
							活动状态：
						</div>
					</td>
					<td align="left" style="padding-left: 2px;"><c:choose>
							<c:when test="${marketing.state eq 0}">
								<span>未发布</span>
							</c:when>
							<c:when test="${marketing.state eq 1}">
								<span>已发布</span>
							</c:when>
						</c:choose></td>
				</tr>
				<tr>
					<td>
						<div align="right">
							参与商品：
						</div>
					</td>
					<td align="left" style="padding-left: 2px;"><c:choose>
							<c:when test="${marketing.isAllProds eq 0}">
								<span>部分商品</span>
							</c:when>
							<c:when test="${marketing.isAllProds eq 1}">
								<span>全部商品</span>
							</c:when>
						</c:choose></td>
				</tr>
				<tr>
					<td>
						<div align="right">
							活动规则：
						</div>
					</td>
					<td align="left" style="padding-left: 2px;"><span>
						<c:choose>
							<c:when
								test="${not empty marketing.marketingMzRules && (marketing.type ==2)}">
								<ul class="ncsc-mansong-rule-list">
									<c:forEach items="${marketing.marketingMzRules}" var="zk"
										varStatus="status">
										<li>限时价格折扣 打<strong>${zk.offDiscount}</strong>折
										</li>
									</c:forEach>
								</ul>
							</c:when>
							<c:otherwise>
								暂无规则
							</c:otherwise>
						</c:choose>
					</span></td>
				</tr>

				<tr>
					<td align="right" valign="top">
						<div>
							活动商品：
						</div>
					</td>
					<c:choose>
						<c:when test="${marketing.isAllProds==0}">
							<td align="left" style="padding-left: 2px;"><ls:plugin pluginId="b2c">
									<input type="button" value="添加商品" id="btn_show_goods_select" />
								</ls:plugin>
								<div class="search" style="display: none;">
									<div class="div-goods-select" id="div_goods_select">
										<table class="search-form">
											<tbody>
												<tr>
													<th class="w150"><strong>第一步：搜索店内商品</strong></th>
													<td class="w160"><input type="text w150" value=""
														name="goods_name" class="${inputclass}"
														id="search_goods_name"></td>
													<td class="w70 tc"><input type="button"
														id="btn_search_goods" class="${btnclass}"
														onclick="search_goods()" value="搜索" /></td>
													<td class="w10"></td>
													<td><span>不输入名称直接搜索将显示店内所有普通商品，特殊商品不能参加。</span></td>
												</tr>
											</tbody>
										</table>
										<div class="search-result" id="div_goods_search_result"></div>
									</div>
								</div>
								<div id="prodContent"></div></td>
						</c:when>
						<c:when test="${marketing.isAllProds==1 && empty list}">
							<td align="left" style="padding-left: 2px;"><span>全部商品</span></td>
						</c:when>
					</c:choose>
				</tr>
				<tr>
					<td></td>
					<td><input style="margin-left: 50px;"
						type="button" class="${btnclass}" value="返回"
						onclick="window.location='<ls:url address='/admin/marketing/zhekou/query?state=${marketing.state}' />'"></td>
					<td></td>
				</tr>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript">
 var contextPath = '${contextPath}';
     var marketId='${marketing.id}';
     var shopId='${marketing.shopId}';
     jQuery(document).ready(function(){
   	  $("#prodContent").load(contextPath+"/admin/marketing/marketingZkRuleProds/"+marketId+"/"+shopId+"?curPage=1");
		    //现实商品搜索
        $('#btn_show_goods_select').on('click', function() {
        	var text=$(".search").html();
        	layer.open({
        		title: '商品搜索',
        		id: 'showSearch',
        		content:text,
        		area: ['920px', '500px']
        	});
	    }); 
     });

        function search_goods(){
        	var url =contextPath +"/admin/marketing/marketingProds/"+marketId+"?curPage=1&type=2";
            url += '&' + $.param({name: $('#search_goods_name').val()});
            $('#div_goods_search_result').load(url);
        }   
    
</script>
</html>