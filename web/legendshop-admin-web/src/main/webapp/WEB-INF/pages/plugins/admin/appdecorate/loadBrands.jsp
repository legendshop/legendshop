 <%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
<link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
<link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/amazeui.css"/>
	<style type="text/css">
		body{
			min-height:96%;
		}
		#form1{
			margin:15px;
			line-height: 28px;
		}	
		input[type="text"]{
			height:28px;
			border: 1px solid #efefef;
		}
		.see-able{
			width: 95%;
			min-width:680px;
		}
		.see-able th,td{
			height:30px !important;
			min-width:50px !important;
		}
		.edit-gray-btn {
		    color: #333;
		    border: 1px solid #efefef;
		    background-color: #f9f9f9;
		    cursor: pointer;
		    height: 28px;
		    text-align: center;
		    line-height: 24px;
		    margin: 0 3px;
		    font-size: 12px;
		    padding: 0 12px;
		    min-width: 50px;
			display: inline-block;
		}
		
	</style>
</head>
<body>
	 <div style="width: 742px;">
	    <form:form action="${contextPath}/admin/app/decorate/loadBrands" id="form1" method="post">
	 	<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
	    <input type="hidden" id="sourceId" name="sourceId" value="${sourceId}" />
	 	<div style="height:50px;width:705px;background-color: #f9f9f9;border: 1px solid #efefef;font-size: 14px;margin: 10px 3px;">
	 		<div style="padding-top:10px;padding-left:20px;">
			<input style="width:605px;border:1px solid #efefef;padding: 5px;"  type="text" name="brandName" id="brandName" maxlength="20" value="${brandName}" size="20" placeholder="请输入品牌名称" />
			<input type="submit" value="搜索"  class="criteria-btn" />
			</div>
		</div>
	 </form:form>
	<table class="see-able am-table am-table-striped am-table-hover table" style="margin: 0 auto;">
		<thead>
			<tr><th width="180">图片</th><th width="410">品牌名称</th><th width="110">操作</th></tr>
		</thead>
		<tbody>
		<c:if test="${empty requestScope.list}">
			<tr><td colspan="3" style="text-align: center;">没有找到符合条件的品牌</td></tr>
		</c:if>
     	<c:forEach items="${requestScope.list}" var="brand">
          <tr>
          	<td><img  width="90" height="30"  src="<ls:photo item='${brand.brandPic}'  />" /></td>
          	<td>${brand.brandName}</td>
          	<td>
          	   <button class="criteria-btn" onclick="saveChoooseBrand('${brand.brandId}','${brand.brandName}')">选择</button></td>
          </tr>
        </c:forEach>
        </tbody>
      </table>
 	  	<div class="fr" style="display: block;width:100%;">
  			 <div class="page">
	   			 <div class="p-wrap" style="font-size:14px;margin-bottom:5px;">
	       		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
				 </div>
  			 </div>
		</div> 	
 		
	 </div>
	 
      <script type="text/javascript">
          var sourceId="${sourceId}";
          var maxChoose="${maxChoose}";
	      function pager(curPageNO){
	          document.getElementById("curPageNO").value=curPageNO;
	          document.getElementById("form1").submit();
	      }
	      function saveChoooseBrand(brandId,brandName){
	 			//保存信息
	 			window.parent.saveChoooseBrand(brandId,brandName,sourceId);
	 			var index = parent.layer.getFrameIndex('loadBrands'); //先得到当前iframe层的索引
	 			parent.layer.close(index); //再执行关闭 
			}
      </script>
</body>
</html>