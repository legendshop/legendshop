<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<html>
<head>
	   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	   <c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
	   <title>秒杀成功页面</title>
	   <title>秒杀成功页面-${systemConfig.shopName}</title>
       <meta name="keywords" content="${systemConfig.keywords}"/>
       <meta name="description" content="${systemConfig.description}" />
	   <link type="text/css" href="<ls:templateResource item='/resources/templets/css/base.css'/>" rel="stylesheet"/>
       <link type="text/css" href="<ls:templateResource item='/resources/templets/css/style${_style_}.css'/>" rel="stylesheet"/>
</head>
<body class="graybody">
	<div id="doc">
	
	           <!--顶部menu-->
		  <div id="hd">
		      <!--hdtop 开始-->
		       <%@ include file="/WEB-INF/pages/plugins/main/home/header.jsp" %>
		      <!--hdtop 结束-->
		       <!-- nav 开始-->
		        <div id="hdmain_cart">
		          <div class="yt-wrap">
		            <h1 class="ilogo">
		                <a href="${contextPath}/"><img src="<ls:photo item="${systemConfig.logo}"/>"  style="max-height:89px;" /></a>
		            </h1>
		            <!--page tit-->
		              <div class="cartnew-title">
		                        <ul class="step_h">
		                            <li class="done"><span>购物车</span></li>
		                            <li class="on"><span>填写核对订单信息</span></li>
		                            <li class=" "><span>提交订单</span></li>
		                        </ul>
		              </div>
		            <!--page tit end-->
		          </div>
		        </div>
		      <!-- nav 结束-->
		 </div><!--hd end-->
		<!--顶部menu end-->
		 </div><!--hd end-->
		<!--顶部menu end-->
    
    
		
		 <div id="bd">
		      <!--order content -->
		         <div class="yt-wrap ordermain" style="margin-top:20px;">
		          	<div class="cart_done">
		               <span class="bighint2 gou2_back" style=" margin-bottom:10px;">秒杀成功，请您确认商品信息！</span>
		               <div class="v_align_mid carthint" style="color: #c6171f;">温馨提示：点击“确定”按钮进入订单确认页，请勿直接关闭本页面</div>      
		          	</div>
		          <div style="padding:5px 20px 0px 20px;">
		            <table class="ncm-default-table m10" style="font-size: 12px;">
		              <tr>
		                <td style="width: 10%;">您选择了：</td>
		                <td style="width: 20%;"><a href="<ls:url address="/seckill/views/${successProd.seckillId}"/>" style="display: block; text-align: center; height: 100px; line-height: 100px;"><img src="<ls:images item='${successProd.pic}' scale='2'/>" alt="" style="max-width: 100px; vertical-align: middle;"></a></td>
		                <td><a href="#">${successProd.name}</a></td>
		                <td style="width: 20%;">¥${successProd.seckillPrice}</td>
		              </tr>
		            </table>
		          </div>
		          
		          <div class="confirm" id="confirm">
		            <span id="confirm_totalPrice">应付金额：<b>￥<em id="totalPrice">${successProd.seckillPrice}</em></b></span>
		            <button class="on" onclick="window.location.href='${contextPath}/p/seckill/order/${successProd.seckillId}/${_md5}/${successProd.prodId}/${successProd.skuId}/goOrder'" type="button" id="next_button">去付款</button>
		          </div>
		          
		        </div>
		<!--order content end-->
		   </div><!--bd end-->
				
		 <%@ include file="/WEB-INF/pages/plugins/main/home/bottom.jsp" %>
	</div>
</body>
</html>