<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<style>
	/* 页码 */
.pages {
  margin: 15px 15px 0 0;
  text-align: right;
  display: block;
}
.pages .page-panel {
  vertical-align: top;
  text-align: center;
  display: inline-block;
	*zoom: 1;
}
.pages .page-panel:before ,
.pages .page-panel:after {
  display: table;
  content: "";
}
.pages .page-panel:after {
  clear: both;
}
.pages .page-panel .item {
  float: left;
  margin-left: 5px;
  display: block;
  padding: 3px 5px;
  min-width: 20px;
  height: 24px;
  line-height: 24px;
  word-break: keep-all;
  border: 1px solid #e4e4e4;
  transition: all 0.1s ease;
  -o-transition: all 0.1s ease;
  -moz-transition: all 0.1s ease;
  -webkit-transition: all 0.1s ease;
  color: #333;
  font-size: 12px;
  font-family: arial;
  background: #fff;
  box-sizing: unset;
}
.pages .page-panel .item:hover {
  color: #0e90d2;
}
.pages .page-panel .cur ,
.pages .page-panel .cur:hover {
  background: #0e90d2;
  color: #fff;
  border-color: #0e90d2;
}
.pages .page-panel .ellipsis {
  float: left;
  display: block;
  width: 30px;
  height: 32px;
  line-height: 24px;
  color: #666;
  text-align: center;
  margin-right: -5px;
  color: #999;
}
.pages .page-panel .go-page {
  color: #999;
  float: left;
  display: block;
  height: 32px;
  line-height: 30px;
  margin-left: 10px;
}
.pages .page-panel .go-page  select {
	height: 100%;
	border: 1px solid #e4e4e4;
	color: #333;
}
.pages .page-panel .go-page .page-input {
  width: 28px;
  height: 24px;
  padding: 2px 5px 3px;
  margin: 0 5px;
  text-align: center;
  border: 1px solid #e4e4e4;
  vertical-align: top;
  color: #333;
  outline: none;
}
.pages .page-panel .go-page .page-btn {
  margin-left: 5px;
  display: inline-block;
  padding: 3px 7px;
  min-width: 28px;
  height: 24px;
  line-height: 24px;
  word-break: keep-all;
  border: 1px solid #e4e4e4;
  transition: all 0.1s ease;
  -o-transition: all 0.1s ease;
  -moz-transition: all 0.1s ease;
  -webkit-transition: all 0.1s ease;
  font-size: 12px;
  font-family: arial;
  vertical-align: top;
  color: #333;
}
.pages .page-panel .go-page .page-btn:hover {
  color: #0e90d2;
}
.pages .page-panel .item.prev , .pages .page-panel .item.next {
  padding: 3px 13px;
}
.pages .page-panel .total-num {
  float: left;
  display: block;
  height: 32px;
  line-height: 30px;
  margin-left: 10px;
  color: #333;
}
.pages .page-panel .total-num .num {
  margin: 0 3px;
  font-family: arial;
}
.pages .page-panel .item.default ,
.pages .page-panel .item.default:hover {
  color: #d2d2d2;
  cursor: not-allowed;
  background: #fff;
  border-color: #ededed;
}
/* end 页码 */
</style>
<ul class="floor_sear_pro" style="width:550px;margin-bottom:15px;">
    <c:forEach items="${requestScope.list}" var="brand" varStatus="status">
  <li style="width:100px">
  	 <a href="javascript:void(0);" class="floor_sear_a"  style="width:90px" onclick="goods_list_set('${brand.brandPic}','${brand.brandId}','${brand.brandName}');"> 
  		<span  class="floor_sear_img" style="width:90px;height:30px"> 
 			<img  id="${brand.brandId}" title="${brand.brandName}" src="<ls:photo item='${brand.brandPic}' />" style="width:90px;height:30px" />
  		</span>
   		</a> 
  </li>
	</c:forEach>
</ul>
<div style="display: block;width:558px;text-align: center;">
	<div class="page">
		<div class="p-wrap">
			<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple" actionUrl="javascript:selectBrandPager"/> 
		</div>
	</div>
</div>
