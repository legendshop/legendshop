<!DOCTYPE HTML>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css"/>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">

<form:form action="${contextPath}/admin/newsCategory/save" method="post" id="form1" enctype="multipart/form-data">
    <input id="newsCategoryId" name="newsCategoryId" value="${bean.newsCategoryId}" type="hidden">
    <div align="center">
        <table class="${tableclass}" style="width: 100%">
            <thead>
            <tr>
                <th class="no-bg title-th">
            		<span class="title-span">
						帮助中心  ＞
						<a href="<ls:url address="/admin/newsCategory/query"/>">帮助栏目</a>
						<c:if test="${not empty bean}">  ＞  <span style="color:#0e90d2;">栏目编辑</span></c:if>
						<c:if test="${empty bean}">  ＞  <span style="color:#0e90d2;">栏目新增</span></c:if>
					</span>
	            </th>
            </tr>
            </thead>
        </table>
        <table align="center" class="${tableclass} no-border content-table" id="col1">
            <tr>
                <td style="width:200px;">
                    <div align="right"><font color="ff0000">*</font>栏目名称： </div>
                </td>
                <td align="left" style="padding-left: 2px;">
                    <input type="text" class="${inputclass}" name="newsCategoryName" id="newsCategoryName" value="${bean.newsCategoryName}" size="50" maxlength="20"/>
                    &nbsp;&nbsp;&nbsp;&nbsp;<font>顺序</font>&nbsp;&nbsp;&nbsp;<input type="text" size="5" maxlength="5" value="${bean.seq}" name="seq" id="seq">
                </td>
            </tr>
            <tr>
                <td>
                    <div align="right"><font color="ff0000">*</font>状态：</div>
                </td>
                <td align="left" style="padding-left: 2px;">
                    <select id="status" name="status">
                        <c:choose>
                            <c:when test="${bean.status == '0'}">
                                <option value="1">上线</option>
                                <option value="0" selected="selected">下线</option>
                            </c:when>
                            <c:otherwise>
                                <option value="1" selected="selected">上线</option>
                                <option value="0">下线</option>
                            </c:otherwise>
                        </c:choose>
                    </select>
                </td>
            </tr>
           <%-- <tr>
                <td align="right" valign="top">
                  	  <font color="ff0000">*</font>上传图片：
                </td>
                <td align="left" style="padding-left: 2px;">

                    <input type="file" name="file" id="file" size="30"/><br><span>图片像素为:133*54</span>

                    <input type="hidden" name="imgName" id="imgName" size="30" value="${bean.pic}"/>
                </td>
            </tr>--%>
           <%-- <c:if test="${not empty bean.pic}">
                <tr>
                    <td align="right" valign="top">
                        	原有图片：
                    </td>
                    <td align="left" style="padding-left: 2px;">
                        <img src="<ls:photo item='${bean.pic}'/>" height="54"/>
                    </td>
                </tr>
            </c:if>--%>
            <tr>
            	<td></td>
                <td align="left" style="padding-left: 2px;">
                    <div>
                        <input type="submit" value="保存" class="${btnclass}"/>
                        <input type="button" value="返回" class="${btnclass}"
                               onclick="window.location='${contextPath}/admin/newsCategory/query'"/>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</form:form>
</div>
</div>
</body>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/checkImage.js'/>" type="text/javascript"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/newsCategory.js'/>"></script>
<script language="javascript">
    jQuery.validator.setDefaults({});
</script>
</html>
