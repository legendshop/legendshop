<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<html>
  <head>
    <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
    <%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
    <link type="text/css" href="<ls:templateResource item='/resources/templets/amaze/css/indexlayout.css'/>" rel="stylesheet"/>
    <title>楼层编辑界面</title>
     
     <style type="text/css">
     	.flooredit{text-align: center;background-color: rgba(0, 0, 0, 0.5);position: absolute;display: none;}
     </style>
  </head>
  <body >
  <form:form  action="${contextPath}/admin/system/floor/query" id="form1" method="post">
        <table class="${tableclass}" style="width: 98%;margin: 10px" >
		    <thead>
		    	<tr>
			    	<th height="40">
				    	<a href="<ls:url address='/admin/index'/>" target="_parent" class="blackcolor">首页</a> &raquo; 
				    	<a href="<ls:url address='/admin/floor/query'/>">楼层管理</a>
			    	</th>
		    	</tr>
		    </thead>
	    </table>
	  </form:form>
	     <div id="doc">
	      <div id="bd">
			<div class="wide_floor">
			 	<div class="wide_floor_4">
				   <div id="floorImage" class="flooredit" style="width: 1190px; height: 25px;z-index:1000;"><a  href="javascript:onclickAdv(${floor.flId})"  style="color:white;">广告编辑(297*80)</a></div>
					 <div class="wide_floor_4_m">
					    <c:choose>
				         <c:when test="${empty advList}">
				               <div class="wide_floor_4_one_bottom">
								</div>
								<div class="wide_floor_4_one_bottom"></div>
								<div class="wide_floor_4_one_bottom"></div>
								<div class="wide_floor_4_one_bottom"></div>
								<div class="wide_floor_4_one_bottom"></div>
								<div class="wide_floor_4_one_bottom"></div>
								<div class="wide_floor_4_one_bottom"></div>
								<div class="wide_floor_4_one_bottom"></div>
				         </c:when>
				         <c:otherwise>
				          <c:forEach items="${advList}" var="adv" end="8"> 
				             <div class="wide_floor_4_one_bottom">
							   <a href="${adv.linkUrl}" target="_blank"><img src="<ls:photo item='${adv.picUrl}'/>"></a>    
							   </a>
							</div>
	      		 		  </c:forEach>
				         </c:otherwise>
				      </c:choose>
					</div>
				</div>
			</div>
	          <div   style="text-align: center;clear: both; padding-top : 30px;" >
	          	      <input type="button" onclick="window.location='${contextPath}/admin/floor/query' " class="criteria-btn" value="返回">
	     		 </div>
		</div>
     </div>
<script src="<ls:templateResource item='/resources/common/js/alternative.js'/>" type="text/javascript"></script>
 <script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/floorWideAdvEight.js'/>"></script>
<script type="text/javascript">
var contextPath = "${contextPath}";
</script>
</html>
