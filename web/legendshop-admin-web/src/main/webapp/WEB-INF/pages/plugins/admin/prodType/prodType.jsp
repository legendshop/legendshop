<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>创建类型 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
        <form:form  action="${contextPath}/admin/prodType/save" method="post" id="form1" >
            <input id="id" name="id" value="${prodType.id}" type="hidden">
            <div align="center">
            <table class="${tableclass} no-border" style="width: 100%">
                <thead>
                    <tr class="sortable">
                        <th class="no-bg title-th">
                             <span class="title-span">属性管理  ＞  <a href="<ls:url address="/admin/prodType/query"/>" style="color: #0e90d2;">属性类型管理</a>  ＞  <span style="color:#0e90d2;">创建类型</span></span>
                        </th>
                    </tr>
                </thead>
            <table border="0" align="center" class="${tableclass} no-border content-table" id="col1" style="width: 100%">
				<tr>
				        <td width="200px;">
				          	<div align="right" style="margin-top: -22px;"><font color="ff0000">*</font>产品类型名称：</div>
				       	</td>
				        <td align="left" style="padding-left: 2px;">
				           	<input type="text" style="width:300px;" name="name" id="name" value="${prodType.name}"  size="32" maxlength="50"/> 
				           	<br><span style="color:#999;">(跟商品类目挂钩，建议名称中带有类目的关键字以方便标示)</span>
				        </td>
				</tr>
				<tr>
				        <td>
				          	<div align="right"><font color="ff0000">*</font>次序：</div>
				       	</td>
				        <td align="left" style="padding-left: 2px;">
				           	<input style="width:45px;" type="text" name="sequence" id="sequence" value="${prodType.sequence}" maxlength="5" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')"/>
				        </td>
				</tr>
		
<%--						<tr>--%>
<%--				        <td>--%>
<%--				          	<div align="right">是否允许用户编辑属性：</div>--%>
<%--				       	</td>--%>
<%--				        <td align="left" style="padding-left: 2px;">--%>
<%--				           	<select id="attrEditable" name="attrEditable">--%>
<%--						    	<ls:optionGroup type="select" required="true" cache="true" beanName="YES_NO" selectedValue="${prodType.attrEditable}"/>--%>
<%--							</select>--%>
<%--				        </td>--%>
<%--				</tr>--%>

<%--						<tr>--%>
<%--				        <td>--%>
<%--				          	<div align="right">是否允许用户编辑参数：</div>--%>
<%--				       	</td>--%>
<%--				        <td align="left" style="padding-left: 2px;">--%>
<%--				           	<select id="paramEditable" name="paramEditable">--%>
<%--						    	<ls:optionGroup type="select" required="true" cache="true" beanName="YES_NO" selectedValue="${prodType.paramEditable}"/>--%>
<%--							</select>--%>
<%--				        </td>--%>
<%--				</tr>--%>
                <tr>
                	<td></td>
                    <td align="left" style="padding-left: 2px;">
                        <div>
                            <input class="${btnclass}" type="submit" value="保存" />
                            <input class="${btnclass}" type="button" value="返回" onclick="window.location='<ls:url address="/admin/prodType/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
</div>
</div>
</body>

         <script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
          <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
        <script language="javascript">
		 $(document).ready(function() {
		        jQuery("#form1").validate({
		             rules: {
		                name: {
		                	required:true,
		                	maxlength:10
		                },
		                sequence:{
		                	number : true,
		    				required: true
		                }
		            },
		            messages: {
		                name: {
		                    required: "请输入类型名称",
		                    maxlength:"分类名称长度不得超过10"
		                },
		                sequence:{
		                	number : "请输入数字",
		    				required: "请输入顺序"
		                }
		            }
		        });
 
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
});
</script>
</html>