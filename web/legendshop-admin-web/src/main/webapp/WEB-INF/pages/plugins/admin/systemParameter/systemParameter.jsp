<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="LegendShop 多用户商城系统">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
        <form:form  action="${contextPath}/admin/system/systemParameter/save" method="get" id="form1">
            <input id="name" name="name" value="${bean.name}" type="hidden">
            <input id="groupId" name="groupId" value="${bean.groupId}" type="hidden">
            <div align="center">
            <table  align="center" class="${tableclass} no-border content-table" id="col1" style="margin-top: 0px;">
                <thead>
                    <tr class="sortable">
                        <th colspan="2" style="background-color: white !important;border-bottom: 1px solid #dddddd;">
                            <div align="center">
                                ${bean.name} 参数
                            </div>
                        </th>
                    </tr>
                </thead>
        <tr>
        <td width="200px;">
          <div align="right" >功能说明：</div>
       </td>
        <td align="left" style="padding-left: 2px;">${bean.memo}</td>
      </tr>
      <tr>
        <td valign="top"><div align="right">参数值：</div></td>
        <td align="left" valign="top" style="padding-left: 2px;">
         <c:choose>
           	<c:when test="${bean.type == 'Selection' || bean.type == 'Boolean'}">
		       <select id="value" name="value">
				  <ls:optionGroup type="select" required="true" cache="true" beanName="${bean.optional}" selectedValue="${bean.value}"/>
	            </select>
           	</c:when>
           	<c:when test="${bean.type == 'Password'}">
           		 <input type="password" name="value" id="value"  size="50" maxlength="200" class="${inputclass}" autocomplete="off"/>
           	 </c:when>
           	<c:when test="${bean.type == 'JSON'}">
           		 <textarea id="value" name="value" cols="80" rows="3">${bean.value}</textarea>
           	 </c:when>
           	<c:otherwise>
           		<input type="text" name="value" id="value" value="${bean.value}" size="50" maxlength="50" class="${inputclass}"/>
           	</c:otherwise>
           </c:choose>
        </td>
      </tr>
                <tr>
                	<td></td>
                    <td style="padding-left: 0px;">
                        <div>
	                        <input type="button" value="保存" onclick="submitForm();" class="${btnclass}" <c:if test="${not empty rs}">disabled style="border: 0px"</c:if> />
                            <input type="button" value="返回" class="${btnclass}"
                                onclick="window.location='javascript:history.go(-1)'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
        <c:if test="${bean.name eq 'SHOP_BILL_DAY'}">
	        <table class="${tableclass}">
			    <tr>
			        <td align="left" style="text-align: left;">说明：<br>
			            1.结算时间有三种：日结（第二天出账单）周结（周一出账单）月结（此设置有效）。<br>
			            2.具体结算方式由商城配置。<br>
			        </td>
			    <tr>
			</table>
        </c:if>
        <c:if test="${bean.name eq 'SHOP_COMMIS_RATE'}">
	        <table class="${tableclass}">
			    <tr>
			        <td align="left" style="text-align: left;">说明：<br>
			            1.结算佣金比例设置说明:例如输入5,即佣金比例为5%<br>
			            2.结算佣金比例设置上限为50%<br>
			        </td>
			    <tr>
			</table>
        </c:if>
        <c:if test="${bean.name eq 'BILL_PERIOD_TYPE'}">
	        <table class="${tableclass}">
			    <tr>
			        <td align="left" style="text-align: left;">说明：<br>
			            1.该配置不支持页面修改，如需改动，需要技术人员进行数据库修改参数<br>
			            2.每天:DAY , 每周：WEEK , 每月：MONTH  <br>
			        </td>
			    <tr>
			</table>
        </c:if>
        </div>
        </div>
        </body>
        <script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
		<script type="text/javascript">
		var id = "${id}";
		var beanName = "${bean.name}";
			$(function() {
				  $("#form1").validate({
				    rules: {
				      value: { 
				      	required : true,
		 		      }
				    },
				    messages: {
				      value: {
				      	required : "参数值不能为空!"
				      },
				    }
				});
			});
			
			function submitForm(){
				if (beanName === 'SHOP_COMMIS_RATE') {
					var value = $("#value").val();
					
					var reg = /^\d$|^[0-4][0-9]$|50/ig;
					if(!reg.test(value)){
						layer.msg("请输入正整数，且范围为0-50");
						return false;
					}
				}
				$("#form1").submit();
			}
			
		</script>
</html>
