<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>头条新闻管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
<link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.css'/>" />
</head>

<style>
label{
color:red;
}
</style>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content" style="overflow-x: auto;">
			<form:form action="${contextPath}/admin/headlineAdmin/update" method="post" id="form1" onsubmit="return check();" enctype="multipart/form-data">
				<div align="center">
					<table class="${tableclass}">
						<thead>
							<tr>
								<th class="no-bg title-th">
									<span class="title-span">
										新闻管理  ＞
										<a href="<ls:url address="/admin/headlineAdmin/query"/>">头条新闻管理</a>
										  ＞  <span style="color:#0e90d2;">修改头条</span>
									</span>
								</th>
							</tr>
						</thead>
					</table>
					<table class="${tableclass} no-border content-table" id="col1" style="width:100%"> 
						<tr>
							<td style="width:200px;">
								<div align="right">
									<font color="ff0000">*</font>头条名称：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" name="title" id="title"
								value="${headline.title }" /></td>
						</tr>
						<tr>
							<td align="right" valign="top">
									<font color="ff0000">*</font>图片：
							</td>
							<td align="left" style="padding-left: 2px;"><input id="id" name="id" value="${headline.id}"
								type="hidden">
								<div id="choose">
									<img src="<ls:photo item='${headline.pic}'/>" height="120" width="150" id="image" /> &nbsp;&nbsp;&nbsp;&nbsp; 
									<br><input type="button" value="更改图片" style="margin-top:10px;margin-left:0;" class="criteria-btn" onclick="change()" id="btn">
								</div>

								<div style="display: none;" id="upload">
									<input type="file" name="imageFile" id="prodImg" onchange="changeImage()" /> 
									<input type="button" value="取消" style="margin-top:10px;margin-left:0;" class="criteria-btn" onclick="hides()">
								</div>
								<div style="display: none;" id="previewImage">
									<img id="ImgPr" width="120" height="120" /> 
									<br><input type="button" value="更改图片" style="margin-top:10px;margin-left:0;" class="criteria-btn" onclick="change()" id="btn">
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>头条顺序：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" name="seq" id="seq"
								value="${headline.seq }" /></td>
						</tr>
						<tr>
							<td align="right" valign="top">
									<font color="ff0000">*</font>头条描述：
							</td>
							<td align="left" style="padding-left: 2px;"><textarea name="detail" id="detail" cols="100" rows="8"
									style="width: 100%; height: 400px; visibility: hidden;">${headline.detail }</textarea>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<div align="center">
									<input type="submit" class="criteria-btn" value="保存" /> 
									<input type="button" class="criteria-btn" value="返回" onclick="window.location='<ls:url address="/admin/headlineAdmin/query"/>'" />
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>

<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
<script type='text/javascript' src="${contextPath}/resources/common/js/jquery.js"></script>
<script type="text/javascript" src='<ls:templateResource item="/resources/common/js/checkImage.js"/>'></script>
<script type='text/javascript' src="${contextPath}/resources/common/js/jquery.validate.js" /></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/headlineUpdate.js'/>"></script>
<script type="text/javascript">
var contextPath = "${contextPath}";
$.validator.setDefaults({});
var UUID = "${cookie.SESSION.value}";
</script>
<script src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>" type="text/javascript"></script>
</html>

