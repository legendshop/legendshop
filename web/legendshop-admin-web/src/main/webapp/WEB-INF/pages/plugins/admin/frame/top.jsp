<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/jquery2.1.4.jsp"%>
<%
	int port = request.getServerPort();
  	String domainPath = "";
  	if(port==80){
  		domainPath = request.getScheme()+"://"+request.getServerName();
  	}else{
  		domainPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort();
  	}
%>

 <link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/amazeui.css"/>
 <link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/admin.css">
 <script src="${contextPath}/resources/templets/amaze/js/amazeui.min.js"></script>
 <script src="${contextPath}/resources/templets/amaze/js/app.js"></script>
 <script src="<ls:templateResource item='/resources/common/js/alternative.js'/>" type="text/javascript"></script>
  <script src="<ls:templateResource item='/resources/common/js/recordstatus.js'/>" type="text/javascript"></script>
  <script src="<ls:templateResource item='/resources/common/js/cookieUtil.js'/>" type="text/javascript"></script>
  <script type="text/javascript">
     var contextPath = '${contextPath}';
 	var domainPath = '<%=domainPath%>';
 	var ua = navigator.userAgent;
 	clickType = (ua.match(/iPad/i)) ? "touchstart" : "click";
 </script>
 <script src="<ls:templateResource item='/resources/templets/amaze/js/legendshop.js'/>"></script>
 <c:set var="adminMessageDtoList"  value="${indexApi:getMessage(pageContext.request)}"></c:set>
<header class="am-topbar admin-header">
  <div class="am-topbar-brand"  style="width:260px;">
    <strong><a id="backendLogo" href="${contextPath}/admin/index" data-type="index">${shopName}</a> </strong>
  </div>
  <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>
  <div class="am-collapse am-topbar-collapse" id="topbar-collapse">
    <ul class="am-nav am-nav-pills am-topbar-nav am-topbar-left admin-header-list">
    <c:forEach items="${sessionScope.MENU_LIST}"  var="menu" varStatus="status">
     	<li><a href="${contextPath}${menu.action}"  action="${menu.menuAction}"  data-type="top"  <c:if test="${currentMenu.menuId eq menu.menuId}">class="am-topbar-nav-a"</c:if>
     	<c:if test="${currentMenu.menuId==0 &&status.index==0 }">class="am-topbar-nav-a"</c:if> >${menu.name}</a></li>
    </c:forEach>
    </ul>
    <ul class="am-nav am-nav-pills am-topbar-nav am-topbar-right admin-header-list">
    <li><a href="${PC_DOMAIN_NAME}"><span class="am-icon-home"></span> 商城首页</a>
       </li>
         <li>
         	<a href="javascript:void(0);" id="shopIndex"><span class="am-icon-envelope-o"></span> 消息箱 <span class="am-badge am-badge-warning">${adminMessageDtoList.totalMessage}</span></a>
            <ul class="am-dropdown-content" id="message">
               <c:forEach items="${adminMessageDtoList.list}" var="adminMessageDto" varStatus="status">
               <c:if test="${status.index < 4 && adminMessageDto.num > 0}">
               <li><a data-menu="${adminMessageDto.menuId}" href="${contextPath}${adminMessageDto.url}"><span class=""></span>${adminMessageDto.name}<font size="3" color="red"> ${adminMessageDto.num}</font> 条</a></li>
               </c:if>
               <c:if test="${status.index >= 4 && adminMessageDto.num > 0}">
               <li style="display:none"><a data-menu="${adminMessageDto.menuId}" href="${contextPath}${adminMessageDto.url}"><span class=""></span>${adminMessageDto.name}<font size="3" color="red"> ${adminMessageDto.num}</font> 条</a></li>
               </c:if>
               </c:forEach>
               <li id="check_more" style="display:block;"><a href="#" onclick="checkmore()"><span class=""></span>查看更多......</a></li>
               <li id="slip" style="display:none;"><a href="#" onclick="slip()"><span class=""></span><font color="#ff0000">收起</font></a></li>
	        </ul>
      </li>
    <!--
      <li><a href="javascript:;"><span class="am-icon-envelope-o"></span> 收件箱 <span class="am-badge am-badge-warning">5</span></a></li>
       -->
       <%--<li><a href="${DOMAIN_NAME}"><span class="am-icon-home"></span> 商城首页</a></li>--%>
      <li  id="myProfile" name="myProfile" >
        <div class="am-dropdown" data-am-dropdown style="margin-top:15px;">
           <a class="am-dropdown-toggle" data-am-dropdown-toggle href="javascript:void(0);">
              <span class="am-icon-user"></span>  您好: <b> ${sessionScope.SPRING_SECURITY_LAST_USERNAME}</b>&nbsp;<span class="am-icon-caret-down"></span>
           </a>
	        <ul class="am-dropdown-content"  >
	        <!--
	          <li><a href="#"><span class="am-icon-user"></span> 资料</a></li>
	           -->
	          <li><a href="javascript:changeAdminPwdDialog(this)"><span class="am-icon-cog"></span> 修改密码</a></li>
	        </ul>
        </div>
      </li>
      <li><a href="${contextPath}/admin/logout"><span class="am-icon-power-off"></span> 退出</a></li>
      <!--
      <li class="am-hide-sm-only"><a href="javascript:;" id="admin-fullscreen"><span class="am-icon-arrows-alt"></span> <span class="admin-fullText">开启全屏</span></a></li>
       -->
    </ul>
  </div>
</header>
