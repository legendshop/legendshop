<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
			Integer offset = (Integer)request.getAttribute("offset");
%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>举报管理 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
</head>
<body>
    <jsp:include page="/admin/top" />
    <div class="am-cf admin-main">
        <jsp:include page="../frame/left.jsp"></jsp:include>
     <div class="admin-content" id="admin-content" style="overflow-x:auto;">
     <table class="${tableclass} title-border" style="width: 100%">
	    	<tr>
	    		<th class="title-border">咨询管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">举报管理</span></th>
	    	</tr>
	 </table>
	    <form:form  action="${contextPath}/admin/accusation/query" id="form1" method="get">
    	    <div class="criteria-div">
    	    <span class="item">
		       	    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
		       	    <input type="text"  id="shopId" name="shopId" value="${accusation.shopId}"/>
		       	  	 <input type="hidden" class="${inputclass}" id="shopName" name="shopName"  value="${accusation.shopName}"/>
		    </span>
		    <span class="item">
		            <input class="${inputclass}" type="text" id="userName" name="userName" maxlength="50" value="${accusation.userName}" placeholder="举报人ID"/>
		    </span>
		            <input class="${inputclass}" type="text" id="nickName" name="nickName" maxlength="50" value="${accusation.nickName}" placeholder="举报人昵称"/>
		    </span>
		    <span class="item" style="padding-left: 10px;">
		            状态：
		           <select id="status" name="status" class="criteria-select">
			           	<ls:optionGroup type="select" required="false" cache="true" beanName="ACCU_STATUS" selectedValue="${accusation.status}"/>
        			</select>
		            <input class="${btnclass}" type="button" onclick="search()" value="搜索"/>
		    </span>
		    </div>
	    </form:form>
	    <div align="center" id="accusationContentList" name="accusationContentList" class="order-content-list"></div>
      </div>
    </div>
</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
	<script language="JavaScript" type="text/javascript">
	       var shopName='';
	 
			$(document).ready(function () {
				if(shopName == ''){
			        makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'shopName');
			    }else{
			    	makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",shopName, 'shopName');
					$("#select2-chosen-1").html(shopName);
			    } 
				
				sendData(1);
				
			});	
	        function pager(curPageNO){
	            document.getElementById("curPageNO").value=curPageNO;
	            document.getElementById("form1").submit();
	        }
	        function search(){
			  	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
			  	sendData(1);
			}
	        function sendData(curPageNO) {
	            var data = {
	                "shopId": $("#shopId").val(),
	                "curPageNO": curPageNO,
	                "status": $("#status").val(),
	                "userName": $("#userName").val(),
	                "nickName": $("#nickName").val(),
	            };
	            $.ajax({
	                url: contextPath+"/admin/accusation/queryContent",
	                data: data,
	                type: 'post',
	                async: true, //默认为true 异步   
	                success: function (result) {
	                    $("#accusationContentList").html(result);
	                }
	            });
	        }

	        function makeSelect2(url,element,text,label,value,holder, textValue){
	        	$("#"+element).select2({
	                placeholder: holder,
	                allowClear: true,
	                width:'200px',
	                ajax: {  
	            	  url : url,
	            	  data: function (term,page) {
	                        return {
	                        	 q: term,
	                        	 pageSize: 10,    //一次性加载的数据条数
	                         	 currPage: page, //要查询的页码
	                        };
	                    },
	        			type : "GET",
	        			dataType : "JSON",
	                    results: function (data, page, query) {
	                    	if(page * 5 < data.total){//判断是否还有数据加载
	                    		data.more = true;
	                    	}
	                        return data;
	                    },
	                    cache: true,
	                    quietMillis: 200//延时
	              }, 
	                formatInputTooShort: "请输入" + text,
	                formatNoMatches: "没有匹配的" + text,
	                formatSearching: "查询中...",
	                formatResult: function(row,container) {//选中后select2显示的 内容
	                    return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
	                },formatSelection: function(row) { //选择的时候，需要保存选中的id
	                	$("#" + textValue).val(row.text);
	                    return row.text;//选择时需要显示的列表内容
	                }, 
	            });
	        }
	        
	        function pager(curPageNO) {
	            $("#curPageNO").val(curPageNO);
	            sendData(curPageNO);
	        }
	</script>

</html>