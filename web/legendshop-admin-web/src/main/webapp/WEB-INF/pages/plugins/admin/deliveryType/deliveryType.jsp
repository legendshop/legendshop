<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>配送方式编辑 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
					<tr>
						<th class="title-border">配送管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/deliveryType/query"/>">配送方式管理</a>
                				&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">配送方式编辑</span>
						</th>
					</tr>
			</table>
			<form:form action="${contextPath}/admin/deliveryType/save"
				method="post" id="form1">
				<input id="dvyTypeId" name="dvyTypeId" value="${deliveryType.dvyTypeId}" type="hidden">
				<input id="isSystem"  value="${deliveryType.isSystem}" type="hidden">
				<div align="center" style="margin-top: 10px;">
					<table border="0" align="center" class="${tableclass} no-border content-table" id="col1">

						<tr>
							<td width="200px">
								<div align="right">
									<font color="ff0000">*</font>配送名称：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" name="name" id="name"
								value="${deliveryType.name}" class="${inputclass}" maxlength="20"/></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>默认物流公司：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><select id="dvyId" name="dvyId">
									<ls:optionGroup type="select" required="true" cache="fasle"
										defaultDisp="-- 物流公司 --"
										sql="select t.dvy_id, t.name from ls_delivery t"
										selectedValue="${deliveryType.dvyId}" />
							</select></td>
						</tr>
<%--						<tr>--%>
<%--							<td>--%>
<%--								<div align="right">--%>
<%--									<font color="ff0000">*</font>物流配送打印模版：--%>
<%--								</div>--%>
<%--							</td>--%>
<%--							<td align="left" style="padding-left: 2px;"><select id="printtempId" name="printtempId">--%>
<%--									<ls:optionGroup type="select" required="true" cache="fasle"--%>
<%--										defaultDisp="-- 打印模版 --"--%>
<%--										sql="select t.prt_tmpl_id, t.prt_tmpl_title from ls_print_tmpl t "--%>
<%--										selectedValue="${deliveryType.printtempId}" />--%>
<%--							</select></td>--%>
<%--						</tr>--%>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>是否系统默认：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<label class="radio-wrapper <c:if test='${empty deliveryType.isSystem or deliveryType.isSystem == "1"}'>radio-wrapper-checked</c:if>">
									<span class="radio-item">
										<input type="radio" class="radio-input" value="1" name="isSystem" id="systemRadio"
<%--										<c:if test="${empty deliveryType.isSystem or deliveryType.isSystem == '1'}">checked="checked"</c:if>--%>
										>
										<span class="radio-inner"></span>
									</span>
									<span class="radio-txt">系统</span>
								</label>
								<label class="radio-wrapper <c:if test='${deliveryType.isSystem == "0"}'>radio-wrapper-checked</c:if>">
									<span class="radio-item">
										<input type="radio" class="radio-input" value="0" name="isSystem" id="ordinaryRadio"
<%--										<c:if test="${printTmpl.isSystem==0}">checked="checked"</c:if>--%>
										>
										<span class="radio-inner"></span>
									</span>
									<span class="radio-txt">普通</span>
								</label>
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">描述：</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" name="notes" id="notes"
								value="${deliveryType.notes}" class="${inputclass}" maxlength="49"/></td>
						</tr>
						<tr>
							<td></td>
							<td align="left" style="padding-left: 0px;">
								<div>
									<input type="submit" value="保存" class="${btnclass}" /> 
									<input type="button" value="返回" class="${btnclass}"
										onclick="window.location='<ls:url address="/admin/deliveryType/query"/>'" />
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>

<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/deliveryType.js'/>"></script>
<script type="text/javascript">
	$.validator.setDefaults({});
	$("input:radio[name='isSystem']").change(function(){
		 $("input:radio[name='isSystem']").each(function(){
			 if(this.checked){
				 $(this).parent().parent().addClass("radio-wrapper-checked");
			 }else{
				 $(this).parent().parent().removeClass("radio-wrapper-checked");
			 }
		});
	});


	$(function () {
		let isSystem = $("#isSystem").val();
		if(isSystem && isSystem === '0'){
			$("#ordinaryRadio").prop("checked",true);
		}else{
			$("#systemRadio").prop("checked",false);
		}

	});

</script>

</html>
