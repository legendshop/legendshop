<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<html>
    <head>
        <title>创建公用的产品类目</title>
         <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
         <script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
        <script language="javascript">
		    $.validator.setDefaults({
		    });

    $(document).ready(function() {
    jQuery("#form1").validate({
            rules: {
            banner: {
                required: true,
                minlength: 5
            },
            url: "required"
        },
        messages: {
            banner: {
                required: "Please enter banner",
                minlength: "banner must consist of at least 5 characters"
            },
            url: {
                required: "Please provide a password"
            }
        }
    });
 
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
});
</script>
</head>
    <body>
        <form:form action="${contextPath}/admin/category/save" method="post" id="form1">
            <input id="id" name="id" value="${category.id}" type="hidden">
            <div align="center">
            <table border="0" align="center" class="${tableclass}" id="col1">
                <thead>
                    <tr class="sortable">
                        <th colspan="2">
                            <div align="center">
                                	创建商品类目
                            </div>
                        </th>
                    </tr>
                </thead>
                
		<tr>
		        <td>
		          	<div align="center">父节点: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="parentId" id="parentId" value="${category.parentId}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">产品类目名称: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="name" id="name" value="${category.name}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">类目的显示图片: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="pic" id="pic" value="${category.pic}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">产品类目类型,参见ProductTypeEnum: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="type" id="type" value="${category.type}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">排序: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="seq" id="seq" value="${category.seq}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">默认是1，表示正常状态,0为下线状态: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="status" id="status" value="${category.status}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">是否Header菜单展示，0否，1是: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="headerMenu" id="headerMenu" value="${category.headerMenu}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">导航菜单中显示，0否1是: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="navigationMenu" id="navigationMenu" value="${category.navigationMenu}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">类型ID: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="typeId" id="typeId" value="${category.typeId}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">SEO关键字: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="keyword" id="keyword" value="${category.keyword}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">SEO描述: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="desc" id="desc" value="${category.desc}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">SEO标题: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="title" id="title" value="${category.title}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">记录时间: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="recDate" id="recDate" value="${category.recDate}" />
		        </td>
		</tr>
                <tr>
                    <td colspan="2">
                        <div align="center">
                            <input type="submit" value="保存" />
                            <input type="button" value="返回"
                                onclick="window.location='<ls:url address="/admin/category/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
    </body>
</html>

