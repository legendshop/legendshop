<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
  <table  style="width: 100%" class="${tableclass}" id="col1">
         	<tr align="center">
         		<td colspan="4"><b>出价记录</b></td>
         	</tr>
         	<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
         	<tr align="center">
         		<td>时间</td>
         		<td>出价人</td>
         		<td>价格</td>
         		<td>状态</td>
         	</tr>
         	<c:forEach items="${list}" var="bid" varStatus="status">
         		<tr align="center">
         			<td><fmt:formatDate value="${bid.bitTime}"  pattern="yyyy-MM-dd HH:mm:ss"/></td>
         			<td>
         			<c:choose>
         				<c:when test="${empty bid.nickName}">****${fn:substring(bid.userName,fn:length(bid.userName)-2,-1)}</c:when>
         				<c:otherwise>****${fn:substring(bid.nickName,fn:length(bid.nickName)-2,-1)}</c:otherwise>
         			</c:choose>
         			</td>
         			<td>${bid.price}</td>
         			<td>
         			<c:choose>
         				<c:when test="${status.first}"><font color="red">领先</font></c:when>
         				<c:otherwise>出局</c:otherwise>
         			</c:choose>
         			</td>
         		</tr>
         	</c:forEach>
         	<c:if test="${empty list}">
         		<tr align="center">
         			<td colspan="4"><b>暂无出价记录</b></td>
         		</tr>
         	</c:if>
 </table>
 <div class="clear"></div>
<c:if test="${not empty toolBar}">
		<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
	</c:if>
  