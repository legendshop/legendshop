<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<style type="text/css">
		*{margin:0;padding:0;}
		.skulist{border:1px solid #ddd; border-width:1px 0px 0px 1px;width: 100%;}
		.skulist tr td{border:1px solid #ddd; border-width:0px 1px 1px 0px;font-size:13px;margin:0px;}
		.skulist thead tr{background-color: #F9F9F9;}
		.skulist thead tr td{padding:10px 0px;text-align: center;color:#333;}
		.skulist tbody tr td{padding:5px 0px;color:#666;}
		.skulist tbody tr td{padding:5px 0px;color:#666;}
		.skulist tr td .sku-name{
			display:inline-block;
			width:200px;
			margin-top: 5px;
			overflow: hidden;
			white-space:nowrap;
			text-overflow: ellipsis;
		}
		.skulist tr td input[type='button']{
			padding:2px 5px;
			border-radius:2px;
			background-color:#fff;
			border:1px solid #ddd;
			color:#666;
			cursor: pointer;
		}
		.skulist tr td input[type='button']:hover{
			background-color:#0E90D2;
			color:#fff;
			border:1px solid #0E90D2;
		}
	</style>
				<table class="skulist">
					<c:if test="${not empty list}">
						<thead>
							<tr>
								<td>sku名称</td>
								<td>属性</td>
								<td>sku价格</td>
								<td>sku库存数量</td>
								<td>操作</td>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${requestScope.list}" var="sku" varStatus="status">
								<tr>
									<td><span class="sku-name">${sku.name}</span></td>
									<td align="center">${sku.cnProperties}</td>
									<td align="center">${sku.price}</td>
									<td align="center">${sku.actualStocks}</td>
									<td align="center">
										<c:choose>
											<c:when test="${ sku.actualStocks le 0}">
												<font color="red"><b>库存不足</b></font>
											</c:when>
											<c:otherwise>
												<input type="button" value="选择" onclick="parent.addProd('${sku.prodId}','${sku.skuId}','${sku.name}','${sku.price}','${sku.cnProperties }');"/>
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</c:if>
				</table>
