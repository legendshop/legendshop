<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
    <%
        Integer offset = (Integer) request.getAttribute("offset");
    %>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>会员管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
	    <table class="${tableclass} title-border" style="width: 100%">
			<tr>
				<th class="title-border">用户管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/system/userDetail/query"/>">用户信息管理</a>
               		&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">基本资料</span>
               	</th>
			</tr>
		</table>
	   	<div class="user_list">
	        <div class="seller_list_title">
	          <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
	            <li  ><i></i><a href="<ls:url address="/admin/userinfo/userDetail/${userId}"/>">基本资料</a></li>
	            <li ><i></i><a href="<ls:url address="/admin/userinfo/userOrderInfo/${userName}?userId=${userId}"/>">会员订单</a></li>
	            <li ><i></i><a href="<ls:url address="/admin/userinfo/expensesRecord/${userId}?userName=${userName}" />">会员消费记录</a></li>
	           <li ><i></i><a href="<ls:url address="/admin/userinfo/userVisitLog/${userName}?userId=${userId}"/>">会员浏览历史</a></li>
	           <li ><i></i><a href="<ls:url address="/admin/userinfo/userProdComm/${userId}?userName=${userName}"/>">会员商品评论</a></li>
	           <li class=" am-active"><i></i><a href="">会员积分明细</a></li>
			     <ls:plugin pluginId="predeposit">
			        <li ><i></i><a href="<ls:url address="/admin/preDeposit/userPdCashLog/${userName}?userId=${userId}"/>">会员预存款明细</a></li>
			     </ls:plugin>
			     <li ><i></i><a href="<ls:url address="/admin/userinfo/userProdCons/${userName}?userId=${userId}"/>">会员商品咨询</a></li>
	          </ul>
	        </div>
      </div>
    <form:form  action="${contextPath}/admin/userIntegral/userIntegralDetail/${userId}?userName=${userName}" id="form1" method="post">
	    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
    </form:form>
    <div align="center" class="order-content-list" style="margin-top: 20px;">
        <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
    <display:table name="list" requestURI="/admin/userIntegral/userIntegralDetail/${userId}?userName=${userName}" id="item" sort="external" export="false" class="${tableclass}" style="width:100%">
      <display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
        <display:column title="用户名称" property="userName" ></display:column>
         <display:column title="积分" property="integralNum" ></display:column>
         <display:column title="操作类型">
         	<c:choose>
         		<c:when test="${item.logType eq 0}">系统</c:when>
         		<c:when test="${item.logType eq 1}">注册</c:when>
         		<c:when test="${item.logType eq 2}">登录</c:when>
         		<c:when test="${item.logType eq 3}">充值</c:when>
         		<c:when test="${item.logType eq 4}">订单兑换</c:when>
         	</c:choose>
         </display:column>
          <display:column title="操作描述" property="logDesc" ></display:column>
          <display:column title="添加时间" property="addTime"  format="{0,date,yyyy-MM-dd HH:mm:ss}" sortable="true"  sortName="add_time"></display:column>
    </display:table>
    <c:if test="${empty list}">
         	<div style="background:#f9f9f9;margin-top:20px;padding:20px;border:1px solid #ddd;height:60px;width:99%;">该会员没有积分哦~</div>
         </c:if> 
        <div class="clearfix" style="margin-bottom: 60px;">
       		<div class="fr">
       			 <div class="page">
	       			 <div class="p-wrap">
	           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	    			 </div>
       			 </div>
       		</div>
       	</div>
    </div>
   </div>
   </div>
   </body>
   <script language="JavaScript" type="text/javascript">
 function pager(curPageNO){
            document.getElementById("curPageNO").value=curPageNO;
            document.getElementById("form1").submit();
        }
        
      $(document).ready(function(){
          		$("button[name='statusImg']").click(function(event){
	          		$this = $(this);
	          		initStatus($this.attr("itemId"), $this.attr("itemName"),  $this.attr("status"), "${contextPath}/admin/brand/updatestatus/", $this,"${contextPath}");
	       			 }
	       		);
           highlightTableRows("item");
          });
</script>
</html>
