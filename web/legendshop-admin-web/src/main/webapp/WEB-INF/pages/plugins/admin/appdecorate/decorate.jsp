<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<head>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>移动装修 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<style>
.table {
    margin-bottom: 0px !important;
}
</style>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
	    <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
		<table class="${tableclass} title-border" style="width: 100%">
	    <tr>
	        <th class="title-border">app&nbsp;＞&nbsp;首页配置&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">首页模板</span></th>
	    </tr>
    </table>
	<div class="page_body container-fluid">
		<div class="row">
			<div class="col-lg-20 col-md-19 col-sm-18 main">
				<div class="main_cont" style="height: 851px;">
					<!-- <ol class="breadcrumb Noprint">
						<li>APP</li>
						<li>首页配置</li>
						<li class="active">首页模板</li>
					</ol> -->

					<div class="common_data" style="padding-left:20px;">
						<div class="layout_box">
							<div class="layout_head"></div>
							
							 <!-- layoutCont  通过VUE 绑定元素  -->
							<div class="layout_cont ui-sortable" id="layoutCont">
                                
                                
                            <!-- 轮播区域  -->
                              <div id="sliders" class="cont_box ui-state-disabled">
                               <%--  <div class="top_line">
                                    <img alt="" src="${contextPath}/resources/templets/amaze/images/appdecorate/top_area.png" />
                                </div> --%>

                                <swipe class="swiper-container app_top_banner">
                                    <swipe-item v-for="e in sliders" class="swiper-slide">
                                        <img alt="" v-bind:src="photoPath+e.img" />
                                    </swipe-item>
                                </swipe>

                                <div class="edit_btns">
                                    <a href="javascript:;" class="edit">编辑</a>
                                </div>

                                <div class="edit_area">
                                    <s></s>
                                    <a href="javascript:;" class="close">×</a>
                                    <div class="edit_form">
                                        <a href="javascript:;" class="add_img" v-on:click="addImage($index, 'sliders')">[+]添加一个图片</a>
                                        <ul class="img_list">
                                            <li v-bind:class="{'item_error': e.error}" v-for="e in sliders">
                                                <div class="img_item">
                                                    <span class="num">{{$index + 1}}</span>
                                                    <img alt="" v-bind:src="photoPath+e.img">
                                                    <a href="javascript:;" class="img_edit" v-on:click="chooseImage('sliders['+ $index +']','768*400')">重新上传</a>
                                                    <div class="img_link">
                                                        <div>
                                                            <span>链接类型：</span>
                                                            <div class="app-select-bar">
                                                                <div v-if="e.action != 'Theme1'">
                                                                    <strong v-if="!e.action || e.action == '' || !e.actionParam || !e.actionParam.searchParam " class="app-selected-text">请选择</strong>
                                                                </div>
                                                                <strong v-if="e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.cates" class="app-selected-text">商品分类</strong>
                                                                <strong v-if="e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.brands" class="app-selected-text">商品品牌</strong>
                                                                <strong v-if="e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.goods" class="app-selected-text">单个商品</strong>
                                                                <%--<strong v-if="e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.themes" class="app-selected-text">专题</strong>--%>
                                                                
                                                                <ul class="app-select-box">
                                                                    <li v-bind:class="{'selected': !e.action || e.action == '' || !e.actionParam || !e.actionParam.searchParam }">
                                                                        <a href="javascript:;" onClick="chooseAction(0, 'sliders['+ {{$index}} + ']')">请选择</a>
                                                                    </li>
                                                                    <li v-bind:class="{'selected': e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.cates}">
                                                                        <a href="javascript:;" onClick="chooseAction(1, 'sliders['+ {{$index}} + ']')">商品分类</a>
                                                                    </li>
                                                                    <li v-bind:class="{'selected': e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.brands}">
                                                                        <a href="javascript:;" onClick="chooseAction(2, 'sliders['+ {{$index}} + ']')">商品品牌</a>
                                                                    </li>
                                                                    <li v-bind:class="{'selected': e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.goods}">
                                                                        <a href="javascript:;" onClick="chooseAction(3, 'sliders['+ {{$index}} + ']')">单个商品</a>
                                                                    </li>
                                                                    <%--<li v-bind:class="{'selected': e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.themes}">--%>
                                                                        <%--<a href="javascript:;" onClick="chooseAction(4, 'sliders['+ {{$index}} + ']')">专题</a>--%>
                                                                    <%--</li>--%>
                                                                   
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <p>
>
                                                            <span>链接内容：</span>
                                                            <template v-if="e.action == 'GoodsDetail' &&  e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.goods ">
                                                                
                                                                    <span>
                                                                        <font color='blue' v-if="e.actionParam.searchParam.goods.goodsInfoname.length > 10" title="{{ e.actionParam.searchParam.goods.goodsInfoname }}">
                                                                            {{ e.actionParam.searchParam.goods.goodsInfoname.substring(0,10) }}...
                                                                        </font>
                                                                        <font color='blue' v-else title="{{ e.actionParam.searchParam.goods.goodsInfoname }}">
                                                                            {{ e.actionParam.searchParam.goods.goodsInfoname }}
                                                                        </font>
                                                                    </span>
                                                            </template>
                                                            <template v-if="e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.cates  ">
                                                                <span><font color='blue'>{{e.actionParam.searchParam.cates.catName}}</font></span>
                                                            </template>
                                                            <template v-if="e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.brands ">
                                                                <span><font color='blue'>{{e.actionParam.searchParam.brands.brandName}}</font></span>
                                                            </template>
                                                            <template v-if="e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.themes ">
                                                                <span><font color='blue'>{{e.actionParam.searchParam.themes.themeName}}</font></span>
                                                            </template>
                                                        </p>
                                                        <span style="position: absolute;top:5px;margin-left:175px; font-weight:500;width: 150px">建议尺寸（768*386px）</span>
                                                    </div>
                                                </div>
                                                <a class="delete_item" href="javascript:;" v-on:click="deleteImg($parent.$index, $index, 'sliders')">×</a>
                                            </li>

                                        </ul>
                                        <a href="javascript:;" class="btn btn_green" onClick="handleSubmit(this, 'sliders')">保存</a>
                                        
                                    </div>
                                </div>
                            </div>
                          <!-- 轮播区域  -->
                          
                            <!-- 一级广告栏 -->
                               <div class="cont_box ui-state-disabled">
                                <div class="img_recommend row">

                                    <div v-for="e in adverts" class="col-12">
                                        <a href="javascript:;">
                                            <img alt="" v-bind:src="photoPath+e.img" height="100"/>
                                        </a>
                                    </div>

                                </div>
                                <div class="edit_btns">
                                    <a href="javascript:;" class="edit">编辑</a>
                                </div>
                                <div class="edit_area">
                                    <s></s>
                                    <a href="javascript:;" class="close">×</a>
                                    <div class="edit_form">
                                        <a href="javascript:;" class="add_img" v-on:click="addImage($index, 'adverts')">[+]添加一个图片</a>
                                        <ul class="img_list">

                                            <li v-bind:class="{'item_error': e.error}" v-for="e in adverts">
                                                <div class="img_item">
                                                    <span class="num">{{$index + 1}}</span>
                                                    <img alt="" v-bind:src="photoPath+e.img" />
                                                    <a href="javascript:;" class="img_edit" v-on:click="chooseImage('adverts['+ $index +']','369*166')">重新上传</a>
                                                    <div class="img_link">
                                                        <div>
                                                            <span>链接类型：</span>
                                                            <div class="app-select-bar">
                                                                <strong v-if="!e.action || e.action == '' || !e.actionParam || !e.actionParam.searchParam" class="app-selected-text">请选择</strong>
                                                                <strong v-if="e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.cates" class="app-selected-text">商品分类</strong>
                                                                <strong v-if="e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.brands" class="app-selected-text">商品品牌</strong>
                                                                <strong v-if="e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.goods" class="app-selected-text">单个商品</strong>
                                                                <%--<strong v-if="e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.themes" class="app-selected-text">专题</strong>--%>
                                                                <ul class="app-select-box">
                                                                    <li v-bind:class="{'selected': !e.action || e.action == '' || !e.actionParam || !e.actionParam.searchParam }">
                                                                        <a href="javascript:;" onClick="chooseAction(0, 'adverts['+ {{$index}} + ']')">请选择</a>
                                                                    </li>
                                                                    <li v-bind:class="{'selected': e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.cates }">
                                                                        <a href="javascript:;" onClick="chooseAction(1, 'adverts['+ {{$index}} + ']')">商品分类</a>
                                                                    </li>
                                                                    <li v-bind:class="{'selected': e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.brands}">
                                                                        <a href="javascript:;" onClick="chooseAction(2, 'adverts['+ {{$index}} + ']')">商品品牌</a>
                                                                    </li>
                                                                    <li v-bind:class="{'selected': e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.goods }">
                                                                        <a href="javascript:;" onClick="chooseAction(3, 'adverts['+ {{$index}} + ']')">单个商品</a>
                                                                    </li>
                                                                    <%--<li v-bind:class="{'selected': e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.themes }">--%>
                                                                        <%--<a href="javascript:;" onClick="chooseAction(4, 'adverts['+ {{$index}} + ']')">专题</a>--%>
                                                                    <%--</li>--%>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <p>
                                                            <span>链接内容：</span>
                                                            <template v-if="e.action == 'GoodsDetail' &&  e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.goods ">
                                                                
                                                                    <span>
                                                                        <font color='blue' v-if="e.actionParam.searchParam.goods.goodsInfoname.length > 10" title="{{ e.actionParam.searchParam.goods.goodsInfoname }}">
                                                                            {{ e.actionParam.searchParam.goods.goodsInfoname.substring(0,10) }}...
                                                                        </font>
                                                                        <font color='blue' v-else title="{{ e.actionParam.searchParam.goods.goodsInfoname }}">
                                                                            {{ e.actionParam.searchParam.goods.goodsInfoname }}
                                                                        </font>
                                                                    </span>
                                                            </template>
                                                            <template v-if="e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.cates  ">
                                                                <span><font color='blue'>{{e.actionParam.searchParam.cates.catName}}</font></span>
                                                            </template>
                                                            <template v-if="e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.brands ">
                                                                <span><font color='blue'>{{e.actionParam.searchParam.brands.brandName}}</font></span>
                                                            </template>
                                                            <template v-if="e.actionParam && e.actionParam.searchParam && e.actionParam.searchParam.themes ">
                                                                <span><font color='blue'>{{e.actionParam.searchParam.themes.themeName}}</font></span>
                                                            </template>
                                                        </p>
                                                        <span style="position: absolute;top:5px;margin-left:175px; font-weight:500;width: 150px">建议尺寸（369*166px）</span>
                                                    </div>
                                                </div>
                                                <a class="delete_item" href="javascript:;" v-on:click="deleteImg($parent.$index, $index, 'adverts')">×</a>
                                            </li>

                                        </ul>
                                        <div class="img_sample">
                                            <a href="javascript:;" class="show_sample">查看图片广告示例 <i class="icon-angle-right"></i></a>
                                            <div style="display:none">
                                                <img alt="" src="${contextPath}/resources/templets/amaze/images/appdecorate/img_type1_sample.jpg">
                                            </div>
                                        </div>
                                        <a href="javascript:;" class="btn btn_green" onClick="handleSubmit(this, 'adverts')">保存</a>
                                    </div>
                                </div>
                             </div>
                            <!-- 一级广告栏 -->
                                 
						
						   <!-- 楼层 -->
						    <template v-for="e in floors">
						        
						        <!-- 抢购 -->
						       <jsp:include page="decorate_groupbuy.jsp" ></jsp:include> 
                                <!-- 抢购 -->
                                
                                <!-- 楼层模版 -->
                                <jsp:include page="decorate_template1.jsp" ></jsp:include>
                                <jsp:include page="decorate_template2.jsp" ></jsp:include>
                                <jsp:include page="decorate_template3.jsp" ></jsp:include>
                                <jsp:include page="decorate_template4.jsp" ></jsp:include>
                                <jsp:include page="decorate_template5.jsp" ></jsp:include>
                                <jsp:include page="decorate_template6.jsp" ></jsp:include>
                                <jsp:include page="decorate_template7.jsp" ></jsp:include>
                                <jsp:include page="decorate_template8.jsp" ></jsp:include>
                                <!-- 楼层模版 -->
						        
						    </template>
						   <!-- 楼层 -->
						   
						  
								 <!-- 导航 -->
                            <div class="ad_blocks clearfix">
                               <%--  <div class="blocks_item">
                                    <input type="button" class="btn btn_green add_box type0" v-on:click="addRushGroupBuy()" value="抢购/团购"/>
                                    <img src="${contextPath}/resources/templets/amaze/images/appdecorate/rush_group_buy.jpg" alt="">
                                </div> --%>
                                <div class="blocks_item">
                                    <input type="button" class="btn btn_green add_box type1" v-on:click="addTemplate01()" value="楼层(类型一)"/>
                                    <img src="${contextPath}/resources/templets/amaze/images/appdecorate/template01.jpg" alt="">
                                </div>
                                <div class="blocks_item">
                                    <input type="button" class="btn btn_green add_box type2" v-on:click="addTemplate02()" value="楼层(类型二)"/>
                                    <img src="${contextPath}/resources/templets/amaze/images/appdecorate/template02.jpg" alt="">
                                </div>
                                <div class="blocks_item">
                                    <input type="button" class="btn btn_green add_box type3" v-on:click="addTemplate03()" value="楼层(类型三)"/>
                                    <img src="${contextPath}/resources/templets/amaze/images/appdecorate/template03.jpg" alt="">
                                </div>
                                <div class="blocks_item">
                                    <input type="button" class="btn btn_green add_box type4" v-on:click="addTemplate04()" value="楼层(类型四)"/>
                                    <img src="${contextPath}/resources/templets/amaze/images/appdecorate/template04.jpg" alt="">
                                </div>
                                <div class="blocks_item">
                                    <input type="button" class="btn btn_green add_box type5" v-on:click="addTemplate05()" value="楼层(类型五)"/>
                                    <img src="${contextPath}/resources/templets/amaze/images/appdecorate/template05.jpg" alt="">
                                </div>
                                
                                <div class="blocks_item">
                                    <input type="button" class="btn btn_green add_box type6" v-on:click="addTemplate06()" value="楼层(类型六)"/>
                                    <img src="${contextPath}/resources/templets/amaze/images/appdecorate/template06.jpg" alt="">
                                </div>
                                
                                  <div class="blocks_item">
                                    <input type="button" class="btn btn_green add_box type6" v-on:click="addTemplate07()" value="楼层(类型七)"/>
                                    <img src="${contextPath}/resources/templets/amaze/images/appdecorate/template07.jpg" alt="">
                                </div>
                                <div class="blocks_item">
                                    <input type="button" class="btn btn_green add_box type8" v-on:click="addTemplate08()" value="楼层(类型八)"/>
                                    <%-- <img src="${contextPath}/resources/templets/amaze/images/appdecorate/template08.jpg" alt=""> --%>
                                </div>
                              </div>
                              <!-- 导航 -->
								
							</div>

							<div class="save_template">
								<input type="hidden" name="appData"> 
								<input type="button" id="saveBtn" class="btn btn_green" value="保存配置">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
<script type="text/javascript">
		var contextPath="${contextPath}";
        var photoPath="<ls:photo item='' />";
        //console.log("photoPath=========="+photoPath);
        var appdateStr=JSON.stringify(${decotateData});
        var appData=  JSON.parse(appdateStr);
        //console.log('appData--->', JSON.stringify(appData));
        var template08Index="";
        var editor;
</script>
<link href="${contextPath}/resources/templets/amaze/css/appdecorate/bootstrap.min.css" rel="stylesheet">
<link href="${contextPath}/resources/templets/amaze/css/appdecorate/vue-swipe.css" rel="stylesheet">
<link href="${contextPath}/resources/templets/amaze/css/appdecorate/iconfont.css" rel="stylesheet">
<link href="${contextPath}/resources/templets/amaze/css/appdecorate/decorate.css" rel="stylesheet">
<link href="${contextPath}/resources/templets/amaze/css/appdecorate/style.css" rel="stylesheet">
<link href="${contextPath}/resources/templets/amaze/css/appdecorate/app_layout.m.css" rel="stylesheet">
<script type="text/javascript"  src="${contextPath}/resources/templets/amaze/js/appdecorate/bootstrap.min.js"></script>
<script src="${contextPath}/resources/templets/amaze/js/appdecorate/jquery.qrcode.min.js" type="text/javascript"></script>
<script src="${contextPath}/resources/templets/amaze/js/appdecorate/jquery.validate.js" type="text/javascript"></script>
<script src="${contextPath}/resources/templets/amaze/js/appdecorate/jquery.cookie.js" type="text/javascript"></script>
<script src="${contextPath}/resources/templets/amaze/js/appdecorate/appdecorate.js" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
<script type="text/javascript"  src="${contextPath}/resources/templets/amaze/js/appdecorate/vue.js"></script>
<script type="text/javascript"  src="${contextPath}/resources/templets/amaze/js/appdecorate/vue-swipe.js"></script>
<script type="text/javascript"  src="${contextPath}/resources/templets/amaze/js/appdecorate/jquery-ui.js"></script>
<script type="text/javascript"  src="${contextPath}/resources/templets/amaze/js/appdecorate/app.js"></script>
</body>
</html>
