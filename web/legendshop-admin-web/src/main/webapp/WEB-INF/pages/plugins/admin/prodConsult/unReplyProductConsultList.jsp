<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
            <ul class="am-comments-list admin-content-comment">
              <c:choose>
              		<c:when test="${not empty productConsultList}">
              		              <c:forEach items="${productConsultList}"  var="productConsult">
					                 <li class="am-comment">
						                <a href="#">
						                 <c:choose>
											<c:when test="${not empty productConsult.portraitPic }">
						                		<img src="<ls:images item="${productConsult.portraitPic}" scale="3" />"  class="am-comment-avatar"   width="48" height="48">
							                </c:when>
											<c:otherwise>
												<img alt="用户头像" src="${contextPath}/resources/templets/amaze/images/no-img_mid_.jpg" class="am-comment-avatar"   width="48" height="48">
											</c:otherwise>
										</c:choose> 
						                </a>
						                <div class="am-comment-main">
						                  <header class="am-comment-hd">
						                    <div class="am-comment-meta"><a href="#" class="am-comment-author">${productConsult.nickName}</a> 评论于<fmt:formatDate value="${productConsult.recDate}" type="both" dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss"/></div>
						                  </header>
						                  <div class="am-comment-bd"><p><a href="${contextPath}/admin/productConsult/load/${productConsult.consId}" class="am-comment-author" target="_blank"> [回复]</a> ${productConsult.content}</p>
						                  </div>
						                </div>
						              </li>
					            </c:forEach>
              		</c:when>
              		<c:otherwise>
					               <li class="am-comment"><span class="am-icon-exclamation-triangle"></span> 暂无商品评论需要处理</td></li>
              		</c:otherwise>
              </c:choose>
            </ul>
			<ul class="am-pagination am-fr ">
					<a href="${contextPath}/admin/productConsult/query" class="am-comment-author" target="_blank"  style="font-weight: normal;">咨询列表</a>
			</ul>