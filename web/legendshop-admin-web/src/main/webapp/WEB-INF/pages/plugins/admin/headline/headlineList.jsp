<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss" var="nowDate" />

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>头条新闻管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">新闻管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">头条新闻管理</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/headlineAdmin/query"
				id="form1" method="get">
					<div class="criteria-div">
						<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
						<span class="item">
							头条名称 ：
							<input type="text" name="title" maxlength="50" value="${headline.title}" class="${inputclass}" placeholder="请输入头条名搜索" /> 
							<input type="button" onclick="search()" value="搜索" class="criteria-btn" /> 
							<input type="button" value="新建头条" class="criteria-btn" onclick='window.location="${contextPath}/admin/headlineAdmin/load"' />
						</span>
					</div>
			</form:form>

			<div align="center" class="order-content-list">
				<%--         <%@ include file="/WEB-INF/pages/common/messages.jsp"%> --%>
				<display:table name="list" requestURI="/admin/headlineAdmin/query"
					id="item" export="false" class="${tableclass}" style="width:100%">
					<display:column title="序号" class="orderwidth"><%=offset++%></display:column>
					<display:column title="头条名称" style="width:35%">
						<div class="order_img_name">
							${item.title}
						</div>
					</display:column>
					<display:column title="图片">
						<img src="<ls:photo item='${item.pic}'/>" height="54" />
					</display:column>
					<display:column title="建立时间" property="addTime"
						format="{0,date,yyyy-MM-dd HH:mm}" sortable="true"></display:column>
					<display:column title="顺序" property="seq"></display:column>
					<display:column title="操作" media="html" style="width:150px">
						<%-- <div class="am-btn-toolbar">
							<div class="am-btn-group am-btn-group-xs">
								<button
									class="am-btn am-btn-default am-btn-xs am-text-secondary"
									onclick="updateUI(${item.id})">
									<span class="am-icon-pencil-square-o"></span> 编辑
								</button>
								<button
									class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only"
									onclick="deleteById(${item.id})">
									<span class="am-icon-trash-o"></span> 删除
								</button>
							</div>
						</div> --%>
							<div class="table-btn-group">
								<button class="tab-btn" onclick="updateUI(${item.id})">编辑</button>
								<span class="btn-line">|</span>
								<button class="tab-btn" onclick="deleteById(${item.id})">删除</button>
							</div>
					</display:column>
				</display:table>
				<!-- 分页条 -->
		     	<div class="clearfix" style="margin-bottom: 60px;">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
			</div>

		</div>
	</div>
</body>

<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.js'/>"></script>
<script type="text/javascript">
     function pager(curPageNO){
            document.getElementById("curPageNO").value=curPageNO;
            document.getElementById("form1").submit();
        }
        
    function deleteById(id) {
		layer.confirm("  确定删除 ?", {icon:3, title:'提示'}, function(){
	      	window.location="${contextPath}/admin/headlineAdmin/delete/"+id;
	    });
    }
    
	   function updateUI(id){
	   		window.location = "${contextPath}/admin/headlineAdmin/load/"+id;
	   }
	   
	   function skip(){
	   		window.location.reload(); 
	   }
	   
	   function search(){
		  	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		  	$("#form1")[0].submit();
		}
	   
	</script>
</html>
