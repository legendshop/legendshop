<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<html>
    <head>
        <title>创建</title>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
</head>
    <body>
        <form:form  action="${contextPath}/admin/userFeedBack/save" method="post" id="form1">
            <input id="id" name="id" value="${userFeedBack.id}" type="hidden">
            <div align="center">
            <table border="0" align="center" class="${tableclass}" id="col1">
                <thead>
                    <tr class="sortable">
                        <th colspan="2">
                            <div align="center">
                                	创建
                            </div>
                        </th>
                    </tr>
                </thead>
                
		<tr>
		        <td>
		          	<div align="center">用户ID: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="userId" id="userId" value="${userFeedBack.userId}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">反馈人名字: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="name" id="name" value="${userFeedBack.name}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">手机号码: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="mobile" id="mobile" value="${userFeedBack.mobile}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">发起反馈的IP地址: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="ip" id="ip" value="${userFeedBack.ip}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">标题: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="title" id="title" value="${userFeedBack.title}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">内容: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="content" id="content" value="${userFeedBack.content}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">记录时间: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="recDate" id="recDate" value="${userFeedBack.recDate}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">是否已经阅读: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="status" id="status" value="${userFeedBack.status}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">回复的管理员ID: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="respMgntId" id="respMgntId" value="${userFeedBack.respMgntId}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">处理意见: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="respContent" id="respContent" value="${userFeedBack.respContent}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">处理时间: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="respDate" id="respDate" value="${userFeedBack.respDate}" />
		        </td>
		</tr>
                <tr>
                    <td colspan="2">
                        <div align="center">
                            <input type="submit" value="保存" />
                            <input type="button" value="返回"
                                onclick="window.location='<ls:url address="/admin/userFeedBack/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
<script type='text/javascript' src="<ls:templateResource item='/common/js/jquery.validate.js'/>" /></script>
<script language="javascript">
		    $.validator.setDefaults({
	 });

    $(document).ready(function() {
    jQuery("#form1").validate({
            rules: {
            banner: {
                required: true,
                minlength: 5
            },
            url: "required"
        },
        messages: {
            banner: {
                required: "Please enter banner",
                minlength: "banner must consist of at least 5 characters"
            },
            url: {
                required: "Please provide a password"
            }
        }
    });
 
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
});
</script>
    </body>
</html>

