<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>

<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>


<%-- <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
&nbsp;名称:&nbsp;<input class="${inputclass}" type="text" id="propName" name="propName" maxlength="50" value="${productProperty.propName}" />&nbsp;
&nbsp;别名:&nbsp;<input class="${inputclass}" type="text" id="memo" name="memo" maxlength="50" value="${productProperty.memo}" />&nbsp;
<input class="${btnclass}" type="button" value="搜索" onclick="javascript:searchContent()"/>
<input class="${btnclass}" type="button" value="创建属性" onclick='window.location="<ls:url address='/admin/productProperty/new/${productProperty.isRuleAttributes}'/>"'/>
<input class="${btnclass}" type="button" value="类似属性导入" onclick="importSimilarProp();"/>
<br> --%>
<div class="criteria-div">
     <span class="item">
		<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
		名称:&nbsp;
		<input class="${inputclass}" type="text" id="propName" name="propName" maxlength="50" value="${productProperty.propName}" />
	</span>
    <span class="item">
		别名:&nbsp;
		<input class="${inputclass}" type="text" id="memo" name="memo" maxlength="50" value="${productProperty.memo}" />&nbsp;
	</span>
	<span class="item">
		<input class="${btnclass}" type="button" value="搜索" onclick="javascript:searchContent();" />
		<%-- <input class="${btnclass}" type="button" value="搜索" onclick="search()"/> --%>
		<input class="${btnclass}" type="button" value="创建属性" onclick='window.location="<ls:url address='/admin/productProperty/new/${productProperty.isRuleAttributes}'/>"' />
		<input class="${btnclass}" type="button" value="类似属性导入" onclick="importSimilarProp();" />
	</span>
</div>	

<div align="center"  class="order-content-list">
    <%@ include file="/WEB-INF/pages/common/messages.jsp" %>
    <display:table name="list" requestURI="/admin/productProperty/query" id="item" export="false" class="${tableclass}" style="width:100%">
        <display:column title="名称" property="propName" style="width:20%"></display:column>
        <display:column title="别名" property="memo" style="width:20%"></display:column>
        <display:column title="类型" style="width:20%">
            <ls:optionGroup type="label" required="true" cache="true" beanName="PROPERTY_TYPE" selectedValue="${item.type}"/>
        </display:column>

        <display:column title="值" style="width:20%">
            <c:forEach var="value" items="${item.valueList}">${value.name};</c:forEach>
        </display:column>

        <display:column title="操作" media="html" style="width:20%">
            <div class="table-btn-group">
					<button class="tab-btn" onclick="window.location='${contextPath}/admin/productProperty/load/${item.propId}'">修改</button>
					<span class="btn-line">|</span>
					<button class="tab-btn" onclick="deleteById('${item.propId}','${item.propName}');">删除</button>
			</div>
        </display:column>
    </display:table>
    <div class="clearfix">
		<div class="fr">
			 <div class="page">
	 			 <div class="p-wrap">
	     		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
		 		</div>
			 </div>
		</div>
	</div>
</div>
<script language="JavaScript" type="text/javascript">
    function pager(curPageNO) {
        $.post("${contextPath}/admin/productProperty/queryContent/${productProperty.isRuleAttributes}", {"curPageNO": curPageNO, "propName": $("#propName").val(), "memo": $("#memo").val()}, function (data) {
            $("#proppertyContent").html(data);
        });
    }

    function searchContent() {
        $.post("${contextPath}/admin/productProperty/queryContent/${productProperty.isRuleAttributes}", {"curPageNO": 1, "propName": $("#propName").val(), "memo": $("#memo").val()}, function (data) {
            $("#proppertyContent").html(data);
        });
    }
    $(document).ready(function () {
        $("button[name='statusImg']").click(function (event) {
                    $this = $(this);
                    initStatus($this.attr("itemId"), $this.attr("itemName"), $this.attr("status"), "${contextPath}/admin/productProperty/updatestatus/", $this, "${contextPath}");
                }
        );
        highlightTableRows("item");
    });
</script>