<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>

<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>推广员管理 - 后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">推广管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">推广员列表 </span></th>
					</th>
				</tr>
		    </table>
			<form:form id="ListForm" method="post" action="${contextPath}/admin/userCommis/query">
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="1" />
					<span class="item">
		           		用户ID：
		           	<input type="text" class="${inputclass}" name="userName" maxlength="50" value="${userCommis.userName}" placeholder="用户ID"/>
		           	</span>
				  	<span class="item">
		           		手机号码：
		           	<input type="text" class="${inputclass}" name="userMobile" maxlength="50" value="${userCommis.userMobile}" placeholder="手机号码"/>
		           	</span>
				  	<span class="item">
		           		申请时间：
		           	<input type="text" value="<fmt:formatDate value="${userCommis.startApplyTime}" pattern="yyyy-MM-dd" />" readonly="readonly" placeholder="申请时间" class="user_title_txt Wdate ${inputclass}" id="startApplyTime" name="startApplyTime">&nbsp;-
		               <input type="text" readonly="readonly" value="<fmt:formatDate value="${userCommis.endApplyTime}" pattern="yyyy-MM-dd" />" id="endApplyTime" name="endApplyTime" placeholder="申请时间" class="user_title_txt Wdate ${inputclass}">
		            </span>
				  	<span class="item">
		                成为推广员时间：
		           	<input type="text" value="<fmt:formatDate value="${userCommis.startPromoterTime}" pattern="yyyy-MM-dd" />" readonly="readonly" placeholder="成为推广员时间" class="user_title_txt Wdate ${inputclass}" id="startPromoterTime" name="startPromoterTime">
							&nbsp;-
		               <input type="text" readonly="readonly" value="<fmt:formatDate value="${userCommis.endPromoterTime}" pattern="yyyy-MM-dd" />" id="endPromoterTime" name="endPromoterTime" placeholder="成为推广员时间" class="user_title_txt Wdate ${inputclass}">
		           </span>
				   <span class="item">
		              	 状态&nbsp;
		               <select name="promoterSts" id="promoterSts" class="criteria-select">
							<option value="" <c:if test="${empty userCommis.promoterSts }">selected = "selected"</c:if>>全部</option>
							<option value="-2" <c:if test="${userCommis.promoterSts eq -2}">selected = "selected"</c:if>>等待审核</option>
							<option value="1" <c:if test="${userCommis.promoterSts eq 1}">selected = "selected"</c:if>>审核通过</option>
							<option value="-1" <c:if test="${userCommis.promoterSts eq -1}">selected = "selected"</c:if>>已拒绝</option>
						</select>
						<input type="submit" class="${btnclass}" value="查询" />
					</span>
				</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" id="item" export="false" sort="external"
					class="${tableclass}" style="width:100%">
					<display:column title="
						<span>
				           <label class='checkbox-wrapper'>
								<span class='checkbox-item'>
									<input type='checkbox' class='checkbox-input selectAll' onclick='selectAll(this);'/>
									<span class='checkbox-inner' style='margin-left:10px;'></span>
								</span>
						   </label>	
						</span>">
						 <c:choose>
				    		<c:when test="${!(item.promoterSts eq 1&&item.commisSts eq 1)}">
							   <label class="checkbox-wrapper checkbox-wrapper-disabled">
									<span class="checkbox-item">
										<input type="checkbox" name="userCommId" value="${item.userId}" can-off="${item.promoterSts eq 1&&item.commisSts eq 1}" class="checkbox-input"  disabled='disabled'"/>
										<span class="checkbox-inner" style="margin-left:10px;"></span>
									</span>
							   </label>	
				    		</c:when>
				    		<c:otherwise>
				    			<label class="checkbox-wrapper">
									<span class="checkbox-item">
										<input type="checkbox" name="userCommId" value="${item.userId}" can-off="${item.promoterSts eq 1&&item.commisSts eq 1}" class="checkbox-input selectOne" onclick="selectOne(this);"/>
										<span class="checkbox-inner" style="margin-left:10px;"></span>
									</span>
							   </label>	
				    		</c:otherwise>
				    	</c:choose>
					</display:column>
					<display:column title="序号"><%=offset++%></display:column>
					<display:column title="用户ID">
						<a href="${contextPath}/admin/userinfo/userDetail/${item.userId}">${item.userName}</a>
					</display:column>
					<display:column title="用户姓名" value="${item.realName}"></display:column>
					<display:column title="手机号码" value="${item.userMobile}"></display:column>
					<display:column title="直接下级数" property="distCommisCount"></display:column>
					<display:column title="直接下级贡献佣金">
						<fmt:formatNumber value="${item.firstDistCommis}" type="currency" pattern="0.00"></fmt:formatNumber>
					</display:column>
					<display:column title="累计获得佣金">
						<fmt:formatNumber value="${item.totalDistCommis}" type="currency" pattern="0.00"></fmt:formatNumber>
					</display:column>
					<display:column title="申请时间">
						<fmt:formatDate value="${item.promoterApplyTime}" type="both" />
					</display:column>
					<display:column title="成为推广员时间">
						<fmt:formatDate value="${item.promoterTime}" type="both" />
					</display:column>
					<display:column title="状态">
						<c:choose>
							<c:when test="${item.promoterSts eq -2}">等待审核</c:when>
							<c:when test="${item.promoterSts eq 1}">审核通过</c:when>
							<c:when test="${item.promoterSts eq -1}">已拒绝</c:when>
						</c:choose>
					</display:column>

					<display:column title="操作" media="html" style="width: 285px;">
						<div class="table-btn-group">
							<c:choose>
								<c:when test="${item.promoterSts eq -2}">
									<button class="tab-btn" onclick="loadUserCommisAudit('${item.userId}');">
										审核
									</button>
								</c:when>
								<c:when test="${item.promoterSts eq 1}">
									<button class="tab-btn" onclick="window.location.href='${contextPath}/admin/userCommis/load/${item.id}'">
										查看详情
									</button>
									<c:choose>
										<c:when test="${item.commisSts eq 0}">
											<span class="btn-line">|</span>
											<button class="tab-btn" onclick="openCommis('${item.userId}')">
												开启分佣提成
											</button>
										</c:when>
										<c:when test="${item.commisSts eq 1}">
											<span class="btn-line">|</span>
											<button class="tab-btn" onclick="closeCommis('${item.userId}')">
												关闭分佣提成
											</button>
										</c:when>
									</c:choose>
								</c:when>
							</c:choose>
						</div>
					</display:column>
				</display:table>
				<div class="clearfix">
		       		<div class="fl">
			       		<input type="button" value="批量关闭分佣提成" id="batch-off" class="batch-btn">
					</div>
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
	       		</div>
			</div>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">

     laydate.render({
	   elem: '#startApplyTime',
	   calendar: true,
	   theme: 'grid',
	   trigger: 'click'
	  });
	   
	 laydate.render({
	   elem: '#endApplyTime',
	   calendar: true,
	   theme: 'grid',
	   trigger: 'click'
    });
	 
    laydate.render({
	   elem: '#startPromoterTime',
	   calendar: true,
	   theme: 'grid',
	   trigger: 'click'
	 });
	   
	 laydate.render({
	   elem: '#endPromoterTime',
	   calendar: true,
	   theme: 'grid',
	   trigger: 'click'
   });
	 
	 
	$("#checkedAll").on("click",function() {
				if ($(this).is(":checked")) {
					$("input[type=checkbox][name=userCommId][can-off=true]")
							.prop("checked", true);
				} else {
					$("input[type=checkbox][name=userCommId][can-off=true]")
							.removeAttr("checked");
				}
			});

	            $("#batch-off").on("click",function() {
						var checkedProd = $(".selectOne:checked");
						var userIdArr = new Array();
						checkedProd.each(function() {
							var userId = $(this).val();
							userIdArr.push(userId);
						});

						var userIdStr = userIdArr.toString();

						if (userIdStr == "") {
							layer.msg("请选择要关闭的分佣用户。", {icon:0});
							return;
						}

						jQuery.ajax({
									url : "${contextPath}/admin/userCommis/batchCloseCommis/"+ userIdStr,
									type : 'PUT',
									async : true, // 默认为true 异步
									dataType : "json",
									error : function(jqXHR, textStatus,errorThrown) {
										layer.alert("网络异常,请稍后重试！",{icon:2});
									},
									success : function(result) {
										if (result == "OK") {
											layer.msg("批量关闭分佣提成成功。", {icon:1, time:700}, function(){
													window.location.reload();
											});
										} else {
											layer.msg(result, {icon:0});
										}

									}
								});
					});

	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("ListForm").submit();
	}

	/**加载推广员审核页面**/
	function loadUserCommisAudit(userId) {
		var url = "${contextPath}/admin/userCommis/loadAuditInfo/" + userId;
		layer.open({
			  title :"推广员审核",
			  id: "auditInfo",
			  type: 2, 
			  content: [url,'no'], //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['${contextPath}/s/loadProdListPage', 'no']
			  area: ['550px', 'auto'],
			  success: function(layero, index) {
				  layer.iframeAuto(index);
				} 
			}); 
	}

	/** 关闭分佣提成 */
	function closeCommis(userId) {
		layer.confirm("确定关闭该推广员的分佣提成吗？", {icon:0}, function() {
			jQuery.ajax({
				url : "${contextPath}/admin/userCommis/closeCommis/" + userId,
				type : 'post',
				async : true, // 默认为true 异步
				error : function(jqXHR, textStatus, errorThrown) {
					layer.alert("网络异常,请稍后重试！",{icon:2});
				},
				success : function(result) {
					window.location.reload();
				}
			});
		});
	}

	/** 开启分佣提成 */
	function openCommis(userId) {
		layer.confirm("确定开启该推广员的分佣提成吗？", {icon:0}, function() {
			jQuery.ajax({
				url : "${contextPath}/admin/userCommis/openCommis/" + userId,
				type : 'post',
				async : true, // 默认为true 异步
				error : function(jqXHR, textStatus, errorThrown) {
					layer.alert("网络异常,请稍后重试！",{icon:2});
				},
				success : function(result) {
					window.location.reload();
				}
			});
		});
	}
	 $(function () {
		 //禁用“确认重新提交表单”
		 window.history.replaceState(null, null, window.location.href);
	 })
</script>
</html>

