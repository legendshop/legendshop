<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>店铺查看 - 后台管理</title>
    <meta name="description" content="LegendShop 多用户商城系统">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon"/>
    <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="Amaze UI"/>
    <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/templets/amaze/css/viewBigImage.css"/>
</head>
<body>
<jsp:include page="/admin/top"/>
<div class="am-cf admin-main">
    <!-- sidebar start -->
    <jsp:include page="../frame/left.jsp"></jsp:include>
    <!-- sidebar end -->
    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
        <table class="${tableclass} title-border" style="width: 100%">
            <tr>
                <th class="title-border">商城管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/shopDetail/query"/>">店铺管理</a>
                    &nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">店铺编辑</span>
                </th>
            </tr>
        </table>
        <div>
            <div class="seller_list_title">
                <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
                    <li class="am-active"><i></i><a href="javaScript:void(0)">店铺信息</a></li>
                    <c:if test="${shopDetail.type eq 1 && shopDetail.opStatus ne 1}">
                        <li><i></i><a href="<ls:url address="/admin/shopDetail/queryCompanyInfo?shopId=${shopDetail.shopId}&userName=${shopDetail.userName}&status=${shopDetail.status}"/>">公司信息</a></li>
                    </c:if>
                    <li><i></i><a href="<ls:url address="/admin/shopDetail/shopAuditDetail?shopId=${shopDetail.shopId}"/>">审核历史</a></li>
                </ul>
            </div>
        </div>
        <form:form action="${contextPath}/admin/shopDetail/updateShopInfo" style="padding: 0 15px;" method="post" id="form1">
            <input type="hidden" name="shopId" id="shopId" value="${shopDetail.shopId}"/>
            <table style="width: 100%" class="${tableclass} no-border content-table" id="col1">
                <tr>
                    <td colspan="2">
                        <div align="right" style="text-align: left; font-weight: bold;margin-left:1%;">个人信息：</div>
                    </td>
                </tr>
                <tr>
                    <td style="width:200px;">
                        <div align="right">用户名:</div>
                    </td>
                    <td align="left">
                            ${shopDetail.userName == null ? param.userName : shopDetail.userName}
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="right">用户昵称:</div>
                    </td>
                    <td align="left">${userDetail.nickName }</td>
                </tr>
                <tr>
                    <td>
                        <div align="right">用户手机:</div>
                    </td>
                    <td align="left">${userDetail.userMobile }</td>
                </tr>
                <tr>
                    <td>
                        <div align="right">用户邮件:</div>
                    </td>
                    <td align="left">${userDetail.userMail }</td>
                </tr>
                <!-- 联系人信息 -->
                <tr>
                    <td colspan="2">
                        <div align="right" style="text-align: left; font-weight: bold;margin-left:1%;">联系人信息：</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="right"><font color="red">*</font>联系人姓名:</div>
                    </td>
                    <td align="left">
                        <input type="text" id="contactName" name="contactName" value="${shopDetail.contactName}" maxlength="20"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="right"><font color="red">*</font>联系人手机:</div>
                    </td>
                    <td align="left">
                        <input type="text" id="contactMobile" name="contactMobile" value="${shopDetail.contactMobile}" maxlength="20"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="right"><font color="red">*</font>联系人邮箱:</div>
                    </td>
                    <td align="left">
                        <input type="text" id="contactMail" name="contactMail" value="${shopDetail.contactMail}" maxlength="100"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="right">联系人QQ号码:</div>
                    </td>
                    <td align="left">
                        <input type="text" id="contactQQ" name="contactQQ" value="${shopDetail.contactQQ}" onkeyup="value=value.replace(/[\uFF00-\uFFFF]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\uFF00-\uFFFF]/g,''))" maxlength="50" autocomplete="off"/>
                    </td>
                </tr>
                <!-- 联系人信息 -->
                <tr>
                    <td colspan="2">
                        <div align="right" style="text-align: left; font-weight: bold;margin-left:1%;">店铺信息：</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="right">商品是否需要审核:</div>
                    </td>
                    <td align="left">
                    <c:if test="${shopDetail.status!=1}">
		                   <c:if test="${empty shopDetail.prodRequireAudit}">
		                   		   采用平台设置
		                   		 <input type="hidden" name="prodRequireAudit" value=""/>
		                   </c:if> 
		                   <c:if test="${shopDetail.prodRequireAudit==1}">
		                  		    需要审核
		                  		 <input type="hidden" name="prodRequireAudit" value="1"/>
		                   </c:if>	
		                   <c:if test="${shopDetail.prodRequireAudit==0}">
		                  		   无需审核
		                  		 <input type="hidden" name="prodRequireAudit" value="0"/>
		                   </c:if>        
                    </c:if>
                     <c:if test="${shopDetail.status==1}">
                        <select id="prodRequireAudit" name="prodRequireAudit" class="sele" disabled="">
                            <option
                                    <c:if test="${empty shopDetail.prodRequireAudit}">selected</c:if> value="">采用平台设置
                            </option>
                            <option
                                    <c:if test="${shopDetail.prodRequireAudit==1}">selected</c:if> value="1">需要审核
                            </option>
                            <option
                                    <c:if test="${shopDetail.prodRequireAudit==0}">selected</c:if> value="0">无需审核
                            </option>
                        </select>
                            <input id="changeRequireAudit" type="button" value="修改" onclick="change(${shopDetail.shopId})" class="${btnclass}"/>
                        </c:if>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="right"><font color="red">*</font>店铺名称:</div>
                    </td>
                    <td align="left">
                        <input id="siteName" type="text" name="siteName" value="${shopDetail.siteName}" maxlength="50"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="right">店铺地址:</div>
                    </td>
                    <td align="left">
                        <select class="combox" id="provinceid" name="provinceid" requiredTitle="true" childNode="cityid" selectedValue="${shopDetail.provinceid}" retUrl="${contextPath}/common/loadProvinces">
                        </select>
                        <select class="combox" id="cityid" name="cityid" requiredTitle="true" selectedValue="${shopDetail.cityid}" showNone="false" parentValue="${shopDetail.provinceid}" childNode="areaid" retUrl="${contextPath}/common/loadCities/{value}">
                        </select>
                        <select class="combox" id="areaid" name="areaid" requiredTitle="true" selectedValue="${shopDetail.areaid}" showNone="false" parentValue="${shopDetail.cityid}" retUrl="${contextPath}/common/loadAreas/{value}">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="right">店铺地址:</div>
                    </td>
                    <td align="left">
                        <input type="text" id="shopAddr" name="shopAddr" value="${shopDetail.shopAddr}" maxlength="300" style="width:326px;"/>
                    </td>
                </tr>

                <tr>
                    <td>
                        <div align="right">店铺类型:</div>
                    </td>
                    <td align="left">
                        <c:choose>
                            <c:when test="${shopDetail.shopType eq 0}">
                                专营店
                            </c:when>
                            <c:when test="${shopDetail.shopType eq 1}">
                                旗舰店
                            </c:when>
                            <c:when test="${shopDetail.shopType eq 2}">
                                自营店
                            </c:when>
                        </c:choose>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="right">简要描述:</div>
                    </td>
                    <td align="left">
                        <input type="text" value="${shopDetail.briefDesc}" id="briefDesc" name="briefDesc" style="width:326px;"/>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <div align="right">详细描述:</div>
                    </td>
                    <td align="left">
                        <textarea rows="5" cols="50" style="width:326px;" name="detailDesc" id="detailDesc" maxlength="300">${shopDetail.detailDesc}</textarea>
                    </td>
                </tr>
                <c:if test="${shopDetail.recDate!=null}">
                    <tr>
                        <td>
                            <div align="right">修改时间:</div>
                        </td>
                        <td align="left"><fmt:formatDate value="${shopDetail.modifyDate}" pattern="yyyy-MM-dd HH:mm"/></td>
                    </tr>
                </c:if>
                <c:if test="${shopDetail.recDate!=null}">
                    <tr>
                        <td>
                            <div align="right">创建时间:</div>
                        </td>
                        <td align="left"><fmt:formatDate value="${shopDetail.recDate}" pattern="yyyy-MM-dd HH:mm"/></td>
                    </tr>
                </c:if>
                <c:if test="${shopDetail.shopPic != null}">
                    <tr>
                        <td>
                            <div align="right">商城图片:</div>
                        </td>
                        <td align="left">
                            <div class="zoombox">
								<span class="photoBox">
									<div class="loadingBox">
										<span class="loading"></span>
									</div>
									<img src="<ls:photo item='${shopDetail.shopPic}'/>" class="zoom" onclick="zoom_image($(this).parent());" style="max-height: 60px;max-width: 198px;">
								</span>

                                <div class="photoArea" style="display:none;">
                                    <p><img src="about:blank" class="minifier" onclick="zoom_image($(this).parent().parent());"/></p>
                                    <p class="toolBar gc">
                                        <span><a class="green" href="javascript:void(0)" onclick="zoom_image($(this).parent().parent().parent());">收起</a></span>|<span class="view"><a class="green" href="<ls:photo item='${shopDetail.shopPic}'/>" target="_blank">查看原图</a></span>
                                    </p>
                                </div>
                            </div>
                        </td>
                    </tr>
                </c:if>
                <tr>
                    <td colspan="2">
                        <div align="right" style="text-align: left; font-weight: bold;margin-left:1%;">验证信息：</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="right">店铺类型:</div>
                    </td>
                    <td align="left">
                        <c:if test="${shopDetail.type != null}">
                            <ls:optionGroup type="label" required="false" cache="true" beanName="SHOP_TYPE" selectedValue="${shopDetail.type}"/>
                        </c:if>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="right">身份证号码:</div>
                    </td>
                    <td align="left">
                        <c:if test="${shopDetail.idCardNum != null}">
                            ${shopDetail.idCardNum}
                        </c:if>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="right">身份证正面:</div>
                    </td>
                    <td align="left">
                        <c:if test="${shopDetail.idCardPic != null}">
                            <div class="zoombox">
								<span class="photoBox">
									<div class="loadingBox">
										<span class="loading"></span>
									</div>
									<img src="<ls:photo item='${shopDetail.idCardPic}'/>" class="zoom" onclick="zoom_image($(this).parent());" style="max-height: 100px;max-width: 150px;">
								</span>

                                <div class="photoArea" style="display:none;">
                                    <p><img src="about:blank" class="minifier" onclick="zoom_image($(this).parent().parent());"/></p>
                                    <p class="toolBar gc">
                                        <span><a class="green" href="javascript:void(0)" onclick="zoom_image($(this).parent().parent().parent());">收起</a></span>|<span class="view"><a class="green" href="<ls:photo item='${shopDetail.idCardPic}'/>" target="_blank">查看原图</a></span>
                                    </p>
                                </div>
                            </div>
                        </c:if>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="right">身份证反面:</div>
                    </td>
                    <td align="left">
                        <c:if test="${shopDetail.idCardBackPic != null}">
                            <div class="zoombox">
								<span class="photoBox">
									<div class="loadingBox">
										<span class="loading"></span>
									</div>
									<img src="<ls:photo item='${shopDetail.idCardBackPic}'/>" class="zoom" onclick="zoom_image($(this).parent());" style="max-height: 100px;max-width: 150px;">
								</span>

                                <div class="photoArea" style="display:none;">
                                    <p><img src="about:blank" class="minifier" onclick="zoom_image($(this).parent().parent());"/></p>
                                    <p class="toolBar gc">
                                        <span><a class="green" href="javascript:void(0)" onclick="zoom_image($(this).parent().parent().parent());">收起</a></span>|<span class="view"><a class="green" href="<ls:photo item='${shopDetail.idCardBackPic}'/>" target="_blank">查看原图</a></span>
                                    </p>
                                </div>
                            </div>
                        </c:if>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div align="right">审核店铺:</div>
                    </td>
                    <td align="left">
                        <!-- for管理员 -->
                        <select id="status" name="status">
                            <ls:optionGroup type="select" required="true" cache="true" beanName="SHOP_STATUS" selectedValue="${shopDetail.status}"/>
                        </select>&nbsp;注：(当审核为拒绝，用户不能重新填写开店信息)
                    </td>
                <tr>
                    <td>
                        <div align="right">审核意见:</div>
                    </td>
                    <td align="left"><textarea rows="5" cols="50" name="auditOpinion" id="auditOpinion">${shopDetail.auditOpinion}</textarea></td>

                </tr>
                <c:if test="${shopDetail. status eq 0 || shopDetail. status eq 1 || shopDetail. status eq -3}">
                    <tr>
                        <td colspan="2">
                            <div align="right" style="text-align: left; font-weight: bold;margin-left:1%;">商品资料：</div>
                        </td>
                    </tr>
                    </tr>
                    <tr>
                        <td>
                            <div align="right">商品列表:</div>
                        </td>
                        <td align="left">
                            <input id="loadShopProduct" type="button" value="查看店铺商品" onclick="javascript:queryShopProd();" class="${btnclass}"/>
                        </td>
                    </tr>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div align="right" style="text-align: left; font-weight: bold;margin-left:1%;">销售订单：</div>
                        </td>
                    </tr>
                    </tr>
                    <tr>
                        <td>
                            <div align="right">订单列表:</div>
                        </td>
                        <td align="left">
                            <input id="loadShoporder" type="button" value="查看店铺订单" onclick="javascript:window.location.href=' ${contextPath}/admin/order/processing?shopName=${shopDetail.siteName}'" class="${btnclass}"/>
                        </td>
                    </tr>
                </c:if>
                <tr>
                    <td></td>
                    <td align="left">
                        <div>
                            <input type="submit" value="保存" class="${btnclass}" onclick ="return checkSiteNameExist();"/>
                            <input type="button" value="返回" class="${btnclass}" onclick="window.history.length !== 1 ? window.history.back() : window.close();"/>
                        </div>
                    </td>
                </tr>
            </table>
        </form:form>
        <table class="${tableclass}" style="width: 100%; border: 0px; margin-top: 10px;">
            <tr>
                <td align="left" style="border: 0;background: #f9f9f9;text-align: left;">店铺审核说明：<br>
                    1. 正常营业（上线）：审核通过，店铺可以上线<br>
                    2. 拒绝：当审核为拒绝，用户不能重新填写开店信息<br>
                    3. 公司信息错误：企业入驻，开店申请信息填写有误<br>
                    4. 店铺信息错误：个人入驻，开店申请信息填写有误<br>
                    5. 店铺下线：把店铺改为下线状态，且商品全部下线。<a style="color: #d71319">该状态下会移除商家用户的供货商权限</a><br>
                    6. 关闭店铺：把店铺改为关闭状态，且商品全部下线。<a style="color: #d71319">该状态下会移除商家用户的供货商权限</a><br>
                </td>
            <tr>
        </table>
    </div>
</div>
</body>

<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/browser.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/infinite-linkage.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/shopdetail.js'/>"></script>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="${contextPath}/resources/templets/amaze/js/viewBigImage.js "></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/shopDetail.js'/>"></script>
<script type="text/javascript">
    var contextPath = "${contextPath}";
    var siteName = '${shopDetail.siteName}';
    $.validator.setDefaults({});
    
    function checkSiteNameExist() {
        var newSiteName = $("#siteName").val();
        if (siteName === newSiteName) {
            return true;
        }
        else {
            var config = true;
            $.ajax({
                type: "POST",
                url: contextPath + "/isSiteNameExist",
                async: false,
                data: {"siteName": newSiteName},
                dataType: "json",
                success: function (data) {
                    //false 是已存在此店铺名称
                    config = data;
                    if (!data) {
                        layer.alert("店铺名已存在", {icon: 0});
                    }
                }
            });
            return config;
        }
    }
</script>
</html>
