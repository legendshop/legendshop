<!Doctype html>
<html class="no-js fixed-layout">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<%@include file='/WEB-INF/pages/common/laydate.jsp'%>
<%@ include file="../back-common.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>移动装修- 页面装修</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item="/resources/common/css/pageManagement.css"/>" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content" style="overflow-x: auto;">
			<div class="seller_right" style="margin-bottom:70px;">
				<table class="${tableclass} title-border" style="width: 100%">
						<tr><th class="title-border">移动装修 &nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">页面装修<span></th></tr>
				</table>
				<div class="user_list">
					<div class="seller_list_title">
						<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
							<li <c:if test="${appPageManage.category eq 'INDEX'}"> class="this am-active"</c:if>>
								<i></i><a href="<ls:url address="/admin/app/pageManagement/query"/>">首页</a>
							</li>
							<li <c:if test="${appPageManage.category eq 'POSTER'}"> class="am-active"</c:if>>
								<i></i><a href="<ls:url address="/admin/app/pageManagement/query?category=POSTER"/>">海报页</a>
							</li>
						</ul>
					</div>
					<div>
						<form:form id="ListForm" method="get" action="${contextPath}/admin/app/pageManagement/query">
							<input type="hidden" value="${curPageNO==null?1:curPageNO}" id="curPageNO" name="curPageNO">
							
							<div class="criteria-div">
								
								<span class="item">
									<input class="${inputclass}" type="text" placeholder="请输入页面名称" id="name" value="${appPageManage.name}" name="name"> 
								</span>
								<span class="item">	
									状态:&nbsp; 
									<select id="status" name="status"  class="criteria-select">
										<option value="">全部</option>
										 <option value="-1">草稿</option>
						                <option value="0">已修改未发布</option>
						                <option value="1">已发布</option>
									</select> 
									<input class="${btnclass}" type="button" onclick="search()" value="查询">
									<input class="${btnclass}" type="button" onclick="newPage();"  value="新增页面">
								</span>
							</div>
						</form:form>
					</div>
					<div align="left" id="pageManagementContent" name="pageManagementContent" class="order-content-list"></div>
				</div>
			</div>
		</div>
	</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/pageManagement.js'/>"></script>
<script>
	var contextPath = "${contextPath}";
	var category = "${appPageManage.category}";
</script>
</body>
</html>
