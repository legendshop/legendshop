<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
   <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
        <form:form  action="${contextPath}/admin/system/district/save" method="post" id="form1">
         <table border="0" align="center" class="${tableclass} no-border " id="col1" style="width: 100%">
                <thead>
                    <tr class="sortable">
                        <th colspan="2" style="background-color: white !important;border-bottom: 1px solid #dddddd;">
                            <div align="center">
                                	省份管理
                            </div>
                        </th>
                    </tr>
                </thead>	
         	</table>
            <input id="id" name="id" value="${province.id}" type="hidden">
            <div align="center">
            <table border="0" align="center" class="${tableclass} no-border content-table" id="col1" style="width: 100%">
    		<tr>
		        <td width="200px;">
		          	<div align="right"><font color="ff0000">*</font>区域：</div>
		       	</td>
		        <td align="left" style="padding-left: 2px;">
		           	<input class="${inputclass}" type="text" name="groupby" id="groupby" value="${province.groupby}"   maxlength="10"/>
		        </td>
		</tr>
		<tr>            
		<tr>
		        <td>
		          	<div align="right"><font color="ff0000">*</font>名称：</div>
		       	</td>
		        <td align="left" style="padding-left: 2px;">
		           	<input class="${inputclass}" type="text" name="province" id="province" value="${province.province}"   maxlength="50"/>
		        </td>
		</tr>
		<tr>
	        <td>
	          	<div align="right"><font color="ff0000">*</font>邮编：</div>
	       	</td>
	        <td align="left" style="padding-left: 2px;">
	           	<input class="${inputclass}" type="text" name="provinceid" id="provinceid" value="${province.provinceid}"   maxlength="10"/>
	        </td>
		</tr>
		<tr>
	        <td>
	          	<div align="right"><font color="ff0000">*</font>省份简称：</div>
	       	</td>
	        <td align="left" style="padding-left: 2px;">
	           	<input class="${inputclass}" type="text" name="shortName" id="shortName" value="${province.shortName}"   maxlength="10"/>
	        </td>
		</tr>
                <tr>
                	<td></td>
                    <td align="left" style="padding-left: 0px;">
                        <div>
                            <input class="${btnclass}" type="submit" value="保存" />
                            
                            <input class="${btnclass}" type="button" value="返回"
                                onclick="window.location='<ls:url address="/admin/system/district/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
        </div>
        </div>
        </body>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/province.js'/>"></script>
<script language="javascript">
var contextPath = "${contextPath}";
</script>
</html>
