<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@include file='/WEB-INF/pages/common/laydate.jsp'%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>退款管理- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
<style type="text/css">
.over-text {
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	width: 100px;
	display: block;
}
.order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
}
</style>
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">售后管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">单退款管理</span></th>
				</tr>
			</table>
			<div class="retfund_list">
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li <c:if test="${status eq 'pending'}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/returnMoney/query/pending"/>">待处理</a></li>
						<li <c:if test="${status eq 'all' }"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/returnMoney/query/all"/>">所有记录</a></li>
					</ul>
				</div>
				<form:form action="${contextPath}/admin/returnMoney/query/${status}" id="form1" method="get">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
					<div class="criteria-div">
						<span class="item">
							 店铺：
							<input type="text" id="shopId" name="shopId" value="${paramData.shopId}" />
							<input type="hidden" id="shopName" name="shopName" value="${paramData.shopId}" class="${inputclass}"/>
							<input type="hidden" id="status" name="status" value="${status}" class="${inputclass}"/>
						</span>
						<span class="item">
							<select id="numType" name="numType" class="criteria-select">
								<option <c:if test="${paramData.numType eq 1 }">selected="selected"</c:if> value="1">订单编号</option>
								<option <c:if test="${paramData.numType eq 2 }">selected="selected"</c:if> value="2">退款编号</option>
								<option <c:if test="${paramData.numType eq 4 }">selected="selected"</c:if> value="4">会员名称</option>
							</select> 
						</span>
						<span class="item">
							<input type="text" id="number" name="number" value="${paramData.number }" class="${inputclass}" />
						</span>
						<span class="item">
							申请时间：
							<input type="text" id="startDate" name="startDate" readonly="readonly" value="<fmt:formatDate value="${paramData.startDate}" pattern="yyyy-MM-dd" />" placeholder="起始时间" class="${inputclass}" />
							– 
							<input type="text" id="endDate" name="endDate" readonly="readonly" value="<fmt:formatDate value="${paramData.endDate}" pattern="yyyy-MM-dd" />" placeholder="结束时间" class="${inputclass}" />
							<input type="button" onclick="search()" class="${btnclass}" value="搜索" />
						</span>
					</div>
			</form:form>
			<div align="center" id="refundContentList" class="order-content-list"></div>
		</div>
	</div>
</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		sendData(curPageNO);
	}
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		sendData(1);
	}
	$(document).ready(function () {
		var shopName = $("#shopName").val();
		if(shopName == ''){
	        makeSelect2("${contextPath}/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'shopName');
	    }else{
	    	makeSelect2("${contextPath}/admin/product/getShopSelect2","shopId","店铺","value","key",shopName, 'shopName');
			$("#select2-chosen-1").html(shopName);
	    } 
		sendData(1);
	});
	function sendData(curPageNO) {
	    var data = {
    		"shopId": $("#shopId").val(),
	        "curPageNO": curPageNO,
	        "status": $("#status").val(),
	        "numType": $("#numType").val(),
	        "number": $("#number").val(),
	        "startDate": $("#startDate").val(),
	        "endDate": $("#endDate").val(),
	    };
	    $.ajax({
	        url: "${contextPath}/admin/returnMoney/queryContent",
	        data: data,
	        type: 'post',
	        async: true, //默认为true 异步   
	        success: function (result) {
	            $("#refundContentList").html(result);
	        }
	    });
	}

	function makeSelect2(url,element,text,label,value,holder, textValue){
		$("#"+element).select2({
	        placeholder: holder,
	        allowClear: true,
	        width:'200px',
	        ajax: {  
	    	  url : url,
	    	  data: function (term,page) {
	                return {
	                	 q: term,
	                	 pageSize: 10,    //一次性加载的数据条数
	                 	 currPage: page, //要查询的页码
	                };
	            },
				type : "GET",
				dataType : "JSON",
	            results: function (data, page, query) {
	            	if(page * 5 < data.total){//判断是否还有数据加载
	            		data.more = true;
	            	}
	                return data;
	            },
	            cache: true,
	            quietMillis: 200//延时
	      }, 
	        formatInputTooShort: "请输入" + text,
	        formatNoMatches: "没有匹配的" + text,
	        formatSearching: "查询中...",
	        formatResult: function(row,container) {//选中后select2显示的 内容
	            return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
	        },formatSelection: function(row) { //选择的时候，需要保存选中的id
	        	$("#" + textValue).val(row.text);
	            return row.text;//选择时需要显示的列表内容
	        }, 
	    });
	}
	laydate.render({
		   elem: '#startDate',
		   calendar: true,
		   theme: 'grid',
	 	   trigger: 'click'
		  });
		   
  	 laydate.render({
		   elem: '#endDate',
		   calendar: true,
		   theme: 'grid',
	 	   trigger: 'click'
	    });
</script>

</html>

