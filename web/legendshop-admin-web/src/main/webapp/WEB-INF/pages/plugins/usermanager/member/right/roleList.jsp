<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%
	String curPageNO = (String) request.getParameter("curPageNO");
	int curpage = 1;
	if ("".equals(curPageNO) || curPageNO == null)
		curpage = 1;
	else {
		curpage = Integer.parseInt(curPageNO);
	}
	int offset = ((Integer) request.getAttribute("offset")).intValue();
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>权限管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">权限管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">角色管理</span></th>
				</tr>
			</table>
			<form:form id="form1" action="${contextPath}/admin/member/role/query"
				method="post">
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="<%=request.getAttribute("curPageNO")%>"> 
					<span class="item">
						名称：<input class="${inputclass}" type="text" name="name" maxlength="50" value="${bean.name }" placeholder="请输入名称"/> 
					</span>
					<span class="item">
						角色类型：
						<select id="category" name="category" class="criteria-select">
							<option value="" <c:if test="${empty bean.category}">selected="selected"</c:if>>-请选择-</option>
							<option value="1" <c:if test="${bean.category eq 1}">selected="selected"</c:if>>系统生成</option>
							<option value="2" <c:if test="${bean.category eq 2}">selected="selected"</c:if>>管理员创建</option>
					 	</select> 
				 	</span>
				 	<span class="item">
						状态：
						<select id="enabled" name="enabled" class="criteria-select">
							<ls:optionGroup type="select" required="false" cache="true" beanName="ENABLED" selectedValue="${bean.enabled}" />
					 	</select> 
					 	<input class="${btnclass}" onclick="search()" type="submit" value="搜索" /> 
					 	<input class="${btnclass}" type="button" value="创建角色" onclick='window.location="${contextPath}/admin/member/role/load"' />
				 	</span>
				</div>
			</form:form>

			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="${contextPath}/admin/member/role/query" id="item" export="false" class="${tableclass}" style="width:100%">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="名称" style="max-weidth : 300px">
						<div title="${item.note}">${item.name}</div>
					</display:column>
					<display:column title="角色名称 " property="roleType" style="max-weidth : 300px"></display:column>
					<display:column title="角色类型 " style="max-weidth : 300px">
						<c:choose>
							<c:when test="${item.category eq 1}">
								系统生成
							</c:when>
							<c:when test="${item.category eq 2}">
								管理员创建
							</c:when>
						</c:choose>
					</display:column>
					<display:column title="状态">
						<ls:optionGroup type="label" required="true" cache="true"
							beanName="ENABLED" selectedValue="${item.enabled}" defaultDisp="" />
					</display:column>
					<display:column title="对应用户" style="width: 90px;text-align:center;">
						<a href="${contextPath}/admin/member/role/loadUserByRole?roleId=${item.id}">用户</a>
					</display:column>
					<display:column title="操作" media="html" style="width:235px">
					<div class="table-btn-group">
						<c:choose>
							<c:when test="${item.category==1 or item.appNo== 'FRONT_END'}">
									<button class="tab-btn" onclick="window.location='${contextPath}/admin/member/role/addFunctions/${item.id}'">
										 分配权限
									</button>
							</c:when>
							<c:otherwise>
									<button class="tab-btn" onclick="window.location='${contextPath}/admin/member/role/addFunctions/${item.id}'">
										分配权限
									</button>
									<span class="btn-line">|</span>
									<button class="tab-btn" onclick="window.location='${contextPath}/admin/member/role/update/${item.id}'">
										修改
									</button>
									<span class="btn-line">|</span>
									<button class="tab-btn" onclick="deleteById('${item.id}')">
										删除
									</button>
							</c:otherwise>
						</c:choose>
					</div>
					</display:column>
				</display:table>
				<div class="clearfix">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
			</div>
			<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
				<tr>
					<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br> 系统角色不允许修改,创建,删除.</td>
				<tr>
			</table>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	var contextPath = '${contextPath}';
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		sendData(1);
	}
	
	function deleteById(id) {
		layer.confirm("确定删除？", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   window.location = "<ls:url address='/admin/member/role/delete/" + id + "'/>";
		   });
	}
	highlightTableRows("item");
</script>
</html>
