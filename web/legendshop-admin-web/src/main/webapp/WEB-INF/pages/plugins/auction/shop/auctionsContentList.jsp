<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}" pattern="yyyy-MM-dd HH:mm:ss"
	var="nowDate" />
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	<display:table name="list" requestURI="/admin/auction/query"
		id="item" export="false" class="${tableclass}" style="width:100%">
		<%-- 		<display:column title='<input type="checkbox" id="checkedAll">' class="orderwidth"> --%>
		<%-- 			<input type="checkbox" name="auctionId" value="${item.id}"> --%>
		<%-- 		</display:column> --%>
		<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
		<display:column title="拍卖活动标题">
		${item.auctionsTitle}
		</display:column>
		<display:column title="店铺">
			<a href="${contextPath}/admin/shopDetail/load/${item.shopId}">${item.shopName}</a>
		</display:column>
		<display:column title="开始时间" property="startTime"
			format="{0,date,yyyy-MM-dd HH:mm:ss}" sortable="true"></display:column>
		<display:column title="结束时间" property="endTime"
			format="{0,date,yyyy-MM-dd HH:mm:ss}" sortable="true"></display:column>
		<display:column title="起拍价">${item.floorPrice }</display:column>
		<display:column title="是否封顶">
			<c:if test="${item.isCeiling==1}">是</c:if>
			<c:if test="${item.isCeiling==0}">否</c:if>
		</display:column>
		<display:column title="一口价">
			<c:choose>
				<c:when test="${empty item.fixedPrice}">无</c:when>
				<c:otherwise>${item.fixedPrice}</c:otherwise>
			</c:choose>
		</display:column>
		<display:column title="活动状态">
		
			<c:choose>
				<c:when test="${item.status eq 2}">
					已完成
				</c:when>
				<c:when test="${empty item.prodStatus || item.prodStatus eq -1 || item.prodStatus eq -2}">
					商品已删除
				</c:when>
				<c:when test="${item.prodStatus eq 0 || item.skuStatus eq 0}">
					商品已下线
				</c:when>
				<c:when test="${item.prodStatus eq 2}">
					商品违规下线
				</c:when>
				<c:when test="${item.prodStatus eq 3}">
					商品待审核
				</c:when>
				<c:when test="${item.prodStatus eq 4}">
					商品审核失败
				</c:when>
				<c:when test="${item.status==-2}">
					审核未通过
				</c:when>
				<c:when test="${item.status==-1 and item.startTime gt nowDate}">
					审核中
				</c:when>
				<c:when test="${item.status==-1 and item.startTime lt nowDate}">
					   已过期
				</c:when>
				<c:when test="${item.status==0}">
					已失效
				</c:when>
				<c:when test="${item.status==1}">
					<c:if test="${item.startTime gt nowDate }">未开始</c:if>
					<c:if test="${item.startTime lt nowDate }">进行中</c:if>
				</c:when>
			</c:choose>
		</display:column>
		<display:column title="操作" media="html">
			<div class="table-btn-group">
				<button class="tab-btn" onclick="window.location='${contextPath}/admin/auction/audit/${item.id}'">
					查看
				</button>
				<c:if test="${item.status ne -2 && item.status ne -1 && item.startTime < nowDate}">
					<span class="btn-line">|</span>
					<button class="tab-btn" onclick="window.location='${contextPath}/admin/auction/operation/${item.id}'">
						运营
					</button>
				</c:if>
				<c:choose>
					<%-- 上线状态并且活动未结束  --%>
					 <c:when test="${item.status eq 1 && item.endTime > nowDate && item.startTime < nowDate }">
						 	<span class="btn-line">|</span>
							<button class="tab-btn" id="statusImg" onclick="offlineAuctions('${item.id}')" >
								终止
							</button>
					</c:when>
					<c:when test="${item.status eq -1 && item.endTime > nowDate}">
							<span class="btn-line">|</span>
							<button class="tab-btn" onclick="window.location='${contextPath}/admin/auction/audit/${item.id}'">
								审核
							</button>
					</c:when>
				</c:choose>
				</button>
			</div>
		</display:column>
	</display:table>

	<div class="clearfix">
   		<div class="fr">
			 <div class="page">
	 			 <div class="p-wrap">
	     		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
		 		 </div>
			</div>
		</div>
   	</div>
