<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>创建结算档期 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<form:form action="${contextPath}/admin/shopBillPeriod/save"
				method="post" id="form1">
				<input id="id" name="id" value="${shopBillPeriod.id}" type="hidden">
				<div align="center">
					<table border="0" align="center" class="${tableclass}" id="col1">
						<thead>
							<tr class="sortable">
								<th colspan="2">
									<div align="center">创建结算档期</div>
								</th>
							</tr>
						</thead>

						<tr>
							<td>
								<div align="center">
									档期标示，例如 201512-ID: <font color="ff0000">*</font>
								</div>
							</td>
							<td><input type="text" name="flag" id="flag"
								value="${shopBillPeriod.flag}" /></td>
						</tr>
						<tr>
							<td>
								<div align="center">
									应结金额: <font color="ff0000">*</font>
								</div>
							</td>
							<td><input type="text" name="resultTotalAmount"
								id="resultTotalAmount"
								value="${shopBillPeriod.resultTotalAmount}" /></td>
						</tr>
						<tr>
							<td>
								<div align="center">
									这期订单的实际金额: <font color="ff0000">*</font>
								</div>
							</td>
							<td><input type="text" name="orderAmount" id="orderAmount"
								value="${shopBillPeriod.orderAmount}" /></td>
						</tr>
						<tr>
							<td>
								<div align="center">
									佣金金额: <font color="ff0000">*</font>
								</div>
							</td>
							<td><input type="text" name="commisTotals" id="commisTotals"
								value="${shopBillPeriod.commisTotals}" /></td>
						</tr>
						<tr>
							<td>
								<div align="center">
									退单金额: <font color="ff0000">*</font>
								</div>
							</td>
							<td><input type="text" name="orderReturnTotals"
								id="orderReturnTotals"
								value="${shopBillPeriod.orderReturnTotals}" /></td>
						</tr>
						<tr>
							<td>
								<div align="center">
									建立时间: <font color="ff0000">*</font>
								</div>
							</td>
							<td><input type="text" name="recDate" id="recDate"
								value="${shopBillPeriod.recDate}" /></td>
						</tr>
						<tr>
							<td colspan="2">
								<div align="center">
									<input type="submit" value="保存" /> 
								    <input type="button" value="返回"
										onclick="window.location='<ls:url address="/admin/shopBillPeriod/query"/>'" />
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>
<script type='text/javascript'
	src="<ls:templateResource item='/common/js/jquery.validate.js'/>" /></script>
<script language="javascript">
	$.validator.setDefaults({});

	$(document).ready(function() {
		jQuery("#form1").validate({
			rules : {
				banner : {
					required : true,
					minlength : 5
				},
				url : "required"
			},
			messages : {
				banner : {
					required : "Please enter banner",
					minlength : "banner must consist of at least 5 characters"
				},
				url : {
					required : "Please provide a password"
				}
			}
		});

		//斑马条纹
		$("#col1 tr:nth-child(even)").addClass("even");
	});
</script>
</html>
