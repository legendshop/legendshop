<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>微信素材管理 - 后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/plugins/weixin/css/weixinImgFile.css'/>" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass}" style="width: 100%">
				<thead>
					<tr>
						<th><strong class="am-text-primary am-text-lg">微信管理</strong>
							/ 微信素材管理</th>
					</tr>
					<tr>
						<td>
							<ul class=" am-tabs-nav am-nav am-nav-tabs">
								<li><a href="${contextPath}/admin/weixinNewstemplate/query">图文消息</a>
								</li>
								<li class="am-active"><a>图片库</a></li>
							</ul>
						</td>
					</tr>
				</thead>
			</table>
			<div class="img_body">
				<div class="img_main">
					<div class="inner_main">
						<div class="bd">
							<div class="group_list">
								<div class="global_mod float_layout">
									<p class="global_info">
										<span class="group_name global_info_ele" data-id="1"
											id="js_currentgroup">${groupName}</span>
										<c:if test="${groupName!='未分组'}">
											<a href="javascript:;"
												onclick="renameGroup('${type}','${groupName}');"
												class="global_info_ele js_popover" id="js_rename">重命名</a>
											<a href="javascript:;"
												onclick="delGroup('${type}','${groupName}');"
												class="global_info_ele js_popover" id="js_delgroup">删除分组</a>
										</c:if>
									</p>
									<div class="global_extra">
										<div class="bubble_tips bubble_right warn">
											<div class="bubble_tips_inner">大小不超过5M</div>
											<i class="bubble_tips_arrow out"></i> <i
												class="bubble_tips_arrow in"></i>
										</div>
										<div class="upload_box align_right">
											<span class="upload_area webuploader-container"> <a
												id="js_upload" onclick="uploadFile();"
												class="btn btn_primary webuploader-pick" data-gid="1">本地上传</a>
												<form:form style='display: none;' id='formFile'
													name='formFile' method="post"
													action="${contextPath}/admin/weixinNewstemplate/uploadFile"
													target='frameFile' enctype="multipart/form-data">
													<input id="js_upload_btn" name="files" type="file"
														multiple="multiple" accept="image/*">
													<input type="hidden" value="${type}" name="groupId">
												</form:form> <iframe id='frameFile' name='frameFile'
													style='display: none;'></iframe>
											</span>
										</div>
									</div>
								</div>

								<div class="oper_group group">
									<div class="frm_controls oper_ele l">
										<label class="frm_checkbox_label" for="js_all"><input
											id="js_all" type="checkbox" class="frm_checkbox"
											data-label="全选"><i class="icon_checkbox"></i><span
											class="lbl_content">全选</span> </label>
									</div>
									<a id="js_batchmove"
										class="btn btn_default btn_disabled oper_ele l js_popover"
										href="javascript:;">移动分组</a> <a id="js_batchdel"
										class="btn btn_default btn_disabled oper_ele l js_popover"
										href="javascript:;">删除</a>
								</div>

								<div class="group img_pick" id="js_imglist">
									<ul class="group">
										<c:if test="${empty requestScope.list}">
											<div class="empty_tips">该分组暂时没有图片素材</div>
										</c:if>
										<c:forEach items="${requestScope.list}" var="imgFile"
											varStatus="status">
											<li class="img_item js_imgitem" data-id="213899583">
												<div class="img_item_bd">
													<img class="pic wxmImg Zoomin"
														src="<ls:photo item='${imgFile.filePath}' />"
														data-previewsrc="https://mmbiz.qlogo.cn/mmbiz/C51t2iaLUNTGy9ogfwOA35BdOdsbsBMTSJ5lA0MnrTTKr3SWrUcDfRTiaHFIibN8gBfjH748C2nfdb0BUCJGOrhuQ/0?wx_fmt=png"
														data-id="213899583"> <span class="check_content">
														<label class="frm_checkbox_label"
														for="checkbox${status.index+1}"><input
															type="checkbox" class="frm_checkbox"
															data-label="QQ截图20150922182607.png"
															data-id="${imgFile.id}" id="checkbox${status.index+1}"><i
															class="icon_checkbox"></i><span class="lbl_content">${imgFile.name}</span>
													</label>
													</span>
												</div>
												<div class="msg_card_ft">
													<ul class="grid_line msg_card_opr_list">
														<li class="grid_item size1of3 msg_card_opr_item"><a
															onclick="renameImg('${imgFile.id}','${imgFile.name}');"
															class="js_edit js_tooltip js_popover"
															data-id="${imgFile.id}" data-name="${imgFile.name}"
															href="javascript:;" data-tooltip="编辑名称"> <span
																class="msg_card_opr_item_inner"> <i
																	class="icon18_common edit_gray">编辑</i>
															</span> <span class="vm_box"></span>
														</a></li>
														<li class="grid_item size1of3 msg_card_opr_item"><a
															onclick="moveImg('${imgFile.id}','${imgFile.name}');"
															class="js_move js_tooltip js_popover" href="javascript:;"
															data-id="${imgFile.id}" data-tooltip="移动分组"> <span
																class="msg_card_opr_item_inner"> <i
																	class="icon18_common move_gray">移动</i>
															</span> <span class="vm_box"></span>
														</a></li>
														<li class="grid_item size1of3 no_extra msg_card_opr_item"><a
															onclick="delImg('${imgFile.id}','${imgFile.name}');"
															class="js_del js_tooltip js_popover"
															data-id="${imgFile.id}" href="javascript:;"
															data-tooltip="删除"> <span
																class="msg_card_opr_item_inner"> <i
																	class="icon18_common del_gray">删除</i>
															</span> <span class="vm_box"></span>
														</a></li>
													</ul>
												</div>
											</li>
										</c:forEach>

									</ul>
									<ls:page pageSize="${pageSize }" total="${total}"
										curPageNO="${curPageNO }" type="default" />
								</div>
							</div>
						</div>
					</div>
					<div class="inner_side">
						<div class="bd">
							<div class="group_list">
								<div class="inner_menu_box" id="js_group">
									<dl class="inner_menu">
										<c:forEach items="${requestScope.imgGroups}" var="g"
											varStatus="status">

											<dd
												class="inner_menu_item <c:if test="${type==g.id}">selected</c:if> js_groupitem"
												data-id="1">
												<a
													href="${contextPath}/admin/weixinNewstemplate/filePage?type=${g.id}"
													class="inner_menu_link" title='${g.id==0?"未分组":g.name}'>
													<strong>${g.id==0?"未分组":g.name}</strong><em class="num">(${g.count})</em>
												</a>
											</dd>
										</c:forEach>

									</dl>
								</div>
								<div class="inner_menu_item">
									<a href="javascript:;" onclick="createGroup();"
										class="inner_menu_link js_popover" id="js_creategroup"><i
										class="am-icon-plus"></i>新建分组</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="popover  pos_center" style="display: none;">
					<div class="popover_inner" style="padding:10px;top: 90px; left: 14px">
						<div class="popover_content jsPopOverContent">
							<div class="frm_control group_select">
								<c:forEach items="${requestScope.imgGroups}" var="g"
									varStatus="status">
									<label class="frm_radio_label"
										for="checkboxr_${status.index+1}"><input
										name="changeGroup" type="radio" class="frm_radio"
										data-id="${g.id}" data-label="${g.name}"
										id="checkboxr_${status.index+1}"><i class="icon_radio"></i><span
										class="lbl_content">${g.id==0?"未分组":g.name}</span> </label>
								</c:forEach>
							</div>
						</div>
						<%--<div class="popover_bar">
				<a href="javascript:;" class="btn btn_primary jsPopoverBt">确定</a>&nbsp;<a href="javascript:;" class="btn btn_default jsPopoverBt" style="margin-left: 20px;">取消</a>
			</div>
		--%>
					</div>
					<i class="popover_arrow popover_arrow_out"></i> <i
						class="popover_arrow popover_arrow_in"></i>
				</div>
			</div>
		</div>
	</div>
</body>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/weixin/js/weixinImgFile.js'/>"></script>
<script type="text/javascript">
	var contextPath = '${contextPath}';
	var type = '${type}';
</script>
</html>
