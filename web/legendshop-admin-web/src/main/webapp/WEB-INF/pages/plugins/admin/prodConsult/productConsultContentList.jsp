<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@include file='/WEB-INF/pages/common/laydate.jsp'%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
	<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	<display:table name="list" requestURI="/admin/productConsult/query" id="item" export="false" class="${tableclass}"
		style="width:100%;min-width=1024px" sort="external">
		<display:column title="
		  <span>
	           <label class='checkbox-wrapper ' style='float:left;margin-left:20px;'>
					<span class='checkbox-item'>
						<input type='checkbox' id='checkbox' class='checkbox-input selectAll' onclick='selectAll(this);'/>
						<span class='checkbox-inner' style='margin-left:10px;'></span>
					</span>
			   </label>	
			</span>" >
			<label class="checkbox-wrapper" style="float:left;margin-left:20px;">
				<span class="checkbox-item">
					<input type="checkbox" name='consultId' value='${item.consId}' class="checkbox-input selectOne" onclick="selectOne(this);"/>
					<span class="checkbox-inner" style="margin-left:10px;"></span>
				</span>
		    </label>	
		</display:column>
		<display:column title="顺序" style="min-width:60px"><%=offset++%></display:column>
      	<display:column title="店铺" style="min-width:120px">
				${item.siteName}
		</display:column>
		<display:column title="商品"   style="min-width:140px">
		<a href="${PC_DOMAIN_NAME}/views/${item.prodId}"  target="_blank" title="${item.prodName}" >${item.prodName}</a>
		</display:column>
		<display:column title="用户名" style="min-width:120px">
				${item.askUserName}
		</display:column>
		<display:column title="咨询内容" style="min-width: 200px; max-width: 300px; word-break:break-all">
			<div class="order_img_name">${item.content}</div>
		</display:column>
		<display:column title="咨询类型"   style="min-width:100px">
		      	<ls:optionGroup type="label" required="false" cache="true"
                beanName="CONSULT_TYPE" selectedValue="${item.pointType}" defaultDisp=""/>
		</display:column>
		<display:column title="咨询时间" style="min-width:150px" ><fmt:formatDate value="${item.recDate}" pattern="yyyy-MM-dd HH:mm:ss"/></display:column>
		<display:column title="回复状态" style="min-width:80px">
			<c:choose><c:when test="${item.replySts==1}">已回复</c:when><c:otherwise>未回复</c:otherwise></c:choose>
		</display:column>
		<display:column title="操作" media="html"  style="min-width:150px">
		      <div class="table-btn-group">
		      	<c:choose>
			      	<c:when test="${item.replySts==1}">
						<button class="tab-btn" onclick="window.location='${contextPath}/admin/productConsult/load/${item.id}'">查看</button>
						<span class="btn-line">|</span>
					</c:when>
					<c:otherwise>
						<button class="tab-btn" onclick="window.location='${contextPath}/admin/productConsult/load/${item.id}'">回复</button>
						<span class="btn-line">|</span>
					</c:otherwise>
				</c:choose>
				<button class="tab-btn" onclick="deleteById('${item.id}');" >删除</button>
			  </div>
		</display:column>
	</display:table>
	<div class="clearfix">
   		<div class="fl">
    		<input type="button" onclick="batchDel()" value="批量删除" id="batch-del" class="batch-btn">
		</div>
   		<div class="fr">
   			 <div cla ss="page">
    			 <div class="p-wrap">
        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			 	</div>
  			 </div>
  		</div>
   	</div>
<script type="text/javascript">

/* 全选  */
$("#checkboxAll").on("click",function(){
	if($(this).is(":checked")){
		$("input[type=checkbox][name=consultId]").prop("checked",true);
	}else{
		$("input[type=checkbox][name=consultId]").removeAttr("checked");s
	}
});
</script>
