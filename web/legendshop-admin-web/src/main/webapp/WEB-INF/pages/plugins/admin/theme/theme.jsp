<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>营销管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css"
	href="<ls:templateResource item='/resources/common/css/themeStyle.css'/>">
<link rel="stylesheet" type="text/css"
	href="<ls:templateResource item='/resources/common/css/spectrum.css'/>">
<link rel="stylesheet"
	href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
<link rel="stylesheet"
	href="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.css'/>" />
</head>
<style>
.subject_attr_con ul li label, .subject_commodity_con ul li label {
    font-weight: normal;
    display: inline-block;
    width: 130px;
    margin-right: 10px;
    text-align: left;
    vertical-align: top;
    line-height: 31px;
    padding-left: 8px;
}
</style>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<input type="hidden" id="imagesScale2"
				value="<ls:images item='PIC_DEFAL' scale='2'/>">
            <input type="hidden"
				id="imagesScale4" value="<ls:images item='PIC_DEFAL' scale='4'/>"> <input
				type="hidden" id="imagesScale6"
				value="<ls:images item='PIC_DEFAL' scale='6'/>"> <input type="hidden"
				id="imagesScale3" value="<ls:images item='PIC_DEFAL' scale='3'/>">
            <input type="hidden"
                   id="imagesScale8" value="<ls:images item='PIC_DEFAL' scale='4'/>">
			<table class="${tableclass}" style="width: 100%">
				<thead>
					<tr>
						<th class="no-bg title-th">
			            	<span class="title-span">
								平台营销  ＞  
								<a href="<ls:url address="/admin/theme/query"/>">专题管理</a>
								  ＞  <span style="color:#0e90d2;">专题编辑</span>
							</span>
			            </th>
					</tr>
				</thead>
			</table>
			<div class="themeDiv">
				<div class="subject_admin">
					<div class="subject_admin_tag">
						<ul class="am-tabs-nav am-nav am-nav-tabs">
							<li class="am-active"><a href="javascript:void(0);">属性编辑</a></li>
							<li id="plate_editor"><a href="javascript:void(0);">商品版块编辑</a></li>
<%--							<li id="correlation_subject"><a href="javascript:void(0);">相关专题</a></li>--%>
							<li id="preview_release"><a href="javascript:void(0);">预览/发布</a></li>
						</ul>
					</div>

					<!-- 专题属性编辑-->
					<div class="subject_attr subject_admin_con subject_admin_check">
						<%--<h2>专题属性编辑</h2>--%>
						<div class="subject_attr_con">
							<form:form action="${contextPath}/admin/theme/saveTheme"
								method="post" id="form1" enctype="multipart/form-data">
								<input type="hidden" id="themeId" name="themeId"
									value="${theme.themeId }">
								<input type="hidden" name="themePcImg"
									value="${theme.themePcImg }">
								<input type="hidden" name="themeMobileImg"
									value="${theme.themeMobileImg }">
								<input type="hidden" name="bannerPcImg"
									value="${theme.bannerPcImg }">
								<input type="hidden" name="bannerMobileImg"
									value="${theme.bannerMobileImg }">
								<input type="hidden" name="backgroundPcImg"
									value="${theme.backgroundPcImg }">
								<input type="hidden" name="backgroundMobileImg"
									value="${theme.backgroundMobileImg }">
								<ul>
									<li><span class="li-span"><em style="color: #e5004f; margin-right: 5px;">*</em>标题</span><input
										type='text' class="subject_attr_input" id="title" name="title"
										value="${theme.title }" maxlength="49" />
<%--										<div class="subject_attr_div">--%>
<%--											字体颜色 <input type='text' class="color_edit" id="titleColor" name="titleColor" value="${theme.titleColor }" /> --%>
<%--											<!-- <input type="checkbox" class="subject_attr_checkbox" value="1" id="isTitleShow" name="isTitleShow" />横幅区显示 -->--%>
<%--											<label class="checkbox-wrapper">--%>
<%--												<span class="checkbox-item">--%>
<%--													<input type="checkbox" value="1" id="isTitleShow" name="isTitleShow" class="checkbox-input selectOne" onclick="selectOne(this);"/>--%>
<%--													<span class="checkbox-inner"></span>--%>
<%--												</span>--%>
<%--												<span>横幅区显示</span>--%>
<%--										   </label>	--%>
<%--										</div>--%>
									</li>
									<%--  <li>
                                 <label>描述</label>
                                 <textarea class="subject_attr_text" id="themeDesc" name="themeDesc">${theme.themeDesc }</textarea>
                             </li> --%>
									<li><span class="li-span"><em style="color: #e5004f; margin-right: 5px;">*</em>专题简介</span>
										<textarea
											class="subject_attr_text" id="intro" maxlength="254"
											name="intro">${theme.intro }</textarea>
<%--										<div class="subject_attr_div">--%>
<%--											字体颜色 <input type='text' class="color_edit" id="introColor" name="introColor" value="${theme.introColor }" /> --%>
<%--											<!-- <input type="checkbox" class="subject_attr_checkbox" value="1" id="isIntroShow" name="isIntroShow" />横幅区显示 -->--%>
<%--											<label class="checkbox-wrapper">--%>
<%--												<span class="checkbox-item">--%>
<%--													<input type="checkbox" value="1" id="isIntroShow" name="isIntroShow" class="checkbox-input selectOne" onclick="selectOne(this);"/>--%>
<%--													<span class="checkbox-inner"></span>--%>
<%--												</span>--%>
<%--												<span>横幅区显示</span>--%>
<%--										   </label>	--%>
<%--										</div>--%>
									</li>
<%--									<li><span class="li-span"><em style="color: #e5004f; margin-right: 5px;">*</em>手机端简介</span>--%>
<%--										<textarea--%>
<%--											class="subject_attr_text" id="introMobile" maxlength="254"--%>
<%--											name="introMobile">${theme.introMobile }</textarea>--%>
<%--										<div class="subject_attr_div">--%>
<%--											字体颜色 <input type='text' class="color_edit" id="introMColor" name="introMColor" value="${theme.introMColor }" /> --%>
<%--											<!-- <input type="checkbox" class="subject_attr_checkbox" value="1" id="isIntroMShow" name="isIntroMShow" />横幅区显示 -->--%>
<%--											<label class="checkbox-wrapper">--%>
<%--												<span class="checkbox-item">--%>
<%--													<input type="checkbox" value="1" id="isIntroMShow" name="isIntroMShow" class="checkbox-input selectOne" onclick="selectOne(this);"/>--%>
<%--													<span class="checkbox-inner"></span>--%>
<%--												</span>--%>
<%--												<span>是否显示</span>--%>
<%--										   </label>	--%>
<%--										</div>--%>
<%--									</li>--%>
									<li style="line-height:30px;"><span class="li-span"><em style="color: #e5004f; margin-right: 5px;">*</em>列表小图</span> 
										<div class="pic_box">
											<a href="javascript:void(0);" class="subject_attr_file  file-btn">PC端
												<input type='file' id="themePcImgFile" name="themePcImgFile" />
											</a>
											<span class="tips-span">&nbsp;&nbsp;建议尺寸：500*300px</span>
											<c:if test="${not empty theme.themePcImg}">
												<div style="display: inline;">
													<img id="themePcImg" Width="120" Height="120" alt=""
														title="网页版活动图"
														src="<ls:images item='${theme.themePcImg }' scale='2'/>">
												</div>
											</c:if> <c:if test="${empty theme.themePcImg}">
												<div style="display: inline;">
													<img id="themePcImg" alt="" Width="120" Height="120"
														title="网页版活动图" src="">
												</div>
											</c:if>
										</div>
									</li>
									<li style="line-height:30px;">
										<span class="li-span"></span> 
										<div class="pic_box">
											<a href="javascript:void(0);" class="subject_attr_file file-btn">手机端<input
												type='file' id="themeMobileImgFile" name="themeMobileImgFile" /></a>
											<span class="tips-span">&nbsp;&nbsp;建议尺寸：750*400px</span>
											<c:if test="${not empty theme.themeMobileImg}">
												<div style="display: inline;">
													<img id="themeMobileImg" alt="" Width="120" Height="120"
														title="手机版活动图"
														src="<ls:images item='${theme.themeMobileImg}' scale='2'/>">
												</div>
											</c:if> <c:if test="${empty theme.themeMobileImg}">
												<div style="display: inline;">
													<img id="themeMobileImg" Width="120" Height="120" alt=""
														title="手机版活动图" src="">
												</div>
											</c:if> 
										</div>
									</li>
									<li>
										<span class="li-span"><em style="color: #e5004f; margin-right: 5px;">*</em>顶部横幅</span>
<%--										<div style="width:200px;">--%>
<%--											<span>背景色&nbsp;&nbsp;</span>--%>
<%--											<input type='text' class="color_edit" id="bannerImgColor"--%>
<%--												name="bannerImgColor" value="${theme.bannerImgColor }" />--%>
<%--										</div>--%>
									</li>	
									<li>
										<span class="li-span"></span>
										<div class="pic_box">
											<a href="javascript:void(0);" class="subject_attr_file file-btn">PC端
												<input type='file' id="bannerPcImgFile" name="bannerPcImgFile" />
											</a>
											<span>&nbsp;&nbsp;建议尺寸：1920*480</span>&nbsp;<span style="color: gray;">(双击图片可删除图片)</span>
											<c:if test="${not empty theme.bannerPcImg}">
												<div style="display: inline;">
													<img
														ondblclick="delThemeImg('bannerPcImg','bannerPcImgFile');"
														id="bannerPcImg" Width="120" Height="120" alt=""
														title="网页版横幅图"
														src="<ls:images item='${theme.bannerPcImg }' scale='2'/>">
												</div>
											</c:if> 
											<c:if test="${empty theme.bannerPcImg}">
												<div style="display: inline;">
													<img ondblclick="delCleanImg(this);" id="bannerPcImg"
														Width="120" Height="120" alt="" title="网页版横幅图" src="">
												</div>
											</c:if>
										</div>
									</li>
									<li>
										<span class="li-span"></span>
										<div class="pic_box">
											<a href="javascript:void(0);" class="subject_attr_file file-btn">手机端<input
												type='file' id="bannerMobileImgFile"
												name="bannerMobileImgFile" /></a>
											<span>&nbsp;&nbsp;建议尺寸：768*440&nbsp;&nbsp;</span>&nbsp;<span style="color: gray;">(双击图片可删除图片)</span>
											<c:if test="${not empty theme.bannerMobileImg}">
												<div style="display: inline;">
													<img ondblclick="delThemeImg('bannerMobileImg');"
														Width="120" Height="120" id="bannerMobileImg" alt=""
														title="手机版横幅图"
														src="<ls:images item='${theme.bannerMobileImg }' scale='2'/>">
												</div>
											</c:if> <c:if test="${empty theme.bannerMobileImg}">
												<div style="display: inline;">
													<img ondblclick="delCleanImg2(this);" Width="120"
														Height="120" id="bannerMobileImg" alt="" title="手机版横幅图"
														src="">
												</div>
											</c:if>
										</div>
									</li>
									<li>
										<span class="li-span"><em style="color: #e5004f; margin-right: 5px;">*</em>网页背景</span> 
										<div>
											<span>背景色&nbsp;&nbsp;</span><input type='text'
											class="color_edit" id="backgroundColor" name="backgroundColor"
											value="${theme.backgroundColor }" /> 
										</div>
									</li>
									<li>
										<span class="li-span"></span>
										<div class="pic_box">
											<a href="javascript:void(0);" class="subject_attr_file file-btn">PC端<input
												type='file' id="backgroundPcImgFile"
												name="backgroundPcImgFile" /></a>
											<span class="tips-span">&nbsp;&nbsp;(双击图片可删除图片)</span> 
											<c:if test="${not empty theme.backgroundPcImg}">
												<div style="display: inline;">
													<img ondblclick="delThemeImg('backgroundPcImg');"
														Width="120" Height="120" id="backgroundPcImg" alt=""
														title="网页版背景图"
														src="<ls:photo item='${theme.backgroundPcImg }'/>">
												</div>
											</c:if> <c:if test="${empty theme.backgroundPcImg}">
												<div style="display: inline;">
													<img ondblclick="delCleanImg3(this);" Width="120"
														Height="120" id="backgroundPcImg" alt="" title="网页版背景图"
														src="">
												</div>
											</c:if> 
										</div>
									</li>
									<li>
										<span class="li-span"></span>
										<div class="pic_box">
											<a href="javascript:void(0);" class="subject_attr_file file-btn">手机端<input
												type='file' id="backgroundMobileImgFile"
												name="backgroundMobileImgFile" /></a>
											<span class="tips-span">&nbsp;&nbsp;(双击图片可删除图片)</span> 
											<c:if test="${not empty theme.backgroundMobileImg}">
												<div style="display: inline;">
													<img ondblclick="delThemeImg('backgroundMobileImg');"
														Width="120" Height="120" id="backgroundMobileImg" alt=""
														title="手机版背景图"
														src="<ls:photo item='${theme.backgroundMobileImg }'/>">
												</div>
											</c:if> <c:if test="${empty theme.backgroundMobileImg}">
												<div style="display: inline;">
													<img ondblclick="delCleanImg4(this);" Width="120"
														Height="120" id="backgroundMobileImg" alt="" title="手机版背景图"
														src="">
												</div>
											</c:if> 
										</div>
									</li>
									<li><span class="li-span"><em style="color: #e5004f; margin-right: 5px;">*</em>活动开始日期</span>
										<input readonly="readonly" type="text"
											value="<fmt:formatDate value='${theme.startTime }' pattern='yyyy-MM-dd HH:mm:ss' />"
											name="startTime" id="startTime" class="subject_attr_input" />
									</li>
									<li><span class="li-span"><em style="color: #e5004f; margin-right: 5px;">*</em>活动截止日期</span>
										<input readonly="readonly" name="endTime"
											value="<fmt:formatDate value='${theme.endTime }' pattern='yyyy-MM-dd HH:mm:ss' />"
											id="endTime" class="subject_attr_input" type="text" />
									</li>
									<li><span class="li-span"><em style="color: #e5004f; margin-right: 5px;">*</em>选择模板类型</span> <c:choose>
											<c:when test="${theme != null && theme.templateType == 2}">
												<label class="radio-wrapper">
													<span class="radio-item">
														<input type="radio" class="radio-input" name="templateType" value="1">
														<span class="radio-inner"></span>
													</span>
													<span class="radio-txt">专题（有商品）</span>
												</label>
												<label class="radio-wrapper radio-wrapper-checked">
													<span class="radio-item">
														<input type="radio" class="radio-input" name="templateType" value="2" checked="checked">
														<span class="radio-inner"></span>
													</span>
													<span class="radio-txt">精选（没有商品）</span>
												</label>
											</c:when>
											<c:otherwise>
												<label class="radio-wrapper radio-wrapper-checked">
													<span class="radio-item">
														<input type="radio" class="radio-input" name="templateType" value="1"  checked="checked">
														<span class="radio-inner"></span>
													</span>
													<span class="radio-txt">专题（有商品）</span>
												</label>
												<label class="radio-wrapper">
													<span class="radio-item">
														<input type="radio" class="radio-input" name="templateType" value="2">
														<span class="radio-inner"></span>
													</span>
													<span class="radio-txt">精选（没有商品）</span>
												</label>
											</c:otherwise>
										</c:choose></li>
<%--									<li><span class="li-span">自定义模块：</span> --%>
<%--										<textarea name="customContent" id="customContent" cols="50" rows="10"--%>
<%--												style="width: 60%; height: 450px; visibility: hidden; float: left;"--%>
<%--												maxlength="60000">${theme.customContent}--%>
<%--										</textarea>--%>
<%--									</li>--%>
									<div class="subject_admin_but" buttonStatus="0" id="attrSubmit">
										<a href="javascript:void(0);" class="criteria-btn"
											onclick="javascript:saveTheme();">保存</a>
									</div>
								</ul>
							</form:form>
						</div>
					</div>

					<!-- 商品版块编辑 -->
					<div class="subject_commodity subject_admin_con">
						<%--<h2>商品版块编辑</h2>--%>
						<div id="allModuleDiv">
							<c:forEach items="${themeModuleList}" var="module"
								varStatus="status">
								<form:form action="${contextPath}/admin/theme/saveThemeModule"
									method="post" id="moduleForm${status.index+1}"
									enctype="multipart/form-data">
									<input type="hidden" name="themeId" value="${module.themeId }">
									<input type="hidden" id="themeModuleId${status.index+1}"
										name="themeModuleId" value="${module.themeModuleId }">
									<input type="hidden" id="titleBgPcimg${status.index+1}"
										name="titleBgPcimg" value="${module.titleBgPcimg }">
									<input type="hidden" id="titleBgMobileimg${status.index+1}"
										name="titleBgMobileimg" value="${module.titleBgMobileimg }">
									<h2>
										商品版块${status.index+1}<a
											href="javascript:delModule('${status.index+1}');" class="fr">删除</a>
									</h2>
									<div class="subject_commodity_con">
										<ul>
											<li><label><em style="color: #e5004f; margin-right: 5px;">*</em>版块名称</label><input
												id="moduleTitle${status.index+1}" value="${module.title }"
												name="title" type='text' class="subject_attr_input"
												maxlength="20" />
											</li>
											<li>
												<div class="subject_attr_div">
													<label><em style="color: #e5004f; margin-right: 5px;">*</em>字体颜色：</label>
													<input id="moduleTitleColor${status.index+1}" value="${module.titleColor }" name="titleColor"
															   type='text' class="color_edit" />
													<!-- <input type="checkbox"
                                                        class="subject_attr_checkbox" id="moduleTitleShow1"
                                                        value="1" name="isTitleShow" />横幅区显示 -->
														<%--												<label class="checkbox-wrapper">--%>
														<%--															<span class="checkbox-item">--%>
														<%--																<input type="checkbox" id="moduleTitleShow1" value="1" name="isTitleShow"--%>
														<%--																	   class="checkbox-input selectOne" onclick="selectOne(this);"/>--%>
														<%--																<span class="checkbox-inner"></span>--%>
														<%--															</span>--%>
														<%--													<span>横幅区显示</span>--%>
														<%--												</label>--%>
												</div>
											</li>
											<li><label>“更多”跳转</label> <input
												id="moduleMoreUrl${status.index+1}"
												value="${module.moreUrl }" name="moreUrl" type='text'
												class="subject_attr_input" /></li>
											<li><label><em style="color: #e5004f; margin-right: 5px;">*</em>顺序</label> <input
												id="moduleSeq${status.index+1}" value="${module.seq }"
												name="seq" type='text' class="subject_attr_input"
												maxlength="5"
												onkeyup="this.value=this.value.replace(/\D/g,'')"
												onafterpaste="this.value=this.value.replace(/\D/g,'')" /></li>
										</ul>
									</div>

									<div class="subject_commodity_con">
										<ul>
											<li>
												<label><em style="color: #e5004f; margin-right: 5px;"></em>广告图片</label>
													<%--												 <span>背景色&nbsp;&nbsp;</span>--%>
													<%--												 <input--%>
													<%--													id="moduleTitleBgColor${status.index+1}"--%>
													<%--													value="${module.titleBgColor}" type='text'--%>
													<%--													class="color_edit" name="titleBgColor" />--%>
													<%--												<br><label></label>--%>
												<a href="javascript:void(0);" class="subject_attr_file file-btn"
												   onclick="uploadtitleBgPcimgFile('${status.index+1}')">
													PC端<input type='file' name="titleBgPcimgFile"
															  id="titleBgPcimgFile${status.index+1}" />
												</a>
												<span style="color: gray;">(双击图片可删除图片)</span>
												<br><label></label>
												<c:if test="${not empty module.titleBgPcimg}">
													<div style="display: inline;">
														<img
																ondblclick="delModuleImg('titleBgPcimg','${status.index+1}');"
																Width="120" Height="120"
																id="modulePcImg${status.index+1}" alt="" title="网页版版头背景图"
																src="<ls:images item='${module.titleBgPcimg }' scale='2'/>">
													</div>
												</c:if>
												<c:if test="${empty module.titleBgPcimg}">
													<div style="display: inline;">
														<img
																ondblclick="delModuleImg4('titleBgPcimg','${status.index+1}');"
																Width="120" Height="120"
																id="modulePcImg${status.index+1}" alt="" title="网页版版头背景图"
																src="<ls:images item='${module.titleBgPcimg }' scale='2'/>">
													</div>
												</c:if>
												<br><label></label>
												<a href="javascript:void(0);" class="subject_attr_file file-btn"  style="margin-top:10px;"
												   onclick="uploadtitleBgMobileimgFile('${status.index+1}')">
													手机端<input type='file' name="titleBgMobileimgFile"
															  id="titleBgMobileimgFile${status.index+1}" />
												</a>
												<span style="color: gray;">(双击图片可删除图片)</span>
												<br><label></label>
												<c:if test="${not empty module.titleBgMobileimg}">
													<div style="display: inline;">
														<img
																ondblclick="delModuleImg('titleBgMobileimg','${status.index+1}');"
																Width="120" Height="120"
																id="moduleMobileImg${status.index+1}" alt=""
																title="手机版版头背景图"
																src="<ls:images item='${module.titleBgMobileimg }' scale='2'/>">
													</div>
												</c:if>
												<c:if test="${empty module.titleBgMobileimg}">
													<div style="display: inline;">
														<img
																ondblclick="delModuleImg4('titleBgMobileimg','${status.index+1}');"
																Width="120" Height="120"
																id="moduleMobileImg${status.index+1}" alt=""
																title="手机版版头背景图"
																src="<ls:images item='${module.titleBgMobileimg }' scale='2'/>">
													</div>
												</c:if>
											</li>
											<li>
												<label>广告链接：</label>
												<input  id="adPcUrl${status.index+1}"  name="adPcUrl" type='text' class="subject_attr_input" value="${module.adPcUrl }"/>
											</li>
											<li>
												<label>广告链接(手机)：</label>
												<input id="adMobileUrl${status.index+1}" name="adMobileUrl" type='text' class="subject_attr_input" value="${module.adMobileUrl }"/>
											</li>
										</ul>
									</div>

									<div class="subject_admin_but">
										<a href="javascript:void(0);" class="criteria-btn"
											onclick="javascript:saveThemeModule('${status.index+1}');">保存</a>
									</div>
									<div class="subject_commodity_list">
										<ul class="clearfix">
											<c:forEach items="${module.moduleProdList}" var="prd"
												varStatus="sts">
												<li id="prd${prd.modulePrdId}">
													<div class="subject_list_img">
														<a target="_blank"
															href="${contextPath}/views/${prd.prodId }"><img
															class="prd_img"
															src="<ls:images item='${prd.img }' scale='4' />"></a>
														<div class="tag"
															style="background-image:url('<ls:images item='${prd.angleIcon }' scale='3'/>');background-repeat: no-repeat;">

														</div>
													</div>
													<div class="subject_list_details">
														<h3 class="prd_name">${prd.prodName }</h3>
														<div class="subject_list_price">
															<span>￥${prd.cash }</span>
														</div>
													</div>
													<div class="oper">
														<a href="javascript:delModuleProd('${prd.modulePrdId }');"
															class="delete">删除</a><a
															href="javascript:editModuleEdit('${prd.modulePrdId }','${prd.cash }元');"
															class="edit">编辑</a>
													</div>
												</li>
											</c:forEach>
											<li class="add_commodity"><a
												href="javascript:popupSelect('${status.index+1}');">+</a></li>
										</ul>
									</div>
								</form:form>
							</c:forEach>
							<c:if test="${empty themeModuleList}">
								<form:form action="${contextPath}/admin/theme/saveThemeModule"
									method="post" id="moduleForm1" enctype="multipart/form-data">
									<input type="hidden" name="themeId" value="${theme.themeId }">
									<input type="hidden" id="themeModuleId1" name="themeModuleId">
									<input type="hidden" id="titleBgPcimg1" name="titleBgPcimg">
									<input type="hidden" id="titleBgMobileimg1"
										name="titleBgMobileimg">
									<h2>
										商品版块<a href="javascript:delModule(1);" class="fr">删除</a>
									</h2>
									<div class="subject_commodity_con">
										<ul>
											<li><label><em style="color: #e5004f; margin-right: 5px;">*</em>版块名称：</label><input id="moduleTitle1"
												name="title" type='text' class="subject_attr_input"
												maxlength="20" />
											</li>
											<li>
												<div class="subject_attr_div">
													<label><em style="color: #e5004f; margin-right: 5px;">*</em>字体颜色：</label>
													<input id="moduleTitleColor1" value="#000000" name="titleColor" type='text' class="color_edit" />
													<!-- <input type="checkbox"
                                                        class="subject_attr_checkbox" id="moduleTitleShow1"
                                                        value="1" name="isTitleShow" />横幅区显示 -->
														<%--												<label class="checkbox-wrapper">--%>
														<%--															<span class="checkbox-item">--%>
														<%--																<input type="checkbox" id="moduleTitleShow1" value="1" name="isTitleShow"--%>
														<%--																	   class="checkbox-input selectOne" onclick="selectOne(this);"/>--%>
														<%--																<span class="checkbox-inner"></span>--%>
														<%--															</span>--%>
														<%--													<span>横幅区显示</span>--%>
														<%--												</label>--%>
												</div>
											</li>
											<li><label>“更多”跳转：</label><input id="moduleMoreUrl1"
												name="moreUrl" type='text' class="subject_attr_input"
												maxlength="50" /></li>
											<li><label><em style="color: #e5004f; margin-right: 5px;">*</em>顺序：</label><input id="moduleSeq1" name="seq"
												type='text' class="subject_attr_input" maxlength="3" /></li>
										</ul>
									</div>
									<div class="subject_commodity_con">
										<ul>
											<li>
												<div class="bg_box">
													<label>广告图片：</label>
														<%--													<span>背景色&nbsp;&nbsp;</span><input id="moduleTitleBgColor1" type='text' class="color_edit"--%>
														<%--														name="titleBgColor" />--%>
														<%--													<br>--%>
														<%--													<label></label>--%>
													<a href="javascript:void(0);" class="subject_attr_file file-btn"
													   onclick="uploadtitleBgPcimgFile(1)">PC端<input
															type='file' name="titleBgPcimgFile" id="titleBgPcimgFile1" />
													</a>
													<span style="color: gray;">(双击图片可删除图片)</span>
												</div>
												<label></label>
												<div style="display: inline;">
													<img ondblclick="delModuleImg2(this);" Width="800"
														 Height="80" id="modulePcImg1" alt="" title="网页版版头背景图"
														 src="">
												</div>
												<div class="bg_box">
													<label></label>
													<a href="javascript:void(0);" class="subject_attr_file file-btn"
													   onclick="uploadtitleBgMobileimgFile('1')">手机端<input
															type='file' name="titleBgMobileimgFile"
															id="titleBgMobileimgFile1" style="margin-top:5px;" /></a>
													<span style="color: gray;">(双击图片可删除图片)</span>
												</div>
												<label></label>
												<div style="display: inline;">
													<img ondblclick="delModuleImg3(this);" Width="350"
														 Height="120" id="moduleMobileImg1" alt="" title="手机版版头背景图"
														 src="">
												</div>
											</li>
											<li>
												<label>广告链接：</label>
												<input  id="adPcUrl0"  name="adPcUrl" type='text' class="subject_attr_input" />
											</li>
											<li>
												<label>广告链接(手机)：</label>
												<input id="adMobileUrl0" name="adMobileUrl" type='text' class="subject_attr_input" />
											</li>
										</ul>
									</div>

									<div class="subject_admin_but">
										<a href="javascript:void(0);"
											onclick="javascript:saveThemeModule('1');" class="criteria-btn">保存</a>
									</div>
									<div>
										<span style="padding-left:68px;height: 30px;float:left;">添加商品：&nbsp;&nbsp;&nbsp;</span> 
										<div class="subject_commodity_list">
											<ul class="clearfix">
												<li class="add_commodity"><a
													href="javascript:popupSelect(1);">+</a></li>
											</ul>
										</div>
									</div>
								</form:form>
							</c:if>
						</div>
						<div class="subject_admin_but">
							<a href="javascript:addModule();" class="add_commodity_but criteria-btn">添加商品模块</a>
						</div>
					</div>

					<!-- 相关专题编辑 -->
<%--					<div class="correlation_subject subject_admin_con">--%>
<%--						&lt;%&ndash;<h2>相关专题</h2>&ndash;%&gt;--%>
<%--						<form:form action="${contextPath}/admin/theme/saveThemeRelated"--%>
<%--							method="post" id="relatedForm" enctype="multipart/form-data">--%>
<%--							<input type="hidden" name="themeId" value="${theme.themeId }">--%>
<%--							<div class="correlation_subject_con clearfix">--%>
<%--								<ul>--%>
<%--									<c:forEach items="${themeRelatedDtoList}" var="related"--%>
<%--										varStatus="sts">--%>
<%--										<li>--%>
<%--											&lt;%&ndash;<input type="hidden" id="relatedId${sts.index+1}"&ndash;%&gt;--%>
<%--											&lt;%&ndash;name="relatedId${sts.index+1}" value="${related.relatedId }">&ndash;%&gt;--%>
<%--											<input type="hidden" id="img${sts.index+1}"--%>
<%--											name="img${sts.index+1}" value="${related.img }">--%>
<%--											<h3>--%>
<%--												<em style="color: #e5004f; margin-right: 5px;">*</em>专题${sts.index+1 }--%>
<%--											</h3>--%>
<%--											<input type='hidden' id="relatedId${sts.index+1}" name="relatedId${sts.index+1}" value="${related.relatedId}"/>--%>
<%--											<div>--%>
<%--												<label><em--%>
<%--													style="color: #e5004f; margin-right: 5px;">*</em>图片：</label> <img--%>
<%--													id="relatedImg${sts.index+1}" alt="" title="相关专题图"--%>
<%--													Width="80" Height="80"--%>
<%--													src="<ls:images item='${related.img }' scale='2'/>">--%>

<%--												<a href="javascript:void(0);">选择<input type='file'--%>
<%--													id="relatedImgFile${sts.index+1}"--%>
<%--													name="imgFile${sts.index+1}" /></a>--%>
<%--											</div>--%>
<%--											<div class="subject_item_box ">--%>
<%--												<label class="relatedThemeId${sts.index+1}"><em style="color: #e5004f; margin-right: 5px;">*</em>--%>
<%--													专题(PC端)：</label>--%>
<%--												<input type='hidden' id="relatedThemeId${sts.index+1}" name="relatedThemeId${sts.index+1}" value="${related.relatedThemeId }" />--%>
<%--												<a class="addThemepc${sts.index+1}" target="_blank" href="${contextPath}/admin/theme/preview/${related.relatedThemeId }" style="color:blue" >${related.pcTitle }</a>--%>
<%--												<a onclick="loadSelectTheme('${theme.themeId}',${sts.index+1},'pc')" class="file-btn loadSelectTheme${sts.index+1}">--%>
<%--													选择专题</a>--%>
<%--												<br>--%>
<%--											</div>--%>
<%--											<div class="subject_item_box ">--%>
<%--												<label class="mRelatedThemeId${sts.index+1}"><em style="color: #e5004f; margin-right: 5px;">*</em>--%>
<%--													专题(手机端)：</label>--%>
<%--												<input type='hidden' id="mRelatedThemeId${sts.index+1}" name="mRelatedThemeId${sts.index+1}" value="${related.mRelatedThemeId}" />--%>
<%--												<a class="addThememobile${sts.index+1}" target="_blank" href="${contextPath}/admin/theme/preview/${related.mRelatedThemeId }" style="color:blue" >${related.mobileTitle }</a>--%>
<%--												<a onclick="loadSelectTheme('${theme.themeId}',${sts.index+1},'mobile')" class="file-btn loadSelectTheme${sts.index+1}">--%>
<%--													选择专题</a>--%>
<%--												<br>--%>
<%--											</div>--%>

<%--											&lt;%&ndash;<div>&ndash;%&gt;--%>
<%--												&lt;%&ndash;<label><em&ndash;%&gt;--%>
<%--													&lt;%&ndash;style="color: #e5004f; margin-right: 5px;">*</em>PC端链接：</label><input&ndash;%&gt;--%>
<%--													&lt;%&ndash;type='text' id="relatedLink${sts.index+1}"&ndash;%&gt;--%>
<%--													&lt;%&ndash;name="link${sts.index+1}" value="${related.link }" />&ndash;%&gt;--%>
<%--											&lt;%&ndash;</div>&ndash;%&gt;--%>
<%--											&lt;%&ndash;<div>&ndash;%&gt;--%>
<%--												&lt;%&ndash;<label><em&ndash;%&gt;--%>
<%--													&lt;%&ndash;style="color: #e5004f; margin-right: 5px;">*</em>手机端链接：</label><input&ndash;%&gt;--%>
<%--													&lt;%&ndash;type='text' id="relatedMLink${sts.index+1}"&ndash;%&gt;--%>
<%--													&lt;%&ndash;name="linkM${sts.index+1}" value="${related.mLink }" />&ndash;%&gt;--%>
<%--											&lt;%&ndash;</div></li>&ndash;%&gt;--%>
<%--									</c:forEach>--%>
<%--									<c:if test="${empty themeRelatedList}">--%>

<%--										<div class="subject_item" >--%>
<%--											<li>--%>
<%--												<h3>专题1</h3>--%>
<%--												<div class="subject_item_box">--%>
<%--													<label><em--%>
<%--															style="color: #e5004f; margin-right: 5px;">*</em>图片：</label>--%>
<%--													<a href="javascript:void(0);" class="file-btn">选择<input type='file'--%>
<%--																											id="relatedImgFile1" name="imgFile1" /></a><br>--%>
<%--													<label></label>--%>
<%--													<div style="display: inline;">--%>
<%--														<img id="relatedImg1" Width="80" Height="80" alt=""--%>
<%--															 title="相关专题图" src="">--%>
<%--													</div>--%>
<%--												</div>--%>
<%--												<div class="subject_item_box ">--%>
<%--													<label class="relatedThemeId1"><em style="color: #e5004f; margin-right: 5px;">*</em>--%>
<%--														专题(PC端)：</label>--%>
<%--													<input type='hidden' id="relatedThemeId1" name="relatedThemeId1" />--%>
<%--													<a onclick="loadSelectTheme('${theme.themeId}',1,'pc')" class="file-btn loadSelectThemePC1">--%>
<%--														选择专题</a><br>--%>
<%--													<br>--%>
<%--												</div>--%>
<%--												<div class="subject_item_box ">--%>
<%--													<label class="mRelatedThemeId1"><em style="color: #e5004f; margin-right: 5px;">*</em>--%>
<%--														专题(手机端)：</label>--%>
<%--													<input type='hidden' id="mRelatedThemeId1" name="mRelatedThemeId1" />--%>
<%--													<a onclick="loadSelectTheme('${theme.themeId}',1,'mobile')" class="file-btn loadSelectThemePC1">--%>
<%--														选择专题</a><br>--%>
<%--													<br>--%>
<%--												</div>--%>
<%--											</li>--%>
<%--										</div>--%>
<%--										<div class="subject_item" >--%>
<%--											<li>--%>
<%--												<h3>专题2</h3>--%>
<%--												<div class="subject_item_box">--%>
<%--													<label><em--%>
<%--														style="color: #e5004f; margin-right: 5px;">*</em>图片：</label>--%>
<%--													<a href="javascript:void(0);" class="file-btn">选择<input type='file'--%>
<%--														id="relatedImgFile2" name="imgFile2" /></a><br>--%>
<%--													<label></label>--%>
<%--													<div style="display: inline;">--%>
<%--														<img id="relatedImg2" Width="80" Height="80" alt=""--%>
<%--															title="相关专题图" src="">--%>
<%--													</div>--%>
<%--												</div>--%>
<%--												<div class="subject_item_box ">--%>
<%--													<label class="relatedThemeId2"><em style="color: #e5004f; margin-right: 5px;">*</em>--%>
<%--														专题(PC端)：</label>--%>
<%--													<input type='hidden' id="relatedThemeId2" name="relatedThemeId2" />--%>
<%--													<a onclick="loadSelectTheme('${theme.themeId}',2,'pc')" class="file-btn loadSelectThemePC2">--%>
<%--														选择专题</a><br>--%>
<%--													<br>--%>
<%--												</div>--%>
<%--												<div class="subject_item_box ">--%>
<%--													<label class="mRelatedThemeId2"><em style="color: #e5004f; margin-right: 5px;">*</em>--%>
<%--														专题(手机端)：</label>--%>
<%--													<input type='hidden' id="mRelatedThemeId2" name="mRelatedThemeId2" />--%>
<%--													<a onclick="loadSelectTheme('${theme.themeId}',2,'mobile')" class="file-btn loadSelectThemePC2">--%>
<%--														选择专题</a><br>--%>
<%--													<br>--%>
<%--												</div>--%>
<%--											</li>--%>
<%--										</div>--%>
<%--										<div class="subject_item" >--%>
<%--											<li>--%>

<%--												<h3>专题3</h3>--%>
<%--												<div class="subject_item_box">--%>
<%--													<label><em--%>
<%--														style="color: #e5004f; margin-right: 5px;">*</em>图片：</label>--%>
<%--													<a href="javascript:void(0);" class="file-btn">选择<input type='file'--%>
<%--														id="relatedImgFile3" name="imgFile3" /></a><br>--%>
<%--													<label></label>--%>
<%--													<div style="display: inline;">--%>
<%--														<img id="relatedImg3" Width="80" Height="80" alt=""--%>
<%--															title="相关专题图" src="">--%>
<%--													</div>--%>
<%--												</div>--%>
<%--												&lt;%&ndash;<div class="subject_item_box">&ndash;%&gt;--%>
<%--													&lt;%&ndash;<label><em&ndash;%&gt;--%>
<%--														&lt;%&ndash;style="color: #e5004f; margin-right: 5px;">*</em>PC端链接：</label><input&ndash;%&gt;--%>
<%--														&lt;%&ndash;type='text' id="relatedLink3" name="link3" />&ndash;%&gt;--%>
<%--												&lt;%&ndash;</div>&ndash;%&gt;--%>
<%--												&lt;%&ndash;<div class="subject_item_box">&ndash;%&gt;--%>
<%--													&lt;%&ndash;<label><em&ndash;%&gt;--%>
<%--														&lt;%&ndash;style="color: #e5004f; margin-right: 5px;">*</em>手机端链接：</label><input&ndash;%&gt;--%>
<%--														&lt;%&ndash;type='text' id="relatedMLink3" name="linkM3" />&ndash;%&gt;--%>
<%--												&lt;%&ndash;</div>&ndash;%&gt;--%>
<%--												<div class="subject_item_box ">--%>
<%--													<label class="relatedThemeId3"><em style="color: #e5004f; margin-right: 5px;">*</em>--%>
<%--														专题(PC端)：</label>--%>
<%--													<input type='hidden' id="relatedThemeId3" name="relatedThemeId3" />--%>
<%--													<a onclick="loadSelectTheme('${theme.themeId}',3,'pc')" class="file-btn loadSelectThemePC1">--%>
<%--														选择专题</a><br>--%>
<%--													<br>--%>
<%--												</div>--%>
<%--												<div class="subject_item_box ">--%>
<%--													<label class="mRelatedThemeId3"><em style="color: #e5004f; margin-right: 5px;">*</em>--%>
<%--														专题(手机端)：</label>--%>
<%--													<input type='hidden' id="mRelatedThemeId3" name="mRelatedThemeId3" />--%>
<%--													<a onclick="loadSelectTheme('${theme.themeId}',3,'mobile')" class="file-btn loadSelectThemeM1">--%>
<%--														选择专题</a>--%>
<%--													<br>--%>
<%--												</div>--%>
<%--											</li>--%>
<%--										</div>--%>
<%--									</c:if>--%>
<%--								</ul>--%>
<%--							</div>--%>
<%--							<div class="subject_admin_but">--%>
<%--								<a href="javascript:saveThemeRelated();" class="criteria-btn">保存</a>--%>
<%--							</div>--%>
<%--						</form:form>--%>
<%--					</div>--%>

					<div class="subject_edit subject_admin_con">
						<div class="subject_edit_con">
							<div class="subject_admin_but">
								<a href="javascript:void(0);" class="preview criteria-btn"
									onclick="javascript:preview()">预览</a> <a
									href="javascript:void(0);" class="release criteria-btn"
									onclick="javascript:onLine();">发布</a><a
									href="${contextPath}/admin/theme/query" class="criteria-btn">返回</a>
							</div>
						</div>
					</div>
				</div>

				<div class="popup_shade"></div>

				<!-- 选择商品弹框 -->
				<div class="subject_select_popup">
					<div class="import_prod_con">
						<span class="floor_choose">已选商品(备注：双击删除已有商品信息)：</span>
						<div class="box_floor_prodel">
							<input type="hidden" value="" id="selProdId">
							<div class="floor_box_pls">
								<span
									style="display: block; width: 100%; text-align: center; color: #FFC7AC; line-height: 139px;">
									选择预览区</span>
							</div>
						</div>
						<span class="floor_choose">选择要展示的商品(注释：点击选择)：</span>
						<div class="floor_choose_box">
							<span class="floor_choose_sp"> 
							商品名称： 
							<input type="text" name="themProdName" class="themProdName" id="selProdId" style="width: 100px;" /> 
							商品分类： 
							<input type="text" style="cursor: pointer; width: 100px;" id="prodCatName" name="categoryName" value="${prod.categoryName }"
								onclick="loadCategoryDialog();" placeholder="商品分类" readonly="readonly" /> 
							<input type="hidden" id="prodCatId" name="categoryId" value="${prod.categoryId }" />
							<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/> 
							<input type="button" value="搜索" onclick="searchThemeProd()" class="criteria-btn"  />
							</span>
							<div id="theme_goods_list">
								<span
									style="display: block; width: 100%; text-align: center; color: #999; line-height: 140px;">
									选择区</span>
							</div>
						</div>
						<div class="add_prod_but">
							<a href="javascript:getProdInfo();" class="save">下一步</a><a
								href="javascript:void(0);" class="cancel">取消</a>
						</div>
					</div>
				</div>
				<!-- 商品编辑弹框 -->
				<div class="subject_admin_popup">
					<form:form action="${contextPath}/admin/theme/saveThemeModulePrd" method="post" id="modulePrdForm" enctype="multipart/form-data">
						<input type="hidden" id="moduleId" name="themeModuleId">
						<input type="hidden" id="modulePrdId" name="modulePrdId">
						<input type="hidden" id="modulePrdImg" name="img">
						<input type="hidden" id="modulePrdAngleIcon" name="angleIcon">
						<div class="subAdmin_popup_tag">添加版块商品</div>
						<div
							class="add_link_con clearfix subject_admin_com subject_admin_check">
							<div class="add_link_l fl">
								<div class="add_link_img">
									<img id="prodImg" src="">
								</div>
								<a href="javascript:void(0);">上传图片<input type='file'
									name="imgFile" id="imgFile" /></a>
							</div>
							<div class="add_link_r fl">
								<ul>
									<li>商品编号：<input class="readonly" type="text"
										readonly="readonly" id="prodId" name="prodId" /></li>
									<li>商品名称：<input type="text" class="name" id="prodName"
										name="prodName" /></li>
									<li>商品价格：<input type="text" class="price readonly"
										id="prodCash" readonly="readonly" /></li>
									<!-- <li><input type="checkbox" id="isSoldOut" name="isSoldOut"
										value="1" />标注为已售罄<a href="javascript:void(0);"> 上传角标图片<input
											type='file' name="angleIconFile" id="angleIconFile" /></a> <img
										id="angleIconImg" style="width: 40px;" alt="" title="角标图片"
										src=""> <input id="delIconBtn" onclick="delAngleIcon();"
										style="display: none;" type="button" value="删除角标图片" /></li> -->
								</ul>
							</div>
							<div class="add_link_but">
								<a href="javascript:saveModuleProd();" class="save criteria-btn">保存</a><a
									href="javascript:void(0);" class="cancel">取消</a>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</div>
</body>

<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/spectrum.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/checkImage.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/theme.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/common/js/jquery-migrate.min.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
<script language="javascript">
var contextPath = '${contextPath}';
var photoPrex = '<ls:photo item=""/>';
var moduleCount = 1;
var currIndex = 1;
var isTitleShow = "${theme.isTitleShow}";
var isIntroShow = "${theme.isIntroShow}";
var isIntroMShow = "${theme.isIntroMShow}";
var path = '${contextPath}';
var pcDomainName = '${pcDomainName}';
var cookieValue = "${cookie.SESSION.value}";
var _curPageNO = "${curPageNO}";
	function loadSelectTheme(themeId,index,type){
		layer.open({
			title: '选择专题',
			id: 'selectShop',
			type: 2,
			content: "${contextPath}/admin/theme/loadSelectTheme?themeId="+themeId+"&index="+index+"&type="+type,
			area: ['700px', '450px']
		});

	}
	function addThemeTo(title,themeId,index,type) {
		var str ='<a class="addTheme'+type+index+'" target="_blank" href="${contextPath}/admin/theme/preview/'+themeId+'" style="color:blue" >'+title+'</a>';
		if ($(".addTheme"+type+index)){
			$(".addTheme"+type+index).remove()
		}
		if (type=="pc") {
			$("#relatedThemeId"+index).val(themeId);
			$(".relatedThemeId"+index).after(str);
		}
		if (type=="mobile") {
			$("#mRelatedThemeId"+index).val(themeId);
			$(".mRelatedThemeId"+index).after(str);
		}


		// $(".loadSelectTheme"+index).text("重新选择");

		// alert("title:"+title);
		// alert($("#relatedId"+index).val());
	}
	 laydate.render({
   		 elem: '#startTime',
   		 type:'datetime',
   		 calendar: true,
   		 theme: 'grid',
	 	 trigger: 'click'
   	  });
   	   
   	  laydate.render({
   	     elem: '#endTime',
   	     type:'datetime',
   	     calendar: true,
   	     theme: 'grid',
	 	 trigger: 'click'
      });
   	  $("input:radio[name='templateType']").change(function(){
 		 $("input:radio[name='templateType']").each(function(){
 			 if(this.checked){
 				 $(this).parent().parent().addClass("radio-wrapper-checked");
 			 }else{
 				 $(this).parent().parent().removeClass("radio-wrapper-checked");
 			 }
 	 	});
 	 });

   	  
</script>
</html>