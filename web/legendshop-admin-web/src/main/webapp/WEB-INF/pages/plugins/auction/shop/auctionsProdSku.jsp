<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<style type="text/css">
		html{font-size:13px;}
		.skulist{border:solid #d5e4fa; border-width:1px 0px 0px 1px;width: 100%;}
		.skulist tr td{border:solid #d5e4fa; border-width:0px 1px 1px 0px; padding:10px 0px;text-align: center;}
	</style>
				<table class="skulist">
					<c:if test="${empty skuList}">
						<c:if test="${not empty prod}">
							<tr >
								<td colspan="4"><b>商品信息</b></td>
							</tr>
							<tr>
								<td>商品名称</td>
								<td>商品价格</td>
								<td>库存数量</td>
								<td>操作</td>
								
							</tr>
								<tr>
									<td>${prod.name}</td>
									<td>${prod.price}</td>
									<td>${prod.stocks}</td>
									<td><input type="button" value="选择" onclick="addProd('','${prod.name}','${prod.prodId}');"/></td>
								</tr>
						</c:if>
					</c:if>
					<c:if test="${not empty skuList}">
							<tr >
								<td colspan="5"><b>商品信息</b></td>
							</tr>
						<tr>
							<td>sku名称</td>
							<td>属性</td>
							<td>sku价格</td>
							<td>sku库存数量</td>
							<td>操作</td>
						</tr>
						<c:forEach items="${skuList}" var="sku" varStatus="status">
							<tr>
								<td>${sku.name}</td>
								<td>${sku.property}</td>
								<td>${sku.price}</td>
								<td>${sku.actualStocks}</td>
								<td>
									<c:choose>
										<c:when test="${ sku.actualStocks le 0}">
											<font color="red"><b>库存不足</b></font>
										</c:when>
										<c:otherwise>
											<input type="button" value="选择" onclick="parent.addProd('${sku.id}','${sku.name}','${sku.prodId}');"/>
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</c:forEach>
					</c:if>
				</table>
