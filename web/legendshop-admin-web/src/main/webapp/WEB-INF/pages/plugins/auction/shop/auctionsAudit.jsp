<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<jsp:useBean id="now" class="java.util.Date" />
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>营销管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<style>
.prod-table{
			width:85%;
			margin:10px 0;
			border:solid #efefef;
			border-width:1px 0px 0px 1px;
		}
		.prod-table tr td{
			margin:0px;
			border:solid #efefef;
		}
		.prod-table thead tr td{
			text-align: center;
			background-color:#F9F9F9;
			padding:8px;
			font-size:14px;
			height:40px;
			border-width:0px 0px 1px 0px;
		}
		.prod-table thead tr td:last-child{
			border-right:1px solid #efefef;
		}
		.prod-table tbody tr td{
			padding:8px 0px 8px 0px;
			font-size:14px;
			border-width:0px 1px 1px 0px;
		}
		.prod-table tbody tr td a{
			overflow: hidden;
			text-overflow: ellipsis;
			white-space: nowrap;
			width: 340px;
			display: inline-block;
		}
.auditAuctionImage img {
	width: 100%;
}

#col1 {
	table-layout: fixed;
}

#col1 tr td:first-child, #infoTable tr td:first-child {
	width: 20%;
}
</style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table id="infoTable" class="${tableclass}" style="width: 100%">
				<thead>
					<tr>
						<th class="title-border">
							拍卖管理&nbsp;＞&nbsp;
							<a href="<ls:url address="/admin/auction/query"/>">拍卖活动管理</a>&nbsp;＞&nbsp;
							<c:choose>
								<c:when test="${bean.status == -1&& bean.endTime>now}">
									<span style="color:#0e90d2; font-size:14px;">审核拍卖活动</span>
								</c:when>
								<c:otherwise><span style="color:#0e90d2; font-size:14px;">查看拍卖活动</span></c:otherwise>
							</c:choose>
						</th>
					</tr>
				</thead>
			</table>
			<div align="center">
				<table style="width: 100%" class="${tableclass} content-table no-border" id="col1">

					<tr>
						<td style="width: 16%;">
							<div align="right">活动名称:</div>
						</td>
						<td>${bean.auctionsTitle}</td>

					</tr>

					<tr>
						<td valign="top">
							<div align="right">拍卖商品:</div>
						</td>
						<td valign="top">
				        	<table class="prod-table content-table see-able" style="padding-top:0;margin-top:0;">
				        		<thead>
					        		<tr>
					        			<td width="10%">单品图片</td>
					        			<td width="50%">单品名称</td>
					        			<td width="15%">单品属性</td>
					        			<td width="15%">价格(元)</td>
					        		</tr>
				        		</thead>
				        		<tbody>
					        		<tr>
					        			<td id="skuPic" align="center"><img src="<ls:images item='${prod.prodPic}' scale='3'/>" alt="单品图片"/></td>
					        			<td align="center"><a href="${PC_DOMAIN_NAME}/views/${bean.prodId}" target="_blank">${prod.prodName}</a></td>
					        			<td id="cnProperties" align="center">${empty prod.cnProperties?'无':prod.cnProperties}</td>
					        			<td id="skuPrice" align="center">${prod.prodPrice}</td>
					        		</tr>
				        		</tbody>
				        	</table>
				        </td>
					</tr>
					<tr>
						<td>
							<div align="right">开始时间:</div>
						</td>
						<td><fmt:formatDate value="${bean.startTime}"
								pattern="yyyy-MM-dd HH:mm:ss" /></td>
					</tr>
					<tr>
						<td>
							<div align="right">结束时间:</div>
						</td>
						<td><fmt:formatDate value="${bean.endTime}"
								pattern="yyyy-MM-dd HH:mm:ss" /></td>
					</tr>
					<tr>
						<td>
							<div align="right">起拍价:</div>
						</td>
						<td>${bean.floorPrice}<span style="margin-left: 5px;">元</span></td>
					</tr>
					<c:choose>
						<c:when test="${bean.isCeiling eq 0}">
							<tr>
								<td>
									<div align="right">是否封顶:</div>
								</td>
								<td>不封顶</td>
							</tr>
						</c:when>
						<c:otherwise>
							<tr>
								<td>
									<div align="right">是否封顶:</div>
								</td>
								<td>封顶</td>
							</tr>
							<tr>
								<td>
									<div align="right">一口价:</div>
								</td>
								<td>${bean.fixedPrice}</td>
							</tr>
						</c:otherwise>

					</c:choose>
					<tr>
						<td>
							<div align="right">最小加价幅度:</div>
						</td>
						<td>${bean.minMarkupRange}<span style="margin-left: 5px;">元</span></td>
					</tr>
					<tr>
						<td>
							<div align="right">最高加价幅度:</div>
						</td>
						<td>${bean.maxMarkupRange}<span style="margin-left: 5px;">元</span></td>
					</tr>
					<c:choose>
						<c:when test="${bean.isSecurity eq 0}">
							<tr>
								<td>
									<div align="right">
										<font color="ff0000">*</font>是否支付保证金:
									</div>
								</td>
								<td>不支付保证金</td>
							</tr>
						</c:when>
						<c:otherwise>
							<tr>
								<td>
									<div align="right">
										<font color="ff0000">*</font>是否支付保证金:
									</div>
								</td>
								<td>支付保证金</td>
							</tr>
							<tr>
								<td>
									<div align="right">保证金:</div>
								</td>
								<td>${bean.securityPrice}<span style="margin-left: 5px;">元</span></td>
							</tr>
							<tr>
								<td>
									<div align="right">延长周期:</div>
								</td>
								<td>${bean.delayTime} <span style="margin-left: 5px;">分钟</span></td>
							</tr>
						</c:otherwise>
					</c:choose>
					<tr>
						<td>
							<div align="right">是否隐藏起拍价:</div>
						</td>
						<td>
							<div>
								<c:choose>
									<c:when test="${bean.hideFloorPrice eq 0}">不隐藏</c:when>
									<c:otherwise>隐藏</c:otherwise>
								</c:choose>
							</div>
						</td>
					</tr>
					<tr>
						<td style="min-width: 250px;">
							<div align="right">拍卖描述:</div>
						</td>
						<td class="auditAuctionImage"
							style="word-wrap: break-word; word-break: break-all; width: 1450;">
							${bean.auctionsDesc}</td>
					</tr>
					<tr>
						<td colspan="2">
							<div align="center">
								<c:if test="${bean.status == -1&& bean.endTime>now}">
									<input type="button" class="${btnclass}" id="btn_audit"
										onclick="audit('${bean.id}')" value="审核" />
								</c:if>
								<input type="button" class="${btnclass}" value="返回"
									onclick="window.location='${contextPath}/admin/auction/query'" />
							</div>
						</td>
					</tr>
				</table>
               <div id="auditFrame" style="display: none;">
					<div  style="font-size: 0.8em; padding: 10px;">
					<div style="margin-right:190px;">
					<label>审核结果</label><font color="red">*</font>： <select id="auditResult">
								<option value="">请选择</option>
								<option value="1">同意</option>
								<option value="0">不同意</option>
							</select>
							</div>
							</br>
							<div>
						<label>审核意见</label><font color="red">*</font>：
						<textarea id="auditOpinion"
							style="resize: none; width: 250px; height: 80px;" maxlength="50"
							placeholder="审核意见（50字符以内）"></textarea>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

<script language="javascript">
	var contextPath = '${contextPath}';
	var content = "";
	jQuery(document).ready(
			function() {
				$('.auditAuctionImage img').attr("style","width: 480px;height: 360px");
			});

	//开始审核
	function auditmethod(id, auditOpinion, flag) {
		if (flag == null || flag == "" || flag == undefined) {
			layer.msg("审核结果不能为空", {icon:0});
			return false;
		}
		if (auditOpinion == null || auditOpinion == "" || auditOpinion == undefined) {
			layer.msg("审核意见不能为空", {icon:0});
			return false;
		}
		$.ajax({
			url : contextPath + "/admin/auction/audit",
			data : {
				"id" : id,
				"auditText" : auditOpinion,
				"flag" : flag
			},
			type : 'post',
			async : true, //默认为true 异步
			dataType : 'json',
			error : function(data) {
				layer.alert("网络异常,请稍后重试！",{icon:2});
			},
			success : function(data) {
				if (data == "OK") {
					layer.msg("审核成功", {icon:1, time:700}, function(){
						window.location = '${contextPath}/admin/auction/query'
					});
					return true;
				} else {
					layer.msg(data, {icon:0});
					return false;
				}
			}
		});
	}
	//审核弹窗
	function audit(id) {
		layer.open({
			  title :"审核意见",
			  id: "audit",
			  content: $('#auditFrame').html(),
			  type: 1,
		      btn: ['确定', '取消'],
		      area: ['350px', '250px'],
			  yes: function(index, layero){
				    var flag = "";
					var result = $(layero).find('#auditResult').val();
					if (result == "1") {
						//通过
						flag = "ok";
					} else if (result == "0") {
						//不通过
						flag = "no";
					}
					if (!auditmethod(id,$(layero).find('#auditOpinion').val(), flag))
					return false;
			  }
		});
	}
</script>
</html>
