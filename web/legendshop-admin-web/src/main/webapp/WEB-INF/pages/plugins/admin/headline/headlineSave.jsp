<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>头部管理 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
		 <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.css'/>" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
	    <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
        <form:form action="${contextPath}/admin/headlineAdmin/save" method="post" id="form1" enctype="multipart/form-data" onSubmit="return check();">
            <div align="center">
            <table class="${tableclass}">
                <thead>
                    <tr class="sortable">
                           <div align="center">
							<thead>
								<tr>
									<th class="no-bg title-th">
										<span class="title-span">
											新闻管理  ＞
											<a href="<ls:url address="/admin/headlineAdmin/query"/>">头条新闻管理</a>
											  ＞  <span style="color:#0e90d2;"> 新建头条</span>
										</span>
									</th>
								</tr>
							</thead>
						</div>
                    </tr>
                </thead>
      	</table>
      	<table class="${tableclass} no-border content-table" id="col1" style="width:100%">        
			<tr>
		        <td style="width:200px;">
		          	<div align="right"><font color="ff0000">*</font>头条名称：</div>
		       	</td> 
		        <td align="left" style="padding-left: 2px;">
		           	<input type="text" name="title" id="prodName"  value="${headline.title }" maxlength="30"/>
		        </td>
			</tr>
		<tr>
		        <td align="right" valign="top">
		          	<font color="ff0000">*</font>图片：
		       	</td>
		        <td align="left" valign="top" style="margin-top:10px;padding-left: 2px;">
		           	<input style="margin-bottom: 8px;" type="file" name="imageFile" id="prodImg" />
		        	<div><img style="display:none;" id="ImgPr" width="120" height="120" /></div>
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="right" onclick="checkval();"><font color="ff0000">*</font>头条顺序：</div>
		       	</td>
		        <td align="left" style="padding-left: 2px;">
		           	<input type="text" name="seq" id="prodCash" value="${headline.seq}" maxlength="5"/>
		        </td>
		</tr>
		<tr>
		        <td align="right" valign="top">
		          	<font color="ff0000">*</font>	头条描述：
		       	</td>
		        <td align="left" style="padding-left: 2px;">
		           	 <textarea name="detail" id="msg" cols="100" rows="8" style="width:100%;height:400px;visibility:hidden;" maxlength="65000">${headline.detail}</textarea> 
		        </td>
		</tr>
                <tr>
                    <td colspan="2">
                        <div align="center">
                            <input type="submit" class="criteria-btn" value="保存" />
                            <input type="button" class="criteria-btn" value="返回"
                                onclick="window.location='<ls:url address="/admin/headlineAdmin/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
        </div>
        </div>
        </body>
     <script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
		 <script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
		 <script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
         <script type='text/javascript' src="${contextPath}/resources/common/js/jquery.js"></script>
         <script type="text/javascript" src="<ls:templateResource item='/resources/common/js/checkImage.js'/>"></script>
         <script type='text/javascript' src="${contextPath}/resources/common/js/jquery.validate.js" /></script>
    <script type="text/javascript">
    	function check(){
    		var name=$("#prodName").val();
    		var pic=$("#prodImg").attr("value");
    		var detail=$("#msg").val();
    		var seq=$("#prodCash").val();
    		var reg = /^[1-9]\d*$/;
    		if(name.length<=0){
    			layer.msg("名称不能为空", {icon:2});
    			return false;
    		}
    		if(name.length>200){
    			layer.msg("名称字数不能大于200", {icon:2});
    			return false;
    		}
    		if(pic<=0){
    			layer.msg("请选择图片", {icon:2});
    			return false;
    		}if(seq.length<=0){
    			layer.msg("顺序不能为空", {icon:2});
    			return false;
    		}if(!reg.test(seq)){
    			layer.msg("顺序必须为正整数 ", {icon:2});
    			return false;
    		}
    		if(detail.length<=0){
    			lsyer.mag("描述不能为空", {icon:2});
    			return false;
    		}
    		return true;
    	}
    	
    	$(function(){
    		$("#prodImg").mouseout(function(){
    			if($("#prodImg").val()){
    				$("#ImgPr").attr("style","display:block");
    			}
    		});
    		
    		$("#prodImg").uploadPreview({
    			Img : "ImgPr",
    			Width : 120,
    			Height : 120,
    			ImgType : [ "gif", "jpeg", "jpg", "bmp", "png" ],
    			Callback : function() {
    			}
    		});
    		
    		KindEditor.options.filterMode=false;
    		KindEditor.create('textarea[name="detail"]', {
				cssPath : '${contextPath}/resources/plugins/kindeditor/plugins/code/prettify.css',
				uploadJson : '${contextPath}/editor/uploadJson/upload;jsessionid=${cookie.SESSION.value}',
				fileManagerJson : '${contextPath}/editor/uploadJson/fileManager',
				allowFileManager : true,
				afterBlur:function(){this.sync();},
				width : '90%',
				height:'400px',
				afterCreate : function() {
					var self = this;
					KindEditor.ctrl(document, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
					KindEditor.ctrl(self.edit.doc, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
				}
			});
    	})
    	
    	$(function(){
    		$("input[id=prodImg]").change(function(){
    			checkImgType(this);
    			checkImgSize(this,1024);    			
    		});
    	 });
    	
    	/*显示上传图片 */
    	jQuery.fn.extend({
		uploadPreview : function(opts) {
			var _self = this, _this = $(this);
			opts = jQuery.extend({
				Img : "ImgPr",
				Width : 100,
				Height : 100,
				ImgType : [ "gif", "jpeg", "jpg", "bmp", "png" ],
				Callback : function() {
				}
			}, opts || {});
			_self.getObjectURL = function(file) {
				var url = null;
				if (window.createObjectURL != undefined) {
					url = window.createObjectURL(file)
				} else if (window.URL != undefined) {
					url = window.URL.createObjectURL(file)
				} else if (window.webkitURL != undefined) {
					url = window.webkitURL.createObjectURL(file)
				}
				return url
			};
			_this.change(function() {

				if (this.value) {
					if (!RegExp("\.(" + opts.ImgType.join("|") + ")$", "i").test(this.value.toLowerCase())) {
						layer.alert("选择文件错误,图片类型必须是" + opts.ImgType.join("，") + "中的一种", {icon:2});
						this.value = "";
						$("#" + opts.Img).attr('src', "");
						return false
					}else if(!checkImgSize(this,1024)){
						this.value = "";
						$("#" + opts.Img).attr('src', "");
						return false;
					}else {
						$("#" + opts.Img).attr('src', _self.getObjectURL(this.files[0]))
					}
					opts.Callback()
				}
			})
		}
	});
    </script>
</html>
