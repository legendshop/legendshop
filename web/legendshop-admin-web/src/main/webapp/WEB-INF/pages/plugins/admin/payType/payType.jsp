<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>支付编辑 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
	    <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
<form:form  action="${contextPath}/admin/paytype/save" method="post" id="form1" enctype="multipart/form-data">>
		 <input id="payId" name="payId" value="${bean.payId}" type="hidden">
         <input id="payParams" name="paymentConfig"  type="hidden">
		<div align="center">
			<table class="${tableclass}" style="width: 100%;">
				<thead>
					<tr><th> <strong class="am-text-primary am-text-lg">商城管理</strong> /  支付编辑</th></tr>
				</thead>
			</table>
			<table align="center" class="${tableclass}" style="margin: 0px;border-bottom: 0px;" id="col1">
				<tr>
					<td style="width: 265px;">
						<div align="center">
							支付方式: <font color="ff0000">*</font>
						</div>
					</td>
					<td><input type="text" readonly="readonly" class="${inputclass}"
						value="${bean.payTypeName}" size="50" maxlength="100" /> 
						
					</td>
				</tr>

				<tr>
					<td style="width: 265px;">
						<div align="center">备注:</div>
					</td>
					<td><input type="text" name="memo" id="memo" class="${inputclass}"
						value="${bean.memo}" size="50"  maxlength="100" />
					</td>
				</tr>
				
				<tr>
					<td style="width: 265px;">
						<div align="center">是否启用:</div>
					</td>
					<td>
					  <input type="radio" name="isEnable"  <c:if test="${bean.isEnable==1}">checked="checked"</c:if>  value="1" />&nbsp;是
					   <input type="radio" name="isEnable"  <c:if test="${bean.isEnable==0}">checked="checked"</c:if>  value="0" />&nbsp;否
					</td>
				</tr>
				
				
			</table>
			<table align="center" style="display: none;margin: 0px;border-top: 0px;" class="${tableclass}" id="col2">
				<tbody>
                
				</tbody>
			</table>
			
			
			
            <table align="center"class="its"> 
                <tr>
                    <td colspan="2">
                        <div align="center">
                            <input type="submit" value="保存" class="${btnclass}" />
                            <input type="button" value="返回" class="${btnclass}"
                                onclick="window.location='${contextPath}/admin/paytype/query'" />
                        </div>
                    </td>
                </tr>
           </table> 
           
           
			<table style="width: 100%; border: 0px;margin: 10px">
				<tr>
					<td align="left">说明：<br>
						如果选择货到付款方式，则无需填写合作身份者ID，安全检验码和签约账号<br>
					</td>
				<tr>
			</table>
		</div>
</form:form>
</div>
</div>
</body>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/common/js/map.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/payType.js'/>"></script>
<script type="text/javascript">
var contextPath = "${contextPath}";
    $.validator.setDefaults({
    
    });
</script>
</html>
