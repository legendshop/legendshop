<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
 <%
        Integer offset = (Integer) request.getAttribute("offset");
    %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	    <table class="${tableclass} title-border" style="width: 100%">
		    	<tr>
			    	<th class="title-border">用户管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/system/userDetail/query"/>">用户信息管理</a>
			             &nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">基本资料</span>
			       </th>
		        </tr>
	    </table>
    <div class="user_list">
        <div class="seller_list_title">
          <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
            <li  ><i></i><a href="<ls:url address="/admin/userinfo/userDetail/${userId}"/>">基本资料</a></li>
            <li ><i></i><a href="<ls:url address="/admin/userinfo/userOrderInfo/${userName}?userId=${userId}"/>">会员订单</a></li>
            <li ><i></i><a href="<ls:url address="/admin/userinfo/expensesRecord/${userId}?userName=${userName}" />">会员消费记录</a></li>
           <li class=" am-active"><i></i><a href="">会员浏览历史</a></li>
           <li ><i></i><a href="<ls:url address="/admin/userinfo/userProdComm/${userId}?userName=${userName}"/>">会员商品评论</a></li>
            <ls:plugin pluginId="integral">
		        <li ><i></i><a href="<ls:url address="/admin/userIntegral/userIntegralDetail/${userId}?userName=${userName}"/>">会员积分明细</a></li>
		     </ls:plugin>
		     <ls:plugin pluginId="predeposit">
		        <li ><i></i><a href="<ls:url address="/admin/preDeposit/userPdCashLog/${userName}?userId=${userId}"/>">会员预存款明细</a></li>
		     </ls:plugin>
		     <li ><i></i><a href="<ls:url address="/admin/userinfo/userProdCons/${userName}?userId=${userId}"/>">会员商品咨询</a></li>
          </ul>
        </div>
      </div>
    <form:form  action="${contextPath}/admin/userinfo/userVisitLog/${userName}?userId=${userId}" id="form1" method="post">
		<div class="criteria-div">
	        <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
	        <span class="item">
	        	   开始&nbsp;
				 <input readonly="readonly"  name="startTime" id="startTime" class="Wdate" type="text"  value='<fmt:formatDate value="${bean.startTime}" pattern="yyyy-MM-dd"/>' />
			</span>
			<span class="item">
				   结束&nbsp;
				 <input readonly="readonly" name="endTime" id="endTime" class="Wdate" type="text"  value='<fmt:formatDate value="${bean.endTime}" pattern="yyyy-MM-dd"/>' />
		    </span>
		    <span class="item">
	                                     页面&nbsp;
	                <select id="page" name="page" class='criteria-select'>
					  <ls:optionGroup type="select" required="false" cache="true"
		                beanName="VISIT_LOG_TYPE" selectedValue="${bean.page}"/>
		            </select>
		    </span>
		    <span class="item">
		                           商品&nbsp;
		        <input type="text" name="productName" maxlength="50" value="${bean.productName}" class="${inputclass}" />
	            <input type="submit" value="搜索" class="${btnclass}" />
	        </span>
	     </div>
    </form:form>
    <div align="center" class="order-content-list">
        <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
    <display:table name="list" requestURI="#" id="item"
         export="false"  class="${tableclass}" style="width:100%" sort="external">
      <display:column title="顺序"  class="orderwidth"><%=offset++%></display:column>
      <display:column title="IP" property="ip"  sortName="ip"></display:column>
      <display:column title="国家" property="country"  sortName="country"></display:column>
       <display:column title="地区" property="area" sortName="area"></display:column>
      <display:column title="商品名" >
      	<c:if test="${item.productName != null}">
      		<a href="${PC_DOMAIN_NAME}/views/${item.productId}"  target="_blank">${item.productName}</a>
      	</c:if>
      </display:column>
      <display:column title="页面">
      	<c:if test="${item.page == 0}">首页</c:if>
      	<c:if test="${item.page == 1}">商品</c:if>
      </display:column>
      <display:column title="日期">
      	<fmt:formatDate value="${item.date}" pattern="yyyy-MM-dd HH:mm"/>
      </display:column>
      <display:column title="访问次数" property="visitNum">
      </display:column>
    </display:table>
    	<c:if test="${empty list}">
         	<div style="background:#f9f9f9;margin-top:20px;padding:20px;border:1px solid #efefef;height:60px;width:99%;">未找到相应记录~</div>
         </c:if> 
       <div class="clearfix" style="margin-bottom: 60px;">
       		<div class="fr">
       			 <div class="page">
	       			 <div class="p-wrap">
	           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	    			 </div>
       			 </div>
       		</div>
       	</div>
    </div>
    </div>
    </div>
    </body>
     <script language="JavaScript" type="text/javascript">
        function pager(curPageNO){
            document.getElementById("curPageNO").value=curPageNO;
            document.getElementById("form1").submit();
        }
         highlightTableRows("item"); 
         
         laydate.render({
	   		 elem: '#startTime',
	   		 calendar: true,
	   		 theme: 'grid',
		 	 trigger: 'click'
	   	  });
	   	   
	   	  laydate.render({
	   	     elem: '#endTime',
	   	     calendar: true,
	   	     theme: 'grid',
		 	 trigger: 'click'
	      });
	</script>
	</html>


