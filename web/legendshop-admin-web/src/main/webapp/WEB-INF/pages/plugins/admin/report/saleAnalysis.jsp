<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%
	Integer offset = 1;
%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>报表管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<style>
	.am-table-bordered > thead > tr > th{
		border:none !important;
		background: #f5f5f5;
	}
	.am-table-bordered > thead > tr > th:first-child{
		border-left:1px solid #ddd !important;
	}
</style>
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">报表管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">销售分析</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/report/saleanalysis"
				id="form1" method="get">
				<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}">
					<div class="criteria-div">
					<span class="item">
						开始日期：<input readonly="readonly" name="startDate" id="startDate" class="Wdate" type="text"
							value='<fmt:formatDate value="${saleAnalysisDto.startDate}" pattern="yyyy-MM-dd"/>' placeholder="开始日期"/>
					</span>
					<span class="item">
						结束日期：<input readonly="readonly" name="endDate" id="endDate" class="Wdate" type="text" 
							value='<fmt:formatDate value="${saleAnalysisDto.endDate}" pattern="yyyy-MM-dd"/>' placeholder="结束日期"/>
						<input type="button" onclick="search()" class="${btnclass}" value="搜索" style="margin-left: 6px;"/>
					</span>
					</div>
			</form:form>
		<div class="order-content-list">
			<table class="${tableclass}" style="width: 100%">
				<thead>
					<tr>
						<th>顺序</th>
						<th>名称</th>
						<th>列表</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><%=offset++%></td>
						<td>平均会员订单量</td>
						<td><display:table name="saleAnalysis.averageMemberOrders"
								id="orders" cellpadding="0" cellspacing="0"
								class="am-table am-table-bordered" style="min-width:490px;">
								<display:column title="总会员数（只统计有效用户）" property="totalMember"></display:column>
								<display:column title="总订单数" style="width:80px"
									property="totalOrder"></display:column>
								<display:column title="平均会员订单量" style="width:160px">
									<fmt:formatNumber 
										value="${orders.averageOrders}" />
								</display:column>
							</display:table></td>
					</tr>
					<tr>
						<td><%=offset++%></td>
						<td>客户订单转化率</td>
						<td><display:table name="saleAnalysis.averageOrderAmouont"
								id="amout" cellpadding="0" cellspacing="0"
								class="am-table am-table-bordered"  style="min-width:490px;">
								<display:column title="总访问次数 " property="totalViews"></display:column>
								<display:column title="总订单数" style="width:80px"
									property="totalOrder"></display:column>
								<display:column title="订单转化率" style="width:160px">
									<fmt:formatNumber type="percent" value="${amout.averageRate}" />
								</display:column>
							</display:table></td>
					</tr>
					<tr>
						<td><%=offset++%></td>
						<td>客户平均订单金额</td>
						<td><display:table name="saleAnalysis.ordersConversionRate"
								id="ordersConversionRate" cellpadding="0" cellspacing="0"
								class="am-table am-table-bordered" style="min-width:490px;">
								<display:column title="总订单金额 ">
									<fmt:formatNumber type="currency"
										value="${ordersConversionRate.totalAmount}"
										pattern="${CURRENCY_PATTERN}" />
								</display:column>
								<display:column title="总订单数" style="width:80px"
									property="totalOrder"></display:column>
								<display:column title="平均订单金额" style="width:160px">
									<fmt:formatNumber type="currency"
										value="${ordersConversionRate.averageAmout}"
										pattern="${CURRENCY_PATTERN}" />
								</display:column>
							</display:table></td>
					</tr>
					<tr>
						<td><%=offset++%></td>
						<td>注册会员购买率</td>
						<td><display:table name="saleAnalysis.registeredPurchaseRate"
								id="registeredPurchaseRate" cellpadding="0" cellspacing="0"
								class="am-table am-table-bordered" style="min-width:490px;">
								<display:column title="总会员数 （只统计有效用户）" property="totalMember"></display:column>
								<display:column title="有过订单的会员数" style="width:80px"
									property="hasOrder"></display:column>
								<display:column title="注册会员购买率" style="width:160px">
									<fmt:formatNumber type="percent"
										value="${registeredPurchaseRate.averageRate}" />
								</display:column>
							</display:table></td>
					</tr>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</body>
<script>
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#form1")[0].submit();
	}
	
	 laydate.render({
   		 elem: '#startDate',
   		 calendar: true,
   		 theme: 'grid',
	 	 trigger: 'click'
   	  });
   	   
   	  laydate.render({
   	     elem: '#endDate',
   	     calendar: true,
   	     theme: 'grid',
	 	 trigger: 'click'
      });
</script>
</html>