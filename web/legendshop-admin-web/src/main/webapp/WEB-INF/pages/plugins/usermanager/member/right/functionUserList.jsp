<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	int offset = ((Integer) request.getAttribute("offset")).intValue();
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>权限管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">权限管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/member/right/query"/>">权限管理</a>
						&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">权限角色</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/member/right/loadUserByFunc"
				id="form1" method="post">
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="<%=request.getAttribute("curPageNO")%>"> 
					<input type="hidden" id="funcId" name="funcId" value="<%=request.getAttribute("funcId")%>"> 
					<span class="item">
						 昵称&nbsp;
					 </span>
					<span class="item">
						<input class="${inputclass}" type="text" name="userName" maxlength="50" value="${userName}" /> 
						<input class="${btnclass}" type="submit" value="搜索" />&nbsp;&nbsp;
					</span>
				</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="#" id="item" export="false"
					class="${tableclass}" style="width:100%">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="用户名 " property="name" sortable="true"></display:column>
					<display:column title="昵称 " property="nickName" sortable="true"></display:column>
					<display:column title="状态">
						<ls:optionGroup type="label" required="true" cache="true"
							beanName="ENABLED" selectedValue="${item.enabled}" defaultDisp="" />
					</display:column>
					<display:column title="备注" property="note"></display:column>

				</display:table>
				<div class="clearfix">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
				<input style="margin-top: 60px;" class="${btnclass}" type="button" value="返回"
					onclick="javascript:toRightQueryPage()" />
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	function pager(curPageNO) {
		//直接下一页
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}

	function toRightQueryPage() {
		window.location.href = '${contextPath}/admin/member/right/query';
	}
</script>
</html>
