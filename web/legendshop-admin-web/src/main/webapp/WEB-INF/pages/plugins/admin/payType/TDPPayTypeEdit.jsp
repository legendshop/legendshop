<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="../back-common.jsp"%>
<%
	Integer offset = (Integer)request.getAttribute("offset");
%>
 <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/templets/amaze/css/payEdit.css'/>" />
 <%@ include file="/WEB-INF/pages/common/jquery2.1.4.jsp"%>

 <form:form  method="post" id="payForm" style="max-height: 650px;height:330px; overflow: auto;">
	<input type="hidden" name="isEnable" id="isEnable" value="${payType.isEnable}" />
	<div class="modal-body-main filll fillr">
		<div style="padding-top:20px;">
			<div class="ant-row ant-form-item">
				<div class="ant-col-7 ant-form-item-label">
					<label class="">支付类型</label>
				</div>
				<div class="ant-col-13">
					<div class="ant-form-item-control ">
						<span>财付通支付</span>
					</div>
				</div>
			</div>
			<div class="ant-row ant-form-item">
				<div class="ant-col-7 ant-form-item-label">
					<label for="MCH_ID" class="ant-form-item-required">商户密钥</label>
				</div>
				<div class="ant-col-13">
					<div class="ant-form-item-control ">
						<input type="text" id="key" name="key" value="${jsonObject['key']}" 
							class="ant-input ant-input-lg">
					</div>
				</div>
			</div>
		
			<div class="ant-row ant-form-item">
				<div class="ant-col-7 ant-form-item-label">
					<label for="PARTNERKEY" class="ant-form-item-required">bargainorId(商家的商户号)</label>
				</div>
				<div class="ant-col-13">
					<div class="ant-form-item-control ">
						<input type="password" id="bargainorId" name="bargainorId" value="${jsonObject['bargainorId']}" 
							class="ant-input ant-input-lg">
					</div>
				</div>
			</div>
			
		</div>
		
		<div class="ant-row ant-form-item">
				<div class="ant-col-7 ant-form-item-label">
					<label for="accountState" class="">状态</label>
				</div>
				<div class="ant-col-13">
					<div class="ant-form-item-control has-success">
						<div class="ant-radio-group ant-radio-group-large">
							<label class="ant-radio-wrapper"><span
								class="ant-radio <c:if test="${empty payType.isEnable || payType.isEnable eq 0}"> ant-radio-checked</c:if>">
								
								<input type="radio"
									class="ant-radio-input ant-radio-isEnable" value="0"  <c:if test="${empty payType.isEnable || payType.isEnable eq 0}"> checked="checked"</c:if>  ><span
									class="ant-radio-inner"></span> </span><span>关闭</span> 
						   </label>
									
							<label
								class="ant-radio-wrapper"><span class="ant-radio <c:if test="${payType.isEnable eq 1}"> ant-radio-checked</c:if>">
								<input
									type="radio" class="ant-radio-input ant-radio-isEnable" value="1"  <c:if test="${payType.isEnable eq 1}"> checked="checked"</c:if> >
									<span
									class="ant-radio-inner"></span> </span><span>启用</span>
							</label>
									
									
						</div>
					</div>
				</div>
			</div>

		<div data-show="true"
			class="ant-alert ant-alert-info ant-alert-no-icon">
			<span class="ant-alert-message"><div>
					<p>注:</p>
					<p>1、设置账号前，请确保您已在财付通平台完成相关设置。</p>
					<p>2、启用后微信端可使用财付通支付进行付款。</p>
				</div> </span>
				<span class="ant-alert-description"></span>
		</div>
		<input type="submit" id="editBtn" value = "提交更改" class="criteria-btn" />
	</div>

</form:form>


<script src="${contextPath}/resources/common/js/jquery.validate.js"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/TDPPayTypeEdit.js'/>"></script>
<script type="text/javascript">
	var contextPath = "${contextPath}";
</script>
</body>
</html>