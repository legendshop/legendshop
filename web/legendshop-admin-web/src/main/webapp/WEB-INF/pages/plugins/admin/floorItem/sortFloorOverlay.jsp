<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<html>
<head>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/floorTemplate.css" />
<title>编辑分类</title>
</head>
<body>
  <div class="box_floor">
    <!--分类开始-->
    <div class="box_floor_class">
      <b>楼层标题：</b>
      <%--<input name="floorName" type="text" id="floorName" value="" />--%>
	  <b>${floor.name}</b>
      <input name="layout" type="hidden" value="${layout}" />
    </div>
    <h6 class="box_floor_h6">当前分类(注释:双击删除已选分类，按下鼠标拖动分类次序)</h6>
    <%--<em class="floor_warning" style="margin-left:20px;margin-bottom:5px;"></em>--%>
    <div class="box_edits">
      <ul class="box_edit_class ui-sortable" id="ui-sort">
      <c:forEach items="${requestScope.floor.floorItems}" var="floorItem">
        <li class="box_edit_li" id="li-sortable">
          <h6><a id="${floorItem.fiId}" level="${floorItem.sortLevel }"  referId="${floorItem.referId}"  href="javascript:void(0);"  ondblclick="javascript:jQuery(this.parentNode.parentNode).remove();">${floorItem.sortName}</a></h6>
      	   <span id="ui-nsort"  class="ui-nsortable">
		           <c:forEach items="${floorItem.floorSubItemSet}" var="floorSubItem">
		           		<a id="${floorSubItem.fsiId}"  pid="${floorSubItem.fiId}"  level="${floorSubItem.seq}"  referId="${floorSubItem.subSortId}"   href="javascript:void(0);" ondblclick="javascript:jQuery(this).remove();">${floorSubItem.nsortName}</a>
		           </c:forEach>
           </span>
          </li>
        </c:forEach>
      </ul>
    </div>
    <div class="box_floor_class"><b>选择商品分类：</b>
		<lb:allSortOption id="sort" type="P" onChange="change(this.value)"/>
		<input name="按钮" type="button" value="保存" onClick="saveSortFloor();" class="edit-blue-btn" />
    </div>

  </div>
 
</body>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.core.js'/>"  type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.widget.js'/>"  type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.mouse.js'/>"  type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.sortable.js'/>"  type="text/javascript"></script>

<script type="text/javascript">
   if(typeof JSON == 'undefined'){$('head').append($("<script type='text/javascript' src='${contextPath}/resources/common/js/json.js'>"));}
	function change(v1){
		var url="${contextPath}/admin/floorItem/loadCategory/"+v1.substring(2);;
		/**  1是一级商品分类 **/
		sendData(url);
	}
	
	function sendData(url){
		$.ajax({
		"url":url, 
		type:'post', 
		async : true,  
		error: function(jqXHR, textStatus, errorThrown) {
		},
		success:function(result){
			 if(jQuery(".box_edit_class .box_edit_li").length>0){
			      jQuery(".box_edit_class .box_edit_li:last").after(result);
			   }else{
				  jQuery(".box_edit_class").append(result);
			   }
		}
		});
}

function saveSortFloor(){
    var floorMap = [];
   jQuery(".box_edit_li a").each(function(){
     var obj = new Object();
	     obj.pid = jQuery(this).attr("pid");
		 obj.id = jQuery(this).attr("id");
		 obj.level=jQuery(this).attr("level");
		 obj.referId=jQuery(this).attr("referId");
		 floorMap.push(obj);
    });
    if(floorMap.length==0){
       layer.alert("请选择分类信息", {icon:2});
       return;
    }
    var sortFloors = JSON.stringify(floorMap);
     $.ajax({
				url:"${contextPath}/admin/floorItem/saveSortFloor", 
				data: {"sortFloors":sortFloors, "flId":${floor.flId},"layout":$("input[name=layout]").val()},
				type:'post', 
				dataType : 'json', 
				async : true, //默认为true 异步   
				error: function(jqXHR, textStatus, errorThrown) {
			 		layer.alert(textStatus, errorThrown);
			 		//layer.close();
				},
				success:function(result){
					layer.msg(convertResopnse(result), {icon:1, time:1000}, function(){
						if(result=="PN"){
	//						alert('权限受限，不能修改别人的楼层!');
						}
						else if(result=="NN"){
	//						alert('数据异常，数据不能为空!');
						}
						else {
							parent.location.reload();
						}
					});
				}
	});         
}

function convertResopnse(result){

	var value = "保存成功";
	if("NN" == result){
		value = "数据异常，数据不能为空!";
	}else if("PN" == result){
		value = "权限受限，不能修改别人的楼层!";
	}	
	return value;

}

//拖拽效果	
jQuery(document).ready(function(){
	   jQuery("#ui-sort").sortable();
	   jQuery("#ui-sort").disableSelection();   
	   
	  jQuery(".ui-nsortable").each(function(){
		  jQuery(this).sortable();
		  jQuery(this).disableSelection();
	   });
	});
</script>
</html>