<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>提现管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">预存款管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">预存款管理</span></th>
				</tr>
			</table>
			<div>
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li><i></i><a href="<ls:url address="/admin/preDeposit/pdRechargeQuery"/>">充值管理</a></li>
						<li class="am-active"><i></i><a>提现管理</a></li>
						<li><i></i><a href="<ls:url address="/admin/preDeposit/pdCashLogQuery"/>">预存款明细</a></li>
					</ul>
				</div>
			</div>
			<form:form action="${contextPath}/admin/preDeposit/pdWithdrawCashQuery" id="form1" method="post">
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
					<span class="item">
						用户名：<input class="${inputclass}" type="text" name="userName" id="userName" maxlength="50" value="${bean.userName}" placeholder="用户名" /> 
					</span>
					<span class="item">
						用户昵称：<input class="${inputclass}" type="text" name="nickName" id="nickName" maxlength="50" value="${bean.nickName}" placeholder="用户昵称" />
					</span>
					<span class="item">
					 	状态：
						 <select id="paymentState"
							name="paymentState" class="${selectclass}">
							<ls:optionGroup type="select" required="false" cache="true"
								beanName="PRE_DEPOSIT_STATUS"
								selectedValue="${bean.paymentState}" />
						</select> 
						<input class="${btnclass}" type="submit" value="搜索" />
					</span>
				</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>

				<display:table name="list" requestURI="/admin/preDeposit/query" id="item" export="false" class="${tableclass}" sort="external">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="提现流水号" property="pdcSn"></display:column>
					<display:column title="用户名" property="userName"></display:column>
					<display:column title="用户昵称" property="nickName"></display:column>
					<display:column title="提现金额(元)" property="amount" sortable="true" sortName="amount"></display:column>
					<display:column title="申请时间" format="{0,date,yyyy-MM-dd HH:mm:ss}" property="addTime" sortable="true" sortName="add_time"></display:column>
					<display:column title="收款银行" property="bankName"></display:column>
					<display:column title="收款账号" property="bankNo"></display:column>
					<display:column title="开户姓名" property="bankUser"></display:column>
					<display:column title="支付状态">
						<c:choose>
							<c:when test="${item.paymentState eq 1}">
								<font style="color:green;">已支付</font>
							</c:when>
							<c:when test="${item.paymentState eq 0}">
								<font style="color: #ff6c00;">未支付</font>
							</c:when>
							<c:otherwise>
								<font style="color: red;">错误</font>
							</c:otherwise>
						</c:choose>
					</display:column>
					<display:column title="操作" media="html" style="width:210px;">
						<div class="table-btn-group">
							<c:choose>
								<c:when test="${item.paymentState eq 0}">
									<button class="tab-btn" onclick="showWithdrawCashInfo(${item.id})">
										查看
									</button>
									<span class="btn-line">|</span>
									<button class="tab-btn" onclick="deleteById(${item.id})">
										删除
									</button>
								</c:when>
								<c:when test="${item.paymentState eq 1}">
									<button class="tab-btn" onclick="showWithdrawCashInfo(${item.id})">
										查看
									</button>
								</c:when>
								<c:otherwise>
								</c:otherwise>
							</c:choose>
						</div>
					</display:column>
				</display:table>
				<div class="clearfix">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
				<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass }">
					<tr>
						<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br> 1.未支付的提现单可以点击查看选项更改提现单的支付状态</br>
							2.点击删除可以删除未支付的提现单</br>
						</td>
					<tr>
				</table>
			</div>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
    
     function pager(curPageNO){
            document.getElementById("curPageNO").value=curPageNO;
            document.getElementById("form1").submit();
        }
    
     function showWithdrawCashInfo(id){
    	 layer.open({
 			title :"提现信息",
 			  type: 2, 
 			  content: ["${contextPath}/admin/preDeposit/pdWithdrawCashQueryById?id="+id,"no"], 
 			  area: ['482px', 'auto'],
 			  success: function(layero, index) {
 				  layer.iframeAuto(index);
 				} 
 	    }); 
    }
    
    
    
  function deleteById(id) {
	  layer.confirm("确定删除 ?", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
	            window.location = "${contextPath}/admin/preDeposit/pdWithdrawCashDelete/"+id;
		   });
    }
        highlightTableRows("item");
</script>

</html>