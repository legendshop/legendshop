<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>库存管理 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/amazeui.css"/>
  <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
        <table border="0" align="center" class="${tableclass}" id="col1">
        <form:form >
        	<input  type="hidden" id="skuId" value="${skuId}"/>
            <div align="center">
            
                <thead>
                <tr>
                	<th width="33.3%"><div align="center">仓库名称</div></th>
                	<th width="33.3%"><div align="center">库存</div></th>
                	<th width="33.3%"><div align="center">实际库存</div></th>
                </tr>
                </thead>
                <c:forEach var="item" items="${list}">
				<tr>
			 		<td>
			 			<input class="whProdId" type="hidden" name="whProdId" value="${item.whProdId}"/>
			 			<input class="skuId" type="hidden" name="skuId" value="${item.skuId}"/>
			       	</td>
			        <td>
			           	<input class="stocks" type="text" name="stocks" id="name" value="${item.stocks}"/>
			        </td>
			        <td>
			           	<input class="actualStocks" type="text" name="actualStocks" id="name" value="${item.actualStocks}"/>
			        </td>
				</tr>
				</c:forEach>
           </div>
        </form:form>
        </table>
        <div align="center">
        	<button id="button" class="${btnclass}">提交</button>
            <button class="${btnclass}" onclick="art.dialog.close();">返回</button>
        </div>
        </div>
        </div>
        </body>
<script language="javascript">
	$(function(){
		$("#button").click(function(){
			var jsondata = JSON.stringify(getTableData());
			$.ajax({
				type: "post",
				data:{"jsondata":jsondata,"skuId":$("#skuId").val()},
             	url: "${contextPath}/admin/stock/update",
             	dataType: "json",
             	async : true, //默认为true 异步   
             	success: function(msg){
                    var win = art.dialog.open.origin;  
					win.location.reload();
					art.dialog.close();
                }
			});	
		});
	});

	function getTableData(){
		var tagArr = $("#col1 tr");
		var dataJson= [];  
        tagArr.each(function (index, domEle){// 
          var data = {};
          var skuId="";
          var stocks="";   
   		  var actualStocks=""; 
          if(index != 0){//遍历除去第一行和第二行之外的json数据传入后台  
           	  skuId =$(domEle).find(".skuId").val();
           	  stocks =$(domEle).find(".stocks").val();
           	  actualStocks =$(domEle).find(".actualStocks").val();
           	  data["whProdId"] = whProdId;
           	  data["skuId"] = skuId;
           	  data["stocks"] = stocks;
           	  data["actualStocks"] = actualStocks;
           	  dataJson.push(data); 
          }
        }); 
        return dataJson;		       //返回数据
	}
</script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
</html>
