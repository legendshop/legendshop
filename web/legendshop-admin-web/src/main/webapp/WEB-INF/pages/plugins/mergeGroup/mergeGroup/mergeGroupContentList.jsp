<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	<display:table name="list" requestURI="/admin/mergeGroup/query" id="item" export="false" class="${tableclass}" style="width:100%" sort="external">
		<%-- <display:column title="顺序" class="orderwidth" style="width:100px;"><%=offset++%></display:column> --%>
		<display:column title="活动名称" property="mergeName" style="width:200px;"></display:column>
		<display:column title="商家" property="siteName" style="width:200px"></display:column>
		<display:column title="创建时间" style="width:200px">
			<fmt:formatDate value="${item.createTime}" pattern="yyyy-MM-dd HH:mm:ss" type="date"/>
		</display:column>
		<display:column title="活动时间" style="width:300px">
			<span>
				<fmt:formatDate value="${item.startTime}" pattern="yyyy-MM-dd HH:mm:ss" type="date" />
				至
			</span>
			<span>
				<fmt:formatDate value="${item.endTime}" pattern="yyyy-MM-dd HH:mm:ss" type="date" />
			</span>
		</display:column>
		<display:column title="成团人数" property="peopleNumber" style="width:150px"></display:column>
		<display:column title="成团订单数" property="count" style="width:150px"></display:column>
		<display:column title="活动状态" style="width:150px">
			<c:choose>
				<c:when test="${item.endTime lt date}">
					<span class="fight-sta">已结束</span><c:if test="${item.deleteStatus eq 1}"><font style="color:#e2393b;">(商家已删除)</font></c:if>
				</c:when>
				<c:when test="${empty item.prodStatus || item.prodStatus eq -1 || item.prodStatus eq -2}">
					<span class="fight-sta">商品已删除</span><c:if test="${item.deleteStatus eq 1}"><font style="color:#e2393b;">(商家已删除)</font></c:if>
				</c:when>
				<c:when test="${item.prodStatus eq 0}">
					<span class="fight-sta">商品已下线</span><c:if test="${item.deleteStatus eq 1}"><font style="color:#e2393b;">(商家已删除)</font></c:if>
				</c:when>
				<c:when test="${item.prodStatus eq 2}">
					<span class="fight-sta">商品违规下线</span><c:if test="${item.deleteStatus eq 1}"><font style="color:#e2393b;">(商家已删除)</font></c:if>
				</c:when>
				<c:when test="${item.prodStatus eq 3}">
					<span class="fight-sta">商品待审核</span><c:if test="${item.deleteStatus eq 1}"><font style="color:#e2393b;">(商家已删除)</font></c:if>
				</c:when>
				<c:when test="${item.prodStatus eq 4}">
					<span class="fight-sta">商品审核失败</span><c:if test="${item.deleteStatus eq 1}"><font style="color:#e2393b;">(商家已删除)</font></c:if>
				</c:when>
				<c:otherwise>
					<c:if test="${item.status==-2}">
						<span class="fight-sta">审核不通过</span>
					</c:if>
					<c:if test="${item.status==-1}">
						<span class="fight-staed">已失效</span><c:if test="${item.deleteStatus eq 1}"><font style="color:#e2393b;">(商家已删除)</font></c:if>
					</c:if>
					<c:if test="${item.status==0}">
						<span class="fight-sta">待审核</span>
					</c:if>
					<c:if test="${item.status==-3}">
						<span class="fight-sta">已结束</span>
					</c:if>
					<c:if test="${item.status==1}">
						<c:choose>
							<c:when test="${item.startTime> date}">
								<span class="fight-sta">未开始</span>
							</c:when>
							<c:otherwise>
								<span class="fight-staing">进行中</span>
							</c:otherwise>
						</c:choose>
					</c:if>
				</c:otherwise>
			</c:choose>
		</display:column>
		<display:column title="操作" media="html" style="width:215px">
			<div class="table-btn-group">
				<button class="tab-btn" onclick="window.location='${contextPath}/admin/mergeGroup/mergeGroupDetail/${item.id}'">
					详情
				</button>
				<c:choose>
					<c:when test="${item.endTime < date || item.status == -1 || empty item.prodStatus || item.prodStatus eq -1 || item.prodStatus eq -2 || item.prodStatus eq 2 }">
						<span class="btn-line">|</span>
						<button class="tab-btn" onclick="window.location='${contextPath}/admin/mergeGroup/operation/${item.id}'">
							运营
						</button>
					</c:when>
					<c:when test="${item.status == 1 && item.endTime > date}">
						
						<span class="btn-line">|</span>
						<button class="tab-btn" onclick="window.location='${contextPath}/admin/mergeGroup/operation/${item.id}'">
							运营
						</button>
						<span class="btn-line">|</span>
						<button class="tab-btn" name="statusImg" itemId="${item.id}" itemName="${item.mergeName}" status="${item.status}" toStatus="-1">
							终止
						</button>
					</c:when>
					<c:when test="${item.status == 0 && item.endTime > date}">
					
					    <span class="btn-line">|</span>
						<button class="tab-btn" name="statusImg" itemId="${item.id}" itemName="${item.mergeName}" status="${item.status}" toStatus="1">
							通过
						</button>
						<span class="btn-line">|</span>
						<button class="tab-btn" name="statusImg" itemId="${item.id}" itemName="${item.mergeName}" status="${item.status}" toStatus="-2">
							拒绝
						</button>
					</c:when>
					<c:when test="${item.status == -2 && item.endTime > date}">
						<span class="btn-line">|</span>
						<button class="tab-btn" name="statusImg" itemId="${item.id}" itemName="${item.mergeName}" status="${item.status}" toStatus="1">
							通过
						</button>
					</c:when>
				</c:choose>
				<c:choose>
					<c:when test="${item.status == 1 && item.endTime > date}">
						<span class="btn-line">|</span>
						<button class="tab-btn" onclick="updateCount(${item.id},${item.count})">
							修改成团订单数
						</button>
					</c:when>
				</c:choose>
			</div>
		</display:column>
	</display:table>
	<div class="clearfix">
   		<div class="fr">
   			 <div cla ss="page">
    			 <div class="p-wrap">
        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			 	</div>
  			 </div>
  		</div>
   	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$("button[name='statusImg']").click(function(event){
				$this = $(this);
				initStatus($this.attr("itemId"), $this.attr("itemName"),  $this.attr("status"), contextPath+"/admin/mergeGroup/updateStatus/", $this,contextPath,$this.attr("toStatus"));
			});
		});
	</script>
