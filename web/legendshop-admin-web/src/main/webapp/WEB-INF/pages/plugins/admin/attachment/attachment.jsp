<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<html>
    <head>
        <title>创建</title>
         <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
</head>
    <body>
        <form:form  action="${contextPath}/admin/attachment/save" method="post" id="form1">
            <input id="attachmentId" name="attachmentId" value="${attachment.attachmentId}" type="hidden">
            <div align="center">
            <table border="0" align="center" class="${tableclass}" id="col1">
                <thead>
                    <tr class="sortable">
                        <th colspan="2">
                            <div align="center">
                                	创建
                            </div>
                        </th>
                    </tr>
                </thead>
                
		<tr>
		        <td>
		          	<div align="center">用户名称: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="userName" id="userName" value="${attachment.userName}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="recDate" id="recDate" value="${attachment.recDate}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="ext" id="ext" value="${attachment.ext}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">文件类型: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="fileType" id="fileType" value="${attachment.fileType}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="height" id="height" value="${attachment.height}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="width" id="width" value="${attachment.width}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">文件路径: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="filePath" id="filePath" value="${attachment.filePath}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="fileSize" id="fileSize" value="${attachment.fileSize}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">状态: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="status" id="status" value="${attachment.status}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="tableName" id="tableName" value="${attachment.tableName}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="fieldName" id="fieldName" value="${attachment.fieldName}" />
		        </td>
		</tr>
                <tr>
                    <td colspan="2">
                        <div align="center">
                            <input type="submit" value="保存" />
                            <input type="button" value="返回"  onclick="window.location='<ls:url address="/admin/attachment/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script language="javascript">
    $.validator.setDefaults({
    });

    $(document).ready(function() {
    jQuery("#form1").validate({
            rules: {
            banner: {
                required: true,
                minlength: 5
            },
            url: "required"
        },
        messages: {
            banner: {
                required: "Please enter banner",
                minlength: "banner must consist of at least 5 characters"
            },
            url: {
                required: "Please provide a password"
            }
        }
    });
 
	//斑马条纹
    $("#col1 tr:nth-child(even)").addClass("even");
});
</script>
    </body>
</html>

