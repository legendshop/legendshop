<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
      <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		<display:table name="provinceList" requestURI="/admin/system/district/query" id="provinces" export="false" sort="external" class="${tableclass}" style="width:100%"> 
      <display:column style="width:55px;vertical-align: middle;text-align: center;" title="
      	<span>
           <label class='checkbox-wrapper'>
				<span class='checkbox-item'>
					<input type='checkbox' class='checkbox-input selectAll' onclick='selectAll(this);'/>
					<span class='checkbox-inner' style='margin-left:10px;'></span>
				</span>
		   </label>	
		</span>">
      	<label class="checkbox-wrapper">
			<span class="checkbox-item">
				<input type="checkbox" value="${provinces.id}" class="checkbox-input selectOne" onclick="selectOne(this);"/>
				<span class="checkbox-inner" style="margin-left:10px;"></span>
			</span>
	   </label>	
	   </display:column>
        <display:column title="区域" property="groupby"><%-- <input type="text" id="name" name="groupby" value="${provinces.groupby}" readonly/> --%></display:column>
     	<display:column title="名称" property="province"><%-- <input type="text" id="name" name="province" value="${provinces.province}" readonly/> --%></display:column>
     	<display:column title="邮编" property="provinceid"><%-- <input type="text" id="name" name="province" value="${provinces.provinceid}" readonly/> --%></display:column>
	    <display:column title="操作" media="html"  style="width: 180px;">
	      <div class="table-btn-group">
		      <a href= "${contextPath}/admin/system/district/load/${provinces.id}" title="修改">
		        	 <button class="tab-btn">修改</button>
		      </a>
		      <span class="btn-line">|</span>
		     <a href='javascript:confirmDeletePrivonce("${provinces.id}","${provinces.province}")' title="删除">
		      		<button class="tab-btn" >删除</button>
		      </a>
	      </div>
      </display:column>
	    </display:table>	
	     	<div class="clearfix" style="margin-bottom: 60px;">
	       		<div class="fl">
		       		<input type="button" value="批量刪除" onclick="return deleteAction('deleteprovince');" id="batch-off" class="batch-btn">
				</div>
	       	</div>
        <script language="JavaScript" type="text/javascript">
          jQuery(document).ready(function(){
			    highlightTableRows("provinces");  
		 });
		</script>
