<!DOCTYPE HTML>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>杂志管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>


<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">杂志管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">杂志评论管理</span></th>
				</tr>
			</table>
			<form action="<ls:url address='/admin/article/comment/query'/>"
				id="form1" method="post">
					<div class="criteria-div">
						<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
						<ls:auth ifAnyGranted="F_VIEW_ALL_DATA">
						<span class="item">
			            	杂志名称：<input type="text" name="artName" maxlength="50" value="${articleComment.artName}" style="padding: 3px 5px;" placeholder="杂志名称"/>
			            </span>
			            <span class="item">
			            	内容：<input type="text" name="content" value="${articleComment.content}" style="padding: 3px 5px;" placeholder="内容"/>
						</ls:auth>
							<input type="submit" onclick="search()" value="搜索" class="${btnclass}" style="margin-left: 5px;"/>
						</span>
					</div>
			</form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/articleComment/query"
					id="item" export="false" sort="external" class="${tableclass}"
					style="width:100%;table-layout: fixed;">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="杂志" property="artName"></display:column>
					<display:column title="评论时间">
						<fmt:formatDate value="${item.createtime}"
							pattern="yyyy-MM-dd HH:mm" />
					</display:column>
					<display:column title="内容" property="content"
						style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"></display:column>
					<display:column title="评论人" property="nickName"></display:column>
					<display:column title="审核状态">
						<c:choose>
							<c:when test="${item.status eq 0 }">
								<span>未通过</span>
							</c:when>
							<c:when test="${item.status eq 1 }">
								<span>通过</span>
							</c:when>
							<c:otherwise>
								<span>待审核</span>
							</c:otherwise>
						</c:choose>
					</display:column>

					<display:column title="操作" media="html">
						<div class="table-btn-group">
							<c:choose>
								<c:when test="${item.status eq 0 || empty item.status  }">
									<button type="button" class="tab-btn" onclick='exchangeStatus("${item.id}","1")'>
										通过审核
									</button>
									<span class="btn-line">|</span>
								</c:when>
								<c:when test="${item.status eq 1 }">
									<button type="button" class="tab-btn" onclick='exchangeStatus("${item.id}","0")'>
										拒绝审核
									</button>
									<span class="btn-line">|</span>
								</c:when>
							</c:choose>
							<button class="tab-btn" onclick="window.location='${contextPath}/admin/article/comment/load/${item.id}'">
								查看
							</button>
							<span class="btn-line">|</span>
							<button class="tab-btn" onclick="deleteById('${item.id}');">
								删除
							</button>
						</div>
					</display:column>
				</display:table>
				 <!-- 分页条 -->
		     	<div class="clearfix">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
			</div>
		</div>
	</div>
</body>
<script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/articleCommentList.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	var contextPath = "${contextPath}";
	var curPageNO = "${curPageNO}"//分页
	
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#form1")[0].submit();
	}
</script>

</html>
