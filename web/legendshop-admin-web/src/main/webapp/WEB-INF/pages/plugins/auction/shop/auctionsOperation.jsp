<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
			Integer offset = (Integer)request.getAttribute("offset");
%>
<head>
 	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item="/resources/common/css/pagination.css"/>" />
	<link type="text/css" href="<ls:templateResource item='/resources/templets/amaze/css/mergeGroup/fight.css'/>" rel="stylesheet" />
</head>
<body>
   
   <jsp:include page="/admin/top" />
   
    <div class="am-cf admin-main">
		  <!-- sidebar start -->
			  <jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		      <!-- sidebar end -->
		      <div style="border-bottom:1px solid #ddd;width: 100%;height:40px;line-height:40px;">
		      		<span class="title-span" style="margin-left:10px;">
						拍卖管理  ＞
						<a href="<ls:url address="/admin/mergeGroup/query"/>">拍卖活动</a>
						  ＞   <span style="color:#0e90d2;">拍卖运营</span>
					</span>
		      	</div>
		      <div class="admin-content" id="admin-content" style="overflow-x:auto;">
					<div class="fight-operate"
						style="margin-left: 50px; float: left; width: 93%;">
						<!-- 运营统计数据 -->

						<table  style="width: 100%;margin: 0px;" class="${tableclass}" id="col1">
							<tr align="center">
								<td colspan="4"><b>出价记录</b></td>
							</tr>
							<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
							<tr align="center">
								<td>时间</td>
								<td>出价人</td>
								<td>价格</td>
								<td>状态</td>
							</tr>
							<c:forEach items="${list}" var="bid" varStatus="status">
								<tr align="center">
									<td><fmt:formatDate value="${bid.bitTime}"  pattern="yyyy-MM-dd HH:mm:ss"/></td>
									<td>
										<c:choose>
											<c:when test="${empty bid.nickName}">****${fn:substring(bid.userName,fn:length(bid.userName)-2,-1)}</c:when>
											<c:otherwise>****${fn:substring(bid.nickName,fn:length(bid.nickName)-2,-1)}</c:otherwise>
										</c:choose>
									</td>
									<td>${bid.price}</td>
									<td>
										<c:choose>
											<c:when test="${status.first && curPageNO eq 1}"><font color="red">领先</font></c:when>
											<c:otherwise>出局</c:otherwise>
										</c:choose>
									</td>
								</tr>
							</c:forEach>
							<c:if test="${empty list}">
								<tr align="center">
									<td colspan="4"><b>暂无出价记录</b></td>
								</tr>
							</c:if>
						</table>
						<div align="center"><br/>
							<ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="default" />
						</div>
						<div class="activity-mes" style="margin-top: 35px;">
							<span class="act-tit"></span> <a
								href="${contextPath }/admin/auction/query" class="lim-btn on"
								style="background: #fff; color: #666; border: 1px solid #e4e4e4;">返回</a>
						</div>
					</div>			    
		     </div>
	 </div>
</body>
<script language="JavaScript" type="text/javascript">
	var curPageNO = "${curPageNO}";
	var contextPath = "${contextPath}";
	var auctionId = "${auctionId}"
	function pager(curPageNO) {
		location.href = contextPath + "/admin/auction/operation/"+auctionId+"?curPageNO="
				+ curPageNO;
	}
</script>
</html>
