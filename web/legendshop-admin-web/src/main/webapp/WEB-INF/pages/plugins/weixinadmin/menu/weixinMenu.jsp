<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>微信管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass}" style="width: 100%">
				<thead>
					<tr>
						<th class="no-bg title-th">
							<span class="title-span">
								权限管理  ＞ 
								<span style="color:#0e90d2;">创建菜单</span>
							</span>
						</th>
					</tr>
				</thead>
			</table>
			<form:form action="${contextPath}/admin/weixinMenu/save"
				method="post" id="form1">
				<input id="menuId" name="id" value="${menu.id}" type="hidden">
				<input id="parentId" name="parentId" value="${parentMenu.id}"
					type="hidden">
				<div align="center">
					<table border="0" align="center" class="${tableclass} no-border content-table" id="col1">
						<tr>
							<td style="width:250px; text-align: center:">
								<div align="right"  style="font-weight: bold;">
									<c:if test="${allParentMenu !=null }">
											<c:forEach items="${allParentMenu }" var="menu">
				                                ${menu.value } &raquo;
				                            </c:forEach>
										</c:if>
										创建
										<c:choose>
											<c:when test="${parentMenu !=null}">${parentMenu.grade +1}级</c:when>
											<c:otherwise>1级</c:otherwise>
										</c:choose>
										菜单：
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>名称： 
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" name="name" id="name"
								value="${menu.name}" maxlength="40" /></td>
						</tr>

						<c:if test="${not empty parentMenu}">
							<tr>
								<td>
									<div align="right">父节点：</div>
								</td>
								<td align="left" style="padding-left: 2px;">${parentMenu.name}</td>
							</tr>

							<tr>
								<td>
									<div align="right">
										<font color="ff0000">*</font>菜单类型：
									</div>
								</td>
								<td align="left" style="padding-left: 2px;"><select id="menuType" name="menuType">
										<option value="click"
											<c:if test="${menu.menuType=='click'}">selected="selected"</c:if>>发送信息
										</option>
										<option value="view"
											<c:if test="${menu.menuType=='view'}">selected="selected"</c:if>>跳转到网页</option>
								</select></td>
							</tr>

							<tr id="trurl" style="display: none;">
								<td>
									<div align="right">
										<font color="ff0000">*</font>URL：
									</div>
								</td>
								<td align="left" style="padding-left: 2px;"><textarea rows="2" cols="3"
										style="width: 60%; height: 40px;" name="menuUrl" id="menuUrl"
										maxlength="250">${menu.menuUrl}</textarea></td>
							</tr>

							<tr id="xxtr" style="display: none;">
								<td>
									<div align="right">
										<font color="ff0000">*</font>消息类型：
									</div>
								</td>
								<td align="left" style="padding-left: 2px;">
									<%--                     	 <c:if test="${menu.infoType=='text'}">checked="checked"</c:if> --%>
									<!-- <input type="radio" name="infoType" checked="checked" value="text" />文本消息&nbsp;  -->
									<label class="radio-wrapper radio-wrapper-checked">
										<span class="radio-item">
											<input type="radio" class="radio-input" name="infoType" value="text" checked="checked">
											<span class="radio-inner"></span>
										</span>
										<span class="radio-txt">文本消息</span>
									</label>
									<%--<input type="radio" name="infoType"--%>
									<%--<c:if test="${menu.infoType=='news'}">checked="checked"</c:if> value="news"/>图文&nbsp;--%>
									<%--<input type="radio" name="infoType"--%> <%--<c:if test="${menu.infoType=='image'}">checked="checked"</c:if> value="image"/>图片&nbsp;--%>
									<%--<input type="radio" name="infoType"--%> <%--<c:if test="${menu.infoType=='expand'}">checked="checked"</c:if> value="expand"/>扩展&nbsp;--%>
								</td>
							</tr>

							<tr id="text_content" style="display: none;">
								<td>
									<div align="right">
										<font color="ff0000">*</font>消息内容：
									</div>
								</td>
								<td align="left" style="padding-left: 2px;"><textarea rows="2" cols="5"
										style="width: 60%; height: 100px;" name="content" id="content"
										maxlength="250">${menu.content}</textarea></td>
							</tr>

							<!--                 <tr id="templateTr" style="display: none;"> -->
							<!--                     <td> -->
							<!--                         <div align="center">选择模板:<font color="ff0000">*</font></div> -->
							<!--                     </td> -->
							<!--                     <td> -->
							<!--                         <select id="templateId" name="templateId" style="width: 150px;"> -->
							<!--                             <option value="">---------------------</option> -->
							<!--                         </select> -->
							<!--                     </td> -->
							<!--                 </tr> -->

							<tr id="menuKeyTr" style="display: none;">
								<td>
									<div align="right">
										<font color="ff0000">*</font>菜单标识：
									</div>
								</td>
								<td align="left" style="padding-left: 2px;"><input type="text" name="menuKey" id="menuKey"
									maxlength="30" value="${menu.menuKey}" size="30" /></td>
							</tr>
						</c:if>

						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>顺序：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" name="seq" id="seq"
								value="${menu.seq}" size="5" maxlength="5" /></td>
						</tr>
						<tr>
							<td></td>
							<td align="left" style="padding-left: 2px;">
								<div>
									<input class="${btnclass}" type="submit" value="保存" /> 
									<c:choose>
										<c:when test="${parentMenu != null }">
											<input class="${btnclass}" type="button" value="返回"
												onclick="window.location='${contextPath}/admin/weixinMenu/queryChildMenu/${parentMenu.id}'" />
										</c:when>
										<c:otherwise>
											<input class="${btnclass}" type="button" value="返回"
												onclick="window.location='<ls:url address="/admin/weixinMenu/query"/>'" />
										</c:otherwise>
									</c:choose>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>

<script language="javascript">
	var contextPath = "${contextPath}";
	var templateId = "${menu.templateId}";
	var menuType = "${menu.menuType}";
	var msgType = "${menu.infoType}";
	var infoType = "${menu.infoType}";
	var menuKey = "${menu.menuKey}";
</script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/amaze/js/weixinmenu.js'/>" /></script>

</html>
