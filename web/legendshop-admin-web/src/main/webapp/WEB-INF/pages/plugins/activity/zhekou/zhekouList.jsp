<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss" var="nowDate" />
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>营销管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">店铺促销&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">限时折扣</span></th>
				</tr>
			</table>
			<div>
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li
							<c:if test="${empty marketing.state}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/marketing/zhekou/query"/>">所有活动</a></li>
						<li <c:if test="${marketing.state eq 1}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/marketing/zhekou/query?state=1"/>">上线中</a></li>
						<li <c:if test="${marketing.state eq 0}"> class="am-active"</c:if>><i></i><a
							href="<ls:url address="/admin/marketing/zhekou/query?state=0"/>">未上线</a></li>
						<li <c:if test="${marketing.state eq 2}"> class="am-active"</c:if>><i></i><a
								href="<ls:url address="/admin/marketing/zhekou/query?state=2"/>">已暂停</a></li>
						<li <c:if test="${marketing.state eq 3}"> class="am-active"</c:if>><i></i><a
								href="<ls:url address="/admin/marketing/zhekou/query?state=3"/>">已失效</a></li>
					</ul>
				</div>
			</div>

			<form:form action="${contextPath}/admin/marketing/zhekou/query" id="form1" method="post">
				<div class="criteria-div">
					<input type="hidden" name="_order_sort_name" value="${_order_sort_name}" />
					<input type="hidden" name="_order_indicator" value="${_order_indicator}" />
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
					<input type="hidden" id="state"name="state" value="${marketing.state}" /> 
					<span class="item">
						<input type="text" id="shopId" name="shopId" value="${marketing.shopId}"/>
					</span>
					<span class="item">
						<input type="hidden" id="siteName" name="siteName" value="${marketing.siteName}" placeholder="店铺名字" class="${inputclass}" /> 
						<input type="text" id="marketName" name="marketName" maxlength="50" value="${marketing.marketName}" class="${inputclass}" placeholder="请输入活动名称搜索" /> 
						<input type="hidden" id="type" name="type" value="${marketing.type}" /> 
						<input type="button" value="搜索" onclick="search()" class="${btnclass}" /> 
						<ls:plugin pluginId="b2c">
							<input type="button" value="添加活动"
								onclick="window.location='${contextPath}/admin/marketing/addZkMarketing'"class="${btnclass}" />
						</ls:plugin>
					</span>
				</div>
			</form:form>
			<div align="center" id="zhekouList" class="order-content-list"></div>
			<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass}">
				<tr>
					<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br> 1.用于折扣促活动的所有销信息展示<br>
					</td>
				<tr>
			</table>
		</div>
	</div>
</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
<script language="JavaScript" type="text/javascript">
var state = "${marketing.state}";
     	function pager(curPageNO){
            document.getElementById("curPageNO").value=curPageNO;
            sendData(curPageNO);
        }
    
		  function deleteById(id,shopId) {
			  layer.confirm('确定删除？', {icon:3}, function(){
		      		window.location = "${contextPath}/admin/marketing/deleteShopMarketing/2/"+id+"/"+shopId+"?state="+state;
			  });
		  }
		  
         $(document).ready(function(){
          	$("button[name='statusImg']").click(function(event){
          		$this = $(this);
          		initStatus($this.attr("itemId"), $this.attr("itemName"),  $this.attr("status"), "${contextPath}/admin/pub/updatestatus/", $this,"${contextPath}");
       			 }
       		 );
       		  //   highlightTableRows("item"); 
       		var shopName = $("#siteName").val();
          	if(shopName == ''){
                makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",'请选择店铺', 'siteName');
            }else{
            	makeSelect2(contextPath + "/admin/product/getShopSelect2","shopId","店铺","value","key",shopName, 'siteName');
        		$("#select2-chosen-1").html(shopName);
            } 
        	sendData(1);
          });
         function sendData(curPageNO) {
        	    var data = {
       	    		"shopId": $("#shopId").val(),
           	        "curPageNO": curPageNO,
           	        "state": $("#state").val(),
           	        "marketName": $("#marketName").val(),
        	    };
        	    $.ajax({
        	        url: "${contextPath}/admin/marketing/zhekou/queryContent",
        	        data: data,
        	        type: 'post',
        	        async: true, //默认为true 异步   
        	        success: function (result) {
        	            $("#zhekouList").html(result);
        	        }
        	    });
        	}

        	function makeSelect2(url,element,text,label,value,holder, textValue){
        		$("#"+element).select2({
        	        placeholder: holder,
        	        allowClear: true,
        	        width:'200px',
        	        ajax: {  
        	    	  url : url,
        	    	  data: function (term,page) {
        	                return {
        	                	 q: term,
        	                	 pageSize: 10,    //一次性加载的数据条数
        	                 	 currPage: page, //要查询的页码
        	                };
        	            },
        				type : "GET",
        				dataType : "JSON",
        	            results: function (data, page, query) {
        	            	if(page * 5 < data.total){//判断是否还有数据加载
        	            		data.more = true;
        	            	}
        	                return data;
        	            },
        	            cache: true,
        	            quietMillis: 200//延时
        	      }, 
        	        formatInputTooShort: "请输入" + text,
        	        formatNoMatches: "没有匹配的" + text,
        	        formatSearching: "查询中...",
        	        formatResult: function(row,container) {//选中后select2显示的 内容
        	            return "<b>"+row.text+"</b>"; //+ "</br>" + row.customText1
        	        },formatSelection: function(row) { //选择的时候，需要保存选中的id
        	        	$("#" + textValue).val(row.text);
        	            return row.text;//选择时需要显示的列表内容
        	        }, 
        	    });
        	}

        	function search() {
        		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
        		sendData(1);
        	}
          
          
           function onlineShopMarketing(_id){
		    layer.confirm("确定要发布该活动麽？", {icon:3}, function () {
		       $.ajax({
					url: "${contextPath}/admin/marketing/onlineShopMarketing/2/"+_id, 
					type:'post', 
					async : false, //默认为true 异步   
					"dataType":"json",
					error: function(jqXHR, textStatus, errorThrown) {
					},
					success:function(retData){
					   if(retData=="OK"){
					     window.location.reload(true);
					     return;
					   }else{
					      layer.alert(retData, {icon:2});
					      return;
					   }
					}
				});
			});
		}
		
		function offlineShopMarketing(_id){
	     	layer.confirm("确定要下线该活动麽？", {icon:3}, function () {
				window.location.href="${contextPath}/admin/marketing/offlineShopMarketing/2/"+_id+"?state="+state;
			});
		}	
</script>
</html>
