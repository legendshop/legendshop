<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

    <%
        Integer offset = (Integer) request.getAttribute("offset");
    %>
    <div>
    		<table class="${tableclass}" style="width: 100%">
			    <thead>
			    	<tr><th> <strong class="am-text-primary am-text-lg">微信用户管理</strong> /  查看用户</th></tr>
			    </thead>
			    </table>
    	<table class="${tableclass}">
    			<tr>
			        <td width="150px" >
			          <div align="right" style="line-height: 40px">用户昵称：</div>
			       </td>
			        <td style="line-height: 40px">${weixinUserInfo.nickname}</td>
			      </tr>
			      <tr>
			        <td width="150px">
			          <div align="right" style="margin-top: 40px">用户头像：</div>
			       </td>
			        <td><image width="100" height="100" src="${weixinUserInfo.headimgurl}"/></td>
			      </tr>
			      <tr>
			        <td width="150px">
			          <div align="right" style="line-height: 40px">性　　别：</div>
			       </td>
			         <td style="line-height: 40px">${weixinUserInfo.sex}</td>
			      </tr>
			      <tr>
			        <td width="150px">
			          <div align="right" style="line-height: 40px">备　　注：</div>
			       </td>
			         <td style="line-height: 40px">${weixinUserInfo.bzname}</td>
			      </tr>
			       <tr>
			        <td width="150px">
			          <div align="right" style="line-height: 40px">地　　区：</div>
			       </td>
			         <td style="line-height: 40px">${weixinUserInfo.country}${weixinUserInfo.province}省${weixinUserInfo.city}市</td>
			      </tr>
			      <tr>
			        <td width="150px">
			          <div align="right" style="line-height: 40px">创建时间：</div>
			       </td>
			         <td style="line-height: 40px"><fmt:formatDate value="${weixinUserInfo.addtime}" type="both" dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" /></td>
			      </tr>
			      <c:if test="${weixinUserInfo.subscribe == 'Y'}">
				      <tr>
				        <td width="150px">
				          <div align="right" style="line-height: 40px">关注时间：</div>
				       </td>
				         <td style="line-height: 40px"><fmt:formatDate value="${weixinUserInfo.subscribeTime}" type="both" dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" /></td>
				      </tr>
			      </c:if>
			      <tr>
			      	<td width="100px">
			      		<div align="right"><input type="button" value="返回" class="${btnclass}" onclick="window.history.go(-1);" /></div>
			      	</td>
			      	<td></td>
			      </tr>
    	</table>
	   </div>
