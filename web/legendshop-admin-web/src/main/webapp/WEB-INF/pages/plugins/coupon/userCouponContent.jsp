<%@ page language="java" pageEncoding="UTF-8"%>
<%Integer offset = (Integer)request.getAttribute("offset");%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<div align="center" style="margin-top: 5px;">
	<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	 <c:choose>
			 <c:when test="${not empty list}">
			  		   <input type="hidden" id="curPageNO" name="curPageNO" value="${arg.curPageNO}">
	<display:table name="list"
		requestURI="#" id="item"
		export="false" class="${tableclass}" style="width:100%" sort="external">
		<display:column style="width:100px;vertical-align: middle;text-align: center;"
			title="全选
			<input type='checkbox'  id='checkbox' name='checkbox' onClick='javascript:selAll()' />"><%=offset++%>
			<input type="checkbox" name="strArray" value="${item.userId}" arg="${item.userName}" />
		</display:column>
		<display:column style="width:150px" title="用户名" property="userName"
			></display:column>
		<display:column style="width:180px" title="用户昵称" property="nickName" ></display:column>
		<display:column style="width:180px" title="邮件" property="userMail"></display:column>
		<display:column style="width:110px" title="手机" property="userMobile"></display:column>
		<display:column style="width:130px" title="注册时间" >
			<fmt:formatDate value="${item.userRegtime}"
				pattern="yyyy-MM-dd HH:mm" />
		</display:column>
		<display:column style="width:180px" title="登录次数" property="logNumber" ></display:column>
		<display:column style="width:180px" title="订单总金额" property="totalAmount" ></display:column>
		<display:column style="width:180px" title="订单数量" property="orderNumber" ></display:column>
	</display:table>
	<c:if test="${not empty toolBar}">
		<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
	</c:if>
	<div style="margin: 10px;display: block;width: 450px;" >
	    <input class="${btnclass}" type="button"  value="确定选择" onclick="javascript:sendUser();"><span style=" margin-right:5px;color: red;">(选中的用户)</span>
	     <input class="${btnclass}" type="button"  value="批量确定" onclick="javascript:batckSendUser();"><span style="margin-right:5px;color: red;" >(筛选的全部的用户)</span>
	   </div>
		</c:when>
			  		<c:otherwise>
			  		   没有相应数据
			  		</c:otherwise>
	</c:choose>
	
</div>

