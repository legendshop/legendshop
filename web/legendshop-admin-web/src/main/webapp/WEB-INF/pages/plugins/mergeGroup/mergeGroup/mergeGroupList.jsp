<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content" style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">拼团管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">拼团活动管理</span></th>
				</tr>
			</table>

			<div>
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li class="am-active"><i></i><a
								href="#" onclick="searchType(this,'')">全部活动</a></li>
						<li><i></i><a
								href="#" onclick="searchType(this,'WAIT_AUDIT')">待审核</a></li>
						<li><i></i><a
								href="#" onclick="searchType(this,'NOT_PASS')">未通过</a></li>
						<li><i></i><a
								href="#" onclick="searchType(this,'NOT_STARTED')">未开始</a></li>
						<li><i></i><a
								href="#" onclick="searchType(this,'ONLINE')">进行中</a></li>
						<li><i></i><a
								href="#" onclick="searchType(this,'FINISHED')">已结束</a></li>
						<li><i></i><a
								href="#" onclick="searchType(this,'EXPIRED')">已失效</a></li>
						<input type="hidden"  id="searchType"/>

					</ul>
				</div>
			</div>


			<form:form method="GET" id="form1" action="${contextPath}/admin/mergeGroup/query">
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}">
					<span class="item">
						店铺：
						<input type="text" id="shopId" name="shopId" value="${requestScope.list.shopId}" />
	                 	<input type="hidden" id="shopName" name="shopName" value="${requestScope.list.shopName}" class="${inputclass}"/>
	                </span>
	                <span class="item">
						活动名称：
						<input type="text" name="mergeName" id="mergeName" maxlength="50" value="${requestScope.list.stageName}" class="${inputclass}" />
					</span>
					<span class="item">
<%--						状态：--%>
<%--						<select id="searchType" name="searchType">--%>
<%--						<option value="">--请选择--</option>--%>
<%--						<option value="WAIT_AUDIT" <c:if test="${'WAIT_AUDIT' eq requestScope.list.searchType}">selected</c:if>>待审核</option>--%>
<%--						<option value="NOT_PASS" <c:if test="${'NOT_PASS' eq requestScope.list.searchType}">selected</c:if>>审核不通过</option>--%>
<%--						<option value="NOT_STARTED" <c:if test="${'NOT_STARTED' eq requestScope.list.searchType}">selected</c:if>>未开始</option>--%>
<%--						<option value="ONLINE" <c:if test="${'ONLINE' eq requestScope.list.searchType}">selected</c:if>>进行中</option>--%>
<%--						<option value="FINISHED" <c:if test="${'FINISHED' eq requestScope.list.searchType}">selected</c:if>>已结束</option>--%>
<%--						<option value="EXPIRED" <c:if test="${'EXPIRED' eq requestScope.list.searchType}">selected</c:if>>已失效</option>--%>
<%--					</select>--%>
						<input type="button" onclick="search()" value="搜索" class="${btnclass}" style="margin-left: 6px;"/>
					</span>
				</div>
			</form:form>
			<div align="center" id="mergeGroupContentList" class="order-content-list"></div>
		</div>
	</div>
</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
<script type="text/javascript" src="${contextPath}/resources/templets/amaze/js/mergeGroupList.js"></script>
<script language="JavaScript" type="text/javascript">
	var curPageNO = "${curPageNO}";
	var contextPath = "${contextPath}";
	function updateCount(id,count){
		layer.open({
			title: '修改成团订单数',
			id:'deliverGoods',
			type: 2,
			content: contextPath+"/admin/mergeGroup/mergeGroupCount/"+id+"/"+count,
			area: ['450px', '150px']
		});
	}
</script>
</html>
