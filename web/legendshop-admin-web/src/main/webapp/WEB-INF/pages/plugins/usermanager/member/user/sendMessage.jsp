<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>权限管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/common/css/errorform.css" />
<link rel="stylesheet"
	href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
<link rel="stylesheet"
	href="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.css'/>" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">用户管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/system/userDetail/query"/>">用户信息管理</a>
              				&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;"><c:if test="${type == 1}">发送站内信</c:if> <c:if
							test="${type == 2}">发送邮件</c:if> <c:if test="${type == 3}">发送短信</c:if>
							</span>
					</th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/sendmessage/send" id="form1"
				method="post">
				<input type="hidden" id="type" name="type" value="${type}">
				<div align="center" style="margin-top: 10px; ">
					<table align="center" class="${tableclass} no-border content-table" id="col1">
						<c:if test="${type!=3}">
							<tr>
								<td style="width:200px;">
									<div align="right">
										<font color="ff0000">*</font>标题 ：
									</div>
								</td>
								<td align="left" style="padding-left: 2px;"><input style="width: 300px;" type="text" name="title" id="title" value=""
									size="50" maxlength="40"/></td>
							</tr>
						</c:if>
						<c:if test="${type == 3 }">
							<tr>
								<td>
									<div align="right">
										<font color="ff0000">*</font>手机号码：
									</div>
								</td>
								<td align="left" style="padding-left: 2px;"><input style="width: 300px;" type="text" name="mobile" id="mobile"
									value="${userDetail.userMobile}" maxlength="30"
									readonly="readonly" /></td>
							</tr>
						</c:if>
						<c:if test="${type == 2 }">
							<tr>
								<td>
									<div align="right">Email</div>
								</td>
								<td align="left" style="padding-left: 2px;"><c:choose>
										<c:when test="${not empty userDetail.userMail}">
											<lable>${userDetail.userMail}</lable>
										</c:when>
										<c:otherwise>
											<lable>用户未绑定邮箱</lable>
										</c:otherwise>
									</c:choose> <%-- <input type="text" name="email" id="email" value="${userDetail.userMail}" maxlength="30" readonly="readonly"/> --%>
								</td>
							</tr>
						</c:if>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>用户名 ：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" style="width: 150px;" name="userName" id="userName"
								value="${userDetail.userName}" maxlength="30"
								readonly="readonly" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>内容 ：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><c:if test="${type==2}">
									<textarea name="text" id="text" cols="100" rows="8"
										style="width: 300px; height: 400px; visibility: hidden;"></textarea>
								</c:if> <c:if test="${type!=2}">
									<input type="text" name="text" style="width: 300px;" id="text" value="" size="70" />
								</c:if></td>
						</tr>
						<tr>
							<td></td>
							<td align="left" style="padding-left: 0px;">
								<div>
									<input class="${btnclass}" type="submit" value="保存" />
									<input class="${btnclass}" type="button" value="返回"
										onclick="window.history.go(-1)" />
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>
<script src="${contextPath}/resources/common/js/jquery.validate.js"
	type="text/javascript"></script>
<script charset="utf-8"
	src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script charset="utf-8"
	src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script charset="utf-8"
	src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
<script language="javascript">
	$.validator.setDefaults({});

	$(document).ready(function() {
		jQuery("#form1").validate({
			ignore : "",
			rules : {
				title : {
					required : true
				},
				email : {
					required : true
				},
				mobile : {
					required : true
				},
				userName : {
					required : true
				},
				text : {
					required : true
				}
			},
			messages : {
				title : {
					required : "标题不能为空"
				},
				email : {
					required : "email不能为空"
				},
				mobile : {
					required : "手机不能为空"
				},
				userName : {
					required : "用户名不能为空"
				},
				text : {
					required : "内容为空"
				}
			}
		});
	});
</script>
<script type="text/javascript">
	$(function() {
		var msg = "${msg}";
		if (msg != "" && msg== 'succ') {
			layer.msg("发送成功",{icon:1},function(){
				window.location.href = "${contextPath}/admin/system/userDetail/query";
			});
		}
		if (msg != "" && msg== 'fail') {
			layer.msg("发送失败",{icon:0},function(){
				window.location.href = "${contextPath}/admin/system/userDetail/query";
			});
		}
		KindEditor.options.filterMode = false;
		KindEditor
				.create(
						'textarea[name="text"]',
						{
							cssPath : '${contextPath}/resources/plugins/kindeditor/plugins/code/prettify.css',
							uploadJson : '${contextPath}/editor/uploadJson/upload;jsessionid=${cookie.SESSION.value}',
							fileManagerJson : '${contextPath}/editor/uploadJson/fileManager',
							allowFileManager : true,
							afterBlur : function() {
								this.sync();
							},
							width : '100%',
							height : '400px',
							afterCreate : function() {
								var self = this;
								KindEditor.ctrl(document, 13, function() {
									self.sync();
									document.forms['example'].submit();
								});
								KindEditor.ctrl(self.edit.doc, 13, function() {
									self.sync();
									document.forms['example'].submit();
								});
							}
						});
	});
</script>
</html>
