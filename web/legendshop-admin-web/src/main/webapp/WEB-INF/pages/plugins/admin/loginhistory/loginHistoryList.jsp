<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%Integer offset = (Integer)request.getAttribute("offset");%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">

    <table class="${tableclass} title-border" style="width: 100%">
    	<tr><th class="title-border">系统报表&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">用户登录历史</span></th></tr>
    </table>
	<form:form  action="${contextPath}/admin/loginHistory/query" id="form1">
		<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}">
		<div class="criteria-div">
			<span class="item">
				用户名称：<input class="${inputclass}" type="text" name="userName" id="userName" maxlength="50" value="${login.userName}" size="15" placeholder="用户名称"/>
			</span>
			<span class="item">
				开始时间：<input readonly="readonly"  name="startTime" id="startTime" class="Wdate" type="text"  value='<fmt:formatDate value="${login.startTime}" pattern="yyyy-MM-dd"/>' placeholder="开始时间"/>
			</span>
			<span class="item">
				结束时间：<input readonly="readonly" name="endTime" id="endTime" class="Wdate" type="text"  value='<fmt:formatDate value="${login.endTime}" pattern="yyyy-MM-dd"/>' placeholder="结束时间"/>
				<input class="${btnclass}" type="button" onclick="search()" value="搜索" style="margin-left: 6px;"/>
			</span>
		</div>
	</form:form>
	<div align="center" class="order-content-list">
        <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
    <display:table name="list" requestURI="/admin/loginHistory/query"  id="item"
         export="false"  class="${tableclass}" style="width:100%" sort="external">
      <display:column title="顺序"  class="orderwidth"><%=offset++%></display:column>
      <display:column title="用户名" property="userName"></display:column>
      <display:column title="IP" property="ip"></display:column>
      <display:column title="国家" property="country"></display:column>
      <display:column title="登录时间">
      	<fmt:formatDate value="${item.time}" pattern="yyyy-MM-dd HH:mm"/>
      </display:column>
      <display:column title="登录类型">
      		<c:choose>
      			<c:when test="${item.loginType=='PC'}">
      				pc端登录
      			</c:when>
      			<c:when test="${item.loginType=='MOBIEL'}">
      				MOBILE端登录
      			</c:when>
      			<c:when test="${item.loginType=='APP'}">
      				APP端登录
      			</c:when>
      		</c:choose>
      </display:column>
    </display:table>
       <div class="clearfix" style="margin-bottom: 60px;">
       		<div class="fr">
       			 <div class="page">
	       			 <div class="p-wrap">
	           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	    			 </div>
       			 </div>
       		</div>
       	</div>
    </div>
    </div>
    </div>
    </body>
	<script language="JavaScript" type="text/javascript">
		function pager(curPageNO){
			document.getElementById("curPageNO").value=curPageNO;
			document.getElementById("form1").submit();
		}
		
		 highlightTableRows("item");  
		 
		 function search(){
		  	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		  	$("#form1")[0].submit();
		}
		 
		 laydate.render({
	   		 elem: '#startTime',
	   		 calendar: true,
	   		 theme: 'grid',
	 		 trigger: 'click'
	   	  });
	   	   
	   	  laydate.render({
	   	     elem: '#endTime',
	   	     calendar: true,
	   	     theme: 'grid',
	 		 trigger: 'click'
	      });
</script>
</html>
