<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>提现管理 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
	    <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	    <table class="${tableclass} title-border" style="width: 100%">
	    	<tr>
		    	<th class="title-border">用户管理&nbsp;＞&nbsp;<a href="<ls:url address="/admin/preDeposit/pdRechargeQuery"/>">预存款管理</a>
		    	&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">提现信息编辑</span>
		    	</th>
	    	</tr>
	    </table>
	<form:form  action="${contextPath}/admin/preDeposit/pdWithdrawCashUpdate" method="post" id="form1" onsubmit="return;">
		    <table style="100%" border="0" align="center" id="col1"  class="${tableclass} no-border content-table">
		    	<input type="hidden" name="id" id="id" value="${pdWithdrawCash.id}"/>
				<tr>
					<td width="200px"><div align="right">提现流水号：</div></td>
					<td align="left"><input type="text" name="pdcSn" id="pdcSn" value="${pdWithdrawCash.pdcSn}" readonly="readonly" size="50"/></td>
				</tr>
				<tr>
					<td><div align="right">提现金额(元)：</div></td>
					<td align="left">${pdWithdrawCash.amount}元</td>
				</tr>
				<tr>
					<c:choose>
						<c:when test="${not empty pdWithdrawCash.nickName}">
							<td><div align="right">用户昵称：</div></td>
							<td align="left">${pdWithdrawCash.nickName}</td>
						</c:when>
						<c:otherwise>
							<td><div align="right">用户名：</div></td>
							<td align="left">${pdWithdrawCash.userName}</td>
						</c:otherwise>
					</c:choose>
				</tr>
				<tr>
					<td><div align="right">收款银行：</div></td>
					<td align="left">${pdWithdrawCash.bankName}</td>
				</tr>
				<tr>
					<td><div align="right">收款账号：</div></td>
					<td align="left">${pdWithdrawCash.bankNo}</td>
				</tr>
				<tr>
					<td><div align="right">开户人姓名：</div></td>
					<td align="left">${pdWithdrawCash.bankUser}</td>
				</tr>
				
				<tr>
					<td><div align="right"><font color="ff0000">*</font>支付时间：</div></td>
					<td align="left" style="text-align: left !important"><input readonly="readonly"  name="paymentTime" id="paymentTime" class="Wdate" type="text" pattern="yyyy-MM-dd HH:mm:ss"/></td>
					<input type="hidden" value="${pdWithdrawCash.paymentState}" name="paymentState" id="paymentState"/>
				</tr>
				<tr>
					<td><div align="right"><font color="ff0000">*</font>操作备注：</div></td>
					<td align="left"><textarea rows="5" cols="50" id="adminNote" name="adminNote" maxlength="120" placeholder="请输入管理员操作，内容不超过120字符"></textarea></td>
				</tr>
				<tr>
					<td></td>
					<td align="left">
                    	 <input type="submit" class="${btnclass}" value="确认提交"/>
           			</td>
				</tr>
			</table>
</form:form>
</div>
</div></body>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		jQuery("#form1").validate({
			rules : {
				"paymentTime":{required:true},
				"adminNote":{required:true}
			},
			messages : {
				"paymentTime":{
					required:"请填写支付时间"
				},
				"adminNote":{
					required:"请填写备注信息"
				},
			}
		});
	});
    highlightTableRows("col1");
    //斑马条纹
    $("#col1 tr:nth-child(even)").addClass("even");	
	
    laydate.render({
 		 elem: '#paymentTime',
 		 type: 'datetime',
 		 calendar: true,
 		 theme: 'grid',
 		 trigger: 'click'
 	  });
</script>
</html>
 