<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<html>
<head>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/floorTemplate.css" />
<title>编辑品牌</title>
<style type="text/css">
	.floor_choose_box select, .floor_choose_box input[type="text"]{
		width:48px;
	}
</style>
</head>
<body>

<div class="box_floor">
  <div class="box_floor_six">
    <div class="box_floor_tit">
      <b>楼层标题：</b>
      <input name="BDname" type="text" id="BDname" value="品牌旗舰店" />
    </div>
    <span class="floor_choose">已选推荐品牌：</span>
    <div class="box_floor_prodel"> <em class="floor_warning">注释：最多选择40种品牌，双击删除已有品牌信息，按下鼠标拖动品牌次序</em>
      <div class="floor_box_pls" id="floor_goods_info">

		<c:forEach items="${requestScope.floorItemList}" var="floorItem">
        <ul ondblclick="jQuery(this).remove();" class="floor_pro">
          <li class="floor_pro_img" style="height:30px">
          	<img src="<ls:photo item='${floorItem.pic}' />"   height="30px" width="90px"/>
		  </li>
          <li class="floor_pro_name" id="${floorItem.referId}">${floorItem.brandName}</li>
        </ul>
		</c:forEach>
		
        </div>
    </div>
    <span class="floor_choose">选择要展示的品牌：</span>
    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
    <div class="floor_choose_box"> <span class="floor_search_sp"><b>品牌名称：</b>
      <input name="brandName" type="text" id="brandName" style="width: 198px;"/>
      <input type="button"  value="搜索" onclick="brandFloorLoad();" id="brandNameBtn" class="edit-blue-btn" />
      </span> <em class="floor_warning">注释：点击品牌加入楼层，最多选择40种品牌</em>
     <div id="floor_goods_list">
     	
     </div>
    </div>
  </div>
  <!--图片开始-->
  <div class="submit" style="text-align:center;">
    &nbsp;<input type="button" value="保存设置" id="saveForm" onclick="save_form();" class="edit-blue-btn" />
  </div>
</div>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/alternative.js'/>" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.core.js'/>"  type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.widget.js'/>"  type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.mouse.js'/>"  type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.sortable.js'/>"  type="text/javascript"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/brandFloorOverLay.js'/>"></script>
<script language="javascript">
	var contextPath = "${contextPath}";
	var flId = '${flId}';
	var layout = '${layout}';
	var limit="${limit==null?12:limit}";
	
	function goods_list_set(img,brandId,name){
		var count=jQuery(".floor_box_pls ul").length;
		var add=0; 
		var flag=true;
		if(count>=limit){
			layer.alert("品牌不能超过"+limit, {icon:2});
			return;
		}else{
			jQuery(".floor_box_pls .floor_pro_name").each(function(){
				if(jQuery(this).attr("id")== brandId){
					layer.alert("已经存在该品牌", {icon:2});
					flag = false;
					return;
				}
			});
		}
		var s="<ul ondblclick='jQuery(this).remove();' class='floor_pro'><li class='floor_pro_img' style='height:30px'><img src='<ls:photo item='"+img+"'/>' height='30px' width='90px'/></li><li class='floor_pro_name' id='"+brandId+"'>"+name+"</li></ul>";
		if(flag){
			jQuery(".floor_box_pls").append(s);
		}
	}
</script>
</body>
</html>