<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<jsp:useBean id="now" class="java.util.Date" />
<fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss" var="nowDate" />
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	<display:table name="list"
		requestURI="/admin/marketing/mansong/query" id="item"
		sort="external" export="false" class="${tableclass}">
		<display:column title="
			<span>
	           <label class='checkbox-wrapper'>
					<span class='checkbox-item'>
						<input type='checkbox' id='checkbox' class='checkbox-input selectAll' onclick='selectAll(this);'/>
						<span class='checkbox-inner' style='margin-left:10px;'></span>
					</span>
			   </label>	
			</span>">
		   <c:choose>
	    		<c:when test="${item.state!=0}">
				   <label class="checkbox-wrapper checkbox-wrapper-disabled">
						<span class="checkbox-item">
							<input type="checkbox" name="mansongIds" state="${item.state}" value="${item.id}" class="checkbox-input"  disabled='disabled'/>
							<span class="checkbox-inner" style="margin-left:10px;"></span>
						</span>
				   </label>	
	    		</c:when>
	    		<c:otherwise>
	    			<label class="checkbox-wrapper">
						<span class="checkbox-item">
							<input type="checkbox" name="mansongIds" state="${item.state}" value="${item.id}" class="checkbox-input selectOne" onclick="selectOne(this);"/>
							<span class="checkbox-inner" style="margin-left:10px;"></span>
						</span>
				   </label>	
	    		</c:otherwise>
	    	</c:choose>
		</display:column>
		<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
		<display:column title="活动名称" property="marketName"></display:column>
		<display:column title="店铺">
			<a
				href="<ls:url address="/admin/shopDetail/load/${item.shopId}"/>"
				target="_blank">${item.siteName}</a>
		</display:column>
		<display:column title="开始时间">
			<fmt:formatDate value="${item.startTime}"
							pattern="yyyy-MM-dd HH:mm:ss" />
			<c:set value="${item.startTime}" var="startTime"></c:set>
		</display:column>
		<display:column title="结束时间">
			<fmt:formatDate value="${item.endTime}"
							pattern="yyyy-MM-dd HH:mm:ss" />
			<c:set value="${item.endTime}" var="endTime"></c:set>
		</display:column>
		<display:column title="状态">
			<c:choose>
				<c:when test="${item.state eq 0}">
					<c:choose>
						<c:when test="${nowDate gt endTime}">已结束</c:when>
						<c:when test="${nowDate gt startTime && nowDate lt endTime}">未发布</c:when>
						<c:otherwise>
							未发布
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:when test="${item.state eq 1}">
					<c:choose>
						<c:when test="${nowDate gt endTime}">已结束</c:when>
						<c:when test="${nowDate gt startTime && nowDate lt endTime}">进行中</c:when>
						<c:otherwise>
							未开始
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:when test="${item.state eq 2}">
					已暂停
				</c:when>
				<c:when test="${item.state eq 3}">
					已失效
				</c:when>
			</c:choose>

		</display:column>
		<display:column title="操作" media="html" style="width:210px;">
				<div class="table-btn-group">
					<c:if test="${item.state eq 0}">
						<c:choose>
							<c:when test="${nowDate gt endTime}">
								<button class="tab-btn" onclick="deleteById(${item.id},${item.shopId})">
									删除
								</button>
							</c:when>
							<c:when test="${nowDate gt startTime && nowDate lt endTime}">
								<button class="tab-btn" onclick="window.location='${contextPath}/admin/marketing/mansongQueryById/${item.id}/${item.shopId}'">
									查看
								</button>
								<span class="btn-line">|</span>
								<button class="tab-btn" onclick="deleteById(${item.id},${item.shopId})">
									删除
								</button>
							</c:when>
							<c:otherwise>
								<button class="tab-btn" onclick="deleteById(${item.id},${item.shopId})">
									删除
								</button>
							</c:otherwise>
						</c:choose>
					</c:if>
					<c:if test="${item.state eq 1}">
						<c:choose>
							<c:when test="${nowDate gt endTime}">
								<button onclick="javascript:offlineShopMarketing('${item.id}');" class="tab-btn">
									下线
								</button>
							</c:when>
							<c:when test="${nowDate gt startTime && nowDate lt endTime}">
								<button class="tab-btn" onclick="window.location='${contextPath}/admin/marketing/mansongQueryById/${item.id}/${item.shopId}'">
									查看
								</button>
							</c:when>
							<c:otherwise>
								<button onclick="javascript:offlineShopMarketing('${item.id}');" class="tab-btn">
									下线
								</button>
							</c:otherwise>
						</c:choose>
					</c:if>
				</div>
		</display:column>
	</display:table>
	<div class="clearfix">
   		<div class="fl">
    		<input type="button" onclick="batchDel()" value="批量删除" id="batch-del" class="batch-btn">
		</div>
   		<div class="fr">
   			 <div cla ss="page">
    			 <div class="p-wrap">
        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			 	</div>
  			 </div>
  		</div>
   	</div>
	<script type="text/javascript">
		$("#checkAll").on("click",function(){
			if($(this).is(":checked")){
				$("input[type=checkbox][name=mansongIds][state=0]").prop("checked",true);
			}else{
				$("input[type=checkbox][name=mansongIds][state=0]").prop("checked",false);
			}
		});
	</script>
