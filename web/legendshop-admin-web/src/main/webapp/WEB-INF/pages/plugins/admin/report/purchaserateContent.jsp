<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	<display:table name="list" requestURI="/admin/report/purchaserate"
		id="item" export="false" sort="external" class="${tableclass}"
		style="width:100%">

		<display:column title="顺序" style="width:50px"><%=offset++%></display:column>
		<display:column title="店铺" property="siteName" style="width:150px"></display:column>
		<display:column title="商品名称" style="width:300px"><div class="order_img_name">${item.name}</div></display:column>
		<display:column title="商品价格" style="width:100px" property="cash"
			sortable="true" sortName="cash"></display:column>
		<display:column title="访问次数" style="width:100px" property="views"
			sortable="true" sortName="views"></display:column>
		<display:column title="销售数量" style="width:100px" sortable="true"
			sortName="payedBuys">
  				${item.payedBuys eq null ? 0 :  item.payedBuys}
  		</display:column>
		<display:column title="访问购买率" style="width:100px" sortable="true">
			<c:choose>
				<c:when test="${item.views != 0}">
					<fmt:formatNumber type="percent"
						value="${item.payedBuys/item.views}" />
				</c:when>
				<c:otherwise>0%</c:otherwise>
			</c:choose>
		</display:column>
		<display:column title="操作" media="html" style="width:120px">
			<div class="table-btn-group">
	  			<button class="tab-btn offline"><a href="${PC_DOMAIN_NAME}/views/${item.prodId}" target="_blank">查看商品</a></button>
	  			<span class="btn-line">|</span>
	  			<button class="tab-btn online"><a href="${contextPath}/admin/shopDetail/load/${item.shopId}" target="_blank">查看店铺</a></button>
			</div>
		</display:column>
	</display:table>
	<div class="clearfix" style="margin-bottom: 60px;">
   		<div class="fr">
   			 <div class="page">
    			 <div class="p-wrap">
        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
 			 	</div>
   			 </div>
   		</div>
   	</div>
   <script src="${contextPath}/resources/templets/amaze/js/amazeui.min.js"></script>