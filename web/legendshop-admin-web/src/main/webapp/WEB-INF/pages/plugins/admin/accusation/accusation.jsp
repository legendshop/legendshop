<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>举报详情 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
  <link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.css'/>" />
</head>
<body>
<jsp:include page="/admin/top" />
<div class="am-cf admin-main">
		<!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	    	<table class="${tableclass} no-border" style="width: 100%;">
                <thead>
                    <tr>
                        <th class="no-bg title-th">
			            	<span class="title-span">
								咨询管理  ＞
								<a href="<ls:url address="/admin/accusation/query"/>" style="color: #0e90d2;">举报管理</a>
								  ＞  <span style="color:#0e90d2;">举报详情</span>
							</span>
			            </th>
                    </tr>
                </thead>
            </table>
         <table style="width: 100%;" class="${tableclass} no-border content-table" id="col1">
          <tr>
		        <td width="200px;">
		          	<div align="right">产品名称：</div>
		       	</td>
		        <td align="left" style="padding-left: 2px;">
		           	${accusation.prodName}
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="right">举报主题：</div>
		       	</td>
		        <td align="left" style="padding-left: 2px;">
		           	${accusation.title}
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="right">举报人ID：</div>
		       	</td>
		        <td align="left" style="padding-left: 2px;">
		           	${accusation.userName}
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="right">举报人昵称：</div>
		       	</td>
		        <td align="left" style="padding-left: 2px;">
		           	${accusation.nickName}
		        </td>
		</tr>
		<tr>
		        <td style="width: 150px">
		          	<div align="right">用户是否删除：</div>
		       	</td>
		        <td align="left" style="padding-left: 2px;">
		           	<c:if test="${accusation.userDelStatus == 0}">否</c:if>
		           	<c:if test="${accusation.userDelStatus == 1}">是</c:if>
		        </td>
		</tr>
		<c:if test="${accusation.result !=null}">
			<tr>
			        <td>
			          	<div align="right">处理结果：</div>
			       	</td>
			        <td align="left" style="padding-left: 2px;">
			           	<c:choose>
				     			<c:when test="${accusation.result ==1}">无效举报</c:when>
				     			<c:when test="${accusation.result ==2}">有效举报</c:when>
				     			<c:when test="${accusation.result ==3}">恶意举报</c:when>
				     			<c:otherwise></c:otherwise>
	     				</c:choose>
			        </td>
			</tr>
		</c:if>
		<c:if test="${accusation.handleInfo !=null}">
			<tr>
			        <td>
			          	<div align="right">处理意见：</div>
			       	</td>
			        <td align="left" style="padding-left: 2px;">
			           	${accusation.handleInfo}
			        </td>
			</tr>
		</c:if>
		<tr>
		        <td>
		          	<div align="right">状态：</div>
		       	</td>
		        <td align="left" style="padding-left: 2px;">
		           	<c:choose>
		     			<c:when test="${accusation.status ==1}">已处理</c:when>
		     			<c:otherwise>未处理</c:otherwise>
     				</c:choose>
		        </td>
		</tr>
		<c:if test="${not empty accusation.pic1}">
			<tr>
			        <td valign="top">
			          	<div align="right">凭证图片：</div>
			       	</td>
			        <td align="left" style="padding-left: 2px;">
			           	<a href="<ls:photo item='${accusation.pic1}'/>" target="_blank"> 
			           		<img src="<ls:photo item='${accusation.pic1}'/>" width="150" height="150"/>
						</a>
			        </td>
			</tr>
		</c:if>
		<c:if test="${not empty accusation.pic2}">
			<tr>
			        <td valign="top">
			          	<div align="right">凭证图片2：</div>
			       	</td>
			        <td align="left" style="padding-left: 2px;">
			           		<a href="<ls:photo item='${accusation.pic2}'/>" target="_blank"> <img
											src="<ls:photo item='${accusation.pic2}'/>" width="150" height="150"/>
							</a>
			        </td>
			</tr>
		</c:if>
		<c:if test="${not empty accusation.pic3}">
			<tr>
			        <td valign="top">
			          	<div align="right">凭证图片3：</div>
			       	</td>
			        <td align="left" style="padding-left: 2px;">
			           		<a href="<ls:photo item='${accusation.pic3}'/>" target="_blank"> <img
											src="<ls:photo item='${accusation.pic3}'/>" width="150" height="150"/>
							</a>
			        </td>
			</tr>
		</c:if>
		<tr>
		        <td valign="top">
		          	<div align="right">举报内容：</div>
		       	</td>
		        <td align="left" style="padding-left: 2px;" valign="top">
		           	${accusation.content}
		        </td>
		</tr>
        <tr>
        	<td></td>
            <td align="left" style="padding-left: 2px;">
                <div>
                    <input class="${btnclass}" type="button" value="返回" onclick="window.location='<ls:url address="/admin/accusation/query"/>'"/>
                </div>
            </td>
        </tr>
            </table>
            </div>
            </div>
       </body>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script charset="utf-8" src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
<script language="javascript">
		    $.validator.setDefaults({
		    });

    $(document).ready(function() {
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
		KindEditor.options.filterMode=false;
     	 KindEditor.create('textarea[name="content"]', {
     		cssPath : '${contextPath}/resources/plugins/kindeditor/plugins/code/prettify.css',
     		uploadJson : '${contextPath}/editor/uploadJson/upload;jsessionid=${cookie.SESSION.value}',
     		fileManagerJson : '${contextPath}/editor/uploadJson/fileManager',
     		allowFileManager : true,
     		afterBlur:function(){this.sync();},
     		width : '100%',
     		height:'400px',
     		afterCreate : function() {
     			var self = this;
     			KindEditor.ctrl(document, 13, function() {
     				self.sync();
     				document.forms['example'].submit();
     			});
     			KindEditor.ctrl(self.edit.doc, 13, function() {
     				self.sync();
     				document.forms['example'].submit();
     			});
     		}
     	});
         prettyPrint();
});
</script>
