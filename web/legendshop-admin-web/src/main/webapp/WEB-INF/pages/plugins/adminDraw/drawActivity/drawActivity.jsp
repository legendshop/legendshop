<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/laydate.jsp"%>
<%@ include file="/WEB-INF/pages/common/back-dialog.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>新增抽奖活动 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet"
	href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
<link rel="stylesheet"
	href="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.css'/>" />
<link rel="stylesheet"
	href="<ls:templateResource item='/resources/common/css/drawActivity.css'/>" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/common/css/errorform.css" />
<style>
	#awardsSet td{
		line-height: 28px;
	}
</style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass}" style="width: 100%">
				<thead>
					<tr>
						<th class="no-bg title-th">
			            	<span class="title-span">
								平台营销  ＞  
								<a href="<ls:url address="/admin/drawActivity/query"/>">抽奖活动</a>
								<c:if test="${not empty drawActivity.id }">  ＞  <span style="color:#0e90d2;">活动编辑</span></c:if>
								<c:if test="${empty drawActivity.id }">  ＞  <span style="color:#0e90d2;">新增活动</span></c:if>
							</span>
			            </th>
					</tr>
				</thead>
			</table>
			<form:form action="${contextPath}/admin/drawActivity/save"
				method="post" id="form1">
				<input id="id" name="id" value="${drawActivity.id}" type="hidden">
				<input id="awardInfo" name="awardInfo"
					value="${drawActivity.awardInfo}" type="hidden">
				<input type="hidden" name="actStatus" id="actStatus"
					value="${drawActivity.actStatus}" />
				<div align="center">
					<table border="0" align="center" class="${tableclass} no-border content-table" id="col1">
						<thead>
						</thead>
						<tr>
							<td style="width:200px;">
								<div align="right">
									<font color="ff0000">*</font>活动类型：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<select id="actType" name="actType" class="${selectclass}">
									<option value="DA_ZHUAN_PAN">大转盘</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>活动名称：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" name="actName" id="actName"
								value="${drawActivity.actName}" class="${inputclass}"
								maxlength="40" style="width: 200px;" /><em></em></td>
						</tr>

						<%-- <tr>
		        <td>
		          	<div align="center">中奖概率：</div>
		       	</td>
		        <td>
		           	<input type="text" name="winPencent" id="winPencent" value="${drawActivity.winPencent}" style="width: 150px;" class="${inputclass}"/>
		           	<span><font color="ff0000">注:中奖概率格式例如 10/100</font></span>
		        </td>
		</tr> --%>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>开始时间：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input readonly="readonly" class="${inputclass}"
								name="startTime" id="startTime" class="Wdate" type="text"
								value='<fmt:formatDate value="${drawActivity.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>' />
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>结束时间：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input readonly="readonly" class="${inputclass}"
								name="endTime" id="endTime" class="Wdate" type="text"
								value='<fmt:formatDate value="${drawActivity.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>' />
							</td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>个人每日抽奖次数：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" class="${inputclass}"
								name="timesPerday" id="timesPerday" maxlength="10"
								value="${drawActivity.timesPerday}" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>个人抽奖总次数：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" class="${inputclass}"
								name="timesPerson" id="timesPerson" maxlength="10"
								value="${drawActivity.timesPerson}" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>中奖是否可继续参与：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><select id="winJoinStatus" name="winJoinStatus"
								class="${selectclass}">
									<ls:optionGroup type="select" required="true" cache="true"
										beanName="YES_NO"
										selectedValue="${drawActivity.winJoinStatus}" />
							</select></td>
						</tr>
						<%-- <tr>
		        <td>
		          	<div align="center">是否绑定手机用户可参与：</div>
		       	</td>
		        <td>
		           	<select id="bindJoinStatus" name="bindJoinStatus" class="${selectclass}">
					    <ls:optionGroup type="select" required="true" cache="true"
		                beanName="YES_NO" selectedValue="${drawActivity.bindJoinStatus}"/>
		            </select>
		        </td>
		</tr> --%>
						<tr>
							<td valign="top">
								<div align="right">
									<font color="ff0000">*</font>活动规则：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><textarea name="actRule" id="actRule" cols="100"
									rows="8" maxlength="500" style="width: 800px; height: 100px;">${drawActivity.actRule}</textarea>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<div align="right">活动描述：</div>
							</td>
							<td colspan="2" align="left" style="padding-left: 2px;"><textarea name="actDes" id="actDes"
									cols="100" rows="8"
									style="width: 700px; height: 200px; visibility: hidden;">${drawActivity.actDes}</textarea>
							</td>
						</tr>


						<!---------------------------------------------------- 奖项设置  ------------------------------------------------------------------->


						<c:if test="${empty drawActivity}">
							<tr>
								<table class="${tableclass}" id="awardsSet">
									<tr>
										<td>
											<div align="right">
												<font color="ff0000">*</font>一等奖：
											</div>
										</td>
										<td align="left" style="padding-left: 2px;">奖项类型: <select id="awardsType" class="${selectclass}"
											name="awardsType"
											onchange="awardsSelect(this.options[this.options.selectedIndex].value,this);">
												<option value="1">实体物</option>
												<option value="2">现金</option>
												<option value="3">积分</option>
												<option value="4">红包</option>
										</select>

										</td>

										<td>
											<div align="right" class="AwardName">
												 <font color="ff0000">*</font>奖品：
											</div>
										</td>
										<td align="left" style="padding-left: 2px;"><input type="input" id="dtoId" name="dtoId" value="1"
											style="display: none;" class="${inputclass}" /> <input
											type="input" id="giftName" name="giftName" maxlength="20"
											placeholder="请输入奖品名称" class="${inputclass}" /> <input
											type="input" id="cash" name="cash" value=""
											placeholder="请输入金额" maxlength="10" style="display: none;"
											class="${inputclass}" /> <input type="input" id="integral"
											name="integral" value="" placeholder="请输入积分" maxlength="10"
											style="display: none;" class="${inputclass}" />
											<!-- 请选择红包 -->
											<div class="drawBox" style="display: none;" data-level="first">
													<input type="hidden" name="coupon" value="" maxlength="10" class="draw" />
													<a href="javascript:void(0)" onclick="openCounpon(this);" id="drawAdbtn">请选择红包</a>
													<div class="coupon-example" style="display: none;">
														<div class="cou-box">
															<div class="cou-info">
																<p class="cou-name">
																	<b>红包券</b>
																	<em class="coupon-type">全店通用</em>
																</p>
																<p class="cou-des coupon-name">适用全品类商品</p>
																<p class="cou-num">
																	剩余
																	<em class="coupon-number">108</em>
																	张
																</p>
															</div>
															<div class="cou-price">
																<p class="price">
																	&#65509;<b class="offPrice">50</b>
																</p>
																<p class="limit">
																	满
																	<span class="fullPrice">200</span>
																	元可用
																</p>
															</div>
															<div class="cou-mid">
																<p class="up"></p>
																<p class="down"></p>
															</div>
														</div>
														<div class="cou-del">
															<a href="javascript:void(0);" onclick="cleanCoupon(this)">删除</a>
														</div>
													</div>
												</div>
										</td>
										<td>
											<div align="right">
												一等奖数量：
											</div>
										</td>
										<td align="left" style="padding-left: 2px;"><input type="text" id="amount" name="amount"
											value="" maxlength="10" /></td>
										<td>
											<div align="right">
												中奖率：
											</div>
										</td>
										<td align="left" style="padding-left: 2px;"><input type="text" id="weight" name="weight"
											value="" maxlength="10" />&nbsp;%</td>
									</tr>

									<tr>
										<td>
											<div align="right">
												<font color="ff0000">*</font>二等奖：
											</div>
										</td>
										<td align="left" style="padding-left: 2px;">奖项类型: <select id="awardsType" class="${selectclass}"
											name="awardsType"
											onchange="awardsSelect(this.options[this.options.selectedIndex].value,this);">
												<option value="1">实体物</option>
												<option value="2">现金</option>
												<option value="3">积分</option>
												<option value="4">红包</option>
										</select>
										</td>

										<td>
											<div align="right" class="AwardName">
												<font color="ff0000">*</font>奖品：
											</div>
										</td>
										<td align="left" style="padding-left: 2px;"><input type="input" id="dtoId" name="dtoId" value="2"
											style="display: none;" class="${inputclass}" /> <input
											type="input" id="giftName" name="giftName" value=""
											maxlength="20" placeholder="请输入奖品名称" class="${inputclass}" />
											<input type="input" id="cash" name="cash" value=""
											placeholder="请输入金额" maxlength="10" style="display: none;"
											class="${inputclass}" /> <input type="input" id="integral"
											name="integral" value="" placeholder="请输入积分" maxlength="10"
											style="display: none;" class="${inputclass}" />
											<!-- 请选择红包 -->
											<div class="drawBox" style="display: none;" data-level="second">
												<input type="hidden" name="coupon" value="" maxlength="10" class="draw" />
												<a href="javascript:void(0)" onclick="openCounpon(this);" id="drawAdbtn">请选择红包</a>
												<div class="coupon-example" style="display: none;">
													<div class="cou-box">
														<div class="cou-info">
															<p class="cou-name">
																<b>红包券</b>
																<em class="coupon-type">全店通用</em>
															</p>
															<p class="cou-des coupon-name">适用全品类商品</p>
															<p class="cou-num">
																剩余
																<em class="coupon-number">108</em>
																张
															</p>
														</div>
														<div class="cou-price">
															<p class="price">
																&#65509;<b class="offPrice">50</b>
															</p>
															<p class="limit">
																满
																<span class="fullPrice">200</span>
																元可用
															</p>
														</div>
														<div class="cou-mid">
															<p class="up"></p>
															<p class="down"></p>
														</div>
													</div>
													<div class="cou-del">
														<a href="javascript:void(0);" onclick="cleanCoupon(this)">删除</a>
													</div>
												</div>
											</div>
										</td>
										<td>
											<div align="right">
												二等奖数量：
											</div>
										</td>
										<td align="left" style="padding-left: 2px;"><input type="text" id="amount" name="amount"
											value="" maxlength="10" /></td>
										<td>
											<div align="right">
												中奖率：
											</div>
										</td>
										<td align="left" style="padding-left: 2px;"><input type="text" id="weight" name="weight"
											value="" maxlength="10" />&nbsp;%</td>
									</tr>

									<tr>
										<td>
											<div align="right">
												<font color="ff0000">*</font>三等奖：
											</div>
										</td>
										<td align="left" style="padding-left: 2px;">奖项类型: <select id="awardsType" class="${selectclass}"
											name="awardsType"
											onchange="awardsSelect(this.options[this.options.selectedIndex].value,this);">
												<option value="1">实体物</option>
												<option value="2">现金</option>
												<option value="3">积分</option>
												<option value="4">红包</option>
										</select>
										</td>

										<td>
											<div align="right" class="AwardName">
												<font color="ff0000">*</font>奖品：
											</div>
										</td>
										<td align="left" style="padding-left: 2px;"><input type="input" id="dtoId" name="dtoId" value="3"
											style="display: none;" class="${inputclass}" /> <input
											type="input" id="giftName" name="giftName" value=""
											maxlength="20" placeholder="请输入奖品名称" class="${inputclass}" />
											<input type="input" id="cash" name="cash" value=""
											placeholder="请输入金额" maxlength="10" style="display: none;"
											class="${inputclass}" /> <input type="input" id="integral"
											name="integral" value="" placeholder="请输入积分" maxlength="10"
											style="display: none;" class="${inputclass}" />
											<!-- 请选择红包 -->
											<div class="drawBox" style="display: none;" data-level="third">
												<input type="hidden" name="coupon" value="" maxlength="10" class="draw" />
												<a href="javascript:void(0)" onclick="openCounpon(this);" id="drawAdbtn">请选择红包</a>
												<div class="coupon-example" style="display: none;">
													<div class="cou-box">
														<div class="cou-info">
															<p class="cou-name">
																<b>红包券</b>
																<em class="coupon-type">全店通用</em>
															</p>
															<p class="cou-des coupon-name">适用全品类商品</p>
															<p class="cou-num">
																剩余
																<em class="coupon-number">108</em>
																张
															</p>
														</div>
														<div class="cou-price">
															<p class="price">
																&#65509;<b class="offPrice">50</b>
															</p>
															<p class="limit">
																满
																<span class="fullPrice">200</span>
																元可用
															</p>
														</div>
														<div class="cou-mid">
															<p class="up"></p>
															<p class="down"></p>
														</div>
													</div>
													<div class="cou-del">
														<a href="javascript:void(0);" onclick="cleanCoupon(this)">删除</a>
													</div>
												</div>
											</div>
										</td>
										<td>
											<div align="right">
												三等奖数量：
											</div>
										</td>
										<td align="left" style="padding-left: 2px;"><input type="text" id="amount" name="amount"
											value="" maxlength="10" /></td>
										<td>
											<div align="right">
												中奖率：
											</div>
										</td>
										<td align="left" style="padding-left: 2px;"><input type="text" id="weight" name="weight"
											value="" maxlength="10" />&nbsp;%</td>
									</tr>
								</table>
							</tr>
						</c:if>


						<c:if
							test="${not empty drawActivity&& not empty drawActivity.drawAwardsDtoList}">
							<tr>
								<table class="${tableclass}" id="awardsSet">
									<c:forEach items="${drawActivity.drawAwardsDtoList}" var="dd">
										<c:choose>
											<c:when test="${dd.id eq 1 }">
												<tr>
													<td>
														<div align="right">
															一等奖：
														</div>
													</td>
													<td align="left" style="padding-left: 2px;">奖项类型: <select id="awardsType"
														class="${selectclass}" name="awardsType"
														onchange="awardsSelect(this.options[this.options.selectedIndex].value,this);">
															<option value="1"
																<c:if test="${dd.awardsType eq 1}" > selected="selected" </c:if>>实体物</option>
															<option value="2"
																<c:if test="${dd.awardsType eq 2}" > selected="selected" </c:if>>现金</option>
															<option value="3"
																<c:if test="${dd.awardsType eq 3}" > selected="selected" </c:if>>积分</option>
															<option value="4"
																<c:if test="${dd.awardsType eq 4}" > selected="selected" </c:if>>红包</option>
													</select>

													</td>

													<td>
														<div align="right" class="AwardName">
															<c:choose>
																<c:when test="${dd.awardsType eq 1}">
							          	                                               奖品:
							          	       </c:when>
																<c:when test="${dd.awardsType eq 2}">
							          	                                             预付款金额:
							          	       </c:when>
																<c:when test="${dd.awardsType eq 3}">
							          	                                              积分:
							          	       </c:when>
																<c:when test="${dd.awardsType eq 4}">
							          	                                              红包:
							          	       </c:when>
															</c:choose>
															<font color="ff0000">*</font>

														</div>
													</td>
													<td align="left" style="padding-left: 2px;"><input type="input" id="dtoId" name="dtoId"
														value="1" style="display: none;" class="${inputclass}" />
														<c:choose>
															<c:when test="${dd.awardsType eq 1}">
																<input type="input" id="giftName" name="giftName"
																	value="${dd.giftName }" maxlength="20"
																	placeholder="请输入奖品名称" class="${inputclass}" />
																<input type="input" id="cash" name="cash" value=""
																	placeholder="请输入金额" maxlength="10"
																	style="display: none;" class="${inputclass}" />
																<input type="input" id="integral" name="integral"
																	value="" placeholder="请输入积分" maxlength="10"
																	style="display: none;" class="${inputclass}" />
																<div class="drawBox" style="display: none;" data-level="first">
																		<input type="hidden" name="coupon" value="" maxlength="10" class="draw" />
																		<a href="javascript:void(0)" onclick="openCounpon(this);" id="drawAdbtn">请选择红包</a>
																		<div class="coupon-example" style="display: none;">
																			<div class="cou-box">
																				<div class="cou-info">
																					<p class="cou-name">
																						<b>红包券</b>
																						<em class="coupon-type">全店通用</em>
																					</p>
																					<p class="cou-des coupon-name">适用全品类商品</p>
																					<p class="cou-num">
																						剩余
																						<em class="coupon-number">108</em>
																						张
																					</p>
																				</div>
																				<div class="cou-price">
																					<p class="price">
																						&#65509;<b class="offPrice">50</b>
																					</p>
																					<p class="limit">
																						满
																						<span class="fullPrice">200</span>
																						元可用
																					</p>
																				</div>
																				<div class="cou-mid">
																					<p class="up"></p>
																					<p class="down"></p>
																				</div>
																			</div>
																			<div class="cou-del">
																				<a href="javascript:void(0);" onclick="cleanCoupon(this)">删除</a>
																			</div>
																		</div>
																	</div>
															</c:when>
															<c:when test="${dd.awardsType eq 2}">
																<input type="input" id="giftName" name="giftName"
																	value="" placeholder="请输入奖品名称" maxlength="20"
																	class="${inputclass}" style="display: none;" />
																<input type="input" id="cash" name="cash"
																	value="${dd.cash }" placeholder="请输入金额" maxlength="10"
																	class="${inputclass}" />
																<input type="input" id="integral" name="integral"
																	value="" placeholder="请输入积分" maxlength="10"
																	style="display: none;" class="${inputclass}" />
																<div class="drawBox" style="display: none;" data-level="first">
																		<input type="hidden" name="coupon" value="" maxlength="10" class="draw" />
																		<a href="javascript:void(0)" onclick="openCounpon(this);" id="drawAdbtn">请选择红包</a>
																		<div class="coupon-example" style="display: none;">
																			<div class="cou-box">
																				<div class="cou-info">
																					<p class="cou-name">
																						<b>红包券</b>
																						<em class="coupon-type">全店通用</em>
																					</p>
																					<p class="cou-des coupon-name">适用全品类商品</p>
																					<p class="cou-num">
																						剩余
																						<em class="coupon-number">108</em>
																						张
																					</p>
																				</div>
																				<div class="cou-price">
																					<p class="price">
																						&#65509;<b class="offPrice">50</b>
																					</p>
																					<p class="limit">
																						满
																						<span class="fullPrice">200</span>
																						元可用
																					</p>
																				</div>
																				<div class="cou-mid">
																					<p class="up"></p>
																					<p class="down"></p>
																				</div>
																			</div>
																			<div class="cou-del">
																				<a href="javascript:void(0);" onclick="cleanCoupon(this)">删除</a>
																			</div>
																		</div>
																	</div>
															</c:when>
															<c:when test="${dd.awardsType eq 3}">
																<input type="input" id="giftName" name="giftName"
																	value="" placeholder="请输入奖品名称" maxlength="20"
																	class="${inputclass}" style="display: none;" />
																<input type="input" id="cash" name="cash" value=""
																	placeholder="请输入金额" maxlength="10"
																	style="display: none;" class="${inputclass}" />
																<input type="input" id="integral" name="integral"
																	value="${dd.integral }" maxlength="10"
																	placeholder="请输入积分" class="${inputclass}" />
																<div class="drawBox" style="display: none;" data-level="first">
																		<input type="hidden" name="coupon" value="" maxlength="10" class="draw" />
																		<a href="javascript:void(0)" onclick="openCounpon(this);" id="drawAdbtn">请选择红包</a>
																		<div class="coupon-example" style="display: none;">
																			<div class="cou-box">
																				<div class="cou-info">
																					<p class="cou-name">
																						<b>红包券</b>
																						<em class="coupon-type">全店通用</em>
																					</p>
																					<p class="cou-des coupon-name">适用全品类商品</p>
																					<p class="cou-num">
																						剩余
																						<em class="coupon-number">108</em>
																						张
																					</p>
																				</div>
																				<div class="cou-price">
																					<p class="price">
																						&#65509;<b class="offPrice">50</b>
																					</p>
																					<p class="limit">
																						满
																						<span class="fullPrice">200</span>
																						元可用
																					</p>
																				</div>
																				<div class="cou-mid">
																					<p class="up"></p>
																					<p class="down"></p>
																				</div>
																			</div>
																			<div class="cou-del">
																				<a href="javascript:void(0);" onclick="cleanCoupon(this)">删除</a>
																			</div>
																		</div>
																	</div>
															</c:when>
															<c:when test="${dd.awardsType eq 4}">
																	<input type="input" id="giftName" name="giftName" value="" placeholder="请输入奖品名称" maxlength="20" class="${inputclass}" style="display: none;" />
																	<input type="input" id="cash" name="cash" value="" placeholder="请输入金额" maxlength="10" style="display: none;" class="${inputclass}" />
																	<input type="input" id="integral" name="integral" value="" style="display: none;" maxlength="10" placeholder="请输入积分" class="${inputclass}" />
																	<div class="drawBox" data-level="first">
																		<input type="hidden" name="coupon" value="${dd.coupon}" maxlength="10" class="draw" />
																		<a href="javascript:void(0)" onclick="openCounpon(this);" id="drawAdbtn">请选择红包</a>
																		<div class="coupon-example">
																			<div class="cou-box">
																				<div class="cou-info">
																					<p class="cou-name">
																						<b>红包券</b>
																						<em class="coupon-type">
																							<c:choose>
																								<c:when test="${dd.drawCoupon.couponType eq 'shop'}">
																									店铺红包
																								</c:when>
																								<c:otherwise>
																									通用红包
																								</c:otherwise>
																							</c:choose>
																						</em>
																					</p>
																					<p class="cou-des coupon-name">${dd.drawCoupon.couponName}</p>
																					<p class="cou-num">
																						剩余
																						<em class="coupon-number">${dd.drawCoupon.couponNumber -  dd.drawCoupon.bindCouponNumber}</em>
																						张
																					</p>
																				</div>
																				<div class="cou-price">
																					<p class="price">
																						&#65509;<b class="offPrice">${dd.drawCoupon.offPrice }</b>
																					</p>
																					<p class="limit">
																						满
																						<span class="fullPrice">${dd.drawCoupon.fullPrice}</span>
																						元可用
																					</p>
																				</div>
																				<div class="cou-mid">
																					<p class="up"></p>
																					<p class="down"></p>
																				</div>
																			</div>
																			<div class="cou-del">
																				<a href="javascript:void(0);" onclick="cleanCoupon(this)">删除</a>
																			</div>
																		</div>
																	</div>
																</c:when>
														</c:choose></td>
													<td>
														<div align="right">
															一等奖数量：
														</div>
													</td>
													<td align="left" style="padding-left: 2px;"><input type="input" id="amount" name="amount"
														value="${dd.amount }" maxlength="10" /></td>
													<td>
														<div align="right">
															中奖率：
														</div>
													</td>
													<td align="left" style="padding-left: 2px;"><input type="text" id="weight" name="weight"
														value="${dd.weight }" maxlength="10" />&nbsp;%</td>
												</tr>
											</c:when>



											<c:when test="${dd.id eq 2 }">
												<tr>
													<td>
														<div align="right">
															二等奖：
														</div>
													</td>
													<td align="left" style="padding-left: 2px;">奖项类型: <select id="awardsType"
														class="${selectclass}" name="awardsType"
														onchange="awardsSelect(this.options[this.options.selectedIndex].value,this);">
															<option value="1"
																<c:if test="${dd.awardsType eq 1}" > selected="selected" </c:if>>实体物</option>
															<option value="2"
																<c:if test="${dd.awardsType eq 2}" > selected="selected" </c:if>>现金</option>
															<option value="3"
																<c:if test="${dd.awardsType eq 3}" > selected="selected" </c:if>>积分</option>
															<option value="4"
																<c:if test="${dd.awardsType eq 4}" > selected="selected" </c:if>>红包</option>
													</select>
													</td>

													<td>
														<div align="right" class="AwardName">
															<c:choose>
																<c:when test="${dd.awardsType eq 1}">
										          	                                               奖品:
										          	       </c:when>
																<c:when test="${dd.awardsType eq 2}">
										          	                                             预付款金额:
										          	       </c:when>
																<c:when test="${dd.awardsType eq 3}">
										          	                                              积分:
										          	       </c:when>
																<c:when test="${dd.awardsType eq 4}">
										          	                                              红包:
										          	       </c:when>
															</c:choose>
															<font color="ff0000">*</font>
														</div>
													</td>
													<td align="left" style="padding-left: 2px;"><input type="input" id="dtoId" name="dtoId"
														value="2" style="display: none;" class="${inputclass}" />
														<c:choose>
															<c:when test="${dd.awardsType eq 1}">
																<input type="input" id="giftName" name="giftName"
																	value="${dd.giftName }" maxlength="20"
																	placeholder="请输入奖品名称" class="${inputclass}" />
																<input type="input" id="cash" name="cash" value=""
																	placeholder="请输入金额" style="display: none;"
																	maxlength="10" class="${inputclass}" />
																<input type="input" id="integral" name="integral"
																	value="" placeholder="请输入积分" style="display: none;"
																	maxlength="10" class="${inputclass}" />
																<!-- 请选择红包 -->
																<div class="drawBox" style="display: none;" data-level="second">
																	<input type="hidden" name="coupon" value="" maxlength="10" class="draw" />
																	<a href="javascript:void(0)" onclick="openCounpon(this);" id="drawAdbtn">请选择红包</a>
																	<div class="coupon-example" style="display: none;">
																		<div class="cou-box">
																			<div class="cou-info">
																				<p class="cou-name">
																					<b>红包券</b>
																					<em class="coupon-type">全店通用</em>
																				</p>
																				<p class="cou-des coupon-name">适用全品类商品</p>
																				<p class="cou-num">
																					剩余
																					<em class="coupon-number">108</em>
																					张
																				</p>
																			</div>
																			<div class="cou-price">
																				<p class="price">
																					&#65509;<b class="offPrice">50</b>
																				</p>
																				<p class="limit">
																					满
																					<span class="fullPrice">200</span>
																					元可用
																				</p>
																			</div>
																			<div class="cou-mid">
																				<p class="up"></p>
																				<p class="down"></p>
																			</div>
																		</div>
																		<div class="cou-del">
																			<a href="javascript:void(0);" onclick="cleanCoupon(this)">删除</a>
																		</div>
																	</div>
																</div>
															</c:when>
															<c:when test="${dd.awardsType eq 2}">
																<input type="input" id="giftName" name="giftName"
																	value="" placeholder="请输入奖品名称" maxlength="20"
																	class="${inputclass}" style="display: none;" />
																<input type="input" id="cash" name="cash"
																	value="${dd.cash }" maxlength="10" placeholder="请输入金额"
																	class="${inputclass}" />
																<input type="input" id="integral" name="integral"
																	value="" maxlength="10" placeholder="请输入积分"
																	style="display: none;" class="${inputclass}" />
																<div class="drawBox" style="display: none;" data-level="second">
																		<input type="hidden" name="coupon" value="" maxlength="10" class="draw" />
																		<a href="javascript:void(0)" onclick="openCounpon(this);" id="drawAdbtn">请选择红包</a>
																		<div class="coupon-example" style="display: none;">
																			<div class="cou-box">
																				<div class="cou-info">
																					<p class="cou-name">
																						<b>红包券</b>
																						<em class="coupon-type">全店通用</em>
																					</p>
																					<p class="cou-des coupon-name">适用全品类商品</p>
																					<p class="cou-num">
																						剩余
																						<em class="coupon-number">108</em>
																						张
																					</p>
																				</div>
																				<div class="cou-price">
																					<p class="price">
																						&#65509;<b class="offPrice">50</b>
																					</p>
																					<p class="limit">
																						满
																						<span class="fullPrice">200</span>
																						元可用
																					</p>
																				</div>
																				<div class="cou-mid">
																					<p class="up"></p>
																					<p class="down"></p>
																				</div>
																			</div>
																			<div class="cou-del">
																				<a href="javascript:void(0);" onclick="cleanCoupon(this)">删除</a>
																			</div>
																		</div>
																	</div>
															</c:when>
															<c:when test="${dd.awardsType eq 3}">
																<input type="input" id="giftName" name="giftName"
																	value="" placeholder="请输入奖品名称" maxlength="20"
																	class="${inputclass}" style="display: none;" />
																<input type="input" id="cash" name="cash" value=""
																	placeholder="请输入金额" style="display: none;"
																	maxlength="10" class="${inputclass}" />
																<input type="input" id="integral" name="integral"
																	value="${dd.integral }" placeholder="请输入积分"
																	maxlength="10" class="${inputclass}" />
																<div class="drawBox" style="display: none;" data-level="second">
																		<input type="hidden" name="coupon" value="" maxlength="10" class="draw" />
																		<a href="javascript:void(0)" onclick="openCounpon(this);" id="drawAdbtn">请选择红包</a>
																		<div class="coupon-example" style="display: none;">
																			<div class="cou-box">
																				<div class="cou-info">
																					<p class="cou-name">
																						<b>红包券</b>
																						<em class="coupon-type">全店通用</em>
																					</p>
																					<p class="cou-des coupon-name">适用全品类商品</p>
																					<p class="cou-num">
																						剩余
																						<em class="coupon-number">108</em>
																						张
																					</p>
																				</div>
																				<div class="cou-price">
																					<p class="price">
																						&#65509;<b class="offPrice">50</b>
																					</p>
																					<p class="limit">
																						满
																						<span class="fullPrice">200</span>
																						元可用
																					</p>
																				</div>
																				<div class="cou-mid">
																					<p class="up"></p>
																					<p class="down"></p>
																				</div>
																			</div>
																			<div class="cou-del">
																				<a href="javascript:void(0);" onclick="cleanCoupon(this)">删除</a>
																			</div>
																		</div>
																	</div>
															</c:when>
															<c:when test="${dd.awardsType eq 4}">
																	<input type="input" id="giftName" name="giftName" value="" placeholder="请输入奖品名称" maxlength="20" class="${inputclass}" style="display: none;" />
																	<input type="input" id="cash" name="cash" value="" placeholder="请输入金额" style="display: none;" maxlength="10" class="${inputclass}" />
																	<input type="input" id="integral" name="integral" value="" style="display: none;" placeholder="请输入积分" maxlength="10" class="${inputclass}" />
																	<div class="drawBox" data-level="second">
																		<input type="hidden" name="coupon" value="${dd.coupon}" maxlength="10" class="draw" />
																		<a href="javascript:void(0)" onclick="openCounpon(this);" id="drawAdbtn">请选择红包</a>
																		<div class="coupon-example">
																			<div class="cou-box">
																				<div class="cou-info">
																					<p class="cou-name">
																						<b>红包券</b>
																						<em class="coupon-type">
																							<c:choose>
																								<c:when test="${dd.drawCoupon.couponType eq 'shop'}">
																									店铺红包
																								</c:when>
																								<c:otherwise>
																									通用红包
																								</c:otherwise>
																							</c:choose>
																						</em>
																					</p>
																					<p class="cou-des coupon-name">${dd.drawCoupon.couponName}</p>
																					<p class="cou-num">
																						剩余
																						<em class="coupon-number">${dd.drawCoupon.couponNumber -  dd.drawCoupon.bindCouponNumber}</em>
																						张
																					</p>
																				</div>
																				<div class="cou-price">
																					<p class="price">
																						&#65509;<b class="offPrice">${dd.drawCoupon.offPrice }</b>
																					</p>
																					<p class="limit">
																						满
																						<span class="fullPrice">${dd.drawCoupon.fullPrice}</span>
																						元可用
																					</p>
																				</div>
																				<div class="cou-mid">
																					<p class="up"></p>
																					<p class="down"></p>
																				</div>
																			</div>
																			<div class="cou-del">
																				<a href="javascript:void(0);" onclick="cleanCoupon(this)">删除</a>
																			</div>
																		</div>
																	</div>
																</c:when>
														</c:choose></td>
													<td>
														<div align="right">
															二等奖数量：
														</div>
													</td>
													<td align="left" style="padding-left: 2px;"><input type="input" id="amount" name="amount"
														value="${dd.amount }" maxlength="10" /></td>
													<td>
														<div align="right">
															中奖率：
														</div>
													</td>
													<td align="left" style="padding-left: 2px;"><input type="text" id="weight" name="weight"
														value="${dd.weight }" maxlength="10" />&nbsp;%</td>
												</tr>
											</c:when>

											<c:when test="${dd.id eq 3 }">
												<tr>
													<td>
														<div align="right">
															三等奖：
														</div>
													</td>
													<td align="left" style="padding-left: 2px;">奖项类型: <select id="awardsType"
														class="${selectclass}" name="awardsType"
														onchange="awardsSelect(this.options[this.options.selectedIndex].value,this);">
															<option value="1"
																<c:if test="${dd.awardsType eq 1}" > selected="selected" </c:if>>实体物</option>
															<option value="2"
																<c:if test="${dd.awardsType eq 2}" > selected="selected" </c:if>>现金</option>
															<option value="3"
																<c:if test="${dd.awardsType eq 3}" > selected="selected" </c:if>>积分</option>
															<option value="4"
																<c:if test="${dd.awardsType eq 4}" > selected="selected" </c:if>>红包</option>
													</select>
													</td>

													<td>
														<div align="right" class="AwardName">
															<c:choose>
																<c:when test="${dd.awardsType eq 1}">
										          	                                               奖品:
										          	       </c:when>
																<c:when test="${dd.awardsType eq 2}">
										          	                                             预付款金额:
										          	       </c:when>
																<c:when test="${dd.awardsType eq 3}">
										          	                                              积分:
										          	       </c:when>
																<c:when test="${dd.awardsType eq 4}">
										          	                                              红包:
										          	       </c:when>
															</c:choose>
															<font color="ff0000">*</font>
														</div>
													</td>
													<td align="left" style="padding-left: 2px;"><input type="input" id="dtoId" name="dtoId"
														value="3" style="display: none;" class="${inputclass}" />
														<c:choose>
															<c:when test="${dd.awardsType eq 1}">
																<input type="input" id="giftName" name="giftName"
																	value="${dd.giftName }" maxlength="20"
																	placeholder="请输入奖品名称" class="${inputclass}" />
																<input type="input" id="cash" name="cash" value=""
																	placeholder="请输入金额" style="display: none;"
																	maxlength="10" class="${inputclass}" />
																<input type="input" id="integral" name="integral"
																	value="" placeholder="请输入积分" style="display: none;"
																	maxlength="10" class="${inputclass}" />
																<div class="drawBox" style="display: none;" data-level="third">
																		<input type="hidden" name="coupon" value="" maxlength="10" class="draw" />
																		<a href="javascript:void(0)" onclick="openCounpon(this);" id="drawAdbtn">请选择红包</a>
																		<div class="coupon-example" style="display: none;">
																			<div class="cou-box">
																				<div class="cou-info">
																					<p class="cou-name">
																						<b>红包券</b>
																						<em class="coupon-type">全店通用</em>
																					</p>
																					<p class="cou-des coupon-name">适用全品类商品</p>
																					<p class="cou-num">
																						剩余
																						<em class="coupon-number">108</em>
																						张
																					</p>
																				</div>
																				<div class="cou-price">
																					<p class="price">
																						&#65509;<b class="offPrice">50</b>
																					</p>
																					<p class="limit">
																						满
																						<span class="fullPrice">200</span>
																						元可用
																					</p>
																				</div>
																				<div class="cou-mid">
																					<p class="up"></p>
																					<p class="down"></p>
																				</div>
																			</div>
																			<div class="cou-del">
																				<a href="javascript:void(0);" onclick="cleanCoupon(this)">删除</a>
																			</div>
																		</div>
																	</div>
															</c:when>
															<c:when test="${dd.awardsType eq 2}">
																<input type="input" id="giftName" name="giftName"
																	value="" placeholder="请输入奖品名称" maxlength="20"
																	class="${inputclass}" style="display: none;" />
																<input type="input" id="cash" name="cash"
																	value="${dd.cash }" maxlength="10" placeholder="请输入金额"
																	class="${inputclass}" />
																<input type="input" id="integral" name="integral"
																	value="" maxlength="10" placeholder="请输入积分"
																	style="display: none;" class="${inputclass}" />
																<div class="drawBox" style="display: none;" data-level="third">
																		<input type="hidden" name="coupon" value="" maxlength="10" class="draw" />
																		<a href="javascript:void(0)" onclick="openCounpon(this);" id="drawAdbtn">请选择红包</a>
																		<div class="coupon-example" style="display: none;">
																			<div class="cou-box">
																				<div class="cou-info">
																					<p class="cou-name">
																						<b>红包券</b>
																						<em class="coupon-type">全店通用</em>
																					</p>
																					<p class="cou-des coupon-name">适用全品类商品</p>
																					<p class="cou-num">
																						剩余
																						<em class="coupon-number">108</em>
																						张
																					</p>
																				</div>
																				<div class="cou-price">
																					<p class="price">
																						&#65509;<b class="offPrice">50</b>
																					</p>
																					<p class="limit">
																						满
																						<span class="fullPrice">200</span>
																						元可用
																					</p>
																				</div>
																				<div class="cou-mid">
																					<p class="up"></p>
																					<p class="down"></p>
																				</div>
																			</div>
																			<div class="cou-del">
																				<a href="javascript:void(0);" onclick="cleanCoupon(this)">删除</a>
																			</div>
																		</div>
																	</div>
															</c:when>
															<c:when test="${dd.awardsType eq 3}">
																<input type="input" id="giftName" name="giftName"
																	value="" placeholder="请输入奖品名称" maxlength="20"
																	class="${inputclass}" style="display: none;" />
																<input type="input" id="cash" name="cash" value=""
																	placeholder="请输入金额" maxlength="10"
																	style="display: none;" class="${inputclass}" />
																<input type="input" id="integral" name="integral"
																	value="${dd.integral }" maxlength="10"
																	placeholder="请输入积分" class="${inputclass}" />
																<div class="drawBox" style="display: none;" data-level="third">
																		<input type="hidden" name="coupon" value="" maxlength="10" class="draw" />
																		<a href="javascript:void(0)" onclick="openCounpon(this);" id="drawAdbtn">请选择红包</a>
																		<div class="coupon-example" style="display: none;">
																			<div class="cou-box">
																				<div class="cou-info">
																					<p class="cou-name">
																						<b>红包券</b>
																						<em class="coupon-type">全店通用</em>
																					</p>
																					<p class="cou-des coupon-name">适用全品类商品</p>
																					<p class="cou-num">
																						剩余
																						<em class="coupon-number">108</em>
																						张
																					</p>
																				</div>
																				<div class="cou-price">
																					<p class="price">
																						&#65509;<b class="offPrice">50</b>
																					</p>
																					<p class="limit">
																						满
																						<span class="fullPrice">200</span>
																						元可用
																					</p>
																				</div>
																				<div class="cou-mid">
																					<p class="up"></p>
																					<p class="down"></p>
																				</div>
																			</div>
																			<div class="cou-del">
																				<a href="javascript:void(0);" onclick="cleanCoupon(this)">删除</a>
																			</div>
																		</div>
																	</div>
															</c:when>
															<c:when test="${dd.awardsType eq 4}">
																	<input type="input" id="giftName" name="giftName" value="" placeholder="请输入奖品名称" maxlength="20" class="${inputclass}" style="display: none;" />
																	<input type="input" id="cash" name="cash" value="" placeholder="请输入金额" maxlength="10" style="display: none;" class="${inputclass}" />
																	<input type="input" id="integral" name="integral" value="" maxlength="10" style="display: none;" placeholder="请输入积分" class="${inputclass}" />
																	<div class="drawBox" data-level="third">
																		<input type="hidden" name="coupon" value="${dd.coupon}" maxlength="10" class="draw" />
																		<a href="javascript:void(0)" onclick="openCounpon(this);" id="drawAdbtn">请选择红包</a>
																		<div class="coupon-example">
																			<div class="cou-box">
																				<div class="cou-info">
																					<p class="cou-name">
																						<b>红包券</b>
																						<em class="coupon-type">
																							<c:choose>
																								<c:when test="${dd.drawCoupon.couponType eq 'shop'}">
																									店铺红包
																								</c:when>
																								<c:otherwise>
																									通用红包
																								</c:otherwise>
																							</c:choose>
																						</em>
																					</p>
																					<p class="cou-des coupon-name">${dd.drawCoupon.couponName}</p>
																					<p class="cou-num">
																						剩余
																						<em class="coupon-number">${dd.drawCoupon.couponNumber -  dd.drawCoupon.bindCouponNumber}</em>
																						张
																					</p>
																				</div>
																				<div class="cou-price">
																					<p class="price">
																						&#65509;<b class="offPrice">${dd.drawCoupon.offPrice }</b>
																					</p>
																					<p class="limit">
																						满
																						<span class="fullPrice">${dd.drawCoupon.fullPrice}</span>
																						元可用
																					</p>
																				</div>
																				<div class="cou-mid">
																					<p class="up"></p>
																					<p class="down"></p>
																				</div>
																			</div>
																			<div class="cou-del">
																				<a href="javascript:void(0);" onclick="cleanCoupon(this)">删除</a>
																			</div>
																		</div>
																	</div>
																</c:when>
														</c:choose></td>
													<td>
														<div align="right">
															<font color="ff0000">*</font>三等奖数量：
														</div>
													</td>
													<td align="left" style="padding-left: 2px;"><input type="input" id="amount" name="amount"
														value="${dd.amount }" maxlength="10" /></td>
													<td>
														<div align="right">
															中奖率：
														</div>
													</td>
													<td align="left" style="padding-left: 2px;"><input type="text" id="weight" name="weight"
														value="${dd.weight }" maxlength="10" />&nbsp;%</td>
												</tr>
											</c:when>
										</c:choose>
									</c:forEach>
								</table>
							</tr>
						</c:if>




						<tr>
							<td>
								<div align="left" style="padding-left: 2px;">&nbsp;&nbsp;奖项权重设置说明:</div>
							</td>
							<td>
								<div align="left" style="padding-left: 2px;" style="color: red;">
									<ul>
										<li>例如:一等奖概率:1% &nbsp;&nbsp; 二等奖概率:5%&nbsp;&nbsp;
											三等奖概率:9% <!-- &nbsp;&nbsp;计算得出最小公倍数为600 -->
										</li>
										<li>计算各奖项区间标识:&nbsp;一等奖=一等奖概率/100*10000-1&nbsp;&nbsp;
											二等奖=二等奖概率/100*10000-一等奖中奖区间-1
											&nbsp;&nbsp;三等奖=三等奖概率/100*10000-（一等奖中奖区间+二等奖中奖区间）-1</li>
										<li>根据上述公式分别得出:&nbsp; 一等奖：100 &nbsp;&nbsp;&nbsp;二等奖 ：500
											&nbsp;&nbsp;&nbsp;三等奖:900</li>
										<li>各中奖区间为:&nbsp; 一等奖(0~99) &nbsp;&nbsp;
											二等奖(100~499)&nbsp;&nbsp;
											三等奖(500~899)&nbsp;&nbsp;【在10000范围内随机抽取一整数，落在区间则中奖，反之，不中奖】</li>
										<li>总结:&nbsp; 抽取随机数越大，中奖的概率越小</li>
									</ul>
								</div>
							</td>
						</tr>

						<tr>
							<!-- id="operBtns" -->
							<td colspan="2">
								<div align="center" style="margin-bottom: 80px;">
									<input class="${btnclass}" type="submit" value="保存" /> <input
										class="${btnclass}" type="button" value="返回"
										onclick="window.location='<ls:url address="/admin/drawActivity/query"/>'" />
								</div>
							</td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>
<script src="${contextPath}/resources/common/js/jquery.form.js"
	type="text/javascript"></script>
<script src="${contextPath}/resources/common/js/jquery.validate.js"
	type="text/javascript"></script>
<script charset="utf-8"
	src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script language="javascript">
	$(document).ready(function() {
		jQuery("#form1").validate({
			rules : {
				actType : {required : true},
				actName : {required : true},
				actStatus : {required : true},
				startTime : {required : true},
				endTime : {required : true},
				timesPerday : {required : true,digits : true},
				timesPerson : {required : true,digits : true},
				winJoinStatus : {required : true},
				bindJoinStatus : {required : true},
				actRule : {required : true,maxlength : 450}
			},
			messages : {
				actType : {
					required : "必填"
				},
				actName : {
					required : "必填"
				},
				actStatus : {
					required : "必选"
				},
				startTime : {
					required : "必填"
				},
				endTime : {
					required : "必填"
				},
				timesPerday : {
					required : "必填",
					digits : "请输入数字"
				},
				timesPerson : {
					required : "必填",
					digits : "请输入数字"
				},
				winJoinStatus : {
					required : "必选"
				},
				bindJoinStatus : {
					required : "必选"
				},
				actRule : {
					required : "必填",
					maxlength : "超出最大长度(450)限制了"
				}
			},
			submitHandler : function(form) {
				var json = checkAwards();
				if (json != "") {
					$("#awardInfo").attr(
							"value", json);
				} else {
					return;
				}

				if ((timesPerday = $("#timesPerday").val()- $("#timesPerson").val()) > 0) {
					layer.alert('个人每日抽奖次数不能大于个人抽奖总次数！',{icon : 0});
					return;
				}

				$(form).ajaxForm().ajaxSubmit({
					type : 'post',
					async : true,
					dataType : 'json',
					success : function(
							data) {
						if (data == "OK") {
							window.location.href = '${contextPath}/admin/drawActivity/query';
						} else {
							layer.alert('操作失败！请重试！',{icon : 2});
						}
					},
					error : function(data) {
						layer.alert(data,{icon : 0});
					},
				});
			}
		});

		KindEditor.options.filterMode = false;
		/*  编辑器初始化 */
		KindEditor.create('textarea[name="actDes"]',{
			cssPath : '${contextPath}/resources/plugins/kindeditor/plugins/code/prettify.css',
			uploadJson : '${contextPath}/editor/uploadJson/upload;jsessionid=${cookie.SESSION.value}',
			fileManagerJson : '${contextPath}/editor/uploadJson/fileManager',
			allowFileManager : true,
			afterBlur : function() {
				this.sync();
			},
			width : '80%',
			height : '300px',
			afterCreate : function() {
				var self = this;
				KindEditor.ctrl(document,13,function() {
									self.sync();
									document.forms['example']
											.submit();
								});
				KindEditor.ctrl(self.edit.doc,13,function() {
					
									self.sync();
									document.forms['example']
											.submit();
				});
			}
		});

	});

function isBlank(_value) {
	if (_value == null || _value == "" || _value == undefined) {
		return true;
	}
	return false;
}

/*根据奖项更改奖项类型显示*/
function awardsSelect(status, _obj) {
	var obj = _obj;
	if (status == 1) {
		$(_obj).parent().parent().find("div[class=AwardName]").html(
				"奖品：<font color='ff0000'>*</font>");
		$(_obj).parents("tr").find("input[name=giftName]").show();
		$(_obj).parents("tr").find("input[name=cash]").hide();
		$(_obj).parents("tr").find("input[name=integral]").hide();
		$(_obj).parents("tr").find(".drawBox").hide();
	} else if (status == 2) {
		$(_obj).parent().parent().find("div[class=AwardName]").html(
				"预付款金额：<font color='ff0000'>*</font>");
		$(_obj).parents("tr").find("input[name=giftName]").hide();
		$(_obj).parents("tr").find("input[name=cash]").show();
		$(_obj).parents("tr").find("input[name=integral]").hide();
		$(_obj).parents("tr").find(".drawBox").hide();
	} else if (status == 3) {
		$(_obj).parent().parent().find("div[class=AwardName]").html(
				"积分：<font color='ff0000'>*</font>");
		$(_obj).parents("tr").find("input[name=giftName]").hide();
		$(_obj).parents("tr").find("input[name=cash]").hide();
		$(_obj).parents("tr").find("input[name=integral]").show();
		$(_obj).parents("tr").find(".drawBox").hide();
	} else if (status == 4) {
		$(_obj).parent().parent().find("div[class=AwardName]").html(
				"红包：<font color='ff0000'>*</font>");
		$(_obj).parents("tr").find("input[name=giftName]").hide();
		$(_obj).parents("tr").find("input[name=cash]").hide();
		$(_obj).parents("tr").find("input[name=integral]").hide();
		$(_obj).parents("tr").find(".drawBox").show();
	}
}

/*奖项*/
function checkAwards() {
	var re = /^[1-9]+[0-9]*]*$/; //整数值
	var regDouble = /^[+]?[\d]+(([\.]{1}[\d]+)|([\d]*))$/; //整浮点数
	var awardsItems = $("#awardsSet tr");
	var retJson = "";
	var error = "";
	var seq = "";
	var awardsType = "";//奖项类型
	var flag = true;
	$.each(awardsItems, function(i, vo) {
		awardsType = $(vo).find("option:selected").val();
		var id = $(vo).find("input[id=dtoId]").val();
		if (id == 1) {
			seq = "一等奖";
		} else if (id == 2) {
			seq = "二等奖";
		} else if (id == 3) {
			seq = "三等奖";
		}

		var weight = $(vo).find("input[id=weight]").val(); //权重
		var amount = $(vo).find("input[id=amount]").val(); //各奖项数量
		if (isBlank(amount) || !re.test(amount) || isNaN(amount)
				|| isBlank(weight) || !re.test(weight) || isNaN(weight)) {
			flag = false;
		}

		//根据奖项类型传json
		if (awardsType == 1) {
			var giftName = $(vo).find("input[id=giftName]").val(); //奖品名称
			if (isBlank(giftName) || flag == false) {
				error += seq + ","
				return false;
			}
			retJson += "{\"id\":\"" + id + "\"," + "\"giftName\":\""
					+ giftName + "\"," + "\"amount\":\"" + amount + "\","
					+ "\"awardsType\":\"" + awardsType + "\","
					+ "\"weight\":\"" + weight + "\"},";
		} else if (awardsType == 2) {
			var cash = $(vo).find("input[id=cash]").val();//现金
			if (isNaN(cash) || !regDouble.test(cash) || isBlank(cash)
					|| flag == false) {
				error += seq + ","
				return false;
			}
			retJson += "{\"id\":\"" + id + "\"," + "\"cash\":\"" + cash
					+ "\"," + "\"amount\":\"" + amount + "\","
					+ "\"awardsType\":\"" + awardsType + "\","
					+ "\"weight\":\"" + weight + "\"},";
		} else if (awardsType == 3) {
			var integral = $(vo).find("input[id=integral]").val(); //积分
			if (isNaN(integral) || !re.test(integral) || isBlank(integral)
					|| flag == false) {
				error += seq + ","
				return false;
			}
			retJson += "{\"id\":\"" + id + "\"," + "\"integral\":\""
					+ integral + "\"," + "\"amount\":\"" + amount + "\","
					+ "\"awardsType\":\"" + awardsType + "\","
					+ "\"weight\":\"" + weight + "\"},";
		}else if (awardsType == 4) {
			var coupon = $(vo).find(".draw").val(); //红包
			if (isNaN(coupon) || !re.test(coupon) || isBlank(coupon) || flag == false) {
				error += seq + ","
				return false;
			}
			retJson += "{\"id\":\"" + id + "\"," + "\"coupon\":\"" + coupon + "\"," + "\"amount\":\"" + amount + "\"," + "\"awardsType\":\"" + awardsType + "\"," + "\"weight\":\"" + weight + "\"},";
		}

	});

	if (error != "") {
		error = error.substring(0, error.length - 1)
		layer.alert(error + "信息有误(非空、整数值)，请检查！", {
			icon : 2
		});
		return false;
	}

	if (retJson != "") {
		retJson = retJson.substring(0, retJson.length - 1)
		retJson = "[" + retJson + "]";
	}
	return retJson;
}

laydate.render({
	elem : '#startTime',
	type : 'datetime',
	calendar : true,
	theme : 'grid',
	trigger: 'click'
});

laydate.render({
	elem : '#endTime',
	type : 'datetime',
	calendar : true,
	theme : 'grid',
	trigger: 'click'
});
/**选择优惠券**/
function openCounpon(ths) {
	//校验是否选择时间
	var startDate = $('#startTime').val();
	var endDate = $('#endTime').val();
	
	if(isBlank(startDate) || isBlank(endDate)) {
		art.dialog.tips("请先选择活动时间");
		return false;
	}
	var _level = $(ths).parent().attr("data-level");
	var url = contextPath+"/admin/drawActivity/loadDrawCoupon/" + _level + "?endDate=" + endDate + "&startDate" + startDate;
	art.dialog.open(url,{
		id:'loadSelectCouponDialog',
		width: 700,
		height:500,
	  	title: '选择红包券',
	  	lock: true,
	  	fixed:true,
	  	opacity: 0.2
	});
}
//选择优惠券
function addCouponTo(level, couponId, couponName, couponType, fullPrice, offPrice, number){
	//检查页面是否已经选择该红包
	var _drawList = $(".draw");
	for (var i = 0; i < _drawList.length; i++) {
		var _couponId = $(_drawList[i]).val();
		if(couponId == _couponId) {
			art.dialog.tips("该红包已经选择，请更换选择");
			return false;
		}
	}
	//替换页面展示红包券参数
	var type = "";
	if ("shop"== couponType) {
		type="店铺红包";
	}else{
		type="通用红包";
	}
	$("div[data-level="+level+"]").find('.coupon-type').html(type);
	$("div[data-level="+level+"]").find('.coupon-name').html(couponName);
	$("div[data-level="+level+"]").find('.coupon-number').html(number);
	$("div[data-level="+level+"]").find('.offPrice').html(offPrice);
	$("div[data-level="+level+"]").find('.fullPrice').html(fullPrice);
	$("div[data-level="+level+"]").find('.draw').val(couponId);
	$("div[data-level="+level+"]").find('.coupon-example').show();
	//关闭art.dialog 弹层
	art.dialog({id:"loadSelectCouponDialog"}).close();
}

//删除选中优惠券
function cleanCoupon(ths){
	var _drawBox = $(ths).parents(".drawBox");
	$(_drawBox).find(".draw").val("");
	$(_drawBox).find('.coupon-example').hide();
};
</script>
</html>
