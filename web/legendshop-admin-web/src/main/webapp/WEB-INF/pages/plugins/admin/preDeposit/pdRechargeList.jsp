<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>用户管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">预存款管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">预存款管理</span></th>
				</tr>
			</table>
			<div>
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li class="am-active"><i></i><a>充值管理</a></li>
						<li><i></i><a
							href="<ls:url address="/admin/preDeposit/pdWithdrawCashQuery"/>">提现管理</a></li>
						<li><i></i><a
							href="<ls:url address="/admin/preDeposit/pdCashLogQuery"/>">预存款明细</a></li>
					</ul>
				</div>
			</div>
			<form:form action="${contextPath}/admin/preDeposit/pdRechargeQuery"
				id="form1" method="post">
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
					<span class="item">
						用户名：<input class="${inputclass}" type="text" name="userName" id="userName" maxlength="50" value="${bean.userName}" placeholder="用户名" /> 
					</span>
					<span class="item">
						用户昵称：<input class="${inputclass}" type="text" name="nickName" id="nickName" maxlength="50" value="${bean.nickName}" placeholder="用户昵称" />
					</span>
					<span class="item">
					 	状态： 
						 <select id="paymentState"
							name="paymentState" class="${selectclass}">
							<ls:optionGroup type="select" required="false" cache="true"
								beanName="PRE_DEPOSIT_STATUS"
								selectedValue="${bean.paymentState}" />
						</select> 
						<input class="${btnclass}" onclick="search()" type="submit" value="搜索"/>
					</span>
				</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>

				<display:table name="list"
					requestURI="/admin/preDeposit/pdRechargeQuery" id="item"
					export="false" sort="external" class="${tableclass}">
					<%-- <display:column
						title='<input type="checkbox" id="checkboxAll" name="pdId" value="${item.id}">'
						class="orderwidth">
						<input type="checkbox" name="pdId" value="${item.id}"
							can-del="${item.paymentState eq 0}"
							<c:if test="${item.paymentState eq 1}">disabled=true</c:if> />
					</display:column> --%>
					
					<display:column title="
						<span>
				           <label class='checkbox-wrapper'>
								<span class='checkbox-item'>
									<input type='checkbox' name='pdId' value='${item.id}' class='checkbox-input selectAll' onclick='selectAll(this);'/>
									<span class='checkbox-inner' style='margin-left:10px;'></span>
								</span>
						   </label>	
						</span>" style="width:100px;">
						<c:choose>
				    		<c:when test="${item.paymentState eq 1}">
							   <label class="checkbox-wrapper checkbox-wrapper-disabled">
									<span class="checkbox-item">
										<input type="checkbox" value="${item.id}" can-del="${item.paymentState eq 0}" class="checkbox-input"  disabled='disabled'"/>
										<span class="checkbox-inner" style="margin-left:10px;"></span>
									</span>
							   </label>	
				    		</c:when>
				    		<c:otherwise>
				    			<label class="checkbox-wrapper">
									<span class="checkbox-item">
										<input type="checkbox" value="${item.id}" can-del="${item.paymentState eq 0}" class="checkbox-input selectOne" onclick="selectOne(this);"/>
										<span class="checkbox-inner" style="margin-left:10px;"></span>
									</span>
							   </label>	
				    		</c:otherwise>
				    	</c:choose>
						
						
						<%-- <input type="checkbox" name="pdId" value="${item.id}"
							can-del="${item.paymentState eq 0}"
							<c:if test="${item.paymentState eq 1}">disabled=true</c:if> /> --%>
					</display:column>
					
					<%-- <display:column title="顺序" class="orderwidth"
						style="min-width:40px"><%=offset++%></display:column> --%>
					<display:column title="充值流水号" property="sn" style="width: 200px;"></display:column>
					<display:column title="用户名" property="userName"
						style="min-width:30px"></display:column>
					<display:column title="用户昵称" property="nickName"
						style="word-wrap:break-word;max-width:460px"></display:column>
					<display:column title="充值金额(元)" property="amount" sortable="true"
						sortName="amount"></display:column>
					<display:column title="支付方式" property="paymentName" sortable="true"
						sortName="payment_name"></display:column>
					<display:column title="创建时间" format="{0,date,yyyy-MM-dd HH:mm:ss}"
						property="addTime" style="min-width:90px" sortable="true"
						sortName="add_time"></display:column>
					<display:column title="支付时间" format="{0,date,yyyy-MM-dd HH:mm:ss}"
						property="paymentTime" style="min-width:90px" sortable="true"
						sortName="payment_time"></display:column>
					<display:column title="支付状态">
						<c:choose>
							<c:when test="${item.paymentState eq 1}">
								<font style="color:green;">已支付</font>
							</c:when>
							<c:when test="${item.paymentState eq 0}">
								<font style="color: #ff6c00;">未支付</font>
							</c:when>
							<c:otherwise>
								<font style="color: red;">错误</font>
							</c:otherwise>
						</c:choose>
					</display:column>
					<display:column title="操作" media="html">
						<div class="table-btn-group">
							<c:choose>
								<c:when test="${item.paymentState eq 0}">
									<button class="tab-btn" onclick="showPdRechargeInfo(${item.id})">
										查看
									</button>
									<span class="btn-line">|</span>
									<button class="tab-btn" onclick="deleteById(${item.id})">
										删除
									</button>
								</c:when>
								<c:when test="${item.paymentState eq 1}">
									<button class="tab-btn" onclick="showPdRechargeInfo(${item.id})">
										查看
									</button>
								</c:when>
								<c:otherwise>
								</c:otherwise>
							</c:choose>
						</div>
					</display:column>
				</display:table>
				<div class="clearfix">
		       		<div class="fl">
			       		<input type="button" value="批量删除" id="batch-del" class="batch-btn">
					</div>
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
				<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass }">
					<tr>
						<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br> 1.可以点击查看浏览本次充值的详细信息<br>
							2.如果系统平台已确认收到充值款，但系统的充值单还是未支付状态，可以点击查看手动更改成已支付状态<br>
						</td>
					<tr>
				</table>
			</div>
		</div>
	</div>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/pdRechargeList.js'/>"></script>
<script type="text/javascript">
	var contextPath = '${contextPath}';
	
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		sendData(1);
	}
</script>

</html>