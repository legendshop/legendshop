<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
 <%@ include file="/WEB-INF/pages/common/layer.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
 <%@ include file="/WEB-INF/pages/common/jquery1.9.1.jsp"%>
<script type="text/javascript" src="<ls:templateResource item='/resources/common/js/jquery.showLoading.min.js'/>" ></script>
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/showLoading.css" />
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/weixin/js/dialogOpen.js'/>" ></script>
    <%
        Integer offset = (Integer) request.getAttribute("offset");
    %>
<style type="text/css"> 
input[type=button]:hover{border: 1px solid #e9e9e9;}
select:hover{border:2px solid #8bade4;cursor:pointer;}
</style>
 <form:form  action="${contextPath}/admin/weixinUser/query" id="form1" method="get">
        <table class="${tableclass}" style="width: 100%">
		    <thead>
		    	<tr>
			    	<th>
				    	<strong><a class="am-text-primary am-text-lg" href="<ls:url address='/admin/index'/>" target="_parent">微信管理</a></strong> / 微信用户管理 
			    	</th>
		    	</tr>
		    </thead>
		    <tbody><tr><td>
		    	    <div align="left" style="padding: 3px">
				       	    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
				                              昵称：<input class="${inputclass}" type="text" name="nickname" maxlength="50" value="${key}" />
				            <input class="${btnclass}" type="button" onclick="search()" value="搜索"/>
				      		<%-- <input class="${btnclass}" type="button" value="新增分组" onclick="addGroup()"/>
				     		<select class="${selectclass}" id="group" name="groupid">
				     			<c:forEach items="${weixinGroupList}" var="winxinGroup">
									<option value="${winxinGroup.id}" <c:if test="${winxinGroup.id==groupId}">selected="selected"</c:if> >
										${winxinGroup.name}			
									</option>
								</c:forEach>
				     		</select>
				     		<input class="${btnclass}" type="button" value="删除分组" onclick="deleteById('0','1');"/> --%>
				     		<input class="${btnclass}" type="button" value="同步微信用户" onclick="downUser()" style="margin-left:270px"/>
				     		<%-- <input class="${btnclass}" type="button" value="下拉微信分组" onclick="downGroup()" style="margin-left:10px"/>  --%>
				      </div>
		     </td></tr></tbody>
		     
	    </table>
    </form:form>
    <div>
      <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
      <c:choose> 
      	<c:when test="${not empty list}">
				<display:table name="list"  id="item"  sort="external" class="${tableclass}">
			     		<display:column title="昵称" property="nickname"></display:column>
			     		<display:column title="头像" >
			     		<image width="50" height="50" src="${item.headimgurl}"/>
			     		</display:column>
			     		<display:column title="分组名称" property="groupName"></display:column>
			     		<display:column title="是否关注">
			     			<c:choose>
			     				<c:when test="${item.subscribe=='Y'}">
			     					是
			     				</c:when>
			     				<c:otherwise>否</c:otherwise>
			     			</c:choose>
			     		</display:column>
			     		<display:column title="备注" property="bzname"></display:column>
				    <display:column title="操作" media="html">
				    	<div class="am-btn-toolbar">
				      		<div class="am-btn-group am-btn-group-xs">
							      <button id="check" class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="window.location='${contextPath}/admin/weixinUser/load/${item.id}'"><span class="am-icon-search" ></span> 查看</button>
			      				   <%-- <button class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="changeUserGroup('${item.id}','${item.groupid}','${item.groupName}');"><span class="am-icon-pencil-square-o"></span>变更分组</button> --%>
			      				   <button class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="changeBzname('${item.id}','${item.bzname}')"><span class="am-icon-pencil-square-o"></span>修改备注</button>
			      				<!--<button class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" onclick="deleteById('${item.id}','0');" ><span class="am-icon-trash-o"></span> 删除</button>  -->
							</div>
						</div>
				      </display:column>
			    </display:table>
		</c:when>
			    <c:otherwise>
			    <div style="margin-left: 400px">
				    <span style="color: #f60;font-size: 16px" >抱歉，没有找到“</span>
				    <em style="font-style: normal">${key}</em>
				    <span style="color: #f60;font-size: 16px">”的搜索结果!</span>
			    </div>
			    </c:otherwise> 
		</c:choose>
        <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/> 
    </div>
        <script language="JavaScript" type="text/javascript">
        var contextPath = '${contextPath}';
        function downUser(){
        	layer.confirm("同步微信用户过程比较缓慢,请勿重复操作?", {
       		 icon: 3
       	     ,btn: ['确定','关闭'] //按钮
       	   }, function(){
       		  //显示遮罩
			    $("body").showLoading();
			     $.ajax({
				 type: "GET",
				 url:contextPath+"/admin/weixinUser/synchronousUser",
				success:function(data){
					var result=eval(data);
						if(result=="OK"){
							layer.msg("下拉用户成功",{icon:1},function(){
								window.location = contextPath+"/admin/weixinUser/query";
								$("body").hideLoading();
							});
							
							}else {
								layer.alert(result,{icon:2});
	                  			 $("body").hideLoading();
	                  			 return false;
						}
				}
			    });
       	   });
        }
        
         function downGroup(){
        	 layer.confirm("同步微信分组过程比较缓慢,请勿重复操作?", {
        		 icon: 3
        	     ,btn: ['确定','关闭'] //按钮
        	   }, function(){
        		   //显示遮罩
    			   $("body").showLoading();
    				$.ajax({
    					 type: "GET",
    					 url:contextPath+"/admin/weixinUser/synchronousGroup",
    					 error:function(data){
    					  $("body").hideLoading();
    					  layer.msg("下拉分组失败！",{icon:2});
    					 },   
    					success:function(data){
    						var result=eval(data);
    						if(result=="OK"){
    							layer.msg("下拉微信分组成功！",{icon:1},function(){
    								 window.location = contextPath+"/admin/weixinUser/query";
        							 $("body").hideLoading();
    							});
    							
    							}else {
    								layer.alert(result,{icon:2});
    	                   			 $("body").hideLoading();
    	                   			 return false;
    						}
    					}
    				});
        	   });
        }
        
        
			  function deleteById(id,groupid) {
			  	if(groupId=0){
			  		layer.confirm("'确定删除该用户 ?", {
			  			 icon: 3
			  		     ,btn: ['确定','关闭'] //按钮
			  		   }, function(){
			  			 window.location = contextPath+"/admin/weixinUser/delete/${item.id}/0";
			  		   });
			    }else{
			    	var groupid=$("#group option:selected").attr("value");
			    	var groupName=$("#group option:selected").text();
			    	if(("未分组"!=$.trim(groupName))&&("星标组"!=$.trim(groupName))&&("黑名单"!=$.trim(groupName))){
			    		layer.confirm("'删除分组将会把该组已有成员全部移动至默认分组里，是否确定删除?", {
				  			 icon: 3
				  		     ,btn: ['确定','关闭'] //按钮
				  		   }, function(){
				  			 var address=contextPath+"/admin/weixinUser/delete/0";
					    		window.location = address+"/"+groupid;
					    		layer.msg("删除成功",{icon:1});
				  		   });
			    	}else{
			    		layer.msg("该分组不能删除",{icon:0});
			        }
			    }
			  }
			        function pager(curPageNO){
			            document.getElementById("curPageNO").value=curPageNO;
			            document.getElementById("form1").submit();
			        }
			        
			 function search(){
			  	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
			  	$("#form1")[0].submit();
			}      
			      
		</script>
