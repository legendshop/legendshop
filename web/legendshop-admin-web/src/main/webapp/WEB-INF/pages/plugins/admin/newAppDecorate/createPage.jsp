<!Doctype html>
<html class="no-js fixed-layout">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<%@include file='/WEB-INF/pages/common/laydate.jsp'%>
<%@ include file="../back-common.jsp"%>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>移动装修- 页面装修</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />

	<!-- 引入线上element-ui组件库 -->
	<link rel="stylesheet" href="https://unpkg.com/element-ui@2.10.0/lib/theme-chalk/index.css">

	<link rel="stylesheet" href="${contextPath}/resources/common/css/swiper/swiper.min.css"/>
	<link rel="stylesheet" href="${contextPath}/resources/plugins/kindeditor/themes/default/default.css"/>
	<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/index.css'/>" />
	<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/floors.css'/>" />
	<style type="text/css">
		*+p {
			margin: 0 !important;
		}
		.table-main {
			min-width: auto !important;
		}
	</style>
</head>

<body>
<jsp:include page="/admin/top" />
<div class="am-cf admin-main">
	<!-- sidebar start -->
	<jsp:include page="../frame/left.jsp"></jsp:include>
	<!-- sidebar end -->
	<div class="admin-content" id="admin-content" style="overflow-x: auto;">
		<div class="seller_right" style="margin-bottom:70px;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">页面装修 &nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">页面编辑<span></th>
				</tr>
			</table>


			<!-- vue结构 -->
			<div id="app" v-cloak>
				<div class="site-editor" id="myPanel" >
					<div class="editor-container" ref="box">
						<div class="wrapper-header"></div>
						<!-- v-clickoutside="handleClose" -->
						<!-- v-show="show" -->
						<div class="wrapper-content" >
							<div class="cont-header">首页</div>
							<div class="site-widget-list">
								<div class="floors" v-for="(item,floorIndex) in floorData.components" :key="floorIndex" style="position: relative;">
									<!-- 富文本 -->
									<div class="widget-list-itme" v-if="item.type == 'richText'">
										<div  class="itme-content" @mouseenter="enter(floorIndex,$event)" :class=" floorIndex == action ? 'on' : '' " @click="clickSelect(item,floorIndex)">
											<div :style="{'background': item.background}" v-html="item.content" id="richText">{{item.content}}</div>
										</div>
									</div>
									<!-- 热区 -->
									<div class="widget-list-itme" v-if="item.type == 'hotspot'">
										<div class="itme-content" @mouseenter="enter(floorIndex,$event)" :class=" floorIndex == action ? 'on' : '' " @click="clickSelect(item,floorIndex)">
											我是热区
										</div>
									</div>
									<!-- 富文本 end -->
									<!-- 轮播图 -->
									<div class="widget-list-itme" v-if="item.type == 'slider'">
										<div class="itme-content" @mouseenter="enter(floorIndex,$event)" :class=" floorIndex == action ? 'on' : '' " @click="clickSelect(item,floorIndex)">



											<!-- <div class="swiper-container">
															<div class="swiper-wrapper" :style="{'height': item.height}" >
																<template v-if="item.photos && item.photos.length" >
																	<div class="swiper-slide" v-for="(item, photosIndex) in item.photos" :key="photosIndex" >
																		<img :src="item.path ? imageServer + item.path : '${contextPath}/resources/common/images/appDecorate/moren.png'" alt=""  >
																	</div>
																</template>

																<template v-else>
																	<div class="swiper-slide">
																		<img src="${contextPath}/resources/common/images/appDecorate/moren.png" alt=""  >
																	</div>
																</template>

															</div>


															<div class="indicator" v-if="item.markStyle == 'point'" >
																<span :class="indicatorIndex == index ? 'on' : ''" v-for="(item, index) in item.photos.length" :key="index"></span>
															</div>


														</div> -->







											<!-- <div class='edit-image-ul'> -->

											<el-carousel
													:height="item.height + 'px'"
													:indicator-position="item.markStyle == 'none' ? 'none' : ''"
													arrow="never"
													:initial-index="indicatorIndex"
													trigger="none"
													:class="[item.markStyle == 'point' ? 'point' : '' , item.markStyle == 'block' ? 'square': '']"
													@change="switchBanner"
											>
												<template v-if="item.photos && item.photos.length" >
													<el-carousel-item v-for="(item, photosIndex) in item.photos" :key="photosIndex" >
														<img :src="item.path ? imageServer + item.path : '${contextPath}/resources/common/images/appDecorate/moren.png'" alt=""  >
													</el-carousel-item>

												</template>

												<template v-else>
													<el-carousel-item >
														<img src="${contextPath}/resources/common/images/appDecorate/moren.png" alt=""  >
													</el-carousel-item>
												</template>

											</el-carousel>


											<!-- </div> -->




											<!-- <ul class="edit-image-ul" :style="{'height': item.height}">
															<template v-if="item.photos && item.photos.length" >
																<li v-for="(item, photosIndex) in item.photos" :key="photosIndex" :class="photosIndex == 0 ? 'on' : ''">

																</li>
															</template>
															<template v-else>
																<li class="on">
																	<img src="${contextPath}/resources/common/images/appDecorate/moren.png" alt=""  >
																</li>
															</template>
														</ul> -->
											<!-- 指示器 -->

											<!-- 指示器 end -->
										</div>
									</div>
									<!-- 轮播图 end -->

									<!-- 商品列表 -->
									<div class="widget-list-itme" v-if="item.type == 'prodList'">
										<div class="itme-content good-list-bar" @mouseenter="enter(floorIndex, $event)" :class=" floorIndex == action ? 'on' : '' " @click="clickSelect(item,floorIndex)">
											<div class="good-list-bar good-content">
												<!-- 小图两列 -->
												<div class="goods-list-2" v-if="item.listStyle == 'style1'" :style="{'background': item.background}">
													<ul class="goods-list-2-ul">

														<template v-if="item.prodSource.type == 'diy'">


															<template v-if='item.prodSource.prods && item.prodSource.prods.length'>
																<li class="goods-li" :class="item.prodStyle == 'style1' ? 'radius' : ''" v-for="(prodItem,CountIndex) in item.prodSource.prods" :key="CountIndex">
																	<div class="goods-li-item">
																		<div class="item-img">
																			<img :src="prodItem.pic?  imageServer + prodItem.pic: currentPath + '/resources/common/images/appDecorate/moren.png'" alt=""  >
																		</div>
																		<div class="goods-horn" v-if="item.displayContent.badgeStyle">
																			<template v-if="item.displayContent.badge == 'new'">
																				<img src="${contextPath}/resources/common/images/appDecorate/horn1.png" alt=""  >
																			</template>
																			<template v-if="item.displayContent.badge == 'hot'">
																				<img src="${contextPath}/resources/common/images/appDecorate/horn2.png" alt=""  >
																			</template>
																			<template v-if="item.displayContent.badge == 'custom'">
																				<img :src="item.displayContent.url? imageServer +  item.displayContent.url : currentPath + '/resources/common/images/appDecorate/moren.png'" alt=""  >
																			</template>
																		</div>
																	</div>
																	<div class="item-content _paddingTB" v-if="item.displayContent" :class="!item.displayContent.spuName && !item.displayContent.skuName && !item.displayContent.price ? 'on' : ''">
																		<span class="goods-name" v-if="item.displayContent.spuName">{{prodItem.spuName}}</span>
																		<span class="goods-spec-name" v-if="item.displayContent.skuName">{{prodItem.skuName}}</span>
																		<div class="pro-price" v-if="item.displayContent.price"><span class="price">￥<em class="now">{{Money(prodItem.price)[0]}}</em>.{{Money(prodItem.price)[1]}}</span></div>
																		<div class="goods-cat-image" v-if="item.displayContent.cartStyle">
																			<img src="${contextPath}/resources/common/images/appDecorate/car.png" alt=""  >
																		</div>
																	</div>

																</li>
															</template>

															<template v-else>
																<li class="goods-li" :class="item.prodStyle == 'style1' ? 'radius' : ''" v-for="(prodItem,CountIndex) in 4" :key="CountIndex">
																	<div class="goods-li-item">
																		<div class="item-img">
																			<img :src="prodItem.pic?  imageServer + prodItem.pic : currentPath + '/resources/common/images/appDecorate/moren.png'" alt=""  >
																		</div>
																		<div class="goods-horn" v-if="item.displayContent.badgeStyle">
																			<template v-if="item.displayContent.badge == 'new'">
																				<img src="${contextPath}/resources/common/images/appDecorate/horn1.png" alt=""  >
																			</template>
																			<template v-if="item.displayContent.badge == 'hot'">
																				<img src="${contextPath}/resources/common/images/appDecorate/horn2.png" alt=""  >
																			</template>
																			<template v-if="item.displayContent.badge == 'custom'">
																				<img :src="item.displayContent.url? imageServer +  item.displayContent.url : currentPath + '/resources/common/images/appDecorate/moren.png'" alt=""  >
																			</template>
																		</div>
																	</div>
																	<div class="item-content _paddingTB" v-if="item.displayContent" :class="!item.displayContent.spuName && !item.displayContent.skuName && !item.displayContent.price ? 'on' : ''">
																		<span class="goods-name" v-if="item.displayContent.spuName">商品示例{{CountIndex + 1}}</span>
																		<span class="goods-spec-name" v-if="item.displayContent.skuName">商品示例sku</span>
																		<div class="pro-price" v-if="item.displayContent.price"><span class="price">￥<em class="now">999</em>.00</span></div>
																		<div class="goods-cat-image" v-if="item.displayContent.cartStyle">
																			<img src="${contextPath}/resources/common/images/appDecorate/car.png" alt=""  >
																		</div>
																	</div>

																</li>
															</template>





														</template>

														<template v-else>
															<li class="goods-li" :class="item.prodStyle == 'style1' ? 'radius' : ''" v-for="(prodItem,CountIndex) in item.prodCount" :key="CountIndex">
																<div class="goods-li-item">
																	<div class="item-img">
																		<img src="${contextPath}/resources/common/images/appDecorate/moren.png" alt=""  >
																	</div>
																	<div class="goods-horn" v-if="item.displayContent.badgeStyle">
																		<template v-if="item.displayContent.badge == 'new'">
																			<img src="${contextPath}/resources/common/images/appDecorate/horn1.png" alt=""  >
																		</template>
																		<template v-if="item.displayContent.badge == 'hot'">
																			<img src="${contextPath}/resources/common/images/appDecorate/horn2.png" alt=""  >
																		</template>
																		<template v-if="item.displayContent.badge == 'custom'">
																			<img :src="item.displayContent.url? imageServer +  item.displayContent.url : currentPath + '/resources/common/images/appDecorate/moren.png'" alt=""  >
																		</template>
																	</div>
																</div>
																<div class="item-content _paddingTB" v-if="item.displayContent" :class="!item.displayContent.spuName && !item.displayContent.skuName && !item.displayContent.price  ? 'on' : ''">
																			<span class="goods-name" v-if="item.displayContent.spuName">
																				<template v-if="item.prodSource.type == 'category'">
																					分类商品示例{{CountIndex + 1 }}
																				</template>
																				<template v-if="item.prodSource.type == 'group'">
																					分组商品示例{{CountIndex + 1 }}
																				</template>
																			</span>
																	<span class="goods-spec-name" v-if="item.displayContent.skuName">示例SKU</span>
																	<div class="pro-price" v-if="item.displayContent.price"><span class="price">￥<em class="now">999</em>.00</span></div>
																	<div class="goods-cat-image" v-if="item.displayContent.cartStyle">
																		<img src="${contextPath}/resources/common/images/appDecorate/car.png" alt=""  >
																	</div>
																</div>
															</li>
														</template>

													</ul>
												</div>
												<!-- 小图两列 end -->

												<!-- 详细列表 -->
												<div class="goods-list-3" v-if="item.listStyle == 'style2'" :class="item.prodStyle == 'style1' ? 'radius' : ''" :style="{'background': item.background}">
													<ul class="goods-list-3-ul">

														<template v-if="item.prodSource.type == 'diy'">
															<li class="goods-li" :class="item.prodStyle == 'style1' ? 'radius' : ''" v-for="(prodItem,CountIndex) in item.prodSource.prods" :key="CountIndex">
																<div style="position: relative;float: left;width:350px;height: 100px;">
																	<div class="item-img">
																		<img :src="prodItem.pic?  imageServer + prodItem.pic : currentPath + '/resources/common/images/appDecorate/moren.png'" alt=""  >
																	</div>
																	<div class="goods-horn" v-if="item.displayContent.badgeStyle">
																		<template v-if="item.displayContent.badge == 'new'">
																			<img src="${contextPath}/resources/common/images/appDecorate/horn1.png" alt=""  >
																		</template>
																		<template v-if="item.displayContent.badge == 'hot'">
																			<img src="${contextPath}/resources/common/images/appDecorate/horn2.png" alt=""  >
																		</template>
																		<template v-if="item.displayContent.badge == 'custom'">
																			<img :src="item.displayContent.url? imageServer +  item.displayContent.url : currentPath + '/resources/common/images/appDecorate/moren.png'" alt=""  >
																		</template>
																	</div>
																	<div class="item-content">
																		<span class="goods-name">{{prodItem.spuName}}</span>
																		<span class="goods-spec-name" v-if="item.displayContent.skuName">{{prodItem.skuName}}</span>
																		<div class="pro-price" v-if="item.displayContent.price"><span class="price">￥<em class="now">{{Money(prodItem.price)[0]}}</em>.{{Money(prodItem.price)[1]}}</span></div>
																		<div class="goods-cat-image" v-if="item.displayContent.cartStyle">
																			<img src="${contextPath}/resources/common/images/appDecorate/car.png" alt=""  >
																		</div>
																	</div>
																</div>
															</li>
														</template>

														<template v-else>
															<li class="goods-li" :class="item.prodStyle == 'style1' ? 'radius' : ''" v-for="(prodItem,CountIndex) in item.prodCount" :key="CountIndex">
																<div style="position: relative;float: left;width:350px;height: 100px;">
																	<div class="item-img">
																		<img src="${contextPath}/resources/common/images/appDecorate/moren.png" alt=""  >
																	</div>
																	<div class="goods-horn" v-if="item.displayContent.badgeStyle">
																		<template v-if="item.displayContent.badge == 'new'">
																			<img src="${contextPath}/resources/common/images/appDecorate/horn1.png" alt=""  >
																		</template>
																		<template v-if="item.displayContent.badge == 'hot'">
																			<img src="${contextPath}/resources/common/images/appDecorate/horn2.png" alt=""  >
																		</template>
																		<template v-if="item.displayContent.badge == 'custom'">
																			<img :src="item.displayContent.url? imageServer +  item.displayContent.url : currentPath + '/resources/common/images/appDecorate/moren.png'" alt=""  >
																		</template>
																	</div>
																	<div class="item-content">
																				<span class="goods-name" v-if="item.displayContent.spuName">
																					<template v-if="item.prodSource.type == 'category'">
																						分类商品示例{{CountIndex + 1 }}
																					</template>
																					<template v-if="item.prodSource.type == 'group'">
																						分组商品示例{{CountIndex + 1 }}
																					</template>
																				</span>
																		<span class="goods-spec-name" v-if="item.displayContent.skuName">示例SKU</span>
																		<div class="pro-price" v-if="item.displayContent.price"><span class="price">￥<em class="now">10</em>.00</span></div>
																		<div class="goods-cat-image" v-if="item.displayContent.cartStyle">
																			<img src="${contextPath}/resources/common/images/appDecorate/car.png" alt=""  >
																		</div>
																	</div>
																</div>
															</li>

														</template>

													</ul>
												</div>
												<!-- 详细列表 end -->

											</div>
										</div>
									</div>
									<!-- 商品列表 end -->

									<!-- 魔方 -->
									<div class="widget-list-itme" v-if="item.type == 'cube'">
										<div class="itme-content" @mouseenter="enter(floorIndex,$event)" :class=" floorIndex == action ? 'on' : '' " @click="clickSelect(item,floorIndex)">
											<!-- 一行两个 -->
											<template v-if="item.layout == 0">
												<div class="cap-cube-wrap">
													<div class="cap-cube-list">
														<template v-if="item.photos">
															<div class="cap-cube-item h160 flex1" v-for="(photosItem,index) in item.photos">
																<img :src="photosItem.path ? imageServer + photosItem.path : ''" alt="" v-if="photosItem">
															</div>
														</template>

													</div>
												</div>
											</template>
											<!-- 一行两个 end -->
											<!-- 一行三个 -->
											<template v-if="item.layout == 1">
												<div class="cap-cube-wrap">
													<div class="cap-cube-list">
														<div class="cap-cube-item h160 flex1" v-for="(photosItem,index) in item.photos">
															<img :src="photosItem.path ? imageServer + photosItem.path : ''" alt="" v-if="photosItem">
														</div>
													</div>
												</div>
											</template>
											<!-- 一行三个 end-->
											<!-- 一行四个 -->
											<template v-if="item.layout == 2">
												<div class="cap-cube-wrap">
													<div class="cap-cube-list">
														<div class="cap-cube-item h160 flex1" v-for="(photosItem,index) in item.photos">
															<img :src="photosItem.path ? imageServer + photosItem.path : ''" alt=""  v-if="photosItem">
														</div>
													</div>
												</div>
											</template>
											<!-- 一行三个 -->
											<!-- 二左二右 -->
											<template v-if="item.layout == 3">
												<div class="cap-cube-wrap">
													<div class="cap-cube-list flexWrap">
														<div class="cap-cube-item  flex flexWrap">
															<div class="cap-cube-item-child h160" v-for="(photosItem,index) in item.photos">
																<img :src="photosItem.path ? imageServer + photosItem.path : ''" alt="" v-if="photosItem">
															</div>
														</div>
													</div>
												</div>
											</template>
											<!-- 二左二右 end-->
											<!-- 一左两右 -->
											<template v-if="item.layout == 4">
												<div class="cap-cube-wrap">
													<div class="cap-cube-list">
														<div class="item-l">
															<img :src="photosItem.path ? imageServer + photosItem.path : ''" alt=""   v-for="(photosItem,index) in item.photos.slice(0,1)" v-if="photosItem">
														</div>
														<div class="item-r">
															<img :src="photosItem.path ? imageServer + photosItem.path : ''" alt=""   v-for="(photosItem,index) in item.photos.slice(1,3)" v-if="photosItem">

														</div>
													</div>
												</div>
											</template>
											<!-- 一左两右 end -->
											<!-- 一上两下 -->
											<template v-if="item.layout == 5">
												<div class="cap-cube-wrap">
													<div class="cap-cube-list flexWrap">
														<div class="item-t">
															<img :src="photosItem.path ? imageServer + photosItem.path : ''" alt=""   v-for="(photosItem,index) in item.photos.slice(0,1)" v-if="photosItem">
														</div>
														<div class="item-b">
															<img :src="photosItem.path ? imageServer + photosItem.path : ''" alt=""   v-for="(photosItem,index) in item.photos.slice(1,3)" v-if="photosItem">
														</div>
													</div>
												</div>
											</template>
											<!-- 一上两下 end -->
										</div>
									</div>
									<!-- 魔方 end -->

									<!-- 图文导航 -->
									<div class="widget-list-itme" v-if="item.type == 'navigation'">
										<div class="itme-content" @mouseenter="enter(floorIndex,$event)" :class=" floorIndex == action ? 'on' : '' " @click="clickSelect(item,floorIndex)">
											<div class="nav" :style="{'background': item.background}">
												<div class="nav-view">
													<template v-if="item.layout != 0">



														<div class="swiper-container">
															<div class="swiper-wrapper">
																<div class="swiper-slide" v-for="(pageItem, pageIndex) in pages(item.navigations , (item.column * item.layout))" :key="pageIndex">
																	<div class="slick-list">
																		<div class="slick-item" v-for="(navItem, index) in pageItem" :key="index" :class="[item.column == '3' ? 'w3' : '', item.column == '4' ? 'w25' : '' , item.column == '5' ? 'w20' : '']">
																			<img :src="navItem.icon ? imageServer + navItem.icon : '${contextPath}/resources/common/images/appDecorate/moren.png' " alt=""  >
																			<span class="text" :style="{'color': item.fontColor}">{{navItem.title}}</span>
																		</div>
																	</div>
																</div>
															</div>
															<div class="pagination">
																<span class="on"></span>
																<span></span>
																<span></span>
																<!-- 指示器 end -->
															</div>
														</div>

													</template>
													<!-- 一直往下走 -->
													<template v-if="item.layout == 0">
														<div class="slick-list">
															<div class="slick-item" v-for="(navItem, index) in item.navigations" :key="index" :class="[item.column == '3' ? 'w3' : '', item.column == '4' ? 'w25' : '' , item.column == '5' ? 'w20' : '']">
																<img :src="navItem.icon ? imageServer + navItem.icon : '${contextPath}/resources/common/images/appDecorate/moren.png'" alt=""  >
																<span class="text" :style="{'color': item.fontColor}">{{navItem.title}}</span>
															</div>
														</div>

													</template>
													<!-- 一直往下走 end-->
												</div>
											</div>
										</div>
									</div>
									<!-- 图片导航 end -->

									<!-- 标题 -->
									<div class="widget-list-itme" v-if="item.type == 'title'">
										<div class="itme-content" @mouseenter="enter(floorIndex,$event)" :class=" floorIndex == action ? 'on' : '' " @click="clickSelect(item,floorIndex)">
											<template v-if="item.layout == 'simple'">
												<div class="floor-tit clear" :class="{'style1': item.style == 'style1' , style2 : item.style == 'style2' , style3 : item.style == 'style3'}">
													<div class="tit-div" :style="{'color': item.titleColor}" :class="{'display': item.photoStyle == 'display' , 'none': item.photoStyle == 'hide' , 'lump': item.photoStyle == 'lump'}">
														<img :src="item.photo ? imageServer + item.photo : '${contextPath}/resources/common/images/appDecorate/moren.png'" alt=""   class="title-img" v-if="item.photoStyle == 'display'">
														<span class="tit-span">{{item.title}}</span>
													</div>
												</div>
											</template>
											<template v-if="item.layout == 'plentiful'">
												<div class="floor-tit clear left" :class="{'style1': item.style == 'style1'}">
													<div class="tit-div" :style="{'color': item.titleColor}" :class="{'display': item.photoStyle == 'display' , 'none': item.photoStyle == 'hide' , 'lump': item.photoStyle == 'lump'}">
														<img :src="item.photo ? imageServer +  item.photo : '${contextPath}/resources/common/images/appDecorate/moren.png'" alt=""   class="title-img" v-if="item.photoStyle == 'display'">
														<span class="tit-span">{{item.title}}</span>
													</div>
													<div class="subTitle clear">
														<span class="title" :style="{'color': item.subheadColor}">{{item.subhead}}</span>
													</div>
													<p class="textLink" :style="{'color': item.subheadColor}"><span>{{item.moreTitle}}</span></p>
												</div>
											</template>
										</div>
									</div>
									<!-- 标题 end -->

									<!-- 辅助线 -->
									<div class="widget-list-itme" v-if="item.type == 'subline'">
										<div class="itme-content" @mouseenter="enter(floorIndex,$event)" :class=" floorIndex == action ? 'on' : '' " @click="clickSelect(item,floorIndex)">
											<div :style="{'height':item.height + 'px', 'background': item.background}" class="blank-box">
												<div class="resultLine" :style="{'border-color': item.lineColor,'width': item.lineStyle == '0' ? '100%' :  item.leftRightPadding + '%' }" :class="{'solid': item.style == 'solid','dashed': item.style == 'dashed','dotted': item.style == 'dotted', 'none': item.style == 'none'}">
												</div>
											</div>
										</div>

									</div>
									<!-- 辅助线 end -->

									<!-- 辅助高 -->
									<div class="widget-list-itme" v-if="item.type == 'blank'">
										<div class="itme-content" @mouseenter="enter(floorIndex,$event)" :class=" floorIndex == action ? 'on' : '' " @click="clickSelect(item,floorIndex)">
											<div :style="{'height': item.height+'px', 'background': item.background}"></div>
										</div>
									</div>
									<!-- 辅助高 end -->

								</div>

							</div>

							<div class="selectBox" style="margin-bottom: 120px;" :style="{'top':rightPost}" v-if="noFloorData">
								<template v-if="showSelectBox">
									<div class="select-content">
										<div class="select-list">
											<div class="item-content">
												<p class="selectBox-title">基础组件</p>
											</div>
											<div class="item-content">
												<ul class="selectBox-list">
													<li class="selectBox-item" @click="addMould('0')">轮播图</li>
													<li class="selectBox-item" @click="addMould('1')">图文导航</li>
													<li class="selectBox-item" @click="addMould('2')">标题</li>
													<li class="selectBox-item" @click="addMould('3')">魔方</li>
													<li class="selectBox-item" @click="addMould('4')">商品列表</li>
													<li class="selectBox-item" @click="addMould('5')">富文本</li>
													<%--<li class="selectBox-item">万能热区</li>--%>
												</ul>
											</div>
										</div>
										<div class="select-list">
											<div class="item-content">
												<p class="selectBox-title">其他</p>
											</div>
											<div class="item-content">
												<ul class="selectBox-list">
													<li class="selectBox-item" @click="addMould('7')">辅助空白</li>
													<li class="selectBox-item" @click="addMould('8')">辅助线</li>
												</ul>
											</div>
										</div>
									</div>
								</template>
								<template v-else>
									<div class="select-content">

										<!-- 轮播编辑 -->
										<div class="select-list" v-if="templateJson.type == 'slider'">
											<div class="item-content">
												<p class="selectBox-title on">{{floorTitle}}</p>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>尺寸样式：</p>
													<div class="edit-radio">

														<el-radio-group v-model="templateJson.sizeType" @change="changeImgH">
															<el-radio label="default">默认尺寸</el-radio>
															<el-radio label="big">大图 </el-radio>
															<el-radio label="small">小图</el-radio>
															<el-radio label="custom">自定义</el-radio>
														</el-radio-group>

													</div>
													<div style="padding: 0 10px;background: #f9f9f9;">
														<el-slider :min="118" :max="1000" v-model="templateJson.height" v-if="templateJson.sizeType == 'custom'"></el-slider>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>标记样式：</p>
													<div class="edit-radio">
														<el-radio v-model="templateJson.markStyle" label="none">无</el-radio>
														<el-radio v-model="templateJson.markStyle" label="point">圆点</el-radio>
														<el-radio v-model="templateJson.markStyle" label="block">方块</el-radio>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>显示样式：</p>
													<div class="edit-radio">
														<el-radio v-model="templateJson.displayStyle" label="default">默认</el-radio>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>切换动画：</p>
													<div class="edit-radio">
														<el-radio v-model="templateJson.switchAnimation" label="default">默认</el-radio>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>添加轮播图：<em style="color:red">建议尺寸为750*{{(templateJson.height * 2)}}</em></p>
												</div>
												<template v-if="templateJson.type == 'slider'">
													<template v-if="templateJson.photos && templateJson.photos.length">
														<div class="photosList">
															<div class="photos-item clear" v-for="(item,Index) in templateJson.photos" :key="Index">
																<div class="component">
																	<div class="has-choosed-image">
																		<img :src="item.path ? imageServer + item.path : '${contextPath}/resources/common/images/appDecorate/moren.png'" alt=""  >
																		<span class="modify-image" @click="showImgPopup(Index)">更换图片</span>
																	</div>
																</div>
																<div class="form-content">
																	<div>
																		<span style="vertical-align: middle;">跳转路径:</span>
																		<img src="${contextPath}/resources/common/images/appDecorate/nav-link-blue.png" alt="" @click="showPopup(Index)">
																	</div>
																	<div style="margin-top: 10px;">
																		<span style="vertical-align: top;">链接内容:</span>
																		<span style="vertical-align: top;display: inline-block;word-wrap: break-word;width: 192px;">
																					<em style="color: rgb(16, 142, 233);" v-if="item.action.type == 'prodDetail'">商品-</em>
																					<em style="color: rgb(16, 142, 233);" v-if="item.action.type == 'prodGroup'">商品分组-</em>
																					<em style="color: rgb(16, 142, 233);" v-if="item.action.type == 'prodList'">分类-</em>
																					<em style="color: rgb(16, 142, 233);" v-if="item.action.type == 'marketing'">营销-</em>
																					<em style="color: rgb(16, 142, 233);" v-if="item.action.type == 'method'">常用功能-</em>
																					<em style="color: rgb(16, 142, 233);" v-if="item.action.type == 'page'">页面-</em>
																					<em style="color: rgb(16, 142, 233);" v-if="item.action.type == 'url'">自定义-</em>
																					{{item.action.name}}
																				</span>
																	</div>
																</div>
																<i class="el-icon-circle-close" @click="deleteData(templateJson.photos , Index)"></i>
															</div>
														</div>

													</template>
												</template>

												<div class="nav-but" @click="addImages">
													<span class="but-a">+ 添加</span>
												</div>
											</div>
										</div>
										<!-- 轮播编辑 end-->

										<!-- 商品分类 -->
										<div class="select-list" v-if="templateJson.type == 'prodList'">
											<div class="item-content">
												<p class="selectBox-title on">{{floorTitle}}</p>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>列表样式：</p>
													<div class="edit-radio">

														<el-radio-group v-model="templateJson.listStyle">
															<el-radio label="style1">小图两列</el-radio>
															<el-radio label="style2">详细列表</el-radio>
														</el-radio-group>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>商品样式：</p>
													<div class="edit-radio">
														<el-radio-group v-model="templateJson.prodStyle">
															<el-radio label="style1">卡片样式1</el-radio>
															<el-radio label="style2">卡片样式2</el-radio>
														</el-radio-group>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>显示内容：</p>
													<div class="edit-radio wrap" style="height: auto;">
														<el-checkbox v-model="templateJson.displayContent.spuName" :disabled="templateJson.listStyle == 'style2' ? true : false">显示SPU商品名称</el-checkbox>
<%--														<el-checkbox v-model="templateJson.displayContent.skuName">显示SKU规格值</el-checkbox>--%>
														<el-checkbox v-model="templateJson.displayContent.price">显示价格</el-checkbox>
														<div class="carStyle">
															<el-checkbox v-model="templateJson.displayContent.cartStyle" >显示购物车</el-checkbox>
														</div>
														<div class="cart" style="width: 100%;margin-left: 20px;padding: 5px 0 10px;">
															<el-radio-group v-model="templateJson.displayContent.cart">
																<el-radio label="style1">默认</el-radio>
															</el-radio-group>
														</div>
														<div class="carStyle">
															<el-checkbox v-model="templateJson.displayContent.badgeStyle" >显示角标</el-checkbox>
														</div>
														<div class="cart" style="width: 100%;margin-left: 20px;padding: 5px 0 10px;">
															<el-radio-group v-model="templateJson.displayContent.badge">
																<el-radio label="new">新品</el-radio>
																<el-radio label="hot">热卖</el-radio>
																<el-radio label="custom">自定义</el-radio>
															</el-radio-group>
															<div class="badge-custom" v-if="templateJson.displayContent.badge =='custom'">
																<div class="">
																			<span class="addHorn"  v-if="templateJson.displayContent.url" @click="showImgPopup()">
																				<img :src="imageServer+templateJson.displayContent.url" alt="" style="display: block;width:100%;height:100%;" />
																			</span>
																	<span class="addHorn" @click="showImgPopup()" v-else>+</span>
																	<span class="tip">建议尺寸：60*60px，图片格式jpg、png格式</span>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>商品数据来源：</p>
													<p style="color: red;">*手动选择的商品不会实时更新,建议商品来源选择分组的方式</p>
													<div class="edit-radio" style="padding: 10px;height: auto;">
														<el-radio-group v-model="templateJson.prodSource.type">
															<el-radio label="category">关联分类</el-radio>
															<el-radio label="group">商品分组</el-radio>
															<el-radio label="diy">手动选择</el-radio>
														</el-radio-group>
													</div>
													<div style="padding: 11px 0 5px 11px;background: #f9f9f9;" v-if="templateJson.prodSource.type == 'category'">
																<span style="color: #108ee9;" @click="showPopup(null,{tab:{category: true}})">
																	<template v-if="templateJson.action.category">
																			{{JSON.stringify(templateJson.action.category) != "{}" ? templateJson.action.category.categoryName :'请选择分类'}}
																	</template>
															   </span>自动展示该分类下的商品
													</div>
													<div style="padding: 11px 0 5px 11px;background: #f9f9f9;" v-if="templateJson.prodSource.type == 'group'">
																<span style="color: #108ee9;" @click="showPopup(null,{tab:{prodGroup: true}})">
																	<template v-if="templateJson.action.category">
																        {{JSON.stringify(templateJson.action.prodGroup) != "{}"  ? templateJson.action.prodGroup.name :'请选择商品分组'}}
																	</template>
																</span>自动展示该商品分组下的商品
													</div>
													<div class="diyList clear" style="padding: 11px 0 5px 11px;background: #f9f9f9;" v-if="templateJson.prodSource.type == 'diy'">
														<ul class="list-content clear">
															<li class="item" v-for="(item,index) in templateJson.prodSource.prods">
																<img :src="imageServer + item.pic" alt=""  >
																<div class="close" @click="deleteData(templateJson.prodSource.prods,index)">x</div>
															</li>
															<li class="item" @click="showPopup(null,{tab:{prod: true}})">+</li>
															<!-- <li></li> -->
														</ul>
													</div>
												</div>

											</div>
											<div class="item-content" v-if="templateJson.prodSource.type != 'diy'">
												<div class="edit-title">
													<p>商品数量：</p>
													<div class="edit-radio">
														<el-radio-group v-model="templateJson.prodCount">
															<el-radio :label="4">4个</el-radio>
															<el-radio :label="6">6个</el-radio>
															<el-radio :label="8">8个</el-radio>
															<!-- <el-radio :label="templateJson.prodCount != 4 || templateJson.prodCount != 8 || templateJson.prodCount != 6">8个</el-radio> -->
														</el-radio-group>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>背景颜色：</p>
													<div class="edit-radio" style="height:auto;">
														<el-color-picker v-model="templateJson.background"></el-color-picker>
													</div>
												</div>
											</div>
										</div>
										<!-- 商品分类 end -->

										<!-- 魔方 -->
										<div class="select-list" v-if="templateJson.type == 'cube'">
											<div class="item-content">
												<p class="selectBox-title on">{{floorTitle}}</p>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>模板样式：</p>
													<div class="edit-radio pad" style="padding: 10px;height: auto;">
														<el-radio-group v-model="templateJson.layout" @change="changecubeType">
															<el-radio :label="0">1行2个</el-radio>
															<el-radio :label="1">1行3个</el-radio>
															<el-radio :label="2">1行4个</el-radio>
															<el-radio :label="3">2左2右</el-radio>
															<el-radio :label="4">1左2右</el-radio>
															<el-radio :label="5">1上2下</el-radio>
														</el-radio-group>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>布局：</p>
													<div class="edit-radio pad" style="padding: 10px;height: auto;">
														<div class="component-cube" v-if="templateJson.layout == 0">
															<div class="cap-cube-list" style="width: 320px;">
																<div class="item flex1" v-for="(item, index) in templateJson.photos" :key="index" @click="switchCube(index)" :class="{'on': cubeIndex == index}">
																	<template  v-if="item">
																		<img :src="item.path ? imageServer+item.path : ''" alt=""   v-if="item" class='cube-img'>
																	</template>
																	<div class="cube-selected-text">375*375</div>
																</div>
															</div>
														</div>
														<div class="component-cube" v-if="templateJson.layout == 1">
															<div class="cap-cube-list" style="width: 320px;">
																<div class="item  flex1" v-for="(item, index) in templateJson.photos" :key="index" @click="switchCube(index)" :class="{'on': cubeIndex == index}">
																	<template  v-if="item">
																		<img :src="item.path ? imageServer+item.path : '' " alt=""   v-if="item" class='cube-img' >
																	</template>
																	<div class="cube-selected-text">250*320</div>
																</div>

															</div>
														</div>
														<div class="component-cube" v-if="templateJson.layout == 2">
															<div class="cap-cube-list" style="width: 320px;">
																<div class="item  flex1" v-for="(item, index) in templateJson.photos" :key="index" @click="switchCube(index)" :class="{'on': cubeIndex == index}">
																	<template  v-if="item">
																		<img :src="item.path ? imageServer+item.path : '' " alt=""   v-if="item" class='cube-img' >
																	</template>
																	<div class="cube-selected-text">188*320</div>
																</div>

															</div>
														</div>
														<div class="component-cube" v-if="templateJson.layout == 3">
															<div class="cap-cube-list flexWrap" style="width: 320px;">
																<div class="item  flex1" v-for="(item, index) in templateJson.photos" :key="index" @click="switchCube(index)" :class="{'on': cubeIndex == index}">
																	<template  v-if="item">
																		<img :src="item.path ? imageServer+item.path : '' " alt=""   v-if="item" class='cube-img' >
																	</template>
																	<div class="cube-selected-text w187">375*375</div>
																</div>
															</div>
														</div>
														<div class="component-cube" v-if="templateJson.layout == 4">
															<div class="cap-cube-list flexWrap" style="width: 320px;">
																<div class="goods-l">
																	<div class="item  flex1" style="height:320px;" v-for="(item,index) in  templateJson.photos.slice(0,1)" @click="switchCube(index)" :class="{'on': cubeIndex == index}">
																		<template  v-if="item">
																			<img :src="item.path ? imageServer+item.path : '' " alt=""   v-if="item" class='cube-img' >
																		</template>
																		<div class="cube-selected-text w187">375*750</div>
																	</div>
																</div>
																<div class="goods-r">
																	<div class="item flex1" v-for="(item,index) in  templateJson.photos.slice(1,3)" @click="switchCube(index + 1)" :class="{'on': cubeIndex == index + 1}">
																		<template  v-if="item">
																			<img :src="item.path ? imageServer+item.path : '' " alt=""   v-if="item" class='cube-img' >
																		</template>
																		<div class="cube-selected-text w187">375*375</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="component-cube" v-if="templateJson.layout == 5">
															<div class="cap-cube-list flexWrap" style="width: 320px;">
																<div class="goods-t" style="width: 320px;">
																	<div class="item  flex1" v-for="(item,index) in  templateJson.photos.slice(0,1)" @click="switchCube(index)" :class="{'on': cubeIndex == index}">
																		<template  v-if="item">
																			<img :src="item.path ? imageServer+item.path : '' " alt=""   v-if="item" class='cube-img' >
																		</template>
																		<div class="cube-selected-text">750*375</div>
																	</div>
																</div>
																<div class="goods-b" style="width: 320px;">
																	<div class="item flex1" style="width: 160px;display: inline-block;" v-for="(item,index) in  templateJson.photos.slice(1,3)"  @click="switchCube(index + 1)" :class="{'on': cubeIndex == index + 1}">
																		<template  v-if="item">
																			<img :src="item.path ? imageServer+item.path : '' " alt=""   v-if="item" class='cube-img'>
																		</template>
																		<div class="cube-selected-text">375*375</div>
																	</div>

																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="photosList">
													<div class="photos-item clear"  >
														<div class="component">
															<div class="has-choosed-image" v-if="templateJson.photos[cubeIndex]">
																<img :src="templateJson.photos[cubeIndex].path ? imageServer + templateJson.photos[cubeIndex].path : '${contextPath}/resources/common/images/appDecorate/moren.png'" alt=""  >
																<span class="modify-image" @click="showImgPopup(cubeIndex)">更换图片</span>
															</div>
														</div>
														<div class="form-content">
															<div>
																<span style="vertical-align: middle;">跳转路径:</span>
																<img src="${contextPath}/resources/common/images/appDecorate/nav-link-blue.png" alt="" @click="showPopup(cubeIndex)">
															</div>
															<div style="margin-top: 10px;">
																<span style="vertical-align: middle;">链接内容:</span>
																<span style="vertical-align: top;display: inline-block;word-wrap: break-word;width: 192px;" v-if="templateJson.photos[cubeIndex]">
																			<em style="color: rgb(16, 142, 233);" v-if="templateJson.photos[cubeIndex].action.type == 'prodDetail'">商品-</em>
																			<em style="color: rgb(16, 142, 233);" v-if="templateJson.photos[cubeIndex].action.type == 'prodGroup'">商品分组-</em>
																			<em style="color: rgb(16, 142, 233);" v-if="templateJson.photos[cubeIndex].action.type == 'category'">分类-</em>
																			<em style="color: rgb(16, 142, 233);" v-if="templateJson.photos[cubeIndex].action.type == 'marketing'">营销-</em>
																			<em style="color: rgb(16, 142, 233);" v-if="templateJson.photos[cubeIndex].action.type == 'method'">常用功能-</em>
																			<em style="color: rgb(16, 142, 233);" v-if="templateJson.photos[cubeIndex].action.type == 'page'">页面-</em>
																			<em style="color: rgb(16, 142, 233);" v-if="templateJson.photos[cubeIndex].action.type == 'url'">自定义-</em>
																			{{templateJson.photos[cubeIndex].action.name}}
																		</span>

															</div>
														</div>
														<!-- <i class="el-icon-circle-close" @click="deleteData(templateJson.photos , cubeIndex)"></i> -->
													</div>
												</div>
											</div>
											<!-- <div class="item-content">
                                                <div class="edit-title">
                                                    <p>图片间隙：</p>
                                                    <el-slider v-model="templateJson.photoMargin" :min="0" :max="30"></el-slider>
                                                </div>
                                            </div> -->
										</div>
										<!-- 魔方 end -->

										<!-- 导航 -->
										<div class="select-list" v-if="templateJson.type == 'navigation'">
											<div class="item-content">
												<p class="selectBox-title on">{{floorTitle}}</p>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>模板样式：</p>
													<div class="edit-radio pad" style="padding: 10px;height: auto;">
														<el-radio-group v-model="templateJson.column">
															<el-radio :label="3">一行三列</el-radio>
															<el-radio :label="4">一行四列</el-radio>
															<el-radio :label="5">一行五列</el-radio>
														</el-radio-group>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>排列方式：</p>
													<div class="edit-radio pad" style="padding: 10px;height: auto;">
														<el-radio-group v-model="templateJson.layout">
															<el-radio :label="0">多次换行</el-radio>
															<el-radio :label="1">一行换页</el-radio>
															<el-radio :label="2">两行换页</el-radio>
														</el-radio-group>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-radio pad" style="padding: 10px;height: auto;">
													<div class='nav-set'>
														<p style='color:red;'>*按住可拖拽导航位置</p>

														<div v-for="(item,navigaIndex) in templateJson.navigations" v-dragging="{ item: item, list: templateJson.navigations, group: 'item' }" style="position: relative;">
															<div class="menu-item">
																<i class="meun-icon"><img :src="item.icon ? imageServer +  item.icon : '${contextPath}/resources/common/images/appDecorate/moren.png'" alt=""></i>
																<input type="text" class="menu-name" v-model="item.title" />
																<i class='el-icon-picture-outline' @click="showImgPopup(navigaIndex)"></i>
																<i class="el-icon-share" style="color:#222;" @click="showClue(navigaIndex,templateJson.navigations)"  v-if="item.action.type"></i>
																<i class="el-icon-share"  v-else @click="showPopup(navigaIndex)"></i>
																<i class="el-icon-delete"  @click="deleteData(templateJson.navigations,navigaIndex)"></i>
															</div>
															<template v-if="item.action.type">
																<div class="clue" :class="item.flag ? 'action' : '' ">
																	<p class="desc">链接内容</p>
																	<p class="desc-cont" style="flex: 1;width: auto;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;">
																			<span style="    white-space: nowrap; overflow: hidden; text-overflow: ellipsis;width: 96%;display: inline-block;vertical-align: middle;">
																				<em style="color: rgb(16, 142, 233);" v-if="item.action.type == 'prodDetail'">商品-</em>
																				<em style="color: rgb(16, 142, 233);" v-if="item.action.type == 'prodGroup'">商品分组-</em>
																				<em style="color: rgb(16, 142, 233);" v-if="item.action.type == 'category'">分类-</em>
																				<em style="color: rgb(16, 142, 233);" v-if="item.action.type == 'marketing'">营销-</em>
																				<em style="color: rgb(16, 142, 233);" v-if="item.action.type == 'method'">常用功能-</em>
																				<em style="color: rgb(16, 142, 233);" v-if="item.action.type == 'page'">页面-</em>
																				<em style="color: rgb(16, 142, 233);" v-if="item.action.type == 'url'">自定义-</em>
																				{{item.action.name}}
																			</span>
																	</p>
																	<i class="el-icon-share" @click="showPopup(navigaIndex)"></i>
																</div>
															</template>

														</div>

														<div class="nav-but" @click="addNavigation(templateJson.navigations)"><span class="but-a">+ 添加</span></div>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>背景颜色：</p>
													<div class="edit-radio pad" style="padding: 10px;height: auto;">
														<el-color-picker v-model="templateJson.background"></el-color-picker>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>文字颜色：</p>
													<div class="edit-radio pad" style="padding: 10px;height: auto;">
														<el-color-picker v-model="templateJson.fontColor"></el-color-picker>
													</div>
												</div>
											</div>
										</div>
										<!-- 导航 end -->

										<!-- 标题 -->
										<div class="select-list" v-if="templateJson.type == 'title'">
											<div class="item-content">
												<p class="selectBox-title on">{{floorTitle}}</p>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>标题模式：</p>
													<div class="edit-radio pad" style="padding: 10px;height: auto;">
														<el-radio-group v-model="templateJson.layout" @change="titlePattern(templateJson)">
															<el-radio label="simple">简化</el-radio>
															<el-radio label="plentiful">丰富</el-radio>
														</el-radio-group>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>样式：</p>
													<div class="edit-radio pad" style="padding: 10px;height: auto;">
														<el-radio-group v-model="templateJson.style">
															<el-radio label="style1">样式1</el-radio>
															<template v-if="templateJson.layout == 'simple'">
																<el-radio label="style2">样式2</el-radio>
																<el-radio label="style3">样式3</el-radio>
															</template>
														</el-radio-group>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>图片风格：</p>
													<div class="edit-radio pad" style="padding: 10px;height: auto;">
														<el-radio-group v-model="templateJson.photoStyle">
															<el-radio label="display">显示</el-radio>
															<el-radio label="hide">隐藏</el-radio>
															<el-radio label="lump">色块</el-radio>
														</el-radio-group>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>主标题：</p>
													<div class="edit-radio pad" style="padding: 10px;height: auto;">
														<div  @click="showImgPopup()" class='bgImg' style="float: left;" :style="{backgroundImage: 'url(' + (templateJson.photo ?  imageServer + templateJson.photo : '${contextPath}/resources/common/images/appDecorate/moren.png') + ')'}"></div>
														<el-input style="float: left;" placeholder="请输入主标题内容" v-model="templateJson.title" clearable>
														</el-input>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>副标题：</p>
													<div class="edit-radio pad" style="padding: 10px;height: auto;">
														<el-input placeholder="请输入副标题内容" v-model="templateJson.subhead" clearable>
														</el-input>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>链接：</p>
													<div class="edit-radio pad" style="padding: 10px;height: auto;">
														<el-input placeholder="请输入链接内容" v-model="templateJson.moreTitle" clearable>
														</el-input>
														<i class="el-icon-share" style="cursor: pointer;font-size: 30px;margin-left: 10px;" @click="showPopup()"></i>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title clear">
													<div class="block">
														<span class="demonstration">主标题颜色</span>
														<el-color-picker v-model="templateJson.titleColor"></el-color-picker>
													</div>
													<div class="block">
														<span class="demonstration">富标题颜色</span>
														<el-color-picker v-model="templateJson.subheadColor"></el-color-picker>
													</div>
												</div>
											</div>
										</div>

										<!-- 标题 end -->

										<!-- 辅助线 -->
										<div class="select-list" v-if="templateJson.type == 'subline'">
											<div class="item-content">
												<p class="selectBox-title on">{{floorTitle}}</p>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>线条样式：</p>
													<div class="edit-radio pad" style="padding: 10px;height: auto;">
														<el-radio-group v-model="templateJson.style">
															<el-radio label="solid">实线</el-radio>
															<el-radio label="dashed">虚线</el-radio>
															<el-radio label="dotted">点线</el-radio>
															<el-radio label="none">无</el-radio>
														</el-radio-group>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>线条样式：</p>
													<div class="edit-radio pad" style="padding: 10px;height: auto;">
														<el-radio-group v-model="templateJson.lineStyle">
															<el-radio :label="0">全部</el-radio>
															<el-radio :label="1">自定义</el-radio>
														</el-radio-group>
													</div>
													<el-slider :min="10" v-model="templateJson.leftRightPadding" v-if="templateJson.lineStyle == 1" style="padding: 0 20px;background: #f9f9f9;"></el-slider>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>线条颜色：</p>
													<div class="edit-radio pad" style="padding: 10px;height: auto;">
														<el-color-picker v-model="templateJson.lineColor"></el-color-picker>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>背景颜色：</p>
													<div class="edit-radio pad" style="padding: 10px;height: auto;">
														<el-color-picker v-model="templateJson.background"></el-color-picker>
													</div>
												</div>
											</div>
										</div>
										<!-- 辅助线 end -->
										<!-- 辅助空白 -->
										<div class="select-list" v-if="templateJson.type == 'blank'">
											<div class="item-content">
												<p class="selectBox-title on">{{floorTitle}}</p>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>背景颜色：</p>
													<div class="edit-radio pad" style="padding: 10px;height: auto;">
														<el-color-picker v-model="templateJson.background"></el-color-picker>
													</div>
												</div>
											</div>
											<div class="item-content">
												<div class="edit-title">
													<p>高度：</p>
													<el-slider v-model="templateJson.height" :min="10" style="padding: 0 20px;background: #f9f9f9;"></el-slider>
												</div>
											</div>
										</div>
										<!-- 辅助空白 end -->

										<!-- 富文本 -->
										<div class="select-list" v-if="templateJson.type == 'richText'">
											<div class="item-content">
												<p class="selectBox-title on">{{floorTitle}}</p>
											</div>
											<div class="item-content">
												<div class="edit-title edit-color">
													<p>背景颜色：<el-color-picker v-model="templateJson.background"></el-color-picker></p>
													<textarea name="content" id="content" cols="100" rows="8" style="width:700px;height:200px;overflow:hidden; resize:none;" v-html="templateJson.content" v-model="templateJson.content"></textarea>
												</div>
											</div>
										</div>
										<!-- 富文本 -->
									</div>
								</template>
							</div>

							<div class="bar-ctrl" :style="{'top':topPosi}" :class=" navIndex ? 'on' : '' ">
								<ul>
									<li @click="addFloor"><i class="el-icon-plus"></i></li>
									<li @click="copyOf"><i class="el-icon-copy-document"></i></li>
									<li @click="deleteFloor"><i class="el-icon-delete"></i></li>
									<template v-if="action != 0">
										<li @click="moveUp"><i class="el-icon-top"></i></li>
									</template>

									<template v-if="action != floorData.components">

										<template v-if="action != floorData.components.length - 1">
											<li @click="moveDown">
												<i class="el-icon-bottom"></i>
											</li>
										</template>
									</template>
								</ul>
							</div>
						</div>
						<div class="wrapper-footer">
							<div class="save">
								<div class='inner'>
									<el-button type="primary" @click="releasePage()">发布</el-button>
									<el-button @click="savePage()">保存</el-button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- 选择跳转链接弹层 -->
				<div class="dialog">

					<el-dialog title="" :visible.sync="dialogVisible" width="50%" :before-close="cancel">
						<div class="ant-tabs-nav-scroll">
							<div class="ant-tabs-nav-container">

								<div class="tab" :class="{'on':popTab.curTab == 'prod'}" @click="switchTab('prod')" v-if="!popTab.tab || popTab.tab.prod">商品</div>
								<div class="tab" :class="{'on':popTab.curTab == 'prodGroup'}" @click="switchTab('prodGroup')" v-if="!popTab.tab || popTab.tab.prodGroup">商品分组</div>
								<div class="tab" :class="{'on':popTab.curTab == 'category'}" @click="switchTab('category')" v-if="!popTab.tab || popTab.tab.category">分类</div>
								<div class="tab" :class="{'on':popTab.curTab == 'activity'}" @click="switchTab('activity')" v-if="!popTab.tab || popTab.tab.activity">营销</div>
								<div class="tab" :class="{'on':popTab.curTab == 'method'}" @click="switchTab('method')" v-if="!popTab.tab || popTab.tab.method"> 常用功能</div>
								<div class="tab" :class="{'on':popTab.curTab == 'page'}" @click="switchTab('page')" v-if="!popTab.tab || popTab.tab.page">页面</div>
								<div class="tab" :class="{'on':popTab.curTab == 'diy'}" @click="switchTab('diy')" v-if="!popTab.tab || popTab.tab.diy">自定义</div>
							</div>
						</div>

						<div class="content" :class="{'block':popTab.curTab == 'prod'}">
							<div class="content-ul">
								<div class="item-li clear">
									<div class="select">

										<el-input placeholder="请输入商品名称" suffix-icon="el-icon-search" v-model="searchInput" @blur="searchProd" @keyup.enter.native="searchProd">
										</el-input>
									</div>
									<div class="products_content">
										<div class="products_list">
											<ul v-if="prodlistArr.resultList && prodlistArr.resultList.length">
												<li class="good-list-item clear" :class="{'action': selectgoodsIndex == prodIndex}" v-for="(prodItem , prodIndex) in prodlistArr.resultList" :key="prodIndex" @click="selectGoods(prodIndex,prodItem)">
													<img :src="prodItem.pic ? imageServer + prodItem.pic : currentPath + '/resources/common/images/appDecorate/moren.png'">
													<span class="title">{{prodItem.	spuName}}</span>
													<!-- <%-- <span class="sku">规格：薄荷香型牙膏 65g</span> --%> -->
													<span class="price">价格：{{prodItem.price}}</span>
												</li>
											</ul>
											<p v-else style="margin-top: 135px;">没有找到符合条件的商品</p>
										</div>
									</div>
									<div class="block" style="width: 100%;">
										<el-pagination :current-page.sync="currentPage1" background @prev-click="goPage('prod',$event)" @current-change="goPage('prod',$event)" :page-size="prodlistArr.pageSize" layout="prev, pager, next, jumper" :total="prodlistArr.total" @next-click="goPage('prod',$event)">
										</el-pagination>
									</div>
								</div>
							</div>

						</div>
						<div class="content" :class="{'block':popTab.curTab == 'category'}" style="background: #fff;">
							<!-- <div class="link-search">
                                            <span class="link-search-title" >分类搜索：
                                                <el-input
                                                    placeholder="请输入商品分类关键词"
                                                    suffix-icon="el-icon-search"
                                                    v-model="classifyInput">
                                                </el-input>
                                            </span>
                                        </div> -->
							<div class="classification-content">
								<div class="currently-selected">
									<span>您当前选择的是：</span>
									<span>{{categoryName}}</span>
								</div>
								<div class="selected-box">
									<div class="ant-row clear">
										<div class="ant-col-8">
											<div class='category'>
												<ul>
													<template v-if="firstClass">
														<li class="clearfix" v-for="(item,index) in firstClass" :key="index" :class="item.check ? 'on' : ''"  @click="changClass(item,item.grade)">
															<span class="text">{{item.name}}</span>
															<i class='el-icon-arrow-right' v-if="item.childrenList"></i>
														</li>
													</template>
												</ul>
											</div>
										</div>
										<div class="ant-col-8">
											<div class='category'>
												<ul>
													<template v-if="secondClass">
														<li class="clearfix" v-for="(item,index) in secondClass" :key="index" :class="item.check ? 'on' : ''" @click="changClass(item,item.grade)">
															<span class="text">{{item.name}}</span>
															<i class='el-icon-arrow-right' v-if="item.childrenList"></i>
														</li>
													</template>
												</ul>
											</div>
										</div>
										<div class="ant-col-8">
											<div class='category'>
												<ul>
													<template v-if="threeClass">
														<li class="clearfix" v-for="(item,index) in threeClass" :key="index" :class="item.check ? 'on' : ''" @click="changClass(item,item.grade)">
															<span class="text">{{item.name}}</span>
															<i class='el-icon-arrow-right' v-if="item.childrenList"></i>
														</li>
													</template>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="content" :class="{'block':popTab.curTab == 'activity'}">
							<div class="page-marketing">
								<div class='page-user'>
									<ul>
										<li :class="{'on' : selectgoodsIndex == item.type}" @click="switchUsed(item.type)" v-for="(item, index) in marketingArr" :key="index">
											<img src="${contextPath}/resources/common/images/appDecorate/Home.png" alt=""  >
											<br />
											<span>{{item.name}}</span>
										</li>
									</ul>
								</div>
							</div>
						</div>

						<div class='content' :class="{'block':popTab.curTab == 'method'}">
							<div class='page-user'>
								<ul>
									<li :class="{'on' : selectgoodsIndex == 'home'}" @click="switchUsed('home')">
										<img src="${contextPath}/resources/common/images/appDecorate/Home.png" alt=""  >
										<br />
										<span>首页</span>
									</li>
									<li :class="{'on' : selectgoodsIndex == 'class'}" @click="switchUsed('class')">
										<img src="${contextPath}/resources/common/images/appDecorate/classify.png" alt=""  >
										<br />
										<span>分类</span>
									</li>
									<li :class="{'on' : selectgoodsIndex == 'car'}" @click="switchUsed('car')">
										<img src="${contextPath}/resources/common/images/appDecorate/shoppCart.png" alt=""  >
										<br />
										<span>购物车</span>
									</li>
									<li :class="{'on' : selectgoodsIndex == 'my'}" @click="switchUsed('my')">
										<img src="${contextPath}/resources/common/images/appDecorate/user.png" alt=""  >
										<br />
										<span>个人中心</span>
									</li>
									<li :class="{'on' : selectgoodsIndex == 'coupon'}" @click="switchUsed('coupon')">
										<img src="${contextPath}/resources/common/images/appDecorate/user.png" alt=""  >
										<br />
										<span>我的优惠券</span>
									</li>
									<li :class="{'on' : selectgoodsIndex == 'myMsg'}" @click="switchUsed('myMsg')">
										<img src="${contextPath}/resources/common/images/appDecorate/user.png" alt=""  >
										<br />
										<span>我的信息</span>
									</li>
									<li :class="{'on' : selectgoodsIndex == 'keep'}" @click="switchUsed('keep')">
										<img src="${contextPath}/resources/common/images/appDecorate/user.png" alt=""  >
										<br />
										<span>我的收藏</span>
									</li>
								</ul>
							</div>
						</div>
						<div class="content" :class="{'block':popTab.curTab == 'prodGroup'}" style="background: #fff;">
							<div class="dialog-list">
								<div class="dialog-action-bar">
									<div class="action-bar">
										<a href="${contextPath}/admin/prodGroup/query" target="_blank" rel="noopener noreferer" class="btn"><span>管理分组</span></a>
										<el-input suffix-icon="el-icon-search" v-model="searchGroupInput" clearable @blur="searchGroup" @keyup.enter.native="searchGroup">
										</el-input>
									</div>

								</div>
							</div>
							<div class="table-container" style="padding: 5px 20px;">


								<div class='table-header-wrapper'>
									<table cellspacing="0" cellpadding="0" border="0" class="table__header" style="width: 100%;">
										<thead class="has-gutter">
										<tr class="">
											<th colspan="1" rowspan="1" class="is-leaf"><div class="cell">请选择</div></th>
											<th colspan="1" rowspan="1" class="is-leaf"><div class="cell">分组名称</div></th>
											<th colspan="1" rowspan="1" class="is-leaf"><div class="cell">分组类型</div></th>
											<th colspan="1" rowspan="1" class="is-leaf"><div class="cell">分组描述</div></th>
											<th colspan="1" rowspan="1" class="is-leaf"><div class="cell">创建时间</div></th>
										</tr>
										</thead>
									</table>
								</div>

								<div class='table-body-wrapper'>
									<table cellspacing="0" cellpadding="0" border="0" class="table__header" style="width: 100%;">
										<tbody class="has-gutter">

										<tr class="el-table__row"  v-for="(prodGroupItem , prodGroupIndex) in prodGroupList.resultList" :key="prodGroupIndex">
											<td rowspan="1" colspan="1" class="el-table_70_column_295  ">
												<input  type="radio" name="type" :value="prodGroupItem" class="radio"  @click="getRadioVal(prodGroupItem,$event)" ref="radio">
											</td>
											<td rowspan="1" colspan="1" class="el-table_70_column_295  ">
												<div class="cell">{{prodGroupItem.name}}</div>
											</td>
											<td rowspan="1" colspan="1" class="el-table_70_column_296  ">
												<div class="cell">{{prodGroupItem.type == '0'? '系统定义' : '自定义'}}</div>
											</td>
											<td rowspan="1" colspan="1" class="el-table_70_column_297  ">
												<div class="cell">{{prodGroupItem.description}}</div>
											</td>
											<td rowspan="1" colspan="1" class="el-table_70_column_297  ">
												<div class="cell">{{prodGroupItem.recDate | dateformat('YYYY-MM-DD HH:mm') }}</div>
											</td>
										</tr>

										</tbody>
									</table>
								</div>

								<div class="block" style="width: 100%;">
									<el-pagination :current-page.sync="currentPage1" background @prev-click="goPage('prodGroup',$event)" @current-change="goPage('prodGroup',$event)" :page-size="prodGroupList.pageSize" layout="prev, pager, next, jumper" :total="prodGroupList.total" @next-click="goPage('prodGroup',$event)">
									</el-pagination>
								</div>







								<!-- <div class="table">
                                    <div class="thead">
                                        <div class="stickrow tr">
                                            <div class="cell " style="flex: 0 1 auto;">
                                                <div class="cell__child-container">分组名称</div>
                                            </div>
                                            <div class="cell ">
                                                <div class="cell__child-container">分组类型</div>
                                            </div>
                                            <div class="cell ">
                                                <div class="cell__child-container">分组描述</div>
                                            </div>
                                            <div class="cell ">
                                                <div class="cell__child-container">创建时间</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tbody">
                                        <div class="tr" v-for="(prodGroupItem , prodGroupIndex) in prodGroupList.resultList" :key="prodGroupIndex">
                                            <div class="cell" style="flex: 0 1 auto;">
                                                <div class="cell__child-container">{{prodGroupItem.name}}</div>
                                            </div>
                                            <div class="cell ">

                                                <div class="cell__child-container">{{prodGroupItem.type == '0'? '系统定义' : '自定义'}}</div>
                                            </div>
                                            <div class="cell ">
                                                <div class="cell__child-container">{{prodGroupItem.description}}</div>
                                            </div>
                                            <div class="cell ">
                                                <div class="cell__child-container">{{prodGroupItem.recDate}}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
							</div>
						</div>
						<div class="content" :class="{'block':popTab.curTab == 'page'}" style="background: #fff;">
							<div class='page'>
								<el-input v-model="pageInput" clearable suffix-icon="el-icon-search"  placeholder="请输入内容" @blur="searchPage" @keyup.enter.native="searchPage"></el-input>
							</div>
							<div class='page-table'>
								<div clsss='table-header' style="display: flex;height: 50px;line-height: 50px;background: #f9f9f9;font-weight: bold;align-items: center;">
									<span class="flex-1" ></span>
									<span class="flex-4">页面名称</span>
									<span class="flex-4">发布时间</span>
									<span class="flex-4">状态</span>
									<span class="flex-3">页面类型</span>
								</div>

								<ul class='table-main'>
									<template v-if="pageArr.resultList && pageArr.resultList.length">
										<li class='table-item' v-for="(pageItem,pageIndex) in pageArr.resultList" :key="pageIndex" >
											<span class="flex-1" ><input type="radio" name="webpage" class='table-radio' :value="pageItem"   @click="getRadioVal(pageItem,$event)"  ref="radio"/></span>
											<span class="flex-4" >{{pageItem.name}}</span>
											<span class="flex-4" >{{pageItem.recDate | dateformat('YYYY-MM-DD HH:mm:ss')}}</span>
											<template v-if="pageItem.status == -1">
												<span class="flex-4" >草稿</span>
											</template>
											<template v-else-if="pageItem.status == 0">
												<span class="flex-4" >已修改未发布</span>
											</template>
											<template v-else >
												<span class="flex-4" >已发布</span>
											</template>


											<span class="flex-3" >{{pageItem.category == 'POSTER' ? '海报页' : '首页'}}</span>
										</li>
									</template>

									<template v-else>
										<li  class='table-item' style="display: block;">暂无页面</li>
									</template>

								</ul>



							</div>
							<div class='page-pagintion' style="margin-top: 20px;">
								<el-pagination
										@current-change="goPage('page',$event)"
										:current-page.sync="currentPage1"
										style="text-align: center;"
										background
										layout="prev, pager, next, jumper"
										:page-size="pageArr.pageSize"
										:total="pageArr.total">
								</el-pagination>
							</div>

						</div>
						<div class="content" :class="{'block':popTab.curTab == 'diy'}">
							<div class="diyInpnt">
								<el-input v-model="diyInpnt" placeholder="输入外部链接请加上http://或https://, #代表正在开发中，自定义页面" style="width: 80%;padding-top:10px;"></el-input>
							</div>
						</div>
						<span slot="footer" class="dialog-footer">
									<el-button @click="cancel">取 消</el-button>
									<el-button type="primary" @click="confirm">确 定</el-button>
								</span>
					</el-dialog>
				</div>
				<!-- 选择跳转链接弹层 -->


				<!-- 图片弹窗 -->
				<div class="pictureLayer">
					<el-dialog
							title="选择文件"
							:visible.sync="dialogPicture"

							:before-close="cancel"
					>

						<div class="modal-body-main">
							<div class="left">
								<el-tree
										:data="treeData"
										node-key="parentId"
										ref="tree"
										:default-expanded-keys="[0]"
										:default-checked-keys="[0]"
										:props="defaultProps"
										current-node-key="parentId"
										accordion
										:highlight-current= "true"
										@node-click="handleNodeClick">
								</el-tree>
							</div>
							<div class="right">
								<el-select v-model="selectValue" placeholder="请选择" @change="changeSelect($event)">
									<el-option
											v-for="item in options"
											:key="item.label"
											:label="item.label"
											:value="item.value">
									</el-option>
								</el-select>
								<el-input
										placeholder="按文件名搜索"
										v-model="pictureParames.searchName"
										size="medium"
										style="width:30%;height: 40px;"
										@keyup.enter.native="searchPictureFile"
										clearable>
								</el-input>
								<el-button style="margin-left: 10px;" @click="searchPictureFile">搜索</el-button>
								<!-- <el-button type="primary" plain>点击上传</el-button> -->
								<div class="files">
									<el-upload
											ref="upload"
											:show-file-list="false"
											action=""
											multiple
											:limit="5"
											:on-exceed="handleExceed"
											:http-request="upload"
											:auto-upload="true">
										<el-button slot="trigger" type="primary" @click="clearFiles">上传</el-button>
									</el-upload>
								</div>

								<div class="tips">
									<div class='tip-title' style="margin: 8px 0;color:red">提示:</div>
									<p>1，鼠标点击就可以选择图片信息 2，本地上传图片大小不能超过500K</p>
								</div>
								<div class="pictures_con">
									<ul class="clearfix" v-if="imagesArr.resultList && imagesArr.resultList.length">
										<li class="item-pic" v-for="(item, index) in imagesArr.resultList" :key="index" @click="selectImg(item.filePath)">
											<img :src="imageServer + item.filePath" alt="" class="img-shape">
											<span class="text">{{item.fileName}}</span>
										</li>
									</ul>
									<p class='Nomsg' v-else>图库暂无图片信息</p>
								</div>
								<div class="pagination1">
									<el-pagination
											@current-change="goPage('pictureLayer',$event)"
											:current-page.sync="currentPage1"
											style="text-align: center;"
											background
											layout="prev, pager, next, jumper"
											:page-size="imagesArr.pageSize"
											:total="imagesArr.total">
									</el-pagination>
								</div>
							</div>
						</div>
					</el-dialog>
				</div>
				<!-- 图片弹窗 -->

			</div>
			<!-- vue 结构 end-->
		</div>
	</div>
</div>

<!-- 开发环境版本，包含了有帮助的命令行警告 -->
<%--<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>--%>
<!-- 生产环境版本，优化了尺寸和速度 -->
<!-- <script src="https://cdn.jsdelivr.net/npm/vue"></script> -->
<%--<script src="https://unpkg.com/element-ui@2.10.0/lib/index.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.0/js/swiper.min.js"></script>--%>


<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/vue/vue.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/element-ui/index.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/swiper/swiper.min.js'/>"></script>


<script>
	var contextPath = '${contextPath}';
	var photoPath = '<ls:photo item="" />';
	var decotateData = ${pageManage.data};
	var pageId = '${pageManage.id}';
	var isUse = '${pageManage.isUse}';
	var category = '${category}';
	var jsessionid = "${cookie.SESSION.value}";
</script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/vue-dragging.es5.js'/>"></script>
<script src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/moment.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/decoration.js'/>"></script>
</body>
</html>