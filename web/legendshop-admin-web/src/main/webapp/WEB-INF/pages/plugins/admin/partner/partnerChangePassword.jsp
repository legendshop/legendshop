<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>


<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
         
	<table class="${tableclass}" style="width: 100%">
	    <thead>
			<tr><th> <strong class="am-text-primary am-text-lg">商城管理</strong> /  供应商管理</th></tr>
	    </thead>
    </table>
        <form:form  action="${contextPath}/admin/partner/savePassword" method="post" id="form1">
            <input id="partnerId" name="partnerId" value="${partner.partnerId}" type="hidden">
            <div align="center">
            <table border="0" align="center" class="${tableclass}" id="col1">
                   
     <tr>
        <td>
          <div align="center">供应商登录名: <font color="ff0000">*</font></div>
       </td>
        <td>
           ${partner.partnerName}
        </td>
      </tr>
     <tr>
        <td>
          <div align="center">登录密码: <font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="password" name="password" id="password" class="${inputclass}" autocomplete="off"/>
        </td>
      </tr>
     <tr>
        <td>
          <div align="center">确认密码: <font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="password" name="passwordag" id="passwordag" class="${inputclass}"  autocomplete="off"/>
        </td>
      </tr>
                <tr>
                    <td colspan="2">
                        <div align="center">
                            <input type="submit" value="保存" class="${btnclass}" />
                            <input type="button" value="返回" class="${btnclass}"
                                onclick="window.location='<ls:url address="/admin/partner/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
<script type="text/javascript">
    $.validator.setDefaults({
    });

    $(document).ready(function() {
        jQuery("#form1").validate({
            rules: {
                password: {
                    required: true,
                },
                passwordag: {
                    required: true,
    		        equalTo:"#password"
                }
            },
            messages: {
                password: {
                    required: '<fmt:message key="password.required"/>',
                },
                passwordag: {
                    required: '<fmt:message key="password.required"/>',
                    equalTo: '<fmt:message key="password.equalTo"/>'
                }
            }
        });
         //斑马条纹
   	  		 $("#col1 tr:nth-child(even)").addClass("even");
         highlightTableRows("col1");  
    });
</script>


