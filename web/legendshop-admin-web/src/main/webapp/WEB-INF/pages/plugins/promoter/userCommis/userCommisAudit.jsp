<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
 <link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/amazeui.css"/>
 <link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/admin.css">
 <style>
 	.no-border > thead > tr > td:first-child,
 	.no-border > tbody > tr > td:first-child,
 	.no-border > tfoot > tr > td:first-child {
	    width: 130px;
	}
 </style>
<body>
			<input id="userId" name="userId" value="${userCommis.userId}"
				type="hidden">
			<div align="center">
				<table border="0" align="center" style="min-width:0;margin-top:15px;" class="${tableclass} content-table no-border" id="col1">

					<tr>
						<td>
							<div align="right">用户ID:</div>
						</td>
						<td>${userCommis.userName}</td>
					</tr>
					<tr>
						<td>
							<div align="right">用户昵称:</div>
						</td>
						<td>${userDetail.nickName}</td>
					</tr>
					<tr>
						<td>
							<div align="right">真实姓名:</div>
						</td>
						<td>${userDetail.realName}</td>
					</tr>
<%--					<tr>--%>
<%--						<td>--%>
<%--							<div align="right">用户邮箱:</div>--%>
<%--						</td>--%>
<%--						<td>${userDetail.userMail}</td>--%>
<%--					</tr>--%>
					<tr>
						<td>
							<div align="right">详细地址:</div>
						</td>
						<td>${userDetail.userAdds}</td>
					</tr>
					<tr>
						<td>
							<div align="right">手机号码:</div>
						</td>
						<td>${userDetail.userMobile}</td>
					</tr>
					<tr>
						<td>
							<div align="right">分销上级用户ID:</div>
						</td>
						<td>${userCommis.parentUserName}</td>
					</tr>
					<tr>
						<td>
							<div align="right">
								审核意见: <font color="ff0000">*</font>
							</div>
						</td>
						<td><textarea name="promoterAuditComm" id="promoterAuditComm"
								style="width: 375px; height: 100px;"></textarea></td>
					</tr>
					<tr>
						<td colspan="2" style="height:80px;">
							<div align="center">
								<input type="button" value="通过" class="criteria-btn"
									onclick="audit(1)" /> <input type="button" value="拒绝"
									class="criteria-btn" onclick="audit(-1)" /> <input
									type="button" value="返回" class="criteria-btn"
									onclick="closeDialog()" />
							</div>
						</td>
					</tr>
				</table>
			</div>
<script language="javascript">
	var contextPath = '${contextPath}';
	/**返回关闭**/
	function closeDialog() {
		window.parent.refreshMyProfile();
		var index = parent.layer.getFrameIndex('auditInfo'); //先得到当前iframe层的索引
		parent.layer.close(index); //再执行关闭
	}

	/**审核**/
	function audit(s) {
		var userId = $("#userId").val();
		var promoterAuditComm = $("#promoterAuditComm").val();
		if (promoterAuditComm.length == 0) {
			layer.msg("请填写审核意见", {icon:0});
			return false;
		}

		$.ajax({
			url : "${contextPath}/admin/userCommis/updateStatus",
			type : 'POST',
			dataType : 'json',
			data : {
				"id" : userId,
				"status" : s,
				"promoterAuditComm" : promoterAuditComm
			},
			success : function(resp) {
				if (resp == "fail") {
					layer.msg("审核失败", {icon:2});
				} else {
					layer.msg("审核成功", {icon:1, time:700}, function(){
						var index = parent.layer.getFrameIndex('auditInfo'); //先得到当前iframe层的索引
						parent.layer.close(index); //再执行关闭
						parent.window.location.reload();
					});
				}
			}
		});
	}
</script>
