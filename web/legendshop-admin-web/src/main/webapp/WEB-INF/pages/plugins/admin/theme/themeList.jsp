<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>营销管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/common/css/errorform.css" />
<link rel="stylesheet"
	href="<ls:templateResource item='/resources/common/css/specProperty.css'/>">
<style>
.order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
}
.am-table > thead > tr > th,
.am-table > tbody > tr > th,
.am-table > tfoot > tr > th,
.am-table > thead > tr > td,
.am-table > tbody > tr > td,
.am-table > tfoot > tr > td {
  padding: 8px 10px;
  line-height: 23px;
  border-bottom: 1px solid #dddddd;
  min-width: 80px;
  max-width: 200px;
}
</style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">平台营销&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">专题管理</span></th>
				</tr>
			</table>
			<div class="demo">
			<div class="" id="tabs">
				<ul class="am-nav am-nav-tabs">
					<li id="ruleAttr0" class=" am-active"><a
						href="javascript:void(0);" onclick="toPage(this,1);">已上线专题</a></li>
					<li id="ruleAttr1"><a href="javascript:void(0);"
						onclick="toPage(this,0);">待上线专题</a></li>
					<li id="ruleAttr3"><a href="javascript:void(0);"
						onclick="toPage(this,3);">已过期专题</a></li>
					<li id="ruleAttr2"><a h ref="javascript:void(0);"
						onclick="toPage(this,2);">已删除专题</a></li>
					<input type="hidden" id="operetion" name="operetion"
						value="${operation}" />
				</ul>
				<div class="criteria-div">
						<span class="item">
						<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
							专题名称：<input class="${inputclass}" type="text" id="title" name="title" maxlength="50" value="${title}" /> 
							<input class="${btnclass}" type="button" value="搜索" onclick="javascript:searchContent();" /> 
							<input class="${btnclass}" type="button" value="创建专题" onclick='window.location="<ls:url address='/admin/theme/load'/>"' />
						</span>
				</div>
				<div id="themeContent"  class="order-content-list">
					<div align="center">
						<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
						<display:table name="list" requestURI="/admin/theme/query"
							id="item" export="false" sort="external" class="${tableclass}"
							style="width:100%">
							<display:column title="图片" >
								<a href="${contextPath}/admin/theme/preview/${item.themeId}"
									target="_blank" title="${item.title}" style="margin: 10px;">
									<img width="65" height="65"
									src="<ls:photo item="${item.themePcImg}"/>"
									alt="${item.title}"> 
								</a>
							</display:column>
							<display:column title="专题名称">
								<%-- <a href="${contextPath}/admin/theme/preview/${item.themeId}"
									target="_blank" title="${item.title}" style="margin: 10px;">
									<img width="65" height="65" style="float: left;"
									src="<ls:photo item="${item.themePcImg}"/>"
									alt="${item.title}"> <span
									style="height: 4em; line-height: 2em; overflow: hidden;">${item.title}</span>
								</a> --%>
								<a href="${contextPath}/admin/theme/preview/${item.themeId}"
									target="_blank" title="${item.title}" style="margin: 10px;">
									<div class="order_img_name">${item.title}</div>
								</a>
							</display:column>
							<display:column title="开始时间" sortable="true"
								sortName="startTime">
								<fmt:formatDate value="${item.startTime}" type="both"
									dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" />
							</display:column>
							<display:column title="截止时间" sortable="true" sortName="endTime">
								<fmt:formatDate value="${item.endTime}" type="both"
									dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" />
							</display:column>
							<display:column title="更新时间" sortable="true"
								sortName="updateTime">
								<fmt:formatDate value="${item.updateTime}" type="both"
									dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" />
							</display:column>
										<%-- <display:column title="描述" property="themeDesc"
							style="width:250px"></display:column> --%>
		
							<display:column title="操作" media="html" style="width: 290px;">
									<div class="table-btn-group">
										<button
											class="tab-btn"
											onclick="window.location='${contextPath}/admin/theme/load/${item.themeId}'">
											编辑
										</button>
										<span class="btn-line">|</span>
										<c:choose>
											<c:when test="${item.status==0}">
												<button
													onclick="javascript:Off('${item.themeId}','${item.title}','1');"
													class="tab-btn"
													name="statusImg" status="${item.status}">
													上线
												</button>
												<span class="btn-line">|</span>
											</c:when>
											<c:when test="${item.status==1}">
												<button
													onclick="javascript:Off('${item.themeId}','${item.title}','0');"
													class="tab-btn">
													下线
												</button>
												<span class="btn-line">|</span>
											</c:when>
											<c:when test="${item.status==2}">
												<button
													class="tab-btn"
													onclick="deleteAction('${item.themeId}')">
													删除
												</button>
												<span class="btn-line">|</span>
												<button
													class="tab-btn"
													onclick="restore('${item.themeId}','${fn:escapeXml(fn:replace(item.title,"'","&acute;"))}')">
													还原
												</button>
											</c:when>
											<c:otherwise>
											</c:otherwise>
		
										</c:choose>
										<c:if test="${item.status!=2 && item.status!=1}">
											<button
												class="tab-btn"
												onclick="confirmDelete('${item.themeId}','${item.title}')">
												回收
											</button>
											<span class="btn-line">|</span>
										</c:if>
										<button
											class="tab-btn"
											onclick="window.open('${contextPath}/admin/theme/preview/${item.themeId}')">
											预览
										</button>
										<c:if test="${item.status==1}">
											<span class="btn-line">|</span>
										</c:if>
										<c:if test="${item.status==1}">
											<button
												class="tab-btn"
												onclick="lookLink('${item.themeId}');">
												查看链接
											</button>
										</c:if>
									</div>
							</display:column>
						</display:table>
						<div class="clearfix" style="margin-bottom: 60px;">
					   		<div class="fr">
					   			 <div cla ss="page">
					    			 <div class="p-wrap">
					        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
								 	</div>
					  			 </div>
					  		</div>
					   	</div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
</body>

<script language="JavaScript" type="text/javascript">
 	function toPage(obj,isRuleAttributes){
 		$(obj).parent().siblings().removeClass("am-active");
 		$(obj).parent().attr("class","am-active");
 		$("#title").val("");
 		$("#operetion").val(isRuleAttributes);
 		
 		var url = "${contextPath}/admin/theme/queryContent?status=" + isRuleAttributes
 				  + "&title=" + $("#title").val() + "&curPageNO=" + $("#curPageNO").val();
 		$("#themeContent").load(url);
 		
 	}
 	
	 	$(document).ready(function() {
	 		var status="${operation}";
	 		var ruleAttr ="ruleAttr"+status;//改变样式
	 		//$("#tabs ul li").attr("class","ui-state-default ui-corner-top");
	 		//$("#"+ruleAttr).attr("class","ui-state-default ui-corner-top ui-tabs-selected ui-state-active");
	 		
	 		highlightTableRows("item");  
	 	});
	 	
	 	
	 	function deleteAction(id)
		{
	 	   if(id==null){
	 		   return;
	 	   }
	 	   
		   layer.confirm("确定删除吗?(此删除没有后悔药!)", {icon:3}, function () {
			   var result;
			   jQuery.ajax({
					url:"${contextPath}/admin/theme/delete/" + id, 
					type:'post', 
					async : false, //默认为true 异步   
				    dataType : 'json', 
					error: function(jqXHR, textStatus, errorThrown) {
						layer.alert("出现异常", {icon:2});
				 		// alert(textStatus, errorThrown);
					},
					success:function(retData){
						result = retData;
					}
				});

		   		if('OK' == result){
		   			layer.msg('删除成功', {icon:1, time:1000}, function(){
			   			window.location.reload(true);
		   			});
		   		}else{
		   			layer.msg(result, {icon:2});
		   		}
			}, function () {
					//cancel
			});
		   
		}
	 	
	 	
	 	function confirmDelete(id,name)
		{
	 	   if(id==null){
	 		   return;
	 	   }
		   layer.confirm("确定要回收'"+name+"'吗？", {icon:3}, function () {
			   var result;
			   jQuery.ajax({
					url:"${contextPath}/admin/theme/updataStatus/" + id +"/2", 
					type:'post', 
					async : false, //默认为true 异步   
				    dataType : 'json', 
					error: function(jqXHR, textStatus, errorThrown) {
						layer.alert("出现异常", {icon:2});
				 		// alert(textStatus, errorThrown);
					},
					success:function(retData){
						result = retData;
					}
				});

		   		if('OK' == result){
		   			layer.msg('回收成功', {icon:1, time:1000}, function(){
			   			//window.location.reload(true);
			   			toPage(this,0);
		   			});
		   		}else{
		   			 layer.msg(result, {icon:2});
		   		}
			}, function () {
					//cancel
			});
		   
		}
	 	
	 	function Off(id,name,status){
	 	   if(id==null || status==null){
	 		   return;
	 	   }
	 	   var desc;
        	if(status == 0){
        	   	   desc ="确定要下线'"+name+"'吗";
        	}else{
        	       desc ="确定要上线'"+name+"'吗";
        	}
        	layer.open({
        		ttitle: '提示',
        		icon: 3,
        		content: desc,
        		btn: ['确定', '取消'],
        		yes: function(index, layero){
        			jQuery.ajax({
						url:"${contextPath}/admin/theme/updataStatus/"+ id +"/"+status, 
						type:'get', 
						async : false, //默认为true 异步  
						dataType : 'json',  
						success:function(result){
							if('OK' == result){
								if(status == 0){
									layer.msg('下线成功', {icon:1});
								}else{
									layer.msg('上线成功', {icon:1});
								}
					   			
					   			window.location.reload(true);
					   		}else{
					   			layer.msg(result, {icon:2});
					   		}
						}   
				    });
        			return true;
        		},
        		btn2: function(index, layero){
        			layer.close(index);
        		}
        	});
		}
	 	
	 	
	 	function restore(item_id,name){
	 		  if(item_id==null){
		 		   return;
		 	   }
			   layer.confirm("确定要还原'"+name+"'吗？", {icon:3}, function () {
				   var result;
				   jQuery.ajax({
						url:"${contextPath}/admin/theme/updataStatus/" + item_id +"/0", 
						type:'post', 
						async : false, //默认为true 异步   
					    dataType : 'json', 
						error: function(jqXHR, textStatus, errorThrown) {
							layer.alert("出现异常", {icon:2});
							// alert(textStatus, errorThrown);
						},
						success:function(retData){
							result = retData;
						}
					});

			   		if('OK' == result){
			   			layer.msg('还原成功', {icon:1, time: 1000}, function(){
				   			window.location.reload(true);
			   			});
			   		}else{
			   			layer.msg(result, {icon:2});
			   		}
				});
	 	}
	 	
	 	
 	
 	function pager(curPageNO){
		$.post("${contextPath}/admin/theme/queryContent", {"curPageNO" :curPageNO, "title": $("#title").val(), "status": $("#operetion").val()}, function(data) {
			$("#themeContent").html(data);
		});
    }
 	
 	function searchContent(){
		$.post("${contextPath}/admin/theme/queryContent", {"curPageNO" :1, "title": $("#title").val(), "status": $("#operetion").val()}, function(data) {
			$("#themeContent").html(data);
		});
	}
 	
 	function lookLink(themeId){
 		
 		var pcUrl = "${pcDomainName}/theme/themeDetail/"+themeId;
 		var vueUrl = "${vueDomainName}/theme/themeDetail?themeId=" + themeId;
 		layer.open({
 			title: '查看链接',
 			content:"pc端链接：<a href='" + pcUrl + "'>"+ pcUrl + "<\/a><br>手机端链接：<a href='" + vueUrl + "'>" + vueUrl + "<\/a>",
 			yes: function(index, layero){
 				layer.close(index);
 			}
 		});
 	}
	</script>
</html>
