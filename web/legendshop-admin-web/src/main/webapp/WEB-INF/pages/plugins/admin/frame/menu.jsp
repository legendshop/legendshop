<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
    <div class="am-offcanvas-bar admin-offcanvas-bar">
      <ul class="am-list admin-sidebar-list" id="secondMenuUl">
		<li><a href='<ls:url address="/admin/index"/>' data-type="index"><span class="am-icon-home"></span> 首页</a></li>
        <c:forEach items="${sessionScope.currentMenu.subList}"  var="subMenu" varStatus="status">
        <li class="admin-parent">
          <a class="am-cf${status.index} am-collapsed" data-am-collapse="{target: '#collapse-nav${status.index}'}">${subMenu.name}<span class="am-icon-angle-right am-fr am-margin-right"></span></a>
          <ul class='am-list am-collapse admin-sidebar-sub  <c:if test="${status.index ==0}">am-in</c:if>'  id="collapse-nav${status.index}">
          <c:forEach items="${subMenu.subList }" var="thirdMenu">
          		<li id="menuId-${thirdMenu.menuId}"><a  href="${contextPath}${thirdMenu.action}" class="am-cf${status.index}"  data-type="menu"  data-menu="${thirdMenu.menuId}">${thirdMenu.name}</a></li>
          </c:forEach>
          </ul>
        </li>
        </c:forEach>
      </ul>
      <div class="am-panel-default admin-sidebar-panel">
          <p><span class="am-icon-bookmark"></span> 公告</p>
          <p>欢迎使用 ${systemConfig.shopName}系统！</p>
      </div>
    </div>