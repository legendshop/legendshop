<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<html>
    <head>
        <title>创建品牌</title>
<link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/amazeui.css"/>
  <link rel="stylesheet" href="${contextPath}/resources/templets/amaze/css/admin.css">
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/plugins/weixin/css/imageDialog.css'/>" />
<%@ include file="/WEB-INF/pages/common/jquery2.1.4.jsp"%>
<script  charset="utf-8" src="<ls:templateResource item='/resources/plugins/weixin/js/imageDialog.js'/>"></script>
<script type="text/javascript">
var contextPath = '${contextPath}';
var type = '${type}';
</script>
</head>
<body style="height:500px;">
<div class="img_body">
	<div class="img_main">
		<div class="inner_main">
			<div class="bd">
				<div class="group_list">
					<div class="global_mod float_layout">
						<p class="global_info">
							<span class="group_name global_info_ele" data-id="1" id="js_currentgroup">${groupName}</span>
						</p>
						<div class="global_extra">
							<div class="bubble_tips bubble_right warn">
								<div class="bubble_tips_inner">建议尺寸：900像素 * 500像素</div>
								<i class="bubble_tips_arrow out"></i> <i class="bubble_tips_arrow in"></i>
							</div>
							<div class="upload_box align_right">
								<span class="upload_area webuploader-container">
									<a id="js_upload" onclick="uploadFile();" class="btn btn_primary webuploader-pick" data-gid="1">本地上传</a>
									<form:form  style='display: none;' id='formFile' name='formFile' method="post" action="${contextPath}/admin/weixinNewstemplate/uploadFile" target='frameFile' enctype="multipart/form-data">
										<input id="js_upload_btn" name="files" type="file" multiple="multiple" accept="image/*" >
										<input type="hidden" value="${type}" name="groupId">
									</form:form>
									<iframe id='frameFile' name='frameFile' style='display: none;'></iframe>
								</span>
							</div>
						</div>
					</div>

					<div class="group img_pick" id="js_imglist">
						<ul class="group">
							<c:if test="${empty requestScope.list}">
								<div class="empty_tips">该分组暂时没有图片素材</div>
							</c:if>
							<c:forEach items="${requestScope.list}" var="imgFile" varStatus="status">
								<li class="img_item js_imgitem">
									<div class="img_item_bd" onclick="selectImg(this);" data-id="${imgFile.id}" data-path="${imgFile.filePath}">
										<img class="pic wxmImg Zoomin" src="<ls:photo item='${imgFile.filePath}' />" > <span
											class="check_content"> <label class="frm_checkbox_label" ><i class="icon_checkbox"></i><span class="lbl_content">${imgFile.name}</span>
										</label> </span>
											<div class="selected_mask">
									            <div class="selected_mask_inner"></div>
									            <div class="selected_mask_icon"></div>
									        </div>
									</div>
								</li>
							</c:forEach>

						</ul>
						 <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/>
					</div>
				</div>
			</div>
		</div>
		<div class="inner_side">
			<div class="bd">
				<div class="group_list">
					<div class="inner_menu_box" id="js_group">
						<dl class="inner_menu">
							<c:forEach items="${requestScope.imgGroups}" var="g" varStatus="status">
								
								<dd class="inner_menu_item <c:if test="${type==g.id}">selected</c:if> js_groupitem" data-id="1">
									<a href="${contextPath}/admin/weixinNewstemplate/imageDialog?type=${g.id}" class="inner_menu_link" title='${g.id==0?"未分组":g.name}'> <strong>${g.id==0?"未分组":g.name}</strong><em
										class="num">(${g.count})</em> </a>
								</dd>
							</c:forEach>

						</dl>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
</body>
</html>