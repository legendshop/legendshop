<html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<head>
	<c:set var="_userDetail" value="${shopApi:getSecurityUserDetail(pageContext.request)}"></c:set>
	<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/floorTemplate.css" />
	<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/common/js/alternative.js'/>" type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.core.js'/>"  type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.widget.js'/>"  type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.mouse.js'/>"  type="text/javascript"></script>
	<script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.sortable.js'/>"  type="text/javascript"></script>
	<title>编辑文章</title>
</head>
<body>
<form:form  action=" " method="post" id="theForm">
<div class="box_floor" style=" ">
  <div class="box_floor_six">
    <div class="box_floor_class">
      <b>楼层标题：</b>
      <input name="gf_name" type="text" id="gf_name" value="文章" />
    </div>
    <span class="floor_choose">已选推荐的文章：</span>
    <div class="box_floor_prodel"> <em class="floor_warning">注释：最多选择7个文章，双击删除已有文章信息，按下鼠标拖动文章次序</em>
      <div class="floor_box_pls" id="floor_goods_info">
		<c:forEach items="${requestScope.floorItemList}" var="floorItem">
        <ul ondblclick="jQuery(this).remove();" class="floor_pro"  id="${floorItem.referId }">
          <li href='${contextPath}/news/${floorItem.referId}'>${floorItem.sortName }</li>
        </ul>
		</c:forEach>
        </div>
    </div>
    <span class="floor_choose">选择要展示的文章：</span>
    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
    <div class="floor_choose_box"> 
    <span class="floor_choose_sp">
       栏目
	    <select id="newsCategoryId" name="newsCategoryId">
	                 <ls:optionGroup type="select" required="false" cache="fasle"
	                sql=" select t.news_category_id, t.news_category_name from ls_news_cat t where t.status = 1 and t.shop_id = ?" param="${_userDetail.shopId}" selectedValue="${news.newsCategoryId}"/>
	      </select>
      <b>文章名称：</b>
      <input name="name" type="text" id="name" />
      <input type="button"  value="搜索" onclick="newsFloorLoad(${curPageNO});" style="cursor:pointer;" />
    </span> 
      <em class="floor_warning">注释：点击文章加入楼层，最多选择7个文章</em>
     <div id="floor_goods_list">
     	
     </div>
    </div>
  </div>
  <!--图片开始-->
  <div class="submit" style="text-align:center;">
    &nbsp;<input type="button" value="保存" onclick="saveForm( );" />
  </div>
</div>
</form:form>

</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/newsFloorOverLay.js'/>"></script>
<script type="text/javascript">
var contextPath = "${contextPath}";
</script>
</html>