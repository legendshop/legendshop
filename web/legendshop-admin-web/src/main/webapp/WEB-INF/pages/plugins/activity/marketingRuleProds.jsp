<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
	<c:if test="${not empty list}">
		 <table class="am-table am-table-striped am-table-hover see-able" >
					<thead>
						<tr>
							<th class="w150 tl" width="300px">商品名称</th>
							<th class="w150 tl" width="200px">商品价格</th>
							<c:if test="${marketing.type eq 2}">
								<th class="w150 tl" width="200px">促销价格</th>
							</c:if>
							<th class="w150 tl">商品库存</th>
							<ls:plugin pluginId="b2c">
								<th class="w150 tl">操作</th>
							</ls:plugin>
						</tr>
					</thead>
					<tbody id="ruleProds">
						<c:forEach items="${list}" var="prods" varStatus="status">
									   <tr>
											<td>
											<a target="_blank" href="${PC_DOMAIN_NAME}/views/${prods.prodId}">${prods.prodName}</a></td>
											<td><fmt:formatNumber value="${prods.cash}" pattern="#0.00#"/></td>
											<c:if test="${marketing.type eq 2}">
												<td>
										  			${prods.discountPrice}
												</td>
											</c:if>
											<td>
											   ${prods.stock}
											</td>
											 <ls:plugin pluginId="b2c">
												 <td>
												   <span>
													    <a href="javascript:void(0);" id="${prods.id}" onclick="deletemark(this);">移除</a>
													</span>
												</td>
											</ls:plugin>
										</tr>
						</c:forEach>
					</tbody>
		</table>
	</c:if>
    <div class="fanye">
					<c:if test="${not empty toolBar}">
					<div>
							<p align="right">
								<c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
							</p>
					</div>
		          </c:if> 
		</div>   
		
		<div class="clear"></div>
	
<script type="text/javascript">
     function ruleProdPager(_curPage){
          $("#prodContent").load(contextPath+"/admin/marketing/marketingRuleProds/"+marketId+"/"+shopId+"?curPage="+_curPage);
     }
     
      function deletemark(_this){
	     var id=$(_this).attr("id");
	     layer.confirm("确定要移除该商品麽？", {icon:3}, function () {
			$.ajax({
	        //提交数据的类型 POST GET
	        type:"POST",
	        //提交的网址
	        url:contextPath+"/admin/marketing/deleteMarketingRuleProds/"+marketId+"/"+id,
	        //提交的数据
	        async : false,  
	        //返回数据的格式
	        datatype: "json",
	        //成功返回之后调用的函数            
	        success:function(data){
	        	 var result=eval(data);
	        	 if(result=="OK"){
	        	     $(_this).parent().parent().parent().remove();
	        		 return ;
	        	 }else{
	        		 layer.alert(result, {icon:2});
	        		 return false;
	        	 }
	        }       
	    });
		});
	  }
</script>	
