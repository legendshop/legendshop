<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<html>
<head>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>"></script>
<link type="text/css" rel="stylesheet"  href="<ls:templateResource item='/resources/plugins/ztree/zTreeStyle.css'/>" >
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.min.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.excheck-3.5.js'/>"></script>
<script type="text/javascript">

	function saveFunctionMenu(){
			var zTree = $.fn.zTree.getZTreeObj("catTrees");
			var chkNodes = zTree.getCheckedNodes(true);
			if(chkNodes.length<1) {
				layer.msg("请选择类目", {icon:0});
				return;
			}
			var catId = chkNodes[0].id;
			var catName = chkNodes[0].name;
			parent.location.reload();
			var index = parent.layer.getFrameIndex('RoleMenu'); //先得到当前iframe层的索引
			parent.layer.close(index); //再执行关闭
	}
	
	$(document).ready(function(){
			//zTree设置
			var setting = {
					check:{
							enable: true,
							chkboxType: {'Y':'ps','N':'ps'},
							chkStyle: 'radio',
							radioType: 'all'
							}, 
					data:{
						simpleData:{ enable: true }
					}, callback:{
						beforeCheck: beforeCheck,
						onCheck: onCheck
					} };
			$.fn.zTree.init($("#catTrees"), setting, ${catTrees});
	});
	
		function beforeCheck(){
		}
		    
		function onCheck(){
		}
</script>
</head>
<body>
     	<fieldset style="padding-bottom: 30px; height:245px;">
			<legend>分类</legend>
			<div id="menuContentList" style="margin:10px 0px 10px 10px;height:245px;overflow-y:auto;" > 
		   		<ul id="catTrees" class="ztree"></ul>
	     	</div>
		</fieldset>
		<div align="center">
			<input style="margin:10px;padding:8px" type="button" class="s" onclick="javascript:saveFunctionMenu();" value="保存" id="Button1" name="Button1">
		</div>
</body>

</html>

