<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
	<style type="text/css">
		.productDetail{border:solid #dddddd; border-width:1px 0px 0px 1px;width: 100%;}
		.productDetail tr td{border:solid #dddddd; border-width:0px 1px 1px 0px; padding:10px 0px;text-align: center;}
	</style>
	<c:if test="${not empty productDetail}">
		<table class="productDetail">
			<tr >
				<td colspan="5"><b>商品信息</b></td>
			</tr>
			<tr>
				<td>商品名称</td>
				<td>商品价格</td>
				
			</tr>
			<tr id="list_${productDetail.prodId}">
				<td>${productDetail.name}</td>
				<td>
					${productDetail.price}
					<input type="hidden" id="prodId" value="${productDetail.prodId}"/>
				</td>
			</tr>
		</table>
	</c:if>
