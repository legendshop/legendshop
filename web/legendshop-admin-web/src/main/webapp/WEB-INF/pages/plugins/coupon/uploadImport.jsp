<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>

<%@include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ include file="/WEB-INF/pages/common/jquery1.9.1.jsp"%>
<html>
  <head>
  <link type="text/css" href="<ls:templateResource item='/resources/templets/css/multishop/seller-prod${_style_}.css'/>" rel="stylesheet">
<style type="text/css">
.a-upload {
	position: absolute;
    position: relative;
    display: inline-block;
    background: #FFF0F5;
    border: 1px solid #99D3F5;
    border-radius: 4px;
    padding: 4px 12px;
    overflow: hidden;
    color: #1E88C7;
    text-decoration: none;
    text-indent: 0;
    line-height: 20px;
}
.mybtn{
	background-color: #0e90d2;
    border: 0 none;
    border-radius: 2px;
    color: #fff;
    cursor: pointer;
    display: block;

    margin-top: 10px;
    padding: 7px 0;
    text-align: center;
    width: 96px;
}
</style>
  </head>
  <body>
  <form:form id="form1"  action="${contextPath}/admin/coupon/sendCoupon" enctype="multipart/form-data"  name="form1">
  	<input type="hidden" name="couponId" value="${data}"/>
  	<div align="center">
  			<span >请选择excel文件</span><br/><br/>
  			<input type="file" name="csvFile" id="file"  class="a-upload"><br/><br/><br/>
  			<input type="button"  class="mybtn" id="but" value="下载模板" onclick="downloadTemplet()"/>
  			<input type="button"  class="mybtn" id="but" value="确定" onclick="confirmUpload()"/> 
  	</div>
  </form:form>
  
  <script src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="<ls:templateResource item='/resources/templets/amaze/js/uploadImport.js'/>"></script>
	<script>
	 	var contextPath="${contextPath}";
	</script>
  </body>
  
</html>
