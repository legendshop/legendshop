<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer)request.getAttribute("offset");
%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	    <table class="${tableclass} title-border" style="width: 100%">
		    	<tr><th class="title-border">业务设置&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">敏感字管理</span></th></tr>
		</table>
    <form:form  action="${contextPath}/admin/system/sensitiveWord/query" id="form1" method="get">
   	      <div class="criteria-div">
	       	    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
	       	    <span class="item">
		       	    关键字：<input type="text" name="words"  id="words" maxlength="50"  value="${sensitiveWord.words}" class="${inputclass}"/>
		            <input type="button" onclick="search()" value="搜索" class="${btnclass}" />
		            <input type="button" value="创建敏感字过滤表" class="${btnclass}" onclick='window.location="<ls:url address='/admin/system/sensitiveWord/load'/>"'/>
	            </span>
	      </div>
    </form:form>
    <div align="center" class="order-content-list">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		<display:table name="list" requestURI="/admin/system/sensitiveWord/query" id="item" export="false" sort="external" class="${tableclass}" style="width:100%">
	    
	 <display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
     		<display:column title="敏感字" property="words"></display:column>
     		<%--<display:column title="是否全局敏感字"  style="width:120px">
	     			<ls:optionGroup type="label" required="true" cache="true" beanName="YES_NO" selectedValue="${item.isGlobal}"/> 
     		</display:column>--%>
	    <display:column title="操作" media="html"  style="width: 150px;">
		      <%-- <div class="am-btn-toolbar">
          		<div class="am-btn-group am-btn-group-xs">
                    <button class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="window.location='<ls:url address='/admin/system/sensitiveWord/load/${item.sensId}'/>'"><span class="am-icon-pencil-square-o"></span> 修改</button>
                	<button class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" onclick="deleteById('${item.sensId}');" ><span class="am-icon-trash-o"></span> 删除</button>
          		</div>
        	  </div> --%>
       		<div class="table-btn-group">
                 <button class="tab-btn" onclick="window.location='<ls:url address='/admin/system/sensitiveWord/load/${item.sensId}'/>'">修改</button>
                 <span class="btn-line">|</span>
             	 <button class="tab-btn" onclick="deleteById('${item.sensId}');" >删除</button>
       		</div>
	      </display:column>
	    </display:table>
        <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/>
    </div>	
    </div>
    </div>
    </body>
        <script language="JavaScript" type="text/javascript">
		
			  function deleteById(id) {
				  layer.confirm("确定删除该关键字 ?", {
						 icon: 3
					     ,btn: ['确定','关闭'] //按钮
					   }, function(){
						   window.location = "<ls:url address='/admin/system/sensitiveWord/delete/" + id + "'/>";
					   });
			    }
			
			        function pager(curPageNO){
			            document.getElementById("curPageNO").value=curPageNO;
			            document.getElementById("form1").submit();
			        }
			        function search(){
					  	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
					  	$("#form1")[0].submit();
					}
			//-->
		</script>
</html>

