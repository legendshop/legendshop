<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/themes/default/default.css'/>" />
<link rel="stylesheet" href="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.css'/>" />
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
<title>中奖纪录列表</title>
<%
        Integer offset = (Integer) request.getAttribute("offset");
%>
<style type="text/css">
.order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
}
</style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content" style="overflow-x: auto;">
		<table class="${tableclass} title-border" style="width: 100%">
		    	<tr>
			    	<th class="title-border">平台营销&nbsp;＞&nbsp;<a href="<ls:url address="/admin/drawActivity/query"/>">抽奖活动</a>
						&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">活动中奖记录</span>
			    	</th>
		    	</tr>
		</table>
    <form:form action="${contextPath}/admin/drawWinRecord/query" id="form1" method="post">
   	    <div class="criteria-div">
	       	    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
	       	    <input type="hidden" id="actId" name="actId" value="${drawWinRecord.actId}" />
	            	<%-- &nbsp;发货状态:&nbsp;
	            	<select name="sendStatus" class="${selectclass}">
	            	     <option value="">--请选择--</option>
	            	     <option value="0" <c:if test="${drawWinRecord.sendStatus eq 0}"> selected="selected" </c:if>
	            	     >未发货</option>
	            	     <option value="1" <c:if test="${drawWinRecord.sendStatus eq 1}"> selected="selected" </c:if>
	            	     >已发货</option>
	            	</select> --%>
	            	<span class="item">
		            	奖项类型&nbsp;
		            	<select name="awardsType" class="criteria-select">
		            	     <option value="">--请选择--</option>
		            	     <option value="1" <c:if test="${drawWinRecord.awardsType eq 1}"> selected="selected" </c:if>
		            	     >实体物</option>
		            	     <option value="2" <c:if test="${drawWinRecord.awardsType eq 2}"> selected="selected" </c:if>
		            	     >预付款</option>
		            	     <option value="3" <c:if test="${drawWinRecord.awardsType eq 3}"> selected="selected" </c:if>
		            	     >积分</option>
		            	</select>
	            	</span>
	            	<span class="item">
		            	抽奖时间&nbsp;
	      		        <input readonly="readonly"  name="startTime" id="startTime" type="text"   value='<fmt:formatDate value="${drawWinRecord.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>'/>
	      		        &nbsp;-&nbsp;
	      		        <input readonly="readonly"  name="endTime" id="endTime"  type="text"  value='<fmt:formatDate value="${drawWinRecord.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>' />
	            	</span>
	            	<span class="item">
	            		<input type="submit" value="搜索" class="${btnclass}"/>
	            	</span>
	      </div>
    </form:form>
    <div align="center" class="order-content-list">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		<display:table name="list" requestURI="/admin/drawWinRecord/query" id="item" export="false" sort="external" class="${tableclass}" style="width:100%">
	        <display:column title="序号"  class="orderwidth"><%=offset++%></display:column>
     		<display:column title="活动名称" property="actName"></display:column>
     		<display:column title="奖项类型">
     		<c:choose>
     		        <c:when test="${item.awardsType == 1}">实体物</c:when>
     		        <c:when test="${item.awardsType == 2}">预付款</c:when>
				    <c:when test="${item.awardsType == 3}">积分</c:when>
		     </c:choose>
     		</display:column>
     		<display:column title="手机号码" property="mobile"></display:column>
     		<display:column title="姓名" property="cusName"></display:column>
     		<display:column title="用户昵称" property="weixinNickname"></display:column>
     		<display:column title="送货地址" property="sendAddress"></display:column>
     		<display:column title="快递单号" property="courierNumber"></display:column>
     		<display:column title="快递公司" property="courierCompany"></display:column>
     		<display:column title="发货状态">
  		    <c:choose>
				<c:when test="${item.sendStatus == 0}">未发货</c:when>
				<c:when test="${item.sendStatus == 1}"><span style="color:red;">已发货</span></c:when>
		    </c:choose>
     		</display:column>
     		<display:column title="抽奖时间" property="awardsTime" format="{0,date,yyyy-MM-dd HH:mm:ss}"></display:column>
     		<display:column title="中奖描述"><div class="order_img_name">${item.winDes}</div></display:column>
	       <display:column title="操作" media="html">
	         <c:if test="${item.awardsType eq 1}">
	               <div class="table-btn-group">
					      <c:if test="${item.sendStatus eq 0}">
					         <button class="tab-btn" onclick="deliverGoods('${item.id}');" > 发货</button>
					      </c:if>
		       	   </div>
	        </c:if>
	      </display:column>
	    </display:table>
        <div class="clearfix">
	   		<div class="fr">
	   			 <div cla ss="page">
	    			 <div class="p-wrap">
	        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
				 	</div>
	  			 </div>
	  		</div>
   		</div>
    </div>
    
    
<script language="JavaScript" type="text/javascript">
	  function deleteById(id) {
		  layer.confirm('确定删除该活动 ?', {icon:0}, function(){
			  jQuery.ajax({
					url: "${contextPath}/admin/drawWinRecord/delete", 
					data: {"id":id}, 
					type:'POST', 
					async : true, //默认为true 异步   
					dataType : 'json', 
					success:function(data){
					   if(data=="OK"){
			        	  window.location.reload();
			        	}else{
			        	  layer.alert('删除失败！', {icon:2});
			        	}
					}   
			 });  
	        return true;
		  });
	 }
			
      function pager(curPageNO){
          document.getElementById("curPageNO").value=curPageNO;
          document.getElementById("form1").submit();
      }
		
	  /**发货**/	        
	  function deliverGoods(winId){
		  layer.open({
			  id:'deliverGoods',
    		  title: '确认发货',
    		  type:2,
    		  content: '${contextPath}/admin/drawWinRecord/toDeliverGoodsPage/'+winId,
    		  area: ['430px', '250px']
		  });
  		}
	  
	  laydate.render({
	   		 elem: '#startTime',
	   		 type:'datetime',
	   		 calendar: true,
	   		 theme: 'grid',
		 	 trigger: 'click'
	   	  });
	   	   
	   	  laydate.render({
	   	     elem: '#endTime',
	   	     type:'datetime',
	   	     calendar: true,
	   	     theme: 'grid',
		 	 trigger: 'click'
	      });
  
		</script>
</body>
</html>

