<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
  <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
		<table class="${tableclass}" style="width: 100%">
		    <thead>
		   		<tr><th> <strong class="am-text-primary am-text-lg">商城管理</strong> /  二级导航编辑</th></tr>
		    </thead>
		 </table>

        <form:form  action="${contextPath}/admin/system/navigationItem/save" method="post" id="form1">
            <input id="naviId" name="naviId" value="${navigationItem.naviId}" type="hidden">
            <input id="itemId" name="itemId" value="${navigationItem.itemId}" type="hidden">
            <div align="center">
            <table border="0" align="center" class="${tableclass}" id="col1" style="width: 100%">
                
		<tr>
		        <td style="width: 30%">
		          	<div align="right">名称: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="name" id="name" value="${navigationItem.name}" class="${inputclass}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="right">连接: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="link" id="link" value="${navigationItem.link}" style="width:450px" class="${inputclass}"/>
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="right">顺序: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="seq" id="seq" value="${navigationItem.seq}" class="${inputclass}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="right">状态: <font color="ff0000">*</font></div>
		       	</td>
		         <td>
		           <select id="status" name="status">
			<ls:optionGroup type="select" required="true" cache="true"
	               beanName="ONOFF_STATUS" selectedValue="${navigationItem.status}"/>
					</select>		
		        </td>
		</tr>
                <tr>
                    <td colspan="2">
                        <div align="center">
                            <input type="submit" value="保存" class="${btnclass}" />
                            <input type="button" value="返回" class="${btnclass}"
                                onclick="window.location='<ls:url address="/admin/system/navigation/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
        </div>
        </div>
        </body>

 <script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script language="javascript">
		    $.validator.setDefaults({
		    });

    $(document).ready(function() {
    jQuery("#form1").validate({
            rules: {
            name: "required",
			 seq: {
			  required: true,
              number: true
            },
            link: "required"
        },
        messages: {
           name:"请输入名字",
			seq: {
				required: "请输入顺序",
                number: "请输入数字"
            },
            link:"请输入链接"		
        }
    });
 
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
});
</script>
</html>
