<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="../back-common.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<div align="center">
	<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	<display:table name="list" requestURI="/admin/theme/query" id="item" export="false" sort="external" class="${tableclass}" style="width:100%">
		<display:column title="图片" >
			<a href="${contextPath}/admin/theme/preview/${item.themeId}"
				target="_blank"  style="margin: 10px;">
				<img width="65" height="65"
				src="<ls:photo item="${item.themePcImg}"/>"
				alt="${item.title}"> 
			</a>
		</display:column>
		<display:column title="专题名称">
			<a href="${contextPath}/admin/theme/preview/${item.themeId}"
				target="_blank" title="${item.title}" style="margin: 10px;">
				<div class="order_img_name">${item.title}</div>
			</a>
		</display:column>
		<display:column title="开始时间" sortable="true" sortName="startTime">
			<fmt:formatDate value="${item.startTime}" type="both" dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" />
		</display:column>
		<display:column title="截止时间" sortable="true" sortName="endTime">
			<fmt:formatDate value="${item.endTime}" type="both" dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" />
		</display:column>
		<display:column title="更新时间" sortable="true" sortName="updateTime">
			<fmt:formatDate value="${item.updateTime}" type="both" dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" />
		</display:column>
		<display:column title="操作" media="html" style="width: 290px;">
	 		<div class="table-btn-group">
				<button
					class="tab-btn"
					onclick="window.location='${contextPath}/admin/theme/load/${item.themeId}'">
					编辑
				</button>
				<span class="btn-line">|</span>
				<c:choose>
					<c:when test="${item.status==0}">
						<button
							onclick="javascript:Off('${item.themeId}','${item.title}','1');"
							class="tab-btn"
							name="statusImg" status="${item.status}">
							上线
						</button>
						<span class="btn-line">|</span>
					</c:when>
					<c:when test="${item.status==1}">
						<button
							onclick="javascript:Off('${item.themeId}','${item.title}','0');"
							class="tab-btn">
							下线
						</button>
						<span class="btn-line">|</span>
					</c:when>
					<c:when test="${item.status==2}">
						<button
							class="tab-btn"
							onclick="deleteAction('${item.themeId}')">
							删除
						</button>
						<span class="btn-line">|</span>
						<button
							class="tab-btn"
							onclick="restore('${item.themeId}','${fn:escapeXml(fn:replace(item.title,"'","&acute;"))}')">
							还原
						</button>
					</c:when>
					<c:otherwise>
					</c:otherwise>

				</c:choose>
				<c:if test="${item.status!=2 && item.status!=1}">
					<button
						class="tab-btn"
						onclick="confirmDelete('${item.themeId}','${item.title}')">
						回收
					</button>
					<span class="btn-line">|</span>
				</c:if>
				<button
					class="tab-btn"
					onclick="window.open('${contextPath}/admin/theme/preview/${item.themeId}')">
					预览
				</button>
				<c:if test="${item.status==1}">
					<span class="btn-line">|</span>
				</c:if>
				<c:if test="${item.status==1}">
					<button
						class="tab-btn"
						onclick="lookLink('${item.themeId}');">
						查看链接
					</button>
				</c:if>
			</div>
		</display:column>

	</display:table>
	<div class="clearfix" style="margin-bottom: 60px;">
   		<div class="fr">
   			 <div cla ss="page">
    			 <div class="p-wrap">
        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			 	</div>
  			 </div>
  		</div>
   	</div>
</div>