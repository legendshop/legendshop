<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>推广员管理 - 后台管理</title>
	<meta name="description" content="LegendShop 多用户商城系统">
	<meta name="keywords" content="index">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit">
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
	<meta name="apple-mobile-web-app-title" content="Amaze UI" />
	<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>


<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} no-border" style="width: 100%;">
				<thead>
					<tr>
						<th class="title-border">
							推广管理&nbsp;＞&nbsp;
							<a href="<ls:url address="/admin/userCommis/query"/>">推广员列表</a>&nbsp;＞&nbsp;
							<span style="color:#0e90d2; font-size:14px;">推广员详情</span>
						</th>
					</tr>
				</thead>
			</table>

			<input id="id" name="id" value="${userCommis.id}" type="hidden">
			<div align="center" style="margin-bottom:50px;">

				<table border="0" align="center" style="width:95%;margin:10px;" class="${tableclass} see-able" id="col1">
					<thead>
						<tr class="sortable">
							<th colspan="4">
								<div align="left">基本信息</div>
							</th>
						</tr>
					</thead>

					<tr>
						<td width="150">
							<div align="right">用户ID:</div>
						</td>
						<td width="200" style="text-align:left;">${userDetail.userName}</td>
						<td width="150">
							<div align="right">用户昵称:</div>
						</td>
						<td style="text-align:left;">${userDetail.nickName}</td>
					</tr>
					<tr>
						<td>
							<div align="right">真实姓名:</div>
						</td>
						<td style="text-align:left;">${userDetail.realName}</td>
						<td>
							<div align="right">手机号码:</div>
						</td>
						<td style="text-align:left;">${userDetail.userMobile}</td>
					</tr>
					<tr>
						<td>
							<div align="right">邮箱:</div>
						</td>
						<td style="text-align:left;">${userDetail.userMail}</td>
						<td>
							<div align="right">详细地址:</div>
						</td>
						<td style="text-align:left;">${userDetail.userAdds}</td>
					</tr>

				</table>

				<table border="0" align="center" style="width:95%;margin:10px;" class="${tableclass} see-able" id="col2">
					<thead>
						<tr class="sortable">
							<th colspan="4">
								<div align="left">推广信息</div>
							</th>
						</tr>
					</thead>
					<tr>
						<td width="200">
							<div align="right">分销上级用户ID:</div>
						</td>
						<td width="200" style="text-align:left;">${userCommis.parentUserName}</td>
						<td width="160">
							<div align="right">绑定上级时间:</div>
						</td>
						<td style="text-align:left;">${userCommis.parentBindingTime}</td>
					</tr>
					<tr>
						<td>
							<div align="right">直接下级会员数量:</div>
						</td>
						<td style="text-align:left;">${subUserCount.firstCount}</td>
						<td>
							<div align="right">直接下级会员贡献佣金:</div>
						</td>
						<td style="text-align:left;">￥${userCommis.firstDistCommis}</td>
					</tr>
					<tr>
						<td>
							<div align="right">下两级会员数量:</div>
						</td>
						<td style="text-align:left;">${subUserCount.secondCount}</td>
						<td>
							<div align="right">下两级会员贡献佣金:</div>
						</td>
						<td style="text-align:left;">￥${userCommis.secondDistCommis}</td>
					</tr>
					<tr>
						<td>
							<div align="right">下三级会员数量:</div>
						</td>
						<td style="text-align:left;">${subUserCount.thirdCount}</td>
						<td>
							<div align="right">下三级会员贡献佣金:</div>
						</td>
						<td style="text-align:left;">￥${userCommis.thirdDistCommis}</td>
					</tr>
					<tr>
						<td>
							<div align="right">累积赚得佣金:</div>
						</td>
						<td colspan="3"  style="text-align:left;">￥${userCommis.totalDistCommis}</td>
					</tr>
					<tr>
						<td>
							<div align="right">已结算佣金:</div>
						</td>
						<td style="text-align:left;">￥${userCommis.settledDistCommis}</td>
						<td>
							<div align="right">未结算佣金:</div>
						</td>
						<td style="text-align:left;">￥${userCommis.unsettleDistCommis}</td>
					</tr>
					<tr>
						<td>
							<div align="right">推广员申请时间:</div>
						</td>
						<td style="text-align:left;"><fmt:formatDate value="${userCommis.promoterApplyTime}"
								pattern="yyyy-MM-dd HH:mm:ss" /></td>
						<td>
							<div align="right">成为推广员时间:</div>
						</td>
						<td style="text-align:left;"><fmt:formatDate value="${userCommis.promoterTime}"
								pattern="yyyy-MM-dd HH:mm:ss" /></td>
					</tr>
				</table>

				<div style="margin: 10px;">
					<ul class="am-tabs-nav am-nav am-nav-tabs">
						<li class="am-active" onclick="loadSubUser('first',this);"><i></i><a
							href="#">直接下级用户</a></li>
						<li onclick="loadSubUser('second',this);"><i></i><a href="#">下两级用户</a></li>
						<li onclick="loadSubUser('third',this);"><i></i><a href="#">下三级用户</a></li>
					</ul>
				</div>
				<div id="subUserDiv" style="margin-left: 11px;"></div>
			</div>
		</div>
	</div>
</body>
<script src="${contextPath}/resources/common/js/jquery.validate.js"
	type="text/javascript"></script>
<script>
	$(document).ready(function() {
		jQuery.ajax({
			url : "${contextPath}/admin/userCommis/loadSubLevel/${userDetail.userName}",
			type : 'get',
			async : false, // 默认为true 异步
			success : function(result) {
				$("#subUserDiv").html(result);
			}
		});
	});

	var currLevel = 'first';
	function loadSubUser(level, ths) {
		currLevel = level;
		$(ths).addClass("am-active").siblings().removeClass("am-active");
		jQuery.ajax({
			url : "${contextPath}/admin/userCommis/loadSubLevel/${userDetail.userName}?level="+level,
			type : 'get',
			async : false, // 默认为true 异步
			success : function(result) {
				$("#subUserDiv").html(result);
			}
		});
	}

	function pager(curPageNO) {
		jQuery.ajax({
			url : "${contextPath}/admin/userCommis/loadSubLevel/${userDetail.userName}?level="+currLevel+"&curPageNO="+curPageNO,
			type : 'get',
			async : false, // 默认为true 异步
			success : function(result) {
				$("#subUserDiv").html(result);
			}
		});
	}
</script>
</html>