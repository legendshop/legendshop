<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
</head>
<style>
</style>
<body>
	<div class="box_floor_tit">
		<form:form  id="form1" action="${contextPath}/admin/floorItem/advListOverlay/${floorId}">
		标题：
		    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}">
			<input type="text" name="title" id="title" maxlength="50" value="${title}" />
			<input type="hidden"  name="layout" value="${layout}">
			<input type="hidden"  name="limit" value="${limit}">
			<input type="submit" value="搜索"/ class="edit-blue-btn">
			<input type="button" value="创建图片" onclick='loadAdv(${floorId});' class="edit-blue-btn" />
		</form:form>
	</div>
	<div align="center">
    <table class="simple" style="width:100%;border: 1px solid #ddd;">
    	<thead>
    		<tr class="adv-th">
		   		<th>广告标题</th>
		   		<th>链接地址</th>
		   		<th>图片</th>
		   		<th>操作</th>
		   	</tr>
    	</thead>
    		<c:forEach items="${requestScope.list}" var="adv" varStatus="status">
		   		<tr id="adv_${adv.fiId}" class="adv-td">
		   			<td style="width: 150px;">${adv.title}</td>
		   			<td style="width: 200px;">${adv.linkUrl}</td>
		   			<td><img src="<ls:images item='${adv.picUrl}' scale='2'/>" /></td>
		   			<td>
		   				<a href="javascript:updateAdv('${floorId}','${adv.fiId}')" title="修改">修改</a>
		   				
		   				<a href="javascript:deleteById('${adv.referId}','${adv.fiId}')" title="删除">删除</a>
		   			</td>
		   		</tr>
    		</c:forEach>
    </table>
    </div>
</body>
</html>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/advListOverlay.js'/>"></script>
<script type="text/javascript">
var contextPath = "${contextPath}";
</script>
