<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>报表管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">报表管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">广告点击量统计</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/advAnalysis" id="form1"
				method="post">
					<div class="criteria-div">
						<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
						<span class="item">
						广告链接： <input type="text" id="url" name="url" maxlength="50" value="${url}" class="${inputclass}" />
						<!-- 
						&nbsp;手机号码 <input type="text" id="userMobile" name="userMobile" maxlength="50" value="${userMobile}" class="${inputclass}" />
						 -->
						<input type="submit" onclick="search()" value="搜索" class="${btnclass}" />
						</span>
					</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list" requestURI="/admin/advAnalysis" id="item"
					export="false" class="${tableclass}" style="width:100%"
					sort="external">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="广告链接" property="url"></display:column>
					<display:column title="点击次数">${item.number }</display:column>
					<display:column title="操作">
						<div class="table-btn-group">
							<button class="tab-btn" onclick="window.location.href='${contextPath}/admin/advAnalysis/detail?url=${item.url}'">查看详情</button>
						</div>
					</display:column>
				</display:table>
				<div class="clearfix">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
	       	</div>
			</div>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		sendData(1);
	}
</script>
</html>


