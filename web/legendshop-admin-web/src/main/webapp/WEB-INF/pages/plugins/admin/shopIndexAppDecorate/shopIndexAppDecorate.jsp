<!Doctype html>
<html class="no-js fixed-layout">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@include file='/WEB-INF/pages/common/layer.jsp'%>
<%@include file='/WEB-INF/pages/common/laydate.jsp'%>
<%@ include file="../back-common.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>移动装修- 店铺首页管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item="/resources/common/css/pageManagement.css"/>" />
<link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" /
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content" style="overflow-x: auto;">
			<div class="seller_right" style="margin-bottom:70px;">
				<table class="${tableclass} title-border" style="width: 100%">
						<tr><th class="title-border">移动装修 &nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">店铺首页管理<span></th></tr>
				</table>
				<div class="user_list">
					<div>
						<form:form id="ListForm" method="get" action="${contextPath}/admin/app/shopIndexDecorate/query">
							<input type="hidden" value="${curPageNO==null?1:curPageNO}" id="curPageNO" name="curPageNO">
							
							<div class="criteria-div">
								<span class="item">
									<input type="text" id="shopId" name="shopId" value="${shopAppDecorate.shopId}"/>
                   					<input type="hidden" id="siteName" name="siteName" value="${shopAppDecorate.siteName}" class="${inputclass}"/> 
                   					<input class="${btnclass}" type="button" onclick="search()" value="查询">
								</span>
							</div>
						</form:form>
					</div>
					<div align="left" id="shopIndexDecorateContent" name="shopIndexDecorateContent" class="order-content-list"></div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript" src="${contextPath}/resources/plugins/select2-3.5.2/select2.js"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/shopIndexDecorate.js'/>"></script>
<script>
	var contextPath = "${contextPath}";
	var siteName = "${shopAppDecorate.siteName}";
</script>
</body>
</html>
