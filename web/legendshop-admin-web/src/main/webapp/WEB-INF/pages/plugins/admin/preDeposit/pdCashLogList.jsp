<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>预存款管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<style>
.order_img_name {
    text-overflow: -o-ellipsis-lastline;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    max-height: 36px;
    line-height: 18px;
    word-break: break-all;
}
</style>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">预存款管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">预存款管理</span></th>
				</tr>
			</table>
			<div>
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li><i></i><a
							href="<ls:url address="/admin/preDeposit/pdRechargeQuery"/>">充值管理</a></li>
						<li><i></i><a
							href="<ls:url address="/admin/preDeposit/pdWithdrawCashQuery"/>">提现管理</a></li>
						<li class="am-active"><i></i><a>预存款明细</a></li>
					</ul>
				</div>
			</div>
			<form:form action="${contextPath}/admin/preDeposit/pdCashLogQuery"
				id="form1" method="post">
					<div class="criteria-div">
						<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
						<span class="item">
							用户名：<input class="${inputclass}" type="text" name="userName" id="userName" maxlength="50" value="${bean.userName}" placeholder="用户名" /> 
						</span>
						<span class="item">
							用户昵称：<input class="${inputclass}" type="text" name="nickName" id="nickName" maxlength="50" value="${bean.nickName}" placeholder="用户昵称" /> 
							<input class="${btnclass}" type="submit" value="搜索" />
						</span>
					</div>
			</form:form>
			<div align="center" class="order-content-list">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>

				<display:table name="list"
					requestURI="/admin/preDeposit/pdCashLogQuery" id="item"
					export="false" class="${tableclass}" sort="external">
					<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
					<display:column title="充值流水号" property="sn"></display:column>
					<display:column title="用户名" property="userName"></display:column>
					<display:column title="用户昵称" property="nickName"></display:column>
					<display:column title="创建时间" format="{0,date,yyyy-MM-dd HH:mm:ss}" property="addTime"></display:column>
					<display:column title="金额变更(元)" property="amount" sortable="true" sortName="amount"></display:column>
					<display:column title="日志描述"><div class="order_img_name">${item.logDesc}</div></display:column>
					<%--   <display:column title="操作" media="html" style="width:210px;">
	  	 	  <div class="am-btn-toolbar">
	              <div class="am-btn-group am-btn-group-xs">
				  	  	<button class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="#"><span class="am-icon-search"></span> 查看</button>
				  	  	<button class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" onclick="#" ><span class="am-icon-trash-o"></span> 删除</button>
	  			 </div>
	  		 </div>
	  	  </display:column>
	   --%>
				</display:table>
				<div class="clearfix">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
		       	</div>
			<table style="width: 100%; border: 0px; margin-top: 10px;" class="${tableclass }">
				<tr>
					<td align="left" style="border: 0;background: #f9f9f9;text-align: left;">说明：<br> 1.此处展示了预存款详细的变更日志信息<br>
					</td>
				<tr>
			</table>
			</div>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	function pager(curPageNO) {
		document.getElementById("curPageNO").value = curPageNO;
		document.getElementById("form1").submit();
	}

	function deleteById(id) {
		if (confirm("  确定删除 ?")) {
			window.location = "${contextPath}/admin/productcomment/delete/"
					+ id;
		}
	}
	highlightTableRows("item");
</script>
</html>
