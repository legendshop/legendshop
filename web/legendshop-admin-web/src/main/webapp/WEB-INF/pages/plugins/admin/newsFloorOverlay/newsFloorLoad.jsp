<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<div align="center" style="clear: both;padding-top: 5px;padding-bottom: 5px;">
    <display:table name="list" requestURI="/admin/floorItem/newsFloorLoad" id="item" export="false" class="${tableclass}"  style="padding-right: 10px;width: 100%">
       <display:column title="商城" property="userName" ></display:column>
      <display:column title="标题">
		      <a href="javascript:void(0);"  onclick="setNewsList('${item.newsId}','${item.newsTitle}');">${item.newsTitle}</a>
      </display:column>
      <display:column title="更新时间"><fmt:formatDate value="${item.newsDate}" pattern="yyyy-MM-dd"/></display:column>
    </display:table>
       <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"  actionUrl="javascript:selectNewsPager"/>
</div>

