<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
	<div class="kui-body" style="width: 150px;">
			<div  class="kui-list kui-group">
			       <div data-value=""  class="kui-option" title=""></div>
				   <c:forEach items="${list }" var="group" >
				        <div  class="kui-option" title="${group.name }"  data-value="${group.id }">${group.name }</div>
				   </c:forEach>
		   </div>
	</div>