<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

    <%
        Integer offset = (Integer) request.getAttribute("offset");
    %>
    <form:form  action="${contextPath}/admin/weixinAutoresponse/query" id="form1" method="post">
        <table class="${tableclass}" style="width: 100%">
		    <thead>
		    	<tr>
			    	<th>
				    	<strong class="am-text-primary am-text-lg">微信管理</strong> /  微信消息自动回复管理
			    	</th>
		    	</tr>
		    </thead>
		    <tbody><tr><td>
		    	     <div align="right" style="padding: 3px">
					            	<input class="${btnclass}" type="button" value="创建消息自动回复" onclick='window.location="<ls:url address='/admin/weixinAutoresponse/load'/>"'/>
					  </div>
		    </td></tr></tbody>
	    </table>
    </form:form>
    <div align="center">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		 <display:table name="list" requestURI="/admin/weixinAutoresponse/query" id="item" export="false" sort="external" class="${tableclass}" style="width:100%">
		 	<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
     		<display:column title="微信触发类型 ">
     		      <c:choose>
				     <c:when test="${item.msgTriggerType=='text'}">文本消息</c:when>
				     <c:when test="${item.msgTriggerType=='news'}">图文消息</c:when>
				       <c:when test="${item.msgTriggerType=='image'}">图片消息</c:when>
				         <c:when test="${item.msgTriggerType=='image'}">图片消息</c:when>
				           <c:when test="${item.msgTriggerType=='voice'}">音频消息</c:when>
				             <c:when test="${item.msgTriggerType=='video'}">视频消息</c:when>
				 </c:choose>
     		</display:column>
     		<display:column title="回复语类型  ">
     		      <c:choose>
				     <c:when test="${item.msgType=='text'}">文本消息</c:when>
				     <c:when test="${item.msgType=='news'}">图文消息</c:when>
				       <c:when test="${item.msgType=='image'}">图片消息</c:when>
				         <c:when test="${item.msgType=='image'}">图片消息</c:when>
				           <c:when test="${item.msgType=='voice'}">音频消息</c:when>
				             <c:when test="${item.msgType=='video'}">视频消息</c:when>
				 </c:choose>
     		</display:column>
     		<display:column title="消息模版" property="templateName" >
     		</display:column>
		    <display:column title="" media="html"  style="width:220px">
		    	<div class="am-btn-toolbar">
				  <div class="am-btn-group am-btn-group-xs">
						<button class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="window.location='${contextPath}/admin/weixinAutoresponse/load/${item.id}'"><span class="am-icon-pencil-square-o"></span> 修改</button>
						<button class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" onclick="deleteById('${item.id}')" ><span class="am-icon-trash-o"></span> 删除</button>
				  </div>
				</div>
		      </display:column>
	    </display:table>
        <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/> 
	    </div>
	<table class="am-table am-table-striped am-table-hover table-main">
		<tbody>
			<tr>
				<td align="left">说明：<br> 1.针对于用户行为，您可以设定文字、语音、图文、图片 作为自动回复，
				      微信用户发送任意消息，服务会根据微信用户发送的消息类型来自动回复你编辑好的内容；
				            如：用户发送 “你好！”; 服务端回复：“亲爱用户您好：回复0查看目录 A推荐商品 B查看个人资料”
					</td>
			</tr>
			<tr>
			</tr>
		</tbody>
	</table>
<script language="JavaScript" type="text/javascript">
			  function deleteById(id) {
				  layer.confirm("删除会影响微信服务确定删除 ?", {
						 icon: 3
					     ,btn: ['确定','关闭'] //按钮
					   }, function(){
						   window.location = "<ls:url address='/admin/weixinAutoresponse/delete/" + id + "'/>";
					   });
			    }
         highlightTableRows("item");
		</script>

