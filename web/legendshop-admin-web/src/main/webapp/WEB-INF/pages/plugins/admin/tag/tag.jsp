<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />

 <form:form  action="${contextPath}/admin/tag/save" method="post" id="form1">
   <input id="tagId" name="tagId" value="${tag.tagId}" type="hidden">
   <table class="${tableclass}" style="width: 100%">
    <thead>
    	<tr><th> <strong class="am-text-primary am-text-lg">商城管理</strong> /  标签编辑</th></tr>
    </thead>
    </table>
            <div align="center">
            <table border="0" align="center" class="${tableclass}" id="col1">
     	<tr>
        <td>
          <div align="center">名称 <font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" name="name" id="name" value="${tag.name}" class="${inputclass}" />
        </td>
      </tr>
      
      <tr>
        <td>
          <div align="center">标签类型 <font color="ff0000">*</font></div>
       </td>
        <td>
            <select id="type" name="type">
	       			<ls:optionGroup type="select" required="true" cache="true" beanName="TAG_TYPE" selectedValue="${tag.type}"/>
			</select>
        </td>
      </tr>
      
        <tr>
        <td>
          <div align="center">编码<font color="ff0000">*</font></div>
       </td>
        <td>
           <input type="text" name="code" id="code" value="${tag.code}" class="${inputclass}"  />
        </td>
      </tr>
      
      <tr>
      <td>
          <div align="center">状态<font color="ff0000">*</font></div>
       </td>
        <td>
           <select id="status" name="status" value="${tag.status}">
            <option value='1' <c:if test="${tag.status == '1'}">selected=true</c:if>>上线</option>
            <option value='0' <c:if test="${tag.status == '0'}">selected=true</c:if>>下线</option>
	        <%-- <ls:optionGroup type="select" required="true" cache="true"
	                beanName="YES_NO" selectedValue="${tag.status}"/> --%>
			</select>
           
        </td>
      </tr>
      
          
      <tr>
      <td>
          <div align="center">顺序<font color="ff0000">*</font></div>
       </td>
        <td>
			<input type="text"  value="${tag.seq}" name="seq"  class="${inputclass}" style="width: 35px;"/>           
        </td>
      </tr>
      
      
      <tr>
                    <td colspan="2">
                        <div align="center">
                            <input type="submit" value="保存" class="${btnclass}" />
                            <input type="button" value="返回" class="${btnclass}"
                                onclick="window.location='<ls:url address="/admin/tag/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
 <script type="text/javascript">
   $.validator.setDefaults({
   });

    $(document).ready(function() {
     jQuery("#form1").validate({
            rules: {
            name: {
                required: true
            },
            code:{
               required: true
            },
            seq:{
               required: true,
               digits:true
            }
        },
        messages: {
            name: {
                required: "请输入名称"
            },
            code:{
               required: "请输入标签"
            },
            seq:{
               required: "请输入顺序",
               digits:"请输入正确数字"
            }
        }
     });
     highlightTableRows("col1");
 		//斑马条纹
    $("#col1 tr:nth-child(even)").addClass("even");
		});
</script>


