<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<li class="box_edit_li">
  <h6><a id="${category.id}" level="1"  href="javascript:void(0);" ondblclick="javascript:jQuery(this.parentNode.parentNode).remove();">${category.name}</a></h6>
  <span id="ui-nsort"  class="ui-nsortable">
         <c:forEach items="${categories}" var="nsort">
             <a  id="${nsort.id}"  pid="${nsort.parentId}"  level="${nsort.grade}"  href="javascript:void(0);" ondblclick="javascript:jQuery(this).remove();">${nsort.name}</a>
         </c:forEach>
  </span>
</li>
<script type="text/javascript">
jQuery(document).ready(function(){
	  jQuery(".ui-nsortable").sortable();
	  jQuery(".ui-nsortable").disableSelection();
});
</script>