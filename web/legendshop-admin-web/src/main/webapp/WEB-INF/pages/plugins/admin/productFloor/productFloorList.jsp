<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
    <%@ include file="/WEB-INF/pages/common/layer.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<html>
<head>
    <title>列表</title>
</head>
<%
        Integer offset = (Integer) request.getAttribute("offset");
 %>
<body>
    <form:form  action="${contextPath}/admin/system/productFloor/query" id="form1" method="post">
        <table class="${tableclass}" style="width: 100%">
		    <thead>
		    	<tr>
			    	<th>
				    	<a href="<ls:url address='/admin/system/index'/>" target="_parent">首页</a> &raquo; 商城管理  &raquo; 
				    	<a href="<ls:url address='/admin/system/productFloor/query'/>"></a>
			    	</th>
		    	</tr>
		    </thead>
		    <tbody><tr><td>
		    	    <div align="left" style="padding: 3px">
				       	    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
				            &nbsp;商城名称
				            <input type="text" name="userName" maxlength="50" value="${productFloor.userName}" />
				            <input type="submit" value="搜索"/>
				            <input type="button" value="创建首页楼层" onclick='window.location="<ls:url address='/admin/system/productFloor/load'/>"'/>
				      </div>
		     </td></tr></tbody>
	    </table>
    </form:form>
    <div align="center">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		<display:table name="list" requestURI="/admin/system/productFloor/query" id="item" export="false" sort="external" class="${tableclass}" style="width:100%">
	    	
	    	<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
     		<display:column title="楼层标题" property="name"></display:column>
     		<display:column title="商品分类" property="sort"></display:column>
     		<display:column title="产品列表" property="prods"></display:column>
     		<display:column title="团购产品列表" property="gprods"></display:column>
     		<display:column title="品牌列表" property="brands"></display:column>
     		<display:column title="文章列表" property="news"></display:column>
     		<display:column title="排序" property="seq"></display:column>

	    <display:column title="操作" media="html">
	    		<c:choose>
					 <c:when test="${item.status == 1}">
					  		<img name="statusImg"  itemId="${item.id}"  itemName="${item.name}"  status="${item.status}"  class="cursor_pointer"   src="<ls:templateResource item='/resources/common/images/blue_down.png'/> ">
					 </c:when>
					 <c:otherwise>
					  		<img  name="statusImg"  itemId="${item.id}"  itemName="${item.name}"  status="${item.status}"   class="cursor_pointer"  src="<ls:templateResource item='/resources/common/images/yellow_up.png'/> ">
					 </c:otherwise>
				</c:choose>   
		      <a href="<ls:url address='/admin/system/productFloor/load/${item.id}'/>" title="修改">
		     		 <img alt="修改" src="<ls:templateResource item='/resources/common/images/grid_edit.png'/>">
		      </a>
		      <a href='javascript:deleteById("${item.id}")' title="删除">
		      		<img alt="删除" src="<ls:templateResource item='/resources/common/images/grid_delete.png'/>">
		      </a>
	      </display:column>
	    </display:table>
        <c:if test="${not empty toolBar}">
            <c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
        </c:if>
    </div>
    <script src="<ls:templateResource item='/resources/common/js/alternative.js'/>" type="text/javascript"></script>
    <script src="<ls:templateResource item='/resources/common/js/recordstatus.js'/>" type="text/javascript"></script>
        <script language="JavaScript" type="text/javascript">
			  function deleteById(id) {
				  layer.confirm("确定删除 吗?", {
						 icon: 3
					     ,btn: ['确定','关闭'] //按钮
					   }, function(){
						   window.location = "<ls:url address='/admin/system/productFloor/delete/" + id + "'/>";
					   });
			    }
			
		        function pager(curPageNO){
		            document.getElementById("curPageNO").value=curPageNO;
		            document.getElementById("form1").submit();
		        }
			    
	            $(document).ready(function(){
	               $("img[name='statusImg']").click(function(event){
	                	$this = $(this);
	                	initStatus($this.attr("itemId"), $this.attr("itemName"),  $this.attr("status"), "${contextPath}/admin/system/productFloor/updatestatus/", $this,"${contextPath}");
	             		}
	             		 );
	                    	
	             		 highlightTableRows("item"); 
	                });
		</script>
</body>
</html>

