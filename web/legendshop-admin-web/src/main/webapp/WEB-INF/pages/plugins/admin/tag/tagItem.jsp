<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<script type='text/javascript'
	src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />

<form:form  action="${contextPath}/admin/tag/saveTagItems" method="post"
	id="form1">
	<input id="itemId" name="itemId" value="${tagItems.itemId}"
		type="hidden" /> <input type="hidden" name="tagId" id="tagId"
		value="${tagId}" />
		<input type="hidden" name="createTime"
					id="createTime" value="<fmt:formatDate value="${tagItems.createTime}" pattern="yyyy-MM-dd HH:mm:ss" />"
					 readonly="readonly" />
	<table class="${tableclass}" style="width: 100%">
		<thead>
			<tr>
				<th><strong class="am-text-primary am-text-lg">商城管理</strong> /
					标签子标签</th>
			</tr>
		</thead>
	</table>
	<div align="center">
		<table border="0" align="center" class="${tableclass}" id="col1">
			<tr>
				<td>
					<div align="center">
						子标签名称: <font color="ff0000">*</font>
					</div></td>
				<td><input type="text" name="itemName" id="itemName"
					value="${tagItems.itemName}" class="${inputclass}" /></td>
			</tr>
			<tr>
				<td colspan="2">
					<div align="center">
						<input type="submit" value="保存" class="${btnclass}" />
						<input type="button" value="返回" class="${btnclass}"
							onclick="window.location='<ls:url address="/admin/tag/query"/>'" />
					</div></td>
			</tr>
		</table>
	</div>
</form:form>
<script type="text/javascript">
	$.validator.setDefaults({});

	$(document).ready(function() {
		jQuery("#form1").validate({
			rules : {
				itemName : {
					required : true
				}
			},
			messages : {
				itemName : {
					required : "请输入名称"
				}
			}
		});
		highlightTableRows("col1");
		//斑马条纹
		$("#col1 tr:nth-child(even)").addClass("even");
	});
</script>


