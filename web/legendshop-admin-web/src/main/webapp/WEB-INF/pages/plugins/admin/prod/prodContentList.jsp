<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%
    Integer offset = (Integer) request.getAttribute("offset");
%>
    <%@ include file="/WEB-INF/pages/common/messages.jsp" %>
    <display:table name="list" requestURI="/admin/product/query" id="item" export="false" class="${tableclass}" style="width:100%;" sort="external">
		<display:column style="width: 80px;" title=" 
				<span>
		           <label class='checkbox-wrapper ' style='float:left;margin-left:10px;'>
						<span class='checkbox-item'>
							<input type='checkbox' id='checkbox' class='checkbox-input selectAll' onclick='selectAll(this);'/>
							<span class='checkbox-inner' style='margin-left:10px;'></span>
						</span>
				   </label>	
				</span>">
            <span>
	           <label class="checkbox-wrapper" style="float:left;margin-left:10px;">
					<span class="checkbox-item">
						<input type="checkbox" id="strArray" class="checkbox-input selectOne" value="${item.prodId}" arg="${item.name}" onclick="selectOne(this);"/>
						<span class="checkbox-inner" style="margin-left:10px;"></span>
					</span>
			   </label>	
			</span>
        </display:column>
        <display:column title="商品编号" style="width:200px;">
            <div class="order_id">
                    ${item.id}
            </div>
        </display:column>
        <display:column title="图片" style="width:100px">
            <a href="${PC_DOMAIN_NAME}/views/${item.prodId}" target="_blank" title="${item.name}"><img width="65" height="65" src="<ls:images item='${item.pic}' scale='3'/>"
            <c:if test="${item.prodType eq 'S'}">
                <p style="background-color: #ff875a;color: #fff;margin-bottom: 5px;margin-top: 5px;padding: 5px;width: 41px;">秒杀</p>
            </c:if>
        </display:column>
        
        <display:column title="商品名称" style="width:450px;">
        	<div class="order_img_name">
        	${item.name}
        	</div>
        </display:column>
        
        <display:column title="价格" style="width:100px">
		            原价：${item.price}<br/>
		            现价：${item.cash}
        </display:column>
        <display:column title="品牌" style="width:100px">
        	${item.brandName}
        </display:column>
<%--         <ls:settings key="VISIT_HW_LOG_ENABLE"> --%>
<%--             <display:column title="查看数" property="views" sortable="true" sortName="views"></display:column> --%>
<%--         </ls:settings> --%>
<%--         <display:column title="订购数" property="buys" sortable="true" sortName="buys"></display:column> --%>
        
        <display:column title="商品状态" style="width:100px">
            <c:choose>
                <c:when test="${item.status==1}">出售中</c:when>
                <c:when test="${item.status==0}">已下线</c:when>
                <c:when test="${item.status==2}">违规下架</c:when>
                <c:when test="${item.status==3}">待审核</c:when>
                <c:when test="${item.status==4}">审核不通过</c:when>
                <c:when test="${item.status==-1}">已删除</c:when>
            </c:choose>
        </display:column>
        
        <display:column title="库存" sortable="true" sortName="stocks" style="width:100px">
            <c:choose>
                <c:when test="${item.stocksArm ne 0 && (item.stocks le item.stocksArm) }">
                    <span style="color: red;font-weight: bold;font-size: 15px;">${item.stocks}</span>
                </c:when>
                <c:otherwise>
                    ${item.stocks}
                </c:otherwise>
            </c:choose>
        </display:column>
        
        <display:column title="店铺" sortable="true"  style="width:200px"><a href="${contextPath}/admin/shopDetail/load/${item.shopId}">${item.siteName}</a></display:column>
        
		<display:column title="订购数" property="buys" sortable="true" sortName="buys" style="width:100px"></display:column>

        <display:column title="权重"  style="width:100px">
                    <c:choose>
                         <c:when test="${item.status==1}">
                             <a href="javascript:void(0)" onclick="updateWeight('${item.prodId}','${item.searchWeight}');">${item.searchWeight}</a>
                         </c:when>
                        <c:otherwise>
                              ${item.searchWeight}
                        </c:otherwise>
                    </c:choose>
        </display:column>
        <c:if test="${type!=2}">
            <display:column title="操作" media="html" style="width:200px;">
                <div class="table-btn-group">
                     <c:choose>
                         <c:when test="${empty type || type==0 || type==1}">
                             <c:if test="${item.status!=2}">
                                 <button onclick="illegalOff(this);" class="tab-btn" siteName="${item.siteName}" item_id="${item.prodId}" itemName="${fn:escapeXml(item.name)}" status="${item.status}">
                                 	下架
                                 </button>
                                 <span class="btn-line">|</span>
                             </c:if>
                             <button class="tab-btn" onclick="realDel('${item.prodId}','${fn:escapeXml(item.name)}');">
                               	   永久删除
                             </button>
                         </c:when>
                         <c:when test="${type==2}">
                             <ls:plugin pluginId="b2c">
                                 <button class="tab-btn" onclick="confirmDelete('${item.prodId}','${fn:escapeXml(item.name)}',this);">
                                     	删除
                                 </button>
                             </ls:plugin>
                         </c:when>
                         <c:when test="${type==3}">
                             <button class="tab-btn" onclick="auditProd('${item.prodId}',this);" itemName="${fn:escapeXml(item.name)}">
                                 	 审核
                             </button>
                         </c:when>
                         <c:when test="${type eq -1}">
                             <button class="tab-btn" onclick="realDel('${item.prodId}','${fn:escapeXml(item.name)}');">
                                 	永久删除
                             </button>
                         </c:when>
                         <c:when test="${type eq -2}">
                             <button class="tab-btn" onclick="realDel('${item.prodId}','${fn:escapeXml(item.name)}');">
                                 	永久删除
                             </button>
                         </c:when>
                     </c:choose>
                </div>
            </display:column>
        </c:if>
    </display:table>
     <div class="clearfix">
       		<div class="fl">
	       		<input type="button" value="永久刪除" onclick="return deleteAction();" class="batch-btn">
                <c:if test="${type != -2 && type != 2 && type != -1 &&type != 3}">
	       		    <input type="button" value="批量下架" onclick="return batchOffline();" class="batch-btn">
                </c:if>
			</div>
       		<div class="fr">
       			 <div class="page">
	       			 <div class="p-wrap">
	           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	    			 </div>
       			 </div>
       		</div>
	 </div>
