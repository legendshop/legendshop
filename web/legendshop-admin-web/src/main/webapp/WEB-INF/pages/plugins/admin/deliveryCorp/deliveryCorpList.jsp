<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>物流公司管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">配送管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">物流公司管理</span></th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/deliveryCorp/query"
				id="form1" method="get">
				<div class="criteria-div">
					<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" /> 
					<span class="item">
						物流公司名称：<input type="text" name="name" maxlength="50" value="${deliveryCorp.name}" class="${inputclass}" placeholder="物流公司名称"/> 
						<input type="button" onclick="search()" value="搜索" class="${btnclass}" /> 
						<input type="button" value="创建物流公司" class="${btnclass}" onclick='window.location="<ls:url address='/admin/deliveryCorp/load'/>"' />
					</span>
				</div>
			</form:form>
			<div align="center" class="order-content-list" style="margin-bottom: 60px;">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="list"
					requestURI="<ls:url address='/admin/deliveryCorp/query'/>"
					id="item" export="false" class="${tableclass}" style="width:100%">
					<display:column title="顺序" class="orderwidth">${item_rowNum}</display:column>
					<display:column title="公司名称" property="name"></display:column>
					<display:column title="公司网址" property="companyHomeUrl"></display:column>
					<display:column title="公司编码" property="companyCode"></display:column>
					<display:column title="操作" media="html" style="width: 150px;">
						<div class="table-btn-group">
							<button class="tab-btn" onclick="window.location='<ls:url address='/admin/deliveryCorp/load/${item.dvyId}'/>'">
								编辑
							</button>
							<span class="btn-line">|</span>
							<button class="tab-btn" onclick="deleteById('${item.dvyId}');">
								删除
							</button>
						</div>
					</display:column>
				</display:table>
				<div class="clearfix">
		       		<div class="fr">
		       			 <div class="page">
			       			 <div class="p-wrap">
			           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			    			 </div>
		       			 </div>
		       		</div>
	       		</div>
			</div>
		</div>
	</div>
</body>
<script language="JavaScript" type="text/javascript">
	function deleteById(id) {
		
		layer.confirm("确定删除,会影响配送和打印 ?", {
			 icon: 3
		     ,btn: ['确定','关闭'] //按钮
		   }, function(){
			   window.location = "<ls:url address='/admin/deliveryCorp/delete/" + id + "'/>";
		   });
	}
	highlightTableRows("item");

	function search() {
		$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
		$("#form1")[0].submit();
	}
</script>
</html>


