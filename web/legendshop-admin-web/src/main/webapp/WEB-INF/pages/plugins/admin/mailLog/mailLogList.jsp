<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<%
			Integer offset = (Integer)request.getAttribute("offset");
%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	    <table class="${tableclass} title-border" style="width: 100%">
		    	<tr><th class="title-border">系统报表&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">邮件发送历史</span></th></tr>
		</table>
    <form:form  action="${contextPath}/admin/system/mailLog/query" id="form1" method="get">
   	    <div class="criteria-div">
       	    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
       	    <span class="item">
            	用户邮箱：<input type="text" name="receiveMail" maxlength="50" value="${mailLog.receiveMail}" class="${inputclass}" placeholder="用户邮箱"/>
            	<input type="button" onclick="search()" value="搜索" class="${btnclass}" />
            </span>
	      </div>
    </form:form>
    <div align="center" class="order-content-list">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		<display:table name="list" requestURI="/admin/system/mailLog/query" id="item" export="false" sort="external" class="${tableclass}" style="width:100%">
	    	<display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
     		<%-- <display:column title="用户名称" property="receiveName"></display:column> --%>
     		<display:column title="邮箱" property="receiveMail"></display:column>
     		<display:column title="邮箱ID" property="mailId"></display:column>
     		<display:column title="发送时间"><fmt:formatDate value="${item.sendTime}" type="both" dateStyle="long" pattern="yyyy-MM-dd HH:mm:ss" /></display:column>
     		<display:column title="发送状态" >
     			<c:if test="true">
     				<span>成功</span>
     			</c:if>
     			<c:if test="false">
	     			<span>失败</span>
     			</c:if>
     		</display:column>
	    </display:table>
        <div class="clearfix" style="margin-bottom: 60px;">
       		<div class="fr">
       			 <div class="page">
	       			 <div class="p-wrap">
	           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
	    			 </div>
       			 </div>
       		</div>
       	</div>
    </div>
    </div>
    </div>
    </body>
        <script type="text/javascript">
			        function pager(curPageNO){
			            document.getElementById("curPageNO").value=curPageNO;
			            document.getElementById("form1").submit();
			        }
			        function search(){
					  	$("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
					  	$("#form1")[0].submit();
					}
			//-->
		</script>

</html>
