<!Doctype html>
<html class="no-js fixed-layout">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="../back-common.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>退款详情 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <style>
  	#col1 td{
  		height:40px;
  	}
  </style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
			<table class="${tableclass} no-border" style="width: 100%;">
			    <thead>
			    	<tr>
			    		<th class="no-bg title-th">
			            	<span class="title-span">
								售后管理  ＞
								<a href="<ls:url address="/admin/returnMoney/query/pending"/>">单退款管理</a>
								  ＞  <span style="color:#0e90d2;">查看详情</span>
							</span>
			            </th>
			    	</tr>
			    </thead>
			</table>
			<form:form id="form1" action="${contextPath}/admin/returnMoney/audit" method="POST" >
				<input type="hidden" name="refundId" id="refundId" value="${subRefundReturn.refundId}"/>
			   		 <table style="width: 95%;margin-left: 30px;" class="${tableclass} no-border content-table" id="col1">
			   		 	<!-- 买家退款申请 -->
			   		 	<tr>
							<td width="200px" style="text-align: center;margin-right: 50px;">
								<div align="right" style="text-align: left; font-weight: bold;">
									<c:if test="${subRefundReturn.refundSouce eq 0 }">
										买家退款申请：
									</c:if>
									<c:if test="${subRefundReturn.refundSouce eq 1 }">
										商家主动退款申请：
									</c:if>
								</div>
							</td>
							<td></td>
						</tr>
			            <tr>
					       <td>
					          <div align="right">退款编号：</div>
					       </td>
					       <td align="left" style="padding-left: 2px;">${subRefundReturn.refundSn }</td>
				        </tr>
						<tr>
					        <td>
					          <div align="right">用户名：</div>
					       </td>
					       <td align="left" style="padding-left: 2px;">
					           ${subRefundReturn.userName}
					       </td>
					    </tr>
			            <tr>
					       <td>
					          <div align="right">申请时间：</div>
					       </td>
					       <td align="left" style="padding-left: 2px;"><fmt:formatDate value="${subRefundReturn.applyTime }" pattern="yyyy-MM-dd HH:mm" /></td>
				      </tr>
				      <tr>
					       <td>
					          <div align="right">订单编号：</div>
					       </td>
					       <td align="left" style="padding-left: 2px;">${subRefundReturn.subNumber }</td>
				    </tr>
				      <tr>
					       <td>
					          <div align="right">店铺名称：</div>
					       </td>
					       <td align="left" style="padding-left: 2px;">${subRefundReturn.shopName }</td>
				    </tr>
				    <tr>
				       <td>
				          <div align="right">涉及商品：</div>
				       </td>
				       <td align="left" style="padding-left: 2px;">
				       		<c:if test="${subRefundReturn.subItemId eq 0 }">
					       		${subRefundReturn.productName }
							</c:if>       
				       		<c:if test="${subRefundReturn.subItemId ne 0 }">
					       		<a href="${PC_DOMAIN_NAME}/views/${subRefundReturn.productId}">${subRefundReturn.productName }</a>
							</c:if>       
				       </td>
				    </tr>
			       <tr>
				        <td>
				          <div align="right">退款金额：</div>
				        </td>
				        <td align="left" style="padding-left: 2px;">
				        	<c:choose>
			              		<c:when test="${subRefundReturn.isPresell && subRefundReturn.isRefundDeposit}">
			              			<c:if test="${empty subRefundReturn.refundAmount }">
			              				&#65509;${subRefundReturn.depositRefund}元
			              			</c:if>
			              			<c:if test="${not empty subRefundReturn.refundAmount }">
										&#65509;${subRefundReturn.depositRefund + subRefundReturn.refundAmount}元 (${info})
			              			</c:if>
			              		</c:when>
			              		<c:when test="${subRefundReturn.isPresell && !subRefundReturn.isRefundDeposit}">
			              			&#65509;${subRefundReturn.refundAmount}元
			              		</c:when>
			              		<c:otherwise>
			              			&#65509;${subRefundReturn.refundAmount}元
			              		</c:otherwise>
			              	</c:choose>
				        </td>
			      </tr>
			       <tr>
				        <td>
				          <div align="right">退款原因：</div>
				        </td>
				        <td align="left" style="padding-left: 2px;">${subRefundReturn.buyerMessage }</td>
			      </tr>
			       <tr>
				        <td>
				          <div align="right">退款说明：</div>
				        </td>
				        <td align="left" style="padding-left: 2px;">${subRefundReturn.reasonInfo }</td>
			      </tr>
			      <c:if test="${not empty subRefundReturn.photoFile1 }">
			       <tr>
				        <td valign="top">
				          <div align="right">凭证上传1：</div>
				        </td>
				        <td align="left" style="padding-left: 2px;" valign="top"><a target="_blank" href="<ls:photo item='${subRefundReturn.photoFile1}'/>">
				        	<img src="<ls:photo item='${subRefundReturn.photoFile1 }'/>"   style="max-height: 100px;max-width: 150px;"/>
				        	</a>
				        </td>
			      </tr>
			      </c:if>
			      <c:if test="${not empty subRefundReturn.photoFile2 }">
				       <tr>
					        <td valign="top">
					          <div align="right">凭证上传2：</div>
					        </td>
					        <td align="left" style="padding-left: 2px;" valign="top"><a target="_blank" href="<ls:photo item='${subRefundReturn.photoFile2}'/>">
					        	<img src="<ls:photo item='${subRefundReturn.photoFile2 }'/>"  style="max-height: 100px;max-width: 150px;"/>
					        	</a>
					        </td>
				      </tr>
				  </c:if>
				  <c:if test="${not empty subRefundReturn.photoFile3 }">
				       <tr>
					        <td valign="top">
					          <div align="right">凭证上传3：</div>
					        </td>
					        <td align="left" style="padding-left: 2px;" valign="top"><a target="_blank" href="<ls:photo item='${subRefundReturn.photoFile3}'/>">
					        	<img src="<ls:photo item='${subRefundReturn.photoFile3 }'/>"  style="max-height: 100px;max-width: 150px;"/>
					        	</a>
					        </td>
				      </tr>
			      </c:if>
			      <!-- 买家退款申请 -->
			      
			      <!-- 商家处理信息 -->
			      <tr>
			        <td colspan="2">
			          <div align="right" style="text-align: left; font-weight: bold;">商家处理信息：</div>
			       </td>
			      </tr>
			      <tr>
			        <td>
			          <div align="right">审核状态：</div>
			        </td>
			        <td align="left" style="padding-left: 2px;">
				        <c:if test="${subRefundReturn.sellerState eq 1}">
				        	待审核
				        </c:if>
				        <c:if test="${subRefundReturn.sellerState eq 2}">
				        	同意
				        </c:if>
				        <c:if test="${subRefundReturn.sellerState eq 3}">
				        	不同意
				        </c:if>
			        </td>
			      </tr>
			      <tr>
			        <td>
			          <div align="right">商家备注： </div>
			        </td>
			        <td align="left" style="padding-left: 2px;">${subRefundReturn.sellerMessage }</td>
			      </tr>
			      <!-- 商家处理信息 -->
			      
			      <tr>
			        <td>
			          <div align="right">订单总额：</div>
			        </td>
			        <td align="left" style="padding-left: 2px;">￥${subRefundReturn.subMoney }</td>
			      </tr>
			      
			      <!-- 订单支付信息 -->
			      
			      <!-- 平台退款审核 -->
			           
				      <tr>
				        <td colspan="2">
				          <div align="right" style="text-align: left; font-weight: bold;">平台退款处理：</div>
				       </td>
				      </tr>
				      
				       <%@include file="refundPay.jsp" %>
				      
				      <tr>
				        <td valign="top">
				          <div align="right">备注信息：</div>
				        </td>
				        <td align="left" style="padding-left: 2px;"> 
				        	<textarea maxlength="200" rows="5" cols="50" name="adminMessage" id="adminMessage" >${subRefundReturn.adminMessage}</textarea></br>
				        	<span style="font-size:12px;color:#aaa;">系统默认退款到“用户预存款”，如果包含“在线支付”的款项,将自动原路还回，建议在备注里说明，方便核对。</span>
				        </td>
				      </tr>
			      <!-- 平台退款审核 -->
			      
			      <tr>
				        <td colspan="2">
				          <div align="right" style="text-align: left; font-weight: bold;">退款结果：</div>
				       </td>
				   </tr>
				   
				   <tr>
				        <td valign="top">
				          <div align="right">退款来源：</div>
				        </td>
				        <td align="left" style="padding-left: 2px;"> 
							<c:choose>
								<c:when test="${subRefundReturn.refundSouce eq 0}">
									用户申请退款
								</c:when>
								<c:when test="${subRefundReturn.refundSouce eq 1}">
									商家申请退款
								</c:when>
								<c:when test="${subRefundReturn.refundSouce eq 2}">
									平台申请退款
								</c:when>
							</c:choose>				        
				        </td>
			      </tr>
			      
			      <c:if test="${subRefundReturn.isHandleSuccess != 0}">
					   <tr>
					        <td valign="top">
					          <div align="right">退款状态：</div>
					        </td>
					        <td align="left" style="padding-left: 2px;"> 
					        	<table class="am-table am-table-striped am-table-hover see-able" >
									<thead>
										<tr>
											<th class="w150 tl">退款类型</th>
											<th class="w150 tl">退款支付途径</th>
											<th class="w150 tl">金额</th>
											<th class="w150 tl">退款状态</th>
										</tr>
									</thead>
									<tbody id="ruleProds">
										<c:forEach items="${subRefundReturnDetailList}" var="subRefundReturnDetai" varStatus="status">
											<tr>
												<td>${subRefundReturnDetai.refundType }</td>
												<td>${subRefundReturnDetai.payType }</td>
												<td>${subRefundReturnDetai.refundAmount }</td>
												<td>
													<c:if test="${subRefundReturnDetai.refundStatus eq true}">
														成功
													</c:if>
													<c:if test="${subRefundReturnDetai.refundStatus eq false}">
														失败
													</c:if>
												</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
					        </td>
				      	</tr>
			      	</c:if>
			      
			      
			      <tr>
			      	  <td></td>
			          <td align="left" style="padding-left: 2px;">
			              <div>
			              		<c:if test="${subRefundReturn.sellerState eq 2 && subRefundReturn.isHandleSuccess eq 0 }">
						          <input type="button" value='<c:if test="${subRefundReturn.goodsState eq 4 || subRefundReturn.goodsState eq 0}">同意退款并保存</c:if>
						          <c:if test="${subRefundReturn.goodsState ne 4 && subRefundReturn.goodsState ne 0}">强制退款并保存</c:if>' id="returnBtn" class="${btnclass}"/>
								</c:if>
				              	<%-- <c:if test="${subRefundReturn.applyState eq 2}">
				              		<input type="button" value="提交处理" class="${btnclass}" onclick="auditRefund();"/>
				              	</c:if> --%>
				                <input type="button" value="返回" class="${btnclass}" onclick="window.location='${contextPath}/admin/returnMoney/query/${status}'" />
			              </div>
			          </td>
			      </tr>
			  </table>
			</form:form>
		</div>
	</div>
</body>
<script type="text/javascript" src="<ls:templateResource item='/resources/templets/amaze/js/refund/refundDetail.js'/>"></script>
<script type="text/javascript">
var refundMSG = '${refundMSG}';
var isHandleSuccess="${subRefundReturn.isHandleSuccess}";
</script>
</html>
