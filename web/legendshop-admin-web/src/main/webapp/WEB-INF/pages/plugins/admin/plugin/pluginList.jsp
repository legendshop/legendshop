<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
	Integer offset = 1;
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>${sessionScope.SPRING_SECURITY_LAST_USERNAME}- 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">系统配置&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">插件列表</span></th>
				</tr>
			</table>

			<div align="center" class="order-content-list" style="margin-top: 15px; margin-bottom: 60px;">
				<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
				<display:table name="pluginList"
					requestURI="/admin/system/plugin/query" id="item" export="false"
					class="${tableclass}" style="width:100%">
					<display:column title="顺序" class="orderwidth">&nbsp;<%=offset++%>&nbsp;&nbsp;&nbsp;</display:column>
					<display:column title="名称" property="pluginConfig.pulginId"></display:column>
					<display:column title="作者" property="pluginConfig.provider"></display:column>
					<display:column title="版本" property="pluginConfig.pulginVersion"></display:column>
					<display:column title="是否必须" property="pluginConfig.required"></display:column>
					<display:column title="描述" property="pluginConfig.description"></display:column>
					<%--
					<display:column title="操作" style="width: 80px">
						<div class="table-btn-group">
							<div class="tab-btn">
								<c:choose>
									<c:when test="${item.pluginConfig.status == 'Y'}">
										<button class="tab-btn" name="statusImg" item_id="${item.pluginConfig.pulginId}"
											itemName="${item.pluginConfig.pulginId}"
											status="${item.pluginConfig.status}">
											下线
										</button>
									</c:when>
									<c:otherwise>
										<button class="tab-btn" name="statusImg" item_id="${item.pluginConfig.pulginId}"
											itemName="${item.pluginConfig.pulginId}"
											status="${item.pluginConfig.status}">
											上线
										</button>
									</c:otherwise>
								</c:choose>
							</div>
						</div>
					</display:column>
					 --%>
				</display:table>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	$(document).ready(
			function() {
				$("button[name='statusImg']").click(
						function(event) {
							var item_id = $(this).attr("item_id");
							var status = $(this).attr("status");
							var desc;
							var toStatus;
							if (status == 'Y') {
								toStatus = 'turnoff';
								desc = $(this).attr("itemName") + '插件下线?';
							} else {
								toStatus = 'turnon';
								desc = $(this).attr("itemName") + '插件上线?';
							}

							layer.confirm("确定将"+desc, {
								icon : 3,
								btn : [ '确定', '关闭' ]
							//按钮
							}, function() {
								jQuery.ajax({
									url : "${contextPath}/admin/system/plugin/"
											+ toStatus + "/" + item_id,
									type : 'get',
									dataType : 'json',
									async : true, //默认为true 异步   
									error : function() {
										layer.alert('系统异常',{icon: 2});
									},
									success : function(data) {
										window.location.reload();
									}
								});
							});

						});

			});
</script>
</html>
