<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<html>
<head>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<style type="text/css">
.criteria-btn {
    color: #ffffff;
    border: 1px solid #0e90d2;
    background-color: #0e90d2;
    border-color: #0e90d2;
    cursor: pointer;
    height: 28px;
    padding-left: 12px;
    padding-right: 12px;
    text-align: center;
    line-height: 24px;
    margin: 0 3px;
    font-size: 12px;
}
</style>
</head>
<body>
		<div id="menuContentList" style="margin:10px 0px 0 10px;height:320px;overflow-y:auto;" > 
	   		<ul id="catTrees" class="ztree"></ul>
     	</div>
		<div align="center">
			<input style="margin:10px;" type="button" class="criteria-btn" onclick="javascript:saveFunctionMenu();" value="保存" id="Button1" name="Button1">
		</div>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>"></script>
<link type="text/css" rel="stylesheet"  href="<ls:templateResource item='/resources/plugins/ztree/zTreeStyle.css'/>" >
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.core-3.5.min.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/plugins/ztree/jquery.ztree.excheck-3.5.js'/>"></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/loadCategory.js'/>"></script>
</body>
<script type="text/javascript">
	var data = '${catTrees}';
</script>
</html>

