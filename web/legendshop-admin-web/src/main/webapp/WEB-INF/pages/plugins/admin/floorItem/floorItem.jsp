<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<html>
    <head>
        <title>创建</title>
         <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
</head>
    <body>
        <form:form  action="${contextPath}/admin/floorItem/save" method="post" id="form1">
            <input id="fiId" name="fiId" value="${floorItem.fiId}" type="hidden">
            <div align="center">
            <table border="0" align="center" class="${tableclass}" id="col1">
                <thead>
                    <tr class="sortable">
                        <th colspan="2">
                            <div align="center">
                                	创建
                            </div>
                        </th>
                    </tr>
                </thead>
                
		<tr>
		        <td>
		          	<div align="center">记录时间: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="recDate" id="recDate" value="${floorItem.recDate}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">父节点: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="floorId" id="floorId" value="${floorItem.floorId}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="referId" id="referId" value="${floorItem.referId}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">类型ID: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="type" id="type" value="${floorItem.type}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">商品分类Level: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="sortLevel" id="sortLevel" value="${floorItem.sortLevel}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">顺序: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="seq" id="seq" value="${floorItem.seq}" />
		        </td>
		</tr>
                <tr>
                    <td colspan="2">
                        <div align="center">
                            <input type="submit" value="保存" />
                            <input type="button" value="返回"
                                onclick="window.location='<ls:url address="/admin/floorItem/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
        <script language="javascript">
		    $.validator.setDefaults({
		    });

    $(document).ready(function() {
    jQuery("#form1").validate({
            rules: {
            banner: {
                required: true,
                minlength: 5
            },
            url: "required"
        },
        messages: {
            banner: {
                required: "Please enter banner",
                minlength: "banner must consist of at least 5 characters"
            },
            url: {
                required: "Please provide a password"
            }
        }
    });
 
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
});
</script>
    </body>
</html>

