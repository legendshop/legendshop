<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ page import="java.util.Date"%>
<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
<!-- 获取当前日期 -->
<c:set var="nowDate">
	<fmt:formatDate value="<%=new Date()%>" pattern="yyyy-MM-dd HH:mm:ss"
		type="date" />
</c:set>
	<display:table name="list" requestURI="/admin/coupon/query"
		id="item" export="false" sort="external" class="${tableclass}"
		style="width:100%">
		<display:column title="序号">${item_rowNum }</display:column>
		<display:column title="优惠券名称" property="couponName"
			style="width:100px"></display:column>
		<display:column title="店铺">
			<a href="<ls:url address="/admin/shopDetail/load/${item.shopId}"/>" target="_blank">${item.shopName}</a>
		</display:column>
		<display:column title="面额" property="offPrice"></display:column>
		<display:column title="消费金额" property="fullPrice"></display:column>
		<display:column title="开始时间" sortable="true" sortName="start_date">
			<fmt:formatDate value="${item.startDate}" type="both"
				dateStyle="long" pattern="yyyy-MM-dd HH:mm" />
		</display:column>
		<display:column title="结束时间" sortable="true" sortName="end_date">
			<fmt:formatDate value="${item.endDate}" type="both"
				dateStyle="long" pattern="yyyy-MM-dd HH:mm" />
		</display:column>
		<display:column title="优惠劵类型">
			<c:choose>
				<c:when test="${item.couponType == 'common'}">店铺通用券</c:when>
				<c:when test="${item.couponType == 'product'}">指定商品劵</c:when>
			</c:choose>
		</display:column>
		<display:column title="领取方式">
			<c:choose>
				<c:when test="${item.getType == 'free'}">免费领取</c:when>
				<c:when test="${item.getType == 'points'}">积分兑换</c:when>
				<c:when test="${item.getType == 'pwd'}">卡密兑换</c:when>
			</c:choose>
		</display:column>
		<display:column title="状态">
			<c:choose>
				<c:when test="${nowDate<=item.endDate}">
					<c:choose>
						<c:when test="${1==item.status}">
							<font>有效</font>
						</c:when>
						<c:when test="${0==item.status}">失效</c:when>
					</c:choose>
				</c:when>
				<c:otherwise>
					<font>过期</font>
				</c:otherwise>
			</c:choose>
		</display:column>
		<display:column title="操作" media="html" style="width:125px;">
				<div class="table-btn-group">
					<c:if test="${nowDate<=item.endDate}">
						<c:choose>
							<c:when test="${item.status == 1}">
								<a class="tab-btn" href="javascript:void(0);" onclick="statusImg(this)" name="statusImg" itemId="${item.couponId}" itemName="${item.couponName}" status="${item.status}">
									下线
								</a>
								<span class="btn-line">|</span>
							</c:when>
							<%-- <c:when test="${item.status==0}">
								<button
									class="am-btn am-btn-default am-btn-xs am-hide-sm-only"
									name="statusImg" itemId="${item.couponId}"
									itemName="${item.couponName}" status="${item.status}"
									style="color: #0e90d2;">
									<span class="am-icon-arrow-up"></span>上线
								</button>
							</c:when> --%>
						</c:choose>
					</c:if>
					<div class="am-dropdown" data-am-dropdown>
						<button class="tab-btn am-dropdown-toggle" data-am-dropdown-toggle>
							<span class="am-icon-cog"></span> <span
								class="am-icon-caret-down"></span>
						</button>
						<ul class="am-dropdown-content">
							<li>
								<a href="<ls:url address='/admin/coupon/watchCoupon/${item.couponId}'/>" title="查看使用情况">查看</a>
							</li>
							<li>
								<a href='javascript:confirmDelete("${item.couponId}","${fn:escapeXml(fn:replace(item.couponName,"'","&acute;"))}")' title="删除">删除</a>
							</li>
						</ul>
					</div>
				</div>

		</display:column>
	</display:table>
	<div class="clearfix">
   		<div class="fr">
   			 <div cla ss="page">
    			 <div class="p-wrap">
        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
			 	</div>
  			 </div>
  		</div>
   	</div>
	<script src="${contextPath}/resources/templets/amaze/js/amazeui.min.js"></script>