<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
	<%
			Integer offset=((Integer)request.getAttribute("offset"));
	%>	
	<input type="hidden" id="curPageNO"  name="curPageNO" value="<%=request.getAttribute("curPageNO")%>">
       <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
      <display:table name="list" requestURI="${contextPath}/admin/member/right/query" id="item" export="false" class="${tableclass}" style="width:100%">
      <display:column title="顺序" style="width:50px"><%=offset++%></display:column>
      <display:column title="名称 "  style="width:150px"><div title="${item.note}">${item.name}</div></display:column>
      <display:column title="权限名称 " property="protectFunction" style="width:250px"></display:column>
      <display:column title="Label " property="note"></display:column>
      <display:column title="菜单" property="menuId"></display:column>
      <display:column title="用户/角色"  style="width:90px;">
     		 <a href="${contextPath}/admin/member/right/loadUserByFunc?funcId=${item.id}">用户</a> /
      		<a href="${contextPath}/admin/member/right/roles/${item.id}">角色</a>
      </display:column>
	      <display:column title="操作" media="html" style="width:145px">
				<div class="table-btn-group">
					<button class="tab-btn" onclick="updateFuncDialog('${item.id}')">修改</button>
					<span class="btn-line">|</span>
					<button class="tab-btn" onclick="deleteById('${item.id}')" >删除</button>
				</div>
	      </display:column>
    </display:table>
    <div class="clearfix" style="margin-bottom: 60px;">
   		<div class="fr">
   			 <div class="page">
    			 <div class="p-wrap">
        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
 			 	</div>
   			 </div>
   		</div>
   	</div>
<%--         <c:if test="${not empty toolBar}"> --%>
<%--             <c:out value="${toolBar}" escapeXml="${toolBar}"></c:out> --%>
<%--         </c:if> --%>

