<!doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <style>
  .am-table {
    width: 100%;
    border-spacing: 0;
    border-collapse: collapse;
    font-size: 12px;
    min-width: 900px;
    border: none !important;
    border-top: 0;
    text-align: center;
  }
  .am-table > thead > tr > td, .am-table > tbody > tr > td, .am-table > tfoot > tr > td {
    padding: 8px 10px;
    line-height: 23px;
    border-bottom: none !important; 
    min-width: 80px;
    max-width: 240px;
    height: 60px;
}
  </style
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
		<table class="${tableclass} title-border" style="width: 100%;order-bottom: 1px solid #dddddd; ">
			<tr><th class="title-border">协议管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">${title}</span></th></tr>
		</table>
		<input id="fileType" name="fileType" value="${fileType}" type="hidden">
		<div align="center" style="margin-top: 10px;" class="order-content-list">
			<table align="center" class="${tableclass}" id="col1"
				style="width:100%">
				<tr style="border: none !important;">
					<td>
						<textarea style="width:100%; height: 400px" id="content" name="content">${content }</textarea>
						</td>
				</tr>
				<tr>
					<td colspan="2" style="border: none !important;">
						<div align="center">
								<input type="submit" value="保存" class="${btnclass}" onclick="javascript:saveFileContent()"/>
						</div></td>
				</tr>
			</table>
		</div>
		</div>
		</div>
		</body>
		
		<script language="javascript">
	function saveFileContent(){
		    var data = {
	   				 "content": $("#content").val(),
                	"fileType": $("#fileType").val()
                };
    			$.ajax({
				url:"${contextPath}/admin/system/file/save" , 
				data: data,
				type:'post', 
				dataType : 'json', 
				async : true, //默认为true 异步   
				error: function(jqXHR, textStatus, errorThrown) {
			 		// alert(textStatus, errorThrown);
				},
				success:function(result){
				   if(result != 0){
				   	alert("保存文件失败，失败代码是 " + result);
				   }else{
				   	alert("保存文件成功");
				   }
				}
				});
	}
</script>
</html>
