<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>权限管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border"><c:if test="${type eq 'admin'}">管理员管理</c:if><c:if test="${type eq 'user'}">用户管理</c:if>
						&nbsp;＞&nbsp;
						<a href="<ls:url address="/admin/system/userDetail/query"/>"><c:if test="${type eq 'admin'}">管理员信息管理</c:if><c:if test="${type eq 'user'}">用户信息管理</c:if></a>
              				&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">修改用户密码</span>
					</th>
				</tr>
			</table>
			<form:form action="${contextPath}/admin/member/user/updatePassword"
				id="form1" method="post">
				<input type="hidden" id="appNo" name="appNo" value="${param.appNo}">
				<input type="hidden" name="id" value="${bean.id }">
				<div align="center" style="margin-top: 10px;">
					<table align="center" class="${tableclass} no-border content-table" id="col1">
						<tr>
							<td width="200px;">
								<div align="right">
									<font color="ff0000">*</font>名称 ：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;">
								<input type="text" style="width: 150px;" name="name" id="name" value="${bean.name}" maxlength="30" <c:if test="${not empty bean}"><c:out value="readonly=\"true\""></c:out> </c:if> />
							</td>
						</tr>
						<tr>
							<td >
								<div align="right">
									<font color="ff0000">*</font>密码 ：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="password" style="width: 150px;" name="password" id="password"
								value="" maxlength="20" /></td>
						</tr>
						<tr>
							<td >
								<div align="right">
									<font color="ff0000">*</font>确认密码 ：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="password" style="width: 150px;" name="passwordag" id="passwordag"
								value="" maxlength="20" /></td>
						</tr>
						<tr>
							<td></td>
							<td align="left" style="padding-left: 2px;">
								<div>
									<input class="${btnclass}" type="submit" value="保存" /> 
								    <input class="${btnclass}" type="button" value="返回"
										onclick="window.history.go(-1)" />
								</div>
							</td>
						</tr>
					</table>
				</div>

			</form:form>
		</div>
	</div>
</body>
<script src="${contextPath}/resources/common/js/jquery.validate.js"
	type="text/javascript"></script>
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/common/css/errorform.css" />
<script language="javascript">
	jQuery.validator.addMethod("checkPassword", function(value, element) {
		var length = value.length;
		/* var regx = /(?!.*[\u4E00-\u9FA5\s])(?!^[a-zA-Z]+$)(?!^[\d]+$)(?!^[^a-zA-Z\d]+$)^.{6,20}$/; */
	    var regx= /(?!^[0-9]+$)(?!^[A-z]+$)(?!^[~\\`!@#$%\\^&*\\(\\)-_+={}|\\[\\];':\\\",\\.\\\\\/\\?]+$)(?!^[^A-z0-9]+$)^.{6,20}$/;
		var regx1= /^[0-9A-z`~!@#$%^&*()_\-+=<>?:"{}|,.\/;'\\[\]·~！@#￥%……&*（）——\-+={}|《》？：“”【】、；‘’，。、]+$/g;
	    return this.optional(element) || (length >= 6 && length<=20 && regx1.test(value) && regx.test(value) && value.indexOf(" ") < 0);
	}, '密码由6-20位字母、数字或符号(除空格)的两种及以上组成');

	$(document).ready(function() {
		$("#form1").validate({
			rules : {
				password : {
					required : true,
					minlength : 6,
					maxlength : 20,
					checkPassword : true
				},
				passwordag : {
					required : true,
					minlength : 6,
					maxlength : 20,
					equalTo : "#password"
				}
			},
			messages : {
				password : {
					required : "请输入密码",
					minlength : "最少为6位长度",
					maxlength : "最长为20位长度"
				},
				passwordag : {
					required : "请重新输入密码",
					minlength : "最少为6位长度",
					maxlength : "最长为20位长度",
					equalTo : "两次输入的密码必须要一致"
				}
			}
		});
	});
</script>
</html>
