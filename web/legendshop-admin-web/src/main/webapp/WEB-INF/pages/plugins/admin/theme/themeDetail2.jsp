<!DOCTYPE html>
<%@ page pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<html>
<head>
<title>精选</title>
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/common/red/css/themeModule.css'/>" />	
</head>
<body>
	<c:if test="${not empty theme.customContent }">
				<div class="ThemeModuleProd_custom">
					<div class="custom_con"><c:out value="${theme.customContent}" escapeXml="${theme.customContent}"></c:out> </div>
				</div>
	</c:if>
</body>
</html>