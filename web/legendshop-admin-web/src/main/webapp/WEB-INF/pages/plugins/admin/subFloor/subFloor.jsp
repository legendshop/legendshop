<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>首页管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/errorform.css" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass}" style="width: 100%">
				<thead>
					<tr>
						<th class="no-bg title-th">
			            	<span class="title-span">
								商城管理  ＞  
								<span style="color:#0e90d2;">子楼层编辑</span>
							</span>
			            </th>
					</tr>
				</thead>
			</table>
			<form:form action="${contextPath}/admin/subFloor/save" method="post"
				id="form1">
				<input id="sfId" name="sfId" value="${subFloor.sfId}" type="hidden">
				<input id="floorId" name="floorId" value="${flId}" type="hidden">
				<div align="center">
					<table border="0" align="center" class="${tableclass} no-border content-table" id="col1"
						style="width: 100%">
						<tr>
							<td style="width:200px;">
								<div align="right">
									<font color="ff0000">*</font>二级楼层名称：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" name="name" id="name" maxlength="49"
								value="${subFloor.name}" class="${inputclass}" /></td>
						</tr>
						<tr>
							<td>
								<div align="right">状态：</div>
							</td>
							<td align="left" style="padding-left: 2px;"><select id="status" name="status">
									<ls:optionGroup type="select" required="true" cache="true"
										beanName="ONOFF_STATUS" selectedValue="${subFloor.status}" />
							</select></td>
						</tr>
						<tr>
							<td>
								<div align="right">
									<font color="ff0000">*</font>顺序：
								</div>
							</td>
							<td align="left" style="padding-left: 2px;"><input type="text" name="seq" id="seq"
								value="${subFloor.seq}" style="width: 50px"
								class="${inputclass}" onkeyup="this.value=this.value.replace(/\D/g,'')"
								onafterpaste="this.value=this.value.replace(/\D/g,'')"
								maxlength="5"/></td>
						</tr>
						<tr>
							<td></td>
							<td align="left" style="padding-left: 2px;">
								<div>
									<input type="submit" value="保存" class="${btnclass}" /> 
								    <input type="button" value="返回" class="${btnclass}"
										onclick="window.location='<ls:url address="/admin/floor/query"/>'" />
								</div>
							</td>
						</tr>
					</table>
					<table style="width: 100%; border: none;" class="${tableclass }">
						<tr>
							<td align="left" style="text-align:left;border: none;">说明：<br> 1、
								混合模式下的子楼层，最多支持显示5个子楼层，多于5个的子楼层不作显示。<br>
							</td>
						<tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>

<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script language="javascript">
	$.validator.setDefaults({
	});

	$(document).ready(function() {
		jQuery("#form1").validate({
			rules : {
				name : "required",
				seq : {
					required : true,
					number : true
				}
			},
			messages : {
				name : "请输入标题",
				seq : {
					required : "不能为空",
					number : "请输入数字"
				}
			}
		});

		//斑马条纹
		$("#col1 tr:nth-child(even)").addClass("even");
	});
</script>
</html>
