<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<html>
<head>
    <script src="<ls:templateResource item='/resources/common/js/alternative.js'/>" type="text/javascript"></script>
    <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
    <title>列表</title>
</head>
<body>
    <form:form  action="${contextPath}/admin/system/floorItem/query" id="form1" method="post">
        <table class="${tableclass}" style="width: 100%">
		    <thead>
		    	<tr>
			    	<th height="40">
				    	<a href="<ls:url address='/admin/system/index'/>" target="_parent" class="blackcolor">首页</a> &raquo; 
				    	<a href="<ls:url address='/admin/floorItem/query'/>">楼层管理</a>
			    	</th>
		    	</tr>
		    </thead>
		    <tbody><tr><td>
		    	    <div align="left" style="padding: 3px">
				       	    <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
				            	&nbsp;商城名称
				            <input type="text" name="userName" maxlength="50" value="${floorItem.userName}" />
				            <input type="submit" value="搜索"/>
				            <input type="button" value="创建" onclick='window.location="<ls:url address='/admin/floorItem/load'/>"'/>
				      </div>
		     </td></tr></tbody>
	    </table>
    </form:form>
    <div align="center">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		<display:table name="list" requestURI="/admin/floorItem/query" id="item" export="false" sort="external" class="${tableclass}" style="width:100%">
	    
     		<display:column title="FiId" property="fiId"></display:column>
     		<display:column title="RecDate" property="recDate"></display:column>
     		<display:column title="FloorId" property="floorId"></display:column>
     		<display:column title="ReferId" property="referId"></display:column>
     		<display:column title="Type" property="type"></display:column>
     		<display:column title="SortLevel" property="sortLevel"></display:column>
     		<display:column title="Seq" property="seq"></display:column>

	    <display:column title="Action" media="html">
		      <a href="<ls:url address='/admin/system/floorItem/load/${item.fiId}'/>" title="修改">
		     		 <img alt="修改" src="<ls:templateResource item='/resources/common/images/grid_edit.png'/>">
		      </a>
		      <a href='javascript:deleteById("${item.fiId}")' title="删除">
		      		<img alt="删除" src="<ls:templateResource item='/resources/common/images/grid_delete.png'/>">
		      </a>
	      </display:column>
	    </display:table>
        <c:if test="${not empty toolBar}">
            <c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
        </c:if>
    </div>
        <script language="JavaScript" type="text/javascript">
			<!--
			  function deleteById(id) {
			      if(confirm("  确定删除 ?")){
			            window.location = "<ls:url address='/admin/floorItem/delete/" + id + "'/>";
			        }
			    }
			
			        function pager(curPageNO){
			            document.getElementById("curPageNO").value=curPageNO;
			            document.getElementById("form1").submit();
			        }
			//-->
		</script>
</body>
</html>

