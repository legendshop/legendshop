<!DOCTYPE HTML>
<html class="no-js fixed-layout">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ include file="../back-common.jsp"%>
<%@ taglib uri="http://www.legendesign.net/date-util"  prefix="dateUtil" %>
<jsp:useBean id="nowTime" class="java.util.Date" />
<fmt:formatDate value="${nowTime}"  pattern="yyyy-MM-dd HH:mm" var="nowDate"/>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>预售订单管理- 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item="/resources/common/css/pagination.css"/>" />
  <link href="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.css'/>" rel="stylesheet" />
<style type="text/css">
/* .orderlist_one {
    float: left;
    padding-bottom: 0px;
    width: 100%;
    margin-top:15px;
}
.orderlist_one_h4 {
    background:#eeeeee none repeat scroll 0 0;
    border: 1px solid #eeeeee;
    height: 50px;
    line-height: 25px;
    margin-top: -1px;
    margin-bottom:0px;
    padding-left:10px;
}
.orderlist_one_h4 span {
    padding-left: 20px;
    font-size: 12px;
}
.user_list{
	margin-left:0.5rem;
}

.order_xx {
    display: block;
    position: relative;
}

.xx_sp {
    float: left;
    text-align: left;
    width: 50px;
}

.xx_date {
    float: left;
    text-align: left;
}
.xx {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #ddd;
    float: left;
    left: 37px;
    padding: 5px;
    position: absolute;
    top: 15px;
    width: 250px;
}
.xx h6 {
    color: #3399ff;
    font-size: 12px;
    height: 20px;
    line-height: 20px;
    text-align: center;
}
.xx ul{
	padding-left:0;
}
.xx ul li {
    list-style-type: none;
    display: inline;
}
.xx ul li div{
	clear: both;
}
.Popup_shade {
    background: #000 none repeat scroll 0 0;
    display: block;
    height: 100%;
    left: 0;
    min-width: 1200px;
    opacity: 0.3;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 1094;
}
取消订单
.white_content {
    background-clip: padding-box;
    border: 1px solid rgba(0, 0, 0, 0.3);
    left: 40%;
    margin-top: -60px;
    overflow: auto;
    position: absolute;
    text-align: center;
    top: 50%;
    width: 404px;
    z-index: 2147483643;
}
.white_close {
	color:#fff;
    display: block;
    float: right;
    height: 21px;
    position: absolute;
    right: 15px;
    top: 0px;
    width: 21px;
    font-size:28px;
    z-index: 1000000;
}
.white_box {
    background: #fff none repeat scroll 0 0;
    margin-left: auto;
    margin-right: auto;
    overflow: hidden;
    padding-bottom: 30px;
    text-align: center;
}
.white_box h1 {
    background: #0e90d2;
    border-bottom: 1px solid #ccc;
    color: #fff;
    float: left;
    font-size: 16px;
    line-height: 40px;
    text-align: left;
    text-indent: 1em;
    width: 100%;
}

.box_table {
    border-collapse: collapse;
    font-size: 12px;
    width: 100%;
    border: 0px;
    margin: 0px;
}
 .editAdd_load{position: absolute;top:50%;left:50%;margin:-16px 0 0 -16px;z-index: 99;display: none;} */
</style>
 </head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">

<div class="seller_right">
	 <table class="${tableclass} title-border" style="width: 100%">
	   	<tr><th class="title-border">订单管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">预售订单管理</span></th>
	   	</tr>
	 </table>
      <div class="user_list">
        <div class="seller_list_title">
          <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
            <li <c:if test="${empty paramDto.status}"> class="this am-active"</c:if>><i></i><a href="<ls:url address="/admin/order/queryPresellOrders"/>">所有订单</a></li>
            <li <c:if test="${paramDto.status==1}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/order/queryPresellOrders?status=1"/>">已经提交</a></li>
            <li <c:if test="${paramDto.status==2}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/order/queryPresellOrders?status=2"/>">已经付款</a></li>
            <li <c:if test="${paramDto.status==3}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/order/queryPresellOrders?status=3"/>">已经发货</a></li>
            <li <c:if test="${paramDto.status==4}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/order/queryPresellOrders?status=4"/>">已经完成</a></li>
            <li <c:if test="${paramDto.status==5}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/order/queryPresellOrders?status=5"/>">已经取消</a></li>
          </ul>
        </div>
        <form:form id="ListForm" method="POST" action="${contextPath}/admin/order/queryPresellOrders"> 
            <input type="hidden" value="${paramDto.curPageNO==null?1:paramDto.curPageNO}" id="curPageNO" name="curPageNO">
            <input type="hidden" value="${paramDto.status}" id="status" name="status">
          <div class="criteria-div"> 
	          <span class="item">
	          	<input type="text" id="shopId" name="shopId" value="${paramDto.shopId}" />
	          </span>
	            <input class="${inputclass}" type="hidden"   id="shopName" name="shopName" value="${paramDto.shopName}" class="user_title_txt"  />
	          <span class="item">
	            <input class="${inputclass}" type="text" placeholder="订单编号" id="subNumber" value="${paramDto.subNumber}" class="user_title_txt" name="subNumber">
	          </span>
	          <span class="item">
	            <input class="${inputclass}" type="text"  placeholder="买家" id="userName" value="${paramDto.userName}" class="user_title_txt" name="userName">
	          </span>
	          <span class="item">
	            <input type="text" value='<fmt:formatDate value="${paramDto.startDate}" pattern="yyyy-MM-dd" />' readonly="readonly"  placeholder="下单时间(起始)"  class="user_title_txt " id="startDate" name="startDate" >
	          </span>
	          <span class="item">
	            <input type="text" readonly="readonly" value='<fmt:formatDate value="${paramDto.endDate}" pattern="yyyy-MM-dd" />' id="endDate" name="endDate"  placeholder="下单时间(结束)"  class="user_title_txt" >
	          </span>
	          <span class="item">
	            <input class="${btnclass}" type="button" onclick="search()" value="查询" >
	            <%-- <input class="${btnclass}" onclick="export_excel();" type="button" value="导出本页数据" > --%>
	            <input class="${btnclass}" onclick="exportAll();" type="button" value="导出搜索数据" >
	          </span>
          </div>
        </form:form>    
     <div align="left" id="presellOrderContentList" name="presellOrderContentList" class="order-content-list"></div>     
     <div style="float:left;height:300px;"></div>                                                                                                
   </div>
</div>
<!-- <div style="clear: both;"> -->
        
<!--   </div> -->

<div class="Popup_shade" style="display: none;"></div>
<div id="order_cancel" style="display: none;">
	<div class="white_content ui-draggable"
		style="position: absolute; top: 30%; left: 30%; width: 400px;">
		<a onclick="closeOrderCancel();" class="white_close" style="font-family: '微软雅黑';" href="javascript:void(0);">×</a>
		<div class="white_box">
			<h1 style="cursor: move;margin-bottom:0;">取消订单</h1>
				<table width="390" cellspacing="0" cellpadding="0" border="0"
					class="${tableclass} no-border" style="float:left;min-width:390px;">
					<tbody>
						<tr>
							<td align="left" colspan="2">您确定要取消下面的订单吗?</td>
						</tr>
						<tr>
							<td width="100" align="right">订单号：
							 <input type="hidden" value="" id="sub_id"></td>
							 <td align="left"></td>
						</tr>
						<tr>
							<td align="right" valign="top"  rowspan="4">取消原因：</td>
							<td align="left" style="padding-top:0;"><input type="radio"
								onclick="switch_reason(this);" checked="checked" value="买家要求取消,改买其他商品"
								id="radio1" name="state_info"> <label for="radio1">买家要求取消</label>
							</td>
						</tr>
						<tr>
							<td align="left"><input type="radio"
								onclick="switch_reason(this);" value="商品缺货,从其他店铺购买" id="radio2"
								name="state_info"> <label for="radio2">商品缺货</label>
							</td>
						</tr>
						<tr>
							<td align="left"><input type="radio"
								onclick="switch_reason(this);" value="其他原因" id="radio3"
								name="state_info"> <label for="radio3">其他原因</label>
							</td>
						</tr>
						<tr>
							<td align="left" style="display:none;" id="other_reason">
							<textarea id="reasonTextarea" palaceholder="nnihao" style="height: 180px;" rows="5" cols="20" id="other_state_info"
									name="other_state_info" maxlength="50" placeholder="请在50字以内简短的描述您的理由" ></textarea>
							</td>
						</tr>
						<tr>
							<td align="center" style="text-align: center;" colspan="2">
						       <span class="inputbtn">
									<input class="${btnclass}" onclick="orderCancel();" style="cursor:pointer;" value="保存" >
							   </span>
							</td>
						</tr>
					</tbody>
				</table>
		</div>
	</div>
	<div class="black_overlay" style="height: 3113px;"></div>
</div>

<!-- 调整订单费用窗口 -->
<div id="order_fee" style="display: none;">
	<div class="white_content ui-draggable"
		style="position: absolute; top: 30%; left: 30%; width: 400px;z-index: 1200; ">
		<a onclick="closeOrderFee();"
			class="white_close" dialog_uri="undefined" href="javascript:void(0);">×</a>
		<div class="white_box">
			<h1 style="cursor: move;margin-bottom:0;">调整费用</h1>
				<table width="390" cellspacing="0" cellpadding="0" border="0"
					class="${tableclass} no-border" style="float:left;min-width:390px;" >
					<tbody>
						<tr>
							<td align="right">买家用户：</td>
							<td align="left" id="orderFeeUserNameText"></td>
							<input type="hidden" id="orderFeeUserName" name="orderFeeUserName"/>
						</tr>
						<tr>
							 <td width="100" align="right">订单号：</td>
							 <td align="left" id="orderFeeIdText" ></td>
							 <input type="hidden" id="orderFeeId" name="orderFeeId"/>
						</tr>
						<tr>
							<td align="right">定金金额：</td>
							<td align="left">
							   <input type="text" maxlength="9"  id="preDepositPrice" name="preDepositPrice" class="valid">
							</td>
						</tr>
						<tr>
							<td align="right">尾款金额：</td>
							<td align="left">
							   <input type="text" maxlength="9"  id="finalPrice" name="finalPrice" class="valid">
							</td>
						</tr>
						<tr>
							<td align="right">配送金额：</td>
							<td align="left">
							<input type="text" maxlength="5"  id="freightAmount" name="freightAmount" class="valid">
							</td>
						</tr>
						<tr>
							<td align="right">订单金额：</td>
							<td align="left">
							  <span id="totalPriceText" style="color:#F00; font-weight:bold;"></span>
							  <input type="hidden" id="totalPrice" name="totalPrice">
							</td>
						</tr>
						<tr>
							<td align="center" style="text-align: center;" colspan="2">
						       <span class="inputbtn">
									<input class="${btnclass}" onclick="submitPreOrderFee();" id="orderFee" style="cursor:pointer;" value="保存" >
							   </span>
							</td>
						</tr>
					</tbody>
				</table>
		</div>
	</div>
	<div class="black_overlay" style="height: 3113px;"></div>
</div>
<%-- <img src="<ls:url address='/resources/common/images/indicator.gif'/>" class="editAdd_load"/> --%>
</div>
</div>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/presellOrderList.js'/>"></script>
<script type="text/javascript" src="<ls:templateResource item='/resources/plugins/select2-3.5.2/select2.js'/>"></script>
<script>
var contextPath = "${contextPath}";
var status = "${paramDto.status}";
var shopName = "${paramDto.shopName}";
var subNumber = "${paramDto.subNumber}"; 
var userName = "${paramDto.userName}";
var startDate = "${paramDto.startDate}";
var endDate = "${paramDto.endDate}";
</script>
</html>
