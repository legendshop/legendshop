<!Doctype html>
<html class="no-js fixed-layout">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="../back-common.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>订单详情 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/templets/amaze/css/orderAdminDetail.css" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} no-border" style="width: 100%;border-bottom:1px solid #efefef !important;">
				<thead>
					<tr>
						<th style="border:none;text-align: left" class="no-bg">
							<span class="title-span">
								订单管理  ＞  
								<span style="color:#0e90d2;">查看订单详情</span>
							</span>
						</th>
					</tr>
				</thead>
			</table>
			<div class="order_pay">
				<div class="order_pay_note">
					<span style="margin-right:30px;">订单号：${order.subNum}</span> 
					<span>状态： 
						<b style="color: #fd6760;"> 
							<c:choose>
								 <c:when test="${order.status==1 }">
								        订单已经提交，等待买家付款
								 </c:when>
								 <c:when test="${order.status==2&&order.subType=='SHOP_STORE'}">
									 待提货
								 </c:when>
								 <c:when test="${order.status==2 }">
								    <c:choose>
											<c:when test="${order.mergeGroupStatus eq 0 && order.subType eq 'MERGE_GROUP'}">拼团中</c:when>
											<c:when test="${order.subType eq 'GROUP' && order.groupStatus eq 0}">拼团中</c:when>
											<c:otherwise>待发货</c:otherwise>
										</c:choose>
								 </c:when>
								 <c:when test="${order.status==3 }">
								       待收货
								 </c:when>
								 <c:when test="${order.status==4 }">
								      已完成
								 </c:when>
								 <c:when test="${order.status==5 }">
								      交易关闭
								 </c:when>
							 </c:choose>
						</b>
					</span>
					<c:if test="${order.refundState eq 1 }">
						<span style="color: #999; margin-left: 50px; font-size: 14px; font-weight: normal;">该订单正在退款/退货中</span>
					</c:if>
					<c:if test="${order.refundState eq 2 and order.status eq 5}">
						<span style="color: #999; margin-left: 50px; font-size: 14px; font-weight: normal;">
							系统于<fmt:formatDate value="${order.updateDate}" type="both" />取消了该订单(商品全部退款完成取消订单)
						</span>
					</c:if>
				</div>
				<!-- 订单的退款状态不是已完成 或者 订单不是已取消的情况下 正常显示订单进度图 -->
				<c:if test="${order.refundState ne 2 || order.status ne 5}">
					<div class="ncm-order-step" id="order-step">
						<dl class="step-first current">
							<dt>生成订单</dt>
							<dd class="bg"></dd>
							<dd title="下单时间" class="date">
								<fmt:formatDate value="${order.subDate}" type="both" />
							</dd>
						</dl>
						<dl class="<c:if test="${not empty order.payDate}">current</c:if>">
							<dt>完成付款</dt>
							<dd class="bg"></dd>
							<c:if test="${not empty order.payDate}">
								<dd title="付款时间" class="date">
									<fmt:formatDate value="${order.payDate}" type="both" />
								</dd>
							</c:if>
						</dl>
						<c:if test="${order.subType!='SHOP_STORE'}">
						<dl class="<c:if test="${not empty order.dvyDate}">current</c:if>">
							<dt>商家发货</dt>
							<dd class="bg"></dd>
							<c:if test="${not empty order.dvyDate}">
								<dd title="发货时间" class="date">
									<fmt:formatDate value="${order.dvyDate}" type="both" />
								</dd>
							</c:if>
						</dl>
						</c:if>
						<dl
							class="<c:if test="${not empty order.finallyDate}">current</c:if>">
							<dt>${(not empty order.finallyDate || order.subType!='SHOP_STORE')?'确认收货':'确认提货'}</dt>
							<dd class="bg"></dd>
							<c:if test="${not empty order.finallyDate}">
								<dd title="${not empty order.finallyDate&&order.subType!='SHOP_STORE'?'确认收货时间':'确认提货时间'}" class="date">
									<fmt:formatDate value="${order.finallyDate}" type="both" />
								</dd>
							</c:if>
						</dl>
						<dl
							class="<c:if test="${not empty order.finallyDate}">current</c:if>">
							<dt>完成</dt>
							<dd class="bg"></dd>
							<c:if test="${not empty order.finallyDate}">
								<dd title="完成时间" class="date">
									<fmt:formatDate value="${order.finallyDate}" type="both" />
								</dd>
							</c:if>
						</dl>
					</div>
				</c:if>

				<div class="order_pay_tab">
					<span style="line-height: 30px;">商品清单</span>
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						class="order_pay_table">
						<tbody>
							<tr>
								<th width="100">商品图片</th>
								<th width="232">商品名称</th>
								<th width="100">单价</th>
								<th width="100">数量</th>
								<th width="100">规格</th>
								<th width="100">总价</th>
								<th width="100">分销用户ID</th>
								<th width="100">分销佣金</th>
							</tr>
							<!-- S 商品列表 -->
							<c:forEach items="${order.subOrderItemDtos}" var="orderItem"
								varStatus="orderItemStatues">
								<tr>
									<td align="center"><a target="_blank"
										href="${PC_DOMAIN_NAME}/views/${orderItem.prodId}"> <img
											width="62" height="62"
											src="<ls:images item='${orderItem.pic}' scale='3'/>">
									</a></td>
									<td align="left"><a target="_blank" class="blue"
										href="${PC_DOMAIN_NAME}/views/${orderItem.prodId}">${orderItem.prodName}</a>
										<c:if
											test="${not empty orderItem.snapshotId && orderItem.snapshotId!=0}">
											<a
												href="${PC_DOMAIN_NAME}/snapshot/${orderItem.snapshotId}"
												target="_blank">【商品快照】</a>
										</c:if></td>
									<td align="center"><b class="red"> ¥${orderItem.cash}
									</b></td>
									<td align="center">${orderItem.basketCount}</td>
									<td align="center">${empty orderItem.attribute?'无':orderItem.attribute}</td>
									<td align="center">¥${orderItem.productTotalAmout}</td>
									<td align="center">${empty orderItem.distUserName?"无":orderItem.distUserName}</td>
									<td align="center">${empty orderItem.distCommisCash?"无":orderItem.distCommisCash}</td>
								</tr>
							</c:forEach>

						</tbody>
					</table>

				</div>
				<div class="order_follow">

					<ul class="order_follow_top" style="padding:0;margin-bottom:0;">
						<li style="cursor: pointer" class="this">付款信息</li>
						<li style="cursor: pointer">物流信息</li>
						<li style="cursor: pointer">订单日志</li>
					</ul>

					<div class="order_follow_box">
						<table width="100%" cellspacing="0" cellpadding="0" border="0"
							class="order_follow_table">
							<tbody>
								<tr>
									<td colspan="2">付款方式： <c:choose>
											<c:when test="${order.status==1}">
							       未支付
							 </c:when>
											<c:when test="${order.payManner==1}">
							       货到付款
							 </c:when>
											<c:otherwise>
							     在线支付
							 </c:otherwise>
										</c:choose>
									</td>
								</tr>
								<tr>
									<td width="400">商品金额：¥ <fmt:formatNumber value="${order.total}" pattern="0.00"></fmt:formatNumber></td>
									<td>运费金额：¥<fmt:formatNumber value="${order.freightAmount}" pattern="0.00"></fmt:formatNumber></td>
								</tr>
								<tr>
									<td>优惠金额：¥ 0.00</td>
									<td>满减金额：¥ <fmt:formatNumber value="${order.discountPrice}" pattern="0.00"></fmt:formatNumber></td>
								</tr>
								<tr>
									<td>使用红包金额：¥ <fmt:formatNumber value="${order.redpackOffPrice}" pattern="0.00"></fmt:formatNumber></td>
									<td>使用优惠券金额：¥ <fmt:formatNumber value="${order.couponOffPrice}" pattern="0.00"></fmt:formatNumber></td>
								</tr>
								<c:if test="${not empty order.subSettlement}">
									<tr>
										<td>商城支付流水号：${order.subSettlement}</td>
									</tr>
								</c:if>
								<c:if test="${not empty order.flowTradeNo}">
									<tr>
										<td>第三方支付流水号： ${order.flowTradeNo}</td>
									</tr>
								</c:if>
								<tr>
									<td>应支付金额：¥ <fmt:formatNumber value="${order.actualTotal}" pattern="0.00"></fmt:formatNumber>&nbsp;
										<c:if test="${not empty order.predPayType}">( </c:if> 
										<c:if test="${order.predPayType eq '1'}">预存款支付金额：¥ <fmt:formatNumber value="${order.predepositAmount}" pattern="0.00"></fmt:formatNumber></c:if>
										<c:if test="${order.predPayType eq '2'}">金币支付金额：¥ <fmt:formatNumber value="${order.predepositAmount}" pattern="0.00"></fmt:formatNumber></c:if>
										<c:if test="${not empty order.predPayType}"> )</c:if>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div style="display: none" class="order_follow_box">
						<table width="100%" cellspacing="0" cellpadding="0" border="0"
							class="order_follow_table">
							<tbody id="order_kuaidi">
								<tr>
									<th align="left">时间</th>
									<th align="left">信息</th>
								</tr>
							</tbody>

						</table>
					</div>
					<div style="display: none" class="order_follow_box">
						<table width="100%" cellspacing="0" cellpadding="0" border="0"
							class="order_follow_table">
							<tbody id="order_ship">
								<tr>
									<th align="left">操作者</th>
									<th align="left">日志信息</th>
								</tr>
							</tbody>
						</table>
					</div>

				</div>
				<div class="order_pay_info">
					<%-- <h3>订单信息</h3>
					<div class="order_pay_con">
						<div class="order_pcon_in">
							<h4>收货人信息</h4>
							<ul class="order_pcon_ul">
								<li>下单用户： <a style="cursor: pointer;" id="show_userinfo"
									target="_blank"
									href='<ls:url address="/admin/userinfo/userDetail/${order.userId}"/>'><span>${order.userName}</span></a>
									<div class="more">
										<span class="arrow"></span>
										<ul id="shipping_ul">
										</ul>
									</div>
								</li>
								<li>收货人：${order.userAddressSub.receiver}</li>
								<li>收货地址：${order.userAddressSub.detailAddress}</li>
								<li>邮政编码：${order.userAddressSub.subPost}</li>
								<li>电话号码：${order.userAddressSub.telphone}</li>
								<li>手机号码：${order.userAddressSub.mobile}</li>
							</ul>
						</div>
						<div class="order_pcon_in">
							<h4>支付方式及配送方式</h4>
							<ul class="order_pcon_ul">
								<c:choose>
									<c:when test="${order.status==1}">
										<li>支付方式：未支付</li>
										<li>支付时间：尚未支付，无支付时间</li>
									</c:when>
									<c:otherwise>
										<li>支付方式：${order.payTypeName}</li>
										<li>支付时间：<fmt:formatDate value="${order.payDate}"
												type="both" /></li>
									</c:otherwise>
								</c:choose>
								<li>配送运费：¥${order.freightAmount}</li>
								<li>配送时间：工作日9点-18点可配送</li>
								<li>配送方式： <c:choose>
										<c:when test="${order.dvyType=='mail'}">
							       平邮
							 </c:when>
										<c:when test="${order.dvyType=='express'}">
							       快递
							 </c:when>
										<c:when test="${order.dvyType=='ems'}">
							       EMS
							 </c:when>
									</c:choose>
								</li>
								<c:if test="${order.status==3 || order.status==4}">
									<li>物流公司： <a href="${order.delivery.delUrl}"
										target="_blank">${order.delivery.delName}</a></li>
									<li>发货时间： <fmt:formatDate value="${order.dvyDate}"
											pattern="yyyy-MM-dd HH:mm:ss" /></li>
									<li>物流单号 <c:if test="${order.status==3}">[<a
												href="javascript:void(0);"
												onclick="deliverGoods('${order.subNum}');">修改</a>]</c:if>：${order.delivery.dvyFlowId}
									</li>
								</c:if>
								<!-- <li>快递公司：快递</li> -->
							</ul>
						</div>
						<div class="order_pcon_in">
							<h4>发票信息</h4>
							<ul class="order_pcon_ul">
								<li>发票信息： <c:choose>
										<c:when test="${empty order.invoiceSub}"> 
								    不开发票
								</c:when>
										<c:otherwise>
								   ${order.invoiceSub.typeId==1?"普通发票":"增值税"} &nbsp;${order.invoiceSub.titleId==1?"个人":"单位"} &nbsp; ${order.invoiceSub.company}<c:if test="${order.invoiceSub.titleId==2}">(${order.invoiceSub.invoiceHumNumber})</c:if>
								</c:otherwise>
									</c:choose></li>
							</ul>
						</div>
						<div class="order_pcon_in">
							<h4>买家留言</h4>
							<ul class="order_pcon_ul">
								<li><c:choose>
										<c:when test="${empty order.orderRemark}"> 
								  无
								</c:when>
										<c:otherwise>
								   ${order.orderRemark}
								</c:otherwise>
									</c:choose></li>
							</ul>
						</div>
					</div>

					<div class="order_pay_money">
						<ul style="margin-right: 30px;">
							<li>商品总金额：¥ <fmt:formatNumber value="${order.total}" pattern="0.00"></fmt:formatNumber></li>
							<li>-优惠：¥ 0.00</li>
							<li>+运费：¥ <fmt:formatNumber value="${order.freightAmount}" pattern="0.00"></fmt:formatNumber></li>
							<li>-满减金额：¥ <fmt:formatNumber value="${order.discountPrice}" pattern="0.00"></fmt:formatNumber></li>
							<li>-优惠券金额：¥ <fmt:formatNumber value="${order.couponOffPrice}" pattern="0.00"></fmt:formatNumber></li>
							<li>-红包金额：¥ <fmt:formatNumber value="${order.redpackOffPrice}" pattern="0.00"></fmt:formatNumber></li>
							<li><span>订单金额：<b>¥ <fmt:formatNumber value="${order.actualTotal}" pattern="0.00"></fmt:formatNumber></b></span></li>
						</ul>
					</div> --%>
					<table width="100%" cellspacing="0" cellpadding="0" border="0"
						class="order_info_table">
						<thead>
							<tr>
								<th style="text-align: left;" colspan="4"><span style="margin-left:10px;">订单信息</span></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td valign="top">
									<div class="order_pcon_in">
										<ul class="order_pcon_ul">
											<li>收货人信息</li>
											<li>下单用户： <a style="cursor: pointer;" id="show_userinfo"
												target="_blank"
												href='<ls:url address="/admin/userinfo/userDetail/${order.userId}"/>'><span>${order.userName}</span></a>
												<div class="more">
													<span class="arrow"></span>
													<ul id="shipping_ul">
													</ul>
												</div>
											</li>
											<li>收货人：${order.userAddressSub.receiver}</li>
											<li>收货地址：${order.userAddressSub.detailAddress}</li>
											<li>邮政编码：${order.userAddressSub.subPost}</li>
											<li>电话号码：${order.userAddressSub.telphone}</li>
											<li>手机号码：${order.userAddressSub.mobile}</li>
										</ul>
									</div>
								</td>
								<td valign="top">
									<div class="order_pcon_in">
										<ul class="order_pcon_ul">
											<li>支付方式及配送方式</li>
											<c:choose>
												<c:when test="${order.status==1}">
													<li>支付方式：未支付</li>
													<li>支付时间：尚未支付，无支付时间</li>
												</c:when>
												<c:otherwise>
													<li>支付方式：${order.payTypeName}</li>
													<li>支付时间：<fmt:formatDate value="${order.payDate}"
															type="both" /></li>
												</c:otherwise>
											</c:choose>
											<li>配送运费：¥${order.freightAmount}</li>
											<li>配送时间：工作日9点-18点可配送</li>
											<li>配送方式： <c:choose>
													<c:when test="${order.dvyType=='mail'}">
										       平邮
										 </c:when>
													<c:when test="${order.dvyType=='express'}">
										       快递
										 </c:when>
													<c:when test="${order.dvyType=='ems'}">
										       EMS
										 </c:when>
												</c:choose>
											</li>
											<c:if test="${order.status==3 || order.status==4}">
												<li>物流公司： <a href="${order.delivery.delUrl}"
													target="_blank">${order.delivery.delName}</a></li>
												<li>发货时间： <fmt:formatDate value="${order.dvyDate}"
														pattern="yyyy-MM-dd HH:mm:ss" /></li>
												<li>物流单号 <c:if test="${order.status==3}">[<a
															href="javascript:void(0);"
															onclick="deliverGoods('${order.subNum}');">修改</a>]</c:if>：${order.delivery.dvyFlowId}
												</li>
											</c:if>
											<!-- <li>快递公司：快递</li> -->
										</ul>
									</div>
								</td>
								<td valign="top">
									<div class="order_pcon_in">
										<ul class="order_pcon_ul">
											<li>发票信息</li>
											<c:choose>
												<c:when test="${empty order.invoiceSub}">
													<li>发票信息：不开发票</li>
												</c:when>
												<c:otherwise>
													<li>发票类型：${order.invoiceSub.type==1?"普通发票":"增值税发票"}</li>
													<li>抬头类型：
														<c:choose>
															<c:when test="${order.invoiceSub.title==1}">[个人抬头]</c:when>
															<c:otherwise>[公司抬头]</c:otherwise>
														</c:choose>
													</li>
													<li>发票抬头：${order.invoiceSub.company}</li>
													<c:if test="${order.invoiceSub.title ne 1}">
														<li>纳税人号：${order.invoiceSub.invoiceHumNumber}</li>
													</c:if>
													<c:if test="${order.invoiceSub.type eq 2}">
														<li>注册地址：${order.invoiceSub.registerAddr}</li>
														<li>注册电话：${order.invoiceSub.registerPhone}</li>
														<li>开户银行：${order.invoiceSub.depositBank}</li>
														<li>银行账号：${order.invoiceSub.bankAccountNum}</li>
													</c:if>
												</c:otherwise>
											</c:choose>
										</ul>
									</div>
								</td>
								<td valign="top">
									<div class="order_pcon_in">
										<h4>买家留言</h4>
										<ul class="order_pcon_ul">
											<li>
												<c:choose>
													<c:when test="${empty order.orderRemark}">
													  无
													</c:when>
													<c:otherwise>
													   ${order.orderRemark}
													</c:otherwise>
												</c:choose>
											</li>
										</ul>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					
					<div class="order_pay_money">
						<ul>
							<li>商品总金额：¥ <fmt:formatNumber value="${order.total}" pattern="0.00"></fmt:formatNumber></li>
							<li>-优惠：¥ 0.00</li>
							<li>+运费：¥ <fmt:formatNumber value="${order.freightAmount}" pattern="0.00"></fmt:formatNumber></li>
							<li>-满减金额：¥ <fmt:formatNumber value="${order.discountPrice}" pattern="0.00"></fmt:formatNumber></li>
							<li>-优惠券金额：¥ <fmt:formatNumber value="${order.couponOffPrice}" pattern="0.00"></fmt:formatNumber></li>
							<li>-红包金额：¥ <fmt:formatNumber value="${order.redpackOffPrice}" pattern="0.00"></fmt:formatNumber></li>
							<c:if test="${not empty order.changedPrice && order.changedPrice!=0}">
								<c:choose>
									<c:when test="${order.changedPrice lt 0}">
										<li>商家改价：<fmt:formatNumber value="${order.changedPrice}" pattern="0.00"></fmt:formatNumber></li>
									</c:when>
									<c:otherwise>
										<li>商家改价：+<fmt:formatNumber value="${order.changedPrice}" pattern="0.00"></fmt:formatNumber></li>
									</c:otherwise>
								</c:choose>
							</c:if>
							<li><span>订单金额：<b>¥ <fmt:formatNumber value="${order.actualTotal}" pattern="0.00"></fmt:formatNumber></b></span></li>
						</ul>
					</div>
				</div>
				
				<div
					style="margin-top: 10px; overflow: hidden; padding: 0 0 10px; width: 100%; text-align: center; padding: 20px 0 10px;">
						<input
						type="button"
						onclick="window.open('<ls:url address="/admin/order/orderPrint/${order.subNum}"/>');"
						class="${btnclass}" value="订单打印">&nbsp;&nbsp; 
						<input type="button"
						onclick="window.history.go(-1)"
						class="${btnclass}" value="返回">
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
var contextPath = "${contextPath}";
var dvyFlowId="${order.delivery.dvyFlowId}";
var dvyTypeId = '${order.delivery.dvyTypeId}';
var reciverMobile="${order.userAddressSub.mobile}";
var subId = '${order.subId}';
</script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/orderAdminDetail.js'/>"></script>
</html>
