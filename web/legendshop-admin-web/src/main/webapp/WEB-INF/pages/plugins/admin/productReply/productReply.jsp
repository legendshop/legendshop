<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<html>
    <head>
        <title>创建商品回复</title>
         <%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
</head>
    <body>
        <form:form  action="${contextPath}/admin/productReply/save" method="post" id="form1">
            <input id="id" name="i" value="${productReply.id}" type="hidden">
            <div align="center">
            <table border="0" align="center" class="${tableclass}" id="col1">
                <thead>
                    <tr class="sortable">
                        <th colspan="2">
                            <div align="center">
                                	创建网站导航
                            </div>
                        </th>
                    </tr>
                </thead>
                
		<tr>
		        <td>
		          	<div align="center">商品ID: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="prodcommId" id="prodcommId" value="${productReply.prodcommId}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">添加时间: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="addtime" id="addtime" value="${productReply.addtime}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">IP来源: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="postip" id="postip" value="${productReply.postip}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">回复内容: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="replyContent" id="replyContent" value="${productReply.replyContent}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">回复人: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="replyName" id="replyName" value="${productReply.replyName}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">回复时间: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="replyTime" id="replyTime" value="${productReply.replyTime}" />
		        </td>
		</tr>
		<tr>
		        <td>
		          	<div align="center">有用: <font color="ff0000">*</font></div>
		       	</td>
		        <td>
		           	<input type="text" name="useful" id="useful" value="${productReply.useful}" />
		        </td>
		</tr>
                <tr>
                    <td colspan="2">
                        <div align="center">
                            <input type="submit" value="保存" />
                            <input type="button" value="返回"
                                onclick="window.location='<ls:url address="/admin/productReply/query"/>'" />
                        </div>
                    </td>
                </tr>
            </table>
           </div>
        </form:form>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
        <script language="javascript">
		    $.validator.setDefaults({
		    });

    $(document).ready(function() {
    jQuery("#form1").validate({
            rules: {
            	postip: {
                required: true
            },
            replyContent: "required"
        },
        messages: {
        	postip: {
                required: "Please enter banner"
            },
            replyContent: {
                required: "Please provide a password"
            }
        }
    });
 
		//斑马条纹
     	 $("#col1 tr:nth-child(even)").addClass("even");
});
</script>
    </body>
</html>

