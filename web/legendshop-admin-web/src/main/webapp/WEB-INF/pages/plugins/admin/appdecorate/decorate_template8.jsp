<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%-- <link rel="stylesheet" href="${contextPath}/resources/plugins/kindeditor/themes/default/default.css"/>
<link rel="stylesheet" href="${contextPath}/resources/plugins/kindeditor/plugins/code/prettify.css" />
<script charset="utf-8" src="${contextPath}/resources/plugins/kindeditor/kindeditor.js"></script>
<script charset="utf-8" src="${contextPath}/resources/plugins/kindeditor/lang/zh-CN.js"></script> --%>
<%-- <script charset="utf-8" src="${contextPath}/resources/plugins/kindeditor/plugins/code/prettify.js"></script> --%>
<%-- <script src="<ls:templateResource item='/resources/plugins/kindeditor/kindeditor.js'/>"></script>
<script src="<ls:templateResource item='/resources/plugins/kindeditor/lang/zh-CN.js'/>"></script>
<script src="<ls:templateResource item='/resources/plugins/kindeditor/plugins/code/prettify.js'/>"></script> --%>
<!-- 模板5 -->
<style type="text/css"> 
.floor-template8 img{
	width:100%;
}
</style> 
   <template v-if="e.template == '8'">
        <!-- 模板8 -->
        <div class="cont_box">
            <div class="floor floor-template8" style="min-height:40px;width:100%;'" v-html="e.content" ></div>
            <div class="edit_btns" floor-index={{$index}}>
                <a href="javascript:;" class="edit">编辑</a>
                <a href="javascript:;" class="delete" v-on:click="removeModule($index)">删除</a>
            </div>
            <div class="edit_area">
                <s></s>
                <a href="javascript:;" class="close">×</a>
                <div class="edit_form">
                    <textarea name="content" floor-index={{$index}} id="content08" cols="100" rows="8" style="width:700px;height:200px;visibility:hidden;" v-html="e.content"></textarea>
                </div>
                    <a href="javascript:;" class="btn btn_green" onClick="handleSubmit(this, 'temlate8', {{$index}})">保存</a>
            </div>
         </div>
	</template>
 <!-- 模板8 -->
 <script type="text/javascript">
	   $(document).ready(function() {
			initKindEditor();
			//alert(editor.html());
		});
		//
		function initKindEditor() {
// 		alert("-----"+'.edit_form>textarea[floor-index="'+template08Index+'"]')
// 		var sssssjkj=$('.edit_form>textarea[floor-index="'+template08Index+'"]').attr("id");
		//alert(sssssjkj);
	// 		斑马条纹
			//$("#col1 tr:nth-child(even)").addClass("even");
			KindEditor.options.filterMode = false;
			editor=KindEditor.create('.edit_form>textarea[floor-index="'+template08Index+'"]', {
				cssPath : '${contextPath}/resources/plugins/kindeditor/plugins/code/prettify.css',
				uploadJson : '${contextPath}/editor/uploadJson/upload;jsessionid=${cookie.SESSION.value}',
				fileManagerJson : '${contextPath}/editor/uploadJson/fileManager',
				allowFileManager : true,
				afterBlur : function() {
					this.sync();
				},
				width : '100%',
				height : '500px',
				afterCreate : function() {
					var self = this;
					KindEditor.ctrl(document, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
					KindEditor.ctrl(self.edit.doc, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
				},
				afterChange : function (){
					this.sync();
				}
			});
			prettyPrint();
		}
</script>
