<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<c:choose>
			<%-- <c:when test="${subRefundReturn.sellerState eq 2 && subRefundReturn.isHandleSuccess eq 0 }">
			    <tr>
			        <td>&nbsp;</td>
			        <td>
			          <input type="button" value='<c:if test="${subRefundReturn.goodsState eq 4 || subRefundReturn.goodsState eq 0}">同意退款</c:if>
			          <c:if test="${subRefundReturn.goodsState ne 4 && subRefundReturn.goodsState ne 0}">强制退款</c:if>' id="returnBtn" class="${btnclass}"/>
			          <input type="button" value="退款查询" id="refundQuery" class="${btnclass}"/>
			        </td>
				</tr>
			</c:when> --%>
			<c:when test="${subRefundReturn.sellerState eq 2 && subRefundReturn.isHandleSuccess=='-1'}">
				<tr>
					<td align="right">退款状态:</td>
					<td align="left" style="color: red">退款失败 </td>
				</tr>
			</c:when>
			<c:when test="${subRefundReturn.sellerState eq 2 && subRefundReturn.isHandleSuccess=='1'}">
			 <tr>
					<td align="right">第三方退款金额:</td>
					<td align="left">￥${empty subRefundReturn.thirdPartyRefund?0:subRefundReturn.thirdPartyRefund}</td>
				</tr>
				<tr>
					<td align="right">平台退款金额:</td>
					<td align="left">￥${empty subRefundReturn.platformRefund?0:subRefundReturn.platformRefund}</td>
				</tr>
				 <tr>
					<td align="right">退款方式:</td>
					<td align="left">${subRefundReturn.handleType}</td>
				</tr>
			    <tr>
			        <td>
			          <div align="right">退款单流水号: </div>
			        </td>
			        <td align="left">${subRefundReturn.outRefundNo}</td>
			     </tr>
			    <tr>
					<td align="right">退款状态:</td>
					<td align="left">退款成功</td>
				</tr>
			</c:when>
</c:choose>
		
<script>
var refundId="${subRefundReturn.refundId}";
 </script>

