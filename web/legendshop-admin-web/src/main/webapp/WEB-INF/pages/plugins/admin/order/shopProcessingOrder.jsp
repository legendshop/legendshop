<!Doctype html>
<html class="no-js fixed-layout">
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ include file="/WEB-INF/pages/common/laydate.jsp" %>
<%@ include file="../back-common.jsp"%>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>订单管理 - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item="/resources/common/css/pagination.css"/>" />
<style>
.orderlist_one {
    float: left;
    padding-bottom: 0px;
    width: 100%;
    margin-top:15px;
}
.orderlist_one_h4 {
 /*    background:#eeeeee none repeat scroll 0 0;
    border: 1px solid #eeeeee; */
    height: 50px;
    line-height: 25px;
    margin-top: -1px;
    margin-bottom:0px;
    padding-left:10px;
}
.orderlist_one_h4 span {
    padding-left: 20px;
    font-size: 12px;
}
.user_list{
	margin-left:0.5rem;
}

.order_xx {
    display: block;
    position: relative;
}

.xx_sp {
    float: left;
    text-align: left;
    width: 50px;
}

.xx_date {
    float: left;
    text-align: left;
}
.xx {
    background: #fff none repeat scroll 0 0;
    border: 1px solid #ddd;
    float: left;
    left: 37px;
    padding: 5px;
    position: absolute;
    top: 15px;
    width: 300px;
}
.xx h6 {
    color: #3399ff;
    font-size: 12px;
    height: 20px;
    line-height: 20px;
    text-align: center;
}
.xx ul{
	padding-left:0;
}
.xx ul li {
    list-style-type: none;
    display: inline;
}
.xx ul li div{
	clear: both;
}
.Popup_shade {
    background: #000 none repeat scroll 0 0;
    display: block;
    height: 100%;
    left: 0;
    min-width: 1200px;
    opacity: 0.3;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 1094;
}
/*取消订单*/
.white_content {
    background-clip: padding-box;
    border: 8px solid rgba(0, 0, 0, 0.3);
    left: 40%;
    margin-top: -60px;
    overflow: auto;
    position: absolute;
    text-align: center;
    top: 50%;
    width: 404px;
    z-index: 2147483643;
}
.white_close {
	color:#fff;
    display: block;
    float: right;
    height: 21px;
    position: absolute;
    right: 15px;
    top: 0px;
    width: 21px;
    font-size:28px;
    z-index: 1000000;
}
.white_box {
    background: #fff none repeat scroll 0 0;
    margin-left: auto;
    margin-right: auto;
    overflow: hidden;
    padding-bottom: 30px;
    text-align: center;
}
.white_box h1 {
    background: #0e90d2;
    border-bottom: 1px solid #ccc;
    color: #fff;
    float: left;
    font-size: 16px;
    line-height: 40px;
    margin-bottom: 20px;
    text-align: left;
    text-indent: 1em;
    width: 100%;
}

.box_table {
    border-collapse: collapse;
    font-size: 12px;
    width: 100%;
    border: 0px;
    margin: 0px;
}
.editAdd_load{position: absolute;top:50%;left:50%;margin:-16px 0 0 -16px;z-index: 99;display: none;}
</style>
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		 <!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
	    <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">

<div class="seller_right">
	<table class="${tableclass}" style="width: 100%">
				    <thead>
				    	<tr><th>
				    	<strong class="am-text-primary am-text-lg">订单管理</strong> /  订单管理</th>
				    	</tr>
				    </thead>
	   		 </table>
      <div class="user_list">
        <div class="seller_list_title">
          <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
            <li <c:if test="${empty paramDto.status}"> class="this am-active"</c:if>><i></i><a href="<ls:url address="/admin/order/processing?shopId=${paramDto.shopId }"/>">所有订单</a></li>
            <li <c:if test="${paramDto.status==1}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/order/processing?shopId=${paramDto.shopId }&status=1"/>">已经提交</a></li>
            <li <c:if test="${paramDto.status==2}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/order/processing?shopId=${paramDto.shopId }&status=2"/>">已经付款</a></li>
            <li <c:if test="${paramDto.status==3}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/order/processing?shopId=${paramDto.shopId }&status=3"/>">已经发货</a></li>
            <li <c:if test="${paramDto.status==4}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/order/processing?shopId=${paramDto.shopId }&status=4"/>">已经完成</a></li>
            <li <c:if test="${paramDto.status==5}"> class="am-active"</c:if> ><i></i><a href="<ls:url address="/admin/order/processing?shopId=${paramDto.shopId }&status=5"/>">已经取消</a></li>
          </ul>
        </div>
        <div style="margin-top:15px;">
	        <form:form  id="ListForm" method="get" action="${contextPath}/admin/order/processing">
	            <input type="hidden" value="${paramDto.curPageNO==null?1:paramDto.curPageNO}" id="curPageNO" name="curPageNO">
	            <input type="hidden" value="${paramDto.status}" id="status" name="status">
	          <div class="user_sp_right"> <span>
	            <input class="${inputclass}" type="text"  placeholder="商家" id="shopName" value="${paramDto.shopName}" class="user_title_txt" name="shopName" />
	            <input class="${inputclass}" type="text" placeholder="订单编号" id="subNumber" value="${paramDto.subNumber}" class="user_title_txt" name="subNumber">
	            <input class="${inputclass}" type="text"  placeholder="用户ID" id="userName" value="${paramDto.userName}" class="user_title_txt" name="userName">
	            <input type="text" value='<fmt:formatDate value="${paramDto.startDate}" pattern="yyyy-MM-dd" />'   readonly="readonly"  placeholder="下单时间(起始)"  class="user_title_txt Wdate" id="startDate" name="startDate" >
	            <input type="text" readonly="readonly" value='<fmt:formatDate value="${paramDto.endDate}" pattern="yyyy-MM-dd" />' id="endDate" name="endDate"  placeholder="下单时间(结束)"  class="user_title_txt Wdate" >
	        	 订单类型:&nbsp;
	        	 
	        	<%--  <select  name="subType">
				  <ls:optionGroup type="select" required="false" cache="true"
	                beanName="ORDER_TYPE" selectedValue="${paramDto.subType}"/>
	            </select> --%>
	            
	            <select name="subType">
				  <option value="">-- 请选择 --</option> 
				  <option value="NORMAL" <c:if test="${paramDto.subType eq 'NORMAL'}">selected="selected"</c:if>>普通订单</option> 
				  <ls:plugin pluginId="group">
				      <option value="GROUP" <c:if test="${paramDto.subType eq 'GROUP'}">selected="selected"</c:if>>团购订单</option> 
				  </ls:plugin> 
				  <ls:plugin pluginId="auction">
				     <option value="AUCTIONS" <c:if test="${paramDto.subType eq 'AUCTIONS'}">selected="selected"</c:if>>拍卖订单</option> 
				  </ls:plugin>
				  <ls:plugin pluginId="presell">
				     <option value="PRESELL" <c:if test="${paramDto.subType eq 'PRESELL'}">selected="selected"</c:if>>预售订单</option>
				  </ls:plugin> 
                 </select>
	            
	            <input class="${btnclass}" type="button" onclick="search()" value="查询" >
	            </span>
	            <%--  <input class="${btnclass}" onclick="export_excel();" type="button" value="导出本页数据" > --%>
	            <input class="${btnclass}" onclick="exportOrder();" type="button" value="导出搜索数据" >
	          </div>
	        </form:form>    
         </div>   
         <c:forEach items="${requestScope.list}" var="order" varStatus="orderstatues">
         <div class="orderlist_one" orderId="${order.subId}" style="border:1px solid #ddd;">
                  
         <h4 class="orderlist_one_h4">
            <span>编号：${orderstatues.count}</span>
             <span>店铺名称：<a href="${contextPath}/admin/shopDetail/load/${order.shopId}">${order.shopName}</a></span>
             <span>订单号：<a href="${contextPath}/admin/order/orderAdminDetail/${order.subNum}">${order.subNum}</a></span>
             <span>下单时间：<fmt:formatDate value="${order.subDate}" type="both" /></span><br>
             <span>订单状态：
             	<strong style="color:#F00">
             		<c:choose>
						 <c:when test="${order.status==1 }">
						        待付款
						 </c:when>
						 <c:when test="${order.status==2 }">
						       待发货
						 </c:when>
						 <c:when test="${order.status==3 }">
						       待收货
						 </c:when>
						 <c:when test="${order.status==4 }">
						      已完成
						 </c:when>
						 <c:when test="${order.status==5 }">
						      交易关闭
						 </c:when>
					</c:choose>
				</strong>
			  </span> 
			  <span class="blue">付款类型：${order.payManner==1?"货到付款":"在线支付"}</span>
              <span class="blue">支付方式：
                  <c:choose>
			      		<c:when test="${order.status==1}">未支付</c:when><c:otherwise>${order.payTypeName}</c:otherwise>
			      </c:choose>
			  </span>
              <span class="blue">订单类型： 
					<c:choose>
					  <c:when test="${order.subType == 'NORMAL'}">普通订单</c:when>
					  <c:when test="${order.subType == 'AUCTIONS'}">拍卖订单</c:when>
					  <c:when test="${order.subType == 'GROUP'}">团购订单</c:when>
					  <c:when test="${order.subType == 'SHOP_STORE'}">门店订单</c:when>
					  <c:when test="${order.subType == 'SECKILL'}">秒杀订单</c:when>
					</c:choose>
				</span>
				<c:if test="${not empty order.subSettlement}">
					<span class="blue">商城支付流水号： 
						${order.subSettlement}
					</span>
				</c:if>
				<c:if test="${not empty order.flowTradeNo}">
					<span class="blue">第三方支付流水号： 
						${order.flowTradeNo}
					</span>
				</c:if>
				<c:if test="${order.refundState eq 1 }">
					<span style="color:red">该订单正在退款/退货中</span>
				</c:if>
             </h4>
             
            <table width="100%" cellspacing="0" cellpadding="0" border="0" class="user_order_table ${tableclass }">
              <tbody>
              <tr>                 
                   <td width="38%" style="vertical-align: middle;">
                     <!-- S 商品列表 -->
				      <c:forEach items="${order.subOrderItemDtos}" var="orderItem" varStatus="orderItemStatues">
				               <div class="order_img" style="padding: 0px;width: 100%;">
			                         <div style="width: 50px;float: left;margin-top: 27px;">
			                           <a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">
			                               <img src="<ls:images item="${orderItem.pic}" scale="3"/>">
			                            </a>
			                        </div><br>
			                         <div style="width: 80%;float: left;margin-left:20px;">
			                           <span class="order_img_name" ><a target="_blank" href="<ls:url address='/views/${orderItem.prodId}'/>">${orderItem.prodName}</a>
			                           <c:if test="${not empty orderItem.snapshotId && orderItem.snapshotId!=0}" >
											<a  href="<ls:url address='/snapshot/${orderItem.snapshotId}'/>" target="_blank">[交易快照]</a>
										 </c:if>
			                           </span>
			                            <br>
			                             <span>${orderItem.attribute}</span> 
			                            <span>数量：${orderItem.basketCount}</span> 
			                            <br/>
			                            <span>促销：${orderItem.promotionInfo}</span> 
			                          </div>
			                          <div style="clear: both;"></div>
                               </div>
				      </c:forEach>
				      <!-- S 商品列表 -->
                </td>
                
                <td width="20%" align="center"  style="vertical-align: middle;">
                <span class="blue2 order_xx">
                  <b obj_id="${order.userName}" mark="name_${order.userName}" addrOrderId="${order.addrOrderId}">
                     <a href='<ls:url address="/admin/userinfo/userDetail/${order.userId}"/>'>${order.userName}</a>
                  </b>
                  </span>
                 </td>
                   <td width="21%" align="center"  style="vertical-align: middle;"><span class="order_sp">¥<fmt:formatNumber  value="${order.actualTotal}" pattern="0.00"></fmt:formatNumber></span>
                   <br>
	                <c:if test="${not empty order.freightAmount}">
	                   <span class="order_sp"><strong>(含运费:¥<fmt:formatNumber value="${order.freightAmount}" pattern="0.00"></fmt:formatNumber>)</strong></span><span class="order_sp">
	                   <br>
	                   <c:if test="${order.status==1}">
		                    <a class="order_money_modify" freight="${order.freightAmount}" actualTotal="${order.actualTotal}" name="${order.userName}" subNumber="${order.subNum}" href="javascript:void(0);"> 
		                    <input class="${btnclass}" type="button" value="调整费用">
		                    </a>
	                   </c:if>
	                   </span>
	                </c:if>
                </td>
                  <td align="center"  style="vertical-align: middle;">
					    <a  class="order_bottom_btn" href="<ls:url address="/admin/order/orderAdminDetail/${order.subNum}"/>"><input class="${btnclass}" type="button" value="查看订单"></a>
					    </br>
                        <c:if test="${order.refundState != 1 }">
	                        <c:choose>
								 <c:when test="${order.status==1 }">
									 <a  class="order_bottom_btn" href="<ls:url address="/admin/order/receiveMoneyPage/${order.subNum}"/>"><input class="${btnclass}" type="button" value="收到货款"></a></br>
	                                 <a  href="javascript:void(0);" onclick="orderCancelShow('${order.subNum}');"> <input class="${btnclass}" type="button" value="取消订单"></a><br>
								 </c:when>
							 <c:when test="${order.status==2 }">
							       <a href="javascript:void(0);" onclick="deliverGoods('${order.subNum}');" class="order_outline_ok"  href="javascript:void(0);"><input class="${btnclass}" type="button" value="确认发货"></a><br>
							 </c:when>
							 <%--<c:when test="${order.status==3 || order.status==4}">
				                  <a href="<ls:url address="/admin/order/printDeliveryDocument/${order.subId}/${order.delivery.dvyTypeId}"/>" class="order_bottom_btn" target="_blank"><input class="${btnclass}" type="button" value="打印快递单"></a><br> 
				             </c:when>--%>
						  </c:choose>
					  </c:if>
                	  <a target="_blank" class="order_bottom_btn" href="<ls:url address="/admin/order/orderPrint/${order.subNum}"/>"><input class="${btnclass}" type="button" value="打印订单"></a> 
                </td>
              </tr>
            </tbody></table>
          </div>
     </c:forEach>   
     <div style="float:left;height:300px;"></div>                                                                                          
   </div>
</div>
	<div style="clear: both;">
        <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/> 
  </div>

<div class="Popup_shade" style="display: none;"></div>
<div id="order_cancel" style="display: none;">
	<div class="white_content ui-draggable"
		style="position: absolute; top: 30%; left: 30%; width: 400px;">
		<a onclick="closeOrderCancel();" class="white_close" style="font-family: '微软雅黑';" href="javascript:void(0);">×</a>
		<div class="white_box">
			<h1 style="cursor: move;">取消订单</h1>
				<table width="390" cellspacing="0" cellpadding="0" border="0"
					class="${tableclass}" style="float:left;">
					<tbody>
						<tr>
							<td valign="top" align="left" colspan="2">您确定要取消下面的订单吗?</td>
						</tr>
						<tr>
							<td width="100" valign="top" align="right">订单号：
							 <input type="hidden" value="" id="sub_id"></td>
							 <td align="left"></td>
						</tr>
						<tr>
							<td valign="top" align="right" rowspan="4">取消原因：</td>
							<td align="left"><input type="radio"
								onclick="switch_reason();" checked="checked" value="买家要求取消,改买其他商品"
								id="radio1" name="state_info"> <label for="radio1">买家要求取消</label>
							</td>
						</tr>
						<tr>
							<td align="left"><input type="radio"
								onclick="switch_reason();" value="商品缺货,从其他店铺购买" id="radio2"
								name="state_info"> <label for="radio2">商品缺货</label>
							</td>
						</tr>
						<tr>
							<td align="left"><input type="radio"
								onclick="switch_reason();" value="其他原因" id="radio3"
								name="state_info"> <label for="radio3">其他原因</label>
							</td>
						</tr>
						<tr>
							<td align="left" style="display:none;" id="other_reason">
							<textarea style="height: 80px;" rows="5" cols="20" id="other_state_info"
									name="other_state_info" maxlength="200"></textarea>
							</td>
						</tr>
						<tr>
							<td align="center" style="text-align: center;" colspan="2">
						       <span class="inputbtn">
									<input type="button" class="${btnclass}" onclick="orderCancel();" style="cursor:pointer;" value="保存" >
							   </span>
							</td>
						</tr>
					</tbody>
				</table>
		</div>
	</div>
	<div class="black_overlay" style="height: 3113px;"></div>
</div>


<div id="order_fee" style="display: none;">
	<div class="white_content ui-draggable"
		style="position: absolute; top: 30%; left: 30%; width: 400px;">
		<a onclick="closeOrderFee();"
			class="white_close" dialog_uri="undefined" href="javascript:void(0);">×</a>
		<div class="white_box">
			<h1 style="cursor: move;">调整费用</h1>
				<table width="390" cellspacing="0" cellpadding="0" border="0"
					class="${tableclass}" style="float:left;min-width:390px;" >
					<tbody>
						<tr>
							<td valign="top" align="right">买家用户：</td>
							<td align="left" id="order_fee_username"></td>
						</tr>
						<tr>
							 <td width="100" valign="top" align="right">订单号：
							 </td>
							 <td align="left" id="order_fee_id" ></td>
						</tr>
						<!-- <tr>
							<td valign="top" align="right">商品佣金：</td>
							<td align="left" id="order_fee_id">0.08</td>
						</tr> -->
						<tr>
							<td valign="top" align="right">商品总价：</td>
							<td align="left">
							   <input type="text" maxlength="9"  id="order_fee_amount" name="order_fee_amount" class="valid">
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">配送金额：</td>
							<td align="left">
							<input type="text" maxlength="5"  id="order_fee_ship_price" name="order_fee_ship_price" class="valid">
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">订单金额：</td>
							<td align="left">
							  <span id="total_price_text" style="color:#F00; font-weight:bold;"></span>
							  <input type="hidden" id="totalPrice" name="totalPrice">
							</td>
						</tr>
						<tr>
							<td align="center" style="text-align: center;" colspan="2">
						       <span class="inputbtn">
									<input class="${btnclass}" onclick="changeOrderFee();" id="orderFee" style="cursor:pointer;" value="保存" >
							   </span>
							</td>
						</tr>
					</tbody>
				</table>
		</div>
	</div>
	<div class="black_overlay" style="height: 3113px;"></div>
</div>
<form  id="exportForm" method="post" action="${contextPath}/admin/order/export">
	<input name="data" type="hidden" id="exportParam">
</form>
<img src="<ls:url address='/resources/common/images/indicator.gif'/>" class="editAdd_load"/>
</div>
</div>
</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/shopProcessingOrder.js'/>"></script>
<script>
var contextPath = "${contextPath}";
</script>
</html>
