<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/pages/common/back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<html>
<head>
    <%@ include file="/WEB-INF/pages/common/jquery.jsp" %>
    <script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
    <script src="<ls:templateResource item='/resources/common/js/alternative.js'/>" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="${contextPath}/resources/common/css/floorTemplate.css"/>
    <script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.core.js'/>" type="text/javascript"></script>
    <script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.widget.js'/>" type="text/javascript"></script>
    <script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.mouse.js'/>" type="text/javascript"></script>
    <script src="<ls:templateResource item='/resources/plugins/jqueryui/js/jquery.ui.sortable.js'/>" type="text/javascript"></script>
    <title>编辑产品</title>
</head>
<body>

<form:form action=" " method="post" id="theForm">
    <div class="box_floor">
        <div class="box_floor_six">
        	<div class="box_floor_tit">
                <c:if test="${not empty subFloorName}">
                    <b>楼层标题：</b>
                    <input name="gf_name" type="text" id="gf_name" value="${subFloorName}" readonly="readonly"/>
                </c:if>
            </div>
            <span class="floor_choose">已选推荐商品(备注：双击删除已有商品信息，按下鼠标拖动商品次序)：</span>
            <div class="box_floor_prodel">
                <div class="floor_box_pls" id="floor_goods_info">

                    <c:forEach items="${requestScope.subFloorItemList}" var="subfloorItem">
                        <ul ondblclick="jQuery(this).remove();" class="floor_pro">
                            <li class="floor_pro_img">
                                <img id="${subfloorItem.referId}" seq="${subfloorItem.seq}" title="${subfloorItem.productName}" src="<ls:images item='${subfloorItem.productPic}' scale='2' />"/>
                            </li>
                        </ul>
                    </c:forEach>

                </div>
            </div>
            <span class="floor_choose">选择要展示的商品(注释：点击商品加入楼层)：</span>
            <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
            <div class="floor_choose_box"> 
                <span class="floor_choose_sp">
                    <lb:allSortOption id="sort" type="P"/>
                    <b>商品名称：</b>
                    <input name="prodName" type="text" id="prodName" maxlength="50"/>
                    <input type="button" value="搜索" onclick="productFloorLoad(${curPageNO});" class="edit-blue-btn"/>
                </span>
                
                <div id="floor_goods_list">

                </div>
            </div>
        </div>
        <!--图片开始-->
        <div class="submit">
        	<input type="button" value="保存设置" id="saveForm" onclick="save_form( );" class="edit-blue-btn"/>
        </div>
    </div>
</form:form>

</body>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/productFloorOverlay.js'/>"></script>
<script language="JavaScript" type="text/javascript">
	var contextPath = "${contextPath}";
	var _sfId = "${sfId}";
	var _fId = "${flId}";
	var layout = "${layout}";
	
	//注意此处不能放到JS里去
	function goods_list_set(img,brandId,name){
		var count=jQuery(".floor_box_pls ul").length;
		var add=0; 
		var limit="${limit==null?12:limit}";

		var flag=true;
		if(count>=limit){
			layer.alert("品牌不能超过"+limit, {icon:2});
			return;
		}else{
			jQuery(".floor_box_pls .floor_pro_name").each(function(){
				if(jQuery(this).attr("id")== brandId){
					layer.alert("已经存在该商品", {icon:2});
					flag = false;
					return;
				}
			});
		}
		var s="<ul ondblclick='jQuery(this).remove();' class='floor_pro'><li class='floor_pro_img' style='height:30px'><img src='<ls:images item='"+img+"' scale='2' />' /></li><li class='floor_pro_name' id='"+brandId+"'>"+name+"</li></ul>";
		if(flag){
			jQuery(".floor_box_pls").append(s);
		}
	}
	
	//选择商品信息  productFloorLoad---》goods_list_set()
	function goods_list_set(img, productId, name) {
		var count = jQuery(".floor_box_pls ul").length;
		var add = 0;
		var limit = "${limit}";
		var ceaseVar = true;
		if (count > limit) {
			layer.alert("最多只能添加" + limit + "件商品！", {icon:2});
			return;
		}

		if (count >= limit) {
			layer.alert("商品不能超过" + limit, {icon:2});
			return;
		}

		jQuery(".floor_box_pls .floor_pro_img img").each(function () {
			if (jQuery(this).attr("id") == productId) {
				layer.alert("已经存在该商品", {icon:2});
				ceaseVar = false;
			}
		});
		if (ceaseVar == false) {
			return;
		}
		var s = "<ul ondblclick='jQuery(this).remove();' class='floor_pro'><li class='floor_pro_img'><img id='" + productId + "' title='" + name + "' src='<ls:images item='"+img+"' scale='2' />' /></li></ul>";
		jQuery(".floor_box_pls").append(s);
	}
</script>
</html>