<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%
    Integer offset = (Integer) request.getAttribute("offset");
%>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
</head>

<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
	  <!-- sidebar start -->
			<jsp:include page="../frame/left.jsp"></jsp:include>
	  <!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
	    <table class="${tableclass} title-border" style="width: 100%">
	        <tr>
	            <th class="title-border">种草社区管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">种草标签管理</span></th>
	        </tr>
	    </table>
	<form:form action="${contextPath}/admin/grassLabel/query" id="form1" method="get">
		 <div class="criteria-div">
		    <span class="item">            
		             <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}"/>
		                      标签：
		             <input type="text" name="name" maxlength="50" size="30" value="${name}" class="${inputclass}" placeholder="请输入标签搜索"/>
                    <input type="button" onclick="search()" value="搜索" class="${btnclass}"/>
                    <input type="button" value="创建标签" class="${btnclass}" onclick='window.location="${contextPath}/admin/grassLabel/load"'/>
		    </span>
		 </div>
	</form:form>
	<div align="center" class="order-content-list">
	    <%@ include file="/WEB-INF/pages/common/messages.jsp" %>
	    <display:table name="list" requestURI="/admin/grassLabelList/query/${news.position}" id="item" export="false" class="${tableclass}" style="width:100%" sort="external">
	        <display:column title="顺序" style="width:70px"><%=offset++%>
	        </display:column>
	        <display:column title="标签" style="width:200px; word-wrap:break-word">
		        <div class="order_img_name">
		        	${item.name}
		        </div>
	        </display:column>
	        <display:column title="使用次数" style="width:150px">
	            ${item.refcount}
	        </display:column>
	        <display:column title="是否推荐" style="width:150px">
	            ${item.isrecommend==1?'推荐':'不推荐'}
	        </display:column>
	        <display:column title="录入时间" style="width:250px;" property="createTime" format="{0,date,yyyy-MM-dd HH:mm}" sortable="true" sortName="n.news_date"></display:column>
	        <display:column title="操作" media="html" style="width:260px;">
                <div class="table-btn-group">
                    <button class="tab-btn" onclick="window.location='${contextPath}/admin/grassLabel/load/${item.id}'">编辑</button>
					<span class="btn-line">|</span>
                    <button class="tab-btn" onclick="deleteById('${item.id}');">删除</button>
                </div>
	        </display:column>
	    </display:table>
	    <!-- 分页条 -->
	     	<div class="clearfix">
	       		<div class="fr">
	       			 <div class="page">
		       			 <div class="p-wrap">
		           		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
		    			 </div>
	       			 </div>
	       		</div>
	       	</div>
	</div>
</div>
</body>
<script language="JavaScript" type="text/javascript">

    function deleteById(id) {
    	layer.confirm('确定删除 ?', {icon:3}, function(){
            window.location = "${contextPath}/admin/grassLabel/delete/" + id;
    	});
    }
    function search() {
        $("#form1 #curPageNO").val("1");//只要是搜索,都是从第一页开始查
        $("#form1")[0].submit();
    }
</script>
</html>


