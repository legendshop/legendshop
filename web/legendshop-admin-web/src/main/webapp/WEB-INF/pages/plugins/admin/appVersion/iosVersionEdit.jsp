<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%
	Integer offset = (Integer)request.getAttribute("offset");
%>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>${sessionScope.SPRING_SECURITY_LAST_USERNAME} - 后台管理</title>
  <meta name="description" content="LegendShop 多用户商城系统">
  <meta name="keywords" content="index">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
   <link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon-precomposed" href="${contextPath}/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" type="text/css" media="screen" href="<ls:templateResource item='/resources/common/css/errorform.css'/>" />
  <style>
	.label{
		display:inline-block;
		min-width: 50px;
	}
	.content{
		display:inline-block;
		margin-right: 20px;
		color: #666;
	}
  </style>
</head>
<body>
<jsp:include page="/admin/top" />
<div class="am-cf admin-main">
	    <!-- sidebar start -->
		<jsp:include page="../frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
	    <div class="admin-content" id="admin-content" style="overflow-x:auto;">
		<!-- 标题 -->
		<table class="${tableclass} title-border" style="width: 100%">
		   	<tr>
		   		<th class="title-border">系统配置&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">APP配置</span></th>
		   	</tr>
		</table>
   	<!-- tab条 -->
   	<div style="margin-left: 0.5rem">
		<div class="seller_list_title">
          <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
          	 <li><i></i><a href="<ls:url address='/admin/appVersion/query/android'/>">安卓</a></li>
         	 <li class="am-active"><i></i><a href="javascript:void(0);">IOS</a></li>
         </ul>
        </div>
    </div>

   <div align="center">
   	<table border="0" align="center" class="${tableclass} no-border" id="col1"  style="width: 100%">
   		<thead>
	        <tr>
	         	<td width="15%">
	         		<div align="right">用户版AppStore地址 :</div>
	      		</td>
	         	<td style="text-align:left">
	         		<input type="text" id=iosUserAppstore name="iosUserAppstore" value="${bean.iosUserAppstore }" class="${inputclass}" style="width:400px;"/>
         			<input id="updateIosUserAppstore" class="${btnclass}" type="button" value="保存" />
	         	</td>
	        </tr>
	    </thead>
	    </tbody>
	        <tr>
	         	<td width="15%">
	         		<div align="right">商家版AppStore地址 :</div>
	      		</td>
	         	<td align="left">
	         		<input type="text" id="iosShopAppstore" name="iosShopAppstore" value="${bean.iosShopAppstore }" class="${inputclass}" style="width:400px;"/>
	         		<input id="updateIosShopAppstore" class="${btnclass}" type="button" value="保存" />
	         	</td>
	        </tr>
	        </tbody>
         </table>
      </div>
      </div>
      </div>
      </body>
      
<!-- 图片居中放大插件 -->
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/jquery.validate.js'/>" /></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/common/js/commonUtils.js'/>" /></script>
<script type='text/javascript' src="<ls:templateResource item='/resources/templets/js/admin/iosVersionEdit.js'/>"></script>
<script type="text/javascript">
   var contextPath = "${contextPath}";
   var urls = {
   		edit: contextPath + "/admin/appVersion/query",
   		updateIOSAppstore: contextPath + "/admin/appVersion/updateIOSAppstore",
   }
</script>
</html>