<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%
    Integer offset = (Integer) request.getAttribute("offset");
%>

<table class="${tableclass}" style="width: 100%">
   	<thead>
			<tr>
				<th><strong class="am-text-primary am-text-lg">用户管理</strong> /金币管理</th>
			</tr>
	</thead>
</table>
	
 <div style="margin-left: 0.5rem">
	<div class="seller_list_title">
         <ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
         	<li><i></i><a href="<ls:url address='/admin/coin/rechargeAdmin'/>">充值管理</a></li>
        	<li class="am-active"><i></i><a>消费明细</a></li>
         </ul>
       </div>
 </div>
   
<form:form  action="${contextPath}/admin/coin/consumeDetails" id="detailsForm" method="post">
<table class="${tableclass}" style="width: 100%">
<tbody><tr><td>
 <div align="left" style="padding: 3px">
	<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
    <input class="${inputclass}" type="text" name="userName" id="userName" maxlength="50" value="${userName}" placeholder="用户名"/>
    <input class="${inputclass}" type="text" name="mobile" id="mobile" maxlength="50" value="${mobile}" placeholder="手机号码"/>
    <input class="${btnclass}" type="submit" value="搜索"/>
 </div>
 </td></tr></tbody>
</table>
</form:form>
<div align="center">
<%@ include file="/WEB-INF/pages/common/messages.jsp"%>
	  <display:table name="list" requestURI="/admin/coin/consumeDetails" id="item" export="false" 
	  	 class="${tableclass}">
	  	 <display:column title="顺序" class="orderwidth"><%=offset++%></display:column>
	  	 <display:column title="用户名" property="userName"></display:column>
	  	 <display:column title="手机号码" property="mobile"></display:column>
	  	 <display:column title="昵称" property="nickName"></display:column>
	  	 <display:column title="金额变更" property="amount"></display:column>
	  	 <display:column title="日志描述" property="details"></display:column>
	  	 <display:column title="消费时间">
	  	 	<fmt:formatDate value="${item.addTime}" type="both" dateStyle="long" pattern="yyyy-MM-dd HH:mm" />
	  	 </display:column>
	  </display:table>
 
      <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/>
    </div>
   </td><tr></table>    
<script language="JavaScript" type="text/javascript">
     function pager(curPageNO){
            document.getElementById("curPageNO").value=curPageNO;
            document.getElementById("detailsForm").submit();
     }
    
</script>

