<!Doctype html>
<html class="no-js fixed-layout">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>积分管理 - 后台管理</title>
<meta name="description" content="LegendShop 多用户商城系统">
<meta name="keywords" content="index">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="icon" href="${contextPath}/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon-precomposed"
	href="${contextPath}/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Amaze UI" />
<link rel="stylesheet" type="text/css" media="screen"
	href="${contextPath}/resources/common/css/errorform.css" />
</head>
<body>
	<jsp:include page="/admin/top" />
	<div class="am-cf admin-main">
		<!-- sidebar start -->
		<jsp:include page="/WEB-INF/pages/plugins/admin/frame/left.jsp"></jsp:include>
		<!-- sidebar end -->
		<div class="admin-content" id="admin-content"
			style="overflow-x: auto;">
			<table class="${tableclass} title-border" style="width: 100%">
				<tr>
					<th class="title-border">
						积分管理&nbsp;＞&nbsp;<span style="color:#0e90d2; font-size:14px;">积分管理</span>
					</th>
				</tr>
			</table>
			<div>
				<div class="seller_list_title">
					<ul class="seller_title_ul am-tabs-nav am-nav am-nav-tabs">
						<li><i></i><a
							href="<ls:url address="/admin/userIntegral/query"/>">积分明细</a></li>
						<li class="am-active"><i></i><a>积分增减</a></li>
					</ul>
				</div>
			</div>
			<form:form action="${contextPath}/admin/userIntegral/decreaseSave"
				method="post" id="form1">
				<div align="center" style="margin-top: 10px;">
					<table align="center" class="${tableclass} no-border" id="col1">
						<tr>
							<td style="width:13%;min-width:185px;">
								<div align="right">
									会员手机号码 :<font color="ff0000">*</font>
								</div>
							</td>
							<td align="left"><input type="text" name="nickName" id="nickName"
								style="width: 300px" placeholder="请输入会员手机号码"
								class="${inputclass}"
								onkeyup="this.value=this.value.replace(' ','')" />&nbsp;&nbsp;<br>
								<span id="info"></span> <input type="hidden" id="userId"
								name="userId"> <input type="hidden" id="score"
								name="score"></td>
						</tr>


						<%--        <tr>
	        <td>
	          <div align="right">手机号码 <font color="ff0000">*</font></div>
	       </td>
	        <td>
	           <input type="text" name="userMobile" id="userMobile" class="${inputclass}" onkeyup="this.value=this.value.replace(' ','')"/>&nbsp;&nbsp;<br>
	           <span id="userMobileInfo" ></span>
	        </td>
       </tr> --%>


						<tr>
							<td>
								<div align="right">增减类型:&nbsp;</div>
							</td>
							<td align="left"><select class="${selectclass}" id="type">
									<option value="1">增加</option>
									<option value="-1">减少</option>
							</select></td>
						</tr>

						<tr>
							<td>
								<div align="right">
									积分值: <font color="ff0000">*</font>
								</div>
							</td>
							<td align="left"><input type="text" name="integralNum" id="integralNum"
								class="${inputclass}" /></td>
						</tr>

						<tr>
							<td valign="top">
								<div align="right">
									描述: <font color="ff0000">*</font>
								</div>
							</td>
							<td align="left"><textarea name="logDesc" id="logDesc" cols="60" rows="5"></textarea>
								<br> <br>描述信息将显示在积分明细相关页，会员和管理员都可见</td>
						</tr>
						<tr>
							<td></td>
							<td colspan="2"><input class="${btnclass}" type="submit"
								value="保存" /></td>
						</tr>
					</table>
				</div>
			</form:form>
		</div>
	</div>
</body>
<script src="${contextPath}/resources/common/js/jquery.validate.js" type="text/javascript"></script>
<script src="<ls:templateResource item='/resources/common/js/jquery.form.js'/>" type="text/javascript"></script>
<script language="javascript">
	jQuery.validator.addMethod("check", function() {
		return $("#nickName").val() != "" || $("#userMobile").val() != "";
	}, "手机号码必须有值！");

	$(document).ready(function() {
		jQuery("#form1").validate({
			rules : {
				nickName : {
					required: true,
					check : true
				},
				integralNum : {
					required : true,
					digits : true,
					range:[0,999999999]
				},
				logDesc : {
					required : true
				}
			},
			messages : {
				nickName: {
					required: "请输入用户号码"
				},
				integralNum : {
					required : "请输入积分值",
					digits : "请输入正整数",
					range : "请输入0-999999999之间的正整数"
				},
				logDesc : {
					required : "请输入描述信息"
				}
			},
			submitHandler : function(form) {
				if (!status) {
					layer.alert('用户名信息不正确',{icon: 0});
					return false;
				}

				var typeVal = $("#type option:selected").val();
				var score = $("#integralNum").val();
				if (typeVal < 0) {
					$("#integralNum").val(-score);
				}
				$("#form1").ajaxForm().ajaxSubmit({success : function(data) {
					if (typeVal < 0) {
						$("#integralNum").val(score);
					}
					var result = eval(data);
					if (result == 'OK') {
						layer.alert('操作成功！',{icon: 1},function(){
							window.location.href = "${contextPath}/admin/userIntegral/query";
						});
						return;
					} else {
						layer.alert(result,{icon: 2});
						return false;
					}
				},
				error : function(XMLHttpRequest,textStatus,errorThrown) {
					if (typeVal < 0) {
						$("#integralNum").val(score);
					}
					layer.alert('系统异操作常！',{icon: 2});
					return false;
				}
				});
			 }
          });
						var status;
						$("#nickName").blur(function() {
							var nickName = $("#nickName").val();
							if (nickName == null
									|| nickName == undefined
									|| nickName == "") {
								return false;
							}
						jQuery.ajax({
									type : 'POST',
									url : "${contextPath}/admin/userIntegral/findUser",
									data : {"nickName" : nickName},
									async : false, //默认为true 异步   
									dataType : 'json',
									error : function(data) {
									},
									success : function(data) {
										var result = eval(data);
										$("#info").text(result.msg);
										$("#userId").val(result.userId);
										$("#score").val(result.score);
										status = result.status;
									}
								});
					  });

						$("#userMobile").blur(function() {
							var userMobile = $("#userMobile").val();
							if (userMobile == null
									|| userMobile == undefined
									|| userMobile == "") {
								return false;
							}
						jQuery.ajax({
									type : 'POST',
									url : "${contextPath}/admin/userIntegral/findUser",
									data : {"nickName" : userMobile},
									async : false, //默认为true 异步   
									dataType : 'json',
									error : function(data) {
									},
									success : function(data) {
										var result = eval(data);
										$("#userMobileInfo").text(result.msg);
										$("#userId").val(result.userId);
										$("#score").val(result.score);
										status = result.status;
									}
								});
							});

					});
</script>
</html>