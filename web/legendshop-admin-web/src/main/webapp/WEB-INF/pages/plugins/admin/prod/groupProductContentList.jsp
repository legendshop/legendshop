<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../back-common.jsp" %>
<%@ include file="/WEB-INF/pages/common/taglib.jsp" %>
<%@ include file="/WEB-INF/pages/common/layer.jsp" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ include file="/WEB-INF/pages/common/messages.jsp" %>
	<display:table name="list" requestURI="/queryProdContentList?groupId=${prod.groupId}" id="item" export="false" class="${tableclass}" sort="external">

		<display:column title="图片" style="width:100px">
			<a href="${PC_DOMAIN_NAME}/views/${item.prodId}" target="_blank" title="${item.name}"><img width="65" height="65" src="<ls:images item='${item.pic}' scale='3'/>"
			<c:if test="${item.prodType eq 'S'}">
				<p style="background-color: #ff875a;color: #fff;margin-bottom: 5px;margin-top: 5px;padding: 5px;width: 41px;">秒杀</p>
			</c:if>
		</display:column>

		<display:column title="商品名称" style="width:450px;">
			<div class="order_img_name">
					${item.name}
			</div>
		</display:column>

		<display:column title="价格" style="width:100px">
			原价：${item.price}<br/>
			现价：${item.cash}
		</display:column>

		<display:column title="商品状态">
			<c:choose>
				<c:when test="${item.status==1}">出售中</c:when>
				<c:when test="${item.status==0}">已下线</c:when>
				<c:when test="${item.status==2}">违规下架</c:when>
				<c:when test="${item.status==3}">待审核</c:when>
				<c:when test="${item.status==4}">审核不通过</c:when>
				<c:when test="${item.status==-1}">已删除</c:when>
			</c:choose>
		</display:column>

		<display:column title="库存" sortable="true" sortName="stocks" style="width:100px">
			<c:choose>
				<c:when test="${item.stocksArm ne 0 && (item.stocks le item.stocksArm) }">
					<span style="color: red;font-weight: bold;font-size: 15px;">${item.stocks}</span>
				</c:when>
				<c:otherwise>
					${item.stocks}
				</c:otherwise>
			</c:choose>
		</display:column>

		<display:column title="店铺" sortable="true"><a href="${contextPath}/admin/shopDetail/load/${item.shopId}">${item.siteName}</a></display:column>

		<display:column title="订购数" property="buys" sortable="true" sortName="buys" style="width:100px"></display:column>
	    <display:column title="操作" media="html" style="width:200px">
	        <div class="table-btn-group">
				<button class="tab-btn" onclick="deleteById('${item.id}','${prod.groupId}');">移除</button>
	        </div>
	    </display:column>
    </display:table>
   	<div class="clearfix" style="margin-bottom: 30px;">
   		<div class="fr">
   			 <div class="page">
    			 <div class="p-wrap">
        		 	<ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="simple"/> 
 			 </div>
   			 </div>
   		</div>
   	</div>
