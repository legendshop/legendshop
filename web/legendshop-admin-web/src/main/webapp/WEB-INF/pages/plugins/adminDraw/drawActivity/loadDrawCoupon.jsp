<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/back-dialog.jsp"%>
<html>
<head>
<style type="text/css">
	html {font-size: 13px;}
	table {margin: 10px;border: 1px solid #ddd;}
	table tbody tr {height: 70px;}
	table tbody tr td {border-bottom: 1px solid #ddd;text-align: center;}
	table thead tr td {border-bottom: 1px solid #ddd;text-align: center;padding: 5px;background: #f9f9f9;}
</style>
</head>
<body>
	<form:form action="${contextPath}/admin/drawActivity/loadDrawCoupon/${level}" id="form1" method="post">
		<input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
		<div style="height: 50px; background-color: #f9f9f9; border: 1px solid #ddd; font-size: 14px; margin: 10px;">
			<div style="padding-top: 10px; padding-left: 20px;">
				<input style="width: 550px; border: 1px solid #ddd; padding: 5px;" type="text" name="couponName" id="name" maxlength="20" value="${couponName}" size="20" placeholder="请输入红包名称" /> 
				<input type="submit" value="搜索" style="cursor: pointer; outline: none; background: #0e90d2; color: #fff; padding: 5px 10px; border: 0px;" />
			</div>
		</div>
	</form:form>
	<table>
		<thead>
			<tr>
				<td width="300">名称</td>
				<td width="300">规则</td>
				<td width="300">券存量</td>
				<td width="110">操作</td>
			</tr>
		</thead>
		<tbody>
			<c:if test="${empty requestScope.list}">
				<tr>
					<td colspan="4" style="text-align: center;">没有找到符合条件的红包</td>
				</tr>
			</c:if>
			<c:forEach items="${requestScope.list}" var="coupon">
				<tr>
					<td>${coupon.couponName}</td>
					<td>满<fmt:formatNumber type="number" value="${coupon.fullPrice}" pattern="0.00" maxFractionDigits="2" />
						减<fmt:formatNumber type="number" value="${coupon.offPrice}" pattern="0.00" maxFractionDigits="2" /></td>
					<td>${coupon.couponNumber-coupon.bindCouponNumber}</td>
					<td>
						<button style="padding: 5px; background: #0e90d2; color: #fff; border: 0px; cursor: pointer;"
							onclick="addShop('${coupon.couponId}','${coupon.couponName}','${coupon.couponType}','${coupon.fullPrice}','${coupon.offPrice}','${coupon.couponNumber-coupon.bindCouponNumber}')">选择</button>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<ls:page pageSize="${pageSize }" total="${total}" curPageNO="${curPageNO }" type="default" />
	<script type="text/javascript">
		var level = '${level}';
		function pager(curPageNO) {
			document.getElementById("curPageNO").value = curPageNO;
			document.getElementById("form1").submit();
		}
		function addShop(couponId, couponName, couponType, fullPrice, offPrice,number) {
			parent.addCouponTo(level, couponId, couponName, couponType, fullPrice, offPrice, number);
		}
	</script>
</body>
</html>