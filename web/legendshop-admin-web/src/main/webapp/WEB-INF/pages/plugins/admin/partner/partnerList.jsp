<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>

<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

    <form:form  action="${contextPath}/admin/partner/query" id="form1" method="post">
        <table class="${tableclass}" style="width: 100%">
		    <thead>
		    	<tr><th> <strong class="am-text-primary am-text-lg">商城管理</strong> /  供应商管理</th></tr>
		    </thead>
		     <tbody><tr><td>
 <div align="left" style="padding: 3px">
 <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
            商城&nbsp;<input type="text" name="userName" maxlength="50" value="${partner.userName}" class="${inputclass}"/>
            &nbsp;&nbsp;登录名&nbsp;<input type="text" name="partnerName" maxlength="50" value="${partner.partnerName}" class="${inputclass}"/>
            <input type="submit" value="搜索" class="${btnclass}" />
            <input type="button" value="创建供应商" class="${btnclass}" onclick='window.location="<ls:url address='/admin/partner/load'/>"'/>
 </div>
 </td></tr></tbody>
	    </table>
       
    </form:form>
    <div align="center">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		<display:table name="list" requestURI="/admin/partner/query" id="item" export="false" class="${tableclass}" style="width:100%">
	<display:column title="顺序"  class="orderwidth">${item_rowNum}</display:column>
      <display:column title="登录名" property="partnerName"></display:column>
      <display:column title="名称" property="title"></display:column>
      <display:column title="主页" property="homepage"></display:column>
      <display:column title="联系人" property="contact"></display:column>
      <display:column title="联系电话" property="phone"></display:column>
      <display:column title="评论满意" property="commentGood"></display:column>
      <display:column title="评论一般" property="commentNone"></display:column>
      <display:column title="评论失望" property="commentBad"></display:column>

	    <display:column title="操作" media="html">
		      
		      <div class="am-btn-toolbar">
          		<div class="am-btn-group am-btn-group-xs">
                    	<button class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="window.location='<ls:url address='/admin/partner/load/${item.partnerId}'/>'"><span class="am-icon-pencil-square-o"></span> 编辑</button>
		 			
                		<button class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" onclick="deleteById('${item.partnerId}');" ><span class="am-icon-trash-o"></span> 删除</button>
            		
		      	 		<button class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="window.location='<ls:url address='/admin/partner/changePassword/${item.partnerId}'/>'"><span class="am-icon-pencil-square-o"></span>修改密码</button>
          		</div>
          	  </div>
	      </display:column>
	    </display:table>
        <c:if test="${not empty toolBar}">
           <ls:page pageSize="${pageSize }"  total="${total}" curPageNO="${curPageNO }"  type="default"/>
        </c:if>
    </div>
        <script language="JavaScript" type="text/javascript">
			<!--
			  function deleteById(id) {
			      if(confirm("  确定删除 ?")){
			            window.location = "<ls:url address='/admin/partner/delete/" + id + "'/>";
			        }
			    }
			  highlightTableRows("item");  
			//-->
		</script>



