<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<c:set var="systemConfig"  value="${indexApi:getSystemConfig()}"></c:set>
<%@ include file="/WEB-INF/pages/common/jquery2.1.4.jsp"%>
<%
    String  hideError = "display-hide";
    if(request.getParameter("error") != null){
    		hideError =	"display-block";
    }
 %>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html>
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<script type="text/javascript">
	if($("#backendLogo").length>0){
		window.location.href = "${contextPath}/adminlogin";
	}
</script>
<meta charset="utf-8"/>
<title>${systemConfig.shopName}</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="${contextPath}/resources/templets/amaze/login/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="${contextPath}/resources/templets/amaze/login/css/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="${contextPath}/resources/templets/amaze/login/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="${contextPath}/resources/templets/amaze/login/css/login-soft.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="${contextPath}/resources/templets/amaze/login/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>


<!-- END THEME STYLES -->
<link rel="shortcut icon" href="${contextPath}/favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="${contextPath}">
		<c:choose>
			<c:when test="${not empty adminLogo}">
				<img style="width: 128px; height: 32px" src="<ls:photo item='${adminLogo}'/>" alt=""/>
			</c:when>
			<c:otherwise>
				<img src="${contextPath}/resources/templets/amaze/login/images/logo-big2.png" alt=""/>
			</c:otherwise>
		</c:choose>
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<form:form  cssClass="login-form" action="${contextPath}/admin/j_spring_security_check" method="post">
		<h3 class="form-title">系统登录</h3>
		<div class="alert alert-danger  <%=hideError %>">
			<button class="close" data-close="alert"></button>
			<span>
			请输入正确的用户名和密码. </span>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">用户名</label>
			<div class="input-icon">
				<i class="fa fa-user"></i>
				<input type="hidden" id="returnUrl" name="returnUrl" value="${param.returnUrl}"/>
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="用户名" name="username"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">密码</label>
			<div class="input-icon">
				<i class="fa fa-lock"></i>
				<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="密码" name="password" autocomplete="off"/>
			</div>
		</div>
		<div class="form-actions">&nbsp;&nbsp;
<!-- 			<label class="checkbox">
			<input type="checkbox" name="_spring_security_remember_me" value="true"/> 记住我 </label> -->
			<button type="submit" class="btn blue pull-right">
			登录 <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
	</form:form>
	<!-- END LOGIN FORM -->

</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	 <c:choose>
		 <c:when test="${not empty shopName}">
			 <div>2015 &copy; ${shopName} - 后台管理系统. </div>
		 </c:when>
		 <c:otherwise>
			 <div>2015 &copy; LegendShop - 后台管理系统. </div>
		 </c:otherwise>
	 </c:choose>
	<div>请用IE10以上，Firefox，Chrome等浏览器访问。</div>
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<%@ include file="/WEB-INF/pages/common/jquery2.1.4.jsp"%>
<script src="${contextPath}/resources/templets/amaze/login/js/jquery-migrate.min.js" type="text/javascript"></script>
<script src="${contextPath}/resources/templets/amaze/login/js/bootstrap.min.js" type="text/javascript"></script>

<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="${contextPath}/resources/templets/amaze/login/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="${contextPath}/resources/templets/amaze/login/js/jquery.backstretch.min.js" type="text/javascript"></script>
<script type="text/javascript" src="${contextPath}/resources/templets/amaze/login/js/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="${contextPath}/resources/templets/amaze/login/js/metronic.js" type="text/javascript"></script>
<script src="${contextPath}/resources/templets/amaze/login/js/layout.js" type="text/javascript"></script>
<script src="${contextPath}/resources/templets/amaze/login/js/login-soft.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
  Metronic.init(); // init metronic core components
  Login.init();
       // init background slide images
       $.backstretch([
        "${contextPath}/resources/templets/amaze/login/bg/2.jpg",
        "${contextPath}/resources/templets/amaze/login/bg/1.jpg",
        "${contextPath}/resources/templets/amaze/login/bg/3.jpg",
        "${contextPath}/resources/templets/amaze/login/bg/4.jpg"
        ], {
          fade: 1000,
          duration: 8000
    }
    );
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>