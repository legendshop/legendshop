<%@ page language="java" isErrorPage="true" contentType="text/html;charset=UTF-8"%>
<%@include file='/WEB-INF/pages/common/taglib.jsp'%>
<div style="width:100%;margin-top:90px;">
	<center>
		<TABLE cellSpacing="0" cellPadding="0">
			<TBODY>
				<tr>
					<TD>
					<FONT face=arial,sans-serif color=#000><B style="font-size:45px;">
								<c:choose>
									<c:when test="${User_Messages.code != null}">${User_Messages.code}</c:when>
									<c:otherwise>404</c:otherwise>
								</c:choose> Error
						</B></FONT></TD>
				</tr>
				<tr>
					<TD>&nbsp;</TD>
				</tr>
			</TBODY>
		</TABLE>

		<br>
		<table cellSpacing=0 cellPadding=0 >
			<tr  style="display: none" id="errorMessages" name="errorMessages">
					<TD>${ERROR_MESSAGE}</TD>
			</tr>
			<tr>
				<td>
					<BLOCKQUOTE style="border:0">
						<c:choose>
							<c:when test="${User_Messages.title != null}">
								<H2>${User_Messages.title}</H2>
							  			 ${User_Messages.desc} 
							</c:when>
							<c:otherwise>
								<H2>系统错误</H2>
							 			 Internal error found on this server. 
							</c:otherwise>
						</c:choose>
					</BLOCKQUOTE>
				</td>
			</tr>
			<tr>
				<td>
				<pre style="background: none;border:0;">
			          .----.
			       _.'__    `.
			   .--($)($$)---/#\
			 .' @          /###\
			 :         ,   #####
			  `-..__.-' _.-\###/
			        `;_:    `"'
			      .'"""""`.
			     /,  ya ,\\
			    // error! \\
			    `-._______.-'
			    ___`. | .'___
			   (______|______)
			   </pre>
				</td>
			</tr>
			<tr>
				<td style="text-align: center">
				<a href="${contextPath}/admin/index">返回首页</a>
				</td>
			</tr>
		</table>
</center>
	<br>
</div>
<style>
	a:link{text-decoration:none ;}
	a:visited {text-decoration:none ;}
	a:hover {text-decoration:underline;}
	a:active {text-decoration:none;}
</style>