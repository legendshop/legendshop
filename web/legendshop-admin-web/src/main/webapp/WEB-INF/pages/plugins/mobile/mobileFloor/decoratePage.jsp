<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/jquery.jsp"%>
<style>

.tb_box {
  width: 465px;
  background: #fff;
  border: 1px solid #e5e5e5;
  margin-bottom: 10px;
  margin-left: auto;
  margin-right: auto;
}

.tb_box .tab_tit {
  margin: 0;
  padding: 10px;
  font-size: 20px;
  font-weight: normal;
  color: #333;
}

.tb_box .tab_tit .more {
  float: right;
  font-size: 14px;
  margin-top: 3px;
  color: #6a91ee;
  position: relative;
  padding-right: 13px;
}

.tb_box .tb_type {
  border-top: 1px solid #e5e5e5;
  overflow: hidden;
}

.tb_floor {
  width: 50%;
  float: left;
  border-right: 1px solid #dadada;
  margin-left: -1px;
}

.tb_type .th_link {
  border-bottom: 1px solid #dadada;
  border-width: 0 0 1px;
  float: right;
  width: 50%;
}

.tb_type .tb_last {
  border-bottom: none;
}

.flooredit{text-align: center;background-color: rgba(0, 0, 0, 0.5);position: absolute;display: none;}

.tb_type_even .tb_floor{float:right; border-left: 1px solid #dadada; border-right:none;}

.tb_type_even .th_link{float:left;}
</style>

<body>

<table class="${tableclass}" style="width: 100%">
      <thead>
          <thead>
			<tr><th><strong class="am-text-primary am-text-lg">商城管理</strong> /  楼层编辑</th></tr>
		</thead>
     </thead>
</table>


	<!--产品块-->
	<div class="tb_box">
		<h2 class="tab_tit">
			<a class="more" href="${mobileFloor.url}">更多</a>
			${mobileFloor.name}			
		</h2>
		<c:choose>
			<c:when test="${mobileFloor.style == 0}"><div class="tb_type tb_type_even clearfix"></c:when>
			<c:otherwise><div class="tb_type clearfix"></c:otherwise>
		</c:choose>
<%--		<div class="tb_type  clearfix">--%>
			<div id="floorImage" class="flooredit" style="width: 465px; height: 25px;z-index:1000;"><a  href="javascript:floorItemList(${fId})"  style="color:white;">内容编辑</a></div>
			
			<c:choose>
				<c:when test="${not empty requestScope.floorItemList}">
					<c:forEach items="${requestScope.floorItemList}" var="floorItem" varStatus="status" end="2"> 
						<c:choose>
							<c:when test="${status.index ==0 }">
								<a class="tb_floor" href="${floorItem.linkPath}">
									<img class="tb_pic" src="<ls:photo item="${floorItem.pic}"/>" style="height: 247px; width: 231px;">
								</a>
							</c:when>
							<c:when test="${status.index ==1 }">
								<a class="th_link" href="${floorItem.linkPath}">
									<img class="tb_pic" src="<ls:photo item="${floorItem.pic}"/>" style="height: 123px; width: 232px;">
			       				</a>
							</c:when>
							<c:when test="${status.index ==2 }">
								<a class="th_link tb_last" href="${floorItem.linkPath}">
									<img class="tb_pic" src="<ls:photo item="${floorItem.pic}"/>" style="height: 123px; width: 232px;">
			       				</a>
							</c:when>
						</c:choose>
				    </c:forEach>
				</c:when>
				<c:otherwise>
					<a class="tb_floor" href="/product/detail">
						<img class="tb_pic" src="<ls:templateResource item='/resources/common/images/mobileFloor(1).png'/>" style="height: 247px; width: 231px;">
					</a>
					<a class="th_link" href="/product/detail">
						<img class="tb_pic" src="<ls:templateResource item='/resources/common/images/mobileFloor(2).png'/>" style="height: 123px; width: 232px;">
			        </a>
					<a class="th_link tb_last" href="/product/detail">
						<img class="tb_pic" src="<ls:templateResource item='/resources/common/images/mobileFloor(2).png'/>" style="height: 123px; width: 232px;">
			       </a>
				</c:otherwise>
			</c:choose>

		</div>
	</div>
	<!--产品块 END-->

</body>

<script type="text/javascript">
var contextPath = '${contextPath}';
$(document).ready(function() {
	 $(".tb_type").hover(function () {  
			$("#floorImage").show();
		},function () {  
			$("#floorImage").hide();  
	 });
});

function floorItemList(id){
	//console.debug("id:"+id);
	art.dialog.open(contextPath+'/admin/mobileFloor/floorItemOverlay/'+id ,{
    	width: 650,
    	height: 540,
    	drag: true,
    	resize: true,
    	title: '楼层内容编辑',
    	lock: false,
    	close:function(){location.reload();}
    }); 	
}
</script>