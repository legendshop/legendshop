<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/pages/plugins/admin/back-common.jsp"%>
<%@ include file="/WEB-INF/pages/common/taglib.jsp"%>
<%@ include file="/WEB-INF/pages/common/layer.jsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

    <%
        Integer offset = (Integer) request.getAttribute("offset");
    %>
    <form:form  action="${contextPath}/admin/weixinSubscribe/query" id="form1" method="post">
        <table class="${tableclass}" style="width: 100%">
		    <thead>
		    	<tr>
			    	<th>
				    	<strong class="am-text-primary am-text-lg">微信管理</strong> /  微信关注欢迎语
			    	</th>
		    	</tr>
		    </thead>
		    <tbody><tr><td>
		    	     <div align="right"  style="padding: 3px">
					        <input type="hidden" id="curPageNO" name="curPageNO" value="${curPageNO}" />
					        <%-- 	
					            	&nbsp;名称  
					            	 <input class="${inputclass}" type="text" name="name" maxlength="50" value="${config.name}" />
					           
					            <input class="${btnclass}" type="submit" value="搜索"/> --%>
					            	<input class="${btnclass}" type="button" value="创建关注欢迎语" onclick='window.location="<ls:url address='/admin/weixinSubscribe/load'/>"'/>
					         </div>
		    </td></tr></tbody>
	    </table>
    </form:form>
    <div align="center">
          <%@ include file="/WEB-INF/pages/common/messages.jsp"%>
		 <display:table name="list" requestURI="/admin/weixinSubscribe/query" id="item"  sort="external" class="${tableclass}" style="width:100%;">
		 	<%-- <display:column title="顺序" class="orderwidth"><%=offset++%></display:column> --%>
     		<display:column title="消息类型" style="width:280px;">
     		      <c:choose>
				     <c:when test="${item.msgType=='text'}">文本消息</c:when>
				     <c:when test="${item.msgType=='news'}">图文消息</c:when>
				     <c:when test="${item.msgType=='image'}">图片消息</c:when>
				     <c:when test="${item.msgType=='image'}">图片消息</c:when>
				     <c:when test="${item.msgType=='voice'}">音频消息</c:when>
				     <c:when test="${item.msgType=='video'}">视频消息</c:when>
				 </c:choose>
     		</display:column>
     		<display:column title="消息模版" style="word-break: break-all;width:70%;word-wrap:break-word;">
     		        <c:choose>
				     	<c:when test="${item.msgType=='text'}">${item.content}</c:when>
				     	<c:otherwise>${item.templatename}</c:otherwise>
				       </c:choose>    
     		</display:column>
		    <display:column title="操作" media="html"  style="width:270px">
		    	<div class="am-btn-toolbar">
				  <div class="am-btn-group am-btn-group-xs">
						<button class="am-btn am-btn-default am-btn-xs am-text-secondary" onclick="window.location='${contextPath}/admin/weixinSubscribe/load/${item.id}'"><span class="am-icon-pencil-square-o"></span> 修改</button>
						<button class="am-btn am-btn-default am-btn-xs am-text-danger am-hide-sm-only" onclick="deleteById('${item.id}')" ><span class="am-icon-trash-o"></span> 删除</button>
				  </div>
				</div>
		      </display:column>
	    </display:table>
        <c:if test="${not empty toolBar}">
            <c:out value="${toolBar}" escapeXml="${toolBar}"></c:out>
        </c:if>
	    </div>
	<table class="am-table am-table-striped am-table-hover table-main">
		<tbody>
			<tr>
				<td align="left">说明：<br> 1.设置成功后，在接收到用户信息时，根据设置的关键字进行自动回复
					</td>
			</tr>
			<tr>
			</tr>
		</tbody>
	</table>
<script language="JavaScript" type="text/javascript">
			  function deleteById(id) {
				  layer.confirm(" 删除会影响微信服务确定删除 ?", {
						 icon: 3
					     ,btn: ['确定','关闭'] //按钮
					   }, function(){
						   window.location = "<ls:url address='/admin/weixinSubscribe/delete/" + id + "'/>";
					   });
			    }
         highlightTableRows("item");
		</script>

